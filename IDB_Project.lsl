// TASKING VX-toolset for TriCore
// Eclipse project linker script file
// 
#if 0
#if defined(__PROC_TC27XB__)
#define __REDEFINE_ON_CHIP_ITEMS 
#include "tc27xb.lsl"
processor mpe
{
    derivative = my_tc27x;
}
derivative my_tc27x extends tc27x
{
    memory dspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 112k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xd0000000, size=112k, priority=1);
        map (dest=bus:sri, dest_offset=0x70000000, size=112k);
    }
    memory pspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 24k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xc0000000, size=24k);
        map (dest=bus:sri, dest_offset=0x70100000, size=24k);
    }
    memory dspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x60000000, size=120k);
    }
    memory pspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x60100000, size=32k);
    }
    memory dspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x50000000, size=120k);
    }
    memory pspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x50100000, size=32k);
    }
    memory pflash0 (tag="on-chip")
    {
        mau = 8;
        type = rom;
        size = 2M;
        map cached(dest=bus:sri, dest_offset=0x80000000, size=2M);
        map not_cached(dest=bus:sri, dest_offset=0xa0000000, size=2M, reserved);
    }
    memory pflash1 (tag="on-chip")
    {
        mau = 8;
        type = rom;
        size = 2M;
        map cached(dest=bus:sri, dest_offset=0x80200000, size=2M);
        map not_cached(dest=bus:sri, dest_offset=0xa0200000, size=2M, reserved);
    }
    memory brom (tag="on-chip")
    {
        mau = 8;
        type = reserved rom;
        size = 32k;
        map cached(dest=bus:sri, dest_offset=0x8fff8000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xafff8000, size=32k, reserved);
    }
    memory dflash0 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 384k+16k;
        map (dest=bus:sri, src_offset=0, dest_offset=0xaf000000, size=384k);
        map (dest=bus:sri, src_offset=384k, dest_offset=0xaf100000, size=16k);
    }
    memory dflash1 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 64k;
        map (dest=bus:sri, dest_offset=0xaf110000, size=64k);
    }
    memory lmuram (tag="on-chip")
    {
        mau = 8;
        type = reserved ram;
        size = 32k;
        priority = 2;
        map cached(dest=bus:sri, dest_offset=0x90000000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xb0000000, size=32k, reserved);
    }
    memory mcs00 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x38000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x38000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs00:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs00:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs01 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x48000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x48000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs01:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs01:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs02 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x58000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x58000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs02:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs02:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs03 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x68000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x68000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs03:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs03:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
}
#else
#define __LINKONLY__
#include <cpu.lsl>
#endif


#define IFXINTTAB 0x800f9FE0
/* trap tab is on the reset address of BTV (cached area) */
#define IFXTRAPTAB 0x800fffe0 /* this is the default address (only cached area) of BTV after reset */

section_layout :vtc:linear
{
  "_lc_u_int_tab" = (IFXINTTAB);
  "_lc_u_int_tab_tc0" = (IFXINTTAB);

  // interrupt vector table
  group int_tab (ordered, contiguous, align = 4,run_addr=IFXINTTAB)
  {
          select ".text.inttab";
  }

  "_lc_u_trap_tab" = (IFXTRAPTAB);
  "_lc_u_trap_tab_tc0" = (IFXTRAPTAB);
  "_lc_u_trap_tab_tc1" = (IFXTRAPTAB+0x0100);
  "_lc_u_trap_tab_tc2" = (IFXTRAPTAB+0x0200);
  
  //"_lc_ub_table_tc1" = (0x8001c406);
  //"_lc_ub_table_tc2" = (0x8001c600);

              // trapvector table for CPU0
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0000)
  {
    //select ".text.CPU0_TRAP_HANDLER_CODE_ROM";
    select ".text.Mcal_Trap.cpu0_trap_*";
  }

              // trapvector table for CPU1
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0100)
  {
    //select ".text.CPU1_TRAP_HANDLER_CODE_ROM";
    select ".text.Mcal_Trap.cpu1_trap_*";
  }

              // trapvector table for CPU2
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0200)
  {
    //select ".text.CPU2_TRAP_HANDLER_CODE_ROM";
    select ".text.Mcal_Trap.cpu2_trap_*";
  }
  
}
/*
section_layout mpe:vtc:linear
{
  group  (ordered, run_addr=0x80000000)
  {
    select ".rodata.BMD_HDR_CONST_FAR_UNSPECIFIED";
  }
  
}
*/
/* Because of Align Problem */

section_layout mpe:vtc:linear
{
	group  (ordered, run_addr=0x60000000)
	{
	    select ".data.*";
	    select ".bss.*";
	}
  	group  (ordered, run_addr=0x60100000, copy)
  	{
  	}    	
	group (ordered, run_addr=0x8001c000)
	{
		select ".rodata.app_header";			
	}
	group (ordered, run_addr=0x8001c200)
	{
		select ".rodata.app_user_lbt";			
	}
	group (ordered, run_addr=0x8001c400)
	{
		select ".text.libc.reset";			
	}
	group (ordered, run_addr=0x8001c600, contiguous)
	{
		/*
		select "table";
		select ".rodata";
		select ".copytable";
		select ".ldata.*";
		select ".rodata.*";
		select ".text.*";
		*/
		select ".rodata.app_program";
	}
}
#endif
// TASKING VX-toolset for TriCore
// Eclipse project linker script file
// 

#define INTTAB0 INTTAB
#define INTTAB2 INTTAB
#define INTTAB1 INTTAB
#define RESET 0x8001C400 
#define INTTAB 0x800F9FE0
#if defined(__PROC_TC27XB__)
#define __REDEFINE_ON_CHIP_ITEMS 
#include "tc27xb.lsl"
processor mpe
{
    derivative = my_tc27x;
}
derivative my_tc27x extends tc27x
{
    memory dspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 112k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xd0000000, size=112k, priority=1);
        map (dest=bus:sri, dest_offset=0x70000000, size=112k);
    }
    memory pspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 24k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xc0000000, size=24k);
        map (dest=bus:sri, dest_offset=0x70100000, size=24k);
    }
    memory dspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x60000000, size=120k);
    }
    memory pspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x60100000, size=32k);
    }
    memory dspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x50000000, size=120k);
    }
    memory pspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x50100000, size=32k);
    }
    memory pflash0 (tag="on-chip")
    {
        mau = 8;
        type = rom;
        size = 2M;
        map cached(dest=bus:sri, dest_offset=0x80000000, size=2M);
        map not_cached(dest=bus:sri, dest_offset=0xa0000000, size=2M, reserved);
    }
    memory pflash1 (tag="on-chip")
    {
        mau = 8;
        type = rom;
        size = 2M;
        map cached(dest=bus:sri, dest_offset=0x80200000, size=2M);
        map not_cached(dest=bus:sri, dest_offset=0xa0200000, size=2M, reserved);
    }
    memory brom (tag="on-chip")
    {
        mau = 8;
        type = reserved rom;
        size = 32k;
        map cached(dest=bus:sri, dest_offset=0x8fff8000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xafff8000, size=32k, reserved);
    }
    memory dflash0 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 384k+16k;
        map (dest=bus:sri, src_offset=0, dest_offset=0xaf000000, size=384k);
        map (dest=bus:sri, src_offset=384k, dest_offset=0xaf100000, size=16k);
    }
    memory dflash1 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 64k;
        map (dest=bus:sri, dest_offset=0xaf110000, size=64k);
    }
    memory lmuram (tag="on-chip")
    {
        mau = 8;
        type = reserved ram;
        size = 32k;
        priority = 2;
        map cached(dest=bus:sri, dest_offset=0x90000000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xb0000000, size=32k, reserved);
    }
    memory mcs00 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x38000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x38000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs00:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs00:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs01 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x48000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x48000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs01:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs01:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs02 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x58000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x58000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs02:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs02:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs03 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x68000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x68000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs03:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs03:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
}
#elif defined(__PROC_TC27X__)
#define __REDEFINE_ON_CHIP_ITEMS 
/* E0929 Ted #include "tc27x.lsl"*/
#include "tc27x_IDB_Appl.lsl"
processor mpe
{
    derivative = my_tc27x;
}
derivative my_tc27x extends tc27x
{
    memory dspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 112k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xd0000000, size=112k, priority=1);
        map (dest=bus:sri, dest_offset=0x70000000, size=112k);
    }
    memory pspr0 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 24k;
        map (dest=bus:tc0:fpi_bus, dest_offset=0xc0000000, size=24k);
        map (dest=bus:sri, dest_offset=0x70100000, size=24k);
    }
    memory dspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x60000000, size=120k, priority=3);
    }
    memory pspr1 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc1:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x60100000, size=32k);
    }
    memory dspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 120k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xd0000000, size=120k, priority=1);
        map (dest=bus:sri, dest_offset=0x50000000, size=120k);
    }
    memory pspr2 (tag="on-chip")
    {
        mau = 8;
        type = ram;
        size = 32k;
        map (dest=bus:tc2:fpi_bus, dest_offset=0xc0000000, size=32k);
        map (dest=bus:sri, dest_offset=0x50100000, size=32k);
    }
    memory pflash1 (tag="on-chip")
    {
        mau = 8;
        type = rom;
        size = 2M;
        map cached(dest=bus:sri, dest_offset=0x80200000, size=2M);
        map not_cached(dest=bus:sri, dest_offset=0xa0200000, size=2M, reserved);
    }
    memory brom (tag="on-chip")
    {
        mau = 8;
        type = reserved rom;
        size = 32k;
        map cached(dest=bus:sri, dest_offset=0x8fff8000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xafff8000, size=32k, reserved);
    }
    memory dflash0 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 384k+16k;
        map (dest=bus:sri, src_offset=0, dest_offset=0xaf000000, size=384k);
        map (dest=bus:sri, src_offset=384k, dest_offset=0xaf100000, size=16k);
    }
    memory dflash1 (tag="on-chip")
    {
        mau = 8;
        type = reserved nvram;
        size = 64k;
        map (dest=bus:sri, dest_offset=0xaf110000, size=64k);
    }
    memory lmuram (tag="on-chip")
    {
        mau = 8;
        type = reserved ram;
        size = 32k;
        priority = 2;
        map cached(dest=bus:sri, dest_offset=0x90000000, size=32k);
        map not_cached(dest=bus:sri, dest_offset=0xb0000000, size=32k, reserved);
    }
    memory mcs00 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x38000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x38000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs00:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs00:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs01 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x48000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x48000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs01:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs01:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs02 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x58000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x58000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs02:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs02:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
    memory mcs03 (tag="on-chip")
    {
        mau = 8;
        write_unit = 4;
        type = ram;
        size = 0x1000+0x800;
        map ram0(dest=bus:aei, src_offset=0x0000, dest_offset=0xf0100000+0x68000, size=0x1000);
        map ram1(dest=bus:aei, src_offset=0x1000, dest_offset=0xf0100000+0x68000+0x1000, size=0x800);
        map mcs_ram0(dest=bus:mcs03:mcs_bus, src_offset=0x0000, dest_offset=0, size=0x1000);
        map mcs_ram1(dest=bus:mcs03:mcs_bus, src_offset=0x1000, dest_offset=0x1000, size=0x800);
    }
}
#else
#include <cpu.lsl>
#endif


#define IFXINTTAB INTTAB
 // #define IFXINTTAB INTTAB
/* trap tab is on the reset address of BTV (cached area) */
// #define IFXTRAPTAB 0x80000100 /* this is the default address (only cached area) of BTV after reset */
#define IFXTRAPTAB (IFXINTTAB + 0x6000)

section_layout :vtc:linear
{
  "_lc_u_int_tab" = (IFXINTTAB);
  "_lc_u_int_tab_tc0" = (IFXINTTAB);

  //E0923 Taeho:
  group APP_Header (ordered, run_addr=mem:IDB_ApplHeader)
  {
	  	select ".rodata.app_header";
  }
  
  group APP_User_LBT (ordered, run_addr=mem:IDB_ApplLBT)
  {
	  	select ".rodata.app_user_lbt";
  }
    
  group APP_Program (ordered, run_addr=mem:IDB_ApplCode)
  {
	  	select ".rodata.app_program";
  }
  
  // interrupt vector table
  group int_tab (ordered, contiguous, align = 4,run_addr=IFXINTTAB)
  {
          select ".text.inttab";
  }

  "_lc_u_trap_tab" = (IFXTRAPTAB);
  "_lc_u_trap_tab_tc0" = (IFXTRAPTAB);
  "_lc_u_trap_tab_tc1" = (IFXTRAPTAB+0x0100);
  "_lc_u_trap_tab_tc2" = (IFXTRAPTAB+0x0200);

              // trapvector table for CPU0
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0000)
  {
    select ".text.CPU0_TRAP_HANDLER_CODE_ROM";
      //select ".text.Mcal_Trap.cpu0_trap_*";
  }

              // trapvector table for CPU1
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0100)
  {
    select ".text.CPU1_TRAP_HANDLER_CODE_ROM";
      //select ".text.Mcal_Trap.cpu1_trap_*";
  }

              // trapvector table for CPU2
  group (ordered, contiguous, align = 1<<5, run_addr=(IFXTRAPTAB)+0x0200)
  {
    select ".text.CPU2_TRAP_HANDLER_CODE_ROM";
     // select ".text.Mcal_Trap.cpu2_trap_*";
  }
  
}

section_layout mpe:vtc:linear
{
  /* E0929 0x80000000 area is reserved for Boot area
  group  (ordered, run_addr=0x80000000)
  {
    select ".rodata.BMD_HDR_CONST_FAR_UNSPECIFIED";
  }*/
  group  (ordered, run_addr=0x80020000)
  {
    select ".rodata.BMD_HDR_CONST_FAR_UNSPECIFIED";
  }
  group (ordered, run_addr=mem:IDB_ApplTable, contiguous)
  {
  	select "table";
  }   
}

section_layout mpe:vtc:linear
{
    
    /*group  (ordered, run_addr = 0xB0001000)
    {
        select ".bss.ECU_PWM.TestArray";        
    }*/

    group  (ordered, run_addr=0x60001000, contiguous)
    {
        select ".bss.vx1000_tc2xx.gVX1000";
        select ".bss.Acmctl_ControlCurrent.GetMotIvtrCtrl";
        select ".bss.Acmctl_ControlCurrent.lacmctls16DutyA";
        select ".bss.Acmctl_ControlCurrent.lacmctls16DutyB";
        select ".bss.Acmctl_ControlCurrent.lacmctls16DutyC";
        select ".bss.Acmctl_ControlCurrent.lacmctls16Id";
        select ".bss.Acmctl_ControlCurrent.lacmctls16Iq";
        select ".bss.Acmctl_ControlCurrent.lamtr3PhDuty";
        select ".bss.LAMTR_CalcCurrentRef.lmccstPidAct";
    }  

    group  (ordered, run_addr=0x50000000)
    {
        select ".bss.NvMIf.*";
        select ".bss.Nvm_Hndlr.*";
        select ".bss.Nvm_Cfg.*";
        select ".bss.Diag_ErrProc.*";
        select ".bss.CCDrvCanDiag.*";
        select ".bss.CDCL_LinkData.*";
        select ".bss.CDCS_DiagService.*";
        select ".bss.CDS_ReadRecordValue.*";
        select ".bss.Diag_Hndlr.*";
        select ".bss.Task_10ms.*";

        select ".data.NvMIf.*";
        select ".data.Nvm_Hndlr.*";
        select ".data.Nvm_Cfg.*";
        select ".data.Diag_ErrProc.*";
        select ".data.CCDrvCanDiag.*";
        select ".data.CDCL_LinkData.*";
        select ".data.CDCS_DiagService.*";
        select ".data.CDS_ReadRecordValue.*";
        select ".data.Diag_Hndlr.*"; 
        select ".data.Task_10ms.*";

        select ".bss.Abc_Ctrl.*";
        select ".bss.CanM_Main.*";
        select ".bss.SenPwrM_Main.*";
        select ".bss.YawM_Main.*";
        select ".bss.AyM_Main.*";
        select ".bss.AxM_Main.*";
        select ".bss.SasM_Main.*";
        select ".bss.YawP_Main.*";
        select ".bss.AyP_Main.*";
        select ".bss.AxP_Main.*";
        select ".bss.SasP_Main.*";

        select ".data.Abc_Ctrl.*";
        select ".data.CanM_Main.*";
        select ".data.SenPwrM_Main.*";
        select ".data.YawM_Main.*";
        select ".data.AyM_Main.*";
        select ".data.AxM_Main.*";
        select ".data.SasM_Main.*";
        select ".data.YawP_Main.*";
        select ".data.AyP_Main.*";
        select ".data.AxP_Main.*";
        select ".data.SasP_Main.*";      
    }
    group  (ordered, run_addr=0x60000000, contiguous)
    {
        select ".bss.Adc.*";
        select ".bss.AdcIf_Conv50us.*";
        select ".bss.Ioc_InputSR50us.*";
        select ".bss.Ioc_OutputCS50us.*";
        select ".bss.Ioc_InputSR50usConditioning.*";
        select ".bss.Ioc_OutputCS50usConditioning.*";
        select ".bss.Spim_Rx50us.*";
        select ".bss.Acmio_ActrProcessing.*";
        select ".bss.Acmio_SenProcessing.*";
        select ".bss.Acmio_Actr.*";
        select ".bss.Acmio_Sen.*";
        select ".bss.Msp_Ctrl.*";
        select ".bss.Acmctl_ControlCurrent.*";
        select ".bss.Acmctl_Ctrl.*";
        select ".bss.Acmctl_MotorControllerLibrary.*";
        select ".bss.Msp_CalcMotSigProc.*";

        select ".data.Adc.*";
        select ".data.AdcIf_Conv50us.*";
        select ".data.Ioc_InputSR50us.*";
        select ".data.Ioc_OutputCS50us.*";
        select ".data.Ioc_InputSR50usConditioning.*";
        select ".data.Ioc_OutputCS50usConditioning.*";
        select ".data.Spim_Rx50us.*";
        select ".data.Acmio_ActrProcessing.*";
        select ".data.Acmio_SenProcessing.*";
        select ".data.Acmio_Actr.*";
        select ".data.Acmio_Sen.*";
        select ".data.Msp_Ctrl.*";
        select ".data.Acmctl_ControlCurrent.*";
        select ".data.Acmctl_Ctrl.*";
        select ".data.Acmctl_MotorControllerLibrary.*";
        select ".data.Msp_CalcMotSigProc.*";
    }  

    group  (ordered, run_addr=0x60100000, copy)
    {        
        select ".text.Adc.*";
        select ".text.AdcIf_Conv50us.*";
        select ".text.Ioc_InputSR50us.*";
        select ".text.Ioc_OutputCS50us.*";
        select ".text.Ioc_InputSR50usConditioning.*";
        select ".text.Ioc_OutputCS50usConditioning.*";
        select ".text.Spim_Rx50us.*";
        select ".text.Acmio_ActrProcessing.*";
        select ".text.Acmio_SenProcessing.*";
        select ".text.Acmio_Actr.*";
        select ".text.Acmio_Sen.*";
        select ".text.Msp_Ctrl.*";
        select ".text.Acmctl_ControlCurrent.*";
        select ".text.Acmctl_Ctrl.*";
        select ".text.Acmctl_MotorControllerLibrary.*";
        select ".text.Msp_CalcMotSigProc.*";

        select ".text.AdcIf_Conv1ms.*";
        select ".text.Spim_5ms.*";
        select ".text.Spim_Cdd.*";
        select ".text.Spim_Rx1ms.*";
        select ".text.Spim_Sch.*";
        select ".text.Spim_Tx1ms.*";
        select ".text.Ioc_InputCS1msConditioning.*";
        select ".text.Ioc_InputSR1msConditioning.*";
        select ".text.Ioc_InputSR5msConditioning.*";
        select ".text.Ioc_OutputCS1msConditioning.*";
        select ".text.Ioc_OutputCS5msConditioning.*";
        select ".text.Ioc_OutputSR1msConditioning.*";
        select ".text.Ioc_InputCS1ms.*";
        select ".text.Ioc_InputSR1ms.*";
        select ".text.Ioc_InputSR5ms.*";
        select ".text.Ioc_OutputCS1ms.*";
        select ".text.Ioc_OutputCS5ms.*";
        select ".text.Ioc_OutputSR1ms.*";
        select ".text.Mtr_Processing.*";
        select ".text.Mtr_Processing_Sig_Process.*";
    }  
}


memory IDB_Boot (tag="user")
{
    mau = 8;
    type = reserved rom;
    size = 0x7c00;
    map cached(dest=bus:mpe:sri, dest_offset=0x80000000, size=0x7c00, reserved);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa0000000, size=0x7c00, reserved);
}
memory IDB_Cal1 (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x4000;
    map cached(dest=bus:mpe:sri, dest_offset=0x80008000, size=0x4000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa0008000, size=0x4000, reserved);
}
memory IDB_Cal2 (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x4000;
    map cached(dest=bus:mpe:sri, dest_offset=0x8000c000, size=0x4000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa000c000, size=0x4000, reserved);
}
memory IDB_Cal3 (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x4000;
    map cached(dest=bus:mpe:sri, dest_offset=0x80010000, size=0x4000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa0010000, size=0x4000, reserved);
}
memory IDB_Cal4 (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x4000;
    map cached(dest=bus:mpe:sri, dest_offset=0x80014000, size=0x4000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa0014000, size=0x4000, reserved);
}
memory IDB_Cal5 (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x4000;
    map cached(dest=bus:mpe:sri, dest_offset=0x80018000, size=0x4000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa0018000, size=0x4000, reserved);
}
memory IDB_ApplHeader (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x200;
    map cached(dest=bus:mpe:sri, dest_offset=0x8001c000, size=0x200);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa001c000, size=0x200, reserved);
}
memory IDB_ApplLBT (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x200;
    map cached(dest=bus:mpe:sri, dest_offset=0x8001c200, size=0x200);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa001c200, size=0x200, reserved);
}
memory IDB_ApplCode (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x1c3c00;
    priority = 2;
    map cached(dest=bus:mpe:sri, dest_offset=0x8001c400, size=0x1c3c00);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa001c400, size=0x1c3c00, reserved);
}
memory IDB_ApplTable (tag="user")
{
    mau = 8;
    type = rom;
    size = 0x20000;
    priority = 2;
    map cached(dest=bus:mpe:sri, dest_offset=0x801e0000, size=0x20000);
    map not_cached(dest=bus:mpe:sri, dest_offset=0xa01e0000, size=0x20000, reserved);
}

