#ifndef __LCALLCOMMONFUNCTION_1MS_H__
#define __LCALLCOMMONFUNCTION_1MS_H__

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define LCALLCOMMONFUNCTION_PATH_VIL(_FILENAME_) STRINGIFY(../ASW/include/_FILENAME_)

/* Includes ******************************************************************/
#include LCALLCOMMONFUNCTION_PATH_VIL(Mando_Std_Types.h)
//#include LCALLCOMMONFUNCTION_PATH_VIL(Asw_Parameter_Def.h)

/*Global MACRO CONSTANT Definition *********************************************/


/*Global MACRO FUNCTION Definition *********************************************/


/*Global Type Declaration ******************************************************/


/*Global Extern Variable Declaration *******************************************/


/*Global Extern Functions Declaration ******************************************/
extern int16_t  L_s16Lpf1Int1ms( int16_t s16Xinput, int16_t s16Xold, uint8_t u8Xgain );
extern int16_t  L_s16IInter2Point1ms( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2 );
extern int16_t  L_s16IInter15Point1ms( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12, int16_t s16X13, int16_t s16Y13, int16_t s16X14, int16_t s16Y14, int16_t s16X15, int16_t s16Y15);
extern int16_t  L_s16LimitMinMax1ms( int16_t s16Xinput, int16_t s16Xmin, int16_t s16Xmax );
extern int32_t  L_s32LimitMinMax1ms( int32_t s32Xinput, int32_t s32Xmin, int32_t s32Xmax );
extern int32_t  L_s32IInter4Point1ms( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4 );
extern int32_t  L_s32Abs1ms( int32_t s32Xinput );
#endif


