#ifndef WCONTLSUBFUNC_H
#define WCONTLSUBFUNC_H

/*includes**********************************************************************/


/*Global MACRO CONSTANT Definition**********************************************/

/*Global MACRO FUNCTION Definition**********************************************/


/*Global Type Declaration ******************************************************/



/*Global Extern Variable  Declaration********************************************/
extern void WF_vMemCopy(const uint8_t *src_ptr, uint8_t *des_ptr, uint8_t number);

/*Global Extern Functions  Declaration*******************************************/



#endif
