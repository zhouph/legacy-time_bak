#ifndef __MCLIB_SINCOS_H__
#define __MCLIB_SINCOS_H__

#include "mclib_types.h"

extern Frac16 MCLIB_f16SinRez360Div48Deg(int16_t x);
extern Frac16 MCLIB_f16CosRez360Div48Deg(int16_t x);
extern Frac16 MCLIB_f16CosRez360Div1024Deg(Frac16 x);
extern Frac16 MCLIB_f16SinRez360Div1024Deg(Frac16 x);

#endif
