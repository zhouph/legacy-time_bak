#ifndef _WCOMMON_H_
#define _WCOMMON_H_

#include "../VIL/_PAR/car/Car.par.h"
#include "WTypedefs.h"

#if __MCU==MCU_PICTUS
    //#include "Genfiles\MPC560xP.h"
#elif __MCU==MCU_LEOPARD
    //#include "Genfiles\MPC5643L.h"  /* HSH_COMPILE */	/* Need to be deleted */
#else
	/*	MCU_TC27X	*/
#endif

//#include "WSetting.par"

#define MEMCAL_APCALABS            
#define MEMCAL_APCALESP         
#define MEMCAL_APCALESPENG         
#define MEMCAL_APCALABSVAFS
#define MEMCAL_APCALESPVAFS
#define MEMCAL_APCALAHB            
#define MEMCAL_APCALRBC         
#define MEMCAL_APCALAHB02         
#define MEMCAL_APCALAHB03
#define MEMCAL_APCALAHB04
#define MEMCAL_APCALAHBPCTRL
#define MEMCAL_APCALRBCOEM

// car.par 에서 향후 Setting 불가를 위하여 common.h 로 이전.
//#define __LOOP_TIME_10MS
#define __LOOP_TIME_5MS
#define __ESC_MOTOR_PWM_CONTROL     ENABLE

#if 0
#define SYSTEM_CLOCK			(( float )(6.25))
#define TIME_TICK_US(TIME)      ((uint16_t)TIME*SYSTEM_CLOCK-1)
#define TIME_TICK_100US(TIME)   (TIME_TICK_US((uint32_t)TIME) * 100UL)
#define TIME_TICK_MS(TIME)      (TIME_TICK_US((uint32_t)TIME) * 1000UL)
#endif

#define SYSTEM_CLOCK			(( float )(6.25))
#define TIME_TICK_US(TIME)      (uint16_t)( ( (uint32_t)TIME * 100 ) / 625 )
#define TIME_TICK_100US(TIME)   (TIME_TICK_US((uint32_t)TIME) / 100UL)
#define TIME_TICK_MS(TIME)      (TIME_TICK_US((uint32_t)TIME) / 1000UL)

/**************** LOOP TIME Setting ****************/
#ifdef __LOOP_TIME_10MS
    #define LOOP_TIME               (10)//ms
#elif defined(__LOOP_TIME_5MS)
    #define LOOP_TIME               (5)//ms
#endif

#define LOOP_TIME_10MS              (10)//ms

#define VALVE_1MS_COUNT             (LOOP_TIME)
#define MOTOR_1MS_COUNT             (LOOP_TIME)

#define LOOP_TIME_US                (LOOP_TIME*1000)

#define __ABS_UPLEVEL               DISABLE

#define LOOPTIME_MS(TIME)       ((TIME) / LOOP_TIME)
#define LOOPTIME_SEC(TIME)      (LOOPTIME_MS(TIME * 1000UL))
#define LOOPTIME_MIN(TIME)      (LOOPTIME_SEC(TIME * 60UL))
#define LOOPTIME_HOUR(TIME)     (LOOPTIME_MIN(TIME * 60UL))

/**************** SPEED [ 0.125KPH] ****************/

#define V_1KPH                  8
#define V_2KPH                  16
#define V_2KPH3                 19
#define V_3KPH                  24
#define V_4KPH                  32
#define V_5KPH                  40
#define V_6KPH                  48
#define V_8KPH                  64
#define V_7KPH                  56
#define V_10KPH                 80
#define V_12KPH                 96
#define V_15KPH                 120
#define V_17KPH5                140
#define V_20KPH                 160
#define V_25KPH                 200
#define V_40KPH                 320
#define V_43KPH                 344
#define V_60KPH                 480 
#define V_80KPH                 640
#define V_350KPH                2800


/**************** VOLTAGE [ MV] ****************/
#define MCU_AD_RESOL            12      // 12bit
#define AD_REF_5V               5000

#define VOLT_TO_AD_TRANS(VOLT,R1,R2) (uint16_t)((uint32_t)(VOLT*(2^MCU_AD_RESOL)*R2)/((R1+R2)*AD_REF_5V))  

#define MOTOR_AD_TRANS(VOLT)    VOLT_TO_AD_TRANS(VOLT,300,100)
#define MOTOR_12V               MOTOR_AD_TRANS(12000)
#define MOTOR_11V               MOTOR_AD_TRANS(11000)

#define FSR_AD_TRANS(VOLT)      VOLT_TO_AD_TRANS(VOLT,300,51)
#define FSR_16V                 FSR_AD_TRANS(16000)
#define FSR_9V0                 FSR_AD_TRANS(9000)

#define IGN_AD_TRANS(VOLT)      FSR_AD_TRANS(VOLT)
#define IGN_16V                 IGN_AD_TRANS(16000)
#define IGN_9V0                 IGN_AD_TRANS(9000)


/************************************************/

/**************** 3 phase Motor Macro ****************/

#define SCALE_12V					(int16_t)(2307) 			/* 1V:2307 Digit */ /* 향후 수정 예정 D0904 Voltage Input 단에서 처리 */
#define SCALE_5V    				(int16_t)(6554) 			/* 1V:6554 Digit */ /* 향후 수정 예정 D0904 Voltage Input 단에서 처리*/

#define SCALE_I_UVW   				(int16_t)(175)  			/* 1A:175 Digit */
#define _3PHASE_MTR_PEAK_I_CURRENT	(int16_t)(120)				/* A :98.99(Apk)=70(A)*1.414 */

#define _3PHASE_MTR_MAX_CURRENT		(int16_t)(SCALE_I_UVW*_3PHASE_MTR_PEAK_I_CURRENT)

#define V_IGN_OFF					(int16_t)(5*SCALE_12V)		/* 향후 수정 예정 D0904 Voltage Input 단에서 처리 */
#define V_BAT_OFF					(int16_t)(6*SCALE_12V)		/* 향후 수정 예정 D0904 Voltage Input 단에서 처리 */

#define ENGINE_START_RPM	330

/* ON/OFF Normal Circuit For 3Phase Motor Failsafe */
#define ON_Normal				1
#define OFF_Normal				0
#define Peripheral_ON			1
#define Peripheral_OFF			0
/*****************************************************/

/****************     Normal Macro    ****************/
#define	CTRL_ON						(uint8_t)(1)
#define	CTRL_OFF					(uint8_t)(0)

#define	CTRL_LO_SIDE_ON				(uint8_t)(0)
#define	CTRL_LO_SIDE_OFF			(uint8_t)(1)

#define	MON_ON						(uint8_t)(1)
#define	MON_OFF						(uint8_t)(0)

#define	STATUS_ON					(uint8_t)(1)
#define	STATUS_OFF					(uint8_t)(0)

#define U8G_CLEAR     				(uint8_t)(0)
#define U16G_CLEAR     				(uint16_t)(0)

#define U8G_BLS_PUSHED 				(uint8_t)(1)
#define U8G_BLS_NOT_PUSHED			(uint16_t)(0)

#define U8G_DOOR_OPEN 				(uint8_t)(0)
#define U8G_DOOR_CLOSED				(uint16_t)(1)


/*****************************************************/
extern uint16_t system_loop_counter;

/* ERR Define */
#define ERR_NONE         (0x00)
#define ERR_PASSED       (0x01)
#define ERR_FAILED       (0x02)  
#define ERR_PREPASSED    (0x03)
#define ERR_PREFAILED    (0x04)
#define ERR_INHIBIT		 (0x05)

/* 1/64 kph Speed Define */
#define 	U8_F64SPEED_0KPH25          (16)
#define 	U8_F64SPEED_0KPH375         (24)
#define 	U8_F64SPEED_1KPH		    (64)
#define 	U8_F64SPEED_1KPH5           (96)
#define 	U8_F64SPEED_1KPH75          (112)
#define     U8_F64SPEED_2KPH            (128) 
#define 	U8_F64SPEED_2KPH125         (136)  
#define		U8_F64SPEED_2KPH25	        (144)  
#define     U8_F64SPEED_2KPH3           (152)  
#define     U8_F64SPEED_3KPH            (192)  
#define     U16_F64SPEED_4KPH           (256)  
#define     U16_F64SPEED_5KPH           (320)  
#define     U16_F64SPEED_6KPH           (384)  
#define     U16_F64SPEED_7KPH           (448)  
#define     U16_F64SPEED_8KPH           (512)  
#define     U16_F64SPEED_10KPH          (640)  
#define     U16_F64SPEED_11KPH          (704)  
#define     U16_F64SPEED_12KPH          (768)  
#define     U16_F64SPEED_13KPH          (832)  
#define     U16_F64SPEED_14KPH          (896)  
#define     U16_F64SPEED_15KPH          (960)  
#define 	U16_F64SPEED_17KPH5 	    (1120) 
#define     U16_F64SPEED_20KPH          (1280) 
#define     U16_F64SPEED_24KPH          (1536) 
#define     U16_F64SPEED_25KPH          (1600) 
#define 	U16_F64SPEED_30KPH          (1920) 
#define 	U16_F64SPEED_35KPH          (2240) 
#define     U16_F64SPEED_40KPH          (2560) 
#define     U16_F64SPEED_43KPH          (2752) 
#define     U16_F64SPEED_50KPH          (3200) 
#define     U16_F64SPEED_60KPH          (3840)  
#define 	U16_F64SPEED_65KPH          (4160) 
#define     U16_F64SPEED_80KPH          (5120) 
#define 	U16_F64SPEED_90KPH          (5760) 
#define 	U16_F64SPEED_100KPH         (6400) 
#define     U16_F64SPEED_350KPH         (22400)
#define     U16_F64SPEED_512KPH         (32768)

/* Voltage */
#define    U16_CALCVOLT_0V1             (100)
#define    U16_CALCVOLT_0V12            (120)
#define    U16_CALCVOLT_0V187           (187)
#define    U16_CALCVOLT_0V2             (200)
#define    U16_CALCVOLT_0V3             (300)
#define    U16_CALCVOLT_0V375           (375)
#define    U16_CALCVOLT_0V4             (400)
#define    U16_CALCVOLT_0V5             (500)
#define    U16_CALCVOLT_0V6             (600)
#define    U16_CALCVOLT_0V7             (700)
#define    U16_CALCVOLT_0V8             (800)
#define    U16_CALCVOLT_0V9             (900)
#define    U16_CALCVOLT_1V0             (1000)
#define    U16_CALCVOLT_1V2             (1200)
#define    U16_CALCVOLT_1V25            (1250)
#define    U16_CALCVOLT_1V4             (1400)
#define    U16_CALCVOLT_1V5             (1500)
#define    U16_CALCVOLT_1V75            (1750)
#define    U16_CALCVOLT_1V9             (1900)
#define    U16_CALCVOLT_2V0             (2000)
#define    U16_CALCVOLT_2V1             (2100)
#define    U16_CALCVOLT_2V25            (2250)
#define    U16_CALCVOLT_2V36            (2360)
#define    U16_CALCVOLT_2V5             (2500)
#define    U16_CALCVOLT_2V64            (2640)
#define    U16_CALCVOLT_2V75            (2750)
#define    U16_CALCVOLT_2V9             (2900)
#define    U16_CALCVOLT_3V0             (3000)
#define    U16_CALCVOLT_3V25            (3250)
#define    U16_CALCVOLT_3V5             (3500)
#define    U16_CALCVOLT_3V75            (3750)
#define	   U16_CALCVOLT_3V9 			(3900)
#define    U16_CALCVOLT_4V0             (4000)
#define    U16_CALCVOLT_4V2             (4200)
#define    U16_CALCVOLT_4V25            (4250)
#define    U16_CALCVOLT_4V5             (4500)
#define    U16_CALCVOLT_4V7             (4700)
#define    U16_CALCVOLT_4V75            (4750)
#define    U16_CALCVOLT_4V8             (4800)
#define    U16_CALCVOLT_5V0             (5000)
#define    U16_CALCVOLT_5V25            (5250)
#define    U16_CALCVOLT_5V5             (5500)
#define    U16_CALCVOLT_5V75            (5750)
#define    U16_CALCVOLT_5V9             (5900)
#define    U16_CALCVOLT_6V0             (6000)
#define    U16_CALCVOLT_6V25            (6250)
#define    U16_CALCVOLT_6V4             (6400)
#define    U16_CALCVOLT_6V5             (6500)
#define    U16_CALCVOLT_6V75            (6750)
#define    U16_CALCVOLT_7V0             (7000)
#define    U16_CALCVOLT_7V2             (7200)
#define    U16_CALCVOLT_7V25            (7250)
#define    U16_CALCVOLT_7V5             (7500)
#define    U16_CALCVOLT_7V75            (7750)
#define    U16_CALCVOLT_7V8             (7800)
#define    U16_CALCVOLT_8V0             (8000)
#define    U16_CALCVOLT_8V2             (8200)
#define    U16_CALCVOLT_8V4             (8400)
#define    U16_CALCVOLT_8V25            (8250)
#define    U16_CALCVOLT_8V5             (8500)
#define    U16_CALCVOLT_8V6             (8600)
#define    U16_CALCVOLT_8V75            (8750)
#define    U16_CALCVOLT_8V8             (8800)
#define    U16_CALCVOLT_9V0             (9000)
#define    U16_CALCVOLT_9V2             (9200)
#define    U16_CALCVOLT_9V25            (9250)
#define    U16_CALCVOLT_9V4             (9400)
#define    U16_CALCVOLT_9V5             (9500)
#define    U16_CALCVOLT_9V6             (9600)
#define    U16_CALCVOLT_9V75            (9750)
#define    U16_CALCVOLT_10V0            (10000)
#define    U16_CALCVOLT_10V25           (10250)
#define    U16_CALCVOLT_10V5            (10500)
#define    U16_CALCVOLT_10V75           (10750)
#define    U16_CALCVOLT_11V0            (11000)
#define    U16_CALCVOLT_11V25           (11250)
#define    U16_CALCVOLT_11V5            (11500)
#define    U16_CALCVOLT_11V75           (11750)
#define    U16_CALCVOLT_12V0            (12000)
#define    U16_CALCVOLT_12V25           (12250)
#define    U16_CALCVOLT_12V5            (12500)
#define    U16_CALCVOLT_12V75           (12750)
#define    U16_CALCVOLT_13V0            (13000)
#define    U16_CALCVOLT_13V25           (13250)  
#define    U16_CALCVOLT_13V5            (13500)
#define    U16_CALCVOLT_13V75           (13750)
#define    U16_CALCVOLT_14V0            (14000)
#define    U16_CALCVOLT_14V25           (14250)
#define    U16_CALCVOLT_14V5            (14500)
#define    U16_CALCVOLT_14V75           (14750)
#define    U16_CALCVOLT_15V0            (15000)
#define    U16_CALCVOLT_15V25           (15250)
#define    U16_CALCVOLT_15V5            (15500)
#define    U16_CALCVOLT_15V75           (15750)
#define    U16_CALCVOLT_16V0            (16000)
#define    U16_CALCVOLT_16V25           (16250)
#define    U16_CALCVOLT_16V5            (16500)
#define    U16_CALCVOLT_16V7            (16700)
#define    U16_CALCVOLT_16V75           (16750)
#define    U16_CALCVOLT_17V0            (17000)
#define    U16_CALCVOLT_17V25           (17250)
#define    U16_CALCVOLT_17V5            (17500)
#define    U16_CALCVOLT_17V75           (17750)
#define    U16_CALCVOLT_18V0            (18000)
#define    U16_CALCVOLT_18V25           (18250)
#define    U16_CALCVOLT_18V5            (18500)
#define    U16_CALCVOLT_18V75           (18750)
#define    U16_CALCVOLT_19V0            (19000)
#define    U16_CALCVOLT_19V25           (19250)
#define    U16_CALCVOLT_19V5            (19500)
#define    U16_CALCVOLT_19V75           (19750)
#define    U16_CALCVOLT_20V0            (20000)

/* IO Control */
#define PORT_HIGH ((0<<16) | (1<<0)) /* OMR register set */
#define PORT_LOW ((1<<16)  | (0<<0)) /* OMR register set */

#endif

