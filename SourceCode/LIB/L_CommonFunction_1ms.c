/*******************************************************************************
* Project Name:     Mando AHB
* File Name:        LCallCommonFunction_1ms.C
* Description:      Common Functions using for Control Logic
                    (Low Pass Filter, Interpolation, Select Min/Max, Limitation)
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  9807         KGY           Initial Release
********************************************************************************/


/* Includes ********************************************************************/
#include "L_CommonFunction_1ms.h"


/* Local Definition ************************************************************/

#define     S16_MAX_LIB               	32767
#define     S16_MIN_LIB                -32767
#define     U16_MAX_LIB                 65535


/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/

/* Implementation **************************************************************/

int16_t L_s16Lpf1Int1ms( int16_t s16Xinput, int16_t s16Xold, uint8_t u8Xgain )
{
	int16_t ls16LpfTempW4 = 0;

	if(u8Xgain>128)
	{
		u8Xgain = 128;
	}

    ls16LpfTempW4 = (int16_t)(((((int32_t)s16Xinput)*u8Xgain)
                   +(((int32_t)s16Xold)*(128-u8Xgain)))/128);

    return (ls16LpfTempW4);
}


/*******************************************************************************
* FUNCTION NAME:        L_u16IInter2Point
* CALLED BY:            LAIDB_s16CalcStorkeByPress()
* Preconditions:        none
* PARAMETER:            u16Xinput, u16X1, u16Y1, u16X2, u16Y2
* RETURN VALUE:         u16CalcResult
* Description:          2Point Interpolation Function for Extract Stroke
*                       with Pressure
*******************************************************************************/

int16_t L_s16IInter2Point1ms( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2 )
{
	int16_t ls16InterpolationTempW0 = 0;
	int16_t ls16InterpolationTempW9 = 0;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 )
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 =  s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    }
    else
    {
        ls16InterpolationTempW9 = s16Y2;
    }
    return ls16InterpolationTempW9;
}

int16_t L_s16IInter15Point1ms( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12, int16_t s16X13, int16_t s16Y13, int16_t s16X14, int16_t s16Y14, int16_t s16X15, int16_t s16Y15)
{
	int16_t ls16InterpolationTempW0 = 0;
	int16_t ls16InterpolationTempW9 = 0;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 )
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X3 )
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X4 )
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X5 )
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X6 )
    {
        ls16InterpolationTempW0=(s16X6-s16X5);
        ls16InterpolationTempW9 = s16Y5 + (int16_t)(((int32_t)(s16Y6-s16Y5)*(s16Xinput-s16X5))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X7 )
    {
        ls16InterpolationTempW0=(s16X7-s16X6);
        ls16InterpolationTempW9 = s16Y6 + (int16_t)(((int32_t)(s16Y7-s16Y6)*(s16Xinput-s16X6))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X8 )
    {
        ls16InterpolationTempW0=(s16X8-s16X7);
        ls16InterpolationTempW9 = s16Y7 + (int16_t)(((int32_t)(s16Y8-s16Y7)*(s16Xinput-s16X7))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X9 )
    {
        ls16InterpolationTempW0=(s16X9-s16X8);
        ls16InterpolationTempW9 = s16Y8 + (int16_t)(((int32_t)(s16Y9-s16Y8)*(s16Xinput-s16X8))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X10 )
    {
        ls16InterpolationTempW0=(s16X10-s16X9);
        ls16InterpolationTempW9 = s16Y9 + (int16_t)(((int32_t)(s16Y10-s16Y9)*(s16Xinput-s16X9))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X11 )
    {
        ls16InterpolationTempW0=(s16X11-s16X10);
        ls16InterpolationTempW9 = s16Y10 + (int16_t)(((int32_t)(s16Y11-s16Y10)*(s16Xinput-s16X10))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X12 )
    {
        ls16InterpolationTempW0=(s16X12-s16X11);
        ls16InterpolationTempW9 = s16Y11 + (int16_t)(((int32_t)(s16Y12-s16Y11)*(s16Xinput-s16X11))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X13 )
    {
        ls16InterpolationTempW0=(s16X13-s16X12);
        ls16InterpolationTempW9 = s16Y12 + (int16_t)(((int32_t)(s16Y13-s16Y12)*(s16Xinput-s16X12))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X14 )
    {
        ls16InterpolationTempW0=(s16X14-s16X13);
        ls16InterpolationTempW9 = s16Y13 + (int16_t)(((int32_t)(s16Y14-s16Y13)*(s16Xinput-s16X13))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X15 )
    {
        ls16InterpolationTempW0=(s16X15-s16X14);
        ls16InterpolationTempW9 = s16Y14 + (int16_t)(((int32_t)(s16Y15-s16Y14)*(s16Xinput-s16X14))/ls16InterpolationTempW0);
    }
    else
    {
        ls16InterpolationTempW9 = s16Y15;
    }

    return ls16InterpolationTempW9;
}
int16_t L_s16LimitMinMax1ms( int16_t s16Xinput, int16_t s16Xmin, int16_t s16Xmax )
{
	int16_t ls16LimitedValue = 0;

	if(s16Xinput < s16Xmin)
	{
		ls16LimitedValue = s16Xmin;
	}
	else if(s16Xinput > s16Xmax)
	{
		ls16LimitedValue = s16Xmax;
	}
	else
	{
		ls16LimitedValue = s16Xinput;
	}

	return ls16LimitedValue;
}

int32_t L_s32LimitMinMax1ms( int32_t s32Xinput, int32_t s32Xmin, int32_t s32Xmax )
{
	int32_t ls32LimitedValue = 0;

    if(s32Xinput < s32Xmin)
    {
        ls32LimitedValue = s32Xmin;
    }
    else if(s32Xinput > s32Xmax)
    {
        ls32LimitedValue = s32Xmax;
    }
    else
    {
        ls32LimitedValue = s32Xinput;
    }

    return ls32LimitedValue;
}


int32_t L_s32IInter4Point1ms( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4 )
{
	int32_t s32InterpolationTemp0 = 0;
	int32_t s32InterpolationTemp1 = 0;
    if ( s32Xinput <= s32X1 )
    {
        s32InterpolationTemp1 = s32Y1;
    }
    else if ( s32Xinput <= s32X2 )
    {
        s32InterpolationTemp0=(s32X2-s32X1);
        s32InterpolationTemp1 = s32Y1 + (((s32Y2-s32Y1)*(s32Xinput-s32X1))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X3 )
    {
        s32InterpolationTemp0=(s32X3-s32X2);
        s32InterpolationTemp1 = s32Y2 + (((s32Y3-s32Y2)*(s32Xinput-s32X2))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X4 )
    {
        s32InterpolationTemp0=(s32X4-s32X3);
        s32InterpolationTemp1 = s32Y3 + (((s32Y4-s32Y3)*(s32Xinput-s32X3))/s32InterpolationTemp0);
    }
    else
    {
        s32InterpolationTemp1 = s32Y4;
    }

    return s32InterpolationTemp1;
}

int32_t L_s32Abs1ms( int32_t s32Xinput )
{
	int32_t ls32LimitedValue;
	if(s32Xinput < 0)
	{
		ls32LimitedValue = -s32Xinput;
	}
	else
	{
		ls32LimitedValue = s32Xinput;
	}

	return ls32LimitedValue;
}
