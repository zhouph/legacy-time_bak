#ifndef __LCALLCOMMONFUNCTION_H__
#define __LCALLCOMMONFUNCTION_H__

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define LCALLCOMMONFUNCTION_PATH_VIL(_FILENAME_) STRINGIFY(../ASW/include/_FILENAME_)

/* Includes ******************************************************************/
#include LCALLCOMMONFUNCTION_PATH_VIL(Mando_Std_Types.h)
//#include LCALLCOMMONFUNCTION_PATH_VIL(Asw_Parameter_Def.h)

/*Global MACRO CONSTANT Definition *********************************************/
#define U8_LPF_1HZ_128G         4
#define U8_LPF_1_5HZ_128G         5
#define U8_LPF_3HZ_128G         11
#define U8_LPF_5HZ_128G         17
#define U8_LPF_7HZ_128G         23
#define U8_LPF_10HZ_128G        31
#define U8_LPF_15HZ_128G        41
#define U8_LPF_20HZ_128G        49

#define S16_LPF_1HZ_1024G       31
#define S16_LPF_3HZ_1024G       88
#define S16_LPF_5HZ_1024G       139
#define S16_LPF_7HZ_1024G       185
#define S16_LPF_10HZ_1024G      245
#define S16_LPF_15HZ_1024G      328
#define S16_LPF_20HZ_1024G      395

/*Global MACRO FUNCTION Definition *********************************************/


/*Global Type Declaration ******************************************************/


/*Global Extern Variable Declaration *******************************************/


/*Global Extern Functions Declaration ******************************************/
extern int16_t  L_s16Lpf1Int( int16_t s16Xinput, int16_t s16Xold, uint8_t u8Xgain );
extern int16_t  L_s16Lpf1Int1024( int16_t s16Xinput, int16_t s16Xold, int16_t s16Xgain );
extern uint16_t L_u16Lpf1Int( uint16_t u16Xinput, uint16_t u16Xold, uint8_t u8Xgain );
extern uint16_t L_u16IInter2Point( uint16_t u16Xinput, uint16_t u16X1, uint16_t u16Y1, uint16_t u16X2, uint16_t u16Y2 );
extern int16_t  L_s16IInter2Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2 );
extern int16_t  L_s16IInter3Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3 );
extern int16_t  L_s16IInter4Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4 );
extern int16_t  L_s16IInter5Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5 );
extern int16_t  L_s16IInter6Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6 );
extern int16_t  L_s16IInter9Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9 );
extern int16_t  L_s16IInter12Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12);
extern int16_t  L_s16IInter15Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12, int16_t s16X13, int16_t s16Y13, int16_t s16X14, int16_t s16Y14, int16_t s16X15, int16_t s16Y15);
extern int16_t  L_s16LimitMinMax( int16_t s16Xinput, int16_t s16Xmin, int16_t s16Xmax );
extern int32_t  L_s32LimitMinMax( int32_t s32Xinput, int32_t s32Xmin, int32_t s32Xmax );
extern uint16_t L_u16LimitMinMax( uint16_t u16Xinput, uint16_t u16Xmin, uint16_t u16Xmax );
extern int16_t  L_s16Abs( int16_t s16Xinput );
extern int16_t  L_s16GetParFromRef(int16_t s16Input, int16_t s16Ref1, int16_t s16Par1, int16_t s16Ref2, int16_t s16Par2, int16_t s16Ref3, int16_t s16Par3, int16_t s16Ref4, int16_t s16Par4, int16_t s16Ref5, int16_t s16Par5);
extern int16_t  L_s16LimitDiff(int16_t s16XInput, int16_t s16Xold, int16_t s16IncMax, int16_t s16DecMax);
extern uint16_t L_u16LimitDiff(uint16_t u16XInput, uint16_t u16Xold, uint16_t u16IncMax, uint16_t u16DecMax);
extern uint16_t L_u16FitSQRT(uint16_t u16XInput);
extern int16_t  L_s16LimSignedLongToSignedInt(int32_t s32TempInputData);
extern int32_t  L_s32LimitDiff(int32_t s32XInput, int32_t s32Xold, int32_t s32IncMax, int32_t s32DecMax);
extern int32_t L_s32IInter4Point( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4 );
extern int32_t L_s32IInter6Point( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4, int32_t s32X5, int32_t s32Y5, int32_t s32X6, int32_t s32Y6 );
#endif


