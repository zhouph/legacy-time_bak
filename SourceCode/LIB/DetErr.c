
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Inclusion of Platform_Types.h and Compiler.h */
#include "Std_Types.h"
#include "Mcal.h"
//#include "Test_Print.h"
#include "DetErr.h"

/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/** \brief Det has not yet been inited */
#define DETERR_STATE_UNINIT      1U
/** \brief Det is initialized but was not yet been started */
#define DETERR_STATE_NOT_STARTED 2U
/** \brief Det is idle and ready to send an error if neccessary */
#define DETERR_STATE_IDLE        3U
/** \brief Det is sending an error and is waiting for the confirmation*/
#define DETERR_STATE_SENDING     4U

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
typedef uint8 DetErr_StateType;
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define DETERR_BUFFERSIZE 50
/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
uint16 DetErr_WriteIndex;
uint16 DetErr_UsedSlots;
uint16 DetErr_Lost;
DetErr_StateType   DetErr_State = DETERR_STATE_UNINIT;
DetErr_BufferType DetErr_Buffer[DETERR_BUFFERSIZE];
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
void DetErr_Init(void)
{
   /* state change: UNINIT -> NOT_STARTED */
   DetErr_State = DETERR_STATE_NOT_STARTED;

   /* initialize all global variables */

   for (DetErr_WriteIndex = 0U;
        DetErr_WriteIndex < DETERR_BUFFERSIZE;
        ++DetErr_WriteIndex)
   {
      DetErr_Buffer[DetErr_WriteIndex].ModuleId   = 0U;
      DetErr_Buffer[DetErr_WriteIndex].InstanceId = 0U;
      DetErr_Buffer[DetErr_WriteIndex].ApiId      = 0U;
      DetErr_Buffer[DetErr_WriteIndex].ErrorId    = 0U;
   }

   DetErr_WriteIndex = 0U;
   DetErr_Lost  = 0U;
   DetErr_UsedSlots  = 0U;
}

/* Test Stub for Det_ReportError */ 
void DetErr_Report(uint16 ModuleId,uint8 InstanceId,uint8 ApiId,uint8 ErrorId) 
{
//  print_f("\n DET OCCURED \n ");
//  print_f("ModuleId:%d, InstanceId: %d, ApiId:%d, ErrorId:%d"
//           ,ModuleId, InstanceId, ApiId, ErrorId);
	   if (DetErr_State != DETERR_STATE_UNINIT)
	   {
	      /* call the notification functions which are configured */
	      //DET_NOTIFICATIONS(ModuleId, InstanceId, ApiId, ErrorId);



	      /* lock access to DET global variables */
	      //SchM_Enter_Det_SCHM_DET_EXCLUSIVE_AREA_0();

	      /* queue the reported error */


	      /* Keep first errors reported to the Det */
	      if (DetErr_UsedSlots < DETERR_BUFFERSIZE)
	      {
	         DetErr_Buffer[DetErr_WriteIndex].ModuleId   = ModuleId;
	         DetErr_Buffer[DetErr_WriteIndex].InstanceId = InstanceId;
	         DetErr_Buffer[DetErr_WriteIndex].ApiId      = ApiId;
	         DetErr_Buffer[DetErr_WriteIndex].ErrorId    = ErrorId;

	         ++DetErr_WriteIndex; /* increase write index, overflow does not matter */
	         ++DetErr_UsedSlots;  /* increment used slot counter */
	      }
	      else /* Det_UsedSlots >= DET_BUFFERSIZE */
	      {
	         if (DetErr_Lost < ((uint16)0xFFFFU))
	         {
	            ++DetErr_Lost;       /* count lost error reports */
	         }
	      }



	      /* unlock access to DET global variables */
	      //SchM_Exit_Det_SCHM_DET_EXCLUSIVE_AREA_0();


	   }
	   /* else Det_State == DET_STATE_UNINIT: ignore the function call in the
	    * uninitialized state. Other modules would report such an development
	    * error to the Det but the Det cannot report this to itself. */


}
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/


