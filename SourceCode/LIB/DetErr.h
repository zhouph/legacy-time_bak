

#ifndef _DETERR_H
#define _DETERR_H

/******************************************************************************
**                      Include Section                                      **
******************************************************************************/

/* This header file includes all the standard data types, platform dependent
   header file and common return types */
#include "Std_Types.h"

/* This header file includes all the configurable parameters  */
/*#include "Det_Cfg.h"*/

/*This header file includes all the OS resources needed to maintain reentrancy*/
/*#include "os.h"*/

/******************************************************************************
**                      Macro Definitions                                    **
******************************************************************************/



/* Declaring the module id for DET */
#define DETERR_MODULE_ID                              1023U

/* File version Information to perform the version check */

                                                             

/******************************************************************************
**                      Global Symbols                                       **
******************************************************************************/

/*Defining the structure to store the parameters of Det Report Error function*/

typedef struct
{  /* It will store the ModuleId of the reporting module */
   uint16 ModuleId;

   /* It will store the index based InstanceId of the reporting module */
   uint8 InstanceId;

   /* It will store the ApiId of the reporting function */
   uint8 ApiId;

   /* It will store the ErrorId of the reporting error */
   uint8 ErrorId;

}DetErr_BufferType ;


/*******************************************************************************
**                      Global Function Prototypes                            **
*******************************************************************************/
#define DETERR_NOTIFICATIONS(a,b,c,d)              \
          do                                    \
          {                                     \
            DetErr_ErrorIndication(a,b,c,d); \
          } while (0)
/* This function is used to clear the logged errors and the reported
   error count */
extern void DetErr_Init(void);

/* This function is used to log the reported errors and count the reported
   errors */
extern void DetErr_Report(uint16 ModuleId,uint8 InstanceId,uint8 ApiId,uint8 ErrorId);

/* This function is used to return the oldest error logged */
extern uint8 DetErr_Read (DetErr_BufferType* Det_Error );

/* This function is the service for starting after the Det_Init() is called */
extern void DetErr_Start(void);

#endif /* End of #ifndef _DETERR_H */

/**************************END OF DET.H******************************************/





