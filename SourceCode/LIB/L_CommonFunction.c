/*******************************************************************************
* Project Name:     Mando AHB
* File Name:        LCallCommonFunction.C
* Description:      Common Functions using for Control Logic
                    (Low Pass Filter, Interpolation, Select Min/Max, Limitation)
* Logic version:    
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  9807         KGY           Initial Release
********************************************************************************/


/* Includes ********************************************************************/
#include "L_CommonFunction.h"


/* Local Definition ************************************************************/

#define     S16_MAX_LIB               32767
#define     S16_MIN_LIB                -32767
#define     U16_MAX_LIB                 65535


/* Variables Definition ********************************************************/


/* Local Function prototype ****************************************************/


/* Implementation **************************************************************/

int16_t L_s16Lpf1Int( int16_t s16Xinput, int16_t s16Xold, uint8_t u8Xgain )
{
	int16_t  ls16LpfTempW4;

	if(u8Xgain>128)
	{
		u8Xgain = 128;
	}
    
    ls16LpfTempW4 = (int16_t)(((((int32_t)s16Xinput)*u8Xgain)
                   +(((int32_t)s16Xold)*(128-u8Xgain)))/128);
   
    return (ls16LpfTempW4);
}

int16_t L_s16Lpf1Int1024( int16_t s16Xinput, int16_t s16Xold, int16_t s16Xgain )
{
	int16_t  ls16LpfTempW4;

	if(s16Xgain>1024)
	{
		s16Xgain = 1024;
	}

    ls16LpfTempW4 = (int16_t)(((((int32_t)s16Xinput)*s16Xgain)
                   +(((int32_t)s16Xold)*(1024-s16Xgain)))/1024);
                   
    return (ls16LpfTempW4);                   
}

uint16_t L_u16Lpf1Int( uint16_t u16Xinput, uint16_t u16Xold, uint8_t u8Xgain )
{
	uint16_t lu16LpfTempUW4;

	if(u8Xgain>128)
	{
		u8Xgain = 128;
	}

    lu16LpfTempUW4 = (uint16_t)(((((uint32_t)u16Xinput)*(uint16_t)u8Xgain)
                   +(((uint32_t)u16Xold)*(uint16_t)(128-u8Xgain)))/128);

    return lu16LpfTempUW4;
}

/*******************************************************************************
* FUNCTION NAME:        L_u16IInter2Point
* CALLED BY:            LAIDB_s16CalcStorkeByPress()
* Preconditions:        none
* PARAMETER:            u16Xinput, u16X1, u16Y1, u16X2, u16Y2
* RETURN VALUE:         u16CalcResult
* Description:          2Point Interpolation Function for Extract Stroke
*                       with Pressure
*******************************************************************************/
uint16_t L_u16IInter2Point( uint16_t u16Xinput, uint16_t u16X1, uint16_t u16Y1, uint16_t u16X2, uint16_t u16Y2 )
{
    uint16_t u16CalcResult = 0;
    uint16_t u16DeltaX = 0;
    uint32_t u32DeltaY = 0;

    if ( u16Xinput <= u16X1 )
    {
        u16CalcResult = u16Y1;
    }
    else if ( u16Xinput <= u16X2 )
    {
        u16DeltaX = (u16X2-u16X1);
        u32DeltaY = ((uint32_t)(u16Y2-u16Y1) * (u16Xinput-u16X1)) / u16DeltaX;

        if(u32DeltaY > U16_MAX_LIB)
        {
            u32DeltaY = U16_MAX_LIB;
            u16CalcResult = (uint16_t)u32DeltaY;
        }
        else
        {
            u16CalcResult =  u16Y1 + (uint16_t)(u32DeltaY);
        }
    }
    else
    {
        u16CalcResult = u16Y2;
    }

    return u16CalcResult;
}

int16_t L_s16IInter2Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 =  s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);                     
    } 
    else
    {
        ls16InterpolationTempW9 = s16Y2;
    }
    return ls16InterpolationTempW9;      
}     

int16_t L_s16IInter3Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else
    {
        ls16InterpolationTempW9 = s16Y3;
    }
            
    return ls16InterpolationTempW9;      
}  

int16_t L_s16IInter4Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else 
    {
        ls16InterpolationTempW9 = s16Y4;
    }
            
    return ls16InterpolationTempW9;      
}  

int16_t L_s16IInter5Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X5 ) 
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    } 
    else
    {
        ls16InterpolationTempW9 = s16Y5;
    }
            
    return ls16InterpolationTempW9;      
}

int16_t L_s16IInter6Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X5 ) 
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X6 ) 
    {
        ls16InterpolationTempW0=(s16X6-s16X5);
        ls16InterpolationTempW9 = s16Y5 + (int16_t)(((int32_t)(s16Y6-s16Y5)*(s16Xinput-s16X5))/ls16InterpolationTempW0);
    } 
    else
    {
        ls16InterpolationTempW9 = s16Y6;
    }
            
    return ls16InterpolationTempW9;      
}

int16_t L_s16IInter9Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9 )
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X5 ) 
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X6 ) 
    {
        ls16InterpolationTempW0=(s16X6-s16X5);
        ls16InterpolationTempW9 = s16Y5 + (int16_t)(((int32_t)(s16Y6-s16Y5)*(s16Xinput-s16X5))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X7 ) 
    {
        ls16InterpolationTempW0=(s16X7-s16X6);
        ls16InterpolationTempW9 = s16Y6 + (int16_t)(((int32_t)(s16Y7-s16Y6)*(s16Xinput-s16X6))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X8 ) 
    {
        ls16InterpolationTempW0=(s16X8-s16X7);
        ls16InterpolationTempW9 = s16Y7 + (int16_t)(((int32_t)(s16Y8-s16Y7)*(s16Xinput-s16X7))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X9 ) 
    {
        ls16InterpolationTempW0=(s16X9-s16X8);
        ls16InterpolationTempW9 = s16Y8 + (int16_t)(((int32_t)(s16Y9-s16Y8)*(s16Xinput-s16X8))/ls16InterpolationTempW0);
    }
    else
    {
        ls16InterpolationTempW9 = s16Y9;
    }
            
    return ls16InterpolationTempW9;      
}

int16_t L_s16IInter12Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12)
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X5 ) 
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X6 ) 
    {
        ls16InterpolationTempW0=(s16X6-s16X5);
        ls16InterpolationTempW9 = s16Y5 + (int16_t)(((int32_t)(s16Y6-s16Y5)*(s16Xinput-s16X5))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X7 ) 
    {
        ls16InterpolationTempW0=(s16X7-s16X6);
        ls16InterpolationTempW9 = s16Y6 + (int16_t)(((int32_t)(s16Y7-s16Y6)*(s16Xinput-s16X6))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X8 ) 
    {
        ls16InterpolationTempW0=(s16X8-s16X7);
        ls16InterpolationTempW9 = s16Y7 + (int16_t)(((int32_t)(s16Y8-s16Y7)*(s16Xinput-s16X7))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X9 ) 
    {
        ls16InterpolationTempW0=(s16X9-s16X8);
        ls16InterpolationTempW9 = s16Y8 + (int16_t)(((int32_t)(s16Y9-s16Y8)*(s16Xinput-s16X8))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X10 ) 
    {
        ls16InterpolationTempW0=(s16X10-s16X9);
        ls16InterpolationTempW9 = s16Y9 + (int16_t)(((int32_t)(s16Y10-s16Y9)*(s16Xinput-s16X9))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X11 ) 
    {
        ls16InterpolationTempW0=(s16X11-s16X10);
        ls16InterpolationTempW9 = s16Y10 + (int16_t)(((int32_t)(s16Y11-s16Y10)*(s16Xinput-s16X10))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X12 ) 
    {
        ls16InterpolationTempW0=(s16X12-s16X11);
        ls16InterpolationTempW9 = s16Y11 + (int16_t)(((int32_t)(s16Y12-s16Y11)*(s16Xinput-s16X11))/ls16InterpolationTempW0);
    }
    else
    {
        ls16InterpolationTempW9 = s16Y12;
    }
            
    return ls16InterpolationTempW9;      
}

int16_t L_s16IInter15Point( int16_t s16Xinput, int16_t s16X1, int16_t s16Y1, int16_t s16X2, int16_t s16Y2, int16_t s16X3, int16_t s16Y3, int16_t s16X4, int16_t s16Y4, int16_t s16X5, int16_t s16Y5, int16_t s16X6, int16_t s16Y6, int16_t s16X7, int16_t s16Y7, int16_t s16X8, int16_t s16Y8, int16_t s16X9, int16_t s16Y9, int16_t s16X10, int16_t s16Y10, int16_t s16X11, int16_t s16Y11, int16_t s16X12, int16_t s16Y12, int16_t s16X13, int16_t s16Y13, int16_t s16X14, int16_t s16Y14, int16_t s16X15, int16_t s16Y15)
{
	int16_t  ls16InterpolationTempW0;
	int16_t  ls16InterpolationTempW9;

    if ( s16Xinput <= s16X1 )
    {
        ls16InterpolationTempW9 = s16Y1;
    }
    else if ( s16Xinput <= s16X2 ) 
    {
        ls16InterpolationTempW0=(s16X2-s16X1);
        ls16InterpolationTempW9 = s16Y1 + (int16_t)(((int32_t)(s16Y2-s16Y1)*(s16Xinput-s16X1))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X3 ) 
    {
        ls16InterpolationTempW0=(s16X3-s16X2);
        ls16InterpolationTempW9 = s16Y2 + (int16_t)(((int32_t)(s16Y3-s16Y2)*(s16Xinput-s16X2))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X4 ) 
    {
        ls16InterpolationTempW0=(s16X4-s16X3);
        ls16InterpolationTempW9 = s16Y3 + (int16_t)(((int32_t)(s16Y4-s16Y3)*(s16Xinput-s16X3))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X5 ) 
    {
        ls16InterpolationTempW0=(s16X5-s16X4);
        ls16InterpolationTempW9 = s16Y4 + (int16_t)(((int32_t)(s16Y5-s16Y4)*(s16Xinput-s16X4))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X6 ) 
    {
        ls16InterpolationTempW0=(s16X6-s16X5);
        ls16InterpolationTempW9 = s16Y5 + (int16_t)(((int32_t)(s16Y6-s16Y5)*(s16Xinput-s16X5))/ls16InterpolationTempW0);
    } 
    else if ( s16Xinput <= s16X7 ) 
    {
        ls16InterpolationTempW0=(s16X7-s16X6);
        ls16InterpolationTempW9 = s16Y6 + (int16_t)(((int32_t)(s16Y7-s16Y6)*(s16Xinput-s16X6))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X8 ) 
    {
        ls16InterpolationTempW0=(s16X8-s16X7);
        ls16InterpolationTempW9 = s16Y7 + (int16_t)(((int32_t)(s16Y8-s16Y7)*(s16Xinput-s16X7))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X9 ) 
    {
        ls16InterpolationTempW0=(s16X9-s16X8);
        ls16InterpolationTempW9 = s16Y8 + (int16_t)(((int32_t)(s16Y9-s16Y8)*(s16Xinput-s16X8))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X10 ) 
    {
        ls16InterpolationTempW0=(s16X10-s16X9);
        ls16InterpolationTempW9 = s16Y9 + (int16_t)(((int32_t)(s16Y10-s16Y9)*(s16Xinput-s16X9))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X11 ) 
    {
        ls16InterpolationTempW0=(s16X11-s16X10);
        ls16InterpolationTempW9 = s16Y10 + (int16_t)(((int32_t)(s16Y11-s16Y10)*(s16Xinput-s16X10))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X12 ) 
    {
        ls16InterpolationTempW0=(s16X12-s16X11);
        ls16InterpolationTempW9 = s16Y11 + (int16_t)(((int32_t)(s16Y12-s16Y11)*(s16Xinput-s16X11))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X13 ) 
    {
        ls16InterpolationTempW0=(s16X13-s16X12);
        ls16InterpolationTempW9 = s16Y12 + (int16_t)(((int32_t)(s16Y13-s16Y12)*(s16Xinput-s16X12))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X14 ) 
    {
        ls16InterpolationTempW0=(s16X14-s16X13);
        ls16InterpolationTempW9 = s16Y13 + (int16_t)(((int32_t)(s16Y14-s16Y13)*(s16Xinput-s16X13))/ls16InterpolationTempW0);
    }
    else if ( s16Xinput <= s16X15 ) 
    {
        ls16InterpolationTempW0=(s16X15-s16X14);
        ls16InterpolationTempW9 = s16Y14 + (int16_t)(((int32_t)(s16Y15-s16Y14)*(s16Xinput-s16X14))/ls16InterpolationTempW0);
    }
    else
    {
        ls16InterpolationTempW9 = s16Y15;
    }
            
    return ls16InterpolationTempW9;      
}
int16_t L_s16LimitMinMax( int16_t s16Xinput, int16_t s16Xmin, int16_t s16Xmax )
{
	int16_t  ls16LimitedValue;

	if(s16Xinput < s16Xmin)
	{
		ls16LimitedValue = s16Xmin;
	}
	else if(s16Xinput > s16Xmax)
	{
		ls16LimitedValue = s16Xmax;
	}
	else
	{
		ls16LimitedValue = s16Xinput;
	}
	
	return ls16LimitedValue;
}

int32_t L_s32LimitMinMax( int32_t s32Xinput, int32_t s32Xmin, int32_t s32Xmax )
{
	int32_t  ls32LimitedValue;

    if(s32Xinput < s32Xmin)
    {
        ls32LimitedValue = s32Xmin;
    }
    else if(s32Xinput > s32Xmax)
    {
        ls32LimitedValue = s32Xmax;
    }
    else
    {
        ls32LimitedValue = s32Xinput;
    }

    return ls32LimitedValue;
}

uint16_t L_u16LimitMinMax( uint16_t u16Xinput, uint16_t u16Xmin, uint16_t u16Xmax )
{
	uint16_t lu16LimitedValue;

	if(u16Xinput < u16Xmin)
	{
		lu16LimitedValue = u16Xmin;
	}
	else if(u16Xinput > u16Xmax)
	{
		lu16LimitedValue = u16Xmax;
	}
	else
	{
		lu16LimitedValue = u16Xinput;
	}
	
	return lu16LimitedValue;
}

int16_t L_s16Abs( int16_t s16Xinput )
{
	int16_t  ls16LimitedValue;

	if(s16Xinput < 0)
	{
		ls16LimitedValue = -s16Xinput;
	}
	else
	{
		ls16LimitedValue = s16Xinput;
	}
	
	return ls16LimitedValue;
}

int16_t L_s16GetParFromRef(int16_t s16Input, int16_t s16Ref1, int16_t s16Par1, int16_t s16Ref2, int16_t s16Par2, int16_t s16Ref3, int16_t s16Par3, int16_t s16Ref4, int16_t s16Par4, int16_t s16Ref5, int16_t s16Par5)
{
    int16_t s16TempOutput;
    
    if(s16Input > s16Ref5)
    {
        s16TempOutput = s16Par5;
    }
    else if(s16Input > s16Ref4)
    {
        s16TempOutput = s16Par4;
    }
    else if(s16Input > s16Ref3)
    {
        s16TempOutput = s16Par3;
    }
    else if(s16Input > s16Ref2)
    {
        s16TempOutput = s16Par2;
    }
    else if(s16Input > s16Ref1)
    {
        s16TempOutput = s16Par1;
    }
    else
    {
        s16TempOutput = s16Par1;
    }

    return s16TempOutput;
}

int16_t L_s16LimitDiff(int16_t s16XInput, int16_t s16Xold, int16_t s16IncMax, int16_t s16DecMax)
{
	int32_t s32DiffRaw = (int32_t)s16XInput - (int32_t)s16Xold;
	int16_t  ls16LimitedValue;
	
	if(s16XInput >= s16Xold)
	{
		if(s32DiffRaw > (int32_t)s16IncMax)
		{
			ls16LimitedValue = s16Xold + s16IncMax;
		}
		else
		{
			ls16LimitedValue = s16XInput;
		}
	}
	else
	{
		if(s32DiffRaw < -(int32_t)s16DecMax)
		{
			ls16LimitedValue = s16Xold - s16DecMax;
		}
		else
		{
			ls16LimitedValue = s16XInput;
		}
	}

	return ls16LimitedValue;
}

uint16_t L_u16LimitDiff(uint16_t u16XInput, uint16_t u16Xold, uint16_t u16IncMax, uint16_t u16DecMax)
{
	uint16_t lu16LimitedValue;
	int32_t s32DiffRaw = (int32_t)u16XInput - (int32_t)u16Xold;
	
	if(u16XInput >= u16Xold)
	{
		if(s32DiffRaw > (int32_t)u16IncMax)
		{
			lu16LimitedValue = u16Xold + u16IncMax;
		}
		else
		{
			lu16LimitedValue = u16XInput;
		}
	}
	else
	{
		if(s32DiffRaw < -(int32_t)u16DecMax)
		{
			lu16LimitedValue = u16Xold - u16DecMax;
		}
		else
		{
			lu16LimitedValue = u16XInput;
		}
	}

	return lu16LimitedValue;
}

uint16_t L_u16FitSQRT(uint16_t u16XInput)
{
	uint16_t u16Gain, u16Offset;
	static uint16_t lu16SqrtValue = 0;
	
    if(u16XInput>=3000)
    {
    	u16XInput=3000;
    }
    else { }

    if(u16XInput<20)
    {
        u16Offset = 5; u16Gain = 1925;
    }
    else if(u16XInput<80)
    {
        u16Offset = 45; u16Gain = 733;
    }
    else if(u16XInput<200)
    {
        u16Offset = 133; u16Gain = 430;
    }
    else if(u16XInput<400)
    {
        u16Offset = 291; u16Gain = 292;
    }
    else if(u16XInput<800)
    {
        u16Offset = 575; u16Gain = 207;
    }
    else if(u16XInput<1600)
    {
        u16Offset = 1145; u16Gain = 147;
    }
    else
    {
        u16Offset = 2249; u16Gain = 105;
    }

    lu16SqrtValue = (uint16_t)(((uint32_t)(u16XInput+u16Offset)*u16Gain)/10000);

    return  lu16SqrtValue;
}

int16_t L_s16LimSignedLongToSignedInt(int32_t s32TempInputData)
{
	int16_t s16LimitedValue;
		
	if(s32TempInputData > S16_MAX_LIB)
	{
	    s16LimitedValue = S16_MAX_LIB;
	}
	else if(s32TempInputData < S16_MIN_LIB)
	{
	    s16LimitedValue = S16_MIN_LIB;
	}
	else
	{
	    s16LimitedValue = (int16_t)s32TempInputData;
	} 
	
	return s16LimitedValue;
}

int32_t L_s32LimitDiff(int32_t s32XInput, int32_t s32Xold, int32_t s32IncMax, int32_t s32DecMax)
{
	int32_t s32DiffRaw = (int32_t)s32XInput - (int32_t)s32Xold;
	int32_t ls32LimitedValue;

	if(s32XInput >= s32Xold)
	{
		if(s32DiffRaw > s32IncMax)
		{
			ls32LimitedValue = s32Xold + s32IncMax;
		}
		else
		{
			ls32LimitedValue = s32XInput;
		}
	}
	else
	{
		if(s32DiffRaw < -s32DecMax)
		{
			ls32LimitedValue = s32Xold - s32DecMax;
		}
		else
		{
			ls32LimitedValue = s32XInput;
		}
	}

	return ls32LimitedValue;
}

int32_t L_s32IInter4Point( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4 )
{
	int32_t s32InterpolationTemp0 = 0;
	int32_t s32InterpolationTemp1 = 0;
    if ( s32Xinput <= s32X1 )
    {
        s32InterpolationTemp1 = s32Y1;
    }
    else if ( s32Xinput <= s32X2 )
    {
        s32InterpolationTemp0=(s32X2-s32X1);
        s32InterpolationTemp1 = s32Y1 + (((s32Y2-s32Y1)*(s32Xinput-s32X1))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X3 )
    {
        s32InterpolationTemp0=(s32X3-s32X2);
        s32InterpolationTemp1 = s32Y2 + (((s32Y3-s32Y2)*(s32Xinput-s32X2))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X4 )
    {
        s32InterpolationTemp0=(s32X4-s32X3);
        s32InterpolationTemp1 = s32Y3 + (((s32Y4-s32Y3)*(s32Xinput-s32X3))/s32InterpolationTemp0);
    }
    else
    {
        s32InterpolationTemp1 = s32Y4;
    }

    return s32InterpolationTemp1;
}

int32_t L_s32IInter6Point( int32_t s32Xinput, int32_t s32X1, int32_t s32Y1, int32_t s32X2, int32_t s32Y2, int32_t s32X3, int32_t s32Y3, int32_t s32X4, int32_t s32Y4, int32_t s32X5, int32_t s32Y5, int32_t s32X6, int32_t s32Y6 )
{
	int32_t s32InterpolationTemp0 = 0;
	int32_t s32InterpolationTemp1 = 0;
    if ( s32Xinput <= s32X1 )
    {
        s32InterpolationTemp1 = s32Y1;
    }
    else if ( s32Xinput <= s32X2 )
    {
        s32InterpolationTemp0=(s32X2-s32X1);
        s32InterpolationTemp1 = s32Y1 + (((s32Y2-s32Y1)*(s32Xinput-s32X1))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X3 )
    {
        s32InterpolationTemp0=(s32X3-s32X2);
        s32InterpolationTemp1 = s32Y2 + (((s32Y3-s32Y2)*(s32Xinput-s32X2))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X4 )
    {
        s32InterpolationTemp0=(s32X4-s32X3);
        s32InterpolationTemp1 = s32Y3 + (((s32Y4-s32Y3)*(s32Xinput-s32X3))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X5 )
    {
        s32InterpolationTemp0=(s32X5-s32X4);
        s32InterpolationTemp1 = s32Y4 + (((s32Y5-s32Y4)*(s32Xinput-s32X4))/s32InterpolationTemp0);
    }
    else if ( s32Xinput <= s32X6 )
    {
        s32InterpolationTemp0=(s32X6-s32X5);
        s32InterpolationTemp1 = s32Y5 + (((s32Y6-s32Y5)*(s32Xinput-s32X5))/s32InterpolationTemp0);
    }
    else
    {
        s32InterpolationTemp1 = s32Y6;
    }

    return s32InterpolationTemp1;
}
