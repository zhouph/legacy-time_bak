#ifndef _WTYPEDEFS_H_
#define _WTYPEDEFS_H_

#include "../../MCAL_RC1/ssc/General/inc/Std_Types.h"
#include "../../../ASW/include/Mando_std_Types.h"
#include "../../../ASW/include/Asw_Types.h"

//typedef unsigned char       uchar8_t;
////typedef signed char         char8_t;
//typedef short int           char16_t;
//typedef long int            char32_t;
//
//typedef unsigned short int  uchar16_t;
//typedef unsigned long int   uchar32_t;
//
//typedef signed char         CHAR;
//typedef short int           INT;
//typedef long int            LONG;
//typedef unsigned char       UCHAR;
//typedef unsigned short int  UINT;
//typedef unsigned long int   ULONG;

//msw typedef unsigned int        uBitType;



struct bit_position 
{
    uBitType bit7    :1;
    uBitType bit6    :1;
    uBitType bit5    :1;
    uBitType bit4    :1;
    uBitType bit3    :1;
    uBitType bit2    :1;
    uBitType bit1    :1;
    uBitType bit0    :1;
};


typedef union
{
    uint8_t R;
    struct
    {
        uBitType bit7    :1;
        uBitType bit6    :1;
        uBitType bit5    :1;
        uBitType bit4    :1;
        uBitType bit3    :1;
        uBitType bit2    :1;
        uBitType bit1    :1;
        uBitType bit0    :1;
    }B;
}t_u8Bit;

typedef union
{
    uint16_t R;
    struct
    {
        uBitType bit15   :1;
        uBitType bit14   :1;
        uBitType bit13   :1;
        uBitType bit12   :1;
        uBitType bit11   :1;
        uBitType bit10   :1;
        uBitType bit9    :1;
        uBitType bit8    :1;
        uBitType bit7    :1;
        uBitType bit6    :1;
        uBitType bit5    :1;
        uBitType bit4    :1;
        uBitType bit3    :1;
        uBitType bit2    :1;
        uBitType bit1    :1;
        uBitType bit0    :1;
    }B;
}t_u16Bit;


typedef struct
{
    uBitType bit15   :1;
    uBitType bit14   :1;
    uBitType bit13   :1;
    uBitType bit12   :1;
    uBitType bit11   :1;
    uBitType bit10   :1;
    uBitType bit9    :1;
    uBitType bit8    :1;
    uBitType bit7    :1;
    uBitType bit6    :1;
    uBitType bit5    :1;
    uBitType bit4    :1;
    uBitType bit3    :1;
    uBitType bit2    :1;
    uBitType bit1    :1;
    uBitType bit0    :1;
}t_u16Bit_Flg;



//MGH80 EPB

struct Epb_8BIT_FLG
{
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
};

typedef struct
{
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
}Epb_8BIT_FLG_t;


struct Epb_16BIT_FLG
{
    uBitType BIT_8      :1;
    uBitType BIT_9      :1;
    uBitType BIT_10     :1;
    uBitType BIT_11     :1;
    uBitType BIT_12     :1;
    uBitType BIT_13     :1;
    uBitType BIT_14     :1;
    uBitType BIT_15     :1;
    
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
};


typedef struct 
{
    uBitType u1Bit0      :1;
    uBitType u1Bit1      :1;
    uBitType u1Bit2      :1;
    uBitType u1Bit3      :1;
    uBitType u1Bit4      :1;
    uBitType u1Bit5      :1;
    uBitType u1Bit6      :1;
    uBitType u1Bit7      :1;
}BIT_STRUCT_t;





#if defined(__LOGGER) 

    #if __ECU==ABS_ECU_1
        extern uint8_t CAN_LOG_DATA[80];
    #elif  __ECU==ESP_ECU_1  
        #if defined(__EXTEND_LOGGER)
            extern uint8_t CAN_LOG_DATA[160];
		#elif defined(__IDB_LOGGER)
            extern uint8_t CAN_LOG_DATA[200];
        #else
            extern uint8_t CAN_LOG_DATA[80];
			#if defined(__AHB_ESC_LOG)
            extern	uint8_t	AHBLOG[80];
			#endif
        #endif
    #endif

#endif


#endif
