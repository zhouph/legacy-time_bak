#ifndef __MCLIB_PID_H__
#define __MCLIB_PID_H__

#include "mclib_types.h"

typedef struct
{
	int16_t		s16Kp;
	Frac16		f16Kp;
	
	int16_t		s16KiTs;
	Frac16		f16KiTs;
	int16_t		s16Kb;
	Frac16		f16Kb;
	
	int16_t		s16Kd;
	Frac16		f16Kd;
	int16_t		s16fs;
	int16_t		s16wc;

	int16_t		s16uOutMax;
	int16_t		s16uOutMin;
	
	int32_t		s32uP;
	int32_t		s32uI;
	int32_t		s32uD;
	int16_t		s16uDifference;
	int16_t		s16errOld;
	int16_t		s16errDotFiltOld;
	int16_t		s16uOut;
}PID;


#endif
