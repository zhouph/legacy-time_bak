/**
 * @defgroup SwcCommonFunction SwcCommonFunction
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SwcCommonFunction.c
 * @brief       Template file
 * @date        2014. 6. 10.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SWC_RETURN_OK                   (0x00)
#define SWC_RETURN_NOT_OK               (0x01)

#define SWC_COMM_ON                     (0x01)
#define SWC_COMM_OFF                    (0x00)

/** User Configuration **/
#define SWC_DEV_ERROR_DETECT            SWC_COMM_ON
#define SWC_STRUCT_INITIAL_VALUE        (0x00)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef unsigned char swc_return;

/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
swc_return SwcInitDataBus(void* const dataPtr, unsigned long dataSize)
{
    unsigned long idx;
    unsigned char* tempStructPtr = (unsigned char*)dataPtr;
    swc_return status = SWC_RETURN_OK;

#if (SWC_DEV_ERROR_DETECT == SWC_COMM_ON)
    if(dataPtr == (void *)0UL)
    {
        /* Error: function parameter out of range */
        status = (swc_return)SWC_RETURN_NOT_OK;
    }
    else if(dataSize <= 0UL)
    {
        /* Error: function parameter out of range */
        status = (swc_return)SWC_RETURN_NOT_OK;
    }
    else
    {
#endif
        for(idx = 0UL; idx < dataSize; idx++)
        {
            /* @violates @ref Mcu_C_REF_3 MISRA 2004 Advisory Rules 17.4 Array indexing
             shall be the only allowed form of pointer* arithmetic*/
            tempStructPtr[idx] = SWC_STRUCT_INITIAL_VALUE;
        }
        for(idx = 0UL; idx < dataSize; idx++)
        {
            /* @violates @ref Mcu_C_REF_3 MISRA 2004 Advisory Rules 17.4 Array indexing
             shall be the only allowed form of pointer* arithmetic*/
            if(tempStructPtr[idx] != SWC_STRUCT_INITIAL_VALUE)
            {
                /* Error: invalid data */
                status = (swc_return)SWC_RETURN_NOT_OK;
                break;
            }
        }
#if (SWC_DEV_ERROR_DETECT == SWC_COMM_ON)
    }
#endif
    return status;
}

#if (0)
static void initCFG(const BaseBrakeController_ConfigType* ConfigPtr)
{
#if (BASEBRAKECONTROLLER_CONFIG_VARIANT != BASEBRAKECONTROLLER_VARIANT_PRECOMPILE)
    /* Post Build Section */

#else /* (BASEBRAKECONTROLLER_CONFIG_VARIANT == BASEBRAKECONTROLLER_VARIANT_PRECOMPILE) */
    /* Precompile Section */
    if (NULL != BBCcfg_ptr)
    {
        ;
    }
    else if (NULL == ConfigPtr)
    {
        BBCcfg_ptr = &BaseBrakeController_Config_PC;
    }
    else
    {
        BBCcfg_ptr = ConfigPtr;
    }
#endif /* (BASEBRAKECONTROLLER_CONFIG_VARIANT != BASEBRAKECONTROLLER_VARIANT_PRECOMPILE) */

}
#endif
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
