/*******************************************************************************
* Project Name: MGH60
* File Name: WContlSubFunc.c
* Description: Timer,RAM,ROM check
+----------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|----------------------------------------------------------------------------------
| Date        Version  Author  Description
| ----------  -------  ------  ----------------------------------------------------
*******************************************************************************/


/* Includes ******************************************************************/
#include "Common.h"

void WF_vMemCopy(const uint8_t *src_ptr, uint8_t *des_ptr, uint8_t number)
{
    uint8_t i;
    for(i=0; i<number; i++)
    {
        *(des_ptr+i)=*(src_ptr+i);
    }
}
