#define S_FUNCTION_NAME      Det_5msCtrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          264
#define WidthOutputPort         37

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Det_5msCtrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[0] = input[0];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[1] = input[1];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[2] = input[2];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[3] = input[3];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[4] = input[4];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[5] = input[5];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[6] = input[6];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[7] = input[7];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[8] = input[8];
    Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[9] = input[9];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[0] = input[10];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[1] = input[11];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[2] = input[12];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[3] = input[13];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[4] = input[14];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[5] = input[15];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[6] = input[16];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[7] = input[17];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[8] = input[18];
    Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[9] = input[19];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[0] = input[20];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[1] = input[21];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[2] = input[22];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[3] = input[23];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[4] = input[24];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[5] = input[25];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[6] = input[26];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[7] = input[27];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[8] = input[28];
    Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[9] = input[29];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[0] = input[30];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[1] = input[31];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[2] = input[32];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[3] = input[33];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[4] = input[34];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[5] = input[35];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[6] = input[36];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[7] = input[37];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[8] = input[38];
    Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[9] = input[39];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[0] = input[40];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[1] = input[41];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[2] = input[42];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[3] = input[43];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[4] = input[44];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[5] = input[45];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[6] = input[46];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[7] = input[47];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[8] = input[48];
    Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[9] = input[49];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[0] = input[50];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[1] = input[51];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[2] = input[52];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[3] = input[53];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[4] = input[54];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[5] = input[55];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[6] = input[56];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[7] = input[57];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[8] = input[58];
    Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[9] = input[59];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[0] = input[60];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[1] = input[61];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[2] = input[62];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[3] = input[63];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[4] = input[64];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[5] = input[65];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[6] = input[66];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[7] = input[67];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[8] = input[68];
    Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[9] = input[69];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[0] = input[70];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[1] = input[71];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[2] = input[72];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[3] = input[73];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[4] = input[74];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[5] = input[75];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[6] = input[76];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[7] = input[77];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[8] = input[78];
    Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[9] = input[79];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0] = input[80];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[1] = input[81];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[2] = input[82];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[3] = input[83];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[4] = input[84];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[0] = input[85];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[1] = input[86];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[2] = input[87];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[3] = input[88];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[4] = input[89];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[0] = input[90];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[1] = input[91];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[2] = input[92];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[3] = input[93];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[4] = input[94];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[0] = input[95];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[1] = input[96];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[2] = input[97];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[3] = input[98];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[4] = input[99];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[0] = input[100];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[1] = input[101];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[2] = input[102];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[3] = input[103];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[4] = input[104];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[0] = input[105];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[1] = input[106];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[2] = input[107];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[3] = input[108];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[4] = input[109];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[0] = input[110];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[1] = input[111];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[2] = input[112];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[3] = input[113];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[4] = input[114];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0] = input[115];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[1] = input[116];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[2] = input[117];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[3] = input[118];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[4] = input[119];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = input[120];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = input[121];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = input[122];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = input[123];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = input[124];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq = input[125];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReq = input[126];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq = input[127];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen = input[128];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen = input[129];
    Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen = input[130];
    Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen = input[131];
    Det_5msCtrlNormVlvReqVlvActInfo.SimVlvDataLen = input[132];
    Det_5msCtrlNormVlvReqVlvActInfo.CircVlvDataLen = input[133];
    Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvDataLen = input[134];
    Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen = input[135];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReq = input[136];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReq = input[137];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReq = input[138];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReq = input[139];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReq = input[140];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReq = input[141];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReq = input[142];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReq = input[143];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvDataLen = input[144];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvDataLen = input[145];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvDataLen = input[146];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvDataLen = input[147];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvDataLen = input[148];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvDataLen = input[149];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvDataLen = input[150];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvDataLen = input[151];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[0] = input[152];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[1] = input[153];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[2] = input[154];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[3] = input[155];
    Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[4] = input[156];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[0] = input[157];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[1] = input[158];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[2] = input[159];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[3] = input[160];
    Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[4] = input[161];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[0] = input[162];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[1] = input[163];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[2] = input[164];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[3] = input[165];
    Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[4] = input[166];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[0] = input[167];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[1] = input[168];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[2] = input[169];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[3] = input[170];
    Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[4] = input[171];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[0] = input[172];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[1] = input[173];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[2] = input[174];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[3] = input[175];
    Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[4] = input[176];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[0] = input[177];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[1] = input[178];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[2] = input[179];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[3] = input[180];
    Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[4] = input[181];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[0] = input[182];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[1] = input[183];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[2] = input[184];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[3] = input[185];
    Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[4] = input[186];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[0] = input[187];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[1] = input[188];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[2] = input[189];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[3] = input[190];
    Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[4] = input[191];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[0] = input[192];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[1] = input[193];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[2] = input[194];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[3] = input[195];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[4] = input[196];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[0] = input[197];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[1] = input[198];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[2] = input[199];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[3] = input[200];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[4] = input[201];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0] = input[202];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[1] = input[203];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[2] = input[204];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[3] = input[205];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[4] = input[206];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq = input[207];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq = input[208];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq = input[209];
    Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvDataLen = input[210];
    Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvDataLen = input[211];
    Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen = input[212];
    Det_5msCtrlEemFailData.Eem_Fail_SimP = input[213];
    Det_5msCtrlEemFailData.Eem_Fail_PedalPDT = input[214];
    Det_5msCtrlEemFailData.Eem_Fail_PedalPDF = input[215];
    Det_5msCtrlEemFailData.Eem_Fail_WssFL = input[216];
    Det_5msCtrlEemFailData.Eem_Fail_WssFR = input[217];
    Det_5msCtrlEemFailData.Eem_Fail_WssRL = input[218];
    Det_5msCtrlEemFailData.Eem_Fail_WssRR = input[219];
    Det_5msCtrlCanRxIdbInfo.HcuServiceMod = input[220];
    Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = input[221];
    Det_5msCtrlPdt5msRawInfo.PdtSig = input[222];
    Det_5msCtrlPdf5msRawInfo.PdfSig = input[223];
    Det_5msCtrlAbsCtrlInfo.AbsActFlg = input[224];
    Det_5msCtrlAbsCtrlInfo.AbsDefectFlg = input[225];
    Det_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = input[226];
    Det_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = input[227];
    Det_5msCtrlAbsCtrlInfo.AbsTarPReLe = input[228];
    Det_5msCtrlAbsCtrlInfo.AbsTarPReRi = input[229];
    Det_5msCtrlAbsCtrlInfo.FrntWhlSlip = input[230];
    Det_5msCtrlAbsCtrlInfo.AbsDesTarP = input[231];
    Det_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = input[232];
    Det_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = input[233];
    Det_5msCtrlCircPFildInfo.PrimCircPFild = input[234];
    Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = input[235];
    Det_5msCtrlCircPFildInfo.SecdCircPFild = input[236];
    Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = input[237];
    Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = input[238];
    Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = input[239];
    Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = input[240];
    Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = input[241];
    Det_5msCtrlPedlTrvlFildInfo.PdfFild = input[242];
    Det_5msCtrlPedlTrvlFildInfo.PdtFild = input[243];
    Det_5msCtrlPistPFildInfo.PistPFild = input[244];
    Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar = input[245];
    Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd = input[246];
    Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe = input[247];
    Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi = input[248];
    Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = input[249];
    Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = input[250];
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssFL = input[251];
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssFR = input[252];
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssRL = input[253];
    Det_5msCtrlEemSuspectData.Eem_Suspect_WssRR = input[254];
    Det_5msCtrlEemSuspectData.Eem_Suspect_SimP = input[255];
    Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = input[256];
    Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = input[257];
    Det_5msCtrlEcuModeSts = input[258];
    Det_5msCtrlPCtrlAct = input[259];
    Det_5msCtrlActvBrkCtrlrActFlg = input[260];
    Det_5msCtrlVehSpd = input[261];
    Det_5msCtrlBlsFild = input[262];
    Det_5msCtrlPCtrlBoostMod = input[263];

    Det_5msCtrl();


    output[0] = Det_5msCtrlBrkPedlStatusInfo.BrkPedlStatus;
    output[1] = Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;
    output[2] = Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg;
    output[3] = Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1;
    output[4] = Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2;
    output[5] = Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg;
    output[6] = Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms;
    output[7] = Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms;
    output[8] = Det_5msCtrlCircPRateInfo.PrimCircPRate;
    output[9] = Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec;
    output[10] = Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms;
    output[11] = Det_5msCtrlCircPRateInfo.SecdCircPChgDurg5ms;
    output[12] = Det_5msCtrlCircPRateInfo.SecdCircPRate;
    output[13] = Det_5msCtrlCircPRateInfo.SecdCircPRateBarPerSec;
    output[14] = Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
    output[15] = Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
    output[16] = Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
    output[17] = Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;
    output[18] = Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate;
    output[19] = Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
    output[20] = Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH;
    output[21] = Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgL;
    output[22] = Det_5msCtrlPedlTrvlTagInfo.PdfSigTag;
    output[23] = Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH;
    output[24] = Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgL;
    output[25] = Det_5msCtrlPedlTrvlTagInfo.PdtSigTag;
    output[26] = Det_5msCtrlPedlTrvlTagInfo.PedlSigTag;
    output[27] = Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH;
    output[28] = Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL;
    output[29] = Det_5msCtrlPistPRateInfo.PistPChgDurg10ms;
    output[30] = Det_5msCtrlPistPRateInfo.PistPChgDurg5ms;
    output[31] = Det_5msCtrlPistPRateInfo.PistPRate;
    output[32] = Det_5msCtrlPistPRateInfo.PistPRateBarPerSec;
    output[33] = Det_5msCtrlVehStopStInfo.VehStandStillStFlg;
    output[34] = Det_5msCtrlVehStopStInfo.VehStopStFlg;
    output[35] = Det_5msCtrlPedlTrvlFinal;
    output[36] = Det_5msCtrlVehSpdFild;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Det_5msCtrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
