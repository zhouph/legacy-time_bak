/**
 * @defgroup Det_Cfg Det_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DET_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_START_SEC_CODE
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DET_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
