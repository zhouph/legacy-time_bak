/**
 * @defgroup Det_5msCtrl_Ifa Det_5msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_5msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_5MSCTRL_IFA_H_
#define DET_5MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqAbcInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData(data) do \
{ \
    *data = Det_5msCtrlEemFailData; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCanRxIdbInfo(data) do \
{ \
    *data = Det_5msCtrlCanRxIdbInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Det_5msCtrlMotRotgAgSigInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPdt5msRawInfo(data) do \
{ \
    *data = Det_5msCtrlPdt5msRawInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPdf5msRawInfo(data) do \
{ \
    *data = Det_5msCtrlPdf5msRawInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo(data) do \
{ \
    *data = Det_5msCtrlCircPFildInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPOffsCorrdInfo(data) do \
{ \
    *data = Det_5msCtrlCircPOffsCorrdInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlSimrPFildInfo(data) do \
{ \
    *data = Det_5msCtrlPedlSimrPFildInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlSimrPOffsCorrdInfo(data) do \
{ \
    *data = Det_5msCtrlPedlSimrPOffsCorrdInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlTrvlFildInfo(data) do \
{ \
    *data = Det_5msCtrlPedlTrvlFildInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPistPFildInfo(data) do \
{ \
    *data = Det_5msCtrlPistPFildInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPistPOffsCorrdInfo(data) do \
{ \
    *data = Det_5msCtrlPistPOffsCorrdInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo(data) do \
{ \
    *data = Det_5msCtrlWhlSpdFildInfo; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_CircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_RelsVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PressDumpVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SimVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_RelsVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_CircVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PressDumpVlvReq(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_SimVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.SimVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_CircVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.CircVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_RelsVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo_PressDumpVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlIvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FlIvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrIvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FrIvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlIvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RlIvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrIvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RrIvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlOvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FlOvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrOvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FrOvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlOvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RlOvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrOvReq(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RrOvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlIvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FlIvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrIvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FrIvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlIvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RlIvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrIvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RrIvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlOvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FlOvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrOvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.FrOvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlOvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RlOvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrOvDataLen(data) do \
{ \
    *data = Det_5msCtrlWhlVlvReqIdbInfo.RrOvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_PrimBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_SecdBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_ChmbBalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[i]; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_PrimBalVlvReq(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_SecdBalVlvReq(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_ChmbBalVlvReq(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_PrimBalVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_SecdBalVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo_ChmbBalVlvDataLen(data) do \
{ \
    *data = Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_SimP; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Det_5msCtrlEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCanRxIdbInfo_HcuServiceMod(data) do \
{ \
    *data = Det_5msCtrlCanRxIdbInfo.HcuServiceMod; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    *data = Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPdt5msRawInfo_PdtSig(data) do \
{ \
    *data = Det_5msCtrlPdt5msRawInfo.PdtSig; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPdf5msRawInfo_PdfSig(data) do \
{ \
    *data = Det_5msCtrlPdf5msRawInfo.PdfSig; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsDefectFlg; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsTarPFrntLe; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsTarPFrntRi; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsTarPReLe; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsTarPReRi; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.FrntWhlSlip; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsDesTarP; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    *data = Det_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo_PrimCircPFild(data) do \
{ \
    *data = Det_5msCtrlCircPFildInfo.PrimCircPFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar(data) do \
{ \
    *data = Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo_SecdCircPFild(data) do \
{ \
    *data = Det_5msCtrlCircPFildInfo.SecdCircPFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar(data) do \
{ \
    *data = Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd(data) do \
{ \
    *data = Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd(data) do \
{ \
    *data = Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(data) do \
{ \
    *data = Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd(data) do \
{ \
    *data = Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlTrvlFildInfo_PdfFild(data) do \
{ \
    *data = Det_5msCtrlPedlTrvlFildInfo.PdfFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPedlTrvlFildInfo_PdtFild(data) do \
{ \
    *data = Det_5msCtrlPedlTrvlFildInfo.PdtFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPistPFildInfo_PistPFild(data) do \
{ \
    *data = Det_5msCtrlPistPFildInfo.PistPFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPistPFildInfo_PistPFild_1_100Bar(data) do \
{ \
    *data = Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrd(data) do \
{ \
    *data = Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntLe(data) do \
{ \
    *data = Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntRi(data) do \
{ \
    *data = Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe(data) do \
{ \
    *data = Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi(data) do \
{ \
    *data = Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_SimP(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_SimP; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    *data = Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlEcuModeSts(data) do \
{ \
    *data = Det_5msCtrlEcuModeSts; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPCtrlAct(data) do \
{ \
    *data = Det_5msCtrlPCtrlAct; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlActvBrkCtrlrActFlg(data) do \
{ \
    *data = Det_5msCtrlActvBrkCtrlrActFlg; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlVehSpd(data) do \
{ \
    *data = Det_5msCtrlVehSpd; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlBlsFild(data) do \
{ \
    *data = Det_5msCtrlBlsFild; \
}while(0);

#define Det_5msCtrl_Read_Det_5msCtrlPCtrlBoostMod(data) do \
{ \
    *data = Det_5msCtrlPCtrlBoostMod; \
}while(0);


/* Set Output DE MAcro Function */
#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo(data) do \
{ \
    Det_5msCtrlCircPRateInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo(data) do \
{ \
    Det_5msCtrlEstimdWhlPInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlRateInfo(data) do \
{ \
    Det_5msCtrlPedlTrvlRateInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo(data) do \
{ \
    Det_5msCtrlPistPRateInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlVehStopStInfo(data) do \
{ \
    Det_5msCtrlVehStopStInfo = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_BrkPedlStatus(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.BrkPedlStatus = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_DrvrIntendToRelsBrk(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_PanicBrkStFlg(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_PedlReldStFlg1(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1 = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_PedlReldStFlg2(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2 = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo_PedlReldStUsingPedlSimrPFlg(data) do \
{ \
    Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_PrimCircPChgDurg10ms(data) do \
{ \
    Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_PrimCircPChgDurg5ms(data) do \
{ \
    Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_PrimCircPRate(data) do \
{ \
    Det_5msCtrlCircPRateInfo.PrimCircPRate = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_PrimCircPRateBarPerSec(data) do \
{ \
    Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_SecdCircPChgDurg10ms(data) do \
{ \
    Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_SecdCircPChgDurg5ms(data) do \
{ \
    Det_5msCtrlCircPRateInfo.SecdCircPChgDurg5ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_SecdCircPRate(data) do \
{ \
    Det_5msCtrlCircPRateInfo.SecdCircPRate = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo_SecdCircPRateBarPerSec(data) do \
{ \
    Det_5msCtrlCircPRateInfo.SecdCircPRateBarPerSec = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo_FrntLeEstimdWhlP(data) do \
{ \
    Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo_FrntRiEstimdWhlP(data) do \
{ \
    Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo_ReLeEstimdWhlP(data) do \
{ \
    Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo_ReRiEstimdWhlP(data) do \
{ \
    Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlRateInfo_PedlTrvlRate(data) do \
{ \
    Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(data) do \
{ \
    Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgH(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgL(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgL = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdfSigTag(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdfSigTag = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgH(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgL(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgL = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PdtSigTag(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PdtSigTag = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PedlSigTag(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PedlSigTag = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PedlSimrPSigNoiseSuspcFlgH(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo_PedlSimrPSigNoiseSuspcFlgL(data) do \
{ \
    Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo_PistPChgDurg10ms(data) do \
{ \
    Det_5msCtrlPistPRateInfo.PistPChgDurg10ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo_PistPChgDurg5ms(data) do \
{ \
    Det_5msCtrlPistPRateInfo.PistPChgDurg5ms = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo_PistPRate(data) do \
{ \
    Det_5msCtrlPistPRateInfo.PistPRate = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo_PistPRateBarPerSec(data) do \
{ \
    Det_5msCtrlPistPRateInfo.PistPRateBarPerSec = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlVehStopStInfo_VehStandStillStFlg(data) do \
{ \
    Det_5msCtrlVehStopStInfo.VehStandStillStFlg = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlVehStopStInfo_VehStopStFlg(data) do \
{ \
    Det_5msCtrlVehStopStInfo.VehStopStFlg = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlPedlTrvlFinal(data) do \
{ \
    Det_5msCtrlPedlTrvlFinal = *data; \
}while(0);

#define Det_5msCtrl_Write_Det_5msCtrlVehSpdFild(data) do \
{ \
    Det_5msCtrlVehSpdFild = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_5MSCTRL_IFA_H_ */
/** @} */
