/**
 * @defgroup Det_1msCtrl_Ifa Det_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_1msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_1MSCTRL_IFA_H_
#define DET_1MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Det_1msCtrl_Read_Det_1msCtrlCircPFild1msInfo(data) do \
{ \
    *data = Det_1msCtrlCircPFild1msInfo; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlPedlTrvlFild1msInfo(data) do \
{ \
    *data = Det_1msCtrlPedlTrvlFild1msInfo; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlPistPFild1msInfo(data) do \
{ \
    *data = Det_1msCtrlPistPFild1msInfo; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlCircPFild1msInfo_PrimCircPFild1ms(data) do \
{ \
    *data = Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlCircPFild1msInfo_SecdCircPFild1ms(data) do \
{ \
    *data = Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms(data) do \
{ \
    *data = Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlPistPFild1msInfo_PistPFild1ms(data) do \
{ \
    *data = Det_1msCtrlPistPFild1msInfo.PistPFild1ms; \
}while(0);

#define Det_1msCtrl_Read_Det_1msCtrlEcuModeSts(data) do \
{ \
    *data = Det_1msCtrlEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Det_1msCtrl_Write_Det_1msCtrlBrkPedlStatus1msInfo(data) do \
{ \
    Det_1msCtrlBrkPedlStatus1msInfo = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlCircPRate1msInfo(data) do \
{ \
    Det_1msCtrlCircPRate1msInfo = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlPedlTrvlRate1msInfo(data) do \
{ \
    Det_1msCtrlPedlTrvlRate1msInfo = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlPistPRate1msInfo(data) do \
{ \
    Det_1msCtrlPistPRate1msInfo = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg(data) do \
{ \
    Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlCircPRate1msInfo_PrimCircPRate1msAvg_1_100Bar(data) do \
{ \
    Det_1msCtrlCircPRate1msInfo.PrimCircPRate1msAvg_1_100Bar = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlCircPRate1msInfo_SecdCircPRate1msAvg_1_100Bar(data) do \
{ \
    Det_1msCtrlCircPRate1msInfo.SecdCircPRate1msAvg_1_100Bar = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec(data) do \
{ \
    Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = *data; \
}while(0);

#define Det_1msCtrl_Write_Det_1msCtrlPistPRate1msInfo_PistPRate1msAvg_1_100Bar(data) do \
{ \
    Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_1MSCTRL_IFA_H_ */
/** @} */
