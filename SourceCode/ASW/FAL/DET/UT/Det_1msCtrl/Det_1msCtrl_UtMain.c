#include "unity.h"
#include "unity_fixture.h"
#include "Det_1msCtrl.h"
#include "Det_1msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Rtesint32 UtInput_Det_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[MAX_STEP] = DET_1MSCTRLCIRCPFILD1MSINFO_PRIMCIRCPFILD1MS;
const Rtesint32 UtInput_Det_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[MAX_STEP] = DET_1MSCTRLCIRCPFILD1MSINFO_SECDCIRCPFILD1MS;
const Rtesint32 UtInput_Det_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[MAX_STEP] = DET_1MSCTRLPEDLTRVLFILD1MSINFO_PDTFILD1MS;
const Rtesint32 UtInput_Det_1msCtrlPistPFild1msInfo_PistPFild1ms[MAX_STEP] = DET_1MSCTRLPISTPFILD1MSINFO_PISTPFILD1MS;
const Mom_HndlrEcuModeSts_t UtInput_Det_1msCtrlEcuModeSts[MAX_STEP] = DET_1MSCTRLECUMODESTS;

const Rtesint32 UtExpected_Det_1msCtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg[MAX_STEP] = DET_1MSCTRLBRKPEDLSTATUS1MSINFO_PANICBRKST1MSFLG;
const Rtesint32 UtExpected_Det_1msCtrlCircPRate1msInfo_PrimCircPRate1msAvg_1_100Bar[MAX_STEP] = DET_1MSCTRLCIRCPRATE1MSINFO_PRIMCIRCPRATE1MSAVG_1_100BAR;
const Rtesint32 UtExpected_Det_1msCtrlCircPRate1msInfo_SecdCircPRate1msAvg_1_100Bar[MAX_STEP] = DET_1MSCTRLCIRCPRATE1MSINFO_SECDCIRCPRATE1MSAVG_1_100BAR;
const Rtesint32 UtExpected_Det_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec[MAX_STEP] = DET_1MSCTRLPEDLTRVLRATE1MSINFO_PEDLTRVLRATE1MSMMPERSEC;
const Rtesint32 UtExpected_Det_1msCtrlPistPRate1msInfo_PistPRate1msAvg_1_100Bar[MAX_STEP] = DET_1MSCTRLPISTPRATE1MSINFO_PISTPRATE1MSAVG_1_100BAR;



TEST_GROUP(Det_1msCtrl);
TEST_SETUP(Det_1msCtrl)
{
    Det_1msCtrl_Init();
}

TEST_TEAR_DOWN(Det_1msCtrl)
{   /* Postcondition */

}

TEST(Det_1msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = UtInput_Det_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[i];
        Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = UtInput_Det_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[i];
        Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = UtInput_Det_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[i];
        Det_1msCtrlPistPFild1msInfo.PistPFild1ms = UtInput_Det_1msCtrlPistPFild1msInfo_PistPFild1ms[i];
        Det_1msCtrlEcuModeSts = UtInput_Det_1msCtrlEcuModeSts[i];

        Det_1msCtrl();

        TEST_ASSERT_EQUAL(Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg, UtExpected_Det_1msCtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg[i]);
        TEST_ASSERT_EQUAL(Det_1msCtrlCircPRate1msInfo.PrimCircPRate1msAvg_1_100Bar, UtExpected_Det_1msCtrlCircPRate1msInfo_PrimCircPRate1msAvg_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Det_1msCtrlCircPRate1msInfo.SecdCircPRate1msAvg_1_100Bar, UtExpected_Det_1msCtrlCircPRate1msInfo_SecdCircPRate1msAvg_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec, UtExpected_Det_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec[i]);
        TEST_ASSERT_EQUAL(Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar, UtExpected_Det_1msCtrlPistPRate1msInfo_PistPRate1msAvg_1_100Bar[i]);
    }
}

TEST_GROUP_RUNNER(Det_1msCtrl)
{
    RUN_TEST_CASE(Det_1msCtrl, All);
}
