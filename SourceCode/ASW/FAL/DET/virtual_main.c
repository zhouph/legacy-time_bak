/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Det_1msCtrl.h"
#include "Det_5msCtrl.h"

int main(void)
{
    Det_1msCtrl_Init();
    Det_5msCtrl_Init();

    while(1)
    {
        Det_1msCtrl();
        Det_5msCtrl();
    }
}