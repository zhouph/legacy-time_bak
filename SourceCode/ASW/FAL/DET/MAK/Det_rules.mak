# \file
#
# \brief Det
#
# This file contains the implementation of the SWC
# module Det.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Det_src

Det_src_FILES        += $(Det_SRC_PATH)\Det_1msCtrl.c
Det_src_FILES        += $(Det_IFA_PATH)\Det_1msCtrl_Ifa.c
Det_src_FILES        += $(Det_SRC_PATH)\Det_5msCtrl.c
Det_src_FILES        += $(Det_IFA_PATH)\Det_5msCtrl_Ifa.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_CalcSignalChangeRate.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_CalcSignalChangeRate1ms.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_DecidePedalTravel.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_DetVehicleState.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_DetVehicleState1ms.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_WheelPressureEstimation.c
Det_src_FILES        += $(Det_SRC_PATH)\LDIDB_LoadMapData.c
Det_src_FILES        += $(Det_CFG_PATH)\Det_Cfg.c
Det_src_FILES        += $(Det_CAL_PATH)\Det_Cal.c
# /* HSH */
ifeq ($(ICE_COMPILE),true)
# Library
Det_src_FILES        += $(Det_CORE_PATH)\virtual_main.c
Spc_src_FILES  += $(Det_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Spc_src_FILES  += $(Det_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Spc_src_FILES  += $(Det_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Spc_src_FILES  += $(Det_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Det_src_FILES        += $(Det_UNITY_PATH)\unity.c
	Det_src_FILES        += $(Det_UNITY_PATH)\unity_fixture.c	
	Det_src_FILES        += $(Det_UT_PATH)\main.c
	Det_src_FILES        += $(Det_UT_PATH)\Det_1msCtrl\Det_1msCtrl_UtMain.c
	Det_src_FILES        += $(Det_UT_PATH)\Det_5msCtrl\Det_5msCtrl_UtMain.c
endif