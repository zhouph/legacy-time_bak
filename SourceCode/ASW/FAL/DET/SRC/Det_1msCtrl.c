/**
 * @defgroup Det_1msCtrl Det_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_1msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_1msCtrl.h"
#include "Det_1msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LDIDB_DetVehicleState.h"
#include "LDIDB_DetVehicleState1ms.h"
#include "LDIDB_CalcSignalChangeRate.h"
#include "LDIDB_CalcSignalChangeRate1ms.h"
#include "LDIDB_DecidePedalTravel.h"
#include "LDIDB_WheelPressureEstimation.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DET_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Det_1msCtrl_HdrBusType Det_1msCtrlBus;

/* Version Info */
const SwcVersionInfo_t Det_1msCtrlVersionInfo = 
{   
    DET_1MSCTRL_MODULE_ID,           /* Det_1msCtrlVersionInfo.ModuleId */
    DET_1MSCTRL_MAJOR_VERSION,       /* Det_1msCtrlVersionInfo.MajorVer */
    DET_1MSCTRL_MINOR_VERSION,       /* Det_1msCtrlVersionInfo.MinorVer */
    DET_1MSCTRL_PATCH_VERSION,       /* Det_1msCtrlVersionInfo.PatchVer */
    DET_1MSCTRL_BRANCH_VERSION       /* Det_1msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Spc_1msCtrlCircPFild1msInfo_t Det_1msCtrlCircPFild1msInfo;
Spc_1msCtrlPedlTrvlFild1msInfo_t Det_1msCtrlPedlTrvlFild1msInfo;
Spc_1msCtrlPistPFild1msInfo_t Det_1msCtrlPistPFild1msInfo;
Mom_HndlrEcuModeSts_t Det_1msCtrlEcuModeSts;

/* Output Data Element */
Det_1msCtrlBrkPedlStatus1msInfo_t Det_1msCtrlBrkPedlStatus1msInfo;
Det_1msCtrlCircPRate1msInfo_t Det_1msCtrlCircPRate1msInfo;
Det_1msCtrlPedlTrvlRate1msInfo_t Det_1msCtrlPedlTrvlRate1msInfo;
Det_1msCtrlPistPRate1msInfo_t Det_1msCtrlPistPRate1msInfo;

uint32 Det_1msCtrl_Timer_Start;
uint32 Det_1msCtrl_Timer_Elapsed;

#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Det_1msCtrl_Init(void)
{
    /* Initialize internal bus */
    Det_1msCtrlBus.Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = 0;
    Det_1msCtrlBus.Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = 0;
    Det_1msCtrlBus.Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = 0;
    Det_1msCtrlBus.Det_1msCtrlPistPFild1msInfo.PistPFild1ms = 0;
    Det_1msCtrlBus.Det_1msCtrlEcuModeSts = 0;
    Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = 0;
    Det_1msCtrlBus.Det_1msCtrlCircPRate1msInfo.PrimCircPRate1msAvg_1_100Bar = 0;
    Det_1msCtrlBus.Det_1msCtrlCircPRate1msInfo.SecdCircPRate1msAvg_1_100Bar = 0;
    Det_1msCtrlBus.Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = 0;
    Det_1msCtrlBus.Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar = 0;
}

void Det_1msCtrl(void)
{
    Det_1msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Det_1msCtrl_Read_Det_1msCtrlCircPFild1msInfo(&Det_1msCtrlBus.Det_1msCtrlCircPFild1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlCircPFild1msInfo 
     : Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
     : Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
     =============================================================================*/
    
    Det_1msCtrl_Read_Det_1msCtrlPedlTrvlFild1msInfo(&Det_1msCtrlBus.Det_1msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlPedlTrvlFild1msInfo 
     : Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/
    
    Det_1msCtrl_Read_Det_1msCtrlPistPFild1msInfo(&Det_1msCtrlBus.Det_1msCtrlPistPFild1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlPistPFild1msInfo 
     : Det_1msCtrlPistPFild1msInfo.PistPFild1ms;
     =============================================================================*/
    
    Det_1msCtrl_Read_Det_1msCtrlEcuModeSts(&Det_1msCtrlBus.Det_1msCtrlEcuModeSts);

    /* Process */
    if(DETsrcbus.SystemOnCnt1ms<10000)
    {
        DETsrcbus.SystemOnCnt1ms++;
    }	
    LDIDB_vCallSigRateCalcn1ms();
    LDIDB_vCallVehStDet1ms();

    /* Output */
    Det_1msCtrl_Write_Det_1msCtrlBrkPedlStatus1msInfo(&Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlBrkPedlStatus1msInfo 
     : Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/
    
    Det_1msCtrl_Write_Det_1msCtrlCircPRate1msInfo(&Det_1msCtrlBus.Det_1msCtrlCircPRate1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlCircPRate1msInfo 
     : Det_1msCtrlCircPRate1msInfo.PrimCircPRate1msAvg_1_100Bar;
     : Det_1msCtrlCircPRate1msInfo.SecdCircPRate1msAvg_1_100Bar;
     =============================================================================*/
    
    Det_1msCtrl_Write_Det_1msCtrlPedlTrvlRate1msInfo(&Det_1msCtrlBus.Det_1msCtrlPedlTrvlRate1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlPedlTrvlRate1msInfo 
     : Det_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/
    
    Det_1msCtrl_Write_Det_1msCtrlPistPRate1msInfo(&Det_1msCtrlBus.Det_1msCtrlPistPRate1msInfo);
    /*==============================================================================
    * Members of structure Det_1msCtrlPistPRate1msInfo 
     : Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar;
     =============================================================================*/
    

    Det_1msCtrl_Timer_Elapsed = STM0_TIM0.U - Det_1msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DET_1MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
