/**
 * @defgroup LDIDB_DetVehicleState LDIDB_DetVehicleState
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_DetVehicleState.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_DetVehicleState1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
     int32_t lsahbs16PedalFilt1ms;
     int32_t lsahbs16PedalRate1ms;
     int32_t ldahbs16VehicleSpeed;
     int32_t u1SpeedInvalidFlg;
}GetDetVehStInfo1ms_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC uint16_t    ldahbu1InitFastPedalApply1msFlg;

FAL_STATIC int16_t ldahbs16FastApplyDetThresLow;
FAL_STATIC int16_t ldahbs16FastApplyDetThresHigh;
FAL_STATIC int16_t ldahbs16FastApply1msCnt;
FAL_STATIC GetDetVehStInfo1ms_t GetDetVehStInfo1ms;

#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"
static void LDIDB_vDetPanicBrkStUsg1msPedlSig(void);
static int16_t LDIDB_s16CalcFastApplyCnt(int16_t s16Pedal, int16_t s16PedalRate, int16_t s16FastApplyCntOld);
static void LSIDB_vInpSigIfForVehStDetn1ms(void);
static void LSIDB_vOutpSigIfForVehStDetn1ms(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void LDIDB_vCallVehStDet1ms(void)
{
    LSIDB_vInpSigIfForVehStDetn1ms();
    LDIDB_vDetPanicBrkStUsg1msPedlSig();
	LSIDB_vOutpSigIfForVehStDetn1ms();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LSIDB_vInpSigIfForVehStDetn1ms(void)
{
    GetDetVehStInfo1ms.lsahbs16PedalFilt1ms  = Det_1msCtrlBus.Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
    GetDetVehStInfo1ms.lsahbs16PedalRate1ms  = DETsrcbus.PedlTrvl1msSigRate;
    GetDetVehStInfo1ms.u1SpeedInvalidFlg    = DETsrcbus.SpeedInvalidFlg;
    GetDetVehStInfo1ms.ldahbs16VehicleSpeed  = DETsrcbus.VehSpdFild;
    
}

static void LSIDB_vOutpSigIfForVehStDetn1ms(void)
{
	Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg   = (int32_t)ldahbu1InitFastPedalApply1msFlg;
}

static void LDIDB_vDetPanicBrkStUsg1msPedlSig(void)
{
	int16_t s16FastApply1msCntChange;

	if(GetDetVehStInfo1ms.u1SpeedInvalidFlg == 0)
	{
		ldahbs16FastApplyDetThresLow = L_s16IInter2Point1ms((int16_t)GetDetVehStInfo1ms.ldahbs16VehicleSpeed,
		                                           S16_SPEED_30_KPH, S16_FAST_APPLY_DET_1MS_TH1_LSPD,
		                                           S16_SPEED_70_KPH, S16_FAST_APPLY_DET_1MS_TH1);
		ldahbs16FastApplyDetThresHigh = L_s16IInter2Point1ms((int16_t)GetDetVehStInfo1ms.ldahbs16VehicleSpeed,
		                                           S16_SPEED_30_KPH, S16_FAST_APPLY_DET_1MS_TH2_LSPD,
		                                           S16_SPEED_70_KPH, S16_FAST_APPLY_DET_1MS_TH2);
	}
	else
	{
		ldahbs16FastApplyDetThresLow = S16_FAST_APPLY_DET_1MS_TH1;
		ldahbs16FastApplyDetThresHigh = S16_FAST_APPLY_DET_1MS_TH2;
	}

	s16FastApply1msCntChange = LDIDB_s16CalcFastApplyCnt((int16_t)GetDetVehStInfo1ms.lsahbs16PedalFilt1ms, GetDetVehStInfo1ms.lsahbs16PedalRate1ms, ldahbs16FastApply1msCnt);

    ldahbs16FastApply1msCnt = ldahbs16FastApply1msCnt + s16FastApply1msCntChange;

    ldahbs16FastApply1msCnt = L_s16LimitMinMax1ms(ldahbs16FastApply1msCnt, 0, 100);

    if(ldahbu1InitFastPedalApply1msFlg == 0)
    {
    	if(ldahbs16FastApply1msCnt >= 20)
    	{
    		ldahbu1InitFastPedalApply1msFlg = 1;
    	}
    }
    else
    {
    	if(ldahbs16FastApply1msCnt <= 0)
    	{
    		ldahbu1InitFastPedalApply1msFlg = 0;
    	}
    }
}


static int16_t LDIDB_s16CalcFastApplyCnt(int16_t s16Pedal, int16_t s16PedalRate, int16_t s16FastApplyCntOld)
{
	int16_t s16FastApplyCntChange;

    if(s16PedalRate>=ldahbs16FastApplyDetThresLow)
	{
		if(s16Pedal<=S16_FAST_APPLY_DET_ALLOW_PEDAL)
		{
			if(s16PedalRate>=ldahbs16FastApplyDetThresHigh)
			{
				s16FastApplyCntChange = s16PedalRate/S16_TRAVEL_1_MM;
			}
			else
			{
				s16FastApplyCntChange = 1;
			}
		}
		else
		{
			s16FastApplyCntChange = 0;
		}
	}
	else if(s16FastApplyCntOld <= 0)
	{
		s16FastApplyCntChange = 0;
	}
	else if((s16PedalRate<S16_FAST_APPLY_RESET_THRES) || (s16Pedal<=S16_TRAVEL_3_MM))
	{
		s16FastApplyCntChange = -20;
	}
	else
	{
		s16FastApplyCntChange = -1;
	}

	return s16FastApplyCntChange;
}

#define DET_1MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
