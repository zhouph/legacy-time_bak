/**
 * @defgroup LDIDB_DetVehicleState LDIDB_DetVehicleState
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_DetVehicleState.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LDIDB_DETVEHICLESTATE1MS_H_
#define LDIDB_DETVEHICLESTATE1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_1msCtrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LDIDB_vCallVehStDet1ms(void);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LDIDB_DETVEHICLESTATE_H_ */
/** @} */
