/**
 * @defgroup LDIDB_DecidePedalTravel LDIDB_DecidePedalTravel
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_DecidePedalTravel.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/
/*==============================================================================
 *  Code Metric
 *  Function								Metric
 *  LSAHB_vCntPedalNoise					STCYC(22), STPTH(5832)
 *  LSAHB_vDetectPedalNoise					STCYC(19), STPTH(4096)
 *  LSAHB_vCallNoiseSuspect					STCYC(13), STPTH(729)
 *  LDIDB_SelPedlInpSig						STCYC(31), STPTH(31104), STMIF(6)
 *  LDIDB_vDetFinalPedlTravl				STCYC(15), STMIF(7)
 *
 =============================================================================*/
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_DecidePedalTravel.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#if __AHB_PEDAL_MODEL_CHECK == ENABLE
	#define __USE_LPF_SIGNAL_FOR_PD_MODEL       ENABLE
	#if !defined(__ONLINE_CALIBRATION_ENABLE)
		#define U8_PEDAL_CHECK_PERIOD           U8_T_100_MS
		#define U16_MAX_VEL_THRES_LV1           S16_TRAVEL_1_MM
		#define U16_MAX_VEL_THRES_LV2           S16_TRAVEL_1_5_MM
		#define U16_MAX_VEL_THRES_LV3           S16_TRAVEL_3_MM
		#define U8_NOISE_DETECT_THRES_L         6
		#define U8_NOISE_DETECT_THRES_H         9
		#define S16_SUSCNT_DOWN_CONDITION       S16_TRAVEL_10_MM
		#define S16_SUS_OFF_CONDITION           S16_TRAVEL_5_MM
		#define U8_SUS_ON_CNT                   10
		#define U8_SUS_OFF_CNT                  0
	#endif /*!defined(__ONLINE_CALIBRATION_ENABLE)*/
	#define U8_HCNT_MAX                     30
	#define U8_HCNT_MIN                     0
	#define U8_LCNT_MAX                     30
	#define U8_LCNT_MIN                     0
#endif /*__AHB_PEDAL_MODEL_CHECK ==1*/



/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    int16_t      lsahbs16PDTRaw;
    int16_t      lsahbs16PDFRaw;
    int16_t      lsahbs16PDFFilt;
    int16_t      lsahbs16PDTFilt;
    int16_t     lsahbs16PSpresFilt_1_100Bar;

    int16_t 	lsahbs16SystemOnCnt;

    uint32_t      fsu1PdtSenFaultDet;
    uint32_t      fdu1PdtSigSusSet;
    uint32_t      fsu1PdfSenFaultDet;
    uint32_t      fdu1PdfSigSusSet;
    uint32_t      fsu1PSPSenFaultDet;
    uint32_t      fu1PspSusDet;
    uint32_t      lcahbu1AHBOn;


}GetDecidePedlTrvl_t;
typedef struct
{

#if   __AHB_PEDAL_MODEL_CHECK == ENABLE
    uint16_t    PDTNoiseSuspectFlgL :1;
    uint16_t    PDFNoiseSuspectFlgL :1;
    uint16_t    PSPNoiseSuspectFlgL :1;
    uint16_t    PDTNoiseSuspectFlgH :1;
    uint16_t    PDFNoiseSuspectFlgH :1;
    uint16_t    PSPNoiseSuspectFlgH :1;
#endif
    uint16_t	AHBonStart			:1;
    uint16_t	PedalSigChg			:1;
    uint16_t	PedalFiltCoeffChg	:1;
}det_bit_t;


/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#if   __AHB_PEDAL_MODEL_CHECK== ENABLE
FAL_STATIC int8_t lsahbu8ChkModelReliabilityCnt;
FAL_STATIC int16_t lsahbs16PDTLowPassFilt;
FAL_STATIC int16_t lsahbs16PDFLowPassFilt;
FAL_STATIC int16_t lsahbs16PSPLowPassFilt;
FAL_STATIC int16_t lsahbs16PDTLowPassFiltOld;
FAL_STATIC int16_t lsahbs16PDFLowPassFiltOld;
FAL_STATIC int16_t lsahbs16PSPLowPassFiltOld;
FAL_STATIC int16_t lsahbs16PDTDiff;
FAL_STATIC int16_t lsahbs16PDFDiff;
FAL_STATIC int16_t lsahbs16PSPDiff;
FAL_STATIC int16_t lsahbs16PDTDiffTemp;
FAL_STATIC int16_t lsahbs16PDFDiffTemp;
FAL_STATIC int16_t lsahbs16PSPDiffTemp;
FAL_STATIC int16_t lsahbs16PDTMaxVel;
FAL_STATIC int16_t lsahbs16PDFMaxVel;
FAL_STATIC int16_t lsahbs16PSPMaxVel;
FAL_STATIC int8_t lsahbs8PDTsgn;
FAL_STATIC int8_t lsahbs8PDFsgn;
FAL_STATIC int8_t lsahbs8PSPsgn;
FAL_STATIC int8_t lsahbs8PDTsgnOld;
FAL_STATIC int8_t lsahbs8PDFsgnOld;
FAL_STATIC int8_t lsahbs8PSPsgnOld;
FAL_STATIC int8_t lsahbs8PDTsgnMulti;
FAL_STATIC int8_t lsahbs8PDFsgnMulti;
FAL_STATIC int8_t lsahbs8PSPsgnMulti;
FAL_STATIC uint8_t  lsahbu8PDTThresOverCnt;
FAL_STATIC uint8_t  lsahbu8PDFThresOverCnt;
FAL_STATIC uint8_t  lsahbu8PSPThresOverCnt;
FAL_STATIC uint8_t  lsahbu8PDTThresOverCntH;
FAL_STATIC uint8_t  lsahbu8PDFThresOverCntH;
FAL_STATIC uint8_t  lsahbu8PSPThresOverCntH;
FAL_STATIC uint8_t lsahbu8PDTSusHCnt;
FAL_STATIC uint8_t lsahbu8PDTSusLCnt;
FAL_STATIC uint8_t lsahbu8PDFSusHCnt;
FAL_STATIC uint8_t lsahbu8PDFSusLCnt;
FAL_STATIC uint8_t lsahbu8PSPSusHCnt;
FAL_STATIC uint8_t lsahbu8PSPSusLCnt;
#endif   /* __AHB_PEDAL_MODEL_CHECK */

/*--- Final Input Signal Tag --*/
FAL_STATIC uint8_t    lsahbu8PedalSigTagPre;
FAL_STATIC uint8_t    lsahbu8SelectedPedal;
FAL_STATIC uint8_t    lsahbu8MCpressSigTag;
FAL_STATIC uint8_t    lsahbu8PedalSigTag;
FAL_STATIC uint8_t    lsahbu8PDTSigTag;
FAL_STATIC uint8_t    lsahbu8PDFSigTag;
FAL_STATIC uint8_t    lsahbu8PSPSigTag;
FAL_STATIC uint8_t    lsahbu8PedalInfoSigTag;
FAL_STATIC uint8_t    lsahbu8PedalInfoSigTagPre;

/*----   Pedal travel Model -------*/
FAL_STATIC int16_t    lsahbs16PedalTravelByPS;
FAL_STATIC int16_t    lsahbs16PSpresModel;
FAL_STATIC int16_t    lsahbs16PedalTravelByPSold;

/*--- Final Input Signal -----*/
FAL_STATIC int16_t		lsahbs16PedalTravelInp;
FAL_STATIC int16_t    	lsahbs16PedalTravelRaw;
FAL_STATIC int16_t		lsahbs16PedalTravelFilt;
FAL_STATIC int16_t		lsahbs16PedalTravelFiltOld;

FAL_STATIC uint8_t    lsahbu8PedalFiltChgCNT;
FAL_STATIC GetDecidePedlTrvl_t GetDecidePedlTrvl;
FAL_STATIC det_bit_t DET_BIT;

#if   __AHB_PEDAL_MODEL_CHECK==1
#define lsahbu1PDTNoiseSuspectFlgL     DET_BIT.PDTNoiseSuspectFlgL
#define lsahbu1PDFNoiseSuspectFlgL     DET_BIT.PDFNoiseSuspectFlgL
#define lsahbu1PSPNoiseSuspectFlgL     DET_BIT.PSPNoiseSuspectFlgL
#define lsahbu1PDTNoiseSuspectFlgH     DET_BIT.PDTNoiseSuspectFlgH
#define lsahbu1PDFNoiseSuspectFlgH     DET_BIT.PDFNoiseSuspectFlgH
#define lsahbu1PSPNoiseSuspectFlgH     DET_BIT.PSPNoiseSuspectFlgH

#endif /*__AHB_PEDAL_MODEL_CHECK==1*/
#define lsahbu1PedalSigChg              DET_BIT.PedalSigChg
#define lsahbu1AHBonStart               DET_BIT.AHBonStart
#define lsahbu1PedalFiltCoeffChg        DET_BIT.PedalFiltCoeffChg

#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

static void LDIDB_vEstimPedlTrvlFromPedlSimrP(void);
static void LDIDB_SelPedlInpSig(void);
static void LDIDB_vDetFinalPedlTravl(void);

#if   __AHB_PEDAL_MODEL_CHECK == ENABLE
static void    LDIDB_vChkPedlSigRliaby(void);
static void    LSAHB_vCntPedalNoise(void);
static void    LSAHB_vDetectPedalNoise(void);
static void    LSAHB_vCallNoiseSuspect(void);
#endif
static void LDIDB_vInpSigIfForPedlTrvlCaln(void);
static void LDIDB_vOutpSigIfForPedlTrvlCaln(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LDIDB_vCallPedlInpSeln(void)
{
    LDIDB_vInpSigIfForPedlTrvlCaln();
    LDIDB_vEstimPedlTrvlFromPedlSimrP();
#if  __AHB_PEDAL_MODEL_CHECK==1
	LDIDB_vChkPedlSigRliaby();
#endif
	LDIDB_SelPedlInpSig();
	LDIDB_vDetFinalPedlTravl();
	LDIDB_vOutpSigIfForPedlTrvlCaln();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LDIDB_vInpSigIfForPedlTrvlCaln(void)
{
    GetDecidePedlTrvl.lsahbs16PDTRaw      = (int16_t)Det_5msCtrlBus.Det_5msCtrlPdt5msRawInfo.PdtSig;
    GetDecidePedlTrvl.lsahbs16PDFRaw      = (int16_t)Det_5msCtrlBus.Det_5msCtrlPdf5msRawInfo.PdfSig;
    GetDecidePedlTrvl.fsu1PdtSenFaultDet  = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDT;
    GetDecidePedlTrvl.fdu1PdtSigSusSet    = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT;
    GetDecidePedlTrvl.fsu1PdfSenFaultDet  = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDF;
    GetDecidePedlTrvl.fdu1PdfSigSusSet    = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF;
    GetDecidePedlTrvl.fsu1PSPSenFaultDet  = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_SimP;
    GetDecidePedlTrvl.fu1PspSusDet        = (uint32_t)Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_SimP;
    GetDecidePedlTrvl.lcahbu1AHBOn        = (uint32_t)Det_5msCtrlBus.Det_5msCtrlPCtrlAct;
    GetDecidePedlTrvl.lsahbs16PDFFilt     = (int16_t)Det_5msCtrlBus.Det_5msCtrlPedlTrvlFildInfo.PdfFild;
    GetDecidePedlTrvl.lsahbs16PDTFilt     = (int16_t)Det_5msCtrlBus.Det_5msCtrlPedlTrvlFildInfo.PdtFild;
    GetDecidePedlTrvl.lsahbs16PSpresFilt_1_100Bar = (int16_t)Det_5msCtrlBus.Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;

    GetDecidePedlTrvl.lsahbs16SystemOnCnt =(int16_t)DETsrcbus.SystemOnCnt;

}
static void LDIDB_vOutpSigIfForPedlTrvlCaln(void)
{
#if   __AHB_PEDAL_MODEL_CHECK == ENABLE
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH       = (int32_t)lsahbu1PDFNoiseSuspectFlgH;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgL       = (int32_t)lsahbu1PDFNoiseSuspectFlgL;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH       = (int32_t)lsahbu1PDTNoiseSuspectFlgH;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgL       = (int32_t)lsahbu1PDTNoiseSuspectFlgL;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH = (int32_t)lsahbu1PSPNoiseSuspectFlgH;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL = (int32_t)lsahbu1PSPNoiseSuspectFlgL;
#endif

    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigTag                    = (int32_t)lsahbu8PDFSigTag;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigTag                    = (int32_t)lsahbu8PDTSigTag;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSigTag                   = (int32_t)lsahbu8PedalSigTag;

    Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal=lsahbs16PedalTravelFilt;

    DETsrcbus.EstimdPedlTrvlUsingPedlSimrP=lsahbs16PedalTravelByPS;

}
static void LDIDB_vEstimPedlTrvlFromPedlSimrP(void)
{
	uint8_t u8PSpresModelFiltCoeff ;

	lsahbs16PedalTravelByPSold = lsahbs16PedalTravelByPS;


	u8PSpresModelFiltCoeff = 11 ;

	lsahbs16PSpresModel     = L_s16Lpf1Int( GetDecidePedlTrvl.lsahbs16PSpresFilt_1_100Bar, lsahbs16PSpresModel, u8PSpresModelFiltCoeff ) ;
	lsahbs16PedalTravelByPS = L_s16IInter9Point( lsahbs16PSpresModel , S16_PSP_PRESS_1P, S16_PSP_PEDAL_1P,
	                                                                   S16_PSP_PRESS_2P, S16_PSP_PEDAL_2P,
	                                                                   S16_PSP_PRESS_3P, S16_PSP_PEDAL_3P,
	                                                                   S16_PSP_PRESS_4P, S16_PSP_PEDAL_4P,
	                                                                   S16_PSP_PRESS_5P, S16_PSP_PEDAL_5P,
	                                                                   S16_PSP_PRESS_6P, S16_PSP_PEDAL_6P,
	                                                                   S16_PSP_PRESS_7P, S16_PSP_PEDAL_7P,
	                                                                   S16_PSP_PRESS_8P, S16_PSP_PEDAL_8P,
	                                                                   S16_PSP_PRESS_9P, S16_PSP_PEDAL_9P );

	lsahbs16PedalTravelByPS = L_s16LimitMinMax(lsahbs16PedalTravelByPS, S16_TRAVEL_0_MM, S16_PSP_PEDAL_9P);
}

#if   __AHB_PEDAL_MODEL_CHECK==1
static void LDIDB_vChkPedlSigRliaby(void)
{
/*=============================================================================================*/
/*===========Raw Signal Low-pass Filter========================================================*/
/*=============================================================================================*/

#if __USE_LPF_SIGNAL_FOR_PD_MODEL == ENABLE
	{
   		lsahbs16PDTLowPassFilt = L_s16Lpf1Int(GetDecidePedlTrvl.lsahbs16PDTRaw, lsahbs16PDTLowPassFilt, U8_LPF_10HZ_128G);

   		lsahbs16PDFLowPassFilt = L_s16Lpf1Int(GetDecidePedlTrvl.lsahbs16PDFRaw, lsahbs16PDFLowPassFilt, U8_LPF_10HZ_128G);

   		lsahbs16PSPLowPassFilt = lsahbs16PedalTravelByPS;
	}
#else
	{
   		lsahbs16PDTLowPassFilt = lsahbs16PDTRaw;

   		lsahbs16PDFLowPassFilt = lsahbs16PDFRaw;

   		lsahbs16PSPLowPassFilt = lsahbs16PedalTravelByPS;
	}
#endif /*__USE_LPF_SIGNAL_FOR_PD_MODEL == ENABLE */

/*=============================================================================================*/
/*===========Cycle=============================================================================*/
/*=============================================================================================*/

	if (lsahbu8ChkModelReliabilityCnt==0)
	{
		lsahbu8PDTThresOverCnt=0;
		lsahbu8PDFThresOverCnt=0;
		lsahbu8PSPThresOverCnt=0;
		lsahbu8PDTThresOverCntH=0;
		lsahbu8PDFThresOverCntH=0;
		lsahbu8PSPThresOverCntH=0;

		lsahbu8ChkModelReliabilityCnt = lsahbu8ChkModelReliabilityCnt+1;
	}
	else if (lsahbu8ChkModelReliabilityCnt == 1)
	{
		lsahbu8PDTThresOverCnt=0;
		lsahbu8PDFThresOverCnt=0;
		lsahbu8PSPThresOverCnt=0;
		lsahbu8PDTThresOverCntH=0;
		lsahbu8PDFThresOverCntH=0;
		lsahbu8PSPThresOverCntH=0;

		LSAHB_vCntPedalNoise();

		lsahbu8ChkModelReliabilityCnt=lsahbu8ChkModelReliabilityCnt+1;
	}
	else if (lsahbu8ChkModelReliabilityCnt<=U8_PEDAL_CHECK_PERIOD)
	{
		LSAHB_vCntPedalNoise();

		if (lsahbu8ChkModelReliabilityCnt==U8_PEDAL_CHECK_PERIOD)
		{
            LSAHB_vDetectPedalNoise();
            LSAHB_vCallNoiseSuspect();

			lsahbu8ChkModelReliabilityCnt=1;
		}
		else
		{
			lsahbu8ChkModelReliabilityCnt=lsahbu8ChkModelReliabilityCnt+1;
		}

	}
	else
	{
		lsahbu8ChkModelReliabilityCnt=1;
	}
/*=============================================================================================*/
/*===========Update of Old Values =============================================================*/
/*=============================================================================================*/
	lsahbs16PDTLowPassFiltOld=lsahbs16PDTLowPassFilt;
	lsahbs16PDFLowPassFiltOld=lsahbs16PDFLowPassFilt;
	lsahbs16PSPLowPassFiltOld=lsahbs16PSPLowPassFilt;
	lsahbs8PDTsgnOld=lsahbs8PDTsgn;
	lsahbs8PDFsgnOld=lsahbs8PDFsgn;
	lsahbs8PSPsgnOld=lsahbs8PSPsgn;
}

/* Code Metric LSAHB_vCntPedalNoise					STCYC(22), STPTH(5832)*/
static void LSAHB_vCntPedalNoise(void)
{
	lsahbs16PDTDiff=lsahbs16PDTLowPassFilt-lsahbs16PDTLowPassFiltOld;
	lsahbs16PDFDiff=lsahbs16PDFLowPassFilt-lsahbs16PDFLowPassFiltOld;
	lsahbs16PSPDiff=lsahbs16PSPLowPassFilt-lsahbs16PSPLowPassFiltOld;

	lsahbs16PDTDiff=L_s16LimitMinMax(lsahbs16PDTDiff,-S16_TRAVEL_100_MM,S16_TRAVEL_100_MM);
	lsahbs16PDFDiff=L_s16LimitMinMax(lsahbs16PDFDiff,-S16_TRAVEL_100_MM,S16_TRAVEL_100_MM);
	lsahbs16PSPDiff=L_s16LimitMinMax(lsahbs16PSPDiff,-S16_TRAVEL_100_MM,S16_TRAVEL_100_MM);

/*********** PDT****************************************************/
	if (lsahbs16PDTDiff>0)
	{
		lsahbs8PDTsgn=1;
	}
	else if (lsahbs16PDTDiff<0)
	{
		lsahbs8PDTsgn=-1;
	}
	else
	{
		lsahbs8PDTsgn=0;
	}
	lsahbs8PDTsgnMulti=lsahbs8PDTsgn*lsahbs8PDTsgnOld;

	if (lsahbs8PDTsgnMulti<=0)
	{
		lsahbs16PDTMaxVel = L_s16Abs(lsahbs16PDTDiffTemp);
		if (lsahbs16PDTMaxVel>=U16_MAX_VEL_THRES_LV3)
		{
			lsahbu8PDTThresOverCnt=lsahbu8PDTThresOverCnt+3;
			lsahbu8PDTThresOverCntH=lsahbu8PDTThresOverCntH+3;
		}
		else if (lsahbs16PDTMaxVel>=U16_MAX_VEL_THRES_LV2)
		{
			lsahbu8PDTThresOverCnt=lsahbu8PDTThresOverCnt+2;
			lsahbu8PDTThresOverCntH=lsahbu8PDTThresOverCntH+2;
		}
		else if (lsahbs16PDTMaxVel>=U16_MAX_VEL_THRES_LV1)
		{
			lsahbu8PDTThresOverCnt=lsahbu8PDTThresOverCnt+1;
		}
		else
		{
			;
		}
		lsahbs16PDTDiffTemp = lsahbs16PDTDiff;
	}
	else
	{
		if (L_s16Abs(lsahbs16PDTDiffTemp)<=L_s16Abs(lsahbs16PDTDiff))
		{
			lsahbs16PDTDiffTemp=lsahbs16PDTDiff;
		}
		else
		{
			;
		}
	}

/****** PDF	****************************************************/
	if (lsahbs16PDFDiff>0)
	{
		lsahbs8PDFsgn=1;
	}
	else if (lsahbs16PDFDiff<0)
	{
		lsahbs8PDFsgn=-1;
	}
	else
	{
		lsahbs8PDFsgn=0;
	}
	lsahbs8PDFsgnMulti=lsahbs8PDFsgn*lsahbs8PDFsgnOld;

	if (lsahbs8PDFsgnMulti<=0)
	{
		lsahbs16PDFMaxVel = L_s16Abs(lsahbs16PDFDiffTemp);
		if (lsahbs16PDFMaxVel>=U16_MAX_VEL_THRES_LV3)
		{
			lsahbu8PDFThresOverCnt=lsahbu8PDFThresOverCnt+3;
			lsahbu8PDFThresOverCntH=lsahbu8PDFThresOverCntH+3;
		}
		else if (lsahbs16PDFMaxVel>=U16_MAX_VEL_THRES_LV2)
		{
			lsahbu8PDFThresOverCnt=lsahbu8PDFThresOverCnt+2;
			lsahbu8PDFThresOverCntH=lsahbu8PDFThresOverCntH+2;
		}
		else if (lsahbs16PDFMaxVel>=U16_MAX_VEL_THRES_LV1)
		{
			lsahbu8PDFThresOverCnt=lsahbu8PDFThresOverCnt+1;
		}
		else
		{
			;
		}

		lsahbs16PDFDiffTemp = lsahbs16PDFDiff;
	}
	else
	{
		if (L_s16Abs(lsahbs16PDFDiffTemp)<=L_s16Abs(lsahbs16PDFDiff))
		{
			lsahbs16PDFDiffTemp=lsahbs16PDFDiff;
		}
		else
		{
			;
		}
	}

/****** PSP	****************************************************/
	if (lsahbs16PSPDiff>0)
	{
		lsahbs8PSPsgn=1;
	}
	else if (lsahbs16PSPDiff<0)
	{
		lsahbs8PSPsgn=-1;
	}
	else
	{
		lsahbs8PSPsgn=0;
	}
	lsahbs8PSPsgnMulti=lsahbs8PSPsgn*lsahbs8PSPsgnOld;

	if (lsahbs8PSPsgnMulti<=0)
	{
		lsahbs16PSPMaxVel = L_s16Abs(lsahbs16PSPDiffTemp);
		if (lsahbs16PSPMaxVel>=U16_MAX_VEL_THRES_LV3)
		{
			lsahbu8PSPThresOverCnt=lsahbu8PSPThresOverCnt+3;
			lsahbu8PSPThresOverCntH=lsahbu8PSPThresOverCntH+3;
		}
		else if (lsahbs16PSPMaxVel>=U16_MAX_VEL_THRES_LV2)
		{
			lsahbu8PSPThresOverCnt=lsahbu8PSPThresOverCnt+2;
			lsahbu8PSPThresOverCntH=lsahbu8PSPThresOverCntH+2;
		}
		else if (lsahbs16PSPMaxVel>=U16_MAX_VEL_THRES_LV1)
		{
			lsahbu8PSPThresOverCnt=lsahbu8PSPThresOverCnt+1;
		}
		else
		{
			;
		}

		lsahbs16PSPDiffTemp = lsahbs16PSPDiff;
	}
	else
	{
		if (L_s16Abs(lsahbs16PSPDiffTemp)<=L_s16Abs(lsahbs16PSPDiff))
		{
			lsahbs16PSPDiffTemp=lsahbs16PSPDiff;
		}
		else
		{
			;
		}
	}

}

/* Code Metric LSAHB_vDetectPedalNoise					STCYC(19), STPTH(4096)*/
static void LSAHB_vDetectPedalNoise(void)
{
/********************************PDT******************************************************************/
	if (lsahbu8PDTThresOverCntH>=U8_NOISE_DETECT_THRES_H)
	{
		if (lsahbu8PDTSusHCnt<(U8_HCNT_MAX-1))
		{
			lsahbu8PDTSusHCnt=lsahbu8PDTSusHCnt+2;
		}
		else
		{
			lsahbu8PDTSusHCnt=U8_HCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PDTSusHCnt>U8_HCNT_MIN))
		{
			lsahbu8PDTSusHCnt=lsahbu8PDTSusHCnt-1;
		}
		else
		{
			;
		}
	}
	if (lsahbu8PDTThresOverCnt>=U8_NOISE_DETECT_THRES_L)
	{
		if (lsahbu8PDTSusLCnt<(U8_LCNT_MAX-1))
		{
			lsahbu8PDTSusLCnt=lsahbu8PDTSusLCnt+2;
		}
		else
		{
			lsahbu8PDTSusLCnt=U8_LCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PDTSusLCnt>U8_LCNT_MIN))
		{
			lsahbu8PDTSusLCnt=lsahbu8PDTSusLCnt-1;
		}
		else
		{
			;
		}
	}
/*************************************PDF********************************************************/
	if (lsahbu8PDFThresOverCntH>=U8_NOISE_DETECT_THRES_H)
	{
		if (lsahbu8PDFSusHCnt<(U8_HCNT_MAX-1))
		{
			lsahbu8PDFSusHCnt=lsahbu8PDFSusHCnt+2;
		}
		else
		{
			lsahbu8PDFSusHCnt=U8_HCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PDFSusHCnt>U8_HCNT_MIN))
		{
			lsahbu8PDFSusHCnt=lsahbu8PDFSusHCnt-1;
		}
		else
		{
			;
		}
	}
	if (lsahbu8PDFThresOverCnt>=U8_NOISE_DETECT_THRES_L)
	{
		if (lsahbu8PDFSusLCnt<(U8_LCNT_MAX-1))
		{
			lsahbu8PDFSusLCnt=lsahbu8PDFSusLCnt+2;
		}
		else
		{
			lsahbu8PDFSusLCnt=U8_LCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PDFSusLCnt>U8_LCNT_MIN))
		{
			lsahbu8PDFSusLCnt=lsahbu8PDFSusLCnt-1;
		}
		else
		{
			;
		}
	}

/*****************************************PSP********************************************************/
	if (lsahbu8PSPThresOverCntH>=U8_NOISE_DETECT_THRES_H)
	{
		if (lsahbu8PSPSusHCnt<(U8_HCNT_MAX-1))
		{
			lsahbu8PSPSusHCnt=lsahbu8PSPSusHCnt+2;
		}
		else
		{
			lsahbu8PSPSusHCnt=U8_HCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PSPSusHCnt>U8_HCNT_MIN))
		{
			lsahbu8PSPSusHCnt=lsahbu8PSPSusHCnt-1;
		}
		else
		{
			;
		}
	}
	if (lsahbu8PSPThresOverCnt>=U8_NOISE_DETECT_THRES_L)
	{
		if (lsahbu8PSPSusLCnt<(U8_LCNT_MAX-1))
		{
			lsahbu8PSPSusLCnt=lsahbu8PSPSusLCnt+2;
		}
		else
		{
			lsahbu8PSPSusLCnt=U8_LCNT_MAX;
		}
	}
	else
	{
		if ((lsahbs16PedalTravelByPS>S16_SUSCNT_DOWN_CONDITION)&&(lsahbu8PSPSusLCnt>U8_LCNT_MIN))
		{
			lsahbu8PSPSusLCnt=lsahbu8PSPSusLCnt-1;
		}
		else
		{
			;
		}
	}
}

/* Code Metric LSAHB_vCallNoiseSuspect					STCYC(13), STPTH(729)*/
static void LSAHB_vCallNoiseSuspect(void)
{
/************************PDT*************************************************************************/
	if (lsahbu8PDTSusHCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PDTNoiseSuspectFlgH=1;
	}
	else if ((lsahbu8PDTSusHCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
	{
		lsahbu1PDTNoiseSuspectFlgH=0;
	}
	else {}

	if (lsahbu8PDTSusLCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PDTNoiseSuspectFlgL=1;
	}
	else if ((lsahbu8PDTSusLCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
    {
		lsahbu1PDTNoiseSuspectFlgL=0;
    }
    else {}
/***********************************PDF*************************************************************/
	if (lsahbu8PDFSusHCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PDFNoiseSuspectFlgH=1;
	}
	else if ((lsahbu8PDFSusHCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
	{
		lsahbu1PDFNoiseSuspectFlgH=0;
	}
	else {}

	if (lsahbu8PDFSusLCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PDFNoiseSuspectFlgL=1;
	}
	else if ((lsahbu8PDFSusLCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
    {
		lsahbu1PDFNoiseSuspectFlgL=0;
    }
    else {}
/*********************************PSP***************************************************************/
	if (lsahbu8PSPSusHCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PSPNoiseSuspectFlgH=1;
	}
	else if ((lsahbu8PSPSusHCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
	{
		lsahbu1PSPNoiseSuspectFlgH=0;
	}
	else {}

	if (lsahbu8PSPSusLCnt>=U8_SUS_ON_CNT)
	{
		lsahbu1PSPNoiseSuspectFlgL=1;
	}
	else if ((lsahbu8PSPSusLCnt==U8_SUS_OFF_CNT)&&(lsahbs16PedalTravelByPS<=S16_SUS_OFF_CONDITION))
    {
		lsahbu1PSPNoiseSuspectFlgL=0;
    }
    else {}
}
#endif


/* Code Metric LDIDB_SelPedlInpSig						STCYC(31), STPTH(31104), STMIF(6)*/
static void LDIDB_SelPedlInpSig(void)
{
	uint8_t lsahbu8PedalSigTagOldTemp;

	lsahbu8PedalSigTagOldTemp = lsahbu8PedalSigTag;

	if(GetDecidePedlTrvl.fsu1PdtSenFaultDet==1)
	{
	    lsahbu8PDTSigTag = 70 ;
	}

	else if(((GetDecidePedlTrvl.fdu1PdtSigSusSet==1) /*||(fcu1PdtOffsetSusSet==1)*/) || (lsahbu1PDTNoiseSuspectFlgH==1) )

	{
		lsahbu8PDTSigTag = 40 ;
	}
	else if( lsahbu1PDTNoiseSuspectFlgL==1 )
	{
		lsahbu8PDTSigTag = 23 ;
	}
	else
	{
		lsahbu8PDTSigTag = 10 ;
	}


	if(GetDecidePedlTrvl.fsu1PdfSenFaultDet==1)

	{
	    lsahbu8PDFSigTag = 80 ;
	}

    else if( ((GetDecidePedlTrvl.fdu1PdfSigSusSet==1)/*||(fcu1PdfOffsetSusSet==1)*/) || (lsahbu1PDFNoiseSuspectFlgH==1) )

	{
		lsahbu8PDFSigTag = 50 ;
	}
	else if(lsahbu1PDFNoiseSuspectFlgL==1)
	{
		lsahbu8PDFSigTag = 27 ;
	}
	else
	{
		lsahbu8PDFSigTag = 20 ;
	}

	if( (GetDecidePedlTrvl.fsu1PSPSenFaultDet==1) /*|| (fsu1CvvSolFaultDet==1)*/ )
	{
	    lsahbu8PSPSigTag = 90 ;
	}
	else if((GetDecidePedlTrvl.fu1PspSusDet==1) || (lsahbu1PSPNoiseSuspectFlgH == 1) || (lsahbu1PSPNoiseSuspectFlgL == 1) )
	{
		lsahbu8PSPSigTag = 60 ;
	}
	else
	{
		lsahbu8PSPSigTag = 30 ;
	}

	#if defined __AHB_RIG_TEST
		lsahbu8PDTSigTag = 10 ;
	#endif

	if(lsahbu8PDTSigTag < lsahbu8PDFSigTag)
	{
		if(lsahbu8PDTSigTag < lsahbu8PSPSigTag)
		{
			lsahbu8PedalInfoSigTagPre = lsahbu8PDTSigTag ;

			if(lsahbu8PedalInfoSigTagPre >= 70 )
			{
                lsahbu8PedalSigTagPre          =  8 ;
			}
			else if(lsahbu8PedalInfoSigTagPre >= 40 )
			{
                lsahbu8PedalSigTagPre          =  5 ;
			}
			else
			{
                lsahbu8PedalSigTagPre          =  0 ;
			}
		}
		else
		{
			lsahbu8PedalInfoSigTagPre = lsahbu8PSPSigTag ;

			if(lsahbu8PedalInfoSigTagPre >= 70 )
			{
                lsahbu8PedalSigTagPre          =  8;
			}
			else if(lsahbu8PedalInfoSigTagPre >=  40 )
			{

                lsahbu8PedalSigTagPre          =  7;
			}
			else
			{
                lsahbu8PedalSigTagPre          =  2;
			}
		}
	}
	else
	{
		if(lsahbu8PDFSigTag < lsahbu8PSPSigTag)
		{
			lsahbu8PedalInfoSigTagPre = lsahbu8PDFSigTag;

			if(lsahbu8PedalInfoSigTagPre >= 70 )
			{
                lsahbu8PedalSigTagPre          =  8 ;
			}
			else if(lsahbu8PedalInfoSigTagPre >= 40 )
			{
                lsahbu8PedalSigTagPre          =  6 ;
			}
			else
			{
                lsahbu8PedalSigTagPre          =  1 ;
			}

		}
		else
		{
			lsahbu8PedalInfoSigTagPre = lsahbu8PSPSigTag ;

			if(lsahbu8PedalInfoSigTagPre >= 70 )
			{
                lsahbu8PedalSigTagPre          =  8;
			}
			else if(lsahbu8PedalInfoSigTagPre >=  40 )
			{
                lsahbu8PedalSigTagPre          =  7;
			}
			else
			{
                lsahbu8PedalSigTagPre          =  2;
			}
		}
	}

	/*---
	 * if AHB is on condition and only noise suspect on ,
	 * then pedal sig tag does not change ---*/
	if(GetDecidePedlTrvl.lcahbu1AHBOn == 1)
	{
		if(lsahbu8PedalInfoSigTagPre < 40)
		{
			if(lsahbu1AHBonStart==0)
			{
		        lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		        lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
				lsahbu1AHBonStart      = 1;

				lsahbu8SelectedPedal   = lsahbu8PedalSigTag;
			}
			else
			{
                if(lsahbu8SelectedPedal==0)     /*--- PDT selected ----*/
                {
                    if(lsahbu8PDTSigTag >= 40 )
                    {
		                lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		                lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
		                lsahbu1AHBonStart      = 0;
                    }
                    else
                    {
                    	;
                    }
                }
                else if(lsahbu8SelectedPedal==1)
                {
                    if(lsahbu8PDFSigTag >= 40 )
                    {
		                lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		                lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
		                lsahbu1AHBonStart      = 0;
                    }
                    else
                    {
                    	;
                    }
                }
                else
                {
                    if(lsahbu8PSPSigTag >= 40 )
                    {
		                lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		                lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
		                lsahbu1AHBonStart      = 0;
                    }
                    else
                    {
                    	;
                    }
                }
			}
		}
		else
		{
		    lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		    lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
		    lsahbu1AHBonStart      = 0;
		}
	}
	else
	{
		lsahbu8PedalSigTag     = lsahbu8PedalSigTagPre;
		lsahbu8PedalInfoSigTag = lsahbu8PedalInfoSigTagPre;
		lsahbu1AHBonStart      = 0;
	}


	/*-- Final Signal Input ---*/
    if(lsahbu8PedalInfoSigTag == lsahbu8PDTSigTag )
    {
        lsahbs16PedalTravelInp = GetDecidePedlTrvl.lsahbs16PDTFilt;
        lsahbs16PedalTravelRaw = GetDecidePedlTrvl.lsahbs16PDTRaw;
    }
    else if(lsahbu8PedalInfoSigTag == lsahbu8PDFSigTag)
    {
        /* 2012_SWD_PSP */
        lsahbs16PedalTravelInp = GetDecidePedlTrvl.lsahbs16PDFFilt;
        /* 2012_SWD_PSP */


        lsahbs16PedalTravelRaw =  GetDecidePedlTrvl.lsahbs16PDFRaw;
    }
    else
    {
        lsahbs16PedalTravelInp = lsahbs16PedalTravelByPS;
        lsahbs16PedalTravelRaw = lsahbs16PedalTravelByPS;
    }

    if(lsahbu8PedalSigTag == lsahbu8PedalSigTagOldTemp)
    {
        lsahbu1PedalSigChg = 0;
    }
    else
    {
    	lsahbu1PedalSigChg = 1;
    }
}
/* Code Metric LDIDB_vDetFinalPedlTravl				STCYC(15), STMIF(7)*/
static void  LDIDB_vDetFinalPedlTravl(void)
{
	uint8_t u8PedalFiltCoeff;
	uint8_t u8PedalFiltCoeffFinal;

	lsahbs16PedalTravelFiltOld = lsahbs16PedalTravelFilt;

	if(GetDecidePedlTrvl.lsahbs16SystemOnCnt<=U8_T_50_MS)
	{
		/*----- Pedal Travel -----*/
		if( lsahbu8PedalSigTag==0 )
		{
            lsahbs16PedalTravelFilt = GetDecidePedlTrvl.lsahbs16PDTFilt;
        }
        else if(lsahbu8PedalSigTag==1)
        {
            /* 2012_SWD_PSP */
        	lsahbs16PedalTravelFilt = GetDecidePedlTrvl.lsahbs16PDFFilt;
            /* 2012_SWD_PSP */
        }
        else if(lsahbu8PedalSigTag==2)
        {
        	lsahbs16PedalTravelFilt = lsahbs16PedalTravelByPS;
        }
        /*
        else if(lsahbu8PedalSigTag==3)
        {
        	lsahbs16PedalTravelFilt =cu16PdtOffset;
        }
        else if(lsahbu8PedalSigTag==4)
        {
        	lsahbs16PedalTravelFilt = lsahbs16PedalTravelByPS;
        }
        */
        else if(lsahbu8PedalSigTag==5)
        {
        	lsahbs16PedalTravelFilt =GetDecidePedlTrvl.lsahbs16PDTFilt;
        }
        else if(lsahbu8PedalSigTag==6)
        {
        	lsahbs16PedalTravelFilt = GetDecidePedlTrvl.lsahbs16PDFFilt;
          }
        else if(lsahbu8PedalSigTag==7)
        {
        	lsahbs16PedalTravelFilt =lsahbs16PedalTravelByPS;
        }
        else
        {
        	lsahbs16PedalTravelFilt = GetDecidePedlTrvl.lsahbs16PDTFilt ;
        }
	}
	else
	{
		/*----- Pedal Travel  Change Condition -----*/
		if(lsahbu1PedalFiltCoeffChg==1)
		{
			if(((lsahbu8PedalSigTag==2)||(lsahbu8PedalSigTag==7))
			        /*&&((fcu1PdtOffsetSusSet==1)||(fcu1PdfOffsetSusSet==1))*/)
			    /* (PDT or PDF -> PSP) only for Offset Suspect */

			{
			    u8PedalFiltCoeff = U8_LPF_1_5HZ_128G;     /* 1.5Hz  */

			    if(lsahbu1PedalSigChg==1)
			    {
			    	lsahbu8PedalFiltChgCNT=0;
			    }
			    else
			    {
			    	lsahbu8PedalFiltChgCNT=lsahbu8PedalFiltChgCNT+1;

			    	if(lsahbu8PedalFiltChgCNT >= U8_T_1000_MS)
			    	{
			    		lsahbu1PedalFiltCoeffChg = 0;
			    		lsahbu8PedalFiltChgCNT   = 0;
			    	}
			    	else
			    	{
			    		;
			    	}
			    }

			    u8PedalFiltCoeffFinal =
			            (uint8_t)( L_s16LimitMinMax(((int16_t)u8PedalFiltCoeff) + ((int16_t)lsahbu8PedalFiltChgCNT/2), 0, 128));

			    lsahbs16PedalTravelFilt =
			            L_s16Lpf1Int( lsahbs16PedalTravelInp, lsahbs16PedalTravelFilt, u8PedalFiltCoeffFinal  );

			}
		    /* 2012_SWD_PSP */
			else
			{
				u8PedalFiltCoeff = U8_LPF_3HZ_128G;

				if(lsahbu1PedalSigChg==1)
				{
					lsahbu8PedalFiltChgCNT=0;
				}
				else
				{
					lsahbu8PedalFiltChgCNT=lsahbu8PedalFiltChgCNT+1;

					if(lsahbu8PedalFiltChgCNT >= U8_T_500_MS)
					{
						lsahbu1PedalFiltCoeffChg = 0;
						lsahbu8PedalFiltChgCNT   = 0;
					}
					else
					{
						;
					}
				}

				u8PedalFiltCoeffFinal =
				        (uint8_t)(L_s16LimitMinMax(((int16_t)u8PedalFiltCoeff) + ((int16_t)lsahbu8PedalFiltChgCNT), 0, 128) );

				lsahbs16PedalTravelFilt =
				        L_s16Lpf1Int( (lsahbs16PedalTravelInp), lsahbs16PedalTravelFilt, u8PedalFiltCoeffFinal  );

			}
		}
		else
		{
			lsahbu8PedalFiltChgCNT   = 0;

			if(lsahbu1PedalSigChg==1)
			{
				lsahbu1PedalFiltCoeffChg=1;
			}
			else
			{
				lsahbs16PedalTravelFilt =  lsahbs16PedalTravelInp ;
			}
		}
	}
}

#define DET_5MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
