/**
 * @defgroup LDIDB_CalcSensorSignalChangeRate LDIDB_CalcSignalChangeRate
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_CalcSignalChangeRate.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/
/*==============================================================================
 *  Code Metric
 *  Function								Metric
 *  LDIDB_vCalcP5msSigRate					STPTH(96)
 *
 =============================================================================*/
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_CalcSignalChangeRate.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


#define lsahbu16SystemOnCnt         DETsrcbus.SystemOnCnt
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    int16_t lss16PedalTravelFilt;

}GetSigRateCalc_t;

typedef struct
{
    int16_t s16PressFilt;
    int16_t s16PressFilt_1_100Bar;
    int16_t s16PressRate;
    int16_t s16PressRateBarPSec;
    int16_t s16PressRate10ms;
    int16_t s16PressRate5ms;
    int16_t s16PressRate10msX10;

    int16_t s16PressBuff[5];
    int16_t s16PressBuffX10[5];
    int16_t s16PressSum;
    int16_t s16PressAvg;
    int16_t s16PressAvgBuff[5];
    int16_t s16PressRateX10;
    uint8_t u8PressAvgCnt;
    uint8_t u8PressRateCnt;
    uint8_t u8PressBuffCnt;

}press_rate_data_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define DET_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC int16_t ldahbs16PedalTravelRate;
FAL_STATIC int16_t ldahbs16PedalTravelRateMmPSec;

FAL_STATIC int16_t ldahbs16PedalTravelSum;
FAL_STATIC int16_t ldahbs16PedalTravelBuff[5];
FAL_STATIC int16_t ldahbs16PedalTravelAvg;
FAL_STATIC int16_t ldahbs16PedalTravelRateBuff[5];
FAL_STATIC int16_t ldahbs16PedalTravelBuffX10[5];
FAL_STATIC int16_t ldahbs16PedalTravelRateX10;
FAL_STATIC uint8_t ldahbu8PedalTravelRateCnt;
FAL_STATIC uint8_t ldahbu8PedalTravelAvgCnt;
FAL_STATIC uint8_t ldahbu8PedalTravelBuffCnt;

FAL_STATIC GetSigRateCalc_t GetSigRateCalc;
FAL_STATIC press_rate_data_t *pPress_Rate;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
FAL_STATIC press_rate_data_t PistP_rate;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
FAL_STATIC press_rate_data_t PrimP_rate;
FAL_STATIC press_rate_data_t SecdP_rate;
#else
#endif

#define lss16MCprsRate1mTrimedAvg           pPress_Rate->s16PressRate1msTrimedAvg
#define lss16MCprsRate1msAvg                pPress_Rate->s16PressRate1msAvg

#define lsahbs16MCPressBuff                 pPress_Rate->s16PressBuff
#define ldahbs16MCPressBuffX10              pPress_Rate->s16PressBuffX10
#define lsahbs16MCPressSum                  pPress_Rate->s16PressSum
#define lsahbs16MCPressAvg                  pPress_Rate->s16PressAvg
#define lsahbs16MCPressAvgBuff              pPress_Rate->s16PressAvgBuff
#define lsahbs16MCPressRateX10              pPress_Rate->s16PressRateX10
#define lsahbu8MCPressAvgCnt                pPress_Rate->u8PressAvgCnt
#define lsahbu8MCPressRateCnt               pPress_Rate->u8PressRateCnt
#define ldahbu8MCPressBuffCnt               pPress_Rate->u8PressBuffCnt
#define lsahbs16MCPressRate                 pPress_Rate->s16PressRate

#define lsahbs16MCPressRateBarPSec          pPress_Rate->s16PressRateBarPSec
#define s16MCpresFilt                  pPress_Rate->s16PressFilt

#define lsahbs16MCPressRate10ms             pPress_Rate->s16PressRate10ms
#define lsahbs16MCPressRate5ms              pPress_Rate->s16PressRate5ms
#define lsahbs16MCPressRate10msX10          pPress_Rate->s16PressRate10msX10

#define lsahbs16MCpresFilt_1_100Bar         pPress_Rate->s16PressFilt_1_100Bar


#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/

#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

static void LDIDB_vCalcPedlTravelRate(void);
static void LDIDB_vCalcPSigRate(void);
static void LDIDB_vCalcP5msSigRate(void);
static void LDIDB_vInpSigIfForSigRateCaln(void);
static void LDIDB_vOutpSigIfForSigRateCaln(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LDIDB_vCallSigRateCalcn(void)
{
    LDIDB_vInpSigIfForSigRateCaln();
    LDIDB_vCalcPedlTravelRate();
    LDIDB_vCalcPSigRate();
    LDIDB_vOutpSigIfForSigRateCaln();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void LDIDB_vInpSigIfForSigRateCaln(void) /*TODO*/
{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    PistP_rate.s16PressFilt                 = (int16_t) (Det_5msCtrlBus.Det_5msCtrlPistPFildInfo.PistPFild/10);/*RTE IF P scale change 0.1bar->0.01bar, Local variable scale 0.1bar*/
    PistP_rate.s16PressFilt_1_100Bar        = (int16_t) Det_5msCtrlBus.Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar;

#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    PrimP_rate.s16PressFilt                 = (int16_t)(Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.PrimCircPFild/10); /*RTE IF P scale change 0.1bar->0.01bar, Local variable scale 0.1bar*/
    PrimP_rate.s16PressFilt_1_100Bar        = (int16_t)Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;

    SecdP_rate.s16PressFilt                 = (int16_t)(Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild/10); /*RTE IF P scale change 0.1bar->0.01bar , Local variable scale 0.1bar*/
    SecdP_rate.s16PressFilt_1_100Bar        = (int16_t)Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
#else
#endif
    GetSigRateCalc.lss16PedalTravelFilt                 =(int16_t) Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal;
}
static void LDIDB_vOutpSigIfForSigRateCaln(void)
{
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate                = ldahbs16PedalTravelRate;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec  = ldahbs16PedalTravelRateMmPSec;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms               = PistP_rate.s16PressRate10ms;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg5ms                = PistP_rate.s16PressRate5ms;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRate                      = PistP_rate.s16PressRate;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec             = PistP_rate.s16PressRateBarPSec;

#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms               = PrimP_rate.s16PressRate10ms;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms                = PrimP_rate.s16PressRate5ms;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRate                      = PrimP_rate.s16PressRate;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec             = PrimP_rate.s16PressRateBarPSec;

    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms               = SecdP_rate.s16PressRate10ms;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPChgDurg5ms                = SecdP_rate.s16PressRate5ms;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPRate                      = SecdP_rate.s16PressRate;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPRateBarPerSec             = SecdP_rate.s16PressRateBarPSec;
#else
#endif
}

static void LDIDB_vCalcPedlTravelRate(void)
{
	int16_t s16FilterInput=0;

/**********************************************************/
/* Update Brake Pedal Position Buffer, Sum for 5scan      */
/**********************************************************/

	ldahbs16PedalTravelSum = (ldahbs16PedalTravelSum - (ldahbs16PedalTravelBuff[ldahbu8PedalTravelAvgCnt])) + GetSigRateCalc.lss16PedalTravelFilt;
    ldahbs16PedalTravelAvg = ldahbs16PedalTravelSum/5;

    ldahbs16PedalTravelBuff[ldahbu8PedalTravelAvgCnt] = GetSigRateCalc.lss16PedalTravelFilt;
    ldahbu8PedalTravelAvgCnt = ldahbu8PedalTravelAvgCnt + 1;

    if(ldahbu8PedalTravelAvgCnt>=5)
    {
        ldahbu8PedalTravelAvgCnt=0;
    }
    else
    {
    	;
    }

/**********************************************************/
/* Pedal Position change rate (1 = 0.1mm/25ms = 4mm/s)    */
/**********************************************************/

    ldahbs16PedalTravelRate = L_s16Lpf1Int((ldahbs16PedalTravelAvg - ldahbs16PedalTravelRateBuff[ldahbu8PedalTravelRateCnt]), ldahbs16PedalTravelRate, U8_LPF_7HZ_128G);
    ldahbs16PedalTravelRateBuff[ldahbu8PedalTravelRateCnt] = ldahbs16PedalTravelAvg;

    ldahbu8PedalTravelRateCnt = ldahbu8PedalTravelRateCnt + 1;

    if(ldahbu8PedalTravelRateCnt>=5)
    {
        ldahbu8PedalTravelRateCnt = 0;
    }
    else
    {
        ;
    }

/**********************************************************/
/* Pedal Position change rate (1 = 0.01mm/25ms = 0.4mm/s) */
/**********************************************************/
	s16FilterInput = (GetSigRateCalc.lss16PedalTravelFilt*10) - ldahbs16PedalTravelBuffX10[ldahbu8PedalTravelBuffCnt]; /* target press variation for 5scan */

	ldahbs16PedalTravelRateX10 = L_s16Lpf1Int(s16FilterInput, ldahbs16PedalTravelRateX10, U8_LPF_7HZ_128G);          /* 7Hz */

	ldahbs16PedalTravelBuffX10[ldahbu8PedalTravelBuffCnt] = GetSigRateCalc.lss16PedalTravelFilt*10;

    ldahbu8PedalTravelBuffCnt = ldahbu8PedalTravelBuffCnt + 1;

    if(ldahbu8PedalTravelBuffCnt>=5)
    {
        ldahbu8PedalTravelBuffCnt = 0;
    }
    else
    {
        ;
    }

/**********************************************************/
/* Pedal Position change rate (1 = 1mm/s)                 */
/**********************************************************/
	ldahbs16PedalTravelRateMmPSec = (ldahbs16PedalTravelRateX10*2)/5;
}


static void LDIDB_vCalcPSigRate(void)
{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    pPress_Rate=&PistP_rate;

    LDIDB_vCalcP5msSigRate();
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    pPress_Rate=&PrimP_rate;
    LDIDB_vCalcP5msSigRate();

    pPress_Rate=&SecdP_rate;
    LDIDB_vCalcP5msSigRate();
#else
#endif
}

/* Code Metric LDIDB_vCalcP5msSigRate					STPTH(96)*/
static void LDIDB_vCalcP5msSigRate(void)
{
    uint8_t u8MCPressBuffCntBf2scan=0;
    int32_t s32FilterInputTemp = 0;
    int16_t s16FilterInput = 0;

/**********************************************************/
/* Master Chamber Press rate (1 = 0.1bar/25ms = 4bar/s)   */
/**********************************************************/
    lsahbs16MCPressSum = (lsahbs16MCPressSum - (lsahbs16MCPressBuff[lsahbu8MCPressAvgCnt])) + s16MCpresFilt;
    lsahbs16MCPressAvg = lsahbs16MCPressSum/5;

    lsahbs16MCPressBuff[lsahbu8MCPressAvgCnt] = s16MCpresFilt;
    lsahbu8MCPressAvgCnt = lsahbu8MCPressAvgCnt + 1;

    if(lsahbu8MCPressAvgCnt>=5)
    {
        lsahbu8MCPressAvgCnt=0;
    }
    else
    {
        ;
    }

    lsahbs16MCPressRate = L_s16Lpf1Int((lsahbs16MCPressAvg - lsahbs16MCPressAvgBuff[lsahbu8MCPressRateCnt]), lsahbs16MCPressRate, 40);
    lsahbs16MCPressAvgBuff[lsahbu8MCPressRateCnt] = lsahbs16MCPressAvg;

    lsahbu8MCPressRateCnt = lsahbu8MCPressRateCnt + 1;

    if(lsahbu8MCPressRateCnt>=5)
    {
        lsahbu8MCPressRateCnt = 0;
    }
    else
    {
        ;
    }

/**********************************************************/
/* Master Chamber Press rate (1 = 0.01bar/25ms = 0.4bar/s)*/
/**********************************************************/

    lsahbs16MCPressRateX10 = L_s16Lpf1Int((lsahbs16MCpresFilt_1_100Bar - ldahbs16MCPressBuffX10[ldahbu8MCPressBuffCnt]), lsahbs16MCPressRateX10, 28);

    ldahbs16MCPressBuffX10[ldahbu8MCPressBuffCnt] = lsahbs16MCpresFilt_1_100Bar;

    ldahbu8MCPressBuffCnt = ldahbu8MCPressBuffCnt + 1;
    if(ldahbu8MCPressBuffCnt>=5)
    {
        ldahbu8MCPressBuffCnt = 0;
    }
    else
    {
        ;
    }

/**********************************************************/
/* Master Chamber Press rate (1 = 1bar/s)                 */
/**********************************************************/
    lsahbs16MCPressRateBarPSec = (lsahbs16MCPressRateX10*2)/5;

/**********************************************************/
/* Master Chamber Press rate (1 = 0.01bar/5ms*2 = 1bar/s)  */
/**********************************************************/
    if(ldahbu8MCPressBuffCnt>=2)/*3) */
    {
        u8MCPressBuffCntBf2scan = ldahbu8MCPressBuffCnt - 2; /*3;*/
    }
    else
    {
        u8MCPressBuffCntBf2scan = ldahbu8MCPressBuffCnt + 3; /*2;*/
    }

    s32FilterInputTemp = (((int32_t)lsahbs16MCpresFilt_1_100Bar - (int32_t)ldahbs16MCPressBuffX10[u8MCPressBuffCntBf2scan])*10)*2; /* *2 : due to dT change (10ms -> 5ms) */

    if(s32FilterInputTemp >= ASW_S16_MAX)
    {
        s16FilterInput = ASW_S16_MAX;
    }
    else if (s32FilterInputTemp <= ASW_S16_MIN)
    {
        s16FilterInput = ASW_S16_MIN;
    }
    else
    {
        s16FilterInput = (int16_t)s32FilterInputTemp;
    }

    lsahbs16MCPressRate10msX10 = L_s16Lpf1Int(s16FilterInput, lsahbs16MCPressRate10msX10, 28);
    lsahbs16MCPressRate10ms = lsahbs16MCPressRate10msX10/10;

/**********************************************************/
/* Master Chamber Press rate (1 = 0.01bar/5ms/2 = 4bar/s) */
/**********************************************************/
    if(ldahbu8MCPressBuffCnt>=2)
    {
        u8MCPressBuffCntBf2scan = ldahbu8MCPressBuffCnt - 2;
    }
    else
    {
        u8MCPressBuffCntBf2scan = ldahbu8MCPressBuffCnt + 3;
    }

    lsahbs16MCPressRate5ms = (lsahbs16MCpresFilt_1_100Bar - ldahbs16MCPressBuffX10[u8MCPressBuffCntBf2scan])/2;
}
#define DET_5MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
