/**
 * @defgroup LDIDB_DetVehicleState LDIDB_DetVehicleState
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_DetVehicleState.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/
/*==============================================================================
 *  Code Metric
 *  Function								Metric
 *  LDIDB_vDetPedlReldStUsgPedlSig			STCYC(16), STPTH(800)
 *  LDIDB_vDetPanicBrkStUsg5msPedlSig		STMIF(5)
 *  LDIDB_vEstimVehSpd						STCYC(11)
 *
 =============================================================================*/
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_DetVehicleState.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


#define lsahbu16SystemOnCnt         DETsrcbus.SystemOnCnt

#define U8_BRAKE_RELEASE_DCT_CNT_MAX    	U8_T_1000_MS
#define U8_BRAKE_RELEASE_MAINTAIN_TIME  	U8_T_300_MS

#define S16_CREDIBLE_PEDAL_BY_PS        	S16_TRAVEL_5_MM
#define S16_PD_PS_RELEASE_MAINTAIN_TIME 	U8_T_100_MS

#define	S16_PEDAL_RELEASE_THRES	    (-S16_TRAVEL_1_MM)
#define S16_PEDAL_APPLY_THRES	    S16_TRAVEL_1_MM
#define BRAKE_PEDAL_RELEASE	0
#define BRAKE_PEDAL_HOLD	1
#define BRAKE_PEDAL_APPLY	2


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint16_t    VehicleStopFlgOld           :1;
    uint16_t    VehicleStopFlg              :1;
    uint16_t    InitFastPedalApplyFlg       :1;
    uint16_t    PedalReleaseFlg1            :1;
    uint16_t    PedalReleaseFlg2            :1;
    uint16_t    SpeedInvalidFlg             :1;
    uint16_t    DriverIntendToRelBrk        :1;
    uint16_t    PedalReleaseByPSPFlg        :1;
}det_veh_st_bit_t;

typedef struct
{
     int32_t liu1cCF_Hcu_ServiceMod;
     int32_t liu1fFLSpeedInvalid;
     int32_t liu1fFRSpeedInvalid;
     int32_t liu1fRLSpeedInvalid;
     int32_t liu1fRRSpeedInvalid;
     int32_t lsahbs16WheelSpeedFL;
     int32_t lsahbs16WheelSpeedFR;
     int32_t lsahbs16WheelSpeedRL;
     int32_t lsahbs16WheelSpeedRR;
     int32_t lis16VrefESC;
     int32_t lsahbu1BLS;
     int32_t lsahbs16PedalTravelFilt;
     int32_t ldahbs16PedalTravelRateMmPSec;
     int32_t lsahbs16PedalTravelByPS;
     int32_t ldahbs16PedalTravelRate;
}GetDetVehStInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC uint8_t ldahbu8PedalState;

FAL_STATIC int16_t ldahbs16VehicleSpeed;
FAL_STATIC int8_t  ldahbs8FastPedalApplyCnt;
FAL_STATIC uint8_t ldahbu8BrkReleaseIntendCnt;

FAL_STATIC int16_t ldahbs16PedalReleaseDctCnt;

FAL_STATIC int16_t s16WheelSpeedFL;
FAL_STATIC int16_t s16WheelSpeedFR;
FAL_STATIC int16_t s16WheelSpeedRL;
FAL_STATIC int16_t s16WheelSpeedRR;
FAL_STATIC uint8_t ldahbu8VehicleStopCnt;

FAL_STATIC det_veh_st_bit_t det_veh_st_bit;
FAL_STATIC GetDetVehStInfo_t GetDetVehStInfo;

#define ldahbu1VehicleStopFlg           det_veh_st_bit.VehicleStopFlg
#define ldahbu1VehicleStopFlgOld        det_veh_st_bit.VehicleStopFlgOld
#define ldahbu1InitFastPedalApplyFlg    det_veh_st_bit.InitFastPedalApplyFlg
#define ldahbu1PedalReleaseFlg1         det_veh_st_bit.PedalReleaseFlg1
#define u1PedalReleaseFlg2         		det_veh_st_bit.PedalReleaseFlg2
#define u1SpeedInvalidFlg          		det_veh_st_bit.SpeedInvalidFlg
#define u1DriverIntendToRelBrk     		det_veh_st_bit.DriverIntendToRelBrk
#define ldahbu1PedalReleaseByPSPFlg     det_veh_st_bit.PedalReleaseByPSPFlg

#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"
static void LDIDB_CallPedlStDet(void);
static void LDIDB_vDetPanicBrkStUsg5msPedlSig(void);
static void LDIDB_vDetPedlReldStUsgPedlSig(void);
static void LDIDB_vDetDrvrIntendToRelsBrk(void);
static void LDIDB_vDetPedlRelsdStUsgPedlSimrP(void);
static void LDIDB_vEstimVehSpd(void);
static void LDIDB_vDetVehStopSt(void);
static void LSIDB_vInpSigIfForVehStDetn(void);
static void LSIDB_vOutpSigIfForVehStDetn(void);
static void LDIDB_vDetBrkPedlAppldSt(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LDIDB_vCallVehStDet(void)
{
    LSIDB_vInpSigIfForVehStDetn();
    LDIDB_CallPedlStDet();
    LDIDB_vEstimVehSpd();
	LDIDB_vDetVehStopSt();
	LSIDB_vOutpSigIfForVehStDetn();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LSIDB_vInpSigIfForVehStDetn(void)
{
    GetDetVehStInfo.liu1cCF_Hcu_ServiceMod  								= Det_5msCtrlBus.Det_5msCtrlCanRxIdbInfo.HcuServiceMod;
    GetDetVehStInfo.liu1fFLSpeedInvalid     								= Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFL;
    GetDetVehStInfo.liu1fFRSpeedInvalid     								= Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFR;
    GetDetVehStInfo.liu1fRLSpeedInvalid     								= Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRL;
    GetDetVehStInfo.liu1fRRSpeedInvalid     								= Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRR;
    GetDetVehStInfo.lsahbs16WheelSpeedFL    								= Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe;
    GetDetVehStInfo.lsahbs16WheelSpeedFR    								= Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi;
    GetDetVehStInfo.lsahbs16WheelSpeedRL    								= Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
    GetDetVehStInfo.lsahbs16WheelSpeedRR    								= Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
    GetDetVehStInfo.lis16VrefESC            								= Det_5msCtrlBus.Det_5msCtrlVehSpd;

    GetDetVehStInfo.lsahbu1BLS              								= Det_5msCtrlBus.Det_5msCtrlBlsFild;
    GetDetVehStInfo.lsahbs16PedalTravelFilt         						= Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal;
    GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec   						= Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;

    GetDetVehStInfo.lsahbs16PedalTravelByPS         						= DETsrcbus.EstimdPedlTrvlUsingPedlSimrP;
    GetDetVehStInfo.ldahbs16PedalTravelRate         						= Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate;
}
static void LSIDB_vOutpSigIfForVehStDetn(void)
{
    Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStopStFlg           			= (int32_t)ldahbu1VehicleStopFlg;
    Det_5msCtrlBus.Det_5msCtrlVehSpdFild                           			= (int32_t)ldahbs16VehicleSpeed;

    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.BrkPedlStatus      			= (int32_t)ldahbu8PedalState;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg      			= (int32_t)ldahbu1InitFastPedalApplyFlg;

    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1     			= (int32_t)ldahbu1PedalReleaseFlg1;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2     			= (int32_t)u1PedalReleaseFlg2;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk			= (int32_t)u1DriverIntendToRelBrk;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg	= (int32_t)ldahbu1PedalReleaseByPSPFlg;
    DETsrcbus.SpeedInvalidFlg 												= (int32_t)u1SpeedInvalidFlg;
    DETsrcbus.VehSpdFild 													= (int32_t)ldahbs16VehicleSpeed;
}
static void LDIDB_CallPedlStDet(void)
{
    LDIDB_vDetBrkPedlAppldSt();
    LDIDB_vDetPanicBrkStUsg5msPedlSig();
    LDIDB_vDetPedlReldStUsgPedlSig();
    LDIDB_vDetDrvrIntendToRelsBrk();
    LDIDB_vDetPedlRelsdStUsgPedlSimrP();
}

/**********************************************************/
/* Est Pedal State                                        */
/**********************************************************/
static void LDIDB_vDetBrkPedlAppldSt(void)
{
    if (GetDetVehStInfo.ldahbs16PedalTravelRate < S16_PEDAL_RELEASE_THRES)
    {
        ldahbu8PedalState = BRAKE_PEDAL_RELEASE;
    }
    else if (GetDetVehStInfo.ldahbs16PedalTravelRate > S16_PEDAL_APPLY_THRES)
    {
        ldahbu8PedalState = BRAKE_PEDAL_APPLY;
    }
    else
    {
        ldahbu8PedalState = BRAKE_PEDAL_HOLD;
    }
}
/* Code Metric LDIDB_vDetPedlReldStUsgPedlSig			STCYC(16), STPTH(800)*/
static void LDIDB_vDetPedlReldStUsgPedlSig(void)
{
	static uint8_t ldahbu8PedalReleaseCnt1, ldahbu8PedalReleaseCnt2;
	uint8_t u8PdReleaseCntDec;

	u8PdReleaseCntDec = (uint8_t)L_s16IInter2Point((int16_t)GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec,
	                                                              S16_TRAVEL_2_MM, 2,
	                                                              S16_TRAVEL_5_MM, 4);
    if(GetDetVehStInfo.lsahbu1BLS==1)
    {
    	u8PdReleaseCntDec = u8PdReleaseCntDec*2;
    }

	if((GetDetVehStInfo.lsahbs16PedalTravelFilt < S16_AHBON_PD_OFFST_INVALID_WBLS) && (GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec <= 0))
	{
		if(ldahbu8PedalReleaseCnt1 < U8_T_200_MS)
		{
			ldahbu8PedalReleaseCnt1 = ldahbu8PedalReleaseCnt1 + 1;
		}
	}
	else
	{
		if(GetDetVehStInfo.lsahbs16PedalTravelFilt > (S16_AHBON_PD_OFFST_INVALID_WBLS+S16_TRAVEL_1_MM))
		{
			ldahbu8PedalReleaseCnt1 = 0;
		}
		else if(ldahbu8PedalReleaseCnt1 >= u8PdReleaseCntDec)
		{
			ldahbu8PedalReleaseCnt1 = ldahbu8PedalReleaseCnt1 - u8PdReleaseCntDec;
		}
		else
		{
			ldahbu8PedalReleaseCnt1 = 0;
		}
	}

	if((GetDetVehStInfo.lsahbs16PedalTravelFilt < S16_AHBON_PD_OFFST_INVALID) && (GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec <= 0))
	{
		if(ldahbu8PedalReleaseCnt2 < U8_T_200_MS)
		{
			ldahbu8PedalReleaseCnt2 = ldahbu8PedalReleaseCnt2 + 1;
		}
	}
	else
	{
		if(GetDetVehStInfo.lsahbs16PedalTravelFilt > (S16_AHBON_PD_OFFST_INVALID+S16_TRAVEL_1_MM))
		{
			ldahbu8PedalReleaseCnt2 = 0;
		}
		else if(ldahbu8PedalReleaseCnt2 >= u8PdReleaseCntDec)
		{
			ldahbu8PedalReleaseCnt2 = ldahbu8PedalReleaseCnt2 - u8PdReleaseCntDec;
		}
		else
		{
			ldahbu8PedalReleaseCnt2 = 0;
		}
	}

	if(ldahbu1PedalReleaseFlg1 == 0)
	{
		if(ldahbu8PedalReleaseCnt1 >= U8_T_100_MS)
		{
			ldahbu1PedalReleaseFlg1 = 1;
		}
	}
	else
	{
		if(ldahbu8PedalReleaseCnt1 <= 0)
		{
			ldahbu1PedalReleaseFlg1 = 0;
		}
	}

	if(u1PedalReleaseFlg2 == 0)
	{
		if(ldahbu8PedalReleaseCnt2 >= U8_T_100_MS)
		{
			u1PedalReleaseFlg2 = 1;
		}
	}
	else
	{
		if(ldahbu8PedalReleaseCnt2 <= 0)
		{
			u1PedalReleaseFlg2 = 0;
		}
	}
}


static void LDIDB_vDetDrvrIntendToRelsBrk(void)
{
    /*static uint8_t ldahbu8BrkReleaseIntendCnt;*/
    int8_t s8BrakeReleaseCntIncRate=0;
    int8_t s8BrakeReleaseCntDecRate=0;
    uint8_t u8BrkReleaseDctThres=0;

    if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec < -S16_TRAVEL_5_MM)
    {
    	s8BrakeReleaseCntIncRate = (int8_t)L_s16IInter4Point(-(int16_t)(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec),
    	                                                              S16_TRAVEL_5_MM,  1,
    	                                                              S16_TRAVEL_10_MM, 3,
    	                                                              S16_TRAVEL_20_MM, 12,
    	                                                              S16_TRAVEL_50_MM, 30);

        if((ldahbu8BrkReleaseIntendCnt+s8BrakeReleaseCntIncRate) <= U8_BRAKE_RELEASE_DCT_CNT_MAX)
        {
            ldahbu8BrkReleaseIntendCnt = ldahbu8BrkReleaseIntendCnt + (uint8_t)s8BrakeReleaseCntIncRate;
        }
        else
        {
            ldahbu8BrkReleaseIntendCnt = U8_BRAKE_RELEASE_DCT_CNT_MAX;
        }
    }
    else if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec > S16_TRAVEL_0_5_MM)
    {
    	s8BrakeReleaseCntDecRate = (int8_t)L_s16IInter4Point((int16_t)GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec,
    	                                                              S16_TRAVEL_2_MM,  6,
    	                                                              S16_TRAVEL_5_MM,  6,
    	                                                              S16_TRAVEL_10_MM, 12,
    	                                                              S16_TRAVEL_50_MM, 50);

        if(ldahbu8BrkReleaseIntendCnt >= s8BrakeReleaseCntDecRate)
        {
            ldahbu8BrkReleaseIntendCnt = ldahbu8BrkReleaseIntendCnt - (uint8_t)s8BrakeReleaseCntDecRate;
        }
        else
        {
            ldahbu8BrkReleaseIntendCnt = 0;
        }
    }
    else
    {
        ;
    }

    u8BrkReleaseDctThres = (uint8_t)L_s16IInter3Point((int16_t)GetDetVehStInfo.lsahbs16PedalTravelFilt,
                                                      S16_TRAVEL_30_MM, U8_T_300_MS,
                                                      S16_TRAVEL_50_MM, U8_T_500_MS,
                                                      S16_TRAVEL_60_MM, U8_T_1000_MS);

    if(u1DriverIntendToRelBrk == 0)
    {
        if(ldahbu8BrkReleaseIntendCnt >= u8BrkReleaseDctThres)
        {
            u1DriverIntendToRelBrk = 1;
            ldahbu8BrkReleaseIntendCnt = U8_BRAKE_RELEASE_MAINTAIN_TIME;
        }
    }
    else
    {
        if(ldahbu8BrkReleaseIntendCnt <= U8_T_0_MS)
        {
            u1DriverIntendToRelBrk = 0;
        }
    	else if(ldahbu8BrkReleaseIntendCnt > U8_BRAKE_RELEASE_MAINTAIN_TIME)
    	{
    		ldahbu8BrkReleaseIntendCnt = U8_BRAKE_RELEASE_MAINTAIN_TIME;
    	}
    	else
    	{
    		;
    	}
    }
}


static void LDIDB_vDetPedlRelsdStUsgPedlSimrP(void)
{
	static int16_t ldahbs16PedalTravelByPSold=0;
	static int8_t  s8PedalDecWeightValue = 10;
	static int8_t  s8PedalIncWeightValue = -8;
	static int8_t  s8PedalHldWeightValue = -1;
	static int16_t s16PedalReleaseDctThres, s16PedalReleaseResetThres;

	if(GetDetVehStInfo.lsahbs16PedalTravelByPS < S16_CREDIBLE_PEDAL_BY_PS)
	{
		s8PedalIncWeightValue = -15;
	}
	else if(GetDetVehStInfo.lsahbs16PedalTravelByPS < S16_TRAVEL_10_MM)
	{
		s8PedalIncWeightValue = -10;
	}
	else
	{
		;
	}

    if((GetDetVehStInfo.lsahbs16PedalTravelByPS < ldahbs16PedalTravelByPSold) && (GetDetVehStInfo.lsahbs16PedalTravelByPS>=S16_CREDIBLE_PEDAL_BY_PS))
    {
    	ldahbs16PedalReleaseDctCnt = ldahbs16PedalReleaseDctCnt + (int16_t)s8PedalDecWeightValue;
    }
    else if(GetDetVehStInfo.lsahbs16PedalTravelByPS > ldahbs16PedalTravelByPSold)
    {
    	ldahbs16PedalReleaseDctCnt = ldahbs16PedalReleaseDctCnt + (int16_t)s8PedalIncWeightValue;
    }
    else
    {
    	ldahbs16PedalReleaseDctCnt = ldahbs16PedalReleaseDctCnt + (int16_t)s8PedalHldWeightValue;
    }

    ldahbs16PedalReleaseDctCnt = L_s16LimitMinMax(ldahbs16PedalReleaseDctCnt, 0, U16_T_2_S);

    if(ldahbu1PedalReleaseByPSPFlg == 0)
    {
    	s16PedalReleaseDctThres = L_s16IInter3Point((int16_t)GetDetVehStInfo.lsahbs16PedalTravelByPS,
    	                                            S16_TRAVEL_15_MM, U8_T_700_MS,
    	                                            S16_TRAVEL_20_MM, U8_T_1000_MS,
    	                                            S16_TRAVEL_30_MM, U8_T_1200_MS);
    	if(ldahbs16PedalReleaseDctCnt > s16PedalReleaseDctThres)
    	{
    		ldahbu1PedalReleaseByPSPFlg = 1;
	    	ldahbs16PedalReleaseDctCnt = S16_PD_PS_RELEASE_MAINTAIN_TIME;
    	}
    }
    else
    {
    	s16PedalReleaseResetThres = 0;
    	if(ldahbs16PedalReleaseDctCnt <= s16PedalReleaseResetThres)
    	{
    		ldahbu1PedalReleaseByPSPFlg = 0;
    	}
    	else if(ldahbs16PedalReleaseDctCnt > S16_PD_PS_RELEASE_MAINTAIN_TIME)
    	{
    		ldahbs16PedalReleaseDctCnt = S16_PD_PS_RELEASE_MAINTAIN_TIME;
    	}
    	else
    	{
    		;
    	}
    }

    ldahbs16PedalTravelByPSold = (int16_t)GetDetVehStInfo.lsahbs16PedalTravelByPS;
}

/* Code Metric LDIDB_vDetPanicBrkStUsg5msPedlSig		STMIF(5)*/
static void LDIDB_vDetPanicBrkStUsg5msPedlSig(void)
{
	int16_t s16FastPedalApplyThres;

/**********************************************************/
/* Est Init Fast Pedal Apply                              */
/**********************************************************/

	s16FastPedalApplyThres = L_s16IInter2Point(ldahbs16VehicleSpeed,
	                                           S16_SPEED_30_KPH, S16_FAST_PEDAL_APPLY_THRES_LSPD,
	                                           S16_SPEED_70_KPH, S16_FAST_PEDAL_APPLY_THRES);

    if(ldahbu1InitFastPedalApplyFlg==0)
    {
    	if((GetDetVehStInfo.lsahbs16PedalTravelFilt>=0) && (GetDetVehStInfo.lsahbs16PedalTravelFilt<=S16_TRAVEL_20_MM))
    	{
    		if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec>=s16FastPedalApplyThres)
    		{
    			ldahbu1InitFastPedalApplyFlg = 1;
    			ldahbs8FastPedalApplyCnt = 10;
    		}
    		else if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec>=((s16FastPedalApplyThres*8)/10))
    		{
    			ldahbs8FastPedalApplyCnt += (int8_t)L_s16IInter3Point((int16_t)GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec,
    			                                            ((s16FastPedalApplyThres*8)/10), 1,
    			                                            ((s16FastPedalApplyThres*9)/10), 2,
    			                                             (s16FastPedalApplyThres),       3);
    			if(ldahbs8FastPedalApplyCnt >= 10)
    			{
    				ldahbu1InitFastPedalApplyFlg = 1;
    				ldahbs8FastPedalApplyCnt = 10;
    			}
    		}
    		else if(ldahbs8FastPedalApplyCnt>2)
    		{
    			ldahbs8FastPedalApplyCnt = ldahbs8FastPedalApplyCnt - 2;
    		}
    		else
    		{
    			ldahbs8FastPedalApplyCnt = 0;
    		}
    	}
    	else
    	{
    		ldahbs8FastPedalApplyCnt = 0;
    	}
    }
    else
    {
    	if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec <= S16_TRAVEL_0_MM)
    	{
    		ldahbs8FastPedalApplyCnt = ldahbs8FastPedalApplyCnt - 4;
    	}
    	else if(GetDetVehStInfo.ldahbs16PedalTravelRateMmPSec < ((s16FastPedalApplyThres*3)/4))
    	{
    		ldahbs8FastPedalApplyCnt = ldahbs8FastPedalApplyCnt - 2;
    	}
    	else
    	{
    		;
    	}

    	if((ldahbs8FastPedalApplyCnt <= 0)/*&&(ldahbs16PressError<=S16_INITF_OFF_DELTAP)*/) /*P error should be considered*/
    	{
    		ldahbu1InitFastPedalApplyFlg = 0;
    		ldahbs8FastPedalApplyCnt = 0;
    	}
    }
}

static void LDIDB_vDetVehStopSt(void)
{

    ldahbu1VehicleStopFlgOld = ldahbu1VehicleStopFlg;

    if (ldahbu1VehicleStopFlg == 0)
    {
        /*if(ldahbs16VehicleSpeed == S16_SPEED_MIN)*/
        if( (s16WheelSpeedFL<=S16_SPEED_MIN)
         && (s16WheelSpeedFR<=S16_SPEED_MIN)
         && (s16WheelSpeedRL<=S16_SPEED_MIN)
         && (s16WheelSpeedRR<=S16_SPEED_MIN))
        {
            if (lsahbu16SystemOnCnt <= U8_T_300_MS)
            {
                ldahbu1VehicleStopFlg = 1;
                ldahbu8VehicleStopCnt = ldahbu8VehicleStopCnt + 1;
            }
            else if (ldahbu8VehicleStopCnt < U8_T_300_MS)
            {
                ldahbu8VehicleStopCnt = ldahbu8VehicleStopCnt + 1;
            }
            else
            {
                ldahbu1VehicleStopFlg = 1;
            }
        }
        else
        {
            ldahbu8VehicleStopCnt = U8_T_0_MS;
            ldahbu1VehicleStopFlg = 0;
        }
    }
    else
    {
        if( (s16WheelSpeedFL>S16_SPEED_MIN)
         || (s16WheelSpeedFR>S16_SPEED_MIN)
         || (s16WheelSpeedRL>S16_SPEED_MIN)
         || (s16WheelSpeedRR>S16_SPEED_MIN))
        {
            ldahbu8VehicleStopCnt = U8_T_0_MS;
            ldahbu1VehicleStopFlg = 0;
        }
        else
        {
            ;
        }
    }
}

/* Code Metric LDIDB_vEstimVehSpd						STCYC(11)*/
static void LDIDB_vEstimVehSpd(void)
{
    int16_t s16VehSpeedTemp;

    uint8_t u8WheelSpeedValid;


    if (GetDetVehStInfo.liu1cCF_Hcu_ServiceMod == 1)
    {
        u8WheelSpeedValid = 0;

        if( (GetDetVehStInfo.liu1fFLSpeedInvalid==0))
        {
            s16WheelSpeedFL = (int16_t)GetDetVehStInfo.lsahbs16WheelSpeedFL;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedFL = 0;
        }

        if((GetDetVehStInfo.liu1fFRSpeedInvalid==0))
        {
            s16WheelSpeedFR =(int16_t)GetDetVehStInfo.lsahbs16WheelSpeedFR;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedFR = 0;
        }

        s16WheelSpeedRL = 0;
        s16WheelSpeedRR = 0;

        if(u8WheelSpeedValid > 0)
        {
            s16VehSpeedTemp = (s16WheelSpeedFL + s16WheelSpeedFR + (int16_t)(u8WheelSpeedValid/2))/(int16_t)u8WheelSpeedValid;
        }
        else
        {
            s16VehSpeedTemp = 0;
        }

        if (ldahbs16VehicleSpeed >= S16_SPEED_0_25_KPH)
        {
            ldahbs16VehicleSpeed = L_s16Lpf1Int(s16VehSpeedTemp, ldahbs16VehicleSpeed, U8_LPF_7HZ_128G); /* 7Hz  */
        }
        else
        {
            ldahbs16VehicleSpeed = s16VehSpeedTemp;
        }
    }
    else
    {
        u8WheelSpeedValid = 0;

        if( (GetDetVehStInfo.liu1fFLSpeedInvalid==0))
        {
            s16WheelSpeedFL = (int16_t)GetDetVehStInfo.lsahbs16WheelSpeedFL;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedFL = 0;
        }

        if((GetDetVehStInfo.liu1fFRSpeedInvalid==0))
        {
            s16WheelSpeedFR = (int16_t)GetDetVehStInfo.lsahbs16WheelSpeedFR;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedFR = 0;
        }

        if((GetDetVehStInfo.liu1fRLSpeedInvalid==0))
        {
            s16WheelSpeedRL =(int16_t) GetDetVehStInfo.lsahbs16WheelSpeedRL;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedRL = 0;
        }

        if((GetDetVehStInfo.liu1fRRSpeedInvalid==0))
        {
            s16WheelSpeedRR = (int16_t)GetDetVehStInfo.lsahbs16WheelSpeedRR;
            u8WheelSpeedValid = u8WheelSpeedValid+1;
        }

        else
        {
            s16WheelSpeedRR = 0;
        }

        ldahbs16VehicleSpeed = (int16_t)GetDetVehStInfo.lis16VrefESC;

    }

    ldahbs16VehicleSpeed = L_s16LimitMinMax(ldahbs16VehicleSpeed, S16_SPEED_MIN, S16_SPEED_MAX);
    if( (GetDetVehStInfo.liu1fFLSpeedInvalid==1)
     && (GetDetVehStInfo.liu1fFRSpeedInvalid==1)
     && (GetDetVehStInfo.liu1fRLSpeedInvalid==1)
     && (GetDetVehStInfo.liu1fRRSpeedInvalid==1) ) /* Multiple fault : WSS */
    {
        u1SpeedInvalidFlg = 1;
    }
    else
    {
        u1SpeedInvalidFlg = 0;
    }

}

#if (__FLEX_BRAKING_MODE == ENABLE)
/******************************************************************************
* FUNCTION NAME:      LDAHB_vSelectFlexBrkMode
* CALLED BY:          LDAHB_vEstDriverRequestByPDT()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Select Flex Braking Mode
******************************************************************************/
static void LDAHB_vSelectFlexBrkMode(void)
{

  	if(lcahbu1AHBOn == 0)
  	{
  		if(lsahbu8FlexBrakeSwitch == 1)
  		{
  			ldahbu8FlexBrakeMode = 1;
  		}
  		else if(lsahbu8FlexBrakeSwitch == 2)
  		{
  			ldahbu8FlexBrakeMode = 2;
  		}
		else
		{
			ldahbu8FlexBrakeMode = 0;
		}
  	}
	else
	{
		;
	}

}
#endif /*  (__FLEX_BRAKING_MODE == ENABLE)  */



#define DET_5MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
