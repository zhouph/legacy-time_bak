/**
 * @defgroup Det_5msCtrl Det_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_5msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_5msCtrl.h"
#include "Det_5msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LDIDB_DetVehicleState.h"
#include "LDIDB_DetVehicleState1ms.h"
#include "LDIDB_CalcSignalChangeRate.h"
#include "LDIDB_CalcSignalChangeRate1ms.h"
#include "LDIDB_DecidePedalTravel.h"
#include "LDIDB_WheelPressureEstimation.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define     U16_T_2_S               400 
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DET_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Det_5msCtrl_HdrBusType Det_5msCtrlBus;

/* Internal Logic Bus */
IDBDetection_SRCbusType    DETsrcbus;
/* Version Info */
const SwcVersionInfo_t Det_5msCtrlVersionInfo = 
{   
    DET_5MSCTRL_MODULE_ID,           /* Det_5msCtrlVersionInfo.ModuleId */
    DET_5MSCTRL_MAJOR_VERSION,       /* Det_5msCtrlVersionInfo.MajorVer */
    DET_5MSCTRL_MINOR_VERSION,       /* Det_5msCtrlVersionInfo.MinorVer */
    DET_5MSCTRL_PATCH_VERSION,       /* Det_5msCtrlVersionInfo.PatchVer */
    DET_5MSCTRL_BRANCH_VERSION       /* Det_5msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Abc_CtrlWhlVlvReqAbcInfo_t Det_5msCtrlWhlVlvReqAbcInfo;
Vat_CtrlNormVlvReqVlvActInfo_t Det_5msCtrlNormVlvReqVlvActInfo;
Vat_CtrlWhlVlvReqIdbInfo_t Det_5msCtrlWhlVlvReqIdbInfo;
Vat_CtrlIdbBalVlvReqInfo_t Det_5msCtrlIdbBalVlvReqInfo;
Eem_MainEemFailData_t Det_5msCtrlEemFailData;
Proxy_RxCanRxIdbInfo_t Det_5msCtrlCanRxIdbInfo;
Msp_CtrlMotRotgAgSigInfo_t Det_5msCtrlMotRotgAgSigInfo;
Pedal_SenSyncPdt5msRawInfo_t Det_5msCtrlPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Det_5msCtrlPdf5msRawInfo;
Abc_CtrlAbsCtrlInfo_t Det_5msCtrlAbsCtrlInfo;
Spc_5msCtrlCircPFildInfo_t Det_5msCtrlCircPFildInfo;
Spc_5msCtrlCircPOffsCorrdInfo_t Det_5msCtrlCircPOffsCorrdInfo;
Spc_5msCtrlPedlSimrPFildInfo_t Det_5msCtrlPedlSimrPFildInfo;
Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Det_5msCtrlPedlSimrPOffsCorrdInfo;
Spc_5msCtrlPedlTrvlFildInfo_t Det_5msCtrlPedlTrvlFildInfo;
Spc_5msCtrlPistPFildInfo_t Det_5msCtrlPistPFildInfo;
Spc_5msCtrlPistPOffsCorrdInfo_t Det_5msCtrlPistPOffsCorrdInfo;
Spc_5msCtrlWhlSpdFildInfo_t Det_5msCtrlWhlSpdFildInfo;
Eem_MainEemSuspectData_t Det_5msCtrlEemSuspectData;
Mom_HndlrEcuModeSts_t Det_5msCtrlEcuModeSts;
Pct_5msCtrlPCtrlAct_t Det_5msCtrlPCtrlAct;
Abc_CtrlActvBrkCtrlrActFlg_t Det_5msCtrlActvBrkCtrlrActFlg;
Abc_CtrlVehSpd_t Det_5msCtrlVehSpd;
Spc_5msCtrlBlsFild_t Det_5msCtrlBlsFild;
Pct_5msCtrlPCtrlBoostMod_t Det_5msCtrlPCtrlBoostMod;

/* Output Data Element */
Det_5msCtrlBrkPedlStatusInfo_t Det_5msCtrlBrkPedlStatusInfo;
Det_5msCtrlCircPRateInfo_t Det_5msCtrlCircPRateInfo;
Det_5msCtrlEstimdWhlPInfo_t Det_5msCtrlEstimdWhlPInfo;
Det_5msCtrlPedlTrvlRateInfo_t Det_5msCtrlPedlTrvlRateInfo;
Det_5msCtrlPedlTrvlTagInfo_t Det_5msCtrlPedlTrvlTagInfo;
Det_5msCtrlPistPRateInfo_t Det_5msCtrlPistPRateInfo;
Det_5msCtrlVehStopStInfo_t Det_5msCtrlVehStopStInfo;
Det_5msCtrlPedlTrvlFinal_t Det_5msCtrlPedlTrvlFinal;
Det_5msCtrlVehSpdFild_t Det_5msCtrlVehSpdFild;

uint32 Det_5msCtrl_Timer_Start;
uint32 Det_5msCtrl_Timer_Elapsed;

#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Det_5msCtrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[i] = 0;   
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SimVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.CircVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlIvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrIvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlIvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrIvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlOvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrOvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlOvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrOvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlIvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrIvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlIvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrIvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlOvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrOvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlOvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrOvDataLen = 0;
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[i] = 0;   
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq = 0;
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_SimP = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDT = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDF = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFL = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFR = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRL = 0;
    Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRR = 0;
    Det_5msCtrlBus.Det_5msCtrlCanRxIdbInfo.HcuServiceMod = 0;
    Det_5msCtrlBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Det_5msCtrlBus.Det_5msCtrlPdt5msRawInfo.PdtSig = 0;
    Det_5msCtrlBus.Det_5msCtrlPdf5msRawInfo.PdfSig = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsActFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDesTarP = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.PrimCircPFild = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlFildInfo.PdfFild = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlFildInfo.PdtFild = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPFildInfo.PistPFild = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = 0;
    Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssFL = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssFR = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssRL = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssRR = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_SimP = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = 0;
    Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = 0;
    Det_5msCtrlBus.Det_5msCtrlEcuModeSts = 0;
    Det_5msCtrlBus.Det_5msCtrlPCtrlAct = 0;
    Det_5msCtrlBus.Det_5msCtrlActvBrkCtrlrActFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlVehSpd = 0;
    Det_5msCtrlBus.Det_5msCtrlBlsFild = 0;
    Det_5msCtrlBus.Det_5msCtrlPCtrlBoostMod = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.BrkPedlStatus = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1 = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2 = 0;
    Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRate = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPChgDurg5ms = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPRate = 0;
    Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPRateBarPerSec = 0;
    Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP = 0;
    Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP = 0;
    Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP = 0;
    Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgL = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdfSigTag = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgL = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PdtSigTag = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSigTag = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg5ms = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRate = 0;
    Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec = 0;
    Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStandStillStFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStopStFlg = 0;
    Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal = 0;
    Det_5msCtrlBus.Det_5msCtrlVehSpdFild = 0;
}

void Det_5msCtrl(void)
{
    uint16 i;
    
    Det_5msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FlIvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FlIvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FrIvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FrIvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RlIvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RlIvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RrIvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RrIvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FlOvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FlOvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_FrOvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.FrOvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RlOvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RlOvReqData);
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqAbcInfo_RrOvReqData(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqAbcInfo.RrOvReqData);

    Det_5msCtrl_Read_Det_5msCtrlNormVlvReqVlvActInfo(&Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlNormVlvReqVlvActInfo 
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReqData;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.SimVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.CircVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvDataLen;
     : Det_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen;
     =============================================================================*/
    
    Det_5msCtrl_Read_Det_5msCtrlWhlVlvReqIdbInfo(&Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlWhlVlvReqIdbInfo 
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvReq;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvDataLen;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData;
     : Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData;
     =============================================================================*/
    
    Det_5msCtrl_Read_Det_5msCtrlIdbBalVlvReqInfo(&Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlIdbBalVlvReqInfo 
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReqData;
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq;
     : Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvDataLen;
     : Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvDataLen;
     : Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_SimP(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_SimP);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_PedalPDT(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDT);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_PedalPDF(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_PedalPDF);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssFL(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFL);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssFR(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssFR);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssRL(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRL);
    Det_5msCtrl_Read_Det_5msCtrlEemFailData_Eem_Fail_WssRR(&Det_5msCtrlBus.Det_5msCtrlEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlCanRxIdbInfo_HcuServiceMod(&Det_5msCtrlBus.Det_5msCtrlCanRxIdbInfo.HcuServiceMod);

    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Det_5msCtrlBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd);

    Det_5msCtrl_Read_Det_5msCtrlPdt5msRawInfo(&Det_5msCtrlBus.Det_5msCtrlPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPdt5msRawInfo 
     : Det_5msCtrlPdt5msRawInfo.PdtSig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlPdf5msRawInfo_PdfSig(&Det_5msCtrlBus.Det_5msCtrlPdf5msRawInfo.PdfSig);

    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsActFlg(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsActFlg);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDefectFlg(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDefectFlg);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPFrntLe);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPFrntRi);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPReLe(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPReLe);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsTarPReRi(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsTarPReRi);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_FrntWhlSlip(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.FrntWhlSlip);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDesTarP(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDesTarP);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg);
    Det_5msCtrl_Read_Det_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(&Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg);

    Det_5msCtrl_Read_Det_5msCtrlCircPFildInfo(&Det_5msCtrlBus.Det_5msCtrlCircPFildInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlCircPFildInfo 
     : Det_5msCtrlCircPFildInfo.PrimCircPFild;
     : Det_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
     : Det_5msCtrlCircPFildInfo.SecdCircPFild;
     : Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd(&Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd);
    Det_5msCtrl_Read_Det_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd(&Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd);

    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(&Det_5msCtrlBus.Det_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);

    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd(&Det_5msCtrlBus.Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd);

    Det_5msCtrl_Read_Det_5msCtrlPedlTrvlFildInfo(&Det_5msCtrlBus.Det_5msCtrlPedlTrvlFildInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPedlTrvlFildInfo 
     : Det_5msCtrlPedlTrvlFildInfo.PdfFild;
     : Det_5msCtrlPedlTrvlFildInfo.PdtFild;
     =============================================================================*/
    
    Det_5msCtrl_Read_Det_5msCtrlPistPFildInfo(&Det_5msCtrlBus.Det_5msCtrlPistPFildInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPistPFildInfo 
     : Det_5msCtrlPistPFildInfo.PistPFild;
     : Det_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrd(&Det_5msCtrlBus.Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd);

    Det_5msCtrl_Read_Det_5msCtrlWhlSpdFildInfo(&Det_5msCtrlBus.Det_5msCtrlWhlSpdFildInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlWhlSpdFildInfo 
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
     : Det_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssFL(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssFL);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssFR(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssFR);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssRL(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssRL);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_WssRR(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_WssRR);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_SimP(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_SimP);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT);
    Det_5msCtrl_Read_Det_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF(&Det_5msCtrlBus.Det_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF);

    Det_5msCtrl_Read_Det_5msCtrlEcuModeSts(&Det_5msCtrlBus.Det_5msCtrlEcuModeSts);
    Det_5msCtrl_Read_Det_5msCtrlPCtrlAct(&Det_5msCtrlBus.Det_5msCtrlPCtrlAct);
    Det_5msCtrl_Read_Det_5msCtrlActvBrkCtrlrActFlg(&Det_5msCtrlBus.Det_5msCtrlActvBrkCtrlrActFlg);
    Det_5msCtrl_Read_Det_5msCtrlVehSpd(&Det_5msCtrlBus.Det_5msCtrlVehSpd);
    Det_5msCtrl_Read_Det_5msCtrlBlsFild(&Det_5msCtrlBus.Det_5msCtrlBlsFild);
    Det_5msCtrl_Read_Det_5msCtrlPCtrlBoostMod(&Det_5msCtrlBus.Det_5msCtrlPCtrlBoostMod);

    /* Process */
	if(DETsrcbus.SystemOnCnt<U16_T_2_S)
    {
        DETsrcbus.SystemOnCnt++;
    }
    LDIDB_vCallPedlInpSeln();
    LDIDB_vCallSigRateCalcn();
    LDIDB_vCallVehStDet();
    LDIDB_vEstWhlP();

    /* Output */
    Det_5msCtrl_Write_Det_5msCtrlBrkPedlStatusInfo(&Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlBrkPedlStatusInfo 
     : Det_5msCtrlBrkPedlStatusInfo.BrkPedlStatus;
     : Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;
     : Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg;
     : Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1;
     : Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2;
     : Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlCircPRateInfo(&Det_5msCtrlBus.Det_5msCtrlCircPRateInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlCircPRateInfo 
     : Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms;
     : Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms;
     : Det_5msCtrlCircPRateInfo.PrimCircPRate;
     : Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec;
     : Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms;
     : Det_5msCtrlCircPRateInfo.SecdCircPChgDurg5ms;
     : Det_5msCtrlCircPRateInfo.SecdCircPRate;
     : Det_5msCtrlCircPRateInfo.SecdCircPRateBarPerSec;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlEstimdWhlPInfo(&Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlEstimdWhlPInfo 
     : Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
     : Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
     : Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
     : Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlPedlTrvlRateInfo(&Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPedlTrvlRateInfo 
     : Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRate;
     : Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlPedlTrvlTagInfo(&Det_5msCtrlBus.Det_5msCtrlPedlTrvlTagInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPedlTrvlTagInfo 
     : Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH;
     : Det_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgL;
     : Det_5msCtrlPedlTrvlTagInfo.PdfSigTag;
     : Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH;
     : Det_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgL;
     : Det_5msCtrlPedlTrvlTagInfo.PdtSigTag;
     : Det_5msCtrlPedlTrvlTagInfo.PedlSigTag;
     : Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH;
     : Det_5msCtrlPedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlPistPRateInfo(&Det_5msCtrlBus.Det_5msCtrlPistPRateInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlPistPRateInfo 
     : Det_5msCtrlPistPRateInfo.PistPChgDurg10ms;
     : Det_5msCtrlPistPRateInfo.PistPChgDurg5ms;
     : Det_5msCtrlPistPRateInfo.PistPRate;
     : Det_5msCtrlPistPRateInfo.PistPRateBarPerSec;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlVehStopStInfo(&Det_5msCtrlBus.Det_5msCtrlVehStopStInfo);
    /*==============================================================================
    * Members of structure Det_5msCtrlVehStopStInfo 
     : Det_5msCtrlVehStopStInfo.VehStandStillStFlg;
     : Det_5msCtrlVehStopStInfo.VehStopStFlg;
     =============================================================================*/
    
    Det_5msCtrl_Write_Det_5msCtrlPedlTrvlFinal(&Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
    Det_5msCtrl_Write_Det_5msCtrlVehSpdFild(&Det_5msCtrlBus.Det_5msCtrlVehSpdFild);

    Det_5msCtrl_Timer_Elapsed = STM0_TIM0.U - Det_5msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DET_5MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
