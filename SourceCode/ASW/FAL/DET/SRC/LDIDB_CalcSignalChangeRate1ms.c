/**
 * @defgroup LDIDB_CalcSensorSignalChangeRate LDIDB_CalcSignalChangeRate
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_CalcSignalChangeRate.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_CalcSignalChangeRate1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
    int16_t lss16PedalFilt1ms;
    uint16_t lsahbu16SystemOnCnt;

}GetSigRateCalc1ms_t;

typedef struct
{
    int16_t s16PressRate1ms_1_100Bar;
    int16_t s16PressFilt1ms_1_100Bar;
    int16_t s16PressFilt1msBuf[5];
    int16_t s16PressFilt1msBufCnt;
    int16_t s16PressRate1msBuf[5];
    int16_t s16PressRate1msBufCnt;

    int16_t s16PressRate1msTrimedAvg;
    int16_t s16PressRate1msAvg;
}press_rate_data_1ms_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define DET_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC int16_t lsahbs16PedalRate1ms;
FAL_STATIC int16_t lsahbs16PedalFilt1msBuf[5];
FAL_STATIC int16_t lsahbs16PedalFilt1msBufCnt;

FAL_STATIC GetSigRateCalc1ms_t GetSigRateCalc1ms;
FAL_STATIC press_rate_data_1ms_t *pPress_Rate_1ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
FAL_STATIC press_rate_data_1ms_t PistP_rate_1ms;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
FAL_STATIC press_rate_data_1ms_t PrimP_rate_1ms;
FAL_STATIC press_rate_data_1ms_t SecdP_rate_1ms;
#else
#endif

#define lss16MCprsRate1mTrimedAvg           pPress_Rate_1ms->s16PressRate1msTrimedAvg
#define lss16MCprsRate1msAvg                pPress_Rate_1ms->s16PressRate1msAvg
#define lsahbs16MCpresFilt_1_100Bar         pPress_Rate_1ms->s16PressFilt_1_100Bar
#define lss16MCprsRate1ms_1_100Bar			pPress_Rate_1ms->s16PressRate1ms_1_100Bar

#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/

#define DET_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_1MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_1MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

static void LDIDB_vCalcPdt1msSigRate(void);
static void LDIDB_vCalcP1msSigRate(void);
static void LDIDB_vInpSigIfForSigRateCaln1ms(void);
static void LDIDB_vOutpSigIfForSigRateCaln1ms(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LDIDB_vCallSigRateCalcn1ms(void)
{
    LDIDB_vInpSigIfForSigRateCaln1ms();
    LDIDB_vCalcPdt1msSigRate();
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    pPress_Rate_1ms=&PistP_rate_1ms;
    LDIDB_vCalcP1msSigRate();
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    pPress_Rate_1ms=&PrimP_rate_1ms;
    LDIDB_vCalcP1msSigRate();
    pPress_Rate_1ms=&SecdP_rate_1ms;
    LDIDB_vCalcP1msSigRate();
#else
#endif
    LDIDB_vOutpSigIfForSigRateCaln1ms();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LDIDB_vInpSigIfForSigRateCaln1ms(void)
{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    PistP_rate_1ms.s16PressFilt1ms_1_100Bar  = (int16_t) Det_1msCtrlBus.Det_1msCtrlPistPFild1msInfo.PistPFild1ms;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    PrimP_rate_1ms.s16PressFilt1ms_1_100Bar = (int16_t)Det_1msCtrlBus.Det_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
    SecdP_rate_1ms.s16PressFilt1ms_1_100Bar  = (int16_t)Det_1msCtrlBus.Det_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
#else
#endif
    GetSigRateCalc1ms.lss16PedalFilt1ms =(int16_t)Det_1msCtrlBus.Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
    GetSigRateCalc1ms.lsahbu16SystemOnCnt  =       DETsrcbus.SystemOnCnt1ms;
}

static void LDIDB_vOutpSigIfForSigRateCaln1ms(void)
{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE

	Det_1msCtrlBus.Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar       = PistP_rate_1ms.s16PressRate1msAvg;
    /*DETsrcbus.PistP1msSigRate=PistP_rate_1ms.s16PressRate1ms_1_100Bar;*/
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    Det_1msCtrlBus.Det_1msCtrlCircPRate1msInfo.PrimCircPRate1msAvg_1_100Bar       = PrimP_rate_1ms.s16PressRate1msAvg;
    Det_1msCtrlBus.Det_1msCtrlCircPRate1msInfo.SecdCircPRate1msAvg_1_100Bar       = SecdP_rate_1ms.s16PressRate1msAvg;

    /*DETsrcbus.PrimP1msSigRate=PrimP_rate_1ms.s16PressRate1ms_1_100Bar;
    DETsrcbus.SecdP1msSigRate=SecdP_rate_1ms.s16PressRate1ms_1_100Bar;*/
#else
#endif
    DETsrcbus.PedlTrvl1msSigRate=lsahbs16PedalRate1ms;
}

/**********************************************************/
/* Pedal Position change rate (1 = 0.1mm/5ms =  20mm/s)   */
/**********************************************************/
static void  LDIDB_vCalcPdt1msSigRate(void)
{
	uint8_t  U8PDRateFiltCoeff;
	int16_t lsahbs16PedalFilt1msOld;

  #if defined __AHB_RIG_TEST
	U8PDRateFiltCoeff = LPF_1_FIL_K_FC_25HZ_TS1MS;          /*  25 Hz   */
  #else
	U8PDRateFiltCoeff = LPF_1_FIL_K_FC_25HZ_TS1MS;          /*  25 Hz    */
  #endif

	lsahbs16PedalFilt1msOld= lsahbs16PedalFilt1msBuf[lsahbs16PedalFilt1msBufCnt];
	lsahbs16PedalFilt1msBuf[lsahbs16PedalFilt1msBufCnt]=GetSigRateCalc1ms.lss16PedalFilt1ms ;
	lsahbs16PedalFilt1msBufCnt++;

	if(lsahbs16PedalFilt1msBufCnt>=5)
	{
		lsahbs16PedalFilt1msBufCnt=0;
	}

	if(GetSigRateCalc1ms.lsahbu16SystemOnCnt<=50)
	{
		lsahbs16PedalRate1ms = GetSigRateCalc1ms.lss16PedalFilt1ms - lsahbs16PedalFilt1msOld;
	}
	else
	{
		lsahbs16PedalRate1ms = L_s16Lpf1Int1ms( (GetSigRateCalc1ms.lss16PedalFilt1ms - lsahbs16PedalFilt1msOld), lsahbs16PedalRate1ms, U8PDRateFiltCoeff);
	}
}
/**********************************************************/
/* Master Chamber Press rate (1 = 0.01bar/5ms*2 = 1bar/s) */
/**********************************************************/
static void  LDIDB_vCalcP1msSigRate(void)
{
	static  uint8_t  U8MCPRateFiltCoeff;
	static  int16_t lsahbs16MCprsRate1msSum;
	static 	int16_t lsahbs16MCprsRate1msMax;
	static 	int16_t lsahbs16MCprsRate1msMin;
	static int16_t lss16MCprsFilt1msOld_1_100Bar;
	static uint8_t Buf_i;



	U8MCPRateFiltCoeff = LPF_1_FIL_K_FC_25HZ_TS1MS;          /*  25 Hz   */

	lss16MCprsFilt1msOld_1_100Bar=pPress_Rate_1ms->s16PressFilt1msBuf[pPress_Rate_1ms->s16PressFilt1msBufCnt];
	pPress_Rate_1ms->s16PressFilt1msBuf[pPress_Rate_1ms->s16PressFilt1msBufCnt]=pPress_Rate_1ms->s16PressFilt1ms_1_100Bar;
	pPress_Rate_1ms->s16PressFilt1msBufCnt++;

	if(pPress_Rate_1ms->s16PressFilt1msBufCnt>=5)
	{
		pPress_Rate_1ms->s16PressFilt1msBufCnt=0;
	}

	if(GetSigRateCalc1ms.lsahbu16SystemOnCnt<=50)
	{
		pPress_Rate_1ms->s16PressRate1ms_1_100Bar = (pPress_Rate_1ms->s16PressFilt1ms_1_100Bar - lss16MCprsFilt1msOld_1_100Bar)*2;
	}
	else
	{
		pPress_Rate_1ms->s16PressRate1ms_1_100Bar = L_s16Lpf1Int1ms( ((pPress_Rate_1ms->s16PressFilt1ms_1_100Bar - lss16MCprsFilt1msOld_1_100Bar)*2), pPress_Rate_1ms->s16PressRate1ms_1_100Bar, U8MCPRateFiltCoeff );
	}

	pPress_Rate_1ms->s16PressRate1msBuf[pPress_Rate_1ms->s16PressRate1msBufCnt]=pPress_Rate_1ms->s16PressRate1ms_1_100Bar;
	pPress_Rate_1ms->s16PressRate1msBufCnt++;

	if(pPress_Rate_1ms->s16PressRate1msBufCnt>=5)
	{
		pPress_Rate_1ms->s16PressRate1msBufCnt=0;
	}

	lsahbs16MCprsRate1msSum=pPress_Rate_1ms->s16PressRate1msBuf[0];
	lsahbs16MCprsRate1msMax=pPress_Rate_1ms->s16PressRate1msBuf[0];
	lsahbs16MCprsRate1msMin=pPress_Rate_1ms->s16PressRate1msBuf[0];

	for(Buf_i=1;Buf_i<5;Buf_i++)
	{
		lsahbs16MCprsRate1msSum+=pPress_Rate_1ms->s16PressRate1msBuf[Buf_i];
		lsahbs16MCprsRate1msMax = (lsahbs16MCprsRate1msMax >= pPress_Rate_1ms->s16PressRate1msBuf[Buf_i]) ? lsahbs16MCprsRate1msMax :  pPress_Rate_1ms->s16PressRate1msBuf[Buf_i];
		lsahbs16MCprsRate1msMin = (lsahbs16MCprsRate1msMin <= pPress_Rate_1ms->s16PressRate1msBuf[Buf_i]) ? lsahbs16MCprsRate1msMin :  pPress_Rate_1ms->s16PressRate1msBuf[Buf_i];
	}

	pPress_Rate_1ms->s16PressRate1msTrimedAvg = (lsahbs16MCprsRate1msSum - lsahbs16MCprsRate1msMax - lsahbs16MCprsRate1msMin)/3;
	pPress_Rate_1ms->s16PressRate1msAvg = (lsahbs16MCprsRate1msSum)/5;
}

#define DET_1MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
