/**
 * @defgroup LDIDB_WheelPressureEstimation LDIDB_WheelPressureEstimation
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LDIDB_WheelPressureEstimation.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LDIDB_WheelPressureEstimation.h"
#include "LDIDB_LoadMapData.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
  =============================================================================*/
/*#define M_WPE_modularization*/
#define M_WPE_For_Mux 						(DISABLE) /*TODO*/
/* Add for Volume based pressure */
#define INLETVOLUME_LIMIT 					(ENABLE) /*TODO*/
/* Add for Volume based pressure */
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint8_t ldidbu8InletValveOpenTi;
	uint8_t ldidbu8InletValveOpenTiOld;
	uint8_t ldidbu8InletValveOpenTiCorrd;
	uint8_t ldidbu8InletValveOpenTiForVol;
	uint8_t ldidbu8OutletValveOpenTi;
	uint8_t ldidbu8OutletValveOpenTiOld;
	uint8_t ldidbu8OutletValveOpenTiCorrd;
	uint8_t ldidbu8OutletValveOpenTiForVol;
	uint8_t ldidbu8InletValveOpenTiLfc;

	int32_t *ldidbs32InletVlvDrvReqOutpI;
	int32_t *ldidbs32OutletVlvDrvReqOutpI;

	/* Add for Volume based pressure */
	uint8_t ldidbu8WhlVolumeResetFlg;
	uint8_t ldidbu8WhlVolumeResetCnt;
	uint8_t ldidbu8InletVolumePercent;
	int32_t ldidbs32WhlVolumeByS;
	int32_t ldidbs32WhlVolumeBySOld;
	int16_t ldidbs16EstmdWhlPByS;
	uint8_t ldidbu8WhlPByVolBrakeOnFlg;
	uint8_t ldidbu8WhlPByVolBrakeOnCnt;
	uint8_t ldidbu8WhlPByVolBrakeOnInitFlg;
	uint8_t ldidbu8WhlPByVol1stRiseFlg;
	uint8_t ldidbu8WhlPByVol1stRiseCnt;
	int16_t ldidbs16WhlVChgViaInletVlvByS;
	int16_t ldidbs16WhlVChgViaOutletVlvByS;
	/* Add for Volume based pressure */
	/* Add for New Concept Pressure based pressure */
	int32_t ldidbs32WhlVolumeByP;
	int16_t ldidbs16EstmdWhlP;
	int16_t ldidbs16EstmdWhlPOld;
	int16_t ldidbs16EstDeltaP;
	int16_t ldidbs16EstDeltaPLPF;
	uint8_t ldidbu8EstPWhlState;
	uint8_t ldidbu8EstPWhlStateChgCnt;
	int16_t ldidbs16WhlVChgViaInletVlv;
	int16_t ldidbs16WhlVChgViaOutletVlv;
	int16_t ldidbs16WhlVChgViaCutVlv;
	int16_t ldidbs16WhlVChgViaBalVlv;
	/* Add for New Concept Pressure based pressure */

	uint8_t ldidbu8ContinuedFullRiseModeFlg;
	uint8_t ldidbu8ContinuedDumpModeFlg;
	uint8_t ldidbu8ContinuedLfcRiseModeFlg;

	uint8_t ldidbu8Wheel_Valve_Mode;
	uint8_t ldidbu8Wheel_Valve_Mode_old;

	uint8_t ldidbu8wpe_press_match_chk_cnt;
	uint8_t ldidbu8u1PressMatchSusFlg;

	int16_t ldidbu8InletValveDuty;

	/*Debuggers*/
	uint8_t Est_WhlP_Inlet_MON;
	uint8_t Est_WhlP_Outlet_MON;
	int32_t WhlPChgTotal_MON;
	int32_t Debugger;
}WPE_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/* Add for New Concept Pressure based pressure */
#define U16_SEL_4WHEEL					(0)
#define U16_SEL_F2WHEEL					(1)
#define U16_SEL_F1R1WHEEL				(2)
#define U16_SEL_F1WHEEL					(3)
#define U16_SEL_R1WHEEL					(4)
#define U16_SEL_F2R1WHEEL				(5)
/* Add for New Concept Pressure based pressure */

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"

#define WPE_FL_CASE 				(0)
#define WPE_FR_CASE 				(1)
#define WPE_RL_CASE 				(2)
#define WPE_RR_CASE 				(3)

#define WHEEL_PRESS_APPLY			(0)
#define WHEEL_PRESS_HOLD			(1)
#define WHEEL_PRESS_DUMP			(2)
#define WHEEL_PRESS_CIRCULATION 	(3)
#define WHEEL_PRESS_NOM 			(4)

/* Add for New Concept Pressure based pressure */
#define S16_PRESS_NONE				(0)
#define S16_PRESS_RISE				(1)
#define S16_PRESS_HOLD				(2)
#define S16_PRESS_DOWN				(3)
/* Add for New Concept Pressure based pressure */

#define S16_WPE_LPA_Pressure 		(0)
#define WPE_INC_LIM					(1500)  /* 15bar */
#define WPE_DEC_LIM					(-1500) /* -15bar */

#define WPE_PRESS_MATCH_CHK_T 		(4)
#define NO_VLV_OPENED 				(0)
#define NC_VLV_OPENED 				(1)
#define WHEEL_PRESS_MAX 			(20000) /*Max 200bar*/
#define WHEEL_PRESS_MIN 			(0) 	/*Min 0bar*/

#define PRESS_RESOL  				(100)
#define SQRT_PRESS_RESOL 			(100)
#define SQRT_ARRAY_LENGTH			(15)

#define NO_VLV 0
#define NC_VLV 1

#define VALVE_OFF_DUTY  0
#define NC_CUT_ON_DUTY  700
#define NC_FULL_ON_DUTY 1000
#define NO_FULL_ON_DUTY 1000

#if M_WPE_For_Mux == ENABLE
#define WHL_IN_VLV_TYPE     NC_VLV
#define WHL_OUT_VLV_TYPE     NC_VLV
#else
#define WHL_IN_VLV_TYPE     NO_VLV
#define WHL_OUT_VLV_TYPE     NC_VLV
#endif

/* HW Spec */
#define S16_VOL_CHAMBER1_AREA			(66)  /* Column area */
#define S16_VOL_CHAMBER2_AREA			(100) /* Fixed */

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"

#define DET_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/

#define DET_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_5MSCTRL_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/
#define DET_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"

FAL_STATIC int32_t ldidbs32PrimCircP;
FAL_STATIC int32_t ldidbs32SecdCircP;
FAL_STATIC int32_t ldidbs32PedlSimrP;

FAL_STATIC uint8_t ldidbu8CutVlv1ActFlg;
FAL_STATIC uint8_t ldidbu8CutVlv2ActFlg;
FAL_STATIC uint8_t ldidbu8BalVlv1ActFlg;
FAL_STATIC uint8_t ldidbu8BalVlv2ActFlg;
FAL_STATIC uint8_t ldidbu8ChmBalVlvActFlg;
FAL_STATIC uint8_t ldidbu8RelsVlvActFlg;
FAL_STATIC uint8_t ldidbu8ABSActFlg;
FAL_STATIC uint8_t ldidbu8BLSFlg;
FAL_STATIC uint8_t ldidbu8ActvBrkCtrlrActFlg;
FAL_STATIC WPE_t WPE_FL, WPE_FR, WPE_RL, WPE_RR;
FAL_STATIC WPE_t *pWPE, *pWPE_Diag;
FAL_STATIC WPE_CAL_t *pWPE_CAL;

/* Add for Volume based pressure */
FAL_STATIC int32_t ldidbs32StrokePosition;
FAL_STATIC int32_t ldidbs32StrokePositionOld;
FAL_STATIC int32_t ldidbs32StrkPosiDelta;
FAL_STATIC uint8_t ldidbu8PrimCircBrakeOnFlg;
FAL_STATIC uint8_t ldidbu8SecdCircBrakeOnFlg;
FAL_STATIC uint8_t ldidbu8PrimCircBrakeOnCnt;
FAL_STATIC uint8_t ldidbu8SecdCircBrakeOnCnt;
FAL_STATIC uint8_t ldidbu8PrimCircBrakeOnInitFlg;
FAL_STATIC uint8_t ldidbu8SecdCircBrakeOnInitFlg;
FAL_STATIC uint8_t ldidbu8PrimCircBrakeOnInitCnt;
FAL_STATIC uint8_t ldidbu8SecdCircBrakeOnInitCnt;
/* Add for Volume based pressure */

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_5MSCTRL_START_SEC_CODE
#include "Det_MemMap.h"

static void LDIDB_vInpSigIfForEstWhlP(void);
static void LDIDB_vOutpSigIfForEstWhlP(void);
/* Add for Volume based pressure */
static void LDIDB_vCorrdDeltaStroke(void);
static void LDIDB_vCircuitBrakeState(void);
static void LDIDB_vPrimCircBrakeOn(void);
static void LDIDB_vSecdCircBrakeOn(void);
static void LDIDB_vPrimCircBrakeInitOn(void);
static void LDIDB_vSecdCircBrakeInitOn(void);
static void LDIDB_vEstEachWhlPByVolume(WPE_t *pWPE_tmp, WPE_CAL_t *pWPE_CAL_tmp, uint8_t WPE_WL_case, uint8_t u8CircBrakeOnInit, uint8_t u8CircBrakeOn);
static void LDIDB_vWhlVolumeResetCondition(uint8_t u8CircBrakeOnFlg);
static void LDIDB_vWhlVolumePercent(void);
static void LDIDB_vDetEstmdWhlVolumeByS(uint8_t u8CircBrakeOnInitFlg);
static void LDIDB_vEachWhlPBrakeOnState(uint8_t u8CircBrakeOnFlg);
static void LDIDB_vEachWhlP1stRiseSection(void);
static void LDIDB_vDetEstmdWhlPressByS(uint8_t WPE_WL_case);
/* Add for Volume based pressure */
static void LDIDB_vEstEachWhlP(WPE_t *pWPE_tmp, WPE_t *pWPE_Diag_tmp, int32_t s32CircP, int32_t s32PedlSimrP, WPE_CAL_t *pWPE_CAL_tmp, uint8_t WPE_WL_case, uint8_t u8CutVVActFlg, uint8_t u8BalVvActFlg);
/* Add for New Concept Pressure based pressure */
static void LDIDB_vCalcWhlVChgViaInletVlv(int32_t s32PSply);
static void LDIDB_vCalcWhlVChgViaCutVlv(uint8_t u8CutVVActFlg, int32_t s32PSply);
static void LDIDB_vCalcWhlVChgViaOutletVlv(void);
static void LDIDB_vCalcWhlVChgViaBalVlv(uint8_t u1WPEBalVvActFlg);
static void LDIDB_vDetEstmdWhlVolumeByP(void);
static void LDIDB_vDetEstmdWhlPressByP(uint8_t WPE_WL_case);
static void LDIDB_vDetEstmdWhlPState(void);
static void LDIDB_vDetEstmdWhlPStateNone(void);
static void LDIDB_vDetEstmdWhlPStateRise(void);
static void LDIDB_vDetEstmdWhlPStateHold(void);
static void LDIDB_vDetEstmdWhlPStateDown(void);
static void LDIDB_vDetFinalEstmdWhlP(int32_t s32PSply, uint8_t WPE_WL_case);
/* Add for New Concept Pressure based pressure */
static void LDIDB_vDetInOutVvState(uint8_t u1WPEBalVvActFlg);
static void LDIDB_vCountValveActnTi(void);
static void LDIDB_vCorrdValveActnTi(void);
static void LDIDB_vCorrdValveActnTiForVol(uint8_t u8CircBalVvActFlg);
static void LDIDB_vDecideWhlVvMode(void);
static void LDIDB_vPressureMatching(void);

static uint16_t LDIDB_u16CalcSqrtDeltaP(int16_t s16DeltaPTemp);
static int16_t LDIDB_s16CalcWpeGain(int16_t Pwhl_tmp, int16_t* WpePressArr, int16_t* WpeGainArr, uint8_t WpeArraySize);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LDIDB_vEstWhlP(void)
{
	LDIDB_vInpSigIfForEstWhlP();

	/* Add for Volume based pressure */
	#if (M_WPE_For_Mux == DISABLE)
	LDIDB_vCorrdDeltaStroke();  /* Chamber Balance, Release valve correction  */
	#endif
	LDIDB_vCircuitBrakeState();
	LDIDB_vEstEachWhlPByVolume(&WPE_FL, &DETcalib.WPE_CAL_FRT, WPE_FL_CASE, ldidbu8SecdCircBrakeOnInitFlg, ldidbu8SecdCircBrakeOnFlg);
	LDIDB_vEstEachWhlPByVolume(&WPE_FR, &DETcalib.WPE_CAL_FRT, WPE_FR_CASE, ldidbu8PrimCircBrakeOnInitFlg, ldidbu8PrimCircBrakeOnFlg);
	LDIDB_vEstEachWhlPByVolume(&WPE_RL, &DETcalib.WPE_CAL_RR , WPE_RL_CASE, ldidbu8PrimCircBrakeOnInitFlg, ldidbu8PrimCircBrakeOnFlg);
	LDIDB_vEstEachWhlPByVolume(&WPE_RR, &DETcalib.WPE_CAL_RR , WPE_RR_CASE, ldidbu8SecdCircBrakeOnInitFlg, ldidbu8SecdCircBrakeOnFlg);
	/* Add for Volume based pressure */

	LDIDB_vEstEachWhlP(&WPE_FL, &WPE_RR, ldidbs32SecdCircP, ldidbs32PedlSimrP, &DETcalib.WPE_CAL_FRT, WPE_FL_CASE, ldidbu8CutVlv2ActFlg, ldidbu8BalVlv2ActFlg);
	LDIDB_vEstEachWhlP(&WPE_FR, &WPE_RL, ldidbs32PrimCircP, ldidbs32PedlSimrP, &DETcalib.WPE_CAL_FRT, WPE_FR_CASE, ldidbu8CutVlv1ActFlg, ldidbu8BalVlv1ActFlg);
	LDIDB_vEstEachWhlP(&WPE_RL, &WPE_FR, ldidbs32PrimCircP, ldidbs32PedlSimrP, &DETcalib.WPE_CAL_RR, WPE_RL_CASE, ldidbu8CutVlv1ActFlg, ldidbu8BalVlv1ActFlg);
	LDIDB_vEstEachWhlP(&WPE_RR, &WPE_FL, ldidbs32SecdCircP, ldidbs32PedlSimrP, &DETcalib.WPE_CAL_RR, WPE_RR_CASE, ldidbu8CutVlv2ActFlg, ldidbu8BalVlv2ActFlg);

	LDIDB_vOutpSigIfForEstWhlP();

	return;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LDIDB_vInpSigIfForEstWhlP(void)
{
#if (M_WPE_For_Mux == ENABLE)
	ldidbs32PrimCircP = Det_5msCtrlBus.Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;
	ldidbs32SecdCircP = Det_5msCtrlBus.Det_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;
	ldidbs32PedlSimrP = Det_5msCtrlBus.Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;

	ldidbu8BalVlv1ActFlg = Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.PrimBalVlvReq;
	ldidbu8BalVlv2ActFlg = Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.SecdBalVlvReq;
#else
	ldidbs32PrimCircP = Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
	ldidbs32SecdCircP = Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;
	ldidbs32PedlSimrP = Det_5msCtrlBus.Det_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;

	ldidbu8BalVlv1ActFlg = 0;
	ldidbu8BalVlv2ActFlg = 0;
#endif

	WPE_FL.ldidbs32InletVlvDrvReqOutpI  = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlIvReqData;
	WPE_FR.ldidbs32InletVlvDrvReqOutpI  = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrIvReqData;
	WPE_RL.ldidbs32InletVlvDrvReqOutpI  = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlIvReqData;
	WPE_RR.ldidbs32InletVlvDrvReqOutpI  = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrIvReqData;
	WPE_FL.ldidbs32OutletVlvDrvReqOutpI = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FlOvReqData;
	WPE_FR.ldidbs32OutletVlvDrvReqOutpI = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.FrOvReqData;
	WPE_RL.ldidbs32OutletVlvDrvReqOutpI = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RlOvReqData;
	WPE_RR.ldidbs32OutletVlvDrvReqOutpI = Det_5msCtrlBus.Det_5msCtrlWhlVlvReqIdbInfo.RrOvReqData;

	ldidbu8ChmBalVlvActFlg = Det_5msCtrlBus.Det_5msCtrlIdbBalVlvReqInfo.ChmbBalVlvReq;
	ldidbu8RelsVlvActFlg   = Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.RelsVlvReq;
	ldidbu8CutVlv1ActFlg = Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
	ldidbu8CutVlv2ActFlg = Det_5msCtrlBus.Det_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq;

	ldidbu8BLSFlg             = Det_5msCtrlBus.Det_5msCtrlBlsFild;    
	ldidbu8ABSActFlg          = Det_5msCtrlBus.Det_5msCtrlAbsCtrlInfo.AbsActFlg;
	ldidbu8ActvBrkCtrlrActFlg = (uint8_t)Det_5msCtrlBus.Det_5msCtrlActvBrkCtrlrActFlg;
	
	ldidbs32StrokePositionOld = ldidbs32StrokePosition;
	#if (__CAR == KMC_TFE)   /* Temp */
	ldidbs32StrokePosition    = Det_5msCtrlBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd*151/100;  /* Temp. until TFE P1 C Sample map setting, A1*1.5 = A2 */
	#else
	ldidbs32StrokePosition    = Det_5msCtrlBus.Det_5msCtrlMotRotgAgSigInfo.StkPosnMeasd;
	#endif
	ldidbs32StrokePosition    = (ldidbs32StrokePosition > 0) ? ldidbs32StrokePosition : 0;
	ldidbs32StrkPosiDelta     = ldidbs32StrokePosition - ldidbs32StrokePositionOld;
	
	return;
}

static void LDIDB_vOutpSigIfForEstWhlP(void)
{
	Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP = WPE_FR.ldidbs16EstmdWhlP;
	Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP = WPE_FL.ldidbs16EstmdWhlP;
	Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP = WPE_RR.ldidbs16EstmdWhlP;
	Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP = WPE_RL.ldidbs16EstmdWhlP;

	return;
}

/* Add for Volume based pressure */
static void LDIDB_vEstEachWhlPByVolume(WPE_t *pWPE_tmp, WPE_CAL_t *pWPE_CAL_tmp, uint8_t WPE_WL_case, uint8_t u8CircBrakeOnInit, uint8_t u8CircBrakeOn)
{
	
	pWPE     = pWPE_tmp;
	pWPE_CAL = pWPE_CAL_tmp;

	LDIDB_vWhlVolumeResetCondition(u8CircBrakeOn);
	LDIDB_vWhlVolumePercent();
	LDIDB_vDetEstmdWhlVolumeByS(u8CircBrakeOnInit);
	LDIDB_vEachWhlPBrakeOnState(u8CircBrakeOn);
	LDIDB_vEachWhlP1stRiseSection();
	LDIDB_vDetEstmdWhlPressByS(WPE_WL_case);

	return;
}

static void LDIDB_vCorrdDeltaStroke(void)
{
	int16_t s16ChamberAreaRatio = (S16_VOL_CHAMBER1_AREA*100)/S16_VOL_CHAMBER2_AREA;

	if(ldidbu8ChmBalVlvActFlg == 1) /* Chamber balance valve is NC, Act = 1 -> Open */
	{
		if((ldidbu8RelsVlvActFlg == 0)&&(ldidbs32StrkPosiDelta < 0)) /* Release valve is NC, Act = 0 -> Close */
		{
			ldidbs32StrkPosiDelta = -((ldidbs32StrkPosiDelta*(100-s16ChamberAreaRatio))/100);
		}
		else
		{
			ldidbs32StrkPosiDelta = ((ldidbs32StrkPosiDelta*s16ChamberAreaRatio)/100);
		}
	}
	else
	{
		if((ldidbu8RelsVlvActFlg == 0)&&(ldidbs32StrkPosiDelta < 0)) /* Release valve is NC, Act = 0 -> Close */
		{
			ldidbs32StrkPosiDelta = 0;
		}
		else{}
	}

	return;
}

static void LDIDB_vCircuitBrakeState(void)
{
	LDIDB_vPrimCircBrakeOn();
	LDIDB_vSecdCircBrakeOn();
	LDIDB_vPrimCircBrakeInitOn();
	LDIDB_vSecdCircBrakeInitOn();

	return;
}

static void LDIDB_vPrimCircBrakeOn(void)
{
	if (ldidbu8PrimCircBrakeOnFlg == 0)
	{
		if (ldidbs32PrimCircP > S16_PCTRL_PRESS_1_BAR)
		{
			ldidbu8PrimCircBrakeOnCnt = 0;
			ldidbu8PrimCircBrakeOnFlg = 1;
		}
		else{}
	}
	else
	{
		if (ldidbs32PrimCircP <= S16_PCTRL_PRESS_1_BAR)
		{
			ldidbu8PrimCircBrakeOnCnt++;
			if (ldidbu8PrimCircBrakeOnCnt > 3)
			{
				ldidbu8PrimCircBrakeOnCnt = 0;
				ldidbu8PrimCircBrakeOnFlg = 0;
			}
			else{}
		}
		else
		{
			ldidbu8PrimCircBrakeOnCnt = 0;
		}
	}
	
	return;
}

static void LDIDB_vSecdCircBrakeOn(void)
{
	if (ldidbu8SecdCircBrakeOnFlg == 0)
	{
		if (ldidbs32SecdCircP >= S16_PCTRL_PRESS_1_BAR)
		{
			ldidbu8SecdCircBrakeOnCnt = 0;
			ldidbu8SecdCircBrakeOnFlg = 1;
		}
		else{}
	}
	else
	{
		if (ldidbs32SecdCircP < S16_PCTRL_PRESS_1_BAR)
		{
			ldidbu8SecdCircBrakeOnCnt++;
			if (ldidbu8SecdCircBrakeOnCnt > 3)
			{
				ldidbu8SecdCircBrakeOnCnt = 0;
				ldidbu8SecdCircBrakeOnFlg = 0;
			}
			else{}
		}
		else
		{
			ldidbu8SecdCircBrakeOnCnt = 0;
		}
	}
	
	return;
}

static void LDIDB_vPrimCircBrakeInitOn(void)
{
	uint8_t u8CircBrakeOnChgCondition = 0;

	if((WPE_FL.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_FR.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RL.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RR.ldidbu8WhlPByVolBrakeOnInitFlg==1))
	/* if((WPE_FR.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RL.ldidbu8WhlPByVolBrakeOnInitFlg==1)) */ /* 15SWD HW */
	{
		u8CircBrakeOnChgCondition = 1;
	}
	else{}

	if(ldidbu8PrimCircBrakeOnFlg==1)
	{
		if(ldidbu8PrimCircBrakeOnInitFlg==0)
		{
			if((u8CircBrakeOnChgCondition == 1) && (ldidbu8PrimCircBrakeOnInitCnt == 0))
			{
				ldidbu8PrimCircBrakeOnInitFlg = 1;
				ldidbu8PrimCircBrakeOnInitCnt = 1;
			}
			else{}
		}
		else
		{
			if ((ldidbu8ActvBrkCtrlrActFlg == 1) && (ldidbu8ABSActFlg == 0))
			{
				if(u8CircBrakeOnChgCondition == 1)
				{
					ldidbu8PrimCircBrakeOnInitFlg =0;
				}
				else{}
			}
			else{}
		}
	}
	else
	{
		ldidbu8PrimCircBrakeOnInitFlg = 0;
		ldidbu8PrimCircBrakeOnInitCnt = 0;
	}
	
	return;
}

static void LDIDB_vSecdCircBrakeInitOn(void)
{
	uint8_t u8CircBrakeOnChgCondition = 0;

	if((WPE_FL.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_FR.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RL.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RR.ldidbu8WhlPByVolBrakeOnInitFlg==1))
	/* if((WPE_FL.ldidbu8WhlPByVolBrakeOnInitFlg==1) || (WPE_RR.ldidbu8WhlPByVolBrakeOnInitFlg==1)) */ /* 15SWD HW */
	{
		u8CircBrakeOnChgCondition = 1;
	}
	else{}
	
	if(ldidbu8SecdCircBrakeOnFlg==1)
	{
		if(ldidbu8SecdCircBrakeOnInitFlg==0)
		{
			if((u8CircBrakeOnChgCondition == 1) && (ldidbu8SecdCircBrakeOnInitCnt == 0))
			{
				ldidbu8SecdCircBrakeOnInitFlg = 1;
				ldidbu8SecdCircBrakeOnInitCnt = 1;
			}
			else{}
		}
		else
		{
			if ((ldidbu8ActvBrkCtrlrActFlg == 1) && (ldidbu8ABSActFlg == 0))
			{
				if(u8CircBrakeOnChgCondition == 1)
				{
					ldidbu8SecdCircBrakeOnInitFlg =0;
				}
				else{}
			}
			else{}
		}
	}
	else
	{
		ldidbu8SecdCircBrakeOnInitFlg = 0;
		ldidbu8SecdCircBrakeOnInitCnt = 0;
	}
	
	return;
}

static void LDIDB_vWhlVolumeResetCondition(uint8_t u8CircBrakeOnFlg)
{
	pWPE->ldidbu8WhlVolumeResetFlg = 0;

	#if M_WPE_For_Mux == DISABLE  /* Check valve 유무 매크로로 추후 변경 필요 */
	if((u8CircBrakeOnFlg == 0)&&(ldidbu8BLSFlg == 0))
	{
		if(pWPE->ldidbu8WhlVolumeResetCnt < U8_T_100_MS)
		{
			pWPE->ldidbu8WhlVolumeResetCnt++;

			if(pWPE->ldidbu8WhlVolumeResetCnt == U8_T_50_MS)
			{
				pWPE->ldidbu8WhlVolumeResetFlg = 1;
			}
			else{}
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8WhlVolumeResetCnt = 0;
	}
	#endif
		
	return;
}

static void LDIDB_vWhlVolumePercent(void)
{
	int16_t s16FrontAreaRatio;
	int16_t s16RearAreaRatio;
	int16_t s16TotalInletVolume;
	int16_t s16WheelInletVolume;

	s16FrontAreaRatio = DETcalib.WPE_CAL_FRT.VOL_INLET_AREA_RATIO;
	s16RearAreaRatio  = DETcalib.WPE_CAL_RR.VOL_INLET_AREA_RATIO;
	
	s16TotalInletVolume = ((int16_t)s16FrontAreaRatio*(WPE_FL.ldidbu8InletValveOpenTiForVol + WPE_FR.ldidbu8InletValveOpenTiForVol)) + ((int16_t)s16RearAreaRatio*(WPE_RL.ldidbu8InletValveOpenTiForVol + WPE_RR.ldidbu8InletValveOpenTiForVol));
	s16WheelInletVolume = (int16_t)S16_VOL_INLET_AREA_RATIO*(pWPE->ldidbu8InletValveOpenTiForVol);
	
	if(s16TotalInletVolume>0)
	{
		pWPE->ldidbu8InletVolumePercent	= (uint8_t)(((int32_t)s16WheelInletVolume*100)/s16TotalInletVolume);
	}
	else
	{
		pWPE->ldidbu8InletVolumePercent	= 0;
	}

	return;
}


static void LDIDB_vDetEstmdWhlVolumeByS(uint8_t u8CircBrakeOnInitFlg)
{
	int16_t s16WheelState;
	int16_t s16InletRiseWhlVGain;
	int16_t s16InletReleaseWhlVGain;
	int16_t s16OutletDumpWhlVGain;
	
	pWPE->ldidbs32WhlVolumeBySOld = pWPE->ldidbs32WhlVolumeByS;
	
	s16WheelState = (int16_t)WPE_FL.ldidbu8InletValveOpenTiForVol * WPE_FR.ldidbu8InletValveOpenTiForVol * WPE_RL.ldidbu8InletValveOpenTiForVol * WPE_RR.ldidbu8InletValveOpenTiForVol;
	
	if(ldidbu8ABSActFlg == 1)
	{
		s16InletRiseWhlVGain    = S16_VOL_ABS_RISE_GAIN;
		s16InletReleaseWhlVGain = S16_VOL_ABS_RERISE_GAIN;
		s16OutletDumpWhlVGain   = S16_VOL_ABS_DUMP_GAIN;
	}
	else
	{
		if(s16WheelState > 0) /* 4wheel Volume */
		{
			if(u8CircBrakeOnInitFlg == 0)  /* Rerise in same circuit */
			{
				s16InletRiseWhlVGain    = (S16_VOL_4WHEEL_RISE_GAIN*S16_VOL_4WHEEL_RERISE_GAIN)/100;
				s16InletReleaseWhlVGain = (S16_VOL_4WHEEL_RELEASE_GAIN*S16_VOL_4WHEEL_RERISE_GAIN)/100;
				s16OutletDumpWhlVGain   = S16_VOL_4WHEEL_DUMP_GAIN;
			}
			else   /* First rise in same circuit */
			{
				s16InletRiseWhlVGain    = S16_VOL_4WHEEL_RISE_GAIN;
				s16InletReleaseWhlVGain = S16_VOL_4WHEEL_RELEASE_GAIN;
				s16OutletDumpWhlVGain   = S16_VOL_4WHEEL_DUMP_GAIN;
			}
		}
		else
		{
			if(u8CircBrakeOnInitFlg == 0)  /* Rerise in same circuit */
			{
				s16InletRiseWhlVGain    = (S16_VOL_1WHEEL_RISE_GAIN*S16_VOL_1WHEEL_RERISE_GAIN)/100;
				s16InletReleaseWhlVGain = (S16_VOL_1WHEEL_RELEASE_GAIN*S16_VOL_1WHEEL_RERISE_GAIN)/100;
				s16OutletDumpWhlVGain   = S16_VOL_1WHEEL_DUMP_GAIN;
			}
			else   /* First rise in same circuit */
			{
				s16InletRiseWhlVGain    = S16_VOL_1WHEEL_RISE_GAIN;
				s16InletReleaseWhlVGain = S16_VOL_1WHEEL_RELEASE_GAIN;
				s16OutletDumpWhlVGain   = S16_VOL_1WHEEL_DUMP_GAIN;
			}
		}
	}
	
	if(pWPE->ldidbu8InletValveOpenTiForVol > 0) /* Inlet Open */
	{
		if(ldidbs32StrkPosiDelta >= 0)
		{
			pWPE->ldidbs16WhlVChgViaInletVlvByS = (int16_t)(((int32_t)ldidbs32StrkPosiDelta * pWPE->ldidbu8InletVolumePercent * s16InletRiseWhlVGain)/10000);
		}
		else
		{
			pWPE->ldidbs16WhlVChgViaInletVlvByS = (int16_t)(((int32_t)ldidbs32StrkPosiDelta * pWPE->ldidbu8InletVolumePercent * s16InletReleaseWhlVGain)/10000);
		}
	}
	else
	{
		if(ldidbs32StrkPosiDelta >= 0)
		{
			pWPE->ldidbs16WhlVChgViaInletVlvByS = 0;
		}
		else
		{
			pWPE->ldidbs16WhlVChgViaInletVlvByS = (int16_t)(((int32_t)ldidbs32StrkPosiDelta * pWPE->ldidbu8InletVolumePercent * s16InletReleaseWhlVGain)/10000);
		}
	}
	
	if(pWPE->ldidbu8OutletValveOpenTiForVol > 0) /* Outlet Open */
	{
		pWPE->ldidbs16WhlVChgViaOutletVlvByS = (int16_t)(((int32_t)(L_u16FitSQRT(pWPE->ldidbs32WhlVolumeByS/10)*316)*s16OutletDumpWhlVGain)/10000);
	}
	else
	{
		pWPE->ldidbs16WhlVChgViaOutletVlvByS = 0;
	}
	
	pWPE->ldidbs32WhlVolumeByS = (pWPE->ldidbs32WhlVolumeByS + pWPE->ldidbs16WhlVChgViaInletVlvByS) - pWPE->ldidbs16WhlVChgViaOutletVlvByS;
	
	if(pWPE->ldidbu8WhlVolumeResetFlg == 1)
	{
		pWPE->ldidbs32WhlVolumeByS = 0;
	}
	else{}
		
	#if M_WPE_For_Mux == ENABLE  /* Check valve 유무 매크로로 추후 변경 필요 */
	/*  임시조치 -> 먹스사양에서는 inlet open때 휠볼륨(휠압)보다 포지션 값(서킷압)이 작은 경우가 자주 발생
	if(pWPE->ldidbu8InletValveOpenTiForVol > 0)
	{
		pWPE->ldidbs32WhlVolumeByS = L_s32LimitMinMax(pWPE->ldidbs32WhlVolumeByS, 0, ldidbs32StrokePosition);
	}
	else{}
	*/
		pWPE->ldidbs32WhlVolumeByS = L_s32LimitMinMax(pWPE->ldidbs32WhlVolumeByS, 0, HW_MAX_STROKE_PULSE);
	#else  /* Inlet Check valve 사양은 항상 서킷압력이 높아야 함  */
		pWPE->ldidbs32WhlVolumeByS = L_s32LimitMinMax(pWPE->ldidbs32WhlVolumeByS, 0, ldidbs32StrokePosition);
	#endif

	return;
}

static void LDIDB_vEachWhlPBrakeOnState(uint8_t u8CircBrakeOnFlg)
{
    pWPE->ldidbu8WhlPByVolBrakeOnInitFlg = 0;

    if(u8CircBrakeOnFlg==1)
    {
    	if (pWPE->ldidbu8WhlPByVolBrakeOnFlg == 0)
    	{
    		if (pWPE->ldidbs32WhlVolumeByS > S32_MTRPOSI_0_5MM)
    		{
    			pWPE->ldidbu8WhlPByVolBrakeOnCnt = 0;
    			pWPE->ldidbu8WhlPByVolBrakeOnFlg = 1;
    			pWPE->ldidbu8WhlPByVolBrakeOnInitFlg = 1;
    		}
    		else{}
    	}
    	else
    	{
    		if (pWPE->ldidbs32WhlVolumeByS < S32_MTRPOSI_0_5MM)
    		{
    			pWPE->ldidbu8WhlPByVolBrakeOnCnt++;
    			if (pWPE->ldidbu8WhlPByVolBrakeOnCnt > 3)
    			{
    				pWPE->ldidbu8WhlPByVolBrakeOnCnt   = 0;
    				pWPE->ldidbu8WhlPByVolBrakeOnFlg   = 0;
    			}
    			else{}
    		}
    		else
    		{
    			pWPE->ldidbu8WhlPByVolBrakeOnCnt = 0;
    		}
    	}
	}
	else
    {
		pWPE->ldidbu8WhlPByVolBrakeOnCnt   = 0;
		pWPE->ldidbu8WhlPByVolBrakeOnFlg   = 0;        
    }
	
	return;
}

static void LDIDB_vEachWhlP1stRiseSection(void)
{
	if (pWPE->ldidbu8WhlPByVol1stRiseFlg == 0)
	{
		if ((pWPE->ldidbs32WhlVolumeByS > S32_MTRPOSI_0_2MM)&&(pWPE->ldidbs32WhlVolumeByS < S32_MTRPOSI_0_5MM) && ((pWPE->ldidbs32WhlVolumeByS - pWPE->ldidbs32WhlVolumeBySOld) > 0))
		{
			pWPE->ldidbu8WhlPByVol1stRiseFlg = 1;
			pWPE->ldidbu8WhlPByVol1stRiseCnt = 0;
		}
		else{}
	}
	else
	{
		if (((pWPE->ldidbs32WhlVolumeByS - pWPE->ldidbs32WhlVolumeBySOld) < 0) || (pWPE->ldidbs32WhlVolumeByS < S32_MTRPOSI_0_2MM))
		{
			pWPE->ldidbu8WhlPByVol1stRiseCnt++;
			if (pWPE->ldidbu8WhlPByVol1stRiseCnt > 2)
			{
				pWPE->ldidbu8WhlPByVol1stRiseFlg = 0;
				pWPE->ldidbu8WhlPByVol1stRiseCnt = 0;
			}
			else{}
		}
		else
		{
			pWPE->ldidbu8WhlPByVol1stRiseCnt = 0;
		}
	}
	
	return;
}

static void LDIDB_vDetEstmdWhlPressByS(uint8_t WPE_WL_case)
{

	if((WPE_WL_case == WPE_FL_CASE) || (WPE_WL_case == WPE_FR_CASE))
	{
		pWPE->ldidbs16EstmdWhlPByS = (int16_t)(LDIDB_s32CalcPressByStorke(pWPE->ldidbs32WhlVolumeByS, U16_SEL_F1WHEEL));
	}
	else
	{
		pWPE->ldidbs16EstmdWhlPByS = (int16_t)(LDIDB_s32CalcPressByStorke(pWPE->ldidbs32WhlVolumeByS, U16_SEL_R1WHEEL));
	}

	pWPE->ldidbs16EstmdWhlPByS = L_s16LimitMinMax(pWPE->ldidbs16EstmdWhlPByS, WHEEL_PRESS_MIN, WHEEL_PRESS_MAX);


	return;
}
/* Add for Volume based pressure */

static void LDIDB_vEstEachWhlP(WPE_t *pWPE_tmp, WPE_t *pWPE_Diag_tmp, int32_t s32CircP, int32_t s32PedlSimrP, WPE_CAL_t *pWPE_CAL_tmp, uint8_t WPE_WL_case, uint8_t u8CutVVActFlg, uint8_t u8BalVvActFlg)
{

	pWPE = pWPE_tmp;
	pWPE_Diag = pWPE_Diag_tmp;
	pWPE_CAL = pWPE_CAL_tmp;

	LDIDB_vDetInOutVvState(u8BalVvActFlg);

/* Add for New Concept Pressure based pressure */
	LDIDB_vCalcWhlVChgViaInletVlv(s32CircP);
#if (M_WPE_For_Mux == ENABLE)
	LDIDB_vCalcWhlVChgViaCutVlv(u8CutVVActFlg, s32PedlSimrP);
	LDIDB_vCalcWhlVChgViaBalVlv(u8BalVvActFlg);
#endif
	LDIDB_vCalcWhlVChgViaOutletVlv();
	
	LDIDB_vDetEstmdWhlVolumeByP();
	LDIDB_vDetEstmdWhlPressByP(WPE_WL_case);
	LDIDB_vDetEstmdWhlPState();
	LDIDB_vDetFinalEstmdWhlP(s32CircP, WPE_WL_case);
/* Add for New Concept Pressure based pressure */

	return;
}

/* Add for New Concept Pressure based pressure */
static void LDIDB_vCalcWhlVChgViaInletVlv(int32_t s32PSply)
{
	uint8_t u8InletVlvOpenTime;
	uint16_t u16SqrtDeltaP;
	int16_t s16DeltaP;
	int16_t s16WhlVChg;
	int16_t s16PGainForWhlVEstm;
	int16_t s16EstmdWhlP_tmp;

	u8InletVlvOpenTime = pWPE->ldidbu8InletValveOpenTiCorrd;
	s16EstmdWhlP_tmp = pWPE->ldidbs16EstmdWhlP;
	s16DeltaP = (int16_t)(s32PSply - s16EstmdWhlP_tmp);

	if(s16DeltaP > 0) /*Pressure Rise*/
	{
		u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(s16DeltaP);

		if(ldidbu8ABSActFlg == 0)
		{
			s16PGainForWhlVEstm = S16_Inlet_BBS_Rise_Gain;
		}
		else
		{
			s16PGainForWhlVEstm = S16_Inlet_ABS_Rise_Gain;
		}

		s16WhlVChg = (int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP * u8InletVlvOpenTime) / (100 * WPE_CALC_TIME)); /* (100K * 100sqrt(x) * 5time / (100*5) ) */
	}
	else if(s16DeltaP < 0) /* Pressure Release */
	{
		u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP((-s16DeltaP));

		if(ldidbu8ABSActFlg == 0)
		{
			s16PGainForWhlVEstm = S16_Inlet_BBS_Release_Gain;
		}
		else
		{
			s16PGainForWhlVEstm = S16_Inlet_ABS_Release_Gain;
		}

		#if M_WPE_For_Mux == DISABLE  /* Check valve 유무 매크로로 추후 변경 필요 */
		s16WhlVChg = -((int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP) / 100)); /* Check valve release, Open time is max */ /* (100K * 100sqrt(x) / (100) ) */
		#else
		s16WhlVChg = -((int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP * u8InletVlvOpenTime) / (100 * WPE_CALC_TIME))); /* (100K * 100sqrt(x) * 5time / (100*5) ) */
		#endif
	}
	else
	{
		s16WhlVChg = 0;
	}

	#if INLETVOLUME_LIMIT == DISABLE
	pWPE->ldidbs16WhlVChgViaInletVlv = s16WhlVChg;
	#else
	if((pWPE->ldidbu8WhlPByVol1stRiseFlg == 1) && (ldidbu8ActvBrkCtrlrActFlg == 1) && (ldidbu8ABSActFlg == 0))
	{
		if(s16WhlVChg  > (pWPE->ldidbs16WhlVChgViaInletVlvByS+S16_INLET_LIMIT_MARGIN))
		{
			pWPE->ldidbs16WhlVChgViaInletVlv = pWPE->ldidbs16WhlVChgViaInletVlvByS+S16_INLET_LIMIT_MARGIN;
		}
		else if(s16WhlVChg  < (pWPE->ldidbs16WhlVChgViaInletVlvByS-S16_INLET_LIMIT_MARGIN))
		{
			pWPE->ldidbs16WhlVChgViaInletVlv = pWPE->ldidbs16WhlVChgViaInletVlvByS-S16_INLET_LIMIT_MARGIN;
		}
		else
		{
			pWPE->ldidbs16WhlVChgViaInletVlv = s16WhlVChg;
		}
	}
	else
	{
		pWPE->ldidbs16WhlVChgViaInletVlv = s16WhlVChg;
	}
	#endif

	return;
}

static void LDIDB_vCalcWhlVChgViaCutVlv(uint8_t u8CutVVActFlg, int32_t s32PSply)
{
	int16_t s16PGainForWhlVEstm;
	int16_t s16EstmdWhlP_tmp;
	int16_t s16DeltaP;
	int16_t s16WhlVChg;
	uint16_t u16SqrtDeltaP;

	s16EstmdWhlP_tmp = pWPE->ldidbs16EstmdWhlP;
	s16DeltaP = (int16_t)(s32PSply - s16EstmdWhlP_tmp);

	if(u8CutVVActFlg == NO_VLV_OPENED)
	{
		if(s16DeltaP > 0) /* Pressure Rise */
		{
			u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(s16DeltaP);
			s16PGainForWhlVEstm = S16_Cut_Rise_Gain;
			s16WhlVChg = (int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP) / 100);
		}
		else if(s16DeltaP < 0) /* Pressure Release */
		{
			u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(-s16DeltaP);
			s16PGainForWhlVEstm = S16_Cut_Release_Gain;
			s16WhlVChg = -((int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP) / 100));
		}
		else
		{
			s16WhlVChg=0;
		}
	}
	else
	{
		s16WhlVChg=0;
	}

	pWPE->ldidbs16WhlVChgViaCutVlv = s16WhlVChg;

	return;
}

static void LDIDB_vCalcWhlVChgViaOutletVlv(void)
{
	uint8_t u8OutletVlvOpenTime;
	int16_t s16DeltaP;
	uint16_t u16SqrtDeltaP;
	int16_t s16WhlVChg;
	int16_t s16PGainForWhlVEstm;
	int16_t s16EstmdWhlP_tmp;

	u8OutletVlvOpenTime = pWPE->ldidbu8OutletValveOpenTi;
	s16EstmdWhlP_tmp = pWPE->ldidbs16EstmdWhlP;
	s16DeltaP = s16EstmdWhlP_tmp - S16_WPE_LPA_Pressure;

	if((s16DeltaP > 0) && (u8OutletVlvOpenTime > 0))
	{
		u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(s16DeltaP);
		s16PGainForWhlVEstm = S16_Outlet_Dump_Gain;
		s16WhlVChg = -((int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP * u8OutletVlvOpenTime) / (100 * WPE_CALC_TIME)));
	}
	else
	{
		s16WhlVChg = 0;
	}

	pWPE->ldidbs16WhlVChgViaOutletVlv = s16WhlVChg;

	return;
}

static void LDIDB_vCalcWhlVChgViaBalVlv(uint8_t u1WPEBalVvActFlg)
{
	int16_t s16DeltaP;
	int16_t s16WhlVChg;
	int16_t s16PGainForWhlVEstm;
	int16_t s16EstmdWhlP_tmp;
	int16_t s16EstmdWhlP_diag;
	uint16_t u16SqrtDeltaP;

	s16EstmdWhlP_tmp = pWPE->ldidbs16EstmdWhlP;
	s16EstmdWhlP_diag = pWPE_Diag->ldidbs16EstmdWhlP;
	s16DeltaP = s16EstmdWhlP_diag - s16EstmdWhlP_tmp;

	if(u1WPEBalVvActFlg == NO_VLV_OPENED)
	{
		if(s16DeltaP > 0) /* Pressure Rise From Diag Wheel */
		{
			u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(s16DeltaP);
			s16PGainForWhlVEstm = S16_Bal_Rise_Gain;
			s16WhlVChg = (int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP) / 100);
		}
		else if (s16DeltaP < 0) /* Pressure Release To Diag Wheel */
		{
			u16SqrtDeltaP = LDIDB_u16CalcSqrtDeltaP(-s16DeltaP);
			s16PGainForWhlVEstm = S16_Bal_Release_Gain;
			s16WhlVChg = -((int16_t)(((int32_t)s16PGainForWhlVEstm * u16SqrtDeltaP) / 100));
		}
		else
		{
			s16WhlVChg=0;
		}
	}
	else
	{
		s16WhlVChg=0;
	}

	pWPE->ldidbs16WhlVChgViaBalVlv = s16WhlVChg;

	return;
}

static void LDIDB_vDetEstmdWhlVolumeByP(void)
{
	int16_t s16WhlVChgTotal = 0;

	#if M_WPE_For_Mux == ENABLE
	s16WhlVChgTotal = pWPE->ldidbs16WhlVChgViaInletVlv + pWPE->ldidbs16WhlVChgViaOutletVlv + pWPE->ldidbs16WhlVChgViaCutVlv + pWPE->ldidbs16WhlVChgViaBalVlv;
	#else
	s16WhlVChgTotal = pWPE->ldidbs16WhlVChgViaInletVlv + pWPE->ldidbs16WhlVChgViaOutletVlv;
	#endif

	pWPE->ldidbs32WhlVolumeByP = pWPE->ldidbs32WhlVolumeByP + s16WhlVChgTotal;
	if(pWPE->ldidbs32WhlVolumeByP < 0)
	{
		pWPE->ldidbs32WhlVolumeByP = 0;
	}
	else{}

	return;
}

static void LDIDB_vDetEstmdWhlPressByP(uint8_t WPE_WL_case)
{
	pWPE->ldidbs16EstmdWhlPOld = pWPE->ldidbs16EstmdWhlP;
	
	if((WPE_WL_case == WPE_FL_CASE) || (WPE_WL_case == WPE_FR_CASE))
	{
		pWPE->ldidbs16EstmdWhlP = (int16_t)(LDIDB_s32CalcPressByStorke(pWPE->ldidbs32WhlVolumeByP, U16_SEL_F1WHEEL));
	}
	else
	{
		pWPE->ldidbs16EstmdWhlP = (int16_t)(LDIDB_s32CalcPressByStorke(pWPE->ldidbs32WhlVolumeByP, U16_SEL_R1WHEEL));
	}

	pWPE->ldidbs16EstmdWhlP = L_s16LimitMinMax(pWPE->ldidbs16EstmdWhlP, WHEEL_PRESS_MIN, WHEEL_PRESS_MAX);


	return;
}

static void LDIDB_vDetEstmdWhlPState(void)
{
	pWPE->ldidbs16EstDeltaP    = (int16_t)(((int32_t)pWPE->ldidbs16EstmdWhlP*10) - ((int32_t)pWPE->ldidbs16EstmdWhlPOld*10));
	pWPE->ldidbs16EstDeltaPLPF = L_s16Lpf1Int(pWPE->ldidbs16EstDeltaP, pWPE->ldidbs16EstDeltaPLPF, U8_LPF_3HZ_128G);
	
	if(pWPE->ldidbu8EstPWhlState == S16_PRESS_NONE)
	{
		LDIDB_vDetEstmdWhlPStateNone();
	}
	else if(pWPE->ldidbu8EstPWhlState == S16_PRESS_RISE)
	{
		LDIDB_vDetEstmdWhlPStateRise();
	}
	else if(pWPE->ldidbu8EstPWhlState == S16_PRESS_HOLD)
	{
		LDIDB_vDetEstmdWhlPStateHold();
	}
	else if(pWPE->ldidbu8EstPWhlState == S16_PRESS_DOWN)
	{
		LDIDB_vDetEstmdWhlPStateDown();
	}
	else{}

	if(pWPE->ldidbs16EstmdWhlP <= S16_PCTRL_PRESS_1_BAR)
	{
		pWPE->ldidbu8EstPWhlState = S16_PRESS_NONE;
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}
	else{}

	return;
}

static void LDIDB_vDetEstmdWhlPStateNone(void)
{
	if(pWPE->ldidbs16EstDeltaPLPF > 30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_RISE;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}
	
	return;
}

static void LDIDB_vDetEstmdWhlPStateRise(void)
{
	if(pWPE->ldidbs16EstDeltaPLPF > 30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}
	else if((pWPE->ldidbs16EstDeltaPLPF <= 30) && (pWPE->ldidbs16EstDeltaPLPF >= -30))
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_HOLD;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else if(pWPE->ldidbs16EstDeltaPLPF < -30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_DOWN;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}

	return;
}

static void LDIDB_vDetEstmdWhlPStateHold(void)
{
	if((pWPE->ldidbs16EstDeltaPLPF <= 30) && (pWPE->ldidbs16EstDeltaPLPF >= -30))
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}
	else if(pWPE->ldidbs16EstDeltaPLPF > 30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_RISE;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else if(pWPE->ldidbs16EstDeltaPLPF < -30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_DOWN;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}

	return;
}

static void LDIDB_vDetEstmdWhlPStateDown(void)
{
	if(pWPE->ldidbs16EstDeltaPLPF < -30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}
	else if(pWPE->ldidbs16EstDeltaPLPF > 30)
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_RISE;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else if((pWPE->ldidbs16EstDeltaPLPF <= 30) && (pWPE->ldidbs16EstDeltaPLPF >= -30))
	{
		pWPE->ldidbu8EstPWhlStateChgCnt++;
		if(pWPE->ldidbu8EstPWhlStateChgCnt > 2)
		{
			pWPE->ldidbu8EstPWhlState = S16_PRESS_HOLD;
			pWPE->ldidbu8EstPWhlStateChgCnt = 0;
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8EstPWhlStateChgCnt = 0;
	}

	return;
}

static void LDIDB_vDetFinalEstmdWhlP(int32_t s32PSply, uint8_t WPE_WL_case)
{
	int16_t s16LimitMargin;
	uint8_t u8EstPLimitByCircP = 0;

	if((pWPE->ldidbu8EstPWhlState == S16_PRESS_DOWN) || (ldidbu8ABSActFlg == 1)) 
	{
		s16LimitMargin = S16_EstPLimitByCircP_Margin;
	}
	else if(pWPE->ldidbu8EstPWhlState == S16_PRESS_HOLD)
	{
		s16LimitMargin = S16_EstPLimitByCircP_Margin/2;
	}
	else if(pWPE->ldidbu8EstPWhlState == S16_PRESS_RISE)
	{
		s16LimitMargin = S16_PCTRL_PRESS_1_BAR;
	}
	else
	{
		s16LimitMargin = S16_EstPLimitByCircP_Margin;
	}
	
	#if M_WPE_For_Mux == ENABLE  /* Check valve 유무 매크로로 추후 변경 필요 */
	/*  임시조치 -> 먹스사양에서는 inlet open때 휠볼륨(휠압)보다 포지션 값(서킷압)이 작은 경우가 자주 발생
	if((pWPE->ldidbs16EstmdWhlP > (s32PSply+s16LimitMargin)) && (pWPE->ldidbu8InletValveOpenTiCorrd > 0))
	{
		pWPE->ldidbs16EstmdWhlP = (int16_t)(s32PSply+s16LimitMargin);
		u8EstPLimitByCircP = 1;
	}
	else{}
	*/
	#else  /* Inlet Check valve 사양은 항상 서킷압력이 높아야 함  */
	if(pWPE->ldidbs16EstmdWhlP > (s32PSply+s16LimitMargin))
	{
		pWPE->ldidbs16EstmdWhlP = (int16_t)(s32PSply+s16LimitMargin);
		u8EstPLimitByCircP = 1;
	}
	else{}
	#endif

	pWPE->ldidbs16EstmdWhlP = L_s16LimitMinMax(pWPE->ldidbs16EstmdWhlP, WHEEL_PRESS_MIN, WHEEL_PRESS_MAX);

	if(u8EstPLimitByCircP == 1)
	{
		if((WPE_WL_case == WPE_FL_CASE) || (WPE_WL_case == WPE_FR_CASE))
		{
			pWPE->ldidbs32WhlVolumeByP = LDIDB_s32CalcStorkeByPress(pWPE->ldidbs16EstmdWhlP, U16_SEL_F1WHEEL);
		}
		else
		{
			pWPE->ldidbs32WhlVolumeByP = LDIDB_s32CalcStorkeByPress(pWPE->ldidbs16EstmdWhlP, U16_SEL_R1WHEEL);
		}
	}
	else{}
	
	return;
}
/* Add for New Concept Pressure based pressure */

static uint16_t LDIDB_u16CalcSqrtDeltaP(int16_t s16DeltaPTemp)
{
	static uint16_t u16input_array[SQRT_ARRAY_LENGTH] = {0, 1, 4, 8, 16, 25, 36, 49, 64, 81, 100, 121 ,144, 169, 196};
	static uint16_t u16output_array[SQRT_ARRAY_LENGTH] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
	
	uint16_t u16sqrt_input  = (uint16_t)s16DeltaPTemp;
	uint16_t u16numerator   = 0;
	uint16_t u16denominator = 0;
	uint16_t u16sqrt_output = 0;
	uint16_t u16arraylimit  = 0;
	uint8_t  u8index        = 0;
	uint8_t  u8counter      = 0;

	for(u8index = 0 ; u8index < SQRT_ARRAY_LENGTH ; u8index++)
	{
		if(u16sqrt_input > (u16input_array[u8index]*PRESS_RESOL))
		{
			u8counter++;
		}
		else{}
	}

	if(u8counter == 0)
	{
		u16sqrt_output = u16output_array[0] * SQRT_PRESS_RESOL;
	}
	else if(u8counter == SQRT_ARRAY_LENGTH)
	{
		u16sqrt_output = u16output_array[SQRT_ARRAY_LENGTH - 1] * SQRT_PRESS_RESOL;
	}
	else
	{
		u16numerator = (u16sqrt_input - (u16input_array[u8counter - 1] * SQRT_PRESS_RESOL)) * (u16output_array[u8counter] - u16output_array[u8counter - 1]);
		u16denominator = u16input_array[u8counter] - u16input_array[u8counter-1];
		u16sqrt_output = (u16output_array[u8counter - 1] * SQRT_PRESS_RESOL) + (u16numerator / u16denominator);
	}

	return u16sqrt_output;
}

static void LDIDB_vDetInOutVvState(uint8_t u1WPEBalVvActFlg)
{
	LDIDB_vCountValveActnTi();
	LDIDB_vCorrdValveActnTi();
	LDIDB_vCorrdValveActnTiForVol(u1WPEBalVvActFlg);
	LDIDB_vDecideWhlVvMode();
	LDIDB_vPressureMatching();

	return;
}

static void LDIDB_vCountValveActnTi(void)
{
	static uint8_t wpe_i = 0;

	pWPE->ldidbu8InletValveOpenTiOld  = pWPE->ldidbu8InletValveOpenTi;
	pWPE->ldidbu8OutletValveOpenTiOld = pWPE->ldidbu8OutletValveOpenTi;

	pWPE->ldidbu8OutletValveOpenTi  = 0;
	pWPE->ldidbu8InletValveOpenTi   = 0;
	pWPE->ldidbu8InletValveOpenTiLfc = 0;


	for(wpe_i = 0 ; wpe_i < WPE_CALC_TIME ; wpe_i++)
	{
		#if WHL_IN_VLV_TYPE == NC_VLV
		/* if (pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i] == NC_FULL_ON_DUTY) */ /*valve open*/
		if (pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i] >= NC_CUT_ON_DUTY) /*valve open*/
		{
			pWPE->ldidbu8InletValveOpenTi++;
		}
		else if(pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i] == VALVE_OFF_DUTY)
		{
		    ;
		}
		else
		{
		    pWPE->ldidbu8InletValveOpenTiLfc++;
		    pWPE->ldidbu8InletValveDuty=pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i];
		}
		#elif WHL_IN_VLV_TYPE == NO_VLV
		if (pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i] == VALVE_OFF_DUTY)
		{
			pWPE->ldidbu8InletValveOpenTi++;
		}
		else if(pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i] == NO_FULL_ON_DUTY) /*valve close*/
		{
		    ;
		}
		else
		{
		    pWPE->ldidbu8InletValveOpenTiLfc++;
		    pWPE->ldidbu8InletValveDuty = pWPE->ldidbs32InletVlvDrvReqOutpI[wpe_i];
		}
		#else
		pWPE->ldidbu8InletValveOpenTi = 0;
		pWPE->ldidbu8InletValveOpenTiLfc = 0;
		#endif

		#if WHL_OUT_VLV_TYPE == NC_VLV
		if (pWPE->ldidbs32OutletVlvDrvReqOutpI[wpe_i] == NC_FULL_ON_DUTY)
		{
			pWPE->ldidbu8OutletValveOpenTi++;
		}
		else if(pWPE->ldidbs32OutletVlvDrvReqOutpI[wpe_i] == VALVE_OFF_DUTY)
		{
			;
		}
		else{} /*T.B.D*/
		#elif WHL_OUT_VLV_TYPE == NO_VLV

		if(pWPE->ldidbs32OutletVlvDrvReqOutpI[wpe_i] == VALVE_OFF_DUTY)
		{
			pWPE->ldidbu8OutletValveOpenTi++;
		}
		else if (pWPE->ldidbs32OutletVlvDrvReqOutpI[wpe_i] == NO_FULL_ON_DUTY)
		{
			;
		}
		else{} /*T.B.D*/
		#else
		pWPE->ldidbu8OutletValveOpenTi=0;
		#endif
	}

	return;
}

static void LDIDB_vCorrdValveActnTi(void)
{
	/* Correct invalid time of inlet valve  */
	if((pWPE->ldidbu8InletValveOpenTiOld == 0) && (pWPE->ldidbu8InletValveOpenTi > 0))         /* Open */
	{
		if(pWPE->ldidbu8InletValveOpenTi > S16_VALVE_INVALID_RISE_TIME)
		{
			pWPE->ldidbu8InletValveOpenTiCorrd = pWPE->ldidbu8InletValveOpenTiCorrd + (pWPE->ldidbu8InletValveOpenTi - S16_VALVE_INVALID_RISE_TIME);
		}
		else
		{
			pWPE->ldidbu8InletValveOpenTiCorrd = 0;
		}
	}
	else if((pWPE->ldidbu8InletValveOpenTiOld > 0) && (pWPE->ldidbu8InletValveOpenTi > 0))     /* Continued Open */
	{
		pWPE->ldidbu8InletValveOpenTiCorrd = pWPE->ldidbu8InletValveOpenTi;
	}
	else if((pWPE->ldidbu8InletValveOpenTiOld > 0) && (pWPE->ldidbu8InletValveOpenTi == 0))    /* Close */
	{
		if(pWPE->ldidbu8InletValveOpenTiCorrd > (S16_VALVE_VALID_RELEASE_TIME))
		{
			pWPE->ldidbu8InletValveOpenTiCorrd = pWPE->ldidbu8InletValveOpenTiCorrd - (S16_VALVE_VALID_RELEASE_TIME);
		}
		else
		{
			pWPE->ldidbu8InletValveOpenTiCorrd = 0;
		}
	}
	else /*((pWPE->ldidbu8InletValveOpenTiOld == 0) && (pWPE->ldidbu8InletValveOpenTi == 0))*/  /* Continued Close */
	{
		pWPE->ldidbu8InletValveOpenTiCorrd = 0;
	}
	
	/* Correct invalid time of outlet valve  */
	pWPE->ldidbu8OutletValveOpenTiCorrd = pWPE->ldidbu8OutletValveOpenTi;

	return;
}

static void LDIDB_vCorrdValveActnTiForVol(uint8_t u8CircBalVvActFlg)
{
#if M_WPE_For_Mux == DISABLE
	pWPE->ldidbu8InletValveOpenTiForVol  = pWPE->ldidbu8InletValveOpenTiCorrd;
	pWPE->ldidbu8OutletValveOpenTiForVol = pWPE->ldidbu8OutletValveOpenTiCorrd;
#else
	/* Inlet */
	if((pWPE->ldidbu8InletValveOpenTiCorrd == 0) && (pWPE_Diag->ldidbu8InletValveOpenTiCorrd > 0) && (u8CircBalVvActFlg == NO_VLV_OPENED))
	{
		pWPE->ldidbu8InletValveOpenTiForVol = pWPE_Diag->ldidbu8InletValveOpenTiCorrd;
	}
	else
	{
		pWPE->ldidbu8InletValveOpenTiForVol = pWPE->ldidbu8InletValveOpenTiCorrd;
	}
	/* Outlet */
	if((pWPE->ldidbu8OutletValveOpenTiCorrd == 0) && (pWPE_Diag->ldidbu8OutletValveOpenTiCorrd > 0) && (u8CircBalVvActFlg == NO_VLV_OPENED))
	{
		pWPE->ldidbu8OutletValveOpenTiForVol = pWPE_Diag->ldidbu8OutletValveOpenTiCorrd;
	}
	else
	{
		pWPE->ldidbu8OutletValveOpenTiForVol = pWPE->ldidbu8OutletValveOpenTiCorrd;
	}
#endif	/* M_WPE_For_Mux == DISABLE */
	
	return;
}

static void LDIDB_vDecideWhlVvMode(void)
{
	pWPE->ldidbu8Wheel_Valve_Mode_old = pWPE->ldidbu8Wheel_Valve_Mode;
	pWPE->ldidbu8ContinuedFullRiseModeFlg = 0;
	pWPE->ldidbu8ContinuedDumpModeFlg = 0;

	if( (pWPE->ldidbu8InletValveOpenTi > 0) && (pWPE->ldidbu8OutletValveOpenTi == 0))
	{
		pWPE->ldidbu8Wheel_Valve_Mode = WHEEL_PRESS_APPLY;

		if(pWPE->ldidbu8Wheel_Valve_Mode_old == WHEEL_PRESS_APPLY)
		{
			pWPE->ldidbu8ContinuedFullRiseModeFlg = 1;
		}
		else{}
	}
	else if((pWPE->ldidbu8InletValveOpenTi == 0) && (pWPE->ldidbu8OutletValveOpenTi > 0))
	{
		pWPE->ldidbu8Wheel_Valve_Mode = WHEEL_PRESS_DUMP;

		if(pWPE->ldidbu8Wheel_Valve_Mode_old == WHEEL_PRESS_DUMP)
		{
			pWPE->ldidbu8ContinuedDumpModeFlg = 1;
		}
		else{}
	}
	else if ((pWPE->ldidbu8InletValveOpenTi == 0) && (pWPE->ldidbu8OutletValveOpenTi == 0))
	{
		pWPE->ldidbu8Wheel_Valve_Mode = WHEEL_PRESS_HOLD;
	}
	else if ( (pWPE->ldidbu8InletValveOpenTi > 0) && (pWPE->ldidbu8OutletValveOpenTi > 0))
	{
		pWPE->ldidbu8Wheel_Valve_Mode = WHEEL_PRESS_CIRCULATION;
	}
	else
	{
		pWPE->ldidbu8Wheel_Valve_Mode = WHEEL_PRESS_NOM;
	}
	
	return;
}

static void LDIDB_vPressureMatching(void)
{
	if(pWPE->ldidbu8Wheel_Valve_Mode == WHEEL_PRESS_APPLY)
	{
		if(pWPE->ldidbu8InletValveOpenTi >= WPE_CALC_TIME)
		{
			if(pWPE->ldidbu8wpe_press_match_chk_cnt < 100)
			{
				pWPE->ldidbu8wpe_press_match_chk_cnt++;

				if(pWPE->ldidbu8wpe_press_match_chk_cnt >= WPE_PRESS_MATCH_CHK_T )
				{
					pWPE->ldidbu8u1PressMatchSusFlg=1;
				}
				else
				{
					pWPE->ldidbu8u1PressMatchSusFlg=0;
				}
			}
			else{}
		}
		else{}
	}
	else
	{
		pWPE->ldidbu8u1PressMatchSusFlg = 0;
		pWPE->ldidbu8wpe_press_match_chk_cnt = 0;
	}

	return;
}

static int16_t LDIDB_s16CalcWpeGain(int16_t Pwhl_tmp, int16_t* WpePressArr, int16_t* WpeGainArr, uint8_t WpeArraySize)
{
	int16_t wpe_intp_tmp0;
	int16_t wpe_intp_tmp1;
	int8_t arr_cnt;
	int8_t Pwhl_inx;

	Pwhl_inx = 0;

	for(arr_cnt = 0 ; arr_cnt < WpeArraySize ; arr_cnt++)
	{
		if(Pwhl_tmp > WpePressArr[arr_cnt])
		{
			Pwhl_inx++;
		}
		else{}
	}

	if(Pwhl_inx == 0)
	{
		wpe_intp_tmp1 = WpeGainArr[0];
	}
	else if (Pwhl_inx >= WpeArraySize)
	{
		wpe_intp_tmp1 = WpeGainArr[WpeArraySize-1];
	}
	else
	{
        wpe_intp_tmp0 = (WpePressArr[Pwhl_inx]-WpePressArr[Pwhl_inx-1]);

        if (wpe_intp_tmp0 == 0)
        {
        	wpe_intp_tmp0 = 1;
        }
        else{}

        wpe_intp_tmp1 =  WpeGainArr[Pwhl_inx-1] + (int16_t)(((int32_t)(WpeGainArr[Pwhl_inx] - WpeGainArr[Pwhl_inx-1]) * (Pwhl_tmp - WpePressArr[Pwhl_inx-1])) / wpe_intp_tmp0);
	}

	if(wpe_intp_tmp1 < 0)
	{
		wpe_intp_tmp1 = 0;
	}
	else{}

    return wpe_intp_tmp1;
}

#define DET_5MSCTRL_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
