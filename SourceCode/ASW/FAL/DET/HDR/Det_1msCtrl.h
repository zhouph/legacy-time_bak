/**
 * @defgroup Det_1msCtrl Det_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_1msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_1MSCTRL_H_
#define DET_1MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_Types.h"
#include "Det_Cfg.h"
#include "Det_Cal.h"
#include "SwcCommonFunction.h"
#include "L_CommonFunction.h"
#include "L_CommonFunction_1ms.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define DET_1MSCTRL_MODULE_ID      (0)
 #define DET_1MSCTRL_MAJOR_VERSION  (2)
 #define DET_1MSCTRL_MINOR_VERSION  (0)
 #define DET_1MSCTRL_PATCH_VERSION  (0)
 #define DET_1MSCTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Det_1msCtrl_HdrBusType Det_1msCtrlBus;

/* Internal Logic Bus */

extern IDBDetection_SRCbusType    DETsrcbus;
/* Version Info */
extern const SwcVersionInfo_t Det_1msCtrlVersionInfo;

/* Input Data Element */
extern Spc_1msCtrlCircPFild1msInfo_t Det_1msCtrlCircPFild1msInfo;
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Det_1msCtrlPedlTrvlFild1msInfo;
extern Spc_1msCtrlPistPFild1msInfo_t Det_1msCtrlPistPFild1msInfo;
extern Mom_HndlrEcuModeSts_t Det_1msCtrlEcuModeSts;

/* Output Data Element */
extern Det_1msCtrlBrkPedlStatus1msInfo_t Det_1msCtrlBrkPedlStatus1msInfo;
extern Det_1msCtrlCircPRate1msInfo_t Det_1msCtrlCircPRate1msInfo;
extern Det_1msCtrlPedlTrvlRate1msInfo_t Det_1msCtrlPedlTrvlRate1msInfo;
extern Det_1msCtrlPistPRate1msInfo_t Det_1msCtrlPistPRate1msInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Det_1msCtrl_Init(void);
extern void Det_1msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_1MSCTRL_H_ */
/** @} */
