/**
 * @defgroup Det_5msCtrl Det_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_5msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_5MSCTRL_H_
#define DET_5MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_Types.h"
#include "Det_Cfg.h"
#include "Det_Cal.h"
#include "SwcCommonFunction.h"
#include "L_CommonFunction.h"
#include "L_CommonFunction_1ms.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define DET_5MSCTRL_MODULE_ID      (0)
 #define DET_5MSCTRL_MAJOR_VERSION  (2)
 #define DET_5MSCTRL_MINOR_VERSION  (0)
 #define DET_5MSCTRL_PATCH_VERSION  (0)
 #define DET_5MSCTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Det_5msCtrl_HdrBusType Det_5msCtrlBus;

/* Internal Logic Bus */

extern IDBDetection_SRCbusType    DETsrcbus;
/* Version Info */
extern const SwcVersionInfo_t Det_5msCtrlVersionInfo;

/* Input Data Element */
extern Abc_CtrlWhlVlvReqAbcInfo_t Det_5msCtrlWhlVlvReqAbcInfo;
extern Vat_CtrlNormVlvReqVlvActInfo_t Det_5msCtrlNormVlvReqVlvActInfo;
extern Vat_CtrlWhlVlvReqIdbInfo_t Det_5msCtrlWhlVlvReqIdbInfo;
extern Vat_CtrlIdbBalVlvReqInfo_t Det_5msCtrlIdbBalVlvReqInfo;
extern Eem_MainEemFailData_t Det_5msCtrlEemFailData;
extern Proxy_RxCanRxIdbInfo_t Det_5msCtrlCanRxIdbInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Det_5msCtrlMotRotgAgSigInfo;
extern Pedal_SenSyncPdt5msRawInfo_t Det_5msCtrlPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Det_5msCtrlPdf5msRawInfo;
extern Abc_CtrlAbsCtrlInfo_t Det_5msCtrlAbsCtrlInfo;
extern Spc_5msCtrlCircPFildInfo_t Det_5msCtrlCircPFildInfo;
extern Spc_5msCtrlCircPOffsCorrdInfo_t Det_5msCtrlCircPOffsCorrdInfo;
extern Spc_5msCtrlPedlSimrPFildInfo_t Det_5msCtrlPedlSimrPFildInfo;
extern Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Det_5msCtrlPedlSimrPOffsCorrdInfo;
extern Spc_5msCtrlPedlTrvlFildInfo_t Det_5msCtrlPedlTrvlFildInfo;
extern Spc_5msCtrlPistPFildInfo_t Det_5msCtrlPistPFildInfo;
extern Spc_5msCtrlPistPOffsCorrdInfo_t Det_5msCtrlPistPOffsCorrdInfo;
extern Spc_5msCtrlWhlSpdFildInfo_t Det_5msCtrlWhlSpdFildInfo;
extern Eem_MainEemSuspectData_t Det_5msCtrlEemSuspectData;
extern Mom_HndlrEcuModeSts_t Det_5msCtrlEcuModeSts;
extern Pct_5msCtrlPCtrlAct_t Det_5msCtrlPCtrlAct;
extern Abc_CtrlActvBrkCtrlrActFlg_t Det_5msCtrlActvBrkCtrlrActFlg;
extern Abc_CtrlVehSpd_t Det_5msCtrlVehSpd;
extern Spc_5msCtrlBlsFild_t Det_5msCtrlBlsFild;
extern Pct_5msCtrlPCtrlBoostMod_t Det_5msCtrlPCtrlBoostMod;

/* Output Data Element */
extern Det_5msCtrlBrkPedlStatusInfo_t Det_5msCtrlBrkPedlStatusInfo;
extern Det_5msCtrlCircPRateInfo_t Det_5msCtrlCircPRateInfo;
extern Det_5msCtrlEstimdWhlPInfo_t Det_5msCtrlEstimdWhlPInfo;
extern Det_5msCtrlPedlTrvlRateInfo_t Det_5msCtrlPedlTrvlRateInfo;
extern Det_5msCtrlPedlTrvlTagInfo_t Det_5msCtrlPedlTrvlTagInfo;
extern Det_5msCtrlPistPRateInfo_t Det_5msCtrlPistPRateInfo;
extern Det_5msCtrlVehStopStInfo_t Det_5msCtrlVehStopStInfo;
extern Det_5msCtrlPedlTrvlFinal_t Det_5msCtrlPedlTrvlFinal;
extern Det_5msCtrlVehSpdFild_t Det_5msCtrlVehSpdFild;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Det_5msCtrl_Init(void);
extern void Det_5msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_5MSCTRL_H_ */
/** @} */
