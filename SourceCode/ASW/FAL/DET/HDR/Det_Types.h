/**
 * @defgroup Det_Types Det_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_TYPES_H_
#define DET_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Spc_1msCtrlCircPFild1msInfo_t Det_1msCtrlCircPFild1msInfo;
    Spc_1msCtrlPedlTrvlFild1msInfo_t Det_1msCtrlPedlTrvlFild1msInfo;
    Spc_1msCtrlPistPFild1msInfo_t Det_1msCtrlPistPFild1msInfo;
    Mom_HndlrEcuModeSts_t Det_1msCtrlEcuModeSts;

/* Output Data Element */
    Det_1msCtrlBrkPedlStatus1msInfo_t Det_1msCtrlBrkPedlStatus1msInfo;
    Det_1msCtrlCircPRate1msInfo_t Det_1msCtrlCircPRate1msInfo;
    Det_1msCtrlPedlTrvlRate1msInfo_t Det_1msCtrlPedlTrvlRate1msInfo;
    Det_1msCtrlPistPRate1msInfo_t Det_1msCtrlPistPRate1msInfo;
}Det_1msCtrl_HdrBusType;

typedef struct
{
/* Input Data Element */
    Abc_CtrlWhlVlvReqAbcInfo_t Det_5msCtrlWhlVlvReqAbcInfo;
    Vat_CtrlNormVlvReqVlvActInfo_t Det_5msCtrlNormVlvReqVlvActInfo;
    Vat_CtrlWhlVlvReqIdbInfo_t Det_5msCtrlWhlVlvReqIdbInfo;
    Vat_CtrlIdbBalVlvReqInfo_t Det_5msCtrlIdbBalVlvReqInfo;
    Eem_MainEemFailData_t Det_5msCtrlEemFailData;
    Proxy_RxCanRxIdbInfo_t Det_5msCtrlCanRxIdbInfo;
    Msp_CtrlMotRotgAgSigInfo_t Det_5msCtrlMotRotgAgSigInfo;
    Pedal_SenSyncPdt5msRawInfo_t Det_5msCtrlPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Det_5msCtrlPdf5msRawInfo;
    Abc_CtrlAbsCtrlInfo_t Det_5msCtrlAbsCtrlInfo;
    Spc_5msCtrlCircPFildInfo_t Det_5msCtrlCircPFildInfo;
    Spc_5msCtrlCircPOffsCorrdInfo_t Det_5msCtrlCircPOffsCorrdInfo;
    Spc_5msCtrlPedlSimrPFildInfo_t Det_5msCtrlPedlSimrPFildInfo;
    Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Det_5msCtrlPedlSimrPOffsCorrdInfo;
    Spc_5msCtrlPedlTrvlFildInfo_t Det_5msCtrlPedlTrvlFildInfo;
    Spc_5msCtrlPistPFildInfo_t Det_5msCtrlPistPFildInfo;
    Spc_5msCtrlPistPOffsCorrdInfo_t Det_5msCtrlPistPOffsCorrdInfo;
    Spc_5msCtrlWhlSpdFildInfo_t Det_5msCtrlWhlSpdFildInfo;
    Eem_MainEemSuspectData_t Det_5msCtrlEemSuspectData;
    Mom_HndlrEcuModeSts_t Det_5msCtrlEcuModeSts;
    Pct_5msCtrlPCtrlAct_t Det_5msCtrlPCtrlAct;
    Abc_CtrlActvBrkCtrlrActFlg_t Det_5msCtrlActvBrkCtrlrActFlg;
    Abc_CtrlVehSpd_t Det_5msCtrlVehSpd;
    Spc_5msCtrlBlsFild_t Det_5msCtrlBlsFild;
    Pct_5msCtrlPCtrlBoostMod_t Det_5msCtrlPCtrlBoostMod;

/* Output Data Element */
    Det_5msCtrlBrkPedlStatusInfo_t Det_5msCtrlBrkPedlStatusInfo;
    Det_5msCtrlCircPRateInfo_t Det_5msCtrlCircPRateInfo;
    Det_5msCtrlEstimdWhlPInfo_t Det_5msCtrlEstimdWhlPInfo;
    Det_5msCtrlPedlTrvlRateInfo_t Det_5msCtrlPedlTrvlRateInfo;
    Det_5msCtrlPedlTrvlTagInfo_t Det_5msCtrlPedlTrvlTagInfo;
    Det_5msCtrlPistPRateInfo_t Det_5msCtrlPistPRateInfo;
    Det_5msCtrlVehStopStInfo_t Det_5msCtrlVehStopStInfo;
    Det_5msCtrlPedlTrvlFinal_t Det_5msCtrlPedlTrvlFinal;
    Det_5msCtrlVehSpdFild_t Det_5msCtrlVehSpdFild;
}Det_5msCtrl_HdrBusType;
typedef struct
{
    int32_t EstimdPedlTrvlUsingPedlSimrP;
    uint16_t SystemOnCnt;
    uint16_t SystemOnCnt1ms;

    int32_t PistP1msSigRate;
    int32_t PrimP1msSigRate;
    int32_t SecdP1msSigRate;
    int32_t PedlTrvl1msSigRate;
    int32_t SpeedInvalidFlg;
    int32_t VehSpdFild;
}IDBDetection_SRCbusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_TYPES_H_ */
/** @} */
