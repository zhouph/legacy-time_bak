Det_5msCtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    };
Det_5msCtrlWhlVlvReqAbcInfo = CreateBus(Det_5msCtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Det_5msCtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Det_5msCtrlNormVlvReqVlvActInfo = CreateBus(Det_5msCtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Det_5msCtrlWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Det_5msCtrlWhlVlvReqIdbInfo = CreateBus(Det_5msCtrlWhlVlvReqIdbInfo, DeList);
clear DeList;

Det_5msCtrlIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Det_5msCtrlIdbBalVlvReqInfo = CreateBus(Det_5msCtrlIdbBalVlvReqInfo, DeList);
clear DeList;

Det_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SimP'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Det_5msCtrlEemFailData = CreateBus(Det_5msCtrlEemFailData, DeList);
clear DeList;

Det_5msCtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'HcuServiceMod'
    };
Det_5msCtrlCanRxIdbInfo = CreateBus(Det_5msCtrlCanRxIdbInfo, DeList);
clear DeList;

Det_5msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    };
Det_5msCtrlMotRotgAgSigInfo = CreateBus(Det_5msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Det_5msCtrlPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Det_5msCtrlPdt5msRawInfo = CreateBus(Det_5msCtrlPdt5msRawInfo, DeList);
clear DeList;

Det_5msCtrlPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    };
Det_5msCtrlPdf5msRawInfo = CreateBus(Det_5msCtrlPdf5msRawInfo, DeList);
clear DeList;

Det_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Det_5msCtrlAbsCtrlInfo = CreateBus(Det_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Det_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild'
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild'
    'SecdCircPFild_1_100Bar'
    };
Det_5msCtrlCircPFildInfo = CreateBus(Det_5msCtrlCircPFildInfo, DeList);
clear DeList;

Det_5msCtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'SecdCircPOffsCorrd'
    };
Det_5msCtrlCircPOffsCorrdInfo = CreateBus(Det_5msCtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Det_5msCtrlPedlSimrPFildInfo = CreateBus(Det_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Det_5msCtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    };
Det_5msCtrlPedlSimrPOffsCorrdInfo = CreateBus(Det_5msCtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdfFild'
    'PdtFild'
    };
Det_5msCtrlPedlTrvlFildInfo = CreateBus(Det_5msCtrlPedlTrvlFildInfo, DeList);
clear DeList;

Det_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild'
    'PistPFild_1_100Bar'
    };
Det_5msCtrlPistPFildInfo = CreateBus(Det_5msCtrlPistPFildInfo, DeList);
clear DeList;

Det_5msCtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    };
Det_5msCtrlPistPOffsCorrdInfo = CreateBus(Det_5msCtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Det_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildFrntLe'
    'WhlSpdFildFrntRi'
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Det_5msCtrlWhlSpdFildInfo = CreateBus(Det_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Det_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SimP'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    };
Det_5msCtrlEemSuspectData = CreateBus(Det_5msCtrlEemSuspectData, DeList);
clear DeList;

Det_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Det_5msCtrlEcuModeSts'};
Det_5msCtrlEcuModeSts = CreateBus(Det_5msCtrlEcuModeSts, DeList);
clear DeList;

Det_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Det_5msCtrlPCtrlAct'};
Det_5msCtrlPCtrlAct = CreateBus(Det_5msCtrlPCtrlAct, DeList);
clear DeList;

Det_5msCtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Det_5msCtrlActvBrkCtrlrActFlg'};
Det_5msCtrlActvBrkCtrlrActFlg = CreateBus(Det_5msCtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Det_5msCtrlVehSpd = Simulink.Bus;
DeList={'Det_5msCtrlVehSpd'};
Det_5msCtrlVehSpd = CreateBus(Det_5msCtrlVehSpd, DeList);
clear DeList;

Det_5msCtrlBlsFild = Simulink.Bus;
DeList={'Det_5msCtrlBlsFild'};
Det_5msCtrlBlsFild = CreateBus(Det_5msCtrlBlsFild, DeList);
clear DeList;

Det_5msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Det_5msCtrlPCtrlBoostMod'};
Det_5msCtrlPCtrlBoostMod = CreateBus(Det_5msCtrlPCtrlBoostMod, DeList);
clear DeList;

Det_5msCtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'BrkPedlStatus'
    'DrvrIntendToRelsBrk'
    'PanicBrkStFlg'
    'PedlReldStFlg1'
    'PedlReldStFlg2'
    'PedlReldStUsingPedlSimrPFlg'
    };
Det_5msCtrlBrkPedlStatusInfo = CreateBus(Det_5msCtrlBrkPedlStatusInfo, DeList);
clear DeList;

Det_5msCtrlCircPRateInfo = Simulink.Bus;
DeList={
    'PrimCircPChgDurg10ms'
    'PrimCircPChgDurg5ms'
    'PrimCircPRate'
    'PrimCircPRateBarPerSec'
    'SecdCircPChgDurg10ms'
    'SecdCircPChgDurg5ms'
    'SecdCircPRate'
    'SecdCircPRateBarPerSec'
    };
Det_5msCtrlCircPRateInfo = CreateBus(Det_5msCtrlCircPRateInfo, DeList);
clear DeList;

Det_5msCtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Det_5msCtrlEstimdWhlPInfo = CreateBus(Det_5msCtrlEstimdWhlPInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate'
    'PedlTrvlRateMilliMtrPerSec'
    };
Det_5msCtrlPedlTrvlRateInfo = CreateBus(Det_5msCtrlPedlTrvlRateInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PdfSigNoiseSuspcFlgH'
    'PdfSigNoiseSuspcFlgL'
    'PdfSigTag'
    'PdtSigNoiseSuspcFlgH'
    'PdtSigNoiseSuspcFlgL'
    'PdtSigTag'
    'PedlSigTag'
    'PedlSimrPSigNoiseSuspcFlgH'
    'PedlSimrPSigNoiseSuspcFlgL'
    };
Det_5msCtrlPedlTrvlTagInfo = CreateBus(Det_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Det_5msCtrlPistPRateInfo = Simulink.Bus;
DeList={
    'PistPChgDurg10ms'
    'PistPChgDurg5ms'
    'PistPRate'
    'PistPRateBarPerSec'
    };
Det_5msCtrlPistPRateInfo = CreateBus(Det_5msCtrlPistPRateInfo, DeList);
clear DeList;

Det_5msCtrlVehStopStInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    'VehStopStFlg'
    };
Det_5msCtrlVehStopStInfo = CreateBus(Det_5msCtrlVehStopStInfo, DeList);
clear DeList;

Det_5msCtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Det_5msCtrlPedlTrvlFinal'};
Det_5msCtrlPedlTrvlFinal = CreateBus(Det_5msCtrlPedlTrvlFinal, DeList);
clear DeList;

Det_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Det_5msCtrlVehSpdFild'};
Det_5msCtrlVehSpdFild = CreateBus(Det_5msCtrlVehSpdFild, DeList);
clear DeList;

