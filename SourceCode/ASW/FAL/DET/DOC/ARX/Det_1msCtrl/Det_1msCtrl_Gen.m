Det_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Det_1msCtrlCircPFild1msInfo = CreateBus(Det_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Det_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Det_1msCtrlPedlTrvlFild1msInfo = CreateBus(Det_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Det_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Det_1msCtrlPistPFild1msInfo = CreateBus(Det_1msCtrlPistPFild1msInfo, DeList);
clear DeList;

Det_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Det_1msCtrlEcuModeSts'};
Det_1msCtrlEcuModeSts = CreateBus(Det_1msCtrlEcuModeSts, DeList);
clear DeList;

Det_1msCtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Det_1msCtrlBrkPedlStatus1msInfo = CreateBus(Det_1msCtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Det_1msCtrlCircPRate1msInfo = Simulink.Bus;
DeList={
    'PrimCircPRate1msAvg_1_100Bar'
    'SecdCircPRate1msAvg_1_100Bar'
    };
Det_1msCtrlCircPRate1msInfo = CreateBus(Det_1msCtrlCircPRate1msInfo, DeList);
clear DeList;

Det_1msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Det_1msCtrlPedlTrvlRate1msInfo = CreateBus(Det_1msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;

Det_1msCtrlPistPRate1msInfo = Simulink.Bus;
DeList={
    'PistPRate1msAvg_1_100Bar'
    };
Det_1msCtrlPistPRate1msInfo = CreateBus(Det_1msCtrlPistPRate1msInfo, DeList);
clear DeList;

