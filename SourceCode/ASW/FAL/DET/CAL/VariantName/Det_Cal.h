/**
 * @defgroup Det_Cal Det_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef DET_CAL_H_
#define DET_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define WPE_CAL_ARRAY_SIZE 	(13)
#define WPE_CALC_TIME 		(5)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct{
	/*Pressure based*/
	int16_t Inlet_BBS_Rise_Gain;
	int16_t Inlet_BBS_Release_Gain;
	int16_t Inlet_ABS_Rise_Gain;
	int16_t Inlet_ABS_Release_Gain;
	int16_t Outlet_Dump_Gain;
	int16_t Cut_Rise_Gain;
	int16_t Cut_Release_Gain;
	int16_t Bal_Rise_Gain;
	int16_t Bal_Release_Gain;
	int16_t EstPLimitByCircP_Margin;

	/*Volume based*/
	int16_t VOL_4WHEEL_RISE_GAIN;
	int16_t VOL_4WHEEL_RELEASE_GAIN;
	int16_t VOL_4WHEEL_RERISE_GAIN;
	int16_t VOL_4WHEEL_DUMP_GAIN;
	int16_t VOL_1WHEEL_RISE_GAIN;
	int16_t VOL_1WHEEL_RELEASE_GAIN;
	int16_t VOL_1WHEEL_RERISE_GAIN;
	int16_t VOL_1WHEEL_DUMP_GAIN;
	int16_t VOL_ABS_RISE_GAIN;
	int16_t VOL_ABS_RERISE_GAIN;
	int16_t VOL_ABS_DUMP_GAIN;
	int16_t VOL_INLET_AREA_RATIO;
	int16_t INLET_LIMIT_MARGIN;
	int16_t VALVE_INVALID_RISE_TIME;
	int16_t VALVE_VALID_RELEASE_TIME;

	/*TODO*/
    int16_t ABS_APPLY_TIME_GAIN[WPE_CALC_TIME];
    int16_t ABS_RELEASE_TIME_GAIN[WPE_CALC_TIME];
	int16_t ABS_DUMP_TIME_GAIN[WPE_CALC_TIME];
    int16_t ABS_APPLY_TIME_GAIN_INIT_RISE[WPE_CALC_TIME];

    int16_t ABS_APPLY_TIME_GAIN_CONTINUE[WPE_CALC_TIME];
    int16_t ABS_RELEASE_TIME_GAIN_CONTINUE[WPE_CALC_TIME];
    int16_t ABS_DUMP_TIME_GAIN_CONTINUE[WPE_CALC_TIME];

}WPE_CAL_t;

typedef struct
{
    int16_t     	S16_PSP_PEDAL_1P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 1 Point ] */
                                                     /* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_2P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 2 Point ] */
                                                     /* ScaleVal[      21 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_3P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 3 Point ] */
                                                     /* ScaleVal[      27 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_4P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 4 Point ] */
                                                     /* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_5P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 5 Point ] */
                                                     /* ScaleVal[      33 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_6P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 6 Point ] */
                                                     /* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_7P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 7 Point ] */
                                                     /* ScaleVal[      51 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_8P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 8 Point ] */
                                                     /* ScaleVal[      64 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PEDAL_9P;                     /* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 9 Point ] */
                                                     /* ScaleVal[      70 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    int16_t     	S16_PSP_PRESS_1P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      1 Point ] */
                                                     /* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_2P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      2 Point ] */
                                                     /* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_3P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      3 Point ] */
                                                     /* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_4P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      4 Point ] */
                                                     /* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_5P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      5 Point ] */
                                                     /* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_6P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      6 Point ] */
                                                     /* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_7P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      7 Point ] */
                                                     /* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_8P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      8 Point ] */
                                                     /* ScaleVal[     16 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    int16_t     	S16_PSP_PRESS_9P;                     /* Comment [ Pedal Simulator Pressure Model : Pressure -      9 Point ] */
                                                     /* ScaleVal[     24 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */

    int16_t         S16_AHBON_PD_OFFST_INVALID;         /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Active Threshold - Logic ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_AHBON_PD_OFFST_INVALID_WBLS;    /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Active Threshold - Logic ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_DET_1MS_TH1;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_DET_1MS_TH1_LSPD;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_DET_1MS_TH2;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_DET_1MS_TH2_LSPD;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_DET_ALLOW_PEDAL;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_APPLY_RESET_THRES;
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_PEDAL_APPLY_THRES;         /* Comment [ Fast Pedal Apply Threshold ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         S16_FAST_PEDAL_APPLY_THRES_LSPD;    /* Comment [ Fast Pedal Apply Threshold at Low Speed ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */

    WPE_CAL_t WPE_CAL_FRT;								/* Front Gain */

    WPE_CAL_t WPE_CAL_RR;								/* Rear Gain */

} IDBDetection_CalibType;

#define S16_PSP_PEDAL_1P 	                        DETcalib.S16_PSP_PEDAL_1P
#define S16_PSP_PEDAL_2P 	                        DETcalib.S16_PSP_PEDAL_2P
#define S16_PSP_PEDAL_3P 	                        DETcalib.S16_PSP_PEDAL_3P
#define S16_PSP_PEDAL_4P 	                        DETcalib.S16_PSP_PEDAL_4P
#define S16_PSP_PEDAL_5P 	                        DETcalib.S16_PSP_PEDAL_5P
#define S16_PSP_PEDAL_6P 	                        DETcalib.S16_PSP_PEDAL_6P
#define S16_PSP_PEDAL_7P 	                        DETcalib.S16_PSP_PEDAL_7P
#define S16_PSP_PEDAL_8P 	                        DETcalib.S16_PSP_PEDAL_8P
#define S16_PSP_PEDAL_9P 	                        DETcalib.S16_PSP_PEDAL_9P

#define S16_PSP_PRESS_1P	                        DETcalib.S16_PSP_PRESS_1P
#define S16_PSP_PRESS_2P	                        DETcalib.S16_PSP_PRESS_2P
#define S16_PSP_PRESS_3P	                        DETcalib.S16_PSP_PRESS_3P
#define S16_PSP_PRESS_4P	                        DETcalib.S16_PSP_PRESS_4P
#define S16_PSP_PRESS_5P	                        DETcalib.S16_PSP_PRESS_5P
#define S16_PSP_PRESS_6P	                        DETcalib.S16_PSP_PRESS_6P
#define S16_PSP_PRESS_7P	                        DETcalib.S16_PSP_PRESS_7P
#define S16_PSP_PRESS_8P	                        DETcalib.S16_PSP_PRESS_8P
#define S16_PSP_PRESS_9P	                        DETcalib.S16_PSP_PRESS_9P

#define S16_AHBON_PD_OFFST_INVALID                  DETcalib.S16_AHBON_PD_OFFST_INVALID
#define S16_AHBON_PD_OFFST_INVALID_WBLS             DETcalib.S16_AHBON_PD_OFFST_INVALID_WBLS
#define S16_FAST_APPLY_DET_1MS_TH1                  DETcalib.S16_FAST_APPLY_DET_1MS_TH1
#define S16_FAST_APPLY_DET_1MS_TH1_LSPD             DETcalib.S16_FAST_APPLY_DET_1MS_TH1_LSPD
#define S16_FAST_APPLY_DET_1MS_TH2                  DETcalib.S16_FAST_APPLY_DET_1MS_TH2
#define S16_FAST_APPLY_DET_1MS_TH2_LSPD             DETcalib.S16_FAST_APPLY_DET_1MS_TH2_LSPD
#define S16_FAST_APPLY_DET_ALLOW_PEDAL              DETcalib.S16_FAST_APPLY_DET_ALLOW_PEDAL
#define S16_FAST_APPLY_RESET_THRES                  DETcalib.S16_FAST_APPLY_RESET_THRES
#define S16_FAST_PEDAL_APPLY_THRES                  DETcalib.S16_FAST_PEDAL_APPLY_THRES
#define S16_FAST_PEDAL_APPLY_THRES_LSPD             DETcalib.S16_FAST_PEDAL_APPLY_THRES_LSPD

/*Pressure based*/
#define S16_Inlet_BBS_Rise_Gain          	pWPE_CAL->Inlet_BBS_Rise_Gain
#define S16_Inlet_BBS_Release_Gain     		pWPE_CAL->Inlet_BBS_Release_Gain
#define S16_Inlet_ABS_Rise_Gain          	pWPE_CAL->Inlet_ABS_Rise_Gain
#define S16_Inlet_ABS_Release_Gain        	pWPE_CAL->Inlet_ABS_Release_Gain
#define S16_Outlet_Dump_Gain            	pWPE_CAL->Outlet_Dump_Gain
#define S16_Cut_Rise_Gain               	pWPE_CAL->Cut_Rise_Gain
#define S16_Cut_Release_Gain            	pWPE_CAL->Cut_Release_Gain
#define S16_Bal_Rise_Gain               	pWPE_CAL->Bal_Rise_Gain
#define S16_Bal_Release_Gain            	pWPE_CAL->Bal_Release_Gain
#define S16_EstPLimitByCircP_Margin        	pWPE_CAL->EstPLimitByCircP_Margin

/*Volume based*/
#define S16_VOL_4WHEEL_RISE_GAIN        	pWPE_CAL->VOL_4WHEEL_RISE_GAIN
#define S16_VOL_4WHEEL_RELEASE_GAIN     	pWPE_CAL->VOL_4WHEEL_RELEASE_GAIN
#define S16_VOL_4WHEEL_RERISE_GAIN      	pWPE_CAL->VOL_4WHEEL_RERISE_GAIN
#define S16_VOL_4WHEEL_DUMP_GAIN        	pWPE_CAL->VOL_4WHEEL_DUMP_GAIN
#define S16_VOL_1WHEEL_RISE_GAIN        	pWPE_CAL->VOL_1WHEEL_RISE_GAIN
#define S16_VOL_1WHEEL_RELEASE_GAIN     	pWPE_CAL->VOL_1WHEEL_RELEASE_GAIN
#define S16_VOL_1WHEEL_RERISE_GAIN      	pWPE_CAL->VOL_1WHEEL_RERISE_GAIN
#define S16_VOL_1WHEEL_DUMP_GAIN        	pWPE_CAL->VOL_1WHEEL_DUMP_GAIN
#define S16_VOL_ABS_RISE_GAIN           	pWPE_CAL->VOL_ABS_RISE_GAIN
#define S16_VOL_ABS_RERISE_GAIN         	pWPE_CAL->VOL_ABS_RERISE_GAIN
#define S16_VOL_ABS_DUMP_GAIN           	pWPE_CAL->VOL_ABS_DUMP_GAIN
#define S16_VOL_INLET_AREA_RATIO           	pWPE_CAL->VOL_INLET_AREA_RATIO
#define S16_INLET_LIMIT_MARGIN          	pWPE_CAL->INLET_LIMIT_MARGIN
#define S16_VALVE_INVALID_RISE_TIME     	pWPE_CAL->VALVE_INVALID_RISE_TIME
#define S16_VALVE_VALID_RELEASE_TIME    	pWPE_CAL->VALVE_VALID_RELEASE_TIME

/*TODO*/
#define S16_ABS_APPLY_TIME_GAIN				pWPE_CAL->ABS_APPLY_TIME_GAIN
#define S16_ABS_RELEASE_TIME_GAIN   		pWPE_CAL->ABS_RELEASE_TIME_GAIN
#define S16_ABS_DUMP_TIME_GAIN				pWPE_CAL->ABS_DUMP_TIME_GAIN
#define S16_ABS_APPLY_TIME_GAIN_INIT_RISE 	pWPE_CAL->ABS_APPLY_TIME_GAIN_INIT_RISE

#define S16_ABS_APPLY_TIME_GAIN_CONTINUE	pWPE_CAL->ABS_APPLY_TIME_GAIN_CONTINUE
#define S16_ABS_RELEASE_TIME_GAIN_CONTINUE	pWPE_CAL->ABS_RELEASE_TIME_GAIN_CONTINUE
#define S16_ABS_DUMP_TIME_GAIN_CONTINUE		pWPE_CAL->ABS_DUMP_TIME_GAIN_CONTINUE

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern IDBDetection_CalibType DETcalib;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* DET_CAL_H_ */
/** @} */
