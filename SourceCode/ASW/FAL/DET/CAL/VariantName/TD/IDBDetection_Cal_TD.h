	/* int16_t     	S16_PSP_PEDAL_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 1 Point ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_2P                      */		     210,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 2 Point ] */
	                                                        		        	/* ScaleVal[      21 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_3P                      */		     270,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 3 Point ] */
	                                                        		        	/* ScaleVal[      27 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 4 Point ] */
	                                                        		        	/* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_5P                      */		     330,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 5 Point ] */
	                                                        		        	/* ScaleVal[      33 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_6P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 6 Point ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_7P                      */		     510,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 7 Point ] */
	                                                        		        	/* ScaleVal[      51 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_8P                      */		     640,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 8 Point ] */
	                                                        		        	/* ScaleVal[      64 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_9P                      */		     700,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 9 Point ] */
	                                                        		        	/* ScaleVal[      70 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PRESS_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      1 Point ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_2P                      */		     100,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      2 Point ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_3P                      */		     200,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      3 Point ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      4 Point ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_5P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      5 Point ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_6P                      */		     600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      6 Point ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_7P                      */		    1000,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      7 Point ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_8P                      */		    1600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      8 Point ] */
	                                                        		        	/* ScaleVal[     16 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_9P                      */		    2400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      9 Point ] */
	                                                        		        	/* ScaleVal[     24 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    /* int16_t      S16_AHBON_PD_OFFST_INVALID            */             100,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
                                                                                /* ScaleVal[    10 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_AHBON_PD_OFFST_INVALID_WBLS       */              30,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
                                                                                /* ScaleVal[     3 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH1            */              30,
                                                                                /* ScaleVal[       3 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH1_LSPD       */              40,
                                                                                /* ScaleVal[       4 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH2            */              35,
                                                                                /* ScaleVal[     3.5 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH2_LSPD       */              45,
                                                                                /* ScaleVal[     4.5 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_ALLOW_PEDAL        */             250,
                                                                                /* ScaleVal[      25 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_RESET_THRES            */              10,
                                                                                /* ScaleVal[       1 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_PEDAL_APPLY_THRES            */             300,   /* Comment [ Fast Pedal Apply Threshold ] */
                                                                                /* ScaleVal[      30 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_PEDAL_APPLY_THRES_LSPD       */             400,   /* Comment [ Fast Pedal Apply Threshold at Low Speed ] */
                                                                                /* ScaleVal[      40 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
