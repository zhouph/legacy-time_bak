	/* int16_t     	S16_PSP_PEDAL_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 1 Point ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_2P                      */		     210,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 2 Point ] */
	                                                        		        	/* ScaleVal[      21 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_3P                      */		     270,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 3 Point ] */
	                                                        		        	/* ScaleVal[      27 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 4 Point ] */
	                                                        		        	/* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_5P                      */		     330,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 5 Point ] */
	                                                        		        	/* ScaleVal[      33 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_6P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 6 Point ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_7P                      */		     510,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 7 Point ] */
	                                                        		        	/* ScaleVal[      51 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_8P                      */		     640,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 8 Point ] */
	                                                        		        	/* ScaleVal[      64 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_9P                      */		     700,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 9 Point ] */
	                                                        		        	/* ScaleVal[      70 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PRESS_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      1 Point ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_2P                      */		     100,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      2 Point ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_3P                      */		     200,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      3 Point ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      4 Point ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_5P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      5 Point ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_6P                      */		     600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      6 Point ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_7P                      */		    1000,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      7 Point ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_8P                      */		    1600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      8 Point ] */
	                                                        		        	/* ScaleVal[     16 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_9P                      */		    2400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      9 Point ] */
	                                                        		        	/* ScaleVal[     24 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
    /* int16_t      S16_AHBON_PD_OFFST_INVALID            */             100,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
                                                                                /* ScaleVal[    10 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_AHBON_PD_OFFST_INVALID_WBLS       */              30,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
                                                                                /* ScaleVal[     3 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH1            */              30,
                                                                                /* ScaleVal[       3 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH1_LSPD       */              40,
                                                                                /* ScaleVal[       4 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH2            */              35,
                                                                                /* ScaleVal[     3.5 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_1MS_TH2_LSPD       */              45,
                                                                                /* ScaleVal[     4.5 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_DET_ALLOW_PEDAL        */             250,
                                                                                /* ScaleVal[      25 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_APPLY_RESET_THRES            */              10,
                                                                                /* ScaleVal[       1 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_PEDAL_APPLY_THRES            */             300,   /* Comment [ Fast Pedal Apply Threshold ] */
                                                                                /* ScaleVal[      30 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
    /* int16_t      S16_FAST_PEDAL_APPLY_THRES_LSPD       */             400,   /* Comment [ Fast Pedal Apply Threshold at Low Speed ] */
                                                                                /* ScaleVal[      40 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */

    /* WPE Front Gain *****************************************************************************/
    {

    		/*		Inlet_BBS_Rise_Gain*/					60,
    		/*		Inlet_BBS_Release_Gain*/				85,
    		/*		Inlet_ABS_Rise_Gain*/					50,
    		/*		Inlet_ABS_Release_Gain*/				70,
    		/*		Outlet_Dump_Gain*/						63,
    		/*		Cut_Rise_Gain*/							50,
    		/*		Cut_Release_Gain*/						50,
    		/*		Bal_Rise_Gain*/							50,
    		/*		Bal_Release_Gain*/						50,
    		/*		EstPLimitByCircP_Margin*/				300,

    		/*		VOL_4WHEEL_RISE_GAIN*/					89,
    		/*		VOL_4WHEEL_RELEASE_GAIN*/				95,
    		/*		VOL_4WHEEL_RERISE_GAIN*/				100,
    		/*		VOL_4WHEEL_DUMP_GAIN*/					350,
    		/*		VOL_1WHEEL_RISE_GAIN*/					75,
    		/*		VOL_1WHEEL_RELEASE_GAIN*/				90,
    		/*		VOL_1WHEEL_RERISE_GAIN*/				100,
    		/*		VOL_1WHEEL_DUMP_GAIN*/					350,
    		/*		VOL_ABS_RISE_GAIN*/						200,
    		/*		VOL_ABS_RERISE_GAIN*/					80,
    		/*		VOL_ABS_DUMP_GAIN*/						350,
    		/*		VOL_INLET_AREA_RATIO*/					24,    /* Inlet valve area ratio(F/R) */
    		/*		INLET_LIMIT_MARGIN*/					20,    /* Rate limit, (X*0.01)bar/scan */
    		/*		VALVE_INVALID_RISE_TIME*/				2,     /* Max 5ms */
    		/*		VALVE_VALID_RELEASE_TIME*/				5,     /* Max 5ms */

    		/*		ABS_APPLY_TIME_GAIN[WPE_CALC_TIME]*/
    		{
					0,
					28,
					43,
					61,
					87,
    		},
    		/*		ABS_RELEASE_TIME_GAIN[WPE_CALC_TIME]*/
    		{
    				0,
					50,
					60,
					80,
					90,
    		},
    		/*		ABS_DUMP_TIME_GAIN[WPE_CALC_TIME]*/
    		{
    				0,
    				0,
    				53,//50,
    				68,//65,
    				81,//77,
    		},
    		/*		ABS_APPLY_TIME_GAIN_INIT_RISE[WPE_CALC_TIME]*/
    		{
    				100,//50,
    				100,//60,
    				100,//80,
    				100,//90,
    				100,//100,
    		},
    		/*		ABS_APPLY_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
					28,//20,
					43,//30,
					61,//43,
					87,//61,
					100,//70,
    		},
    		/*		ABS_RELEASE_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
					50,
					60,
					80,
					90,
    				100,
    		},
    		/*		ABS_DUMP_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
    				53,//50,
    				68,//65,
    				81,//77,
    				89,//85,
    				100,//95,
    		},

    },

    /* WPE Rear Gain **********************************************************************************************************************/
    {

    		/*		Inlet_BBS_Rise_Gain*/					42,
    		/*		Inlet_BBS_Release_Gain*/				42,
    		/*		Inlet_ABS_Rise_Gain*/					40,
    		/*		Inlet_ABS_Release_Gain*/				70,
    		/*		Outlet_Dump_Gain*/						45,
    		/*		Cut_Rise_Gain*/							25,
    		/*		Cut_Release_Gain*/						25,
    		/*		Bal_Rise_Gain*/							50,
    		/*		Bal_Release_Gain*/						50,
    		/*		EstPLimitByCircP_Margin*/				300,

    		/*		VOL_4WHEEL_RISE_GAIN*/					89,
    		/*		VOL_4WHEEL_RELEASE_GAIN*/				95,
    		/*		VOL_4WHEEL_RERISE_GAIN*/				100,
    		/*		VOL_4WHEEL_DUMP_GAIN*/					350,
    		/*		VOL_1WHEEL_RISE_GAIN*/					75,
    		/*		VOL_1WHEEL_RELEASE_GAIN*/				90,
    		/*		VOL_1WHEEL_RERISE_GAIN*/				100,
    		/*		VOL_1WHEEL_DUMP_GAIN*/					350,
    		/*		VOL_ABS_RISE_GAIN*/						200,
    		/*		VOL_ABS_RERISE_GAIN*/					80,
    		/*		VOL_ABS_DUMP_GAIN*/						350,
    		/*		VOL_INLET_AREA_RATIO*/					10,    /* Fixed */
    		/*		INLET_LIMIT_MARGIN*/					20,    /* Rate limit, (X*0.01)bar/scan */
    		/*		VALVE_INVALID_RISE_TIME*/				2,     /* Max 5ms */
    		/*		VALVE_VALID_RELEASE_TIME*/				5,     /* Max 5ms */

    		/*		ABS_APPLY_TIME_GAIN[WPE_CALC_TIME]*/
    		{
    				0,
    				22,
    				34,
    				44,
    				64,
    		},
    		/*		ABS_RELEASE_TIME_GAIN[WPE_CALC_TIME]*/
    		{
    				0,
    				50,
    				60,
    				80,
    				90,
    		},
    		/*		ABS_DUMP_TIME_GAIN[WPE_CALC_TIME]*/
    		{
    				0,
    				0,
    				50,//40,
    				62,//50,
    				79,//63,
    		},
    		/*		ABS_APPLY_TIME_GAIN_INIT_RISE[WPE_CALC_TIME]*/
    		{
    				20,
    				40,
    				60,
    				80,
    				100,
    		},
    		/*		ABS_APPLY_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
    				22,//12,
    				36,//20,
    				54,//30,
    				84,//46,
    				100,//55,
    		},
    		/*		ABS_RELEASE_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
    				50,
    				60,
    				80,
    				90,
    				100,
    		},
    		/*		ABS_DUMP_TIME_GAIN_CONTINUE[WPE_CALC_TIME]*/
    		{
    				50,//40,
    				62,//50,
    				79,//63,
    				91,//73,
    				100,//80,
    		},

    },




