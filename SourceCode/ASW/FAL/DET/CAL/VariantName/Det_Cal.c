/**
 * @defgroup Det_Cal Det_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Det_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Det_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_CALIB_UNSPECIFIED
#include "Det_MemMap.h"
/* Global Calibration Section */


#define DET_STOP_SEC_CALIB_UNSPECIFIED
#include "Det_MemMap.h"

#define DET_START_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define DET_STOP_SEC_CONST_UNSPECIFIED
#include "Det_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

IDBDetection_CalibType DETcalib =
{
#if (__CAR == KMC_TD)
    #include "TD/IDBDetection_Cal_TD.h"
#elif (__CAR == HMC_BH)
    #include "BH/IDBDetection_Cal_BH.h"
#elif (__CAR == HMC_YFE)
    #include "YFE/IDBDetection_Cal_YFE.h"
#elif (__CAR == KMC_TFE)
    #include "TFE/IDBDetection_Cal_TFE.h"
#elif (__CAR == HMC_HGE)
	#include "HGE/IDBDetection_Cal_HGE.h"
#endif
};

#define DET_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define DET_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define DET_STOP_SEC_VAR_NOINIT_32BIT
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define DET_STOP_SEC_VAR_UNSPECIFIED
#include "Det_MemMap.h"
#define DET_START_SEC_VAR_32BIT
#include "Det_MemMap.h"
/** Variable Section (32BIT)**/


#define DET_STOP_SEC_VAR_32BIT
#include "Det_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define DET_START_SEC_CODE
#include "Det_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define DET_STOP_SEC_CODE
#include "Det_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
