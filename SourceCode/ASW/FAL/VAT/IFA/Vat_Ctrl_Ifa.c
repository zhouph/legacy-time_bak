/**
 * @defgroup Vat_Ctrl_Ifa Vat_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Ctrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vat_Ctrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VAT_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VAT_CTRL_START_SEC_CODE
#include "Vat_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VAT_CTRL_STOP_SEC_CODE
#include "Vat_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
