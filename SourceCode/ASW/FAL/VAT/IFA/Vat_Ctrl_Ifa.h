/**
 * @defgroup Vat_Ctrl_Ifa Vat_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VAT_CTRL_IFA_H_
#define VAT_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo(data) do \
{ \
    *data = Vat_CtrlWhlOutVlvCtrlTarInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo(data) do \
{ \
    *data = Vat_CtrlWhlnVlvCtrlTarInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlStkRecvryActnIfInfo(data) do \
{ \
    *data = Vat_CtrlStkRecvryActnIfInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo(data) do \
{ \
    *data = Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo_FlOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo_FrOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo_RlOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo_RrOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_CvVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_RlVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_PdVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_BalVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_CvVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_RlVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_PdVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_BalVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_CvVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_RlVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_PdVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo_BalVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SimVlvSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SimVlvNoiseCtrlSt(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_SimVlvHoldTime(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo_CutVlvLongOpen(data) do \
{ \
    *data = Vat_CtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo_FlInVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo_FrInVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo_RlInVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo_RrInVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[i]; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlStkRecvryActnIfInfo_StkRecvryCtrlState(data) do \
{ \
    *data = Vat_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(data) do \
{ \
    *data = Vat_CtrlStkRecvryActnIfInfo.StkRecvryRequestedTime; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo_PedlSimrPGendOnFlg(data) do \
{ \
    *data = Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlEcuModeSts(data) do \
{ \
    *data = Vat_CtrlEcuModeSts; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlPCtrlAct(data) do \
{ \
    *data = Vat_CtrlPCtrlAct; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlFuncInhibitVlvActSts(data) do \
{ \
    *data = Vat_CtrlFuncInhibitVlvActSts; \
}while(0);

#define Vat_Ctrl_Read_Vat_CtrlPCtrlBoostMod(data) do \
{ \
    *data = Vat_CtrlPCtrlBoostMod; \
}while(0);


/* Set Output DE MAcro Function */
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbVlvActInfo(data) do \
{ \
    Vat_CtrlIdbVlvActInfo = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SimVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_CircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_RelsVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PressDumpVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCutVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCutVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCircVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCircVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SimVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_RelsVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_CircVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PressDumpVlvReq(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCutVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCutVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PrimCircVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SecdCircVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_SimVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.SimVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_CircVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.CircVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_RelsVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.RelsVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo_PressDumpVlvDataLen(data) do \
{ \
    Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlIvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FlIvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrIvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FrIvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlIvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RlIvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrIvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RrIvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlOvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FlOvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrOvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FrOvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlOvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RlOvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrOvReq(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RrOvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlIvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FlIvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrIvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FrIvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlIvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RlIvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrIvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RrIvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlOvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FlOvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrOvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.FrOvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlOvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RlOvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrOvDataLen(data) do \
{ \
    Vat_CtrlWhlVlvReqIdbInfo.RrOvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_FrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo_RrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_PrimBalVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_SecdBalVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_ChmbBalVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[i] = *data[i]; \
}
#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_PrimBalVlvReq(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_SecdBalVlvReq(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_ChmbBalVlvReq(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_PrimBalVlvDataLen(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_SecdBalVlvDataLen(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo_ChmbBalVlvDataLen(data) do \
{ \
    Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbVlvActInfo_VlvFadeoutEndOK(data) do \
{ \
    Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbVlvActInfo_FadeoutSt2EndOk(data) do \
{ \
    Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk = *data; \
}while(0);

#define Vat_Ctrl_Write_Vat_CtrlIdbVlvActInfo_CutVlvOpenFlg(data) do \
{ \
    Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VAT_CTRL_IFA_H_ */
/** @} */
