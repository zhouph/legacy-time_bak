#define S_FUNCTION_NAME      Vat_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          69
#define WidthOutputPort         136

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Vat_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[0] = input[0];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[1] = input[1];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[2] = input[2];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[3] = input[3];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[4] = input[4];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[0] = input[5];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[1] = input[6];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[2] = input[7];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[3] = input[8];
    Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[4] = input[9];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[0] = input[10];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[1] = input[11];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[2] = input[12];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[3] = input[13];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[4] = input[14];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[0] = input[15];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[1] = input[16];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[2] = input[17];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[3] = input[18];
    Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[4] = input[19];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt = input[20];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt = input[21];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt = input[22];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = input[23];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt = input[24];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt = input[25];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt = input[26];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = input[27];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime = input[28];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime = input[29];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime = input[30];
    Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime = input[31];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt = input[32];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt = input[33];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt = input[34];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = input[35];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = input[36];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt = input[37];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime = input[38];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime = input[39];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime = input[40];
    Vat_CtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen = input[41];
    Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[0] = input[42];
    Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[1] = input[43];
    Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[2] = input[44];
    Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[3] = input[45];
    Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[4] = input[46];
    Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[0] = input[47];
    Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[1] = input[48];
    Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[2] = input[49];
    Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[3] = input[50];
    Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[4] = input[51];
    Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[0] = input[52];
    Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[1] = input[53];
    Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[2] = input[54];
    Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[3] = input[55];
    Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[4] = input[56];
    Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[0] = input[57];
    Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[1] = input[58];
    Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[2] = input[59];
    Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[3] = input[60];
    Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[4] = input[61];
    Vat_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = input[62];
    Vat_CtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = input[63];
    Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = input[64];
    Vat_CtrlEcuModeSts = input[65];
    Vat_CtrlPCtrlAct = input[66];
    Vat_CtrlFuncInhibitVlvActSts = input[67];
    Vat_CtrlPCtrlBoostMod = input[68];

    Vat_Ctrl();


    output[0] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0];
    output[1] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[1];
    output[2] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[2];
    output[3] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[3];
    output[4] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[4];
    output[5] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[0];
    output[6] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[1];
    output[7] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[2];
    output[8] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[3];
    output[9] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[4];
    output[10] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[0];
    output[11] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[1];
    output[12] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[2];
    output[13] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[3];
    output[14] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[4];
    output[15] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[0];
    output[16] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[1];
    output[17] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[2];
    output[18] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[3];
    output[19] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[4];
    output[20] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0];
    output[21] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[1];
    output[22] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[2];
    output[23] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[3];
    output[24] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[4];
    output[25] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0];
    output[26] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[1];
    output[27] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[2];
    output[28] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[3];
    output[29] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[4];
    output[30] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0];
    output[31] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[1];
    output[32] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[2];
    output[33] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[3];
    output[34] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[4];
    output[35] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0];
    output[36] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[1];
    output[37] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[2];
    output[38] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[3];
    output[39] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[4];
    output[40] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
    output[41] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
    output[42] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq;
    output[43] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq;
    output[44] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq;
    output[45] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq;
    output[46] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq;
    output[47] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq;
    output[48] = Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen;
    output[49] = Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen;
    output[50] = Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen;
    output[51] = Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen;
    output[52] = Vat_CtrlNormVlvReqVlvActInfo.SimVlvDataLen;
    output[53] = Vat_CtrlNormVlvReqVlvActInfo.CircVlvDataLen;
    output[54] = Vat_CtrlNormVlvReqVlvActInfo.RelsVlvDataLen;
    output[55] = Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen;
    output[56] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReq;
    output[57] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReq;
    output[58] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReq;
    output[59] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReq;
    output[60] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReq;
    output[61] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReq;
    output[62] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReq;
    output[63] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReq;
    output[64] = Vat_CtrlWhlVlvReqIdbInfo.FlIvDataLen;
    output[65] = Vat_CtrlWhlVlvReqIdbInfo.FrIvDataLen;
    output[66] = Vat_CtrlWhlVlvReqIdbInfo.RlIvDataLen;
    output[67] = Vat_CtrlWhlVlvReqIdbInfo.RrIvDataLen;
    output[68] = Vat_CtrlWhlVlvReqIdbInfo.FlOvDataLen;
    output[69] = Vat_CtrlWhlVlvReqIdbInfo.FrOvDataLen;
    output[70] = Vat_CtrlWhlVlvReqIdbInfo.RlOvDataLen;
    output[71] = Vat_CtrlWhlVlvReqIdbInfo.RrOvDataLen;
    output[72] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[0];
    output[73] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[1];
    output[74] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[2];
    output[75] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[3];
    output[76] = Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[4];
    output[77] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[0];
    output[78] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[1];
    output[79] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[2];
    output[80] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[3];
    output[81] = Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[4];
    output[82] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[0];
    output[83] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[1];
    output[84] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[2];
    output[85] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[3];
    output[86] = Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[4];
    output[87] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[0];
    output[88] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[1];
    output[89] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[2];
    output[90] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[3];
    output[91] = Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[4];
    output[92] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[0];
    output[93] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[1];
    output[94] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[2];
    output[95] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[3];
    output[96] = Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[4];
    output[97] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[0];
    output[98] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[1];
    output[99] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[2];
    output[100] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[3];
    output[101] = Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[4];
    output[102] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[0];
    output[103] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[1];
    output[104] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[2];
    output[105] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[3];
    output[106] = Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[4];
    output[107] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[0];
    output[108] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[1];
    output[109] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[2];
    output[110] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[3];
    output[111] = Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[4];
    output[112] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[0];
    output[113] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[1];
    output[114] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[2];
    output[115] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[3];
    output[116] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[4];
    output[117] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[0];
    output[118] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[1];
    output[119] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[2];
    output[120] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[3];
    output[121] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[4];
    output[122] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0];
    output[123] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[1];
    output[124] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[2];
    output[125] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[3];
    output[126] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[4];
    output[127] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq;
    output[128] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq;
    output[129] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq;
    output[130] = Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvDataLen;
    output[131] = Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvDataLen;
    output[132] = Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen;
    output[133] = Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK;
    output[134] = Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk;
    output[135] = Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Vat_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
