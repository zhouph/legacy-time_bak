Vat_CtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Vat_CtrlWhlOutVlvCtrlTarInfo = CreateBus(Vat_CtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Vat_CtrlIdbPCtrllVlvCtrlStInfo = Simulink.Bus;
DeList={
    'CvVlvSt'
    'RlVlvSt'
    'PdVlvSt'
    'BalVlvSt'
    'CvVlvNoiseCtrlSt'
    'RlVlvNoiseCtrlSt'
    'PdVlvNoiseCtrlSt'
    'BalVlvNoiseCtrlSt'
    'CvVlvHoldTime'
    'RlVlvHoldTime'
    'PdVlvHoldTime'
    'BalVlvHoldTime'
    };
Vat_CtrlIdbPCtrllVlvCtrlStInfo = CreateBus(Vat_CtrlIdbPCtrllVlvCtrlStInfo, DeList);
clear DeList;

Vat_CtrlIdbPedlFeelVlvCtrlStInfo = Simulink.Bus;
DeList={
    'PrimCutVlvSt'
    'SecdCutVlvSt'
    'SimVlvSt'
    'PrimCutVlvNoiseCtrlSt'
    'SecdCutVlvNoiseCtrlSt'
    'SimVlvNoiseCtrlSt'
    'PrimCutVlvHoldTime'
    'SecdCutVlvHoldTime'
    'SimVlvHoldTime'
    'CutVlvLongOpen'
    };
Vat_CtrlIdbPedlFeelVlvCtrlStInfo = CreateBus(Vat_CtrlIdbPedlFeelVlvCtrlStInfo, DeList);
clear DeList;

Vat_CtrlWhlnVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlInVlvCtrlTar_array_0'
    'FlInVlvCtrlTar_array_1'
    'FlInVlvCtrlTar_array_2'
    'FlInVlvCtrlTar_array_3'
    'FlInVlvCtrlTar_array_4'
    'FrInVlvCtrlTar_array_0'
    'FrInVlvCtrlTar_array_1'
    'FrInVlvCtrlTar_array_2'
    'FrInVlvCtrlTar_array_3'
    'FrInVlvCtrlTar_array_4'
    'RlInVlvCtrlTar_array_0'
    'RlInVlvCtrlTar_array_1'
    'RlInVlvCtrlTar_array_2'
    'RlInVlvCtrlTar_array_3'
    'RlInVlvCtrlTar_array_4'
    'RrInVlvCtrlTar_array_0'
    'RrInVlvCtrlTar_array_1'
    'RrInVlvCtrlTar_array_2'
    'RrInVlvCtrlTar_array_3'
    'RrInVlvCtrlTar_array_4'
    };
Vat_CtrlWhlnVlvCtrlTarInfo = CreateBus(Vat_CtrlWhlnVlvCtrlTarInfo, DeList);
clear DeList;

Vat_CtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Vat_CtrlStkRecvryActnIfInfo = CreateBus(Vat_CtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Vat_CtrlEcuModeSts = Simulink.Bus;
DeList={'Vat_CtrlEcuModeSts'};
Vat_CtrlEcuModeSts = CreateBus(Vat_CtrlEcuModeSts, DeList);
clear DeList;

Vat_CtrlPCtrlAct = Simulink.Bus;
DeList={'Vat_CtrlPCtrlAct'};
Vat_CtrlPCtrlAct = CreateBus(Vat_CtrlPCtrlAct, DeList);
clear DeList;

Vat_CtrlFuncInhibitVlvActSts = Simulink.Bus;
DeList={'Vat_CtrlFuncInhibitVlvActSts'};
Vat_CtrlFuncInhibitVlvActSts = CreateBus(Vat_CtrlFuncInhibitVlvActSts, DeList);
clear DeList;

Vat_CtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Vat_CtrlPCtrlBoostMod'};
Vat_CtrlPCtrlBoostMod = CreateBus(Vat_CtrlPCtrlBoostMod, DeList);
clear DeList;

Vat_CtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'PrimCircVlvReqData_array_0'
    'PrimCircVlvReqData_array_1'
    'PrimCircVlvReqData_array_2'
    'PrimCircVlvReqData_array_3'
    'PrimCircVlvReqData_array_4'
    'SecdCircVlvReqData_array_0'
    'SecdCircVlvReqData_array_1'
    'SecdCircVlvReqData_array_2'
    'SecdCircVlvReqData_array_3'
    'SecdCircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    'RelsVlvReq'
    'CircVlvReq'
    'PressDumpVlvReq'
    'PrimCutVlvDataLen'
    'SecdCutVlvDataLen'
    'PrimCircVlvDataLen'
    'SecdCircVlvDataLen'
    'SimVlvDataLen'
    'CircVlvDataLen'
    'RelsVlvDataLen'
    'PressDumpVlvDataLen'
    };
Vat_CtrlNormVlvReqVlvActInfo = CreateBus(Vat_CtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Vat_CtrlWhlVlvReqIdbInfo = Simulink.Bus;
DeList={
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    };
Vat_CtrlWhlVlvReqIdbInfo = CreateBus(Vat_CtrlWhlVlvReqIdbInfo, DeList);
clear DeList;

Vat_CtrlIdbBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'SecdBalVlvReqData_array_0'
    'SecdBalVlvReqData_array_1'
    'SecdBalVlvReqData_array_2'
    'SecdBalVlvReqData_array_3'
    'SecdBalVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    'PrimBalVlvReq'
    'SecdBalVlvReq'
    'ChmbBalVlvReq'
    'PrimBalVlvDataLen'
    'SecdBalVlvDataLen'
    'ChmbBalVlvDataLen'
    };
Vat_CtrlIdbBalVlvReqInfo = CreateBus(Vat_CtrlIdbBalVlvReqInfo, DeList);
clear DeList;

Vat_CtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'FadeoutSt2EndOk'
    'CutVlvOpenFlg'
    };
Vat_CtrlIdbVlvActInfo = CreateBus(Vat_CtrlIdbVlvActInfo, DeList);
clear DeList;

