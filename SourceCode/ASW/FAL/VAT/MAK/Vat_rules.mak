# \file
#
# \brief Vat
#
# This file contains the implementation of the SWC
# module Vat.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Vat_src

Vat_src_FILES        += $(Vat_SRC_PATH)\Vat_Ctrl.c
Vat_src_FILES        += $(Vat_SRC_PATH)\LAVAT_DecideValvePattern.c
Vat_src_FILES        += $(Vat_SRC_PATH)\LAVAT_DecideValveActuation.c
Vat_src_FILES        += $(Vat_IFA_PATH)\Vat_Ctrl_Ifa.c
Vat_src_FILES        += $(Vat_CFG_PATH)\Vat_Cfg.c
Vat_src_FILES        += $(Vat_CAL_PATH)\Vat_Cal.c

ifeq ($(ICE_COMPILE),true)
# Library
VlvAct_src_FILES  += $(Vat_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
VlvAct_src_FILES  += $(Vat_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
VlvAct_src_FILES  += $(Vat_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
VlvAct_src_FILES  += $(Vat_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
Vat_src_FILES        += $(Vat_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Vat_src_FILES        += $(Vat_UNITY_PATH)\unity.c
	Vat_src_FILES        += $(Vat_UNITY_PATH)\unity_fixture.c	
	Vat_src_FILES        += $(Vat_UT_PATH)\main.c
	Vat_src_FILES        += $(Vat_UT_PATH)\Vat_Ctrl\Vat_Ctrl_UtMain.c
endif