/**
 * @defgroup Vat_Cal Vat_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vat_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_START_SEC_CALIB_UNSPECIFIED
#include "Vat_MemMap.h"
/* Global Calibration Section */
#if (__CAR == HMC_YFE)
	#include "YFE/BALValveCal_YFE.h"
	#include "YFE/CIRValveCal_YFE.h"
	#include "YFE/CutValveCal_YFE.h"
	#include "YFE/PDValveCal_YFE.h"
	#include "YFE/RLValveCal_YFE.h"
	#include "YFE/SimValveCal_YFE.h"
#elif (__CAR == KMC_TFE)
	#include "TFE/BALValveCal_TFE.h"
	#include "TFE/CIRValveCal_TFE.h"
	#include "TFE/CutValveCal_TFE.h"
	#include "TFE/PDValveCal_TFE.h"
	#include "TFE/RLValveCal_TFE.h"
	#include "TFE/SimValveCal_TFE.h"
#elif (__CAR == HMC_HGE)
	#include "HGE/BALValveCal_HGE.h"
	#include "HGE/CIRValveCal_HGE.h"
	#include "HGE/CutValveCal_HGE.h"
	#include "HGE/PDValveCal_HGE.h"
	#include "HGE/RLValveCal_HGE.h"
	#include "HGE/SimValveCal_HGE.h"

#endif

IDBValveActuation_CalibType VALVEACTcalib =
{
	&apCalCutValveCal,
	&apCalSimValveCal,
	&apCalPDValveCal,
	&apCalBalValveCal,
	&apCalRlValveCal,
	&apCalCirValveCal
};

#define VAT_STOP_SEC_CALIB_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_START_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VAT_STOP_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VAT_START_SEC_CODE
#include "Vat_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VAT_STOP_SEC_CODE
#include "Vat_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
