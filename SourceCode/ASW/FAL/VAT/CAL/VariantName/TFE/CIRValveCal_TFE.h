DATA_CIR_VALVECAL_t               apCalCirValveCal =
{
        /* uint16_t     U16_CIRV_CURRENT_INITIAL                */            600,
        /* uint16_t     U16_CIRV_CURRENT_MAX_HIGH_P             */           2100,
        /* uint16_t     U16_CIRV_CURRENT_MAX_LOW_P              */           1200,
        /* uint16_t     U16_CIRV_CURRENT_ON_DIFF_MAX            */            300,
        /* uint16_t     U16_CIRV_CURRENT_ON_DIFF_MID            */            100,
        /* uint16_t     U16_CIRV_CURRENT_ON_DIFF_MIN            */             10,
        /* uint16_t     U16_CIRV_CURRENT_HOLD                   */            700,
        /* uint16_t     U16_CIRV_CURRENT_OFF_DIFF_MAX           */            300,
        /* uint16_t     U16_CIRV_CURRENT_OFF_DIFF_MID           */            100,
        /* uint16_t     U16_CIRV_CURRENT_OFF_DIFF_MIN           */             10,
        /* uint16_t     U16_CIRV_MAX_HOLD_TIME                  */            100,   /* 100: 500ms */
        /* uint16_t     U16_CIRV_DITHER_AMPLITUDE                */             0,
};
