DATA_CUT_VALVECAL_t               apCalCutValveCal =
{
        /* uint16_t     U16_CV_CURRENT_DIFF_MAX               */             100,   /* Comment [ Maximum value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_DIFF_MID               */              50,   /* Comment [ Middle value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_DIFF_MIN               */              10,   /* Comment [ Minimum value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_MAX                    */            1000,   /* [IDB - 1000] Ball-Screw Type :1200, Worm-Rack Type : 1000*/
        /* uint16_t     U16_CV_CURRENT_MID                    */             800,   /* Comment [ MiD value of Cut Valve Current (Initial value) ] */
        /* uint16_t     U16_CV_CURRENT_MIN                    */             500,   /* Comment [ Minimum value of Cut Valve Current (Initial value) ] */
        /* uint16_t    	U16_CV_CURRENT_OFF_DIFF_LSD           */		      10,	/* Comment [ Cut Valve 占쎈꽞占쎄턁筌앾옙 �뜝�럥六� Current �뤆�룆흮占쎄틬 �솻洹⑥삕�뜝�럩�꼨�뜝�럩�럸 at Low Speed ] */
        /* uint16_t    	U16_CV_CURRENT_OFF_DIFF_NORMAL        */		     100,	/* Comment [ Cut Valve 占쎈꽞占쎄턁筌앾옙 �뜝�럥六� Current �뤆�룆흮占쎄틬 �솻洹⑥삕�뜝�럩�꼨�뜝�럩�럸 at Normal Speed ] */
        /* uint16_t    	U16_CV_OFF_HOLD_CURRENT_DIFF          */		     100,	/* Comment [ Cut Valve �뜝�럩占썹춯�쉻�삕 �뜝�럥六� Current Hold DIFF �뤆�룊�삕(MIN �뤆�룊�삕 �뜝�룞�삕占쎈쑏�뜝占� 嶺뚢뼰維곻옙逾�) ] */
        /* int16_t      S16_CV_FM_FADE_OUT_CURRENT_MAX        */            1000,   /* Comment [ Cut Valve Current Max in Fail State ] */
        /* int16_t     	S16_CV_FM_FADE_OUT_RATE               */		      10,	/* Comment [ Cut Valve Current �솻洹⑥삕�뜝�럩�꼨�뜝�럩�럸 in Fail State ] */
        /* uint16_t    	U16_CV_ON_TIME_LSD                    */		    1000,	/* Comment [ �뜝�럩�젧嶺뚢댙�삕 �뜝�럩援℡뜝�럥裕� �뜝�룞�삕�뜝�럥爰쀥뜝�럥�뱺�뜝�럡�맋 Cut Valve �뜝�럩占썲뜝�럩援� �뜝�럥六삥뤆�룊�삕(Noise �뜝�룞�삕�뤆�룊�삕) ] */
        /* uint16_t    	U16_CV_ON_TIME_P_GEAR                 */		    2000,	/* idb : Org 6000*//* Comment [ P�뜝�럥堉� �뜝�럥�뱺�뜝�럡�맋 cut valve �뜝�럩占썲뜝�럩援쇿뜝�럥六삥뤆�룊�삕 ] */
        																			/* ScaleVal[     30 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
        /* int16_t     	S16_CVV_MNT_END_BCP_DIFF_REF          */		      30,	/* Comment [ Cut Valve �뜝�럩占썲뜝�럩援숋옙紐닷뜝占� 占쎈꽞占쎄턁筌앸툦�삕�뇡占썲뜝�럥裕� Circuit Pressure �뜝�럡留믣뜝�럥諭� �뜝�럥由띶뜝�럩�졑 ] */
        /* int16_t     	S16_CVV_MNT_END_BCP_REF               */		     100,	/* Comment [ Cut Valve �뜝�럩占썲뜝�럩援숋옙紐닷뜝占� 占쎈꽞占쎄턁筌앸툦�삕�뇡占썲뜝�럥裕� Circuit Pressure �뜝�럥由띶뜝�럩�졑 ] */
        /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_H         */            4000,   /* Comment [ Fast Apply 媛먯��떆 Cut V/V Close �쑀蹂� PSP High Threshold ] */
                                                                                    /* ScaleVal[     40 bar ]   Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1500 ] */
        /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_L         */             500,   /* Comment [ Fast Apply 媛먯��떆 Cut V/V Close �쑀蹂� PSP Low Threshold ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1500 ] */
        /* int16_t      S16_CVV_CLOSE_DIFF_QUICK              */              10
};
