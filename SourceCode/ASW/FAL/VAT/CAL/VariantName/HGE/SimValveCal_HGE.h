DATA_SIM_VALVECAL_t               apCalSimValveCal =
{
        /* uint16_t    	U16_SIMV_CURRENT_DIFF_MAX             */		     300,	/* Comment [ Maximum value of Sim Valve Current change ] */
        /* uint16_t     U16_SIMV_CURRENT_DIFF_MIN             */              10,   /* Comment [ Minimum value of Sim Valve Current change ] */
        /* uint16_t     U16_SIMV_CURRENT_MAX                  */            2500,   /* Comment [ Maximum value of Sim Valve Current ] */
        /* uint16_t     U16_SIMV_CURRENT_MID                  */             300,   /* Comment [ Mid value of Sim Valve Current ] */
        /* uint16_t     U16_SIMV_CURRENT_MIN                  */             200,   /* Comment [ Minimum value of Sim Valve Current (Initial value) ] */
        /* uint16_t    	U16_SIMV_OFF_HOLD_CURRENT_DIFF        */		       0,	/* Comment [ Sim Valve �뜝�럩占썹춯�쉻�삕 �뜝�럥六� Current Hold DIFF �뤆�룊�삕(MIN �뤆�룊�삕 �뜝�룞�삕占쎈쑏�뜝占� 嶺뚢뼰維곻옙逾�) ] */
        /* uint16_t    	U16_SIM_CURRENT_OFF_DIFF_LSD          */		      10,	/* Comment [ Sim Valve 占쎈꽞占쎄턁筌앾옙 �뜝�럥六� Current �뤆�룆흮占쎄틬 �솻洹⑥삕�뜝�럩�꼨�뜝�럩�럸 at Low Speed ] */
        /* uint16_t    	U16_SIM_CURRENT_OFF_DIFF_NORMAL       */		      10,	/* Comment [ Sim Valve 占쎈꽞占쎄턁筌앾옙 �뜝�럥六� Current �뤆�룆흮占쎄틬 �솻洹⑥삕�뜝�럩�꼨�뜝�럩�럸 at Normal Speed ] */
        /* uint16_t     U16_SIMV_CURRENT_MAX_AFTER_OPEN       */             500,
        /* uint16_t     U16_SIMV_MAX_HOLD_TIME                */             100,   /* 100: 500ms */
};
