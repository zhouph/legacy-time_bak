/**
 * @defgroup Vat_Cal Vat_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VAT_CAL_H_
#define VAT_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vat_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define S16_AP_CAL_CUT_VALVE_CAL                        (VALVEACTcalib.apCalCutValveCal)
#define S16_AP_CAL_SIM_VALVE_CAL                        (VALVEACTcalib.apCalSimValveCal)
#define S16_AP_CAL_PD_VALVE_CAL                         (VALVEACTcalib.apCalPDValveCal)
#define S16_AP_CAL_BALV_VALVE_CAL                       (VALVEACTcalib.apCalBalValveCal)
#define S16_AP_CAL_RLV_VALVE_CAL                        (VALVEACTcalib.apCalRlValveCal)
#define S16_AP_CAL_CIR_VALVE_CAL                        (VALVEACTcalib.apCalCirValveCal)

#define U16_CV_CURRENT_DIFF_MAX                         (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentDiffMax)
#define U16_CV_CURRENT_DIFF_MID                         (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentDiffMid)
#define U16_CV_CURRENT_DIFF_MIN                         (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentDiffMin)
#define U16_CV_CURRENT_MAX                              (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentMax)
#define U16_CV_CURRENT_MID                              (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentMid)
#define U16_CV_CURRENT_MIN                              (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentMin)
#define U16_CV_CURRENT_OFF_DIFF_LSD                     (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentOffDiffLsd)
#define U16_CV_CURRENT_OFF_DIFF_NORMAL                  (S16_AP_CAL_CUT_VALVE_CAL->u16CvCurrentOffDiffNormal)
#define U16_CV_OFF_HOLD_CURRENT_DIFF                    (S16_AP_CAL_CUT_VALVE_CAL->u16CvOffHoldCurrentDiff)
#define S16_CV_FM_FADE_OUT_CURRENT_MAX                  (S16_AP_CAL_CUT_VALVE_CAL->s16CvFmFadeOutCurrentMax)
#define S16_CV_FM_FADE_OUT_RATE                         (S16_AP_CAL_CUT_VALVE_CAL->s16CvFmFadeOutRate)
#define	U16_CV_ON_TIME_LSD                              (S16_AP_CAL_CUT_VALVE_CAL->s16CvOnTimeLsd)
#define U16_CV_ON_TIME_P_GEAR                           (S16_AP_CAL_CUT_VALVE_CAL->u16CvOnTimePGear)
#define	S16_CVV_MNT_END_BCP_DIFF_REF                    (S16_AP_CAL_CUT_VALVE_CAL->s16CvvMntEndBcpDiffRef)
#define	S16_CVV_MNT_END_BCP_REF                         (S16_AP_CAL_CUT_VALVE_CAL->s16CvvMntEndBcpRef)
#define S16_CVV_CLOSE_DELAY_PSP_THR_H                   (S16_AP_CAL_CUT_VALVE_CAL->s16CvvCloseDelayPspThrH)
#define S16_CVV_CLOSE_DELAY_PSP_THR_L                   (S16_AP_CAL_CUT_VALVE_CAL->s16CvvCloseDelayPspThrL)
#define S16_CVV_CLOSE_DIFF_QUICK                        (S16_AP_CAL_CUT_VALVE_CAL->s16CvvCloseDiffQuick)

#define U16_SIMV_CURRENT_DIFF_MAX                       (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentDiffMax)
#define U16_SIMV_CURRENT_DIFF_MIN                       (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentDiffMin)
#define U16_SIMV_CURRENT_MAX                            (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentMax)
#define U16_SIMV_CURRENT_MID                            (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentMid)
#define U16_SIMV_CURRENT_MIN                            (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentMin)
#define U16_SIMV_OFF_HOLD_CURRENT_DIFF                  (S16_AP_CAL_SIM_VALVE_CAL->u16SimvOffHoldCurrentDiff)
#define U16_SIM_CURRENT_OFF_DIFF_LSD                    (S16_AP_CAL_SIM_VALVE_CAL->u16SimCurrentOffDiffLsd)
#define U16_SIM_CURRENT_OFF_DIFF_NORMAL                 (S16_AP_CAL_SIM_VALVE_CAL->u16SimCurrentOffDiffNormal)
#define U16_SIMV_CURRENT_MAX_AFTER_OPEN                 (S16_AP_CAL_SIM_VALVE_CAL->u16SimvCurrentMaxAfterOpen)
#define U16_SIMV_MAX_HOLD_TIME                          (S16_AP_CAL_SIM_VALVE_CAL->u16SimVCurrentMaxHoldTime)

#define U16_PDV_CURRENT_INITIAL                         (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentInitial)
#define U16_PDV_CURRENT_MAX_HIGH_P                      (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentMaxHighP)
#define U16_PDV_CURRENT_MAX_LOW_P                       (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentMaxLowP)
#define U16_PDV_CURRENT_ON_DIFF_MAX                     (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOnDiffMax)
#define U16_PDV_CURRENT_ON_DIFF_MID                     (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOnDiffMid)
#define U16_PDV_CURRENT_ON_DIFF_MIN                     (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOnDiffMin)
#define U16_PDV_CURRENT_OFF_DIFF_MAX                    (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOffDiffMax)
#define U16_PDV_CURRENT_OFF_DIFF_MID                    (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOffDiffMid)
#define U16_PDV_CURRENT_OFF_DIFF_MIN                    (S16_AP_CAL_PD_VALVE_CAL->u16PDVCurrentOffDiffMin)
#define U16_PDV_DITHER_AMPLITUDE                 	    (S16_AP_CAL_PD_VALVE_CAL->u16PDVDitherAmplitude)

#define U16_BALV_CURRENT_INITIAL                        (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentInitial)
#define U16_BALV_CURRENT_MAX_HIGH_P                     (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentMaxHighP)
#define U16_BALV_CURRENT_MAX_LOW_P                      (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentMaxLowP)
#define U16_BALV_CURRENT_ON_DIFF_MAX                    (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOnDiffMax)
#define U16_BALV_CURRENT_ON_DIFF_MID                    (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOnDiffMid)
#define U16_BALV_CURRENT_ON_DIFF_MIN                    (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOnDiffMin)
#define U16_BALV_CURRENT_HOLD                           (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentHold)
#define U16_BALV_CURRENT_OFF_DIFF_MAX                   (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOffDiffMax)
#define U16_BALV_CURRENT_OFF_DIFF_MID                   (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOffDiffMid)
#define U16_BALV_CURRENT_OFF_DIFF_MIN                   (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentOffDiffMin)
#define U16_BALV_MAX_HOLD_TIME                          (S16_AP_CAL_BALV_VALVE_CAL->u16BalVCurrentMaxHoldTime)
#define U8_BALV_DITHER_ENABLE                           (S16_AP_CAL_BALV_VALVE_CAL->u8BalVDitherEnable)
#define U16_BALV_DITHER_AMPLITUDE                       (S16_AP_CAL_BALV_VALVE_CAL->u16BalVDitherAmplitude)

#define U16_RLV_CURRENT_INITIAL                         (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentInitial)
#define U16_RLV_CURRENT_MAX_HIGH_P                      (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentMaxHighP)
#define U16_RLV_CURRENT_MAX_LOW_P                       (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentMaxLowP)
#define U16_RLV_CURRENT_ON_DIFF_MAX                     (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOnDiffMax)
#define U16_RLV_CURRENT_ON_DIFF_MID                     (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOnDiffMid)
#define U16_RLV_CURRENT_ON_DIFF_MIN                     (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOnDiffMin)
#define U16_RLV_CURRENT_HOLD                            (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentHold)
#define U16_RLV_CURRENT_OFF_DIFF_MAX                    (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOffDiffMax)
#define U16_RLV_CURRENT_OFF_DIFF_MID                    (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOffDiffMid)
#define U16_RLV_CURRENT_OFF_DIFF_MIN                    (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentOffDiffMin)
#define U16_RLV_MAX_HOLD_TIME                           (S16_AP_CAL_RLV_VALVE_CAL->u16RlVCurrentMaxHoldTime)
#define U16_RLV_DITHER_AMPLITUDE                        (S16_AP_CAL_RLV_VALVE_CAL->u16RlVDitherAmplitude)

#define U16_CIRV_CURRENT_INITIAL                        (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentInitial)
#define U16_CIRV_CURRENT_MAX_HIGH_P                     (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentMaxHighP)
#define U16_CIRV_CURRENT_MAX_LOW_P                      (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentMaxLowP)
#define U16_CIRV_CURRENT_ON_DIFF_MAX                    (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOnDiffMax)
#define U16_CIRV_CURRENT_ON_DIFF_MID                    (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOnDiffMid)
#define U16_CIRV_CURRENT_ON_DIFF_MIN                    (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOnDiffMin)
#define U16_CIRV_CURRENT_HOLD                           (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentHold)
#define U16_CIRV_CURRENT_OFF_DIFF_MAX                   (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOffDiffMax)
#define U16_CIRV_CURRENT_OFF_DIFF_MID                   (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOffDiffMid)
#define U16_CIRV_CURRENT_OFF_DIFF_MIN                   (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentOffDiffMin)
#define U16_CIRV_MAX_HOLD_TIME                          (S16_AP_CAL_CIR_VALVE_CAL->u16CirVCurrentMaxHoldTime)
#define U16_CIRV_DITHER_AMPLITUDE                       (S16_AP_CAL_CIR_VALVE_CAL->u16CirVDitherAmplitude)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint16_t        u16CvCurrentDiffMax;            /* Comment [ Maximum value of Cut Valve Current change ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentDiffMid;            /* Comment [ Middle value of Cut Valve Current change ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentDiffMin;            /* Comment [ Minimum value of Cut Valve Current change ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentMax;                 /* Comment [ Maximum value of Cut Valve Current ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentMid;                 /* Comment [ Mid value of Cut Valve Current (Initial value) ] */
													/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
													/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentMin;                 /* Comment [ Minimum value of Cut Valve Current (Initial value) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentOffDiffLsd;        /* Comment [ Cut Valve 종료 시 Current 감소 변화량 at Low Speed ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvCurrentOffDiffNormal;     /* Comment [ Cut Valve 종료 시 Current 감소 변화량 at Normal Speed ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16CvOffHoldCurrentDiff;       /* Comment [ Cut Valve 유지 시 Current Hold DIFF 값(MIN 값 대비 차이) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 1000 ] */
	int16_t         s16CvFmFadeOutCurrentMax;     /* Comment [ Cut Valve Current Max in Fail State ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation/FM Fade Out ] */
														/* Scale   [ None ]  Range [ -32768 <= X <= 32767 ] */
	int16_t         s16CvFmFadeOutRate;            /* Comment [ Cut Valve Current 변화량 in Fail State ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation/FM Fade Out ] */
														/* Scale   [ None ]  Range [ -32768 <= X <= 32767 ] */
	uint16_t    	s16CvOnTimeLsd;                	/* Comment [ 정차 또는 저속에서 Cut Valve 유예 시간(Noise 저감) ] */
														/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
														/* Scale   [ f(x) = (X + 0) * 0.005 ]  Range [ 0 <= X <= 327.675 ] */
	uint16_t    	u16CvOnTimePGear;             	/* Comment [ P단 에서 cut valve 유예시간 ] */
														/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
														/* Scale   [ f(x) = (X + 0) * 0.005 ]  Range [ 0 <= X <= 327.675 ] */
	int16_t     	s16CvvMntEndBcpDiffRef;      	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 상승 압력  ] */
														/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
	int16_t     	s16CvvMntEndBcpRef;           	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 압력 ] */
															/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
	int16_t         s16CvvCloseDelayPspThrH;      	/* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP High Threshold ] */
															/* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 15 ] */
	int16_t         s16CvvCloseDelayPspThrL;      	/* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP Low Threshold ] */
															/* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 15 ] */
	int16_t         s16CvvCloseDiffQuick;
} DATA_CUT_VALVECAL_t;

typedef struct
{
	uint16_t        u16SimvCurrentDiffMax;          /* Comment [ Maximum value of Sim Valve Current change ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvCurrentDiffMin;          /* Comment [ Minimum value of Sim Valve Current change ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvCurrentMax;               /* Comment [ Maximum value of Sim Valve Current ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvCurrentMid;              /* Comment [ Mid value of Sim Valve Current (Initial value) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvCurrentMin;               /* Comment [ Minimum value of Sim Valve Current (Initial value) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvOffHoldCurrentDiff;     /* Comment [ Sim Valve 유지 시 Current Hold DIFF 값(MIN 값 대비 차이) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 1000 ] */
	uint16_t        u16SimCurrentOffDiffLsd;       /* Comment [ Sim Valve 종료 시 Current 감소 변화량 at Low Speed ] */
															/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
															/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimCurrentOffDiffNormal;    /* Comment [ Sim Valve 종료 시 Current 감소 변화량 at Normal Speed ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimvCurrentMaxAfterOpen;         /* Comment [ Maximum value of Sim Valve Current (Initial value) ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t        u16SimVCurrentMaxHoldTime;          /* Comment [ Sim valve Current Max hold time ] */
														/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
                                                    /* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
} DATA_SIM_VALVECAL_t;

typedef struct
{
	uint16_t        u16PDVCurrentInitial;
	uint16_t        u16PDVCurrentMaxHighP;
	uint16_t        u16PDVCurrentMaxLowP;
	uint16_t        u16PDVCurrentOnDiffMax;
	uint16_t        u16PDVCurrentOnDiffMid;
	uint16_t        u16PDVCurrentOnDiffMin;
	uint16_t        u16PDVCurrentOffDiffMax;
	uint16_t        u16PDVCurrentOffDiffMid;
	uint16_t        u16PDVCurrentOffDiffMin;
	uint16_t 		u16PDVDitherAmplitude;
} DATA_PD_VALVECAL_t;

typedef struct
{
	uint16_t        u16BalVCurrentInitial;
	uint16_t        u16BalVCurrentMaxHighP;
	uint16_t        u16BalVCurrentMaxLowP;
	uint16_t        u16BalVCurrentOnDiffMax;
	uint16_t        u16BalVCurrentOnDiffMid;
	uint16_t        u16BalVCurrentOnDiffMin;
	uint16_t        u16BalVCurrentHold;
	uint16_t        u16BalVCurrentOffDiffMax;
	uint16_t        u16BalVCurrentOffDiffMid;
	uint16_t        u16BalVCurrentOffDiffMin;
	uint16_t        u16BalVCurrentMaxHoldTime;
	uint8_t			u8BalVDitherEnable;
	uint16_t 		u16BalVDitherAmplitude;
} DATA_BAL_VALVECAL_t;

typedef struct
{
	uint16_t        u16RlVCurrentInitial;
	uint16_t        u16RlVCurrentMaxHighP;
	uint16_t        u16RlVCurrentMaxLowP;
	uint16_t        u16RlVCurrentOnDiffMax;
	uint16_t        u16RlVCurrentOnDiffMid;
	uint16_t        u16RlVCurrentOnDiffMin;
	uint16_t        u16RlVCurrentHold;
	uint16_t        u16RlVCurrentOffDiffMax;
	uint16_t        u16RlVCurrentOffDiffMid;
	uint16_t        u16RlVCurrentOffDiffMin;
	uint16_t        u16RlVCurrentMaxHoldTime;
	uint16_t        u16RlVDitherAmplitude;
} DATA_RL_VALVECAL_t;

typedef struct
{
	uint16_t        u16CirVCurrentInitial;
	uint16_t        u16CirVCurrentMaxHighP;
	uint16_t        u16CirVCurrentMaxLowP;
	uint16_t        u16CirVCurrentOnDiffMax;
	uint16_t        u16CirVCurrentOnDiffMid;
	uint16_t        u16CirVCurrentOnDiffMin;
	uint16_t        u16CirVCurrentHold;
	uint16_t        u16CirVCurrentOffDiffMax;
	uint16_t        u16CirVCurrentOffDiffMid;
	uint16_t        u16CirVCurrentOffDiffMin;
	uint16_t        u16CirVCurrentMaxHoldTime;
	uint16_t        u16CirVDitherAmplitude;
} DATA_CIR_VALVECAL_t;

typedef struct
{
	const DATA_CUT_VALVECAL_t			*apCalCutValveCal;
	const DATA_SIM_VALVECAL_t			*apCalSimValveCal;
	const DATA_PD_VALVECAL_t			*apCalPDValveCal;
	const DATA_BAL_VALVECAL_t			*apCalBalValveCal;
	const DATA_RL_VALVECAL_t			*apCalRlValveCal;
	const DATA_CIR_VALVECAL_t			*apCalCirValveCal;

} IDBValveActuation_CalibType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern IDBValveActuation_CalibType VALVEACTcalib;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VAT_CAL_H_ */
/** @} */
