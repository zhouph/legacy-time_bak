DATA_PD_VALVECAL_t               apCalPDValveCal =
{
        /* uint16_t     U16_PDV_CURRENT_INITIAL                */             200,
        /* uint16_t     U16_PDV_CURRENT_MAX_HIGH_P             */            1300,
        /* uint16_t     U16_PDV_CURRENT_MAX_LOW_P              */             800,
        /* uint16_t     U16_PDV_CURRENT_ON_DIFF_MAX            */             300,
        /* uint16_t     U16_PDV_CURRENT_ON_DIFF_MID            */             100,
        /* uint16_t     U16_PDV_CURRENT_ON_DIFF_MIN            */              10,
        /* uint16_t     U16_PDV_CURRENT_OFF_DIFF_MAX           */             300,
        /* uint16_t     U16_PDV_CURRENT_OFF_DIFF_MID           */             100,
        /* uint16_t     U16_PDV_CURRENT_OFF_DIFF_MIN           */              10,
        /* uint16_t 	U16_PDV_DITHER_AMPLITUDE			   */			    0,
};
