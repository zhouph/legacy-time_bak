DATA_BAL_VALVECAL_t               apCalBalValveCal =
{
        /* uint16_t     U16_BALV_CURRENT_INITIAL                */            400,
        /* uint16_t     U16_BALV_CURRENT_MAX_HIGH_P             */           1800,
        /* uint16_t     U16_BALV_CURRENT_MAX_LOW_P              */           1000,
        /* uint16_t     U16_BALV_CURRENT_ON_DIFF_MAX            */            300,
        /* uint16_t     U16_BALV_CURRENT_ON_DIFF_MID            */            100,
        /* uint16_t     U16_BALV_CURRENT_ON_DIFF_MIN            */             10,
        /* uint16_t     U16_BALV_CURRENT_HOLD                   */            600,
        /* uint16_t     U16_BALV_CURRENT_OFF_DIFF_MAX           */            300,
        /* uint16_t     U16_BALV_CURRENT_OFF_DIFF_Mid           */            100,
        /* uint16_t     U16_BALV_CURRENT_OFF_DIFF_MIN           */             10,
        /* uint16_t     U16_BALV_MAX_HOLD_TIME                  */            100,   /* 100: 500ms */
        /* uint8_t      U8_BALV_DITHER_ENABLE                   */              0,
        /* uint16_t     U16_BALV_DITHER_AMPLITUDE               */              0,   /* mA */
};
