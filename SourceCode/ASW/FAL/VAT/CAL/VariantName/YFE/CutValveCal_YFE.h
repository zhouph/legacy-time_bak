DATA_CUT_VALVECAL_t               apCalCutValveCal =
{
        /* uint16_t     U16_CV_CURRENT_DIFF_MAX               */             100,   /* Comment [ Maximum value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_DIFF_MID               */              50,   /* Comment [ Middle value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_DIFF_MIN               */              10,   /* Comment [ Minimum value of Cut Valve Current change ] */
        /* uint16_t     U16_CV_CURRENT_MAX                    */            1000,   /* [IDB - 1000] Ball-Screw Type :1200, Worm-Rack Type : 1000*/
        /* uint16_t     U16_CV_CURRENT_MID                    */             800,   /* Comment [ MiD value of Cut Valve Current (Initial value) ] */
        /* uint16_t     U16_CV_CURRENT_MIN                    */             500,   /* Comment [ Minimum value of Cut Valve Current (Initial value) ] */
        /* uint16_t    	U16_CV_CURRENT_OFF_DIFF_LSD           */		      10,	/* Comment [ Cut Valve �뜝�럥苑욃뜝�럡�꼤嶺뚯빢�삕 占쎈쐻占쎈윥筌묕옙 Current 占쎈쨬占쎈즴�씙�뜝�럡�떖 占쎌녃域밟뫁�굲占쎈쐻占쎈윪占쎄섀占쎈쐻占쎈윪占쎈읁 at Low Speed ] */
        /* uint16_t    	U16_CV_CURRENT_OFF_DIFF_NORMAL        */		     100,	/* Comment [ Cut Valve �뜝�럥苑욃뜝�럡�꼤嶺뚯빢�삕 占쎈쐻占쎈윥筌묕옙 Current 占쎈쨬占쎈즴�씙�뜝�럡�떖 占쎌녃域밟뫁�굲占쎈쐻占쎈윪占쎄섀占쎈쐻占쎈윪占쎈읁 at Normal Speed ] */
        /* uint16_t    	U16_CV_OFF_HOLD_CURRENT_DIFF          */		     100,	/* Comment [ Cut Valve 占쎈쐻占쎈윪�뜝�뜾異�占쎌돸占쎌굲 占쎈쐻占쎈윥筌묕옙 Current Hold DIFF 占쎈쨬占쎈즸占쎌굲(MIN 占쎈쨬占쎈즸占쎌굲 占쎈쐻占쎈짗占쎌굲�뜝�럥�몡占쎈쐻�뜝占� 癲ル슓堉곁땟怨살삕�억옙) ] */
        /* int16_t      S16_CV_FM_FADE_OUT_CURRENT_MAX        */            1000,   /* Comment [ Cut Valve Current Max in Fail State ] */
        /* int16_t     	S16_CV_FM_FADE_OUT_RATE               */		      10,	/* Comment [ Cut Valve Current 占쎌녃域밟뫁�굲占쎈쐻占쎈윪占쎄섀占쎈쐻占쎈윪占쎈읁 in Fail State ] */
        /* uint16_t    	U16_CV_ON_TIME_LSD                    */		    1000,	/* Comment [ 占쎈쐻占쎈윪占쎌젳癲ル슓�뙔占쎌굲 占쎈쐻占쎈윪�뤃�꽒�쐻占쎈윥獒뺧옙 占쎈쐻占쎈짗占쎌굲占쎈쐻占쎈윥�댆�λ쐻占쎈윥占쎈군占쎈쐻占쎈윞占쎈쭓 Cut Valve 占쎈쐻占쎈윪�뜝�뜴�쐻占쎈윪�뤃占� 占쎈쐻占쎈윥筌묒궏琉놅옙猷딉옙�굲(Noise 占쎈쐻占쎈짗占쎌굲占쎈쨬占쎈즸占쎌굲) ] */
        /* uint16_t    	U16_CV_ON_TIME_P_GEAR                 */		    2000,	/* idb : Org 6000*//* Comment [ P占쎈쐻占쎈윥�젆占� 占쎈쐻占쎈윥占쎈군占쎈쐻占쎈윞占쎈쭓 cut valve 占쎈쐻占쎈윪�뜝�뜴�쐻占쎈윪�뤃�눨�쐻占쎈윥筌묒궏琉놅옙猷딉옙�굲 ] */
        																			/* ScaleVal[     30 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
        /* int16_t     	S16_CVV_MNT_END_BCP_DIFF_REF          */		      30,	/* Comment [ Cut Valve 占쎈쐻占쎈윪�뜝�뜴�쐻占쎈윪�뤃�닂�삕筌뤿떣�쐻�뜝占� �뜝�럥苑욃뜝�럡�꼤嶺뚯빖�닰占쎌굲占쎈눀�뜝�뜴�쐻占쎈윥獒뺧옙 Circuit Pressure 占쎈쐻占쎈윞筌띾�ｋ쐻占쎈윥獄�占� 占쎈쐻占쎈윥�뵳�씢�쐻占쎈윪占쎌죷 ] */
        /* int16_t     	S16_CVV_MNT_END_BCP_REF               */		     100,	/* Comment [ Cut Valve 占쎈쐻占쎈윪�뜝�뜴�쐻占쎈윪�뤃�닂�삕筌뤿떣�쐻�뜝占� �뜝�럥苑욃뜝�럡�꼤嶺뚯빖�닰占쎌굲占쎈눀�뜝�뜴�쐻占쎈윥獒뺧옙 Circuit Pressure 占쎈쐻占쎈윥�뵳�씢�쐻占쎈윪占쎌죷 ] */
        /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_H         */            4000,   /* Comment [ Fast Apply 揶쏅Ŋ占쏙옙�뻻 Cut V/V Close 占쎌�癰귨옙 PSP High Threshold ] */
                                                                                    /* ScaleVal[     40 bar ]   Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1500 ] */
        /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_L         */             500,   /* Comment [ Fast Apply 揶쏅Ŋ占쏙옙�뻻 Cut V/V Close 占쎌�癰귨옙 PSP Low Threshold ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1500 ] */
        /* int16_t      S16_CVV_CLOSE_DIFF_QUICK              */              10
};
