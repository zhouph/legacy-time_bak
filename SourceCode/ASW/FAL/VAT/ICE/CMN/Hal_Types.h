/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef HAL_TYPES_H_
#define HAL_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Halsint8;
typedef uint8 Haluint8;
typedef sint16 Halsint16;
typedef uint16 Haluint16;
typedef sint32 Halsint32;
typedef uint32 Haluint32;
typedef float32 Halfloat32;
typedef float64 Halfloat64;

typedef struct
{
    Haluint8 Ach_Asic_vdd1_ovc;
    Haluint8 Ach_Asic_vdd2_out_of_reg;
    Haluint8 Ach_Asic_vdd3_ovc;
    Haluint8 Ach_Asic_vdd4_ovc;
    Haluint8 Ach_Asic_vdd1_diode_loss;
    Haluint8 Ach_Asic_vdd2_rev_curr;
    Haluint8 Ach_Asic_vdd1_t_sd;
    Haluint8 Ach_Asic_vdd2_t_sd;
    Haluint8 Ach_Asic_vdd1_uv;
    Haluint8 Ach_Asic_vdd2_uv;
    Haluint8 Ach_Asic_vdd3_uv;
    Haluint8 Ach_Asic_vdd4_uv;
    Haluint8 Ach_Asic_vdd1_ov;
    Haluint8 Ach_Asic_vdd2_ov;
    Haluint8 Ach_Asic_vdd3_ov;
    Haluint8 Ach_Asic_vdd4_ov;
    Haluint8 Ach_Asic_vint_ov;
    Haluint8 Ach_Asic_vint_uv;
    Haluint8 Ach_Asic_dgndloss;
    Haluint8 Ach_Asic_vpwr_uv;
    Haluint8 Ach_Asic_vpwr_ov;
    Haluint8 Ach_Asic_comm_too_long;
    Haluint8 Ach_Asic_comm_too_short;
    Haluint8 Ach_Asic_comm_wrong_add;
    Haluint8 Ach_Asic_comm_wrong_fcnt;
    Haluint8 Ach_Asic_comm_wrong_crc;
    Haluint8 Ach_Asic_vdd5_ov;
    Haluint8 Ach_Asic_vdd5_uv;
    Haluint8 Ach_Asic_vdd5_t_sd;
    Haluint8 Ach_Asic_vdd5_out_of_reg;
    Haluint8 Ach_Asic_3_OSC_STUCK_MON;
    Haluint8 Ach_Asic_3_OSC_FRQ_MON;
    Haluint8 Ach_Asic_3_WDOG_TO;
    Haluint8 Ach_Asic_7_AN_TRIM_CRC_RESULT;
    Haluint8 Ach_Asic_7_NVM_CRC_RESULT;
    Haluint8 Ach_Asic_7_NVM_BUSY;
}AchSysPwrAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CP_OV;
    Haluint8 Ach_Asic_CP_UV;
    Haluint8 Ach_Asic_VPWR_UV;
    Haluint8 Ach_Asic_VPWR_OV;
    Haluint8 Ach_Asic_PMP_LD_ACT;
    Haluint8 Ach_Asic_FS_TURN_ON;
    Haluint8 Ach_Asic_FS_TURN_OFF;
    Haluint8 Ach_Asic_FS_VDS_FAULT;
    Haluint8 Ach_Asic_FS_RVP_FAULT;
    Haluint8 Ach_Asic_VHD_OV;
    Haluint8 Ach_Asic_T_SD_INT;
    Haluint16 Ach_Asic_No0_AVG_CURRENT;
    Haluint8 Ach_Asic_No0_PWM_FAULT;
    Haluint8 Ach_Asic_No0_CURR_SENSE;
    Haluint8 Ach_Asic_No0_ADC_FAULT;
    Haluint8 Ach_Asic_No0_T_SD;
    Haluint8 Ach_Asic_No0_OPEN_LOAD;
    Haluint8 Ach_Asic_No0_GND_LOSS;
    Haluint8 Ach_Asic_No0_LVT;
    Haluint8 Ach_Asic_No0_LS_OVC;
    Haluint8 Ach_Asic_No0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No0_HS_SHORT;
    Haluint8 Ach_Asic_No0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No0_UNDER_CURR;
    Haluint16 Ach_Asic_No1_AVG_CURRENT;
    Haluint8 Ach_Asic_No1_PWM_FAULT;
    Haluint8 Ach_Asic_No1_CURR_SENSE;
    Haluint8 Ach_Asic_No1_ADC_FAULT;
    Haluint8 Ach_Asic_No1_T_SD;
    Haluint8 Ach_Asic_No1_OPEN_LOAD;
    Haluint8 Ach_Asic_No1_GND_LOSS;
    Haluint8 Ach_Asic_No1_LVT;
    Haluint8 Ach_Asic_No1_LS_OVC;
    Haluint8 Ach_Asic_No1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No1_HS_SHORT;
    Haluint8 Ach_Asic_No1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No1_UNDER_CURR;
    Haluint16 Ach_Asic_No2_AVG_CURRENT;
    Haluint8 Ach_Asic_No2_PWM_FAULT;
    Haluint8 Ach_Asic_No2_CURR_SENSE;
    Haluint8 Ach_Asic_No2_ADC_FAULT;
    Haluint8 Ach_Asic_No2_T_SD;
    Haluint8 Ach_Asic_No2_OPEN_LOAD;
    Haluint8 Ach_Asic_No2_GND_LOSS;
    Haluint8 Ach_Asic_No2_LVT;
    Haluint8 Ach_Asic_No2_LS_OVC;
    Haluint8 Ach_Asic_No2_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No2_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No2_HS_SHORT;
    Haluint8 Ach_Asic_No2_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No2_UNDER_CURR;
    Haluint16 Ach_Asic_No3_AVG_CURRENT;
    Haluint8 Ach_Asic_No3_PWM_FAULT;
    Haluint8 Ach_Asic_No3_CURR_SENSE;
    Haluint8 Ach_Asic_No3_ADC_FAULT;
    Haluint8 Ach_Asic_No3_T_SD;
    Haluint8 Ach_Asic_No3_OPEN_LOAD;
    Haluint8 Ach_Asic_No3_GND_LOSS;
    Haluint8 Ach_Asic_No3_LVT;
    Haluint8 Ach_Asic_No3_LS_OVC;
    Haluint8 Ach_Asic_No3_VGS_LS_FAULT;
    Haluint8 Ach_Asic_No3_VGS_HS_FAULT;
    Haluint8 Ach_Asic_No3_HS_SHORT;
    Haluint8 Ach_Asic_No3_LS_CLAMP_ON;
    Haluint8 Ach_Asic_No3_UNDER_CURR;
    Haluint16 Ach_Asic_Nc0_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc0_PWM_FAULT;
    Haluint8 Ach_Asic_Nc0_CURR_SENSE;
    Haluint8 Ach_Asic_Nc0_ADC_FAULT;
    Haluint8 Ach_Asic_Nc0_T_SD;
    Haluint8 Ach_Asic_Nc0_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc0_GND_LOSS;
    Haluint8 Ach_Asic_Nc0_LVT;
    Haluint8 Ach_Asic_Nc0_LS_OVC;
    Haluint8 Ach_Asic_Nc0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc0_HS_SHORT;
    Haluint8 Ach_Asic_Nc0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc0_UNDER_CURR;
    Haluint16 Ach_Asic_Nc1_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc1_PWM_FAULT;
    Haluint8 Ach_Asic_Nc1_CURR_SENSE;
    Haluint8 Ach_Asic_Nc1_ADC_FAULT;
    Haluint8 Ach_Asic_Nc1_T_SD;
    Haluint8 Ach_Asic_Nc1_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc1_GND_LOSS;
    Haluint8 Ach_Asic_Nc1_LVT;
    Haluint8 Ach_Asic_Nc1_LS_OVC;
    Haluint8 Ach_Asic_Nc1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc1_HS_SHORT;
    Haluint8 Ach_Asic_Nc1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc1_UNDER_CURR;
    Haluint16 Ach_Asic_Nc2_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc2_PWM_FAULT;
    Haluint8 Ach_Asic_Nc2_CURR_SENSE;
    Haluint8 Ach_Asic_Nc2_ADC_FAULT;
    Haluint8 Ach_Asic_Nc2_T_SD;
    Haluint8 Ach_Asic_Nc2_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc2_GND_LOSS;
    Haluint8 Ach_Asic_Nc2_LVT;
    Haluint8 Ach_Asic_Nc2_LS_OVC;
    Haluint8 Ach_Asic_Nc2_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc2_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc2_HS_SHORT;
    Haluint8 Ach_Asic_Nc2_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc2_UNDER_CURR;
    Haluint16 Ach_Asic_Nc3_AVG_CURRENT;
    Haluint8 Ach_Asic_Nc3_PWM_FAULT;
    Haluint8 Ach_Asic_Nc3_CURR_SENSE;
    Haluint8 Ach_Asic_Nc3_ADC_FAULT;
    Haluint8 Ach_Asic_Nc3_T_SD;
    Haluint8 Ach_Asic_Nc3_OPEN_LOAD;
    Haluint8 Ach_Asic_Nc3_GND_LOSS;
    Haluint8 Ach_Asic_Nc3_LVT;
    Haluint8 Ach_Asic_Nc3_LS_OVC;
    Haluint8 Ach_Asic_Nc3_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Nc3_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Nc3_HS_SHORT;
    Haluint8 Ach_Asic_Nc3_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Nc3_UNDER_CURR;
    Haluint16 Ach_Asic_Tc0_AVG_CURRENT;
    Haluint8 Ach_Asic_Tc0_PWM_FAULT;
    Haluint8 Ach_Asic_Tc0_CURR_SENSE;
    Haluint8 Ach_Asic_Tc0_ADC_FAULT;
    Haluint8 Ach_Asic_Tc0_T_SD;
    Haluint8 Ach_Asic_Tc0_OPEN_LOAD;
    Haluint8 Ach_Asic_Tc0_GND_LOSS;
    Haluint8 Ach_Asic_Tc0_LVT;
    Haluint8 Ach_Asic_Tc0_LS_OVC;
    Haluint8 Ach_Asic_Tc0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Tc0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Tc0_HS_SHORT;
    Haluint8 Ach_Asic_Tc0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Tc0_UNDER_CURR;
    Haluint16 Ach_Asic_Tc1_AVG_CURRENT;
    Haluint8 Ach_Asic_Tc1_PWM_FAULT;
    Haluint8 Ach_Asic_Tc1_CURR_SENSE;
    Haluint8 Ach_Asic_Tc1_ADC_FAULT;
    Haluint8 Ach_Asic_Tc1_T_SD;
    Haluint8 Ach_Asic_Tc1_OPEN_LOAD;
    Haluint8 Ach_Asic_Tc1_GND_LOSS;
    Haluint8 Ach_Asic_Tc1_LVT;
    Haluint8 Ach_Asic_Tc1_LS_OVC;
    Haluint8 Ach_Asic_Tc1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Tc1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Tc1_HS_SHORT;
    Haluint8 Ach_Asic_Tc1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Tc1_UNDER_CURR;
    Haluint16 Ach_Asic_Esv0_AVG_CURRENT;
    Haluint8 Ach_Asic_Esv0_PWM_FAULT;
    Haluint8 Ach_Asic_Esv0_CURR_SENSE;
    Haluint8 Ach_Asic_Esv0_ADC_FAULT;
    Haluint8 Ach_Asic_Esv0_T_SD;
    Haluint8 Ach_Asic_Esv0_OPEN_LOAD;
    Haluint8 Ach_Asic_Esv0_GND_LOSS;
    Haluint8 Ach_Asic_Esv0_LVT;
    Haluint8 Ach_Asic_Esv0_LS_OVC;
    Haluint8 Ach_Asic_Esv0_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Esv0_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Esv0_HS_SHORT;
    Haluint8 Ach_Asic_Esv0_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Esv0_UNDER_CURR;
    Haluint16 Ach_Asic_Esv1_AVG_CURRENT;
    Haluint8 Ach_Asic_Esv1_PWM_FAULT;
    Haluint8 Ach_Asic_Esv1_CURR_SENSE;
    Haluint8 Ach_Asic_Esv1_ADC_FAULT;
    Haluint8 Ach_Asic_Esv1_T_SD;
    Haluint8 Ach_Asic_Esv1_OPEN_LOAD;
    Haluint8 Ach_Asic_Esv1_GND_LOSS;
    Haluint8 Ach_Asic_Esv1_LVT;
    Haluint8 Ach_Asic_Esv1_LS_OVC;
    Haluint8 Ach_Asic_Esv1_VGS_LS_FAULT;
    Haluint8 Ach_Asic_Esv1_VGS_HS_FAULT;
    Haluint8 Ach_Asic_Esv1_HS_SHORT;
    Haluint8 Ach_Asic_Esv1_LS_CLAMP_ON;
    Haluint8 Ach_Asic_Esv1_UNDER_CURR;
}AchValveAsicInfo_t;

typedef struct
{
    Haluint16 ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
    Haluint16 ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
}AchAdcData_t;

typedef struct
{
    Haluint8 Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
}AchWssPort0AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
}AchWssPort1AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
}AchWssPort2AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
    Haluint16 Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_NODATA;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_INVALID;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_OPEN;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STB;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_STG;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_WSI_OT;
    Haluint8 Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
}AchWssPort3AsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_VSO_T_SD;
    Haluint8 Ach_Asic_VSO_OPENLOAD;
    Haluint8 Ach_Asic_VSO_LVT;
    Haluint8 Ach_Asic_VSO_VGS_LS_FAULT;
    Haluint8 Ach_Asic_VSO_LS_CLAMP_ACT;
    Haluint8 Ach_Asic_VSO_LS_OVC;
    Haluint8 Ach_Asic_GND_LOSS;
    Haluint8 Ach_Asic_SEL_T_SD;
    Haluint8 Ach_Asic_SEL_OPENLOAD;
    Haluint8 Ach_Asic_SEL_LVT;
    Haluint8 Ach_Asic_SEL_VGS_LS_FAULT;
    Haluint8 Ach_Asic_SEL_LS_CLAMP_ACT;
    Haluint8 Ach_Asic_SEL_LS_OVC;
}AchVsoSelAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_WLD_T_SD;
    Haluint8 Ach_Asic_WLD_OPL;
    Haluint8 Ach_Asic_WLD_LVT;
    Haluint8 Ach_Asic_WLD_LS_OVC;
    Haluint8 Ach_Asic_WLD_VGS_LS_FAULT;
    Haluint8 Ach_Asic_WLD_LS_CLAMP;
    Haluint8 Ach_Asic_SHLS_T_SD;
    Haluint8 Ach_Asic_SHLS_OPL;
    Haluint8 Ach_Asic_SHLS_LVT;
    Haluint8 Ach_Asic_SHLS_OVC;
    Haluint8 Ach_Asic_SHLS_VGS_FAULT;
    Haluint8 Ach_Asic_SHLS_LS_CLAMP;
}AchWldShlsAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_CH0_WS1_CNT;
    Haluint8 Ach_Asic_CH1_WS1_CNT;
    Haluint8 Ach_Asic_CH2_WS1_CNT;
    Haluint8 Ach_Asic_CH3_WS1_CNT;
}AchWssRedncyAsicInfo_t;

typedef struct
{
    Haluint8 Ach_Asic_PMP_LD_ACT;
    Haluint8 Ach_Asic_PMP1_TURN_ON;
    Haluint8 Ach_Asic_PMP1_TURN_OFF;
    Haluint8 Ach_Asic_PMP2_TURN_ON;
    Haluint8 Ach_Asic_PMP2_TURN_OFF;
    Haluint8 Ach_Asic_PMP3_TURN_ON;
    Haluint8 Ach_Asic_PMP3_TURN_OFF;
    Haluint8 Ach_Asic_PMP_VDS_TURNOFF;
    Haluint8 Ach_Asic_PMP1_VDS_FAULT;
    Haluint8 Ach_Asic_PMP_FLYBACK;
    Haluint8 Ach_Asic_CP_OV;
    Haluint8 Ach_Asic_CP_UV;
}AchMotorAsicInfo_t;

typedef struct
{
    Haluint8 C0ol;
    Haluint8 C0sb;
    Haluint8 C0sg;
    Haluint8 C1ol;
    Haluint8 C1sb;
    Haluint8 C1sg;
    Haluint8 C2ol;
    Haluint8 C2sb;
    Haluint8 C2sg;
    Haluint8 Fr;
    Haluint8 Ot;
    Haluint8 Lr;
    Haluint8 Uv;
    Haluint8 Ff;
}VlvdFF0DcdInfo_t;

typedef struct
{
    Haluint8 C3ol;
    Haluint8 C3sb;
    Haluint8 C3sg;
    Haluint8 C4ol;
    Haluint8 C4sb;
    Haluint8 C4sg;
    Haluint8 C5ol;
    Haluint8 C5sb;
    Haluint8 C5sg;
    Haluint8 Fr;
    Haluint8 Ot;
    Haluint8 Lr;
    Haluint8 Uv;
    Haluint8 Ff;
}VlvdFF1DcdInfo_t;

typedef struct
{
    Haluint16 VoltVBatt01Mon_Esc;
    Haluint16 VoltVBatt02Mon_Esc;
    Haluint16 VoltGio2Mon_Esc;
    Haluint16 VoltExt5VSupplyMon_Esc;
    Haluint16 VoltExt12VSupplyMon_Esc;
    Haluint16 VoltSwVpwrMon_Esc;
    Haluint16 VoltVpwrMon_Esc;
    Haluint16 VoltGio1Mon_Esc;
    Haluint16 VoltIgnMon_Esc;
}PwrMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltFspMon_Esc;
}VlvMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltMtpMon_Esc;
    Haluint16 VoltMtrFwSMon_Esc;
}MotMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltEpbPwrMon_Esc;
    Haluint16 VoltEpbI1LMon_Esc;
    Haluint16 VoltEpbI1RMon_Esc;
    Haluint16 VoltEpbI2LMon_Esc;
    Haluint16 VoltEpbI2RMon_Esc;
    Haluint16 VoltEpbMonLPlus_Esc;
    Haluint16 VoltEpbMonLMinus_Esc;
    Haluint16 VoltEpbMonRPlus_Esc;
    Haluint16 VoltEpbMonRMinus_Esc;
}EpbMonInfoEsc_t;

typedef struct
{
    Haluint16 VoltMcuAdc1_Esc;
    Haluint16 VoltMcuAdc2_Esc;
    Haluint16 VoltMcuAdc3_Esc;
    Haluint16 VoltMcuAdc4_Esc;
    Haluint16 VoltMcuAdc5_Esc;
}ReservedMonInfoEsc_t;

typedef struct
{
    Haluint16 McpPresData1;
    Haluint16 McpPresData2;
    Haluint8 McpSentCrc;
    Haluint16 McpSentData;
    Haluint8 McpSentMsgId;
    Haluint8 McpSentSerialCrc;
    Haluint8 McpSentConfig;
    Haluint16 Wlp1PresData1;
    Haluint16 Wlp1PresData2;
    Haluint8 Wlp1SentCrc;
    Haluint16 Wlp1SentData;
    Haluint8 Wlp1SentMsgId;
    Haluint8 Wlp1SentSerialCrc;
    Haluint8 Wlp1SentConfig;
    Haluint16 Wlp2PresData1;
    Haluint16 Wlp2PresData2;
    Haluint8 Wlp2SentCrc;
    Haluint16 Wlp2SentData;
    Haluint8 Wlp2SentMsgId;
    Haluint8 Wlp2SentSerialCrc;
    Haluint8 Wlp2SentConfig;
}HalPressureInfo_t;

typedef struct
{
    Haluint16 MotPwrVoltMon;
    Haluint16 MotStarMon;
}MotMonInfo_t;

typedef struct
{
    Haluint16 MotVoltPhUMon;
    Haluint16 MotVoltPhVMon;
    Haluint16 MotVoltPhWMon;
}MotVoltsMonInfo_t;

typedef struct
{
    Haluint16 PdfSigMon;
    Haluint16 PdtSigMon;
}PedlSigMonInfo_t;

typedef struct
{
    Haluint16 Pdt5vMon;
    Haluint16 Pdf5vMon;
}PedlPwrMonInfo_t;

typedef struct
{
    Haluint16 RlCurrMainMon;
    Haluint16 RlCurrSubMon;
    Haluint16 RlMocNegMon;
    Haluint16 RlMocPosMon;
    Haluint16 RrCurrMainMon;
    Haluint16 RrCurrSubMon;
    Haluint16 RrMocNegMon;
    Haluint16 RrMocPosMon;
}MocMonInfo_t;

typedef struct
{
    Haluint16 VlvCVMon;
    Haluint16 VlvRLVMon;
    Haluint16 PrimCutVlvMon;
    Haluint16 SecdCutVlvMon;
    Haluint16 SimVlvMon;
}VlvMonInfo_t;

typedef struct
{
    Haluint16 FlOvCurrMon;
    Haluint16 FlIvDutyMon;
    Haluint16 FrOvCurrMon;
    Haluint16 FrIvDutyMon;
    Haluint16 RlOvCurrMon;
    Haluint16 RlIvDutyMon;
    Haluint16 RrOvCurrMon;
    Haluint16 RrIvDutyMon;
}WhlVlvFbMonInfo_t;

typedef struct
{
    Haluint16 Ext5vMon;
    Haluint16 CEMon;
    Haluint16 Int5vMon;
    Haluint16 CSPMon;
    Haluint16 Vbatt01Mon;
    Haluint16 Vbatt02Mon;
    Haluint16 VddMon;
    Haluint16 Vdd3Mon;
    Haluint16 Vdd5Mon;
}SwTrigPwrInfo_t;

typedef struct
{
    Haluint16 FspAbsHMon;
    Haluint16 FspAbsMon;
    Haluint16 FspCbsHMon;
    Haluint16 FspCbsMon;
}SwTrigFspInfo_t;

typedef struct
{
    Haluint16 LineTestMon;
    Haluint16 TempMon;
}SwTrigTmpInfo_t;

typedef struct
{
    Haluint16 MotPwrMon;
    Haluint16 StarMon;
    Haluint16 UoutMon;
    Haluint16 VoutMon;
    Haluint16 WoutMon;
}SwTrigMotInfo_t;

typedef struct
{
    Haluint16 PdfSigMon;
    Haluint16 Pdt5vMon;
    Haluint16 Pdf5vMon;
    Haluint16 PdtSigMon;
}SwTrigPdtInfo_t;

typedef struct
{
    Haluint16 RlIMainMon;
    Haluint16 RlISubMonFs;
    Haluint16 RlMocNegMon;
    Haluint16 RlMocPosMon;
    Haluint16 RrIMainMon;
    Haluint16 RrISubMonFs;
    Haluint16 RrMocNegMon;
    Haluint16 RrMocPosMon;
    Haluint16 BFLMon;
}SwTrigMocInfo_t;

typedef struct
{
    Haluint16 VlvCVMon;
    Haluint16 VlvRLVMon;
    Haluint16 VlvCutPMon;
    Haluint16 VlvCutSMon;
    Haluint16 VlvSimMon;
}HwTrigVlvInfo_t;

typedef struct
{
    Haluint16 IF_A7_MON;
    Haluint16 IF_A2_MON;
    Haluint16 P1_NEG_MON;
    Haluint16 IF_A6_MON;
    Haluint16 MTR_FW_S_MON;
    Haluint16 IF_A4_MON;
    Haluint16 MTP_MON;
    Haluint16 IF_A3_MON;
    Haluint16 P1_POS_MON;
    Haluint16 IF_A5_MON;
    Haluint16 VDD_MON;
    Haluint16 COOL_MON;
    Haluint16 CAN_L_MON;
    Haluint16 P3_NEG_MON;
    Haluint16 IF_A1_MON;
    Haluint16 P2_POS_MON;
    Haluint16 IF_A8_MON;
    Haluint16 IF_A9_MON;
    Haluint16 P2_NEG_MON;
    Haluint16 P3_POS_MON;
    Haluint16 OP_OUT_MON;
}SwTrigEscInfo_t;

typedef struct
{
    Haluint16 McpPresData1;
    Haluint16 McpPresData2;
    Haluint8 McpSentCrc;
    Haluint16 McpSentData;
    Haluint8 McpSentMsgId;
    Haluint8 McpSentSerialCrc;
    Haluint8 McpSentConfig;
    Haluint32 McpSentInvalid;
    Haluint32 MCPSentSerialInvalid;
    Haluint16 Wlp1PresData1;
    Haluint16 Wlp1PresData2;
    Haluint8 Wlp1SentCrc;
    Haluint16 Wlp1SentData;
    Haluint8 Wlp1SentMsgId;
    Haluint8 Wlp1SentSerialCrc;
    Haluint8 Wlp1SentConfig;
    Haluint32 Wlp1SentInvalid;
    Haluint32 Wlp1SentSerialInvalid;
    Haluint16 Wlp2PresData1;
    Haluint16 Wlp2PresData2;
    Haluint8 Wlp2SentCrc;
    Haluint16 Wlp2SentData;
    Haluint8 Wlp2SentMsgId;
    Haluint8 Wlp2SentSerialCrc;
    Haluint8 Wlp2SentConfig;
    Haluint32 Wlp2SentInvalid;
    Haluint32 Wlp2SentSerialInvalid;
}SentHPressureInfo_t;

typedef struct
{
    Haluint16 D1IIFAngRawData;
    Haluint16 D1IIFAngDegree;
}MpsD1IIFAngInfo_t;

typedef struct
{
    Haluint16 D2IIFAngRawData;
    Haluint16 D2IIFAngDegree;
}MpsD2IIFAngInfo_t;

typedef struct
{
    Haluint16 D1SpiAngRawData;
    Haluint16 D2SpiAngRawData;
    Haluint16 D1SpiAng36000;
}MpsD1SpiAngInfo_t;

typedef struct
{
    Haluint8 S_VR;
    Haluint8 S_WD;
    Haluint8 S_ROM;
    Haluint8 S_ADCT;
    Haluint8 S_DSPU;
    Haluint8 S_FUSE;
    Haluint8 S_MAGOL;
    Haluint8 S_OV;
}MpsD1SpiDcdInfo_t;

typedef struct
{
    Haluint16 D2SpiAng36000;
}MpsD2SpiAngInfo_t;

typedef struct
{
    Haluint8 S_VR;
    Haluint8 S_WD;
    Haluint8 S_ROM;
    Haluint8 S_ADCT;
    Haluint8 S_DSPU;
    Haluint8 S_FUSE;
    Haluint8 S_MAGOL;
    Haluint8 S_OV;
}MpsD2SpiDcdInfo_t;

typedef struct
{
    Haluint8 mgdIdleM;
    Haluint8 mgdConfM;
    Haluint8 mgdConfLock;
    Haluint8 mgdSelfTestM;
    Haluint8 mgdSoffM;
    Haluint8 mgdErrM;
    Haluint8 mgdRectM;
    Haluint8 mgdNormM;
    Haluint8 mgdOsf;
    Haluint8 mgdOp;
    Haluint8 mgdScd;
    Haluint8 mgdSd;
    Haluint8 mgdIndiag;
    Haluint8 mgdOutp;
    Haluint8 mgdExt;
    Haluint8 mgdInt12;
    Haluint8 mgdRom;
    Haluint8 mgdLimpOn;
    Haluint8 mgdStIncomplete;
    Haluint8 mgdApcAct;
    Haluint8 mgdGtm;
    Haluint8 mgdCtrlRegInvalid;
    Haluint8 mgdLfw;
    Haluint8 mgdErrOtW;
    Haluint8 mgdErrOvReg1;
    Haluint8 mgdErrUvVccRom;
    Haluint8 mgdErrUvReg4;
    Haluint8 mgdErrOvReg6;
    Haluint8 mgdErrUvReg6;
    Haluint8 mgdErrUvReg5;
    Haluint8 mgdErrUvCb;
    Haluint8 mgdErrClkTrim;
    Haluint8 mgdErrUvBs3;
    Haluint8 mgdErrUvBs2;
    Haluint8 mgdErrUvBs1;
    Haluint8 mgdErrCp2;
    Haluint8 mgdErrCp1;
    Haluint8 mgdErrOvBs3;
    Haluint8 mgdErrOvBs2;
    Haluint8 mgdErrOvBs1;
    Haluint8 mgdErrOvVdh;
    Haluint8 mgdErrUvVdh;
    Haluint8 mgdErrOvVs;
    Haluint8 mgdErrUvVs;
    Haluint8 mgdErrUvVcc;
    Haluint8 mgdErrOvVcc;
    Haluint8 mgdErrOvLdVdh;
    Haluint8 mgdSdDdpStuck;
    Haluint8 mgdSdCp1;
    Haluint8 mgdSdOvCp;
    Haluint8 mgdSdClkfail;
    Haluint8 mgdSdUvCb;
    Haluint8 mgdSdOvVdh;
    Haluint8 mgdSdOvVs;
    Haluint8 mgdSdOt;
    Haluint8 mgdErrScdLs3;
    Haluint8 mgdErrScdLs2;
    Haluint8 mgdErrScdLs1;
    Haluint8 mgdErrScdHs3;
    Haluint8 mgdErrScdHs2;
    Haluint8 mgdErrScdHs1;
    Haluint8 mgdErrIndLs3;
    Haluint8 mgdErrIndLs2;
    Haluint8 mgdErrIndLs1;
    Haluint8 mgdErrIndHs3;
    Haluint8 mgdErrIndHs2;
    Haluint8 mgdErrIndHs1;
    Haluint8 mgdErrOsfLs1;
    Haluint8 mgdErrOsfLs2;
    Haluint8 mgdErrOsfLs3;
    Haluint8 mgdErrOsfHs1;
    Haluint8 mgdErrOsfHs2;
    Haluint8 mgdErrOsfHs3;
    Haluint8 mgdErrSpiFrame;
    Haluint8 mgdErrSpiTo;
    Haluint8 mgdErrSpiWd;
    Haluint8 mgdErrSpiCrc;
    Haluint8 mgdEpiAddInvalid;
    Haluint8 mgdEonfTo;
    Haluint8 mgdEonfSigInvalid;
    Haluint8 mgdErrOcOp1;
    Haluint8 mgdErrOp1Uv;
    Haluint8 mgdErrOp1Ov;
    Haluint8 mgdErrOp1Calib;
    Haluint8 mgdErrOcOp2;
    Haluint8 mgdErrOp2Uv;
    Haluint8 mgdErrOp2Ov;
    Haluint8 mgdErrOp2Calib;
    Haluint8 mgdErrOcOp3;
    Haluint8 mgdErrOp3Uv;
    Haluint8 mgdErrOp3Ov;
    Haluint8 mgdErrOp3Calib;
    Haluint8 mgdErrOutpErrn;
    Haluint8 mgdErrOutpMiso;
    Haluint8 mgdErrOutpPFB1;
    Haluint8 mgdErrOutpPFB2;
    Haluint8 mgdErrOutpPFB3;
    Haluint8 mgdTle9180ErrPort;
}MgdDcdInfo_t;

typedef struct
{
    Haluint16 Uphase0Mon;
    Haluint16 Uphase1Mon;
    Haluint16 Vphase0Mon;
    Haluint16 VPhase1Mon;
}HwTrigMotInfo_t;

typedef struct
{
    Haluint16 FlRisngIdx;
    Haluint16 FrRisngIdx;
    Haluint16 RlRisngIdx;
    Haluint16 RrRisngIdx;
}RisngIdxInfo_t;

typedef struct
{
    Haluint16 FlFallIdx;
    Haluint16 FrFallIdx;
    Haluint16 RlFallIdx;
    Haluint16 RrFallIdx;
}FallIdxInfo_t;

typedef struct
{
    Haluint32 FlRisngTiStamp[32];
    Haluint32 FrRisngTiStamp[32];
    Haluint32 RlRisngTiStamp[32];
    Haluint32 RrRisngTiStamp[32];
}RisngTiStampInfo_t;

typedef struct
{
    Haluint32 FlFallTiStamp[32];
    Haluint32 FrFallTiStamp[32];
    Haluint32 RlFallTiStamp[32];
    Haluint32 RrFallTiStamp[32];
}FallTiStampInfo_t;

typedef struct
{
    Haluint16 MotCurrPhUMon0;
    Haluint16 MotCurrPhUMon1;
    Haluint16 MotCurrPhVMon0;
    Haluint16 MotCurrPhVMon1;
}MotCurrMonInfo_t;

typedef struct
{
    Haluint8 AvhSwtMon;
    Haluint8 BflSwtMon;
    Haluint8 BlsSwtMon;
    Haluint8 BsSwtMon;
    Haluint8 DoorSwtMon;
    Haluint8 EscSwtMon;
    Haluint8 FlexBrkASwtMon;
    Haluint8 FlexBrkBSwtMon;
    Haluint8 HzrdSwtMon;
    Haluint8 HdcSwtMon;
    Haluint8 PbSwtMon;
}SwtMonInfo_t;

typedef struct
{
    Haluint8 BlsSwtMon_Esc;
    Haluint8 AvhSwtMon_Esc;
    Haluint8 EscSwtMon_Esc;
    Haluint8 HdcSwtMon_Esc;
    Haluint8 PbSwtMon_Esc;
    Haluint8 GearRSwtMon_Esc;
    Haluint8 BlfSwtMon_Esc;
    Haluint8 ClutchSwtMon_Esc;
    Haluint8 ItpmsSwtMon_Esc;
}SwtMonInfoEsc_t;

typedef struct
{
    Haluint8 RlyDbcMon;
    Haluint8 RlyEssMon;
    Haluint8 RlyFault;
}RlyMonInfo_t;

typedef struct
{
    Haluint8 Sw1LineCMon;
    Haluint8 Sw2LineBMon;
    Haluint8 Sw3LineEMon;
    Haluint8 Sw4LineFMon;
}SwLineMonInfo_t;

typedef struct
{
    Haluint16 FlRisngIdx;
    Haluint16 FlFallIdx;
    Haluint32 FlRisngTiStamp[32];
    Haluint32 FlFallTiStamp[32];
    Haluint16 FrRisngIdx;
    Haluint16 FrFallIdx;
    Haluint32 FrRisngTiStamp[32];
    Haluint32 FrFallTiStamp[32];
    Haluint16 RlRisngIdx;
    Haluint16 RlFallIdx;
    Haluint32 RlRisngTiStamp[32];
    Haluint32 RlFallTiStamp[32];
    Haluint16 RrRisngIdx;
    Haluint16 RrFallIdx;
    Haluint32 RrRisngTiStamp[32];
    Haluint32 RrFallTiStamp[32];
}WssMonInfo_t;

typedef struct
{
    Haluint16 MotPosiAngle1deg;
    Haluint16 MotPosiAngle2deg;
    Haluint16 MotPosiAngle1raw;
    Haluint16 MotPosiAngle2raw;
}MotAngleMonInfo_t;

typedef struct
{
    Haluint8 MtrDutyDataFlg;
    Haluint8 ACH_Tx8_PUMP_DTY_PWM;
}IocDcMtrDutyData_t;

typedef struct
{
    Haluint8 MtrFreqDataFlg;
    Haluint8 ACH_Tx8_PUMP_TCK_PWM;
}IocDcMtrFreqData_t;

typedef struct
{
    Haluint8 VlvNo0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo0Data_t;

typedef struct
{
    Haluint8 VlvNo1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo1Data_t;

typedef struct
{
    Haluint8 VlvNo2DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo2Data_t;

typedef struct
{
    Haluint8 VlvNo3DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNo3Data_t;

typedef struct
{
    Haluint8 VlvTc0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvTc0Data_t;

typedef struct
{
    Haluint8 VlvTc1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvTc1Data_t;

typedef struct
{
    Haluint8 VlvEsv0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvEsv0Data_t;

typedef struct
{
    Haluint8 VlvEsv1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvEsv1Data_t;

typedef struct
{
    Haluint8 VlvNc0DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc0Data_t;

typedef struct
{
    Haluint8 VlvNc1DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc1Data_t;

typedef struct
{
    Haluint8 VlvNc2DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc2Data_t;

typedef struct
{
    Haluint8 VlvNc3DataFlg;
    Haluint16 ACH_TxValve_SET_POINT;
}IocVlvNc3Data_t;

typedef struct
{
    Haluint8 VlvRelayDataFlg;
    Haluint8 ACH_Tx1_FS_CMD;
}IocVlvRelayData_t;

typedef struct
{
    Haluint8 AdcSelDataFlg;
    Haluint8 ACH_Tx13_ADC_SEL;
}IocAdcSelWriteData_t;


typedef Haluint32 AchAsicInvalidInfo_t;
typedef Haluint16 VBatt1Mon_t;
typedef Haluint16 VBatt2Mon_t;
typedef Haluint16 FspCbsHMon_t;
typedef Haluint16 FspCbsMon_t;
typedef Haluint32 VlvdInvalid_t;
typedef Haluint8 AcmAsicInitCompleteFlag_t;
typedef Haluint16 FspAbsHMon_t;
typedef Haluint16 FspAbsMon_t;
typedef Haluint16 Ext5vMon_t;
typedef Haluint16 CEMon_t;
typedef Haluint16 LineTestMon_t;
typedef Haluint16 TempMon_t;
typedef Haluint16 Vdd1Mon_t;
typedef Haluint16 Vdd2Mon_t;
typedef Haluint16 Vdd3Mon_t;
typedef Haluint16 Vdd4Mon_t;
typedef Haluint16 Vdd5Mon_t;
typedef Haluint16 VddMon_t;
typedef Haluint16 CspMon_t;
typedef Haluint16 Int5vMon_t;
typedef Haluint16 BFLMon_t;
typedef Haluint32 AdcInvalid_t;
typedef Haluint32 MpsInvalid_t;
typedef Haluint32 MgdInvalid_t;
typedef Haluint8 RsmDbcMon_t;
typedef Haluint8 PbcVdaAi_t;
typedef Haluint8 MainCanEn_t;
typedef Haluint8 VlvDrvEnRst_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* HAL_TYPES_H_ */
/** @} */
