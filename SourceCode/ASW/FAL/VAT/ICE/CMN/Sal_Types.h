/**
 * @defgroup Bsw_Types Bsw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bsw_Types.h
 * @brief       Template file
 * @date        2014. 7. 31.
 ******************************************************************************/

#ifndef SAL_TYPES_H_
#define SAL_TYPES_H_


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef sint8 Salsint8;
typedef uint8 Saluint8;
typedef sint16 Salsint16;
typedef uint16 Saluint16;
typedef sint32 Salsint32;
typedef uint32 Saluint32;
typedef float32 Salfloat32;
typedef float64 Salfloat64;

typedef struct
{
    Saluint8 fs_on_sim_time;
    Saluint8 fs_on_cutp_time;
    Saluint8 fs_on_cuts_time;
    Saluint8 fs_on_rlv_time;
    Saluint8 fs_on_cv_time;
}FSBbsVlvActr_t;

typedef struct
{
    Saluint8 FsrCbsDrv;
    Saluint8 FsrCbsOff;
}FSFsrBbsDrvInfo_t;

typedef struct
{
    Saluint8 Batt1Fuse_Open_Err;
    Saluint8 BbsVlvRelay_Open_Err;
    Saluint8 BbsVlvRelay_S2G_Err;
    Saluint8 BbsVlvRelay_S2B_Err;
    Saluint8 BbsVlvRelay_Short_Err;
    Saluint8 BbsVlvRelay_OverTemp_Err;
    Saluint8 BbsVlvRelay_ShutdownLine_Err;
    Saluint8 BbsVlvRelay_CP_Err;
    Saluint8 BbsVlv_Sim_Open_Err;
    Saluint8 BbsVlv_Sim_PsvOpen_Err;
    Saluint8 BbsVlv_Sim_Short_Err;
    Saluint8 BbsVlv_Sim_CurReg_Err;
    Saluint8 BbsVlv_CutP_Open_Err;
    Saluint8 BbsVlv_CutP_PsvOpen_Err;
    Saluint8 BbsVlv_CutP_Short_Err;
    Saluint8 BbsVlv_CutP_CurReg_Err;
    Saluint8 BbsVlv_CutS_Open_Err;
    Saluint8 BbsVlv_CutS_PsvOpen_Err;
    Saluint8 BbsVlv_CutS_Short_Err;
    Saluint8 BbsVlv_CutS_CurReg_Err;
    Saluint8 BbsVlv_Rlv_Open_Err;
    Saluint8 BbsVlv_Rlv_PsvOpen_Err;
    Saluint8 BbsVlv_Rlv_Short_Err;
    Saluint8 BbsVlv_Rlv_CurReg_Err;
    Saluint8 BbsVlv_CircVlv_Open_Err;
    Saluint8 BbsVlv_CircVlv_PsvOpen_Err;
    Saluint8 BbsVlv_CircVlv_Short_Err;
    Saluint8 BbsVlv_CircVlv_CurReg_Err;
}BBSVlvErrInfo_t;

typedef struct
{
    Saluint8 FsrCbsDrv;
    Saluint8 FsrCbsOff;
}ArbFsrBbsDrvInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 RelsVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 CircVlvReqData[5];
    Saluint16 SimVlvReqData[5];
}NormVlvReqInfo_t;

typedef struct
{
    Haluint16 PrimCircVlv1msCurrMon;
    Haluint16 SecdCirCVlv1msCurrMon;
    Haluint16 PrimCutVlv1msCurrMon;
    Haluint16 SecdCutVlv1msCurrMon;
    Haluint16 SimVlv1msCurrMon;
}NormVlvCurrInfo_t;

typedef struct
{
    Saluint8 FsrAbsDrv;
    Saluint8 FsrAbsOff;
}ArbFsrAbsDrvInfo_t;

typedef struct
{
    Saluint16 FlOvReqData[5];
    Saluint16 FlIvReqData[5];
    Saluint16 FrOvReqData[5];
    Saluint16 FrIvReqData[5];
    Saluint16 RlOvReqData[5];
    Saluint16 RlIvReqData[5];
    Saluint16 RrOvReqData[5];
    Saluint16 RrIvReqData[5];
}ArbWhlVlvReqInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 SimVlvReqData[5];
    Saluint16 RelsVlvReqData[5];
    Saluint16 CircVlvReqData[5];
    Saluint16 PressDumpVlvReqData[5];
}ArbNormVlvReqInfo_t;

typedef struct
{
    Saluint16 BalVlvReqData[5];
}ArbBalVlvReqInfo_t;

typedef struct
{
    Saluint16 ResPVlvReqData[5];
}ArbResPVlvReqInfo_t;

typedef struct
{
    Saluint8 fs_on_flno_time;
    Saluint8 fs_on_frno_time;
    Saluint8 fs_on_rlno_time;
    Saluint8 fs_on_rrno_time;
    Saluint8 fs_on_bal_time;
    Saluint8 fs_on_pressdump_time;
    Saluint8 fs_on_resp_time;
    Saluint8 fs_on_flnc_time;
    Saluint8 fs_on_frnc_time;
    Saluint8 fs_on_rlnc_time;
    Saluint8 fs_on_rrnc_time;
}FSEscVlvActr_t;

typedef struct
{
    Saluint8 FsrAbsDrv;
    Saluint8 FsrAbsOff;
}FSFsrAbsDrvInfo_t;

typedef struct
{
    Salsint32 PtcReqData[5];
    Salsint32 StcReqData[5];
    Salsint32 PesvReqData[5];
    Salsint32 SesvReqData[5];
    Salsint32 PtcReq;
    Salsint32 StcReq;
    Salsint32 PesvReq;
    Salsint32 SesvReq;
}EscVlvReqAswInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvReqData[5];
    Saluint16 SecdCutVlvReqData[5];
    Saluint16 PrimCircVlvReqData[5];
    Saluint16 SecdCircVlvReqData[5];
    Saluint16 SimVlvReqData[5];
    Saluint8 PrimCutVlvReq;
    Saluint8 SecdCutVlvReq;
    Saluint8 PrimCircVlvReq;
    Saluint8 SecdCircVlvReq;
    Saluint8 SimVlvReq;
    Saluint8 PrimCutVlvDataLen;
    Saluint8 SecdCutVlvDataLen;
    Saluint8 PrimCircVlvDataLen;
    Saluint8 SecdCircVlvDataLen;
    Saluint8 SimVlvDataLen;
}NormVlvReqSesInfo_t;

typedef struct
{
    Saluint16 FlIvReqData[5];
    Saluint16 FrIvReqData[5];
    Saluint16 RlIvReqData[5];
    Saluint16 RrIvReqData[5];
    Saluint8 FlIvReq;
    Saluint8 FrIvReq;
    Saluint8 RlIvReq;
    Saluint8 RrIvReq;
    Saluint8 FlIvDataLen;
    Saluint8 FrIvDataLen;
    Saluint8 RlIvDataLen;
    Saluint8 RrIvDataLen;
}WhlVlvReqSesInfo_t;

typedef struct
{
    Saluint8 Eem_Fail_BBSSol;
    Saluint8 Eem_Fail_ESCSol;
    Saluint8 Eem_Fail_FrontSol;
    Saluint8 Eem_Fail_RearSol;
    Saluint8 Eem_Fail_Motor;
    Saluint8 Eem_Fail_MPS;
    Saluint8 Eem_Fail_MGD;
    Saluint8 Eem_Fail_BBSValveRelay;
    Saluint8 Eem_Fail_ESCValveRelay;
    Saluint8 Eem_Fail_ECUHw;
    Saluint8 Eem_Fail_ASIC;
    Saluint8 Eem_Fail_OverVolt;
    Saluint8 Eem_Fail_UnderVolt;
    Saluint8 Eem_Fail_LowVolt;
    Saluint8 Eem_Fail_LowerVolt;
    Saluint8 Eem_Fail_SenPwr_12V;
    Saluint8 Eem_Fail_SenPwr_5V;
    Saluint8 Eem_Fail_Yaw;
    Saluint8 Eem_Fail_Ay;
    Saluint8 Eem_Fail_Ax;
    Saluint8 Eem_Fail_Str;
    Saluint8 Eem_Fail_CirP1;
    Saluint8 Eem_Fail_CirP2;
    Saluint8 Eem_Fail_SimP;
    Saluint8 Eem_Fail_BLS;
    Saluint8 Eem_Fail_ESCSw;
    Saluint8 Eem_Fail_HDCSw;
    Saluint8 Eem_Fail_AVHSw;
    Saluint8 Eem_Fail_BrakeLampRelay;
    Saluint8 Eem_Fail_EssRelay;
    Saluint8 Eem_Fail_GearR;
    Saluint8 Eem_Fail_Clutch;
    Saluint8 Eem_Fail_ParkBrake;
    Saluint8 Eem_Fail_PedalPDT;
    Saluint8 Eem_Fail_PedalPDF;
    Saluint8 Eem_Fail_BrakeFluid;
    Saluint8 Eem_Fail_TCSTemp;
    Saluint8 Eem_Fail_HDCTemp;
    Saluint8 Eem_Fail_SCCTemp;
    Saluint8 Eem_Fail_TVBBTemp;
    Saluint8 Eem_Fail_MainCanLine;
    Saluint8 Eem_Fail_SubCanLine;
    Saluint8 Eem_Fail_EMSTimeOut;
    Saluint8 Eem_Fail_FWDTimeOut;
    Saluint8 Eem_Fail_TCUTimeOut;
    Saluint8 Eem_Fail_HCUTimeOut;
    Saluint8 Eem_Fail_MCUTimeOut;
    Saluint8 Eem_Fail_VariantCoding;
    Saluint8 Eem_Fail_WssFL;
    Saluint8 Eem_Fail_WssFR;
    Saluint8 Eem_Fail_WssRL;
    Saluint8 Eem_Fail_WssRR;
    Saluint8 Eem_Fail_SameSideWss;
    Saluint8 Eem_Fail_DiagonalWss;
    Saluint8 Eem_Fail_FrontWss;
    Saluint8 Eem_Fail_RearWss;
}EemFailData_t;

typedef struct
{
    Saluint8 Eem_CtrlIhb_Cbs;
    Saluint8 Eem_CtrlIhb_Ebd;
    Saluint8 Eem_CtrlIhb_Abs;
    Saluint8 Eem_CtrlIhb_Edc;
    Saluint8 Eem_CtrlIhb_Btcs;
    Saluint8 Eem_CtrlIhb_Etcs;
    Saluint8 Eem_CtrlIhb_Tcs;
    Saluint8 Eem_CtrlIhb_Vdc;
    Saluint8 Eem_CtrlIhb_Hsa;
    Saluint8 Eem_CtrlIhb_Hdc;
    Saluint8 Eem_CtrlIhb_Pba;
    Saluint8 Eem_CtrlIhb_Avh;
    Saluint8 Eem_CtrlIhb_Moc;
    Saluint8 Eem_BBS_AllControlInhibit;
    Saluint8 Eem_BBS_DiagControlInhibit;
    Saluint8 Eem_BBS_DegradeModeFail;
    Saluint8 Eem_BBS_DefectiveModeFail;
}EemCtrlInhibitData_t;

typedef struct
{
    Saluint16 SolenoidDrvDuty;
    Saluint8 FlOvReqData;
    Saluint8 FlIvReqData;
    Saluint8 FrOvReqData;
    Saluint8 FrIvReqData;
    Saluint8 RlOvReqData;
    Saluint8 RlIvReqData;
    Saluint8 RrOvReqData;
    Saluint8 RrIvReqData;
    Saluint8 PrimCutVlvReqData;
    Saluint8 SecdCutVlvReqData;
    Saluint8 SimVlvReqData;
    Saluint8 ResPVlvReqData;
    Saluint8 BalVlvReqData;
    Saluint8 CircVlvReqData;
    Saluint8 PressDumpReqData;
    Saluint8 RelsVlvReqData;
}DiagVlvActr_t;

typedef struct
{
    Saluint8 AbsVlvM_ValveRelay_Open_Err;
    Saluint8 AbsVlvM_ValveRelay_S2G_Err;
    Saluint8 AbsVlvM_ValveRelay_S2B_Err;
    Saluint8 AbsVlvM_ValveRelay_CP_Err;
    Saluint8 AbsVlvM_ValveRelay_Short_Err;
    Saluint8 AbsVlvM_ValveRelay_Batt1FuseOpen_Err;
    Saluint8 AbsVlvM_ValveRelay_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRelay_ShutdownLine_Err;
    Saluint8 AbsVlvM_ValveFLNO_Open_Err;
    Saluint8 AbsVlvM_ValveFLNO_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveFLNO_Short_Err;
    Saluint8 AbsVlvM_ValveFLNO_OverTemp_Err;
    Saluint8 AbsVlvM_ValveFLNO_CurReg_Err;
    Saluint8 AbsVlvM_ValveFRNO_Open_Err;
    Saluint8 AbsVlvM_ValveFRNO_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveFRNO_Short_Err;
    Saluint8 AbsVlvM_ValveFRNO_OverTemp_Err;
    Saluint8 AbsVlvM_ValveFRNO_CurReg_Err;
    Saluint8 AbsVlvM_ValveRLNO_Open_Err;
    Saluint8 AbsVlvM_ValveRLNO_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveRLNO_Short_Err;
    Saluint8 AbsVlvM_ValveRLNO_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRLNO_CurReg_Err;
    Saluint8 AbsVlvM_ValveRRNO_Open_Err;
    Saluint8 AbsVlvM_ValveRRNO_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveRRNO_Short_Err;
    Saluint8 AbsVlvM_ValveRRNO_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRRNO_CurReg_Err;
    Saluint8 AbsVlvM_ValveBAL_Open_Err;
    Saluint8 AbsVlvM_ValveBAL_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveBAL_Short_Err;
    Saluint8 AbsVlvM_ValveBAL_OverTemp_Err;
    Saluint8 AbsVlvM_ValveBAL_CurReg_Err;
    Saluint8 AbsVlvM_ValvePD_Open_Err;
    Saluint8 AbsVlvM_ValvePD_PsvOpen_Err;
    Saluint8 AbsVlvM_ValvePD_Short_Err;
    Saluint8 AbsVlvM_ValvePD_OverTemp_Err;
    Saluint8 AbsVlvM_ValvePD_CurReg_Err;
    Saluint8 AbsVlvM_ValveRESP_Open_Err;
    Saluint8 AbsVlvM_ValveRESP_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveRESP_Short_Err;
    Saluint8 AbsVlvM_ValveRESP_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRESP_CurReg_Err;
    Saluint8 AbsVlvM_ValveFLNC_Open_Err;
    Saluint8 AbsVlvM_ValveFLNC_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveFLNC_Short_Err;
    Saluint8 AbsVlvM_ValveFLNC_OverTemp_Err;
    Saluint8 AbsVlvM_ValveFLNC_CurReg_Err;
    Saluint8 AbsVlvM_ValveFRNC_Open_Err;
    Saluint8 AbsVlvM_ValveFRNC_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveFRNC_Short_Err;
    Saluint8 AbsVlvM_ValveFRNC_OverTemp_Err;
    Saluint8 AbsVlvM_ValveFRNC_CurReg_Err;
    Saluint8 AbsVlvM_ValveRLNC_Open_Err;
    Saluint8 AbsVlvM_ValveRLNC_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveRLNC_Short_Err;
    Saluint8 AbsVlvM_ValveRLNC_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRLNC_CurReg_Err;
    Saluint8 AbsVlvM_ValveRRNC_Open_Err;
    Saluint8 AbsVlvM_ValveRRNC_PsvOpen_Err;
    Saluint8 AbsVlvM_ValveRRNC_Short_Err;
    Saluint8 AbsVlvM_ValveRRNC_OverTemp_Err;
    Saluint8 AbsVlvM_ValveRRNC_CurReg_Err;
}AbsVlvMData_t;

typedef struct
{
    Salsint16 PrimCircPSig;
    Salsint16 SecdCircPSig;
}CircP1msRawInfo_t;

typedef struct
{
    Salsint16 PistPSig;
}PistP1msRawInfo_t;

typedef struct
{
    Salsint16 PdtSig;
}Pdt1msRawInfo_t;

typedef struct
{
    Saluint8 HcuRegenEna;
    Saluint16 HcuRegenBrkTq;
}CanRxRegenInfo_t;

typedef struct
{
    Salsint16 AccelPedlVal;
    Saluint8 AccelPedlValErr;
}CanRxAccelPedlInfo_t;

typedef struct
{
    Saluint8 EngMsgFault;
    Saluint8 TarGearPosi;
    Saluint8 GearSelDisp;
    Saluint8 TcuSwiGs;
    Saluint8 HcuServiceMod;
    Saluint8 SubCanBusSigFault;
    Salsint16 CoolantTemp;
    Saluint8 CoolantTempErr;
}CanRxIdbInfo_t;

typedef struct
{
    Salsint16 EngTemp;
    Saluint8 EngTempErr;
}CanRxEngTempInfo_t;

typedef struct
{
    Salsint16 Ax;
    Salsint16 YawRate;
    Salsint16 SteeringAngle;
    Salsint16 Ay;
    Saluint8 PbSwt;
    Saluint8 ClutchSwt;
    Saluint8 GearRSwt;
    Salsint16 EngActIndTq;
    Salsint16 EngRpm;
    Salsint16 EngIndTq;
    Salsint16 EngFrictionLossTq;
    Salsint16 EngStdTq;
    Saluint16 TurbineRpm;
    Salsint16 ThrottleAngle;
    Salsint16 TpsResol1000;
    Salsint16 PvAvCanResol1000;
    Saluint8 EngChr;
    Saluint8 EngVol;
    Saluint8 GearType;
    Saluint8 EngClutchState;
    Salsint16 EngTqCmdBeforeIntv;
    Salsint16 MotEstTq;
    Salsint16 MotTqCmdBeforeIntv;
    Salsint16 TqIntvTCU;
    Salsint16 TqIntvSlowTCU;
    Salsint16 TqIncReq;
    Salsint16 DecelReq;
    Saluint8 RainSnsStat;
    Saluint8 EngSpdErr;
    Saluint8 AtType;
    Saluint8 MtType;
    Saluint8 CvtType;
    Saluint8 DctType;
    Saluint8 HevAtTcu;
    Saluint8 TurbineRpmErr;
    Saluint8 ThrottleAngleErr;
    Saluint8 TopTrvlCltchSwtAct;
    Saluint8 TopTrvlCltchSwtActV;
    Saluint8 HillDesCtrlMdSwtAct;
    Saluint8 HillDesCtrlMdSwtActV;
    Saluint8 WiperIntSW;
    Saluint8 WiperLow;
    Saluint8 WiperHigh;
    Saluint8 WiperValid;
    Saluint8 WiperAuto;
    Saluint8 TcuFaultSts;
}CanRxEscInfo_t;

typedef struct
{
    Saluint8 IgnRun;
}CanRxClu2Info_t;

typedef struct
{
    Salsint32 MotPwmPhUData;
    Salsint32 MotPwmPhVData;
    Salsint32 MotPwmPhWData;
    Saluint8 MotReq;
}MotReqDataDiagInfo_t;

typedef struct
{
    Salsint32 MotPwmPhUData;
    Salsint32 MotPwmPhVData;
    Salsint32 MotPwmPhWData;
    Saluint8 MotReq;
}MotReqDataAcmctlInfo_t;

typedef struct
{
    Salsint32 MotCurrPhUMeasd;
    Salsint32 MotCurrPhVMeasd;
    Salsint32 MotCurrPhWMeasd;
}MotCurrInfo_t;

typedef struct
{
    Saluint16 MotElecAngle1;
    Saluint16 MotMechAngle1;
}MotAngle1Info_t;

typedef struct
{
    Saluint16 MotElecAngle2;
    Saluint16 MotMechAngle2;
}MotAngle2Info_t;

typedef struct
{
    Salsint32 MotIqMeasd;
    Salsint32 MotIdMeasd;
}MotDqIMeasdInfo_t;

typedef struct
{
    Salsint32 StkPosnMeasd;
    Salsint32 MotElecAngleFild;
    Salsint32 MotMechAngleSpdFild;
}MotRotgAgSigInfo_t;

typedef struct
{
    Salsint32 StkPosnMeasdBfMotOrgSet;
    Salsint32 MotMechAngleSpdBfMotOrgSet;
}MotRotgAgSigBfMotOrgSetInfo_t;

typedef struct
{
    Salsint16 PdtSig;
}Pdt5msRawInfo_t;

typedef struct
{
    Salsint16 PdfSig;
    Salsint16 MoveAvrPdfSig;
    Salsint16 MoveAvrPdfSigEolOffset;
    Salsint16 MoveAvrPdtSig;
    Salsint16 MoveAvrPdtSigEolOffset;
}Pdf5msRawInfo_t;

typedef struct
{
    Saluint8 MotOrgSetErr;
    Saluint8 MotOrgSet;
}MotOrgSetStInfo_t;

typedef struct
{
    Salsint32 IdRef;
    Salsint32 IqRef;
}MotDqIRefSesInfo_t;

typedef struct
{
    Salsint32 PosiWallTemp;
    Salsint32 WallDetectedFlg;
}PwrPistStBfMotOrgSetInfo_t;

typedef struct
{
    Saluint8 FlexBrkASwt;
    Saluint8 FlexBrkBSwt;
}SwtStsFlexBrkInfo_t;

typedef struct
{
    Saluint8 BlsSwt_Esc;
    Saluint8 AvhSwt_Esc;
    Saluint8 EscSwt_Esc;
    Saluint8 HdcSwt_Esc;
    Saluint8 PbSwt_Esc;
    Saluint8 GearRSwt_Esc;
    Saluint8 BlfSwt_Esc;
    Saluint8 ClutchSwt_Esc;
    Saluint8 ItpmsSwt_Esc;
}SwtInfoEsc_t;

typedef struct
{
    Saluint8 EscDisabledBySwt;
    Saluint8 TcsDisabledBySwt;
    Saluint8 HdcEnabledBySwt;
}EscSwtStInfo_t;

typedef struct
{
    Saluint8 FlWhlPulseCnt;
    Saluint8 FrWhlPulseCnt;
    Saluint8 RlWhlPulseCnt;
    Saluint8 RrWhlPulseCnt;
}WhlPulseCntInfo_t;

typedef struct
{
    Saluint16 FlWhlSpd;
    Saluint16 FrWhlSpd;
    Saluint16 RlWhlSpd;
    Saluint16 RrlWhlSpd;
}WhlSpdInfo_t;

typedef struct
{
    Saluint16 FlWhlEdgeCnt;
    Saluint16 FrWhlEdgeCnt;
    Saluint16 RlWhlEdgeCnt;
    Saluint16 RrWhlEdgeCnt;
}WhlEdgeCntInfo_t;

typedef struct
{
    Saluint8 FlWhlSnsrType;
    Saluint8 FrWhlSnsrType;
    Saluint8 RlWhlSnsrType;
    Saluint8 RrWhlSnsrType;
}WhlSnsrTypeInfo_t;

typedef struct
{
    Saluint16 PrimCutVlvDrvData;
    Saluint16 RelsVlvDrvData;
    Saluint16 SecdCutVlvDrvData;
    Saluint16 CircVlvDrvData;
    Saluint16 SimVlvDrvData;
}NormVlvDrvInfo_t;

typedef struct
{
    Saluint16 FlOvDrvData;
    Saluint16 FlIvDrvData;
    Saluint16 FrOvDrvData;
    Saluint16 FrIvDrvData;
    Saluint16 RlOvDrvData;
    Saluint16 RlIvDrvData;
    Saluint16 RrOvDrvData;
    Saluint16 RrIvDrvData;
}WhlVlvDrvInfo_t;

typedef struct
{
    Saluint16 PrimBalVlvDrvData;
    Saluint16 PressDumpVlvDrvData;
    Saluint16 ChmbBalVlvDrvData;
}BalVlvDrvInfo_t;

typedef struct
{
    Saluint8 RlyDbcDrv;
    Saluint8 RlyEssDrv;
}RlyDrvInfo_t;

typedef struct
{
    Salsint16 PrimCircPSig;
    Salsint16 SecdCircPSig;
}CircP5msRawInfo_t;

typedef struct
{
    Salsint16 PistPSig;
}PistP5msRawInfo_t;

typedef struct
{
    Salsint16 Pres_MCP_1_100_bar_Esc;
    Salsint16 Pres_FLP_1_100_bar_Esc;
    Salsint16 Pres_FRP_1_100_bar_Esc;
    Salsint16 Pres_RLP_1_100_bar_Esc;
    Salsint16 Pres_RRP_1_100_bar_Esc;
    Salsint16 Pres_MCP_1_100_temp_Esc;
    Salsint16 Pres_FLP_1_100_temp_Esc;
    Salsint16 Pres_FRP_1_100_temp_Esc;
    Salsint16 Pres_RLP_1_100_temp_Esc;
    Salsint16 Pres_RRP_1_100_temp_Esc;
}PresCalcEsc_t;

typedef struct
{
    Salsint32 PdtRaw[5];
}PdtBufInfo_t;

typedef struct
{
    Salsint32 PdfRaw[5];
}PdfBufInfo_t;

typedef struct
{
    Salsint32 KPdtOffsEolReadVal;
    Salsint32 KPdfOffsEolReadVal;
    Salsint32 KPdtOffsDrvgReadVal;
    Salsint32 KPdfOffsDrvgReadVal;
    Salsint32 KPedlSimPOffsEolReadVal;
    Salsint32 KPedlSimPOffsDrvgReadVal;
    Salsint32 KPistPOffsEolReadVal;
    Salsint32 KPistPOffsDrvgReadVal;
    Salsint32 KPrimCircPOffsEolReadVal;
    Salsint32 KPrimCircPOffsDrvgReadVal;
    Salsint32 KSecdCircPOffsEolReadVal;
    Salsint32 KSecdCircPOffsDrvgReadVal;
    Salsint32 KSteerEepOffs;
    Salsint32 KYawEepOffs;
    Salsint32 KLatEepOffs;
    Salsint32 KFsYawEepOffs;
    Salsint32 KYawEepMax;
    Salsint32 KYawEepMin;
    Salsint32 KSteerEepMax;
    Salsint32 KSteerEepMin;
    Salsint32 KLatEepMax;
    Salsint32 KLatEepMin;
    Salsint32 KYawStillEepMax;
    Salsint32 KYawStillEepMin;
    Salsint32 KYawStandEepOffs;
    Salsint32 KYawTmpEepMap[31];
    Salsint32 KAdpvVehMdlVchEep;
    Salsint32 KAdpvVehMdlVcrtRatEep;
    Salsint32 KFlBtcTmpEep;
    Salsint32 KFrBtcTmpEep;
    Salsint32 KRlBtcTmpEep;
    Salsint32 KRrBtcTmpEep;
    Salsint32 KEeBtcsDataEep;
    Salsint32 KLgtSnsrEolEepOffs;
    Salsint32 KLgtSnsrDrvgEepOffs;
    Salsint32 ReadInvld;
    Salsint32 WrReqCmpld;
}LogicEepDataInfo_t;

typedef struct
{
    Saluint16 MAI_MTPVoltage;
}MotorAbsInputEsc_t;

typedef struct
{
    Saluint8 YawRateInvld;
    Saluint8 AyInvld;
    Saluint8 SteeringAngleRxOk;
    Saluint8 AxInvldData;
    Saluint8 Ems1RxErr;
    Saluint8 Ems2RxErr;
    Saluint8 Tcu1RxErr;
    Saluint8 Tcu5RxErr;
    Saluint8 Hcu1RxErr;
    Saluint8 Hcu2RxErr;
    Saluint8 Hcu3RxErr;
}CanRxEemInfo_t;

typedef struct
{
    Saluint8 FsrAbsOff;
    Saluint8 FsrAbsDrv;
}FsrAbsDrvInfo_t;

typedef struct
{
    Saluint8 FsrCbsOff;
    Saluint8 FsrCbsDrv;
}FsrCbsDrvInfo_t;

typedef struct
{
    Saluint16 MotPwmPhUhighData;
    Saluint16 MotPwmPhVhighData;
    Saluint16 MotPwmPhWhighData;
    Saluint16 MotPwmPhUlowData;
    Saluint16 MotPwmPhVlowData;
    Saluint16 MotPwmPhWlowData;
}MotPwmDataInfo_t;

typedef struct
{
    Salsint16 PrimCircPRaw[5];
    Salsint16 SecdCircPRaw[5];
}CircPBufInfo_t;

typedef struct
{
    Salsint16 PistPRaw[5];
}PistPBufInfo_t;

typedef struct
{
    Salsint16 PedlSimPRaw[5];
}PspBufInfo_t;

typedef struct
{
    Saluint16 FlOvReqData[5];
    Saluint16 FlIvReqData[5];
    Saluint16 FrOvReqData[5];
    Saluint16 FrIvReqData[5];
    Saluint16 RlOvReqData[5];
    Saluint16 RlIvReqData[5];
    Saluint16 RrOvReqData[5];
    Saluint16 RrIvReqData[5];
}WhlVlvReqInfo_t;

typedef struct
{
    Saluint16 PrimBalVlvReqData[5];
    Saluint16 PressDumpVlvReqData[5];
    Saluint16 ChmbBalVlvReqData[5];
}BalVlvReqInfo_t;

typedef struct
{
    Salsint32 DiagSasCaltoAppl;
}SasCalInfo_t;

typedef struct
{
    Salsint32 FlexBrkSwtFaultDet;
}SwtStsFSInfo_t;

typedef struct
{
    Salsint32 MaiCanSusDet;
    Salsint32 EmsTiOutSusDet;
    Salsint32 TcuTiOutSusDet;
}CanTimeOutStInfo_t;

typedef struct
{
    Salsint32 PedlTrvlOffsEolCalcReqFlg;
    Salsint32 PSnsrOffsEolCalcReqFlg;
}IdbSnsrOffsEolCalcReqByDiagInfo_t;

typedef struct
{
    Saluint8 MainBusOffFlag;
    Saluint8 SenBusOffFlag;
}CanRxInfo_t;

typedef struct
{
    Saluint8 Bms1SocPc;
}RxBms1Info_t;

typedef struct
{
    Saluint8 YawSerialNum_0;
    Saluint8 YawSerialNum_1;
    Saluint8 YawSerialNum_2;
}RxYawSerialInfo_t;

typedef struct
{
    Saluint8 YawRateValidData;
    Saluint8 YawRateSelfTestStatus;
    Saluint8 YawRateSignal_0;
    Saluint8 YawRateSignal_1;
    Saluint8 SensorOscFreqDev;
    Saluint8 Gyro_Fail;
    Saluint8 Raster_Fail;
    Saluint8 Eep_Fail;
    Saluint8 Batt_Range_Err;
    Saluint8 Asic_Fail;
    Saluint8 Accel_Fail;
    Saluint8 Ram_Fail;
    Saluint8 Rom_Fail;
    Saluint8 Ad_Fail;
    Saluint8 Osc_Fail;
    Saluint8 Watchdog_Rst;
    Saluint8 Plaus_Err_Pst;
    Saluint8 RollingCounter;
    Saluint8 Can_Func_Err;
    Saluint8 AccelEratorRateSig_0;
    Saluint8 AccelEratorRateSig_1;
    Saluint8 LatAccValidData;
    Saluint8 LatAccSelfTestStatus;
}RxYawAccInfo_t;

typedef struct
{
    Saluint8 ShiftClass_Ccan;
}RxTcu6Info_t;

typedef struct
{
    Saluint8 Typ;
    Saluint8 GearTyp;
}RxTcu5Info_t;

typedef struct
{
    Saluint8 Targe;
    Saluint8 GarChange;
    Saluint8 Flt;
    Saluint8 GarSelDisp;
    Saluint16 TQRedReq_PC;
    Saluint16 TQRedReqSlw_PC;
    Saluint8 TQIncReq_PC;
}RxTcu1Info_t;

typedef struct
{
    Saluint16 Angle;
    Saluint8 Speed;
    Saluint8 Ok;
    Saluint8 Cal;
    Saluint8 Trim;
    Saluint8 CheckSum;
    Saluint8 MsgCount;
}RxSasInfo_t;

typedef struct
{
    Saluint8 Flt;
}RxMcu2Info_t;

typedef struct
{
    Saluint16 MoTestTQ_PC;
    Saluint16 MotActRotSpd_RPM;
}RxMcu1Info_t;

typedef struct
{
    Saluint8 IntSenFltSymtmActive;
    Saluint8 IntSenFaultPresent;
    Saluint8 LongAccSenCirErrPre;
    Saluint8 LonACSenRanChkErrPre;
    Saluint8 LongRollingCounter;
    Saluint8 IntTempSensorFault;
    Saluint8 LongAccInvalidData;
    Saluint8 LongAccSelfTstStatus;
    Saluint8 LongAccRateSignal_0;
    Saluint8 LongAccRateSignal_1;
}RxLongAccInfo_t;

typedef struct
{
    Saluint8 HevMod;
}RxHcu5Info_t;

typedef struct
{
    Saluint16 TmIntQcMDBINV_PC;
    Saluint16 MotTQCMC_PC;
    Saluint16 MotTQCMDBINV_PC;
}RxHcu3Info_t;

typedef struct
{
    Saluint8 ServiceMod;
    Saluint8 RegenENA;
    Saluint16 RegenBRKTQ_NM;
    Saluint16 CrpTQ_NM;
    Saluint16 WhlDEMTQ_NM;
}RxHcu2Info_t;

typedef struct
{
    Saluint8 EngCltStat;
    Saluint8 HEVRDY;
    Saluint8 EngTQCmdBinV_PC;
    Saluint8 EngTQCmd_PC;
}RxHcu1Info_t;

typedef struct
{
    Saluint8 OutTemp_SNR_C;
}RxFact1Info_t;

typedef struct
{
    Saluint8 EngColTemp_C;
}RxEms3Info_t;

typedef struct
{
    Saluint8 EngSpdErr;
    Saluint8 AccPedDep_PC;
    Saluint8 Tps_PC;
}RxEms2Info_t;

typedef struct
{
    Saluint8 TqStd_NM;
    Saluint8 ActINDTQ_PC;
    Saluint16 EngSpd_RPM;
    Saluint8 IndTQ_PC;
    Saluint8 FrictTQ_PC;
}RxEms1Info_t;

typedef struct
{
    Saluint8 IGN_SW;
}RxClu2Info_t;

typedef struct
{
    Saluint8 P_Brake_Act;
    Saluint8 Cf_Clu_BrakeFluIDSW;
}RxClu1Info_t;

typedef struct
{
    Saluint8 Bms1MsgOkFlg;
    Saluint8 YawSerialMsgOkFlg;
    Saluint8 YawAccMsgOkFlg;
    Saluint8 Tcu6MsgOkFlg;
    Saluint8 Tcu5MsgOkFlg;
    Saluint8 Tcu1MsgOkFlg;
    Saluint8 SasMsgOkFlg;
    Saluint8 Mcu2MsgOkFlg;
    Saluint8 Mcu1MsgOkFlg;
    Saluint8 LongAccMsgOkFlg;
    Saluint8 Hcu5MsgOkFlg;
    Saluint8 Hcu3MsgOkFlg;
    Saluint8 Hcu2MsgOkFlg;
    Saluint8 Hcu1MsgOkFlg;
    Saluint8 Fatc1MsgOkFlg;
    Saluint8 Ems3MsgOkFlg;
    Saluint8 Ems2MsgOkFlg;
    Saluint8 Ems1MsgOkFlg;
    Saluint8 Clu2MsgOkFlg;
    Saluint8 Clu1MsgOkFlg;
}RxMsgOkFlgInfo_t;

typedef struct
{
    Salsint32 EstimatedYaw;
    Salsint32 EstimatedAY;
    Salsint32 A_long_1_1000g;
}TxESCSensorInfo_t;

typedef struct
{
    Saluint32 TestYawRateSensor;
    Saluint32 TestAcclSensor;
    Saluint32 YawSerialNumberReq;
}TxYawCbitInfo_t;

typedef struct
{
    Saluint32 WhlSpdFL;
    Saluint32 WhlSpdFR;
    Saluint32 WhlSpdRL;
    Saluint32 WhlSpdRR;
}TxWhlSpdInfo_t;

typedef struct
{
    Saluint32 WhlPulFL;
    Saluint32 WhlPulFR;
    Saluint32 WhlPulRL;
    Saluint32 WhlPulRR;
}TxWhlPulInfo_t;

typedef struct
{
    Saluint32 CfBrkAbsWLMP;
    Saluint32 CfBrkEbdWLMP;
    Saluint32 CfBrkTcsWLMP;
    Saluint32 CfBrkTcsFLMP;
    Saluint32 CfBrkAbsDIAG;
    Saluint32 CrBrkWheelFL_KMH;
    Saluint32 CrBrkWheelFR_KMH;
    Saluint32 CrBrkWheelRL_KMH;
    Saluint32 CrBrkWheelRR_KMH;
}TxTcs5Info_t;

typedef struct
{
    Saluint32 CfBrkTcsREQ;
    Saluint32 CfBrkTcsPAS;
    Saluint32 CfBrkTcsDEF;
    Saluint32 CfBrkTcsCTL;
    Saluint32 CfBrkAbsACT;
    Saluint32 CfBrkAbsDEF;
    Saluint32 CfBrkEbdDEF;
    Saluint32 CfBrkTcsGSC;
    Saluint32 CfBrkEspPAS;
    Saluint32 CfBrkEspDEF;
    Saluint32 CfBrkEspCTL;
    Saluint32 CfBrkMsrREQ;
    Saluint32 CfBrkTcsMFRN;
    Saluint32 CfBrkMinGEAR;
    Saluint32 CfBrkMaxGEAR;
    Saluint32 BrakeLight;
    Saluint32 HacCtl;
    Saluint32 HacPas;
    Saluint32 HacDef;
    Saluint32 CrBrkTQI_PC;
    Saluint32 CrBrkTQIMSR_PC;
    Saluint32 CrBrkTQISLW_PC;
    Saluint32 CrEbsMSGCHKSUM;
}TxTcs1Info_t;

typedef struct
{
    Saluint32 CalSas_CCW;
    Saluint32 CalSas_Lws_CID;
}TxSasCalInfo_t;

typedef struct
{
    Saluint32 LatAccel;
    Saluint32 LatAccelStat;
    Saluint32 LatAccelDiag;
    Saluint32 LongAccel;
    Saluint32 LongAccelStat;
    Saluint32 LongAccelDiag;
    Saluint32 YawRate;
    Saluint32 YawRateStat;
    Saluint32 YawRateDiag;
    Saluint32 CylPres;
    Saluint32 CylPresStat;
    Saluint32 CylPresDiag;
}TxEsp2Info_t;

typedef struct
{
    Saluint32 CfBrkEbsStat;
    Saluint32 CrBrkRegenTQLimit_NM;
    Saluint32 CrBrkEstTot_NM;
    Saluint32 CfBrkSnsFail;
    Saluint32 CfBrkRbsWLamp;
    Saluint32 CfBrkVacuumSysDef;
    Saluint32 CrBrkEstHyd_NM;
    Saluint32 CrBrkStkDep_PC;
    Saluint8 CfBrkPgmRun1;
}TxEbs1Info_t;

typedef struct
{
    Saluint32 CfAhbWLMP;
    Saluint32 CfAhbDiag;
    Saluint32 CfAhbAct;
    Saluint32 CfAhbDef;
    Saluint32 CfAhbSLMP;
    Saluint32 CfBrkAhbSTDep_PC;
    Saluint32 CfBrkBuzzR;
    Saluint32 CfBrkPedalCalStatus;
    Saluint32 CfBrkAhbSnsFail;
    Saluint32 WhlSpdFLAhb;
    Saluint32 WhlSpdFRAhb;
    Saluint32 CrAhbMsgChkSum;
}TxAhb1Info_t;

typedef struct
{
    Haluint8 KeepAliveDataFlg;
    Haluint8 ACH_Tx1_PHOLD;
    Haluint8 ACH_Tx1_KA;
}AkaKeepAliveWriteData_t;

typedef struct
{
    Saluint8 MotorDutyDataFlg;
    Saluint16 MotorDuty;
}DcMtrDutyData_t;

typedef struct
{
    Saluint8 MotorFreqDataFlg;
    Saluint16 MotorFreq;
}DcMtrFreqData_t;

typedef struct
{
    Saluint8 RlyM_HDCRelay_Open_Err;
    Saluint8 RlyM_HDCRelay_Short_Err;
    Saluint8 RlyM_ESSRelay_Open_Err;
    Saluint8 RlyM_ESSRelay_Short_Err;
}RelayMonitor_t;

typedef struct
{
    Saluint8 SenPwrM_5V_Stable;
    Saluint8 SenPwrM_12V_Stable;
    Saluint8 SenPwrM_5V_DriveReq;
    Saluint8 SenPwrM_12V_Drive_Req;
    Saluint8 SenPwrM_5V_Err;
    Saluint8 SenPwrM_12V_Open_Err;
    Saluint8 SenPwrM_12V_Short_Err;
}SenPwrMonitor_t;

typedef struct
{
    Saluint8 Eem_Suspect_WssFL;
    Saluint8 Eem_Suspect_WssFR;
    Saluint8 Eem_Suspect_WssRL;
    Saluint8 Eem_Suspect_WssRR;
    Saluint8 Eem_Suspect_SameSideWss;
    Saluint8 Eem_Suspect_DiagonalWss;
    Saluint8 Eem_Suspect_FrontWss;
    Saluint8 Eem_Suspect_RearWss;
    Saluint8 Eem_Suspect_BBSSol;
    Saluint8 Eem_Suspect_ESCSol;
    Saluint8 Eem_Suspect_FrontSol;
    Saluint8 Eem_Suspect_RearSol;
    Saluint8 Eem_Suspect_Motor;
    Saluint8 Eem_Suspect_MPS;
    Saluint8 Eem_Suspect_MGD;
    Saluint8 Eem_Suspect_BBSValveRelay;
    Saluint8 Eem_Suspect_ESCValveRelay;
    Saluint8 Eem_Suspect_ECUHw;
    Saluint8 Eem_Suspect_ASIC;
    Saluint8 Eem_Suspect_OverVolt;
    Saluint8 Eem_Suspect_UnderVolt;
    Saluint8 Eem_Suspect_LowVolt;
    Saluint8 Eem_Suspect_SenPwr_12V;
    Saluint8 Eem_Suspect_SenPwr_5V;
    Saluint8 Eem_Suspect_Yaw;
    Saluint8 Eem_Suspect_Ay;
    Saluint8 Eem_Suspect_Ax;
    Saluint8 Eem_Suspect_Str;
    Saluint8 Eem_Suspect_CirP1;
    Saluint8 Eem_Suspect_CirP2;
    Saluint8 Eem_Suspect_SimP;
    Saluint8 Eem_Suspect_BS;
    Saluint8 Eem_Suspect_BLS;
    Saluint8 Eem_Suspect_ESCSw;
    Saluint8 Eem_Suspect_HDCSw;
    Saluint8 Eem_Suspect_AVHSw;
    Saluint8 Eem_Suspect_BrakeLampRelay;
    Saluint8 Eem_Suspect_EssRelay;
    Saluint8 Eem_Suspect_GearR;
    Saluint8 Eem_Suspect_Clutch;
    Saluint8 Eem_Suspect_ParkBrake;
    Saluint8 Eem_Suspect_PedalPDT;
    Saluint8 Eem_Suspect_PedalPDF;
    Saluint8 Eem_Suspect_BrakeFluid;
    Saluint8 Eem_Suspect_TCSTemp;
    Saluint8 Eem_Suspect_HDCTemp;
    Saluint8 Eem_Suspect_SCCTemp;
    Saluint8 Eem_Suspect_TVBBTemp;
    Saluint8 Eem_Suspect_MainCanLine;
    Saluint8 Eem_Suspect_SubCanLine;
    Saluint8 Eem_Suspect_EMSTimeOut;
    Saluint8 Eem_Suspect_FWDTimeOut;
    Saluint8 Eem_Suspect_TCUTimeOut;
    Saluint8 Eem_Suspect_HCUTimeOut;
    Saluint8 Eem_Suspect_MCUTimeOut;
    Saluint8 Eem_Suspect_VariantCoding;
}EemSuspectData_t;

typedef struct
{
    Saluint8 Eem_Ece_Wss;
    Saluint8 Eem_Ece_Yaw;
    Saluint8 Eem_Ece_Ay;
    Saluint8 Eem_Ece_Ax;
    Saluint8 Eem_Ece_Cir1P;
    Saluint8 Eem_Ece_Cir2P;
    Saluint8 Eem_Ece_SimP;
    Saluint8 Eem_Ece_Bls;
    Saluint8 Eem_Ece_Pedal;
    Saluint8 Eem_Ece_Motor;
    Saluint8 Eem_Ece_Vdc_Sw;
    Saluint8 Eem_Ece_Hdc_Sw;
    Saluint8 Eem_Ece_GearR_Sw;
    Saluint8 Eem_Ece_Clutch_Sw;
}EemEceData_t;

typedef struct
{
    Saluint8 Eem_Lamp_EBDLampRequest;
    Saluint8 Eem_Lamp_ABSLampRequest;
    Saluint8 Eem_Lamp_TCSLampRequest;
    Saluint8 Eem_Lamp_TCSOffLampRequest;
    Saluint8 Eem_Lamp_VDCLampRequest;
    Saluint8 Eem_Lamp_VDCOffLampRequest;
    Saluint8 Eem_Lamp_HDCLampRequest;
    Saluint8 Eem_Lamp_BBSBuzzorRequest;
    Saluint8 Eem_Lamp_RBCSLampRequest;
    Saluint8 Eem_Lamp_AHBLampRequest;
    Saluint8 Eem_Lamp_ServiceLampRequest;
    Saluint8 Eem_Lamp_TCSFuncLampRequest;
    Saluint8 Eem_Lamp_VDCFuncLampRequest;
    Saluint8 Eem_Lamp_AVHLampRequest;
    Saluint8 Eem_Lamp_AVHILampRequest;
    Saluint8 Eem_Lamp_ACCEnable;
    Saluint8 Eem_Lamp_CDMEnable;
    Saluint8 Eem_Buzzor_On;
}EemLampData_t;

typedef struct
{
    Saluint8 PedalM_PTS1_Open_Err;
    Saluint8 PedalM_PTS1_Short_Err;
    Saluint8 PedalM_PTS2_Open_Err;
    Saluint8 PedalM_PTS2_Short_Err;
    Saluint8 PedalM_PTS1_SupplyPower_Err;
    Saluint8 PedalM_PTS2_SupplyPower_Err;
    Saluint8 PedalM_VDD3_OverVolt_Err;
    Saluint8 PedalM_VDD3_UnderVolt_Err;
    Saluint8 PedalM_VDD3_OverCurrent_Err;
    Saluint8 PedalM_PTS1_Check_Ihb;
    Saluint8 PedalM_PTS2_Check_Ihb;
}PedalMData_t;

typedef struct
{
    Saluint8 CIRP1_FC_Fault_Err;
    Saluint8 CIRP1_FC_Mismatch_Err;
    Saluint8 CIRP1_FC_CRC_Err;
    Saluint8 CIRP1_SC_Temp_Err;
    Saluint8 CIRP1_SC_Mismatch_Err;
    Saluint8 CIRP1_SC_CRC_Err;
    Saluint16 CIRP1_SC_Diag_Info;
    Saluint8 CIRP2_FC_Fault_Err;
    Saluint8 CIRP2_FC_Mismatch_Err;
    Saluint8 CIRP2_FC_CRC_Err;
    Saluint8 CIRP2_SC_Temp_Err;
    Saluint8 CIRP2_SC_Mismatch_Err;
    Saluint8 CIRP2_SC_CRC_Err;
    Saluint16 CIRP2_SC_Diag_Info;
    Saluint8 SIMP_FC_Fault_Err;
    Saluint8 SIMP_FC_Mismatch_Err;
    Saluint8 SIMP_FC_CRC_Err;
    Saluint8 SIMP_SC_Temp_Err;
    Saluint8 SIMP_SC_Mismatch_Err;
    Saluint8 SIMP_SC_CRC_Err;
    Saluint16 SIMP_SC_Diag_Info;
}PresMonFaultInfo_t;

typedef struct
{
    Saluint8 ASICM_AsicOverVolt_Err;
    Saluint8 ASICM_AsicOverVolt_Err_Info;
    Saluint8 ASICM_AsicUnderVolt_Err;
    Saluint8 ASICM_AsicUnderVolt_Err_Info;
    Saluint8 ASICM_AsicOverTemp_Err;
    Saluint8 ASICM_AsicOverTemp_Err_Info;
    Saluint8 ASICM_AsicComm_Err;
    Saluint8 ASICM_AsicOsc_Err;
    Saluint8 ASICM_AsicNvm_Err;
    Saluint8 ASICM_AsicGroundLoss_Err;
}AsicMonFaultInfo_t;

typedef struct
{
    Saluint8 SWM_ESCOFF_Sw_Err;
    Saluint8 SWM_HDC_Sw_Err;
    Saluint8 SWM_AVH_Sw_Err;
    Saluint8 SWM_BFL_Sw_Err;
    Saluint8 SWM_PB_Sw_Err;
}SwtMonFaultInfo_t;

typedef struct
{
    Saluint8 SWM_BLS_Sw_HighStick_Err;
    Saluint8 SWM_BLS_Sw_LowStick_Err;
    Saluint8 SWM_BS_Sw_HighStick_Err;
    Saluint8 SWM_BS_Sw_LowStick_Err;
}SwtPFaultInfo_t;

typedef struct
{
    Saluint8 YAWM_SerialNumOK_Flg;
    Saluint8 YAWM_SerialUnMatch_Flg;
}YAWMSerialInfo_t;

typedef struct
{
    Saluint8 YAWM_Timeout_Err;
    Saluint8 YAWM_Invalid_Err;
    Saluint8 YAWM_CRC_Err;
    Saluint8 YAWM_Rolling_Err;
    Saluint8 YAWM_Temperature_Err;
    Saluint8 YAWM_Range_Err;
}YAWMOutInfo_t;

typedef struct
{
    Saluint8 AYM_Timeout_Err;
    Saluint8 AYM_Invalid_Err;
    Saluint8 AYM_CRC_Err;
    Saluint8 AYM_Rolling_Err;
    Saluint8 AYM_Temperature_Err;
    Saluint8 AYM_Range_Err;
}AYMOutInfo_t;

typedef struct
{
    Saluint8 AXM_Timeout_Err;
    Saluint8 AXM_Invalid_Err;
    Saluint8 AXM_CRC_Err;
    Saluint8 AXM_Rolling_Err;
    Saluint8 AXM_Temperature_Err;
    Saluint8 AXM_Range_Err;
}AXMOutInfo_t;

typedef struct
{
    Saluint8 SASM_Timeout_Err;
    Saluint8 SASM_Invalid_Err;
    Saluint8 SASM_CRC_Err;
    Saluint8 SASM_Rolling_Err;
    Saluint8 SASM_Range_Err;
}SASMOutInfo_t;

typedef struct
{
    Saluint8 MTRM_Power_Open_Err;
    Saluint8 MTRM_Open_Err;
    Saluint8 MTRM_Phase_U_OverCurret_Err;
    Saluint8 MTRM_Phase_V_OverCurret_Err;
    Saluint8 MTRM_Phase_W_OverCurret_Err;
    Saluint8 MTRM_Calibration_Err;
    Saluint8 MTRM_Short_Err;
    Saluint8 MTRM_Driver_Err;
}MTRMOutInfo_t;

typedef struct
{
    Saluint8 SysPwrM_OverVolt_1st_Err;
    Saluint8 SysPwrM_OverVolt_2st_Err;
    Saluint8 SysPwrM_UnderVolt_1st_Err;
    Saluint8 SysPwrM_UnderVolt_2st_Err;
    Saluint8 SysPwrM_UnderVolt_3st_Err;
    Saluint8 SysPwrM_Asic_Vdd_Err;
    Saluint8 SysPwrM_Asic_OverVolt_Err;
    Saluint8 SysPwrM_Asic_UnderVolt_Err;
    Saluint8 SysPwrM_Asic_OverTemp_Err;
    Saluint8 SysPwrM_Asic_Gndloss_Err;
    Saluint8 SysPwrM_Asic_Comm_Err;
    Saluint8 SysPwrM_IgnLine_Open_Err;
}SysPwrMonData_t;

typedef struct
{
    Saluint8 WssM_FlOpen_Err;
    Saluint8 WssM_FlShort_Err;
    Saluint8 WssM_FlOverTemp_Err;
    Saluint8 WssM_FlLeakage_Err;
    Saluint8 WssM_FrOpen_Err;
    Saluint8 WssM_FrShort_Err;
    Saluint8 WssM_FrOverTemp_Err;
    Saluint8 WssM_FrLeakage_Err;
    Saluint8 WssM_RlOpen_Err;
    Saluint8 WssM_RlShort_Err;
    Saluint8 WssM_RlOverTemp_Err;
    Saluint8 WssM_RlLeakage_Err;
    Saluint8 WssM_RrOpen_Err;
    Saluint8 WssM_RrShort_Err;
    Saluint8 WssM_RrOverTemp_Err;
    Saluint8 WssM_RrLeakage_Err;
    Saluint8 WssM_Asic_Comm_Err;
    Saluint8 WssM_Inhibit_FL;
    Saluint8 WssM_Inhibit_FR;
    Saluint8 WssM_Inhibit_RL;
    Saluint8 WssM_Inhibit_RR;
}WssMonData_t;

typedef struct
{
    Saluint8 CanM_MainBusOff_Err;
    Saluint8 CanM_SubBusOff_Err;
    Saluint8 CanM_MainOverRun_Err;
    Saluint8 CanM_SubOverRun_Err;
    Saluint8 CanM_EMS1_Tout_Err;
    Saluint8 CanM_EMS2_Tout_Err;
    Saluint8 CanM_TCU1_Tout_Err;
    Saluint8 CanM_TCU5_Tout_Err;
    Saluint8 CanM_FWD1_Tout_Err;
    Saluint8 CanM_HCU1_Tout_Err;
    Saluint8 CanM_HCU2_Tout_Err;
    Saluint8 CanM_HCU3_Tout_Err;
    Saluint8 CanM_VSM2_Tout_Err;
    Saluint8 CanM_EMS_Invalid_Err;
    Saluint8 CanM_TCU_Invalid_Err;
}CanMonData_t;

typedef struct
{
    Saluint8 MgdInterPwrSuppMonErr;
    Saluint8 MgdClkMonErr;
    Saluint8 MgdBISTErr;
    Saluint8 MgdFlashMemoryErr;
    Saluint8 MgdRAMErr;
    Saluint8 MgdConfigRegiErr;
    Saluint8 MgdInputPattMonErr;
    Saluint8 MgdOverTempErr;
    Saluint8 MgdCPmpVMonErr;
    Saluint8 MgdHBuffCapVErr;
    Saluint8 MgdInOutPlauErr;
    Saluint8 MgdCtrlSigMonErr;
}MgdErrInfo_t;

typedef struct
{
    Saluint8 ExtPWRSuppMonErr;
    Saluint8 IntPWRSuppMonErr;
    Saluint8 WatchDogErr;
    Saluint8 FlashMemoryErr;
    Saluint8 StartUpTestErr;
    Saluint8 ConfigRegiTestErr;
    Saluint8 HWIntegConsisErr;
    Saluint8 MagnetLossErr;
}MpsD1ErrInfo_t;

typedef struct
{
    Saluint8 ExtPWRSuppMonErr;
    Saluint8 IntPWRSuppMonErr;
    Saluint8 WatchDogErr;
    Saluint8 FlashMemoryErr;
    Saluint8 StartUpTestErr;
    Saluint8 ConfigRegiTestErr;
    Saluint8 HWIntegConsisErr;
    Saluint8 MagnetLossErr;
}MpsD2ErrInfo_t;

typedef struct
{
    Saluint8 WssP_AbsLongTermErr;
    Saluint8 WssP_AbsLongTerm_Ihb;
    Saluint8 WssP_FL_PhaseErr;
    Saluint8 WssP_FL_PhaseErr_Ihb;
    Saluint8 WssP_FR_PhaseErr;
    Saluint8 WssP_FR_PhaseErr_Ihb;
    Saluint8 WssP_RL_PhaseErr;
    Saluint8 WssP_RL_PhaseErr_Ihb;
    Saluint8 WssP_RR_PhaseErr;
    Saluint8 WssP_RR_PhaseErr_Ihb;
    Saluint8 WssP_FL_ExciterErr;
    Saluint8 WssP_FL_ExciterErr_Ihb;
    Saluint8 WssP_FR_ExciterErr;
    Saluint8 WssP_FR_ExciterErr_Ihb;
    Saluint8 WssP_RL_ExciterErr;
    Saluint8 WssP_RL_ExciterErr_Ihb;
    Saluint8 WssP_RR_ExciterErr;
    Saluint8 WssP_RR_ExciterErr_Ihb;
    Saluint8 WssP_FL_AirGapErr;
    Saluint8 WssP_FL_AirGapErr_Ihb;
    Saluint8 WssP_FR_AirGapErr;
    Saluint8 WssP_FR_AirGapErr_Ihb;
    Saluint8 WssP_RL_AirGapErr;
    Saluint8 WssP_RL_AirGapErr_Ihb;
    Saluint8 WssP_RR_AirGapErr;
    Saluint8 WssP_RR_AirGapErr_Ihb;
    Saluint8 WssP_FL_PJumpErr;
    Saluint8 WssP_FL_PJumpErr_Ihb;
    Saluint8 WssP_FR_PJumpErr;
    Saluint8 WssP_FR_PJumpErr_Ihb;
    Saluint8 WssP_RL_PJumpErr;
    Saluint8 WssP_RL_PJumpErr_Ihb;
    Saluint8 WssP_RR_PJumpErr;
    Saluint8 WssP_RR_PJumpErr_Ihb;
    Saluint8 WssP_FL_MJumpErr;
    Saluint8 WssP_FL_MJumpErr_Ihb;
    Saluint8 WssP_FR_MJumpErr;
    Saluint8 WssP_FR_MJumpErr_Ihb;
    Saluint8 WssP_RL_MJumpErr;
    Saluint8 WssP_RL_MJumpErr_Ihb;
    Saluint8 WssP_RR_MJumpErr;
    Saluint8 WssP_RR_MJumpErr_Ihb;
    Saluint8 WssP_FL_WheelTypeSwapErr;
    Saluint8 WssP_FL_WheelTypeSwapErr_Ihb;
    Saluint8 WssP_FR_WheelTypeSwapErr;
    Saluint8 WssP_FR_WheelTypeSwapErr_Ihb;
    Saluint8 WssP_RL_WheelTypeSwapErr;
    Saluint8 WssP_RL_WheelTypeSwapErr_Ihb;
    Saluint8 WssP_RR_WheelTypeSwapErr;
    Saluint8 WssP_RR_WheelTypeSwapErr_Ihb;
    Saluint16 WssP_max_speed;
    Saluint16 WssP_2nd_speed;
    Saluint16 WssP_3rd_speed;
    Saluint16 WssP_min_speed;
}WssPlauData_t;

typedef struct
{
    Salsint32 IqSelected;
    Salsint32 IdSelected;
    Salsint32 UPhasePWMSelected;
    Salsint32 VPhasePWMSelected;
    Salsint32 WPhasePWMSelected;
    Salsint32 MtrCtrlMode;
}MtrArbitratorData_t;

typedef struct
{
    Saluint8 MtrArbCalMode;
    Saluint32 MtrArbState;
}MtrArbitratorInfo_t;

typedef struct
{
    Salsint32 MtrProIqFailsafe;
    Salsint32 MtrProIdFailsafe;
    Saluint32 MtrProUPhasePWM;
    Saluint32 MtrProVPhasePWM;
    Saluint32 MtrProWPhasePWM;
}MtrProcessOutData_t;

typedef struct
{
    Saluint8 MtrProCalibrationState;
    Saluint32 MtrCaliResult;
}MtrProcessDataInfo_t;

typedef struct
{
    Saluint8 DiagRequest_Order;
    Saluint16 DiagRequest_Iq;
    Saluint16 DiagRequest_Id;
    Saluint16 DiagRequest_U_PWM;
    Saluint16 DiagRequest_V_PWM;
    Saluint16 DiagRequest_W_PWM;
}DiagHndlRequest_t;

typedef struct
{
    Saluint8 Reverse_Gear_flg;
    Saluint8 Reverse_Judge_Time;
}IMUCalc_t;

typedef struct
{
    Saluint32 WssMax;
    Saluint32 WssMin;
}WssSpeedOut_t;

typedef struct
{
    Saluint8 Rough_Sus_Flg;
}WssCalc_t;

typedef struct
{
    Saluint8 YawPlauModelErr;
    Saluint8 YawPlauNoisekErr;
    Saluint8 YawPlauShockErr;
    Saluint8 YawPlauRangeErr;
    Saluint8 YawPlauStandStillErr;
}YawPlauOutput_t;

typedef struct
{
    Saluint8 AxPlauOffsetErr;
    Saluint8 AxPlauDrivingOffsetErr;
    Saluint8 AxPlauStickErr;
    Saluint8 AxPlauNoiseErr;
}AxPlauOutput_t;

typedef struct
{
    Saluint8 AyPlauNoiselErr;
    Saluint8 AyPlauModelErr;
    Saluint8 AyPlauShockErr;
    Saluint8 AyPlauRangeErr;
    Saluint8 AyPlauStandStillErr;
}AyPlauOutput_t;

typedef struct
{
    Saluint8 SasPlauModelErr;
    Saluint8 SasPlauOffsetErr;
    Saluint8 SasPlauStickErr;
}SasPlauOutput_t;

typedef struct
{
    Salsint32 MotCurrPhUMeasd;
    Salsint32 MotCurrPhVMeasd;
    Salsint32 MotCurrPhWMeasd;
    Saluint16 MotPosiAngle1raw;
    Saluint16 MotPosiAngle2raw;
    Salsint32 MotPosiAngle1_1_100Deg;
    Salsint32 MotPosiAngle2_1_100Deg;
}MtrProcessOutInfo_t;

typedef struct
{
    Saluint8 ST_ECU_ST_REPAT_XTRQ_FTAX_BAX;
    Saluint8 ALIV_ST_REPAT_XTRQ_FTAX_BAX;
    Saluint8 CRC_ST_REPAT_XTRQ_FTAX_BAX;
    Saluint16 QU_SER_REPAT_XTRQ_FTAX_BAX_ACT;
    Saluint16 AVL_REPAT_XTRQ_FTAX_BAX;
    Saluint8 QU_AVL_REPAT_XTRQ_FTAX_BAX;
}ST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t;

typedef struct
{
    Saluint16 QU_SER_LTRQD_BAX;
    Saluint8 CRC_AVL_LTRQD_BAX;
    Saluint8 QU_AVL_LTRQD_BAX;
    Saluint16 AVL_LTRQD_BAX;
    Saluint8 ALIV_AVL_LTRQD_BAX;
}AVL_LTRQD_BAXInfo_t;

typedef struct
{
    Saluint16 AVL_TORQ_CRSH_DMEE;
    Saluint16 AVL_TORQ_CRSH;
    Saluint8 ST_SAIL_DRV_2;
    Saluint8 CRC_ANG_ACPD;
    Saluint8 QU_AVL_RPM_ENG_CRSH;
    Saluint16 AVL_RPM_ENG_CRSH;
    Saluint8 ALIV_TORQ_CRSH_1;
    Saluint8 CRC_TORQ_CRSH_1;
    Saluint16 AVL_ANG_ACPD;
    Saluint8 QU_AVL_ANG_ACPD;
    Saluint8 ALIV_ANG_ACPD;
    Saluint8 ST_ECU_ANG_ACPD;
    Saluint16 AVL_ANG_ACPD_VIRT;
    Saluint8 ECO_ANG_ACPD;
    Saluint16 GRAD_AVL_ANG_ACPD;
    Saluint8 ST_INTF_DRASY;
}TORQ_CRSH_1_ANG_ACPDInfo_t;

typedef struct
{
    Saluint16 AVL_RPM_BAX_RED;
    Saluint8 ST_PENG_PT;
    Saluint8 ST_AVAI_INTV_PT_DRS;
    Saluint8 QU_AVL_RPM_BAX_RED;
    Saluint8 QU_SER_WMOM_PT_SUM_DRS;
    Saluint8 QU_SER_WMOM_PT_SUM_STAB;
    Saluint8 ST_ECU_WMOM_DRV_5;
    Saluint16 TAR_WMOM_PT_SUM_COOTD;
    Saluint8 QU_SER_COOR_TORQ_BDRV;
    Saluint8 ST_ECU_WMOM_DRV_4;
    Saluint8 ST_DRVDIR_DVCH;
    Saluint8 CRC_WMOM_DRV_4;
    Saluint8 ALIV_WMOM_DRV_4;
    Saluint8 ALIV_WMOM_DRV_5;
    Saluint8 CRC_WMOM_DRV_5;
}WMOM_DRV_5_WMOM_DRV_4Info_t;

typedef struct
{
    Saluint8 QU_AVL_WMOM_PT_SUM;
    Saluint16 AVL_WMOM_PT_SUM_ERR_AMP;
    Saluint16 REIN_PT;
    Saluint16 AVL_WMOM_PT_SUM;
    Saluint8 CRC_WMOM_DRV_1;
    Saluint8 ALIV_WMOM_DRV_1;
    Saluint8 ST_ECU_WMOM_DRV_1;
    Saluint16 AVL_WMOM_PT_SUM_MAX;
    Saluint16 AVL_WMOM_PT_SUM_FAST_TOP;
    Saluint16 AVL_WMOM_PT_SUM_FAST_BOT;
    Saluint8 ST_ECU_WMOM_DRV_2;
    Saluint8 QU_REIN_PT;
    Saluint8 CRC_WMOM_DRV_2;
    Saluint8 ALIV_WMOM_DRV_2;
}WMOM_DRV_1_WMOM_DRV_2Info_t;

typedef struct
{
    Saluint8 HGLV_VEH_FILT_FRH;
    Saluint8 HGLV_VEH_FILT_FLH;
    Saluint8 QU_HGLV_VEH_FILT_FRH;
    Saluint8 QU_HGLV_VEH_FILT_FLH;
    Saluint8 QU_HGLV_VEH_FILT_RRH;
    Saluint8 HGLV_VEH_FILT_RLH;
    Saluint8 ALIV_HGLV_VEH_FILT;
    Saluint8 HGLV_VEH_FILT_RRH;
    Saluint8 QU_HGLV_VEH_FILT_RLH;
    Saluint8 CRC_HGLV_VEH_FILT;
}HGLV_VEH_FILTInfo_t;

typedef struct
{
    Saluint16 ATTAV_ESTI;
    Saluint8 ATTA_ESTI_ERR_AMP;
    Saluint8 VY_ESTI_ERR_AMP;
    Saluint8 VY_ESTI;
    Saluint8 ATTAV_ESTI_ERR_AMP;
    Saluint8 CRC_VEH_DYNMC_DT_ESTI_VRFD;
    Saluint8 ALIV_VEH_DYNMC_DT_ESTI_VRFD;
    Saluint8 QU_VEH_DYNMC_DT_ESTI;
    Saluint16 ATTA_ESTI;
}VEH_DYNMC_DT_ESTI_VRFDInfo_t;

typedef struct
{
    Saluint8 QU_ACLNY_COG;
    Saluint16 ACLNY_COG_ERR_AMP;
    Saluint8 ALIV_ACLNX_COG;
    Saluint16 ACLNX_COG;
    Saluint8 ALIV_ACLNY_COG;
    Saluint8 CRC_ACLNX_COG;
    Saluint8 CRC_ACLNY_COG;
    Saluint16 ACLNY_COG;
    Saluint16 ACLNX_COG_ERR_AMP;
    Saluint8 QU_ACLNX_COG;
}ACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t;

typedef struct
{
    Saluint8 CRC_V_VEH;
    Saluint8 ALIV_V_VEH;
    Saluint8 QU_V_VEH_COG;
    Saluint8 DVCO_VEH;
    Saluint8 ST_V_VEH_NSS;
    Saluint16 V_VEH_COG;
}V_VEH_V_VEH_2Info_t;

typedef struct
{
    Saluint8 ALIV_VYAW_VEH;
    Saluint16 VYAW_VEH_ERR_AMP;
    Saluint8 CRC_VYAW_VEH;
    Saluint8 QU_VYAW_VEH;
    Saluint16 VYAW_VEH;
}DT_DRDYSEN_EXT_VYAW_VEHInfo_t;

typedef struct
{
    Saluint8 QU_AVL_TRGR_RW;
    Saluint16 AVL_TRGR_RW;
    Saluint16 AVL_LOGR_RW_FAST;
    Saluint8 ALIV_TLT_RW;
    Saluint8 CRC_TLT_RW;
    Saluint8 QU_AVL_LOGR_RW;
    Saluint16 AVL_LOGR_RW;
}TLT_RW_STEA_FTAX_EFFVInfo_t;

typedef struct
{
    Saluint8 QU_AVL_STEA_FTAX_PNI;
    Saluint16 AVL_STEA_FTAX_PNI;
    Saluint8 CRC_AVL_STEA_FTAX;
    Saluint8 QU_AVL_STEA_FTAX_WHL;
    Saluint8 ALIV_AVL_STEA_FTAX;
    Saluint16 AVL_STEA_FTAX_WHL;
    Saluint16 AVL_STEA_FTAX_WHL_ERR_AMP;
}AVL_STEA_FTAXInfo_t;

typedef struct
{
    Saluint8 TAR_PRMSN_DBC_DCRN_MAX;
    Saluint8 QU_TAR_PRF_BRK;
    Saluint8 QU_TAR_V_HDC;
    Saluint8 TAR_PRMSN_DBC_DCRN_GRAD_MAX;
    Saluint8 ALIV_SPEC_PRMSN_IBRK_HDC;
    Saluint8 CRC_SPEC_PRMSN_IBRK_HDC;
    Saluint8 TAR_PRMSN_DBC_TR_THRV;
    Saluint16 TAR_BRTORQ_SUM;
    Saluint8 QU_TAR_BRTORQ_SUM;
    Saluint8 QU_TAR_PRMSN_DBC;
    Saluint8 RQ_TAO_SSM;
    Saluint8 CRC_RQ_BRTORQ_SUM;
    Saluint8 ALIV_RQ_BRTORQ_SUM;
}SPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t;

typedef struct
{
    Saluint16 AVL_WMOM_PT_SUM_DTORQ_BOT;
    Saluint16 AVL_WMOM_PT_SUM_RECUP_MAX;
    Saluint16 AVL_WMOM_PT_SUM_ILS;
    Saluint8 CRC_WMOM_DRV_3;
    Saluint8 ALIV_WMOM_DRV_3;
    Saluint16 AVL_WMOM_PT_SUM_DTORQ_TOP;
    Saluint8 ST_ECU_WMOM_DRV_6;
    Saluint8 ALIV_WMOM_DRV_6;
    Saluint8 CRC_WMOM_DRV_6;
}WMOM_DRV_3_WMOM_DRV_6Info_t;

typedef struct
{
    Saluint8 ST_EL_DRVG;
    Saluint16 PRD_AVL_WMOM_PT_SUM_RECUP_MAX;
    Saluint8 ALIV_WMOM_DRV_7;
    Saluint8 CRC_WMOM_DRV_7;
    Saluint8 ST_ECU_WMOM_DRV_7;
    Saluint8 QU_SER_WMOM_PT_SUM_RECUP;
}WMOM_DRV_7Info_t;

typedef struct
{
    Saluint16 TAR_DIFF_BRTORQ_BAX_YMR;
    Saluint8 FACT_TAR_COMPT_DRV_YMR;
    Saluint8 QU_TAR_DIFF_BRTORQ_YMR;
    Saluint8 CRC_RQ_DIFF_BRTORQ_YMR;
    Saluint8 ALIV_RQ_DIFF_BRTORQ_YMR;
    Saluint16 TAR_DIFF_BRTORQ_FTAX_YMR;
}TAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t;

typedef struct
{
    Saluint8 QU_AVL_LOWP_BRKFA;
    Saluint8 AVL_LOWP_BRKFA;
    Saluint8 CRC_DT_BRKSYS_ENGMG;
    Saluint8 ALIV_DT_BRKSYS_ENGMG;
}DT_BRKSYS_ENGMGInfo_t;

typedef struct
{
    Saluint8 CTR_ERRM_BN_U;
}ERRM_BN_UInfo_t;

typedef struct
{
    Saluint8 ALIV_CTR_CR;
    Saluint8 ST_EXCE_ACLN_THRV;
    Saluint8 CTR_PHTR_CR;
    Saluint8 CTR_ITLI_CR;
    Saluint8 CTR_CLSY_CR;
    Saluint8 CTR_AUTOM_ECAL_CR;
    Saluint8 CTR_SWO_EKP_CR;
    Saluint8 CRC_CTR_CR;
    Saluint8 CTR_PCSH_MST;
    Saluint8 CTR_HAZW_CR;
}CTR_CRInfo_t;

typedef struct
{
    Saluint8 ST_OP_MSA;
    Saluint8 RQ_DRVG_RDI;
    Saluint8 ST_KL_DBG;
    Saluint8 ST_KL_50_MSA;
    Saluint8 ST_SSP;
    Saluint8 RWDT_BLS;
    Saluint8 ST_STCD_PENG;
    Saluint8 ST_KL;
    Saluint8 ST_KL_DIV;
    Saluint8 ST_VEH_CON;
    Saluint8 CRC_KL;
    Saluint8 ALIV_COU_KL;
    Saluint8 ST_KL_30B;
    Saluint8 CON_CLT_SW;
    Saluint8 CTR_ENG_STOP;
    Saluint8 ST_PLK;
    Saluint8 ST_KL_15N;
    Saluint8 ST_KL_KEY_VLD;
}KLEMMENInfo_t;

typedef struct
{
    Saluint16 RDC_MES_TSTMP;
    Saluint8 PCKG_ID;
    Saluint8 SUPP_ID;
    Saluint8 RDC_DT_5;
    Saluint8 RDC_DT_8;
    Saluint8 RDC_DT_7;
    Saluint8 RDC_DT_6;
    Saluint8 ALIV_RDC_DT_PCKG_2;
    Saluint8 RDC_DT_1;
    Saluint32 TYR_ID;
    Saluint8 ALIV_RDC_DT_PCKG_1;
    Saluint8 RDC_DT_4;
    Saluint8 RDC_DT_3;
    Saluint8 RDC_DT_2;
}RDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t;

typedef struct
{
    Saluint8 ALIV_WISW;
    Saluint8 OP_WISW;
    Saluint8 OP_WIPO;
}BEDIENUNG_WISCHERInfo_t;

typedef struct
{
    Saluint8 ST_GRSEL_DRV;
    Saluint8 TEMP_EOI_DRV;
    Saluint8 RLS_ENGSTA;
    Saluint8 RPM_ENG_MAX_ALW;
    Saluint8 RDUC_DOCTR_RPM_DRV_2;
    Saluint8 TEMP_ENG_DRV;
    Saluint8 ST_DRV_VEH;
    Saluint8 ST_ECU_DT_PT_2;
    Saluint8 ST_IDLG_ENG_DRV;
    Saluint8 ST_ILK_STRT_DRV;
    Saluint8 ST_SW_CLT_DRV;
    Saluint8 CRC_DT_PT_2;
    Saluint8 ALIV_DT_PT_2;
    Saluint8 ST_ENG_RUN_DRV;
}DT_PT_2Info_t;

typedef struct
{
    Saluint8 RQ_MSA_ENG_STA_1;
    Saluint8 RQ_MSA_ENG_STA_2;
    Saluint8 ALIV_ST_ENG_STA_AUTO;
    Saluint8 PSBTY_MSA_ENG_STOP_STA;
    Saluint8 CRC_ST_ENG_STA_AUTO;
    Saluint8 ST_SHFT_MSA_ENGSTP;
    Saluint8 RQ_SLIP_K0;
    Saluint8 ST_TAR_PENG_CENG;
    Saluint8 SPEC_TYP_ENGSTA;
    Saluint8 ST_RPM_CLCTR_MOT;
    Saluint8 ST_FN_MSA;
    Saluint8 ST_AVL_PENG_CENG_ENGMG;
    Saluint8 DISP_REAS_PREV_SWO_CENG;
    Saluint8 VARI_TYP_ENGSTA;
    Saluint8 ST_TAR_CENG;
    Saluint8 ST_AVAI_SAIL_DME;
}STAT_ENG_STA_AUTOInfo_t;

typedef struct
{
    Saluint8 ST_LDST_GEN_DRV;
    Saluint8 DT_PCU_SCP;
    Saluint8 ST_GEN_DRV;
    Saluint8 AVL_I_GEN_DRV;
    Saluint8 LDST_GEN_DRV;
    Saluint8 ST_LDREL_GEN;
    Saluint8 ST_CHG_STOR;
    Saluint8 TEMP_BT_14V;
    Saluint16 ST_I_IBS;
    Saluint8 ST_SEP_STOR;
    Saluint8 ST_BN2_SCP;
}ST_ENERG_GENInfo_t;

typedef struct
{
    Saluint8 RQ_SHPA_GRB_REGE_PAFI;
    Saluint8 ST_RTIR_DRV;
    Saluint8 CTR_SLCK_DRV;
    Saluint8 RQ_STASS_ENGMG;
    Saluint8 ST_CAT_HT;
    Saluint8 RQ_SHPA_GRB_CHGBLC;
    Saluint8 TAR_RPM_IDLG_DRV_EXT;
    Saluint8 SLCTN_BUS_COMM_ENG_GRB;
    Saluint8 TAR_RPM_IDLG_DRV;
    Saluint8 ST_INFS_DRV;
    Saluint8 ST_SW_WAUP_DRV;
    Saluint8 AIP_ENG_DRV;
    Saluint8 RDUC_DOCTR_RPM_DRV;
}DT_PT_1Info_t;

typedef struct
{
    Saluint8 ST_OPMO_GRDT_DRV;
    Saluint8 ST_RSTA_GRDT;
    Saluint8 CRC_GRDT_DRV;
    Saluint8 ALIV_GRDT_DRV;
}DT_GRDT_DRVInfo_t;

typedef struct
{
    Saluint8 RQ_MIL_DIAG_OBD_ENGMG_EL;
    Saluint8 RQ_RST_OBD_DIAG;
}DIAG_OBD_ENGMG_ELInfo_t;

typedef struct
{
    Saluint8 ST_CT_HABR;
}STAT_CT_HABRInfo_t;

typedef struct
{
    Saluint16 TRNRAO_BAX;
    Saluint8 QU_TRNRAO_BAX;
    Saluint8 ALIV_DT_PT_3;
}DT_PT_3Info_t;

typedef struct
{
    Saluint8 UN_TORQ_S_MOD;
    Saluint8 UN_PWR_S_MOD;
    Saluint8 UN_DATE_EXT;
    Saluint8 UN_COSP_EL;
    Saluint8 UN_TEMP;
    Saluint8 UN_AIP;
    Saluint8 LANG;
    Saluint8 UN_DATE;
    Saluint8 UN_T;
    Saluint8 UN_SPDM_DGTL;
    Saluint8 UN_MILE;
    Saluint8 UN_FU;
    Saluint8 UN_COSP;
}EINHEITEN_BN2020Info_t;

typedef struct
{
    Saluint8 TEMP_EX_UNFILT;
    Saluint8 TEMP_EX;
}A_TEMPInfo_t;

typedef struct
{
    Saluint8 ST_RNSE;
    Saluint8 INT_RN;
    Saluint8 V_WI;
}WISCHERGESCHWINDIGKEITInfo_t;

typedef struct
{
    Saluint8 ST_DSW_DRD_SFY_CTRL;
    Saluint8 ST_DSW_DVDR_VRFD;
    Saluint8 ST_DSW_DVDR_SFY_CTRL;
    Saluint8 ST_DSW_PSDR_SFY_CTRL;
    Saluint8 ST_DSW_PSD_SFY_CTRL;
    Saluint8 ALIV_ST_DSW_VRFD;
    Saluint8 CRC_ST_DSW_VRFD;
    Saluint8 ST_DSW_DRD_VRFD;
    Saluint8 ST_DSW_PSDR_VRFD;
    Saluint8 ST_DSW_PSD_VRFD;
}STAT_DS_VRFDInfo_t;

typedef struct
{
    Saluint8 AVL_MOD_SW_DRDY;
    Saluint8 RQ_SW_DRDY_MMID;
    Saluint16 RQ_SU_SW_DRDY;
    Saluint8 CRC_SU_SW_DRDY;
    Saluint8 ALIV_SU_SW_DRDY;
    Saluint8 RQ_SW_DRDY_KDIS;
    Saluint8 AVL_MOD_SW_DRDY_CHAS;
    Saluint8 AVL_MOD_SW_DRDY_DRV;
    Saluint8 AVL_MOD_SW_DRDY_STAB;
    Saluint8 DISP_ST_DSC;
}SU_SW_DRDY_SU_SW_DRDY_2Info_t;

typedef struct
{
    Saluint8 ST_SYNCN_HAZWCL_TRAI;
    Saluint8 ST_RFLI_DF_TRAI;
    Saluint8 ST_TRAI;
    Saluint8 ST_DI_DF_TRAI;
    Saluint8 ST_PO_AHV;
}STAT_ANHAENGERInfo_t;

typedef struct
{
    Saluint8 ST_ILK_ERRM_FZM;
    Saluint8 ST_ENERG_FZM;
    Saluint8 ST_BT_PROTE_WUP;
}FZZSTDInfo_t;

typedef struct
{
    Saluint8 OP_MOD_TRCT_DSC;
    Saluint8 OP_TPCT;
}BEDIENUNG_FAHRWERKInfo_t;

typedef struct
{
    Saluint8 QUAN_CYL;
    Saluint8 QUAN_GR;
    Saluint8 TYP_VEH;
    Saluint8 TYP_BODY;
    Saluint8 TYP_ENG;
    Saluint8 CLAS_PWR;
    Saluint8 TYP_CNT;
    Saluint8 TYP_STE;
    Saluint8 TYP_GRB;
    Saluint8 TYP_CAPA;
}FAHRZEUGTYPInfo_t;

typedef struct
{
    Saluint8 ST_SEAT_OCCU_RRH;
    Saluint8 ST_BLTB_SW_RM;
    Saluint8 ST_BLTB_SW_RRH;
    Saluint8 ST_ERR_SEAT_MT_DR;
    Saluint8 ST_BLTB_SW_DR;
    Saluint8 CRC_ST_BLT_CT_SOCCU;
    Saluint8 ALIV_COU_ST_BLT_CT_SOCCU;
    Saluint8 ST_SEAT_OCCU_DR;
    Saluint8 ST_BLTB_SW_RLH;
    Saluint8 ST_SEAT_OCCU_RLH;
    Saluint8 ST_BLTB_SW_PS;
    Saluint8 ST_SEAT_OCCU_PS;
}ST_BLT_CT_SOCCUInfo_t;

typedef struct
{
    Saluint8 NO_VECH_1;
    Saluint8 NO_VECH_2;
    Saluint8 NO_VECH_3;
    Saluint8 NO_VECH_6;
    Saluint8 NO_VECH_7;
    Saluint8 NO_VECH_4;
    Saluint8 NO_VECH_5;
}FAHRGESTELLNUMMERInfo_t;

typedef struct
{
    Saluint32 T_SEC_COU_REL;
    Saluint16 T_DAY_COU_ABSL;
}RELATIVZEITInfo_t;

typedef struct
{
    Saluint16 RNG;
    Saluint8 ST_FLLV_FUTA_SPAR;
    Saluint8 FLLV_FUTA_RH;
    Saluint8 FLLV_FUTA_LH;
    Saluint8 FLLV_FUTA;
    Saluint32 MILE_KM;
}KILOMETERSTANDInfo_t;

typedef struct
{
    Saluint8 DISP_DATE_WDAY;
    Saluint8 DISP_DATE_DAY;
    Saluint8 DISP_DATE_MON;
    Saluint8 ST_DISP_CTI_DATE;
    Saluint16 DISP_DATE_YR;
    Saluint8 DISP_HR;
    Saluint8 DISP_SEC;
    Saluint8 DISP_MN;
}UHRZEIT_DATUMInfo_t;

typedef struct
{
    Saluint8 QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT;
    Saluint16 TAR_REPAT_XTRQ_FTAX_BAX_ACT;
    Saluint16 TAR_WMOM_PT_SUM_STAB_FAST;
    Saluint8 ST_ECU_RQ_WMOM_PT_SUM_STAB;
    Saluint8 QU_RQ_ILK_PT;
    Saluint16 QU_TAR_WMOM_PT_SUM_STAB;
    Saluint16 TAR_WMOM_PT_SUM_STAB;
}RQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t;

typedef struct
{
    Saluint8 ST_ECU_AVL_BRTORQ_SUM;
    Saluint8 QU_SER_PRMSN_DBC;
    Saluint8 QU_SER_PRF_BRK;
    Saluint16 AVL_BRTORQ_SUM_DVCH;
    Saluint16 AVL_BRTORQ_SUM;
    Saluint8 QU_AVL_BRTORQ_SUM;
    Saluint8 QU_AVL_BRTORQ_SUM_DVCH;
}AVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t;

typedef struct
{
    Saluint8 QU_AVL_BRTORQ_WHL_FLH;
    Saluint8 QU_AVL_BRTORQ_WHL_RRH;
    Saluint8 QU_AVL_BRTORQ_WHL_FRH;
    Saluint16 AVL_BRTORQ_WHL_RRH;
    Saluint16 AVL_BRTORQ_WHL_RLH;
    Saluint16 AVL_BRTORQ_WHL_FLH;
    Saluint8 QU_AVL_BRTORQ_WHL_RLH;
    Saluint16 AVL_BRTORQ_WHL_FRH;
}AVL_BRTORQ_WHLInfo_t;

typedef struct
{
    Saluint16 AVL_RPM_WHL_RLH;
    Saluint16 AVL_RPM_WHL_RRH;
    Saluint16 AVL_RPM_WHL_FLH;
    Saluint8 QU_AVL_RPM_WHL_FLH;
    Saluint8 QU_AVL_RPM_WHL_FRH;
    Saluint8 QU_AVL_RPM_WHL_RLH;
    Saluint8 QU_AVL_RPM_WHL_RRH;
    Saluint16 AVL_RPM_WHL_FRH;
}AVL_RPM_WHLInfo_t;

typedef struct
{
    Saluint8 QU_TAR_WMOM_PT_SUM_RECUP;
    Saluint16 TAR_WMOM_PT_SUM_RECUP;
    Saluint8 ST_ECU_RQ_WMOM_PT_SUM_RECUP;
}RQ_WMOM_PT_SUM_RECUPInfo_t;

typedef struct
{
    Saluint8 ST_ECU_ST_STAB_DSC;
    Saluint8 ST_BRG_MSA;
    Saluint8 ST_BRG_DV;
    Saluint8 ST_UNRU_DSC;
    Saluint16 QU_FN_FDR;
    Saluint16 QU_FN_ABS;
    Saluint16 QU_FN_ASC;
    Saluint8 PRD_TIM_VEHSS;
}ST_STAB_DSC_ST_STAB_DSC_2Info_t;

typedef struct
{
    Saluint16 QU_SER_ECBA;
    Saluint16 TAR_BRTORQ_SUM_COOTD;
    Saluint16 AVL_WAY_BRKFA;
    Saluint16 AVL_FORC_BRKFA;
}QU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t;

typedef struct
{
    Saluint16 AVL_QUAN_EES_WHL_RLH;
    Saluint16 AVL_QUAN_EES_WHL_RRH;
    Saluint16 AVL_QUAN_EES_WHL_FLH;
    Saluint16 AVL_QUAN_EES_WHL_FRH;
    Saluint8 QU_AVL_QUAN_EES_WHL_FLH;
    Saluint8 QU_AVL_QUAN_EES_WHL_RRH;
    Saluint8 QU_AVL_QUAN_EES_WHL_FRH;
    Saluint8 QU_AVL_QUAN_EES_WHL_RLH;
}AVL_QUAN_EES_WHLInfo_t;

typedef struct
{
    Saluint16 TAR_LTRQD_BAX;
    Saluint16 LIM_MAX_LTRQD_BAX;
}TAR_LTRQD_BAXInfo_t;

typedef struct
{
    Saluint8 QU_SER_CLCTR_YMR;
}ST_YMRInfo_t;

typedef struct
{
    Saluint16 MILE_FLH;
    Saluint16 MILE_FRH;
    Saluint16 MILE_INTM_TOMSRM;
    Saluint16 MILE;
}WEGSTRECKEInfo_t;

typedef struct
{
    Saluint8 ST_IDC_CC_DRDY_00;
    Saluint8 ST_CC_DRDY_00;
    Saluint8 TRANF_CC_DRDY_00;
    Saluint16 NO_CC_DRDY_00;
}DISP_CC_DRDY_00Info_t;

typedef struct
{
    Saluint8 TRANF_CC_BYPA_00;
    Saluint8 ST_IDC_CC_BYPA_00;
    Saluint8 ST_CC_BYPA_00;
    Saluint16 NO_CC_BYPA_00;
}DISP_CC_BYPA_00Info_t;

typedef struct
{
    Saluint8 ST_VEHSS;
}ST_VHSSInfo_t;

typedef struct
{
    Saluint16 QU_FN_OBD_1_SEN_1_ERR;
    Saluint16 QU_FN_OBD_1_SEN_3_ERR;
    Saluint16 QU_FN_OBD_1_SEN_4_ERR;
    Saluint8 DIAG_OBD_HYB_1_MUX_MAX;
    Saluint16 QU_FN_OBD_1_SEN_2_ERR;
    Saluint8 DIAG_OBD_HYB_1_MUX_IMME;
}DIAG_OBD_HYB_1Info_t;

typedef struct
{
    Saluint8 SU_DSC_WSS;
}SU_DSCInfo_t;

typedef struct
{
    Saluint8 QU_TPL_RDC;
    Saluint8 QU_FN_TYR_INFO_RDC;
    Saluint8 QU_TFAI_RDC;
}ST_TYR_RDCInfo_t;

typedef struct
{
    Saluint8 QU_TFAI_RPA;
    Saluint8 QU_FN_TYR_INFO_RPA;
    Saluint8 QU_TPL_RPA;
}ST_TYR_RPAInfo_t;

typedef struct
{
    Saluint32 TYR_ID_2;
    Saluint32 TYR_ID_1;
    Saluint32 TYR_ID_3;
    Saluint32 TYR_ID_4;
}RDC_TYR_ID1_RDC_TYR_ID2Info_t;

typedef struct
{
    Saluint8 QU_RDC_INIT_DISP;
}ST_TYR_2Info_t;

typedef struct
{
    Saluint16 AVL_TEMP_TYR_RRH;
    Saluint16 AVL_TEMP_TYR_FLH;
    Saluint16 AVL_TEMP_TYR_RLH;
    Saluint16 AVL_TEMP_TYR_FRH;
}AVL_T_TYRInfo_t;

typedef struct
{
    Saluint16 TEMP_BRDSK_RLH_VRFD;
    Saluint8 QU_TEMP_BRDSK_RLH_VRFD;
    Saluint16 TEMP_BRDSK_RRH_VRFD;
    Saluint8 QU_TEMP_BRDSK_RRH_VRFD;
}TEMP_BRKInfo_t;

typedef struct
{
    Saluint16 AVL_P_TYR_FRH;
    Saluint16 AVL_P_TYR_FLH;
    Saluint16 AVL_P_TYR_RRH;
    Saluint16 AVL_P_TYR_RLH;
}AVL_P_TYRInfo_t;

typedef struct
{
    Saluint8 DBG_RDCi_13;
    Saluint8 DBG_RDCi_04;
    Saluint8 DBG_RDCi_05;
    Saluint8 DBG_RDCi_06;
    Saluint8 DBG_RDCi_01;
    Saluint8 DBG_RDCi_02;
    Saluint8 DBG_RDCi_03;
    Saluint8 DBG_RDCi_07;
    Saluint8 DBG_RDCi_11;
    Saluint8 DBG_RDCi_10;
    Saluint8 DBG_RDCi_09;
    Saluint8 DBG_RDCi_15;
    Saluint8 DBG_RDCi_14;
    Saluint8 DBG_RDCi_12;
    Saluint8 DBG_RDCi_00;
    Saluint16 BDTM_TA_FR;
    Saluint16 BDTM_TA_RL;
    Saluint16 BDTM_TA_RR;
    Saluint8 DBG_RDCi_08;
    Saluint16 ST_STATE;
    Saluint16 BDTM_TA_FL;
    Saluint16 ABSRef;
    Saluint8 LOW_VOLTAGE;
    Saluint8 DEBUG_PCIB;
    Saluint8 DBG_DSC;
    Saluint16 LATACC;
    Saluint8 TRIGGER_FS;
    Saluint8 TRIGGER_IS;
}FR_DBG_DSCInfo_t;

typedef struct
{
    Saluint16 PressP_SimP_1_100_bar;
    Saluint16 PressP_CirP1_1_100_bar;
    Saluint16 PressP_CirP2_1_100_bar;
    Saluint16 SimPMoveAvr;
    Saluint16 CirPMoveAvr;
    Saluint16 CirP2MoveAvr;
}PressCalcInfo_t;

typedef struct
{
    Saluint8 SimP_Noise_Err;
    Saluint8 CirP1_Noise_Err;
    Saluint8 CirP2_Noise_Err;
}PressPInfo_t;

typedef struct
{
    Saluint16 PressP_SimPMoveAve;
    Saluint16 PressP_CirP1MoveAve;
    Saluint16 PressP_CirP2MoveAve;
    Saluint16 PressP_SimPMoveAveEolOfs;
    Saluint16 PressP_CirP1MoveAveEolOfs;
    Saluint16 PressP_CirP2MoveAveEolOfs;
}PressSenCalcInfo_t;

typedef struct
{
    Saluint8 PedalP_PTS1_Stick_Err;
    Saluint8 PedalP_PTS1_Offset_Err;
    Saluint8 PedalP_PTS1_Noise_Err;
    Saluint8 PedalP_PTS2_Stick_Err;
    Saluint8 PedalP_PTS2_Offset_Err;
    Saluint8 PedalP_PTS2_Noise_Err;
}PedalPData_t;


typedef Saluint8 FSBbsVlvInitTest_t;
typedef Saluint8 EcuModeSts_t;
typedef Saluint8 IgnOnOffSts_t;
typedef Saluint8 IgnEdgeSts_t;
typedef Saluint8 DiagClr_t;
typedef Saluint8 ArbAsicEnDrDrvInfo_t;
typedef Saluint8 ArbVlvSync_t;
typedef Saluint8 ArbVlvDriveState_t;
typedef Saluint8 FSEscVlvInitTest_t;
typedef Saluint8 DiagSci_t;
typedef Saluint8 DiagAhbSci_t;
typedef Saluint8 FuncInhibitVlvSts_t;
typedef Saluint8 FuncInhibitIocSts_t;
typedef Saluint8 FuncInhibitGdSts_t;
typedef Saluint8 CanRxGearSelDispErrInfo_t;
typedef Rtesint32 VdcLinkFild_t;
typedef Saluint16 VdcLink_t;
typedef Saluint8 BlsSwt_t;
typedef Saluint8 EscSwt_t;
typedef Saluint8 AvhSwt_t;
typedef Saluint8 BsSwt_t;
typedef Saluint8 DoorSwt_t;
typedef Saluint8 HzrdSwt_t;
typedef Saluint8 HdcSwt_t;
typedef Saluint8 PbSwt_t;
typedef Saluint8 MomEcuInhibit_t;
typedef Saluint8 PrlyEcuInhibit_t;
typedef Salsint16 PedlSimPRaw_t;
typedef Saluint8 FuncInhibitProxySts_t;
typedef Saluint8 FuncInhibitDiagSts_t;
typedef Saluint8 FuncInhibitFsrSts_t;
typedef Saluint8 FuncInhibitAcmioSts_t;
typedef Saluint8 FuncInhibitAcmctlSts_t;
typedef Saluint8 FuncInhibitMspSts_t;
typedef Saluint8 FuncInhibitNvmSts_t;
typedef Saluint8 FuncInhibitWssSts_t;
typedef Saluint8 FuncInhibitPedalSts_t;
typedef Saluint8 FuncInhibitPressSts_t;
typedef Saluint8 FuncInhibitRlySts_t;
typedef Saluint8 FuncInhibitSesSts_t;
typedef Saluint8 FuncInhibitSwtSts_t;
typedef Saluint8 FuncInhibitPrlySts_t;
typedef Saluint8 FuncInhibitMomSts_t;
typedef Saluint8 FuncInhibitVlvdSts_t;
typedef Saluint8 FuncInhibitSpimSts_t;
typedef Saluint8 FuncInhibitAdcifSts_t;
typedef Saluint8 FuncInhibitIcuSts_t;
typedef Saluint8 FuncInhibitMpsSts_t;
typedef Saluint8 FuncInhibitRegSts_t;
typedef Saluint8 FuncInhibitAsicSts_t;
typedef Saluint8 FuncInhibitCanTrcvSts_t;
typedef Saluint8 FuncInhibitBbcSts_t;
typedef Saluint8 FuncInhibitRbcSts_t;
typedef Saluint8 FuncInhibitArbiSts_t;
typedef Saluint8 FuncInhibitPctrlSts_t;
typedef Saluint8 FuncInhibitMccSts_t;
typedef Saluint8 FuncInhibitVlvActSts_t;
typedef Saluint8 VlvSync_t;
typedef Haluint8 AwdPrnDrv_t;
typedef Saluint8 FsrDcMtrShutDwn_t;
typedef Saluint8 FsrEnDrDrv_t;
typedef Saluint8 MTRInitTest_t;
typedef Salsint16 VacuumCalcType_t;
typedef Saluint8 MtrArbDriveState_t;
typedef Saluint8 MtrDiagState_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SAL_TYPES_H_ */
/** @} */
