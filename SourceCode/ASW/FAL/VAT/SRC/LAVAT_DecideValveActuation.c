/**
 * @defgroup LAVAT_DecideValveActuation LAVAT_DecideValveActuation
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAVAT_DecideValveActuation.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LAVAT_DecideValveActuation.h"
#include "../MUX/LAIDB_CallMultiplexValveActuation.h"





/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define M__1MS_CURR_DIFF_LIMITATION  ENABLE /* ENABLE DISABLE */

#if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)
	/* Cal Setting for 1ms Current diff limitation */
	#define S8_LIMIT_1MS_CURR_DIFF_CIRCVLV   0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_CUTVLV    0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_SIMVLV    0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_INLETVLV   0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_OUTLETVLV  0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_BALVLV    0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_CV        0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_RLV       0 /* Does not apply */
	#define S8_LIMIT_1MS_CURR_DIFF_PDV       0 /* Does not apply */

	#define S16_CURR_DIFF_FOR_1MS_LIMIT  	20 /* 20 mA */
    #define S8_DITHER_AMPLITUDE_CIRCVLV     20 /* 20 mA */
    #define S8_DITHER_AMPLITUDE_CUTVLV     	20 /* 20 mA */
    #define S8_DITHER_AMPLITUDE_SIMVLV    	20 /* 20 mA */
	#define S8_DITHER_AMPLITUDE_INLETVLV    20 /* 20 mA */
	#define S8_DITHER_AMPLITUDE_OUTLETVLV  	20 /* 20 mA */
	#define S8_DITHER_AMPLITUDE_BALVLV    	20 /* 20 mA */
	#define S8_DITHER_AMPLITUDE_CV        	20 /* 20 mA */
	#define S8_DITHER_AMPLITUDE_RLV        	20 /* 20 mA */
	#define S8_DITHER_PERIOD_MS             10 /* 10ms Cycle Pattern Dither */
#endif


#define U16_FULL_ON_CURRENT       2250
#define U16_SIMV_CURRENT_MAX      2500

#define S8_WL_INLET_CURRT_SCALE   2

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
    uint32_t lau1ValveAct:  1;
    uint32_t lau1ValveDitherToggle:  1;
    uint8_t  lau8ValveOnTime;
    uint16_t lau16Current;
    uint16_t lau16CurrentDither;

} LA_IDB_VALVE_ACT_t;

typedef struct
{
	uint16_t lau16CurrentOutput[5];

} LA_VALVE_DRV_t;

typedef struct
{
	int32_t las32WhlVlvCurrentOutput[5];

} LA_WL_VALVE_DRV_t;

typedef struct
{
	uint32_t lau1VlvDriveReq:	1;

}LA_VALVE_BIT_STRUCT_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_CircVlv1   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_CircVlv2   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActCutVlv1 = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActCutVlv2 = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActSimVlv  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActInFLVlv = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActInFRVlv = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActInRLVlv = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActInRRVlv = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_CircBalV1  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_CircBalV2  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ChamBalV   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActCV         = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActRLV        = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t laVAT_ActPDV        = {0,0,0,0,0};

FAL_STATIC LA_IDB_VALVE_ACT_t la_CircVlv1  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CircVlv2  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CutVlv1   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CutVlv2   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_SimVlv    = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_InFLVlv   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_InFRVlv   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_InRLVlv   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_InRRVlv   = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CircBalV1 = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CircBalV2 = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_ChamBalV  = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_CV        = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_RLV       = {0,0,0,0,0};
FAL_STATIC LA_IDB_VALVE_ACT_t la_PDV       = {0,0,0,0,0};

FAL_STATIC LA_VALVE_DRV_t laDRV_CutVlv1     = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_CutVlv2     = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_SimVlv      = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_INFL        = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_INFR        = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_INRL        = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_INRR        = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_CircBalV1   = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_CircBalV2   = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_ChamBalV    = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_CV         = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_RLV        = {0,0,0,0,0};
FAL_STATIC LA_VALVE_DRV_t laDRV_PDV        = {0,0,0,0,0};

FAL_STATIC LA_WL_VALVE_DRV_t laDRV_FlWhlOut = {0,0,0,0,0};
FAL_STATIC LA_WL_VALVE_DRV_t laDRV_FrWhlOut = {0,0,0,0,0};
FAL_STATIC LA_WL_VALVE_DRV_t laDRV_RlWhlOut = {0,0,0,0,0};
FAL_STATIC LA_WL_VALVE_DRV_t laDRV_RrWhlOut = {0,0,0,0,0};

FAL_STATIC LA_VALVE_BIT_STRUCT_t laHWActFlg01 = {0};

/*FAL_STATIC uint8_t IdbPCtrlActFlg;*/ /* IDB Pressure control active: AHBon flag ��ü */
/*FAL_STATIC uint8_t PedalSimPressGenOnFlg;*/ /* PedlSimrPGendOnFlg */

#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VAT_CTRL_START_SEC_CODE
#include "Vat_MemMap.h"

static void LAVAT_vGetVlvInterface(void);
static void LAVAT_vGetOutletVlvIf(void);
static void LAVAT_vGetInletVlvIf(void);
static void LAVAT_vGetBalVlvIf(void);
static void LAVAT_vGetCirVlvIf(void);
static void LAVAT_vGetPedalFeelVlvIf(void);

static void LAVAT_vSetVlvInterface(void);
static void LAVAT_vSetOutletVlvActn(void);
static void LAVAT_vSetInletVlvActn(void);
static void LAVAT_vSetBalVlvActn(void);
static void LAVAT_vSetPedalFeelVlvActn(void);
static void LAVAT_vSetCirVlvActn(void);

static void LAVAT_vOrganizeEachVldCurrent( const LA_IDB_VALVE_ACT_t* const laVAT_VV,  LA_IDB_VALVE_ACT_t *la_VV, uint16_t u16CurrentMax);
static void LAVAT_vCalcVlvActVariables(void);

  #if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)
static void LAVAT_vAllocVlvCurrentArray( const LA_IDB_VALVE_ACT_t* const la_VV,  LA_VALVE_DRV_t *la_DRV, int8_t s8Limit1msCurrentDiffEnable,   int16_t s16DitherAmplitude);
  #else
static void LAVAT_vAllocVlvCurrentArray(LA_IDB_VALVE_ACT_t *la_VV, LA_VALVE_DRV_t *la_DRV);
  #endif
static void LAVAT_vAllocCurrentArray(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAVAT_vCallVlvActn(void)
{
	LAVAT_vGetVlvInterface();
	LAVAT_vCalcVlvActVariables();
 	LAVAT_vAllocCurrentArray();
 	LAVAT_vSetVlvInterface();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAVAT_vGetVlvInterface(void)
{

	LAVAT_vGetPedalFeelVlvIf();
	LAVAT_vGetOutletVlvIf();
	LAVAT_vGetInletVlvIf();
	LAVAT_vGetBalVlvIf();
	LAVAT_vGetCirVlvIf();

}

static void LAVAT_vGetPedalFeelVlvIf(void)
{
	laVAT_ActCutVlv1.lau1ValveAct   = laVAT_Localbus.stCutVlv1.lau1ValveAct     ;
	laVAT_ActCutVlv1.lau8ValveOnTime= laVAT_Localbus.stCutVlv1.lau8ValveOnTime  ;
	laVAT_ActCutVlv1.lau16Current   = laVAT_Localbus.stCutVlv1.lau16Current     ;

	laVAT_ActCutVlv2.lau1ValveAct   = laVAT_Localbus.stCutVlv2.lau1ValveAct     ;
	laVAT_ActCutVlv2.lau8ValveOnTime= laVAT_Localbus.stCutVlv2.lau8ValveOnTime  ;
	laVAT_ActCutVlv2.lau16Current   = laVAT_Localbus.stCutVlv2.lau16Current     ;

	laVAT_ActSimVlv.lau1ValveAct    = laVAT_Localbus.stSimVlv.lau1ValveAct      ;
	laVAT_ActSimVlv.lau8ValveOnTime = laVAT_Localbus.stSimVlv.lau8ValveOnTime   ;
	laVAT_ActSimVlv.lau16Current    = laVAT_Localbus.stSimVlv.lau16Current      ;
}


static void LAVAT_vGetInletVlvIf(void)
{
	laDRV_INFL.lau16CurrentOutput[0]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[0] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INFL.lau16CurrentOutput[1]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[1] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INFL.lau16CurrentOutput[2]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[2] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INFL.lau16CurrentOutput[3]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[3] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INFL.lau16CurrentOutput[4]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[4] * S8_WL_INLET_CURRT_SCALE ;

	laDRV_INFR.lau16CurrentOutput[0]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[0] * S8_WL_INLET_CURRT_SCALE  ;
	laDRV_INFR.lau16CurrentOutput[1]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[1] * S8_WL_INLET_CURRT_SCALE  ;
	laDRV_INFR.lau16CurrentOutput[2]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[2] * S8_WL_INLET_CURRT_SCALE  ;
	laDRV_INFR.lau16CurrentOutput[3]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[3] * S8_WL_INLET_CURRT_SCALE  ;
	laDRV_INFR.lau16CurrentOutput[4]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[4] * S8_WL_INLET_CURRT_SCALE  ;

	laDRV_INRL.lau16CurrentOutput[0]            =     Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[0]  * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRL.lau16CurrentOutput[1]            =     Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[1]  * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRL.lau16CurrentOutput[2]            =     Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[2]  * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRL.lau16CurrentOutput[3]            =     Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[3]  * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRL.lau16CurrentOutput[4]            =     Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[4]  * S8_WL_INLET_CURRT_SCALE ;

	laDRV_INRR.lau16CurrentOutput[0]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[0] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRR.lau16CurrentOutput[1]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[1] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRR.lau16CurrentOutput[2]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[2] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRR.lau16CurrentOutput[3]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[3] * S8_WL_INLET_CURRT_SCALE ;
	laDRV_INRR.lau16CurrentOutput[4]             =    Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[4] * S8_WL_INLET_CURRT_SCALE ;
}

static void LAVAT_vGetBalVlvIf(void)
{
	laVAT_CircBalV1.lau1ValveAct    =laVAT_Localbus.stBalCircV1.lau1ValveAct    ;
	laVAT_CircBalV1.lau8ValveOnTime =laVAT_Localbus.stBalCircV1.lau8ValveOnTime ;
	laVAT_CircBalV1.lau16Current    =laVAT_Localbus.stBalCircV1.lau16Current    ;

	laVAT_CircBalV2.lau1ValveAct    =laVAT_Localbus.stBalCircV2.lau1ValveAct    ;
	laVAT_CircBalV2.lau8ValveOnTime =laVAT_Localbus.stBalCircV2.lau8ValveOnTime ;
	laVAT_CircBalV2.lau16Current    =laVAT_Localbus.stBalCircV2.lau16Current    ;

	laVAT_ChamBalV.lau1ValveAct     =laVAT_Localbus.stBalChamV.lau1ValveAct      ;
	laVAT_ChamBalV.lau8ValveOnTime  =laVAT_Localbus.stBalChamV.lau8ValveOnTime   ;
	laVAT_ChamBalV.lau16Current     =laVAT_Localbus.stBalChamV.lau16Current      ;
}

static void LAVAT_vGetCirVlvIf(void)
{
	laVAT_ActCV.lau1ValveAct    =laVAT_Localbus.stCircVlv.lau1ValveAct    ;
	laVAT_ActCV.lau8ValveOnTime =laVAT_Localbus.stCircVlv.lau8ValveOnTime ;
	laVAT_ActCV.lau16Current    =laVAT_Localbus.stCircVlv.lau16Current    ;

	laVAT_ActRLV.lau1ValveAct    =laVAT_Localbus.stRelsVlv.lau1ValveAct    ;
	laVAT_ActRLV.lau8ValveOnTime =laVAT_Localbus.stRelsVlv.lau8ValveOnTime ;
	laVAT_ActRLV.lau16Current    =laVAT_Localbus.stRelsVlv.lau16Current    ;

	laVAT_ActPDV.lau1ValveAct    =laVAT_Localbus.stPDVlv.lau1ValveAct    ;
	laVAT_ActPDV.lau8ValveOnTime =laVAT_Localbus.stPDVlv.lau8ValveOnTime ;
	laVAT_ActPDV.lau16Current    =laVAT_Localbus.stPDVlv.lau16Current    ;
}

static void LAVAT_vGetOutletVlvIf(void)
{
	laDRV_FlWhlOut.las32WhlVlvCurrentOutput[0]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[0]  ;
	laDRV_FlWhlOut.las32WhlVlvCurrentOutput[1]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[1]  ;
	laDRV_FlWhlOut.las32WhlVlvCurrentOutput[2]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[2]  ;
	laDRV_FlWhlOut.las32WhlVlvCurrentOutput[3]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[3]  ;
	laDRV_FlWhlOut.las32WhlVlvCurrentOutput[4]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[4]  ;

	laDRV_FrWhlOut.las32WhlVlvCurrentOutput[0]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[0]  ;
	laDRV_FrWhlOut.las32WhlVlvCurrentOutput[1]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[1]  ;
	laDRV_FrWhlOut.las32WhlVlvCurrentOutput[2]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[2]  ;
	laDRV_FrWhlOut.las32WhlVlvCurrentOutput[3]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[3]  ;
	laDRV_FrWhlOut.las32WhlVlvCurrentOutput[4]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[4]  ;

	laDRV_RlWhlOut.las32WhlVlvCurrentOutput[0]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[0]  ;
	laDRV_RlWhlOut.las32WhlVlvCurrentOutput[1]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[1]  ;
	laDRV_RlWhlOut.las32WhlVlvCurrentOutput[2]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[2]  ;
	laDRV_RlWhlOut.las32WhlVlvCurrentOutput[3]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[3]  ;
	laDRV_RlWhlOut.las32WhlVlvCurrentOutput[4]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[4]  ;

	laDRV_RrWhlOut.las32WhlVlvCurrentOutput[0]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[0]  ;
	laDRV_RrWhlOut.las32WhlVlvCurrentOutput[1]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[1]  ;
	laDRV_RrWhlOut.las32WhlVlvCurrentOutput[2]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[2]  ;
	laDRV_RrWhlOut.las32WhlVlvCurrentOutput[3]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[3]  ;
	laDRV_RrWhlOut.las32WhlVlvCurrentOutput[4]  =    Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[4]  ;
}

static void LAVAT_vSetVlvInterface(void)
{
	/* wheel valve Current & act */
	LAVAT_vSetOutletVlvActn();
	/* inlet valve Current & act */
	LAVAT_vSetInletVlvActn();

	LAVAT_vSetBalVlvActn();

	LAVAT_vSetPedalFeelVlvActn();
	/* pedal feel generation*/

	LAVAT_vSetCirVlvActn();
}

static void LAVAT_vSetCirVlvActn(void)
{
	LA_VALVE_BIT_STRUCT_t laHWAct_CV, laHWAct_RLV, laHWAct_PDV;

	Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0] =  (int32_t)laDRV_CV.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[1] =  (int32_t)laDRV_CV.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[2] =  (int32_t)laDRV_CV.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[3] =  (int32_t)laDRV_CV.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[4] =  (int32_t)laDRV_CV.lau16CurrentOutput[4];

	laHWAct_CV.lau1VlvDriveReq   = (((laDRV_CV.lau16CurrentOutput[0])>0) ||
									((laDRV_CV.lau16CurrentOutput[1])>0) ||
									((laDRV_CV.lau16CurrentOutput[2])>0) ||
									((laDRV_CV.lau16CurrentOutput[3])>0) ||
									((laDRV_CV.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0] =  (int32_t)laDRV_RLV.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[1] =  (int32_t)laDRV_RLV.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[2] =  (int32_t)laDRV_RLV.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[3] =  (int32_t)laDRV_RLV.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[4] =  (int32_t)laDRV_RLV.lau16CurrentOutput[4];

    laHWAct_RLV.lau1VlvDriveReq  = (((laDRV_RLV.lau16CurrentOutput[0])>0) ||
									((laDRV_RLV.lau16CurrentOutput[1])>0) ||
									((laDRV_RLV.lau16CurrentOutput[2])>0) ||
									((laDRV_RLV.lau16CurrentOutput[3])>0) ||
									((laDRV_RLV.lau16CurrentOutput[4])>0));

    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0] =  (int32_t)laDRV_PDV.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[1] =  (int32_t)laDRV_PDV.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[2] =  (int32_t)laDRV_PDV.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[3] =  (int32_t)laDRV_PDV.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[4] =  (int32_t)laDRV_PDV.lau16CurrentOutput[4];

    laHWAct_PDV.lau1VlvDriveReq  = (((laDRV_PDV.lau16CurrentOutput[0])>0) ||
    		((laDRV_PDV.lau16CurrentOutput[1])>0) ||
    		((laDRV_PDV.lau16CurrentOutput[2])>0) ||
    		((laDRV_PDV.lau16CurrentOutput[3])>0) ||
    		((laDRV_PDV.lau16CurrentOutput[4])>0));



	Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq      = laHWAct_CV.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq      = laHWAct_RLV.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq = laHWAct_PDV.lau1VlvDriveReq;

    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvDataLen      = 5;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvDataLen      = 5;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen = 5;

}

static void LAVAT_vSetPedalFeelVlvActn(void)
{
	LA_VALVE_BIT_STRUCT_t laHWAct_CutVlv1, laHWAct_CutVlv2,	laHWAct_SimVlv;

	Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0] =  laDRV_CutVlv1.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[1] =  laDRV_CutVlv1.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[2] =  laDRV_CutVlv1.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[3] =  laDRV_CutVlv1.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[4] =  laDRV_CutVlv1.lau16CurrentOutput[4];

    laHWAct_CutVlv1.lau1VlvDriveReq =  (((laDRV_CutVlv1.lau16CurrentOutput[0])>0) ||
    		                            ((laDRV_CutVlv1.lau16CurrentOutput[1])>0) ||
    		                            ((laDRV_CutVlv1.lau16CurrentOutput[2])>0) ||
    		                            ((laDRV_CutVlv1.lau16CurrentOutput[3])>0) ||
    		                            ((laDRV_CutVlv1.lau16CurrentOutput[4])>0));

    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[0] =  laDRV_CutVlv2.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[1] =  laDRV_CutVlv2.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[2] =  laDRV_CutVlv2.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[3] =  laDRV_CutVlv2.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[4] =  laDRV_CutVlv2.lau16CurrentOutput[4];

    laHWAct_CutVlv2.lau1VlvDriveReq =  (((laDRV_CutVlv2.lau16CurrentOutput[0])>0) ||
    									((laDRV_CutVlv2.lau16CurrentOutput[1])>0) ||
    									((laDRV_CutVlv2.lau16CurrentOutput[2])>0) ||
    									((laDRV_CutVlv2.lau16CurrentOutput[3])>0) ||
    								    ((laDRV_CutVlv2.lau16CurrentOutput[4])>0));

    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0] = laDRV_SimVlv.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[1] = laDRV_SimVlv.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[2] = laDRV_SimVlv.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[3] = laDRV_SimVlv.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[4] = laDRV_SimVlv.lau16CurrentOutput[4];

    laHWAct_SimVlv.lau1VlvDriveReq =  (((laDRV_SimVlv.lau16CurrentOutput[0])>0) ||
    		                           ((laDRV_SimVlv.lau16CurrentOutput[1])>0) ||
    		                           ((laDRV_SimVlv.lau16CurrentOutput[2])>0) ||
    		                           ((laDRV_SimVlv.lau16CurrentOutput[3])>0) ||
    		                           ((laDRV_SimVlv.lau16CurrentOutput[4])>0));
    		                           
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq = laHWAct_CutVlv1.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq = laHWAct_CutVlv2.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq = laHWAct_SimVlv.lau1VlvDriveReq;

    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvDataLen = 5;
}

static void LAVAT_vSetInletVlvActn(void)
{
	LA_VALVE_BIT_STRUCT_t laHWAct_INFL, laHWAct_INFR, laHWAct_INRL, laHWAct_INRR;

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[0] =  (int32_t)laDRV_INFL.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[1] =  (int32_t)laDRV_INFL.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[2] =  (int32_t)laDRV_INFL.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[3] =  (int32_t)laDRV_INFL.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[4] =  (int32_t)laDRV_INFL.lau16CurrentOutput[4];

	laHWAct_INFL.lau1VlvDriveReq = (((laDRV_INFL.lau16CurrentOutput[0])>0) ||
									((laDRV_INFL.lau16CurrentOutput[1])>0) ||
									((laDRV_INFL.lau16CurrentOutput[2])>0) ||
									((laDRV_INFL.lau16CurrentOutput[3])>0) ||
									((laDRV_INFL.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[0] =  (int32_t)laDRV_INFR.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[1] =  (int32_t)laDRV_INFR.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[2] =  (int32_t)laDRV_INFR.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[3] =  (int32_t)laDRV_INFR.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[4] =  (int32_t)laDRV_INFR.lau16CurrentOutput[4];

    laHWAct_INFR.lau1VlvDriveReq = (((laDRV_INFR.lau16CurrentOutput[0])>0) ||
									((laDRV_INFR.lau16CurrentOutput[1])>0) ||
									((laDRV_INFR.lau16CurrentOutput[2])>0) ||
									((laDRV_INFR.lau16CurrentOutput[3])>0) ||
									((laDRV_INFR.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[0] =  (int32_t)laDRV_INRL.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[1] =  (int32_t)laDRV_INRL.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[2] =  (int32_t)laDRV_INRL.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[3] =  (int32_t)laDRV_INRL.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[4] =  (int32_t)laDRV_INRL.lau16CurrentOutput[4];

    laHWAct_INRL.lau1VlvDriveReq = (((laDRV_INRL.lau16CurrentOutput[0])>0) ||
									((laDRV_INRL.lau16CurrentOutput[1])>0) ||
									((laDRV_INRL.lau16CurrentOutput[2])>0) ||
									((laDRV_INRL.lau16CurrentOutput[3])>0) ||
									((laDRV_INRL.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[0] =  (int32_t)laDRV_INRR.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[1] =  (int32_t)laDRV_INRR.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[2] =  (int32_t)laDRV_INRR.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[3] =  (int32_t)laDRV_INRR.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[4] =  (int32_t)laDRV_INRR.lau16CurrentOutput[4];

    laHWAct_INRR.lau1VlvDriveReq = (((laDRV_INRR.lau16CurrentOutput[0])>0) ||
									((laDRV_INRR.lau16CurrentOutput[1])>0) ||
									((laDRV_INRR.lau16CurrentOutput[2])>0) ||
									((laDRV_INRR.lau16CurrentOutput[3])>0) ||
									((laDRV_INRR.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq = laHWAct_INFL.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq = laHWAct_INFR.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq = laHWAct_INRL.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq = laHWAct_INRR.lau1VlvDriveReq;
    
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvDataLen = 5;
}

static void LAVAT_vSetBalVlvActn(void)
{
	LA_VALVE_BIT_STRUCT_t laHWAct_CircBalV1, laHWAct_CircBalV2,laHWAct_ChamBalV;

	Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[0] = (int32_t)laDRV_CircBalV1.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[1] = (int32_t)laDRV_CircBalV1.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[2] = (int32_t)laDRV_CircBalV1.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[3] = (int32_t)laDRV_CircBalV1.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[4] = (int32_t)laDRV_CircBalV1.lau16CurrentOutput[4];

    laHWAct_CircBalV1.lau1VlvDriveReq =(((laDRV_CircBalV1.lau16CurrentOutput[0])>0) ||
    		                            ((laDRV_CircBalV1.lau16CurrentOutput[1])>0) ||
    		                            ((laDRV_CircBalV1.lau16CurrentOutput[2])>0) ||
    		                            ((laDRV_CircBalV1.lau16CurrentOutput[3])>0) ||
    		                            ((laDRV_CircBalV1.lau16CurrentOutput[4])>0));


	Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[0] = (int32_t)laDRV_CircBalV2.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[1] = (int32_t)laDRV_CircBalV2.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[2] = (int32_t)laDRV_CircBalV2.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[3] = (int32_t)laDRV_CircBalV2.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[4] = (int32_t)laDRV_CircBalV2.lau16CurrentOutput[4];

    laHWAct_CircBalV2.lau1VlvDriveReq =(((laDRV_CircBalV2.lau16CurrentOutput[0])>0) ||
    									((laDRV_CircBalV2.lau16CurrentOutput[1])>0) ||
    									((laDRV_CircBalV2.lau16CurrentOutput[2])>0) ||
    									((laDRV_CircBalV2.lau16CurrentOutput[3])>0) ||
    								    ((laDRV_CircBalV2.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0] = (int32_t)laDRV_ChamBalV.lau16CurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[1] = (int32_t)laDRV_ChamBalV.lau16CurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[2] = (int32_t)laDRV_ChamBalV.lau16CurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[3] = (int32_t)laDRV_ChamBalV.lau16CurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[4] = (int32_t)laDRV_ChamBalV.lau16CurrentOutput[4];

    laHWAct_ChamBalV.lau1VlvDriveReq = (((laDRV_ChamBalV.lau16CurrentOutput[0])>0) ||
    		                            ((laDRV_ChamBalV.lau16CurrentOutput[1])>0) ||
    		                            ((laDRV_ChamBalV.lau16CurrentOutput[2])>0) ||
    		                            ((laDRV_ChamBalV.lau16CurrentOutput[3])>0) ||
    		                            ((laDRV_ChamBalV.lau16CurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq = laHWAct_CircBalV1.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq = laHWAct_CircBalV2.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq = laHWAct_ChamBalV.lau1VlvDriveReq;

    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen = 5;

}

static void LAVAT_vSetOutletVlvActn(void)
{
	LA_VALVE_BIT_STRUCT_t laHWAct_FlWhlOut,laHWAct_FrWhlOut,laHWAct_RlWhlOut,laHWAct_RrWhlOut;

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[0] =  laDRV_FlWhlOut.las32WhlVlvCurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[1] =  laDRV_FlWhlOut.las32WhlVlvCurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[2] =  laDRV_FlWhlOut.las32WhlVlvCurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[3] =  laDRV_FlWhlOut.las32WhlVlvCurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[4] =  laDRV_FlWhlOut.las32WhlVlvCurrentOutput[4];

    laHWAct_FlWhlOut.lau1VlvDriveReq = (((laDRV_FlWhlOut.las32WhlVlvCurrentOutput[0])>0) ||
    		                            ((laDRV_FlWhlOut.las32WhlVlvCurrentOutput[1])>0) ||
    		                            ((laDRV_FlWhlOut.las32WhlVlvCurrentOutput[2])>0) ||
    		                            ((laDRV_FlWhlOut.las32WhlVlvCurrentOutput[3])>0) ||
    		                            ((laDRV_FlWhlOut.las32WhlVlvCurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[0] =  laDRV_FrWhlOut.las32WhlVlvCurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[1] =  laDRV_FrWhlOut.las32WhlVlvCurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[2] =  laDRV_FrWhlOut.las32WhlVlvCurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[3] =  laDRV_FrWhlOut.las32WhlVlvCurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[4] =  laDRV_FrWhlOut.las32WhlVlvCurrentOutput[4];

    laHWAct_FrWhlOut.lau1VlvDriveReq = (((laDRV_FrWhlOut.las32WhlVlvCurrentOutput[0])>0) ||
    									((laDRV_FrWhlOut.las32WhlVlvCurrentOutput[1])>0) ||
    									((laDRV_FrWhlOut.las32WhlVlvCurrentOutput[2])>0) ||
    									((laDRV_FrWhlOut.las32WhlVlvCurrentOutput[3])>0) ||
    								    ((laDRV_FrWhlOut.las32WhlVlvCurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[0] =  laDRV_RlWhlOut.las32WhlVlvCurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[1] =  laDRV_RlWhlOut.las32WhlVlvCurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[2] =  laDRV_RlWhlOut.las32WhlVlvCurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[3] =  laDRV_RlWhlOut.las32WhlVlvCurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[4] =  laDRV_RlWhlOut.las32WhlVlvCurrentOutput[4];

    laHWAct_RlWhlOut.lau1VlvDriveReq = (((laDRV_RlWhlOut.las32WhlVlvCurrentOutput[0])>0) ||
    		                            ((laDRV_RlWhlOut.las32WhlVlvCurrentOutput[1])>0) ||
    		                            ((laDRV_RlWhlOut.las32WhlVlvCurrentOutput[2])>0) ||
    		                            ((laDRV_RlWhlOut.las32WhlVlvCurrentOutput[3])>0) ||
    		                            ((laDRV_RlWhlOut.las32WhlVlvCurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[0] =  laDRV_RrWhlOut.las32WhlVlvCurrentOutput[0];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[1] =  laDRV_RrWhlOut.las32WhlVlvCurrentOutput[1];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[2] =  laDRV_RrWhlOut.las32WhlVlvCurrentOutput[2];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[3] =  laDRV_RrWhlOut.las32WhlVlvCurrentOutput[3];
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[4] =  laDRV_RrWhlOut.las32WhlVlvCurrentOutput[4];

    laHWAct_RrWhlOut.lau1VlvDriveReq = (((laDRV_RrWhlOut.las32WhlVlvCurrentOutput[0])>0) ||
    		                            ((laDRV_RrWhlOut.las32WhlVlvCurrentOutput[1])>0) ||
    		                            ((laDRV_RrWhlOut.las32WhlVlvCurrentOutput[2])>0) ||
    		                            ((laDRV_RrWhlOut.las32WhlVlvCurrentOutput[3])>0) ||
    		                            ((laDRV_RrWhlOut.las32WhlVlvCurrentOutput[4])>0));

	Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq = laHWAct_FlWhlOut.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq = laHWAct_FrWhlOut.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq = laHWAct_RlWhlOut.lau1VlvDriveReq;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq = laHWAct_RrWhlOut.lau1VlvDriveReq;
    
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvDataLen = 5;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvDataLen = 5;
}

static void LAVAT_vCalcVlvActVariables(void)
{

	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActCutVlv1, (LA_IDB_VALVE_ACT_t *)&la_CutVlv1,  U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActCutVlv2, (LA_IDB_VALVE_ACT_t *)&la_CutVlv2,  U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActSimVlv,  (LA_IDB_VALVE_ACT_t *)&la_SimVlv, U16_SIMV_CURRENT_MAX);

	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_CircBalV1, (LA_IDB_VALVE_ACT_t *)&la_CircBalV1, U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_CircBalV2, (LA_IDB_VALVE_ACT_t *)&la_CircBalV2, U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ChamBalV,  (LA_IDB_VALVE_ACT_t *)&la_ChamBalV,  U16_FULL_ON_CURRENT);

	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActCV,        (LA_IDB_VALVE_ACT_t *)&la_CV,        U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActRLV,       (LA_IDB_VALVE_ACT_t *)&la_RLV,       U16_FULL_ON_CURRENT);
	LAVAT_vOrganizeEachVldCurrent((LA_IDB_VALVE_ACT_t *)&laVAT_ActPDV,       (LA_IDB_VALVE_ACT_t *)&la_PDV,       U16_FULL_ON_CURRENT);


}

static void LAVAT_vAllocCurrentArray(void)
{
  #if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)

	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_CutVlv1, (LA_VALVE_DRV_t *)&laDRV_CutVlv1, S8_LIMIT_1MS_CURR_DIFF_CUTVLV, 0);
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_CutVlv2, (LA_VALVE_DRV_t *)&laDRV_CutVlv2, S8_LIMIT_1MS_CURR_DIFF_CUTVLV, 0);

	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_SimVlv, (LA_VALVE_DRV_t *)&laDRV_SimVlv, S8_LIMIT_1MS_CURR_DIFF_SIMVLV, 0);

	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_CircBalV1, (LA_VALVE_DRV_t *)&laDRV_CircBalV1, S8_LIMIT_1MS_CURR_DIFF_BALVLV, 0);
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_CircBalV2, (LA_VALVE_DRV_t *)&laDRV_CircBalV2, S8_LIMIT_1MS_CURR_DIFF_BALVLV, 0);

	/*TODO: Dither Cal is temporary for developing, should be replaced with Logic parameter */
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_ChamBalV,  (LA_VALVE_DRV_t *)&laDRV_ChamBalV,  S8_LIMIT_1MS_CURR_DIFF_BALVLV, U16_BALV_DITHER_AMPLITUDE);
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_CV,        (LA_VALVE_DRV_t *)&laDRV_CV,        S8_LIMIT_1MS_CURR_DIFF_CV,     U16_CIRV_DITHER_AMPLITUDE);
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_RLV,       (LA_VALVE_DRV_t *)&laDRV_RLV,       S8_LIMIT_1MS_CURR_DIFF_RLV,    U16_RLV_DITHER_AMPLITUDE);
	LAVAT_vAllocVlvCurrentArray((LA_IDB_VALVE_ACT_t *)&la_PDV,       (LA_VALVE_DRV_t *)&laDRV_PDV,       S8_LIMIT_1MS_CURR_DIFF_PDV,    U16_PDV_DITHER_AMPLITUDE);
	/* Dither Cal is temporary for developing, should be replaced with Logic parameter */

  #else
	LAVAT_vAllocVlvCurrentArray(&la_CutVlv1, &laDRV_CutVlv1);
	LAVAT_vAllocVlvCurrentArray(&la_CutVlv2, &laDRV_CutVlv2);

	LAVAT_vAllocVlvCurrentArray(&la_SimVlv, &laDRV_SimVlv);

	LAVAT_vAllocVlvCurrentArray(&la_InFLVlv,  &laDRV_INFL);
	LAVAT_vAllocVlvCurrentArray(&la_InFRVlv,  &laDRV_INFR);
	LAVAT_vAllocVlvCurrentArray(&la_InRLVlv,  &laDRV_INRL);
	LAVAT_vAllocVlvCurrentArray(&la_InRRVlv,  &laDRV_INRR);
                                             
	LAVAT_vAllocVlvCurrentArray(&la_CircBalV1, &laDRV_CircBalV1);
	LAVAT_vAllocVlvCurrentArray(&la_CircBalV2, &laDRV_CircBalV2);
	LAVAT_vAllocVlvCurrentArray(&la_ChamBalV, &laDRV_ChamBalV);
  #endif
}

static void LAVAT_vAllocVlvCurrentArray( const LA_IDB_VALVE_ACT_t* const la_VV,  LA_VALVE_DRV_t *la_DRV
		#if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)
	, int8_t s8Limit1msCurrentDiffEnable, int16_t s16DitherAmplitude
	  #endif
		)
{

    uint8_t CurrTim;
  #if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)
    int16_t s16CurrentScanAbsDiff;
    int16_t s16Current1msDiffLimit;
    int16_t s16CurrentDither1ms;
  #endif /* M__1MS_CURR_DIFF_LIMITATION */


  #if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)

	if(s8Limit1msCurrentDiffEnable==1)
	{
	    s16CurrentScanAbsDiff = ((int16_t)la_VV->lau16Current - (int16_t)la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-1]);
	}
	else
	{
	    s16CurrentScanAbsDiff = 0;
	}

	if((s16DitherAmplitude > 0) && (la_VV->lau16Current>S16_CURRENT_300_MA))
	{
		s16CurrentDither1ms = (s16DitherAmplitude/((S8_DITHER_PERIOD_MS-1)/2));
	}
	else
	{
		s16CurrentDither1ms = 0;
	}

	if((s16CurrentScanAbsDiff >= S16_CURR_DIFF_FOR_1MS_LIMIT)||(s16CurrentScanAbsDiff <= (-S16_CURR_DIFF_FOR_1MS_LIMIT)) ||(s16CurrentDither1ms > 0) )
	{
	    s16Current1msDiffLimit = s16CurrentScanAbsDiff / U8_FULL_CYCLETIME;

		if(la_VV->lau1ValveDitherToggle == 1)
		{
	        /*la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-1] = (la_VV->lau16Current) + (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-2] = (la_VV->lau16Current - (s16Current1msDiffLimit)) + (s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-3] = (la_VV->lau16Current - (s16Current1msDiffLimit*2));
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-4] = (la_VV->lau16Current - (s16Current1msDiffLimit*3)) - (s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-5] = (la_VV->lau16Current - (s16Current1msDiffLimit*4)) - (2*s16CurrentDither1ms);*/

	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-1] = (la_VV->lau16Current);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-2] = (la_VV->lau16Current);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-3] = (la_VV->lau16Current) + (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-4] = (la_VV->lau16Current) + (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-5] = (la_VV->lau16Current) - (2*s16CurrentDither1ms);
	    }
	    else
	    {
/*
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-1] = (la_VV->lau16Current) - (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-2] = (la_VV->lau16Current - (s16Current1msDiffLimit)) - (s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-3] = (la_VV->lau16Current - (s16Current1msDiffLimit*2));
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-4] = (la_VV->lau16Current - (s16Current1msDiffLimit*3)) + (s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-5] = (la_VV->lau16Current - (s16Current1msDiffLimit*4)) + (2*s16CurrentDither1ms);
*/

	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-1] = (la_VV->lau16Current) - (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-2] = (la_VV->lau16Current) + (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-3] = (la_VV->lau16Current) + (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-4] = (la_VV->lau16Current) - (2*s16CurrentDither1ms);
	        la_DRV->lau16CurrentOutput[U8_FULL_CYCLETIME-5] = (la_VV->lau16Current) - (2*s16CurrentDither1ms);

	    }

	}
	else
	{

  #endif /* (M__1MS_CURR_DIFF_LIMITATION) */

	    if(la_VV->lau8ValveOnTime>=U8_FULL_CYCLETIME)
	    {
	        for(CurrTim=0; CurrTim<U8_FULL_CYCLETIME; CurrTim++)
	        {
	            la_DRV->lau16CurrentOutput[CurrTim] = la_VV->lau16Current;
	        }
	    }
	    else
	    {
	        for(CurrTim=0; CurrTim<(la_VV->lau8ValveOnTime); CurrTim++)
	        {
	            la_DRV->lau16CurrentOutput[CurrTim] = la_VV->lau16Current;
	        }

	        for(CurrTim=la_VV->lau8ValveOnTime; CurrTim<U8_FULL_CYCLETIME; CurrTim++)
	        {
	            la_DRV->lau16CurrentOutput[CurrTim] = 0;
	        }
	    }

  #if (M__1MS_CURR_DIFF_LIMITATION==ENABLE)
	}
  #endif /* M__1MS_CURR_DIFF_LIMITATION */
}

static void LAVAT_vOrganizeEachVldCurrent( const LA_IDB_VALVE_ACT_t* const laVAT_VV,  LA_IDB_VALVE_ACT_t *la_VV, uint16_t u16CurrentMax)
{

	la_VV->lau1ValveAct = laVAT_VV->lau1ValveAct;

    if(laVAT_VV->lau1ValveAct==1)
    {
        if(laVAT_VV->lau16Current > u16CurrentMax)
        {
            la_VV->lau16Current = u16CurrentMax;
        }
        else
        {
            la_VV->lau16Current = laVAT_VV->lau16Current;
        }

        la_VV->lau8ValveOnTime = laVAT_VV->lau8ValveOnTime;
        la_VV->lau16CurrentDither = laVAT_VV->lau16CurrentDither;

        if(la_VV->lau16Current > 0)
        {
	        la_VV->lau1ValveDitherToggle ^= 1;
	    }
	    else
	    {
	    	la_VV->lau1ValveDitherToggle = 0;
	    }
    }
    else
    {
        if(la_VV->lau8ValveOnTime>U8_FULL_CYCLETIME)
        {
            la_VV->lau8ValveOnTime = la_VV->lau8ValveOnTime-U8_FULL_CYCLETIME;
            la_VV->lau16Current = la_VV->lau16Current*1;
            la_VV->lau16CurrentDither = la_VV->lau16CurrentDither*1;
        }
        else
        {
            la_VV->lau8ValveOnTime = 0;
            la_VV->lau16Current = 0;
            la_VV->lau16CurrentDither = 0;
        }
        la_VV->lau1ValveDitherToggle = 0;
    }

}

#define VAT_CTRL_STOP_SEC_CODE
#include "Vat_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
