/**
 * @defgroup LAVAT_DecideValvePattern LAVAT_DecideValvePattern
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAVAT_DecideValvePattern.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LAVAT_DecideValvePattern.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define U8_VALVE_ON_TIME		5
#define U8_VALVE_OFF_TIME		0
#define U16_VALVE_ACTIVE_ON		1
#define U16_VALVE_ACTIVE_OFF	0

#define U8_CUT1_VLV_INDEX		0
#define U8_CUT2_VLV_INDEX		1
#define U8_SIM_VLV_INDEX		2
#define U8_PD_VLV_INDEX			3
#define U8_BALV_VLV_INDEX		4
#define U8_RL_VLV_INDEX			5
#define U8_CIR_VLV_INDEX		6

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint16_t u16SpecialCurrent;
	uint16_t u16StepCurrent;
	uint16_t u16MaxCurrent;
	uint16_t u16MidCurrent;
	uint16_t u16MinCurrent;
}stCurrentVariety;


typedef struct
{
	uint8_t u8DitherEnableFlg;
	uint16_t u16DitherAmplitude;
	uint16_t u16DitherCnt;
	uint8_t u8DitherOnFlg;
	uint16_t u16OrgCurrent;

}stDitherInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"

#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/
#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VAT_CTRL_START_SEC_CODE
#include "Vat_MemMap.h"

FAL_STATIC	LA_VAL_PETN laVAT_CutVlv1   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_CutVlv2   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_SimVlv    = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_InFLVlv   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_InFRVlv   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_InRLVlv   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_InRRVlv   = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_BalCircV1 = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_BalCircV2 = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_BalChamV  = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_CV        = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_RLV       = {0,0,0};
FAL_STATIC	LA_VAL_PETN laVAT_PDV       = {0,0,0};

/* Valve Refactoring SW  - Begin*/
FAL_STATIC uint8_t lavatu8VlvCtrlRespLv[7] = {U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL,U8_VLV_RESPONSE_LV_NORMAL};
FAL_STATIC uint8_t lavatu8VlvMaxHoldTime[7] = {U8_T_0_MS,U8_T_0_MS,U8_T_100_MS,U8_T_0_MS, U8_T_500_MS,U8_T_500_MS,U8_T_500_MS};
FAL_STATIC uint8_t lavatu8ValveState[7] = {U8_VALVE_STATE_READY,U8_VALVE_STATE_READY,U8_VALVE_STATE_READY,U8_VALVE_STATE_READY,U8_VALVE_STATE_READY,U8_VALVE_STATE_READY,U8_VALVE_STATE_READY};

/* Valve Refactoring SW  - End*/
uint16_t lavatu16DitherPattern = 1;
uint16_t lavatu16DitherSelect = 2;

static void LAVAT_vGetMainVlvInput(void);
static void LAVAT_vSetMainVlvOuput(void);
/* Valve Refactoring SW  - Begin*/
static uint16_t LAVAT_u16CountValveActiveTime(uint8_t u8ValveCtrlState, uint16_t u16ValveActiveCnt);
static uint16_t LAVAT_u16ValvePattern(uint8_t u8ValveCtrlState, st_VALVE_CURRENT_INFO_t stValveCurrentInfo, uint16_t u16ValveCurrentOld);
static uint16_t LAVAT_u16ValveFadeOutPattern(uint16_t u16FadeoutDiffCurrent, uint16_t u16ValveCurrentOld);
static uint16_t LAVAT_u16GetValveMaxHoldCurrent(uint16_t u16ValveActiveCnt, uint16_t u16MaxHoldCnt, uint16_t u16MaxHoldCurrent, uint16_t u16FirstHoldCurrent);
static uint16_t LAVAT_u16ValveActivePattern(st_VALVE_CURRENT_INFO_t stValveCurrentInfo_r, uint16_t u16ValveMaxHoldCurrent, uint16_t u16ValveCurrentOld);
static uint16_t LAVAT_u16ValveMaintainPattern(uint16_t u16MaintainCurrent, uint16_t u16MaintainDiffCurrent,  uint16_t u16ValveCurrentOld);
static LA_VAL_PETN LAVAT_stDecideCut1VlvCurrent(LA_VAL_PETN stCut1VVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecideCut2VlvCurrent(LA_VAL_PETN stCut2VVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecideSimVlvCurrent(LA_VAL_PETN stCutVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecidePDVlvCurrent(LA_VAL_PETN stPDVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecideBalVlvCurrent(LA_VAL_PETN stBalVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecideRLVlvCurrent(LA_VAL_PETN stRLVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static LA_VAL_PETN LAVAT_stDecideCirVlvCurrent(LA_VAL_PETN stCirVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime);
static uint16_t LAVAT_u16GetRiseDiffCurrent(uint8_t u8VlvCtrlRespLv, stCurrentVariety stDiffCurrent_r);
static uint16_t LAVAT_u16GetFadeoutDiffCurrent(uint8_t u8VlvCtrlRespLv, stCurrentVariety stDiffCurrent_r);
static uint16_t LAVAT_u16DitherCurrent20ms(uint16_t u16ValveMaxHoldCurrent, uint16_t u16ValveBaseCurrent, stDitherInfo_t	* pstDitherInfo);
/* Valve Refactoring SW  - End*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAVAT_vCallVlvParttern(void)
{
	uint16_t u16FadeoutState2EndOK = FALSE;
	uint16_t u16ValveFadeoutEndOK = FALSE;
	LAVAT_vGetMainVlvInput();
	/* state machine */

	/* Valve Refactoring SW  - Begin*/
	laVAT_CutVlv1     = LAVAT_stDecideCut1VlvCurrent(laVAT_CutVlv1, lavatu8ValveState[U8_CUT1_VLV_INDEX], lavatu8VlvCtrlRespLv[U8_CUT1_VLV_INDEX], lavatu8VlvMaxHoldTime[U8_CUT1_VLV_INDEX]);
	laVAT_CutVlv2     = LAVAT_stDecideCut2VlvCurrent(laVAT_CutVlv2, lavatu8ValveState[U8_CUT2_VLV_INDEX], lavatu8VlvCtrlRespLv[U8_CUT2_VLV_INDEX], lavatu8VlvMaxHoldTime[U8_CUT2_VLV_INDEX]);
	laVAT_SimVlv      = LAVAT_stDecideSimVlvCurrent( laVAT_SimVlv,  lavatu8ValveState[U8_SIM_VLV_INDEX] , lavatu8VlvCtrlRespLv[U8_SIM_VLV_INDEX] , lavatu8VlvMaxHoldTime[U8_SIM_VLV_INDEX] );
	laVAT_PDV         = LAVAT_stDecidePDVlvCurrent(  laVAT_PDV,     lavatu8ValveState[U8_PD_VLV_INDEX]  , lavatu8VlvCtrlRespLv[U8_PD_VLV_INDEX]  , lavatu8VlvMaxHoldTime[U8_PD_VLV_INDEX]  );
	laVAT_BalChamV    = LAVAT_stDecideBalVlvCurrent( laVAT_BalChamV,lavatu8ValveState[U8_BALV_VLV_INDEX], lavatu8VlvCtrlRespLv[U8_BALV_VLV_INDEX], lavatu8VlvMaxHoldTime[U8_BALV_VLV_INDEX]);
	laVAT_RLV         = LAVAT_stDecideRLVlvCurrent(  laVAT_RLV,     lavatu8ValveState[U8_RL_VLV_INDEX]  , lavatu8VlvCtrlRespLv[U8_RL_VLV_INDEX]  , lavatu8VlvMaxHoldTime[U8_RL_VLV_INDEX]  );
	laVAT_CV          = LAVAT_stDecideCirVlvCurrent( laVAT_CV,      lavatu8ValveState[U8_CIR_VLV_INDEX] , lavatu8VlvCtrlRespLv[U8_CIR_VLV_INDEX] , lavatu8VlvMaxHoldTime[U8_CIR_VLV_INDEX] );


	u16FadeoutState2EndOK = (uint16_t)(laVAT_CutVlv1.lau1ValveFadeoutEndOKFlg & laVAT_CutVlv2.lau1ValveFadeoutEndOKFlg);
	u16ValveFadeoutEndOK= (uint16_t)( laVAT_CutVlv1.lau1ValveFadeoutEndOKFlg
									& laVAT_CutVlv2.lau1ValveFadeoutEndOKFlg
									& laVAT_SimVlv.lau1ValveFadeoutEndOKFlg
									& laVAT_PDV.lau1ValveFadeoutEndOKFlg
									& laVAT_BalChamV.lau1ValveFadeoutEndOKFlg);

	Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk = u16FadeoutState2EndOK;
	Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK = u16ValveFadeoutEndOK;
	/* Valve Refactoring SW  - End*/

	Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg = ((laVAT_CutVlv1.lau1ValveAct == FALSE) && (laVAT_CutVlv2.lau1ValveAct == FALSE)) ? TRUE : FALSE;

	/* valve act */
	LAVAT_vSetMainVlvOuput();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAVAT_vGetMainVlvInput(void)
{
	/* Temp location */
	/* Valve Refactoring SW  - Begin*/
	lavatu8ValveState[U8_CUT1_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
	lavatu8ValveState[U8_CUT2_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
	lavatu8ValveState[U8_SIM_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
	lavatu8ValveState[U8_PD_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
	lavatu8ValveState[U8_BALV_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;
	lavatu8ValveState[U8_RL_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
	lavatu8ValveState[U8_CIR_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;

	lavatu8VlvCtrlRespLv[U8_CUT1_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_CUT2_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_SIM_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_PD_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_BALV_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_RL_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
	lavatu8VlvCtrlRespLv[U8_CIR_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;

	lavatu8VlvMaxHoldTime[U8_CUT1_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_CUT2_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_SIM_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_PD_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_BALV_VLV_INDEX] = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_RL_VLV_INDEX]   = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime;
	lavatu8VlvMaxHoldTime[U8_CIR_VLV_INDEX]  = Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime;

	/* Valve Refactoring SW  - End*/

    /*==============================================================================
    * Members of structure Vat_CtrlIdbPCtrllVlvCtrlStInfo
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.FlInVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.FrInVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlInVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RrInVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PrimBalVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.SecdBalVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.ChmbBalVlvActFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.FlInVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.FrInVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlInVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RrInVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PrimBalVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.SecdBalVlvInhibFlg;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.ChmbBalVlvInhibFlg;
     =============================================================================*/

    /*==============================================================================
    * Members of structure Vat_CtrlIdbPedlFeelVlvCtrlStInfo
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvActFlg;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvActFlg;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvActFlg;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvInhibFlg;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvInhibFlg;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvInhibFlg;
     =============================================================================*/

    /*==============================================================================
    * Members of structure Vat_CtrlIbdVlvStInfo
     : Vat_CtrlIbdVlvStInfo.SimVlvSt;
     : Vat_CtrlIbdVlvStInfo.PCtrlVlvSt;
     : Vat_CtrlIbdVlvStInfo.VlvNoiseCtrlSt;
     =============================================================================*/

}

static void LAVAT_vSetMainVlvOuput(void)
{
	laVAT_Localbus.stCutVlv1   = laVAT_CutVlv1  ;
	laVAT_Localbus.stCutVlv2   = laVAT_CutVlv2  ;
	laVAT_Localbus.stSimVlv    = laVAT_SimVlv   ;
	laVAT_Localbus.stInFLVlv   = laVAT_InFLVlv  ;
	laVAT_Localbus.stInFRVlv   = laVAT_InFRVlv  ;
	laVAT_Localbus.stInRLVlv   = laVAT_InRLVlv  ;
	laVAT_Localbus.stInRRVlv   = laVAT_InRRVlv  ;
	laVAT_Localbus.stBalCircV1 = laVAT_BalCircV1;
	laVAT_Localbus.stBalCircV2 = laVAT_BalCircV2;
	laVAT_Localbus.stBalChamV  = laVAT_BalChamV ;
	laVAT_Localbus.stCircVlv   = laVAT_CV       ;
	laVAT_Localbus.stRelsVlv   = laVAT_RLV      ;
	laVAT_Localbus.stPDVlv     = laVAT_PDV      ;
}

static uint16_t LAVAT_u16CountValveActiveTime(uint8_t u8ValveCtrlState, uint16_t u16ValveActiveCnt)
{
	uint16_t u16ActiveCnt = 0;

	if ((u8ValveCtrlState == U8_VALVE_STATE_ACTIVE) || (u8ValveCtrlState == U8_VALVE_STATE_MAINTAIN))
	{
		if (u16ValveActiveCnt < U16_T_5_S)
		{
			u16ActiveCnt = u16ValveActiveCnt + U8_T_5_MS;
		}
		else
		{
			u16ActiveCnt = U16_T_5_S;
		}
	}
	else
	{
		u16ActiveCnt = U8_T_0_MS;
	}

	return u16ActiveCnt;
}

static uint16_t LAVAT_u16ValvePattern(uint8_t u8ValveCtrlState, st_VALVE_CURRENT_INFO_t stValveCurrentInfo, uint16_t u16ValveCurrentOld)
{
	uint16_t u16ValveMaxHoldCurrent = stValveCurrentInfo.u16MaxHoldCurrent;

	uint16_t u16ValveCurrent = S16_CURRENT_0_MA;

	switch(u8ValveCtrlState)
	{
	case U8_VALVE_STATE_READY:
		/* Valve Current Fadeout Control */
		u16ValveCurrent = LAVAT_u16ValveFadeOutPattern(stValveCurrentInfo.u16FadeoutDiffCurrent, u16ValveCurrentOld);
		break;

	case U8_VALVE_STATE_INITIAL:
		/* Valve Initial Current Control */
		if (u16ValveCurrentOld > stValveCurrentInfo.u16InitialCurrent)
		{
			u16ValveCurrent = u16ValveCurrentOld;
		}
		else
		{
			u16ValveCurrent = stValveCurrentInfo.u16InitialCurrent;
		}
		break;

	case U8_VALVE_STATE_ACTIVE:
		/* Valve Pattern Control */
		u16ValveMaxHoldCurrent = LAVAT_u16GetValveMaxHoldCurrent(stValveCurrentInfo.u16ValveActiveCnt, stValveCurrentInfo.u16MaxHoldTime, stValveCurrentInfo.u16MaxHoldCurrent, stValveCurrentInfo.u16FirstHoldCurrent);
		u16ValveCurrent = LAVAT_u16ValveActivePattern(stValveCurrentInfo, u16ValveMaxHoldCurrent, u16ValveCurrentOld);

		break;

	case U8_VALVE_STATE_MAINTAIN:
		/* Valve Maintain Pattern Control */
		u16ValveCurrent = LAVAT_u16ValveMaintainPattern(stValveCurrentInfo.u16MaintainCurrent, stValveCurrentInfo.u16MaintainDiffCurrent, u16ValveCurrentOld);
		break;

	default:
		break;
	}

	u16ValveCurrent = L_u16LimitMinMax(u16ValveCurrent, S16_CURRENT_0_MA, stValveCurrentInfo.u16MaxHoldCurrent);

	return u16ValveCurrent;
}

static uint16_t LAVAT_u16ValveFadeOutPattern(uint16_t u16FadeoutDiffCurrent, uint16_t u16ValveCurrentOld)
{
	uint16_t u16ValveFadeoutCurrent = S16_CURRENT_0_MA;

	if (u16ValveCurrentOld > u16FadeoutDiffCurrent)
	{
		u16ValveFadeoutCurrent = u16ValveCurrentOld - u16FadeoutDiffCurrent;
	}
	else
	{
		u16FadeoutDiffCurrent = S16_CURRENT_0_MA;
	}

	return u16ValveFadeoutCurrent;
}

static uint16_t LAVAT_u16GetValveMaxHoldCurrent(uint16_t u16ValveActiveCnt, uint16_t u16MaxHoldTime, uint16_t u16MaxHoldCurrent, uint16_t u16FirstHoldCurrent)
{
	uint16_t u16MaxCurrent = S16_CURRENT_0_MA;

	if (u16ValveActiveCnt < u16MaxHoldTime)
	{
		u16MaxCurrent = u16MaxHoldCurrent;
	}
	else
	{
		u16MaxCurrent = u16FirstHoldCurrent;
	}

	return u16MaxCurrent;
}

static uint16_t LAVAT_u16ValveActivePattern(st_VALVE_CURRENT_INFO_t stValveCurrentInfo_r, uint16_t u16ValveMaxHoldCurrent, uint16_t u16ValveCurrentOld)
{
	uint16_t u16ValveActiveCurrent = S16_CURRENT_0_MA;

	if (stValveCurrentInfo_r.u16InitialCurrent > u16ValveCurrentOld)
	{
		u16ValveActiveCurrent = stValveCurrentInfo_r.u16InitialCurrent + stValveCurrentInfo_r.u16RiseDiffCurrent;
	}
	else if (u16ValveCurrentOld - stValveCurrentInfo_r.u16RistToHoldDiffCurrent > u16ValveMaxHoldCurrent)
	{
		u16ValveActiveCurrent = u16ValveCurrentOld - stValveCurrentInfo_r.u16RistToHoldDiffCurrent;
	}
	else if (u16ValveCurrentOld + stValveCurrentInfo_r.u16RiseDiffCurrent < u16ValveMaxHoldCurrent)
	{
		u16ValveActiveCurrent = u16ValveCurrentOld + stValveCurrentInfo_r.u16RiseDiffCurrent;
	}
	else
	{
		u16ValveActiveCurrent = u16ValveMaxHoldCurrent;
	}

	return u16ValveActiveCurrent;
}

static uint16_t LAVAT_u16ValveMaintainPattern(uint16_t u16MaintainCurrent, uint16_t u16MaintainDiffCurrent,  uint16_t u16ValveCurrentOld)
{
	uint16_t u16ValveMaintainCurrent = S16_CURRENT_0_MA;

	if (u16ValveCurrentOld > u16MaintainCurrent + u16MaintainDiffCurrent)
	{
		u16ValveMaintainCurrent = u16ValveCurrentOld - u16MaintainDiffCurrent;
	}
	else
	{
		u16ValveMaintainCurrent = u16MaintainCurrent;
	}

	return u16ValveMaintainCurrent;
}

static LA_VAL_PETN LAVAT_stDecideCut1VlvCurrent(LA_VAL_PETN stCut1VVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stCut1VVInfo = 	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stCut1VVPtnOut = {0,0,0};
	static stCurrentVariety stCut1RiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stCut1FadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stCut1RiseDiffCurrent.u16SpecialCurrent = S16_CVV_CLOSE_DIFF_QUICK;
	stCut1RiseDiffCurrent.u16StepCurrent    = U16_CV_CURRENT_MAX;
	stCut1RiseDiffCurrent.u16MaxCurrent     = U16_CV_CURRENT_DIFF_MAX;
	stCut1RiseDiffCurrent.u16MidCurrent     = U16_CV_CURRENT_DIFF_MID;
	stCut1RiseDiffCurrent.u16MinCurrent     = U16_CV_CURRENT_DIFF_MIN;

	stCut1FadeoutDiffCurrent.u16SpecialCurrent = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut1FadeoutDiffCurrent.u16StepCurrent    = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut1FadeoutDiffCurrent.u16MaxCurrent     = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut1FadeoutDiffCurrent.u16MidCurrent     = U16_CV_CURRENT_OFF_DIFF_LSD;
	stCut1FadeoutDiffCurrent.u16MinCurrent     = U16_CV_CURRENT_OFF_DIFF_LSD;

	stCut1VVInfo.u16InitialCurrent			=	U16_CV_CURRENT_MIN;                                                                   /* u16InitialCurrent        */
	stCut1VVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stCut1RiseDiffCurrent);                  /* u16RiseDiffCurrent       */
	stCut1VVInfo.u16MaxHoldCurrent			=	U16_CV_CURRENT_MAX;                                                                   /* u16MaxHoldCurrent        */
	stCut1VVInfo.u16RistToHoldDiffCurrent	=	U16_CV_CURRENT_OFF_DIFF_NORMAL;                                                       /* u16RistToHoldDiffCurrent */
	stCut1VVInfo.u16FirstHoldCurrent		=	U16_CV_CURRENT_MAX;                                                                   /* u16FirstHoldCurrent      */
	stCut1VVInfo.u16MaintainCurrent			=	U16_CV_CURRENT_MID;                                                                   /* u16MaintainCurrent       */
	stCut1VVInfo.u16MaintainDiffCurrent		=	U16_CV_CURRENT_OFF_DIFF_NORMAL;                                                       /* u16MaintainDiffCurrent   */
	stCut1VVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stCut1FadeoutDiffCurrent);            /* u16FadeoutDiffCurrent    */
	stCut1VVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                     /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */
	if (stCut1VVPtn.lau16Current <= S16_CURRENT_200_MA)
	{
		stCut1VVInfo.u16FadeoutDiffCurrent	=	S16_CURRENT_200_MA;           /* Cut Valve Current Step Off Under 200mA    */
	}
	else { }

	stCut1VVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stCut1VVInfo.u16ValveActiveCnt);
	stCut1VVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stCut1VVInfo, stCut1VVPtn.lau16Current );
	stCut1VVPtnOut.lau8ValveOnTime = (stCut1VVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stCut1VVPtnOut.lau1ValveAct = (stCut1VVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stCut1VVPtnOut.lau1ValveFadeoutEndOKFlg = (stCut1VVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stCut1VVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecideCut2VlvCurrent(LA_VAL_PETN stCut2VVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stCut2VVInfo = 	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stCut2VVPtnOut = {0,0,0};
	static stCurrentVariety stCut2RiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stCut2FadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stCut2RiseDiffCurrent.u16SpecialCurrent = S16_CVV_CLOSE_DIFF_QUICK;
	stCut2RiseDiffCurrent.u16StepCurrent    = U16_CV_CURRENT_MAX;
	stCut2RiseDiffCurrent.u16MaxCurrent     = U16_CV_CURRENT_DIFF_MAX;
	stCut2RiseDiffCurrent.u16MidCurrent     = U16_CV_CURRENT_DIFF_MID;
	stCut2RiseDiffCurrent.u16MinCurrent     = U16_CV_CURRENT_DIFF_MIN;

	stCut2FadeoutDiffCurrent.u16SpecialCurrent = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut2FadeoutDiffCurrent.u16StepCurrent    = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut2FadeoutDiffCurrent.u16MaxCurrent     = U16_CV_CURRENT_OFF_DIFF_NORMAL;
	stCut2FadeoutDiffCurrent.u16MidCurrent     = U16_CV_CURRENT_OFF_DIFF_LSD;
	stCut2FadeoutDiffCurrent.u16MinCurrent     = U16_CV_CURRENT_OFF_DIFF_LSD;

	stCut2VVInfo.u16InitialCurrent			=	U16_CV_CURRENT_MIN;                                                                   /* u16InitialCurrent        */
	stCut2VVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stCut2RiseDiffCurrent);                  /* u16RiseDiffCurrent       */
	stCut2VVInfo.u16MaxHoldCurrent			=	U16_CV_CURRENT_MAX;                                                                   /* u16MaxHoldCurrent        */
	stCut2VVInfo.u16RistToHoldDiffCurrent	=	U16_CV_CURRENT_OFF_DIFF_NORMAL;                                                       /* u16RistToHoldDiffCurrent */
	stCut2VVInfo.u16FirstHoldCurrent		=	U16_CV_CURRENT_MAX;                                                                   /* u16FirstHoldCurrent      */
	stCut2VVInfo.u16MaintainCurrent			=	U16_CV_CURRENT_MID;                                                                   /* u16MaintainCurrent       */
	stCut2VVInfo.u16MaintainDiffCurrent		=	U16_CV_CURRENT_OFF_DIFF_NORMAL;                                                       /* u16MaintainDiffCurrent   */
	stCut2VVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stCut2FadeoutDiffCurrent);            /* u16FadeoutDiffCurrent    */
	stCut2VVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                     /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */
	if (stCut2VVPtn.lau16Current <= S16_CURRENT_200_MA)
	{
		stCut2VVInfo.u16FadeoutDiffCurrent	=	S16_CURRENT_200_MA;           /* Cut Valve Current Step Off Under 200mA    */
	}
	else { }

	stCut2VVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stCut2VVInfo.u16ValveActiveCnt);
	stCut2VVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stCut2VVInfo, stCut2VVPtn.lau16Current );
	stCut2VVPtnOut.lau8ValveOnTime = (stCut2VVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stCut2VVPtnOut.lau1ValveAct = (stCut2VVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stCut2VVPtnOut.lau1ValveFadeoutEndOKFlg = (stCut2VVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stCut2VVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecideSimVlvCurrent(LA_VAL_PETN stSimVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stSimVVInfo = 	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stSimVVPtnOut = {0,0,0};
	static stCurrentVariety stSimRiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stSimFadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stSimRiseDiffCurrent.u16SpecialCurrent = U16_SIMV_CURRENT_MAX;
	stSimRiseDiffCurrent.u16StepCurrent    = U16_SIMV_CURRENT_MAX;
	stSimRiseDiffCurrent.u16MaxCurrent     = U16_SIMV_CURRENT_DIFF_MAX;
	stSimRiseDiffCurrent.u16MidCurrent     = U16_SIMV_CURRENT_DIFF_MAX;
	stSimRiseDiffCurrent.u16MinCurrent     = U16_SIMV_CURRENT_DIFF_MAX;

	stSimFadeoutDiffCurrent.u16SpecialCurrent = U16_SIM_CURRENT_OFF_DIFF_NORMAL;
	stSimFadeoutDiffCurrent.u16StepCurrent    = U16_SIMV_CURRENT_MAX;
	stSimFadeoutDiffCurrent.u16MaxCurrent     = U16_SIM_CURRENT_OFF_DIFF_NORMAL;
	stSimFadeoutDiffCurrent.u16MidCurrent     = U16_SIM_CURRENT_OFF_DIFF_LSD;
	stSimFadeoutDiffCurrent.u16MinCurrent     = U16_SIM_CURRENT_OFF_DIFF_LSD;

	stSimVVInfo.u16InitialCurrent			=	U16_SIMV_CURRENT_MIN;                                                                     /* u16InitialCurrent        */
	stSimVVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stSimRiseDiffCurrent);                       /* u16RiseDiffCurrent       */
	stSimVVInfo.u16MaxHoldCurrent			=	U16_SIMV_CURRENT_MAX;                                                                     /* u16MaxHoldCurrent        */
	stSimVVInfo.u16RistToHoldDiffCurrent	=	U16_SIMV_CURRENT_DIFF_MIN;                                                                /* u16RistToHoldDiffCurrent */
	stSimVVInfo.u16FirstHoldCurrent			=	U16_SIMV_CURRENT_MAX_AFTER_OPEN;                                                          /* u16FirstHoldCurrent      */
	stSimVVInfo.u16MaintainCurrent			=	U16_SIMV_CURRENT_MID;                                                                     /* u16MaintainCurrent       */
	stSimVVInfo.u16MaintainDiffCurrent		=	U16_SIM_CURRENT_OFF_DIFF_NORMAL;                                                          /* u16MaintainDiffCurrent   */
	stSimVVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stSimFadeoutDiffCurrent);                /* u16FadeoutDiffCurrent    */
	stSimVVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                         /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */

	stSimVVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stSimVVInfo.u16ValveActiveCnt);
	stSimVVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stSimVVInfo, stSimVVPtn.lau16Current );
	stSimVVPtnOut.lau8ValveOnTime = (stSimVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stSimVVPtnOut.lau1ValveAct = (stSimVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stSimVVPtnOut.lau1ValveFadeoutEndOKFlg = (stSimVVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stSimVVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecidePDVlvCurrent(LA_VAL_PETN stPDVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stPDVVInfo = 	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stPDVVPtnOut = {0,0,0};
	static stCurrentVariety stPDRiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stPDFadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stPDRiseDiffCurrent.u16SpecialCurrent = U16_PDV_CURRENT_ON_DIFF_MAX;
	stPDRiseDiffCurrent.u16StepCurrent    = U16_PDV_CURRENT_MAX_HIGH_P;
	stPDRiseDiffCurrent.u16MaxCurrent     = U16_PDV_CURRENT_ON_DIFF_MAX;
	stPDRiseDiffCurrent.u16MidCurrent     = U16_PDV_CURRENT_ON_DIFF_MID;
	stPDRiseDiffCurrent.u16MinCurrent     = U16_PDV_CURRENT_ON_DIFF_MIN;

	stPDFadeoutDiffCurrent.u16SpecialCurrent = U16_PDV_CURRENT_MAX_HIGH_P;
	stPDFadeoutDiffCurrent.u16StepCurrent    = U16_PDV_CURRENT_MAX_HIGH_P;
	stPDFadeoutDiffCurrent.u16MaxCurrent     = U16_PDV_CURRENT_OFF_DIFF_MAX;
	stPDFadeoutDiffCurrent.u16MidCurrent     = U16_PDV_CURRENT_OFF_DIFF_MID;
	stPDFadeoutDiffCurrent.u16MinCurrent     = U16_PDV_CURRENT_OFF_DIFF_MIN;

	stPDVVInfo.u16InitialCurrent			=	U16_PDV_CURRENT_INITIAL;                                                                 /* u16InitialCurrent        */
	stPDVVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stPDRiseDiffCurrent);                       /* u16RiseDiffCurrent       */
	stPDVVInfo.u16MaxHoldCurrent			=	U16_PDV_CURRENT_MAX_HIGH_P;                                                              /* u16MaxHoldCurrent        */
	stPDVVInfo.u16RistToHoldDiffCurrent		=	U16_PDV_CURRENT_OFF_DIFF_MAX;                                                            /* u16RistToHoldDiffCurrent */
	stPDVVInfo.u16FirstHoldCurrent			=	U16_PDV_CURRENT_MAX_HIGH_P;                                                              /* u16FirstHoldCurrent      */
	stPDVVInfo.u16MaintainCurrent			=	U16_PDV_CURRENT_MAX_HIGH_P;                                                              /* u16MaintainCurrent       */
	stPDVVInfo.u16MaintainDiffCurrent		=	U16_PDV_CURRENT_OFF_DIFF_MID;                                                         /* u16MaintainDiffCurrent   */
	stPDVVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stPDFadeoutDiffCurrent);                /* u16FadeoutDiffCurrent    */
	stPDVVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                        /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */

	stPDVVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stPDVVInfo.u16ValveActiveCnt);
	stPDVVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stPDVVInfo, stPDVVPtn.lau16Current);
	stPDVVPtnOut.lau8ValveOnTime = (stPDVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stPDVVPtnOut.lau1ValveAct = (stPDVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stPDVVPtnOut.lau1ValveFadeoutEndOKFlg = (stPDVVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stPDVVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecideBalVlvCurrent(LA_VAL_PETN stBalVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stBalVVInfo =	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stBalVVPtnOut = {0,0,0};
	static stCurrentVariety stBalVRiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stBalVFadeoutDiffCurrent = {0,0,0,0,0};
	/*static stDitherInfo_t	stDitherInfo = {0,0,0,0,0};*/
	/* Variable Initialize */
	stBalVRiseDiffCurrent.u16SpecialCurrent = U16_BALV_CURRENT_ON_DIFF_MAX;
	stBalVRiseDiffCurrent.u16StepCurrent    = U16_BALV_CURRENT_MAX_HIGH_P;
	stBalVRiseDiffCurrent.u16MaxCurrent     = U16_BALV_CURRENT_ON_DIFF_MAX;
	stBalVRiseDiffCurrent.u16MidCurrent     = U16_BALV_CURRENT_ON_DIFF_MID;
	stBalVRiseDiffCurrent.u16MinCurrent     = U16_BALV_CURRENT_ON_DIFF_MIN;

	stBalVFadeoutDiffCurrent.u16SpecialCurrent = U16_BALV_CURRENT_MAX_HIGH_P;
	stBalVFadeoutDiffCurrent.u16StepCurrent    = U16_BALV_CURRENT_MAX_HIGH_P;
	stBalVFadeoutDiffCurrent.u16MaxCurrent     = U16_BALV_CURRENT_OFF_DIFF_MAX;
	stBalVFadeoutDiffCurrent.u16MidCurrent     = U16_BALV_CURRENT_OFF_DIFF_MID;
	stBalVFadeoutDiffCurrent.u16MinCurrent     = U16_BALV_CURRENT_OFF_DIFF_MIN;

	stBalVVInfo.u16InitialCurrent			=	U16_BALV_CURRENT_INITIAL;                                                                  /* u16InitialCurrent        */
	stBalVVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stBalVRiseDiffCurrent);                       /* u16RiseDiffCurrent       */
	stBalVVInfo.u16MaxHoldCurrent			=	U16_BALV_CURRENT_MAX_HIGH_P;                                                               /* u16MaxHoldCurrent        */
	stBalVVInfo.u16RistToHoldDiffCurrent	=	U16_BALV_CURRENT_OFF_DIFF_MIN;                                                             /* u16RistToHoldDiffCurrent */
	stBalVVInfo.u16FirstHoldCurrent			=	U16_BALV_CURRENT_HOLD;                                                                     /* u16FirstHoldCurrent      */
	stBalVVInfo.u16MaintainCurrent			=	U16_BALV_CURRENT_HOLD;                                                                     /* u16MaintainCurrent       */
	stBalVVInfo.u16MaintainDiffCurrent		=	U16_BALV_CURRENT_OFF_DIFF_MIN;                                                             /* u16MaintainDiffCurrent   */
	stBalVVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stBalVFadeoutDiffCurrent);                 /* u16FadeoutDiffCurrent    */
	stBalVVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                          /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */

	stBalVVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stBalVVInfo.u16ValveActiveCnt);
	stBalVVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stBalVVInfo, stBalVVPtn.lau16Current);

	stBalVVPtnOut.lau8ValveOnTime = (stBalVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stBalVVPtnOut.lau1ValveAct = (stBalVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stBalVVPtnOut.lau1ValveFadeoutEndOKFlg = (stBalVVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stBalVVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecideRLVlvCurrent(LA_VAL_PETN stRLVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stRLVVInfo =		{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stRLVVPtnOut = {0,0,0};
	static stCurrentVariety stRLVRiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stRLVFadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stRLVRiseDiffCurrent.u16SpecialCurrent = U16_RLV_CURRENT_ON_DIFF_MAX;
	stRLVRiseDiffCurrent.u16StepCurrent    = U16_RLV_CURRENT_MAX_HIGH_P;
	stRLVRiseDiffCurrent.u16MaxCurrent     = U16_RLV_CURRENT_ON_DIFF_MAX;
	stRLVRiseDiffCurrent.u16MidCurrent     = U16_RLV_CURRENT_ON_DIFF_MID;
	stRLVRiseDiffCurrent.u16MinCurrent     = U16_RLV_CURRENT_ON_DIFF_MIN;

	stRLVFadeoutDiffCurrent.u16SpecialCurrent = U16_RLV_CURRENT_MAX_HIGH_P;
	stRLVFadeoutDiffCurrent.u16StepCurrent    = U16_RLV_CURRENT_MAX_HIGH_P;
	stRLVFadeoutDiffCurrent.u16MaxCurrent     = U16_RLV_CURRENT_OFF_DIFF_MAX;
	stRLVFadeoutDiffCurrent.u16MidCurrent     = U16_RLV_CURRENT_OFF_DIFF_MID;
	stRLVFadeoutDiffCurrent.u16MinCurrent     = U16_RLV_CURRENT_OFF_DIFF_MIN;

	stRLVVInfo.u16InitialCurrent			=	U16_RLV_CURRENT_INITIAL;                                                                  /* u16InitialCurrent        */
	stRLVVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stRLVRiseDiffCurrent);                      /* u16RiseDiffCurrent       */
	stRLVVInfo.u16MaxHoldCurrent			=	U16_RLV_CURRENT_MAX_HIGH_P;                                                               /* u16MaxHoldCurrent        */
	stRLVVInfo.u16RistToHoldDiffCurrent		=	U16_RLV_CURRENT_OFF_DIFF_MIN;                                                             /* u16RistToHoldDiffCurrent */
	stRLVVInfo.u16FirstHoldCurrent			=	U16_RLV_CURRENT_HOLD;                                                                     /* u16FirstHoldCurrent      */
	stRLVVInfo.u16MaintainCurrent			=	U16_RLV_CURRENT_HOLD;                                                                     /* u16MaintainCurrent       */
	stRLVVInfo.u16MaintainDiffCurrent		=	U16_RLV_CURRENT_OFF_DIFF_MIN;                                                             /* u16MaintainDiffCurrent   */
	stRLVVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stRLVFadeoutDiffCurrent);                /* u16FadeoutDiffCurrent    */
	stRLVVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                         /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */

	stRLVVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stRLVVInfo.u16ValveActiveCnt);
	stRLVVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stRLVVInfo, stRLVVPtn.lau16Current);
	stRLVVPtnOut.lau8ValveOnTime = (stRLVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stRLVVPtnOut.lau1ValveAct = (stRLVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stRLVVPtnOut.lau1ValveFadeoutEndOKFlg = (stRLVVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stRLVVPtnOut;
}

static LA_VAL_PETN LAVAT_stDecideCirVlvCurrent(LA_VAL_PETN stCirVVPtn, uint8_t u8ValveState, uint8_t u8VlvCtrlRespLv, uint8_t u8VlvMaxHoldTime)
{
	static st_VALVE_CURRENT_INFO_t stCirVVInfo =	{0,0,0,0,0,0,0,0,0,0};
	LA_VAL_PETN stCirVVPtnOut = {0,0,0};
	static stCurrentVariety stCVRiseDiffCurrent = {0,0,0,0,0};
	static stCurrentVariety stCVFadeoutDiffCurrent = {0,0,0,0,0};

	/* Variable Initialize */
	stCVRiseDiffCurrent.u16SpecialCurrent = U16_CIRV_CURRENT_ON_DIFF_MAX;
	stCVRiseDiffCurrent.u16StepCurrent    = U16_CIRV_CURRENT_MAX_HIGH_P;
	stCVRiseDiffCurrent.u16MaxCurrent     = U16_CIRV_CURRENT_ON_DIFF_MAX;
	stCVRiseDiffCurrent.u16MidCurrent     = U16_CIRV_CURRENT_ON_DIFF_MID;
	stCVRiseDiffCurrent.u16MinCurrent     = U16_CIRV_CURRENT_ON_DIFF_MIN;

	stCVFadeoutDiffCurrent.u16SpecialCurrent = U16_CIRV_CURRENT_MAX_HIGH_P;
	stCVFadeoutDiffCurrent.u16StepCurrent    = U16_CIRV_CURRENT_MAX_HIGH_P;
	stCVFadeoutDiffCurrent.u16MaxCurrent     = U16_CIRV_CURRENT_OFF_DIFF_MAX;
	stCVFadeoutDiffCurrent.u16MidCurrent     = U16_CIRV_CURRENT_OFF_DIFF_MID;
	stCVFadeoutDiffCurrent.u16MinCurrent     = U16_CIRV_CURRENT_OFF_DIFF_MIN;

	stCirVVInfo.u16InitialCurrent			=	U16_CIRV_CURRENT_INITIAL;                                                              /* u16InitialCurrent        */
	stCirVVInfo.u16RiseDiffCurrent			=	LAVAT_u16GetRiseDiffCurrent(u8VlvCtrlRespLv, stCVRiseDiffCurrent);                     /* u16RiseDiffCurrent       */
	stCirVVInfo.u16MaxHoldCurrent			=	U16_CIRV_CURRENT_MAX_HIGH_P;                                                           /* u16MaxHoldCurrent        */
	stCirVVInfo.u16RistToHoldDiffCurrent	=	U16_CIRV_CURRENT_OFF_DIFF_MIN;                                                         /* u16RistToHoldDiffCurrent */
	stCirVVInfo.u16FirstHoldCurrent			=	U16_CIRV_CURRENT_HOLD;                                                                 /* u16FirstHoldCurrent      */
	stCirVVInfo.u16MaintainCurrent			=	U16_CIRV_CURRENT_HOLD;                                                                 /* u16MaintainCurrent       */
	stCirVVInfo.u16MaintainDiffCurrent		=	U16_CIRV_CURRENT_OFF_DIFF_MIN;                                                         /* u16MaintainDiffCurrent   */
	stCirVVInfo.u16FadeoutDiffCurrent		=	LAVAT_u16GetFadeoutDiffCurrent(u8VlvCtrlRespLv, stCVFadeoutDiffCurrent);               /* u16FadeoutDiffCurrent    */
	stCirVVInfo.u16MaxHoldTime				=	u8VlvMaxHoldTime;                                                                      /* u16MaxHoldTime           */

	/* Replace to the Valve Information Decide Function */

	stCirVVInfo.u16ValveActiveCnt = LAVAT_u16CountValveActiveTime(u8ValveState, stCirVVInfo.u16ValveActiveCnt);
	stCirVVPtnOut.lau16Current = LAVAT_u16ValvePattern(u8ValveState, stCirVVInfo, stCirVVPtn.lau16Current);
	stCirVVPtnOut.lau8ValveOnTime = (stCirVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U8_VALVE_ON_TIME : U8_VALVE_OFF_TIME;
	stCirVVPtnOut.lau1ValveAct = (stCirVVPtnOut.lau16Current > S16_CURRENT_0_MA) ? U16_VALVE_ACTIVE_ON : U16_VALVE_ACTIVE_OFF;
	stCirVVPtnOut.lau1ValveFadeoutEndOKFlg = (stCirVVPtnOut.lau16Current == S16_CURRENT_0_MA) ? TRUE : FALSE;

	return stCirVVPtnOut;
}

static uint16_t LAVAT_u16GetRiseDiffCurrent(uint8_t u8VlvCtrlRespLv, stCurrentVariety stDiffCurrent_r)
{
	uint16_t u16RiseDiffCurrent = S16_CURRENT_100_MA;

	if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_SPECIAL)
	{
		u16RiseDiffCurrent = stDiffCurrent_r.u16SpecialCurrent;
	}
	else if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_QUICK)
	{
		u16RiseDiffCurrent = stDiffCurrent_r.u16StepCurrent;
	}
	else if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_NORMAL)
	{
		u16RiseDiffCurrent = stDiffCurrent_r.u16MaxCurrent;
	}
	else if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_MID)
	{
		u16RiseDiffCurrent = stDiffCurrent_r.u16MidCurrent;
	}
	else /*if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_SLOW)*/
	{
		u16RiseDiffCurrent = stDiffCurrent_r.u16MinCurrent;
	}

	return u16RiseDiffCurrent;
}

static uint16_t LAVAT_u16GetFadeoutDiffCurrent(uint8_t u8VlvCtrlRespLv, stCurrentVariety stDiffCurrent_r)
{
	uint16_t u16FadeoutDiffCurrent = S16_CURRENT_100_MA;

	if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_QUICK)
	{
		u16FadeoutDiffCurrent = stDiffCurrent_r.u16StepCurrent;
	}
	else if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_NORMAL)
	{
		u16FadeoutDiffCurrent = stDiffCurrent_r.u16MaxCurrent;
	}
	else if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_MID)
		{
			u16FadeoutDiffCurrent = stDiffCurrent_r.u16MidCurrent;
		}
	else /*if (u8VlvCtrlRespLv == U8_VLV_RESPONSE_LV_SLOW)*/
	{
		u16FadeoutDiffCurrent = stDiffCurrent_r.u16MinCurrent;
	}

	return u16FadeoutDiffCurrent;
}
#if DISABLE
static uint16_t LAVAT_u16DitherCurrent20ms(uint16_t u16ValveMaxHoldCurrent, uint16_t u16ValveBaseCurrent, stDitherInfo_t	* pstDitherInfo)
{
	uint16_t u16ValveCurrentDitherAdded = 0;


	if(pstDitherInfo->u8DitherOnFlg == FALSE)
	{
		if(u16ValveBaseCurrent < u16ValveMaxHoldCurrent)
		{
			pstDitherInfo->u8DitherOnFlg = TRUE;
		}
	}
	else
	{
		if(u16ValveBaseCurrent >= u16ValveMaxHoldCurrent)
		{
			pstDitherInfo->u8DitherOnFlg = FALSE;
		}
	}

	if(pstDitherInfo->u8DitherOnFlg == TRUE)
	{
		if(lavatu16DitherPattern == TRUE)
		{
			if (lavatu16DitherSelect == 1)
			{
				pstDitherInfo->u16DitherCnt = (pstDitherInfo->u16DitherCnt + 1) % 4;

				if(pstDitherInfo->u16DitherCnt < 2)
				{
					u16ValveCurrentDitherAdded = u16ValveBaseCurrent + pstDitherInfo->u16DitherAmplitude;
				}
				else /*if(pstDitherInfo->u16DitherCnt < 3)*/
				{
					u16ValveCurrentDitherAdded = u16ValveBaseCurrent - pstDitherInfo->u16DitherAmplitude;
				}
			}
			else
			{

				if(pstDitherInfo->u16DitherCnt < 2)
				{
					u16ValveCurrentDitherAdded = u16ValveBaseCurrent - pstDitherInfo->u16DitherAmplitude;
				}
				else if (pstDitherInfo->u16DitherCnt < 6)
				{
					u16ValveCurrentDitherAdded = u16ValveBaseCurrent + pstDitherInfo->u16DitherAmplitude;
				}
				else /*if(pstDitherInfo->u16DitherCnt < 8)*/
				{
					u16ValveCurrentDitherAdded = u16ValveBaseCurrent - pstDitherInfo->u16DitherAmplitude;
				}
				pstDitherInfo->u16DitherCnt = (pstDitherInfo->u16DitherCnt + 1) % 8;
			}
		}
		else
		{
			if(pstDitherInfo->u16DitherCnt == 0)
			{
				u16ValveCurrentDitherAdded = u16ValveBaseCurrent + pstDitherInfo->u16DitherAmplitude;
			}
			else if(pstDitherInfo->u16DitherCnt == 1)
			{
				u16ValveCurrentDitherAdded = u16ValveBaseCurrent - pstDitherInfo->u16DitherAmplitude;
			}
			else
			{
				u16ValveCurrentDitherAdded = u16ValveBaseCurrent;
			}
			pstDitherInfo->u16DitherCnt = (pstDitherInfo->u16DitherCnt + 1) % 2;
		}

	}
	else
	{
		pstDitherInfo->u16DitherCnt = 0;
		u16ValveCurrentDitherAdded = u16ValveBaseCurrent;
	}

	return u16ValveCurrentDitherAdded;
}
#endif

#define VAT_CTRL_STOP_SEC_CODE
#include "Vat_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
