/**
 * @defgroup Vat_Ctrl Vat_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vat_Ctrl.h"
#include "Vat_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LAVAT_DecideValveActuation.h"
#include "LAVAT_DecideValvePattern.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VAT_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Vat_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Vat_Ctrl_HdrBusType Vat_CtrlBus;

/* Internal Logic Bus */
Vat_Ctrl_Local laVAT_Localbus;

/* Version Info */
const SwcVersionInfo_t Vat_CtrlVersionInfo = 
{   
    VAT_CTRL_MODULE_ID,           /* Vat_CtrlVersionInfo.ModuleId */
    VAT_CTRL_MAJOR_VERSION,       /* Vat_CtrlVersionInfo.MajorVer */
    VAT_CTRL_MINOR_VERSION,       /* Vat_CtrlVersionInfo.MinorVer */
    VAT_CTRL_PATCH_VERSION,       /* Vat_CtrlVersionInfo.PatchVer */
    VAT_CTRL_BRANCH_VERSION       /* Vat_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Arb_CtrlWhlOutVlvCtrlTarInfo_t Vat_CtrlWhlOutVlvCtrlTarInfo;
Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Vat_CtrlIdbPCtrllVlvCtrlStInfo;
Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Vat_CtrlIdbPedlFeelVlvCtrlStInfo;
Pct_5msCtrlWhlInVlvCtrlTarInfo_t Vat_CtrlWhlnVlvCtrlTarInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Vat_CtrlStkRecvryActnIfInfo;
Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
Mom_HndlrEcuModeSts_t Vat_CtrlEcuModeSts;
Pct_5msCtrlPCtrlAct_t Vat_CtrlPCtrlAct;
Eem_SuspcDetnFuncInhibitVlvActSts_t Vat_CtrlFuncInhibitVlvActSts;
Pct_5msCtrlPCtrlBoostMod_t Vat_CtrlPCtrlBoostMod;

/* Output Data Element */
Vat_CtrlNormVlvReqVlvActInfo_t Vat_CtrlNormVlvReqVlvActInfo;
Vat_CtrlWhlVlvReqIdbInfo_t Vat_CtrlWhlVlvReqIdbInfo;
Vat_CtrlIdbBalVlvReqInfo_t Vat_CtrlIdbBalVlvReqInfo;
Vat_CtrlIdbVlvActInfo_t Vat_CtrlIdbVlvActInfo;

uint32 Vat_Ctrl_Timer_Start;
uint32 Vat_Ctrl_Timer_Elapsed;

#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VAT_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VAT_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Vat_MemMap.h"
#define VAT_CTRL_START_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/** Variable Section (32BIT)**/


#define VAT_CTRL_STOP_SEC_VAR_32BIT
#include "Vat_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VAT_CTRL_START_SEC_CODE
#include "Vat_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vat_Ctrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i] = 0;   
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime = 0;
    Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen = 0;
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[i] = 0;   
    Vat_CtrlBus.Vat_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = 0;
    Vat_CtrlBus.Vat_CtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = 0;
    Vat_CtrlBus.Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = 0;
    Vat_CtrlBus.Vat_CtrlEcuModeSts = 0;
    Vat_CtrlBus.Vat_CtrlPCtrlAct = 0;
    Vat_CtrlBus.Vat_CtrlFuncInhibitVlvActSts = 0;
    Vat_CtrlBus.Vat_CtrlPCtrlBoostMod = 0;
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[i] = 0;   
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvDataLen = 0;
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[i] = 0;   
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq = 0;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen = 0;
    Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK = 0;
    Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk = 0;
    Vat_CtrlBus.Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg = 0;
}

void Vat_Ctrl(void)
{
    uint16 i;
    
    Vat_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Vat_Ctrl_Read_Vat_CtrlWhlOutVlvCtrlTarInfo(&Vat_CtrlBus.Vat_CtrlWhlOutVlvCtrlTarInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlWhlOutVlvCtrlTarInfo 
     : Vat_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar;
     : Vat_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar;
     =============================================================================*/
    
    Vat_Ctrl_Read_Vat_CtrlIdbPCtrllVlvCtrlStInfo(&Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlIdbPCtrllVlvCtrlStInfo 
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime;
     : Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime;
     =============================================================================*/
    
    Vat_Ctrl_Read_Vat_CtrlIdbPedlFeelVlvCtrlStInfo(&Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlIdbPedlFeelVlvCtrlStInfo 
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime;
     : Vat_CtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen;
     =============================================================================*/
    
    Vat_Ctrl_Read_Vat_CtrlWhlnVlvCtrlTarInfo(&Vat_CtrlBus.Vat_CtrlWhlnVlvCtrlTarInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlWhlnVlvCtrlTarInfo 
     : Vat_CtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar;
     : Vat_CtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Vat_Ctrl_Read_Vat_CtrlStkRecvryActnIfInfo_StkRecvryCtrlState(&Vat_CtrlBus.Vat_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState);
    Vat_Ctrl_Read_Vat_CtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(&Vat_CtrlBus.Vat_CtrlStkRecvryActnIfInfo.StkRecvryRequestedTime);

    Vat_Ctrl_Read_Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo(&Vat_CtrlBus.Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo 
     : Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
     =============================================================================*/
    
    Vat_Ctrl_Read_Vat_CtrlEcuModeSts(&Vat_CtrlBus.Vat_CtrlEcuModeSts);
    Vat_Ctrl_Read_Vat_CtrlPCtrlAct(&Vat_CtrlBus.Vat_CtrlPCtrlAct);
    Vat_Ctrl_Read_Vat_CtrlFuncInhibitVlvActSts(&Vat_CtrlBus.Vat_CtrlFuncInhibitVlvActSts);
    Vat_Ctrl_Read_Vat_CtrlPCtrlBoostMod(&Vat_CtrlBus.Vat_CtrlPCtrlBoostMod);

    /* Process */

    LAVAT_vCallVlvParttern();
	LAVAT_vCallVlvActn();

    /* Output */
    Vat_Ctrl_Write_Vat_CtrlNormVlvReqVlvActInfo(&Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlNormVlvReqVlvActInfo 
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData;
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq;
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.SimVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.CircVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.RelsVlvDataLen;
     : Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvDataLen;
     =============================================================================*/
    
    Vat_Ctrl_Write_Vat_CtrlWhlVlvReqIdbInfo(&Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlWhlVlvReqIdbInfo 
     : Vat_CtrlWhlVlvReqIdbInfo.FlIvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.FrIvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.RlIvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.RrIvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.FlOvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.FrOvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.RlOvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.RrOvReq;
     : Vat_CtrlWhlVlvReqIdbInfo.FlIvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.FrIvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.RlIvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.RrIvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.FlOvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.FrOvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.RlOvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.RrOvDataLen;
     : Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.FlOvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.FrOvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.RlOvReqData;
     : Vat_CtrlWhlVlvReqIdbInfo.RrOvReqData;
     =============================================================================*/
    
    Vat_Ctrl_Write_Vat_CtrlIdbBalVlvReqInfo(&Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlIdbBalVlvReqInfo 
     : Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReqData;
     : Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReqData;
     : Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData;
     : Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq;
     : Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq;
     : Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq;
     : Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvDataLen;
     : Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvDataLen;
     : Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvDataLen;
     =============================================================================*/
    
    Vat_Ctrl_Write_Vat_CtrlIdbVlvActInfo(&Vat_CtrlBus.Vat_CtrlIdbVlvActInfo);
    /*==============================================================================
    * Members of structure Vat_CtrlIdbVlvActInfo 
     : Vat_CtrlIdbVlvActInfo.VlvFadeoutEndOK;
     : Vat_CtrlIdbVlvActInfo.FadeoutSt2EndOk;
     : Vat_CtrlIdbVlvActInfo.CutVlvOpenFlg;
     =============================================================================*/
    

    Vat_Ctrl_Timer_Elapsed = STM0_TIM0.U - Vat_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VAT_CTRL_STOP_SEC_CODE
#include "Vat_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
