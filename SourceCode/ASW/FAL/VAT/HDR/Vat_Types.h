/**
 * @defgroup Vat_Types Vat_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VAT_TYPES_H_
#define VAT_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Arb_CtrlWhlOutVlvCtrlTarInfo_t Vat_CtrlWhlOutVlvCtrlTarInfo;
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Vat_CtrlIdbPCtrllVlvCtrlStInfo;
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Vat_CtrlIdbPedlFeelVlvCtrlStInfo;
    Pct_5msCtrlWhlInVlvCtrlTarInfo_t Vat_CtrlWhlnVlvCtrlTarInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Vat_CtrlStkRecvryActnIfInfo;
    Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
    Mom_HndlrEcuModeSts_t Vat_CtrlEcuModeSts;
    Pct_5msCtrlPCtrlAct_t Vat_CtrlPCtrlAct;
    Eem_SuspcDetnFuncInhibitVlvActSts_t Vat_CtrlFuncInhibitVlvActSts;
    Pct_5msCtrlPCtrlBoostMod_t Vat_CtrlPCtrlBoostMod;

/* Output Data Element */
    Vat_CtrlNormVlvReqVlvActInfo_t Vat_CtrlNormVlvReqVlvActInfo;
    Vat_CtrlWhlVlvReqIdbInfo_t Vat_CtrlWhlVlvReqIdbInfo;
    Vat_CtrlIdbBalVlvReqInfo_t Vat_CtrlIdbBalVlvReqInfo;
    Vat_CtrlIdbVlvActInfo_t Vat_CtrlIdbVlvActInfo;
}Vat_Ctrl_HdrBusType;

typedef struct
{
    uint8_t  lau8ValveOnTime;
    uint16_t lau16Current;
    uint16_t lau1ValveAct:  1;
    uint32_t lau1ValveFadeoutEndOKFlg:  1;
}LA_VAL_PETN;

typedef struct
{
	LA_VAL_PETN stCutVlv1;
	LA_VAL_PETN stCutVlv2;
	LA_VAL_PETN stSimVlv;
	LA_VAL_PETN stInFLVlv;
	LA_VAL_PETN stInFRVlv;
	LA_VAL_PETN stInRLVlv;
	LA_VAL_PETN stInRRVlv;
	LA_VAL_PETN stBalCircV1;
	LA_VAL_PETN stBalCircV2;
	LA_VAL_PETN stBalChamV;
	LA_VAL_PETN stCircVlv;
	LA_VAL_PETN stRelsVlv;
	LA_VAL_PETN stPDVlv;
}Vat_Ctrl_Local;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VAT_TYPES_H_ */
/** @} */
