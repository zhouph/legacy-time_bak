/**
 * @defgroup Vat_Ctrl Vat_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vat_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VAT_CTRL_H_
#define VAT_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vat_Types.h"
#include "Vat_Cfg.h"
#include "Vat_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define VAT_CTRL_MODULE_ID      (0)
 #define VAT_CTRL_MAJOR_VERSION  (2)
 #define VAT_CTRL_MINOR_VERSION  (0)
 #define VAT_CTRL_PATCH_VERSION  (0)
 #define VAT_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Vat_Ctrl_HdrBusType Vat_CtrlBus;

/* Internal Logic Bus */
extern Vat_Ctrl_Local laVAT_Localbus;

/* Version Info */
extern const SwcVersionInfo_t Vat_CtrlVersionInfo;

/* Input Data Element */
extern Arb_CtrlWhlOutVlvCtrlTarInfo_t Vat_CtrlWhlOutVlvCtrlTarInfo;
extern Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Vat_CtrlIdbPCtrllVlvCtrlStInfo;
extern Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Vat_CtrlIdbPedlFeelVlvCtrlStInfo;
extern Pct_5msCtrlWhlInVlvCtrlTarInfo_t Vat_CtrlWhlnVlvCtrlTarInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Vat_CtrlStkRecvryActnIfInfo;
extern Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Vat_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
extern Mom_HndlrEcuModeSts_t Vat_CtrlEcuModeSts;
extern Pct_5msCtrlPCtrlAct_t Vat_CtrlPCtrlAct;
extern Eem_SuspcDetnFuncInhibitVlvActSts_t Vat_CtrlFuncInhibitVlvActSts;
extern Pct_5msCtrlPCtrlBoostMod_t Vat_CtrlPCtrlBoostMod;

/* Output Data Element */
extern Vat_CtrlNormVlvReqVlvActInfo_t Vat_CtrlNormVlvReqVlvActInfo;
extern Vat_CtrlWhlVlvReqIdbInfo_t Vat_CtrlWhlVlvReqIdbInfo;
extern Vat_CtrlIdbBalVlvReqInfo_t Vat_CtrlIdbBalVlvReqInfo;
extern Vat_CtrlIdbVlvActInfo_t Vat_CtrlIdbVlvActInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Vat_Ctrl_Init(void);
extern void Vat_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VAT_CTRL_H_ */
/** @} */
