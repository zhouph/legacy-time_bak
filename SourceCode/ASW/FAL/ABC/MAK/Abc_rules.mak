# \file
#
# \brief Abc
#
# This file contains the implementation of the SWC
# module Abc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Abc_src

#Abc_src_FILES        += $(Abc_SRC_PATH)\Abc_Ctrl.c
Abc_src_FILES        += $(Abc_IFA_PATH)\Abc_Ctrl_Ifa.c
Abc_src_FILES        += $(Abc_CFG_PATH)\Abc_Cfg.c
Abc_src_FILES        += $(Abc_CAL_PATH)\Abc_Cal.c
Abc_src_FILES        += $(Abc_CAL_PATH)\LTunePar.c
ifeq ($(ICE_COMPILE),true)
Abc_src_FILES        += $(Abc_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Abc_src_FILES        += $(Abc_UNITY_PATH)\unity.c
	Abc_src_FILES        += $(Abc_UNITY_PATH)\unity_fixture.c	
	Abc_src_FILES        += $(Abc_UT_PATH)\main.c
	Abc_src_FILES        += $(Abc_UT_PATH)\Abc_Ctrl\Abc_Ctrl_UtMain.c
endif

DIRS 	= $(Abc_SRC_PATH) $(Abc_SRC_SUB_PATH)

#DIRS 	= $(Abc_SRC_SUB_PATH)
#		$(Abc_SRC_PATH)\ETCSGenCode \		$(Abc_SRC_PATH)\ETCSGenCodeHeader \

#DIRS = $(Abc_SRC_PATH)
#	$(Abc_SRC_PATH)\ETCSGenCode \
#	$(Abc_SRC_PATH)\ETCSGenCodeHeader \
	


#DIRS = $(Abc_SRC_PATH)\ETCSGenCode

Abc_src_FILES  += $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.c))
Abc_src_FILES  += $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.C))

# /* HSH */
ifeq ($(ICE_COMPILE), true)

# Library
Abc_src_FILES  += $(Abc_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Abc_src_FILES  += $(Abc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Abc_src_FILES  += $(Abc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Abc_src_FILES  += $(Abc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c

# Abc Source
#DIRS 	= $(PROJECT_ROOT)/source/MGH80/FLEXRAY \		$(PROJECT_ROOT)/source/MGH80/System \		$(PROJECT_ROOT)/source/MGH80/System/Drivers \
#DIRS = $(Abc_SRC_PATH)
#	$(Abc_SRC_PATH)\ETCSGenCode \
#	$(Abc_SRC_PATH)\ETCSGenCodeHeader \

#Abc_src_FILES  += $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.c))
#Abc_src_FILES  += $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.C))	

endif
