/**
 * @defgroup IoHwAb IoHwAb
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        IoHwAb.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef IOHWAB_H_
#define IOHWAB_H_

/*==============================================================================
 *                  INCLUDE FILES
==============================================================================*/
#include "IoHwAb_Types.h"
#include "IoHwAb_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
==============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
==============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
==============================================================================*/
 /* IO Output On/Off Control API */
 extern void IoHwAb_SetHigh(IoHwAb_PortType portId);
 extern void IoHwAb_SetLow (IoHwAb_PortType portId);

 /* Digital Level */
 extern IoHwAb_LevelType IoHwAb_GetLevel(IoHwAb_PortType portId);
 
 /* IO Output HW PWM Duty Control API */
 extern void IoHwAb_SetDuty(IoHwAb_PortType portId,
                            IoHwAb_DataType duty);

 /* IO HW voltage input */
 extern IoHwAb_DataType IoHwAb_GetVoltage(IoHwAb_PortType portId);

 /* IO HW voltage input */
 extern IoHwAb_VoltArrayType IoHwAb_GetVoltageArray(IoHwAb_PortType portId);
 
 /* IO HW angle input */
 extern IoHwAb_DataType IoHwAb_GetAngle(IoHwAb_PortType portId);

 /* IO HW duty input */
 extern IoHwAb_DataType IoHwAb_GetDuty(IoHwAb_PortType portId);

 /* IO HW Status Query API */
 extern IoHwAb_StatusType IoHwAb_GetStatus(IoHwAb_PortType portId);

 /* IO HW Direction Control */
 extern void IoHwAb_SetDirectionIn(IoHwAb_PortType portId);
 extern void IoHwAb_SetDirectionOut(IoHwAb_PortType portId);

 extern IoHwAb_DataIfType IoHwAb_GetDataIfType(IoHwAb_PortType portId);

 /* IO HW Edge Index Input for Rising Edge */
 extern IoHwAb_EdgeIdxType IoHwAb_GetRisingEdgeIndex(IoHwAb_PortType portId);
 /* IO HW Edge Index Input for Falling Edge */
 extern IoHwAb_EdgeIdxType IoHwAb_GetFallingEdgeIndex(IoHwAb_PortType portId);

 /* IO HW TimeStamp Array Input for Rising Edge */
 extern IoHwAb_TimeStampArrayType* IoHwAb_GetRisingEdgeTimeStampArray(IoHwAb_PortType portId);
 /* IO HW TimeStamp Array Input for Falling Edge */
 extern IoHwAb_TimeStampArrayType* IoHwAb_GetFallingEdgeTimeStampArray(IoHwAb_PortType portId);

/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/
#endif /* IOHWAB_H_ */
/** @} */
