#define UT_MAX_STEP 5

#define ABC_CTRLEEMFAILDATA_EEM_FAIL_BBSSOL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ESCSOL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_FRONTSOL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_REARSOL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_MOTOR {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_MPS {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_MGD {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_BBSVALVERELAY {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ESCVALVERELAY {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ECUHW {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ASIC {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_OVERVOLT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_UNDERVOLT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_LOWVOLT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_LOWERVOLT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SENPWR_12V {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SENPWR_5V {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_YAW {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_AY {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_AX {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_STR {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_CIRP1 {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_CIRP2 {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SIMP {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_BLS {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ESCSW {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_HDCSW {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_AVHSW {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_BRAKELAMPRELAY {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_ESSRELAY {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_GEARR {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_CLUTCH {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_PARKBRAKE {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_PEDALPDT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_PEDALPDF {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_BRAKEFLUID {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_TCSTEMP {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_HDCTEMP {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SCCTEMP {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_TVBBTEMP {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_MAINCANLINE {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SUBCANLINE {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_EMSTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_FWDTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_TCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_HCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_MCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_VARIANTCODING {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_WSSFL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_WSSFR {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_WSSRL {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_WSSRR {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_SAMESIDEWSS {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_DIAGONALWSS {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_FRONTWSS {0,0,0,0,0}
#define ABC_CTRLEEMFAILDATA_EEM_FAIL_REARWSS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_CBS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_EBD {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_ABS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_EDC {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_BTCS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_ETCS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_TCS {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_VDC {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_HSA {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_HDC {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_PBA {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_AVH {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_CTRLIHB_MOC {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_BBS_ALLCONTROLINHIBIT {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_BBS_DIAGCONTROLINHIBIT {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_BBS_DEGRADEMODEFAIL {0,0,0,0,0}
#define ABC_CTRLEEMCTRLINHIBITDATA_EEM_BBS_DEFECTIVEMODEFAIL {0,0,0,0,0}
#define ABC_CTRLCANRXACCELPEDLINFO_ACCELPEDLVAL {0,0,0,0,0}
#define ABC_CTRLCANRXACCELPEDLINFO_ACCELPEDLVALERR {0,0,0,0,0}
#define ABC_CTRLCANRXIDBINFO_TARGEARPOSI {0,0,0,0,0}
#define ABC_CTRLCANRXIDBINFO_GEARSELDISP {0,0,0,0,0}
#define ABC_CTRLCANRXIDBINFO_TCUSWIGS {0,0,0,0,0}
#define ABC_CTRLCANRXENGTEMPINFO_ENGTEMP {0,0,0,0,0}
#define ABC_CTRLCANRXENGTEMPINFO_ENGTEMPERR {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_AX {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_YAWRATE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_STEERINGANGLE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_AY {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_PBSWT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_CLUTCHSWT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_GEARRSWT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGACTINDTQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGRPM {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGINDTQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGFRICTIONLOSSTQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGSTDTQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TURBINERPM {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_THROTTLEANGLE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TPSRESOL1000 {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_PVAVCANRESOL1000 {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGCHR {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGVOL {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_GEARTYPE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGCLUTCHSTATE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGTQCMDBEFOREINTV {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_MOTESTTQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_MOTTQCMDBEFOREINTV {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TQINTVTCU {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TQINTVSLOWTCU {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TQINCREQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_DECELREQ {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_RAINSNSSTAT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ENGSPDERR {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_ATTYPE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_MTTYPE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_CVTTYPE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_DCTTYPE {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_HEVATTCU {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TURBINERPMERR {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_THROTTLEANGLEERR {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TOPTRVLCLTCHSWTACT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TOPTRVLCLTCHSWTACTV {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_HILLDESCTRLMDSWTACT {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_HILLDESCTRLMDSWTACTV {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_WIPERINTSW {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_WIPERLOW {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_WIPERHIGH {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_WIPERVALID {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_WIPERAUTO {0,0,0,0,0}
#define ABC_CTRLCANRXESCINFO_TCUFAULTSTS {0,0,0,0,0}
#define ABC_CTRLMOTROTGAGSIGINFO_STKPOSNMEASD {0,0,0,0,0}
#define ABC_CTRLESCSWTSTINFO_ESCDISABLEDBYSWT {0,0,0,0,0}
#define ABC_CTRLESCSWTSTINFO_TCSDISABLEDBYSWT {0,0,0,0,0}
#define ABC_CTRLWHLSPDINFO_FLWHLSPD {0,0,0,0,0}
#define ABC_CTRLWHLSPDINFO_FRWHLSPD {0,0,0,0,0}
#define ABC_CTRLWHLSPDINFO_RLWHLSPD {0,0,0,0,0}
#define ABC_CTRLWHLSPDINFO_RRLWHLSPD {0,0,0,0,0}
#define ABC_CTRLSASCALINFO_DIAGSASCALTOAPPL {0,0,0,0,0}
#define ABC_CTRLBRKPEDLSTATUSINFO_DRVRINTENDTORELSBRK {0,0,0,0,0}
#define ABC_CTRLCIRCPFILDINFO_PRIMCIRCPFILD_1_100BAR {0,0,0,0,0}
#define ABC_CTRLCIRCPFILDINFO_SECDCIRCPFILD_1_100BAR {0,0,0,0,0}
#define ABC_CTRLCIRCPOFFSCORRDINFO_PRIMCIRCPOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLCIRCPOFFSCORRDINFO_SECDCIRCPOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLESTIMDWHLPINFO_FRNTLEESTIMDWHLP {0,0,0,0,0}
#define ABC_CTRLESTIMDWHLPINFO_FRNTRIESTIMDWHLP {0,0,0,0,0}
#define ABC_CTRLESTIMDWHLPINFO_RELEESTIMDWHLP {0,0,0,0,0}
#define ABC_CTRLESTIMDWHLPINFO_RERIESTIMDWHLP {0,0,0,0,0}
#define ABC_CTRLPEDLSIMRPOFFSCORRDINFO_PEDLSIMRPOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLPEDLTRVLOFFSCORRDINFO_PDFRAWOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLPEDLTRVLOFFSCORRDINFO_PDTRAWOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLPISTPFILDINFO_PISTPFILD_1_100BAR {0,0,0,0,0}
#define ABC_CTRLPISTPOFFSCORRDINFO_PISTPOFFSCORRD {0,0,0,0,0}
#define ABC_CTRLRGNBRKCOOPWITHABSINFO_FRNTSLIPDETTHDSLIP {0,0,0,0,0}
#define ABC_CTRLRGNBRKCOOPWITHABSINFO_FRNTSLIPDETTHDUDIFF {0,0,0,0,0}
#define ABC_CTRLSTKRECVRYACTNIFINFO_RECOMMENDSTKRCVRLVL {0,0,0,0,0}
#define ABC_CTRLSTKRECVRYACTNIFINFO_STKRECVRYCTRLSTATE {0,0,0,0,0}
#define ABC_CTRLMUXCMDEXECSTINFO_MUXCMDEXECSTOFWHLFRNTLE {0,0,0,0,0}
#define ABC_CTRLMUXCMDEXECSTINFO_MUXCMDEXECSTOFWHLFRNTRI {0,0,0,0,0}
#define ABC_CTRLMUXCMDEXECSTINFO_MUXCMDEXECSTOFWHLRELE {0,0,0,0,0}
#define ABC_CTRLMUXCMDEXECSTINFO_MUXCMDEXECSTOFWHLRERI {0,0,0,0,0}
#define ABC_CTRLTXESCSENSORINFO_ESTIMATEDYAW {0,0,0,0,0}
#define ABC_CTRLTXESCSENSORINFO_ESTIMATEDAY {0,0,0,0,0}
#define ABC_CTRLTXESCSENSORINFO_A_LONG_1_1000G {0,0,0,0,0}
#define ABC_CTRLSENPWRMONITORDATA_SENPWRM_12V_STABLE {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFR {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRR {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SAMESIDEWSS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_DIAGONALWSS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_REARWSS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BBSSOL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ESCSOL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_FRONTSOL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_REARSOL {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_MOTOR {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_MPS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_MGD {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BBSVALVERELAY {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ESCVALVERELAY {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ECUHW {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ASIC {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_OVERVOLT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_UNDERVOLT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_LOWVOLT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_12V {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SENPWR_5V {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_YAW {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_AY {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_AX {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_STR {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_CIRP1 {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_CIRP2 {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SIMP {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BLS {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ESCSW {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_HDCSW {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_AVHSW {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BRAKELAMPRELAY {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_ESSRELAY {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_GEARR {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_CLUTCH {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_PARKBRAKE {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDF {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_BRAKEFLUID {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_TCSTEMP {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_HDCTEMP {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SCCTEMP {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_TVBBTEMP {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_MAINCANLINE {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_SUBCANLINE {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_EMSTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_FWDTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_TCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_HCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_MCUTIMEOUT {0,0,0,0,0}
#define ABC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_VARIANTCODING {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_WSS {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_YAW {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_AY {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_AX {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_CIR1P {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_CIR2P {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_SIMP {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_BLS {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_PEDAL {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_MOTOR {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_VDC_SW {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_HDC_SW {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_GEARR_SW {0,0,0,0,0}
#define ABC_CTRLEEMECEDATA_EEM_ECE_CLUTCH_SW {0,0,0,0,0}
#define ABC_CTRLYAWMSERIALINFO_YAWM_SERIALNUMOK_FLG {0,0,0,0,0}
#define ABC_CTRLYAWMSERIALINFO_YAWM_SERIALUNMATCH_FLG {0,0,0,0,0}
#define ABC_CTRLYAWMOUTINFO_YAWM_TEMPERATURE_ERR {0,0,0,0,0}
#define ABC_CTRLFINALTARPINFO_FINALMAXCIRCUITTP {0,0,0,0,0}
#define ABC_CTRLFSBBSVLVINITENDFLG  {0,0,0,0,0}
#define ABC_CTRLECUMODESTS  {0,0,0,0,0}
#define ABC_CTRLIGNONOFFSTS  {0,0,0,0,0}
#define ABC_CTRLVBATT1MON  {0,0,0,0,0}
#define ABC_CTRLFSESCVLVINITENDFLG  {0,0,0,0,0}
#define ABC_CTRLDIAGSCI  {0,0,0,0,0}
#define ABC_CTRLCANRXGEARSELDISPERRINFO  {0,0,0,0,0}
#define ABC_CTRLPCTRLACT  {0,0,0,0,0}
#define ABC_CTRLBLSSWT  {0,0,0,0,0}
#define ABC_CTRLESCSWT  {0,0,0,0,0}
#define ABC_CTRLBRKPREDNFORBASEBRKCTRLR  {0,0,0,0,0}
#define ABC_CTRLPEDLTRVLFINAL  {0,0,0,0,0}
#define ABC_CTRLRGNBRKCTRLRACTSTFLG  {0,0,0,0,0}
#define ABC_CTRLFINALTARPFROMPCTRL  {0,0,0,0,0}
#define ABC_CTRLTARPFROMBASEBRKCTRLR  {0,0,0,0,0}
#define ABC_CTRLTARPFROMBRKPEDL  {0,0,0,0,0}
#define ABC_CTRLMTRINITENDFLG  {0,0,0,0,0}

#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_0  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_1  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_2  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_3  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_4  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_5  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_6  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_7  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_8  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQDATA_9  {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVREQ   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLIVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FRIVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLIVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RRIVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FLOVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_FROVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RLOVDATALEN   {0,0,0,0,0}
#define ABC_CTRLWHLVLVREQABCINFO_RROVDATALEN   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TQINTVTCS   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TQINTVMSR   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TQINTVSLOWTCS   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_MINGEAR   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_MAXGEAR   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TCSREQ   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TCSCTRL   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_ABSACT   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TCSGEARSHIFTCHR   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_ESPCTRL   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_MSRREQ   {0,0,0,0,0}
#define ABC_CTRLCANTXINFO_TCSPRODUCTINFO   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSACTFLG   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRELE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRERI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_FRNTWHLSLIP   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDESTARP   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDESTARPREQFLG   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSCTRLFADEOUTFLG   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSCTRLMODEFRNTLE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSCTRLMODEFRNTRI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSCTRLMODERELE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSCTRLMODERERI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRATEFRNTLE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRATEFRNTRI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRATERELE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSTARPRATERERI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSPRIOFRNTLE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSPRIOFRNTRI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSPRIORELE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSPRIORERI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDELTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDELTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDELTARPRELE   {0,0,0,0,0}
#define ABC_CTRLABSCTRLINFO_ABSDELTARPRERI   {0,0,0,0,0}
#define ABC_CTRLAVHCTRLINFO_AVHACTFLG   {0,0,0,0,0}
#define ABC_CTRLAVHCTRLINFO_AVHDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLAVHCTRLINFO_AVHTARP   {0,0,0,0,0}
#define ABC_CTRLBACTRLINFO_BAACTFLG   {0,0,0,0,0}
#define ABC_CTRLBACTRLINFO_BACDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLBACTRLINFO_BATARP   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDACTFLG   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPRELE   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPRERI   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPRATERELE   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDTARPRATERERI   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDCTRLMODERELE   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDCTRLMODERERI   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDDELTARPRELE   {0,0,0,0,0}
#define ABC_CTRLEBDCTRLINFO_EBDDELTARPRERI   {0,0,0,0,0}
#define ABC_CTRLEBPCTRLINFO_EBPACTFLG   {0,0,0,0,0}
#define ABC_CTRLEBPCTRLINFO_EBPDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLEBPCTRLINFO_EBPTARP   {0,0,0,0,0}
#define ABC_CTRLEPBICTRLINFO_EPBIACTFLG   {0,0,0,0,0}
#define ABC_CTRLEPBICTRLINFO_EPBIDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLEPBICTRLINFO_EPBITARP   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCACTFLG   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRERI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCCTRLMODEFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCCTRLMODEFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCCTRLMODERELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCCTRLMODERERI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRATEFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRATEFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRATERELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCTARPRATERERI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRIOFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRIOFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRIORELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRIORERI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRECTRLMODEFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRECTRLMODEFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRECTRLMODERELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCPRECTRLMODERERI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCDELTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCDELTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCDELTARPRELE   {0,0,0,0,0}
#define ABC_CTRLESCCTRLINFO_ESCDELTARPRERI   {0,0,0,0,0}
#define ABC_CTRLHDCCTRLINFO_HDCACTFLG   {0,0,0,0,0}
#define ABC_CTRLHDCCTRLINFO_HDCDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLHDCCTRLINFO_HDCTARP   {0,0,0,0,0}
#define ABC_CTRLHSACTRLINFO_HSAACTFLG   {0,0,0,0,0}
#define ABC_CTRLHSACTRLINFO_HSADEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLHSACTRLINFO_HSATARP   {0,0,0,0,0}
#define ABC_CTRLSCCCTRLINFO_SCCACTFLG   {0,0,0,0,0}
#define ABC_CTRLSCCCTRLINFO_SCCDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLSCCCTRLINFO_SCCTARP   {0,0,0,0,0}
#define ABC_CTRLSTKRECVRYCTRLIFINFO_STKRCVRESCALLOWEDTIME   {0,0,0,0,0}
#define ABC_CTRLSTKRECVRYCTRLIFINFO_STKRCVRABSALLOWEDTIME   {0,0,0,0,0}
#define ABC_CTRLSTKRECVRYCTRLIFINFO_STKRCVRTCSALLOWEDTIME   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSACTFLG   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSTARPRELE   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSTARPRERI   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BOTHDRVGWHLBRKCTLREQFLG   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKCTRLFCTFRNTLEBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKCTRLFCTFRNTRIBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKCTRLFCTRELEBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKCTRLFCTRERIBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKTQGRDTREQFRNTLEBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKTQGRDTREQFRNTRIBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKTQGRDTREQRELEBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_BRKTQGRDTREQRERIBYWSPC   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSDELTARPFRNTLE   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSDELTARPFRNTRI   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSDELTARPRELE   {0,0,0,0,0}
#define ABC_CTRLTCSCTRLINFO_TCSDELTARPRERI   {0,0,0,0,0}
#define ABC_CTRLTVBBCTRLINFO_TVBBACTFLG   {0,0,0,0,0}
#define ABC_CTRLTVBBCTRLINFO_TVBBDEFECTFLG   {0,0,0,0,0}
#define ABC_CTRLTVBBCTRLINFO_TVBBTARP   {0,0,0,0,0}
#define ABC_CTRLFUNCTIONLAMP     {0,0,0,0,0}
#define ABC_CTRLACTVBRKCTRLRACTFLG     {0,0,0,0,0}
#define ABC_CTRLAY     {0,0,0,0,0}
#define ABC_CTRLVEHSPD     {0,0,0,0,0}
