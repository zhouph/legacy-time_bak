/**
 * @defgroup Abc_Types Abc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABC_TYPES_H_
#define ABC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define MEMCAL_APCALABS
#define MEMCAL_APCALESP
#define MEMCAL_APCALESPENG
#define MEMCAL_APCALABSVAFS
#define MEMCAL_APCALESPVAFS
#define MEMCAL_APCALAHB
#define MEMCAL_APCALRBC
#define MEMCAL_APCALAHB02
#define MEMCAL_APCALAHB03
#define MEMCAL_APCALAHB04
#define MEMCAL_APCALAHBPCTRL
#define MEMCAL_APCALRBCOEM

//#define __LOOP_TIME_10MS
#define __LOOP_TIME_5MS
#define __ESC_MOTOR_PWM_CONTROL     ENABLE

#ifdef __LOOP_TIME_10MS
    #define LOOP_TIME               (10)//ms
#elif defined(__LOOP_TIME_5MS)
    #define LOOP_TIME               (5)//ms
#endif

#define LOOP_TIME_10MS              (10)//ms

//#define VALVE_1MS_COUNT             (LOOP_TIME)
//#define MOTOR_1MS_COUNT             (LOOP_TIME)

//#define LOOP_TIME_US                (LOOP_TIME*1000)

//#define __ABS_UPLEVEL               DISABLE

#define LOOPTIME_MS(TIME)       ((TIME) / LOOP_TIME)
#define LOOPTIME_SEC(TIME)      (LOOPTIME_MS(TIME * 1000UL))
//#define LOOPTIME_MIN(TIME)      (LOOPTIME_SEC(TIME * 60UL))
//#define LOOPTIME_HOUR(TIME)     (LOOPTIME_MIN(TIME * 60UL))


/**************** VOLTAGE [ MV] ****************/
#define MCU_AD_RESOL            12      // 12bit
#define AD_REF_5V               5000

#define VOLT_TO_AD_TRANS(VOLT,R1,R2) (uint16_t)((uint32_t)(VOLT*(2^MCU_AD_RESOL)*R2)/((R1+R2)*AD_REF_5V))

#define MOTOR_AD_TRANS(VOLT)    VOLT_TO_AD_TRANS(VOLT,300,100)
#define MOTOR_12V               MOTOR_AD_TRANS(12000)
#define MOTOR_11V               MOTOR_AD_TRANS(11000)

//#define FSR_AD_TRANS(VOLT)      VOLT_TO_AD_TRANS(VOLT,300,51)
//#define FSR_16V                 FSR_AD_TRANS(16000)
//#define FSR_9V0                 FSR_AD_TRANS(9000)

#define IGN_AD_TRANS(VOLT)      FSR_AD_TRANS(VOLT)
#define IGN_16V                 IGN_AD_TRANS(16000)
#define IGN_9V0                 IGN_AD_TRANS(9000)


#define SLEEP_MODE                          0
#define ENGINE_INACTIVE_RUN_MODE            1
#define ENGINE_ACTIVE_RUN_MODE              2

#if __CAR_MAKER==HMC_KMC
  #define HMC_CAN
#endif

#if defined(HMC_CAN)
	#define IDB_SYSTEM
	#define CAR_YFE
	#define YFE_CAN_SPEC
#endif
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Abc_CtrlEemFailData;
    Eem_MainEemCtrlInhibitData_t Abc_CtrlEemCtrlInhibitData;
    Proxy_RxCanRxAccelPedlInfo_t Abc_CtrlCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t Abc_CtrlCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t Abc_CtrlCanRxEngTempInfo;
    Proxy_RxCanRxEscInfo_t Abc_CtrlCanRxEscInfo;
    Msp_CtrlMotRotgAgSigInfo_t Abc_CtrlMotRotgAgSigInfo;
    Swt_SenEscSwtStInfo_t Abc_CtrlEscSwtStInfo;
    Wss_SenWhlSpdInfo_t Abc_CtrlWhlSpdInfo;
    Diag_HndlrSasCalInfo_t Abc_CtrlSasCalInfo;
    Det_5msCtrlBrkPedlStatusInfo_t Abc_CtrlBrkPedlStatusInfo;
    Spc_5msCtrlCircPFildInfo_t Abc_CtrlCircPFildInfo;
    Spc_5msCtrlCircPOffsCorrdInfo_t Abc_CtrlCircPOffsCorrdInfo;
    Det_5msCtrlEstimdWhlPInfo_t Abc_CtrlEstimdWhlPInfo;
    Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Abc_CtrlPedlSimrPOffsCorrdInfo;
    Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Abc_CtrlPedlTrvlOffsCorrdInfo;
    Spc_5msCtrlPistPFildInfo_t Abc_CtrlPistPFildInfo;
    Spc_5msCtrlPistPOffsCorrdInfo_t Abc_CtrlPistPOffsCorrdInfo;
    Rbc_CtrlRgnBrkCoopWithAbsInfo_t Abc_CtrlRgnBrkCoopWithAbsInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Abc_CtrlStkRecvryActnIfInfo;
    Pct_5msCtrlMuxCmdExecStInfo_t Abc_CtrlMuxCmdExecStInfo;
    Proxy_TxTxESCSensorInfo_t Abc_CtrlTxESCSensorInfo;
    SenPwrM_MainSenPwrMonitor_t Abc_CtrlSenPwrMonitorData;
    Eem_MainEemSuspectData_t Abc_CtrlEemSuspectData;
    Eem_MainEemEceData_t Abc_CtrlEemEceData;
    YawM_MainYAWMSerialInfo_t Abc_CtrlYAWMSerialInfo;
    YawM_MainYAWMOutInfo_t Abc_CtrlYAWMOutInfo;
    Arb_CtrlFinalTarPInfo_t Abc_CtrlFinalTarPInfo;
    BbsVlvM_MainFSBbsVlvInitTest_t Abc_CtrlFSBbsVlvInitEndFlg;
    Mom_HndlrEcuModeSts_t Abc_CtrlEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Abc_CtrlIgnOnOffSts;
    Ioc_InputSR1msVBatt1Mon_t Abc_CtrlVBatt1Mon;
    AbsVlvM_MainFSEscVlvInitTest_t Abc_CtrlFSEscVlvInitEndFlg;
    Diag_HndlrDiagSci_t Abc_CtrlDiagSci;
    Proxy_RxCanRxGearSelDispErrInfo_t Abc_CtrlCanRxGearSelDispErrInfo;
    Pct_5msCtrlPCtrlAct_t Abc_CtrlPCtrlAct;
    Swt_SenBlsSwt_t Abc_CtrlBlsSwt;
    Swt_SenEscSwt_t Abc_CtrlEscSwt;
    Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Abc_CtrlBrkPRednForBaseBrkCtrlr;
    Det_5msCtrlPedlTrvlFinal_t Abc_CtrlPedlTrvlFinal;
    Rbc_CtrlRgnBrkCtrlrActStFlg_t Abc_CtrlRgnBrkCtrlrActStFlg;
    Pct_5msCtrlFinalTarPFromPCtrl_t Abc_CtrlFinalTarPFromPCtrl;
    Bbc_CtrlTarPFromBaseBrkCtrlr_t Abc_CtrlTarPFromBaseBrkCtrlr;
    Bbc_CtrlTarPFromBrkPedl_t Abc_CtrlTarPFromBrkPedl;
    MtrM_MainMTRInitTest_t Abc_CtrlMTRInitEndFlg;

/* Output Data Element */
    Abc_CtrlWhlVlvReqAbcInfo_t Abc_CtrlWhlVlvReqAbcInfo;
    Abc_CtrlCanTxInfo_t Abc_CtrlCanTxInfo;
    Abc_CtrlAbsCtrlInfo_t Abc_CtrlAbsCtrlInfo;
    Abc_CtrlAvhCtrlInfo_t Abc_CtrlAvhCtrlInfo;
    Abc_CtrlBaCtrlInfo_t Abc_CtrlBaCtrlInfo;
    Abc_CtrlEbdCtrlInfo_t Abc_CtrlEbdCtrlInfo;
    Abc_CtrlEbpCtrlInfo_t Abc_CtrlEbpCtrlInfo;
    Abc_CtrlEpbiCtrlInfo_t Abc_CtrlEpbiCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Abc_CtrlEscCtrlInfo;
    Abc_CtrlHdcCtrlInfo_t Abc_CtrlHdcCtrlInfo;
    Abc_CtrlHsaCtrlInfo_t Abc_CtrlHsaCtrlInfo;
    Abc_CtrlSccCtrlInfo_t Abc_CtrlSccCtrlInfo;
    Abc_CtrlStkRecvryCtrlIfInfo_t Abc_CtrlStkRecvryCtrlIfInfo;
    Abc_CtrlTcsCtrlInfo_t Abc_CtrlTcsCtrlInfo;
    Abc_CtrlTvbbCtrlInfo_t Abc_CtrlTvbbCtrlInfo;
    Abc_CtrlFunctionLamp_t Abc_CtrlFunctionLamp;
    Abc_CtrlActvBrkCtrlrActFlg_t Abc_CtrlActvBrkCtrlrActFlg;
    Abc_CtrlAy_t Abc_CtrlAy;
    Abc_CtrlVehSpd_t Abc_CtrlVehSpd;
}Abc_Ctrl_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABC_TYPES_H_ */
/** @} */
