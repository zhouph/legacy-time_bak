/**
 * @defgroup Abc_Ctrl Abc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABC_CTRL_H_
#define ABC_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Abc_Types.h"
#include "Abc_Cfg.h"
#include "Abc_Cal.h"
#include "Logic.par.h"
#include "../../../../BSW/NVM/HDR/NvMIf.h"
#include "ActiveBrakeControlInOutIfTemp.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ABC_CTRL_MODULE_ID      (0)
 #define ABC_CTRL_MAJOR_VERSION  (2)
 #define ABC_CTRL_MINOR_VERSION  (0)
 #define ABC_CTRL_PATCH_VERSION  (0)
 #define ABC_CTRL_BRANCH_VERSION (0)

/*ESC Sensor Offset Eep Data*/
#define NVM_KSTEER_offset_of_eeprom	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_offset_of_eeprom
#define NVM_KYAW_offset_of_eeprom   NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_offset_of_eeprom
#define NVM_KLAT_offset_of_eeprom   NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_offset_of_eeprom
#define NVM_fs_yaw_eeprom_offset    NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.fs_yaw_eeprom_offset
#define NVM_KYAW_eep_max            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_max
#define NVM_KYAW_eep_min            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_min
#define NVM_KSTEER_eep_max          NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_max
#define NVM_KSTEER_eep_min          NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_min
#define NVM_KLAT_eep_max            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_max
#define NVM_KLAT_eep_min            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_min
#define NVM_yaw_still_eep_max       NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_max
#define NVM_yaw_still_eep_min       NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_min
#define NVM_lsesps16EEPYawStandOfs  NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.lsesps16EEPYawStandOfs
#define NVM_YawTmpEEPMap            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.YawTmpEEPMap

/*ESC Sensor Offset Eep Complement Data*/
#define NVM_KSTEER_offset_of_eeprom_c NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_offset_of_eeprom_c
#define NVM_KYAW_offset_of_eeprom_c   NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_offset_of_eeprom_c
#define NVM_KLAT_offset_of_eeprom_c   NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_offset_of_eeprom_c
#define NVM_fs_yaw_eeprom_offset_c    NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.fs_yaw_eeprom_offset_c
#define NVM_KYAW_eep_max_c            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_eep_max_c
#define NVM_KYAW_eep_min_c            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_eep_min_c
#define NVM_KSTEER_eep_max_c          NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_eep_max_c
#define NVM_KSTEER_eep_min_c          NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_eep_min_c
#define NVM_KLAT_eep_max_c            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_eep_max_c
#define NVM_KLAT_eep_min_c            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_eep_min_c
#define NVM_yaw_still_eep_max_c       NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.yaw_still_eep_max_c
#define NVM_yaw_still_eep_min_c       NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.yaw_still_eep_min_c
#define NVM_lsesps16EEPYawStandOfs_c  NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.lsesps16EEPYawStandOfs_c
#define NVM_YawTmpEEPMap_c            NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.YawTmpEEPMap_c

/* ESC Adaptive model Eep Data */
#define NVM_AdpvVehMdlVchEep        NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVchEep
#define NVM_AdpvVehMdlVcrtRatEep    NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVcrtRatEep

/* TCS DiscTemp Eep Data */
#define NVM_btc_tmp_fl             	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fl_Eep
#define NVM_btc_tmp_fr             	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fr_Eep
#define NVM_btc_tmp_rl             	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rl_Eep
#define NVM_btc_tmp_rr             	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rr_Eep
#define NVM_ee_btcs_data           	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.ee_btcs_data_Eep

/* Ax Sensor Offset Eee Data*/
#define NVM_LgtSnsrEolOffsEep      NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrEolOffsEep
#define NVM_LgtSnsrDrvgOffsEep     NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrDrvgOffsEep
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Abc_Ctrl_HdrBusType Abc_CtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Abc_CtrlVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Abc_CtrlEemFailData;
extern Eem_MainEemCtrlInhibitData_t Abc_CtrlEemCtrlInhibitData;
extern Proxy_RxCanRxAccelPedlInfo_t Abc_CtrlCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t Abc_CtrlCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t Abc_CtrlCanRxEngTempInfo;
extern Proxy_RxCanRxEscInfo_t Abc_CtrlCanRxEscInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Abc_CtrlMotRotgAgSigInfo;
extern Swt_SenEscSwtStInfo_t Abc_CtrlEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Abc_CtrlWhlSpdInfo;
extern Diag_HndlrSasCalInfo_t Abc_CtrlSasCalInfo;
extern Det_5msCtrlBrkPedlStatusInfo_t Abc_CtrlBrkPedlStatusInfo;
extern Spc_5msCtrlCircPFildInfo_t Abc_CtrlCircPFildInfo;
extern Spc_5msCtrlCircPOffsCorrdInfo_t Abc_CtrlCircPOffsCorrdInfo;
extern Det_5msCtrlEstimdWhlPInfo_t Abc_CtrlEstimdWhlPInfo;
extern Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Abc_CtrlPedlSimrPOffsCorrdInfo;
extern Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Abc_CtrlPedlTrvlOffsCorrdInfo;
extern Spc_5msCtrlPistPFildInfo_t Abc_CtrlPistPFildInfo;
extern Spc_5msCtrlPistPOffsCorrdInfo_t Abc_CtrlPistPOffsCorrdInfo;
extern Rbc_CtrlRgnBrkCoopWithAbsInfo_t Abc_CtrlRgnBrkCoopWithAbsInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Abc_CtrlStkRecvryActnIfInfo;
extern Pct_5msCtrlMuxCmdExecStInfo_t Abc_CtrlMuxCmdExecStInfo;
extern Proxy_TxTxESCSensorInfo_t Abc_CtrlTxESCSensorInfo;
extern SenPwrM_MainSenPwrMonitor_t Abc_CtrlSenPwrMonitorData;
extern Eem_MainEemSuspectData_t Abc_CtrlEemSuspectData;
extern Eem_MainEemEceData_t Abc_CtrlEemEceData;
extern YawM_MainYAWMSerialInfo_t Abc_CtrlYAWMSerialInfo;
extern YawM_MainYAWMOutInfo_t Abc_CtrlYAWMOutInfo;
extern Arb_CtrlFinalTarPInfo_t Abc_CtrlFinalTarPInfo;
extern BbsVlvM_MainFSBbsVlvInitTest_t Abc_CtrlFSBbsVlvInitEndFlg;
extern Mom_HndlrEcuModeSts_t Abc_CtrlEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Abc_CtrlIgnOnOffSts;
extern Ioc_InputSR1msVBatt1Mon_t Abc_CtrlVBatt1Mon;
extern AbsVlvM_MainFSEscVlvInitTest_t Abc_CtrlFSEscVlvInitEndFlg;
extern Diag_HndlrDiagSci_t Abc_CtrlDiagSci;
extern Proxy_RxCanRxGearSelDispErrInfo_t Abc_CtrlCanRxGearSelDispErrInfo;
extern Pct_5msCtrlPCtrlAct_t Abc_CtrlPCtrlAct;
extern Swt_SenBlsSwt_t Abc_CtrlBlsSwt;
extern Swt_SenEscSwt_t Abc_CtrlEscSwt;
extern Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Abc_CtrlBrkPRednForBaseBrkCtrlr;
extern Det_5msCtrlPedlTrvlFinal_t Abc_CtrlPedlTrvlFinal;
extern Rbc_CtrlRgnBrkCtrlrActStFlg_t Abc_CtrlRgnBrkCtrlrActStFlg;
extern Pct_5msCtrlFinalTarPFromPCtrl_t Abc_CtrlFinalTarPFromPCtrl;
extern Bbc_CtrlTarPFromBaseBrkCtrlr_t Abc_CtrlTarPFromBaseBrkCtrlr;
extern Bbc_CtrlTarPFromBrkPedl_t Abc_CtrlTarPFromBrkPedl;
extern MtrM_MainMTRInitTest_t Abc_CtrlMTRInitEndFlg;

/* Output Data Element */
extern Abc_CtrlWhlVlvReqAbcInfo_t Abc_CtrlWhlVlvReqAbcInfo;
extern Abc_CtrlCanTxInfo_t Abc_CtrlCanTxInfo;
extern Abc_CtrlAbsCtrlInfo_t Abc_CtrlAbsCtrlInfo;
extern Abc_CtrlAvhCtrlInfo_t Abc_CtrlAvhCtrlInfo;
extern Abc_CtrlBaCtrlInfo_t Abc_CtrlBaCtrlInfo;
extern Abc_CtrlEbdCtrlInfo_t Abc_CtrlEbdCtrlInfo;
extern Abc_CtrlEbpCtrlInfo_t Abc_CtrlEbpCtrlInfo;
extern Abc_CtrlEpbiCtrlInfo_t Abc_CtrlEpbiCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Abc_CtrlEscCtrlInfo;
extern Abc_CtrlHdcCtrlInfo_t Abc_CtrlHdcCtrlInfo;
extern Abc_CtrlHsaCtrlInfo_t Abc_CtrlHsaCtrlInfo;
extern Abc_CtrlSccCtrlInfo_t Abc_CtrlSccCtrlInfo;
extern Abc_CtrlStkRecvryCtrlIfInfo_t Abc_CtrlStkRecvryCtrlIfInfo;
extern Abc_CtrlTcsCtrlInfo_t Abc_CtrlTcsCtrlInfo;
extern Abc_CtrlTvbbCtrlInfo_t Abc_CtrlTvbbCtrlInfo;
extern Abc_CtrlFunctionLamp_t Abc_CtrlFunctionLamp;
extern Abc_CtrlActvBrkCtrlrActFlg_t Abc_CtrlActvBrkCtrlrActFlg;
extern Abc_CtrlAy_t Abc_CtrlAy;
extern Abc_CtrlVehSpd_t Abc_CtrlVehSpd;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Abc_Ctrl_Init(void);
extern void Abc_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABC_CTRL_H_ */
/** @} */
