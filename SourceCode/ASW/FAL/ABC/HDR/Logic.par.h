
#include "../CFG/VariantName/setting.par.h"

/*---LOOPTIME SETTING-----------------------*/

    #define __L_CYCLETIME  LOOP_10MS

                        /*  LOOP_5MS */
                        /*  LOOP_10MS */

/*---------------------------------*/
    #define LOOP_5MS    5
    #define LOOP_10MS   10

/*---------------------------------*/

    #if (__L_CYCLETIME==LOOP_10MS)

        #define __ABS_CTRL_LOOPTIME     LOOP_5MS    /* CL updated every 10ms - 2 loops of ABS ctrl time */
        #define L_CONTROL_PERIOD        2           /* CL updated every 10ms - 2 loops of 5ms act. time */

    #elif (__L_CYCLETIME==LOOP_5MS)

        #define __ABS_CTRL_LOOPTIME     LOOP_5MS   /* CL updated every 5ms - 1 loops of ABS ctrl time */
        #define L_CONTROL_PERIOD        1          /* CL updated every 5ms - 1 loops of 5ms act. time */

    #endif

/*---------------------------------*/
    
    #if (__ABS_CTRL_LOOPTIME == LOOP_10MS)
        #include "../SRC/L10msLoopPars.h"
    #elif (__ABS_CTRL_LOOPTIME == LOOP_5MS)
        #include "../SRC/L5msLoopPars.h"
    #endif

#define CE_ON_STATE     				1/*0*//*KYB*/
#define CE_OFF_STATE    				0/*1*/

/*------- Sdiff Conversion ------------*/

  #if (__L_CYCLETIME==LOOP_10MS)

    #define S16_SDIFF_0G0     VREF_0_KPH
    #define S16_SDIFF_0G5     VREF_0_125_KPH
    #define S16_SDIFF_1G0     VREF_0_375_KPH
    #define S16_SDIFF_2G0     VREF_0_75_KPH
    #define S16_SDIFF_3G0     VREF_1_KPH
    #define S16_SDIFF_4G0     VREF_1_375_KPH
    #define S16_SDIFF_5G0     VREF_1_75_KPH
    #define S16_SDIFF_6G0     VREF_2_125_KPH
    #define S16_SDIFF_8G0     VREF_2_75_KPH
    #define S16_SDIFF_10G0    VREF_3_5_KPH
    #define S16_SDIFF_12G0    VREF_4_25_KPH

  #elif (__L_CYCLETIME==LOOP_5MS)

    #define S16_SDIFF_0G0     VREF_0_KPH
    #define S16_SDIFF_0G5     VREF_0_125_KPH
    #define S16_SDIFF_1G0     VREF_0_25_KPH
    #define S16_SDIFF_2G0     VREF_0_375_KPH
    #define S16_SDIFF_3G0     VREF_0_5_KPH
    #define S16_SDIFF_4G0     VREF_0_75_KPH
    #define S16_SDIFF_5G0     VREF_0_875_KPH
    #define S16_SDIFF_6G0     VREF_1_KPH
    #define S16_SDIFF_8G0     VREF_1_375_KPH
    #define S16_SDIFF_10G0    VREF_1_75_KPH
    #define S16_SDIFF_12G0    VREF_2_125_KPH

  #endif  /*(__L_CYCLETIME==LOOP_5MS)*/


/*------------ Type Max ---------------*/
       
    #define U8_TYPE_MAXNUM   250
    #define S8_TYPE_MAXNUM   120
    


/*-- Loop Time 5msec :  Filter Gain  --------*/
#define    L_U8FILTER_GAIN_5MSLOOP_0_2HZ     1
#define    L_U8FILTER_GAIN_5MSLOOP_0_5HZ     2
#define    L_U8FILTER_GAIN_5MSLOOP_0_7HZ     3
#define    L_U8FILTER_GAIN_5MSLOOP_1HZ       4 
#define    L_U8FILTER_GAIN_5MSLOOP_1_2HZ     5  
#define    L_U8FILTER_GAIN_5MSLOOP_1_5HZ     6 
#define    L_U8FILTER_GAIN_5MSLOOP_2HZ       8 
#define    L_U8FILTER_GAIN_5MSLOOP_2_5HZ     9 
#define    L_U8FILTER_GAIN_5MSLOOP_3HZ       11  
#define    L_U8FILTER_GAIN_5MSLOOP_3_5HZ     13
#define    L_U8FILTER_GAIN_5MSLOOP_4HZ       14  
#define    L_U8FILTER_GAIN_5MSLOOP_4_5HZ     16
#define    L_U8FILTER_GAIN_5MSLOOP_5HZ       17  
#define    L_U8FILTER_GAIN_5MSLOOP_5_5HZ     19
#define    L_U8FILTER_GAIN_5MSLOOP_6HZ       20 
#define    L_U8FILTER_GAIN_5MSLOOP_6_5HZ     22
#define    L_U8FILTER_GAIN_5MSLOOP_7HZ       23
#define    L_U8FILTER_GAIN_5MSLOOP_7_5HZ     24
#define    L_U8FILTER_GAIN_5MSLOOP_8HZ       26  
#define    L_U8FILTER_GAIN_5MSLOOP_8_5HZ     27
#define    L_U8FILTER_GAIN_5MSLOOP_9HZ       28    
#define    L_U8FILTER_GAIN_5MSLOOP_9_5HZ     29
#define    L_U8FILTER_GAIN_5MSLOOP_10HZ      31  
#define    L_U8FILTER_GAIN_5MSLOOP_10_5HZ    32
#define    L_U8FILTER_GAIN_5MSLOOP_11HZ      33  
#define    L_U8FILTER_GAIN_5MSLOOP_11_5HZ    34
#define    L_U8FILTER_GAIN_5MSLOOP_12HZ      35  
#define    L_U8FILTER_GAIN_5MSLOOP_12_5HZ    36
#define    L_U8FILTER_GAIN_5MSLOOP_13HZ      37
#define    L_U8FILTER_GAIN_5MSLOOP_13_5HZ    38
#define    L_U8FILTER_GAIN_5MSLOOP_14HZ      39 
#define    L_U8FILTER_GAIN_5MSLOOP_14_5HZ    40
#define    L_U8FILTER_GAIN_5MSLOOP_15HZ      41 
#define    L_U8FILTER_GAIN_5MSLOOP_15_5HZ    42
#define    L_U8FILTER_GAIN_5MSLOOP_16HZ      43
#define    L_U8FILTER_GAIN_5MSLOOP_16_5HZ    44
#define    L_U8FILTER_GAIN_5MSLOOP_17HZ      45   
#define    L_U8FILTER_GAIN_5MSLOOP_18HZ      46
#define    L_U8FILTER_GAIN_5MSLOOP_18_5HZ    47
#define    L_U8FILTER_GAIN_5MSLOOP_19HZ      48 
#define    L_U8FILTER_GAIN_5MSLOOP_20HZ      49
#define    L_U8FILTER_GAIN_5MSLOOP_20_5HZ    50  
#define    L_U8FILTER_GAIN_5MSLOOP_22_5HZ    53
#define    L_U8FILTER_GAIN_5MSLOOP_23HZ      54
#define    L_U8FILTER_GAIN_5MSLOOP_26HZ    	 58                                                                                    
#define    L_U8FILTER_GAIN_5MSLOOP_29HZ      61
#define    L_U8FILTER_GAIN_5MSLOOP_54HZ      81
#define    L_U8FILTER_GAIN_5MSLOOP_67HZ      87 



/*-- Loop Time 10msec :  Filter Gain  -------*/
#define    L_U8FILTER_GAIN_10MSLOOP_0_2HZ    1
#define    L_U8FILTER_GAIN_10MSLOOP_0_3HZ    2
#define    L_U8FILTER_GAIN_10MSLOOP_0_5HZ    4
#define	   L_U8FILTER_GAIN_10MSLOOP_0_7HZ	 6
#define    L_U8FILTER_GAIN_10MSLOOP_1HZ      8   
#define    L_U8FILTER_GAIN_10MSLOOP_1_5HZ    11
#define    L_U8FILTER_GAIN_10MSLOOP_2HZ      14   
#define    L_U8FILTER_GAIN_10MSLOOP_2_5HZ    17   
#define    L_U8FILTER_GAIN_10MSLOOP_3HZ      20   
#define    L_U8FILTER_GAIN_10MSLOOP_3_5HZ    23
#define    L_U8FILTER_GAIN_10MSLOOP_4HZ      26  
#define    L_U8FILTER_GAIN_10MSLOOP_4_5HZ    28
#define    L_U8FILTER_GAIN_10MSLOOP_5HZ      31    
#define    L_U8FILTER_GAIN_10MSLOOP_5_5HZ    33
#define    L_U8FILTER_GAIN_10MSLOOP_6HZ      35 
#define    L_U8FILTER_GAIN_10MSLOOP_6_5HZ    37
#define    L_U8FILTER_GAIN_10MSLOOP_7HZ      39
#define    L_U8FILTER_GAIN_10MSLOOP_7_5HZ    41
#define    L_U8FILTER_GAIN_10MSLOOP_8HZ      43 
#define    L_U8FILTER_GAIN_10MSLOOP_8_5HZ    45
#define    L_U8FILTER_GAIN_10MSLOOP_9HZ      46  
#define    L_U8FILTER_GAIN_10MSLOOP_9_5HZ    48
#define    L_U8FILTER_GAIN_10MSLOOP_10HZ     49  
#define    L_U8FILTER_GAIN_10MSLOOP_10_5HZ   51
#define    L_U8FILTER_GAIN_10MSLOOP_11HZ     52  
#define    L_U8FILTER_GAIN_10MSLOOP_11_5HZ   54
#define    L_U8FILTER_GAIN_10MSLOOP_12HZ     55  
#define    L_U8FILTER_GAIN_10MSLOOP_12_5HZ   56
#define    L_U8FILTER_GAIN_10MSLOOP_13HZ     58
#define    L_U8FILTER_GAIN_10MSLOOP_13_5HZ   59
#define    L_U8FILTER_GAIN_10MSLOOP_14HZ     60 
#define    L_U8FILTER_GAIN_10MSLOOP_14_5HZ   61
#define    L_U8FILTER_GAIN_10MSLOOP_15HZ     62 
#define    L_U8FILTER_GAIN_10MSLOOP_15_5HZ   63
#define    L_U8FILTER_GAIN_10MSLOOP_16HZ     64 
#define    L_U8FILTER_GAIN_10MSLOOP_16_5HZ   65
#define    L_U8FILTER_GAIN_10MSLOOP_17HZ     66  
#define    L_U8FILTER_GAIN_10MSLOOP_17_5HZ   67
#define    L_U8FILTER_GAIN_10MSLOOP_18HZ     68 
#define    L_U8FILTER_GAIN_10MSLOOP_18_5HZ   69
#define    L_U8FILTER_GAIN_10MSLOOP_19HZ     70 
#define    L_U8FILTER_GAIN_10MSLOOP_20HZ     71
#define    L_U8FILTER_GAIN_10MSLOOP_20_5HZ   72
#define	   L_U8FILTER_GAIN_10MSLOOP_23HZ     76
#define	   L_U8FILTER_GAIN_10MSLOOP_26HZ     79
#define	   L_U8FILTER_GAIN_10MSLOOP_29HZ	 83
#define    L_U8FILTER_GAIN_10MSLOOP_43HZ     93
#define	   L_U8FILTER_GAIN_10MSLOOP_54HZ	 99
#define	   L_U8FILTER_GAIN_10MSLOOP_67HZ	 103



/*-- Loop Time 5msec :  Time Counter -------*/
#define    L_U8_TIME_5MSLOOP_0MS            0
#define    L_U8_TIME_5MSLOOP_5MS            1
#define    L_U8_TIME_5MSLOOP_10MS           2
#define    L_U8_TIME_5MSLOOP_15MS           3
#define    L_U8_TIME_5MSLOOP_20MS           4
#define    L_U8_TIME_5MSLOOP_25MS           5
#define    L_U8_TIME_5MSLOOP_30MS           6
#define    L_U8_TIME_5MSLOOP_35MS           7
#define    L_U8_TIME_5MSLOOP_40MS           8
#define    L_U8_TIME_5MSLOOP_45MS           9
#define    L_U8_TIME_5MSLOOP_50MS           10
#define    L_U8_TIME_5MSLOOP_55MS           11
#define    L_U8_TIME_5MSLOOP_60MS           12
#define    L_U8_TIME_5MSLOOP_65MS           13
#define    L_U8_TIME_5MSLOOP_70MS           14
#define    L_U8_TIME_5MSLOOP_75MS           15
#define    L_U8_TIME_5MSLOOP_80MS           16
#define    L_U8_TIME_5MSLOOP_85MS           17
#define    L_U8_TIME_5MSLOOP_90MS           18
#define    L_U8_TIME_5MSLOOP_95MS           19
#define    L_U8_TIME_5MSLOOP_100MS          20
#define    L_U8_TIME_5MSLOOP_105MS          21                       
#define    L_U8_TIME_5MSLOOP_110MS          22 
#define    L_U8_TIME_5MSLOOP_120MS          24                
#define    L_U8_TIME_5MSLOOP_130MS          26
#define    L_U8_TIME_5MSLOOP_140MS          28
#define    L_U8_TIME_5MSLOOP_145MS          29
#define    L_U8_TIME_5MSLOOP_150MS          30
#define    L_U8_TIME_5MSLOOP_170MS          34
#define    L_U8_TIME_5MSLOOP_175MS          35
#define    L_U8_TIME_5MSLOOP_200MS          40
#define    L_U8_TIME_5MSLOOP_210MS          42
#define    L_U8_TIME_5MSLOOP_220MS          44
#define    L_U8_TIME_5MSLOOP_230MS          46
#define    L_U8_TIME_5MSLOOP_245MS          49
#define    L_U8_TIME_5MSLOOP_250MS          50
#define    L_U8_TIME_5MSLOOP_280MS          56
#define    L_U8_TIME_5MSLOOP_290MS          58
#define    L_U8_TIME_5MSLOOP_300MS          60
#define    L_U8_TIME_5MSLOOP_350MS          70
#define    L_U8_TIME_5MSLOOP_400MS          80
#define    L_U8_TIME_5MSLOOP_420MS          84
#define    L_U8_TIME_5MSLOOP_450MS          90
#define    L_U8_TIME_5MSLOOP_490MS          98
#define    L_U8_TIME_5MSLOOP_500MS          100
#define    L_U8_TIME_5MSLOOP_550MS          110
#define    L_U8_TIME_5MSLOOP_560MS          112
#define    L_U8_TIME_5MSLOOP_570MS          114
#define    L_U8_TIME_5MSLOOP_600MS          120
#define    L_U8_TIME_5MSLOOP_650MS          130
#define    L_U8_TIME_5MSLOOP_700MS          140
#define    L_U8_TIME_5MSLOOP_730MS          146
#define    L_U8_TIME_5MSLOOP_750MS          150
#define    L_U8_TIME_5MSLOOP_800MS          160
#define    L_U8_TIME_5MSLOOP_850MS          170
#define    L_U8_TIME_5MSLOOP_900MS          180
#define    L_U8_TIME_5MSLOOP_980MS          196
#define    L_U8_TIME_5MSLOOP_1000MS         200
#define    L_U8_TIME_5MSLOOP_1050MS			210
#define    L_U8_TIME_5MSLOOP_1055MS			211
#define    L_U8_TIME_5MSLOOP_1120MS         224
#define    L_U8_TIME_5MSLOOP_1200MS         240 
#define    L_U16_TIME_5MSLOOP_1400MS        280
#define    L_U16_TIME_5MSLOOP_1450MS        290
#define    L_U16_TIME_5MSLOOP_1480MS        296
#define    L_U16_TIME_5MSLOOP_1500MS        300
#define    L_U16_TIME_5MSLOOP_1680MS		336
#define    L_U16_TIME_5MSLOOP_1750MS        350
#define    L_U16_TIME_5MSLOOP_1960MS        392
#define    L_U8_TIME_5MSLOOP_1S        		200
#define    L_U16_TIME_5MSLOOP_2S            400
#define    L_U16_TIME_5MSLOOP_3S            600
#define    L_U16_TIME_5MSLOOP_4S            800
#define    L_U16_TIME_5MSLOOP_4_1S          820  
#define    L_U16_TIME_5MSLOOP_4_2S          840
#define    L_U16_TIME_5MSLOOP_4_3S          860
#define    L_U16_TIME_5MSLOOP_4_4S          880
#define    L_U16_TIME_5MSLOOP_4_5S          900 
#define    L_U16_TIME_5MSLOOP_4_7S          940 
#define    L_U16_TIME_5MSLOOP_5S            1000
#define    L_U16_TIME_5MSLOOP_6S            1200
#define    L_U16_TIME_5MSLOOP_7S         	1400   
#define    L_U16_TIME_5MSLOOP_8S         	1600   
#define    L_U16_TIME_5MSLOOP_9S         	1800   
#define    L_U16_TIME_5MSLOOP_10S        	2000 
#define    L_U16_TIME_5MSLOOP_11S        	2200 
#define    L_U16_TIME_5MSLOOP_12S        	2400 
#define    L_U16_TIME_5MSLOOP_13S        	2600 
#define    L_U16_TIME_5MSLOOP_14S        	2800 
#define    L_U16_TIME_5MSLOOP_15S        	3000 
#define    L_U16_TIME_5MSLOOP_16S        	3200 
#define    L_U16_TIME_5MSLOOP_17S        	3400 
#define    L_U16_TIME_5MSLOOP_18S        	3600 
#define    L_U16_TIME_5MSLOOP_19S        	3800 
#define    L_U16_TIME_5MSLOOP_20S        	4000 
#define    L_U16_TIME_5MSLOOP_21S        	4200 
#define    L_U16_TIME_5MSLOOP_22S        	4400 
#define    L_U16_TIME_5MSLOOP_23S        	4600 
#define    L_U16_TIME_5MSLOOP_24S        	4800 
#define    L_U16_TIME_5MSLOOP_25S        	5000 
#define    L_U16_TIME_5MSLOOP_26S        	5200 
#define    L_U16_TIME_5MSLOOP_27S        	5400 
#define    L_U16_TIME_5MSLOOP_28S        	5600 
#define    L_U16_TIME_5MSLOOP_29S        	5800 
#define    L_U16_TIME_5MSLOOP_30S        	6000 
#define    L_U16_TIME_5MSLOOP_31S        	6200 
#define    L_U16_TIME_5MSLOOP_32S        	6400 
#define    L_U16_TIME_5MSLOOP_33S        	6600 
#define    L_U16_TIME_5MSLOOP_34S        	6800 
#define    L_U16_TIME_5MSLOOP_35S        	7000 
#define    L_U16_TIME_5MSLOOP_36S        	7200 
#define    L_U16_TIME_5MSLOOP_37S        	7400 
#define    L_U16_TIME_5MSLOOP_38S        	7600 
#define    L_U16_TIME_5MSLOOP_39S        	7800 
#define    L_U16_TIME_5MSLOOP_40S        	8000   
#define    L_U16_TIME_5MSLOOP_41S        	8200   
#define    L_U16_TIME_5MSLOOP_42S        	8400   
#define    L_U16_TIME_5MSLOOP_43S        	8600   
#define    L_U16_TIME_5MSLOOP_44S        	8800   
#define    L_U16_TIME_5MSLOOP_45S        	9000   
#define    L_U16_TIME_5MSLOOP_46S        	9200   
#define    L_U16_TIME_5MSLOOP_47S        	9400   
#define    L_U16_TIME_5MSLOOP_48S        	9600   
#define    L_U16_TIME_5MSLOOP_49S        	9800   
#define    L_U16_TIME_5MSLOOP_50S        	10000  
#define    L_U16_TIME_5MSLOOP_51S        	10200  
#define    L_U16_TIME_5MSLOOP_52S        	10400  
#define    L_U16_TIME_5MSLOOP_53S        	10600  
#define    L_U16_TIME_5MSLOOP_54S        	10800  
#define    L_U16_TIME_5MSLOOP_55S        	11000  
#define    L_U16_TIME_5MSLOOP_56S        	11200  
#define    L_U16_TIME_5MSLOOP_57S        	11400  
#define    L_U16_TIME_5MSLOOP_58S        	11600  
#define    L_U16_TIME_5MSLOOP_59S        	11800  
#define    L_U16_TIME_5MSLOOP_60S        	12000  
#define    L_U16_TIME_5MSLOOP_61S        	12200  
#define    L_U16_TIME_5MSLOOP_62S        	12400  
#define    L_U16_TIME_5MSLOOP_63S        	12600  
#define    L_U16_TIME_5MSLOOP_64S 	     	12800  
#define    L_U16_TIME_5MSLOOP_65S        	13000
#define    L_U16_TIME_5MSLOOP_66S        	13200
#define    L_U16_TIME_5MSLOOP_67S        	13400
#define    L_U16_TIME_5MSLOOP_68S        	13600
#define    L_U16_TIME_5MSLOOP_69S        	13800
#define    L_U16_TIME_5MSLOOP_70S        	14000
#define    L_U16_TIME_5MSLOOP_71S        	14200
#define    L_U16_TIME_5MSLOOP_72S        	14400
#define    L_U16_TIME_5MSLOOP_73S        	14600
#define    L_U16_TIME_5MSLOOP_74S        	14800
#define    L_U16_TIME_5MSLOOP_75S        	15000
#define    L_U16_TIME_5MSLOOP_76S        	15200
#define    L_U16_TIME_5MSLOOP_77S        	15400
#define    L_U16_TIME_5MSLOOP_78S        	15600
#define    L_U16_TIME_5MSLOOP_79S        	15800
#define    L_U16_TIME_5MSLOOP_80S        	16000
#define    L_U16_TIME_5MSLOOP_81S        	16200
#define    L_U16_TIME_5MSLOOP_82S        	16400
#define    L_U16_TIME_5MSLOOP_83S        	16600
#define    L_U16_TIME_5MSLOOP_84S        	16800
#define    L_U16_TIME_5MSLOOP_85S        	17000
#define    L_U16_TIME_5MSLOOP_86S        	17200
#define    L_U16_TIME_5MSLOOP_87S        	17400
#define    L_U16_TIME_5MSLOOP_88S        	17600
#define    L_U16_TIME_5MSLOOP_89S        	17800
#define    L_U16_TIME_5MSLOOP_90S        	18000
#define    L_U16_TIME_5MSLOOP_91S        	18200
#define    L_U16_TIME_5MSLOOP_92S        	18400
#define    L_U16_TIME_5MSLOOP_93S        	18600
#define    L_U16_TIME_5MSLOOP_94S        	18800
#define    L_U16_TIME_5MSLOOP_95S        	19000
#define    L_U16_TIME_5MSLOOP_96S        	19200
#define    L_U16_TIME_5MSLOOP_97S        	19400
#define    L_U16_TIME_5MSLOOP_98S        	19600
#define    L_U16_TIME_5MSLOOP_99S        	19800
#define    L_U16_TIME_5MSLOOP_100S       	20000
#define    L_U16_TIME_5MSLOOP_1MIN          12000
#define    L_U16_TIME_5MSLOOP_2MIN          24000
#define    L_U16_TIME_5MSLOOP_3MIN          36000
#define    L_U16_TIME_5MSLOOP_5MIN          60000
#define    L_U32_TIME_5MSLOOP_7MIN          84000
#define    L_U32_TIME_5MSLOOP_30MIN         360000
                                      

/*-- Loop Time 10msec :  Time Counter ------*/
#define    L_U8_TIME_10MSLOOP_0MS           0
#define    L_U8_TIME_10MSLOOP_10MS          1
#define    L_U8_TIME_10MSLOOP_20MS          2
#define    L_U8_TIME_10MSLOOP_30MS          3
#define    L_U8_TIME_10MSLOOP_40MS          4
#define    L_U8_TIME_10MSLOOP_50MS          5
#define    L_U8_TIME_10MSLOOP_60MS          6
#define    L_U8_TIME_10MSLOOP_70MS          7
#define    L_U8_TIME_10MSLOOP_80MS          8
#define    L_U8_TIME_10MSLOOP_90MS          9
#define    L_U8_TIME_10MSLOOP_100MS         10
#define    L_U8_TIME_10MSLOOP_110MS         11
#define    L_U8_TIME_10MSLOOP_120MS         12
#define    L_U8_TIME_10MSLOOP_130MS         13
#define    L_U8_TIME_10MSLOOP_140MS         14
#define    L_U8_TIME_10MSLOOP_150MS         15
#define    L_U8_TIME_10MSLOOP_160MS         16
#define    L_U8_TIME_10MSLOOP_170MS         17
#define    L_U8_TIME_10MSLOOP_180MS         18
#define    L_U8_TIME_10MSLOOP_190MS         19
#define    L_U8_TIME_10MSLOOP_200MS         20
#define    L_U8_TIME_10MSLOOP_210MS         21
#define    L_U8_TIME_10MSLOOP_220MS         22
#define    L_U8_TIME_10MSLOOP_230MS         23
#define    L_U8_TIME_10MSLOOP_240MS         24
#define    L_U8_TIME_10MSLOOP_250MS         25
#define    L_U8_TIME_10MSLOOP_260MS         26
#define    L_U8_TIME_10MSLOOP_270MS         27
#define    L_U8_TIME_10MSLOOP_280MS         28
#define    L_U8_TIME_10MSLOOP_290MS         29
#define    L_U8_TIME_10MSLOOP_300MS         30
#define    L_U8_TIME_10MSLOOP_310MS         31
#define    L_U8_TIME_10MSLOOP_320MS         32
#define    L_U8_TIME_10MSLOOP_330MS         33
#define    L_U8_TIME_10MSLOOP_340MS         34
#define    L_U8_TIME_10MSLOOP_350MS         35
#define    L_U8_TIME_10MSLOOP_360MS         36
#define    L_U8_TIME_10MSLOOP_370MS         37
#define    L_U8_TIME_10MSLOOP_380MS         38
#define    L_U8_TIME_10MSLOOP_390MS         39
#define    L_U8_TIME_10MSLOOP_400MS         40
#define    L_U8_TIME_10MSLOOP_410MS         41
#define    L_U8_TIME_10MSLOOP_420MS         42
#define    L_U8_TIME_10MSLOOP_430MS         43
#define    L_U8_TIME_10MSLOOP_440MS         44
#define    L_U8_TIME_10MSLOOP_450MS         45
#define    L_U8_TIME_10MSLOOP_460MS         46
#define    L_U8_TIME_10MSLOOP_470MS         47
#define    L_U8_TIME_10MSLOOP_480MS         48
#define    L_U8_TIME_10MSLOOP_490MS         49
#define    L_U8_TIME_10MSLOOP_500MS         50
#define    L_U8_TIME_10MSLOOP_510MS         51
#define    L_U8_TIME_10MSLOOP_520MS         52
#define    L_U8_TIME_10MSLOOP_530MS         53
#define    L_U8_TIME_10MSLOOP_540MS         54
#define    L_U8_TIME_10MSLOOP_550MS         55
#define    L_U8_TIME_10MSLOOP_560MS         56
#define    L_U8_TIME_10MSLOOP_570MS         57
#define    L_U8_TIME_10MSLOOP_580MS         58
#define    L_U8_TIME_10MSLOOP_590MS         59
#define    L_U8_TIME_10MSLOOP_600MS         60
#define    L_U8_TIME_10MSLOOP_610MS         61
#define    L_U8_TIME_10MSLOOP_620MS         62
#define    L_U8_TIME_10MSLOOP_630MS         63
#define    L_U8_TIME_10MSLOOP_640MS         64
#define    L_U8_TIME_10MSLOOP_650MS         65
#define    L_U8_TIME_10MSLOOP_660MS         66
#define    L_U8_TIME_10MSLOOP_670MS         67
#define    L_U8_TIME_10MSLOOP_680MS         68
#define    L_U8_TIME_10MSLOOP_690MS         69
#define    L_U8_TIME_10MSLOOP_700MS         70
#define    L_U8_TIME_10MSLOOP_710MS         71
#define    L_U8_TIME_10MSLOOP_720MS         72
#define    L_U8_TIME_10MSLOOP_730MS         73
#define    L_U8_TIME_10MSLOOP_740MS         74
#define    L_U8_TIME_10MSLOOP_750MS         75
#define    L_U8_TIME_10MSLOOP_760MS         76
#define    L_U8_TIME_10MSLOOP_770MS         77
#define    L_U8_TIME_10MSLOOP_780MS         78
#define    L_U8_TIME_10MSLOOP_790MS         79
#define    L_U8_TIME_10MSLOOP_800MS         80
#define    L_U8_TIME_10MSLOOP_810MS         81
#define    L_U8_TIME_10MSLOOP_820MS         82
#define    L_U8_TIME_10MSLOOP_830MS         83
#define    L_U8_TIME_10MSLOOP_840MS         84
#define    L_U8_TIME_10MSLOOP_850MS         85
#define    L_U8_TIME_10MSLOOP_860MS         86
#define    L_U8_TIME_10MSLOOP_870MS         87
#define    L_U8_TIME_10MSLOOP_880MS         88
#define    L_U8_TIME_10MSLOOP_890MS         89
#define    L_U8_TIME_10MSLOOP_900MS         90
#define    L_U8_TIME_10MSLOOP_910MS         91
#define    L_U8_TIME_10MSLOOP_920MS         92
#define    L_U8_TIME_10MSLOOP_930MS         93
#define    L_U8_TIME_10MSLOOP_940MS         94
#define    L_U8_TIME_10MSLOOP_950MS         95
#define    L_U8_TIME_10MSLOOP_960MS         96
#define    L_U8_TIME_10MSLOOP_970MS         97
#define    L_U8_TIME_10MSLOOP_980MS         98
#define    L_U8_TIME_10MSLOOP_990MS         99
#define    L_U8_TIME_10MSLOOP_1000MS        100
#define    L_U8_TIME_10MSLOOP_1050MS        105
#define    L_U8_TIME_10MSLOOP_1100MS        110
#define    L_U8_TIME_10MSLOOP_1120MS        112
#define    L_U8_TIME_10MSLOOP_1200MS        120
#define    L_U8_TIME_10MSLOOP_1400MS        140
#define    L_U8_TIME_10MSLOOP_1450MS        145
#define    L_U8_TIME_10MSLOOP_1480MS        148
#define    L_U8_TIME_10MSLOOP_1500MS        150
#define    L_U8_TIME_10MSLOOP_1600MS        160
#define    L_U8_TIME_10MSLOOP_1680MS		168
#define    L_U8_TIME_10MSLOOP_1700MS        170
#define    L_U8_TIME_10MSLOOP_1750MS        175
#define    L_U8_TIME_10MSLOOP_1960MS		196
#define    L_U8_TIME_10MSLOOP_2S            200
#define    L_U16_TIME_10MSLOOP_3S           300
#define    L_U16_TIME_10MSLOOP_3_5S         350
#define    L_U16_TIME_10MSLOOP_4S           400
#define    L_U16_TIME_10MSLOOP_4_1S         410
#define    L_U16_TIME_10MSLOOP_4_2S         420
#define    L_U16_TIME_10MSLOOP_4_3S         430
#define    L_U16_TIME_10MSLOOP_4_4S         440
#define    L_U16_TIME_10MSLOOP_4_5S         450
#define    L_U16_TIME_10MSLOOP_5S           500
#define    L_U16_TIME_10MSLOOP_6S           600
#define    L_U16_TIME_10MSLOOP_7S           700
#define    L_U16_TIME_10MSLOOP_8S 	        800
#define    L_U16_TIME_10MSLOOP_10S          1000
#define    L_U16_TIME_10MSLOOP_12S	        1200
#define    L_U16_TIME_10MSLOOP_14S          1400
#define    L_U16_TIME_10MSLOOP_15S          1500
#define    L_U16_TIME_10MSLOOP_16S	        1600
#define    L_U16_TIME_10MSLOOP_20S	        2000
#define    L_U16_TIME_10MSLOOP_24S	        2400
#define    L_U16_TIME_10MSLOOP_28S	        2800
#define    L_U16_TIME_10MSLOOP_30S	        3000
#define    L_U16_TIME_10MSLOOP_32S	        3200
#define    L_U16_TIME_10MSLOOP_36S	        3600
#define    L_U16_TIME_10MSLOOP_40S	        4000
#define    L_U16_TIME_10MSLOOP_44S	        4400
#define    L_U16_TIME_10MSLOOP_48S	        4800
#define    L_U16_TIME_10MSLOOP_50S          5000
#define    L_U16_TIME_10MSLOOP_52S	        5200
#define    L_U16_TIME_10MSLOOP_56S	        5600
#define    L_U16_TIME_10MSLOOP_60S	        6000
#define    L_U16_TIME_10MSLOOP_64S	        6400
#define    L_U16_TIME_10MSLOOP_68S	        6800
#define    L_U16_TIME_10MSLOOP_72S	        7200
#define    L_U16_TIME_10MSLOOP_76S	        7600
#define    L_U16_TIME_10MSLOOP_80S	        8000
#define    L_U16_TIME_10MSLOOP_84S	        8400
#define    L_U16_TIME_10MSLOOP_88S	        8800
#define    L_U16_TIME_10MSLOOP_90S	        9000
#define    L_U16_TIME_10MSLOOP_92S	        9200
#define    L_U16_TIME_10MSLOOP_96S	        9600
#define    L_U16_TIME_10MSLOOP_100S	        10000
#define    L_U16_TIME_10MSLOOP_1MIN         6000
#define    L_U16_TIME_10MSLOOP_2MIN         12000
#define    L_U16_TIME_10MSLOOP_3MIN         18000
#define    L_U16_TIME_10MSLOOP_5MIN         30000
#define    L_U16_TIME_10MSLOOP_7MIN         42000
#define    L_U32_TIME_10MSLOOP_30MIN        180000

// COUNTER
#define    L_U8_CNT_1        1  
#define    L_U8_CNT_2        2  
#define    L_U8_CNT_3        3  
#define    L_U8_CNT_4        4  
#define    L_U8_CNT_5        5  
#define    L_U8_CNT_6        6  
#define    L_U8_CNT_7        7  
#define    L_U8_CNT_8        8  
#define    L_U8_CNT_9        9  
#define    L_U8_CNT_10       10 
#define    L_U8_CNT_11       11 
#define    L_U8_CNT_12       12 
#define    L_U8_CNT_13       13 
#define    L_U8_CNT_14       14 
#define    L_U8_CNT_15       15 
#define    L_U8_CNT_16       16 
#define    L_U8_CNT_17       17 
#define    L_U8_CNT_18       18 
#define    L_U8_CNT_19       19 
#define    L_U8_CNT_20       20 
#define    L_U8_CNT_21       21 
#define    L_U8_CNT_22       22 
#define    L_U8_CNT_23       23 
#define    L_U8_CNT_24       24 
#define    L_U8_CNT_25       25 
#define    L_U8_CNT_26       26 
#define    L_U8_CNT_27       27 
#define    L_U8_CNT_28       28 
#define    L_U8_CNT_29       29 
#define    L_U8_CNT_30       30 
#define    L_U8_CNT_31       31 
#define    L_U8_CNT_32       32 
#define    L_U8_CNT_33       33 
#define    L_U8_CNT_34       34 
#define    L_U8_CNT_35       35 
#define    L_U8_CNT_36       36 
#define    L_U8_CNT_37       37 
#define    L_U8_CNT_38       38 
#define    L_U8_CNT_39       39 
#define    L_U8_CNT_40       40 
#define    L_U8_CNT_41       41 
#define    L_U8_CNT_42       42 
#define    L_U8_CNT_43       43 
#define    L_U8_CNT_44       44 
#define    L_U8_CNT_45       45 
#define    L_U8_CNT_46       46 
#define    L_U8_CNT_47       47 
#define    L_U8_CNT_48       48 
#define    L_U8_CNT_49       49 
#define    L_U8_CNT_50       50 
#define    L_U8_CNT_51       51 
#define    L_U8_CNT_52       52 
#define    L_U8_CNT_53       53 
#define    L_U8_CNT_54       54 
#define    L_U8_CNT_55       55 
#define    L_U8_CNT_56       56 
#define    L_U8_CNT_57       57 
#define    L_U8_CNT_58       58 
#define    L_U8_CNT_59       59 
#define    L_U8_CNT_60       60 
#define    L_U8_CNT_61       61 
#define    L_U8_CNT_62       62 
#define    L_U8_CNT_63       63 
#define    L_U8_CNT_64       64 
#define    L_U8_CNT_65       65 
#define    L_U8_CNT_66       66 
#define    L_U8_CNT_67       67 
#define    L_U8_CNT_68       68 
#define    L_U8_CNT_69       69 
#define    L_U8_CNT_70       70 
#define    L_U8_CNT_71       71 
#define    L_U8_CNT_72       72 
#define    L_U8_CNT_73       73 
#define    L_U8_CNT_74       74 
#define    L_U8_CNT_75       75 
#define    L_U8_CNT_76       76 
#define    L_U8_CNT_77       77 
#define    L_U8_CNT_78       78 
#define    L_U8_CNT_79       79 
#define    L_U8_CNT_80       80 
#define    L_U8_CNT_81       81 
#define    L_U8_CNT_82       82 
#define    L_U8_CNT_83       83 
#define    L_U8_CNT_84       84 
#define    L_U8_CNT_85       85 
#define    L_U8_CNT_86       86 
#define    L_U8_CNT_87       87 
#define    L_U8_CNT_88       88 
#define    L_U8_CNT_89       89 
#define    L_U8_CNT_90       90 
#define    L_U8_CNT_91       91 
#define    L_U8_CNT_92       92 
#define    L_U8_CNT_93       93 
#define    L_U8_CNT_94       94 
#define    L_U8_CNT_95       95 
#define    L_U8_CNT_96       96 
#define    L_U8_CNT_97       97 
#define    L_U8_CNT_98       98 
#define    L_U8_CNT_99       99 
#define    L_U8_CNT_100      100
#define    L_U8_CNT_101      101
#define    L_U8_CNT_102      102
#define    L_U8_CNT_103      103
#define    L_U8_CNT_104      104
#define    L_U8_CNT_105      105
#define    L_U8_CNT_106      106
#define    L_U8_CNT_107      107
#define    L_U8_CNT_108      108
#define    L_U8_CNT_109      109
#define    L_U8_CNT_110      110
#define    L_U8_CNT_111      111
#define    L_U8_CNT_112      112
#define    L_U8_CNT_113      113
#define    L_U8_CNT_114      114
#define    L_U8_CNT_115      115
#define    L_U8_CNT_116      116
#define    L_U8_CNT_117      117
#define    L_U8_CNT_118      118
#define    L_U8_CNT_119      119
#define    L_U8_CNT_120      120
#define    L_U8_CNT_121      121
#define    L_U8_CNT_122      122
#define    L_U8_CNT_123      123
#define    L_U8_CNT_124      124
#define    L_U8_CNT_125      125
#define    L_U8_CNT_126      126
#define    L_U8_CNT_127      127
#define    L_U8_CNT_128      128
#define    L_U8_CNT_129      129
#define    L_U8_CNT_130      130
#define    L_U8_CNT_131      131
#define    L_U8_CNT_132      132
#define    L_U8_CNT_133      133
#define    L_U8_CNT_134      134
#define    L_U8_CNT_135      135
#define    L_U8_CNT_136      136
#define    L_U8_CNT_137      137
#define    L_U8_CNT_138      138
#define    L_U8_CNT_139      139
#define    L_U8_CNT_140      140
#define    L_U8_CNT_141      141
#define    L_U8_CNT_142      142
#define    L_U8_CNT_143      143
#define    L_U8_CNT_144      144
#define    L_U8_CNT_145      145
#define    L_U8_CNT_146      146
#define    L_U8_CNT_147      147
#define    L_U8_CNT_148      148
#define    L_U8_CNT_149      149
#define    L_U8_CNT_150      150
#define    L_U8_CNT_151      151
#define    L_U8_CNT_152      152
#define    L_U8_CNT_153      153
#define    L_U8_CNT_154      154
#define    L_U8_CNT_155      155
#define    L_U8_CNT_156      156
#define    L_U8_CNT_157      157
#define    L_U8_CNT_158      158
#define    L_U8_CNT_159      159
#define    L_U8_CNT_160      160
#define    L_U8_CNT_161      161
#define    L_U8_CNT_162      162
#define    L_U8_CNT_163      163
#define    L_U8_CNT_164      164
#define    L_U8_CNT_165      165
#define    L_U8_CNT_166      166
#define    L_U8_CNT_167      167
#define    L_U8_CNT_168      168
#define    L_U8_CNT_169      169
#define    L_U8_CNT_170      170
#define    L_U8_CNT_171      171
#define    L_U8_CNT_172      172
#define    L_U8_CNT_173      173
#define    L_U8_CNT_174      174
#define    L_U8_CNT_175      175
#define    L_U8_CNT_176      176
#define    L_U8_CNT_177      177
#define    L_U8_CNT_178      178
#define    L_U8_CNT_179      179
#define    L_U8_CNT_180      180
#define    L_U8_CNT_181      181
#define    L_U8_CNT_182      182
#define    L_U8_CNT_183      183
#define    L_U8_CNT_184      184
#define    L_U8_CNT_185      185
#define    L_U8_CNT_186      186
#define    L_U8_CNT_187      187
#define    L_U8_CNT_188      188
#define    L_U8_CNT_189      189
#define    L_U8_CNT_190      190
#define    L_U8_CNT_191      191
#define    L_U8_CNT_192      192
#define    L_U8_CNT_193      193
#define    L_U8_CNT_194      194
#define    L_U8_CNT_195      195
#define    L_U8_CNT_196      196
#define    L_U8_CNT_197      197
#define    L_U8_CNT_198      198
#define    L_U8_CNT_199      199
#define    L_U8_CNT_200      200
#define    L_U8_CNT_201      201
#define    L_U8_CNT_202      202
#define    L_U8_CNT_203      203
#define    L_U8_CNT_204      204
#define    L_U8_CNT_205      205
#define    L_U8_CNT_206      206
#define    L_U8_CNT_207      207
#define    L_U8_CNT_208      208
#define    L_U8_CNT_209      209
#define    L_U8_CNT_210      210
#define    L_U8_CNT_211      211
#define    L_U8_CNT_212      212
#define    L_U8_CNT_213      213
#define    L_U8_CNT_214      214
#define    L_U8_CNT_215      215
#define    L_U8_CNT_216      216
#define    L_U8_CNT_217      217
#define    L_U8_CNT_218      218
#define    L_U8_CNT_219      219
#define    L_U8_CNT_220      220
#define    L_U8_CNT_221      221
#define    L_U8_CNT_222      222
#define    L_U8_CNT_223      223
#define    L_U8_CNT_224      224
#define    L_U8_CNT_225      225
#define    L_U8_CNT_226      226
#define    L_U8_CNT_227      227
#define    L_U8_CNT_228      228
#define    L_U8_CNT_229      229
#define    L_U8_CNT_230      230
#define    L_U8_CNT_231      231
#define    L_U8_CNT_232      232
#define    L_U8_CNT_233      233
#define    L_U8_CNT_234      234
#define    L_U8_CNT_235      235
#define    L_U8_CNT_236      236
#define    L_U8_CNT_237      237
#define    L_U8_CNT_238      238
#define    L_U8_CNT_239      239
#define    L_U8_CNT_240      240
#define    L_U8_CNT_241      241
#define    L_U8_CNT_242      242
#define    L_U8_CNT_243      243
#define    L_U8_CNT_244      244
#define    L_U8_CNT_245      245
#define    L_U8_CNT_246      246
#define    L_U8_CNT_247      247
#define    L_U8_CNT_248      248
#define    L_U8_CNT_249      249
#define    L_U8_CNT_250      250
#define    L_U8_CNT_251      251
#define    L_U8_CNT_252      252
#define    L_U8_CNT_253      253
#define    L_U8_CNT_254      254
#define    L_U8_CNT_255      255
#define    L_U16_CNT_256      256
#define    L_U16_CNT_257      257
#define    L_U16_CNT_258      258
#define    L_U16_CNT_259      259
#define    L_U16_CNT_260      260
#define    L_U16_CNT_261      261
#define    L_U16_CNT_262      262
#define    L_U16_CNT_263      263
#define    L_U16_CNT_264      264
#define    L_U16_CNT_265      265
#define    L_U16_CNT_266      266
#define    L_U16_CNT_267      267
#define    L_U16_CNT_268      268
#define    L_U16_CNT_269      269
#define    L_U16_CNT_270      270
#define    L_U16_CNT_271      271
#define    L_U16_CNT_272      272
#define    L_U16_CNT_273      273
#define    L_U16_CNT_274      274
#define    L_U16_CNT_275      275
#define    L_U16_CNT_276      276
#define    L_U16_CNT_277      277
#define    L_U16_CNT_278      278
#define    L_U16_CNT_279      279
#define    L_U16_CNT_280      280
#define    L_U16_CNT_281      281
#define    L_U16_CNT_282      282
#define    L_U16_CNT_283      283
#define    L_U16_CNT_284      284
#define    L_U16_CNT_285      285
#define    L_U16_CNT_286      286
#define    L_U16_CNT_287      287
#define    L_U16_CNT_288      288
#define    L_U16_CNT_289      289
#define    L_U16_CNT_290      290
#define    L_U16_CNT_291      291
#define    L_U16_CNT_292      292
#define    L_U16_CNT_293      293
#define    L_U16_CNT_294      294
#define    L_U16_CNT_295      295
#define    L_U16_CNT_296      296
#define    L_U16_CNT_297      297
#define    L_U16_CNT_298      298
#define    L_U16_CNT_299      299
#define    L_U16_CNT_300      300
#define    L_U16_CNT_301      301
#define    L_U16_CNT_302      302
#define    L_U16_CNT_303      303
#define    L_U16_CNT_304      304
#define    L_U16_CNT_305      305
#define    L_U16_CNT_306      306
#define    L_U16_CNT_307      307
#define    L_U16_CNT_308      308
#define    L_U16_CNT_309      309
#define    L_U16_CNT_310      310
#define    L_U16_CNT_311      311
#define    L_U16_CNT_312      312
#define    L_U16_CNT_313      313
#define    L_U16_CNT_314      314
#define    L_U16_CNT_315      315
#define    L_U16_CNT_316      316
#define    L_U16_CNT_317      317
#define    L_U16_CNT_318      318
#define    L_U16_CNT_319      319
#define    L_U16_CNT_320      320
#define    L_U16_CNT_321      321
#define    L_U16_CNT_322      322
#define    L_U16_CNT_323      323
#define    L_U16_CNT_324      324
#define    L_U16_CNT_325      325
#define    L_U16_CNT_326      326
#define    L_U16_CNT_327      327
#define    L_U16_CNT_328      328
#define    L_U16_CNT_329      329
#define    L_U16_CNT_330      330
#define    L_U16_CNT_331      331
#define    L_U16_CNT_332      332
#define    L_U16_CNT_333      333
#define    L_U16_CNT_334      334
#define    L_U16_CNT_335      335
#define    L_U16_CNT_336      336
#define    L_U16_CNT_337      337
#define    L_U16_CNT_338      338
#define    L_U16_CNT_339      339
#define    L_U16_CNT_340      340
#define    L_U16_CNT_341      341
#define    L_U16_CNT_342      342
#define    L_U16_CNT_343      343
#define    L_U16_CNT_344      344
#define    L_U16_CNT_345      345
#define    L_U16_CNT_346      346
#define    L_U16_CNT_347      347
#define    L_U16_CNT_348      348
#define    L_U16_CNT_349      349
#define    L_U16_CNT_350      350
#define    L_U16_CNT_351      351
#define    L_U16_CNT_352      352
#define    L_U16_CNT_353      353
#define    L_U16_CNT_354      354
#define    L_U16_CNT_355      355
#define    L_U16_CNT_356      356
#define    L_U16_CNT_357      357
#define    L_U16_CNT_358      358
#define    L_U16_CNT_359      359
#define    L_U16_CNT_360      360
#define    L_U16_CNT_361      361
#define    L_U16_CNT_362      362
#define    L_U16_CNT_363      363
#define    L_U16_CNT_364      364
#define    L_U16_CNT_365      365
#define    L_U16_CNT_366      366
#define    L_U16_CNT_367      367
#define    L_U16_CNT_368      368
#define    L_U16_CNT_369      369
#define    L_U16_CNT_370      370
#define    L_U16_CNT_371      371
#define    L_U16_CNT_372      372
#define    L_U16_CNT_373      373
#define    L_U16_CNT_374      374
#define    L_U16_CNT_375      375
#define    L_U16_CNT_376      376
#define    L_U16_CNT_377      377
#define    L_U16_CNT_378      378
#define    L_U16_CNT_379      379
#define    L_U16_CNT_380      380
#define    L_U16_CNT_381      381
#define    L_U16_CNT_382      382
#define    L_U16_CNT_383      383
#define    L_U16_CNT_384      384
#define    L_U16_CNT_385      385
#define    L_U16_CNT_386      386
#define    L_U16_CNT_387      387
#define    L_U16_CNT_388      388
#define    L_U16_CNT_389      389
#define    L_U16_CNT_390      390
#define    L_U16_CNT_391      391
#define    L_U16_CNT_392      392
#define    L_U16_CNT_393      393
#define    L_U16_CNT_394      394
#define    L_U16_CNT_395      395
#define    L_U16_CNT_396      396
#define    L_U16_CNT_397      397
#define    L_U16_CNT_398      398
#define    L_U16_CNT_399      399
#define    L_U16_CNT_400      400
#define    L_U16_CNT_401      401
#define    L_U16_CNT_402      402
#define    L_U16_CNT_403      403
#define    L_U16_CNT_404      404
#define    L_U16_CNT_405      405
#define    L_U16_CNT_406      406
#define    L_U16_CNT_407      407
#define    L_U16_CNT_408      408
#define    L_U16_CNT_409      409
#define    L_U16_CNT_410      410
#define    L_U16_CNT_411      411
#define    L_U16_CNT_412      412
#define    L_U16_CNT_413      413
#define    L_U16_CNT_414      414
#define    L_U16_CNT_415      415
#define    L_U16_CNT_416      416
#define    L_U16_CNT_417      417
#define    L_U16_CNT_418      418
#define    L_U16_CNT_419      419
#define    L_U16_CNT_420      420
#define    L_U16_CNT_421      421
#define    L_U16_CNT_422      422
#define    L_U16_CNT_423      423
#define    L_U16_CNT_424      424
#define    L_U16_CNT_425      425
#define    L_U16_CNT_426      426
#define    L_U16_CNT_427      427
#define    L_U16_CNT_428      428
#define    L_U16_CNT_429      429
#define    L_U16_CNT_430      430
#define    L_U16_CNT_431      431
#define    L_U16_CNT_432      432
#define    L_U16_CNT_433      433
#define    L_U16_CNT_434      434
#define    L_U16_CNT_435      435
#define    L_U16_CNT_436      436
#define    L_U16_CNT_437      437
#define    L_U16_CNT_438      438
#define    L_U16_CNT_439      439
#define    L_U16_CNT_440      440
#define    L_U16_CNT_441      441
#define    L_U16_CNT_442      442
#define    L_U16_CNT_443      443
#define    L_U16_CNT_444      444
#define    L_U16_CNT_445      445
#define    L_U16_CNT_446      446
#define    L_U16_CNT_447      447
#define    L_U16_CNT_448      448
#define    L_U16_CNT_449      449
#define    L_U16_CNT_450      450
#define    L_U16_CNT_451      451
#define    L_U16_CNT_452      452
#define    L_U16_CNT_453      453
#define    L_U16_CNT_454      454
#define    L_U16_CNT_455      455
#define    L_U16_CNT_456      456
#define    L_U16_CNT_457      457
#define    L_U16_CNT_458      458
#define    L_U16_CNT_459      459
#define    L_U16_CNT_460      460
#define    L_U16_CNT_461      461
#define    L_U16_CNT_462      462
#define    L_U16_CNT_463      463
#define    L_U16_CNT_464      464
#define    L_U16_CNT_465      465
#define    L_U16_CNT_466      466
#define    L_U16_CNT_467      467
#define    L_U16_CNT_468      468
#define    L_U16_CNT_469      469
#define    L_U16_CNT_470      470
#define    L_U16_CNT_471      471
#define    L_U16_CNT_472      472
#define    L_U16_CNT_473      473
#define    L_U16_CNT_474      474
#define    L_U16_CNT_475      475
#define    L_U16_CNT_476      476
#define    L_U16_CNT_477      477
#define    L_U16_CNT_478      478
#define    L_U16_CNT_479      479
#define    L_U16_CNT_480      480
#define    L_U16_CNT_481      481
#define    L_U16_CNT_482      482
#define    L_U16_CNT_483      483
#define    L_U16_CNT_484      484
#define    L_U16_CNT_485      485
#define    L_U16_CNT_486      486
#define    L_U16_CNT_487      487
#define    L_U16_CNT_488      488
#define    L_U16_CNT_489      489
#define    L_U16_CNT_490      490
#define    L_U16_CNT_491      491
#define    L_U16_CNT_492      492
#define    L_U16_CNT_493      493
#define    L_U16_CNT_494      494
#define    L_U16_CNT_495      495
#define    L_U16_CNT_496      496
#define    L_U16_CNT_497      497
#define    L_U16_CNT_498      498
#define    L_U16_CNT_499      499
#define    L_U16_CNT_500	  500


// SPEED PARAMETER
#define     VREF_0_KPH          0               // 0.125 km/h resolution
#define     VREF_0G125_KPH      1
#define     VREF_0_125_KPH      1
#define     VREF_0_25_KPH       2
#define     VREF_0_375_KPH      3
#define     VREF_0_5_KPH        4
#define     VREF_0_KPH5         4
#define     VREF_0_75_KPH       6
#define     VREF_0_875_KPH      7
#define     VREF_1_KPH          8
#define     VREF_1_25_KPH       10
#define     VREF_1_375_KPH      11
#define     VREF_1_5_KPH        12
#define     VREF_1_75_KPH       14
#define     VREF_2_KPH          16
#define     VREF_2_125_KPH      17
#define     VREF_2_25_KPH       18
#define     VREF_2_375_KPH      19
#define     VREF_2_5_KPH        20
#define     VREF_2_75_KPH       22
#define     VREF_3_KPH          24
#define     VREF_3_5_KPH        28
#define     VREF_4_KPH          32
#define     VREF_4_25_KPH       34
#define     VREF_5_KPH          40
#define     VREF_6_KPH          48
#define     VREF_7_KPH          56
#define     VREF_8_KPH          64
#define		VREF_9_KPH			72
#define     VREF_10_KPH         80
#define     VREF_11_KPH         88
#define     VREF_12_KPH         96
#define     VREF_12_5_KPH       100
#define     VREF_13_KPH         104
#define     VREF_14_KPH         112
#define     VREF_15_KPH         120
#define     VREF_16_KPH         128
#define     VREF_17_KPH         136
#define     VREF_18_KPH         144
#define     VREF_18_75_KPH      150
#define     VREF_20_KPH         160
#define     VREF_21_KPH         168
#define     VREF_23_KPH         184
#define     VREF_24_KPH         192
#define     VREF_25_KPH         200
#define     VREF_27_KPH         216
#define     VREF_30_KPH         240
#define     VREF_32_KPH         256
#define     VREF_35_KPH         280
#define     VREF_38_KPH         304
#define     VREF_40_KPH         320
#define     VREF_45_KPH         360
#define     VREF_47_KPH         376
#define     VREF_50_KPH         400
#define     VREF_55_KPH			440
#define     VREF_60_KPH         480
#define     VREF_70_KPH         560
#define     VREF_80_KPH         640
#define     VREF_90_KPH         720
#define     VREF_100_KPH        800
#define     VREF_110_KPH        880
#define     VREF_120_KPH        960
#define     VREF_130_KPH        1040
#define     VREF_140_KPH        1120
#define     VREF_150_KPH        1200
#define     VREF_180_KPH        1440
#define     VREF_200_KPH        1600
#define     VREF_250_KPH        2000
#define     VREF_MIN            16
#define     VREF_MAX            2040

#if __WL_SPEED_RESOL_CHANGE == 1
#define     VREF_0G125_KPH_RESOL_CHANGE      8
#define     VREF_0_25_KPH_RESOL_CHANGE       16
#define     VREF_0_375_KPH_RESOL_CHANGE      24
#define     VREF_0_5_KPH_RESOL_CHANGE        32
#define     VREF_0_75_KPH_RESOL_CHANGE       48
#define     VREF_1_KPH_RESOL_CHANGE          64
#define     VREF_1_25KPH_RESOL_CHANGE        80
#define     VREF_1_5_KPH_RESOL_CHANGE        96
#define     VREF_2_KPH_RESOL_CHANGE          128
#define     VREF_3_KPH_RESOL_CHANGE          192
#define     VREF_4_KPH_RESOL_CHANGE          256
#define     VREF_5_KPH_RESOL_CHANGE          320
#define     VREF_6_KPH_RESOL_CHANGE          384
#define     VREF_7_KPH_RESOL_CHANGE          448
#define     VREF_8_KPH_RESOL_CHANGE          512
#define     VREF_10_KPH_RESOL_CHANGE         640
#define     VREF_12_5_KPH_RESOL_CHANGE       800
#define 	VREF_15_KPH_RESOL_CHANGE		 960
#define     VREF_16_KPH_RESOL_CHANGE         1024
#define     VREF_18_75_KPH_RESOL_CHANGE      1200
#define     VREF_20_KPH_RESOL_CHANGE         1280
#define     VREF_24_KPH_RESOL_CHANGE         1536
#define     VREF_32_KPH_RESOL_CHANGE         2048
#define     VREF_100_KPH_RESOL_CHANGE        6400
#define     VREF_120_KPH_RESOL_CHANGE        7680
#define     VREF_MIN_RESOL_CHANGE            128
#define     VREF_MAX_RESOL_CHANGE            16320
    #if __SIDE_VREF ==1
// SIDE WHEEL REFERENCE ACCELERATION PARAMETER         04.11.10 by K.S.W.
#define     ALIMIT_POS_0_G1     2
#define     ALIMIT_POS_0_G5     8
#define     ALIMIT_POS_0_G7     11
#define     ALIMIT_POS_1_G      16
#define     ALIMIT_POS_1_G25    20
#define     ALIMIT_POS_1_G5     24
#define     ALIMIT_POS_2_G      32
#define     ALIMIT_NEG_0_G1     -2
#define     ALIMIT_NEG_0_G2     -3
#define     ALIMIT_NEG_0_G25    -4
#define     ALIMIT_NEG_0_G3     -5
#define     ALIMIT_NEG_0_G4     -6
#define     ALIMIT_NEG_0_G5     -8
#define     ALIMIT_NEG_1_G      -16
#define     ALIMIT_NEG_1_G25    -20
#define     ALIMIT_NEG_1_G5     -24
    #endif
#endif

#if __WL_SPEED_RESOL_CHANGE ==1
#define     VREF_MIN_SPEED  8
#define     VREF_MAX_SPEED  16320

#else
#define     VREF_MIN_SPEED  1
#define     VREF_MAX_SPEED  2040

#endif
#define     VREF_MIN_SPEED_RESOL_8  1

// DISC TEMPERATURE PARAMETER
#define     TMP_0               0
#define     TMP_20              640             // 0.03125 resolution
#define     TMP_80              2560
#define     TMP_200             6400
#define     TMP_300             9600
#define     TMP_400             12800
#define     TMP_500             16000
#define     TMP_600             19200
#define     TMP_650             20800
#define     TMP_980             31360

/* TEMPERATURE - R:0.01'C*/
#define     TMP_0_DC               	0
#define     TMP_5_DC               	500
#define     TMP_23_DC               2300
#define     TMP_25_DC               2500
#define     TMP_85_DC               8500
#define     TMP_M_40_DC             -4000

// WHEEL SLIP PARAMETER
#define     LAM_1P              1               // percentage
#define     LAM_2P              2
#define     LAM_3P              3
#define     LAM_4P              4
#define     LAM_5P              5
#define     LAM_6P              6
#define     LAM_7P              7
#define     LAM_8P              8
#define     LAM_10P             10
#define     LAM_12P             12
#define     LAM_14P             14
#define     LAM_15P             15
#define     LAM_17P             17
#define     LAM_20P             20
#define     LAM_22P             22
#define     LAM_24P             24
#define     LAM_25P             25
#define     LAM_30P             30
#define     LAM_35P             35
#define     LAM_40P             40
#define     LAM_45P             45
#define     LAM_50P             50
#define     LAM_60P             60
#define     LAM_70P             70
#define     LAM_80P             80
#define     LAM_90P             90
#define     LAM_100P           100

#define     LAMUDA_1            7
#define     LAMUDA_2            30
#define     LAMUDA_3            50
#define     LAMUDA_4            70

// WHEEL ACCELERATION PARAMETER
#define     ARAD_0G0            0               // 0.25g resolution
#define     ARAD_0G25           1
#define     ARAD_0G5            2
#define     ARAD_0G75           3
#define     ARAD_1G0            4
#define     ARAD_1G25           5
#define     ARAD_1G5            6
#define     ARAD_1G75           7
#define     ARAD_2G0            8
#define     ARAD_2G5            10
#define     ARAD_3G0            12
#define     ARAD_3G5            14
#define     ARAD_4G0            16
#define     ARAD_5G0            20
#define     ARAD_5G5            22
#define     ARAD_6G0            24
#define     ARAD_7G0            28
#define     ARAD_8G0            32
#define     ARAD_9G0            36
#define     ARAD_10G0           40
#define     ARAD_12G0           48
#define     ARAD_15G0           60
#define     ARAD_18G0           72
#define     ARAD_20G0           80

#define     ARAD32_0G0            0
#define     ARAD32_0G125          4
#define     ARAD32_0G25           8
#define     ARAD32_0G5           16
#define     ARAD32_0G75          24
#define     ARAD32_1G0           32
#define     ARAD32_1G125         36
#define     ARAD32_1G25          40
#define     ARAD32_1G5           48
#define     ARAD32_1G75          56
#define     ARAD32_2G0           64
#define     ARAD32_2G5           80
#define     ARAD32_3G0           96
#define     ARAD32_10G0          320

#define     FACTOR_FOR_ARAD_95      95          // percentage
#define     FACTOR_FOR_ARAD_105     105
#define     FACTOR_FOR_ARAD_80      80
#define     FACTOR_FOR_ARAD_120     120


// VEHICLE ACCELERATION PARAMETER
#define     ACC_OFFSET          256             // 2.5V VALUE
#define     AFZ_0G0             0               // 0.01g resolution
#define     AFZ_0G01            1
#define     AFZ_0G02            2
#define     AFZ_0G03            3
#define     AFZ_0G04            4
#define     AFZ_0G05            5
#define     AFZ_0G06            6
#define     AFZ_0G08            8
#define     AFZ_0G09            9
#define     AFZ_0G1             10
#define     AFZ_0G12            12
#define     AFZ_0G15            15
#define     AFZ_0G18            18
#define     AFZ_0G2             20
#define     AFZ_0G22            22
#define     AFZ_0G25            25
#define     AFZ_0G27            27
#define     AFZ_0G3             30
#define     AFZ_0G32            32
#define     AFZ_0G35            35
#define     AFZ_0G36            36
#define     AFZ_0G4             40
#define     AFZ_0G45            45
#define     AFZ_0G5             50
#define     AFZ_0G55            55
#define     AFZ_0G6             60
#define     AFZ_0G7             70
#define     AFZ_0G75            75
#define     AFZ_0G8             80
#define 	AFZ_0G9				90
#define     AFZ_1G0             100
#define     AFZ_POS_LIMIT       150
#define     AFZ_NEG_LIMIT       -150



        
#define     T_10_MS             1
#define     T_20_MS				2      
#define     T_30_MS             3  
#define     T_50_MS             5        
#define     T_100_MS            10
#define     T_130_MS            13
#define     T_150_MS            15
#define     T_200_MS            20
#define     T_250_MS            25
#define     T_300_MS            30
#define     T_350_MS            35
#define     T_400_MS            40
#define     T_450_MS            45
#define     T_500_MS            50
#define     T_550_MS            55
#define     T_600_MS            60
#define     T_700_MS            70
#define     T_800_MS            80
#define     T_900_MS            90
#define     T_1000_MS           100
#define     T_1500_MS           150
#define     T_1750_MS           175 
#define     T_2_S               200
#define     T_3_S               300
//#define     T_4S                400
#define     T_4S1               410
#define     T_4S2               420
#define     T_4S3               430
#define     T_4S4               440
#define     T_4S5               450
#define     T_5_S               500
#define     T_10_S              1000
#define     T_20_S              2000
#define     T_1MIN              6000
#define     T_3MIN              18000
#define     T_5MIN              30000
#define     T_7MIN              42000
#define     T_30MIN             180000
#define     T_4_S               400  
#define     T_8_S 	            800 
#define     T_12_S	            1200 
#define     T_16_S	            1600 
#define     T_20_S	            2000 
#define     T_24_S	            2400 
#define     T_28_S	            2800 
#define     T_32_S	            3200 
#define     T_36_S	            3600 
#define     T_40_S	            4000 
#define     T_44_S	            4400 
#define     T_48_S	            4800 
#define     T_52_S	            5200
#define     T_56_S	            5600
#define     T_60_S	            6000
#define     T_64_S	            6400
#define     T_68_S	            6800
#define     T_72_S	            7200
#define     T_76_S	            7600
#define     T_80_S	            8000
#define     T_84_S	            8400
#define     T_88_S	            8800
#define     T_92_S	            9200
#define     T_96_S	            9600
#define     T_100_S	            10000


#define     MTP_0_P             0
#define     MTP_1_P             1
#define     MTP_2_P             2
#define     MTP_3_P             3
#define     MTP_4_P             4
#define     MTP_5_P             5
#define     MTP_6_P             6
#define     MTP_7_P             7
#define     MTP_8_P             8
#define     MTP_9_P             9
#define     MTP_10_P            10
#define     MTP_15_P            15
#define     MTP_20_P            20
#define     MTP_25_P            25
#define     MTP_30_P            30
#define     MTP_35_P            35
#define     MTP_40_P            40
#define     MTP_45_P            45
#define     MTP_50_P            50
#define     MTP_55_P            55
#define     MTP_60_P            60
#define     MTP_65_P            65
#define     MTP_70_P            70
#define     MTP_75_P            75
#define     MTP_80_P            80
#define     MTP_85_P            85
#define     MTP_90_P            90
#define     MTP_95_P            95
#define     MTP_100_P          100


#define     PERCENTAGE_12G5P    32                /* percentage * 2.56 */
#define     PERCENTAGE_70P      179
#define     PERCENTAGE_80P      205
#define     PERCENTAGE_90P      230
#define     PERCENTAGE_93P      238
#define     PERCENTAGE_95P      243

#define     ABMITTEL_FZ_60P     198             // percentage * 4.70
#define     ABMITTEL_FZ_80P     264

#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS7  18
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS6  16
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS5  13
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS4  10
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS3  8
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS2  6
#define     DV_BASE_PRO_FOR_FRONT_OUT_ABS1  3

#define     DV_BASE_PRO_FOR_REAR_OUT_ABS6   16
#define     DV_BASE_PRO_FOR_REAR_IN_ABS3    8
#define     DV_BASE_PRO_FOR_REAR_OUT_ABS4   10

#define     DV_BASE_PRO_FOR_FRONT_IN_ABS9   23
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS8   20
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS7   18
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS6   16
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS5   13
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS4   10
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS3   8
#define     DV_BASE_PRO_FOR_FRONT_IN_ABS2   6
#define     DV_BASE_PRO_FOR_REAR_IN_ABS4    10
#define     DV_BASE_PRO_FOR_REAR_IN_ABS2    6

//#define     DV_BASE_LOW_FOR_FRONT_OUT_ABS   24    /* 0.125 km/h resolution */
//#define     DV_BASE_LOW_FOR_REAR_OUT_ABS    20
#define     DV_BASE_LOW_FOR_35_IN_ABS       28
#define     DV_BASE_LOW_FOR_IN_ABS_5KPH     40
#define     DV_BASE_LOW_FOR_IN_ABS_4_5KPH   36
#define     DV_BASE_LOW_FOR_IN_ABS_4KPH     32
#define     DV_BASE_LOW_FOR_IN_ABS_3_5KPH   28
#define     DV_BASE_LOW_FOR_IN_ABS_3KPH     24
#define     DV_BASE_LOW_FOR_IN_ABS_2_5KPH   20
#define     DV_BASE_LOW_FOR_IN_ABS_2KPH     16
#define     DV_BASE_LOW_FOR_IN_ABS_1_5KPH   12
#define     DV_BASE_LOW_FOR_IN_ABS_1KPH     8
#define     DV_BASE_LOW_FOR_IN_ABS_0_75KPH  6
#define     DV_BASE_LOW_FOR_IN_ABS_0_5KPH   4
#define     DV_BASE_LOW_FOR_IN_ABS          20
//#define     DV_BASE_LOW_FOR_GMA             16            // Original Std
//#define     DV_BASE_LOW_FOR_GMA             8               // For LSV by csh
//#define     DV_BASE_LOW_FOR_FRONT_IN_ABS_HI_MU    20      // Original Std
//#define     DV_BASE_LOW_FOR_FRONT_LOW_SPEED       12
#define     DV_BASE_LOW_FOR_FRONT_IN_ABS_HI_MU    8        // For LSV by csh 2%
#define     DV_BASE_LOW_FOR_FRONT_LOW_SPEED       4
//#define     DV_BASE_LOW_FOR_FRONT_IN_ABS_HI_MU    12        // For LSV by csh 3%
//#define     DV_BASE_LOW_FOR_FRONT_LOW_SPEED       8
//#define     DV_BASE_LOW_FOR_REAR_IN_ABS_HI_MU     20      // Original Std
//#define     DV_BASE_LOW_FOR_REAR_LOW_SPEED        16
//#define     DV_BASE_LOW_FOR_REAR_IN_ABS_HI_MU     12        // For LSV by csh
#define     DV_BASE_LOW_FOR_REAR_IN_ABS_HI_MU     10        // For LSV by csh 2%, 3%
#define     DV_BASE_LOW_FOR_REAR_LOW_SPEED        8

//#define     DV_BASE_HIGH_FOR_FRONT_OUT_ABS  48
//#define     DV_BASE_HIGH_FOR_REAR_OUT_ABS   32
#define     DV_BASE_HIGH_FOR_FRONT_IN_ABS   48
#define     DV_BASE_HIGH_FOR_REAR_IN_ABS    32
#define     VFILTN_MAX_PRO                  128 // percentage * 2.56
#define     ABS_DETECT_SLIP_THRESHOLD_PRO   33
#define     VFILT_ROUGH_PER_PEAK_ACC        1   // 0.5 kph/1 g
#define     VFILT_ROUGH_PER_ARAD            4   // 2 kph/1 g
#define     VFILT_ROUGH_PER_ABS             2   // 1 kph/1 g
#define     VFILT_ROUGH_PER_OUTABS          6   // 3 kph/1 g
#define		VFILT_ROUGH_PER_OUTABS_LOW_SPD	9   // 4.5 kph/1 g
#define     VFILT_ROUGH_DEC                 4   // 0.5 kph/scantime

#define     ABS_OFF_REAPPLY_TIME_SHORT      55  // 7.08 msec resolution
#define     ABS_OFF_REAPPLY_TIME_LONG       142
#define     T_AQUA_LIMIT                    254
#define     T_AQUA                          140
#define     T_AV_RL_LONG_DUMP               49

#define     MINI_WHEEL_CHECK_RATE           5   // percentage
#define     MINI_WHEEL_BEND_RATE            4
//#define       MINI_WHEEL_DOWN_RATE            93  // percentage,GK(93)XD(91)
//#define       MINI_WHEEL_UP_RATE              107 // percentage,GK(107)XD(110)
#define     SPIN_RATE                       20  // percentage

#define     DETECTION           0
#define     DECEL               1
#define     ACCEL               2
/*MISRA, H22*/
/*#define     BTCS_STATE1         3
#define     BTCS_STATE2         4*/

#define     UNSTABLE            10
#define     UNDECIDED           1
#define     CHECK_DETECTION     2
#define     SETTLED             3
#define     REAPPLY_MODE        10
#define     BUILT_MODE          11
#define     HOLD_MODE           12
#define     DUMP_START          13
#define     DUMP_YMR            14
#define     REAR_MSL_MODE       15
#define     ESP_ABS_COOPERATION_MODE        16
#define     UNSTABLE_REAPPLY_MODE        17
#define     UNSTABLE_REAPPLY_HOLD_MODE        18

/**********************************************************/
/* Solenoid Coil Current Compensation                     */
/**********************************************************/
#define     NO_HMON_UPPER_LIMIT     1100//MGH60: 350     // MGH25 :542
#define     NO_HMON_LOWER_LIMIT     760//MGH60: 240     // MGH25 :341
#define     NO_LMON_UPPER_LIMIT     440//MGH60: 210     // MGH25 :337
#define     NO_LMON_LOWER_LIMIT     200//MGH60: 80      // MGH25 :136    
#define     VOLT_RESISTER_CONST     20003//MGH60: 3163  // MGH25 :2108 // VOLT_ref * (Rs / Rc_ref)
#define 	VCC_ADC_VOLT			4096 //MGH60: 1024  // MGH25 :1024
#define		AD_SCALE_FACTOR			1024 //MGH60: 128   // MGH25 :128

// TKTK
/*
#define     TIME                    1
*/

#define     WSTR_LEFT_LIMIT         WSTR_ZERO_POINT-340
#define     WSTR_RIGHT_LIMIT        WSTR_ZERO_POINT+340
#define     WSTR_ZERO_POINT         453

#define     ALAT_ZERO_POINT         518
#define     ALAT_ZERO_LOW_LIMIT     120
#define     ALAT_ZERO_HI_LIMIT      140
/* #if __SIMENS_YAW_LG
#define     ALAT_RESOLUTION         367
#else */ /* 2006.06.27  K.G.Y */
#define     ALAT_RESOLUTION         48          // r : 0.0001
/* #endif */ /* 2006.06.27  K.G.Y */

#define     YAW_ZERO_POINT          510

/* #if __SIMENS_YAW_LG
#define     YAW_RESOLUTION          183
#else */ /* 2006.06.27  K.G.Y */
#define     YAW_RESOLUTION          25          // r : 0.01
/* #endif */ /* 2006.06.27  K.G.Y */

#define     YAW_VREF_ZERO_TIME      286         //2sec
#define     YAW_ZERO_TIME           150         //2sec
#define     ALAT_ZERO_TIME          1000        //2sec

#define     PRESS_ZERO_POINT        119
#define     PRESS_RESOLUTION        21          // 20.8, r : 0.01

/**********************************************************/
/* Vehicle Speed KPH resolution : 0.1                     */
/**********************************************************/
#define     VREF_K_6_KPH        60
#define     VREF_K_15_KPH       150
#define     VREF_K_20_KPH       200
#define     VREF_K_30_KPH       300
#define     VREF_K_40_KPH       400
#define     VREF_K_50_KPH       500
#define     VREF_K_60_KPH       600
#define     VREF_K_70_KPH       700
#define     VREF_K_80_KPH       800
#define     VREF_K_90_KPH       900
#define     VREF_K_100_KPH      1000
#define     VREF_K_120_KPH      1200
#define     VREF_K_150_KPH      1500
#define     VREF_K_180_KPH      1800
/**********************************************************/
/* YAW                                                    */
/**********************************************************/

#define     YAW_0G01DEG         1
#define     YAW_0G02DEG         2
#define     YAW_0G03DEG         3
#define     YAW_0G05DEG         5
#define     YAW_0G1DEG          10
#define     YAW_0G2DEG          20
#define     YAW_0G25DEG         25
#define     YAW_0G4DEG          40
#define     YAW_0G5DEG          50
#define     YAW_0G8DEG          80
#define     YAW_1DEG            100
#define     YAW_1G5DEG          150
#define     YAW_2DEG            200
#define     YAW_2G5DEG          250
#define     YAW_2G7DEG          270
#define     YAW_3DEG            300
#define     YAW_3G5DEG          350
#define     YAW_4DEG            400
#define     YAW_4G5DEG          450
#define     YAW_5DEG            500
#define     YAW_5G5DEG          550
#define     YAW_6DEG            600
#define     YAW_6G5DEG          650
#define     YAW_7DEG            700
#define     YAW_8DEG            800
#define     YAW_9DEG            900
#define     YAW_10DEG           1000
#define     YAW_11DEG           1100
#define     YAW_12DEG           1200
#define     YAW_13DEG           1300
#define     YAW_15DEG           1500
#define     YAW_16DEG           1600
#define     YAW_20DEG           2000
#define     YAW_25DEG           2500
#define     YAW_30DEG           3000
#define     YAW_40DEG           4000
#define     YAW_50DEG           5000
#define     YAW_60DEG           6000
#define     YAW_70DEG           7000
#define     YAW_75DEG           7500
#define     YAW_90DEG           9000

#define     YAW_SENSOR_CHANGE_LIMIT 350  // 500 deg/s^2*0.007 = 3.5deg/s

/**********************************************************/
/* LAT                                                    */
/**********************************************************/

#define     LAT_RATE_CYCLE      14
#define     LAT_0G002G           2
#define     LAT_0G005G           5
#define     LAT_0G01G           10
#define     LAT_0G02G           20
#define     LAT_0G05G           50
#define     LAT_0G07G           70
#define     LAT_0G08G           80
#define     LAT_0G09G           90
#define     LAT_0G1G            100
#define     LAT_0G15G           150
#define     LAT_0G17G           170
#define     LAT_0G2G            200
#define     LAT_0G25G           250
#define     LAT_0G26G           260 // 15 deg에 해당.
#define     LAT_0G3G            300
#define     LAT_0G4G            400
#define     LAT_0G45G           450
#define     LAT_0G5G            500
#define     LAT_0G55G           550
#define     LAT_0G6G            600
#define     LAT_0G65G			650
#define     LAT_0G7G            700
#define     LAT_0G8G            800
#define     LAT_0G85G           850
#define     LAT_1G              1000
#define     LAT_1G3G            1300
#define     LAT_1G5G            1500
#define     LAT_4G              4000

#define     LAT_CHANGE_LIMIT    140  // 20g/sec*0.007 = 0.14

/**********************************************************/
/* WSTR                                                   */
/**********************************************************/

#define     WSTR_0_DEG          0
#define     WSTR_1_DEG          10
#define     WSTR_2_DEG          20
#define     WSTR_2G5_DEG        25
#define     WSTR_3_DEG          30
#define     WSTR_3G5_DEG        35
#define     WSTR_4_DEG          40
#define     WSTR_5_DEG          50
#define     WSTR_8_DEG          80
#define     WSTR_10_DEG         100
#define     WSTR_12_DEG         120
#define     WSTR_15_DEG         150
#define     WSTR_20_DEG         200
#define     WSTR_25_DEG         250
#define     WSTR_30_DEG         300
#define     WSTR_40_DEG         400
#define     WSTR_45_DEG         450
#define     WSTR_50_DEG         500
#define     WSTR_55_DEG         550
#define     WSTR_60_DEG         600
#define     WSTR_70_DEG         700
#define     WSTR_80_DEG         800
#define     WSTR_90_DEG         900
#define     WSTR_100_DEG        1000
#define     WSTR_110_DEG        1100
#define     WSTR_120_DEG        1200
#define     WSTR_130_DEG        1300
#define     WSTR_180_DEG        1800
#define     WSTR_200_DEG        2000
#define     WSTR_220_DEG        2200
#define     WSTR_250_DEG        2500
#define     WSTR_260_DEG        2600
#define     WSTR_270_DEG        2700
#define     WSTR_360_DEG        3600
#define     WSTR_450_DEG        4500
#define     WSTR_500_DEG        5000
#define     WSTR_550_DEG        5500
#define     WSTR_650_DEG        6500
#define     WSTR_720_DEG        7200
#define     WSTR_850_DEG        8500

#define     WSTR_CHANGE_LIMIT   105 // 1500*0.007 = 10.5

#define     WSTR_ZERO_L         998
#define     WSTR_ZERO_R         1003

/**********************************************************/
/* WSTR  DOT  : 1->0.1deg/s                               */
/**********************************************************/
#define	WSTR_DOT_10DPS	    100
#define	WSTR_DOT_20DPS	    200
#define	WSTR_DOT_30DPS	    300
#define	WSTR_DOT_40DPS	    400
#define	WSTR_DOT_50DPS	    500
#define	WSTR_DOT_60DPS	    600
#define	WSTR_DOT_70DPS	    700
#define	WSTR_DOT_80DPS	    800
#define	WSTR_DOT_90DPS	    900
#define	WSTR_DOT_100DPS	    1000
#define	WSTR_DOT_110DPS	    1100
#define	WSTR_DOT_120DPS	    1200
#define	WSTR_DOT_130DPS	    1300
#define	WSTR_DOT_140DPS	    1400
#define	WSTR_DOT_150DPS	    1500
#define	WSTR_DOT_160DPS	    1600
#define	WSTR_DOT_170DPS	    1700
#define	WSTR_DOT_180DPS	    1800
#define	WSTR_DOT_190DPS	    1900
#define	WSTR_DOT_200DPS	    2000
#define	WSTR_DOT_210DPS	    2100
#define	WSTR_DOT_220DPS	    2200
#define	WSTR_DOT_230DPS	    2300
#define	WSTR_DOT_240DPS	    2400
#define	WSTR_DOT_250DPS	    2500
#define	WSTR_DOT_260DPS	    2600
#define	WSTR_DOT_270DPS	    2700
#define	WSTR_DOT_280DPS	    2800
#define	WSTR_DOT_290DPS	    2900
#define	WSTR_DOT_300DPS	    3000
#define	WSTR_DOT_310DPS	    3100
#define	WSTR_DOT_320DPS	    3200
#define	WSTR_DOT_330DPS	    3300
#define	WSTR_DOT_340DPS	    3400
#define	WSTR_DOT_350DPS	    3500
#define	WSTR_DOT_360DPS	    3600
#define	WSTR_DOT_370DPS	    3700
#define	WSTR_DOT_380DPS	    3800
#define	WSTR_DOT_390DPS	    3900
#define	WSTR_DOT_400DPS	    4000
#define	WSTR_DOT_410DPS	    4100
#define	WSTR_DOT_420DPS	    4200
#define	WSTR_DOT_430DPS	    4300
#define	WSTR_DOT_440DPS	    4400
#define	WSTR_DOT_450DPS	    4500
#define	WSTR_DOT_460DPS	    4600
#define	WSTR_DOT_470DPS	    4700
#define	WSTR_DOT_480DPS	    4800
#define	WSTR_DOT_490DPS	    4900
#define	WSTR_DOT_500DPS	    5000
#define	WSTR_DOT_600DPS	    6000
#define	WSTR_DOT_700DPS	    7000
#define	WSTR_DOT_800DPS	    8000
#define	WSTR_DOT_900DPS	    9000
#define	WSTR_DOT_1000DPS	10000
#define	WSTR_DOT_1100DPS	11000
#define	WSTR_DOT_1200DPS	12000
#define	WSTR_DOT_1300DPS	13000
#define	WSTR_DOT_1400DPS	14000
#define	WSTR_DOT_1500DPS	15000
#define	WSTR_DOT_1600DPS	16000
#define	WSTR_DOT_1700DPS	17000
#define	WSTR_DOT_1800DPS	18000
#define	WSTR_DOT_1900DPS	19000
#define	WSTR_DOT_2000DPS	20000

/**********************************************************/
/* BANK ANGLE DOT  : 1->0.01deg/s                         */
/**********************************************************/

#define	BANK_ANGLE_DOT_1DPS	    100
#define	BANK_ANGLE_DOT_2DPS	    200
#define	BANK_ANGLE_DOT_3DPS	    300
#define	BANK_ANGLE_DOT_4DPS	    400
#define	BANK_ANGLE_DOT_5DPS	    500
#define	BANK_ANGLE_DOT_6DPS	    600
#define	BANK_ANGLE_DOT_7DPS	    700
#define	BANK_ANGLE_DOT_8DPS	    800
#define	BANK_ANGLE_DOT_9DPS	    900
#define	BANK_ANGLE_DOT_10DPS	1000
#define	BANK_ANGLE_DOT_11DPS	1100
#define	BANK_ANGLE_DOT_12DPS	1200
#define	BANK_ANGLE_DOT_13DPS	1300
#define	BANK_ANGLE_DOT_14DPS	1400
#define	BANK_ANGLE_DOT_15DPS	1500
#define	BANK_ANGLE_DOT_16DPS	1600
#define	BANK_ANGLE_DOT_17DPS	1700
#define	BANK_ANGLE_DOT_18DPS	1800
#define	BANK_ANGLE_DOT_19DPS	1900
#define	BANK_ANGLE_DOT_20DPS	2000
#define	BANK_ANGLE_DOT_21DPS	2100
#define	BANK_ANGLE_DOT_22DPS	2200
#define	BANK_ANGLE_DOT_23DPS	2300
#define	BANK_ANGLE_DOT_24DPS	2400
#define	BANK_ANGLE_DOT_25DPS	2500
#define	BANK_ANGLE_DOT_26DPS	2600
#define	BANK_ANGLE_DOT_27DPS	2700
#define	BANK_ANGLE_DOT_28DPS	2800
#define	BANK_ANGLE_DOT_29DPS	2900
#define	BANK_ANGLE_DOT_30DPS	3000
#define	BANK_ANGLE_DOT_40DPS	4000
#define	BANK_ANGLE_DOT_50DPS	5000


/**********************************************************/
/* BANK ANGLE     : 1->0.01deg                            */
/**********************************************************/

#define	BANK_ANGLE_1DEG	    100
#define	BANK_ANGLE_2DEG	    200
#define	BANK_ANGLE_3DEG	    300
#define	BANK_ANGLE_4DEG	    400
#define	BANK_ANGLE_5DEG	    500
#define	BANK_ANGLE_6DEG	    600
#define	BANK_ANGLE_7DEG	    700
#define	BANK_ANGLE_8DEG	    800
#define	BANK_ANGLE_9DEG	    900
#define	BANK_ANGLE_10DEG	1000
#define	BANK_ANGLE_11DEG	1100
#define	BANK_ANGLE_12DEG	1200
#define	BANK_ANGLE_13DEG	1300
#define	BANK_ANGLE_14DEG	1400
#define	BANK_ANGLE_15DEG	1500
#define	BANK_ANGLE_16DEG	1600
#define	BANK_ANGLE_17DEG	1700
#define	BANK_ANGLE_18DEG	1800
#define	BANK_ANGLE_19DEG	1900
#define	BANK_ANGLE_20DEG	2000
#define	BANK_ANGLE_30DEG	3000
#define	BANK_ANGLE_40DEG	4000
#define	BANK_ANGLE_50DEG	5000

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))

/* C709 PEDAL */
/**********************************************************/
/* PEDAL TRAVEL (RESOLUTION 0.1mm)                        */
/**********************************************************/

#define		S16_TRAVEL_0_MM                  0  
#define		S16_TRAVEL_0_1_MM                1
#define		S16_TRAVEL_0_3_MM                3
#define		S16_TRAVEL_0_5_MM                5  
#define		S16_TRAVEL_1_MM                  10  
#define		S16_TRAVEL_1_5_MM                15  
#define     S16_TRAVEL_2_MM                  20
#define     S16_TRAVEL_2_5_MM                25  
#define     S16_TRAVEL_3_MM                  30  
#define     S16_TRAVEL_4_MM                  40  
#define     S16_TRAVEL_5_MM                  50  
#define     S16_TRAVEL_6_MM                  60  
#define     S16_TRAVEL_7_MM                  70  
#define     S16_TRAVEL_8_MM                  80  
#define     S16_TRAVEL_9_MM                  90  
#define     S16_TRAVEL_10_MM                 100 
#define     S16_TRAVEL_11_MM                 110 
#define     S16_TRAVEL_12_MM                 120 
#define     S16_TRAVEL_13_MM                 130 
#define     S16_TRAVEL_14_MM                 140 
#define     S16_TRAVEL_15_MM                 150 
#define     S16_TRAVEL_16_MM                 160 
#define     S16_TRAVEL_17_MM                 170 
#define     S16_TRAVEL_18_MM                 180 
#define     S16_TRAVEL_19_MM                 190 
#define     S16_TRAVEL_20_MM                 200 
#define     S16_TRAVEL_21_MM                 210 
#define     S16_TRAVEL_22_MM                 220 
#define     S16_TRAVEL_23_MM                 230 
#define     S16_TRAVEL_24_MM                 240 
#define     S16_TRAVEL_25_MM                 250 
#define     S16_TRAVEL_26_MM                 260 
#define     S16_TRAVEL_27_MM                 270 
#define     S16_TRAVEL_28_MM                 280 
#define     S16_TRAVEL_29_MM                 290 
#define     S16_TRAVEL_30_MM                 300 
#define     S16_TRAVEL_31_MM                 310 
#define     S16_TRAVEL_32_MM                 320 
#define     S16_TRAVEL_33_MM                 330 
#define     S16_TRAVEL_34_MM                 340 
#define     S16_TRAVEL_35_MM                 350 
#define     S16_TRAVEL_36_MM                 360 
#define     S16_TRAVEL_37_MM                 370 
#define     S16_TRAVEL_38_MM                 380 
#define     S16_TRAVEL_39_MM                 390 
#define     S16_TRAVEL_40_MM                 400 
#define     S16_TRAVEL_41_MM                 410 
#define     S16_TRAVEL_42_MM                 420 
#define     S16_TRAVEL_43_MM                 430 
#define     S16_TRAVEL_44_MM                 440 
#define     S16_TRAVEL_45_MM                 450 
#define     S16_TRAVEL_46_MM                 460 
#define     S16_TRAVEL_47_MM                 470 
#define     S16_TRAVEL_48_MM                 480 
#define     S16_TRAVEL_49_MM                 490 
#define     S16_TRAVEL_50_MM                 500 
#define     S16_TRAVEL_51_MM                 510 
#define     S16_TRAVEL_52_MM                 520 
#define     S16_TRAVEL_53_MM                 530 
#define     S16_TRAVEL_54_MM                 540 
#define     S16_TRAVEL_55_MM                 550 
#define     S16_TRAVEL_56_MM                 560 
#define     S16_TRAVEL_57_MM                 570 
#define     S16_TRAVEL_58_MM                 580 
#define     S16_TRAVEL_59_MM                 590 
#define     S16_TRAVEL_60_MM                 600 
#define     S16_TRAVEL_61_MM                 610 
#define     S16_TRAVEL_62_MM                 620 
#define     S16_TRAVEL_63_MM                 630 
#define     S16_TRAVEL_64_MM                 640 
#define     S16_TRAVEL_65_MM                 650 
#define     S16_TRAVEL_66_MM                 660 
#define     S16_TRAVEL_67_MM                 670 
#define     S16_TRAVEL_68_MM                 680 
#define     S16_TRAVEL_69_MM                 690 
#define     S16_TRAVEL_70_MM                 700 
#define     S16_TRAVEL_71_MM                 710 
#define     S16_TRAVEL_72_MM                 720 
#define     S16_TRAVEL_73_MM                 730 
#define     S16_TRAVEL_74_MM                 740 
#define     S16_TRAVEL_75_MM                 750 
#define     S16_TRAVEL_76_MM                 760 
#define     S16_TRAVEL_77_MM                 770 
#define     S16_TRAVEL_78_MM                 780 
#define     S16_TRAVEL_79_MM                 790 
#define     S16_TRAVEL_80_MM                 800 
#define     S16_TRAVEL_81_MM                 810 
#define     S16_TRAVEL_82_MM                 820 
#define     S16_TRAVEL_83_MM                 830 
#define     S16_TRAVEL_84_MM                 840 
#define     S16_TRAVEL_85_MM                 850 
#define     S16_TRAVEL_86_MM                 860 
#define     S16_TRAVEL_87_MM                 870 
#define     S16_TRAVEL_88_MM                 880 
#define     S16_TRAVEL_89_MM                 890 
#define     S16_TRAVEL_90_MM                 900 
#define     S16_TRAVEL_91_MM                 910 
#define     S16_TRAVEL_92_MM                 920 
#define     S16_TRAVEL_93_MM                 930 
#define     S16_TRAVEL_94_MM                 940 
#define     S16_TRAVEL_95_MM                 950 
#define     S16_TRAVEL_96_MM                 960 
#define     S16_TRAVEL_97_MM                 970 
#define     S16_TRAVEL_98_MM                 980 
#define     S16_TRAVEL_99_MM                 990 
#define     S16_TRAVEL_100_MM                1000
#define     S16_TRAVEL_101_MM                1010
#define     S16_TRAVEL_102_MM                1020
#define     S16_TRAVEL_103_MM                1030
#define     S16_TRAVEL_104_MM                1040
#define     S16_TRAVEL_105_MM                1050
#define     S16_TRAVEL_106_MM                1060
#define     S16_TRAVEL_107_MM                1070
#define     S16_TRAVEL_108_MM                1080
#define     S16_TRAVEL_109_MM                1090
#define     S16_TRAVEL_110_MM                1100
#define     S16_TRAVEL_111_MM                1110
#define     S16_TRAVEL_112_MM                1120
#define     S16_TRAVEL_113_MM                1130
#define     S16_TRAVEL_114_MM                1140
#define     S16_TRAVEL_115_MM                1150
#define     S16_TRAVEL_116_MM                1160
#define     S16_TRAVEL_117_MM                1170
#define     S16_TRAVEL_118_MM                1180
#define     S16_TRAVEL_119_MM                1190
#define     S16_TRAVEL_120_MM                1200
#define     S16_TRAVEL_121_MM                1210
#define     S16_TRAVEL_122_MM                1220
#define     S16_TRAVEL_123_MM                1230
#define     S16_TRAVEL_124_MM                1240
#define     S16_TRAVEL_125_MM                1250
#define     S16_TRAVEL_126_MM                1260
#define     S16_TRAVEL_127_MM                1270
#define     S16_TRAVEL_128_MM                1280
#define     S16_TRAVEL_129_MM                1290
#define     S16_TRAVEL_130_MM                1300
#define     S16_TRAVEL_131_MM                1310
#define     S16_TRAVEL_132_MM                1320
#define     S16_TRAVEL_133_MM                1330
#define     S16_TRAVEL_134_MM                1340
#define     S16_TRAVEL_135_MM                1350
#define     S16_TRAVEL_136_MM                1360
#define     S16_TRAVEL_137_MM                1370
#define     S16_TRAVEL_138_MM                1380
#define     S16_TRAVEL_139_MM                1390
#define     S16_TRAVEL_140_MM                1400
#define     S16_TRAVEL_141_MM                1410
#define     S16_TRAVEL_142_MM                1420
#define     S16_TRAVEL_143_MM                1430
#define     S16_TRAVEL_144_MM                1440
#define     S16_TRAVEL_145_MM                1450
#define     S16_TRAVEL_146_MM                1460
#define     S16_TRAVEL_147_MM                1470
#define     S16_TRAVEL_148_MM                1480
#define     S16_TRAVEL_149_MM                1490
#define     S16_TRAVEL_150_MM                1500
#define     S16_TRAVEL_151_MM                1510
#define     S16_TRAVEL_152_MM                1520
#define     S16_TRAVEL_153_MM                1530
#define     S16_TRAVEL_154_MM                1540
#define     S16_TRAVEL_155_MM                1550
#endif /*__AHB_GEN3_SYSTEM == DISABLE*/


/**********************************************************/
/* WSTR BIG DIFF CHECK                                    */
/**********************************************************/

#define     WSTR_BELOW_THR_CYCLE            100
#define     WSTR_SLOP_CHECK_CYCLE           6
#define     WSTR_OVER_THR_VALUE             120
#define     WSTR_OVER_SHOOT_VALUE           65
#define     WSTR_OVER_SHOOT_CLEAR_VALUE     15

/**********************************************************/
/* PRESSURE                                               */
/**********************************************************/

#define     MPRESS_STABLE_TIME          142     // 1sec
#define     MPRESS_0BAR                 0
#define     MPRESS_0G2BAR               2
#define     MPRESS_0G5BAR               5
#define     MPRESS_0G8BAR               8
#define     MPRESS_1BAR                 10
#define     MPRESS_2BAR                 20
#define     MPRESS_3BAR                 30
#define     MPRESS_4BAR                 40
#define     MPRESS_5BAR                 50
#define     MPRESS_6BAR                 60
#define     MPRESS_7BAR                 70
#define     MPRESS_8BAR                 80
#define     MPRESS_9BAR                 90
#define     MPRESS_10BAR                100
#define     MPRESS_11BAR                110
#define     MPRESS_12BAR                120
#define     MPRESS_13BAR                130
#define     MPRESS_14BAR                140
#define     MPRESS_15BAR                150
#define     MPRESS_16BAR                160
#define     MPRESS_17BAR                170
#define     MPRESS_18BAR                180
#define     MPRESS_19BAR                190
#define     MPRESS_20BAR                200
#define     MPRESS_21BAR                210
#define     MPRESS_22BAR                220
#define     MPRESS_23BAR                230
#define     MPRESS_24BAR                240
#define     MPRESS_25BAR                250
#define     MPRESS_26BAR                260
#define     MPRESS_27BAR                270
#define     MPRESS_28BAR                280
#define     MPRESS_29BAR                290
#define     MPRESS_30BAR                300
#define     MPRESS_31BAR                310
#define     MPRESS_32BAR                320
#define     MPRESS_33BAR                330
#define     MPRESS_34BAR                340
#define     MPRESS_35BAR                350
#define     MPRESS_36BAR                360
#define     MPRESS_37BAR                370
#define     MPRESS_38BAR                380
#define     MPRESS_39BAR                390
#define     MPRESS_40BAR                400
#define     MPRESS_45BAR                450
#define     MPRESS_48BAR                480
#define     MPRESS_50BAR                500
#define     MPRESS_55BAR                550
#define     MPRESS_60BAR                600
#define     MPRESS_65BAR                650
#define     MPRESS_70BAR                700
#define     MPRESS_80BAR                800
#define     MPRESS_90BAR                900
#define     MPRESS_100BAR               1000
#define     MPRESS_105BAR               1050
#define     MPRESS_110BAR               1100
#define     MPRESS_120BAR               1200
#define     MPRESS_130BAR               1300
#define     MPRESS_135BAR               1350
#define     MPRESS_140BAR               1400
#define     MPRESS_150BAR               1500
#define     MPRESS_160BAR               1600
#define     MPRESS_170BAR               1700
#define     MPRESS_180BAR               1800
#define     MPRESS_190BAR               1900
#define     MPRESS_200BAR               2000
#define     MPRESS_245BAR               2450

#define     VDC_VREF_0_KPH              17      // 2.125KPH

#define     WSTR_LOW_VALUE              78
#define     WSTR_HIGH_VALUE             82

/**********************************************************/
/* PBA                                                    */
/**********************************************************/

#define     LARGE_PEDAL_SLOP            105
#define     PEDAL_NOT_ZERO              50
#define     PEDAL_SLOP_ZERO             10
#define     PRECHARGE_TIME_LIMIT        100
#define     PBA_ON_TIME_LIMIT           3000

#define     YAW_LG_COM_SENSOR           0

#define     ALAT_COM_ZERO_POINT         518
#define     ALAT_COM_RESOLUTION         37      // r : 0.0001

#define     YAW_COM_ZERO_POINT          505
#define     YAW_COM_RESOLUTION          19      // r : 0.01


//#define       S_T_S_A             150             // Slope Threshold Standard mode to Apply mode
//#define       P_T_S_A             400             // Pressure Threshold Standard mode to Apply mode
//#define       V_T_S_A             80              // Velocity Threshold Standard mode to Apply mode
#define     P_T_S_C             10              // Pressure Threshold Standard mode to Charge mode
#define     V_T_S_C             160             // Velocity Threshold Standard mode to Charge mode
#define     TP_T_S_C            6               // Throttle Position Threshold Standard mode to Charge mode

//#define       P_T_A_A             300             // Pressure Threshold Apply mode to Apply mode
#define     T_T_A_H             100             // Time Threshold Apply mode to Hold mode
//#define       P_T_A_S             400             // Pressure Threshold Apply mode to Standard mode
//#define       V_T_A_S             80              // Velocity Threshold Apply mode to Standard mode

#define     P_T_H_A             200             // Pressure Threshold Hold mode to Apply mode
//#define       P_T_H_S             400             // Pressure Threshold Hold mode to Standard mode
//#define       HP_T_H_S            800             // High Pressure Threshold Hold mode to Standard mode
//#define       V_T_H_S             80              // Velocity Threshold Hold mode to Standard mode

#define     P_T_C_S             10              // Pressure Threshold Charge mode to Standard mode
#define     TP_T_C_S            6               // Throttle Position Threshold Charge mode to Standard mode

#define     E_T_P               50              // Exit Time for Charge

#define     PWM_P_C             10              // PWM Period for Charge
#define     PWM_OFF_PW_C        6               // PWM Pulth Width for Charge

#define     PBA_STANDARD        0
#define     PBA_APPLY           1
#define     PBA_HOLD            2
#define     PBA_CHARGE          3
#define     PBA_DUMP            4   

#define     BA_STANDBY	       0
#define     BA_APPLY           1
#define     BA_HOLD            2
#define     BA_CHARGE          3
#define     BA_DUMP            4 


#if(DEL_M_IMPROVEMENT_CONCEPT)
//#define     SLIP_TARGET_THRESHOLD   -9
#else
#define     SLIP_TARGET_THRESHOLD   -9
#endif

#define     SLIP_TARGET_GAIN        10

#define     YAW_DOT_SCALE           10
#define     YAW_SCALE               100

//#define     COUNTER_CONTROL         0

// ---------------------------------------------------------------------------------------
//                               Hill Start Assist
// ---------------------------------------------------------------------------------------
/*MISRA, H22*/
/*#define     HSA_READY                   1
#define     HSA_HOLD                    2
#define     HSA_APPLY                   3
#define     HSA_RELEASE                 4
#define      NORMAL                     0*/

#if __SPECIAL_1st_PULSEDOWN
// MGH40 INITIAL PULSEDOWN
#define SPECIAL_FOR_LOW 2
#define SPECIAL_FOR_MED 1
#endif
// ---------------------------------------------------------------------------------------
//                               Slip Controller
// ---------------------------------------------------------------------------------------
#define 	WHEEL_SLIP_2			   200
#define 	WHEEL_SLIP_3			   300
#define 	WHEEL_SLIP_4			   400
#define     WHEEL_SLIP_5               500
#define 	WHEEL_SLIP_6			   600
#define     WHEEL_SLIP_7               700
#define 	WHEEL_SLIP_8			   800
#define 	WHEEL_SLIP_9			   900
#define     WHEEL_SLIP_10             1000
#define     WHEEL_SLIP_20             2000
#define     WHEEL_SLIP_30             3000
#define     WHEEL_SLIP_40             4000
#define     WHEEL_SLIP_50             5000
#define     WHEEL_SLIP_60             6000
#define     WHEEL_SLIP_70             7000
#define     WHEEL_SLIP_80             8000
#define     WHEEL_SLIP_90             9000

// ---------------------------------------------------------------------------------------
//                               TCS (Traction Control System) by KJH
// ---------------------------------------------------------------------------------------
// 기본 파라미터 중 없는 부분 추가
/*MISRA, H22*/
//#define     T_0_MS                          0
//#define     T_70_MS                        10
//#define     T_214_MS                       30
//#define     T_350_MS                       50
//#define     T_1480_MS                     210
//#define     T_1750_MS                     248
//#define     T_3000_MS                     426
//#define     T_3500_MS                     496

#define     VREF_12_5_KPH                 100
#define     VREF_18_75_KPH                150
#define     VREF_200_KPH                 1600

//  Resolution : ACCEL_X_1G0G = 200
#define     ACCEL_X_0G0G                    0
#define     ACCEL_X_0G01G                   2
#define     ACCEL_X_0G015G                  3
#define     ACCEL_X_0G02G                   4
#define     ACCEL_X_0G025G                  5
#define     ACCEL_X_0G03G                   6
#define     ACCEL_X_0G04G                   8
#define     ACCEL_X_0G05G                  10
#define     ACCEL_X_0G06G                  12
#define     ACCEL_X_0G07G                  14
#define     ACCEL_X_0G075G                 15
#define     ACCEL_X_0G1G                   20
#define     ACCEL_X_0G15G                  30
#define     ACCEL_X_0G2G                   40
#define     ACCEL_X_0G25G                  50
#define     ACCEL_X_0G3G                   60
#define     ACCEL_X_0G32G                  64
#define     ACCEL_X_0G35G                  70
#define     ACCEL_X_0G4G                   80
#define     ACCEL_X_0G45G                  90
#define     ACCEL_X_0G5G                  100
#define     ACCEL_X_0G6G                  120
#define     ACCEL_X_0G7G                  140
#define     ACCEL_X_0G75G                 150
#define     ACCEL_X_0G8G                  160
#define     ACCEL_X_0G9G                  180
#define     ACCEL_X_1G0G                  200
#define     ACCEL_X_1G25G                 250
#define     ACCEL_X_1G5G                  300
#define     ACCEL_X_1G6G                  320
#define     ACCEL_X_2G0G                  400

#define     VREF_TO_TORQ_PERCENT            1
#define     GAIN_TO_TORQ_PERCENT           10
#define     GAIN_OFFSET_TO_TOQR_PERCENT     1
#define     ACCEL_X_TO_TORQ_PERCENT         1

#define     MAKE_DRIVE_TORQ_LEVEL_10PRO    10
#define     MAKE_DRIVE_TORQ_LEVEL_17PRO     6
#define     MAKE_DRIVE_TORQ_LEVEL_50PRO     2

//  Resolution : TORQ_PERCENT_100PRO = 1000
#define     TORQ_PERCENT_0G0PRO             0
#define     TORQ_PERCENT_0G1PRO             1
#define     TORQ_PERCENT_0G2PRO             2
#define     TORQ_PERCENT_0G3PRO             3
#define     TORQ_PERCENT_0G4PRO             4
#define     TORQ_PERCENT_0G5PRO             5
#define     TORQ_PERCENT_0G6PRO             6
#define     TORQ_PERCENT_0G7PRO             7
#define     TORQ_PERCENT_0G8PRO             8
#define     TORQ_PERCENT_0G9PRO             9
#define     TORQ_PERCENT_1PRO              10
#define     TORQ_PERCENT_1G2PRO            12
#define     TORQ_PERCENT_1G5PRO            15
#define     TORQ_PERCENT_1G6PRO            16
#define     TORQ_PERCENT_2PRO              20
#define     TORQ_PERCENT_2G5PRO            25
#define     TORQ_PERCENT_3PRO              30
#define     TORQ_PERCENT_4PRO              40
#define     TORQ_PERCENT_5PRO              50
#define     TORQ_PERCENT_6PRO              60
#define     TORQ_PERCENT_7PRO              70
#define     TORQ_PERCENT_8PRO              80
#define     TORQ_PERCENT_9PRO              90
#define     TORQ_PERCENT_10PRO            100
#define     TORQ_PERCENT_12PRO            120
#define     TORQ_PERCENT_15PRO            150
#define     TORQ_PERCENT_20PRO            200
#define     TORQ_PERCENT_30PRO            300
#define     TORQ_PERCENT_40PRO            400
#define     TORQ_PERCENT_50PRO            500
#define     TORQ_PERCENT_60PRO            600
#define     TORQ_PERCENT_70PRO            700
#define     TORQ_PERCENT_80PRO            800
#define     TORQ_PERCENT_90PRO            900
#define     TORQ_PERCENT_100PRO          1000
/*MISRA, H22*/
/*#define     GEAR_POS_TM_P                   0
#define     GEAR_POS_TM_N                   0
#define     GEAR_POS_TM_1                   1
#define     GEAR_POS_TM_2                   2
#define     GEAR_POS_TM_3                   3
#define     GEAR_POS_TM_4                   4
#define     GEAR_POS_TM_5                   5
#define     GEAR_POS_TM_R                   7

// Resolution : TORQ_GAIN_PERCENT_100PRO = 20
#define     TORQ_GAIN_PERCENT_5PRO          1
#define     TORQ_GAIN_PERCENT_95PRO        19
#define     TORQ_GAIN_PERCENT_100PRO       20

// Resolution : TPS_PERCENT_10PRO = 10
#define     TPS_PERCENT_10PRO              10*/

#define STEER_1450DEG 14500
#define STEER_1440DEG 14400
#define STEER_1430DEG 14300

#define STEER_1200DEG 12000

#define STEER_1090DEG 10900
#define STEER_1080DEG 10800
#define STEER_1070DEG 10700
#define STEER_800DEG  8000
#define STEER_730DEG  7300
#define STEER_720DEG  7200
#define STEER_710DEG  7100

#define STEER_700DEG  7000
#define STEER_650DEG  6500
#define STEER_540DEG  5400
#define STEER_480DEG  4800
#define STEER_450DEG  4500
#define STEER_364DEG  3640

#define STEER_370DEG  3700
#define STEER_360DEG  3600
#define STEER_350DEG  3500

#define STEER_300DEG  3000
#define STEER_260DEG  2600
#define STEER_200DEG  2000
#define STEER_180DEG  1800
#define STEER_100DEG  1000
#define STEER_60DEG   600
#define STEER_55DEG   550
#define STEER_50DEG   500
#define STEER_40DEG   400
#define STEER_30DEG   300
#define STEER_20DEG   200
#define STEER_15DEG   150
#define STEER_10DEG   100
#define STEER_8DEG    80
#define STEER_7DEG    70
#define STEER_5DEG    50
#define STEER_3DEG    30
#define STEER_0DEG    0


       
/*-------For Half-Period Control------------*/

    #define L_SCAN(x)   (x*L_CONTROL_PERIOD)

    #define L_1ST_SCAN  L_CONTROL_PERIOD
    #define L_1SCAN     (L_CONTROL_PERIOD)
    #define L_2SCAN     (L_CONTROL_PERIOD*2)
    #define L_3SCAN     (L_CONTROL_PERIOD*3)
    #define L_4SCAN     (L_CONTROL_PERIOD*4)
    #define L_5SCAN     (L_CONTROL_PERIOD*5)
    #define L_6SCAN     (L_CONTROL_PERIOD*6)
    #define L_7SCAN     (L_CONTROL_PERIOD*7)
    #define L_8SCAN     (L_CONTROL_PERIOD*8)
    #define L_9SCAN     (L_CONTROL_PERIOD*9)
    #define L_10SCAN    (L_CONTROL_PERIOD*10)


/*----- For Half-period Dump ------*/

    #define U8_L_DUMP_CASE_11  11
    #define U8_L_DUMP_CASE_10  10
    #define U8_L_DUMP_CASE_01  01
    #define U8_L_DUMP_CASE_00  00

/*---------------------------------*/



