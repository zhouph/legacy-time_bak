/*
 * ActiveBrakeControlInOutIfTemp.h
 *
 *  Created on: 2014. 12. 9.
 *      Author: sohyun.ahn
 */

#ifndef ACTIVEBRAKECONTROLINOUTIFTEMP_H_
#define ACTIVEBRAKECONTROLINOUTIFTEMP_H_

#include "Abc_Types.h"
/*Global Type Declaration ******************************************************/



/*Global Extern Variable Declaration *******************************************/


#define fu1DelayedParkingBrakeSignal	Abc_CtrlBus.Abc_CtrlCanRxEscInfo.PbSwt
#define fu1GearR_Switch					Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ClutchSwt
#define fu1ClutchSwitch					Abc_CtrlBus.Abc_CtrlCanRxEscInfo.GearRSwt
#define wu8IgnStat				        Abc_CtrlBus.Abc_CtrlIgnOnOffSts

#define fu1WheelFLErrDet				Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssFL
#define fu1WheelFRErrDet                Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssFR
#define fu1WheelRLErrDet                Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssRL
#define fu1WheelRRErrDet                Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssRR

#define fu1WheelFLSusDet                Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssFL
#define fu1WheelFRSusDet                Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssFR
#define fu1WheelRLSusDet                Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssRL
#define fu1WheelRRSusDet                Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssRR
#define fu1SameSideWSSErrDet            Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SameSideWss
#define fu1DiagonalWSSErrDet            Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_DiagonalWss
#define fu1FrontWSSErrDet               Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_FrontWss
#define fu1RearWSSErrDet                Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_RearWss

#define lsahbu1BrakeLightSigRaw         Abc_CtrlBus.Abc_CtrlBlsRaw
#define ldahbs16RBCTPReduction          Abc_CtrlBus.Abc_CtrlBrkPRednForBaseBrkCtrlr
#define lcrbcu1ActiveFlg                Abc_CtrlBus.Abc_CtrlRgnBrkCtrlrActStFlg

#define lcu1EbdCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.EbdCtrlFail

#define lcu1AbsCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.AbsCtrlFail
#define lcu1CbcCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.CbcCtrlFail
#define lcu1EdcCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.EdcCtrlFail
#define lcu1TcsCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TcsCtrlFail
#define lcu1EscCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.EscCtrlFail
#define lcu1HbbCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.HbbCtrlFail
#define lcu1HdcCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.HdcCtrlFail
#define lcu1HsaCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.HsaCtrlFail
#define lcu1VsmCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.VsmCtrlFail
#define lcu1SccCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.SccCtrlFail
#define lcu1CdmCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.CdmCtrlFail
#define lcu1AvhCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.AvhCtrlFail
#define lcu1PbaCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.PbaCtrlFail
#define lcu1BdwCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.BdwCtrlFail
#define lcu1EssCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.EssCtrlFail
#define lcu1TvbbCtrlFail       Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TvbbCtrlFail
#define lcu1TspCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TspCtrlFail
#define lcu1RopCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.RopCtrlFail
#define lcu1EcdCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.EcdCtrlFail
#define lcu1TodCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TodCtrlFail
#define lcu1AebCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.AebCtrlFail
#define lcu1AdcCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.AdcCtrlFail
#define lcu1SlsCtrlFail        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.SlsCtrlFail

#define lcu1TcsDiscTempErrFlg  Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TcsDiscTempErrFlg
#define lcu1HdcDiscTempErrFlg  Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.HdcDiscTempErrFlg
#define lcu1SccDiscTempErrFlg  Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.SccDiscTempErrFlg
#define lcu1TvbbDiscTempErrFlg Abc_CtrlBus.Abc_CtrlActvBrkCtrlFailFlgsforNewFMInfo.TvbbDiscTempErrFlg

#define fu1MainCanLineErrDet    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_MainCanLine
#define fu1MainCanSusDet        Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine
#define fu1EMSTimeOutErrDet     Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut
#define fu1EMSTimeOutSusDet     Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut
#define fu1TCUTimeOutErrDet     Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut
#define fu1TCUTimeOutSusDet     Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut
#define ccu1IsgEmsH2Timeout		0 // No Tx check!!


#define ebd_error_flg           Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd
#define abs_error_flg           Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs
#define tcs_error_flg           Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs
#define vdc_error_flg           Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc
#define fu1ESCDisabledBySW      Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt
#define fu1ESCSwitchSignal      Abc_CtrlBus.Abc_CtrlEscSwt
#define fu1TCSDisabledBySW      Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt
#define fu1ESCSwitchFail        Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ESCSw
#define fu1ECUHwErrDet          Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ECUHw
#define fu1ESCEcuHWErrDet       Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ECUHw
#define fu1VoltageOverErrDet    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_OverVolt
#define fu1VoltageUnderErrDet   Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_UnderVolt
#define fu1VoltageLowErrDet     Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_LowVolt
#define fou1SenPwr1secOk        Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define cbit_process_step       0 // No Tx
#define fu1ESCSensorErrDet      (Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Yaw || Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ay || Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ax || Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Str)
#define fu1SenRasterSuspect     Abc_CtrlBus.Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err
#define fu1PedalErrDet          Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_PedalPDT
#define mp_hw_suspcs_flg        Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP1
#define fu1MCPErrorDet          Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_CirP1
#define fu1MCPSusDet            Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP1
#define fu1McpSenPwr1sOk        Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define fu1YawSenPwr1sOk        Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define fu1YawErrorDet          Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Yaw
#define fu1YawSusDet            Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Yaw
#define fu1SteerSenPwr1sOk      Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define fu1StrErrorDet          Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Str
#define fu1StrSusDet            Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Str
#define fu1AySusDet             Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Ay
#define fu1AySenPwr1sOk         Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define fu1AyErrorDet           Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ay
#define fu1AxSenPwr1sOk         Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable
#define fu1AxErrorDet           Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ax
#define fu1AxSusDet             Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Ax
#define fu1ParkingBrakeSusDet           Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake
#define ccu1GearRSwitchInv              Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_GearR
#define fu1OnDiag                       Abc_CtrlBus.Abc_CtrlDiagSci 
#define init_end_flg                    (Abc_CtrlBus.Abc_CtrlFSEscVlvInitEndFlg || Abc_CtrlBus.Abc_CtrlFSBbsVlvInitEndFlg)
#define fu16CalVoltIntMOTOR             0  // 미사용
#define fu16CalVoltVDD                  Abc_CtrlBus.Abc_CtrlVBatt1Mon
#define fu1MotErrFlg                    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Motor
#define motor_init_on_flg               Abc_CtrlBus.Abc_CtrlMTRInitEndFlg
#define wmu8DrvModeToLogic              0 // 미구현
#define fu8EngineModeStep               Abc_CtrlBus.Abc_CtrlEcuModeSts
#define fu16CalVoltMOTOR                0 // 미사용
#define standstill_flg                  0 // 미사용
#define mot_mon_ad                      0 // 미사용
#define cdu1DiagSasCaltoCL              Abc_CtrlBus.Abc_CtrlSasCalInfo.DiagSasCaltoAppl
#define d_alat_1_100g_s                 0 // 미사용
#define turn_table_counter              0 // 미사용
#define wbu1YawSerialNOKFlg             Abc_CtrlBus.Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg
#define wbu1YawSNUnmatchFlg             Abc_CtrlBus.Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg
#define yaw_unstable_flg                0 // 미사용
#define yaw_correction                  0 // 미사용
//#define read_yaw_eeprom_offset          Abc_CtrlBus.Abc_CtrlActvBrkCtrlFSIfInfo.read_yaw_eeprom_offset
//#define yaw_eeprom_offset_write         Abc_CtrlBus.Abc_CtrlActvBrkCtrlFSIfInfo.yaw_eeprom_offset_write
//#define fs_yaw_offset_eeprom_ok_flg     Abc_CtrlBus.Abc_CtrlActvBrkCtrlFSIfInfo.fs_yaw_offset_eeprom_ok_flg
//#define req_yaw_eeprom_write_flg        Abc_CtrlBus.Abc_CtrlActvBrkCtrlFSIfInfo.req_yaw_eeprom_write_flg
//#define flu1BrakeLampErrDet				Abc_CtrlBus.Abc_CtrlActvBrkCtrlFSIfInfo.flu1BrakeLampErrDet //check!!

#define fu1RearSolErrDet				Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_RearSol
#define fu1FrontSolErrDet				Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_FrontSol
#define fu1ESCSolErrDet					Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ESCSol
#define fu1BLSErrDet					Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BLS
#define fu1BlsSusDet					Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BLS
#define fu1BLSSignal					Abc_CtrlBus.Abc_CtrlBlsSwt
#define fu1DelayedBLS					Abc_CtrlBus.Abc_CtrlBlsSwt //check 50ms delay signal


#define lcans16AhbMcTargetPress     	lis16DriverIntendedTP  /*Master Cylinder Target Pressure : 0.1 bar Resolution */
//#define lcans16AhbCircuitTargetPressP	lis16PriTargetPress   			 /*Primary Circuit Target Pressure : 0.1 bar Resolution */
//#define lcans16AhbCircuitTargetPressS	lis16SecTargetPress  			/*Secondary Circuit Target Pressure : 0.1 bar Resolution */
#define lcans16AhbCircuitBoostPressP	lss16BCpresFiltP     /*Primary Circuit Boost Pressure : 0.1 bar Resolution */
#define lcans16AhbCircuitBoostPressS	lss16BCpresFiltP   /*Secondary Circuit Boost Pressure : 0.1 bar Resolution */

#define lcans16AhbPedalTravel       	lis16PedalTravelFilt  /*Pedal Travel : 0.1 mm Resolution */
#define	lcahbu1AHBOn					liu1AHBon

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
#define	fu1BcpPriSusDet		 Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP1
#else
#define fu1BcpPriSusDet			Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP1
#endif
#define fu1ABSErrorDet			abs_error_flg
#define fu1EBDErrorDet			ebd_error_flg


extern U32_BIT_STRUCT_t ActvBrkCrtlIfFlgs_Rx;
extern U32_BIT_STRUCT_t ActvBrkCrtlIfFlgs_Tx;

#define liu1PdtSenFaultDet		ActvBrkCrtlIfFlgs_Rx.bit0
#define liu1PdtSigSusSet		ActvBrkCrtlIfFlgs_Rx.bit1
#define liu1PdfSenFaultDet		ActvBrkCrtlIfFlgs_Rx.bit2
#define liu1PdfSigSusSet		ActvBrkCrtlIfFlgs_Rx.bit3
#define liu1BCP1SenFaultDet		ActvBrkCrtlIfFlgs_Rx.bit4
#define liu1AHBon				ActvBrkCrtlIfFlgs_Rx.bit5
#define liu1SimVVActFlg			ActvBrkCrtlIfFlgs_Rx.bit6
#define liu1CutVV1ActFlg		ActvBrkCrtlIfFlgs_Rx.bit7
#define liu1CutVV2ActFlg		ActvBrkCrtlIfFlgs_Rx.bit8
#define liu1RelVV1ActFlg		ActvBrkCrtlIfFlgs_Rx.bit9
#define liu1RelVV2ActFlg		ActvBrkCrtlIfFlgs_Rx.bit10
#define liu1ReservedActvFlags11	     ActvBrkCrtlIfFlgs_Rx.bit11

#define liu1TestModeAct_BBS		ActvBrkCrtlIfFlgs_Rx.bit12
#define liu1FlgHoldFL			ActvBrkCrtlIfFlgs_Rx.bit13
#define liu1FlgHoldFR			ActvBrkCrtlIfFlgs_Rx.bit14
#define liu1FlgHoldRL			ActvBrkCrtlIfFlgs_Rx.bit15
#define liu1FlgHoldRR			ActvBrkCrtlIfFlgs_Rx.bit16
#define liu1FlgDumpFL			ActvBrkCrtlIfFlgs_Rx.bit17
#define liu1FlgDumpFR			ActvBrkCrtlIfFlgs_Rx.bit18
#define liu1FlgDumpRL			ActvBrkCrtlIfFlgs_Rx.bit19
#define	liu1FlgDumpRR 			ActvBrkCrtlIfFlgs_Rx.bit20
#define liu1FlgCircFL			ActvBrkCrtlIfFlgs_Rx.bit21
#define liu1FlgCircFR			ActvBrkCrtlIfFlgs_Rx.bit22
#define liu1FlgCircRL			ActvBrkCrtlIfFlgs_Rx.bit23
#define	liu1FlgCircRR 			ActvBrkCrtlIfFlgs_Rx.bit24
#define lcanu1AhbRbcAct         ActvBrkCrtlIfFlgs_Rx.bit25//    	liu1RBCTPReductionFlg
#define	liu1ChkCircPOK			ActvBrkCrtlIfFlgs_Rx.bit26
#define	liu1InhibitRecvrByBBS	ActvBrkCrtlIfFlgs_Rx.bit27
#define	liu1StrkRcvrStabnEndOk	     ActvBrkCrtlIfFlgs_Rx.bit28
#define	liu1DriverIntendToRelBrk     ActvBrkCrtlIfFlgs_Rx.bit29



extern	int16_t lis16MasterPress;
extern	int16_t lis16DriverIntendedTP;
extern	int16_t lis16PDFRaw;
extern	int16_t lis16PDFofs;
extern	int16_t lis16PDTRaw;
extern	int16_t lis16PDTofs;
extern	int16_t lis16Position;
extern	int16_t lis16PedalTravelFilt;
extern	int16_t lcans16AhbRbcPress;

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
extern	int16_t lis16MCPRawPress;
extern	int16_t lis16MCP2RawPress;
#elif __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
extern	int16_t lis16MCPRawPress;
#endif
extern  int16_t lis16PedalSimulPressRaw;

//Temporary
extern int16_t max_speed;
extern int16_t min_speed;

extern int16_t a_long_1_1000g;
extern int16_t yaw_1_100deg;
extern int16_t steer_1_10deg;
extern int16_t a_lat_1_1000g;
extern int16_t FeeDataReadInvalidFlg;

extern uint8_t CAN_LOG_DATA[80];
extern int16_t   read_yaw_eeprom_offset;
extern int16_t fs_yaw_offset_eeprom_ok_flg;
extern int16_t req_yaw_eeprom_write_flg;
//extern int16_t fs_yaw_init_rq_flg;

extern int16_t flu1BrakeLampErrDet;
extern uint16_t system_loop_counter;

extern uint8_t LOOP_TIME_10MS_TASK;
extern uint8_t LOOP_TIME_20MS_TASK_0;
extern uint8_t LOOP_TIME_20MS_TASK_1;
extern int8_t lis8FRONT_SLIP_DET_THRES_SLIP;
extern int8_t lis8FRONT_SLIP_DET_THRES_VDIFF;
extern int16_t lis16EstWheelPressFL;
extern int16_t lis16EstWheelPressFR;
extern int16_t lis16EstWheelPressRL;
extern int16_t lis16EstWheelPressRR;
extern int8_t lis8StrkRcvrCtrlState_Rx;
extern int8_t lis8RecommendStkRcvrLvl_Rx;
extern int16_t lis16StrkRcvrAbsAllowedTime_Tx;

/* SAL Scaling 누락 임시조치 KYB */
extern signed int Proxy_fys16EstimatedYaw;
extern signed int Proxy_fys16EstimatedAY;
extern signed int Proxy_a_long_1_1000g;

extern void ActvBrkCtrlInpIf(void);
extern void ActvBrkCtrlOutpIf(void);

#endif /* ACTIVEBRAKECONTROLINOUTIFTEMP_H_ */
