/*============================================================================
* Function Activation Setting
*============================================================================*/

#if	__IDB_SYSTEM==ENABLE
	#define	__IDB_LOGIC		ENABLE
#endif


/* ESP Control Disable for Specific Vehicle
* Modeling Test 미실시된 차종의 ESP 작동금지한 sample 제출시만
* 대상 차종에 대하여 setting 할 것 */ 

#define __IDB		1
#define	__IDB_RIG_TEST	DISABLE//DISABLE//ENABLE

//#if (__IDB_PRESSURE_TYPE == HMC_PRE_DEMO || __IDB_PRESSURE_TYPE == HMC_DEMO)
#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
#define __IDB_2_CIRCUIT_P_SENSOR
#endif

#define	__ESC_TCMF_CONTROL                 	1
#define __OS2WHEEL_IN_COMBINATION           0
#define __ESC_CYCLE_DETECTION              	1

	/* Improve Bank Detection */
	#if	 (__VDC	==ENABLE)
		#define __BANK_YCW					1 /* Bank Detection while control */
		#define __BANK_DFC_CHK				1 /* Improve Bank Angle calculation */
		#if (__BANK_DFC_CHK == 1)
			#define __BANK_ROLL_COMPENSATION	1 /* Roll compensation for Bank Angle calculation*/
		#endif
	#endif
	/* End of Improve Bank Detection */

		#define     __Ext_Under_Ctrl						   			 0     /*  Extreme Understeer Control logic */ 
		#define     __MGH60_ESC_IMPROVE_CONCEPT 			   1     /*  Adjust GSUV Control logic */
		#define     __YAW_M_OVERSHOOT_DETECTION          1
 
#if __ROP
    #define     __ROP_Pre_Front_W_CRTL               1     /* 1 : ROP : Pre_Front_Control Enabled */
    #define     __ESC_SW_OFF_ROP_ACT_ENABLE          1     /*  ROP in ESC_Switch Off-mode  */  		
    #define     _ESC_OFF_ROP_FUNCITON_LAMP_OFF       1     /*  ROP in ESC_Switch Off-mode  */    
		#define     __Pro_Active_Roll_Ctrl						   1     /*  Pro-Active Roll Control logic */    
		#define     __Pro_Active_ROP_Decel						   1		
    
#else
    #define     __ROP_Pre_Front_W_CRTL               0     /* 1 : ROP : Pre_Front_Control Enabled */
    #define     __ESC_SW_OFF_ROP_ACT_ENABLE          0     /*  ROP in ESC_Switch Off-mode  */  		
    #define     _ESC_OFF_ROP_FUNCITON_LAMP_OFF 		 0     /*  ROP in ESC_Switch Off-mode  */    
		#define     __Pro_Active_Roll_Ctrl						   0     /*  Pro-Active Roll Control logic */   	
		#define     __Pro_Active_ROP_Decel						   0			
    
#endif

#if __VDC==ENABLE
	#define ESC_WHEEL_STRUCTURE_ENABLE         1 /* 1: ESC WHEEL STRUCTURE ENABLED 2: PREVIOUS STRUCTURE*/
#else
	#define ESC_WHEEL_STRUCTURE_ENABLE         0 /* 1: ESC WHEEL STRUCTURE ENABLED 2: PREVIOUS STRUCTURE*/
#endif

#define DEL_M_IMPROVEMENT_CONCEPT          1 

#if __Ext_Under_Ctrl == 1
#define     __Decel_ESC				0 /*do not change*/         
#else
#define     __Decel_ESC				0 /*select*/        
#endif
#if (__CAR_MAKER==GM_KOREA)
	#define T_300_CPG_BANK_SUSPECT_SET  1
#else
	#define T_300_CPG_BANK_SUSPECT_SET  0	
#endif

/*  TSP   */
#if (__TSP == ENABLE)
    #define __TSP_PBA_COMBINATION		    1   
#else
    #define __TSP_PBA_COMBINATION		    0    
#endif

/*----------------------- CPG Bank --------------------------*/
#if (__CAR == GM_M300) || (__CAR==GM_T300) || (__CAR==GM_S4500) || (__CAR==GM_GSUV) || (__CAR==GM_M350)
	#define __GM_CPG_BANK 							1
#else
	#define __GM_CPG_BANK 							0
#endif 

/* ----- IMPROVEMENT OF DISK TEMPERATUE ESTIMATION : ENGINE OFF TIME APPLICATION -----*/
#if(GMLAN_ENABLE==ENABLE)
		#define __GM_ENGINE_OFF_TIME_APPLICATION	1
#endif

/*============================================================================
*  FMVSS_126 : BANK_ROAD ESC Control Activation
*============================================================================*/
    #define __BANK_DETECT_ESC_CTRL_ENABLE  1   /* Enable ESC_Ctrl at Bank-detection */
    #define __BANK_ESC_IMPROVEMENT_CONCEPT 1   /* Enable Bank ESC Improvement Concept */
/*    
#if __BANK_DETECT_ESC_CTRL_ENABLE     
    #define     S16_BANK_CTRL_INS_D_WEG     20   
#endif 
*/
/*============================================================================
* Value Added Functions Setting
*============================================================================*/
    #define    __ETC                    0   /* ETC only, ADDED BY HJH */
                                            /* __HDC, __HSA, __ROP, __CBC, __EDC 는 해당 차량.par로 이동했음 */
/*============================================================================
* Logic Setting
*============================================================================*/
#define     __MY_08             1 /*    08 evol. Logc */

#if __MY_08
	/* TCS MGH60 Development */
    #define __MY08_TCS_DCT_L2H                      0       /* Low-to-High 탐색 입력 concept */
    #define __MY08_TCS_DCT_L2SPLIT                  1       /* Low-to-Split 탐색 입력 concept */
    #define __MY08_TCS_PREVENT_SPIN_OVERSHOOT       0       /* Spin Overshoot 방지 concept */
    #define __MY08_TCS_VREF_CORRECTION_AID          1       /* 4WD 차량에서 Vref 보정을 돕는 Concept */
    #define __MY08_TCS_TORQ_RISE_IN_GD_TRACK        1       /* Target ΔV 잘 추종시 Torque Rise하는 Concept */
    #define __MY08_TCS_SUSPEND_PRESSURE_DUMP        1       /* Split-mu 가속 중 Dump 유보 Concept           :  coding 해야함*/
	#define __MY08_TCS_TRANSITION2STATE1			1       /* Pressure Dump후 다시 Rise시 State1으로 전환 */
	#define	__MY08_TCS_DISCRETE_ENG_CTL				0       /* Discrete Engine Control Concept */
	#define __MY08_TCS_SPLIT_LONG_DUMP              0       /* Split-mu에서 BTCS dump 길게 가져 가는 구조 */
	#define __MY08_TCS_TORQUE_LEVEL_SEARCH			1		/* 노면에 적당한 Torque Level 빠르게 찾는 Concept */	
	#define __MY08_TCS_1ST_SCAN_CTL					1		/* 직진 및 선회시 1 SCAN 제어량 설정 */		
	#define __MY08_TCS_LOW_SPIN_SNOW_CTL			1		/* Spin이 작은 경우 Snow 노면으로 간주하여 제어 */
	#define __MY08_TCS_TORQ_FAST_UP_HIGH_MU			1		/* 가속도 큰 경우 토크 빠르게 회복 */
	#define __MY08_TCS_SPLIT_INI_PRES_RISE			0		/* Split mu 감지시 초기 일정 압력 rise concept */
	#define __MY08_TCS_SPLIT_TORQ_CTL_PATTERN		1		/* Split mu 제어 Pattern */
	#define __MY08_TCS_TORQ_UP_LVL_AFTER_EEC		1		/* EEC 종료후 일정 수준까지 토크 증가 시키는 Concept */
	#define __MY08_TCS_DCT_H2L						0		/* H2L 감지 Concept */
	#define __MY08_TCS_ESV_CLOSE_DELAY				1		/* ESV Close Delay 적용 */
	#define __TCS_CODE_SIZE_REDUCTION_RESTORE		0		/* Code Size 축소 위한 작업 원상 복귀 */
	#define	__TCS_RISE_TH_ADJUST					1		/* Spin 기울기에 따른 Rise Rate 가변 */
	#define __TCS_CONTROLLER_UNIFICATION			1       /* Homo/Split/선회 제어기 일원화 */	
	#define __TCS_CTRL_AMNT_DEC_IN_GEAR_SHIFT		1       /* Gear 변속중 제어량 저감 */
	#define __TCS_DISCRETE_CTL_REFINE				1		/* Discrete Input 적용시 Spin 기울기 고려 */
	#define __TCS_INDIV_WHEEL_CTRL_IN_BTCS			1		/* BTCS 개별 휠 제어 */
	#define	__TCS_GOTO_ELIMINATION					1		/* GOTO 삭제 */
#if __ECU==ESP_ECU_1
	#define BTCS_TCMF_CONTROL  /* BTCS_TCMF_CONTROL */
#endif

#if (GMLAN_ENABLE==ENABLE)||(__CAR==GM_CTS)
#define __TCS_STUCK_ALGO_ENABLE	1
	#elif __MY_08
#define __TCS_STUCK_ALGO_ENABLE	1
	#else
#define __TCS_STUCK_ALGO_ENABLE	0
	#endif

	/* ESC MGH60 Development */
	#define     MGH60_New_Yaw_Limit         		0    /* Ay_Continuous Yaw_Limit  */
	#define     MGH60_NEW_TARGET_SLIP       		1    /* Target slip */
	#define     ESC_Del_V_Control           		0
	#define     ESC_PULSE_UP_RISE           		1    /* Pulse Up rise */
#if  (__CAR==GM_T300) || (__CAR==GM_M300) 
	#define     Drum_Brake                  		1
#else
	#define     Drum_Brake                  		0
#endif

#if ( __AHB_SYSTEM==ENABLE )&& ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    #define     __BRK_SIG_MPS_TO_PEDAL_SEN              1    /* Mpress sensor --> Pedal travel sensor */
#elif ( ( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE) )&& ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    #define     __BRK_SIG_MPS_TO_PEDAL_SEN              1    /* Mpress sensor --> Pedal travel sensor */
#else
    #define     __BRK_SIG_MPS_TO_PEDAL_SEN              0    
#endif

	#define     ENABLE_PULSE_DOWN_DUMP      		1
	
	/* ABS MGH60 Development */
	#define __MGH60_ABS_CONCEPT_RELEASED            1
	#define __MGH60_ABS_IMPROVE_CONCEPT             0
#endif

#if (__CAR==GM_M300) ||(__CAR==GM_T300)||(__CAR==GM_S4500)   /*||(__CAR==HMC_NF)*/||(__CAR==GM_C100) || (__CAR==GM_GSUV)
    #define     __GM_FailM          1 /*    GM Fail mangement 사용 여부 */
#else
    #define     __GM_FailM          0 /*    GM Fail mangement 사용 여부 */
#endif

/*--------  FMVSS126-- Absolute SAS EEP offset  ---------------*/

#if __STEER_SENSOR_TYPE==CAN_TYPE
    #define __CAN_SAS_EEP_OFFSET_APPLY  1   /* Possible Set */
#else
    #define __CAN_SAS_EEP_OFFSET_APPLY  0   /* Always Reset */
#endif        

/*---------------------------------------------------------------------------*/

/* ESPlus & HBB H/W setting -------------------------------------------------*/
#if __TCMF_ENABLE
#define   __TCMF_CONTROL        1
#define   __TCMF_CONTROL2       0
#define   __TCMF_LowVacuumBoost __HBB
#define   __Vacuum_Sen          0
#define   __Vacuum_Sen_CBox_AD  0
#define   __MFC_L9352B          1
#else
#define   __TCMF_CONTROL        0
#define   __TCMF_CONTROL2       0
#define   __TCMF_LowVacuumBoost 0
#define   __Vacuum_Sen          0
#define   __Vacuum_Sen_CBox_AD  0
#define   __MFC_L9352B          0
#endif
/*---------------------------------------------------------------------------*/

#define     __MGH40_ESC_Concepts        1   /* default : 1, for new ESC concept applied */
#define     __TIRE_MODEL                0   /* default : 0, till TBD */
#define     __P_CONTROL                 0   /* default : 1, for ESC Pressure Control applied */
#define     __Mu_Level_Adaptation       0   /* default : 0, After '07 SWD confirm, Consider whether it's applied. */
#define     __New_Mu_Level_Yaw_Limit    1   /* default : 1, After '07 SWD confirm, Consider whether it's applied. */

#if __VDC
  #if __MGH60_ABS_CONCEPT_RELEASED
#define     __STABLE_DUMPHOLD_ENABLE        1
#define     __EST_MU_FROM_SKID              1
#define     __THRDECEL_RESOLUTION_CHANGE    1
  #elif __MGH60_ABS_IMPROVE_CONCEPT
#define     __STABLE_DUMPHOLD_ENABLE        1
#define     __EST_MU_FROM_SKID              1
#define     __THRDECEL_RESOLUTION_CHANGE    1
  #else
#define     __STABLE_DUMPHOLD_ENABLE        0
#define     __EST_MU_FROM_SKID              0
#define     __THRDECEL_RESOLUTION_CHANGE    0
  #endif
#else
#define     __STABLE_DUMPHOLD_ENABLE        0
#define     __EST_MU_FROM_SKID              0
#define     __THRDECEL_RESOLUTION_CHANGE    0
#endif

  #if __MGH60_ABS_CONCEPT_RELEASED
#define     __REDUCE_1ST_UR_PRESS_DIFF      1
#define     __PRES_RISE_DEFER_FOR_UR        0
#define	    __PREV_RISE_DUMP_IMPROVE        1
#define	    __HIGH_MU_DECEL_IMPROVE         1
#define     __IMPROVE_DECEL_AT_CERTAIN_HMU  1
#define     __RISE_DELAY_FOR_PREV_CAS		1
#define     __LOWMU_REAR_1stCYCLE_RISE_DELAY 1
#define     __OUT_ABS_P_RATE_COMPENSATION    1
#define     __EBD_MI_ENABLE                 1
#define     __IMPROVE_CASCADING             1
  #elif __MGH60_ABS_IMPROVE_CONCEPT
#define     __REDUCE_1ST_UR_PRESS_DIFF      1
#define     __PRES_RISE_DEFER_FOR_UR        1
#define	    __PREV_RISE_DUMP_IMPROVE        1
#define	    __HIGH_MU_DECEL_IMPROVE         1
#define     __IMPROVE_DECEL_AT_CERTAIN_HMU  1
#define     __RISE_DELAY_FOR_PREV_CAS		1
#define     __LOWMU_REAR_1stCYCLE_RISE_DELAY 1
#define     __OUT_ABS_P_RATE_COMPENSATION    1
#define     __EBD_MI_ENABLE                 1
#define     __IMPROVE_CASCADING             1
  #else
#define     __REDUCE_1ST_UR_PRESS_DIFF      0
#define     __PRES_RISE_DEFER_FOR_UR        0
#define	    __PREV_RISE_DUMP_IMPROVE        1
#define	    __HIGH_MU_DECEL_IMPROVE         0
#define     __IMPROVE_DECEL_AT_CERTAIN_HMU  0
#define     __RISE_DELAY_FOR_PREV_CAS		0
#define     __LOWMU_REAR_1stCYCLE_RISE_DELAY 0
#define     __OUT_ABS_P_RATE_COMPENSATION    0
#define     __EBD_MI_ENABLE                 0
#define     __IMPROVE_CASCADING             0
  #endif

#define     NEW_PULSE_DOWN_DUMP         1   /* ?? default : 1 ?? */
#if (__IMPROVE_CASCADING==1)
#define     __IMP_RISEDELAY_AND_DUMP_FOR_CAS   		   1
#define     __IMP_1CYCLE_NON_DRIVEN_W_RISE_DELAY  	   1
#define     __IMP_4WD_1CYCLE_DRIVEN_1W_RISE_DELAY  	   0
#define     __IMP_N_CYCLE_RISE_DELAY                   1
  #else
#define     __IMP_RISEDELAY_AND_DUMP_FOR_CAS   		   0
#define     __IMP_1CYCLE_NON_DRIVEN_W_RISE_DELAY  	   0
#define     __IMP_4WD_1CYCLE_DRIVEN_1W_RISE_DELAY  	   0
#define     __IMP_N_CYCLE_RISE_DELAY                   0
  #endif

#define     __REDUCE_ABS_TIME_AT_BUMP       1

#if ((__CAR==GM_TAHOE) || (__CAR ==GM_SILVERADO) ||(Drum_Brake==1))
    #define     __REAR_FULL_RISE            0   /* Rear full rise 적용 여부 */
#else
    #define     __REAR_FULL_RISE            0   /* Rear full rise 적용 여부 */
#endif

#if (__CAR==GM_GSUV)&&(__HDC==ENABLE)
	#define     __HDC_ABS_IMPROVE               DISABLE
#endif

#if (__AHB_SYSTEM==ENABLE) || ( ( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE) )

    #define     __SLIGHT_BRAKING_COMPENSATION   1

#else /* (__AHB_SYSTEM==ENABLE) || (__AHB_GEN3_SYSTEM==ENABLE) */

  #if __VDC == ENABLE

    #define     __SLIGHT_BRAKING_COMPENSATION   1

  #else /*__VDC */

    #define     __SLIGHT_BRAKING_COMPENSATION   0

  #endif /*__VDC */

#endif /* (__AHB_SYSTEM==ENABLE) || (__AHB_GEN3_SYSTEM==ENABLE) */

#if (__AHB_SYSTEM==ENABLE) || (( ( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE) )&&(__RBC==ENABLE))
#define     __ABS_RBC_COOPERATION				ENABLE
#endif

#if __VDC == ENABLE
    #define     __ESP_ABS_COOPERATION_CONTROL   1   /* ?? default : 1 ?? */
    #define     __MGH40_Beta_Estimation         1  /* ?? default : 1 ??, J.K.Lee at 04.11 */
    #define     __MGH40_Bank_Estimation         1  /* ?? default : 1 ??, J.K.Lee at 06.01 */
    #define     __Beta_Control                  1  /* __MGH40_ESC_Concepts == 0 일때,Beta 제어 게인 증대 */

    #define     __YMC_MGH40                     1   /* only LDESPCalVehicleModel.c, YMC Yaw Limit Logic */
    #define     __YMC_DeltaM                    0   /* default : 0, yet...YMC Delta_M_Stability... Logic */
    #define     __ESP_DUTY_COMPEN               1   /* default : 1, Duty Compensation Logic for ESP with Pressure Estimation base */
    #define     __DET_ALAT_FLTR                 1   /* only LDESPCalVehicleModel.c, Delta_Det_Alat 계산방법(1) : Yaw_Limit Enter 조건시 */
    #define     __DELTA_DET_AY_MOD              0   /* only LDESPCalVehicleModel.c, Delta_Det_Alat 계산방법(2) : '06.04.20, cyj */

    #define     __BETA_Q_INC                    0   /* __MGH40_ESC_Concepts == 0 일때, Beta 제어량 증대 */
    #define     __ESP_ABS_SLIP_CON              0   /* only LCESPInterfaceSlipController.c, 협조 제어 */
    #define     __ESP_BTCS_CONTROL              1   /* BTCS 협조 제어 가능 */
#endif

#if __VDC == ENABLE
#define     __MGH40_PRESS               0   /* only LSESPCalSensorOffset.c */
#define     __PRESS_EST                 1   /* Mpress 값이 있는경우에만 압력 추정하는 로직(Default : 1) */
#define     __PRESS_EST_ACTIVE          1   /* Mpress + MOTOR에의한 압력 추정하는 로직(Default : 1) */
#define     __MP_COMP                   1   /* ?? default : 1 ?? */
#define     __WP_DROP_COMP              0   /* ?? default : 0 ?? */
#define     __ABS_COMB                  1   /* ?? default : 1 ?? */

  #if ( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE) /* (__CAR == KMC_PSEV) */
#define     __BUMP_SUSPECT              0	/* 2013 SWD PSEV wheel vibration */
  #else
#define     __BUMP_SUSPECT              1   /* (Default : 1) */
  #endif
#endif

#if __VDC && __MP_COMP
#define __ABS_CORNER_BRAKING_IMPROVE    ENABLE
#else
#define __ABS_CORNER_BRAKING_IMPROVE    DISABLE
#endif


#if __PRESS_EST_ACTIVE
		#define     __BTEM                ENABLE   /* __PRESS_EST_ACTIVE값이 있는 경우에만 압력기반 온도 추정하는 로직 */
#else
		#define     __BTEM                DISABLE   /* __PRESS_EST_ACTIVE값이 없는 경우에 속도기반 온도 추정하는 로직 */
#endif


#define     __PRESS_EST_ABS             1       /* ABS_ECU 적용 압력추정  (Default : 1) */

#if  (__VDC==1) && (__PRESS_EST==1)
#define     __PRESS_DECEL_VREF_APPLY                    1   /* ESC default : 1 , S.W.Kim at 07.11 */
#else
#define     __PRESS_DECEL_VREF_APPLY                    0
#endif

//#if __PRESS_EST || __PRESS_EST_ABS
//#include Press_est_parameters.par"    //압력 추정용 헤어 파일 Loading
//#endif

#define     __VRSELECT_NEW_CONCEPT       1
#define     __SPLIT_VERIFY              1   /* ?? default : 1 ?? */
//#define     __WHEEL_VEL_VERIFY          1   /* ?? default : 1 ?? moved to LSABSCallSensorSignal.h */
//#define     __4WD_BLS_G_SENSOR_lIMIT    0   /* ?? default : 0 ??  moved to LDABSCallVrefEst.h*/

#define     __TEMP_for_CAS              1   /* ?? default : 1 ?? */
#define     __TEMP_for_CAS2             1   /* ?? default : 1 ?? */

#if __CAR == HMC_GK
#define     __CASCADING_WEAKEN          1   /* Additional logic to prevent vref cascading */
#else
#define     __CASCADING_WEAKEN          0
#endif

#if __CAR == HMC_NF_MECU || __CAR == SYC_RAXTON_MECU || __CAR == ASTRA_MECU || __CAR == FORD_C214_MECU
    #define     __BEND_DETECT_using_SIDEVREF    0
#else
    #define     __BEND_DETECT_using_SIDEVREF    1   /* ?? default : 1 ?? */
#endif
#define     __MGH40_ESP_BEND_DETECT     1   /* ?? default : 1 ?? */

#define     __UNSTABLE_REAPPLY_ENABLE   1   /* ?? default : 1 ?? */

#define     __PRESS_EST_ABS             1       /* ABS_ECU 적용 압력추정  (Default : 1) */ 

    #if __UNSTABLE_REAPPLY_ENABLE
        #define __UNSTABLE_REAPPLY_ADAPTATION       1
        #define __DETECT_MU_FOR_UR                  1
      #if __PRESS_EST_ABS
        #define __DETECT_LOCK_IN_CYCLE              1
      #endif
    #else
        #define __UNSTABLE_REAPPLY_ADAPTATION       0
        #define __DETECT_MU_FOR_UR                  0
      #if __PRESS_EST_ABS
        #define __DETECT_LOCK_IN_CYCLE              0
      #endif
    #endif

#define     __ESP_SPLIT_2               0   /* ?? default : 0 ?? */

// BANKED ROAD & HIGH SPEED & 2 WHEEL CONTROL & Puse Down Dump추가
#define     ENABLE_HIGH_SPEED           1
#define     ENABLE_2WHEEL_CONTROL       1
#define     ENABLE_PULSE_DOWN_DUMP      1
#define     SAME_PHASE_FRONT_PRESSURE_HOLD_ENABLE   0   /* not used!! */
#define     ENABLE_3WHEEL_CONTROL       0

#if (__CAR==HMC_GK)||(__CAR==HMC_JM )||(__CAR==KMC_KM)
    #define     __SIDE_SLIP_ANGLE_MODULE        0   // Old_Beta 계산 : Not include
#else
    #define     __SIDE_SLIP_ANGLE_MODULE        0   // Old_Beta 계산 : Include
#endif

#if __VDC
#if !SIM_MATLAB
    #define __PARTIAL_PERFORMANCE           1
#else
    #define __PARTIAL_PERFORMANCE           0
#endif
#if !SIM_MATLAB
#define     __ENABLE_PREFILL            1   /* 항시 Enable 부가기능과 동등취급 자리 이동 필요 */
#else
#define     __ENABLE_PREFILL            0
#endif
#endif

#if defined(__UCC_1)
#define     __UCC_COOPERATION_CONTROL   1
#else
#define     __UCC_COOPERATION_CONTROL   0
#endif

 /***** Premium *********/
#define     __NC_CURRENT_CONTROL        1
#define     __WHEEL_PRESSURE_SENSOR     0
 /***** Premium End *****/

// Sangmook Added
#define IMPROVE_CASE_FOR_GEAR_D_TO_N    1		// Improve Case for Gear D to N 

#define		__TEST_MODE_FOR_VIRTUAL_REGEN_COMMAND_TORQUE        0	    // 1: Virtual Test Mode
                                                                     // Torque Intervention

#define     __VALVE_TEST_L              0   /* Noise Test시 enable */
#define     __VALVE_TEST_E              0   /* ESP VALVE 시험할때 enable */
#define     __VALVE_TEST_A              0   /* ABS VALVE 시험할때 enable */
#define     __VALVE_TEST_B              0   /* BTCS VALVE 시험할때 enable */
#define     __VALVE_TEST_F              0   /* Front 승압시험할때 enable */
#define     __EMS_TEST_T                0   /* ETCS 시험할때 enable */
#define     __BLEEDING_E                0   /* 2차회로 Bleeding시 enable */
#define     __VALVE_TEST_CPS            0 	/* CPS VALVE 시험할 때 enable */
#define     __VALVE_TEST_ESC_LFC        0 	/* CPS VALVE 시험할 때 enable */
#define     __VALVE_TEST_ESC_ABS_LFC    0 	/* CPS VALVE 시험할 때 enable */


	  
/*============================================================================
* This item will be moved to local header. Reconsideration is needed.
*============================================================================*/

/* These are only using in LCESPCallEngineControl.c-------------------------------------*/
#define     __SEVERE_CORNERG            1   /* SEVERE_CORNERG 적용시 enable */
#define     __EMS_AY_C                  1   /* EMS AY Control시 enable */
#define     __EMS_PD_C                  1   /* ??? ETO PD Control시 enable */
#define     __TORQ_LIMIT                1   /* TORQ_LIMIT 적용시 enable */
/*---------------------------------------------------------------------------*/

/*only vref setting, why is it located here?---------------------------------*/
#define __SPEED_CALC_LOGIC_IMPROVING    ENABLE
#if __SPEED_CALC_LOGIC_IMPROVING == ENABLE
    #if __CAR == HMC_NF_MECU || __CAR == SYC_RAXTON_MECU || __CAR == ASTRA_MECU || __CAR == FORD_C214_MECU
        #define     __WL_SPEED_RESOL_CHANGE         0
    #else
        #define     __WL_SPEED_RESOL_CHANGE         1
    #endif
#else
        #define     __WL_SPEED_RESOL_CHANGE         0
#endif

#if __WL_SPEED_RESOL_CHANGE
    #if __CAR == HMC_NF_MECU || __CAR == SYC_RAXTON_MECU
        #define     __SIDE_VREF         0
    #else
        #define     __SIDE_VREF         1      // Side Vref added 04.11.18
    #endif
#else
    #define     __SIDE_VREF             0
#endif

#if   __SIDE_VREF
    #define   __USE_SIDE_VREF           1
    #define   __VREF_AUX                0
#endif
//#define     __4WD_BLS_G_SENSOR_lIMIT    0   /* ?? default : 0 ?? */
/*---------------------------------------------------------------------------*/

/* __REAR_D can be changed to __DRIVE_TYPE==REAR_WHEEL_DRV-------------------*/
/* __ESP_LowSpeedChange is only using in LCESPCallControl.c------------------*/
#if __DRIVE_TYPE==REAR_WHEEL_DRV
    #define __REAR_D                    1
    #define __ESP_LowSpeedChange        1    // 제어 금지 속도 변경
#else
    #define __REAR_D                    0
    #define __ESP_LowSpeedChange        0    // 제어 금지 속도 변경
#endif
/*---------------------------------------------------------------------------*/

/* These are only using in LCESPCallControl.c------------------*/
#define YC_P_GAIN_R_OVERSTEER_OUT       0
#define YC_D_GAIN_R_OVERSTEER_OUT       0
#define ST_LIMIT_R_OVERSTEER_OUT        0

#define YC_P_GAIN_F_OVERSTEER_IN        0
#define YC_P_GAIN_R_OVERSTEER_IN        0
#define YC_D_GAIN_R_OVERSTEER_IN        0
#define ST_LIMIT_R_OVERSTEER_IN         0

#define YC_P_GAIN_F_UNDERSTEER_OUT      0
#define YC_D_GAIN_F_UNDERSTEER_OUT      0
#define ST_LIMIT_F_UNDERSTEER_OUT       0

#define YC_P_GAIN_F_UNDERSTEER_IN       0
#define YC_D_GAIN_F_UNDERSTEER_IN       0
#define YC_P_GAIN_R_UNDERSTEER_IN       0
#define ST_LIMIT_F_UNDERSTEER_IN        0

#if __CAR == HMC_JB
#define DELTA_P_GAIN_ADD_MU             -400
#else
#define DELTA_P_GAIN_ADD_MU             0
#endif

#if __CAR==HMC_GK
    #define UNDER_CONTROL_EXIT_GRAD_GAIN    30
    #define UNDER_ENTER_ADD_WSTR            2500
#else
    #define UNDER_CONTROL_EXIT_GRAD_GAIN    100
    #define UNDER_ENTER_ADD_WSTR            2500
#endif

#if  (__CAR == HMC_JM && (__SUSPENSION_TYPE == 2))||(__CAR == KMC_KM && (__SUSPENSION_TYPE == 2))
#define UNDER_CONTROL_ENTER_CONDITION   -15
#elif  (__CAR == HMC_JM && (__SUSPENSION_TYPE == 0))||(__CAR == KMC_KM && (__SUSPENSION_TYPE == 0))
#define UNDER_CONTROL_ENTER_CONDITION   -15
#else
#define UNDER_CONTROL_ENTER_CONDITION   -20
#endif

#if  (__CAR == HMC_JM && (__SUSPENSION_TYPE == 2))||(__CAR == KMC_KM && (__SUSPENSION_TYPE == 2))
#define UNDER_CONTROL_MED_SP_ADD_GAIN_WANT  1
#elif  (__CAR == HMC_JM && (__SUSPENSION_TYPE == 0))||(__CAR == KMC_KM && (__SUSPENSION_TYPE == 0))
#define UNDER_MED_SP_ADD_GAIN_WANT  1
#else
#define UNDER_MED_SP_ADD_GAIN_WANT  1
#endif

/*---------------------------------------------------------------------------*/

/* Hydraulic Circuit system will be moved to vehicle par file----------------*/
#if (__CAR==GM_TAHOE) || (__CAR == SYC_RAXTON)|| (__CAR ==GM_SILVERADO)
    #define     __SPLIT_TYPE            1   /* 1 : F/R-split */
#elif (__CAR==KMC_HM) || (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO) /* HM & TAHOE F/R Split Test for Sierra Demo */
    #define     __SPLIT_TYPE            1   /* 1 : F/R-split */
#elif ((__CAR==HMC_BH)||(__CAR==HMC_DH)/*||(__CAR==HMC_YFE)||(__CAR==KMC_PSEV)) && (( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE)*/) /* BH AHB Gen3 F/R Split Test */
    #define     __SPLIT_TYPE            1   /* 1 : F/R-split */
#else
    #define     __SPLIT_TYPE            0   /* 0 : X-split */
#endif
/*---------------------------------------------------------------------------*/

/* Model Selection-----------------------------------------------------------*/
#if __CAR==HMC_GK || __CAR==SYC_KYRON || __CAR==SYC_RAXTON /*|| __CAR==HMC_EN*/
    #define       __MODEL_CHANGE        0
#else
    #define       __MODEL_CHANGE        1
#endif
/*---------------------------------------------------------------------------*/

/* __LFC_AFTER_HOLD is only using in LCESPInterfaceSlipController.c----------*/
#define     __LFC_AFTER_HOLD            1   // 압력 HOLD 후부터 LFC Rise
/*---------------------------------------------------------------------------*/

/* __ESV can be changed to __ESV_ENABLE--------------------------------------*/
 #define    __ESV  1          /* define 0 in case of Hydraulic Shuttle Valve */
                              /* always 1 for ESC system                     */
/*---------------------------------------------------------------------------*/

/* __AX_SENSOR can be changed to __G_SENSOR_ENABLE---------------------------*/
#if ((__G_SENSOR_TYPE == CAN_TYPE)||(__G_SENSOR_TYPE == ANALOG_TYPE))
 #define __AX_SENSOR    1
#else
 #define __AX_SENSOR    0
#endif
/*---------------------------------------------------------------------------*/

/*Value Added Function or subfunction??--------------------------------------*/
#if __ACC || __HDC || __EPB_INTERFACE || __TSP || __SCC
#define  __DEC          1
#else
#define  __DEC          0
#endif
/*---------------------------------------------------------------------------*/

/* These are only using in LCABSDecideCtrlState.c----------------------------*/
#define     __SPOLD_UPDATE              1
#define     __FRONT_SL_DUMP             1
#define     __MGH40_EBD                 1
#define     __SPECIAL_1st_PULSEDOWN     1
#define     __PWM_YMR_PLUS              1
#define     __DUMP_THRES_COMP_for_Spold_update  0
#define     __UNSTABLE_REAPPLY_dV_COMP  1
#define     __SDIFF_OFFSET              1
#define     __USE_VREF_AUX              0

  #if ( __AHB_GEN3_SYSTEM==ENABLE ) || (__IDB_LOGIC == ENABLE) /*(__CAR == KMC_PSEV)*/
#define     __dV_COMP_OVERSHOOT         0	/* 2013 SWD PSEV wheel vibration */
  #else
#define     __dV_COMP_OVERSHOOT         1
  #endif

#define     __ESP_SPLIT                 1   /* ABS+ESP SPLIT 협조제어 적용시 enable */
/*---------------------------------------------------------------------------*/

/*Is it needed here?---------------------------------------------------------*/
/*#if  (__AX_SENSOR==1) && ((__HSA==ENABLE) || (__HDC== ENABLE))*/
/*
#if defined  __G_SEN_EOL_OFFS_CORR
#define     __Ax_OFFSET_MON         1    
#define     __Ax_OFFSET_APPLY       1
#define     __Ax_OFFSET_EEP         1
#else
#define     __Ax_OFFSET_MON         0    
#define     __Ax_OFFSET_APPLY       0
#define     __Ax_OFFSET_EEP         0
#endif
*/

#if   __G_SEN_EOL_OFFS_CORR
  #if   __G_SEN_DRV_OFFS_CORR
  #define     __Ax_OFFSET_MON         1    /*  G offset Monitor */
  #define     __Ax_OFFSET_MON_DRV     1
  #define     __Ax_OFFSET_APPLY       1
  #else
  #define     __Ax_OFFSET_MON         1    
  #define     __Ax_OFFSET_MON_DRV     0
  #define     __Ax_OFFSET_APPLY       1    
  #endif
    
#else
  #if   __G_SEN_DRV_OFFS_CORR
  #define     __Ax_OFFSET_MON         0    
  #define     __Ax_OFFSET_MON_DRV     1
  #define     __Ax_OFFSET_APPLY       1
  #else
  #define     __Ax_OFFSET_MON         0    
  #define     __Ax_OFFSET_MON_DRV     0
  #define     __Ax_OFFSET_APPLY       0    
  #endif
#endif


#if  __VDC==1
#define  __ARAD_RESOL_CHANGE        1
#else
#define  __ARAD_RESOL_CHANGE        0
#endif
/*---------------------------------------------------------------------------*/

#define     __PARKING_BRAKE_OPERATION   1   /* ?? Which function ? */
//#define     __RTA_ENABLE                1   /* ?? for Which ?  moved to LDRTACallDetection.h*/
#define     __MGH_40_VREF               1   /* default ?? */


/* __4WD_TRACE is only using in LCTCSCallControl.c---------------------------*/
#define     __4WD_TRACE                 0   /* 4WD Trace 로직을 사용하는 경우 set시킴 */
/*---------------------------------------------------------------------------*/

/* __CREEP_VIBRATION is only using in LDABSCallDctForVref.c------------------*/
#define     __CREEP_VIBRATION           1   /* Creep Vibration 고려... */
/*---------------------------------------------------------------------------*/

/*Is it needed here?---------------------------------------------------------*/
#define     __DUTY_CHECK                0   /* only LAABSGenerateLFCDuty.c */
#define     __ROUGH_EBD                 1   /* only LDABSDctRoadSurface.c, ROUGH_ROAD EBD 개선시 enable */
#define     __TCS_NOISE                 1   /* TCS Noise New Concept 적용시 enable */
/*---------------------------------------------------------------------------*/

/* These are only using in LSESPCalSlipRatio.c-------------------------------*/
#define ESP_SLIP_FILTER_CHANGE_SLIP     1000
#define ESP_SLIP_FILTER_GAIN    L_U8FILTER_GAIN_10MSLOOP_3_5HZ
#define ESP_SLIP_FILTER_GAIN_DEEP_SLIP  L_U8FILTER_GAIN_10MSLOOP_23HZ
/* #define ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS  100 */
#define ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS  L_U8FILTER_GAIN_10MSLOOP_23HZ
#define ESP_SLIP_FILTER_GAIN_ROUGH  L_U8FILTER_GAIN_10MSLOOP_3_5HZ
/*---------------------------------------------------------------------------*/

/* These are only using in LDESPDetectVehicleStatus.c------------------------*/
#if  (__CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE == 2))|| (__CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE == 2))
#define ON_CENTER_GAIN_MAX  10
#else
#define ON_CENTER_GAIN_MAX  10
#endif
/*---------------------------------------------------------------------------*/


/*============================================================================
* This item will be deleted
*============================================================================*/
#define     SIM_MATLAB                  0   /* Simulation in the loop with MATLAB : default 0 */
#define     __ADVANCED_MSC              1   /* default : 1, Motor 1ms 구동 - 07 Winter 이후 삭제예정*/
#define     __ADVANCE_TCMF              0   /* FVSloPsvCheck.c */
#define     __REAR_MI                   1   /* default 1, MI 적용시 enable */
#define     __HDC_MT_VAR_SPD            0   /* not used!! When HDC target speed can be changed in MT vehicle */
#define     __CHANGE_MU                 1   /* always default : 1 ??? */
#define     __PREVENT_SUTTLE_NOISE      1   /* not used!!, Suttle Valve Noise 방지... */
//#define     __NON_DRIVEN_WHL_RELIABILITY_CHECK  1   /* only LDABSCallDctForVref.C but deleted */
//#define     _VRSELECT_ADVANCE           0   /* duplicated in LDABSCallVrefMainFilterProcess.h */
//#define     __ABS_ENTRY_SPEED_REF       0   /* not used!! */
#define     __MU_INT_PULSEDOWN          1   /* not used!! */
#define     __MOMENT_VERIFICATION       0   /* not used!! */
#define     __STRUCTURE_C2              1   /* only LDABSDecideCtrlState.c but not used */
#define     __REORGANIZE_FOR_MGH60_CYCLETIM  			1
#define     __EP_MODULATOR              0   /* not used!! */
#define     __BETA_TCS                  0   /* not used!!, BETA TCS 협조제어 적용시 enable */

#if __BDW || __EBP || __ENABLE_PREFILL          // ESP ECU에서 항시 Enable - 07 Winter 이후 삭제예정
    #define __ENABLE_202_DUTY           1
#else
    #define __ENABLE_202_DUTY           0
#endif

#if __ECU==ESP_ECU_1
        #define     pwm_duty_null  0
        #define     PWM_duty_HOLD  200
        #define     PWM_duty_DUMP  201
        #define     PWM_duty_CIRC  202
#else
        #define     pwm_duty_null  0
        #define     PWM_duty_HOLD  200
        #define     PWM_duty_DUMP  201
        #define     PWM_duty_CIRC  202
#endif

#if __HSA && (__CAR == HMC_JM)
#define     __HSA_ENG_TORQUEUP          0   /* not used!! */
#else
#define     __HSA_ENG_TORQUEUP          0   /* HSA w/ENG mode : enable */
#endif

#if __ECU==ESP_ECU_1
#define     __MGH60_CYCLE_TIME_OPT_ABS      DISABLE
#else
#define     __MGH60_CYCLE_TIME_OPT_ABS      DISABLE
#endif


/* There are no such vehicles-------------------------------------------------*/
#if __CAR == HMC_A1 && __4WD            /* 03NZ winter */
    #define     __INGEAR_VIB            0
#else
    #define     __INGEAR_VIB            1
#endif

#if __CAR == HMC_A1
#define     __EBD_LFC_MODE              0
#define     __AQUA_ENABLE               0
#elif __CAR == GM_M300
#define     __EBD_LFC_MODE              1
#define     __AQUA_ENABLE               0
#else
#define     __EBD_LFC_MODE              1
#define     __AQUA_ENABLE               1
#endif

#if __CAR == GM_DAT_REZZO
    #define     __RESONANCE             1
#else
    #define     __RESONANCE             0
#endif
/*---------------------------------------------------------------------------*/

/*It will be deleted---------------------------------------------------------*/
#define     MPA_USE_SW                  0     //VDC 동작시 MPA 사용여부결정 (ON=1,OFF=0)
#define     MPA_OVER_TIME               1     //1000  //MPA ONLY 연속사용한도 (단위:cycle)
#define     MPA_INIT_PRE_TIME           100   //MPA 초기 precharge 시간 (단위:cycle)
#define     MPA_INIT_PRE_SPEED          170   //MPA 초기 precharge 시작 차속 (단위:0.125kph)

#define YAW_LIMIT_H_SMALL_ANG   2000
// '    0.1 0   High mu에서, 횡가속도 차(1)를 결정하는 조향각1 [100 ~ 400deg]
#define YAW_LIMIT_H_LARGE_ANG   3000
// '    0.1 0   High mu에서, 횡가속도 차(2)를 결정하는 조향각2 [100 ~ 400deg]
#define YAW_LIMIT_DELTA_HIGH_FAST_LARGE 300

///////////////////////////////////////////////////////////////////////////
// jgkim 20040210  Variable filter for Speed
//
// LOW_&&_MED MU FOR VARING SPEED
#define FILTER_GAIN_FOR_M_A_V1_IN_YAW_LINIT_ENTER 10    // FILTER_GAIN = 10
#define FILTER_GAIN_FOR_M_A_V2_IN_YAW_LINIT_ENTER 40    // FILTER_GAIN = 40
#define FILTER_GAIN_FOR_M_A_V3_IN_YAW_LINIT_ENTER 40    // FILTER_GAIN = 40
// HIGH MU FOR VARING SPEED
#define FILTER_GAIN_FOR_H_A_V1_IN_YAW_LINIT_ENTER 10    // FILTER_GAIN = 10
#define FILTER_GAIN_FOR_H_A_V2_IN_YAW_LINIT_ENTER 40    // FILTER_GAIN = 40
#define FILTER_GAIN_FOR_H_A_V3_IN_YAW_LINIT_ENTER 40    // FILTER_GAIN = 40
///////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------*/

#define OPTRAM_PAGED_1


