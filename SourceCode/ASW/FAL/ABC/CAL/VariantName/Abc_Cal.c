/**
 * @defgroup Abc_Cal Abc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Abc_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ABC_START_SEC_CALIB_UNSPECIFIED
#include "Abc_MemMap.h"
/* Global Calibration Section */


#define ABC_STOP_SEC_CALIB_UNSPECIFIED
#include "Abc_MemMap.h"

#define ABC_START_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ABC_STOP_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
DATA_APCALABS_t*        pDATA_apCalAbs; /* check CE*/
DATA_APCALABSVAFS_t*    pDATA_apCalAbsVafs;
DATA_APCALESP_t*       pDATA_apCalEsp;
DATA_APCALESPVAFS_t*    pDATA_apCalEspVafs;
DATA_APCALESPENG_t*     pDATA_apCalEspEng;
ESP_MODEL_t *pEspModel;


ActiveBrakeControl_CalibType ABCcalib =
{
    0,

};
#define ABC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ABC_START_SEC_CODE
#include "Abc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ABC_STOP_SEC_CODE
#include "Abc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
