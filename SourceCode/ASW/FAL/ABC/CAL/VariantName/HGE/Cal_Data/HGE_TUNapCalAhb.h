#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_29_START


const /*far*/	DATA_APCALAHB_t	apCalAhb =
{

	/* uint16_t    	apCalAhbInfo.CDID                     */		0xFFFF,
	/* uint16_t    	apCalAhbInfo.CDVer                    */		0xFFFF,
	/* uchar8_t    	apCalAhbInfo.MID                      */		0xFF,
	/* int16_t     	S16_TARGET_PRESS_SET_SPD_HIGH         */		     480,	/* Comment [ Normal Target Press Map이 적용되는 최저 속도 (default 20KPH) ] */
	                                                        		        	/* ScaleVal[     60 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TARGET_PRESS_SET_SPD_LOW          */		     120,	/* Comment [ Low Speed Target Press Map이 적용되는 최저 속도 (default 10KPH) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_01                   */		      15,	/* Comment [ Pedal Travel_01의 정의 ] */
	                                                        		        	/* ScaleVal[     1.5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_02                   */		      60,	/* Comment [ Pedal Travel_02의 정의 ] */
	                                                        		        	/* ScaleVal[       6 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_03                   */		      80,	/* Comment [ Pedal Travel_03의 정의 ] */
	                                                        		        	/* ScaleVal[       8 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_04                   */		     100,	/* Comment [ Pedal Travel_04의 정의 ] */
	                                                        		        	/* ScaleVal[      10 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_05                   */		     120,	/* Comment [ Pedal Travel_05의 정의 ] */
	                                                        		        	/* ScaleVal[      12 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_06                   */		     150,	/* Comment [ Pedal Travel_06의 정의 ] */
	                                                        		        	/* ScaleVal[      15 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_07                   */		     180,	/* Comment [ Pedal Travel_07의 정의 ] */
	                                                        		        	/* ScaleVal[      18 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_08                   */		     200,	/* Comment [ Pedal Travel_08의 정의 ] */
	                                                        		        	/* ScaleVal[      20 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_09                   */		     250,	/* Comment [ Pedal Travel_09의 정의 ] */
	                                                        		        	/* ScaleVal[      25 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_10                   */		     300,	/* Comment [ Pedal Travel_10의 정의 ] */
	                                                        		        	/* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_11                   */		     350,	/* Comment [ Pedal Travel_11의 정의 ] */
	                                                        		        	/* ScaleVal[      35 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_12                   */		     400,	/* Comment [ Pedal Travel_12의 정의 ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_13                   */		     500,	/* Comment [ Pedal Travel_13의 정의 ] */
	                                                        		        	/* ScaleVal[      50 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_14                   */		     600,	/* Comment [ Pedal Travel_14의 정의 ] */
	                                                        		        	/* ScaleVal[      60 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TRAVEL_MAX                  */		     700,	/* Comment [ Pedal Travel_MAX의 정의 ] */
	                                                        		        	/* ScaleVal[      70 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01                 */		       5,	/* Comment [ Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02                 */		      12,	/* Comment [ Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03                 */		      14,	/* Comment [ Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04                 */		      20,	/* Comment [ Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05                 */		      26,	/* Comment [ Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    2.6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06                 */		      40,	/* Comment [ Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07                 */		      58,	/* Comment [ Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    5.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08                 */		      73,	/* Comment [ Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    7.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09                 */		     123,	/* Comment [ Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   12.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10                 */		     198,	/* Comment [ Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   19.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11                 */		     295,	/* Comment [ Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   29.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12                 */		     450,	/* Comment [ Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13                 */		     730,	/* Comment [ Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     73 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14                 */		    1100,	/* Comment [ Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX                */		    1500,	/* Comment [ Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_MED_SPD         */		       5,	/* Comment [ 중속에서 Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_MED_SPD         */		      12,	/* Comment [ 중속에서 Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_MED_SPD         */		      14,	/* Comment [ 중속에서 Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_MED_SPD         */		      20,	/* Comment [ 중속에서 Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_MED_SPD         */		      26,	/* Comment [ 중속에서 Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    2.6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_MED_SPD         */		      40,	/* Comment [ 중속에서 Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_MED_SPD         */		      58,	/* Comment [ 중속에서 Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    5.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_MED_SPD         */		      73,	/* Comment [ 중속에서 Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    7.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_MED_SPD         */		     123,	/* Comment [ 중속에서 Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   12.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_MED_SPD         */		     198,	/* Comment [ 중속에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   19.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_MED_SPD         */		     295,	/* Comment [ 중속에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   29.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_MED_SPD         */		     450,	/* Comment [ 중속에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_MED_SPD         */		     730,	/* Comment [ 중속에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     73 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_MED_SPD         */		    1100,	/* Comment [ 중속에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_MED_SPD        */		    1500,	/* Comment [ 중속에서 Pedal이 Travel_Max만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_LOW_SPD         */		       5,	/* Comment [ 저속에서 Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_LOW_SPD         */		      12,	/* Comment [ 저속에서 Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_LOW_SPD         */		      14,	/* Comment [ 저속에서 Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_LOW_SPD         */		      20,	/* Comment [ 저속에서 Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_LOW_SPD         */		      26,	/* Comment [ 저속에서 Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    2.6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_LOW_SPD         */		      40,	/* Comment [ 저속에서 Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_LOW_SPD         */		      58,	/* Comment [ 저속에서 Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    5.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_LOW_SPD         */		      73,	/* Comment [ 저속에서 Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    7.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_LOW_SPD         */		     123,	/* Comment [ 저속에서 Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   12.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_LOW_SPD         */		     198,	/* Comment [ 저속에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   19.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_LOW_SPD         */		     295,	/* Comment [ 저속에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   29.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_LOW_SPD         */		     450,	/* Comment [ 저속에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_LOW_SPD         */		     730,	/* Comment [ 저속에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     73 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_LOW_SPD         */		    1100,	/* Comment [ 저속에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_LOW_SPD        */		    1500,	/* Comment [ 저속에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_EBD_FAIL        */		       5,	/* Comment [ EBD Fail 시 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_EBD_FAIL        */		      12,	/* Comment [ EBD Fail 시 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_EBD_FAIL        */		      14,	/* Comment [ EBD Fail 시 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    1.4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_EBD_FAIL        */		      20,	/* Comment [ EBD Fail 시 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_EBD_FAIL        */		      26,	/* Comment [ EBD Fail 시 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    2.6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_EBD_FAIL        */		      40,	/* Comment [ EBD Fail 시 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_EBD_FAIL        */		      58,	/* Comment [ EBD Fail 시 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    5.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_EBD_FAIL        */		      73,	/* Comment [ EBD Fail 시 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    7.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_EBD_FAIL        */		     123,	/* Comment [ EBD Fail 시 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   12.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_EBD_FAIL        */		     198,	/* Comment [ EBD Fail 시 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   19.8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_EBD_FAIL        */		     295,	/* Comment [ EBD Fail 시 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[   29.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_EBD_FAIL        */		     450,	/* Comment [ EBD Fail 시 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_EBD_FAIL        */		     730,	/* Comment [ EBD Fail 시 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     73 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_EBD_FAIL        */		    1100,	/* Comment [ EBD Fail 시 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_EBD_FAIL       */		    1500,	/* Comment [ EBD Fail 시 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_LFU             */		      10,	/* Comment [ LFU Mode 시 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_LFU             */		      30,	/* Comment [ LFU Mode 시 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_LFU             */		      50,	/* Comment [ LFU Mode 시 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_LFU             */		      60,	/* Comment [ LFU Mode 시 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_LFU             */		      70,	/* Comment [ LFU Mode 시 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_LFU             */		      90,	/* Comment [ LFU Mode 시 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      9 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_LFU             */		     110,	/* Comment [ LFU Mode 시 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     11 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_LFU             */		     130,	/* Comment [ LFU Mode 시 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     13 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_LFU             */		     180,	/* Comment [ LFU Mode 시 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     18 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_LFU             */		     250,	/* Comment [ LFU Mode 시 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_LFU             */		     340,	/* Comment [ LFU Mode 시 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     34 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_LFU             */		     440,	/* Comment [ LFU Mode 시 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     44 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_LFU             */		     650,	/* Comment [ LFU Mode 시 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     65 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_LFU             */		     950,	/* Comment [ LFU Mode 시 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     95 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_LFU            */		    1500,	/* Comment [ LFU Mode 시 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_FLEX_1          */		      10,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_FLEX_1          */		      30,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_FLEX_1          */		      50,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_FLEX_1          */		      60,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_FLEX_1          */		      70,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_FLEX_1          */		      90,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      9 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_FLEX_1          */		     110,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     11 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_FLEX_1          */		     130,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     13 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_FLEX_1          */		     180,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     18 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_FLEX_1          */		     250,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_FLEX_1          */		     340,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     34 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_FLEX_1          */		     440,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     44 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_FLEX_1          */		     650,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     65 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_FLEX_1          */		     950,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     95 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_FLEX_1         */		    1500,	/* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_01_FLEX_2          */		      10,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_02_FLEX_2          */		      30,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_03_FLEX_2          */		      50,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_04_FLEX_2          */		      60,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_05_FLEX_2          */		      70,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_06_FLEX_2          */		      90,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[      9 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_07_FLEX_2          */		     110,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     11 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_08_FLEX_2          */		     130,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     13 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_09_FLEX_2          */		     180,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     18 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_10_FLEX_2          */		     250,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_11_FLEX_2          */		     340,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     34 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_12_FLEX_2          */		     440,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     44 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_13_FLEX_2          */		     650,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     65 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_14_FLEX_2          */		     950,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[     95 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PEDAL_TARGET_P_MAX_FLEX_2         */		    1500,	/* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LIMITED_TARGET_P_AT_DELTAP2       */		     100,	/* Comment [ Delta P2구간에서의 target P 최저 limit ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LIMITED_TARGET_P_AT_DELTAP3       */		     300,	/* Comment [ Delta P3구간에서의 target P 최저 limit ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LIMITED_TARGET_P_AT_DELTAP4       */		     500,	/* Comment [ Delta P4구간에서의 target P 최저 limit ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LIMITED_TARGET_P_AT_DELTAP5       */		    1000,	/* Comment [ Delta P5구간에서의 target P 최저 limit ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MAX_LINEAR_TARGET_P_AT_0KPH       */		     100,	/* Comment [ Target Press limitation 적용 시작 press ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ONSTOP_LIMITED_TARGET_P_MAX       */		     900,	/* Comment [ 정차중 Target P 제한 최대값 설정 ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_SEAL_FRIC_COMP_RATIO_RBC_END       */		      80,	/* Comment [ RBC 제어 말기 seal friction의 최대 보상 정도 (default 80%) ] */
	/* int16_t     	S16_AHBON_PD_MIN                      */		      30,	/* Comment [ AHB 작동하는 최저 Pedal Travel 값 (Min 2mm, 3mm 이상 권장) ] */
	                                                        		        	/* ScaleVal[       3 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_HIGH              */		    1730,	/* Comment [ HPA Charge On Threshold Pressue at S16_HPA_CHARGE_SPD_HIGH ] */
	                                                        		        	/* ScaleVal[    173 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_LOW               */		    1600,	/* Comment [ HPA Charge On Threshold Pressue at S16_HPA_CHARGE_SPD_LOW ] */
	                                                        		        	/* ScaleVal[    160 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_MED               */		    1600,	/* Comment [ HPA Charge On Threshold Pressue at S16_HPA_CHARGE_SPD_MED ] */
	                                                        		        	/* ScaleVal[    160 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_MED_HIGH          */		    1600,	/* Comment [ HPA Charge On Threshold Pressue at S16_HPA_CHARGE_SPD_MED_HIGH ] */
	                                                        		        	/* ScaleVal[    160 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_SPD_INVLD         */		    1700,	/* Comment [ HPA Charge On Threshold Pressue during Speed Invalid condition ] */
	                                                        		        	/* ScaleVal[    170 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_SPD_HIGH               */		     800,	/* Comment [ HPA Charge On Threshold speed at S16_HPA_CHARGE_ON_P_HIGH ] */
	                                                        		        	/* ScaleVal[    100 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HPA_CHARGE_SPD_LOW                */		     160,	/* Comment [ HPA Charge On Threshold speed at S16_HPA_CHARGE_ON_P_LOW ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HPA_CHARGE_SPD_MED                */		     240,	/* Comment [ HPA Charge On Threshold speed at S16_HPA_CHARGE_ON_P_MED ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HPA_CHARGE_SPD_MED_HIGH           */		     480,	/* Comment [ HPA Charge On Threshold speed at S16_HPA_CHARGE_ON_P_MED_HIGH ] */
	                                                        		        	/* ScaleVal[     60 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_ADD_P              */		     200,	/* Comment [ HPA Charge Off Threshold  = HPA Charge On Threshold Pressure + Add Pressure ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_ADD_P_LOW          */		     150,	/* Comment [ HPA Charge Off Threshold  = HPA Charge On Threshold Pressure + Add Pressure (below S16_HPA_CHARGE_SPD_LOW) ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_P_MAX              */		    1830,	/* Comment [ Maximum value of HPA Charge Off Threshold Pressue ] */
	                                                        		        	/* ScaleVal[    183 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_COMP_AHB_ACT           */		     200,	/* Comment [ HPA Charge Threshold in AHB = HPA Charge Threshold Pressure - Comp Pressure ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_COMP_AHB_ACT_LOW       */		     200,	/* Comment [ HPA Charge Threshold in AHB = HPA Charge Threshold Pressure - Comp Pressure (below S16_HPA_CHARGE_SPD_LOW) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_ABS_MOTOR_ON_MIN_SPD          */		      80,	/* Comment [ HPA Charge threshold 결정 시 ABS 제어 여부를 고려하는 최저속도 (default 10KPH) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_H_P_ABS            */		    1700,	/* Comment [ ABS 작동 중 HPA Charge 종료 압력 at High Target Press ] */
	                                                        		        	/* ScaleVal[    170 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_L_P_ABS            */		    1450,	/* Comment [ ABS 작동 중 HPA Charge 종료 압력 at Low Target Press ] */
	                                                        		        	/* ScaleVal[    145 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_OFF_M_P_ABS            */		    1550,	/* Comment [ ABS 작동 중 HPA Charge 종료 압력 at Med Target Press ] */
	                                                        		        	/* ScaleVal[    155 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_H_P_ABS             */		    1650,	/* Comment [ ABS 작동 중 HPA Charge 시작 압력 at High Target Press ] */
	                                                        		        	/* ScaleVal[    165 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_L_P_ABS             */		    1400,	/* Comment [ ABS 작동 중 HPA Charge 시작 압력 at Low Target Press ] */
	                                                        		        	/* ScaleVal[    140 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_M_P_ABS             */		    1500,	/* Comment [ ABS 작동 중 HPA Charge 시작 압력 at Med Target Press ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HPA_CHARGE_ON_P_QUICK_BRK         */		    1800,	/* Comment [ ABS 작동 중 HPA Charge 시작 압력 at Quick Braking ] */
	                                                        		        	/* ScaleVal[    180 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_MTR_DRIVE_SPD_HIGH            */		     960,	/* Comment [ AHB Motor Target Speed at S16_AHB_MSC_TARGET_V_HIGH ] */
	                                                        		        	/* ScaleVal[    120 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_AHB_MTR_DRIVE_SPD_LOW             */		      80,	/* Comment [ AHB Motor Target Speed at S16_AHB_MSC_TARGET_V_LOW ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_AHB_MTR_DRIVE_SPD_MED             */		     160,	/* Comment [ AHB Motor Target Speed at S16_AHB_MSC_TARGET_V_MED ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_AHB_MTR_DRIVE_SPD_MED_HIGH        */		     240,	/* Comment [ AHB Motor Target Speed at S16_AHB_MSC_TARGET_V_MED_HIGH ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_HIGH             */		    1200,	/* Comment [ AHB Motor Target Voltage at S16_AHB_MTR_DRIVE_SPD_HIGH ] */
	                                                        		        	/* ScaleVal[       12 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_LOW              */		     700,	/* Comment [ AHB Motor Target Voltage at S16_AHB_MTR_DRIVE_SPD_LOW ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_MED              */		     800,	/* Comment [ AHB Motor Target Voltage at S16_AHB_MTR_DRIVE_SPD_MED ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_MED_HIGH         */		    1000,	/* Comment [ AHB Motor Target Voltage at S16_AHB_MTR_DRIVE_SPD_HIGH ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_SPD_INVLD        */		    1000,	/* Comment [ AHB Motor Target Voltage during Speed Invalid condition ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_STOP             */		     700,	/* Comment [ AHB Motor Target Voltage at a stop ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TARGET_V              */		    1400,	/* Comment [ AHB Motor Target Voltage during ABS control ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_QUICK_BRK        */		    1400,	/* Comment [ AHB Motor Target Voltage during Quick Braking ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_Target_Volt_DIFF_MAX              */		       2,	/* Comment [ AHB Motor Target Voltage 최대 변화량 ] */
	                                                        		        	/* ScaleVal[     0.02 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_HP_LSPD         */		    1200,	/* Comment [ AHB Motor Target Voltage during ABS control, High Pressure, Low speed ] */
	                                                        		        	/* ScaleVal[       12 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_LP_LSPD         */		    1000,	/* Comment [ AHB Motor Target Voltage during ABS control, Low Pressure, Low speed ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_MP_LSPD         */		    1200,	/* Comment [ AHB Motor Target Voltage during ABS control, Med Pressure, Low speed ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_HP_MSPD         */		    1200,	/* Comment [ AHB Motor Target Voltage during ABS control, High Pressure, Med speed ] */
	                                                        		        	/* ScaleVal[       12 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_LP_MSPD         */		    1200,	/* Comment [ AHB Motor Target Voltage during ABS control, Low Pressure, Med speed ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_ABS_MSC_TAR_V_MP_MSPD         */		    1200,	/* Comment [ AHB Motor Target Voltage during ABS control, Med Pressure, Med speed ] */
	                                                        		        	/* ScaleVal[       12 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_MAX              */		    1400,	/* Comment [ Maximum value of AHB Motor Target Voltage ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_AHB_MSC_TARGET_V_MIN              */		     500,	/* Comment [ Minimum value of AHB Motor Target Voltage ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_MAX_MOTOR_ON_TIME                  */		      30,	/* Comment [ Motor 1회 구동 허용 최대 시간 ] */
	/* uchar8_t    	U8_MIN_MOTOR_OFF_TIME                 */		       2,	/* Comment [ Motor 구동 후 재구동 허용하는 최소 휴지 시간 ] */
	/* int16_t     	S16_CV_OFF_PEDAL_TRAVEL               */		      15,	/* Comment [ Pedal Simulator의 압력을 해제하는 (Cut Valve를 Open하는) Pedal Travel 기준 값 ] */
	                                                        		        	/* ScaleVal[     1.5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CV_ON_PEDAL_TRAVEL                */		      20,	/* Comment [ Pedal Simulator에 압력을 형성하는 (Cut Valve를 Close하는) Pedal Travel 기준 값 ] */
	                                                        		        	/* ScaleVal[       2 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_CV_CURRENT_DIFF_MAX               */		     300,	/* Comment [ Maximum value of Cut Valve Current change ] */
	/* uint16_t    	U16_CV_CURRENT_MAX                    */		    1000,	/* [IDB - 1000] Ball-Screw Type :1200, Worm-Rack Type : 1000*/ 
	/* uint16_t    	U16_CV_CURRENT_MIN                    */		     500,	/* Comment [ Minimum value of Cut Valve Current (Initial value) ] */
	/* uint16_t    	U16_CV_CURRENT_OFF_DIFF_LSD           */		      10,	/* Comment [ Cut Valve 종료 시 Current 감소 변화량 at Low Speed ] */
	/* uint16_t    	U16_CV_CURRENT_OFF_DIFF_NORMAL        */		     100,	/* Comment [ Cut Valve 종료 시 Current 감소 변화량 at Normal Speed ] */
	/* uint16_t    	U16_CV_OFF_HOLD_CURRENT_DIFF          */		     100,	/* Comment [ Cut Valve 유지 시 Current Hold DIFF 값(MIN 값 대비 차이) ] */
	/* uint16_t    	U16_SIMV_CURRENT_DIFF_MAX             */		     300,	/* Comment [ Maximum value of Sim Valve Current change ] */
	/* uint16_t    	U16_SIMV_CURRENT_DIFF_MIN             */		      10,	/* Comment [ Minimum value of Sim Valve Current change ] */
	/* uint16_t    	U16_SIMV_CURRENT_MAX                  */		    2500,	/* Comment [ Maximum value of Sim Valve Current ] */
	/* uint16_t    	U16_SIMV_CURRENT_MIN                  */		     400,	/* Comment [ Minimum value of Sim Valve Current (Initial value) ] */
	/* uint16_t    	U16_SIMV_OFF_HOLD_CURRENT_DIFF        */		       0,	/* Comment [ Sim Valve 유지 시 Current Hold DIFF 값(MIN 값 대비 차이) ] */
	/* uint16_t    	U16_SIM_CURRENT_OFF_DIFF_LSD          */		      10,	/* Comment [ Sim Valve 종료 시 Current 감소 변화량 at Low Speed ] */
	/* uint16_t    	U16_SIM_CURRENT_OFF_DIFF_NORMAL       */		      10,	/* Comment [ Sim Valve 종료 시 Current 감소 변화량 at Normal Speed ] */
	/* int16_t     	S16_CV_FM_FADE_OUT_CURRENT_MAX        */		    1000,	/* Comment [ Cut Valve Current Max in Fail State ] */
	/* int16_t     	S16_CV_FM_FADE_OUT_RATE               */		      10,	/* Comment [ Cut Valve Current 변화량 in Fail State ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_1_P              */		      15,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_1 (default 15) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_2_P              */		      45,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_2 (default 45) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_3_P              */		      90,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_3 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_4_P              */		     100,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_4 (default 100) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_5_P              */		      90,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_5 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_6_P              */		      90,	/* Comment [ Primary Estimation Pressure의 NO Rise Gain at Level Press_6 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_1_P         */		       0,	/* Comment [ Primary Estimation Pressure에서의 Level Press_1 (default 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_2_P         */		      70,	/* Comment [ Primary Estimation Pressure에서의 Level Press_2 (default 7bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_3_P         */		     400,	/* Comment [ Primary Estimation Pressure에서의 Level Press_3 (default 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_4_P         */		     600,	/* Comment [ Primary Estimation Pressure에서의 Level Press_4 (default 60bar) ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_5_P         */		    1300,	/* Comment [ Primary Estimation Pressure에서의 Level Press_5 (default 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_6_P         */		    1500,	/* Comment [ Primary Estimation Pressure에서의 Level Press_6 (default 150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_1_S              */		      15,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_1 (default 15) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_2_S              */		      45,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_2 (default 45) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_3_S              */		      90,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_3 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_4_S              */		     100,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_4 (default 100) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_5_S              */		      90,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_5 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_6_S              */		      90,	/* Comment [ Secondary Estimation Pressure의 NO Rise Gain at Level Press_6 (default 90) ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_1_S         */		       0,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_1 (default 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_2_S         */		      70,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_2 (default 7bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_3_S         */		     400,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_3 (default 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_4_S         */		     600,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_4 (default 60bar) ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_5_S         */		    1300,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_5 (default 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WHL_NO_RISE_GAIN_PRES_6_S         */		    1500,	/* Comment [ Secondary Estimation Pressure에서의 Level Press_6 (default 150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ACTIVE_BRK_TP_DIFF_MAX_A          */		      30,	/* Comment [ Active Braking Apply Target Pressure Diff 량 ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ACTIVE_BRK_TP_DIFF_MAX_R          */		     500,	/* Comment [ Active Braking Release Target Pressure Diff 량 ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ACTIVE_BRK_TP_MAX_ON_STOP         */		     400,	/* Comment [ 정차 중 Active Braking Target Pressure Max 값 ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ACTV_BRK_FADEOUT_TIME_MAX         */		      80,	/* Comment [ Active Braking Fade out Max Time ] */
	                                                        		        	/* ScaleVal[     400 ms ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	S16_ABS_LIMIT_SPEED                   */		      40,	/* Comment [ ABS limit speed(default 5kph) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_VEHICLE_CREEPING_SPEED             */		      40,	/* Comment [ Vehicle creeping speed (default 5kph) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_AHBON_PD_BF_EOL_CORRECTION        */		     150,	/* Comment [ AHB 유압 제어 허용 최소 Pedal stroke before EOL offset calibration ] */
	                                                        		        	/* ScaleVal[    15 mm/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHBON_PD_OFFST_INVALID            */		     100,	/* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
	                                                        		        	/* ScaleVal[    10 mm/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHBON_PD_OFFST_INVALID_WBLS       */		      30,	/* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
	                                                        		        	/* ScaleVal[     3 mm/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_FADE_OUT_ALLOW_PRESS          */		      20,	/* Comment [ Release 종료 시 RV Fade Out을 허용하는 압력 ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_FAST_EXIT_ALLOW_PRESS         */		       1,	/* Comment [ Release 종료 시 RV Fade Out을 종료하는 압력 ] */
	                                                        		        	/* ScaleVal[    0.1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CVV_MNT_END_BCP_DIFF_REF          */		       3,	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 상승 압력 ] */
	                                                        		        	/* ScaleVal[    0.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CVV_MNT_END_BCP_REF               */		      10,	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 압력 ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_AHB_FADE_OUT_TIME_MAX             */		     400,	/* Comment [ Release 종료 시 RV Fade Out을 허용하는 최대 시간 ] */
	                                                        		        	/* ScaleVal[    2000 ms ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 327675 ] */
	/* uint16_t    	U16_CV_ON_TIME_LSD                    */		    1000,	/* Comment [ 정차 또는 저속에서 Cut Valve 유예 시간(Noise 저감) ] */
	                                                        		        	/* ScaleVal[        5 S ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_CV_ON_TIME_P_GEAR                 */		    2000,	/* idb : Org 6000*//* Comment [ P단 에서 cut valve 유예시간 ] */
	                                                        		        	/* ScaleVal[     30 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* int16_t     	S16_INIT_QUICK_BRK_OFF_THRES_H        */		     200,	/* Comment [ Quick Braking Flg Off Threshold at High Speed ] */
	/* int16_t     	S16_INIT_QUICK_BRK_OFF_THRES_L        */		     250,	/* Comment [ Quick Braking Flg Off Threshold at Low Speed ] */
	/* int16_t     	S16_INIT_QUICK_BRK_OFF_THRES_M        */		     300,	/* Comment [ Quick Braking Flg Off Threshold at Mid Speed ] */
	/* int16_t     	S16_INIT_QUICK_BRK_ON_THRES_H         */		     250,	/* Comment [ Quick Braking Detection Threshold at High Speed ] */
	/* int16_t     	S16_INIT_QUICK_BRK_ON_THRES_L         */		     350,	/* Comment [ Quick Braking Detection Threshold at Low Speed ] */
	/* int16_t     	S16_INIT_QUICK_BRK_ON_THRES_M         */		     300,	/* Comment [ Quick Braking Detection Threshold at Mid Speed ] */
	/* int16_t     	S16_QUICK_BRK_OFF_DELTAP              */		     100,	/* Comment [ Quick Braking Detection 해제 DELTAP ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_CVV_CLOSE_DELAY_PSP_THR_H         */		     400,	/* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP High Threshold ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_CVV_CLOSE_DELAY_PSP_THR_L         */		      50,	/* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP Low Threshold ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_FAST_APPLY_DET_1MS_TH1            */		      30,
	                                                        		        	/* ScaleVal[       3 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_APPLY_DET_1MS_TH1_LSPD       */		      40,
	                                                        		        	/* ScaleVal[       4 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_APPLY_DET_1MS_TH2            */		      35,
	                                                        		        	/* ScaleVal[     3.5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_APPLY_DET_1MS_TH2_LSPD       */		      45,
	                                                        		        	/* ScaleVal[     4.5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_APPLY_DET_ALLOW_PEDAL        */		     250,
	                                                        		        	/* ScaleVal[      25 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_APPLY_RESET_THRES            */		      10,
	                                                        		        	/* ScaleVal[       1 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_PEDAL_APPLY_THRES            */		     300,	/* Comment [ Fast Pedal Apply Threshold ] */
	                                                        		        	/* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FAST_PEDAL_APPLY_THRES_LSPD       */		     400,	/* Comment [ Fast Pedal Apply Threshold at Low Speed ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_INITF_OFF_DELTAP                  */		      10,	/* Comment [ Fast Pedal Apply 해제 DELTAP ] */
	/* int16_t     	S16_MAX_PAD_MU_COMP_P                 */		      40,	/* Comment [ RBC END Target Pressure Max Comp_P. For Pad Mu ] */
	/* int16_t     	S16_RBC_HYDRAULIC_MIN_PRESS           */		       3,	/* Comment [ RBC END Target Pressure Max Comp_P. For Pad Mu ] */
	                                                        		        	/* ScaleVal[    0.3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 2 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_01             */		       0,	/* Comment [ AHB Main Temp Parameter_01 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_02             */		       0,	/* Comment [ AHB Main Temp Parameter_02 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_03             */		       0,	/* Comment [ AHB Main Temp Parameter_03 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_04             */		       0,	/* Comment [ AHB Main Temp Parameter_04 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_05             */		       0,	/* Comment [ AHB Main Temp Parameter_05 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_06             */		       0,	/* Comment [ AHB Main Temp Parameter_06 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_07             */		       0,	/* Comment [ AHB Main Temp Parameter_07 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_08             */		       0,	/* Comment [ AHB Main Temp Parameter_08 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_09             */		       0,	/* Comment [ AHB Main Temp Parameter_09 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_10             */		       0,	/* Comment [ AHB Main Temp Parameter_10 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_11             */		       0,	/* Comment [ AHB Main Temp Parameter_11 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_12             */		       0,	/* Comment [ AHB Main Temp Parameter_12 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_13             */		       0,	/* Comment [ AHB Main Temp Parameter_13 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_14             */		       0,	/* Comment [ AHB Main Temp Parameter_14 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_15             */		       0,	/* Comment [ AHB Main Temp Parameter_15 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_16             */		       0,	/* Comment [ AHB Main Temp Parameter_16 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_17             */		       0,	/* Comment [ AHB Main Temp Parameter_17 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_18             */		       0,	/* Comment [ AHB Main Temp Parameter_18 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_19             */		       0,	/* Comment [ AHB Main Temp Parameter_19 ] */
	/* int16_t     	S16_AHB_LOGIC_CAL_TEMP_20             */		       0,	/* Comment [ AHB Main Temp Parameter_20 ] */
	/* int16_t     	S16_QUICK_PEDAL_APPLY_THRES           */		     250,	/* Comment [ 급제동 판단 Threshold for seal friction ] */
	/* uchar8_t    	U8_PSM_1ST_RATIO                      */		      25,	/* Comment [ Pedal Stroke by Pressure Model ( 1st Ratio ) ] */
	/* uchar8_t    	U8_PSM_2ND_RATIO                      */		       5,	/* Comment [ Pedal Stroke By Pressure Model (2nd Ratio ) ] */
	/* int16_t     	S16_PSP_PEDAL_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 1 Point ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_2P                      */		     210,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 2 Point ] */
	                                                        		        	/* ScaleVal[      21 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_3P                      */		     270,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 3 Point ] */
	                                                        		        	/* ScaleVal[      27 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 4 Point ] */
	                                                        		        	/* ScaleVal[      30 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_5P                      */		     330,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 5 Point ] */
	                                                        		        	/* ScaleVal[      33 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_6P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 6 Point ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_7P                      */		     510,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 7 Point ] */
	                                                        		        	/* ScaleVal[      51 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_8P                      */		     640,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 8 Point ] */
	                                                        		        	/* ScaleVal[      64 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PEDAL_9P                      */		     700,	/* Comment [ Pedal Simulator Pressure Model : Pedal Travel - 9 Point ] */
	                                                        		        	/* ScaleVal[      70 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PSP_PRESS_1P                      */		       0,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      1 Point ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_2P                      */		     100,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      2 Point ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_3P                      */		     200,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      3 Point ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_4P                      */		     300,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      4 Point ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_5P                      */		     400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      5 Point ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_6P                      */		     600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      6 Point ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_7P                      */		    1000,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      7 Point ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_8P                      */		    1600,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      8 Point ] */
	                                                        		        	/* ScaleVal[     16 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PSP_PRESS_9P                      */		    2400,	/* Comment [ Pedal Simulator Pressure Model : Pressure -      9 Point ] */
	                                                        		        	/* ScaleVal[     24 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SUSCNT_DOWN_CONDITION             */		     100,	/* Comment [ Suspect count can be down when pedal travel is bigger than this constant ] */
	                                                        		        	/* ScaleVal[      10 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3000 ] */
	/* int16_t     	S16_SUS_OFF_CONDITION                 */		      50,	/* Comment [ Suspect flag can be off when pedal travel smaller than this constant ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3000 ] */
	/* uint16_t    	U16_MAX_VEL_THRES_LV1                 */		      10,	/* Comment [ Pedal signal rate LV 1 threshold (for 100 ms) ] */
	                                                        		        	/* ScaleVal[ 200 mm/sec ]	Scale [ f(x) = (X + 0) * 20 ]  Range   [ 0 <= X <= 1000 ] */
	/* uint16_t    	U16_MAX_VEL_THRES_LV2                 */		      15,	/* Comment [ Pedal signal rate LV2 threshold (for 100 ms) ] */
	                                                        		        	/* ScaleVal[ 300 mm/sec ]	Scale [ f(x) = (X + 0) * 20 ]  Range   [ 0 <= X <= 1000 ] */
	/* uint16_t    	U16_MAX_VEL_THRES_LV3                 */		      30,	/* Comment [ Pedal signal rate LV3 threshold (for 100 ms) ] */
	                                                        		        	/* ScaleVal[ 600 mm/sec ]	Scale [ f(x) = (X + 0) * 20 ]  Range   [ 0 <= X <= 1000 ] */
	/* uchar8_t    	U8_NOISE_DETECT_THRES_H               */		       9,	/* Comment [ High level noise detect counter threshold for U8_PEDAL_CHECK_PERIOD ] */
	/* uchar8_t    	U8_NOISE_DETECT_THRES_L               */		       6,	/* Comment [ Low level noise detect counter threshold for U8_PEDAL_CHECK_PERIOD ] */
	/* uchar8_t    	U8_PEDAL_CHECK_PERIOD                 */		      20,	/* Comment [ Pedal Noise Check Period ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 1275 ] */
	/* uchar8_t    	U8_SUS_OFF_CNT                        */		       0,	/* Comment [ Suspect flag off count ] */
	/* uchar8_t    	U8_SUS_ON_CNT                         */		      10	/* Comment [ Suspect flag on count ] */
};

#define LOGIC_CAL_MODULE_29_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

