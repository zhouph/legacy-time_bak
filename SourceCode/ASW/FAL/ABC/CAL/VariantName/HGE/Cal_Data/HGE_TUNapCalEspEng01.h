#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_6_START


/*far*/	DATA_APCALESPENG_t	apCalEspEng01 =
{
		{
	/* uint16_t    	apCalEspEngInfo.CDID                  */		0x0000,
	/* uint16_t    	apCalEspEngInfo.CDVer                 */		0x0000,
	/* uchar8_t    	apCalEspEngInfo.MID                   */		0x00,
		},
	/* int16_t     	S16TCSCpTCcrnt_1                      */		       0,	/* Comment [ TC current at pressure point 1 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpTCcrnt_2                      */		     380,	/* Comment [ TC current at pressure point 2 ] */
	                                                        		        	/* ScaleVal[       0.38 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpTCcrnt_3                      */		     530,	/* Comment [ TC current at pressure point 3 ] */
	                                                        		        	/* ScaleVal[       0.53 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpTCcrnt_4                      */		     700,	/* Comment [ TC current at pressure point 4 ] */
	                                                        		        	/* ScaleVal[        0.7 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpTCcrnt_5                      */		    1200,	/* Comment [ TC current at pressure point 5 ] */
	                                                        		        	/* ScaleVal[        1.2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpWhlPre_1                      */		       0,	/* Comment [ Pressure point 1 of TC valve characteristic curve ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpWhlPre_2                      */		      10,	/* Comment [ Pressure point 2 of TC valve characteristic curve ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpWhlPre_3                      */		     100,	/* Comment [ Pressure point 3 of TC valve characteristic curve ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpWhlPre_4                      */		     300,	/* Comment [ Pressure point 4 of TC valve characteristic curve ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpWhlPre_5                      */		     900,	/* Comment [ Pressure point 5 of TC valve characteristic curve ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8TCSCpSclFctTrq2PresFrt              */		      26,	/* Comment [ S16TCSCpSclFctTrq2PresFrt=2×μ_(b,front)×A_front×r_(b,front)×g,  제어 Brake torque / U8TCSCpSclFctTrq2PresFrt = target pressure ] */
	/* uchar8_t    	U8TCSCpSclFctTrq2PresRer              */		      30,	/* Comment [ S16TCSCpSclFctTrq2PresRer=2×μ_(b,rear)×A_rear×r_(b,rear)×g, 제어 Brake torque / U8TCSCpSclFctTrq2PresRer = target pressure ] */
	/* int16_t     	S16TCSCpAddMtrV4SPHillHghMuStab       */		    1000,	/* Comment [ Hill 등판시 add motor voltage ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpAddMtrV4SPHillHghMuUnSt       */		    5000,	/* Comment [ hill 등판시 high side unstable상황의 add motor voltage ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpFBMtrV_ASC                    */		    2000,	/* Comment [ Asymmetric spin Control Motor Voltage at feedback mode ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpFBMtrV_Turn                   */		    3000,	/* Comment [ 선회 제어시 feedback mode motor voltage ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpFFMtrV_ASC                    */		    2000,	/* Comment [ Asymmetric spin Control Motor Voltage at feedforward mode ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpFFMtrV_Turn                   */		    2000,	/* Comment [ 선회 제어시 feedforward mode motor voltage ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpIniRMdeMtrV_ASC               */		    5000,	/* Comment [ Asymmetric spin Control Motor Voltage at Initial mode ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpIniRMdeMtrV_SC                */		    3000,	/* Comment [ Symmetric spin Control Motor Voltage at Initial mode ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpIniRMdeMtrV_Turn              */		    5000,	/* Comment [ 선회 제어시 Initial motor voltage ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpNormMtrV_SC                   */		    2000,	/* Comment [ Symmetric spin Control Motor Voltage at normal mode ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8TCSCpAsymSpnCtlFFmodeTimeUp         */		     100,	/* Comment [ feedforward mode max sustain time ] */
	/* uchar8_t    	U8TCSCpCurntIncRateInFF               */		      10,	/* Comment [ resolution 0.001  Current increase rate in feedforward mode ] */
	/* uchar8_t    	U8TCSCpIniMtrCtlModeTime              */		       2,	/* Comment [ Initial moter control time ] */
	/* uchar8_t    	U8TCSCpIniMtrCtlModeTimeInTurn        */		       2,	/* Comment [ 선회시 Initial moter control time ] */
	/* uchar8_t    	U8TCSCpIniPres                        */		      10,	/* Comment [ resolution 0.1 Expected pressure level right after initial rise mode finished ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpIniTCVCtlModeTime              */		       1,	/* Comment [ Initial TC valve control time ] */
	/* uchar8_t    	U8TCSCpIniTCVCtlModeTimeHill          */		       1,	/* Comment [ split hill제어시 Initial 제어 시간 ] */
	/* uchar8_t    	U8TCSCpIniTCVCtlModeTimeHunt          */		       0,	/* Comment [ Hunting제어시 Initial 제어 시간 ] */
	/* uchar8_t    	U8TCSCpIniTCVCtlModeTimeInTurn        */		       1,	/* Comment [ 선회시 Initial TC valve control time ] */
	/* int16_t     	S16TCSCpDctTurnIndexEnterAyTh_1       */		     800,	/* Comment [ Speed1 에서의 선회 판단 시작 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.8 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexEnterAyTh_2       */		     600,	/* Comment [ Speed2 에서의 선회 판단 시작 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexEnterAyTh_3       */		     400,	/* Comment [ Speed3 에서의 선회 판단 시작 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexEnterAyTh_4       */		     400,	/* Comment [ Speed4 에서의 선회 판단 시작 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexEnterAyTh_5       */		     350,	/* Comment [ Speed5 에서의 선회 판단 시작 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexFinalAyTh_1       */		     900,	/* Comment [ Speed1 에서의 선회 판단 확정  Threshold  A ] */
	                                                        		        	/* ScaleVal[      0.9 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexFinalAyTh_2       */		     700,	/* Comment [ Speed2 에서의 선회 판단 확정 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexFinalAyTh_3       */		     600,	/* Comment [ Speed3 에서의 선회 판단 확정 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexFinalAyTh_4       */		     500,	/* Comment [ Speed4 에서의 선회 판단 확정 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpDctTurnIndexFinalAyTh_5       */		     450,	/* Comment [ Speed5 에서의 선회 판단 확정 Threshold  Ay ] */
	                                                        		        	/* ScaleVal[     0.45 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_TCS_BRK_CLT_MAX_SPD               */		     960,	/* Comment [ 차속이 이값 이상인 경우 브레이크 제어 금지 ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffInSt_1          */		      64,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffInSt_2          */		      64,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffInSt_3          */		      64,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffInSt_4          */		      64,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffInSt_5          */		      64,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffDPInSt_1        */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffDPInSt_2        */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffDPInSt_3        */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffDPInSt_4        */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfWhlSpnDiffDPInSt_5        */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtl_1               */		     160,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtl_2               */		     160,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtl_3               */		     160,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtl_4               */		     160,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtl_5               */		     160,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtlDP_1             */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtlDP_2             */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtlDP_3             */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtlDP_4             */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfSymSpnCtlDP_5             */		     280,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfAsymSpCtlInTurn_1         */		      40,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfAsymSpCtlInTurn_2         */		      32,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfAsymSpCtlInTurn_3         */		      32,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfAsymSpCtlInTurn_4         */		      32,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpThOfAsymSpCtlInTurn_5         */		      32,	/* Comment [ Enter Threshold of wheel spin difference at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpADDASymThOfDP                 */		      40,	/* Comment [ Driveline Protection감지시 추가하는 Threshold   (BTCS감지 둔감화) ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpADDTh4ESC                     */		     160,	/* Comment [ ESC 제어중 추가하는 Threshold   (BTCS감지 둔감화) ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpADDTh4RoughRoad               */		      40,	/* Comment [ Rough Road감지시 추가하는 Threshold   (BTCS감지 둔감화) ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16TCSCpADDTh4RTA                     */		      40,	/* Comment [ RTA감지시 추가하는 Threshold   (BTCS감지 둔감화) ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8TCSCpBaseTarWhlSpinDiff_1           */		      64,	/* Comment [ Control target spin at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[      8 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpBaseTarWhlSpinDiff_2           */		      56,	/* Comment [ Control target spin at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[      7 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpBaseTarWhlSpinDiff_3           */		      48,	/* Comment [ Control target spin at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[      6 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpBaseTarWhlSpinDiff_4           */		      48,	/* Comment [ Control target spin at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[      6 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpBaseTarWhlSpinDiff_5           */		      48,	/* Comment [ Control target spin at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[      6 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffHill_1           */		      88,	/* Comment [ Control target spin at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[     11 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffHill_2           */		      88,	/* Comment [ Control target spin at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[     11 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffHill_3           */		      88,	/* Comment [ Control target spin at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[     11 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffHill_4           */		      88,	/* Comment [ Control target spin at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[     11 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffHill_5           */		      88,	/* Comment [ Control target spin at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[     11 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffTurn_1           */		      40,	/* Comment [ Control target spin at U16_TCS_SPEED1 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffTurn_2           */		      40,	/* Comment [ Control target spin at U16_TCS_SPEED2 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffTurn_3           */		      40,	/* Comment [ Control target spin at U16_TCS_SPEED3 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffTurn_4           */		      40,	/* Comment [ Control target spin at U16_TCS_SPEED4 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlSpinDiffTurn_5           */		      40,	/* Comment [ Control target spin at U16_TCS_SPEED5 ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_F1              */		      25,	/* Comment [ Front axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[     0.29 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_F2              */		      23,	/* Comment [ Front axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[     0.23 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_F3              */		      15,	/* Comment [ Front axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_F4              */		      15,	/* Comment [ Front axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_F5              */		      12,	/* Comment [ Front axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_R1              */		       0,	/* Comment [ Rear axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_R2              */		       0,	/* Comment [ Rear axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_R3              */		       0,	/* Comment [ Rear axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_R4              */		       0,	/* Comment [ Rear axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffSt_R5              */		       0,	/* Comment [ Rear axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_F1            */		      47,	/* Comment [ Front axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[     0.38 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_F2            */		      20,	/* Comment [ Front axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_F3            */		      15,	/* Comment [ Front axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_F4            */		       5,	/* Comment [ Front axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_F5            */		       5,	/* Comment [ Front axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_R1            */		      42,	/* Comment [ Rear axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[     0.42 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_R2            */		      28,	/* Comment [ Rear axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[     0.28 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_R3            */		      20,	/* Comment [ Rear axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_R4            */		      15,	/* Comment [ Rear axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffHill_R5            */		      15,	/* Comment [ Rear axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_F1            */		      20,	/* Comment [ Front axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_F2            */		      10,	/* Comment [ Front axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_F3            */		       3,	/* Comment [ Front axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_F4            */		       1,	/* Comment [ Front axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_F5            */		       1,	/* Comment [ Front axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_R1            */		      20,	/* Comment [ Rear axle Delta(difference) Mu assume at 1st gear ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_R2            */		      10,	/* Comment [ Rear axle Delta(difference) Mu assume at 2nd gear ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_R3            */		       3,	/* Comment [ Rear axle Delta(difference) Mu assume at 3rd gear ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_R4            */		       1,	/* Comment [ Rear axle Delta(difference) Mu assume at 4th gear ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsAsmdMuDiffTurn_R5            */		       1,	/* Comment [ Rear axle Delta(difference) Mu assume at 5th gear ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_F1             */		      10,	/* Comment [ Front axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_F2             */		      10,	/* Comment [ Front axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_F3             */		      10,	/* Comment [ Front axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_F4             */		      10,	/* Comment [ Front axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_F5             */		      10,	/* Comment [ Front axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_R1             */		       0,	/* Comment [ Rear axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_R2             */		       0,	/* Comment [ Rear axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_R3             */		       0,	/* Comment [ Rear axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_R4             */		       0,	/* Comment [ Rear axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlN_R5             */		       0,	/* Comment [ Rear axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_F1             */		      15,	/* Comment [ Front axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_F2             */		      12,	/* Comment [ Front axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_F3             */		      12,	/* Comment [ Front axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_F4             */		      10,	/* Comment [ Front axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_F5             */		      10,	/* Comment [ Front axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_R1             */		       0,	/* Comment [ Rear axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_R2             */		       0,	/* Comment [ Rear axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_R3             */		       0,	/* Comment [ Rear axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_R4             */		       0,	/* Comment [ Rear axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStPG4ASFBCtlP_R5             */		       0,	/* Comment [ Rear axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_F1           */		      12,	/* Comment [ Front axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_F2           */		      12,	/* Comment [ Front axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_F3           */		      12,	/* Comment [ Front axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_F4           */		      12,	/* Comment [ Front axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_F5           */		      12,	/* Comment [ Front axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_R1           */		       0,	/* Comment [ Rear axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_R2           */		       0,	/* Comment [ Rear axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_R3           */		       0,	/* Comment [ Rear axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_R4           */		       0,	/* Comment [ Rear axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlN_R5           */		       0,	/* Comment [ Rear axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_F1           */		      15,	/* Comment [ Front axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_F2           */		      12,	/* Comment [ Front axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_F3           */		      12,	/* Comment [ Front axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_F4           */		      10,	/* Comment [ Front axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_F5           */		      10,	/* Comment [ Front axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_R1           */		       0,	/* Comment [ Rear axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_R2           */		       0,	/* Comment [ Rear axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_R3           */		       0,	/* Comment [ Rear axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_R4           */		       0,	/* Comment [ Rear axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillPG4ASFBCtlP_R5           */		       0,	/* Comment [ Rear axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_F1           */		       5,	/* Comment [ Front axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_F2           */		       5,	/* Comment [ Front axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_F3           */		      50,	/* Comment [ Front axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_F4           */		      50,	/* Comment [ Front axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_F5           */		      50,	/* Comment [ Front axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_R1           */		       5,	/* Comment [ Rear axle Error Negative P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_R2           */		       5,	/* Comment [ Rear axle Error Negative P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_R3           */		       5,	/* Comment [ Rear axle Error Negative P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_R4           */		       5,	/* Comment [ Rear axle Error Negative P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlN_R5           */		       5,	/* Comment [ Rear axle Error Negative P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_F1           */		      12,	/* Comment [ Front axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_F2           */		      12,	/* Comment [ Front axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_F3           */		      30,	/* Comment [ Front axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_F4           */		      30,	/* Comment [ Front axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_F5           */		      30,	/* Comment [ Front axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_R1           */		      12,	/* Comment [ Rear axle Error Positive P gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_R2           */		      12,	/* Comment [ Rear axle Error Positive P gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_R3           */		      12,	/* Comment [ Rear axle Error Positive P gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_R4           */		      12,	/* Comment [ Rear axle Error Positive P gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnPG4ASFBCtlP_R5           */		      12,	/* Comment [ Rear axle Error Positive P gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_F1             */		     100,	/* Comment [ Front axle P gain factor % at 1000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_F2             */		     120,	/* Comment [ Front axle P gain factor % at 2000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_F3             */		     150,	/* Comment [ Front axle P gain factor % at 3000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_F4             */		     180,	/* Comment [ Front axle P gain factor % at 4000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_F5             */		     200,	/* Comment [ Front axle P gain factor % at 5000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_R1             */		     100,	/* Comment [ Rear axle P gain factor % at 1000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_R2             */		     100,	/* Comment [ Rear axle P gain factor % at 2000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_R3             */		     100,	/* Comment [ Rear axle P gain factor % at 3000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_R4             */		     180,	/* Comment [ Rear axle P gain factor % at 4000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlSt_R5             */		     220,	/* Comment [ Rear axle P gain factor % at 5000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_F1           */		     100,	/* Comment [ Front axle P gain factor % at 1000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_F2           */		     100,	/* Comment [ Front axle P gain factor % at 2000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_F3           */		     100,	/* Comment [ Front axle P gain factor % at 3000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_F4           */		     100,	/* Comment [ Front axle P gain factor % at 4000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_F5           */		     100,	/* Comment [ Front axle P gain factor % at 5000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_R1           */		     100,	/* Comment [ Rear axle P gain factor % at 1000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_R2           */		     100,	/* Comment [ Rear axle P gain factor % at 2000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_R3           */		     100,	/* Comment [ Rear axle P gain factor % at 3000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_R4           */		     100,	/* Comment [ Rear axle P gain factor % at 4000 rpm ] */
	/* uchar8_t    	U8TCSCpPgFac4ASFBCtlTurn_R5           */		     100,	/* Comment [ Rear axle P gain factor % at 5000 rpm ] */
	/* uchar8_t    	U8TCSCpICtlActCntTh                   */		      90,	/* Comment [ wheel change rate stable time for I controller active ] */
	/* uchar8_t    	U8TCSCpICtlActWhlChgRateTh            */		     140,	/* Comment [ wheel change rate threshold for I controller active ] */
	                                                        		        	/* ScaleVal[      1.4 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_F1              */		       7,	/* Comment [ Front axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_F2              */		       7,	/* Comment [ Front axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_F3              */		       7,	/* Comment [ Front axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_F4              */		       7,	/* Comment [ Front axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_F5              */		       7,	/* Comment [ Front axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_R1              */		       5,	/* Comment [ Rear axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_R2              */		       5,	/* Comment [ Rear axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_R3              */		       5,	/* Comment [ Rear axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_R4              */		       5,	/* Comment [ Rear axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsStIG4ASFBCtl_R5              */		       5,	/* Comment [ Rear axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_F1            */		      10,	/* Comment [ Front axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_F2            */		      10,	/* Comment [ Front axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_F3            */		      10,	/* Comment [ Front axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_F4            */		      10,	/* Comment [ Front axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_F5            */		      10,	/* Comment [ Front axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_R1            */		       5,	/* Comment [ Rear axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_R2            */		       5,	/* Comment [ Rear axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_R3            */		       5,	/* Comment [ Rear axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_R4            */		       5,	/* Comment [ Rear axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsHillIG4ASFBCtl_R5            */		       5,	/* Comment [ Rear axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_F1            */		       3,	/* Comment [ Front axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_F2            */		       3,	/* Comment [ Front axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_F3            */		       3,	/* Comment [ Front axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_F4            */		       3,	/* Comment [ Front axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_F5            */		       3,	/* Comment [ Front axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_R1            */		       3,	/* Comment [ Rear axle I gain at 1st gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_R2            */		       3,	/* Comment [ Rear axle I gain at 2nd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_R3            */		       3,	/* Comment [ Rear axle I gain at 3rd gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_R4            */		       3,	/* Comment [ Rear axle I gain at 4th gear ] */
	/* uchar8_t    	U8TCSCpBsTurnIG4ASFBCtl_R5            */		       3,	/* Comment [ Rear axle I gain at 5th gear ] */
	/* uchar8_t    	U8TCSCpIgainEffPctStN_F               */		     100,	/* Comment [ Front axle I gain factor % at error -5kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctStN_R               */		     100,	/* Comment [ Rear axle I gain factor % at error -5kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctStP_F               */		     100,	/* Comment [ Front axle I gain factor % at error 10kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctStP_R               */		     100,	/* Comment [ Rear axle I gain factor % at error 10kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctTurnN_F             */		     100,	/* Comment [ Front axle I gain factor % at error -5kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctTurnN_R             */		     100,	/* Comment [ Rear axle I gain factor % at error -5kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctTurnP_F             */		     100,	/* Comment [ Front axle I gain factor % at error 10kph ] */
	/* uchar8_t    	U8TCSCpIgainEffPctTurnP_R             */		     100,	/* Comment [ Rear axle I gain factor % at error 10kph ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_F1        */		     280,	/* Comment [ Symmetric spin target control torque at 1st gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_F2        */		     280,	/* Comment [ Symmetric spin target control torque at 2nd gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_F3        */		     280,	/* Comment [ Symmetric spin target control torque at 3rd gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_F4        */		     280,	/* Comment [ Symmetric spin target control torque at 4th gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_F5        */		     280,	/* Comment [ Symmetric spin target control torque at 5th gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_R1        */		     280,	/* Comment [ Symmetric spin target control torque at 1st gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_R2        */		     280,	/* Comment [ Symmetric spin target control torque at 2nd gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_R3        */		     280,	/* Comment [ Symmetric spin target control torque at 3rd gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_R4        */		     280,	/* Comment [ Symmetric spin target control torque at 4th gear ] */
	/* int16_t     	S16TCSCpBsTBrkTorq4SymWhlSp_R5        */		     280,	/* Comment [ Symmetric spin target control torque at 5th gear ] */
	/* uchar8_t    	U8TCSCpPresDifLR4SymSp                */		      20,	/* Comment [ Exit condition of the Symmetric spin detection for Front wheel, if pressure difference between left and right wheel >= U8TCSCpPresDifLR4SymSpF, symmetric spin detection will be end ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpSymBoth2WlSpdDifTh             */		      32,	/* Comment [ symmetric spin감지를 위한 양쪽 2wheel의 speed 차이 threshold ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpSymBoth2WlSpinTh               */		      28,	/* Comment [ symmetric spin감지를 위한 양쪽 2wheel의 spin threshold ] */
	                                                        		        	/* ScaleVal[    3.5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpSymExtHysOfMovDiffLnR          */		       8,	/* Comment [ Symmetric spin해제를 위해 좌우 moving average의 speed 차이에 Add되는 hysterisis ] */
	                                                        		        	/* ScaleVal[      1 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTBrkTorqDec4Sym                */		      13,	/* Comment [ Target brake torque decrease amount per 10ms when finish symmetric spin control ] */
	/* uchar8_t    	U8TCSCpDecBsTarBrkF4ESCBrkCtl         */		      20,	/* Comment [ Decrease brake torque rate in ESC Control ] */
	/* int16_t     	S16TCSCpBrkCtlTrqAddInVIB             */		     150,	/* Comment [ Add Brake Torque at Vibration Control ] */
	/* uchar8_t    	U8_TCS_VIB_TIMESHIFT_SCAN_TCMF        */		       3,	/* Comment [ TimeShift about Rise/Hold/Dump time in Vibration Brake Mode ] */
	/* int16_t     	S16TCSCpBrkCtlTrqOfStuck              */		    1500,	/* Comment [ Stuck control시 인가하는 Base Torque량 ] */
	/* int16_t     	S16TCSCpCtlMotorV_Stuck               */		    5000,	/* Comment [ stuck control시 motor voltage ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8TCSCpStuckPressureRiseTime          */		      30,	/* Comment [ stuck control시 압력제어 인가 cycle time ] */
	/* int16_t     	S16TCSCpBrkCtlTrqOfVCA                */		     600,	/* Comment [ target brake torque for VCA control ] */
	/* int16_t     	S16TCSCpCtlMotorV_VCA                 */		    2000,	/* Comment [ VCA normal mode motor voltage ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpInitMaxMotorV_VCA             */		    2000,	/* Comment [ VCA initial max motor voltage ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpInitMaxVCATCCurnt             */		    1600,	/* Comment [ VCA initial max current ] */
	                                                        		        	/* ScaleVal[      1.6 A ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8TCSCpIniVCACtlModeMtorTime          */		       7,	/* Comment [ Initial Voltage Time of VCA ] */
	/* uchar8_t    	U8TCSCpIniVCACtlModeTime              */		      14,	/* Comment [ VCA initial mode time ] */
	/* uchar8_t    	U8TCSCpRearCtlOfVCA                   */		       1,	/* Comment [ VCA Rear Control Setting 0:Front Axle  1: Rear  Axle ] */
	/* uchar8_t    	U8_ACC_TH_VCA                         */		      10,	/* Comment [ VCA 작동을 위한 차량 가속도(acc_r)의 Threshold ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_MAX_VCA_SUSTAIN_SCAN               */		     142,	/* Comment [ VCA 작동 후 여기 설정된 Scan 이 경과하면 작동 종료 ] */
	/* uchar8_t    	U8_SPEED_TH_VCA                       */		      56,	/* Comment [ VCA 작동을 위한 차속의 Threshold ] */
	                                                        		        	/* ScaleVal[      7 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SPIN_TH_VCA                        */		       0,	/* Comment [ VCA 작동을 위한 Wheel Spin의 Threshold ] */
	                                                        		        	/* ScaleVal[      0 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VCA_REENGAGE_ALLOW_TIME        */		      42,	/* Comment [ VCA 작동 종료후 이 시간 이내 에는 재진입 못함 ] */
	/* uchar8_t    	U8_VREF_DECREASE_LIMIT_OF_VCA         */		      20,	/* Comment [ VCA 작동 후 차속 감속이 설정된 값을 초과하면 작동 종료 ] */
	                                                        		        	/* ScaleVal[    2.5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpTarWhlPreTurnMax_1             */		     150,	/* Comment [ 선회시 Speed1에서의 최대압력 Limit ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpTarWhlPreTurnMax_2             */		     120,	/* Comment [ 선회시 Speed2에서의 최대압력 Limit ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpTarWhlPreTurnMax_3             */		     100,	/* Comment [ 선회시 Speed3에서의 최대압력 Limit ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpTarWhlPreTurnMax_4             */		      90,	/* Comment [ 선회시 Speed4에서의 최대압력 Limit ] */
	                                                        		        	/* ScaleVal[      9 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpTarWhlPreTurnMax_5             */		      80,	/* Comment [ 선회시 Speed5에서의 최대압력 Limit ] */
	                                                        		        	/* ScaleVal[      8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpBrkTorqDecPcntOfEngStl         */		      60,	/* Comment [ Target brake torque decrease level at the moment of engine stall is detected ] */
	/* uchar8_t    	U8TCSCpBrkTorqDecRateOfEngStl         */		      50,	/* Comment [ Target brake torque decrease rate when engine stall is detected ] */
	/* uchar8_t    	U8TCSCpBrkTorqRecPcntOfEngStl         */		      50,	/* Comment [ Target brake torque recovery level at the moment of engine stall detection is cleared ] */
	/* uchar8_t    	U8TCSCpTBrkTorqDec4SymInStall         */		      50,	/* Comment [ Target brake torque decrease rate when engine stall is detected ] */
	/* int16_t     	S16TCSCpBrkTorqDecRateOfOvBrk         */		     150,	/* Comment [ Target brake torque decrease rate when over braking is detected ] */
	/* uchar8_t    	S8TCSCpOverBrkTh                      */		       3,	/* Comment [ Counter Threshold of over braking detection ] */
	/* uchar8_t    	U8TCSCpBrkTorqDecPcntOfOvBrk          */		      50,	/* Comment [ Target brake torque decrease level at the moment of over braking is detected ] */
	/* int16_t     	S16TCSCpWLSpdCrosTh4QuickFadOut       */		      16,	/* Comment [ wheel speed cross threshold for quick fade out reset ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8TCSCpFadeOutDctScan                 */		       3,	/* Comment [ Fade Out Mode Detection Scan ] */
	/* uchar8_t    	U8TCSCpFadeOutExitScan                */		       3,	/* Comment [ Fade Out Mode Exit Scan ] */
	/* uchar8_t    	U8TCSCpFadeOutSpdDiffThr              */		      16,	/* Comment [ Fade Out Mode Enter Threshold   wheel spin difference ] */
	                                                        		        	/* ScaleVal[      2 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpSymSpFadeOutHystSpd            */		       8,	/* Comment [ symmetric spin fade out hysteresis speed (between symmetric enter speed and fade out start speed -2kph) ] */
	                                                        		        	/* ScaleVal[      1 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSFadeOutBrkCtrlMinTorq            */		     250,	/* Comment [ Minimum Fade Out Torque level for torque Reset ] */
	/* int16_t     	S16TCSCpFadeOutCtrlHghPreSTTh         */		     200,	/* Comment [ High pressure level in stright fade out mode ] */
	                                                        		        	/* ScaleVal[     20 Bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpFadeOutCtrlLowPreSTTh         */		      50,	/* Comment [ Low pressure level in stright fade out mode ] */
	                                                        		        	/* ScaleVal[      5 Bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpFadeOutDecTQ4HghPreST         */		       2,	/* Comment [ Torque decrease rate in High level stright fade out mode ] */
	/* int16_t     	S16TCSCpFadeOutDecTQ4LowPreST         */		       5,	/* Comment [ Torque decrease rate in Low level stright fade out mode ] */
	/* int16_t     	S16TCSCpFadeOutCtrlHghPreTunTh        */		     150,	/* Comment [ High pressure level in turn fade out mode ] */
	                                                        		        	/* ScaleVal[     15 Bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpFadeOutCtrlLowPreTunTh        */		      50,	/* Comment [ Low pressure level in turn fade out mode ] */
	                                                        		        	/* ScaleVal[      5 Bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16TCSCpFadeOutDecTQ4HghPreTurn       */		      10,	/* Comment [ Torque decrease rate in High level turn fade out mode ] */
	/* int16_t     	S16TCSCpFadeOutDecTQ4LowPreTurn       */		      10,	/* Comment [ Torque decrease rate in Low level turn fade out mode ] */
	/* int16_t     	S16TCSCpBrkTorq1stDecTrqOfHWIS        */		     400,	/* Comment [ Target brake torque decrease level at the moment of high mu wheel instability is detected ] */
	/* uchar8_t    	U8CpAfterHghMuUnstbCntTh              */		      50,	/* Comment [ HighMu Unstable 해제 후 Torq up 시 천천히 올리는 시간 Scan ] */
	/* uchar8_t    	U8TCSCpBrkTorqDecRateOfHWIS           */		      60,	/* Comment [ Nm/10ms Target brake torque decrease rate when high mu wheel instability is detected ] */
	/* uchar8_t    	U8TCSCpBrkTorqFastDecRateOfHWIS       */		     150,	/* Comment [ Nm/10ms Target brake torque decrease rate when high mu wheel Fast decrease is detected ] */
	/* uchar8_t    	U8TCSCpBrkTorqRecPcntOfHWIS           */		      80,	/* Comment [ Target brake torque recovery level at the moment of high mu wheel instability detection is cleared ] */
	/* uchar8_t    	U8TCSCpHghMuWhlDelSpinTh              */		      20,	/* Comment [ High Mu 판단 Undtable 좌우 속도 차 Threshold ] */
	                                                        		        	/* ScaleVal[    2.5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpHghMuWhlHuntSpinTh             */		      10,	/* Comment [ hunting상황에서 high mu의 spin이 cal.값보다 크면 high side spin 즉시감지 ] */
	                                                        		        	/* ScaleVal[   1.25 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpHghMuWhlSpinTh                 */		      24,	/* Comment [ If spin of high-mu wheel is greater than U8TCSCpHghMuWhlSpinTh for U8TCSCpHghMuWhlSpinTm, high-mu wheel instability will be detected ] */
	                                                        		        	/* ScaleVal[      3 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpHghMuWhlSpinTm                 */		       4,	/* Comment [ If spin of high-mu wheel is greater than U8TCSCpHghMuWhlSpinTh for U8TCSCpHghMuWhlSpinTm, high-mu wheel instability will be detected ] */
	/* uchar8_t    	U8TCSCpMtrFstRnTmrAftHghMuWhStb       */		      10,	/* Comment [ wheel on high-mu 가 stable 상태로 되돌아 온 경우에도 이 시간 동안은 S16TCSCpAddMtrV4SPHillHghMuUnSt 를 적용 ] */
	/* uchar8_t    	U8TCSHghMuUnstbRecoveryTh             */		      24,	/* Comment [ High side unstable Recovery threshold ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSCpReducePgainF4GearShift         */		      90,	/* Comment [ Reduce P gain Factor in Gear Shift ] */
	/* int16_t     	S16TCSCpWorkTh4HuntDctReset           */		     500,	/* Comment [ brake torque work threshold for hunting detect reset ] */
	/* int16_t     	S16TCSCpWorkTh4L2SplitSuspect         */		     200,	/* Comment [ Low To Split Detection work threshold ] */
	/* uchar8_t    	U8TCSCpFFReductFactInHunting          */		      25,	/* Comment [ base torque reduction rate in hunting detecting ] */
	/* uchar8_t    	U8TCSCpL2SplitSuspectTorq             */		       5,	/* Comment [ Low to Split  Feed Forward Add torque rate ] */
	/* int16_t     	S16TCSCpBrkTrqMaxIncStartTime         */		     150,	/* Comment [ brake torque max increase start time ] */
	/* int16_t     	S16TCSCpWorkTh4BrkTrqMaxLim           */		     850,	/* Comment [ work threshold for brake torque max limit ] */
	/* int16_t     	S16TCSCpWorkTh4FFAdapt                */		    1500,	/* Comment [ work threshold for feedforward adaptation ] */
	/* int16_t     	S16TCSCpWorkTh4FFAdaptReset           */		    4000,	/* Comment [ work threshold for feedforward adaptation reset ] */
	/* uchar8_t    	U8TCSCpBrkTrqMaxIncRate               */		       6,	/* Comment [ brake torque max increase rate for max limit control ] */
	/* uint16_t    	ETCS_THR1_0                           */		      48,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_1                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_2                           */		      64,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_3                           */		      64,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_4                           */		      64,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_0                           */		      88,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     11 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_1                           */		      88,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     11 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_2                           */		      96,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_3                           */		      96,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_4                           */		      96,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_START_ENGINE_TORQUE           */		     200,	/* Comment [ 이 값보다 Engine Torque가 커야지 TCS 제어 허용 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_START_RPM                     */		     800,	/* Comment [ 이 값보다 RPM이 커야지 TCS 제어 허용 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_0            */		      16,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_1            */		      24,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_2            */		      32,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_0            */		      32,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_1            */		      32,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_2            */		      40,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	ENG_OK_RPM                            */		    1000,	/* Comment [ 엔진 정상적인 경우로 판단하는 RPM을 설정 ] */
	/* uint16_t    	ENG_OK_TORQ                           */		     400,	/* Comment [ 엔진 정상적인 경우의 최소 토크 레벨을 설정 ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_STALL_RPM                         */		     900,	/* Comment [ 엔진 스톨로 판단하는 RPM을 설정 ] */
	/* uint16_t    	ENG_STALL_TORQ                        */		     350,	/* Comment [ 엔진 스톨 시의 최소 토크 레벨을 설정 ] */
	                                                        		        	/* ScaleVal[       35 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ETCS_ENG_STL_TORQ_RISE                */		     500,	/* Comment [ ETCS 작동 중 Engine Stall 감지시 Torque 보상 추가 Rise Rate 설정 ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	FTCS_ENG_STL_TORQ_RISE                */		       0,	/* Comment [ FTCS 작동 중 Engine Stall 감지시 Torque 보상 추가 Rise Rate 설정 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_0_0               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_0_1               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_0_2               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_1_0               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 2000 , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_1_1               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 2000 , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_1_2               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 2000 , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_2_0               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 3000 , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_2_1               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 3000 , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_2_2               */		       0,	/* Comment [ Minimum Torque Level, [engine rpm : 3000 , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_IN_TURN_0         */		     200,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_IN_TURN_1         */		     400,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_IN_TURN_2         */		     500,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NEW_ENG_OK_TORQ_IN_TURN_3         */		     600,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED4] ] */
	                                                        		        	/* ScaleVal[      60 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_ETCS_ENG_STL_TORQ_RISE_RATE        */		       0,	/* Comment [ STALL 감지시 토크 Rise Rate 설정(예를 들면 1:0.1%/1Scan , 2:0.1%/2Scan, 3:0.1%/3Scan 증가) ] */
	/* uchar8_t    	U8_TCS_ENG_STALL_RPM_OFFSET           */		     100,	/* Comment [ RPM 기울기에 상관 없이 RPM이 ENG_STALL_RPM - U8_TCS_ENG_STALL_RPM_OFFSET보다 작으면 Stall 감지하고, ENG_OK_RPM + U8_TCS_ENG_STALL_RPM_OFFSET보다 크면 Stall 감지 해제함. ] */
	/* int16_t     	S16_HIGH_RISK_STALL_RPM_FLAT          */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is High on the flat] ] */
	/* int16_t     	S16_HIGH_RISK_STALL_RPM_HILL          */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is High on the hill] ] */
	/* int16_t     	S16_HIGH_RISK_STALL_TORQ_FLAT         */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is High on the flat] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HIGH_RISK_STALL_TORQ_HILL         */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is High on the hill] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LOW_RISK_STALL_RPM_FLAT           */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is Low on the flat] ] */
	/* int16_t     	S16_LOW_RISK_STALL_RPM_HILL           */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is Low on the hill] ] */
	/* int16_t     	S16_LOW_RISK_STALL_TORQ_FLAT          */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Low on the flat] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LOW_RISK_STALL_TORQ_HILL          */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Low on the hill] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MID_RISK_STALL_RPM_FLAT           */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is Mid on the flat] ] */
	/* int16_t     	S16_MID_RISK_STALL_RPM_HILL           */		     800,	/* Comment [ RPM to detect engine stall[Stall rise index is Mid on the hill] ] */
	/* int16_t     	S16_MID_RISK_STALL_TORQ_FLAT          */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Mid on the flat] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MID_RISK_STALL_TORQ_HILL          */		     350,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Mid on the hill] ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_SLOW_TORQ_DEVIATION                */		       7,	/* Comment [ In Torque intervention, difference value between Slow torque request and Fast torque request : ( (Slow torque request - Fast torque request) =  U8_SLOW_TORQ_DEVIATION ) ] */
	/* uint16_t    	U16_TCS_SPEED1                        */		      40,	/* Comment [ ETCS 제어시 적용되는 정차 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED2                        */		      80,	/* Comment [ ETCS 제어시 적용되는 저속/중속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED3                        */		     240,	/* Comment [ ETCS 제어시 적용되는 중속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED4                        */		     560,	/* Comment [ ETCS 제어시 적용되는 중속/고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     70 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED5                        */		     800,	/* Comment [ ETCS 제어시 적용되는 고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_AX_RANGE_HIGH_MU_MAX              */		      80,	/* Comment [ High-mu에서 정차중 급가속시 발생되는 최대의 가속도 ] */
	                                                        		        	/* ScaleVal[      0.4 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_AX_RANGE_MAX                      */		      50,	/* Comment [ 해당 차량이 Snow 노면에서 낼수 있는 최대 종가속도(acc_r 기준) ] */
	                                                        		        	/* ScaleVal[     0.25 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_AX_RANGE_MIN                      */		      10,	/* Comment [ 해당 차량이 Ice 노면에서 낼수 있는 최대 종가속도(acc_r 기준) ] */
	                                                        		        	/* ScaleVal[     0.05 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_FACTOR         */		      60,	/* Comment [ 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 P/D Gain Decrease Factor ] */
	                                                        		        	/* ScaleVal[        6 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_INC_F          */		     100,	/* Comment [ 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 Spin이 Target 보다 작을때 P/D Gain Increase Factor ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_INC_F_S        */		      20,	/* Comment [ Split-mu 판단시 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 Spin이 Target 보다 작을때 P/D Gain Increase Factor ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_TCS_TURN_DET_4_ENT_BY_ALAT_TH     */		     400,	/* Comment [ ETCS enter threshold 에서 휠스티어나 횡가속도값이 선회 판단인 상태에서 ESC 횡가속도 값(det_alatm)이 해당 TP 보다 작을때 선회 Enter threshold decrease 적용됨 ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCS_TURN_DET_BY_ALAT_TH           */		     200,	/* Comment [ ETCS enter threshold decrease, 1st scan drop, Gain add in turn, gain decrease in highmu 에 횡가속도 값이 해당 TP 보다 큰경우 선회 판단 제어가 적용됨 ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_TCS_OFF_OFFSET_IN_TURN            */		      50,	/* Comment [ EEC 제어 후 spin 이 1kph 미만이고 ESC 횡가속도가 0.5g 이상일때 cal_torq는 max 토크의 해당 TP%만큼 설정 됨 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_0            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_1            */		      48,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_2            */		      40,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_3            */		      40,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_0            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_1            */		      48,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_2            */		      40,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_3            */		      40,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_0           */		      80,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_1           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_2           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_3           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_0           */		      80,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_1           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_2           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_3           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* char8_t     	S8_TARGET_SPIN_ALAT_DIFF_1            */		     -50,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 alatm 기울기 값(Factor : 1.5배) ] */
	                                                        		        	/* ScaleVal[    -0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -0.128 <= X <= 0.127 ] */
	/* char8_t     	S8_TARGET_SPIN_ALAT_DIFF_2            */		     -10,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 alatm 기울기 값(Factor : 1.0배) ] */
	                                                        		        	/* ScaleVal[    -0.01 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -0.128 <= X <= 0.127 ] */
	/* char8_t     	S8_TARGET_SPIN_ALAT_DIFF_3            */		      50,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 alatm 기울기 값(Factor : 0.8배) ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -0.128 <= X <= 0.127 ] */
	/* char8_t     	S8_TARGET_SPIN_YAW_RATE_DIFF_1        */		     -50,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 del_t 기울기 값(Factor : 1.5배) ] */
	                                                        		        	/* ScaleVal[ -0.5 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_TARGET_SPIN_YAW_RATE_DIFF_2        */		     -10,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 del_t 기울기 값(Factor : 1.0배) ] */
	                                                        		        	/* ScaleVal[ -0.1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_TARGET_SPIN_YAW_RATE_DIFF_3        */		      50,	/* Comment [ Target Delta V 감소량 Factor를 결정하는 del_t 기울기 값(Factor : 0.8배) ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_1          */		      32,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_2          */		      16,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_3          */		      16,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_1         */		      32,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_2         */		      16,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_3         */		      16,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_1         */		     200,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_2         */		     100,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_3         */		     100,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_GT_HOMO            */		       0,	/* Comment [ Homo-mu 제어 중 Target Delta V를 잘 추종하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_GT_SPLT            */		       0,	/* Comment [ Split-mu 제어 중 Target Delta V를 잘 추종하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_ROUGH_ROAD         */		       0,	/* Comment [ Rough Road 감지시 Engine 제어에 사용되는 Target Delta V 증가량 ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_SPLIT_HILL         */		      48,	/* Comment [ Split-mu 제어 중 Hill 판단하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_0            */		      30,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_1            */		      30,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_2            */		      40,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_0            */		      30,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_1            */		      30,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_2            */		      40,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_0            */		      50,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_1            */		      60,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_2            */		      60,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_STEER_TO_ZERO_CNT              */		      30,	/* Comment [ 운전자가 직진으로 해당 값만큼 조향회복 의지일시 Torque Increase를 위하여 Target Spin을 회복하기 위함 ] */
	/* uchar8_t    	U8_TCS_TARGET_SPIN_VARI_F_HSS         */		       0,	/* Comment [ High Side Spin 감지시 Target Spin Level 설정(0:Split-mu Target Spin / 100:Homo-mu Target Spin) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_0        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_1        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_2        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_3        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_0        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_1        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_2        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_3        */		      50,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_0        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_1        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_2        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_3        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_0        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_1        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_2        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_3        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_0        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_1        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_2        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_3        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_0        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_1        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_2        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_3        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_0         */		      48,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_1         */		      48,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_2         */		       8,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_3         */		       8,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_0         */		      48,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_1         */		      48,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_2         */		       8,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_3         */		       8,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_0           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_1           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_2           */		      20,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_3           */		       1,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_0           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_1           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_2           */		      20,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_3           */		       1,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_0          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_1          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_2          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_3          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_0          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_1          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_2          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_3          */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_0       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_1       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_2       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_3       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_0       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_1       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_2       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_3       */		      10,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_0             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_1             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_2             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_3             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_0             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_1             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_2             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_3             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_0             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_1             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_2             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_3             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_0             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_1             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_2             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_3             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_0             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_1             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_2             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_3             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_0             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_1             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_2             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_3             */		      50,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_0          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_1          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_2          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_3          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_0          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_1          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_2          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_3          */		      40,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_0        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_1        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_2        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_3        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_0        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_1        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_2        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_3        */		       1,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_0          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_1          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_2          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_3          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_0          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_1          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_2          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_3          */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_0       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_1       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_2       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_3       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_0       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_1       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_2       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_3       */		     750,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   75 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_0        */		      50,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_1        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_0        */		      50,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_1        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_0        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_1        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_0        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_1        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_2        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_0        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_1        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_2        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_0        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_1        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_2        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_0        */		      60,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_1        */		      50,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_0        */		      50,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_1        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_0        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_1        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_2        */		      40,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_0          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_1          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_2          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_0          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_1          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_2          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_0          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_1          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_2          */		       8,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_0        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_1        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_2        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_0        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_1        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_2        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_0        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_1        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_2        */		       1,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  0.1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle(det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_0          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_2          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_3          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_0          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_2          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_3          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_0          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_1          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_2          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_0          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_1          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_2          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_0          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_1          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_2          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_3          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_0          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_1          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_2          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_3          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_0          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_1          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_2          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_0          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_1          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_2          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_0          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_1          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_2          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_3          */		      35,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_0          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_1          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_2          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_3          */		      35,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_0          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_1          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_2          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_3          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_0          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_1          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_2          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_3          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_1          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_2          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_3          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_1          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_2          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_3          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_0          */		      60,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_1          */		      60,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_2          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_0          */		      60,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_1          */		      60,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_2          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_0         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_1         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_2         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_3         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_0         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_1         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_2         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_3         */		      15,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_0         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_1         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_2         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_3         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_0         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_1         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_2         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_3         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_0         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_1         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_2         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_3         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_0         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_1         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_2         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_3         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_0         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_1         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_0         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_1         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_0         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_2         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_3         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_0         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_2         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_3         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_0         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_1         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_2         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_2        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_2        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_0        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_1        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_2        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_0        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_1        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_2        */		      10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_2        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_2        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_0        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_1        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_2        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_0        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_1        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_2        */		     -15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_0        */		     -10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_1        */		     -10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_2        */		     -10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_0        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_1        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_2        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_0        */		     -20,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_1        */		     -20,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_2        */		     -20,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_0        */		     -10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_1        */		     -10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_2        */		     -10,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_0        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_1        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_2        */		      -5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_0        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_1        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_2        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_LM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_0        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_1        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_2        */		      30,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_MM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : S16_HM_EMS, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_0      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED1, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_1      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED2, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_2      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED3, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_3      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED4, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_0      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED1, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_1      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED2, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_2      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED3, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_3      */		      30,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED4, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uint16_t    	U16_ONE_WHEEL_SPIN_LIMIT              */		      80,	/* Comment [ 한쪽 Wheel에만 10KPH 이상의 Spin이 발생하는 SCAN이 이 Parameter에 설정된 값보다 큰 경우 SPLIT-MU 제어로 전환 ] */
	/* uint16_t    	U16_TCS_L2SPLIT_SUSPECT1              */		      50,	/* Comment [ SplitTransitionCnt가 이 값보다 커지면 Split-mu 노면인지 확인하기 위하여 브레이크 압력을 증압하고 엔진토크를 일정량 증가시키는 모드가 작동함 ] */
	/* uchar8_t    	U8_H2S_SUSPECT_QUIT_DELTA_SPD         */		      64,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 좌우 속도차가 이값 이내로 들어오면 즉시 Dump하여 두 wheel 속도가 같아 지도록 유도함 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_THR1                */		       4,	/* Comment [ Homo-mu 제어 중 (구동륜중 속도가 낮은 Wheel 속도 < U8_HOMO2SP_DETECT_THR1) 이고 (구동륜중 속도가 높은 Wheel 속도 > U8_HOMO2SP_DETECT_THR2) 인 경우 Split-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_THR2                */		      48,	/* Comment [ Homo-mu 제어 중 (구동륜중 속도가 낮은 Wheel 속도 < U8_HOMO2SP_DETECT_THR1) 이고 (구동륜중 속도가 높은 Wheel 속도 > U8_HOMO2SP_DETECT_THR2) 인 경우 Split-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_WO_B_CTL_THR1       */		       4,	/* Comment [ 브레이크 제어가 없는 경우 한쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR1보다 작고 다른 쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR2보다 큰 경우 SplitTransitionCnt 증가시킴 ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_WO_B_CTL_THR2       */		      48,	/* Comment [ 브레이크 제어가 없는 경우 한쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR1보다 작고 다른 쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR2보다 큰 경우 SplitTransitionCnt 증가시킴 ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_TORQ_INPUT_FOR_L2SPLT          */		      10,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 엔진 토크의 증가량 결정 ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HUNTING_DETECT_SPEED_DIF          */		      80,	/* Comment [ Spin 좌우 교번시 두 휠의 속도차이 이 값이상이면 Homo-mu로 전환 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_HOMO_TRAN_YAWRATE             */		    1000,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 Yaw Rate 조건 ] */
	                                                        		        	/* ScaleVal[   10 Deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_BOTH_WHEEL_SPIN_LIMIT             */		      50,	/* Comment [ 구동륜 두 Wheel 모두에 5KPH 이상의 Spin이 발생하고, 두 구동륜의 속도차가 1KPH 미만인 SCAN이 Parameter에 설정된 값보다 큰 경우 HOMO-MU 제어로 전환 ] */
	/* uint16_t    	U16_BOTH_WHEEL_SPIN_LIMIT_4WD         */		     100,	/* Comment [ 4WD 차량중 대각의 휠 속도가 반대편 휠 속도보다 크고, 차속 대비 최소 속도 레벨을 지니고 있을 시 count 증가되며 해당 값보다 큰 경우 homo전환 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR1                */		      24,	/* Comment [ Split-mu 제어 중 (좌우 속도차<U8_SP2HOMO_DETECT_THR1) 이고 (구동륜 중 속도가 낮은 Wheel의 Spin > U8_SP2HOMO_DETECT_THR2) 인 경우 Homo-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR2                */		     144,	/* Comment [ Split-mu 제어 중 (좌우 속도차<U8_SP2HOMO_DETECT_THR1) 이고 (구동륜 중 속도가 낮은 Wheel의 Spin > U8_SP2HOMO_DETECT_THR2) 인 경우 Homo-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[     18 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR_4WD             */		      32,	/* Comment [ 4WD차량 중 후륜의 좌우 속도와 차속의 차가 해당 값보다 작으면 homo 전환 count 증가 ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_CTL_START_HOMO_AY              */		     180,	/* Comment [ Ay Model에 의한 횡가속도 계산 값이 이 값 이상이면 Homo-mu 제어로 전환이 진행됨 ] */
	                                                        		        	/* ScaleVal[     0.18 G ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_TCS_HOMO_TRAN_ADD_CNT              */		       4,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 Counter 증가량 ] */
	/* uchar8_t    	U8_TCS_HOMO_TRAN_MAX_SPD              */		      80,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 차속 조건 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_P_GAIN_INC_AFTER_EEC_0G1           */		      10,	/* Comment [ EEC 제어후 Fade Out Mode 감지시 det_alatm과 alat의 차이가 0.1g 인 경우 P Gain의 증가량 설정 ] */
	/* uchar8_t    	U8_P_GAIN_INC_AFTER_EEC_0G8           */		      15,	/* Comment [ EEC 제어후 Fade Out Mode 감지시 det_alatm과 alat의 차이가 0.8g 인 경우 P Gain의 증가량 설정 ] */
	/* uchar8_t    	U8_TORQ_FAST_UP_AFTER_EEC_SCAN        */		      10,	/* Comment [ EEC 제어후 Fade Out Mode 감지 민감도 설정(증가시켜면 둔감하게 감지됨) ] */
	/* int16_t     	S16_TCS_BACKUP_ENTER_AY               */		     300,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 lateral acceleration. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCS_BACKUP_ENTER_YAW              */		     300,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 delta yaw. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TCS_BACKUP_RISE_START_TIME        */		     100,	/* Comment [ Δyaw가 3deg/s 이하이고, 발생하는 Spin이 Target Spin의 1/2인 시간이 이 값 이상이면 Back-up 모드 작동하여 토크 빠르게 회복 ] */
	                                                        		        	/* ScaleVal[      1 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_TCS_RED_ENG_TORQ_IN_GSC           */		     200,	/* Comment [ Backup Torque에 의한 제어 해제 시 기준 Torque Level 설정 적용에서 Gear 변속 시 cal_torq와 eng_torq와의 차이가 해당 값보다 크면 기준 Torque Level을 cal_torq로 설정함 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_1ST_RISE_DURATION       */		      80,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 지속 시간 ] */
	                                                        		        	/* ScaleVal[    0.8 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_BACKUP_1ST_RISE_RATE           */		      20,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 2 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_RPM_SLOPE         */		      12,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 RPM Slope. 해당 값보다 작아야 진입 조건 만족 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_THR1              */		       8,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[     1 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_THR2              */		       6,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin diff error. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[ 0.075 km/h ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_BACKUP_LAST_RISE_RATE          */		      30,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 마지막 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 3 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_MONITOR_TIME            */		      80,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드 작동 후 휠거동 모니터링 시간 ] */
	                                                        		        	/* ScaleVal[    0.8 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_BACKUP_TORQUE_IMPROVE          */		       1,	/* Comment [ Backup Torque에 의한 제어 해제 시 기준 Torque Level 설정 적용 유무 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_CLR_RPM             */		    3500,	/* Comment [ 이 RPM 이상인 경우 Split Hill 감지 해제 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_CLR_TIME            */		     186,	/* Comment [ Split Hill 감지 후 counter가 이 값 보다 작아지면 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[       1.86 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_DCT_RPM             */		    3400,	/* Comment [ 이 RPM 이하에서만 Split Hill 감지 허용 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_DCT_TIME            */		     257,	/* Comment [ Split Hill 경향이 이 시간 동안 지속되면 Split Hill 감지 ] */
	                                                        		        	/* ScaleVal[       2.57 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* char8_t     	S8_TCS_HILL_AX_DIFF                   */		      50,	/* Comment [ Split Hill 판단을 위한 종G Sensor 값의 기울기 ] */
	/* char8_t     	S8_TCS_HILL_DELTA_AX                  */		      50,	/* Comment [ Split Hill 판단을 위한 종G Sensor 값의 변화량 ] */
	/* char8_t     	S8_TCS_HILL_SLOPE                     */		      12,	/* Comment [ Split Hill 판단을 위한 종G Sensor 값(=구배율) ] */
	/* uchar8_t    	U8_TCS_ENABLE_SPLT_HILL_DCT           */		       1,	/* Comment [ Split Hill 감지 로직 작동 허용 여부 Setting ] */
	/* uchar8_t    	U8_TCS_HILL_DCT_TIMES_BY_AX           */		      50,	/* Comment [ 상기 조건(S8_TCS_HILL_SLOPE, AX_DIFF, DLTA_AX) 만족시 해당 값보다 클 시 Split Hill 판단을 위한 Count 2배씩 증가 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_CLR_ACCEL            */		      30,	/* Comment [ 이 차체 가속도 이상인 경우 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[     0.15 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_CLR_SPD              */		     160,	/* Comment [ 이 속도 이상인 경우 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_ACCEL            */		      28,	/* Comment [ 이 차체 가속도 이하에서만 Split Hill 감지를 허용 ] */
	                                                        		        	/* ScaleVal[     0.14 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_PRESS_FRNT       */		      10,	/* Comment [ FWD 차량의 Split Hill 감지 시작 Brake Pressure ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_PRESS_REAR       */		      10,	/* Comment [ RWD 차량의 Split Hill 감지 시작 Brake Pressure ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_SPD              */		     152,	/* Comment [ 이 속도 이하에서만 Split Hill 감지를 허용 ] */
	                                                        		        	/* ScaleVal[     19 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_GD_TRACK_SET_TIME              */		     150,	/* Comment [ Spin이 Target을 U8_TCS_GD_TRACK_SET_TIME동안 잘 추종하는 경우 U8_TCS_TORQ_UP_IN_GD_TRACKING만큼 토크 증가함 ] */
	/* uchar8_t    	U8_TCS_TORQ_UP_IN_GD_TRACKING         */		      50,	/* Comment [ Spin이 Target을 U8_TCS_GD_TRACK_SET_TIME동안 잘 추종하는 경우 U8_TCS_TORQ_UP_IN_GD_TRACKING만큼 토크 증가함 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TCS_1ST_SCAN_TRQ_MOD_HILL         */		       0,	/* Comment [ Hill 등판중, 제어 진입시 첫 Scan의 토크 저감량에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_1ST_SCAN_TRQ_MOD_IN_GS        */		     200,	/* Comment [ Gear 변속중, 제어 진입시 첫 Scan의 토크 저감량에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_0          */		      70,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_1          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_2          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_3          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_0          */		      70,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_1          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_2          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_3          */		      85,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_0          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_1          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_2          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_3          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_0         */		      80,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_1         */		      80,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_2         */		      80,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_0         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_1         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_2         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_0         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_1         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_2         */		      90,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_0         */		      70,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_1         */		      70,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_2         */		      70,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_3         */		      70,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_0         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_1         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_2         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_3         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_0         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_1         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_2         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_3         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_0         */		      55,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_1         */		      55,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_2         */		      55,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_0         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_1         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_2         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_0         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_1         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_2         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_DISCRETE_TORQ_LIMIT            */		      10,	/* Comment [ limit value of the discrete torque reduction and recovery : actual discrete torque value <=max_torq*U8_TCS_DISCRETE_TORQ_LIMIT/100 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_HIGH_H           */		      32,	/* Comment [ In Homo-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_LOW_H            */		       4,	/* Comment [ In Homo-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[   0.05 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_MID_H            */		      16,	/* Comment [ In Homo-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.2 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_HIGH_H           */		      60,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_HIGH_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_HIGH_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_LOW_H            */		      20,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_LOW_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_LOW_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_MID_H            */		      40,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_MID_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_MID_H  / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_HIGH_H          */		      32,	/* Comment [ In Homo-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_LOW_H           */		       4,	/* Comment [ In Homo-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[   0.05 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_MID_H           */		      16,	/* Comment [ In Homo-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.2 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_HIGH_H           */		      60,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_HIGH_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_HIGH_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_LOW_H            */		      20,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_LOW_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_LOW_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_MID_H            */		      25,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_MID_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_MID_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_HIGH_SP          */		      32,	/* Comment [ In Split-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_LOW_SP           */		       4,	/* Comment [ In Split-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[   0.05 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_MID_SP           */		      16,	/* Comment [ In Split-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.2 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_HIGH_SP          */		      60,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_HIGH_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_HIGH_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_LOW_SP           */		      20,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_LOW_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_LOW_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_MID_SP           */		      40,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_MID_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_MID_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_HIGH_SP         */		      32,	/* Comment [ In Split-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_LOW_SP          */		       4,	/* Comment [ In Split-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[   0.05 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_MID_SP          */		      16,	/* Comment [ In Split-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.2 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_HIGH_SP          */		      60,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_HIGH_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_HIGH_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_LOW_SP           */		      20,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_LOW_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_LOW_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_MID_SP           */		      40,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_MID_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_MID_SP / 100) ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_0             */		    1000,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     100 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_1             */		    1200,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     120 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_2             */		    1400,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     140 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_3             */		    1600,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     160 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIM_MOD_SPLIT_HILL        */		    1000,	/* Comment [ Hill 등판중, Split-mu 제어 시 토크의 최소값 제한에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_0          */		     120,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_1          */		     120,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_2          */		     120,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_3          */		     120,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_0          */		     160,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_1          */		     160,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_2          */		     160,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_3          */		     160,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_TCS_TARGET_TORQ_DISABLE_HSS        */		      80,	/* Comment [ High Side Spin이 이 값 이상이면 Target Torque는 ENG_OK_TORQ까지 감소됨 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_TARGET_TORQ_INC_RATE           */		      40,	/* Comment [ Target Torque의 SCAN 당 증가량 최대값 설정 ] */
	                                                        		        	/* ScaleVal[  4 Nm/SCAN ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TRQ_DEC_TH_SPLIT_HILL          */		       0,	/* Comment [ Split-mu hill 제어시 Spin에의한  Torque Limit 저감 시작을 Cal.값만큼 둔감화 ] */
	                                                        		        	/* ScaleVal[      0 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_0        */		     800,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      80 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_1        */		     900,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      90 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_2        */		    1000,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     100 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_0        */		     900,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      90 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_1        */		    1000,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     100 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_2        */		    1100,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     110 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_0        */		    2500,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_1        */		    2500,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_2        */		    2500,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_0        */		       1,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     0.1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_1        */		       2,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     0.2 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_2        */		       3,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_LM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     0.3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_0        */		       2,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     0.2 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_1        */		       3,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     0.3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_2        */		       4,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_MM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     0.4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_0        */		       3,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     0.3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_1        */		       4,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     0.4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_2        */		       5,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : S16_HM_EMS 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     0.5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TCS_DIAG_WHL_SPIN_CNT             */		     100,	/* Comment [ 대각 Spin에 의한 Stuck 감지 시간 설정(대각 Spin 경향이 이 값 이상동안 지속되면 대각 Spin에 의한 Stuck 감지) ] */
	/* uint16_t    	U16_TCS_DIAG_WHL_SPIN_END_SPD         */		      80,	/* Comment [ 차속이 이 값 이하인 경우만  대각 Spin에 의한 Stuck 감지 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_TCS_DIAG_WHL_SPIN_TH               */		      40,	/* Comment [ 대각 Spin에 의한 Stuck 감지 시작 Spin Level 설정 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_T_SPIN_INC_IN_DIA_WHL_SPIN         */		     120,	/* Comment [ 대각 Spin에 의한 Stuck 감지시 Target Spin 설정 ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_TCS_DELTA_DRV_REQ_TORQ            */		     150,	/* Comment [ Lamp Staragety1. torq가 증가 경향이고 drive torq와 cal torq와의 차이가 해당 값보다 작을 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_TCS_FUN_LAMP_OFF_OFFSET           */		     200,	/* Comment [ cal_torq가 eng_torq보다 이 변수에 설정된 값보다 큰 경우 Function Lamp 작동 종료 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MIN_TORQ_PERCENT         */		      70,	/* Comment [ Lamp Staragety2. cal torq가 drive torq의 해당 percentage 이상일 시 Function Lamp 조기 해제 만족 ] */
	/* uchar8_t    	U8_FLAMP_OFF_TORQ_RATE_LIMIT          */		      40,	/* Comment [ Lamp Staragety2. cal torq 증가율이 S16_TCS_DELTA_REQ_TORQ보다 큰 상태에서 증가된 Count가 해당 값 이상일 시 Function Lamp 조기 해제 만족 ] */
	/* uchar8_t    	U8_TCS_DELTA_REQ_TORQ_RATE            */		      40,	/* Comment [ Lamp Staragety2. cal torq 증가율이 해당 값보다 클 시 Function Lamp 조기 해제를 위한 Count 시작 ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FLAMP_OFF_AX_SLOPE                 */		       2,	/* Comment [ Lamp Staragety3. acc_r 기울기가 해당 값보다 크고 acc_r이 U8_FLAMP_OFF_MIN_AX보다 클 시  Function Lamp 조기 해제 유도 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MAX_AX                   */		      60,	/* Comment [ Lamp Staragety3. acc_r이 해당 값보다 클 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[      0.3 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MIN_AX                   */		      50,	/* Comment [ Lamp Staragety3. acc_r이 해당 값보다 크고 acc_r 기울기가 U8_FLAMP_OFF_AX_SLOPE보다 클 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[     0.25 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* char8_t     	S8_FLAMP_OFF_BRK_INPUT                */		      15,	/* Comment [ Lamp Staragety4. BTCS일 시 State2 Dump Mode상태에서 추정압력이 해당 값보다 작을 시 Function Lamp 조기 해제 유도 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S1         */		     160,	/* Comment [ 정차구간(U16_TCS_SPEED1) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S2         */		     160,	/* Comment [ 저속구간(U16_TCS_SPEED2) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S3         */		     160,	/* Comment [ 중속구간(U16_TCS_SPEED3) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S4         */		     160,	/* Comment [ 고속구간(U16_TCS_SPEED1) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TORQ_LIMIT                */		     450,	/* Comment [ Driveline Protection 적용 시 토크 리미트 최대값 설정 ] */
	                                                        		        	/* ScaleVal[      45 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_DP_FRONT_REAR_DV_THR              */		     320,	/* Comment [ 전후 축간 속도차이를 보고 Driveline Protection 제어 수행하기 위한 진입 DV Threshold ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_LEFT_RIGHT_DV_THR              */		     320,	/* Comment [ 좌우 축간 속도차이를 보고 Driveline Protection 제어 수행하기 위한 진입 DV Threshold ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_DETECT_SCAN                 */		    1800,	/* Comment [ Stuck 판단 시간 Threshold ] */
	/* uint16_t    	U16_STUCK_DETECT_SCAN_SP              */		    1800,	/* Comment [ Stuck detection time in Split mu ] */
	/* uint16_t    	U16_STUCK_DETECT_SPEED                */		      24,	/* Comment [ Stuck 구간 속도 Threshold ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_EXIT_SPEED                  */		      32,	/* Comment [ Stuck 종료 속도 ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_COMP              */		      24,	/* Comment [ Stuck 판단 시 실시하는 Target DV 보상량 ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_COMP_SP           */		      32,	/* Comment [ After Stuck detection, Target DV compensation value in split mu ] */
	                                                        		        	/* ScaleVal[          4 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_MAX               */		     200,	/* Comment [ Stuck 판단 시 보상량이 적용된 Target DV의 최대값 ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_MIN               */		      16,	/* Comment [ Stuck 판단 시 보상량이 적용된 Target DV의 최소값 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TORQ_LIMIT                  */		    2000,	/* Comment [ Stuck Algorithm 적용 시 토크 리미트 최대값 설정 ] */
	                                                        		        	/* ScaleVal[     200 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_STUCK_TORQ_LIMIT_SP               */		    2000,	/* Comment [ After Stuck detection, Maximum torque limit in split mu ] */
	                                                        		        	/* ScaleVal[        200 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_STUCK_TORQ_RISE_MAX                */		       8,	/* Comment [ Stuck Algorithm 적용 시 토크 업 기울기 최대값 설정 ] */
	                                                        		        	/* ScaleVal[     0.8 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STUCK_TORQ_RISE_MAX_SP             */		       6,	/* Comment [ After Stuck detection, Maximum torque rise rate in split mu ] */
	                                                        		        	/* ScaleVal[        0.6 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_TCS_ENG_TORQUE_MAX                */		    4000,	/* Comment [ 정차중 Full throttle시 Engine Torque의 최대값, TCS 제어 토크가 이 값 이상인 상황에서 토크 저감시 이 토크 Level까지 즉시 토크 저감후 통상 제어 진행함 ] */
	                                                        		        	/* ScaleVal[     400 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_DEC4BIG_SPIN_CTL          */		     200,	/* Comment [ 2ND Cycle에서 Big Spin Overshoot 감지시 토크 저감량 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_WAIT_T4BIG_SPIN_CTL           */		      40,	/* Comment [ 2ND Cycle에서 Big Spin Overshoot 감지시 토크 저감후 Spin 상태 모니터링 시간, 이 시간 경과후 에도 Big Spin 상태가 지속되면 동일한 토크 저감을 재차 수행함 ] */
	/* int16_t     	S16_TCS_D_GAIN_F_RPM_FAST_INC_1       */		     400,	/* Comment [ 1st cycle fast increase detection of the engine rpm, increase value of the D gain (ex.S16_TCS_D_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* int16_t     	S16_TCS_D_GAIN_F_RPM_FAST_INC_N       */		     300,	/* Comment [ In fast increase detection of the engine rpm, increase value of the D gain (ex.S16_TCS_D_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* int16_t     	S16_TCS_P_GAIN_F_RPM_FAST_INC_1       */		     400,	/* Comment [ 1st cycle fast increase detection of the engine rpm, increase value of the P gain (ex.S16_TCS_P_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* int16_t     	S16_TCS_P_GAIN_F_RPM_FAST_INC_N       */		     300,	/* Comment [ In fast increase detection of the engine rpm, increase value of the P gain (ex.S16_TCS_P_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* uchar8_t    	U8_TCS_CLR_FAST_RPM_INC_TH            */		      10,	/* Comment [ Threshold value to clear fast increase detection of the engine rpm ] */
	/* uchar8_t    	U8_TCS_SET_FAST_RPM_INC_TH            */		      16,	/* Comment [ Threshold value to detect fast increase of the engine rpm ] */
	/* uchar8_t    	U8_TCS_SET_FAST_RPM_INC_TH_1          */		      16,	/* Comment [ 1st cycle Threshold value to detect fast increase of the engine rpm ] */
	/* uchar8_t    	U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E       */		      80,	/* Comment [ In Gear shifting, PD gain tuning value in homo-mu (U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E<100 : decease , U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E>100 : increase) ] */
	/* uchar8_t    	U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F       */		      80,	/* Comment [ In Gear shifting, PD gain tuning value in split-mu (U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F<100 : decease , U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F>100 : increase) ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_MODIFY              */		      20,	/* Comment [ In Gear shifting, increase value of the torque rise rate ] */
	                                                        		        	/* ScaleVal[    2 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_THRESHOLD_MODIFY         */		     160,	/* Comment [ In Gear shifting, increase value of the torque rise threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	U16_GEAR_SHIFT_UP_RPM                 */		    4500,	/* Comment [ In case of 'engine rpm >=  U16_GEAR_SHIFT_UP_RPM' , gear shift up will be premit (for only Gear shift up available vehicle) ] */
	/* uint16_t    	U16_TCS_BUMP_DETECT_SPIN              */		      16,	/* Comment [ If Average wheel spin is less than U16_TCS_BUMP_DETECT_SPIN in U8_TCS_BUMP_DETECT_TIME,  BUMP will be detect. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_TCS_BUMP_ADD_P_GAIN_HIGH_AX        */		      10,	/* Comment [ In Bump detection state, Added P gain value in 'Ax >= U16_AX_RANGE_MAX' ] */
	/* uchar8_t    	U8_TCS_BUMP_ADD_P_GAIN_LOW_AX         */		      10,	/* Comment [ In Bump detection state, Added P gain value in 'Ax <= U16_AX_RANGE_MIN' ] */
	/* uchar8_t    	U8_TCS_BUMP_DETECT_TIME               */		      43,	/* Comment [ Bump detection time setting ] */
	                                                        		        	/* ScaleVal[   0.43 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_TCS_VIB_ETCS_ENG_TORQUE            */		      20,	/* Comment [ Reduction Percentage Value of Engine Torque in Vibration Engine Control at the ETCS ] */
	/* char8_t     	S8_TCS_VIB_FTCS_ENG_TORQUE            */		      20,	/* Comment [ Reduction Percentage Value of Engine Torque in Vibration Engine Control at the FTCS ] */
	/* uchar8_t    	U8_TCS_VIB_AMPLITUDE_ENTER            */		     104,	/* Comment [ Wheel Amplitude to Vibration Control Detect Enterance ] */
	                                                        		        	/* ScaleVal[         13 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_AMPLITUDE_OUT              */		      48,	/* Comment [ Wheel Amplitude to Vibration Control Detect Clearance ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_BRK_CTL_PERIOD             */		       4,	/* Comment [ Vibration Brake Control Period ] */
	/* uchar8_t    	U8_TCS_VIB_CNT_SCAN                   */		       1,	/* Comment [ Increase, Decrease of Wheel Speed Count Filter Scan ] */
	/* uchar8_t    	U8_TCS_VIB_ETCS_ENG_PERIOD            */		       1,	/* Comment [ Vibration Brake Control Period at the ETCS ] */
	                                                        		        	/* ScaleVal[    2 Cycle ]	Scale [ f(x) = (X + 0) * 2 ]  Range   [ 0 <= X <= 510 ] */
	/* uchar8_t    	U8_TCS_VIB_FLAG_ENTER_CYCLE           */		       3,	/* Comment [ Wheel Speed Cycle to Vibration Control in Vibration Detect Condition ] */
	/* uchar8_t    	U8_TCS_VIB_FLAG_OUT_CYCLE             */		       1,	/* Comment [ Wheel Speed Cycle to Vibration Control Hysteresis in Vibration Clearance Condition ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_ENTER            */		       7,	/* Comment [ Wheel Frequency to Vibration Control Detect Enterance ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_OUT              */		       4,	/* Comment [ Wheel Frequency to Vibration Control Detect Clearance ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_THR              */		      20,	/* Comment [ Vibration Control Detect Inhibit about Wheel Fequency in Wheel Noise ] */
	/* uchar8_t    	U8_TCS_VIB_FTCS_ENG_PERIOD            */		       2,	/* Comment [ Vibration Brake Control Period at the FTCS ] */
	/* uchar8_t    	U8_TCS_VIB_REFERENCE_VREF_ENTER       */		      80,	/* Comment [ Wheel Speed(U Peak Point)-Vehicle Speed to Vibration Control Detect Enterance ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_REFERENCE_VREF_OUT         */		     120,	/* Comment [ Wheel Speed(U Peak Point)-Vehicle Speed to Vibration Control Detect Clearance ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16TCSGearShiftInhibitLATAcc          */		     200,	/* Comment [ rpm이 작고 해당 TP 가 ESC 횡가속도(det_alatm) 보다 작을 때 변속 금지 판단, 해제는 rpm이 크거나 해당 TP보다 횡가속도가 클때 변속금지 해제됨 ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_TCS_GSC_RPM_ENTER                 */		   20600,	/* Comment [ EEC 제어 중 & 제어 후 기어 변속 금지 진입 최대 rpm(해당 rpm 이하 변속 금지) ] */
	/* uint16_t    	U16_TCS_GSC_RPM_EXIT                  */		     770,	/* Comment [ EEC 제어 중& 제어 후 기어 변속 금지 해제 rpm (해당 rpm 이상 변속 허용) ] */
	/* char8_t     	S8_COMPLETE_CLOSE_THROTTLE_POS        */		       2,	/* Comment [ 운전자의 가속의지가 전혀 없다고 판단하는 Throttle Position 값 ] */
	/* uchar8_t    	U8_TCS_MOVING_WINDOW_LENGTH           */		      12,	/* Comment [ TCS 제어의 기준 신호가 되는 Moving Average를 계산하는 Window의 길이 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_HIGH_SPD        */		     300,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 80km/h ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_LOWMED_SPD      */		     900,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 20km/h ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_LOW_SPD         */		    1200,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 10km/h ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_MED_SPD         */		     600,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 50km/h ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_HIGH_SPD       */		     300,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 80km/h ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_LOWMED_SPD     */		     900,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 20km/h ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_LOW_SPD        */		    1200,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 10km/h ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_MED_SPD        */		     600,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 50km/h ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	K_Brake_temperature_Overheated_Threshold */		     125,	/* Comment [ 브레이크 온도 추정 값이 이값 이상이면 과열에 의한 TCS 제어 금지 ] */
	                                                        		        	/* ScaleVal[  625 deg C ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	K_Brake_temperature_Overheated_Threshold_Low */		 100,	/* Comment [ 디스크 과열에 의한 TCS 제어 금지중 추정온도가 이값 이하이면 제어 진입 허용 ] */
	                                                        		        	/* ScaleVal[  500 deg C ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	S16TCSCpMaxTCCurnt                    */		    1600,	/* Comment [ Maximum current to exert on TC valve ] */
	                                                        		        	/* ScaleVal[        1.6 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16TCSCpTarWhlPreStMax                */		    1600,	/* Comment [ Maximum Target Pressure ] */
	                                                        		        	/* ScaleVal[    160 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8TCSCpAfterIIntActScan               */		     150,	/* Comment [ I control end sustain time ] */
	/* uchar8_t    	U8TCSCpAveWhlAcc_1                    */		      14,	/* Comment [ average axle wheel acceleration of wheel acc condition 1 ] */
	                                                        		        	/* ScaleVal[     0.14 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpAveWhlAcc_2                    */		     210,	/* Comment [ average axle wheel acceleration of wheel acc condition 2 ] */
	                                                        		        	/* ScaleVal[      2.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpChngRateWhlSpin_1              */		      14,	/* Comment [ Change rate of wheel spin condition 1 ] */
	                                                        		        	/* ScaleVal[     0.14 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpChngRateWhlSpin_2              */		     210,	/* Comment [ Change rate of wheel spin condition 2 ] */
	                                                        		        	/* ScaleVal[      2.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpCurntFiltGain                  */		      14,	/* Comment [ Current filter gain for estimated pressure ] */
	/* uchar8_t    	U8TCSCpFltGain4GearShiftSt            */		      20,	/* Comment [ Gear 변경시 직진 moving average filter gain ] */
	/* uchar8_t    	U8TCSCpFltGain4GearShiftTurn          */		      76,	/* Comment [ Gear 변경시 선회 moving average filter gain ] */
	/* uchar8_t    	U8TCSCpLowPresTh4IniTCVCtl            */		      50,	/* Comment [ low pressure threshold for initial tc valve control mode ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8TCSCpMonTime4AsymSpnCtlS_1          */		       5,	/* Comment [ Monitoring time for activation of asymmetric spin control when ChngRateWhlSpinDiffNF is U8TCSCpChngRateWhlSpin_1 ] */
	/* uchar8_t    	U8TCSCpMonTime4AsymSpnCtlS_2          */		       3,	/* Comment [ Monitoring time for activation of asymmetric spin control when ChngRateWhlSpinDiffNF is U8TCSCpChngRateWhlSpin_2 ] */
	/* uchar8_t    	U8TCSCpMonTime4SymSpnCtlS_1           */		       5,	/* Comment [ Monitoring time for activation of symmetric spin control when WhlAccAvg is U8TCSCpAveWhlAcc_1 ] */
	/* uchar8_t    	U8TCSCpMonTime4SymSpnCtlS_2           */		       3,	/* Comment [ Monitoring time for activation of symmetric spin control when WhlAccAvg is U8TCSCpAveWhlAcc_2 ] */
	/* uchar8_t    	U8TCSCpSymDctScanTime                 */		       8,	/* Comment [ symmetric spin detect scan time ] */
	/* uchar8_t    	U8TCSCpThOfCnt4SpnDiffSlopeDct        */		       4,	/* Comment [ Threshold of counter for spin difference slope detection ] */
	/* uchar8_t    	U8_TCS_ESV_CLOSE_DELAY_SCAN           */		       7,	/* Comment [ 이 값에 설정된 시간만큼 ESV Close Delay됨 ] */
	/* uchar8_t    	U8TCSCpAsmdMuDiff4SplitMuDct          */		      60,	/* Comment [ Split 판단 Threshold 좌우 Mu difference ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8TCSCpSplitMuDctEnterCnt             */		      40,	/* Comment [ Split판단 enter counter scan ] */
	/* uchar8_t    	U8TCSCpSplitMuDctExitCnt              */		      20,	/* Comment [ Split판단 exit counter scan ] */
	/* uchar8_t    	U8TCSCpSplitMuDiffExitHyst            */		      50,	/* Comment [ Split 판단해제 Threshold  좌우 Mu difference  hysterisis ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16TCSCpSplitHilExtDistanceTh         */		   20000,	/* Comment [ Split hill 해제 이동거리 ] */
	                                                        		        	/* ScaleVal[       20 m ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8TCSCpSplitHilDctAccTh               */		      20,	/* Comment [ Split hill판단 종가속도(acc_r) threshold ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8TCSCpSplitHilDctEnterCnt            */		      50,	/* Comment [ Split hill판단 enter counter sacn ] */
	/* uchar8_t    	U8TCSCpSplitHilDctExitCnt             */		      20,	/* Comment [ Split hill판단 exit counter sacn ] */
	/* uchar8_t    	U8TCSCpSplitHilDctSpdTh               */		     144,	/* Comment [ Split hill판단을 위한 max speed threshold (속도 이하에서 판단) ] */
	                                                        		        	/* ScaleVal[     18 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8TCSHillDctAlongTh                   */		      10,	/* Comment [ Hill 판단 노면 기울기 ] */
	/* uint16_t    	U16_TCS_RESERVED_01                   */		      12,
	/* uint16_t    	U16_TCS_RESERVED_02                   */		     200,
	/* uint16_t    	U16_TCS_RESERVED_03                   */		      50,
	/* uint16_t    	U16_TCS_RESERVED_04                   */		     100,
	/* uint16_t    	U16_TCS_RESERVED_05                   */		       6,
	/* uint16_t    	U16_TCS_RESERVED_06                   */		     150,
	/* uint16_t    	U16_TCS_RESERVED_07                   */		      24,
	/* uint16_t    	U16_TCS_RESERVED_08                   */		       5,
	/* uint16_t    	U16_TCS_RESERVED_09                   */		      10,
	/* uint16_t    	U16_TCS_RESERVED_10                   */		      20,
	/* uint16_t    	U16_TCS_RESERVED_11                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_12                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_13                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_14                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_15                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_16                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_17                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_18                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_19                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_20                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_21                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_22                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_23                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_24                   */		       0,
	/* uint16_t    	U16_TCS_RESERVED_25                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_01                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_02                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_03                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_04                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_05                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_06                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_07                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_08                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_09                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_10                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_11                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_12                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_13                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_14                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_15                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_16                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_17                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_18                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_19                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_20                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_21                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_22                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_23                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_24                   */		       0,
	/* int16_t     	S16_TCS_RESERVED_25                   */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_01                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_02                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_03                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_04                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_05                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_06                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_07                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_08                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_09                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_10                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_11                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_12                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_13                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_14                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_15                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_16                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_17                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_18                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_19                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_20                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_21                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_22                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_23                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_24                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_25                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_26                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_27                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_28                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_29                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_30                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_31                    */		       0,
	/* uchar8_t    	U8_TCS_RESERVED_32                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_01                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_02                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_03                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_04                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_05                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_06                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_07                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_08                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_09                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_10                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_11                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_12                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_13                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_14                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_15                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_16                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_17                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_18                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_19                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_20                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_21                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_22                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_23                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_24                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_25                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_26                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_27                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_28                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_29                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_30                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_31                    */		       0,
	/* char8_t     	S8_TCS_RESERVED_32                    */		       0,
	/* uchar8_t    	U8ETCSCpAxSenAvail                    */		       1,	/* Comment [ This calibration parameter represents availability of longitudinal acceleration sensor. If the sensor is available, it should be set to 1 and if not, it should be set to 0. ] */
	/* int16_t     	S16ETCSCpTireRadi                     */		     316,	/* Comment [ This calibration parameter represents tire rolling radius. ] */
	                                                        		        	/* ScaleVal[    0.339 m ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16ETCSCpTtlVehMass                   */		    1680,	/* Comment [ This calibration parameter represents total vehicle mass without any load. ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_1             */		    1436,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at 1st gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[      14.52 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_2             */		     889,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at 2nd gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[       9.08 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_3             */		     575,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at 3rd gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[       5.99 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_4             */		     434,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at 4th gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[       4.22 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_5             */		     306,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at 5th gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[       3.89 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTotalGearRatio_R             */		    1037,	/* Comment [ This calibration parameter represents total gear ratio(=transmission gear ratio * final drive gear ratio) at reverse gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	                                                        		        	/* ScaleVal[       11.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16ETCSCpTrnfrCsGrRtoHgMd             */		     100,	/* Comment [ This calibration parameter represents transfer case high gear ratio. This value can be obtained from vehicle specification and normally 1. ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	S16ETCSCpTrnfrCsGrRtoLwMd             */		     200,	/* Comment [ This calibration parameter represents transfer case low gear ratio. This value can be obtained from vehicle specification and normally 2~3. ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* uchar8_t    	U8ETCSCpGrTransTme                    */		      50,	/* Comment [ This calibration parameter represents transition time for gear change. When gear is changed, total gear ratio and transmission efficiency will be changed gradually during this time period. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTransEff_1                    */		      78,	/* Comment [ This calibration parameter represents transmission efficiency at 1st gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTransEff_2                    */		      88,	/* Comment [ This calibration parameter represents transmission efficiency at 2nd gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTransEff_3                    */		      92,	/* Comment [ This calibration parameter represents transmission efficiency at 3rd gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTransEff_4                    */		      98,	/* Comment [ This calibration parameter represents transmission efficiency at 4th gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTransEff_5                    */		     100,	/* Comment [ This calibration parameter represents transmission efficiency at 5th gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTransEff_R                    */		      80,	/* Comment [ This calibration parameter represents transmission efficiency at reverse gear. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* int16_t     	S16ETCSCpEngSpd_1                     */		    1000,	/* Comment [ This calibration parameter represents the 1st break point of engine speed in engine traction control. This value should be greater than or equal to 0 and less than S16ETCSCpEngSpd_2. ] */
	/* int16_t     	S16ETCSCpEngSpd_2                     */		    2000,	/* Comment [ This calibration parameter represents the 2nd break point of engine speed in engine traction control. This value should be greater than S16ETCSCpEngSpd_1 and less than S16ETCSCpEngSpd_3. ] */
	/* int16_t     	S16ETCSCpEngSpd_3                     */		    3000,	/* Comment [ This calibration parameter represents the 3rd break point of engine speed in engine traction control. This value should be greater than S16ETCSCpEngSpd_2 and less than S16ETCSCpEngSpd_4. ] */
	/* int16_t     	S16ETCSCpEngSpd_4                     */		    4000,	/* Comment [ This calibration parameter represents the 4th break point of engine speed in engine traction control. This value should be greater than S16ETCSCpEngSpd_3 and less than S16ETCSCpEngSpd_5. ] */
	/* int16_t     	S16ETCSCpEngSpd_5                     */		    5000,	/* Comment [ This calibration parameter represents the 5th break point of engine speed in engine traction control. This value should be greater than S16ETCSCpEngSpd_5 and less than 17000. ] */
	/* uchar8_t    	U8ETCSCpEngTrqFltGainCmdDec           */		      80,	/* Comment [ This calibration parameter represents gain of actual engine torque signal filter when the engine torque is increased. This value should be obtained through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpEngTrqFltGainCmdInc           */		      68,	/* Comment [ This calibration parameter represents gain of actual engine torque signal filter when the engine torque is increased. This value should be obtained through the vehicle test which is designed for it. ] */
	/* uchar8_t    	U8ETCSCpTrqDel_1                      */		       3,	/* Comment [ This calibration parameter represents time delay between actual engine torque signal and real engine shaft torque at the 1st break point of engine speed. ] */
	                                                        		        	/* ScaleVal[   0.06 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTrqDel_2                      */		       2,	/* Comment [ This calibration parameter represents time delay between actual engine torque signal and real engine shaft torque at the 2nd break point of engine speed. ] */
	                                                        		        	/* ScaleVal[   0.04 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTrqDel_3                      */		       1,	/* Comment [ This calibration parameter represents time delay between actual engine torque signal and real engine shaft torque at the 3rd break point of engine speed. ] */
	                                                        		        	/* ScaleVal[   0.02 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTrqDel_4                      */		       0,	/* Comment [ This calibration parameter represents time delay between actual engine torque signal and real engine shaft torque at the 4th break point of engine speed. ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTrqDel_5                      */		       0,	/* Comment [ This calibration parameter represents time delay between actual engine torque signal and real engine shaft torque at the 5th break point of engine speed. ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* int16_t     	S16ETCSCpSpd_1                        */		       0,	/* Comment [ This calibration parameter represents the 1st break point of vehicle speed in engine traction control. This value should be greater than or equal to 0 and less than S16ETCSCpSpd_2. ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16ETCSCpSpd_2                        */		      24,	/* Comment [ This calibration parameter represents the 2nd break point of vehicle speed in engine traction control. This value should be greater than S16ETCSCpSpd_1 and less than S16ETCSCpSpd_3. ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16ETCSCpSpd_3                        */		      80,	/* Comment [ This calibration parameter represents the 3rd break point of vehicle speed in engine traction control. This value should be greater than S16ETCSCpSpd_2 and less than S16ETCSCpSpd_4. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16ETCSCpSpd_4                        */		     240,	/* Comment [ This calibration parameter represents the 4th break point of vehicle speed in engine traction control. This value should be greater than S16ETCSCpSpd_3 and less than S16ETCSCpSpd_5. ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16ETCSCpSpd_5                        */		     640,	/* Comment [ This calibration parameter represents the 5th break point of vehicle speed in engine traction control. This value should be greater than S16ETCSCpSpd_4 and less than 4000. ] */
	                                                        		        	/* ScaleVal[     80 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	U8ETCSCpCtlActTmThInLmtCnrg           */		       4,	/* Comment [ This calibration parameter represents time threshold for control entering while limit cornering. ] */
	                                                        		        	/* ScaleVal[   0.08 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 2.54 ] */
	/* uchar8_t    	U8ETCSCpMinTarSpn                     */		       8,	/* Comment [ This calibration parameter represents minimum target wheel spin. The final target wheel spin can not be less than this value. ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarSpnTransTme                */		       5,	/* Comment [ This calibration parameter represents transition time to change target spin. ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* uchar8_t    	U8ETCSCpTarWhlSpin_1                  */		      48,	/* Comment [ This calibration parameter represents base target wheel spin at the 1st vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarWhlSpin_2                  */		      64,	/* Comment [ This calibration parameter represents base target wheel spin at the 2nd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarWhlSpin_3                  */		      40,	/* Comment [ This calibration parameter represents base target wheel spin at the 3rd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarWhlSpin_4                  */		      40,	/* Comment [ This calibration parameter represents base target wheel spin at the 4th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarWhlSpin_5                  */		      32,	/* Comment [ This calibration parameter represents base target wheel spin at the 5th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpAddTarWhlSpnSp                */		      40,	/* Comment [ This calibration parameter represents target wheel spin increasing amount when split-mu is detected. When split-mu is detected, base value of target wheel spin for engine control will be changed to half of brake traction control allowing wheel spin difference between left and right. And this parameter value will be added on the base target wheel spin to provide enough torque. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarTransCplBrkTrq2Sp          */		      55,	/* Comment [ This calibration parameter represents split-mu detection complete brake torque threshold. Detection of split-mu is based on brake torque which is exerted on low-mu wheel by brake traction control. To achieve maximum traction performance on split-mu, target wheel spin and control gain will be changed gradually depend on the amount of brake torque. ] */
	                                                        		        	/* ScaleVal[     550 Nm ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpTarTransStBrkTrq2Sp           */		      30,	/* Comment [ This calibration parameter represents split-mu detection starting brake torque threshold. Detection of split-mu is based on brake torque which is exerted on low-mu wheel by brake traction control. To achieve maximum traction performance on split-mu, target wheel spin and control gain will be changed gradually depend on the amount of brake torque. ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16ETCSCpCircumferenceOfTire          */		    2053,	/* Comment [ This calibration parameter represents circumference of the tire. This parameter is used to calculate corresponding target wheel spin for target engine speed when engine stall is detected. This value can be obtained from vehicle specification or through the vehicle test which is designed for it. ] */
	/* int16_t     	S16ETCSCpTrgtEngSpdStlDetd            */		    1000,	/* Comment [ This calibration parameter represents target engine speed when engine stall is detected. The target engine speed is converted to target wheel spin in the software and target wheel spin will be replaced to this value when engine stall is detected. Spin increasing value when the deep snow detection counter is greater than or equals to U8ETCSCpDepSnwHilCnt_2. ] */
	/* uchar8_t    	U8ETCSCpAlwAddTar4DepSnwSpd           */		      30,	/* Comment [ This calibration parameter represents target wheel spin increase allowing vehicle speed threshold when deep snow is detected. If the vehicle speed is less than or equals to this value, target wheel spin increase due to deep snow detection will be allowed. This value should be less than U8ETCSCpNAlwAddTar4DepSnwSpd. ] */
	/* uchar8_t    	U8ETCSCpNAlwAddTar4DepSnwSpd          */		      40,	/* Comment [ This calibration parameter represents target wheel spin increase not allowing vehicle speed threshold when deep snow is detected. If the vehicle speed is greater than or equals to this value, target wheel spin increase due to deep snow detection will not be allowed. This value should be greater than U8ETCSCpAlwAddTar4DepSnwSpd. ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4AbnmSizTire         */		      40,	/* Comment [ This calibration parameter represents target wheel spin increasing value when abnormal size of tire is detected. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4AdHiRd              */		      16,	/* Comment [ This calibration parameter represents increase target spin in adaptive high-mu detection. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 30 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4DepSnwHil           */		      40,	/* Comment [ This calibration parameter represents target wheel spin increasing value when the deep snow detection counter is greater than or equals to U8ETCSCpDepSnwHilCnt_2. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4DrvPrtnMd           */		      16,	/* Comment [ This calibration parameter represents increase target spin in drive line protection mode. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 30 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4Hill                */		      32,	/* Comment [ This calibration parameter represents target wheel spin increasing value when hill is detected. ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4PreHiRd             */		       8,	/* Comment [ This calibration parameter represents increase target spin in previous high-mu detection. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 30 ] */
	/* uchar8_t    	U8ETCSCpTarSpnAdd4Sp                  */		       8,	/* Comment [ This calibration parameter represents increase target spin in split-mu detection. ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 30 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecAsp_1             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on asphalt at the 1th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecAsp_2             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on asphalt at the 2nd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecAsp_3             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on asphalt at the 3rd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecAsp_4             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on asphalt at the 4th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecAsp_5             */		     -24,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on asphalt at the 5th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecIce_1             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on ice at the 1th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecIce_2             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on ice at the 2nd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecIce_3             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on ice at the 3rd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecIce_4             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on ice at the 4th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecIce_5             */		     -24,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on ice at the 5th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecSnw_1             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on snow at the 1th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecSnw_2             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on snow at the 2nd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecSnw_3             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on snow at the 3rd vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecSnw_4             */		     -32,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on snow at the 4th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpFnlTarSpnDecSnw_5             */		     -24,	/* Comment [ This calibration parameter represents target wheel spin decrease amount on snow at the 5th vehicle speed break point. ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 0 ] */
	/* char8_t     	S8ETCSCpTarSpnDec4Vib                 */		     -32,	/* Comment [ This calibration parameter represents decrease target engine speed when wheel vibration is detected. ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -15 <= X <= 15 ] */
	/* uchar8_t    	U8ETCSCpDltYawOffset4MinTrgtSpn       */		      30,	/* Comment [ This calibration parameter represents offset of delta yaw rate which decreased target spin to minimum. If decrease of target spin is started at 2deg/s of delta yaw rate absolute value, the target spin is decreased to minimum when absolute value of delta yaw rate reaches to 2deg/s + U8ETCSCpDltYawOffset4MinTrgtSpn. ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpAyAsp                         */		      70,	/* Comment [ This calibration parameter represents typical lateral vehicle acceleration on dry asphalt. This value should be measured on dry asphalt. ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAyIce                         */		       5,	/* Comment [ This calibration parameter represents typical lateral vehicle acceleration on very slippery ice. This value should be measured on very slippery ice. ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAySnw                         */		      40,	/* Comment [ This calibration parameter represents typical lateral vehicle acceleration on packed snow. This value should be measured on packed snow. ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpCnt4Tar1stIncInTrn            */		       6,	/* Comment [ This calibration parameter represents the time which takes to increase target wheel spin to 1st step and the amount of restored value at the same time. It will take time which is defined in U8ETCSCpCnt4TarMaxDecInTrn to restore target wheel spin completely. The decreased target wheel spin will not be recovered completely by driver's steering intention. To achieve full recovery of target wheel spin, lateral acceleration should be decreased pretty close to zero. ] */
	                                                        		        	/* ScaleVal[   0.12 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpCnt4TarMaxDecInTrn            */		      12,	/* Comment [ This calibration parameter represents the time which takes to decrease target wheel spin to minimum. So target wheel spin changes gradually. ] */
	                                                        		        	/* ScaleVal[   0.24 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpCrnExDctCnt                   */		      12,	/* Comment [ This calibration parameter represents cornering exit detection counter threshold. If the steering converge counter is greater than or equals to this value, target wheel spin decrease due to cornering is not allowed. That means driver shows an intention to restore the steering wheel to zero for certain period of time which is defined in this parameter, so engine traction controller give back the target wheel spin to normal to help exit the corner quickly by recovering engine torque. ] */
	                                                        		        	/* ScaleVal[   0.24 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpDltYawNVldSpd                 */		      10,	/* Comment [ This calibration parameter represents vehicle speed threshold which is delta yaw rate is not valid. If vehicle speed is less than this value, delta yaw information is not valid any more. This value should be less than U8ETCSCpDltYawVldSpd. ] */
	                                                        		        	/* ScaleVal[   1.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpDltYawStbDctCnt               */		      25,	/* Comment [ This calibration parameter represents delta yaw rate stable detection threshold. If the delta yaw rate is staying in stable region for certain period of time which is defined in this parameter, this can be a criteria to restore target wheel spin to normal. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpDltYawVldSpd                  */		      15,	/* Comment [ This calibration parameter represents vehicle speed threshold which is delta yaw rate is valid. If vehicle speed is greater than this value, delta yaw information is completely valid. This value should be greater than U8ETCSCpDltYawNVldSpd. ] */
	                                                        		        	/* ScaleVal[  1.875 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpDltYawVldVyHSpd               */		      70,	/* Comment [ This calibration parameter represents high vehicle speed threshold which is delta yaw rate is valid. If vehicle speed is greater than this value, delta yaw information is completely valid. This value should be greater than U8ETCSCpDltYawVldSpd. ] */
	                                                        		        	/* ScaleVal[   8.75 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpTarNChngDltYawHiBrkTrq        */		      50,	/* Comment [ This calibration parameter represents dead band of delta yaw which is not allowing target wheel spin decrease. If brake torque on low mu wheel is greater than U8ETCSCpTarTransCplBrkTrq2Sp*10, this parameter value will be the dead band of delta yaw rate. This value should be greater than U8ETCSCpTarNChngDltYawLwBrkTrq. ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpTarNChngDltYawHiSpd           */		      50,	/* Comment [ This calibration parameter represents dead band of delta yaw which is not allowing target wheel spin decrease. If vehicle speed is greater than U8ETCSCpDltYawVldSpd, this parameter value will be the dead band of delta yaw rate. This value should be less than U8ETCSCpTarNChngDltYawLwSpd. ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpTarNChngDltYawLwBrkTrq        */		      20,	/* Comment [ This calibration parameter represents dead band of delta yaw which is not allowing target wheel spin decrease. If brake torque on low mu wheel is less than U8ETCSCpTarTransStBrkTrq2Sp*10, this parameter value will be the dead band of delta yaw rate. This value should be less than U8ETCSCpTarNChngDltYawHiBrkTrq. ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpTarNChngDltYawLwSpd           */		     150,	/* Comment [ This calibration parameter represents dead band of delta yaw which is not allowing target wheel spin decrease. If vehicle speed is less than U8ETCSCpDltYawNVldSpd, this parameter value will be the dead band of delta yaw rate. This value should be greater than U8ETCSCpTarNChngDltYawHiSpd. ] */
	                                                        		        	/* ScaleVal[   15 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpTarNChngDltYawVyHSpd          */		      40,	/* Comment [ This calibration parameter represents dead band of delta yaw at high speed which is allowing target wheel spin decrease. If vehicle speed is greater than U8ETCSCpDltYawVldVyHSpd, this parameter value will be the dead band of delta yaw rate. This value should be less than U8ETCSCpTarNChngDltYawHiSpd. ] */
	                                                        		        	/* ScaleVal[    4 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8ETCSCpLdTrqByGrdTransTme            */		      25,	/* Comment [ This calibration parameter represents transition time of load torque due to gradient. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpLdTrqChngFac                  */		     100,	/* Comment [ This calibration parameter represents factor to modify load torque due to brake traction control. Normally this value is set to 100, but it can be slightly changed if there is big modeling error in brake torque calculation model of brake traction control. ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlTh_1                 */		      80,	/* Comment [ This calibration parameter represents maximum threshold to start symmetric brake control at vehicle speed break point1. If the acceleration of driven axle is less than U8ETCSCpLwAxlAccTh, this parameter value will be the threshold to start symmetric brake control. but if the acceleration of driven axle is greater than U8ETCSCpHiAxlAccTh, the threshold can be reduced to target wheel spin. This value should be greater than target wheel spin at vehicle speed break point1. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlTh_2                 */		      80,	/* Comment [ This calibration parameter represents maximum threshold to start symmetric brake control at vehicle speed break point2. If the acceleration of driven axle is less than U8ETCSCpLwAxlAccTh, this parameter value will be the threshold to start symmetric brake control. but if the acceleration of driven axle is greater than U8ETCSCpHiAxlAccTh, the threshold can be reduced to target wheel spin. This value should be greater than target wheel spin at vehicle speed break point2. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlTh_3                 */		      80,	/* Comment [ This calibration parameter represents maximum threshold to start symmetric brake control at vehicle speed break point3. If the acceleration of driven axle is less than U8ETCSCpLwAxlAccTh, this parameter value will be the threshold to start symmetric brake control. but if the acceleration of driven axle is greater than U8ETCSCpHiAxlAccTh, the threshold can be reduced to target wheel spin. This value should be greater than target wheel spin at vehicle speed break point3. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlTh_4                 */		      80,	/* Comment [ This calibration parameter represents maximum threshold to start symmetric brake control at vehicle speed break point4. If the acceleration of driven axle is less than U8ETCSCpLwAxlAccTh, this parameter value will be the threshold to start symmetric brake control. but if the acceleration of driven axle is greater than U8ETCSCpHiAxlAccTh, the threshold can be reduced to target wheel spin. This value should be greater than target wheel spin at vehicle speed break point4. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlTh_5                 */		      80,	/* Comment [ This calibration parameter represents maximum threshold to start symmetric brake control at vehicle speed break point5. If the acceleration of driven axle is less than U8ETCSCpLwAxlAccTh, this parameter value will be the threshold to start symmetric brake control. but if the acceleration of driven axle is greater than U8ETCSCpHiAxlAccTh, the threshold can be reduced to target wheel spin. This value should be greater than target wheel spin at vehicle speed break point5. ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* char8_t     	S8ETCSCpCtlExitCntDnTh                */		     -10,	/* Comment [ This calibration parameter represents threshold for counting down the control exit counter. If the value of (engine torque command from traction control - driver requested engine torque) is less than this parameter, the control exit counter will be increased. This value should be greater than or equal to 0 and less than S8ETCSCpCtlExitCntUpTh. ] */
	                                                        		        	/* ScaleVal[      -1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12 <= X <= 12 ] */
	/* char8_t     	S8ETCSCpCtlExitCntTh                  */		      25,	/* Comment [ This calibration parameter represents threshold for exiting control. If the control exit counter is greater than or equal to this parameter, engine traction control will be terminated. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 2.54 ] */
	/* char8_t     	S8ETCSCpCtlExitCntUpTh                */		       0,	/* Comment [ This calibration parameter represents threshold for counting up the control exit counter. If the value of (engine torque command from traction control - driver requested engine torque) is greater than this parameter, the control exit counter will be increased. This value should be greater than S8ETCSCpCtlExitCntDnTh. ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12 <= X <= 12 ] */
	/* uchar8_t    	U8ETCSCpCtlActCntThAtHiAxlAcc         */		       1,	/* Comment [ This calibration parameter represents threshold for starting control when the acceleration of driven axle is high level. If the counter for starting control is greater than or equal to this value, control will be started. This value should be greater than 0 and less than U8ETCSCpCtlActCntThAtLwAxlAcc. ] */
	                                                        		        	/* ScaleVal[   0.02 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpCtlActCntThAtLwAxlAcc         */		       4,	/* Comment [ This calibration parameter represents threshold for starting control when the acceleration of driven axle is low level. If the counter for starting control is greater than or equal to this value, control will be started. This value should be greater than U8ETCSCpCtlActCntThAtHiAxlAcc. ] */
	                                                        		        	/* ScaleVal[   0.08 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpCtlEnterAccPdlTh              */		       3,	/* Comment [ This calibration parameter represents accelerator pedal threshold to allow starting engine traction control. If the accelerator pedal position is greater than or equals to this value, engine traction control is ready to start and waiting to meet wheel spin condition. This value should be greater than U8ETCSCpCtlExitAccPdlTh. ] */
	/* uchar8_t    	U8ETCSCpCtlExitAccPdlTh               */		       1,	/* Comment [ This calibration parameter represents accelerator pedal threshold to exit engine traction control. If the accelerator pedal position is less than or equals to this value, engine traction control will be terminated. This value should be less than U8ETCSCpCtlEnterAccPdlTh. ] */
	/* uchar8_t    	U8ETCSCpHiAxlAccTh                    */		     200,	/* Comment [ This calibration parameter represents threshold to decide high level of driven axle acceleration. If the acceleration of the driven axle is greater than this value, it means the acceleration is high. In that case, threshold of the control starting counter will be decreased and it takes shorter time to start control. This value should be greater than U8ETCSCpLwAxlAccTh. ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpLwAxlAccTh                    */		      80,	/* Comment [ This calibration parameter represents threshold to decide low level of driven axle acceleration. If the acceleration of the driven axle is less than this value, it means the acceleration is low. In that case, threshold of the control starting counter will be increased and it takes longer time to start control.This value should be greater than or equal to 0 and less than U8ETCSCpHiAxlAccTh. ] */
	                                                        		        	/* ScaleVal[      0.8 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16ETCSCpEngStallRpm                  */		     500,	/* Comment [ This calibration parameter represents engine stall detection engine speed when longitudinal acceleration sensor is not available. ] */
	/* int16_t     	S16ETCSCpHighRiskStallRpmFlat         */		     500,	/* Comment [ This calibration parameter represents engine stall detection engine speed on flat at high risk of engine stall. ] */
	/* int16_t     	S16ETCSCpHighRiskStallRpmHill         */		     700,	/* Comment [ This calibration parameter represents engine stall detection engine speed on hill at high risk of engine stall. ] */
	/* int16_t     	S16ETCSCpLowRiskStallRpmFlat          */		     500,	/* Comment [ This calibration parameter represents engine stall detection engine speed on flat at low risk of engine stall. ] */
	/* int16_t     	S16ETCSCpLowRiskStallRpmHill          */		     700,	/* Comment [ This calibration parameter represents engine stall detection engine speed on hill at low risk of engine stall. ] */
	/* int16_t     	S16ETCSCpMidRiskStallRpmFlat          */		     500,	/* Comment [ This calibration parameter represents engine stall detection engine speed on flat at medium risk of engine stall. ] */
	/* int16_t     	S16ETCSCpMidRiskStallRpmHill          */		     700,	/* Comment [ This calibration parameter represents engine stall detection engine speed on hill at medium risk of engine stall. ] */
	/* uchar8_t    	U8ETCSCpEngAvail                      */		       1,	/* Comment [ This calibration parameter represents availability of engine. ] */
	/* uchar8_t    	U8ETCSCpEngStallRpmOffset             */		     250,	/* Comment [ This calibration parameter represents offset of hysteresis engine speed to exit stall. ] */
	/* uchar8_t    	U8ETCSCpLwWhlSpnTh2ClrCyl1st          */		      20,	/* Comment [ This calibration parameter represents threshold for resetting the 1st cycle detection based upon low wheel spin counter. If the value of low wheel spin counter is greater or equals to this parameter, detection of 1st cycle will be reset. ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpTimeTh2ClrCyl1st              */		     200,	/* Comment [ This calibration parameter represents maximum time of maintaining 1st cycle detection. The software will check how long the 1st cycle detection is holding and reset the detection if elapsed time is greater than or equals to the value of parameter. ] */
	                                                        		        	/* ScaleVal[      4 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAspSpd_1            */		    3000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on asphlat at speed range 1. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAspSpd_2            */		    3000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on asphlat at speed range 2. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAspSpd_3            */		    3000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on asphlat at speed range 3. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAspSpd_4            */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on asphlat at speed range 4. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAspSpd_5            */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on asphlat at speed range 5. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIceSpd_1            */		    1000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on ice at speed range 1. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIceSpd_2            */		    1000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on ice at speed range 2. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIceSpd_3            */		    1000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on ice at speed range 3. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIceSpd_4            */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on ice at speed range 4. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIceSpd_5            */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on ice at speed range 5. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnowSpd_1           */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on snow at speed range 1. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnowSpd_2           */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on snow at speed range 2. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnowSpd_3           */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on snow at speed range 3. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnowSpd_4           */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on snow at speed range 4. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnowSpd_5           */		    2000,	/* Comment [ This calibration parameter represents reference cardan shaft torque on snow at speed range 5. ] */
	/* uchar8_t    	U8ETCSCpFtr4JmpDnMax                  */		      50,	/* Comment [ This calibration parameter represents maximum jump down factor which is applied when the reference torque level is completely valid.(lcetcss16RefTrqSetTm<=U8ETCSCpRefTrqVldTm). ] */
	/* uchar8_t    	U8ETCSCpFtr4JmpDnMin                  */		       1,	/* Comment [ This calibration parameter represents minimum jump down factor which is applied when the reference torque level is not valid anymore.(lcetcss16RefTrqSetTm>=U8ETCSCpRefTrqNvldTm). ] */
	/* uchar8_t    	U8ETCSCpFtr4JmpUpMax                  */		      50,	/* Comment [ This calibration parameter represents maximum jump up factor which is applied when the reference torque level is completely valid.(lcetcss16RefTrqSetTm<=U8ETCSCpRefTrqVldTm). ] */
	/* uchar8_t    	U8ETCSCpFtr4JmpUpMin                  */		       1,	/* Comment [ This calibration parameter represents minimum jump up factor which is applied when the reference torque level is not valid anymore.(lcetcss16RefTrqSetTm>=U8ETCSCpRefTrqNvldTm). ] */
	/* uchar8_t    	U8ETCSCpFtr4RefTrqAt2ndCylStrt        */		      90,	/* Comment [ This calibration parameter represents factor which is multiplied to estimated torque level during 1st cycle phase. At the start of 2nd cycle, the I control portion will be updated based on this parameter and available road friction information at the moment is pretty reliable. ] */
	/* uchar8_t    	U8ETCSCpRecTrqFacInBump               */		      90,	/* Comment [ This calibration parameter represents factor which is multiplied to estimated torque level during bump. ] */
	/* uchar8_t    	U8ETCSCpRefTrqNvldTm                  */		      75,	/* Comment [ This calibration parameter represents invalid time of reference torque level for I control portion jump up and down. The reference torque level will be set whenever control error crosses the zero and the reliability of this signal will be deteriorated as time passes. The reference torque level is regarded as completely invalid if the time passes the value which is defined in the calibration parameter.  In this case, I control portion update strategy will not work. ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpRefTrqVldTm                   */		      25,	/* Comment [ This calibration parameter represents valid time of reference torque level for I control portion jump up and down. The reference torque level will be set whenever control error crosses the zero and the reliability of this signal will be deteriorated as time passes. The reference torque level is regarded as completely valid for the time which is defined in the calibration parameter. In this case, I control portion update strategy will work completely. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpLowWhlSpnCntDnTh              */		      12,	/* Comment [ This calibration parameter represents threshold for counting down the low spin detection counter. If the value of wheel spin is greater than this parameter, the low spin detection counter will be decreased. This value should be greater than U8ETCSCpLowWhlSpnCntUpTh. ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpLowWhlSpnCntUpTh              */		       4,	/* Comment [ This calibration parameter represents threshold for counting up the low spin detection counter. If the value of wheel spin is less than this parameter, the low spin detection counter will be increased. This value should be greater than 0 and less than U8ETCSCpLowWhlSpnCntDnTh. ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpDetDepSnwHilCnt               */		     100,	/* Comment [ This calibration parameter represents counter threshold for deep snow detection. ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpHysDepSnwHilCnt               */		      50,	/* Comment [ This calibration parameter represents counter hysteresis threshold for deep snow exit. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* int16_t     	S16ETCSCpSplitHillClrEngSpd           */		    3500,	/* Comment [ This calibration parameter represents engine speed threshold to exit hill . ] */
	/* int16_t     	S16ETCSCpSplitHillDctEngSpd           */		    3000,	/* Comment [ This calibration parameter represents engine speed threshold to detect hill. ] */
	/* uchar8_t    	U8ETCSCpHillClrTime                   */		      10,	/* Comment [ This calibration parameter represents count threshold to exit hill. ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpHillDctAlongTh                */		       8,	/* Comment [ This calibration parameter represents longitudinal sensor value threshold to detect hill. ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpHillDctTime                   */		      25,	/* Comment [ This calibration parameter represents count threshold for start hill detection. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpHillDctTimeAx                 */		      25,	/* Comment [ This calibration parameter represents count threshold for longitudinal sensor effect. ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpHillDiffAx                    */		      30,	/* Comment [ This calibration parameter represents changed rate of longitudinal sensor value. ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpHsaHillTime                   */		     100,	/* Comment [ This calibration parameter represents count threshold for complete hill detection. ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpSplitHillClrAccel             */		      15,	/* Comment [ This calibration parameter represents longitudinal acceleration threshold to exit hill. ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpSplitHillClrVehSpd            */		     102,	/* Comment [ This calibration parameter represents vehicle speed threshold to exit hill. ] */
	                                                        		        	/* ScaleVal[  12.75 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpSplitHillDctAccel             */		      10,	/* Comment [ This calibration parameter represents longitudinal acceleration threshold to detect hill. ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpSplitHillDctMinAx             */		       8,	/* Comment [ This calibration parameter represents minimum longitudinal sensor threshold to detect hill. ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpSplitHillDctVehSpd            */		      40,	/* Comment [ This calibration parameter represents vehicle speed threshold to detect hill. ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16ETCSCpIGainIncFacH2L               */		     800,	/* Comment [ This calibration parameter represents I control gain increase factor which will be applied when high-to-low mu transition is detected. If high-to-low mu transition is detected, that means road friction is changed. so operating point of controller should be quickly changed to appropriate level for low-mu. To do this, I gain should be increased remarkably during detection of high-to-low mu transition stays on. This value should be greater than 100 and less than 32767. ] */
	/* char8_t     	S8ETCSCpH2LDctDltTh                   */		     -10,	/* Comment [ This calibration parameter represents required vehicle acceleration difference between high-mu and low-mu to detect high-to-low mu transition. The vehicle acceleration difference can be calculated as (acceleration on low-mu) - (acceleration on high-mu) and the value of this calibration parameter should be negative. ] */
	                                                        		        	/* ScaleVal[     -0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8ETCSCpH2LDctHiMuTh                  */		      20,	/* Comment [ This calibration parameter represents high mu detecting vehicle acceleration threshold for high-to-low mu transition detection. This value should be greater than low mu detection threshold(=S8ETCSCpH2LDctLwMuTh) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8ETCSCpH2LDctLwMuTh                  */		      10,	/* Comment [ This calibration parameter represents low mu detecting vehicle acceleration threshold for high-to-low mu transition detection. This value should be less than high mu detection threshold(=S8ETCSCpH2LDctHiMuTh) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8ETCSCpH2LTrnsTime                   */		      15,	/* Comment [ This calibration parameter represents transition time of longitudinal acceleration signal when the surface is changed from high-mu to low-mu. Usually it takes around 0.3sec but depends on inertia of wheel, tire type and road friction difference between high-mu and low-mu. This value should be greater than 1(=0.02sec) and less than 25(=0.5sec). ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 0.5 ] */
	/* uchar8_t    	U8ETCSCpH2LDctHldTm                   */		      50,	/* Comment [ This calibration parameter represents the maximum time which the high-to-low mu transition detection stays on. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpH2LDctSpnTh                   */		      16,	/* Comment [ This calibration parameter represents required wheel spin to detect high-to-low mu transition. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8ETCSCpPGainIncFacH2L                */		     200,	/* Comment [ This calibration parameter represents P control gain increase factor which will be applied when high-to-low mu transition is detected. This value should be greater than 100 and less than 255. ] */
	/* int16_t     	S16ETCSCpLow2HighDecel                */		    -250,	/* Comment [ This calibration parameter represents deceleration of wheel speed from low-mu to high-mu. ] */
	                                                        		        	/* ScaleVal[     -2.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -10 <= X <= 0 ] */
	/* int16_t     	S16ETCSCpPreHighRdDctCplCnt           */		     155,	/* Comment [ This calibration parameter represents time which takes to previous high-mu detection maximum count. ] */
	                                                        		        	/* ScaleVal[    3.1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16ETCSCpPreHighRdDctStrtCnt          */		     150,	/* Comment [ This calibration parameter represents time which takes to previous high-mu detection minimum count. ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16ETCSCpStrtCdrnTrqAtAsphalt         */		    2400,	/* Comment [ This calibration parameter represents typical minimum cardan shaft torque value on dry asphalt. This value should be measured on dry asphalt. ] */
	/* uchar8_t    	U8ETCSCpAdaptHighRdCplCnt             */		      15,	/* Comment [ This calibration parameter represents time which takes to adaptive high-mu detection maximum count. ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* uchar8_t    	U8ETCSCpAdaptHighRdStrtCnt            */		      10,	/* Comment [ This calibration parameter represents time which takes to adaptive high-mu detection minimum count. ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* uchar8_t    	U8ETCSCpAreaSetLwToHiSpin             */		      24,	/* Comment [ This calibration parameter represents spin value from low-mu to high-mu. ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.25 ] */
	/* uchar8_t    	U8ETCSCpLwToHiDetAreaCnt              */		      50,	/* Comment [ This calibration parameter represent count threshold for detection by spin area from low to high-mu. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* uchar8_t    	U8ETCSCpLwToHiDetCnt                  */		      50,	/* Comment [ This calibration parameter represent count threshold for detection by wheel speed decel from low to high-mu. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5 ] */
	/* uchar8_t    	U8ETCSCpAWDVehicle                    */		       0,	/* Comment [ This signal represents AWD Vehicle or 2WD Vehicle ] */
	/* uchar8_t    	U8ETCSCpFtr4RefTrqAtVcaOfCtl          */		     100,	/* Comment [ This calibration parameter represents factor which is multiplied to estimated torque level during VCA control ] */
	/* uchar8_t    	U8ETCSCpHysDeltAcc                    */		       3,	/* Comment [ This signal represents difference of longitudinal acceleration calculated by the acceleration sensor and vehicle speed Threshold longitudinal acceleration(calculated by vehicle speed ? calculated by longitudinal sensor)for VCA exit ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 0.3 ] */
	/* uchar8_t    	U8ETCSCpStDeltAcc                     */		       5,	/* Comment [ This signal represents difference of longitudinal acceleration calculated by the acceleration sensor and vehicle speed Threshold longitudinal acceleration(calculated by vehicle speed ? calculated by longitudinal sensor)for VCA detection. ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 0.3 ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtAsphalt             */		    2700,	/* Comment [ This calibration parameter represents typical cardan shaft torque value on dry asphalt. This value should be measured on dry asphalt. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtIce                 */		     650,	/* Comment [ This calibration parameter represents typical cardan shaft torque value on very slippery ice. This value should be measured on very slippery ice. ] */
	/* int16_t     	S16ETCSCpCdrnTrqAtSnow                */		    1700,	/* Comment [ This calibration parameter represents typical cardan shaft torque value on packed snow This value should be measured on packed snow. ] */
	/* uchar8_t    	U8ETCSCpAxAtAsphalt                   */		      35,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on dry asphalt. This value should be measured on dry asphalt. ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxAtIce                       */		       4,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on very slippery ice. This value should be measured on very slippery ice. ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxAtSnow                      */		      20,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on packed snow. This value should be measured on packed snow. ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear2AtAsphalt              */		      35,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on dry asphalt at 2nd gear. ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear2AtIce                  */		       4,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on very slippery ice at 2nd gear. ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear2AtSnow                 */		      20,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on packed snow at 2nd gear. ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear3AtAsphalt              */		      20,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on dry asphalt at 3rd gear. ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear3AtIce                  */		       4,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on very slippery ice at 3rd gear. ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear3AtSnow                 */		      15,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on packed snow at 3rd gear. ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear4AtAsphalt              */		      15,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on dry asphalt at 4th gear. ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear4AtIce                  */		       4,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on very slippery ice at 4th gear. ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear4AtSnow                 */		      12,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on packed snow at 4th gear. ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear5AtAsphalt              */		      15,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on dry asphalt at 5th gear. ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear5AtIce                  */		       4,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on very slippery ice at 5th gear. ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpAxGear5AtSnow                 */		      12,	/* Comment [ This calibration parameter represents typical longitudinal vehicle acceleration on packed snow at 5th gear. ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8ETCSCpPgTransTmDrvVib               */		      50,	/* Comment [ This calibration parameter represents p gain factor transition time while driveline vibration is detected. ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 2.54 ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainAsp_1              */		      70,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 1st gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainAsp_2              */		      20,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 2nd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainAsp_3              */		      10,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 3rd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainAsp_4              */		      10,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 4th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainAsp_5              */		      10,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 5th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainIce_1              */		      90,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 1st gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainIce_2              */		      60,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 2nd gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainIce_3              */		      45,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 3rd gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainIce_4              */		      40,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 4th gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainIce_5              */		      30,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 5th gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSnw_1              */		      80,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 1st gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSnw_2              */		      40,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 2nd gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSnw_3              */		      30,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 3rd gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSnw_4              */		      25,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 4th gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSnw_5              */		      20,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 5th gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainAsp_1              */		     150,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 1st gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainAsp_2              */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 2nd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainAsp_3              */		      50,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 3rd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainAsp_4              */		      50,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 4th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainAsp_5              */		      50,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 5th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainIce_1              */		      70,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 1st gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainIce_2              */		      45,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 2nd gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainIce_3              */		      35,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 3rd gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainIce_4              */		      25,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 4th gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainIce_5              */		      15,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 5th gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSnw_1              */		      90,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 1st gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSnw_2              */		      60,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 2nd gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSnw_3              */		      40,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 3rd gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSnw_4              */		      30,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 4th gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSnw_5              */		      20,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 5th gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSplt_1             */		      70,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 1st gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSplt_2             */		      70,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 2nd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSplt_3             */		      60,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 3rd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSplt_4             */		      60,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 4th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrPgainSplt_5             */		      60,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is negative with 5th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSplt_1             */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 1st gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSplt_2             */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 2nd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSplt_3             */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 3rd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSplt_4             */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 4th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrPgainSplt_5             */		     100,	/* Comment [ This calibration parameter represents P gain value which is applied to the P controller when the control error is positive with 5th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpGainTrnsTmAftrGearShft        */		      20,	/* Comment [ This calibration parameter represents control gain transition time after complete of gear shift. While gear changing, the control gain will be modified to cope with special event but when it is completed the control gain should be back to normal. The gain change will be done through this time period which is defined in this parameter. ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8ETCSCpPgFacGrChgErrNeg_1            */		     180,	/* Comment [ This calibration parameter represents P gain modification factor while gear is changing from 1st to 2nd.  This factor will be applied to the base P gain when the control error is negative. Normally this value is greater than 100 to quickly suppress the wheel spin while gear is changing. ] */
	/* uchar8_t    	U8ETCSCpPgFacGrChgErrNeg_2            */		     150,	/* Comment [ This calibration parameter represents P gain modification factor while gear is changing from 2nd to 3rd.  This factor will be applied to the base P gain when the control error is negative. Normally this value is greater than 100 to quickly suppress the wheel spin while gear is changing. ] */
	/* uchar8_t    	U8ETCSCpPgFacGrChgErrNeg_3            */		     140,	/* Comment [ This calibration parameter represents P gain modification factor while gear is changing from 3rd to 4th.  This factor will be applied to the base P gain when the control error is negative. Normally this value is greater than 100 to quickly suppress the wheel spin while gear is changing. ] */
	/* uchar8_t    	U8ETCSCpPgFacGrChgErrNeg_4            */		     100,	/* Comment [ This calibration parameter represents P gain modification factor while gear is changing from 4th to 5th. This factor will be applied to the base P gain when the control error is negative. Normally this value is greater than 100 to quickly suppress the wheel spin while gear is changing. ] */
	/* uchar8_t    	U8ETCSCpPgFacGrChgErrNeg_5            */		      80,	/* Comment [ This calibration parameter represents P gain modification factor while gear is changing from 5th to 6th.  This factor will be applied to the base P gain when the control error is negative. Normally this value is greater than 100 to quickly suppress the wheel spin while gear is changing. ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnAsp_1               */		      15,	/* Comment [ This calibration parameter represents P gain factor while cornering on asphalt when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnAsp_2               */		      15,	/* Comment [ This calibration parameter represents P gain factor while cornering on asphalt when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnAsp_3               */		      15,	/* Comment [ This calibration parameter represents P gain factor while cornering on asphalt when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnAsp_4               */		      15,	/* Comment [ This calibration parameter represents P gain factor while cornering on asphalt when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnAsp_5               */		      15,	/* Comment [ This calibration parameter represents P gain factor while cornering on asphalt when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnIce_1               */		      30,	/* Comment [ This calibration parameter represents P gain factor while cornering on ice when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnIce_2               */		      30,	/* Comment [ This calibration parameter represents P gain factor while cornering on ice when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnIce_3               */		      30,	/* Comment [ This calibration parameter represents P gain factor while cornering on ice when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnIce_4               */		      30,	/* Comment [ This calibration parameter represents P gain factor while cornering on ice when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnIce_5               */		      30,	/* Comment [ This calibration parameter represents P gain factor while cornering on ice when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnSnw_1               */		      20,	/* Comment [ This calibration parameter represents P gain factor while cornering on snow when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnSnw_2               */		      20,	/* Comment [ This calibration parameter represents P gain factor while cornering on snow when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnSnw_3               */		      20,	/* Comment [ This calibration parameter represents P gain factor while cornering on snow when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnSnw_4               */		      20,	/* Comment [ This calibration parameter represents P gain factor while cornering on snow when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpPgFacInTrnSnw_5               */		      20,	/* Comment [ This calibration parameter represents P gain factor while cornering on snow when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainAsp_1              */		       7,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 1st gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainAsp_2              */		       5,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 2nd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainAsp_3              */		       3,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 3rd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainAsp_4              */		       3,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 4th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainAsp_5              */		       3,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 5th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainIce_1              */		      25,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 1st gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainIce_2              */		      20,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 2nd gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainIce_3              */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 3rd gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainIce_4              */		       5,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 4th gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainIce_5              */		       5,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 5th gear on ice. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSnw_1              */		      20,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 1st gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSnw_2              */		      15,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 2nd gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSnw_3              */		       7,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 3rd gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSnw_4              */		       4,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 4th gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSnw_5              */		       4,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 5th gear on snow. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainAsp_1              */		     100,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 1st gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainAsp_2              */		     100,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 2nd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainAsp_3              */		      50,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 3rd gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainAsp_4              */		      50,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 4th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainAsp_5              */		      50,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 5th gear on dry asphalt. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainIce_1              */		      30,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 1st gear on ice.  The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainIce_2              */		      20,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 2nd gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainIce_3              */		      12,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 3rd gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainIce_4              */		      12,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 4th gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainIce_5              */		      12,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 5th gear on ice. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSnw_1              */		      35,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 1st gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSnw_2              */		      25,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 2nd gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSnw_3              */		      17,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 3rd gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSnw_4              */		      15,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 4th gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSnw_5              */		      15,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 5th gear on snow. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSplt_1             */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 1st gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSplt_2             */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 2nd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSplt_3             */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 3rd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSplt_4             */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 4th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpNegErrIgainSplt_5             */		      10,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is negative with 5th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so negative control error means wheel spin is greater than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSplt_1             */		      70,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 1st gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSplt_2             */		      70,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 2nd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSplt_3             */		      70,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 3rd gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSplt_4             */		      70,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 4th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpPosErrIgainSplt_5             */		      70,	/* Comment [ This calibration parameter represents I gain value which is applied to the I controller when the control error is positive with 5th gear on split-mu. The control error is defined as (target wheel spin - wheel spin), so positive control error means wheel spin is less than target wheel spin. ] */
	/* uchar8_t    	U8ETCSCpIgFacGrChgErrNeg_1            */		      30,	/* Comment [ This calibration parameter represents I gain modification factor while gear is changing from 1st to 2nd. This factor will be applied to the base I gain when the control error is negative. Normally this value is close to 0 because I control portion shouldn't be changed much while gear is changing. If the road friction is not changed, I control portion should remain as same. ] */
	/* uchar8_t    	U8ETCSCpIgFacGrChgErrNeg_2            */		      30,	/* Comment [ This calibration parameter represents I gain modification factor while gear is changing from 2nd to 3rd. This factor will be applied to the base I gain when the control error is negative. Normally this value is close to 0 because I control portion shouldn't be changed much while gear is changing. If the road friction is not changed, I control portion should remain as same. ] */
	/* uchar8_t    	U8ETCSCpIgFacGrChgErrNeg_3            */		      30,	/* Comment [ This calibration parameter represents I gain modification factor while gear is changing from 3rd to 4th. This factor will be applied to the base I gain when the control error is negative. Normally this value is close to 0 because I control portion shouldn't be changed much while gear is changing. If the road friction is not changed, I control portion should remain as same. ] */
	/* uchar8_t    	U8ETCSCpIgFacGrChgErrNeg_4            */		      30,	/* Comment [ This calibration parameter represents I gain modification factor while gear is changing from 4th to 5th. This factor will be applied to the base I gain when the control error is negative. Normally this value is close to 0 because I control portion shouldn't be changed much while gear is changing. If the road friction is not changed, I control portion should remain as same. ] */
	/* uchar8_t    	U8ETCSCpIgFacGrChgErrNeg_5            */		      30,	/* Comment [ This calibration parameter represents I gain modification factor while gear is changing from 5th to 6th. This factor will be applied to the base I gain when the control error is negative. Normally this value is close to 0 because I control portion shouldn't be changed much while gear is changing. If the road friction is not changed, I control portion should remain as same. ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnAsp_1               */		      30,	/* Comment [ This calibration parameter represents I gain factor while cornering on asphalt when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnAsp_2               */		      30,	/* Comment [ This calibration parameter represents I gain factor while cornering on asphalt when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnAsp_3               */		      30,	/* Comment [ This calibration parameter represents I gain factor while cornering on asphalt when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnAsp_4               */		      30,	/* Comment [ This calibration parameter represents I gain factor while cornering on asphalt when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnAsp_5               */		      30,	/* Comment [ This calibration parameter represents I gain factor while cornering on asphalt when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnIce_1               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on ice when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnIce_2               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on ice when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnIce_3               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on ice when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnIce_4               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on ice when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnIce_5               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on ice when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnSnw_1               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on snow when the gear is 1st. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnSnw_2               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on snow when the gear is 2nd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnSnw_3               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on snow when the gear is 3rd. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnSnw_4               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on snow when the gear is 4th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgFacInTrnSnw_5               */		      70,	/* Comment [ This calibration parameter represents I gain factor while cornering on snow when the gear is 5th. Normally this value is greater than or equals to 100. ] */
	                                                        		        	/* ScaleVal[      700 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnAsp_1          */		      50,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      500 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnAsp_2          */		      50,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      500 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnAsp_3          */		      50,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      500 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnAsp_4          */		      50,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      500 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnAsp_5          */		      50,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      500 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnIce_1          */		      30,	/* Comment [ This calibration parameter represents I gain increase factor after turn on ice. ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnIce_2          */		      30,	/* Comment [ This calibration parameter represents I gain increase factor after turn on ice. ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnIce_3          */		      30,	/* Comment [ This calibration parameter represents I gain increase factor after turn on ice. ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnIce_4          */		      30,	/* Comment [ This calibration parameter represents I gain increase factor after turn on ice. ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnIce_5          */		      30,	/* Comment [ This calibration parameter represents I gain increase factor after turn on ice. ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnSnow_1         */		      40,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      400 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnSnow_2         */		      40,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      400 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnSnow_3         */		      40,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      400 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnSnow_4         */		      40,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      400 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpIgainFacAftrTrnSnow_5         */		      40,	/* Comment [ This calibration parameter represents I gain increase factor after turn on snow. ] */
	                                                        		        	/* ScaleVal[      400 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8ETCSCpBigSpnMaxIgFac                */		     130,	/* Comment [ This calibration parameter represents I gain factor when spin error is over than -7KPH. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainAsp_1          */		      15,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 1 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainAsp_2          */		      15,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 2 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainAsp_3          */		      15,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 3 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainAsp_4          */		      15,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 4 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainAsp_5          */		      15,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 5 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainIce_1          */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 1 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainIce_2          */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 2 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainIce_3          */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 3 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainIce_4          */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 4 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainIce_5          */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 5 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainSnw_1          */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 1 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainSnw_2          */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 2 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainSnw_3          */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 3 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainSnw_4          */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 4 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawDivrgGainSnw_5          */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 5 and it is applied when delta yaw rate is getting bigger. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainAsp_1         */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainAsp_2         */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainAsp_3         */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainAsp_4         */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainAsp_5         */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainIce_1         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainIce_2         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainIce_3         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainIce_4         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainIce_5         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainSnw_1         */		      40,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainSnw_2         */		      40,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainSnw_3         */		      40,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainSnw_4         */		      40,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawFstIncGainSnw_5         */		      40,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainAsp_1         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainAsp_2         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainAsp_3         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainAsp_4         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainAsp_5         */		      30,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainIce_1         */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainIce_2         */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainIce_3         */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainIce_4         */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainIce_5         */		      20,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainSnw_1         */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 1 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainSnw_2         */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 2 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainSnw_3         */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 3 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainSnw_4         */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 4 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* uchar8_t    	U8ETCSCpDelYawSlwIncGainSnw_5         */		      25,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow at vehicle speed break point 5 and it is applied when delta yaw rate is getting smaller and driver doesn't decreases steering angle. ] */
	/* int16_t     	S16ETCSCpLmtCrngTarTrqAsp             */		    1000,	/* Comment [ This calibration parameter represents target cardan shaft torque level on asphalt. If the driver decreases steering angle while limit cornering and the current cardan shaft torque is smaller than this value, cardan shaft torque will be increased to this value rapidly by U8ETCSCpDelYawLowTrqGainAsp. ] */
	/* int16_t     	S16ETCSCpLmtCrngTarTrqIce             */		     700,	/* Comment [ This calibration parameter represents target cardan shaft torque level on ice. If the driver decreases steering angle while limit cornering and the current cardan shaft torque is smaller than this value, cardan shaft torque will be increased to this value rapidly by U8ETCSCpDelYawLowTrqGainIce. ] */
	/* int16_t     	S16ETCSCpLmtCrngTarTrqSnw             */		     800,	/* Comment [ This calibration parameter represents target cardan shaft torque level on snow. If the driver decreases steering angle while limit cornering and the current cardan shaft torque is smaller than this value, cardan shaft torque will be increased to this value rapidly by U8ETCSCpDelYawSnwTrqGainSnw. ] */
	/* uchar8_t    	U8ETCSCpDelYawLowTrqGainAsp           */		     100,	/* Comment [ This calibration parameter represents delta yaw controller gain on asphalt which is applied when current cardan shaft torque is smaller than target cardan shaft torque. ] */
	/* uchar8_t    	U8ETCSCpDelYawLowTrqGainIce           */		      50,	/* Comment [ This calibration parameter represents delta yaw controller gain on ice which is applied when current cardan shaft torque is smaller than target cardan shaft torque. ] */
	/* uchar8_t    	U8ETCSCpDelYawLowTrqGainSnw           */		      70,	/* Comment [ This calibration parameter represents delta yaw controller gain on snow which is applied when current cardan shaft torque is smaller than target cardan shaft torque. ] */
	/* uchar8_t    	U8ETCSCpSymBrkCtlIhbAxTh              */		      20,	/* Comment [ This calibration parameter represents vehicle acceleration threshold to inhibit symmetric brake control. If vehicle acceleration is greater than this value, symmetric brake control will be inhibited. That means symmetric brake control is not needed if the road friction is high enough. Normally this parameter value is chosen slightly higher than maximum vehicle acceleration on snow surface. ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqDecRate              */		      15,	/* Comment [ This calibration parameter represents brake torque decrease rate when doing symmetric brake control. This value should be greater than 0 and less than 255. ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqMax_1                */		     100,	/* Comment [ This calibration parameter represents maximum brake torque on one wheel when doing symmetric brake control with 1st gear. Total load torque will be doubled of this parameter value because the brake torque will be applied to both wheel. This value should be greater than 0 and less than 255. ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqMax_2                */		      80,	/* Comment [ This calibration parameter represents maximum brake torque on one wheel when doing symmetric brake control with 2nd gear. Total load torque will be doubled of this parameter value because the brake torque will be applied to both wheel. This value should be greater than 0 and less than 255. ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqMax_3                */		      60,	/* Comment [ This calibration parameter represents maximum brake torque on one wheel when doing symmetric brake control with 3rd gear. Total load torque will be doubled of this parameter value because the brake torque will be applied to both wheel. This value should be greater than 0 and less than 255. ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqMax_4                */		      50,	/* Comment [ This calibration parameter represents maximum brake torque on one wheel when doing symmetric brake control with 4th gear. Total load torque will be doubled of this parameter value because the brake torque will be applied to both wheel. This value should be greater than 0 and less than 255. ] */
	/* uchar8_t    	U8ETCSCpSymBrkTrqMax_5                */		      40,	/* Comment [ This calibration parameter represents maximum brake torque on one wheel when doing symmetric brake control with 5th gear. Total load torque will be doubled of this parameter value because the brake torque will be applied to both wheel. This value should be greater than 0 and less than 255. ] */
	/* int16_t     	S16ETCSCpMinCrdnCmdTrq                */		       0,	/* Comment [ This calibration parameter represents minimum cardan torque command. ] */
	                                                        		        	/* ScaleVal[       0 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16ETCSCpOffsetTrq4FunLamp            */		     400,	/* Comment [ This signal represents offSet torque for function lamp off. ] */
	/* uchar8_t    	U8ETCSCpFunLampOffCnt                 */		       5,	/* Comment [ This signal represents function lamp off count in engine traction control. ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8TMCCpTurnIndex_1                    */		     100,	/* Comment [ This signal represents turn index of p gain turn low factor to changed reference vehicle speed for target up-shift in turn. ] */
	/* uchar8_t    	U8TMCCpTurnIndex_2                    */		     120,	/* Comment [ This signal represents turn index of p gain turn mid factor to changed reference vehicle speed for target up-shift in turn. ] */
	/* uchar8_t    	U8TMCCpTurnIndex_3                    */		     150,	/* Comment [ This signal represents turn index of p gain turn high factor to changed reference vehicle speed for target up-shift in turn. ] */
	/* uchar8_t    	U8TMCpAllowDnShftMinVehSpd_1          */		       5,	/* Comment [ This signal represents minimum vehicle speed to gear down shift at 1st gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftMinVehSpd_2          */		      10,	/* Comment [ This signal represents minimum vehicle speed to gear down shift at 2nd gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftMinVehSpd_3          */		      30,	/* Comment [ This signal represents minimum vehicle speed to gear down shift at 3rd gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftMinVehSpd_4          */		      50,	/* Comment [ This signal represents minimum vehicle speed to gear down shift at 4th gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftMinVehSpd_5          */		      70,	/* Comment [ This signal represents minimum vehicle speed to gear down shift at 5th gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftRPM_1                */		      15,	/* Comment [ This signal represents target engine speed to gear shift down at 1st gear. ] */
	                                                        		        	/* ScaleVal[   1500 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowDnShftRPM_2                */		      17,	/* Comment [ This signal represents target engine speed to gear shift down at 2nd gear. ] */
	                                                        		        	/* ScaleVal[   1700 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowDnShftRPM_3                */		      20,	/* Comment [ This signal represents target engine speed to gear shift down at 3rd gear. ] */
	                                                        		        	/* ScaleVal[   2000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowDnShftRPM_4                */		      20,	/* Comment [ This signal represents target engine speed to gear shift down at 4th gear. ] */
	                                                        		        	/* ScaleVal[   2000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowDnShftRPM_5                */		      20,	/* Comment [ This signal represents target engine speed to gear shift down at 5th gear. ] */
	                                                        		        	/* ScaleVal[   2000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowDnShftVehSpd_1             */		      10,	/* Comment [ This signal represents target vehicle speed to gear shift down at 1st gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftVehSpd_2             */		      30,	/* Comment [ This signal represents target vehicle speed to gear shift down at 2nd gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftVehSpd_3             */		      50,	/* Comment [ This signal represents target vehicle speed to gear shift down at 3rd gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftVehSpd_4             */		      70,	/* Comment [ This signal represents target vehicle speed to gear shift down at 4th gear. ] */
	/* uchar8_t    	U8TMCpAllowDnShftVehSpd_5             */		      90,	/* Comment [ This signal represents target vehicle speed to gear shift down at 5th gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftMaxVehSpd_1          */		      40,	/* Comment [ This signal represents maximum vehicle speed to gear up shift at 1st gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftMaxVehSpd_2          */		      60,	/* Comment [ This signal represents maximum vehicle speed to gear up shift at 2nd gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftMaxVehSpd_3          */		      80,	/* Comment [ This signal represents maximum vehicle speed to gear up shift at 3rd gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftMaxVehSpd_4          */		     100,	/* Comment [ This signal represents maximum vehicle speed to gear up shift at 4th gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftMaxVehSpd_5          */		     120,	/* Comment [ This signal represents maximum vehicle speed to gear up shift at 5th gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftRPM_1                */		      32,	/* Comment [ This signal represents target engine speed to gear shift up at 1st gear. ] */
	                                                        		        	/* ScaleVal[   3200 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowUpShftRPM_2                */		      32,	/* Comment [ This signal represents target engine speed to gear shift up at 2nd gear. ] */
	                                                        		        	/* ScaleVal[   3200 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowUpShftRPM_3                */		      30,	/* Comment [ This signal represents target engine speed to gear shift up at 3rd gear. ] */
	                                                        		        	/* ScaleVal[   3000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowUpShftRPM_4                */		      30,	/* Comment [ This signal represents target engine speed to gear shift up at 4th gear. ] */
	                                                        		        	/* ScaleVal[   3000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowUpShftRPM_5                */		      30,	/* Comment [ This signal represents target engine speed to gear shift up at 5th gear. ] */
	                                                        		        	/* ScaleVal[   3000 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpAllowUpShftVehSpd_1             */		      20,	/* Comment [ This signal represents target vehicle speed to gear shift up at 1st gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftVehSpd_2             */		      40,	/* Comment [ This signal represents target vehicle speed to gear shift up at 2nd gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftVehSpd_3             */		      60,	/* Comment [ This signal represents target vehicle speed to gear shift up at 3rd gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftVehSpd_4             */		      80,	/* Comment [ This signal represents target vehicle speed to gear shift up at 4th gear. ] */
	/* uchar8_t    	U8TMCpAllowUpShftVehSpd_5             */		     100,	/* Comment [ This signal represents target vehicle speed to gear shift up at 5th gear. ] */
	/* uchar8_t    	U8TMCpGearShiftTime                   */		       5,	/* Comment [ This signal represents time after request gear shift. ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.02 ]  Range   [ 0 <= X <= 5.1 ] */
	/* uchar8_t    	U8TMCpMaxGear                         */		       6,	/* Comment [ This signal represents maximum drive gear level of transimission. ] */
	/* uchar8_t    	U8TMCpMinGear                         */		       1,	/* Comment [ This signal represents minimum drive gear level of transimission. ] */
	/* uchar8_t    	U8TMCpShftChgRPMHys                   */		       1,	/* Comment [ This signal represents hysteresis value of target engine speed to gear shift. ] */
	                                                        		        	/* ScaleVal[    100 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 8000 ] */
	/* uchar8_t    	U8TMCpShftChgRPMInTrn_1               */		       0,	/* Comment [ This signal represents decrease target engine speed to gear shift in turn index low. ] */
	                                                        		        	/* ScaleVal[      0 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 1000 ] */
	/* uchar8_t    	U8TMCpShftChgRPMInTrn_2               */		       0,	/* Comment [ This signal represents decrease target engine speed to gear shift in turn index mid. ] */
	                                                        		        	/* ScaleVal[      0 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 1000 ] */
	/* uchar8_t    	U8TMCpShftChgRPMInTrn_3               */		       0,	/* Comment [ This signal represents decrease target engine speed to gear shift in turn index high. ] */
	                                                        		        	/* ScaleVal[      0 RPM ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 1000 ] */
	/* uchar8_t    	U8TMCpShftChgVehSpdHys                */		       3,	/* Comment [ This signal represents hysteresis value of target vehicle speed to gear shift. ] */
	/* uchar8_t    	U8TMCpShftChgVehSpdInHill             */		       5,	/* Comment [ This signal represents increase target vehicle speed to gear shift up at hill. ] */
	/* uchar8_t    	U8TMCpShftChgVehSpdInTrn_1            */		       0,	/* Comment [ This signal represents decrease target vehicle speed to gear shift in turn index low. ] */
	/* uchar8_t    	U8TMCpShftChgVehSpdInTrn_2            */		       0,	/* Comment [ This signal represents decrease target vehicle speed to gear shift in turn index mid. ] */
	/* uchar8_t    	U8TMCpShftChgVehSpdInTrn_3            */		       0,	/* Comment [ This signal represents decrease target vehicle speed to gear shift in turn index high. ] */
	/* int16_t     	S16_HM_EMS                            */		     600,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 레벨 ] */
	/* int16_t     	S16_LM_EMS                            */		     250,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 레벨 ] */
	/* int16_t     	S16_MHM_EMS                           */		     450,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-High-m 레벨 ] */
	/* int16_t     	S16_MM_EMS                            */		     300,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_HSP               */		    1100,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_LSP               */		     600,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_MSP               */		     800,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_HSP               */		    1000,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 고속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_LSP               */		     400,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 저속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_MSP               */		     700,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 중속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_HSP               */		     800,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_LSP               */		     300,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_MSP               */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 레벨 ] */
	/* uchar8_t    	U8_EEC_ENTER_CNT                      */		      21,	/* Comment [ ESP 엔진제어 진입을 위한 Threshold 유보 Count ] */
	/* int16_t     	S16_HM_HSP_TH_GAIN                    */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_LSP_TH_GAIN                    */		     120,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_MSP_TH_GAIN                    */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_SLIP_TH_GAIN                   */		      10,	/* Comment [ ESP 엔진제어에서 사용하는 High-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_HSP_TH_GAIN                    */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_LSP_TH_GAIN                    */		     120,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_MSP_TH_GAIN                    */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_SLIP_TH_GAIN                   */		       8,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_HSP_TH_GAIN                    */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_LSP_TH_GAIN                    */		     120,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_MSP_TH_GAIN                    */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_SLIP_TH_GAIN                   */		       8,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_HSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 고속 Threshold 게인 추가 레벨(EEC Gain = S16_HSP_TH_GAIN_ADD_TVBB + S16_XM_HSP_TH_GAIN) ] */
	/* int16_t     	S16_LSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 저속 Threshold 게인 추가 레벨(EEC Gain = S16_LSP_TH_GAIN_ADD_TVBB + S16_XM_LSP_TH_GAIN) ] */
	/* int16_t     	S16_MSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 중속 Threshold 게인 추가 레벨(EEC Gain = S16_MSP_TH_GAIN_ADD_TVBB + S16_XM_MSP_TH_GAIN) ] */
	/* int16_t     	S16_HM_HSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_LSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_MSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_HSP_TH_GAIN_IN_OVER            */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_LSP_TH_GAIN_IN_OVER            */		     120,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_MSP_TH_GAIN_IN_OVER            */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_HSP_TH_GAIN_IN_OVER            */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_LSP_TH_GAIN_IN_OVER            */		     120,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_MSP_TH_GAIN_IN_OVER            */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_USP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVER_PGAIN             */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVSTB_PGAIN            */		       3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_STB_DGAIN              */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_STB_PGAIN              */		      30,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNDER_DGAIN            */		     250,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNSTB_DGAIN            */		     250,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNSTB_PGAIN            */		     -12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVER_PGAIN             */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVSTB_PGAIN            */		       3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_STB_DGAIN              */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_STB_PGAIN              */		      30,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNDER_DGAIN            */		     250,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNDER_PGAIN            */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNSTB_DGAIN            */		     250,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNSTB_PGAIN            */		     -15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVER_PGAIN             */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVSTB_PGAIN            */		       3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_STB_DGAIN              */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_STB_PGAIN              */		      30,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNDER_DGAIN            */		     300,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNDER_PGAIN            */		      20,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNSTB_DGAIN            */		     300,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNSTB_PGAIN            */		     -20,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVER_PGAIN             */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVSTB_PGAIN            */		       3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_STB_DGAIN              */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_STB_PGAIN              */		      30,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNDER_DGAIN            */		     200,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNDER_PGAIN            */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNSTB_DGAIN            */		     200,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNSTB_PGAIN            */		     -15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVER_DGAIN             */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVSTB_DGAIN            */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNDER_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNDER_PGAIN            */		      15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNSTB_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNSTB_PGAIN            */		     -15,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVER_DGAIN             */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVSTB_DGAIN            */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNDER_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNSTB_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNSTB_PGAIN            */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNDER_DGAIN            */		     100,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNSTB_DGAIN            */		     100,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNSTB_PGAIN            */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVER_PGAIN             */		      -5,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVSTB_PGAIN            */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNDER_DGAIN            */		      50,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNDER_PGAIN            */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNSTB_DGAIN            */		      50,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNSTB_PGAIN            */		      -5,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVER_DGAIN             */		    -250,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVER_PGAIN             */		     -20,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVSTB_DGAIN            */		    -250,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVSTB_PGAIN            */		      20,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNDER_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNSTB_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNSTB_PGAIN            */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVER_DGAIN             */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVSTB_DGAIN            */		    -200,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNDER_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNSTB_DGAIN            */		     150,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNSTB_PGAIN            */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNDER_DGAIN            */		     100,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNSTB_DGAIN            */		     100,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNSTB_PGAIN            */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVER_PGAIN             */		     -10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVSTB_DGAIN            */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVSTB_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_STB_PGAIN              */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNDER_DGAIN            */		      50,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNDER_PGAIN            */		       5,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNSTB_DGAIN            */		      50,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNSTB_PGAIN            */		      -5,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_EEC_HM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.6G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_LM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.2G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_MM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.4G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_TORQ_LIMIT_MAX                */		      50,	/* Comment [ EEC Torque Down Rate Maximum Limitation Final Value ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_HSP      */		      15,	/* Comment [ 고속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_LSP      */		      15,	/* Comment [ 저속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_MSP      */		      15,	/* Comment [ 중속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* uint16_t    	U16_EEC_HM_TARGET_GAIN_1ST_CYC        */		     350,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 High-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_LM_TARGET_GAIN_1ST_CYC        */		     180,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Low-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_MM_TARGET_GAIN_1ST_CYC        */		     210,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Med-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_TARGET_TORQ_1ST_MAX           */		     700,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Max. Torque Level ] */
	/* uint16_t    	U16_EEC_TARGET_TORQ_1ST_MIN           */		     300,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Min. Torque Level ] */
	/* uchar8_t    	U8_EEC_1ST_CONTROL                    */		       0,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_EEC_START_DRIVE_TORQUE             */		      50,	/* Comment [ Minimum drive torque level to detect ESP engine control ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_HM              */		       5,	/* Comment [ Yaw 안정화시 High-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_LM              */		      10,	/* Comment [ Yaw 안정화시 Low-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_MM              */		       7,	/* Comment [ Yaw 안정화시 Mid-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uchar8_t    	U8_EEC_FADE_OUT_CONTROL               */		       1,	/* Comment [ EEC Fade Out 제어 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_HM           */		       9,	/* Comment [ EEC Fade Out 제어 시 High-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.9 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_LM           */		       3,	/* Comment [ EEC Fade Out 제어 시 Low-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_MM           */		       6,	/* Comment [ EEC Fade Out 제어 시 Med-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.6 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_EEC_UTURN_COMP_DYAW_THR1          */		   -2000,	/* Comment [ EEC Uturn 보정 제어 시 보정을 수행하는 Min. Yaw Rate 레벨 설정 ] */
	                                                        		        	/* ScaleVal[  -20 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EEC_UTURN_COMP_DYAW_THR2          */		   -3000,	/* Comment [ EEC Uturn 보정 제어 시 최대 보정을 수행하는 Max. Yaw Rate 레벨 설정 ] */
	                                                        		        	/* ScaleVal[  -30 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_EEC_UTURN_COMPENSATE               */		       0,	/* Comment [ EEC Uturn 보정 제어 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_TOD_MTP_THR                        */		       0,	/* Comment [ Full Acceleration 상황을 나타내는 MTP Threshold. ] */
	/* uchar8_t    	U8_TOD_OVER_THR_G1                    */		       0,	/* Comment [ Oversteer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_OVER_THR_G2                    */		       0,	/* Comment [ Severe Oversteer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_UNDER_THR_G1                   */		       0,	/* Comment [ Understeer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_UNDER_THR_G2                   */		       0,	/* Comment [ Severe Understeer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uint16_t    	S16_TOD_MAX_TORQ                      */		       0,	/* Comment [ TOD에서 사용하는 Max. Torque 레벨을 설정함 ] */
	/* uint16_t    	S16_TOD_MIN_TORQ                      */		       0,	/* Comment [ TOD에서 사용하는 Min. Torque 레벨을 설정함 ] */
	/* int16_t     	S16_TOD_TORQ_CST_OVER1                */		       0,	/* Comment [ Coast 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_OVER2                */		       0,	/* Comment [ Coast 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_STABL                */		       0,	/* Comment [ Coast 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_UNDER1               */		       0,	/* Comment [ Coast 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_UNDER2               */		       0,	/* Comment [ Coast 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_OVER1                */		       0,	/* Comment [ Half Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_OVER2                */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_STABL                */		       0,	/* Comment [ Half Open Throttle 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_UNDER1               */		       0,	/* Comment [ Half Open Throttle 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_UNDER2               */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_OVER1                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_OVER2                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_STABL                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_UNDER1               */		       0,	/* Comment [ Wild Open Throttle 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_UNDER2               */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_OVER1                 */		       0,	/* Comment [ Coast 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_OVER2                 */		       0,	/* Comment [ Coast 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_STABL                 */		       0,	/* Comment [ Coast 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_UNDER1                */		       0,	/* Comment [ Coast 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_UNDER2                */		       0,	/* Comment [ Coast 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_OVER1                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_OVER2                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_STABL                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_UNDER1                */		       0,	/* Comment [ Half Open Throttle 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_UNDER2                */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_OVER1                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_OVER2                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_STABL                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_UNDER1                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_UNDER2                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* int16_t     	S16_HSA_a_uphill_detection_along      */		       5,	/* Comment [ Hill grade thres for HSA (default 0.08g) ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_b_MidLongG                    */		      18,	/* Comment [ Midium Hill grade for HSA (default 0.18g) ] */
	                                                        		        	/* ScaleVal[     0.18 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_c_MaxLongG                    */		      35,	/* Comment [ Max Hill grade for HSA (default 0.35g) ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_d_Flat_by_LongG_AVG           */		       2,	/* Comment [ LongG Average thres for Flat Detection (default 0.04g) ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_PressRatioAtMidHill           */		      10,	/* Comment [ ratio of enter pressure at mid hill ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_HSA_STOP_ON_HILL_MIN_SPD          */		       4,	/* Comment [ HSA Determine Stop on hill speed thres. (default : 0.5kph) ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_LongGSafetyFactor             */		       0,	/* Comment [ Safety factor for ax offset (0.01g ~ 0.05g) ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_PressToLongGRatio             */		      15,	/* Comment [ The ratio of Assist Pressure to Hill grade (1.5bar/0.01g ~ 2.5bar/0.01g) ] */
	                                                        		        	/* ScaleVal[ 1.5 bar/0.01g ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_MAX_MP_SLOP                   */		      50,	/* Comment [ HSA Max. Mpress slope(default : 5bar/2scan) ] */
	                                                        		        	/* ScaleVal[ 5 bar/2scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_MIN_MP_SLOP                   */		      10,	/* Comment [ HSA Min. Mpress slope(default : 1bar/2scan) ] */
	                                                        		        	/* ScaleVal[ 1 bar/2scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_VV_ACT_COMP_MAX_MP_SLP        */		     150,	/* Comment [ HSA Valve Acting Mpress at Max. Mpress slope(default : 15bar) ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_VV_ACT_COMP_MIN_MP_SLP        */		     100,	/* Comment [ HSA Valve Acting Mpress at Min. Mpress slope(default : 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_HSA_holding_time_on_Accel         */		     200,	/* Comment [ HSA Max hold time on Accel pedal (default : 1.5sec) ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_holding_time_out              */		     200,	/* Comment [ Maximum Assist time of HSA (default 1.5sec) ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* int16_t     	S16_HSA_a_EngTorqPerLongG             */		       7,	/* Comment [ The ratio of Engine torque to Hill grade for engine ] */
	                                                        		        	/* ScaleVal[ 0.7 %/0.01g ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_b_LongGSFTorqComp             */		       0,	/* Comment [ Engine torque to compensate ax safety factor for engine ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_DEFAULT_ENG_IDLE_TORQ         */		     100,	/* Comment [ HSA Default Engine Idle Torque (default : 10%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_HSA_c_ReleaseTim_4_Accel_1_AT     */		      10,	/* Comment [ Time exceeding engine torque thres1 for HSA release for AT  (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_d_ReleaseTim_4_Accel_2_AT     */		      25,	/* Comment [ Time exceeding engine torque thres2 for HSA release for AT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[   0.25 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_e_ReleaseTim_4_Accel_1_MT     */		      60,	/* Comment [ Time exceeding engine torque thres1 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.6 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_f_ReleaseTim_4_Accel_2_MT     */		      40,	/* Comment [ Time exceeding engine torque thres2 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_g_ReleaseTim_4_Accel_3_MT     */		      30,	/* Comment [ Time exceeding engine torque thres3 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_HSA_AT_MIN_MTP_FOR_EXIT            */		       3,	/* Comment [ minimum mtp for A/T HSA exit ] */
	/* uchar8_t    	U8_HSA_DW_HILL_EXIT_TORQ_COMP         */		       0,	/* Comment [ HSA down hill Exit torque compensation (default : 5%) ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_EST_CLUT_ENG_TH_1             */		     160,	/* Comment [ clutch torq thres1 of M/T vehicle for HSA exit ] */
	/* int16_t     	S16_HSA_EST_CLUT_ENG_TH_2             */		     300,	/* Comment [ clutch torq thres2 of M/T vehicle for HSA exit ] */
	/* int16_t     	S16_HSA_EST_CLUT_REL_TORQ_TH          */		      50,	/* Comment [ minimum engine relative torque for clutch engage detection ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_EST_CLUT_RPM_TH               */		    1300,	/* Comment [ minimum engine RPM for clutch engage detection ] */
	/* uchar8_t    	U8_HSA_EST_CLUT_ENG_INDEX             */		      10,	/* Comment [ index of cluth engage by clutch torq thres ] */
	/* uchar8_t    	U8_HSA_EST_CLUT_MTP_TH                */		       5,	/* Comment [ minimum aceel pedal value for clutch engage detection ] */
	/* int16_t     	S16_HSA_AT_FAST_EXIT_ARAD_TH          */		       8,	/* Comment [ min. wheel accel Th. For HSA fast exit A/T (default 2g) ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -8192 <= X <= 8191.75 ] */
	/* int16_t     	S16_HSA_MT_FAST_EXIT_ARAD_TH          */		       8,	/* Comment [ min. wheel accel Th. For HSA fast exit M/T (default 2g) ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -8192 <= X <= 8191.75 ] */
	/* uchar8_t    	U8_HSA_FAST_EXIT_MTP_TH               */		      30,	/* Comment [ min. accel pedal Th. For HSA fast exit (default 30%) ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MAX            */		       8,	/* Comment [ Maximum pressure fade-out rate during time-out release ] */
	                                                        		        	/* ScaleVal[ 0.8 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MAX_ACC        */		      20,	/* Comment [ Maximum pressure fade-out rate after accel pedal apply ] */
	                                                        		        	/* ScaleVal[ 2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MIN            */		       3,	/* Comment [ Minimum pressure fade-out rate during time-out release ] */
	                                                        		        	/* ScaleVal[ 0.3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_G_GAIN          */		       8,	/* Comment [ The ratio of Pressure fade-out rate to Hill grade ] */
	                                                        		        	/* ScaleVal[ 0.08 0.01g/0.1bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_HighMedG           */		      16,	/* Comment [ fade out rate at high-med grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.6 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_LowMedG            */		      18,	/* Comment [ fade out rate at low-med grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.8 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_MaxG               */		      14,	/* Comment [ fade out rate at max grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.4 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_MinG               */		      20,	/* Comment [ fade out rate at min grade with acc. ] */
	                                                        		        	/* ScaleVal[ 2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_HighMedG               */		       4,	/* Comment [ fade out rate at high-med grade ] */
	                                                        		        	/* ScaleVal[ 0.4 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_LowMedG                */		       5,	/* Comment [ fade out rate at low-med grade ] */
	                                                        		        	/* ScaleVal[ 0.5 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_MaxG                   */		       3,	/* Comment [ fade out rate at max grade ] */
	                                                        		        	/* ScaleVal[ 0.3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_MinG                   */		       6,	/* Comment [ fade out rate at min grade ] */
	                                                        		        	/* ScaleVal[ 0.6 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_HighMedLongG                   */		      25,	/* Comment [ high_med grade to decide fade out rate ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_LowMedLongG                    */		      15,	/* Comment [ low_med grade to decide fade out rate ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_PHASEOUT_RATE_RATIO_ACC        */		       0,	/* Comment [ HSA Phase out rate compensation for Accel. exit (default : 50 %) ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_FOR_ROLL        */		       2,	/* Comment [ fade out rate after vehicle moving on HSA releasing ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_NOT_HILL        */		      30,	/* Comment [ fade out rate at improper condition on HSA ] */
	                                                        		        	/* ScaleVal[ 3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_REBRK           */		      30,	/* Comment [ fade out rate at re-brake ] */
	                                                        		        	/* ScaleVal[ 3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_Act_Default_TC_Current        */		     450,	/* Comment [ Min TC current For HSA control ] */
	/* int16_t     	S16_HSA_ENTER_INBIT_MPRESS            */		      50,	/* Comment [ The highest mpress inhibited HSA engagement (Default 5 bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_ENTER_LIMIT_MPRESS            */		      70,	/* Comment [ The lowest mpress allowed HSA engagement (Default 10bar/ MIN 5bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_ENTER_LIMIT_SPEED             */		       2,	/* Comment [ The highest speed allowed HSA engagement (Default 0.25 KPH) ] */
	                                                        		        	/* ScaleVal[  0.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_EXIT_SPEED                    */		      16,	/* Comment [ HSA disengagement speed (Default 2 KPH) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_MAX_MP_OFFSET_BLS_FAIL        */		      80,	/* Comment [ MP compensation at BLS fail (default 8bar) ] */
	                                                        		        	/* ScaleVal[      8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TEMP_1                        */		       0,	/* Comment [ Temp value for HSA ] */
	/* int16_t     	S16_HSA_TEMP_2                        */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_CNTR_P_SCAN                    */		       3,	/* Comment [ HSA Retain pressure control counter during Release state (default : 3scan) ] */
	/* uchar8_t    	U8_HSA_Disable                        */		       0,	/* Comment [ HSA Activation T/P( 0 : Enable / 1 : Disable ) ] */
	/* uchar8_t    	U8_HSA_SET_CLUTCH_ENGAGE_EST          */		       0,	/* Comment [ HSA Setting for Clutch engage estimation ( 0 : Disable / 1 : Enable ) ] */
	/* uchar8_t    	U8_HSA_SET_DECAY_RATE_EACH_SLOP       */		       0,	/* Comment [ HSA Setting for Decay rate each slope ( 0 : Disable / 1 : Enable ) ] */
	/* uchar8_t    	U8_HSA_TEMP_3                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_4                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_5                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_6                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* int16_t     	S16_HSA_CNTR_MIN_TC_CURRENT           */		     150,	/* Comment [ HSA Min. TC Current(default : 150mA) ] */
	/* int16_t     	S16_HSA_INIT_CNTR_CUR_MAX_COMP        */		     300,	/* Comment [ HSA Max. Compensated Current during Initial Hold pattern Control Time (default : 300mA) ] */
	/* int16_t     	S16_HSA_INIT_HOLD_CNTR_T              */		      20,	/* Comment [ HSA Initial Hold pattern Control Time (default : 200ms) ] */
	                                                        		        	/* ScaleVal[     200 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_HSA_ISG_1st_Drop_Cur              */		      50,	/* Comment [ fade out 1st scan TC current drop at ISG(default 50mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MAX               */		     750,	/* Comment [ hold current for Target press 60bar (default 750mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MID               */		     520,	/* Comment [ hold current for Target press 25bar (default 520mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MIN               */		     280,	/* Comment [ hold current for Target press 5bar (default 280mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MAX             */		     400,	/* Comment [ HSA Hold Max. pressure (default : 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MID             */		     250,	/* Comment [ HSA Hold Mid. pressure (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MIN             */		      50,	/* Comment [ HSA Hold Min. pressure (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MAX           */		    -100,	/* Comment [ HSA TC Initial comp. current for Max. pressure (default : -100) ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MID           */		     -50,	/* Comment [ HSA TC Initial comp. current for Mid. pressure (default : -50) ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MIN           */		     100,	/* Comment [ HSA TC Initial comp. current for Min. pressure (default : 100) ] */
	/* int16_t     	S16_ISG_End_RPM_Th                    */		     500,	/* Comment [ RPM thres for ISG exit (default 500RPM) ] */
	/* int16_t     	S16_ISG_ON_MIN_SLOP_HOLD_PRESS        */		     100,	/* Comment [ minimum assist pressure (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_ISG_HSA_ON_AX_TH_GEAR_D            */		      -3,	/* Comment [ grade thres for ISG (default -0.03g) ] */
	                                                        		        	/* ScaleVal[    -0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_ISG_ON_MIN_SLOPE                   */		       3,	/* Comment [ max grade for minimum assist pressure (default 0.03g) ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_RATE_GAIN_FOR_ISG                  */		       5,	/* Comment [ ratio of ISG fade out rate compare with HSA (default 10<same rate with HSA>) ] */
	/* char8_t     	S8_RATE_GAIN_FOR_ISG_ACC              */		       5,	/* Comment [ ratio of ISG fade out rate after accel compared with HSA (default 10<same rate with HSA>) ] */
	/* uchar8_t    	U8_ISG_End_RPM_OK_delay_time          */		       5,	/* Comment [ time delay after RPM thres satisfaction (default 0.05sec) ] */
	                                                        		        	/* ScaleVal[   0.05 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_RISE_ENABLE                    */		       0,	/* Comment [ HSA rise mode enable (0:disable, 1:enable) ] */
	/* int16_t     	S16_HSA_RISE_WL_SPD_TH                */		       2,	/* Comment [ Minimum wheel speed for roll back detection  (default 0.25KPH) ] */
	                                                        		        	/* ScaleVal[   0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HSA_RISE_NUM_OF_WL                 */		       2,	/* Comment [ Number of wheel satisefied roll back detection Th.  (default 2) ] */
	/* int16_t     	S16_HSA_CUR_AT_RISE                   */		     850,	/* Comment [ TC valve current at rise mode (default 850mA) ] */
	/* int16_t     	S16_HSA_MOTOR_ON_TIME                 */		      30,	/* Comment [ motor on-time at rise mode (default 0.3sec@10bar) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_RISE_PRESS                    */		     100,	/* Comment [ rise press at roll back detection (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_MOTOR_TARGET_VOLT              */		      20,	/* Comment [ motor target volt at rise mode (default 2volt) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_HOLD_COMP_P_OVERLOAD          */		     100,	/* Comment [ compensasion of target hold press at vehicle overload (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_DCT_OVERLOAD_RISE_NUM          */		       2,	/* Comment [ Number of roll back rise for vehicle overload detection  (default 2) ] */
	/* int16_t     	S16_HSA_T_P_TEMP_1                    */		       0,	/* Comment [ HSA temporarily T/P 1 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_10                   */		       0,	/* Comment [ HSA temporarily T/P 10 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_2                    */		       0,	/* Comment [ HSA temporarily T/P 2 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_3                    */		       0,	/* Comment [ HSA temporarily T/P 3 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_4                    */		       0,	/* Comment [ HSA temporarily T/P 4 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_5                    */		       0,	/* Comment [ HSA temporarily T/P 5 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_6                    */		       0,	/* Comment [ HSA temporarily T/P 6 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_7                    */		       0,	/* Comment [ HSA temporarily T/P 7 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_8                    */		       0,	/* Comment [ HSA temporarily T/P 8 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_9                    */		       0,	/* Comment [ HSA temporarily T/P 9 ] */
	/* int16_t     	S16_HSA_AX_DIFF_FOR_DIVE_DETECT       */		     100,	/* Comment [ HSA Ax Difference for Dive Detection (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_LOW_REF               */		     100,	/* Comment [ HSA Ax Difference Low Refernce (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_MAX_REF               */		     800,	/* Comment [ HSA Ax Difference Max Refernce (default : 0.08g) ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_MED_REF               */		     600,	/* Comment [ HSA Ax Difference Med Refernce (default : 0.06g) ] */
	                                                        		        	/* ScaleVal[     0.06 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_LOW_DIF        */		      20,	/* Comment [ HSA Ax Avg. Valid Thres Low Difference (default : 200ms) ] */
	                                                        		        	/* ScaleVal[     200 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_MAX_DIF        */		      40,	/* Comment [ HSA Ax Avg. Valid Thres Max Difference (default : 400ms) ] */
	                                                        		        	/* ScaleVal[     400 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_MID_DIF        */		      30,	/* Comment [ HSA Ax Avg. Valid Thres Mid Difference (default : 300ms) ] */
	                                                        		        	/* ScaleVal[     300 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_HSA_PRESS_DEC_RATE_TC_OFF         */		      10,	/* Comment [ HSA Decrease press during TCS interface time (default : 1bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_SPIN_EXIT_SET_TIME             */		      50,	/* Comment [ HSA TCS interface time after HSA Exit by Wheel spin (default : 500ms) ] */
	                                                        		        	/* ScaleVal[     500 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_INDICATION_ON_AX_TH            */		       7,	/* Comment [ HSA status indication threhold slope (default : 0.07 g) ] */
	                                                        		        	/* ScaleVal[     0.07 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_ONHILL_SET_REF_AX_OK_T         */		      25,	/* Comment [ HSA on hill detection time for HSA status indication (default : 250ms) ] */
	                                                        		        	/* ScaleVal[     250 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_AVH_HIGH_MID_SLOP_MIN_H_P         */		     320,	/* Comment [ high-min down 경사에서 정차 가능한 최소 브레이크 답력 (default : 6bar) ] */
	                                                        		        	/* ScaleVal[     32 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_HIGH_MID_SLOP_SAFE_H_P        */		     320,	/* Comment [ high-mid down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 27bar) ] */
	                                                        		        	/* ScaleVal[     32 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_H_M_SLOP_MIN_H_P_UP           */		     250,	/* Comment [ high-min up 경사에서 정차 가능한 최소 브레이크 답력 (default : 7bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_H_M_SLOP_SAFE_H_P_UP          */		     250,	/* Comment [ high-mid up 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_LOW_MID_SLOP_MIN_H_P          */		     250,	/* Comment [ low-mid down 경사에서 정차 가능한 최소 브레이크 답력 (default : 5bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_LOW_MID_SLOP_SAFE_H_P         */		     250,	/* Comment [ low-mid down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_L_M_SLOP_MIN_H_P_UP           */		     200,	/* Comment [ low-mid up 경사에서 정차 가능한 최소 브레이크 답력 (default : 6bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_L_M_SLOP_SAFE_H_P_UP          */		     200,	/* Comment [ low-mid up 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_MIN_H_P              */		     350,	/* Comment [ max down 경사에서 제어 정차 가능한 최소 브레이크 답력  (default : 7bar) ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_MIN_H_P_UP           */		     330,	/* Comment [ max up 경사에서 제어 정차 가능한 최소 브레이크 답력  (default : 10bar) ] */
	                                                        		        	/* ScaleVal[     33 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_SAFE_H_P             */		     350,	/* Comment [ max down 경사에서 밀림 발생없는 충분한 브레이크 압력  (default : 35bar) ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_SAFE_H_P_UP          */		     330,	/* Comment [ max up 경사에서 밀림 발생없는 충분한 브레이크 압력  (default : 35bar) ] */
	                                                        		        	/* ScaleVal[     33 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_MIN_H_P              */		     100,	/* Comment [ min down 경사에서 정차 가능한 최소 브레이크 답력 (default : 5bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_MIN_H_P_MT           */		     100,	/* Comment [ Minimum MCP for M/T AVH(default : 3bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_SAFE_H_P             */		     100,	/* Comment [ min down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 13bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_HIGH_MID_SLOP                  */		      17,	/* Comment [ high-mid 기준 경사 (default : 17%) ] */
	/* uchar8_t    	U8_AVH_LOW_MID_SLOP                   */		      10,	/* Comment [ low-mid 기준 경사 (default : 10%) ] */
	/* uchar8_t    	U8_AVH_MAX_SLOP                       */		      25,	/* Comment [ max 기준 경사 (default : 25%) ] */
	/* uchar8_t    	U8_AVH_MIN_SLOP                       */		       3,	/* Comment [ min 기준 경사 (default : 3%) ] */
	/* int16_t     	S16_AVH_MAX_MP_SLOP                   */		     100,	/* Comment [ valve 제어 진입 시점 결정을 위한 MCP 감소율 최대값 (default : 10bar/0.02s) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_MP_SLOP                   */		      10,	/* Comment [ valve 제어 진입 시점 결정을 위한 MCP 감소율 최소값 (default : 1bar/0.02s) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_VV_ACT_COMP_MAX_MP_SLP        */		     180,	/* Comment [ S16_AVH_MAX_MP_SLOP 이상 영역의 valve 제어 진입 시점 MCP 보상값 (default : 18bar, 타겟압력+18bar) ] */
	                                                        		        	/* ScaleVal[     18 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_VV_ACT_COMP_MIN_MP_SLP        */		      30,	/* Comment [ S16_AVH_MIN_MP_SLOP 이하 영역의 valve 제어 진입 시점 MCP 보상값 (default : 3bar, 타겟압력+3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1              */		      10,	/* Comment [ level 1 해제 모드 진입 엔진 토크(APS>0%, Idle 토크 대비 증가량) (default : 1%) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1_LV2          */		      10,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>5%, Idle 토크 대비 증가량) (default : 20%) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1_LV3          */		      50,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>10%, Idle 토크 대비 증가량) (default : 35%) ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV2_M1          */		       3,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV3_M1          */		       4,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_LOW_MID_SLOP       */		     700,	/* Comment [ low-mid경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MAX_SLOP           */		     700,	/* Comment [ max경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MID_SLOP           */		     700,	/* Comment [ high-mid경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MIN_SLOP           */		     700,	/* Comment [ min경사, 즉시 해제 기준 엔진 토크 Th. (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_HMID                */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_LMID                */		      10,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MAX                 */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MIN                 */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV2_TQ_TH           */		     150,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>10%,  Idle 토크 대비 증가량) (default : 15%) ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV3_TQ_TH           */		     300,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>20%,  Idle 토크 대비 증가량) (default : 30%) ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_HMID               */		      20,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_LMID               */		       0,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MAX                */		      30,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MIN                */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV2                  */		       2,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV3                  */		       3,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_HMID_MT             */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_LMID_MT             */		      18,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[      1.8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MAX_MT              */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MIN_MT              */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV2_TQ_TH_MT        */		     150,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>10%,  Idle 토크 대비 증가량) (default : 15%) ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV3_TQ_TH_MT        */		     300,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>20%,  Idle 토크 대비 증가량) (default : 30%) ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_HMID_MT            */		      43,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.43 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_LMID_MT            */		      21,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.21 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MAX_MT             */		      57,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[   0.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MIN_MT             */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV2_MT               */		       2,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV3_MT               */		       3,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_HMID                */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_LMID                */		      18,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[      1.8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_MAX                 */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_MIN                 */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_HMID               */		      29,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.29 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_LMID               */		      21,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.21 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_MAX                */		      57,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[   0.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_MIN                */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2              */		      12,	/* Comment [ level 1 해제 모드 진입 엔진 토크(APS>0%, Idle 토크 대비 증가량) (default : 1%) ] */
	                                                        		        	/* ScaleVal[      1.2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2_LV2          */		      50,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>5%, Idle 토크 대비 증가량) (default : 20%) ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2_LV3          */		     100,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>10%, Idle 토크 대비 증가량) (default : 35%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV2_M2          */		       3,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV3_M2          */		       6,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_CUR_INIT_MAX_CUR_COMP         */		     150,	/* Comment [ initial valve 도달 최대값 보상량. 해당값 만큼 높아짐 (default : 150mA) ] */
	/* int16_t     	S16_AVH_HIGH_CUR_INIT_COMP            */		     150,	/* Comment [ high 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : 150mA) ] */
	/* int16_t     	S16_AVH_LOW_CUR_INIT_COMP             */		     -30,	/* Comment [ low 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : -30mA) ] */
	/* int16_t     	S16_AVH_MED_CUR_INIT_COMP             */		     100,	/* Comment [ med 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : 100mA) ] */
	/* int16_t     	S16_AVH_MIN_CUR_INIT_COMP             */		     -50,	/* Comment [ min 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : -50mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_HIGH_REF_P          */		     400,	/* Comment [ High Ref Press (default : 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_HIGH_TH_CUR         */		     610,	/* Comment [ High Ref Press hold 가능 TC 전류 (default : 615mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_LOW_REF_P           */		     120,	/* Comment [ Low Ref Press (default : 12bar) ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_LOW_TH_CUR          */		     420,	/* Comment [ Low Ref Press hold 가능 TC 전류 (default : 360mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MED_REF_P           */		     250,	/* Comment [ Med Ref Press (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MED_TH_CUR          */		     550,	/* Comment [ Med Ref Press hold 가능 TC 전류 (default : 570mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MIN_CUR             */		     300,	/* Comment [ Min Ref Press hold 가능 TC 전류 (default : 250mA) ] */
	/* uchar8_t    	U8_AVH_CUR_DOWN_TIME_AFTER_RISE       */		      25,	/* Comment [ AVH Down time after Rise mode (default : 250ms) ] */
	                                                        		        	/* ScaleVal[   250 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_CUR_INIT_DOWN_TIME             */		      20,	/* Comment [ 제어 초기 전류 패턴 하강 시간 (default : 200ms) ] */
	                                                        		        	/* ScaleVal[   200 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_CUR_INIT_UP_TIME               */		      15,	/* Comment [ 제어 초기 전류 패턴 상승 시간 (default : 150ms) ] */
	                                                        		        	/* ScaleVal[   150 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_MED              */		       0,	/* Comment [ medium fade out시 1st scan 전류 감소량 (default : 80mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_MED                    */		       7,	/* Comment [ medium fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_MED                   */		       2,	/* Comment [ medium fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_SLOW             */		       0,	/* Comment [ slow fade out시 1st scan 전류 감소량 (default : 60mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_SLOW                   */		       3,	/* Comment [ slow fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_SLOW                  */		       2,	/* Comment [ slow fade out시 전류 감소 주기  (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_FLAT             */		       0,	/* Comment [ Flat fade out시 1st scan 전류 감소량 (default : 85mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_FLAT                   */		      10,	/* Comment [ Flat fade out시 주기별 전류 감소량 (default : 2mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_FLAT_AUTO              */		      10,	/* Comment [ auto act 모드 중, 평지 fade out시 주기별 전류 감소량 (default : 2mA) ] */
	/* uchar8_t    	U8_AVH_F_O_FLAT_SLOPE_TH              */		       5,	/* Comment [ Flat fade out mode 적용 최대 경사도 (default : 5%) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_FLAT                  */		       1,	/* Comment [ Flat fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_UPHILL           */		       0,	/* Comment [ up drive fade out시 1st scan 전류 감소량 (default : 80mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_UPHILL                 */		      10,	/* Comment [ up drive fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_UPHILL_AUTO            */		      10,	/* Comment [ auto act 모드 중, 오르막 fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_UPHILL                */		       1,	/* Comment [ up drive fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_DW_DRV_MAX       */		       0,	/* Comment [ down drive fade out시 1st scan 전류 감소량 (25% 경사) (default : 120mA) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_DW_DRV_MIN       */		       0,	/* Comment [ down drive fade out시 1st scan 전류 감소량 (8% 경사) (default : 120mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_DOWN_DRV               */		       6,	/* Comment [ down drive fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_DOWN_DRV_AUTO          */		       6,	/* Comment [ auto act 모드 중, 내리막 fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_DOWN_DRV              */		       2,	/* Comment [ down drive fade out시 전류 감소 주기 (default : 2scan) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENTER_DELAY          */		      30,	/* Comment [ Sensitive mode enter delay time for vehicle stabilization (default : 0.30s) ] */
	                                                        		        	/* ScaleVal[     300 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENTER_THRES          */		       3,	/* Comment [ Min. enter press for sensitive mode at min slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_HM_THRES         */		       3,	/* Comment [ High mid enter press for sensitive mode at high mid slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_LM_THRES         */		       3,	/* Comment [ Low mid enter press for sensitive mode at low mid slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_MAX_THRES        */		       3,	/* Comment [ Max. enter press for sensitive mode at max slope (default : 5bar) ] */
	/* int16_t     	S16_AVH_T_P_TEMP_1                    */		    1500,	/* Comment [ AVH temporarily T/P 1 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_10                   */		      30,	/* Comment [ AVH temporarily T/P 10 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_2                    */		      30,	/* Comment [ AVH temporarily T/P 2 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_3                    */		      30,	/* Comment [ AVH temporarily T/P 3 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_4                    */		      30,	/* Comment [ AVH temporarily T/P 4 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_5                    */		      30,	/* Comment [ AVH temporarily T/P 5 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_6                    */		       0,	/* Comment [ AVH temporarily T/P 6 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_7                    */		       0,	/* Comment [ AVH temporarily T/P 7 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_8                    */		       0,	/* Comment [ AVH temporarily T/P 8 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_9                    */		       0,	/* Comment [ AVH temporarily T/P 9 ] */
	/* int16_t     	S16_AVH_DEFAULT_IDLE_TORQ             */		     100,	/* Comment [ AVH Default Idle torque (default : 10%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_ISG_IDLETOQ_INHIBIT_TIME      */		      60,	/* Comment [ AVH Exception time for calcuate Idle torque during ISG (default : 600ms) ] */
	                                                        		        	/* ScaleVal[     600 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SCC_TEMP_1                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_2                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_3                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_4                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_5                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* uchar8_t    	U8_SCC_ENGTORQ_OFFSET_HIGHSPD         */		       1,	/* Comment [ Offset for min. Engine Torque at med-high speed above 35kph ] */
	/* uchar8_t    	U8_SCC_ENGTORQ_OFFSET_LOWSPD          */		       8,	/* Comment [ Offset for min. Engine Torque at low speed below 15kph ] */
	/* uint16_t    	U16_SCC_Gear_1                        */		     143,	/* Comment [ 1st Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_10                       */		      22,	/* Comment [ 10th Gear Ratio * Final Gear *10 , If max gear is below 10th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_2                        */		      94,	/* Comment [ 2nd Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_3                        */		      63,	/* Comment [ 3rd Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_4                        */		      47,	/* Comment [ 4th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_5                        */		      39,	/* Comment [ 5th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_6                        */		      32,	/* Comment [ 6th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_7                        */		      25,	/* Comment [ 7th Gear Ratio * Final Gear *10 , If max gear is below 7th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_8                        */		      22,	/* Comment [ 8th Gear Ratio * Final Gear *10 , If max gear is below 8th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_9                        */		      22,	/* Comment [ 9th Gear Ratio * Final Gear *10 , If max gear is below 9th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_R                        */		      89,	/* Comment [ Rear Gear Ratio * Final Gear *10 ] */
	/* uchar8_t    	U8_SCC_MASS                           */		      20,	/* Comment [ Mass of Vehicle ] */
	                                                        		        	/* ScaleVal[    2000 kg ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 25500 ] */
	/* uchar8_t    	U8_SCC_TIRE_R                         */		      33,	/* Comment [ Rolling Radius of Tire ] */
	                                                        		        	/* ScaleVal[     0.33 m ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_SCC_CONT_E_D_GAIN                  */		      10,	/* Comment [ D gain for Engine & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_D_GAIN               */		      10,	/* Comment [ D gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_I_GAIN               */		      55,	/* Comment [ Inverse I gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_P_GAIN               */		      35,	/* Comment [ P gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G23_D_GAIN              */		      10,	/* Comment [ D gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_G23_I_GAIN              */		      75,	/* Comment [ I gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_G23_P_GAIN              */		      23,	/* Comment [ P gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_I_GAIN                  */		      47,	/* Comment [ Inverse I gain for Engine & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_E_P_GAIN                  */		      20,	/* Comment [ P gain for Engine & above 2nd Gear ] */
	/* uchar8_t    	U8_SCC_ENG_FADEOUT_SLOPE              */		       5,	/* Comment [ Fadeout Slope of Engine Torque ] */
	/* uchar8_t    	U8_SCC_FF_DEC_FACTOR_NEG_G            */		     100,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq<0 ] */
	/* uchar8_t    	U8_SCC_FF_DEC_FACTOR_POS_G            */		     100,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq>0 ] */
	/* uchar8_t    	U8_SCC_I_GAIN_AMP_BRK_HILL            */		      15,	/* Comment [ I-Gain amp at Brake & hill & vref<8kph ] */
	/* uchar8_t    	U8_SCC_I_GAIN_AMP_ENG_HILL            */		      15,	/* Comment [ I-Gain amp at Engine & hill & vref<2kph ] */
	/* uchar8_t    	U8_SCC_PID_GAIN_DIV_ENG_HILL          */		       3,	/* Comment [ PID-Gain amp at Engine & hill & accel & vref<8kph ] */
	/* int16_t     	S16_SCC_BRAKE_CONTROL_MIN_TORQUE      */		     100,	/* Comment [ Dead zone torque for Brake control activation (default = 100Nm) ] */
	/* int16_t     	S16_SCC_MAX_ENGINE_DRAG_TORQUE_COMP   */		     250,	/* Comment [ Max. engine drag torque to substitute brake control (default = 250Nm) ] */
	/* char8_t     	S8_SCC_CONT_B_D_GAIN                  */		      15,	/* Comment [ D gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_D_GAIN               */		      10,	/* Comment [ D gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_I_GAIN               */		      35,	/* Comment [ Inverse I gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_P_GAIN               */		      25,	/* Comment [ P gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_D_GAIN    */		      14,	/* Comment [ D gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_I_GAIN    */		      25,	/* Comment [ I gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_P_GAIN    */		      28,	/* Comment [ P gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_D_GAIN      */		       3,	/* Comment [ D gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_I_GAIN      */		      18,	/* Comment [ I gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_P_GAIN      */		      12,	/* Comment [ P gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_I_GAIN                  */		      45,	/* Comment [ Inverse I gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_P_GAIN                  */		      18,	/* Comment [ P gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_D_GAIN             */		      10,	/* Comment [ D gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_I_GAIN             */		      25,	/* Comment [ I gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_P_GAIN             */		      28,	/* Comment [ P gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_TARGET_P_MIN_STOP              */		     110,	/* Comment [ min Pressure to hold PID-output on flat road ] */
	                                                        		        	/* ScaleVal[     11 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* uchar8_t    	U8_SCC_BRAKE_CONST                    */		      10,	/* Comment [ factor between Wheel Torque and Pressure ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_G1_MAX            */		       5,	/* Comment [ Max. torque rate for limiting engine torque increasing at 1st gear position ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_G1_MIN            */		       1,	/* Comment [ Min. torque rate for limiting engine torque decreasing at 1st gear position ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_MAX               */		       2,	/* Comment [ Max. torque rate for limiting engine torque increasing at gear position above 1st ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_MIN               */		       1,	/* Comment [ Min. torque rate for limiting engine torque decreasing at gear position above 1st ] */
	/* int16_t     	S16_SCC_OVERRIDE_ACCEL_DIFF_1         */		    -200,	/* Comment [ Accel. difference for using engine torque after override (default -0.2g) ] */
	                                                        		        	/* ScaleVal[     -0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_OVERRIDE_ACCEL_DIFF_2         */		      10,	/* Comment [ Accel. difference for using SCC request torque after override (default 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_SCC_I_ERROR_LEVEL_H               */		     100,	/* Comment [ High SCC Decel. Error level for using ERROR_H pseudo Integration gain (default = 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_SCC_I_ERROR_LEVEL_S               */		      30,	/* Comment [ Small SCC Decel. Error level for using ERROR_S pseudo Integration gain (default = 0.03g) ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uchar8_t    	U8_SCC_I_PSEUDO_GAIN_ERROR_H          */		       3,	/* Comment [ Pseudo Integration gain at High SCC Decel. Error level (default = 3) ] */
	/* uchar8_t    	U8_SCC_I_PSEUDO_GAIN_ERROR_S          */		       5,	/* Comment [ Pseudo Integration gain at Small SCC Decel. Error level (default = 5) ] */
	/* int16_t     	S16_CDM_TEMP_1                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_2                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_3                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_4                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_5                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* char8_t     	S8_CDM_CONT_B_D_GAIN                  */		       8,	/* Comment [ D gain for CDM brake control above gear 2nd ] */
	/* char8_t     	S8_CDM_CONT_B_I_GAIN                  */		      36,	/* Comment [ I gain for CDM brake control above gear 2nd ] */
	/* char8_t     	S8_CDM_CONT_B_P_GAIN                  */		      40,	/* Comment [ P gain for CDM brake control above gear 2nd ] */
	/* uchar8_t    	U8_CDM_FF_DEC_FACTOR_NEG_G            */		     160,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq<0 (default 160) ] */
	/* int16_t     	S16_CDM_CONT_MIN_HIGH_G_THRES         */		     400,	/* Comment [ Min. Deceleration Level for High-g CDM control (default = 0.4g) ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_CDM_CONT_B_D_GAIN_HIGH_G           */		       8,	/* Comment [ D gain for CDM brake control with High-g request (default = 8) ] */
	/* char8_t     	S8_CDM_CONT_B_I_GAIN_HIGH_G           */		      36,	/* Comment [ I gain for CDM brake control with High-g request (default = 36) ] */
	/* char8_t     	S8_CDM_CONT_B_P_GAIN_HIGH_G           */		      40,	/* Comment [ P gain for CDM brake control with High-g request (default = 40) ] */
	/* uchar8_t    	U8_CDM_ENG_CONTROL_EXIT_MTP           */		      60,	/* Comment [ CDM engine brake exit above this throttle position (default = 60%) ] */
	/* uchar8_t    	U8_CDM_ENG_CONTROL_REQ_MTP            */		      50,	/* Comment [ CDM engine brake activation below this throttle position (default = 50%) ] */
	/* uchar8_t    	U8_CDM_ENG_CTRL_TORQUE_DOWN_RATE      */		     100,	/* Comment [ Torque down rate of CDM engine brake control (default = 100 Nm) ] */
	/* uchar8_t    	U8_CDM_ENG_CTRL_TORQUE_UP_RATE        */		     100,	/* Comment [ Torque up rate of CDM engine brake control (default = 100 Nm) ] */
	/* uint16_t    	U16_CDM_I_ERROR_LEVEL_H               */		     500,	/* Comment [ High CDM Decel. Error level for using ERROR_H pseudo Integration gain (default = 0.5g) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_CDM_I_ERROR_LEVEL_S               */		     200,	/* Comment [ Small CDM Decel. Error level for using ERROR_S pseudo Integration gain (default = 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uchar8_t    	U8_CDM_I_PSEUDO_GAIN_ERROR_H          */		      10,	/* Comment [ Pseudo Integration gain at High CDM Decel. Error level (default = 10) ] */
	/* uchar8_t    	U8_CDM_I_PSEUDO_GAIN_ERROR_S          */		       1,	/* Comment [ Pseudo Integration gain at Small CDM Decel. Error level (default = 1) ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_RPM               */		     900,	/* Comment [ Idle RPM of M/T vehicle ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_STALL_RPM         */		     700,	/* Comment [ RPM level to happen engine stall ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_TORQ              */		     100,	/* Comment [ Idle torque of M/T vehicle (relative torque) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_TORQ_COMP         */		      40,	/* Comment [ Idle torque compensation value at engine overload. (ex, air conditioner on) ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_MT_ENG_TORQ_OK                */		      70,	/* Comment [ Idle torque compensation value at engine overload. (ex, air conditioner on) ] */
	                                                        		        	/* ScaleVal[        7 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_TS_RPMC_FAST_RPM_DW           */		     200,	/* Comment [ RPM comp. for fast increase target speed. when RPM dot <-20 (default 200RPM) ] */
	/* int16_t     	S16_HDC_TS_RPMC_MID_RPM_DW            */		     100,	/* Comment [ RPM comp. for slow increase target speed. when RPM dot <-10 (default 100RPM) ] */
	/* uchar8_t    	U8_HDC_RAPID_STALL_RPM_RATIO          */		       8,	/* Comment [ % of stall RPM for fast increase target speed (default 80%) ] */
	                                                        		        	/* ScaleVal[       80 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_1          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 1st gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_2          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 2nd gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_3          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 3rd gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_4          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 4th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_5          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 5th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_6          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 6th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_1       */		      30,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.030g ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_2       */		      25,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.025g ] */
	                                                        		        	/* ScaleVal[    0.025 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_3       */		      20,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.020g ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_4       */		      15,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.015g ] */
	                                                        		        	/* ScaleVal[    0.015 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_5       */		      10,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.010g ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_6       */		       5,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.005g ] */
	                                                        		        	/* ScaleVal[    0.005 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* int16_t     	HBB_PRESSURE_HYSTERESIS               */		     100,	/* Comment [ Drop press at pedal releasing state (default = 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_1             */		      12,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL1 (default = 12) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_2             */		      13,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL2 (default = 13) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_3             */		      14,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL3 (default = 14) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_4             */		      18,	/* Comment [ Knee point ratio correspond to HbbNormalVacuumPressure (default = 18) ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_1         */		       0,	/* Comment [ Pedal Travel Level 1 for HBB Mpress Model at No Vacuum level (default = 0mm) ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_2         */		      50,	/* Comment [ Pedal Travel Level 2 for HBB Mpress Model at No Vacuum level (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_3         */		     400,	/* Comment [ Pedal Travel Level 3 for HBB Mpress Model at No Vacuum level (default = 40mm) ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_4         */		    1500,	/* Comment [ Pedal Travel Level 4 for HBB Mpress Model at No Vacuum level (default = 150mm) ] */
	                                                        		        	/* ScaleVal[     150 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_1         */		       0,	/* Comment [ Mpress Level 1 correspond to Pedal Travel Level 1 at No Vacuum (default = 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_2         */		      30,	/* Comment [ Mpress Level 2 correspond to Pedal Travel Level 1 at No Vacuum (default = 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_3         */		     300,	/* Comment [ Mpress Level 3 correspond to Pedal Travel Level 1 at No Vacuum (default = 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_4         */		    1300,	/* Comment [ Mpress Level 4 correspond to Pedal Travel Level 1 at No Vacuum (default = 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel1                       */		       0,	/* Comment [ Pedal Travel Level 1 for HBB Mpress Model (default = 0mm) ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel2                       */		      50,	/* Comment [ Pedal Travel Level 2 for HBB Mpress Model (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel3                       */		     400,	/* Comment [ Pedal Travel Level 3 for HBB Mpress Model (default = 40mm) ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel4                       */		    1500,	/* Comment [ Pedal Travel Level 4 for HBB Mpress Model (default = 150mm) ] */
	                                                        		        	/* ScaleVal[     150 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure1                    */		       0,	/* Comment [ Mpress Level 1 correspond to Pedal Travel Level 1 (default = 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure2                    */		      30,	/* Comment [ Mpress Level 2 correspond to Pedal Travel Level 2 (default = 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure3                    */		     300,	/* Comment [ Mpress Level 3 correspond to Pedal Travel Level 3 (default = 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure4                    */		    1300,	/* Comment [ Mpress Level 4 correspond to Pedal Travel Level 4 (default = 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbNormalVacuumPressure               */		     650,	/* Comment [ Max. Vacuum Press for HBB activation (default = 65Kpa) ] */
	                                                        		        	/* ScaleVal[     65 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	Pedal_Travel_Rate_0                   */		       0,	/* Comment [ Pedal Travel Rate for fast HBB activation (default = 0) ] */
	                                                        		        	/* ScaleVal[ 0 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	Pedal_Travel_Rate_UPPER_LIMIT         */		     350,	/* Comment [ Pedal Travel Rate for late HBB activation (default = 350) ] */
	                                                        		        	/* ScaleVal[ 35 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL1           */		     200,	/* Comment [ Vacuum Press 1 for HBB enter at Speed Thres. 1 (default = 20kPa) ] */
	                                                        		        	/* ScaleVal[     20 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL2           */		     300,	/* Comment [ Vacuum Press 2 for HBB enter at Speed Thres. 2 (default = 30kPa) ] */
	                                                        		        	/* ScaleVal[     30 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL3           */		     400,	/* Comment [ Vacuum Press 3 for HBB enter at Speed Thres. 3 (default = 40kPa) ] */
	                                                        		        	/* ScaleVal[     40 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY1             */		      40,	/* Comment [ Speed Threshold 1 for HBB enter ( default = 5km/h) ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY2             */		     240,	/* Comment [ Speed Threshold 2 for HBB enter ( default = 30km/h) ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY3             */		     640,	/* Comment [ Speed Threshold 3 for HBB enter ( default = 80km/h) ] */
	                                                        		        	/* ScaleVal[    80 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HBB_ENABLE_FLG                     */		       0,	/* Comment [ Flag for Enable/Disable according to Engine Variation ] */
	/* int16_t     	S16_HBB_NO_VAC_ENT_INIT_PED_RATE      */		       1,	/* Comment [ Initial Pedal rate for HBB Enter at No Vacuum (default = 0.1mm/3scan) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_ENT_PEDAL_TRAVEL       */		      10,	/* Comment [ Pedal Travel for HBB Enter at No Vacuum (defualt = 1mm) ] */
	                                                        		        	/* ScaleVal[       1 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_ENTER_INIT_PEDAL_RATE         */		       5,	/* Comment [ Initial Pedal rate for HBB Enter (default = 0.1mm/3scan) ] */
	                                                        		        	/* ScaleVal[ 0.5 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_ENTER_PEDAL_TRAVEL            */		     100,	/* Comment [ Pedal Travel for HBB Enter(defualt = 10) ] */
	                                                        		        	/* ScaleVal[      10 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HBB_EXIT_PEDAL_TRAVEL                 */		      50,	/* Comment [ HBB exit below this pedal travel stroke (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_EXIT_VACUUM_LEVEL             */		     600,	/* Comment [ HBB Exit Vacuum Level (default : 60kPa) ] */
	                                                        		        	/* ScaleVal[     60 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	HbbPedalStateApplyThres               */		       1,	/* Comment [ Pedal Travel Rate for HBB apply mode (default = 1) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	HbbPedalStateReleaseThres             */		      -3,	/* Comment [ Pedal Travel Rate for HBB release mode (default = -3) ] */
	                                                        		        	/* ScaleVal[ -0.3 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_HBB_ABS_COMB_TC_GAIN              */		      15,	/* Comment [ TC Gain for ABS + HBB Combination Control (default : 15) ] */
	/* int16_t     	S16_HBB_INIT_LIMIT_ASSIST_PRESS       */		     200,	/* Comment [ Initial Assist Pressure limitation for TC V/V control(default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_MIN_ASSIST_PRESS              */		      50,	/* Comment [ Minimum Assist Pressure for TC V/V control after 300ms(default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_RELEASE_HYSTRESIS_1           */		     200,	/* Comment [ Release pressure hystresis 1 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_RELEASE_HYSTRESIS_2           */		     200,	/* Comment [ Release pressure hystresis 2 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_1                 */		     250,	/* Comment [ TC Mapping Duty 1 (default : 250mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_2                 */		     750,	/* Comment [ TC Mapping Duty 2 (default : 750mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_3                 */		    1550,	/* Comment [ TC Mapping Duty 3 (default : 1550mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_1               */		      10,	/* Comment [ TC Mapping Target Pressure 1 (default : 1bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_2               */		     300,	/* Comment [ TC Mapping Target Pressure 2 (default : 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_3               */		    1500,	/* Comment [ TC Mapping Target Pressure 3 (default : 150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN1         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 1(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN2         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 2(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN3         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 3(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_P_MPRESS_COMP_GAIN      */		      10,	/* Comment [ Assist Pressure Gain by Mpress compensation  (default = 10) ] */
	/* char8_t     	HBB_Assist_Press_Motor_D_Gain         */		       2,	/* Comment [ HBB MSC D gain of assist pressure (default = 2) ] */
	/* int16_t     	HBB_Assist_Press_Motor_P_Gain         */		       0,	/* Comment [ HBB MSC P gain of assist pressure (default = 0) ] */
	/* int16_t     	S16_HBB_MSC_EBD_COMP_GAIN             */		       7,	/* Comment [ HBB MSC Comp gain on EBD (default = 7) ] */
	/* int16_t     	S16_HBB_MSC_INIT_TIME                 */		      10,	/* Comment [ HBB MSC Initial Time (default = 100msec) ] */
	                                                        		        	/* ScaleVal[   100 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_HBB_ABS_COMB_TARGET_VOL            */		      30,	/* Comment [ HBB MSC Target V during ABS control (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_0                     */		       0,	/* Comment [ HBB Pedal Feel MSC 0 by Mpress (default = 0V) ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_1                     */		       3,	/* Comment [ HBB Pedal Feel MSC 1 by Mpress (default = 0.3V) ] */
	                                                        		        	/* ScaleVal[      0.3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_2                     */		       7,	/* Comment [ HBB Pedal Feel MSC 2 by Mpress (default = 0.7V) ] */
	                                                        		        	/* ScaleVal[      0.7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_3                     */		      20,	/* Comment [ HBB Pedal Feel MSC 3 by Mpress (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_1                  */		      10,	/* Comment [ HBB MSC for Release 1 (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_2                  */		      15,	/* Comment [ HBB MSC for Release 2 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_3                  */		      20,	/* Comment [ HBB MSC for Release 3 (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_4                  */		      30,	/* Comment [ HBB MSC for Release 4 (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_1                     */		      10,	/* Comment [ HBB MSC during Vehicle stops 1 (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_2                     */		      15,	/* Comment [ HBB MSC during Vehicle stops 2 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_3                     */		      20,	/* Comment [ HBB MSC during Vehicle stops 3 (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_4                     */		      30,	/* Comment [ HBB MSC during Vehicle stops 4 (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_100BAR       */		      70,	/* Comment [ HBB MSC for Initial Control 4 at No Vacuum Level (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_10BAR        */		      40,	/* Comment [ HBB MSC for Initial Control 2 at No Vacuum Level (default = 4V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_1BAR         */		      30,	/* Comment [ HBB MSC for Initial Control 1 at No Vacuum Level (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_30BAR        */		      60,	/* Comment [ HBB MSC for Initial Control 3 at No Vacuum Level (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_1         */		      15,	/* Comment [ HBB MSC for Low Speed 1 at No Vacuum Level (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_2         */		      25,	/* Comment [ HBB MSC for Low Speed 2 at No Vacuum Level (default = 2.5V) ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_3         */		      35,	/* Comment [ HBB MSC for Low Speed 3 at No Vacuum Level (default = 3.5V) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_4         */		      50,	/* Comment [ HBB MSC for Low Speed 4 at No Vacuum Level (default = 5V) ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_100BAR       */		      60,	/* Comment [ HBB MSC 4 at No Vacuum Level (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_10BAR        */		      20,	/* Comment [ HBB MSC 2 at No Vacuum Level (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_1BAR         */		      10,	/* Comment [ HBB MSC 1 at No Vacuum Level (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_30BAR        */		      40,	/* Comment [ HBB MSC 3 at No Vacuum Level (default = 4V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_100BAR      */		      70,	/* Comment [ Initial HBB MSC Target V for assist pressure 100bar (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_10BAR       */		      15,	/* Comment [ Initial HBB MSC Target V for assist pressure 10bar (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_1BAR        */		      10,	/* Comment [ Initial HBB MSC Target V for assist pressure 1bar (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_30BAR       */		      60,	/* Comment [ Initial HBB MSC Target V for assist pressure 30bar (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_1                */		      15,	/* Comment [ HBB MSC for Low Speed 1 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_2                */		      25,	/* Comment [ HBB MSC for Low Speed 2 (default = 2.5V) ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_3                */		      35,	/* Comment [ HBB MSC for Low Speed 3 (default = 3.5V) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_4                */		      50,	/* Comment [ HBB MSC for Low Speed 4 (default = 5V) ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_100BAR              */		      70,	/* Comment [ HBB MSC Target V for assist pressure 100bar (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_10BAR               */		      15,	/* Comment [ HBB MSC Target V for assist pressure 10bar (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_1BAR                */		       5,	/* Comment [ HBB MSC Target V for assist pressure 1bar (default = 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_30BAR               */		      30,	/* Comment [ HBB MSC Target V for assist pressure 30bar (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HBB_Temp_1                        */		       0,	/* Comment [ HBB Temporarily INT Variable 1 ] */
	/* int16_t     	S16_HBB_Temp_2                        */		       0,	/* Comment [ HBB Temporarily INT Variable 2 ] */
	/* int16_t     	S16_HBB_Temp_3                        */		       0,	/* Comment [ HBB Temporarily INT Variable 3 ] */
	/* int16_t     	S16_HBB_Temp_4                        */		       0,	/* Comment [ HBB Temporarily INT Variable 4 ] */
	/* int16_t     	S16_HBB_Temp_5                        */		       0,	/* Comment [ HBB Temporarily INT Variable 5 ] */
	/* uchar8_t    	U8_HBB_Temp_1                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 1 ] */
	/* uchar8_t    	U8_HBB_Temp_2                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 2 ] */
	/* uchar8_t    	U8_HBB_Temp_3                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 3 ] */
	/* uchar8_t    	U8_HBB_Temp_4                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 4 ] */
	/* uchar8_t    	U8_HBB_Temp_5                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 5 ] */
	/* int16_t     	S16_HBB_ADD_BRAKE_MSC_MARGIN          */		      70,	/* Comment [ Add Brake MSC margin (default : 0.07V/(mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.07 V/(mm/scan) ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH1                */		       6,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel1 (default : 0.6 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.6 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH2                */		       5,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel2 (default : 0.5 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.5 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH3                */		       2,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel3 (default : 0.2 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.2 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH4                */		       1,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel4 (default : 0.1 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_0        */		      10,	/* Comment [ Vacuum Pressrue 0 for Boost Ratio 0 (default = 1kpa) ] */
	                                                        		        	/* ScaleVal[      1 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_1        */		      40,	/* Comment [ Vacuum Pressrue 1 for Boost Ratio 1 (default = 4kpa) ] */
	                                                        		        	/* ScaleVal[      4 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_2        */		     100,	/* Comment [ Vacuum Pressrue 2 for Boost Ratio 2 (default = 10kpa) ] */
	                                                        		        	/* ScaleVal[     10 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_3        */		     200,	/* Comment [ Vacuum Pressrue 3 for Boost Ratio 3 (default = 20kpa) ] */
	                                                        		        	/* ScaleVal[     20 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_0                  */		      14,	/* Comment [ Boost Ratio 0 at Vacuum Pressrue 0 (default = 14) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_1                  */		      12,	/* Comment [ Boost Ratio 1 at Vacuum Pressrue 1 (default = 12) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_2                  */		       8,	/* Comment [ Boost Ratio 2 at Vacuum Pressrue 2 (default = 8) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_3                  */		       5,	/* Comment [ Boost Ratio 3 at Vacuum Pressrue 3 (default = 5) ] */
	/* int16_t     	S16_ADC_TARGET_G_INIT_MAX             */		      -5,	/* Comment [ 제어 시작 1st scan의 목표 감속도 Max Limit (default : -0.05g) ] */
	                                                        		        	/* ScaleVal[    -0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_JUMP_LIMIT           */		       1,	/* Comment [ 추종 cycle 당, 요구 감속도 추종량 (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_MAX_LIMIT            */		     -40,	/* Comment [ ADC 제어 감속도 max limit (default : -0.4g) ] */
	                                                        		        	/* ScaleVal[     -0.4 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_UPDATE_CYCLE         */		      10,	/* Comment [ 요구 감속도 추종 cycle (default : 100ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL                 */		       3,	/* Comment [ 가속페달 Th. 이하 시의 감압 Level (default : 3) ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL_ACCEL           */		       3,	/* Comment [ 가속페달 Th. 초과 시의 감압 Level (default : 3) ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL_ACCEL_REF       */		      10,	/* Comment [ 감압 기울기 Level 결정 위한 가속페달 Th. (default : 10%) ] */
	/* int16_t     	S16_ACC_INITIAL_CURRENT               */		     400,	/* Comment [ 제어 시작 1st scan의 TC v/v 인가 전류 (default : 400mA) ] */
	/* int16_t     	S16_ACC_INITIAL_TARGET_V              */		    5000,	/* Comment [ 제어 초기 Feed Forward 구간의 모터 구동 전압 (default : 5V) ] */
	                                                        		        	/* ScaleVal[     5 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADC_BRK_LAMP_ON_REF_TIMER         */		     100,	/* Comment [ 제동 등 점등 index Th. (모터 구동 전압 및 시간에 따른 index) (default : 100) ] */
	/* uchar8_t    	U8_ADC_RESERVED_1                     */		       0,	/* Comment [ ADC reserved 1 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_2                     */		       0,	/* Comment [ ADC reserved 2 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_3                     */		       0,	/* Comment [ ADC reserved 3 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_4                     */		       0,	/* Comment [ ADC reserved 4 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_5                     */		       0,	/* Comment [ ADC reserved 5 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_6                     */		       0,	/* Comment [ ADC reserved 6 (default : 0) ] */
	/* int16_t     	S16_WPE_Max_TCMF_Hold_Press           */		   15000,	/* Comment [ TC_Valve_Max_Hold_Press, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_Motor_Eccentricity                 */		     100,	/* Comment [ Motor Eccentricity, default : 0.00100m ] */
	                                                        		        	/* ScaleVal[    0.001 m ]	Scale [ f(x) = (X + 0) * 1E-005 ]  Range   [ 0 <= X <= 0.00255 ] */
	/* uchar8_t    	U8_Motor_K_Torque_Current             */		      21,	/* Comment [ Motor_K_Torque_Current, default : 0.021Nm/A ] */
	                                                        		        	/* ScaleVal[ 0.021 Nm/A ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_Motor_Resistance                   */		      35,	/* Comment [ Motor_Resistancer, default : 0.035ohm ] */
	                                                        		        	/* ScaleVal[  0.035 ohm ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_Motor_Volt_RPM                     */		      23,	/* Comment [ Motor_Volt_RPM, default : 0.0023Volt/rpm ] */
	                                                        		        	/* ScaleVal[ 0.0023 Volt/rpm ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ 0 <= X <= 0.0255 ] */
	/* uchar8_t    	U8_Piston_Diameter                    */		      78,	/* Comment [ Piston_Diameter, default : 0.0078m ] */
	                                                        		        	/* ScaleVal[   0.0078 m ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ 0 <= X <= 0.0255 ] */
	/* uchar8_t    	U8_Pump_Efficiency_High_P             */		      50,	/* Comment [ Pump_Efficiency_High_P, default : 50% ] */
	/* uchar8_t    	U8_Pump_Efficiency_Low_P              */		      65,	/* Comment [ Pump_Efficiency_Low_P, default : 65% ] */
	/* int16_t     	S16_Caliper_Model_Volume_No_f         */		     100,	/* Comment [ Front_Caliper_Model_Invalidity_Volume, default : 0.100cc ] */
	                                                        		        	/* ScaleVal[     0.1 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_No_r         */		      30,	/* Comment [ Rear_Caliper_Model_Invalidity_Volume, default : 0.030cc ] */
	                                                        		        	/* ScaleVal[    0.03 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Press_1             */		       0,	/* Comment [ Caliper_Model_Pressure_Map_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_10            */		    8000,	/* Comment [ Caliper_Model_Pressure_Map_10, default : 80bar ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_11            */		    9000,	/* Comment [ Caliper_Model_Pressure_Map_11, default : 90bar ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_12            */		   10000,	/* Comment [ Caliper_Model_Pressure_Map_12, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_13            */		   11000,	/* Comment [ Caliper_Model_Pressure_Map_13, default : 110bar ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_14            */		   12000,	/* Comment [ Caliper_Model_Pressure_Map_14, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_15            */		   13000,	/* Comment [ Caliper_Model_Pressure_Map_15, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_16            */		   14000,	/* Comment [ Caliper_Model_Pressure_Map_16, default : 140bar ] */
	                                                        		        	/* ScaleVal[    140 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_2             */		     500,	/* Comment [ Caliper_Model_Pressure_Map_2, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_3             */		    1000,	/* Comment [ Caliper_Model_Pressure_Map_3, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_4             */		    2000,	/* Comment [ Caliper_Model_Pressure_Map_4, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_5             */		    3000,	/* Comment [ Caliper_Model_Pressure_Map_5, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_6             */		    4000,	/* Comment [ Caliper_Model_Pressure_Map_6, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_7             */		    5000,	/* Comment [ Caliper_Model_Pressure_Map_7, default : 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_8             */		    6000,	/* Comment [ Caliper_Model_Pressure_Map_8, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_9             */		    7000,	/* Comment [ Caliper_Model_Pressure_Map_9, default : 70bar ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_1          */		       0,	/* Comment [ Front_Caliper_Model_Volume_Map_1, default : 0.000cc ] */
	                                                        		        	/* ScaleVal[       0 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_10         */		    2377,	/* Comment [ Front_Caliper_Model_Volume_Map_10, default : 2.377cc ] */
	                                                        		        	/* ScaleVal[   2.377 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_11         */		    2515,	/* Comment [ Front_Caliper_Model_Volume_Map_11, default : 2.515cc ] */
	                                                        		        	/* ScaleVal[   2.515 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_12         */		    2650,	/* Comment [ Front_Caliper_Model_Volume_Map_12, default : 2.650cc ] */
	                                                        		        	/* ScaleVal[    2.65 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_13         */		    2789,	/* Comment [ Front_Caliper_Model_Volume_Map_13, default : 2.789cc ] */
	                                                        		        	/* ScaleVal[   2.789 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_14         */		    2919,	/* Comment [ Front_Caliper_Model_Volume_Map_14, default : 2.919cc ] */
	                                                        		        	/* ScaleVal[   2.919 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_15         */		    3058,	/* Comment [ Front_Caliper_Model_Volume_Map_15, default : 3.058cc ] */
	                                                        		        	/* ScaleVal[   3.058 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_16         */		    3192,	/* Comment [ Front_Caliper_Model_Volume_Map_16, default : 3.192cc ] */
	                                                        		        	/* ScaleVal[   3.192 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_2          */		     660,	/* Comment [ Front_Caliper_Model_Volume_Map_2, default : 0.660cc ] */
	                                                        		        	/* ScaleVal[    0.66 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_3          */		     860,	/* Comment [ Front_Caliper_Model_Volume_Map_3, default : 0.860cc ] */
	                                                        		        	/* ScaleVal[    0.86 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_4          */		    1180,	/* Comment [ Front_Caliper_Model_Volume_Map_4, default : 1.180cc ] */
	                                                        		        	/* ScaleVal[    1.18 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_5          */		    1460,	/* Comment [ Front_Caliper_Model_Volume_Map_5, default : 1.460cc ] */
	                                                        		        	/* ScaleVal[    1.46 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_6          */		    1690,	/* Comment [ Front_Caliper_Model_Volume_Map_6, default : 1.690cc ] */
	                                                        		        	/* ScaleVal[    1.69 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_7          */		    1890,	/* Comment [ Front_Caliper_Model_Volume_Map_7, default : 1.890cc ] */
	                                                        		        	/* ScaleVal[    1.89 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_8          */		    2080,	/* Comment [ Front_Caliper_Model_Volume_Map_8, default : 2.080cc ] */
	                                                        		        	/* ScaleVal[    2.08 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_9          */		    2228,	/* Comment [ Front_Caliper_Model_Volume_Map_9, default : 2.228cc ] */
	                                                        		        	/* ScaleVal[   2.228 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_1          */		       0,	/* Comment [ Rear_Caliper_Model_Volume_Map_1, default : 0.000cc ] */
	                                                        		        	/* ScaleVal[       0 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_10         */		    1080,	/* Comment [ Rear_Caliper_Model_Volume_Map_10, default : 1.080cc ] */
	                                                        		        	/* ScaleVal[    1.08 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_11         */		    1150,	/* Comment [ Rear_Caliper_Model_Volume_Map_11, default : 1.150cc ] */
	                                                        		        	/* ScaleVal[    1.15 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_12         */		    1230,	/* Comment [ Rear_Caliper_Model_Volume_Map_12, default : 1.230cc ] */
	                                                        		        	/* ScaleVal[    1.23 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_13         */		    1300,	/* Comment [ Rear_Caliper_Model_Volume_Map_13, default : 1.300cc ] */
	                                                        		        	/* ScaleVal[     1.3 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_14         */		    1370,	/* Comment [ Rear_Caliper_Model_Volume_Map_14, default : 1.370cc ] */
	                                                        		        	/* ScaleVal[    1.37 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_15         */		    1440,	/* Comment [ Rear_Caliper_Model_Volume_Map_15, default : 1.440cc ] */
	                                                        		        	/* ScaleVal[    1.44 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_16         */		    1510,	/* Comment [ Rear_Caliper_Model_Volume_Map_16, default : 1.510cc ] */
	                                                        		        	/* ScaleVal[    1.51 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_2          */		     350,	/* Comment [ Rear_Caliper_Model_Volume_Map_2, default : 0.350cc ] */
	                                                        		        	/* ScaleVal[    0.35 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_3          */		     440,	/* Comment [ Rear_Caliper_Model_Volume_Map_3, default : 0.440cc ] */
	                                                        		        	/* ScaleVal[    0.44 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_4          */		     570,	/* Comment [ Rear_Caliper_Model_Volume_Map_4, default : 0.570cc ] */
	                                                        		        	/* ScaleVal[    0.57 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_5          */		     670,	/* Comment [ Rear_Caliper_Model_Volume_Map_5, default : 0.670cc ] */
	                                                        		        	/* ScaleVal[    0.67 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_6          */		     760,	/* Comment [ Rear_Caliper_Model_Volume_Map_6, default : 0.760cc ] */
	                                                        		        	/* ScaleVal[    0.76 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_7          */		     850,	/* Comment [ Rear_Caliper_Model_Volume_Map_7, default : 0.850cc ] */
	                                                        		        	/* ScaleVal[    0.85 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_8          */		     920,	/* Comment [ Rear_Caliper_Model_Volume_Map_8, default : 0.920cc ] */
	                                                        		        	/* ScaleVal[    0.92 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_9          */		    1000,	/* Comment [ Rear_Caliper_Model_Volume_Map_9, default : 1.000cc ] */
	                                                        		        	/* ScaleVal[       1 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MP_Rise_Gain_f_1_volume           */		      70,	/* Comment [ Front_MP_Rise_Gain_1, default : 50% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_2_volume           */		     100,	/* Comment [ Front_MP_Rise_Gain_2, default : 60% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_3_volume           */		     110,	/* Comment [ Front_MP_Rise_Gain_3, default : 90% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_4_volume           */		     155,	/* Comment [ Front_MP_Rise_Gain_4, default : 140% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_5_volume           */		     160,	/* Comment [ Front_MP_Rise_Gain_5, default : 160% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_6_volume           */		     165,	/* Comment [ Front_MP_Rise_Gain_6, default : 180% ] */
	/* int16_t     	S16_MP_Rise_Press_f_1_volume          */		      50,	/* Comment [ Front_MP_Rise_Press_1, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_2_volume          */		     150,	/* Comment [ Front_MP_Rise_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_3_volume          */		     450,	/* Comment [ Front_MP_Rise_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_4_volume          */		     650,	/* Comment [ Front_MP_Rise_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     65 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_5_volume          */		     900,	/* Comment [ Front_MP_Rise_Press_5, default : 90bar ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_6_volume          */		    1200,	/* Comment [ Front_MP_Rise_Press_6, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_r_1_volume           */		      70,	/* Comment [ Rear_MP_Rise_Gain_1, default : 30% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_2_volume           */		      80,	/* Comment [ Rear_MP_Rise_Gain_2, default : 45% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_3_volume           */		     110,	/* Comment [ Rear_MP_Rise_Gain_3, default : 90% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_4_volume           */		     135,	/* Comment [ Rear_MP_Rise_Gain_4, default : 130% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_5_volume           */		     140,	/* Comment [ Rear_MP_Rise_Gain_5, default : 140% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_6_volume           */		     150,	/* Comment [ Rear_MP_Rise_Gain_6, default : 150% ] */
	/* int16_t     	S16_MP_Rise_Press_r_1_volume          */		      50,	/* Comment [ Rear_MP_Rise_Press_1, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_2_volume          */		     150,	/* Comment [ Rear_MP_Rise_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_3_volume          */		     350,	/* Comment [ Rear_MP_Rise_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_4_volume          */		     650,	/* Comment [ Rear_MP_Rise_Press_4, default : 70bar ] */
	                                                        		        	/* ScaleVal[     65 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_5_volume          */		     950,	/* Comment [ Rear_MP_Rise_Press_5, default : 100bar ] */
	                                                        		        	/* ScaleVal[     95 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_6_volume          */		    1200,	/* Comment [ Rear_MP_Rise_Press_6, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_4                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_4, default : 0 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_5                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_5, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_4                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_4, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_5                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_5, default : 0 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_01                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_01 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_02                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_02 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_03                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_03 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_04                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_04 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_05                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_05 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_06                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_06 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_07                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_07 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_1                */		      75,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_01 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_2                */		      80,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_02 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_3                */		      85,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_03 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_4                */		      90,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_04 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_5                */		      90,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_05 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_6                */		      90	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_06 ] */
};

#define LOGIC_CAL_MODULE_6_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

