#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_31_START


const /*far*/	DATA_APCALAHBPCTRL_t	apCalAhbPCtrlSecondary =
{

	/* uint16_t    	apCalAhbPCtrlInfo.CDID                */		0xFFFF,
	/* uint16_t    	apCalAhbPCtrlInfo.CDVer               */		0xFFFF,
	/* uchar8_t    	apCalAhbPCtrlInfo.MID                 */		0xFF,
	/* int16_t     	S16_AV_CURRENT_STATIC_MAX             */		    1300,
	/* int16_t     	S16_HPA_P_COMP_FACTOR_PER_10BAR       */		      65,
	/* int16_t     	S16_ALV_PRE_CURRENT_OFFSET            */		     300,	/* Comment [ Apply V/V Pre Current (Initial 값에서 ? offset ) ] */
	/* int16_t     	S16_AV_CURRENT_1_1ST                  */		     700,	/* Comment [ 1st cycle에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_1ST                  */		     850,	/* Comment [ 1st cycle에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_1ST                  */		     900,	/* Comment [ 1st cycle에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_1ST                  */		    1000,	/* Comment [ 1st cycle에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_1ST_CYC          */		       0,	/* Comment [ 1st cycle에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_1ST_CYC          */		      20,	/* Comment [ 1st cycle에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_1ST_CYC          */		      40,	/* Comment [ 1st cycle에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_1ST_CYC          */		      60,	/* Comment [ 1st cycle에서의 가압 제어 기준 기울기 4 (기준 기울기 4 <= 기준 기울기 5) ] */
	/* uint16_t    	U16_INIT_FAST_APPLY_CURRENT           */		    1600,	/* Comment [ 1st cycle에서 quick braking 판단 시의 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_1ST_LOW_SPD          */		     650,	/* Comment [ creeping speed 이하, 1st cycle에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_1ST_LOW_SPD          */		     700,	/* Comment [ creeping speed 이하, 1st cycle에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_1ST_LOW_SPD          */		     800,	/* Comment [ creeping speed 이하, 1st cycle에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_1ST_LOW_SPD          */		     900,	/* Comment [ creeping speed 이하, 1st cycle에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* uint16_t    	U16_INIT_FAST_APPLY_CURRENT_LSD       */		    1200,	/* Comment [ Low Speed(30kph) 이하에서 1st cycle에서 quick braking 판단 시의 Apply valve current ] */
	/* int16_t     	S16_APPLY_PRESS_LEVEL_1               */		      50,	/* Comment [ Apply Valve Current를 Setting하는 Boost Chamber Pressure Level_1 ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_APPLY_PRESS_LEVEL_2               */		     100,	/* Comment [ Apply Valve Current를 Setting하는 Boost Chamber Pressure Level_2 ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_APPLY_PRESS_LEVEL_3               */		     400,	/* Comment [ Apply Valve Current를 Setting하는 Boost Chamber Pressure Level_3 ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_APPLY_PRESS_LEVEL_4               */		     800,	/* Comment [ Apply Valve Current를 Setting하는 Boost Chamber Pressure Level_4 ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_APPLY_PRESS_LEVEL_5               */		    1200,	/* Comment [ Apply Valve Current를 Setting하는 Boost Chamber Pressure Level_5 ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_1_AT_L1P       */		     680,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_2_AT_L1P       */		     780,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_3_AT_L1P       */		    1100,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_4_AT_L1P       */		    1450,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_AT_L1P               */		     800,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_AT_L1P               */		     950,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_AT_L1P               */		    1050,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_AT_L1P               */		    1200,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_AT_L1P           */		       2,	/* Comment [ Boost Chamber Press Level_1에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_AT_L1P           */		      20,	/* Comment [ Boost Chamber Press Level_1에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[  2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_AT_L1P           */		     100,	/* Comment [ Boost Chamber Press Level_1에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ 10 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_AT_L1P           */		     500,	/* Comment [ Boost Chamber Press Level_1에서의 가압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ 50 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_1_AT_L2P       */		     640,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_2_AT_L2P       */		     780,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_3_AT_L2P       */		    1100,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_4_AT_L2P       */		    1580,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_AT_L2P               */		     850,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_AT_L2P               */		    1000,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_AT_L2P               */		    1150,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_AT_L2P               */		    1300,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_AT_L2P           */		       2,	/* Comment [ Boost Chamber Press Level_2에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_AT_L2P           */		      50,	/* Comment [ Boost Chamber Press Level_2에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[  5 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_AT_L2P           */		     150,	/* Comment [ Boost Chamber Press Level_2에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ 15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_AT_L2P           */		     500,	/* Comment [ Boost Chamber Press Level_2에서의 가압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ 50 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_1_AT_L3P       */		     980,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_2_AT_L3P       */		    1100,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_3_AT_L3P       */		    1300,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_4_AT_L3P       */		    1750,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_AT_L3P               */		    1000,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_AT_L3P               */		    1100,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_AT_L3P               */		    1300,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_AT_L3P               */		    1500,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_AT_L3P           */		       2,	/* Comment [ Boost Chamber Press Level_3에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_AT_L3P           */		      50,	/* Comment [ Boost Chamber Press Level_3에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[  5 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_AT_L3P           */		     150,	/* Comment [ Boost Chamber Press Level_3에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ 15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_AT_L3P           */		     700,	/* Comment [ Boost Chamber Press Level_3에서의 가압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ 70 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_1_AT_L4P       */		    1320,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_2_AT_L4P       */		    1400,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_3_AT_L4P       */		    1550,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_4_AT_L4P       */		    1800,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_AT_L4P               */		    1250,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_AT_L4P               */		    1350,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_AT_L4P               */		    1500,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_AT_L4P               */		    1750,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_AT_L4P           */		       2,	/* Comment [ Boost Chamber Press Level_4에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_AT_L4P           */		      50,	/* Comment [ Boost Chamber Press Level_4에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[  5 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_AT_L4P           */		     150,	/* Comment [ Boost Chamber Press Level_4에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ 15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_AT_L4P           */		     700,	/* Comment [ Boost Chamber Press Level_4에서의 가압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ 70 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_1_AT_L5P       */		    1480,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_2_AT_L5P       */		    1570,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_3_AT_L5P       */		    1680,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_1V_CTRL_CURRENT_4_AT_L5P       */		    1900,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_1_AT_L5P               */		    1500,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_1의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_2_AT_L5P               */		    1600,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_2의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_3_AT_L5P               */		    1750,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_3의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_AV_CURRENT_4_AT_L5P               */		    2000,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Increase Rate_4의 기울기로 BCP 증가하는 Apply valve current ] */
	/* int16_t     	S16_PRESS_INC_RATE_1_AT_L5P           */		       2,	/* Comment [ Boost Chamber Press Level_5에서의 가압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_2_AT_L5P           */		      50,	/* Comment [ Boost Chamber Press Level_5에서의 가압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[  5 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_3_AT_L5P           */		     150,	/* Comment [ Boost Chamber Press Level_5에서의 가압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ 15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_INC_RATE_4_AT_L5P           */		     700,	/* Comment [ Boost Chamber Press Level_5에서의 가압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ 70 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RLV_PRE_CURRENT_OFFSET            */		     100,	/* Comment [ Release V/V Pre Current (Initial 값에서 ? offset ) ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_INIT              */		    1200,	/* Comment [ Release Valve initial Current At Pressure Level 1 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_INIT              */		    1100,	/* Comment [ Release Valve initial Current At Pressure Level 2 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_INIT              */		    1000,	/* Comment [ Release Valve initial Current At Pressure Level 3 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_INIT              */		     800,	/* Comment [ Release Valve initial Current At Pressure Level 4 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RV_CURRENT_5_AT_INIT              */		     500,	/* Comment [ Release Valve initial Current At Pressure Level 5 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RV_CURRENT_6_AT_INIT              */		     300,	/* Comment [ Release Valve initial Current At Pressure Level 6 (S16_RV_CURRENT_4_AT_L1P 보다 10~30mA 작은값) ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_0             */		      50,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_0 ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_1             */		     100,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_1 ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_2             */		     200,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_2 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_3             */		     400,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_3 ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_4             */		     800,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_4 ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_5             */		    1200,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_5 ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RELEASE_PRESS_LEVEL_6             */		    1500,	/* Comment [ RELEASE Valve Current를 Setting하는 Boost Chamber Pressure Level_6 ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L0P           */		    -650,	/* Comment [ Boost Chamber Press Level_0에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -65 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L0P           */		    -150,	/* Comment [ Boost Chamber Press Level_0에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L0P           */		     -10,	/* Comment [ Boost Chamber Press Level_0에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L0P           */		      -2,	/* Comment [ Boost Chamber Press Level_0에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L0P       */		    2250,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L0P       */		    2250,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L0P       */		    2180,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L0P       */		    2120,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L0P               */		     870,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L0P               */		     820,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L0P               */		     760,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L0P               */		     740,	/* Comment [ Boost Chamber Press Level_0에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L1P           */		    -650,	/* Comment [ Boost Chamber Press Level_1에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -65 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L1P           */		    -150,	/* Comment [ Boost Chamber Press Level_1에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L1P           */		     -10,	/* Comment [ Boost Chamber Press Level_1에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L1P           */		      -2,	/* Comment [ Boost Chamber Press Level_1에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L1P       */		    2250,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L1P       */		    2200,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L1P       */		    2160,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L1P       */		    2100,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L1P               */		     850,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L1P               */		     800,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L1P               */		     750,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L1P               */		     730,	/* Comment [ Boost Chamber Press Level_1에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L2P           */		    -750,	/* Comment [ Boost Chamber Press Level_2에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -75 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L2P           */		    -150,	/* Comment [ Boost Chamber Press Level_2에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L2P           */		     -10,	/* Comment [ Boost Chamber Press Level_2에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L2P           */		      -2,	/* Comment [ Boost Chamber Press Level_2에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L2P       */		    2250,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L2P       */		    2160,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L2P       */		    2060,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L2P       */		    2000,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L2P               */		     800,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L2P               */		     760,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L2P               */		     710,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L2P               */		     690,	/* Comment [ Boost Chamber Press Level_2에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L3P           */		    -850,	/* Comment [ Boost Chamber Press Level_3에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -85 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L3P           */		    -150,	/* Comment [ Boost Chamber Press Level_3에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L3P           */		     -10,	/* Comment [ Boost Chamber Press Level_3에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L3P           */		      -2,	/* Comment [ Boost Chamber Press Level_3에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L3P       */		    2250,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L3P       */		    2100,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L3P       */		    1920,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L3P       */		    1850,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L3P               */		     780,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L3P               */		     730,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L3P               */		     660,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L3P               */		     630,	/* Comment [ Boost Chamber Press Level_3에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L4P           */		   -1200,	/* Comment [ Boost Chamber Press Level_4에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -120 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L4P           */		    -150,	/* Comment [ Boost Chamber Press Level_4에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L4P           */		     -10,	/* Comment [ Boost Chamber Press Level_4에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L4P           */		      -2,	/* Comment [ Boost Chamber Press Level_4에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L4P       */		    2050,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L4P       */		    1840,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L4P       */		    1600,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L4P       */		    1520,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L4P               */		     750,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L4P               */		     650,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L4P               */		     530,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L4P               */		     500,	/* Comment [ Boost Chamber Press Level_4에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L5P           */		   -1200,	/* Comment [ Boost Chamber Press Level_5에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -120 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L5P           */		    -150,	/* Comment [ Boost Chamber Press Level_5에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L5P           */		     -10,	/* Comment [ Boost Chamber Press Level_5에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L5P           */		      -2,	/* Comment [ Boost Chamber Press Level_5에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L5P       */		    1900,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L5P       */		    1500,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L5P       */		    1300,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L5P       */		    1220,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L5P               */		     710,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L5P               */		     580,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L5P               */		     400,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L5P               */		     360,	/* Comment [ Boost Chamber Press Level_5에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_PRESS_DEC_RATE_1_AT_L6P           */		   -1200,	/* Comment [ Boost Chamber Press Level_6에서의 감압 제어 기준 기울기 1 (기준 기울기 1 <= 기준 기울기 2) ] */
	                                                        		        	/* ScaleVal[ -120 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_2_AT_L6P           */		    -150,	/* Comment [ Boost Chamber Press Level_6에서의 감압 제어 기준 기울기 2 (기준 기울기 2 <= 기준 기울기 3) ] */
	                                                        		        	/* ScaleVal[ -15 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_3_AT_L6P           */		     -10,	/* Comment [ Boost Chamber Press Level_6에서의 감압 제어 기준 기울기 3 (기준 기울기 3 <= 기준 기울기 4) ] */
	                                                        		        	/* ScaleVal[ -1 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_DEC_RATE_4_AT_L6P           */		      -2,	/* Comment [ Boost Chamber Press Level_6에서의 감압 제어 기준 기울기 4 ] */
	                                                        		        	/* ScaleVal[ -0.2 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_1_AT_L6P       */		    1800,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_2_AT_L6P       */		    1350,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_3_AT_L6P       */		    1000,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_1V_CTRL_CURRENT_4_AT_L6P       */		     900,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_1_AT_L6P               */		     700,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_1의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_2_AT_L6P               */		     500,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_2의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_3_AT_L6P               */		     350,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_3의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_RV_CURRENT_4_AT_L6P               */		     320,	/* Comment [ Boost Chamber Press Level_6에서 Pressure Decrease Rate_4의 기울기로 BCP 증가하는 Release valve current ] */
	/* int16_t     	S16_AVCOMP_PRESS_LEVEL_1              */		     200,	/* Comment [ Boost Chamber Pressure Level_1 For AV current compensation ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AVCOMP_PRESS_LEVEL_2              */		     400,	/* Comment [ Boost Chamber Pressure Level_2 For AV current compensation ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_1_RBC            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for AV current compensation during RBC ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_2_RBC            */		      30,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for AV current compensation during RBC ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_3_RBC            */		     100,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for AV current compensation during RBC ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_4_RBC            */		     200,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for AV current compensation during RBC ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_5_RBC            */		     500,	/* Comment [ Reference P rate diff level 5(BCP rate - TarP rate), for AV current compensation during RBC ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_1_RBC              */		       0,	/* Comment [ AV comp. current at P rate error 1 at RBC ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_2_RBC              */		       5,	/* Comment [ AV comp. current at P rate error 2 at RBC ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_3_RBC              */		      10,	/* Comment [ AV comp. current at P rate error 3 at RBC ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_4_RBC              */		      20,	/* Comment [ AV comp. current at P rate error 4 at RBC ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_5_RBC              */		      20,	/* Comment [ AV comp. current at P rate error 5 at RBC ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_1_L1P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_2_L1P            */		       5,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_3_L1P            */		      40,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_4_L1P            */		     100,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_5_L1P            */		     200,	/* Comment [ Reference P rate diff level 5(BCP rate - TarP rate), for AV current compensation ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_1_L1P              */		       0,	/* Comment [ AV comp. current at P rate error 1, BCP < level 1 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_2_L1P              */		       5,	/* Comment [ AV comp. current at P rate error 2, BCP < level 1 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_3_L1P              */		      10,	/* Comment [ AV comp. current at P rate error 3, BCP < level 1 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_4_L1P              */		      20,	/* Comment [ AV comp. current at P rate error 4, BCP < level 1 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_5_L1P              */		      25,	/* Comment [ AV comp. current at P rate error 5, BCP < level 1 ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_1_L2P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_2_L2P            */		       5,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_3_L2P            */		      50,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_4_L2P            */		     200,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_5_L2P            */		     500,	/* Comment [ Reference P rate diff level 5(BCP rate - TarP rate), for AV current compensation ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_1_L2P              */		       0,	/* Comment [ AV comp. current at P rate error 1, BCP < Level 1~2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_2_L2P              */		       5,	/* Comment [ AV comp. current at P rate error 2, BCP < Level 1~2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_3_L2P              */		      10,	/* Comment [ AV comp. current at P rate error 3, BCP < Level 1~2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_4_L2P              */		      20,	/* Comment [ AV comp. current at P rate error 4, BCP < Level 1~2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_5_L2P              */		      20,	/* Comment [ AV comp. current at P rate error 5, BCP < Level 1~2 ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_1_L3P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_2_L3P            */		       5,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_3_L3P            */		      50,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_4_L3P            */		     200,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for AV current compensation ] */
	/* int16_t     	S16_AVCOMP_REF_DRATE_5_L3P            */		     500,	/* Comment [ Reference P rate diff level 5(BCP rate - TarP rate), for AV current compensation ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_1_L3P              */		       0,	/* Comment [ AV comp. current at P rate error 1, BCP < Level 2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_2_L3P              */		      10,	/* Comment [ AV comp. current at P rate error 2, BCP < Level 2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_3_L3P              */		      15,	/* Comment [ AV comp. current at P rate error 3, BCP < Level 2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_4_L3P              */		      20,	/* Comment [ AV comp. current at P rate error 4, BCP < Level 2 ] */
	/* char8_t     	S8_AVCOMP_AT_DRATE_5_L3P              */		      20,	/* Comment [ AV comp. current at P rate error 5, BCP < Level 2 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH11_DP_NEG            */		       8,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP , Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[       40 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 600 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH11_DP_POS            */		      24,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 600 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH12_DP_NEG            */		     -20,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP , Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[     -100 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 0 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH12_DP_POS            */		     -13,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[      -65 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 0 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH21_DP_NEG            */		       0,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP , Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 600 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH21_DP_POS            */		       6,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 600 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH22_DP_NEG            */		     -40,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP , Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[     -200 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 0 ] */
	/* char8_t     	S8_AVCOMP_GAIN_PH22_DP_POS            */		     -30,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[     -150 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 0 ] */
	/* int16_t     	S16_RVCOMP_PRESS_LEVEL_0              */		      50,	/* Comment [ Boost Chamber Pressure Level_0 For RV current compensation ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_RVCOMP_PRESS_LEVEL_1              */		     100,	/* Comment [ Boost Chamber Pressure Level_1 For RV current compensation ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_RVCOMP_PRESS_LEVEL_2              */		     200,	/* Comment [ Boost Chamber Pressure Level_2 For RV current compensation ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_RVCOMP_PRESS_LEVEL_3              */		     400,	/* Comment [ Boost Chamber Pressure Level_3 For RV current compensation ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L0P_RBC        */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L0P_RBC        */		      10,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L0P_RBC        */		      30,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L0P_RBC        */		      50,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L0P_RBC          */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP < level 0, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L0P_RBC          */		       5,	/* Comment [ RV comp. current at P rate error 2, BCP < level 0, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L0P_RBC          */		      10,	/* Comment [ RV comp. current at P rate error 3, BCP < level 0, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L0P_RBC          */		      12,	/* Comment [ RV comp. current at P rate error 4, BCP < level 0, at RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L1P_RBC        */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L1P_RBC        */		      10,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L1P_RBC        */		     100,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L1P_RBC        */		     300,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L1P_RBC          */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP level 0~1, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L1P_RBC          */		       5,	/* Comment [ RV comp. current at P rate error 2, BCP level 0~1, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L1P_RBC          */		      10,	/* Comment [ RV comp. current at P rate error 3, BCP level 0~1, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L1P_RBC          */		      12,	/* Comment [ RV comp. current at P rate error 4, BCP level 0~1, at RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L2P_RBC        */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L2P_RBC        */		      10,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L2P_RBC        */		     100,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L2P_RBC        */		     300,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L2P_RBC          */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP level 1~2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L2P_RBC          */		       5,	/* Comment [ RV comp. current at P rate error 2, BCP level 1~2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L2P_RBC          */		      10,	/* Comment [ RV comp. current at P rate error 3, BCP level 1~2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L2P_RBC          */		      12,	/* Comment [ RV comp. current at P rate error 4, BCP level 1~2, at RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L3P_RBC        */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L3P_RBC        */		      20,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L3P_RBC        */		     150,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L3P_RBC        */		     300,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation during RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L3P_RBC          */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP > level 2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L3P_RBC          */		       5,	/* Comment [ RV comp. current at P rate error 2, BCP > level 2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L3P_RBC          */		      10,	/* Comment [ RV comp. current at P rate error 3, BCP > level 2, at RBC ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L3P_RBC          */		      12,	/* Comment [ RV comp. current at P rate error 4, BCP > level 2, at RBC ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L1P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L1P            */		       5,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L1P            */		      50,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L1P            */		     100,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L1P              */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP < level 1 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L1P              */		       3,	/* Comment [ RV comp. current at P rate error 2, BCP < level 1 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L1P              */		       8,	/* Comment [ RV comp. current at P rate error 3, BCP < level 1 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L1P              */		      10,	/* Comment [ RV comp. current at P rate error 4, BCP < level 1 ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L2P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L2P            */		      20,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L2P            */		     100,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L2P            */		     200,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L2P              */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP < Level 1~2 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L2P              */		       3,	/* Comment [ RV comp. current at P rate error 2, BCP < Level 1~2 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L2P              */		       8,	/* Comment [ RV comp. current at P rate error 3, BCP < Level 1~2 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L2P              */		      10,	/* Comment [ RV comp. current at P rate error 4, BCP < Level 1~2 ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L3P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L3P            */		      20,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L3P            */		     300,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L3P            */		     500,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L3P              */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP < Level 2~3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L3P              */		       3,	/* Comment [ RV comp. current at P rate error 2, BCP < Level 2~3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L3P              */		       5,	/* Comment [ RV comp. current at P rate error 3, BCP < Level 2~3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L3P              */		      10,	/* Comment [ RV comp. current at P rate error 4, BCP < Level 2~3 ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_1_L4P            */		       0,	/* Comment [ Reference P rate diff level 1(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_2_L4P            */		      50,	/* Comment [ Reference P rate diff level 2(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_3_L4P            */		     300,	/* Comment [ Reference P rate diff level 3(BCP rate - TarP rate), for RV current compensation ] */
	/* int16_t     	S16_RVCOMP_REF_DRATE_4_L4P            */		     500,	/* Comment [ Reference P rate diff level 4(BCP rate - TarP rate), for RV current compensation ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_1_L4P              */		       0,	/* Comment [ RV comp. current at P rate error 1, BCP > Level 3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_2_L4P              */		       5,	/* Comment [ RV comp. current at P rate error 2, BCP > Level 3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_3_L4P              */		       8,	/* Comment [ RV comp. current at P rate error 3, BCP > Level 3 ] */
	/* char8_t     	S8_RVCOMP_AT_DRATE_4_L4P              */		      10,	/* Comment [ RV comp. current at P rate error 4, BCP > Level 3 ] */
	/* uchar8_t    	U8_RVCOMP_P_ERROR_LEVEL1              */		      30,	/* Comment [ P error Level 1, Reference for RV comp gain ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_RVCOMP_P_ERROR_LEVEL2              */		     100,	/* Comment [ P error Level 2, Reference for RV comp gain ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_NEG_L1         */		     -14,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror < Level1  , Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_NEG_L2         */		     -14,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror Level1~2 , Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_NEG_L3         */		     -10,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror > Level2 , Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_POS_L1         */		     -16,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Perror < Level1 , Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -80 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_POS_L2         */		     -20,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Perror Level1~2, Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[     -100 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH11_DP_POS_L3         */		     -30,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Perror > Level2, Phase 1-1: TarP rate > BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[     -150 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH12_DP_NEG_L1         */		       0,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror < Level1  , Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH12_DP_NEG_L2         */		      10,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror Level1~2, Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH12_DP_NEG_L3         */		      20,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror > Level2 , Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH12_DP_POS            */		       0,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 1-2: TarP rate > BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH21_DP_NEG_L1         */		     -14,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror < Level1 , Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH21_DP_NEG_L2         */		     -14,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror Level1~2, Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH21_DP_NEG_L3         */		     -10,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror > Level2, Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH21_DP_POS_L1         */		     -14,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Perror < Level1, Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH21_DP_POS_L2         */		     -16,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Perror > Level1, Phase 2-1: TarP rate < BCP rate, TarP old rate > BCP rate ] */
	                                                        		        	/* ScaleVal[      -80 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_NEG_HO         */		       7,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, HoldOld 상황 , Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -1280 <= X <= 1270 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_NEG_L1         */		      10,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror < Level1 , Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_NEG_L2         */		      20,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror Level1~2, Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_NEG_L3         */		      30,	/* Comment [ 계산된 comp. current에 대한 gain at TarP>BCP, Perror > Level2, Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_POS            */		      12,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[       60 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -600 <= X <= 600 ] */
	/* char8_t     	S8_RVCOMP_GAIN_PH22_DP_POS_HO         */		       0,	/* Comment [ 계산된 comp. current에 대한 gain at TarP<=BCP, HoldOld 상황 , Phase 2-2: TarP rate < BCP rate, TarP old rate < BCP rate ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -1280 <= X <= 1270 ] */
	/* char8_t     	S8_AVCOMP_CREEPING_SPD_GAIN           */		      13,	/* Comment [ Creeping speed gain for AV Current Comp (default 65%) ] */
	                                                        		        	/* ScaleVal[       65 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	S8_RVCOMP_CREEPING_SPD_GAIN           */		      14,	/* Comment [ Creeping speed gain for RV Current Comp (default 50%) ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16_AV_COMP_CURRENT_INIT_MAX          */		     200,	/* Comment [ Apply Valve Current Comp. Max at Initial Rise State ] */
	/* int16_t     	S16_AV_COMP_CURRENT_INIT_R_MAX        */		     800,	/* Comment [ Apply Valve Current Rate Diff Comp. Max at Initial Rise State ] */
	/* int16_t     	S16_AV_COMP_CURRENT_MAX               */		     400,	/* Comment [ Apply Valve Current Comp. Max ] */
	/* int16_t     	S16_AV_CURRENT_MODECHANGE_MAX         */		     600,	/* Comment [ 정차 중 Apply Valve Initial Current at Mode Change ] */
	/* int16_t     	S16_RV_COMP_CURRENT_MAX               */		     100,	/* Comment [ Release Valve Current Comp. Max ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_1               */		      50,	/* Comment [ Ref. Target P level_1 for FRC detection ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_2               */		     150,	/* Comment [ Ref. Target P level_2 for FRC detection ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_3               */		     500,	/* Comment [ Ref. Target P level_3 for FRC detection ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_4               */		    1000,	/* Comment [ Ref. Target P level_4 for FRC detection ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L1P             */		       5,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 1 ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L2P             */		      20,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 2 ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L3P             */		      50,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 3 ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L4P             */		     200,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 4 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_1_RBC           */		      20,	/* Comment [ Ref. Target P level_1 for FRC detection during RBC ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_2_RBC           */		     100,	/* Comment [ Ref. Target P level_2 for FRC detection during RBC ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_3_RBC           */		     200,	/* Comment [ Ref. Target P level_3 for FRC detection during RBC ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_4_RBC           */		     500,	/* Comment [ Ref. Target P level_4 for FRC detection during RBC ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L1P_RBC         */		       5,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 1, in RBC ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L2P_RBC         */		       5,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 2, in RBC ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L3P_RBC         */		      10,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 3, in RBC ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L4P_RBC         */		      20,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 4, in RBC ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_1_LSPD          */		      50,	/* Comment [ Ref. Target P level_1 for FRC detection at creeping speed ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_2_LSPD          */		     100,	/* Comment [ Ref. Target P level_2 for FRC detection at creeping speed ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_3_LSPD          */		     300,	/* Comment [ Ref. Target P level_3 for FRC detection at creeping speed ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_4_LSPD          */		    1000,	/* Comment [ Ref. Target P level_4 for FRC detection at creeping speed ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L1P_LSPD        */		      10,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 1, at creeping speed ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L2P_LSPD        */		      20,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 2, at creeping speed ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L3P_LSPD        */		      50,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 3, at creeping speed ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L4P_LSPD        */		     300,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 4, at creeping speed ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_1_STOP          */		      50,	/* Comment [ Ref. Target P level_1 for FRC detection at standstill ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_2_STOP          */		     100,	/* Comment [ Ref. Target P level_2 for FRC detection at standstill ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_3_STOP          */		     300,	/* Comment [ Ref. Target P level_3 for FRC detection at standstill ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_DCT_PLEVEL_4_STOP          */		    1000,	/* Comment [ Ref. Target P level_4 for FRC detection at standstill ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L1P_STOP        */		      10,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 1, at standstill ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L2P_STOP        */		      20,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 2, at standstill ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L3P_STOP        */		      50,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 3, at standstill ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_ENT_THR_AT_L4P_STOP        */		     150,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 4, at standstill ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -50 <= X <= 50 ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_1_PH1        */		       0,	/* Comment [ Reference P rate error level 1(BCP rate - TarP rate), for fast rate change comp., Phase 1: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_2_PH1        */		      50,	/* Comment [ Reference P rate error level 2(BCP rate - TarP rate), for fast rate change comp., Phase 1: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_3_PH1        */		     200,	/* Comment [ Reference P rate error level 3(BCP rate - TarP rate), for fast rate change comp., Phase 1: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_1_PH1        */		      50,	/* Comment [ Fast rate change comp. current at Rate error 1, Phase 1: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_2_PH1        */		      80,	/* Comment [ Fast rate change comp. current at Rate error 2, Phase 1: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_3_PH1        */		     120,	/* Comment [ Fast rate change comp. current at Rate error 3, Phase 1: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_1_PH2        */		       0,	/* Comment [ Reference P rate error level 1(BCP rate - TarP rate), for fast rate change comp., Phase 2: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_2_PH2        */		      30,	/* Comment [ Reference P rate error level 2(BCP rate - TarP rate), for fast rate change comp., Phase 2: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_INC_REF_DRATE_3_PH2        */		     100,	/* Comment [ Reference P rate error level 3(BCP rate - TarP rate), for fast rate change comp., Phase 2: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_1_PH2        */		      70,	/* Comment [ Fast rate change comp. current at Rate error 1, Phase 2: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_2_PH2        */		      50,	/* Comment [ Fast rate change comp. current at Rate error 2, Phase 2: when BCP rate < TarP rate ] */
	/* uchar8_t    	U8_AV_FRC_INC_CURR_DRATE_3_PH2        */		      30,	/* Comment [ Fast rate change comp. current at Rate error 3, Phase 2: when BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_GAIN_BCP_REF_1             */		     100,	/* Comment [ Reference BCP level 1 ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_BCP_REF_2             */		     300,	/* Comment [ Reference BCP level 2 ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L1P_PH1        */		       0,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L1P_PH1        */		      50,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L1P_PH1        */		     500,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L1P_PH1         */		      12,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L1P_PH1         */		      15,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L1P_PH1         */		      30,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L2P_PH1        */		      40,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L2P_PH1        */		     100,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L2P_PH1        */		     500,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L2P_PH1         */		      10,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L2P_PH1         */		      20,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L2P_PH1         */		      30,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L3P_PH1        */		      70,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L3P_PH1        */		     200,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L3P_PH1        */		     700,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 1: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L3P_PH1         */		      10,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L3P_PH1         */		      20,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L3P_PH1         */		      30,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 1: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      300 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L1P_PH2        */		      10,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L1P_PH2        */		      50,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L1P_PH2        */		     300,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L1P_PH2         */		      10,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L1P_PH2         */		      15,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L1P_PH2         */		      20,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L2P_PH2        */		      30,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L2P_PH2        */		     100,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L2P_PH2        */		     400,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L2P_PH2         */		      10,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L2P_PH2         */		      15,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L2P_PH2         */		      20,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF1_L3P_PH2        */		      50,	/* Comment [ Reference P error level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF2_L3P_PH2        */		     200,	/* Comment [ Reference P error level 2 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_GAIN_DP_RF3_L3P_PH2        */		     500,	/* Comment [ Reference P error level 3 at BCP <10bar, Phase 2: when BCP rate < TarP rate ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP1_L3P_PH2         */		      10,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP2_L3P_PH2         */		      15,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_GAIN_AT_DP3_L3P_PH2         */		      20,	/* Comment [ Fast rate change P error gain, for Perror level 1 at BCP <10bar, Phase 2: when BCP rate < TarP rate resolution 10% ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_P_REF_1           */		      30,	/* Comment [ Reference BCP level 1 for RBC compensation ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_P_REF_2           */		      50,	/* Comment [ Reference BCP level 2 for RBC compensation ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_DR_RF0_PH1        */		     -10,	/* Comment [ Reference P rate error(BCP rate - TP rate) level 0, Phase 1 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_DR_RF1_PH1        */		      10,	/* Comment [ Reference P rate error(BCP rate - TP rate) level 1, Phase 1 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_DR_RF2_PH1        */		      20,	/* Comment [ Reference P rate error(BCP rate - TP rate) level 2, Phase 1 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR0_PH1        */		      15,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error <= Level 0, default 170% ] */
	                                                        		        	/* ScaleVal[      150 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR1_PH1        */		      13,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error Level 0~1, default 150% ] */
	                                                        		        	/* ScaleVal[      130 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR2_PH1        */		      13,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error Level 1~2, default 130%resolution 10% ] */
	                                                        		        	/* ScaleVal[      130 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR3_PH1        */		      12,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error > Level 2, default 100%r ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR1_PH1        */		      13,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error <= Level 1, default 140% ] */
	                                                        		        	/* ScaleVal[      130 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR2_PH1        */		      11,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error Level 1~2, default 120% ] */
	                                                        		        	/* ScaleVal[      110 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR3_PH1        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error > Level 2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR1_PH1        */		      11,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error <= Level 1, default 120% ] */
	                                                        		        	/* ScaleVal[      110 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR2_PH1        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error Level 1~2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR3_PH1        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error > Level 2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_DR_RF1_PH2        */		      10,	/* Comment [ Reference P rate error(BCP rate - TP rate) level 1, Phase 2 ] */
	/* int16_t     	S16_AV_FRC_RBC_GAIN_DR_RF2_PH2        */		      20,	/* Comment [ Reference P rate error(BCP rate - TP rate) level 2, Phase 2 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR1_PH2        */		      13,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error <= Level 1, default 150% ] */
	                                                        		        	/* ScaleVal[      130 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR2_PH2        */		      12,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error Level 1~2, default 130% ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L1P_DR3_PH2        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP <= Level 1, P rate error > Level 2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR1_PH2        */		      12,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error <= Level 1, default 140% ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR2_PH2        */		      11,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error Level 1~2, default 120% ] */
	                                                        		        	/* ScaleVal[      110 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L2P_DR3_PH2        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP Level 1~2, P rate error > Level 2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR1_PH2        */		      11,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error <= Level 1, default 120% ] */
	                                                        		        	/* ScaleVal[      110 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR2_PH2        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error Level 1~2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_AV_FRC_RBC_GAIN_L3P_DR3_PH2        */		      10,	/* Comment [ Fast rate change weighting for RBC, at BCP > Level 2, P rate error > Level 2, default 100% ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_H_PH1             */		     400,	/* Comment [ Lower  Limit of FRC Comp at Fast Rise in 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_L_PH1             */		      70,	/* Comment [ Higher Limit of FRC Comp at Fast Rise in 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_FAST_H_PH1            */		     800,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_FAST_L_PH1            */		     400,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_H_PH1             */		     400,	/* Comment [ Lower  Limit of FRC Comp at Nth cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_L_PH1             */		      70,	/* Comment [ Higher Limit of FRC Comp at Nth cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_QBRK_H_PH1            */		    1500,	/* Comment [ Higher Limit of FRC Comp at Quick Brake in 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_QBRK_L_PH1            */		     500,	/* Comment [ Lower  Limit of FRC Comp at Quick Brake in 1st cycle, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_H_PH1_LSPD        */		     150,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, Low speed, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_L_PH1_LSPD        */		      70,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, Low speed, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_H_PH1_LSPD        */		     100,	/* Comment [ Higher Limit of FRC Comp at Nth cycle, Low speed, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_L_PH1_LSPD        */		      50,	/* Comment [ Lower  Limit of FRC Comp at Nth cycle, Low speed, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1_H_PH1_LSPD_LP       */		     200,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, BCP<=3bar, Speed<10kph, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1_L_PH1_LSPD_LP       */		      70,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, BCP<=3bar, Speed<10kph, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_N_H_PH1_LSPD_LP       */		     150,	/* Comment [ Higher Limit of FRC Comp at Nth cycle, BCP<=3bar, Speed<10kph, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_N_L_PH1_LSPD_LP       */		      70,	/* Comment [ Lower  Limit of FRC Comp at Nth cycle, BCP<=3bar, Speed<10kph, Phase 1: BCP rate < TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_H_PH2             */		     200,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_L_PH2             */		      50,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_H_PH2             */		     150,	/* Comment [ Higher Limit of FRC Comp at Nth cycle, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_L_PH2             */		      50,	/* Comment [ Lower  Limit of FRC Comp at Nth cycle, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_H_PH2_LSPD        */		     100,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, Speed<10kph, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_L_PH2_LSPD        */		      50,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, Speed<10kph, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_H_PH2_LSPD        */		     100,	/* Comment [ Higher Limit of FRC Comp at Nth cycle, Speed<10kph, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_NTH_L_PH2_LSPD        */		      50,	/* Comment [ Lower  Limit of FRC Comp at Nth cycle, Speed<10kph, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_H_PH2_QBRK        */		    1000,	/* Comment [ Higher Limit of FRC Comp at 1st cycle, Quick Brake, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_COMP_1ST_L_PH2_QBRK        */		     400,	/* Comment [ Lower  Limit of FRC Comp at 1st cycle, Quick Brake, Phase 2: BCP rate >= TarP rate ] */
	/* int16_t     	S16_AV_FRC_ADDCOMP_BCP_RATE_REF       */		      80,	/* Comment [ Limit conditions for additional FRC compensation at low BCP: BCP rate ref ] */
	/* int16_t     	S16_AV_FRC_ADDCOMP_BCP_REF            */		     100,	/* Comment [ Limit conditions for additional FRC compensation at low BCP: BCP ref ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_AV_FRC_ADDCOMP_P_ERROR_REF        */		      10,	/* Comment [ Limit conditions for additional FRC compensation at low BCP: P error ref ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_DCT_PLEVEL_1               */		      50,	/* Comment [ Ref. Target P level_1 for FRC detection ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_DCT_PLEVEL_2               */		     150,	/* Comment [ Ref. Target P level_2 for FRC detection ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_DCT_PLEVEL_3               */		     300,	/* Comment [ Ref. Target P level_3 for FRC detection ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_DCT_PLEVEL_4               */		     600,	/* Comment [ Ref. Target P level_4 for FRC detection ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_ENT_THR_AT_L1P             */		       5,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 1 ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -20 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_ENT_THR_AT_L2P             */		       7,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 2 ] */
	                                                        		        	/* ScaleVal[    0.7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -20 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_ENT_THR_AT_L3P             */		      10,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 3 ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -20 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_ENT_THR_AT_L4P             */		      20,	/* Comment [ P error threshold(Tar_P-BCP) at target P level 4 ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -20 <= X <= 200 ] */
	/* int16_t     	S16_RV_FRC_GAIN_AT_DP0_PH2            */		      21,	/* Comment [ Release Valve FastRateChange Gain at Del Press_0 , BP_rate >=TP_DecRate ] */
	                                                        		        	/* ScaleVal[      105 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	S16_RV_FRC_GAIN_AT_DP1_PH2            */		      21,	/* Comment [ Release Valve FastRateChange Gain at Del Press_1 , BP_rate >=TP_DecRate ] */
	                                                        		        	/* ScaleVal[      105 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	S16_RV_FRC_GAIN_AT_DP2_PH2            */		      22,	/* Comment [ Release Valve FastRateChange Gain at Del Press_2 , BP_rate >=TP_DecRate ] */
	                                                        		        	/* ScaleVal[      110 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ -163840 <= X <= 163835 ] */
	/* int16_t     	S16_RV_FRC_GAIN_AT_PH1                */		      20,	/* Comment [ Release Valve FastRateChange Gain, BP_rate <TP_DecRate ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_RV_FRC_COMP_CURR_MAX              */		     100,	/* Comment [ Release Valve FastRateChange Current Max ] */
	/* int16_t     	S16_RV_FRC_COMP_CURR_MIN              */		      50,	/* Comment [ Release Valve FastRateChange Current Min ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF1                  */		     -10,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF2                  */		      50,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF3                  */		     200,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR1                   */		      10,	/* Comment [ Over-target enter P error threshold(TarP-BCP) at rate error level 1 ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR2                   */		     -10,	/* Comment [ Over-target enter P error threshold(TarP-BCP) at rate error level 2 ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR3                   */		     -30,	/* Comment [ Over-target enter P error threshold(TarP-BCP) at rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_1_AFRC_PH11       */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_2_AFRC_PH11       */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_3_AFRC_PH11       */		     200,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_1_AFRC_PH11        */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_2_AFRC_PH11        */		      20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_3_AFRC_PH11        */		      30,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_1_AFRC_PH12       */		    -150,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_2_AFRC_PH12       */		    -100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_3_AFRC_PH12       */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_1_AFRC_PH12        */		      30,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_2_AFRC_PH12        */		      20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_3_AFRC_PH12        */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_1_AFRC_PH2        */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_2_AFRC_PH2        */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF_3_AFRC_PH2        */		     150,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 2: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_1_AFRC_PH2         */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_2_AFRC_PH2         */		       0,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR_3_AFRC_PH2         */		     -10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_DR_RF1_LSPD                 */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1 ] */
	/* int16_t     	S16_AV_OT_DR_RF2_LSPD                 */		      50,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2 ] */
	/* int16_t     	S16_AV_OT_DR_RF3_LSPD                 */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3 ] */
	/* char8_t     	S8_AV_OT_TH_DR1_LSPD                  */		       5,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1 ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR2_LSPD                  */		     -10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2 ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR3_LSPD                  */		     -30,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_DR_RF1_AFRC_PH11_LSPD       */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF2_AFRC_PH11_LSPD       */		      50,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF3_AFRC_PH11_LSPD       */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 1-1: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_TH_DR1_AFRC_PH11_LSPD        */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR2_AFRC_PH11_LSPD        */		      20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR3_AFRC_PH11_LSPD        */		      30,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 1-1: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_DR_RF1_AFRC_PH12_LSPD       */		    -150,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF2_AFRC_PH12_LSPD       */		    -100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF3_AFRC_PH12_LSPD       */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 1-2: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_TH_DR1_AFRC_PH12_LSPD        */		      30,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR2_AFRC_PH12_LSPD        */		      20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR3_AFRC_PH12_LSPD        */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 1-2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_DR_RF1_AFRC_PH2_LSPD        */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1. Phase 2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF2_AFRC_PH2_LSPD        */		      50,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2. Phase 2: Target P Increasing, BCP Increasing ] */
	/* int16_t     	S16_AV_OT_DR_RF3_AFRC_PH2_LSPD        */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3. Phase 2: Target P Increasing, BCP Increasing ] */
	/* char8_t     	S8_AV_OT_TH_DR1_AFRC_PH2_LSPD         */		      10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR2_AFRC_PH2_LSPD         */		       0,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR3_AFRC_PH2_LSPD         */		     -10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3. Phase 2: Target P Increasing, BCP Increasing ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_DR_RF1_LOWP_LSPD            */		       0,	/* Comment [ At Low Press(Below 20bar), Reference P rate error(BCP rate - TarP rate) level 1 ] */
	/* int16_t     	S16_AV_OT_DR_RF2_LOWP_LSPD            */		      30,	/* Comment [ At Low Press(Below 20bar), Reference P rate error(BCP rate - TarP rate) level 2 ] */
	/* int16_t     	S16_AV_OT_DR_RF3_LOWP_LSPD            */		     100,	/* Comment [ At Low Press(Below 20bar), Reference P rate error(BCP rate - TarP rate) level 3 ] */
	/* char8_t     	S8_AV_OT_TH_DR1_LOWP_LSPD             */		      -5,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at Low Press(Below 20bar),  rate error level 1 ] */
	                                                        		        	/* ScaleVal[   -0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR2_LOWP_LSPD             */		     -10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at Low Press(Below 20bar),  rate error level 2 ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_TH_DR3_LOWP_LSPD             */		     -20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at Low Press(Below 20bar),  rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF1_STOP             */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1, at standstill ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF2_STOP             */		      20,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2, at standstill ] */
	/* int16_t     	S16_AV_OT_ENT_DR_RF3_STOP             */		     100,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3, at standstill ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR1_STOP              */		      -5,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 1, at stanstill ] */
	                                                        		        	/* ScaleVal[   -0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR2_STOP              */		     -10,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 2, at stanstill ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_AV_OT_ENT_TH_DR3_STOP              */		     -20,	/* Comment [ Over-target enter P error(BCP-TarP) threshold at rate error level 3, at stanstill ] */
	                                                        		        	/* ScaleVal[     -2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DP_LEVEL1          */		     -10,	/* Comment [ Reference P error(BCP-TarP) level 1, for over-target compensation ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DP_LEVEL2          */		      10,	/* Comment [ Reference P error(BCP-TarP) level 2, for over-target compensation ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DP_LEVEL3          */		      30,	/* Comment [ Reference P error(BCP-TarP) level 3, for over-target compensation ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L1DP_1       */		       0,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 1, for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L1DP_2       */		      50,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 2, for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L1DP_3       */		     200,	/* Comment [ Reference P rate error(BCP rate - TarP rate) level 3, for over-target comp. ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L1_DR1            */		       1,	/* Comment [ Over-target comp. current % at Rate error level 1, P error < level 1 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L1_DR2            */		       2,	/* Comment [ Over-target comp. current % at Rate error level 2, P error < level 1 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L1_DR3            */		       2,	/* Comment [ Over-target comp. current % at Rate error level 3, P error < level 1 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L2DP_1       */		       0,	/* Comment [ Reference P rate error level 1(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L2DP_2       */		      50,	/* Comment [ Reference P rate error level 2(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L2DP_3       */		     200,	/* Comment [ Reference P rate error level 3(BCP rate - TarP rate), for over-target comp. ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L2_DR1            */		       1,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L2_DR2            */		       2,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L2_DR3            */		       3,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3DP_1       */		       0,	/* Comment [ Reference P rate error level 1(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3DP_2       */		      60,	/* Comment [ Reference P rate error level 2(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3DP_3       */		     200,	/* Comment [ Reference P rate error level 3(BCP rate - TarP rate), for over-target comp. ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3_DR1            */		       1,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3_DR2            */		       3,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3_DR3            */		       4,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3M_1        */		     -50,	/* Comment [ Reference P rate error level 1(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3M_2        */		       0,	/* Comment [ Reference P rate error level 2(BCP rate - TarP rate), for over-target comp. ] */
	/* int16_t     	S16_AV_OT_COMP_REF_DRATE_L3M_3        */		      50,	/* Comment [ Reference P rate error level 3(BCP rate - TarP rate), for over-target comp. ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3M_DR1           */		       2,	/* Comment [ Over-target comp. current % at Rate error 1, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3M_DR2           */		       3,	/* Comment [ Over-target comp. current % at Rate error 2, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_AV_OT_COMP_RATIO_L3M_DR3           */		       4,	/* Comment [ Over-target comp. current % at Rate error 3, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_AV_OT_ADD_LIM_1ST_SCAN            */		      80,	/* Comment [ Additional current limit for 1st scan of over-target compensation, target P above 10bar ] */
	/* int16_t     	S16_AV_OT_ADD_LIM_1ST_SCAN_LOWP       */		      40,	/* Comment [ Additional current limit for 1st scan of over-target compensation, target P below 10bar ] */
	/* int16_t     	S16_AV_OT_COMP_CURR_MAX               */		     400,	/* Comment [ Max over-target comp. current ] */
	/* int16_t     	S16_AV_OT_COMP_CURR_MIN               */		      50,	/* Comment [ Min over-target comp. current at normal conditions ] */
	/* int16_t     	S16_AV_OT_COMP_CURR_MIN_RBC           */		      50,	/* Comment [ Min over-target comp. current during RBC ] */
	/* char8_t     	S8_AV_OT_COMP_CREEPSPD_GAIN           */		      15,	/* Comment [ Creeping speed에서 AV OverTargetHold Comp. Gain(Noise 저감) ] */
	                                                        		        	/* ScaleVal[       75 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	S8_AV_OT_COMP_STANDSTILL_GAIN         */		      15,	/* Comment [ 정차 중 AV OverTargetHold Comp. Gain(Noise 저감) ] */
	                                                        		        	/* ScaleVal[       75 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF1                  */		       0,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 1 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF2                  */		      50,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 2 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF3                  */		     150,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 3 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR1                   */		       0,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 1 ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR2                   */		     -30,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 2 ] */
	                                                        		        	/* ScaleVal[     -3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR3                   */		     -50,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_RV_OT_DR_RF1_LSPD                 */		       0,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 1 ] */
	/* int16_t     	S16_RV_OT_DR_RF2_LSPD                 */		      20,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 2 ] */
	/* int16_t     	S16_RV_OT_DR_RF3_LSPD                 */		     100,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 3 ] */
	/* char8_t     	S8_RV_OT_TH_DR1_LSPD                  */		      10,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 1 ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_TH_DR2_LSPD                  */		       0,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 2 ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_TH_DR3_LSPD                  */		     -10,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF1_STOP             */		       0,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 1 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF2_STOP             */		      20,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 2 ] */
	/* int16_t     	S16_RV_OT_ENT_DR_RF3_STOP             */		     100,	/* Comment [ Reference P rate error(TarP rate - BCP rate) level 3 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR1_STOP              */		      -5,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 1 ] */
	                                                        		        	/* ScaleVal[   -0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR2_STOP              */		     -10,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 2 ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_RV_OT_ENT_TH_DR3_STOP              */		     -20,	/* Comment [ Over-target enter P error(TarP-BCP)threshold at rate error level 3 ] */
	                                                        		        	/* ScaleVal[     -2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DP_LEVEL1          */		     -10,	/* Comment [ Reference P error(TarP-BCP) level 1, for over-target compensation ] */
	                                                        		        	/* ScaleVal[     -1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DP_LEVEL2          */		       0,	/* Comment [ Reference P error(TarP-BCP) level 2, for over-target compensation ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DP_LEVEL3          */		      30,	/* Comment [ Reference P error(TarP-BCP) level 3, for over-target compensation ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -200 <= X <= 200 ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L1_1        */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L1_2        */		     200,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L1_3        */		     400,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L1_DR1           */		       1,	/* Comment [ Over-target comp. current % at Rate error level 1, P error < level 1 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L1_DR2           */		       1,	/* Comment [ Over-target comp. current % at Rate error level 2, P error < level 1 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L1_DR3           */		       2,	/* Comment [ Over-target comp. current % at Rate error level 3, P error < level 1 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L2_1        */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L2_2        */		     200,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L2_3        */		     400,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L2_DR1           */		       1,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L2_DR2           */		       1,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L2_DR3           */		       2,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3_1        */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3_2        */		     200,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3_3        */		     400,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3_DR1           */		       1,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3_DR2           */		       2,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3_DR3           */		       3,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3M_1       */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3M_2       */		     200,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_1RV_OT_COMP_REF_DRATE_L3M_3       */		     400,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3M_DR1          */		       3,	/* Comment [ Over-target comp. current % at Rate error 1, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3M_DR2          */		       4,	/* Comment [ Over-target comp. current % at Rate error 2, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_1RV_OT_COMP_RATIO_L3M_DR3          */		       5,	/* Comment [ Over-target comp. current % at Rate error 3, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       25 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_1RV_OT_COMP_CURR_MAX              */		     200,	/* Comment [ Max over-target comp. current, during 1 vv control ] */
	/* int16_t     	S16_1RV_OT_COMP_CURR_MIN              */		      30,	/* Comment [ Min over-target comp. current at normal conditions, during 1 vv control ] */
	/* int16_t     	S16_1RV_OT_COMP_CURR_MIN_RBC          */		      50,	/* Comment [ Min over-target comp. current during RBC and 1 vv control ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L1_1         */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L1_2         */		      10,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L1_3         */		     200,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L1_DR1            */		       1,	/* Comment [ Over-target comp. current % at Rate error level 1, P error < level 1 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L1_DR2            */		       2,	/* Comment [ Over-target comp. current % at Rate error level 2, P error < level 1 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L1_DR3            */		       3,	/* Comment [ Over-target comp. current % at Rate error level 3, P error < level 1 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L2_1         */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L2_2         */		      10,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L2_3         */		     200,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L2_DR1            */		       1,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L2_DR2            */		       2,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L2_DR3            */		       4,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 1~2 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3_1         */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3_2         */		      20,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3_3         */		     200,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3_DR1            */		       2,	/* Comment [ Over-target comp. current % at Rate error 1, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3_DR2            */		       4,	/* Comment [ Over-target comp. current % at Rate error 2, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3_DR3            */		       5,	/* Comment [ Over-target comp. current % at Rate error 3, P error level 2~3 ] */
	                                                        		        	/* ScaleVal[       25 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3M_1        */		       0,	/* Comment [ Reference P rate error level 1(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3M_2        */		     200,	/* Comment [ Reference P rate error level 2(TarP rate - BCP rate), for over-target comp. ] */
	/* int16_t     	S16_RV_OT_COMP_REF_DRATE_L3M_3        */		     400,	/* Comment [ Reference P rate error level 3(TarP rate - BCP rate), for over-target comp. ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3M_DR1           */		       3,	/* Comment [ Over-target comp. current % at Rate error 1, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3M_DR2           */		       5,	/* Comment [ Over-target comp. current % at Rate error 2, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       25 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* uchar8_t    	U8_RV_OT_COMP_RATIO_L3M_DR3           */		       6,	/* Comment [ Over-target comp. current % at Rate error 3, P error > level 3 ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 750 ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MAX               */		     200,	/* Comment [ Max over-target comp. current, during 1 vv control ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MAX_L1P           */		     400,	/* Comment [ Pressure Level 1에서 Release Valve OverTarget_Current_MAX값 ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MAX_L2P           */		     300,	/* Comment [ Pressure Level 2에서 Release Valve OverTarget_Current_MAX값 ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MAX_L3P           */		     150,	/* Comment [ Pressure Level 3에서 Release Valve OverTarget_Current_MAX값 ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MIN               */		      30,	/* Comment [ Min over-target comp. current at normal conditions, during 1 vv control ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MIN_RBC           */		      30,	/* Comment [ Min over-target comp. current during RBC and 1 vv control ] */
	/* int16_t     	S16_RV_OT_COMP_CURR_MIN_RBC_HS        */		      50,	/* Comment [ Min over-target comp. current during RBC, at high speed ] */
	/* int16_t     	S16_RV_OT_COMP_MAX_IN_ESC_LFC         */		     300,	/* Comment [ Esc Rear 제어에서 Max over-target Comp ] */
	/* int16_t     	S16_RV_OT_PRESS_LEVEL_1               */		     100,	/* Comment [ Release Valve OverTarget_Current_MAX 값 설정 기준 Pressure Level 1 ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_OT_PRESS_LEVEL_2               */		     800,	/* Comment [ Release Valve OverTarget_Current_MAX 값 설정 기준 Pressure Level 2 ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_OT_PRESS_LEVEL_3               */		    1200,	/* Comment [ Release Valve OverTarget_Current_MAX 값 설정 기준 Pressure Level 3 ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RV_OT_MAINTAIN_COMP_CURR_T1       */		     -50,	/* Comment [ Release Valve OverTarget 이 T1(100ms)이상 유지 될때 Current Comp 량, ] */
	/* int16_t     	S16_RV_OT_MAINTAIN_COMP_CURR_T2       */		    -100,	/* Comment [ Release Valve OverTarget 이 T2(300ms)이상 유지 될때 Current Comp 량, ] */
	/* int16_t     	S16_RV_OT_MAINTAIN_COMP_CURR_T3       */		    -150,	/* Comment [ Release Valve OverTarget 이 T3(600ms)이상 유지 될때 Current Comp 량, ] */
	/* int16_t     	S16_RV_OT_1ST_SCAN_ADD_GAIN           */		       2,	/* Comment [ Release Valve OverTargetHold Gain, at Fisrt Scan ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16_RV_OT_1ST_SCAN_GAIN_MAX           */		      20,	/* Comment [ Release Valve OverTargetHold Gain MAX, at Fisrt Scan ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16_RV_OT_1ST_SCAN_GAIN_MIN           */		      17,	/* Comment [ Release Valve OverTargetHold Gain Min, at Fisrt Scan ] */
	                                                        		        	/* ScaleVal[       85 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	S8_RV_OT_COMP_CREEPSPD_GAIN           */		      17,	/* Comment [ Creeping speed에서 RV OverTargetHold Comp. Gain(Noise 저감) ] */
	                                                        		        	/* ScaleVal[       85 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	S8_RV_OT_COMP_STANDSTILL_GAIN         */		      12,	/* Comment [ 정차 중 RV OverTargetHold Comp. Gain(Noise 저감) ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* char8_t     	S8_RV_OT_COMP_STANDSTILL_LP_GAIN      */		      12,	/* Comment [ 정차 중 Low Pressure 에서 RV OverTargetHold Comp. Gain(Noise 저감) ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	S16_RV_FHC_COMP_GAIN                  */		       9,	/* Comment [ Release Valve ForcedHoldComp에서 Gain ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_RV_NFH_COMP_GAIN                  */		       9,	/* Comment [ Release Valve NeedForcedHold에서 Gain ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_APPLY_HOLD_TIME                    */		       4,	/* Comment [ AV current 유지 시간 ] */
	/* uchar8_t    	U8_APPLY_HOLD_TIME_ABS                */		       3,	/* Comment [ AV current 유지 최대 허용 간 ] */
	/* uchar8_t    	U8_APPLY_HOLD_TIME_RBC                */		       2,	/* Comment [ RBC 제어 중 AV current 유지 시간 ] */
	/* uchar8_t    	U8_RELEASE_HOLD_TIME                  */		       3,	/* Comment [ RV current 유지 시간 ] */
	/* uchar8_t    	U8_RELEASE_HOLD_TIME_MAX              */		       3,	/* Comment [ RV current 유지 최대 허용 간 ] */
	/* uchar8_t    	U8_RELEASE_HOLD_TIME_RBC              */		       2,	/* Comment [ RBC 제어 중 RV current 유지 시간 ] */
	/* int16_t     	S16_AV_MODE_CHANGE_COMP_MAX           */		      50,	/* Comment [ 저속(5kph)에서 Apply -> Release 모드 전환 초기에 밸브 안정화를 위한  AV current 보상량 ] */
	/* int16_t     	S16_AV_MODE_CHANGE_COMP_MIN           */		      20,	/* Comment [ 중속이상(20kph이상)에서 Apply -> Release 모드 전환 초기에 밸브 안정화를 위한  AV current 보상량 ] */
	/* int16_t     	S16_RV_MODE_CHANGE_COMP_MAX           */		      30,	/* Comment [ 저속(5kph)에서 Apply -> Release 모드 전환 초기에 밸브 안정화를 위한  RV current 보상량 ] */
	/* int16_t     	S16_RV_MODE_CHANGE_COMP_MIN           */		      25,	/* Comment [ 중속이상(20kph이상)에서 Apply -> Release 모드 전환 초기에 밸브 안정화를 위한  RV current 보상량 ] */
	/* uint16_t    	U16_AV_CURRENT_COMP_DIFF_MAX          */		      30,	/* Comment [ Max AV comp current 변화량 ] */
	/* uint16_t    	U16_AV_CURRENT_DIFF_MAX               */		     100,	/* Comment [ Max AV current 변화량 ] */
	/* uint16_t    	U16_AV_CURRENT_DIFF_MAX_ABS           */		     500,	/* Comment [ In-ABS 제어모드 Nth cycle에서의 Max AV current 변화량 ] */
	/* uint16_t    	U16_AV_CURRENT_DIFF_MAX_ABS_1ST       */		     500,	/* Comment [ In-ABS 제어모드 1st cycle에서의 Max AV current 변화량 ] */
	/* uint16_t    	U16_AV_CURRENT_MAX                    */		    2250,	/* Comment [ Max AV current ] */
	/* uint16_t    	U16_AV_CURRENT_MAX_IN_ABS             */		    2250,	/* Comment [ In-ABS 제어모드에서 Max AV current ] */
	/* uint16_t    	U16_AV_CURRENT_MIN                    */		     500,	/* Comment [ Min AV current ] */
	/* uint16_t    	U16_AV_CURRENT_MIN_IN_ABS             */		    1000,	/* Comment [ In-ABS 제어모드에서 Min AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MAX_IN_ABS        */		    2000,	/* Comment [ In-ABS 제어모드 1st cycle에서의 Max AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MAX_IN_ABSH       */		    2250,	/* Comment [ In-ABS 제어모드 High Pressure(80bar) 1st cycle에서의 Max AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MAX_IN_ABSL       */		    1700,	/* Comment [ In-ABS 제어모드 Low Pressure(50bar) 1st cycle에서의 Max AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MIN_IN_ABS        */		    1000,	/* Comment [ In-ABS 제어모드 1st cycle에서의 Min AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MIN_IN_ABSH       */		    1500,	/* Comment [ In-ABS 제어모드 High Pressure(80bar) 1st cycle에서의 Min AV current ] */
	/* uint16_t    	U16_INIT_AV_CURRENT_MIN_IN_ABSL       */		    1000,	/* Comment [ In-ABS 제어모드 Low Pressure(50bar) 1st cycle에서의 Min AV current ] */
	/* uint16_t    	U16_INIT_RV_CURRENT_MAX_IN_ABS        */		    1000,	/* Comment [ In-ABS 제어모드 1st cycle에서 Release valve Current 최대값 ] */
	/* uint16_t    	U16_INIT_RV_CURRENT_MIN_IN_ABS        */		     250,	/* Comment [ In-ABS 제어모드 1st cycle에서 Release valve Current 최소값 ] */
	/* uint16_t    	U16_RV_CURRENT_DIFF_MAX               */		      50,	/* Comment [ Release valve Current DIFF Max 값 ] */
	/* uint16_t    	U16_RV_CURRENT_MAX                    */		     900,	/* Comment [ Release valve Current 최대값 ] */
	/* uint16_t    	U16_RV_CURRENT_MAX_IN_ABS             */		    1000,	/* Comment [ In-ABS 제어모드에서 Release valve Current 최대값 ] */
	/* uint16_t    	U16_RV_CURRENT_MIN                    */		     100,	/* Comment [ Release valve Current 최소값 ] */
	/* uint16_t    	U16_RV_CURRENT_MIN_IN_ABS             */		     250,	/* Comment [ In-ABS 제어모드에서 Release valve Current 최소값 ] */
	/* uint16_t    	U16_RV_INIT_FADEIN_CURR_LSPD          */		     600,	/* Comment [ Low speed Release Valve의 Fade in current(Gen1 RV NO Type용) ] */
	/* uint16_t    	U16_RV_INIT_FADEIN_CURR_ON_STOP       */		     600,	/* Comment [ 정차 중  Release Valve의 Fade in current(Gen1 RV NO Type용) ] */
	/* uint16_t    	U16_RV_INIT_FADEIN_TIME_LSPD          */		       8,	/* Comment [ Low speed Release Valve의 Fade in time(Gen1 RV NO Type용) ] */
	/* uint16_t    	U16_RV_INIT_FADEIN_TIME_ON_STOP       */		       8,	/* Comment [ 정차 중 Release Valve의 Fade in time(Gen1 RV NO Type용) ] */
	/* int16_t     	S16_FADE_OUT_CURRENT_DIFF             */		      30,	/* Comment [ Normal Mode에서  Fadeout 시 Current DIFF 값 (기울기 mA/1scan) ] */
	/* int16_t     	S16_FADE_OUT_CURRENT_INIT             */		     500,	/* Comment [ Normal Mode에서  Fadeout 시작 시 Initial Current ] */
	/* int16_t     	S16_FADE_OUT_CURRENT_MIN              */		     200,	/* Comment [ Normal Mode에서  Fadeout 시 Current 최소값 (Min_Current 이후에 0으로 Reset) ] */
	/* int16_t     	S16_RV_FADE_OUT_CURRENT_INIT          */		     800,	/* Comment [ Curcuit Pressure 잔압 해소 이후에 Release Valve  Fadeout 시작 하는 Initial Current ] */
	/* int16_t     	S16_RV_FADE_OUT_CURRENT_MIN           */		     600,	/* Comment [ Curcuit Pressure 잔압 해소 이후에 Release Valve  종료 Current ] */
	/* int16_t     	S16_RV_FADE_OUT_RATE                  */		       1,	/* Comment [ Release Valve Fadeout Current 변화량 ] */
	/* int16_t     	S16_RV_FM_FADE_OUT_RATE               */		      10,	/* Comment [ Release Valve Fadeout Current 변화량 in Fail State ] */
	/* uint16_t    	U16_AV_FADE_OUT_CURRENT_DIFF          */		     200,	/* Comment [ Apply Valve Fadeout 시 Current DIFF 값 ] */
	/* uint16_t    	U16_AV_FADE_OUT_CURRENT_INIT          */		     400,	/* Comment [ Apply Valve Fadeout 시작 시 Initial Current ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_01       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_01 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_02       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_02 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_03       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_03 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_04       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_04 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_05       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_05 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_06       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_06 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_07       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_07 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_08       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_08 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_09       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_09 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_10       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_10 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_11       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_11 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_12       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_12 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_13       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_13 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_14       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_14 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_15       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_15 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_16       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_16 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_17       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_17 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_18       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_18 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_19       */		       0,	/* Comment [ AHB PCtrl Temp Parameter_19 ] */
	/* int16_t     	S16_AHB_PCTRL_LOGIC_CAL_TEMP_20       */		       0	/* Comment [ AHB PCtrl Temp Parameter_20 ] */
};

#define LOGIC_CAL_MODULE_31_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

