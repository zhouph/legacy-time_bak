  #ifndef __ENG_VAR_STRUCT_SETTING_H__
  #define __ENG_VAR_STRUCT_SETTING_H__

/*includes**************************************************/







/*Global MACRO CONSTANT Definition******************************************************/

    #define S16_AUTO_TM_ID      1
    #define S16_MANUAL_TM_ID    2
    #define S16_DCT_TM_ID       3
    #define S16_CVT_TM_ID       4









 /* ---- Settings per vehicle ----- */

  #define __ENG_VAR_01_DEFINED    /*default */
/* KYB_SILS */
//  #define __ENG_VAR_02_DEFINED
//  #define __ENG_VAR_03_DEFINED
//  #define __ENG_VAR_04_DEFINED
//  #define __ENG_VAR_05_DEFINED
//  #define __ENG_VAR_06_DEFINED
//  #define __ENG_VAR_07_DEFINED
//  #define __ENG_VAR_08_DEFINED
//  #define __ENG_VAR_09_DEFINED
//  #define __ENG_VAR_10_DEFINED
//  #define __ENG_VAR_11_DEFINED
//  #define __ENG_VAR_12_DEFINED

  #define __ENG_VAR_PER_VOL
  #define __ENG_VAR_PER_FUEL_TYPE
  #define __ENG_VAR_PER_MAX_TQ
  #define __ENG_VAR_PER_TM_TYPE
/*  #define __ENG_VAR_PER_DRV_MODE    */
/*  #define __ENG_VAR_PER_TCS_SWITCH  */
/*  #define __ENG_VAR_PER_VEHICLE_CODE*/
/*  #define __ENG_VAR_PER_EPS         */

  #if defined (__ENG_VAR_PER_TM_TYPE)
      /* #define __ENG_VAR_DCT_TM_TYPE      */
      /* #define __ENG_VAR_CVT_TM_TYPE      */
  #endif

  #if defined (__ENG_VAR_PER_DRV_MODE)
    /*#define __SAME_ENG_VAR_FOR_DM_AUTO_N_4H*/
  #endif

/*Global Type Declaration *******************************************************************/

    typedef struct
    {
int16_t      s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId;

    } ENG_CAL_HMC_VAR_OPTION_t;


    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption01 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           1               ,         0     ,        24,            4,  S16_AUTO_TM_ID,         33,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption02 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           2               ,         0     ,        24,            4,  S16_MANUAL_TM_ID,         33,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption03 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           3               ,         0     ,        24,            4,  S16_AUTO_TM_ID,         34,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption04 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           4               ,         0     ,        24,            4,  S16_MANUAL_TM_ID,         34,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption05 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           5               ,         0     ,        30,            5,  S16_AUTO_TM_ID,         37,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption06 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           6               ,         0     ,        30,            5,  S16_MANUAL_TM_ID,         37,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption07 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           7               ,         0     ,        30,            4,  S16_AUTO_TM_ID,         37,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption08 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           8               ,         0     ,        30,            4,  S16_MANUAL_TM_ID,         37,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption09 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           9               ,         0     ,        30,            4,  S16_AUTO_TM_ID,         40,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption10 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           10               ,         0     ,        30,            4,  S16_MANUAL_TM_ID,         40,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption11 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           11               ,         0     ,        33,            4,  S16_AUTO_TM_ID,         42,       0,            0
    };
    const ENG_CAL_HMC_VAR_OPTION_t EngVarOption12 =
    {
        /* s16EngCalStructId, s16DrvModeId, s16EngVolId, s16FuelTypeId, s16TmTypeId, s16MaxTqId, s16VehCodeId, s16VehEpsId; */
           12               ,         0     ,        33,            4,  S16_MANUAL_TM_ID,         42,       0,            0
    };

/*Global Extern Variable  Declaration*****************************************/


  #endif /* __ENG_VAR_STRUCT_SETTING_H__ */
