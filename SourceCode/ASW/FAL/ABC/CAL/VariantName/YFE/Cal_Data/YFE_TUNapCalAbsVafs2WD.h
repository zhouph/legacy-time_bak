#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_22_START


const /*far*/	DATA_APCALABSVAFS_t	apCalAbsVafs2WD =
{
		{
	/* uint16_t    	apCalAbsVafsInfo.CDID                 */		0x0000,
	/* uint16_t    	apCalAbsVafsInfo.CDVer                */		0x0000,
	/* uchar8_t    	apCalAbsVafsInfo.MID                  */		0x00,
		},
	/* uchar8_t    	U8_EDC_SYS_ENABLE                     */		       1,	/* Comment [ EDC Enable/Disable ] */
	/* uint16_t    	U16_EDC_SPEED1                        */		     200,	/* Comment [ EDC 제어시 적용되는 저속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_EDC_SPEED2                        */		     400,	/* Comment [ EDC 제어시 적용되는 중속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     50 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_EDC_SPEED3                        */		     600,	/* Comment [ EDC 제어시 적용되는 고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     75 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_EDC_SPEED4                        */		     800,	/* Comment [ EDC 제어시 적용되는 초고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[    100 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_S1                  */		     -32,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B(1.5G) 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_S2                  */		     -32,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B(1.5G) 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_S3                  */		     -32,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B(1.5G) 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_S4                  */		     -32,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B(1.5G) 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_S1                  */		     -40,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_S2                  */		     -40,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_S3                  */		     -40,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_S4                  */		     -40,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_S1                  */		     -48,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_S2                  */		     -48,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되지 않았을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_S3                  */		     -48,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되지 않았을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_S4                  */		     -48,	/* Comment [ ABS/ESC 제어 중이 아닐 때, -B 감지 되지 않았을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_EDC_ENT_THR_DEL_YAW                */		       0,	/* Comment [ EDC entrance threshold of delta yaw (ESC system only) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ABS_S1              */		     -32,	/* Comment [ ABS 제어 중일 때, -B(1.5G) 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ABS_S2              */		     -32,	/* Comment [ ABS 제어 중일 때, -B(1.5G) 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ABS_S3              */		     -32,	/* Comment [ ABS 제어 중일 때, -B(1.5G) 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ABS_S4              */		     -32,	/* Comment [ ABS 제어 중일 때, -B(1.5G) 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ABS_S1              */		     -40,	/* Comment [ ABS 제어 중일 때, -B(0.5G) 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ABS_S2              */		     -40,	/* Comment [ ABS 제어 중일 때, -B(0.5G) 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ABS_S3              */		     -40,	/* Comment [ ABS 제어 중일 때, -B(0.5G) 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ABS_S4              */		     -40,	/* Comment [ ABS 제어 중일 때, -B(0.5G) 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ABS_S1              */		     -48,	/* Comment [ ABS 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ABS_S2              */		     -48,	/* Comment [ ABS 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ABS_S3              */		     -48,	/* Comment [ ABS 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ABS_S4              */		     -48,	/* Comment [ ABS 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ESC_S1              */		     -32,	/* Comment [ ESC 제어 중일 때, -B(1.5G) 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ESC_S2              */		     -32,	/* Comment [ ESC 제어 중일 때, -B(1.5G) 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ESC_S3              */		     -32,	/* Comment [ ESC 제어 중일 때, -B(1.5G) 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR0_ESC_S4              */		     -32,	/* Comment [ ESC 제어 중일 때, -B(1.5G) 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ESC_S1              */		     -40,	/* Comment [ ESC 제어 중일 때, -B(0.5G) 감지 되었을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ESC_S2              */		     -40,	/* Comment [ ESC 제어 중일 때, -B(0.5G) 감지 되었을 때, 중속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ESC_S3              */		     -40,	/* Comment [ ESC 제어 중일 때, -B(0.5G) 감지 되었을 때, 고속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR1_ESC_S4              */		     -40,	/* Comment [ ESC 제어 중일 때, -B(0.5G) 감지 되었을 때, 초고속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ESC_S1              */		     -48,	/* Comment [ ESC 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED1)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ESC_S2              */		     -48,	/* Comment [ ESC 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED2)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ESC_S3              */		     -48,	/* Comment [ ESC 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED3)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENTRY_THR2_ESC_S4              */		     -48,	/* Comment [ ESC 제어 중일 때, -B 감지 되지 않았을 때, 저속 영역(U16_EDC_SPEED4)에서의 EDC 제어 진입을 위한 Delta V Threshold ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_DEC_ENT_THR_INC_IN_CTL         */		      32,	/* Comment [ EDC 제어 끝나고, 1sec안에, 재슬립 발생시 EDC 제어 진입 민감을 위한 Delta V 추가량 (각 상황별 Threshold + S8_EDC_DEC_ENT_THR_INC_IN_CTL) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_DEC_ENT_THR_IN_RR              */		     -16,	/* Comment [ Rough Road 감지시, EDC 제어 진입 둔감을 위한 Delta V 추가량 (각 상황별 Threshold + S8_EDC_DEC_ENT_THR_IN_RR) ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_ENT_THR_REL_LAM                */		      -8,	/* Comment [ EDC entrance threshold of relative slip at no acceleration condition(-b) detection ] */
	/* char8_t     	S8_EDC_ENT_THR_REL_LAM_AT_B_DCT       */		      -7,	/* Comment [ EDC entrance threshold of relative slip at acceleration condition(-b) detection ] */
	/* char8_t     	S8_EDC_EXIT_RELLAM_THR                */		      -2,	/* Comment [ EDC 제어종료를 위한 슬립률 threshold값 설정 (이 슬립률 이상이면 제어 종료모드로 전환) ] */
	/* char8_t     	S8_EDC_EXIT_RELLAM_THR_3G0            */		      -5,	/* Comment [ 휠가속도가 3G이상으로 휠이 빠르게 살아 날 때, EDC 제어종료를 위한 RELLAM threshold값 설정 (이 슬립률 이상이면 제어 종료모드로 전환) ] */
	/* char8_t     	S8_EDC_EXIT_SPEED_THR                 */		      -4,	/* Comment [ EDC 제어종료를 위한 속도 threshold값 설정 (lcedcs16TargetWheelSlip + S8_EDC_EXIT_SPEED_THR 속도이상이면 제어 종료모드로 전환) ] */
	                                                        		        	/* ScaleVal[   -0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_EXIT_SPEED_THR_3G0             */		     -24,	/* Comment [ 휠가속도가 3G이상으로 휠이 빠르게 살아 날 때, EDC 제어종료를 위한 속도 threshold값 설정 (lcedcs16TargetWheelSlip + S8_EDC_EXIT_SPEED_THR_3G0 속도이상이면 제어 종료모드로 전환) ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_S1              */		     -16,	/* Comment [ ABS/ESC 제어 중이 아닐 때, 저속 영역(U16_EDC_SPEED1)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_S2              */		     -16,	/* Comment [ ABS/ESC 제어 중이 아닐 때, 중속 영역(U16_EDC_SPEED2)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_S3              */		     -16,	/* Comment [ ABS/ESC 제어 중이 아닐 때, 고속 영역(U16_EDC_SPEED3)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_S4              */		     -16,	/* Comment [ ABS/ESC 제어 중이 아닐 때, 초고속 영역(U16_EDC_SPEED4)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ABS_S1          */		     -16,	/* Comment [ ABS 제어 중일 때, 저속 영역(U16_EDC_SPEED1)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ABS_S2          */		     -16,	/* Comment [ ABS 제어 중일 때, 중속 영역(U16_EDC_SPEED2)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ABS_S3          */		     -16,	/* Comment [ ABS 제어 중일 때, 고속 영역(U16_EDC_SPEED3)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ABS_S4          */		     -16,	/* Comment [ ABS 제어 중일 때, 초고속 영역(U16_EDC_SPEED4)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ESC_S1          */		     -16,	/* Comment [ ESC 제어 중일 때, 저속 영역(U16_EDC_SPEED1)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ESC_S2          */		     -16,	/* Comment [ ESC 제어 중일 때, 중속 영역(U16_EDC_SPEED2)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ESC_S3          */		     -16,	/* Comment [ ESC 제어 중일 때, 고속 영역(U16_EDC_SPEED3)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_TARGET_DELTA_V_ESC_S4          */		     -16,	/* Comment [ ESC 제어 중일 때, 초고속 영역(U16_EDC_SPEED4)에서의 목표 슬립량 ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_DEC_TAR_DEL_V_IN_RR            */		      -8,	/* Comment [ Rough Road 감지시, EDC 제어 둔감을 위한 목표 슬립 추가량 (각 상황별 Target Delta V + S8_EDC_DEC_TAR_DEL_V_IN_RR) ] */
	                                                        		        	/* ScaleVal[     -1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_S1               */		      20,	/* Comment [ PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_S2               */		      30,	/* Comment [ PHASE 1이고, 중속 영역(U16_EDC_SPEED2)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_S3               */		      40,	/* Comment [ PHASE 1이고, 고속 영역(U16_EDC_SPEED3)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_S4               */		      50,	/* Comment [ PHASE 1이고, 초고속 영역(U16_EDC_SPEED4)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_S1               */		      30,	/* Comment [ PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_S2               */		      40,	/* Comment [ PHASE 1이고, 중속 영역(U16_EDC_SPEED2)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_S3               */		      50,	/* Comment [ PHASE 1이고, 고속 영역(U16_EDC_SPEED3)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_S4               */		      60,	/* Comment [ PHASE 1이고, 초고속 영역(U16_EDC_SPEED4)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_S1               */		      50,	/* Comment [ PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_S2               */		      60,	/* Comment [ PHASE 2이고, 중속 영역(U16_EDC_SPEED2)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_S3               */		      70,	/* Comment [ PHASE 2이고, 고속 영역(U16_EDC_SPEED3)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_S4               */		      80,	/* Comment [ PHASE 2이고, 초고속 영역(U16_EDC_SPEED4)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_S1               */		       5,	/* Comment [ PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_S2               */		       6,	/* Comment [ PHASE 2이고, 중속 영역(U16_EDC_SPEED2)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_S3               */		       7,	/* Comment [ PHASE 2이고, 고속 영역(U16_EDC_SPEED3)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_S4               */		       8,	/* Comment [ PHASE 2이고, 초고속 영역(U16_EDC_SPEED4)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_S1               */		      88,	/* Comment [ PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_S2               */		      88,	/* Comment [ PHASE 3이고, 중속 영역(U16_EDC_SPEED2)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_S3               */		      88,	/* Comment [ PHASE 3이고, 고속 영역(U16_EDC_SPEED3)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_S4               */		      88,	/* Comment [ PHASE 3이고, 초고속 영역(U16_EDC_SPEED4)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_S1               */		      20,	/* Comment [ PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_S2               */		      24,	/* Comment [ PHASE 3이고, 중속 영역(U16_EDC_SPEED2)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_S3               */		      24,	/* Comment [ PHASE 3이고, 고속 영역(U16_EDC_SPEED3)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_S4               */		      24,	/* Comment [ PHASE 3이고, 초고속 영역(U16_EDC_SPEED4)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_S1               */		       0,	/* Comment [ PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_S2               */		       0,	/* Comment [ PHASE 4이고, 중속 영역(U16_EDC_SPEED2)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_S3               */		       0,	/* Comment [ PHASE 4이고, 고속 영역(U16_EDC_SPEED3)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_S4               */		       0,	/* Comment [ PHASE 4이고, 초고속 영역(U16_EDC_SPEED4)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_S1               */		      12,	/* Comment [ PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_S2               */		      12,	/* Comment [ PHASE 4이고, 중속 영역(U16_EDC_SPEED2)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_S3               */		      12,	/* Comment [ PHASE 4이고, 고속 영역(U16_EDC_SPEED3)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_S4               */		      12,	/* Comment [ PHASE 4이고, 초고속 영역(U16_EDC_SPEED4)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ABS_S1           */		      15,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ABS_S2           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ABS_S3           */		      25,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ABS_S4           */		      30,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ABS_S1           */		      60,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ABS_S2           */		      80,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ABS_S3           */		     100,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ABS_S4           */		     100,	/* Comment [ ABS 제어 중일 때, PHASE 1이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ABS_S1           */		      30,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ABS_S2           */		      30,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ABS_S3           */		      30,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ABS_S4           */		      30,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ABS_S1           */		      40,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ABS_S2           */		      45,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ABS_S3           */		      50,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ABS_S4           */		      55,	/* Comment [ ABS 제어 중일 때, PHASE 2이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ABS_S1           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ABS_S2           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ABS_S3           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ABS_S4           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ABS_S1           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ABS_S2           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ABS_S3           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ABS_S4           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 3이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ABS_S1           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ABS_S2           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ABS_S3           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ABS_S4           */		      20,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ABS_S1           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ABS_S2           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ABS_S3           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ABS_S4           */		      10,	/* Comment [ ABS 제어 중일 때, PHASE 4이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ESC_S1           */		      15,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ESC_S2           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ESC_S3           */		      25,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_D_GAIN_ESC_S4           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ESC_S1           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ESC_S2           */		      25,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ESC_S3           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE1_P_GAIN_ESC_S4           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 1이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ESC_S1           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ESC_S2           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ESC_S3           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_D_GAIN_ESC_S4           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ESC_S1           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ESC_S2           */		      25,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ESC_S3           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE2_P_GAIN_ESC_S4           */		      30,	/* Comment [ ESC 제어 중일 때, PHASE 2이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ESC_S1           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ESC_S2           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ESC_S3           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_D_GAIN_ESC_S4           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ESC_S1           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ESC_S2           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ESC_S3           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE3_P_GAIN_ESC_S4           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 3이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ESC_S1           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ESC_S2           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 중속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ESC_S3           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_D_GAIN_ESC_S4           */		      20,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 초고속 영역(U16_EDC_SPEED1)에서의 D Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ESC_S1           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 저속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ESC_S2           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 중속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ESC_S3           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_EDC_PHASE4_P_GAIN_ESC_S4           */		      10,	/* Comment [ ESC 제어 중일 때, PHASE 4이고, 초고속 영역(U16_EDC_SPEED1)에서의 P Gain량 ] */
	/* uchar8_t    	U8_D_GAIN_DEC_FACTOR_IN_RR            */		       2,	/* Comment [ Rough Road 감지시, EDC 민감 제어 방지를 위한 D Gain 영향 감소 Factor (Rough Road 감지시 D Gain량 = D Gain / U8_D_GAIN_DEC_FACTOR_IN_RR) ] */
	/* uchar8_t    	U8_GAIN_DEC_FACTOR_IN_HIGH_MU         */		       3,	/* Comment [ High-mu 감지후 P,D gain량 줄이는 factor (최종P.D Gain = P,D gain / U8_GAIN_DEC_FACTOR_IN_HIGH_MU) ] */
	/* uint16_t    	U16_EDC_ENG_RPM_LIMIT                 */		    4500,	/* Comment [ 설정된 값 이상의 Engine RPM 상승시, EDC 제어 금지 ] */
	/* uchar8_t    	U8_EDC_MTP_THR_FOR_EDC_INHIBIT        */		      30,	/* Comment [ 설정된 값 이상의 Acceleration pedal을 밟았을 때, EDC 제어 금지 ] */
	/* uchar8_t    	U8_EDC_TORQ_DEC_RATE                  */		      50,	/* Comment [ EDC제어중 ABS 및 ESC Brake 제어 개입시 현재 제어 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_DEC_RATE_EXIT_EAO         */		      40,	/* Comment [ ABS 제어 중인 상황에서의 Fade out 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_DEC_RATE_EXIT_EEO         */		      40,	/* Comment [ ESC 제어 중인 상황에서의 Fade out 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_DEC_RATE_EXIT_ESO         */		      15,	/* Comment [ ABS/ESC 제어 중이 아닌 상황에서의 Fade out 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[      1.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_DEC_RATE_RPM              */		       4,	/* Comment [ EDC제어중 설정된 Engine RPM이상의 고RPM 발생시 현재 제어 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[      0.4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_FAST_DEC_RATE_EXIT        */		     100,	/* Comment [ 휠슬립이 -0.25kph이상이고, MSR_cal_torq가 fric_torq보다 크면, 빠른 Fade out 종료를 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TORQ_SLOW_DEC_RATE_EXIT        */		       1,	/* Comment [ 휠슬립이 -0.25kph이상이고, MSR_cal_torq가 fric_torq보다 작으면, Fade out 종료를 천천히 가져가기 위한 torque decrease rate ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_HOLD_LV_IN_EDC_S1             */		     100,	/* Comment [ 저속 영역(U16_EDC_SPEED1)에서, EDC 제어 중 delta target slip이 target slip미만이고, engine torque가 설정된 이값 보다 작은 영역에서, 설정된 torque level로 제어량 유지함. ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_HOLD_LV_IN_EDC_S2             */		     120,	/* Comment [ 중속 영역(U16_EDC_SPEED2)에서, EDC 제어 중 delta target slip이 target slip미만이고, engine torque가 설정된 이값 보다 작은 영역에서, 설정된 torque level로 제어량 유지함. ] */
	                                                        		        	/* ScaleVal[       12 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_HOLD_LV_IN_EDC_S3             */		     130,	/* Comment [ 고속 영역(U16_EDC_SPEED3)에서, EDC 제어 중 delta target slip이 target slip미만이고, engine torque가 설정된 이값 보다 작은 영역에서, 설정된 torque level로 제어량 유지함. ] */
	                                                        		        	/* ScaleVal[       13 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_HOLD_LV_IN_EDC_S4             */		     130,	/* Comment [ 초고속 영역(U16_EDC_SPEED4)에서, EDC 제어 중 delta target slip이 target slip미만이고, engine torque가 설정된 이값 보다 작은 영역에서, 설정된 torque level로 제어량 유지함. ] */
	                                                        		        	/* ScaleVal[       13 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_UP_LIMIT_ABS                  */		     110,	/* Comment [ ABS 제어 중인 상황에서의 torque increase 제한량 ] */
	/* uchar8_t    	U8_TORQ_UP_LIMIT_ABS_HIGH_MU          */		      10,	/* Comment [ ABS 제어 중인 상황에서, High-mu 감지후 torque increase 제한량 ] */
	/* uchar8_t    	U8_TORQ_UP_LIMIT_ESC                  */		      70,	/* Comment [ ESC 제어 중인 상황에서의 torque increase 제한량 ] */
	/* uchar8_t    	U8_TORQ_UP_LIMIT_SOLO                 */		     105,	/* Comment [ ABS/ESC 제어 중이 아닌 상황에서의 torque increase 제한량 ] */
	/* uchar8_t    	U8_EDC_TM_1_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 1st Gear의 Total gear ratio (Transmission 1st gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_2_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 2nd Gear의 Total gear ratio (Transmission 2nd gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_3_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 3rd Gear의 Total gear ratio (Transmission 3rd gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_4_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 4th Gear의 Total gear ratio (Transmission 4th gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_5_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 5th Gear의 Total gear ratio (Transmission 5th gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_6_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 6th Gear의 Total gear ratio (Transmission 6th gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EDC_TM_R_GEAR_RATIO                */		       0,	/* Comment [ Transmission의 Reverse Gear의 Total gear ratio (Transmission Reverse gear ratio × Final gear ratio) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_EDC_INHIBIT_SPEED                 */		     120,	/* Comment [ 설정값 미만에서는 EDC 제어 금지 (FWD:20kph , RWD:15kph권장) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_EDC_INHIBIT_SPEED_IN_EDC          */		      96,	/* Comment [ EDC제어중이고, 설정값 미만에서는 EDC 제어 금지 (FWD:18kph , RWD:12kph권장) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* char8_t     	S8_EDC_ENTRY_LIMIT                    */		     -16,	/* Comment [ EDC 제어 진입을 위한 Delta V Threshold 제한 값 (민감 제어 방지용) ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uint16_t    	U16_EDC_GAIN_SCALING_FACTOR           */		      80,	/* Comment [ P/D Effect = (P_Gain * WheelSlipError + D_Gain * WheelSlipErrorDiff) / U16_EDC_GAIN_SCALING_FACTOR ] */
	/* uchar8_t    	U8_EDC_CONTROL_TIME_MAX               */		      30,	/* Comment [ 1회 EDC 최대 작동시간 (단위 : 초) ] */
	/* uchar8_t    	U8_VEL_GAP_MOVING_WINDOW_LENGTH       */		      14,	/* Comment [ In a vehicle installed different kind tire for two wheels, Moving average window length to calculate constant slip band for EDC control termination. ] */
	/* int16_t     	S16_EDC_DEL_ENG_RPM_FOR_PARK          */		    -400,	/* Comment [ M/T에서 Parking Brake 감지를 위한 Engine RPM 변화량 ] */
	/* int16_t     	S16_EDC_PARK_ENTRY_RPM_DIFF           */		    2000,	/* Comment [ AT에서, Parking Brake 감지를 위한 Engine rpm과 Turbine rpm과의 속도차값 설정 (이 값 보다 크면 parking brake 감지) ] */
	/* int16_t     	S16_EDC_PARK_ENTRY_TC_RPM             */		     100,	/* Comment [ AT에서, Parking Brake 감지를 위한 Turbine rpm의 lower limit값 설정 (이 값 보다 작으면 parking brake 감지) ] */
	/* char8_t     	S8_EDC_PARKING_SUSPECT_LIMIT          */		     120,	/* Comment [ Parking Brake 감지 제한 속도 (설정값 이하에서는 parking brake 감지 제한) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* int16_t     	S16_AT_In_Gear_Lower_Limit            */		    -200,	/* Comment [ AutoTransmission에서 In Gear 판단을 위한 입력단과 출력단 회전 속도 차이의 lower boundary ] */
	/* int16_t     	S16_AT_In_Gear_Upper_Limit            */		     300,	/* Comment [ AutoTransmission에서 In Gear 판단을 위한 입력단과 출력단 회전 속도 차이의 upper boundary ] */
	/* int16_t     	S16_MT_In_Gear_Out_Vel_Threshold      */		     200,	/* Comment [ ManualTransmission에서 In Gear 판단 해지를 위한 속도 threshold (입력단과 출력단 회전 속도 차이) ] */
	/* int16_t     	S16_MT_In_Gear_Vel_Threshold_1to3     */		     150,	/* Comment [ ManualTransmission에서 1~3단기어의 In Gear 판단을 위한 속도 threshold (입력단과 출력단 회전 속도 차이) ] */
	/* int16_t     	S16_MT_In_Gear_Vel_Threshold_4to6     */		      50,	/* Comment [ ManualTransmission에서 4~6단기어의 In Gear 판단을 위한 속도 threshold (입력단과 출력단 회전 속도 차이) ] */
	/* char8_t     	S8_MT_In_Gear_Accel_Threshold         */		       4,	/* Comment [ ManualTransmission에서 In Gear 판단을 위한 가속도 threshold (입력단과 출력단 회전 가속도 차이) ] */
	/* char8_t     	S8_MT_In_Gear_Out_Accel_Threshold     */		       8,	/* Comment [ ManualTransmission에서 In Gear 판단 해지를 위한 가속도 threshold (입력단과 출력단 회전 가속도 차이) ] */
	/* int16_t     	S16_AT_Eng_Status_Threshold           */		      50,	/* Comment [ AutoTransmission에서 Drag 판단을 위한 threshold (엔진과 토크컨버터 속도 차이 <-S16_AT_Eng_Status_Threshold 인 경우 Drag로 판단) ] */
	/* char8_t     	S8_MT_Eng_Status_Threshold            */		      10,	/* Comment [ ManualTransmission에서 Drag 판단을 위한 threshold (Flywheel torque값 <-S8_MT_Eng_Status_Threshold 인 경우 Drag로 판단) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR      */		       4,	/* Comment [ Gear shift down 감지를 위한 평균 휠 슬립 기울기 threshold값 설정 (이 값이 클수록 감지 둔감해짐) ] */
	/* char8_t     	S8_EDC_GSD_ENTRY_CNT                  */		      15,	/* Comment [ Gear shift down 감지 카운터 (설정값 이상이면 Gear shift down 감지됨) ] */
	/* char8_t     	S8_EDC_GSD_ENTRY_WHL_SLIP             */		     -40,	/* Comment [ 평균 구동륜 슬립 기준 Gear shift down 감지 진입 threshold값(설정값 미만에서만 Gear shift down 감지) ] */
	                                                        		        	/* ScaleVal[     -5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_GSD_EXIT_WHL_SLIP              */		     -16,	/* Comment [ 평균 구동륜 슬립 기준 Gear shift down 감지 해제 threshold값(설정값 이상에서는 Gear shift down 감지 해제) ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EDC_GSD_SLOPE_ENG_RPM_THR          */		       2,	/* Comment [ Gear shift down 감지를 위한 엔진 RPM 기울기 threshold값 설정 (이 값이 클수록 감지 둔감해짐) ] */
	/* uchar8_t    	U8_EDC_GSD_ENABLE_FLG                 */		       1,	/* Comment [ Gear shift down 감지 컨셉 적용 여부 결정을 위한 flag (1 : enable , 0 : disable) ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_H              */		     600,	/* Comment [ Ay level for CBC enter at high speed ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_M              */		     700,	/* Comment [ Ay level for CBC enter at middle speed ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_S              */		     800,	/* Comment [ Ay level for CBC enter at low speed ] */
	                                                        		        	/* ScaleVal[      0.8 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_BRAKING_DET_THRES_ABS         */		     150,	/* Comment [ Min Mpress for Partial brake detect SET, default 15bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_BRAKING_MIN_THRES_ABS         */		      50,	/* Comment [ Min Mpress for Partial brake detect RESET, default 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_CBC_MPRESS_ENTER_ABS              */		     500,	/* Comment [ Estimated mpress level for CBC enter condition ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_ALAT_EXIT_ABS                 */		     200,	/* Comment [ Ay level for CBC exit ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_MPRESS_EXIT_ABS               */		     600,	/* Comment [ Estimated mpress level for CBC exit condition ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ABS_SPD_H         */		     400,	/* Comment [ Rear delta pressure at high speed ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ABS_SPD_M         */		     300,	/* Comment [ Rear delta pressure at middle speed ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ABS_SPD_S         */		     100,	/* Comment [ Rear delta pressure at low speed ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRES_MAX_DUMP_ABS       */		     100,	/* Comment [ Minimum pressure difference for full dump ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRES_MAX_RISE_ABS       */		     200,	/* Comment [ Minimum pressure difference for full rise ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_CBC_REAPPLY_TIME_ABS_R             */		       7,	/* Comment [ Rear conventional reapply time of one pulse up for CBC ] */
	/* char8_t     	S8_CBC_REAPPLY_TIME_FADEOUT_ABS_R     */		       7,	/* Comment [ Rear conventional reapply time of one pulse up for CBC fade out ] */
	/* uchar8_t    	U8_CBC_DUMPSCAN_TIME                  */		      10,	/* Comment [ CBC dump scan time ] */
	/* uchar8_t    	U8_CBC_MAX_HOLD_TIMER                 */		       4,	/* Comment [ CBC max. hold timer ] */
	/* int16_t     	S16_CBC_FADE_OUT_HOLD_SCAN_ABS        */		       0,	/* Comment [ Hold scan for pulse up rise at fade out mode ] */
	/* int16_t     	S16_CBC_FADE_OUT_TIME_ABS             */		      29,	/* Comment [ Time length for fade out mode after CBC reset ] */
	                                                        		        	/* ScaleVal[   0.29 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_BRAKING_AX_THRES_BLS          */		      50,	/* Comment [ Min. deceleration for estimation of Mpress with valid BLS, default 0.1g ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_BRAKING_AX_THRES_NO_BLS       */		     150,	/* Comment [ Min. deceleration for estimation of Mpress without BLS, default 0.15g ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_CBC_AX_MPRES_FACTOR                */		      10,	/* Comment [ Estimated mpress multiply Ax and this parameter together, default 12 ] */
	/* uchar8_t    	U8_CBC_PRESS_EST_FRONT_DUMP_GAIN      */		      10,	/* Comment [ Dump rate gain for estimation of front wheel pressure ] */
	/* uchar8_t    	U8_CBC_PRESS_EST_FRONT_RISE_GAIN      */		       5,	/* Comment [ Rise rate gain for estimation of front wheel pressure ] */
	/* uchar8_t    	U8_CBC_PRESS_EST_REAR_DUMP_GAIN       */		      10,	/* Comment [ Dump rate gain for estimation of rear wheel pressure ] */
	/* uchar8_t    	U8_CBC_PRESS_EST_REAR_RISE_GAIN       */		       7,	/* Comment [ Rise rate gain for estimation of rear wheel pressure ] */
	/* int16_t     	S16_CBC_AY_EST_REF_SPD_H              */		     960,	/* Comment [ High speed range for using CBC_AY_EST_GAIN_SPD_H parameters ] */
	                                                        		        	/* ScaleVal[   120 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_AY_EST_REF_SPD_M              */		     560,	/* Comment [ Middle speed range for using CBC_AY_EST_GAIN_SPD_M parameters ] */
	                                                        		        	/* ScaleVal[    70 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_AY_EST_REF_SPD_S              */		     240,	/* Comment [ Low speed range for using CBC_AY_EST_GAIN_SPD_S parameters ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_CBC_AY_EST_GAIN_SPD_H              */		      80,	/* Comment [ Gain multiplied to estimated Ay by wheel speed difference at high speed range ] */
	/* uchar8_t    	U8_CBC_AY_EST_GAIN_SPD_M              */		      80,	/* Comment [ Gain multiplied to estimated Ay by wheel speed difference at middle speed range ] */
	/* uchar8_t    	U8_CBC_AY_EST_GAIN_SPD_S              */		     100,	/* Comment [ Gain multiplied to estimated Ay by wheel speed difference at low speed range ] */
	/* int16_t     	S16_CBC_STEADY_MAX_ACCEL              */		     150,	/* Comment [ Max. acceleration level for detection of steady state driving condition, default 0.15g ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_STEADY_MAX_DECEL              */		     100,	/* Comment [ Max. deceleration level for detection of steady state driving condition, default 0.1g ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_CBC_WHEEL_SPIN_OK_LEVEL            */		      24,	/* Comment [ Max. wheel spin level for detection of steady state driving condition, default 3km/h ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* int16_t     	S16_CBC_AY_ERROR_CHECK_TIME           */		    1428,	/* Comment [ Min. time for checking error or suspect condition of estimated Ay, default 10sec. ] */
	                                                        		        	/* ScaleVal[  14.28 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_AY_ERROR_DET_LEVEL            */		     400,	/* Comment [ Min. Ay difference level between two models for detecting error condition of estimated Ay ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_AY_ERROR_SUS_LEVEL            */		     200,	/* Comment [ Min. Ay difference level between two models for detecting suspect condition of estimated Ay ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_AY_OK_CHECK_TIME              */		    1428,	/* Comment [ Min. time for checking initial reliability of estimated Ay, default 10sec. ] */
	                                                        		        	/* ScaleVal[  14.28 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_ABS_REAR_ENTER_DV_SPD_H       */		      96,	/* Comment [ Delta V of rear wheel for CBC slip control of ABS solo at high speed, default value = 12kph ] */
	                                                        		        	/* ScaleVal[    12 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_ABS_REAR_ENTER_DV_SPD_M       */		      80,	/* Comment [ Delta V of rear wheel for CBC slip control of ABS solo at middle speed, default value = 10kph ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_ABS_REAR_ENTER_DV_SPD_S       */		      64,	/* Comment [ Delta V of rear wheel for CBC slip control of ABS solo at low speed, default value = 8kph ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_ABS_REAR_EXIT_DV              */		      40,	/* Comment [ Delta V of rear wheel for CBC slip control exit of ABS solo, default value = 5kph ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_H     */		     200,	/* Comment [ Additional rear delta pressure for slip control of ABS solo at high speed, default value = 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_M     */		     150,	/* Comment [ Additional rear delta pressure for slip control of ABS solo at middle speed, default value = 15bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_S     */		     100,	/* Comment [ Additional rear delta pressure for slip control of ABS solo at low speed, default value = 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_THRES_HIGH_SPEED              */		     960,	/* Comment [ CBC reference High speed(default : 120kph) ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_THRES_LOW_SPEED               */		     480,	/* Comment [ CBC reference Low speed(default : 60kph) ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_THRES_MED_SPEED               */		     720,	/* Comment [ CBC reference Med speed(default : 90kph) ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* char8_t     	S16_Accele_End_0                      */		     -50,	/* Comment [ Deceleration Criteria for ESS exit start (default:-0.5g, range:-0.65g~-0.4g) ] */
	                                                        		        	/* ScaleVal[     -0.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S16_Accele_End_1                      */		     -40,	/* Comment [ Deceleration Criteria for ESS exit(default:-0.4g, range:-0.25g~-0.4g) ] */
	                                                        		        	/* ScaleVal[     -0.4 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S16_Accele_Start                      */		     -65,	/* Comment [ Deceleration Criteria for ESS entry(default:-0.65g, range:-0.5g~-1.0g) ] */
	                                                        		        	/* ScaleVal[    -0.65 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* int16_t     	S16_Vdiff_ExdEss_Exit                 */		     640,	/* Comment [ Vehicle speed Deviation  for extended ESS exit(default:10kph) ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* int16_t     	S16_Vehicle_Speed_ExdEss_Max          */		    2560,	/* Comment [ Vehicle speed for extended ESS entry(default:40kph) ] */
	                                                        		        	/* ScaleVal[    40 km/h ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* int16_t     	S16_Vehicle_Speed_ExdEss_Min          */		     640,	/* Comment [ Vehicle speed for extended ESS exit condition change (default:10kph) ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* int16_t     	S16_Vehicle_Speed_Min                 */		    3520,	/* Comment [ Vehicle speed for ESS entry(default:55kph, range: 70kph~40kph) ] */
	                                                        		        	/* ScaleVal[    55 km/h ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* uchar8_t    	U16_T_BLS_Off                         */		      50,	/* Comment [ BLS off time duration for ESS exit(default:500ms, range:100ms~1000ms) ] */
	                                                        		        	/* ScaleVal[     500 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U16_T_Dct_ExdEss                      */		      10,	/* Comment [ Time duration for Extended ESS state transition(default:100ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uint16_t    	U16_T_Dct_ExdEss_Exit                 */		    1000,	/* Comment [ Maximum time duration for Extended ESS exit(default:10s) ] */
	                                                        		        	/* ScaleVal[   10000 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 655350 ] */
	/* uchar8_t    	U16_T_Detect_ABS_ON                   */		      10,	/* Comment [ ABS On time duration for ESS entry (default:100ms, range:50ms~1000ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U16_T_Detect_Decel                    */		      10,	/* Comment [ Duration for ESS entry by Deceleration condition(default:100ms, range:50ms~1000ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U16_T_Hazard_Lamp_On                  */		      10,	/* Comment [ Hazard Lamp On time duration for ESS exit(default:100ms, range:50ms~1000ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uint16_t    	K_Max_Pump_On_Run_Time_1              */		   17143,	/* Comment [ Maximum Pump Operating Time(Atmospheric Pressure : U16_BARO_PRESS_1) ] */
	                                                        		        	/* ScaleVal[ 171.43 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	K_Max_Pump_On_Run_Time_2              */		   15714,	/* Comment [ Maximum Pump Operating Time(Atmospheric Pressure : U16_BARO_PRESS_2) ] */
	                                                        		        	/* ScaleVal[ 157.14 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	K_Max_Pump_On_Run_Time_3              */		   14286,	/* Comment [ Maximum Pump Operating Time(Atmospheric Pressure : U16_BARO_PRESS_3) ] */
	                                                        		        	/* ScaleVal[ 142.86 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	K_Max_Pump_On_Run_Time_4              */		   12857,	/* Comment [ Maximum Pump Operating Time(Atmospheric Pressure : U16_BARO_PRESS_4) ] */
	                                                        		        	/* ScaleVal[ 128.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uchar8_t    	K_Max_Pump_Turn_On                    */		      10,	/* Comment [ Maximum Operating Count ] */
	/* uint16_t    	K_Min_Pump_Off_Time                   */		     143,	/* Comment [ EVP Control re-enty limit Time ] */
	                                                        		        	/* ScaleVal[   1.43 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	U16_BARO_PRESS_1                      */		    1010,	/* Comment [ Standard pressue For EVP ] */
	                                                        		        	/* ScaleVal[    101 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6335.5 ] */
	/* uint16_t    	U16_BARO_PRESS_2                      */		     900,	/* Comment [ Lowland Pressure For EVP ] */
	                                                        		        	/* ScaleVal[     90 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6335.5 ] */
	/* uint16_t    	U16_BARO_PRESS_3                      */		     800,	/* Comment [ Low/HighLand Pressure ] */
	                                                        		        	/* ScaleVal[     80 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6335.5 ] */
	/* uint16_t    	U16_BARO_PRESS_4                      */		     750,	/* Comment [ High Land Pressure For EVP ] */
	                                                        		        	/* ScaleVal[     75 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6335.5 ] */
	/* uchar8_t    	K_EVP_FUNCTION                        */		       1,	/* Comment [ EVP Fuction 1=ENABLE/ 0=DISABLE ] */
	/* uchar8_t    	K_EVP_Vacuum_Sensor_Type              */		       1,	/* Comment [ EVP Vacuum Sensor Type(Analog Type = 1, CAN Type = 2) ] */
	/* uint16_t    	K_Debounce_Time                       */		       7,	/* Comment [ Debounce Time Reference. K_Debounce_Time>Tunnig Parameter-> Control condition satisfied ] */
	                                                        		        	/* ScaleVal[   0.07 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* int16_t     	K_Turn_EVP_Off_Threshold              */		    -500,	/* Comment [ Vacuum Signal Threshold for EVP Control release, Vacuum Signal<Tunnig Parameter ->Control release condition satisfied ] */
	                                                        		        	/* ScaleVal[    -50 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	K_Turn_EVP_Off_Threshold_High_A       */		    -435,	/* Comment [ Vacuum Signal Threshold for EVP Control release in high altitude, Vacuum Signal<Tunnig Parameter ->Control release condition satisfied ] */
	                                                        		        	/* ScaleVal[  -43.5 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	K_Turn_EVP_On_Threshold               */		    -350,	/* Comment [ Vacuum Signal Threshold for EVP Control, Vacuum Signal>K_Turn_EVP_On_Threshold ->Control condition satisfied ] */
	                                                        		        	/* ScaleVal[    -35 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	K_Turn_EVP_On_Threshold_High_A        */		    -375,	/* Comment [ Vacuum Signal Threshold for EVP Control in high altitude, Vacuum Signal>K_Turn_EVP_On_Threshold ->Control condition satisfied ] */
	                                                        		        	/* ScaleVal[  -37.5 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	K_CAT_LightOff_Present                */		       0,	/* Comment [ CAT LightOff Present True/False ] */
	/* uchar8_t    	K_MAP_Present                         */		       0,	/* Comment [ MAP Present True / False ] */
	/* int16_t     	K_MAP_Threshold                       */		     500,	/* Comment [ Difference between atmospheric Pressure and engine manifold Pressure For EVP Control ] */
	                                                        		        	/* ScaleVal[     50 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	K_MAX_Start_Time                      */		   42857,	/* Comment [ Maximum Engine Operating Time(Limit 5min) For EVP Control . ] */
	                                                        		        	/* ScaleVal[ 428.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	K_Min_Pump_On_Run_Time                */		    1429,	/* Comment [ EVP Operating time>K_Min_Pump_On_Run_Time ->Control release condition satisfied ] */
	                                                        		        	/* ScaleVal[  14.29 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	K_MIN_Start_Time                      */		       0,	/* Comment [ Minimum Operating Time For EVP Control . ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 633.55 ] */
	/* uint16_t    	U16_AX_OFFSET_CHECK_ENG_DRAG_1        */		     500,	/* Comment [ Engine Drag Condition Check for A/T : [rpm] _ abs(eng_rpm - tc_rpm), default : 500rpm ] */
	/* uchar8_t    	U8_AX_OFFSET_CHECK_DYN_FLAT           */		      40,	/* Comment [ Zero Gradient Check Condition for Dynamic Model, default : 0.04g ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_CHECK_ENG_DRAG_2         */		      20,	/* Comment [ Engine Drag Condition Check for A/T : [%] _ [abs(eng_rpm - tc_rpm)*100]/eng_rpm, default : 20% ] */
	/* uchar8_t    	U8_AX_OFFSET_CHECK_KIN_FLAT_1         */		      90,	/* Comment [ Zero Gradient Check Condition for Kinematic Model Min_-0.09g≤X≤-0.02g, default : 0.09g ] */
	                                                        		        	/* ScaleVal[     0.09 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_CHECK_KIN_FLAT_2         */		      20,	/* Comment [ Zero Gradient Check Condition for Kinematic Model Max_-0.09g≤X≤-0.02g, default : 0.02g ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_AIR_AREA           */		      20,	/* Comment [ Ax Offset Air Model Area_vehicle front area, default : 2m^2 ] */
	                                                        		        	/* ScaleVal[      2 m^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_AIR_CD             */		       8,	/* Comment [ Ax Offset Air Model Cd_wind resistance coefficient, default : 0.8 ] */
	                                                        		        	/* ScaleVal[   0.8 zero ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_AIR_RO             */		      12,	/* Comment [ Ax Offset Air Model Ro_air density, default : 1.2kg/m^3 ] */
	                                                        		        	/* ScaleVal[ 1.2 kg/m^3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ROLL_TOTAL         */		      20,	/* Comment [ Ax Offset Roll Model Total_rolling resistance, default : 0.02g ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* int16_t     	S16_CAR_STOP_STATE_DET_TIME           */		     180,	/* Comment [ Vehicle Stop State Detection Time ] */
	/* int16_t     	S16_RTA_ABSENCE_DET_RATIO             */		      20,	/* Comment [ RTA Absence Detection Ratio ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_COMP_RATIO_MAX                */		    1300,	/* Comment [ RTA Minimum Compensation Ratio ] */
	                                                        		        	/* ScaleVal[        130 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_COMP_RATIO_MIN                */		     700,	/* Comment [ RTA Maximum Compensation Ratio ] */
	                                                        		        	/* ScaleVal[         70 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_DET_MIN_MAX_DIFF_THR          */		      30,	/* Comment [ RTA detection threshold (max diff ratio - min diff ratio) ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_DET_THRES                     */		      30,	/* Comment [ RTA Detection Minimum Threshold ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_DET_THRES_MAX                 */		     300,	/* Comment [ RTA Detection Maximum Threshold ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_DET_TIME                      */		     300,	/* Comment [ RTA Detection Time ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_DIFF_RATIO_THRES              */		      50,	/* Comment [ RTA suspect detection threshold (max diff. ratio - min diff. ratio) ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RTA_RESET_TIME                    */		     300,	/* Comment [ RTA Reset Time ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_RESET_TIME2                   */		     600,	/* Comment [ RTA Reset Time 2 ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_STATE_CONFIRM_TIME            */		     600,	/* Comment [ RTA Stste Confirm Time ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_SUS_CLEAR_TIME                */		     150,	/* Comment [ RTA Suspect Clear Time ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_SUS_CLEAR_TIME2               */		     300,	/* Comment [ RTA Suspect Clear Time 2 ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_RTA_SUS_DET_TIME                  */		     150	/* Comment [ RTA Suspect Detection Time ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
};

#define LOGIC_CAL_MODULE_22_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

