#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_24_START


const /*far*/	DATA_APCALESPVAFS_t	apCalEspVafs2WD =
{
		{
	/* uint16_t    	apCalEspVafsInfo.CDID                 */		0x0000,
	/* uint16_t    	apCalEspVafsInfo.CDVer                */		0x0000,
	/* uchar8_t    	apCalEspVafsInfo.MID                  */		0x00,
		},
	/* int16_t     	S16_DEC_DEADZONE_L                    */		      -2,	/* Comment [ 감속도제어기 deadzone을 규정하는 하위 감속도 값 ] */
	                                                        		        	/* ScaleVal[    -0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEC_DEADZONE_U                    */		       2,	/* Comment [ 감속도제어기 deadzone을 규정하는 상위 감속도 값 ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MAX               */		     210,	/* Comment [ 감속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MAX_A_ABS         */		     210,	/* Comment [ 감속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정, 단 모든 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MAX_P_ABS         */		     210,	/* Comment [ 감속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정, 단 어떤 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MIN               */		       1,	/* Comment [ 감속도제어 최소 주기 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MIN_A_ABS         */		       2,	/* Comment [ 감속도제어 최소 주기, 단 모든 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_DEC_CTRL_PERIOD_MIN_P_ABS         */		       1,	/* Comment [ 감속도제어 최소 주기, 단 어떤 Wheel이 ABS 제어인 경우 ] */
	/* int16_t     	S16_DEC_ME_MED_DGAIN                  */		       3,	/* Comment [ 감속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_ME_MED_DGAIN_ACC              */		       3,	/* Comment [ 가속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_ME_MED_IGAIN                  */		       1,	/* Comment [ 감속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_ME_MED_IGAIN_ACC              */		       1,	/* Comment [ 가속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_ME_MED_PGAIN                  */		       3,	/* Comment [ 감속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_ME_MED_PGAIN_ACC              */		       3,	/* Comment [ 가속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_ME_PED_DGAIN                  */		       3,	/* Comment [ 감속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_ME_PED_DGAIN_ACC              */		       3,	/* Comment [ 가속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_ME_PED_IGAIN                  */		       1,	/* Comment [ 감속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_ME_PED_IGAIN_ACC              */		       1,	/* Comment [ 가속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_ME_PED_PGAIN                  */		       3,	/* Comment [ 감속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_ME_PED_PGAIN_ACC              */		       3,	/* Comment [ 가속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_PE_MED_DGAIN                  */		       7,	/* Comment [ 감속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_PE_MED_DGAIN_ACC              */		       7,	/* Comment [ 가속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_PE_MED_IGAIN                  */		       2,	/* Comment [ 감속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_PE_MED_IGAIN_ACC              */		       2,	/* Comment [ 가속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_PE_MED_PGAIN                  */		      10,	/* Comment [ 감속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_PE_MED_PGAIN_ACC              */		      10,	/* Comment [ 가속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_PE_PED_DGAIN                  */		       7,	/* Comment [ 감속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_PE_PED_DGAIN_ACC              */		       7,	/* Comment [ 가속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_DEC_PE_PED_IGAIN                  */		       2,	/* Comment [ 감속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_PE_PED_IGAIN_ACC              */		       2,	/* Comment [ 가속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 I gain ] */
	/* int16_t     	S16_DEC_PE_PED_PGAIN                  */		      10,	/* Comment [ 감속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_PE_PED_PGAIN_ACC              */		      10,	/* Comment [ 가속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_DEC_INITIAL_TARGET_V              */		    5000,	/* Comment [ Decel 제어 초기 최소 모터 구동 전압 ] */
	                                                        		        	/* ScaleVal[     5 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_DEC_MSC_1ST_ERR                   */		     -15,	/* Comment [ 감속도 MSC제어 첫번째 포인트의 에러값 ] */
	                                                        		        	/* ScaleVal[    -0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEC_MSC_1ST_VOL                   */		    3500,	/* Comment [ 감속도 MSC제어 첫번째 포인트의 전압값 ] */
	                                                        		        	/* ScaleVal[    3.5 Vol ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_DEC_MSC_2ND_ERR                   */		      -2,	/* Comment [ 감속도 MSC제어 두번째 포인트의 에러값 ] */
	                                                        		        	/* ScaleVal[    -0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEC_MSC_2ND_VOL                   */		    1750,	/* Comment [ 감속도 MSC제어 두번째 포인트의 전압값 ] */
	                                                        		        	/* ScaleVal[   1.75 Vol ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_1                    */		       0,	/* Comment [ DEC temporarily T/P 1 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_10                   */		       0,	/* Comment [ DEC temporarily T/P 10 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_2                    */		       0,	/* Comment [ DEC temporarily T/P 2 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_3                    */		       0,	/* Comment [ DEC temporarily T/P 3 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_4                    */		       0,	/* Comment [ DEC temporarily T/P 4 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_5                    */		       0,	/* Comment [ DEC temporarily T/P 5 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_6                    */		       0,	/* Comment [ DEC temporarily T/P 6 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_7                    */		       0,	/* Comment [ DEC temporarily T/P 7 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_8                    */		       0,	/* Comment [ DEC temporarily T/P 8 ] */
	/* int16_t     	S16_DEC_T_P_TEMP_9                    */		       0,	/* Comment [ DEC temporarily T/P 9 ] */
	/* int16_t     	S16_HDC_ACT_SPD_MAX                   */		     280,	/* Comment [ HDC 최대 진입속도 ] */
	                                                        		        	/* ScaleVal[     35 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_ACT_SPD_MIN                   */		      16,	/* Comment [ HDC 최소 진입속도 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_ENT_DISC_TEMP                 */		   12800,	/* Comment [ HDC 작동 금지 해제 기준 추정 디스크 온도 (default : 400`C) ] */
	                                                        		        	/* ScaleVal[     400 `C ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_HDC_EXIT_DISC_TEMP                */		   20800,	/* Comment [ HDC 작동 금지 기준 추정 디스크 온도 (default : 650`C) ] */
	                                                        		        	/* ScaleVal[     650 `C ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_HDC_FADE_OUT_CTRL_DECEL           */		      80,	/* Comment [ Fade-out 시, 해제 기준 차량 가속도 min (default : 0.08g) ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_FADE_OUT_CTRL_DECEL2          */		     150,	/* Comment [ Fade-out 시, 해제 기준 차량 가속도 max (default : 0.15g) ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_SWITCH_ON_SPD                 */		     320,	/* Comment [ HDC 스위치 ON이 가능한 최대 속도 ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_TEMP_1                        */		       0,	/* Comment [ HDC reserved 1 ] */
	/* int16_t     	S16_HDC_TEMP_2                        */		       0,	/* Comment [ HDC reserved 2 ] */
	/* int16_t     	S16_HDC_TEMP_3                        */		       0,	/* Comment [ HDC reserved 3 ] */
	/* int16_t     	S16_HDC_TEMP_4                        */		       0,	/* Comment [ HDC reserved 4 ] */
	/* int16_t     	S16_HDC_TEMP_5                        */		       0,	/* Comment [ HDC reserved 5 ] */
	/* int16_t     	S16_HDC_TERMINATE_SPD                 */		     480,	/* Comment [ HDC 종료속도 ] */
	                                                        		        	/* ScaleVal[     60 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HDC_ACCEL_ENTER                    */		       5,	/* Comment [ 운전자 가속 의지 판단 기준, 가속 페달 수치(default : 5%) ] */
	/* uchar8_t    	U8_HDC_ACCEL_EXIT                     */		       2,	/* Comment [ 운전자 가속 의지 판단 해제 기준, 가속 페달 수치(default : 2%) ] */
	/* uchar8_t    	U8_HDC_TEMP_10                        */		       0,	/* Comment [ HDC reserved 10 ] */
	/* uchar8_t    	U8_HDC_TEMP_6                         */		       0,	/* Comment [ HDC reserved 6 ] */
	/* uchar8_t    	U8_HDC_TEMP_7                         */		       0,	/* Comment [ HDC reserved 7 ] */
	/* uchar8_t    	U8_HDC_TEMP_8                         */		       0,	/* Comment [ HDC reserved 8 ] */
	/* uchar8_t    	U8_HDC_TEMP_9                         */		       0,	/* Comment [ HDC reserved 9 ] */
	/* uchar8_t    	U8_HDC_AT_TG_SPD_SET_MODE             */		       2,	/* Comment [ AT차량의 목표속도 셋팅 방법(1:CAN, 2: Constant Value) ] */
	/* uchar8_t    	U8_HDC_MT_TG_SPD_SET_MODE             */		       2,	/* Comment [ MT차량의 목표속도 셋팅 방법(1:CAN, 2:Constant Value) ] */
	/* int16_t     	S16_HDC_MAX_TARGET_SPD_MT             */		     160,	/* Comment [ Maximum target speed of M/T ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MIN_TARGET_SPD_MT             */		      40,	/* Comment [ Minimum target speed of M/T ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_1ST_TG_SPD                 */		      56,	/* Comment [ MT / !4L 차량의 1단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_2ND_TG_SPD                 */		     104,	/* Comment [ MT / !4L 차량의 2단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_3RD_TG_SPD                 */		     104,	/* Comment [ MT / !4L 차량의 3단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_4TH_TG_SPD                 */		     104,	/* Comment [ MT / !4L 차량의 4단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_5TH_TG_SPD                 */		     104,	/* Comment [ MT / !4L 차량의 5단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_NEU_TG_SPD                 */		      56,	/* Comment [ MT / !4L 차량의 N단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_REV_TG_SPD                 */		      56,	/* Comment [ MT / !4L 차량의 R단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_1ST_TG_SPD_4L              */		      56,	/* Comment [ MT / 4L 차량의 1단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_2ND_TG_SPD_4L              */		     104,	/* Comment [ MT / 4L 차량의 2단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_3RD_TG_SPD_4L              */		     104,	/* Comment [ MT / 4L 차량의 3단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_4TH_TG_SPD_4L              */		     104,	/* Comment [ MT / 4L 차량의 4단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_5TH_TG_SPD_4L              */		     104,	/* Comment [ MT / 4L 차량의 5단 목표속도 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_NEU_TG_SPD_4L              */		      56,	/* Comment [ MT / 4L 차량의 N단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_REV_TG_SPD_4L              */		      56,	/* Comment [ MT / 4L 차량의 R단 목표속도 ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MAX_TARGET_SPD_AT             */		     160,	/* Comment [ Maximum target speed of A/T ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MIN_TARGET_SPD_AT             */		      40,	/* Comment [ Minimum target speed of A/T ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_FOR_TG_SPD                 */		      64,	/* Comment [ AT / !4L 차량의 전진 목표속도 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_NEU_TG_SPD                 */		      64,	/* Comment [ AT / !4L 차량의 중립 목표속도 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_REV_TG_SPD                 */		      48,	/* Comment [ AT / !4L 차량의 후진 목표속도 ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_FOR_TG_SPD_4L              */		      64,	/* Comment [ AT / 4L 차량의 전진 목표속도 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_NEU_TG_SPD_4L              */		      64,	/* Comment [ AT / 4L 차량의 중립 목표속도 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_REV_TG_SPD_4L              */		      48,	/* Comment [ AT / 4L 차량의 후진 목표속도 ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_THETAG_PITCH                  */		      12,	/* Comment [ 피치 의한 Theta g 보정값(decel/thetag_pitch) ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_THETAG_STEER                  */		      40,	/* Comment [ Beta추정 불능시 조향에 의한 Theta g 보정값(yaw/thetag_steer) ] */
	                                                        		        	/* ScaleVal[   0.04 '/s ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_YAW_DOT_FACTOR                */		      11,	/* Comment [ 언덕판단 경계값의 Yaw 미분 보정값(Yaw/Yaw_dot_factor) ] */
	                                                        		        	/* ScaleVal[ 0.11 '/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_YAW_FACTOR                    */		     110,	/* Comment [ 언덕판단 경계값의 Yaw 보정값(Yaw/Yaw_factor) ] */
	                                                        		        	/* ScaleVal[   0.11 '/s ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_BIG_G_TH                      */		      15,	/* Comment [ Theta g의 언덕판단 최대 경계값 ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_MID_G_TH                      */		      12,	/* Comment [ Theta g의 언덕판단 중간 경계값 ] */
	                                                        		        	/* ScaleVal[     0.12 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_SMA_G_TH                      */		       8,	/* Comment [ Theta g의 언덕판단 최소 경계값 ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_HDC_BIG_G_EST_TIME                 */		      80,	/* Comment [ Theta g의 언덕판단 최대 경계값의 추정시간 ] */
	                                                        		        	/* ScaleVal[    0.8 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_MID_G_EST_TIME                 */		      90,	/* Comment [ Theta g의 언덕판단 중간 경계값의 추정시간 ] */
	                                                        		        	/* ScaleVal[    0.9 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_SMA_G_EST_TIME                 */		     100,	/* Comment [ Theta g의 언덕판단 최소 경계값의 추정시간 ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_HDC_BUMP_ACT_HIGH_SPEED           */		     120,	/* Comment [ Bump 판단을 위한 HDC Active Delay Time 설정 속도 V2(Kph) ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_BUMP_ACT_LOW_SPEED            */		      64,	/* Comment [ Bump 판단을 위한 HDC Active Delay Time 설정 속도 V1(Kph) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_BUMP_G_TH                     */		       0,	/* Comment [ Theta g의 Bump판단 경계값 ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_FLAT_G_TH                     */		       3,	/* Comment [ Theta g의 평지판단 경계값 ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_HDC_FLAT_PRE_CUR                  */		     400,	/* Comment [ 평지에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* uint16_t    	U16_HDC_NO_PRE_CUR                    */		     400,	/* Comment [ 제어 해제 기준 TC v/v 전류 (default : 250mA) ] */
	/* uchar8_t    	U8_HDC_BUMP_DELAY_ACT_TIME            */		      37,	/* Comment [ Bump 추정으로 delay되는 진입시간 ] */
	                                                        		        	/* ScaleVal[   0.37 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_FLAT_G_D_EST_TIME              */		     140,	/* Comment [ Theta g의 평지판단 추정시간(전진) ] */
	                                                        		        	/* ScaleVal[    1.4 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_FLAT_G_R_EST_TIME              */		     200,	/* Comment [ Theta g의 평지판단 추정시간(후진) ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_FLAT_PRE_D_EST_TIME            */		      60,	/* Comment [ 평지 최대 전류값을 이용한 평지판단 추정시간(전진) ] */
	                                                        		        	/* ScaleVal[    0.6 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HDC_FLAT_PRE_R_EST_TIME            */		      60,	/* Comment [ 평지 최대 전류값을 이용한 평지판단 추정시간(후진) ] */
	                                                        		        	/* ScaleVal[    0.6 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_D             */		     300,	/* Comment [ AT 평지 R단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_LD            */		     300,	/* Comment [ AT 4L 평지 R단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_LN            */		     300,	/* Comment [ AT 4L 평지 N단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_LR            */		     300,	/* Comment [ AT 4L 평지 1단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_N             */		     300,	/* Comment [ AT 평지 N단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_FLAT_PRE_CUR_R             */		     300,	/* Comment [ AT 평지 1단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_1             */		     300,	/* Comment [ MT 평지 1단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_2             */		     300,	/* Comment [ MT 평지 2단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_3             */		     300,	/* Comment [ MT 평지 3단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_4             */		     300,	/* Comment [ MT 평지 4단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_5             */		     300,	/* Comment [ MT 평지 5단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_6             */		     300,	/* Comment [ MT 평지 6단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L1            */		     300,	/* Comment [ MT 4L 평지 1단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L2            */		     300,	/* Comment [ MT 4L 평지 2단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L3            */		     300,	/* Comment [ MT 4L 평지 3단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L4            */		     300,	/* Comment [ MT 4L 평지 4단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L5            */		     300,	/* Comment [ MT 4L 평지 5단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_L6            */		     300,	/* Comment [ MT 4L 평지 6단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_LN            */		     300,	/* Comment [ MT 4L 평지 N단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_LR            */		     300,	/* Comment [ MT 4L 평지 R단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_N             */		     300,	/* Comment [ MT 평지 N단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_MT_FLAT_PRE_CUR_R             */		     300,	/* Comment [ MT 평지 R단에서 HDC 압력제어가 수행될 경우 최대 전류값 ] */
	/* int16_t     	S16_HDC_AT_CREEP_SPD_REF              */		      56,	/* Comment [ AT D단 평지 Creeping Speed ] */
	                                                        		        	/* ScaleVal[     7 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_AT_CREEP_SPD_REF_LOW          */		      24,	/* Comment [ AT 4L D단 평지 Creeping Speed ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MT_CREEP_SPD_REF              */		      56,	/* Comment [ MT 1단 평지 Creeping Speed ] */
	                                                        		        	/* ScaleVal[     7 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_DEADZONE_L                    */		      -2,	/* Comment [ 속도제어기 deadzone을 규정하는 하위 속도 값 ] */
	                                                        		        	/* ScaleVal[  -0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_DEADZONE_U                    */		       2,	/* Comment [ 속도제어기 deadzone을 규정하는 상위 속도 값 ] */
	                                                        		        	/* ScaleVal[   0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MAX               */		     210,	/* Comment [ 속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MAX_A_ABS         */		     210,	/* Comment [ 속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정, 단 모든 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MAX_P_ABS         */		     210,	/* Comment [ 속도제어 최대 주기, 이 값을 제어 입력으로 나누어 최종 주기 결정, 단 어떤 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MIN               */		       1,	/* Comment [ 속도제어 최소 주기 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MIN_A_ABS         */		       2,	/* Comment [ 속도제어 최소 주기, 단 모든 Wheel이 ABS 제어인 경우 ] */
	/* uint16_t    	U16_HDC_CTRL_PERIOD_MIN_P_ABS         */		       1,	/* Comment [ 속도제어 최소 주기, 단 어떤 Wheel이 ABS 제어인 경우 ] */
	/* int16_t     	S16_HDC_ME_MED_DGAIN                  */		     100,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_HDC_ME_MED_DGAIN_4L               */		     100,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 D gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_MED_DGAIN_N                */		     100,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 D gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_MED_PGAIN                  */		       3,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_HDC_ME_MED_PGAIN_4L               */		       3,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 P gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_MED_PGAIN_N                */		       3,	/* Comment [ 속도제어시 음(-)의 에러, 음(-)의 에러미분에 적용되는 P gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_PED_DGAIN                  */		      90,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_HDC_ME_PED_DGAIN_4L               */		      90,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 D gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_PED_DGAIN_N                */		      90,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 D gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_PED_PGAIN                  */		       1,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_HDC_ME_PED_PGAIN_4L               */		       1,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 P gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_ME_PED_PGAIN_N                */		       1,	/* Comment [ 속도제어시 음(-)의 에러, 양(+)의 에러미분에 적용되는 P gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_MED_DGAIN                  */		     180,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_HDC_PE_MED_DGAIN_4L               */		     180,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 D gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_MED_DGAIN_N                */		     180,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 D gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_MED_PGAIN                  */		       2,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_HDC_PE_MED_PGAIN_4L               */		       2,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 P gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_MED_PGAIN_N                */		       2,	/* Comment [ 속도제어시 양(+)의 에러, 음(-)의 에러미분에 적용되는 P gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_PED_DGAIN                  */		     250,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 D gain ] */
	/* int16_t     	S16_HDC_PE_PED_DGAIN_4L               */		     250,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 D gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_PED_DGAIN_N                */		     250,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 D gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_PED_PGAIN                  */		       7,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 P gain ] */
	/* int16_t     	S16_HDC_PE_PED_PGAIN_4L               */		       7,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 P gain(단 4L Gear Mode시) ] */
	/* int16_t     	S16_HDC_PE_PED_PGAIN_N                */		       7,	/* Comment [ 속도제어시 양(+)의 에러, 음(+)의 에러미분에 적용되는 P gain(단 N Gear Mode시) ] */
	/* int16_t     	S16_HDC_INITIAL_TARGET_V              */		    3000,	/* Comment [ HDC 제어 초기 Speed control시 최소 모터 구동 전압 ] */
	                                                        		        	/* ScaleVal[     3 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_MSC_1ST_ERR                   */		     -12,	/* Comment [ HDC MSC제어 첫번째 포인트의 에러값 ] */
	                                                        		        	/* ScaleVal[   -1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MSC_1ST_VOL                   */		    2500,	/* Comment [ HDC MSC제어 첫번째 포인트의 전압값 ] */
	                                                        		        	/* ScaleVal[    2.5 Vol ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_HDC_MSC_2ND_ERR                   */		      -2,	/* Comment [ HDC MSC제어 두번째 포인트의 에러값 ] */
	                                                        		        	/* ScaleVal[  -0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_MSC_2ND_VOL                   */		    1000,	/* Comment [ HDC MSC제어 두번째 포인트의 전압값 ] */
	                                                        		        	/* ScaleVal[      1 Vol ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Hdc_Ctrl_Over_Speed_Target_G_1st  */		       2,	/* Comment [ HDC Target Speed 이상 Decel Ctrl시 Spd Error 0kph 에서의 Decel 값 [default:0.02g] ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Over_Speed_Target_G_2nd  */		       5,	/* Comment [ HDC Target Speed 이상 Decel Ctrl시 Spd Error 1kph 에서의 Decel 값 [default:0.05g] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Over_Speed_Target_G_3rd  */		      10,	/* Comment [ HDC Target Speed 이상 Decel Ctrl시 Spd Error 4kph 에서의 Decel 값 [default:0.10g] ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Over_Speed_Target_G_4th  */		      15,	/* Comment [ HDC Target Speed 이상 Decel Ctrl시 Spd Error 8kph 에서의 Decel 값 [default:0.15g] ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Over_Speed_Target_G_5th  */		      20,	/* Comment [ HDC Target Speed 이상 Decel Ctrl시 Spd Error 16kph 에서의 Decel 값 [default:0.20g] ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Under_Speed_Target_G_1st */		       2,	/* Comment [ HDC Target Speed 이하 Decel Ctrl시 Spd Error 0kph 에서의 Decel 값 [default:0.03g] ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Under_Speed_Target_G_2nd */		       3,	/* Comment [ HDC Target Speed 이하 Decel Ctrl시 Spd Error 1kph 에서의 Decel 값 [default:0.04g] ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Hdc_Ctrl_Under_Speed_Target_G_3rd */		       5,	/* Comment [ HDC Target Speed 이하 Decel Ctrl시 Spd Error 4kph 에서의 Decel 값 [default:0.05g] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HDC_INITIAL_CURRENT               */		     550,	/* Comment [ HDC 제어시 TCMF 제어 초기 전류값 [default:0.45mA] ] */
	/* int16_t     	S16_HDC_MT_DGEAR_RPM_DIFF_HIGH_REF    */		      30,	/* Comment [ MT gear down shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* int16_t     	S16_HDC_MT_DGEAR_RPM_DIFF_LOW_REF     */		      10,	/* Comment [ MT gear down shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* int16_t     	S16_HDC_MT_DGEAR_RPM_DIFF_MID_REF     */		      20,	/* Comment [ MT gear down shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* int16_t     	S16_HDC_MT_UGEAR_RPM_DIFF_HIGH_REF    */		      60,	/* Comment [ MT gear up shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* int16_t     	S16_HDC_MT_UGEAR_RPM_DIFF_LOW_REF     */		      15,	/* Comment [ MT gear up shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* int16_t     	S16_HDC_MT_UGEAR_RPM_DIFF_MID_REF     */		      30,	/* Comment [ MT gear up shift / clutch disengage 상태 동력전달 판단을 위한 RPM과 추정RPM의 차 ] */
	/* uchar8_t    	U8_HDC_MT_P_ENGAGE_GEAR_DOWN          */		      40,	/* Comment [ MT gear down shift 동력 전달 판단 기준값 ] */
	/* uchar8_t    	U8_HDC_MT_P_ENGAGE_GEAR_UP            */		      40,	/* Comment [ MT gear up shift 동력 전달 판단 기준값 ] */
	/* uchar8_t    	U8_HDC_TARGET_SPD_ADAPT_ENABLE        */		       1,	/* Comment [ HDC target speed adaptation by brk/acc enble (0:disable, 1:enable) ] */
	/* uchar8_t    	U8_HDC_TARGET_SPD_ADAPT_MODE          */		       1,	/* Comment [ HDC target speed adaptation concept (1:act at brk/acc, 2:de-act brk/acc) ] */
	/* int16_t     	S16_HDC_CONTROL_MIN_CURRENT           */		     250,	/* Comment [ HDC 작동 전구간, TC 전류 Low Limit (default 250mA) ] */
	/* int16_t     	S16_HDC_LIMIT_CUR_AT_ACC_DUMP         */		     300,	/* Comment [ 가속 페달에 의한 덤프 시 TC 전류 Low Limit (default 300mA) ] */
	/* int16_t     	S16_HDC_RISE_CURRENT_INIT             */		     340,	/* Comment [ 증압 모드 시, TC 전류 Low Limit (default 340mA) ] */
	/* int16_t     	S16_HDC_TC_CUR_AT_MP10bar             */		     290,	/* Comment [ 제어 중, brake pedal 10bar 밟을 시 인가되는 TC 전류 (default 290mA) ] */
	/* int16_t     	S16_HDC_TC_CUR_AT_MP20bar             */		     330,	/* Comment [ 제어 중, brake pedal 20bar 밟을 시 인가되는 TC 전류 (default 330mA) ] */
	/* int16_t     	S16_HDC_TC_CUR_AT_MP30bar             */		     360,	/* Comment [ 제어 중, brake pedal 30bar 밟을 시 인가되는 TC 전류 (default 360mA) ] */
	/* uchar8_t    	U8_HDC_RISE_CURRENT_COMP              */		       5,	/* Comment [ HDC 증압 모드 시, TC 전류 증가량 (default 5mA) ] */
	/* uchar8_t    	U8_HDC_STALL_DUMP_RATE_10P            */		      15,	/* Comment [ stall 판단에 의한 fast dump 시, 10% 경사 TC 감소량 (default 15mA) ] */
	/* uchar8_t    	U8_HDC_STALL_DUMP_RATE_20P            */		       7,	/* Comment [ stall 판단에 의한 fast dump 시, 20% 경사 TC 감소량 (default 7mA) ] */
	/* uchar8_t    	U8_HDC_STALL_DUMP_RATE_30P            */		       4,	/* Comment [ stall 판단에 의한 fast dump 시, 30% 경사 TC 감소량 (default 4mA) ] */
	/* int16_t     	S16_HDC_TAR_SPD_GAP_AT_HIG_ACC        */		       8,	/* Comment [ gap of target speed with vehicle speed at max. accel (default 1KPH) ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_TAR_SPD_GAP_AT_LOW_ACC        */		       4,	/* Comment [ gap of target speed with vehicle speed at min. accel (default 0.5KPH) ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HDC_TAR_SPD_GAP_AT_MED_ACC        */		       6,	/* Comment [ gap of target speed with vehicle speed at mid. accel (default 0.75KPH) ] */
	                                                        		        	/* ScaleVal[   0.75 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HDC_TAR_SPD_REF_HIG_ACC            */		      70,	/* Comment [ max. accel pedal Th. For target speed increase (default 70%) ] */
	/* uchar8_t    	U8_HDC_TAR_SPD_REF_LOW_ACC            */		       5,	/* Comment [ min. accel pedal Th. For target speed increase (default 5%) ] */
	/* uchar8_t    	U8_HDC_TAR_SPD_REF_MED_ACC            */		      30,	/* Comment [ mid. accel pedal Th. For target speed increase (default 30%) ] */
	/* int16_t     	S16_CBC_BRAKING_DET_THRES             */		     150,	/* Comment [ Min Mpress for Partial brake detect SET, default 15bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_BRAKING_MIN_THRES             */		      50,	/* Comment [ Min Mpress for Partial brake detect RESET, default 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_MPRESS_ENTER                  */		     800,	/* Comment [ Mpress level for CBC enter condition ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_H_FAST_STR     */		     300,	/* Comment [ Ay level for CBC enter at high speed with fast steer ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_H_SLOW_STR     */		     400,	/* Comment [ Ay level for CBC enter at high speed with slow steer ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_M_FAST_STR     */		     600,	/* Comment [ Ay level for CBC enter at middle speed with fast steer ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_M_SLOW_STR     */		     600,	/* Comment [ Ay level for CBC enter at middle speed with slow steer ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_S_FAST_STR     */		    1000,	/* Comment [ Ay level for CBC enter at low speed with fast steer ] */
	                                                        		        	/* ScaleVal[        1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_ALAT_ENTER_SPD_S_SLOW_STR     */		    1000,	/* Comment [ Ay level for CBC enter at low speed with slow steer ] */
	                                                        		        	/* ScaleVal[        1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_CBC_ARAD_THRES_COMP_H              */		       3,	/* Comment [ CBC start wheel deceleration condition compensation at high vehicle speed ] */
	                                                        		        	/* ScaleVal[     0.75 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_CBC_ARAD_THRES_COMP_M              */		       1,	/* Comment [ CBC start wheel deceleration condition compensation at med vehicle speed ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_CBC_ARAD_THRES_H                   */		       2,	/* Comment [ CBC start wheel deceleration threshold at low Ay level ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_CBC_ARAD_THRES_L                   */		       5,	/* Comment [ CBC start wheel deceleration threshold at high Ay level ] */
	                                                        		        	/* ScaleVal[     1.25 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_CBC_ARAD_THRES_M                   */		       3,	/* Comment [ CBC start wheel deceleration threshold at med Ay level ] */
	                                                        		        	/* ScaleVal[     0.75 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* int16_t     	S16_CBC_DECEL_THRES_COMP_H            */		      12,	/* Comment [ CBC start decel condition compensation at high vehicle speed ] */
	                                                        		        	/* ScaleVal[    1.5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_DECEL_THRES_COMP_M            */		       8,	/* Comment [ CBC start decel condition compensation at med vehicle speed ] */
	                                                        		        	/* ScaleVal[      1 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_DECEL_THRES_H                 */		      16,	/* Comment [ CBC start decel(verf-vrad) threshold at high Ay level ] */
	                                                        		        	/* ScaleVal[      2 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_DECEL_THRES_L                 */		      32,	/* Comment [ CBC start decel(verf-vrad) threshold at low Ay level ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_DECEL_THRES_M                 */		      24,	/* Comment [ CBC start decel(verf-vrad) threshold at med Ay level ] */
	                                                        		        	/* ScaleVal[      3 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* char8_t     	S8_CBC_SLIP_THRES_COMP_H              */		       2,	/* Comment [ CBC start slip ratio condition compensation at high vehicle speed ] */
	/* char8_t     	S8_CBC_SLIP_THRES_COMP_M              */		       1,	/* Comment [ CBC start slip ratio condition compensation at med vehicle speed ] */
	/* char8_t     	S8_CBC_SLIP_THRES_H                   */		       3,	/* Comment [ CBC start slip ratio threshold at high Ay level ] */
	/* char8_t     	S8_CBC_SLIP_THRES_L                   */		       5,	/* Comment [ CBC start slip ratio threshold at low Ay level ] */
	/* char8_t     	S8_CBC_SLIP_THRES_M                   */		       4,	/* Comment [ CBC start slip ratio threshold at med Ay level ] */
	/* int16_t     	S16_CBC_ALAT_EXIT                     */		     200,	/* Comment [ Ay level for CBC exit ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_MPRESS_EXIT                   */		    1000,	/* Comment [ Mpress level for CBC exit condition ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_LEVEL_H      */		     800,	/* Comment [ Reference high Ay level for Rear delta pressure control ] */
	                                                        		        	/* ScaleVal[      0.8 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_LEVEL_M      */		     600,	/* Comment [ Reference middle Ay level for Rear delta pressure control ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CBC_COMP_MAX_DEL_YAW              */		     300,	/* Comment [ reference del yaw for S16_CBC_COMP_MAX_PRESS(default : 3) ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_COMP_MAX_PRESS                */		      50,	/* Comment [ Additional pressure to reduce target pressure at max del yaw ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_COMP_MIN_DEL_YAW              */		     200,	/* Comment [ reference del yaw for S16_CBC_COMP_MIN_PRESS(default : 2) ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_COMP_MIN_PRESS                */		      20,	/* Comment [ Additional pressure to reduce target pressure at min del yaw ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_H_Spd_H      */		     500,	/* Comment [ Rear delta pressure at high Ay level and high speed ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_H_Spd_M      */		     500,	/* Comment [ Rear delta pressure at high Ay level and middle speed ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_H_Spd_S      */		     400,	/* Comment [ Rear delta pressure at high Ay level and low speed ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_M_Spd_H      */		     500,	/* Comment [ Rear delta pressure at middle Ay level and high speed ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_M_Spd_M      */		     400,	/* Comment [ Rear delta pressure at middle Ay level and middle speed ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_M_Spd_S      */		     300,	/* Comment [ Rear delta pressure at middle Ay level and low speed ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_S_Spd_H      */		     400,	/* Comment [ Rear delta pressure at small Ay level and high speed ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_S_Spd_M      */		     300,	/* Comment [ Rear delta pressure at small Ay level and middle speed ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRESS_ALAT_S_Spd_S      */		     200,	/* Comment [ Rear delta pressure at small Ay level and low speed ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRES_MAX_DUMP           */		     100,	/* Comment [ Minimum pressure difference for full dump ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_DELTA_PRES_MAX_RISE           */		     200,	/* Comment [ Minimum pressure difference for full rise ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_CBC_REAPPLY_TIME_FADEOUT_R         */		       7,	/* Comment [ Rear conventional reapply time of one pulse up for CBC fade out ] */
	/* char8_t     	S8_CBC_REAPPLY_TIME_R                 */		      15,	/* Comment [ Rear conventional reapply time of one pulse up for CBC ] */
	/* int16_t     	S16_CBC_FADE_OUT_HOLD_SCAN            */		       0,	/* Comment [ Hold scan for pulse up rise at fade out mode  ] */
	/* int16_t     	S16_CBC_FADE_OUT_TIME                 */		      29,	/* Comment [ Time length for fade out mode after CBC reset ] */
	                                                        		        	/* ScaleVal[   0.29 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_CBC_FRONT_ENTER_DV_SPD_H          */		     120,	/* Comment [ Min. delta V of front wheel for CBC enter condition of a front wheel at high speed, default 15km/h ] */
	                                                        		        	/* ScaleVal[    15 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_ENTER_DV_SPD_M          */		      96,	/* Comment [ Min. delta V of front wheel for CBC enter condition of a front wheel at middle speed, default 12km/h ] */
	                                                        		        	/* ScaleVal[    12 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_ENTER_DV_SPD_S          */		      80,	/* Comment [ Min. delta V of front wheel for CBC enter condition of a front wheel at low speed, default 10km/h ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_EXIT_DV_SPD_H           */		      40,	/* Comment [ Max. delta V of front wheel for CBC exit condition of a front wheel at high speed ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_EXIT_DV_SPD_M           */		      32,	/* Comment [ Max. delta V of front wheel for CBC exit condition of a front wheel at middle speed ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_EXIT_DV_SPD_S           */		      32,	/* Comment [ Max. delta V of front wheel for CBC exit condition of a front wheel at low speed ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_MEDIUM_DV_SPD_H         */		      64,	/* Comment [ Delta V of front wheel for slip control of a front wheel at high speed ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_MEDIUM_DV_SPD_M         */		      48,	/* Comment [ Delta V of front wheel for slip control of a front wheel at middle speed ] */
	                                                        		        	/* ScaleVal[     6 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_MEDIUM_DV_SPD_S         */		      48,	/* Comment [ Delta V of front wheel for slip control of a front wheel at low speed ] */
	                                                        		        	/* ScaleVal[     6 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_H_SPD_H       */		     100,	/* Comment [ Front delta pressure at large del V level and high speed ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_H_SPD_M       */		     100,	/* Comment [ Front delta pressure at large del V level and middle speed ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_H_SPD_S       */		      50,	/* Comment [ Front delta pressure at large del V level and low speed ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_S_SPD_H       */		      50,	/* Comment [ Front delta pressure at small del V level and high speed ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_S_SPD_M       */		      50,	/* Comment [ Front delta pressure at small del V level and middle speed ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_FRONT_DEL_PRESS_S_SPD_S       */		      30,	/* Comment [ Front delta pressure at small del V level and low speed ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_REAR_ENTER_DV_SPD_H           */		      96,	/* Comment [ Delta V of rear wheel for CBC slip control at high speed, default value = 12kph ] */
	                                                        		        	/* ScaleVal[    12 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_REAR_ENTER_DV_SPD_M           */		      80,	/* Comment [ Delta V of rear wheel for CBC slip control at middle speed, default value = 10kph ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_REAR_ENTER_DV_SPD_S           */		      64,	/* Comment [ Delta V of rear wheel for CBC slip control at low speed, default value = 8kph ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_REAR_EXIT_DV                  */		      40,	/* Comment [ Delta V of rear wheel for CBC slip control exit, default value = 5kph ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_CBC_REAR_SLIP_DEL_P_SPD_H         */		     200,	/* Comment [ Additional rear delta pressure for slip control at high speed, default value = 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_REAR_SLIP_DEL_P_SPD_M         */		     150,	/* Comment [ Additional rear delta pressure for slip control at middle speed, default value = 15bar ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_REAR_SLIP_DEL_P_SPD_S         */		     100,	/* Comment [ Additional rear delta pressure for slip control at low speed, default value = 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CBC_RESERVED_01                   */		       0,
	/* int16_t     	S16_CBC_RESERVED_02                   */		       0,
	/* int16_t     	S16_CBC_RESERVED_03                   */		       0,
	/* int16_t     	S16_CBC_RESERVED_04                   */		       0,
	/* int16_t     	S16_CBC_RESERVED_05                   */		       0,
	/* int16_t     	S16_CBC_RESERVED_06                   */		       0,
	/* char8_t     	S8_CBC_RESERVED_01                    */		       0,
	/* char8_t     	S8_CBC_RESERVED_02                    */		       0,
	/* char8_t     	S8_CBC_RESERVED_03                    */		       0,
	/* char8_t     	S8_CBC_RESERVED_04                    */		       0,
	/* char8_t     	S8_CBC_RESERVED_05                    */		       0,
	/* char8_t     	S8_CBC_RESERVED_06                    */		       0,
	/* int16_t     	S16_RI_THRES                          */		      95,	/* Comment [ ROP시, Rollover Index 에 대한 판단 threshold : 이 값을 넘으면, TTL 의 시간을 줄임. ] */
	/* int16_t     	S16_ROP_PRE_FRT_ENT_WSTR_TH           */		      50,	/* Comment [ At ROP, Pre Front Activation Wstr, Default = 5 ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TTL_max                           */		      71,	/* Comment [ ROP시, Time To Lift 최대 시간 ] */
	/* int16_t     	S16_TTL_min                           */		       0,	/* Comment [ ROP시, Time To Lift 최소 시간 ] */
	/* int16_t     	S16_TTL_thres                         */		      45,	/* Comment [ ROP시, Time To Lift 기준 시간 ] */
	/* int16_t     	S16_SS_PHI_DOT_TH_1st                 */		     464,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       46.4 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SS_PHI_DOT_TH_2nd                 */		     283,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       28.3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SS_PHI_DOT_TH_3rd                 */		     423,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       42.3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SS_PHI_DOT_TH_4th                 */		     400,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SS_PHI_TH_1st                     */		     513,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       5.13 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SS_PHI_TH_2nd                     */		     574,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       5.74 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SS_PHI_TH_3rd                     */		     440,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[        4.4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SS_PHI_TH_4th                     */		     486,	/* Comment [ ROP 판단 : Steady state threshold ] */
	                                                        		        	/* ScaleVal[       4.86 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DYN_PHI_DOT_TH_1st                */		     384,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[       38.4 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DYN_PHI_DOT_TH_2nd                */		     390,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[         39 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DYN_PHI_DOT_TH_3rd                */		     423,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[       42.3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DYN_PHI_DOT_TH_4th                */		     400,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DYN_PHI_TH_1st                    */		     460,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[        4.6 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DYN_PHI_TH_2nd                    */		     459,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[       4.59 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DYN_PHI_TH_3rd                    */		     440,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[        4.4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DYN_PHI_TH_4th                    */		     486,	/* Comment [ ROP 판단 : Dynamic Handling threshold ] */
	                                                        		        	/* ScaleVal[       4.86 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_TH_1st             */		     300,	/* Comment [ ROP 판단 : 횡g 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.3 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_TH_2nd             */		     600,	/* Comment [ ROP 판단 : 횡g 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.6 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_TH_3rd             */		     700,	/* Comment [ ROP 판단 : 횡g 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.7 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_TH_4th             */		     850,	/* Comment [ ROP 판단 : 횡g 진입 조건 ] */
	                                                        		        	/* ScaleVal[       0.85 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_TH_5th             */		    1000,	/* Comment [ ROP 판단 : 횡g 진입 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_WEG_1st            */		       0,	/* Comment [ ROP 판단 : 횡g 조건에 대한 가중치 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_WEG_2nd            */		      90,	/* Comment [ ROP 판단 : 횡g 조건에 대한 가중치 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_WEG_3rd            */		     100,	/* Comment [ ROP 판단 : 횡g 조건에 대한 가중치 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_WEG_4th            */		     110,	/* Comment [ ROP 판단 : 횡g 조건에 대한 가중치 ] */
	/* int16_t     	S16_ROP_ENTER_ALAT_WEG_5th            */		     130,	/* Comment [ ROP 판단 : 횡g 조건에 대한 가중치 ] */
	/* int16_t     	S16_DYN_WSTR_DOT_MIN                  */		    2000,	/* Comment [ ROP 판단 : Dynamic Handling 최소 조향각속도 ] */
	                                                        		        	/* ScaleVal[        200 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SS_WSTR_DOT_MAX                   */		    1500,	/* Comment [ ROP 판단 : Steady state 최대 조향각속도 ] */
	                                                        		        	/* ScaleVal[        150 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	S16_ROP_RI_THRES_VEL_100K             */		     100,	/* Comment [ RI_THRES 값의 속도에 대한 가중치. 값을 키우면, 100kph 에서 ROP 제어 둔감. 5 이상씩 변화. default = 100, [10~250] ] */
	/* uchar8_t    	S16_ROP_RI_THRES_VEL_20K              */		     100,	/* Comment [ RI_THRES 값의 속도에 대한 가중치. 값을 키우면, 20kph 에서 ROP 제어 둔감. 5 이상씩 변화. default = 100, [10~250] ] */
	/* uchar8_t    	S16_ROP_RI_THRES_VEL_40K              */		     100,	/* Comment [ RI_THRES 값의 속도에 대한 가중치. 값을 키우면, 40kph 에서 ROP 제어 둔감. 5 이상씩 변화. default = 100, [10~250] ] */
	/* uchar8_t    	S16_ROP_RI_THRES_VEL_60K              */		     100,	/* Comment [ RI_THRES 값의 속도에 대한 가중치. 값을 키우면, 60kph 에서 ROP 제어 둔감. 5 이상씩 변화. default = 100, [10~250] ] */
	/* uchar8_t    	S16_ROP_RI_THRES_VEL_80K              */		     100,	/* Comment [ RI_THRES 값의 속도에 대한 가중치. 값을 키우면, 80kph 에서 ROP 제어 둔감. 5 이상씩 변화. default = 100, [10~250] ] */
	/* uchar8_t    	U8_ROP_AY_VX_DOT_1ST_WEG              */		     160,	/* Comment [ At ROP Detection, Weight of Ay_Vx_Dot, [Default = 160] ] */
	/* uchar8_t    	U8_ROP_AY_VX_DOT_2ND_WEG              */		     100,	/* Comment [ At ROP Detection, Weight of Ay_Vx_Dot, [Default = 100] ] */
	/* uchar8_t    	U8_ROP_AY_VX_DOT_WEG                  */		     180,	/* Comment [ At ROP Detection, Weight of Ay_Vx_Dot, [Default = 180] ] */
	/* int16_t     	S16_ROP_MODEL_MU_LEVEL                */		     750,	/* Comment [ ROP시, Model_limit 노면한계 설정 ] */
	                                                        		        	/* ScaleVal[       0.75 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_MU_LEVEL_AY                   */		     700,	/* Comment [ ROP 제어 : 목표 Ay 설정 ] */
	                                                        		        	/* ScaleVal[        0.7 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_AY_P_GAIN_FED_FOWR                */		      20,	/* Comment [ ROP 제어 : 목표 Ay Feedforwaed P 게인 ] */
	/* int16_t     	S16_AYC_D_GAIN_OUTSIDE_DEC            */		       2,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_D_GAIN_OUTSIDE_INC            */		      10,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_P_GAIN_OUTSIDE_DEC            */		      18,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_P_GAIN_OUTSIDE_INC            */		      18,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_D_GAIN_INSIDE_DEC             */		      10,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_D_GAIN_INSIDE_INC             */		       5,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_P_GAIN_INSIDE_DEC             */		       4,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_P_GAIN_INSIDE_INC             */		       4,	/* Comment [ ROP 제어 : 목표 Ay Feedback P/D 게인 ] */
	/* int16_t     	S16_AYC_MQ_WEG_VEL_100K               */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Moment_Q_rop 에 대한 속도 가중치. 값을 키우면, 100kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 100, [0~500] ] */
	/* int16_t     	S16_AYC_MQ_WEG_VEL_20K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Moment_Q_rop 에 대한 속도 가중치. 값을 키우면, 20kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 20, [0~500] ] */
	/* int16_t     	S16_AYC_MQ_WEG_VEL_40K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Moment_Q_rop 에 대한 속도 가중치. 값을 키우면, 40kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 100, [0~500] ] */
	/* int16_t     	S16_AYC_MQ_WEG_VEL_60K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Moment_Q_rop 에 대한 속도 가중치. 값을 키우면, 60kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 100, [0~500] ] */
	/* int16_t     	S16_AYC_MQ_WEG_VEL_80K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Moment_Q_rop 에 대한 속도 가중치. 값을 키우면, 80kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 100, [0~500] ] */
	/* int16_t     	S16_AY_REF_DYN_WEG_VEL_100K           */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 100kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 90, [0~500] ] */
	/* int16_t     	S16_AY_REF_DYN_WEG_VEL_20K            */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 20kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 120, [0~500] ] */
	/* int16_t     	S16_AY_REF_DYN_WEG_VEL_40K            */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 40kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 110, [0~500] ] */
	/* int16_t     	S16_AY_REF_DYN_WEG_VEL_60K            */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 60kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 90, [0~500] ] */
	/* int16_t     	S16_AY_REF_DYN_WEG_VEL_80K            */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 80kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 90, [0~500] ] */
	/* int16_t     	S16_AY_REF_MAX_OFFSET                 */		      50,	/* Comment [ ROP 제어, TTL 에 따른 Ay_Ref 의 변동값. TTL_time 이 S16_TTL_max 이면, S16_ROP_MU_LEVEL_AY + (해당값) 으로 제어량 감소 효과. 0.01 이상씩 변화. default = 0.05, [0~0.5] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.5 ] */
	/* int16_t     	S16_AY_REF_MIN_OFFSET                 */		      50,	/* Comment [ ROP 제어, TTL 에 따른 Ay_Ref 의 변동값. TTL_time 이 S16_TTL_min 이면, S16_ROP_MU_LEVEL_AY - 해당값 으로 제어량 증대 효과. 0.01 이상씩 변화. default = 0.05, [0~0.5] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.5 ] */
	/* int16_t     	S16_AY_REF_WEG_VEL_100K               */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 100kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 100, [0~500] ] */
	/* int16_t     	S16_AY_REF_WEG_VEL_20K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 20kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 150, [0~500] ] */
	/* int16_t     	S16_AY_REF_WEG_VEL_40K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 40kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 140, [0~500] ] */
	/* int16_t     	S16_AY_REF_WEG_VEL_60K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 60kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 130, [0~500] ] */
	/* int16_t     	S16_AY_REF_WEG_VEL_80K                */		     100,	/* Comment [ ROP 제어, Ay_Control 시 Ay_Ref 에 대한 속도 가중치. 값을 키우면, 80kph 에서 ROP 제어량 감소. 5 이상씩 변화. default = 120, [0~500] ] */
	/* int16_t     	S16_PRE_FRONT_ROP_M_WEG               */		     110,	/* Comment [ 조향각 Counter시, 반대 Front wheel 제어량 가중치 ] */
	/* int16_t     	S16_ROP_DUMP_ALAT_1st                 */		     500,	/* Comment [ ROP 제어 : Rate_Interval 설정 횡g(1) ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_DUMP_ALAT_2nd                 */		     600,	/* Comment [ ROP 제어 : Rate_Interval 설정 횡g(2) ] */
	                                                        		        	/* ScaleVal[        0.6 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_DUMP_ALAT_3rd                 */		     800,	/* Comment [ ROP 제어 : Rate_Interval 설정 횡g(3) ] */
	                                                        		        	/* ScaleVal[        0.8 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROP_DUMP_RATE_1st                 */		      10,	/* Comment [ ROP 제어 : Rate_Interval 설정(1) ] */
	/* int16_t     	S16_ROP_DUMP_RATE_2nd                 */		       7,	/* Comment [ ROP 제어 : Rate_Interval 설정(2) ] */
	/* int16_t     	S16_ROP_DUMP_RATE_3rd                 */		       7,	/* Comment [ ROP 제어 : Rate_Interval 설정(3) ] */
	/* int16_t     	S16_ROP_DUMP_RATE_ADD                 */		       5,	/* Comment [ ROP 제어 : 추가 Add Rate_Interval 설정 ] */
	/* int16_t     	S16_ROP_DUMP_RATE_SLIP_MAX            */		   -7000,	/* Comment [ ROP 제어 : Rate_Interval 상황에서 최대 슬립 ] */
	                                                        		        	/* ScaleVal[        -70 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ROP_DUMP_RATE_SLIP_MIN            */		   -3000,	/* Comment [ ROP 제어 : Rate_Interval 상황에서 적용 슬립 ] */
	                                                        		        	/* ScaleVal[        -30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ROP_PULSE_DOWN_DUMP_RATE          */		      15,	/* Comment [ ROP 제어 : ESP 이후 dump_rate 기울기 설정 ] */
	/* int16_t     	S16_ROP_DUMP_DYN_WEG_VEL_100K         */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [100kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_DYN_WEG_VEL_20K          */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [20kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_DYN_WEG_VEL_40K          */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [40kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_DYN_WEG_VEL_60K          */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [60kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_DYN_WEG_VEL_80K          */		     100,	/* Comment [ ROP 제어, Dynamic Handling Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [80kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_WEG_VEL_100K             */		     100,	/* Comment [ ROP 제어, Stationary Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [100kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_WEG_VEL_20K              */		     100,	/* Comment [ ROP 제어, Stationary Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [20kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_WEG_VEL_40K              */		     100,	/* Comment [ ROP 제어, Stationary Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [40kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_WEG_VEL_60K              */		     100,	/* Comment [ ROP 제어, Stationary Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [60kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_ROP_DUMP_WEG_VEL_80K              */		     100,	/* Comment [ ROP 제어, Stationary Ay_Control 시 해당속도별 dump_rate 에 대한 가중치. [80kph]. 5 이상씩 변화. default = 100, [0~1000] ] */
	/* int16_t     	S16_AYC_1ST_CYCLE_D_IN_WEG            */		     150,	/* Comment [ ROP 제어, Ay Control / 1st_Cycle 에서 D_in 게인에 대한 가중치. 5 이상씩 변화. default = 150, [0~500] ] */
	/* int16_t     	S16_AYC_1ST_CYCLE_D_OUT_WEG           */		     150,	/* Comment [ ROP 제어, Ay Control / 1st_Cycle 에서 D_out 게인에 대한 가중치. 5 이상씩 변화. default = 150, [0~500] ] */
	/* int16_t     	S16_AYC_1ST_CYCLE_P_IN_WEG            */		     150,	/* Comment [ ROP 제어, Ay Control / 1st_Cycle 에서 P_in 게인에 대한 가중치. 5 이상씩 변화. default = 150, [0~500] ] */
	/* int16_t     	S16_AYC_1ST_CYCLE_P_OUT_WEG           */		     150,	/* Comment [ ROP 제어, Ay Control / 1st_Cycle 에서 P_out 게인에 대한 가중치. 5 이상씩 변화. default = 150, [0~500] ] */
	/* int16_t     	S16_ROP_YAW_DOT_ENT_40K               */		     180,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 진입 조건 ( 40 kph ). 값을 키우면, 40kph 에서 판단 둔감. 0.2 이상씩 변화. default = 2.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 1.8 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_ENT_50K               */		     130,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 진입 조건 ( 50 kph ). 값을 키우면, 50kph 에서 판단 둔감. 0.2 이상씩 변화. default = 2.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 1.3 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_ENT_60K               */		      90,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 진입 조건 ( 60 kph ). 값을 키우면, 60kph 에서 판단 둔감. 0.2 이상씩 변화. default = 2.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.9 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_ENT_70K               */		      70,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 진입 조건 ( 70 kph ). 값을 키우면, 70kph 에서 판단 둔감. 0.2 이상씩 변화. default = 2.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.7 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_ENT_80K               */		      60,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 진입 조건 ( 80 kph ). 값을 키우면, 80kph 에서 판단 둔감. 0.2 이상씩 변화. default = 2.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.6 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_EXT_40K               */		     160,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 해제 조건 ( 40 kph ). 값을 키우면, 40kph 에서 해제 빨라짐. 0.2 이상씩 변화. default = 1.8, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 1.6 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_EXT_50K               */		     110,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 해제 조건 ( 50 kph ). 값을 키우면, 50kph 에서 해제 빨라짐. 0.2 이상씩 변화. default = 1.3, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 1.1 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_EXT_60K               */		      90,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 해제 조건 ( 60 kph ). 값을 키우면, 60kph 에서 해제 빨라짐. 0.2 이상씩 변화. default = 1.0, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.9 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_EXT_70K               */		      70,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 해제 조건 ( 70 kph ). 값을 키우면, 70kph 에서 해제 빨라짐. 0.2 이상씩 변화. default = 0.8, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.7 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_YAW_DOT_EXT_80K               */		      50,	/* Comment [ ROP시, Front Prefill 요레이트_기울기 해제 조건 ( 80 kph ). 값을 키우면, 80kph 에서 해제 빨라짐. 0.2 이상씩 변화. default = 0.5, [0 ~5] ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 5 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_ENT_40K              */		     850,	/* Comment [ ROP시, Front Prefill 조향각속도 진입 조건 ( 40 kph ). 값을 키우면, 40kph 에서 판단 둔감. 30 이상씩 변화. default = 850, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 85 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_ENT_50K              */		     850,	/* Comment [ ROP시, Front Prefill 조향각속도 진입 조건 ( 50 kph ). 값을 키우면, 50kph 에서 판단 둔감. 30 이상씩 변화. default = 850, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 85 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_ENT_60K              */		     850,	/* Comment [ ROP시, Front Prefill 조향각속도 진입 조건 ( 60 kph ). 값을 키우면, 60kph 에서 판단 둔감. 30 이상씩 변화. default = 850, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 85 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_ENT_70K              */		     850,	/* Comment [ ROP시, Front Prefill 조향각속도 진입 조건 ( 70 kph ). 값을 키우면, 70kph 에서 판단 둔감. 30 이상씩 변화. default = 850, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 85 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_ENT_80K              */		     850,	/* Comment [ ROP시, Front Prefill 조향각속도 진입 조건 ( 80 kph ). 값을 키우면, 80kph 에서 판단 둔감. 30 이상씩 변화. default = 850, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 85 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_EXT_40K              */		     800,	/* Comment [ ROP시, Front Prefill 조향각속도 해제 조건 ( 40 kph ). 값을 키우면, 40kph 에서 해제 빨라짐. 30 이상씩 변화. default = 800, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 80 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_EXT_50K              */		     800,	/* Comment [ ROP시, Front Prefill 조향각속도 해제 조건 ( 50 kph ). 값을 키우면, 50kph 에서 해제 빨라짐. 30 이상씩 변화. default = 700, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 80 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_EXT_60K              */		     800,	/* Comment [ ROP시, Front Prefill 조향각속도 해제 조건 ( 60 kph ). 값을 키우면, 60kph 에서 해제 빨라짐. 30 이상씩 변화. default = 600, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 80 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_EXT_70K              */		     800,	/* Comment [ ROP시, Front Prefill 조향각속도 해제 조건 ( 70 kph ). 값을 키우면, 70kph 에서 해제 빨라짐. 30 이상씩 변화. default = 400, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 80 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_WSTR_DOT_EXT_80K              */		     800,	/* Comment [ ROP시, Front Prefill 조향각속도 해제 조건 ( 80 kph ). 값을 키우면, 80kph 에서 해제 빨라짐. 30 이상씩 변화. default = 300, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 80 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_PREFILL_MQ_WEG_VEL_100K       */		     160,	/* Comment [ ROP시, Front Prefill 제어량의 속도에 대한 가중치 값. 100kph 에서의 가중치. 5% 이상씩 변화. default = 100, [0 ~200] ] */
	/* int16_t     	S16_ROP_PREFILL_MQ_WEG_VEL_20K        */		      60,	/* Comment [ ROP시, Front Prefill 제어량의 속도에 대한 가중치 값. 20kph 에서의 가중치. 5% 이상씩 변화. default = 20, [0 ~200] ] */
	/* int16_t     	S16_ROP_PREFILL_MQ_WEG_VEL_40K        */		     100,	/* Comment [ ROP시, Front Prefill 제어량의 속도에 대한 가중치 값. 40kph 에서의 가중치. 5% 이상씩 변화. default = 80, [0 ~200] ] */
	/* int16_t     	S16_ROP_PREFILL_MQ_WEG_VEL_60K        */		     160,	/* Comment [ ROP시, Front Prefill 제어량의 속도에 대한 가중치 값. 60kph 에서의 가중치. 5% 이상씩 변화. default = 100, [0 ~200] ] */
	/* int16_t     	S16_ROP_PREFILL_MQ_WEG_VEL_80K        */		     160,	/* Comment [ ROP시, Front Prefill 제어량의 속도에 대한 가중치 값. 80kph 에서의 가중치. 5% 이상씩 변화. default = 100, [0 ~200] ] */
	/* int16_t     	S16_ROP_AY_DOT_TH_1st                 */		     300,	/* Comment [ ROP시, Front Prefill 횡가속도_기울기 진입 조건. 값을 키우면, 판단 둔감. 0.1 이상씩 변화. default = 0.5, [0 ~2] ] */
	                                                        		        	/* ScaleVal[  0.3 g/sec ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 2 ] */
	/* int16_t     	S16_ROP_AY_DOT_TH_2nd                 */		     100,	/* Comment [ ROP시, Front Prefill 횡가속도_기울기 해제 조건. 값을 키우면, 해제 빨라짐. 0.1 이상씩 변화. default = 0.3, [0 ~2] ] */
	                                                        		        	/* ScaleVal[  0.1 g/sec ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 2 ] */
	/* int16_t     	S16_ROP_PREFILL_MPRESS_TH             */		     300,	/* Comment [ ROP시, Front Prefill 마스터_실린더 압력 진입 조건. 해당값 아래서 진입함. 5 bar 이상씩 변화. default = 30, [0 ~150] ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_ROP_Pre_Frt_Alatm_TH              */		     600,	/* Comment [ ROP시, Front Prefill 횡가속도(det_alatm) 진입 조건. 해당값 이상에서 진입함. 0.05 이상씩 변화. default = 0.7, [0 ~1] ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16_ROP_Pre_Frt_Vel_TH                */		     450,	/* Comment [ ROP시, Front Prefill 차량속도 진입 조건. 해당값 이상에서 진입함. 5 kph 이상씩 변화. default = 50, [0 ~150] ] */
	                                                        		        	/* ScaleVal[     45 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_ROP_TTP_FAST_VEL_DELAY            */		       0,	/* Comment [ ROP시, Front Prefill 의 차량속도 Fast 조건에 대한 Time To Prefill. 3 이상씩 변화. default = 0, [0~100] ] */
	/* int16_t     	S16_ROP_TTP_FAST_WSTR_DOT_DELAY       */		       0,	/* Comment [ ROP시, Front Prefill 의 조향각속도 Fast 조건에 대한 Time To Prefill. 3 이상씩 변화. default = 0, [0~100] ] */
	/* int16_t     	S16_ROP_TTP_SLOW_VEL_DELAY            */		       0,	/* Comment [ ROP시, Front Prefill 의 차량속도 Slow 조건에 대한 Time To Prefill. 3 이상씩 변화. default = 0, [0~100] ] */
	/* int16_t     	S16_ROP_TTP_SLOW_WSTR_DOT_DELAY       */		       0,	/* Comment [ ROP시, Front Prefill 의 조향각속도 Slow 조건에 대한 Time To Prefill. 3 이상씩 변화. default = 0, [0~100] ] */
	/* int16_t     	S16_ROP_TTP_VEL_FAST                  */		    1000,	/* Comment [ ROP시, Front Prefill 의 Time to Prefill 에 대한 차량 속도 Fast 조건. 10 이상씩 변화. default = 100, [10~300] ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_ROP_TTP_VEL_SLOW                  */		     450,	/* Comment [ ROP시, Front Prefill 의 Time to Prefill 에 대한 차량 속도 Slow 조건. 10 이상씩 변화. default = 50, [10~300] ] */
	                                                        		        	/* ScaleVal[     45 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_ROP_TTP_WSTR_DOT_FAST             */		    6000,	/* Comment [ ROP시, Front Prefill 의 Time to Prefill 에 대한 조향각속도 Fast 조건. 50 이상씩 변화. default = 600, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 600 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	S16_ROP_TTP_WSTR_DOT_SLOW             */		    3500,	/* Comment [ ROP시, Front Prefill 의 Time to Prefill 에 대한 조향각속도 Slow 조건. 50 이상씩 변화. default = 350, [100~1500] ] */
	                                                        		        	/* ScaleVal[ 350 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 1500 ] */
	/* int16_t     	h_roll                                */		      63,	/* Comment [ Roll model 에 따른 차량 파라미터 : Height ] */
	/* int16_t     	iix_roll                              */		     690,	/* Comment [ Roll model 에 따른 차량 파라미터 : Inertia ] */
	/* int16_t     	mass_roll                             */		     195,	/* Comment [ Roll model 에 따른 차량 파라미터 : Mass ] */
	/* int16_t     	roll_damping                          */		     400,	/* Comment [ Roll model 에 따른 차량 파라미터 : Damping ] */
	/* int16_t     	roll_stiffness                        */		   15490,	/* Comment [ Roll model 에 따른 차량 파라미터 : Stiffness ] */
	/* uchar8_t    	S16_ROP_FRT_PRESS_MIN                 */		      30,	/* Comment [ At Pro_active Roll control, Min Frt_press_scan to act a same rear brake, Default = 30 ] */
	/* int16_t     	S16_ROP_PRESS_RISE_1ST_LMT            */		    1500,	/* Comment [ At Pro_active Roll control, Max 1st Front Press Scan, Default = 1500 ] */
	/* int16_t     	S16_ROP_PRESS_RISE_2ND_LMT            */		    1500,	/* Comment [ At Pro_active Roll control, Max 2nd Front Press Scan, Default = 1500 ] */
	/* uchar8_t    	S16_ROP_PRE_FRT_CTRL_MIN              */		      10,	/* Comment [ At Pro_active Roll control, Min Pre Frt_press_scan to rise a rear brake, Default = 10 ] */
	/* int16_t     	S16_ROP_PROACTIVE_DEL_SLIP_R          */		     500,	/* Comment [ At Pro_active Roll control, Rear Brake Del_Slip, Default = 5 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S16_ROP_PULSE_UP_INI_DUTY_REAR        */		      21,	/* Comment [ At Pro_active Roll control, Rear Brake Initial Duty, Default = 21 ] */
	/* uchar8_t    	S16_ROP_REAR_RISE_PRESS_BAR           */		      20,	/* Comment [ At Pro_active Roll control, Min Rear_press to rise a rear brake, Default = 20 ] */
	/* uchar8_t    	S16_RI_THRES_WEG_IN_ESC_OFF           */		     125,	/* Comment [ At ROP_in_ESC_OFF, RI_Thres to enter ROP, Default = 125 ] */
	/* uchar8_t    	S16_ROP_IN_ESC_OFF_D_GAIN_WEG         */		     150,	/* Comment [ At ROP_in_ESC_OFF, ESC YC D-Gain Weight, Default = 150 ] */
	/* uchar8_t    	S16_ROP_IN_ESC_OFF_P_GAIN_WEG         */		     150,	/* Comment [ At ROP_in_ESC_OFF, ESC YC P-Gain Weight, Default = 150 ] */
	/* int16_t     	S16_ROP_IN_ESC_OFF_VEL_ENT_TH         */		     500,	/* Comment [ At ROP_in_ESC_OFF, Vehicle Velocity to enter ROP, Default = 50kph ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	S8_DR_2ND_F_ENT_PRESS_OF_OUT_F        */		      35,	/* Comment [ Deceleration ROP : 2nd_Cycle Inside Front enter condition, comparing to Outside Front pressure [default=35] ] */
	/* int16_t     	S8_DR_2ND_F_LMT_DEL_SLIP              */		    1000,	/* Comment [ Deceleration ROP : 2nd_Cycle Inside Front target slip value [default=10] ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S8_DR_2ND_R_ENT_PRESS_OF_OUT_F        */		      30,	/* Comment [ Deceleration ROP : 2nd_Cycle Rear Outside enter condition, comparing to Outside Front pressure [default=30] ] */
	/* uchar8_t    	S8_DR_2ND_R_EXT_PRESS_OF_OUT_F        */		      10,	/* Comment [ Deceleration ROP : 2nd_Cycle Rear Outside exit condition, comparing to Outside Front pressure [default=10] ] */
	/* uchar8_t    	S8_DR_2ND_R_LMT_PRESS                 */		      15,	/* Comment [ Deceleration ROP : 2nd_Cycle Rear Outside pressure limitation [default=15] ] */
	/* int16_t     	S16_SCC_BRK_Temp1                     */		       0,	/* Comment [ Temp Variable 1 ] */
	/* int16_t     	S16_SCC_BRK_Temp2                     */		       0,	/* Comment [ Temp Variable 2 ] */
	/* int16_t     	S16_SCC_BRK_Temp3                     */		       0,	/* Comment [ Temp Variable 3 ] */
	/* int16_t     	S16_SCC_BRK_Temp4                     */		       0,	/* Comment [ Temp Variable 4 ] */
	/* int16_t     	S16_SCC_BRK_Temp5                     */		       0,	/* Comment [ Temp Variable 5 ] */
	/* int16_t     	S16_SCC_TARGETG_LIMIT_MAX             */		     400,	/* Comment [ Max of Accel Request of ISO ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_TARGETG_LIMIT_MIN             */		    -500,	/* Comment [ Min of Accel Request of ISO ] */
	                                                        		        	/* ScaleVal[     -0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_TARGETG_MAX_STOP              */		    -200,	/* Comment [ Max Accel Request for CDM Stop Mode (default = -0.2g) ] */
	                                                        		        	/* ScaleVal[     -0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_TARGETG_MIN_HILLUP_STOP       */		     -70,	/* Comment [ Min. Target G for stop control on uphill ] */
	                                                        		        	/* ScaleVal[    -0.07 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_TARGETG_MIN_HILL_STOP         */		     -80,	/* Comment [ Min of Accel Request for Stop Mode on Hill ] */
	                                                        		        	/* ScaleVal[    -0.08 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_TARGETG_MIN_STOP              */		     -50,	/* Comment [ Min of Accel Request for Stop Mode ] */
	                                                        		        	/* ScaleVal[    -0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_BRAKE_FADE_OUT_RATE           */		       5,	/* Comment [ Target Pressure decreasing rate at CDM fade-out mode (default = 0.5bar/scan ) ] */
	                                                        		        	/* ScaleVal[ 0.5 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SCC_PRESS_MAX                     */		     600,	/* Comment [ Max Pressure of SCC ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_SCC_PRESS_RESET_THD                */		       2,	/* Comment [ Threshold to disable Pressure Control ] */
	                                                        		        	/* ScaleVal[    0.2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SCC_PRESS_SET_THD                  */		      10,	/* Comment [ Threshold to enable Pressure Control ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SCC_TARGET_PRESS_LPF_GAIN_128      */		      55,	/* Comment [ Low Path Filter Gain to 128 resolution for SCC Target Pressure from wheel torque (default=55, 12Hz) ] */
	/* int16_t     	S16_SCC_EPB_DEMAND_MAX_CNT            */		     300,	/* Comment [ Max. time for activation request from AVH to EPB ] */
	                                                        		        	/* ScaleVal[     3 sec. ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SCC_GEAR_HOLD_ENGRPM_MAX          */		    1800,	/* Comment [ Max of Engine RPM for Gear Hold ] */
	/* int16_t     	S16_SCC_GEAR_HOLD_TARGETG_MAX         */		     200,	/* Comment [ Max of Accel Request for Gear Hold ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_GEAR_HOLD_TARGETG_MIN         */		      50,	/* Comment [ Min of Accel Request for Gear Hold ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_SCC_PHOLD_WTIME                   */		     428,	/* Comment [ max transition scan between from ESC to EPB ] */
	/* uchar8_t    	U8_SCC_G_SLOPE_HILLUP_STOP            */		       2,	/* Comment [ Target G rate for limiting decreasing rate at stop state on uphill ] */
	                                                        		        	/* ScaleVal[    0.002 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_SCC_G_SLOPE_HILL_STOP              */		       2,	/* Comment [ Slope 0f Accel Request to transit Stop Mode on Hill ] */
	                                                        		        	/* ScaleVal[    0.002 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_SCC_G_SLOPE_STOP                   */		       2,	/* Comment [ Slope 0f Accel Request to transit Stop Mode ] */
	                                                        		        	/* ScaleVal[    0.002 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_SCC_G_SLOPE_STOP_CNT               */		       6,	/* Comment [ Hold count for SCC target-G slope limit at standstill on flat road (default = 6) ] */
	/* uchar8_t    	U8_SCC_PIDHOLD_MAXSPD                 */		      16,	/* Comment [ Max. speed for holding feedback error of wheel torque at stop state ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SCC_SLOPE_STOP_FACTOR              */		      16,	/* Comment [ Gain proportional Slope to calculate Min Wheel Torq ] */
	/* uchar8_t    	U8_SCC_SLOPE_THD                      */		      50,	/* Comment [ threshold to judge hill ] */
	                                                        		        	/* ScaleVal[      0.5 G ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_SST_DEC_MAX                       */		    -160,	/* Comment [ Max of Wheel Pressure Slope to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[     -0.8 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ -163.84 <= X <= 163.835 ] */
	/* int16_t     	S16_SST_DEC_THD                       */		      -9,	/* Comment [ Min of Wheel Pressure Slope to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[   -0.045 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ -163.84 <= X <= 163.835 ] */
	/* int16_t     	S16_SST_MAX_P_DEC                     */		     200,	/* Comment [ Max of Pressure Decerase in Soft Stop ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_SST_DUMP_P_S_INHIBIT_THD           */		      10,	/* Comment [ Threshold of Pressure Slope to inhibit dump in Soft Stop ] */
	                                                        		        	/* ScaleVal[    1 bar/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_SST_MIN_PRESS_SLOPE_GAIN           */		       5,	/* Comment [ Gain of Pressure Dump in Soft Stop ] */
	                                                        		        	/* ScaleVal[  0.5 bar/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* uchar8_t    	U8_SST_DUMP_RATE_CONST_G              */		      50,	/* Comment [ Min of Decel for min Pressure Dump ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_SST_MIN_P_DEC                      */		      50,	/* Comment [ Min of Pressure Decerase in Soft Stop ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SST_MPRESS_DIFF_THD                */		      20,	/* Comment [ Min of Wheel Pressure Slope to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[    2 bar/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SST_MPRESS_THD                     */		      70,	/* Comment [ Min of Wheel Pressure to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SST_SPD_THD_MAX                    */		      64,	/* Comment [ Max of VRef to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SST_SPD_THD_MIN                    */		      12,	/* Comment [ Min of VRef to enter Soft Stop Control ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SST_TARGET_P_MIN                   */		     100,	/* Comment [ Min of Target Pressure  of Soft Stop Control ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SCC_BRKLAMPOFF_PRESS               */		      10,	/* Comment [ Max. wheel pressure for turning off brake lamp ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_SCC_BRKLAMPON_PRESS                */		      30,	/* Comment [ Min. wheel pressure for turning on brake lamp ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_WPC_MAX_DUTY                      */		     150,	/* Comment [ Max. Duty Used for SCC Wheel Pressure Control (default 150) ] */
	/* int16_t     	S16_WPC_MIN_DUTY                      */		      35,	/* Comment [ Min. Duty Used for SCC Wheel Pressure Control (default 60) ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_CUR_LEVEL0           */		      40,	/* Comment [ Min. Current value for holding wheel pressure of PRES_LEVEL0 (default = 400mA) ] */
	                                                        		        	/* ScaleVal[     400 mA ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_CUR_LEVEL1           */		      45,	/* Comment [ Min. Current value for holding wheel pressure of PRES_LEVEL1 (default = 450mA) ] */
	                                                        		        	/* ScaleVal[     450 mA ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_CUR_LEVEL2           */		      75,	/* Comment [ Min. Current value for holding wheel pressure of PRES_LEVEL2 (default = 750mA) ] */
	                                                        		        	/* ScaleVal[     750 mA ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_CUR_LEVEL3           */		     170,	/* Comment [ Min. Current value for holding wheel pressure of PRES_LEVEL3 (default = 1700mA) ] */
	                                                        		        	/* ScaleVal[    1700 mA ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_PRES_LEVEL0          */		       2,	/* Comment [ Wheel pressure level 0 for setting TC Valve Map (default = 2bar) ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_PRES_LEVEL1          */		       5,	/* Comment [ Wheel pressure level 1 for setting TC Valve Map (default = 5bar) ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_PRES_LEVEL2          */		      30,	/* Comment [ Wheel pressure level 2 for setting TC Valve Map (default = 30bar) ] */
	/* uchar8_t    	U8_WPC_TC_VV_MAP_PRES_LEVEL3          */		     170,	/* Comment [ Wheel pressure level 3 for setting TC Valve Map (default = 170bar) ] */
	/* int16_t     	S16_MSC_VOL_MAX_STEP                  */		     500,	/* Comment [ Max. Motor Volt to Change per 1scan (default 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_1ST_ERR                   */		      10,	/* Comment [ Small Pres. Difference for application of MSC_1ST_VOL (default 2bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_MSC_1ST_VOL                   */		     500,	/* Comment [ Motor Target Voltage under MSC_1ST_ERROR (default 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_2ND_ERR                   */		     100,	/* Comment [ Big Pres. Difference for application of MSC_2ND_VOL (default 20bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_MSC_2ND_VOL                   */		    4000,	/* Comment [ Motor Target Voltage under MSC_2ND_ERROR (default 3V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_HILL_ADD_VOL              */		    1000,	/* Comment [ Additional Motor Target Voltage for SCC on the HILL (default 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_INIT_RISE_ADD_TIME        */		      15,	/* Comment [ Times for using ADD VOL at rise mode (default 100ms) ] */
	                                                        		        	/* ScaleVal[     150 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_WPC_MSC_INIT_RISE_ADD_VOL         */		    1000,	/* Comment [ Added Motor Voltage during ADD TIME at rise mode after hold or dump (default 2V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_MAX_TIME          */		      71,	/* Comment [ Max. Time for Initial Motor Activation with WPC_MSC_PREFILL_VOL (default 500ms) ] */
	                                                        		        	/* ScaleVal[   710 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 327670 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_MIN_TIME          */		      29,	/* Comment [ Min. Time for Initial Motor Activation with WPC_MSC_PREFILL_VOL (default 200ms) ] */
	                                                        		        	/* ScaleVal[   290 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 327670 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_TARGET_P_PECENT_THRES */		      30,	/* Comment [ Percentage of wheel press to Target press for SCC Initial Motor Act. with WPC_MSC_PREFILL_VOL (default = 30%) ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_VOL1              */		    2000,	/* Comment [ Initial Motor Activation Voltage at Target Pres. 10bar (default 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_VOL2              */		    4000,	/* Comment [ Initial Motor Activation Voltage at Target Pres. 30bar (default 4V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_H_SPEED_ADD_VOL           */		     500,	/* Comment [ Additional MSC voltage above U8_WPC_MSC_HIGH_SPEED (default = 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_T_PRESS_RATE_ADD_VOL      */		     500,	/* Comment [ Additional MSC voltage more than U8_WPC_TP_RATE_HIGH (default = 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_WPC_MSC_HIGH_SPEED                 */		      30,	/* Comment [ Adding MSC_H_SPEED_ADD_VOL above this speed (default = 30km/h) ] */
	/* uchar8_t    	U8_WPC_MSC_LOW_SPEED                  */		      20,	/* Comment [ No adding MSC_H_SPEED_ADD_VOL below this speed (default = 20km/h) ] */
	/* uchar8_t    	U8_WPC_TP_RATE_HIGH                   */		      10,	/* Comment [ Initial target pressure rate for adding S16_WPC_MSC_T_PRESS_RATE_ADD_VOL (default = 10bar/s) ] */
	/* uchar8_t    	U8_WPC_TP_RATE_LOW                    */		       5,	/* Comment [ Initial target pressure rate for no adding S16_WPC_MSC_T_PRESS_RATE_ADD_VOL (default = 5bar/s) ] */
	/* int16_t     	S16_WPC_DEADZONE_LOW_P_LEVEL_THRES    */		     100,	/* Comment [ Low pressure level thres. for dead zone value distinction (default=10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_GAIN_LOW_P_LEVEL_THRES        */		     100,	/* Comment [ Low pressure level thres. for using wheel pressure PID control gain (default=10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_PE_MED_DGAIN                  */		      10,	/* Comment [ D Gain Used When Target is Smaller than Real Pres.(default 10) ] */
	/* int16_t     	S16_WPC_PE_MED_DGAIN_LOW_P_LEVEL      */		      10,	/* Comment [ D Gain Used When Target is smaller than Wheel Pres. at low pressure range (default=10) ] */
	/* int16_t     	S16_WPC_PE_MED_I_GAIN                 */		       0,	/* Comment [ I Gain Used When Target is Smaller than Real Pres.(default 0) ] */
	/* int16_t     	S16_WPC_PE_MED_I_GAIN_LOW_P_LEVEL     */		       0,	/* Comment [ I Gain Used When Target is smaller than Wheel Pres. at low pressure range (default=0) ] */
	/* int16_t     	S16_WPC_PE_MED_PGAIN                  */		      20,	/* Comment [ P Gain Used When Target is Smaller than Real Pres.(default 20) ] */
	/* int16_t     	S16_WPC_PE_MED_PGAIN_LOW_P_LEVEL      */		      50,	/* Comment [ P Gain Used When Target is smaller than Wheel Pres. at low pressure range (default=50) ] */
	/* int16_t     	S16_WPC_PE_PED_DGAIN                  */		      20,	/* Comment [ D Gain Used When Target is Bigger than Real Pres.(default 10) ] */
	/* int16_t     	S16_WPC_PE_PED_DGAIN_LOW_P_LEVEL      */		      20,	/* Comment [ D Gain Used When Target is bigger than Wheel Pres. at low pressure range (default=20) ] */
	/* int16_t     	S16_WPC_PE_PED_I_GAIN                 */		       0,	/* Comment [ I Gain Used When Target is Bigger than Real Pres.(default 0) ] */
	/* int16_t     	S16_WPC_PE_PED_I_GAIN_LOW_P_LEVEL     */		       0,	/* Comment [ I Gain Used When Target is bigger than Wheel Pres. at low pressure range (default=0) ] */
	/* int16_t     	S16_WPC_PE_PED_PGAIN                  */		      60,	/* Comment [ P Gain Used When Target is Bigger than Real Pres.(default 20) ] */
	/* int16_t     	S16_WPC_PE_PED_PGAIN_LOW_P_LEVEL      */		      30,	/* Comment [ P Gain Used When Target is bigger than Wheel Pres. at low pressure range (default=30) ] */
	/* char8_t     	S8_WPC_DEADZONE_APPLY                 */		      10,	/* Comment [ Min. Pres. Difference between Target and Real Value for Rising Mode (default 1bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_WPC_DEADZONE_APPLY_LOW_P_LEVEL     */		     100,	/* Comment [ Min. Pres. Difference between Target and Wheel value for Rising Mode at low pres. level (default=1bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_WPC_DEADZONE_DUMP                  */		      -5,	/* Comment [ Min. Pres. Difference between Target and Real Value for Dump Mode (default -1bar) ] */
	                                                        		        	/* ScaleVal[   -0.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_WPC_DEADZONE_DUMP_LOW_P_LEVEL      */		     -50,	/* Comment [ Min. Pres. Difference between Target and Wheel value for Dump Mode at low pres. level (default=-0.5bar) ] */
	                                                        		        	/* ScaleVal[     -5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* uchar8_t    	U8_WPC_ESV_CONTROL_INIT_DUTY          */		     140,	/* Comment [ Minimum ESV duty at SCC brake control starting point (default = ESV_DRIVE_DUTY_MIN - 20 ] */
	/* uchar8_t    	U8_WPC_HOLD_MARGIN_DUTY               */		       5,	/* Comment [ Plus Margin Duty for Holding Pessure at Hold Mode (default 5%) ] */
	/* uchar8_t    	U8_WPC_RISE_MARGIN_DUTY               */		      50,	/* Comment [ Plus Margin Duty for Holding Pessure at Hold Mode (default 20%) ] */
	/* uchar8_t    	U8_WPC_TC_1ST_CONTROL_MAX_TIME        */		      20,	/* Comment [ Max. time for adding TC additional duty U8_WPC_RISE_MARGIN_DUTY (default = 2sec.) ] */
	                                                        		        	/* ScaleVal[   2 second ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_WPC_FADEOUT_DEC_DUTY               */		       1,	/* Comment [ Dec. Duty at Decreasing State of Fade out Mode (default 1) ] */
	/* uchar8_t    	U8_WPC_FADEOUT_END_DUTY               */		      20,	/* Comment [ Min. Duty at Final State of Fade out Mode (default 20) ] */
	/* int16_t     	S16_WPC_TC_ESV_READY_ACT_MAX_SPEED    */		     240,	/* Comment [ Max. Speed level for TC & ESV ready mode activation (default=30kph) ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_WPC_CTRL_READY_ESV_DUTY            */		      20,	/* Comment [ ESV duty at SCC stand-by brake control (default 40 = 0.5A) ] */
	/* uchar8_t    	U8_WPC_CTRL_READY_MAX_ON_TIME         */		      60,	/* Comment [ Max. continuous time of SCC stand-by brake control (default 60 min.) ] */
	/* uchar8_t    	U8_WPC_CTRL_READY_MIN_OFF_TIME        */		      10,	/* Comment [ Min. break time for radiating ESV coil heat. (default 10 min.) ] */
	/* uchar8_t    	U8_WPC_CTRL_READY_TC_DUTY             */		       0,	/* Comment [ TC duty at SCC stand-by brake control (default 10 = 0.1A) ] */
	/* int16_t     	S16_WPC_ESV_FULL_ON_INTERVAL_TIME     */		     500,	/* Comment [ Interval Time of ESV Full ON Duty actuation (default = 5sec.) ] */
	                                                        		        	/* ScaleVal[   5 second ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_WPC_ESV_FULL_ON_DUTY               */		     200,	/* Comment [ ESV Full ON Duty for assuring valve open at initial actuation (default = 200) ] */
	/* uchar8_t    	U8_WPC_ESV_INIT_FULL_DUTY_ON_TIME     */		       5,	/* Comment [ ESV Full ON Duty actuation Time (default = 50ms) ] */
	                                                        		        	/* ScaleVal[   50 msec. ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_WPC_ESV_MIN_DUTY                   */		      70,	/* Comment [ ESV Min. Duty for assuring valve open state after ESV ON (default = 70) ] */
	/* int16_t     	S16_CDM_BRK_Temp1                     */		       0,	/* Comment [ Temp Variable 1 ] */
	/* int16_t     	S16_CDM_BRK_Temp2                     */		       0,	/* Comment [ Temp Variable 2 ] */
	/* int16_t     	S16_CDM_BRK_Temp3                     */		       0,	/* Comment [ Temp Variable 3 ] */
	/* int16_t     	S16_CDM_BRK_Temp4                     */		       0,	/* Comment [ Temp Variable 4 ] */
	/* int16_t     	S16_CDM_BRK_Temp5                     */		       0,	/* Comment [ Temp Variable 5 ] */
	/* int16_t     	S16_CDM_PRE_FILL_TARGET_VOLTAGE       */		    3000,	/* Comment [ MSC target voltage at CDM prefill stage (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_1ST_ERR_CDM               */		      20,	/* Comment [ Small Pres. Difference for application of MSC_1ST_VOL (default 2bar) ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_MSC_1ST_VOL_CDM               */		     500,	/* Comment [ Big Pres. Difference for application of MSC_2ND_VOL (default 20bar) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_2ND_ERR_CDM               */		     100,	/* Comment [ Motor Target Voltage under MSC_1ST_ERROR (default 0.5V) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPC_MSC_2ND_VOL_CDM               */		    6000,	/* Comment [ Motor Target Voltage under MSC_2ND_ERROR (default 3V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_ABS_VOL                   */		    8000,	/* Comment [ MSC target voltage during ABS & CDM cooperation control (default = 8V) ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_MAX_TIME_CDM      */		      70,	/* Comment [ Max. Time for CDM Initial Motor Activation with WPC_MSC_PREFILL_VOL_CDM (default=700ms) ] */
	                                                        		        	/* ScaleVal[     700 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_TARGET_P_PECENT_THRES_CDM */		      70,	/* Comment [ Percentage of wheel press to Target press for CDM Initial Motor Act. with WPC_MSC_PREFILL_VOL_CDM (default = 70%) ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_VOL1_CDM          */		   10000,	/* Comment [ Initial CDM Motor Activation Voltage at Target Pres. 10bar (default 2V) ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_WPC_MSC_PREFILL_VOL2_CDM          */		   14000,	/* Comment [ Initial CDM Motor Activation Voltage at Target Pres. 30bar (default 10V) ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_CDM_PRESS_RISE_RATE_THR1_L1       */		     140,	/* Comment [ Reduced Mpress rate for CDM PBA conrol at Level 1 (default 14bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 14 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_CDM_PRESS_RISE_RATE_THR1_L2       */		     120,	/* Comment [ Reduced Mpress rate for CDM PBA conrol at Level 2 (default 12bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 12 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_CDM_PRESS_RISE_RATE_THR1_L3       */		     100,	/* Comment [ Reduced Mpress rate for CDM PBA conrol at Level 3 (default 12bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 10 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_CDM_PRESS_RISE_RATE_THR2          */		      40,	/* Comment [ Reduced PBA THR2 Mpress rate for CDM PBA conrol (default 4bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 4 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_CDM_SPEED_ENTER_THR               */		      80,	/* Comment [ Min. Speed for CDM PBA Entering (default 10km/h) ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* int16_t     	S16_CDM_BRAKE_FADE_OUT_RATE           */		      50,	/* Comment [ Target Pressure decreasing rate at CDM fade-out mode (default = 5bar/scan ) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CDM_PRESS_MAX                     */		    1200,	/* Comment [ Max. Target Pressure limitation of CDM control (default 120bar) ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CDM_TARGETG_LIMIT_MAX             */		       0,	/* Comment [ Max. Target G Limitation of CDM control (default 0g) ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CDM_TARGETG_LIMIT_MIN             */		   -1000,	/* Comment [ Min. Target G Limitation of CDM control (default -1g) ] */
	                                                        		        	/* ScaleVal[       -1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_CDM_TARGETG_MAX_STOP              */		    -300,	/* Comment [ Max Accel Request for CDM Stop Mode (default = -0.3g) ] */
	                                                        		        	/* ScaleVal[     -0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_CDM_TARGET_PRESS_LPF_GAIN_128      */		     100,	/* Comment [ Low Path Filter Gain to 128 resolution for CDM Target Pressure from wheel torque (default=100, 56Hz) ] */
	/* int16_t     	S16_EPS_HIGH_SPD_1                    */		    1000,	/* Comment [ 중고속구간(5단계중 4번째) ] */
	                                                        		        	/* ScaleVal[    100 KPH ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_HIGH_SPD_2                    */		    1300,	/* Comment [ 고속구간(5단계중 5번째) ] */
	                                                        		        	/* ScaleVal[    130 KPH ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_LOW_SPD                       */		     200,	/* Comment [ 저속 구간(5단계중 1번째) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_MED_SPD_1                     */		     400,	/* Comment [ 중저속구간(5단계중 2번째) ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_MED_SPD_2                     */		     700,	/* Comment [ 중속구간(5단계중 3번째) ] */
	                                                        		        	/* ScaleVal[     70 KPH ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_EPS_HND_DAMP_GAIN_H_SPD1           */		       8,	/* Comment [ Handling제어 Damping Torque계산시  중고속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_HND_DAMP_GAIN_H_SPD2           */		      10,	/* Comment [ Handling제어 Damping Torque계산시 고속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_HND_DAMP_GAIN_L_SPD            */		       4,	/* Comment [ Handling제어 Damping Torque계산시 저속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_HND_DAMP_GAIN_M_SPD1           */		       4,	/* Comment [ Handling제어 Damping Torque계산시 중저속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_HND_DAMP_GAIN_M_SPD2           */		       6,	/* Comment [ Handling제어 Damping Torque계산시 중속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_SPL_DAMP_GAIN_H_SPD1           */		       8,	/* Comment [ SPLIT MU 제어 Damping Torque계산시  중고속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_SPL_DAMP_GAIN_H_SPD2           */		      10,	/* Comment [ SPLIT MU 제어 Damping Torque계산시 고속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_SPL_DAMP_GAIN_L_SPD            */		       4,	/* Comment [ SPLIT MU 제어 Damping Torque계산시 저속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_SPL_DAMP_GAIN_M_SPD1           */		       4,	/* Comment [ SPLIT MU 제어 Damping Torque계산시 중저속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_SPL_DAMP_GAIN_M_SPD2           */		       6,	/* Comment [ SPLIT MU 제어 Damping Torque계산시 중속구간의 GAIN ] */
	/* uchar8_t    	U8_EPS_ANG_TO_TQ_GAIN_HND             */		       2,	/* Comment [ Handling제어시 Angle에서 Torque로 변환해주는 GAIN ] */
	/* uchar8_t    	U8_EPS_ANG_TO_TQ_GAIN_SPLIT           */		       3,	/* Comment [ SPLIT MU 제어시 Angle에서 Torque로 변환해주는 GAIN ] */
	/* uint16_t    	U16_EPS_MAX_DAMPING_TORQUE            */		     600,	/* Comment [ Damping Torque의최대 제한 값(무조건 U16_EPS_MAX_DAMPING_TORQUE < U16_MAX_TORQUE) ] */
	                                                        		        	/* ScaleVal[      6 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_MAX_TORQUE                        */		     800,	/* Comment [ EPS로 보내는 Output Torque의최대 제한 값 ] */
	                                                        		        	/* ScaleVal[      8 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_EPS_FADE_TORQUE                    */		     100,	/* Comment [ Fade Out Control 에서 1 Scan당 Fade 되는 Torque량 ] */
	                                                        		        	/* ScaleVal[      1 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_EPS_STEP_DEC_TQ_LIMIT              */		     100,	/* Comment [ Torque 량이 Increase 시에 1Scan당 최대 변화량 Limit ] */
	                                                        		        	/* ScaleVal[      1 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_EPS_STEP_INC_TQ_LIMIT              */		     100,	/* Comment [ Torque 량이 Decrease 시에 1Scan당 최대 변화량 Limit ] */
	                                                        		        	/* ScaleVal[      1 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_EPS_GMA_DUMP_TIME_WEG              */		      90,	/* Comment [ ABS GMA 영역에서 Dump Time 감소량의 가중치(100 이면 ABS 상태 그대로, MAX :100) ] */
	/* uchar8_t    	U8_EPS_GMA_HOLD_TIME_WEG              */		      90,	/* Comment [ ABS GMA 영역에서 Hold Time 감소량의 가중치(100 이면 ABS 상태 그대로, MAX:100) ] */
	/* int16_t     	S16_EPS_SPL_1ST_CY_D_GAIN_WEG         */		     120,	/* Comment [ Split_mu 1cycle 에서의 D gain 의 가중치(100 이면 기존의 P Gain 그대로, MIN:100) ] */
	/* int16_t     	S16_EPS_SPL_1ST_CY_P_GAIN_WEG         */		     120,	/* Comment [ Split_mu 1cycle 에서의 P gain 의 가중치(100 이면 기존의 P Gain 그대로, MIN:100) ] */
	/* uint16_t    	U16_EPS_H_YAWERR_TH                   */		     600,	/* Comment [ Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold를 위한 큰 YAW ERR로 판단하는 Threshold ] */
	                                                        		        	/* ScaleVal[  6 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_EPS_L_YAWERR_TH                   */		     400,	/* Comment [ Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold를 위한 작은 YAW ERR로 판단하는 Threshold ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_EPS_YAW2DOT_H_YAWERR_TH_H          */		      70,	/* Comment [ 큰 YawError에서의 Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold_H ] */
	                                                        		        	/* ScaleVal[ 7 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_YAW2DOT_H_YAWERR_TH_L          */		       7,	/* Comment [ 큰 YawError에서의 Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold_H ] */
	                                                        		        	/* ScaleVal[ 0.7 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_YAW2DOT_L_YAWERR_TH_H          */		     140,	/* Comment [ 작은 YawError에서의 Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold_H ] */
	                                                        		        	/* ScaleVal[ 14 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_YAW2DOT_L_YAWERR_TH_L          */		      35,	/* Comment [ 작은 YawError에서의 Split_mu 1cycle 구분을 위한 delta_yaw2_dot의 threshold_L ] */
	                                                        		        	/* ScaleVal[ 3.5 deg/sec2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_EPS_SPL_O_INC_PGAIN_H_SPD1        */		     380,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_PGAIN_H_SPD2        */		     380,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_PGAIN_L_SPD         */		     400,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_PGAIN_M_SPD1        */		     400,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_PGAIN_M_SPD2        */		     390,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_DGAIN_H_SPD1        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_DGAIN_H_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_DGAIN_L_SPD         */		      15,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_DGAIN_M_SPD1        */		      15,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_DGAIN_M_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_IGAIN_H_SPD1        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_IGAIN_H_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_IGAIN_L_SPD         */		      30,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_IGAIN_M_SPD1        */		      30,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_INC_IGAIN_M_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  INCREASE 시 중속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_PGAIN_H_SPD1        */		     380,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_PGAIN_H_SPD2        */		     380,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_PGAIN_L_SPD         */		     400,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_PGAIN_M_SPD1        */		     400,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_PGAIN_M_SPD2        */		     390,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_DGAIN_H_SPD1        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_DGAIN_H_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_DGAIN_L_SPD         */		      15,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_DGAIN_M_SPD1        */		      15,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_DGAIN_M_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_IGAIN_H_SPD1        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_IGAIN_H_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_IGAIN_L_SPD         */		      30,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_IGAIN_M_SPD1        */		      30,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_O_DEC_IGAIN_M_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 OVERSTEER &&  DECREASE 시 중속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_PGAIN_H_SPD1        */		     410,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_PGAIN_H_SPD2        */		     410,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_PGAIN_L_SPD         */		     430,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_PGAIN_M_SPD1        */		     430,	/* Comment [ SPLIT MU 제어  UNDERSTEER &&  INCREASE 시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_PGAIN_M_SPD2        */		     420,	/* Comment [ SPLIT MU 제어  UNDERSTEER &&  INCREASE 시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_DGAIN_H_SPD1        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_DGAIN_H_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_DGAIN_L_SPD         */		      15,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_DGAIN_M_SPD1        */		      15,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_DGAIN_M_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_IGAIN_H_SPD1        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_IGAIN_H_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_IGAIN_L_SPD         */		      30,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_IGAIN_M_SPD1        */		      30,	/* Comment [ SPLIT MU 제어 UNDERSTEER&&  INCREASE 시 중저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_INC_IGAIN_M_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  INCREASE 시 중속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_PGAIN_H_SPD1        */		     410,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_PGAIN_H_SPD2        */		     410,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_PGAIN_L_SPD         */		     430,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_PGAIN_M_SPD1        */		     430,	/* Comment [ SPLIT MU 제어  UNDERSTEER &&  DECREASE 시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_PGAIN_M_SPD2        */		     420,	/* Comment [ SPLIT MU 제어  UNDERSTEER &&  DECREASE 시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_DGAIN_H_SPD1        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_DGAIN_H_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_DGAIN_L_SPD         */		      15,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_DGAIN_M_SPD1        */		      15,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_DGAIN_M_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_IGAIN_H_SPD1        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_IGAIN_H_SPD2        */		      20,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 고속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_IGAIN_L_SPD         */		      30,	/* Comment [ SPLIT MU 제어 UNDERSTEER && DECREASE 시 저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_IGAIN_M_SPD1        */		      30,	/* Comment [ SPLIT MU 제어 UNDERSTEER&&  DECREASE 시 중저속구간의 I GAIN ] */
	/* int16_t     	S16_EPS_SPL_U_DEC_IGAIN_M_SPD2        */		      25,	/* Comment [ SPLIT MU 제어 UNDERSTEER &&  DECREASE 시 중속구간의 I GAIN ] */
	/* uint16_t    	U16_EPS_SPL_AST_TH_H                  */		     200,	/* Comment [ SPLIT MU 제어 진입시 Target Angle 의 Threshold ] */
	                                                        		        	/* ScaleVal[     20 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_SPL_AST_TH_L                  */		     100,	/* Comment [ SPLIT MU 제어 유지시 Target Angle 의 Threshold ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_SPL_ENTER_VREFK               */		     300,	/* Comment [ SPLIT MU 제어 진입 가능 속도 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_EPS_SPL_AFZ_HMU_TH_H               */		      60,	/* Comment [ SPLIT MU 제어(유지)시 노면 판단에 있어 High Mu 판단 조건 AFZ의 큰 값 ] */
	                                                        		        	/* ScaleVal[        6 G ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_SPL_AFZ_HMU_TH_L               */		      50,	/* Comment [ SPLIT MU 제어(진입)시 노면 판단에 있어 High Mu 판단 조건 AFZ의 작은 값 ( 무조건 U8_EPS_SPL_AFZ_HMU_TH_L < U8_EPS_SPL_AFZ_HMU_TH_H) ] */
	                                                        		        	/* ScaleVal[        5 G ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_SPL_AFZ_LMU_TH_H               */		      30,	/* Comment [ SPLIT MU 제어(유지)시 노면 판단에 있어 Low Mu 판단 조건 AFZ의 큰 값 ] */
	                                                        		        	/* ScaleVal[        3 G ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_EPS_SPL_AFZ_LMU_TH_L               */		      20,	/* Comment [ SPLIT MU 제어(진입)시 노면 판단에 있어 Low Mu 판단 조건 AFZ의 작은값 (무조건 U8_EPS_SPL_AFZ_LMU_TH_L < U8_EPS_SPL_AFZ_LMU_TH_H) ] */
	                                                        		        	/* ScaleVal[        2 G ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_EPS_TQ_RELEASE_TH                 */		     150,	/* Comment [ 핸들 Release를 판단할 수 있는 EPS Torque sensor 값의 threshold ] */
	                                                        		        	/* ScaleVal[    1.5 N.M ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_EPS_WSTR_DOT2_RE2STR1             */		    1800,	/* Comment [ 핸들 Release와 조타를 판단할 수 있는 EPS Torque sensor 값의 threshold(RELEASE와 조타 구분 4단계중 2번째) ] */
	                                                        		        	/* ScaleVal[  180 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_WSTR_DOT2_RE2STR2             */		    2200,	/* Comment [ 핸들 Release와 조타를 판단할 수 있는 EPS Torque sensor 값의 threshold(RELEASE와 조타 구분 4단계중 3번째) ] */
	                                                        		        	/* ScaleVal[  220 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_WSTR_DOT2_RELEASE_TH          */		    1500,	/* Comment [ 핸들 Release를 판단할 수 있는 det_wstr_dot2의 threshold(RELEASE와 조타 구분 4단계중 1번째로 RELEASE 판단) ] */
	                                                        		        	/* ScaleVal[  150 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_WSTR_DOT2_STR_TH              */		    2500,	/* Comment [ 핸들 조타를 판단할 수 있는 det_wstr_dot2의 threshold(4단계 중 4번째) ] */
	                                                        		        	/* ScaleVal[  250 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_EPS_WSTR_DOT2_RE2STR_COEF1         */		      80,	/* Comment [ 핸들 Release로 판단되었을때 Torque 가중치(4단계 중 2번째) ] */
	/* uchar8_t    	U8_EPS_WSTR_DOT2_RE2STR_COEF2         */		      50,	/* Comment [ 핸들 Release로 판단되었을때 Torque 가중치(4단계중 3번째) ] */
	/* uchar8_t    	U8_EPS_WSTR_DOT2_RELEASE_COEF         */		     100,	/* Comment [ 핸들 Release로 판단되었을때 Torque 가중치(100 이면 Assist Torque 그대로, 4단계중 1번째) ] */
	/* uchar8_t    	U8_EPS_WSTR_DOT2_STR_COEF             */		       0,	/* Comment [ 핸들 조타로 판단되었을때 Torque 가중치(0 이면 Assist Torque = 0, 4단계중 4번째)) ] */
	/* int16_t     	S16_EPS_HND_INC_PGAIN_H_SPD1          */		     120,	/* Comment [ Handling제어 YawError Increase시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_PGAIN_H_SPD2          */		     120,	/* Comment [ Handling제어 YawError Increase시 고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_PGAIN_L_SPD           */		     120,	/* Comment [ Handling제어 YawError Increase시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_PGAIN_M_SPD1          */		     120,	/* Comment [ Handling제어 YawError Increase시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_PGAIN_M_SPD2          */		     120,	/* Comment [ Handling제어 YawError Increase시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_DGAIN_H_SPD1          */		      12,	/* Comment [ Handling제어 YawError Increase시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_DGAIN_H_SPD2          */		      12,	/* Comment [ Handling제어 YawError Increase시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_DGAIN_L_SPD           */		      12,	/* Comment [ Handling제어 YawError Increase시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_DGAIN_M_SPD1          */		      12,	/* Comment [ Handling제어 YawError Increase시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_INC_DGAIN_M_SPD2          */		      12,	/* Comment [ Handling제어 YawError Increase시 중속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_PGAIN_H_SPD1          */		     120,	/* Comment [ Handling제어 YawError Decrease시 중고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_PGAIN_H_SPD2          */		     120,	/* Comment [ Handling제어 YawError Decrease시고속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_PGAIN_L_SPD           */		     120,	/* Comment [ Handling제어 YawError Decrease시 저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_PGAIN_M_SPD1          */		     120,	/* Comment [ Handling제어 YawError Decrease시 중저속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_PGAIN_M_SPD2          */		     120,	/* Comment [ Handling제어 YawError Decrease시 중속구간의 P GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_DGAIN_H_SPD1          */		      12,	/* Comment [ Handling제어 YawError Decrease시 중고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_DGAIN_H_SPD2          */		      12,	/* Comment [ Handling제어 YawError Decrease시 고속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_DGAIN_L_SPD           */		      12,	/* Comment [ Handling제어 YawError Decrease시 저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_DGAIN_M_SPD1          */		      12,	/* Comment [ Handling제어 YawError Decrease시 중저속구간의 D GAIN ] */
	/* int16_t     	S16_EPS_HND_DEC_DGAIN_M_SPD2          */		      12,	/* Comment [ Handling제어 YawError Decrease시 중속구간의 D GAIN ] */
	/* uint16_t    	U16_EPS_HND_ANG_LIMIT                 */		    1000,	/* Comment [ Handling제어시 Target Angle의 한계값(Limit) ] */
	                                                        		        	/* ScaleVal[    100 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_HND_AST_TH_H                  */		     500,	/* Comment [ Handling제어 진입 시 Target Angle 의  Threshold ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_HND_AST_TH_L                  */		     200,	/* Comment [ Handling제어 유지 시  Target Angle 의 Threshold ] */
	                                                        		        	/* ScaleVal[     20 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_EPS_HND_ENTER_VREFK               */		     150,	/* Comment [ Handling제어  진입 가능 속도 ] */
	                                                        		        	/* ScaleVal[     15 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_EPSi_HND_THRES_GAIN                */		      80,	/* Comment [ delta yaw threshold gain for handling control ] */
	/* uchar8_t    	U8_EPS_HND_Raod_GAIN_H_MU             */		      50,	/* Comment [ Handling제어시 Medium Mu에서의 Torque 반영 Gain (100 일때 torque 100% ) ] */
	/* uchar8_t    	U8_EPS_HND_Raod_GAIN_L_MU             */		      50,	/* Comment [ Handling제어시 Low Mu에서의 Torque 반영 Gain (100 일때 torque 100% ) ] */
	/* uchar8_t    	U8_EPS_HND_Raod_GAIN_M_MU             */		     100,	/* Comment [ Handling제어시 High Mu에서의 Torque 반영 Gain (100 일때 torque 100% ) ] */
	/* int16_t     	S16_EPS_PRESS_DIFF_1                  */		      90,	/* Comment [ 1st reference pressure difference to make target angle in split mu control ] */
	                                                        		        	/* ScaleVal[      9 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_EPS_PRESS_DIFF_2                  */		     100,	/* Comment [ 2nd reference pressure difference to make target angle in split mu control ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_EPS_PRESS_DIFF_3                  */		     400,	/* Comment [ 3rd reference pressure difference to make target angle in split mu control ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_EPS_PRESS_DIFF_4                  */		     600,	/* Comment [ 4th reference pressure difference to make target angle in split mu control ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_EPS_PRESS_DIFF_5                  */		    1000,	/* Comment [ 5th reference pressure difference to make target angle in split mu control ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_EPS_PRS_TO_ANG_GAIN_H_SPD1        */		     100,	/* Comment [ Pressure difference to target angle speed gain for high speed 1 ] */
	/* int16_t     	S16_EPS_PRS_TO_ANG_GAIN_H_SPD2        */		     100,	/* Comment [ Pressure difference to target angle speed gain for high speed 2 ] */
	/* int16_t     	S16_EPS_PRS_TO_ANG_GAIN_L_SPD         */		     190,	/* Comment [ Pressure difference to target angle speed gain for low speed ] */
	/* int16_t     	S16_EPS_PRS_TO_ANG_GAIN_M_SPD1        */		     140,	/* Comment [ Pressure difference to target angle speed gain for med speed 1 ] */
	/* int16_t     	S16_EPS_PRS_TO_ANG_GAIN_M_SPD2        */		     140,	/* Comment [ Pressure difference to target angle speed gain for med speed 2 ] */
	/* int16_t     	S16_EPS_SP_DIFF_PRESS_TO_ANGLE_1      */		       0,	/* Comment [ Target steering angle for 1st reference pressure difference ] */
	                                                        		        	/* ScaleVal[      0 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_SP_DIFF_PRESS_TO_ANGLE_2      */		      70,	/* Comment [ Target steering angle for 2nd reference pressure difference ] */
	                                                        		        	/* ScaleVal[      7 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_SP_DIFF_PRESS_TO_ANGLE_3      */		     110,	/* Comment [ Target steering angle for 3rd reference pressure difference ] */
	                                                        		        	/* ScaleVal[     11 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_SP_DIFF_PRESS_TO_ANGLE_4      */		     140,	/* Comment [ Target steering angle for 4th reference pressure difference ] */
	                                                        		        	/* ScaleVal[     14 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPS_SP_DIFF_PRESS_TO_ANGLE_5      */		     180,	/* Comment [ Target steering angle for 5th reference pressure difference ] */
	                                                        		        	/* ScaleVal[     18 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_EPSi_TORQUE_GAIN_GMA              */		     120,	/* Comment [ Assist Torque gain when ABS GMA rise is active ] */
	/* int16_t     	S16_EPS_SPL_NTQ_SPD_GAIN_H_SPD1       */		      15,	/* Comment [ Assist Torque speed-gain for high speed 1 when steering angle is larger than target angle ] */
	/* int16_t     	S16_EPS_SPL_NTQ_SPD_GAIN_H_SPD2       */		      15,	/* Comment [ Assist Torque speed-gain for high speed 2 when steering angle is larger than target angle ] */
	/* int16_t     	S16_EPS_SPL_NTQ_SPD_GAIN_L_SPD        */		       5,	/* Comment [ Assist Torque speed-gain for low speed when steering angle is larger than target angle ] */
	/* int16_t     	S16_EPS_SPL_NTQ_SPD_GAIN_M_SPD1       */		      10,	/* Comment [ Assist Torque speed-gain for med speed 1 when steering angle is larger than target angle ] */
	/* int16_t     	S16_EPS_SPL_NTQ_SPD_GAIN_M_SPD2       */		      10,	/* Comment [ Assist Torque speed-gain for med speed 2 when steering angle is larger than target angle ] */
	/* int16_t     	S16_EPS_SPL_TORQ_D_GAIN_H_SPD1        */		      10,	/* Comment [ Assist Torque D-gain for high speed 1 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_D_GAIN_H_SPD2        */		      10,	/* Comment [ Assist Torque D-gain for high speed 2 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_D_GAIN_L_SPD         */		      10,	/* Comment [ Assist Torque D-gain for low speed ] */
	/* int16_t     	S16_EPS_SPL_TORQ_D_GAIN_M_SPD1        */		      10,	/* Comment [ Assist Torque D-gain for med speed 1 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_D_GAIN_M_SPD2        */		      10,	/* Comment [ Assist Torque D-gain for med speed 2 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_P_GAIN_H_SPD1        */		     270,	/* Comment [ Assist Torque P-gain for high speed 1 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_P_GAIN_H_SPD2        */		     270,	/* Comment [ Assist Torque P-gain for high speed 2 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_P_GAIN_L_SPD         */		     290,	/* Comment [ Assist Torque P-gain for low speed ] */
	/* int16_t     	S16_EPS_SPL_TORQ_P_GAIN_M_SPD1        */		     270,	/* Comment [ Assist Torque P-gain for med speed 1 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_P_GAIN_M_SPD2        */		     270,	/* Comment [ Assist Torque P-gain for med speed 2 ] */
	/* int16_t     	S16_EPS_SPL_TORQ_SPD_GAIN_H_SPD1      */		     100,	/* Comment [ Assist Torque speed-gain for high speed 1 when steering angle is smaller than target angle ] */
	/* int16_t     	S16_EPS_SPL_TORQ_SPD_GAIN_H_SPD2      */		     100,	/* Comment [ Assist Torque speed-gain for high speed 2 when steering angle is smaller than target angle ] */
	/* int16_t     	S16_EPS_SPL_TORQ_SPD_GAIN_L_SPD       */		     100,	/* Comment [ Assist Torque speed-gain for low speed when steering angle is smaller than target angle ] */
	/* int16_t     	S16_EPS_SPL_TORQ_SPD_GAIN_M_SPD1      */		      90,	/* Comment [ Assist Torque speed-gain for med speed 1 when steering angle is smaller than target angle ] */
	/* int16_t     	S16_EPS_SPL_TORQ_SPD_GAIN_M_SPD2      */		      90,	/* Comment [ Assist Torque speed-gain for med speed 2 when steering angle is smaller than target angle ] */
	/* int16_t     	S16_EPSi_YAWM_DEAD_BAND               */		     300,	/* Comment [ Dead band of yaw feed back ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 327.67 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_GAIN_H_SPD1        */		     320,	/* Comment [ P-gain of yaw error to target steering angle feed back for high speed 1 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_GAIN_H_SPD2        */		     320,	/* Comment [ P-gain of yaw error to target steering angle feed back for high speed 2 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_GAIN_L_SPD         */		     340,	/* Comment [ P-gain of yaw error to target steering angle feed back for low speed ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_GAIN_M_SPD1        */		     340,	/* Comment [ P-gain of yaw error to target steering angle feed back for med speed 1 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_GAIN_M_SPD2        */		     330,	/* Comment [ P-gain of yaw error to target steering angle feed back for med speed 2 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_I_GAIN_H_SPD1      */		      60,	/* Comment [ I-gain of yaw error to target steering angle feed back for high speed 1 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_I_GAIN_H_SPD2      */		      60,	/* Comment [ I-gain of yaw error to target steering angle feed back for high speed 2 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_I_GAIN_L_SPD       */		     100,	/* Comment [ I-gain of yaw error to target steering angle feed back for low speed ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_I_GAIN_M_SPD1      */		      70,	/* Comment [ I-gain of yaw error to target steering angle feed back for med speed 1 ] */
	/* int16_t     	S16_EPS_SPLIT_YAWM_I_GAIN_M_SPD2      */		      70,	/* Comment [ I-gain of yaw error to target steering angle feed back for med speed 2 ] */
	/* int16_t     	S16_EBP_ENTER_SPEED_MIN               */		       0,	/* Comment [ Speed Lower Limitation for EBP enable.(def: 60kph) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_EBP_EXIT_MPRESS_MIN               */		      50,	/* Comment [ If M/C pressure >= TP when EBP on --> EBP exit.(def: 5bar) ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EBP_ACT_PERIOD                    */		     100,	/* Comment [ EBP on time.(def: 71scan(500ms)) ] */
	/* int16_t     	S16_EBP_TARGET_VOLT                   */		    3000,	/* Comment [ Motor target voltage for EBP enable. (def: 3volt) ] */
	                                                        		        	/* ScaleVal[     3 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_EBP_INI_FULLRISE_PERIOD            */		      10,	/* Comment [ Initial full-rise time when EBP enabled. (def: 7scan(50ms)) ] */
	/* char8_t     	U8_SLS_ENABLE                         */		       1,	/* Comment [ SLS ON/OFF ] */
	/* int16_t     	S16_SLS_HIGH_SPEED                    */		    1500,	/* Comment [ SLS 제어시, 고속에 해당하는 속도 설정. default = 150 ] */
	                                                        		        	/* ScaleVal[    150 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_LOW_SPEED                     */		     800,	/* Comment [ SLS 제어시, 저속에 해당하는 속도 설정. default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_MED_SPEED                     */		    1200,	/* Comment [ SLS 제어시, 중속에 해당하는 속도 설정. default = 120 ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_DEC_H_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_INC_H_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_DEC_H_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_INC_H_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_DEC_M_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_INC_M_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_DEC_M_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_INC_M_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_DEC_L_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_D_GAIN_OUT_INC_L_mu           */		      10,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 10 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_DEC_L_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_P_GAIN_OUT_INC_L_mu           */		      30,	/* Comment [ SLS 제어시, Moment 계산 Gain. default = 30 ] */
	/* uchar8_t    	S16_SLS_HIGH_SPD_PD_WEG               */		     100,	/* Comment [ SLS 제어시, 고속 영역 가중치. default = 100 ] */
	/* uchar8_t    	S16_SLS_LOW_SPD_PD_WEG                */		     100,	/* Comment [ SLS 제어시, 저속 영역 가중치. default = 100 ] */
	/* uchar8_t    	S16_SLS_MED_SPD_PD_WEG                */		     100,	/* Comment [ SLS 제어시, 중속 영역 가중치. default = 100 ] */
	/* int16_t     	S16_SLS_ENTRANCE_SPD                  */		     800,	/* Comment [ SLS 제어 진입 설정 속도. default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_EXIT_SPD                      */		     600,	/* Comment [ SLS 제어 해제 설정 속도. default = 60 ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_HIGH_SPD_ENT_MQ               */		     500,	/* Comment [ SLS 제어/고속영역 진입 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_HIGH_SPD_EXIT_MQ              */		     500,	/* Comment [ SLS 제어 해제/고속영역 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_LOW_SPD_ENT_MQ                */		     500,	/* Comment [ SLS 제어/저속영역 진입 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_LOW_SPD_EXIT_MQ               */		     500,	/* Comment [ SLS 제어 해제/저속영역 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_MED_SPD_ENT_MQ                */		     500,	/* Comment [ SLS 제어/중속영역 진입 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_MED_SPD_EXIT_MQ               */		     500,	/* Comment [ SLS 제어 해제/중속영역 모우멘트 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_STRAIGHT_YAWC_ENTER           */		     300,	/* Comment [ SLS 제어/직진 판단 YawC 크기. default = 3 ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_STRAIGHT_YAWC_EXIT            */		     500,	/* Comment [ SLS 제어/직진 해제 YawC 크기. default = 5 ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_PARTIAL_BRK_ENTER_MAX         */		    1000,	/* Comment [ SLS 제어/Partial Brake 판단 Mpress 최대값. default = 100 ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_PARTIAL_BRK_ENTER_MIN         */		      50,	/* Comment [ SLS 제어/Partial Brake 판단 Mpress 최소값. default = 5 ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_PARTIAL_BRK_EXIT_MAX          */		    1200,	/* Comment [ SLS 제어/Partial Brake 해제 Mpress 최대값. default = 120 ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_PARTIAL_BRK_EXIT_MIN          */		      30,	/* Comment [ SLS 제어/Partial Brake 해제 Mpress 최소값. default = 3 ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SLS_MQ_TH_1ST                     */		    1000,	/* Comment [ SLS 제어시, 1st Hold 영역 모우멘트 크기. default = 10 ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_MQ_TH_2ND                     */		    2000,	/* Comment [ SLS 제어시, 2nd Hold 영역 모우멘트 크기. default = 20 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLS_MQ_TH_3RD                     */		    3000,	/* Comment [ SLS 제어시, 3rd Hold 영역 모우멘트 크기. default = 30 ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_1st_MQ_H_Spd         */		      30,	/* Comment [ SLS 제어시, 1st 모우멘트/고속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_1st_MQ_L_Spd         */		      30,	/* Comment [ SLS 제어시, 1st 모우멘트/저속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_1st_MQ_M_Spd         */		      30,	/* Comment [ SLS 제어시, 1st 모우멘트/중속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_2nd_MQ_H_Spd         */		      30,	/* Comment [ SLS 제어시, 2nd 모우멘트/고속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_2nd_MQ_L_Spd         */		      30,	/* Comment [ SLS 제어시, 2nd 모우멘트/저속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_2nd_MQ_M_Spd         */		      30,	/* Comment [ SLS 제어시, 2nd 모우멘트/중속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_3rd_MQ_H_Spd         */		      30,	/* Comment [ SLS 제어시, 3rd 모우멘트/고속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_3rd_MQ_L_Spd         */		      30,	/* Comment [ SLS 제어시, 3rd 모우멘트/저속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_3rd_MQ_M_Spd         */		      30,	/* Comment [ SLS 제어시, 3rd 모우멘트/중속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_4th_MQ_H_Spd         */		      30,	/* Comment [ SLS 제어시, 4th 모우멘트/고속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_4th_MQ_L_Spd         */		      30,	/* Comment [ SLS 제어시, 4th 모우멘트/저속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_HOLD_CNT_4th_MQ_M_Spd         */		      30,	/* Comment [ SLS 제어시, 4th 모우멘트/중속영역에 해당하는 Hold scan, default = 30 ] */
	/* uchar8_t    	S16_SLS_FADE_HOLD_CNT_H_Spd           */		      15,	/* Comment [ SLS 제어후, 고속 영역에서 Fade-out 시, Hold Scan수 , default = 15 ] */
	/* uchar8_t    	S16_SLS_FADE_HOLD_CNT_L_Spd           */		      15,	/* Comment [ SLS 제어후, 저속 영역에서 Fade-out 시, Hold Scan수 , default = 15 ] */
	/* uchar8_t    	S16_SLS_FADE_HOLD_CNT_M_Spd           */		      15,	/* Comment [ SLS 제어후, 중속 영역에서 Fade-out 시, Hold Scan수 , default = 15 ] */
	/* uchar8_t    	S16_SLS_FADE_OUT_TIME_H_Spd           */		      50,	/* Comment [ SLS 제어후, 고속 영역에서 Fade-out Scan수 , default = 50 ] */
	/* uchar8_t    	S16_SLS_FADE_OUT_TIME_L_Spd           */		      50,	/* Comment [ SLS 제어후, 저속 영역에서 Fade-out Scan수 , default = 50 ] */
	/* uchar8_t    	S16_SLS_FADE_OUT_TIME_M_Spd           */		      50,	/* Comment [ SLS 제어후, 중속 영역에서 Fade-out Scan수 , default = 50 ] */
	/* char8_t     	S8_SLS_REAPPLY_TIME_FADEOUT_R         */		       7,	/* Comment [ NO valve on time for SLS Fadeout ] */
	/* char8_t     	S8_SLS_REAPPLY_TIME_R                 */		       7,	/* Comment [ NO valve on time for SLS ] */
	/* int16_t     	S16_SLS_RESERVED_01                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_02                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_03                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_04                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_05                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_06                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_07                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_08                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_09                   */		       0,
	/* int16_t     	S16_SLS_RESERVED_10                   */		       0,
	/* char8_t     	S8_SLS_RESERVED_01                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_02                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_03                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_04                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_05                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_06                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_07                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_08                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_09                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_10                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_11                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_12                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_13                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_14                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_15                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_16                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_17                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_18                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_19                    */		       0,
	/* char8_t     	S8_SLS_RESERVED_20                    */		       0,
	/* uchar8_t    	U8_SLS_DUMPSCAN_TIME_R                */		       5,	/* Comment [ SLS Dump scan time ] */
	/* int16_t     	S16_AVH_MOVING_DETECT_CYCLE           */		    6000,	/* Comment [ 차량 구름 감지 기준인, 바퀴 펄스 모니터링 주기(default : 60sec) ] */
	                                                        		        	/* ScaleVal[     60 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ROLL_RISE_PRESSURE                */		     150,	/* Comment [ 1회 증압 시 증압 압력 (default : 10bar) ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_MAX_ROLL_RISE_ATTEMPT              */		     255,	/* Comment [ 추가 증압 모드 최대 허용 횟수 (default : 5) ] */
	/* uchar8_t    	U8_ROLL_RISE_TIME                     */		     150,	/* Comment [ 1회 증압 시 모터 구동 시간 (default : 0.25s) ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_ROLL_TH_FOR_EPB_ACT                */		     150,	/* Comment [ EPB 전환 기준 차량 밀림 거리 (default : 15Cm) ] */
	                                                        		        	/* ScaleVal[      15 Cm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ROLL_TH_FOR_RISE                   */		      40,	/* Comment [ 추가 증압 모드 진입 기준 차량 밀림 거리 (default : 4Cm) ] */
	                                                        		        	/* ScaleVal[       4 Cm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_ADD_ACT_TIME_NO_ACT_EPB           */		       5,	/* Comment [ EPB 미 작동 시, AVH 추가 제어 시간 (default : 5s) ] */
	/* int16_t     	S16_AUTO_PAUSE_TIME                   */		     600,	/* Comment [ Auto Act 1 IGN cycle 누적 작동 허용 시간 reset time (default : 600s) ] */
	/* int16_t     	S16_AVH_IGN_DECOUNT_CYCLE             */		     200,	/* Comment [ 누적 작동 카운터 감소 주기 (default : 2sec) ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_RECOVERY_IGN_CNT_REF          */		       0,	/* Comment [ 누적 작동 초과 후, AVH 정상 회복 기준 시간 (default : 0sec) ] */
	/* int16_t     	S16_MAX_ON_TIME_ONE_CYCLE             */		     600,	/* Comment [ AVH 1회 작동 최대 허용 시간 (default : 600s) ] */
	/* int16_t     	S16_MAX_ON_TIME_ONE_CYCLE_AUTO        */		     300,	/* Comment [ Auto Act 1회 작동 최대 허용 시간 (default : 300s) ] */
	/* int16_t     	S16_MAX_ON_TIME_ONE_IGN               */		    1800,	/* Comment [ AVH 1 IGN cycle 누적 작동 허용 시간 (default : 1800s) ] */
	/* int16_t     	S16_MAX_ON_TIME_ONE_IGN_AUTO          */		    3600,	/* Comment [ Auto Act 1 IGN cycle 누적 작동 허용 시간 (default : 3600s) ] */
	/* int16_t     	S16_AVH_DETECTION_HILL_TH             */		       5,	/* Comment [ AVH Determine Hill threshold (default : 0.05g) ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EPB_ACT_DELAY_MAXHILL             */		     300,	/* Comment [ 최대 경사 이상에서 AVH 작동 시 EPB전환 time delay (default : 2.5s) ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* char8_t     	S8_AVH_ACT_MAX_GRADE_DW               */		     -25,	/* Comment [ AVH 작동 유지 가능한 내리막 최대 경사도 (default : -25%) ] */
	/* uchar8_t    	U8_AVH_ACT_MAX_GRADE                  */		      25,	/* Comment [ AVH 작동 유지 가능한 최대 경사도 (default : 25%) ] */
	/* int16_t     	S16_AVH_ENTER_MIN_SPEED               */		       2,	/* Comment [ 제어 진입 가능 차량 속도 (default : 0.125kph) ] */
	                                                        		        	/* ScaleVal[  0.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_AVH_MOTOR_TARGET_VOLT             */		    3000,	/* Comment [ 증압 모드 시 모터 구동 전압 (default : 3volt) ] */
	                                                        		        	/* ScaleVal[     3 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_AVH_MOT_ON_TIME_1ST_REF_P         */		      30,	/* Comment [ 1st 목표 증압 압력 (default : 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MOT_ON_TIME_2ND_REF_P         */		      50,	/* Comment [ 2nd 목표 증압 압력 (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MOT_ON_TIME_3RD_REF_P         */		     100,	/* Comment [ 3rd 목표 증압 압력 (default : 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MOT_ON_TIME_4TH_REF_P         */		     150,	/* Comment [ 4th 목표 증압 압력 (default : 15bar) ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MOT_ON_TIME_5TH_REF_P         */		     300,	/* Comment [ 5th 목표 증압 압력 (default : 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_MOT_ON_TIME_1ST                */		     250,	/* Comment [ 1st 목표 압력 모터 구동 시간 (default : 0.13s) ] */
	                                                        		        	/* ScaleVal[    2.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_MOT_ON_TIME_2ND                */		     225,	/* Comment [ 2nd 목표 압력 모터 구동 시간 (default : 0.15s) ] */
	                                                        		        	/* ScaleVal[   2.25 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_MOT_ON_TIME_3RD                */		     200,	/* Comment [ 3rd 목표 압력 모터 구동 시간 (default : 0.25s) ] */
	                                                        		        	/* ScaleVal[      2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_MOT_ON_TIME_4TH                */		     175,	/* Comment [ 4th 목표 압력 모터 구동 시간 (default : 0.30s) ] */
	                                                        		        	/* ScaleVal[   1.75 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_MOT_ON_TIME_5TH                */		     150,	/* Comment [ 5th 목표 압력 모터 구동 시간 (default : 0.45s) ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_AVH_ALARM_TIME_EPB_OK             */		     800,	/* Comment [ AVH 중 EPB 자동 전환 시, 메시지 송출 시간 (default : 8sec) ] */
	                                                        		        	/* ScaleVal[      8 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_ALARM_TIME_INBIT_SW_OFF       */		     400,	/* Comment [ AVH 해제 불가 상황 발생 시, 알람/메시지 송출 시간 (default : 4sec) ] */
	                                                        		        	/* ScaleVal[      4 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_ALARM_TIME_INBIT_SW_ON        */		     400,	/* Comment [ AVH 진입 불가 상황 발생 시, 알람/메시지 송출 시간 (default : 4sec) ] */
	                                                        		        	/* ScaleVal[      4 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_ABNORMAL_FO_REF_SCAN          */		       2,	/* Comment [ 작동 중 시동 off 시, AVH 추가 유압 해제 시 TC v/v 감압 cycle (default : 0.02sec) ] */
	                                                        		        	/* ScaleVal[   0.02 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_ABNORMAL_F_O_MIN_DUTY         */		       5,	/* Comment [ AVH Abnormal mode Fade out Min Duty (default : 5mA) ] */
	/* int16_t     	S16_AVH_ABNORMAL_HOLD_DUTY            */		      70,	/* Comment [ 작동 중 시동 off 시, AVH 추가 유압 홀드 시 TC v/v 인가 전류 (default : 70duty) ] */
	/* int16_t     	S16_AVH_ABNORMAL_HOLD_TIME            */		     150,	/* Comment [ 작동 중 시동 off 시, AVH 추가 유압 홀드 시간 (default : 1.5sec) ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_AVH_ACC_PEDAL_ON_MIN_TH           */		      11,	/* Comment [ 제어 해제 기준 가속 페달 개도량 (default : 1.1%) ] */
	                                                        		        	/* ScaleVal[      1.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FAST_EXIT_ARAD_TH             */		       8,	/* Comment [ 즉시 제어 해제 기준 바퀴 가속도 (default : 2g) ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -8192 <= X <= 8191.75 ] */
	/* int16_t     	S16_AVH_FAST_EXIT_MTP_TH              */		      30,	/* Comment [ 즉시 제어 해제 기준 가속 페달 개도량 (default : 30%) ] */
	/* int16_t     	S16_AVH_MAX_MP_OFFSET_BLS_FAIL        */		      80,	/* Comment [ BLS 고장 시, 보상되는 MCP 센서값 (default : 8bar) ] */
	                                                        		        	/* ScaleVal[      8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_REBRAKE_DCT_THRESHOLD         */		      30,	/* Comment [ AVH Rebrake detect threshold (default : 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_REBRAKE_DCT_TIME               */		      15,	/* Comment [ AVH Rebrake detect time (default : 150ms) ] */
	                                                        		        	/* ScaleVal[     150 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_EPBI_FORCED_HOLD_SPD              */		      24,	/* Comment [ 강제 hold mode 진입 차속 (default : 3kph) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_EPBI_TARGET_DECEL                 */		     -50,	/* Comment [ 제어 요구 감속도 (default : -0.5g) ] */
	                                                        		        	/* ScaleVal[     -0.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EPBI_INITIAL_CURRENT              */		     550,	/* Comment [ Initial TC current (default : 550mA) ] */
	/* int16_t     	S16_EPBI_MAX_CURRENT                  */		     800,	/* Comment [ 최대 허용 TC current (default : 800mA) ] */
	/* char8_t     	S8_HRB_ADD_REAPPLY_TIME               */		       2,	/* Comment [ Compensation Value of Pulse-up Rise Time During HRB Ctrl. (default 2ms) ] */
	/* char8_t     	S8_HRB_ENTER_SLIP                     */		       3,	/* Comment [ HRB Enter Slip ] */
	/* uchar8_t    	U8_HRB_FINAL_PULSEUP_HOLD             */		       2,	/* Comment [ Hold Scans at the last HRB pulse-up. (default 20ms) ] */
	/* uchar8_t    	U8_HRB_INITIAL_PULSEUP_HOLD           */		       5,	/* Comment [ Hold Scans at the first HRB pulse-up. (default 50ms) ] */
	/* uchar8_t    	U8_HRB_MAX_PULSE_UP                   */		       4,	/* Comment [ Max. Num. of pulse-ups Since HRB Engagement Until Brake Boost. (default : 4) ] */
	/* uchar8_t    	U8_HRB_MP_DEC_GAIN                    */		      70,	/* Comment [ Amount of adaptation per 10bar decreasing (default 7bar, max 10bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_MP_DEC_GAIN_BOOST              */		      50,	/* Comment [ Amount of adaptation per 10bar decreasing during brake boost (default 5bar, max 10bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_MP_INC_GAIN                    */		      70,	/* Comment [ Amount of adaptation per 10bar increasing (default 7bar, max 10bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_MP_RELEASE_COMP                */		      30,	/* Comment [ Additional adaptation at brake releasing state (default 3bar, max 5 bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_DELTA_MP_AT_SLIGHT_BRK         */		      10,	/* Comment [ MP Rise Rate at Slight Braking (default : 1 bar/4scan) ] */
	                                                        		        	/* ScaleVal[ 1 bar/4scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_DELTA_MP_AT_SPIKE_BRK          */		     100,	/* Comment [ MP Rise Rate at Spike Braking (default : 10 bar/4scan) ] */
	                                                        		        	/* ScaleVal[ 10 bar/4scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HRB_ENTER_DP_PERCENTAGE_H          */		      20,	/* Comment [ HRB Enter ΔP Threshold at High Braking (default : 20% of mpress) ] */
	/* uchar8_t    	U8_HRB_ENTER_DP_PERCENTAGE_M          */		      15,	/* Comment [ HRB Enter ΔP Threshold at Medium Braking (default : 15% of mpress) ] */
	/* uchar8_t    	U8_HRB_ENTER_PRESS_SLIGHT             */		     120,	/* Comment [ HRB Enter MP Threshold at Slight Braking (Min : 100bar, Max : 200bar, default : 120bar) ] */
	/* uchar8_t    	U8_HRB_ENTER_PRESS_SPIKE              */		      80,	/* Comment [ HRB Enter MP Threshold at Spike Braking (Min : 50bar, Max : 200bar, default : 80bar) ] */
	/* int16_t     	S16_HRB_ALLOW_SPEED                   */		     280,	/* Comment [ HRB Entering Minium speed (default : 35kph) ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_HRB_EXIT_SPEED                    */		      24,	/* Comment [ HRB Exit Speed (default : 3kph) ] */
	                                                        		        	/* ScaleVal[      3 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	S16_HRB_REAR_MAX_PRESSURE             */		    1500,	/* Comment [ Maxium Pressure Allowed in Rear Wheels (default : 200bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 250 ] */
	/* uchar8_t    	U8_HRB_MSC_TARGET_VOL                 */		     140,	/* Comment [ HRB Target Volt (default : 4V) ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 14 ] */
	/* int16_t     	S16_TSP_BRK_CTRL_ENTER_PRESS          */		     400,	/* Comment [ Max pressure to enter TSP brake ctrl (def:40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_TSP_BRK_CTRL_EXIT_PRESS           */		     450,	/* Comment [ Min pressure to exit TSP brake ctrl (def:45kph) ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
	/* int16_t     	S16_TSP_DISABLE_SPD_DETECT_MAX        */		     960,	/* Comment [ Min speed for TSP Enter (def:200kph) ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_DISABLE_SPD_DETECT_MIN        */		     400,	/* Comment [ Min speed for TSP Enter (def:50kph) ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_TSP_EXIT_SPD_MIN                  */		     320,	/* Comment [ Min speed for TSP Exit (def:40kph) ] */
	                                                        		        	/* ScaleVal[     40 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_TSP_OSCI_MITIG_END_TIME           */		     400,	/* Comment [ Oscil mitigation count threshold for TSP exit (def : 280sc) ] */
	/* uchar8_t    	S8_TSP_ENABLE                         */		       1,	/* Comment [ TSP Function ENABLE (def :1) ] */
	/* int16_t     	S16_TSP_CS_THR                        */		     720,	/* Comment [ Critical speed level to set the yaw threshold (def:90kph) ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_TSP_HS_THR                        */		     640,	/* Comment [ Med speed level to set the yaw threshold (def:80kph) ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_TSP_LS_THR                        */		     520,	/* Comment [ Low speed level to set the yaw threshold (def:65kph) ] */
	                                                        		        	/* ScaleVal[     65 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_TSP_ALAT_DYAW_THR_HIGH            */		     600,	/* Comment [ high value of det_alatc to del yaw thr (def : 0.6) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_ALAT_DYAW_THR_LOW             */		     100,	/* Comment [ low value of det_alatc to del yaw thr (def : 0.1) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_ALAT_DYAW_THR_MED             */		     500,	/* Comment [ med value of det_alatc to del yaw thr (def : 0.5) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_DYAW_THR_HIGH     */		     600,	/* Comment [ del yaw threshold add value as high del_alatc when critical speed (def : 6) ] */
	                                                        		        	/* ScaleVal[    6 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_DYAW_THR_LOW      */		       0,	/* Comment [ del yaw threshold add value as low del_alatc when critical speed (def : 0) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_DYAW_THR_MED      */		      50,	/* Comment [ del yaw threshold add value as med del_alatc when critical speed (def : 0.5) ] */
	                                                        		        	/* ScaleVal[  0.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_DYAW_THR                   */		     200,	/* Comment [ critical speed del yaw threshold (def : 2) ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_DYAW_THR_HIGH     */		    1000,	/* Comment [ del yaw threshold add value as high del_alatc when high speed (def : 10) ] */
	                                                        		        	/* ScaleVal[   10 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_DYAW_THR_LOW      */		       0,	/* Comment [ del yaw threshold add value as low del_alatc when high speed (def : 0) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_DYAW_THR_MED      */		     100,	/* Comment [ del yaw threshold add value as med del_alatc when high speed (def : 1) ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_DYAW_THR                   */		     200,	/* Comment [ high speed del yaw threshold (def : 2) ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_DYAW_THR_HIGH     */		    1000,	/* Comment [ del yaw threshold add value as high del_alatc when low speed (def : 10) ] */
	                                                        		        	/* ScaleVal[   10 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_DYAW_THR_LOW      */		       0,	/* Comment [ del yaw threshold add value as low del_alatc when low speed (def : 0) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_DYAW_THR_MED      */		     600,	/* Comment [ del yaw threshold add value as med del_alatc when low speed (def : 6) ] */
	                                                        		        	/* ScaleVal[    6 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_DYAW_THR                   */		     300,	/* Comment [ low speed del yaw threshold (def : 3) ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_ALAT_YAWC_THR_HIGH            */		     600,	/* Comment [ high value of det_alatc to yawc thr (def : 0.6) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_ALAT_YAWC_THR_LOW             */		     100,	/* Comment [ low value of det_alatc to yawc thr (def : 0.1) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_ALAT_YAWC_THR_MED             */		     300,	/* Comment [ med value of det_alatc to yawc thr (def : 0.3) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_YAWC_THR_HIGH     */		     800,	/* Comment [ yawc threshold add value as high del_alatc when critical speed (def : 8) ] */
	                                                        		        	/* ScaleVal[    8 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_YAWC_THR_LOW      */		     100,	/* Comment [ yawc threshold add value as low del_alatc when critical speed (def : 1) ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_ALAT_ADD_YAWC_THR_MED      */		     550,	/* Comment [ yawc threshold add value as med del_alatc when critical speed (def : 5.5) ] */
	                                                        		        	/* ScaleVal[  5.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_CS_YAWC_THR                   */		     300,	/* Comment [ critical speed yawc threshold (def : 3) ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_YAWC_THR_HIGH     */		     600,	/* Comment [ yawc threshold add value as high del_alatc when high speed (def : 6) ] */
	                                                        		        	/* ScaleVal[    6 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_YAWC_THR_LOW      */		       0,	/* Comment [ yawc threshold add value as low del_alatc when high speed (def : 0) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_ALAT_ADD_YAWC_THR_MED      */		     400,	/* Comment [ yawc threshold add value as med del_alatc when high speed (def : 4) ] */
	                                                        		        	/* ScaleVal[    4 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_HS_YAWC_THR                   */		     250,	/* Comment [ high speed yawc threshold (def : 2.5) ] */
	                                                        		        	/* ScaleVal[  2.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_YAWC_THR_HIGH     */		     100,	/* Comment [ yawc threshold add value as high del_alatc when low speed (def : 1) ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_YAWC_THR_LOW      */		       0,	/* Comment [ yawc threshold add value as low del_alatc when low speed (def : 0) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_ALAT_ADD_YAWC_THR_MED      */		      50,	/* Comment [ yawc threshold add value as med del_alatc when low speed (def : 0.5) ] */
	                                                        		        	/* ScaleVal[  0.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_LS_YAWC_THR                   */		     200,	/* Comment [ low speed yawc threshold (def : 2) ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_FREQ_MAX_RATE_2nd_3rd         */		      80,	/* Comment [ When frequency rate of 2 and 3 count is over this value, oscil count reset (def : 80%) ] */
	/* int16_t     	S16_TSP_FREQ_MAX_RATE_3rd_4th         */		      50,	/* Comment [ When frequency rate of 3 and 4 count is over this value, TSP is exited (def : 50%) ] */
	/* int16_t     	S16_TSP_FRQNC_MAX_TIME_TSP_HS         */		     213,	/* Comment [ Max Yaw-rate 1 osc cyc t for High spd during TSP, if longer than this, osc cnt reset (def : 148sc) ] */
	/* int16_t     	S16_TSP_FRQNC_MAX_TIME_TSP_LS         */		     213,	/* Comment [ Max Yaw-rate 1 osc cyc t for Low spd during TSP, if longer than this, oscil cnt reset (def : 200sc) ] */
	/* int16_t     	S16_TSP_FRQNC_MAX_TIME_TSP_NO         */		     149,	/* Comment [ Max Yaw-rate 1 osc cycle t before TSP, if longer than this, then osc cnt reset (def : 149sc) ] */
	/* int16_t     	S16_TSP_FRQNC_MIN_TIME                */		      28,	/* Comment [ Min Yaw-rate 1 osc cycle time, if shorter than this, then no oscil count (def : 28sc) ] */
	/* uchar8_t    	U8_TSP_DETECT_OSCIL_CYCLE             */		       3,	/* Comment [ Oscil count threshold for TSP enter. 3->1.5cycle (def:3) ] */
	/* uchar8_t    	U8_TSP_DETECT_OSCIL_CYCLE_NOTR        */		       5,	/* Comment [ Oscil count thr for TSP enter when not conneted the trailer s/w. 3->1.5cycle (def:3) ] */
	/* uchar8_t    	U8_TSP_MAX_DAMP_RATIO_1st_3rd         */		      40,	/* Comment [ When reduction of del yaw peak 1 and 3 count is over this value, oscil count reset (def : 40%/s) ] */
	/* uchar8_t    	U8_TSP_MAX_DAMP_RATIO_2nd_4th         */		      30,	/* Comment [ When reduction of del yaw peak 2 and 4 count is over this value, TSP is exited (def : 30%/s) ] */
	/* int16_t     	S16_TSP_STEER_MAX_TIME                */		     710,	/* Comment [ Max yawc 1 osc cycle for Driver interv detection. If longer than this, osc cnt reset (def : 497sc) ] */
	/* int16_t     	S16_TSP_STEER_MIN_TIME                */		      18,	/* Comment [ Min yawc 1 osc cycle for Driver interv detection. If shorter than this, no detection(def : 18sc) ] */
	/* int16_t     	S16_TSP_DYAW_DOT_MITIG_THR_HIGH       */		     100,	/* Comment [ Mitigation detect del yaw dot high threshold (def : 1deg/s^2) ] */
	                                                        		        	/* ScaleVal[  1 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* int16_t     	S16_TSP_DYAW_DOT_MITIG_THR_LOW        */		      50,	/* Comment [ Mitigation detect del yaw dot low threshold (def : 0.5deg/s^2) ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* int16_t     	S16_TSP_DYAW_DOT_MITIG_THR_MED        */		      70,	/* Comment [ Mitigation detect del yaw dot med threshold (def : 0.7deg/s^2) ] */
	                                                        		        	/* ScaleVal[ 0.7 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* int16_t     	S16_TSP_DYAW_MITIG_THR_HIGH           */		     190,	/* Comment [ Mitigation detect del yaw high threshold (def : 1.9deg/s) ] */
	                                                        		        	/* ScaleVal[  1.9 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* int16_t     	S16_TSP_DYAW_MITIG_THR_LOW            */		      50,	/* Comment [ Mitigation detect del yaw low threshold  (def : 0.5deg/s) ] */
	                                                        		        	/* ScaleVal[  0.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* int16_t     	S16_TSP_DYAW_MITIG_THR_MED            */		     100,	/* Comment [ Mitigation detect del yaw med threshold  (def : 1deg/s) ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.5 ] */
	/* uchar8_t    	U8_MITI_HIGH_THR_COUNT                */		       1,	/* Comment [ Added trailer mitigation count when high threshold (def : 1) ] */
	/* uchar8_t    	U8_MITI_LOW_THR_COUNT                 */		       3,	/* Comment [ Added trailer mitigation count when low threshold (def : 3) ] */
	/* uchar8_t    	U8_MITI_MED_THR_COUNT                 */		       2,	/* Comment [ Added trailer mitigation count when med threshold (def : 2) ] */
	/* uchar8_t    	U8_MITI_OVER_THR_COUNT                */		       5,	/* Comment [ Added trailer mitigation count when over threshold (def : 5) ] */
	/* int16_t     	S16_TSP_CURRENT_TG_DECEL_H            */		     300,	/* Comment [ High target deceleration for decision initial TC current (def : 0.3g) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_CURRENT_TG_DECEL_L            */		     100,	/* Comment [ Low target deceleration for decision initial TC current (def : 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_CURRENT_TG_DECEL_M            */		     200,	/* Comment [ Med target deceleration for decision initial TC current (def : 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_DELTA_SPEED_HIGH              */		     480,	/* Comment [ When decrease target decel, high delta speed (def : 60kph) ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* int16_t     	S16_TSP_DELTA_SPEED_LOW               */		       0,	/* Comment [ When decrease target decel, Low delta speed (def : 0kph) ] */
	                                                        		        	/* ScaleVal[      0 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* int16_t     	S16_TSP_DYAW_AMP_MAX_TARGET_G_H       */		     300,	/* Comment [ Target deceleration of big del yaw peak (def : 0.3g) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_DYAW_AMP_MAX_TARGET_G_L       */		     200,	/* Comment [ Target deceleration of small del yaw peak (def : 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_HIGH_DECEL   */		     300,	/* Comment [ High target deceleration for decision first rear NO hold time (def : 0.3g) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_LOW_DECEL    */		     100,	/* Comment [ Low target deceleration for decision first rear NO hold time (def : 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_MED_DECEL    */		     200,	/* Comment [ Med target deceleration for decision first rear NO hold time (def : 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_TIME_HIGH    */		      55,	/* Comment [ First rear NO hold time at high target deceleration (def : 55sc) ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_TIME_LOW     */		      40,	/* Comment [ First rear NO hold time at low target deceleration (def : 40sc) ] */
	/* int16_t     	S16_TSP_FIRST_DIFF_ENTER_TIME_MED     */		      50,	/* Comment [ First rear NO hold time at med target deceleration (def : 50sc) ] */
	/* int16_t     	S16_TSP_INITIAL_CURRENT_HD            */		     650,	/* Comment [ Initial current at high target deceleration (def : 650) ] */
	/* int16_t     	S16_TSP_INITIAL_CURRENT_LD            */		     550,	/* Comment [ Initial current at low target deceleration (def : 550) ] */
	/* int16_t     	S16_TSP_INITIAL_CURRENT_MD            */		     600,	/* Comment [ Initial current at med target deceleration (def : 600) ] */
	/* int16_t     	S16_TSP_INITIAL_RISE_CURRENT          */		    1000,	/* Comment [ TC current at initial deceleration control rise (def: 1000) ] */
	/* int16_t     	S16_TSP_PBA_ADD_CURRENT               */		     100,	/* Comment [ When TSP PBA combination control activate, add Duty (def : 100) ] */
	/* int16_t     	S16_TSP_TARGET_G_GAIN_HS              */		     100,	/* Comment [ Target deceleration gain at high speed (def : 100%) ] */
	/* int16_t     	S16_TSP_TARGET_G_GAIN_LS              */		      50,	/* Comment [ Target deceleration gain at low speed (def : 50%) ] */
	/* int16_t     	S16_TSP_TARGET_G_GAIN_MS              */		      85,	/* Comment [ Target deceleration gain at med speed (def : 85%) ] */
	/* int16_t     	S16_TSP_TARGET_VOLT_BRK               */		    4000,	/* Comment [ Initial Target Mot volt @ barking (def: 4v) ] */
	                                                        		        	/* ScaleVal[     4 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TARGET_VOLT_CONTROL           */		    4500,	/* Comment [ Target Motor volt at decel+diff control (def: 4.5v) ] */
	                                                        		        	/* ScaleVal[   4.5 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TARGET_VOLT_INI               */		   10000,	/* Comment [ Target Motor volt at initial deceleration control rise (def: 10v) ] */
	                                                        		        	/* ScaleVal[    10 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TARGET_VOLT_PREFILL           */		    3000,	/* Comment [ Target Motor volt at prefill (def: 3v) ] */
	                                                        		        	/* ScaleVal[     3 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TG_DYAW_AMP_MAX_H             */		     500,	/* Comment [ Max del yaw peak for decision target deceleration (def : 5deg/s) ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_TG_GAIN_SPEED_H               */		     720,	/* Comment [ High speed for decision target deceleration gain (def : 90kph) ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_TG_GAIN_SPEED_L               */		     520,	/* Comment [ Low speed for decision target deceleration gain (def : 65kph) ] */
	                                                        		        	/* ScaleVal[     65 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_TG_GAIN_SPEED_M               */		     640,	/* Comment [ Med speed for decision target deceleration gain (def : 80kph) ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_VAR_T_G_SPEED_H               */		     960,	/* Comment [ When decrease target decel, speed high threshold (def : 120kph) ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* int16_t     	S16_TSP_VAR_T_G_SPEED_L               */		     480,	/* Comment [ When decrease target decel, speed Low threshold (def : 60kph) ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* uchar8_t    	U8_TSP_BRK_EN_YAW_DOT_THR             */		       5,	/* Comment [ If del yaw dot is below this value at 3count, TSP brake control is entered  (def : 0.05deg/s2) ] */
	                                                        		        	/* ScaleVal[ 0.05 deg/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TSP_DEC_TARGET_DECEL_RATE          */		       1,	/* Comment [ When reduce a target decel as vehicle velocity, target decel decrease rate (def : 1) ] */
	/* uchar8_t    	U8_TSP_RISE_TIME                      */		      20,	/* Comment [ Max rise time when TSP Activate (def : 20sc) ] */
	/* int16_t     	U8_TSP_SPEED_MIN_DECEL                */		     150,	/* Comment [ When TSP Differential Control, minimun target decel (def : 0.15deg/s) ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32 ] */
	/* int16_t     	S16_TSP_DIFF_ADD_CURRENT_GAIN         */		     200,	/* Comment [ Differential add TC current = Del_M_TSP x S16_TSP_DIFF_ADD_CURRENT_GAIN (def : 2) ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_HS          */		     500,	/* Comment [ Differential only add TC current at high speed (def : 500) ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_LS          */		     300,	/* Comment [ Differential only add TC current at low speed (def : 300) ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_MS          */		     400,	/* Comment [ Differential only add TC current at med speed (def : 400) ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_SPEED_H     */		     720,	/* Comment [ High speed for decision diff only add current (def : 90kph) ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_SPEED_L     */		     520,	/* Comment [ Low speed for decision diff only add current (def : 65kph) ] */
	                                                        		        	/* ScaleVal[     65 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_CURRENT_SPEED_M     */		     640,	/* Comment [ Med speed for decision diff only add current (def : 80kph) ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_DYAW_P              */		     250,	/* Comment [ Diffential Only control enter Min Del yaw peak add threthold (def : 0.5deg/s) ] */
	                                                        		        	/* ScaleVal[  2.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_DIFF_ONLY_SPEED               */		     720,	/* Comment [ If vehicle speed faster than this value, diff only control is not entered (def : 90kph) ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TSP_DIFF_TO_DEC_DYAW_P            */		      50,	/* Comment [ When differential only control, this value is bigger than threshold, change TSP Decel control. (def : 1.5deg/s) ] */
	                                                        		        	/* ScaleVal[  0.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TSP_TARGET_VOLT_DIFFONLY          */		    4500,	/* Comment [ Target Mot Volt when TSP Differntial Only control (def : 4.5v) ] */
	                                                        		        	/* ScaleVal[   4.5 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_TSP_DIFF_CTRL_YAW_DOT_EN_THR       */		      50,	/* Comment [ Yaw acc. lower limit to enter diff ctrl.(def : 0.5deg/s2) ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_TSP_DIFF_CTRL_YAW_EN_THR           */		       0,	/* Comment [ Yaw upper limit to enter diff ctrl.(def : 0deg/s) ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* uchar8_t    	U8_TSP_DIFF_DGAIN                     */		      25,	/* Comment [ Yaw-rate D-gain to decrease TC add current when yaw dec (def : 25) ] */
	/* uchar8_t    	U8_TSP_DIFF_PGAIN                     */		      10,	/* Comment [ Yaw-rate P-gain to decrease TC add current when yaw dec (def : 10) ] */
	/* int16_t     	S16_TSP_TORQ_LIMIT_DECEL_H            */		     300,	/* Comment [ Med target deceleration for decision engine torque limit level (def : 0.3g) ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TORQ_LIMIT_DECEL_L            */		     100,	/* Comment [ Low target deceleration for decision engine torque limit level (def : 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TSP_TORQ_LIMIT_HD                 */		     100,	/* Comment [ Engine torque limit value at high target deceleration (def : 10%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TSP_TORQ_LIMIT_LD                 */		     500,	/* Comment [ Engine torque limit value at low target deceleration (def : 50%) ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TSP_TORQ_SPEED_H                  */		     960,	/* Comment [ Torq down rate speed high threshold (def : 120kph) ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* int16_t     	S16_TSP_TORQ_SPEED_L                  */		     480,	/* Comment [ Torq down rate speed Low threshold (def : 60kph) ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4000 ] */
	/* uchar8_t    	U8_TSP_TORQ_DOWN_RATE_HS              */		      70,	/* Comment [ Torq down rate when speed high threshold (def : 7%) ] */
	                                                        		        	/* ScaleVal[        7 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TSP_TORQ_DOWN_RATE_LS              */		      10,	/* Comment [ Torq down rate when speed Low threshold (def : 1%) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_TSP_INHIBIT_EXIT_TIME             */		      70,	/* Comment [ Inhibit Fade out Time (def : 70sc) ] */
	/* int16_t     	S16_TSP_OSCI_FADE_E_TIME              */		     100,	/* Comment [ Fade out control Exit Oscillation mitigation count (def : 70sc) ] */
	/* int16_t     	S16_TSP_OSCI_FADE_S_TIME              */		     210,	/* Comment [ Fade out control start after this scan since oscil mitigate (def : 147sc) ] */
	/* int16_t     	U16_TSP_FADE_END_DECEL                */		      20,	/* Comment [ Target fade decel. (def: 0.02g) ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_TSP_FADE_DEC_DECEL_RATE            */		       3,	/* Comment [ Target decel decrease rate. 0.003g/1scan (def:0.003g/1sc) ] */
	                                                        		        	/* ScaleVal[    0.003 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* int16_t     	S16_BDW_ENABLE_DISK_TEMP              */		    9600,	/* Comment [ Max. Disk Temperature for BDW enter (default : 300deg) ] */
	                                                        		        	/* ScaleVal[    300 deg ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_BDW_ENT_ALATM                     */		     200,	/* Comment [ Max Lateral Accel. for BDW enter (default : 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BDW_ENT_MPRESS                    */		      40,	/* Comment [ Max MC Pressure for BDW enter (default : 4bar) ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BDW_ENT_MTP                       */		       3,	/* Comment [ Min MTP for BDW enter (default : 3%) ] */
	/* int16_t     	S16_BDW_ENT_SPEED                     */		     480,	/* Comment [ Min Speed for BDW enter (default : 60kph) ] */
	                                                        		        	/* ScaleVal[    60 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BDW_RAIN_DETECT_TIME              */		      10,	/* Comment [ Detection Time of raining condition for BDW act (default : 10sec ) ] */
	/* uchar8_t    	U8_BDW_Initial_Clean_Enable           */		       1,	/* Comment [ Initial Brake Disc cleaning selection ] */
	/* int16_t     	S16_BDW_ACTIVE_TIME                   */		     300,	/* Comment [ BDW Act time (default: 3sec) ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BDW_INTERVAL_TIME                 */		      90,	/* Comment [ BDW Interval time, tuning value for each circuit (default : 90sec) ] */
	/* int16_t     	S16_BDW_TARGET_VOLT                   */		    3500,	/* Comment [ Target motor voltage when BDW engage (default : 3.5v) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BDW_DISABLE_DISK_TEMP             */		   12800,	/* Comment [ Min. Disk Temperature for BDW inhibit (default : 400deg) ] */
	                                                        		        	/* ScaleVal[    400 deg ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_BDW_EXT_ALATM                     */		     250,	/* Comment [ Lateral Acceleration upper limit for BDW exit (default : 0.25g) ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BDW_EXT_MPRESS                    */		      50,	/* Comment [ MC Pressure upper limit for Bdw exit (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BDW_EXT_MTP                       */		       2,	/* Comment [ MTP lower limit for Bdw exit (default : 2%) ] */
	/* int16_t     	S16_BDW_EXT_SPEED                     */		     400,	/* Comment [ Speed lower limit for BDW exit (default : 50kph) ] */
	                                                        		        	/* ScaleVal[    50 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BDW_Temp1                         */		       0,	/* Comment [ Temp Variable 1 ] */
	/* int16_t     	S16_BDW_Temp2                         */		       0,	/* Comment [ Temp Variable 2 ] */
	/* int16_t     	S16_BDW_Temp3                         */		       0,	/* Comment [ Temp Variable 3 ] */
	/* int16_t     	S16_BDW_Temp4                         */		       0,	/* Comment [ Temp Variable 4 ] */
	/* int16_t     	S16_BDW_Temp5                         */		       0,	/* Comment [ Temp Variable 5 ] */
	/* int16_t     	S16_BRAKE_DISK_FADE_TEMPERATURE       */		   16000,	/* Comment [ Disk Fading Temperature for FBC actuation (default = 500deg) ] */
	                                                        		        	/* ScaleVal[   500 deg. ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_BRAKE_DISK_MONITOR_TEMPERATURE    */		   11200,	/* Comment [ Minimum Disk Temperature for FBC actuation (default = 350deg) ] */
	                                                        		        	/* ScaleVal[   350 deg. ]	Scale [ f(x) = (X + 0) * 0.03125 ]  Range   [ -1024 <= X <= 1023.96875 ] */
	/* int16_t     	S16_BRAKE_High_MCPRESS                */		    1500,	/* Comment [ High Level Braking Input (default = 150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BRAKE_LOWMU_ABSPRESS              */		     400,	/* Comment [ Wheel Skid Press for Low-Mu detection (default = 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BRAKE_Low_MCPRESS                 */		     800,	/* Comment [ Low Level Braking Input (default = 80bar) ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BRAKE_Med_MCPRESS                 */		    1200,	/* Comment [ Medium Level Braking Input (default = 120bar) ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_BRAKE_High_Decel                   */		      60,	/* Comment [ Max. Decel for fading detection at High Level Braking Input (default = 0.6g) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_BRAKE_Low_Decel                    */		      60,	/* Comment [ Max. Decel for fading detection at Low Level Braking Input (default = 0.6g) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_BRAKE_Med_Decel                    */		      60,	/* Comment [ Max. Decel for fading detection at Medium Level Braking Input (default = 0.6g) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_FBC_FADING_DCT_COUNT_MAX           */		      20,	/* Comment [ Min. time for disk fading detection (default = 200 ms) ] */
	                                                        		        	/* ScaleVal[   200 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_FBC_ENTER_DECEL                   */		      60,	/* Comment [ Max. Decel for FBC activation (default = 0.6g) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FBC_PRESS_ENTER_THR               */		     800,	/* Comment [ Minimum Master Pressure for FBC activation (default = 80bar) ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FBC_PRESS_RATE_ENTER_THR          */		     160,	/* Comment [ Minimum Mpress rate for FBC activation (default = 16bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 16 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FBC_SPEED_ENTER_THR               */		     400,	/* Comment [ Minimun Vehicle Speed for FBC activation (default = 50km/h) ] */
	                                                        		        	/* ScaleVal[    50 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_FBC_PRESS_EXIT_THR1               */		     100,	/* Comment [ FBC exit below this Mpress value (default = 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FBC_PRESS_EXIT_THR2               */		    2000,	/* Comment [ FBC exit above this Mpress value (default = 200bar) ] */
	                                                        		        	/* ScaleVal[    200 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FBC_PRESS_EXIT_THR3               */		     300,	/* Comment [ FBC Dump Mode Activation below this Mpress value (default = 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_FBC_SPEED_EXIT_THR                */		      80,	/* Comment [ FBC exit Speed (default = 10km/h) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_FBC_PRESS_EXIT_THR3_TIMER          */		      35,	/* Comment [ If Mpress below FBC Press Exit Thres3 over than this time, FBC dump mode start (default = 350ms) ] */
	                                                        		        	/* ScaleVal[   350 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_LOW_MPRESS_FBC_ON_TIME_ACCEL      */		      10,	/* Comment [ FBC activation time when accelerating at low mpress level due to suction mode (default = 0.1sec) ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LOW_MPRESS_FBC_ON_TIME_BRAKING    */		     300,	/* Comment [ FBC activation time when braking at low mpress level due to suction mode (default = 3sec) ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LOW_MPRESS_FBC_ON_TIME_DEFAULT    */		      30,	/* Comment [ FBC activation time when being unknown at low mpress level due to suction mode (default = 0.5sec) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_FBC_EXIT_MTP1                      */		       1,	/* Comment [ Max. Throttle position for Driver's braking intention (default = 1%) ] */
	/* uchar8_t    	U8_FBC_EXIT_MTP2                      */		       3,	/* Comment [ Min. Throttle position for Driver's accelerating intention (default = 3%) ] */
	/* uint16_t    	U16_FBC_MAX_APPLY_TIME_BEFORE_ABS     */		     100,	/* Comment [ Max. apply mode time before ABS actuation (default=1000ms) ] */
	                                                        		        	/* ScaleVal[    1000 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 655350 ] */
	/* uchar8_t    	U8_FBC_DUMP_MODE_HOLD_TIME            */		       5,	/* Comment [ FBC dump hold time (default = 50ms) ] */
	                                                        		        	/* ScaleVal[    50 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_FBC_DUMP_MODE_MAX_TIME             */		      10,	/* Comment [ FBC dump mode max time (default = 100ms) ] */
	                                                        		        	/* ScaleVal[   100 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_FBC_TARGET_VOLT                   */		    3500,	/* Comment [ Target Voltage for FBC Motor actuation (default = 3.5V) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_FBC_ESP_MU_LOW                    */		     250,	/* Comment [ Low-Mu Level for prevention of FBC activation (default = 0.25g) ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_FBC_ESP_MU_MEDIUM                 */		     450,	/* Comment [ Medium-Mu Level for prevention of FBC activation (default = 0.45g) ] */
	                                                        		        	/* ScaleVal[     0.45 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_FBC_Temp1                         */		       0,	/* Comment [ Temp Variable 1 ] */
	/* int16_t     	S16_FBC_Temp2                         */		       0,	/* Comment [ Temp Variable 2 ] */
	/* int16_t     	S16_FBC_Temp3                         */		       0,	/* Comment [ Temp Variable 3 ] */
	/* int16_t     	S16_FBC_Temp4                         */		       0,	/* Comment [ Temp Variable 4 ] */
	/* int16_t     	S16_FBC_Temp5                         */		       0,	/* Comment [ Temp Variable 5 ] */
	/* uchar8_t    	S16_ESC_P_UP_INI_DUTY_R_EXT_UND       */		       3,	/* Comment [ At Extreme Under, Rear Add_Duty, Default = 3 ] */
	/* uchar8_t    	S16_EXT_UND_MQ_LMT_WEG_Hmu            */		     200,	/* Comment [ At Extreme Under, Rear Moment Increasing Weight, Default = 200 ] */
	/* uchar8_t    	S16_EXT_UND_MQ_LMT_WEG_Lmu            */		     200,	/* Comment [ At Extreme Under, Rear Moment Increasing Weight, Default = 200 ] */
	/* uchar8_t    	S16_EXT_UND_MQ_LMT_WEG_Mmu            */		     200,	/* Comment [ At Extreme Under, Rear Moment Increasing Weight, Default = 200 ] */
	/* uchar8_t    	S16_EXT_UND_PULSE_UP_CYCLE_SCAN_F     */		       5,	/* Comment [ At Extreme Under, Front Pulse_up Scan, Default = 5 ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_P_ADD                */		     100,	/* Comment [ At Extreme Under, Rear Slip_Control P-gain Add, Default = 100 ] */
	/* uchar8_t    	S16_EX_UND_PULSE_UP_DUTY_MIN          */		       3,	/* Comment [ At Extreme Under, Front min_Duty, Default = 3 ] */
	/* uchar8_t    	S16_EX_UND_PULSE_UP_INI_DUTY_F_IN     */		      15,	/* Comment [ At Extreme Under, Front Inside Ini_Duty, Default = 15 ] */
	/* uchar8_t    	S16_EX_UND_PULSE_UP_INI_DUTY_F_OUT    */		      13,	/* Comment [ At Extreme Under, Front Outside Ini_Duty, Default = 13 ] */
	/* uchar8_t    	S16_EX_UND_VAR_RISE_SCAN              */		       0,	/* Comment [ At Extreme Under, Variable rise scan, Default = 0 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_F_EXT_UND      */		       3,	/* Comment [ At Extreme Under, Dump_Gain, Default = 3 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_IN_MOMENT_Hmu     */		      28,	/* Comment [ At Extreme Under, Inside_Front_Moment, Default = 28 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_IN_MOMENT_Lmu     */		       0,	/* Comment [ At Extreme Under, Inside_Front_Moment, Default = 0 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_IN_MOMENT_Mmu     */		       0,	/* Comment [ At Extreme Under, Inside_Front_Moment, Default = 0 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_MOMENT_Hmu        */		      28,	/* Comment [ At Extreme Under, Total_Front_Moment, Default = 28 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_MOMENT_Lmu        */		       0,	/* Comment [ At Extreme Under, Total_Front_Moment, Default = 0 ] */
	/* uchar8_t    	S16_EXT_UND_MAX_FRT_MOMENT_Mmu        */		       0,	/* Comment [ At Extreme Under, Total_Front_Moment, Default = 0 ] */
	/* int16_t     	S16_EXT_UND_PRESS_FRT_SLIP_1ST        */		   -1000,	/* Comment [ At Extreme Under, Inside_Front_Slip_1st, Default = -10 ] */
	                                                        		        	/* ScaleVal[      -10 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S16_EXT_UND_PRESS_F_IN_HALF_MAX       */		      20,	/* Comment [ At Extreme Under, Press dep. on inside_Front_Slip_1st, Default = 20 ] */
	/* uchar8_t    	S16_EXT_UND_PRESS_F_IN_WEG            */		     120,	/* Comment [ At Extreme Under, Inside Front Press Weg dep. on F_IN_Half Max, Default = 120 ] */
	/* uchar8_t    	S16_EXT_UND_PRESS_F_OUT_WEG           */		     100,	/* Comment [ At Extreme Under, Outside Front Press Weg dep. on F_IN_Half Max, Default = 100 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_GAIN_Hmu       */		     -15,	/* Comment [ At Extreme Under, D-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_GAIN_Lmu       */		     -15,	/* Comment [ At Extreme Under, D-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_GAIN_Mmu       */		     -15,	/* Comment [ At Extreme Under, D-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_INC_Hmu        */		      -6,	/* Comment [ At Extreme Under, D-increase gain, [Default = -6] ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_INC_Lmu        */		     -10,	/* Comment [ At Extreme Under, D-increase gain, [Default = -10] ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_D_INC_Mmu        */		     -10,	/* Comment [ At Extreme Under, D-increase gain, [Default = -10] ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_GAIN_Hmu       */		     -15,	/* Comment [ At Extreme Under, P-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_GAIN_Lmu       */		     -15,	/* Comment [ At Extreme Under, P-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_GAIN_Mmu       */		     -15,	/* Comment [ At Extreme Under, P-gain, Default = -15 ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_INC_Hmu        */		      -3,	/* Comment [ At Extreme Under, P-increase gain, [Default = -3] ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_INC_Lmu        */		      -4,	/* Comment [ At Extreme Under, P-increase gain, [Default = -4] ] */
	/* char8_t     	S16_EXT_UND_R_US_OUT_P_INC_Mmu        */		      -3,	/* Comment [ At Extreme Under, P-increase gain, [Default = -3] ] */
	/* uchar8_t    	S16_HMU_EU_F_HIGH_SPD_GAIN_WEG        */		     100,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 100] ] */
	/* uchar8_t    	S16_HMU_EU_F_LOW_SPD_GAIN_WEG         */		     100,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 100] ] */
	/* uchar8_t    	S16_HMU_EU_F_MED_SPD_GAIN_WEG         */		     100,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 100] ] */
	/* uchar8_t    	S16_LMU_EU_F_HIGH_SPD_GAIN_WEG        */		      50,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 50] ] */
	/* uchar8_t    	S16_LMU_EU_F_LOW_SPD_GAIN_WEG         */		     100,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 100] ] */
	/* uchar8_t    	S16_LMU_EU_F_MED_SPD_GAIN_WEG         */		      80,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 80] ] */
	/* uchar8_t    	S16_MMU_EU_F_HIGH_SPD_GAIN_WEG        */		      50,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 50] ] */
	/* uchar8_t    	S16_MMU_EU_F_LOW_SPD_GAIN_WEG         */		     100,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 100] ] */
	/* uchar8_t    	S16_MMU_EU_F_MED_SPD_GAIN_WEG         */		      80,	/* Comment [ At Extreme Under, Speed weight of P/D-gain, [Default = 80] ] */
	/* int16_t     	S16_EXT_UND_DEL_YAW_2ND_ENTER         */		    1000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw on a speed of YC_Rear, Default = 10 ] */
	                                                        		        	/* ScaleVal[      10 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_DEL_YAW_2ND_EXIT          */		    1050,	/* Comment [ At Extreme Under, Exit condition Del_Yaw on a speed of YC_Rear, Default = 10.5 ] */
	                                                        		        	/* ScaleVal[    10.5 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S16_EXT_UND_MTP_ENTER                 */		     100,	/* Comment [ At Extreme Under, Enter condition MTP, Default = 100 ] */
	/* uchar8_t    	S16_EXT_UND_MTP_EXIT                  */		     100,	/* Comment [ At Extreme Under, Exit condition MTP, Default = 100 ] */
	/* int16_t     	S16_EXT_UND_VEL_ENTER                 */		     200,	/* Comment [ At Extreme Under, Enter condition Velocity, Default = 20 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EXT_UND_VEL_EXIT                  */		     150,	/* Comment [ At Extreme Under, Exit condition Velocity, Default = 15 ] */
	                                                        		        	/* ScaleVal[     15 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MAX_UND_VEL_EXIT                  */		    1000,	/* Comment [ At Extreme Under, Max condition Velocity, Default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_HMU_HSP       */		   -2000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -20 ] */
	                                                        		        	/* ScaleVal[     -20 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_HMU_LSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_HMU_MSP       */		   -2500,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -25 ] */
	                                                        		        	/* ScaleVal[     -25 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_LMU_HSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_LMU_LSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_LMU_MSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_MMU_HSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_MMU_LSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_ENT_DEL_YAW_MMU_MSP       */		   -4000,	/* Comment [ At Extreme Under, Enter condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_HMU_HSP       */		   -2000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -20 ] */
	                                                        		        	/* ScaleVal[     -20 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_HMU_LSP       */		   -4000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -40 ] */
	                                                        		        	/* ScaleVal[     -40 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_HMU_MSP       */		   -2500,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -25 ] */
	                                                        		        	/* ScaleVal[     -25 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_LMU_HSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_LMU_LSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_LMU_MSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_MMU_HSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_MMU_LSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EXT_UND_EXT_DEL_YAW_MMU_MSP       */		   -3000,	/* Comment [ At Extreme Under, Exit condition Del_Yaw_First on a speed of YC_Rear, Default = -30 ] */
	                                                        		        	/* ScaleVal[     -30 /s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	PTA_PD_GAIN_INC_FACTOR                */		     400,	/* Comment [ Phase5 PD gain increase factor ] */
	                                                        		        	/* ScaleVal[        4 - ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_1                   */		    3300,	/* Comment [ 1st gear PTA mode min. torq. Level  in Straight driving ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_2                   */		    3300,	/* Comment [ 2nd gear PTA mode min. torq. Level in Straight driving ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_3                   */		    3300,	/* Comment [ 3rd gear PTA mode min. torq. Leve in Straight driving ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_4UP                 */		    3300,	/* Comment [ 4th gear PTA mode min. torq. Level in Straight driving ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_TURN_1              */		    2200,	/* Comment [ 1st gear PTA mode min. torq. Level  in Circle driving ] */
	                                                        		        	/* ScaleVal[     220 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_TURN_2              */		    2500,	/* Comment [ 2nd gear PTA mode min. torq. Level in circle driving ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_TURN_3              */		    3000,	/* Comment [ 3rd gear PTA mode min. torq. Leve in circle drivingl ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_OK_TORQ_4_PTA_TURN_4UP            */		    3500,	/* Comment [ 4th gear PTA mode min. torq. Level in circle driving ] */
	                                                        		        	/* ScaleVal[     350 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ENG_OK_TORQ_4_NOSHTUPSHT_1        */		      30,	/* Comment [ 1st Gear shifting  min. torq. Level ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ENG_OK_TORQ_4_NOSHTUPSHT_2        */		     120,	/* Comment [ 2nd Gear shifting  min. torq. Level ] */
	                                                        		        	/* ScaleVal[      12 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ENG_OK_TORQ_4_NOSHTUPSHT_3        */		     180,	/* Comment [ 3rd Gear shiftin  min. torq. Level ] */
	                                                        		        	/* ScaleVal[      18 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ENG_OK_TORQ_4_NOSHTUPSHT_4UP      */		     200,	/* Comment [ 4th Gear shifting  min. torq. Level ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_HOMO_1           */		    3000,	/* Comment [ 1st gear_1st scan control compensation torque in straight ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_HOMO_2           */		    3000,	/* Comment [ 2nd gear_1st scan control compensation torque in straight ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_HOMO_3           */		    3000,	/* Comment [ 3rd gear_1st scan control compensation torque in straight ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_HOMO_4UP         */		    3000,	/* Comment [ 4th gear_1st scan control compensation torque in straight ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SHFT_1           */		    3300,	/* Comment [ 1st gear_1st scan control compensation torque in shift ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SHFT_2           */		    3300,	/* Comment [ 2nd gear_1st scan control compensation torque in shift ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SHFT_3           */		    3300,	/* Comment [ 3rd gear_1st scan control compensation torque in shift ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SHFT_4UP         */		    3300,	/* Comment [ 4th gear_1st scan control compensation torque in shift ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SPLT_1           */		    3300,	/* Comment [ 1st gear_1st scan control compensation torque in split ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SPLT_2           */		    3300,	/* Comment [ 2nd gear_1st scan control compensation torque in split ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SPLT_3           */		    3300,	/* Comment [ 3rd gear_1st scan control compensation torque in split ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_SPLT_4UP         */		    3300,	/* Comment [ 4th gear_1st scan control compensation torque in split ] */
	                                                        		        	/* ScaleVal[     330 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_TURN_1           */		    2200,	/* Comment [ 1st gear_1st scan control compensation torque in circle ] */
	                                                        		        	/* ScaleVal[     220 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_TURN_2           */		    2500,	/* Comment [ 2nd gear_1st scan control compensation torque in circle ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_TURN_3           */		    3000,	/* Comment [ 3rd gear_1st scan control compensation torque in circle ] */
	                                                        		        	/* ScaleVal[     300 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TRQ_TURN_4UP         */		    3500,	/* Comment [ 4th gear_1st scan control compensation torque in circle ] */
	                                                        		        	/* ScaleVal[     350 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_TRQ_DEC_RATE_SHFT_1           */		     350,	/* Comment [ 1st gear Decrese Torque in upshift ] */
	                                                        		        	/* ScaleVal[      35 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_TRQ_DEC_RATE_SHFT_2           */		     300,	/* Comment [ 2nd gear Decrese Torque in upshift ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_TRQ_DEC_RATE_SHFT_3           */		     200,	/* Comment [ 3rd gear Decrese Torque in upshift ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_TRQ_DEC_RATE_SHFT_4UP         */		     150,	/* Comment [ 4th gear Decrese Torque in upshift ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	LAUNCH_RPM                            */		    3000,	/* Comment [ Launch 제한 RPM 값 ] */
	/* int16_t     	S16_PTA_1ST_SCAN_TQ_SHFT_LAUNCH       */		       0,	/* Comment [ Launch 중 1st scan량 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PTA_TRQ_DEC_RATE_LAUNCH           */		     200,	/* Comment [ Launch 중 Torq 저감량 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_ENG_OK_TORQ_4_LAUNCH              */		      60,	/* Comment [ Engine Launch OK Torque Level ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* int16_t     	PS_PD_GAIN_INC_FACTOR                 */		       0,	/* Comment [ Engine Torque Factor in PwrShft mode ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_HOMO_1            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(homo) - 1st gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_HOMO_2            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(homo) - 2nd gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_HOMO_3            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(homo) - 3rd gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_HOMO_4UP          */		    2500,	/* Comment [ 1st scan control compensation in straight driving(homo) - 4th gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_SPLT_1            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(split) - 1st gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_SPLT_2            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(split) - 2nd gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_SPLT_3            */		    2500,	/* Comment [ 1st scan control compensation in straight driving(split) - 3rd gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PS_1ST_SCAN_TRQ_SPLT_4UP          */		    2500,	/* Comment [ 1st scan control compensation in straight driving(split) - 4th gear ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_TSPIN_ADD_THR_1                   */		     120,	/* Comment [ Add Tspin _ 1st Gear PwrShft mode ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TSPIN_ADD_THR_2                   */		     120,	/* Comment [ Add Tspin _ 2nd Gear PwrShft mode ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TSPIN_ADD_THR_3                   */		     120,	/* Comment [ Add Tspin _ 3rd Gear PwrShft mode ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_ACC_PEDAL_THR                     */		      70,	/* Comment [ POWERSHIFT Entery PEDAL_Threshold ] */
	/* uint16_t    	U16_ENG_RPM_THR_1                     */		    2000,	/* Comment [ RPM THR _ 1st Gear ] */
	/* uint16_t    	U16_ENG_RPM_THR_2                     */		    2000,	/* Comment [ RPM THR _ 2nd Gear ] */
	/* uint16_t    	U16_ENG_RPM_THR_3                     */		    2000,	/* Comment [ RPM THR _ 3rd Gear ] */
	/* uint16_t    	U16_STR_WHL_ANG_THR_1                 */		     100,	/* Comment [ Angle Threshold _ 1st Gear ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_STR_WHL_ANG_THR_2                 */		     100,	/* Comment [ Angle Threshold _ 2nd Gear ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_STR_WHL_ANG_THR_3                 */		     100,	/* Comment [ Angle Threshold _ 3rd Gear ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_VEH_ACC_THR_1                     */		      10,	/* Comment [ Vertical acceleration Threshold _ 1st Gear Entery ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_VEH_ACC_THR_2                     */		      10,	/* Comment [ Vertical acceleration Threshold _ 2nd Gear Entery ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_VEH_ACC_THR_3                     */		      10,	/* Comment [ Vertical acceleration Threshold _ 3rd Gear Entery ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_VEH_SPD_THR_1                     */		      80,	/* Comment [ Speed THR _ 1st Gear ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_VEH_SPD_THR_2                     */		      80,	/* Comment [ Speed THR _ 2nd Gear ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_VEH_SPD_THR_3                     */		      80,	/* Comment [ Speed THR _ 3rd Gear ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_HysTime_THR_1                     */		    1000,	/* Comment [ PwrShft mode Release hysteresis Time in 1st Gear ] */
	/* uint16_t    	U16_HysTime_THR_2                     */		    1000,	/* Comment [ PwrShft mode Release hysteresis Time in 2nd Gear ] */
	/* uint16_t    	U16_HysTime_THR_3                     */		    1000,	/* Comment [ PwrShft mode Release hysteresis Time in 3rd Gear ] */
	/* uchar8_t    	U8_RDCM_CONTROL_FUNCTION              */		       0,	/* Comment [ RDCM Control Function Enable, Disable ] */
	/* int16_t     	S16_RDCM_OK_TORQ_0_0                  */		     500,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_0_1                  */		     300,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_0_2                  */		     100,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_1_0                  */		     200,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_1_1                  */		     100,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_1_2                  */		      50,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_2_0                  */		     200,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_2_1                  */		     100,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_RDCM_OK_TORQ_2_2                  */		      50,	/* Comment [ In RDCM Control, Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_PTU_GEAR_RATIO                    */		    3940,	/* Comment [ PTU Gear Ratio ] */
	                                                        		        	/* ScaleVal[       3.94 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_RDM_GEAR_RATIO                    */		    2529,	/* Comment [ RDM Gear Ratio ] */
	                                                        		        	/* ScaleVal[      2.529 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_DELTA_RPM_LIMIT               */		     300,	/* Comment [ Maximum Difference Between △RPM and Target RPM ] */
	/* int16_t     	S16_TCS_1ST_SCAN_TRQ_RDCM_0           */		     700,	/* Comment [ In RDCM Control, 1st Scan Target Reduction Torque (Vehicle_Speed(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      70 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_1ST_SCAN_TRQ_RDCM_1           */		     700,	/* Comment [ In RDCM Control, 1st Scan Target Reduction Torque (Vehicle_Speed(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      70 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_1ST_SCAN_TRQ_RDCM_2           */		     700,	/* Comment [ In RDCM Control, 1st Scan Target Reduction Torque (Vehicle_Speed(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      70 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_1ST_SCAN_TRQ_RDCM_3           */		     700,	/* Comment [ In RDCM Control, 1st Scan Target Reduction Torque (Vehicle_Speed(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      70 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_0_0         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_0_1         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_0_2         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_0_3         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_1_0         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_1_1         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_1_2         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_RATE_RDCM_1_3         */		      10,	/* Comment [ In RDCM Control, △RPM>Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       1 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_0_0         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_0_1         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_0_2         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_0_3         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_1_0         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_1_1         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_1_2         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_RDCM_1_3         */		      30,	/* Comment [ In RDCM Control, △RPM<Target RPM -> Decrease Rate((acc_r) : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TVBB_FRONT_WHEEL_CTRL              */		       1,	/* Comment [ Front TVBB control : 1, Rear TVBB control : 2, default = 1 for FWD(or FWD base AWD), 0 for RWD(or RWD base AWD) ] */
	/* int16_t     	S16_TVBB_INHIBIT_CTRL_WHEEL_SLIP      */		     400,	/* Comment [ controled wheel slip threshold to inhibit TVBB, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_WSTR_DIR_CHANGE_THRES        */		      50,	/* Comment [ steering wheel angle to detect counter steer, default = 5 ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* char8_t     	S8_TVBB_ENTER_TIME                    */		       6,	/* Comment [ time to enter TVBB after all entering condition satisfied, default = 28 ] */
	                                                        		        	/* ScaleVal[    60 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* char8_t     	S8_TVBB_TURN_INTENT_DCT_TIME          */		      10,	/* Comment [ time to detect driver's turning intent, default = 50 ] */
	                                                        		        	/* ScaleVal[   100 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 1270 ] */
	/* uchar8_t    	U8_TVBB_APS_TH                        */		       5,	/* Comment [ maximum mtp threshold to enter TVBB, default = 5 ] */
	/* uchar8_t    	U8_TVBB_EXIT_TIME                     */		      60,	/* Comment [ time to exit TVBB after exit condition satisfied, default = 300 ] */
	                                                        		        	/* ScaleVal[   600 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_TVBB_EXIT_TIME_RAPID               */		       1,	/* Comment [ rapid exit time duration, default = 10 ] */
	                                                        		        	/* ScaleVal[    10 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_TVBB_AYC_TH                       */		     500,	/* Comment [ minimum calculated lateral accel threshold to enter TVBB, default = 0.5 ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_AY_HIGH_TH                   */		    1100,	/* Comment [ maximum measured lateral accel threshold to enter TVBB, default = 1.1 ] */
	                                                        		        	/* ScaleVal[      1.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_AY_LOW_TH                    */		     600,	/* Comment [ minimum measured lateral accel threshold to enter TVBB, default = 0.6 ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_ENTER_ALATM_TH_HSP           */		     600,	/* Comment [ lateral acceleration threshold at high speed, default = 0.6 ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_ENTER_ALATM_TH_LSP           */		     600,	/* Comment [ lateral acceleration threshold at low speed, default = 0.6 ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_ENTER_ALATM_TH_MSP           */		     600,	/* Comment [ lateral acceleration threshold at medium speed, default = 0.6 ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_ENTER_HSPD_LIMIT_HMU         */		     800,	/* Comment [ maximum speed limit to enter TVBB on high-mu, default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_ENTER_HSPD_LIMIT_LMU         */		     720,	/* Comment [ maximum speed limit to enter TVBB on low-mu, default = 90 ] */
	                                                        		        	/* ScaleVal[     90 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_ENTER_HSPD_LIMIT_MMU         */		     800,	/* Comment [ maximum speed limit to enter TVBB on med-mu, default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_SPD_MIN                      */		     240,	/* Comment [ minimum speed threshold to enter TVBB, default = 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_DELTA_YAW_TH_EXTI            */		     100,	/* Comment [ delta yaw threshold to exit TVBB, default = 1 ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_DELTA_YAW_TH_HSP             */		    -250,	/* Comment [ delta yaw threshold to enter TVBB at high speed, default = -2.5 ] */
	                                                        		        	/* ScaleVal[ -2.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_DELTA_YAW_TH_LSP             */		    -300,	/* Comment [ delta yaw threshold to enter TVBB at low speed, default = -3 ] */
	                                                        		        	/* ScaleVal[   -3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_DELTA_YAW_TH_MSP             */		    -300,	/* Comment [ delta yaw threshold to enter TVBB at medium speed, default = -3 ] */
	                                                        		        	/* ScaleVal[   -3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_DEL_YAW_INHIBIT_TH_ENTER     */		     200,	/* Comment [ delta yaw threshold to inhibit TVBB before TVBB activation, default = 2 ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_DEL_YAW_INHIBIT_TH_EXIT      */		     300,	/* Comment [ delta yaw threshold to inhibit TVBB during TVBB activation, default = 3 ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_ENTER_TH_REF_HSP             */		     640,	/* Comment [ reference high speed for delta yaw threshold to enter TVBB, default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_ENTER_TH_REF_LSP             */		     240,	/* Comment [ reference low speed for delta yaw threshold to enter TVBB, default = 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_ENTER_TH_REF_MSP             */		     480,	/* Comment [ reference medium speed for delta yaw threshold to enter TVBB, default = 60 ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_HM_1       */		     100,	/* Comment [ gain for delta yaw rate threshold at high mu 1 ayvx dot, default = 100 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_HM_2       */		     150,	/* Comment [ gain for delta yaw rate threshold at high mu 2 ayvx dot, default = 150 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_HM_3       */		     500,	/* Comment [ gain for delta yaw rate threshold at high mu 3 ayvx dot, default = 500 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_LM_1       */		     100,	/* Comment [ gain for delta yaw rate threshold at low mu 1 ayvx dot, default = 100 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_LM_2       */		     150,	/* Comment [ gain for delta yaw rate threshold at low mu 2 ayvx dot, default = 150 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_LM_3       */		     500,	/* Comment [ gain for delta yaw rate threshold at low mu 3 ayvx dot, default = 500 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_MM_1       */		     100,	/* Comment [ gain for delta yaw rate threshold at med mu 1 ayvx dot, default = 100 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_MM_2       */		     150,	/* Comment [ gain for delta yaw rate threshold at med mu 2 ayvx dot, default = 150 ] */
	/* int16_t     	S16_TVBB_TH_AY_VX_DOT_GAIN_MM_3       */		     500,	/* Comment [ gain for delta yaw rate threshold at med mu 3 ayvx dot, default = 500 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_HM_1        */		    5000,	/* Comment [ reference ayvx dot for enter del yaw calculation at high mu 1, default=5000 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_HM_2        */		    5500,	/* Comment [ reference ayvx dot for enter del yaw calculation at high mu 2, default=5500 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_HM_3        */		    6000,	/* Comment [ reference ayvx dot for enter del yaw calculation at high mu 3, default=6000 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_LM_1        */		    5000,	/* Comment [ reference ayvx dot for enter del yaw calculation at low mu 1, default=5000 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_LM_2        */		    5500,	/* Comment [ reference ayvx dot for enter del yaw calculation at low mu 2, default=5500 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_LM_3        */		    6000,	/* Comment [ reference ayvx dot for enter del yaw calculation at low mu 3, default=6000 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_MM_1        */		    5000,	/* Comment [ reference ayvx dot for enter del yaw calculation at med mu 1, default=5000 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_MM_2        */		    5500,	/* Comment [ reference ayvx dot for enter del yaw calculation at med mu 2, default=5500 ] */
	/* int16_t     	S16_TVBB_TH_REF_AY_VX_DOT_MM_3        */		    6000,	/* Comment [ reference ayvx dot for enter del yaw calculation at med mu 3, default=6000 ] */
	/* char8_t     	S8_TVBB_ENTER_WHEEL_SPIN_H            */		       0,	/* Comment [ Enter threshold of wheel spin for high reference lateral acceleration, default  = 0 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_TVBB_ENTER_WHEEL_SPIN_L            */		      40,	/* Comment [ Enter threshold of wheel spin for low reference lateral acceleration, default  = 4 ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_TVBB_ENTER_WHEEL_SPIN_M            */		      20,	/* Comment [ Enter threshold of wheel spin for med reference lateral acceleration, default  = 2 ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_TVBB_INHIBIT_WH_SLIP_HSP           */		     -40,	/* Comment [ If negative wheel slip of control wheel is less than this value, TVBB control is inhibited in high speed range, default = -4 ] */
	                                                        		        	/* ScaleVal[       -4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_TVBB_INHIBIT_WH_SLIP_LSP           */		     -70,	/* Comment [ If negative wheel slip of control wheel is less than this value, TVBB control is inhibited in low speed range, default = -7 ] */
	                                                        		        	/* ScaleVal[       -7 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_TVBB_INHIBIT_WH_SLIP_MSP           */		     -40,	/* Comment [ If negative wheel slip of control wheel is less than this value, TVBB control is inhibited in med speed range, default = -4 ] */
	                                                        		        	/* ScaleVal[       -4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* uchar8_t    	U8_TVBB_ENTER_SPIN_REF_ALATM_H        */		      60,	/* Comment [ High reference lateral acceleration for enter threshold of wheel spin, default = 0.6 ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TVBB_ENTER_SPIN_REF_ALATM_L        */		      30,	/* Comment [ Low reference lateral acceleration for enter threshold of wheel spin, default = 0.3 ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TVBB_ENTER_SPIN_REF_ALATM_M        */		      50,	/* Comment [ Med reference lateral acceleration for enter threshold of wheel spin, default = 0.5 ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_TVBB_ENTER_MOMENT                 */		     -30,	/* Comment [ Control moment threshold to enter TVBB ] */
	/* int16_t     	S16_TVBB_EXIT_MOMENT                  */		       0,	/* Comment [ Control moment threshold to exit TVBB ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_ENGINE_HTQ       */		     100,	/* Comment [ PD gain weighting when engine torque is high, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_ENGINE_LTQ       */		     100,	/* Comment [ PD gain weighting when engine torque is low, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_ENGINE_MHTQ      */		     100,	/* Comment [ PD gain weighting when engine torque is med-high, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_ENGINE_MLTQ      */		     100,	/* Comment [ PD gain weighting when engine torque is med-low, default = 100 ] */
	/* int16_t     	S16_TVBB_ENGINE_HTQ                   */		    2000,	/* Comment [ reference high engine torque to determine PD gain weighting, default = 200 ] */
	                                                        		        	/* ScaleVal[     200 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_ENGINE_LTQ                   */		     500,	/* Comment [ reference low engine torque to determine PD gain weighting, default = 50 ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_ENGINE_MHTQ                  */		    1500,	/* Comment [ reference med-high engine torque to determine PD gain weighting, default = 150 ] */
	                                                        		        	/* ScaleVal[     150 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_ENGINE_MLTQ                  */		    1000,	/* Comment [ reference med-low engine torque to determine PD gain weighting, default = 100 ] */
	                                                        		        	/* ScaleVal[     100 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_DEC_HM             */		      90,	/* Comment [ del yaw D gain when understeer decrease on high-mu, default = 90 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_DEC_LM             */		      30,	/* Comment [ del yaw D gain when understeer decrease on low-mu, default = 30 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_DEC_MM             */		      60,	/* Comment [ del yaw D gain when understeer decrease on med-mu, default = 60 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_INC_HM             */		      90,	/* Comment [ del yaw D gain when understeer increase on high-mu, default = 90 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_INC_LM             */		      30,	/* Comment [ del yaw D gain when understeer increase on low-mu, default = 30 ] */
	/* int16_t     	S16_TVBB_YC_D_GAIN_INC_MM             */		      60,	/* Comment [ del yaw D gain when understeer increase on med-mu, default = 60 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_DEC_HM             */		      30,	/* Comment [ del yaw P gain when understeer decrease on high-mu, default = 30 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_DEC_LM             */		      10,	/* Comment [ del yaw P gain when understeer decrease on low-mu, default = 10 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_DEC_MM             */		      20,	/* Comment [ del yaw P gain when understeer decrease on med-mu, default = 20 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_INC_HM             */		      30,	/* Comment [ del yaw P gain when understeer increase on high-mu, default = 30 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_INC_LM             */		      10,	/* Comment [ del yaw P gain when understeer increase on low-mu, default = 10 ] */
	/* int16_t     	S16_TVBB_YC_P_GAIN_INC_MM             */		      20,	/* Comment [ del yaw P gain when understeer increase on med-mu, default = 20 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_HMU_HSPD         */		     100,	/* Comment [ PD gain weighting on high mu at high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_HMU_LSPD         */		     100,	/* Comment [ PD gain weighting on high mu at low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_HMU_MHSPD        */		     100,	/* Comment [ PD gain weighting on high mu at med-high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_HMU_MLSPD        */		     100,	/* Comment [ PD gain weighting on high mu at med-low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_HMU_HSPD                     */		    1000,	/* Comment [ reference high speed to determine PD gain weighting on high mu, default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_HMU_LSPD                     */		     300,	/* Comment [ reference low speed to determine PD gain weighting on high mu, default = 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_HMU_MHSPD                    */		     800,	/* Comment [ reference med-high speed to determine PD gain weighting on high mu, default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_HMU_MLSPD                    */		     500,	/* Comment [ reference med-low speed to determine PD gain weighting on high mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_LMU_HSPD         */		     100,	/* Comment [ PD gain weighting on low mu at high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_LMU_LSPD         */		     100,	/* Comment [ PD gain weighting on low mu at low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_LMU_MHSPD        */		     100,	/* Comment [ PD gain weighting on low mu at med-high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_LMU_MLSPD        */		     100,	/* Comment [ PD gain weighting on low mu at med-low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_LMU_HSPD                     */		    1000,	/* Comment [ reference high speed to determine PD gain weighting on low mu, default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_LMU_LSPD                     */		     300,	/* Comment [ reference low speed to determine PD gain weighting on low mu, default = 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_LMU_MHSPD                    */		     800,	/* Comment [ reference med-med speed to determine PD gain weighting on low mu, default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_LMU_MLSPD                    */		     500,	/* Comment [ reference med-low speed to determine PD gain weighting on low mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_MMU_HSPD         */		     100,	/* Comment [ PD gain weighting on med mu at high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_MMU_LSPD         */		     100,	/* Comment [ PD gain weighting on med mu at low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_MMU_MHSPD        */		     100,	/* Comment [ PD gain weighting on med mu at med-high speed, default = 100 ] */
	/* int16_t     	S16_TVBB_CTRL_PD_WEG_MMU_MLSPD        */		     100,	/* Comment [ PD gain weighting on med mu at med-low speed, default = 100 ] */
	/* int16_t     	S16_TVBB_MMU_HSPD                     */		    1000,	/* Comment [ reference high speed to determine PD gain weighting on med mu, default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MMU_LSPD                     */		     300,	/* Comment [ reference low speed to determine PD gain weighting on med mu, default = 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MMU_MHSPD                    */		     800,	/* Comment [ reference med-med speed to determine PD gain weighting on med mu, default = 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MMU_MLSPD                    */		     500,	/* Comment [ reference med-low speed to determine PD gain weighting on med mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_HMU_1      */		    -400,	/* Comment [ 1st reference wheel spin(slip) to determine control gain on high mu, default = -4 ] */
	                                                        		        	/* ScaleVal[       -4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_HMU_2      */		    -200,	/* Comment [ 2nd reference wheel spin(slip) to determine control gain on high mu, default = -2 ] */
	                                                        		        	/* ScaleVal[       -2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_HMU_3      */		       0,	/* Comment [ 3rd reference wheel spin(slip) to determine control gain on high mu, default = 0 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_HMU_4      */		     200,	/* Comment [ 4th reference wheel spin(slip) to determine control gain on high mu, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_HMU_5      */		     400,	/* Comment [ 5th reference wheel spin(slip) to determine control gain on high mu, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_1       */		      50,	/* Comment [ Control gain for 1st reference wheel spin(slip) on high mu, default = 50 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_2       */		      90,	/* Comment [ Control gain for 2nd reference wheel spin(slip) on high mu, default = 90 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_3       */		     100,	/* Comment [ Control gain for 3rd reference wheel spin(slip) on high mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_4       */		     100,	/* Comment [ Control gain for 4th reference wheel spin(slip) on high mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_5       */		     110,	/* Comment [ Control gain for 5th reference wheel spin(slip) on high mu, default = 110 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_LMU_1      */		    -400,	/* Comment [ 1st reference wheel spin(slip) to determine control gain on low mu, default = -4 ] */
	                                                        		        	/* ScaleVal[       -4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_LMU_2      */		    -200,	/* Comment [ 2nd reference wheel spin(slip) to determine control gain on low mu, default = -2 ] */
	                                                        		        	/* ScaleVal[       -2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_LMU_3      */		       0,	/* Comment [ 3rd reference wheel spin(slip) to determine control gain on low mu, default = 0 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_LMU_4      */		     200,	/* Comment [ 4th reference wheel spin(slip) to determine control gain on low mu, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_LMU_5      */		     400,	/* Comment [ 5th reference wheel spin(slip) to determine control gain on low mu, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_LMU_1       */		      50,	/* Comment [ Control gain for 1st reference wheel spin(slip) on low mu, default = 50 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_LMU_2       */		      90,	/* Comment [ Control gain for 2nd reference wheel spin(slip) on low mu, default = 90 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_LMU_3       */		     100,	/* Comment [ Control gain for 3rd reference wheel spin(slip) on low mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_LMU_4       */		     100,	/* Comment [ Control gain for 4th reference wheel spin(slip) on low mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_LMU_5       */		     110,	/* Comment [ Control gain for 5th reference wheel spin(slip) on low mu, default = 110 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_MMU_1      */		    -400,	/* Comment [ 1st reference wheel spin(slip) to determine control gain on med mu, default = -4 ] */
	                                                        		        	/* ScaleVal[       -4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_MMU_2      */		    -200,	/* Comment [ 2nd reference wheel spin(slip) to determine control gain on med mu, default = -2 ] */
	                                                        		        	/* ScaleVal[       -2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_MMU_3      */		       0,	/* Comment [ 3rd reference wheel spin(slip) to determine control gain on med mu, default = 0 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_MMU_4      */		     200,	/* Comment [ 4th reference wheel spin(slip) to determine control gain on med mu, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TVBB_CTRL_REF_WHL_SLIP_MMU_5      */		     400,	/* Comment [ 5th reference wheel spin(slip) to determine control gain on med mu, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_MMU_1       */		      50,	/* Comment [ Control gain for 1st reference wheel spin(slip) on med mu, default = 50 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_MMU_2       */		      90,	/* Comment [ Control gain for 2nd reference wheel spin(slip) on med mu, default = 90 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_MMU_3       */		     100,	/* Comment [ Control gain for 3rd reference wheel spin(slip) on med mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_MMU_4       */		     100,	/* Comment [ Control gain for 4th reference wheel spin(slip) on med mu, default = 100 ] */
	/* uchar8_t    	U8_TVBB_CTRL_WEG_FOR_SLIP_MMU_5       */		     110,	/* Comment [ Control gain for 5th reference wheel spin(slip) on med mu, default = 110 ] */
	/* uint16_t    	U16_TVBB_TOD_LIM_TORQ                 */		     250,	/* Comment [ minimum TOD torque limitation, default = 250 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_1ST_G              */		      40,	/* Comment [ engine torque up level at 1st gear when TVBB engine torque-up function is activated, default = 4 ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_2ND_G              */		      80,	/* Comment [ engine torque up level at 2nd gear when TVBB engine torque-up function is activated, default = 8 ] */
	                                                        		        	/* ScaleVal[       8 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_3RD_G              */		     130,	/* Comment [ engine torque up level at 3rd gear when TVBB engine torque-up function is activated, default = 13 ] */
	                                                        		        	/* ScaleVal[      13 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_4TH_G              */		     180,	/* Comment [ engine torque up level at 4th gear when TVBB engine torque-up function is activated, default = 18 ] */
	                                                        		        	/* ScaleVal[      18 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_5TH_G              */		     230,	/* Comment [ engine torque up level at 5th gear when TVBB engine torque-up function is activated, default = 23 ] */
	                                                        		        	/* ScaleVal[      23 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_6TH_G              */		     270,	/* Comment [ engine torque up level at 6th gear when TVBB engine torque-up function is activated, default = 27 ] */
	                                                        		        	/* ScaleVal[      27 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_7TH_G              */		     270,	/* Comment [ engine torque up level at 7th gear when TVBB engine torque-up function is activated, default = 27 ] */
	                                                        		        	/* ScaleVal[      27 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_E_TORQ_UP_8TH_G              */		     270,	/* Comment [ engine torque up level at 8th gear when TVBB engine torque-up function is activated, default = 27 ] */
	                                                        		        	/* ScaleVal[      27 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_INIT_MOTOR_VOLTAGE           */		    2500,	/* Comment [ initial target motor voltage after TVBB engaged, default = 2.5 ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOLTAGE_HSP            */		    3000,	/* Comment [ target motor voltage at high speed, default = 3 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOLTAGE_LSP            */		    3000,	/* Comment [ target motor voltage at low speed, default = 3 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOLTAGE_MHSP           */		    3000,	/* Comment [ target motor voltage at med-high speed, default = 3 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOLTAGE_MLSP           */		    2500,	/* Comment [ target motor voltage at med-low speed, default = 2.5 ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOLTAGE_MSP            */		    2000,	/* Comment [ target motor voltage at med speed, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 32.767 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOL_REF_SPD_H          */		    1000,	/* Comment [ reference high speed to determine target motor voltage, 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOL_REF_SPD_L          */		     300,	/* Comment [ reference low speed to determine target motor voltage, 30 ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOL_REF_SPD_M          */		     600,	/* Comment [ reference med speed to determine target motor voltage, 60 ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOL_REF_SPD_MH         */		     800,	/* Comment [ reference med-high speed to determine target motor voltage, 80 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_MOTOR_VOL_REF_SPD_ML         */		     450,	/* Comment [ reference med-low speed to determine target motor voltage, 45 ] */
	                                                        		        	/* ScaleVal[     45 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 3276.7 ] */
	/* int16_t     	S16_TVBB_CTRL_MOMENT_HIGH             */		     300,	/* Comment [ reference high control moment to determine TC valve current, default = 300 ] */
	/* int16_t     	S16_TVBB_CTRL_MOMENT_LOW              */		       0,	/* Comment [ reference low control moment to determine TC valve current, default = 0 ] */
	/* int16_t     	S16_TVBB_CTRL_MOMENT_MEDHIGH          */		     200,	/* Comment [ reference med-high control moment to determine TC valve current, default = 200 ] */
	/* int16_t     	S16_TVBB_CTRL_MOMENT_MEDLOW           */		     100,	/* Comment [ reference med-low control moment to determine TC valve current, default = 100 ] */
	/* int16_t     	S16_TVBB_CURRENT_HIGH_HMU             */		     550,	/* Comment [ TC valve current when control moment is high on high mu, default = 550 ] */
	/* int16_t     	S16_TVBB_CURRENT_HIGH_LMU             */		     550,	/* Comment [ TC valve current when control moment is high on low mu, default = 550 ] */
	/* int16_t     	S16_TVBB_CURRENT_HIGH_MMU             */		     550,	/* Comment [ TC valve current when control moment is high on med mu, default = 550 ] */
	/* int16_t     	S16_TVBB_CURRENT_LOW_HMU              */		     300,	/* Comment [ TC valve current when control moment is low on high mu, default = 300 ] */
	/* int16_t     	S16_TVBB_CURRENT_LOW_LMU              */		     300,	/* Comment [ TC valve current when control moment is low on low mu, default = 300 ] */
	/* int16_t     	S16_TVBB_CURRENT_LOW_MMU              */		     300,	/* Comment [ TC valve current when control moment is low on med mu, default = 300 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDHIGH_HMU          */		     470,	/* Comment [ TC valve current when control moment is med-high on high mu, default = 470 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDHIGH_LMU          */		     470,	/* Comment [ TC valve current when control moment is med-high on low mu, default = 470 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDHIGH_MMU          */		     470,	/* Comment [ TC valve current when control moment is med-high on med mu, default = 470 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDLOW_HMU           */		     400,	/* Comment [ TC valve current when control moment is med-low on high mu, default = 400 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDLOW_LMU           */		     400,	/* Comment [ TC valve current when control moment is med-low on low mu, default = 400 ] */
	/* int16_t     	S16_TVBB_CURRENT_MEDLOW_MMU           */		     400,	/* Comment [ TC valve current when control moment is med-low on med mu, default = 400 ] */
	/* int16_t     	S16_TVBB_INITIAL_CURRENT              */		    1500,	/* Comment [ initial tc valve current during initial full on time, default = 1500 ] */
	/* int16_t     	S16_TVBB_MIN_EXIT_CURRENT             */		     250,	/* Comment [ exit tc valve current when TVBB exit, default = 250 ] */
	/* uint16_t    	U16_TVBB_INIT_FULL_ON_TIME            */		      14,	/* Comment [ initial tc valve current full on time, default = 70 ] */
	                                                        		        	/* ScaleVal[    98 msec ]	Scale [ f(x) = (X + 0) * 7 ]  Range   [ 0 <= X <= 458745 ] */
	/* int16_t     	S16_SPAS_BRK_CTRL_MAX_LIMIT           */		    1000,	/* Comment [ SBC Maximum Limit Control Time (default = 10sec) ] */
	                                                        		        	/* ScaleVal[     10 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SPAS_FB_MOTOR_MAX                 */		    7000,	/* Comment [ Maximum Motor Voltage for Feed-back (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SPAS_FB_MOTOR_MIN                 */		    1000,	/* Comment [ Minimum Motor Voltage for Feed-back (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SPAS_FF_MOTOR                     */		   14000,	/* Comment [ Motor Voltage for Feed-forward (default = 14V) ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SPAS_TC_CUR_DROP_1ST              */		      50,	/* Comment [ Dropped TC Current at Fade-out 1st Scan (default = 50mA) ] */
	/* uchar8_t    	U8_SPAS_F_O_CUR_DROP1                 */		      20,	/* Comment [ Dropped TC Current in 1st Fade-out period (default = 20mA) ] */
	/* uchar8_t    	U8_SPAS_F_O_CUR_DROP2                 */		       5,	/* Comment [ Dropped TC Current in 2nd Fade-out period (default = 5mA) ] */
	/* uchar8_t    	U8_SPAS_F_O_CYCLE_SCAN_REF1           */		       2,	/* Comment [ Hold scans of Dropped TC Current for 1st Fade-out period (default = 2scans) ] */
	/* uchar8_t    	U8_SPAS_F_O_CYCLE_SCAN_REF2           */		       2,	/* Comment [ Hold scans of Dropped TC Current for 2nd Fade-out period (default = 2scans) ] */
	/* int16_t     	S16_SPAS_1ST_HOLD_TIME                */		      50,	/* Comment [ Holding Time for 1st Target Pressure (default = 1000msec) ] */
	                                                        		        	/* ScaleVal[   500 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SPAS_PRESS_DOWN_TIME1             */		     100,	/* Comment [ Adaptation Time to 1st Target Pressure (default = 500msec) ] */
	                                                        		        	/* ScaleVal[  1000 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SPAS_PRESS_DOWN_TIME2             */		     150,	/* Comment [ Adaptation Time to 2nd Target Pressure (default = 1500msec) ] */
	                                                        		        	/* ScaleVal[  1500 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_SPAS_RISE_TO_HOLD_DELAY_TIME       */		       5,	/* Comment [ Detect Time for Vehicle Stop (default = 50msec) ] */
	                                                        		        	/* ScaleVal[    50 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_SPAS_FF_CTRL_MAX_TIME             */		      50,	/* Comment [ SBC Maximum Feed-forward Control Time (default = 500msec) ] */
	                                                        		        	/* ScaleVal[   500 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SPAS_FF_CTRL_MIN_SPD              */		      24,	/* Comment [ SBC Minimum Feed-forward Control Velocity (default = 3kph) ] */
	                                                        		        	/* ScaleVal[      3 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_SPAS_FF_CTRL_TIME                 */		      40,	/* Comment [ SBC Minimum Feed-forward Control Time (default = 400msec) ] */
	                                                        		        	/* ScaleVal[   400 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SPAS_TC_CUR_FB_RISE               */		    1100,	/* Comment [ TC Curruent for Feed-back (default = 1100mA) ] */
	/* int16_t     	S16_SPAS_TC_CUR_FF_RISE               */		    1100,	/* Comment [ TC Curruent for Feed-forward (default = 1100mA) ] */
	/* int16_t     	S16_SBC_T_P_TEMP_1                    */		       0,	/* Comment [ SBC temporarily T/P 1 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_10                   */		       0,	/* Comment [ SBC temporarily T/P 10 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_2                    */		       0,	/* Comment [ SBC temporarily T/P 2 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_3                    */		       0,	/* Comment [ SBC temporarily T/P 3 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_4                    */		       0,	/* Comment [ SBC temporarily T/P 4 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_5                    */		       0,	/* Comment [ SBC temporarily T/P 5 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_6                    */		       0,	/* Comment [ SBC temporarily T/P 6 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_7                    */		       0,	/* Comment [ SBC temporarily T/P 7 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_8                    */		       0,	/* Comment [ SBC temporarily T/P 8 ] */
	/* int16_t     	S16_SBC_T_P_TEMP_9                    */		       0,	/* Comment [ SBC temporarily T/P 9 ] */
	/* int16_t     	S16_HOB_ENTER_INIT_MP_RATE            */		     150,	/* Comment [ Min. Mpress rate at initial braking stage for HBB Over Boost activation (default=15bar/3scan) ] */
	                                                        		        	/* ScaleVal[ 15 bar/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HOB_ENTER_MPRESS_LEVEL            */		     800,	/* Comment [ Min. Mpress for HBB Over Boost activation (default=80bar) ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HOB_ENTER_SPEED_MIN               */		     480,	/* Comment [ Min. speed for HBB Over Boost activation (default=60km/h) ] */
	                                                        		        	/* ScaleVal[    60 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HOB_EXIT_MPRESS_LEVEL_HIGH        */		    1500,	/* Comment [ High level Mpress for HBB Over Boost exit after ABS activation (default=150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HOB_EXIT_MPRESS_LEVEL_LOW         */		     400,	/* Comment [ Low level Mpress for HBB Over Boost exit (default=40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HOB_EXIT_SPEED_MIN                */		      80,	/* Comment [ Low speed level for control exit during HBB Over Boost activation (default=10km/h) ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HOB_MSC_TARGET_V                  */		    5000,	/* Comment [ Motor target voltage during HOB activation (default=5V) ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uchar8_t    	U8_HOB_MSC_INIT_FULL_ON_TIME          */		       0,	/* Comment [ Full duty motor actuation time at initial stage of HOB activation (default=0ms) ] */
	                                                        		        	/* ScaleVal[       0 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_HOB_Temp1                         */		       0,	/* Comment [ Temp Variable 1 ] */
	/* int16_t     	S16_HOB_Temp2                         */		       0,	/* Comment [ Temp Variable 2 ] */
	/* int16_t     	S16_HOB_Temp3                         */		       0,	/* Comment [ Temp Variable 3 ] */
	/* int16_t     	S16_HOB_Temp4                         */		       0,	/* Comment [ Temp Variable 4 ] */
	/* int16_t     	S16_HOB_Temp5                         */		       0,	/* Comment [ Temp Variable 5 ] */
	/* uchar8_t    	U8_HOB_Temp1                          */		       0,	/* Comment [ Temp Variable 6 ] */
	/* uchar8_t    	U8_HOB_Temp2                          */		       0,	/* Comment [ Temp Variable 7 ] */
	/* uchar8_t    	U8_HOB_Temp3                          */		       0,	/* Comment [ Temp Variable 8 ] */
	/* uchar8_t    	U8_HOB_Temp4                          */		       0,	/* Comment [ Temp Variable 9 ] */
	/* uchar8_t    	U8_HOB_Temp5                          */		       0,	/* Comment [ Temp Variable 10 ] */
	/* uchar8_t    	K_VDC_Min_Activation_Time             */		      80,	/* Comment [ 0.1 ~25.5, Resolution 0.1, Default 0.5, Unit Sec, Tolerance +/-0.1 ] */
	/* uint16_t    	K_VDC_Min_Display_Time                */		       0,	/* Comment [ 0.1 ~25.5, Resolution 0.1, Default 3.0, Unit Sec, Tolerance +/-0.1 ] */
	/* uint16_t    	K_VDC_Mode_Switch_Hold_Time           */		     300,	/* Comment [ 0.1 ~25.5, Resolution 0.1, Default 5.0, Unit Sec, Tolerance +/-0.1 ] */
	/* uchar8_t    	ESC_LAMP_ON_TIME_SET_BY_FUNCTION      */		       0,	/* Comment [ ESC Lamp Minimum Activation Time Enable by Function ] */
	/* uchar8_t    	VDC_Min_Activation_Time_Normal        */		     107,	/* Comment [ ESC Lamp Minimum Activation Time by Normal Control ] */
	/* uchar8_t    	ESC_Min_Activation_Time               */		      21,	/* Comment [ ESC Lamp Minimum Activation Time by ESC Intervention ] */
	/* uchar8_t    	ROP_Min_Activation_Time               */		       1,	/* Comment [ ESC Lamp Minimum Activation Time by ROP Intervention ] */
	/* uchar8_t    	U8_ESC_BRAKE_INTERVENTION             */		     100,	/* Comment [ ESC Brake Intervention Pressure Request ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_ENGINE_INTERVENTION            */		       5,	/* Comment [ ESC Engine Intervention Percentage of Drive Torque ] */
	/* uchar8_t    	TCS_Min_Activation_Time               */		      21,	/* Comment [ ESC Lamp Minimum Activation Time by TCS Intervention ] */
	/* uchar8_t    	U8_TCS_BRAKE_INTERVENTION             */		     100,	/* Comment [ TCS Brake Intervention Pressure Request ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_ENGINE_INTERVENTION            */		       5,	/* Comment [ TCS Engine Intervention Percentage of Drive Torque ] */
	/* uchar8_t    	U8_PACC_ENABLE                        */		       0,	/* Comment [ Pre_ABS function enable ] */
	/* int16_t     	S16_PAC_ENTER_SPEED_2ND_P             */		     320,	/* Comment [ Pre-ABS 2nd vehicle speed point for mpress enter threshold ] */
	                                                        		        	/* ScaleVal[     40 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_PAC_ENTER_SPEED_3RD_P             */		     480,	/* Comment [ Pre-ABS 3rd vehicle speed point for mpress enter threshold ] */
	                                                        		        	/* ScaleVal[     60 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_PAC_ENTER_SPEED_4TH_P             */		     960,	/* Comment [ Pre-ABS 4th vehicle speed point for mpress enter threshold ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_PAC_ENTER_SPEED_MAX               */		    2000,	/* Comment [ Pre-ABS enter maximum vehicle speed threshold ] */
	                                                        		        	/* ScaleVal[    250 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_PAC_ENTER_SPEED_MIN               */		      80,	/* Comment [ Pre-ABS enter minmum vehicle speed threshold and 1st point for mpress enter threshold ] */
	                                                        		        	/* ScaleVal[     10 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_PAC_ENTER_TH_MPRESS               */		     300,	/* Comment [ Pre-ABS enter vehicle mpress threshold 1st point ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_PAC_ENTER_TH_MPRESS_2ND_P         */		     300,	/* Comment [ Pre-ABS enter vehicle mpress threshold 2nd point ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_PAC_ENTER_TH_MPRESS_3RD_P         */		     600,	/* Comment [ Pre-ABS enter vehicle mpress threshold 3rd point ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_PAC_ENTER_TH_MPRESS_4TH_P         */		     700,	/* Comment [ Pre-ABS enter vehicle mpress threshold 4th point ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_PAC_ENTER_TH_WPRESS_MIN           */		     300,	/* Comment [ Pre-ABS enter minimum wheel pressure threshold ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* char8_t     	S8_PAC_ENTER_TH_DV                    */		       8,	/* Comment [ Pre-ABS enter wheel dV(vehicle speed - wheel speed) ] */
	                                                        		        	/* ScaleVal[      1 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 15 ] */
	/* char8_t     	S8_PAC_ENTER_TH_SLIP                  */		      -1,	/* Comment [ Pre-ABS enter wheel slip threshold,default 2% (2~6%) ] */
	/* char8_t     	S8_PAC_ENTER_TH_VEHICLE_DECEL         */		     -10,	/* Comment [ Pre-ABS enter vehicle deceleration ] */
	                                                        		        	/* ScaleVal[     -0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.25 <= X <= 1.25 ] */
	/* char8_t     	S8_PAC_ENTER_TH_WHEEL_DECEL           */		      -4,	/* Comment [ Pre-ABS enter wheel deceleration ] */
	                                                        		        	/* ScaleVal[       -1 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -31 <= X <= 31 ] */
	/* uchar8_t    	U8_PAC_ENTER_TH_MPRESS_SLOP           */		      42,	/* Comment [ Pre-ABS enter vehicle mpress slope threshold, default 1.5bar/scan ] */
	                                                        		        	/* ScaleVal[ 1.05 bar/scan ]	Scale [ f(x) = (X + 0) * 0.025 ]  Range   [ 0 <= X <= 6.25 ] */
	/* uchar8_t    	U8_PAC_END_DP_CHECK_CNT               */		      12,	/* Comment [ Min. reapply counter to check PAC end in case of small delta pressure ] */
	/* uchar8_t    	U8_PAC_FADEOUT_END_CNT_REF            */		      35,	/* Comment [ Pre-ABS end reapply counter after counter fade-out start ] */
	/* uchar8_t    	U8_PAC_FADEOUT_HOLD_TIME              */		       3,	/* Comment [ Pre-ABS reapply hold time at fade-out control mode ] */
	/* uchar8_t    	U8_PAC_FADEOUT_START_CNT_REF          */		      30,	/* Comment [ Pre-ABS start reapply counter for transition to fade-out mode ] */
	/* uchar8_t    	U8_PAC_PULSE_UP_HOLD_TIME_PH1         */		       2,	/* Comment [ Pre-ABS reapply hold time at phase #1 ] */
	/* uchar8_t    	U8_PAC_PULSE_UP_HOLD_TIME_PH2         */		       5,	/* Comment [ Pre-ABS reapply hold time at phase #2 ] */
	/* uchar8_t    	U8_PAC_PULSE_UP_HOLD_TIME_PH3         */		      10,	/* Comment [ Pre-ABS reapply hold time at phase #3 ] */
	/* char8_t     	S8_PAC_FADE_OUT_REAPPLY_TIME          */		       7,	/* Comment [ Pre-ABS enter vehicle mpress threshold 1st point ] */
	/* char8_t     	S8_PAC_REAPPLY_TIME_PH1               */		       9,	/* Comment [ Pre-ABS reapply time at phase #1 ] */
	/* char8_t     	S8_PAC_REAPPLY_TIME_PH2               */		       9,	/* Comment [ Pre-ABS reapply time at phase #2 ] */
	/* char8_t     	S8_PAC_REAPPLY_TIME_PH3               */		       7,	/* Comment [ Pre-ABS reapply time at phase #3 ] */
	/* char8_t     	S8_PREABS_SLIP_FOR_RISE_T_MAX         */		       0,	/* Comment [ Wheel slip to apply PREABS_RISE_T_MAX , default 1%, min 0%, max 5% ] */
	/* char8_t     	S8_PRE_ABS_MAX_OPTIMAL_SLIP           */		       6,	/* Comment [ Optimal slip at S16_PRE_ABS_ENTER_MIN_SPEED , default 6% (4~20%) ] */
	/* char8_t     	S8_PRE_ABS_MIN_OPTIMAL_SLIP           */		       2,	/* Comment [ Optimal slip at 120kph , default 2% (2~10%) ] */
	/* uchar8_t    	U8_PREABS_RISE_T_AT_OPT_SLIP          */		       5,	/* Comment [ U8_PREABS_RISE_T_AT_OPT_SLIP ] */
	/* uchar8_t    	U8_PREABS_RISE_T_MAX                  */		       8,	/* Comment [ Rise time at optimal slip band, default 8ms, min 7ms, max 10ms ] */
	/* int16_t     	S16_ABS_EXIT_SPEED_HIGH_MU            */		      32,	/* Comment [ ABS control min. speed after pre-ABS control cycle ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 250 ] */
	/* char8_t     	S8_AFTER_PAC_REAPPLY_TIME_F           */		       8,	/* Comment [ ABS conventional reapply time when PAC to ABS transition ] */
	/* char8_t     	S8_PAC_DECEL_F_OUT_ABS                */		      -6,	/* Comment [ -b threshol when PAC to ABS thansition ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -7.5 <= X <= 7.5 ] */
	/* char8_t     	S8_PRE_ABS_OUT_TH_SLIP                */		       6,	/* Comment [ ABS enter slip condition when PAC to ABS transition ] */
	/* uchar8_t    	U8_DV_PAC_F_OUT_ABS                   */		       8,	/* Comment [ dV threshol when PAC to ABS thansition ] */
	                                                        		        	/* ScaleVal[      1 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 12.5 ] */
	/* uchar8_t    	U8_PRE_ABS_MIN_ALLOWED_PULSE_UP       */		       3,	/* Comment [ Pre-ABS minimum pulse up times for apply PAC2ABS threshold(-b+dV) ] */
	/* int16_t     	S16_ESP_COMPETITIVE_TEMP_1            */		       0,
	/* int16_t     	S16_ESP_COMPETITIVE_TEMP_2            */		       0,
	/* int16_t     	S16_ESP_COMPETITIVE_TEMP_3            */		       0,
	/* int16_t     	S16_ESP_COMPETITIVE_TEMP_4            */		       0,
	/* int16_t     	S16_ESP_COMPETITIVE_TEMP_5            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_01            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_02            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_03            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_04            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_05            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_06            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_07            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_08            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_09            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_10            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_11            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_12            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_13            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_14            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_15            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_16            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_17            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_18            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_19            */		       0,
	/* uchar8_t    	U8_ESP_COMPETITIVE_TEMP_20            */		       0,
	/* int16_t     	S16_LAC_ACCEL_MODE_CALC_CNT           */		      80,	/* Comment [ Vehicle_Load_Estimation__ACCEL_MODE_CALC_CNT ] */
	/* int16_t     	S16_LAC_DETECTED_GVW_LOAD             */		      45,	/* Comment [ Vehicle_Load_Estimation__DETECTED_GVW_LOAD ] */
	/* int16_t     	S16_LAC_DETECTED_GVW_TIME             */		     300,	/* Comment [ Vehicle_Load_Estimation__DETECTED_GVW_TIME ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LAC_DETECTED_RESET_TIME           */		   18000,	/* Comment [ Vehicle_Load_Estimation__DETECTED_RESET_TIME ] */
	                                                        		        	/* ScaleVal[    180 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LAC_FR_MODE_CALC_CNT              */		    1000,	/* Comment [ Vehicle_Load_Estimation__FR_MODE_CALC_CNT ] */
	                                                        		        	/* ScaleVal[     10 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LAC_RESETED_GVW_LOAD              */		      30,	/* Comment [ Vehicle_Load_Estimation__RESETED_GVW_LOAD ] */
	/* int16_t     	S16_LAC_RESETED_GVW_TIME              */		     300,	/* Comment [ Vehicle_Load_Estimation__RESETED_GVW_TIME ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LAC_VEHICLE_MASS_MAXLOAD          */		    3100,	/* Comment [ Vehicle_Load_Estimation__VEHICLE_MASS_MAXLOAD ] */
	/* int16_t     	S16_LAC_VEHICLE_MASS_UNLOAD           */		    2300,	/* Comment [ Vehicle_Load_Estimation__VEHICLE_MASS_UNLOAD ] */
	/* int16_t     	S16_LAC_VEHICLE_SPEED_MAX             */		     800,	/* Comment [ Vehicle_Load_Estimation__VEHICLE_SPEED_MAX_1/8kph ] */
	/* int16_t     	S16_LAC_VEHICLE_SPEED_MIN             */		     120,	/* Comment [ Vehicle_Load_Estimation__VEHICLE_SPEED_MIN_1/8kph ] */
	/* int16_t     	S16_LAC_YAW_MAX                       */		     500,	/* Comment [ Vehicle_Load_Estimation__LAC_YAW_MAX ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_LAC_MODEL_AIR_AREA                 */		      25,	/* Comment [ Vehicle_Load_Estimation__MODEL_AIR_AREA ] */
	                                                        		        	/* ScaleVal[    2.5 m^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_LAC_MODEL_AIR_CD                   */		       3,	/* Comment [ Vehicle_Load_Estimation__MODEL_AIR_CD ] */
	                                                        		        	/* ScaleVal[   0.3 zero ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_LAC_MODEL_AIR_RO                   */		      12,	/* Comment [ Vehicle_Load_Estimation__MODEL_AIR_RO ] */
	                                                        		        	/* ScaleVal[ 1.2 kg/m^3 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_LAC_MODEL_ROLL_MU                  */		      15	/* Comment [ Vehicle_Load_Estimation__MODEL_ROLL_MU ] */
	                                                        		        	/* ScaleVal[ 0.015 zero ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
};

#define LOGIC_CAL_MODULE_24_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

