#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_LEngTunePar
	#include "Mdyn_autosar.h"
#endif
#include "Abc_Types.h"
//#if defined (__HMC_CCP) && (__HMC_CCP==ENABLE)

    #if defined __GM_CAL_SET
        #define __SET_ENG_VAR_PER_CALSET
    #else
        #define __ENG_VAR_SET_PER_ENG_VEH_TYPE
    #endif

    #define S16_AUTO_TM_ID  1
    #define S16_MANUAL_TM_ID  2
    #define S16_DCT_TM_ID   3
    #define S16_CVT_TM_ID   4    

    #if defined (__ENG_VAR_SET_PER_ENG_VEH_TYPE)

        #if (__CAR == KMC_HM)

            #if __SUSPENSION_TYPE == US
                #include "./Hm_US/Cal_data/HM_TUNapEngVarStruct.h"
            #else
                #include "./Hm/Cal_data/HM_TUNapEngVarStruct.h"
            #endif

        #elif (__CAR == HMC_LM)
			#if __SUSPENSION_TYPE == DOM
            	#include "./LM/Cal_data/LM_TUNapEngVarStruct.h"
            #else
            
            #endif

        #elif (__CAR==GM_J300)

            #include "./J300/Cal_data/J300_TUNapEngVarStruct.h"

        #elif (__CAR==GM_MALIBU)

            #include "./MALIBU/Cal_data/Malibu_TUNapEngVarStruct.h"
        
        #elif (__CAR==GM_XTS)

            #include "./XTS/Cal_data/XTS_TUNapEngVarStruct.h"    

        #elif (__CAR==GM_LAMBDA)

            #include "./LAMBDA/Cal_data/LAMBDA_TUNapEngVarStruct.h"
            
        #elif (__CAR==GM_TAHOE)

            #include "./TAHOE/Cal_data/TAHOE_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_EN)

            #include "./EN/Cal_data/EN_TUNapEngVarStruct.h"

        #elif (__CAR==BMW740i)

            #include "./BMW740i/Cal_data/BMW740i_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_BH)

            #include "./BH/Cal_data/BH_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_HG)

            #if __SUSPENSION_TYPE == US
                #include "./HG_US/Cal_data/HG_TUNapEngVarStruct.h"
            #else
                #include "./HG/Cal_data/HG_TUNapEngVarStruct.h"
            #endif
            
        #elif (__CAR==KMC_TF)
            
            #include "./TF/Cal_data/TF_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_YF)
            
            #include "./YF/Cal_data/YF_TUNapEngVarStruct.h"
            
        #elif (__CAR==HMC_DH)
            #if __SUSPENSION_TYPE == DOM
            #include "./DH/Cal_data/DH_TUNapEngVarStruct.h"
            #else
            
            #endif

        #elif (__CAR==HMC_VI)

            #if  __SUSPENSION_TYPE == US
                #include "./VI_US/Cal_data/VI_TUNapEngVarStruct.h"
            #else
                #include "./VI/Cal_data/VI_TUNapEngVarStruct.h"
            #endif
            
        #elif (__CAR==GM_S4500)

			#if __ESP_UPLEVEL==ENABLE
				#include "./GM_S4500_UPLEVEL/Cal_data/GM_S4500_UPLEVEL_TUNapEngVarStruct.h"
            #else
                #ifdef _GM_ESC_BASE_VARIANT
                	#include "./GM_S4500/Cal_data/GM_S4500_TUNapEngVarStruct.h"
                #else
                	#include "./GM_S4500_UPLEVEL/Cal_data/GM_S4500_UPLEVEL_TUNapEngVarStruct.h"
                #endif	
            #endif
            
        #elif (__CAR==DCX_COMPASS)

            #include "./COMPASS/Cal_data/COMPASS_TUNapEngVarStruct.h"

       #elif (__CAR==GM_SILVERADO)

            #include "./SILVERADO/Cal_data/SILVERADO_TUNapEngVarStruct.h"

        #elif (__CAR==PSA_C4)

            #include "./C4/Cal_data/C4_TUNapEngVarStruct.h"

        #elif (__CAR==GM_T300)

            #if __ESP_UPLEVEL==ENABLE
				#include "./GM_T300_UPLEVEL/Cal_data/GM_T300_UPLEVEL_TUNapEngVarStruct.h"
            #else
                #ifdef _GM_ESC_BASE_VARIANT
                	#include "./GM_T300/Cal_data/GM_T300_TUNapEngVarStruct.h"
                #else
                	#include "./GM_T300_UPLEVEL/Cal_data/GM_T300_UPLEVEL_TUNapEngVarStruct.h"
                #endif	
            #endif

        #elif (__CAR==HMC_YFE)
            
            #include "./YFE/Cal_data/YFE_TUNapEngVarStruct.h"
            
        #elif (__CAR==KMC_TFE)
          #if __SUSPENSION_TYPE == DOM
          	#include "./TFE/Cal_data/TFE_TUNapEngVarStruct.h"
          #elif __SUSPENSION_TYPE == US
              #include "./TFE_US/Cal_data/TFE_TUNapEngVarStruct.h"
		  #elif __SUSPENSION_TYPE == EU
              #include "./TFE_EU/Cal_data/TFE_TUNapEngVarStruct.h"
		  #elif __SUSPENSION_TYPE == CODING_TYPE 
            #include "./TFE_CODING/Cal_data/TFE_TUNapEngVarStruct_01.h"
            #include "./TFE_CODING/Cal_data/TFE_TUNapEngVarStruct_02.h"
            #include "./TFE_CODING/Cal_data/TFE_TUNapEngVarStruct_03.h"
		  #else
		    #error "Engine cal structure not defined"
		  #endif

        #elif (__CAR==KMC_PSEV)
          #if __SUSPENSION_TYPE == DOM
          	#include "./PSEV/Cal_data/PSEV_TUNapEngVarStruct.h"
          #elif __SUSPENSION_TYPE == US
            #include "./PSEV_US/Cal_data/PSEV_TUNapEngVarStruct.h"
		  #elif __SUSPENSION_TYPE == EU
			#include "./PSEV_EU/Cal_data/PSEV_TUNapEngVarStruct.h"
		  #elif __SUSPENSION_TYPE == CODING_TYPE
            #include "./PSEV_CODING/Cal_data/PSEV_TUNapEngVarStruct_01.h"
            #include "./PSEV_CODING/Cal_data/PSEV_TUNapEngVarStruct_02.h"
            #include "./PSEV_CODING/Cal_data/PSEV_TUNapEngVarStruct_03.h"
		  #else
		    #error "Engine cal structure not defined"
		  #endif
		  
	    #elif (__CAR==HMC_HGE)
            
            #include "./HGE/Cal_data/HGE_TUNapEngVarStruct.h"

        #elif (__CAR==KMC_VGE)
            
            #include "./VGE/Cal_data/VGE_TUNapEngVarStruct.h"
            
        #elif (__CAR==HMC_LFPE)
            
            #include "./LFPE/Cal_data/LFPE_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_TLFC)
            
            #include "./TLFC/Cal_data/TLFC_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_LFE)
            
            #include "./LFE/Cal_data/LFE_TUNapEngVarStruct.h"

        #else
        

            #error "Engine cal structure not defined"

        #endif

    #elif defined (__SET_ENG_VAR_PER_CALSET)
        
        /* Eng var struct not defined */
        
    #endif

//#endif
