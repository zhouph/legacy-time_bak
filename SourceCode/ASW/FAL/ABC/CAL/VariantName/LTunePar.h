#ifndef __LTUNEPAR_H__
#define __LTUNEPAR_H__

//#if __HMC_CCP==ENABLE

#include "APCalDataForm.h"
#include "APEspModelForm.h"

#if REGION_INLINE_ENABLE == DISABLE
/* #pragma CONST_SEG LOGIC_CAL_MODULE_0 */
extern	const DATA_APCALABS_t          	 apCalAbs2WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_1 */
extern	/*const*/ DATA_APCALABS_t          	 apCalAbs4WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_2 */
extern	const DATA_APCALESP_t          	 apCalEsp2WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_3 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_2H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_4 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_4H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_5 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_AUTO;

/*Engine parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_6 */
extern	const DATA_APCALESPENG_t       	 apCalEspEng01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_7 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_8 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_9 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng04;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_10 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng05;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_11 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng06;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_12 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng07;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_13 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng08;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_14 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng09;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_15 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng10;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_16 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng11;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_17 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng12;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_18 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng13;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_19 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng14;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_20 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng15;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_21 */
extern	/*const*/ DATA_APCALESPENG_t       	 apCalEspEng16;

/*VAFs parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_22 */
extern	const DATA_APCALABSVAFS_t        apCalAbsVafs2WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_23 */
extern	/*const*/ DATA_APCALABSVAFS_t        apCalAbsVafs4WD;

/* #pragma CONST_SEG LOGIC_CAL_MODULE_24 */
extern	const DATA_APCALESPVAFS_t        apCalEspVafs2WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_25 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_2H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_26 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_4H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_27 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_AUTO;

//extern DATA_APCALRBCOEM_t	apCalRbcOem2WD;

//  #if __MGH80_MOCi == ENABLE
///* #pragma CONST_SEG LOGIC_CAL_MODULE_28 */
//extern	/*const*/ DATA_APCALEPB_t        apCalEpb01;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_29 */
//extern	/*const*/ DATA_APCALEPB_t        apCalEpb02;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_30 */
//extern	/*const*/ DATA_APCALEPB_t        apCalEpb03;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_31 */
//extern	/*const*/ DATA_APCALEPB_t        apCalEpb04;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_32 */
//extern	/*const*/ DATA_APCALEPB_t        apCalEpb05;
//  #endif
//
//#if  __AHB_GEN3_SYSTEM == ENABLE
///* #pragma CONST_SEG LOGIC_CAL_MODULE_0 */
//extern	/*const*/ DATA_APCALAHB_t          	apCalAhb;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_1 */
//extern	const DATA_APCALRBC_t          	apCalRbc; /*KYB_SILS*/
///* #pragma CONST_SEG LOGIC_CAL_MODULE_2 */
//extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlPrimary;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_3 */
//extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlSecondary;
///* #pragma CONST_SEG LOGIC_CAL_MODULE_4 */
//extern	/*const*/ DATA_APCALAHB04_t         apCalAhb04;
///* #pragma CONST_SEG DEFAULT */
//#endif /* __AHB_GEN3_SYSTEM */

/*Model parameters*/
/* #pragma CONST_SEG LOGIC_ESP_MODEL_0 */
extern const ESP_MODEL_t  apEspModel2WD;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_1 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_2H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_2 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_4H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_3 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_AUTO;
/* #pragma CONST_SEG DEFAULT */
#else //REGION_INLINE_ENABLE

/* #pragma CONST_SEG LOGIC_CAL_MODULE_0 */
extern	/*const*/ DATA_APCALABS_t          	 apCalAbs2WD_01;
extern	/*const*/ DATA_APCALABS_t          	 apCalAbs2WD_02;
extern	/*const*/ DATA_APCALABS_t          	 apCalAbs2WD_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_1 */
extern	/*const*/ DATA_APCALABS_t          	 apCalAbs4WD;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_2 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp2WD_01;
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp2WD_02;
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp2WD_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_3 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_2H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_4 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_4H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_5 */
extern	/*const*/ DATA_APCALESP_t          	 apCalEsp4WD_AUTO;



/*Engine parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_6 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng01_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_7 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng02_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_8 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng03_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_9 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng04_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_10 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng05_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_11 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng06_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_12 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng07_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_13 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng08_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_14 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng09_01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_15 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng10_01;

/*Engine parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_6 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng01_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_7 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng02_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_8 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng03_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_9 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng04_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_10 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng05_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_11 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng06_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_12 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng07_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_13 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng08_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_14 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng09_02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_15 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng10_02;

/*Engine parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_6 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng01_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_7 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng02_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_8 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng03_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_9 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng04_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_10 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng05_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_11 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng06_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_12 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng07_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_13 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng08_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_14 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng09_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_15 */
extern  /*const*/ DATA_APCALESPENG_t         apCalEspEng10_03;


/*VAFs parameters*/
/* #pragma CONST_SEG LOGIC_CAL_MODULE_22 */
extern	/*const*/ DATA_APCALABSVAFS_t        apCalAbsVafs2WD_01;
extern	/*const*/ DATA_APCALABSVAFS_t        apCalAbsVafs2WD_02;
extern	/*const*/ DATA_APCALABSVAFS_t        apCalAbsVafs2WD_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_23 */
extern	/*const*/ DATA_APCALABSVAFS_t        apCalAbsVafs4WD;

/* #pragma CONST_SEG LOGIC_CAL_MODULE_24 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs2WD_01;
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs2WD_02;
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs2WD_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_25 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_2H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_26 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_4H;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_27 */
extern	/*const*/ DATA_APCALESPVAFS_t        apCalEspVafs4WD_AUTO;

//extern DATA_APCALRBCOEM_t	apCalRbcOem2WD;

  #if __MGH80_MOCi == ENABLE
/* #pragma CONST_SEG LOGIC_CAL_MODULE_28 */
extern	/*const*/ DATA_APCALEPB_t        apCalEpb01;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_29 */
extern	/*const*/ DATA_APCALEPB_t        apCalEpb02;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_30 */
extern	/*const*/ DATA_APCALEPB_t        apCalEpb03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_31 */
extern	/*const*/ DATA_APCALEPB_t        apCalEpb04;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_32 */
extern	/*const*/ DATA_APCALEPB_t        apCalEpb05;
  #endif

#if  __AHB_GEN3_SYSTEM == ENABLE
/* #pragma CONST_SEG LOGIC_CAL_MODULE_0 */
extern	/*const*/ DATA_APCALAHB_t          	apCalAhb_01;
extern	/*const*/ DATA_APCALAHB_t          	apCalAhb_02;
extern	/*const*/ DATA_APCALAHB_t          	apCalAhb_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_1 */
extern	/*const*/ DATA_APCALRBC_t          	apCalRbc_01;
extern	/*const*/ DATA_APCALRBC_t          	apCalRbc_02;
extern	/*const*/ DATA_APCALRBC_t          	apCalRbc_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_2 */
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlPrimary_01;
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlPrimary_02;
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlPrimary_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_3 */
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlSecondary_01;
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlSecondary_02;
extern	/*const*/ DATA_APCALAHBPCTRL_t      apCalAhbPCtrlSecondary_03;
/* #pragma CONST_SEG LOGIC_CAL_MODULE_4 */
extern	/*const*/ DATA_APCALAHB04_t         apCalAhb04;
/* #pragma CONST_SEG DEFAULT */

extern	/*const*/ DATA_APCALAHBPCTRL_t      *papCalAhbPCtrlPrimary;
extern	/*const*/ DATA_APCALAHBPCTRL_t      *papCalAhbPCtrlSecondary;
#endif /* __AHB_GEN3_SYSTEM */

/*Model parameters*/
/* #pragma CONST_SEG LOGIC_ESP_MODEL_0 */
extern /*const*/ ESP_MODEL_t  apEspModel2WD_01;
extern /*const*/ ESP_MODEL_t  apEspModel2WD_02;
extern /*const*/ ESP_MODEL_t  apEspModel2WD_03;


/* #pragma CONST_SEG LOGIC_ESP_MODEL_1 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_2H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_2 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_4H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_3 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_AUTO;
/* #pragma CONST_SEG DEFAULT */
#endif

#endif
//#endif
