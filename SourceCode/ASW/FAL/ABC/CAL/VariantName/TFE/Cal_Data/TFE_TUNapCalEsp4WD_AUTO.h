#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_5_START


const /*far*/	DATA_APCALESP_t	apCalEsp4WD_AUTO =
{

	/* uint16_t    	apCalEspInfo.CDID                     */		0x0E20,
	/* uint16_t    	apCalEspInfo.CDVer                    */		0x4131,
	/* uchar8_t    	apCalEspInfo.MID                      */		0x02,
	/* uint16_t    	U16_E1_MAX_TORQUE                     */		     250,	/* Comment [ ENGINE TYPE 1 최대 토크 ] */
	/* uint16_t    	U16_E2_MAX_TORQUE                     */		     330,	/* Comment [ ENGINE TYPE 2 최대 토크 ] */
	/* uint16_t    	U16_E3_MAX_TORQUE                     */		       0,	/* Comment [ ENGINE TYPE 3 최대 토크 ] */
	/* uchar8_t    	U8_ESP_Output_Selection_Mode          */		     255,	/* Comment [ ESP, ABS, TCS, VAFs 각 test mode에 해당하는 viewer의 출력변수 선택 ] */
	/* int16_t     	K_ESC_Center_Speed_Threshold          */		      30,	/* Comment [ 센서 초기화 등의 문제로 ESC가 작동 못하는 경우에 메시지 송출하는 최소 속도 ] */
	/* int16_t     	K_ESC_Center_Time_Threshold           */		      30,	/* Comment [ 센서 초기화 등의 문제로 ESC가 작동 못하는 경우에 메시지 송출하는 최소 시간 ] */
	/* int16_t     	K_ESC_Temporary_Enable_Pressure       */		      40,	/* Comment [ ESC Switch Off 시, ABS 진입하면, 해당Pressurse 이상에서 ESC 제어 가능 ] */
	                                                        		        	/* ScaleVal[      4 Bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	K_ESC_Temporary_Enable_Speed          */		      60,	/* Comment [ ESC Switch Off 시, ABS 진입하면, 해당속도 이상에서 ESC 제어 가능 ] */
	/* char8_t     	K_Tire_Low_ESC_Override               */		       0,	/* Comment [ At TPMS is installed, ESC activation in a low pressure, 0 : Disable, 1: Enable ] */
	/* int16_t     	S16_TIRE_LOW_PRESSURE_THRESHOLD       */		     140,	/* Comment [ At TPMS is installed, ESC activation under this pressure, Default : 140kPa ] */
	/* int16_t     	S16_MU_HIGH                           */		     700,	/* Comment [ High mu 설정 기준  [0.7g] ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MU_LOW                            */		     150,	/* Comment [ Low mu 설정 기준  [0.2g] ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MU_MED                            */		     250,	/* Comment [ Medium mu 설정 기준  [0.35g] ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MU_MED_HIGH                       */		     500,	/* Comment [ Medium mu 영역 기준  [0.5g] ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MU_HIGH2                          */		     750,	/* Comment [ High mu 영역 기준  [0.5g] ] */
	                                                        		        	/* ScaleVal[     0.75 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MU_V_HIGH                         */		     850,	/* Comment [ Very High mu 설정 기준  [0.7g] ] */
	                                                        		        	/* ScaleVal[     0.85 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MODEL_C2_RATIO                    */		      14,	/* Comment [ Dynamic Modeling 시 적용되지 않음. Model damping 조절 증가하면 반응 속도 빠름 [10%~14%] ] */
	/* int16_t     	S16_LIMIT_WEIGHT_H                    */		     100,	/* Comment [ Dynamic Modeling 시 적용되지 않음. Yaw lmit 진입후 조향각 추정성, 100%이면 모델과 동일 [0~100%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_H                  */		     100,	/* Comment [ High mu, Yaw limit  게인 [80%~120%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_H_ABS              */		      87,	/* Comment [ ABS협조제어에서, High mu Yaw Limit Gain [70~110] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_H_V1               */		     100,	/* Comment [ At High_mu and Speed_V1, Yaw Limit Gain, [Default = 100] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_H_V2               */		     100,	/* Comment [ At High_mu and Speed_V2, Yaw Limit Gain, [Default = 100] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_LIMIT_H            */		      80,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소 제한(적은값일수록 제한 없슴)  [0~100%] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_H_V1         */		      90,	/* Comment [ At High_mu and Speed_V1, Yaw 2nd Limit Gain, [Default = 90] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_H_V2         */		      90,	/* Comment [ At High_mu and Speed_V2, Yaw 2nd Limit Gain, [Default = 90] ] */
	/* int16_t     	S16_YAW_LIMIT_WSTR_DEC_GAIN_H         */		     100,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소(적은값이 빨리 감소)[0~100%] ] */
	/* int16_t     	S16_YAW_LMT_WEG_SLOW_STR_MU_H         */		       0,	/* Comment [ At Yaw-Limit, weight according to slow-steering on high-mu ] */
	/* int16_t     	S16_LIMIT_WEIGHT_M                    */		     100,	/* Comment [ Dynamic Modeling 시 적용되지 않음. Yaw lmit 진입후 조향각 추정성, 100%이면 모델과 동일 [0~100%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_LIMIT_M            */		      80,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소 제한(적은값일수록 제한 없슴)  [0~100%] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_M_V1         */		      85,	/* Comment [ At Med_mu and Speed_V1, Yaw 2nd Limit Gain, [Default = 104] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_M_V2         */		      95,	/* Comment [ At Med_mu and Speed_V2, Yaw 2nd Limit Gain, [Default = 96] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_M                  */		     100,	/* Comment [ Med mu, Yaw limit  게인 [80%~120%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_M_ABS              */		      80,	/* Comment [ ABS협조제어에서, Med mu Yaw Limit Gain [70~110] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_M_V1               */		      90,	/* Comment [ At Med_mu and Speed_V1, Yaw Limit Gain, [Default = 117] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_M_V2               */		     100,	/* Comment [ At Med_mu and Speed_V2, Yaw Limit Gain, [Default = 108] ] */
	/* int16_t     	S16_YAW_LIMIT_WSTR_DEC_GAIN_M         */		     100,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소(적은값이 빨리 감소)[0~100%] ] */
	/* int16_t     	S16_YAW_LMT_WEG_SLOW_STR_MU_M         */		       0,	/* Comment [ At Yaw-Limit, weight according to slow-steering on med-mu ] */
	/* int16_t     	S16_LIMIT_WEIGHT_L                    */		     100,	/* Comment [ Dynamic Modeling 시 적용되지 않음. Yaw lmit 진입후 조향각 추정성, 100%이면 모델과 동일 [0~100%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_L                  */		     100,	/* Comment [ Low mu, Yaw limit  게인 [80%~120%] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_LIMIT_L            */		      90,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소 제한(적은값일수록 제한 없슴)  [0~100%] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_L_V1         */		      90,	/* Comment [ At Low_mu and Speed_V1, Yaw 2nd Limit Gain, [Default = 104] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_LIMIT_L_V2         */		      96,	/* Comment [ At Low_mu and Speed_V2, Yaw 2nd Limit Gain, [Default = 96] ] */
	/* int16_t     	S16_YAW_LIMIT_GAIN_L_ABS              */		      80,	/* Comment [ ABS협조제어에서, Low mu Yaw Limit Gain [70~110] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_L_V1               */		      80,	/* Comment [ At Low_mu and Speed_V1, Yaw Limit Gain, [Default = 117] ] */
	/* uchar8_t    	S16_YAW_LIMIT_GAIN_L_V2               */		      90,	/* Comment [ At Low_mu and Speed_V2, Yaw Limit Gain, [Default = 108] ] */
	/* int16_t     	S16_YAW_LIMIT_WSTR_DEC_GAIN_L         */		     100,	/* Comment [ Counter steer시 steering angle 과 연동 Yawlimit 감소(적은값이 빨리 감소)[0~100%] ] */
	/* int16_t     	S16_YAW_LMT_WEG_SLOW_STR_MU_L         */		       0,	/* Comment [ At Yaw-Limit, weight according to slow-steering on low-mu ] */
	/* int16_t     	S16_YAW_LIMIT_ST_FAST_ENTER           */		    1600,	/* Comment [ Yaw lmit 진입 조향각속도2 [200~400'/s] ] */
	                                                        		        	/* ScaleVal[  160 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ST_SLOW_ENTER           */		    1000,	/* Comment [ Yaw lmit 진입 조향각속도1 [100~200'/s] ] */
	                                                        		        	/* ScaleVal[  100 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ST_VSLOW_ENTER_H        */		     500,	/* Comment [ Yaw_Limit Enter minimum steering rate in high mu ] */
	                                                        		        	/* ScaleVal[   50 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ST_VSLOW_ENTER_M        */		     500,	/* Comment [ Yaw_Limit Enter minimum steering rate in med mu ] */
	                                                        		        	/* ScaleVal[   50 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ENTER_FA_ST_H_SP        */		    1300,	/* Comment [ 진입 조향각속도2 - 추가 조향각 속도2(FAST_ST_HIGH_SP_ENTER_ADD) 에 해당되는 차량속도3   [100 kph 이상] ] */
	                                                        		        	/* ScaleVal[   130 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ENTER_FA_ST_L_SP        */		     650,	/* Comment [ 진입 조향각속도2 에 해당되는 차량속도1   [80 kph 이하] ] */
	                                                        		        	/* ScaleVal[    65 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_ENTER_FA_ST_M_SP        */		     900,	/* Comment [ 진입 조향각속도2 - 추가 조향각 속도1(FAST_ST_MED_SP_ENTER_ADD) 에 해당되는 차량속도2   [80~100 kph ] ] */
	                                                        		        	/* ScaleVal[    90 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_FA_ST_H_SP_ENT_AD       */		    -300,	/* Comment [ 차량속도3에 해당되는 추가 조향각 속도2    [-100 ~ 0] ] */
	                                                        		        	/* ScaleVal[  -30 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_FA_ST_M_SP_ENT_AD       */		    -200,	/* Comment [ 차량속도2에 해당되는 추가 조향각 속도1    [-100 ~ 0] ] */
	                                                        		        	/* ScaleVal[  -20 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HIGH_FAST         */		     170,	/* Comment [ Low mu에서, Yaw lmit 진입 조향각속도2 에 따른 횡가속도 차[0.15~0.3g] ] */
	                                                        		        	/* ScaleVal[     0.17 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HIGH_SLOW         */		     210,	/* Comment [ High mu에서, Yaw lmit 진입 조향각속도1 에 따른 횡가속도 차 [0.3~0.5g] ] */
	                                                        		        	/* ScaleVal[     0.21 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_LOW_FAST          */		     200,	/* Comment [ Low mu에서, Yaw lmit 진입 조향각속도2 에 따른 횡가속도 차[0.15~0.3g] ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_LOW_SLOW          */		     250,	/* Comment [ Low mu에서, Yaw lmit 진입 조향각속도1 에 따른 횡가속도 차[0.3~0.5g] ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HIGH_FAST_ABS     */		     200,	/* Comment [ High mu에서, 빠른 조향각 속도에서 Yaw lmit 진입 횡가속도 차 ] */
	                                                        		        	/* ScaleVal[        0.2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HIGH_SLOW_ABS     */		     250,	/* Comment [ High mu에서, 느린 조향각 속도에서 Yaw lmit 진입 횡가속도 차 ] */
	                                                        		        	/* ScaleVal[       0.25 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_LOW_FAST_ABS      */		     200,	/* Comment [ Low mu에서, 빠른 조향각 속도에서 Yaw lmit 진입 횡가속도 차 ] */
	                                                        		        	/* ScaleVal[        0.2 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_LOW_SLOW_ABS      */		     300,	/* Comment [ Low mu에서, 느린 조향각 속도에서 Yaw lmit 진입 횡가속도 차 ] */
	                                                        		        	/* ScaleVal[        0.3 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_H_SPEED_V1              */		     300,	/* Comment [ Yaw lmit 진입 차량 속도 1 (저속) [10 ~ 50 KPH] ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_H_SPEED_V2              */		     800,	/* Comment [ Yaw lmit 진입 차량 속도 2 (중속) [60 ~ 80 KPH] ] */
	                                                        		        	/* ScaleVal[    80 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_H_SPEED_V3              */		    1100,	/* Comment [ Yaw lmit 진입 차량 속도 3 (고속) [80 KPH ~ ] ] */
	                                                        		        	/* ScaleVal[   110 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_H_V1_INC_DEL_AY         */		     150,	/* Comment [ 차량속도1 에서의 횡가속도 차 증가량 [0 ~ 0.9g] ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_H_V2_INC_DEL_AY         */		      50,	/* Comment [ 차량속도2 에서의 횡가속도 차 증가량 [0 ~ 0.3g] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_M_SPEED_V1              */		     300,	/* Comment [ Yaw lmit 진입 차량 속도 1 (저속) [10 ~ 40 KPH] ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_M_SPEED_V2              */		     500,	/* Comment [ Yaw lmit 진입 차량 속도 2 (중속) [40 ~ 60 KPH] ] */
	                                                        		        	/* ScaleVal[    50 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_M_SPEED_V3              */		     800,	/* Comment [ Yaw lmit 진입 차량 속도 3 (고속) [60 KPH ~ ] ] */
	                                                        		        	/* ScaleVal[    80 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_LIMIT_M_V1_INC_DEL_AY         */		     100,	/* Comment [ 차량속도1 에서의 횡가속도 차 증가량 [0 ~ 0.9g] ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_M_V2_INC_DEL_AY         */		      50,	/* Comment [ 차량속도2 에서의 횡가속도 차 증가량 [0 ~ 0.3g] ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_BETAD_H_SP_CON          */		     500,	/* Comment [ High Speed 시 Yaw Limit 진입 가능 Beta dot [600~1000] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YAW_LIMIT_BETAD_L_SP_CON          */		    1800,	/* Comment [ Low Speed 시 Yaw Limit 진입 가능 Beta dot [1000~2500] ] */
	                                                        		        	/* ScaleVal[         18 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YAW_LIMIT_BETAD_M_SP_CON          */		     600,	/* Comment [ Med Speed 시 Yaw Limit 진입 가능 Beta dot [600~1000] ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HMU_AY_L          */		     150,	/* Comment [ High mu, Low Speed, Beta dot에 의한 Yaw Limit 진입 시 Add 횡가속도 차 [100~1000] ] */
	                                                        		        	/* ScaleVal[       0.15 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_HMU_AY_M          */		     100,	/* Comment [ High mu, Med Speed, Beta dot에 의한 Yaw Limit 진입 시 Add횡가속도 차 [100~1000] ] */
	                                                        		        	/* ScaleVal[        0.1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_MMU_AY_L          */		     150,	/* Comment [ Med mu, Low Speed, Beta dot에 의한 Yaw Limit 진입 시 Add 횡가속도 차 [100~1000] ] */
	                                                        		        	/* ScaleVal[       0.15 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_DELTA_MMU_AY_M          */		     100,	/* Comment [ Med mu, Low Speed, Beta dot에 의한 Yaw Limit 진입 시 Add 횡가속도 차 [100~1000] ] */
	                                                        		        	/* ScaleVal[        0.1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_HMU_DEL_AY              */		     250,	/* Comment [ High mu, Beta dot에 의한 Yaw Limit 진입 시 횡가속도 차 [200~1000] ] */
	                                                        		        	/* ScaleVal[       0.25 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_YAW_LIMIT_MMU_DEL_AY              */		     250,	/* Comment [ Med mu, Beta dot에 의한 Yaw Limit 진입 시 횡가속도 차 [200~1000] ] */
	                                                        		        	/* ScaleVal[       0.25 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ax_EST_1ST                */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ax_1st. default =0.05, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ax_EST_2ND                */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ax_2nd. default =0.3, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ax_EST_3RD                */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ax_3rd. default =0.5, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ax_EST_4TH                */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ax_4th. default =0.8, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ay_VALUE_1ST              */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ay_1st. default =0.1, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ay_VALUE_2ND              */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ay_2nd. default =0.35, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ay_VALUE_3RD              */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ay_3rd. default =0.4, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BRK_LMT_Ay_VALUE_4TH              */		       0,	/* Comment [ 제동 노면한계에세, Ax에 따른 Ay 모델 설정 : Ay_4th. default =0.4, [0~1.5g] ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_BANK_LIMIT                    */		     300,	/* Comment [ ADM Vch gathering condition maximum value // default 300 -> 3deg/sec ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_CURVE_YAW                     */		     300,	/* Comment [ ADM Vch gathering condition minimum value // default 300 -> 3deg/sec ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_TEMP_1                        */		       0,	/* Comment [ ADM Vch gathering condition maximum value // default 10 -> 1deg/sec^2 ] */
	/* int16_t     	S16_ADM_TEMP_2                        */		       0,	/* Comment [ ADM Vch gathering condition maximum value // default 10 -> 1deg/sec^2 ] */
	/* int16_t     	S16_ADM_TEMP_3                        */		       0,	/* Comment [ ADM Vch gathering condition maximum value // default 10 -> 1deg/sec^2 ] */
	/* int16_t     	S16_ADM_VCH_ACC_MAX                   */		      20,	/* Comment [ ADM Vch gathering condition maximum value // default 20 -> 0.02g ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VCH_ALAT_MAX                  */		     400,	/* Comment [ ADM Vch gathering condition maximum value // default 400 -> 0.4g ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VCH_ALAT_MIN                  */		     100,	/* Comment [ ADM Vch gathering condition minimum value // default 100 -> 0.1g ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VCH_AY_COMP                   */		     200,	/* Comment [ Minimum value for Roll compensation // default 200 -> 0.2g ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VCH_AY_COMP_PERCENT           */		      85,	/* Comment [ Roll compensation level // default 85% ] */
	/* int16_t     	S16_ADM_VCH_BETADOT_MAX               */		     100,	/* Comment [ ADM Vch gathering condition maximum value // default 100 -> 1deg/sec ] */
	                                                        		        	/* ScaleVal[  1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_VCH_CRT_THR                   */		      32,	/* Comment [ 2kph   1 step Update  Limit   Threshold // default 32 -> 4kph ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_CRT_THR_F                 */		      96,	/* Comment [ 12kph   Final  Max     Limit   Threshold // default 96 -> 12kph ] */
	                                                        		        	/* ScaleVal[     12 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_DEC_MIN                   */		     -70,	/* Comment [ ADM Vch gathering condition minimum value // default -70 -> -0.07g ] */
	                                                        		        	/* ScaleVal[    -0.07 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VCH_DEL_YAW_ACC_LIMIT         */		      20,	/* Comment [ Hysterisis applied when delta_yaw_acceleration is larger than this parameter // default 20 -> 2deg/s^2 ] */
	                                                        		        	/* ScaleVal[  2 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_DEL_YAW_ACC_MAX           */		      10,	/* Comment [ Vch gathering when delta_yaw_acceleration is smaller than this parameter // default 10 -> 1deg/s^2 ] */
	                                                        		        	/* ScaleVal[  1 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_DEL_YAW_ACC_TIME          */		      10,	/* Comment [ Delta_yaw_acceleration decrease 0.1deg/s^2 after 10 scan // default 10 ] */
	/* int16_t     	S16_ADM_VCH_EEP_THR                   */		      32,	/* Comment [ Write EEPROM when the change of Vch is larger than this Thres // default 32 -> 4kph ] */
	                                                        		        	/* ScaleVal[      4 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_MAX                 */		     960,	/* Comment [ ADM Vch gathering condition maximum value // default 960 -> 120kph ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_MIN                 */		     560,	/* Comment [ ADM Vch gathering condition minimum value // default 560 -> 70kph ] */
	                                                        		        	/* ScaleVal[     70 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_S1           */		     560,	/* Comment [ 3point Interpolation velocity1 // Vch gathering condition minimum value // default 560 -> 70kph ] */
	                                                        		        	/* ScaleVal[     70 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_S2           */		     640,	/* Comment [ 3point Interpolation velocity2 // Vch gathering condition minimum value // default 640 -> 80kph ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_S3           */		     960,	/* Comment [ 3point Interpolation velocity3 // Vch gathering condition minimum value // default 960 -> 120kph ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_W1           */		     200,	/* Comment [ 3point Interpolation steer1 // Vch gathering condition minimum value // default 200 -> 20deg ] */
	                                                        		        	/* ScaleVal[     20 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_W2           */		     150,	/* Comment [ 3point Interpolation steer2 // Vch gathering condition minimum value // default 150 -> 15deg ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_SPEED_WSTEER_W3           */		     100,	/* Comment [ 3point Interpolation steer3 // Vch gathering condition minimum value // default 100 -> 10deg ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_SQRT_MIN                  */		     500,	/* Comment [ ADM Vch gathering condition minimum value // default 500 -> 500 ] */
	/* int16_t     	S16_ADM_VCH_THR_OK                    */		     160,	/* Comment [ 20kph   Deviation 10sec offset OK Condition -Think More // default 160 -> 20kph ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_WHEEL_SPIN_MAX            */		      48,	/* Comment [ ADM Vch gathering condition maximum value // default 48 -> 6kph ] */
	                                                        		        	/* ScaleVal[      6 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VCH_WSTR_ACC_MAX              */		      10,	/* Comment [ ADM Vch gathering condition maximum value // default 10 -> 1deg/sec ] */
	                                                        		        	/* ScaleVal[  1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_YAW_ACC_LIMIT             */		      20,	/* Comment [ Hysterisis applied when yaw_acceleration is larger than this parameter // default 20 ->2deg/s^2 ] */
	                                                        		        	/* ScaleVal[  2 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_YAW_ACC_MAX               */		      10,	/* Comment [ ADM Vch gathering condition maximum value // default 10 -> 1deg/sec^2 ] */
	                                                        		        	/* ScaleVal[ 1 deg/sec^2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VCH_YAW_ACC_TIME              */		      10,	/* Comment [ Yaw_acceleration decrease 0.1deg/s^2 after 10 scan // default 10 ] */
	/* int16_t     	S16_ADM_VELO_ALAT_MAX                 */		     400,	/* Comment [ ADM Velo gathering condition maximum value // default 400 -> 0.4g ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADM_VELO_BETADOT_MAX              */		     400,	/* Comment [ ADM Velo gathering condition maximum value // default 400 -> 4deg/sec ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_VELO_CRT_THR                  */		      20,	/* Comment [ 2 %     1 step Update  Limit   Threshold // default 20 -> 2% ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_CRT_THR_F                */		      80,	/* Comment [ 8 %    Final  Max     Limit   Threshold // default 80 -> 8% ] */
	                                                        		        	/* ScaleVal[        8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_EEP_THR                  */		      10,	/* Comment [ Write EEPROM when the change of Velo is larger than this Thres // default 10 -> 1% ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_MD_BETADOT_MAX           */		     100,	/* Comment [ ADM Velo gathering condition maximum value // default 100 -> 1deg/sec ] */
	                                                        		        	/* ScaleVal[  1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_VELO_RADIUS_MAX               */		      27,	/* Comment [ ADM Velo gathering condition maximum value // default 27m ] */
	/* int16_t     	S16_ADM_VELO_SPEED_MIN                */		      40,	/* Comment [ ADM Velo gathering condition minimum value // default 40 -> 5kph ] */
	                                                        		        	/* ScaleVal[      5 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ADM_VELO_THR_OK                   */		      50,	/* Comment [ 5 %     Deviation,  10sec offset OK Condition // default 50 -> 5% ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_WSTR_MAX                 */		    3600,	/* Comment [ ADM Velo gathering condition maximum value // default 3600 -> 360deg ] */
	                                                        		        	/* ScaleVal[    360 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_WSTR_MIN                 */		    1200,	/* Comment [ ADM Velo gathering condition minimum value // default 1200 -> 120deg ] */
	                                                        		        	/* ScaleVal[    120 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADM_VELO_YAW_MIN                  */		     500,	/* Comment [ ADM Velo gathering condition minimum value // default 500 -> 5deg/sec ] */
	                                                        		        	/* ScaleVal[  5 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADM_WHEEL_YAW_STB_TIME            */		     100,	/* Comment [ No wheelspin gathering time // default 100 -> 1sec ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_ADM_ACCEL_THR                      */		      70,	/* Comment [ gathering condition limit of accel percent // default 70% ] */
	/* uchar8_t    	U8_ADM_VCH_1ST_UPD_PERCENT            */		      50,	/* Comment [ 0~100% // default 50% ] */
	/* uchar8_t    	U8_ADM_VCH_1ST_UPD_TIME               */		       5,	/* Comment [ 1~8sec // default 5sec ] */
	/* uchar8_t    	U8_ADM_VCH_2ND_UPD_PERCENT            */		      70,	/* Comment [ 0~100% // default 70% ] */
	/* uchar8_t    	U8_ADM_VCH_2ND_UPD_TIME               */		       7,	/* Comment [ 1~8sec // default 7sec ] */
	/* uchar8_t    	U8_ADM_VCH_3RD_UPD_PERCENT            */		     100,	/* Comment [ 0~100% // default 100% ] */
	/* uchar8_t    	U8_ADM_VCH_3RD_UPD_TIME               */		       8,	/* Comment [ 1~8sec // default 8sec ] */
	/* uchar8_t    	U8_ADM_VCH_APPLY                      */		       1,	/* Comment [ 1: ENABLE     0: DISABLE // default 1 ] */
	/* uchar8_t    	U8_ADM_VELO_1ST_UPD_TIME              */		       5,	/* Comment [ 1~8sec // default 5sec ] */
	/* uchar8_t    	U8_ADM_VELO_2ND_UPD_TIME              */		       7,	/* Comment [ 1~8sec // default 7sec ] */
	/* uchar8_t    	U8_ADM_VELO_3RD_UPD_TIME              */		       8,	/* Comment [ 1~8sec // default 8sec ] */
	/* uchar8_t    	U8_ADM_VELO_APPLY                     */		       1,	/* Comment [ 1: ENABLE     0: DISABLE // default 1 ] */
	/* int16_t     	S16_PRE_FILL_ENTER_DELM_H             */		     300,	/* Comment [ High mu에서 Pre filing 진입 Del_M 값 ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PRE_FILL_ENTER_DELM_M             */		     900,	/* Comment [ Med mu에서 Pre filing 진입 Del_M 값 ] */
	                                                        		        	/* ScaleVal[          9 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_PRE_FILL_ENTER_SPEED_FRONT        */		     500,	/* Comment [ Front에 Pre fill enter SPEED ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRE_FILL_TARGET_VOLTAGE           */		    5000,	/* Comment [ Pre filling target voltage ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_VALVE_PRE_ACTION_TIME             */		      30,	/* Comment [ Pre filling 작동 Time ] */
	/* int16_t     	S16_VALVE_PRE_DEF_ALAT_DEC_TIME       */		      20,	/* Comment [ alat 감소 시 Pre filling 금지 시간 ] */
	/* int16_t     	S16_VALVE_PRE_OFF_TIME                */		      30,	/* Comment [ Pre filling 작동 후 작동 정지 시간 ] */
	/* int16_t     	S16_PRE_ACTION_HMU_HSP_ADD_AY         */		     -50,	/* Comment [ High mu, High speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[      -0.05 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PRE_ACTION_HMU_LSP_ADD_AY         */		     -10,	/* Comment [ High mu, Low speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[      -0.01 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PRE_ACTION_HMU_MSP_ADD_AY         */		     -10,	/* Comment [ High mu, Med speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[      -0.01 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PRE_ACTION_MMU_HSP_ADD_AY         */		    1000,	/* Comment [ Med mu, High speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PRE_ACTION_MMU_LSP_ADD_AY         */		    1000,	/* Comment [ Med mu, Low speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PRE_ACTION_MMU_MSP_ADD_AY         */		    1000,	/* Comment [ Med mu, med speed Pre filling 진입 조건 - Yaw Limit 진입조건에 add 값 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	REAR_PREFILL                          */		       0,	/* Comment [ Rear Prefill enable ] */
	/* int16_t     	S16_PRE_FILL_ENTER_DELM_H_R           */		     600,	/* Comment [ Del_M for Prefill enter condition of rear drum @ High mu. (default 600, DelM=6) ] */
	                                                        		        	/* ScaleVal[        6 N ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 30 ] */
	/* int16_t     	S16_PRE_FILL_ENTER_DELM_M_R           */		     900,	/* Comment [ Del_M for Prefill enter condition of rear drum @ Med mu. Dem_M > this -> Prefill of Rear drum (default 900,DelM=9) ] */
	                                                        		        	/* ScaleVal[        9 N ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 30 ] */
	/* int16_t     	S16_PRE_FILL_ENTER_SPEED_REAR         */		     300,	/* Comment [ Rear에 Pre fill enter SPEED ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_VALVE_PRE_OFF_TIME_REAR           */		      35,	/* Comment [ Rear, Pre fill 작동 후 다음 작동 가능 scan ] */
	/* int16_t     	S16_HMU_R_HSP_PreFILL_DELYAW          */		     100,	/* Comment [ High mu, High Speed에서, Rear, True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HMU_R_LSP_PreFILL_DELYAW          */		       0,	/* Comment [ High mu, Low Speed Rear, True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HMU_R_MSP_PreFILL_DELYAW          */		      50,	/* Comment [ High mu, Med Speed, Rear,True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HMU_R_HSP_PreFILL_LIM             */		     150,	/* Comment [ High mu, High Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HMU_R_LSP_PreFILL_LIM             */		     200,	/* Comment [ High mu, Low Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HMU_R_MSP_PreFILL_LIM             */		     200,	/* Comment [ High mu, Med Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_HSP_PreFILL_DELYAW          */		     100,	/* Comment [ Med mu, High Speed에서, Rear,True Under Control 시 Under Threshold 에 add한  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_LSP_PreFILL_DELYAW          */		       0,	/* Comment [ Med mu, Low Speed Rear, True Under Control 시 Under Threshold 에 add한 pre_fill_rear_del_yaw_conPre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_MSP_PreFILL_DELYAW          */		      50,	/* Comment [ Med mu, Med Speed, Rear, True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_HSP_PreFILL_LIM             */		     150,	/* Comment [ Med mu, High Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_LSP_PreFILL_LIM             */		     200,	/* Comment [ Med mu, Low Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_MMU_R_MSP_PreFILL_LIM             */		     200,	/* Comment [ Med mu, Med Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_HSP_PreFILL_DELYAW          */		     100,	/* Comment [ Low mu, High Speed에서, Rear, True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_LSP_PreFILL_DELYAW          */		       0,	/* Comment [ Low mu, Low Speed Rear, True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_MSP_PreFILL_DELYAW          */		      50,	/* Comment [ Low mu, Med Speed, Rear,True Under Control 시 Under Threshold 에 add한 Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_HSP_PreFILL_LIM             */		     150,	/* Comment [ Low mu, High Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_LSP_PreFILL_LIM             */		     200,	/* Comment [ Low mu, Low Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LMU_R_MSP_PreFILL_LIM             */		     200,	/* Comment [ Low mu, Med Speed Rear, Tracking Control 이나 under Control Under 시  Pre fill del yaw 진입 조건 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADD_RISE_SCAN_H_SP                */		    1000,	/* Comment [ High Speed Setting 값 ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADD_RISE_SCAN_L_SP                */		     400,	/* Comment [ Low Speed Setting 값 ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADD_RISE_SCAN_M_SP                */		     800,	/* Comment [ Med Speed Setting 값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ADD_RISE_SCAN_V_L_SP              */		     200,	/* Comment [ Very Low Speed Setting, Default = 20 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ROP_ADD_VARIABLE_RISE_SCAN        */		       9,	/* Comment [ ROP Detect 시 Add 되는 Scan ] */
	/* int16_t     	S16_VARIABLE_RISE_MODE                */		       1,	/* Comment [ Variable rise mode setting [0~1] ] */
	/* int16_t     	S16_VARIABLE_RISE_SCAN_START_H        */		      10,	/* Comment [ High mu ( > 0.5 ) 에서 VARIABLE_RISE_MODE enable시, Full rise 하는 Scan 수 [ 0 ~ 100 ] ] */
	/* int16_t     	S16_VARIABLE_RISE_SCAN_START_M        */		      10,	/* Comment [ Med mu ( < 0.5 )에서 VARIABLE_RISE_MODE enable시, Full rise 하는 Scan 수 [ 0 ~ 100 ] ] */
	/* int16_t     	S16_ADD_SCAN_H_SP_H_MU                */		       5,	/* Comment [ High mu, High Speed 시 ADD 값 ] */
	/* int16_t     	S16_ADD_SCAN_L_SP_H_MU                */		       0,	/* Comment [ High mu, Low Speed 시 ADD 값 ] */
	/* int16_t     	S16_ADD_SCAN_M_SP_H_MU                */		       3,	/* Comment [ High mu, Med Speed 시 ADD 값 ] */
	/* uchar8_t    	S16_ADD_SCAN_V_L_SP_H_MU              */		       1,	/* Comment [ At High mu, Very_Low Speed, Scan_ADD, Default = 3 ] */
	/* int16_t     	S16_ADD_SCAN_H_SP_M_MU                */		       2,	/* Comment [ Med mu, High Speed 시 ADD 값 ] */
	/* int16_t     	S16_ADD_SCAN_L_SP_M_MU                */		       0,	/* Comment [ Med mu, Low Speed 시 ADD 값 ] */
	/* int16_t     	S16_ADD_SCAN_M_SP_M_MU                */		       1,	/* Comment [ Med mu, Med Speed 시 ADD 값 ] */
	/* uchar8_t    	S16_ADD_SCAN_V_L_SP_M_MU              */		       1,	/* Comment [ At Med mu, Very_Low Speed, Scan_ADD, Default = 5 ] */
	/* int16_t     	S16_ADD_SCAN_H_SP_L_MU                */		       2,	/* Comment [ High mu, High Speed 시 ADD 값 ] */
	/* int16_t     	S16_ADD_SCAN_L_SP_L_MU                */		       0,
	/* int16_t     	S16_ADD_SCAN_M_SP_L_MU                */		       1,
	/* uchar8_t    	S16_ADD_SCAN_V_L_SP_L_MU              */		       1,	/* Comment [ At Low mu, Very_Low Speed, Scan_ADD, Default = 5 ] */
	/* int16_t     	DELTA_PRESS_L_TH_FRONT                */		     400,	/* Comment [ FRONT 델타프레셔가 이 값(40 bar) 보다 작으면 Motor 구동해서 rise하므로 델타보상듀티 계산안함, 10 bar 이하는 의미없음. 로직에서 제한 ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_PRESS_M_TH_FRONT                */		     800,	/* Comment [ FRONT 델타프레셔 중간값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_PRESS_U_TH_FRONT                */		    1500,	/* Comment [ FRONT 델타프레셔가 이 값(150 bar) 보다 작으면 델타보상듀티 계산함 ] */
	                                                        		        	/* ScaleVal[        150 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_PRESS_L_TH_FRONT                  */		     100,	/* Comment [ FRONT 휠프레셔가 이 값(10 bar) 이하일 때 프레스보상듀티 MIN값 ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_PRESS_U_TH_FRONT                  */		     800,	/* Comment [ FRONT 휠프레셔가 이 값(50 bar) 이상일 때 프레스보상듀티 MAX값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_L_FRONT    */		       0,	/* Comment [ Delta Pressure Compensation @ Front, Low Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default 0), by 1 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_M_FRONT    */		      -1,	/* Comment [ Delta Pressure Compensation @ Front, Med Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default -1), by 1 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_U_FRONT    */		      -2,	/* Comment [ Delta Pressure Compensation @ Front, High Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default -2), by 1 ] */
	/* int16_t     	PRESS_COMPENSATION_DUTY_PU_L_FRONT    */		       0,	/* Comment [ Pressure Compensation @ Front, Low Press. Tune as Master_P big->Duty down(small rise), (Default 0), by 1 ] */
	/* int16_t     	PRESS_COMPENSATION_DUTY_PU_U_FRONT    */		      -2,	/* Comment [ Pressure Compensation @ Front, High Press. Tune as Master_P big->Duty down(small rise), (Default 0), by 1 ] */
	/* int16_t     	DELTA_PRESS_L_TH_REAR                 */		     400,	/* Comment [ REAR 델타프레셔가 이 값(40 bar) 보다 작으면 Motor 구동해서 rise하므로 델타보상듀티 계산안함, 10 bar 이하는 의미없음. 로직에서 제한 ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_PRESS_M_TH_REAR                 */		     800,	/* Comment [ REAR 델타프레셔 중간값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_PRESS_U_TH_REAR                 */		    1500,	/* Comment [ REAR 델타프레셔가 이 값(150 bar) 보다 작으면 델타보상듀티 계산함 ] */
	                                                        		        	/* ScaleVal[        150 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_PRESS_L_TH_REAR                   */		     100,	/* Comment [ REAR 휠프레셔가 이 값(10 bar) 이하일 때 프레스보상듀티 MIN값 ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_PRESS_U_TH_REAR                   */		     500,	/* Comment [ REAR 휠프레셔가 이 값(50 bar) 이상일 때 프레스보상듀티 MAX값 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_L_REAR     */		       0,	/* Comment [ Delta Pressure Compensation @ Rear, Low Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default 0), by 1 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_M_REAR     */		      -1,	/* Comment [ Delta Pressure Compensation @ Rear, Med Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default -1), by 1 ] */
	/* int16_t     	DELTA_COMPENSATION_DUTY_PU_U_REAR     */		      -2,	/* Comment [ Delta Pressure Compensation @ Rear, High Del_P. (Del_P=Mst_p-Wh_P), Tune as Del_p big->Duty down, (Default -2), by 1 ] */
	/* int16_t     	PRESS_COMPENSATION_DUTY_PU_L_REAR     */		       0,	/* Comment [ Pressure Compensation @ Rear, Low Press. Tune as Master_P big->Duty down(small rise), (Default 0), by 1 ] */
	/* int16_t     	PRESS_COMPENSATION_DUTY_PU_U_REAR     */		      -1,	/* Comment [ Pressure Compensation @ Rear, High Press. Tune as Master_P big->Duty down(small rise), (Default 0), by 1 ] */
	/* uchar8_t    	S16_VAR_RISE_ADD_DUTY_MAX             */		      10,	/* Comment [ At Variable rise duty time, Max added duty, Default = 10 ] */
	/* uchar8_t    	S16_VAR_RISE_ADD_DUTY_MIN             */		       0,	/* Comment [ At Variable rise duty time, Min added duty, Default = 0 ] */
	/* int16_t     	S16_VAR_RISE_DEL_M_HIGH               */		    8500,	/* Comment [ At Variable rise duty time, Max Del_M setting, Default = 85 ] */
	                                                        		        	/* ScaleVal[         85 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_VAR_RISE_DEL_M_LOW                */		    6000,	/* Comment [ At Variable rise duty time, Min Del_M setting, Default = 60 ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_PULSE_UP_CYCLE_SCAN_F         */		       3,	/* Comment [ Front wheel의 Pulse Up 주기. ] */
	/* int16_t     	S16_ESC_PULSE_UP_CYCLE_SCAN_R         */		       3,	/* Comment [ Front wheel의 Pulse Up 주기. ] */
	/* int16_t     	S16_ESC_PULSE_UP_MAX_DUTY_F           */		      20,	/* Comment [ Upper limitation for valve open time dec/inc @ front (default 20) ] */
	/* int16_t     	S16_ESC_PULSE_UP_MAX_DUTY_R           */		      16,	/* Comment [ Upper limitation for valve open time dec/inc @ rear (default 16) ] */
	/* int16_t     	S16_ESC_PULSE_UP_MIN_DUTY_F           */		       3,	/* Comment [ Lower limitation for valve open time dec/inc @ front (default 9), rise rate up in high press -> this inc, by 1 ] */
	/* uchar8_t    	S16_ESC_PULSE_UP_MIN_DUTY_ONLY_F      */		      14,	/* Comment [ At Pulse-up rise, ESC_only_front min open time, Default = 14 ] */
	/* int16_t     	S16_ESC_PULSE_UP_MIN_DUTY_R           */		       3,	/* Comment [ Lower limitation for valve open time dec/inc @ rear (default 5), rise rate up in high press -> this inc, by 1 ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_ABS */		      14,	/* Comment [ Initial valve open time of ESC control @ ABS, front (default 10), by 1 ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_CBS */		      14,	/* Comment [ Initial valve open time of ESC control @ CBS, front (default 10), by 1 ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_H */		      12,	/* Comment [ Initial valve open time of ESC control @ high mu, front (default 13), by 1, rise rate up -> 1. Full-rise, 2. INI_DUTY, 3.ADD_CYCLE, 4. MIN_DUTY ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_L */		      10,	/* Comment [ Initial valve open time of ESC control @ low mu, front (default 14), by 1, rise rate up -> 1. Full-rise, 2.INI_DUTY, 3.ADD_CYCLE, 4. MIN_DUTY ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_M */		      10,	/* Comment [ Initial valve open time of ESC control @ med mu, front (default 14), by 1, rise rate up -> 1. Full-rise, 2.INI_DUTY, 3.ADD_CYCLE, 4. MIN_DUTY ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_ABS */		       8,	/* Comment [ Initial valve open time of ESC control @ ABS, rear (default 8), by 1 ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_CBS */		       8,	/* Comment [ Initial valve open time of ESC control @ CBS, rear (default 8), by 1 ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_H  */		       8,	/* Comment [ Initial valve open time of ESC control @ high mu, rear (default 10), by 1, rise rate up -> 1. INI_DUTY, 2.ADD_CYCLE, 3. MIN_DUTY ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_L  */		       9,	/* Comment [ Initial valve open time of ESC control @ low mu, rear (default 10), by 1, rise rate up ->1. INI_DUTY, 2.ADD_CYCLE, 3. MIN_DUTY ] */
	/* int16_t     	S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_M  */		      10,	/* Comment [ Initial valve open time of ESC control @ med mu, rear (default 10), by 1, rise rate up ->1. INI_DUTY, 2.ADD_CYCLE, 3. MIN_DUTY ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_FRONT      */		       3,	/* Comment [ Unit scan for valve open time dec/inc @ front (default 14) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_FRONT_ABS  */		       3,	/* Comment [ Unit scan for valve open time dec/inc @ ABS, front (default 10) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_FRONT_CBS  */		       3,	/* Comment [ Unit scan for valve open time dec/inc @ CBS, front (default 10) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_FRONT       */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ front (default -1) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_FRONT_ABS   */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ ABS, front (default -1) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_FRONT_CBS   */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ CBS, front (default -1) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_REAR       */		       4,	/* Comment [ Unit scan for valve open time dec/inc @ rear (default 7) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_REAR_ABS   */		       4,	/* Comment [ Unit scan for valve open time dec/inc @ ABS, rear (default 5) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_CYCLE_REAR_CBS   */		       4,	/* Comment [ Unit scan for valve open time dec/inc @ CBS, rear (default 5) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_REAR        */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ rear (default -1) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_REAR_ABS    */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ ABS, rear (default -1) ] */
	/* int16_t     	S16_ESC_PULSE_UP_ADD_DUTY_REAR_CBS    */		      -1,	/* Comment [ Add time for valve open time dec/inc each unit scan @ CBS, rear (default -1) ] */
	/* int16_t     	S16_MSC_HOLD_TARGET_VOL               */		    8000,	/* Comment [ ESC Hold 제어시, 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_HMU_HSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, High-mu, High_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_HMU_LSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, High-mu, Low-speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_HMU_MSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, High-mu, Med_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_LMU_HSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Low-mu, High_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_LMU_LSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Low-mu, Low-speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_LMU_MSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Low-mu, Med_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_MMU_HSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Med-mu, High_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_MMU_LSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Med-mu, Low-speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_REAR_TARGET_VOL_MMU_MSP       */		    8000,	/* Comment [ ESC Hold / Rear 제어시, Med-mu, Med_speed 시의 목표 모터 voltage. default = 8, [1~14] ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_RISE_TARGET_VOL_HMU           */		   12000,	/* Comment [ ESC rise 제어시, High-mu ( esp_mu > 0.7 ) 시의 목표 모터 voltage. default = 12, [1~14] ] */
	                                                        		        	/* ScaleVal[       12 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_RISE_TARGET_VOL_LMU           */		    3000,	/* Comment [ ESC rise 제어시, Low-mu 목표 모터 voltage ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MSC_RISE_TARGET_VOL_MMU           */		   10000,	/* Comment [ ESC rise 제어시, Med-mu ( esp_mu < 0.7 ) 시의 목표 모터 voltage. default = 10, [1~14] ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_INC_RATE_AFT_TVBB        */		      10,	/* Comment [ TC current increase rate after TVBB, default = 10 ] */
	/* int16_t     	S16_TCMF_USC_INITIAL_ADD_DUTY         */		       0,	/* Comment [ Initial additional current after TVBB, default = 0 ] */
	/* int16_t     	S16_TCMF_USC_INITIAL_FULL_TIME        */		       6,	/* Comment [ Initial full in time of TCMF understeer control, default = 60 ] */
	                                                        		        	/* ScaleVal[      60 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_HM_1          */		     200,	/* Comment [ TC current for 1st reference USC_R at high mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_HM_2          */		     200,	/* Comment [ TC current for 2nd reference USC_R at high mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_HM_3          */		     300,	/* Comment [ TC current for 3rd reference USC_R at high mu, default = 300 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_HM_4          */		     450,	/* Comment [ TC current for 4th reference USC_R at high mu, default = 450 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_HM_5          */		     530,	/* Comment [ TC current for 5th reference USC_R at high mu, default = 530 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_HM_1           */		     -50,	/* Comment [ 1st reference USC_R for TC current at high mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_HM_2           */		       0,	/* Comment [ 2nd reference USC_R for TC current at high mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_HM_3           */		     100,	/* Comment [ 3rd reference USC_R for TC current at high mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_HM_4           */		     200,	/* Comment [ 4th reference USC_R for TC current at high mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_HM_5           */		     400,	/* Comment [ 5th reference USC_R for TC current at high mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_MM_1          */		     150,	/* Comment [ TC current for 1st reference USC_R at med mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_MM_2          */		     150,	/* Comment [ TC current for 2nd reference USC_R at med mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_MM_3          */		     250,	/* Comment [ TC current for 3rd reference USC_R at med mu, default = 300 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_MM_4          */		     300,	/* Comment [ TC current for 4th reference USC_R at med mu, default = 450 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_MM_5          */		     350,	/* Comment [ TC current for 5th reference USC_R at med mu, default = 530 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_MM_1           */		     -50,	/* Comment [ 1st reference USC_R for TC current at med mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_MM_2           */		       0,	/* Comment [ 2nd reference USC_R for TC current at med mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_MM_3           */		     100,	/* Comment [ 3rd reference USC_R for TC current at med mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_MM_4           */		     200,	/* Comment [ 4th reference USC_R for TC current at med mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_MM_5           */		     400,	/* Comment [ 5th reference USC_R for TC current at med mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_LM_1          */		     150,	/* Comment [ TC current for 1st reference USC_R at low mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_LM_2          */		     150,	/* Comment [ TC current for 2nd reference USC_R at low mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_LM_3          */		     250,	/* Comment [ TC current for 3rd reference USC_R at low mu, default = 300 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_LM_4          */		     300,	/* Comment [ TC current for 4th reference USC_R at low mu, default = 450 ] */
	/* int16_t     	S16_TCMF_USC_INI_TC_CUR_LM_5          */		     350,	/* Comment [ TC current for 5th reference USC_R at low mu, default = 530 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_LM_1           */		     -50,	/* Comment [ 1st reference USC_R for TC current at low mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_LM_2           */		       0,	/* Comment [ 2nd reference USC_R for TC current at low mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_LM_3           */		     100,	/* Comment [ 3rd reference USC_R for TC current at low mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_LM_4           */		     200,	/* Comment [ 4th reference USC_R for TC current at low mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_INI_USC_R_LM_5           */		     400,	/* Comment [ 5th reference USC_R for TC current at low mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_TCMF_USC_H_LIMIT_TC_CUR_HM        */		     550,	/* Comment [ TC current upper limit at high mu, default = 550 ] */
	/* int16_t     	S16_TCMF_USC_H_LIMIT_TC_CUR_LM        */		     550,	/* Comment [ TC current upper limit at med mu, default = 550 ] */
	/* int16_t     	S16_TCMF_USC_H_LIMIT_TC_CUR_MM        */		     550,	/* Comment [ TC current upper limit at med mu, default = 550 ] */
	/* int16_t     	S16_TCMF_USC_L_LIMIT_TC_CUR_HM        */		     200,	/* Comment [ TC current lower limit at med mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_L_LIMIT_TC_CUR_LM        */		     150,	/* Comment [ TC current lower limit at med mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_L_LIMIT_TC_CUR_MM        */		     150,	/* Comment [ TC current lower limit at med mu, default = 200 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_HMU_HSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at high-mu/high speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_HMU_LSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at high-mu/low speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_HMU_MSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at high-mu/med speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_LMU_HSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at low-mu/high speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_LMU_LSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at low-mu/low speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_LMU_MSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at low-mu/med speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_MMU_HSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at med-mu/high speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_MMU_LSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at med-mu/low speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TARGET_VOL_MMU_MSP       */		    4000,	/* Comment [ Motor target voltage while TCMF understeer control at med-mu/med speed, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_TAR_V_INC_AFT_TVBB       */		     100,	/* Comment [ Motor target voltage increase rate after TVBB, default = 0.1 ] */
	                                                        		        	/* ScaleVal[      0.1 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCMF_USC_FADE_OUT_DEC             */		      10,	/* Comment [ TC current fade out rate at the end of TCMF understeer control, default = 10 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_F_1         */		     200,	/* Comment [ 1st reference delta pressure for LFC rise duty compensation without suction, Front default = 20 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_F_2         */		     500,	/* Comment [ 2nd reference delta pressure for LFC rise duty compensation without suction, Front default = 50 ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_F_3         */		     700,	/* Comment [ 3rd reference delta pressure for LFC rise duty compensation without suction, Front default = 70 ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_F_4         */		     900,	/* Comment [ 4th reference delta pressure for LFC rise duty compensation without suction, Front default = 90 ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_F_1         */		     -30,	/* Comment [ LFC duty compensation for 1st reference delta pressure without suction, Front default = -30 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_F_2         */		     -20,	/* Comment [ LFC duty compensation for 2nd reference delta pressure without suction, Front default = -20 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_F_3         */		     -10,	/* Comment [ LFC duty compensation for 3rd reference delta pressure without suction, Front default = -10 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_F_4         */		       0,	/* Comment [ LFC duty compensation for 4th reference delta pressure without suction, Front default = 0 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_F_1         */		       0,	/* Comment [ 1st reference delta pressure for LFC rise duty compensation with suction, Front, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_F_2         */		     600,	/* Comment [ 2nd reference delta pressure for LFC rise duty compensation with suction, Front, default = 60 ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_F_3         */		    1100,	/* Comment [ 3rd reference delta pressure for LFC rise duty compensation with suction, Front, default = 110 ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_F_1          */		      40,	/* Comment [ 1st reference M/C pressure for LFC rise duty compensation with suction, Front, default = 4 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_F_2          */		     200,	/* Comment [ 2nd reference M/C pressure for LFC rise duty compensation with suction, Front default = 20 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_F_3          */		     500,	/* Comment [ 3rd reference M/C pressure for LFC rise duty compensation with suction, Front default = 50 ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_F_1          */		    1000,	/* Comment [ 1st reference motor target voltage for LFC rise duty compensation with suction, Front, default = 1 ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_F_2          */		    4000,	/* Comment [ 2nd reference motor target voltage for LFC rise duty compensation with suction, Front, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_F_3          */		    6000,	/* Comment [ 3rd reference motor target voltage for LFC rise duty compensation with suction, Front, default = 6 ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_F_4          */		    8000,	/* Comment [ 4th reference motor target voltage for LFC rise duty compensation with suction, Front, default = 8 ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_F_1         */		     -30,	/* Comment [ LFC duty compensation for 1st reference delta pressure with suction, Front, default = -30 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_F_2         */		     -10,	/* Comment [ LFC duty compensation for 2nd reference delta pressure with suction, Front, default = -10 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_F_3         */		       0,	/* Comment [ LFC duty compensation for 3rd reference delta pressure with suction, Front, default = 0 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_F_1          */		       0,	/* Comment [ LFC duty compensation for 1st reference M/C pressure with suction, Front default = 0 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_F_2          */		      20,	/* Comment [ LFC duty compensation for 2nd reference M/C pressure with suction, Front default = 20 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_F_3          */		      30,	/* Comment [ LFC duty compensation for 3rd reference M/C pressure with suction, Front default = 30 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_F_1          */		     -30,	/* Comment [ LFC duty compensation for 1st reference target voltage with suction, Front, default = -30 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_F_2          */		     -10,	/* Comment [ LFC duty compensation for 2nd reference target voltage with suction, Front, default = -10 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_F_3          */		      -5,	/* Comment [ LFC duty compensation for 3rd reference target voltage with suction, Front, default = -5 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_F_4          */		       0,	/* Comment [ LFC duty compensation for 4th reference target voltage with suction, Front, default = 0 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_ABS_H      */		      50,	/* Comment [ Base Front LFC rise duty during ESC-ABS co-control without suction on high mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_ABS_L      */		      50,	/* Comment [ Base Front LFC rise duty during ESC-ABS co-control without suction on low mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_ABS_M      */		      50,	/* Comment [ Base Front LFC rise duty during ESC-ABS co-control without suction on med mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_H          */		      50,	/* Comment [ Base Front LFC rise duty during ESC with suction on high mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_L          */		      50,	/* Comment [ Base Front LFC rise duty during ESC with suction on low mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_FRONT_M          */		      50,	/* Comment [ Base Front LFC rise duty during ESC with suction on med mu, default = 80 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_R_1         */		     200,	/* Comment [ 1st reference delta pressure for LFC rise duty compensation without suction, Rear default = 20 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_R_2         */		     500,	/* Comment [ 2nd reference delta pressure for LFC rise duty compensation without suction, Rear default = 50 ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_R_3         */		     700,	/* Comment [ 3rd reference delta pressure for LFC rise duty compensation without suction, Rear default = 70 ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_ABS_LFC_REF_DEL_P_R_4         */		     900,	/* Comment [ 4th reference delta pressure for LFC rise duty compensation without suction, Rear default = 90 ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_R_1         */		     -30,	/* Comment [ LFC duty compensation for 1st reference delta pressure without suction, Rear default = -30 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_R_2         */		     -20,	/* Comment [ LFC duty compensation for 2nd reference delta pressure without suction, Rear default = -20 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_R_3         */		     -10,	/* Comment [ LFC duty compensation for 3rd reference delta pressure without suction, Rear default = -10 ] */
	/* char8_t     	S8_ESC_ABS_LFC_DEL_P_COMP_R_4         */		       0,	/* Comment [ LFC duty compensation for 4th reference delta pressure without suction, Rear default = 0 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_R_1         */		       0,	/* Comment [ 1st reference delta pressure for LFC rise duty compensation with suction, Rear, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_R_2         */		     600,	/* Comment [ 2nd reference delta pressure for LFC rise duty compensation with suction, Rear, default = 60 ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_DEL_PRESS_R_3         */		    1100,	/* Comment [ 3rd reference delta pressure for LFC rise duty compensation with suction, Rear, default = 110 ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_R_1          */		      40,	/* Comment [ 1st reference M/C pressure for LFC rise duty compensation with suction, Rear, default = 4 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_R_2          */		     200,	/* Comment [ 2nd reference M/C pressure for LFC rise duty compensation with suction, Rear default = 20 ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MC_PRESS_R_3          */		     500,	/* Comment [ 3rd reference M/C pressure for LFC rise duty compensation with suction, Rear default = 50 ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_R_1          */		    2000,	/* Comment [ 1st reference motor target voltage for LFC rise duty compensation with suction, Rear, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_R_2          */		    3000,	/* Comment [ 2nd reference motor target voltage for LFC rise duty compensation with suction, Rear, default = 3 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_R_3          */		    4000,	/* Comment [ 3rd reference motor target voltage for LFC rise duty compensation with suction, Rear, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ESC_LFC_REF_MTR_VOLT_R_4          */		    4000,	/* Comment [ 4th reference motor target voltage for LFC rise duty compensation with suction, Rear, default = 4 ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_R_1         */		     -30,	/* Comment [ LFC duty compensation for 1st reference delta pressure with suction, Rear, default = -30 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_R_2         */		     -10,	/* Comment [ LFC duty compensation for 2nd reference delta pressure with suction, Rear, default = -10 ] */
	/* char8_t     	S8_ESC_LFC_DEL_PRESS_COMP_R_3         */		       0,	/* Comment [ LFC duty compensation for 3rd reference delta pressure with suction, Rear, default = 0 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_R_1          */		       0,	/* Comment [ LFC duty compensation for 1st reference M/C pressure with suction, Rear default = 0 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_R_2          */		      20,	/* Comment [ LFC duty compensation for 2nd reference M/C pressure with suction, Rear default = 20 ] */
	/* char8_t     	S8_ESC_LFC_MC_PRESS_COMP_R_3          */		      30,	/* Comment [ LFC duty compensation for 3rd reference M/C pressure with suction, Rear default = 30 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_R_1          */		     -15,	/* Comment [ LFC duty compensation for 1st reference target voltage with suction, Rear, default = -15 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_R_2          */		     -10,	/* Comment [ LFC duty compensation for 2nd reference target voltage with suction, Rear, default = -10 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_R_3          */		       0,	/* Comment [ LFC duty compensation for 3rd reference target voltage with suction, Rear, default = 0 ] */
	/* char8_t     	S8_ESC_LFC_MTR_VOLT_COMP_R_4          */		       0,	/* Comment [ LFC duty compensation for 4th reference target voltage with suction, Rear, default = 0 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_ABS_H       */		      80,	/* Comment [ Base Rear LFC rise duty during ESC-ABS co-control without suction on high mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_ABS_L       */		      80,	/* Comment [ Base Rear LFC rise duty during ESC-ABS co-control without suction on low mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_ABS_M       */		      80,	/* Comment [ Base Rear LFC rise duty during ESC-ABS co-control without suction on med mu, default = 80 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_H           */		     100,	/* Comment [ Base Rear LFC rise duty during ESC with suction on high mu, default = 100 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_L           */		     100,	/* Comment [ Base Rear LFC rise duty during ESC with suction on low mu, default = 100 ] */
	/* uchar8_t    	U8_ESC_LFC_RISE_DUTY_REAR_M           */		     100,	/* Comment [ Base Rear LFC rise duty during ESC with suction on med mu, default = 100 ] */
	/* int16_t     	S16_UND_2WH_FADE_OUT_DEC              */		      10,	/* Comment [ TC current fade out rate at the end of 2wheel under steer control, default = 10 ] */
	/* uchar8_t    	U8_UND_2WH_MSC_ADD_TARGET_VOLT        */		      20,	/* Comment [ Additional target voltage during 2wheel under steer control, default = 2 ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_UND_2WH_MSC_INITIAL_TARGET_V       */		      60,	/* Comment [ Initial target voltage during 2wheel under steer control, default = 6 ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_UND_2WH_MSC_INITIAL_TIME           */		      30,	/* Comment [ Time duration for initial target voltage during 2wheel under steer control, default = 300 ] */
	                                                        		        	/* ScaleVal[     300 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_UND_2WH_USC_INITIAL_COMP_TIME      */		      25,	/* Comment [ Time duration for initial TC valve current compensation during 2wheel under steer control, default = 250 ] */
	                                                        		        	/* ScaleVal[     250 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_UND_2WH_USC_TC_COMP_RATIO          */		     130,	/* Comment [ Initial TC valve current compensation rate, default = 130 ] */
	/* int16_t     	S16_UND_2WH_H_LIMIT_TC_CUR_HM         */		     600,	/* Comment [ TC current upper limit during 2wheel under steer control on high mu, default = 600 ] */
	/* int16_t     	S16_UND_2WH_L_LIMIT_TC_CUR_HM         */		     150,	/* Comment [ TC current lower limit during 2wheel under steer control on high mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_F_HM_1                */		     -50,	/* Comment [ 1st reference USC_F for TC current during 2wheel under steer control on high mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_HM_2                */		       0,	/* Comment [ 2nd reference USC_F for TC current during 2wheel under steer control on high mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_HM_3                */		     100,	/* Comment [ 3rd reference USC_F for TC current during 2wheel under steer control on high mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_HM_4                */		     200,	/* Comment [ 4th reference USC_F for TC current during 2wheel under steer control on high mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_HM_5                */		     400,	/* Comment [ 5th reference USC_F for TC current during 2wheel under steer control on high mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_HM_1           */		     150,	/* Comment [ TC current for 1st reference USC_F during 2wheel under steer control on high mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_HM_2           */		     200,	/* Comment [ TC current for 2nd reference USC_F during 2wheel under steer control on high mu, default = 200 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_HM_3           */		     350,	/* Comment [ TC current for 3rd reference USC_F during 2wheel under steer control on high mu, default = 350 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_HM_4           */		     550,	/* Comment [ TC current for 4th reference USC_F during 2wheel under steer control on high mu, default = 550 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_HM_5           */		     600,	/* Comment [ TC current for 5th reference USC_F during 2wheel under steer control on high mu, default = 600 ] */
	/* int16_t     	S16_UND_2WH_H_LIMIT_TC_CUR_MM         */		     550,	/* Comment [ TC current upper limit during 2wheel under steer control on med mu, default = 550 ] */
	/* int16_t     	S16_UND_2WH_L_LIMIT_TC_CUR_MM         */		     150,	/* Comment [ TC current lower limit during 2wheel under steer control on med mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_F_MM_1                */		     -50,	/* Comment [ 1st reference USC_F for TC current during 2wheel under steer control on med mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_MM_2                */		       0,	/* Comment [ 2nd reference USC_F for TC current during 2wheel under steer control on med mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_MM_3                */		     100,	/* Comment [ 3rd reference USC_F for TC current during 2wheel under steer control on med mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_MM_4                */		     200,	/* Comment [ 4th reference USC_F for TC current during 2wheel under steer control on med mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_MM_5                */		     400,	/* Comment [ 5th reference USC_F for TC current during 2wheel under steer control on med mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_MM_1           */		     150,	/* Comment [ TC current for 1st reference USC_F during 2wheel under steer control on med mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_MM_2           */		     200,	/* Comment [ TC current for 2nd reference USC_F during 2wheel under steer control on med mu, default = 200 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_MM_3           */		     350,	/* Comment [ TC current for 3rd reference USC_F during 2wheel under steer control on med mu, default = 350 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_MM_4           */		     380,	/* Comment [ TC current for 4th reference USC_F during 2wheel under steer control on med mu, default = 380 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_MM_5           */		     450,	/* Comment [ TC current for 5th reference USC_F during 2wheel under steer control on med mu, default = 450 ] */
	/* int16_t     	S16_UND_2WH_H_LIMIT_TC_CUR_LM         */		     550,	/* Comment [ TC current upper limit during 2wheel under steer control on low mu, default = 550 ] */
	/* int16_t     	S16_UND_2WH_L_LIMIT_TC_CUR_LM         */		     150,	/* Comment [ TC current lower limit during 2wheel under steer control on low mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_F_LM_1                */		     -50,	/* Comment [ 1st reference USC_F for TC current during 2wheel under steer control on low mu, default = 50 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_LM_2                */		       0,	/* Comment [ 2nd reference USC_F for TC current during 2wheel under steer control on low mu, default = 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_LM_3                */		     100,	/* Comment [ 3rd reference USC_F for TC current during 2wheel under steer control on low mu, default = -100 ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_LM_4                */		     200,	/* Comment [ 4th reference USC_F for TC current during 2wheel under steer control on low mu, default = -200 ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_F_LM_5                */		     400,	/* Comment [ 5th reference USC_F for TC current during 2wheel under steer control on low mu, default = -400 ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_LM_1           */		     150,	/* Comment [ TC current for 1st reference USC_F during 2wheel under steer control on low mu, default = 150 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_LM_2           */		     200,	/* Comment [ TC current for 2nd reference USC_F during 2wheel under steer control on low mu, default = 200 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_LM_3           */		     350,	/* Comment [ TC current for 3rd reference USC_F during 2wheel under steer control on low mu, default = 350 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_LM_4           */		     380,	/* Comment [ TC current for 4th reference USC_F during 2wheel under steer control on low mu, default = 380 ] */
	/* int16_t     	S16_UND_2WH_USC_TC_CUR_LM_5           */		     450,	/* Comment [ TC current for 5th reference USC_F during 2wheel under steer control on low mu, default = 450 ] */
	/* int16_t     	S16_AHB_REAR_FADEOUT_TIME             */		       5,	/* Comment [ AHB Rear Fade out time ] */
	                                                        		        	/* ScaleVal[      50 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 327670 ] */
	/* int16_t     	S16_AHB_REAR_TAR_DEC_DEL_PRE          */		      30,	/* Comment [ Manipulating target pressure in fade out phase ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_REAR_TAR_INC_DEL_PRE          */		      30,	/* Comment [ Manipulating target pressure in initial phase ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_REF_USC_R_HM_1                */		     -50,	/* Comment [ 1st reference usc_r for target pressure on high mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_HM_2                */		       0,	/* Comment [ 2nd reference usc_r for target pressure on high mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_HM_3                */		     100,	/* Comment [ 3rd reference usc_r for target pressure on high mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_HM_4                */		     200,	/* Comment [ 4th reference usc_r for target pressure on high mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_HM_5                */		     400,	/* Comment [ 5th reference usc_r for target pressure on high mu ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_HM_1          */		      50,	/* Comment [ target pressure for 1st reference usc_r on high_mu ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_HM_2          */		      70,	/* Comment [ target pressure for 2nd reference usc_r on high_mu ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_HM_3          */		     100,	/* Comment [ target pressure for 3rd reference usc_r on high_mu ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_HM_4          */		     120,	/* Comment [ target pressure for 4th reference usc_r on high_mu ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_HM_5          */		     150,	/* Comment [ target pressure for 5th reference usc_r on high_mu ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_REF_USC_R_MM_1                */		     -50,	/* Comment [ 1st reference usc_r for target pressure on med mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_MM_2                */		       0,	/* Comment [ 2nd reference usc_r for target pressure on med mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_MM_3                */		     100,	/* Comment [ 3rd reference usc_r for target pressure on med mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_MM_4                */		     200,	/* Comment [ 4th reference usc_r for target pressure on med mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_MM_5                */		     400,	/* Comment [ 5th reference usc_r for target pressure on med mu ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_MM_1          */		      50,	/* Comment [ target pressure for 1st reference usc_r on med_mu ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_MM_2          */		      70,	/* Comment [ target pressure for 2nd reference usc_r on med_mu ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_MM_3          */		     100,	/* Comment [ target pressure for 3rd reference usc_r on med_mu ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_MM_4          */		     120,	/* Comment [ target pressure for 4th reference usc_r on med_mu ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_MM_5          */		     150,	/* Comment [ target pressure for 5th reference usc_r on med_mu ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_REF_USC_R_LM_1                */		     -50,	/* Comment [ 1st reference usc_r for target pressure on low mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_LM_2                */		       0,	/* Comment [ 2nd reference usc_r for target pressure on low mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_LM_3                */		     100,	/* Comment [ 3rd reference usc_r for target pressure on low mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_LM_4                */		     200,	/* Comment [ 4th reference usc_r for target pressure on low mu ] */
	/* int16_t     	S16_AHB_REF_USC_R_LM_5                */		     400,	/* Comment [ 5th reference usc_r for target pressure on low mu ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_LM_1          */		      50,	/* Comment [ target pressure for 1st reference usc_r on low_mu ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_LM_2          */		      70,	/* Comment [ target pressure for 2nd reference usc_r on low_mu ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_LM_3          */		     100,	/* Comment [ target pressure for 3rd reference usc_r on low_mu ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_LM_4          */		     120,	/* Comment [ target pressure for 4th reference usc_r on low_mu ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_AHB_TCMF_TARGET_PRE_LM_5          */		     150,	/* Comment [ target pressure for 5th reference usc_r on low_mu ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_ESC_BRAKE_ENABLE_SPEED            */		     120,	/* Comment [ ESC Entrance Speed, [default=15kph] ] */
	                                                        		        	/* ScaleVal[     15 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_YAW_CONTROL_HIGH_SPEED            */		    1200,	/* Comment [ Yaw Control 제어 최고 속도 [ 120kph ~ 180kph ] ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_DEC_H          */		       4,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 감소 구간)[0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_INC_H          */		       7,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_DEC_H          */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 감소 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_INC_H          */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_DEC            */		       4,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 감소 구간)[0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_INC            */		       5,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_DEC            */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 감소 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_INC            */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_DEC_L          */		       4,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 감소 구간)[0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_OUT_INC_L          */		       5,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_DEC_L          */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 감소 구간) [0~100] ] */
	/* int16_t     	S16_YC_P_GAIN_F_OS_OUT_INC_L          */		      15,	/* Comment [ 증가:목표 slip 증가 (delta yaw 증가 구간) [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_IN_H               */		       6,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_IN                 */		      10,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_F_OS_IN_L               */		      10,	/* Comment [ 증가:delta yaw dot에 대한 민감도 증가 [0~100] ] */
	/* int16_t     	S16_BETA_THRES_Hig_Spd                */		     600,	/* Comment [ Beta Control시, Beta_Ref 진입조건에 대한 속도 설정 - 고속 영역 ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BETA_THRES_Low_Spd                */		     150,	/* Comment [ Beta Control시, Beta_Ref 진입조건에 대한 속도 설정 - 저속 영역 ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BETA_THRES_Med_Spd                */		     300,	/* Comment [ Beta Control시, Beta_Ref 진입조건에 대한 속도 설정 - 중속 영역 ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BETA_THRES_LMT_H                  */		     400,	/* Comment [ Beta Control High mu, 고속영역에서 진입 조건 - Beta가 이상일 때 진입 ] */
	                                                        		        	/* ScaleVal[          4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Lspd_Hmu               */		    2500,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 저속 and High-mu ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Mspd_Hmu               */		     900,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 중속 and High-mu ] */
	                                                        		        	/* ScaleVal[          9 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_LMT                    */		     400,	/* Comment [ Beta Control Med mu, 고속영역 진입 조건 - Beta가 이상일 때 진입 ] */
	                                                        		        	/* ScaleVal[          4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Lspd_Mmu               */		    2500,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 저속 and Med-mu ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Mspd_Mmu               */		     900,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 중속 and Med-mu ] */
	                                                        		        	/* ScaleVal[          9 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_LMT_L                  */		     500,	/* Comment [ Beta Control Low mu, 고속영역 진입 조건 - Beta가 이상일 때 진입 ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Lspd_Lmu               */		    2500,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 저속 and Low-mu ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_THRES_Mspd_Lmu               */		     900,	/* Comment [ Beta Control시, Beta_Ref 조건 설정 - 중속 and Low-mu ] */
	                                                        		        	/* ScaleVal[          9 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_H              */		     100,	/* Comment [ Beta gain을 큰   del_yaw 설정값 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_L              */		    -200,	/* Comment [ Beta gain을 작은 del_yaw 설정값 ] */
	                                                        		        	/* ScaleVal[         -2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_M              */		       0,	/* Comment [ Beta gain을 중간 del_yaw 설정값 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_WEG_H          */		     100,	/* Comment [ Beta gain을 del_yaw에 dependant 가중치 - 큰 del_yaw ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_WEG_L          */		       0,	/* Comment [ Beta gain을 del_yaw에 dependant 가중치 - 작은 del_yaw ] */
	/* int16_t     	S16_BETA_GAIN_DEP_DYAW_WEG_M          */		      70,	/* Comment [ Beta gain을 del_yaw에 dependant 가중치 - 중간  del_yaw ] */
	/* int16_t     	YAW_CONTORL_P_GAIN_BETA_H             */		      10,	/* Comment [ High mu에서, Beta Control 시 P Gain ] */
	/* int16_t     	YAW_CONTORL_P_GAIN_BETA_L             */		      15,	/* Comment [ Low mu에서, Beta Control 시 P Gain ] */
	/* int16_t     	YAW_CONTORL_P_GAIN_BETA_M             */		      15,	/* Comment [ Med mu에서, Beta Control 시 P Gain ] */
	/* int16_t     	ESP_BETA_SPEED_GAIN_DEPENDANT_H_V     */		     800,	/* Comment [ Beta Control P Gain speed dependant한 고속 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_BETA_SPEED_GAIN_DEPENDANT_L_V     */		     300,	/* Comment [ Beta Control P Gain speed dependant한 중속1 ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_BETA_SPEED_GAIN_DEPENDANT_M_V     */		     500,	/* Comment [ Beta Control P Gain speed dependant한 중속2 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_BETA_SPEED_GAIN_DEPENDANT_S_V     */		      60,	/* Comment [ Beta Control P Gain speed dependant한 저속 ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_H  */		     100,	/* Comment [ Beta Control P Gain speed dep. High mu 고속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_L  */		      30,	/* Comment [ Beta Control P Gain speed dep. High mu 저속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_M  */		      50,	/* Comment [ Beta Control P Gain speed dep. High mu 중속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_H  */		     100,	/* Comment [ Beta Control P Gain speed dep. Med mu 고속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_L  */		      30,	/* Comment [ Beta Control P Gain speed dep. Med mu 저속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_M  */		      60,	/* Comment [ Beta Control P Gain speed dep. Med mu 중속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_H  */		     100,	/* Comment [ Beta Control P Gain speed dep. Low mu 고속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_L  */		      30,	/* Comment [ Beta Control P Gain speed dep. Low mu 저속  weighting ] */
	/* int16_t     	ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_M  */		      60,	/* Comment [ Beta Control P Gain speed dep. Low mu 중속  weighting ] */
	/* int16_t     	S16_BETA_M_LMT_Hspd_Hmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 고속 and High-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Lspd_Hmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 저속 and High-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Mspd_Hmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 중속 and High-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Hspd_Mmu               */		     800,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 고속 and Med-mu ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Lspd_Mmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 저속 and Med-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Mspd_Mmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 중속 and Med-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Hspd_Lmu               */		     800,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 고속 and Low-mu ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Lspd_Lmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 저속 and Low-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BETA_M_LMT_Mspd_Lmu               */		     700,	/* Comment [ Beta Control시, Beta_Moment_Limit 양 설정 - 중속 and Low-mu ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_DEC_H          */		      -7,	/* Comment [ delta yaw 변화에 대한 민감도 ( 절대치 증가:목표 slip 증가) [-50 ~ 0] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_INC_H          */		      -8,	/* Comment [ delta yaw 변화에 대한 민감도( 절대치 증가:목표 slip 감소) [-100 ~ 0] ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_OUT_H              */		       4,	/* Comment [ 증가:목표 slip 증가 [0~100] ] */
	/* char8_t     	S16_YC_P_GAIN_R_US_OUT_INC_H          */		       4,	/* Comment [ High_mu, US outside increase P-gain, [default=4] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_DEC_H               */		       0,	/* Comment [ High_mu, Extreme_Under, US outside gain, [default=-7] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_INC_H               */		       0,	/* Comment [ High_mu, Extreme_Under, US outside gain, [default=-8] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_DEC_H               */		       0,	/* Comment [ High_mu, Extreme_Under, US outside gain, [default=4] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_INC_H               */		       0,	/* Comment [ High_mu, Extreme_Under, US outside gain, [default=4] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_DEC            */		      -7,	/* Comment [ delta yaw 변화에 대한 민감도 ( 절대치 증가:목표 slip 증가) [-50 ~ 0] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_INC            */		      -8,	/* Comment [ delta yaw 변화에 대한 민감도( 절대치 증가:목표 slip 감소) [-100 ~ 0] ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_OUT                */		       4,	/* Comment [ 증가:목표 slip 증가 [0~100] ] */
	/* char8_t     	S16_YC_P_GAIN_R_US_OUT_INC            */		       4,	/* Comment [ Med_mu, US outside increase P-gain, [default=4] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_DEC_M               */		       0,	/* Comment [ Med_mu, Extreme_Under, US outside gain, [default=-7] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_INC_M               */		       0,	/* Comment [ Med_mu, Extreme_Under, US outside gain, [default=-8] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_DEC_M               */		       0,	/* Comment [ Med_mu, Extreme_Under, US outside gain, [default=4] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_INC_M               */		       0,	/* Comment [ Med_mu, Extreme_Under, US outside gain, [default=4] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_DEC_L          */		      -7,	/* Comment [ delta yaw 변화에 대한 민감도 ( 절대치 증가:목표 slip 증가) [-50 ~ 0] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_OUT_INC_L          */		      -8,	/* Comment [ delta yaw 변화에 대한 민감도( 절대치 증가:목표 slip 감소) [-100 ~ 0] ] */
	/* char8_t     	S16_YC_P_GAIN_R_US_OUT_INC_L          */		       5,	/* Comment [ Low_mu, US outside increase P-gain, [default=5] ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_OUT_L              */		       5,	/* Comment [ 증가:목표 slip 증가 [0~100] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_DEC_L               */		       0,	/* Comment [ Low_mu, Extreme_Under, US outside gain, [default=-7] ] */
	/* char8_t     	S16_EU_REAR_D_OUT_INC_L               */		       0,	/* Comment [ Low_mu, Extreme_Under, US outside gain, [default=-8] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_DEC_L               */		       0,	/* Comment [ Low_mu, Extreme_Under, US outside gain, [default=5] ] */
	/* char8_t     	S16_EU_REAR_P_OUT_INC_L               */		       0,	/* Comment [ Low_mu, Extreme_Under, US outside gain, [default=5] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN                 */		       0,	/* Comment [ Old Concept => 증가:delta yaw 변화에 대한 민감도 [0~100] ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_INC             */		     -10,	/* Comment [ Med mu, Understeer Inside Increase D Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_INC_H           */		     -10,	/* Comment [ High mu, Understeer Inside Increase D Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_INC_L           */		     -10,	/* Comment [ Low mu, Understeer Inside Increase D Gain ] */
	/* int16_t     	MGH60_YAWC_OFFSET_UNDER_CTRL          */		       1,	/* Comment [ Under Control Under 제어 여부 조건 ] */
	/* uchar8_t    	S16_DEC_OS_M_WEG_ON_US_DELM           */		     100,	/* Comment [ At Under_control_under, Weight of OS_M to decrease US_M, Default = 100 ] */
	/* int16_t     	S16_UNDER_CTRL_ENTER_MQ               */		     600,	/* Comment [ Under Control Under 시 계산된 del_M이 Tuning 값 이상일 때 Under Control Under 진입 ] */
	                                                        		        	/* ScaleVal[        6 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_UNDER_CTRL_ENTER_VEL              */		     250,	/* Comment [ Under Control 제어 진입 차량 속도. 해당값 이상에서 제어 진입함. ] */
	                                                        		        	/* ScaleVal[     25 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_UNDER_CTRL_EXIT_MQ                */		     500,	/* Comment [ Under Control Under 시 계산된 del_M이 Tuning 값 이하일 때 Under Control Under 해제 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_UNDER_CTRL_EXIT_VEL               */		     200,	/* Comment [ Under Control 제어 해제 차량 속도. 해당값 이하에서 제어 해제함. ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 300 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_HMU_HSP         */		     400,	/* Comment [ High mu, High Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_HMU_LSP         */		     400,	/* Comment [ High mu, Low Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_HMU_MSP         */		     400,	/* Comment [ High mu, Med Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_HMU_HSP         */		     400,	/* Comment [ High mu, High Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_HMU_LSP         */		     400,	/* Comment [ High mu, Low Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_HMU_MSP         */		     400,	/* Comment [ High mu, Med Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  4 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_MMU_HSP         */		     300,	/* Comment [ Med mu, High Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_MMU_LSP         */		     300,	/* Comment [ Med mu, Low Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_MMU_MSP         */		     300,	/* Comment [ Med mu, Med Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_MMU_HSP         */		     300,	/* Comment [ Med mu, High Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_MMU_LSP         */		     300,	/* Comment [ Med mu, Low Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_MMU_MSP         */		     300,	/* Comment [ Med mu, Med Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_LMU_HSP         */		     300,	/* Comment [ Low mu, High Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_LMU_LSP         */		     300,	/* Comment [ Low mu, Low Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_ENT_DEL_YAW_LMU_MSP         */		     300,	/* Comment [ Low mu, Med Speed시 Under Control Under 진입 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_LMU_HSP         */		     300,	/* Comment [ Low mu, High Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_LMU_LSP         */		     300,	/* Comment [ Low mu, Low Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_EXT_DEL_YAW_LMU_MSP         */		     300,	/* Comment [ Low mu, Med Speed시 Under Control Under 해제 del_yaw 조건. 키우면, Under 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 15 ] */
	/* int16_t     	S16_UNDER_MQ_WEG_MED_ALPHA_R          */		      20,	/* Comment [ Med_Alpha_r 값에 적용되는 Del_M의 Med_Weigthing 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_SMALL_ALPHA_R_HMU_HSP             */		    1000,	/* Comment [ High mu, High speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_HMU_LSP             */		    1500,	/* Comment [ High mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_HMU_MSP             */		    1100,	/* Comment [ High mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     11 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_HMU_HSP               */		     500,	/* Comment [ High mu, High speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_HMU_LSP               */		     500,	/* Comment [ High mu, Low speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_HMU_MSP               */		     500,	/* Comment [ High mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_HMU_HSP               */		     500,	/* Comment [ High mu, High speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_HMU_LSP               */		     500,	/* Comment [ High mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_HMU_MSP               */		     500,	/* Comment [ High mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_MMU_HSP             */		    1100,	/* Comment [ Med mu, High speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     11 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_MMU_LSP             */		    1600,	/* Comment [ Med mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     16 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_MMU_MSP             */		    1200,	/* Comment [ Med mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     12 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_MMU_HSP               */		    1500,	/* Comment [ Med mu, High speed에서, Under Control Under에서 계산되 del_M 값이 S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_MMU_LSP               */		    1500,	/* Comment [ Med mu, Low speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_MMU_MSP               */		    1500,	/* Comment [ Med mu, Med speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_MMU_HSP               */		    1500,	/* Comment [ Med mu, High speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_MMU_LSP               */		    1500,	/* Comment [ Med mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_MMU_MSP               */		    1500,	/* Comment [ Med mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_LMU_HSP             */		    1200,	/* Comment [ Low mu, High speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     12 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_LMU_LSP             */		    1700,	/* Comment [ Low mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     17 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_SMALL_ALPHA_R_LMU_MSP             */		    1300,	/* Comment [ Low mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 100% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     13 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_LMU_HSP               */		    1500,	/* Comment [ Low mu, High speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_LMU_LSP               */		    1500,	/* Comment [ Low mu, Low speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_MED_ALPHA_R_LMU_MSP               */		    1500,	/* Comment [ Low mu, Med speed에서, Under Control Under에서 계산되 del_M 값이  S16_UNDER_MQ_WEG_MED_ALPHA_R(20%) 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_LMU_HSP               */		    1500,	/* Comment [ Low mu, High speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_LMU_LSP               */		    1500,	/* Comment [ Low mu, Low speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_BIG_ALPHA_R_LMU_MSP               */		    1500,	/* Comment [ Low mu, Med speed에서, Under Control Under에서 계산되 del_M 값이 0% 사용되는 alpha_r 값. 값을 키우면, 제어 진입 범위 넓어짐. ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 20 ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_HMU_HSP         */		     100,	/* Comment [ High mu, High Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_HMU_LSP         */		     100,	/* Comment [ High mu, Low Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_HMU_MSP         */		     100,	/* Comment [ High mu, Med Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_HMU_HSP              */		    1200,	/* Comment [ High mu, High Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_HMU_LSP              */		    1200,	/* Comment [ High mu, Low Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_HMU_MSP              */		    1200,	/* Comment [ High mu, Med Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_MMU_HSP         */		      70,	/* Comment [ Med mu, High Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_MMU_LSP         */		      70,	/* Comment [ Med mu, Low Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_MMU_MSP         */		      70,	/* Comment [ Med mu, Med Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_MMU_HSP              */		    1200,	/* Comment [ Med mu, High Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_MMU_LSP              */		    1200,	/* Comment [ Med mu, Low Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_MMU_MSP              */		    1200,	/* Comment [ Med mu, Med Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_LMU_HSP         */		     100,	/* Comment [ Low mu, High Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_LMU_LSP         */		     100,	/* Comment [ Low mu, Low Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_CTRL_PD_WEG_LMU_MSP         */		     100,	/* Comment [ Low mu, Med Speed 시 PD Gain의 Weighting. Under Control Under 의 PD Gain은 True Under Control의 Outside PD Gain임. 값을 낮추면, 제어량 감소. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_LMU_HSP              */		    1200,	/* Comment [ Low mu, High Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_LMU_LSP              */		    1200,	/* Comment [ Low mu, Low Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_UNDER_MQ_LMT_LMU_MSP              */		    1200,	/* Comment [ Low mu, Med Speed에 Under Control Under 시 Del_M의 MAX 값. 값을 키우면, 제어량 많아짐. ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_DEC_H       */		      -5,	/* Comment [ Under Control Decrese D Gain - Yaw Limit 시 Threshold High mu P Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_INC_H       */		      -6,	/* Comment [ Under Control Increse D Gain - Yaw Limit 시 Threshold High mu P Gain ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_IN_LMT_H           */		       8,	/* Comment [ Under Control P Gain - Yaw Limit 시 Threshold High mu P Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_DEC         */		      -3,	/* Comment [ Under Control Decrese D Gain - Yaw Limit 시 Threshold Med mu P Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_INC         */		     -20,	/* Comment [ Under Control Increse D Gain - Yaw Limit 시 Threshold Med mu P Gain ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_IN_LMT             */		       7,	/* Comment [ Under Control P Gain - Yaw Limit 시 Threshold Med mu P Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_DEC_L       */		      -5,	/* Comment [ Under Control Decrese D Gain - Yaw Limit 시 Threshold Low mu P Gain ] */
	/* int16_t     	S16_YC_D_GAIN_R_US_IN_LMT_INC_L       */		     -20,	/* Comment [ Under Control Increse D Gain - Yaw Limit 시 Threshold Low mu P Gain ] */
	/* int16_t     	S16_YC_P_GAIN_R_US_IN_LMT_L           */		       7,	/* Comment [ Under Control P Gain - Yaw Limit 시 Threshold Low mu P Gain ] */
	/* int16_t     	S16_YC_SPEED_DEP_HIG2_V               */		    1300,	/* Comment [ Yaw Control Gain 가중치 설정속도(고속2) [70~100] ] */
	                                                        		        	/* ScaleVal[        130 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_HIG_V                */		    1100,	/* Comment [ Yaw Control Gain 가중치 설정속도(고속1) [60~90] ] */
	                                                        		        	/* ScaleVal[        110 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_LOW_V                */		     300,	/* Comment [ Yaw Control Gain 가중치 설정속도(저속) [10~20] ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_MED1_V               */		     700,	/* Comment [ Yaw Control Gain 가중치(100%)  설정속도(중저속) [50~80] ] */
	                                                        		        	/* ScaleVal[         70 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_MED2_V               */		     900,	/* Comment [ Yaw Control Gain 가중치(100%) 설정속도(중고속) [50~80] ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_VER_HIG_V            */		    1800,	/* Comment [ Yaw Control Gain 가중치 설정속도(고속3) [100~140] ] */
	                                                        		        	/* ScaleVal[        180 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_VLOW_V               */		     100,	/* Comment [ Yaw Control Gain 가중치 설정속도(극저속) ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_SPEED_DEP_HIG_H_GAIN           */		     120,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SPEED_DEP_LOW_H_GAIN           */		      70,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SPEED_DEP_MED_H_GAIN           */		     100,	/* Comment [ Med_Speed, Yaw Control Gain weight [default=100] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VER_HIG_H_GAIN       */		     165,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VLOW_H_GAIN          */		      10,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SPEED_DEP_HIG_M_GAIN           */		     110,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SPEED_DEP_LOW_M_GAIN           */		      70,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SPEED_DEP_MED_M_GAIN           */		     100,	/* Comment [ Med_Speed, Yaw Control Gain weight [default=100] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VER_HIG_M_GAIN       */		     130,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VLOW_M_GAIN          */		      30,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SPEED_DEP_HIG_L_GAIN           */		     100,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SPEED_DEP_LOW_L_GAIN           */		      80,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SPEED_DEP_MED_L_GAIN           */		     100,	/* Comment [ Med_Speed, Yaw Control Gain weight [default=100] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VER_HIG_L_GAIN       */		     100,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* int16_t     	S16_YC_SPEED_DEP_VLOW_L_GAIN          */		      60,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SP_DEP_HIG_ABS_H_GAIN          */		     120,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SP_DEP_LOW_ABS_H_GAIN          */		      80,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SP_DEP_MED_ABS_H_GAIN          */		     100,	/* Comment [ (중속~중속) 시 Yaw Control Gain 가중치: 기준 속도임 [90~110] ] */
	/* int16_t     	S16_YC_SP_DEP_VLOW_ABS_H_GAIN         */		      30,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SP_DEP_V_HIG_ABS_H_GAIN        */		     150,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* int16_t     	S16_YC_SP_DEP_HIG_ABS_GAIN            */		     110,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SP_DEP_LOW_ABS_GAIN            */		      70,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SP_DEP_MED_ABS_GAIN            */		     100,	/* Comment [ (중속~중속) 시 Yaw Control Gain 가중치: 기준 속도임 [90~110] ] */
	/* int16_t     	S16_YC_SP_DEP_VLOW_ABS_GAIN           */		      30,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SP_DEP_V_HIG_ABS_GAIN          */		     130,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* int16_t     	S16_YC_SP_DEP_HIG_ABS_L_GAIN          */		     100,	/* Comment [ (고속2~고속1) 시 Yaw Control Gain 가중치 [100~120] ] */
	/* int16_t     	S16_YC_SP_DEP_LOW_ABS_L_GAIN          */		      80,	/* Comment [ 저속 시 Yaw Control Gain 가중치 [40~60] ] */
	/* int16_t     	S16_YC_SP_DEP_MED_ABS_L_GAIN          */		      90,	/* Comment [ (중속~중속) 시 Yaw Control Gain 가중치: 기준 속도임 [90~110] ] */
	/* int16_t     	S16_YC_SP_DEP_VLOW_ABS_L_GAIN         */		      50,	/* Comment [ 극저속 시 Yaw Control Gain 가중치 [20~30] ] */
	/* int16_t     	S16_YC_SP_DEP_V_HIG_ABS_L_GAIN        */		     100,	/* Comment [ 고속3 시 Yaw Control Gain 가중치 [100~180] ] */
	/* uchar8_t    	S16_YC_SP_DEP_HIG_ABS_SPLIT_GAIN      */		      20,	/* Comment [ At ABS and Split_road, Speed dependent moment weight, [Default = 100] ] */
	/* uchar8_t    	S16_YC_SP_DEP_LOW_ABS_SPLIT_GAIN      */		      20,	/* Comment [ At ABS and Split_road, Speed dependent moment weight, [Default = 40] ] */
	/* uchar8_t    	S16_YC_SP_DEP_MED_ABS_SPLIT_GAIN      */		      20,	/* Comment [ At ABS and Split_road, Speed dependent moment weight, [Default = 80] ] */
	/* uchar8_t    	S16_YC_SP_DEP_VLOW_ABS_SPLIT_GAIN     */		      10,	/* Comment [ At ABS and Split_road, Speed dependent moment weight, [Default = 30] ] */
	/* uchar8_t    	S16_YC_SP_DEP_V_HIG_ABS_SPLIT_GAIN    */		      20,	/* Comment [ At ABS and Split_road, Speed dependent moment weight, [Default = 130] ] */
	/* int16_t     	S16_REAR_HMU_SPD_H                    */		    1000,	/* Comment [ Under control P Gain Speed dep. High mu High Speed값 ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_HMU_SPD_L                    */		     400,	/* Comment [ Under control P Gain Speed dep. High mu Low Speed ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_HMU_SPD_M1                   */		     600,	/* Comment [ Under control P Gain Speed dep. High mu Med_1 Speed ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_HMU_SPD_M2                   */		     800,	/* Comment [ Under control P Gain Speed dep. High mu Med_2 Speed ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_MMU_SPD_H                    */		     800,	/* Comment [ Under control P Gain Speed dep. Med mu High Speed값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_MMU_SPD_L                    */		     200,	/* Comment [ Under control P Gain Speed dep. Med mu Low Speed ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_MMU_SPD_M1                   */		     400,	/* Comment [ Under control P Gain Speed dep. Med mu Med_1 Speed ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_MMU_SPD_M2                   */		     600,	/* Comment [ Under control P Gain Speed dep. Med mu Med_2 Speed ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_LMU_SPD_H                    */		     800,	/* Comment [ Under control P Gain Speed dep. Low mu High Speed값 ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_LMU_SPD_L                    */		     150,	/* Comment [ Under control P Gain Speed dep. Low mu Low Speed ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_LMU_SPD_M1                   */		     400,	/* Comment [ Under control P Gain Speed dep. Low mu Med_1 Speed ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_REAR_LMU_SPD_M2                   */		     600,	/* Comment [ Under control P Gain Speed dep. Low mu Med_2 Speed ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HMU_REAR_HIGH_SPD_GAIN_WEG        */		      50,	/* Comment [ Under control P Gain Speed dep. High mu High Speed시 Weighting ] */
	/* int16_t     	S16_HMU_REAR_LOW_SPD_GAIN_WEG         */		      20,	/* Comment [ Under control P Gain Speed dep. High mu Low Speed시 Weighting ] */
	/* int16_t     	S16_HMU_REAR_MED_SPD_GAIN_WEG         */		      30,	/* Comment [ Under control P Gain Speed dep. High mu Med Speed시 Weighting ] */
	/* int16_t     	S16_MMU_REAR_HIGH_SPD_GAIN_WEG        */		      10,	/* Comment [ Under control P Gain Speed dep. Med mu High Speed시 Weighting ] */
	/* int16_t     	S16_MMU_REAR_LOW_SPD_GAIN_WEG         */		      10,	/* Comment [ Under control P Gain Speed dep. Med mu Low Speed시 Weighting ] */
	/* int16_t     	S16_MMU_REAR_MED_SPD_GAIN_WEG         */		      10,	/* Comment [ Under control P Gain Speed dep. Med mu Med Speed시 Weighting ] */
	/* int16_t     	S16_LMU_REAR_HIGH_SPD_GAIN_WEG        */		      60,	/* Comment [ Under control P Gain Speed dep. Low mu High Speed시 Weighting ] */
	/* int16_t     	S16_LMU_REAR_LOW_SPD_GAIN_WEG         */		     100,	/* Comment [ Under control P Gain Speed dep. Low mu Low Speed시 Weighting ] */
	/* int16_t     	S16_LMU_REAR_MED_SPD_GAIN_WEG         */		      80,	/* Comment [ Under control P Gain Speed dep. Low mu Med Speed시 Weighting ] */
	/* int16_t     	S16_HMU_REAR_HIG_SPD_GAIN_ABS         */		     100,	/* Comment [ High mu, ABS 협조 제어 시 Rear PD Gain의 High Speed 가중치 ] */
	/* int16_t     	S16_HMU_REAR_LOW_SPD_GAIN_ABS         */		      30,	/* Comment [ High mu, ABS 협조 제어 시 Rear PD Gain의 Low Speed 가중치 ] */
	/* int16_t     	S16_HMU_REAR_MED_SPD_GAIN_ABS         */		      50,	/* Comment [ High mu, ABS 협조 제어 시 Rear PD Gain의 Med Speed 가중치 ] */
	/* int16_t     	S16_MMU_REAR_HIG_SPD_GAIN_ABS         */		     100,	/* Comment [ Med mu, ABS 협조 제어 시 Rear PD Gain의 High Speed 가중치 ] */
	/* int16_t     	S16_MMU_REAR_LOW_SPD_GAIN_ABS         */		      30,	/* Comment [ Med mu, ABS 협조 제어 시 Rear PD Gain의 Low Speed 가중치 ] */
	/* int16_t     	S16_MMU_REAR_MED_SPD_GAIN_ABS         */		      50,	/* Comment [ Med mu, ABS 협조 제어 시 Rear PD Gain의 Med Speed 가중치 ] */
	/* int16_t     	S16_LMU_REAR_HIG_SPD_GAIN_ABS         */		     100,	/* Comment [ Low mu, ABS 협조 제어 시 Rear PD Gain의 High Speed 가중치 ] */
	/* int16_t     	S16_LMU_REAR_LOW_SPD_GAIN_ABS         */		      30,	/* Comment [ Low mu, ABS 협조 제어 시 Rear PD Gain의 Low Speed 가중치 ] */
	/* int16_t     	S16_LMU_REAR_MED_SPD_GAIN_ABS         */		      50,	/* Comment [ Low mu, ABS 협조 제어 시 Rear PD Gain의 Med Speed 가중치 ] */
	/* int16_t     	S16_VHMU_REAR_HIGH_SPD_GAIN_ABS       */		      10,	/* Comment [ Very High mu, ABS 협조 제어 시 Rear PD Gain의 High Speed 가중치 ] */
	/* int16_t     	S16_VHMU_REAR_LOW_SPD_GAIN_ABS        */		      10,	/* Comment [ Very High mu, ABS 협조 제어 시 Rear PD Gain의 Low Speed 가중치 ] */
	/* int16_t     	S16_VHMU_REAR_MED_SPD_GAIN_ABS        */		      10,	/* Comment [ Very High mu, ABS 협조 제어 시 Rear PD Gain의 Med Speed 가중치 ] */
	/* char8_t     	S16_DELM_HM_SPD_H_GAIN                */		     100,	/* Comment [ High_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_HM_SPD_L_GAIN                */		     100,	/* Comment [ High_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_HM_SPD_M_GAIN                */		     100,	/* Comment [ High_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_LM_SPD_H_GAIN                */		     100,	/* Comment [ Low_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_LM_SPD_L_GAIN                */		      30,	/* Comment [ Low_mu, OS_DelM_Weight_in_Understeer state [default=30] ] */
	/* char8_t     	S16_DELM_LM_SPD_M_GAIN                */		     100,	/* Comment [ Low_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_MM_SPD_H_GAIN                */		     100,	/* Comment [ Med_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* char8_t     	S16_DELM_MM_SPD_L_GAIN                */		      30,	/* Comment [ Med_mu, OS_DelM_Weight_in_Understeer state [default=30] ] */
	/* char8_t     	S16_DELM_MM_SPD_M_GAIN                */		     100,	/* Comment [ Med_mu, OS_DelM_Weight_in_Understeer state [default=100] ] */
	/* int16_t     	S16_VHMU_REAR_HIGH_SPD_GAIN_WEG       */		      60,	/* Comment [ Under control Gain Speed dep. Very High mu High Speed시 Weighting ] */
	/* int16_t     	S16_VHMU_REAR_LOW_SPD_GAIN_WEG        */		      10,	/* Comment [ Under control Gain Speed dep. Very High mu Low Speed시 Weighting ] */
	/* int16_t     	S16_VHMU_REAR_MED_SPD_GAIN_WEG        */		      80,	/* Comment [ Under control Gain Speed dep. Very High mu Med Speed시 Weighting ] */
	/* int16_t     	S16_TWO_WHEEL_SPEED_H_V               */		    1200,	/* Comment [ Oversteer reference high speed ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TWO_WHEEL_SPEED_L_V               */		     150,	/* Comment [ Oversteer reference low speed ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TWO_WHEEL_SPEED_M_V               */		     600,	/* Comment [ Oversteer reference med speed ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_2WH_HMU_REAR_SLIP_MAX             */		    1000,	/* Comment [ High mu에서 2 Wheel 제어 시 Rear del_target_slip의 Max값 ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_HM_1               */		     -50,	/* Comment [ 1st reference USC_R for TC current at high mu ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_HM_2               */		       0,	/* Comment [ 2nd reference USC_R for TC current at high mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_HM_3               */		     100,	/* Comment [ 3rd reference USC_R for TC current at high mu ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_HM_4               */		     200,	/* Comment [ 4th reference USC_R for TC current at high mu ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_HM_5               */		     400,	/* Comment [ 5th reference USC_R for TC current at high mu ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_HM_1          */		     350,	/* Comment [ TC current for 1st reference USC_R at high mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_HM_2          */		     400,	/* Comment [ TC current for 2nd reference USC_R at high mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_HM_3          */		     450,	/* Comment [ TC current for 3rd reference USC_R at high mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_HM_4          */		     480,	/* Comment [ TC current for 4th reference USC_R at high mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_HM_5          */		     550,	/* Comment [ TC current for 5th reference USC_R at high mu ] */
	/* int16_t     	S16_TWO_HMU_REAR_TAR_SLIP_WEG         */		       0,	/* Comment [ High mu에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_HMU_HSP  */		       0,	/* Comment [ High mu, High speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_HMU_LSP  */		       0,	/* Comment [ High mu, Low speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_HMU_MSP  */		       0,	/* Comment [ High mu, Med speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_2WH_MMU_REAR_SLIP_MAX             */		     500,	/* Comment [ Med mu에서 2 Wheel 제어 시 Rear del_target_slip의 Max값 ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_MM_1               */		     -50,	/* Comment [ 1st reference USC_R for TC current at medmu ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_MM_2               */		       0,	/* Comment [ 2nd reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_MM_3               */		     100,	/* Comment [ 3rd reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_MM_4               */		     200,	/* Comment [ 4th reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_MM_5               */		     400,	/* Comment [ 5th reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_MM_1          */		     350,	/* Comment [ TC current for 1st reference USC_R at med mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_MM_2          */		     400,	/* Comment [ TC current for 2nd reference USC_R at med mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_MM_3          */		     450,	/* Comment [ TC current for 3rd reference USC_R at med mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_MM_4          */		     480,	/* Comment [ TC current for 4th reference USC_R at med mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_MM_5          */		     550,	/* Comment [ TC current for 5th reference USC_R at med mu ] */
	/* int16_t     	S16_TWO_MMU_REAR_TAR_SLIP_WEG         */		       0,	/* Comment [ Med mu에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_MMU_HSP  */		       0,	/* Comment [ Med mu, High speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_MMU_LSP  */		       0,	/* Comment [ Med mu, Low speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_MMU_MSP  */		       0,	/* Comment [ Med mu, Med speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_2WH_LMU_REAR_SLIP_MAX             */		    1000,	/* Comment [ Low mu에서 2 Wheel 제어 시 Rear del_target_slip의 Max값 ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_LM_1               */		     -50,	/* Comment [ 1st reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_LM_2               */		       0,	/* Comment [ 2nd reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_LM_3               */		     100,	/* Comment [ 3rd reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -100 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_LM_4               */		     200,	/* Comment [ 4th reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -200 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_F_LM_5               */		     400,	/* Comment [ 5th reference USC_R for TC current at med mu ] */
	                                                        		        	/* ScaleVal[       -400 ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 32768 <= X <= -32767 ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_LM_1          */		     150,	/* Comment [ TC current for 1st reference USC_R at low mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_LM_2          */		     200,	/* Comment [ TC current for 2nd reference USC_R at low mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_LM_3          */		     350,	/* Comment [ TC current for 3rd reference USC_R at low mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_LM_4          */		     380,	/* Comment [ TC current for 4th reference USC_R at low mu ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_LM_5          */		     450,	/* Comment [ TC current for 5th reference USC_R at low mu ] */
	/* int16_t     	S16_TWO_LMU_REAR_TAR_SLIP_WEG         */		      30,	/* Comment [ Low mu에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_LMU_HSP  */		     100,	/* Comment [ Low mu, High speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_LMU_LSP  */		     100,	/* Comment [ Low mu, Low speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_TWO_REAR_TARGET_SLIP_WEG_LMU_MSP  */		     100,	/* Comment [ Low mu, Med speed에서 2 Wheel 제어 시 Rear del target slip 가중치 ] */
	/* int16_t     	S16_OVER_2WH_FADE_OUT_DEC             */		      50,	/* Comment [ TC current fade out rate at the end of Oversteer 2wheel control ] */
	/* int16_t     	S16_OVER_2WH_FRT_FADE_OUT_HM          */		       5,	/* Comment [ FadeOut scan time for oversteer 2wheel in high mu ] */
	/* int16_t     	S16_OVER_2WH_FRT_FADE_OUT_LM          */		       5,	/* Comment [ FadeOut scan time for oversteer 2wheel in low mu ] */
	/* int16_t     	S16_OVER_2WH_FRT_FADE_OUT_MM          */		       5,	/* Comment [ FadeOut scan time for oversteer 2wheel in med mu ] */
	/* int16_t     	S16_OVER_2WH_USC_INITIAL_FULL_TIME    */		       7,	/* Comment [ Initial full in time of Oversteer 2wheel control ] */
	/* int16_t     	S16_OVER_2WH_USC_TC_CUR_FACTOR        */		      70,	/* Comment [ TC current weighting factor during transient time in oversteer 2wheel control ] */
	/* int16_t     	S16_OVER_2WH_USC_TRANSIENT_TIME       */		       7,	/* Comment [ Transient time after initial full time to control TC current for oversteer 2wheel ] */
	/* int16_t     	S16_OVER_2WH_H_LIMIT_TC_CUR_HM        */		     550,	/* Comment [ TC current upper limit at high mu ] */
	/* int16_t     	S16_OVER_2WH_H_LIMIT_TC_CUR_LM        */		     550,	/* Comment [ TC current upper limit at low mu ] */
	/* int16_t     	S16_OVER_2WH_H_LIMIT_TC_CUR_MM        */		     550,	/* Comment [ TC current upper limit at med mu ] */
	/* int16_t     	S16_OVER_2WH_L_LIMIT_TC_CUR_HM        */		     150,	/* Comment [ TC current lower limit at high mu ] */
	/* int16_t     	S16_OVER_2WH_L_LIMIT_TC_CUR_LM        */		     150,	/* Comment [ TC current lower limit at low mu ] */
	/* int16_t     	S16_OVER_2WH_L_LIMIT_TC_CUR_MM        */		     150,	/* Comment [ TC current lower limit at med mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_ENT_H                */		     150,	/* Comment [ delta moment to make oversteer 2wheel enter in high mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_ENT_L                */		     150,	/* Comment [ delta moment to make oversteer 2wheel enter in low mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_ENT_M                */		     150,	/* Comment [ delta moment to make oversteer 2wheel enter in med mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_ENT_ROP_H            */		     255,	/* Comment [ delta moment to make oversteer 2wheel enter in ROP ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_EXIT_H               */		      30,	/* Comment [ delta moment to make oversteer 2wheel exit in high mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_EXIT_L               */		      30,	/* Comment [ delta moment to make oversteer 2wheel exit in low mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_EXIT_M               */		      30,	/* Comment [ delta moment to make oversteer 2wheel exit in med mu ] */
	/* uchar8_t    	U8_OS_2_WHEEL_QM_EXIT_ROP_H           */		     100,	/* Comment [ delta moment to make oversteer 2wheel exit in ROP ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_HMU_HSP        */		     100,	/* Comment [ Motor target voltage while Oversteer 2wheel control at high-mu/high speed ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_HMU_LSP        */		     100,	/* Comment [ Motor target voltage while Oversteer 2wheel control at high-mu/low speed ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_HMU_MSP        */		     100,	/* Comment [ Motor target voltage while Oversteer 2wheel control at high-mu/med speed ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_LMU_HSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at low-mu/high speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_LMU_LSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at low-mu/low speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_LMU_MSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at low-mu/med speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_MMU_HSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at med-mu/high speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_MMU_LSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at med-mu/low speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_OVER_2WH_TARGET_VOL_MMU_MSP        */		      80,	/* Comment [ Motor target voltage while Oversteer 2wheel control at med-mu/med speed ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_D_GAIN_HIGH_SPD                   */		    1000,
	/* int16_t     	S16_D_GAIN_LOW_SPD                    */		     500,
	/* int16_t     	S16_D_GAIN_MED_SPD                    */		     800,
	/* int16_t     	S16_D_GAIN_V_LOW_SPD                  */		     300,
	/* int16_t     	S16_H_MU_HIGH_D_WEG                   */		     130,
	/* int16_t     	S16_H_MU_LOW_D_WEG                    */		      60,
	/* int16_t     	S16_H_MU_MED_D_WEG                    */		     100,
	/* int16_t     	S16_H_MU_V_LOW_D_WEG                  */		      40,
	/* int16_t     	S16_M_MU_HIGH_D_WEG                   */		     115,
	/* int16_t     	S16_M_MU_LOW_D_WEG                    */		      70,
	/* int16_t     	S16_M_MU_MED_D_WEG                    */		     100,
	/* int16_t     	S16_M_MU_V_LOW_D_WEG                  */		      50,
	/* int16_t     	S16_L_MU_HIGH_D_WEG                   */		     120,
	/* int16_t     	S16_L_MU_LOW_D_WEG                    */		      90,
	/* int16_t     	S16_L_MU_MED_D_WEG                    */		     100,
	/* int16_t     	S16_L_MU_V_LOW_D_WEG                  */		      70,
	/* int16_t     	S16_ABS_HIGH_D_WEG                    */		     100,
	/* int16_t     	S16_ABS_LOW_D_WEG                     */		      80,
	/* int16_t     	S16_ABS_MED_D_WEG                     */		     100,
	/* int16_t     	S16_ABS_V_LOW_D_WEG                   */		      50,
	/* int16_t     	S16_YA_F_OS_HM_TH1                    */		     150,	/* Comment [ Yaw-acceleration threshold_1, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_HM_TH1_HSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at High-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_HM_TH1_LSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at Low-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_HM_TH2                    */		     250,	/* Comment [ Yaw-acceleration threshold_2, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_HM_TH2_HSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at High_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_HM_TH2_LSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at Low_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH1                    */		     150,	/* Comment [ Yaw-acceleration threshold_1, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH1_HSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at High-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH1_LSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at Low-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH2                    */		     250,	/* Comment [ Yaw-acceleration threshold_2, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH2_HSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at High_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_LM_TH2_LSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at Low_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH1                    */		     150,	/* Comment [ Yaw-acceleration threshold_1, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH1_HSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at High-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH1_LSP                */		     150,	/* Comment [ Yaw-acceleration threshold_1 at Low-Speed, 100% weight under this value [default=150] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH2                    */		     250,	/* Comment [ Yaw-acceleration threshold_2, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH2_HSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at High_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_MM_TH2_LSP                */		     250,	/* Comment [ Yaw-acceleration threshold_2 at Low_speed, TP weight over this value [default=250] ] */
	/* int16_t     	S16_YA_F_OS_WEG_LM_HIG                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_LM_LOW                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_LM_MED                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_LM_VHIG               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_LM_VLOW               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_MM_HIG                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_MM_LOW                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_MM_MED                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_MM_VHIG               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_MM_VLOW               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_HM_HIG                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_HM_LOW                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_HM_MED                */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_HM_VHIG               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* int16_t     	S16_YA_F_OS_WEG_HM_VLOW               */		     100,	/* Comment [ Yaw-control PD gain weight using yaw-acceleration under yawc limit,  [default=100%] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG                */		      70,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot Zero, Default = 70 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG_1ST            */		      85,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 1ST, Default = 85 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG_2ND            */		     120,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 2ND, Default = 120 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG                */		      70,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot Zero, Default = 70 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG_1ST            */		      85,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 1ST, Default = 85 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG_2ND            */		     120,	/* Comment [ At High-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 2ND, Default = 120 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG          */		     130,	/* Comment [ At High-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot Zero, Default = 130 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST      */		     120,	/* Comment [ At High-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot 1ST, Default = 120 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND      */		     100,	/* Comment [ At High-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot 2ND, Default = 100 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_HIG_V_1ST            */		    3000,	/* Comment [ At High-mu, Ay_Vx_Dot 1ST, Default = 30 ] */
	                                                        		        	/* ScaleVal[  30 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_LOW_V_1ST            */		    5000,	/* Comment [ At High-mu, Ay_Vx_Dot 1ST, Default = 50 ] */
	                                                        		        	/* ScaleVal[  50 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_MED1_V_1ST           */		    3200,	/* Comment [ At High-mu, Ay_Vx_Dot 1ST, Default = 32 ] */
	                                                        		        	/* ScaleVal[  32 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VER_HIG_V_1ST        */		    2000,	/* Comment [ At High-mu, Ay_Vx_Dot 1ST, Default = 20 ] */
	                                                        		        	/* ScaleVal[  20 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VLOW_V_1ST           */		    6000,	/* Comment [ At High-mu, Ay_Vx_Dot 1ST, Default = 60 ] */
	                                                        		        	/* ScaleVal[  60 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_HIG_V_2ND            */		    4000,	/* Comment [ At High-mu, Ay_Vx_Dot 2ND, Default = 40 ] */
	                                                        		        	/* ScaleVal[  40 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_LOW_V_2ND            */		    6500,	/* Comment [ At High-mu, Ay_Vx_Dot 2ND, Default = 65 ] */
	                                                        		        	/* ScaleVal[  65 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_MED1_V_2ND           */		    4800,	/* Comment [ At High-mu, Ay_Vx_Dot 2ND, Default = 48 ] */
	                                                        		        	/* ScaleVal[  48 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VER_HIG_V_2ND        */		    3500,	/* Comment [ At High-mu, Ay_Vx_Dot 2ND, Default = 35 ] */
	                                                        		        	/* ScaleVal[  35 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VLOW_V_2ND           */		    7000,	/* Comment [ At High-mu, Ay_Vx_Dot 2ND, Default = 70 ] */
	                                                        		        	/* ScaleVal[  70 rad/s2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_DIFF_Lmu           */		     400,	/* Comment [ At Low-mu, Difference Beta from Permit Beta angle, Default = 4 ] */
	                                                        		        	/* ScaleVal[      4 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_DIFF_Mmu           */		     400,	/* Comment [ At Med-mu, Difference Beta from Permit Beta angle, Default = 4 ] */
	                                                        		        	/* ScaleVal[      4 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_HIGH_SPD           */		    1000,	/* Comment [ High-speed dependent on Gain Weight, Default = 100 ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_H_Spd_Lmu          */		    1000,	/* Comment [ At Low-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_H_Spd_Mmu          */		    1000,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_LOW_SPD            */		     200,	/* Comment [ Low-speed dependent on Gain Weight, Default = 20 ] */
	                                                        		        	/* ScaleVal[     20 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_L_Spd_Lmu          */		    1000,	/* Comment [ At Low-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_L_Spd_Mmu          */		    1000,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_MED_SPD            */		     700,	/* Comment [ Med-speed dependent on Gain Weight, Default = 70 ] */
	                                                        		        	/* ScaleVal[     70 kph ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	S16_YC_PERMIT_BETA_MIN_GAIN_WEG_Lmu   */		      70,	/* Comment [ At min beta, ESC YC PD Gain Weight, Default = 70 ] */
	/* uchar8_t    	S16_YC_PERMIT_BETA_MIN_GAIN_WEG_Mmu   */		      70,	/* Comment [ At min beta, ESC YC PD Gain Weight, Default = 70 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_M_Spd_Lmu          */		    1000,	/* Comment [ At Low-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_PERMIT_BETA_M_Spd_Mmu          */		    1000,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Beta, Default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG_1ST_Mmu        */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 1ST, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG_2ND_Mmu        */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 2ND, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_D_WEG_Mmu            */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot Zero, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG_1ST_Mmu        */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 1ST, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG_2ND_Mmu        */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot 2ND, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_P_WEG_Mmu            */		     100,	/* Comment [ At Med-mu, ESC YC Gain Weight dependent on Ay_Vx_Dot Zero, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST_Mmu  */		     100,	/* Comment [ At Med-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot 1ST, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND_Mmu  */		     100,	/* Comment [ At Med-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot 2ND, [Default = 100] ] */
	/* uchar8_t    	S16_YC_AY_VX_DOT_YAW_LMT_WEG_Mmu      */		     100,	/* Comment [ At Med-mu, Yaw_Limit Gain Weight dependent on Ay_Vx_Dot Zero, [Default = 100] ] */
	/* int16_t     	S16_YC_AY_VX_DOT_HIG_V_1ST_Mmu        */		    3000,	/* Comment [ At Med-mu, Ay_Vx_Dot 1ST, [Default = 30] ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_LOW_V_1ST_Mmu        */		    5000,	/* Comment [ At Med-mu, Ay_Vx_Dot 1ST, [Default = 50] ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_MED1_V_1ST_Mmu       */		    3200,	/* Comment [ At Med-mu, Ay_Vx_Dot 1ST, [Default = 32] ] */
	                                                        		        	/* ScaleVal[         32 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VER_HIG_V_1ST_Mmu    */		    2000,	/* Comment [ At Med-mu, Ay_Vx_Dot 1ST, [Default = 20] ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VLOW_V_1ST_Mmu       */		    6000,	/* Comment [ At Med-mu, Ay_Vx_Dot 1ST, [Default = 60] ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_HIG_V_2ND_Mmu        */		    4000,	/* Comment [ At Med-mu, Ay_Vx_Dot 2ND, [Default = 40] ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_LOW_V_2ND_Mmu        */		    6500,	/* Comment [ At Med-mu, Ay_Vx_Dot 2ND, [Default = 65] ] */
	                                                        		        	/* ScaleVal[         65 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_MED1_V_2ND_Mmu       */		    4800,	/* Comment [ At Med-mu, Ay_Vx_Dot 2ND, [Default = 48] ] */
	                                                        		        	/* ScaleVal[         48 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VER_HIG_V_2ND_Mmu    */		    3500,	/* Comment [ At Med-mu, Ay_Vx_Dot 2ND, [Default = 35] ] */
	                                                        		        	/* ScaleVal[         35 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_YC_AY_VX_DOT_VLOW_V_2ND_Mmu       */		    7000,	/* Comment [ At Med-mu, Ay_Vx_Dot 2ND, [Default = 70] ] */
	                                                        		        	/* ScaleVal[         70 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_3RD_SHOT_DET_REF_WSTR_H           */		     500,	/* Comment [ 3rd reference steering wheel angle for calculation of 3rd shot detection steering wheel angle rate threshold, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_REF_WSTR_L           */		       0,	/* Comment [ 1st reference steering wheel angle for calculation of 3rd shot detection steering wheel angle rate threshold, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_REF_WSTR_M           */		     100,	/* Comment [ 2nd reference steering wheel angle for calculation of 3rd shot detection steering wheel angle rate threshold, default = 10 ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_STABLE_D_YAW         */		     200,	/* Comment [ yaw rate threshold to determine stable state, default = 2 ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_3RD_SHOT_DET_WSTR                 */		     500,	/* Comment [ Steering wheel angle threshold for 3rd shot detection, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_WSTR_DOT_H           */		    3600,	/* Comment [ Steering wheel angle rate threshold for 3rd reference steering wheel angle, default = 360 ] */
	                                                        		        	/* ScaleVal[  360 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_WSTR_DOT_INC         */		    1000,	/* Comment [ Steering wheel angle rate threshold when steering wheel angle is increasing, default = 100 ] */
	                                                        		        	/* ScaleVal[  100 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_WSTR_DOT_L           */		    1000,	/* Comment [ Steering wheel angle rate threshold for 1st reference steering wheel angle, default = 100 ] */
	                                                        		        	/* ScaleVal[  100 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_SHOT_DET_WSTR_DOT_M           */		    2500,	/* Comment [ Steering wheel angle rate threshold for 2nd reference steering wheel angle, default = 250 ] */
	                                                        		        	/* ScaleVal[  250 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_3RD_SHOT_DET_STABLE_D_YAW_T       */		      50,	/* Comment [ time duration to determine stable state, default = 500 ] */
	                                                        		        	/* ScaleVal[     500 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 655350 ] */
	/* uchar8_t    	U8_3RD_SHOT_DET_MU_GAIN_HMU           */		     100,	/* Comment [ Gain of mu level to determine the vehicle is within stable range on high mu, default = 100 ] */
	/* uchar8_t    	U8_3RD_SHOT_DET_MU_GAIN_LMU           */		     100,	/* Comment [ Gain of mu level to determine the vehicle is within stable range on low mu, default = 100 ] */
	/* uchar8_t    	U8_3RD_SHOT_DET_MU_GAIN_MMU           */		     100,	/* Comment [ Gain of mu level to determine the vehicle is within stable range on med mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_H_HMU      */		     500,	/* Comment [ 3rd reference steering wheel angle rate for calculation of 3rd shot gain at high-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[   50 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_L_HMU      */		       0,	/* Comment [ 1st reference steering wheel angle rate for calculation of 3rd shot gain at high-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_M_HMU      */		     400,	/* Comment [ 2nd reference steering wheel angle rate for calculation of 3rd shot gain at high-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[   40 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_H_HMU          */		     500,	/* Comment [ 3rd reference steering wheel angle for calculation of 3rd shot gain at high-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_L_HMU          */		       0,	/* Comment [ 1st reference steering wheel angle for calculation of 3rd shot gain at high-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_M_HMU          */		     400,	/* Comment [ 2nd reference steering wheel angle for calculation of 3rd shot gain at high-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[     40 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_H_HMU       */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle rate at high-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_L_HMU       */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle rate at high-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_M_HMU       */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle rate at high-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_H_HMU        */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle at high-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_L_HMU        */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle at high-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_M_HMU        */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle at high-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_H_MMU      */		     500,	/* Comment [ 3rd reference steering wheel angle rate for calculation of 3rd shot gain at med-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[   50 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_L_MMU      */		       0,	/* Comment [ 1st reference steering wheel angle rate for calculation of 3rd shot gain at med-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_M_MMU      */		     400,	/* Comment [ 2nd reference steering wheel angle rate for calculation of 3rd shot gain at med-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[   40 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_H_MMU          */		     500,	/* Comment [ 3rd reference steering wheel angle for calculation of 3rd shot gain at med-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_L_MMU          */		       0,	/* Comment [ 1st reference steering wheel angle for calculation of 3rd shot gain at med-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_M_MMU          */		     400,	/* Comment [ 2nd reference steering wheel angle for calculation of 3rd shot gain at med-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[     40 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_H_MMU       */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle rate at med-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_L_MMU       */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle rate at med-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_M_MMU       */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle rate at med-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_H_MMU        */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle at med-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_L_MMU        */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle at med-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_M_MMU        */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle at med-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_H_LMU      */		     500,	/* Comment [ 3rd reference steering wheel angle rate for calculation of 3rd shot gain at low-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[   50 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_L_LMU      */		       0,	/* Comment [ 1st reference steering wheel angle rate for calculation of 3rd shot gain at low-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_DOT_M_LMU      */		     400,	/* Comment [ 2nd reference steering wheel angle rate for calculation of 3rd shot gain at low-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[   40 deg/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_H_LMU          */		     500,	/* Comment [ 3rd reference steering wheel angle for calculation of 3rd shot gain at low-mu, default = 50 ] */
	                                                        		        	/* ScaleVal[     50 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_L_LMU          */		       0,	/* Comment [ 1st reference steering wheel angle for calculation of 3rd shot gain at low-mu, default = 0 ] */
	                                                        		        	/* ScaleVal[      0 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_REF_WSTR_M_LMU          */		     400,	/* Comment [ 2nd reference steering wheel angle for calculation of 3rd shot gain at low-mu, default = 40 ] */
	                                                        		        	/* ScaleVal[     40 deg ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_H_LMU       */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle rate at low-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_L_LMU       */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle rate at low-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTRD_M_LMU       */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle rate at low-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_H_LMU        */		     100,	/* Comment [ 3rd shot gain at 3rd reference steering wheel angle at low-mu, default = 100 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_L_LMU        */		      30,	/* Comment [ 3rd shot gain at 1st reference steering wheel angle at low-mu, default = 30 ] */
	/* int16_t     	S16_3RD_ST_DM_WT_BY_WSTR_M_LMU        */		      30,	/* Comment [ 3rd shot gain at 2nd reference steering wheel angle at low-mu, default = 30 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_REF_SPD1        */		     240,	/* Comment [ 1st reference vehicle speed to determine initial motor scan ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_REF_SPD2        */		     400,	/* Comment [ 2nd reference vehicle speed to determine initial motor scan ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_REF_SPD3        */		     640,	/* Comment [ 3rd reference vehicle speed to determine initial motor scan ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_REF_SPD4        */		     960,	/* Comment [ 4th reference vehicle speed to determine initial motor scan ] */
	                                                        		        	/* ScaleVal[    120 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_HMU_SPD1        */		      10,	/* Comment [ Initial motor scan for 1st reference vehicle speed in high mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_HMU_SPD2        */		      12,	/* Comment [ Initial motor scan for 2nd reference vehicle speed in high mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_HMU_SPD3        */		      25,	/* Comment [ Initial motor scan for 3rd reference vehicle speed in high mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_HMU_SPD4        */		      35,	/* Comment [ Initial motor scan for 4th reference vehicle speed in high mu ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_HMU_1           */		      50,	/* Comment [ 1st reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_HMU_2           */		     100,	/* Comment [ 2nd reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[  1 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_HMU_3           */		     200,	/* Comment [ 3rd reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[  2 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_HMU_1       */		      80,	/* Comment [ Initial motor target voltage for 1st reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_HMU_2       */		     100,	/* Comment [ Initial motor target voltage for 2nd reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_HMU_3       */		     140,	/* Comment [ Initial motor target voltage for 3rd reference yaw out dot in high mu ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_LMU_SPD1        */		      10,	/* Comment [ Initial motor scan for 1st reference vehicle speed in low mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_LMU_SPD2        */		      12,	/* Comment [ Initial motor scan for 2nd reference vehicle speed in low mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_LMU_SPD3        */		      25,	/* Comment [ Initial motor scan for 3rd reference vehicle speed in low mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_LMU_SPD4        */		      35,	/* Comment [ Initial motor scan for 4th reference vehicle speed in low mu ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_LMU_1           */		      50,	/* Comment [ 1st reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_LMU_2           */		     100,	/* Comment [ 2nd reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[  1 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_LMU_3           */		     200,	/* Comment [ 3rd reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[  2 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_LMU_1       */		      30,	/* Comment [ Initial motor target voltage for 1st reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_LMU_2       */		      70,	/* Comment [ Initial motor target voltage for 2nd reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_LMU_3       */		     100,	/* Comment [ Initial motor target voltage for 3rd reference yaw out dot in low mu ] */
	                                                        		        	/* ScaleVal[       10 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_MMU_SPD1        */		      10,	/* Comment [ Initial motor scan for 1st reference vehicle speed in med mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_MMU_SPD2        */		      12,	/* Comment [ Initial motor scan for 2nd reference vehicle speed in med mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_MMU_SPD3        */		      15,	/* Comment [ Initial motor scan for 3rd reference vehicle speed in med mu ] */
	/* int16_t     	S16_ESC_INIT_MTR_SCAN_MMU_SPD4        */		      25,	/* Comment [ Initial motor scan for 4th reference vehicle speed in med mu ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_MMU_1           */		      50,	/* Comment [ 1st reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[ 0.5 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_MMU_2           */		     100,	/* Comment [ 2nd reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[  1 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ESC_INIT_REF_YAWD_MMU_3           */		     200,	/* Comment [ 3rd reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[  2 deg/s^2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_MMU_1       */		      30,	/* Comment [ Initial motor target voltage for 1st reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_MMU_2       */		      50,	/* Comment [ Initial motor target voltage for 2nd reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_ESC_INIT_TAR_VOLT_YAWD_MMU_3       */		      60,	/* Comment [ Initial motor target voltage for 3rd reference yaw out dot in med mu ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_UND_2WH_USC_INITIAL_FULL_TIME      */		       5,	/* Comment [ Initial full on time for actuation during 2wheel under steer control, default = 50 ] */
	                                                        		        	/* ScaleVal[      50 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_HM_HSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on high mu/high speed, default = 20 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_HM_LSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on high mu/low speed, default = 20 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_HM_MSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on high mu/med speed, default = 20 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_LM_HSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on low mu/high speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_LM_LSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on low mu/low speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_LM_MSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on low mu/med speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_MM_HSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on med mu/high speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_MM_LSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on med mu/low speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_DELM_REAR_MM_MSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 2wheel under steer control on med mu/med speed, default = 30 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_HM_HSP       */		       0,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on high mu/high speed, default = 0 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_HM_LSP       */		       0,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on high mu/low speed, default = 0 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_HM_MSP       */		       0,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on high mu/med speed, default = 0 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_LM_HSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on low mu/high speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_LM_LSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on low mu/low speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_LM_MSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on low mu/med speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_MM_HSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on med mu/high speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_MM_LSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on med mu/low speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_ENT_SLIP_REAR_MM_MSP       */		      10,	/* Comment [ Rear negative wheel slip threshold to enter 2wheel under steer control on med mu/med speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_HM_HSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on high mu/high speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_HM_LSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on high mu/low speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_HM_MSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on high mu/med speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_LM_HSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on low mu/high speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_LM_LSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on low mu/low speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_LM_MSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on low mu/med speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_MM_HSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on med mu/high speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_MM_LSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on med mu/low speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_EXIT_DELM_REAR_MM_MSP      */		       9,	/* Comment [ Delta moment_under threshold to exit 2wheel under steer control on med mu/med speed, default = 9 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_HM_HSP        */		      15,	/* Comment [ Delta moment_front limit during 2wheel under steer control on high mu/high speed, default = 15 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_HM_LSP        */		      15,	/* Comment [ Delta moment_front limit during 2wheel under steer control on high mu/low speed, default = 15 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_HM_MSP        */		      15,	/* Comment [ Delta moment_front limit during 2wheel under steer control on high mu/med speed, default = 15 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_LM_HSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on low mu/high speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_LM_LSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on low mu/low speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_LM_MSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on low mu/med speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_MM_HSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on med mu/high speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_MM_LSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on med mu/low speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_DELM_LMT_MM_MSP        */		      10,	/* Comment [ Delta moment_front limit during 2wheel under steer control on med mu/med speed, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_FADE_OUT_HM            */		      10,	/* Comment [ Delta moment_under threshold to fade out 2wheel under steer control on high mu, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_FADE_OUT_LM            */		      10,	/* Comment [ Delta moment_under threshold to fade out 2wheel under steer control on low mu, default = 10 ] */
	/* char8_t     	S8_UND_2WH_FRT_FADE_OUT_MM            */		      10,	/* Comment [ Delta moment_under threshold to fade out 2wheel under steer control on med mu, default = 10 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_HM_HSP       */		     125,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on high mu/high speed, default = 25 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_HM_LSP       */		     125,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on high mu/low speed, default = 25 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_HM_MSP       */		     125,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on high mu/med speed, default = 25 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_LM_HSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on low mu/high speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_LM_LSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on low mu/low speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_LM_MSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on low mu/med speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_MM_HSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on med mu/high speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_MM_LSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on med mu/low speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_ENT_DELM_REAR_MM_MSP       */		     100,	/* Comment [ Delta moment_under threshold to enter 3wheel under steer control on med mu/med speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_HM_HSP      */		      10,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on high mu/high speed, default = 10 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_HM_LSP      */		      10,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on high mu/low speed, default = 10 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_HM_MSP      */		      10,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on high mu/med speed, default = 10 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_LM_HSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on low mu/high speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_LM_LSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on low mu/low speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_LM_MSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on low mu/med speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_MM_HSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on med mu/high speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_MM_LSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on med mu/low speed, default = 100 ] */
	/* char8_t     	S8_UND_3WH_EXIT_DELM_REAR_MM_MSP      */		     100,	/* Comment [ Delta moment_under threshold to exit 3wheel under steer control on med mu/med speed, default = 100 ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_F            */		     200,	/* Comment [ Front, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_FRONT는 차량마다 다르나 130~210정도의 값임. C1_ Front T300은 137임. SUV 차량은 더 큰값임. View에서 직진 주행중 sPgan값이 C1_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_F            */		     150,	/* Comment [ Front, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_F        */		      30,	/* Comment [ Front, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_F        */		     250,	/* Comment [ Front, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임.C2_ Front T300은 220임. SUV 차량은 더 큰값임. View에서 직진 주행중 dPgan값이 C2_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_F_CBS        */		     250,	/* Comment [ Partial Braking 시, Front, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_FRONT는 차량마다 다르나 130~210정도의 값임.C1_ Front T300은 137임. SUV 차량은 더 큰값임. View에서 직진 주행중 sPgan값이 C1_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_F_CBS        */		     150,	/* Comment [ Partial Braking 시,Front, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_CBS    */		      30,	/* Comment [ Partial Braking 시,Front, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_F_CBS    */		     150,	/* Comment [ Partial Braking 시,Front, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임.C2_ Front T300은 220임. SUV 차량은 더 큰값임. View에서 직진 주행중 dPgan값이 C2_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_F_ABS        */		     250,	/* Comment [ Full Braking 시, Front, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_FRONT는 차량마다 다르나 130~210정도의 값임.C1_ Front T300은 137임. SUV 차량은 더 큰값임. View에서 직진 주행중 sPgan값이 C1_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_F_ABS        */		     150,	/* Comment [ Full Braking 시, Front, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_ABS    */		      30,	/* Comment [ Full Braking 시, Front, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_F_ABS    */		     150,	/* Comment [ Full Braking 시, Front, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임.C2_ Front T300은 220임. SUV 차량은 더 큰값임. View에서 직진 주행중 dPgan값이 C2_FRONT ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_F            */		     200,	/* Comment [ Front, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_F            */		     150,	/* Comment [ Front, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_F        */		      30,	/* Comment [ Front, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_F        */		     250,	/* Comment [ Front, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_F_CBS        */		     250,	/* Comment [ Partial Braking 시,Front, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_F_CBS        */		     150,	/* Comment [ Partial Braking 시,Front, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_CBS    */		      30,	/* Comment [ Partial Braking 시,Front, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_F_CBS    */		     150,	/* Comment [ Partial Braking 시,Front, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_F_ABS        */		     250,	/* Comment [ Full Braking 시, Front, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_F_ABS        */		     150,	/* Comment [ Full Braking 시, Front, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_ABS    */		      30,	/* Comment [ Full Braking 시, Front, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_F_ABS    */		     150,	/* Comment [ Full Braking 시, Front, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_F            */		     200,	/* Comment [ Front, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_F            */		     150,	/* Comment [ Front, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_F        */		      30,	/* Comment [ Front, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_F        */		     250,	/* Comment [ Front, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_F_CBS        */		     250,	/* Comment [ Partial Braking 시,Front, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_F_CBS        */		     150,	/* Comment [ Partial Braking 시,Front, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_CBS    */		      30,	/* Comment [ Partial Braking 시,Front, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_F_CBS    */		     150,	/* Comment [ Partial Braking 시,Front, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_F_ABS        */		     250,	/* Comment [ Full Braking 시, Front, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_F_ABS        */		     150,	/* Comment [ Full Braking 시, Front, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_ABS    */		      30,	/* Comment [ Full Braking 시, Front, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_F_ABS    */		     150,	/* Comment [ Full Braking 시, Front, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_R            */		      90,	/* Comment [ Rear, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_Rear는 차량마다 다르나 170~210정도의 값임. C1_ Reart T300은 150임. SUV 차량은 더 큰값임. View에서 직진 주행중del_yaw <0 sPgan값이 C1_rear ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_R            */		      60,	/* Comment [ Rear, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_R        */		     120,	/* Comment [ Rear, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_R        */		     100,	/* Comment [ Rear, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임 ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_R_CBS        */		     100,	/* Comment [ Partial Braking 시, Rear, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_rear는 차량마다 다르나 170~210정도의 값임C1_ Reart T300은 150임. SUV 차량은 더 큰값임. View에서 직진 주행중del_yaw <0 sPgan값이 C1_rear ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_R_CBS        */		      60,	/* Comment [ Partial Braking 시,Rear, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_CBS    */		     120,	/* Comment [ Partial Braking 시,Rear, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_R_CBS    */		     100,	/* Comment [ Partial Braking 시,Rear, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임 ] */
	/* int16_t     	S16_DEL_SLIP_HMU_NEG_WEG_R_ABS        */		     100,	/* Comment [ Full Braking 시, Rear, High mu에서, slip이 진행할 때, del_target_slip의 Weighting 값,기존 mgj-40에서 slip control  P gain 23 일 때,  130 정도임. WEG * C1_FRONT *del_target_slip 의 weghting값임.  C1_rear는 차량마다 다르나 170~210정도의 값임 .C1_ Reart T300은 150임. SUV 차량은 더 큰값임. View에서 직진 주행중del_yaw <0 sPgan값이 C1_rear ] */
	/* int16_t     	S16_DEL_SLIP_HMU_POS_WEG_R_ABS        */		      70,	/* Comment [ Full Braking 시, Rear, High mu에서,휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_ABS    */		     100,	/* Comment [ Full Braking 시, Rear, High mu에서,휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_HMU_WEG_R_ABS    */		      90,	/* Comment [ Full Braking 시, Rear, High mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값. 기존 mgj-40에서 slip control  D gain 100 일 때,  60 정도임. WEG * C2_FRONT *Wheel_speed_rate 의 weghting값임. C2_FRONT는 차량마다 다르나 200 정도의 값임 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_R            */		     100,	/* Comment [ Rear, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_R            */		      90,	/* Comment [ Rear, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_R        */		     100,	/* Comment [ Rear, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_R        */		      80,	/* Comment [ Rear, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_R_CBS        */		      80,	/* Comment [ Partial Braking 시,Rear, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_R_CBS        */		      90,	/* Comment [ Partial Braking 시,Rear, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_CBS    */		     100,	/* Comment [ Partial Braking 시,Rear, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_R_CBS    */		      80,	/* Comment [ Partial Braking 시,Rear, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_NEG_WEG_R_ABS        */		      90,	/* Comment [ Full Braking 시, Rear, Med mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_MMU_POS_WEG_R_ABS        */		      90,	/* Comment [ Full Braking 시, Rear, Med mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_ABS    */		     100,	/* Comment [ Full Braking 시, Rear, Med mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_MMU_WEG_R_ABS    */		      80,	/* Comment [ Full Braking 시, Rear, Med mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_R            */		     150,	/* Comment [ Rear, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_R            */		     120,	/* Comment [ Rear, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_R        */		     100,	/* Comment [ Rear, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_R        */		      80,	/* Comment [ Rear, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_R_CBS        */		     110,	/* Comment [ Partial Braking 시,Rear, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_R_CBS        */		     100,	/* Comment [ Partial Braking 시,Rear, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_CBS    */		     100,	/* Comment [ Partial Braking 시,Rear, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_R_CBS    */		      80,	/* Comment [ Partial Braking 시,Rear, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_NEG_WEG_R_ABS        */		     120,	/* Comment [ Full Braking 시, Rear, Low mu에서, slip이 진행할 때, del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_SLIP_LMU_POS_WEG_R_ABS        */		     100,	/* Comment [ Full Braking 시, Rear, Low mu에서, 휠이 살때 , del_target_slip의 Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_ABS    */		     100,	/* Comment [ Full Braking 시, Rear, Low mu에서, 휠이 살때 , wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_DEL_WHEEL_SP_INC_LMU_WEG_R_ABS    */		      80,	/* Comment [ Full Braking 시, Rear, Low mu에서, slip이 진행할 때, wheel_speed_rate Weighting 값 ] */
	/* int16_t     	S16_ESP_SLIP_SPEED_GAIN_DEP_H_V       */		    1050,	/* Comment [ 고속 속도영역 ] */
	                                                        		        	/* ScaleVal[        105 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESP_SLIP_SPEED_GAIN_DEP_L_V       */		     550,	/* Comment [ 중저속 속도영역 ] */
	                                                        		        	/* ScaleVal[         55 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESP_SLIP_SPEED_GAIN_DEP_M_V       */		     700,	/* Comment [ 중고속 속도영역 ] */
	                                                        		        	/* ScaleVal[         70 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESP_SLIP_SPEED_GAIN_DEP_S_V       */		     150,	/* Comment [ 저속 속도영역 ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ESP_SLIP_SP_H_MU_GAIN_DEP_H       */		     100,	/* Comment [ High mu 고속 영역 weight [90~300] ] */
	/* int16_t     	S16_ESP_SLIP_SP_H_MU_GAIN_DEP_L       */		      80,	/* Comment [ High mu 저속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_SLIP_SP_H_MU_GAIN_DEP_M       */		      90,	/* Comment [ High mu 중속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_H     */		     100,	/* Comment [ High mu, High Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_L     */		      90,	/* Comment [ High mu, Low Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_M     */		      90,	/* Comment [ High mu, Med Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_SLIP_SP_M_MU_GAIN_DEP_H       */		     100,	/* Comment [ Med mu 고속 영역 weight [90~300] ] */
	/* int16_t     	S16_ESP_SLIP_SP_M_MU_GAIN_DEP_L       */		      80,	/* Comment [ Med mu 저속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_SLIP_SP_M_MU_GAIN_DEP_M       */		      90,	/* Comment [ Med mu 저속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_H     */		      80,	/* Comment [ Med mu, High Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_L     */		     100,	/* Comment [ Med mu, Low Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_M     */		      90,	/* Comment [ Med mu, Med Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_SLIP_SP_L_MU_GAIN_DEP_H       */		     100,	/* Comment [ Low mu 고속 영역 weight [80~300] ] */
	/* int16_t     	S16_ESP_SLIP_SP_L_MU_GAIN_DEP_L       */		      80,	/* Comment [ Low mu 저속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_SLIP_SP_L_MU_GAIN_DEP_M       */		      95,	/* Comment [ Low mu 중속 영역 weight [10~100] ] */
	/* int16_t     	S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_H     */		      80,	/* Comment [ Low mu, High Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_L     */		     100,	/* Comment [ Low mu, Low Speed D gain의 Speed Weighting Factor ] */
	/* int16_t     	S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_M     */		      90,	/* Comment [ Low mu, Med Speed D gain의 Speed Weighting Factor ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_HM_HSP_REAR         */		     120,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_HM_LSP_REAR         */		      60,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 80] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_HM_MSP_REAR         */		      90,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_LM_HSP_REAR         */		      70,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_LM_LSP_REAR         */		      30,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 90] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_LM_MSP_REAR         */		      50,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_MM_HSP_REAR         */		     100,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_MM_LSP_REAR         */		      60,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 90] ] */
	/* uchar8_t    	S16_ESP_SLIP_SP_W_MM_MSP_REAR         */		      80,	/* Comment [ At Slip_control Rear, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_HMU_HSP       */		     110,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_HMU_LSP       */		      70,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 70] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_HMU_MSP       */		      90,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 80] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_LMU_HSP       */		     100,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 100] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_LMU_LSP       */		      90,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 90] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_LMU_MSP       */		      95,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 95] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_MMU_HSP       */		     100,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 125] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_MMU_LSP       */		      50,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 40] ] */
	/* uchar8_t    	S16_EXT_UND_SLIP_SP_WEG_MMU_MSP       */		      90,	/* Comment [ At Extreme Under control, Speed dependent weight, [Default = 105] ] */
	/* int16_t     	S16_RATE_INTERVAL_FRONT_MIN           */		       1,	/* Comment [ Dump 시 최소 Dump 기울기 [1~3] ] */
	/* int16_t     	S16_RATE_INTERVAL_GAIN                */		      50,	/* Comment [ Dump Sacn 결정 Gain ( Dump Scan = Gain / slip control) [100 ~ 500] ] */
	/* int16_t     	S16_RATE_INTERVAL_MAX_H               */		       2,	/* Comment [ High mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_MAX_L               */		       2,	/* Comment [ Low mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_MAX_M               */		       2,	/* Comment [ Med mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_GAIN           */		      40,	/* Comment [ Rear의 Dump scan 결정 Gain [100~500] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_MAX_H          */		       3,	/* Comment [ Rear의 High mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_MAX_L          */		       3,	/* Comment [ Rear의 Low mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_MAX_M          */		       3,	/* Comment [ Rear의 Med mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_MIN            */		       1,	/* Comment [ Rear의 Dump scan 최소값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_ABS_MAX_H           */		       3,	/* Comment [ ABS High mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_ABS_MAX_L           */		       4,	/* Comment [ ABS Low mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_ABS_MAX_M           */		       4,	/* Comment [ ABS Med mu에서의 MAX Dump rate 설정 ] */
	/* int16_t     	S16_RATE_INTERVAL_FRONT_MIN_ABS       */		       1,	/* Comment [ ABS협조제어에서 Dump 시 최소 Dump 기울기[1~3] ] */
	/* int16_t     	S16_RATE_INTERVAL_GAIN_ABS            */		      50,	/* Comment [ ABS에서 Dump Sacn 결정 Gain ( Dump Scan = Gain / slip control) [100 ~ 500] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_ABS_MAXH       */		       3,	/* Comment [ ABS협조 제어 시 Rear의 High mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_ABS_MAXL       */		       3,	/* Comment [ ABS협조 제어 시 Rear의 Low mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_ABS_MAXM       */		       3,	/* Comment [ ABS협조 제어 시 Rear의 Med mu에서 Dump scan 최대값 [1~5] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_GAIN_ABS       */		      30,	/* Comment [ ABS협조 제어 시 Rear의 Dump scan 결정 Gain [100~500] ] */
	/* int16_t     	S16_RATE_INTERVAL_REAR_MIN_ABS        */		       1,	/* Comment [ ABS협조 제어 시 Rear의 Dump scan 최소값 ] */
	/* int16_t     	S16_1ST_GUARANTEE_SLIP_ERROR_F        */		     500,	/* Comment [ 1st상황에서 front 휠 rise확보 위한 slip error 보장 범위. 예를 들어, 5% 설정시, 1st Pgain은 바퀴 slip없을 경우 err 5%이상시 rise시킬 수 있는 값으로 fix됨. ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	U8_SLIP_F_RISE_FISRT_TH               */		     200,	/* Comment [ 1st cycle 시 Front Slip control Rise Thresold add 됨- 값을 키우면 Rise을 더 많이 함[0~100] ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_H_MU_F        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_H_MU_F         */		      10,	/* Comment [ Yaw rate condition for 1st cycle reset @ High mu, Front (default 300,(3d/s)), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[ 0.1 X100deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_H_MU_F      */		     500,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ High mu, Front (default 500,(5%)), dec this -> reset delay ] */
	                                                        		        	/* ScaleVal[    5 X100% ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_M_MU_F        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_M_MU_F         */		      30,	/* Comment [ Yaw rate condition for 1st cycle reset @ Med mu, Front (default 10,(0.1d/s)), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[ 0.3 X100deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_M_MU_F      */		     200,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ Med mu, Front (default 200,(2%)), dec this -> reset delay ] */
	                                                        		        	/* ScaleVal[    2 X100% ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_L_MU_F        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_L_MU_F         */		      10,	/* Comment [ Yaw rate condition for 1st cycle reset @ Low mu, Front (default 10,(0.1d/s)), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[ 0.1 X100deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_L_MU_F      */		     200,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ Low mu, Front (default -200, (-2%)), dec this -> reset delay, low mu->down ] */
	                                                        		        	/* ScaleVal[    2 X100% ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_front         */		       0,	/* Comment [ High mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_front           */		      40,	/* Comment [ 1st cycle 시 Slip control Front P Gain add gain[0~50] ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_CBS         */		       0,	/* Comment [ High mu, Front D gain, 1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_CBS           */		      20,	/* Comment [ High mu, Front P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_ABS         */		       0,	/* Comment [ High mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_ABS           */		      20,	/* Comment [ High mu, Front P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_front_MMU     */		      20,	/* Comment [ Med mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_front_MMU       */		      50,	/* Comment [ Med mu, Front P gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu add Pgain. High mu gain은 BKTU 1st cylce p gain 값임 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_MMU_CBS     */		      10,	/* Comment [ Med mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_MMU_CBS       */		      40,	/* Comment [ Med mu, Front P gain1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_MMU_ABS     */		      10,	/* Comment [ Med mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_MMU_ABS       */		      40,	/* Comment [ Med mu, Front P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_front_LMU     */		      20,	/* Comment [ Low mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_front_LMU       */		      50,	/* Comment [ Low mu, Front P gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu add Pgain. High mu gain은 BKTU 1st cylce p gain 값임 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_LMU_CBS     */		      10,	/* Comment [ Low mu, Front D gain,1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_LMU_CBS       */		      40,	/* Comment [ Low mu, Front P gain1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_f_LMU_ABS     */		      10,	/* Comment [ Low mu, Front Dgain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_f_LMU_ABS       */		      40,	/* Comment [ Low mu, Front P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	S16_1ST_GUARANTEE_SLIP_ERROR_R        */		     700,	/* Comment [ 1st상황에서 rear 휠 rise확보 위한 slip error 보장 범위. 예를 들어, 7% 설정시, 1st Pgain은 바퀴 slip없을 경우 err 7%이상시 rise시킬 수 있는 값으로 fix됨. ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	U8_SLIP_R_RISE_FISRT_TH               */		      50,	/* Comment [ 1st cycle 시 Rear Slip control Rise Thresold add 됨- 값을 키우면 Rise을 더 많이 함 ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_H_MU_R        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_H_MU_R         */		     300,	/* Comment [ Yaw rate condition for 1st cycle reset @ High mu, Rear (default 1), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_H_MU_R      */		     500,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ High mu, Rear (default 500), dec this -> reset delay ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_M_MU_R        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_M_MU_R         */		      10,	/* Comment [ Yaw rate condition for 1st cycle reset @ Med mu, Rear (default 1), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[ 0.1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_M_MU_R      */		     200,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ Med mu, Rear (default 200), dec this -> reset delay ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_ENTER_THR_L_MU_R        */		       0,	/* Comment [ 1st cycle Detect 하는 yaw_error_dot ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_1ST_CYCLE_EXIT_THR_L_MU_R         */		      10,	/* Comment [ Yaw rate condition for 1st cycle reset @ Low mu, Rear (default 1), Inc this -> reset delay ] */
	                                                        		        	/* ScaleVal[ 0.1 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	S16_1ST_CYCLE_WH_EXIT_THR_L_MU_R      */		    -200,	/* Comment [ Wheel slip condition for 1st cycle wheel reset @ Low mu, Rear (default -200), dec this -> reset delay, low mu->down ] */
	                                                        		        	/* ScaleVal[       -2 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -30 <= X <= 30 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_rear          */		       0,	/* Comment [ High mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_rear            */		      20,	/* Comment [ 1st cycle 시 Slip control Rear P Gain add gain ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_CBS         */		       0,	/* Comment [ High mu, Rear D gain, 1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_CBS           */		      35,	/* Comment [ High mu, Rear P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_ABS         */		       0,	/* Comment [ High mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_ABS           */		      40,	/* Comment [ High mu, Rear P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_rear_MMU      */		       0,	/* Comment [ Med mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_rear_MMU        */		      20,	/* Comment [ Med mu, Rear P gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu add Pgain. High mu gain은 BKTU 1st cylce p gain 값임 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_MMU_CBS     */		       0,	/* Comment [ Med mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_MMU_CBS       */		      20,	/* Comment [ Med mu, Rear P gain1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_MMU_ABS     */		       0,	/* Comment [ Med mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_MMU_ABS       */		      20,	/* Comment [ Med mu, Rear P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_rear_LMU      */		       0,	/* Comment [ Low mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_rear_LMU        */		      20,	/* Comment [ Low mu, Rear P gain,1ST CYCLE, 1ST WHEEL DETECT 시, ESP 제어 시 High mu add Pgain. High mu gain은 BKTU 1st cylce p gain 값임 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_LMU_CBS     */		       0,	/* Comment [ Low mu, Rear D gain,1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu Minus D gain. D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_LMU_CBS       */		      10,	/* Comment [ Low mu, Rear P gain1ST CYCLE, 1ST WHEEL DETECT 시, CBS 협조 제어 시 High mu add Pgain. P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_D_r_LMU_ABS     */		       0,	/* Comment [ Low mu, Rear Dgain,1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu Minus D gain.D Gain에 직접 Mius 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	sc_add_gain_1st_cycle_r_LMU_ABS       */		      10,	/* Comment [ Low mu, Rear P gain, 1ST CYCLE, 1ST WHEEL DETECT 시, ABS 협조 제어 시 High mu add Pgain.  P Gain에 직접 add 됨. RISE을 더 하고 싶으면 값을 키우면 됨 ] */
	/* int16_t     	S16_COMP_HOLD_EN_F                    */		      25,	/* Comment [ 강제 Hold mode 진입을 위해, Front wheel에 대해 SLIP_CONTROL_NEED 진행 Scan [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_FRONT                   */		      15,	/* Comment [ 강제 Hold mode 진입후, Front wheel에 대해 강제 Hold 유지 Scan [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_LOCK_ACCEPT_F           */		      10,	/* Comment [ 강제 Hold에 따른  Front wheel이  Deep slip(80%) 발생 허용 Scan [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_PRE_CNT_F               */		      14,	/* Comment [ 강제 Hold mode시, Front 압력이 Parameter 이하일 때 강제 Hold진입 [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_EN_R                    */		      20,	/* Comment [ 강제 Hold mode 진입을 위해, Rear wheel에 대해 SLIP_CONTROL_NEED 진행 Scan [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_LOCK_ACCEPT_R           */		      15,	/* Comment [ 강제 Hold에 따른  Rear wheel이  Deep slip(60%) 발생 허용 Scan [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_PRE_CNT_R               */		      15,	/* Comment [ 강제 Hold mode시, Rear 압력이 Parameter 이하일 때 강제 Hold진입 [0~30] ] */
	/* int16_t     	S16_COMP_HOLD_REAR                    */		      15,	/* Comment [ 강제 Hold mode 진입후, Rear wheel에 대해 강제 Hold 유지 Scan [0~30] ] */
	/* int16_t     	S16_SLIP_F_RISE_TH_ADD                */		      40,	/* Comment [ Front Slip Control에서 압력 모드 결정을 위한 Threshold add값(40이면 2%슬립차) [0~60] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_F_H              */		      30,	/* Comment [ High mu, Front Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_F_L              */		      25,	/* Comment [ Low mu, Front Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_F_M              */		      25,	/* Comment [ Med mu, Front Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_SLIP_R_RISE_TH_ADD                */		      50,	/* Comment [ Rear Slip Control에서 압력 모드 결정을 위한 Threshold add값(50이면 3%슬립차) [0~60] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_R_H              */		      20,	/* Comment [ High mu,Rear Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_R_L              */		      20,	/* Comment [ Low mu, Rear Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_SLIP_THR_VAR_CNT_R_M              */		      20,	/* Comment [ Med mu, Rear Slip Control에서 Threshold을 가변 시키기 위한 압력 Level [10~50] ] */
	/* int16_t     	S16_F_APP_RISE_DEL_P_H                */		    2000,	/* Comment [ High mu 에서 U_sc가  tuning값 이상이면 Full Rise ] */
	/* int16_t     	S16_F_APP_RISE_DEL_P_L                */		    2000,	/* Comment [ High mu 에서 U_sc가  tuning값 이상이면 Full Rise ] */
	/* int16_t     	S16_F_APP_RISE_DEL_P_M                */		    2000,	/* Comment [ Med mu 에서 U_sc가  tuning값 이상이면 Full Rise ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_ADD_DUTY_H      */		       0,	/* Comment [ High mu ,add duty ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_ADD_DUTY_L      */		       0,	/* Comment [ Low mu ,add duty ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_ADD_DUTY_M      */		       0,	/* Comment [ Med mu ,add duty ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_GAIN_H          */		     400,	/* Comment [ High mu, Pulse up rise 모드 시, U_sc 값과 연동된 add Duty에 계산됨. Del_P 가 이값마다 add duty 됨 ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_GAIN_L          */		     400,	/* Comment [ Low mu, Pulse up rise 모드 시, U_sc 값과 연동된 add Duty에 계산됨. Del_P 가 이값마다 add duty 됨 ] */
	/* int16_t     	S16_F_DEL_P_RISE_RATE_GAIN_M          */		     400,	/* Comment [ Med mu, Pulse up rise 모드 시, U_sc 값과 연동된 add Duty에 계산됨. Del_P 가 이값마다 add duty 됨 ] */
	/* int16_t     	S16_FADE_OUT_HMU_FULL_DUMP            */		     500,	/* Comment [ Front, High mu에서 yawm이 tuning값 이하 일 때 Full Dump ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_LMU_FULL_DUMP            */		     500,	/* Comment [ Front, Low mu에서 yawm이 tuning값 이하 일 때 Full Dump ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_MMU_FULL_DUMP            */		     500,	/* Comment [ Front, Med mu에서 yawm이 tuning값 이하 일 때 Full Dump ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_F_HMU          */		     500,	/* Comment [ Front, High mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_HSP_F_YAW       */		    2000,	/* Comment [ Front, High mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_LSP_F_YAW       */		    2000,	/* Comment [ Front, High mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_MSP_F_YAW       */		    2000,	/* Comment [ Front, High mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_HMU_F      */		       3,	/* Comment [ Front, High mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_F_MMU          */		     500,	/* Comment [ Front, Med mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_HSP_F_YAW       */		   10000,	/* Comment [ Front, Med mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_LSP_F_YAW       */		   10000,	/* Comment [ Front, Med mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_MSP_F_YAW       */		   10000,	/* Comment [ Front, Med mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_MMU_F      */		       3,	/* Comment [ Front, Med mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_F_LMU          */		     500,	/* Comment [ Front, Low mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_HSP_F_YAW       */		   10000,	/* Comment [ Front, Low mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_LMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_LSP_F_YAW       */		   10000,	/* Comment [ Front, Low mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_LMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_MSP_F_YAW       */		   10000,	/* Comment [ Front, Low mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_F_LMMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[        100 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_LMU_F      */		       3,	/* Comment [ Front, Low mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_R_HMU          */		    1000,	/* Comment [ Rear, High mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_HSP_R_YAW       */		    2000,	/* Comment [ Rear, High mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_LSP_R_YAW       */		    2000,	/* Comment [ High mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_HMU_MSP_R_YAW       */		    2000,	/* Comment [ Rear, High mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_HMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_HMU_R      */		       3,	/* Comment [ Rear, High mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_R_MMU          */		    1000,	/* Comment [ Rear, Med mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_HSP_R_YAW       */		    3000,	/* Comment [ Rear, Med mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_LSP_R_YAW       */		    3000,	/* Comment [ Rear, Med mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_MMU_MSP_R_YAW       */		    3000,	/* Comment [ Rear, Med mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_MMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_MMU_R      */		       3,	/* Comment [ Rear, Med mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_GAIN_R_LMU          */		    1000,	/* Comment [ Rear, Low mu, Fade Out gain. Yawm이 이 튜닝값이 바뀔 때 마다 Dump rate 가 바뀜 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_HSP_R_YAW       */		    3000,	/* Comment [ Rear, Low mu High Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_LMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_LSP_R_YAW       */		    3000,	/* Comment [ Rear, Low mu Low Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_LMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_DUMP_LMU_MSP_R_YAW       */		    3000,	/* Comment [ Rear, Low mu Med Speed에서, (현재 yawm 값 + tuning 값 )/ S16_FADE_OUT_DUMP_GAIN_R_LMMU 이 Fade out control의 Dump rate 가 됨. 이값을 키우면 압력을 천천히 뺌.( Hold scan이 길어짐..) ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_FADE_OUT_RATE_MIN_COMB_LMU_R      */		       3,	/* Comment [ Rear, Low mu, 협조 제어 일 때, Dump rate Min 값 ] */
	/* int16_t     	S16_FADE_OUT_EXIT_SLIP_ABS            */		       0,	/* Comment [ ABS 상황에서 Fade-out 해제되는 slip, 이 값 이하이면 제어해제(No ABS: 4%), 슬립으로 인해 제어해제가 지연되면 증가, def:10% ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_FADE_OUT_TIME_ABS                 */		       0,	/* Comment [ ABS 상황에서 Fade-out control max time, No ABS->40sc, 슬립으로 인해 제어해제가 지연되면 감소, def:20scan ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_HSP_DelM          */		    2500,	/* Comment [ High mu, High speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_LSP_DelM          */		    4000,	/* Comment [ High mu, Low speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_MSP_DelM          */		    2700,	/* Comment [ High mu, Med speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         27 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_HSP_YAW_ERR_D     */		      50,	/* Comment [ High mu, High speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_LSP_YAW_ERR_D     */		     100,	/* Comment [ High mu, Low speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_HMU_MSP_YAW_ERR_D     */		      80,	/* Comment [ High mu, Med speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_HSP_DelM          */		    2800,	/* Comment [ Med mu, High speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         28 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_LSP_DelM          */		    4000,	/* Comment [ Med mu, Low speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_MSP_DelM          */		    2800,	/* Comment [ Med mu, Med speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         28 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_HSP_YAW_ERR_D     */		      50,	/* Comment [ Med mu, High speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_LSP_YAW_ERR_D     */		     100,	/* Comment [ Med mu, Low speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_MMU_MSP_YAW_ERR_D     */		      80,	/* Comment [ Med mu, Med speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_HSP_DelM          */		    3000,	/* Comment [ Low mu, High speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_LSP_DelM          */		    5000,	/* Comment [ Low mu, Low speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_MSP_DelM          */		    3500,	/* Comment [ Low mu, Med speed 시 대각휠제어 판단 Del_M 조건 ] */
	                                                        		        	/* ScaleVal[         35 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_HSP_YAW_ERR_D     */		      50,	/* Comment [ Low mu, High speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_LSP_YAW_ERR_D     */		     100,	/* Comment [ Low mu, Low speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[          1 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	COMB_CNTR_ENTER_LMU_MSP_YAW_ERR_D     */		      80,	/* Comment [ Low mu, Med speed 시 대각휠제어 판단 yaw_error_dot 조건 ] */
	                                                        		        	/* ScaleVal[        0.8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_COMB_MPRESS_H_TH                  */		     400,	/* Comment [ At Partial_Braking, H_Mpress Threshold value, [default=40bar] ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_COMB_MPRESS_L_TH                  */		     200,	/* Comment [ At Partial_Braking, L_Mpress Threshold value, [default=20bar] ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_COMB_MPRESS_M_TH                  */		     300,	/* Comment [ At Partial_Braking, M_Mpress Threshold value, [default=30bar] ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_COMB_MPRESS_V_L_TH                */		     100,	/* Comment [ At Partial_Braking, V_L_Mpress Threshold value, [default=10bar] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	S16_REAR_DEL_PRESS_H_TH               */		      40,	/* Comment [ At Partial_Braking, Rear delta_pressure Threshold value, [default=4bar] ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	S16_REAR_DEL_PRESS_L_TH               */		      30,	/* Comment [ At Partial_Braking, Rear delta_pressure Threshold value, [default=3bar] ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	S16_REAR_DEL_PRESS_M_TH               */		      40,	/* Comment [ At Partial_Braking, Rear delta_pressure Threshold value, [default=4bar] ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	S16_REAR_DEL_PRESS_V_L_TH             */		      30,	/* Comment [ At Partial_Braking, Rear delta_pressure Threshold value, [default=3bar] ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_ABS_CBS_COMB_ADD_MIN_WHEEL_SLIP   */		     500,	/* Comment [ ABS/CBS 협조제어시 최소 wheel slip add 값 ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_HMU_GAIN             */		   10000,	/* Comment [ High mu에서 ABS/CBS 협조제어시 wheel slip의 gain ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_HMU_LIMIT_SLIP       */		    2000,	/* Comment [ High mu에서 ABS/CBS 협조제어시 wheel slip의 limit 값 ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_LMU_GAIN             */		   14000,	/* Comment [ Low mu에서 ABS/CBS 협조제어시 wheel slip의 gain ] */
	                                                        		        	/* ScaleVal[      140 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_LMU_LIMIT_SLIP       */		    4000,	/* Comment [ Low mu에서 ABS/CBS 협조제어시 wheel slip의 limit 값 ] */
	                                                        		        	/* ScaleVal[       40 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_MMU_GAIN             */		   12000,	/* Comment [ Med mu에서 ABS/CBS 협조제어시 wheel slip의 gain ] */
	                                                        		        	/* ScaleVal[      120 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_MMU_LIMIT_SLIP       */		    3000,	/* Comment [ Med mu에서 ABS/CBS 협조제어시 wheel slip의 limit 값 ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_PRESSURE_GAIN        */		     100,	/* Comment [ ABS/CBS 협조제어시 Enter/Exit 조건의 lcesps16EstPressFromDelM gain ] */
	/* int16_t     	S16_ABS_CBS_COMB_PRESSURE_MARGIN      */		      10,	/* Comment [ ABS/CBS 협조제어시 Enter/Exit 조건의 추정압력 마진 ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_PRESSURE_OFFSET      */		      70,	/* Comment [ ABS/CBS 협조제어시 Enter/Exit 조건의 lcesps16EstPressFromDelM offset ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ABS_CBS_COMB_TEMP0                */		       1,	/* Comment [ ABS/CBS 협조제어 temp0 ] */
	/* int16_t     	S16_ABS_CBS_COMB_TEMP1                */		       1,	/* Comment [ ABS/CBS 협조제어 temp1 ] */
	/* int16_t     	S16_ABS_CBS_COMB_TEMP2                */		       1,	/* Comment [ ABS/CBS 협조제어 temp2 ] */
	/* int16_t     	S16_ABS_CBS_COMB_TEMP3                */		       1,	/* Comment [ ABS/CBS 협조제어 temp3 ] */
	/* int16_t     	S16_ABS_CBS_COMB_TEMP4                */		       1,	/* Comment [ ABS/CBS 협조제어 temp4 ] */
	/* int16_t     	S16_BRK_OS_TH_HOLD_MPRESS_ENTER       */		     100,	/* Comment [ At ESC threshold, Mpress value to hold a threshold after braking, [Default = 10] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_YAW_SIGN_CH_THRESHOLD             */		     500,	/* Comment [ At Yaw_sign change, Threshold dependent on steering and yaw-out, [Default = 5] ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SPEED_THR_HIG                     */		    1200,	/* Comment [ Steering angle threshold 설정 속도 ( 고속) [100~120kph] ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SPEED_THR_LOW                     */		     150,	/* Comment [ Steering angle threshold 설정 속도 ( 저속) [10~30] ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SPEED_THR_MED                     */		     600,	/* Comment [ Steering angle threshold 설정 속도 ( 중속) [50~70kph] ] */
	                                                        		        	/* ScaleVal[         60 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_HIG              */		     500,	/* Comment [ Steering angle-Medium 설정각 threshold 크기 (고속) [5~7] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_LARGE_VAL        */		    1000,	/* Comment [ Steering angle-Large 설정각 threshold 크기 [8~10] ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_LOW              */		     700,	/* Comment [ Steering angle-Medium 설정각 threshold 크기 (저속) [7~10] ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_MED              */		     500,	/* Comment [ Steering angle-Medium 설정각 threshold 크기 (중속) [5~7] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_WSTR_THR_LARGE                    */		    5400,	/* Comment [ Steering angle-Large 설정각(고속) [450~MAX] ] */
	                                                        		        	/* ScaleVal[        540 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WSTR_THR_MEDIU                    */		    1200,	/* Comment [ Steering angle-Medium 설정각(중속) [100~200] ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WSTR_THR_SMALL                    */		     100,	/* Comment [ Steering angle-Small 설정각 (저속) [15~50] ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DEL_YAW_THR_LMT_ABS               */		     150,	/* Comment [ del yaw threshold limit in ABS ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_HIG_ABS          */		     400,	/* Comment [ ABS 협조 제어 시 Steering angle-Medium 설정각 threshold 크기 (고속) [5~7] ] */
	                                                        		        	/* ScaleVal[          4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_LARGE_VAL_ABS    */		    1000,	/* Comment [ ABS 협조 제어 시 Steering angle-Large 설정각 threshold 크기 [8~10] ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_LOW_ABS          */		     500,	/* Comment [ ABS 협조 제어 시 Steering angle-Medium 설정각 threshold 크기 (저속) [7~10] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DEL_YAW_THR_WSTR_MED_ABS          */		     400,	/* Comment [ ABS 협조 제어 시 Steering angle-Medium 설정각 threshold 크기 (중속) [5~7] ] */
	                                                        		        	/* ScaleVal[          4 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_UNDER_CONTROL_DISABLE_SPEED       */		     150,	/* Comment [ UNDER CONTROL 제어 금지 적용 속도 [10~20] ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MAX_ON_CENTER_WSTR                */		     100,	/* Comment [ On Center Feel 에서 최대 조향각 조건 ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MAX_SLALOM_WSTR                   */		     300,	/* Comment [ Slalom 에서 최대 조향각 조건 ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_DET_DRF_BETA_D_ENT_H              */		     500,	/* Comment [ Drift Detect 시 High speed 시 Beta dot 조건   [500~1000] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_BETA_D_ENT_M              */		     600,	/* Comment [ Drift Detect 시 Med speed 시 Beta dot 조건   [500~1000] ] */
	                                                        		        	/* ScaleVal[          6 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_BETA_ENT_H                */		     500,	/* Comment [ Drift Detect 시 High speed 시 Beta 조건   [500~1000] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_BETA_ENT_M                */		     900,	/* Comment [ Drift Detect 시 Med speed 시 Beta 조건   [500~1000] ] */
	                                                        		        	/* ScaleVal[          9 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_BETA_EXT_H                */		     300,	/* Comment [ Drift Detect 시 High speed 시 Beta 해지 조건   [200~1000] ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_BETA_EXT_M                */		     500,	/* Comment [ Drift Detect 시 Med speed 시 Beta 해지 조건   [200~1000] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_DET_DRF_SP_H                      */		     500,	/* Comment [ Drift Detect 시 High speed 조건  [200~1000] ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_DET_DRF_SP_M                      */		     300,	/* Comment [ Drift Detect 시 Med speed 조건  [200~1000] ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* int16_t     	S16_SLIP_TARGET_LIMIT_SPLIT_F         */		    1000,	/* Comment [ Front에 대한 Split mu에서 SLIP LIMIT  [1000~2000] ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_SLIP_TARGET_LIMIT_SPLIT_R         */		     500,	/* Comment [ Rear에 대한 Split mu에서 SLIP LIMIT  [1000~2000] ] */
	                                                        		        	/* ScaleVal[          5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Ay_Roll_Gain                      */		     100,	/* Comment [ Gain for compensating lateral G offset by roll effect ] */
	/* int16_t     	S16_Ay_Roll_Thres                     */		     200,	/* Comment [ Threshold of lateral G for S16_Ay_Roll_Gain application ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Beta_Cf_Gain                      */		     100,	/* Comment [ Cf parameter gain for using beta estimation ] */
	/* int16_t     	S16_Beta_Cr_Gain                      */		     100,	/* Comment [ Cr parameter gain for using beta estimation ] */
	/* int16_t     	S16_Front_length                      */		      22,	/* Comment [ Distance from CG to front side of vehicle ] */
	                                                        		        	/* ScaleVal[      2.2 m ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Beta_Nonlinear_Time_Ref           */		     798,	/* Comment [ Minimum time for admitting kinematic model beta ] */
	                                                        		        	/* ScaleVal[   7.98 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Brake_Reset_Time_Ref              */		     430,	/* Comment [ Time after braking for using S16_Pseudo_Gain_Brake ] */
	                                                        		        	/* ScaleVal[    4.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Pseudo_Gain_Bank_Suspect          */		       2,	/* Comment [ Pseudo gain used in case of detecting bank road suspect ] */
	                                                        		        	/* ScaleVal[      0.002 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Brake                 */		       2,	/* Comment [ Pseudo gain used after releasing brake pedal ] */
	                                                        		        	/* ScaleVal[      0.002 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Default               */		       0,	/* Comment [ Pseudo gain used in normal condition, default 0 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Large_Rough_Road      */		       3,	/* Comment [ Pseudo gain used in case of detecting large gravel ] */
	                                                        		        	/* ScaleVal[      0.003 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Med_Rough_Road        */		       2,	/* Comment [ Pseudo gain used in case of detecting medium gravel ] */
	                                                        		        	/* ScaleVal[      0.002 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Offset_Small          */		       1,	/* Comment [ Pseudo gain used in case of detecting small sensor offset ] */
	                                                        		        	/* ScaleVal[      0.001 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Pseudo_Gain_Offset_Suspect        */		       3,	/* Comment [ Pseudo gain used in case of detecting sensor offset suspect ] */
	                                                        		        	/* ScaleVal[      0.003 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Yaw_Stable_Time_Ref               */		     602,	/* Comment [ Minimum time for resetting kinematic model beta to dynamic model beta ] */
	                                                        		        	/* ScaleVal[   6.02 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Del_Ay_Sat_Above_Med_Mu           */		     200,	/* Comment [ Delta lateral G for detecting saturated road adhesion at med or high mu ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Del_Ay_Sat_Below_Med_Mu           */		     150,	/* Comment [ Delta lateral G for detecting saturated road adhesion at med or low mu ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Sensor_Offset_Level1              */		     200,	/* Comment [ Minimum beta dot level for detecting sensor offset ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Sensor_Offset_Level2              */		     500,	/* Comment [ Large beta dot level for detecting sensor offset ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Sensor_Small_Offset_Level1        */		      50,	/* Comment [ Minimum beta dot level for detecting small sensor offset ] */
	                                                        		        	/* ScaleVal[  0.5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Sensor_Small_Offset_Level2        */		     200,	/* Comment [ Large beta dot level for detecting small sensor offset ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_DFC_Ay_Model_Gain                 */		     100,	/* Comment [ Weighting factor of Ay model for detecting bank road ] */
	/* int16_t     	S16_DFC_Yaw_Model_Gain                */		     100,	/* Comment [ Weighting factor of yaw model for detecting bank road ] */
	/* int16_t     	S16_Bank_FastSteer_Bank_Dot_Gain      */		       0,	/* Comment [ Bank dot gain for calculating bank angle at fast steer range after bank detection ] */
	/* int16_t     	S16_Bank_FastSteer_DFC_Dot_Gain       */		       0,	/* Comment [ DFC dot gain for calculating bank angle at fast steer range after bank detection ] */
	/* int16_t     	S16_Bank_FastSteer_DFC_Gain           */		      20,	/* Comment [ DFC gain for calculating bank angle at fast steer range after bank detection ] */
	/* int16_t     	S16_Bank_MedSteer_Bank_Dot_Gain       */		       0,	/* Comment [ Bank dot gain for calculating bank angle at medium-rate steer range after bank detection ] */
	/* int16_t     	S16_Bank_MedSteer_DFC_Dot_Gain        */		      10,	/* Comment [ DFC dot gain for calculating bank angle at medium-rate steer range after bank detection ] */
	/* int16_t     	S16_Bank_MedSteer_DFC_Gain            */		      20,	/* Comment [ DFC gain for calculating bank angle at medium-rate steer range after bank detection ] */
	/* int16_t     	S16_Bank_SlowSteer_Bank_Dot_Gain      */		      10,	/* Comment [ Bank dot gain for calculating bank angle at slow steer range after bank detection ] */
	/* int16_t     	S16_Bank_SlowSteer_DFC_Dot_Gain       */		      10,	/* Comment [ DFC dot gain for calculating bank angle at slow steer range after bank detection ] */
	/* int16_t     	S16_Bank_SlowSteer_DFC_Gain           */		      20,	/* Comment [ DFC gain for calculating bank angle at slow steer range after bank detection ] */
	/* int16_t     	S16_FastSteer_Bank_Dot_Gain           */		      10,	/* Comment [ Bank dot gain for calculating bank angle at fast steer range ] */
	/* int16_t     	S16_FastSteer_DFC_Dot_Gain            */		      20,	/* Comment [ DFC dot gain for calculating bank angle at fast steer range ] */
	/* int16_t     	S16_FastSteer_DFC_Gain                */		      30,	/* Comment [ DFC gain for calculating bank angle at fast steer range ] */
	/* int16_t     	S16_MedSteer_Bank_Dot_Gain            */		      20,	/* Comment [ Bank dot gain for calculating bank angle at medium-rate steer range ] */
	/* int16_t     	S16_MedSteer_DFC_Dot_Gain             */		      30,	/* Comment [ DFC dot gain for calculating bank angle at medium-rate steer range ] */
	/* int16_t     	S16_MedSteer_DFC_Gain                 */		      20,	/* Comment [ DFC gain for calculating bank angle at medium-rate steer range ] */
	/* int16_t     	S16_SlowSteer_Bank_Dot_Gain           */		      50,	/* Comment [ Bank dot gain for calculating bank angle at slow steer range ] */
	/* int16_t     	S16_SlowSteer_DFC_Dot_Gain            */		      30,	/* Comment [ DFC dot gain for calculating bank angle at slow steer range ] */
	/* int16_t     	S16_SlowSteer_DFC_Gain                */		      20,	/* Comment [ DFC gain for calculating bank angle at slow steer range ] */
	/* int16_t     	S16_Bank_High_Speed                   */		    1120,	/* Comment [ Minimum value of high speed range for bank detection ] */
	                                                        		        	/* ScaleVal[   140 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Bank_High_Speed_Gain              */		      50,	/* Comment [ Weighting factor for calculating bank angle at high speed range ] */
	/* int16_t     	S16_Bank_MedLow_Speed                 */		     960,	/* Comment [ Maximum value of medium speed range for bank detection ] */
	                                                        		        	/* ScaleVal[   120 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Bank_Detection_Time_Ref           */		      50,	/* Comment [ Bank detection time // Default : 50 -> 500ms ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Detect_Thres2                */		    1000,	/* Comment [ Bank angle threshold for prevent ESC from activating ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Detect_Thres_LowSpeed2       */		    2000,	/* Comment [ Bank angle threshold for prevent ESC from activating at low speed range ] */
	                                                        		        	/* ScaleVal[     20 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Exit_Thres2                  */		     500,	/* Comment [ Bank angle threshold for releasing bank detection ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Exit_Thres_LowSpeed2         */		    1000,	/* Comment [ Bank angle threshold for releasing bank detection at low speed range ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Hold_Fast_Str_Thres          */		    5000,	/* Comment [ Min Wstr_ dot (to hold bank state / when bank suspect flag=1) // Default : 5000 -> 500deg/sec ] */
	                                                        		        	/* ScaleVal[ 500 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BANK_K_DETECT_ALAT                */		     600,	/* Comment [ Min det_alatm (to detect bank) // Default : LAT_0G6G = 600 -> 0.6g ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BANK_K_DETECT_BANK_K              */		    1000,	/* Comment [ Min Bank_Angle_K (to detect bank) // Default : BANK_ANGLE_10DEG = 1000 -> 10deg ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_K_DETECT_BANK_K_DOT          */		    1500,	/* Comment [ Min Bank_angle_k_dot (to detect bank) // Default : BANK_ANGLE_DOT_15DPS = 1500 -> 15deg/sec ] */
	                                                        		        	/* ScaleVal[ 15 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_K_DETECT_DYN_F               */		     200,	/* Comment [ Max Dynamic_Factor (to detect bank) // Default : 200 ] */
	/* int16_t     	S16_BANK_K_DETECT_VREF                */		     880,	/* Comment [ Max Vref (to detect bank) // Default : VREF_110_KPH = 880 -> 110KPH ] */
	                                                        		        	/* ScaleVal[    110 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BANK_K_DETECT_WSTR_DOT            */		    1000,	/* Comment [ Max Wstr_dot(to detect bank) // Default : WSTR_DOT_1_10_100DPS = 1000 -> 100deg/sec ] */
	                                                        		        	/* ScaleVal[ 100 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_BANK_K_EXIT_ALAT                  */		     580,	/* Comment [ Max_alatm (to exit bank) // Default : 580 -> 0.58g ] */
	                                                        		        	/* ScaleVal[     0.58 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_BANK_K_EXIT_BANK_K                */		     600,	/* Comment [ Max Bank_Angle_K (to exit bank) // Default : BANK_ANGLE_6DEG = 600 -> 6deg ] */
	                                                        		        	/* ScaleVal[      6 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_K_EXIT_BANK_K_DOT            */		    1500,	/* Comment [ Max Bank_angle_k_dot (to exit bank) // Default : BANK_ANGLE_DOT_15DPS = 1500 -> 15deg/sec ] */
	                                                        		        	/* ScaleVal[ 15 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_K_EXIT_DYN_F                 */		    1000,	/* Comment [ Min Dynamic_Factor (to exit bank) // Default : 1000 ] */
	/* int16_t     	S16_BANK_K_EXIT_VREF                  */		     880,	/* Comment [ Min Vref (to exit bank) // Default : VREF_110_KPH = 880 -> 110KPH ] */
	                                                        		        	/* ScaleVal[    110 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BANK_K_EXIT_WSTR_DOT              */		    1000,	/* Comment [ Min Wstr_dot (to exit bank) // Default : WSTR_DOT_1_10_100DPS = 1000 -> 100deg/sec ] */
	                                                        		        	/* ScaleVal[ 100 deg/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Bank_Reset_Time                   */		     100,	/* Comment [ Bank reset time // Default : 100 -> 1000ms ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Suspect_Thres2               */		     700,	/* Comment [ Bank angle threshold for detecting bank suspect ] */
	                                                        		        	/* ScaleVal[      7 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Suspect_Thres_LowSpeed2      */		    1200,	/* Comment [ Bank angle threshold for detecting bank suspect at low speed range ] */
	                                                        		        	/* ScaleVal[     12 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Detect_Thres                 */		    1000,	/* Comment [ Bank angle threshold for bank detection of beta model ] */
	                                                        		        	/* ScaleVal[     10 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Detect_Thres_LowSpeed        */		    1500,	/* Comment [ Bank angle threshold for bank detection of beta model at low speed range ] */
	                                                        		        	/* ScaleVal[     15 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Exit_Thres                   */		     300,	/* Comment [ Bank angle threshold for releasing bank detection of beta model ] */
	                                                        		        	/* ScaleVal[      3 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Exit_Thres_LowSpeed          */		     500,	/* Comment [ Bank angle threshold for releasing bank detection of beta model at low speed range ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Suspect_Thres                */		     500,	/* Comment [ Bank angle threshold for compensating estimated beta ] */
	                                                        		        	/* ScaleVal[      5 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Suspect_Thres_LowSpeed       */		     700,	/* Comment [ Bank angle threshold for compensating estimated beta at low speed range ] */
	                                                        		        	/* ScaleVal[      7 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Det_Thres_Drv_Big_Slip1           */		      80,	/* Comment [ Big drive slip level of one wheel for detecting saturated road adhesion ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Det_Thres_Drv_Big_Slip2           */		      64,	/* Comment [ Big drive slip level of two wheels for detecting saturated road adhesion ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Det_Thres_Drv_Slip1               */		      40,	/* Comment [ Drive slip level of one wheel for detecting saturated road adhesion ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Det_Thres_Drv_Slip2               */		      32,	/* Comment [ Drive slip level of two wheels for detecting saturated road adhesion ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_Reset_Thres_Drv_Slip              */		      16,	/* Comment [ Drive slip level for releasing saturated road condition ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S8_SLIP_MU_MIN_SLIP_THRES             */		      15,	/* Comment [ Min. slip level of front wheel by ESC control for detection of road mu level ] */
	/* uchar8_t    	U8_SLIP_MU_FLAG_RESET_TIME            */		      10,	/* Comment [ Max. hold time of detected value after detection of road mu level ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_SLIP_MU_LEVEL_GAIN                 */		      40,
	/* uchar8_t    	U8_SLIP_MU_MIN_PRESS                  */		      20,	/* Comment [ Min. pressure level of front wheel's ESC control for detection of road mu level ] */
	/* uchar8_t    	U8_SLIP_MU_MIN_RISE_TIME              */		      20,	/* Comment [ Min. rise mode time of front wheel's ESC control for detection of road mu level ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_SLIP_MU_RESET_RATE                 */		      50,	/* Comment [ Reset rate per 1scan of calculated mu ] */
	                                                        		        	/* ScaleVal[ 0.05 g/scan ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* int16_t     	S16_BANK_CTRL_INS_D_WEG               */		      20,	/* Comment [ FMVSS 126 적용에 따른 Bank 노면 ESC 제어 허용시, Yaw_Control Inside D-gain 에 대한 가중치 설정 0~100, 작을수록 D-gain 크기 작아짐 ] */
	/* int16_t     	S16_Bank_Detect_L_Yaw_Thres2          */		     200,	/* Comment [ Additional Del_Yaw_Thres at S16_Bank_Detect_Thres2 under Bank Condition, default=2 ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Detect_U_Yaw_Thres2          */		     600,	/* Comment [ Additional Del_Yaw_Thres at (S16_Bank_Detect_Thres2)*3 under Bank Condition, default=6 ] */
	                                                        		        	/* ScaleVal[    6 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Lower_Suspect_Thres2         */		     300,	/* Comment [ Bank Suspect Lower Threshold, default=3 ] */
	                                                        		        	/* ScaleVal[      3 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Lower_Suspect_Thres_LowSpeed2 */		     800,	/* Comment [ Lower Suspect threshold at Low Speed // default 800 -> 8deg ] */
	                                                        		        	/* ScaleVal[      8 deg ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_LOWER_SUSPECT_TIME           */		      50,	/* Comment [ Lower Suspect set time // default 50 -> 0.5s ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Bank_Suspect_Yaw_Thres2           */		     100,	/* Comment [ Additional Del_Yaw_Thres at S16_Bank_Suspect_Thres2 under Bank Condition, default=1 ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_YAW_D_GAIN_WEG               */		      50,	/* Comment [ At Bank-road detection, weight of Outside D-gain ] */
	/* int16_t     	S16_BANK_YAW_LMT_GAIN_WEG             */		     150,	/* Comment [ At Bank-road detection, weight of yaw-limit level ] */
	/* int16_t     	S16_BANK_YAW_P_GAIN_WEG               */		      50,	/* Comment [ At Bank-road detection, weight of Outside P-gain ] */
	/* char8_t     	S8_BANK_LOWER_SUSPECT_TIME_1ST_ADD    */		       1,	/* Comment [ Lower Suspect 1st grade add counter // default 1 ] */
	/* char8_t     	S8_BANK_LOWER_SUSPECT_TIME_2ND_ADD    */		       2,	/* Comment [ Lower Suspect 2nd grade add counter // default 2 ] */
	/* char8_t     	S8_BANK_LOWER_SUSPECT_TIME_3RD_ADD    */		       3,	/* Comment [ Lower Suspect 3rd grade add counter // default 3 ] */
	/* char8_t     	S8_BANK_LOWER_SUSPECT_TIME_4TH_ADD    */		       4,	/* Comment [ Lower Suspect 4th grade add counter // default 4 ] */
	/* char8_t     	S8_BANK_LOWER_SUSPECT_TIME_SUB        */		       2,	/* Comment [ Lower Suspect sub counter // default 2 ] */
	/* int16_t     	S16_AY_DOT_CHK_THR                    */		     300,	/* Comment [ Stable state threshold of ay dot (jerk)// default 300 ] */
	/* int16_t     	S16_BANK_CHK_VALID_TIME               */		      50,	/* Comment [ Bank detection time threshold on ESC activation condition  // default 50 -> 0.5s ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_K_BETADOT_COMP_VELO          */		     800,	/* Comment [ Beta dot compensation velocity // default 800 -> 100kph ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BANK_STABLE_TIME                  */		      50,	/* Comment [ Bank stable time to reduce DFC factor // default 50 -> 0.5s ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_BANK_VALID_PRESSURE               */		     500,	/* Comment [ Bank detection pressure threshold on ESC activation condition // default 500 -> 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DFC_CHK_THR                       */		     200,	/* Comment [ Valid Bank model DFC threshold // default 200 ] */
	/* int16_t     	S16_LATERALDIFF_CHK_THR               */		     200,	/* Comment [ Difference threshold between ay dot and v-yaw dot // default 200 ] */
	/* int16_t     	S16_ROAD_MU_LAT_CHK_THR               */		     700,	/* Comment [ BANK high mu threshold calculated by lateral acceleration// default 700 -> 0.7g ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ROAD_MU_YAW_CHK_THR               */		     500,	/* Comment [ BANK high mu threshold calculated by yawrate // default 500 -> 0.5g ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_VYAW_DOT_CHK_THR                  */		     300,	/* Comment [ Stable state threshold of (v*yaw) dot (jerk)// default 300 ] */
	/* int16_t     	SLIP_F_DUMP_THRESHOLD                 */		      50,	/* Comment [ Front High mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_F_HOLD_THRESHOLD                 */		     200,	/* Comment [ Front High mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_F_RISE_THRESHOLD                 */		     210,	/* Comment [ Front High mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_DUMP_THRESHOLD                 */		      50,	/* Comment [ Rear High mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_R_HOLD_THRESHOLD                 */		     200,	/* Comment [ Rear High mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_RISE_THRESHOLD                 */		     200,	/* Comment [ Rear High mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_F_DUMP_THRESHOLD_M               */		      80,	/* Comment [ Front Med mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_F_HOLD_THRESHOLD_M               */		     200,	/* Comment [ Front Med mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_F_RISE_THRESHOLD_M               */		     250,	/* Comment [ Front Med mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_DUMP_THRESHOLD_M               */		      50,	/* Comment [ Rear Med mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_R_HOLD_THRESHOLD_M               */		     200,	/* Comment [ Rear Med mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_RISE_THRESHOLD_M               */		     200,	/* Comment [ Rear Med mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_F_DUMP_THRESHOLD_L               */		      50,	/* Comment [ Front Low mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_F_HOLD_THRESHOLD_L               */		     200,	/* Comment [ Front Low mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_F_RISE_THRESHOLD_L               */		     250,	/* Comment [ Front Low mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_DUMP_THRESHOLD_L               */		      50,	/* Comment [ Rear Low mu,  U_sc 값이 이값 이하이면 dump. RISE와 DUMP 사이 값은 HOLD임 ] */
	/* int16_t     	SLIP_R_HOLD_THRESHOLD_L               */		     200,	/* Comment [ Rear Low mu,  Wheel이 살 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	SLIP_R_RISE_THRESHOLD_L               */		     200,	/* Comment [ Rear Low mu,  Wheel slip이 증가할 때,  U_sc 값이 이값 이상이면 Rise ] */
	/* int16_t     	FULL_RISE_ENTER_TAR_SLIP              */		    2500,	/* Comment [ Full Dump detect 시 Target Slip 값 ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	FULL_RISE_ENTER_YAW_ERR_D             */		      50,	/* Comment [ Full Dump detect 시 yaw_err_dot 기울기. Tuning 값 이상 시 감지 ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_WEG_RTA_FRONT_80KPH_H_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), High mu. ] */
	/* int16_t     	S16_WEG_RTA_FRONT_80KPH_L_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), Low mu. ] */
	/* int16_t     	S16_WEG_RTA_FRONT_80KPH_M_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), Medium mu. ] */
	/* int16_t     	S16_WEG_RTA_FRONT_20KPH_H_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), High mu. ] */
	/* int16_t     	S16_WEG_RTA_FRONT_20KPH_L_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), Low mu. ] */
	/* int16_t     	S16_WEG_RTA_FRONT_20KPH_M_MU          */		     100,	/* Comment [ Front mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), Medium mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_80KPH_H_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), High mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_80KPH_L_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), Low mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_80KPH_M_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 고속(80kph), Medium mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_20KPH_H_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), High mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_20KPH_L_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), Low mu. ] */
	/* int16_t     	S16_WEG_RTA_REAR_20KPH_M_MU           */		     100,	/* Comment [ Rear mini tire 감지시 DelM(control moment) 계산의 Gain값 변화(%), default = 100. 저속(20kph), Medium mu. ] */
	/* int16_t     	S16_DEL_M_H_SPEED                     */		    1100,	/* Comment [ High speed range in del_M enter, exit condition ] */
	                                                        		        	/* ScaleVal[   110 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DEL_M_L_SPEED                     */		     300,	/* Comment [ Low speed range in del_M enter, exit condition ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DEL_M_M_SPEED                     */		     700,	/* Comment [ Med speed range in del_M enter, exit condition ] */
	                                                        		        	/* ScaleVal[    70 km/h ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_HS        */		       9,	/* Comment [ del_M enter front threshold in high mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_LS        */		       9,	/* Comment [ del_M enter front threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_MS        */		       9,	/* Comment [ del_M enter front threshold in high mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_HS         */		       9,	/* Comment [ del_M enter rear threshold in high mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_LS         */		       9,	/* Comment [ del_M enter rear threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_MS         */		       9,	/* Comment [ del_M enter rearthreshold in high mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_HS_ABS    */		       9,	/* Comment [ del_M enter front threshold in high mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_LS_ABS    */		       9,	/* Comment [ del_M enter front threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_HM_MS_ABS    */		       9,	/* Comment [ del_M enter front threshold in high mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_HS_ABS     */		       9,	/* Comment [ del_M enter rear threshold in high mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_LS_ABS     */		       9,	/* Comment [ del_M enter rear threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_HM_MS_ABS     */		       9,	/* Comment [ del_M enter rearthreshold in high mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_HS        */		       9,	/* Comment [ del_M enter front threshold in low mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_LS        */		       9,	/* Comment [ del_M enter front threshold in low mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_MS        */		       9,	/* Comment [ del_M enter front threshold in low mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_HS         */		       7,	/* Comment [ del_M enter rear threshold in low mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_LS         */		       7,	/* Comment [ del_M enter rear threshold in low mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_MS         */		       7,	/* Comment [ del_M enter rear threshold in low mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_HS_ABS    */		       9,	/* Comment [ del_M enter front threshold in low mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_LS_ABS    */		       9,	/* Comment [ del_M enter front threshold in low mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_LM_MS_ABS    */		       9,	/* Comment [ del_M enter front threshold in low mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_HS_ABS     */		       7,	/* Comment [ del_M enter rear threshold in low mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_LS_ABS     */		       7,	/* Comment [ del_M enter rear threshold in low mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_LM_MS_ABS     */		       7,	/* Comment [ del_M enter rear threshold in low mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_HS        */		       9,	/* Comment [ del_M enter front threshold in med mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_LS        */		       9,	/* Comment [ del_M enter front threshold in med mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_MS        */		       9,	/* Comment [ del_M enter front threshold in med mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_HS         */		       8,	/* Comment [ del_M enter rear threshold in med mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_LS         */		       8,	/* Comment [ del_M enter rear threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_MS         */		       8,	/* Comment [ del_M enter rear threshold in med mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_HS_ABS    */		       9,	/* Comment [ del_M enter front threshold in med mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_LS_ABS    */		       9,	/* Comment [ del_M enter front threshold in med mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_FRONT_MM_MS_ABS    */		       9,	/* Comment [ del_M enter front threshold in med mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_HS_ABS     */		       8,	/* Comment [ del_M enter rear threshold in med mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_LS_ABS     */		       8,	/* Comment [ del_M enter rear threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_ENTER_REAR_MM_MS_ABS     */		       8,	/* Comment [ del_M enter rear threshold in med mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_HS         */		       4,	/* Comment [ del_M exit front threshold in high mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_LS         */		       4,	/* Comment [ del_M exit front threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_MS         */		       4,	/* Comment [ del_M exit front threshold in high mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_HS          */		       5,	/* Comment [ del_M exit rear threshold in high mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_LS          */		       5,	/* Comment [ del_M exit rear threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_MS          */		       5,	/* Comment [ del_M exit rearthreshold in high mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_HS_ABS     */		       4,	/* Comment [ del_M exit front threshold in high mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_LS_ABS     */		       4,	/* Comment [ del_M exit front threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_HM_MS_ABS     */		       4,	/* Comment [ del_M exit front threshold in high mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_HS_ABS      */		       5,	/* Comment [ del_M exit rear threshold in high mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_LS_ABS      */		       5,	/* Comment [ del_M exit rear threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_HM_MS_ABS      */		       5,	/* Comment [ del_M exit rearthreshold in high mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_HS         */		       4,	/* Comment [ del_M exitr front threshold in low mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_LS         */		       4,	/* Comment [ del_M exit front threshold in low mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_MS         */		       4,	/* Comment [ del_M exit front threshold in low mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_HS          */		       4,	/* Comment [ del_M exit rear threshold in low mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_LS          */		       4,	/* Comment [ del_M exit rear threshold in low mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_MS          */		       4,	/* Comment [ del_M exit rear threshold in low mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_HS_ABS     */		       4,	/* Comment [ del_M exitr front threshold in low mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_LS_ABS     */		       4,	/* Comment [ del_M exit front threshold in low mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_LM_MS_ABS     */		       4,	/* Comment [ del_M exit front threshold in low mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_HS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in low mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_LS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in low mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_LM_MS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in low mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_HS         */		       4,	/* Comment [ del_M exit front threshold in med mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_LS         */		       4,	/* Comment [ del_M exit front threshold in med mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_MS         */		       4,	/* Comment [ del_M exit front threshold in med mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_HS          */		       4,	/* Comment [ del_M exit rear threshold in med mu and high speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_LS          */		       4,	/* Comment [ del_M exit rear threshold in high mu and low speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_MS          */		       4,	/* Comment [ del_M exit rear threshold in med mu and med speed condition ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_HS_ABS     */		       4,	/* Comment [ del_M exit front threshold in med mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_LS_ABS     */		       4,	/* Comment [ del_M exit front threshold in med mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_FRONT_MM_MS_ABS     */		       4,	/* Comment [ del_M exit front threshold in med mu and med speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_HS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in med mu and high speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_LS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in high mu and low speed condition when ABS's operating ] */
	/* uchar8_t    	U8_DEL_M_THR_EXIT_REAR_MM_MS_ABS      */		       4,	/* Comment [ del_M exit rear threshold in med mu and med speed condition when ABS's operating ] */
	/* int16_t     	S16_VARIABLE_RISE_SCAN_START_H_R      */		      10,	/* Comment [ Full rise time of rear drum in initial pressure @ High mu (default 10) ] */
	/* int16_t     	S16_VARIABLE_RISE_SCAN_START_M_R      */		      10,	/* Comment [ Full rise time of rear drum in initial pressure @ Med mu (default 10) ] */
	/* int16_t     	K_Brake_Boost_BASS_grad_Threshold     */		       1,	/* Comment [ HBA 진입하기 위한 Pedal Sensor 변화값 ] */
	                                                        		        	/* ScaleVal[    32.68 % ]	Scale [ f(x) = (X + 0) * 32.68 ]  Range   [ 0 <= X <= 272332.244 ] */
	/* int16_t     	K_Brake_Boost_MSP_Rel_Threshold       */		      45,	/* Comment [ HBA exit pressure ] */
	/* int16_t     	K_Brake_Boost_MSP_Threshold           */		     250,	/* Comment [ HBA enter pressure ] */
	/* int16_t     	K_Brake_Boost_Rel_Pos_Threshold       */		      30,	/* Comment [ HBA Pedal Position페달 포지션 ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 0.5 ]  Range   [ 0 <= X <= 50 ] */
	/* uint16_t    	U16_PBA_ESP_MU_HIGH                   */		     650,	/* Comment [ PBA 제어를 시작하는 High mu level (0이면 제어에 사용안함) ] */
	                                                        		        	/* ScaleVal[      6.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_PBA_ESP_MU_LOW                    */		     450,	/* Comment [ PBA 제어를 유보하는 Low mu level (0이면 제어에 사용안함) ] */
	                                                        		        	/* ScaleVal[      4.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_PBA_ESP_MU_MEDIUM                 */		     600,	/* Comment [ PBA 제어를 유보하는 Medium mu level (0이면 제어에 사용안함) ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_PBA_MOTOR_ON_TIME                 */		       4,	/* Comment [ PBA motor PWM 제어 변수 (10 scan 중에 motor 구동 scan) ] */
	/* uint16_t    	U16_PBA_PRESS_ENTER_THR               */		     400,	/* Comment [ PBA enter pressure (이 압력이상이 되야 PBA 제어 시작함) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_PBA_PRESS_RISE_RATE_THR1          */		     160,	/* Comment [ PBA enter slope (급제동 판단에 사용됨) ] */
	                                                        		        	/* ScaleVal[ 16 bar/4scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_PBA_PRESS_RISE_RATE_THR2          */		      20,	/* Comment [ PBA enter slope (Panic 상황 판단에 사용됨) ] */
	                                                        		        	/* ScaleVal[ 2 bar/4scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_PBA_SPEED_ENTER_THR               */		     240,	/* Comment [ PBA enter velocity (이 속도 이상이 되야 PBA 제어 시작함) ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* int16_t     	S16_PBA_PRESS_EXIT_THR3               */		     300,	/* Comment [ If MCP stay under this for TBD sec, PBA exit (default : 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PBA_PRESS_EXIT_THR3_TIMER         */		      30,	/* Comment [ If MCP stay under TBD for this sec, PBA exit (default : 0.3sec) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_PBA_PRESS_EXIT_THR1               */		     100,	/* Comment [ PBA exit pressure (LOW Limit, 이 압력 미만이면 제어 종료) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_PBA_PRESS_EXIT_THR2               */		    1000,	/* Comment [ PBA exit pressure (HIGH Limit, 이 압력 초과하면 제어 종료) ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_PBA_SPEED_EXIT_THR                */		      80,	/* Comment [ PBA exit velocity (이 속도 미만에서 제어 종료) ] */
	                                                        		        	/* ScaleVal[    10 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* int16_t     	S16_PBA_TARGET_VOLT                   */		    4000,	/* Comment [ Target mot voltage when PBA engage (default : 4v) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_PBA_TARGET_VOLT_ABS               */		    3500,	/* Comment [ Target mot voltage @ ABS when PBA engage (default : 3.5v) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_PBA_FULL_MOTOR_ACT_TIME           */		      20,	/* Comment [ Initial Motor Full Driving Time (default : 0.2sec) ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_PBA_PRE_ABS_ENTER_WHL_SLIP        */		     300,	/* Comment [ Wheel slip thr for Pre-ABS mode.(pulse-up rise) (def: 3%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_PBA_PRE_ABS_ENTER_DECEL            */		      70,	/* Comment [ Decel thr condition for Pre-ABS mode (pulse-up rise) (default: 0.7g) ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	PBA_DUMP_MODE_HOLD_TIME               */		       5,	/* Comment [ Hold Time during dump mode with BA_DUMP_PATTERN macro (default = 50msec) ] */
	                                                        		        	/* ScaleVal[    50 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	PBA_DUMP_MODE_MAX_TIME                */		      10,	/* Comment [ Max. dump and hold mode time with BA_DUMP_PATTERN macro (default = 100msec) ] */
	                                                        		        	/* ScaleVal[   100 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	PBA_EXIT_CONTROL_TIME                 */		      50,	/* Comment [ Fade-out mode time after PBA exit (default = 500ms) ] */
	                                                        		        	/* ScaleVal[   500 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_NC_Dump_Max_Press_f               */		      40,	/* Comment [ NC Valve에 의한 Wheel Press Dump시 Max 값 [default:4.0bar] ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Max_Press_r               */		      40,	/* Comment [ NC Valve에 의한 Wheel Press Dump시 Max 값 [default:4.0bar] ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NO_Dump_Max_Press_f               */		      60,	/* Comment [ NO Valve에 의한 Wheel Press Dump시 Max 값 [default:6.0bar] ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NO_Dump_Max_Press_r               */		      60,	/* Comment [ NO Valve에 의한 Wheel Press Dump시 Max 값 [default:6.0bar] ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NO_Rise_Max_Press_f               */		      70,	/* Comment [ NO Valve에 의한 Wheel Press Rise시 Max 값 [default:7.0bar] ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NO_Rise_Max_Press_r               */		      60,	/* Comment [ NO Valve에 의한 Wheel Press Rise시 Max 값 [default:7.0bar] ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCMF_Max_Press                    */		    1350,	/* Comment [ TCMF 사양 HW에서 최대 Hold 가능한 Max 압력 값  [default:135.0bar] ] */
	                                                        		        	/* ScaleVal[    135 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_ESC_FIRST_ON_TIME_GAIN_f           */		     100,	/* Comment [ 1st cycle control compensation_front @ Motor Rise Mode, default : 100% ] */
	/* uchar8_t    	U8_ESC_FIRST_ON_TIME_GAIN_r           */		     100,	/* Comment [ 1st cycle control compensation_front @ Motor Rise Mode, default : 100% ] */
	/* uchar8_t    	U8_ESC_PULSE_UP_RISE_GAIN_f           */		     100,	/* Comment [ pulse up rise mode compensation_front @ Motor Rise Mode, default : 100% ] */
	/* uchar8_t    	U8_ESC_PULSE_UP_RISE_GAIN_r           */		     100,	/* Comment [ pulse up rise mode compensation_rear @ Motor Rise Mode, default : 100% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_1st                */		      20,	/* Comment [ Master Press에 의한 Rise Rate Gain set 1st [default:20%] ] */
	/* int16_t     	S16_MP_Rise_Gain_f_2nd                */		      40,	/* Comment [ Master Press에 의한 Rise Rate Gain set 2nd [default:40%] ] */
	/* int16_t     	S16_MP_Rise_Gain_f_3rd                */		      75,	/* Comment [ Master Press에 의한 Rise Rate Gain set 3rd [default:75%] ] */
	/* int16_t     	S16_MP_Rise_Gain_f_4th                */		     100,	/* Comment [ Master Press에 의한 Rise Rate Gain set 4th [default:100%] ] */
	/* int16_t     	S16_MP_Rise_Gain_f_5th                */		      90,	/* Comment [ Master Press에 의한 Rise Rate Gain set 5th [default:90%] ] */
	/* int16_t     	S16_MP_Rise_Gain_f_6th                */		      90,	/* Comment [ Master Press에 의한 Rise Rate Gain set 6th [default:90%] ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_1st          */		       0,	/* Comment [ Master Press에 의한 Rise Rate Gain 1st에 해당하는 Wheel Press [default:0bar] ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_2nd          */		      75,	/* Comment [ Master Press에 의한 Rise Rate Gain 2nd에 해당하는 Wheel Press [default:7.5bar] ] */
	                                                        		        	/* ScaleVal[    7.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_3rd          */		     400,	/* Comment [ Master Press에 의한 Rise Rate Gain 3rd에 해당하는 Wheel Press [default:40.0bar] ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_4th          */		     600,	/* Comment [ Master Press에 의한 Rise Rate Gain 4th에 해당하는 Wheel Press [default:60.0bar] ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_5th          */		    1330,	/* Comment [ Master Press에 의한 Rise Rate Gain 5th에 해당하는 Wheel Press [default:133.0bar] ] */
	                                                        		        	/* ScaleVal[    133 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_f_6th          */		    1500,	/* Comment [ Master Press에 의한 Rise Rate Gain 6th에 해당하는 Wheel Press [default:150.0bar] ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_1st          */		      25,	/* Comment [ Master Press에 의한 Rise Rate Gain 1st에 해당하는 Wheel Press [default:2.5bar] ] */
	                                                        		        	/* ScaleVal[    2.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_2nd          */		     100,	/* Comment [ Master Press에 의한 Rise Rate Gain 2nd에 해당하는 Wheel Press [default:10.0bar] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_3rd          */		     400,	/* Comment [ Master Press에 의한 Rise Rate Gain 3rd에 해당하는 Wheel Press [default:40.0bar] ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_4th          */		     800,	/* Comment [ Master Press에 의한 Rise Rate Gain 4th에 해당하는 Wheel Press [default:80.0bar] ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_5th          */		    1400,	/* Comment [ Master Press에 의한 Rise Rate Gain 5th에 해당하는 Wheel Press [default:140.0bar] ] */
	                                                        		        	/* ScaleVal[    140 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_Press_r_6th          */		    1500,	/* Comment [ Master Press에 의한 Rise Rate Gain 6th에 해당하는 Wheel Press [default:150.0bar] ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_r_1st                */		      20,	/* Comment [ Master Press에 의한 Rise Rate Gain set 1st [default:20%] ] */
	/* int16_t     	S16_MP_Rise_Gain_r_2nd                */		      40,	/* Comment [ Master Press에 의한 Rise Rate Gain set 2nd [default:40%] ] */
	/* int16_t     	S16_MP_Rise_Gain_r_3rd                */		     100,	/* Comment [ Master Press에 의한 Rise Rate Gain set 3rd [default:100%] ] */
	/* int16_t     	S16_MP_Rise_Gain_r_4th                */		     120,	/* Comment [ Master Press에 의한 Rise Rate Gain set 4th [default:120%] ] */
	/* int16_t     	S16_MP_Rise_Gain_r_5th                */		     130,	/* Comment [ Master Press에 의한 Rise Rate Gain set 5th [default:130%] ] */
	/* int16_t     	S16_MP_Rise_Gain_r_6th                */		     140,	/* Comment [ Master Press에 의한 Rise Rate Gain set 6th [default:140%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_1st             */		       8,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 1st - 6Volt Motor 구동 기준치 [default:5%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_2nd             */		      22,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 2nd - 6Volt Motor 구동 기준치 [default:14%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_3rd             */		      35,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 3rd - 6Volt Motor 구동 기준치 [default:25%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_4th             */		      53,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 4th - 6Volt Motor 구동 기준치 [default:37%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_5th             */		      83,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 5th - 6Volt Motor 구동 기준치 [default:67%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_f_6th             */		     120,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 6th - 6Volt Motor 구동 기준치 [default:100%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_1st       */		       0,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 1st에 해당하는 Wheel Press [default:0.0bar] ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_2nd       */		      25,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 2nd에 해당하는 Wheel Press [default:2.5bar] ] */
	                                                        		        	/* ScaleVal[    2.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_3rd       */		      50,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 3rd에 해당하는 Wheel Press [default:5.0bar] ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_4th       */		     100,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 4th에 해당하는 Wheel Press [default:10.0bar] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_5th       */		     300,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 5th에 해당하는 Wheel Press [default:30.0bar] ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_f_6th       */		     900,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 6th에 해당하는 Wheel Press [default:70.0bar] ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Rate_Max_f             */		    2900,	/* Comment [ Motor에 의한 최대 Rise Rate Front 기준 [6 Volt Test Mode기준] [default:250.0bar/sec] ] */
	                                                        		        	/* ScaleVal[    290 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_1st       */		       0,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 1st에 해당하는 Wheel Press [default:0.0bar] ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_2nd       */		      25,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 2nd에 해당하는 Wheel Press [default:2.5bar] ] */
	                                                        		        	/* ScaleVal[    2.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_3rd       */		      50,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 3rd에 해당하는 Wheel Press [default:5.0bar] ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_4th       */		     100,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 4th에 해당하는 Wheel Press [default:10.0bar] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_5th       */		     300,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 5th에 해당하는 Wheel Press [default:30.0bar] ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_Press_r_6th       */		     900,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 6th에 해당하는 Wheel Press [default:70.0bar] ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_1st             */		      10,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 1st - 6Volt Motor 구동 기준치 [default:10%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_2nd             */		      30,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 2nd - 6Volt Motor 구동 기준치 [default:28%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_3rd             */		      65,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 3rd - 6Volt Motor 구동 기준치 [default:50%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_4th             */		     100,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 4th - 6Volt Motor 구동 기준치 [default:80%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_5th             */		     180,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 5th - 6Volt Motor 구동 기준치 [default:150%] ] */
	/* int16_t     	S16_Motor_Rise_Gain_r_6th             */		     180,	/* Comment [ Motor Pumping에 의한 Rise Rate Gain set 6th - 6Volt Motor 구동 기준치 [default:180%] ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Before_AFZ_OK_f */		    3000,	/* Comment [ AFZ OK 이전에서 ABS LFC Mode에서 Rise Rate [default:300.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 300 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Before_AFZ_OK_r */		    3000,	/* Comment [ AFZ OK 이전에서 ABS LFC Mode에서 Rise Rate [default:300.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 300 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_High_mu_f       */		    1500,	/* Comment [ ABS LFC Mode에서 Rise Rate High Mu [default:150.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 150 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_High_mu_r       */		    1500,	/* Comment [ ABS LFC Mode에서 Rise Rate High Mu [default:150.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 150 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Low_mu_f        */		     500,	/* Comment [ ABS LFC Mode에서 Rise Rate Low Mu [default:50.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 50 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Low_mu_r        */		     500,	/* Comment [ ABS LFC Mode에서 Rise Rate Low Mu [default:50.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 50 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Medium_mu_f     */		    1200,	/* Comment [ ABS LFC Mode에서 Rise Rate Med Mu [default:150.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 120 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_LFC_RISE_RATE_Medium_mu_r     */		    1200,	/* Comment [ ABS LFC Mode에서 Rise Rate Med Mu [default:150.0bar/sec] ] */
	                                                        		        	/* ScaleVal[ 120 bar/sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_f_1st        */		      20,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 1st Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_f_2nd        */		      50,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 2nd Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_f_3rd        */		      80,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 3rd Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_r_1st        */		      20,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 1st Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_r_2nd        */		      50,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 2nd Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainDuty_r_3rd        */		      80,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 3rd Duty 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainf_1st             */		     100,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 1st Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainf_2nd             */		      40,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 2nd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainf_3rd             */		       0,	/* Comment [ [Front Rise Gain 설정] ESC MP LFC 증압조건에서, 3rd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainr_1st             */		     100,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 1st Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainr_2nd             */		      40,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 2nd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMPRiseGainr_3rd             */		       0,	/* Comment [ [Rear Rise Gain 설정] ESC MP LFC 증압조건에서, 3rd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_f_1st         */		      35,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 1st Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_f_2nd         */		      60,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 2nd Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_f_3rd         */		      80,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 3rd Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_r_1st         */		      30,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 1st Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_r_2nd         */		      60,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 2nd Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainDuty_r_3rd         */		      80,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 3rd Duty 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainf_1st              */		     100,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 1st Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainf_2nd              */		      40,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 2nd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainf_3rd              */		       0,	/* Comment [ [Front Rise Gain 설정] ESC 모터 LFC 증압조건에서, 3rd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainr_1st              */		     100,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 1st Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainr_2nd              */		      50,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 2nd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LfcEscMRiseGainr_3rd              */		       0,	/* Comment [ [Rear Rise Gain 설정] ESC 모터 LFC 증압조건에서, 3rd Duty 에서 Gain 값 ] */
	/* int16_t     	S16_LFC_Hold_Duty_1st                 */		      50,	/* Comment [ LFC제어 영역에서 Duty 1st [default:30] ] */
	/* int16_t     	S16_LFC_Hold_Duty_2nd                 */		      60,	/* Comment [ LFC제어 영역에서 Duty 2nd [default:40] ] */
	/* int16_t     	S16_LFC_Hold_Duty_3rd                 */		      75,	/* Comment [ LFC제어 영역에서 Duty 3rd [default:50] ] */
	/* int16_t     	S16_LFC_Hold_Duty_4th                 */		      90,	/* Comment [ LFC제어 영역에서 Duty 4th [default:60] ] */
	/* int16_t     	S16_LFC_Hold_Duty_5th                 */		     100,	/* Comment [ LFC제어 영역에서 Duty 5th [default:70] ] */
	/* int16_t     	S16_LFC_Hold_Duty_6th                 */		     100,	/* Comment [ LFC제어 영역에서 Duty 6th [default:80] ] */
	/* int16_t     	S16_LFC_Hold_press_1st                */		       0,	/* Comment [ LFC제어 영역에서 Duty 1st에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:0.0bar] ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_2nd                */		     350,	/* Comment [ LFC제어 영역에서 Duty 2nd에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:35.0bar] ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_3rd                */		     700,	/* Comment [ LFC제어 영역에서 Duty 3rd에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:70.0bar] ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_4th                */		    1050,	/* Comment [ LFC제어 영역에서 Duty 4th에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:105.0bar] ] */
	                                                        		        	/* ScaleVal[    105 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_5th                */		    1500,	/* Comment [ LFC제어 영역에서 Duty 5th에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:150.0bar] ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_6th                */		    1600,	/* Comment [ LFC제어 영역에서 Duty 6th에 해당하는 Master Press와 Wheel Press의 압력 차이 [default:160.0bar] ] */
	                                                        		        	/* ScaleVal[    160 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_f_1st                   */		       0,	/* Comment [ Dump Rate Gain set 1st [default:0%] ] */
	/* int16_t     	S16_Dump_Gain_f_2nd                   */		      20,	/* Comment [ Dump Rate Gain set 2nd [default:20%] ] */
	/* int16_t     	S16_Dump_Gain_f_3rd                   */		      60,	/* Comment [ Dump Rate Gain set 3rd [default:80%] ] */
	/* int16_t     	S16_Dump_Gain_f_4th                   */		      80,	/* Comment [ Dump Rate Gain set 4th [default:100%] ] */
	/* int16_t     	S16_Dump_Gain_f_5th                   */		     140,	/* Comment [ Dump Rate Gain set 5th [default:160%] ] */
	/* int16_t     	S16_Dump_Gain_f_6th                   */		     150,	/* Comment [ Dump Rate Gain set 6th [default:160%] ] */
	/* int16_t     	S16_Dump_Gain_Press_f_1st             */		       0,	/* Comment [ Dump Rate Gain set 1st에 해당하는 Wheel Press [default:0.0bar] ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_f_2nd             */		      75,	/* Comment [ Dump Rate Gain set 2nd에 해당하는 Wheel Press [default:7.5bar] ] */
	                                                        		        	/* ScaleVal[    7.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_f_3rd             */		     400,	/* Comment [ Dump Rate Gain set 3rd에 해당하는 Wheel Press [default:40.0bar] ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_f_4th             */		     600,	/* Comment [ Dump Rate Gain set 4th에 해당하는 Wheel Press [default:60.0bar] ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_f_5th             */		    1330,	/* Comment [ Dump Rate Gain set 5th에 해당하는 Wheel Press [default:133.0bar] ] */
	                                                        		        	/* ScaleVal[    133 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_f_6th             */		    1500,	/* Comment [ Dump Rate Gain set 6th에 해당하는 Wheel Press [default:150.0bar] ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_1st             */		      25,	/* Comment [ Dump Rate Gain set 1st에 해당하는 Wheel Press [default:2.5bar] ] */
	                                                        		        	/* ScaleVal[    2.5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_2nd             */		     100,	/* Comment [ Dump Rate Gain set 2nd에 해당하는 Wheel Press [default:10.0bar] ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_3rd             */		     400,	/* Comment [ Dump Rate Gain set 3rd에 해당하는 Wheel Press [default:40.0bar] ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_4th             */		     800,	/* Comment [ Dump Rate Gain set 4th에 해당하는 Wheel Press [default:80.0bar] ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_5th             */		    1400,	/* Comment [ Dump Rate Gain set 5th에 해당하는 Wheel Press [default:140.0bar] ] */
	                                                        		        	/* ScaleVal[    140 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_Press_r_6th             */		    1500,	/* Comment [ Dump Rate Gain set 6th에 해당하는 Wheel Press [default:150.0bar] ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Dump_Gain_r_1st                   */		      30,	/* Comment [ Dump Rate Gain set 1st [default:30%] ] */
	/* int16_t     	S16_Dump_Gain_r_2nd                   */		      80,	/* Comment [ Dump Rate Gain set 2nd [default:57%] ] */
	/* int16_t     	S16_Dump_Gain_r_3rd                   */		     120,	/* Comment [ Dump Rate Gain set 3rd [default:100%] ] */
	/* int16_t     	S16_Dump_Gain_r_4th                   */		     120,	/* Comment [ Dump Rate Gain set 4th [default:130%] ] */
	/* int16_t     	S16_Dump_Gain_r_5th                   */		     140,	/* Comment [ Dump Rate Gain set 5th [default:140%] ] */
	/* int16_t     	S16_Dump_Gain_r_6th                   */		     150,	/* Comment [ Dump Rate Gain set 6th [default:150%] ] */
	/* int16_t     	S16_Circulation_Motor_speed_1st       */		    2500,	/* Comment [ Pre Circulation 작동시 Motor rpm 1st  [default:2500rpm] ] */
	/* int16_t     	S16_Circulation_Motor_speed_2nd       */		    3300,	/* Comment [ Pre Circulation 작동시 Motor rpm 2nd [default:3300rpm] ] */
	/* int16_t     	S16_Circulation_Press_1st             */		      20,	/* Comment [ Pre Circulation 작동시 Motor rpm 1st에 해당하는 Wheel Press [default:2.0bar] ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_Circulation_Press_2nd             */		      30,	/* Comment [ Pre Circulation 작동시 Motor rpm 2nd에 해당하는 Wheel Press [default:3.0bar] ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_PRESS_COEFF_FRT                   */		      46,	/* Comment [ Front 1 Wheel Force to bar Coefficient ] */
	/* int16_t     	S16_PRESS_COEFF_RR                    */		      32,	/* Comment [ Rear 1 Wheel Force to bar Coefficient ] */
	/* uchar8_t    	U8_PRESSURE_TEST_MODE                 */		       0,	/* Comment [ Valve Test Mode ] */
	/* uchar8_t    	U8_Usage_Level_of_ESC_Sen             */		       3,	/* Comment [ Use ESC sensor in GMA Hold Compensation for (3: Inc and Dec / 2: Inc only / 1: Dec only / 0: No Use) ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW_SPEED_H       */		     300,	/* Comment [ Target Delta-Yaw at High Speed in Normal Mu Condition, default 5 deg/sec ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW_SPEED_L       */		     500,	/* Comment [ Target Delta-Yaw at Low Speed in Normal Mu Condition, default 7 deg/sec ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW_SPEED_M       */		     500,	/* Comment [ Target Delta-Yaw at Med Speed in Normal Mu Condition, default 5 deg/sec ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW_SPEED_H           */		     300,	/* Comment [ Target Delta-Yaw at High Speed in Low Mu, default 3 deg/sec ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW_SPEED_L           */		     500,	/* Comment [ Target Delta-Yaw at Low Speed in Low Mu, default 5 deg/sec ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW_SPEED_M           */		     500,	/* Comment [ Target Delta-Yaw at Med Speed in Low Mu, default 3 deg/sec ] */
	                                                        		        	/* ScaleVal[    5 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW_SPEED_H         */		     800,	/* Comment [ Target Delta-Yaw at High Speed in Split Mu, default 5 deg/sec ] */
	                                                        		        	/* ScaleVal[    8 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW_SPEED_L         */		    1000,	/* Comment [ Target Delta-Yaw at Low Speed in Split Mu, default 9 deg/sec ] */
	                                                        		        	/* ScaleVal[   10 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW_SPEED_M         */		     700,	/* Comment [ Target Delta-Yaw at Med Speed in Split Mu, default 7 deg/sec ] */
	                                                        		        	/* ScaleVal[    7 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW2_SPEED_H      */		     100,	/* Comment [ Target Delta-Yaw2 at High Speed in Normal Mu Condition for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW2_SPEED_L      */		     100,	/* Comment [ Target Delta-Yaw2 at Low Speed in Normal Mu Condition for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_DEFAULT_TARGET_dYAW2_SPEED_M      */		     100,	/* Comment [ Target Delta-Yaw2 at Med Speed in Normal Mu Condition for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW2_SPEED_H          */		     100,	/* Comment [ Target Delta-Yaw2 at High Speed in Low Mu for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW2_SPEED_L          */		     100,	/* Comment [ Target Delta-Yaw2 at Low Speed in Low Mu for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_LMu_TARGET_dYAW2_SPEED_M          */		     100,	/* Comment [ Target Delta-Yaw2 at Med Speed in Low Mu for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW2_SPEED_H        */		     300,	/* Comment [ Target Delta-Yaw2 at High Speed in Split Mu for stable hold time(flasg:111), default 1 deg/sec ] */
	                                                        		        	/* ScaleVal[    3 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW2_SPEED_L        */		     100,	/* Comment [ Target Delta-Yaw2 at Low Speed in Split Mu for stable hold time(flasg:111), default 3 deg/sec ] */
	                                                        		        	/* ScaleVal[    1 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_SPLIT_TARGET_dYAW2_SPEED_M        */		     200,	/* Comment [ Target Delta-Yaw2 at Med Speed in Split Mu for stable hold time(flasg:111), default 2 deg/sec ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 50 ] */
	/* char8_t     	S8_HOMO_dYAW_ABS_GAIN                 */		      15,	/* Comment [ Delta-Yaw Gain @ Normal Mu Condition During ABS, default 15, min 10, max 20 ] */
	/* char8_t     	S8_HOMO_dYAW_DOT_ABS_GAIN             */		       1,	/* Comment [ Delta-Yaw Dot Gain @ Normal Mu Condition During ABS, default 2, min 0, max 4 ] */
	/* char8_t     	S8_SPLIT_dYAW_ABS_GAIN_1              */		       8,	/* Comment [ Delta-Yaw Gain @ Split Mu @ the early stage of ABS cycle, default 10, min 10, max 20 ] */
	/* char8_t     	S8_SPLIT_dYAW_ABS_GAIN_2              */		      10,	/* Comment [ Delta-Yaw Gain @ Split Mu @ the remaining ABS cycle, default 15, min 10, max 20 ] */
	/* char8_t     	S8_SPLIT_dYAW_DOT_ABS_GAIN_1          */		       0,	/* Comment [ Delta-Yaw Dot Gain @ Split Mu @ the early stage of ABS cycle, default 0, min 0, max 4 ] */
	/* char8_t     	S8_SPLIT_dYAW_DOT_ABS_GAIN_2          */		       1,	/* Comment [ Delta-Yaw Dot Gain @ Split Mu @ the remaining ABS cycle, default 2, min 0, max 4 ] */
	/* int16_t     	S16_NEED_INC_PRESS_YDF_L1             */		    -400,	/* Comment [ Yaw-dump factor for Pressure Increase level 1 (default : -400) ] */
	/* int16_t     	S16_NEED_INC_PRESS_YDF_L2             */		   -1000,	/* Comment [ Yaw-dump factor for Pressure Increase level 2 (default : -1000) ] */
	/* int16_t     	S16_YDF_RISEHOLD_T_MAX_YDF            */		       0,	/* Comment [ Yaw-dump factor for maximum rise-hold scans (default : 1000) ] */
	/* uchar8_t    	U8_YDF_RISEHOLD_T_AT_INC_L1           */		      15,	/* Comment [ Rise hold scans at Pressure Increase level 1 (default : 15) ] */
	/* uchar8_t    	U8_YDF_RISEHOLD_T_AT_INC_L2           */		      10,	/* Comment [ Rise hold scans at Pressure Increase level 2 (default : 10) ] */
	/* uchar8_t    	U8_YDF_RISEHOLD_T_AT_TARGETDYAW       */		      40,	/* Comment [ Rise hold scans at Target del-yaw, level 0 (default : 40) ] */
	/* uchar8_t    	U8_YDF_RISEHOLD_T_MAX                 */		     100,	/* Comment [ Maximum rise-hold scans at Pressure Increase at Max YDF, level max. (default : 100) ] */
	/* int16_t     	S16_NEED_DEC_PRESS_YDF_L1             */		     300,	/* Comment [ Yaw-dump factor for Pressure decrease level 1 (default : 300) ] */
	/* int16_t     	S16_NEED_DEC_PRESS_YDF_L2             */		    1000,	/* Comment [ Yaw-dump factor for Pressure decrease level 2 (default : 1000) ] */
	/* int16_t     	S16_NEED_DEC_PRESS_YDF_L3             */		    2000,	/* Comment [ Yaw-dump factor for Pressure decrease level 3 (default : 2000) ] */
	/* uchar8_t    	U8_YDF_DUMPHOLD_T_AT_DEC_L1           */		      50,	/* Comment [ Dump hold scans at Pressure Decrease level 1 (default : 50) ] */
	/* uchar8_t    	U8_YDF_DUMPHOLD_T_AT_DEC_L2           */		      10,	/* Comment [ Dump hold scans at Pressure Decrease level 2 (default : 10) ] */
	/* uchar8_t    	U8_YDF_DUMPHOLD_T_AT_DEC_L3           */		       3,	/* Comment [ Dump hold scans at Pressure Decrease level 3 (default : 3) ] */
	/* uchar8_t    	U8_YDF_DUMPHOLD_T_MAX                 */		      50,	/* Comment [ Maximum Dump hold scans at Pressure Decrease level 0 (default : 50) ] */
	/* int16_t     	S16_YDF_STB_HLD_TIME_MAX              */		      10,	/* Comment [ Stable hold max. time at yaw rate 10deg/s in split mu(split tendancy), wheel flags: 111 ] */
	/* int16_t     	S16_YDF_STB_HLD_TIME_MAX_F_OINR       */		       5,	/* Comment [ Stable hold max. time at yaw rate 10deg/s in normal mu, wheel flags: 111 ] */
	/* uchar8_t    	U8_STB_DMP_MAX_HIGH_OverInF           */		       2,	/* Comment [ Stable Dump limitation level for Over Inside Front Wheel, High-mu (default : 2) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_LOW_OverInF            */		       6,	/* Comment [ Stable Dump limitation level for Over Inside Front Wheel, low-mu (default : 6) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_MED_OverInF            */		       4,	/* Comment [ Stable Dump limitation level for Over Inside Front Wheel, med-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_SP_OverInF             */		       4,	/* Comment [ Stable Dump limitation level for Over Inside Front Wheel, Split-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_HIGH_OverInR           */		       4,	/* Comment [ Stable Dump limitation level for Over Inside Rear Wheel, High-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_LOW_OverInR            */		       8,	/* Comment [ Stable Dump limitation level for Over Inside Rear Wheel, low-mu (default : 8) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_MED_OverInR            */		       6,	/* Comment [ Stable Dump limitation level for Over Inside Rear Wheel, med-mu (default : 6) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_SP_OverInR             */		       4,	/* Comment [ Stable Dump limitation level for Over Inside Rear Wheel, Split-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_HIGH_BothOverInF       */		       2,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, high-mu (default : 2) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_LOW_BothOverInF        */		       4,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, low-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_MED_BothOverInF        */		       2,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, med-mu (default : 2) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_SP_BothOverInF         */		       2,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, split-mu (default : 2) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_HIGH_BothOverInR       */		       4,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, high-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_LOW_BothOverInR        */		       6,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, low-mu (default : 6) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_MED_BothOverInR        */		       4,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, med-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_SP_BothOverInR         */		       4,	/* Comment [ Stable Dump limitation level for Front Wheel if Both Front and Rear Wheels are in Over Ctrl, split-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_HIGH_UnderOutF         */		       1,	/* Comment [ Stable Dump limitation level for Under Outside Front Wheel, High-mu (default : 1) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_LOW_UnderOutF          */		       4,	/* Comment [ Stable Dump limitation level for Under Outside Front Wheel, low-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_MED_UnderOutF          */		       4,	/* Comment [ Stable Dump limitation level for Under Outside Front Wheel, med-mu (default : 4) ] */
	/* uchar8_t    	U8_STB_DMP_MAX_SP_UnderOutF           */		       0,	/* Comment [ Stable Dump limitation level for Under Outside Front Wheel, Split-mu (default : 0) ] */
	/* int16_t     	S16_FRNT_GMA_STB_DMP_THR_YDF_B_L      */		     500,	/* Comment [ Front Yaw-Dump factor threshold below low speed during GMA pulse up (default : 400). If Yaw-Dump Factor is bigger than this value, stable dump is applied ] */
	/* int16_t     	S16_FRNT_GMA_STB_DMP_THR_YDF_H        */		     300,	/* Comment [ Front Yaw-Dump factor threshold above high speed during GMA pulse up (default : 300). If Yaw-Dump Factor is bigger than this value, stable dump is applied ] */
	/* int16_t     	S16_FRNT_GMA_STB_DMP_THR_YDF_L        */		     500,	/* Comment [ Front Yaw-Dump factor threshold above low speed during GMA pulse up (default : 400). If Yaw-Dump Factor is bigger than this value, stable dump is applied ] */
	/* int16_t     	S16_FRNT_GMA_STB_DMP_THR_YDF_M        */		     300,	/* Comment [ Front Yaw-Dump factor threshold above med speed during GMA pulse up (default : 300). If Yaw-Dump Factor is bigger than this value, stable dump is applied ] */
	/* int16_t     	S16_REAR_HOMO_SL_HLD_THR_YDF_B_L      */		     500,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold below low speed in Homo-mu (default : 500). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_HOMO_SL_HLD_THR_YDF_H        */		     500,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above high speed in Homo-mu (default : 500). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_HOMO_SL_HLD_THR_YDF_L        */		     500,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above low speed in Homo-mu (default : 500). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_HOMO_SL_HLD_THR_YDF_M        */		     500,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above med speed in Homo-mu (default : 500). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_SP_SL_HLD_THRES_YDF_B_L      */		     250,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold below speed in Split-mu (default : 250). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_SP_SL_HLD_THRES_YDF_H        */		     250,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above high speed in Split-mu (default : 250). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_SP_SL_HLD_THRES_YDF_L        */		     250,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above low speed in Split-mu (default : 250). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_REAR_SP_SL_HLD_THRES_YDF_M        */		     250,	/* Comment [ Rear Selec-Low Yaw-Dump factor threshold above med speed in Split-mu (default : 250). If Yaw-Dump Factor is bigger than this value, Select Low Dump is applied ] */
	/* char8_t     	S8_REAR_HOMO_SL_HOLD_THRES_SLIP       */		       5,	/* Comment [ Rear Selec-Low Slip threshold in Homo-mu (default : 5%). If slip is bigger than this value, Select Low Dump is applied ] */
	/* char8_t     	S8_REAR_SP_SL_HOLD_THRES_SLIP         */		       3,	/* Comment [ Rear Selec-Low Slip threshold in Split-mu (default : 3%). If slip is bigger than this value, Select Low Dump is applied ] */
	/* int16_t     	S16_LCB_ENTER_MAX_LATERAL_G           */		     500,	/* Comment [ Max. Lateral G for Limit Cornering Brake Detection (default 0.5g) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -1 <= X <= 1 ] */
	/* int16_t     	S16_LCB_EXIT_MIN_LATERAL_G            */		     600,	/* Comment [ Min. Lateral G for Limit Cornering Brake Reset (default 0.6g) ] */
	                                                        		        	/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -1 <= X <= 1 ] */
	/* char8_t     	S8_LCB_ENTER_DEL_YAW_THRES            */		       2,	/* Comment [ Min. Del-Yaw (Oversteer) Thres. for Limit Cornering Brake Detection (default 2deg/s) ] */
	/* char8_t     	S8_LCB_EXIT_DEL_YAW_THRES             */		       0,	/* Comment [ Max. Del-Yaw (Nuetralsteer) Thres. for Limit Cornering Brake Reset (default 0deg/s) ] */
	/* uchar8_t    	U8_LCB_MED_MU_DET_F_INS_SKID          */		      40,	/* Comment [ Min. Skid Pressure of Front Inside Wheel for Detecting Limit Cornering Brake (default 40bar) ] */
	/* uchar8_t    	U8_LCB_MED_MU_DET_F_OUT_SKID          */		      50,	/* Comment [ Min. Skid Pressure of Front Outside Wheel for Detecting Limit Cornering Brake (default 50bar) ] */
	/* uchar8_t    	U8_Add_ABS_Mtr_Off_Duty_in_ESC        */		      40,	/* Comment [ Threshold Duty for Additional Motor Off after ABS in ESC System, default 40,min 0V ] */
	/* uchar8_t    	U8_Add_ABS_Mtr_On_Duty_in_ESC         */		      70,	/* Comment [ Threshold Duty for Additional Motor On after ABS in ESC System, default 70,min 0V ] */
	/* int16_t     	S16_ESP_Motor_BEMF_ADD_HIGH_MU        */		       0,	/* Comment [ Additional ABS Motor BEMF Voltage in ESC system @ U8_PULSE_HIGH_MU ~, default 0,min 0V" ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* int16_t     	S16_ESP_Motor_BEMF_ADD_MED_MU         */		       0,	/* Comment [ Additional ABS Motor BEMF Voltage in ESC system @ U8_PULSE_HIGH_MU ~, default 0,min 0V" ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* uchar8_t    	U8_DPDutyCompRef_HighMuSkid           */		      80,	/* Comment [ High-mu skid pressure: reference for duty compensation, default 80bar ] */
	/* uchar8_t    	U8_DPDutyCompRef_HighMuSkidR          */		      80,	/* Comment [ High-mu skid pressure for rear wheels: reference for duty compensation, default 80bar ] */
	/* uchar8_t    	U8_DPDutyCompRef_MPVariation          */		      40,	/* Comment [ Minimum MP variation for 1 duty, default 7bar, min 5, max 10 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 20 ] */
	/* uchar8_t    	U8_DPDutyCompRef_MPVariationR         */		      40,	/* Comment [ Minimum MP variation for 1 duty, rear wheels, default 7bar, min 5, max 10 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_DPDutyCompRef_PDiffFromSkid        */		      40,	/* Comment [ Minimum ΔP for 1 duty, default 7bar, min 5, max 10 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 20 ] */
	/* uchar8_t    	U8_DPDutyCompRef_PDiffFromSkidR       */		      40,	/* Comment [ Minimum ΔP for 1 duty, rear wheels, default 7bar, min 5, max 10 ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_Initial_Duty_Ratio_HDC_MAX         */		      50,	/* Comment [ Initial Duty during HDC, at press 20bar, default 50, min 10, max 70 ] */
	/* uchar8_t    	U8_Initial_Duty_Ratio_HDC_MIN         */		      15,	/* Comment [ Initial Duty during HDC, at press 20bar, default 15, min 10, max 40 ] */
	/* uchar8_t    	S8_Decel_Gain_LMu_10_20k_F_HDC        */		      40,	/* Comment [ Decel Gain during HDC, Low Mu, 10-20kph, Front Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Decel_Gain_LMu_B_10k_F_HDC         */		      35,	/* Comment [ Decel Gain during HDC, Low Mu, 10-20kph, Front Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Slip_Gain_LMu_10_20k_F_HDC         */		      14,	/* Comment [ Slip  Gain during HDC, Low Mu, 10-20kph, Front Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Slip_Gain_LMu_B_10k_F_HDC          */		      10,	/* Comment [ Slip  Gain during HDC, Low Mu, Below 10kph, Front Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Decel_Gain_LMu_10_20k_R_HDC        */		      45,	/* Comment [ Decel Gain during HDC, Low Mu, 10-20kph, Rear Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Decel_Gain_LMu_B_10k_R_HDC         */		      30,	/* Comment [ Decel Gain during HDC, Low Mu, 10-20kph, Rear Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Slip_Gain_LMu_10_20k_R_HDC         */		      12,	/* Comment [ Slip  Gain during HDC, Low Mu, 10-20kph, Rear Wheels, Default Low Mu gain ] */
	/* uchar8_t    	S8_Slip_Gain_LMu_B_10k_R_HDC          */		      10,	/* Comment [ Slip  Gain during HDC, Low Mu, Below 10kph, Rear Wheels, Default Low Mu gain ] */
	/* uint16_t    	K_Brake_Pedal_Pressure_Threshold      */		     700,	/* Comment [ 0~ 20000,  Resolution 100, Default 1000,Unit Bar ] */
	/* uchar8_t    	U8_ESV_OPEN                           */		       0,	/* Comment [ ESV Valve enable/disable flag (1:Open) ] */
	/* uchar8_t    	U8_MOTOR_ON_SPEED                     */		      96,	/* Comment [ Motor Initial check speed ] */
	                                                        		        	/* ScaleVal[    12 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_MOTOR_ON_TEST                      */		       1,	/* Comment [ Motor Initail check on time set (0:PWM, 1:IGN 전압에 따른구동, 2이상: X 7ms) ] */
	/* int16_t     	ACC_PERC_1st                          */		      50,	/* Comment [ HDC 상황에서 Disk 온도 추정시, 감속도 에 따른 온도 추정 기울기 변경 가중치 ( 감속도_Low ). 값을 키우면, 추정 온도 커짐. 10 이상씩 변화. default = 50, [1~500] ] */
	/* int16_t     	ACC_PERC_2nd                          */		     100,	/* Comment [ HDC 상황에서 Disk 온도 추정시, 감속도 에 따른 온도 추정 기울기 변경 가중치 ( 감속도_Med ). 값을 키우면, 추정 온도 커짐. 10 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	ACC_PERC_3rd                          */		     150,	/* Comment [ HDC 상황에서 Disk 온도 추정시, 감속도 에 따른 온도 추정 기울기 변경 가중치 ( 감속도_High ). 값을 키우면, 추정 온도 커짐. 10 이상씩 변화. default = 150, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_ACC_2WD                   */		      80,	/* Comment [ 온도 추정시, 차량 감속도에 따른 Disk 온도 추정 기울기 변경 ( 2WD ). 값을 키우면, 2WD 에서 온도 추정 기울기 커짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_ACC_4WD                   */		      80,	/* Comment [ 온도 추정시, 차량 감속도에 따른 Disk 온도 추정 기울기 변경 ( 4WD ). 값을 키우면, 4WD 에서 온도 추정 기울기 커짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_BTCS_2WD                  */		      30,	/* Comment [ 온도 추정시, BTCS 작동에 따른 Disk 온도 추정 기울기 변경 ( 2WD ). 값을 키우면, 2WD 에서 온도 추정 기울기 커짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_BTCS_4WD                  */		      30,	/* Comment [ 온도 추정시, BTCS 작동에 따른 Disk 온도 추정 기울기 변경 ( 4WD ). 값을 키우면, 4WD 에서 온도 추정 기울기 커짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_2WD_30K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 2WD, 30KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_2WD_60K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 2WD, 60KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_2WD_90K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 2WD, 90KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_4WD_30K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 4WD, 30KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_4WD_60K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 4WD, 60KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	S16_WEG_TMP_COOL_4WD_90K              */		      70,	/* Comment [ 온도 추정시, 차속에 따른 Disk 온도 Cooling 기울기 변경 ( 4WD, 90KPH ). 값을 키우면, 온도 Cooling 빨라짐. 5 이상씩 변화. default = 100, [1~500] ] */
	/* int16_t     	WEG_THETA_G_2_TMP_ACC_12KPH           */		      19,	/* Comment [ HDC 상황에서 Disk 온도 추정시, Target 속도 변경에 따른 온도 추정 기울기 변경 가중치 ( 12KPH ). 값을 키우면, 추정 온도 커짐. 3 이상씩 변화. default = 19, [1~100] ] */
	/* int16_t     	WEG_THETA_G_2_TMP_ACC_18KPH           */		      23,	/* Comment [ HDC 상황에서 Disk 온도 추정시, Target 속도 변경에 따른 온도 추정 기울기 변경 가중치 ( 18KPH ). 값을 키우면, 추정 온도 커짐. 3 이상씩 변화. default = 23, [1~100] ] */
	/* int16_t     	WEG_THETA_G_2_TMP_ACC_27KPH           */		      25,	/* Comment [ HDC 상황에서 Disk 온도 추정시, Target 속도 변경에 따른 온도 추정 기울기 변경 가중치 ( 27KPH ). 값을 키우면, 추정 온도 커짐. 3 이상씩 변화. default = 25, [1~100] ] */
	/* int16_t     	WEG_THETA_G_2_TMP_ACC_7KPH            */		      11,	/* Comment [ HDC 상황에서 Disk 온도 추정시, Target 속도 변경에 따른 온도 추정 기울기 변경 가중치 ( 7KPH ). 값을 키우면, 추정 온도 커짐. 3 이상씩 변화. default = 11, [1~100] ] */
	/* int16_t     	S16_AIR_CONDUCTIVITY                  */		     255,	/* Comment [ Air conductivity // default 255 -> 0.0255W/mK ] */
	                                                        		        	/* ScaleVal[ 0.0255 W/mK ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ 0 <= X <= 3.2767 ] */
	/* int16_t     	S16_BRAKE_DISK_SPECIFIC_HEAT          */		     420,	/* Comment [ Brake disk specific heat // default 420 -> 420N*m/(kg*℃) ] */
	/* int16_t     	S16_TIRE_DIAMETER                     */		    2073,	/* Comment [ Dynamic wheel radius*2*pi // default 2010 -> 2010mm ] */
	/* int16_t     	S16_WHEEL_RADIUS                      */		     330,	/* Comment [ Dynamic wheel radius // default 320 -> 320mm ] */
	/* int16_t     	S16_BRAKE_COOLING_COEFFIC_F           */		      38,	/* Comment [ Front cooling coefficient in driving// default 26 -> 26 ] */
	/* int16_t     	S16_BRAKE_DISK_AREA_F                 */		     467,	/* Comment [ Front brake disk area // default 460 -> 460cm^2 ] */
	/* int16_t     	S16_BRAKE_DISK_DIAMETER_F             */		     760,	/* Comment [ Front brake disk effective radius*2*pi // default 760 -> 760mm ] */
	/* int16_t     	S16_BRAKE_DISK_MASS_F                 */		       8,	/* Comment [ Front brake disk mass // default 6 -> 6kg ] */
	/* int16_t     	S16_BRAKE_PAD_AREA_F                  */		      28,	/* Comment [ Front brake cylinder area // default 28 -> 28cm^2 ] */
	/* int16_t     	S16_BRAKE_PAD_FRICTION_F              */		      35,	/* Comment [ Front brake pad friction // default 35 -> 35% ] */
	/* int16_t     	S16_BRAKE_THERM_EFFIC_F               */		      87,	/* Comment [ Front heating efficient //0~100%, default 80 -> 80% ] */
	/* int16_t     	S16_STOP_STATE_CONV_COEFFIC_F         */		      60,	/* Comment [ Front stop state convection coefficient // default 50 -> 50 ] */
	/* int16_t     	S16_BRAKE_COOLING_COEFFIC_R           */		      26,	/* Comment [ Rear cooling coefficient in driving// default 26 -> 26 ] */
	/* int16_t     	S16_BRAKE_DISK_AREA_R                 */		     483,	/* Comment [ Rear brake disk area // default 460 -> 460cm^2 ] */
	/* int16_t     	S16_BRAKE_DISK_DIAMETER_R             */		     780,	/* Comment [ Rear brake disk effective radius*2*pi // default 760 -> 760mm ] */
	/* int16_t     	S16_BRAKE_DISK_MASS_R                 */		       6,	/* Comment [ Rear brake disk mass // default 6 -> 6kg ] */
	/* int16_t     	S16_BRAKE_PAD_AREA_R                  */		      18,	/* Comment [ Rear brake cylinder area // default 11 -> 11cm^2 ] */
	/* int16_t     	S16_BRAKE_PAD_FRICTION_R              */		      35,	/* Comment [ Rear brake pad friction // default 35 -> 35% ] */
	/* int16_t     	S16_BRAKE_THERM_EFFIC_R               */		      87,	/* Comment [ Rear heating efficient //0~100%, default 80 -> 80% ] */
	/* int16_t     	S16_STOP_STATE_CONV_COEFFIC_R         */		     110,	/* Comment [ Rear stop state convection coefficient // default 50 -> 50 ] */
	/* int16_t     	S16_CRAM_OUTSIDE_REAR_SLIP_LMT        */		       0,	/* Comment [ CRAM 제어휠(Out-Rear)의 최대 moment limitation, def: 8 ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 60 ] */
	/* int16_t     	S16_CRAM_OUT_FRT_SLIP_ADD_WEG         */		     100,	/* Comment [ CRAM 진입 시, Front Outside휠(제어휠)에 추가되는 moment량, 100%면 CRAM 제어휠의 moment와 같음 ] */
	/* int16_t     	S16_CRAM_OUT_REAR_SLIP_LMT_1ST        */		       0,	/* Comment [ CRAM 진입 초기에 압력 확보를 위해 더 큰 moment필요한 경우, 일시적으로 limitation 변경값 ] */
	                                                        		        	/* ScaleVal[        0 N ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 100 ] */
	/* int16_t     	S16_CRAM_PULSE_UP_INI_DUTY_REAR       */		      20,	/* Comment [ CRAM 제어휠(Out-Rear)의 압력 상승 pulse-up duty, def: 20msec ] */
	/* int16_t     	S16_CRAM_REAR_1ST_CYCLE_TIME          */		      14,	/* Comment [ 초기 moment limitation 변경값의 유지 시간, 초기에 충분한 압력 확보가 되도록 지정, def: 14msec ] */
	/* int16_t     	S16_CRAM_MOMENT_HIGH_WEG_R            */		     100,	/* Comment [ CRAM Moment speed gain @ S16_D_GAIN_HIGH_SPD(yaw-ctrl,D-gain Weg), 100%이면 delV랑 같음 ] */
	/* int16_t     	S16_CRAM_MOMENT_LOW_WEG_R             */		     100,	/* Comment [ CRAM Moment speed gain @ S16_D_GAIN_LOW_SPD(yaw-ctrl,D-gain Weg), 100%이면 delV랑 같음 ] */
	/* int16_t     	S16_CRAM_MOMENT_MED_WEG_R             */		     100,	/* Comment [ CRAM Moment speed gain @ S16_D_GAIN_MED_SPD(yaw-ctrl,D-gain Weg), 100%이면 delV랑 같음 ] */
	/* int16_t     	S16_CRAM_MOMENT_V_LOW_WEG_R           */		     100,	/* Comment [ CRAM Moment speed gain @ S16_D_GAIN_V_LOW_SPD(yaw-ctrl,D-gain Weg), 100%이면 delV랑 같음 ] */
	/* int16_t     	S16_CRAM_DELV_CALC_ALAT               */		     500,	/* Comment [ DelV 계산 시작 조건, 낮출수록 delV 계산 시점이 빨라짐, def:0.5g ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16_CRAM_DELV_HIGH_WEG_R              */		     100,	/* Comment [ DelV Speed gain @ S16_D_GAIN_HIGH_SPD(yaw-ctrl D-gain Weg) 낮추면 delV가 해당속도에서 작아짐 ] */
	/* int16_t     	S16_CRAM_DELV_LOW_WEG_R               */		     100,	/* Comment [ DelV Speed gain @ S16_D_GAIN_LOW_SPD(yaw-ctrl D-gain Weg) 낮추면 delV가 해당속도에서 작아짐 ] */
	/* int16_t     	S16_CRAM_DELV_MED_WEG_R               */		     100,	/* Comment [ DelV Speed gain @ S16_D_GAIN_MED_SPD(yaw-ctrl D-gain Weg) 낮추면 delV가 해당속도에서 작아짐 ] */
	/* int16_t     	S16_CRAM_DELV_V_LOW_WEG_R             */		     100,	/* Comment [ DelV Speed gain @ S16_D_GAIN_V_LOW_SPD(yaw-ctrl D-gain Weg) 낮추면 delV가 해당속도에서 작아짐 ] */
	/* int16_t     	S16_CRAM_ENTER_ALATM                  */		     550,	/* Comment [ CRAM 진입 alatm조건, 이 값 이상이면 진입, def:0.55g ] */
	                                                        		        	/* ScaleVal[       0.55 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16_CRAM_ENTER_DELV                   */		     -50,	/* Comment [ CRAM 진입 delV조건, 이 값 이하이면 진입, def: -5kph ] */
	                                                        		        	/* ScaleVal[         -5 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -100 <= X <= 0 ] */
	/* int16_t     	S16_CRAM_ENTER_MTP                    */		       5,	/* Comment [ Max MTP to enter CRAM ] */
	/* int16_t     	S16_CRAM_ENTER_SPD                    */		     350,	/* Comment [ CRAM 진입 vrefk 조건, 이 값 이상이면 진입, def: 35kph ] */
	                                                        		        	/* ScaleVal[         35 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 250 ] */
	/* int16_t     	S16_CRAM_EXIT_ALATM                   */		     500,	/* Comment [ CRAM 해제 alatm 조건, 이값 미만이면 해제, def: 0.5g ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16_CRAM_EXIT_DELV                    */		     -20,	/* Comment [ CRAM 해제 delV조건, 이 값 초과이면 해제, def: -2kph ] */
	                                                        		        	/* ScaleVal[         -2 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -100 <= X <= 0 ] */
	/* int16_t     	S16_CRAM_EXIT_SPD                     */		     300,	/* Comment [ CRAM 해제 vrefk 조건, 이 값 이하면 해제, def: 30kph ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 250 ] */
	/* int16_t     	S16_CRAM_LMT_ENT_DELYAW               */		       0,	/* Comment [ CRAM 진입 delta_yaw조건(0이상이면 over, 0이하면 under), 이 값 이상이면 진입, def: 0deg/sec ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_CRAM_LMT_EXT_DELYAW               */		     -50,	/* Comment [ CRAM 해제 delta_yaw조건(0이상이면 over, 0이하면 under), 이 값 미만이면 해제, def: -5deg/sec ] */
	                                                        		        	/* ScaleVal[         -5 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	MAX_ENGINE_TORQ_RTA2WHL               */		     800,	/* Comment [ 2 Wheel RTA Max Eng. Torq threshold ] */
	                                                        		        	/* ScaleVal[         80 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 500 ] */
	/* int16_t     	RTA_2WHL_DET_TIME                     */		    1000,	/* Comment [ 2 Wheel RTA detection Time, default 10sec ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	RTA_2WHL_RESET_TIME                   */		     300,	/* Comment [ 2 Wheel RTA Reset Time , default 3sec ] */
	                                                        		        	/* ScaleVal[          3 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_LFC_Hold_press_f_1st              */		       0,	/* Comment [ Front_LFC_Hold_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_f_2nd              */		     300,	/* Comment [ Front_LFC_Hold_Press_2, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_f_3rd              */		     350,	/* Comment [ Front_LFC_Hold_Press_3, default : 35bar ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_f_4th              */		     500,	/* Comment [ Front_LFC_Hold_Press_4, default : 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_f_5th              */		    1000,	/* Comment [ Front_LFC_Hold_Press_5, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_f_6th              */		    1500,	/* Comment [ Front_LFC_Hold_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_1st                */		      30,	/* Comment [ Front_LFC_Hold_Duty_1, default : 30 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_2nd                */		      60,	/* Comment [ Front_LFC_Hold_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_3rd                */		      70,	/* Comment [ Front_LFC_Hold_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_4th                */		      80,	/* Comment [ Front_LFC_Hold_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_5th                */		     110,	/* Comment [ Front_LFC_Hold_Duty_5, default : 110 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_f_6th                */		     150,	/* Comment [ Front_LFC_Hold_Duty_6, default : 150 ] */
	/* int16_t     	S16_LFC_Hold_press_r_1st              */		       0,	/* Comment [ Rear_LFC_Hold_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_r_2nd              */		     200,	/* Comment [ Rear_LFC_Hold_Press_2, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_r_3rd              */		     300,	/* Comment [ Rear_LFC_Hold_Press_3, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_r_4th              */		     550,	/* Comment [ Rear_LFC_Hold_Press_4, default : 55bar ] */
	                                                        		        	/* ScaleVal[     55 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_r_5th              */		     900,	/* Comment [ Rear_LFC_Hold_Press_5, default : 90bar ] */
	                                                        		        	/* ScaleVal[     90 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LFC_Hold_press_r_6th              */		    1500,	/* Comment [ Rear_LFC_Hold_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 BAR ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_1st                */		      30,	/* Comment [ Rear_LFC_Hold_Duty_1, default : 30 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_2nd                */		      60,	/* Comment [ Rear_LFC_Hold_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_3rd                */		      70,	/* Comment [ Rear_LFC_Hold_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_4th                */		      80,	/* Comment [ Rear_LFC_Hold_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_5th                */		     110,	/* Comment [ Rear_LFC_Hold_Duty_5, default : 110 ] */
	/* uchar8_t    	U8_LFC_Hold_Duty_r_6th                */		     150,	/* Comment [ Rear_LFC_Hold_Duty_6, default : 150 ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_1st               */		     100,	/* Comment [ Front_LFC_Rise_Gain_1, default : 100% ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_2nd               */		      70,	/* Comment [ Front_LFC_Rise_Gain_2, default : 70% ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_3rd               */		      40,	/* Comment [ Front_LFC_Rise_Gain_3, default : 40% ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_4th               */		      25,	/* Comment [ Front_LFC_Rise_Gain_4, default : 25% ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_5th               */		      15,	/* Comment [ Front_LFC_Rise_Gain_5, default : 15% ] */
	/* int16_t     	S16_LFC_Rise_Gain_f_6th               */		       0,	/* Comment [ Front_LFC_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_1st                */		       0,	/* Comment [ Front_LFC_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_2nd                */		      60,	/* Comment [ Front_LFC_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_3rd                */		      70,	/* Comment [ Front_LFC_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_4th                */		      80,	/* Comment [ Front_LFC_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_5th                */		     100,	/* Comment [ Front_LFC_Rise_Duty_5, default : 100 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_f_6th                */		     200,	/* Comment [ Front_LFC_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_1st               */		     100,	/* Comment [ Rear_LFC_Rise_Gain_1, default : 100% ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_2nd               */		      95,	/* Comment [ Rear_LFC_Rise_Gain_2, default : 95% ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_3rd               */		      75,	/* Comment [ Rear_LFC_Rise_Gain_3, default : 75% ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_4th               */		      45,	/* Comment [ Rear_LFC_Rise_Gain_4, default : 45% ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_5th               */		      15,	/* Comment [ Rear_LFC_Rise_Gain_5, default : 15% ] */
	/* int16_t     	S16_LFC_Rise_Gain_r_6th               */		       0,	/* Comment [ Rear_LFC_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_1st                */		       0,	/* Comment [ Rear_LFC_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_2nd                */		      60,	/* Comment [ Rear_LFC_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_3rd                */		      70,	/* Comment [ Rear_LFC_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_4th                */		      80,	/* Comment [ Rear_LFC_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_5th                */		     100,	/* Comment [ Rear_LFC_Rise_Duty_5, default : 110 ] */
	/* uchar8_t    	U8_LFC_Rise_Duty_r_6th                */		     200,	/* Comment [ Rear_LFC_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_1st        */		      50,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_1, default : 50% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_2nd        */		      40,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_2, default : 40% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_3rd        */		      30,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_3, default : 30% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_4th        */		      15,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_4, default : 15% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_5th        */		      10,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_5, default : 10% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_f_6th        */		       0,	/* Comment [ Front_LFC_ESC_MP_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_1st         */		       0,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_2nd         */		      60,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_3rd         */		      70,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_4th         */		      80,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_5th         */		     100,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_5, default : 100 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_f_6th         */		     200,	/* Comment [ Front_LFC_ESC_MP_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_1st        */		      60,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_1, default : 60% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_2nd        */		      45,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_2, default : 45% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_3rd        */		      35,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_3, default : 35% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_4th        */		      20,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_4, default : 20% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_5th        */		      10,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_5, default : 10% ] */
	/* int16_t     	S16_LFC_ESC_MP_Rise_Gain_r_6th        */		       0,	/* Comment [ Rear_LFC_ESC_MP_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_1st         */		       0,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_2nd         */		      60,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_3rd         */		      70,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_4th         */		      80,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_5th         */		     100,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_5, default : 100 ] */
	/* uchar8_t    	U8_LFC_ESC_MP_Rise_Duty_r_6th         */		     200,	/* Comment [ Rear_LFC_ESC_MP_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_1st           */		     100,	/* Comment [ Front_LFC_ESC_Rise_Gain_1, default : 100% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_2nd           */		      70,	/* Comment [ Front_LFC_ESC_Rise_Gain_2, default : 70% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_3rd           */		      50,	/* Comment [ Front_LFC_ESC_Rise_Gain_3, default : 50% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_4th           */		      40,	/* Comment [ Front_LFC_ESC_Rise_Gain_4, default : 40% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_5th           */		      15,	/* Comment [ Front_LFC_ESC_Rise_Gain_5, default : 15% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_f_6th           */		       0,	/* Comment [ Front_LFC_ESC_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_1st            */		       0,	/* Comment [ Front_LFC_ESC_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_2nd            */		      60,	/* Comment [ Front_LFC_ESC_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_3rd            */		      70,	/* Comment [ Front_LFC_ESC_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_4th            */		      80,	/* Comment [ Front_LFC_ESC_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_5th            */		     100,	/* Comment [ Front_LFC_ESC_Rise_Duty_5, default : 100 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_f_6th            */		     200,	/* Comment [ Front_LFC_ESC_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_1st           */		     100,	/* Comment [ Rear_LFC_ESC_Rise_Gain_1, default : 100% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_2nd           */		      95,	/* Comment [ Rear_LFC_ESC_Rise_Gain_2, default : 95% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_3rd           */		      60,	/* Comment [ Rear_LFC_ESC_Rise_Gain_3, default : 60% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_4th           */		      40,	/* Comment [ Rear_LFC_ESC_Rise_Gain_4, default : 40% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_5th           */		      15,	/* Comment [ Rear_LFC_ESC_Rise_Gain_5, default : 15% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_r_6th           */		       0,	/* Comment [ Rear_LFC_ESC_Rise_Gain_6, default : 0% ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_1st            */		       0,	/* Comment [ Rear_LFC_ESC_Rise_Duty_1, default : 0 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_2nd            */		      60,	/* Comment [ Rear_LFC_ESC_Rise_Duty_2, default : 60 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_3rd            */		      70,	/* Comment [ Rear_LFC_ESC_Rise_Duty_3, default : 70 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_4th            */		      80,	/* Comment [ Rear_LFC_ESC_Rise_Duty_4, default : 80 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_5th            */		     110,	/* Comment [ Rear_LFC_ESC_Rise_Duty_5, default : 110 ] */
	/* uchar8_t    	U8_LFC_ESC_Rise_Duty_r_6th            */		     200,	/* Comment [ Rear_LFC_ESC_Rise_Duty_6, default : 200 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_1st         */		     200,	/* Comment [ Front_LFC_ESC_Rise_Gain2_1, default : 200% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_2nd         */		     180,	/* Comment [ Front_LFC_ESC_Rise_Gain2_2, default : 180% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_3rd         */		     150,	/* Comment [ Front_LFC_ESC_Rise_Gain2_3, default : 150% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_4th         */		     130,	/* Comment [ Front_LFC_ESC_Rise_Gain2_4, default : 130% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_5th         */		     105,	/* Comment [ Front_LFC_ESC_Rise_Gain2_5, default : 105% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_f_6th         */		     100,	/* Comment [ Front_LFC_ESC_Rise_Gain2_6, default : 100% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_1st          */		       0,	/* Comment [ Front_LFC_ESC_Rise_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_2nd          */		     500,	/* Comment [ Front_LFC_ESC_Rise_Press_2, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_3rd          */		    1000,	/* Comment [ Front_LFC_ESC_Rise_Press_3, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_4th          */		    2000,	/* Comment [ Front_LFC_ESC_Rise_Press_4, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_5th          */		    5000,	/* Comment [ Front_LFC_ESC_Rise_Press_5, default : 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_f_6th          */		    8000,	/* Comment [ Front_LFC_ESC_Rise_Press_6, default : 80bar ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_1st         */		     200,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_1, default : 200% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_2nd         */		     180,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_2, default : 180% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_3rd         */		     150,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_3, default : 150% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_4th         */		     130,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_4, default : 130% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_5th         */		     105,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_5, default : 105% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Gain_2_r_6th         */		     100,	/* Comment [ Rear_LFC_ESC_Rise_Gain2_6, default : 100% ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_1st          */		       0,	/* Comment [ Rear_LFC_ESC_Rise_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_2nd          */		     500,	/* Comment [ Rear_LFC_ESC_Rise_Press_2, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_3rd          */		    1000,	/* Comment [ Rear_LFC_ESC_Rise_Press_3, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_4th          */		    2000,	/* Comment [ Rear_LFC_ESC_Rise_Press_4, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_5th          */		    5000,	/* Comment [ Rear_LFC_ESC_Rise_Press_5, default : 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_LFC_ESC_Rise_Press_r_6th          */		    8000,	/* Comment [ Rear_LFC_ESC_Rise_Press_6, default : 80bar ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_NC_Dump_Gain_f_1st                */		      20,	/* Comment [ Front_NC_Dump_Gain_1, default : 20% ] */
	/* int16_t     	S16_NC_Dump_Gain_f_2nd                */		      40,	/* Comment [ Front_NC_Dump_Gain_2, default : 40% ] */
	/* int16_t     	S16_NC_Dump_Gain_f_3rd                */		      90,	/* Comment [ Front_NC_Dump_Gain_3, default : 90% ] */
	/* int16_t     	S16_NC_Dump_Gain_f_4th                */		     115,	/* Comment [ Front_NC_Dump_Gain_4, default : 115% ] */
	/* int16_t     	S16_NC_Dump_Gain_f_5th                */		     125,	/* Comment [ Front_NC_Dump_Gain_5, default : 125% ] */
	/* int16_t     	S16_NC_Dump_Gain_f_6th                */		     140,	/* Comment [ Front_NC_Dump_Gain_6, default : 140% ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_1st          */		       0,	/* Comment [ Front_NC_Dump_Gain_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_2nd          */		     100,	/* Comment [ Front_NC_Dump_Gain_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_3rd          */		     400,	/* Comment [ Front_NC_Dump_Gain_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_4th          */		     600,	/* Comment [ Front_NC_Dump_Gain_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_5th          */		    1300,	/* Comment [ Front_NC_Dump_Gain_Press_5, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_f_6th          */		    1500,	/* Comment [ Front_NC_Dump_Gain_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_1st          */		       0,	/* Comment [ Rear_NC_Dump_Gain_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_2nd          */		     100,	/* Comment [ Rear_NC_Dump_Gain_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_3rd          */		     200,	/* Comment [ Rear_NC_Dump_Gain_Press_3, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_4th          */		     400,	/* Comment [ Rear_NC_Dump_Gain_Press_4, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_5th          */		     700,	/* Comment [ Rear_NC_Dump_Gain_Press_5, default : 70bar ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_Press_r_6th          */		    1000,	/* Comment [ Rear_NC_Dump_Gain_Press_6, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_NC_Dump_Gain_r_1st                */		      20,	/* Comment [ Rear_NC_Dump_Gain_1, default : 20% ] */
	/* int16_t     	S16_NC_Dump_Gain_r_2nd                */		      50,	/* Comment [ Rear_NC_Dump_Gain_2, default : 50% ] */
	/* int16_t     	S16_NC_Dump_Gain_r_3rd                */		      70,	/* Comment [ Rear_NC_Dump_Gain_3, default : 70% ] */
	/* int16_t     	S16_NC_Dump_Gain_r_4th                */		     135,	/* Comment [ Rear_NC_Dump_Gain_4, default : 135% ] */
	/* int16_t     	S16_NC_Dump_Gain_r_5th                */		     145,	/* Comment [ Rear_NC_Dump_Gain_5, default : 145% ] */
	/* int16_t     	S16_NC_Dump_Gain_r_6th                */		     155,	/* Comment [ Rear_NC_Dump_Gain_6, default : 155% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_1st                */		      10,	/* Comment [ Front_TC_Dump_Gain_1, default : 10% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_2nd                */		      50,	/* Comment [ Front_TC_Dump_Gain_2, default : 50% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_3rd                */		     100,	/* Comment [ Front_TC_Dump_Gain_3, default : 100% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_4th                */		     140,	/* Comment [ Front_TC_Dump_Gain_4, default : 140% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_5th                */		     190,	/* Comment [ Front_TC_Dump_Gain_5, default : 190% ] */
	/* int16_t     	S16_TC_Dump_Gain_f_6th                */		     210,	/* Comment [ Front_TC_Dump_Gain_6, default : 210% ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_1st          */		       0,	/* Comment [ Front_TC_Dump_Gain_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_2nd          */		     100,	/* Comment [ Front_TC_Dump_Gain_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_3rd          */		     400,	/* Comment [ Front_TC_Dump_Gain_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_4th          */		     600,	/* Comment [ Front_TC_Dump_Gain_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_5th          */		    1300,	/* Comment [ Front_TC_Dump_Gain_Press_5, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_f_6th          */		    1500,	/* Comment [ Front_TC_Dump_Gain_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_1st          */		       0,	/* Comment [ Rear_TC_Dump_Gain_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_2nd          */		     100,	/* Comment [ Rear_TC_Dump_Gain_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_3rd          */		     400,	/* Comment [ Rear_TC_Dump_Gain_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_4th          */		     600,	/* Comment [ Rear_TC_Dump_Gain_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_5th          */		    1300,	/* Comment [ Rear_TC_Dump_Gain_Press_5, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_Press_r_6th          */		    1500,	/* Comment [ Rear_TC_Dump_Gain_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Dump_Gain_r_1st                */		      50,	/* Comment [ Rear_TC_Dump_Gain_1, default : 50% ] */
	/* int16_t     	S16_TC_Dump_Gain_r_2nd                */		     110,	/* Comment [ Rear_TC_Dump_Gain_2, default : 110% ] */
	/* int16_t     	S16_TC_Dump_Gain_r_3rd                */		     170,	/* Comment [ Rear_TC_Dump_Gain_3, default : 170% ] */
	/* int16_t     	S16_TC_Dump_Gain_r_4th                */		     190,	/* Comment [ Rear_TC_Dump_Gain_4, default : 190% ] */
	/* int16_t     	S16_TC_Dump_Gain_r_5th                */		     220,	/* Comment [ Rear_TC_Dump_Gain_5, default : 220% ] */
	/* int16_t     	S16_TC_Dump_Gain_r_6th                */		     240,	/* Comment [ Rear_TC_Dump_Gain_6, default : 240% ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_1             */		       0,	/* Comment [ TC_VALVE_Hold_Duty_Map_1, default : 0 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_10            */		      75,	/* Comment [ TC_VALVE_Hold_Duty_Map_10, default : 75 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_11            */		      85,	/* Comment [ TC_VALVE_Hold_Duty_Map_11, default : 85 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_12            */		      95,	/* Comment [ TC_VALVE_Hold_Duty_Map_12, default : 95 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_13            */		     105,	/* Comment [ TC_VALVE_Hold_Duty_Map_13, default : 105 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_14            */		     115,	/* Comment [ TC_VALVE_Hold_Duty_Map_14, default : 115 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_15            */		     135,	/* Comment [ TC_VALVE_Hold_Duty_Map_15, default : 135 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_16            */		     150,	/* Comment [ TC_VALVE_Hold_Duty_Map_16, default : 150 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_2             */		      10,	/* Comment [ TC_VALVE_Hold_Duty_Map_2, default : 10 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_3             */		      20,	/* Comment [ TC_VALVE_Hold_Duty_Map_3, default : 20 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_4             */		      25,	/* Comment [ TC_VALVE_Hold_Duty_Map_4, default : 25 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_5             */		      30,	/* Comment [ TC_VALVE_Hold_Duty_Map_5, default : 30 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_6             */		      35,	/* Comment [ TC_VALVE_Hold_Duty_Map_6, default : 35 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_7             */		      45,	/* Comment [ TC_VALVE_Hold_Duty_Map_7, default : 45 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_8             */		      55,	/* Comment [ TC_VALVE_Hold_Duty_Map_8, default : 55 ] */
	/* uchar8_t    	U8_TC_Valve_Hold_Duty_V_9             */		      65,	/* Comment [ TC_VALVE_Hold_Duty_Map_9, default : 65 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_1           */		       0,	/* Comment [ TC_VALVE_Hold_Press_Map_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_10          */		    4500,	/* Comment [ TC_VALVE_Hold_Press_Map_10, default : 50bar ] */
	                                                        		        	/* ScaleVal[     45 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_11          */		    6000,	/* Comment [ TC_VALVE_Hold_Press_Map_11, default : 61bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_12          */		    7500,	/* Comment [ TC_VALVE_Hold_Press_Map_12, default : 73bar ] */
	                                                        		        	/* ScaleVal[     75 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_13          */		    8500,	/* Comment [ TC_VALVE_Hold_Press_Map_13, default : 85bar ] */
	                                                        		        	/* ScaleVal[     85 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_14          */		   10000,	/* Comment [ TC_VALVE_Hold_Press_Map_14, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_15          */		   13000,	/* Comment [ TC_VALVE_Hold_Press_Map_15, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_16          */		   15000,	/* Comment [ TC_VALVE_Hold_Press_Map_16, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_2           */		      50,	/* Comment [ TC_VALVE_Hold_Press_Map_2, default : 1bar ] */
	                                                        		        	/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_3           */		     100,	/* Comment [ TC_VALVE_Hold_Press_Map_3, default : 1bar ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_4           */		     200,	/* Comment [ TC_VALVE_Hold_Press_Map_4, default : 3bar ] */
	                                                        		        	/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_5           */		     400,	/* Comment [ TC_VALVE_Hold_Press_Map_5, default : 5bar ] */
	                                                        		        	/* ScaleVal[      4 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_6           */		     600,	/* Comment [ TC_VALVE_Hold_Press_Map_6, default : 7bar ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_7           */		    1000,	/* Comment [ TC_VALVE_Hold_Press_Map_7, default : 20bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_8           */		    2000,	/* Comment [ TC_VALVE_Hold_Press_Map_8, default : 30bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TC_Valve_Hold_Press_V_9           */		    3000,	/* Comment [ TC_VALVE_Hold_Press_Map_9, default : 40bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_WPE_ESC_Rise_Hold_Comp_1st         */		       1,	/* Comment [ WPE_ESC_Rise_Hold_Comp_CNT_1, default : 1 ] */
	/* uchar8_t    	U8_WPE_ESC_Rise_Hold_Comp_2nd         */		       2,	/* Comment [ WPE_ESC_Rise_Hold_Comp_CNT_2, default : 2 ] */
	/* uchar8_t    	U8_WPE_ESC_Rise_Hold_Thres_1st        */		       8,	/* Comment [ WPE_ESC_Rise_Hold_Thres_CNT_1, default : 8 ] */
	/* uchar8_t    	U8_WPE_ESC_Rise_Hold_Thres_2nd        */		       5,	/* Comment [ WPE_ESC_Rise_Hold_Thres_CNT_2, default : 5 ] */
	/* uchar8_t    	U8_WPE_ESC_Rise_Hold_Thres_3rd        */		       2,	/* Comment [ WPE_ESC_Rise_Hold_Thres_CNT_3, default : 2 ] */
	/* uchar8_t    	U8_WPE_Straight_Cont_Comp_f           */		     140,	/* Comment [ Front_WPE_Straight_Cont_Comp, default : 140% ] */
	/* uchar8_t    	U8_WPE_Straight_Cont_Comp_r           */		     140,	/* Comment [ Rear_WPE_Straight_Cont_Comp, default : 140% ] */
	/* uchar8_t    	U8_WPE_Straight_Cont_Comp_T           */		     140,	/* Comment [ 2wheel_WPE_Straight_Cont_Comp, default : 140% ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_1                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_1, default : 0 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_2                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_2, default : 0 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_3                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_3, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_1                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_1, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_2                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_2, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_3                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_3, default : 0 ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_1             */		      45,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_1, default : 45% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_2             */		      50,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_2, default : 50% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_3             */		      55,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_3, default : 55% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_4             */		      60,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_4, default : 60% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_5             */		      80,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_5, default : 80% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_f_6             */		     100,	/* Comment [ Front_Wheel_TC_Release_Comp_Gain_6, default : 100% ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_1            */		       0,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_2            */		     100,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_3            */		     300,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_3, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_4            */		     600,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_5            */		    1000,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_5, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_f_6            */		    1500,	/* Comment [ Front_Wheel_TC_Release_Comp_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_1             */		      45,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_1, default : 45% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_2             */		      50,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_2, default : 50% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_3             */		      55,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_3, default : 55% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_4             */		      60,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_4, default : 60% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_5             */		      80,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_5, default : 80% ] */
	/* int16_t     	S16_TC_Rels_Comp_Gain_r_6             */		     100,	/* Comment [ Rear_Wheel_TC_Release_Comp_Gain_6, default : 100% ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_1            */		       0,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_2            */		     100,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_3            */		     300,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_3, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_4            */		     600,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_5            */		    1000,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_5, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TC_Rels_Comp_Press_r_6            */		    1500,	/* Comment [ Rear_Wheel_TC_Release_Comp_Press_6, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_SPIN_SUS_CLR_CNT1                 */		     100,	/* Comment [ Spin suspect clear count (maximum duration)// default 100scan ] */
	/* int16_t     	S16_SPIN_SUS_CLR_CNT2                 */		       3,	/* Comment [ Spin suspect clear count // default 3can ] */
	/* int16_t     	S16_SPIN_SUS_CLR_CNT3                 */		      20,	/* Comment [ Spin suspect clear count // default 20 ] */
	/* int16_t     	S16_SPIN_SUS_DCT_CNT1                 */		       3,	/* Comment [ Spin suspect detection count// 3can ] */
	/* int16_t     	S16_SPIN_SUS_DCT_DV_THR1              */		      48,	/* Comment [ Spin Suspect delta V threshold // default 0.75KPH ] */
	                                                        		        	/* ScaleVal[   0.75 kph ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* int16_t     	S16_SPIN_SUS_DCT_DV_THR2              */		      96,	/* Comment [ Spin suspect delta V threshold// default 1.5KPH ] */
	                                                        		        	/* ScaleVal[    1.5 kph ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
	/* int16_t     	S16_VCA_REQ_CLR_CNT1                  */		     100,	/* Comment [ VCA request clear count (maximum duration)// default 100 ] */
	/* int16_t     	S16_VCA_REQ_CLR_CNT2                  */		       3,	/* Comment [ VCA request clear count// default 3 ] */
	/* int16_t     	S16_VCA_REQ_CLR_CNT3                  */		      20,	/* Comment [ VCA request clear count// default 20 ] */
	/* int16_t     	S16_VCA_REQ_DCT_CNT1                  */		     100,	/* Comment [ VCA request detection count // default 100scan ] */
	/* int16_t     	S16_VCA_REQ_DCT_DV_THR1               */		     128	/* Comment [ VCA request detection delta V threshold// default 2kph ] */
	                                                        		        	/* ScaleVal[      2 kph ]	Scale [ f(x) = (X + 0) * 0.015625 ]  Range   [ -512 <= X <= 511.984375 ] */
};

#define LOGIC_CAL_MODULE_5_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

