#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_19_START


const /*far*/	DATA_APCALESPENG_t	apCalEspEng14 =
{

	/* uint16_t    	apCalEspEngInfo.CDID                  */		0x0000,
	/* uint16_t    	apCalEspEngInfo.CDVer                 */		0x0000,
	/* uchar8_t    	apCalEspEngInfo.MID                   */		0x00,
	/* uchar8_t    	U8_DRIVELINE_TYPE                     */		       0,	/* Comment [ 구동방식(0:2WD, 1:4WD 2H, 2:4WD 4H, 3:4WD AUTO) ] */
	/* uchar8_t    	U8_ENGINE_VOLUME                      */		       0,	/* Comment [ 엔진 배기량(ex : 10 -> 1000cc) ] */
	/* uchar8_t    	U8_FUEL                               */		       0,	/* Comment [ 사용 연료(0:Gasoline, 1:Diesel, 3:LPG) ] */
	/* uchar8_t    	U8_MAX_TORQUE                         */		       0,	/* Comment [ 최대 토크(ex 20 -> 200Nm) ] */
	/* uchar8_t    	U8_TRANSMISSION_TYPE                  */		       0,	/* Comment [ 변속기 TYPE(0:M/T, 1:A/T , 2:CVT) ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOLTAGE_2W_RISE      */		    3000,	/* Comment [ Homo-mu중 2Wheel 동시 Rise시 Motor Volgate 설정 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOLTAGE_HOMO         */		    2500,	/* Comment [ Homo-mu중 1Wheel Rise시 Motor Volgate 설정 ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOLTAGE_INI          */		    8000,	/* Comment [ Initial Rise 제어시 Motor Volgate 설정 ] */
	                                                        		        	/* ScaleVal[        8 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOLTAGE_SPLIT        */		    3000,	/* Comment [ Split-mu 제어시 Motor Volgate 설정 ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOL_HOLD_DUMP        */		       0,	/* Comment [ Hold or Dump mode시 Motor Volgate 설정 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_BTCS_THR1_0                       */		      96,	/* Comment [ .+B가 설정된 순간의 휠속도 대비 현재 휠속도의 △ V를 설정함(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR1_1                       */		      96,	/* Comment [ .+B가 설정된 순간의 휠속도 대비 현재 휠속도의 △ V를 설정함(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR1_2                       */		      96,	/* Comment [ .+B가 설정된 순간의 휠속도 대비 현재 휠속도의 △ V를 설정함(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR1_3                       */		      96,	/* Comment [ .+B가 설정된 순간의 휠속도 대비 현재 휠속도의 △ V를 설정함(U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR2_0                       */		     136,	/* Comment [ 현재 차속 대비 현재 휠속의 △ V를 설정함(MAXIMUM SLIP THRESHOLD, U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     17 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR2_1                       */		     136,	/* Comment [ 현재 차속 대비 현재 휠속의 △ V를 설정함(MAXIMUM SLIP THRESHOLD, U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     17 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR2_2                       */		     136,	/* Comment [ 현재 차속 대비 현재 휠속의 △ V를 설정함(MAXIMUM SLIP THRESHOLD, U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     17 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_BTCS_THR2_3                       */		     136,	/* Comment [ 현재 차속 대비 현재 휠속의 △ V를 설정함(MAXIMUM SLIP THRESHOLD, U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     17 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_BTCS_THR_HOMO_S1                   */		     120,	/* Comment [ Homo-mu 제어중 U16_TCS_SPEED1에서 BTCS 제어 진입 Threshold ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_HOMO_S2                   */		     120,	/* Comment [ Homo-mu 제어중 U16_TCS_SPEED2에서 BTCS 제어 진입 Threshold ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_HOMO_S3                   */		     240,	/* Comment [ Homo-mu 제어중 U16_TCS_SPEED3에서 BTCS 제어 진입 Threshold ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_HOMO_S4                   */		     240,	/* Comment [ Homo-mu 제어중 U16_TCS_SPEED4에서 BTCS 제어 진입 Threshold ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_INC_ROUGH_ROAD            */		      40,	/* Comment [ Rough Road 감지시 BTCS 진입 조건 증가량 설정 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_SYM_SPIN_S1               */		     120,	/* Comment [ BTCS enter threshold value in low speed range(U16_EDC_SPEED1) in Symmetric spin detection state ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_SYM_SPIN_S2               */		     120,	/* Comment [ BTCS enter threshold value in midium speed range(U16_EDC_SPEED2) in Symmetric spin detection  state ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_SYM_SPIN_S3               */		     240,	/* Comment [ BTCS enter threshold value in high speed range(U16_EDC_SPEED3) in Symmetric spin detection  state ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_THR_SYM_SPIN_S4               */		     240,	/* Comment [ BTCS enter threshold value in ultra high speed range(U16_EDC_SPEED4) in Symmetric spin detection state ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_BRK_TH_INC_IN_ESC_CTL          */		      40,	/* Comment [ ESC 제어 중 BTCS 제어 진입 조건 증가량 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	K_Brake_temperature_Overheated_Threshold */		     125,	/* Comment [ 브레이크 온도 추정 값이 이값 이상이면 과열에 의한 TCS 제어 금지 ] */
	                                                        		        	/* ScaleVal[  500 Deg C ]	Scale [ f(x) = (X + 0) * 4 ]  Range   [ -131072 <= X <= 131068 ] */
	/* int16_t     	K_Brake_temperature_Overheated_Threshold_Low */		     100,	/* Comment [ 디스크 과열에 의한 TCS 제어 금지중 추정온도가 이값 이하이면 제어 진입 허용 ] */
	                                                        		        	/* ScaleVal[  400 Deg C ]	Scale [ f(x) = (X + 0) * 4 ]  Range   [ -131072 <= X <= 131068 ] */
	/* uint16_t    	U16_BTCS_PULSE_DOWN_DUMP_TIME_S       */		     150,	/* Comment [ Split-mu 제어중 Pulse Down Time 설정 ] */
	/* uint16_t    	U16_TCS_BRK_CLT_MAX_SPD               */		     560,	/* Comment [ 차속이 이값 이상인 경우 브레이크 제어 금지 ] */
	                                                        		        	/* ScaleVal[     70 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_BTCS_PULSE_DOWN_DUMP_TIME_H        */		      35,	/* Comment [ Homo-mu 제어중 Pulse Down Time 설정 ] */
	/* uchar8_t    	U8_PRESS_DIFF_LR_FOR_SYM_SPIN_F       */		      20,	/* Comment [ Exit condition of the Symmetric spin detection for Front wheel, if pressure difference between left and right wheel >= U8_PRESS_DIFF_LR_FOR_SYM_SPIN_F, symmetric spin detection will be end ] */
	/* uchar8_t    	U8_PRESS_DIFF_LR_FOR_SYM_SPIN_R       */		      30,	/* Comment [ Exit condition of the Symmetric spin detection for Rear wheel, if pressure difference between left and right wheel >= U8_PRESS_DIFF_LR_FOR_SYM_SPIN_R, symmetric spin detection will be end ] */
	/* uint16_t    	U16_SPEED_FOR_BTCS_IN_RUN             */		     160,	/* Comment [ In Split-mu, vehicle speed value to separate in standstill and running acceleration ( for separation of BTCS Rise/Dump rate) ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_ALLOWABLE_RISE_SCAN_IN_STATE1      */		       6,	/* Comment [ STATE1에 머물수 있는 최대 SCAN 수 ] */
	/* uchar8_t    	U8_BTCS_STATE_TRANSITION_CNT          */		       2,	/* Comment [ STATE1에서 이 Parameter에 설정된 Scan 동안 Spin 감소시 STATE2로 전환됨 ] */
	/* uchar8_t    	U8_BTCS_DELTA_SPIN_GAIN               */		      10,	/* Comment [ BTCS제어시 참조하는 Reference Spin 계산시 Spin Error의 가중치 ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_BTCS_LR_SPEED_DELTA_GAIN           */		      10,	/* Comment [ BTCS제어시 참조하는 Reference Spin 계산시 좌우륜 속도차의 가중치 ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_BTCS_SPIN_DEC_RATE_GAIN            */		      20,	/* Comment [ BTCS제어시 참조하는 Reference Spin 계산시 Spin이 감소하는 경우 Spin 변화율의 가중치 ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_BTCS_SPIN_INC_RATE_GAIN            */		      20,	/* Comment [ BTCS제어시 참조하는 Reference Spin 계산시 Spin이 증가하는 경우 Spin 변화율의 가중치 ] */
	                                                        		        	/* ScaleVal[      200 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_BTCS_TARGETSPIN_FOR_SPININDEX      */		      24,	/* Comment [ ControlIndex를 계산하기 위한 Target Spin 이원화(Target Spin을 높이게 되면 상대적으로 ControlIndex가 낮게 계산됨) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_BTCS_INITIAL_RISE_SCAN_FRONT       */		       1,	/* Comment [ FRONT WHEEL 제어 시 적용되는 BTCS 제어 진입 초기 Rise Scan ] */
	/* uchar8_t    	U8_BTCS_INITIAL_RISE_SCAN_REAR        */		       1,	/* Comment [ REAR WHEEL 제어 시 적용되는 BTCS 제어 진입 초기 Rise Scan ] */
	/* uchar8_t    	U8_TCS_FF_RISE_ALLOW_SPEED            */		       0,	/* Comment [ 일정 Scan 동안 State1으로 압력 Rise하는 모드 허용 속도 ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_SPLT_INI_RISE_SCAN_LS_F        */		       1,	/* Comment [ 일정 Scan 동안 State1으로 압력 Rise하는 모드 작동시 Front wheel의 Rise Scan ] */
	/* uchar8_t    	U8_TCS_SPLT_INI_RISE_SCAN_LS_R        */		       1,	/* Comment [ 일정 Scan 동안 State1으로 압력 Rise하는 모드 작동시 Rear wheel의 Rise Scan ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_0_0                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_0_1                */		       5,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_0_2                */		       8,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_0_3                */		      10,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_1_0                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_1_1                */		       5,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_1_2                */		       8,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FH_S1_HOLD_SCAN_1_3                */		      10,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FH_S1_RISE_SCAN                    */		       1,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE1에서 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_FH_MAX            */		     100,	/* Comment [ STATE1에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_FH_MIN            */		     120,	/* Comment [ STATE1에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_0_0              */		     -24,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_0_1              */		     -16,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_0_2              */		      -8,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_0_3              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_1_0              */		     -24,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_1_1              */		     -16,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_1_2              */		      -8,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_DUMP_MARGIN_1_3              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_0_0              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_0_1              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_0_2              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_0_3              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_1_0              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_1_1              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_1_2              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_S2_RISE_MARGIN_1_3              */		       0,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_FH_S2_SD_ARTH_HOLD_SCAN_MAX        */		      50,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FH_S2_SD_ARTH_HOLD_SCAN_MIN        */		      40,	/* Comment [ Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FH_S2_SD_ARTH_MAX_RISE_TH          */		     160,	/* Comment [ Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FH_S2_SD_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FH_S2_SD_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FH_S2_SD_BDTH_HOLD_SCAN            */		      10,	/* Comment [ Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_FH_S2_SI_ARTH_HOLD_SCAN_MAX        */		      30,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FH_S2_SI_ARTH_HOLD_SCAN_MIN        */		      20,	/* Comment [ Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FH_S2_SI_ARTH_MAX_RISE_TH          */		     160,	/* Comment [ Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FH_S2_SI_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FH_S2_SI_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FH_S2_SI_BDTH_HOLD_SCAN            */		      10,	/* Comment [ Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FH_MAX_MAX        */		      95,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FH_MAX_MIN        */		     105,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FH_MIN_MAX        */		     105,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FH_MIN_MIN        */		     110,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_0_0                */		       3,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_0_1                */		       3,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_0_2                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_0_3                */		      10,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_1_0                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_1_1                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_1_2                */		       6,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_1_3                */		      10,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FS_S1_RISE_SCAN                    */		       1,	/* Comment [ HOMO-MU에서 FRONT WHEEL 제어 시 STATE1에서 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FS_S1_HOLD_SCAN_HILL               */		       5,	/* Comment [ Hill 판단중, STATE1에서 적용되는 HOLD SCAN(Front Wheel, Split-mu) ] */
	/* uchar8_t    	U8_FS_S1_RISE_SCAN_HILL               */		       1,	/* Comment [ Hill 판단중, STATE1에서 적용되는 RISE SCAN(Front Wheel, Split-mu) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_FS_MAX            */		      60,	/* Comment [ STATE1에서 적용되는 LFC Duty(Front Wheel, Split-mu, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_FS_MIN            */		      70,	/* Comment [ STATE1에서 적용되는 LFC Duty(Front Wheel, Split-mu, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_0_0              */		     -48,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_0_1              */		     -32,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_0_2              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_0_3              */		      16,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_1_0              */		     -48,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_1_1              */		     -32,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_1_2              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_DUMP_MARGIN_1_3              */		      16,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_0_0              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_0_1              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_0_2              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_0_3              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_1_0              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_1_1              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_1_2              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FS_S2_RISE_MARGIN_1_3              */		       0,	/* Comment [ SPLIT-MU에서 FRONT WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HOLD_SCAN_MAX        */		      30,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HOLD_SCAN_MIN        */		      20,	/* Comment [ Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_MAX_RISE_TH          */		     240,	/* Comment [ Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_BDTH_HOLD_SCAN            */		      25,	/* Comment [ Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HOLD_SCAN_MAX        */		      12,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HOLD_SCAN_MIN        */		       8,	/* Comment [ Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_MAX_RISE_TH          */		     240,	/* Comment [ Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_BDTH_HOLD_SCAN            */		      40,	/* Comment [ Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HS_MAX_IN_RUN        */		      50,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value for Rise rate in case of 'spin < 0  and Control Index = Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HS_MIN_IN_RUN        */		      40,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value ( for max. rise rate ) in case of 'spin < 0  and Control Index = Max. Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SD_BDTH_HS_IN_RUN            */		      30,	/* Comment [ In Split-mu and running acceleration, HOLD SCAN value for Dump Rate in case of 'spin < 0' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HS_MAX_IN_RUN        */		      50,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value for Rise rate in case of 'spin > 0  and Control Index = Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HS_MIN_IN_RUN        */		      30,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value ( for max. rise rate ) in case of 'spin > 0  and Control Index = Max. Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SI_BDTH_HS_IN_RUN            */		      25,	/* Comment [ In Split-mu and running acceleration, HOLD SCAN value for Dump Rate in case of 'spin > 0' (Front Wheel) ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HOLD_HILL_MAX        */		      23,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_HOLD_HILL_MIN        */		      23,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FS_S2_SD_ARTH_RISE_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_AR_MAX_RISE_HILL_TH       */		     160,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FS_S2_SD_BDTH_DUMP_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FS_S2_SD_BDTH_HOLD_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HOLD_HILL_MAX        */		      23,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_HOLD_HILL_MIN        */		      23,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_FS_S2_SI_ARTH_RISE_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_AR_MAX_RISE_HILL_TH       */		     160,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FS_S2_SI_BDTH_DUMP_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_FS_S2_SI_BDTH_HOLD_SCAN_HILL       */		      10,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FS_MAX_MAX        */		      85,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FS_MAX_MIN        */		      95,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FS_MIN_MAX        */		      95,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_FS_MIN_MIN        */		     105,	/* Comment [ STATE2에서 적용되는 LFC Duty(Front Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_0_0            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_0_1            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_0_2            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_0_3            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_1_0            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_1_1            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_1_2            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FH_SYM_S1_HOLD_SCAN_1_3            */		      15,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FH_SYM_S1_RISE_SCAN                */		       1,	/* Comment [ In Symmetric spin detection state,  RISE SCAN value of the BTCS STATE1 (Front Wheel, Homo-mu) ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_0_0          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_0_1          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_0_2          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_0_3          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_1_0          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_1_1          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_1_2          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_DUMP_MARGIN_1_3          */		      64,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_0_0          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_0_1          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_0_2          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_0_3          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_1_0          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_1_1          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_1_2          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_FH_SYM_S2_RISE_MARGIN_1_3          */		     120,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Front Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_ARTH_HS_MAX           */		      30,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value for Rise rate in case of 'spin < 0  and Control Index = Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_ARTH_HS_MIN           */		      25,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value ( for max. rise rate ) in case of 'spin < 0  and Control Index = Max. Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_ARTH_MX_RISE_TH       */		     240,	/* Comment [ In Symmetric spin detection state,  Control Index Threshold value for max. rise rate in case of 'spin < 0' (Front Wheel) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_ARTH_RISE_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, RISE SCAN value  in case of 'spin < 0 and Control Index > Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_BDTH_DUMP_SCAN        */		       2,	/* Comment [ In Symmetric spin detection state, DUMP SCAN value  in case of 'spin < 0 and Control Index < Dump Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SD_BDTH_HOLD_SCAN        */		       5,	/* Comment [ In Symmetric spin detection state, HOLD SCAN value for Dump Rate in case of 'spin < 0' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_ARTH_HS_MAX           */		      30,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value for Rise rate in case of 'spin > 0  and Control Index = Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_ARTH_HS_MIN           */		      25,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value ( for max. rise rate ) in case of 'spin > 0  and Control Index = Max. Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_ARTH_MX_RISE_TH       */		     200,	/* Comment [ In Symmetric spin detection state,  Control Index Threshold value for max. rise rate in case of 'spin > 0' (Front Wheel) ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_ARTH_RISE_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, RISE SCAN value  in case of 'spin > 0 and Control Index > Rise Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_BDTH_DUMP_SCAN        */		       2,	/* Comment [ In Symmetric spin detection state, DUMP SCAN value  in case of 'spin > 0 and Control Index < Dump Threshold' (Front Wheel) ] */
	/* uchar8_t    	U8_FH_SYM_S2_SI_BDTH_HOLD_SCAN        */		       5,	/* Comment [ In Symmetric spin detection state, HOLD SCAN value for Dump Rate in case of 'spin > 0' (Front Wheel) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_0_0                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_0_1                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_0_2                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_0_3                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_1_0                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_1_1                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_1_2                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RH_S1_HOLD_SCAN_1_3                */		      21,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Homo-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RH_S1_RISE_SCAN                    */		       3,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE1에서 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_RH_MAX            */		     100,	/* Comment [ STATE1에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Spin Slope : U8_SPIN_DIFF_FOR_LFC_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_RH_MIN            */		     120,	/* Comment [ STATE1에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Spin Slope : U8_SPIN_DIFF_FOR_LFC_MIN_FOR_LFC) ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_0_0              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_0_1              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_0_2              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_0_3              */		      -8,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_1_0              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_1_1              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_1_2              */		     -16,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_DUMP_MARGIN_1_3              */		      -8,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_0_0              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_0_1              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_0_2              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_0_3              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_1_0              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_1_1              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_1_2              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_S2_RISE_MARGIN_1_3              */		       0,	/* Comment [ HOMO-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_RH_S2_SD_ARTH_HOLD_SCAN_MAX        */		      20,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RH_S2_SD_ARTH_HOLD_SCAN_MIN        */		      10,	/* Comment [ Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RH_S2_SD_ARTH_MAX_RISE_TH          */		     200,	/* Comment [ Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RH_S2_SD_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RH_S2_SD_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RH_S2_SD_BDTH_HOLD_SCAN            */		       1,	/* Comment [ Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_RH_S2_SI_ARTH_HOLD_SCAN_MAX        */		      10,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RH_S2_SI_ARTH_HOLD_SCAN_MIN        */		       5,	/* Comment [ Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RH_S2_SI_ARTH_MAX_RISE_TH          */		     200,	/* Comment [ Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RH_S2_SI_ARTH_RISE_SCAN            */		       2,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RH_S2_SI_BDTH_DUMP_SCAN            */		      10,	/* Comment [ Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RH_S2_SI_BDTH_HOLD_SCAN            */		      15,	/* Comment [ Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RH_MAX_MAX        */		      90,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RH_MAX_MIN        */		     100,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RH_MIN_MAX        */		     100,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RH_MIN_MIN        */		     105,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Homo-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_0_0                */		       3,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_0_1                */		       2,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_0_2                */		       3,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_0_3                */		       4,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_1_0                */		       2,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_1_1                */		       2,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_1_2                */		       5,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_1_3                */		       6,	/* Comment [ STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu, 차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RS_S1_RISE_SCAN                    */		       2,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE1에서 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RS_S1_HOLD_SCAN_HILL               */		       5,	/* Comment [ Hill 판단중, STATE1에서 적용되는 HOLD SCAN(Rear Wheel, Split-mu) ] */
	/* uchar8_t    	U8_RS_S1_RISE_SCAN_HILL               */		       1,	/* Comment [ Hill 판단중, STATE1에서 적용되는 RISE SCAN(Rear Wheel, Split-mu) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_RS_MAX            */		      55,	/* Comment [ STATE1에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST1_RS_MIN            */		      65,	/* Comment [ STATE1에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_0_0              */		     -72,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -9 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_0_1              */		     -72,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -9 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_0_2              */		     -80,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    -10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_0_3              */		     -80,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    -10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_1_0              */		     -72,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -9 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_1_1              */		     -72,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[     -9 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_1_2              */		     -96,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    -12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_DUMP_MARGIN_1_3              */		     -96,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 DUMP Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : DUMP Threshold = TargetΔV + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    -12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_0_0              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_0_1              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_0_2              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_0_3              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_1_0              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_1_1              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_1_2              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RS_S2_RISE_MARGIN_1_3              */		       0,	/* Comment [ SPLIT-MU에서 REAR WHEEL 제어 시 STATE2에서 적용되는 RISE Threshold 설정(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) : RISE Threshold = TargetΔV ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HOLD_SCAN_MAX        */		      70,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HOLD_SCAN_MIN        */		      60,	/* Comment [ Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_MAX_RISE_TH          */		     240,	/* Comment [ Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_BDTH_HOLD_SCAN            */		      50,	/* Comment [ Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HOLD_SCAN_MAX        */		      30,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HOLD_SCAN_MIN        */		      40,	/* Comment [ Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_MAX_RISE_TH          */		     200,	/* Comment [ Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_RISE_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_BDTH_DUMP_SCAN            */		       1,	/* Comment [ Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_BDTH_HOLD_SCAN            */		      50,	/* Comment [ Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HS_MAX_IN_RUN        */		      60,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value for Rise rate in case of 'spin < 0  and Control Index = Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HS_MIN_IN_RUN        */		      40,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value ( for max. rise rate ) in case of 'spin < 0  and Control Index = Max. Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SD_BDTH_HS_IN_RUN            */		      30,	/* Comment [ In Split-mu and running acceleration, HOLD SCAN value for Dump Rate in case of 'spin < 0' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HS_MAX_IN_RUN        */		      70,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value for Rise rate in case of 'spin > 0  and Control Index = Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HS_MIN_IN_RUN        */		      60,	/* Comment [ In Split-mu and running acceleration,  HOLD SCAN value ( for max. rise rate ) in case of 'spin > 0  and Control Index = Max. Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SI_BDTH_HS_IN_RUN            */		      30,	/* Comment [ In Split-mu and running acceleration, HOLD SCAN value for Dump Rate in case of 'spin > 0' (Rear Wheel) ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HOLD_HILL_MAX        */		      23,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_HOLD_HILL_MIN        */		      23,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RS_S2_SD_ARTH_RISE_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_AR_MAX_RISE_HILL_TH       */		     160,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RS_S2_SD_BDTH_DUMP_SCAN_HILL       */		       2,	/* Comment [ Hill 판단중, Spin이 감소 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RS_S2_SD_BDTH_HOLD_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 감소 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HOLD_HILL_MAX        */		      23,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Rise Threshold와 같은 경우 적용되는 HOLD SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_HOLD_HILL_MIN        */		      23,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 HOLD SCAN의 최소값(최대의 Rise Rate) ] */
	/* uchar8_t    	U8_RS_S2_SI_ARTH_RISE_SCAN_HILL       */		       2,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Rise Threshold보다 큰 경우 적용되는 RISE SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_AR_MAX_RISE_HILL_TH       */		     160,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 RISE RATE가 최대가 되는 Control Index Threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RS_S2_SI_BDTH_DUMP_SCAN_HILL       */		       1,	/* Comment [ Hill 판단중, Spin이 증가 경향이고 Control Index가 Dump Threshold보다 작은 경우 적용되는 DUMP SCAN ] */
	/* uchar8_t    	U8_RS_S2_SI_BDTH_HOLD_SCAN_HILL       */		      10,	/* Comment [ Hill 판단중, Spin이 증가 경향일때 Dump Rate 결정하는 HOLD SCAN ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RS_MAX_MAX        */		      80,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RS_MAX_MIN        */		      90,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MAX_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RS_MIN_MAX        */		      90,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MAX_FOR_LFC) ] */
	/* uchar8_t    	U8_TCS_LFC_DUTY_ST2_RS_MIN_MIN        */		     100,	/* Comment [ STATE2에서 적용되는 LFC Duty(Rear Wheel, Split-mu, Delta Spin : U8_DELTA_SPIN_MIN_FOR_LFC, Spin Slope : U8_SPIN_DIFF_MIN_FOR_LFC) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_0_0            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_0_1            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_0_2            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_0_3            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_1_0            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_1_1            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_1_2            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_RH_SYM_S1_HOLD_SCAN_1_3            */		       6,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RH_SYM_S1_RISE_SCAN                */		       2,	/* Comment [ In Symmetric spin detection state,  RISE SCAN value of the BTCS STATE1 (Rear Wheel, Homo-mu) ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_0_0          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_0_1          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_0_2          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_0_3          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_1_0          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_1_1          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_1_2          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_DUMP_MARGIN_1_3          */		      32,	/* Comment [ In Symmetric spin detection state,  DUMP Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) : DUMP Threshold = Target del V + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_0_0          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_0_1          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_0_2          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_0_3          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_1_0          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_1_1          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_1_2          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_RH_SYM_S2_RISE_MARGIN_1_3          */		      80,	/* Comment [ In Symmetric spin detection state,  RISE Threshold value of the BTCS STATE2 (Rear Wheel, Homo-mu, vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) : RISE Threshold = Target del V ± RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_ARTH_HS_MAX           */		      30,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value for Rise rate in case of 'spin < 0  and Control Index = Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_ARTH_HS_MIN           */		      20,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value ( for max. rise rate ) in case of 'spin < 0  and Control Index = Max. Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_ARTH_MX_RISE_TH       */		     240,	/* Comment [ In Symmetric spin detection state,  Control Index Threshold value for max. rise rate in case of 'spin < 0' (Rear Wheel) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_ARTH_RISE_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, RISE SCAN value  in case of 'spin < 0 and Control Index > Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_BDTH_DUMP_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, DUMP SCAN value  in case of 'spin < 0 and Control Index < Dump Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SD_BDTH_HOLD_SCAN        */		       5,	/* Comment [ In Symmetric spin detection state, HOLD SCAN value for Dump Rate in case of 'spin < 0' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_ARTH_HS_MAX           */		      30,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value for Rise rate in case of 'spin > 0  and Control Index = Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_ARTH_HS_MIN           */		      10,	/* Comment [ In Symmetric spin detection state,  HOLD SCAN value ( for max. rise rate ) in case of 'spin > 0  and Control Index = Max. Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_ARTH_MX_RISE_TH       */		     200,	/* Comment [ In Symmetric spin detection state,  Control Index Threshold value for max. rise rate in case of 'spin > 0' (Rear Wheel) ] */
	                                                        		        	/* ScaleVal[     25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_ARTH_RISE_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, RISE SCAN value  in case of 'spin > 0 and Control Index > Rise Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_BDTH_DUMP_SCAN        */		       1,	/* Comment [ In Symmetric spin detection state, DUMP SCAN value  in case of 'spin > 0 and Control Index < Dump Threshold' (Rear Wheel) ] */
	/* uchar8_t    	U8_RH_SYM_S2_SI_BDTH_HOLD_SCAN        */		       5,	/* Comment [ In Symmetric spin detection state, HOLD SCAN value for Dump Rate in case of 'spin > 0' (Rear Wheel) ] */
	/* uchar8_t    	U8_BTCS_HOLD_SCAN_ADD_IN_HOMO         */		       4,	/* Comment [ Homo-mu 제어중 한쪽 Wheel만 Rise 할때 Rise Rate 감소시키기 위한 HOLD SCAN 증가 값 ] */
	/* uchar8_t    	U8_BTCS_HOLD_SCAN_ADD_IN_SPLIT        */		       1,	/* Comment [ Split-mu 제어중 High-side Spin 제어시 Rise Rate 감소를 위한 Hold Scan 증가 값 ] */
	/* uchar8_t    	U8_BTCS_HOLD_SCAN_SUB_IN_SPLIT        */		       2,	/* Comment [ Split-mu 제어중 High-side Spin 제어시 Dump Rate 증가를 위한 Hold Scan 감소 값 ] */
	/* uchar8_t    	U8_TCS_DUMP_SUSPEND_SCAN_S1           */		       0,	/* Comment [ 이 값에 설정되 시간 동안 Dump 유보되고 Hold 작동(차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_DUMP_SUSPEND_SCAN_S2           */		       0,	/* Comment [ 이 값에 설정되 시간 동안 Dump 유보되고 Hold 작동(차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_DUMP_SUSPEND_SCAN_S3           */		       0,	/* Comment [ 이 값에 설정되 시간 동안 Dump 유보되고 Hold 작동(차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_DUMP_SUSPEND_SCAN_S4           */		       0,	/* Comment [ 이 값에 설정되 시간 동안 Dump 유보되고 Hold 작동(차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_RISE_DUMP_TH_OFFSET_IN_STALL       */		      40,	/* Comment [ Engine Stall 감지 시 Rise / Dump Threshold 증대량 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_DUMP_SCAN_IN_STALL             */		       2,	/* Comment [ Engine Stall 감지시 DUMP SCAN 설정 ] */
	/* uchar8_t    	U8_TCS_HOLD_SCAN_IN_STALL             */		      10,	/* Comment [ Engine Stall 감지시 HOLD SCAN 설정 ] */
	/* uchar8_t    	U8_TCS_BRK_HOLD_RESET_OK_SCAN         */		       7,	/* Comment [ U8_TCS_BRK_HOLD_RESET_OK_SCAN에 설정된 값 이상으로 한 제어모드를 지속한 경우만 제어 모드 전환(제어모드 전환 채터링 방지) ] */
	/* uchar8_t    	U8_TCS_HIGH_SIDE_DCT_OK_TIME          */		     200,	/* Comment [ 제어 시작후 이 값에 설정된 시간이후 부터 High Side Spin 감지 시작 ] */
	/* uchar8_t    	U8_TCS_HIGH_SIDE_SPIN_TH              */		      12,	/* Comment [ Split-mu 제어 중 High Side Spin 발생 판단 기준, High Side Wheel의 Spin이 이 값 이상인 경우 High Side Spin으로 판단 ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_HSS_RESET_TH                   */		       8,	/* Comment [ Split-mu 제어 중 High Side Spin 발생 판단 기준, High Side Wheel의 Spin이 이 값 이하인 경우 High Side Spin 해제조건 만족 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_LOW_SIDE_DUMP_SCAN             */		       1,	/* Comment [ Split-mu 제어 중 High Side Spin 발생시 Low side wheel의 Brake 제어 Dump Pattern 설정 ] */
	/* uchar8_t    	U8_TCS_LOW_SIDE_HOLD_SCAN             */		      15,	/* Comment [ Split-mu 제어 중 High Side Spin 발생시 Low side wheel의 Brake 제어 Hold Pattern 설정 ] */
	/* uchar8_t    	U8_TCS_OK_DUMP_SCAN_IN_STATE2         */		       2,	/* Comment [ U8_TCS_OK_DUMP_SCAN_IN_STATE2에 설정된 만큼 dump한 경우 Spin이 다시 일정량 증가하면 State1으로 제어 전환 ] */
	/* uchar8_t    	U8_TCS_PRES_INC_RATIO                 */		      13,	/* Comment [ Brake 제어가 State2에서 State1으로 전환된 경우 Dump Scan을 기준으로 이 비율 만큼 Rise Scan 허용(dump 후 빠른 압력 rise) ] */
	                                                        		        	/* ScaleVal[      130 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_TCS_SPIN_TH_S1_CHANGE_HOMO         */		     104,	/* Comment [ Homo-mu 제어중 U8_TCS_OK_DUMP_SCAN_IN_STATE2만큼 Dump 수행시 Spin이 U8_TCS_SPIN_TH_S1_CHANGE_SPLIT보다 커지면 State1 제어로 전환 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_SPIN_TH_S1_CHANGE_SPLIT        */		     104,	/* Comment [ Split-mu 제어중 U8_TCS_OK_DUMP_SCAN_IN_STATE2만큼 Dump 수행시 Spin이 U8_TCS_SPIN_TH_S1_CHANGE_SPLIT보다 커지면 State1 제어로 전환 ] */
	                                                        		        	/* ScaleVal[     13 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ACC_TH_VCA                         */		      10,	/* Comment [ VCA 작동을 위한 차량 가속도(acc_r)의 Threshold ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_MAX_RISE_SCAN4VCA_MOTOR_RUN        */		      14,	/* Comment [ 모터가 작동중인 상태에서 VCA 작동시 Rise Scan 설정 ] */
	/* uchar8_t    	U8_MAX_RISE_SCAN4VCA_MOTOR_STOP       */		      20,	/* Comment [ 모터가 정지한 상태에서 VCA 작동시 Rise Scan 설정 ] */
	/* uchar8_t    	U8_MAX_VCA_SUSTAIN_SCAN               */		     142,	/* Comment [ VCA 작동 후 여기 설정된 Scan 이 경과하면 작동 종료 ] */
	/* uchar8_t    	U8_SPEED_TH_VCA                       */		      56,	/* Comment [ VCA 작동을 위한 차속의 Threshold ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SPIN_TH_VCA                        */		       0,	/* Comment [ VCA 작동을 위한 Wheel Spin의 Threshold ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VCA_REENGAGE_ALLOW_TIME        */		      42,	/* Comment [ VCA 작동 종료후 이 시간 이내 에는 재진입 못함 ] */
	/* uchar8_t    	U8_VREF_DECREASE_LIMIT_OF_VCA         */		      20,	/* Comment [ VCA 작동 후 차속 감속이 설정된 값을 초과하면 작동 종료 ] */
	                                                        		        	/* ScaleVal[    2.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_ESV_CLOSE_DELAY_SCAN           */		       7,	/* Comment [ 이 값에 설정된 시간만큼 ESV Close Delay됨 ] */
	/* uchar8_t    	U8_TCS_BRK_CMD_RESET_SPIN_SLOPE       */		      16,	/* Comment [ Hold 수행한 Scan이 U8_TCS_MIN_HOLD_SCAN에 설정된 값 이상이고, Spin 변화량이 이값 이상이면 Brake 제어 Command Reset 하여 압력 Rise Mode로 전환함. ] */
	                                                        		        	/* ScaleVal[    0.2 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_BTCS_SPIN_SLOPE_CNT            */		      10,	/* Comment [ Spin 증가 및 감소 경향 판단시 일관된 경향이 여기 설정된 시간 이상 지속되어야 Spin 증가 및 감소 경향 판단 ] */
	/* uchar8_t    	U8_TCS_MIN_HOLD_SCAN                  */		      15,	/* Comment [ 압력 Rise Mode로 전환함. ] */
	/* uchar8_t    	U8_TCS_RISE_DUMP_TH_DEC_HOMO          */		       0,	/* Comment [ Homo-mu 제어중 Spin 증가 경향시 이 값만큼 Rise/Dump Threshold를 감소시켜 조기 Rise Mode 진입 유도 ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_RISE_DUMP_TH_DEC_SPLIT         */		      16,	/* Comment [ Split-mu 제어중 Spin 증가 경향시 이 값만큼 Rise/Dump Threshold를 감소시켜 조기 Rise Mode 진입 유도 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_L2SPLIT_F        */		      20,	/* Comment [ Low to Split Suspect 감지시 보상되는 LFC Duty(Front Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_L2SPLIT_R        */		      20,	/* Comment [ Low to Split Suspect 감지시 보상되는 LFC Duty(Rear Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_STUCK_F          */		      20,	/* Comment [ Stuck 감지시 보상되는 LFC Duty(Front Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_STUCK_R          */		      20,	/* Comment [ Stuck 감지시 보상되는 LFC Duty(Rear Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_VCA_F            */		      20,	/* Comment [ VCA 감지시 보상되는 LFC Duty(Front Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_VCA_R            */		      20,	/* Comment [ VCA 감지시 보상되는 LFC Duty(Rear Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_VIB_F            */		      20,	/* Comment [ Vibration 감지시 보상되는 LFC Duty(Front Wheel, Base Duty +α) ] */
	/* uchar8_t    	U8_TCS_LFC_COMP_DUTY_VIB_R            */		      20,	/* Comment [ Vibration 감지시 보상되는 LFC Duty(Rear Wheel, Base Duty +α) ] */
	/* char8_t     	S8_INS_COMPLETE_CLOSE_THROTTLE        */		      60,	/* Comment [ Acceleration Pedal Throttle Position for Inside Wheel Lift Control Enterance(mtp>S8_INS_COMPLETE_CLOSE_THROTTLE) ] */
	/* uint16_t    	U16_INS_TCS_ALLOWED_ALAT_1            */		       0,	/* Comment [ Lateral acceleration threshold to enterance inside wheel lift control(Vehicle Speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_ALLOWED_ALAT_2            */		     900,	/* Comment [ Lateral acceleration threshold to enterance inside wheel lift control(Vehicle Speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      0.9 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_ALLOWED_ALAT_3            */		     800,	/* Comment [ Lateral acceleration threshold to enterance inside wheel lift control(Vehicle Speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      0.8 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_ALLOWED_ALAT_4            */		     700,	/* Comment [ Lateral acceleration threshold to enterance inside wheel lift control(Vehicle Speed : U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_BEMF_VOLTAGE_INS          */		    6000,	/* Comment [ Motor volgate in inside wheel lift ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_TCS_MSC_INITIAL_VOLTAGE_INS       */		   14000,	/* Comment [ Initial motor volgate in inside wheel lift ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uchar8_t    	U8_INS_BTCS_THR_HOMO                  */		      16,	/* Comment [ BTCS enterance threshold in inside wheel lift activation. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INS_DELVLRLIMIT_1                  */		      80,	/* Comment [ Spin of Inside wheel(Vehicle Speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INS_DELVLRLIMIT_2                  */		      64,	/* Comment [ Spin of Inside wheel(Vehicle Speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INS_DELVLRLIMIT_3                  */		      48,	/* Comment [ Spin of Inside wheel(Vehicle Speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      6 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INS_DELVLRLIMIT_4                  */		      24,	/* Comment [ Spin of Inside wheel(Vehicle Speed : U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_INS_CONTROL_FUNCTION           */		       0,	/* Comment [ Inside Wheel Lift Control Enable, Disable ] */
	/* uchar8_t    	U8_INS_TARGET_SPIN_FOR_BRKINDEX       */		       8,	/* Comment [ Reference target spin to calculate brake control index in inside wheel lift control ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	U16_INS_ACTIVATION_ALLOWED_TIME       */		    1428,	/* Comment [ Maximum activation time in inside wheel lift control ] */
	/* uint16_t    	U16_INS_DELTAMOMENT_ESC_OS            */		     500,	/* Comment [ Delta moment to detection oversteer in inside wheel lift control ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_INS_DELTAMOMENT_OK_ESC_OS         */		     400,	/* Comment [ Delta moment to exit detection oversteer in inside wheel lift control ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_INS_TCS_BRK_CLT_MAX_SPD           */		    1200,	/* Comment [ BTCS entry inhibit in inside wheel lift control(Vehicle Speed > U16_INS_TCS_BRK_CLT_MAX_SPD) ] */
	                                                        		        	/* ScaleVal[    150 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_INS_TCS_EXIT_ALAT_1               */		     500,	/* Comment [ Lateral acceleration threshold to exit inside wheel lift control(Vehicle Speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_EXIT_ALAT_2               */		     500,	/* Comment [ Lateral acceleration threshold to exit inside wheel lift control(Vehicle Speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_EXIT_ALAT_3               */		     500,	/* Comment [ Lateral acceleration threshold to exit inside wheel lift control(Vehicle Speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_TCS_EXIT_ALAT_4               */		     500,	/* Comment [ Lateral acceleration threshold to exit inside wheel lift control(Vehicle Speed : U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_INS_YAW_MAX                       */		     200,	/* Comment [ Maximum delta yaw to determine dump/hold scan in inside wheel lift control ] */
	                                                        		        	/* ScaleVal[    2 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_INS_YAW_MIN                       */		       0,	/* Comment [ Minimum delta yaw to determine dump/hold scan in inside wheel lift control ] */
	                                                        		        	/* ScaleVal[    0 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_INS_PULSE_DOWN_DUMP_TIME           */		      50,	/* Comment [ Pulse Down Time in inside wheel lift control ] */
	/* uchar8_t    	U8_INS_PULSE_DOWN_TIME                */		      30,	/* Comment [ Determine pulse down time in inside wheel lift control(alat < U16_INS_TCS_EXIT_ALAT) ] */
	/* uchar8_t    	U8_INS_PULSE_DOWN_TIME_BY_ESCOS       */		      30,	/* Comment [ Determine pulse down time in inside wheel lift control(delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_FH_S1_HOLD_SCAN_1              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift front wheel control(Vehicle Speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_INS_FH_S1_HOLD_SCAN_2              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift front wheel control(Vehicle Speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_INS_FH_S1_HOLD_SCAN_3              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift front wheel control(Vehicle Speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_INS_FH_S1_HOLD_SCAN_4              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift front wheel control(Vehicle Speed : U16_TCS_SPEED5) ] */
	/* uchar8_t    	U8_INS_FH_S1_RISE_SCAN                */		       3,	/* Comment [ Rise scan of state1 in inside wheel lift front wheel control ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_DUMP_SCAN           */		       1,	/* Comment [ Dump scan of front wheel(Brake control index<=Dump Threshold and UnderSteer) ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_DUMP_SCAN_MAX       */		       1,	/* Comment [ Maximum dump scan of front wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_DUMP_SCAN_MIN       */		       1,	/* Comment [ Minimum dump scan of front wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_HOLD_SCAN           */		      20,	/* Comment [ Hold scan of front wheel(Brake control index<=Dump Threshold and UnderSteer) ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_HOLD_SCAN_MAX       */		       5,	/* Comment [ Maximum hold scan of front wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_FH_S2_BDTH_HOLD_SCAN_MIN       */		       7,	/* Comment [ Minimum hold scan of front wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_FH_S2_SD_ARTH_HOLD_SCAN_MAX    */		      25,	/* Comment [ Hold scan of state2 in inside wheel lift front wheel control(Spin is decreased & Brake control index = Rise threshold) ] */
	/* uchar8_t    	U8_INS_FH_S2_SD_ARTH_HOLD_SCAN_MIN    */		      10,	/* Comment [ Minimum hold scan of state2 in inside wheel lift front wheel control(Spin is decreased, maximum rise rate) ] */
	/* uchar8_t    	U8_INS_FH_S2_SD_ARTH_MAX_RISE_TH      */		     160,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift front wheel control(Spin is decreased) ] */
	/* uchar8_t    	U8_INS_FH_S2_SD_ARTH_RISE_SCAN        */		       1,	/* Comment [ Rise scan of state2 in inside wheel lift front wheel control(Spin is decreased & Brake control index > Rise threshold) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_ARTH_HOLD_SCAN_MAX    */		      30,	/* Comment [ Hold scan of state2 in inside wheel lift front wheel control(Spin is increased & Brake control index = Rise threshold) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_ARTH_HOLD_SCAN_MIN    */		      25,	/* Comment [ Minimum hold scan of state2 in inside wheel lift front wheel control(Spin is increased, maximum rise rate) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_ARTH_MAX_RISE_TH      */		     200,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift front wheel control(Spin is increased) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_ARTH_RISE_SCAN        */		       1,	/* Comment [ Rise scan of state2 in inside wheel lift front wheel control(Spin is increased & Brake control index > Rise threshold) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_AR_HOLD_DEC_MAX       */		       5,	/* Comment [ Decreased hold scan of state2 in inside wheel lift front wheel control(Spin slope is U8_INS_FH_S2_SI_ARSP_MAX_RISE_SP) ] */
	/* uchar8_t    	U8_INS_FH_S2_SI_AR_MAX_RISE_SP        */		      10,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift front wheel control(Spin slope is increased) ] */
	                                                        		        	/* ScaleVal[  0.125 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_INS_RH_S1_HOLD_SCAN_1              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift rear wheel control(Vehicle Speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_INS_RH_S1_HOLD_SCAN_2              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift rear wheel control(Vehicle Speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_INS_RH_S1_HOLD_SCAN_3              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift rear wheel control(Vehicle Speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_INS_RH_S1_HOLD_SCAN_4              */		       5,	/* Comment [ Hold scan of state1 in inside wheel lift rear wheel control(Vehicle Speed : U16_TCS_SPEED5) ] */
	/* uchar8_t    	U8_INS_RH_S1_RISE_SCAN                */		       3,	/* Comment [ Rise scan of state1 in inside wheel lift rear wheel control ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_DUMP_SCAN           */		       1,	/* Comment [ Dump scan of rear wheel(Brake control index<=Dump Threshold and UnderSteer) ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_DUMP_SCAN_MAX       */		       1,	/* Comment [ Maximum dump scan of rear wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_DUMP_SCAN_MIN       */		       1,	/* Comment [ Minimum dump scan of rear wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_HOLD_SCAN           */		      20,	/* Comment [ Hold scan of rear wheel(Brake control index<=Dump Threshold and UnderSteer) ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_HOLD_SCAN_MAX       */		       5,	/* Comment [ Maximum hold scan of rear wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_RH_S2_BDTH_HOLD_SCAN_MIN       */		       7,	/* Comment [ Minimum hold scan of rear wheel(Brake control index<=Dump Threshold or Delta moment > 5Nm) ] */
	/* uchar8_t    	U8_INS_RH_S2_SD_ARTH_HOLD_SCAN_MAX    */		      25,	/* Comment [ Hold scan of state2 in inside wheel lift rear wheel control(Spin is decreased & Brake control index = Rise threshold) ] */
	/* uchar8_t    	U8_INS_RH_S2_SD_ARTH_HOLD_SCAN_MIN    */		      10,	/* Comment [ Minimum hold scan of state2 in inside wheel lift rear wheel control(Spin is decreased, maximum rise rate) ] */
	/* uchar8_t    	U8_INS_RH_S2_SD_ARTH_MAX_RISE_TH      */		     160,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift rear wheel control(Spin is decreased) ] */
	/* uchar8_t    	U8_INS_RH_S2_SD_ARTH_RISE_SCAN        */		       1,	/* Comment [ Rise scan of state2 in inside wheel lift rear wheel control(Spin is decreased & Brake control index > Rise threshold) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_ARTH_HOLD_SCAN_MAX    */		      30,	/* Comment [ Hold scan of state2 in inside wheel lift rear wheel control(Spin is increased & Brake control index = Rise threshold) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_ARTH_HOLD_SCAN_MIN    */		      25,	/* Comment [ Minimum hold scan of state2 in inside wheel lift rear wheel control(Spin is increased, maximum rise rate) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_ARTH_MAX_RISE_TH      */		     200,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift rear wheel control(Spin is increased) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_ARTH_RISE_SCAN        */		       1,	/* Comment [ Rise scan of state2 in inside wheel lift rear wheel control(Spin is increased & Brake control index > Rise threshold) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_AR_HOLD_DEC_MAX       */		       5,	/* Comment [ Decreased hold scan of state2 in inside wheel lift rear wheel control(Spin slope is U8_INS_RH_S2_SI_ARSP_MAX_RISE_SP) ] */
	/* uchar8_t    	U8_INS_RH_S2_SI_AR_MAX_RISE_SP        */		      10,	/* Comment [ Brake control index to be maximum rise rate of state2 in inside wheel lift rear wheel control(Spin slope is increased) ] */
	                                                        		        	/* ScaleVal[  0.125 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* char8_t     	S8_INS_DUMP_MARGIN_1                  */		       4,	/* Comment [ Dump threshold of state2 in front wheel Inside Wheel Lift control(Vehicle Speed : U16_TCS_SPEED2) : DUMP Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_DUMP_MARGIN_2                  */		       4,	/* Comment [ Dump threshold of state2 in front wheel Inside Wheel Lift control(Vehicle Speed : U16_TCS_SPEED3) : DUMP Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_DUMP_MARGIN_3                  */		       4,	/* Comment [ Dump threshold of state2 in front wheel Inside Wheel Lift control(Vehicle Speed : U16_TCS_SPEED4) : DUMP Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_DUMP_MARGIN_4                  */		       4,	/* Comment [ Dump threshold of state2 in front wheel Inside Wheel Lift control(Vehicle Speed : U16_TCS_SPEED5) : DUMP Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + DUMP_MARGIN ] */
	                                                        		        	/* ScaleVal[    0.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_RISE_MARGIN_1                  */		       0,	/* Comment [ Rise threshold of state2 in front wheel Inside Wheel Lift control(Wheel Steer Angle : 90) : RISE Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_RISE_MARGIN_2                  */		       0,	/* Comment [ Rise threshold of state2 in front wheel Inside Wheel Lift control(Wheel Steer Angle : 180) : RISE Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_RISE_MARGIN_3                  */		       0,	/* Comment [ Rise threshold of state2 in front wheel Inside Wheel Lift control(Wheel Steer Angle : 270) : RISE Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_INS_RISE_MARGIN_4                  */		       0,	/* Comment [ Rise threshold of state2 in front wheel Inside Wheel Lift control(Wheel Steer Angle : 360) : RISE Threshold = U8_INS_TARGET_SPIN_FOR_BRKINDEX + RISE_MARGIN ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uint16_t    	ETCS_THR1_0                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_1                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_2                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_3                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR1_4                           */		      56,	/* Comment [ ETCS DETECTION THRESHOLD 1(U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_0                           */		     112,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_1                           */		     112,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_2                           */		     112,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_3                           */		     112,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	ETCS_THR2_4                           */		     112,	/* Comment [ ETCS DETECTION THRESHOLD 2(U16_TCS_SPEED5) ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_START_ENGINE_TORQUE           */		       0,	/* Comment [ 이 값보다 Engine Torque가 커야지 TCS 제어 허용 ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_START_RPM                     */		       0,	/* Comment [ 이 값보다 RPM이 커야지 TCS 제어 허용 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN              */		      16,	/* Comment [ 선회시 ETCS_THR1에서 이 값만큼 ETCS Threshold를 감소시켜 조기 Engine제어 진입 유도 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN              */		      16,	/* Comment [ 선회시 ETCS_THR2에서 이 값만큼 ETCS Threshold를 감소시켜 조기 Engine제어 진입 유도 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_0            */		      16,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_1            */		      16,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR1_DEC_IN_TURN_2            */		      16,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold1이 해당 값만큼 낮아짐(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_0            */		       0,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_1            */		       0,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ETCS_THR2_DEC_IN_TURN_2            */		       0,	/* Comment [ 횡G가 0.2g보다 크고 0.4g보다 작은 선회시 ETCS 진입 Threshold2이 해당 값만큼 낮아짐(U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      0 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	ENG_OK_RPM                            */		     800,	/* Comment [ 엔진 정상적인 경우로 판단하는 RPM을 설정 ] */
	/* uint16_t    	ENG_OK_TORQ                           */		     400,	/* Comment [ 엔진 정상적인 경우의 최소 토크 레벨을 설정 ] */
	                                                        		        	/* ScaleVal[       40 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ENG_STALL_RPM                         */		     300,	/* Comment [ 엔진 스톨로 판단하는 RPM을 설정 ] */
	/* uint16_t    	ENG_STALL_TORQ                        */		     700,	/* Comment [ 엔진 스톨 시의 최소 토크 레벨을 설정 ] */
	                                                        		        	/* ScaleVal[       70 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	ETCS_ENG_STL_TORQ_RISE                */		       1,	/* Comment [ ETCS 작동 중 Engine Stall 감지시 Torque 보상 추가 Rise Rate 설정 ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	FTCS_ENG_STL_TORQ_RISE                */		       1,	/* Comment [ FTCS 작동 중 Engine Stall 감지시 Torque 보상 추가 Rise Rate 설정 ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_0_0               */		     300,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_0_1               */		     400,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[         40 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_0_2               */		     500,	/* Comment [ Minimum Torque Level, [engine rpm : ENG_OK_RPM , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_1_0               */		     480,	/* Comment [ Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[         48 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_1_1               */		     750,	/* Comment [ Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[         75 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_1_2               */		     950,	/* Comment [ Minimum Torque Level, [engine rpm : 2000rpm , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[         95 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_2_0               */		     550,	/* Comment [ Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[         55 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_2_1               */		     950,	/* Comment [ Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[         95 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_2_2               */		    1200,	/* Comment [ Minimum Torque Level, [engine rpm : 3000rpm , Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[        120 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_IN_TURN_0         */		     150,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED1] ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_IN_TURN_1         */		     150,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED2] ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_IN_TURN_2         */		     150,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED3] ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_NEW_ENG_OK_TORQ_IN_TURN_3         */		     150,	/* Comment [ Minimum Torque Level, [at GAIN ADD IN TURN, Vehicle speed  : U16_TCS_SPEED4] ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_ETCS_ENG_STL_TORQ_RISE_RATE        */		       1,	/* Comment [ STALL 감지시 토크 Rise Rate 설정(예를 들면 1:0.1%/1Scan , 2:0.1%/2Scan, 3:0.1%/3Scan 증가) ] */
	/* uchar8_t    	U8_TCS_ENG_STALL_RPM_OFFSET           */		     200,	/* Comment [ RPM 기울기에 상관 없이 RPM이 ENG_STALL_RPM - U8_TCS_ENG_STALL_RPM_OFFSET보다 작으면 Stall 감지하고, ENG_OK_RPM + U8_TCS_ENG_STALL_RPM_OFFSET보다 크면 Stall 감지 해제함. ] */
	/* uchar8_t    	U8_TCS_STALL_DCT_OK_RISE_SCAN         */		       6,	/* Comment [ Split-mu 제어시 압력 Rise Scan이 이 값보다 작으면 Stall 감지하지 않음 ] */
	/* int16_t     	S16_HIGH_RISK_STALL_RPM_FLAT          */		    1500,	/* Comment [ RPM to detect engine stall[Stall rise index is High on the flat] ] */
	/* int16_t     	S16_HIGH_RISK_STALL_RPM_HILL          */		    1800,	/* Comment [ RPM to detect engine stall[Stall rise index is High on the hill] ] */
	/* int16_t     	S16_HIGH_RISK_STALL_TORQ_FLAT         */		     400,	/* Comment [ Minimum torque level at engine stall[Stall rise index is High on the flat] ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HIGH_RISK_STALL_TORQ_HILL         */		     700,	/* Comment [ Minimum torque level at engine stall[Stall rise index is High on the hill] ] */
	                                                        		        	/* ScaleVal[      70 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LOW_RISK_STALL_RPM_FLAT           */		    1100,	/* Comment [ RPM to detect engine stall[Stall rise index is Low on the flat] ] */
	/* int16_t     	S16_LOW_RISK_STALL_RPM_HILL           */		    1200,	/* Comment [ RPM to detect engine stall[Stall rise index is Low on the hill] ] */
	/* int16_t     	S16_LOW_RISK_STALL_TORQ_FLAT          */		     550,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Low on the flat] ] */
	                                                        		        	/* ScaleVal[      55 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_LOW_RISK_STALL_TORQ_HILL          */		     600,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Low on the hill] ] */
	                                                        		        	/* ScaleVal[      60 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MID_RISK_STALL_RPM_FLAT           */		    1100,	/* Comment [ RPM to detect engine stall[Stall rise index is Mid on the flat] ] */
	/* int16_t     	S16_MID_RISK_STALL_RPM_HILL           */		    1200,	/* Comment [ RPM to detect engine stall[Stall rise index is Mid on the hill] ] */
	/* int16_t     	S16_MID_RISK_STALL_TORQ_FLAT          */		     550,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Mid on the flat] ] */
	                                                        		        	/* ScaleVal[      55 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MID_RISK_STALL_TORQ_HILL          */		     600,	/* Comment [ Minimum torque level at engine stall[Stall rise index is Mid on the hill] ] */
	                                                        		        	/* ScaleVal[      60 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_SLOW_TORQ_DEVIATION                */		       7,	/* Comment [ In Torque intervention, difference value between Slow torque request and Fast torque request : ( (Slow torque request - Fast torque request) =  U8_SLOW_TORQ_DEVIATION ) ] */
	/* uint16_t    	U16_TCS_SPEED1                        */		      40,	/* Comment [ ETCS 제어시 적용되는 정차 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED2                        */		      80,	/* Comment [ ETCS 제어시 적용되는 저속/중속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED3                        */		     240,	/* Comment [ ETCS 제어시 적용되는 중속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED4                        */		     640,	/* Comment [ ETCS 제어시 적용되는 중속/고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[     80 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_TCS_SPEED5                        */		     880,	/* Comment [ ETCS 제어시 적용되는 고속 영역 판단 속도 ] */
	                                                        		        	/* ScaleVal[        110 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_AX_RANGE_HIGH_MU_MAX              */		      90,	/* Comment [ High-mu에서 정차중 급가속시 발생되는 최대의 가속도 ] */
	                                                        		        	/* ScaleVal[     0.45 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_AX_RANGE_MAX                      */		      50,	/* Comment [ 해당 차량이 Snow 노면에서 낼수 있는 최대 종가속도(acc_r 기준) ] */
	                                                        		        	/* ScaleVal[     0.25 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_AX_RANGE_MIN                      */		      20,	/* Comment [ 해당 차량이 Ice 노면에서 낼수 있는 최대 종가속도(acc_r 기준) ] */
	                                                        		        	/* ScaleVal[      0.1 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_FACTOR         */		      20,	/* Comment [ 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 P/D Gain Decrease Factor ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_INC_F          */		      20,	/* Comment [ 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 Spin이 Target 보다 작을때 P/D Gain Increase Factor ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_AX_HIGH_MU_MAX_GAIN_INC_F_S        */		      20,	/* Comment [ Split-mu 판단시 차량 종가속도(acc_r참조)가 U16_AX_RANGE_HIGH_MU_MAX인 경우 ETCS 제어시 Spin이 Target 보다 작을때 P/D Gain Increase Factor ] */
	                                                        		        	/* ScaleVal[        2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_1STTORQUE_RANGE_HIGH_MU_MAX_EST_MU */		    2500,	/* Comment [ [1st mue detection] TCS active Torque on High-mue ] */
	                                                        		        	/* ScaleVal[     250 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_1STTORQUE_RANGE_MAX_EST_MU        */		    2000,	/* Comment [ [1st mue detection] TCS active Torque on Mid-mue ] */
	                                                        		        	/* ScaleVal[     200 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_1STTORQUE_RANGE_MIN_EST_MU        */		    1500,	/* Comment [ [1st mue detection] TCS active Torque on Low-mue ] */
	                                                        		        	/* ScaleVal[     150 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQUE_TH_SPEEDUP_OFFSET_MAX      */		    2000,	/* Comment [ [1st mue detection] TCS active Torque speed up offset ] */
	                                                        		        	/* ScaleVal[     200 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_0            */		      64,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_1            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_2            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_0_3            */		      64,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_0            */		      64,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_1            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_2            */		      56,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_HOMO_MU_1_3            */		      64,	/* Comment [ Homo-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_0           */		      64,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_1           */		      64,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_2           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_0_3           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_0           */		      64,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_1           */		      64,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_2           */		      56,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_SPLIT_MU_1_3           */		      64,	/* Comment [ Split-mu 판단 시 Target Spin(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_1          */		      32,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_2          */		      16,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_HOMO_MU_3          */		      16,	/* Comment [ TCS Homo-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_1         */		      32,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_2         */		      16,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_INC_TARGET_SPIN_SPLIT_MU_3         */		      16,	/* Comment [ TCS Split-mu Engine 제어 초기 설정된 값만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_1         */		     200,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_2         */		     100,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_RED_TCS_TIME_TARGET_SPIN_3         */		     100,	/* Comment [ TCS Engine 제어 초기 설정된 시간만큼 Target Spin을 Add함(차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_GT_HOMO            */		       8,	/* Comment [ Homo-mu 제어 중 Target Delta V를 잘 추종하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_GT_SPLT            */		       8,	/* Comment [ Split-mu 제어 중 Target Delta V를 잘 추종하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_ROUGH_ROAD         */		      16,	/* Comment [ Rough Road 감지시 Engine 제어에 사용되는 Target Delta V 증가량 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_INC_SPLIT_HILL         */		      16,	/* Comment [ Split-mu 제어 중 Hill 판단하는 경우 이 값 만큼 Target Delta V를 증가 ] */
	                                                        		        	/* ScaleVal[          2 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_GS_S1               */		      16,	/* Comment [ A/T 사양에서 제어중 UP 발생시 Driving 상황에서 Target Delta V 감소 량 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_GS_S2               */		      16,	/* Comment [ A/T 사양에서 제어중 UP 발생시 Drag 상황에서 Target Delta V 증가량 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_0            */		     100,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.2g 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_1            */		      80,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.2g 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_0_2            */		      40,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.2g 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_0            */		     100,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.5g 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_1            */		      80,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.5g 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_1_2            */		      40,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.5g 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_0            */		     100,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.8g 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_1            */		      90,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.8g 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TARGET_SPIN_IN_TURN_2_2            */		      60,	/* Comment [ 선회 판단시 Target Delta V 감소량 결정하는 Factor(det_alatm : 0.8g 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_TARGET_SPIN_VARI_F_HSS         */		     100,	/* Comment [ High Side Spin 감지시 Target Spin Level 설정(0:Split-mu Target Spin / 100:Homo-mu Target Spin) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_0        */		     180,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_1        */		     180,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_2        */		     120,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_0_3        */		      80,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_0        */		     180,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_1        */		     180,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_2        */		     120,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE0_1ST_CYCLE_D_GAIN_1_3        */		      80,	/* Comment [ 1ST Cycle에서 Phase0에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_0        */		       5,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_1        */		       7,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_2        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_0_3        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_0        */		       5,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_1        */		       7,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_2        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_P_GAIN_1_3        */		      10,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_0        */		      70,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_1        */		      40,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_2        */		      30,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_0_3        */		      30,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_0        */		      50,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_1        */		      40,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_2        */		      30,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE1_1ST_CYCLE_D_GAIN_1_3        */		      30,	/* Comment [ 1ST Cycle에서 Phase1에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_0_0        */		       2,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_0_1        */		       3,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_0_2        */		       4,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_0_3        */		       4,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_1_0        */		       2,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_1_1        */		       3,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_1_2        */		       4,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_P_GAIN_1_3        */		       4,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_0_0        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_0_1        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_0_2        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_0_3        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_1_0        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_1_1        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_1_2        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE2_1ST_CYCLE_D_GAIN_1_3        */		      40,	/* Comment [ 1ST Cycle에서 Phase2에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_0         */		      56,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_1         */		      56,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      7 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_2         */		      64,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_0_3         */		      64,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_0         */		      40,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_1         */		      40,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_2         */		      64,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THRESHOLD_1_3         */		      64,	/* Comment [ Phase2가 시작되는 ΔV 설정(ΔV = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_0           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_1           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_2           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_0_3           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_0           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_1           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_2           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQUE_RISE_RATE_1ST_1_3           */		      30,	/* Comment [ 1ST Cycle 에서 Torque Fast Recovery Mode에 의하여 매 SCAN 증가 되는 토크량(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_0          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_1          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_2          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_0_3          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_0          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_1          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_2          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_1_3          */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_0       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_1       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_2       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_0_3       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_0       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_1       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_2       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_ETCS_TORQ_RISE_LIMIT_SS_1_3       */		      50,	/* Comment [ In Homo-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_0             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_1             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_2             */		      40,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_0_3             */		      20,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_0             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_1             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_2             */		      40,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_FTCS_PHASE0_D_GAIN_1_3             */		      20,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase0 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_0             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_1             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_2             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_0_3             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_0             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_1             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_2             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_P_GAIN_1_3             */		      10,	/* Comment [ In Split mu, P gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_0             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_1             */		      40,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_2             */		      30,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_0_3             */		      30,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_0             */		     120,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_1             */		      40,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_2             */		      30,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_FTCS_PHASE1_D_GAIN_1_3             */		      30,	/* Comment [ In Split mu, D gain value of the 1st Cycle, Phase1 (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_0          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_1          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_2          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_0_3          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_0          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_1          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_2          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_ENTER_PHASE2_THR_1_3          */		     240,	/* Comment [ In Split mu, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + ENTER_PHASE2_THRESHOLD) (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_0        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_1        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_2        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_0_3        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_0        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_1        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_2        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FTCS_TORQ_RISE_RATE_1ST_1_3        */		      30,	/* Comment [ In Split-mu, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_0          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_1          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_2          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_0_3          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_0          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_1          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_2          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_1_3          */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed >= 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_0       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_1       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_2       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_0_3       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MIN, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_0       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_1       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_2       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_FTCS_TORQ_RISE_LIMIT_SS_1_3       */		      50,	/* Comment [ In Split-mu, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle and 'vehicle speed < 5kph' (vehicle mu level : U16_AX_RANGE_MAX, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_0_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_1_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE0_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase0 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_0_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_1_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_0_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_1_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE1_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 1st Cycle, Phase1 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_0          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_1          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_0_2          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_0          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_1          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_1_2          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_0          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_1          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_ENTER_PHASE2_THR_TURN_2_2          */		      12,	/* Comment [ In a turn control, delta V value for starting point of the Phase2 (delta V = TARGET_SPIN + U8_ENTER_PHASE2_THR_TURN) (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    1.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_0        */		      10,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    1 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_1        */		      15,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[  1.5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_0_2        */		      15,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[  1.5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_0        */		      20,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    2 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_1        */		      20,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    2 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_1_2        */		      20,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    2 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_0        */		      25,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[  2.5 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_1        */		      30,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_1ST_TURN_2_2        */		      30,	/* Comment [ In a turn control, torque value increased by Torque Fast Recovery Mode each scan in 1ST Cycle  (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    3 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle(det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_0_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_1_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_0          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_1          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle  (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_RISE_LIMIT_TURN_2_2          */		     300,	/* Comment [ In a turn control, Max. Rise limit value of the torque increased by Torque Fast Recovery Mode in 1ST Cycle (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[   30 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_1          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_2          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_0_3          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_1          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_2          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_P_GAIN_1_3          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_0          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_2          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_0_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_0          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_2          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_HOMO_MU_D_GAIN_1_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_0          */		       3,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_1          */		       3,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_2          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_0_3          */		       3,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_0          */		       3,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_1          */		       3,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_2          */		       2,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_P_GAIN_1_3          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_0          */		     140,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_1          */		     100,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_2          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_0_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_0          */		     140,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_1          */		     100,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_2          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_HOMO_MU_D_GAIN_1_3          */		      40,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_0          */		       7,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_1          */		       7,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_2          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_0_3          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_0          */		       7,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_2          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_P_GAIN_1_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_0          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_1          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_2          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_0_3          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_0          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_1          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_2          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_HOMO_MU_D_GAIN_1_3          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_2          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_0_3          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_0          */		       5,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_1          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_2          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_P_GAIN_1_3          */		      20,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_0          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_1          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_2          */		      60,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_0_3          */		      30,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_0          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_1          */		      70,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_2          */		      50,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_HOMO_MU_D_GAIN_1_3          */		      10,	/* Comment [ HOMO-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_0         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_1         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_2         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_0_3         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_0         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_1         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_2         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_P_GAIN_1_3         */		      12,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_0         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_0_3         */		      30,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_0         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE3_SPLIT_MU_D_GAIN_1_3         */		      30,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase3에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_1         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_2         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_0_3         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_1         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_2         */		       2,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_P_GAIN_1_3         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_0         */		     180,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_1         */		     120,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_2         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_0_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_0         */		     160,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_1         */		     120,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_2         */		      50,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE4_SPLIT_MU_D_GAIN_1_3         */		      40,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase4에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_0         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_1         */		      30,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_2         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_0_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_0         */		      30,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_1         */		      30,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_P_GAIN_1_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_1         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_0_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_1         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_2         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE5_SPLIT_MU_D_GAIN_1_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase5에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_2         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_0_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_0         */		       5,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_1         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_2         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_P_GAIN_1_3         */		      20,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 P gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_0         */		     100,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_1         */		      90,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_2         */		      80,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_0_3         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_0         */		     100,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_1         */		      90,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_2         */		      80,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_PHASE6_SPLIT_MU_D_GAIN_1_3         */		      10,	/* Comment [ SPLIT-MU로 판단된 경우 2ND Cycle에서 Phase6에 적용되는 D gain(차량가속도 : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_0        */		       7,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_1        */		       8,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_0_2        */		       7,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_0        */		       6,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_1_2        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_P_GAIN_2_2        */		      10,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_0        */		      15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_1        */		      20,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_0_2        */		      15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_0        */		      15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_1        */		      20,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_1_2        */		      15,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE3_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase3 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_1        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_0_2        */		       4,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_0        */		       5,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_1        */		       4,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_1_2        */		       3,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_0        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_1        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_0_2        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_0        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_1        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_1_2        */		       3,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE4_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase4 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_0        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_1        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_0_2        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_0        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_1        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_1_2        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_0        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_1        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_0_2        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_0        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_1        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_1_2        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE5_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase5 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_0        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_1        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_0_2        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_0        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_1        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_1_2        */		       1,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_P_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added P gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_0        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_1        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_0_2        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.15G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_0        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_1        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_1_2        */		       1,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.3G, vehicle speed : U16_TCS_SPEED4) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_0        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED2) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_1        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED3) ] */
	/* char8_t     	S8_PHASE6_COMP_TURN_D_GAIN_2_2        */		       0,	/* Comment [ In a turn control, Added D gain value for gain compensation of the 2nd Cycle, Phase6 (det_alatm : 0.8G, vehicle speed : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_0      */		       4,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED1, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_1      */		       4,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED2, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_2      */		       3,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED3, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_3      */		       2,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED4, 종가속도 : U16_AX_RANGE_MIN) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_0      */		       4,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED1, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_1      */		       4,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED2, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_2      */		       2,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED3, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_3      */		       2,	/* Comment [ High Side Spin 감지시 P Gain 증가량(속도 : U16_TCS_SPEED4, 종가속도 : U16_AX_RANGE_MAX) ] */
	/* uchar8_t    	U8_HIGH_SIDE_SPIN_ENTRY_THR           */		      32,	/* Comment [ Split-mu 제어중 High-Side Spin으로 판단하기 위한 Spin 조건(U8_HIGH_SIDE_SPIN_ENTRY_THR > U8_HIGH_SIDE_SPIN_EXIT_THR 되도록 설정 해야함) ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HIGH_SIDE_SPIN_EXIT_THR            */		      16,	/* Comment [ Split-mu 제어중 High-Side Spin 판단을 해제 하기 위한 Spin 조건(U8_HIGH_SIDE_SPIN_ENTRY_THR > U8_HIGH_SIDE_SPIN_EXIT_THR 되도록 설정 해야함) ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	U16_ONE_WHEEL_SPIN_LIMIT              */		      80,	/* Comment [ 한쪽 Wheel에만 10KPH 이상의 Spin이 발생하는 SCAN이 이 Parameter에 설정된 값보다 큰 경우 SPLIT-MU 제어로 전환 ] */
	/* uint16_t    	U16_TCS_L2SPLIT_SUSPECT1              */		      40,	/* Comment [ SplitTransitionCnt가 이 값보다 커지면 Split-mu 노면인지 확인하기 위하여 브레이크 압력을 증압하고 엔진토크를 일정량 증가시키는 모드가 작동함 ] */
	/* uchar8_t    	U8_H2S_SUSPECT_QUIT_DELTA_SPD         */		      40,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 좌우 속도차가 이값 이내로 들어오면 즉시 Dump하여 두 wheel 속도가 같아 지도록 유도함 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_P_RISE_TH           */		      40,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어 압력 시 Rise Threshold ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_THR1                */		       8,	/* Comment [ Homo-mu 제어 중 (구동륜중 속도가 낮은 Wheel 속도 < U8_HOMO2SP_DETECT_THR1) 이고 (구동륜중 속도가 높은 Wheel 속도 > U8_HOMO2SP_DETECT_THR2) 인 경우 Split-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_THR2                */		      64,	/* Comment [ Homo-mu 제어 중 (구동륜중 속도가 낮은 Wheel 속도 < U8_HOMO2SP_DETECT_THR1) 이고 (구동륜중 속도가 높은 Wheel 속도 > U8_HOMO2SP_DETECT_THR2) 인 경우 Split-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_WO_B_CTL_THR1       */		      24,	/* Comment [ 브레이크 제어가 없는 경우 한쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR1보다 작고 다른 쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR2보다 큰 경우 SplitTransitionCnt 증가시킴 ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_HOMO2SP_DETECT_WO_B_CTL_THR2       */		     160,	/* Comment [ 브레이크 제어가 없는 경우 한쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR1보다 작고 다른 쪽 Wheel의 Spin이 U8_HOMO2SP_DETECT_WO_B_CTL_THR2보다 큰 경우 SplitTransitionCnt 증가시킴 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_BRK_INPUT_FOR_L2SPLT_F         */		       7,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 Front Wheel의 브레이크 제어 압력 결정(추정압력 기준 ] */
	/* uchar8_t    	U8_TCS_BRK_INPUT_FOR_L2SPLT_R         */		       7,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 Rear Wheel의 브레이크 제어 압력 결정(추정압력 기준 ] */
	/* uchar8_t    	U8_TCS_HOLD_SCAN_L2SPLT_SUSPECT       */		      14,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Hold Scan 결정 ] */
	/* uchar8_t    	U8_TCS_HOLD_SCAN_L2SPLT_SUSPECT_F     */		      14,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Front Hold Scan 결정 ] */
	/* uchar8_t    	U8_TCS_HOLD_SCAN_L2SPLT_SUSPECT_R     */		      14,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Rear Hold Scan 결정 ] */
	/* uchar8_t    	U8_TCS_RISE_SCAN_L2SPLT_SUSPECT       */		       1,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Rise Scan 결정 ] */
	/* uchar8_t    	U8_TCS_RISE_SCAN_L2SPLT_SUSPECT_F     */		       1,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Front Rise Scan 결정 ] */
	/* uchar8_t    	U8_TCS_RISE_SCAN_L2SPLT_SUSPECT_R     */		       1,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 브레이크 제어의 Rear Rise Scan 결정 ] */
	/* uchar8_t    	U8_TCS_TORQ_INPUT_FOR_L2SPLT          */		      80,	/* Comment [ SplitTransitionCnt가 U16_TCS_L2SPLIT_SUSPECT1에 설정된 값보다 큰 경우 Split-mu 노면인지 확인하기 위한 엔진 토크의 증가량 결정 ] */
	                                                        		        	/* ScaleVal[       8 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HUNTING_DETECT_SPEED_DIF          */		     160,	/* Comment [ Spin 좌우 교번시 두 휠의 속도차이 이 값이상이면 Homo-mu로 전환 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_HOMO_TRAN_YAWRATE             */		    1000,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 Yaw Rate 조건 ] */
	                                                        		        	/* ScaleVal[   10 Deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_BOTH_WHEEL_SPIN_LIMIT             */		     100,	/* Comment [ 구동륜 두 Wheel 모두에 5KPH 이상의 Spin이 발생하고, 두 구동륜의 속도차가 1KPH 미만인 SCAN이 Parameter에 설정된 값보다 큰 경우 HOMO-MU 제어로 전환 ] */
	/* uint16_t    	U16_BOTH_WHEEL_SPIN_LIMIT_4WD         */		      15,	/* Comment [ 4WD 차량중 대각의 휠 속도가 반대편 휠 속도보다 크고, 차속 대비 최소 속도 레벨을 지니고 있을 시 count 증가되며 해당 값보다 큰 경우 homo전환 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR1                */		      16,	/* Comment [ Split-mu 제어 중 (좌우 속도차<U8_SP2HOMO_DETECT_THR1) 이고 (구동륜 중 속도가 낮은 Wheel의 Spin > U8_SP2HOMO_DETECT_THR2) 인 경우 Homo-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR2                */		      64,	/* Comment [ Split-mu 제어 중 (좌우 속도차<U8_SP2HOMO_DETECT_THR1) 이고 (구동륜 중 속도가 낮은 Wheel의 Spin > U8_SP2HOMO_DETECT_THR2) 인 경우 Homo-mu 전환 Counter 증가 ] */
	                                                        		        	/* ScaleVal[      8 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SP2HOMO_DETECT_THR_4WD             */		      32,	/* Comment [ 4WD차량 중 후륜의 좌우 속도와 차속의 차가 해당 값보다 작으면 homo 전환 count 증가 ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_CTL_START_HOMO_AY              */		     180,	/* Comment [ Ay Model에 의한 횡가속도 계산 값이 이 값 이상이면 Homo-mu 제어로 전환이 진행됨 ] */
	                                                        		        	/* ScaleVal[     0.18 G ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_TCS_HOMO_TRAN_ADD_CNT              */		       4,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 Counter 증가량 ] */
	/* uchar8_t    	U8_TCS_HOMO_TRAN_MAX_SPD              */		      80,	/* Comment [ 선회 후 가속 시 Split-mu 감지 시 Homo-mu 전환 차속 조건 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_INI_MU_DCT_TIME                */		      14,	/* Comment [ Split-mu 제어 시작 초기 이 값에 설정된 시간 이내에 반대편 휠 Spin이 ETCS_THR1에 설정된 값보다 클 경우 Homo-mu 제어로 전환됨 ] */
	/* int16_t     	S16_LOW_SPD_RPM                       */		    1700,	/* Comment [ available maximum engine RPM at the low speed ] */
	/* int16_t     	S16_RED_ZONE_RPM                      */		    5500,	/* Comment [ available maximum engine RPM at the red zone ] */
	/* char8_t     	S8_TCS_RPM_SLOPE_MAXLIMIT             */		       0,	/* Comment [ maximum rpm slope to rpm limit control ] */
	/* uint16_t    	U16_TORQ_DROP_AMOUNT_EX_RPM           */		     100,	/* Comment [ When the actual engine RPM exceeded available maximum engine RPM, added reduction request engine torque. ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_DROP_PERIOD_EX_RPM           */		     100,	/* Comment [ When the actual engine RPM exceeded available maximum engine RPM, period of control activation. ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* int16_t     	S16_HIGH_SPD_MIN_RPM                  */		    3000,	/* Comment [ available minimum engine RPM at the high speed ] */
	/* int16_t     	S16_LOW_SPD_MIN_RPM                   */		    1500,	/* Comment [ available minimum engine RPM at the low speed ] */
	/* char8_t     	S8_TCS_RPM_SLOPE_MINLIMIT             */		       0,	/* Comment [ minimum rpm slope to rpm limit control ] */
	/* uint16_t    	U16_TORQ_UP_AMOUNT_UN_RPM             */		     100,	/* Comment [ When the actual engine RPM under available minimum engine RPM, added increasing request engine torque. ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TORQ_UP_PERIOD_UN_RPM             */		     100,	/* Comment [ When the actual engine RPM under available minimum engine RPM, period of control activation. ] */
	                                                        		        	/* ScaleVal[      10 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_P_GAIN_INC_AFTER_EEC_0G1           */		       5,	/* Comment [ EEC 제어후 Fade Out Mode 감지시 det_alatm과 alat의 차이가 0.1g 인 경우 P Gain의 증가량 설정 ] */
	/* uchar8_t    	U8_P_GAIN_INC_AFTER_EEC_0G8           */		       5,	/* Comment [ EEC 제어후 Fade Out Mode 감지시 det_alatm과 alat의 차이가 0.8g 인 경우 P Gain의 증가량 설정 ] */
	/* uchar8_t    	U8_TORQ_FAST_UP_AFTER_EEC_SCAN        */		       1,	/* Comment [ EEC 제어후 Fade Out Mode 감지 민감도 설정(증가시켜면 둔감하게 감지됨) ] */
	/* int16_t     	S16_TCS_BACKUP_ENTER_AY               */		     300,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 lateral acceleration. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_TCS_BACKUP_ENTER_YAW              */		     300,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 delta yaw. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[  3 deg/sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TCS_BACKUP_RISE_START_TIME        */		     420,	/* Comment [ Δyaw가 3deg/s 이하이고, 발생하는 Spin이 Target Spin의 1/2인 시간이 이 값 이상이면 Back-up 모드 작동하여 토크 빠르게 회복 ] */
	                                                        		        	/* ScaleVal[    4.2 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uint16_t    	U16_TCS_RED_ENG_TORQ_IN_GSC           */		     200,	/* Comment [ Backup Torque에 의한 제어 해제 시 기준 Torque Level 설정 적용에서 Gear 변속 시 cal_torq와 eng_torq와의 차이가 해당 값보다 크면 기준 Torque Level을 cal_torq로 설정함 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_1ST_RISE_DURATION       */		      46,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 지속 시간 ] */
	                                                        		        	/* ScaleVal[   0.46 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_BACKUP_1ST_RISE_RATE           */		       5,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 0.5 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_RPM_SLOPE         */		      12,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 RPM Slope. 해당 값보다 작아야 진입 조건 만족 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_THR1              */		       4,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_BACKUP_ENTER_THR2              */		       2,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin diff error. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[ 0.025 km/h ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_BACKUP_LAST_RISE_RATE          */		       5,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 마지막 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 0.5 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_BACKUP_MONITOR_TIME            */		      49,	/* Comment [ Back-up로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드 작동 후 휠거동 모니터링 시간 ] */
	                                                        		        	/* ScaleVal[   0.49 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_BACKUP_TORQUE_IMPROVE          */		       1,	/* Comment [ Backup Torque에 의한 제어 해제 시 기준 Torque Level 설정 적용 유무 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_CLR_RPM             */		    3500,	/* Comment [ 이 RPM 이상인 경우 Split Hill 감지 해제 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_CLR_TIME            */		     180,	/* Comment [ Split Hill 감지 후 counter가 이 값 보다 작아지면 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[        1.8 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_DCT_RPM             */		    3000,	/* Comment [ 이 RPM 이하에서만 Split Hill 감지 허용 ] */
	/* int16_t     	S16_TCS_SPLT_HILL_DCT_TIME            */		     150,	/* Comment [ Split Hill 경향이 이 시간 동안 지속되면 Split Hill 감지 ] */
	                                                        		        	/* ScaleVal[        1.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_TCS_ENABLE_SPLT_HILL_DCT           */		       1,	/* Comment [ Split Hill 감지 로직 작동 허용 여부 Setting ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_CLR_ACCEL            */		      30,	/* Comment [ 이 차체 가속도 이상인 경우 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[     0.15 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_CLR_SPD              */		     120,	/* Comment [ 이 속도 이상인 경우 Split Hill 감지 해제 ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_ACCEL            */		      20,	/* Comment [ 이 차체 가속도 이하에서만 Split Hill 감지를 허용 ] */
	                                                        		        	/* ScaleVal[      0.1 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_PRESS_FRNT       */		      20,	/* Comment [ FWD 차량의 Split Hill 감지 시작 Brake Pressure ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_PRESS_REAR       */		      40,	/* Comment [ RWD 차량의 Split Hill 감지 시작 Brake Pressure ] */
	/* uchar8_t    	U8_TCS_SPLT_HILL_DCT_SPD              */		      80,	/* Comment [ 이 속도 이하에서만 Split Hill 감지를 허용 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_GD_TRACK_SET_TIME              */		     125,	/* Comment [ Spin이 Target을 U8_TCS_GD_TRACK_SET_TIME동안 잘 추종하는 경우 U8_TCS_TORQ_UP_IN_GD_TRACKING만큼 토크 증가함 ] */
	/* uchar8_t    	U8_TCS_TORQ_UP_IN_GD_TRACKING         */		      50,	/* Comment [ Spin이 Target을 U8_TCS_GD_TRACK_SET_TIME동안 잘 추종하는 경우 U8_TCS_TORQ_UP_IN_GD_TRACKING만큼 토크 증가함 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TCS_1ST_SCAN_TRQ_MOD_HILL         */		     200,	/* Comment [ Hill 등판중, 제어 진입시 첫 Scan의 토크 저감량에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_1ST_SCAN_TRQ_MOD_IN_GS        */		     200,	/* Comment [ Gear 변속중, 제어 진입시 첫 Scan의 토크 저감량에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_0          */		      70,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_1          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_2          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_0_3          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_0          */		      70,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_1          */		      85,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_2          */		      85,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_1_3          */		      85,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_0          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_1          */		      80,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_2          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HOMO_2_3          */		      90,	/* Comment [ Homo-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_0         */		      75,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_1         */		      75,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_0_2         */		      70,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_0         */		      70,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_1         */		      70,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_1_2         */		      80,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_0         */		      70,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_1         */		      75,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_HTURN_2_2         */		      85,	/* Comment [ Homo-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_0         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_1         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_2         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_0_3         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MIN, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_0         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_1         */		      85,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_2         */		      85,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_1_3         */		      85,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_0         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED1) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_1         */		      80,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_2         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_SPLIT_2_3         */		      90,	/* Comment [ Split-mu 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(차량 가속도(acc_r) : U16_AX_RANGE_HIGH_MU_MAX, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_0         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_1         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_0_2         */		      70,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_LM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_0         */		      80,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_1         */		      80,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_1_2         */		      80,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_MM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_0         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED2) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_1         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED3) ] */
	/* uchar8_t    	U8_TCS_1ST_SCAN_TRQ_STURN_2_2         */		      85,	/* Comment [ Split-mu 선회 제어 진입시 첫 Scan에 Engine Torque 대비 해당 값만큼 적용됨(det_alatm : S16_HM_EMS, 차속 : U16_TCS_SPEED4) ] */
	/* uchar8_t    	U8_TCS_DISCRETE_TORQ_LIMIT            */		      10,	/* Comment [ limit value of the discrete torque reduction and recovery : actual discrete torque value <=max_torq*U8_TCS_DISCRETE_TORQ_LIMIT/100 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_HIGH_H           */		      64,	/* Comment [ In Homo-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.8 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_LOW_H            */		       8,	/* Comment [ In Homo-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.1 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_MID_H            */		      32,	/* Comment [ In Homo-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_HIGH_H           */		      10,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_HIGH_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_HIGH_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_LOW_H            */		       3,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_LOW_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_LOW_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_MID_H            */		       5,	/* Comment [ In Homo-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_MID_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_MID_H  / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_HIGH_H          */		      64,	/* Comment [ In Homo-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.8 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_LOW_H           */		       8,	/* Comment [ In Homo-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.1 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_MID_H           */		      32,	/* Comment [ In Homo-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_HIGH_H           */		       0,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_HIGH_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_HIGH_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_LOW_H            */		       0,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_LOW_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_LOW_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_MID_H            */		       0,	/* Comment [ In Homo-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_MID_H' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_MID_H / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_HIGH_SP          */		      64,	/* Comment [ In Split-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.8 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_LOW_SP           */		       8,	/* Comment [ In Split-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.1 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RECOVERY_MID_SP           */		      32,	/* Comment [ In Split-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque recovery ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_HIGH_SP          */		      10,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_HIGH_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_HIGH_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_LOW_SP           */		       3,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_LOW_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_LOW_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REC_TORQ_MID_SP           */		       5,	/* Comment [ In Split-mu, Discrete torque recovery value in case of 'spin error difference =  U8_TCS_FAST_RECOVERY_MID_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_REC_TORQ_MID_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_HIGH_SP         */		      64,	/* Comment [ In Split-mu, spin error difference value[high value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.8 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_LOW_SP          */		       8,	/* Comment [ In Split-mu, spin error difference value[low value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.1 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_REDUCTION_MID_SP          */		      32,	/* Comment [ In Split-mu, spin error difference value[mid value for reduction torque interpolation] for Discrete torque reduction ] */
	                                                        		        	/* ScaleVal[    0.4 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_HIGH_SP          */		      10,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_HIGH_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_HIGH_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_LOW_SP           */		       3,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_LOW_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_LOW_SP / 100) ] */
	/* uchar8_t    	U8_TCS_FAST_RED_TORQ_MID_SP           */		       5,	/* Comment [ In Split-mu, Discrete torque reduction value in case of 'spin error difference =  U8_TCS_FAST_REDUCTION_MID_SP' (actual reduction value = (current base torque level - previous base torque level)* U8_TCS_FAST_RED_TORQ_MID_SP / 100) ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_0             */		     400,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_1             */		     400,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_2             */		     500,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIMIT_SPLIT_3             */		     500,	/* Comment [ Split-mu 제어 시 토크의 최소값 제한(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_LIM_MOD_SPLIT_HILL        */		     200,	/* Comment [ Hill 등판중, Split-mu 제어 시 토크의 최소값 제한에 대한 보상량 ] */
	                                                        		        	/* ScaleVal[         20 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_0          */		      60,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[    7.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_1          */		      60,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[    7.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_2          */		      60,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[    7.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_DEC_TH_SPLIT_3          */		      60,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit 저감 시작함(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[    7.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_0          */		      80,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED1) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_1          */		      80,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_2          */		      80,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_TCS_T_TRQ_MIN_TH_SPLIT_3          */		      80,	/* Comment [ Split-mu 제어시 Spin이 이 값 이상이면 Torque Limit이 ENG_OK_TORQ까지 저감됨(차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_TCS_TARGET_TORQ_DISABLE_HSS        */		      80,	/* Comment [ High Side Spin이 이 값 이상이면 Target Torque는 ENG_OK_TORQ까지 감소됨 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_TARGET_TORQ_INC_RATE           */		      30,	/* Comment [ Target Torque의 SCAN 당 증가량 최대값 설정 ] */
	                                                        		        	/* ScaleVal[  3 Nm/SCAN ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_0        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.2g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_1        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.2g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_0_2        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.2g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_0        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.5g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_1        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.5g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_1_2        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.5g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_0        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.8g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_1        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.8g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TORQ_LVL_AFTER_EEC_2_2        */		     300,	/* Comment [ ESC Engine 제어 종료후 여기에 설정된 값 까지 토크 회복 후 TCS 통상 제어 모드 작동(det_alatm : 0.8g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[      30 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_0        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.2g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_1        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.2g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_0_2        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.2g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_0        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.5g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_1        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.5g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_1_2        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.5g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_0        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.8g 차속 : U16_TCS_SPEED2) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_1        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.8g 차속 : U16_TCS_SPEED3) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_INC_RATE_A_EEC_2_2        */		      30,	/* Comment [ ESC Engine 제어 종료후 토크 회복 모드 작동시 적용되는 토크 Rise Rate(det_alatm : 0.8g 차속 : U16_TCS_SPEED4) ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_TCS_H2L_RESET_TIME                */		      20,	/* Comment [ High-to-Low 감지시 Spin이 감소하는 시간이 이 값 동안 지속될 경우 High-to-Low 감지 해제 ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_TCS_H2L_SUSPECT_RESET_TIME        */		     300,	/* Comment [ High-mu suspect 감지시 차체 가속도가 U8_TCS_H2L_SUSPECT_RESET_TH보다 작은 시간이 이 값동안 지속될 경우 High-mu suspect clear ] */
	                                                        		        	/* ScaleVal[      3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_TCS_D_GAIN_ADD_SPIN_DEC_H2L        */		       0,	/* Comment [ High-to-Low 감지 한 경우 Spin 감소 경향시 D gain 증가량 ] */
	/* uchar8_t    	U8_TCS_D_GAIN_ADD_SPIN_INC_H2L        */		       0,	/* Comment [ High-to-Low 감지 한 경우 Spin 증가 경향시 D gain 증가량 ] */
	/* uchar8_t    	U8_TCS_H2L_SUSPECT_RESET_TH           */		      20,	/* Comment [ High-mu suspect 감지시 차체 가속도가 이 값보다 작은 시간이 U16_TCS_H2L_SUSPECT_RESET_TIME 동안 지속될 경우 High-mu suspect clear ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_HIGH_MU_TH_FOR_H2L_DCT         */		      50,	/* Comment [ 차체 가속도가 이 값보다 클 경우 High-mu suspect 감지 ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_TCS_LOW_MU_TH_FOR_H2L_DCT          */		      10,	/* Comment [ High-mu suspect 감지가 되어 있는 상태에서 Spin이 3KPH이상 발생하고 차체 가속도가 이 값보다 작으면 High-to-Low 감지 ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_TCS_HIGHMU_DETECT_ENTER_YAW       */		     200,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동을 위한 delta yaw. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[    2 Deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_TCS_LOWMU_DETECT_ENTER_YAW        */		     300,	/* Comment [ L2H 로직에 의한 토크 증가 모드 해제를 위한 delta yaw. 해당 값보다 커야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[    3 Deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* char8_t     	S8_TCS_HIGHMU_DETECT_SPINTHR1         */		       2,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[   0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_TCS_HIGHMU_DETECT_SPINTHR2         */		     -20,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동을 위한 Wheel Spin 기울기. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[  -0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ -1.6 <= X <= 1.5875 ] */
	/* char8_t     	S8_TCS_LOWMU_DETECT_SPINTHR1          */		       3,	/* Comment [ L2H 로직에 의한 토크 증가 모드 해제를 위한 Wheel Spin. 해당 값보다 커야 해제 조건 만족 ] */
	                                                        		        	/* ScaleVal[  0.375 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_TCS_LOWMU_DETECT_SPINTHR2          */		       4,	/* Comment [ L2H 로직에 의한 토크 증가 모드 해제를 위한 Wheel Spin 기울기. 해당 값보다 커야 해제 조건 만족 ] */
	                                                        		        	/* ScaleVal[   0.05 KPH ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ -1.6 <= X <= 1.5875 ] */
	/* uint16_t    	U16_TCS_BRK_TORQ_FOR_INPUT_L2H        */		     400,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동을 위한 이전 Cycle의 브레이크 토크 변화량. 해당 값보다 작아야 진입 조건 만족 ] */
	/* uint16_t    	U16_TCS_ENG_TORQ_FOR_INPUT_L2H        */		     200,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동을 위한 이전 Cycle의 엔진 토크 변화량. 해당 값보다 작아야 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_ENG_TORQ_IN_GSC_L2H           */		     400,	/* Comment [ L2H 판단시 기어 변속 시점의 토크 저감에 의한 휠경향 판단을 위하여 해당 값보다 토크 변화가 작을 시 L2H 진입 조건 만족 ] */
	                                                        		        	/* ScaleVal[      40 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_LOWTORQ_FOR_L2H               */		     800,	/* Comment [ acc_r이 U16_AX_RANGE_MAX에서 해당 값만큼 감한 값보다 크고 엔진 토크가 해당 값보다 작을 시 discrete 토크 적용 ] */
	                                                        		        	/* ScaleVal[      80 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_AX_RANGE_COMP_IN_LOWTORQ           */		      10,	/* Comment [ 엔진 토크 레벨(U8_TCS_LOWTORQ_FOR_L2H) 이하시 acc_r이 U16_AX_RANGE_MAX에서 해당 값만큼 감한 값보다 클 시 discrete 토크 적용 ] */
	                                                        		        	/* ScaleVal[     0.05 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_TCS_1ST_INPUT_TORQ_L2H             */		     200,	/* Comment [ Homo L2H 판단시 해당 값만큼 discrete input 토크 증가 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_1ST_INPUT_TORQ_L2H_SPLIT       */		     200,	/* Comment [ Split L2H 판단시 해당 값만큼 discrete input 토크 증가 ] */
	                                                        		        	/* ScaleVal[      20 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_BRAKE_TORQUE_FACTOR_F          */		      40,	/* Comment [ 전륜의 브레이크 토크 계산을 위한 패드 마찰계수 Factor ] */
	/* uchar8_t    	U8_TCS_BRAKE_TORQUE_FACTOR_R          */		      14,	/* Comment [ 후륜의 브레이크 토크 계산을 위한 패드 마찰계수 Factor ] */
	/* uchar8_t    	U8_TCS_L2H_1ST_DURATION_SCAN          */		      30,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 지속 시간 ] */
	/* uchar8_t    	U8_TCS_L2H_1ST_MONITOR_SCAN           */		      30,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드 작동 후 휠거동 모니터링 시간 ] */
	/* uchar8_t    	U8_TCS_L2H_1ST_RISE_RATE              */		       7,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동시 첫번째 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 0.7 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_L2H_2ND_MONITOR_SCAN           */		      50,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동시 마지막 토크 증가 모드의 Rise Rate ] */
	/* uchar8_t    	U8_TCS_L2H_2ND_RISE_RATE              */		      10,	/* Comment [ L2H 로직에 의한 토크 증가 모드 작동시 두번째 토크 증가 모드의 Rise Rate ] */
	                                                        		        	/* ScaleVal[ 1 0.1%/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TCS_TORQ_DEC_GAIN_FOR_L2H          */		      50,	/* Comment [ L2H에 의한 탐색 토크 적용 후 바로 해제시 증가시킨 탐색 입력에 해당 값만큼의 비율만큼 토크 저감 ] */
	/* uint16_t    	U16_TCS_DIAG_WHL_SPIN_CNT             */		     286,	/* Comment [ 대각 Spin에 의한 Stuck 감지 시간 설정(대각 Spin 경향이 이 값 이상동안 지속되면 대각 Spin에 의한 Stuck 감지) ] */
	/* uint16_t    	U16_TCS_DIAG_WHL_SPIN_END_SPD         */		      80,	/* Comment [ 차속이 이 값 이하인 경우만  대각 Spin에 의한 Stuck 감지 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_TCS_DIAG_WHL_SPIN_TH               */		      40,	/* Comment [ 대각 Spin에 의한 Stuck 감지 시작 Spin Level 설정 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_T_SPIN_INC_IN_DIA_WHL_SPIN         */		     120,	/* Comment [ 대각 Spin에 의한 Stuck 감지시 Target Spin 설정 ] */
	                                                        		        	/* ScaleVal[     15 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_TCS_DELTA_DRV_REQ_TORQ            */		     150,	/* Comment [ Lamp Staragety1. torq가 증가 경향이고 drive torq와 cal torq와의 차이가 해당 값보다 작을 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[      15 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_TCS_FUN_LAMP_OFF_OFFSET           */		      25,	/* Comment [ cal_torq가 eng_torq보다 이 변수에 설정된 값보다 큰 경우 Function Lamp 작동 종료 ] */
	                                                        		        	/* ScaleVal[     2.5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MIN_TORQ_PERCENT         */		      80,	/* Comment [ Lamp Staragety2. cal torq가 drive torq의 해당 percentage 이상일 시 Function Lamp 조기 해제 만족 ] */
	/* uchar8_t    	U8_FLAMP_OFF_TORQ_RATE_LIMIT          */		      50,	/* Comment [ Lamp Staragety2. cal torq 증가율이 S16_TCS_DELTA_REQ_TORQ보다 큰 상태에서 증가된 Count가 해당 값 이상일 시 Function Lamp 조기 해제 만족 ] */
	/* uchar8_t    	U8_TCS_DELTA_REQ_TORQ_RATE            */		      30,	/* Comment [ Lamp Staragety2. cal torq 증가율이 해당 값보다 클 시 Function Lamp 조기 해제를 위한 Count 시작 ] */
	                                                        		        	/* ScaleVal[       3 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_FLAMP_OFF_AX_SLOPE                 */		       3,	/* Comment [ Lamp Staragety3. acc_r 기울기가 해당 값보다 크고 acc_r이 U8_FLAMP_OFF_MIN_AX보다 클 시  Function Lamp 조기 해제 유도 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MAX_AX                   */		      80,	/* Comment [ Lamp Staragety3. acc_r이 해당 값보다 클 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[      0.4 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_FLAMP_OFF_MIN_AX                   */		      60,	/* Comment [ Lamp Staragety3. acc_r이 해당 값보다 크고 acc_r 기울기가 U8_FLAMP_OFF_AX_SLOPE보다 클 시 Function Lamp 조기 해제 유도 ] */
	                                                        		        	/* ScaleVal[      0.3 G ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* char8_t     	S8_FLAMP_OFF_BRK_INPUT                */		       0,	/* Comment [ Lamp Staragety4. BTCS일 시 State2 Dump Mode상태에서 추정압력이 해당 값보다 작을 시 Function Lamp 조기 해제 유도 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S1         */		     160,	/* Comment [ 정차구간(U16_TCS_SPEED1) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S2         */		     160,	/* Comment [ 저속구간(U16_TCS_SPEED2) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S3         */		     160,	/* Comment [ 중속구간(U16_TCS_SPEED3) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TARGET_DV_HOMO_S4         */		     160,	/* Comment [ 고속구간(U16_TCS_SPEED1) Driveline Protection 적용 시 Target Spin 보상량 ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_COMP_TORQ_LIMIT                */		     450,	/* Comment [ Driveline Protection 적용 시 토크 리미트 최대값 설정 ] */
	                                                        		        	/* ScaleVal[      45 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_DP_FRONT_REAR_DV_THR              */		     320,	/* Comment [ 전후 축간 속도차이를 보고 Driveline Protection 제어 수행하기 위한 진입 DV Threshold ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DP_LEFT_RIGHT_DV_THR              */		     320,	/* Comment [ 좌우 축간 속도차이를 보고 Driveline Protection 제어 수행하기 위한 진입 DV Threshold ] */
	                                                        		        	/* ScaleVal[     40 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_DUMP_TH4DP                        */		      64,	/* Comment [ Driveline Protection 제어 수행하기 위한 DUMP Threshold 설정 ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_RISE_TH4DP                        */		     120,	/* Comment [ Driveline Protection 제어 수행하기 위한 RISE Threshold 설정 ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_BRK_DUMP_TRH_LIMIT          */		       8,	/* Comment [ Stuck 감지시, brake 압력 증가를 위하여, 설정된 값으로 dump threshold 제한 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_BRK_DUMP_TRH_LIMIT_SP       */		       0,	/* Comment [ After Stuck detection, Dump threshold limit value to increase brake pressure in split mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_BRK_RISE_TRH_LIMIT          */		      80,	/* Comment [ Stuck 감지시, brake 압력 증가를 위하여, 설정된 값으로 rise threshold 제한 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_BRK_RISE_TRH_LIMIT_SP       */		       0,	/* Comment [ After Stuck detection, Rise threshold limit value to increase brake pressure in split mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_DETECT_SCAN                 */		     700,	/* Comment [ Stuck 판단 시간 Threshold ] */
	/* uint16_t    	U16_STUCK_DETECT_SCAN_SP              */		     700,	/* Comment [ Stuck detection time in Split mu ] */
	/* uint16_t    	U16_STUCK_DETECT_SPEED                */		      24,	/* Comment [ Stuck 구간 속도 Threshold ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_EXIT_SPEED                  */		      32,	/* Comment [ Stuck 종료 속도 ] */
	                                                        		        	/* ScaleVal[      4 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_COMP              */		      24,	/* Comment [ Stuck 판단 시 실시하는 Target DV 보상량 ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_COMP_SP           */		       0,	/* Comment [ After Stuck detection, Target DV compensation value in split mu ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_MAX               */		      80,	/* Comment [ Stuck 판단 시 보상량이 적용된 Target DV의 최대값 ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TARGET_DV_MIN               */		      24,	/* Comment [ Stuck 판단 시 보상량이 적용된 Target DV의 최소값 ] */
	                                                        		        	/* ScaleVal[      3 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uint16_t    	U16_STUCK_TORQ_LIMIT                  */		     500,	/* Comment [ Stuck Algorithm 적용 시 토크 리미트 최대값 설정 ] */
	                                                        		        	/* ScaleVal[      50 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_STUCK_TORQ_LIMIT_SP               */		     500,	/* Comment [ After Stuck detection, Maximum torque limit in split mu ] */
	                                                        		        	/* ScaleVal[         50 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uchar8_t    	U8_S1_FNT_HS_FOR_4WDSTUCK             */		       0,	/* Comment [ After 4WD Stuck detection, Hold scan of front wheel in state1 ] */
	/* uchar8_t    	U8_S1_FNT_RS_FOR_4WDSTUCK             */		       3,	/* Comment [ After 4WD Stuck detection, Rise scan of front wheel in state1 ] */
	/* uchar8_t    	U8_S1_REAR_HS_FOR_4WDSTUCK            */		       1,	/* Comment [ After 4WD Stuck detection, Hold scan of rear wheel in state1 ] */
	/* uchar8_t    	U8_S1_REAR_RS_FOR_4WDSTUCK            */		       3,	/* Comment [ After 4WD Stuck detection, Rise scan of rear wheel in state1 ] */
	/* uchar8_t    	U8_S2_DUMP_THR_FOR_4WDSTUCK           */		       8,	/* Comment [ After 4WD Stuck detection, Dump threshold ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_S2_RISE_THR_FOR_4WDSTUCK           */		      80,	/* Comment [ After 4WD Stuck detection, Rise threshold ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_STUCK_TORQ_RISE_MAX                */		       5,	/* Comment [ Stuck Algorithm 적용 시 토크 업 기울기 최대값 설정 ] */
	                                                        		        	/* ScaleVal[     0.5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STUCK_TORQ_RISE_MAX_SP             */		       5,	/* Comment [ After Stuck detection, Maximum torque rise rate in split mu ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_TCS_ENG_TORQUE_MAX                */		    2700,	/* Comment [ 정차중 Full throttle시 Engine Torque의 최대값, TCS 제어 토크가 이 값 이상인 상황에서 토크 저감시 이 토크 Level까지 즉시 토크 저감후 통상 제어 진행함 ] */
	                                                        		        	/* ScaleVal[     270 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_TRQ_DEC4BIG_SPIN_CTL          */		      50,	/* Comment [ 2ND Cycle에서 Big Spin Overshoot 감지시 토크 저감량 ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TCS_WAIT_T4BIG_SPIN_CTL           */		     100,	/* Comment [ 2ND Cycle에서 Big Spin Overshoot 감지시 토크 저감후 Spin 상태 모니터링 시간, 이 시간 경과후 에도 Big Spin 상태가 지속되면 동일한 토크 저감을 재차 수행함 ] */
	/* int16_t     	S16_TCS_D_GAIN_F_RPM_FAST_INC_N       */		     300,	/* Comment [ In fast increase detection of the engine rpm, increase value of the D gain (ex.S16_TCS_D_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* int16_t     	S16_TCS_P_GAIN_F_RPM_FAST_INC_N       */		     300,	/* Comment [ In fast increase detection of the engine rpm, increase value of the P gain (ex.S16_TCS_P_GAIN_F_RPM_FAST_INC_N : 300 -> 3 times) ] */
	/* uchar8_t    	U8_TCS_CLR_FAST_RPM_INC_TH            */		      10,	/* Comment [ Threshold value to clear fast increase detection of the engine rpm ] */
	/* uchar8_t    	U8_TCS_SET_FAST_RPM_INC_TH            */		      18,	/* Comment [ Threshold value to detect fast increase of the engine rpm ] */
	/* int16_t     	S16_TORQ_RISE_LIMIT_MODIFY            */		     200,	/* Comment [ In Gear shifting, increase or decrease value of the torque rise limit ] */
	                                                        		        	/* ScaleVal[   20 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E       */		      40,	/* Comment [ In Gear shifting, PD gain tuning value in homo-mu (U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E<100 : decease , U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E>100 : increase) ] */
	/* uchar8_t    	U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F       */		      40,	/* Comment [ In Gear shifting, PD gain tuning value in split-mu (U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F<100 : decease , U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F>100 : increase) ] */
	/* uchar8_t    	U8_TORQ_RISE_RATE_MODIFY              */		      20,	/* Comment [ In Gear shifting, increase value of the torque rise rate ] */
	                                                        		        	/* ScaleVal[    2 Nm[%] ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_TORQ_RISE_THRESHOLD_MODIFY         */		     160,	/* Comment [ In Gear shifting, increase value of the torque rise threshold ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uint16_t    	U16_GEAR_SHIFT_UP_RPM                 */		    4500,	/* Comment [ In case of 'engine rpm >=  U16_GEAR_SHIFT_UP_RPM' , gear shift up will be premit (for only Gear shift up available vehicle) ] */
	/* uint16_t    	U16_TCS_BUMP_DETECT_SPIN              */		      16,	/* Comment [ If Average wheel spin is less than U16_TCS_BUMP_DETECT_SPIN in U8_TCS_BUMP_DETECT_TIME,  BUMP will be detect. ] */
	                                                        		        	/* ScaleVal[      2 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 8191.875 ] */
	/* uchar8_t    	U8_TCS_BUMP_ADD_P_GAIN_HIGH_AX        */		      10,	/* Comment [ In Bump detection state, Added P gain value in 'Ax >= U16_AX_RANGE_MAX' ] */
	/* uchar8_t    	U8_TCS_BUMP_ADD_P_GAIN_LOW_AX         */		      10,	/* Comment [ In Bump detection state, Added P gain value in 'Ax <= U16_AX_RANGE_MIN' ] */
	/* uchar8_t    	U8_TCS_BUMP_DETECT_TIME               */		      30,	/* Comment [ Bump detection time setting ] */
	                                                        		        	/* ScaleVal[    0.3 Sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_TCS_VIB_ETCS_ENG_TORQUE            */		      20,	/* Comment [ Reduction Percentage Value of Engine Torque in Vibration Engine Control at the ETCS ] */
	/* char8_t     	S8_TCS_VIB_FTCS_ENG_TORQUE            */		      20,	/* Comment [ Reduction Percentage Value of Engine Torque in Vibration Engine Control at the FTCS ] */
	/* uchar8_t    	U8_TCS_VIB_AMPLITUDE_ENTER            */		      64,	/* Comment [ Wheel Amplitude to Vibration Control Detect Enterance ] */
	                                                        		        	/* ScaleVal[          8 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_AMPLITUDE_OUT              */		      88,	/* Comment [ Wheel Amplitude to Vibration Control Detect Clearance ] */
	                                                        		        	/* ScaleVal[         11 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_BRK_CTL_PERIOD             */		       3,	/* Comment [ Vibration Brake Control Period ] */
	/* uchar8_t    	U8_TCS_VIB_BTCS_DUMP_SCAN             */		       1,	/* Comment [ DUMP SCAN in Vibration Brake Control(BTCS_ON=1 & VibFlag=1) ] */
	/* uchar8_t    	U8_TCS_VIB_BTCS_HOLD_SCAN_DUMP        */		       2,	/* Comment [ HOLD SCAN in Vibration Brake Control Dump Mode(BTCS_ON=1 & VibFlag=1) ] */
	/* uchar8_t    	U8_TCS_VIB_BTCS_HOLD_SCAN_RISE        */		       3,	/* Comment [ HOLD SCAN in Vibration Brake Control Rise Mode(BTCS_ON=1 & VibFlag=1) ] */
	/* uchar8_t    	U8_TCS_VIB_BTCS_RISE_SCAN             */		       1,	/* Comment [ RISE SCAN in Vibration Brake Control(BTCS_ON=1 & VibFlag=1) ] */
	/* uchar8_t    	U8_TCS_VIB_CNT_SCAN                   */		       1,	/* Comment [ Increase, Decrease of Wheel Speed Count Filter Scan ] */
	/* uchar8_t    	U8_TCS_VIB_ETCS_ENG_PERIOD            */		       2,	/* Comment [ Vibration Brake Control Period at the ETCS ] */
	                                                        		        	/* ScaleVal[    4 Cycle ]	Scale [ f(x) = (X + 0) * 2 ]  Range   [ 0 <= X <= 510 ] */
	/* uchar8_t    	U8_TCS_VIB_ETCS_PGAIN_INC             */		       0,	/* Comment [ P Gain Increase to Torque Reduction in Vibration Engine Control(ETCS_ON=1 & VibFlag=1) ] */
	/* uchar8_t    	U8_TCS_VIB_ETCS_TARGETSPIN_DEC        */		       0,	/* Comment [ TargetSpin Decrease to Torque Reduction in Vibration Engine Control(ETCS_ON=1 & VibFlag=1) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_FLAG_ENTER_CYCLE           */		       2,	/* Comment [ Wheel Speed Cycle to Vibration Control in Vibration Detect Condition ] */
	/* uchar8_t    	U8_TCS_VIB_FLAG_OUT_CYCLE             */		       1,	/* Comment [ Wheel Speed Cycle to Vibration Control Hysteresis in Vibration Clearance Condition ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_ENTER            */		       5,	/* Comment [ Wheel Frequency to Vibration Control Detect Enterance ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_OUT              */		       4,	/* Comment [ Wheel Frequency to Vibration Control Detect Clearance ] */
	/* uchar8_t    	U8_TCS_VIB_FREQUENCY_THR              */		      20,	/* Comment [ Vibration Control Detect Inhibit about Wheel Fequency in Wheel Noise ] */
	/* uchar8_t    	U8_TCS_VIB_FTCS_ENG_PERIOD            */		       2,	/* Comment [ Vibration Brake Control Period at the FTCS ] */
	/* uchar8_t    	U8_TCS_VIB_REFERENCE_VREF_ENTER       */		      80,	/* Comment [ Wheel Speed(U Peak Point)-Vehicle Speed to Vibration Control Detect Enterance ] */
	                                                        		        	/* ScaleVal[         10 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_REFERENCE_VREF_OUT         */		     120,	/* Comment [ Wheel Speed(U Peak Point)-Vehicle Speed to Vibration Control Detect Clearance ] */
	                                                        		        	/* ScaleVal[         15 ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_TCS_VIB_TIMESHIFT_SCAN             */		       3,	/* Comment [ TimeShift about Rise/Hold/Dump time in Vibration Brake Mode ] */
	/* uint16_t    	U16_TCS_MINI_DET_DURATION_TIME        */		       0,	/* Comment [ Total detection time to detect two wheel deferent kind tire ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_TCS_TORQ_INPUT_FOR_MINI           */		       0,	/* Comment [ Searching input torque to detect two wheel deferent kind tire ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 6553.5 ] */
	/* uint16_t    	U16_TCS_TQ_INP_STRT_T_FOR_MINI        */		       0,	/* Comment [ Searching input starting time to detect two wheel deferent kind tire ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_TCS_SD_THR_FOR_TQ_INP_EXIT         */		       0,	/* Comment [ Spin error difference threshold to exit searching input torque ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCS_SPIN_THR_FOR_TQ_INP_EXIT       */		       0,	/* Comment [ Spin threshold to exit searching input torque ] */
	/* uchar8_t    	U8_TCS_TQ_INPUT_INTRVL_FOR_MINI       */		       0,	/* Comment [ The time interval of searching input torque to detect two wheel deferent kind tire ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_COMPLETE_CLOSE_THROTTLE_POS        */		       2,	/* Comment [ 운전자의 가속의지가 전혀 없다고 판단하는 Throttle Position 값 ] */
	/* uint16_t    	U16_TCS_GAIN_SCALING_FACTOR           */		      80,	/* Comment [ P/D Effect = (P_Gain * WheelSpinError + D_Gain * WheelSpinErrorDiff) / U16_TCS_GAIN_SCALING_FACTOR ] */
	/* uchar8_t    	U8_CONT_ENGAGE_FACTOR_AY_0G3          */		      14,	/* Comment [ 횡가속도가 0.3G 일때 Inside Wheel Lift에 의한 ETCS 진입 조건 민감도 설정 ] */
	/* uchar8_t    	U8_CONT_ENGAGE_FACTOR_AY_0G3_RR       */		      28,	/* Comment [ Rough Road감지된 경우 횡가속도가 0.3G 일때 Inside Wheel Lift에 의한 ETCS 진입 조건 민감도 설정 ] */
	/* uchar8_t    	U8_CONT_ENGAGE_FACTOR_AY_0G7          */		       4,	/* Comment [ 횡가속도가 0.7G 일때 Inside Wheel Lift에 의한 ETCS 진입 조건 민감도 설정 ] */
	/* uchar8_t    	U8_CONT_ENGAGE_FACTOR_AY_0G7_RR       */		       4,	/* Comment [ Rough Road감지된 경우 횡가속도가 0.8G 일때 Inside Wheel Lift에 의한 ETCS 진입 조건 민감도 설정 ] */
	/* uchar8_t    	U8_CONT_ENGAGE_FACTOR_DYAW            */		      60,	/* Comment [ Inside Wheel Lift에 의한 ETCS 진입이 허용되는 delta yaw level 설정 ] */
	/* uchar8_t    	U8_D_GAIN_COMP_RATIO_IN_RR            */		       3,	/* Comment [ Rough Road 감지시 Engine 제어에 사용되는 D Gain을 감소시키는 Factor(원래 D Gain을 이 값으로 나눔) ] */
	/* uchar8_t    	U8_TCS_MOVING_WINDOW_LENGTH           */		      14,	/* Comment [ TCS 제어의 기준 신호가 되는 Moving Average를 계산하는 Window의 길이 ] */
	/* uint16_t    	U16_BTCS_BUILD_PATTERN                */		       1,	/* Comment [ BTCS 제어 초기에 Build Up 패턴을 사용함 ] */
	/* uint16_t    	U16_BTCS_ESV_CONTROL                  */		       0,	/* Comment [ ESV 밸브 제어 패턴을 변경 ] */
	/* uint16_t    	U16_BTCS_HIGH_SIDE_CHECK              */		       1,	/* Comment [ BTCS 제어 중에 High mu Side에 스핀 발생 시 DUMP 유도 ] */
	/* uint16_t    	U16_BTCS_LFC_RISE_CONTROL             */		       0,	/* Comment [ LFC Duty 제어를 BTCS 제어에 사용함 ] */
	/* uint16_t    	U16_BTCS_TWO_ON_HOMO_MU               */		       1,	/* Comment [ 저속구간에서 두바퀴 동시에 BTCS 제어 진입 허용함 ] */
	/* uchar8_t    	U8_TCS_APPLY_TIME_FRT                 */		      10,	/* Comment [ BTCS 제어 초기 Front Wheel의 압력 Rise 제어 패턴을 사용함 ] */
	/* uchar8_t    	U8_TCS_APPLY_TIME_RR                  */		       7,	/* Comment [ BTCS 제어 초기 Rear Wheel의 압력 Rise 제어 패턴을 사용함 ] */
	/* uchar8_t    	U8_TCS_DUMP_TIME_FRT                  */		       5,	/* Comment [ BTCS 제어 초기 Front Wheel의 압력 Dump 제어 패턴을 사용함 ] */
	/* uchar8_t    	U8_TCS_DUMP_TIME_RR                   */		       5,	/* Comment [ BTCS 제어 초기 Rear Wheel의 압력 Dump 제어 패턴을 사용함 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_HIGH_SPD        */		     250,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 80km/h ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_LOWMED_SPD      */		     900,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 20km/h ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_LOW_SPD         */		    1500,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 10km/h ] */
	                                                        		        	/* ScaleVal[        150 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_HOMO_MED_SPD         */		     300,	/* Comment [ Min. Steer Angle for detect vehicle turning on Homo-Mu at 50km/h ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_HIGH_SPD       */		     250,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 80km/h ] */
	                                                        		        	/* ScaleVal[         25 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_LOWMED_SPD     */		     900,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 20km/h ] */
	                                                        		        	/* ScaleVal[         90 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_LOW_SPD        */		    1500,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 10km/h ] */
	                                                        		        	/* ScaleVal[        150 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_DETECT_STEER_SPLIT_MED_SPD        */		     300,	/* Comment [ Min. Steer Angle for detect vehicle turning on Split-Mu at 50km/h ] */
	                                                        		        	/* ScaleVal[         30 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_BTCS_LFC_CONTROL_ENABLE            */		       1,	/* Comment [ BTCS Control Type Seclection.(1:LFC Type, 0:On/Off Pulse-Up Type) ] */
	/* uchar8_t    	U8_DELTA_SPIN_MAX_FOR_LFC             */		      40,	/* Comment [ TCS Brake LFC Duty 결정 시 기준이 되는 Delta Spin(Spin Index-Rise Threshold) 최대값 ] */
	                                                        		        	/* ScaleVal[      5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DELTA_SPIN_MIN_FOR_LFC             */		       8,	/* Comment [ TCS Brake LFC Duty 결정 시 기준이 되는 Delta Spin(Spin Index-Rise Threshold) 최소값 ] */
	                                                        		        	/* ScaleVal[      1 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_SPIN_DIFF_MAX_FOR_LFC              */		      24,	/* Comment [ TCS Brake LFC Duty 결정 시 기준이 되는 Spin Slope 최대값 ] */
	                                                        		        	/* ScaleVal[  0.3 Slope ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_SPIN_DIFF_MIN_FOR_LFC              */		       4,	/* Comment [ TCS Brake LFC Duty 결정 시 기준이 되는 Spin Slope 최소값 ] */
	                                                        		        	/* ScaleVal[ 0.05 Slope ]	Scale [ f(x) = (X + 0) * 0.0125 ]  Range   [ 0 <= X <= 3.1875 ] */
	/* uchar8_t    	U8_TCMF_HW_01                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_02                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_03                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_04                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_05                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_06                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_07                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_08                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_09                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_10                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_11                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_12                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_13                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_14                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_15                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_16                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_17                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_18                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_19                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_20                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_21                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_22                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_23                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_24                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_25                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_26                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_27                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_28                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_29                         */		       0,
	/* uchar8_t    	U8_TCMF_HW_30                         */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_01                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_02                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_03                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_04                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_05                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_06                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_07                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_08                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_09                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_10                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_11                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_12                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_13                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_14                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_15                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_16                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_17                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_18                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_19                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_20                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_21                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_22                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_23                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_24                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_25                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_26                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_27                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_28                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_29                      */		       0,
	/* uchar8_t    	U8_TCMF_ENTER_30                      */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_01                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_02                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_03                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_04                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_05                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_06                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_07                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_08                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_09                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_10                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_11                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_12                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_13                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_14                */		       0,
	/* uchar8_t    	U8_TCMF_TARGET_SPIN_15                */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_01                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_02                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_03                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_04                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_05                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_06                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_07                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_08                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_09                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_10                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_11                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_12                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_13                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_14                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_15                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_16                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_17                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_18                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_19                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_20                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_21                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_22                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_23                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_24                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_25                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_26                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_27                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_28                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_29                 */		       0,
	/* uchar8_t    	U8_TCMF_FF_CONTROL_30                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_01                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_02                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_03                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_04                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_05                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_06                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_07                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_08                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_09                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_10                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_11                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_12                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_13                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_14                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_15                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_16                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_17                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_18                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_19                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_20                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_21                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_22                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_23                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_24                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_25                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_26                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_27                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_28                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_29                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_30                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_31                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_32                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_33                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_34                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_35                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_36                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_37                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_38                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_39                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_40                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_41                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_42                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_43                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_44                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_45                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_46                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_47                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_48                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_49                 */		       0,
	/* uchar8_t    	U8_TCMF_FB_CONTROL_50                 */		       0,
	/* uchar8_t    	U8_TCMF_ETC_01                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_02                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_03                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_04                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_05                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_06                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_07                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_08                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_09                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_10                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_11                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_12                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_13                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_14                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_15                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_16                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_17                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_18                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_19                        */		       0,
	/* uchar8_t    	U8_TCMF_ETC_20                        */		       0,
	/* int16_t     	S16_HM_EMS                            */		     600,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 레벨 ] */
	/* int16_t     	S16_LM_EMS                            */		     250,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 레벨 ] */
	/* int16_t     	S16_MHM_EMS                           */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-High-m 레벨 ] */
	/* int16_t     	S16_MM_EMS                            */		     300,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_HSP               */		    1100,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_LSP               */		     600,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 레벨 ] */
	/* int16_t     	S16_HM_ESP_TORQ_CON_MSP               */		     800,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_HSP               */		     900,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 고속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_LSP               */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 저속 레벨 ] */
	/* int16_t     	S16_MM_ESP_TORQ_CON_MSP               */		     700,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 중속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_HSP               */		     800,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_LSP               */		     300,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 레벨 ] */
	/* int16_t     	S16_LM_ESP_TORQ_CON_MSP               */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 레벨 ] */
	/* uchar8_t    	U8_EEC_ENTER_CNT                      */		      21,	/* Comment [ ESP 엔진제어 진입을 위한 Threshold 유보 Count ] */
	/* int16_t     	S16_HM_HSP_TH_GAIN                    */		      90,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_LSP_TH_GAIN                    */		     110,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_MSP_TH_GAIN                    */		     100,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_SLIP_TH_GAIN                   */		      10,	/* Comment [ ESP 엔진제어에서 사용하는 High-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_HSP_TH_GAIN                    */		      80,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_LSP_TH_GAIN                    */		      80,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_MSP_TH_GAIN                    */		      80,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_SLIP_TH_GAIN                   */		       8,	/* Comment [ ESP 엔진제어에서 사용하는 Med-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_HSP_TH_GAIN                    */		      75,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_LSP_TH_GAIN                    */		      75,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_MSP_TH_GAIN                    */		      75,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_SLIP_TH_GAIN                   */		       8,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m Slip Threshold 게인 레벨 ] */
	/* int16_t     	S16_HSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 고속 Threshold 게인 추가 레벨(EEC Gain = S16_HSP_TH_GAIN_ADD_TVBB + S16_XM_HSP_TH_GAIN) ] */
	/* int16_t     	S16_LSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 저속 Threshold 게인 추가 레벨(EEC Gain = S16_LSP_TH_GAIN_ADD_TVBB + S16_XM_LSP_TH_GAIN) ] */
	/* int16_t     	S16_MSP_TH_GAIN_ADD_TVBB              */		      20,	/* Comment [ TVBB 제어시 ESP 엔진제어에 사용하는 중속 Threshold 게인 추가 레벨(EEC Gain = S16_MSP_TH_GAIN_ADD_TVBB + S16_XM_MSP_TH_GAIN) ] */
	/* int16_t     	S16_HM_HSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_LSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_MSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 High-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_HSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_LSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_MM_MSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Mid-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_HSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 고속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_LSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 저속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_LM_MSP_TH_GAIN_IN_OVER            */		     500,	/* Comment [ ESP 엔진제어에서 사용하는 Low-m 중속 OverSteer Threshold 게인 레벨 ] */
	/* int16_t     	S16_HM_USP_YAW_OVER_DGAIN             */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_USP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVER_DGAIN             */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNSTB_DGAIN            */		     450,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_HSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVER_DGAIN             */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNSTB_DGAIN            */		     450,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_MSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVER_DGAIN             */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNDER_PGAIN            */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNSTB_DGAIN            */		     450,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_HM_LSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 High-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVER_DGAIN             */		    -100,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVER_PGAIN             */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVSTB_DGAIN            */		    -100,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_OVSTB_PGAIN            */		       3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_USP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVER_DGAIN             */		    -100,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_HSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVER_DGAIN             */		    -100,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_MSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVER_DGAIN             */		    -100,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVER_PGAIN             */		      -1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_OVSTB_PGAIN            */		       2,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_MM_LSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Med-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVER_PGAIN             */		     -12,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_USP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 초고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVER_PGAIN             */		      -9,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_OVSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 고속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_HSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 고속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVER_PGAIN             */		      -9,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_OVSTB_PGAIN            */		      -2,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 중속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_MSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 중속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVER_DGAIN             */		    -150,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVER_PGAIN             */		      -9,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVSTB_DGAIN            */		     -50,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_OVSTB_PGAIN            */		      -3,	/* Comment [ Yaw PD 제어 게인 값으로 Oversteer-Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_STB_DGAIN              */		      10,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_STB_PGAIN              */		      14,	/* Comment [ Yaw PD 제어 게인 값으로 Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNDER_DGAIN            */		     350,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNDER_PGAIN            */		      12,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNSTB_DGAIN            */		     500,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 저속 영역에서 사용하는 D 게인 값 ] */
	/* int16_t     	S16_LM_LSP_YAW_UNSTB_PGAIN            */		       1,	/* Comment [ Yaw PD 제어 게인 값으로 Understeer-Stable 상황에서 Low-m 저속 영역에서 사용하는 P 게인 값 ] */
	/* int16_t     	S16_EEC_HM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.6G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_LM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.2G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_MM_TRQ_LIMIT                  */		      40,	/* Comment [ EEC Torque Down Rate Limitation Value(alatm : 0.4G) ] */
	                                                        		        	/* ScaleVal[       4 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_EEC_TORQ_LIMIT_MAX                */		      50,	/* Comment [ EEC Torque Down Rate Maximum Limitation Final Value ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_HSP      */		       4,	/* Comment [ 고속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_LSP      */		       4,	/* Comment [ 저속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* char8_t     	S8_EEC_1ST_CONTROL_DROP_RATE_MSP      */		       4,	/* Comment [ 중속에서 EEC 1st Cycle 제어 시 일괄적인 토크 다운량 설정 ] */
	/* uint16_t    	U16_EEC_HM_TARGET_GAIN_1ST_CYC        */		     180,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 High-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_LM_TARGET_GAIN_1ST_CYC        */		     350,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Low-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_MM_TARGET_GAIN_1ST_CYC        */		     210,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Med-mu 게인 값 ] */
	/* uint16_t    	U16_EEC_TARGET_TORQ_1ST_MAX           */		     700,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Max. Torque Level ] */
	/* uint16_t    	U16_EEC_TARGET_TORQ_1ST_MIN           */		     300,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 설정 시 Min. Torque Level ] */
	/* uchar8_t    	U8_EEC_1ST_CONTROL                    */		       0,	/* Comment [ EEC 1st Cycle 제어에 Feed Forward 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_EEC_START_DRIVE_TORQUE             */		      50,	/* Comment [ Minimum drive torque level to detect ESP engine control ] */
	                                                        		        	/* ScaleVal[       5 Nm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_HM              */		       5,	/* Comment [ Yaw 안정화시 High-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_LM              */		      10,	/* Comment [ Yaw 안정화시 Low-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uint16_t    	U16_EEC_YAW_FADE_TIME_MM              */		       7,	/* Comment [ Yaw 안정화시 Mid-mu 빠르게 EEC 제어 종료 시키는 시간 설정 (최대 1420) ] */
	/* uchar8_t    	U8_EEC_FADE_OUT_CONTROL               */		       1,	/* Comment [ EEC Fade Out 제어 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_HM           */		       9,	/* Comment [ EEC Fade Out 제어 시 High-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.9 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_LM           */		       3,	/* Comment [ EEC Fade Out 제어 시 Low-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_STATE_YAW_STABLE_RISE_MM           */		       6,	/* Comment [ EEC Fade Out 제어 시 Med-mu 토크 업 기울기 설정 ] */
	                                                        		        	/* ScaleVal[      0.6 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_EEC_UTURN_COMP_DYAW_THR1          */		   -2000,	/* Comment [ EEC Uturn 보정 제어 시 보정을 수행하는 Min. Yaw Rate 레벨 설정 ] */
	                                                        		        	/* ScaleVal[  -20 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_EEC_UTURN_COMP_DYAW_THR2          */		   -3000,	/* Comment [ EEC Uturn 보정 제어 시 최대 보정을 수행하는 Max. Yaw Rate 레벨 설정 ] */
	                                                        		        	/* ScaleVal[  -30 deg/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_EEC_UTURN_COMPENSATE               */		       0,	/* Comment [ EEC Uturn 보정 제어 모드 사용 여부 설정 ] */
	/* uchar8_t    	U8_TOD_MTP_THR                        */		       0,	/* Comment [ Full Acceleration 상황을 나타내는 MTP Threshold. ] */
	/* uchar8_t    	U8_TOD_OVER_THR_G1                    */		       0,	/* Comment [ Oversteer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_OVER_THR_G2                    */		       0,	/* Comment [ Severe Oversteer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_UNDER_THR_G1                   */		       0,	/* Comment [ Understeer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uchar8_t    	U8_TOD_UNDER_THR_G2                   */		       0,	/* Comment [ Severe Understeer 상황을 판단하기 위해 사용되는 Threshold Gain ] */
	/* uint16_t    	S16_TOD_MAX_TORQ                      */		       0,	/* Comment [ TOD에서 사용하는 Max. Torque 레벨을 설정함 ] */
	/* uint16_t    	S16_TOD_MIN_TORQ                      */		       0,	/* Comment [ TOD에서 사용하는 Min. Torque 레벨을 설정함 ] */
	/* int16_t     	S16_TOD_TORQ_CST_OVER1                */		       0,	/* Comment [ Coast 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_OVER2                */		       0,	/* Comment [ Coast 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_STABL                */		       0,	/* Comment [ Coast 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_UNDER1               */		       0,	/* Comment [ Coast 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_CST_UNDER2               */		       0,	/* Comment [ Coast 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_OVER1                */		       0,	/* Comment [ Half Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_OVER2                */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_STABL                */		       0,	/* Comment [ Half Open Throttle 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_UNDER1               */		       0,	/* Comment [ Half Open Throttle 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_HOT_UNDER2               */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_OVER1                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_OVER2                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_STABL                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Stable 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_UNDER1               */		       0,	/* Comment [ Wild Open Throttle 주행 중 Understeer 상황에서 TOD 제어 토크 ] */
	/* int16_t     	S16_TOD_TORQ_WOT_UNDER2               */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 토크 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_OVER1                 */		       0,	/* Comment [ Coast 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_OVER2                 */		       0,	/* Comment [ Coast 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_STABL                 */		       0,	/* Comment [ Coast 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_UNDER1                */		       0,	/* Comment [ Coast 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_CST_UNDER2                */		       0,	/* Comment [ Coast 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_OVER1                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_OVER2                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_STABL                 */		       0,	/* Comment [ Half Open Throttle 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_UNDER1                */		       0,	/* Comment [ Half Open Throttle 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_HOT_UNDER2                */		       0,	/* Comment [ Half Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_OVER1                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_OVER2                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Oversteer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_STABL                 */		       0,	/* Comment [ Wild Open Throttle 주행 중 Stable 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_UNDER1                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Understeer 상황에서 TOD 제어 모드 ] */
	/* uchar8_t    	U8_TOD_MODE_WOT_UNDER2                */		       0,	/* Comment [ Wild Open Throttle 주행 중 Severe Understeer 상황에서 TOD 제어 모드 ] */
	/* int16_t     	S16_HSA_a_uphill_detection_along      */		       8,	/* Comment [ Hill grade thres for HSA (default 0.08g) ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_b_MidLongG                    */		      18,	/* Comment [ Midium Hill grade for HSA (default 0.18g) ] */
	                                                        		        	/* ScaleVal[     0.18 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_c_MaxLongG                    */		      35,	/* Comment [ Max Hill grade for HSA (default 0.35g) ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_d_Flat_by_LongG_AVG           */		       4,	/* Comment [ LongG Average thres for Flat Detection (default 0.04g) ] */
	                                                        		        	/* ScaleVal[     0.04 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_PressRatioAtMidHill           */		      10,	/* Comment [ ratio of enter pressure at mid hill ] */
	                                                        		        	/* ScaleVal[      100 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_HSA_STOP_ON_HILL_MIN_SPD          */		       4,	/* Comment [ HSA Determine Stop on hill speed thres. (default : 0.5kph) ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_LongGSafetyFactor             */		       0,	/* Comment [ Safety factor for ax offset (0.01g ~ 0.05g) ] */
	                                                        		        	/* ScaleVal[        0 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_PressToLongGRatio             */		      15,	/* Comment [ The ratio of Assist Pressure to Hill grade (1.5bar/0.01g ~ 2.5bar/0.01g) ] */
	                                                        		        	/* ScaleVal[ 1.5 bar/0.01g ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_MAX_MP_SLOP                   */		      50,	/* Comment [ HSA Max. Mpress slope(default : 5bar/2scan) ] */
	                                                        		        	/* ScaleVal[ 5 bar/2scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_MIN_MP_SLOP                   */		      10,	/* Comment [ HSA Min. Mpress slope(default : 1bar/2scan) ] */
	                                                        		        	/* ScaleVal[ 1 bar/2scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_VV_ACT_COMP_MAX_MP_SLP        */		     150,	/* Comment [ HSA Valve Acting Mpress at Max. Mpress slope(default : 15bar) ] */
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_VV_ACT_COMP_MIN_MP_SLP        */		     100,	/* Comment [ HSA Valve Acting Mpress at Min. Mpress slope(default : 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_HSA_holding_time_on_Accel         */		     150,	/* Comment [ HSA Max hold time on Accel pedal (default : 1.5sec) ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_holding_time_out              */		     150,	/* Comment [ Maximum Assist time of HSA (default 1.5sec) ] */
	                                                        		        	/* ScaleVal[    1.5 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* int16_t     	S16_HSA_a_EngTorqPerLongG             */		      10,	/* Comment [ The ratio of Engine torque to Hill grade for engine ] */
	                                                        		        	/* ScaleVal[  1 %/0.01g ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_b_LongGSFTorqComp             */		       0,	/* Comment [ Engine torque to compensate ax safety factor for engine ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_DEFAULT_ENG_IDLE_TORQ         */		     100,	/* Comment [ HSA Default Engine Idle Torque (default : 10%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_HSA_c_ReleaseTim_4_Accel_1_AT     */		      10,	/* Comment [ Time exceeding engine torque thres1 for HSA release for AT  (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.1 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_d_ReleaseTim_4_Accel_2_AT     */		      25,	/* Comment [ Time exceeding engine torque thres2 for HSA release for AT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[   0.25 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_e_ReleaseTim_4_Accel_1_MT     */		      60,	/* Comment [ Time exceeding engine torque thres1 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.6 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_f_ReleaseTim_4_Accel_2_MT     */		      40,	/* Comment [ Time exceeding engine torque thres2 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	U16_HSA_g_ReleaseTim_4_Accel_3_MT     */		      30,	/* Comment [ Time exceeding engine torque thres3 for HSA release for MT (MIN 0.1s) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uchar8_t    	U8_HSA_AT_MIN_MTP_FOR_EXIT            */		       3,	/* Comment [ minimum mtp for A/T HSA exit ] */
	/* uchar8_t    	U8_HSA_DW_HILL_EXIT_TORQ_COMP         */		       0,	/* Comment [ HSA down hill Exit torque compensation (default : 5%) ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_EST_CLUT_ENG_TH_1             */		     160,	/* Comment [ clutch torq thres1 of M/T vehicle for HSA exit ] */
	/* int16_t     	S16_HSA_EST_CLUT_ENG_TH_2             */		     300,	/* Comment [ clutch torq thres2 of M/T vehicle for HSA exit ] */
	/* int16_t     	S16_HSA_EST_CLUT_REL_TORQ_TH          */		      50,	/* Comment [ minimum engine relative torque for clutch engage detection ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_EST_CLUT_RPM_TH               */		    1300,	/* Comment [ minimum engine RPM for clutch engage detection ] */
	/* uchar8_t    	U8_HSA_EST_CLUT_ENG_INDEX             */		      10,	/* Comment [ index of cluth engage by clutch torq thres ] */
	/* uchar8_t    	U8_HSA_EST_CLUT_MTP_TH                */		       5,	/* Comment [ minimum aceel pedal value for clutch engage detection ] */
	/* int16_t     	S16_HSA_AT_FAST_EXIT_ARAD_TH          */		       8,	/* Comment [ min. wheel accel Th. For HSA fast exit A/T (default 2g) ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -8192 <= X <= 8191.75 ] */
	/* int16_t     	S16_HSA_MT_FAST_EXIT_ARAD_TH          */		       8,	/* Comment [ min. wheel accel Th. For HSA fast exit M/T (default 2g) ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -8192 <= X <= 8191.75 ] */
	/* uchar8_t    	U8_HSA_FAST_EXIT_MTP_TH               */		      30,	/* Comment [ min. accel pedal Th. For HSA fast exit (default 30%) ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MAX            */		       8,	/* Comment [ Maximum pressure fade-out rate during time-out release ] */
	                                                        		        	/* ScaleVal[ 0.8 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MAX_ACC        */		      20,	/* Comment [ Maximum pressure fade-out rate after accel pedal apply ] */
	                                                        		        	/* ScaleVal[ 2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_PHASE_OUT_RATE_MIN            */		       3,	/* Comment [ Minimum pressure fade-out rate during time-out release ] */
	                                                        		        	/* ScaleVal[ 0.3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_G_GAIN          */		       8,	/* Comment [ The ratio of Pressure fade-out rate to Hill grade ] */
	                                                        		        	/* ScaleVal[ 0.08 0.01g/0.1bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_HighMedG           */		      16,	/* Comment [ fade out rate at high-med grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.6 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_LowMedG            */		      18,	/* Comment [ fade out rate at low-med grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.8 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_MaxG               */		      14,	/* Comment [ fade out rate at max grade with acc. ] */
	                                                        		        	/* ScaleVal[ 1.4 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_ACC_MinG               */		      20,	/* Comment [ fade out rate at min grade with acc. ] */
	                                                        		        	/* ScaleVal[ 2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_HighMedG               */		       4,	/* Comment [ fade out rate at high-med grade ] */
	                                                        		        	/* ScaleVal[ 0.4 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_LowMedG                */		       5,	/* Comment [ fade out rate at low-med grade ] */
	                                                        		        	/* ScaleVal[ 0.5 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_MaxG                   */		       3,	/* Comment [ fade out rate at max grade ] */
	                                                        		        	/* ScaleVal[ 0.3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_FO_Rate_MinG                   */		       6,	/* Comment [ fade out rate at min grade ] */
	                                                        		        	/* ScaleVal[ 0.6 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_HighMedLongG                   */		      25,	/* Comment [ high_med grade to decide fade out rate ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_LowMedLongG                    */		      15,	/* Comment [ low_med grade to decide fade out rate ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_PHASEOUT_RATE_RATIO_ACC        */		       0,	/* Comment [ HSA Phase out rate compensation for Accel. exit (default : 50 %) ] */
	                                                        		        	/* ScaleVal[        0 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_FOR_ROLL        */		       2,	/* Comment [ fade out rate after vehicle moving on HSA releasing ] */
	                                                        		        	/* ScaleVal[ 0.2 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_NOT_HILL        */		      30,	/* Comment [ fade out rate at improper condition on HSA ] */
	                                                        		        	/* ScaleVal[ 3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HSA_PHASE_OUT_RATE_REBRK           */		      30,	/* Comment [ fade out rate at re-brake ] */
	                                                        		        	/* ScaleVal[ 3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_Act_Default_TC_Current        */		     450,	/* Comment [ Min TC current For HSA control ] */
	/* int16_t     	S16_HSA_ENTER_INBIT_MPRESS            */		      50,	/* Comment [ The highest mpress inhibited HSA engagement (Default 5 bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_ENTER_LIMIT_MPRESS            */		      70,	/* Comment [ The lowest mpress allowed HSA engagement (Default 10bar/ MIN 5bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_ENTER_LIMIT_SPEED             */		       2,	/* Comment [ The highest speed allowed HSA engagement (Default 0.25 KPH) ] */
	                                                        		        	/* ScaleVal[  0.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_EXIT_SPEED                    */		      16,	/* Comment [ HSA disengagement speed (Default 2 KPH) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HSA_MAX_MP_OFFSET_BLS_FAIL        */		      80,	/* Comment [ MP compensation at BLS fail (default 8bar) ] */
	                                                        		        	/* ScaleVal[      8 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TEMP_1                        */		       0,	/* Comment [ Temp value for HSA ] */
	/* int16_t     	S16_HSA_TEMP_2                        */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_CNTR_P_SCAN                    */		       3,	/* Comment [ HSA Retain pressure control counter during Release state (default : 3scan) ] */
	/* uchar8_t    	U8_HSA_Disable                        */		       0,	/* Comment [ HSA Activation T/P( 0 : Enable / 1 : Disable ) ] */
	/* uchar8_t    	U8_HSA_SET_CLUTCH_ENGAGE_EST          */		       0,	/* Comment [ HSA Setting for Clutch engage estimation ( 0 : Disable / 1 : Enable ) ] */
	/* uchar8_t    	U8_HSA_SET_DECAY_RATE_EACH_SLOP       */		       0,	/* Comment [ HSA Setting for Decay rate each slope ( 0 : Disable / 1 : Enable ) ] */
	/* uchar8_t    	U8_HSA_TEMP_3                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_4                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_5                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* uchar8_t    	U8_HSA_TEMP_6                         */		       0,	/* Comment [ Temp value for HSA ] */
	/* int16_t     	S16_HSA_CNTR_MIN_TC_CURRENT           */		     150,	/* Comment [ HSA Min. TC Current(default : 150mA) ] */
	/* int16_t     	S16_HSA_INIT_CNTR_CUR_MAX_COMP        */		     300,	/* Comment [ HSA Max. Compensated Current during Initial Hold pattern Control Time (default : 300mA) ] */
	/* int16_t     	S16_HSA_INIT_HOLD_CNTR_T              */		      20,	/* Comment [ HSA Initial Hold pattern Control Time (default : 200ms) ] */
	                                                        		        	/* ScaleVal[     200 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_HSA_ISG_1st_Drop_Cur              */		      50,	/* Comment [ fade out 1st scan TC current drop at ISG(default 50mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MAX               */		     750,	/* Comment [ hold current for Target press 60bar (default 750mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MID               */		     520,	/* Comment [ hold current for Target press 25bar (default 520mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_CUR_MIN               */		     280,	/* Comment [ hold current for Target press 5bar (default 280mA) ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MAX             */		     400,	/* Comment [ HSA Hold Max. pressure (default : 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MID             */		     250,	/* Comment [ HSA Hold Mid. pressure (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_HOLD_PRESS_MIN             */		      50,	/* Comment [ HSA Hold Min. pressure (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MAX           */		    -100,	/* Comment [ HSA TC Initial comp. current for Max. pressure (default : -100) ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MID           */		     -50,	/* Comment [ HSA TC Initial comp. current for Mid. pressure (default : -50) ] */
	/* int16_t     	S16_HSA_TC_INITIAL_COMP_MIN           */		     100,	/* Comment [ HSA TC Initial comp. current for Min. pressure (default : 100) ] */
	/* int16_t     	S16_ISG_End_RPM_Th                    */		     500,	/* Comment [ RPM thres for ISG exit (default 500RPM) ] */
	/* int16_t     	S16_ISG_ON_MIN_SLOP_HOLD_PRESS        */		     100,	/* Comment [ minimum assist pressure (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	S8_ISG_HSA_ON_AX_TH_GEAR_D            */		      -3,	/* Comment [ grade thres for ISG (default -0.03g) ] */
	                                                        		        	/* ScaleVal[    -0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_ISG_ON_MIN_SLOPE                   */		       3,	/* Comment [ max grade for minimum assist pressure (default 0.03g) ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_RATE_GAIN_FOR_ISG                  */		       5,	/* Comment [ ratio of ISG fade out rate compare with HSA (default 10<same rate with HSA>) ] */
	/* char8_t     	S8_RATE_GAIN_FOR_ISG_ACC              */		       5,	/* Comment [ ratio of ISG fade out rate after accel compared with HSA (default 10<same rate with HSA>) ] */
	/* uchar8_t    	U8_ISG_End_RPM_OK_delay_time          */		       5,	/* Comment [ time delay after RPM thres satisfaction (default 0.05sec) ] */
	                                                        		        	/* ScaleVal[   0.05 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_RISE_ENABLE                    */		       0,	/* Comment [ HSA rise mode enable (0:disable, 1:enable) ] */
	/* int16_t     	S16_HSA_RISE_WL_SPD_TH                */		       2,	/* Comment [ Minimum wheel speed for roll back detection  (default 0.25KPH) ] */
	                                                        		        	/* ScaleVal[   0.25 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HSA_RISE_NUM_OF_WL                 */		       2,	/* Comment [ Number of wheel satisefied roll back detection Th.  (default 2) ] */
	/* int16_t     	S16_HSA_CUR_AT_RISE                   */		     850,	/* Comment [ TC valve current at rise mode (default 850mA) ] */
	/* int16_t     	S16_HSA_MOTOR_ON_TIME                 */		      30,	/* Comment [ motor on-time at rise mode (default 0.3sec@10bar) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_HSA_RISE_PRESS                    */		     100,	/* Comment [ rise press at roll back detection (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_MOTOR_TARGET_VOLT              */		      20,	/* Comment [ motor target volt at rise mode (default 2volt) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HSA_HOLD_COMP_P_OVERLOAD          */		     100,	/* Comment [ compensasion of target hold press at vehicle overload (default 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_DCT_OVERLOAD_RISE_NUM          */		       2,	/* Comment [ Number of roll back rise for vehicle overload detection  (default 2) ] */
	/* int16_t     	S16_HSA_T_P_TEMP_1                    */		       0,	/* Comment [ HSA temporarily T/P 1 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_10                   */		       0,	/* Comment [ HSA temporarily T/P 10 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_2                    */		       0,	/* Comment [ HSA temporarily T/P 2 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_3                    */		       0,	/* Comment [ HSA temporarily T/P 3 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_4                    */		       0,	/* Comment [ HSA temporarily T/P 4 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_5                    */		       0,	/* Comment [ HSA temporarily T/P 5 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_6                    */		       0,	/* Comment [ HSA temporarily T/P 6 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_7                    */		       0,	/* Comment [ HSA temporarily T/P 7 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_8                    */		       0,	/* Comment [ HSA temporarily T/P 8 ] */
	/* int16_t     	S16_HSA_T_P_TEMP_9                    */		       0,	/* Comment [ HSA temporarily T/P 9 ] */
	/* int16_t     	S16_HSA_AX_DIFF_FOR_DIVE_DETECT       */		     100,	/* Comment [ HSA Ax Difference for Dive Detection (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_LOW_REF               */		     100,	/* Comment [ HSA Ax Difference Low Refernce (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_MAX_REF               */		     800,	/* Comment [ HSA Ax Difference Max Refernce (default : 0.08g) ] */
	                                                        		        	/* ScaleVal[     0.08 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* int16_t     	S16_HSA_AX_DIFF_MED_REF               */		     600,	/* Comment [ HSA Ax Difference Med Refernce (default : 0.06g) ] */
	                                                        		        	/* ScaleVal[     0.06 g ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ -3.2768 <= X <= 3.2767 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_LOW_DIF        */		      20,	/* Comment [ HSA Ax Avg. Valid Thres Low Difference (default : 200ms) ] */
	                                                        		        	/* ScaleVal[     200 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_MAX_DIF        */		      40,	/* Comment [ HSA Ax Avg. Valid Thres Max Difference (default : 400ms) ] */
	                                                        		        	/* ScaleVal[     400 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_AX_AVG_VALID_TH_MID_DIF        */		      30,	/* Comment [ HSA Ax Avg. Valid Thres Mid Difference (default : 300ms) ] */
	                                                        		        	/* ScaleVal[     300 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_HSA_PRESS_DEC_RATE_TC_OFF         */		      10,	/* Comment [ HSA Decrease press during TCS interface time (default : 1bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HSA_SPIN_EXIT_SET_TIME             */		      50,	/* Comment [ HSA TCS interface time after HSA Exit by Wheel spin (default : 500ms) ] */
	                                                        		        	/* ScaleVal[     500 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_HSA_INDICATION_ON_AX_TH            */		       7,	/* Comment [ HSA status indication threhold slope (default : 0.07 g) ] */
	                                                        		        	/* ScaleVal[     0.07 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_HSA_ONHILL_SET_REF_AX_OK_T         */		      25,	/* Comment [ HSA on hill detection time for HSA status indication (default : 250ms) ] */
	                                                        		        	/* ScaleVal[     250 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_AVH_HIGH_MID_SLOP_MIN_H_P         */		      60,	/* Comment [ high-min down 경사에서 정차 가능한 최소 브레이크 답력 (default : 6bar) ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_HIGH_MID_SLOP_SAFE_H_P        */		     270,	/* Comment [ high-mid down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 27bar) ] */
	                                                        		        	/* ScaleVal[     27 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_H_M_SLOP_MIN_H_P_UP           */		      70,	/* Comment [ high-min up 경사에서 정차 가능한 최소 브레이크 답력 (default : 7bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_H_M_SLOP_SAFE_H_P_UP          */		     250,	/* Comment [ high-mid up 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_LOW_MID_SLOP_MIN_H_P          */		      50,	/* Comment [ low-mid down 경사에서 정차 가능한 최소 브레이크 답력 (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_LOW_MID_SLOP_SAFE_H_P         */		     200,	/* Comment [ low-mid down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_L_M_SLOP_MIN_H_P_UP           */		      60,	/* Comment [ low-mid up 경사에서 정차 가능한 최소 브레이크 답력 (default : 6bar) ] */
	                                                        		        	/* ScaleVal[      6 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_L_M_SLOP_SAFE_H_P_UP          */		     200,	/* Comment [ low-mid up 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_MIN_H_P              */		      70,	/* Comment [ max down 경사에서 제어 정차 가능한 최소 브레이크 답력  (default : 7bar) ] */
	                                                        		        	/* ScaleVal[      7 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_MIN_H_P_UP           */		     100,	/* Comment [ max up 경사에서 제어 정차 가능한 최소 브레이크 답력  (default : 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_SAFE_H_P             */		     350,	/* Comment [ max down 경사에서 밀림 발생없는 충분한 브레이크 압력  (default : 35bar) ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MAX_SLOP_SAFE_H_P_UP          */		     350,	/* Comment [ max up 경사에서 밀림 발생없는 충분한 브레이크 압력  (default : 35bar) ] */
	                                                        		        	/* ScaleVal[     35 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_MIN_H_P              */		      50,	/* Comment [ min down 경사에서 정차 가능한 최소 브레이크 답력 (default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_MIN_H_P_MT           */		      30,	/* Comment [ Minimum MCP for M/T AVH(default : 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_SLOP_SAFE_H_P             */		     130,	/* Comment [ min down 경사에서 밀림 발생없는 충분한 브레이크 압력 (default : 13bar) ] */
	                                                        		        	/* ScaleVal[     13 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_HIGH_MID_SLOP                  */		      17,	/* Comment [ high-mid 기준 경사 (default : 17%) ] */
	/* uchar8_t    	U8_AVH_LOW_MID_SLOP                   */		      10,	/* Comment [ low-mid 기준 경사 (default : 10%) ] */
	/* uchar8_t    	U8_AVH_MAX_SLOP                       */		      25,	/* Comment [ max 기준 경사 (default : 25%) ] */
	/* uchar8_t    	U8_AVH_MIN_SLOP                       */		       3,	/* Comment [ min 기준 경사 (default : 3%) ] */
	/* int16_t     	S16_AVH_MAX_MP_SLOP                   */		     100,	/* Comment [ valve 제어 진입 시점 결정을 위한 MCP 감소율 최대값 (default : 10bar/0.02s) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_MIN_MP_SLOP                   */		      10,	/* Comment [ valve 제어 진입 시점 결정을 위한 MCP 감소율 최소값 (default : 1bar/0.02s) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_VV_ACT_COMP_MAX_MP_SLP        */		     180,	/* Comment [ S16_AVH_MAX_MP_SLOP 이상 영역의 valve 제어 진입 시점 MCP 보상값 (default : 18bar, 타겟압력+18bar) ] */
	                                                        		        	/* ScaleVal[     18 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_VV_ACT_COMP_MIN_MP_SLP        */		      30,	/* Comment [ S16_AVH_MIN_MP_SLOP 이하 영역의 valve 제어 진입 시점 MCP 보상값 (default : 3bar, 타겟압력+3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1              */		      10,	/* Comment [ level 1 해제 모드 진입 엔진 토크(APS>0%, Idle 토크 대비 증가량) (default : 1%) ] */
	                                                        		        	/* ScaleVal[        1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1_LV2          */		     200,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>5%, Idle 토크 대비 증가량) (default : 20%) ] */
	                                                        		        	/* ScaleVal[       20 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH1_LV3          */		     350,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>10%, Idle 토크 대비 증가량) (default : 35%) ] */
	                                                        		        	/* ScaleVal[       35 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV2_M1          */		       2,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV3_M1          */		       3,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_LOW_MID_SLOP       */		     900,	/* Comment [ low-mid경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MAX_SLOP           */		     900,	/* Comment [ max경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MID_SLOP           */		     900,	/* Comment [ high-mid경사, 즉시 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_F_END_E_TQ_MIN_SLOP           */		     900,	/* Comment [ min경사, 즉시 해제 기준 엔진 토크 Th. (Idle 토크 대비 증가량) (default : 70%) ] */
	                                                        		        	/* ScaleVal[       90 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_HMID                */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_LMID                */		      18,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[      1.8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MAX                 */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MIN                 */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV2_TQ_TH           */		     150,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>10%,  Idle 토크 대비 증가량) (default : 15%) ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV3_TQ_TH           */		     300,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>20%,  Idle 토크 대비 증가량) (default : 30%) ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_HMID               */		      43,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.43 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_LMID               */		      21,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.21 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MAX                */		      57,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[   0.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MIN                */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV2                  */		       2,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV3                  */		       3,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_HMID_MT             */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_LMID_MT             */		      18,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[      1.8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MAX_MT              */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH1_MIN_MT              */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV2_TQ_TH_MT        */		     150,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>10%,  Idle 토크 대비 증가량) (default : 15%) ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_UP_DRV_FO_LV3_TQ_TH_MT        */		     300,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>20%,  Idle 토크 대비 증가량) (default : 30%) ] */
	                                                        		        	/* ScaleVal[       30 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_HMID_MT            */		      43,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.43 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_LMID_MT            */		      21,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.21 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MAX_MT             */		      57,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[   0.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH1_MIN_MT             */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV2_MT               */		       2,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_UP_DRV_FO_LV3_MT               */		       3,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_HMID                */		      30,	/* Comment [ high-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2.5%) ] */
	                                                        		        	/* ScaleVal[        3 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_LMID                */		      18,	/* Comment [ low-mid경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 2%) ] */
	                                                        		        	/* ScaleVal[      1.8 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_MAX                 */		      35,	/* Comment [ max경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 3%) ] */
	                                                        		        	/* ScaleVal[      3.5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_FO_TQ_TH2_MIN                 */		       1,	/* Comment [ min경사, 해제 기준 엔진 토크 (Idle 토크 대비 증가량) (default : 1.5%) ] */
	                                                        		        	/* ScaleVal[      0.1 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_HMID               */		      29,	/* Comment [ high-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.29 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_LMID               */		      21,	/* Comment [ low-mid경사, 해제 기준 time delay (default : 0.2s) ] */
	                                                        		        	/* ScaleVal[   0.21 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_MAX                */		      57,	/* Comment [ max경사, 해제 기준 time delay (default : 0.4s) ] */
	                                                        		        	/* ScaleVal[   0.57 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_AVH_T_DELAY_TH2_MIN                */		       0,	/* Comment [ min경사, 해제 기준 time delay (default : 0s) ] */
	                                                        		        	/* ScaleVal[      0 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2              */		      12,	/* Comment [ level 1 해제 모드 진입 엔진 토크(APS>0%, Idle 토크 대비 증가량) (default : 1%) ] */
	                                                        		        	/* ScaleVal[      1.2 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2_LV2          */		      50,	/* Comment [ level 2 해제 모드 진입 엔진 토크(APS>5%, Idle 토크 대비 증가량) (default : 20%) ] */
	                                                        		        	/* ScaleVal[        5 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_DW_DRV_FO_TQ_TH2_LV3          */		     100,	/* Comment [ level 3 해제 모드 진입 엔진 토크(APS>10%, Idle 토크 대비 증가량) (default : 35%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV2_M2          */		       3,	/* Comment [ level 2 해제 모드, 감압 rate gain (default : 2) ] */
	/* uchar8_t    	U8_AVH_DW_DRV_FO_RATE_LV3_M2          */		       6,	/* Comment [ level 3 해제 모드, 감압 rate gain (default : 3) ] */
	/* int16_t     	S16_AVH_CUR_INIT_MAX_CUR_COMP         */		     150,	/* Comment [ initial valve 도달 최대값 보상량. 해당값 만큼 높아짐 (default : 150mA) ] */
	/* int16_t     	S16_AVH_HIGH_CUR_INIT_COMP            */		     150,	/* Comment [ high 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : 150mA) ] */
	/* int16_t     	S16_AVH_LOW_CUR_INIT_COMP             */		     -30,	/* Comment [ low 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : -30mA) ] */
	/* int16_t     	S16_AVH_MED_CUR_INIT_COMP             */		     100,	/* Comment [ med 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : 100mA) ] */
	/* int16_t     	S16_AVH_MIN_CUR_INIT_COMP             */		     -50,	/* Comment [ min 타겟압력  영역 작동 시, valve 제어 최초 TC 전류 보상값. 해당값 만큼 낮아짐 (default : -50mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_HIGH_REF_P          */		     400,	/* Comment [ High Ref Press (default : 40bar) ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_HIGH_TH_CUR         */		     610,	/* Comment [ High Ref Press hold 가능 TC 전류 (default : 615mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_LOW_REF_P           */		     120,	/* Comment [ Low Ref Press (default : 12bar) ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_LOW_TH_CUR          */		     420,	/* Comment [ Low Ref Press hold 가능 TC 전류 (default : 360mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MED_REF_P           */		     250,	/* Comment [ Med Ref Press (default : 25bar) ] */
	                                                        		        	/* ScaleVal[     25 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MED_TH_CUR          */		     550,	/* Comment [ Med Ref Press hold 가능 TC 전류 (default : 570mA) ] */
	/* int16_t     	S16_AVH_P_TO_DUTY_MIN_CUR             */		     300,	/* Comment [ Min Ref Press hold 가능 TC 전류 (default : 250mA) ] */
	/* uchar8_t    	U8_AVH_CUR_DOWN_TIME_AFTER_RISE       */		      25,	/* Comment [ AVH Down time after Rise mode (default : 250ms) ] */
	                                                        		        	/* ScaleVal[   250 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_CUR_INIT_DOWN_TIME             */		      20,	/* Comment [ 제어 초기 전류 패턴 하강 시간 (default : 200ms) ] */
	                                                        		        	/* ScaleVal[   200 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_CUR_INIT_UP_TIME               */		      15,	/* Comment [ 제어 초기 전류 패턴 상승 시간 (default : 150ms) ] */
	                                                        		        	/* ScaleVal[   150 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_MED              */		     800,	/* Comment [ medium fade out시 1st scan 전류 감소량 (default : 80mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_MED                    */		      70,	/* Comment [ medium fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_MED                   */		      20,	/* Comment [ medium fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_SLOW             */		      60,	/* Comment [ slow fade out시 1st scan 전류 감소량 (default : 60mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_SLOW                   */		       3,	/* Comment [ slow fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_SLOW                  */		       2,	/* Comment [ slow fade out시 전류 감소 주기  (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_FLAT             */		      50,	/* Comment [ Flat fade out시 1st scan 전류 감소량 (default : 85mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_FLAT                   */		       4,	/* Comment [ Flat fade out시 주기별 전류 감소량 (default : 2mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_FLAT_AUTO              */		       4,	/* Comment [ auto act 모드 중, 평지 fade out시 주기별 전류 감소량 (default : 2mA) ] */
	/* uchar8_t    	U8_AVH_F_O_FLAT_SLOPE_TH              */		       5,	/* Comment [ Flat fade out mode 적용 최대 경사도 (default : 5%) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_FLAT                  */		       2,	/* Comment [ Flat fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_UPHILL           */		      50,	/* Comment [ up drive fade out시 1st scan 전류 감소량 (default : 80mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_UPHILL                 */		       7,	/* Comment [ up drive fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_UPHILL_AUTO            */		       7,	/* Comment [ auto act 모드 중, 오르막 fade out시 주기별 전류 감소량 (default : 7mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_UPHILL                */		       2,	/* Comment [ up drive fade out시 전류 감소 주기 (default : 2scan) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_DW_DRV_MAX       */		      90,	/* Comment [ down drive fade out시 1st scan 전류 감소량 (25% 경사) (default : 120mA) ] */
	/* int16_t     	S16_AVH_F_O_CUR_DROP_DW_DRV_MIN       */		      50,	/* Comment [ down drive fade out시 1st scan 전류 감소량 (8% 경사) (default : 120mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_DOWN_DRV               */		       5,	/* Comment [ down drive fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_CUR_DOWN_DRV_AUTO          */		       5,	/* Comment [ auto act 모드 중, 내리막 fade out시 주기별 전류 감소량 (default : 3mA) ] */
	/* uchar8_t    	U8_AVH_F_O_SCAN_DOWN_DRV              */		       2,	/* Comment [ down drive fade out시 전류 감소 주기 (default : 2scan) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENTER_DELAY          */		      30,	/* Comment [ Sensitive mode enter delay time for vehicle stabilization (default : 0.30s) ] */
	                                                        		        	/* ScaleVal[     300 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENTER_THRES          */		       5,	/* Comment [ Min. enter press for sensitive mode at min slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_HM_THRES         */		       5,	/* Comment [ High mid enter press for sensitive mode at high mid slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_LM_THRES         */		       5,	/* Comment [ Low mid enter press for sensitive mode at low mid slope (default : 5bar) ] */
	/* uchar8_t    	U8_AVH_SENSITIVE_ENT_MAX_THRES        */		       5,	/* Comment [ Max. enter press for sensitive mode at max slope (default : 5bar) ] */
	/* int16_t     	S16_AVH_T_P_TEMP_1                    */		       0,	/* Comment [ AVH temporarily T/P 1 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_10                   */		       0,	/* Comment [ AVH temporarily T/P 10 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_2                    */		       0,	/* Comment [ AVH temporarily T/P 2 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_3                    */		       0,	/* Comment [ AVH temporarily T/P 3 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_4                    */		       0,	/* Comment [ AVH temporarily T/P 4 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_5                    */		       0,	/* Comment [ AVH temporarily T/P 5 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_6                    */		       0,	/* Comment [ AVH temporarily T/P 6 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_7                    */		       0,	/* Comment [ AVH temporarily T/P 7 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_8                    */		       0,	/* Comment [ AVH temporarily T/P 8 ] */
	/* int16_t     	S16_AVH_T_P_TEMP_9                    */		       0,	/* Comment [ AVH temporarily T/P 9 ] */
	/* int16_t     	S16_AVH_DEFAULT_IDLE_TORQ             */		     100,	/* Comment [ AVH Default Idle torque (default : 10%) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AVH_ISG_IDLETOQ_INHIBIT_TIME      */		      60,	/* Comment [ AVH Exception time for calcuate Idle torque during ISG (default : 600ms) ] */
	                                                        		        	/* ScaleVal[     600 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* int16_t     	S16_SCC_TEMP_1                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_2                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_3                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_4                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* int16_t     	S16_SCC_TEMP_5                        */		       0,	/* Comment [ SCC Temporary Variable ] */
	/* uchar8_t    	U8_SCC_ENGTORQ_OFFSET_HIGHSPD         */		       1,	/* Comment [ Offset for min. Engine Torque at med-high speed above 35kph ] */
	/* uchar8_t    	U8_SCC_ENGTORQ_OFFSET_LOWSPD          */		       8,	/* Comment [ Offset for min. Engine Torque at low speed below 15kph ] */
	/* uint16_t    	U16_SCC_Gear_1                        */		     143,	/* Comment [ 1st Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_10                       */		      22,	/* Comment [ 10th Gear Ratio * Final Gear *10 , If max gear is below 10th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_2                        */		      94,	/* Comment [ 2nd Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_3                        */		      63,	/* Comment [ 3rd Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_4                        */		      47,	/* Comment [ 4th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_5                        */		      39,	/* Comment [ 5th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_6                        */		      32,	/* Comment [ 6th Gear Ratio * Final Gear *10 ] */
	/* uint16_t    	U16_SCC_Gear_7                        */		      25,	/* Comment [ 7th Gear Ratio * Final Gear *10 , If max gear is below 7th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_8                        */		      22,	/* Comment [ 8th Gear Ratio * Final Gear *10 , If max gear is below 8th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_9                        */		      22,	/* Comment [ 9th Gear Ratio * Final Gear *10 , If max gear is below 9th, use max gear value ] */
	/* uint16_t    	U16_SCC_Gear_R                        */		      89,	/* Comment [ Rear Gear Ratio * Final Gear *10 ] */
	/* uchar8_t    	U8_SCC_MASS                           */		      20,	/* Comment [ Mass of Vehicle ] */
	                                                        		        	/* ScaleVal[    2000 kg ]	Scale [ f(x) = (X + 0) * 100 ]  Range   [ 0 <= X <= 25500 ] */
	/* uchar8_t    	U8_SCC_TIRE_R                         */		      33,	/* Comment [ Rolling Radius of Tire ] */
	                                                        		        	/* ScaleVal[     0.33 m ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_SCC_CONT_E_D_GAIN                  */		      10,	/* Comment [ D gain for Engine & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_D_GAIN               */		      10,	/* Comment [ D gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_I_GAIN               */		      55,	/* Comment [ Inverse I gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G1_P_GAIN               */		      35,	/* Comment [ P gain for Engine & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_E_G23_D_GAIN              */		      10,	/* Comment [ D gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_G23_I_GAIN              */		      75,	/* Comment [ I gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_G23_P_GAIN              */		      23,	/* Comment [ P gain for engine control on 2 or 3 gear position ] */
	/* char8_t     	S8_SCC_CONT_E_I_GAIN                  */		      47,	/* Comment [ Inverse I gain for Engine & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_E_P_GAIN                  */		      20,	/* Comment [ P gain for Engine & above 2nd Gear ] */
	/* uchar8_t    	U8_SCC_ENG_FADEOUT_SLOPE              */		       5,	/* Comment [ Fadeout Slope of Engine Torque ] */
	/* uchar8_t    	U8_SCC_FF_DEC_FACTOR_NEG_G            */		     100,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq<0 ] */
	/* uchar8_t    	U8_SCC_FF_DEC_FACTOR_POS_G            */		     100,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq>0 ] */
	/* uchar8_t    	U8_SCC_I_GAIN_AMP_BRK_HILL            */		      15,	/* Comment [ I-Gain amp at Brake & hill & vref<8kph ] */
	/* uchar8_t    	U8_SCC_I_GAIN_AMP_ENG_HILL            */		      15,	/* Comment [ I-Gain amp at Engine & hill & vref<2kph ] */
	/* uchar8_t    	U8_SCC_PID_GAIN_DIV_ENG_HILL          */		       3,	/* Comment [ PID-Gain amp at Engine & hill & accel & vref<8kph ] */
	/* int16_t     	S16_SCC_BRAKE_CONTROL_MIN_TORQUE      */		     100,	/* Comment [ Dead zone torque for Brake control activation (default = 100Nm) ] */
	/* int16_t     	S16_SCC_MAX_ENGINE_DRAG_TORQUE_COMP   */		     250,	/* Comment [ Max. engine drag torque to substitute brake control (default = 250Nm) ] */
	/* char8_t     	S8_SCC_CONT_B_D_GAIN                  */		      15,	/* Comment [ D gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_D_GAIN               */		      10,	/* Comment [ D gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_I_GAIN               */		      35,	/* Comment [ Inverse I gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_G1_P_GAIN               */		      25,	/* Comment [ P gain for Brake & 1st Gear ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_D_GAIN    */		      14,	/* Comment [ D gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_I_GAIN    */		      25,	/* Comment [ I gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLDOWN_STOP_P_GAIN    */		      28,	/* Comment [ P gain for stop control on downhill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_D_GAIN      */		       3,	/* Comment [ D gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_I_GAIN      */		      18,	/* Comment [ I gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_HILLUP_STOP_P_GAIN      */		      12,	/* Comment [ P gain for stop control on uphill ] */
	/* char8_t     	S8_SCC_CONT_B_I_GAIN                  */		      45,	/* Comment [ Inverse I gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_P_GAIN                  */		      18,	/* Comment [ P gain for Brake & above 2nd Gear ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_D_GAIN             */		      10,	/* Comment [ D gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_I_GAIN             */		      25,	/* Comment [ I gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_CONT_B_STOP_P_GAIN             */		      28,	/* Comment [ P gain for stop brake control on flat road ] */
	/* char8_t     	S8_SCC_TARGET_P_MIN_STOP              */		     110,	/* Comment [ min Pressure to hold PID-output on flat road ] */
	                                                        		        	/* ScaleVal[     11 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* uchar8_t    	U8_SCC_BRAKE_CONST                    */		      10,	/* Comment [ factor between Wheel Torque and Pressure ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_G1_MAX            */		       5,	/* Comment [ Max. torque rate for limiting engine torque increasing at 1st gear position ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_G1_MIN            */		       1,	/* Comment [ Min. torque rate for limiting engine torque decreasing at 1st gear position ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_MAX               */		       2,	/* Comment [ Max. torque rate for limiting engine torque increasing at gear position above 1st ] */
	/* uchar8_t    	U8_SCC_ENGTORQSLOPE_MIN               */		       1,	/* Comment [ Min. torque rate for limiting engine torque decreasing at gear position above 1st ] */
	/* int16_t     	S16_SCC_OVERRIDE_ACCEL_DIFF_1         */		    -200,	/* Comment [ Accel. difference for using engine torque after override (default -0.2g) ] */
	                                                        		        	/* ScaleVal[     -0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_SCC_OVERRIDE_ACCEL_DIFF_2         */		      10,	/* Comment [ Accel. difference for using SCC request torque after override (default 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* uint16_t    	U16_SCC_I_ERROR_LEVEL_H               */		     100,	/* Comment [ High SCC Decel. Error level for using ERROR_H pseudo Integration gain (default = 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_SCC_I_ERROR_LEVEL_S               */		      30,	/* Comment [ Small SCC Decel. Error level for using ERROR_S pseudo Integration gain (default = 0.03g) ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uchar8_t    	U8_SCC_I_PSEUDO_GAIN_ERROR_H          */		       3,	/* Comment [ Pseudo Integration gain at High SCC Decel. Error level (default = 3) ] */
	/* uchar8_t    	U8_SCC_I_PSEUDO_GAIN_ERROR_S          */		       5,	/* Comment [ Pseudo Integration gain at Small SCC Decel. Error level (default = 5) ] */
	/* int16_t     	S16_CDM_TEMP_1                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_2                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_3                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_4                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* int16_t     	S16_CDM_TEMP_5                        */		       0,	/* Comment [ CDM Temporary Variable ] */
	/* char8_t     	S8_CDM_CONT_B_D_GAIN                  */		       8,	/* Comment [ D gain for CDM brake control above gear 2nd ] */
	/* char8_t     	S8_CDM_CONT_B_I_GAIN                  */		      36,	/* Comment [ I gain for CDM brake control above gear 2nd ] */
	/* char8_t     	S8_CDM_CONT_B_P_GAIN                  */		      40,	/* Comment [ P gain for CDM brake control above gear 2nd ] */
	/* uchar8_t    	U8_CDM_FF_DEC_FACTOR_NEG_G            */		     160,	/* Comment [ Amp ratio of feedforward of target wheel torque at AccReq<0 (default 160) ] */
	/* int16_t     	S16_CDM_CONT_MIN_HIGH_G_THRES         */		     400,	/* Comment [ Min. Deceleration Level for High-g CDM control (default = 0.4g) ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_CDM_CONT_B_D_GAIN_HIGH_G           */		       8,	/* Comment [ D gain for CDM brake control with High-g request (default = 8) ] */
	/* char8_t     	S8_CDM_CONT_B_I_GAIN_HIGH_G           */		      36,	/* Comment [ I gain for CDM brake control with High-g request (default = 36) ] */
	/* char8_t     	S8_CDM_CONT_B_P_GAIN_HIGH_G           */		      40,	/* Comment [ P gain for CDM brake control with High-g request (default = 40) ] */
	/* uchar8_t    	U8_CDM_ENG_CONTROL_EXIT_MTP           */		      60,	/* Comment [ CDM engine brake exit above this throttle position (default = 60%) ] */
	/* uchar8_t    	U8_CDM_ENG_CONTROL_REQ_MTP            */		      50,	/* Comment [ CDM engine brake activation below this throttle position (default = 50%) ] */
	/* uchar8_t    	U8_CDM_ENG_CTRL_TORQUE_DOWN_RATE      */		     100,	/* Comment [ Torque down rate of CDM engine brake control (default = 100 Nm) ] */
	/* uchar8_t    	U8_CDM_ENG_CTRL_TORQUE_UP_RATE        */		     100,	/* Comment [ Torque up rate of CDM engine brake control (default = 100 Nm) ] */
	/* uint16_t    	U16_CDM_I_ERROR_LEVEL_H               */		     500,	/* Comment [ High CDM Decel. Error level for using ERROR_H pseudo Integration gain (default = 0.5g) ] */
	                                                        		        	/* ScaleVal[      0.5 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uint16_t    	U16_CDM_I_ERROR_LEVEL_S               */		     200,	/* Comment [ Small CDM Decel. Error level for using ERROR_S pseudo Integration gain (default = 0.2g) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 65.535 ] */
	/* uchar8_t    	U8_CDM_I_PSEUDO_GAIN_ERROR_H          */		      10,	/* Comment [ Pseudo Integration gain at High CDM Decel. Error level (default = 10) ] */
	/* uchar8_t    	U8_CDM_I_PSEUDO_GAIN_ERROR_S          */		       1,	/* Comment [ Pseudo Integration gain at Small CDM Decel. Error level (default = 1) ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_RPM               */		     900,	/* Comment [ Idle RPM of M/T vehicle ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_STALL_RPM         */		     700,	/* Comment [ RPM level to happen engine stall ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_TORQ              */		     100,	/* Comment [ Idle torque of M/T vehicle (relative torque) ] */
	                                                        		        	/* ScaleVal[       10 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_MT_ENG_IDLE_TORQ_COMP         */		      40,	/* Comment [ Idle torque compensation value at engine overload. (ex, air conditioner on) ] */
	                                                        		        	/* ScaleVal[        4 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_MT_ENG_TORQ_OK                */		      70,	/* Comment [ Idle torque compensation value at engine overload. (ex, air conditioner on) ] */
	                                                        		        	/* ScaleVal[        7 % ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HDC_TS_RPMC_FAST_RPM_DW           */		     200,	/* Comment [ RPM comp. for fast increase target speed. when RPM dot <-20 (default 200RPM) ] */
	/* int16_t     	S16_HDC_TS_RPMC_MID_RPM_DW            */		     100,	/* Comment [ RPM comp. for slow increase target speed. when RPM dot <-10 (default 100RPM) ] */
	/* uchar8_t    	U8_HDC_RAPID_STALL_RPM_RATIO          */		       8,	/* Comment [ % of stall RPM for fast increase target speed (default 80%) ] */
	                                                        		        	/* ScaleVal[       80 % ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ 0 <= X <= 2550 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_1          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 1st gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_2          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 2nd gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_3          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 3rd gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_4          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 4th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_5          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 5th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_ETA_6          */		      80,	/* Comment [ Torque Efficiency from Engine to Wheel @ 6th gear, default : 80% ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_1       */		      30,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.030g ] */
	                                                        		        	/* ScaleVal[     0.03 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_2       */		      25,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.025g ] */
	                                                        		        	/* ScaleVal[    0.025 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_3       */		      20,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.020g ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_4       */		      15,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.015g ] */
	                                                        		        	/* ScaleVal[    0.015 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_5       */		      10,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.010g ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_AX_OFFSET_MODEL_ENG_RESIST_6       */		       5,	/* Comment [ Ax Offset Engine Model resistance for HMC/KMC/SYMC, default : 0.005g ] */
	                                                        		        	/* ScaleVal[    0.005 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* int16_t     	HBB_PRESSURE_HYSTERESIS               */		     100,	/* Comment [ Drop press at pedal releasing state (default = 10bar) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_1             */		      12,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL1 (default = 12) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_2             */		      13,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL2 (default = 13) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_3             */		      14,	/* Comment [ Knee point ratio correspond to S16_HBB_VACUUM_PRESS_LEVEL3 (default = 14) ] */
	/* uchar8_t    	U8_HBB_KNEE_POINT_RATIO_4             */		      18,	/* Comment [ Knee point ratio correspond to HbbNormalVacuumPressure (default = 18) ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_1         */		       0,	/* Comment [ Pedal Travel Level 1 for HBB Mpress Model at No Vacuum level (default = 0mm) ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_2         */		      50,	/* Comment [ Pedal Travel Level 2 for HBB Mpress Model at No Vacuum level (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_3         */		     400,	/* Comment [ Pedal Travel Level 3 for HBB Mpress Model at No Vacuum level (default = 40mm) ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_PEDAL_TRAVEL_4         */		    1500,	/* Comment [ Pedal Travel Level 4 for HBB Mpress Model at No Vacuum level (default = 150mm) ] */
	                                                        		        	/* ScaleVal[     150 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_1         */		       0,	/* Comment [ Mpress Level 1 correspond to Pedal Travel Level 1 at No Vacuum (default = 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_2         */		      30,	/* Comment [ Mpress Level 2 correspond to Pedal Travel Level 1 at No Vacuum (default = 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_3         */		     300,	/* Comment [ Mpress Level 3 correspond to Pedal Travel Level 1 at No Vacuum (default = 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_TARGET_PRESS_4         */		    1300,	/* Comment [ Mpress Level 4 correspond to Pedal Travel Level 1 at No Vacuum (default = 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel1                       */		       0,	/* Comment [ Pedal Travel Level 1 for HBB Mpress Model (default = 0mm) ] */
	                                                        		        	/* ScaleVal[       0 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel2                       */		      50,	/* Comment [ Pedal Travel Level 2 for HBB Mpress Model (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel3                       */		     400,	/* Comment [ Pedal Travel Level 3 for HBB Mpress Model (default = 40mm) ] */
	                                                        		        	/* ScaleVal[      40 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbPedalTravel4                       */		    1500,	/* Comment [ Pedal Travel Level 4 for HBB Mpress Model (default = 150mm) ] */
	                                                        		        	/* ScaleVal[     150 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure1                    */		       0,	/* Comment [ Mpress Level 1 correspond to Pedal Travel Level 1 (default = 0bar) ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure2                    */		      30,	/* Comment [ Mpress Level 2 correspond to Pedal Travel Level 2 (default = 3bar) ] */
	                                                        		        	/* ScaleVal[      3 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure3                    */		     300,	/* Comment [ Mpress Level 3 correspond to Pedal Travel Level 3 (default = 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbTargetPressure4                    */		    1300,	/* Comment [ Mpress Level 4 correspond to Pedal Travel Level 4 (default = 130bar) ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HbbNormalVacuumPressure               */		     650,	/* Comment [ Max. Vacuum Press for HBB activation (default = 65Kpa) ] */
	                                                        		        	/* ScaleVal[     65 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	Pedal_Travel_Rate_0                   */		       0,	/* Comment [ Pedal Travel Rate for fast HBB activation (default = 0) ] */
	                                                        		        	/* ScaleVal[ 0 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	Pedal_Travel_Rate_UPPER_LIMIT         */		     350,	/* Comment [ Pedal Travel Rate for late HBB activation (default = 350) ] */
	                                                        		        	/* ScaleVal[ 35 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL1           */		     200,	/* Comment [ Vacuum Press 1 for HBB enter at Speed Thres. 1 (default = 20kPa) ] */
	                                                        		        	/* ScaleVal[     20 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL2           */		     300,	/* Comment [ Vacuum Press 2 for HBB enter at Speed Thres. 2 (default = 30kPa) ] */
	                                                        		        	/* ScaleVal[     30 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VACUUM_PRESS_LEVEL3           */		     400,	/* Comment [ Vacuum Press 3 for HBB enter at Speed Thres. 3 (default = 40kPa) ] */
	                                                        		        	/* ScaleVal[     40 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY1             */		      40,	/* Comment [ Speed Threshold 1 for HBB enter ( default = 5km/h) ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY2             */		     240,	/* Comment [ Speed Threshold 2 for HBB enter ( default = 30km/h) ] */
	                                                        		        	/* ScaleVal[    30 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_HBB_VEHICLE_VELOCITY3             */		     640,	/* Comment [ Speed Threshold 3 for HBB enter ( default = 80km/h) ] */
	                                                        		        	/* ScaleVal[    80 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	U8_HBB_ENABLE_FLG                     */		       0,	/* Comment [ Flag for Enable/Disable according to Engine Variation ] */
	/* int16_t     	S16_HBB_NO_VAC_ENT_INIT_PED_RATE      */		       1,	/* Comment [ Initial Pedal rate for HBB Enter at No Vacuum (default = 0.1mm/3scan) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_NO_VAC_ENT_PEDAL_TRAVEL       */		      10,	/* Comment [ Pedal Travel for HBB Enter at No Vacuum (defualt = 1mm) ] */
	                                                        		        	/* ScaleVal[       1 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_ENTER_INIT_PEDAL_RATE         */		       5,	/* Comment [ Initial Pedal rate for HBB Enter (default = 0.1mm/3scan) ] */
	                                                        		        	/* ScaleVal[ 0.5 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_ENTER_PEDAL_TRAVEL            */		     100,	/* Comment [ Pedal Travel for HBB Enter(defualt = 10) ] */
	                                                        		        	/* ScaleVal[      10 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	HBB_EXIT_PEDAL_TRAVEL                 */		      50,	/* Comment [ HBB exit below this pedal travel stroke (default = 5mm) ] */
	                                                        		        	/* ScaleVal[       5 mm ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_EXIT_VACUUM_LEVEL             */		     600,	/* Comment [ HBB Exit Vacuum Level (default : 60kPa) ] */
	                                                        		        	/* ScaleVal[     60 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* char8_t     	HbbPedalStateApplyThres               */		       1,	/* Comment [ Pedal Travel Rate for HBB apply mode (default = 1) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	HbbPedalStateReleaseThres             */		      -3,	/* Comment [ Pedal Travel Rate for HBB release mode (default = -3) ] */
	                                                        		        	/* ScaleVal[ -0.3 mm/3scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_HBB_ABS_COMB_TC_GAIN              */		      15,	/* Comment [ TC Gain for ABS + HBB Combination Control (default : 15) ] */
	/* int16_t     	S16_HBB_INIT_LIMIT_ASSIST_PRESS       */		     200,	/* Comment [ Initial Assist Pressure limitation for TC V/V control(default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_MIN_ASSIST_PRESS              */		      50,	/* Comment [ Minimum Assist Pressure for TC V/V control after 300ms(default : 5bar) ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_RELEASE_HYSTRESIS_1           */		     200,	/* Comment [ Release pressure hystresis 1 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_RELEASE_HYSTRESIS_2           */		     200,	/* Comment [ Release pressure hystresis 2 (default : 20bar) ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_1                 */		     250,	/* Comment [ TC Mapping Duty 1 (default : 250mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_2                 */		     750,	/* Comment [ TC Mapping Duty 2 (default : 750mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_DUTY_3                 */		    1550,	/* Comment [ TC Mapping Duty 3 (default : 1550mA) ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_1               */		      10,	/* Comment [ TC Mapping Target Pressure 1 (default : 1bar) ] */
	                                                        		        	/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_2               */		     300,	/* Comment [ TC Mapping Target Pressure 2 (default : 30bar) ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_TC_MAP_MPRESS_3               */		    1500,	/* Comment [ TC Mapping Target Pressure 3 (default : 150bar) ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN1         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 1(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN2         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 2(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_PRESS_VEL_GAIN3         */		      10,	/* Comment [ Assist Pressure gain for Speed Thres. 3(default = 10) ] */
	/* uchar8_t    	U8_HBB_ASSIST_P_MPRESS_COMP_GAIN      */		      10,	/* Comment [ Assist Pressure Gain by Mpress compensation  (default = 10) ] */
	/* char8_t     	HBB_Assist_Press_Motor_D_Gain         */		       2,	/* Comment [ HBB MSC D gain of assist pressure (default = 2) ] */
	/* int16_t     	HBB_Assist_Press_Motor_P_Gain         */		       0,	/* Comment [ HBB MSC P gain of assist pressure (default = 0) ] */
	/* int16_t     	S16_HBB_MSC_EBD_COMP_GAIN             */		       7,	/* Comment [ HBB MSC Comp gain on EBD (default = 7) ] */
	/* int16_t     	S16_HBB_MSC_INIT_TIME                 */		      10,	/* Comment [ HBB MSC Initial Time (default = 100msec) ] */
	                                                        		        	/* ScaleVal[   100 msec ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_HBB_ABS_COMB_TARGET_VOL            */		      30,	/* Comment [ HBB MSC Target V during ABS control (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_0                     */		       0,	/* Comment [ HBB Pedal Feel MSC 0 by Mpress (default = 0V) ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_1                     */		       3,	/* Comment [ HBB Pedal Feel MSC 1 by Mpress (default = 0.3V) ] */
	                                                        		        	/* ScaleVal[      0.3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_2                     */		       7,	/* Comment [ HBB Pedal Feel MSC 2 by Mpress (default = 0.7V) ] */
	                                                        		        	/* ScaleVal[      0.7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_MSC_COMP_3                     */		      20,	/* Comment [ HBB Pedal Feel MSC 3 by Mpress (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_1                  */		      10,	/* Comment [ HBB MSC for Release 1 (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_2                  */		      15,	/* Comment [ HBB MSC for Release 2 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_3                  */		      20,	/* Comment [ HBB MSC for Release 3 (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_RELEASE_MSC_4                  */		      30,	/* Comment [ HBB MSC for Release 4 (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_1                     */		      10,	/* Comment [ HBB MSC during Vehicle stops 1 (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_2                     */		      15,	/* Comment [ HBB MSC during Vehicle stops 2 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_3                     */		      20,	/* Comment [ HBB MSC during Vehicle stops 3 (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_STOP_MSC_4                     */		      30,	/* Comment [ HBB MSC during Vehicle stops 4 (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_100BAR       */		      70,	/* Comment [ HBB MSC for Initial Control 4 at No Vacuum Level (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_10BAR        */		      40,	/* Comment [ HBB MSC for Initial Control 2 at No Vacuum Level (default = 4V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_1BAR         */		      30,	/* Comment [ HBB MSC for Initial Control 1 at No Vacuum Level (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_INIT_T_VOL_30BAR        */		      60,	/* Comment [ HBB MSC for Initial Control 3 at No Vacuum Level (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_1         */		      15,	/* Comment [ HBB MSC for Low Speed 1 at No Vacuum Level (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_2         */		      25,	/* Comment [ HBB MSC for Low Speed 2 at No Vacuum Level (default = 2.5V) ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_3         */		      35,	/* Comment [ HBB MSC for Low Speed 3 at No Vacuum Level (default = 3.5V) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_LOW_SPEED_MSC_4         */		      50,	/* Comment [ HBB MSC for Low Speed 4 at No Vacuum Level (default = 5V) ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_100BAR       */		      60,	/* Comment [ HBB MSC 4 at No Vacuum Level (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_10BAR        */		      20,	/* Comment [ HBB MSC 2 at No Vacuum Level (default = 2V) ] */
	                                                        		        	/* ScaleVal[        2 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_1BAR         */		      10,	/* Comment [ HBB MSC 1 at No Vacuum Level (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_NO_VAC_TARGET_VOL_30BAR        */		      40,	/* Comment [ HBB MSC 3 at No Vacuum Level (default = 4V) ] */
	                                                        		        	/* ScaleVal[        4 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_100BAR      */		      70,	/* Comment [ Initial HBB MSC Target V for assist pressure 100bar (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_10BAR       */		      15,	/* Comment [ Initial HBB MSC Target V for assist pressure 10bar (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_1BAR        */		      10,	/* Comment [ Initial HBB MSC Target V for assist pressure 1bar (default = 1V) ] */
	                                                        		        	/* ScaleVal[        1 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_INITIAL_TARGET_VOL_30BAR       */		      60,	/* Comment [ Initial HBB MSC Target V for assist pressure 30bar (default = 6V) ] */
	                                                        		        	/* ScaleVal[        6 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_1                */		      15,	/* Comment [ HBB MSC for Low Speed 1 (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_2                */		      25,	/* Comment [ HBB MSC for Low Speed 2 (default = 2.5V) ] */
	                                                        		        	/* ScaleVal[      2.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_3                */		      35,	/* Comment [ HBB MSC for Low Speed 3 (default = 3.5V) ] */
	                                                        		        	/* ScaleVal[      3.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_LOW_SPEED_MSC_4                */		      50,	/* Comment [ HBB MSC for Low Speed 4 (default = 5V) ] */
	                                                        		        	/* ScaleVal[        5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_100BAR              */		      70,	/* Comment [ HBB MSC Target V for assist pressure 100bar (default = 7V) ] */
	                                                        		        	/* ScaleVal[        7 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_10BAR               */		      15,	/* Comment [ HBB MSC Target V for assist pressure 10bar (default = 1.5V) ] */
	                                                        		        	/* ScaleVal[      1.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_1BAR                */		       5,	/* Comment [ HBB MSC Target V for assist pressure 1bar (default = 0.5V) ] */
	                                                        		        	/* ScaleVal[      0.5 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_HBB_TARGET_VOL_30BAR               */		      30,	/* Comment [ HBB MSC Target V for assist pressure 30bar (default = 3V) ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_HBB_Temp_1                        */		       0,	/* Comment [ HBB Temporarily INT Variable 1 ] */
	/* int16_t     	S16_HBB_Temp_2                        */		       0,	/* Comment [ HBB Temporarily INT Variable 2 ] */
	/* int16_t     	S16_HBB_Temp_3                        */		       0,	/* Comment [ HBB Temporarily INT Variable 3 ] */
	/* int16_t     	S16_HBB_Temp_4                        */		       0,	/* Comment [ HBB Temporarily INT Variable 4 ] */
	/* int16_t     	S16_HBB_Temp_5                        */		       0,	/* Comment [ HBB Temporarily INT Variable 5 ] */
	/* uchar8_t    	U8_HBB_Temp_1                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 1 ] */
	/* uchar8_t    	U8_HBB_Temp_2                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 2 ] */
	/* uchar8_t    	U8_HBB_Temp_3                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 3 ] */
	/* uchar8_t    	U8_HBB_Temp_4                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 4 ] */
	/* uchar8_t    	U8_HBB_Temp_5                         */		       0,	/* Comment [ HBB Temporarily UCHAR Variable 5 ] */
	/* int16_t     	S16_HBB_ADD_BRAKE_MSC_MARGIN          */		      70,	/* Comment [ Add Brake MSC margin (default : 0.07V/(mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.07 V/(mm/scan) ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH1                */		       6,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel1 (default : 0.6 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.6 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH2                */		       5,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel2 (default : 0.5 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.5 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH3                */		       2,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel3 (default : 0.2 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.2 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* char8_t     	S8_HBB_ADD_BRK_SLP_TH4                */		       1,	/* Comment [ Add Brake Threshold slope at HbbPedalTravel4 (default : 0.1 mm/scan) ] */
	                                                        		        	/* ScaleVal[ 0.1 mm/scan ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -12.8 <= X <= 12.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_0        */		      10,	/* Comment [ Vacuum Pressrue 0 for Boost Ratio 0 (default = 1kpa) ] */
	                                                        		        	/* ScaleVal[      1 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_1        */		      40,	/* Comment [ Vacuum Pressrue 1 for Boost Ratio 1 (default = 4kpa) ] */
	                                                        		        	/* ScaleVal[      4 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_2        */		     100,	/* Comment [ Vacuum Pressrue 2 for Boost Ratio 2 (default = 10kpa) ] */
	                                                        		        	/* ScaleVal[     10 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_HBB_BOOST_RATIO_VAC_PRES_3        */		     200,	/* Comment [ Vacuum Pressrue 3 for Boost Ratio 3 (default = 20kpa) ] */
	                                                        		        	/* ScaleVal[     20 kPa ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_0                  */		      14,	/* Comment [ Boost Ratio 0 at Vacuum Pressrue 0 (default = 14) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_1                  */		      12,	/* Comment [ Boost Ratio 1 at Vacuum Pressrue 1 (default = 12) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_2                  */		       8,	/* Comment [ Boost Ratio 2 at Vacuum Pressrue 2 (default = 8) ] */
	/* uchar8_t    	U8_HBB_BOOST_RATIO_3                  */		       5,	/* Comment [ Boost Ratio 3 at Vacuum Pressrue 3 (default = 5) ] */
	/* int16_t     	S16_ADC_TARGET_G_INIT_MAX             */		      -5,	/* Comment [ 제어 시작 1st scan의 목표 감속도 Max Limit (default : -0.05g) ] */
	                                                        		        	/* ScaleVal[    -0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_JUMP_LIMIT           */		       1,	/* Comment [ 추종 cycle 당, 요구 감속도 추종량 (default : 0.01g) ] */
	                                                        		        	/* ScaleVal[     0.01 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_MAX_LIMIT            */		     -40,	/* Comment [ ADC 제어 감속도 max limit (default : -0.4g) ] */
	                                                        		        	/* ScaleVal[     -0.4 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_ADC_TARGET_G_UPDATE_CYCLE         */		      10,	/* Comment [ 요구 감속도 추종 cycle (default : 100ms) ] */
	                                                        		        	/* ScaleVal[     100 ms ]	Scale [ f(x) = (X + 0) * 10 ]  Range   [ -327680 <= X <= 327670 ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL                 */		       3,	/* Comment [ 가속페달 Th. 이하 시의 감압 Level (default : 3) ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL_ACCEL           */		       3,	/* Comment [ 가속페달 Th. 초과 시의 감압 Level (default : 3) ] */
	/* uchar8_t    	U8_ADC_FADE_OUT_LEVEL_ACCEL_REF       */		      10,	/* Comment [ 감압 기울기 Level 결정 위한 가속페달 Th. (default : 10%) ] */
	/* int16_t     	S16_ACC_INITIAL_CURRENT               */		     400,	/* Comment [ 제어 시작 1st scan의 TC v/v 인가 전류 (default : 400mA) ] */
	/* int16_t     	S16_ACC_INITIAL_TARGET_V              */		    5000,	/* Comment [ 제어 초기 Feed Forward 구간의 모터 구동 전압 (default : 5V) ] */
	                                                        		        	/* ScaleVal[     5 volt ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_ADC_BRK_LAMP_ON_REF_TIMER         */		     100,	/* Comment [ 제동 등 점등 index Th. (모터 구동 전압 및 시간에 따른 index) (default : 100) ] */
	/* uchar8_t    	U8_ADC_RESERVED_1                     */		       0,	/* Comment [ ADC reserved 1 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_2                     */		       0,	/* Comment [ ADC reserved 2 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_3                     */		       0,	/* Comment [ ADC reserved 3 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_4                     */		       0,	/* Comment [ ADC reserved 4 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_5                     */		       0,	/* Comment [ ADC reserved 5 (default : 0) ] */
	/* uchar8_t    	U8_ADC_RESERVED_6                     */		       0,	/* Comment [ ADC reserved 6 (default : 0) ] */
	/* int16_t     	S16_WPE_Max_TCMF_Hold_Press           */		   15000,	/* Comment [ TC_Valve_Max_Hold_Press, default : 150bar ] */
	                                                        		        	/* ScaleVal[    150 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* uchar8_t    	U8_Motor_Eccentricity                 */		     100,	/* Comment [ Motor Eccentricity, default : 0.00100m ] */
	                                                        		        	/* ScaleVal[    0.001 m ]	Scale [ f(x) = (X + 0) * 1E-005 ]  Range   [ 0 <= X <= 0.00255 ] */
	/* uchar8_t    	U8_Motor_K_Torque_Current             */		      21,	/* Comment [ Motor_K_Torque_Current, default : 0.021Nm/A ] */
	                                                        		        	/* ScaleVal[ 0.021 Nm/A ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_Motor_Resistance                   */		      35,	/* Comment [ Motor_Resistancer, default : 0.035ohm ] */
	                                                        		        	/* ScaleVal[  0.035 ohm ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.255 ] */
	/* uchar8_t    	U8_Motor_Volt_RPM                     */		      23,	/* Comment [ Motor_Volt_RPM, default : 0.0023Volt/rpm ] */
	                                                        		        	/* ScaleVal[ 0.0023 Volt/rpm ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ 0 <= X <= 0.0255 ] */
	/* uchar8_t    	U8_Piston_Diameter                    */		      78,	/* Comment [ Piston_Diameter, default : 0.0078m ] */
	                                                        		        	/* ScaleVal[   0.0078 m ]	Scale [ f(x) = (X + 0) * 0.0001 ]  Range   [ 0 <= X <= 0.0255 ] */
	/* uchar8_t    	U8_Pump_Efficiency_High_P             */		      50,	/* Comment [ Pump_Efficiency_High_P, default : 50% ] */
	/* uchar8_t    	U8_Pump_Efficiency_Low_P              */		      65,	/* Comment [ Pump_Efficiency_Low_P, default : 65% ] */
	/* int16_t     	S16_Caliper_Model_Volume_No_f         */		     100,	/* Comment [ Front_Caliper_Model_Invalidity_Volume, default : 0.100cc ] */
	                                                        		        	/* ScaleVal[     0.1 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_No_r         */		      30,	/* Comment [ Rear_Caliper_Model_Invalidity_Volume, default : 0.030cc ] */
	                                                        		        	/* ScaleVal[    0.03 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Press_1             */		       0,	/* Comment [ Caliper_Model_Pressure_Map_1, default : 0bar ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_10            */		    8000,	/* Comment [ Caliper_Model_Pressure_Map_10, default : 80bar ] */
	                                                        		        	/* ScaleVal[     80 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_11            */		    9000,	/* Comment [ Caliper_Model_Pressure_Map_11, default : 90bar ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_12            */		   10000,	/* Comment [ Caliper_Model_Pressure_Map_12, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_13            */		   11000,	/* Comment [ Caliper_Model_Pressure_Map_13, default : 110bar ] */
	                                                        		        	/* ScaleVal[    110 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_14            */		   12000,	/* Comment [ Caliper_Model_Pressure_Map_14, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_15            */		   13000,	/* Comment [ Caliper_Model_Pressure_Map_15, default : 130bar ] */
	                                                        		        	/* ScaleVal[    130 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_16            */		   14000,	/* Comment [ Caliper_Model_Pressure_Map_16, default : 140bar ] */
	                                                        		        	/* ScaleVal[    140 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_2             */		     500,	/* Comment [ Caliper_Model_Pressure_Map_2, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_3             */		    1000,	/* Comment [ Caliper_Model_Pressure_Map_3, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_4             */		    2000,	/* Comment [ Caliper_Model_Pressure_Map_4, default : 20bar ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_5             */		    3000,	/* Comment [ Caliper_Model_Pressure_Map_5, default : 30bar ] */
	                                                        		        	/* ScaleVal[     30 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_6             */		    4000,	/* Comment [ Caliper_Model_Pressure_Map_6, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_7             */		    5000,	/* Comment [ Caliper_Model_Pressure_Map_7, default : 50bar ] */
	                                                        		        	/* ScaleVal[     50 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_8             */		    6000,	/* Comment [ Caliper_Model_Pressure_Map_8, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Press_9             */		    7000,	/* Comment [ Caliper_Model_Pressure_Map_9, default : 70bar ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_1          */		       0,	/* Comment [ Front_Caliper_Model_Volume_Map_1, default : 0.000cc ] */
	                                                        		        	/* ScaleVal[       0 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_10         */		    2377,	/* Comment [ Front_Caliper_Model_Volume_Map_10, default : 2.377cc ] */
	                                                        		        	/* ScaleVal[   2.377 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_11         */		    2515,	/* Comment [ Front_Caliper_Model_Volume_Map_11, default : 2.515cc ] */
	                                                        		        	/* ScaleVal[   2.515 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_12         */		    2650,	/* Comment [ Front_Caliper_Model_Volume_Map_12, default : 2.650cc ] */
	                                                        		        	/* ScaleVal[    2.65 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_13         */		    2789,	/* Comment [ Front_Caliper_Model_Volume_Map_13, default : 2.789cc ] */
	                                                        		        	/* ScaleVal[   2.789 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_14         */		    2919,	/* Comment [ Front_Caliper_Model_Volume_Map_14, default : 2.919cc ] */
	                                                        		        	/* ScaleVal[   2.919 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_15         */		    3058,	/* Comment [ Front_Caliper_Model_Volume_Map_15, default : 3.058cc ] */
	                                                        		        	/* ScaleVal[   3.058 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_16         */		    3192,	/* Comment [ Front_Caliper_Model_Volume_Map_16, default : 3.192cc ] */
	                                                        		        	/* ScaleVal[   3.192 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_2          */		     660,	/* Comment [ Front_Caliper_Model_Volume_Map_2, default : 0.660cc ] */
	                                                        		        	/* ScaleVal[    0.66 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_3          */		     860,	/* Comment [ Front_Caliper_Model_Volume_Map_3, default : 0.860cc ] */
	                                                        		        	/* ScaleVal[    0.86 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_4          */		    1180,	/* Comment [ Front_Caliper_Model_Volume_Map_4, default : 1.180cc ] */
	                                                        		        	/* ScaleVal[    1.18 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_5          */		    1460,	/* Comment [ Front_Caliper_Model_Volume_Map_5, default : 1.460cc ] */
	                                                        		        	/* ScaleVal[    1.46 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_6          */		    1690,	/* Comment [ Front_Caliper_Model_Volume_Map_6, default : 1.690cc ] */
	                                                        		        	/* ScaleVal[    1.69 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_7          */		    1890,	/* Comment [ Front_Caliper_Model_Volume_Map_7, default : 1.890cc ] */
	                                                        		        	/* ScaleVal[    1.89 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_8          */		    2080,	/* Comment [ Front_Caliper_Model_Volume_Map_8, default : 2.080cc ] */
	                                                        		        	/* ScaleVal[    2.08 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_f_9          */		    2228,	/* Comment [ Front_Caliper_Model_Volume_Map_9, default : 2.228cc ] */
	                                                        		        	/* ScaleVal[   2.228 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_1          */		       0,	/* Comment [ Rear_Caliper_Model_Volume_Map_1, default : 0.000cc ] */
	                                                        		        	/* ScaleVal[       0 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_10         */		    1080,	/* Comment [ Rear_Caliper_Model_Volume_Map_10, default : 1.080cc ] */
	                                                        		        	/* ScaleVal[    1.08 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_11         */		    1150,	/* Comment [ Rear_Caliper_Model_Volume_Map_11, default : 1.150cc ] */
	                                                        		        	/* ScaleVal[    1.15 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_12         */		    1230,	/* Comment [ Rear_Caliper_Model_Volume_Map_12, default : 1.230cc ] */
	                                                        		        	/* ScaleVal[    1.23 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_13         */		    1300,	/* Comment [ Rear_Caliper_Model_Volume_Map_13, default : 1.300cc ] */
	                                                        		        	/* ScaleVal[     1.3 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_14         */		    1370,	/* Comment [ Rear_Caliper_Model_Volume_Map_14, default : 1.370cc ] */
	                                                        		        	/* ScaleVal[    1.37 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_15         */		    1440,	/* Comment [ Rear_Caliper_Model_Volume_Map_15, default : 1.440cc ] */
	                                                        		        	/* ScaleVal[    1.44 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_16         */		    1510,	/* Comment [ Rear_Caliper_Model_Volume_Map_16, default : 1.510cc ] */
	                                                        		        	/* ScaleVal[    1.51 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_2          */		     350,	/* Comment [ Rear_Caliper_Model_Volume_Map_2, default : 0.350cc ] */
	                                                        		        	/* ScaleVal[    0.35 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_3          */		     440,	/* Comment [ Rear_Caliper_Model_Volume_Map_3, default : 0.440cc ] */
	                                                        		        	/* ScaleVal[    0.44 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_4          */		     570,	/* Comment [ Rear_Caliper_Model_Volume_Map_4, default : 0.570cc ] */
	                                                        		        	/* ScaleVal[    0.57 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_5          */		     670,	/* Comment [ Rear_Caliper_Model_Volume_Map_5, default : 0.670cc ] */
	                                                        		        	/* ScaleVal[    0.67 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_6          */		     760,	/* Comment [ Rear_Caliper_Model_Volume_Map_6, default : 0.760cc ] */
	                                                        		        	/* ScaleVal[    0.76 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_7          */		     850,	/* Comment [ Rear_Caliper_Model_Volume_Map_7, default : 0.850cc ] */
	                                                        		        	/* ScaleVal[    0.85 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_8          */		     920,	/* Comment [ Rear_Caliper_Model_Volume_Map_8, default : 0.920cc ] */
	                                                        		        	/* ScaleVal[    0.92 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_Caliper_Model_Volume_r_9          */		    1000,	/* Comment [ Rear_Caliper_Model_Volume_Map_9, default : 1.000cc ] */
	                                                        		        	/* ScaleVal[       1 cc ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ -32.768 <= X <= 32.767 ] */
	/* int16_t     	S16_MP_Rise_Gain_f_1_volume           */		      50,	/* Comment [ Front_MP_Rise_Gain_1, default : 50% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_2_volume           */		      60,	/* Comment [ Front_MP_Rise_Gain_2, default : 60% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_3_volume           */		      90,	/* Comment [ Front_MP_Rise_Gain_3, default : 90% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_4_volume           */		     140,	/* Comment [ Front_MP_Rise_Gain_4, default : 140% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_5_volume           */		     160,	/* Comment [ Front_MP_Rise_Gain_5, default : 160% ] */
	/* int16_t     	S16_MP_Rise_Gain_f_6_volume           */		     180,	/* Comment [ Front_MP_Rise_Gain_6, default : 180% ] */
	/* int16_t     	S16_MP_Rise_Press_f_1_volume          */		      50,	/* Comment [ Front_MP_Rise_Press_1, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_2_volume          */		     100,	/* Comment [ Front_MP_Rise_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_3_volume          */		     400,	/* Comment [ Front_MP_Rise_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_4_volume          */		     600,	/* Comment [ Front_MP_Rise_Press_4, default : 60bar ] */
	                                                        		        	/* ScaleVal[     60 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_5_volume          */		     900,	/* Comment [ Front_MP_Rise_Press_5, default : 90bar ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_f_6_volume          */		    1200,	/* Comment [ Front_MP_Rise_Press_6, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Gain_r_1_volume           */		      30,	/* Comment [ Rear_MP_Rise_Gain_1, default : 30% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_2_volume           */		      45,	/* Comment [ Rear_MP_Rise_Gain_2, default : 45% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_3_volume           */		      90,	/* Comment [ Rear_MP_Rise_Gain_3, default : 90% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_4_volume           */		     130,	/* Comment [ Rear_MP_Rise_Gain_4, default : 130% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_5_volume           */		     140,	/* Comment [ Rear_MP_Rise_Gain_5, default : 140% ] */
	/* int16_t     	S16_MP_Rise_Gain_r_6_volume           */		     150,	/* Comment [ Rear_MP_Rise_Gain_6, default : 150% ] */
	/* int16_t     	S16_MP_Rise_Press_r_1_volume          */		      50,	/* Comment [ Rear_MP_Rise_Press_1, default : 5bar ] */
	                                                        		        	/* ScaleVal[      5 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_2_volume          */		     100,	/* Comment [ Rear_MP_Rise_Press_2, default : 10bar ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_3_volume          */		     400,	/* Comment [ Rear_MP_Rise_Press_3, default : 40bar ] */
	                                                        		        	/* ScaleVal[     40 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_4_volume          */		     700,	/* Comment [ Rear_MP_Rise_Press_4, default : 70bar ] */
	                                                        		        	/* ScaleVal[     70 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_5_volume          */		    1000,	/* Comment [ Rear_MP_Rise_Press_5, default : 100bar ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_MP_Rise_Press_r_6_volume          */		    1200,	/* Comment [ Rear_MP_Rise_Press_6, default : 120bar ] */
	                                                        		        	/* ScaleVal[    120 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_4                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_4, default : 0 ] */
	/* int16_t     	S16_WPE_VOLUME_TEMP_5                 */		       0,	/* Comment [ S16_WPE_VOLUME_TEMP_5, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_4                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_4, default : 0 ] */
	/* uchar8_t    	U8_WPE_VOLUME_TEMP_5                  */		       0,	/* Comment [ U8_WPE_VOLUME_TEMP_5, default : 0 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_01                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_01 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_02                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_02 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_03                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_03 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_04                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_04 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_05                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_05 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_06                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_06 ] */
	/* int16_t     	S16_VWE_ENG_TEMP_07                   */		       0,	/* Comment [ Vehicle_Load_Estimation__ENG_TEMP_07 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_1                */		      75,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_01 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_2                */		      80,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_02 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_3                */		      85,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_03 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_4                */		      90,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_04 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_5                */		      90,	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_05 ] */
	/* uchar8_t    	U8_LAC_MODEL_ENG_ETA_6                */		      90	/* Comment [ Vehicle_Load_Estimation__ENG_ETA_06 ] */
};

#define LOGIC_CAL_MODULE_19_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

