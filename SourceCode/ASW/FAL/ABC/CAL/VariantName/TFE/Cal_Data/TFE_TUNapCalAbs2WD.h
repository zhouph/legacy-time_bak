#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_0_START


const /*far*/	DATA_APCALABS_t	apCalAbs2WD =
{

	/* uint16_t    	apCalAbsInfo.CDID                     */		0x0A20,
	/* uint16_t    	apCalAbsInfo.CDVer                    */		0x4131,
	/* uchar8_t    	apCalAbsInfo.MID                      */		0x02,
	/* uchar8_t    	U8_MINI_WHEEL_DOWN_RATE               */		      98,	/* Comment [ Compensation Ratio for Mini-spare Wheel (MINI SPARE WHEEL 적용시 정상 WHEEL과의 속도차를 보정함) ] */
	/* uchar8_t    	U8_PULSE_HIGH_MU                      */		      70,	/* Comment [ Differentiation Point of High Mu Region ] */
	                                                        		        	/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_PULSE_LOW_MED_MU                   */		      30,	/* Comment [ Differentiation Point of Low-med MU Region ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_PULSE_LOW_MU                       */		      20,	/* Comment [ Differentiation Point of Low MU Region ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_PULSE_MEDIUM_MU                    */		      40,	/* Comment [ Differentiation Point of Med MU Region ] */
	                                                        		        	/* ScaleVal[      0.4 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_PULSE_MED_HIGH_MU                  */		      55,	/* Comment [ Differentiation Point of Med-high MU Region ] */
	                                                        		        	/* ScaleVal[     0.55 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_PULSE_UNDER_LOW_MED_MU             */		      25,	/* Comment [ Differentiation Point of Low-med MU Region ] */
	                                                        		        	/* ScaleVal[     0.25 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* char8_t     	S8_dV_F_slip_after_ABS_fz             */		       4,	/* Comment [ rel_lam for outside front wheel after ABS_fz(flags=234) ] */
	/* uchar8_t    	U8_dV_Higher_Limit_F                  */		      64,	/* Comment [ Higher Limit of d_V in Front Wheels ] */
	                                                        		        	/* ScaleVal[     8 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Higher_Limit_R                  */		      32,	/* Comment [ Higher Limit of d_V in Rear Wheels ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_100kph_F               */		      16,	/* Comment [ d_V at 100kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_10kph_F                */		      12,	/* Comment [ d_V at 10kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_20kph_F                */		      12,	/* Comment [ d_V at 20kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_40kph_F                */		      16,	/* Comment [ d_V at 40kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_60kph_F                */		      16,	/* Comment [ d_V at 60kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_80kph_F                */		      20,	/* Comment [ d_V at 80kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_High_Vel_dV_Pro_BF_AFZOK_F         */		       8,	/* Comment [ d_V Percentage of Vref above 100kph(Before AFZ OK, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   3.1248 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_100kph_R               */		      16,	/* Comment [ d_V at 100kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_10kph_R                */		      12,	/* Comment [ d_V at 10kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_20kph_R                */		      12,	/* Comment [ d_V at 20kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_40kph_R                */		      14,	/* Comment [ d_V at 40kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_60kph_R                */		      14,	/* Comment [ d_V at 60kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_BF_AFZOK_80kph_R                */		      12,	/* Comment [ d_V at 80kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_High_Vel_dV_Pro_BF_AFZOK_R         */		       8,	/* Comment [ d_V Percentage of Vref above 100kph(Before AFZ OK, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   3.1248 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_HMu_100kph_F                    */		      24,	/* Comment [ d_V at 100kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_10kph_F                     */		      12,	/* Comment [ d_V at 10kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_20kph_F                     */		      16,	/* Comment [ d_V at 20kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_40kph_F                     */		      20,	/* Comment [ d_V at 40kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_60kph_F                     */		      24,	/* Comment [ d_V at 60kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_80kph_F                     */		      24,	/* Comment [ d_V at 80kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_HMu_F              */		       9,	/* Comment [ d_V Percentage of Vref above 100kph(High_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   3.5154 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_HMu_100kph_R                    */		      22,	/* Comment [ d_V at 100kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  2.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_10kph_R                     */		      12,	/* Comment [ d_V at 10kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_20kph_R                     */		      16,	/* Comment [ d_V at 20kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_40kph_R                     */		      20,	/* Comment [ d_V at 40kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_60kph_R                     */		      22,	/* Comment [ d_V at 60kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  2.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_HMu_80kph_R                     */		      22,	/* Comment [ d_V at 80kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  2.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_HMu_R              */		       8,	/* Comment [ d_V Percentage of Vref above 100kph(High_Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   3.1248 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_MMu_100kph_F                    */		      22,	/* Comment [ d_V at 100kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  2.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_10kph_F                     */		      12,	/* Comment [ d_V at 10kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_20kph_F                     */		      12,	/* Comment [ d_V at 20kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_40kph_F                     */		      16,	/* Comment [ d_V at 40kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_60kph_F                     */		      20,	/* Comment [ d_V at 60kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_80kph_F                     */		      20,	/* Comment [ d_V at 80kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_MMu_F              */		       9,	/* Comment [ d_V Percentage of Vref above 100kph(Medium Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   3.5154 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_MMu_100kph_R                    */		      16,	/* Comment [ d_V at 100kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_10kph_R                     */		      12,	/* Comment [ d_V at 10kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_20kph_R                     */		      12,	/* Comment [ d_V at 20kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_40kph_R                     */		      14,	/* Comment [ d_V at 40kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_60kph_R                     */		      14,	/* Comment [ d_V at 60kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_MMu_80kph_R                     */		      14,	/* Comment [ d_V at 80kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_MMu_R              */		       9,	/* Comment [ d_V Percentage of Vref above 100kph(Medium Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   3.5154 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_LMMu_100kph_F                   */		      14,	/* Comment [ d_V at 100kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_10kph_F                    */		      12,	/* Comment [ d_V at 10kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_20kph_F                    */		      12,	/* Comment [ d_V at 20kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_40kph_F                    */		      14,	/* Comment [ d_V at 40kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_60kph_F                    */		      14,	/* Comment [ d_V at 60kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_80kph_F                    */		      14,	/* Comment [ d_V at 80kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_LMMu_F             */		       8,	/* Comment [ d_V Percentage of Vref above 100kph(Lowmed Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   3.1248 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_LMMu_100kph_R                   */		      12,	/* Comment [ d_V at 100kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_10kph_R                    */		      10,	/* Comment [ d_V at 10kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_20kph_R                    */		      10,	/* Comment [ d_V at 20kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_40kph_R                    */		      10,	/* Comment [ d_V at 40kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_60kph_R                    */		      12,	/* Comment [ d_V at 60kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMMu_80kph_R                    */		      12,	/* Comment [ d_V at 80kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_LMMu_R             */		       6,	/* Comment [ d_V Percentage of Vref above 100kph(Lowmed Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   2.3436 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_LMu_100kph_F                    */		      14,	/* Comment [ d_V at 100kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_10kph_F                     */		      10,	/* Comment [ d_V at 10kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_20kph_F                     */		      10,	/* Comment [ d_V at 20kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_40kph_F                     */		      12,	/* Comment [ d_V at 40kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_60kph_F                     */		      14,	/* Comment [ d_V at 60kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_80kph_F                     */		      14,	/* Comment [ d_V at 80kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_LMu_F              */		       6,	/* Comment [ d_V Percentage of Vref above 100kph(Low Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.3436 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_LMu_100kph_R                    */		      12,	/* Comment [ d_V at 100kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_10kph_R                     */		      10,	/* Comment [ d_V at 10kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_20kph_R                     */		      10,	/* Comment [ d_V at 20kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_40kph_R                     */		      10,	/* Comment [ d_V at 40kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_60kph_R                     */		      10,	/* Comment [ d_V at 60kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_LMu_80kph_R                     */		      10,	/* Comment [ d_V at 80kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_LMu_R              */		       6,	/* Comment [ d_V Percentage of Vref above 100kph(Low Mu, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   2.3436 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_DV_BASE_PRO_FOR_GMA_WL             */		       5,	/* Comment [ dV percentage after U8_GMA_WL_DV_SET_COUNTER_REF times pulse-up ] */
	                                                        		        	/* ScaleVal[    1.953 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_GMA_WL_DV_SET_COUNTER_REF          */		       5,	/* Comment [ No. of GMA Pulse-ups needed to reach High-Mu skid pressure ] */
	/* uchar8_t    	U8_dV_Pro_High_Vel_SpMu_F             */		       5,	/* Comment [ d_V Percentage of Vref above 100kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[    1.953 % ]	Scale [ f(x) = (X + 0) * 0.3906 ]  Range   [ 0 <= X <= 99.603 ] */
	/* uchar8_t    	U8_dV_SpMu_100kph_F                   */		      20,	/* Comment [ d_V at 100kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_SpMu_10kph_F                    */		      12,	/* Comment [ d_V at 10kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_SpMu_20kph_F                    */		      12,	/* Comment [ d_V at 20kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_SpMu_40kph_F                    */		      14,	/* Comment [ d_V at 40kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[  1.75 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_SpMu_60kph_F                    */		      16,	/* Comment [ d_V at 60kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_dV_SpMu_80kph_F                    */		      16,	/* Comment [ d_V at 80kph(Split Mu High side, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     2 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_GMA_H_SPEED                       */		     640,	/* Comment [ GMA High Speed Reference, default 70kph, min 70, max 100 ] */
	                                                        		        	/* ScaleVal[     80 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_GMA_L_SPEED                       */		     280,	/* Comment [ GMA Low Speed Reference, default 35kph, min 20, max 50 ] */
	                                                        		        	/* ScaleVal[     35 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 200 ] */
	/* int16_t     	S16_GMA_M_SPEED                       */		     400,	/* Comment [ GMA Medium Speed Reference, default 50kph, min 40, max 70 ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 200 ] */
	/* uchar8_t    	U8_GMA_D_F_H_SPEED                    */		       6,	/* Comment [ GMA DUMP TIME FRONT @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_GMA_D_F_L_SPEED                    */		       2,	/* Comment [ GMA DUMP TIME FRONT @S16_GMA_L_SPEED ] */
	/* uchar8_t    	U8_GMA_D_F_M_SPEED                    */		       5,	/* Comment [ GMA DUMP TIME FRONT @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_GMA_D_R_H_SPEED                    */		       7,	/* Comment [ GMA DUMP TIME REAR @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_GMA_D_R_L_SPEED                    */		       5,	/* Comment [ GMA DUMP TIME REAR @S16_GMA_L_SPEED ] */
	/* uchar8_t    	U8_GMA_D_R_M_SPEED                    */		       6,	/* Comment [ GMA DUMP TIME REAR @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_HOLD13_H_SPEED                     */		      20,	/* Comment [ HOLD_TIME 1st to 3rd hold phase @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_HOLD45_H_SPEED                     */		      26,	/* Comment [ HOLD_TIME 4th to 5th hold phase @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_HOLD67_H_SPEED                     */		      30,	/* Comment [ HOLD_TIME 6th to 7th hold phase @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_HOLD8_H_SPEED                      */		      40,	/* Comment [ HOLD_TIME after 8th hold phase @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_HOLD_0_H_SPEED                     */		      10,	/* Comment [ HOLD_TIME at initial hold phase @S16_GMA_H_SPEED ] */
	/* uchar8_t    	U8_HOLD13                             */		      10,	/* Comment [ HOLD_TIME 1st to 3rd hold phase @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_HOLD45                             */		      18,	/* Comment [ HOLD_TIME 4th to 5th hold phase @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_HOLD67                             */		      20,	/* Comment [ HOLD_TIME 6th to 7th hold phase @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_HOLD8                              */		      10,	/* Comment [ HOLD_TIME after 8th hold phase @S16_GMA_M_SPEED ] */
	/* uchar8_t    	U8_HOLD_0                             */		       4,	/* Comment [ HOLD_TIME at initial hold phase @S16_GMA_M_SPEED ] */
	/* char8_t     	S8_GMA_RiseTime_H_SPEED_PHASE1        */		      12,	/* Comment [ GMA pulse-up reapply time @H_SPEED, 1st pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_GMA_RiseTime_H_SPEED_PHASE2        */		       8,	/* Comment [ GMA pulse-up reapply time @H_SPEED, 2nd~4th pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_GMA_RiseTime_H_SPEED_PHASE3        */		       6,	/* Comment [ GMA pulse-up reapply time @H_SPEED, 5th~higher pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_GMA_RiseTime_ML_SPEED_PHASE1       */		      12,	/* Comment [ GMA pulse-up reapply time @ML_SPEED, 1st pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_GMA_RiseTime_ML_SPEED_PHASE2       */		      10,	/* Comment [ GMA pulse-up reapply time @ML_SPEED, 2nd~4th pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_GMA_RiseTime_ML_SPEED_PHASE3       */		       6,	/* Comment [ GMA pulse-up reapply time @ML_SPEED, 5th~higher pulse, default 7, min 5, max 20 ] */
	/* char8_t     	S8_Clear_GMA_Max_Accel1               */		      40,	/* Comment [ Max.Wheel Accel Reference to Clear GMA Ctrl (default:15g) ] */
	                                                        		        	/* ScaleVal[       10 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Clear_GMA_Max_Accel2               */		      40,	/* Comment [ Max.Wheel Accel Reference to Clear GMA Ctrl When Dump scans is fewer than 15 (default:10g) ] */
	                                                        		        	/* ScaleVal[       10 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_LFC_Conven_Reapply_Time_F          */		       8,	/* Comment [ NO Valve On Time for Each On/Off Reapply for Front ] */
	/* char8_t     	S8_LFC_Conven_Reapply_Time_R          */		       8,	/* Comment [ NO Valve On Time for Each On/Off Reapply for Rear ] */
	/* char8_t     	S8_Comp_Duty_For_SPHside_F            */		       2,	/* Comment [ Compensation Duty For Split High-side Front Wheel ] */
	/* uchar8_t    	U8_Initial_Duty_Ratio_F               */		      12,	/* Comment [ Initial Duty for Front Wheels ] */
	/* uchar8_t    	U8_Initial_Duty_Ratio_R               */		       9,	/* Comment [ Initial Duty for Rear Wheels ] */
	/* uchar8_t    	U8_LFC_Initial_Duty_Comp_Low_F        */		       2,	/* Comment [ Initial Duty for Low_Mu(Front Wheels) ] */
	/* uchar8_t    	U8_LFC_Initial_Duty_Comp_Low_R        */		       2,	/* Comment [ Initial Duty for Low_Mu(Rear Wheels) ] */
	/* uchar8_t    	U8_Hold_Timer_ABS_Limit_Speed         */		       4,	/* Comment [ Hold Timer Ref. for LFC rising below ABS Limit Speed ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_High            */		      24/*12*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_High_R          */		      24/*12*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_Initial         */		      24/*12*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_Low             */		      30,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_Low_R           */		      30,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_Med             */		      30/*20*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_Med_R           */		      30/*20*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_Hold_Timer_New_Ref_SpHigh_F        */		      28/*12*/,	/* Comment [ Reference Hold Time ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_High_Ref           */		      24,	/* Comment [ Reference Built Reapply Scans(High_Mu, Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_Low_B_15_Ref       */		      50,	/* Comment [ Reference Built Reapply Scans(Low_Mu(Below_15KPH), Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_Low_Ref            */		      50,	/* Comment [ Reference Built Reapply Scans(Low_Mu, Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_Med_Ref            */		      50,	/* Comment [ Reference Built Reapply Scans(Medium_Mu, Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_Rough_Ref          */		      50,	/* Comment [ Reference Built Reapply Scans(Rough Road, Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_F_SpHigh_Ref         */		      50,	/* Comment [ Reference Built Reapply Scans(Split_Mu, High side Front Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_R_High_Ref           */		      50,	/* Comment [ Reference Built Reapply Scans(High_Mu, Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_R_Low_Ref            */		      50,	/* Comment [ Reference Built Reapply Scans(Low_Mu, Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_R_Med_Ref            */		      50,	/* Comment [ Reference Built Reapply Scans(Medium_Mu, Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_built_cnt_R_Rough_Ref          */		      50,	/* Comment [ Reference Built Reapply Scans(Rough Road, Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_Long_Hold_Comp_Duty_F          */		      50,	/* Comment [ Duty Rise for Temporary Hold(Front) ] */
	/* uchar8_t    	U8_LFC_Long_Hold_Comp_Duty_R          */		      50,	/* Comment [ Duty Rise for Temporary Hold(Rear) ] */
	/* char8_t     	S8_LFC_Initial_Dump_Slip_Low          */		     -15,	/* Comment [ First Cycle에서 Low_Mu 판단의 기준이 되는 slip_at_real_dump ] */
	                                                        		        	/* ScaleVal[       15 % ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 128 <= X <= -127 ] */
	/* char8_t     	S8_LFC_Initial_Peak_Slip_Low          */		     -45,	/* Comment [ First Cycle에서 Low_Mu 판단의 기준이 되는 peak_slip ] */
	                                                        		        	/* ScaleVal[       45 % ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 128 <= X <= -127 ] */
	/* char8_t     	S8_LFC_Initial_Slip_Ratio_Low         */		       3,	/* Comment [ First Cycle에서 Low_Mu 판단의 기준이 되는 peak_slip과 slip_at_real_dump의 비 ] */
	/* uchar8_t    	U8_LFC_Initial_Dump_Scan_Low          */		      40,	/* Comment [ First Cycle에서 무조건 Low_Mu라 판정할 수 있는 기준 Dump량 ] */
	/* uchar8_t    	U8_LFC_Initial_Dump_Scan_Med          */		      15,	/* Comment [ First Cycle에서 Low_Mu라 판정할 수 있는 최소 Dump량 ] */
	/* char8_t     	S8_LFC_Dump_Slip_Jump_Mu              */		     -25,	/* Comment [ High-to-Low Mu_Jump 판단의 기준이 되는 slip_at_real_dump ] */
	                                                        		        	/* ScaleVal[       25 % ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 128 <= X <= -127 ] */
	/* char8_t     	S8_LFC_Peak_Slip_Jump_Mu              */		     -50,	/* Comment [ High-to-Low Mu_Jump 판단의 기준이 되는 peak_slip ] */
	                                                        		        	/* ScaleVal[       50 % ]	Scale [ f(x) = (X + 0) * -1 ]  Range   [ 128 <= X <= -127 ] */
	/* char8_t     	S8_LFC_Slip_Ratio_Jump_Mu             */		       2,	/* Comment [ High-to-Low Mu_Jump 판단의 기준이 되는 peak_slip과 slip_at_real_dump의 비 ] */
	/* uchar8_t    	U8_LFC_2ND_CYC_COMP_LOW               */		      13,	/* Comment [ 2nd deep slip Compensation Duty after initial deep slip detection ] */
	/* uchar8_t    	U8_LFC_Dump_Scan_Jump_Mu              */		      20,	/* Comment [ High-to-Low Mu_Jump 판단 가능한 최소 Dump량 ] */
	/* uchar8_t    	U8_LFC_Dump_Scan_Rough_Road           */		      30,	/* Comment [ Rough_Road에서 High-to-Low Mu_Jump 보상 제한 Dump량 ] */
	/* uchar8_t    	U8_LFC_Jump_Down_Comp_F               */		       2,	/* Comment [ Duty Compensation High-to-Low Mu_Jump(Front Wheels) ] */
	/* uchar8_t    	U8_LFC_Jump_Down_Comp_R               */		       2,	/* Comment [ Duty Compensation High-to-Low Mu_Jump(Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_L2H_Comp_F                     */		       5,	/* Comment [ Compensation Duty for Low-to-High Mu_Jump(Front Wheels) ] */
	/* uchar8_t    	U8_LFC_L2H_Comp_R                     */		       4,	/* Comment [ Compensation Duty for Low-to-High Mu_Jump(Rear Wheels) ] */
	/* uchar8_t    	U8_LFC_L2H_Suspect_Comp_F             */		       3,	/* Comment [ Compensation Duty for Low-to-High suspect (Front Wheels) ] */
	/* char8_t     	S8_Dec_Hump_Counting_arad             */		      12,	/* Comment [ Unstable 영역에서 In_gear_vibration의 경향을 보이는 wheel 감속도 ] */
	                                                        		        	/* ScaleVal[        3 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_IN_GEAR_Vib_Detect_arad1           */		      20,	/* Comment [ In_gear_vibration이라 판단되는 wheel 감속도의 Peak 값 ] */
	                                                        		        	/* ScaleVal[        5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_IN_GEAR_Vib_Detect_arad2           */		      16,	/* Comment [ In_gear_vibration이라 판단되는 wheel 감속도의 Peak 값 ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_IN_GEAR_detect_cycle               */		       3,	/* Comment [ In_gear_vibration을 판단을 허용하는 LFC_zyklus_z ] */
	/* uchar8_t    	U8_IN_GEAR_detect_Dec_Hump_cnt        */		       4,	/* Comment [ In_gear 상태라 판단되는 Dec_hump_counter의 수 ] */
	/* uchar8_t    	U8_IN_GEAR_detect_Vib_cnt             */		       3,	/* Comment [ In_gear 상태라 판단되는 In_gear_vibration_counter의 수 ] */
	/* uchar8_t    	U8_IN_GEAR_Vib_Acc_to_Dec_time        */		      14,	/* Comment [ In_gear_vibration이라 판단되는 Cycle의 Peak_acc에서 Peak_dec까지의 시간 ] */
	/* char8_t     	S8_Pulse_Down_Gain_Front_Decel        */		       5,	/* Comment [ Pulse Down Gain of Front, Accel State ] */
	/* char8_t     	S8_Pulse_Down_Gain_Rear_Decel         */		       7,	/* Comment [ Pulse Down Gain of Rear, Accel State ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_100_150_F      */		      30,	/* Comment [ Decel Gain, Before AFZ OK, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_10_20_F        */		      18,	/* Comment [ Decel Gain, Before AFZ OK, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_20_40_F        */		      20,	/* Comment [ Decel Gain, Before AFZ OK, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_40_60_F        */		      25,	/* Comment [ Decel Gain, Before AFZ OK, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_60_100_F       */		      28,	/* Comment [ Decel Gain, Before AFZ OK, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_Ab_150_F       */		      42,	/* Comment [ Decel Gain, Before AFZ OK, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_B_10_F         */		      12,	/* Comment [ Decel Gain, Before AFZ OK, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_100_150_F       */		      20,	/* Comment [ Slip Gain, Before AFZ OK, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_10_20_F         */		       8,	/* Comment [ Slip Gain, Before AFZ OK, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_20_40_F         */		      12,	/* Comment [ Slip Gain, Before AFZ OK, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_40_60_F         */		      13,	/* Comment [ Slip Gain, Before AFZ OK, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_60_100_F        */		      17,	/* Comment [ Slip Gain, Before AFZ OK, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_Ab_150_F        */		      25,	/* Comment [ Slip Gain, Before AFZ OK, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_B_10_F          */		       6,	/* Comment [ Slip Gain, Before AFZ OK, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_100_150_R      */		      38,	/* Comment [ Decel Gain, Before AFZ OK, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_10_20_R        */		      27,	/* Comment [ Decel Gain, Before AFZ OK, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_20_40_R        */		      27,	/* Comment [ Decel Gain, Before AFZ OK, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_40_60_R        */		      25,	/* Comment [ Decel Gain, Before AFZ OK, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_60_100_R       */		      30,	/* Comment [ Decel Gain, Before AFZ OK, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_Ab_150_R       */		      45,	/* Comment [ Decel Gain, Before AFZ OK, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_BFAFZOK_B_10_R         */		      27,	/* Comment [ Decel Gain, Before AFZ OK, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_100_150_R       */		      24,	/* Comment [ Slip Gain, Before AFZ OK, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_10_20_R         */		       8,	/* Comment [ Slip Gain, Before AFZ OK, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_20_40_R         */		      12,	/* Comment [ Slip Gain, Before AFZ OK, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_40_60_R         */		      16,	/* Comment [ Slip Gain, Before AFZ OK, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_60_100_R        */		      16,	/* Comment [ Slip Gain, Before AFZ OK, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_Ab_150_R        */		      30,	/* Comment [ Slip Gain, Before AFZ OK, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_BFAFZOK_B_10_R          */		       6,	/* Comment [ Slip Gain, Before AFZ OK, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_100_150kph_F       */		      23,	/* Comment [ Decel Gain, High Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_10_20kph_F         */		      12,	/* Comment [ Decel Gain, High Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_20_40kph_F         */		      15,	/* Comment [ Decel Gain, High Mu Detect, 20-40kph, Decel State, Front wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_40_60kph_F         */		      15,	/* Comment [ Decel Gain, High Mu Detect, 40-60kph, Decel State, Front wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_60_100kph_F        */		      15,	/* Comment [ Decel Gain, High Mu Detect, 60-100kph, Decel State, Front wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_Ab_150kph_F        */		      29,	/* Comment [ Decel Gain, High Mu Detect, Above 150kph, Decel State, Front wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_B_10kph_F          */		      24,	/* Comment [ Decel Gain, High Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_100_150kph_F        */		      18,	/* Comment [ Slip Gain, High Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_10_20kph_F          */		       5,	/* Comment [ Slip Gain, High Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_20_40kph_F          */		       8,	/* Comment [ Slip Gain, High Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_40_60kph_F          */		      10,	/* Comment [ Slip Gain, High Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_60_100kph_F         */		      12,	/* Comment [ Slip Gain, High Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_Ab_150kph_F         */		      20,	/* Comment [ Slip Gain, High Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_B_10kph_F           */		       6,	/* Comment [ Slip Gain, High Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_100_150kph_R       */		      24,	/* Comment [ Decel Gain, High Mu Detect, 100-150kph, Decel State, Rear wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_10_20kph_R         */		      12,	/* Comment [ Decel Gain, High Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_20_40kph_R         */		      15,	/* Comment [ Decel Gain, High Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_40_60kph_R         */		      16,	/* Comment [ Decel Gain, High Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_60_100kph_R        */		      18,	/* Comment [ Decel Gain, High Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_Ab_150kph_R        */		      34,	/* Comment [ Decel Gain, High Mu Detect, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_HMu_B_10kph_R          */		      20,	/* Comment [ Decel Gain, High Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_100_150kph_R        */		      18,	/* Comment [ Slip Gain, High Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_10_20kph_R          */		       4,	/* Comment [ Slip Gain, High Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_20_40kph_R          */		       6,	/* Comment [ Slip Gain, High Mu Detect, 20-40kph, Decel State,Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_40_60kph_R          */		      10,	/* Comment [ Slip Gain, High Mu Detect, 40-60kph, Decel State, Rear Wheels- ] */
	/* int16_t     	S16_Slip_Gain_HMu_60_100kph_R         */		      12,	/* Comment [ Slip Gain, High Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_Ab_150kph_R         */		      18,	/* Comment [ Slip Gain, High Mu Detect, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_HMu_B_10kph_R           */		       3,	/* Comment [ Slip Gain, High Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_100_150kph_F       */		      34,	/* Comment [ Decel Gain, Medium Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_10_20kph_F         */		      12,	/* Comment [ Decel Gain, Medium Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_20_40kph_F         */		      17,	/* Comment [ Decel Gain, Medium Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_40_60kph_F         */		      21,	/* Comment [ Decel Gain, Medium Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_60_100kph_F        */		      30,	/* Comment [ Decel Gain, Medium Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_Ab_150kph_F        */		      32,	/* Comment [ Decel Gain, Medium Mu Detect, Above 150 kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_B_10kph_F          */		      10,	/* Comment [ Decel Gain, Medium Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_100_150kph_F        */		      20,	/* Comment [ Slip Gain, Medium Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_10_20kph_F          */		      12,	/* Comment [ Slip Gain, Medium Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_20_40kph_F          */		      16,	/* Comment [ Slip Gain, Medium Mu Detect, 20-40kph, Decel State, FrontWheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_40_60kph_F          */		      20,	/* Comment [ Slip Gain, Medium Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_60_100kph_F         */		      22,	/* Comment [ Slip Gain, Medium Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_Ab_150kph_F         */		      25,	/* Comment [ Slip Gain, Medium Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_B_10kph_F           */		      10,	/* Comment [ Slip Gain, Medium Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_100_150kph_R       */		      34,	/* Comment [ Decel Gain, Medium Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_10_20kph_R         */		      12,	/* Comment [ Decel Gain, Medium Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_20_40kph_R         */		      19,	/* Comment [ Decel Gain, Medium Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_40_60kph_R         */		      23,	/* Comment [ Decel Gain, Medium Mu Detect, 40-60kph, Decel State,RearWheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_60_100kph_R        */		      29,	/* Comment [ Decel Gain, Medium Mu Detect, 60-100kph, Decel State,Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_Ab_150kph_R        */		      32,	/* Comment [ Decel Gain, Medium Mu Detect, Above 150 kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_MMu_B_10kph_R          */		       9,	/* Comment [ Decel Gain, Medium Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_100_150kph_R        */		      20,	/* Comment [ Slip Gain, Medium Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_10_20kph_R          */		       8,	/* Comment [ Slip Gain, Medium Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_20_40kph_R          */		      11,	/* Comment [ Slip Gain, Medium Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_40_60kph_R          */		      14,	/* Comment [ Slip Gain, Medium Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_60_100kph_R         */		      17,	/* Comment [ Slip Gain, Medium Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_Ab_150kph_R         */		      23,	/* Comment [ Slip Gain, Medium Mu Detect, Above 150kph, Decel State,Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_MMu_B_10kph_R           */		       4,	/* Comment [ Slip Gain, Medium Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_100_150kph_F      */		      28,	/* Comment [ Decel Gain, Lowmed Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_10_20kph_F        */		      22,	/* Comment [ Decel Gain, Lowmed Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_20_40kph_F        */		      27,	/* Comment [ Decel Gain, Lowmed Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_40_60kph_F        */		      28,	/* Comment [ Decel Gain, Lowmed Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_60_100kph_F       */		      28,	/* Comment [ Decel Gain, Lowmed Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_Ab_150kph_F       */		      35,	/* Comment [ Decel Gain, Lowmed Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_B_10kph_F         */		      21,	/* Comment [ Decel Gain, Lowmed Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_100_150kph_F       */		      20,	/* Comment [ Slip Gain, Lowmed Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_10_20kph_F         */		      16,	/* Comment [ Slip Gain, Lowmed Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_20_40kph_F         */		      18,	/* Comment [ Slip Gain, Lowmed Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_40_60kph_F         */		      20,	/* Comment [ Slip Gain, Lowmed Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_60_100kph_F        */		      20,	/* Comment [ Slip Gain, Lowmed Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_Ab_150kph_F        */		      25,	/* Comment [ Slip Gain, Lowmed Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_B_10kph_F          */		       7,	/* Comment [ Slip Gain, Lowmed Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_100_150kph_R      */		      35,	/* Comment [ Decel Gain, Lowmed Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_10_20kph_R        */		      15,	/* Comment [ Decel Gain, Lowmed Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_20_40kph_R        */		      19,	/* Comment [ Decel Gain, Lowmed Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_40_60kph_R        */		      23,	/* Comment [ Decel Gain, Lowmed Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_60_100kph_R       */		      28,	/* Comment [ Decel Gain, Lowmed Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_Ab_150kph_R       */		      38,	/* Comment [ Decel Gain, Lowmed Mu Detect, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMMu_B_10kph_R         */		      12,	/* Comment [ Decel Gain, Lowmed Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_100_150kph_R       */		      20,	/* Comment [ Slip Gain, Lowmed Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_10_20kph_R         */		       9,	/* Comment [ Slip Gain, Lowmed Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_20_40kph_R         */		      11,	/* Comment [ Slip Gain, Lowmed Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_40_60kph_R         */		      14,	/* Comment [ Slip Gain, Lowmed Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_60_100kph_R        */		      17,	/* Comment [ Slip Gain, Lowmed Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_Ab_150kph_R        */		      25,	/* Comment [ Slip Gain, Lowmed Mu Detect, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMMu_B_10kph_R          */		       4,	/* Comment [ Slip Gain, Lowmed Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_100_150kph_F       */		      30,	/* Comment [ Decel Gain, Low Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_10_20kph_F         */		      24,	/* Comment [ Decel Gain, Low Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_20_40kph_F         */		      26,	/* Comment [ Decel Gain, Low Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_40_60kph_F         */		      30,	/* Comment [ Decel Gain, Low Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_60_100kph_F        */		      28,	/* Comment [ Decel Gain, Low Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_Ab_150kph_F        */		      35,	/* Comment [ Decel Gain, Low Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_B_10kph_F          */		      18,	/* Comment [ Decel Gain, Low Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_100_150kph_F        */		      25,	/* Comment [ Slip Gain, Low Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_10_20kph_F          */		      14,	/* Comment [ Slip Gain, Low Mu Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_20_40kph_F          */		      16,	/* Comment [ Slip Gain, Low Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_40_60kph_F          */		      20,	/* Comment [ Slip Gain, Low Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_60_100kph_F         */		      24,	/* Comment [ Slip Gain, Low Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_Ab_150kph_F         */		      25,	/* Comment [ Slip Gain, Low Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_B_10kph_F           */		      12,	/* Comment [ Slip Gain, Low Mu Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_100_150kph_R       */		      38,	/* Comment [ Decel Gain, LowMu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_10_20kph_R         */		      25,	/* Comment [ Decel Gain, Low Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_20_40kph_R         */		      25,	/* Comment [ Decel Gain, Low Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_40_60kph_R         */		      30,	/* Comment [ Decel Gain, Low Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_60_100kph_R        */		      26,	/* Comment [ Decel Gain, Low Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_Ab_150kph_R        */		      40,	/* Comment [ Decel Gain, Low Mu Detect, Above 150kph, Decel State, RearWheels ] */
	/* int16_t     	S16_Decel_Gain_LMu_B_10kph_R          */		      15,	/* Comment [ Decel Gain, Low Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_100_150kph_R        */		      25,	/* Comment [ Slip Gain, Low Mu Detect, 100-150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_10_20kph_R          */		      10,	/* Comment [ Slip Gain, Low Mu Detect, 10-20kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_20_40kph_R          */		      12,	/* Comment [ Slip Gain, Low Mu Detect, 20-40kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_40_60kph_R          */		      20,	/* Comment [ Slip Gain, Low Mu Detect, 40-60kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_60_100kph_R         */		      20,	/* Comment [ Slip Gain, Low Mu Detect, 60-100kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_Ab_150kph_R         */		      30,	/* Comment [ Slip Gain, Low Mu Detect, Above 150kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Slip_Gain_LMu_B_10kph_R           */		       8,	/* Comment [ Slip Gain, Low Mu Detect, Below 10kph, Decel State, Rear Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_100_150_F        */		      22,	/* Comment [ Decel Gain, Split High Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_10_20kph_F       */		      14,	/* Comment [ Decel Gain, Split Mu High side Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_20_40kph_F       */		      15,	/* Comment [ Decel Gain, Split High Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_40_60kph_F       */		      18,	/* Comment [ Decel Gain, Split High Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_60_100kph_F      */		      20,	/* Comment [ Decel Gain, Split High Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_Ab_150kph_F      */		      25,	/* Comment [ Decel Gain, Split High Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Decel_Gain_SpHMu_B_10kph_F        */		      24,	/* Comment [ Decel Gain, Split Mu High side Detect, Below 10kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_100_150_F         */		      18,	/* Comment [ Slip Gain, Split High Mu Detect, 100-150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_10_20kph_F        */		       5,	/* Comment [ Slip Gain, Split Mu High side Detect, 10-20kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_20_40kph_F        */		       8,	/* Comment [ Slip Gain, Split High Mu Detect, 20-40kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_40_60kph_F        */		       8,	/* Comment [ Slip Gain, Split High Mu Detect, 40-60kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_60_100kph_F       */		      10,	/* Comment [ Slip Gain, Split High Mu Detect, 60-100kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_Ab_150kph_F       */		      20,	/* Comment [ Slip Gain, Split High Mu Detect, Above 150kph, Decel State, Front Wheels ] */
	/* int16_t     	S16_Slip_Gain_SpHMu_B_10kph_F         */		       3,	/* Comment [ Slip Gain, Split Mu High side Detect, Below 10kph, Decel State, Front Wheels ] */
	/* char8_t     	S8_UNSTABLE_RISE_REF_ARAD_1st_F       */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise before AFZOK, Front Wheels ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_UNSTABLE_RISE_REF_ARAD_1st_R       */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise before AFZOK, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_UNSTABLE_RISE_REF_DUMP_1st_F       */		       2,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise before AFZOK, Front Wheels ] */
	/* uchar8_t    	U8_UNSTABLE_RISE_REF_DUMP_1st_R       */		       5,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise before AFZOK, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_Ab_HMu_F               */		      16,	/* Comment [ Arad Ref. for starting Unstable Rise above High-mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_Ab_HMu_F           */		       1,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise above High-mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_Ab_HMu_R               */		       8,	/* Comment [ Arad Ref. for starting Unstable Rise above High-mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_Ab_HMu_R           */		       1,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise above High-mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_MHMu_HMu_F             */		      20,	/* Comment [ Arad Ref. for starting Unstable Rise, From Med-High to High-mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_MHMu_HMu_F         */		       2,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Med-High to High-mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_MHMu_HMu_R             */		      22,	/* Comment [ Arad Ref. for starting Unstable Rise, From Med-High to High-mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[      5.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_MHMu_HMu_R         */		       2,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Med-High to High-mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_MMu_MHMu_F             */		      12,	/* Comment [ Arad Ref. for starting Unstable Rise, From Medium to Med-High mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        3 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_MMu_MHMu_F         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Medium to Med-High mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_MMu_MHMu_R             */		      12,	/* Comment [ Arad Ref. for starting Unstable Rise, From Medium to Med-High mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        3 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_MMu_MHMu_R         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Medium to Med-High mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_LMMu_MMu_F             */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise, From Low-Medium to Medium mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_LMMu_MMu_F         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Low-Medium to Medium mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_LMMu_MMu_R             */		      20,	/* Comment [ Arad Ref. for starting Unstable Rise, From Low-Medium to Medium mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_LMMu_MMu_R         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Low-Medium to Medium mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_LMu_LMMu_F             */		      28,	/* Comment [ Arad Ref. for starting Unstable Rise, From Low to Low-Medium mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        7 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_LMu_LMMu_F         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Low to Low-Medium mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_LMu_LMMu_R             */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise, From Low to Low-Medium mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_LMu_LMMu_R         */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise, From Low to Low-Medium mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_Bw_LMu_F               */		      28,	/* Comment [ Arad Ref. for starting Unstable Rise below Low mu, Front Wheels ] */
	                                                        		        	/* ScaleVal[        7 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_Bw_LMu_F           */		       4,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise below Low mu, Front Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_Bw_LMu_R               */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise below Low mu, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_Bw_LMu_R           */		       4,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise below Low mu, Rear Wheels ] */
	/* char8_t     	S8_dP_Arad_Ref_SpHMu_F                */		      24,	/* Comment [ Arad Ref. for starting Unstable Rise at Split mu, High-side Front Wheel ] */
	                                                        		        	/* ScaleVal[        6 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_dP_Dump_cnt_Ref_SpHMu_F            */		       3,	/* Comment [ Number of Dump scans Ref. for starting Unstable Rise at Split mu, High-side Front Wheel ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_1_F           */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_2_F           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_3_F           */		       4,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_Max_F         */		       4,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_1_F          */		       1,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_2_F          */		       3,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_3_F          */		       5,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, above High-mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_1_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_2_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_3_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Ab_HMu_Max_R         */		       4,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_1_R          */		       1,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_2_R          */		       3,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Ab_HMu_3_R          */		       5,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, above High-mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_1_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_2_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_3_F         */		       4,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_Max_F       */		       4,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_1_F        */		       2,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_2_F        */		       4,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_3_F        */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_1_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_2_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_3_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MHMu_HMu_Max_R       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_1_R        */		       2,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_2_R        */		       4,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MHMu_HMu_3_R        */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Med-High to High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_1_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_2_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_3_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_Max_F       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_1_F        */		       2,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_2_F        */		       3,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_3_F        */		       5,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_1_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_2_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_3_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_MMu_MHMu_Max_R       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_1_R        */		       3,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_2_R        */		       5,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_MMu_MHMu_3_R        */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Medium to Med-High mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_1_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_2_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_3_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_Max_F       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_1_F        */		       2,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_2_F        */		       4,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_3_F        */		       5,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_1_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_2_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_3_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMMu_MMu_Max_R       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_1_R        */		       2,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_2_R        */		       3,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMMu_MMu_3_R        */		       5,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Low-Med to Medium mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_1_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_2_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_3_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_Max_F       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_1_F        */		       3,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_2_F        */		       5,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_3_F        */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_1_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_2_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_3_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_LMu_LMMu_Max_R       */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_1_R        */		       3,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_2_R        */		       4,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_LMu_LMMu_3_R        */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Low to Low-Med mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_1_F           */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_2_F           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_3_F           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_Max_F         */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_1_F          */		       3,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_2_F          */		       5,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_3_F          */		       7,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_1_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_2_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_3_R           */		       2,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_Bw_LMu_Max_R         */		       2,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_1_R          */		       3,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_2_R          */		       5,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_Bw_LMu_3_R          */		       6,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Below Low mu, Rear Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_SPHMu_1_F            */		       2,	/* Comment [ dP rise time if num. of dump scans is smaller than 1st reference point, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_SPHMu_2_F            */		       2,	/* Comment [ dP rise time if num. of dump scans is between 1st and 2nd reference point, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_SPHMu_3_F            */		       4,	/* Comment [ dP rise time if num. of dump scans is between 2nd and 3rd reference point, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_by_Dump_SPHMu_Max_F          */		       4,	/* Comment [ dP rise time if num. of dump scans is larger than 3rd reference point, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_SPHMu_1_F           */		       1,	/* Comment [ 1st reference point to set the dP rise time depending on the number of dump scans, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_SPHMu_2_F           */		       2,	/* Comment [ 2nd reference point to set the dP rise time depending on the number of dump scans, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_dPTim_Dump_Ref_SPHMu_3_F           */		       4,	/* Comment [ 3rd reference point to set the dP rise time depending on the number of dump scans, Split mu, High-side Front Wheels ] */
	/* uchar8_t    	U8_MAXIMUM_dP_RISESCAN_F              */		       4,	/* Comment [ Maximum unstable-rise scans allowed, Front Wheels ] */
	/* uchar8_t    	U8_MAXIMUM_dP_RISESCAN_R              */		       4,	/* Comment [ Maximum unstable-rise scans allowed, Rear Wheels ] */
	/* uchar8_t    	U8_MINIMUM_dP_RISESCAN_F              */		       2,	/* Comment [ Minimum rise-scans required for Unstable-rise mode, Front Wheels ] */
	/* uchar8_t    	U8_MINIMUM_dP_RISESCAN_R              */		       2,	/* Comment [ Minimum rise-scans required for Unstable-rise mode, Rear Wheels ] */
	/* uchar8_t    	U8_MIN_dP_RISE_Bw_15kph_F             */		       2,	/* Comment [ Minimum rise-scans required for Unstable-rise at Low Speed (Below 15kph), Front Wheels ] */
	/* uchar8_t    	U8_MIN_dP_RISE_Bw_15kph_R             */		       2,	/* Comment [ Minimum rise-scans required for Unstable-rise at Low Speed (Below 15kph), Rear Wheels ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_HHDF_F        */		       9,	/* Comment [ Time of 1st dump-scan for High High_dump_factor in High mu(Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_LHDF_F        */		       7,	/* Comment [ Time of 1st dump-scan for Low High_dump_factor in High mu(Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_LMHDF_F       */		       9,	/* Comment [ Time of 1st dump-scan for LowMed High_dump_factor in High mu(Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_MHDF_F        */		       9,	/* Comment [ Time of 1st dump-scan for Medium High_dump_factor in High mu(Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumptime_HMu_B_20_F            */		      10,	/* Comment [ Dump time in ms of 1st dump-scan in High mu, below 20kph, Front Wheels ] */
	/* uchar8_t    	U8_1st_Dumptime_HMu_LHDF_B_20_F       */		      10,	/* Comment [ Dump time in ms of 1st dump-scan for Low High_dump_factor in High mu, below 20kph, Front Wheels, ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_HHDF_R        */		       8,	/* Comment [ Time of 1st dump-scan for High High_dump_factor in High mu(Rear Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_LHDF_R        */		       5,	/* Comment [ Time of 1st dump-scan for Low High_dump_factor in High mu(Rear Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_HMu_MHDF_R        */		       7,	/* Comment [ Time of 1st dump-scan for Medium High_dump_factor in High mu(Rear Wheels) ] */
	/* uchar8_t    	U8_1st_Dumptime_HMu_B_20_R            */		       6,	/* Comment [ Dump time in ms of 1st dump-scan in High mu, below 20kph, Rear Wheels ] */
	/* uchar8_t    	U8_1st_Dumptime_HMu_LHDF_B_20_R       */		       5,	/* Comment [ Dump time in ms of 1st dump-scan for Low High_dump_factor in High mu, below 20kph, Rear Wheels ] */
	/* uchar8_t    	U8_1st_Dumpscantime_MMu_F             */		      10,	/* Comment [ Time of 1st dump-scan in Low-med~medium mu(Front Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_MMu_F             */		       7,	/* Comment [ Time of nth dump-scan in Low-med~medium mu(Front Wheels) ] */
	/* uchar8_t    	U8_B_20KPH_Dumpscantime_MMu_F         */		      10,	/* Comment [ Time of dump-scan in Medium mu below 20kph (Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_MMu_R             */		       9,	/* Comment [ Time of 1st dump-scan in Low-med~medium mu(Rear Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_MMu_R             */		       7,	/* Comment [ Time of nth dump-scan in Low-med~medium mu(Rear Wheels) ] */
	/* uchar8_t    	U8_B_20KPH_Dumpscantime_MMu_R         */		      10,	/* Comment [ Time of dump-scan in Medium mu below 20kph (Rear Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_LMMu_F            */		      10,	/* Comment [ Time of 1st dump-scan in Low~Low-medium mu(Front Wheels) ] */
	/* uchar8_t    	U8_2nd3rd_Dumpscantime_LMMu_F         */		       9,	/* Comment [ Time of 2nd and 3rd dump-scan in Low~Under-Low-Medium mu(Front Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_LMMu_F            */		       8,	/* Comment [ Time of nth dump-scan in Low-med mu(Front Wheels) ] */
	/* uchar8_t    	U8_B_20KPH_Dumpscantime_LMMu_F        */		       7,	/* Comment [ Time of dump-scan in Low-med mu below 20kph (Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_LMMu_R            */		       8,	/* Comment [ Time of 1st dump-scan in Low~Low-medium mu(Rear Wheels) ] */
	/* uchar8_t    	U8_2nd3rd_Dumpscantime_LMMu_R         */		       8,	/* Comment [ Time of 2nd and 3rd dump-scan in Low~Under-Low-Medium mu(Rear Wheels) ] */
	/* uchar8_t    	U8_4th5th_Dumpscantime_LMMu_R         */		       7,	/* Comment [ Time of 4th and 5th dump-scan in Low~Under-Low-Medium mu(Rear Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_LMMu_R            */		       7,	/* Comment [ Time of nth dump-scan in Low-med mu(Rear Wheels) ] */
	/* uchar8_t    	U8_B_10KPH_Dumpscantime_LMMu_R        */		       7,	/* Comment [ Time of dump-scan in Low-med mu below 10kph (Rear Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_LMu_F             */		      12,	/* Comment [ Time of 1st dump-scan in Low mu(Front Wheels) ] */
	/* uchar8_t    	U8_2nd3rd_Dumpscantime_LMu_F          */		      10,	/* Comment [ Time of 2nd and 3rd dump-scan in Low mu(Front Wheels) ] */
	/* uchar8_t    	U8_4th5th_Dumpscantime_LMu_F          */		      10,	/* Comment [ Time of 4th and 5th dump-scan in Low mu(Front Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_LMu_F             */		       8,	/* Comment [ Time of nth dump-scan in Low mu(Front Wheels) ] */
	/* uchar8_t    	U8_UnstableTim_for_IncDumptime        */		      20,	/* Comment [ For Low-mu surfaces. If unstable time of the last cycle is bigger than this parameter, dump scantime is increased. ] */
	/* uchar8_t    	U8_B_10KPH_Dumpscantime_LMu_F         */		       7,	/* Comment [ Time of dump-scan in Low mu below 10kph (Front Wheels) ] */
	/* uchar8_t    	U8_1st_Dumpscantime_LMu_R             */		      12,	/* Comment [ Time of 1st dump-scan in Low mu(Rear Wheels) ] */
	/* uchar8_t    	U8_2nd3rd_Dumpscantime_LMu_R          */		      10,	/* Comment [ Time of 2nd and 3rd dump-scan in Low mu(Rear Wheels) ] */
	/* uchar8_t    	U8_4th5th_Dumpscantime_LMu_R          */		       8,	/* Comment [ Time of 4th and 5th dump-scan in Low mu(Rear Wheels) ] */
	/* uchar8_t    	U8_nth_Dumpscantime_LMu_R             */		       7,	/* Comment [ Time of nth dump-scan in Low mu(Rear Wheels) ] */
	/* uchar8_t    	U8_B_10KPH_Dumpscantime_LMu_R         */		       7,	/* Comment [ Time of dump-scan in Low mu below 10kph (Rear Wheels) ] */
	/* int16_t     	S16_WPThres_for_IncDumptim_1st        */		     100,	/* Comment [ Wheel Skid Pressure in Low-mu. Reference to increase dump time in Low-mu surface ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_ABS_Motor_BEMF_ADD_HIGH_MU        */		       0,	/* Comment [ Additional ABS Motor BEMF Voltage in ABS system @ U8_PULSE_HIGH_MU ~, default 0,min 0V" ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* int16_t     	S16_ABS_Motor_BEMF_ADD_MED_MU         */		       0,	/* Comment [ Additional ABS Motor BEMF Voltage in ABS system @ U8_PULSE_MED_HIGH_MU ~ ] */
	                                                        		        	/* ScaleVal[        0 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* int16_t     	S16_ABS_Motor_BEMF_Voltage_Ref        */		    3000,	/* Comment [ ABS Motor Speed Control BEMF Level of ABS system ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* int16_t     	S16_ESP_Motor_BEMF_Voltage_Ref        */		    3000,	/* Comment [ ABS Motor Speed Control BEMF Level of ESP system ] */
	                                                        		        	/* ScaleVal[        3 V ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 14 ] */
	/* uchar8_t    	U8_Add_ABS_Mtr_Off_Duty_in_ABS        */		      40,	/* Comment [ Motor on-time ratio for 130ms to turn off motor after additional motor on, default 40(ABS solo) ] */
	/* uchar8_t    	U8_Add_ABS_Mtr_On_Duty_in_ABS         */		      70,	/* Comment [ Motor on-time ratio for 130ms to turn on motor after ABS control, default 70(ABS solo) ] */
	/* uchar8_t    	U8_MSC_Activate_Indicator             */		      70,	/* Comment [ Continuous Dump Scans for Starting Point of Motor Speed Control ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_MSC_Activate_Indicator_ABS2        */		      70,	/* Comment [ Continuous Dump Scans for Starting Point of Motor Speed Control (ESC Only) ] */
	                                                        		        	/* ScaleVal[          7 ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_MSC_init_fully_driving_ABS2        */		      16,	/* Comment [ Initial fully driving scan time before Motor Speed Control operate (Default=10) (ESC Only) ] */
	/* uchar8_t    	U8_MSC_init_fully_driving_scan        */		      16,	/* Comment [ Initial fully driving scan time before Motor Speed Control operate (Default=10) ] */
	/* uchar8_t    	U8_MSC_Limit_Dump_Scans_F             */		      25,	/* Comment [ Maximum Dump Limit for Motor Speed Control Indicator(Front) ] */
	/* uchar8_t    	U8_MSC_Limit_Dump_Scans_R             */		      15,	/* Comment [ Maximum Dump Limit for Motor Speed Control Indicator(Rear) ] */
	/* uchar8_t    	U8_MSC_low_mu_skid_counter            */		      15,	/* Comment [ BLS set 된 후 ABS_fz set까지의 scan이 이 값보다 짧으면 low mu 판단 ] */
	/* char8_t     	S8_Counter_threshold_Lower_Lim_F      */		       4,	/* Comment [ Rough Road Detection counting threshold, if wheel speed difference is negative(-) ( Default = 0.5kph ), Front Wheels ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_Counter_threshold_Lower_Lim_R      */		       4,	/* Comment [ Rough Road Detection counting threshold, if wheel speed difference is negative(-) ( Default = 0.5kph ), Rear Wheels ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_Counter_threshold_Upper_Lim_F      */		       4,	/* Comment [ Rough Road Detection counting threshold, if wheel speed difference is positive(+) ( Default = 0.5kph ), Front Wheels ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_Counter_threshold_Upper_Lim_R      */		       4,	/* Comment [ Rough Road Detection counting threshold, if wheel speed difference is positive(+) ( Default = 0.5kph ), Rear Wheels ] */
	                                                        		        	/* ScaleVal[   0.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_Rough_road_detector_F_level0       */		       6,	/* Comment [ Starting value of transient Rough Road ( Default = 5 ) ] */
	/* char8_t     	S8_Rough_road_detector_F_level1       */		       9,	/* Comment [ Starting value of transient Rough Road ( Default = 7 ) ] */
	/* char8_t     	S8_Rough_road_detector_F_level2       */		      11,	/* Comment [ Starting value of fully Rough Road ( Default = 10 ) ] */
	/* char8_t     	S8_Rough_road_detector_R_level0       */		       6,	/* Comment [ Starting value of transient Rough Road ( Default = 5 ) ] */
	/* char8_t     	S8_Rough_road_detector_R_level1       */		       9,	/* Comment [ Starting value of transient Rough Road ( Default = 7 ) ] */
	/* char8_t     	S8_Rough_road_detector_R_level2       */		      11,	/* Comment [ Starting value of fully Rough Road ( Default = 10 ) ] */
	/* uchar8_t    	U8_ROUGH_SPEED_LEVEL1                 */		      50,	/* Comment [ Max. Speed for Using Tunned dV Threshold Value ( default 50km/h ) ] */
	/* uchar8_t    	U8_ROUGH_SPEED_LEVEL2                 */		      80,	/* Comment [ Min. Speed for Using GAIN LEVEL2 X Tunned dV Threshold Value ( default 80km/h ) ] */
	/* uchar8_t    	U8_ROUGH_THRES_GAIN_LEVEL2            */		       4,	/* Comment [ Front Interpolation Gain Used at Upper Range of Speed Level2 (default gain 4 ) ] */
	/* uchar8_t    	U8_ROUGH_THRES_GAIN_LEVEL2_R          */		       4,	/* Comment [ Rear Interpolation Gain Used at Upper Range of Speed Level2 (default gain 4 ) ] */
	/* uchar8_t    	U8_Initial_Duty_Rough_Comp_F          */		       8,	/* Comment [ Initial duty compensation to rough road ] */
	/* uchar8_t    	U8_Initial_Duty_Rough_Comp_R          */		       6,	/* Comment [ Initial duty compensation to rough road ] */
	/* uchar8_t    	U8_Init_Duty_Rough_Sus_Comp_F         */		       5,	/* Comment [ Initial duty compensation to rough road suspect ] */
	/* uchar8_t    	U8_Init_Duty_Rough_Sus_Comp_R         */		       5,	/* Comment [ Initial duty compensation to rough road suspect ] */
	/* char8_t     	S8_ABS_TEMP_1                         */		       1,	/* Comment [ Temp value for ABS ] */
	/* char8_t     	S8_ABS_TEMP_2                         */		      20,	/* Comment [ Temp value for ABS ] */
	/* char8_t     	S8_ABS_TEMP_3                         */		      15,	/* Comment [ Temp value for ABS ] */
	/* uchar8_t    	U8_HIGH_MU                            */		      65,	/* Comment [ Differentiation Point of High Mu Region ] */
	                                                        		        	/* ScaleVal[       0.65 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_LOW_MU                             */		      25,	/* Comment [ Differentiation Point of Low MU Region ] */
	                                                        		        	/* ScaleVal[       0.25 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_DV_BASE_HIGH_FOR_F_OUT_ABS         */		      56,	/* Comment [ Higher Limit of d_V(Outside ABS, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     7 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DV_BASE_HIGH_FOR_R_OUT_ABS         */		      32,	/* Comment [ Higher Limit of d_V(Outside ABS, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[     4 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DV_BASE_LOW_FOR_F_OUT_ABS          */		      40,	/* Comment [ Higher Limit of d_V(Outside ABS, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DV_BASE_LOW_FOR_GMA                */		      24,	/* Comment [ Lower Limit of d_V(Split_Mu, Front Wheels) ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DV_BASE_LOW_FOR_R_OUT_ABS          */		      20,	/* Comment [ Higher Limit of d_V(Outside ABS, Rear Wheels) ] */
	                                                        		        	/* ScaleVal[   2.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* uchar8_t    	U8_DV_BASE_LOW_F_OUT_ABS_B40KPH       */		      24,	/* Comment [ Higher Limit of d_V @ lower speed than 40kph(Outside ABS, Front Wheels), Default 2KPH, Min 2KPH, ] */
	                                                        		        	/* ScaleVal[     3 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
	/* int16_t     	S16_ABS_ALLOW_SLIP_REF_HSPD           */		    1200,	/* Comment [ vehicle reference High speed for ABS entrance wheel slip(default 100kph) ] */
	                                                        		        	/* ScaleVal[    150 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ABS_ALLOW_SLIP_REF_LSPD           */		     240,	/* Comment [ vehicle reference Low speed for ABS entrance wheel slip(default 30kph) ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_HSPD                */		       4,	/* Comment [ Max. ABS entrance wheel slip on out-ABS ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_HSPD_BEND           */		       7,	/* Comment [ Max. ABS entrance wheel slip when BEND is detected(ESC:outside wheel/ABS:all front wheels) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_HSPD_BEND_IN        */		       8,	/* Comment [ Max. ABS entrance inside wheel slip of left BEND detected(ESC only) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_HSPD_BEND_OUT       */		       8,	/* Comment [ Max. ABS entrance inside wheel slip of right BEND detected(ESC only) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_HSPD_IN             */		       5,	/* Comment [ Max. ABS entrance wheel slip on in-ABS ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_LSPD                */		       6,	/* Comment [ Min. ABS entrance wheel slip on out-ABS ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_LSPD_BEND           */		       9,	/* Comment [ Min. ABS entrance wheel slip when BEND is detected(ESC:outside wheel/ABS:all front wheels) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_LSPD_BEND_IN        */		      10,	/* Comment [ Min. ABS entrance inside wheel slip of left BEND detected(ESC only) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_LSPD_BEND_OUT       */		      10,	/* Comment [ Min. ABS entrance inside wheel slip of right BEND detected(ESC only) ] */
	/* char8_t     	S8_ABS_ALLOW_SLIP_LSPD_IN             */		       6,	/* Comment [ Min. ABS entrance wheel slip on in-ABS ] */
	/* uchar8_t    	U8_ABS_ENTRANCE_LIMIT_BY_SLIP         */		       1,	/* Comment [ Enable slip limitation when deciding ABS entrance (1: -b+dV+slip limit, 0 : -b+dV) ] */
	/* char8_t     	S8_Ref_Pk_Accel1_AFZ_Lim_F            */		      36,	/* Comment [ Reference Peak accel. Under 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	                                                        		        	/* ScaleVal[        9 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Ref_Pk_Accel1_AFZ_Lim_H_V_F        */		      28,	/* Comment [ Reference Peak accel. Above 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	                                                        		        	/* ScaleVal[        7 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Ref_Pk_Accel2_AFZ_Lim_F            */		      20,	/* Comment [ Reference Peak accel. Under 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	                                                        		        	/* ScaleVal[        5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Ref_Pk_Accel2_AFZ_Lim_H_V_F        */		      16,	/* Comment [ Reference Peak accel. Above 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uchar8_t    	U8_Ref_Dump1_for_AFZ_Lim_F            */		       5,	/* Comment [ Reference Dump scan Under 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	/* uchar8_t    	U8_Ref_Dump1_for_AFZ_Lim_H_V_F        */		       7,	/* Comment [ Reference Dump scan Above 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	/* uchar8_t    	U8_Ref_Dump2_for_AFZ_Lim_F            */		       3,	/* Comment [ Reference Dump scan Under 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	/* uchar8_t    	U8_Ref_Dump2_for_AFZ_Lim_H_V_F        */		       5,	/* Comment [ Reference Dump scan Above 120KPH for AFZ Lower Limit(-HIGH_MU) ] */
	/* char8_t     	S8_Rough_Disab_Dump_BF_AFZOK_F        */		       2,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Medium_Mu, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_BF_AFZOK_R        */		       2,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Medium_Mu, Rear Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_High_F            */		       1,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(High_Mu, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_High_R            */		       1,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Low_Mu, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Low_F             */		       3,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Low_Mu, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Low_R             */		       3,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Low_Mu, Rear Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Med_F             */		       2,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Medium_Mu, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Med_R             */		       2,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(High_Mu, Rear Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Ref_F             */		       5,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Rough Road, Front Wheels) ] */
	/* char8_t     	S8_Rough_Disab_Dump_Ref_R             */		       5,	/* Comment [ Fast Rise Rate 보상 제한 Dump량(Rough Road, Rear Wheels) ] */
	/* char8_t     	S8_LFC_HIGH_PEAK_ACCEL                */		      20,	/* Comment [ 일반적인 High_Mu라 판단되는 Peak Accel 기준 ] */
	                                                        		        	/* ScaleVal[        5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_LFC_MED_PEAK_ACCEL                 */		      16,	/* Comment [ 일반적인 Medium_Mu라 판단되는 Peak Accel 기준 ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* uint16_t    	U16_LFC_H2L_UNSTABIL_TIME_H_V         */		     210,	/* Comment [ 고속 High-to-Low Mu_Jump 판단의 기준이 되는 Unstable 영역 지속 시간의 기준 ] */
	/* uchar8_t    	U8_LFC_H2L_DUMP_H_V                   */		      10,	/* Comment [ 고속 High-to-Low Mu_Jump 판단의 기준이 되는 dump 수 ] */
	/* uchar8_t    	U8_LFC_HIGH_DUMP                      */		       3,	/* Comment [ 일반적인 Medium_Mu라 판단되는 dump 기준 ] */
	/* uchar8_t    	U8_LFC_HIGH_UNSTABIL_TIME             */		      20,	/* Comment [ 일반적인 High_Mu에서의 Unstable 영역 지속 시간의 기준 ] */
	/* uchar8_t    	U8_LFC_MED_DUMP                       */		       5,	/* Comment [ 일반적인 Medium_Mu라 판단되는 dump 기준 ] */
	/* uchar8_t    	U8_LFC_MED_UNSTABIL_TIME              */		      20,	/* Comment [ 일반적인 Medium_Mu에서의 Unstable 영역 지속 시간의 기준 ] */
	/* uchar8_t    	U8_LFC_L2H_Suspect_Comp_R             */		       5,	/* Comment [ Compensation Duty for Low-to-High suspect (Rear Wheels), default 0 ] */
	/* uchar8_t    	U8_BIG_DUMP_BY_LOWMU_FAST_RISE        */		      20,	/* Comment [ Pedal Effort 변화 등에 따른 Deep slip cycle 에 의한 big dump 갯수(Ice/Ceramic 등 저마찰 노면) default 16,min 10 ] */
	/* uchar8_t    	U8_BIG_DUMP_GAUGE_AT_LEVEL1           */		       9,	/* Comment [ Pedal Effort 변화 등에 따른 Deep slip cycle 감지 위한 dump level1에 의한 counter 증가량, default 9,min 0, max 25 ] */
	/* uchar8_t    	U8_BIG_DUMP_GAUGE_AT_LEVEL2           */		      10,	/* Comment [ Pedal Effort 변화 등에 따른 Deep slip cycle 감지 위한 dump level2에 의한 counter 증가량, default 10, min 0, max 25 ] */
	/* uchar8_t    	U8_BIG_DUMP_GAUGE_AT_LEVEL3           */		      15,	/* Comment [ Pedal Effort 변화 등에 따른 Deep slip cycle 감지 위한 dump level3에 의한 counter 증가량, default 15, min 0, max 25 ] */
	/* uchar8_t    	U8_BIG_RISE_DMP_COMP_DUTY             */		      20,	/* Comment [ Compensation Duty by Pedal effort change detection ] */
	/* uchar8_t    	U8_BRD_DETECTION_REF                  */		      20,	/* Comment [ 최종 Pedal Effort 변화 등에 따른 Deep slip cycle 감지를 위한 dump gauge 총합 threshold (default : 30) ] */
	/* uchar8_t    	U8_BRD_DUMP_CNT_LEVEL1                */		       9,	/* Comment [ Counter 증가량의 기준이 되는 dump scan수 Lv1(default : 9) ] */
	/* uchar8_t    	U8_BRD_DUMP_CNT_LEVEL2                */		      11,	/* Comment [ Counter 증가량의 기준이 되는 dump scan수 Lv2(default : 11) ] */
	/* uchar8_t    	U8_BRD_DUMP_CNT_LEVEL3                */		      12,	/* Comment [ Counter 증가량의 기준이 되는 dump scan수 Lv3(default : 12) ] */
	/* uchar8_t    	U8_Chess_Dump_Rise_Ref                */		      20,	/* Comment [ Chess detection reference by using Dump_Rise_Factor ] */
	/* uchar8_t    	U8_Low_Mu_Dump_Rise_Factor            */		      20,	/* Comment [ Not check H-to-split condition at low-mu road ] */
	/* int16_t     	S16_H2SP_DETECT_FRONT_VRAD_DIFF1      */		     152,	/* Comment [ Front 두바퀴의 Dump&Rise 경향이 H2SP에 해당될 때, H2SP 감지를 위한 Front 두바퀴의 속도차 기준                    , default 25 KPH,min 20KPH ] */
	                                                        		        	/* ScaleVal[     19 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_H2SP_DETECT_FRONT_VRAD_DIFF2      */		     280,	/* Comment [ Front 두바퀴의 Dump&Rise 경향이 H2SP 감지 조건을 불만족할 때, H2SP 감지를 위한 Front 두바퀴의 속도차 기준        , default 35 KPH,min 30KPH ] */
	                                                        		        	/* ScaleVal[     35 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* char8_t     	S8_H2SP_DETECT_FRONT_DECEL1           */		     -40,	/* Comment [ Front 두바퀴의 Dump&Rise 경향이 H2SP에 해당될 때, H2SP 감지를 위한 Low side Front 바퀴의 감속도 기준             , default -20g , max -15g ] */
	                                                        		        	/* ScaleVal[      -10 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_H2SP_DETECT_FRONT_DECEL2           */		     -80,	/* Comment [ Front 두바퀴의 Dump&Rise 경향이 H2SP 감지 조건을 불만족할 때, H2SP 감지를 위한 Low side Front 바퀴의 감속도 기준 , default -20g , max -20g ] */
	                                                        		        	/* ScaleVal[      -20 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_H_to_Split_Detection_AFZ           */		      35,	/* Comment [ Afz boundary to check H-to-split ( Default = 0.35g) ] */
	                                                        		        	/* ScaleVal[     0.35 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -1.28 <= X <= 1.27 ] */
	/* char8_t     	S8_H_to_Split_Dump_Diff               */		      10,	/* Comment [ Dump_rise_factor difference of front wheels ( Default = 1 ) ] */
	/* uchar8_t    	U8_H2SP_DET_LOW_MU                    */		      20,	/* Comment [ Low-mu Detection Road Mu for Resetting High to Split Control (default 0.25g ) ] */
	                                                        		        	/* ScaleVal[      0.2 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2.55 ] */
	/* uchar8_t    	U8_H2SPLIT_SLIP_DIFF                  */		      15,	/* Comment [ Min. Front Wheel Slip Difference for Detecting High to Split Mu by Using Speed Difference (default 15%) ] */
	/* uchar8_t    	U8_H2SPLIT_VDIFF_F1                   */		      14,	/* Comment [  Front Speed Difference Level1 Value for Detecting H2Split by Using Front Vrad Diff. VDIFF_F1 & Rear Vrad Diff. VDIFF_R1 ] */
	/* uchar8_t    	U8_H2SPLIT_VDIFF_F2                   */		       8,	/* Comment [ Front Speed Difference Level2 Value for Detecting H2Split by Using Front Vrad Diff. VDIFF_F2 & Rear Vrad Diff. VDIFF_R2 ] */
	/* uchar8_t    	U8_H2SPLIT_VDIFF_R1                   */		       4,	/* Comment [ Rear Speed Difference Level1 Value for Detecting H2Split by Using Front Vrad Diff. VDIFF_F1 & Rear Vrad Diff. VDIFF_R1 ] */
	/* uchar8_t    	U8_H2SPLIT_VDIFF_R2                   */		      12,	/* Comment [ Rear Speed Difference Level2 Value for Detecting H2Split by Using Front Vrad Diff. VDIFF_F2 & Rear Vrad Diff. VDIFF_R2 ] */
	/* char8_t     	S8_L_to_Split_Duty_Diff1              */		       5,	/* Comment [ Difference between current-duty and end-duty ( Default = 10 ) ] */
	/* char8_t     	S8_L_to_Split_Duty_Diff2              */		       5,	/* Comment [ Current-duty difference of front wheels at 1st cycle ( Default = 10 ) ] */
	/* uchar8_t    	U8_F_High_side_SL_dump                */		       5,	/* Comment [ High side forced dump scans on H-to-split, 35-70KPH ( Default = 6 ) ] */
	/* uchar8_t    	U8_F_High_side_SL_dump_Ab70KPH        */		       8,	/* Comment [ High side forced dump scans on H-to-split, Above 70KPH  ( Default = 11 ) ] */
	/* uchar8_t    	U8_F_High_side_SL_dump_Bw35KPH        */		       3,	/* Comment [ High side forced dump scans on H-to-split, Below 35KPH ( Default = 4 ) ] */
	/* int16_t     	S16_Additional_HDF_ref_F              */		     100,	/* Comment [ Additional HDF reference, for the first 2 and 3 dump scans, Front wheel ] */
	/* int16_t     	S16_Additional_HDF_ref_R              */		     500,	/* Comment [ Additional HDF reference, for the first 2 and 3 dump scans, Rear wheels ] */
	/* int16_t     	S16_HDF_REF_LMu_40_60kph_F            */		    3800,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_LMu_Ab_60kph_F            */		    3500,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_LMu_B_40kph_F             */		    3200,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_LMu_40_60kph_R            */		    3800,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_LMu_Ab_60kph_R            */		    3500,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_LMu_B_40kph_R             */		    3200,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_40_60kph_F            */		    3200,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_Ab_60kph_F            */		    3500,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_B_40kph_F             */		    3000,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_40_60kph_R            */		    3200,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_Ab_60kph_R            */		    3500,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_HDF_REF_MMu_B_40kph_R             */		    3000,	/* Comment [ HDF reference, condition for special 1st cycle dump ] */
	/* int16_t     	S16_High_dump_factor_Lower_Limit      */		    1000,	/* Comment [ Lower Limit of High Dump Factor ] */
	/* int16_t     	S16_High_dump_factor_Numerator        */		     100,	/* Comment [ Numerator of High Dump Factor ] */
	/* int16_t     	S16_High_dump_factor_Upper_Limit      */		    3000,	/* Comment [ Upper Limit of High Dump Factor ] */
	/* uchar8_t    	U8_Max_hold_scan_ref                  */		      30,	/* Comment [ Maximum Allowable Hold Scans ] */
	/* uchar8_t    	U8_1ST_CYCLE_UR_RISE_SCAN_MAX         */		       4,	/* Comment [ Max dP Rise time @ 1st cycle, Default 6, Min 2, Max 10 ] */
	/* uchar8_t    	U8_1ST_CYCLE_UR_RISE_SCAN_MIN         */		       1,	/* Comment [ Max dP Rise time @ 1st cycle, Default 2, Min 2, Max 6 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_HMu_F        */		     100,	/* Comment [ 1st cycle dP Rise time @ High-mu condition, Ratio to Dump scans, Front Wheels, default 50%, min 10, max 100 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_HMu_R        */		      30,	/* Comment [ 1st cycle dP Rise time @ High-mu condition, Ratio to Dump scans, Rear Wheels, default 50%, min 10, max 100 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_LMu_F        */		       5,	/* Comment [ 1st cycle dP Rise time @ Low-mu condition, Ratio to Dump scans, Front Wheels, default 20%, min 10, max 100 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_LMu_R        */		      20,	/* Comment [ 1st cycle dP Rise time @ Low-mu condition, Ratio to Dump scans, Rear Wheels, default 20%, min 10, max 100 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_MMu_F        */		      20,	/* Comment [ 1st cycle dP Rise time @ Med-mu condition, Ratio to Dump scans, Front Wheels, default 40%, min 10, max 100 ] */
	/* uchar8_t    	U8_1stDPRisePerDumpRatio_MMu_R        */		      30,	/* Comment [ 1st cycle dP Rise time @ Med-mu condition, Ratio to Dump scans, Rear Wheels, default 40%, min 10, max 100 ] */
	/* uchar8_t    	U8_dP_HMu_Dct_AradRef_1st_F           */		       4,	/* Comment [ Arad Reference to detect High Mu for Unstable-rise compensation at 1st cycle, Front Wheels ] */
	                                                        		        	/* ScaleVal[        1 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	U8_dP_HMu_Dct_DumpRef_1st_F           */		       2,	/* Comment [ Dump Reference to detect High Mu for Unstable-rise compensation at 1st cycle, Front Wheels ] */
	/* uchar8_t    	U8_dP_LMu_Dct_AradRef_1st_F           */		     100,	/* Comment [ Arad Reference to detect Low Mu for Unstable-rise compensation at 1st cycle, Front Wheels ] */
	                                                        		        	/* ScaleVal[       25 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	U8_dP_LMu_Dct_DumpRef_1st_F           */		      15,	/* Comment [ Dump Reference to detect Low Mu for Unstable-rise compensation at 1st cycle, Front Wheels ] */
	/* uchar8_t    	U8_dP_HMu_Dct_AradRef_1st_R           */		      16,	/* Comment [ Arad Reference to detect High Mu for Unstable-rise compensation at 1st cycle, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	U8_dP_HMu_Dct_DumpRef_1st_R           */		       3,	/* Comment [ Dump Reference to detect High Mu for Unstable-rise compensation at 1st cycle, Rear Wheels ] */
	/* uchar8_t    	U8_dP_LMu_Dct_AradRef_1st_R           */		      28,	/* Comment [ Arad Reference to detect Low Mu for Unstable-rise compensation at 1st cycle, Rear Wheels ] */
	                                                        		        	/* ScaleVal[        7 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	U8_dP_LMu_Dct_DumpRef_1st_R           */		      15,	/* Comment [ Dump Reference to detect Low Mu for Unstable-rise compensation at 1st cycle, Rear Wheels ] */
	/* uchar8_t    	U8_DEFAULT_DUMPSCANTIME               */		       8,	/* Comment [ Default Dump scantime(min: 10) ] */
	/* uchar8_t    	U8_STABDUMP_SCANTIME_F                */		       7,	/* Comment [ Dump scantime for stable dump by Yaw stability control (ESC only) ] */
	/* uchar8_t    	U8_SP_LOW_FRT_DUMPTIM_AFZ             */		      15,	/* Comment [ Split Low-side front wheel dump scantime의 기준 mu, default 0.3g, min 0.15g ] */
	                                                        		        	/* ScaleVal[     0.15 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1 ] */
	/* uchar8_t    	U8_SP_LOW_RER_DUMPTIM_AFZ             */		      30,	/* Comment [ Split Low-side rear wheel dump scantime의 기준 mu, default 0.3g, min 0.15g ] */
	                                                        		        	/* ScaleVal[      0.3 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 1 ] */
	/* int16_t     	S16_GMA_DCT_SP_DIFF_0_50_BABS         */		      96,	/* Comment [ ABS 진입 이전 GMA 감지 기준 Front 속도 차 @ 0~50 KPH, default 12 KPH,min 10KPH ] */
	                                                        		        	/* ScaleVal[    12 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_GMA_DCT_SP_DIFF_50_80_BABS        */		     120,	/* Comment [ ABS 진입 이전, GMA 감지 기준 Front 속도 차 @ 50~80 KPH, default 20 KPH,min 10KPH ] */
	                                                        		        	/* ScaleVal[    15 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_GMA_DCT_SP_DIFF_80_M_BABS         */		     120,	/* Comment [ ABS 진입 이전, GMA 감지 기준 Front 속도 차 @ 80~MAX KPH, default 30 KPH,min 10KPH ] */
	                                                        		        	/* ScaleVal[    15 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_0_50_BABS        */		      30,	/* Comment [ ABS 진입 이전, GMA 감지 기준 Front slip 차 @ 0~50 KPH, default 30%,min 20% ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_50_80_BABS       */		      20,	/* Comment [ ABS 진입 이전, GMA 감지 기준 Front slip 차 @ 50~80 KPH, default  25%,min 15% ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_80_M_BABS        */		      20,	/* Comment [ ABS 진입 이전, GMA 감지 기준 Front slip 차 @ 80~MAX KPH, default 20%,min 12% ] */
	/* int16_t     	S16_GMA_DCT_SPEED_DIFF_0_50KPH        */		      80,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front 속도 차 @ 0~50 KPH, default 10 KPH,min 10KPH ] */
	                                                        		        	/* ScaleVal[     10 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_GMA_DCT_SPEED_DIFF_50_70KPH       */		      96,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front 속도 차 @ 50~70 KPH,default 12 KPH,min 12KPH ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_GMA_DCT_SPEED_DIFF_70_90KPH       */		     112,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front 속도 차 @ 70~90 KPH,default 14 KPH,min 12KPH ] */
	                                                        		        	/* ScaleVal[     14 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* int16_t     	S16_GMA_DCT_SPEED_DIFF_90_M_KPH       */		      96,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front 속도 차 @ 90~Max KPH, default 15 KPH,min 12KPH ] */
	                                                        		        	/* ScaleVal[     12 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 50 ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_0_50KPH          */		      24,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front slip 차 @ 0~50 KPH, default 24 %  ,min 24 ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_50_70KPH         */		      20,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front slip 차 @ 50~70 KPH,default 20 %  ,min 20 ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_70_90KPH         */		      17,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front slip 차 @ 70~90 KPH,default 17 %  ,min 17 ] */
	/* char8_t     	S8_GMA_DCT_SLIP_DIFF_90_M_KPH         */		      10,	/* Comment [ ABS 진입 이후, GMA 감지 기준 Front slip 차 @ 90~Max KPH, default 10 %  ,min 10 ] */
	/* int16_t     	S16_GMA_ALLOWED_LIMIT_SPEED           */		     160,	/* Comment [ GMA Lower Entrance Speed, default 20KPH ,max 40KPH ] */
	                                                        		        	/* ScaleVal[     20 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 40 ] */
	/* uchar8_t    	U8_GMA_HOLD_TIME_MAX                  */		      80,	/* Comment [ Maximum GMA hold time, default 70, min 70, max 140 ] */
	/* uchar8_t    	U8_GMA_HOLD_Comp_for_HSPEED           */		      20,	/* Comment [ Maximum GMA hold time comp For High Speed, default 28, min 14, max 56 ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_BFAFZOK_F         */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold before AFZ_OK ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_BFAFZOK_R         */		       2,	/* Comment [ Rear Wheel slip to allow stable forced hold before AFZ_OK ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_HIGH_F            */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold for High Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_HIGH_R            */		       1,	/* Comment [ Rear Wheel slip to allow stable forced hold for High Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_LOW_F             */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold for Low Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_LOW_R             */		       2,	/* Comment [ Rear Wheel slip to allow stable forced hold for Low Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_MED_F             */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold for Medium Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_MED_R             */		       2,	/* Comment [ Rear Wheel slip to allow stable forced hold for Medium Mu ] */
	/* char8_t     	S8_FORCED_HOLD_SLIP_SP_HIGH_F         */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold for Split-High Mu ] */
	/* char8_t     	S8_MAXIMUM_FORCED_HOLD_SCAN           */		       7,	/* Comment [ Max. Num of Stable Forced Hold scans ] */
	/* char8_t     	S8_FORCED_HLD_SLIP_CHMU_A80KPH_F      */		       2,	/* Comment [ Front Wheel slip to allow stable forced hold for Certain High Mu above 80kph ] */
	/* char8_t     	S8_FORCED_HLD_SLIP_CHMU_B40KPH_F      */		       4,	/* Comment [ Front Wheel slip to allow stable forced hold for Certain High Mu below 40kph ] */
	/* char8_t     	S8_FORCED_HLD_SLIP_CHMU_B80KPH_F      */		       3,	/* Comment [ Front Wheel slip to allow stable forced hold for Certain High Mu below 80kph ] */
	/* char8_t     	S8_DetectAccel_EngTorq_L1             */		      20,	/* Comment [ Engine Torque Level at MTP Position P1 to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_EngTorq_L2             */		      40,	/* Comment [ Engine Torque Level at MTP Position P2 to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_EngTorq_L3             */		      60,	/* Comment [ Engine Torque Level at MTP Position P3 to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_Throttle_P1            */		      10,	/* Comment [ MTP % to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_Throttle_P2            */		      30,	/* Comment [ MTP % to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_Throttle_P3            */		      50,	/* Comment [ MTP % to Detect Driver's Intend to Accelerate ] */
	/* char8_t     	S8_DetectAccel_Reset_EngTorq          */		      10,	/* Comment [ Engine Torque Level to Detect Driver's Release Trhrottle ] */
	/* char8_t     	S8_DetectAccel_Reset_Throttle         */		       3,	/* Comment [ MPT % to Detect Driver's Release Throttle ] */
	/* int16_t     	S16_BUMP_HI_SPD                       */		     800,	/* Comment [ Max speed ref. for bump detection, default 100kph (S16_BUMP_MED_SPD+α , max 150kph) ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BUMP_LOW_SPD                      */		     320,	/* Comment [ Low speed ref. for bump detection, default 40kph (0 ~ 50kph) ] */
	                                                        		        	/* ScaleVal[     40 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_BUMP_MED_SPD                      */		     400,	/* Comment [ Medium speed ref. for bump detection, default 50kph (S16_BUMP_LOW_SPD+α , max 70kph) ] */
	                                                        		        	/* ScaleVal[     50 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* uchar8_t    	S8_BUMP_PEAK_ACC_REF_1_DUMP           */		      16,	/* Comment [ Peak Accel threshold for 1 dump scan, default 7g, min 4g, max 10g ] */
	                                                        		        	/* ScaleVal[        4 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	S8_BUMP_PEAK_ACC_REF_MAX_DUMP         */		      40,	/* Comment [ Peak Accel threshold for max dump scan, default 14g, min 10g, max 25g  ] */
	                                                        		        	/* ScaleVal[       10 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ 0 <= X <= 63.75 ] */
	/* uchar8_t    	U8_BUMP_MAX_DUMP_SCANS                */		      10,	/* Comment [ Max dump scans for ABS due to bump, default 14, min 10, max 30 ] */
	/* uchar8_t    	U8_LIMIT_CNT_FOR_BMP_DCT              */		      10,	/* Comment [ Num of unstable state scans to inhibit bump detection on Homo mu, default 14 ] */
	/* uchar8_t    	U8_LIMIT_CNT_SPLT_FOR_BMP_DCT         */		       8,	/* Comment [ Num of unstable state scans to inhibit bump detection on Split mu, default 11 ] */
	/* uchar8_t    	U8_LIMIT_DMP_CNT_FOR_BMP_DCT          */		       3,	/* Comment [ Num of dump scans to inhibit bump detection, default 4 ] */
	/* int16_t     	S16_L2H_DCT_ALLOWED_AFZ               */		      10,	/* Comment [ L2H Detect allowed Min AFZ(default : 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_L2H_DCT_DIFF_DECEL                */		       5,	/* Comment [ L2H Detect decel diff(default : 0.05g) ] */
	                                                        		        	/* ScaleVal[     0.05 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_L2H_DCT_DIFF_DECEL_H              */		      10,	/* Comment [ L2H Detect decel diff high(default : 0.1g) ] */
	                                                        		        	/* ScaleVal[      0.1 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_L2H_RESET_DIFF_DECEL              */		       2,	/* Comment [ L2H reset condition by decel diff(default : 0.03g) ] */
	                                                        		        	/* ScaleVal[     0.02 g ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* char8_t     	S8_L2H_STABLE_THR_SLIP_F              */		       2,	/* Comment [ Slip reference for stable state, front wheel(default : 2) ] */
	/* char8_t     	S8_L2H_STABLE_THR_SLIP_F_OPP          */		       3,	/* Comment [ Slip reference for stable state, front opposite wheel(default : 3) ] */
	/* char8_t     	S8_L2H_STABLE_THR_SLIP_R              */		       3,	/* Comment [ Slip reference for stable state, rear wheel(default : 3) ] */
	/* char8_t     	S8_L2H_STABLE_THR_SLIP_R_OPP          */		       3,	/* Comment [ Slip reference for stable state, rear opposite wheel(default : 3) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_F            */		       8,	/* Comment [ Slip reference for unstable state, front wheel(default : 3) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_F_1          */		       9,	/* Comment [ Slip reference for Unstable state Lv1, front wheel(default : 5) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_F_2          */		      10,	/* Comment [ Slip reference for Unstable state Lv2, front wheel(default : 10) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_F_3          */		      15,	/* Comment [ Slip reference for Unstable state Lv3, front wheel(default : 15) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_F_OPP        */		       8,	/* Comment [ Slip reference for unstable state, front opposite wheel(default : 3) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_R            */		       6,	/* Comment [ Slip reference for unstable state, rear wheel(default : 4) ] */
	/* char8_t     	S8_L2H_UNSTABLE_THR_SLIP_R_OPP        */		       7,	/* Comment [ Slip reference for unstable state, rear opposite wheel(default : 4) ] */
	/* uchar8_t    	U8_L2Hsus_SP_STB_TIM_L_DEC_SUM        */		      90,	/* Comment [ reference stable time decel sum, split at U8_L2H_DCT_DECEL_CNT_L(default : 450ms) ] */
	                                                        		        	/* ScaleVal[   0.45 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_SP_STB_TIM_S_DEC_SUM1       */		     100,	/* Comment [ reference stable time decel sum, split at S16_L2H_DCT_DIFF_DECEL_H(default : 500ms) ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_SP_STB_TIM_S_DEC_SUM2       */		     110,	/* Comment [ reference stable time decel sum, split at S16_L2H_DCT_DIFF_DECEL(default : 550ms) ] */
	                                                        		        	/* ScaleVal[   0.55 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_L_DECEL_F1          */		      60,	/* Comment [ L2H suspect stable timeat U8_L2H_DCT_DECEL_CNT_L, front wheel(default : 300ms) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_L_DECEL_MIN         */		      34,	/* Comment [ L2H detect reference time for all wheel(default : 170ms) ] */
	                                                        		        	/* ScaleVal[   0.17 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_L_DECEL_R1          */		      30,	/* Comment [ L2H suspect stable timeat U8_L2H_DCT_DECEL_CNT_L, rear wheel(default : 150ms) ] */
	                                                        		        	/* ScaleVal[   0.15 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_L_DECEL_SUM         */		     130,	/* Comment [ reference stable time decel sum, homo at U8_L2H_DCT_DECEL_CNT_L(default : 650ms) ] */
	                                                        		        	/* ScaleVal[   0.65 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_F1          */		      60,	/* Comment [ L2H suspect stable timeat S16_L2H_DCT_DIFF_DECEL_H, front wheel(default : 300ms) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_F2          */		      80,	/* Comment [ L2H suspect stable timeat S16_L2H_DCT_DIFF_DECEL, front wheel(default : 400ms) ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_MIN         */		      36,	/* Comment [ L2H detect reference time for all wheel(default : 180ms) ] */
	                                                        		        	/* ScaleVal[   0.18 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_R1          */		      40,	/* Comment [ L2H suspect stable timeat S16_L2H_DCT_DIFF_DECEL_H, rear wheel(default : 200ms) ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_R2          */		      50,	/* Comment [ L2H suspect stable timeat S16_L2H_DCT_DIFF_DECEL, rear wheel(default : 250ms) ] */
	                                                        		        	/* ScaleVal[   0.25 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_SUM1        */		     100,	/* Comment [ reference stable time decel sum, homo at S16_L2H_DCT_DIFF_DECEL_H(default : 500ms) ] */
	                                                        		        	/* ScaleVal[    0.5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2Hsus_STB_TIM_S_DECEL_SUM2        */		     160,	/* Comment [ reference stable time decel sum, homo at S16_L2H_DCT_DIFF_DECEL(default : 800ms) ] */
	                                                        		        	/* ScaleVal[    0.8 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_DCT_DECEL_CNT_L                */		      60,	/* Comment [ L2H detect reference time by arad(default : 300ms) ] */
	                                                        		        	/* ScaleVal[    0.3 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_DCT_DECEL_CNT_S                */		      50,	/* Comment [ L2H detect reference time by arad(default : 250ms) ] */
	                                                        		        	/* ScaleVal[   0.25 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_STB_TIM_L_DECEL_F              */		      70,	/* Comment [ L2H stable time front wheel at U8_L2H_DCT_DECEL_CNT_L(default : 350ms) ] */
	                                                        		        	/* ScaleVal[   0.35 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_STB_TIM_L_DECEL_R              */		      40,	/* Comment [ L2H stable time rear wheel at U8_L2H_DCT_DECEL_CNT_L(default : 200ms) ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_STB_TIM_S_DECEL_F              */		      80,	/* Comment [ L2H stable time front wheel at U8_L2H_DCT_DECEL_CNT_S(default : 400ms) ] */
	                                                        		        	/* ScaleVal[    0.4 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_STB_TIM_S_DECEL_R              */		      40,	/* Comment [ L2H stable time rear wheel at U8_L2H_DCT_DECEL_CNT_S(default : 200ms) ] */
	                                                        		        	/* ScaleVal[    0.2 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 1.275 ] */
	/* uchar8_t    	U8_L2H_DCT_WPRESS_DIFF                */		     100,	/* Comment [ Wheel est. pressure difference between FL and FR with U8_L2H_DCT_WPRESS_THRES2 condition(ESC only) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_L2H_DCT_WPRESS_THRES1              */		     100,	/* Comment [ FL and FR wheel est. pressure increased after previous skid level for L2H detection(ESC only) ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_L2H_DCT_WPRESS_THRES2              */		     120,	/* Comment [ FL and FR wheel est. pressure increased after previous skid level for L2H detection(ESC only) ] */
	                                                        		        	/* ScaleVal[     12 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_OUTABS_YMR_ALLOW_SPEED            */		     240,	/* Comment [ OutABS YMR allowed Min speed(default : 30kph) ] */
	                                                        		        	/* ScaleVal[     30 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_OUTABS_YMR_DUMP_ALLOW_SPEED       */		     320,	/* Comment [ OutABS YMR dump reference Min speed(default : 40kph) ] */
	                                                        		        	/* ScaleVal[     40 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_OUTABS_YMR_DUMP_HIGH_SPEED        */		     800,	/* Comment [ OutABS YMR dump reference High speed(default : 100kph) ] */
	                                                        		        	/* ScaleVal[    100 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_OUTABS_YMR_DUMP_MAX               */		       1,	/* Comment [ YMR dump at S16_OUTABS_YMR_DUMP_HIGH_SPEED(default : 1) ] */
	/* int16_t     	S16_OUTABS_YMR_DUMP_MIN               */		       1,	/* Comment [ YMR dump at S16_OUTABS_YMR_DUMP_ALLOW_SPEED(default : 1) ] */
	/* char8_t     	S8_L1_LOWSIDE_PEAK_DEC                */		      12,	/* Comment [ Low side peak decel Lv1(default : 3g) ] */
	                                                        		        	/* ScaleVal[        3 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_L1_SLIP_DIFF_50_80                 */		       0,	/* Comment [ Slip diff 50-80kph Lv1(default : 10) ] */
	/* char8_t     	S8_L1_SLIP_DIFF_80_M                  */		       0,	/* Comment [ Slip diff 80-Max speed Lv1(default : 5) ] */
	/* char8_t     	S8_L2_LOWSIDE_PEAK_DEC                */		      22,	/* Comment [ Low side peak decel Lv2(default : 5.5g) ] */
	                                                        		        	/* ScaleVal[      5.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_L2_SLIP_DIFF_50_80                 */		      15,	/* Comment [ Slip diff 50-80kph Lv2(default : 15) ] */
	/* char8_t     	S8_L2_SLIP_DIFF_80_M                  */		      10,	/* Comment [ Slip diff 80-Max speed Lv2(default : 10) ] */
	/* char8_t     	S8_LFC_Conven_Reapply_Time_R_EBD      */		      10,	/* Comment [ NO Valve On Time for Each On/Off Reapply for Rear EBD ] */
	/* uchar8_t    	U8_EBD_HOLD_TIMER                     */		     250,	/* Comment [ Max. EBD hold time at high vehicle decel (default : 250) ] */
	/* uchar8_t    	U8_EBD_STANDSTILL_HOLD_TIME           */		     100,	/* Comment [ max ebd hold time @ lower limit speed, default 10,min 2 sec ] */
	                                                        		        	/* ScaleVal[     10 sec ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 25.5 ] */
	/* char8_t     	S8_EBD_Cut_Arad_100_150KPH            */		      14,	/* Comment [ Arad Threshold to start EBD control, 100~150KPH ] */
	                                                        		        	/* ScaleVal[      3.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Arad_20_40KPH              */		       8,	/* Comment [ Arad Threshold to start EBD control, 20~40KPH ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Arad_40_60KPH              */		       8,	/* Comment [ Arad Threshold to start EBD control, 40~60KPH ] */
	                                                        		        	/* ScaleVal[        2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Arad_60_100KPH             */		      14,	/* Comment [ Arad Threshold to start EBD control, 60~100KPH ] */
	                                                        		        	/* ScaleVal[      3.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Arad_Ab_150KPH             */		       6,	/* Comment [ Arad Threshold to start EBD control, Above 150KPH ] */
	                                                        		        	/* ScaleVal[      1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Arad_B_20KPH               */		       6,	/* Comment [ Arad Threshold to start EBD control, Below 20KPH ] */
	                                                        		        	/* ScaleVal[      1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_100_150KPH           */		      12,	/* Comment [ Vref-Vrad threshold to start EBD control, 100~150KPH ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_20_40KPH             */		      12,	/* Comment [ Vref-Vrad threshold to start EBD control, 20~40KPH ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_40_60KPH             */		      12,	/* Comment [ Vref-Vrad threshold to start EBD control, 40~60KPH ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_60_100KPH            */		      10,	/* Comment [ Vref-Vrad threshold to start EBD control, 60~100KPH ] */
	                                                        		        	/* ScaleVal[  1.25 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_Ab_150KPH            */		      12,	/* Comment [ Vref-Vrad threshold to start EBD control, Above 150KPH ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* char8_t     	S8_EBD_Cut_Vdiff_B_20KPH              */		      12,	/* Comment [ Vref-Vrad threshold to start EBD control, Below 20KPH ] */
	                                                        		        	/* ScaleVal[   1.5 km/h ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -16 <= X <= 15.875 ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_100_150kph            */		       3,	/* Comment [ EBD Cut-in slip, 20-40kph, tuning range (Min:3%) ~(Max:20%) ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_20_40kph              */		       2,	/* Comment [ EBD Cut-in slip, 20-40kph, tuning range (Min:3%) ~(Max:20%) ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_40_60kph              */		       3,	/* Comment [ EBD Cut-in slip, 40-60kph, tunng range (Min:3%) ~(Max:20%) ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_60_100kph             */		       6,	/* Comment [ EBD Cut-in slip, 60-100kph, tuning range (Min:3%) ~(Max:20%) ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_Ab_150kph             */		       2,	/* Comment [ EBD Cut-in slip, Above 150kph, tuning range (Min:3%) ~(Max:20%) ] */
	/* uchar8_t    	U8_EBD_Cut_Slip_B_20kph               */		       7,	/* Comment [ EBD Cut-in slip, Below 20kph, tuning range (Min:3%) ~(Max:20%) ] */
	/* char8_t     	S8_Arad_1stPoint_B_20KPH              */		      -7,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, Below 20KPH ] */
	                                                        		        	/* ScaleVal[    -1.75 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_B_20KPH              */		      -8,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, Below 20KPH ] */
	                                                        		        	/* ScaleVal[       -2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_B_20KPH              */		     -10,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, Below 20KPH ] */
	                                                        		        	/* ScaleVal[     -2.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_B_20KPH             */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, Below 20KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_B_20KPH             */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, Below 20KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_B_20KPH            */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, Below 20KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_B_20KPH             */		       0,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, Below 20KPH ] */
	/* char8_t     	S8_Arad_1stPoint_20_40KPH             */		      -6,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, 20~40KPH ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_20_40KPH             */		      -7,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, 20~40KPH ] */
	                                                        		        	/* ScaleVal[    -1.75 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_20_40KPH             */		      -8,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, 20~40KPH ] */
	                                                        		        	/* ScaleVal[       -2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_20_40KPH            */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, 20~40KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_20_40KPH            */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, 20~40KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_20_40KPH           */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, 20~40KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_20_40KPH            */		       0,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, 20~40KPH ] */
	/* char8_t     	S8_Arad_1stPoint_40_60KPH             */		      -6,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, 40~60KPH ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_40_60KPH             */		      -7,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, 40~60KPH ] */
	                                                        		        	/* ScaleVal[    -1.75 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_40_60KPH             */		      -8,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, 40~60KPH ] */
	                                                        		        	/* ScaleVal[       -2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_40_60KPH            */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, 40~60KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_40_60KPH            */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, 40~60KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_40_60KPH           */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, 40~60KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_40_60KPH            */		       0,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, 40~60KPH ] */
	/* char8_t     	S8_Arad_1stPoint_60_100KPH            */		      -4,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, 60~100KPH ] */
	                                                        		        	/* ScaleVal[       -1 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_60_100KPH            */		      -6,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, 60~100KPH ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_60_100KPH            */		      -8,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, 60~100KPH ] */
	                                                        		        	/* ScaleVal[       -2 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_60_100KPH           */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, 60~100KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_60_100KPH           */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, 60~100KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_60_100KPH          */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, 60~100KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_60_100KPH           */		       0,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, 60~100KPH ] */
	/* char8_t     	S8_Arad_1stPoint_100_150KPH           */		      -2,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, 100~150KPH ] */
	                                                        		        	/* ScaleVal[     -0.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_100_150KPH           */		      -4,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, 100~150KPH ] */
	                                                        		        	/* ScaleVal[       -1 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_100_150KPH           */		      -6,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, 100~150KPH ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_100_150KPH          */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, 100~150KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_100_150KPH          */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, 100~150KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_100_150KPH         */		       4,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, 100~150KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_100_150KPH          */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, 100~150KPH ] */
	/* char8_t     	S8_Arad_1stPoint_Ab_150KPH            */		      -2,	/* Comment [ 1st reference point to set the reference EBD cut slip depending on wheel acceleration, Above 150KPH ] */
	                                                        		        	/* ScaleVal[     -0.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_2ndPoint_Ab_150KPH            */		      -4,	/* Comment [ 2nd reference point to set the reference EBD cut slip depending on wheel acceleration, Above 150KPH ] */
	                                                        		        	/* ScaleVal[       -1 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_3rdPoint_Ab_150KPH            */		      -6,	/* Comment [ 3rd reference point to set the reference EBD cut slip depending on wheel acceleration, Above ~150KPH ] */
	                                                        		        	/* ScaleVal[     -1.5 g ]	Scale [ f(x) = (X + 0) * 0.25 ]  Range   [ -32 <= X <= 31.75 ] */
	/* char8_t     	S8_Arad_Fact_1_2P_Ab_150KPH           */		       2,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_1stPoint ~ Arad_2ndPoint, Above 150KPH ] */
	/* char8_t     	S8_Arad_Fact_2_3P_Ab_150KPH           */		       3,	/* Comment [ subtracting value from initial cut slip at wheel acceleration between Arad_2ndPoint ~ Arad_3rdPoint, Above 150KPH ] */
	/* char8_t     	S8_Arad_Fact_Ab_3P_Ab_150KPH          */		       4,	/* Comment [ subtracting value from initial cut slip at wheel acceleration above Arad_3rdPoint, Above 150KPH ] */
	/* char8_t     	S8_Arad_Fact_B_1P_Ab_150KPH           */		       1,	/* Comment [ subtracting value from initial cut slip at wheel acceleration below Arad_1stPoint, Above 150KPH ] */
	/* char8_t     	S8_F_WL_Fact_ASum1G_SlipPos           */		       0,	/* Comment [ Cut slip compensation value for positive Front wheel slip, if Front arad sum > -1g ] */
	/* char8_t     	S8_F_WL_Fact_ASum3G_SlipPos           */		       0,	/* Comment [ Cut slip compensation value for positive Front wheel slip, if -1g > Front arad sum > -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASumAb3G_SlipPos         */		       1,	/* Comment [ Cut slip compensation value for positive Front wheel slip, if Front arad sum < -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASum1G_Slip0_2P          */		       1,	/* Comment [ Cut slip compensation value if : 0%>avg.Front wheel slip >-2%, AND Front arad sum > -1g ] */
	/* char8_t     	S8_F_WL_Fact_ASum3G_Slip0_2P          */		       1,	/* Comment [ Cut slip compensation value if : 0%>avg.Front wheel slip >-2%, AND -1g > Front arad sum > -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASumAb3G_Slip0_2P        */		       2,	/* Comment [ Cut slip compensation value if : 0%>avg.Front wheel slip >-2%, AND Front arad sum < -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASum1G_Slip2_5P          */		       3,	/* Comment [ Cut slip compensation value if : -2%>avg.Front wheel slip >-5%, AND Front arad sum > -1g ] */
	/* char8_t     	S8_F_WL_Fact_ASum3G_Slip2_5P          */		       4,	/* Comment [ Cut slip compensation value if : -2%>avg.Front wheel slip >-5%, AND -1g > Front arad sum > -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASumAb3G_Slip2_5P        */		       4,	/* Comment [ Cut slip compensation value if : -2%>avg.Front wheel slip >-5%, AND Front arad sum < -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASum1G_SlipAb5P          */		       3,	/* Comment [ Cut slip compensation value if : avg.Front wheel slip <-5%, AND Front arad sum > -1g ] */
	/* char8_t     	S8_F_WL_Fact_ASum3G_SlipAb5P          */		       4,	/* Comment [ Cut slip compensation value if : avg.Front wheel slip <-5%, AND -1g > Front arad sum > -3g ] */
	/* char8_t     	S8_F_WL_Fact_ASumAb3G_SlipAb5P        */		       5,	/* Comment [ Cut slip compensation value if : avg.Front wheel slip <-5%, AND Front arad sum < -3g ] */
	/* int16_t     	S16_Decel_Gain_EBD_100_150kph_R       */		      18,	/* Comment [ Slip Gain, EBD control, 100-150kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Decel_Gain_EBD_20_40kph_R         */		      14,	/* Comment [ Slip Gain, EBD control, 20-40kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Decel_Gain_EBD_40_60kph_R         */		      20,	/* Comment [ Slip Gain, EBD control, 40-60kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Decel_Gain_EBD_60_100kph_R        */		      22,	/* Comment [ Slip Gain, EBD control, 60-100kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Decel_Gain_EBD_Ab_150kph_R        */		      24,	/* Comment [ Slip Gain, EBD control, Above 150kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Decel_Gain_EBD_B_20kph_R          */		      12,	/* Comment [ Slip Gain, EBD control, Below 20kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_100_150kph_R        */		      15,	/* Comment [ Slip Gain, EBD control, 100-150kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_20_40kph_R          */		       7,	/* Comment [ Slip Gain, EBD control, 20-40kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_40_60kph_R          */		      10,	/* Comment [ Slip Gain, EBD control, 40-60kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_60_100kph_R         */		      12,	/* Comment [ Slip Gain, EBD control, 60-100kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_Ab_150kph_R         */		      17,	/* Comment [ Slip Gain, EBD control, Above 150kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_Slip_Gain_EBD_B_20kph_R           */		       5,	/* Comment [ Slip Gain, EBD control, Below 20kph, Decel State, max dump time이 4가 넘지 않도록 ] */
	/* int16_t     	S16_EBD_Dump_HDF_ref                  */		    2500,
	/* int16_t     	S16_EBD_Hold_Dump_ref                 */		     500,	/* Comment [ EBD dump reference after hold ] */
	/* uchar8_t    	U8_1st_Dumpscantime_EBD_R             */		       5,	/* Comment [ EBD Dump time in ms of 1st dump-scan, default 7, min 5, max 20 ] */
	/* uchar8_t    	U8_nth_Dumpscantime_EBD_R             */		       5,	/* Comment [ EBD Dump time in ms of nth dump-scans, default 7, min 5, max 20 ] */
	/* int16_t     	S16_EBD_KNEE_POINT                    */		     900,	/* Comment [ required EBD cut-in pressure ] */
	                                                        		        	/* ScaleVal[     90 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* uint16_t    	U16_TIRE_L                            */		    2010,	/* Comment [ Tire Size ] */
	/* uint16_t    	U16_TIRE_L_R                          */		    2010,	/* Comment [ Rear Tire Size in case of F_R_Diff ] */
	/* uint16_t    	U16_WHEEL_TIME_STAMP_RESOL            */		    1000,	/* Comment [ Wheel distance pulse time stamp resolution ] */
	/* uchar8_t    	U8_F_R_DIFF                           */		       1,	/* Comment [ Front Rear Size Difference ] */
	/* uchar8_t    	U8_TEETH                              */		      48,	/* Comment [ Teeth Number ] */
	/* uchar8_t    	U8_TEETH_R                            */		      47,	/* Comment [ Rear Teeth Number in case of F_R_Diff ] */
	/* uint16_t    	K_Low_Brake_Fluid_Time                */		    2860,	/* Comment [ Brake fluid level low fault detection time(default : 20sec) ] */
	                                                        		        	/* ScaleVal[   28.6 sec ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 655.35 ] */
	/* uint16_t    	K_odometer_Error_Rate                 */		   10000,	/* Comment [ Odometer error compenstation rate ] */
	/* uint16_t    	K_Tool_Learn_Delay_Time               */		     600,	/* Comment [ Delay time for re-learning Immobilizer ID(default : 600sec) ] */
	/* int16_t     	S16_FAILSAFE_ABS_RESERVED_7           */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* int16_t     	S16_FAILSAFE_ABS_RESERVED_8           */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* char8_t     	S8_FAILSAFE_ABS_RESERVED_3            */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* char8_t     	S8_FAILSAFE_ABS_RESERVED_4            */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* uint16_t    	U16_FAILSAFE_ABS_RESERVED_5           */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* uint16_t    	U16_FAILSAFE_ABS_RESERVED_6           */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* uchar8_t    	U16_MOTOR_INIT_LOCK_TARGET_SPEED      */		      30,	/* Comment [ Motor initial lock check speed(default : 30kph) ] */
	/* uchar8_t    	U8_FAILSAFE_ABS_RESERVED_1            */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* uchar8_t    	U8_FAILSAFE_ABS_RESERVED_2            */		       0,	/* Comment [ reserved parameter for failsafe(ABS) ] */
	                                                        		        	/* ScaleVal[          0 ]	Scale [ f(x) = (X + 0) * 0 ]  Range   [ 0 <= X <= 0 ] */
	/* uchar8_t    	U8_MOTOR_INIT_LOCK_CHECK_ON_TIME      */		      10,	/* Comment [ Motor drive time on initial lock check(default : 10ms) ] */
	/* int16_t     	S16_VREF_CBS_UPPER_LIMIT              */		      50,	/* Comment [ Acceleration limitation of vehicle ,especially for vehicle reference speed  ] */
	                                                        		        	/* ScaleVal[        0.5 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_VREF_LOWER_LIMIT                  */		    -120,	/* Comment [ Braking deceleration limitation of vehicle ,specially for vehicle reference speed ] */
	                                                        		        	/* ScaleVal[       -1.2 ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ -327.68 <= X <= 327.67 ] */
	/* int16_t     	S16_VREF_LOW_MU_DUMP_1                */		     140,	/* Comment [ Conversion factor of dump/rise control counter, especially for reference vehicle deceleration in ABS 1st cycle (condition 1) ] */
	/* int16_t     	S16_VREF_LOW_MU_DUMP_2                */		      80,	/* Comment [ Conversion factor of dump/rise control counter, especially for reference vehicle deceleration in ABS 1st cycle (condition 2) ] */
	/* uchar8_t    	U8_VREF_TUN_TEMP1                     */		       1,	/* Comment [ Vref temp T.P ] */
	/* uchar8_t    	U8_VREF_TUN_TEMP2                     */		       1	/* Comment [ Vref temp T.P2 ] */
};

#define LOGIC_CAL_MODULE_0_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

