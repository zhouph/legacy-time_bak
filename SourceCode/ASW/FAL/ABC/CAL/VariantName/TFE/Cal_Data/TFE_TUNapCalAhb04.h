#include  "../../APCalDataForm.h"


#define LOGIC_CAL_MODULE_32_START


const /*far*/	DATA_APCALAHB04_t	apCalAhb04 =
{

	/* uint16_t    	apCalAhb04Info.CDID                   */		0xFFFF,
	/* uint16_t    	apCalAhb04Info.CDVer                  */		0xFFFF,
	/* uchar8_t    	apCalAhb04Info.MID                    */		0xFF,
	/* uchar8_t    	U8_HW_TEST_MODE                       */		       3,	/* Comment [ Determine which HW will be tested (1:Pattern_1, 2:Pattern_2, 3:Valve_Characteristics, 4:Motor_Test) ] */
	/* uchar8_t    	U8_HW_VALVE_TEST_MODE                 */		       6,	/* Comment [ Selection of tested valve type (AV or RV) and test pattern (1:AV_STEP, 2:AV_PIRAMID, 3:RV_STEP, 4:RV_PIRAMID, 5:REL_RATE_VS_CURR_VS_INITP_RV, 6:REL_RATE_VS_CURR_VS_INITP_AV) ] */
	/* uchar8_t    	U8_VALVE_AV_TEST_MODE                 */		       5,	/* Comment [ Determine apply valve test mode (2:VARYING_INITIAL_PRESS, 4:RAMP_ON_AV_CURRENT, 5:VARYING_AV_CURRENT) ] */
	/* uchar8_t    	U8_VALVE_RV_TEST_MODE                 */		       3,	/* Comment [ Determine release valve test mode (1:VARYING_RV_CURRENT, 2:VARYING_INITIAL_PRESS, 3:FADE_OUT_RV_CURRENT) ] */
	/* uchar8_t    	U8_VV_SELECT_MAIN_VALVE               */		       0,	/* Comment [ Testing valve selection if 1 V/V control is activated ] */
	/* uchar8_t    	U8_VV_SINGLE_AV_TEST_ENABLE           */		       0,	/* Comment [ Single apply valve test ] */
	/* uchar8_t    	U8_VV_SINGLE_RV_TEST_ENABLE           */		       0,	/* Comment [ Single release valve test ] */
	/* int16_t     	S16_HPA_TEST_PRESSURE                 */		    1800,	/* Comment [ HPA Pressure setting for test ] */
	                                                        		        	/* ScaleVal[    180 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* uint16_t    	U16_AV_TEST_CURRENT                   */		    1500,	/* Comment [ Apply valve current setting for test ] */
	/* uint16_t    	U16_CV_TEST_CURRENT                   */		    2250,	/* Comment [ Cut valve current setting for test ] */
	/* uint16_t    	U16_MOTOR_TEST_VOLT                   */		    1400,	/* Comment [ Motor target voltage setting for test ] */
	                                                        		        	/* ScaleVal[       14 V ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 16 ] */
	/* uint16_t    	U16_RV_TEST_CURRENT                   */		    1500,	/* Comment [ Release valve current setting for test ] */
	/* uint16_t    	U16_INIT_WAIT_T                       */		     200,	/* Comment [ Waiting time to start the test ] */
	                                                        		        	/* ScaleVal[      1 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_TEST_APPLY_T                      */		    1000,	/* Comment [ Current applied time to satisfy the condition of S16_TEST_INITIAL_PRESS_RV(AV) ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_TEST_MODE_T                       */		    1200,	/* Comment [ The time specifed test action is performed(step, ramp) ] */
	                                                        		        	/* ScaleVal[      6 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_TEST_RESET_T                      */		     120,	/* Comment [ Time to back to initial state ] */
	                                                        		        	/* ScaleVal[   600 msec ]	Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 327675 ] */
	/* uint16_t    	U16_HPA_CHARGE_T_V_1                  */		    1000,	/* Comment [ HPA charging time at MC_P level 1 ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_HPA_CHARGE_T_V_2                  */		    1000,	/* Comment [ HPA charging time at MC_P level 2 ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_HPA_CHARGE_T_V_3                  */		    1000,	/* Comment [ HPA charging time at MC_P level 3 ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_HPA_CHARGE_T_V_4                  */		    1000,	/* Comment [ HPA charging time at MC_P level 4 ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* uint16_t    	U16_HPA_CHARGE_T_V_5                  */		    1000,	/* Comment [ HPA charging time at MC_P level 5 ] */
	                                                        		        	/* ScaleVal[      5 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
	/* int16_t     	S16_AV_CURRENT_HIGH_P                 */		    1700,	/* Comment [ AV current at high pressure ] */
	/* int16_t     	S16_AV_CURRENT_HIGH_P_1VV             */		    2000,	/* Comment [ AV current at high pressure point when 1 valve test mode is activated ] */
	/* int16_t     	S16_AV_CURRENT_LOW_P                  */		     900,	/* Comment [ AV current at low pressure ] */
	/* int16_t     	S16_AV_CURRENT_LOW_P_1VV              */		    1200,	/* Comment [ AV current at low pressure point when 1 valve test mode is activated ] */
	/* int16_t     	S16_TEST_INITIAL_PRESS_RV             */		       0,	/* Comment [ Initial value of BC_P condition for RV test ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_TEST_INITIAL_P_DIFF_RV            */		     100,	/* Comment [ Increment of initial BC_P for RV step mode test ] */
	                                                        		        	/* ScaleVal[     10 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_TEST_MAX_MPRESS_RV                */		    1000,	/* Comment [ Maximum value of MC_P for RV test ] */
	                                                        		        	/* ScaleVal[    100 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* uint16_t    	U16_VALVE_CURRENT_DIFF_RV             */		     100,	/* Comment [ Increment of valve current for RV test ] */
	/* uint16_t    	U16_VALVE_INITIAL_CURRENT_RV          */		    2250,	/* Comment [ Initial valve current for RV test ] */
	/* uint16_t    	U16_VALVE_TEST_MAX_CURRENT_RV         */		    2250,	/* Comment [ Maximum current for RV test ] */
	/* int16_t     	S16_VALVE_CURRENT_DEC_PERIOD          */		       4,	/* Comment [ Period for current decrement (1=5ms) ] */
	/* uint16_t    	U16_TEST_RUNS_FADE_OUT                */		      16,	/* Comment [ Test repeat number for Fade Out mode test ] */
	/* int16_t     	S16_HPA_VARIATION_DIFF                */		     200,	/* Comment [ Increment of initial BC_P for RV step mode test ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_HPA_VARIATION_MAX_PRESSURE        */		    1800,	/* Comment [ HPA maximum pressure ] */
	                                                        		        	/* ScaleVal[    180 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_HPA_VARIATION_MIN_PRESSURE        */		    1800,	/* Comment [ HPA minimum pressure ] */
	                                                        		        	/* ScaleVal[    180 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_TEST_INITIAL_PRESS_AV             */		       0,	/* Comment [ Initial value of BC_P condition for AV test ] */
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_TEST_INITIAL_P_DIFF_AV            */		     200,	/* Comment [ Increment of initial BC_P for AV step mode test ] */
	                                                        		        	/* ScaleVal[     20 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* int16_t     	S16_TEST_MAX_MPRESS_AV                */		    1800,	/* Comment [ Maximum value of MC_P for AV test ] */
	                                                        		        	/* ScaleVal[    180 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 180 ] */
	/* uint16_t    	U16_VALVE_CURRENT_DIFF_AV             */		     100,	/* Comment [ Increment of valve current for AV current variation test ] */
	/* uint16_t    	U16_VALVE_INITIAL_CURRENT_AV          */		    2250,	/* Comment [ Initial valve current for AV test ] */
	/* uint16_t    	U16_VALVE_TEST_MAX_CURRENT_AV         */		    2250,	/* Comment [ Maximum current for AV test ] */
	/* int16_t     	S16_VALVE_CURRENT_INC_PERIOD          */		       4,	/* Comment [ Period for current increment (1=5ms) ] */
	/* uint16_t    	U16_TEST_RUNS_RAMP_ON                 */		      16,	/* Comment [ Test repeat number for Ramp On mode test ] */
	/* uchar8_t    	GM_SSTS_TEST_MODE                     */		       3,
	/* int16_t     	S16_PT_RAMP_RATE                      */		     500,
	                                                        		        	/* ScaleVal[    5 bar/s ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TEST_PRESS_HIGH                   */		    1500,
	                                                        		        	/* ScaleVal[     15 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_TEST_PRESS_LOW                    */		       0,
	                                                        		        	/* ScaleVal[      0 bar ]	Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_01          */		       1,	/* U16_PT_LOOP_CNT_REF Comment [ AHB HW Test Temp Parameter_01 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_02          */		       0,	/* Comment [ AHB HW Test Temp Parameter_02 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_03          */		       0,	/* Comment [ AHB HW Test Temp Parameter_03 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_04          */		       0,	/* Comment [ AHB HW Test Temp Parameter_04 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_05          */		       0,	/* Comment [ AHB HW Test Temp Parameter_05 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_06          */		       0,	/* Comment [ AHB HW Test Temp Parameter_06 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_07          */		       0,	/* Comment [ AHB HW Test Temp Parameter_07 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_08          */		       0,	/* Comment [ AHB HW Test Temp Parameter_08 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_09          */		       0,	/* Comment [ AHB HW Test Temp Parameter_09 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_10          */		       0,	/* Comment [ AHB HW Test Temp Parameter_10 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_11          */		       0,	/* Comment [ AHB HW Test Temp Parameter_11 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_12          */		       0,	/* Comment [ AHB HW Test Temp Parameter_12 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_13          */		       0,	/* Comment [ AHB HW Test Temp Parameter_13 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_14          */		       0,	/* Comment [ AHB HW Test Temp Parameter_14 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_15          */		       0,	/* Comment [ AHB HW Test Temp Parameter_15 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_16          */		       0,	/* Comment [ AHB HW Test Temp Parameter_16 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_17          */		       0,	/* Comment [ AHB HW Test Temp Parameter_17 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_18          */		       0,	/* Comment [ AHB HW Test Temp Parameter_18 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_19          */		       0,	/* Comment [ AHB HW Test Temp Parameter_19 ] */
	/* int16_t     	S16_AHB_LOGIC_HW_CAL_TEMP_20          */		       0	/* Comment [ AHB HW Test Temp Parameter_20 ] */
};

#define LOGIC_CAL_MODULE_32_STOP


/*=================================================================================*/
/*  End Of File!                                                                   */
/*=================================================================================*/

