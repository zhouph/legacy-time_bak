/**
 * @defgroup Abc_Ctrl_Ifa Abc_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ABC_CTRL_IFA_H_
#define ABC_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Abc_Ctrl_Read_Abc_CtrlEemFailData(data) do \
{ \
    *data = Abc_CtrlEemFailData; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxAccelPedlInfo(data) do \
{ \
    *data = Abc_CtrlCanRxAccelPedlInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo(data) do \
{ \
    *data = Abc_CtrlCanRxIdbInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEngTempInfo(data) do \
{ \
    *data = Abc_CtrlCanRxEngTempInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Abc_CtrlMotRotgAgSigInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEscSwtStInfo(data) do \
{ \
    *data = Abc_CtrlEscSwtStInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo(data) do \
{ \
    *data = Abc_CtrlWhlSpdInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlSasCalInfo(data) do \
{ \
    *data = Abc_CtrlSasCalInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlBrkPedlStatusInfo(data) do \
{ \
    *data = Abc_CtrlBrkPedlStatusInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPFildInfo(data) do \
{ \
    *data = Abc_CtrlCircPFildInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPOffsCorrdInfo(data) do \
{ \
    *data = Abc_CtrlCircPOffsCorrdInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo(data) do \
{ \
    *data = Abc_CtrlEstimdWhlPInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlSimrPOffsCorrdInfo(data) do \
{ \
    *data = Abc_CtrlPedlSimrPOffsCorrdInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlTrvlOffsCorrdInfo(data) do \
{ \
    *data = Abc_CtrlPedlTrvlOffsCorrdInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPistPFildInfo(data) do \
{ \
    *data = Abc_CtrlPistPFildInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPistPOffsCorrdInfo(data) do \
{ \
    *data = Abc_CtrlPistPOffsCorrdInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlRgnBrkCoopWithAbsInfo(data) do \
{ \
    *data = Abc_CtrlRgnBrkCoopWithAbsInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlStkRecvryActnIfInfo(data) do \
{ \
    *data = Abc_CtrlStkRecvryActnIfInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo(data) do \
{ \
    *data = Abc_CtrlMuxCmdExecStInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTxESCSensorInfo(data) do \
{ \
    *data = Abc_CtrlTxESCSensorInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlSenPwrMonitorData(data) do \
{ \
    *data = Abc_CtrlSenPwrMonitorData; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData(data) do \
{ \
    *data = Abc_CtrlEemSuspectData; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData(data) do \
{ \
    *data = Abc_CtrlEemEceData; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlYAWMSerialInfo(data) do \
{ \
    *data = Abc_CtrlYAWMSerialInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlYAWMOutInfo(data) do \
{ \
    *data = Abc_CtrlYAWMOutInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlFinalTarPInfo(data) do \
{ \
    *data = Abc_CtrlFinalTarPInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_BBSSol(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_BBSSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ESCSol(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ESCSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_FrontSol(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_FrontSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_RearSol(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_RearSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Motor(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Motor; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_MPS(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_MPS; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_MGD(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_MGD; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_BBSValveRelay(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_BBSValveRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ESCValveRelay(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ESCValveRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ECUHw(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ECUHw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ASIC(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ASIC; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_OverVolt(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_OverVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_UnderVolt(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_UnderVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_LowVolt(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_LowVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_LowerVolt(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_LowerVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SenPwr_5V(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SenPwr_5V; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Yaw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Ay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Ax(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Ax; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Str; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_CirP1(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_CirP1; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_CirP2(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_CirP2; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SimP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_BLS; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ESCSw(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ESCSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_HDCSw(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_HDCSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_AVHSw(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_AVHSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_BrakeLampRelay(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_BrakeLampRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_EssRelay(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_EssRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_GearR(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_GearR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_Clutch(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_Clutch; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_ParkBrake(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_ParkBrake; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_BrakeFluid(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_BrakeFluid; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_TCSTemp(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_TCSTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_HDCTemp(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_HDCTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SCCTemp(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SCCTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_TVBBTemp(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_TVBBTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_MainCanLine(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_MainCanLine; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SubCanLine(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SubCanLine; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_EMSTimeOut(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_FWDTimeOut(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_FWDTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_TCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_HCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_HCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_MCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_MCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_VariantCoding(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_VariantCoding; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_SameSideWss(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_SameSideWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_DiagonalWss(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_DiagonalWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_FrontWss(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_FrontWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemFailData_Eem_Fail_RearWss(data) do \
{ \
    *data = Abc_CtrlEemFailData.Eem_Fail_RearWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Cbs(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Cbs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Ebd(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Abs(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Edc(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Edc; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Btcs(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Btcs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Etcs(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Etcs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Tcs(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Vdc(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Hsa(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hsa; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Hdc(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hdc; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Pba(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Pba; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Avh(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Avh; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_CtrlIhb_Moc(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Moc; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_BBS_AllControlInhibit(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_BBS_AllControlInhibit; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_BBS_DiagControlInhibit(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_BBS_DiagControlInhibit; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_BBS_DegradeModeFail; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Abc_CtrlEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_TarGearPosi(data) do \
{ \
    *data = Abc_CtrlCanRxIdbInfo.TarGearPosi; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_GearSelDisp(data) do \
{ \
    *data = Abc_CtrlCanRxIdbInfo.GearSelDisp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    *data = Abc_CtrlCanRxIdbInfo.TcuSwiGs; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEngTempInfo_EngTemp(data) do \
{ \
    *data = Abc_CtrlCanRxEngTempInfo.EngTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEngTempInfo_EngTempErr(data) do \
{ \
    *data = Abc_CtrlCanRxEngTempInfo.EngTempErr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_Ax(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.Ax; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_YawRate(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.YawRate; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.SteeringAngle; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_Ay(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.Ay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_PbSwt(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.PbSwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_ClutchSwt(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.ClutchSwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_GearRSwt(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.GearRSwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngActIndTq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngActIndTq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngRpm(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngRpm; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngIndTq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngIndTq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngFrictionLossTq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngFrictionLossTq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngStdTq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngStdTq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TurbineRpm(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TurbineRpm; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_ThrottleAngle(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.ThrottleAngle; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TpsResol1000(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TpsResol1000; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_PvAvCanResol1000(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.PvAvCanResol1000; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngChr(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngChr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngVol(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngVol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_GearType(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.GearType; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngClutchState(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngClutchState; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngTqCmdBeforeIntv(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_MotEstTq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.MotEstTq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_MotTqCmdBeforeIntv(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TqIntvTCU(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TqIntvTCU; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TqIntvSlowTCU(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TqIntvSlowTCU; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TqIncReq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TqIncReq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_DecelReq(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.DecelReq; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_RainSnsStat(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.RainSnsStat; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_EngSpdErr(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.EngSpdErr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_AtType(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.AtType; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_MtType(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.MtType; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_CvtType(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.CvtType; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_DctType(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.DctType; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_HevAtTcu(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.HevAtTcu; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TurbineRpmErr(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TurbineRpmErr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_ThrottleAngleErr(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.ThrottleAngleErr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TopTrvlCltchSwtAct(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TopTrvlCltchSwtActV(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_HillDesCtrlMdSwtAct(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_HillDesCtrlMdSwtActV(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_WiperIntSW(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.WiperIntSW; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_WiperLow(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.WiperLow; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_WiperHigh(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.WiperHigh; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_WiperValid(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.WiperValid; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_WiperAuto(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.WiperAuto; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo_TcuFaultSts(data) do \
{ \
    *data = Abc_CtrlCanRxEscInfo.TcuFaultSts; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    *data = Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Abc_CtrlEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEscSwtStInfo_TcsDisabledBySwt(data) do \
{ \
    *data = Abc_CtrlEscSwtStInfo.TcsDisabledBySwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Abc_CtrlWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Abc_CtrlWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Abc_CtrlWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Abc_CtrlWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlSasCalInfo_DiagSasCaltoAppl(data) do \
{ \
    *data = Abc_CtrlSasCalInfo.DiagSasCaltoAppl; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlBrkPedlStatusInfo_DrvrIntendToRelsBrk(data) do \
{ \
    *data = Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar(data) do \
{ \
    *data = Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar(data) do \
{ \
    *data = Abc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd(data) do \
{ \
    *data = Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd(data) do \
{ \
    *data = Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo_FrntLeEstimdWhlP(data) do \
{ \
    *data = Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo_FrntRiEstimdWhlP(data) do \
{ \
    *data = Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo_ReLeEstimdWhlP(data) do \
{ \
    *data = Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo_ReRiEstimdWhlP(data) do \
{ \
    *data = Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd(data) do \
{ \
    *data = Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlTrvlOffsCorrdInfo_PdfRawOffsCorrd(data) do \
{ \
    *data = Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlTrvlOffsCorrdInfo_PdtRawOffsCorrd(data) do \
{ \
    *data = Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPistPFildInfo_PistPFild_1_100Bar(data) do \
{ \
    *data = Abc_CtrlPistPFildInfo.PistPFild_1_100Bar; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPistPOffsCorrdInfo_PistPOffsCorrd(data) do \
{ \
    *data = Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdSlip(data) do \
{ \
    *data = Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdUDiff(data) do \
{ \
    *data = Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlStkRecvryActnIfInfo_RecommendStkRcvrLvl(data) do \
{ \
    *data = Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlStkRecvryActnIfInfo_StkRecvryCtrlState(data) do \
{ \
    *data = Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlFrntLe(data) do \
{ \
    *data = Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlFrntRi(data) do \
{ \
    *data = Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlReLe(data) do \
{ \
    *data = Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlReRi(data) do \
{ \
    *data = Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    *data = Abc_CtrlTxESCSensorInfo.EstimatedYaw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTxESCSensorInfo_EstimatedAY(data) do \
{ \
    *data = Abc_CtrlTxESCSensorInfo.EstimatedAY; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    *data = Abc_CtrlTxESCSensorInfo.A_long_1_1000g; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SameSideWss(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SameSideWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_DiagonalWss(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_DiagonalWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BBSSol(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BBSSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ESCSol(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ESCSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_FrontSol(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_FrontSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_RearSol(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_RearSol; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Motor(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Motor; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_MPS(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_MPS; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_MGD(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_MGD; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BBSValveRelay(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BBSValveRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ESCValveRelay(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ESCValveRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ECUHw(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ECUHw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ASIC(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ASIC; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_OverVolt(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_OverVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_UnderVolt(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_UnderVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_LowVolt(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_LowVolt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SenPwr_12V(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_12V; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SenPwr_5V(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_5V; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Yaw(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Yaw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Ay(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Ay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Ax(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Ax; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Str(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Str; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_CirP1(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_CirP1; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_CirP2(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_CirP2; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SimP(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SimP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BS(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BS; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BLS(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BLS; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ESCSw(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ESCSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_HDCSw(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_HDCSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_AVHSw(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_AVHSw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BrakeLampRelay(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BrakeLampRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_EssRelay(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_EssRelay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_GearR(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_GearR; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_Clutch(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_Clutch; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_ParkBrake(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_BrakeFluid(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_BrakeFluid; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_TCSTemp(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_TCSTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_HDCTemp(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_HDCTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SCCTemp(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SCCTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_TVBBTemp(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_TVBBTemp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_MainCanLine(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_SubCanLine(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_SubCanLine; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_EMSTimeOut(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_FWDTimeOut(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_FWDTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_TCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_HCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_HCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_MCUTimeOut(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_MCUTimeOut; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemSuspectData_Eem_Suspect_VariantCoding(data) do \
{ \
    *data = Abc_CtrlEemSuspectData.Eem_Suspect_VariantCoding; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Wss(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Wss; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Yaw(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Yaw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Ay(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Ay; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Ax(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Ax; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Cir1P(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Cir1P; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Cir2P(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Cir2P; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_SimP(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_SimP; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Bls(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Bls; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Pedal(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Pedal; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Motor(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Motor; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Vdc_Sw(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Vdc_Sw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Hdc_Sw(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Hdc_Sw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_GearR_Sw(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_GearR_Sw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEemEceData_Eem_Ece_Clutch_Sw(data) do \
{ \
    *data = Abc_CtrlEemEceData.Eem_Ece_Clutch_Sw; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlYAWMSerialInfo_YAWM_SerialNumOK_Flg(data) do \
{ \
    *data = Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlYAWMSerialInfo_YAWM_SerialUnMatch_Flg(data) do \
{ \
    *data = Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlYAWMOutInfo_YAWM_Temperature_Err(data) do \
{ \
    *data = Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlFinalTarPInfo_FinalMaxCircuitTp(data) do \
{ \
    *data = Abc_CtrlFinalTarPInfo.FinalMaxCircuitTp; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlFSBbsVlvInitEndFlg(data) do \
{ \
    *data = Abc_CtrlFSBbsVlvInitEndFlg; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEcuModeSts(data) do \
{ \
    *data = Abc_CtrlEcuModeSts; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlIgnOnOffSts(data) do \
{ \
    *data = Abc_CtrlIgnOnOffSts; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlVBatt1Mon(data) do \
{ \
    *data = Abc_CtrlVBatt1Mon; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlFSEscVlvInitEndFlg(data) do \
{ \
    *data = Abc_CtrlFSEscVlvInitEndFlg; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlDiagSci(data) do \
{ \
    *data = Abc_CtrlDiagSci; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlCanRxGearSelDispErrInfo(data) do \
{ \
    *data = Abc_CtrlCanRxGearSelDispErrInfo; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPCtrlAct(data) do \
{ \
    *data = Abc_CtrlPCtrlAct; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlBlsSwt(data) do \
{ \
    *data = Abc_CtrlBlsSwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlEscSwt(data) do \
{ \
    *data = Abc_CtrlEscSwt; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlBrkPRednForBaseBrkCtrlr(data) do \
{ \
    *data = Abc_CtrlBrkPRednForBaseBrkCtrlr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlPedlTrvlFinal(data) do \
{ \
    *data = Abc_CtrlPedlTrvlFinal; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlRgnBrkCtrlrActStFlg(data) do \
{ \
    *data = Abc_CtrlRgnBrkCtrlrActStFlg; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlFinalTarPFromPCtrl(data) do \
{ \
    *data = Abc_CtrlFinalTarPFromPCtrl; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTarPFromBaseBrkCtrlr(data) do \
{ \
    *data = Abc_CtrlTarPFromBaseBrkCtrlr; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlTarPFromBrkPedl(data) do \
{ \
    *data = Abc_CtrlTarPFromBrkPedl; \
}while(0);

#define Abc_Ctrl_Read_Abc_CtrlMTRInitEndFlg(data) do \
{ \
    *data = Abc_CtrlMTRInitEndFlg; \
}while(0);


/* Set Output DE MAcro Function */
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo(data) do \
{ \
    Abc_CtrlCanTxInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo(data) do \
{ \
    Abc_CtrlAbsCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAvhCtrlInfo(data) do \
{ \
    Abc_CtrlAvhCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlBaCtrlInfo(data) do \
{ \
    Abc_CtrlBaCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo(data) do \
{ \
    Abc_CtrlEbdCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbpCtrlInfo(data) do \
{ \
    Abc_CtrlEbpCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEpbiCtrlInfo(data) do \
{ \
    Abc_CtrlEpbiCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo(data) do \
{ \
    Abc_CtrlEscCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHdcCtrlInfo(data) do \
{ \
    Abc_CtrlHdcCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHsaCtrlInfo(data) do \
{ \
    Abc_CtrlHsaCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlSccCtrlInfo(data) do \
{ \
    Abc_CtrlSccCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlStkRecvryCtrlIfInfo(data) do \
{ \
    Abc_CtrlStkRecvryCtrlIfInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo(data) do \
{ \
    Abc_CtrlTcsCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTvbbCtrlInfo(data) do \
{ \
    Abc_CtrlTvbbCtrlInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlIvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrIvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlIvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrIvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlOvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrOvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlOvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrOvReqData(data) \
{ \
    for(i=0;i<10;i++) Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[i] = *data[i]; \
}
#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlIvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FlIvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrIvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FrIvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlIvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RlIvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrIvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RrIvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlOvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FlOvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrOvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FrOvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlOvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RlOvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrOvReq(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RrOvReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlIvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FlIvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrIvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FrIvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlIvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RlIvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrIvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RrIvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FlOvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FlOvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_FrOvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.FrOvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RlOvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RlOvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo_RrOvDataLen(data) do \
{ \
    Abc_CtrlWhlVlvReqAbcInfo.RrOvDataLen = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TqIntvTCS(data) do \
{ \
    Abc_CtrlCanTxInfo.TqIntvTCS = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TqIntvMsr(data) do \
{ \
    Abc_CtrlCanTxInfo.TqIntvMsr = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TqIntvSlowTCS(data) do \
{ \
    Abc_CtrlCanTxInfo.TqIntvSlowTCS = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_MinGear(data) do \
{ \
    Abc_CtrlCanTxInfo.MinGear = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_MaxGear(data) do \
{ \
    Abc_CtrlCanTxInfo.MaxGear = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TcsReq(data) do \
{ \
    Abc_CtrlCanTxInfo.TcsReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TcsCtrl(data) do \
{ \
    Abc_CtrlCanTxInfo.TcsCtrl = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_AbsAct(data) do \
{ \
    Abc_CtrlCanTxInfo.AbsAct = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TcsGearShiftChr(data) do \
{ \
    Abc_CtrlCanTxInfo.TcsGearShiftChr = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_EspCtrl(data) do \
{ \
    Abc_CtrlCanTxInfo.EspCtrl = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_MsrReq(data) do \
{ \
    Abc_CtrlCanTxInfo.MsrReq = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlCanTxInfo_TcsProductInfo(data) do \
{ \
    Abc_CtrlCanTxInfo.TcsProductInfo = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.FrntWhlSlip = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDesTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsCtrlModeFrntLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsCtrlModeFrntRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsCtrlModeReLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsCtrlModeReRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPRateFrntLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPRateFrntRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPRateReLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsTarPRateReRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsPrioFrntLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsPrioFrntRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsPrioReLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsPrioReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsPrioReRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsPrioReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDelTarPFrntLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDelTarPFrntRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDelTarPReLe(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo_AbsDelTarPReRi(data) do \
{ \
    Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAvhCtrlInfo_AvhActFlg(data) do \
{ \
    Abc_CtrlAvhCtrlInfo.AvhActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAvhCtrlInfo_AvhDefectFlg(data) do \
{ \
    Abc_CtrlAvhCtrlInfo.AvhDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAvhCtrlInfo_AvhTarP(data) do \
{ \
    Abc_CtrlAvhCtrlInfo.AvhTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlBaCtrlInfo_BaActFlg(data) do \
{ \
    Abc_CtrlBaCtrlInfo.BaActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlBaCtrlInfo_BaCDefectFlg(data) do \
{ \
    Abc_CtrlBaCtrlInfo.BaCDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlBaCtrlInfo_BaTarP(data) do \
{ \
    Abc_CtrlBaCtrlInfo.BaTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdActFlg(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdDefectFlg(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPFrntLe(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPFrntRi(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPReLe(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPReRi(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPRateReLe(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPRateReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdTarPRateReRi(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdTarPRateReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdCtrlModeReLe(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdCtrlModeReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdCtrlModeReRi(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdCtrlModeReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdDelTarPReLe(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdDelTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo_EbdDelTarPReRi(data) do \
{ \
    Abc_CtrlEbdCtrlInfo.EbdDelTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbpCtrlInfo_EbpActFlg(data) do \
{ \
    Abc_CtrlEbpCtrlInfo.EbpActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbpCtrlInfo_EbpDefectFlg(data) do \
{ \
    Abc_CtrlEbpCtrlInfo.EbpDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEbpCtrlInfo_EbpTarP(data) do \
{ \
    Abc_CtrlEbpCtrlInfo.EbpTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEpbiCtrlInfo_EpbiActFlg(data) do \
{ \
    Abc_CtrlEpbiCtrlInfo.EpbiActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEpbiCtrlInfo_EpbiDefectFlg(data) do \
{ \
    Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEpbiCtrlInfo_EpbiTarP(data) do \
{ \
    Abc_CtrlEpbiCtrlInfo.EpbiTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscActFlg(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscDefectFlg(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscCtrlModeFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscCtrlModeFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscCtrlModeReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscCtrlModeReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscCtrlModeReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscCtrlModeReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPRateFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPRateFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPRateReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPRateReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscTarPRateReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscTarPRateReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPrioFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPrioFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPrioFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPrioFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPrioReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPrioReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPrioReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPrioReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPreCtrlModeFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPreCtrlModeFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPreCtrlModeReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscPreCtrlModeReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscDelTarPFrntLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscDelTarPFrntRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscDelTarPReLe(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscDelTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo_EscDelTarPReRi(data) do \
{ \
    Abc_CtrlEscCtrlInfo.EscDelTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHdcCtrlInfo_HdcActFlg(data) do \
{ \
    Abc_CtrlHdcCtrlInfo.HdcActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHdcCtrlInfo_HdcDefectFlg(data) do \
{ \
    Abc_CtrlHdcCtrlInfo.HdcDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHdcCtrlInfo_HdcTarP(data) do \
{ \
    Abc_CtrlHdcCtrlInfo.HdcTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHsaCtrlInfo_HsaActFlg(data) do \
{ \
    Abc_CtrlHsaCtrlInfo.HsaActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHsaCtrlInfo_HsaDefectFlg(data) do \
{ \
    Abc_CtrlHsaCtrlInfo.HsaDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlHsaCtrlInfo_HsaTarP(data) do \
{ \
    Abc_CtrlHsaCtrlInfo.HsaTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlSccCtrlInfo_SccActFlg(data) do \
{ \
    Abc_CtrlSccCtrlInfo.SccActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlSccCtrlInfo_SccDefectFlg(data) do \
{ \
    Abc_CtrlSccCtrlInfo.SccDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlSccCtrlInfo_SccTarP(data) do \
{ \
    Abc_CtrlSccCtrlInfo.SccTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlStkRecvryCtrlIfInfo_StkRcvrEscAllowedTime(data) do \
{ \
    Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlStkRecvryCtrlIfInfo_StkRcvrAbsAllowedTime(data) do \
{ \
    Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlStkRecvryCtrlIfInfo_StkRcvrTcsAllowedTime(data) do \
{ \
    Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsActFlg(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsDefectFlg(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsTarPFrntLe(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsTarPFrntRi(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsTarPReLe(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsTarPReRi(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkCtrlFctFrntLeByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkCtrlFctFrntRiByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkCtrlFctReLeByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkCtrlFctReRiByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkTqGrdtReqFrntLeByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkTqGrdtReqFrntRiByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkTqGrdtReqReLeByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_BrkTqGrdtReqReRiByWspc(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsDelTarPFrntLe(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsDelTarPFrntRi(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsDelTarPReLe(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo_TcsDelTarPReRi(data) do \
{ \
    Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTvbbCtrlInfo_TvbbActFlg(data) do \
{ \
    Abc_CtrlTvbbCtrlInfo.TvbbActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTvbbCtrlInfo_TvbbDefectFlg(data) do \
{ \
    Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlTvbbCtrlInfo_TvbbTarP(data) do \
{ \
    Abc_CtrlTvbbCtrlInfo.TvbbTarP = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlFunctionLamp(data) do \
{ \
    Abc_CtrlFunctionLamp = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlActvBrkCtrlrActFlg(data) do \
{ \
    Abc_CtrlActvBrkCtrlrActFlg = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlAy(data) do \
{ \
    Abc_CtrlAy = *data; \
}while(0);

#define Abc_Ctrl_Write_Abc_CtrlVehSpd(data) do \
{ \
    Abc_CtrlVehSpd = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ABC_CTRL_IFA_H_ */
/** @} */
