/**
 * @defgroup Abc_Ctrl_Ifa Abc_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Ctrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Abc_Ctrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ABC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ABC_CTRL_START_SEC_CODE
#include "Abc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ABC_CTRL_STOP_SEC_CODE
#include "Abc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
