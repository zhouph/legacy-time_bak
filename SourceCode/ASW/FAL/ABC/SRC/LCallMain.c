/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LCallMain.c
* Description:		Main Code of Logic
* Logic version:	HV25
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C12			eslim			Initial Release
*  6216			eslim			Modified based on MISRA rule
********************************************************************************/

/* Includes ********************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCallMain
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCallMain.h"
#include "LCCallMain.h"
#include "LACallMain.h"
#include "LDABSDctRoadSurface.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LDABSCallDctForVref.H"
#include "LCABSDecideCtrlState.h"
#include "LCABSCallESPCombination.h"
#include "_LVarHead.h"

#include "LCTCSCallControl.h"
//#include "../CAL/APCalDataForm.h"

#include "hm_logic_var.h"

#if __VACUUM_SENSOR_ENABLE
#include "LAHBBCallActHW.h"
#endif

#if defined(__DURABILITY_TEST_ENABLE)
#include "LCHDCCallControl.h"
#endif
#include "LCCBCCallControl.h"
#include "LCHRBCallControl.h"

#if __FBC
#include "LCFBCCallControl.h"
#endif
#include "LCPBACallControl.h"

#if __SLS
#include "LCSLSCallControl.h"
#endif

#if __VDC
#include "LCPBACallControl.h"
#endif

#if __HSA
#include "LCHSACallControl.h"
#endif

#if __AVH
#include "LCAVHCallControl.h"
#endif

#if __TSP
#include "LCTSPCallControl.h"
#endif

#if __PAC 
#include "LCPACCallControl.h"
#endif 

#if __EPB_INTERFACE
#include "LCEPBCallControl.h"
#endif



/* Logcal Definiton ************************************************************/
/* uint8_t  temp_0_flg; //향후 Bit 로 전환 */
/* Variables Definition ********************************************************/
#if defined(__DURABILITY_TEST_ENABLE)
 
uint16_t	ESPCtlNumCnt;
uint16_t	TCSCtlNumCnt;
uint16_t 	ABSCtlNumCnt;
uint16_t	HSACtlNumCnt;
uint32_t	HDCCtlCnt;
uint32_t	CBSCtlCnt;
	#if __ECU==ABS_ECU_1
uint16_t 	ABS_ON_OLD_FOR_ET;
uint16_t 	CBS_ON_OLD_FOR_ET;
	#endif
	/*#if __ECU==ESP_ECU_1
		uint16_t 	ESP_ON_OLD_FOR_ET;
		uint16_t 	TCS_ON_OLD_FOR_ET;
	 #endif*/
#endif


/* Local Function prototype ****************************************************/
void L_vCallMain(void);
void L_vInitializeVariables(void);
void RAMSET(void);
void LFC_initial_parameter_set(void);
void LFC_initial_parameter_set_wl(struct W_STRUCT *WL_temp);
void abs_off(void);
void ebd_off(void);
void btc_off(void);
void vdc_off(void);

void u16mulu16divu16(void);
void s16muls16divs16(void);
void u16mul16div16(void);
void s16mul16div16(void);
void u16mul16div(void);
void s16mul16div(void);

#if defined(__DURABILITY_TEST_ENABLE)
void ResetCtlNumCnts(void);
void UpdateCtlFlagsForEnduranceTest(void);
void CntREdgeCtlFlagsForEnduranceTest(void);
#endif

#if __EDC
extern void LAEDC_vCallActCan(void);
#endif
/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		L_vCallMain
* CALLED BY:			Main()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Main Code of System
********************************************************************************/
void L_vCallMain(void)
{
    lcIgnOffModeFlg = 0;

/* Move to Signal Processing module */
    #if __4WD_VARIANT_CODE==ENABLE
	lsu8DrvMode=wmu8DrvModeToLogic;
	#elif __4WD==ENABLE
	lsu8DrvMode=DM_AUTO;
	#else
	lsu8DrvMode=DM_2WD;
	#endif
	BLS_PIN=fu1BLSSignal;
/* Move to Signal Processing module */

  #if defined(__DURABILITY_TEST_ENABLE)
	UpdateCtlFlagsForEnduranceTest();
  #endif

	ActvBrkCtrlInpIf();
	
	LS_vCallMainSensorSignal();

	LD_vCallMainDetection();
	
  #if (__MGH80_MOCi==ENABLE)
	L_vCallMainEPB();	  
  #endif		

	
  	LC_vCallMainControl();

#if __VDC
	LO_vCallMainCoordinator();
#endif
  	LA_vCallMainValve();

	LA_vCallMainCan();

	ActvBrkCtrlOutpIf();
  	
  #if defined(__DURABILITY_TEST_ENABLE)
	CntREdgeCtlFlagsForEnduranceTest();
  #endif
}

void L_vInitializeVariables(void)
{
	RAMSET();

/*	#if __PWM */ /* 2006.06.15  K.G.Y */
		LFC_initial_parameter_set();	/* initialize pwm parameter set : 1 pulse(added by 최 성 호) */
/*	#endif */ /* 2006.06.15  K.G.Y */
}

void RAMSET(void)
{
#if __VDC
    speed_fl=speed_fr=speed_rl=speed_rr=
    vrad_fl=vrad_fr=vrad_rl=vrad_rr=
#endif
    voptfz1=voptfz2
    =voptfz_mittel1=voptfz_mittel2
    =vref=vref2=vref4=(INT)VREF_MIN_SPEED_RESOL_8;            /* MAIN03C */
    /*Min Speed: 2kph->0.125 081208*/
    FL.vrad_resol_change=FR.vrad_resol_change
    =RL.vrad_resol_change=RR.vrad_resol_change
    =FZ1.voptfz_resol_change=FZ2.voptfz_resol_change
    =FZ1.voptfz_mittel_resol_change=FZ2.voptfz_mittel_resol_change
    =vref_resol_change=(INT)VREF_MIN_SPEED;


#if __VDC
    STABIL_fl=1; STABIL_fr=1; STABIL_rl=1; STABIL_rr=1;
	RE_ABS_flag=0;
#endif
#if __BTC
#if !SIM_MATLAB
	LSTCS_vInitBTCSVariable();
#endif

#elif __TCS || __ETC		/* BY HJH */
    tc_on_flg=1;
#endif

#if __VDC && __BTC		/* BY HJH */
    counter_steer_timer=10000;
    esv_count_lpa=7;
#endif

  #if defined(__DURABILITY_TEST_ENABLE)
	ResetCtlNumCnts();
  #endif
}

void LFC_initial_parameter_set(void)
{

	FL.REAR_WHEEL=0; FL.LEFT_WHEEL=1;
	FR.REAR_WHEEL=0; FR.LEFT_WHEEL=0;
	RL.REAR_WHEEL=1; RL.LEFT_WHEEL=1;
	RR.REAR_WHEEL=1; RR.LEFT_WHEEL=0;

#if __REAR_D
	FL.DRIVEN_WHEEL=0; FR.DRIVEN_WHEEL=0;
	RL.DRIVEN_WHEEL=1; RR.DRIVEN_WHEEL=1;
#else
	FL.DRIVEN_WHEEL=1; FR.DRIVEN_WHEEL=1;
	RL.DRIVEN_WHEEL=0; RR.DRIVEN_WHEEL=0;
#endif

#if __VDC
    LFC_initial_parameter_set_wl(&FL);
    LFC_initial_parameter_set_wl(&FR);
    LFC_initial_parameter_set_wl(&RL);
    LFC_initial_parameter_set_wl(&RR);

#if __CHANGE_MU
    LFC_Split_flag=0;
    LFC_Split_suspect_flag=0;
    LFC_H_to_Split_flag=0;
    LFC_L_to_Split_flag=0;
    LFC_GMA_Split_flag=0;
    MSL_SPLIT_CLR=0;
#endif

    LFC_mot_drv_cnt=0;
    LFC_mot_pwm_ok=0;

    num=0;
    scan_counter=0;
    Rough_road_arad_OK=0;
    Rough_road_detect_vehicle=0;
    Rough_road_suspect_vehicle=0;
    Rough_road_detector_counter=0;
    Rough_road_detector_counter2=0;

    MOTOR_FORCE_OFF_VEHICLE=0;

    Double_suspect=0;

#if __PARKING_BRAKE_OPERATION
    PARKING_BRAKE_OPERATION=0;
#endif
#if __UCC_COOPERATION_CONTROL
    UCC_DETECT_BUMP_Vehicle_flag=0;
#endif
#endif

  #if __CBC
	lccbcRL.u1CbcRearWL = 1;
	lccbcRR.u1CbcRearWL = 1;
	lccbcRL.u1CbcLeftWL = 1;
	lccbcRR.u1CbcLeftWL = 0;
	
    #if __FRONT_CBC_ENABLE	
	lccbcFL.u1CbcRearWL = 0;
	lccbcFR.u1CbcRearWL = 0;
	lccbcFL.u1CbcLeftWL = 1;
	lccbcFR.u1CbcLeftWL = 0;
    #endif
  #endif
}


void LFC_initial_parameter_set_wl(struct W_STRUCT *WL_temp)
/* LFC initial parameter set*/
{
    int8_t K;
    WL = WL_temp;

    WL->LFC_dump_counter=0;
    WL->LFC_dump_counter_old=0;
    WL->LFC_unstable_hold_counter=0;

    WL->LFC_zyklus_z=0;                                       /* added at 2002.winter */
    WL->LFC_built_reapply_counter=0;
    WL->LFC_built_reapply_counter_old=0;
    WL->LFC_built_reapply_counter_delta=0;
    WL->LFC_built_reapply_flag=0;
    WL->LFC_built_reapply_flag_old=0;
    WL->LFC_s0_reapply_counter=0;
    WL->LFC_s0_reapply_counter_old=0;
    WL->LFC_s0_reapply_counter_old2=0;
    WL->LFC_reapply_end_duty=0;
    WL->LFC_reapply_current_duty=0;
  #if	__VDC && __MP_COMP
    WL->LFC_reapply_current_duty1=0;
    WL->LFC_reapply_current_duty2=0;
  #endif
    WL->LFC_fictitious_cycle_flag=0;
    WL->LFC_fictitious_cycle_flag2=0;
/*    WL->Rough_disable_cycle_counter=0; */

    WL->Reapply_Accel_flag=0;                   /* Reapply Accel 020619 */
    WL->LFC_initial_deep_slip_comp_flag=0;      /* Initial Deep Slip Duty Compensation 020619 */
    WL->LFC_n_th_deep_slip_comp_flag=0;         /* n_th Deep Slip Duty Compensation 020619 */
    WL->LFC_n_th_deep_slip_pre_flag=0;
    WL->LFC_rough_road_disable_comp_flag=0; /* Flag to consider Rough Road Condition. */
    WL->Check_double_brake_flag=0;              /* Double_braking 030113 */
    WL->Check_double_brake_flag2=0;             /* Double_braking 030113 */

    WL->LFC_big_rise_and_dump_flag=0;

    WL->LFC_pres_rise_delay_comp=0;
    WL->LFC_special_comp_duty = 0;              /* Special Road Situation 고려 Duty Compensation */

  #if __BUMP_SUSPECT
  	WL->LFC_Bump_Suspect_flag = 0;
  #endif

    WL->Rough_road_counter=0;
    WL->Rough_road_counter2=0;
    WL->Rough_road_counter3=0;
    WL->Rough_road_counter4=0;
    WL->Rough_road_counter5=0;
    WL->Rough_road_counter_arad=0;
    WL->Rough_road_detector=0;
    WL->Rough_road_detector2=0;
    WL->Rough_road_detector3=0;
    WL->Rough_road_detector4=0;
    WL->Rough_road_detector5=0;
    WL->Rough_road_detector_arad=0;
    WL->Rough_road_detect_flag0=0;
    WL->Rough_road_detect_flag1=0;
    WL->Rough_road_detect_flag2=0;
    WL->peak_acc_rough=0;
    WL->vrad_max_old=0;
    WL->diff_max=0;
    WL->sign_old=0;
    for(K=0 ; K<7 ; K++){
        WL->Rough_road_counter_sum[K]=0;
        WL->Rough_road_counter_sum2[K]=0;
        WL->Rough_road_counter_sum3[K]=0;
        WL->Rough_road_counter_sum4[K]=0;
        WL->Rough_road_counter_sum5[K]=0;
        WL->Rough_road_counter_arad_sum[K]=0;
    }

    WL->In_gear_vibration_counter = 0;


    WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
    WL->LFC_pres_rise_comp_disable_flag=0;
    WL->LFC_special_comp_disable_flag=0;

#if __VDC
    WL->LFC_to_ESP_flag = 0;
    WL->ESP_end_counter = 0;
    WL->ESP_to_LFC_hold_timer = 0;
    WL->CROSS_SLIP_CONTROL = 0;
    WL->ESP_partial_dump_counter = 0;
    WL->Forced_n_th_deep_slip_comp_flag = 0;
  #if __ESP_ABS_COOPERATION_CONTROL
    WL->ESP_ABS_SLIP_CONTROL = 0;
    if(REAR_WHEEL_wl==1){
        WL->ESP_ABS_Over_Rear_Inside = 0;
        WL->ESP_ABS_Over_Rear_Outside = 0;
        WL->ESP_ABS_Under_Rear_Outside = 0;
        WL->ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;
    }
    else{
        WL->ESP_ABS_Over_Front_Inside = 0;
        WL->ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;
    }
  #endif

#endif

    WL->motor_force_off_flag=0;
#if __PREVENT_SUTTLE_NOISE
#if __BTC && !__ESV
    WL->MSC_continue_no_dump_counter=0;
#endif
#endif

#if __VDC && __MP_COMP
    WL->LFC_MP_Cyclic_comp_duty = 0;
    WL->MP_Init = MPRESS_150BAR;
#endif
#if __PARKING_BRAKE_OPERATION
    WL->PARKING_BRAKE_WHEEL=0;
    WL->PARKING_DECEL_CHECK=0;
    WL->Wheel_Big_Slip_Counter=0;
#endif
#if __UCC_COOPERATION_CONTROL
	WL->UCC_DETECT_BUMP_flag=0;
	WL->UCC_DETECT_PotHole_flag=0;
	WL->ABS_PotHole_Stable_flg1=0;
	WL->ABS_PotHole_Stable_flg2=0;
#endif

}

void abs_off(void)
{
    INSIDE_ABS_fz=0; ABS_fz=0;
    #if __ECU==ABS_ECU_1
    lespu1TCS_ABS_ACT = 0;
    #endif
	ABS_fl=0; ABS_fr=0; ABS_rl=0; ABS_rr=0;

    #if __BTC
    if(BTC_fz==0)
    {
    #endif
        BUILT_fl=BUILT_fr=HV_VL_fl=HV_VL_fr=AV_VL_fl=AV_VL_fr=0;
        if(EBD_RA==0)
        {
        	BUILT_rl=BUILT_rr=HV_VL_rl=HV_VL_rr=AV_VL_rl=AV_VL_rr=0;
        }
        else
        {
        	;
        }
		FL.state=0; FR.state=0; RL.state=0; RR.state=0;
    #if __BTC
    }
    else
    {
    	;
    }
    #endif
  
  #if __CBC
    LCCBC_vResetControl();
  #endif

  #if __PAC
    LCPAC_vResetControl();
  #endif  
}


/*=================================================================================
    함수  void ebd_off(void)
  =================================================================================
    설명: ebd 기능 off 함수.

  =================================================================================*/
void ebd_off(void)
{
    EBD_RA=0; EBD_rl=0; EBD_rr=0;
    abs_off();
    #if __BTC
        btc_off();
    #endif
    #if __TCS
        LCTCS_vTurnOffFTCS();
    #endif
    #if __VDC
        vdc_off();
    #endif
    ADVANCED_MSC_ON_FLG =0;
  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
    lau8MscDuty=0;
  #endif
    
    #if __EDC
    EDC_ON = 0;
    #endif
}


#if __BTC
/*=================================================================================
    함수  void btc_off(void)
  =================================================================================
    설명: btcs 기능 off 함수.

  =================================================================================*/
void btc_off(void)
{
    BTC_fz=0;
    BTCS_ON=0;

    BTCS_fl=0;
    BTCS_fr=0;
    BTCS_rl=0;
    BTCS_rr=0;

    TCL_DEMAND_fl=0;
    TCL_DEMAND_fr=0;
    TCL_DEMAND_rl=0;
    TCL_DEMAND_rr=0;

    if(ABS_fz==0)
    {
        BUILT_fl=BUILT_fr=HV_VL_fl=HV_VL_fr=AV_VL_fl=AV_VL_fr=0;
        if(EBD_RA==0)
        {
        	BUILT_rl=BUILT_rr=HV_VL_rl=HV_VL_rr=AV_VL_rl=AV_VL_rr=0;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
}
#endif

#if __VDC
void vdc_off(void)
{
    FLAG_ACTIVE_BRAKING = 0;   /* '09 SWD : Diag Problem as a use in FS */

    VDC_MOTOR_ON=0;
    S_VALVE_RIGHT=0;
    S_VALVE_LEFT=0;
    PC_VALVE_RIGHT=0;
    PC_VALVE_LEFT=0;

    YAW_CDC_WORK = 0;
    ESP_TCS_ON = 0;

    SLIP_CONTROL_NEED_FL = 0;
    SLIP_CONTROL_NEED_FR = 0;
    SLIP_CONTROL_NEED_RL = 0;
    SLIP_CONTROL_NEED_RR = 0;
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    FL_ESC.SLIP_CONTROL_NEED = 0;
    FR_ESC.SLIP_CONTROL_NEED = 0;
    RL_ESC.SLIP_CONTROL_NEED = 0;
    RR_ESC.SLIP_CONTROL_NEED = 0;
#endif

    SLIP_CONTROL_NEED_FL_old = 0;
    SLIP_CONTROL_NEED_FR_old = 0;
    SLIP_CONTROL_NEED_RL_old = 0;
    SLIP_CONTROL_NEED_RR_old = 0;

    ESP_BRAKE_CONTROL_FL = 0;
    ESP_BRAKE_CONTROL_FR = 0;
    ESP_BRAKE_CONTROL_RL = 0;
    ESP_BRAKE_CONTROL_RR = 0;

  #if __HRB
    LCHRB_vResetCtrl();
  #endif
 
  #if __HSA
    HSA_reset();
  #endif
   
    PBA_ON=0;
    PBA_WORK=0;
    PBA_ACT = 0; 
    PBA_ON_old = 0;
    PBA_EXIT_CONTROL = 0;
    PBA_EXIT_CONTROL_cnt = 0;
    PBA_MSC_MOTOR_ON = 0;
    PBA_pulsedown = 0; 
 
  #if __TSP
    LCTSP_vCallInitialize();
  #endif
   
    #if __FBC
    	FBC_ON=0;
    	FBC_WORK=0;
    	FBC_MSC_MOTOR_ON = 0;
    #endif 
    
    #if __SLS 
    
      SLS_ACTIVE_rl =0 ; 
      SLS_ACTIVE_rr =0 ;       
      SLS_ON=0; 
      
      SLS_FADE_OUT_rr=0; 
      SLS_FADE_OUT_rl=0;

    #endif   
     
  #if __AVH
    LCAVH_vControlAvhAbnormalMode();
  #endif
  
  #if __EPB_INTERFACE
    LCEPB_vResetCtrlVariable();
    lcu8epbLdmstate=1; /* 1:ESP slave, Dynamic Braking By EPB */
  #endif
  
}


void L_vCallMainIgnOffMode(void)
{
	if(lcIgnOffModeFlg==0)
	{
		if(wu8IgnStat==CE_OFF_STATE)
		{
			lcIgnOffModeFlg = 1;
			ebd_off();
		}
		else
		{
			;
		}
	}
	else
	{
		if(wu8IgnStat==CE_ON_STATE)
		{
			lcIgnOffModeFlg = 0;
		}
		else
		{
			;
		}
	}
	
	if(lcIgnOffModeFlg==1)
	{
/*		LS_vCallMainSensorSignal();

		LD_vCallMainDetection();

	  	LC_vCallMainControl();
*/
	  	LA_vCallMainValveIgnOffMode();

/*		LA_vCallMainCan(); */
	}
	else
	{
		ebd_off();
	}
}

#endif


void u16mulu16divu16(void)
{
	/******************************
    *  tempUW3 : unsigned integer *
    *  tempUW2 : unsigned integer *
    *  tempUW1 : unsigned integer *
    *  tempUW0 : unsigned integer *
	******************************/
     tempUW3 = (uint16_t)((uint32_t)tempUW2*tempUW1/tempUW0) ;
}

void s16muls16divs16(void)
{
	/******************************
    *  tempW3 : signed integer    *
    *  tempW2 : signed integer    *
    *  tempW1 : signed integer    *
    *  tempW0 : signed integer    *
	******************************/
    tempW3 = (int16_t)((int32_t)tempW2*tempW1/tempW0);
}

void    u16mul16div16(void)
{
	/******************************
    *  tempW2 : unsigned integer *
    *  tempW1 : unsigned integer *
    *  tempW0 : unsigned integer *
	******************************/
	tempW3 = (uint16_t)((uint32_t)(uint16_t)tempW2*(uint16_t)tempW1/(uint16_t)tempW0);
}

void	s16mul16div16(void)
{
    // tempW2 : signed integer
    // tempW1 : unsigned integer
    // tempW0 : unsigned integer

	//tempW3 = (int16_t)(((int32_t)(tempW2*(uint16_t)tempW1))/(uint16_t)tempW0);
	tempW3 = (int16_t)((int32_t)tempW2*(uint16_t)tempW1/(uint16_t)tempW0);

}

void	u16mul16div(void)
{
    // tempW2 : unsigned integer
    // tempW1 : unsigned integer

	//tempW3 = (uint16_t)(((uint32_t)tempW2*(uint16_t)tempW1)/256);
	tempW3 = (uint16_t)((uint32_t)(uint16_t)tempW2*(uint16_t)tempW1/256);
}

void	s16mul16div(void)
{
    // tempW2 : signed integer
    // tempW1 : unsigned integer

//	tempW3 = (int16_t)(((int32_t)tempW2*(uint16_t)tempW1)/256);
	tempW3 = (int16_t)((int32_t)tempW2*(uint16_t)tempW1/256);
}

#if defined(__DURABILITY_TEST_ENABLE)
void ResetCtlNumCnts(void)
{
	ESPCtlNumCnt = 0;
	TCSCtlNumCnt = 0;
	ABSCtlNumCnt = 0;
	HSACtlNumCnt = 0;
	HDCCtlCnt = 0;
	CBSCtlCnt = 0;
}

void UpdateCtlFlagsForEnduranceTest(void)
{
#if __ECU==ESP_ECU_1
	ESP_ON_OLD_FOR_ET = ESP_ON;
	TCS_ON_OLD_FOR_ET = (FTCS_ON|ETCS_ON|BTCS_ON);
#endif	
	ABS_ON_OLD_FOR_ET = ABS_fz;
  #if (__HSA == ENABLE)
	HSA_ON_OLD_FOR_ET = HSA_flg;
  #endif
    CBS_ON_OLD_FOR_ET = BLS; 
  
}

void CntREdgeCtlFlagsForEnduranceTest(void)
{
	/* Rising Edge Detection */
#if __ECU==ESP_ECU_1
	if( (ESP_ON_OLD_FOR_ET==0)&&(ESP_ON==1) )
	{
		if (ESPCtlNumCnt<65530)
		{
			ESPCtlNumCnt++;
		}
		else
		{
			ESPCtlNumCnt=65530;
		}
	}
	else {;}


	if( (TCS_ON_OLD_FOR_ET==0)&&((FTCS_ON|ETCS_ON|BTCS_ON)) )
	{
		if (TCSCtlNumCnt<65530)
		{
			TCSCtlNumCnt++;
		}
		else
		{
			TCSCtlNumCnt=65530;
		}
	}
	else {;}
#endif

	if( (ABS_ON_OLD_FOR_ET==0)&&(ABS_fz==1) )
	{
		if (ABSCtlNumCnt<65530)
		{
			ABSCtlNumCnt++;
		}
		else
		{
			ABSCtlNumCnt=65530;
		}
	}
	else {;}

  #if (__HSA == ENABLE)
	if( (HSA_ON_OLD_FOR_ET==0)&&(HSA_flg==1) )
	{
		if (HSACtlNumCnt<65530)
		{
			HSACtlNumCnt++;
		}
		else
		{
			HSACtlNumCnt=65530;
		}
	}
	else {;}
  #endif

  #if (__HDC == ENABLE)
	if(lcu1HdcActiveFlg==1)
	{
		if (HDCCtlCnt<4294967290)
		{
			HDCCtlCnt++;
		}
		else
		{
			HDCCtlCnt=4294967290;
		}
	}
	else {;}
  #endif
  
	/* Rising Edge Detection */
	if( (CBS_ON_OLD_FOR_ET==0)&&(BLS==1) )
	{
		if (CBSCtlCnt<4294967290)
		{
			CBSCtlCnt++;
		}
		else
		{
			CBSCtlCnt=4294967290;
		}
	}
	else {;}
  
}
#endif	/* #if defined(__DURABILITY_TEST_ENABLE) */
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCallMain
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
