#ifndef __LACALLMAIN_H__
#define __LACALLMAIN_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

#if  __DV_LEVEL==PREMIUM_DV2
#define     __ADDITIONAL_ESV_VALVES    0
#endif

#define     __MGH_80_10MS              ENABLE

/*
#define		TC_FULL_ON_DUTY		225
#define		SV_FULL_ON_DUTY		255
#define		NO_FULL_ON_DUTY		255
#define		NC_FULL_ON_DUTY		1
#define		VALVE_OFF_DUTY		0
*/

#define		TC_FULL_ON_DUTY_8BIT		225
#define		SV_FULL_ON_DUTY_8BIT		255
  #if __DV_LEVEL==PREMIUM_DV2 
#define		SV_FULL_ON_DUTY_8BIT		255 /* For Additional 2 ESV in 14VV */
  #endif

#define		NO_FULL_ON_DUTY_8BIT		255
  #if __NC_CURRENT_CONTROL
#define		NC_FULL_ON_DUTY_8BIT		255
  #endif

#define		TC_FULL_ON_DUTY_16BIT		1000
#define		SV_FULL_ON_DUTY_16BIT		1000
#define		NO_FULL_ON_DUTY_16BIT		1000
#define		NC_FULL_ON_DUTY_16BIT		1000

#define		DUTY_RESOL_CONVERSION       4 /* 8bit to 16bit */

#define		TC_FULL_ON_DUTY		TC_FULL_ON_DUTY_16BIT
#define		SV_FULL_ON_DUTY		SV_FULL_ON_DUTY_16BIT
#define		NO_FULL_ON_DUTY		NO_FULL_ON_DUTY_16BIT
  #if __NC_CURRENT_CONTROL
#define		NC_FULL_ON_DUTY		NC_FULL_ON_DUTY_16BIT
  #endif
#define		VALVE_OFF_DUTY		0

#define __DITHER_PATTERN        __1MS_DITHER
#define __1MS_DITHER            1
#define __2MS_DITHER            2
#define U16_PWM_DITHER_DUTY     5 /*3*/

  #if __MGH_80_10MS == DISABLE
#define U8_BASE_LOOPTIME       5 /*scan*/
#define U8_BASE_CTRLTIME       5 /*scan*/
  #else
#define U8_BASE_LOOPTIME       10 /*scan*/
#define U8_BASE_CTRLTIME       5 /*scan*/
  #endif /*__MGH_80_10MS == DISABLE*/

#if   (__DITHER_PATTERN == __1MS_DITHER)
#define U8_DITHER_PERIOD        2
#elif (__DITHER_PATTERN == __2MS_DITHER)
#define U8_DITHER_PERIOD        4
#endif

/*Global MACRO FUNCTION Definition *********************************************/


#define __ACTUATION_DEBUG

/*Global Type Declaration ******************************************************/
typedef struct
{
	uint16_t lau16PwmDuty[U8_BASE_LOOPTIME];

}SOL_LOGIC_DUTY_t;

typedef struct
{
	uint8_t u8PwmDuty;
	uint8_t u8InletOnTime;
	uint8_t u8OutletOnTime;
  #if defined (__ACTUATION_DEBUG)
	uint8_t u8debugger;
  #endif /* defined (__ACTUATION_DEBUG) */

}WHEEL_VV_OUTPUT_t;

typedef struct
{
    uint16_t LA_BIT_07     :1;
    uint16_t LA_BIT_06     :1;
    uint16_t LA_BIT_05     :1;
    uint16_t LA_BIT_04     :1;
    uint16_t LA_BIT_03     :1;
    uint16_t LA_BIT_02     :1;
    uint16_t LA_BIT_01     :1;
    uint16_t LA_BIT_00     :1;
}LA_FLAG_t;

  #if __ADDITIONAL_ESV_VALVES
#define RS_VALVE_PRIMARY    laValveFlags0.LA_BIT_07
#define RS_VALVE_SECONDARY  laValveFlags0.LA_BIT_06
  #endif


/*Global Extern Variable Declaration *******************************************/
extern SOL_LOGIC_DUTY_t la_FLNO,la_FRNO,la_RLNO,la_RRNO;
extern SOL_LOGIC_DUTY_t la_FLNC,la_FRNC,la_RLNC,la_RRNC;
extern SOL_LOGIC_DUTY_t la_PTC,la_STC,la_PSV,la_SSV;
extern WHEEL_VV_OUTPUT_t la_FL1HP, la_FL2HP, la_FR1HP, la_FR2HP;
extern WHEEL_VV_OUTPUT_t la_RL1HP, la_RL2HP, la_RR1HP, la_RR2HP;
#if __DV_LEVEL==PREMIUM_DV2
extern SOL_LOGIC_DUTY_t la_SRSV,la_PRSV;
#endif

#if __VDC
extern WHEEL_VV_OUTPUT_t laesc_FL1HP,laesc_FL2HP,laesc_FR1HP,laesc_FR2HP;
extern WHEEL_VV_OUTPUT_t laesc_RL1HP,laesc_RL2HP,laesc_RR1HP,laesc_RR2HP;

extern WHEEL_VV_OUTPUT_t laepc_FL1HP,laepc_FL2HP,laepc_FR1HP,laepc_FR2HP;
extern WHEEL_VV_OUTPUT_t laepc_RL1HP,laepc_RL2HP,laepc_RR1HP,laepc_RR2HP;
#endif

#if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F || __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
extern WHEEL_VV_OUTPUT_t lavalvetest_FL1HP,lavalvetest_FL2HP,lavalvetest_FR1HP,lavalvetest_FR2HP;
extern WHEEL_VV_OUTPUT_t lavalvetest_RL1HP,lavalvetest_RL2HP,lavalvetest_RR1HP,lavalvetest_RR2HP;
#endif

#if __TVBB
extern WHEEL_VV_OUTPUT_t latvbb_FL1HP,latvbb_FL2HP,latvbb_FR1HP,latvbb_FR2HP;
extern WHEEL_VV_OUTPUT_t latvbb_RL1HP,latvbb_RL2HP,latvbb_RR1HP,latvbb_RR2HP;
#endif

#if __TSP
extern WHEEL_VV_OUTPUT_t latsp_FL1HP,latsp_FL2HP,latsp_FR1HP,latsp_FR2HP;
extern WHEEL_VV_OUTPUT_t latsp_RL1HP,latsp_RL2HP,latsp_RR1HP,latsp_RR2HP;
#endif

/*
extern uint8_t la_FLNC,la_FRNC,la_RLNC,la_RRNC;
*/
extern LA_FLAG_t laValveFlags0;

/* 
//idb : Stroke Recovery
extern uint8_t	laidbu8StrkRcv;
extern uint8_t	laidbu8StrkRcvOld;
*/

/*Global Extern Functions Declaration ******************************************/
extern void LA_vCallMainValve(void);
extern void LA_vCallMainCan(void);
extern void LA_vAllocCircuitValveDutyArray(void);
#if __VDC
extern void LA_vCallMainValveIgnOffMode(void);
#endif
extern void LA_vSetNominalWheelValvePwmDuty(uint8_t u8InletAct, uint8_t u8OutletAct, uint8_t u8InletActMs, uint8_t u8OutletActMs, WHEEL_VV_OUTPUT_t *laWL);
extern void LA_vResetWheelValvePwmDuty(WHEEL_VV_OUTPUT_t *laWL);
extern void LA_vSetWheelVvOnOffRiseOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8NoReapplyTime);
extern void LA_vSetWheelVvOnOffDumpOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8NcDumpTime);
extern void LA_vSetWheelVvOnOffHoldOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP);
extern void LA_vSetWheelVvOnOffCircOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP);
extern void LA_vSetWheelVvHold_Forcing(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP);
#endif
