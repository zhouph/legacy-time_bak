
#include "Abc_Ctrl.h"
#if defined(__UCC_1)

 


int16_t cdc_wz_fl_raw=0, cdc_wz_fr_raw=0;
int16_t cdc_wz_fl_raw_old=0, cdc_wz_fr_raw_old=0;
int16_t cdc_wz_fl_HF=0, cdc_wz_fr_HF=0;
int16_t cdc_wz_fl_HF_old=0, cdc_wz_fr_HF_old=0;
int16_t cdc_wz_fl=0, cdc_wz_fr=0;
int16_t cdc_wz_fl_old=0, cdc_wz_fr_old=0;

int16_t cdc_bz_fl_raw=0, cdc_bz_fr_raw=0, cdc_bz_rr_raw=0;
int16_t cdc_bz_fl_raw_old=0, cdc_bz_fr_raw_old=0, cdc_bz_rr_raw_old=0;
int16_t cdc_bz_fl_HF=0, cdc_bz_fr_HF=0, cdc_bz_rr_HF=0;
int16_t cdc_bz_fl_HF_old=0, cdc_bz_fr_HF_old=0, cdc_bz_rr_HF_old=0;
int16_t cdc_bz_fl=0, cdc_bz_fr=0, cdc_bz_rr=0;
int16_t cdc_bz_fl_old=0, cdc_bz_fr_old=0, cdc_bz_rr_old=0;

int16_t cdc_bump_counter_fl=0;
int16_t cdc_bump_counter_fr=0;
int16_t cdc_PotHole_counter_fl=0;
int16_t cdc_PotHole_counter_fr=0;

int16_t override_angle=0, override_angle_old=0;
int16_t override_assist=0,override_assist_old=0;
int16_t wstr_org=0;

// o_delta_yaw_thres for CDC control
int16_t cdc_o_delta_yaw_thres[4];
int16_t cdc_u_delta_yaw_thres[4];

// delta_V & LFC rate for normal force
int16_t vfiltn_normal_load_front;
int16_t vfiltn_normal_load_rear;
int16_t LFC_gain_normal_load_front;
int16_t LFC_gain_normal_load_rear;
int16_t cdc_pitch_index;
int16_t Lateral_Force_Sign;
int16_t Delta_Yaw_Damping_Force_Sign;
int16_t delta_yaw_damping;
int16_t delta_yaw_damping1;

int16_t lrswBodyvAccel[4];
int16_t lrswCorrectBodyvAccel[5];
int16_t lrswCorrectBodyvAccelU[5];
int16_t lrswCorrectBodyvAccel_1[5];
int16_t lrswCorrectBodyvAccelHF[5];
int16_t lrswCorrectBodyvAccelHF_1[5];
int16_t lrswBodyvVel[5];
int16_t lrswBodyvVelU[5];
int16_t lrswBodyvVelU_1[5];
int16_t lrswBodyvVelHF[5];
int16_t lrswBodyvVelHF_1[5];
int16_t lrswModalVelPitch;
int16_t luswUk;
int16_t luswYk1;
int32_t luslTmp[4];
int16_t luswYk;

int8_t lrscCoefCorrectAccel[3][4] = { { 0x40,  0x0D,  0x00, -0x39},
                                    { 0x06,  0x39,  0x09,  0x42},
                                    {-0x06, -0x06,  0x37,  0x37} };

int16_t wstr_10deg;                                    
int16_t vrefk_10kph;
int16_t loswRolA;
unsigned int16_t louwRolA_abs;
unsigned int16_t alat_abs;
unsigned int8_t loucFlagRoll;
unsigned int8_t lducFlagDive;
unsigned int8_t lsucFlagSquat;
unsigned int8_t ljucRoadCond;

int16_t cdc_q;
int16_t yaw_control_UCC_P_gain_front, yaw_control_UCC_D_gain_front, ucc_moment_q_front;

int16_t afs_p_gain, afs_d_gain, afs_i_gain, afs_control_input;
int16_t delta_yaw_error=0, delta_yaw_integrate, delta_yaw_integrate_old=0, delta_yaw_diff;
int16_t delta_yaw_error_buf[4];

 

#endif
