/******************************************************************************
* Project Name: ELECTRONIC BRAKE PREFILL
* File: LCEBPCallControl.C
* Description:
* Date: June. 4. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCEBPCallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/


#if __EBP
#include "LCEBPCallControl.h"
#include "LDEBPCallDetection.h"
#endif

#if __EBP
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/
struct	U8_BIT_STRUCT_t EBPC0,EBPC1;
     #if __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
int16_t lcebps16CtrlTargetPress;
     #endif /* __AHB_GEN3_SYSTEM == ENABLE */	


/* LocalFunction prototype ***************************************************/
void 	LCEBP_vCallControl(void);
void    LCEBP_vCallClearValveMotor(void);
void    LCEBP_vCallControlValve(void);
void    LCEBP_vCallControlMotor(void);

/* GlobalFunction prototype ***************************************************/



/* Implementation*************************************************************/


void 	LCEBP_vCallControl(void)
{
    /*
    On;Off Time set
    Max On Time SET  (500msec이하 - 전체 작동구간)
    state 1 Time set (Full Rise구간)
    */
    esp_tempW8 = L_U8_TIME_5MSLOOP_300MS;
    esp_tempW7 = L_U8_TIME_5MSLOOP_50MS;

    if (EBP_ENABLE)
    {
        if (EBP_ENABLE_cnt<esp_tempW8)
        {
            EBP_ON=1;
            if(EBP_ENABLE_cnt<esp_tempW7)
            {
                EBP_STATE_FLG=0;
            }
            else
            {
                EBP_STATE_FLG=1;
            }
          #if __SPLIT_TYPE      /* FR Split */
            if (EBP_ENABLE_cnt%esp_tempW7 >= 2) /* 5,6  */
            {
                EBP_STATE_FLG2=1;
            }
            else
            {
                EBP_STATE_FLG2=0;
            }
          #endif
        }
        else
        {
            EBP_ON=0;
            EBP_STATE_FLG=0;
            EBP_STATE_FLG2=0;
        }
    }
    else
    {
        EBP_STATE_FLG=0;
        EBP_STATE_FLG2=0;
        EBP_ON=0;
    }

    LCEBP_vCallClearValveMotor();

    LCEBP_vCallControlValve();

    LCEBP_vCallControlMotor();

}

void LCEBP_vCallClearValveMotor(void)
{
    EBP_HV_VL_fl=0;
    EBP_AV_VL_fl=0;
    EBP_HV_VL_fr=0;
    EBP_AV_VL_fr=0;
    EBP_HV_VL_rl=0;
    EBP_AV_VL_rl=0;
    EBP_HV_VL_rr=0;
    EBP_AV_VL_rr=0;

    EBP_S_VALVE_PRIMARY 	=0;
    EBP_S_VALVE_SECONDARY	=0;
    EBP_TCL_DEMAND_PRIMARY  =0;
    EBP_TCL_DEMAND_SECONDARY=0;

    EBP_MOTOR_ON=0;
}

void LCEBP_vCallControlValve(void){

    if (EBP_ON)
    {
     #if __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
        if(lcebps16CtrlTargetPress < MPRESS_3BAR)
        {
	        lcebps16CtrlTargetPress = lcebps16CtrlTargetPress + MPRESS_0G5BAR;
	    }
	    else
	    {
	    	;
	    }
     #else	
        EBP_S_VALVE_PRIMARY 	=1;
        EBP_S_VALVE_SECONDARY	=1;
        EBP_TCL_DEMAND_PRIMARY  =1;
        EBP_TCL_DEMAND_SECONDARY=1;

      #if __SPLIT_TYPE      /* FR Split */
        if (EBP_STATE_FLG2)
        {
            EBP_AV_VL_fl=1;
            EBP_AV_VL_fr=1;
            EBP_AV_VL_rl=1;
            EBP_AV_VL_rr=1;
        }
        else
        {
            EBP_AV_VL_fl=0;
            EBP_AV_VL_fr=0;
            EBP_AV_VL_rl=0;
            EBP_AV_VL_rr=0;
        }
      #else                 /* X Split */
        if (EBP_STATE_FLG)
        {
            EBP_AV_VL_rr=1;
            EBP_AV_VL_rl=1;
        }
        else
        {
            EBP_AV_VL_rr=0;
            EBP_AV_VL_rl=0;
        }
      #endif
     #endif /* __AHB_GEN3_SYSTEM == ENABLE */	
    }
    else
    {
    	;
     #if __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
        lcebps16CtrlTargetPress = MPRESS_0BAR;
     #endif /* __AHB_GEN3_SYSTEM == ENABLE */	
    }

}

void LCEBP_vCallControlMotor(void){
/*
초기 Rise 구간 (Full)
Circulation 구간.
*/
    esp_tempW8=L_U8_TIME_5MSLOOP_5MS;
    esp_tempW7=L_U8_TIME_5MSLOOP_70MS;

    if (EBP_ON)
    {
        if(((!EBP_STATE_FLG)&&(EBP_ENABLE_cnt%esp_tempW8)==0)
            ||((EBP_STATE_FLG)&&(EBP_ENABLE_cnt%esp_tempW7)==0))
        {
            EBP_MOTOR_ON=1;
        }
        else
        {
            EBP_MOTOR_ON=0;
        }
    }
    else
    {
        EBP_MOTOR_ON=0;
    }
}

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCEBPCallControl
	#include "Mdyn_autosar.h"
#endif    