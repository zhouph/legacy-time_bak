#ifndef __LADECCALLACTCAN_H__
#define __LADECCALLACTCAN_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition**************************/

/*Global MACRO FUNCTION Definition**************************/

/*Global Type Declaration **********************************/

/*Global Extern Variable  Declaration***********************/
  #if __DEC
/********** From */
extern uint8_t	Actual_DECEL_G;
  #endif

/*Global Extern Functions  Declaration**********************/
  #if __DEC
extern void LADEC_vCallActCan(void);
  #endif
#endif
