/* Includes ********************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDAFSCallDetection
	#include "Mdyn_autosar.h"
#endif

#include "LVarHead.h"
/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/

#if defined(__UCC_1)
void LDAFS_vCallDetection(void);
#endif

/* Implementation***************************************************/

#if defined(__UCC_1)

void LDAFS_vCallDetection(void)
{

}

#endif		
	
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDAFSCallDetection
	#include "Mdyn_autosar.h"
#endif
