/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LAABSCallActCan.C
* Description: CAN Handling for ABS control
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAABSCallActCan
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LAABSCallActCan.h"
#include "LCABSDecideCtrlState.h"

#include "hm_logic_var.h"
#include "LDRTACallDetection.h"


/* Local Definiton  **********************************************************/


/* Variables Definition*******************************************************/

ABS_CAN_FLAG_t ABSCANBIT01;
uint8_t laabsu8SpareTireStatus;
/* LocalFunction prototype ***************************************************/
	#if (GMLAN_ENABLE==ENABLE)
void LAABS_vConvertABS2EMSForGM(void);
	#endif
/* GlobalFunction prototype **************************************************/


/* Implementation*************************************************************/


/******************************************************************************
* FUNCTION NAME:      LAABS_vCallActCan
* CALLED BY:          LA_vCallMainCan()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        CAN Handling Function for ABS control
******************************************************************************/
void LAABS_vCallActCan(void)
{
	if((fu1OnDiag==1)||(init_end_flg==0)||
	   (fu1ECUHwErrDet==1)||(fu1RearSolErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1SameSideWSSErrDet==1)
      #if (__GM_FailM==0)
       ||(fu1DiagonalWSSErrDet==1)
      #endif
	) /* ebd_error_flg */
	{
		lespu1TCS_ABS_ACT = 0;                              /* reset ABS_ACT*/		
		laabsu1TrnsBrkSysCltchRelRqd = 0;
	  #if (GMLAN_ENABLE==ENABLE)
		lespu1ABSAct = 0;
		laabsu8SpareTireStatus=0;
	  #endif
	  #if __CAR_MAKER==SSANGYONG	 
		lespu8TCS_EBS_STATUS = 0; 	   
	  #endif
	}
	else
	{
  #if (__ECU==ABS_ECU_1) || (__ECU==ESP_ECU_1 && __TCS==DISABLE )
	    if(laabsu1ABSActCan==1) 
	    {
	        lespu1TCS_ABS_ACT = 1;                              /* set ABS_ACT*/
		  #if (GMLAN_ENABLE==ENABLE)
			lespu1ABSAct = 1;
		  #endif
	  	  #if __CAR_MAKER==SSANGYONG	 
			lespu8TCS_EBS_STATUS = 1; 	   
	  	  #endif
	    }
	    else 
	    {
	        lespu1TCS_ABS_ACT = 0;                              /* reset ABS_ACT*/
	      #if (GMLAN_ENABLE==ENABLE)
	        lespu1ABSAct = 0;
	      #endif
	  	  #if __CAR_MAKER==SSANGYONG	 
			lespu8TCS_EBS_STATUS = 0; 	   
	  	  #endif	      
	    }
  #endif /*(__ECU==ABS_ECU_1) || (__ECU==ESP_ECU_1 && __TCS==DISABLE )*/
		
	  #if (GMLAN_ENABLE==ENABLE)
		/************* Clutch Release Request *************/
		if(laabsu1ABSActCan==1)
    	{
      	#if __VDC
       		if((fu1MCPErrorDet==1)&&(fu1BLSErrDet==1))
        	{
            	laabsu1TrnsBrkSysCltchRelRqd = 1;
        	}
        	else
        	{
            	if(((fu1MCPErrorDet==0)&&(BRAKE_BY_MPRESS_FLG==0))&&((fu1BLSErrDet==0)&&(BLS==0)))
            	{
                	laabsu1TrnsBrkSysCltchRelRqd = 0;
            	}
            	else if((fu1MCPErrorDet==1)&&(fu1BLSErrDet==0)&&(BLS==0))
            	{
                	laabsu1TrnsBrkSysCltchRelRqd = 0;
            	}
            	else if((fu1MCPErrorDet==0)&&(BRAKE_BY_MPRESS_FLG==0)&&(fu1BLSErrDet==1))
            	{
                	laabsu1TrnsBrkSysCltchRelRqd = 0;
            	}
            	else
            	{
                	laabsu1TrnsBrkSysCltchRelRqd = 1;
            	}
        	}
      	#else
        	if((fu1BLSErrDet==0)&&(BLS==0))
        	{
            	laabsu1TrnsBrkSysCltchRelRqd = 0;
        	}
        	else
        	{
            	laabsu1TrnsBrkSysCltchRelRqd = 1;
        	}
      	#endif
    	}
    	else
    	{
        	laabsu1TrnsBrkSysCltchRelRqd = 0;
    	}
    	/********************************************************/
        
    	/************* RTA Status *************/
    	laabsu8SpareTireStatus=ldrtau8RtaState;
    	/**************************************/  
      #endif
	}

	  #if(GMLAN_ENABLE==ENABLE)
	   #if (__ECU==ESP_ECU_1) || (__CAR!=GM_M300)
        LAABS_vConvertABS2EMSForGM();	      
       #endif    
      #endif
        
  #if (__ECU==ABS_ECU_1) || (__ECU==ESP_ECU_1 && __TCS==DISABLE )	
	#if __HMC_CAN_VER==CAN_HD_HEV
	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
        lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
        lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */
        lespu1TCS_TCS_CTL = 0;                      /* reset TCS_CTL		  */
        lespu1TCS_ESP_CTL = 0;
        lesps16TCS_TQI_TCS = 0x1ff*4;                  /* clear TQI_TCS		  */
        lesps16TCS_TQI_SLOW_TCS = 0x1ff*4;             /* clear TQI_SLOW_TCS     */
	#endif
  #endif	  

    if((ABS_fz==1) 
  #if (GMLAN_ENABLE==ENABLE)
    && (ABS_INHIBITION_flag==0)
  #endif
    )
    {
        laabsu1ABSActCan=1;
    }
    else
    {
        laabsu1ABSActCan=0;
    }
}

	#if (GMLAN_ENABLE==ENABLE)
void LAABS_vConvertABS2EMSForGM(void)
{
    /*********** GMLAN ABS Only Setting Parameter ************/
  #if (__ECU==ABS_ECU_1) || (__ECU==ESP_ECU_1 && __TCS==DISABLE )      
      #if (__CAR==GM_T300) || (__CAR==GM_M350) || (__CAR==GM_4NB) || (__CAR==GM_ONIX) || (__CAR==GM_PM7)
    lespu1TCS_TCS_REQ  = 0;
    lespu1_TTDGCDcyCntrlEnbld = 0;
	lespu1TracTrqDecCntAtv = 0;			
	lespu16_TTDGCDcyGrad = 1023;    
    lespu16_TracCntrlMaxTorqIncRt = 1008;
	lespu8_BSTGRReqdGear = 0;			
	lespu8_BSTGRReqType = 0;			
	lespu8TCSysOpMd = 0;
	lespu8VehStabEnhmntMd = 0;
	lespu1TreInfMonSysRstPrfmd = 0;
	lespu1TirePrsLowIO = 0;
	lespu1BrkSysBrkLtsReqd = 0;
	lespu1BrkSysRedBrkTlltlReq = 0;
	lespu8BrkSysVTopSpdLimVal = 255;
	lespu8TracCntrlMaxTorqIncRt = 63;
	lespu8TqIntvnTyp = 0;
	lespu16TorqReqVal = 1696;
	lespu1TCSAct=0;
	lespu1VSEAct = 0;
	lespu1DrvIndpntBrkAppAct = 0;
	lespu1AutoBrkngAct = 0;
	lespu1BrkSysTrnsGrRqTnstnTp = 0;
	
	lespu1ActVehAccelV = 0;
	lesps16ActVehAccel_0 = 0;				
	
      #elif (__CAR==GM_GSUV) 
    lespu1TCS_TCS_REQ  = 0;
    lespu1_TTDGCDcyCntrlEnbld = 0;
	lespu1TracTrqDecCntAtv = 0;			
	lespu16_TTDGCDcyGrad = 1023;    
    lespu16_TracCntrlMaxTorqIncRt = 1008;
	lespu8_BSTGRReqdGear = 0;			
	lespu8_BSTGRReqType = 0;			
	lespu8TCSysOpMd = 0;
	lespu8VehStabEnhmntMd = 0;
	lespu1TreInfMonSysRstPrfmd = 0;
	lespu1TirePrsLowIO = 0;
	lespu1BrkSysBrkLtsReqd = 0;
	lespu1BrkSysRedBrkTlltlReq = 0;
	lespu8BrkSysVTopSpdLimVal = 255;
	lespu8TracCntrlMaxTorqIncRt = 63;
	lespu8TqIntvnTyp = 0;
	lespu16TorqReqVal = 1696;
	lespu1TCSAct=0;
	lespu1VSEAct = 0;
	lespu1DrvIndpntBrkAppAct = 0;
	lespu1AutoBrkngAct = 0;
	lespu1BrkSysTrnsGrRqTnstnTp = 0; 
	   
	lespu1ActVehAccelV = lsabsu8Decelvalidity;
	lesps16ActVehAccel_0 = lsabss16DecelRefiltByVref5;		
      #endif
  #endif     
    /*********** GMLAN Default Setting Parameter ******************/
  	#if (__CAR==GM_T300) || (__CAR==GM_GSUV) || (__CAR==GM_M350) || (__CAR==GM_4NB) || (__CAR==GM_ONIX) || (__CAR==GM_PM7)
	lesps16_BrkTemp = 0;				/* Brake Temperature */
	lespu8_ChsBrkgLoad = 0;				/* Chassis Braking Load */
	lesps8_VehDynOvrUndrStr = 0;        /* Vehicle Dynamics Over Under Steer */
	lespu1_VehDynOvrUndrStrV = 0;		/* Vehicle Dynamics Over Under Steer Validity */
	lespu1_BrakePadWornIndicationOn	=0;
	lespu1_ACCBrakingActive	=0;
	lespu1_AutomaticBrkngFailed	=0;
	lespu1_IBAPrefillRequest =0;
	lesps16ActVehAccel_0 = 0; //RSY temp
  	#elif (__CAR==GM_M300) && (__ECU==ESP_ECU_1)
	lesps16_BrkTemp = 0;				/* Brake Temperature */
	lespu8_ChsBrkgLoad = 0;				/* Chassis Braking Load */
	lesps8_VehDynOvrUndrStr = 0;        /* Vehicle Dynamics Over Under Steer */
	lespu1_VehDynOvrUndrStrV = 0;		/* Vehicle Dynamics Over Under Steer Validity */
	lespu1_BrakePadWornIndicationOn	=0;
	lespu1_ACCBrakingActive	=0;		
  	#endif	
}
	#endif
	
	
/* AUTOSAR --------------------------*/
	#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAABSCallActCan
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
