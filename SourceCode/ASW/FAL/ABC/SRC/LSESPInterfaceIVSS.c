

#include "LVarHead.h"
#include "LCESPInterfaceIVSS.h"
#include "tcs_var.h"
#include "hm_logic_var.h"
//#include "UCC_EPSi/LCEPSCallControl.h"
//#include "UCC_EPSi/LSEPSCallSensorSignal.h"
//#include "SCC/LCSCCCallControl.h"

#if __IVSS

/*****IVSS -> ESC*****/
uint8_t Ivss_ModeECS;
uint8_t Ivss_ModeMDPS;
uint8_t Ivss_ModeTM;
uint8_t Ivss_ModeENG;
uint8_t Ivss_ModeESC;
uint8_t Ivss_Mode4WD;
uint8_t Ivss_AliveCounter;
uint8_t Ivss_CheckSum;
/*********************/

/*****ESC -> IVSS*****/
int16_t Ivss_EMS1_TQI_ACOR;
int16_t Ivss_EMS1_TQI;
int16_t Ivss_EMS1_TQFR;
int16_t Ivss_EMS2_TQ_STND;
int16_t Ivss_EMS2_TPS;
int16_t Ivss_EMS2_PV_AV_CAN;
int16_t Ivss_ESP2_LAT_ACCEL;
int16_t Ivss_ESP2_LONG_ACCEL;
int16_t Ivss_ESP2_CYL_PRES;
int16_t Ivss_ESP2_YAW_RATE;
int16_t Ivss_SAS1_SAS_Angle;
int16_t Ivss_TCS1_TQI_TCS;
int16_t Ivss_TCS5_WHEEL_FL;
int16_t Ivss_TCS5_WHEEL_FR;
int16_t Ivss_TCS5_WHEEL_RL;
int16_t Ivss_TCS5_WHEEL_RR;
int16_t Ivss_TCU2_CUR_GR;
int16_t Ivss_VSM2_CR_Mdps_StrTq;
int16_t Ivss_VSM2_CR_Mdps_OutTq;

uint16_t Ivss_EMS1_N;

uint8_t Ivss_EMS2_MUL_CODE;
uint8_t Ivss_EMS2_BRAKE_ACT;
uint8_t Ivss_VSM2_CF_Mdps_Def;
uint8_t Ivss_SCC3_VSM_Warn;
uint8_t Ivss_Clu_DrivingModeSwi;

uint8_t Ivss_SCC_Enable;
uint8_t Ivss_ESC_Mcu_defect;
uint16_t Ivss_Bat_Volt;
/********************************/

int16_t lccanIVSS_TCU2_CUR_GR;
uint8_t lccanIVSS_EMS2_MUL_CODE;
uint8_t Ivss_ECS1_RoadCond;


void LSESPInterfaceIVSS(void);

void LSESPInterfaceIVSS(void)
{
    Ivss_EMS1_TQI_ACOR      =eng_torq;
    Ivss_EMS1_N             =eng_rpm;
    Ivss_EMS1_TQI           =drive_torq;
    Ivss_EMS1_TQFR          =fric_torq;
    Ivss_EMS2_TQ_STND       =max_torq;
    Ivss_EMS2_MUL_CODE      =lccanIVSS_EMS2_MUL_CODE;
    Ivss_EMS2_BRAKE_ACT     =ccu8BlsActSignal;
    Ivss_EMS2_TPS           =lesps16EMS_TPS;
    Ivss_EMS2_PV_AV_CAN     =lesps16EMS_PV_AV_CAN;
    Ivss_ESP2_LAT_ACCEL     =a_lat_1_1000g;
    Ivss_ESP2_LONG_ACCEL    =a_long_1_1000g;
    Ivss_ESP2_CYL_PRES      =pos_mc_1_100bar;
    Ivss_ESP2_YAW_RATE      =yaw_1_100deg;
    Ivss_SAS1_SAS_Angle     =steer_1_10deg;
    Ivss_TCS1_TCS_REQ       =lespu1TCS_TCS_REQ;
    Ivss_TCS1_TCS_CTL       =lespu1TCS_TCS_CTL;
    Ivss_ABS_CTL            =lespu1TCS_ABS_ACT;
    Ivss_TCS1_ESP_CTL       =lespu1TCS_ESP_CTL;
    Ivss_TCS1_TQI_TCS       =cal_torq;
    Ivss_TCS3_BrakeLight    =fu1BrakeLampRequest;

#if __SCC
    Ivss_TCS3_DriverBraking =ccu1DrvBraking;
#else
    Ivss_TCS3_DriverBraking =MPRESS_BRAKE_ON;
#endif
    
    Ivss_TCS5_WHEEL_FL      =FL.lsabss16WhlSpdRawSignal64;
    Ivss_TCS5_WHEEL_FR      =FR.lsabss16WhlSpdRawSignal64;
    Ivss_TCS5_WHEEL_RL      =RL.lsabss16WhlSpdRawSignal64;
    Ivss_TCS5_WHEEL_RR      =RR.lsabss16WhlSpdRawSignal64;
    Ivss_TCU2_CUR_GR        =lccanIVSS_TCU2_CUR_GR;

#if __VSM==ENABLE    
    Ivss_VSM2_CR_Mdps_StrTq =lsepss16EPSTorqLPF_2HZ;
    Ivss_VSM2_CR_Mdps_OutTq =lcepss16TargetTorqF;
    Ivss_VSM2_CF_Mdps_Def   =lcepsu8UCCState;    
#else
    Ivss_VSM2_CR_Mdps_StrTq =0 ;
    Ivss_VSM2_CR_Mdps_OutTq =0;
    Ivss_VSM2_CF_Mdps_Def   =0;    
#endif

#if __SCC    
    Ivss_SCC3_VSM_Warn      =lcu8CdmWarnLevel;
#else
    Ivss_SCC3_VSM_Warn      =0;
#endif

    Ivss_Clu_ActiveEcoSW    =lccanIVSS_Clu_ActiveEcoSW;    

    Ivss_Clu_DrivingModeSwi =ccu2DrvModeSwitch;
    
    #if __SCC==ENABLE
    Ivss_SCC_Enable=1;
    #else
    Ivss_SCC_Enable=0;
    #endif
    
    Ivss_Bat_Volt = fu16CalVoltVDD;
    
    //-----Invalid variable-----//
    if(lespu1EMS_F_N_ENG == 1)
    {
    	   Ivss_EMS1_N_IVD=1;
    }
    else
    {
    	  Ivss_EMS1_N_IVD=0;
    }
    
    if(lespu1EMS_F_SUB_TQI == 1)
    {
    	   Ivss_EMS1_TQI_IVD=1;
    }
    else
    {
    	  Ivss_EMS1_TQI_IVD=0;
    }
    
    if(ccu8BlsActSignal == 3)
    {
    	   Ivss_EMS2_BRAKE_ACT_IVD=1;
    }
    else
    {
    	  Ivss_EMS2_BRAKE_ACT_IVD=0;
    }
    
    if(lespu1EMS_TPS_ERR == 0xFF)
    {
    	   Ivss_EMS2_TPS_IVD=1;
    }
    else
    {
    	  Ivss_EMS2_TPS_IVD=0;
    }
    
    if(lespu1EMS_PV_AV_CAN_ERR == 0xFF)
    {
    	   Ivss_EMS2_PV_AV_CAN_IVD=1;
    }
    else
    {
    	  Ivss_EMS2_PV_AV_CAN_IVD=0;
    }
    
    if(fu1AyErrorDet == 1)
    {
    	   Ivss_ESP2_LAT_ACCEL_IVD=1;
    }
    else
    {
    	  Ivss_ESP2_LAT_ACCEL_IVD=0;
    }
    
    if(fu1AxErrorDet == 1)
    {
    	   Ivss_ESP2_LONG_ACCEL_IVD=1;
    }
    else
    {
    	  Ivss_ESP2_LONG_ACCEL_IVD=0;
    }
    
    if(fu1MCPErrorDet == 1)
    {
    	   Ivss_ESP2_CYL_PRES_IVD=1;
    }
    else
    {
    	  Ivss_ESP2_CYL_PRES_IVD=0;
    }
    
    if(fu1YawErrorDet == 1)
    {
    	   Ivss_ESP2_YAW_RATE_IVD=1;
    }
    else
    {
    	  Ivss_ESP2_YAW_RATE_IVD=0;
    }
    
    if(fu1StrErrorDet == 1)
    {
    	   Ivss_SAS1_SAS_Angle_IVD=1;
    }
    else
    {
    	  Ivss_SAS1_SAS_Angle_IVD=0;
    }
    
#if __SCC
    if((fu1BLSErrDet==1)&&(fu1BlsSusDet==1))
    {
    	  Ivss_TCS3_DriverBraking_IVD=1;
    }
    else
    {
    	  Ivss_TCS3_DriverBraking_IVD=0;
    }
#else
    if((fu1MCPErrorDet==1)||(fu1MCPSusDet==1))
    {
    	  Ivss_TCS3_DriverBraking_IVD=1;
    }
    else
    {
    	  Ivss_TCS3_DriverBraking_IVD=0;
    }
#endif
    
    if(fu1WheelFLErrDet == 1)
    {
    	  Ivss_TCS5_WHEEL_FL_IVD=1;
    }
    else
    {
    	  Ivss_TCS5_WHEEL_FL_IVD=0;
    }
    
    if(fu1WheelFRErrDet == 1)
    {
    	  Ivss_TCS5_WHEEL_FR_IVD=1;
    }
    else
    {
    	  Ivss_TCS5_WHEEL_FR_IVD=0;
    }
    
    if(fu1WheelRLErrDet == 1)
    {
    	  Ivss_TCS5_WHEEL_RL_IVD=1;
    }
    else
    {
    	  Ivss_TCS5_WHEEL_RL_IVD=0;
    }
    
    if(fu1WheelRRErrDet == 1)
    {
    	  Ivss_TCS5_WHEEL_RR_IVD=1;
    }
    else
    {
    	  Ivss_TCS5_WHEEL_RR_IVD=0;
    }
    
    if(cu1_TCU2_CUR_GR_IVD == 1)
    {
    	  Ivss_TCU2_CUR_GR_IVD=1;
    }
    else
    {
    	  Ivss_TCU2_CUR_GR_IVD=0;
    }
    
    
    if(lsepss16EPSTorq==0xFFF)
    {
        Ivss_VSM2_CR_Mdps_StrTq_IVD=1;
    }
    else
    {
        Ivss_VSM2_CR_Mdps_StrTq_IVD=0;
    }
    
    if(lsepss16EPSOutputTorq==0xFFF)
    {
        Ivss_VSM2_CR_Mdps_OutTq_IVD=1;
    }
    else
    {
        Ivss_VSM2_CR_Mdps_OutTq_IVD=0;
    }

#if __SCC    
    if(lcu8CdmStatus!=1)
    {
        Ivss_SCC3_VSM_Warn_IVD=1;
    }
    else
    {
        Ivss_SCC3_VSM_Warn_IVD=0;
    }
#else
    Ivss_SCC3_VSM_Warn_IVD=0;
#endif

    //----------------------//
    
    //-----TimeOut flag-----//
    if((cu1_EMS11_RxMissing_Flg==1) || (EMS1_timeout_err_flg==0))
    {
    	  Ivss_EMS11_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_EMS11_RxMissing_Flg = 0;
    }
    
    if((cu1_EMS12_RxMissing_Flg==1) || (EMS2_timeout_err_flg==0))
    {
    	 Ivss_EMS12_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_EMS12_RxMissing_Flg = 0;
    }
    
    if(cu1_TCU12_RxMissing_Flg==1)
    {
    	  Ivss_TCU12_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_TCU12_RxMissing_Flg = 0;
    }
    
    if((cu1_MDPS12_RxMissing_Flg==1) || (feu1VSM2_HW_Line_Err_flg==0))
    {
    	  Ivss_MDPS12_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_MDPS12_RxMissing_Flg = 0;
    }
    
    if((cu1_SCC12_RxMissing_Flg) || (feu1SCCTIMEOUT_Err_flg==0))
    {
    	  Ivss_SCC12_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_SCC12_RxMissing_Flg = 0;
    }
    
    if(cu1_CLU13_RxMissing_Flg==1)
    {
    	  Ivss_CLU13_RxMissing_Flg = 1;
    }
    else
    {
    	  Ivss_CLU13_RxMissing_Flg = 0;
    }
    //--------------------------------//         
}

#endif
