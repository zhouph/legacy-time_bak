/*******************************************************************/
/*  Program ID: MGH-40 int32_t G Sensor Offset Estimation             */
/*  Program description: int32_t G Sensor Offset Estimation           */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes: Will be applicated to KMC HM(SOP.07.08)         */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author           Description                    */
/*  -------       ----------    -----------------------------      */
/*  06.09.11      SeWoong Kim   Initial Release                    */
/*  07.07.23      SeWoong Kim   Free rolling condition change      */
/*  10.12.27      SeWoong Kim   New Structure                      */
/*                                                                 */
/*******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSABSEstLongAccSensorOffsetMon
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************/

#include "LSABSEstLongAccSensorOffsetMon.h"

#if (__Ax_OFFSET_MON==1)||(__Ax_OFFSET_MON_DRV==1 )

#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LDABSCallDctForVref.H"
#include "LSABSCallSensorSignal.h"
//#include "../Failsafe/F_VariableLinkSet.h"
#include "hm_logic_var.h"
#include "LCHDCCallControl.H"
#include "LDABSCallEstVehDecel.H"    /* SWD11_KJS / For AxBrakeModel  */

/*------------ EEP----------------*/
//#include "../Diag/CDC_DiagService.h"
//#include "../System/WContlSenSig.h"

/*------------ Sensor Failure-----*/
//#include "../Failsafe/FEErrorProcessing.h"
 

/* Local Definition  ***********************************************/

#define  FILOT_0_1G       102
#define  FILOT_0_0G       0
#define  FILOT_0_01G      10
#define  FILOT_0_02G      20
#define  FILOT_0_03G      30
#define  FILOT_0_04G      40
#define  FILOT_0_05G      50
#define  FILOT_0_06G      60
#define  FILOT_0_07G      70
#define  FILOT_0_08G      80
#define  FILOT_0_09G      90
#define  Ax1secOffsetTHR  50
#define  AxOFFSETLimit    200 
#define  AxOFFSETFirstEEPLimit        150 
#define  AxOFFSETFirstEEPMaxMinLimit  50
#define  Ax100secOffsetUpdateLimit    2
#define  Ax5secStandStillTHR          20

#define  AxOFS_VELO_MIN_THR           320   /* 40*8  40kph */
/* 20110916_KJS */
#define  AxOFS_VELO_MAX_THR           800   /* 100*8  100kph */
/* 20110916_KJS */
#define  AxOFS_MTP_THR                3     /* 3%  */
#define  AxOFS_ENG_TORQ_THR           200   /* 20% */
#define  AxOFS_YAW_THR                500   /* 5 deg/s */
#define  AxOFS_MPRESS_THR             30    /* 3 bar  */

/*----- Ax EEP Offset --------------------------------------------*/
#define  AxEEP_DELAY_T                L_U8_TIME_10MSLOOP_1000MS
#define  AxEEP_RETRY                  1

/*----- Ax DRV EEP Offset --------------------------------------------*/
#define  AxDRVEEP_WRT_THR             10    /* 0.01g */
#define  AxDRVEEP_FINAL_LIMIT         150   /* 0.15g */

  /*---------------------------------------------------------------*/
  /*       Vehicle Parameters                                      */
  /*---------------------------------------------------------------*/
  
#if __MODEL_CHANGE    
    #define Mass      (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */    
#else   
  #if (__CAR==HMC_GK) || (__CAR==ASTRA_MECU)  
    #define Mass            1330      /* Kg */  
  #elif (__CAR==SYC_RAXTON)||(__CAR==SYC_RAXTON_MECU)    
    #define Mass            2096      /* Kg */    
  #elif (__CAR==SYC_KYRON)||(__CAR==SYC_KYRON_MECU)  
    #define Mass            2026      /* Kg */  
  #else  /* NF */  
    #define Mass            1562      /* Kg */    
  #endif  
#endif      

/* swd11_kjs */
#define  AX_MASS_LIMIT         5000      /* 5000 [Kg] */
#define  AX_VREF_LIMIT         80        /* 10 [kph]  */ 
/* swd11_kjs */  

/* 20110613_KJS */
#define  __KINE_MODEL_AX_OFFSET         	DISABLE     
#define  __STANDSTATE_MODEL_AX_OFFSET       DISABLE       
/* 20110613_KJS */

#define  AxOFS_BRK_MODEL_MPRESS_THR         50          /* 5 bar  */
/* 20110810_KJS */

#define  AxOFS_DRV_TOTAL_TIME               7                          /* 10sec*(AxOFS_DRV_TOTAL_TIME) = 70sec */
#define  AxOFS_DRV_WIPER_THR               	L_U8_TIME_10MSLOOP_1000MS   /* Wiper Activation Threshold */

///* F078_BKTU */
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_1    30
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_2    25
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_3    20
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_4    15
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_5    10
//#define  U8_AX_OFFSET_MODEL_ENG_RESIST_6    5
///* F078_BKTU */

/* Variables Definition*********************************************/

  /* Global For Other's Use */

struct	U8_BIT_STRUCT_t  G_OFFSET_0; 
struct	U8_BIT_STRUCT_t  G_OFFSET_1; 
struct	U8_BIT_STRUCT_t  G_OFFSET_2; 
struct	U8_BIT_STRUCT_t  G_OFFSET_3; 
struct	U8_BIT_STRUCT_t  G_OFFSET_4; 
struct  LongG_offset  *ALGoff, ALGoff1, ALGoff2;

uint16_t	lcabsu16FuncStartTimeAx;
uint16_t	lcabsu16FuncCycleTimeAx;

int16_t 	 lsabss16AxDynModel; /* SWD11_KJS */
  
uint16_t   lsabsu16AxOffCNT;
int16_t	   lsabss16AxOfsDyModelRaw;	 
int32_t    lsabss32AxOfsDyModelRawSum;
 	
uint8_t    lsabsu8AccelAftCnt;

#if (__KINE_MODEL_AX_OFFSET ==ENABLE)  
uint16_t   lsabsu16AxOffKineCNT;
int16_t    lsabss16AxOfsKModelRaw;
int32_t    lsabss32AxOfsKModelRawSum;
#endif

int16_t    lsabss16AxOffsetf;
int16_t    lsabss16AxOffsetDrvf;
int16_t    lsabss16AxOffsetTOTf;

int16_t    lsabss16AxSenAirSus;
uint8_t    lsabsu8AxRollResist;            
int16_t    lsabss16AxAeroResist;        /* swd11_kjs */
int16_t    lsabss16AxEngModelResist;    /* swd11_kjs */
int16_t    lsabss16AxBrakeModelResist;  /* swd11_kjs */
uint8_t    lsabsu8AxMTPAfterCnt;        /* swd11_kjs */

int16_t    lsabss16AxEngTorqByGearState;
uint8_t  	 lsabsu8AxBrakeAfterCnt;
uint8_t  	 lsabsu8AxWheelSpinAfterCnt;
uint8_t  	 lsabsu8AxDriveModeDragResetCnt;

#if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE)
  /* Stand Still */    
uint16_t   lsabsu16AxStandCnt;
int32_t    lsabss32AxStandSum;  
int16_t    lsabss16AxOffset5secStand;  
int16_t    lsabss16AxOffsetStand[5];
uint8_t    lsabsu8AxOffset5secStandCnt;
int16_t    lsabss16AxOffsetStandMax;
int16_t    lsabss16AxOffsetStandMin;
int16_t    lsabss16AxOffset5sec5cntMean;
#endif   
  
  /* Vehicle First EEPROM */
int16_t    lsabss16AxOfsFirstCal;  
uint8_t  	 lsabsu8AxOfsFirstCalCnt;
int32_t    lsabss32AxOfsSumFirstCal;
int16_t    lsabss16AxOfsFirstCalMax;
int16_t    lsabss16AxOfsFirstCalMin;

  /*  EEPROM  */
int16_t    lsabss16AxOffsetEEPwriteTempo;  
uint8_t    lsabsu8AxOfsEEPMaxMinLMOverCnt;  
  /* Local */
uint8_t    lsabsu8AxOfsDelyCnt;
  
uint8_t    u8DataCNTAxOffset;  /* For Data Logging */
 
int16_t	   lsabss16AxOfsWiperOnCNT; /* Wiper Activation */ 


/* Local Function prototype ****************************************/

void  LSABS_vEstLongAccSensorOffsetMon(void);
void  LSABS_vCalcAxOriginSignal(void);
void  LSABS_vAxOfsEepromCalcAndRead(void);
void  LSABS_vDetAxOffsetFinal(void);
void  LSABS_vAxOfsEepromCalc(void);
void  LSABS_vAxOfsEepromCalcReset(void);
void  LSABS_vAxOfsDelayTime(void);
void  LSABS_vDetAxOfsFailManagement(void);

#if __VDC 
void  LSABS_vAxDrvOfsCalcMain(void);
void  LSABS_vCalcAx100secOffset(long );
void  LSABS_vCalcAxOffset(void );
void  LSABS_vDetAxCheckCondition(void );
void  LSABS_vEstAxRollResist(void);
void  LSABS_vEstAxAeroResist(void);
/* SWD11_KJS */
void  LSABS_vEstAxEngineModelResist(void);
void  LSABS_vEstAxBrakeModelResist(void);
/* SWD11_KJS */
void  LSABS_vCalcAxOffsetDynamicModel(void );
/* 20110615_KJS */
#if (__KINE_MODEL_AX_OFFSET ==ENABLE) 
void  LSABS_vCalcAxOffsetKinematModel(void );
#endif
#if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE) 
void  LSABS_vCalcAxOffsetStandState(void);
#endif
/* 20110615_KJS */
void  LSABS_vDetAxOffsetEEPwrite(void);


/* GlobalFunction prototype ****************************************/

extern uint8_t com_rx_buf[8];
extern uint16_t  weu16EepBuff[2];
extern uint16_t LSESP_u16WriteEEPROM(uint16_t addr,uint16_t data);

#endif

/* Implementation **************************************************/


void    LSABS_vEstLongAccSensorOffsetMon(void)
{
    
	  LSABS_vCalcAxOriginSignal();
	  
	  #if (__Ax_OFFSET_MON ==1)
	  LSABS_vAxOfsEepromCalcAndRead();	 
	  #endif
	  
    #if (__Ax_OFFSET_MON_DRV ==1)   
    LSABS_vAxDrvOfsCalcMain();
    #endif
             
    /*---- Determin Final Ax Offset ---*/
 	  LSABS_vDetAxOffsetFinal(); 	 		    
 	  
 	  /*---- Write EEPROM Ax Offset -----*/ 
 	  #if (__Ax_OFFSET_MON_DRV ==1)    
	  LSABS_vDetAxOffsetEEPwrite();	  	
	  #endif 	
	  	
}	

/*------------------ Ax Original Signal considering Air-sus  Condition  -------*/
void  LSABS_vCalcAxOriginSignal(void)
{
	  #if (__CAR==KMC_HM)
	    if (lcu8HdcEcsCompEnable==1)
      { 
          lsabss16AxSenAirSus = a_long_1_1000g + lcs16HdcEcsLongGComp; /* Compensation for Air_Suspention */
      }
      else
      {
          lsabss16AxSenAirSus = a_long_1_1000g;
      }
    #else
      lsabss16AxSenAirSus = a_long_1_1000g;     
    #endif
	
}

#if (__Ax_OFFSET_MON ==1)

/*------------------ Vehicle First EEPROM Calc And Read-----------------------------*/
void    LSABS_vAxOfsEepromCalcAndRead(void)
{
	
	if(lsabsu1AxOfsEepCalcStart==1)
	{		
	    if(lsabsu1AxOfsEepCalEndFlg==1)	
	    {
	        lsabsu1AxOfsEepCalcStart= 0;	
	    }
	    else
	    {
	    	if(lsabsu8AxOfsEEPMaxMinLMOverCnt <= 0 )
	    	{
	    		LSABS_vAxOfsEepromCalc();
	    	}
	    	else
	    	{
	    		if(lsabsu1AxOfsEepReCalcOK==1)
	    		{
	    			LSABS_vAxOfsEepromCalc();
	    		}
	    		else
	    		{
	    			LSABS_vAxOfsDelayTime();
	    		}
	    	}
	    }      			
	}
	else
	{
		if(cdcsu1AxOfsEepStart ==1 )
		{
			lsabsu1AxOfsEepCalcStart=1;
			LSABS_vAxOfsEepromCalcReset();
		}
		else
		{
			;
		}
	}
			  
    if(wbu1AxOfsReadEep==1)
    {
    	if( McrAbs(wbs16AxOfsFirstEep) <= AxOFFSETFirstEEPLimit)
    	{
    		lsabsu1AxOfsReadEepSpecInFlg=1;
    	}
    	else
    	{
    		lsabsu1AxOfsReadEepSpecInFlg=0;
    	}	
    	
    }
    else
    {
    	lsabsu1AxOfsReadEepSpecInFlg=0;
    }	
		
}

void   LSABS_vAxOfsEepromCalc(void)
{
	int16_t  lsabss16AxOfsCalcMaxMinTemp;
	
	if(cdcsu1AxOfsEepStart==1)
	{

	    #if __G_SENSOR_TYPE==CAN_TYPE
	    if( (cbit_process_step!=1) && (fu1AxSusDet==0)   )
	    #else			
	    if( (fu1AxSusDet==0) )
	    #endif
	    {	  		
			if(lsabsu8AxOfsFirstCalCnt < L_U8_TIME_10MSLOOP_200MS)
			{
				if(lsabsu8AxOfsFirstCalCnt==0)
				{
					lsabss32AxOfsSumFirstCal = 0 ;
					lsabss16AxOfsFirstCalMax = lsabss16AxSenAirSus;
					lsabss16AxOfsFirstCalMin = lsabss16AxSenAirSus;
					lsabss32AxOfsSumFirstCal=lsabss32AxOfsSumFirstCal + lsabss16AxSenAirSus;									
				}
				else
				{
					lsabss32AxOfsSumFirstCal=lsabss32AxOfsSumFirstCal + lsabss16AxSenAirSus;
					if( lsabss16AxOfsFirstCalMax < lsabss16AxSenAirSus  )
					{
						lsabss16AxOfsFirstCalMax= lsabss16AxSenAirSus;
					}
					else if(lsabss16AxOfsFirstCalMin > lsabss16AxSenAirSus )
					{
						lsabss16AxOfsFirstCalMin= lsabss16AxSenAirSus;
					}
					else
					{
					  ;	
					}
					
				}		
        
				lsabsu8AxOfsFirstCalCnt=lsabsu8AxOfsFirstCalCnt+1;	
				
			}
			else
			{
				lsabss16AxOfsFirstCal       = (int16_t)(lsabss32AxOfsSumFirstCal/(L_U8_TIME_10MSLOOP_200MS) );
				lsabss16AxOfsCalcMaxMinTemp =  lsabss16AxOfsFirstCalMax-lsabss16AxOfsFirstCalMin ;		
				lsabsu8AxOfsEEPMaxMinLMOverCnt=lsabsu8AxOfsEEPMaxMinLMOverCnt+1;							 
        
				if(  McrAbs(lsabss16AxOfsFirstCal)<= AxOFFSETFirstEEPLimit  )
				{
					lsabsu1AxOffsetEEPLimitOK=1;
				}
				else
				{
					lsabsu1AxOffsetEEPLimitOK=0;
				}	 
				
				if(  lsabss16AxOfsCalcMaxMinTemp <= AxOFFSETFirstEEPMaxMinLimit  )
				{
					lsabsu1AxOffsetEEPMaxMinOK=1;
				}
				else
				{
					lsabsu1AxOffsetEEPMaxMinOK=0;
				}	 		
				
								
                if( (lsabsu1AxOffsetEEPLimitOK==1) && (lsabsu1AxOffsetEEPMaxMinOK==1) )
                {
                	lsabsu1AxOfsEepCalEndFlg=1;
                	lsabsu1AxOfsEepCalOkFlg =1;                	
                }
                else if( (lsabsu1AxOffsetEEPLimitOK==1) && (lsabsu1AxOffsetEEPMaxMinOK==0) )
                {
                	lsabsu1AxOfsEepCalEndFlg=0;
                	lsabsu1AxOfsEepCalOkFlg =0;                     	
                }
                else if( (lsabsu1AxOffsetEEPLimitOK==0) && (lsabsu1AxOffsetEEPMaxMinOK==1) )
                {
                	lsabsu1AxOfsEepCalEndFlg=1;
                	lsabsu1AxOfsEepCalOkFlg =0;                          	
                }
                else
                {
                	lsabsu1AxOfsEepCalEndFlg=1;
                	lsabsu1AxOfsEepCalOkFlg =0;   
                }					
    									
    			if(lsabsu8AxOfsEEPMaxMinLMOverCnt>= (AxEEP_RETRY+1) )
    			{
    				lsabsu1AxOfsEepCalEndFlg=1;
    			}
    			else
    			{
  				    ;
    			}	
    																		
				lsabsu8AxOfsFirstCalCnt= 0;					
				
	            lsabsu8AxOfsDelyCnt=0;
	            lsabsu1AxOfsEepReCalcOK=0;  				
			}		
		}
		else
		{
            ;
		}
		
	}
	else
	{
        lsabsu1AxOfsEepCalEndFlg=1;    	
	}		
}

void    LSABS_vAxOfsEepromCalcReset(void)
{
	lsabsu8AxOfsEEPMaxMinLMOverCnt=0;
	lsabsu8AxOfsFirstCalCnt= 0;	
  lsabsu1AxOffsetEEPLimitOK=0;
	lsabsu1AxOffsetEEPMaxMinOK=0;
	lsabsu1AxOfsEepCalEndFlg=0;
	lsabsu1AxOfsEepCalOkFlg=0;		
	lsabsu8AxOfsDelyCnt=0;
	lsabsu1AxOfsEepReCalcOK=0;
}

void    LSABS_vAxOfsDelayTime(void)
{
	lsabsu8AxOfsDelyCnt=lsabsu8AxOfsDelyCnt +1 ;
	
	if(lsabsu8AxOfsDelyCnt > AxEEP_DELAY_T )
	{
		lsabsu1AxOfsEepReCalcOK =1 ;
	}
	else
	{
		;
	}	    	
}

#endif

/*----------------- Final Determine Ax Sensor Offset between Kinematic vs Dynamic Model---- */
void LSABS_vDetAxOffsetFinal(void)
{  

  #if (__Ax_OFFSET_MON ==1)
  
   if(lsabsu1AxOfsReadEepSpecInFlg==1)  /*--����� EEPROM ---*/
   {
      lsabss16AxOffsetf = wbs16AxOfsFirstEep;
      lsabsu1AxOfsNone=0;
   }
   else
   {
 	    lsabss16AxOffsetf = 0;
 	    lsabsu1AxOfsNone=1;    		  
   }
   
   lsabss16AxOffsetTOTf = lsabss16AxOffsetf;
    
  #endif
  
  
  #if (__Ax_OFFSET_MON_DRV ==1)            /* wbu1YawSNUnmatchFlg==1 ?? */
  
    if( McrAbs(ALGoff1.lsabss16Ax100secOffMean) <= AxDRVEEP_FINAL_LIMIT )
    {   
    lsabss16AxOffsetDrvf = ALGoff1.lsabss16Ax100secOffMean ;
    }
    else
    {
    		if(ALGoff1.lsabss16Ax100secOffMean > AxDRVEEP_FINAL_LIMIT)
	      {
	      		lsabss16AxOffsetDrvf = AxDRVEEP_FINAL_LIMIT;  			    		
	      }
	      else if(ALGoff1.lsabss16Ax100secOffMean < -AxDRVEEP_FINAL_LIMIT )
	      {
	        	lsabss16AxOffsetDrvf = -AxDRVEEP_FINAL_LIMIT;  
	      }
	      else
	      {
	        	;
	      }
            
       	lsabsu1AxOffsetSuspect=1; /* Need After Detecting Ax Suspect */	  	          	
    
  	}	 
    
    if((ALGoff1.lsabsu1Ax100secOffOK==1)||(ALGoff1.lsabsu8Ax10secOffsetCnt >= AxOFS_DRV_TOTAL_TIME))
    {
        lsabss16AxOffsetTOTf = lsabss16AxOffsetDrvf;    
        lsabsu1AxOfsNone=0;
    }
    else 
    {
        /* EEPROM READ */
        if(wbu1AxDrivingOfsReadEep == 1)
        {
            lsabss16AxOffsetTOTf = wbs16AxDrivingOfsFirstEep;
            lsabsu1AxOfsNone     = 0; 
        }
        else
        {
            #if (__Ax_OFFSET_MON ==1)
            
              if(lsabsu1AxOfsReadEepSpecInFlg==1)  /*--����� EEPROM ---*/
              {
                  lsabss16AxOffsetTOTf = wbs16AxOfsFirstEep;
                  lsabsu1AxOfsNone=0;
              }
              else
              {
 	              lsabss16AxOffsetTOTf = 0;
 	                lsabsu1AxOfsNone=1;    		  
              }        
            #else
              lsabss16AxOffsetTOTf = 0;
              lsabsu1AxOfsNone  = 1;
            #endif                    
        }
    }
    
  #endif
       
}

#if (__Ax_OFFSET_MON_DRV ==1) 

void    LSABS_vAxDrvOfsCalcMain(void)
{
    uint8_t  i;
  
    if(speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS) 
    {         		        
	      lsabsu16AxOffCNT=0;			      
	      lsabss16AxOfsDyModelRaw=0;
	      lsabss32AxOfsDyModelRawSum=0;      
	    	
	    	#if (__KINE_MODEL_AX_OFFSET ==ENABLE)  
  	    lsabsu16AxOffKineCNT=0;	
	      lsabss16AxOfsKModelRaw=0;
	      lsabss32AxOfsKModelRawSum=0;
	      #endif   		    	
	      
	      lsabsu1AxOfsAccAftFlag=0;
	      lsabsu1AxBrakeAfter=0;
	      lsabsu1AxOfsWheelSlipFlg=0;
	      
	      lsabsu8AxWheelSpinAfterCnt=0;
	      lsabsu8AccelAftCnt=0;
	      lsabsu8AxBrakeAfterCnt=0;
	      lsabsu8AxDriveModeDragResetCnt=0;

				/*--- Dynamic Model G offset Estimation --- */ 	      	    	
	      ALGoff1.lsabsu8Ax1secOffsetCnt=0;
	      ALGoff1.lsabsu8Ax10secOffsetCnt=0;
				ALGoff1.lsabsu1Ax10secOffOK = 0;
				ALGoff1.lsabsu1Ax1secFirstOK= 0;
				ALGoff1.lsabsu1Ax10secFirstOK=0;
				ALGoff1.lsabsu1Ax100secFirstOK=0;
				ALGoff1.lsabsu1Ax100secOffOK= 0;
				ALGoff1.lsabss16Ax1secOffMAX= 0;
				ALGoff1.lsabss16Ax1secOffMIN= 0;
				ALGoff1.lsabss16Ax10secOffMean=0;
				ALGoff1.lsabss16Ax100secOffMean=0;
				ALGoff1.lsabsu1Ax10secOffSusp=0;
				
			  #if (__KINE_MODEL_AX_OFFSET ==ENABLE)
			  /*--- Kinematic Model G offset Estimation --- */				
	      ALGoff2.lsabsu8Ax1secOffsetCnt=0;
	      ALGoff2.lsabsu8Ax10secOffsetCnt=0;
	      ALGoff2.lsabsu1Ax10secOffOK = 0;	      
	      ALGoff2.lsabsu1Ax1secFirstOK= 0;	      
	      ALGoff2.lsabsu1Ax10secFirstOK=0;	      
	      ALGoff2.lsabsu1Ax100secFirstOK=0;      
	      ALGoff2.lsabsu1Ax100secOffOK= 0;	      
	      ALGoff2.lsabss16Ax1secOffMAX= 0;        
	      ALGoff2.lsabss16Ax1secOffMIN= 0;	          	 
        ALGoff2.lsabss16Ax10secOffMean=0;	          
        ALGoff2.lsabss16Ax100secOffMean=0;                     
        ALGoff2.lsabsu1Ax10secOffSusp=0;
        #endif    	  	
	    	
	    	for (i=0;i<=9;i=i+1)
	    	{
	    		  /*--- Dynamic Model G offset Estimation --- */
            ALGoff1.lsabss16Ax1SecOfs[i]=0;
            ALGoff1.lsabss16Ax10SecOfs[i]=0;
            
            #if (__KINE_MODEL_AX_OFFSET ==ENABLE)
			  		/*--- Kinematic Model G offset Estimation --- */	              
            ALGoff2.lsabss16Ax1SecOfs[i]=0;	              	
            ALGoff2.lsabss16Ax10SecOfs[i]=0;
            #endif   	
	    	}
	    	
	    	#if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE)
	    	/*  Stand Still */
        lsabsu16AxStandCnt=0;
        lsabss32AxStandSum=0;  	    		   
        lsabss16AxOffset5secStand=0;	
        lsabsu8AxOffset5secStandCnt=0;	        
        lsabss16AxOffsetStand[0]=0;
        lsabss16AxOffsetStand[1]=0;       
        lsabss16AxOffsetStand[2]=0;
        lsabss16AxOffsetStand[3]=0;              
        lsabss16AxOffsetStand[4]=0;     
        #endif
        lsabsu1AxOffsetStand5secOK=0;  
        lsabsu1AxOffsetStand5sFirstOK=0;
        lsabsu1AxOffsetStnd5s5cntFOK=0;     
        lsabsu1AxOffsetStandStillOK=0;
        lsabsu1AxOffsetStandStillSus=0;
   		    	 	 	
    }
    else
    {
        /*---------- Ax Sensor Offset Est Failmanagement -------------------------*/
        LSABS_vDetAxOfsFailManagement();     
        
        /*---------- Dynamic Model resistance Estimation -------------------------*/
        LSABS_vEstAxRollResist();
        LSABS_vEstAxAeroResist();
        LSABS_vEstAxEngineModelResist();
        LSABS_vEstAxBrakeModelResist();
        
        
        LSABS_vDetAxCheckCondition();
        
        /*---------- Dynamic   Model G offset Estimation ------------------------ */  
        LSABS_vCalcAxOffsetDynamicModel();
        /*---------- Kinematic Model G offset Estimation ------------------------ */
        #if (__KINE_MODEL_AX_OFFSET ==ENABLE)  
        LSABS_vCalcAxOffsetKinematModel();
        #endif       
        /*---------- Standstill G offset Estimation ------------------------ */
        #if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE)    
        LSABS_vCalcAxOffsetStandState();
		    #endif                             
	  }		    
    
}

/*------------------ Ax Signal Fail-management  -------------------------------*/
void    LSABS_vDetAxOfsFailManagement(void)
{
    /*--- 1. Ax Sensor Suspicious ---*/
    /*--- 2. BLS error            ---*/
    /*--- 3. Wheel Speed Sensor   ---*/
    /* mtp error */
    /* Main CAN Error : eng_T, eng_RPM, TC_rpm, gear inform  ... */
            
    if(    (fu1AxErrorDet==1)  ||  (fu1AxSusDet==1) 
    	   #if __G_SENSOR_TYPE==CAN_TYPE
    	   ||(cbit_process_step==1)
    	   #endif                
         ||(fu1BLSErrDet ==1)                                        
         ||(fu1WheelFLErrDet==1) || (fu1WheelFRErrDet==1) || (fu1WheelRLErrDet==1) || (fu1WheelRRErrDet==1)
         ||(fu1MainCanLineErrDet ==1) || (fu1EMSTimeOutErrDet ==1) || (fu1TCUTimeOutErrDet==1) /* 20110610_KJS */
         
         /*    Mpress  Sensor Suspicious */
         /*    YawRate Sensor Suspicious */
         #if __VDC  
         ||(fu1MCPErrorDet==1) ||  (fu1MCPSusDet==1)               
         ||(fu1YawErrorDet==1)                                            
         #endif
                        
      )
    {
        lsabsu1AxOfsCalcInhibit = 1; 
    }
    else
    {
        lsabsu1AxOfsCalcInhibit = 0;  
    }
  
}

/*---------- Dynamic Model resistance Estimation ------------------------------*/
/*------------------ Roll Resist Force Estimation  -------*/
void    LSABS_vEstAxRollResist(void)
{
  /* Think More Concept-- */

		/* swd11_kjs */
		lsabsu8AxRollResist=U8_AX_OFFSET_MODEL_ROLL_TOTAL; /* 20 => 0.020[g] */
		/* swd11_kjs */
	
}

/*------------------ Aero Resist Force Estimation  -------*/
void    LSABS_vEstAxAeroResist(void)
{

	 /*** swd11_kjs ***/
	 int16_t   S16AxAeroTemp0;
	 int16_t   S16AxAeroTemp1;
	 int16_t   S16AxAeroTemp2;
   int32_t  S32AxAeroTemp0;
   	 
   /* SWD11_KJS FailManagement is needed */
   
   /*-- Vehicle Mass Limitation --*/
      
   /* SWD11_KJS FailManagement is needed */

	 S16AxAeroTemp0 = (int16_t)( ((int16_t)U8_AX_OFFSET_MODEL_AIR_CD*U8_AX_OFFSET_MODEL_AIR_RO)*U8_AX_OFFSET_MODEL_AIR_AREA); /* Cd*Ro*Area (1/1000)*/
	 S16AxAeroTemp1 = (int16_t)( ((int32_t)vref*347)/10000 ); /* Vref [m/s] (0.0347=10/288=(1/8)*(10/36) */
	 
	 /* [g]_1/1000 / 1[g]=10[m/s^2] / Mass [kg] */	 	
  
   S32AxAeroTemp0 = (int32_t)(Mass/10)*200;
   S16AxAeroTemp2 = S16AxAeroTemp1*S16AxAeroTemp1;

	 lsabss16AxAeroResist = (int16_t)(((int32_t)S16AxAeroTemp0*S16AxAeroTemp2)/S32AxAeroTemp0) ;
	 /*** swd11_kjs ***/
	
}

/*------------ Engine Model Resist Force Estimation -----------*/
void    LSABS_vEstAxEngineModelResist(void)
{
    
    #if GMLAN_ENABLE==ENABLE  /* GM : Minus Engine Torque_O / HMC_KMC : Minus Engine Torque_X */
 		
 		/*** swd11_jinse ***/
 		
		int16_t S16AxEngModelResistETA; /* resol : 1_100 / 80->80% */
		int16_t S16AxEngModelVref;
		
		/* SWD11_KJS / Cycle Time */
		int16_t S16AxEngModelTemp0;
		int16_t S16AxEngModelTemp1;
		int16_t S16AxEngModelTemp2;
		int32_t S32AxEngModelTemp0;
		int32_t S32AxEngModelTemp1;
		/* SWD11_KJS / Cycle Time */
		
		/* SWD11_KJS FailManagement is needed */

 		if(gear_state==1)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_1;
 		}
 		else if(gear_state==2)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_2;
 		}
 		else if(gear_state==3)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_3;
 		}
 		else if(gear_state==4)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_4;
 		}
 		else if(gear_state==5)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_5;
 		}
 		else if(gear_state==6)
 		{
 				S16AxEngModelResistETA = U8_AX_OFFSET_MODEL_ENG_ETA_6;
 		}
 		else /* gear_state==0 or Else */
 		{ 
 				S16AxEngModelResistETA = 0;
 		}
 		
 		/*--------------------------------------------------------------------------------------*/				
 		/* Engine Torque : A[Nm]  = (A/10)[(kg*m/s^2)m]                                         */
 		/* Engine Speed  : B[rpm] = (B/10)[2*pi*rad/60sec]==[rad/sec] / <2*pi/60==0.1047==1/10> */	 		
		/* Vref          : C[kph] = (C*(10/36))[m/s]                                            */
    /*--------------------------------------------------------------------------------------*/

    if(vref <= AX_VREF_LIMIT)
    {
    		S16AxEngModelVref = AX_VREF_LIMIT; /* 10kph */
    }
    else
    {
    		S16AxEngModelVref = vref;
    }
     
    S16AxEngModelTemp0 = eng_torq/10;
    S16AxEngModelTemp1 = (int16_t)(((int32_t)S16AxEngModelResistETA*29)/10);
    S16AxEngModelTemp2 = (eng_rpm/10);
    S32AxEngModelTemp0 = ((int32_t)S16AxEngModelVref*Mass)/10;
    S32AxEngModelTemp1 = ((int32_t)S16AxEngModelTemp0*S16AxEngModelTemp1);

    lsabss16AxEngModelResist = (int16_t)((S32AxEngModelTemp1*S16AxEngModelTemp2)/S32AxEngModelTemp0);
 		/*** swd11_jinse ***/
	#else

		#if __VDC
 			if(gear_state==1)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_1;
 			}
 			else if(gear_state==2)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_2;
 			}
 			else if(gear_state==3)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_3;
 			}
 			else if(gear_state==4)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_4;
 			}
 			else if(gear_state==5)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_5;
 			}
 			else if(gear_state==6)
 			{
 					lsabss16AxEngModelResist = -U8_AX_OFFSET_MODEL_ENG_RESIST_6;
 			}
 			else /* gear_state==0 or Else */
 			{ 
 					lsabss16AxEngModelResist = 0;
 			}
 		#else
 			lsabss16AxEngModelResist = 0;
        #endif   			

	#endif             			
             			
}

/*------------ Brake Model Resist Force Estimation -----------*/
void    LSABS_vEstAxBrakeModelResist(void)
{
    
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
		if(mpress>=AxOFS_BRK_MODEL_MPRESS_THR)
    #else
		if(lsesps16MpressByAHBgen3>=AxOFS_BRK_MODEL_MPRESS_THR)
    #endif
		{
		/* swd11_kjs */
		lsabss16AxBrakeModelResist = -(Vehicle_Decel_EST*10);  /* 1.Signed_Plus, 2.Resol_1/1000[g] */	
		/* swd11_kjs */
}
		else
		{
				lsabss16AxBrakeModelResist = 0;   /* 0.000g */
		}
}

/*------------------ Ax Offset Condition Check  ---------------------------------*/
void  LSABS_vDetAxCheckCondition(void)
{
	/*---------------------- Ax Sensor Condition Check --------------------------*/   
	/*  1. After Accel Hysterisis     0.5 sec                                    */
	/*  2. After Brake Hysterisis     1.0 sec                                    */ 
	/*  3. Wheel Slip  restriction    diff_max<=1kph                             */
	/*  4. Engine Drag Condition      eng_rpm-tc_rpm & ratio                     */
	/*  5. Zero Gradient Estimation   range of filter-out ( wheel acceleration)  */
 	/*  6. Drive Mode Drag Condition  4WD-LOCK, 4L Mode Hyserisis                */
	/*  7. MTP Hysterisis             0.2 sec                                    */
	/*  8. Sports Mode Prevention for A/T                                        */
	/*  9. Wiper Act Signal 											         */
	/* 10. Trailer Connector Signal                                              */
	/* 11. Bump or Road Gradient      Ax Sensor variation                        */
	/*     Change Check                                                          */
	/*---------------------------------------------------------------------------*/
	
	 uint16_t   U16EngineTcRPMdiff;
	 uint16_t   U16EngineTcRPMratio;
	 int16_t    S16AxVehAcc;
	   
	 
    /*------1.  Accel Hysterisis 500msec ---------------*/
    if(gear_state==0)
    {
    		lsabss16AxEngTorqByGearState = 0;
    }
    else
    {
    		lsabss16AxEngTorqByGearState = eng_torq_rel;    	
    }
    
    if( lsabss16AxEngTorqByGearState<=(AxOFS_ENG_TORQ_THR) )
    {
        lsabsu8AccelAftCnt=lsabsu8AccelAftCnt+1;
        
        if(lsabsu8AccelAftCnt >= L_U8_TIME_10MSLOOP_500MS )
        {
            lsabsu1AxOfsAccAftFlag = 1;
            lsabsu8AccelAftCnt = L_U8_TIME_10MSLOOP_500MS;
        }
        else
        {
            lsabsu1AxOfsAccAftFlag = 0;
        }
    }
    else
    {
        lsabsu8AccelAftCnt=0;
        lsabsu1AxOfsAccAftFlag =0;
    }
    
    /*------ 2. Brake Hysterisis 1sec ---------------*/    
    if(BRAKE_SIGNAL==0)
    {
    	if(lsabsu8AxBrakeAfterCnt>=L_U8_TIME_10MSLOOP_1000MS)
    	{
    		lsabsu1AxBrakeAfter=1;
    		lsabsu8AxBrakeAfterCnt=L_U8_TIME_10MSLOOP_1000MS;
    	}
    	else
    	{
    		lsabsu1AxBrakeAfter=0;
	    	lsabsu8AxBrakeAfterCnt=lsabsu8AxBrakeAfterCnt+1;
    	}
    }
    else
    {
    	lsabsu8AxBrakeAfterCnt=0;
    	lsabsu1AxBrakeAfter=0;
    }
    
    
    /*------3.  Wheel Spin Hysterisis 500msec ---------------*/
    if(lsabsu1AxOfsWheelSlipFlg ==1)
    {
      if(lsabsu8AxWheelSpinAfterCnt>=L_U8_TIME_10MSLOOP_500MS) 
      {
      	lsabsu1AxOfsWheelSlipFlg=0;
      	lsabsu8AxWheelSpinAfterCnt=0;
      }
      else
      {
      	lsabsu8AxWheelSpinAfterCnt=lsabsu8AxWheelSpinAfterCnt+1;
      }
    }
    else
    {
      if(vrad_diff_max_resol_change <= VREF_1_KPH_RESOL_CHANGE)
      {
         lsabsu1AxOfsWheelSlipFlg=0;
      }
      else
      {
         lsabsu1AxOfsWheelSlipFlg=1;
      }    
      lsabsu8AxWheelSpinAfterCnt=0;	
    	
    }
    
    /*------ 4. Engine Drag Check Condition ---------------*/     
    /*  OR condition
       1. abs(Engine_rpm-Tc_rpm)>250;
       2. abs(Engine_rpm-Tc_rpm)/(Engine_rpm) <= 0.20 (20 %) <-- Tuning Factor       
    */
    if(AUTO_TM==1)    /*Think More - Tuning Parameter */
    {
    	if((eng_rpm==0)||(tc_rpm==0)) /* 20110610_kjs : for GSUV, because 'tc_rpm always Zero */
    	{   			
				lsabsu1AxEngineDrag=0;
      }
      else
      {
      	U16EngineTcRPMdiff=McrAbs(eng_rpm - tc_rpm) ;
        U16EngineTcRPMratio=(uint16_t)( ((int32_t)U16EngineTcRPMdiff*100)/eng_rpm); /* Check Possiblity : eng_rpm==0 */
        
        if( (U16EngineTcRPMdiff > U16_AX_OFFSET_CHECK_ENG_DRAG_1) || (U16EngineTcRPMratio > U8_AX_OFFSET_CHECK_ENG_DRAG_2 ) )
        /* if( (U16EngineTcRPMdiff > 500) || (U16EngineTcRPMratio >20 ) ) : SWD11_KJS */	
        {
        	lsabsu1AxEngineDrag=1;
        }
        else
        {
        	lsabsu1AxEngineDrag=0;
        }
      }                
    }
    else
    {
    	lsabsu1AxEngineDrag=0;
    } 
        
    
        
        
    /*------ 5. Zero Gradient Check Condition ---------------*/  
    /*       
       - AxDynModel             : lsabss16AxEngModelResist - lsabsu8AxRollResist - lsabss16AxAeroResist	
       - Dynamic    Model Check : lsabss16AxDynModel-0.04g <= filter-out <= lsabss16AxDynModel+0.04g       
       - Kinematics Model Check : -0.09g <= filter-out <= -0.02g
    */        
       
    S16AxVehAcc =(int16_t)( (((int32_t)FZ1.filter_out)*125)/128 ) ;
    
    /* SWD11_KJS */
    /* Relative Standard based on lsabss16AxDynModel */
    if(  ((S16AxVehAcc >= (lsabss16AxDynModel-U8_AX_OFFSET_CHECK_DYN_FLAT)) && (S16AxVehAcc <= (lsabss16AxDynModel+U8_AX_OFFSET_CHECK_DYN_FLAT)) ) && ((lsabss16AxDynModel)<=(-20)) ) /* -10 : -0.01g  */
    {
    	lsabsu1AxDyOffsetFlat=1;
    }
    else
    {
    	lsabsu1AxDyOffsetFlat=0;
    }
    
    /* SWD11_KJS */
    /* Absolute Standard based on S16AxVehAcc */
    if( (S16AxVehAcc >= -(U8_AX_OFFSET_CHECK_KIN_FLAT_1)) && (S16AxVehAcc <= -(U8_AX_OFFSET_CHECK_KIN_FLAT_2)) )
    /* SWD11_KJS */		
    {
    	lsabsu1AxKineOffsetFlat=1;
    }
    else
    {
    	lsabsu1AxKineOffsetFlat=0;
    }
        
    /*---- 6. Drive Mode Drag Condition  4WD-LOCK, 4L Mode Hysterisis 500msec  -------*/
    #if (__CAR==KMC_HM)   /*-- Think More --*/
      if( (lespu1AWD_LOW_ACT==1)   )
      {
        lsabsu1AxDriveModeDrag=1;	
        lsabsu8AxDriveModeDragResetCnt=0;
      }
      else
      {
      	if(lsabsu1AxDriveModeDrag==1)
      	{
      	  if(lsabsu8AxDriveModeDragResetCnt < L_U8_TIME_10MSLOOP_500MS)
      	  {
    	      lsabsu1AxDriveModeDrag=1;    	    
    	      lsabsu8AxDriveModeDragResetCnt=lsabsu8AxDriveModeDragResetCnt+1;    	    
    	    }
    	    else
    	    {
    	      lsabsu1AxDriveModeDrag=0;
    	      lsabsu8AxDriveModeDragResetCnt=0;
    	    }
    	  }
    	  else
    	  {
    	  	lsabsu1AxDriveModeDrag=0;
    	  	lsabsu8AxDriveModeDragResetCnt=0;
    	  }    	  
      }
    #else
      lsabsu1AxDriveModeDrag=0;
    #endif  
    
        /* swd11_kjs */
    /*------ 7. MTP Hysterisis 200msec ---------------*/    
    if(lsabsu1AxMTPAfterFlg==0) /* Set Condition */
    {
    	    if(mtp <= AxOFS_MTP_THR)
    			{
    					if(lsabsu8AxMTPAfterCnt>=L_U8_TIME_10MSLOOP_200MS)
    					{
    							lsabsu1AxMTPAfterFlg=1;
    							lsabsu8AxMTPAfterCnt=L_U8_TIME_10MSLOOP_200MS;
    					}
    					else
    					{
	    						lsabsu8AxMTPAfterCnt=lsabsu8AxMTPAfterCnt+1;
    					}
    			}
    			else
    			{
    					lsabsu8AxMTPAfterCnt=0;
    			}
    }
    else /* Reset Condition */
    {
    			if(mtp > AxOFS_MTP_THR)
    			{
    					lsabsu1AxMTPAfterFlg=0;
    					lsabsu8AxMTPAfterCnt=0;
    			}
    			else
    			{
    				;
    			}
    }
    /* swd11_kjs */
    
    /* 20110615_KJS */
    /*------ 8. Sports Mode Prevention for A/T ---------------*/
    if(lsabsu1AxOfsSportModeFlg==0) /* Set Condition */
    {
    		if(AUTO_TM==1)
    		{
    			#if GMLAN_ENABLE==ENABLE
    			if(gs_sel==3)
    			#else
    			if(gs_sel==8)
    			#endif
    			{
    				lsabsu1AxOfsSportModeFlg=1;
    			}
    			else
    			{
    				lsabsu1AxOfsSportModeFlg=0;
    			}
    		}
    		else
    		{
    			lsabsu1AxOfsSportModeFlg=0;
    		}
    }
    else /* Reset Condition */
    {
    		if(AUTO_TM==1)
    		{
    			#if GMLAN_ENABLE==ENABLE
    			if(gs_sel==3)
    			#else
    			if(gs_sel==8)
    			#endif
    			{
    				;
    			}
    			else
    			{
    				lsabsu1AxOfsSportModeFlg=0;
    			}
    		}
    		else
    		{
    			lsabsu1AxOfsSportModeFlg=0;
    		}
    }    
    /* 20110615_KJS */    
                     
    /* 20110821_KJS */
    /*------ 9. Wiper Act Signal ---------------*/
    #if GMLAN_ENABLE==ENABLE  /* GMLAN Signal */
    if(lsabsu1AxOfsWiperOnFlg==0)		/* Set Condition */
    {
    		if(lsespu1WiperActFlg==1)
		    {
					lsabsu1AxOfsWiperOnFlg = 1;
					lsabss16AxOfsWiperOnCNT = AxOFS_DRV_WIPER_THR;
			}
			else
			{
					;
			}
	}
	else	/* Reset Condition */
	{
			if(lsespu1WiperActFlg==1)
			{
					lsabss16AxOfsWiperOnCNT = AxOFS_DRV_WIPER_THR;
			}
			else
			{
					if(lsabss16AxOfsWiperOnCNT <=0)
					{
							lsabsu1AxOfsWiperOnFlg=0;
					}
					else
					{
							lsabss16AxOfsWiperOnCNT = lsabss16AxOfsWiperOnCNT - 1;
					}
			}				
	}
	#else
	lsabsu1AxOfsWiperOnFlg = 0;		
	#endif 
    /* 20110821_KJS */   
                     
}


/*------------------ Dynamic Model Ax Offset Calculation ------------------------*/
void     LSABS_vCalcAxOffsetDynamicModel(void)
{	    		 
		 /* swd11_kjs */	    		 	    
    lsabss16AxDynModel = lsabss16AxEngModelResist - lsabss16AxBrakeModelResist - (int16_t)lsabsu8AxRollResist - lsabss16AxAeroResist; 
		 	    
        /*----------  G offset Estimation ------------------------ */    	
  #if defined (__ABS_ONLY_SYSTEM_CODE )    
        if( (lsabsu1AxOfsCalcInhibit==0)                                &&      /* 1. Inhibit =0               */               
            ((vref >=AxOFS_VELO_MIN_THR)&&(vref <=AxOFS_VELO_MAX_THR) ) &&      /* 2. vref >=40 && vref <=90   */
        	  (BLS==0)                                                    &&      /* 3. Brake =0  (BLS=0)        */
        	  (PARKING_BRAKE_OPERATION==0)                                &&      /* 4. Brake =0  (Parking=0)    */
        	  (lsabsu1AxMTPAfterFlg==1)                                   &&      /* 5. mtp   < 3%  : 200ms      */
        	  (lsabsu1AxEngineDrag==0)                                    &&      /* 6. Engine   Drag Check      */  /*�ŷ� flag */
        	  (lsabsu1AxDriveModeDrag==0)                                 &&      /* 7. Drv Mode Drag Check      */ 
        	  (lsabsu1AxBrakeAfter==1)                                    &&      /* 8. Brake After              */       	  
        	  (lsabsu1AxOfsAccAftFlag==1)                                 &&      /* 9. Accel After              */   	        	          	    	      	         
            (BACKWARD_MOVE==0)                                          &&      /*10. Backward=0               */                               
        	  (lsabsu1AxOfsWheelSlipFlg==0 )                              &&      /*11. Wheel Slip=0             */ /* Think More */
        	  (lsabsu1AxDyOffsetFlat==1)                                  &&      /*12. Dynamic Zero gradient    */
            (ABS_fz==0)                                                         /*13. ABS fz=0                 */       	  
	        )                                                                     /*    Rough Road �߰�?         */
  #else
				/* swd11_kjs */
        if( (lsabsu1AxOfsCalcInhibit==0)                                                                  &&   /* 1. Inhibit =0               */ 
            ((vref >=AxOFS_VELO_MIN_THR)&&(vref <=AxOFS_VELO_MAX_THR) )                                   &&   /* 2. vref >=40 && vref <=90   */
            (BLS==0)                                                                                      &&   /* 3. Brake =0  (BLS=0)        */
        #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
            (mpress<=AxOFS_MPRESS_THR)                                                                    &&   /* 4. Brake =0  (Mpress<=5bar) */ /* Think More */
        #else
            (lsesps16MpressByAHBgen3<=AxOFS_MPRESS_THR)                                                   &&   /* 4. Brake =0  (Mpress<=5bar) */ /* Think More */        
        #endif
            (PARKING_BRAKE_OPERATION==0)                                                                  &&   /* 5. Brake =0  (Parking=0)    */ 
            (lsabsu1AxMTPAfterFlg==1)                                      																&&   /* 6. mtp   < 3%  : 200ms      */
            (lsabsu1AxEngineDrag==0)                                    																	&&   /* 7. Engine   Drag Check      */  /*�ŷ� flag */
            (lsabsu1AxDriveModeDrag==0)                                 																	&&   /* 8. Drv Mode Drag Check      */ 
            (lsabsu1AxBrakeAfter==1)                                    																	&&   /* 9. Brake After              */
            (lsabsu1AxOfsAccAftFlag==1)                                 																	&&   /*10. Accel After              */
            (BACKWARD_MOVE==0)                                          																	&&   /*11. Backward=0               */ 
            (BACK_DIR==0)                                               																	&&      
            (lsabsu1AxOfsWheelSlipFlg==0 )                              																	&&   /*12. Wheel Slip=0             */ /* Think More */
            (lsabsu1AxDyOffsetFlat==1)                                  																	&&   /*13. Dynmic Zero gradient     */  
            (McrAbs(yaw_out)<= AxOFS_YAW_THR)                              																    &&   /*14. Yaw <=  5'/s             */         
            (ABS_fz==0)                                                 																	&&   /*15. ABS_fz=0                 */      	          	         	 
        	  (ESP_ROUGH_ROAD==0)                                         																	&&   /*16. Rough =0                 */
            (ESP_TCS_ON==0)																																								&&
            (YAW_CDC_WORK==0)&&(BTC_fz==0)&&(ETCS_ON==0)&&(FTCS_ON==0)  																	&&   /*17. Control = 0              */
            (FLAG_ACTIVE_BRAKING==0)                                    																	&&                                 
            (vdc_error_flg==0)                                                                            &&   /*18. vdc_err_flag ??          */ 
            ( (gear_state==0) ||(gear_state==4) || (gear_state==5) || (gear_state==6) )                   &&   /*19. Gear 4th or 5th 6th      */
            (lsabsu1AxOfsSportModeFlg==0)                                                                                                   &&   /*20. Not Sports Mode on A/T  */
            (lsabsu1AxOfsWiperOnFlg==0)                                                                   	   /*	21. Not Wiper Act Mode   */						
						#if __TSP
						&&(TSP_ON==0)																		   /* 22. Not On TSP Control */
						#endif
						#if ((__PARK_BRAKE_TYPE == CAN_TYPE)||(__PARK_BRAKE_TYPE == ANALOG_TYPE))
						&&(ldabsu1ParkingBrkActSignal==0)
						#endif                                                                      
        	)
        	/* swd11_kjs */      
  #endif                          
        {                       
          lsabss16AxOfsDyModelRaw = lsabss16AxSenAirSus - lsabss16AxDynModel; /* lsabss16AxOfsDyModelRaw Resol 0.001 */
 
 			    if(lsabsu16AxOffCNT < L_U8_TIME_10MSLOOP_1000MS)
 			    {
 				    lsabss32AxOfsDyModelRawSum = lsabss32AxOfsDyModelRawSum + lsabss16AxOfsDyModelRaw;
 				    lsabsu16AxOffCNT=lsabsu16AxOffCNT+1; 								
 			    }
 			    else
 			    {
  /*----------------- Ax Offset Estimation --------------------------------------*/				
 				    ALGoff = (struct LongG_offset *) &ALGoff1;   
 				    /*------- Calculate offset */  
 				    LSABS_vCalcAx100secOffset(lsabss32AxOfsDyModelRawSum);
                
 							    			    
			  	  lsabsu16AxOffCNT=0;
			  	  lsabss32AxOfsDyModelRawSum=0;		
 							
 			    }
 
	        lsabsu1FreeRollFlag=1;      	  
        }
        else
        {
      	 	lsabsu1FreeRollFlag=0;
        }             
}


/* 20110615_KJS */
#if (__KINE_MODEL_AX_OFFSET ==ENABLE) 
/*------------------ Kinematic Model Ax Offset Calculation -----------------------*/
void     LSABS_vCalcAxOffsetKinematModel(void)
{
        /*----------  Kinematic G offset Estimation --------------------- */    	
  #if defined (__ABS_ONLY_SYSTEM_CODE )    
        if( (lsabsu1AxOfsCalcInhibit==0)                                &&       /* 1. Inhibit =0               */       
            ((vref >=AxOFS_VELO_MIN_THR)&&(vref <=AxOFS_VELO_MAX_THR) ) &&       /* 2. vref >=40 && vref <=90   */
            (BLS==0)                                                    &&       /* 3. Brake =0  (BLS=0)        */
            (PARKING_BRAKE_OPERATION==0)                                &&       /* 4. Brake =0  (Parking=0)    */
            (mtp <= AxOFS_MTP_THR)                                      &&       /* 5. mtp   < 3%  : 200ms      */
            (lsabsu1AxEngineDrag==0)                                    &&       /* 6. Engine   Drag Check      */  /*�ŷ� flag */
            (lsabsu1AxDriveModeDrag==0)                                 &&       /* 7. Engine   Drag Check      */  /*�ŷ� flag */
            (lsabsu1AxBrakeAfter==1)                                    &&       /* 8. Brake After              */
            (lsabsu1AxOfsAccAftFlag==1)                                 &&       /* 9. Accel After              */
            (BACKWARD_MOVE==0)                                          &&       /*10. Backward=0               */ 
            (lsabsu1AxOfsWheelSlipFlg==0 )                              &&       /*11. Wheel Slip=0             */ /* Think More */
            (lsabsu1AxKineOffsetFlat==1)                                &&       /*12. Kinematic Zero Gradient  */
            (ABS_fz==0)                                                          /*13. ABS fz=0                 */        	  
        	  )         	                                    
  #else

        if( (lsabsu1AxOfsCalcInhibit==0)                                                                  &&      /* 1. Inhibit =0               */ 
            ((vref >=AxOFS_VELO_MIN_THR)&&(vref <=AxOFS_VELO_MAX_THR) )                                   &&      /* 2. vref >=40 && vref <=90   */
            (BLS==0)                                                                                      &&      /* 3. Brake =0  (BLS=0)        */
        #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
            (mpress<=AxOFS_MPRESS_THR)                                                                    &&   /* 4. Brake =0  (Mpress<=5bar) */ /* Think More */
        #else
            (lsesps16MpressByAHBgen3<=AxOFS_MPRESS_THR)                                                   &&   /* 4. Brake =0  (Mpress<=5bar) */ /* Think More */        
        #endif
            (PARKING_BRAKE_OPERATION==0)                                                                  &&      /* 5. Brake =0  (Parking=0)    */ 
            (lsabsu1AxMTPAfterFlg==1)                                                                     &&      /* 6. mtp   < 3%               */
            (lsabsu1AxEngineDrag==0)                                                                      &&      /* 7. Engine   Drag Check      */  /*�ŷ� flag */
            (lsabsu1AxDriveModeDrag==0)                                                                   &&      /* 8. Drv Mode Drag Check      */
            (lsabsu1AxBrakeAfter==1)                                                                      &&      /* 9. Brake After              */
            (lsabsu1AxOfsAccAftFlag==1)                                                                   &&      /*10. Accel After              */
            (BACKWARD_MOVE==0)                                                                            &&      /*11. Backward=0               */ 
            (BACK_DIR==0)                                                                                 && 
            (lsabsu1AxOfsWheelSlipFlg==0 )                                                                &&      /*12. Wheel Slip=0             */ /* Think More */
            (lsabsu1AxKineOffsetFlat==1)                                                                  &&      /*13. Kinematic Zero gradient  */ 
            (McrAbs(yaw_out)<= AxOFS_YAW_THR)                                                             &&      /*14. Yaw <=  5'/s             */ 
            (ABS_fz==0)                                                                                   &&      /*15. ABS_fz=0                 */
            (ESP_ROUGH_ROAD==0)                                                                           &&      /*16. Rough =0                 */
            (ESP_TCS_ON==0)                                                                               &&
            (YAW_CDC_WORK==0)&&(BTC_fz==0)&&(ETCS_ON==0)&&(FTCS_ON==0)                                    &&      /*17. Control = 0              */
            (FLAG_ACTIVE_BRAKING==0)                                                                      &&
            (vdc_error_flg==0)                                                                            &&      /*18. vdc_err_flag ??          */ 
            ( (gear_state==0) ||(gear_state==4) || (gear_state==5) || (gear_state==6) )                   &&      /*19. Gear 4th or 5th 6th      */
            (lsabsu1AxOfsSportModeFlg==0)                                                                         /*20. Not Sports Mode on A/T   */
          )
  #endif                          
        {                       

          lsabss16AxOfsKModelRaw= lsabss16AxSenAirSus  - (int16_t)( (((LONG)FZ1.filter_out)*125)/128 ) ;  
 
 			    if(lsabsu16AxOffKineCNT< L_U8_TIME_10MSLOOP_1000MS )
 			    {
 				    lsabss32AxOfsKModelRawSum= lsabss32AxOfsKModelRawSum + lsabss16AxOfsKModelRaw;
 				    lsabsu16AxOffKineCNT=lsabsu16AxOffKineCNT+1; 								
 			    }
 			    else
 			    {
  /*----------------- Ax Offset Kinematic Estimation --------------------------------------*/				
 				
				    ALGoff = (struct LongG_offset *) &ALGoff2;   
				    
 				    /*------- Calculate offset */  
 				    LSABS_vCalcAx100secOffset(lsabss32AxOfsKModelRawSum);
 				     				
 							    			    
			  	  lsabsu16AxOffKineCNT=0;
 				    lsabss32AxOfsKModelRawSum=0;
			
 			    }
 
	          lsabsu1AxOfsFreeRollKineFlg=1;      	  
        }
        else
        {
      	    lsabsu1AxOfsFreeRollKineFlg=0;
        }             
}
#endif
/* 20110615_KJS */

/*------------------ Ax Offset Calculation ------------------*/

void     LSABS_vCalcAx100secOffset(long AxOffsetSUM )
{
	int16_t    S16AxOffset1secMean;
	int16_t    S16Ax100secOffMeanOld;
  

  /*------------  Ax Offset 1sec  Mean --------------------*/		
	S16AxOffset1secMean= (int16_t)(AxOffsetSUM/L_U8_TIME_10MSLOOP_1000MS);
	
  /*------------  Ax Offset 10sec Mean --------------------*/		
	if(ALGoff->lsabsu8Ax1secOffsetCnt < 9)
	{	
			/*---------- First 1sec offset calculation --------------*/
			if(ALGoff->lsabsu1Ax1secFirstOK==0)
		{
					ALGoff->lsabsu1Ax1secFirstOK=1;
		}
		else
		{
					;
		}

			ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] = S16AxOffset1secMean;
		
			if(ALGoff->lsabsu8Ax1secOffsetCnt == 0)
		{
		  ALGoff->lsabss16Ax1secOffMAX = ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
		  ALGoff->lsabss16Ax1secOffMIN = ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
					ALGoff->lsabss16Ax10secOffSum=ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt]; 			    											
			}
			else
			{
			  			
  		    if( ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] > ALGoff->lsabss16Ax1secOffMAX )
      {			
  		    		ALGoff->lsabss16Ax1secOffMAX=ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
        }
  		    else if( ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] < ALGoff->lsabss16Ax1secOffMIN )
        {
  		      	ALGoff->lsabss16Ax1secOffMIN=ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
        }
        else
        {
    	    ;
        }
  		    ALGoff->lsabss16Ax10secOffSum=ALGoff->lsabss16Ax10secOffSum + ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] ;  		       		    
      
      }
                              
  		ALGoff->lsabsu8Ax1secOffsetCnt=ALGoff->lsabsu8Ax1secOffsetCnt+1;	 	    
		
	}
	else
	{
				
		ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] = S16AxOffset1secMean;			

    if( ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] > ALGoff->lsabss16Ax1secOffMAX )
    {
    	ALGoff->lsabss16Ax1secOffMAX=ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
    }
    else if( ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] < ALGoff->lsabss16Ax1secOffMIN )
    {
      ALGoff->lsabss16Ax1secOffMIN=ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt];
    }
    else
    {
    	;
    }
    
	    ALGoff->lsabss16Ax10secOffSum=ALGoff->lsabss16Ax10secOffSum + ALGoff->lsabss16Ax1SecOfs[ALGoff->lsabsu8Ax1secOffsetCnt] ;
	    	    
	    ALGoff->lsabss16Ax10secOffMean = (int16_t)(ALGoff->lsabss16Ax10secOffSum/10);	
    
			if( ( ALGoff->lsabss16Ax1secOffMAX -  ALGoff->lsabss16Ax1secOffMIN ) <= Ax1secOffsetTHR  )
			{
		    	ALGoff->lsabsu1Ax10secOffOK	= 1 ;
		    	ALGoff->lsabsu1Ax10secOffSusp=0 ;
			}
			else
			{
					ALGoff->lsabsu1Ax10secOffOK	= 0 ;
	}
	
  /*------------  Ax Offset 10sec*10 (100sec) Moving Average -----------------*/			
  if(ALGoff->lsabsu1Ax10secOffOK ==1)
  {
    if(ALGoff->lsabsu1Ax100secOffOK==1)
	  {
      if(ALGoff->lsabsu8Ax10secOffsetCnt>9)
      {
    	  ALGoff->lsabsu8Ax10secOffsetCnt=0;
      }
      else
      {
    	  ;
      }
      
              S16Ax100secOffMeanOld           = ALGoff->lsabss16Ax100secOffMean; 
      
              ALGoff->lsabss16Ax100secOffSum  = ALGoff->lsabss16Ax100secOffSum - ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt] + ALGoff->lsabss16Ax10secOffMean; 	    	
      	    	ALGoff->lsabss16Ax100secOffMean = (int16_t)(ALGoff->lsabss16Ax100secOffSum/10);      
      
      ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt] = ALGoff->lsabss16Ax10secOffMean ;
      ALGoff->lsabsu8Ax10secOffsetCnt=ALGoff->lsabsu8Ax10secOffsetCnt+1;
      
	    /*------ Ax offset Limitation ---------*/
	          	if( McrAbs(ALGoff->lsabss16Ax100secOffMean) <  AxOFFSETLimit )
	    {
	    	lsabsu1AxOffsetSuspect=0;
	          	
	          			if(ALGoff->lsabss16Ax100secOffMean > (S16Ax100secOffMeanOld + Ax100secOffsetUpdateLimit ))
	    	{
	          					ALGoff->lsabss16Ax100secOffMean= S16Ax100secOffMeanOld + Ax100secOffsetUpdateLimit;
	    	}
	          			else if(ALGoff->lsabss16Ax100secOffMean < (S16Ax100secOffMeanOld - Ax100secOffsetUpdateLimit ))
	    	{
	          					ALGoff->lsabss16Ax100secOffMean= S16Ax100secOffMeanOld - Ax100secOffsetUpdateLimit;
	    	}
	    	else
	    	{
	    		;
	    	}	
	    }
	    else
	    {
	          			if(ALGoff->lsabss16Ax100secOffMean >= AxOFFSETLimit)
	    	{
	          			  	ALGoff->lsabss16Ax100secOffMean= AxOFFSETLimit;  			    		
	    	}
	          			else if(ALGoff->lsabss16Ax100secOffMean <= -AxOFFSETLimit )
	    	{
	          					ALGoff->lsabss16Ax100secOffMean= -AxOFFSETLimit;  
	    	}
	    	else
	    	{
	    		;
	    	}
        
     	  lsabsu1AxOffsetSuspect=1; /* Need After Detecting Ax Suspect */	  
	    }		        	    	    	  
  	        
    }
    else
    {

	  	/*------ First 10sec offset calculation ---------*/
	  	if(ALGoff->lsabsu1Ax10secFirstOK==0)
	  	{
	  		ALGoff->lsabsu1Ax10secFirstOK=1;
	  	}
	  	else
	  	{
	  		;
	  	}
	  		
             	    ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt] = ALGoff->lsabss16Ax10secOffMean ;	    		
	        		
	      			if(ALGoff->lsabsu8Ax10secOffsetCnt < 9)	
		  {	
	            
	              	    if(ALGoff->lsabsu8Ax10secOffsetCnt==0)
		  	{
	        		        ALGoff->lsabss16Ax100secOffSum  = ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt];
	      	  	            ALGoff->lsabss16Ax100secOffMean = ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt];     
		  	}
		  	else
		  	{
                            ALGoff->lsabss16Ax100secOffSum  = ALGoff->lsabss16Ax100secOffSum + ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt];
	      	  	        	ALGoff->lsabss16Ax100secOffMean = (int16_t)(ALGoff->lsabss16Ax100secOffSum/(ALGoff->lsabsu8Ax10secOffsetCnt+1));			  			    	  		
		  	}
		  	
	              	    ALGoff->lsabsu8Ax10secOffsetCnt = ALGoff->lsabsu8Ax10secOffsetCnt+1;		  			    
	      		      
	    }
	    else
	    {
	      	  			ALGoff->lsabsu1Ax100secFirstOK  = 1;	  	  				  	  	
	      	  			ALGoff->lsabsu1Ax100secOffOK    = 1;
	      
	      	  			ALGoff->lsabss16Ax100secOffSum  = ALGoff->lsabss16Ax100secOffSum + ALGoff->lsabss16Ax10SecOfs[ALGoff->lsabsu8Ax10secOffsetCnt];
	      	  			ALGoff->lsabss16Ax100secOffMean = (int16_t)(ALGoff->lsabss16Ax100secOffSum/(ALGoff->lsabsu8Ax10secOffsetCnt+1));		  	  			
	      	  			ALGoff->lsabsu8Ax10secOffsetCnt = 0;
	      
	    }
	  }  	
  	
  }
  else 
  {
  	;
  }  	  		  			

	  ALGoff->lsabsu8Ax1secOffsetCnt = 0;  
	  ALGoff->lsabss16Ax10secOffSum  = 0;

	}
	 		  			

}

/* 20110615_KJS */
#if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE) 
/*------------------ StandState Ax Offset Calculation -----------------------*/
void     LSABS_vCalcAxOffsetStandState(void)
{
	/*----- Stand State Long G offset Concept---------/
	/  1. All Wheel Speed Zero
	/  2. Gear N Position
	/  3. No brake No accel No parking brake
	/  4. Long G Sensor no variation 
	/  5. Vehicle starts within 100sec
	/  6. 5 th Continual
	-------------------------------------------------*/
	
	if((FL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (FR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && 
		 (RL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (RR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && 
		 (gs_ena != 0) && (McrAbs(lss16absAx1000gDiff)<5) &&
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
		 (PARKING_BRAKE_OPERATION==0) && (mpress<=50) && (BLS==0) && (mtp <= 10) )
    #else
		 (PARKING_BRAKE_OPERATION==0) && (lsesps16MpressByAHBgen3<=50) && (BLS==0) && (mtp <= 10) )    
    #endif
	{
		lsabsu16AxStandCnt=lsabsu16AxStandCnt+1;
		
		if(lsabsu16AxStandCnt<L_U16_TIME_5MSLOOP_100S)
		{
			if(lsabsu16AxStandCnt<L_U16_TIME_5MSLOOP_5S)
			{
			  lsabss32AxStandSum=lsabss32AxStandSum+lsabss16AxSenAirSus;
			}
			else
			{
				lsabss16AxOffset5secStand=(int16_t)(lsabss32AxStandSum/L_U16_TIME_5MSLOOP_5S);
				lsabsu1AxOffsetStand5secOK=1;
			}
		}
		else
		{
			lsabsu1AxOffsetStand5secOK=0;
		}
	}
	else
	{
		lsabsu16AxStandCnt=0;
		lsabss32AxStandSum=0;
		
		if(lsabsu1AxOffsetStand5secOK==1)
		{
			if(lsabsu1AxOffsetStand5sFirstOK==0)
			{
				lsabsu1AxOffsetStand5sFirstOK=1;
				lsabss16AxOffsetStandMax=lsabss16AxOffset5secStand;
				lsabss16AxOffsetStandMin=lsabss16AxOffset5secStand;
			}
			else
			{
				;
			}
			
			lsabss16AxOffsetStand[lsabsu8AxOffset5secStandCnt]=lsabss16AxOffset5secStand;
						
			/*----------  Stand still Max/Min Calc ----------*/
			if(lsabss16AxOffset5secStand > lsabss16AxOffsetStandMax )
			{
				lsabss16AxOffsetStandMax= lsabss16AxOffset5secStand;
			}
			else if(lsabss16AxOffset5secStand < lsabss16AxOffsetStandMin)
			{
				lsabss16AxOffsetStandMin= lsabss16AxOffset5secStand;
			}
			else
			{
				;
			}									
				
			/*----------  5 count OK  ----------*/			
			if(lsabsu8AxOffset5secStandCnt<5)
			{									
				lsabsu8AxOffset5secStandCnt=lsabsu8AxOffset5secStandCnt+1;
			}
			else
			{				
				if(lsabsu1AxOffsetStnd5s5cntFOK==0)
				{
					lsabsu1AxOffsetStnd5s5cntFOK=1;
				}
				else
				{
					;
				}	
				
				lsabsu8AxOffset5secStandCnt=0;
			}
			
			/*----------  Final Stand Still offset  ----------*/
      if(lsabsu1AxOffsetStnd5s5cntFOK==1)
      {
      	if(lsabss16AxOffsetStandMax < (lsabss16AxOffsetStandMin+ Ax5secStandStillTHR))
      	{
      		lsabss16AxOffset5sec5cntMean=(lsabss16AxOffsetStand[0]+lsabss16AxOffsetStand[1]+lsabss16AxOffsetStand[2]+lsabss16AxOffsetStand[3]+lsabss16AxOffsetStand[4] )/5 ;
      		
      		if( McrAbs(lsabss16AxOffset5sec5cntMean) < AxOFFSETLimit)
      		{
      		  lsabsu1AxOffsetStandStillOK=1;
      		  lsabsu1AxOffsetStandStillSus=0;      			
      		}
      		else
      		{
      			lsabsu1AxOffsetStandStillOK=0;
      			lsabsu1AxOffsetStandStillSus=1; 
      		}
      	}
      	else
      	{
      		lsabss16AxOffset5sec5cntMean=0;
      		lsabsu1AxOffsetStandStillOK=0;
      		lsabsu1AxOffsetStandStillSus=1;
      	}		
      }
      else
      {
      	;
      }
							
			
			lsabsu1AxOffsetStand5secOK=0;
		}
		else
		{
			;
		}
		
		
	}
	
	
}
#endif
/* 20110615_KJS */

/*------------------ EEPROM  ---------------------------------------*/
void    LSABS_vDetAxOffsetEEPwrite(void)
{
  
    if((ALGoff1.lsabsu1Ax100secOffOK==1)||(ALGoff1.lsabsu8Ax10secOffsetCnt >= AxOFS_DRV_TOTAL_TIME))
    {
        /*--- EEPROM WRITING CHECK ---*/
        if(wbu1AxDrivingOfsReadEep ==1 )
        {
            if( McrAbs( lsabss16AxOffsetDrvf  - wbs16AxDrivingOfsFirstEep) > AxDRVEEP_WRT_THR  )
            {
                lsabsu1AxDrvOfsEEPwriteReq = 1; 
            }
            else
            {
                lsabsu1AxDrvOfsEEPwriteReq = 0; 
            }
        }
        else
        {
            lsabsu1AxDrvOfsEEPwriteReq = 1;
        }
    }
    else  
    {
          lsabsu1AxDrvOfsEEPwriteReq = 0; 
    }  
      
    if(fu1AxErrorDet==1)
    {
    	lsabss16AxOffsetDrvf = 0;
        lsabsu1AxDrvOfsEEPwriteReq = 1;
    }
    else
    {
        ;
    }  
      
}

#endif   /* (__Ax_OFFSET_MON_DRV ==1) */

#endif   /* (__Ax_OFFSET_MON==1)||(__Ax_OFFSET_MON_DRV==1 ) */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSABSEstLongAccSensorOffsetMon
	#include "Mdyn_autosar.h"
#endif

