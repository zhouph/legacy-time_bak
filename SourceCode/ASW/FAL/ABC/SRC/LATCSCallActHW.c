/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LATCSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LATCSCallActHW.h"
#include "LCTCSCallControl.h"
#include "LSENGCallSensorSignal.h"
#include "LCRDCMCallControl.h"
#include "LACallMain.h"
#include "Hardware_Control.h"
#include "LCTCSCallBrakeControl.h"
#include "LABTCSCallActHW.h"
#include "LSCANSignalInterface.h"
#include "LCESPCalLpf.h"
#include "ETCSGenCode/LCETCS_vCallMain.h" /* MBD ETCS */

#if __FBC
extern struct bit_position FBCF00;
#define FBC_ON                          FBCF00.bit2
#endif


/* Variables Definition*******************************************************/


/* GlobalFunction prototype **************************************************/


/* LocalFunction prototype ***************************************************/
void LATCS_vControlFuncLamp(void);

/* Implementation*************************************************************/
#if __TCS || __BTC || __ETC
void LATCS_vCallActHW(void)
{
    LATCS_vControlFuncLamp();
}
    
void LATCS_vControlFuncLamp(void)
{
    if((lcetcsu1FuncLampOn==1)||(AY_ON)||((YAW_CDC_WORK)&&(!ABS_fz))) 
    {/*MBD ETCS*/
		if( (YAW_CDC_WORK)||(lcetcsu1FuncLampOn==1) ) 
		{/*MBD ETCS*/
            FUNCTION_LAMP_ON=1;
            lamp_on_delay=0;
        }
        else
        {/* Engine Intervention�� Lamp ���� */
		    if( (!YAW_CDC_WORK) ) /*MBD ETCS*/ 
            {
                FUNCTION_LAMP_ON=0;
                lamp_on_delay=0;
            }
            else
            {
                lamp_on_delay++;
                if (lamp_on_delay>20)
                {
                    FUNCTION_LAMP_ON=1;
                    lamp_on_delay=0;
                }
                else
                {
                    ;
                }
            }
        }
    }
    else
    {
        FUNCTION_LAMP_ON=0;
        lamp_on_delay=0;
    }

#if __TSP
    if(TSP_ON)
    {
        FUNCTION_LAMP_ON=1;
        lamp_on_delay=0;
    }
    else
    {
        ;
    }
#endif
/* 2005.08.30 added by eslim for FBC */
#if __FBC
        if(FBC_ON)
        {
            FUNCTION_LAMP_ON=1;
            lamp_on_delay=0;
        }
        else
        {
            ;
        }
#endif
/* 2005.08.30 added by eslim for FBC */

    if ((vdc_on_flg==0)&&(lctcss16BrkCtrlMode==S16_TCS_CTRL_MODE_DISABLE)&&(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE))
    {
      #if __TCS_DRIVELINE_PROTECT
    	if(( ETCS_ON==1) || (FTCS_ON==1) || (BTC_fz==1))
    	{
    		FUNCTION_LAMP_ON=1;
        	lamp_on_delay=0;
    	}
    	else
    	{
    		FUNCTION_LAMP_ON=0;
    		lamp_on_delay=0;
    	}
      #else
        ETCS_ON = FTCS_ON = AY_ON = BTC_fz = ESP_TCS_ON = 0;
        FUNCTION_LAMP_ON=0;
        lamp_on_delay=0;
      #endif
    }
  
  #if _ESC_OFF_ROP_FUNCITON_LAMP_OFF    
    else if ((vdc_on_flg==1)&&(Flg_ESC_SW_ROP_OK==1))
	{
		FUNCTION_LAMP_ON=0;
		lamp_on_delay=0;		
	}    	
  #endif	
	else
	{
		;
	}  
}
    
#endif /*__TCS || __BTC || __ETC*/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LATCSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
