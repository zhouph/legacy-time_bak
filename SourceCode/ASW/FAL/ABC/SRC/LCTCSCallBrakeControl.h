#ifndef __LCTCSCALLBRAKECONTROL_H__
#define __LCTCSCALLBRAKECONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition******************************************************/


#if	defined(BTCS_TCMF_CONTROL) /* BTCS_TCMF_CONTROL */

#define Motor_0_25_Vol                 		250 /*resolution 0.001= 250-> 0.25 V */
#define TCSHighMuNotDcted      				0    
#define TCSLeftHighMu          				1    
#define TCSRightHighMu         				2    
#define TCSHighMuStable        				1    
#define TCSHighMuUnStable      				2  
#define TCSPgainFactor100pcnt  				100
#define TCSHillMovDistanceMAX  				60000  /*lctcsu1DctBTCHillByAx 판단 Hill 이동거리 Max 60미터 */
#define TCSStpNMovDistanceOffSet10Cm 		100 /*Stop후 0.5kph로 이동 시작시 움직인 거리 Offset 가정 10cm*/
#define MOVING_WINDOW_MAX  					32 /*Moving Window Max Scan*/
#define S16TCSAsymSpinFwdCtlMode     		0
#define S16TCSAsymSpinBwdCtlMode     		1     
#define TCSMinimumCurrentLevel 				1 /*최소 TC 전류 인가 값 0.001A*/

//#define S16TCSCpMaxTCCurnt 					1600	//1600 /* resolution 0.001  Maximum current to exert on TC valve */
//#define S16TCSCpTarWhlPreStMax 				1600 /*160bar Maximum Target Pressure*/  
//#define U8TCSCpAfterIIntActScan 			150 /*1.5s  after IIntAct Time*/
//#define U8TCSCpAveWhlAcc_1					14		/* 0.14g */				
//#define U8TCSCpAveWhlAcc_2					210		/* 2.10g */
//#define U8TCSCpChngRateWhlSpin_1 			14     /*0.14g   Change rate of wheel spin condition 1*/
//#define U8TCSCpChngRateWhlSpin_2 			210    /*2.105g   Change rate of wheel spin condition 2*/
//#define U8TCSCpCurntFiltGain				14	/* 2Hz */
//#define U8TCSCpFltGain4GearShiftSt   		20 /*20: L_U8FILTER_GAIN_10MSLOOP_3HZ*/ 
//#define U8TCSCpFltGain4GearShiftTurn 		76 /*76 L_U8FILTER_GAIN_10MSLOOP_23HZ*/
//#define U8TCSCpLowPresTh4IniTCVCtl      	50 /*5 bar*/
//#define U8TCSCpMonTime4AsymSpnCtlS_1 		10 /*scan 100ms Monitoring time for activation of asymmetric spin control when ltcss16ChngRateWhlSpinDiffNF is U8TCSCpChngRateWhlSpin_1*/
//#define U8TCSCpMonTime4AsymSpnCtlS_2 		3  /*scan 10ms  Monitoring time for activation of asymmetric spin control when ltcss16ChngRateWhlSpinDiffNF is U8TCSCpChngRateWhlSpin_2*/    
//#define U8TCSCpMonTime4SymSpnCtlS_1			10		
//#define U8TCSCpMonTime4SymSpnCtlS_2			3
//#define U8TCSCpSymDctScanTime 				8	 
//#define U8TCSCpThOfCnt4SpnDiffSlopeDct 		4 /*Threshold of counter for spin difference slope detection*/
//
//#define S16TCSCpSplitHilExtDistanceTh 		20000 /*20m*/
//#define U8TCSCpSplitHilDctAccTh  			20 /*0.1g Hill 판단 ax threshold*/
//#define U8TCSCpSplitHilDctEnterCnt  		20  
//#define U8TCSCpSplitHilDctExitCnt 			10
//#define U8TCSCpSplitHilDctSpdTh  			18*8 /*Hill 판단 vref , 이값 이하에서 감지*/
//#define U8TCSHillDctAlongTh  				10 /* 0.1g  Hill grade thres for Hill Detection*/
//
//#define U8TCSCpAsmdMuDiff4SplitMuDct  		60 /* 0.6g 좌우 Mu diff - Split 판단 Threshold  */
//#define U8TCSCpSplitMuDctEnterCnt  			25  /* 25scan  130ms Split판단 Enter cnt 시간 */
//#define U8TCSCpSplitMuDctExitCnt   			20 /* 20scan 200ms  Split 판단 해제 Exit cnt 시간*/
//#define U8TCSCpSplitMuDiffExitHyst 			50  /*좌우 mu diff split 해제 systerisis 0.6g-0.5g = 0.1g*/
//                                                
//                                              
///*HW Setting*/
//#define S16TCSCpTCcrnt_1            	 	(U8_TCMF_HW_06*10)   // 0   //S16_TCS_RESERVED_06 //0                          
//#define S16TCSCpTCcrnt_2            	 	(U8_TCMF_HW_07*10)   // 380 //S16_TCS_RESERVED_07 //450 //임시 400             
//#define S16TCSCpTCcrnt_3            	 	(U8_TCMF_HW_08*10)   // 530  //S16_TCS_RESERVED_08 //800 //700                 
//#define S16TCSCpTCcrnt_4            	 	(U8_TCMF_HW_09*10)   // 800  //S16_TCS_RESERVED_09 //1050 //950                
//#define S16TCSCpTCcrnt_5            	 	(U8_TCMF_HW_10*10)   // 1450 //S16_TCS_RESERVED_10 //1400 //1350               
//#define S16TCSCpWhlPre_1  			 	 	(U8_TCMF_HW_01*10)   //S16_TCS_RESERVED_01 //0    /*resolution 0.1 Pressure point 1 of TC valve characteristic curve  */
//#define S16TCSCpWhlPre_2  			 	 	(U8_TCMF_HW_02*10)   //S16_TCS_RESERVED_02 //10   /*resolution 0.1 Pressure point 2 of TC valve characteristic curve  */
//#define S16TCSCpWhlPre_3  			 	 	(U8_TCMF_HW_03*10)   //S16_TCS_RESERVED_03 //100   /*resolution 0.1 Pressure point 3 of TC valve characteristic curve  */
//#define S16TCSCpWhlPre_4  			 	 	(U8_TCMF_HW_04*10)   //S16_TCS_RESERVED_04 //250   /*resolution 0.1 Pressure point 4 of TC valve characteristic curve  */
//#define S16TCSCpWhlPre_5  			 	 	(U8_TCMF_HW_05*10)	  //S16_TCS_RESERVED_05 //900  /*resolution 0.1 Pressure point 5 of TC valve characteristic curve  */
//#define U8TCSCpSclFctTrq2PresFrt    	 	 U8_TCMF_HW_19 	  //S8_TCS_RESERVED_19 //50 /*33 Traverse 제어토크 ->압력 변환 Factor U8TCSCpSclFctTrq2PresFrt=2×μ_(b,front)×A_front×r_(b,front)×g;*/
//#define U8TCSCpSclFctTrq2PresRer		 	 U8_TCMF_HW_20 	  //S8_TCS_RESERVED_20 /*33 Traverse 엔지니어가 계산 후 반영U8TCSCpSclFctTrq2PresRer=2×μ_(b,rear)×A_rear×r_(b,rear)×g;*/    
//
///*Motor Control*/
//#define S16TCSCpAddMtrV4SPHillHghMuStab		1000 /*Split Hill 감지 시 wheel on high-mu 가 stable 할때 더해지는 motor voltage */
//#define S16TCSCpAddMtrV4SPHillHghMuUnSt 	7000 /*Split Hill 감지 시 wheel on high-mu 가 unstable 할때 더해지는 motor voltage */
//#define S16TCSCpFBMtrV_ASC          		(U8_TCMF_HW_15*100)	//2000//U16_TCS_RESERVED_14 //3000                          
//#define S16TCSCpFBMtrV_Turn   				2000       /*2Volt 선회시 */
//#define S16TCSCpFFMtrV_ASC          		(U8_TCMF_HW_14*100)	//2500//U16_TCS_RESERVED_12 //10000                         
//#define S16TCSCpFFMtrV_Turn   				3000       /*2Volt 선회시 */
//#define S16TCSCpIniRMdeMtrV_ASC     		(U8_TCMF_HW_13*100)	//3000//U16_TCS_RESERVED_11 //10000                         
//#define S16TCSCpIniRMdeMtrV_SC      		(U8_TCMF_HW_16*100)	//3000//U16_TCS_RESERVED_15 //9000                          
//#define S16TCSCpIniRMdeMtrV_Turn  			3000    /*4Volt 선회시 Initial Rise Voltage */
//#define S16TCSCpNormMtrV_SC         		(U8_TCMF_HW_17*100)	//2000//U16_TCS_RESERVED_16 //2000       
//
///*Initial Control*/
//#define U8TCSCpAsymSpnCtlFFmodeTimeUp		100	/* 1sec */                   
//#define U8TCSCpCurntIncRateInFF 			U8_TCMF_FF_CONTROL_30 //8//U8_TCS_RESERVED_06//8   /* resolution 0.001  Current increase rate in feedforward mode  */
//#define U8TCSCpIniMtrCtlModeTime    		U8_TCMF_HW_11 //8//U16_TCS_RESERVED_17 //12                            
//#define U8TCSCpIniMtrCtlModeTimeInTurn 		8 /*8 scan   선회시 Initial Motor Voltage Max적용 Scan*/
//#define U8TCSCpIniPres    					U8_TCMF_FF_CONTROL_29 //60//  S8_TCS_RESERVED_18 //70 /* resolution 0.1 Expected pressure level right after initial rise mode finished */
//#define U8TCSCpIniTCVCtlModeTime			U8_TCMF_HW_12 //8//	U16_TCS_RESERVED_17                                
//#define U8TCSCpIniTCVCtlModeTimeHill		1
//#define U8TCSCpIniTCVCtlModeTimeHunt		0
//#define U8TCSCpIniTCVCtlModeTimeInTurn 		1 /* 8 scan   선회시 Initial TC Current Max적용 Scan*/
//
///*Turnning Index*/
//#define S16TCSCpDctTurnIndexEnterAyTh_1    (U8_TCMF_HW_21*10) //400  /*Speed1 에서의 선회 판단 시작 Threshold  Ay 0.4G*/  
//#define S16TCSCpDctTurnIndexEnterAyTh_2    (U8_TCMF_HW_22*10) //400  /*Speed2 에서의 선회 판단 시작 Threshold  Ay 0.4G*/  
//#define S16TCSCpDctTurnIndexEnterAyTh_3    (U8_TCMF_HW_23*10) //350  /*Speed3 에서의 선회 판단 시작 Threshold  Ay 0.35G*/ 
//#define S16TCSCpDctTurnIndexEnterAyTh_4    (U8_TCMF_HW_24*10) //300  /*Speed4 에서의 선회 판단 시작 Threshold  Ay 0.3G*/  
//#define S16TCSCpDctTurnIndexEnterAyTh_5    (U8_TCMF_HW_25*10) //300  /*Speed5 에서의 선회 판단 시작 Threshold  Ay 0.3G*/  
//#define S16TCSCpDctTurnIndexFinalAyTh_1    (U8_TCMF_HW_26*10) //550  /*Speed1 에서의 선회 판단 확정  Threshold  Ay 0.55G*/
//#define S16TCSCpDctTurnIndexFinalAyTh_2    (U8_TCMF_HW_27*10) //550  /*Speed2 에서의 선회 판단 확정 Threshold  Ay 0.55G*/ 
//#define S16TCSCpDctTurnIndexFinalAyTh_3    (U8_TCMF_HW_28*10) //500  /*Speed3 에서의 선회 판단 확정 Threshold  Ay 0.5G*/  
//#define S16TCSCpDctTurnIndexFinalAyTh_4    (U8_TCMF_HW_29*10) //450  /*Speed4 에서의 선회 판단 확정 Threshold  Ay 0.45G*/ 
//#define S16TCSCpDctTurnIndexFinalAyTh_5    (U8_TCMF_HW_30*10) //450  /*Speed5 에서의 선회 판단 확정 Threshold  Ay 0.45G*/ 
//
///*BTCS Enter Condition_Asymmetric*/
//#define S16TCSCpThOfWhlSpnDiffInSt_1   		(U8_TCMF_ENTER_01*8) //8*8      /*8kph  Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffInSt_2   		(U8_TCMF_ENTER_02*8) //8*8      /*8kph  Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffInSt_3   		(U8_TCMF_ENTER_03*8) //8*8      /*8kph  Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffInSt_4   		(U8_TCMF_ENTER_04*8) //8*8      /*8kph  Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffInSt_5   		(U8_TCMF_ENTER_05*8) //8*8      /*8kph  Threshold of wheel spin difference*/
///*BTCS Enter Condition_Asymmetric Driveline Protect*/
//#define S16TCSCpThOfWhlSpnDiffDPInSt_1  	(U8_TCMF_ENTER_06*8) //35*8      /*35kph  Driveline Protection Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffDPInSt_2  	(U8_TCMF_ENTER_07*8) //35*8      /*35kph  Driveline Protection Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffDPInSt_3  	(U8_TCMF_ENTER_08*8) //35*8      /*35kph  Driveline Protection Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffDPInSt_4  	(U8_TCMF_ENTER_09*8) //35*8      /*35kph  Driveline Protection Threshold of wheel spin difference*/
//#define S16TCSCpThOfWhlSpnDiffDPInSt_5  	(U8_TCMF_ENTER_10*8) //35*8      /*35kph  Driveline Protection Threshold of wheel spin difference*/                                          
///*BTCS Enter Condition_Symmetric*/
//#define S16TCSCpThOfSymSpnCtl_1  			(U8_TCMF_ENTER_11*8) //14*8      /*14kph Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtl_2  			(U8_TCMF_ENTER_12*8) //14*8      /*14kph Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtl_3  			(U8_TCMF_ENTER_13*8) //14*8      /*14kph Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtl_4  			(U8_TCMF_ENTER_14*8) //14*8      /*14kph Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtl_5  			(U8_TCMF_ENTER_15*8) //14*8      /*14kph Symmetric Sipn Threshold*/
///*BTCS Enter Condition_Symmetric Driveline Protect*/
//#define S16TCSCpThOfSymSpnCtlDP_1  			(U8_TCMF_ENTER_16*8) //20*8      /*20kph Driveline Protection Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtlDP_2  			(U8_TCMF_ENTER_17*8) //20*8      /*20kph Driveline Protection Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtlDP_3  			(U8_TCMF_ENTER_18*8) //20*8      /*20kph Driveline Protection Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtlDP_4  			(U8_TCMF_ENTER_19*8) //20*8      /*20kph Driveline Protection Symmetric Sipn Threshold*/
//#define S16TCSCpThOfSymSpnCtlDP_5  			(U8_TCMF_ENTER_20*8) //20*8      /*20kph Driveline Protection Symmetric Sipn Threshold*/
///*BTCS Enter Condition_Turnning Asymmetric*/
//#define S16TCSCpThOfAsymSpCtlInTurn_1  		(U8_TCMF_ENTER_21*8) //4*8 
//#define S16TCSCpThOfAsymSpCtlInTurn_2  		(U8_TCMF_ENTER_22*8) //4*8 
//#define S16TCSCpThOfAsymSpCtlInTurn_3  		(U8_TCMF_ENTER_23*8) //4*8 
//#define S16TCSCpThOfAsymSpCtlInTurn_4  		(U8_TCMF_ENTER_24*8) //4*8 
//#define S16TCSCpThOfAsymSpCtlInTurn_5  		(U8_TCMF_ENTER_25*8) //4*8 
///*BTCS Enter Condition_Modify Threshold*/
//#define S16TCSCpADDTh4RTA 					(U8_TCMF_ENTER_26*8) //5*8 	/*RTA감지시 추가하는 Threshold   (BTCS감지 둔감화)*/
//#define S16TCSCpADDASymThOfDP  				(U8_TCMF_ENTER_27*8) //5*8 	/*Driveline Protection감지시 추가하는 Threshold   (BTCS감지 둔감화)*/ 
//#define S16TCSCpADDTh4RoughRoad 			(U8_TCMF_ENTER_28*8) //5*8 	/*Rough Road감지시 추가하는 Threshold   (BTCS감지 둔감화)*/
//#define S16TCSCpADDTh4ESC					(U8_TCMF_ENTER_29*8) //20*8	/* ESC 제어중 추가하는 Threshold   (BTCS감지 둔감화)*/
//
///*BTCS TargetSpin_Straight*/
//#define U8TCSCpBaseTarWhlSpinDiff_1    		(U8_TCMF_TARGET_SPIN_01*8) //8*8//S8_TCS_RESERVED_05*8 // 12*8 at TCS_SPEED1*/
//#define U8TCSCpBaseTarWhlSpinDiff_2    		(U8_TCMF_TARGET_SPIN_02*8) //8*8//S8_TCS_RESERVED_06*8 // 12*8 at TCS_SPEED2*/
//#define U8TCSCpBaseTarWhlSpinDiff_3    		(U8_TCMF_TARGET_SPIN_03*8) //8*8//S8_TCS_RESERVED_07*8 // 12*8 at TCS_SPEED3*/ 
//#define U8TCSCpBaseTarWhlSpinDiff_4    		(U8_TCMF_TARGET_SPIN_04*8) //8*8//S8_TCS_RESERVED_08*8 // 12*8 at TCS_SPEED4*/
//#define U8TCSCpBaseTarWhlSpinDiff_5    		(U8_TCMF_TARGET_SPIN_05*8) //8*8//S8_TCS_RESERVED_09*8 // 12*8 at TCS_SPEED5*/
///*BTCS TargetSpin_Hill*/             		
//#define U8TCSCpTarWhlSpinDiffHill_1    		(U8_TCMF_TARGET_SPIN_06*8) //6*8//(S8_TCS_RESERVED_05-2)*8
//#define U8TCSCpTarWhlSpinDiffHill_2    		(U8_TCMF_TARGET_SPIN_07*8) //6*8//(S8_TCS_RESERVED_06-2)*8
//#define U8TCSCpTarWhlSpinDiffHill_3    		(U8_TCMF_TARGET_SPIN_08*8) //6*8//(S8_TCS_RESERVED_07-2)*8
//#define U8TCSCpTarWhlSpinDiffHill_4    		(U8_TCMF_TARGET_SPIN_09*8) //6*8//(S8_TCS_RESERVED_08-2)*8
//#define U8TCSCpTarWhlSpinDiffHill_5    		(U8_TCMF_TARGET_SPIN_10*8) //6*8//(S8_TCS_RESERVED_09-2)*8
///*BTCS TargetSpin_Turn*/
//#define U8TCSCpTarWhlSpinDiffTurn_1  		(U8_TCMF_TARGET_SPIN_11*8)  //3*8
//#define U8TCSCpTarWhlSpinDiffTurn_2  		(U8_TCMF_TARGET_SPIN_12*8)  //3*8
//#define U8TCSCpTarWhlSpinDiffTurn_3  		(U8_TCMF_TARGET_SPIN_13*8)  //3*8
//#define U8TCSCpTarWhlSpinDiffTurn_4  		(U8_TCMF_TARGET_SPIN_14*8)  //3*8
//#define U8TCSCpTarWhlSpinDiffTurn_5  		(U8_TCMF_TARGET_SPIN_15*8)  //3*8
//
///*BTCS Torque Control_Asymmetric Base Torque Straight*/
//#define U8TCSCpBsAsmdMuDiffSt_F1    		U8_TCMF_FF_CONTROL_01 //21//S16_TCS_RESERVED_16 //1단 28  /*Delta(difference) Mu 0.28   Resoution 0.01*/
//#define U8TCSCpBsAsmdMuDiffSt_F2    		U8_TCMF_FF_CONTROL_02 //21//S16_TCS_RESERVED_17 //2단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_F3    		U8_TCMF_FF_CONTROL_03 //21//S16_TCS_RESERVED_18 //3단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_F4    		U8_TCMF_FF_CONTROL_04 //21//S16_TCS_RESERVED_19 //4단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_F5    		U8_TCMF_FF_CONTROL_05 //21//S16_TCS_RESERVED_20 //5단 28  /*0.28*/                                        	
//#define U8TCSCpBsAsmdMuDiffSt_R1    		U8_TCMF_FF_CONTROL_06 //21//S16_TCS_RESERVED_16 //1단 28  /*Delta(difference) Mu 0.28   Resoution 0.01*/
//#define U8TCSCpBsAsmdMuDiffSt_R2    		U8_TCMF_FF_CONTROL_07 //21//S16_TCS_RESERVED_17 //2단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_R3    		U8_TCMF_FF_CONTROL_08 //21//S16_TCS_RESERVED_18 //3단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_R4    		U8_TCMF_FF_CONTROL_09 //21//S16_TCS_RESERVED_19 //4단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffSt_R5    		U8_TCMF_FF_CONTROL_10 //21//S16_TCS_RESERVED_20 //5단 28  /*0.28*/
///*BTCS Torque Control_Asymmetric Base Torque Hill*/                                        	
//#define U8TCSCpBsAsmdMuDiffHill_F1    		U8_TCMF_FF_CONTROL_11  //21//S16_TCS_RESERVED_16+10 //1단 28  /*Delta(difference) Mu 0.28   Resoution 0.01*/
//#define U8TCSCpBsAsmdMuDiffHill_F2    		U8_TCMF_FF_CONTROL_12  //21//S16_TCS_RESERVED_17+10 //2단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_F3    		U8_TCMF_FF_CONTROL_13  //21//S16_TCS_RESERVED_18+10 //3단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_F4    		U8_TCMF_FF_CONTROL_14  //21//S16_TCS_RESERVED_19+10 //4단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_F5    		U8_TCMF_FF_CONTROL_15  //21//S16_TCS_RESERVED_20+10//5단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_R1    		U8_TCMF_FF_CONTROL_16 //21//S16_TCS_RESERVED_16+10 //1단 28  /*Delta(difference) Mu 0.28   Resoution 0.01*/
//#define U8TCSCpBsAsmdMuDiffHill_R2    		U8_TCMF_FF_CONTROL_17 //21//S16_TCS_RESERVED_17+10 //2단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_R3    		U8_TCMF_FF_CONTROL_18 //21//S16_TCS_RESERVED_18+10 //3단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_R4    		U8_TCMF_FF_CONTROL_19 //21//S16_TCS_RESERVED_19+10 //4단 28  /*0.28*/
//#define U8TCSCpBsAsmdMuDiffHill_R5    		U8_TCMF_FF_CONTROL_20 //21//S16_TCS_RESERVED_20+10 //5단 28  /*0.28*/
///*BTCS Torque Control_Asymmetric Base Torque Turn*/
//#define U8TCSCpBsAsmdMuDiffTurn_F1     		U8_TCMF_FF_CONTROL_21 //7//S16_TCS_RESERVED_16 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_F2     		U8_TCMF_FF_CONTROL_22 //7//S16_TCS_RESERVED_17 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_F3     		U8_TCMF_FF_CONTROL_23 //7//S16_TCS_RESERVED_18 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_F4     		U8_TCMF_FF_CONTROL_24 //7//S16_TCS_RESERVED_19 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_F5     		U8_TCMF_FF_CONTROL_25 //7//S16_TCS_RESERVED_20 - 10                              		                                                  
//#define U8TCSCpBsAsmdMuDiffTurn_R1     		U8_TCMF_FF_CONTROL_26 //7//S16_TCS_RESERVED_16 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_R2     		U8_TCMF_FF_CONTROL_27 //7//S16_TCS_RESERVED_17 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_R3     		U8_TCMF_FF_CONTROL_28 //7//S16_TCS_RESERVED_18 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_R4     		U8_TCMF_FF_CONTROL_28 //7//S16_TCS_RESERVED_19 - 10      
//#define U8TCSCpBsAsmdMuDiffTurn_R5     		U8_TCMF_FF_CONTROL_28 //7//S16_TCS_RESERVED_20 - 10      
//
///*BTCS Torque Control P Effect_P Gain Straight*/
//#define U8TCSCpBsStPG4ASFBCtlN_F1			U8_TCMF_FB_CONTROL_05	//30//S8_TCS_RESERVED_02  /* Front Axle 1단 Error Negative 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlN_F2			U8_TCMF_FB_CONTROL_06	//25//S8_TCS_RESERVED_04  /* Front Axle 2단 Error Negative 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlN_F3			U8_TCMF_FB_CONTROL_06	//25//S8_TCS_RESERVED_04  /* Front Axle 3단 Error Negative 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlN_F4			U8_TCMF_FB_CONTROL_06	//25//S8_TCS_RESERVED_04  /* Front Axle 4단 Error Negative 영역에서의 P gain */ 
//#define U8TCSCpBsStPG4ASFBCtlN_F5			U8_TCMF_FB_CONTROL_06	//25//S8_TCS_RESERVED_04  /* Front Axle 5단 Error Negative 영역에서의 P gain */                                     		
//#define U8TCSCpBsStPG4ASFBCtlN_R1			U8_TCMF_FB_CONTROL_07	//30//S8_TCS_RESERVED_02  /* Rear Axle 1단 Error Negative 영역에서의 P gain */ 
//#define U8TCSCpBsStPG4ASFBCtlN_R2			U8_TCMF_FB_CONTROL_08	//25//S8_TCS_RESERVED_04  /* Rear Axle 2단 Error Negative 영역에서의 P gain */  
//#define U8TCSCpBsStPG4ASFBCtlN_R3			U8_TCMF_FB_CONTROL_08	//25//S8_TCS_RESERVED_04  /* Rear Axle 3단 Error Negative 영역에서의 P gain */  
//#define U8TCSCpBsStPG4ASFBCtlN_R4			U8_TCMF_FB_CONTROL_08	//25//S8_TCS_RESERVED_04  /* Rear Axle 4단 Error Negative 영역에서의 P gain */  
//#define U8TCSCpBsStPG4ASFBCtlN_R5			U8_TCMF_FB_CONTROL_08	//25//S8_TCS_RESERVED_04  /* Rear Axle 5단 Error Negative 영역에서의 P gain */  
//#define U8TCSCpBsStPG4ASFBCtlP_F1			U8_TCMF_FB_CONTROL_01	//22//S8_TCS_RESERVED_01  /* Front Axle 1단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_F2			U8_TCMF_FB_CONTROL_02	//4//S8_TCS_RESERVED_03  /* Front Axle 2단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_F3			U8_TCMF_FB_CONTROL_02 //- 3 //4//S8_TCS_RESERVED_03  /* Front Axle 3단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_F4			U8_TCMF_FB_CONTROL_02 //- 5	//4//S8_TCS_RESERVED_03  /* Front Axle 4단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_F5			U8_TCMF_FB_CONTROL_02 //- 5	//4//S8_TCS_RESERVED_03  /* Front Axle 5단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_R1			U8_TCMF_FB_CONTROL_03	//22//S8_TCS_RESERVED_01  /* Rear Axle 1단 Error Positive 영역에서의 P gain */
//#define U8TCSCpBsStPG4ASFBCtlP_R2			U8_TCMF_FB_CONTROL_04	//4//S8_TCS_RESERVED_03  /* Rear Axle 2단 Error Positive 영역에서의 P gain */ 
//#define U8TCSCpBsStPG4ASFBCtlP_R3			U8_TCMF_FB_CONTROL_04 //- 3	//4//S8_TCS_RESERVED_03  /* Rear Axle 3단 Error Positive 영역에서의 P gain */ 
//#define U8TCSCpBsStPG4ASFBCtlP_R4			U8_TCMF_FB_CONTROL_04 //- 5	//4//S8_TCS_RESERVED_03  /* Rear Axle 4단 Error Positive 영역에서의 P gain */ 
//#define U8TCSCpBsStPG4ASFBCtlP_R5			U8_TCMF_FB_CONTROL_04 //- 5	//4//S8_TCS_RESERVED_03  /* Rear Axle 5단 Error Positive 영역에서의 P gain */ 
///*BTCS Torque Control P Effect_P Gain Hill*/
//#define U8TCSCpBsHillPG4ASFBCtlN_F1  		U8_TCMF_FB_CONTROL_13  //24//S8_TCS_RESERVED_02 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_F2  		U8_TCMF_FB_CONTROL_14  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_F3  		U8_TCMF_FB_CONTROL_14  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_F4  		U8_TCMF_FB_CONTROL_14  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_F5  		U8_TCMF_FB_CONTROL_14  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_R1  		U8_TCMF_FB_CONTROL_15  //24//S8_TCS_RESERVED_02 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_R2  		U8_TCMF_FB_CONTROL_16  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_R3  		U8_TCMF_FB_CONTROL_16  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_R4  		U8_TCMF_FB_CONTROL_16  //19//S8_TCS_RESERVED_04 -6
//#define U8TCSCpBsHillPG4ASFBCtlN_R5  		U8_TCMF_FB_CONTROL_16  //19//S8_TCS_RESERVED_04 -6     
//#define U8TCSCpBsHillPG4ASFBCtlP_F1  		U8_TCMF_FB_CONTROL_09  //20//S8_TCS_RESERVED_01 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_F2  		U8_TCMF_FB_CONTROL_10  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_F3  		U8_TCMF_FB_CONTROL_10  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_F4  		U8_TCMF_FB_CONTROL_10  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_F5  		U8_TCMF_FB_CONTROL_10  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_R1  		U8_TCMF_FB_CONTROL_11  //20//S8_TCS_RESERVED_01 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_R2  		U8_TCMF_FB_CONTROL_12  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_R3  		U8_TCMF_FB_CONTROL_12  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_R4  		U8_TCMF_FB_CONTROL_12  //7//S8_TCS_RESERVED_03 +3
//#define U8TCSCpBsHillPG4ASFBCtlP_R5  		U8_TCMF_FB_CONTROL_12  //7//S8_TCS_RESERVED_03 +3
///*BTCS Torque Control P Effect_P Gain Turn*/
//#define U8TCSCpBsTurnPG4ASFBCtlN_F1			U8_TCMF_FB_CONTROL_21	//24//S8_TCS_RESERVED_02
//#define U8TCSCpBsTurnPG4ASFBCtlN_F2			U8_TCMF_FB_CONTROL_22	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_F3			U8_TCMF_FB_CONTROL_22	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_F4			U8_TCMF_FB_CONTROL_22	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_F5			U8_TCMF_FB_CONTROL_22	//24//S8_TCS_RESERVED_04     		   
//#define U8TCSCpBsTurnPG4ASFBCtlN_R1			U8_TCMF_FB_CONTROL_23	//24//S8_TCS_RESERVED_02
//#define U8TCSCpBsTurnPG4ASFBCtlN_R2			U8_TCMF_FB_CONTROL_24	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_R3			U8_TCMF_FB_CONTROL_24	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_R4			U8_TCMF_FB_CONTROL_24	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlN_R5			U8_TCMF_FB_CONTROL_24	//24//S8_TCS_RESERVED_04
//#define U8TCSCpBsTurnPG4ASFBCtlP_F1			U8_TCMF_FB_CONTROL_17	//12//S8_TCS_RESERVED_01 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_F2			U8_TCMF_FB_CONTROL_18	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_F3			U8_TCMF_FB_CONTROL_18	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_F4			U8_TCMF_FB_CONTROL_18	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_F5			U8_TCMF_FB_CONTROL_18	//12//S8_TCS_RESERVED_03 - 5                                    		
//#define U8TCSCpBsTurnPG4ASFBCtlP_R1			U8_TCMF_FB_CONTROL_19	//12//S8_TCS_RESERVED_01 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_R2			U8_TCMF_FB_CONTROL_20	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_R3			U8_TCMF_FB_CONTROL_20	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_R4			U8_TCMF_FB_CONTROL_20	//12//S8_TCS_RESERVED_03 - 5
//#define U8TCSCpBsTurnPG4ASFBCtlP_R5			U8_TCMF_FB_CONTROL_20	//12//S8_TCS_RESERVED_03 - 5 
//
///*BTCS Torque Control P Effect_P Factor Straight*/
//#define U8TCSCpPgFac4ASFBCtlSt_F1  			U8_TCMF_FB_CONTROL_25 //100//U8_TCS_RESERVED_01 
//#define U8TCSCpPgFac4ASFBCtlSt_F2  			U8_TCMF_FB_CONTROL_26 //100//U8_TCS_RESERVED_02 
//#define U8TCSCpPgFac4ASFBCtlSt_F3  			U8_TCMF_FB_CONTROL_27 //100//U8_TCS_RESERVED_03 
//#define U8TCSCpPgFac4ASFBCtlSt_F4  			U8_TCMF_FB_CONTROL_28 //180//U8_TCS_RESERVED_04 
//#define U8TCSCpPgFac4ASFBCtlSt_F5  			U8_TCMF_FB_CONTROL_29 //220//U8_TCS_RESERVED_05  
//#define U8TCSCpPgFac4ASFBCtlSt_R1  			U8_TCMF_FB_CONTROL_30 //100//U8_TCS_RESERVED_01
//#define U8TCSCpPgFac4ASFBCtlSt_R2  			U8_TCMF_FB_CONTROL_31 //100//U8_TCS_RESERVED_02
//#define U8TCSCpPgFac4ASFBCtlSt_R3  			U8_TCMF_FB_CONTROL_32 //100//U8_TCS_RESERVED_03
//#define U8TCSCpPgFac4ASFBCtlSt_R4  			U8_TCMF_FB_CONTROL_33 //180//U8_TCS_RESERVED_04
//#define U8TCSCpPgFac4ASFBCtlSt_R5  			U8_TCMF_FB_CONTROL_34 //220//U8_TCS_RESERVED_05     
///*BTCS Torque Control P Effect_P Factor Turn*/
//#define U8TCSCpPgFac4ASFBCtlTurn_F1  		100//U8_TCS_RESERVED_01 
//#define U8TCSCpPgFac4ASFBCtlTurn_F2  		100//U8_TCS_RESERVED_02 
//#define U8TCSCpPgFac4ASFBCtlTurn_F3  		100//U8_TCS_RESERVED_03 
//#define U8TCSCpPgFac4ASFBCtlTurn_F4  		100//U8_TCS_RESERVED_04 
//#define U8TCSCpPgFac4ASFBCtlTurn_F5  		100//U8_TCS_RESERVED_05 
//#define U8TCSCpPgFac4ASFBCtlTurn_R1  		100//U8_TCS_RESERVED_01 
//#define U8TCSCpPgFac4ASFBCtlTurn_R2  		100//U8_TCS_RESERVED_02 
//#define U8TCSCpPgFac4ASFBCtlTurn_R3  		100//U8_TCS_RESERVED_03 
//#define U8TCSCpPgFac4ASFBCtlTurn_R4  		100//U8_TCS_RESERVED_04 
//#define U8TCSCpPgFac4ASFBCtlTurn_R5  		100//U8_TCS_RESERVED_05 
//
///*BTCS Torque Control I Effect_I Gain*/
//#define U8TCSCpICtlActCntTh					80
//#define U8TCSCpICtlActWhlChgRateTh   		(14*10) /* 140 = 1.4g */
///*BTCS Torque Control I Effect_I Gain Straight*/
//#define U8TCSCpBsStIG4ASFBCtl_F1			U8_TCMF_FB_CONTROL_35 //10//S8_TCS_RESERVED_17 //25   /* Front Axle 1단에서의 Base I gain */
//#define U8TCSCpBsStIG4ASFBCtl_F2			U8_TCMF_FB_CONTROL_36 //10//S8_TCS_RESERVED_16 //25   /* Front Axle 2단에서의 Base I gain */
//#define U8TCSCpBsStIG4ASFBCtl_F3			U8_TCMF_FB_CONTROL_36 //10//S8_TCS_RESERVED_16 //25   /* Front Axle 3단에서의 Base I gain */
//#define U8TCSCpBsStIG4ASFBCtl_F4			U8_TCMF_FB_CONTROL_36 //10//S8_TCS_RESERVED_16 //25   /* Front Axle 4단에서의 Base I gain */
//#define U8TCSCpBsStIG4ASFBCtl_F5			U8_TCMF_FB_CONTROL_36 //10//S8_TCS_RESERVED_16 //25   /* Front Axle 5단에서의 Base I gain */                                     		
//#define U8TCSCpBsStIG4ASFBCtl_R1  			U8_TCMF_FB_CONTROL_37 //10//S8_TCS_RESERVED_17 //25   /* Rear Axle 1단에서의 Base I gain */ 
//#define U8TCSCpBsStIG4ASFBCtl_R2  			U8_TCMF_FB_CONTROL_38 //10//S8_TCS_RESERVED_16 //25   /* Rear Axle 2단에서의 Base I gain */ 
//#define U8TCSCpBsStIG4ASFBCtl_R3  			U8_TCMF_FB_CONTROL_38 //10//S8_TCS_RESERVED_16 //25   /* Rear Axle 3단에서의 Base I gain */ 
//#define U8TCSCpBsStIG4ASFBCtl_R4  			U8_TCMF_FB_CONTROL_38 //10//S8_TCS_RESERVED_16 //25   /* Rear Axle 4단에서의 Base I gain */ 
//#define U8TCSCpBsStIG4ASFBCtl_R5  			U8_TCMF_FB_CONTROL_38 //10//S8_TCS_RESERVED_16 //25   /* Rear Axle 5단에서의 Base I gain */ 
///*BTCS Torque Control I Effect_I Gain Hill*/
//#define U8TCSCpBsHillIG4ASFBCtl_F1	 		U8_TCMF_FB_CONTROL_39	//12//S8_TCS_RESERVED_17 +2
//#define U8TCSCpBsHillIG4ASFBCtl_F2	 		U8_TCMF_FB_CONTROL_40	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_F3	 		U8_TCMF_FB_CONTROL_40	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_F4	 		U8_TCMF_FB_CONTROL_40	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_F5	 		U8_TCMF_FB_CONTROL_40	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_R1   		U8_TCMF_FB_CONTROL_41	//12//S8_TCS_RESERVED_17 +2
//#define U8TCSCpBsHillIG4ASFBCtl_R2   		U8_TCMF_FB_CONTROL_42	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_R3   		U8_TCMF_FB_CONTROL_42	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_R4   		U8_TCMF_FB_CONTROL_42	//12//S8_TCS_RESERVED_16 +2
//#define U8TCSCpBsHillIG4ASFBCtl_R5   		U8_TCMF_FB_CONTROL_42	//12//S8_TCS_RESERVED_16 +2
///*BTCS Torque Control I Effect_I Gain Turn*/                                        
//#define U8TCSCpBsTurnIG4ASFBCtl_F1	 		U8_TCMF_FB_CONTROL_43  //10//S8_TCS_RESERVED_17
//#define U8TCSCpBsTurnIG4ASFBCtl_F2	 		U8_TCMF_FB_CONTROL_44  //10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_F3	 		U8_TCMF_FB_CONTROL_44  //10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_F4	 		U8_TCMF_FB_CONTROL_44  //10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_F5	 		U8_TCMF_FB_CONTROL_44 	 //10//S8_TCS_RESERVED_16                        
//#define U8TCSCpBsTurnIG4ASFBCtl_R1  		U8_TCMF_FB_CONTROL_45	//10//S8_TCS_RESERVED_17
//#define U8TCSCpBsTurnIG4ASFBCtl_R2  		U8_TCMF_FB_CONTROL_46	//10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_R3  		U8_TCMF_FB_CONTROL_46	//10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_R4  		U8_TCMF_FB_CONTROL_46	//10//S8_TCS_RESERVED_16
//#define U8TCSCpBsTurnIG4ASFBCtl_R5  		U8_TCMF_FB_CONTROL_46	//10//S8_TCS_RESERVED_16    
//
///*BTCS Torque Control I Effect_I Factor Straight*/                                        
//#define U8TCSCpIgainEffPctStN_F   			U8_TCMF_FB_CONTROL_47 //30//S8_TCS_RESERVED_11//20%    -5kph 에서의 i gain 적용 비율
//#define U8TCSCpIgainEffPctStP_F   			U8_TCMF_FB_CONTROL_48 //127//S8_TCS_RESERVED_12//150%     10kph 에서의 i gain 적용 비율  
//#define U8TCSCpIgainEffPctStN_R   			U8_TCMF_FB_CONTROL_49 //30//S8_TCS_RESERVED_11//20%    -5kph 에서의 i gain 적용 비율
//#define U8TCSCpIgainEffPctStP_R   			U8_TCMF_FB_CONTROL_50 //127//S8_TCS_RESERVED_12//150%     10kph 에서의 i gain 적용 비율 
///*BTCS Torque Control I Effect_I Factor Turn*/                                        
//#define U8TCSCpIgainEffPctTurnN_F   		U8_TCMF_FB_CONTROL_47 //30//S8_TCS_RESERVED_11
//#define U8TCSCpIgainEffPctTurnP_F   		U8_TCMF_FB_CONTROL_48 //127//S8_TCS_RESERVED_12
//#define U8TCSCpIgainEffPctTurnN_R   		U8_TCMF_FB_CONTROL_49 //30//S8_TCS_RESERVED_11
//#define U8TCSCpIgainEffPctTurnP_R   		U8_TCMF_FB_CONTROL_50 //127//S8_TCS_RESERVED_12     
//
///*Symmetric Torque Base*/
//#define S16TCSCpBsTBrkTorq4SymWhlSp_F1  	(U8_TCMF_ETC_01*10) //250//U16_TCS_RESERVED_06  //300    /*Nm Resolution 1*/ 
//#define S16TCSCpBsTBrkTorq4SymWhlSp_F2  	(U8_TCMF_ETC_02*10) //250//U16_TCS_RESERVED_07  //300
//#define S16TCSCpBsTBrkTorq4SymWhlSp_F3  	(U8_TCMF_ETC_02*10) //250//U16_TCS_RESERVED_08  //300
//#define S16TCSCpBsTBrkTorq4SymWhlSp_F4  	(U8_TCMF_ETC_02*10) //250//U16_TCS_RESERVED_09  //300
//#define S16TCSCpBsTBrkTorq4SymWhlSp_F5  	(U8_TCMF_ETC_02*10) //250//U16_TCS_RESERVED_10  //300                                        	
//#define S16TCSCpBsTBrkTorq4SymWhlSp_R1   	(U8_TCMF_ETC_03*10) // 250     /*Nm Resolution 1*/ 
//#define S16TCSCpBsTBrkTorq4SymWhlSp_R2   	(U8_TCMF_ETC_04*10) // 250
//#define S16TCSCpBsTBrkTorq4SymWhlSp_R3   	(U8_TCMF_ETC_04*10) // 250
//#define S16TCSCpBsTBrkTorq4SymWhlSp_R4   	(U8_TCMF_ETC_04*10) // 250 
//#define S16TCSCpBsTBrkTorq4SymWhlSp_R5   	(U8_TCMF_ETC_04*10) // 250
//
///*Symmetric Torque Control*/
//#define U8TCSCpPresDifLR4SymSp      		U8_TCMF_ETC_12	//20 /* Comment [ Exit condition of the Symmetric spin detection for Front wheel, if pressure difference between left and right wheel >= U8TCSCpPresDifLR4SymSpF, symmetric spin detection will be end ] */
//#define U8TCSCpSymBoth2WlSpdDifTh   		U8_TCMF_ETC_14	//64 /*Symmetric 판단을 위한 좌우 Speed 차이 Threshold*/
//#define U8TCSCpSymBoth2WlSpinTh     		U8_TCMF_ETC_13	//34 /*4.25Kph*/
//#define U8TCSCpSymExtHysOfMovDiffLnR  		8  /*1kph*/
//#define U8TCSCpTBrkTorqDec4Sym 				U8_TCMF_ENTER_30  //20 /*Nm Target brake torque decrease amount per 10ms when finish symmetric spin control */
//
///*BTCS Special Case*/
//#define U8TCSCpDecBsTarBrkF4ESCBrkCtl   	20 /*선회시 Brake Torque Reduce P Gain Factor*/
///*BTCS Special Case_Vibration*/
//#define S16TCSCpBrkCtlTrqAddInVIB  			150 /*Add Brake Torque at Vibration Control Nm*/
//#define U8_TCS_VIB_TIMESHIFT_SCAN_TCMF 		3
///*BTCS Special Case_Stuck*/
//#define S16TCSCpBrkCtlTrqOfStuck 			1500 /*1500Nm Stuck 발생시 인가하는 Base Torque량*/
//#define S16TCSCpCtlMotorV_Stuck  			5000 /*Stuck 시 Motor Voltage*/         
//#define U8TCSCpStuckPressureRiseTime 		30 /*300ms stuck 시 rise time*/
///*BTCS Special Case_VCA*/
//#define S16TCSCpBrkCtlTrqOfVCA  			600//U16_TCS_RESERVED_18 /*600 Nm Resolution 1*/
//#define S16TCSCpCtlMotorV_VCA     			2000 //2000         
//#define S16TCSCpInitMaxMotorV_VCA  			2000//3000 /*5V    VCA 초기 최대 전압값*/
//#define S16TCSCpInitMaxVCATCCurnt  			1600 /*1.2A  VCA 초기 전류 최대값*/
//#define U8TCSCpIniVCACtlModeMtorTime 		7 /*VCA시 Initial Voltage time*/
//#define U8TCSCpIniVCACtlModeTime   			14 /* 100ms VCA 초기 최대 전류 적용 시간*/
//#define U8TCSCpRearCtlOfVCA 				1
//#define U8_ACC_TH_VCA						10
//#define U8_MAX_VCA_SUSTAIN_SCAN				30
//#define U8_SPEED_TH_VCA						16
//#define U8_SPIN_TH_VCA						0
//#define U8_TCS_VCA_REENGAGE_ALLOW_TIME		60
//#define U8_VREF_DECREASE_LIMIT_OF_VCA		8
///*BTCS Special Case_PressLimit*/
//#define U8TCSCpTarWhlPreTurnMax_1			180 /* 13bar 선회시 Speed1에서의 최대압력 Limit*/
//#define U8TCSCpTarWhlPreTurnMax_2			150 /* 12bar 선회시 Speed2에서의 최대압력 Limit*/
//#define U8TCSCpTarWhlPreTurnMax_3			130 /* 10bar 선회시 Speed3에서의 최대압력 Limit*/
//#define U8TCSCpTarWhlPreTurnMax_4 			100 /* 9bar 선회시 Speed4에서의 최대압력 Limit*/
//#define U8TCSCpTarWhlPreTurnMax_5 			80 /* 8bar 선회시 Speed5에서의 최대압력 Limit*/
///*BTCS Special Case_Stall*/
//#define U8TCSCpBrkTorqDecPcntOfEngStl   	U8_TCMF_ETC_20 //60 /*%        Target brake torque decrease level at the moment of engine stall is detected*/
//#define U8TCSCpBrkTorqDecRateOfEngStl  		50 /* Nm/10ms  Target brake torque decrease rate when engine stall is detected*/
//#define U8TCSCpBrkTorqRecPcntOfEngStl   	50 /*%        Target brake torque recovery level at the moment of engine stall detection is cleared*/
//#define U8TCSCpTBrkTorqDec4SymInStall		50
///*BTCS Special Case_OverBraking*/
//#define S16TCSCpBrkTorqDecRateOfOvBrk  		U8_TCMF_ETC_17 // 100/* Nm/10ms Target brake torque decrease rate when over braking is detected*/
//#define S8TCSCpOverBrkTh 			   		U8_TCMF_ETC_15 //	3 /*Threshold of over braking detection*/
//#define U8TCSCpBrkTorqDecPcntOfOvBrk   		U8_TCMF_ETC_16 //  50 /*%        Target brake torque decrease level at the moment of over braking is detected*/
///*BTCS Special Case_FadeOut*/
//#define S16TCSCpWLSpdCrosTh4QuickFadOut 	16 /*2KPH  FadeOut 제어중 비제어 Wheel이 제어Wheel보다 빨라질 경우 즉시 Feedforward량 Reset*/
//#define U8TCSCpFadeOutDctScan     			U8_TCMF_ETC_19  //  30  /* Fade Out Mode Detection Scan */
//#define U8TCSCpFadeOutExitScan    			20			   //  20  /* Fade Out Mode Exit Scan */
//#define U8TCSCpFadeOutSpdDiffThr  			U8_TCMF_ETC_18  //  16 /*Fade Out Mode Enter Threshold   WhlSpinDiff */
//#define U8TCSCpSymSpFadeOutHystSpd 			20 /* 2.5kph symmetric spin fade out hysteresis speed (between symmetric enter speed and fade out start speed -2kph)*/
//#define U8TCSFadeOutBrkCtrlMinTorq   		120
///*BTCS Special Case_FadeOut_Straight*/
//#define S16TCSCpFadeOutCtrlHghPreSTTh  		200 //S16_TCS_RESERVED_06//200 /*20bar 직진시 FadeOut을 위한 High Pressure Level*/
//#define S16TCSCpFadeOutCtrlLowPreSTTh  		50 //S16_TCS_RESERVED_05//50 /*5bar 직진시 FadeOut을 위한 Low Pressure Level*/
//#define S16TCSCpFadeOutDecTQ4HghPreST 		2 //S16_TCS_RESERVED_08//2 /*2Nm/scan 직진시 High Pressuer에서의 FadeOut Control   천천히뺌 Fadeout Control Scan*/
//#define S16TCSCpFadeOutDecTQ4LowPreST 		5 //S16_TCS_RESERVED_07//5 /* 5Nm/scan 직진시 Low Pressuer에서의 FadeOut Control   빨리 뺌 Fadeout Control Scan*/
///*BTCS Special Case_FadeOut_Turn*/
//#define S16TCSCpFadeOutCtrlHghPreTunTh  	150 //S16_TCS_RESERVED_10// 150   /* 15bar 선회시 FadeOut을 위한 Low Pressure Level*/
//#define S16TCSCpFadeOutCtrlLowPreTunTh  	50 //S16_TCS_RESERVED_09// 50   /* 5bar 선회시 FadeOut을 위한 High Pressure Level*/
//#define S16TCSCpFadeOutDecTQ4HghPreTurn 	10 //S16_TCS_RESERVED_12//10 /*10Nm/scan 선회시 Low Pressure에서의 FadeOut Control 시간 짧게해 빨리 뺌 Fadeout Control Scan*/
//#define S16TCSCpFadeOutDecTQ4LowPreTurn 	10 //S16_TCS_RESERVED_11//10 /*10Nm/scan 선회시 High Pressuer에서의 FadeOut Control 시간 크게해  천천히뺌 Fadeout Control Scan*/
///*BTCS Special Case_HSS*/
//#define S16TCSCpBrkTorq1stDecTrqOfHWIS		U8_TCMF_ETC_07	//500	/* 60%	Target brake torque decrease level at the moment of high mu wheel instability is detected. %	*/ 
//#define U8CpAfterHghMuUnstbCntTh			U8_TCMF_ETC_10  //200  /*2s   HighMu Unstable 해제 후 Torq up 시 천천히 올리는 시간 Scan*/
//#define U8TCSCpBrkTorqDecRateOfHWIS			U8_TCMF_ETC_08  //30/* Nm/10ms Target brake torque decrease rate when high mu wheel instability is detected*/
//#define U8TCSCpBrkTorqFastDecRateOfHWIS  	100
//#define U8TCSCpBrkTorqRecPcntOfHWIS			U8_TCMF_ETC_09  //50/* %        Target brake torque recovery level at the moment of high mu wheel instability detection is cleared*/
//#define U8TCSCpHghMuWhlDelSpinTh      		U8_TCMF_ETC_06	//20 /*2.5kph High Mu 판단 Undtable 좌우 속도 차 Threshold*/
//#define U8TCSCpHghMuWhlHuntSpinTh 			12 /*1.5kph*/
//#define U8TCSCpHghMuWhlSpinTh				U8_TCMF_ETC_05	//24  /*3kph High Mu Unstable Spin Threshold*/
//#define U8TCSCpHghMuWhlSpinTm				U8_TCMF_ETC_11  //3 /*If spin of high-mu wheel is greater than U8TCSCpHghMuWhlSpinTh for U8TCSCpHghMuWhlSpinTm, high-mu wheel instability will be detected*/
//#define U8TCSCpMtrFstRnTmrAftHghMuWhStb 	10 /* 10 means 100ms : wheel on high-mu 가 stable 상태로 되돌아 온 경우에도 이 시간 동안은 S16TCSCpAddMtrV4SPHillHghMuUnSt 를 적용한다. */
//#define U8TCSHghMuUnstbRecoveryTh			24 /* 3kph HSS 후 recovery threshold*/
///*BTCS Special Case_GearShift*/
//#define U8TCSCpReducePgainF4GearShift  		70 /*70% gear shift시 적용되는 P gain 량*/ 
///*BTCS Special Case_Hunting*/
//#define S16TCSCpWorkTh4HuntDctReset 		500		/* 500 means 500 [Nm sec] */
//#define S16TCSCpWorkTh4L2SplitSuspect 		200 /*Hunting에 의한 누적 Work Torque가 해당 값 이상일 시 Low To Split Torque 제어 수행*/
//#define U8TCSCpFFReductFactInHunting 		30 /* 30 means 30% */
//#define U8TCSCpL2SplitSuspectTorq     		5   /*Low to Split 감지시 Feed Forward 값을 scan당 해당 값만큼 증가시켜 조기 Split 판단 및 After Hutning Torque에 의한 Hesiation 방지*/
///*BTCS Special Case_FF Adpatation*/
//#define S16TCSCpBrkTrqMaxIncStartTime		300		/* 300 means 3sec */
//#define S16TCSCpWorkTh4BrkTrqMaxLim 		850 	/* 850 means 850 [Nm sec] */
//#define S16TCSCpWorkTh4FFAdapt 				1500	/* 1500 means 1500 [Nm sec] */
//#define S16TCSCpWorkTh4FFAdaptReset 		4000	/* 4000 means 4000 [Nm sec] */
//#define U8TCSCpBrkTrqMaxIncRate				4		/* 4 means 4Nm/100ms */

#endif 

/* Definition of Developing Function ******************************************************/

/* NEW STRUCTURE Tuning Parameter ******************************************************/
#define S16TCSCpADDBTCEntTh4ComPMod  		40 /*5KPH Competitive Mode시 BTCS Enter Add Threshold */
#define S16TCSCpADDBTCTarSP4ComPMod  		24 /*3KPH Competitive Mode시 BTCS Target Spin Add */
#define S16FFBrkTorqReduce4Vibration 		200
#define S16TCSCpBrkTq1stDecTrqOfHWISHill 	(S16TCSCpBrkTorq1stDecTrqOfHWIS/10)*12 /*Target brake torque decrease level at the moment of high mu wheel instability is detected(Hill)*/
#define U8TCSHillVibrationCTRL 				0 /*AWD flat&hill   2WD Vehicle flat  hill is option */
#define U8TCSHghMuUstbReptSP2HomoCntTh 		1 /*기본 HSS 감지 이후 추가 HSS시 FTCS->ETCS Change counter threshold*/

/*Refactoring Undefined TP MBD ETCS*/
#define U8TCSCpFFStartPointPres 			30
#define U8TCSCpInitTarPres 					10
#define U8TCSCpPresIncRateInFF 				7


/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/


/*Global Extern Functions  Declaration******************************************************/
extern void LCTCS_vBrakeControl(void);
extern int16_t LCTCS_vConvPres2CurntOnTC (int16_t Pressure);   
extern int16_t LCTCS_vConvCurntOnTC2Pres (int16_t Current);

#endif
