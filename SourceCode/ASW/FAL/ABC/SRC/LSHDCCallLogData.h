#ifndef __LSHDCCALLLOGDATA_H__
#define __LSHDCCALLLOGDATA_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
  #if __HSA
extern U8_BIT_STRUCT_t	HSAF0;
  #endif	
/*Global Extern Functions Declaration ******************************************/
  #if defined(__LOGGER) && (__LOGGER_DATA_TYPE==1)
extern void LSHDC_vCallHDCLogData(void);
  #endif
#endif
