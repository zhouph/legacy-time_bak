#ifndef __LDFBCCALLDETECTION_H__
#define __LDFBCCALLDETECTION_H__
/*Includes *********************************************************************/

#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/


/*Global Extern Variable Declaration *******************************************/


extern U8_BIT_STRUCT_t FBCF00;

 

extern uint8_t   FBC_ACTIVE;
extern int16_t	FBC_decel;
extern	int16_t	FBC_decelbuffer[10];
extern	int16_t FBC_decel_buffer_index;
extern	int16_t FBC_decel_sum;
extern	uint8_t FBC_decel_buffer_Full_Flag;

 

/*Global Extern Functions Declaration ******************************************/
extern void LCFBC_vInitializeVariables(void);

#endif
