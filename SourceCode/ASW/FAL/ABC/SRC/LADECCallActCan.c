/* Includes ********************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LADECCallActCan
	#include "Mdyn_autosar.h"               
#endif


#include "LADECCallActCan.h"

#if __DEC

#include "LCDECCallControl.h"
#include "LSABSCallSensorSignal.h"
#include "LCESPCalLpf.h"
   #if !SIM_MATLAB
   		#if GMLAN_ENABLE==DISABLE
		#include "../CAN/CCTestFunc.h"
		#endif
   #endif
  #endif
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __DEC
int16_t		lcs16DecCtrlBoxDecel,lcs16DecCtrlBoxDecelOld;
  #endif
/* Local Function prototype ****************************************/
  #if __DEC
static void LADEC_vTransferDecel2CtrlBox(void);
  #endif

/* Implementation***************************************************/
  #if __DEC
void LADEC_vCallActCan(void)
{
	LADEC_vTransferDecel2CtrlBox();
}

void LADEC_vTransferDecel2CtrlBox(void)
{
	  #if defined(__CAN_SW)
	if(ABS_fz==1)
	{
		lcs16DecCtrlBoxDecel=LCESP_s16Lpf1Int((lsabss16RawAxSignal/10),lcs16DecCtrlBoxDecelOld,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
	}
	else
	{
		lcs16DecCtrlBoxDecel=LCESP_s16Lpf1Int(ebd_filt_grv,lcs16DecCtrlBoxDecelOld,L_U8FILTER_GAIN_10MSLOOP_17HZ);
	}
	lcs16DecCtrlBoxDecelOld=lcs16DecCtrlBoxDecel;

	if(lcs16DecCtrlBoxDecel<-127)
	{
		Actual_DECEL_G=255;
	}
	else if(lcs16DecCtrlBoxDecel<=0)
	{
		Actual_DECEL_G=128+(uint8_t)(abs(lcs16DecCtrlBoxDecel));
	}
	else if(lcs16DecCtrlBoxDecel>127)
	{
		Actual_DECEL_G=127;
	}
	else
	{
		Actual_DECEL_G=(uint8_t)lcs16DecCtrlBoxDecel;
	}
	  #endif
}
  #endif

  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LADECCallActCan
	#include "Mdyn_autosar.h"               
#endif  
