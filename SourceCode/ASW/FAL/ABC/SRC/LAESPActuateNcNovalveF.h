#ifndef __LAESPACTUTATENCNOVALVEF_H__
#define __LAESPACTUTATENCNOVALVEF_H__

/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/
#define __INITIAL_MTR_DRIVE_IMPROVE     ENABLE

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
 
extern int16_t msc_rise_scan;
extern int16_t pulse_dump_nc_duty_front;
extern int16_t pulse_dump_nc_duty_rear;
extern int16_t esp_slip_nc_duty_front;
extern int16_t esp_slip_nc_duty_rear;
extern int16_t fade_rate_counter_fl ;
extern int16_t fade_rate_counter_fr ;
extern int16_t fade_rate_counter_rl ;
extern int16_t fade_rate_counter_rr ;

 

/*Global Extern Functions Declaration ******************************************/


#endif
