#ifndef __LAABSCALLACTCAN_H__
#define __LAABSCALLACTCAN_H__

/*includes********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition********************************************/

/*Global Type Declaration ****************************************************/

typedef struct
{
    uint16_t ABS_CAN_BIT_07     :1;
    uint16_t ABS_CAN_BIT_06     :1;
    uint16_t ABS_CAN_BIT_05     :1;
    uint16_t ABS_CAN_BIT_04     :1;
    uint16_t ABS_CAN_BIT_03     :1;
    uint16_t ABS_CAN_BIT_02     :1;
    uint16_t ABS_CAN_BIT_01     :1;
    uint16_t ABS_CAN_BIT_00     :1;
}ABS_CAN_FLAG_t;




/*Global Extern Variable  Declaration*****************************************/

extern ABS_CAN_FLAG_t ABSCANBIT01;

extern uint8_t laabsu8SpareTireStatus;

#define laabsu1TrnsBrkSysCltchRelRqd         ABSCANBIT01.ABS_CAN_BIT_07
#define laabsu1ABSActCan                     ABSCANBIT01.ABS_CAN_BIT_06
/*Global Extern Functions  Declaration****************************************/
extern void LAABS_vCallActCan(void);


#endif
