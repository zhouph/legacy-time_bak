#ifndef __LDCALLMAIN_H__
#define __LDCALLMAIN_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/
extern void LDABS_vCallDetection(void);

#if __EDC || __ETC || __TCS		// ABS_PLUS_ECU 적용시 macro 삭제 예정
extern void LDENG_vCallDetection(void);
#endif

#if __BTC
extern void LDTCS_vCallDetection(void);
#endif

#if __ETSC
extern void	LDETSC_vCallDetection(void);
#endif

#if __VDC
   #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    extern void LDESP_vUpdateState(void);
   #endif
extern void LDESP_vCallDetection(void);
#endif

#if __VSM
extern void LCEPS_vCallControl(void);
#endif

#if __FBC
extern void LDFBC_vCallDetection(void);
#endif

#if __BDW
extern void LDBDW_vCallDetection(void);
#endif

#if __EBP
extern void LDEBP_vCallDetection(void);
#endif

#if defined(__CDC)
extern void LDCDC_vCallDetection(void);
#endif

#endif
