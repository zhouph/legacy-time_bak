#ifndef __LABTCSCALLACTHW_H__
#define __LABTCSCALLACTHW_H__
/*Includes *********************************************************************/
 #include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/
#define TCSMinimumPressureLevel		1

/*Global MACRO FUNCTION Definition *********************************************/
#define MULTIPLEX_TCS                       (DISABLE) //(ENABLE) (DISABLE)
#define TARGET_PRESSURE_RATE_LIMITATION		(DISABLE) //(ENABLE) (DISABLE)

#if (TARGET_PRESSURE_RATE_LIMITATION == ENABLE)

#define S16_TARGET_PRES_LEVEL_1		(MPRESS_4BAR)
#define S16_TARGET_PRES_LEVEL_2		(MPRESS_10BAR)

#define S16_TARGET_PRES_RATE_1		(5) //(MPRESS_2BAR)
#define S16_TARGET_PRES_RATE_2		(12) //(MPRESS_2BAR)
#define S16_TARGET_PRES_RATE_3		(20) //(MPRESS_4BAR)

#endif /* (TARGET_PRESSURE_RATE_LIMITATION == ENABLE) */


/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern int16_t tcs_msc_target_vol;
/*Global Extern Functions Declaration ******************************************/
extern void LATCS_vCallActHWGen2(void);
extern void LATCS_vCtlNoNcValveXsplitIDB(void);
/*Temporary Tuning Parameter Declaration ***************************************/


#endif
