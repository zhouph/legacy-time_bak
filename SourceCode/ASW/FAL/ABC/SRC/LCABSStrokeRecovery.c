/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LCABSStrokeRecovery.c
* Description: ABS control algorithm
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCABSDecideCtrlState
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "LCABSStrokeRecovery.h"
#include "LACallMain.h"
#include "LCABSCallControl.h"


/* Local Definition  **********************************************************/

#define S16_RECOVERY_END_REQ_TIME_ABC10MS		(S16_RECOVERY_END_REQ_TIME_MS/10)
#define S16_RECOVERY_DONT_CARE_TIME_ABC10MS		(S16_RECOVERY_DONT_CARE_TIME_MS/10)
#define S16_RECOVERY_MIN_ACT_TIME_ABC10MS			(S16_RECOVERY_MIN_ACT_TIME_MS/10)

/* Variables Definition*******************************************************/
 #if __ABS_STK_RCVR_CTRL_MON == ENABLE
struct W_STRUCT WhlMon;
lcStrkRecvryWhlSt_t lcStrkRecvryWhlStFL, lcStrkRecvryWhlStFR, referencemonwhl;
int16_t lcabss16StrkRecSlipThd;
int16_t lcabss16StrkRecAradThd;
int16_t lcabss16StrkRecDUMPRecvRateThd;
int16_t lcabss16StrkRecPulseUpCntThd;
uint8_t lcabsu8strkrcvrdebugger;
uint8_t u8ReferenceWheelOfSR;
int8_t lcabss8FrontRiseTimeAfStrkRcvr;

int8_t lcabss8StrkRcvrCtrlState;
int8_t lcabss8RecommendStkRcvrLvl;
int8_t lcabss8StrkRcvrCtrlStateOld;
int16_t lcabss16StrkRcvrAbsAllowedTime;
 #else
/*struct W_STRUCT WhlMon;*/
lcStrkRecvryWhlSt_t lcStrkRecvryWhlStFL, lcStrkRecvryWhlStFR/*, referencemonwhl*/;
static int16_t lcabss16StrkRecSlipThd;
static int16_t lcabss16StrkRecAradThd;
static int16_t lcabss16StrkRecDUMPRecvRateThd;
static int16_t lcabss16StrkRecPulseUpCntThd;
uint8_t lcabsu8strkrcvrdebugger;
/*uint8_t u8ReferenceWheelOfSR;*/
static int8_t lcabss8FrontRiseTimeAfStrkRcvr;

static int8_t lcabss8StrkRcvrCtrlState;
static int8_t lcabss8RecommendStkRcvrLvl;
static int8_t lcabss8StrkRcvrCtrlStateOld;
static int16_t lcabss16StrkRcvrAbsAllowedTime;
 #endif



/* Local Function prototype ***************************************************/
static int16_t LCABS_s16DecideABSStrkRcvrTime(void);
static void LCABS_vInitSRPossibleTimeVars(lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt);
static uint8_t LCABS_u8SelectStrkRcvRefWhl(void);
static void LCABS_vCheckStrkRcvrCntForCycle(struct W_STRUCT *WL_temp,  lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt);
static void LCABS_vCalcGradeOfBrakingForce(struct W_STRUCT *WL_temp,  lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt);
static int16_t LCABS_s16DecidePossibleStrkRec(lcStrkRecvryWhlSt_t *pABSStrkRecRefWhl, lcStrkRecvryWhlSt_t *pABSStrkRecRefOppWhl);
static void LCABS_vSetStrkRcvrBrkForceTHD(void);

static void LCABS_vDcdWhlCtrlReqForStrkRec(int8_t s8StrkRcvrCtrlState, int8_t s8StrkRcvrCtrlStateOld);
static int8_t LCABS_s8DcdWhlRiseBfStrkRec(struct W_STRUCT *pWhl);
static int8_t LCABS_s8DcdWhlRiseAfStrkRec(struct W_STRUCT *pWhl);

/* GlobalFunction prototype **************************************************/

void LCABS_vStrokeRecoveryMain(void);

/* Implementation*************************************************************/

/******************************************************************************
* FUNCTION NAME:      LCABS_vStrokeRecoveryMain
* CALLED BY:          LCABS_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Main function of the stroke recovery ABS control
******************************************************************************/
void LCABS_vStrokeRecoveryMain(void)
{
	/*input*/
	lcabss8StrkRcvrCtrlState = lis8StrkRcvrCtrlState_Rx;
	lcabss8RecommendStkRcvrLvl = lis8RecommendStkRcvrLvl_Rx;

	lcabss16StrkRcvrAbsAllowedTime = LCABS_s16DecideABSStrkRcvrTime();
	
	LCABS_vDcdWhlCtrlReqForStrkRec(lcabss8StrkRcvrCtrlState,lcabss8StrkRcvrCtrlStateOld);
	
	lcabss8StrkRcvrCtrlStateOld = lcabss8StrkRcvrCtrlState;
	
	/*output*/
	lis16StrkRcvrAbsAllowedTime_Tx = lcabss16StrkRcvrAbsAllowedTime*10;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideABSStrkRcvrTime
* CALLED BY:          LCABS_vStrokeRecoveryMain()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide ABS Stroke recovery Time
******************************************************************************/
static int16_t LCABS_s16DecideABSStrkRcvrTime(void)
{
	#if __ABS_STK_RCVR_CTRL_MON == ENABLE
	#else
	uint8_t u8ReferenceWheelOfSR;
	#endif
	int16_t s16StrkRcvrAllowedTime = S16_RECOVERY_DONT_CARE_TIME_ABC10MS;

	if(ABS_fz==0)
	{
		LCABS_vInitSRPossibleTimeVars(&lcStrkRecvryWhlStFL);
		LCABS_vInitSRPossibleTimeVars(&lcStrkRecvryWhlStFR);

		s16StrkRcvrAllowedTime = S16_RECOVERY_DONT_CARE_TIME_ABC10MS;
	}
	else
	{
		u8ReferenceWheelOfSR = LCABS_u8SelectStrkRcvRefWhl();    /* check reference wheel */

		LCABS_vCheckStrkRcvrCntForCycle(&FL, &lcStrkRecvryWhlStFL);     /* Number of stroke recovery during ABS cycle */
		LCABS_vCheckStrkRcvrCntForCycle(&FR, &lcStrkRecvryWhlStFR);

		LCABS_vSetStrkRcvrBrkForceTHD();    /* Set braking force threshold */

		LCABS_vCalcGradeOfBrakingForce(&FL, &lcStrkRecvryWhlStFL);    /* calculate braking force */
		LCABS_vCalcGradeOfBrakingForce(&FR, &lcStrkRecvryWhlStFR);
			
		if(LOW_to_HIGH_suspect == 1)
	    {
			s16StrkRcvrAllowedTime = S16_RECOVERY_END_REQ_TIME_ABC10MS;
	    }
		else if((ABS_LIMIT_SPEED_fl==1)||(ABS_LIMIT_SPEED_fr==1)||(ABS_LIMIT_SPEED_rl==1)||(ABS_LIMIT_SPEED_rr==1))
		{
			s16StrkRcvrAllowedTime = S16_RECOVERY_END_REQ_TIME_ABC10MS;
		}
		else if(lcabsu1BrkPedalRelDuringABS == 1)
		{
			s16StrkRcvrAllowedTime = S16_RECOVERY_END_REQ_TIME_ABC10MS;
		}
		else
		{
			if(u8ReferenceWheelOfSR == U8_SR_REF_FL_SIDE)    /*decide stroke recovery possible point*/
	    	{
				s16StrkRcvrAllowedTime = LCABS_s16DecidePossibleStrkRec(&lcStrkRecvryWhlStFL, &lcStrkRecvryWhlStFR);
				 #if __ABS_STK_RCVR_CTRL_MON == ENABLE
				WhlMon.vrad = FL.vrad;
				WhlMon.s16_Estimated_Active_Press = FL.s16_Estimated_Active_Press;
				 #endif
	    	}
	    	else
	    	{
				s16StrkRcvrAllowedTime = LCABS_s16DecidePossibleStrkRec(&lcStrkRecvryWhlStFR, &lcStrkRecvryWhlStFL);
				 #if __ABS_STK_RCVR_CTRL_MON == ENABLE
				WhlMon.vrad = FR.vrad;
				WhlMon.s16_Estimated_Active_Press = FR.s16_Estimated_Active_Press;
				 #endif
	    	}
		}
	}

	return s16StrkRcvrAllowedTime;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vInitSRPossibleTimeVars
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Initialize Stroke Recovery Variables
******************************************************************************/
static void LCABS_vInitSRPossibleTimeVars(lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt)
{
	lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt = 0;
	lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt = 0;
	lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP = 0;
	lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP = 0;
	lcStrkRecvryWhlSt->s16GradeOfRefWhlSt = 0;
	lcStrkRecvryWhlSt->s16GradeOfRefWhlP = 0;
	lcStrkRecvryWhlSt->s16DumpRecoveryRatio = 0;
	lcStrkRecvryWhlSt->s16AbsStrkRcvrCntForCycle = 0;
	lcStrkRecvryWhlSt->s8RefRatioEstValueOfSpd = 0;
	lcStrkRecvryWhlSt->s8RefRatioEstValueOfPress = 0;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_u8SelectStrkRcvRefWhl
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Select stroke recovery reference wheel
******************************************************************************/
static uint8_t LCABS_u8SelectStrkRcvRefWhl(void)
{
	static uint8_t u8ReferenceWheel = U8_SR_REF_FL_SIDE;

	if((FL.state == DETECTION) && (FR.state == DETECTION)) /* 2 wheel stable : lower pulse-up */
	{
		if(FL.s0_reapply_counter > FR.s0_reapply_counter)
		{
			u8ReferenceWheel = U8_SR_REF_FR_SIDE;
		}
		else if(FL.s0_reapply_counter < FR.s0_reapply_counter)
		{
			u8ReferenceWheel = U8_SR_REF_FL_SIDE;
		}
		else {}
	}
	else if((FL.state == UNSTABLE) && (FR.state == UNSTABLE)) /* 2 wheel unstable : more unstable (consideration by wheel slip) */
	{
		if(FL.rel_lam >= FR.rel_lam)
		{
			u8ReferenceWheel = U8_SR_REF_FR_SIDE;
		}
		else
		{
			u8ReferenceWheel = U8_SR_REF_FL_SIDE;
		}
	}
	else /* 1 wheel stable and 1 wheel unstable in FRONT WHEEL */
	{
		if(FL.state == UNSTABLE)
		{
			u8ReferenceWheel = U8_SR_REF_FL_SIDE;
		}
		else
		{
			u8ReferenceWheel = U8_SR_REF_FR_SIDE;
		}
	}
	
	return u8ReferenceWheel;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vCheckStrkRcvrCntForCycle
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Count stroke recovery per ABS cycle
******************************************************************************/
static void LCABS_vCheckStrkRcvrCntForCycle(struct W_STRUCT *WL_temp,  lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt)
{
	if(WL_temp->state == DETECTION)
	{
		if((lcabss8StrkRcvrCtrlStateOld == S8_STRK_RCVR_STABILIZING2_STATE) && (lcabss8StrkRcvrCtrlState == S8_STRK_RCVR_READY_STATE))
		{
			lcStrkRecvryWhlSt->s16AbsStrkRcvrCntForCycle = lcStrkRecvryWhlSt->s16AbsStrkRcvrCntForCycle + 1;
		}
		else {}
	}
	else
	{
		lcStrkRecvryWhlSt->s16AbsStrkRcvrCntForCycle = 0;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vSetStrkRcvrBrkForceTHD
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set stroke recovery braking force threshold
******************************************************************************/
static void LCABS_vSetStrkRcvrBrkForceTHD(void)
{
	if (AFZ_OK == 1)
	{
		lcabss16StrkRecSlipThd = LCABS_s16GetParamFromRef(-afz,U8_PULSE_LOW_MU,U8_PULSE_MEDIUM_MU,U8_PULSE_MED_HIGH_MU,
																S16_SR_REF_SLIP_THD_LOW_MU, S16_SR_REF_SLIP_THD_MED_MU, S16_SR_REF_SLIP_THD_MED_H_MU, S16_SR_REF_SLIP_THD_HIGH_MU);

		lcabss16StrkRecAradThd = LCABS_s16GetParamFromRef(-afz,U8_PULSE_LOW_MU,U8_PULSE_MEDIUM_MU,U8_PULSE_MED_HIGH_MU,
																S16_SR_REF_ARAD_THD_LOW_MU, S16_SR_REF_ARAD_THD_MED_MU, S16_SR_REF_ARAD_THD_M_HIGH_MU, S16_SR_REF_ARAD_THD_HIGH_MU);

		lcabss16StrkRecPulseUpCntThd = LCABS_s16GetParamFromRef(-afz,U8_PULSE_LOW_MU,U8_PULSE_MEDIUM_MU,U8_PULSE_MED_HIGH_MU,
																S16_SR_REF_PULSEUP_THD_LOW_MU, S16_SR_REF_PULSEUP_THD_MED_MU, S16_SR_REF_PULSEUP_THD_MED_H_MU, S16_SR_REF_PULSEUP_THD_HIGH_MU);

		lcabss16StrkRecDUMPRecvRateThd = LCABS_s16GetParamFromRef(-afz,U8_PULSE_LOW_MU,U8_PULSE_MEDIUM_MU,U8_PULSE_MED_HIGH_MU,
																S16_SR_REF_RCVR_P_THD_LOW_MU, S16_SR_REF_RCVR_P_THD_MED_MU, S16_SR_REF_RCVR_P_THD_MED_H_MU, S16_SR_REF_RCVR_P_THD_HIGH_MU);
	}
	else
	{
		lcabss16StrkRecSlipThd = S16_SR_REF_SLIP_THD_BFAFZOK;
		lcabss16StrkRecAradThd = S16_SR_REF_ARAD_THD_BFAFZOK;
		lcabss16StrkRecPulseUpCntThd = S16_SR_REF_PULSEUP_THD_BFAFZOK;
		lcabss16StrkRecDUMPRecvRateThd	= S16_SR_REF_RCVR_P_THD_BFAFZOK;
		}
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vCalcGradeOfBrakingForce
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Evaluate grade of braking force
******************************************************************************/
static void LCABS_vCalcGradeOfBrakingForce(struct W_STRUCT *WL_temp,  lcStrkRecvryWhlSt_t *lcStrkRecvryWhlSt)
{
	int8_t s8GradeRefCertainValueOfSpd = 50;
	int8_t s8GradeRefEstValueOfSpd = 50;
	int8_t s8GradeRefCertainValueOfPress = 50;
	int8_t s8GradeRefEstValueOfPress = 50;
	int8_t s8RefRatioEstValueOfSpd = 0;
	int8_t s8RefRatioEstValueOfPress = 0;


	if(WL_temp->lcabss16DumpDeltaWhlP != 0)
	{
		lcStrkRecvryWhlSt->s16DumpRecoveryRatio = (((WL_temp->s16_Estimated_Active_Press - WL_temp->lcabss16EstWhlPAftDump)*100)/WL_temp->lcabss16DumpDeltaWhlP);
	}
	else
	{
		lcStrkRecvryWhlSt->s16DumpRecoveryRatio = 1;/* yule temp : to avoid devided by zero in simulation */
	}

	if(vref > (int16_t)VREF_30_KPH)
	{
		s8RefRatioEstValueOfSpd = 50;
		lcStrkRecvryWhlSt->s8RefRatioEstValueOfSpd = 50;
	}
	else
	{
		s8RefRatioEstValueOfSpd = 30;
		lcStrkRecvryWhlSt->s8RefRatioEstValueOfSpd = 30;
	}

	if (AFZ_OK == 1)
	{
		s8RefRatioEstValueOfPress = 50;
		lcStrkRecvryWhlSt->s8RefRatioEstValueOfPress = 50;
	}
	else
	{
		s8RefRatioEstValueOfPress = 50;
		lcStrkRecvryWhlSt->s8RefRatioEstValueOfPress = 50;
	}

	s8GradeRefCertainValueOfSpd = (s8GradeRefCertainValueOfSpd*2) - s8RefRatioEstValueOfSpd;
	s8GradeRefEstValueOfSpd = s8RefRatioEstValueOfSpd;
	s8GradeRefCertainValueOfPress = (s8GradeRefCertainValueOfPress*2) - s8RefRatioEstValueOfPress;
	s8GradeRefEstValueOfPress = s8RefRatioEstValueOfPress;


	if(WL_temp->state == DETECTION)
	{
		lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt = (int16_t)(((int16_t)WL_temp->rel_lam*s8GradeRefEstValueOfSpd)/lcabss16StrkRecSlipThd);
		lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt = (int16_t)(((int16_t)WL_temp->arad*s8GradeRefCertainValueOfSpd)/lcabss16StrkRecAradThd);
		lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP = (int16_t)(((int16_t)WL_temp->s0_reapply_counter*s8GradeRefCertainValueOfPress)/lcabss16StrkRecPulseUpCntThd);
		lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP = (int16_t)(((int16_t)lcStrkRecvryWhlSt->s16DumpRecoveryRatio*s8GradeRefEstValueOfPress)/lcabss16StrkRecDUMPRecvRateThd);

		lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt = LCABS_s16LimitMinMax(lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt, 0, (s8GradeRefEstValueOfSpd + S16_SR_ALLOW_SUPPL_GRADE_WHL_ST));
		lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt = LCABS_s16LimitMinMax(lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt, 0, (s8GradeRefCertainValueOfSpd + S16_SR_ALLOW_SUPPL_GRADE_WHL_ST));
		lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP = LCABS_s16LimitMinMax(lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP, 0, (s8GradeRefCertainValueOfPress + S16_SR_ALLOW_SUPPL_GRADE_WHL_P));
		lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP = LCABS_s16LimitMinMax(lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP, 0, (s8GradeRefEstValueOfPress + S16_SR_ALLOW_SUPPL_GRADE_WHL_P));

		lcStrkRecvryWhlSt->s16GradeOfRefWhlSt = (lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt + lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt);
		lcStrkRecvryWhlSt->s16GradeOfRefWhlP = (lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP + lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP);
	}
	else
	{
		lcStrkRecvryWhlSt->s16SlipGradeOfRefWhlSt = 0;
		lcStrkRecvryWhlSt->s16AradGradeOfRefWhlSt = 0;
		lcStrkRecvryWhlSt->s16PulseUpGradeOfRefWhlP = 0;
		lcStrkRecvryWhlSt->s16RecoverPGradeOfRefWhlP = 0;

		lcStrkRecvryWhlSt->s16GradeOfRefWhlSt = 0;
		lcStrkRecvryWhlSt->s16GradeOfRefWhlP = 0;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecidePossibleStrkRec
* CALLED BY:          LCABS_s16DecideABSStrkRcvrTime()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide stroke recovery time refer to braking force
******************************************************************************/
static int16_t LCABS_s16DecidePossibleStrkRec(lcStrkRecvryWhlSt_t *pABSStrkRecRefWhl, lcStrkRecvryWhlSt_t *pABSStrkRecRefOppWhl)
{
	int16_t s16PossibleStrkRecTime = 0;
	uint8_t u8StrkRcvrRefAbsCycleThd = 1;
	uint8_t u8StrkRcvrRefWhlStLvl1Thd = U8_SR_REF_WHL_ST_L1_THD;
	uint8_t u8StrkRcvrRefWhlStLvl2Thd = U8_SR_REF_WHL_ST_L2_THD;
	uint8_t u8StrkRcvrRefWhlStLvl3Thd = U8_SR_REF_WHL_ST_L3_THD;
	uint8_t u8StrkRcvrRefWhlPLvl1Thd = U8_SR_REF_WHL_P_L1_THD;
	uint8_t u8StrkRcvrRefWhlPLvl2Thd = U8_SR_REF_WHL_P_L2_THD;
	uint8_t u8StrkRcvrRefWhlPLvl3Thd = U8_SR_REF_WHL_P_L3_THD;

	lcabsu8strkrcvrdebugger = 0;

	if(lcabss8RecommendStkRcvrLvl == S8_RCVR_RECOMMEND_LVL_1)
	{
		u8StrkRcvrRefAbsCycleThd = 1;
		u8StrkRcvrRefWhlStLvl1Thd = U8_SR_REF_WHL_ST_L1_THD;
		u8StrkRcvrRefWhlStLvl2Thd = U8_SR_REF_WHL_ST_L2_THD;
		u8StrkRcvrRefWhlStLvl3Thd = U8_SR_REF_WHL_ST_L3_THD;
		u8StrkRcvrRefWhlPLvl1Thd = U8_SR_REF_WHL_P_L1_THD;
		u8StrkRcvrRefWhlPLvl2Thd = U8_SR_REF_WHL_P_L2_THD;
		u8StrkRcvrRefWhlPLvl3Thd = U8_SR_REF_WHL_P_L3_THD;
	}
	else /*if(lcabss8RecommendStkRcvrLvl == S8_RCVR_RECOMMEND_LVL_2)*/
	{
		u8StrkRcvrRefAbsCycleThd = 2;
		u8StrkRcvrRefWhlStLvl1Thd = U8_SEVERE_SR_REF_WHL_ST_L1_THD;
		u8StrkRcvrRefWhlStLvl2Thd = U8_SEVERE_SR_REF_WHL_ST_L2_THD;
		u8StrkRcvrRefWhlStLvl3Thd = U8_SEVERE_SR_REF_WHL_ST_L3_THD;
		u8StrkRcvrRefWhlPLvl1Thd = U8_SEVERE_SR_REF_WHL_P_L1_THD;
		u8StrkRcvrRefWhlPLvl2Thd = U8_SEVERE_SR_REF_WHL_P_L2_THD;
		u8StrkRcvrRefWhlPLvl3Thd = U8_SEVERE_SR_REF_WHL_P_L3_THD;
	}

	if(pABSStrkRecRefWhl->s16AbsStrkRcvrCntForCycle < u8StrkRcvrRefAbsCycleThd)
	{
		if((pABSStrkRecRefWhl->s16GradeOfRefWhlSt >= u8StrkRcvrRefWhlStLvl1Thd) && (pABSStrkRecRefWhl->s16GradeOfRefWhlP >= u8StrkRcvrRefWhlPLvl1Thd))
		{
			s16PossibleStrkRecTime = S16_RECOVERY_MIN_ACT_TIME_ABC10MS + 1; /*temp*/
			lcabsu8strkrcvrdebugger = 100;
		}
		else if((pABSStrkRecRefWhl->s16GradeOfRefWhlSt >= u8StrkRcvrRefWhlStLvl2Thd) && (pABSStrkRecRefWhl->s16GradeOfRefWhlP >= u8StrkRcvrRefWhlPLvl2Thd))
		{
			s16PossibleStrkRecTime = S16_RECOVERY_MIN_ACT_TIME_ABC10MS;
			lcabsu8strkrcvrdebugger = 90;
		}
		else if((pABSStrkRecRefWhl->s16GradeOfRefWhlSt >= u8StrkRcvrRefWhlStLvl3Thd) && (pABSStrkRecRefWhl->s16GradeOfRefWhlP >= u8StrkRcvrRefWhlPLvl3Thd))
		{
			if((pABSStrkRecRefOppWhl->s16GradeOfRefWhlSt >= u8StrkRcvrRefWhlStLvl1Thd) && (pABSStrkRecRefOppWhl->s16GradeOfRefWhlP >= u8StrkRcvrRefWhlPLvl1Thd))
			{
				s16PossibleStrkRecTime = S16_RECOVERY_MIN_ACT_TIME_ABC10MS;
				lcabsu8strkrcvrdebugger = 80;
			}
			else
			{
				s16PossibleStrkRecTime = 0;
				lcabsu8strkrcvrdebugger = 50;
			}
		}
		else
		{
			s16PossibleStrkRecTime = 0;
			lcabsu8strkrcvrdebugger = 20;
		}
	}
	else
	{
		s16PossibleStrkRecTime = 0;
		lcabsu8strkrcvrdebugger = 0;
	}
	 #if __ABS_STK_RCVR_CTRL_MON == ENABLE
	referencemonwhl.s16SlipGradeOfRefWhlSt = pABSStrkRecRefWhl->s16SlipGradeOfRefWhlSt;
	referencemonwhl.s16AradGradeOfRefWhlSt = pABSStrkRecRefWhl->s16AradGradeOfRefWhlSt;
	referencemonwhl.s16PulseUpGradeOfRefWhlP = pABSStrkRecRefWhl->s16PulseUpGradeOfRefWhlP;
	referencemonwhl.s16RecoverPGradeOfRefWhlP = pABSStrkRecRefWhl->s16RecoverPGradeOfRefWhlP;

	referencemonwhl.s16GradeOfRefWhlSt = pABSStrkRecRefWhl->s16GradeOfRefWhlSt;
	referencemonwhl.s16GradeOfRefWhlP = pABSStrkRecRefWhl->s16GradeOfRefWhlP;
	referencemonwhl.s16AbsStrkRcvrCntForCycle = pABSStrkRecRefWhl->s16AbsStrkRcvrCntForCycle;
	 #endif

	return s16PossibleStrkRecTime;
	}

/******************************************************************************
* FUNCTION NAME:      LCABS_vDcdWhlCtrlReqForStrkRec
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Wheel Control Mode Request during Stroke recovery
******************************************************************************/
static void LCABS_vDcdWhlCtrlReqForStrkRec(int8_t s8StrkRcvrCtrlState, int8_t s8StrkRcvrCtrlStateOld)
	{
	if((s8StrkRcvrCtrlState==S8_STRK_RCVR_RECOVERY1_STATE) || (s8StrkRcvrCtrlState==S8_STRK_RCVR_RECOVERY2_STATE) || (s8StrkRcvrCtrlState==S8_STRK_RCVR_STABILIZING1_STATE) || (s8StrkRcvrCtrlState==S8_STRK_RCVR_STABILIZING2_STATE))
	{
		RL.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
		RR.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;

		if(s8StrkRcvrCtrlStateOld==S8_STRK_RCVR_READY_STATE) /* 1st scan: Check Initial Wheel Rise */
		{
			FL.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseBfStrkRec(&FL);
			FR.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseBfStrkRec(&FR);
		}
		else
		{
			FL.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
			FR.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
		}
	}
	else
	{
		if(s8StrkRcvrCtrlStateOld==S8_STRK_RCVR_STABILIZING2_STATE) /* after stroke recovery: check immediate rise */
		{
			FL.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&FL);
			FR.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&FR);

			if((FL.lcabss8WhlCtrlReqForStrkRec == S8_SR_WHLCTRLREQ_POSTRISE) || (FR.lcabss8WhlCtrlReqForStrkRec == S8_SR_WHLCTRLREQ_POSTRISE))
			{
				RL.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
				RR.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;

				if(((FL.lcabss8WhlCtrlReqForStrkRec == S8_SR_WHLCTRLREQ_POSTRISE) && (FL.s0_reapply_counter == 0))
					||((FR.lcabss8WhlCtrlReqForStrkRec == S8_SR_WHLCTRLREQ_POSTRISE) && (FR.s0_reapply_counter == 0)))
				{
					lcabss8FrontRiseTimeAfStrkRcvr = L_TIME_30MS;
				}
				else
				{
					lcabss8FrontRiseTimeAfStrkRcvr = L_TIME_10MS;
				}
			}
			else
			{
				RL.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&RL);
				RR.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&RR);

				lcabss8FrontRiseTimeAfStrkRcvr = L_TIME_0MS;
			}
		}
		else
		{
			lcabss8FrontRiseTimeAfStrkRcvr = (lcabss8FrontRiseTimeAfStrkRcvr > 0) ? (lcabss8FrontRiseTimeAfStrkRcvr - L_TIME_10MS) : 0;

			FL.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_NONE;
			FR.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_NONE;

			if(lcabss8FrontRiseTimeAfStrkRcvr > 0)
			{
				RL.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
				RR.lcabss8WhlCtrlReqForStrkRec = S8_SR_WHLCTRLREQ_HOLD;
			}
			else
			{
				RL.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&RL);
				RR.lcabss8WhlCtrlReqForStrkRec = LCABS_s8DcdWhlRiseAfStrkRec(&RR);
			}
		}
	}
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s8DcdWhlRiseBfStrkRec
* CALLED BY:          LCABS_vDcdWhlCtrlReqForStrkRec()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Wheel Rise Before Stroke Recovery
******************************************************************************/
static int8_t LCABS_s8DcdWhlRiseBfStrkRec(struct W_STRUCT *pWhl)
{
	int8_t s8PresModeForStrkRecvr;

	if((FL.state==DETECTION) && (FR.state==DETECTION)
			&& (FL.s0_reapply_counter >= 2) && (FR.s0_reapply_counter >= 2) && (FL.rel_lam >= -LAM_3P) && (FR.rel_lam >= -LAM_3P) )
	{
		if(pWhl->hold_timer_new >= L_TIME_20MS) /* Rise before S.R. after minimum hold time */
		{
			s8PresModeForStrkRecvr = S8_SR_WHLCTRLREQ_PRERISE;
		}
		else
		{
			s8PresModeForStrkRecvr = S8_SR_WHLCTRLREQ_HOLD;
		}
	}
	else
	{
		s8PresModeForStrkRecvr = S8_SR_WHLCTRLREQ_HOLD;
	}

	return s8PresModeForStrkRecvr;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s8DcdWhlRiseAfStrkRec
* CALLED BY:          LCABS_vDcdWhlCtrlReqForStrkRec()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Wheel Rise After Stroke Recovery
******************************************************************************/
static int8_t LCABS_s8DcdWhlRiseAfStrkRec(struct W_STRUCT *pWhl)
{
	int8_t s8PresModeForStrkRecvr;

	if(pWhl->lcabsu8ForcedHldForStrkRecvrCnt>0)
	{
		s8PresModeForStrkRecvr = S8_SR_WHLCTRLREQ_POSTRISE;
	}
	else
	{
		s8PresModeForStrkRecvr = S8_SR_WHLCTRLREQ_NONE;
	}

	return s8PresModeForStrkRecvr;
}

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCABSDecideCtrlState
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
