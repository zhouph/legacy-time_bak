#ifndef __LACBCCALLACTHW_H__
#define __LACBCCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"

#if __CBC
/*Global Type Declaration ****************************************************/
typedef struct
{
    uint16_t    u1CbcHvVlAct     :1;
    uint16_t    u1CbcAvVlAct     :1;
}LACBC_VALVE_t;


#define U8_CBC_DUMPSCAN_TIME                10

  #if __VDC && __CBC_FADE_OUT_LFC_RISE
#define	U8_Initial_Duty_Ratio_CBC_MIN		45	/*need to edit*/
#define	U8_Initial_Duty_Ratio_CBC_MAX		110	/*need to edit*/
#define	U8_PWM_DUTY_LIMIT_CBC				30	/*need to edit*/
#define	U8_Init_Duty_Comp_CBC_Min_vref		 0	/*need to edit*/
#define	U8_Init_Duty_Comp_CBC_Max_vref		 0	/*need to edit*/
  #endif



/*Global Extern Variable  Declaration*****************************************/
extern LACBC_VALVE_t lacbcValveFL, lacbcValveFR, lacbcValveRL, lacbcValveRR;

/*Global Extern Functions  Declaration****************************************/
extern void LACBC_vCallActHW(void);

#endif

#endif
