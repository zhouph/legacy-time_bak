#ifndef __LCHBBCALLCONTROL_H__
#define __LCHBBCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"


#if __TCMF_LowVacuumBoost
/*Global Type Declaration ****************************************************/

/* 120618 BKTU */
/*

#define U8_HBB_ENABLE_FLG    1  

#if (__CAR==GM_LAMBDA)
#define S16_HBB_ENTER_PEDAL_TRAVEL      1200
#else
#define S16_HBB_ENTER_PEDAL_TRAVEL      100
#endif

#if (__CAR==GM_LAMBDA)
#define S16_HBB_ENTER_INIT_PEDAL_RATE        50
#else
#define S16_HBB_ENTER_INIT_PEDAL_RATE        5
#endif

#define S16_HBB_MSC_INIT_TIME           10

#define S16_HBB_ASSIST_PRES_COMP_MIN 		MPRESS_20BAR
#define S16_HBB_ASSIST_PRES_MAX 			MPRESS_50BAR
#define S16_HBB_ASSIST_PRES_COMP_GAIN		3
#define S16_HBB_MSC_EBD_COMP_GAIN           7

#define S16_HBB_VACUUM_PRESS_LEVEL1		    200//240
#define S16_HBB_VACUUM_PRESS_LEVEL2		    320//360
#define S16_HBB_VACUUM_PRESS_LEVEL3		    440//480

#define S16_HBB_VEHICLE_VELOCITY1		VREF_5_KPH
#define S16_HBB_VEHICLE_VELOCITY2		VREF_30_KPH
#define S16_HBB_VEHICLE_VELOCITY3		VREF_80_KPH

#define U8_HBB_TARGET_PRESS_VEL_GAIN1	10//8
#define U8_HBB_TARGET_PRESS_VEL_GAIN2	10//9
#define U8_HBB_TARGET_PRESS_VEL_GAIN3	10

#define S16_HBB_KNEE_POINT_RATIO_1		12//11
#define S16_HBB_KNEE_POINT_RATIO_2		13//12
#define S16_HBB_KNEE_POINT_RATIO_3		14//13
#define S16_HBB_KNEE_POINT_RATIO_4		18

#define S16_HBB_INIT_LIMIT_ASSIST_PRESS	MPRESS_20BAR
#define S16_HBB_MIN_ASSIST_PRESS		MPRESS_5BAR
*/

#define HbbBrakePedalRelease			 1
#define HbbBrakePedalApply				 0

#define HBB_READY						0
#define HBB_BOOST_MODE					1
#define HBB_FADE_OUT					2
#define HBB_RAPID_FADE_OUT				3
#define HBB_FAIL_STATE_RAPID_FADE_OUT	4
#define HBB_SLOW_FADE_OUT				5

#define HBB_EXCEED_TRHES_MPRESS			HBB01.bit7
#define HBB_NEED_AdditionalBrkFluidP    HBB01.bit6
#define	HBB_NEED_AdditionalBrkFluidS    HBB01.bit5
#define	HBB_BRAKE_PEDAL_FAST_RELEASE    HBB01.bit4
#define	HBB_TC_Primary_ACT              HBB01.bit3
#define	HBB_TC_Secondary_ACT            HBB01.bit2
#define HBB_ESV_Primary_ACT             HBB01.bit1
#define HBB_ESV_Secondary_ACT           HBB01.bit0
                                             
#define HBB_EXCEED_MPRESS_5BAR			HBB02.bit7
#define HBB_BOOST_NEED_flg              HBB02.bit6
#define	HBB_PRESS_DECREASE_NEED_flg     HBB02.bit5
#define	HBB_START_flg                   HBB02.bit4
#define	HBB_PEDAL_PRESS                 HBB02.bit3
#define	HBB_PEDAL_TRAVEL_USE            HBB02.bit2
#define HBB_PREVENT_ASSIST_PRESS_JUMP   HBB02.bit1
#define HBB_MP_VALID                    HBB02.bit0

#define lcu1HbbInhibitFlag              HBB03.bit7
#define lcu1HbbPedalState               HBB03.bit6
#define	lcu1HbbMotorOffFlg              HBB03.bit5
#define	lcu1HbbPedalRateOK              HBB03.bit4
#define	HBB_flag_03                     HBB03.bit3
#define	HBB_flag_02                     HBB03.bit2
#define HBB_flag_01                     HBB03.bit1
#define HBB_flag_00                     HBB03.bit0

/*Global Extern Variable  Declaration*****************************************/
extern  struct	U8_BIT_STRUCT_t HBB01, HBB02, HBB03;

#if __ADVANCED_MSC
extern int16_t hbb_msc_target_vol;
#endif

 
extern uint8_t	TCMF_LowVacuumBoost_cnt, HBB_MSC_DEBUGGER;
extern int16_t	HBB_release_cnt,HBB_release_mpress_cnt, HBB_OFF_cnt, HBB_ON_cnt;	
extern int16_t	HBB_motor_delay_cnt, booster_pressure_avg700;
extern int16_t	lcs16HbbNeedBrkFluidCntP, lcs16HbbNeedBrkFluidCntS;
extern int16_t	lcs16HbbRearEBDCutPress;
extern int8_t	lcs8HbbMpressFastDecCnt, lcs8HbbPressMode, lcs8HbbMpressSlowDecCnt, lcs8HbbOtherCtrlEnable;
extern int16_t	lcs16HbbMscTargetVolFactor;
extern int16_t assist_pressure_inc_cnt, lcs16HbbKneePointEst;
extern int16_t lcs16HbbAssistPressureMotor,lcs16HbbAssistPressureOldMotor, lcs16HbbAssistPressureRateMotor, lcs16HbbAssistPressTC2;
extern int16_t lcs16HbbAssistPressureTC,lcs16HbbAssistPressureOldTC, lcs16HbbAssistPressureRateTC;

extern int16_t assist_pressure_filter, lcs16HbbPedalTravelCnt, lcs16HbbPedalTravelRate;
extern int16_t lcs16HbbTargetPressure;
extern uint8_t braking_after_timer;
extern int16_t lcs16HbbMCPressureAvg, lcs16HbbMCPressureSum, lcu8HbbMCPressureCnt, lcs16HbbMCPressureBuff[10];
extern int16_t lcs16HbbTargetPressureComp;

extern int16_t    lcs16HbbVacuumPres, lcs16HbbPedalTravel;


/*Global Extern Functions  Declaration****************************************/

    #if __ADVANCED_MSC
extern void LCMSC_vSetHBBTargetVoltage(void);
    #endif

extern void LCHBB_vCallControl(void);

#endif
#endif
