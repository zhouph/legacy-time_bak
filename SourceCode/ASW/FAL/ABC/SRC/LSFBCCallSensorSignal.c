/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LSFBCCallSensorSignal.c
* Description:		Sensor Signal Processiong of FBC
* Logic version:	HV26
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C15			eslim			Initial Release
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSFBCCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/
#include "LSFBCCallSensorSignal.h"

#if __FBC


/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/
void LSFBC_vCallSensorSignal(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LSFBC_vCallSensorSignal
* CALLED BY:			LS_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Sensor Signal of FBC
********************************************************************************/
void LSFBC_vCallSensorSignal(void)
{

}

#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSFBCCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
