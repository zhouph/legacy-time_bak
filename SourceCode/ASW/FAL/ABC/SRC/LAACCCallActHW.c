/* Includes ********************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAACCCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LAACCCallActHW.h"
  #if __ACC
#include "LADECCallActHW.h"
#include "LCACCCallControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
#include "hm_logic_var.h"
#include "LCDECCallControl.h"
   #if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
   #endif
  #endif
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __ACC
 
static uint8_t		lcu8AccValveMode;
uint8_t		lcu8AccMotorMode;
 

ACC_VALVE_t	laAccValveP, laAccValveS;
  #endif

/* Local Function prototype ****************************************/
  #if __ACC
   #if __TCMF_CONTROL
static void LAACC_vOriginalValveCmd(void);
   #endif
int16_t LAMFC_s16SetMFCCurrentACC(ACC_VALVE_t *pAcc, int16_t MFC_Current_old);   
  #endif

/* Implementation***************************************************/
  #if __ACC
void LAACC_vCallActHW(void)
{
	if(lcu1AccActiveFlg==1)
	{
	  #if !__TCMF_CONTROL
		HV_VL_fl=lcu8DecFlNoValve;
		AV_VL_fl=lcu8DecFlNcValve;
		HV_VL_fr=lcu8DecFrNoValve;
		AV_VL_fr=lcu8DecFrNcValve;
		HV_VL_rl=lcu8DecRlNoValve;
		AV_VL_rl=lcu8DecRlNcValve;
		HV_VL_rr=lcu8DecRrNoValve;
		AV_VL_rr=lcu8DecRrNcValve;
	  #endif
		  #if __SPLIT_TYPE		/* FR Split */
		S_VALVE_PRIMARY=lcu8DecFlFrEsvValve;
		S_VALVE_SECONDARY=lcu8DecRlRrEsvValve;
		TCL_DEMAND_PRIMARY=lcu8DecFlFrTcValve;
		TCL_DEMAND_SECONDARY=lcu8DecRlRrTcValve;
		  #else				/* X Split */
		S_VALVE_RIGHT=lcu8DecFrRlEsvValve;
		S_VALVE_LEFT=lcu8DecFlRrEsvValve;
		TCL_DEMAND_fr=lcu8DecFrRlTcValve;
		TCL_DEMAND_fl=lcu8DecFlRrTcValve;
		  #endif
		lcu8AccValveMode=lcu8DecValveMode;
		lcu8AccMotorMode=lcu8DecMotorMode;
	}
	else
	{
		;
	}
	  #if __TCMF_CONTROL
	LAACC_vOriginalValveCmd();
	  #endif
}


  #if __TCMF_CONTROL
void LAACC_vOriginalValveCmd(void)
{
	if(lcu8AccValveMode==U8_ACC_HOLD)
	{
		ACC_HV_VL_fl=1;
		ACC_AV_VL_fl=0;
		ACC_HV_VL_fr=1;
		ACC_AV_VL_fr=0;
		ACC_HV_VL_rl=1;
		ACC_AV_VL_rl=0;
		ACC_HV_VL_rr=1;
		ACC_AV_VL_rr=0;
	}
	else if(lcu8AccValveMode==U8_ACC_APPLY)
	{
		ACC_HV_VL_fl=0;
		ACC_AV_VL_fl=0;
		ACC_HV_VL_fr=0;
		ACC_AV_VL_fr=0;
		ACC_HV_VL_rl=0;
		ACC_AV_VL_rl=0;
		ACC_HV_VL_rr=0;
		ACC_AV_VL_rr=0;
	}
	else if(lcu8AccValveMode==U8_ACC_DUMP)
	{
		ACC_HV_VL_fl=1;
		ACC_AV_VL_fl=1;
		ACC_HV_VL_fr=1;
		ACC_AV_VL_fr=1;
		ACC_HV_VL_rl=1;
		ACC_AV_VL_rl=1;
		ACC_HV_VL_rr=1;
		ACC_AV_VL_rr=1;
	}
	else
	{
		;
	}
}
  #endif

int16_t LAMFC_s16SetMFCCurrentACC(ACC_VALVE_t *pAcc, int16_t MFC_Current_old)
{
    int16_t     MFC_Current_new = 0;
    if(lcu1AccActiveFlg==1)
    {
        if(MFC_Current_old==0)
        {
            MFC_Current_new=450;
        }
        else
        {
            /********************************************************/
            if((pAcc->lau1AccWL1No==0)&&(pAcc->lau1AccWL1Nc==0))            /* REAPPLY */
            {
                tempW0=abs(lcs16AccTargetG*5)+550;
                if(MFC_Current_old>=tempW0)
                {
                    MFC_Current_new=tempW0;
                }
                else
                {
                    MFC_Current_new=MFC_Current_old+3;
                }
                }
                /********************************************************/
                else if((pAcc->lau1AccWL1No==1)&&(pAcc->lau1AccWL1Nc==0))       /* HOLD */
                {
                    MFC_Current_new=MFC_Current_old;
            }
            /********************************************************/
            else if((pAcc->lau1AccWL1No==1)&&(pAcc->lau1AccWL1Nc==1))       /* DUMP */
            {
                if(MFC_Current_old>200)
                {
                    if(lcs8DecState==S8_DEC_CLOSING)
                    {
                        MFC_Current_new=MFC_Current_old-(int16_t)(lcs16DecClosingCur/L_U8_TIME_10MSLOOP_200MS);
                    }
                    else
                    {
                        MFC_Current_new=MFC_Current_old-3;
                    }
                }
                else
                {
                    MFC_Current_new=200;
                }
                }
            /********************************************************/
                else                                /* Free Circulation */
                {
                    MFC_Current_new = MFC_Current_old;
                }
        }
    }
    else
    {
        MFC_Current_new=LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_old);
    }

    return MFC_Current_new;
}


  #if __ADVANCED_MSC
void LCMSC_vSetACCTargetVoltage(void)
{
	if(lcu1AccActiveFlg==1)
	{
		acc_msc_target_vol=lcs16DecMscTargetVol;
		ACC_MSC_MOTOR_ON=1;
	}
	else
	{
		acc_msc_target_vol=0;
		ACC_MSC_MOTOR_ON=0;
	}
}
   #endif
  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAACCCallActHW
	#include "Mdyn_autosar.h"               
#endif  
