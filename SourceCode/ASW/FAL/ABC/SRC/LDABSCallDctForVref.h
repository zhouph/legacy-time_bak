#ifndef	__LDABSVREFDCT_H__
#define	__LDABSVREFDCT_H__
/*includes********************************************************************/
#include	"LVarHead.h"
/*Global Type Declaration ****************************************************/

#if	__EDC || __TCS || __ETC
#define		__ENGINE_INFORMATION	1
#else
#define		__ENGINE_INFORMATION	0
#endif

#define		__NON_DRIVEN_WHL_RELIABILITY_CHECK	1 
#define		__ABS_ENTRY_SPEED_REF		0

#define		__VREF_USING_AX_SENSOR		0

#if	(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)
#define		__ACCEL_ON_BRK_VREF		0
#else
#define		__ACCEL_ON_BRK_VREF		1
#endif

#define	   __MGH60_NEW_SELECTION	1	/* J.K.Lee */

#if (__4WD ||(__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)&&(__VDC==1)
#define     __SPIN_DET_IMPROVE  0
#else
#define     __SPIN_DET_IMPROVE  0
#endif

#if  (__4WD ||(__4WD_VARIANT_CODE==ENABLE))&& __AX_SENSOR
#define    __IMPROVED_VREF_IN_TCS_1  DISABLE
    #else   
#define    __IMPROVED_VREF_IN_TCS_1  DISABLE
    #endif

#if __VDC
	#if((__GEAR_SWITCH_TYPE==CAN_TYPE)||(__GEAR_SWITCH_TYPE==ANALOG_TYPE))
	#define __R_GEAR_INFORM_FOR_MT 1
	#else
	#define __R_GEAR_INFORM_FOR_MT 0
	#endif
#endif

#define	SUSPECT_VREF					VDCTF0.bit7
#define	PARKING_BRAKE_OPERATION			VDCTF0.bit6
#define	ABS_fz_K						VDCTF0.bit5
#define	ABS_fz_OLD						VDCTF0.bit4
#define	BRAKE_SIGNAL_K					VDCTF0.bit3
#define	BRAKE_SIGNAL_OLD				VDCTF0.bit2
#define	LONG_ACCEL_STATE			 	VDCTF0.bit1
#define	ldvrefu1DctVehAccelSpinOnBrk   	VDCTF0.bit0


#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))


#define	ACCEL_SPIN_FZ				VDCTF1.bit7
#define	BIG_ACCEL_SPIN_FZ			VDCTF1.bit6
#define	ACC_CHANGE					VDCTF1.bit5
#define	VRAD_DIFF_MAX_OK_FLG		VDCTF1.bit4
#define	CONTROL_FAIL				VDCTF1.bit3
#define	VREF_FAIL_MODE				VDCTF1.bit2
#define	VREF_FAIL					VDCTF1.bit1
#define	FREE_ROLLING_DETECT			VDCTF1.bit0
#endif

#if	__AX_SENSOR
#define	VrefDct_Flag_Unused2_7		VDCTF2.bit7
#define	USE_ALONG_1					VDCTF2.bit6
#define	ACCEL_SPIN_FADE_OUT_FLAG		VDCTF2.bit5
#define	VrefDct_Flag_Unused2_4		VDCTF2.bit4
#define	STANDSTILL					VDCTF2.bit3
#define	EFFECTIVE_ALONG				VDCTF2.bit2
#define	BACKWARD_MOVE				VDCTF2.bit1
#define	SPIN_CALC_FLG			 	VDCTF2.bit0
#endif

#if	__VDC
#define	VREF_FAIL_ON_ESP			VDCTF3.bit7
#define	ESP_BRAKE_CONTROL_WL		VDCTF3.bit6
#define	ALONG_FAIL_BY_DRIFT_ON_ABS	VDCTF3.bit5
#define	ALONG_FAIL_BY_DRIFT_ON_ESP	VDCTF3.bit4
#define	ESP_BRAKE_CONTROL_CNT_WL	VDCTF3.bit3
#define	VDC_REF_WOKR_OLD			VDCTF3.bit2
#define	VDCTF3_Unused3				VDCTF3.bit1
#define	VDCTF3_Unused4				VDCTF3.bit0
#endif

#define	ldabsu1CanErrFlg			VDCTF4.bit7
#define	ldabsu1ParkingBrkActSignal	VDCTF4.bit6
#define	ABS_fz_A					VDCTF4.bit5
#define	USE_ALONG					VDCTF4.bit4
#define	ldu1AbsEntrySpeedUpdated	VDCTF4.bit3
#define ldu1SlowestWheelSelectFlag  VDCTF4.bit2 
#define lsu1VcaReqbyAllWhlSpinFlg   VDCTF4.bit1 
#define	lsu1VcaReqbySpinSusFlg		VDCTF4.bit0

#if (__4WD||(__4WD_VARIANT_CODE==ENABLE))

#define ldu1nospinstate      		VDCTF5.bit7 
#define ldu1wheelaxgSameSign        VDCTF5.bit6
#define ldu1ngearstop               VDCTF5.bit5
#define ldu1ngeardrv                VDCTF5.bit4
#define ldu1grdstateduration        VDCTF5.bit3
#define ldu1vehSpinDet              VDCTF5.bit2
#define ldu1vehSpinDetOld           VDCTF5.bit1
#define ldu1gradechangeflag         VDCTF5.bit0

#endif

#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
#define ldu1absl2hdetectionflag     VDCTF6.bit7
#define ldu1absl2hFilterRespon      VDCTF6.bit6
#define VDCTF6_Unused5              VDCTF6.bit5
#define VDCTF6_Unused4              VDCTF6.bit4
#define VDCTF6_Unused3              VDCTF6.bit3
#define VDCTF6_Unused2              VDCTF6.bit2
#define VDCTF6_Unused1              VDCTF6.bit1
#define VDCTF6_Unused0              VDCTF6.bit0
#endif

#if	__VDC
	#if (__R_GEAR_INFORM_FOR_MT==1)
	#define ldu1RgearSigFail			RGEARSTATE.bit7
	#define bit_temp6		            RGEARSTATE.bit6
	#define bit_temp5					RGEARSTATE.bit5
	#define bit_temp4					RGEARSTATE.bit4
	#define bit_temp3					RGEARSTATE.bit3
	#define bit_temp2					RGEARSTATE.bit2
	#define bit_temp1					RGEARSTATE.bit1
	#define bit_temp0					RGEARSTATE.bit0
	#endif
#endif


/*Global Extern	Variable  Declaration*****************************************/

extern U8_BIT_STRUCT_t	  VDCTF0,VDCTF4;
#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))
extern U8_BIT_STRUCT_t	  VDCTF1;
extern U8_BIT_STRUCT_t    VDCTF5;
#endif

#if	__AX_SENSOR
extern U8_BIT_STRUCT_t	  VDCTF2;
#endif

#if	__VDC
extern U8_BIT_STRUCT_t	  VDCTF3;
extern U8_BIT_STRUCT_t    VDCTF6;
#endif

	#if	 (__4WD	 ||(__4WD_VARIANT_CODE==ENABLE))	&& __AX_SENSOR 
	#if	__VDC
	extern int16_t	along_by_drift;
	#endif
	extern int16_t along_vref;
	#endif
	extern int16_t	ABS_entry_speed;
	extern int16_t	ABS_entry_speed2;
	extern uint8_t  abs_duration_timer;
	#if	 (__4WD	 ||(__4WD_VARIANT_CODE==ENABLE))
	#if	 __AX_SENSOR 
	
	extern int16_t	vref_by_along;
	extern int16_t	WheelAccelG_1;
	extern int16_t	lds16HPFWheelAccel;
	extern int16_t	lds16HPFLongG;
	extern int16_t	lds16VrefDelta;
	
	extern int8_t lds8SpinSusCount;
	extern int8_t lds8SpinSetCount;
	extern int8_t lds8SpinSetCount_2;
	extern int8_t lds8SpinSetCount_3;
	extern uint8_t lds8SpinClearCnt;
	extern int8_t lds8BigSpinSetCnt;
	extern int8_t lds8BigSpinClearCnt;
	extern int16_t lds16AccelSpinSustainCnt;	
    extern int16_t  lds16vdiffbyalongmod;
    extern int16_t  lds16vdiffbyalong;
    extern int16_t  lds16vrefbyalong;
    extern int16_t  lds16vrefbyalongold;
     
	#endif

    extern uint8_t SPIN_RESET_FLG;
	extern int16_t	 lds8BigSpinHoldCount;
	extern uint8_t	accel_det_flags;
	
	#endif
	
	#if	__AX_SENSOR	   
	extern int16_t	along_standstill, di_along;	   
	#endif
	
	#if	__VDC
	extern int16_t	ESP_entry_speed;
	#endif

#if	__VDC
	#if (__R_GEAR_INFORM_FOR_MT==1)
	extern U8_BIT_STRUCT_t RGEARSTATE ;
	#endif
	extern uint8_t Reverse_Gear_cnt;
	extern uint8_t ldu8RgearErrorCnt;
	extern int16_t lds16espBackwardDirCNT;
	extern int16_t lds16espDetYawmYawcDiff;
	extern int16_t lds16espDetYawmYawcDiffFilt;
	extern int16_t lds16espDetYawmYawcDiffOld;
	extern int16_t lds16espBackFlags;
#endif

#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
    extern int8_t  lds8lowmudetectioncnt;
    extern int8_t  lds8midmudetectioncnt;
    extern int8_t  lds8highmudetectioncnt;
    extern uint8_t ldu8muchangeholdtimer;
    extern uint8_t ldu8absmustate;
    extern uint8_t ldu8absmustateold;
#endif

#if __SPIN_DET_IMPROVE == ENABLE

extern int16_t tempcnt1;
extern int16_t tempcnt_tmp;
extern int16_t ls16vehgradient_tmp;
extern int16_t ldu8vehiclestopstablecnt;
extern int16_t ldu8vehiclestablecnt;
extern int16_t lds16vehgradientG;
extern int16_t lds16vehgradientGold;
extern int16_t lds16vehgradientGtmp;
extern uint8_t ldu8vehSpinCnt;
extern uint8_t ldu8vehSpinCntOld;
extern int16_t ls16Spindetfactor;
extern uint8_t ldu8grdindex;
extern int16_t lds16tempinput;
extern uint8_t ldu8gearpositionforgrd;

struct WL_SPIN_STRUCT
{
	uint8_t lu8spindetcnt;
	uint8_t lu8spinclrcnt;
	uint8_t lu8spinclrcnt2;
	unsigned ldu1SelWLSpinDet:                    1;
	unsigned ldu1SameSign:                        1;
};
extern struct WL_SPIN_STRUCT *SPIN_WL, SPIN_FL, SPIN_FR, SPIN_RL, SPIN_RR;

struct AX_ITG_STRUCT
{
	int16_t lds16AxSenForVrefCac;
	int16_t lds16VdiffByAxSenmod;
	int16_t lds16VdiffByAxSen;
	int16_t lds16VspeedByAxSen;
	int16_t lds16Caclcnt;
	int16_t lds16VspeedByAxSenOld;
};
extern struct AX_ITG_STRUCT *AX_P, AX_1_P;

#endif

/*Global Extern	Functions  Declaration****************************************/
extern void		LDABS_vCallDctForVref(void);
extern void		LDABS_vArrangeWheelSpeed(int16_t INPUT_1,int16_t INPUT_2,int16_t INPUT_3,int16_t INPUT_4);
#endif
