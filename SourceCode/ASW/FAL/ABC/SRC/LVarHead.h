#ifndef LVARHEAD_H
#define LVARHEAD_H

#define OPTRAM_PAGED_1

//#ifdef OPTIME_MACFUNC_3
#define McrAbs(x1) (((INT)(x1) >= 0) ? (INT) (x1) : (INT) (-(x1)))
//#else
//#define McrAbs abs    /* skeon: abs library 함수 사용시 */
//#endif

#include "LBaseHead.h"

//#include "../CAL/LTunePar.h"
#include "LTunePar.h"
#include "algo_var.h"
#include "edc_var.h"
#include "LSCANSignalInterface.h"
#if __VDC
    #include "tcs_var.h"
#endif

#if __VDC
    #include "esp_var.h"

    #if !__MODEL_CHANGE
    	#if __CAR==SYC_RAXTON/*|| __CAR==HMC_EN*/|| __CAR==SYC_KYRON
    	#else
    	#include "esp_model_var.h"/*060602 for HM reconstruction by eslim*/
    	#endif
	#endif

    #if __TIRE_MODEL
//	   	#include esp_tire_force.h"
    #endif
#endif

#if defined(__UCC_1)
    #include "../SRC/UCC_Logic/UCC.h"
#endif

#if __TSP
    #include "tsp_var.h"
#endif




/* <skeon optime_macfunc_1>: 0303 */

#ifdef OPTIME_MACFUNC_1

#define MAXINT(x1,x2)      (((INT)(x1) > (INT)(x2)) ? (INT)(x1) : (INT)(x2))
#define _NONZERO(x)    ((x)!=0) ? x : 1)

#define   LCESP_s16IInter2Point(x,x1,y1,x2,y2) \
((x<=x1) ? y1 : ((x >= x2) ? y2 : (y1+(INT)(((long)(y2-y1)*(x-x1))/(MAXINT(x2-x1,1))) ) ) )

     
#define   LCESP_s16IInter4Point(x,x1,y1,x2,y2,x3,y3,x4,y4) \
((x<=x1) ? y1 : \
  ((x <= x2) ? (y1+(INT)(((long)(y2-y1)*(x-x1))/(MAXINT(x2-x1,1)))) : \
     ((x <= x3) ? (y2+(INT)(((long)(y3-y2)*(x-x2))/(MAXINT(x3-x2,1)))) : \
       ((x <= x4) ? (y3+(INT)(((long)(y4-y3)*(x-x3))/(MAXINT(x4-x3,1)))) : y4 ) )))


#define   LCESP_s16Lpf1Int(x,x1,ig) \
((INT)((((LONG)x)*((INT)ig)+(((LONG)x1)*(128-(INT)ig)))/128))


#endif    /* </optime_macfunc_1> */


#ifdef OPTIME_MACFUNC_2 
#define LCTCS_s16ILimitMinimum(x, minv) (((x)<(minv)) ? (INT) (minv) : (INT) (x))
#define LCTCS_s16ILimitMaximum(x, maxv) (((x)>(maxv)) ? (INT) (maxv) : (INT) (x))
#define LCTCS_s16ILimitRange(x, minv, maxv) (((x)>(maxv)) ? (INT) (maxv) : ( ((x) < (minv)) ? (INT) (minv) : (INT) (x) ))
#define LCTCS_s16IFindMaximum(x1, x2) (((x1)>(x2)) ? (INT) (x1) : (INT) (x2))
#define LCABS_s16LimitMinMax(x, minv, maxv) (((x)>(maxv)) ? (char16_t) (maxv) : ( ((x) < (minv)) ? (char16_t) (minv) : (char16_t) (x) ))
#define LCABS_s16DecreaseUnsignedCnt(x_old, inc) (((x_old+inc)>0) ? (char16_t) (x_old+inc) : 0)
#define LDABS_s16s16MAX(x1, x2) (((x1) >= (x2)) ? (INT) x1 : (INT) x2)
#define LCTCS_s16IFindMinimum(x1, x2) (((x1) > (x2)) ? (INT) x2 : (INT) x1)
#define LCTCS_u16ILimitMaximum(x, maxv) (((UINT)(x) > (UINT)(maxv)) ? (UINT) maxv : (UINT) x)
#define LDABS_s16s16MIN(x1, x2) (((x1) >= (x2)) ? (UINT) x2 : (UINT) x1)


#endif  /* </optime_macfunc_2> */


#endif
