/******************************************************************************
* Project Name: BRAKE DISC WIPING
* File: LDBDWCallDetection.C
* Date: Nov. 30. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDBDWCallDetection
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/


#include "LDBDWCallDetection.h"
#include "LCBDWCallControl.h"
#include "LAEPCCallActHW.h"


#if __HDC
#include "LCHDCCallControl.h"
#endif

#if __SCC
#include "../SRC/SCC/LCWPCCallControl.h"
#endif

#if __EBP
#include "LDEBPCallDetection.h"
#include "LCEBPCallControl.h"
#endif
#include "LCESPCalInterpolation.h"

#include "hm_logic_var.h"

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

U8_BIT_STRUCT_t BDWD0, BDWD1;

#if __BDW
/* Logcal Definiton  *********************************************************/
#define     BDW_DEMO_TEST    0

/* Variables Definition*******************************************************/

/* LocalFunction prototype ***************************************************/
void LDBDW_vDetectDiscCleanActCondition(void);
void LDBDW_vCallDetection(void);
void LDBDW_vDetectActCondition(void);
void LDBDW_vDetectDisable(void);


/* Implementation*************************************************************/

 

static int16_t bdw_tempD9,bdw_tempD8,bdw_tempD7,bdw_tempD6,bdw_tempD5;
static int16_t bdw_tempD4,bdw_tempD3,bdw_tempD2,bdw_tempD1,bdw_tempD0;

static int16_t bdw_enable_speed_th,bdw_disable_speed_th;
static int16_t bdw_enable_mtp_th,bdw_disable_mtp_th;
static int16_t bdw_enable_mpress_th,bdw_disable_mpress_th;
static int16_t bdw_enable_alat_th,bdw_disable_alat_th;
static int16_t bdw_enable_disctemp_th,bdw_disable_disctemp_th;

static int16_t driver_brake_time_cnt;
static int16_t BDW_inihibit_delay_cnt;


int16_t bdw_contorl_priod,bdw_active_time;
int16_t lds16BDWWiperOnTime;
uint8_t lsbdwu8RainSnsStat;
 

void LDBDW_vCallDetection(void)
{
    LDBDW_vDetectDiscCleanActCondition();
    LDBDW_vDetectActCondition();
    LDBDW_vDetectDisable();
}

void LDBDW_vDetectDiscCleanActCondition(void){


    if ((DISC_CLEAN_NEED_TEMP==0)&&(U8_BDW_Initial_Clean_Enable==1))
    {
        DISC_CLEAN_NEED=1;
        DISC_CLEAN_NEED_TEMP=1;
    }
    else
    {
        ;
    }

    if ((DISC_CLEAN_NEED==1)
        &&((DETECT_RAIN==1)
        ||(driver_brake_time_cnt > bdw_contorl_priod)
        ||(bdw_active_counter >= 1))
        )
    {
        DISC_CLEAN_NEED=0;
    }
    else
    {
        ;
    }
}

void LDBDW_vDetectActCondition(void){

    bdw_enable_speed_th     = S16_BDW_ENT_SPEED;   /* 60kph */
    bdw_disable_speed_th    = S16_BDW_EXT_SPEED;   /* 50kph */
    bdw_enable_mtp_th       = S16_BDW_ENT_MTP;     /* 3% */   
    bdw_disable_mtp_th      = S16_BDW_EXT_MTP;     /* 2% */   
    bdw_enable_mpress_th    = S16_BDW_ENT_MPRESS;  /* 4bar */ 
    bdw_disable_mpress_th   = S16_BDW_EXT_MPRESS;  /* 5bar */ 
    bdw_enable_alat_th      = S16_BDW_ENT_ALATM;   /* 0.2g */ 
    bdw_disable_alat_th     = S16_BDW_EXT_ALATM;   /* 0.25g */
    bdw_enable_disctemp_th  = S16_BDW_ENABLE_DISK_TEMP;           
    bdw_disable_disctemp_th = S16_BDW_DISABLE_DISK_TEMP;           

    if (BDW_DEMO_TEST==1)
    {
        if (bdw_active_counter <= 1)
        {
          #if defined(__H2DCL) && defined(__CAN_SW)
            DETECT_RAIN = ctu1BDW;
          #else
            DETECT_RAIN = 0;
          #endif
        }
        else
        {
            DETECT_RAIN = 0 ;
        }
        bdw_contorl_priod       = L_U16_TIME_10MSLOOP_10S;
        bdw_active_time         = L_U16_TIME_10MSLOOP_3S; 
    }
    else
    {
        DETECT_RAIN = 0;        /*  CAN 정보 추가 필요함. */
        bdw_contorl_priod       = S16_BDW_INTERVAL_TIME*L_U8_TIME_10MSLOOP_1000MS;  /* 90sec */
        bdw_active_time         = S16_BDW_ACTIVE_TIME;   /*  3sec */
    }

  /* ----------------------------------------------------------------- */
    
    bdw_tempD4 = S16_BDW_RAIN_DETECT_TIME*L_U8_TIME_10MSLOOP_1000MS; /* 10sec */
    
    if (((lsbdwu1WiperIntSW==1)||(lsbdwu1WiperLow==1)||(lsbdwu1WiperHigh==1))
    	&&(lds16BDWWiperOnTime<bdw_tempD4)&&(lsbdwu1Clu2WiperMsgRxOkFlg==1))
    {	
        DETECT_RAIN = 0;
        lds16BDWWiperOnTime++;
    }
    else if(((lsbdwu1WiperIntSW==1)||(lsbdwu1WiperLow==1)||(lsbdwu1WiperHigh==1))
    	&&(lds16BDWWiperOnTime>=bdw_tempD4)&&(lsbdwu1Clu2WiperMsgRxOkFlg==1))
    {
        DETECT_RAIN = 1;
    }
    else
    {
    	DETECT_RAIN = 0;
    	lds16BDWWiperOnTime = 0;
    }	
    
  /* ----------------------------------------------------------------- */  

    bdw_tempD9 = LCESP_s16IInter2Point( vref , bdw_enable_speed_th/2 , 5, bdw_enable_speed_th, 10 );
  #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
    bdw_tempD8 = LCESP_s16IInter2Point( lsesps16EstBrkPressByBrkPedalF , MPRESS_3BAR , 10, MPRESS_15BAR, 50 );
  #else
    bdw_tempD8 = LCESP_s16IInter2Point( mpress , MPRESS_3BAR , 10, MPRESS_15BAR, 50 );
  #endif  

    if (driver_brake_time_cnt < 0){
        driver_brake_time_cnt = 0;
    }
  #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if ((lsesps16EstBrkPressByBrkPedalF > MPRESS_3BAR)&&(lsespu1BrkAppSenInvalid == 0)
        &&(vref > (bdw_enable_speed_th/2))
        &&(driver_brake_time_cnt < (bdw_contorl_priod)))
  #else
    else if ((mpress > MPRESS_3BAR)
        &&(vref > (bdw_enable_speed_th/2))
        &&(driver_brake_time_cnt < (bdw_contorl_priod)))
  #endif       
    {
        bdw_tempD7 = bdw_tempD9*bdw_tempD8/100*(bdw_contorl_priod/bdw_active_time);
        driver_brake_time_cnt = driver_brake_time_cnt + bdw_tempD7;
    }
    else if (driver_brake_time_cnt > 0)
    {
        driver_brake_time_cnt = driver_brake_time_cnt - 1;
    }
    else
    {
        ;
    }

    if ((BDW_ACT_CONDTION==0)
        &&(vref > bdw_enable_speed_th)
        &&(mtp > bdw_enable_mtp_th)
        &&((DETECT_RAIN==1)||(DISC_CLEAN_NEED==1))
        &&(det_alatm < bdw_enable_alat_th)
/*        &&(FL.btc_tmp < bdw_enable_disctemp_th)
        &&(FR.btc_tmp < bdw_enable_disctemp_th)
        &&(RL.btc_tmp < bdw_enable_disctemp_th)
        &&(RR.btc_tmp < bdw_enable_disctemp_th) */
        &&(driver_brake_time_cnt <= 0)
      #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
        &&((lsesps16EstBrkPressByBrkPedalF < bdw_enable_mpress_th)&&(lsespu1BrkAppSenInvalid == 0))
      #else
        &&(mpress < bdw_enable_mpress_th)
      #endif  
        )
    {
        BDW_ACT_CONDTION=1;
    }
    else if ((vref < bdw_disable_speed_th)
        ||(mtp < bdw_disable_mtp_th)
        ||((!DETECT_RAIN)&&(!DISC_CLEAN_NEED))
        ||(det_alatm > bdw_disable_alat_th)
/*        ||(FL.btc_tmp > bdw_disable_disctemp_th)
        ||(FR.btc_tmp > bdw_disable_disctemp_th)
        ||(RL.btc_tmp > bdw_disable_disctemp_th)
        ||(RR.btc_tmp > bdw_disable_disctemp_th)    */
        ||(driver_brake_time_cnt > 0)
      #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)   
        ||((lsesps16EstBrkPressByBrkPedalF > bdw_disable_mpress_th)&&(lsespu1BrkAppSenInvalid == 0))
      #else
        ||(mpress > bdw_disable_mpress_th)
      #endif
        )
    {
        BDW_ACT_CONDTION=0;
    }
    else { }

}

void LDBDW_vDetectDisable(void){

    if ((YAW_CDC_WORK==1)||(ESP_PULSE_DUMP_FLAG==1)||(ABS_fz==1)||(BTC_fz==1)||(EBD_RA==1)||
        (EPC_ON||PBA_ON==1)
        #if __EBP
        ||(EBP_ON==1)
        #endif
        #if __HDC
        ||(lcu1HdcActiveFlg==1)
        #endif
        #if __TSP
        ||(TSP_ON==1)
        #endif
        #if __SCC
        ||(lcu8WpcActiveFlg==1)
        #endif
    
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
        ||(lsespu1BrkAppSenInvalid == 1)
    #endif
    
	#if __GM_FailM
		||(fu1ESCEcuHWErrDet==1)																		/* ESC HW	*/
		||(fu1MCPErrorDet==1)																			/* MP		*/
		||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)	/* Wheel	*/
		||(fu1YawErrorDet==1)																			/* YAW		*/
		||(fu1AyErrorDet==1)																			/*	LAT		*/
		||(fu1MainCanLineErrDet==1)																		/* GMLAN (Vehicle)	*/
																										/* GMLAN (ESC Sensor)	*/
		||(fu1OnDiag==1)
		||(init_end_flg==0)	
		||(wu8IgnStat==CE_OFF_STATE)	/* IGN OFF */																								            
	  #if __BRAKE_FLUID_LEVEL
		||(fu1DelayedBrakeFluidLevel==1)
		||(fu1LatchedBrakeFluidLevel==1)	/* 090901 for Bench Test */
      #endif
  	#else
		||(ESP_ERROR_FLG==1)||(fu1OnDiag==1)||(init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)
  	#endif
        )
    {
        BDW_inihibit_delay_cnt=0;
    }
    else if (BDW_inihibit_delay_cnt < L_U16_TIME_10MSLOOP_10S)
    {
        BDW_inihibit_delay_cnt++;
    }
    else { }

    if (BDW_inihibit_delay_cnt < L_U8_TIME_10MSLOOP_1000MS)
    {
        BDW_INIHIBIT=1;
    }
    else
    {
        BDW_INIHIBIT=0;
    }

    if (BDW_INIHIBIT==1)
    {
        BDW_ACT_CONDTION=0;
    }
    else { }

}
#endif


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDBDWCallDetection
	#include "Mdyn_autosar.h"
#endif
