/*******************************************************************************
* Project Name:     IDB
* File Name:        LCESPCalVlvOpenTiForComb.h
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/
#ifndef LCESPCALVLVOPENTIFORCOMB_H
#define LCESPCALVLVOPENTIFORCOMB_H

/* Includes ******************************************************************/
#include".\LVarHead.h"

/* Global Definition *********************************************************/
#define VALVE_OPEN_TIME_EXTENTION		(ENABLE) /* ENABLE DISABLE */

#define	U8_INTVAL_RISE_INITIALIZED		(0)
#define	U8_INTVAL_RISE_EXTENDED			(1)
#define	U8_INTVAL_RISE_EXIT				(2)

#define	U8_INTVAL_DUMP_INITIALIZED		(0)
#define	U8_INTVAL_DUMP_EXTENDED			(1)
#define	U8_INTVAL_DUMP_EXIT				(2)

//#define	S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_1		(MPRESS_2BAR)
//#define	S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_2		(MPRESS_10BAR)
//#define	S16_ESC_COMB_DIFF_PRESS_LEVEL_1			(MPRESS_2BAR)
//#define	S16_ESC_COMB_DIFF_PRESS_LEVEL_2			(MPRESS_10BAR)

#define	S16_DUMP_DEL_TP_LEVEL_1			(MPRESS_2BAR)
#define	S16_DUMP_DEL_TP_LEVEL_2			(MPRESS_7BAR)
#define	S16_DUMP_DEL_TP_LEVEL_3			(MPRESS_10BAR)
#define	S16_DUMP_DEL_TP_LEVEL_4			(MPRESS_15BAR)

#define	S16_DUMP_WHL_PRESS_LEVEL_1		(MPRESS_10BAR)
#define	S16_DUMP_WHL_PRESS_LEVEL_2		(MPRESS_30BAR)
#define	S16_DUMP_WHL_PRESS_LEVEL_3		(MPRESS_60BAR)

#define	S16_DUMP_TIME_1_1			(3)/*ms*/
#define	S16_DUMP_TIME_1_2			(3)/*ms*/
#define	S16_DUMP_TIME_1_3			(3)/*ms*/
#define	S16_DUMP_TIME_2_1			(25)/*ms*/
#define	S16_DUMP_TIME_2_2			(8)/*ms*/
#define	S16_DUMP_TIME_2_3			(3)/*ms*/
#define	S16_DUMP_TIME_3_1			(30)/*ms*/
#define	S16_DUMP_TIME_3_2			(15)/*ms*/
#define	S16_DUMP_TIME_3_3			(5)/*ms*/
#define	S16_DUMP_TIME_4_1			(40)/*ms*/
#define	S16_DUMP_TIME_4_2			(25)/*ms*/
#define	S16_DUMP_TIME_4_3			(10)/*ms*/



#define	S16_ESC_BBS_COMB_DEL_TP_LEVEL_1			(MPRESS_3BAR)
#define	S16_ESC_BBS_COMB_DEL_TP_LEVEL_2			(MPRESS_4BAR)
#define	S16_ESC_BBS_COMB_DEL_TP_LEVEL_3			(MPRESS_6BAR)
#define	S16_ESC_BBS_COMB_DEL_TP_LEVEL_4			(MPRESS_15BAR)

#define	U8_ESC_BBS_COMB_VALVE_OPEN_TIME_1		(30) /*ms*/
#define	U8_ESC_BBS_COMB_VALVE_OPEN_TIME_2		(30) /*ms*/
#define	U8_ESC_BBS_COMB_VALVE_OPEN_TIME_3		(30) /*ms*/
#define	U8_ESC_BBS_COMB_VALVE_OPEN_TIME_4		(40) /*ms*/

#define	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1	(MPRESS_10BAR)
#define	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2	(MPRESS_30BAR)
#define	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_3	(MPRESS_50BAR)

#define	S16_ESC_ABS_COMB_DIFFP_LEVEL_1			(MPRESS_10BAR)
#define	S16_ESC_ABS_COMB_DIFFP_LEVEL_2			(MPRESS_20BAR)
#define	S16_ESC_ABS_COMB_DIFFP_LEVEL_3			(MPRESS_30BAR)
#define	S16_ESC_ABS_COMB_DIFFP_LEVEL_4			(MPRESS_40BAR)
#define	S16_ESC_ABS_COMB_DIFFP_LEVEL_5			(MPRESS_50BAR)

#define	S16_ESC_ABS_COMB_DEL_TP_LEVEL_1			(MPRESS_1BAR)
#define	S16_ESC_ABS_COMB_DEL_TP_LEVEL_2			(MPRESS_3BAR)
#define	S16_ESC_ABS_COMB_DEL_TP_LEVEL_3			(MPRESS_5BAR)
#define	S16_ESC_ABS_COMB_DEL_TP_LEVEL_4			(MPRESS_8BAR)

/* U8_ESC_ABS_DelTP1_OPEN_TI_X_Y
 * X : Estimated Wheel Pressure level		1 = S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1,
 * 											2 = S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2
 * Y : Circuit-Wheel Pressure Difference	1 = S16_ESC_ABS_COMB_DIFFP_LEVEL_1,
 * 											2 = ...
 */
#define U8_ESC_ABS_DelTP1_OPEN_TI_1_1		(40)//(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_1_2		(30)//(8)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_1_3		(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_1_4		(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_1_5		(5)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_2_1		(40)//(9)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_2_2		(30)//(8)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_2_3		(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_2_4		(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_2_5		(5)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_3_1		(40)//(7)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_3_2		(30)//(6)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_3_3		(15)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_3_4		(10)/*ms*/
#define U8_ESC_ABS_DelTP1_OPEN_TI_3_5		(5)/*ms*/

#define U8_ESC_ABS_DelTP2_OPEN_TI_1_1		(40)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_1_2		(30)//(17)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_1_3		(17)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_1_4		(13)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_1_5		(5)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_2_1		(40)//(30)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_2_2		(30)//(15)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_2_3		(13)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_2_4		(10)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_2_5		(5)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_3_1		(30)//(15)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_3_2		(30)//(10)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_3_3		(10)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_3_4		(5)/*ms*/
#define U8_ESC_ABS_DelTP2_OPEN_TI_3_5		(5)/*ms*/

#define U8_ESC_ABS_DelTP3_OPEN_TI_1_1		(40)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_1_2		(30)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_1_3		(20)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_1_4		(13)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_1_5		(8)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_2_1		(40)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_2_2		(30)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_2_3		(15)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_2_4		(10)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_2_5		(8)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_3_1		(40)//(20)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_3_2		(30)//(15)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_3_3		(9)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_3_4		(7)/*ms*/
#define U8_ESC_ABS_DelTP3_OPEN_TI_3_5		(6)/*ms*/

#define U8_ESC_ABS_DelTP4_OPEN_TI_1_1		(40)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_1_2		(40)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_1_3		(30)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_1_4		(30)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_1_5		(30)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_2_1		(40)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_2_2		(40)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_2_3		(30)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_2_4		(25)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_2_5		(20)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_3_1		(40)//(15)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_3_2		(40)//(13)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_3_3		(25)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_3_4		(25)/*ms*/
#define U8_ESC_ABS_DelTP4_OPEN_TI_3_5		(20)/*ms*/

//
///* U8_ESC_COMB_VALVE_OPEN_TIME_ABS_X_Y
// * X : Estimated Wheel Pressure level		0 = Low, 1 = Med, 2 = High
// * Y : Circuit-Wheel Pressure Difference	0 = Low, 1 = Med, 2 = High
// */
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_0		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_2		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_0		(10) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_2		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_0		(10) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_2		(20) /*ms*/
//
//
///* U8_ESC_COMB_VALVE_OPEN_TIME_X_Y
// * X : Estimated Wheel Pressure level		0 = Low, 1 = Med, 2 = High
// * Y : Circuit-Wheel Pressure Difference	0 = Low, 1 = Med, 2 = High
// */
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_0_0		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_0_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_0_2		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_1_0		(10) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_1_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_1_2		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_2_0		(10) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_2_1		(20) /*ms*/
//#define	U8_ESC_COMB_VALVE_OPEN_TIME_2_2		(20) /*ms*/


/* Calibration Type Definition************************************************/

/* Global Extern Variable ****************************************************/

/* Global Function ***********************************************************/
extern void LCESP_vCalcExtdVlvRiseTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
extern void LCESP_vResetVlvRiseTi(struct ESC_STRUCT *ESC_WL);

extern void LCESP_vCalcExtdVlvDumpTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
extern void LCESP_vResetVlvDumpTi(struct ESC_STRUCT *ESC_WL);

#endif /* LAESPCALCOMBESCVALVEOPENTIME_H */
