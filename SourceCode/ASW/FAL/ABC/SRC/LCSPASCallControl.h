#ifndef __LCSPASCALLCONTROL_H__
#define __LCSPASCALLCONTROL_H__

/*includes********************************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition********************************************************/
  #if __SPAS_INTERFACE
#define     S8_SPAS_STANDBY              1
#define     S8_SPAS_RISE                 2
#define     S8_SPAS_HOLD                 3
#define     S8_SPAS_RELEASE              4
#define     S8_SPAS_INHIBITION           5

#define     S8_SPAS_BRK_REQUEST_READY    0
#define     S8_SPAS_BRK_REQUEST_INHIBIT  1
#define     S8_SPAS_BRK_REQUEST_ON		 2
  #endif

/*SPAS TEST MODE SETTING********************************************************/
#define  __SPAS_TEST_MODE               DISABLE
#define  SPAS_TEST_MODE_TARGET_P        MPRESS_150BAR
/*SPAS TEST MODE SETTING********************************************************/


/*Global Type Declaration ****************************************************************/
/*
#define S16_SPAS_FF_CTRL_TIME             L_U8_TIME_10MSLOOP_400MS
#define S16_SPAS_FF_CTRL_MAX_TIME         L_U8_TIME_10MSLOOP_500MS
#define S16_SPAS_FF_CTRL_MIN_SPD          VREF_3_KPH
#define U8_SPAS_RISE_TO_HOLD_DELAY_TIME   L_U8_TIME_10MSLOOP_50MS
#define S16_SPAS_BRK_CTRL_MAX_LIMIT       L_U16_TIME_10MSLOOP_10S
*/
/*Global Extern Variable  Declaration*****************************************************/
  #if __SPAS_INTERFACE
extern struct  U8_BIT_STRUCT_t SPAS00;
extern struct  U8_BIT_STRUCT_t SPAS01;

#define lcu1SpasActiveFlg		SPAS00.bit0
#define lcu1SpasInhibitFlg      SPAS00.bit1
#define lcu1SpasEntOk           SPAS00.bit2
#define lcu1SpasExitOkFlg       SPAS00.bit3
#define lcu1SpasRiseToHold		SPAS00.bit4
#define lcu1SpasHoldToRise		SPAS00.bit5
#define lcu1SpasFFControl		SPAS00.bit6
#define lcu1SpasFBControl		SPAS00.bit7

#define SPAS_TCL_DEMAND_fl		SPAS01.bit0
#define SPAS_TCL_DEMAND_fr		SPAS01.bit1
#define SPAS_S_VALVE_LEFT 		SPAS01.bit2
#define SPAS_S_VALVE_RIGHT		SPAS01.bit3
#define SPAS_MOTOR_ON		    SPAS01.bit4
#define SPAS_not_used_bit_01_5	SPAS01.bit5
#define SPAS_not_used_bit_01_6	SPAS01.bit6
#define SPAS_not_used_bit_01_7	SPAS01.bit7

/********** To */
#pragma DATA_SEG __GPAGE_SEG PAGED_RAM

extern int8_t        lcs8SpasState;

extern uint8_t       lcu8SpasCtrlReq;
extern uint8_t       lcu8SpasCompleteStopCnt;

extern int16_t      lcs16SpasCtrlError;
extern int16_t      lcs16SpasReqTargetP;

extern uint16_t lcu16SpasHoldActCnt, lcu16SpasReleaseActCnt, lcu16SpasRiseActCnt, lcu16SpasCtrlErrorCnt;


#pragma DATA_SEG DEFAULT

  #endif /* defined(__SPAS_INTERFACE)*/

/*Global Extern Functions  Declaration******************************************************/
  #if __SPAS_INTERFACE
extern void LCSPAS_vCallControl(void);
extern void LCSPAS_vResetCtrlVariable(void);
  #endif

#endif