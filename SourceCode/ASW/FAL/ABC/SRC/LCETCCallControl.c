/*******************************************************************/
/*  Program ID: MGH-40 ETC 						                   */
/*  Program description:. Engine Traction Control 		           */
/*  Input files: 												   */
/*  Output files:                                            	   */
/*  Special notes: none                                            */
/*******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCETCCallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************/

#include "LCETCCallControl.h"

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/

/* Local Function prototype ****************************************/
#if __ETC
void LCETC_vCallControl(void);                   /* ETCS main logic */
void LCETC_vCalculateVelocity(void);             /* 속도, 가속도, 슬립 계산 main logic */
#endif

/* Implementation***************************************************/
#if __ETC   /* TAG1 */
void LCETC_vCallControl(void)                    /* 7mSec마다 동작 */
{
	BTCS_ON = 0;
	if(FTCS_ON==1)
	{
		ETCS_ON = 1;
		FTCS_ON = 0;
	}
	else
	{
		;
	}

    ABS_ON  = ABS_fz;                   /* ABS flag */
		#if !SIM_MATLAB
    /*LATCS_vConvertEMS2TCS();*/
    	#endif
    LCTCS_vLoadParameter(); 					/* App.Field upgrade for multi-engine variables in 2003.12.08 */
    LCETC_vCalculateVelocity();
    LCTCS_vEstmateTMGear();
    LCTCS_vEstimateTargetTorque();
    LCTCS_vCallDetectTraction();
    LCTCS_vCallDetectTrace();
    LCTCS_vControlTraction();
	LCTCS_vControlTrace();
    LCTCS_vControlCommand();
		#if !SIM_MATLAB
    LATCS_vControlFuncLamp();
    LATCS_vConvertTCS2EMS();
    	#endif
}

void LCETC_vCalculateVelocity(void)
{
	LCTCS_vCalTractionWheelSpeed();
	LCTCS_vDetectSplitMu();
	LCTCS_vCalFilteredWheelSpeed();
	LCTCS_vCalculateDV();
	LCTCS_vCalculateACC();
#if !__4WD
	HIGH_HOLD=0;
#endif
}

int16_t INTER_2POINT( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2 )
{

    if( input_x <= x_1 )
    {
    	tempW9 = y_1;
    }
    else if ( input_x < x_2 )
    {
  /************************* CT 개선 전********** 2005.02.21  ******************************
        tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;
  *****************************************************************************************/
  /************************* CT 개선 후********** 2005.02.21  *****************************
        tempW0=(x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        else{}
        tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/tempW0);
  ****************************************************************************************/
    }
    else
    {
        tempW9 = y_2;
    }
    return tempW9;
}
#endif	/* TAG1 */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCETCCallControl
	#include "Mdyn_autosar.h"
#endif    