/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LSCallLogData.c
* Description:      Logging Data format Selection of Logic
* Logic version:    HV25
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C12         eslim           Initial Release
*  6113         eslim           ESP_EEC view adding, cycle time adding
*  6216         eslim           Modified based on MISRA rule
*  6A05         eslim           Modified to delete Warning Message
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSCallLogData
	#include "Mdyn_autosar.h"
#endif

/* Includes ********************************************************************/
#include "LAABSCallActCan.h"
#include "LAABSGenerateLFCDuty.h"
#include "LSCallLogData.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LSESPCalSensorOffset.h"
#include "LSESPFilterEspSensor.h"
#include "LDESPEstBeta.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "Hardware_Control.h"
#include "LCHSACallControl.h"
#include "LCHDCCallControl.h"
#include "LCDECCallControl.h"
#include "LCACCCallControl.h"
#include "LADECCallActHW.h"
#include "LCESPCallEngineControl.h"
#include "LCESPCallPressureControl.h"
#include "hm_logic_var.h"
#include "LCHBBCallControl.h"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LDABSDctRoadGradient.h"
#include "LDABSDctWheelStatus.h"
#include "LSMSCCallLogData.h"
#include "LDROPCallDetection.h"
#include "LCHBBCallControl.h"
#include "LCHRBCallControl.h"
#include "LCTCSCallControl.h"
#include "LDABSCallDctForVref.H"
#include "LSABSCallSensorSignal.H"
#include "LCESPCallControl.h"
#include "LDRTACallDetection.h"
#include "LDABSCallDctForVref.h"
#include "LCCBCCallControl.h"
#include "LCSLSCallControl.h"
#include "LAABSCallHWTest.h"
#include "LCESPInterfaceSlipController.h"
#include "LACallMain.h"
#include "LAESPCallActHW.h"
#include "LCEPBCallControl.h"
#include "LCAVHCallControl.h"
#include "LDABSDctVehicleStatus.h"
#include "LDESPDetectVehicleStatus.h"
#include "LSESPCalSlipRatio.h"
#include "LDABSCallEstVehDecel.H"
#include "LCETSCCallControl.h"
#include "LCEBDCallControl.h"
#include "LDTCSCallDetection.h"
#include "LSBTCSCallLogdata.h"
#include "LSCallMain.h"
#include "LCABSStrokeRecovery.h"
#include "LATCSCallActHW.h"
#include "LABTCSCallActHW.h"

#include "LCETSCCallControl.h"


#include "LDESPEstDiscTemp.h"
#if __VDC
#include "LDABSCallVrefCompForESP.H"
#include "LDESPCalVehicleModel.h"
#endif


#if __TSP
#include "LCTSPCallControl.h"
#include "LDTSPCallDetection.h"
#endif
#include "LCPBACallControl.h"

#if __SPAS_INTERFACE
#include "LASPASCallActHW.h"
#include "LCSPASCallControl.h"
#endif

#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#if defined(__UCC_1)
#include "UCC_Logic/LSCDCCallSensorSignal.h"
#include "UCC_Logic/LCCDCCallControl.h"
	#if defined(__AFS)
#include "../UCC_Logic/LSAFSCallLogData.h"
	#endif
#endif

#if __EVP || __VACUUM_PUMP_RELAY
#include "LCEVPCallControl.h"
#endif

#if __TOD
#include "LCTODCallControl.h"
#endif

#if defined(__dDCT_INTERFACE)
#include "LCHRCCallControl.h"
#endif

#if __SCC
#include "../SRC/SCC/LCSSTCallControl.h"
#endif
#include "LAEPCCallActHW.h"

#include "LCTCSCallBrakeControl.h"
#if (__IDB_LOGIC == ENABLE)
#include "LAESPCallTestHW.h"
#include "LCABSStrokeRecovery.h"

#endif

#include "ETCSGenCode/WhlSpinCoorr_struct_types.h" /* MBD ETCS */
#include "ETCSGenCode/LCETCS_vCallMain.h" /* MBD ETCS */

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/
uint16_t u16CycleTime;
uint8_t u8CycleTime;

	#if !defined(__AUTOSAR_CORE_ENA)
uint16_t curCycleTime;
	#endif


#if __ADVANCED_MSC
extern int16_t target_vol;
#endif

/* Local Function prototype ****************************************************/
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1

#if !__VDC
void    LS_vCallLogData(void);
void    LSABS_vCallABSLogData(void);
#else
uint8_t INHIBITION_INDICATOR(void);
void  CHECK_FLAG_INDICATOR(void);

void    LS_vCallLogData(void);

void    LSESP_vCallESPLogData(void);
void    LSESP_vCallABSLogData(void);
void    LSTCS_vCallTCSLogData(void);
#if	defined(BTCS_TCMF_CONTROL)	
void    LSTCS_vCallTCSLogDataGen2(void);
#endif
//void    LSTCS_vCallEECLogData(void);
void    LSESP_vCallROPLogData(void);
void    LSESP_vCallESPABSLogData(void);
void    LSESP_vCallIDBABSLogData(void);
#if __SPAS_INTERFACE
void    LSSPAS_vCallSPASBRAKELogData(void);
#endif
void    LSESP_vCallEECLogData(void);
void    LSESP_vCallTODLogData(void);
void    LSTCS_vCallCANCheckLogData(void);
void    LSESP_vCallESCPlusLogData(void);
#if (__IDB_LOGIC==ENABLE)
void 	LSESP_vCallESCPlusLogData_modd(void);
void	LSESP_vCallESCPlusLogData_DelTP(void);
void	LSESP_vCallESCPlusLogData_Fadeout(void);
#endif
void    LSESP_vCallEspApplLog(void);
#if __TCMF_LowVacuumBoost
void    LSHBB_vCallHBBLogData(void);
#endif
#if __EDC
void    LSEDC_vCallEDCLogData(void);
#endif
#if __CBC
void    LSESP_vCallCBCLogData(void);
#endif
#if defined(__EXTEND_LOGGER)
void 	LSESC_vCallTotalLogData(void);
#endif
void    LSTCS_vCallTMPLogData(void);

#if  (__Ax_OFFSET_MON_DRV==1)
void    LSESP_vCallAxOffsetLogData(void);
#endif

#if __SLS
void	LSESP_vCallSLSLogData(void);
#endif
#if __TSP
void	LSESP_vCallTSPLogData(void);
#endif
#if __TVBB
void    LSESP_vCallTVBBLogData(void);
#endif

#if(__ESC_TCMF_CONTROL ==1)
void LSESP_vCallESCTCMFLogData(void);
#endif

void    LSESP_vCallESCoffsetLogData(void);

#if  (__ADAPTIVE_MODEL_VCH_GEN==ENABLE) || (__ADAPTIVE_MODEL_VELO==ENABLE)
void    LSESP_vCallAdaptiveModelLogData(void);
#endif


#if (__HDC || __HSA)
void    LSHDCHSA_vCall_GSUV_LogData(void);
#endif
#if __HSA
void    LSHSA_vCall_LogData(void);
#endif
#if __ACC
void    LSACC_vCallAccLogData(void);
#endif
#if defined(__dDCT_INTERFACE)
void    LSHRC_vCall_LogData(void);
#endif
#if defined(__UCC_1)
void    LSUCC_vCallUCCLogData(void);
#endif

#if __EVP
void    LSEVP_vCallEVPLogData(void);
#endif
	
    #if __SCC
void	LSSCC_vCallSCCLogData(void);
	#endif
	
#endif
#endif
#endif

/* Global Function prototype ***************************************************/


/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LS_vCallLogData
* CALLED BY:            LS_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data Selection
********************************************************************************/
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1

#if !__VDC
void LS_vCallLogData(void)
{
  #ifdef __CYCLE_TIME_MEASURE
  	/*u16CycleTime = (uint16_t)(((uint32_t)wmu16CurCycleTime*10)/10);  resolution 1000 */    
    /*u8CycleTime =  (uint8_t)(u16CycleTime/100);  resolution 10 */    
    u16CycleTime = (uint16_t)(((uint32_t)wu16CurCycleTime*10)/10); /* resolution 1000 */
    u8CycleTime =  (uint8_t)(wu16CurCycleTime/100); /* resolution 10 */
  #endif   
  	
	LSABS_vCallABSLogData();
}

/*******************************************************************************
* FUNCTION NAME:        LSABS_vCallABSLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ABS
********************************************************************************/
void LSABS_vCallABSLogData(void)
{
    uint8_t i;
    	/* ID 0x111, 0x6DC */
        CAN_LOG_DATA[0] =(uint8_t) (system_loop_counter);
        CAN_LOG_DATA[1] =(uint8_t) (system_loop_counter>>8);               /* CH 01 : SYS_C */
        CAN_LOG_DATA[2] =(uint8_t) (vrad_fl);
        CAN_LOG_DATA[3] =(uint8_t) (vrad_fl>>8);                           /* CH 02 : vrdFL */
        CAN_LOG_DATA[4] =(uint8_t) (vrad_fr);
        CAN_LOG_DATA[5] =(uint8_t) (vrad_fr>>8);                           /* CH 03 : vrdFR */
        CAN_LOG_DATA[6] =(uint8_t) (vrad_rl);
        CAN_LOG_DATA[7] =(uint8_t) (vrad_rl>>8);                           /* CH 04 : vrdRL */
		/* ID 0x222, 0x6DD */
        CAN_LOG_DATA[8] =(uint8_t) (vrad_rr);
        CAN_LOG_DATA[9] =(uint8_t) (vrad_rr>>8);                           /* CH 05 : vrdRR */
        CAN_LOG_DATA[10] =(uint8_t) (vref);
        CAN_LOG_DATA[11] =(uint8_t) (vref>>8);                             /* CH 06 : vref  */
      #if __4WD || __AX_SENSOR
        CAN_LOG_DATA[12] =(uint8_t) (along);
        CAN_LOG_DATA[13] =(uint8_t) (along>>8);                            /* CH 07 : along */
      #else
        CAN_LOG_DATA[12] =(uint8_t) (ebd_filt_grv);
        CAN_LOG_DATA[13] =(uint8_t) (ebd_filt_grv>>8);                     /* CH 07 :  */
      #endif
        CAN_LOG_DATA[14] =(uint8_t) (fu16CalVoltMOTOR);
        CAN_LOG_DATA[15] =(uint8_t) (fu16CalVoltMOTOR>>8);                 /* CH 08 : mot_m */
		/* ID 0x333, 0x6DE */
        CAN_LOG_DATA[16] =(uint8_t) (target_vol);
        CAN_LOG_DATA[17] =(uint8_t) (target_vol>>8);                  	   /* CH 09 : TargetV */

      #if __SIDE_VREF
        CAN_LOG_DATA[18] =(uint8_t) (vref_L);
        CAN_LOG_DATA[19] =(uint8_t) (vref_L>>8);                           /* CH 10 : vrefL */
        CAN_LOG_DATA[20] =(uint8_t) (vref_R);
        CAN_LOG_DATA[21] =(uint8_t) (vref_R>>8);                           /* CH 11 : vrefR */
      #else
        CAN_LOG_DATA[18] =(uint8_t) (MSC_indicator);
        CAN_LOG_DATA[19] =(uint8_t) (MSC_indicator>>8);                    /* CH 10 :  */
        CAN_LOG_DATA[20] =(uint8_t) (t_afz);
        CAN_LOG_DATA[21] =(uint8_t) (t_afz>>8);                            /* CH 11 :  */
      #endif

      #if __ETC || __EDC
        CAN_LOG_DATA[22] =(uint8_t) (eng_rpm);
        CAN_LOG_DATA[23] =(uint8_t) (eng_rpm>>8);                          /* CH 12 :  */
      #else
        CAN_LOG_DATA[22] =(uint8_t) (v_afz);
        CAN_LOG_DATA[23] =(uint8_t) (v_afz>>8);                            /* CH 12 : v_afz */
      #endif
		/* ID 0x444, 0x6DF */
        CAN_LOG_DATA[24] =(uint8_t) (afz>127)?127:((afz<-127)?-127:(int8_t)afz);/* CH 13 : afz */
        CAN_LOG_DATA[25] =(uint8_t) (FL.LFC_zyklus_z);                     /* CH 14 : zycFL */
        CAN_LOG_DATA[26] =(uint8_t) (u8CycleTime);                         /* CH 15 : vrefR */
        CAN_LOG_DATA[27] = (uint8_t)(gma_flags);                           /* CH 16 : gmaFG */
        CAN_LOG_DATA[28] =(uint8_t) (FL.flags);                            /* CH 17 : flgFL */
        CAN_LOG_DATA[29] =(uint8_t) (FR.flags);                            /* CH 18 : flgFR */
        CAN_LOG_DATA[30] =(uint8_t) (RL.flags);                            /* CH 19 : flgRL */
        CAN_LOG_DATA[31] =(uint8_t) (RR.flags);                            /* CH 20 : flgRR */
		/* ID 0x555, 0x6E0 */
        CAN_LOG_DATA[32] =(uint8_t) (FL.rel_lam);                          /* CH 21 : relFL */
        CAN_LOG_DATA[33] =(uint8_t) (FR.rel_lam);                          /* CH 22 : relFR */
        CAN_LOG_DATA[34] =(uint8_t) (RL.rel_lam);                          /* CH 23 : relRL */
        CAN_LOG_DATA[35] =(uint8_t) (RR.rel_lam);                          /* CH 24 : relRR */
        CAN_LOG_DATA[36] =(uint8_t) (FL.pwm_duty_temp);                    /* CH 25 : FLDty */
        CAN_LOG_DATA[37] =(uint8_t) (FR.pwm_duty_temp);                    /* CH 26 : FRDty */
        CAN_LOG_DATA[38] =(uint8_t) (RL.pwm_duty_temp);                    /* CH 27 : RLDty */
        CAN_LOG_DATA[39] =(uint8_t) (RR.pwm_duty_temp);                    /* CH 28 : RRDty */
		/* ID 0x666, 0x6E1 */
        CAN_LOG_DATA[40] =(uint8_t) (FL.Rough_road_detector);              /* CH 29 : RRdFL */
        CAN_LOG_DATA[41] =(uint8_t) (FR.Rough_road_detector);              /* CH 30 : RRdFR */
        CAN_LOG_DATA[42] =(uint8_t) (RL.Rough_road_detector);              /* CH 31 : RRdRL */
        CAN_LOG_DATA[43] =(uint8_t) (RR.Rough_road_detector);              /* CH 32 : RRdRR */
        CAN_LOG_DATA[44] = (uint8_t)(FL.vfiltn);                    	   /* CH 33 : dVFL  */
        CAN_LOG_DATA[45] = (uint8_t)(FR.vfiltn);                    	   /* CH 34 : dVFR  */
        CAN_LOG_DATA[46] = (uint8_t)(RL.vfiltn);                    	   /* CH 35 : dVRL  */
        CAN_LOG_DATA[47] = (uint8_t)(RR.vfiltn);                    	   /* CH 36 : dVRR  */
		/* ID 0x117, 0x6E2 */
        CAN_LOG_DATA[48] =(uint8_t) (arad_fl);                             /* CH 37 : ardFL  */
        CAN_LOG_DATA[49] =(uint8_t) (arad_fr);                             /* CH 38 : ardFR  */
        CAN_LOG_DATA[50] =(uint8_t) (arad_rl);                             /* CH 39 : ardRL  */
        CAN_LOG_DATA[51] =(uint8_t) (arad_rr);                             /* CH 40 : ardRR  */

        CAN_LOG_DATA[52] =(uint8_t) (FL.High_dump_factor>12700)?127:
           ((FL.High_dump_factor<-12700)?-127:(int8_t)(FL.High_dump_factor/100)); /* CH 41 : hdfFL  */
        CAN_LOG_DATA[53] =(uint8_t) (FR.High_dump_factor>12700)?127:
           ((FR.High_dump_factor<-12700)?-127:(int8_t)(FR.High_dump_factor/100)); /* CH 42 : hdfFR  */
        CAN_LOG_DATA[54] =(uint8_t) (RL.High_dump_factor>12700)?127:
           ((RL.High_dump_factor<-12700)?-127:(int8_t)(RL.High_dump_factor/100)); /* CH 43 : hdfRL  */
        CAN_LOG_DATA[55] =(uint8_t) (RR.High_dump_factor>12700)?127:
           ((RR.High_dump_factor<-12700)?-127:(int8_t)(RR.High_dump_factor/100)); /* CH 44 : hdfRR  */
		/* ID 0x227, 0x6E3 */
        CAN_LOG_DATA[56] =(uint8_t) ((int8_t)FL.LFC_special_comp_duty);      /* CH 45 : spcFL  */
        CAN_LOG_DATA[57] =(uint8_t) ((int8_t)FR.LFC_special_comp_duty);      /* CH 46 : spcFR  */
        CAN_LOG_DATA[58] =(uint8_t) ((int8_t)RL.LFC_special_comp_duty);      /* CH 47 : spcRL  */
        CAN_LOG_DATA[59] =(uint8_t) ((int8_t)RR.LFC_special_comp_duty);      /* CH 48 : spcRR  */
        CAN_LOG_DATA[60] =(uint8_t) ((int8_t)FL.on_time_outlet_pwm);         /* CH 49 : outFL  */
        CAN_LOG_DATA[61] =(uint8_t) ((int8_t)RL.on_time_outlet_pwm);         /* CH 50 : outRL  */
/*
        CAN_LOG_DATA[60] =(uint8_t) ((int8_t)FL.LFC_Dump_Rise_factor);       //49
        CAN_LOG_DATA[61] =(uint8_t) ((int8_t)FR.LFC_Dump_Rise_factor);       //50
*/
      #if __ETC || __EDC
        CAN_LOG_DATA[62] =(uint8_t) (cal_torq/5);                            //51
        CAN_LOG_DATA[63] =(uint8_t) (drive_torq/5);                          //52
        CAN_LOG_DATA[64] =(uint8_t) (eng_torq/5);                            //53
        CAN_LOG_DATA[65] =(uint8_t) ((int8_t)mtp);                           //54
      #else/* ID 0x337, 0x6E4 */
        CAN_LOG_DATA[62] =(uint8_t) (FL.det_count);                          /* CH 51 : DetFL  */
        CAN_LOG_DATA[63] =(uint8_t) (FR.det_count);                          /* CH 52 : DetFR  */
        CAN_LOG_DATA[64] =(uint8_t) ((int8_t)FL.SPECIAL_1st_Pulsedown_need); /* CH 53 : 1stFL  */
        CAN_LOG_DATA[65] =(uint8_t) ((int8_t)RL.SPECIAL_1st_Pulsedown_need); /* CH 54 : 1stFR  */
      #endif

        CAN_LOG_DATA[66] =(uint8_t) (FL.spold);
        CAN_LOG_DATA[67] =(uint8_t) (FL.spold>>8);                           /* CH 55 : spdFL  */
        CAN_LOG_DATA[68] =(uint8_t) (FR.spold);
        CAN_LOG_DATA[69] =(uint8_t) (FR.spold>>8);                           /* CH 56 : spdFR  */

        i=0;
        if (ABS_fz                          ==1) i|=0x01;	/* Flag 01 : ABSfz */
        if ((lcabsu1BrakeByBLS||BLS)        ==1) i|=0x02;	/* Flag 02 : BLS   */
        if (AFZ_OK                          ==1) i|=0x04;	/* Flag 03 : AFZOK */	
        if (MSL_SPLIT_CLR                   ==1) i|=0x08;	/* Flag 04 : MSPcr */
        if (BEND_DETECT                     ==1) i|=0x10;	/* Flag 05 : BEND  */
        if (LFC_end_duty_OK_flag            ==1) i|=0x20;	/* Flag 06 : DtyOK */
        if (LFC_Split_flag                  ==1) i|=0x40;	/* Flag 07 : LFCsp */
        if (LFC_Split_suspect_flag          ==1) i|=0x80;	/* Flag 08 : SPsus */
        CAN_LOG_DATA[70] =(uint8_t) (i);	/* 57 */

        i=0;
        if (GMA_SLIP_fl                     ==1) i|=0x01;	/* Flag 09 : GMAfl */   
        if (GMA_SLIP_fr                     ==1) i|=0x02;   /* Flag 10 : GMAfr */   
        if (Rough_road_suspect_vehicle      ==1) i|=0x04;   /* Flag 11 : RRsus */	
        if (Rough_road_arad_OK              ==1) i|=0x08;   /* Flag 12 : RR_OK */   
        if (Rough_road_detect_vehicle_EBD   ==1) i|=0x10;   /* Flag 13 : RREBD */   
        if (Rough_road_detect_vehicle       ==1) i|=0x20;   /* Flag 14 : Rough */   
        if (MOT_ON_FLG                      ==1) i|=0x40;   /* Flag 15 : MOTON */   
        if (EBD_RA                          ==1) i|=0x80;   /* Flag 16 : EBDra */   
        CAN_LOG_DATA[71] =(uint8_t) (i);	/* 58 */
		/* ID 0x447, 0x6E5 */
        i = 0;
        if (ABS_fl                          ==1) i|=0x01;	/* Flag 17 : ABSfl */
        if (BUILT_fl                        ==1) i|=0x02;   /* Flag 18 : BUTfl */
        if (STABIL_fl                       ==1) i|=0x04;   /* Flag 19 : STBfl */
        if (AV_HOLD_fl                      ==1) i|=0x08;   /* Flag 20 : HLDfl */
        if (AV_DUMP_fl                      ==1) i|=0x10;   /* Flag 21 : DMPfl */
        if (FL.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;   /* Flag 22 : HOLfl */
        if (HV_VL_fl                        ==1) i|=0x40;   /* Flag 23 : HV_fl */
        if (AV_VL_fl                        ==1) i|=0x80;   /* Flag 24 : AV_fl */
        CAN_LOG_DATA[72] =(uint8_t) (i);	/* 59 */

        i=0;
        if (ABS_fr                          ==1) i|=0x01;	/* Flag 25 : ABSfr */
        if (BUILT_fr                        ==1) i|=0x02;   /* Flag 26 : BUTfr */
        if (STABIL_fr                       ==1) i|=0x04;   /* Flag 27 : STBfr */
        if (AV_HOLD_fr                      ==1) i|=0x08;   /* Flag 28 : HLDfr */
        if (AV_DUMP_fr                      ==1) i|=0x10;   /* Flag 29 : DMPfr */
        if (FR.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;   /* Flag 30 : HOLfr */
        if (HV_VL_fr                        ==1) i|=0x40;   /* Flag 31 : HV_fr */
        if (AV_VL_fr                        ==1) i|=0x80;   /* Flag 32 : AV_fr */
        CAN_LOG_DATA[73] =(uint8_t) (i);	/* 60 */

        i=0;
        if (ABS_rl                          ==1) i|=0x01;	/* Flag 33 : ABSrl */
        if (BUILT_rl                        ==1) i|=0x02;   /* Flag 34 : BUTrl */
        if (STABIL_rl                       ==1) i|=0x04;   /* Flag 35 : STBrl */
        if (SPLIT_JUMP_rl                   ==1) i|=0x08;   /* Flag 36 : JMPrl */
        if (SPLIT_PULSE_rl                  ==1) i|=0x10;   /* Flag 37 : PLSrl */
        if (RL.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;   /* Flag 38 : HOLrl */
        if (HV_VL_rl                        ==1) i|=0x40;   /* Flag 39 : HV_rl */
        if (AV_VL_rl                        ==1) i|=0x80;   /* Flag 40 : AV_rl */
        CAN_LOG_DATA[74] =(uint8_t) (i);	/* 61 */

        i = 0;
        if(ABS_rr                           ==1) i|=0x01;	/* Flag 41 : ABSrr */
        if(BUILT_rr                         ==1) i|=0x02;   /* Flag 42 : BUTrr */
        if(STABIL_rr                        ==1) i|=0x04;   /* Flag 43 : STBrr */
        if(SPLIT_JUMP_rr                    ==1) i|=0x08;   /* Flag 44 : JMPrr */
        if(SPLIT_PULSE_rr                   ==1) i|=0x10;   /* Flag 45 : PLSrr */
        if(RR.Hold_Duty_Comp_flag_1st       ==1) i|=0x20;   /* Flag 46 : HOLrr */
        if(HV_VL_rr                         ==1) i|=0x40;   /* Flag 47 : HV_rr */
        if(AV_VL_rr                         ==1) i|=0x80;   /* Flag 48 : AV_rr */
        CAN_LOG_DATA[75] =(uint8_t) (i);    /* 62 */

        i=0;

        if(FL.High_TO_Low_Suspect_flag  		==1) i|=0x01;		/* Flag 49 : H2Lfl */
/*        if(FL.LFC_rough_road_disable_comp_flag  ==1) i|=0x01;   */
        if(FL.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;		/* Flag 50 : IDSfl */
        if(FL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;		/* Flag 51 : BRDfl */
        if(FL.LFC_maintain_split_flag           ==1) i|=0x08;		/* Flag 52 : Mntfl */
        if(FL.In_Gear_flag                      ==1) i|=0x10;		/* Flag 53 : INGfl */
        if(FL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;		/* Flag 54 : NDPfl */
        if(FL.LFC_built_reapply_flag            ==1) i|=0x40;		/* Flag 55 : BRFfl */
        if(SPOLD_RESET_fl                       ==1) i|=0x80;		/* Flag 56 : SPRfl */

        CAN_LOG_DATA[76] =(uint8_t) (i);	/* 63 */

         i=0;

        if(FR.High_TO_Low_Suspect_flag  		==1) i|=0x01;		/* Flag 57 : H2Lfr */
/*        if(FR.LFC_rough_road_disable_comp_flag  ==1) i|=0x01;   */
        if(FR.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;       /* Flag 58 : IDSfr */
        if(FR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;       /* Flag 59 : BRDfr */
        if(FR.LFC_maintain_split_flag           ==1) i|=0x08;       /* Flag 60 : Mntfr */
        if(FR.In_Gear_flag                      ==1) i|=0x10;       /* Flag 61 : INGfr */
        if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;       /* Flag 62 : NDPfr */
        if(FR.LFC_built_reapply_flag            ==1) i|=0x40;       /* Flag 63 : BRFfr */
        if(SPOLD_RESET_fr                       ==1) i|=0x80;       /* Flag 64 : SPRfr */

        CAN_LOG_DATA[77] =(uint8_t) (i);	/* 64 */

        i=0;
        if(RL.Duty_Limitation_flag              ==1) i|=0x01;		/* Flag 65 : LIMrl */
        if(RL.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;       /* Flag 66 : IDSrl */
        if(RL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;       /* Flag 67 : BRDrl */
        if(RL.LFC_maintain_split_flag           ==1) i|=0x08;       /* Flag 68 : Mntrl */
        if(RL.MSL_BASE                          ==1) i|=0x10;       /* Flag 69 : MSLrl */
        if(RL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;       /* Flag 70 : NDPrl */
        if(RL.LFC_built_reapply_flag            ==1) i|=0x40;       /* Flag 71 : BRFrl */
        if(SPOLD_RESET_rl                       ==1) i|=0x80;       /* Flag 72 : SPRrl */

        CAN_LOG_DATA[78] =(uint8_t) (i);	/* 65 */

        i=0;
        if(RR.Duty_Limitation_flag              ==1) i|=0x01;		/* Flag 73 : LIMrr */
        if(RR.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;       /* Flag 74 : IDSrr */
        if(RR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;       /* Flag 75 : BRDrr */
        if(RR.LFC_maintain_split_flag           ==1) i|=0x08;       /* Flag 76 : Mntrr */
        if(RR.MSL_BASE                          ==1) i|=0x10;       /* Flag 77 : MSLrr */
        if(RR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;       /* Flag 78 : NDPrr */
        if(RR.LFC_built_reapply_flag            ==1) i|=0x40;       /* Flag 79 : BRFrr */
        if(SPOLD_RESET_rr                       ==1) i|=0x80;       /* Flag 80 : SPRrr */

        CAN_LOG_DATA[79] =(uint8_t) (i);	/* 66 */

}
#else /* #if !__VDC */

/*******************************************************************************
* FUNCTION NAME:        INHIBITION_INDICATOR
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Inhibition indicator of ESP
********************************************************************************/
uint8_t INHIBITION_INDICATOR(void)
{
    if(ABS_PERMIT_BY_BRAKE==1) return 14;
    else if(VDC_LOW_SPEED==1) return 1;
    else if(BACK_DIR==1) return 2;
    else if(!WSTR_360_CHECK==1) return 3;
    else if(ESP_SPLIT==1) return 4;
    else if(WSTR_360_SUSPECT_FLG==1) return 5;
    else if(KYAW.FAILSAFE_SUSPECT_FLAG==1) return 6;
    else if((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==0)) return 7;
    else if(KYAW.SUSPECT_FLG==1) return 8;
    else if(ABS_PERMIT_BY_BRAKE==1) return 9;
    else if(PHZ_detected_flg2==0) return 10;
    else if(ESP_ERROR_FLG==1) return 11;
    else if(fou1SenPwr1secOk==0) return 12;
    else if(KSTEER.SUSPECT_FLG==1) return 13;
    else if(fu1VoltageUnderErrDet==1) return 15;
    else if(FLAG_INHIBIT_OFST_TURN==1) return 51;
    else if(fu1ESCDisabledBySW==1) return 71;
    else if(vdc_on_flg==0) return 72;
    else if(FLAG_BANK_DETECTED==1) return 73;
    else if(FLAG_BANK_SUSPECT==1) return 74;
    else if(PARTIAL_PERFORM==1) return 75;
    else if(SAS_CHECK_OK==0) return 76;

 /**********************************
 * partial perform only
 **********************************/
    else if(VDC_UNDER_YAW_ERR==1) return 101;
    else if(VDC_UNDER_BLS_ERR==1) return 102;
    else if(VDC_UNDER_SWITCH_ERR==1) return 103;
    else if(VDC_UNDER_CAN_ERR==1) return 104;
    else if(VDC_UNDER_LAT_ERR==1) return 105;
    else if(VDC_UNDER_STR_ERR==1) return 106;
    else if(ROUGH_ROAD_FOR_YAW==1) return 107;
    else if(stable_check_cnt >0) return 108;
    else return 0;

}

/*******************************************************************************
* FUNCTION NAME:        CHECK_FLAG_INDICATOR
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Checking Flag of ESP
********************************************************************************/
void  CHECK_FLAG_INDICATOR(void)
{
 /*************************************
 * partial : SUSPECT !
 *************************************/
    Check_Flag_Suspect = 0;
    if  ( PARTIAL_PERFORM ==1 ) Check_Flag_Suspect = Check_Flag_Suspect + 1 ;       // + 1
    if  ( PRE_ESP_CONTROL ==1 ) Check_Flag_Suspect = Check_Flag_Suspect + 2 ;       // + 2
    if  ( SAS_CHECK_OK ==0 )   Check_Flag_Suspect = Check_Flag_Suspect + 5 ;
    if  ( WSTR_360_JUMP ==1)   Check_Flag_Suspect = Check_Flag_Suspect + 10 ;

}

/*******************************************************************************
* FUNCTION NAME:        LS_vCallLogData
* CALLED BY:            LS_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data Selection
********************************************************************************/
void LS_vCallLogData(void)
{
  #ifdef __CYCLE_TIME_MEASURE
  	/*u16CycleTime = (uint16_t)(((uint32_t)wmu16CurCycleTime*10)/10); /* resolution 1000 */    
    /*u8CycleTime =  (uint8_t)(u16CycleTime/100);  resolution 10 */    
    u16CycleTime = (uint16_t)(((uint32_t)wu16CurCycleTime*10)/10); /* resolution 1000 */
    u8CycleTime =  (uint8_t)(wu16CurCycleTime/100); /* resolution 10 */
  #endif   

/* U8_ESP_Output_Selection_Mode assignment **************************************
*
*	ESP 		: 0(default)
*	ABS 		: 1
*	TCS 		: 2
*	EEC 		: 3
*	ABS+ESP 	: 4
*	ESP Engine 	: 5
*	ESP TOD 	: 6
* 	ESCplus 	: 7
*	BAS		 	: 8
*	---			: 9
*
*   ESC/TCS/ABS: 12 - 160byte logging by Extended CAN
*	ESP		: 10~14,	ABS		: 15~19,
*	TCS		: 20~24,	EBD		: 25~29,
*	Beta	: 30~34,	Speed	: 35~39,
*	Offset	: 40~44,	ROP		: 45~49,
*   TSP		: 50~54,	HSA		: 55~59,
*   HDC		: 60~64,	PBA		: 65~69,
*   FBC		: 70~74,	BDW		: 75~79,
*   EBP		: 80~84,	EDC		: 85~89,
*   ETC		: 90~94,	기타	: 95~99,
*   Application	: 100~199,
*	reservation	: 200~255
*/

	switch( U8_ESP_Output_Selection_Mode )
    {
        case 0:
            LSESP_vCallESPLogData();
        break;

        case 1:
            LSESP_vCallABSLogData();
        break;

        case 2:
            LSTCS_vCallTCSLogData();
        break;

        case 3:
            //LSTCS_vCallEECLogData();
        break;

        case 4:
            /*LSESP_vCallESPABSLogData();*/
            LSESP_vCallIDBABSLogData();
        break;

        case 5:
            LSESP_vCallEECLogData();
        break;

        case 6:
            LSESP_vCallTODLogData();
        break;

        case 7:
            LSESP_vCallESCPlusLogData();
        break;
#if (__IDB_LOGIC==ENABLE)
		case 8:
/*			LSESP_vCallESCPlusLogData_modd();*/
/*			LSESP_vCallESCPlusLogData_DelTP();*/
			LSESP_vCallESCPlusLogData_Fadeout();
		break;
#endif
		
        case 10:
            LSTCS_vCallCANCheckLogData();
        break;
        
      #if(__ESC_TCMF_CONTROL ==1)
        case 11:
            LSESP_vCallESCTCMFLogData();
        break;
      #endif
        
      #if defined(__EXTEND_LOGGER)
        case 12:
            LSESC_vCallTotalLogData();
        break;
      #endif/*#if defined(__EXTEND_LOGGER)*/
        
  
  #if __HDC || __HSA
        case 19:
            LSHDCHSA_vCall_GSUV_LogData();	// GSUV_OEM ESC_view    
        break; 
  #endif
  
        case 20:
            LSESP_vCallESCoffsetLogData();
        break; 

#if  (__Ax_OFFSET_MON_DRV==1)
        case 21:
        		LSESP_vCallAxOffsetLogData();
        break;
#endif

#if  (__ADAPTIVE_MODEL_VCH_GEN==ENABLE) || (__ADAPTIVE_MODEL_VELO==ENABLE)
        case 22:
            LSESP_vCallAdaptiveModelLogData();
        break;  
#endif   

        case 23:
        	  LSTCS_vCallTMPLogData();        // Disc Temperature Logdata
        break;
        	  
        case 30:
            LSESP_vCallBetaLogData();
        break;

#if __CBC
        case 33:
            LSESP_vCallCBCLogData();
        break;
#endif

#if __SLS
        case 34:
            LSESP_vCallSLSLogData();
        break;
#endif

        case 35:
            LSABS_vCallVrefLogData();
        break;
        
#if	__RTA_ENABLE        
        case 36:
            LSRTA_vCallRTALogData();
        break;
#endif

#if __ROP
        case 45:
            LSESP_vCallROPLogData();
        break;
#endif

#if __HSA
        case 55:
            LSHSA_vCall_LogData();
        break; 
#endif

#if defined(__dDCT_INTERFACE)
        case 56:
            LSHRC_vCall_LogData();
        break; 
#endif

#if __HDC || __DEC
        case 60:
            LSHDC_vCallHDCLogData();
        break;
#endif

#if __AVH
        case 61:
            LSAVH_vCallAVHLogData();
        break;
#endif

#if __SPAS_INTERFACE
        case 62:
            LSSPAS_vCallSPASBRAKELogData();
        break; 
#endif

#if __ACC
        case 63:
            LSACC_vCallAccLogData();
        break; 
#endif

#if __TCMF_LowVacuumBoost
        case 69:
            LSHBB_vCallHBBLogData();
        break;
#endif

#if __FBC
        case 70:
            LSFBC_vCallFBCLogData();
        break;
#endif

#if __HRB
        case 71:
            LSESP_vCallESPABSLogData(); /* Brake Assist & ABS log data*/
        break;
#endif

#if __TSP
        case 75:
            LSESP_vCallTSPLogData();
        break;
#endif

#if __TVBB
        case 76:
            LSESP_vCallTVBBLogData();
        break;
#endif

#if __EDC
        case 85:
            LSEDC_vCallEDCLogData();
        break;
#endif

#if __EVP
        case 95:
            LSEVP_vCallEVPLogData();
        break;
#endif

#if __ADVANCED_MSC
        case 99:
            LSMSC_vCallMSCLogData();
        break;
#endif
        case 101:
            LSESP_vCallEspApplLog();
        break;

#if (__MGH80_MOCi == ENABLE) && (__ECU_TYPE_2 == __INTEGRATED)
        case 111:
			L_vCallLogData();
        break;
#endif	

#if defined(__UCC_1)

        case 200:
            LSUCC_vCallUCCLogData();
        break;

        #if defined(__IDC_System)
            case 203:
                LSUCC_vCallIDCLogData();
        break;
#endif

    #if defined(__AFS)

        case 202:
            LSUCC_vCallAFSLogData();
        break;
    #endif
#endif

#if __VSM
        case 201:
            LSUCC_vCallEPSLogData();
        break;
#endif
	
	#if __SCC
	    case 215:
	    LSSCC_vCallSCCLogData();
		break;
	#endif

    case 222:
			#if	defined(BTCS_TCMF_CONTROL)	
			LSTCS_vCallBTCS_Gen2_Val_Log(); 
			//LSTCS_vCallTCSLogDataGen2();
			#endif
        break;

	#if (__IDB_LOGIC==ENABLE)
	    case 255:
	    //LSIDB_vCallIDBLogData();
		break;
	#endif

		/* MBD ETCS */
		case 223:
		MBDedETCS_Log();
		break;

        default:
            LSESP_vCallESPLogData();
        break;
    }
	#ifdef __EXTEND_LOGGER
		#if (__IDB_LOGIC==ENABLE)
			//LSIDB_vCallIDB160ByteLogData();
		#endif
	#endif
	

}

/*******************************************************************************
* FUNCTION NAME:        LSESC_vCallTotalLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          160 byte Total Logging Data of ESC system
********************************************************************************/
#if defined(__EXTEND_LOGGER)
void LSESC_vCallTotalLogData(void)
{

    uint8_t i;

        CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);
        CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);               	//1 (same channel as ESP log data)
        CAN_LOG_DATA[2]  =(uint8_t) (vrad_crt_fl);
        CAN_LOG_DATA[3]  =(uint8_t) (vrad_crt_fl>>8);                           //2 (same channel as ESP log data)
        CAN_LOG_DATA[4]  =(uint8_t) (vrad_crt_fr);
        CAN_LOG_DATA[5]  =(uint8_t) (vrad_crt_fr>>8);                           //3 (same channel as ESP log data)
        CAN_LOG_DATA[6]  =(uint8_t) (vrad_crt_rl);
        CAN_LOG_DATA[7]  =(uint8_t) (vrad_crt_rl>>8);                           //4 (same channel as ESP log data)0x111
                         
        CAN_LOG_DATA[8]  =(uint8_t) (vrad_crt_rr);
        CAN_LOG_DATA[9]  =(uint8_t) (vrad_crt_rr>>8);                           //5 (same channel as ESP log data)
	if(ABS_fz==1)
	{
        CAN_LOG_DATA[10] =(uint8_t) (vref);
        CAN_LOG_DATA[11] =(uint8_t) (vref>>8);                             		//6 (same channel as ESP log data)
    }
    else
    {
    	CAN_LOG_DATA[10] =(uint8_t) (vref5);
        CAN_LOG_DATA[11] =(uint8_t) (vref5>>8);                             	//6 (same channel as ESP log data)
    }    
        CAN_LOG_DATA[12] =(uint8_t) (along);
        CAN_LOG_DATA[13] =(uint8_t) (along>>8);                           		//7  (same channel as ESP log data)
      
        CAN_LOG_DATA[14] =(uint8_t) (fu16CalVoltIntMOTOR);    			
        CAN_LOG_DATA[15] =(uint8_t) (fu16CalVoltIntMOTOR>>8); 			   		 //8 0x222
                                                                           		
        CAN_LOG_DATA[16] =(uint8_t) (wstr);                                		
        CAN_LOG_DATA[17] =(uint8_t) (wstr>>8);                             		//9
        CAN_LOG_DATA[18] =(uint8_t) (alat);                                		
        CAN_LOG_DATA[19] =(uint8_t) (alat>>8);                             		//10
        CAN_LOG_DATA[20] =(uint8_t) (yaw_out);                             		
        CAN_LOG_DATA[21] =(uint8_t) (yaw_out>>8);                          		//11
        CAN_LOG_DATA[22] =(uint8_t) (mpress);                              		
        CAN_LOG_DATA[23] =(uint8_t) (mpress>>8);                           		//12 0x333

        CAN_LOG_DATA[24] =(uint8_t) ((afz>127)?127:((afz<-127)?-127:(int8_t)afz));  //13
        CAN_LOG_DATA[25] =(uint8_t) (target_vol/1000);                       //14
      #if __ESC_MOTOR_PWM_CONTROL
        CAN_LOG_DATA[26] =(uint8_t) (lau8MscDuty); //15 FL.LFC_zyklus_z
      #else
        CAN_LOG_DATA[26] =(uint8_t) (FL.LFC_zyklus_z); //15 FL.LFC_zyklus_z
      #endif
        CAN_LOG_DATA[27] =(uint8_t) (gma_flags);
		if(ABS_fz==0)
        {
            if(vref<=VREF_0_25_KPH)
            {
              #if __HSA  
                CAN_LOG_DATA[27] =(uint8_t) (HSA_gs_sel);                    //16
              #else
                CAN_LOG_DATA[27] =(uint8_t) (gs_sel);                    //16
              #endif  
            }
            else
            {
                CAN_LOG_DATA[27] =(uint8_t) (gear_pos);                    //16
            }
        }

      #if __HW_TEST_MODE
        CAN_LOG_DATA[26] =(uint8_t) (target_vol/1000);                    //15
        CAN_LOG_DATA[27] =(uint8_t) (las8HwTestMode);                    //16
      #endif  

        CAN_LOG_DATA[28] =(uint8_t) (FL.flags);                          //17
        CAN_LOG_DATA[29] =(uint8_t) (FR.flags);                        	//18
        CAN_LOG_DATA[30] =(uint8_t) (RL.flags);                            //19
        CAN_LOG_DATA[31] =(uint8_t) (RR.flags);                            //20 0x444

        CAN_LOG_DATA[32] =(uint8_t) (FL.rel_lam);                          //21
        CAN_LOG_DATA[33] =(uint8_t) (FR.rel_lam);                          //22
        CAN_LOG_DATA[34] =(uint8_t) (RL.rel_lam);                          //23
        CAN_LOG_DATA[35] =(uint8_t) (RR.rel_lam);                          //24

        CAN_LOG_DATA[36] =(uint8_t) (FL.pwm_duty_temp);                    //25
        CAN_LOG_DATA[37] =(uint8_t) (FR.pwm_duty_temp);                    //26
        CAN_LOG_DATA[38] =(uint8_t) (RL.pwm_duty_temp);                    //27
        CAN_LOG_DATA[39] =(uint8_t) (RR.pwm_duty_temp);                    //28 0x555

      	  #if __MP_COMP
	    if(lcabsu1ValidMpress==1)
	    {
	        CAN_LOG_DATA[40] =(uint8_t) ((int8_t)FL.LFC_MP_Cyclic_comp_duty);    //29
	        CAN_LOG_DATA[41] =(uint8_t) ((int8_t)FR.LFC_MP_Cyclic_comp_duty);    //30
	        CAN_LOG_DATA[42] =(uint8_t) ((int8_t)RL.LFC_MP_Cyclic_comp_duty);    //31
	        CAN_LOG_DATA[43] =(uint8_t) ((int8_t)RR.LFC_MP_Cyclic_comp_duty);    //32
	    }
	    else
	      #endif
	    {
	        CAN_LOG_DATA[40] = 255;
	        CAN_LOG_DATA[41] = 255;
	        CAN_LOG_DATA[42] = 255;
	        CAN_LOG_DATA[43] = 255;
	    }

        CAN_LOG_DATA[44] =(uint8_t) (FL.vfiltn);                    //33
        CAN_LOG_DATA[45] =(uint8_t) (RL.vfiltn);                    //34
        CAN_LOG_DATA[46] =(uint8_t) ((FL.High_dump_factor>12700)?127:
           ((FL.High_dump_factor<-12700)?-127:(int8_t)(FL.High_dump_factor/100)));  //35
        CAN_LOG_DATA[47] =(uint8_t) ((FR.High_dump_factor>12700)?127:
           ((FR.High_dump_factor<-12700)?-127:(int8_t)(FR.High_dump_factor/100)));  //36 0x666
           	
		CAN_LOG_DATA[48] =(uint8_t) ((RL.High_dump_factor>12700)?127:
           ((RL.High_dump_factor<-12700)?-127:(int8_t)(RL.High_dump_factor/100)));  //35
        CAN_LOG_DATA[49] =(uint8_t) ((RR.High_dump_factor>12700)?127:
           ((RR.High_dump_factor<-12700)?-127:(int8_t)(RR.High_dump_factor/100)));  //36           	
        CAN_LOG_DATA[50] =(uint8_t) (FL.arad);                             //37
        CAN_LOG_DATA[51] =(uint8_t) (RL.arad);                             //38

        //CAN_LOG_DATA[50] =(uint8_t) ((int8_t)FL.SPECIAL_1st_Pulsedown_need); //39
        //CAN_LOG_DATA[51] =(uint8_t) ((int8_t)RL.SPECIAL_1st_Pulsedown_need); //40
        
        CAN_LOG_DATA[52] =(uint8_t) ((int8_t)FL.LFC_Conven_Reapply_Time); //39
        CAN_LOG_DATA[53] =(uint8_t) ((int8_t)FR.LFC_Conven_Reapply_Time); //40

    if((FL.FSF==0) || (FL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FL.GMA_SLIP==1)) 
      {
        CAN_LOG_DATA[52] =(uint8_t) ((FL.lcabss16YawDumpFactor>6350)?127:
                         ((FL.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FL.lcabss16YawDumpFactor/50))); // ((int8_t)FL.SPECIAL_1st_Pulsedown_need); //39
      }
    if((RL.FSF==0) || (RL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FR.GMA_SLIP==1))
      {
        CAN_LOG_DATA[53] =(uint8_t) ((FR.lcabss16YawDumpFactor>6350)?127:
                         ((FR.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FR.lcabss16YawDumpFactor/50))); // ((int8_t)RL.SPECIAL_1st_Pulsedown_need); //40
      }  
   
#if __PRESS_EST_ACTIVE
        CAN_LOG_DATA[54] = (uint8_t)(FL.s16_Estimated_Active_Press/10);      //41 
        CAN_LOG_DATA[55] = (uint8_t)(FR.s16_Estimated_Active_Press/10);      //42 0x117
        
        CAN_LOG_DATA[56] = (uint8_t)(RL.s16_Estimated_Active_Press/10);      //43 
        CAN_LOG_DATA[57] = (uint8_t)(RR.s16_Estimated_Active_Press/10);      //44 
#endif
        CAN_LOG_DATA[58] = (uint8_t)((int8_t)FL.LFC_special_comp_duty);     //45
        CAN_LOG_DATA[59] = (uint8_t)((int8_t)FR.LFC_special_comp_duty);		//46
        
        CAN_LOG_DATA[60] = (uint8_t)((int8_t)RL.LFC_special_comp_duty);     //47
        CAN_LOG_DATA[61] = (uint8_t)((int8_t)RR.LFC_special_comp_duty);     //48
       /* CAN_LOG_DATA[56] = (uint8_t)(fsu8NumberOfEdge_fl);     //45
        CAN_LOG_DATA[57] = (uint8_t)(fsu8NumberOfEdge_fr);
        CAN_LOG_DATA[58] = (uint8_t)(fsu8NumberOfEdge_rl);     //47
        CAN_LOG_DATA[59] = (uint8_t)(fsu8NumberOfEdge_rr);     //48*/
        CAN_LOG_DATA[62] = (uint8_t)(((sign_o_delta_yaw_thres/10)>127)?127:(((sign_o_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_o_delta_yaw_thres/10))); //49
        CAN_LOG_DATA[63] = (uint8_t)(((sign_u_delta_yaw_thres/10)>127)?127:(((sign_u_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_u_delta_yaw_thres/10))); //50 0x227

        CAN_LOG_DATA[64] = (uint8_t)(FL.on_time_outlet_pwm); //51
        CAN_LOG_DATA[65] = (uint8_t)(RL.on_time_outlet_pwm); //52
#if __TCMF_CONTROL
  #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)
  #else
    if(TCL_DEMAND_fr==1)
  #endif
    {
        CAN_LOG_DATA[64] = (uint8_t)(MFC_PWM_DUTY_P); //51
    }
  #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_SECONDARY==1)
  #else
    if(TCL_DEMAND_fl==1)
  #endif
    {
        CAN_LOG_DATA[65] = (uint8_t)(MFC_PWM_DUTY_S); //52
    }
#endif

        CAN_LOG_DATA[66] = (uint8_t)(wu16CurCycleTime/50);   //53
        CAN_LOG_DATA[67] = (uint8_t)(esp_mu/10);             //54 
        
            /*
        CAN_LOG_DATA[64] = (uint8_t)((q_front<-120)?-120:(int8_t)q_front);   //53
        CAN_LOG_DATA[65] = (uint8_t)((q_rear<-120)?-120:(int8_t)q_rear);     //54
            */

        CAN_LOG_DATA[68] = (uint8_t)(rf2);
        CAN_LOG_DATA[69] = (uint8_t)(rf2>>8);                              //55
        CAN_LOG_DATA[70] = (uint8_t)(delta_moment_q);
        CAN_LOG_DATA[71] = (uint8_t)(delta_moment_q>>8);                //56 
        
 /* #if __EST_MU_FROM_SKID
        tempW7 = 0;
        if     (FL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1000;}
        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1000;}
        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1000;}
        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1000;}
        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1000;}
        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1000;}
        else                                                             {tempW7 = tempW7 + 9*1000;}
        if     (FR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*100;}
        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*100;}
        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*100;}
        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*100;}
        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*100;}
        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*100;}
        else                                                             {tempW7 = tempW7 + 9*100;}
        if     (RL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*10;}
        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*10;}
        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*10;}
        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*10;}
        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*10;}
        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*10;}
        else                                                             {tempW7 = tempW7 + 9*10;}
        if     (RR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1;}
        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1;}
        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1;}
        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1;}
        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1;}
        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1;}
        else                                                             {tempW7 = tempW7 + 9*1;}
        
        CAN_LOG_DATA[68] = (uint8_t)(tempW7);
        CAN_LOG_DATA[69] = (uint8_t)(tempW7>>8);                //56
  #endif  */     
  
      #if __HW_TEST_MODE
        CAN_LOG_DATA[70] = (uint8_t)(HW_test_cnt_t);
        CAN_LOG_DATA[71] = (uint8_t)(HW_test_cnt_t>>8);                //56  0x337
      #endif  
		
/**********TCS****************************************/		
	if 		(etcs_on_count==1)
	{
        CAN_LOG_DATA[72] =(uint8_t) (ltcss16ETCSEnterExitFlags);		 // 57
        CAN_LOG_DATA[73] =(uint8_t) (ltcss16ETCSEnterExitFlags>>8);
    }
    else if	(ETCS_ON==1)
    {
        CAN_LOG_DATA[72] =(uint8_t) (ltcss16BaseTorqueLevel);			// 57
        CAN_LOG_DATA[73] =(uint8_t) (ltcss16BaseTorqueLevel>>8);
    }
	else
	{
        CAN_LOG_DATA[72] =(uint8_t) (lctcss16MinimumTorqLevel);		// 57
        CAN_LOG_DATA[73] =(uint8_t) (lctcss16MinimumTorqLevel>>8);
    }

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = esp_cmd;}
        else                                { tcs_tempW0 = (INT)ENG_VAR;}
        CAN_LOG_DATA[74] =(uint8_t) (tcs_tempW0);                 // CH8
        CAN_LOG_DATA[75] =(uint8_t) (tcs_tempW0>>8);

        CAN_LOG_DATA[76] =(uint8_t) (lts16WheelSpin);             // CH9
        CAN_LOG_DATA[77] =(uint8_t) (lts16WheelSpin>>8);
        CAN_LOG_DATA[78] =(uint8_t) (lts16WheelSpinErrorDiff);    // CH10
        CAN_LOG_DATA[79] =(uint8_t) (lts16WheelSpinErrorDiff>>8);	///0x447
                
	   	CAN_LOG_DATA[80] =(uint8_t) (nosign_delta_yaw_first);                     //ch33
	   	CAN_LOG_DATA[81] =(uint8_t) (nosign_delta_yaw_first>>8);        
	   	
    #if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==ENABLE)) && (__AX_SENSOR==1)
        CAN_LOG_DATA[82] = (uint8_t)(eng_torq_rel);          // CH14
        CAN_LOG_DATA[83] = (uint8_t)(eng_torq_rel>>8);    
	if ( (ETCS_ON==1) || (FTCS_ON==1) )
	{
        CAN_LOG_DATA[84] =(uint8_t) (lsu8DrvMode);                // CH15
        CAN_LOG_DATA[85] =(uint8_t) (lsu8DrvMode>>8);
    }
    else
    {
        CAN_LOG_DATA[84] =(uint8_t) (ltcss16MiniTireTendencyCnt); // CH15
        CAN_LOG_DATA[85] =(uint8_t) (ltcss16MiniTireTendencyCnt>>8);
    }
    #else
        CAN_LOG_DATA[82] =(uint8_t) (eng_torq_rel);          // CH14
        CAN_LOG_DATA[83] =(uint8_t) (eng_torq_rel>>8);
        CAN_LOG_DATA[84] =(uint8_t) (ltcss16MiniTireTendencyCnt); // CH15
        CAN_LOG_DATA[85] =(uint8_t) (ltcss16MiniTireTendencyCnt>>8);
    #endif   
         
        CAN_LOG_DATA[86] =(uint8_t) (acc_r);                  // CH16
        CAN_LOG_DATA[87] =(uint8_t) (acc_r>>8);  						//0x118
        
        CAN_LOG_DATA[88] =(uint8_t) (drive_torq);             // CH17
        CAN_LOG_DATA[89] =(uint8_t) (drive_torq>>8);
        CAN_LOG_DATA[90] =(uint8_t) (cal_torq);                   // CH18
        CAN_LOG_DATA[91] =(uint8_t) (cal_torq>>8);
        CAN_LOG_DATA[92] =(uint8_t) (eng_torq);                   // CH19
        CAN_LOG_DATA[93] =(uint8_t) (eng_torq>>8);
        CAN_LOG_DATA[94] =(uint8_t) (eng_rpm);                    // CH20
        CAN_LOG_DATA[95] =(uint8_t) (eng_rpm>>8);								//0x228
        											
        CAN_LOG_DATA[96] =(uint8_t) (gear_pos);               // CH21
        CAN_LOG_DATA[97] =(uint8_t) (mtp);                // CH22

      #if __REAR_D        
        if ( (BTCS_rl==1) || (BTCS_rr==1) )
        {tcs_tempW0 = LCTCS_s16IFindMaximum(rise_threshold_rl, rise_threshold_rr);}
        else if (DETECT_BTC_rl==0)
        {tcs_tempW0 = LCTCS_s16ILimitRange(arad_rl,0,255);}
        else    {tcs_tempW0 = LCTCS_s16ILimitRange(b_rad_rl,0,255);}
        CAN_LOG_DATA[98] = (uint8_t)(tcs_tempW0);     // CH25

        if ( (BTCS_rl==1) || (BTCS_rr==1) )
        {tcs_tempW0 = LCTCS_s16IFindMaximum(dump_threshold_rl, dump_threshold_rr);}
        else if (DETECT_BTC_rr==0)
        {tcs_tempW0 = LCTCS_s16ILimitRange(arad_rr,0,255);}
        else    {tcs_tempW0 = LCTCS_s16ILimitRange(b_rad_rr,0,255);}
        CAN_LOG_DATA[99] = (uint8_t)(tcs_tempW0);         // CH26	 
	  #else
        if ( (BTCS_fl==1) || (BTCS_fr==1) )
        {tcs_tempW0 = LCTCS_s16IFindMaximum(rise_threshold_fl, rise_threshold_fr);}
        else if (DETECT_BTC_fl==0)
        {tcs_tempW0 = LCTCS_s16ILimitRange(arad_fl,0,255);}
        else    {tcs_tempW0 = LCTCS_s16ILimitRange(b_rad_fl,0,255);}
        CAN_LOG_DATA[98] = (uint8_t)(tcs_tempW0);     // CH25

        if ( (BTCS_fl==1) || (BTCS_fr==1) )
        {tcs_tempW0 = LCTCS_s16IFindMaximum(dump_threshold_fl, dump_threshold_fr);}
        else if (DETECT_BTC_fr==0)
        {tcs_tempW0 = LCTCS_s16ILimitRange(arad_fr,0,255);}
        else    {tcs_tempW0 = LCTCS_s16ILimitRange(b_rad_fr,0,255);}
        CAN_LOG_DATA[99] = (uint8_t)(tcs_tempW0);         // CH26	 
      #endif  	          
        
      #if __REAR_D
        if (BTCS_ON==1)
        {      
        	CAN_LOG_DATA[100] =(uint8_t) (btcs_hold_scan_disp_rl);     // CH29
        	CAN_LOG_DATA[101] =(uint8_t) (btcs_hold_scan_disp_rr);  // CH30               
        }
        else
        {
			CAN_LOG_DATA[100] =(uint8_t) (btc_tmp_rl/320);     // CH29
        	CAN_LOG_DATA[101] =(uint8_t) (btc_tmp_rr/320);  // CH30                       
        }
      #else
        if (BTCS_ON==1)
        {      
        	CAN_LOG_DATA[100] =(uint8_t) (btcs_hold_scan_disp_fl);     // CH29
        	CAN_LOG_DATA[101] =(uint8_t) (btcs_hold_scan_disp_fr);  // CH30               
        }
        else
        {
			CAN_LOG_DATA[100] =(uint8_t) (btc_tmp_fl/320);     // CH29
        	CAN_LOG_DATA[101] =(uint8_t) (btc_tmp_fr/320);  // CH30                       
        }
      #endif
                
      #if __REAR_D                
        if (BTCS_rl==1)
        {tcs_tempW0 = LCTCS_s16ILimitRange(ltcss16BrkControlIndex_rl/2,0,255);}
        else
        {tcs_tempW0 = LCTCS_s16ILimitRange(abs(wvref_rl - vrad_crt_rl)/2,0,255);}
        CAN_LOG_DATA[102] =(uint8_t) (tcs_tempW0); // CH39
        
        if (BTCS_rr==1)
        {tcs_tempW0 = LCTCS_s16ILimitRange(ltcss16BrkControlIndex_rr/2,0,255);}
        else
        {tcs_tempW0 = LCTCS_s16ILimitRange(abs(wvref_rr - vrad_crt_rr)/2,0,255);}
        CAN_LOG_DATA[103] =(uint8_t) (tcs_tempW0); // CH56        
      #else
        if (BTCS_fl==1)
        {tcs_tempW0 = LCTCS_s16ILimitRange(ltcss16BrkControlIndex_fl/2,0,255);}
        else
        {tcs_tempW0 = LCTCS_s16ILimitRange(abs(wvref_fl - vrad_crt_fl)/2,0,255);}
        CAN_LOG_DATA[102] =(uint8_t) (tcs_tempW0); // CH39
        
        if (BTCS_fr==1)
        {tcs_tempW0 = LCTCS_s16ILimitRange(ltcss16BrkControlIndex_fr/2,0,255);}
        else
        {tcs_tempW0 = LCTCS_s16ILimitRange(abs(wvref_fr - vrad_crt_fr)/2,0,255);}
        CAN_LOG_DATA[103] =(uint8_t) (tcs_tempW0); // CH56                           //0x338
      #endif          

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = (INT)FTCS_flags;}
        //else                                { tcs_tempW0 = lts8InsideWheelLiftCnt;}
        CAN_LOG_DATA[104] = (uint8_t)(tcs_tempW0);     // CH35

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = lts16TargetWheelSpin;}
        else                                { tcs_tempW0 = ltcss16DelVLRLimit;}
        CAN_LOG_DATA[105] = (uint8_t)(tcs_tempW0); // CH36

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = lts16PGain; tcs_tempW1 = lts16DGain; }
        else                                { tcs_tempW0 = (INT)lctcsu16etcsthreshold1; tcs_tempW1 = (INT)lctcsu16etcsthreshold2; }
        CAN_LOG_DATA[106] = (uint8_t)(tcs_tempW0);             // CH37
        CAN_LOG_DATA[107] = (uint8_t)(tcs_tempW1);             // CH38      
        
        CAN_LOG_DATA[108] = (uint8_t)(det_alatm/10);   // CH40
                  
        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = -lts16PGainEffect;}
        else                                { tcs_tempW0 = vel_f_small-vel_r;}
        CAN_LOG_DATA[109] = (uint8_t)(tcs_tempW0);             // CH43
        CAN_LOG_DATA[110] = (uint8_t)(tcs_tempW0>>8);

		CAN_LOG_DATA[111] =(uint8_t) (INHIBITION_INDICATOR());   //0x448
		
        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = -lts16DGainEffect;}
        else                                { tcs_tempW0 = vel_f_big-vel_r;}
        CAN_LOG_DATA[112] = (uint8_t)(tcs_tempW0);             // CH44
        CAN_LOG_DATA[113] = (uint8_t)(tcs_tempW0>>8);

        if      (ETCS_ON==1)    { tcs_tempW0 = (int16_t)SplitTransitionCnt;}
        else if (FTCS_ON==1)    { tcs_tempW0 = ltcss16BothWheelSpinCnt;}
	  #if 	__SUSPENSION_TYPE == DOM
        else                    { tcs_tempW0 = 1;}
      #elif __SUSPENSION_TYPE == EU
        else                    { tcs_tempW0 = 2;}
      #elif __SUSPENSION_TYPE == US
        else                    { tcs_tempW0 = 3;}
	  #else
        else                    { tcs_tempW0 = 4;}
      #endif

        CAN_LOG_DATA[114] =(uint8_t) (tcs_tempW0);             // CH57
        CAN_LOG_DATA[115] =(uint8_t) (tcs_tempW0>>8);
        
   /***********ESC*********************/        	                
	#if __PRESS_EST_ACTIVE
	    CAN_LOG_DATA[116] =(lcs16EscInitialPulseupDutyfl);//(uint8_t) (FL.u8_Estimated_Active_Press);              //ch12
	    CAN_LOG_DATA[117] =(lcs16EscInitialPulseupDutyfr);//(uint8_t) (FR.u8_Estimated_Active_Press);              //ch13
	    CAN_LOG_DATA[118] =(lcs16EscInitialPulseupDutyrl);//(uint8_t) (RL.u8_Estimated_Active_Press);              //ch14                    //ch14
	    CAN_LOG_DATA[119] =(lcs16EscInitialPulseupDutyrr);//(uint8_t) (RR.u8_Estimated_Active_Press);              //ch15
	#else
	  #if __ESP_DUTY_COMPEN
	    CAN_LOG_DATA[116] =(uint8_t) (wheelpress_fl);                      //ch12
	    CAN_LOG_DATA[117] =(uint8_t) (wheelpress_fr);            //ch13
	    CAN_LOG_DATA[118] =(uint8_t) (wheelpress_rl);                       //ch14
	    CAN_LOG_DATA[119] =(uint8_t) (wheelpress_rr);                      //ch15
	  #else
	    CAN_LOG_DATA[116] =(uint8_t) (esp_pressure_count_fl*10);                       //ch12
	    CAN_LOG_DATA[117] =(uint8_t) (esp_pressure_count_fr*10);             //ch13
	    CAN_LOG_DATA[118] =(uint8_t) (esp_pressure_count_rl*10);                        //ch14
	    CAN_LOG_DATA[119] =(uint8_t) (esp_pressure_count_rr*10);                       //ch15      //0x558
	  #endif
	#endif   

	    CAN_LOG_DATA[120] =(uint8_t) (del_target_slip_fl/100);                             //ch21   -- New
	    CAN_LOG_DATA[121] =(uint8_t) (del_target_slip_fr/100); 	     
    
	    CAN_LOG_DATA[122] =(uint8_t) (Beta_MD);                                 //ch22
	    CAN_LOG_DATA[123] =(uint8_t) (Beta_MD>>8);     
	    
	    CAN_LOG_DATA[124] =(uint8_t) (nosign_delta_yaw);                           //ch32
	    CAN_LOG_DATA[125] =(uint8_t) (nosign_delta_yaw>>8);
	    CAN_LOG_DATA[126] =(uint8_t) (nosign_delta_yaw_first);                     //ch33 //0x668
	    
	    CAN_LOG_DATA[127] =(uint8_t) (nosign_delta_yaw_first>>8);
	    CAN_LOG_DATA[128] =(uint8_t) (det_alatc_fltr);//slip_m_rl_current;//;                   //ch34
	    CAN_LOG_DATA[129] =(uint8_t) (det_alatc_fltr>>8);//slip_m_rl_current>>8);//>>8);
	
	    CAN_LOG_DATA[130] =(uint8_t) (det_alatm_fltr);                              //ch35
	    CAN_LOG_DATA[131] =(uint8_t) (det_alatm_fltr>>8);    
 
	    CAN_LOG_DATA[132] =(uint8_t) (det_alatc);//slip_control_front;//target_pressure_rr;                            //ch38
	    CAN_LOG_DATA[133] =(uint8_t) (det_alatc>>8);//slip_control_front>>8);//target_pressure_rr>>8);
	
	if(SLIP_CONTROL_NEED_RL==1)
	{
	    esp_tempW6 = del_target_slip_rl;
	}
	else if(SLIP_CONTROL_NEED_RR==1)
	{
	    esp_tempW6 = del_target_slip_rr;
	}
	else
	{
	    esp_tempW6 = 0;
	}
	    CAN_LOG_DATA[134] =(uint8_t) (esp_tempW6/100);//u16CycleTime;//esp_k9;
	
	    /*CAN_LOG_DATA[134] =(uint8_t) (tINHIBITION_INDICATOR());    */

	  #if __TVBB	
		CAN_LOG_DATA[135] = (uint8_t)(ldtvbbu8InhibitID); // 0x119
		
		CAN_LOG_DATA[136] = (uint8_t)(lctvbbs16ControlMoment);
		CAN_LOG_DATA[137] = (uint8_t)(lctvbbs16ControlMoment>>8);

		 	
	  #else
		CAN_LOG_DATA[135] = 0;
		CAN_LOG_DATA[136] = 0;
                            
		CAN_LOG_DATA[137] = 0; 
	  #endif
		
            		                
        i=0;
        if (BLS                             ==1) i|=0x01;
        if (ABS_fz                          ==1) i|=0x02;
        if (AFZ_OK                          ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
        if ((PBA_ON==1) || (PBA_pulsedown  ==1)) i|=0x10; // BTC_fz
        if (YAW_CDC_WORK                    ==1) i|=0x20;
        if (BEND_DETECT2                    ==1) i|=0x40; // HSA_flg
	
        //if (CBC_ENABLE              		==1) i|=0x80; //CBC_ENABLE
  #if __HSA
    if((vref<=VREF_0_25_KPH) && (ABS_fz==0))
    {      
        if (CLUTCH_POSITION                 ==1) i|=0x10; // BTC_fz // (PBA_ON==1) || (PBA_pulsedown  ==1)
        if (HSA_flg                         ==1) i|=0x20;
        if (STOP_ON_HILL_flag               ==1) i|=0x40; // 
        if (HSA_INHIBITION_flag             ==1) i|=0x80; 
    }
  #endif
        CAN_LOG_DATA[138] = (i);               //57

        i=0;
        if (MOT_ON_FLG                      ==1) i|=0x01;
        if (ESP_SPLIT                       ==1) i|=0x02;                  // ESP_SPLIT
        if (LFC_Split_flag                  ==1) i|=0x04;                  //
        if (LFC_Split_suspect_flag          ==1) i|=0x08;                  //
        if (BEND_DETECT_LEFT                ==1) i|=0x10; //PARTIAL_BRAKE  // BEND_DETECT_LEFT
        if (BEND_DETECT_RIGHT               ==1) i|=0x20; //INITIAL_BRAKE  // BEND_DETECT_RIGHT
        if (Rough_road_suspect_vehicle      ==1) i|=0x40;                  // Rough_road_suspect_vehicle
        if (Rough_road_detect_vehicle       ==1) i|=0x80;                  //
        CAN_LOG_DATA[139] = i;               //58

        i = 0;
        if (ABS_fl                          ==1) i|=0x01;
        if (ESP_ABS_SLIP_CONTROL_fl         ==1) i|=0x02;
        if (STABIL_fl                       ==1) i|=0x04;
        if (AV_HOLD_fl                      ==1) i|=0x08;
        if (AV_DUMP_fl                      ==1) i|=0x10;
        if (GMA_SLIP_fl                     ==1) i|=0x20;
        if (HV_VL_fl                        ==1) i|=0x40;
        if (AV_VL_fl                        ==1) i|=0x80;
        CAN_LOG_DATA[140] = i;               // 59

        i=0;
        if (ABS_fr                          ==1) i|=0x01;
        if (ESP_ABS_SLIP_CONTROL_fr         ==1) i|=0x02;
        if (STABIL_fr                       ==1) i|=0x04;
        if (AV_HOLD_fr                      ==1) i|=0x08;
        if (AV_DUMP_fr                      ==1) i|=0x10;
        if (GMA_SLIP_fr                     ==1) i|=0x20;
        if (HV_VL_fr                        ==1) i|=0x40;
        if (AV_VL_fr                        ==1) i|=0x80;
        CAN_LOG_DATA[141] = i;               //60

        i=0;
        if (ABS_rl                          ==1) i|=0x01;
        if (ESP_ABS_SLIP_CONTROL_rl         ==1) i|=0x02;
        if (STABIL_rl                       ==1) i|=0x04;
        if (SPLIT_JUMP_rl                   ==1) i|=0x08;
        if (SPLIT_PULSE_rl                  ==1) i|=0x10;
        if (RL.AV_DUMP                      ==1) i|=0x20;
        if (HV_VL_rl                        ==1) i|=0x40;
        if (AV_VL_rl                        ==1) i|=0x80;
        CAN_LOG_DATA[142] = i;               //61

        i = 0;
        if(ABS_rr                           ==1) i|=0x01;
        if(ESP_ABS_SLIP_CONTROL_rr          ==1) i|=0x02;
        if(STABIL_rr                        ==1) i|=0x04;
        if(SPLIT_JUMP_rr                    ==1) i|=0x08;
        if(SPLIT_PULSE_rr                   ==1) i|=0x10;
        if(RR.AV_DUMP                       ==1) i|=0x20;
        if(HV_VL_rr                         ==1) i|=0x40;
        if(AV_VL_rr                         ==1) i|=0x80;
        CAN_LOG_DATA[143] = i;               // 62     //0x229

        i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_SECONDARY                        ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fl                        ==1) i|=0x01;
      #endif
        if(FL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(FL.LFC_maintain_split_flag           ==1) i|=0x08;
        if(FL.In_Gear_flag                      ==1) i|=0x10;
        if(FL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(FL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fl                       ==1) i|=0x80;
        CAN_LOG_DATA[144] = i;                   // 63

        i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_PRIMARY                        ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fr                        ==1) i|=0x01;
      #endif
        if(FR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(FR.LFC_maintain_split_flag           ==1) i|=0x08;
        if(FR.In_Gear_flag                      ==1) i|=0x10;
        if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(FR.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fr                       ==1) i|=0x80;
        CAN_LOG_DATA[145] = i;                   // 64

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY                         ==1) i|=0x01;
      #else
        if(S_VALVE_LEFT                         ==1) i|=0x01;
      #endif
        if(RL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(RL.MSL_BASE                          ==1) i|=0x08;
        if(RL.Duty_Limitation_flag              ==1) i|=0x10;
        if(RL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(RL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_rl                       ==1) i|=0x80;
        CAN_LOG_DATA[146] = i;                   // 65

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_PRIMARY                        ==1) i|=0x01;
      #else
        if(S_VALVE_RIGHT                        ==1) i|=0x01;
      #endif
        if(RR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(RR.MSL_BASE                          ==1) i|=0x08;
        if(RR.Duty_Limitation_flag              ==1) i|=0x10;
        if(RR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(RR.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_rr                       ==1) i|=0x80;
        CAN_LOG_DATA[147] = i;                   // 66
        
        i=0;
        if (MPRESS_BRAKE_ON                 ==1) i|=0x01;
        if (BTCS_ON                         ==1) i|=0x02;
        if (ETCS_ON                         ==1) i|=0x04;
        if (FTCS_ON                         ==1) i|=0x08;
        if (ENG_STALL                       ==1) i|=0x10;
		//if (lctcsu1DctSymSpin				==1) i|=0x20;
        if (lctcsu1FastRPMInc				==1) i|=0x40;   
        if (BTCS_rl                         ==1) i|=0x80;             
        CAN_LOG_DATA[148] =(uint8_t) (i);                      // CH45        

        i=0;
        if (BTCS_rr                         ==1) i|=0x01;
        if (lctcsu1Low2SplitSuspect         ==1) i|=0x02;
        if (DETECT_BTC_rl                   ==1) i|=0x04;	
        if (DETECT_BTC_rr					==1) i|=0x08;
        if (ltcsu1HomoMuCtlHavePriority     ==1) i|=0x10;
        if (BTCS_TWO_OFF                    ==1) i|=0x20;
        if (ESP_TCS_ON	                    ==1) i|=0x40;
        if (tc_on_flg	                    ==1) i|=0x80;
        CAN_LOG_DATA[149] =(uint8_t) (i);                      // CH49  
        
        i=0;
        if (tc_error_flg                  	==1) i|=0x01;
        if (tcs_error_flg                 	==1) i|=0x02;	
        if (fu1VoltageUnderErrDet         	==1) i|=0x04;	
        if (BTCS_Entry_Inhibition      	  	==1) i|=0x08;
        //if (lctcsu1BTCSHighSideSpinLeft   	==1) i|=0x10;
        //if (lctcsu1BTCSHighSideSpinRight  	==1) i|=0x20;
        if (ltcsu1TorqLvlRecoverAfterEEC  	==1) i|=0x40;
        if (vdc_on_flg                    	==1) i|=0x80;
        CAN_LOG_DATA[150] =(uint8_t) (i);                      // CH49   
        
        i=0;
        if (ESP_ERROR_FLG         			==1) i|=0x01;
        if (lctcsu1DctSplitHillFlg			==1) i|=0x02;	
        if (gs_ena                			==1) i|=0x04;	
        if (lctcsu1FunctionLampReq			==1) i|=0x08;
        if (ldtcsu1HillDetectionByAx		==1) i|=0x10;
        if (TCS_ON_START          			==1) i|=0x20;
        if (FUNCTION_LAMP_ON      			==1) i|=0x40;
        //if (BTCS_INITIAL_RISE     			==1) i|=0x80;
        CAN_LOG_DATA[151] =(uint8_t) (i);                      // CH49    //0x339
        
        i=0;
        if (GAIN_ADD_IN_TURN         		==1) i|=0x01;
        if (YAW_CHANGE_LIMIT_ACT     		==1) i|=0x02;	
        if (GOOD_TARGET_TRACKING_MODE		==1) i|=0x04;	
        if (FTC_DETECT               		==1) i|=0x08;
        if (ltcsu1AccROK             		==1) i|=0x10;
        //if (ltcsu1HighSideSpinInFlat 		==1) i|=0x20;
        if (state_rl                 		==1) i|=0x40;
        if (state_rr                 		==1) i|=0x80;
        CAN_LOG_DATA[152] =(uint8_t) (i);                      // CH49   
        
        i=0;
        if ((FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)                        ==1) i|=0x01;
        if ((FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)==1) i|=0x02;	
        if (0                                                                                     ==1) i|=0x04;	//lctvbbu1TVBB_ON 
        if (ESP_BRAKE_CONTROL_FL                                                                                ==1) i|=0x08;
        if (ESP_BRAKE_CONTROL_FR                                                                                ==1) i|=0x10;
        if (ESP_BRAKE_CONTROL_RL                                                                                ==1) i|=0x20;
        if (ESP_BRAKE_CONTROL_RR                                                                                ==1) i|=0x40;
        if ((FLAG_ACT_PRE_ACTION_FL||FLAG_ACT_PRE_ACTION_FR)                                                    ==1) i|=0x80;
        CAN_LOG_DATA[153] =(uint8_t) (i);                      // CH49         //0x449
}
#endif/*#if defined(__EXTEND_LOGGER)*/

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallEspLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ESP
********************************************************************************/
void LSESP_vCallESPLogData(void)
{
	uint8_t i;
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);                    //ch1
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                //ch2
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                //ch3
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                //ch4
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                //ch5
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (vref);                       //ch6
    CAN_LOG_DATA[11] =(uint8_t) (vref>>8);
//    CAN_LOG_DATA[12] =(uint8_t) ((q_front<-120)?-120:(int8_t)q_front);  //ch7
//    CAN_LOG_DATA[13] =(uint8_t) ((q_rear<-120)?-120:(int8_t)q_rear);        //ch8
    CAN_LOG_DATA[14] =(uint8_t) (vref5);                              //ch9
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

    CAN_LOG_DATA[16] =(uint8_t) ((moment_q_front<-120)?-120:(int8_t)moment_q_front);    //ch10
    CAN_LOG_DATA[17] =(uint8_t) ((moment_q_rear<-120)?-120:(int8_t)moment_q_rear);  //ch11

if(MPRESS_BRAKE_ON==1)
{
    CAN_LOG_DATA[18] =(uint8_t) ((afz>127)?127:((afz<-127)?-127:(int8_t)afz));     //ch12
}
else
{
    CAN_LOG_DATA[18] =(uint8_t) (ENG_VAR);                                //ch12
}
    CAN_LOG_DATA[19] =(uint8_t) (inhibition_number    =INHIBITION_INDICATOR());   //ch13


if(MPRESS_BRAKE_ON==1) {
    CAN_LOG_DATA[20] = (uint8_t)(flags_fl);                       //ch14
    CAN_LOG_DATA[21] = (uint8_t)(flags_fr);                       //ch15
    CAN_LOG_DATA[22] = (uint8_t)(flags_rl);                       //ch16
    CAN_LOG_DATA[23] = (uint8_t)(flags_rr);                       //ch17
}
else if (BTC_fz==1) {
    CAN_LOG_DATA[20] = (uint8_t)(BTCS_flags_fl);                   //ch14
    CAN_LOG_DATA[21] = (uint8_t)(BTCS_flags_fr);                   //ch15
    CAN_LOG_DATA[22] = (uint8_t)(BTCS_flags_rl);                   //ch16
    CAN_LOG_DATA[23] = (uint8_t)(BTCS_flags_rr);                   //ch17
}
else if (ESP_TCS_ON==1) {
    CAN_LOG_DATA[20] = (uint8_t)(McrAbs(P_gain_effect));         //ch14
    CAN_LOG_DATA[21] = (uint8_t)(McrAbs(D_gain_effect));         //ch15
    CAN_LOG_DATA[22] = (uint8_t)(wslip_max);                  //ch16
    CAN_LOG_DATA[23] = (uint8_t)(TorqLimit/10);               //ch17
}
else {
    CAN_LOG_DATA[20] = (uint8_t)(flags_fl);                        //ch14
    CAN_LOG_DATA[21] = (uint8_t)(flags_fr);                        //ch15
    CAN_LOG_DATA[22] = (uint8_t)(flags_rl);                        //ch16
    CAN_LOG_DATA[23] = (uint8_t)(flags_rr);                        //ch17
}

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
if((YAW_CDC_WORK==1)||(ESP_TCS_ON==1)) 
{
    if      (SLIP_CONTROL_NEED_FL==1)      esp_tempW0=FL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_FR==1)      esp_tempW0=FR_ESC.true_slip_target;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL==1)      esp_tempW1=RL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_RR==1)      esp_tempW1=RR_ESC.true_slip_target;
    else                                esp_tempW1=0;
}
else
{
/*
    tempW2 = max_cycle_time_cal; tempW1 = 100; tempW0 = 625;
    s16muls16divs16();
    esp_tempW0=tempW3;      //max_cycle_time View Setting 100
    if(!KSTEER.FIRST_OFFSET_CAL_FLG)    esp_tempW1=KSTEER.offset_at_15kph;
    else                                esp_tempW1=KSTEER.offset_10avr;
*/
}
#else
if((YAW_CDC_WORK==1)||(ESP_TCS_ON==1)) 
{
    if      (SLIP_CONTROL_NEED_FL==1)      esp_tempW0=true_slip_target_fl;
    else if (SLIP_CONTROL_NEED_FR==1)      esp_tempW0=true_slip_target_fr;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL==1)      esp_tempW1=true_slip_target_rl;
    else if (SLIP_CONTROL_NEED_RR==1)      esp_tempW1=true_slip_target_rr;
    else                                esp_tempW1=0;
}
else
{
/*
    tempW2 = max_cycle_time_cal; tempW1 = 100; tempW0 = 625;
    s16muls16divs16();
    esp_tempW0=tempW3;      //max_cycle_time View Setting 100
    if(!KSTEER.FIRST_OFFSET_CAL_FLG)    esp_tempW1=KSTEER.offset_at_15kph;
    else                                esp_tempW1=KSTEER.offset_10avr;
*/
}
#endif

    CAN_LOG_DATA[24] =(uint8_t) (esp_tempW0);                         //ch18
    CAN_LOG_DATA[25] =(uint8_t) (esp_tempW0>>8);
    CAN_LOG_DATA[26] =(uint8_t) (esp_tempW1);                         //ch19
    CAN_LOG_DATA[27] =(uint8_t) (esp_tempW1>>8);


    CAN_LOG_DATA[28] =(uint8_t) (moment_q_rear_under);//delta_yaw_third_ems;                  //ch20
    CAN_LOG_DATA[29] =(uint8_t) (moment_q_rear_under>>8);//delta_yaw_third_ems>>8);

    CAN_LOG_DATA[30] =(uint8_t) (drive_torq);                         //ch21
    CAN_LOG_DATA[31] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (cal_torq);                           //ch22
    CAN_LOG_DATA[33] =(uint8_t) (cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (eng_torq);                           //ch23
    CAN_LOG_DATA[35] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (eng_rpm);                                //ch24
    CAN_LOG_DATA[37] =(uint8_t) (eng_rpm>>8);

    CAN_LOG_DATA[38] =(uint8_t) (gear_pos);                           //ch25
    CAN_LOG_DATA[39] =(uint8_t) (mtp);                                    //ch26

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               //ch27
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

    CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch28
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                    //ch29
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
    CAN_LOG_DATA[46] =(uint8_t) (Beta_MD);                                //ch30
    CAN_LOG_DATA[47] =(uint8_t) (Beta_MD>>8);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 //ch31
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                           //ch32
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                     //ch33
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);
    CAN_LOG_DATA[54] =(uint8_t) (sign_u_delta_yaw_thres);                 //ch34
    CAN_LOG_DATA[55] =(uint8_t) (sign_u_delta_yaw_thres>>8);

    CAN_LOG_DATA[56] =(uint8_t) (alat);                               //ch35
    CAN_LOG_DATA[57] =(uint8_t) (alat>>8);
    CAN_LOG_DATA[58] =(uint8_t) (esp_mu);                             //ch36
    CAN_LOG_DATA[59] =(uint8_t) (esp_mu>>8);
    CAN_LOG_DATA[60] =(uint8_t) (det_alatm_fltr);                         //ch37
    CAN_LOG_DATA[61] =(uint8_t) (det_alatm_fltr>>8);
    CAN_LOG_DATA[62] =(uint8_t) (det_alatc_fltr);                         //ch38
    CAN_LOG_DATA[63] =(uint8_t) (det_alatc_fltr>>8);
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                             //ch39
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

	i=0;
    if(ESP_ERROR_FLG==1)               i|=0x01;
    if(BLS_PIN==1)                     i|=0x02;
    if(ABS_fz==1)                      i|=0x04;
    if(EBD_RA==1)                      i|=0x08;
    if(BTC_fz==1)                      i|=0x10;
#if __TCS
    if(ETCS_ON==1)                     i|=0x20;
#endif
    if(YAW_CDC_WORK==1)                i|=0x40;
#if __TCS
    if(ESP_TCS_ON==1)                  i|=0x80;
#endif
    CAN_LOG_DATA[66] =(uint8_t) (i);

	i=0;
    if(FLAG_BETA_UNDER_DRIFT==1)       i|=0x01;
    if(FLAG_ACTIVE_BRAKING==1)         i|=0x02;

    if(PARTIAL_BRAKE==1)               i|=0x04;
    if(FLAG_BANK_DETECTED==1)          i|=0x08;
    if(MPRESS_BRAKE_ON==1)             i|=0x10;
#if __ADVANCED_MSC
    if ( (YAW_CDC_WORK ==1)&& (ABS_fz==0) ){
        if(target_vol == MSC_14_V)  i|=0x20;
    }
    else {
        if(MOT_ON_FLG==1)              i|=0x20;
    }
#else
    if(MOT_ON_FLG==1)                  i|=0x20;
#endif
    if(AUTO_TM==1) {
        if(BACK_DIR==1)                i|=0x40;
    } else {
#if __AX_SENSOR
        if(BACKWARD_MOVE==1)           i|=0x40;
#else
//      if(BACKWARD_MOVE)           tempB4|=0x40;
#endif
    }
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((FL_ESC.FLAG_COMPULSION_HOLD==1)||(FR_ESC.FLAG_COMPULSION_HOLD==1)||(RL_ESC.FLAG_COMPULSION_HOLD==1)||(RR_ESC.FLAG_COMPULSION_HOLD==1))            i|=0x80;
#else
    if((FLAG_COMPULSION_HOLD==1)||(FLAG_COMPULSION_HOLD_REAR==1))            i|=0x80;
#endif
    
    CAN_LOG_DATA[67] =(uint8_t) (i);

	i=0;
    if(AV_VL_fl==1)                    i|=0x01;
    if(HV_VL_fl==1)                    i|=0x02;
    if(ABS_fl==1)                      i|=0x04;
    if(BTCS_fl==1)                     i|=0x08;
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fl==1)               i|=0x10;
#else
    if(TCL_DEMAND_SECONDARY==1)        i|=0x10;
#endif
    if(SLIP_CONTROL_NEED_FL==1)        i|=0x20;
    if(OVER_UNDER_JUMP==1)             i|=0x40;
    if(Flag_UNDER_CONTROL==1)          i|=0x80;
    CAN_LOG_DATA[68] =(uint8_t) (i);

	i=0;
    if(AV_VL_fr==1)                    i|=0x01;
    if(HV_VL_fr==1)                    i|=0x02;
    if(ABS_fr==1)                      i|=0x04;
    if(BTCS_fr==1)                     i|=0x08;
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fr==1)               i|=0x10;
#else
    if(TCL_DEMAND_PRIMARY==1)          i|=0x10;
#endif
    if(SLIP_CONTROL_NEED_FR==1)        i|=0x20;
//    if(FLAG_BIG_OVERSTEER==1)          i|=0x40;
    if(KSTEER.SUSPECT_FLG==1)          i|=0x80;
    CAN_LOG_DATA[69] =(uint8_t) (i);

	i=0;
    if(AV_VL_rl==1)                    i|=0x01;
    if(HV_VL_rl==1)                    i|=0x02;
    if(ABS_rl==1)                      i|=0x04;
    if(BTCS_rl==1)                     i|=0x08;
    if(EBD_rl==1)                      i|=0x10;
    if(SLIP_CONTROL_NEED_RL==1)        i|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_LEFT==1)                i|=0x40;
#else
    if(S_VALVE_SECONDARY==1)               i|=0x40;
#endif
    if(KYAW.SUSPECT_FLG==1)            i|=0x80;

    CAN_LOG_DATA[70] =(uint8_t) (i);

	i=0;
    if(AV_VL_rr==1)            i|=0x01;
    if(HV_VL_rr==1)            i|=0x02;
    if(ABS_rr)              i|=0x04;
    if(SAS_OFFSET_SUSPECT_HOLD==1)             i|=0x08;
    if(EBD_rr==1)               i|=0x10;
//    if(QUICK_OUT)               tempB0|=0x10;
    if(SLIP_CONTROL_NEED_RR==1)                i|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_RIGHT==1)       i|=0x40;
#else
    if(S_VALVE_PRIMARY==1)     i|=0x40;
#endif
    if(PHZ_detected_flg2==0){
        if(FLAG_TIME_OPT==1)           i|=0x80;   // steer_360_ok_flg
    }
    else {
        if(WSTR_360_CHECK==1)          i|=0x80;   // steer_360_ok_flg
    }
    CAN_LOG_DATA[71] =(uint8_t) (i);


    CAN_LOG_DATA[72] =(uint8_t) (u16CycleTime);//(Beta_K_Dot);//esp_k9;
    CAN_LOG_DATA[73] =(uint8_t) (u16CycleTime>>8);//(Beta_K_Dot>>8);//(esp_k9>>8);
#if __4WD || __AX_SENSOR
    CAN_LOG_DATA[74] =(uint8_t) (along);  //;ay_log
    CAN_LOG_DATA[75] =(uint8_t) (along>>8);   //acc_r>>8);
#else
    CAN_LOG_DATA[74] =(uint8_t) (afz);
    CAN_LOG_DATA[75] =(uint8_t) (afz>>8);
#endif
    CAN_LOG_DATA[76] =(uint8_t)(FL.pwm_duty_temp);
    CAN_LOG_DATA[77] =(uint8_t)(FR.pwm_duty_temp);
    CAN_LOG_DATA[78] =(uint8_t)(RL.pwm_duty_temp);
    CAN_LOG_DATA[79] =(uint8_t)(RR.pwm_duty_temp);
}


/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallABSLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ABS
********************************************************************************/
void LSESP_vCallABSLogData(void)
{
    uint8_t i;
    /* ID 0x111, 0x6DC */
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);              //1
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);                          //2
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);                          //3
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);                          //4
	/* ID 0x222, 0x6DD */
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);                          //5
    CAN_LOG_DATA[10] =(uint8_t) (vref);
    CAN_LOG_DATA[11] =(uint8_t) (vref>>8);                             //6
  #if __4WD || __AX_SENSOR
    CAN_LOG_DATA[12] =(uint8_t) (along);
    CAN_LOG_DATA[13] =(uint8_t) (along>>8);                            //7
  #else
    CAN_LOG_DATA[12] =(uint8_t) (ebd_filt_grv);
    CAN_LOG_DATA[13] =(uint8_t) (ebd_filt_grv>>8);                     //7
  #endif
	CAN_LOG_DATA[14] =(uint8_t) (fu16CalVoltMOTOR);                             
	CAN_LOG_DATA[15] =(uint8_t) (fu16CalVoltMOTOR>>8);                 //8
    /* ID 0x333, 0x6DE */
    CAN_LOG_DATA[16] =(uint8_t) (MSC_ref_voltage);
    CAN_LOG_DATA[17] =(uint8_t) (MSC_ref_voltage>>8);                  //9
  #if  __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
    CAN_LOG_DATA[18] =(uint8_t) (lcabss16RefMpress);
    CAN_LOG_DATA[19] =(uint8_t) (lcabss16RefMpress>>8);                //10
  #else
    CAN_LOG_DATA[18] =(uint8_t) (MSC_indicator);
    CAN_LOG_DATA[19] =(uint8_t) (MSC_indicator>>8);                           //10
  #endif  
    CAN_LOG_DATA[20] =(uint8_t) (aref_avg-aref0);
    CAN_LOG_DATA[21] =(uint8_t) ((aref_avg-aref0)>>8);                 //11
	CAN_LOG_DATA[22] =(uint8_t) (eng_rpm);
    CAN_LOG_DATA[23] =(uint8_t) (eng_rpm>>8);                          //12
    /* ID 0x444, 0x6DF */
    CAN_LOG_DATA[24] =(uint8_t) (afz>127)?127:((afz<-127)?-127:(int8_t)afz);//13
    CAN_LOG_DATA[25] =(uint8_t) (L2H_sus_cnt_by_aref);                 //14
    CAN_LOG_DATA[26] =(uint8_t) (lsabsu8L2HMode);                      //15
    CAN_LOG_DATA[27] =(uint8_t) (gma_flags);                            //16
    CAN_LOG_DATA[28] =(uint8_t) (FL.flags);                            //17
    CAN_LOG_DATA[29] =(uint8_t) (FR.flags);                            //18
    CAN_LOG_DATA[30] =(uint8_t) (RL.flags);                            //19
    CAN_LOG_DATA[31] =(uint8_t) (RR.flags);                            //20
	/* ID 0x555, 0x6E0 */
    CAN_LOG_DATA[32] =(uint8_t) (FL.rel_lam);                          //21
    CAN_LOG_DATA[33] =(uint8_t) (FR.rel_lam);                          //22
    CAN_LOG_DATA[34] =(uint8_t) (RL.rel_lam);                          //23
    CAN_LOG_DATA[35] =(uint8_t) (RR.rel_lam);                          //24
    CAN_LOG_DATA[36] =(uint8_t) (FL.pwm_duty_temp);                    //25
    CAN_LOG_DATA[37] =(uint8_t) (FR.pwm_duty_temp);                    //26
    CAN_LOG_DATA[38] =(uint8_t) (RL.pwm_duty_temp);                    //27
    CAN_LOG_DATA[39] =(uint8_t) (RR.pwm_duty_temp);                    //28
	/* ID 0x666, 0x6E1 */
    CAN_LOG_DATA[40] =(uint8_t) (FL.Rough_road_detector);              //29
    CAN_LOG_DATA[41] =(uint8_t) (FR.Rough_road_detector);              //30
    CAN_LOG_DATA[42] =(uint8_t) (RL.Rough_road_detector);              //31
    CAN_LOG_DATA[43] =(uint8_t) (RR.Rough_road_detector);              //32
    CAN_LOG_DATA[44] =(uint8_t) (FL.vfiltn);                     	   //33
    CAN_LOG_DATA[45] =(uint8_t) (FR.vfiltn);                     	   //34
    CAN_LOG_DATA[46] =(uint8_t) (RL.vfiltn);                     	   //35
    CAN_LOG_DATA[47] =(uint8_t) (RR.vfiltn);                     	   //36
	/* ID 0x117, 0x6E2 */
    CAN_LOG_DATA[48] =(uint8_t) (arad_fl);                             //37
    CAN_LOG_DATA[49] =(uint8_t) (arad_fr);                             //38
    CAN_LOG_DATA[50] =(uint8_t) (arad_rl);                             //39
    CAN_LOG_DATA[51] =(uint8_t) (arad_rr);                             //40
    CAN_LOG_DATA[52] =(uint8_t) (FL.High_dump_factor>12700)?127:
       ((FL.High_dump_factor<-12700)?-127:(int8_t)(FL.High_dump_factor/100)); //41
    CAN_LOG_DATA[53] =(uint8_t) (FR.High_dump_factor>12700)?127:
       ((FR.High_dump_factor<-12700)?-127:(int8_t)(FR.High_dump_factor/100)); //42
    CAN_LOG_DATA[54] =(uint8_t) (RL.High_dump_factor>12700)?127:
       ((RL.High_dump_factor<-12700)?-127:(int8_t)(RL.High_dump_factor/100)); //43
    CAN_LOG_DATA[55] =(uint8_t) (RR.High_dump_factor>12700)?127:
       ((RR.High_dump_factor<-12700)?-127:(int8_t)(RR.High_dump_factor/100)); //44
	/* ID 0x227, 0x6E3 */
    CAN_LOG_DATA[56] =(uint8_t) ((int8_t)FL.LFC_special_comp_duty);    //45
    CAN_LOG_DATA[57] =(uint8_t) ((int8_t)FR.LFC_special_comp_duty);    //46
    CAN_LOG_DATA[58] =(uint8_t) ((int8_t)RL.LFC_special_comp_duty);    //47
    CAN_LOG_DATA[59] =(uint8_t) ((int8_t)RR.LFC_special_comp_duty);    //48
    CAN_LOG_DATA[60] =(uint8_t) ((int8_t)FL.on_time_outlet_pwm);       //49
    CAN_LOG_DATA[61] =(uint8_t) ((int8_t)RL.on_time_outlet_pwm);       //50
    CAN_LOG_DATA[62] =(uint8_t) (FL.LFC_STABIL_counter2);              //51
    CAN_LOG_DATA[63] =(uint8_t) (FR.LFC_STABIL_counter2);              //52
	/* ID 0x337, 0x6E4 */
    CAN_LOG_DATA[64] =(uint8_t) (RL.LFC_STABIL_counter2);              //53
    CAN_LOG_DATA[65] =(uint8_t) (RR.LFC_STABIL_counter2);              //54
  #if __EST_MU_FROM_SKID
    tempW7 = 0;
    if     (FL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1000;}
    else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1000;}
    else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1000;}
    else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1000;}
    else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1000;}
    else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1000;}
    else                                                             {tempW7 = tempW7 + 9*1000;}
    if     (FR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*100;}
    else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*100;}
    else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*100;}
    else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*100;}
    else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*100;}
    else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*100;}
    else                                                             {tempW7 = tempW7 + 9*100;}
    if     (RL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*10;}
    else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*10;}
    else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*10;}
    else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*10;}
    else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*10;}
    else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*10;}
    else                                                             {tempW7 = tempW7 + 9*10;}
    if     (RR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1;}
    else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1;}
    else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1;}
    else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1;}
    else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1;}
    else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1;}
    else                                                             {tempW7 = tempW7 + 9*1;}
    
    CAN_LOG_DATA[66] =(uint8_t) (tempW7);
    CAN_LOG_DATA[67] =(uint8_t) (tempW7>>8);                		   //55
  #endif

  #if  __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
  	CAN_LOG_DATA[68] =(uint8_t) (lcabss16RefCircuitPress);
  	CAN_LOG_DATA[69] =(uint8_t) (lcabss16RefCircuitPress>>8);   	   //56
  #else  
    CAN_LOG_DATA[68] =(uint8_t) (mpress);
    CAN_LOG_DATA[69] =(uint8_t) (mpress>>8);                           //56
  #endif

    i=0;
    if (ABS_fz                          ==1) i|=0x01;
    if (BLS                             ==1) i|=0x02;
    if (AFZ_OK                          ==1) i|=0x04;
    if (MSL_SPLIT_CLR                   ==1) i|=0x08;
    if (BEND_DETECT                     ==1) i|=0x10;
    if (LFC_end_duty_OK_flag            ==1) i|=0x20;
    if (LFC_Split_flag                  ==1) i|=0x40;
    if (LFC_Split_suspect_flag          ==1) i|=0x80;
    CAN_LOG_DATA[70] =(uint8_t) (i);                                   //57

    i=0;
    if (GMA_SLIP_fl                     ==1) i|=0x01;
    if (GMA_SLIP_fr                     ==1) i|=0x02;
    if (Rough_road_suspect_vehicle      ==1) i|=0x04;
    if (Rough_road_arad_OK              ==1) i|=0x08;
    if (Rough_road_detect_vehicle_EBD   ==1) i|=0x10;
    if (Rough_road_detect_vehicle       ==1) i|=0x20;
    if (MOT_ON_FLG                      ==1) i|=0x40;
    if (EBD_RA                          ==1) i|=0x80;
    CAN_LOG_DATA[71] =(uint8_t) (i);                                   //58
	/* ID 0x447, 0x6E5 */
    i = 0;
    if (ABS_fl                          ==1) i|=0x01;
    if (FL.LFC_Bump_Suspect_flag        ==1) i|=0x02;
    if (STABIL_fl                       ==1) i|=0x04;
    if (AV_HOLD_fl                      ==1) i|=0x08;
    if (AV_DUMP_fl                      ==1) i|=0x10;
    if (FL.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;
    if (HV_VL_fl                        ==1) i|=0x40;
    if (AV_VL_fl                        ==1) i|=0x80;
    CAN_LOG_DATA[72] =(uint8_t) (i);                                   //59

    i=0;
    if (ABS_fr                          ==1) i|=0x01;
    if (FR.LFC_Bump_Suspect_flag        ==1) i|=0x02;
    if (STABIL_fr                       ==1) i|=0x04;
    if (AV_HOLD_fr                      ==1) i|=0x08;
    if (AV_DUMP_fr                      ==1) i|=0x10;
    if (FR.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;
    if (HV_VL_fr                        ==1) i|=0x40;
    if (AV_VL_fr                        ==1) i|=0x80;
    CAN_LOG_DATA[73] =(uint8_t) (i);                                   //60

    i=0;
    if (ABS_rl                          ==1) i|=0x01;
    if (RL.LFC_Bump_Suspect_flag        ==1) i|=0x02;
    if (STABIL_rl                       ==1) i|=0x04;
    if (SPLIT_JUMP_rl                   ==1) i|=0x08;
    if (SPLIT_PULSE_rl                  ==1) i|=0x10;
    if (RL.Hold_Duty_Comp_flag_1st      ==1) i|=0x20;
    if (HV_VL_rl                        ==1) i|=0x40;
    if (AV_VL_rl                        ==1) i|=0x80;
    CAN_LOG_DATA[74] =(uint8_t) (i);                                   //61

    i = 0;
    if(ABS_rr                           ==1) i|=0x01;
    if(RR.LFC_Bump_Suspect_flag         ==1) i|=0x02;
    if(STABIL_rr                        ==1) i|=0x04;
    if(SPLIT_JUMP_rr                    ==1) i|=0x08;
    if(SPLIT_PULSE_rr                   ==1) i|=0x10;
    if(RR.Hold_Duty_Comp_flag_1st       ==1) i|=0x20;
    if(HV_VL_rr                         ==1) i|=0x40;
    if(AV_VL_rr                         ==1) i|=0x80;
    CAN_LOG_DATA[75] =(uint8_t) (i);                                   //62

    i=0;

    if(FL.High_TO_Low_Suspect_flag  ==1) i|=0x01;
/*    if(FL.LFC_rough_road_disable_comp_flag  ==1) i|=0x01;   */
    if(FL.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;
    if(FL.PossibleABSForBump                ==1) i|=0x04;
    if(FL.LFC_maintain_split_flag           ==1) i|=0x08;
    if(FL.In_Gear_flag                      ==1) i|=0x10;
    if(FL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;
    if(LOW_to_HIGH_suspect                  ==1) i|=0x40;
    if(SPOLD_RESET_fl                       ==1) i|=0x80;

    CAN_LOG_DATA[76] =(uint8_t) (i);                                   //63

     i=0;

    if(FR.High_TO_Low_Suspect_flag  ==1) i|=0x01;
/*    if(FR.LFC_rough_road_disable_comp_flag  ==1) i|=0x01;   */
    if(FR.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;
    if(FR.PossibleABSForBump                ==1) i|=0x04;
    if(FR.LFC_maintain_split_flag           ==1) i|=0x08;
    if(FR.In_Gear_flag                      ==1) i|=0x10;
    if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;
    if(LOW_to_HIGH_suspect2_flg             ==1) i|=0x40;
    if(SPOLD_RESET_fr                       ==1) i|=0x80;

    CAN_LOG_DATA[77] =(uint8_t) (i);                                  //64

    i=0;
    if(RL.Duty_Limitation_flag              ==1) i|=0x01;
    if(RL.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;
    if(RL.PossibleABSForBump                ==1) i|=0x04;
    if(RL.LFC_maintain_split_flag           ==1) i|=0x08;
    if(RL.MSL_BASE                          ==1) i|=0x10;
    if(RL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;
    if(PossibleABSForBumpVehicle            ==1) i|=0x40;
    if(SPOLD_RESET_rl                       ==1) i|=0x80;

    CAN_LOG_DATA[78] =(uint8_t) (i);                                  //65

    i=0;
    if(RR.Duty_Limitation_flag              ==1) i|=0x01;
    if(RR.LFC_initial_deep_slip_comp_flag   ==1) i|=0x02;
    if(RR.PossibleABSForBump                ==1) i|=0x04;
    if(RR.LFC_maintain_split_flag           ==1) i|=0x08;
    if(RR.MSL_BASE                          ==1) i|=0x10;
    if(RR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;
    if(RR.LFC_built_reapply_flag            ==1) i|=0x40;
    if(SPOLD_RESET_rr                       ==1) i|=0x80;

    CAN_LOG_DATA[79] =(uint8_t) (i);                                  //66

}

/*******************************************************************************
* FUNCTION NAME:        LSTCS_vCallTcsLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of TCS
********************************************************************************/
void LSTCS_vCallTCSLogData(void)
{
#if defined(__EXTEND_LOGGER)
        uint8_t i;
/*0x6DC*******************************************************************************************************/        
        /*CH-1*/
        CAN_LOG_DATA[0] =(uint8_t) (system_loop_counter);
        CAN_LOG_DATA[1] =(uint8_t) (system_loop_counter>>8);
        /*CH-2*/
        CAN_LOG_DATA[2] =(uint8_t) (vrad_crt_fl);
        CAN_LOG_DATA[3] =(uint8_t) (vrad_crt_fl>>8);
        /*CH-3*/
        CAN_LOG_DATA[4] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_fl);
        CAN_LOG_DATA[5] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_fl>>8);
        /*CH-4*/
        CAN_LOG_DATA[6] =(uint8_t) (vrad_crt_fr);
        CAN_LOG_DATA[7] =(uint8_t) (vrad_crt_fr>>8);
        
/*0x6DD*******************************************************************************************************/                
        /*CH-5*/
        CAN_LOG_DATA[8] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_fr);
        CAN_LOG_DATA[9] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_fr>>8);
        /*CH-6*/
        CAN_LOG_DATA[10] =(uint8_t) (vrad_crt_rl);
        CAN_LOG_DATA[11] =(uint8_t) (vrad_crt_rl>>8);
        /*CH-7*/
        CAN_LOG_DATA[12] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_rl);
        CAN_LOG_DATA[13] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_rl>>8);
        /*CH-8*/
        CAN_LOG_DATA[14] =(uint8_t) (vrad_crt_rr);
        CAN_LOG_DATA[15] =(uint8_t) (vrad_crt_rr>>8);
		
		/*0x6DE*******************************************************************************************************/
		/*CH-9*/
        CAN_LOG_DATA[16] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_rr);
        CAN_LOG_DATA[17] =(uint8_t) (ltcss16MovingAveragedWhlSpdDiff_rr>>8);        
#if __VDC
		/*CH-10*/
        CAN_LOG_DATA[18] =(uint8_t) (vref5);
        CAN_LOG_DATA[19] =(uint8_t) (vref5>>8);
	#else
		/*CH-10*/
        CAN_LOG_DATA[18] =(uint8_t) (vref); 
        CAN_LOG_DATA[19] =(uint8_t) (vref>>8);
#endif
		if (etcs_on_count==1)
		{	/*CH-11*/
        	CAN_LOG_DATA[20] =(uint8_t) (ltcss16ETCSEnterExitFlags);		 
        	CAN_LOG_DATA[21] =(uint8_t) (ltcss16ETCSEnterExitFlags>>8);
    	}
    	else if	(ETCS_ON==1)
    	{  	/*CH-11*/
        	CAN_LOG_DATA[20] =(uint8_t) (ltcss16BaseTorqueLevel);			
        	CAN_LOG_DATA[21] =(uint8_t) (ltcss16BaseTorqueLevel>>8);
    	}
		else
		{	/*CH-11*/
        	CAN_LOG_DATA[20] =(uint8_t) (lctcss16MinimumTorqLevel);		
        	CAN_LOG_DATA[21] =(uint8_t) (lctcss16MinimumTorqLevel>>8);
    	}

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = esp_cmd;}
        else                                { tcs_tempW0 = (int16_t)ENG_VAR;}
        /*CH-12*/
        CAN_LOG_DATA[22] =(uint8_t) (tcs_tempW0);            
        CAN_LOG_DATA[23] =(uint8_t) (tcs_tempW0>>8);

/*0x6DF*******************************************************************************************************/                        
		/*CH-13*/
        CAN_LOG_DATA[24] =(uint8_t) (lts16WheelSpin);
        CAN_LOG_DATA[25] =(uint8_t) (lts16WheelSpin>>8);
        /*CH-14*/
        CAN_LOG_DATA[26] =(uint8_t) (lts16WheelSpinErrorDiff);
        CAN_LOG_DATA[27] =(uint8_t) (lts16WheelSpinErrorDiff>>8);
		/*CH-15*/
        CAN_LOG_DATA[28] =(uint8_t) (delta_yaw_eec);
        CAN_LOG_DATA[29] =(uint8_t) (delta_yaw_eec>>8);
        /*CH-16*/
        CAN_LOG_DATA[30] =(uint8_t) (yaw_out);
        CAN_LOG_DATA[31] =(uint8_t) (yaw_out>>8);
        
/*0x6E0*******************************************************************************************************/                        
        /*CH-17*/
        CAN_LOG_DATA[32] =(uint8_t) (u_delta_yaw_thres_eec);
        CAN_LOG_DATA[33] =(uint8_t) (u_delta_yaw_thres_eec>>8);
        /*CH-18*/
        CAN_LOG_DATA[34] =(uint8_t) (o_delta_yaw_thres_eec);
        CAN_LOG_DATA[35] =(uint8_t) (o_delta_yaw_thres_eec>>8);
        /*CH-19*/
        CAN_LOG_DATA[36] =(uint8_t) (delta_yaw_third_ems);
        CAN_LOG_DATA[37] =(uint8_t) (delta_yaw_third_ems>>8);
        /*CH-20*/
        CAN_LOG_DATA[38] =(uint8_t) (o_delta_yaw_third_ems);
        CAN_LOG_DATA[39] =(uint8_t) (o_delta_yaw_third_ems>>8);
        
        /*0x6E1*******************************************************************************************************/
        /*CH-21*/
        CAN_LOG_DATA[40] =(uint8_t) (wstr);
        CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);
    #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==ENABLE) || (__AX_SENSOR)
        /*CH-22*/
        CAN_LOG_DATA[42] =(uint8_t) (ax_Filter);
        CAN_LOG_DATA[43] =(uint8_t) (ax_Filter>>8);
		if ( (ETCS_ON==1) || (FTCS_ON==1) || (vref < VREF_0_25_KPH) )
		{	/*CH-23*/
        	CAN_LOG_DATA[44] =(uint8_t) (lsu8DrvMode);                    
        	CAN_LOG_DATA[45] =(uint8_t) (lsu8DrvMode>>8);
    	}
    	else
    	{	/*CH-23*/
        	CAN_LOG_DATA[44] =(uint8_t) (ltcss16MiniTireTendencyCnt);     
        	CAN_LOG_DATA[45] =(uint8_t) (ltcss16MiniTireTendencyCnt>>8);
    	}
    #else
    	/*CH-22*/
        CAN_LOG_DATA[42] =(uint8_t) (u16CycleTime);       
        CAN_LOG_DATA[43] =(uint8_t) (u16CycleTime>>8);
        /*CH-23*/
        CAN_LOG_DATA[44] =(uint8_t) (ltcss16MiniTireTendencyCnt);  
        CAN_LOG_DATA[45] =(uint8_t) (ltcss16MiniTireTendencyCnt>>8);
    #endif
		#if __BRK_SIG_MPS_TO_PEDAL_SEN    
		/*CH-24*/
        CAN_LOG_DATA[46] =(uint8_t) (lsesps16EstBrkPressByBrkPedal);
        CAN_LOG_DATA[47] =(uint8_t) (lsesps16EstBrkPressByBrkPedal>>8);    	
		#endif
		/*CH-24*/
        CAN_LOG_DATA[46] =(uint8_t) (acc_r);   
        CAN_LOG_DATA[47] =(uint8_t) (acc_r>>8);
        
        /*0x6E2*******************************************************************************************************/
        /*CH-25*/
        CAN_LOG_DATA[48] =(uint8_t) (drive_torq);       
        CAN_LOG_DATA[49] =(uint8_t) (drive_torq>>8);
        /*CH-26*/
        CAN_LOG_DATA[50] =(uint8_t) (cal_torq);         
        CAN_LOG_DATA[51] =(uint8_t) (cal_torq>>8);
        /*CH-27*/
        CAN_LOG_DATA[52] =(uint8_t) (eng_torq);         
        CAN_LOG_DATA[53] =(uint8_t) (eng_torq>>8);
        /*CH-28*/
        CAN_LOG_DATA[54] =(uint8_t) (eng_rpm);          
        CAN_LOG_DATA[55] =(uint8_t) (eng_rpm>>8);
        
        /*0x6E3*******************************************************************************************************/
        if (BTCS_fl==1)
	{
        	tcs_tempW0 = rise_threshold_fl;
        	tcs_tempW1 = dump_threshold_fl;
    }
    else
    {
        	tcs_tempW0 = LCTCS_s16ILimitRange(arad_fl,0,255);
    }
        /*CH-29*/
        CAN_LOG_DATA[56] = (uint8_t)(tcs_tempW0); 
        /*CH-30*/
        CAN_LOG_DATA[57] = (uint8_t)(tcs_tempW1); 

        if (BTCS_fr==1)
        {
        	tcs_tempW0 = rise_threshold_fr;
        	tcs_tempW1 = dump_threshold_fr;
        }
        else
        {
        	tcs_tempW0 = LCTCS_s16ILimitRange(arad_fr,0,255);
        }
        /*CH-31*/
        CAN_LOG_DATA[58] = (uint8_t)(tcs_tempW0); 
        /*CH-32*/
        CAN_LOG_DATA[59] = (uint8_t)(tcs_tempW1); 

        if (BTCS_rl==1)
        {
        	tcs_tempW0 = rise_threshold_rl;
        	tcs_tempW1 = dump_threshold_rl;
        }
        else 
        {
        	tcs_tempW0 = LCTCS_s16ILimitRange(arad_rl,0,255);
        }
        /*CH-33*/
        CAN_LOG_DATA[60] = (uint8_t)(tcs_tempW0); 
        /*CH-34*/
        CAN_LOG_DATA[61] = (uint8_t)(tcs_tempW1); 

        if (BTCS_rr==1)
        {
        	tcs_tempW0 = rise_threshold_rr;
        	tcs_tempW1 = dump_threshold_rr;
        }
        else 
        {
        	tcs_tempW0 = LCTCS_s16ILimitRange(arad_rr,0,255);
        }
        /*CH-35*/
        CAN_LOG_DATA[62] = (uint8_t)(tcs_tempW0); 
        /*CH-36*/
        CAN_LOG_DATA[63] = (uint8_t)(tcs_tempW1);                 

		/*0x6E4*******************************************************************************************************/
        /*CH-37*/
        CAN_LOG_DATA[64] =(uint8_t) (gear_pos);     
        /*CH-38*/
        CAN_LOG_DATA[65] =(uint8_t) (gs_sel);         
        /*CH-39*/
        CAN_LOG_DATA[66] =(uint8_t) (mtp);      
        /*CH-40*/
        tcs_tempW0 = LCTCS_s16ILimitRange(mpress/10, 0 , 255 );
        CAN_LOG_DATA[67] = (uint8_t)(tcs_tempW0);
        /*CH-41*/
        CAN_LOG_DATA[68] =(uint8_t) (la_FL1HP.u8PwmDuty);
        /*CH-42*/
        CAN_LOG_DATA[69] =(uint8_t) (la_FL2HP.u8PwmDuty);
        /*CH-43*/
        CAN_LOG_DATA[70] =(uint8_t) (la_FR1HP.u8PwmDuty);
        /*CH-44*/
        CAN_LOG_DATA[71] =(uint8_t) (la_FR2HP.u8PwmDuty);
        
        /*0x6E5*******************************************************************************************************/
        /*CH-45*/
        CAN_LOG_DATA[72] =(uint8_t) (la_RL1HP.u8PwmDuty);
        /*CH-46*/
        CAN_LOG_DATA[73] =(uint8_t) (la_RL2HP.u8PwmDuty);
        /*CH-47*/
        CAN_LOG_DATA[74] =(uint8_t) (la_RR1HP.u8PwmDuty);
        /*CH-48*/
        CAN_LOG_DATA[75] =(uint8_t) (la_RR2HP.u8PwmDuty);

		if (BTCS_ON==1)
		{
			/*CH-49*/
            CAN_LOG_DATA[76] =(uint8_t) (0); 
            /*CH-50*/
            CAN_LOG_DATA[77] =(uint8_t) (0); 
            /*CH-51*/
            CAN_LOG_DATA[78] =(uint8_t) (0); 
            /*CH-52*/
            CAN_LOG_DATA[79] =(uint8_t) (0); 
        }
        else
        {
        	/*CH-49*/
            CAN_LOG_DATA[76] =(uint8_t) (btc_tmp_fl/320);         
            /*CH-50*/
            CAN_LOG_DATA[77] =(uint8_t) (btc_tmp_fr/320);         
            /*CH-51*/
            CAN_LOG_DATA[78] =(uint8_t) (btc_tmp_rl/320);         
            /*CH-52*/
            CAN_LOG_DATA[79] =(uint8_t) (btc_tmp_rr/320);         
        }
        
        /*0x6E6*******************************************************************************************************/
        {tcs_tempW0 = LCTCS_s16ILimitRange(McrAbs(wvref_fl - vrad_crt_fl)/2,0,255);}
        /*CH-53*/
        CAN_LOG_DATA[80] =(int8_t) (tcs_tempW0);

        {tcs_tempW0 = LCTCS_s16ILimitRange(McrAbs(wvref_fr - vrad_crt_fr)/2,0,255);}
        /*CH-54*/
        CAN_LOG_DATA[81] =(int8_t) (tcs_tempW0);
        
        {tcs_tempW0 = LCTCS_s16ILimitRange(McrAbs(wvref_rl - vrad_crt_rl)/2,0,255);}
        /*CH-55*/
        CAN_LOG_DATA[82] =(int8_t) (tcs_tempW0);
        
        {tcs_tempW0 = LCTCS_s16ILimitRange(McrAbs(wvref_rr - vrad_crt_rr)/2,0,255);}
        /*CH-56*/
        CAN_LOG_DATA[83] =(int8_t) (tcs_tempW0);                

        if (BTCS_ON==1)
        {
        	/*CH-57*/
            CAN_LOG_DATA[84] = (uint8_t)(target_vol/100);
            /*CH-58*/
            CAN_LOG_DATA[85] = (uint8_t)(fu16CalVoltIntMOTOR/100);
        }
        else
        {
        	/*CH-57*/
            CAN_LOG_DATA[84] = (uint8_t)(FL_TCS.lctcss16AsymSpnCtlTh); 
            /*CH-58*/
            CAN_LOG_DATA[85] = (uint8_t)(RL_TCS.lctcss16AsymSpnCtlTh); 
        }

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = (int16_t)lctcsu8TCSEngCtlPhase;}
        else                                { tcs_tempW0 = 0;}
        /*CH-59*/
        CAN_LOG_DATA[86] = (uint8_t)(tcs_tempW0);

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = lts16TargetWheelSpin;}
        else                                { tcs_tempW0 = ltcss16DelVLRLimit;}
        /*CH-60*/
        CAN_LOG_DATA[87] = (uint8_t)(tcs_tempW0);
        
		/*0x6E7*******************************************************************************************************/
        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = lts16PGain; tcs_tempW1 = lts16DGain; }
        else                                { tcs_tempW0 = (int16_t)lctcsu16etcsthreshold1; tcs_tempW1 = (int16_t)lctcsu16etcsthreshold2; }
        /*CH-61*/
        CAN_LOG_DATA[88] = (uint8_t)(tcs_tempW0);
        /*CH-62*/
        CAN_LOG_DATA[89] = (uint8_t)(tcs_tempW1);
        /*CH-63*/
        CAN_LOG_DATA[90] = (uint8_t)(det_alatm/10);   
        /*CH-64*/
        tcs_tempW0 = LCTCS_s16ILimitRange((alat/10),-125,125);
        CAN_LOG_DATA[91] = (uint8_t)((int8_t)(tcs_tempW0));
        /*CH-65*/
        tcs_tempW0 = LCTCS_s16ILimitRange((det_alatc/10),-125,125);
        CAN_LOG_DATA[92] = (uint8_t)((int8_t)(tcs_tempW0));
        /*CH-66*/
	
	#if (__ESC_MOTOR_PWM_CONTROL == ENABLE)
        CAN_LOG_DATA[93] = (uint8_t)(lau8MscDuty);
	#else        
		CAN_LOG_DATA[93] = (uint8_t)(0);
	#endif

        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = -lts16PGainEffect;}
        else                                { tcs_tempW0 = vel_f_small-vel_r;}
        /*CH-67*/
        CAN_LOG_DATA[94] = (uint8_t)(tcs_tempW0);
        CAN_LOG_DATA[95] = (uint8_t)(tcs_tempW0>>8);

		/*0x6E8*******************************************************************************************************/
        if ( (ETCS_ON==1) || (FTCS_ON==1) ) { tcs_tempW0 = -lts16DGainEffect;}
        else                                { tcs_tempW0 = vel_f_big-vel_r;}
        /*CH-68*/
        CAN_LOG_DATA[96] = (uint8_t)(tcs_tempW0);             
        CAN_LOG_DATA[97] = (uint8_t)(tcs_tempW0>>8);
        
        if      (ETCS_ON==1)    { tcs_tempW0 = (int16_t)SplitTransitionCnt;}
        else if (FTCS_ON==1)    { tcs_tempW0 = ltcss16BothWheelSpinCnt;}
	  #if 	__SUSPENSION_TYPE == KOREA
        else                    { tcs_tempW0 = 1;}
      #elif __SUSPENSION_TYPE == EU
        else                    { tcs_tempW0 = 2;}
      #elif __SUSPENSION_TYPE == USA
        else                    { tcs_tempW0 = 3;}
	  #else
        else                    { tcs_tempW0 = 4;}
      #endif
		/*CH-69*/
        CAN_LOG_DATA[98] =(uint8_t) (tcs_tempW0);          
        CAN_LOG_DATA[99] =(uint8_t) (tcs_tempW0>>8);
        /*CH-70*/
		CAN_LOG_DATA[100] =(uint8_t) (s8SetYawPDGainSelecter);
		/*CH-71*/
		CAN_LOG_DATA[101] =(uint8_t) (lctcss16BrkCtrlMode);
		/*CH-72*/
		CAN_LOG_DATA[102] =(uint8_t) (lctcss16EngCtrlMode);
		/*CH-73*/
		CAN_LOG_DATA[103] =(uint8_t) (0);/*lctcss16EECCtrlMode '13SWD Ver.3.6 반영예정*/

		/*0x6E9*******************************************************************************************************/
		/*CH-74*/
		CAN_LOG_DATA[104] =(uint8_t) (delta_yaw_first_dot);
		CAN_LOG_DATA[105] =(uint8_t) (delta_yaw_first_dot>>8);
		/*CH-75*/
		CAN_LOG_DATA[106] =(uint8_t) (s16SetYawPGain);
		CAN_LOG_DATA[107] =(uint8_t) (s16SetYawPGain>>8);
		/*CH-76*/
		CAN_LOG_DATA[108] =(uint8_t) (s16SetYawDGain);
		CAN_LOG_DATA[109] =(uint8_t) (s16SetYawDGain>>8);
		/*CH-77*/
		CAN_LOG_DATA[110] =(uint8_t) (s16YawPGainEffect);
		CAN_LOG_DATA[111] =(uint8_t) (s16YawPGainEffect>>8);

		/*0x6EA*******************************************************************************************************/
		/*CH-78*/
		CAN_LOG_DATA[112] =(uint8_t) (s16YawDGainEffect);
		CAN_LOG_DATA[113] =(uint8_t) (s16YawDGainEffect>>8);
		/*CH-79*/
		CAN_LOG_DATA[114] =(uint8_t) (FL.s16_Estimated_Active_Press/10);
		/*CH-80*/
		CAN_LOG_DATA[115] =(uint8_t) (FR.s16_Estimated_Active_Press/10);
		/*CH-81*/
		CAN_LOG_DATA[116] =(uint8_t) (RL.s16_Estimated_Active_Press/10);
		/*CH-82*/
		CAN_LOG_DATA[117] =(uint8_t) (RR.s16_Estimated_Active_Press/10);
		/*CH-83*/
		CAN_LOG_DATA[118] =(uint8_t) (eec_stable_exit_count);
		CAN_LOG_DATA[119] =(uint8_t) (eec_stable_exit_count>>8);

		/*0x6EB*******************************************************************************************************/
		/*CH-84*/		
		CAN_LOG_DATA[120] =(uint8_t) (Beta_MD);
		CAN_LOG_DATA[121] =(uint8_t) (Beta_MD>>8);
		/*CH-85*/		
		#if(__EDC==ENABLE)
		CAN_LOG_DATA[122] =(uint8_t) (EDC_cmd);
		CAN_LOG_DATA[123] =(uint8_t) (EDC_cmd>>8);
		#else
		CAN_LOG_DATA[122] =(uint8_t) (tcs_cmd);
		CAN_LOG_DATA[123] =(uint8_t) (tcs_cmd>>8);
		#endif

		/*CH-86*/		
		CAN_LOG_DATA[124] =(uint8_t) (slope_acc_r);
		CAN_LOG_DATA[125] =(uint8_t) (slope_acc_r>>8);
		/*CH-87*/		
		CAN_LOG_DATA[126] =(uint8_t) (slope_eng_rpm);
		CAN_LOG_DATA[127] =(uint8_t) (slope_eng_rpm>>8);
		
		/*0x6EC*******************************************************************************************************/
		/*CH-88*/		
	   #if(__EDC==ENABLE)
		CAN_LOG_DATA[128] =(uint8_t) (EDC_flags);
	   #else
		CAN_LOG_DATA[128] =(uint8_t) (lctcsu8FastTorqRecoveryStatus);
	   #endif
		/*CH-89*/		
		CAN_LOG_DATA[129] =(uint8_t) (gear_state);
		/*CH-90*/		
		CAN_LOG_DATA[130] =(uint8_t) (engine_state);
		/*CH-91*/		
	   #if(__EDC==ENABLE)
		CAN_LOG_DATA[131] =(uint8_t) (EDC_exit_counter);
	   #else
		CAN_LOG_DATA[131] =(uint8_t) (lctcss16SplitHillDctCnt);
	   #endif
		
	   #if (__TOD==ENABLE)
		/*CH-92*/		
		CAN_LOG_DATA[132] =(uint8_t) (TODModeSelector);
		/*CH-93*/		
		CAN_LOG_DATA[133] =(uint8_t) (TOD4wdTqcLimTorq);
		CAN_LOG_DATA[134] =(uint8_t) (TOD4wdTqcLimTorq>>8);
		/*CH-94*/		
		CAN_LOG_DATA[135] =(uint8_t) (TOD4wdLimMode);
	   #else
		/*CH-92*/		
		CAN_LOG_DATA[132] =(uint8_t) (lctcss16AllowableMaxRPM/100);
		/*CH-93*/		
		CAN_LOG_DATA[133] =(uint8_t) (lctcss16WhlSpdreferanceByMiniT);
		CAN_LOG_DATA[134] =(uint8_t) (lctcss16WhlSpdreferanceByMiniT>>8);
		/*CH-94*/		
		CAN_LOG_DATA[135] =(uint8_t) (lctcss16AllowableMinRPM/100);
	   #endif
		/*0x6ED*******************************************************************************************************/
	   #if (__TCS_MU_DETECTION==ENABLE)
		/*CH-95*/		
		CAN_LOG_DATA[136] =(uint8_t) (acc_r_est_mu);
		/*CH-96*/		               
		CAN_LOG_DATA[137] =(uint8_t) (acc_r_est_mu>>8);		
		/*CH-97*/		
		CAN_LOG_DATA[138] =(uint8_t) (TCS_MU_CONT_CMD);
		/*CH-98*/		
		CAN_LOG_DATA[139] =(uint8_t) (lctcsu16mubytorqenterth/100);
		/*CH-99*/		
		CAN_LOG_DATA[140] =(uint8_t) (lctcs1streductcontbymu);
		/*CH-100*/		
		CAN_LOG_DATA[141] =(uint8_t) (lctcs1streductcontbymu>>8);
		
		/*CH-101*/		
		#if (__TCS_2ND_MU_DETECTION	 == ENABLE)
		CAN_LOG_DATA[142] =(uint8_t) (lctcsu16deltslevel/10);
		#else
		CAN_LOG_DATA[142] =(uint8_t) (0);
		#endif

	   #else		
		/*CH-95*/
		tcs_tempW0 = LCTCS_s16ILimitRange(arad_fl,0,255);
		CAN_LOG_DATA[136] =(uint8_t) (tcs_tempW0);
		/*CH-96*/		               
		tcs_tempW0 = LCTCS_s16ILimitRange(arad_fr,0,255);
		CAN_LOG_DATA[137] =(uint8_t) (tcs_tempW0);
		/*CH-97*/		
		tcs_tempW0 = LCTCS_s16ILimitRange(arad_rl,0,255);
		CAN_LOG_DATA[138] =(uint8_t) (tcs_tempW0);
		/*CH-98*/		
		tcs_tempW0 = LCTCS_s16ILimitRange(arad_rr,0,255);
		CAN_LOG_DATA[139] =(uint8_t) (tcs_tempW0);
		/*CH-99*/		
		CAN_LOG_DATA[140] =(uint8_t) 0;
		/*CH-100*/		
		CAN_LOG_DATA[141] =(uint8_t) 0;
		/*CH-101*/		
		CAN_LOG_DATA[142] =(uint8_t) 0;
	   #endif		 
        
	   #if __AX_SENSOR
		/*CH-102*/		
		CAN_LOG_DATA[143] =(uint8_t) (lctcsu8SplitHillDctCntbyAx);
		#else
		/*CH-102*/		
		CAN_LOG_DATA[143] =(uint8_t) 0;
	   #endif

/*Flag*/
/*0x6EE*******************************************************************************************************/
        i=0;
    #if __VDC
        if (MPRESS_BRAKE_ON                 ==1) i|=0x01;
    #else
        if (BLS_PIN                         ==1) i|=0x01;
    #endif
        if (BLS                             ==1) i|=0x02;
        if (ABS_ON                          ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
        if (BTCS_ON                         ==1) i|=0x10;
        if (ETCS_ON                         ==1) i|=0x20;
        if (FTCS_ON                         ==1) i|=0x40;
	  #if __BRK_SIG_MPS_TO_PEDAL_SEN 
        if (lsespu1PedalBrakeOn				==1) i|=0x80;
	  #elif (__4WD||(__4WD_VARIANT_CODE==ENABLE))
        if (ACCEL_SPIN_FZ					==1) i|=0x80;
	  #else
	  #endif
        /*CH-*/
        CAN_LOG_DATA[144] =(uint8_t) (i);

        i=0;
	  #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
        if (BIG_ACCEL_SPIN_FZ				==1) i|=0x01;
	  #endif
        if (ENG_STALL                       ==1) i|=0x02;
        if (CYCLE_2ND                       ==1) i|=0x04;
	  #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))        	
        if (VCA_ON_TCMF		                    ==1) i|=0x08;
	  #endif

    #if __REAR_D
        if (TMP_ERROR_rl                    ==1) i|=0x10;
        if (TMP_ERROR_rr                    ==1) i|=0x20;
    #else
        if (TMP_ERROR_fl                    ==1) i|=0x10;
        if (TMP_ERROR_fr                    ==1) i|=0x20;
    #endif
        if (0				==1) i|=0x40;
        if (lctcsu1FastRPMInc				==1) i|=0x80;
       	/*CH-*/
        CAN_LOG_DATA[145] =(uint8_t) (i);                    

        i = 0;
        if (AV_VL_fl                        ==1) i|=0x01;
        if (HV_VL_fl                        ==1) i|=0x02;
        if (0                    ==1) i|=0x04;
        if (BTCS_fl                         ==1) i|=0x08;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_SECONDARY            ==1) i|=0x10;
      #else
        if (((TCL_DEMAND_rr)|(TCL_DEMAND_fl)) ==1) i|=0x10;
      #endif

      #if __RTA_ENABLE
        if (RTA_FL.RTA_SUSPECT_WL			==1) i|=0x20;
        if (RTA_RL.RTA_SUSPECT_WL           ==1) i|=0x40;
      #else
        if (FL.MINI_SPARE_WHEEL			    ==1) i|=0x20;
        if (RL.MINI_SPARE_WHEEL             ==1) i|=0x40;
      #endif
        if (0			                    ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[146] =(uint8_t) (i);                      

        i=0;
        if (AV_VL_fr                        ==1) i|=0x01;
        if (HV_VL_fr                        ==1) i|=0x02;
        if (0                    ==1) i|=0x04;
        if (BTCS_fr                         ==1) i|=0x08;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_PRIMARY              ==1) i|=0x10;
      #else
        if (((TCL_DEMAND_rl)|(TCL_DEMAND_fr)) ==1) i|=0x10;
      #endif
      #if __RTA_ENABLE
        if (RTA_FR.RTA_SUSPECT_WL			==1) i|=0x20;
        if (RTA_RR.RTA_SUSPECT_WL           ==1) i|=0x40;
      #else
        if (FR.MINI_SPARE_WHEEL			    ==1) i|=0x20;
        if (RR.MINI_SPARE_WHEEL             ==1) i|=0x40;
      #endif
        if (0			                    ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[147] =(uint8_t) (i);                     

        i=0;
        if (AV_VL_rl                        ==1) i|=0x01;
        if (HV_VL_rl                        ==1) i|=0x02;
        if (BTCS_rl                         ==1) i|=0x04;
        if (AUTO_TM                         ==1) i|=0x08;
        if (0                               ==1) i|=0x10;
        if (lctcsu1Low2SplitSuspect			==1) i|=0x20;
        if (ltcsu1HomoMuCtlHavePriority     ==1) i|=0x40;
        if (BTCS_TWO_OFF                    ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[148] =(uint8_t) (i);                    

        i = 0;
        if (AV_VL_rr                        ==1) i|=0x01;
        if (HV_VL_rr                        ==1) i|=0x02;
        if (BTCS_rr                         ==1) i|=0x04;
        if (0			                    ==1) i|=0x08;
        if (YAW_CDC_WORK                    ==1) i|=0x10;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY                ==1) i|=0x20;
        if(S_VALVE_PRIMARY                  ==1) i|=0x40;
      #else
        if(S_VALVE_LEFT                     ==1) i|=0x20;
        if(S_VALVE_RIGHT                    ==1) i|=0x40;
      #endif
        if (ESP_TCS_ON                      ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[149] =(uint8_t) (i);                     

        i = 0;
        if (tc_on_flg                       ==1) i|=0x01;
        if (tc_error_flg                    ==1) i|=0x02;
        if (tcs_error_flg                   ==1) i|=0x04;
        if (fu1VoltageUnderErrDet			==1) i|=0x08;
        if (BTCS_Entry_Inhibition           ==1) i|=0x10;
        if (lctcsu1BTCSHighSideSpinLeft		==1) i|=0x20;
        if (lctcsu1BTCSHighSideSpinRight    ==1) i|=0x40;
        if (ltcsu1TorqLvlRecoverAfterEEC	==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[150] =(uint8_t) (i);                     

        i = 0;
        if (vdc_on_flg                      ==1) i|=0x01;
        if (ESP_ERROR_FLG                   ==1) i|=0x02;
        if (lctcsu1DctSplitHillFlg			==1) i|=0x04;
        if (TORQUE_FAST_RECOVERY_AFTER_ETO  ==1) i|=0x08;
        if (gs_ena							==1) i|=0x10;
        if (BLS_ERROR                       ==1) i|=0x20;
       #if (__RDCM_PROTECTION==ENABLE)
        if (ltu1RDCMProtENGOn               ==1) i|=0x40;
       #else 		
        if (BLS_EEPROM                      ==1) i|=0x40;
       #endif 	
        if (TCS_ON_START                    ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[151] =(uint8_t) (i);                     

/*Flag*/
/*0x6EF*******************************************************************************************************/
        i = 0;
        if ((FL.MINI_SPARE_WHEEL==1)||(FR.MINI_SPARE_WHEEL==1)
            ||(RL.MINI_SPARE_WHEEL==1)||(RR.MINI_SPARE_WHEEL==1)) i|=0x01;
       #if (__CAR_MAKER==GM_KOREA)
        if (TCS_ON                          ==1) i|=0x02;
       #else 	
        if (FUNCTION_LAMP_ON                ==1) i|=0x02;
       #endif 	
        if (0					            ==1) i|=0x04;
        if (Rough_road_detect_vehicle       ==1) i|=0x08;
        if (Rough_road_suspect_vehicle      ==1) i|=0x10;
        if (GAIN_ADD_IN_TURN                ==1) i|=0x20;
        if (YAW_CHANGE_LIMIT_ACT		    ==1) i|=0x40;
		if (GOOD_TARGET_TRACKING_MODE       ==1) i|=0x80;
		/*CH-*/
        CAN_LOG_DATA[152] =(uint8_t) (i);                      

        i = 0;
        if (FTC_DETECT                      ==1) i|=0x01;
        if (TCS_MINI_TIRE_SUSPECT			==1) i|=0x02;
		if (ltcsu1AccROK                  	==1) i|=0x04;
        if (0								==1) i|=0x08;
        if (state_fl                        ==4) i|=0x10;
        if (state_fr                        ==4) i|=0x20;
        if (state_rl                        ==4) i|=0x40;
        if (state_rr                        ==4) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[153] =(uint8_t) (i);                      

        i = 0;
        if (lctcsu1FLampOffByEstimatedBrk   ==1) i|=0x01;
        if (lctcsu1FLampOffByDeltaTorq		==1) i|=0x02;
		if (lctcsu1FLampOffByTorqRate      	==1) i|=0x04;
        if (lctcsu1FLampOffByAx     		==1) i|=0x08;
        if (ltcsu1FastTorqReduction         ==1) i|=0x10;
        if (ltcsu1FastTorqRecovery          ==1) i|=0x20;
        if (lctcsu1SpinOverThreshold        ==1) i|=0x40;
        if (ldtcsu1HillDetectionByAx        ==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[154] =(uint8_t) (i);                              

        i = 0;
        if (fu1ESCDisabledBySW   			==1) i|=0x01;
        if (fu1TCSDisabledBySW				==1) i|=0x02;
		if (lespu1TCS_MSR_C_REQ      		==1) i|=0x04;
        if (EDC_SOLO_ON     				==1) i|=0x08;
        if (In_gear_flag         			==1) i|=0x10;
        if (EDC_SUSTAIN_1SEC          		==1) i|=0x20;
       #if (__TOD==ENABLE)
        if (TOD_ON    						==1) i|=0x40;
	  #else
        if (0								==1) i|=0x40;
       #endif
        if (TCS_STUCK_ON					==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[155] =(uint8_t) (i);                 

        i = 0;
       #if (__TCS_MU_DETECTION==ENABLE)
        if (0   							==1) i|=0x01;
        if (TCS_MU_CONT_FLAG				==1) i|=0x02;
       #else
        if (0   							==1) i|=0x01;
        if (0								==1) i|=0x02;
	  #endif
		if (lctcsu1BackupTorqActivation		==1) i|=0x04;
        if (lctcsu1FLampOffByAccelForce		==1) i|=0x08;
        if (lctcsu1DetectVibration			==1) i|=0x10;
       #if __REAE_D 
        if (lctcsu1BtcsModeByVrefRed_rl		==1) i|=0x20;
        if (lctcsu1BtcsModeByVrefRed_rr		==1) i|=0x40;
    #else
        if (lctcsu1BtcsModeByVrefRed_fl		==1) i|=0x20;
        if (lctcsu1BtcsModeByVrefRed_fr		==1) i|=0x40;       
    #endif
        if (lctcsu1DriverBrakeIntend		==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[156] =(uint8_t) (i);             

        i = 0;
        if (lctcsu1DriverBrakeIntendSus		==1) i|=0x01;
        if (lctcsu1DriverBrakeIntendInvalid	==1) i|=0x02;
		if (0      							==1) i|=0x04;
        if (0     							==1) i|=0x08;
        if (0         						==1) i|=0x10;
        if (0          						==1) i|=0x20;
        if (0    							==1) i|=0x40;
        if (0        						==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[157] =(uint8_t) (i);   

        i = 0;
        if (0					   			==1) i|=0x01;
        if (0								==1) i|=0x02;
		if (0      							==1) i|=0x04;
        if (0     							==1) i|=0x08;
        if (0         						==1) i|=0x10;
        if (0          						==1) i|=0x20;
        if (0    							==1) i|=0x40;
        if (0        						==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[158] =(uint8_t) (i);       
    
        i = 0;
        if (0					   			==1) i|=0x01;
        if (0								==1) i|=0x02;
		if (0      							==1) i|=0x04;
        if (0     							==1) i|=0x08;
        if (0         						==1) i|=0x10;
        if (0          						==1) i|=0x20;
        if (0    							==1) i|=0x40;
        if (0        						==1) i|=0x80;
        /*CH-*/
        CAN_LOG_DATA[159] =(uint8_t) (i);     
#else

        uint8_t i;
/*0x6DC*******************************************************************************************************/        
		/*CH1*/
        CAN_LOG_DATA[0] =(uint8_t) (vrad_crt_fl);
        CAN_LOG_DATA[1] =(uint8_t) (vrad_crt_fl>>8);
		/*CH2*/
        CAN_LOG_DATA[2] =(uint8_t) (vrad_crt_fr);
        CAN_LOG_DATA[3] =(uint8_t) (vrad_crt_fr>>8);
        /*CH3*/
        CAN_LOG_DATA[4] =(uint8_t) (vrad_crt_rl);
        CAN_LOG_DATA[5] =(uint8_t) (vrad_crt_rl>>8);
        /*CH4*/
        CAN_LOG_DATA[6] =(uint8_t) (vrad_crt_rr);
        CAN_LOG_DATA[7] =(uint8_t) (vrad_crt_rr>>8);
/*0x6DD*******************************************************************************************************/                
#if __VDC
        /*CH5*/
        CAN_LOG_DATA[8] =(uint8_t) (vref5);
        CAN_LOG_DATA[9] =(uint8_t) (vref5>>8);
#else
        /*CH5*/
        CAN_LOG_DATA[8] =(uint8_t) (vref);                
        CAN_LOG_DATA[9] =(uint8_t) (vref>>8);
#endif
        if (ETCS_ON==1) { tcs_tempW0 = ETCS_CTL_CMD.lcetcss32CardanTrqCmd/10;}		/*WheelTorqueBased ETCS*/
        else                                { tcs_tempW0 = (int16_t)ENG_VAR;}
		/*CH6*/        
        CAN_LOG_DATA[10] =(uint8_t) (tcs_tempW0);
        CAN_LOG_DATA[11] =(uint8_t) (tcs_tempW0>>8);
		/*CH7*/        
        CAN_LOG_DATA[12] =(uint8_t) (ETCS_TAR_SPIN.lcetcss16TarWhlSpin);
        CAN_LOG_DATA[13] =(uint8_t) (ETCS_TAR_SPIN.lcetcss16TarWhlSpin>>8); 		/*WheelTorqueBased ETCS*/
		/*CH08*/        
		tcs_tempW0 = (int16_t)(ETCS_TRQ_REP_RD_FRIC.lcetcss32TrqRepRdFric/10);
        CAN_LOG_DATA[14] =(uint8_t) (tcs_tempW0);
        CAN_LOG_DATA[15] =(uint8_t) (tcs_tempW0>>8);	  							/*WheelTorqueBased ETCS*/      
/*0x6DE*******************************************************************************************************/                		
		/*CH09*/
        CAN_LOG_DATA[16] =(uint8_t) (delta_yaw_first);
        CAN_LOG_DATA[17] =(uint8_t) (delta_yaw_first>>8); 							/*WheelTorqueBased ETCS*/
		/*CH10*/        
        CAN_LOG_DATA[18] =(uint8_t) (yaw_out);
        CAN_LOG_DATA[19] =(uint8_t) (yaw_out>>8);
		/*CH11*/        
        CAN_LOG_DATA[20] =(uint8_t) (wstr);
        CAN_LOG_DATA[21] =(uint8_t) (wstr>>8);
		/*CH12*/        
        CAN_LOG_DATA[22] =(uint8_t) (ETCS_VEH_ACC.lcetcss16RsltntAcc); 				/*WheelTorqueBased ETCS*/

		/*tcs_tempW0 = LCTCS_s16ILimitRange(mpress/10, 0 , 255 );*/
        tcs_tempW0 = LCTCS_s16ILimitRange(lsesps16AHBGEN3mpress/10, 0 , 255 );
        /*CH13*/
        CAN_LOG_DATA[23] = (uint8_t)(tcs_tempW0);                
/*0x6DF*******************************************************************************************************/                        
		/*CH14*/        
        CAN_LOG_DATA[24] =(uint8_t) (drive_torq);
        CAN_LOG_DATA[25] =(uint8_t) (drive_torq>>8);
		/*CH15*/        
        CAN_LOG_DATA[26] =(uint8_t) (cal_torq);
        CAN_LOG_DATA[27] =(uint8_t) (cal_torq>>8);                
		/*CH16*/                
        CAN_LOG_DATA[28] =(uint8_t) (eng_torq);
        CAN_LOG_DATA[29] =(uint8_t) (eng_torq>>8);
		/*CH17*/        
        CAN_LOG_DATA[30] =(uint8_t) (eng_rpm);  
        CAN_LOG_DATA[31] =(uint8_t) (eng_rpm>>8);        
        
/*0x6E0*******************************************************************************************************/                        
		/*CH18*/        
        CAN_LOG_DATA[32] =(uint8_t) (gear_pos);
		/*CH19*/               
        CAN_LOG_DATA[33] =(uint8_t) (mtp);      
        	/*CH20*/
        CAN_LOG_DATA[34] = (uint8_t)(tcs_msc_target_vol/100);
        /*CH21*/																	/*WheelTorqueBased ETCS*/
        CAN_LOG_DATA[35] = (uint8_t)LCTCS_s16IFindMaximum(FA_TCS.lctcss16EstBrkTorqOnLowMuWhl/10, FA_TCS.lctcss16EstBrkTorqOnLowMuWhl/10);

           /*CH22*/
		CAN_LOG_DATA[36] = (uint8_t)(ETCS_CTL_GAINS.lcetcss16PgainFac); 			/*WheelTorqueBased ETCS*/ 
		   /*CH23*/
        CAN_LOG_DATA[37] = (uint8_t)(ETCS_CTL_GAINS.lcetcss16IgainFac);				/*WheelTorqueBased ETCS*/ 

        /*CH24*/
        CAN_LOG_DATA[38] = (uint8_t)(ltcss16BaseTarWhlSpinDiff);					/*WheelTorqueBased ETCS*/ 
        
        /*CH25*/
        CAN_LOG_DATA[39] = (uint8_t)(ETCS_WHL_SPIN.lcetcss16WhlSpin/2); 			/*WheelTorqueBased ETCS*/        
/*0x6E1*******************************************************************************************************/                                
        
        /*CH26*/
        CAN_LOG_DATA[40] = (uint8_t)(ETCS_CTL_GAINS.lcetcss16Pgain);				/*WheelTorqueBased ETCS*/        
        /*CH27*/
        CAN_LOG_DATA[41] = (uint8_t)(ETCS_CTL_GAINS.lcetcss16Igain);				/*WheelTorqueBased ETCS*/        

		/*CH28*/

        if (BTCS_ON==1)
        {
			tcs_tempW0 = LCTCS_s16ILimitRange((det_alatc/10),0,255);
			CAN_LOG_DATA[42] = (uint8_t)(tcs_tempW0);
        }
        else
        {
			tcs_tempW0 = LCTCS_s16ILimitRange((det_alatm/10),0,255);
			CAN_LOG_DATA[42] = (uint8_t)(tcs_tempW0);
        }

        /*CH29*/
		tcs_tempW0 = LCTCS_s16ILimitRange((nor_alat/10),-125,125);
		CAN_LOG_DATA[43] = (uint8_t)((int8_t)(tcs_tempW0));

        /*CH30*/
        CAN_LOG_DATA[44] = (uint8_t)(ETCS_CTL_CMD.lcetcss32PCtlPor/10);
        CAN_LOG_DATA[45] = (uint8_t)(ETCS_CTL_CMD.lcetcss32PCtlPor/10>>8);       	/*WheelTorqueBased ETCS*/

        /*CH31*/
        CAN_LOG_DATA[46] = (uint8_t)(ETCS_CTL_CMD.lcetcss32ICtlPor/10);
        CAN_LOG_DATA[47] = (uint8_t)(ETCS_CTL_CMD.lcetcss32ICtlPor/10>>8);        		/*WheelTorqueBased ETCS*/
        
/*0x6E2*******************************************************************************************************/
		/*CH32*/
		CAN_LOG_DATA[48] = (uint8_t)(ETCS_LOW_WHL_SPN.lcetcss16LowWhlSpnCnt);		/*WheelTorqueBased ETCS*/
		/*CH33*/
        CAN_LOG_DATA[49] = (uint8_t)(latcss16TarPreGrdtMd);							/*WheelTorqueBased ETCS*/
        /*CH34*/
        if ((FA_TCS.lctcsu1BrkCtlActAxle==1)&&(FA_TCS.ltcsu1ModOfAsymSpnCtl!=S16TCSAsymSpinFwdCtlMode)) 
        {
        	tcs_tempW0 = FA_TCS.lctcss16FFBrkTorq4Asym/10;
        	tcs_tempW2 = FA_TCS.lctcss16PeffOnBsTBrkTorq4Asym/10;
        	tcs_tempW4 = FA_TCS.lctcss16IeffOnBsTBrkTorq4Asym/10;
        	tcs_tempW6 = FL_TCS.lctcss16BsTarBrkTorq;
        	tcs_tempW7 = FR_TCS.lctcss16BsTarBrkTorq;
        }
        else 
        {
        	tcs_tempW0 = 0;
        	tcs_tempW2 = 0;
        	tcs_tempW4 = 0;
        	tcs_tempW6 = 0;
        	tcs_tempW7 = 0;
        }
        
        if ((RA_TCS.lctcsu1BrkCtlActAxle==1)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl!=S16TCSAsymSpinFwdCtlMode)) 
        {
        	tcs_tempW1 = RA_TCS.lctcss16FFBrkTorq4Asym/10;
        	tcs_tempW3 = RA_TCS.lctcss16PeffOnBsTBrkTorq4Asym/10;
        	tcs_tempW5 = RA_TCS.lctcss16IeffOnBsTBrkTorq4Asym/10;
        	tcs_tempW8 = RL_TCS.lctcss16BsTarBrkTorq;
        	tcs_tempW9 = RR_TCS.lctcss16BsTarBrkTorq;
        }
        else 
        {
        	tcs_tempW1 = 0;
        	tcs_tempW3 = 0;
        	tcs_tempW5 = 0;
        	tcs_tempW8 = 0;
        	tcs_tempW9 = 0;
        }
        CAN_LOG_DATA[50] = (uint8_t)(tcs_tempW0); /*FA Base torque*/
        /*CH35*/
        CAN_LOG_DATA[51] = (uint8_t)(tcs_tempW1); /*RA Base torque*/
        /*CH36*/
        CAN_LOG_DATA[52] = (uint8_t)(tcs_tempW2); /*FA P Effect torque*/ 
        /*CH37*/
        CAN_LOG_DATA[53] = (uint8_t)(tcs_tempW3); /*RA P Effect torque*/
        /*CH38*/
        CAN_LOG_DATA[54] = (uint8_t)(tcs_tempW4); /*FA I Effect torque*/
        /*CH39*/
        CAN_LOG_DATA[55] = (uint8_t)(tcs_tempW5); /*RA I Effect torque*/     

/*0x6E3*******************************************************************************************************/
        /*CH40*/
        CAN_LOG_DATA[56] = (uint8_t)(tcs_tempW6);    
        CAN_LOG_DATA[57] = (uint8_t)(tcs_tempW6>>8); /*FL Target brake torque*/
        /*CH41*/
        CAN_LOG_DATA[58] = (uint8_t)(tcs_tempW7);
        CAN_LOG_DATA[59] = (uint8_t)(tcs_tempW7>>8); /*FR Target brake torque*/
        /*CH42*/
        CAN_LOG_DATA[60] = (uint8_t)(tcs_tempW8);
        CAN_LOG_DATA[61] = (uint8_t)(tcs_tempW8>>8); /*RL Target brake torque*/
        /*CH43*/
        CAN_LOG_DATA[62] = (uint8_t)(tcs_tempW9); 
        CAN_LOG_DATA[63] = (uint8_t)(tcs_tempW9>>8);  /*RR Target brake torque*/
        
/*0x6E4*******************************************************************************************************/
        if ((FA_TCS.lctcsu1BrkCtlActAxle==1)&&(FA_TCS.ltcsu1ModOfAsymSpnCtl!=S16TCSAsymSpinFwdCtlMode)) 
        {
        	tcs_tempW0 = FL_TCS.lctcss16TarWhlPre/10;
        	tcs_tempW1 = FR_TCS.lctcss16TarWhlPre/10;
        	tcs_tempW4 = FA_TCS.lctcss16BrkCtlErr/4;
        }
        else 
        {
        	tcs_tempW0 = 0;
        	tcs_tempW1 = 0;
        	tcs_tempW4 = 0;
        }

        if ((RA_TCS.lctcsu1BrkCtlActAxle==1)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl!=S16TCSAsymSpinFwdCtlMode)) 
        {
        	tcs_tempW2 = RL_TCS.lctcss16TarWhlPre/10;
        	tcs_tempW3 = RR_TCS.lctcss16TarWhlPre/10;
        	tcs_tempW5 = RA_TCS.lctcss16BrkCtlErr/4;
        }
        else 
        {
        	tcs_tempW2 = 0;
        	tcs_tempW3 = 0;
        	tcs_tempW5 = 0;
        }

        /*CH44*/
        CAN_LOG_DATA[64] = (uint8_t)(tcs_tempW0); /*FL Target pressure*/
        /*CH45*/
        CAN_LOG_DATA[65] = (uint8_t)(tcs_tempW1); /*FR Target pressure*/
        /*CH46*/
        CAN_LOG_DATA[66] = (uint8_t)(tcs_tempW2); /*RL Target pressure*/
        /*CH47*/
        CAN_LOG_DATA[67] = (uint8_t)(tcs_tempW3); /*RR Target pressure*/
        /*CH48*/
        CAN_LOG_DATA[68] = (uint8_t)(tcs_tempW4);  /*FA Brake Error */
        /*CH49*/
        CAN_LOG_DATA[69] = (uint8_t)(tcs_tempW5);  /*RA Brake Error */
        /*CH50*/
        CAN_LOG_DATA[70] = (uint8_t)(latcss16PriTcTargetPress/10);						/*WheelTorqueBased ETCS*/
        /*CH51*/
       	CAN_LOG_DATA[71] = (uint8_t)(latcss16SecTcTargetPress/10);						/*WheelTorqueBased ETCS*/

/*0x6E5*******************************************************************************************************/        
        i=0;
    #if __VDC
        if (MPRESS_BRAKE_ON                 ==1) i|=0x01;
    #else
        if (BLS_PIN                         ==1) i|=0x01;
    #endif
        if (BLS                             ==1) i|=0x02;
        if (ABS_ON                          ==1) i|=0x04;
        if (ETCS_CTL_ERR.lcetcsu1CtlErrNeg  ==1) i|=0x08; 							/*WheelTorqueBased ETCS*/
        if (ETCS_CTL_ACT.lcetcsu1CtlAct     ==1) i|=0x10; 							/*WheelTorqueBased ETCS*/
        if (ETCS_CYL_1ST.lcetcsu1Cyl1st     ==1) i|=0x20; 							/*WheelTorqueBased ETCS*/
        if (ETCS_ENG_STALL.lcetcsu1EngStall ==1) i|=0x40; 							/*WheelTorqueBased ETCS*/
        if (ETCS_CTL_ERR.lcetcsu1CtlErrPos  ==1) i|=0x80; 							/*WheelTorqueBased ETCS*/
	    /*CH52*/
        CAN_LOG_DATA[72] =(uint8_t) (i);

        i=0;
	  #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
        if (VCA_ON_TCMF		                    ==1) i|=0x01;
	  #else
	    if (TCS_STUCK_ON                    ==1) i|=0x01;
	  #endif

    #if __REAR_D
        if (TMP_ERROR_rl                    ==1) i|=0x02;
        if (TMP_ERROR_rr                    ==1) i|=0x04;
    #else
        if (TMP_ERROR_fl                    ==1) i|=0x02;
        if (TMP_ERROR_fr                    ==1) i|=0x04;
    #endif
        if (ETCS_TAR_SPIN.lcetcsu1TarSpnDec 					==1) i|=0x08; 		/*WheelTorqueBased ETCS*/
        if (ETCS_IVAL_RST.lcetcsu1RstIVal						==1) i|=0x10; 		/*WheelTorqueBased ETCS*/
        if (ETCS_SPLIT_HILL.lcetcsu1SplitHillDct 				==1) i|=0x20; 		/*WheelTorqueBased ETCS*/
        if (ETCS_GAIN_GEAR_SHFT.lcetcsu1GainChngInGearShft		==1) i|=0x40; 		/*WheelTorqueBased ETCS*/
        if (ETCS_GAIN_GEAR_SHFT.lcetcsu1GainTrnsAftrGearShft	==1) i|=0x80; 		/*WheelTorqueBased ETCS*/        
        /*CH53*/
        CAN_LOG_DATA[73] =(uint8_t) (i);

        i = 0;
        if (AV_VL_fl                        ==1) i|=0x01;
        if (HV_VL_fl                        ==1) i|=0x02;
        if (BTCS_fl                         ==1) i|=0x04;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_SECONDARY            ==1) i|=0x08;
      #else
        if (((TCL_DEMAND_rr)|(TCL_DEMAND_fl)) ==1) i|=0x08;
      #endif

      #if __RTA_ENABLE
        if (RTA_FL.RTA_SUSPECT_WL			==1) i|=0x10;
        if (RTA_RL.RTA_SUSPECT_WL           ==1) i|=0x20;
      #else
        if (FL.MINI_SPARE_WHEEL			    ==1) i|=0x10;
        if (RL.MINI_SPARE_WHEEL             ==1) i|=0x20;
      #endif
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY                ==1) i|=0x40;
        if(S_VALVE_PRIMARY                  ==1) i|=0x80;
      #else
        if(S_VALVE_LEFT                     ==1) i|=0x40;
        if(S_VALVE_RIGHT                    ==1) i|=0x80;
      #endif      
        /*CH54*/
        CAN_LOG_DATA[74] =(uint8_t) (i);

        i=0;
        if (AV_VL_fr                        ==1) i|=0x01;
        if (HV_VL_fr                        ==1) i|=0x02;
        if (BTCS_fr                         ==1) i|=0x04;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_PRIMARY              ==1) i|=0x08;
      #else
        if (((TCL_DEMAND_rl)|(TCL_DEMAND_fr)) ==1) i|=0x08;
      #endif
      #if __RTA_ENABLE
        if (RTA_FR.RTA_SUSPECT_WL			==1) i|=0x10;
        if (RTA_RR.RTA_SUSPECT_WL           ==1) i|=0x20;
      #else
        if (FR.MINI_SPARE_WHEEL			    ==1) i|=0x10;
        if (RR.MINI_SPARE_WHEEL             ==1) i|=0x20;
      #endif
        if ((FL.MINI_SPARE_WHEEL==1)||(FR.MINI_SPARE_WHEEL==1)
            ||(RL.MINI_SPARE_WHEEL==1)||(RR.MINI_SPARE_WHEEL==1)) i|=0x40;
        if (lcetcsu1FuncLampOn              ==1) i|=0x80; 							/*WheelTorqueBased ETCS*/
        /*CH55*/
        CAN_LOG_DATA[75] =(uint8_t) (i);

        i=0;
        if (AV_VL_rl                        ==1) i|=0x01;
        if (HV_VL_rl                        ==1) i|=0x02;
        if (BTCS_rl                         ==1) i|=0x04;
        if (AV_VL_rr                        ==1) i|=0x08;
        if (HV_VL_rr                        ==1) i|=0x10;
        if (BTCS_rr                         ==1) i|=0x20;
        if (YAW_CDC_WORK                    ==1) i|=0x40;
        if (ETCS_CTL_GAINS.lcetcsu1DelYawDivrg 			==1) i|=0x80; 				/*WheelTorqueBased ETCS*/
        /*CH56*/
        CAN_LOG_DATA[76] =(uint8_t) (i);

		
        i = 0;
        if (tc_on_flg                       ==1) i|=0x01;
        if (tc_error_flg                    ==1) i|=0x02;
        if (tcs_error_flg                   ==1) i|=0x04;
        if (ETCS_REQ_VCA.lcetcsu1ReqVcaTrqAct			==1) i|=0x08; 				/*WheelTorqueBased ETCS*/
        if (BTCS_Entry_Inhibition           ==1) i|=0x10;
        if (vdc_on_flg                      ==1) i|=0x20;
        if (ESP_ERROR_FLG                   ==1) i|=0x40;
        if (lcetcsu1VehVibDct				==1) i|=0x80;							/*WheelTorqueBased ETCS*/
        /*CH57*/
        CAN_LOG_DATA[77] =(uint8_t) (i);

        i = 0;
        if (ETCS_H2L.lcetcsu1HiToLw			==1) i|=0x01; 							/*WheelTorqueBased ETCS*/
        if ((gs_ena==1)|(lctcsu1GearShiftFlag==1))	i|=0x02;
        if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle)) i|=0x04;
        if (ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqFA	==1) i|=0x08; 				/*WheelTorqueBased ETCS*/
        if (ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqRA	==1) i|=0x10; 				/*WheelTorqueBased ETCS*/
        if (FA_TCS.lctcsu1SymSpinDctFlg     ==1) i|=0x20;
        if (RA_TCS.lctcsu1SymSpinDctFlg     ==1) i|=0x40;
        if ((FA_TCS.lctcsu1OK2AdaptFF==1)||(RA_TCS.lctcsu1OK2AdaptFF==1)) i|=0x80;
        /*CH58*/
        CAN_LOG_DATA[78] =(uint8_t) (i);

        i = 0;
        if (FA_TCS.lctcsu1Any2HghMuWhlUnstb     ==1) i|=0x01;
        if (RA_TCS.lctcsu1Any2HghMuWhlUnstb     ==1) i|=0x02;
        if (FA_TCS.lctcsu1HghMuWhlUnstb2Any     ==1) i|=0x04;
        if (RA_TCS.lctcsu1HghMuWhlUnstb2Any     ==1) i|=0x08;
        if (FA_TCS.lctcsu1BrkCtlActAxle     ==1) i|=0x10;
        if (RA_TCS.lctcsu1BrkCtlActAxle     ==1) i|=0x20;
        if (FA_TCS.lctcsu1BrkCtlFadeOutModeFlag     ==1) i|=0x40;
        if (RA_TCS.lctcsu1BrkCtlFadeOutModeFlag     ==1) i|=0x80;
        /*CH59*/
        CAN_LOG_DATA[79] =(uint8_t) (i);
#endif
    }

#if __EVP
void    LSEVP_vCallEVPLogData(void)
{
	uint8_t i;
        CAN_LOG_DATA[0] = (uint8_t)(system_loop_counter);
        CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);              /*CH1*/ 
        CAN_LOG_DATA[2] = (uint8_t)(vrad_fl);                             
        CAN_LOG_DATA[3] = (uint8_t)(vrad_fl>>8);                          /*CH2*/ 
        CAN_LOG_DATA[4] = (uint8_t)(vrad_fr);                             
        CAN_LOG_DATA[5] = (uint8_t)(vrad_fr>>8);                          /*CH3*/ 
        CAN_LOG_DATA[6] = (uint8_t)(vrad_rl);                             
        CAN_LOG_DATA[7] = (uint8_t)(vrad_rl>>8);                          /*CH4 - Message 1 */ 
        CAN_LOG_DATA[8] = (uint8_t)(vrad_rr);                             
        CAN_LOG_DATA[9] = (uint8_t)(vrad_rr>>8);                          /*CH5*/ 
	#if __VDC                                                           
        CAN_LOG_DATA[10] = (uint8_t)(vref5);                              
        CAN_LOG_DATA[11] = (uint8_t)(vref5>>8);                           /*CH6*/ 
	#else                                                               
        CAN_LOG_DATA[10] = (uint8_t)(vref);                               
        CAN_LOG_DATA[11] = (uint8_t)(vref>>8);                            /*CH6*/ 
	#endif                                                              
        CAN_LOG_DATA[12] = (uint8_t)(drive_torq);                         
        CAN_LOG_DATA[13] = (uint8_t)(drive_torq>>8);                      /*CH7*/              
        CAN_LOG_DATA[14] = (uint8_t)(cal_torq);                                               
        CAN_LOG_DATA[15] = (uint8_t)(cal_torq>>8);                        /*CH8 - Message 2 */
        CAN_LOG_DATA[16] = (uint8_t)(eng_torq);                                               
        CAN_LOG_DATA[17] = (uint8_t)(eng_torq>>8);                        /*CH9*/             
        CAN_LOG_DATA[18] = (uint8_t)(eng_rpm);                                                
        CAN_LOG_DATA[19] = (uint8_t)(eng_rpm>>8);                         /*CH10*/            
        CAN_LOG_DATA[20] = (uint8_t)(yaw_out);                            
        CAN_LOG_DATA[21] = (uint8_t)(yaw_out>>8);				            /*CH11*/
        CAN_LOG_DATA[22] = (uint8_t)(rf2);                                
        CAN_LOG_DATA[23] = (uint8_t)(rf2>>8);					            /*CH12 - Message 3*/
        CAN_LOG_DATA[24] = (uint8_t)(wstr);                               
        CAN_LOG_DATA[25] = (uint8_t)(wstr>>8);				            /*CH13*/
    	CAN_LOG_DATA[26] = (uint8_t)(alat);                               
    	CAN_LOG_DATA[27] = (uint8_t)(alat>>8);				            /*CH14*/
    	CAN_LOG_DATA[28] = (uint8_t)(det_alatm);                          
    	CAN_LOG_DATA[29] = (uint8_t)(det_alatm>>8);      		            /*CH15*/
        CAN_LOG_DATA[30] = (uint8_t)(delta_yaw_first);                    
        CAN_LOG_DATA[31] = (uint8_t)(delta_yaw_first>>8);		            /*CH16 - Message 4 */
	#if __BRK_SIG_MPS_TO_PEDAL_SEN
        CAN_LOG_DATA[32] = (uint8_t)(lsesps16EstBrkPressByBrkPedal);                             
        CAN_LOG_DATA[33] = (uint8_t)(lsesps16EstBrkPressByBrkPedal>>8);   /*CH17*/	
    #else    
      #if __VDC                                                           
        CAN_LOG_DATA[32] = (uint8_t)(mpress);                             
        CAN_LOG_DATA[33] = (uint8_t)(mpress>>8);				            /*CH17*/
      #else                                                               
        CAN_LOG_DATA[32] = 0;                                           
        CAN_LOG_DATA[33] = 0;								            /*CH17*/
      #endif   
    #endif    	                                                        
        CAN_LOG_DATA[34] = (uint8_t)(levps16VacuumSignal);                                  
        CAN_LOG_DATA[35] = (uint8_t)(levps16VacuumSignal>>8);	            /*CH18*/ 
        CAN_LOG_DATA[36] = (uint8_t)(levpu16EVPPumpOffTimer);                           
        CAN_LOG_DATA[37] = (uint8_t)(levpu16EVPPumpOffTimer>>8);			/*CH19*/ 	
        CAN_LOG_DATA[38] = (uint8_t)(levpu16EVPPumpOnRunTimer);                           
        CAN_LOG_DATA[39] = (uint8_t)(levpu16EVPPumpOnRunTimer>>8);		/*CH20 - Message 5 */ 
        CAN_LOG_DATA[40] = (uint8_t)(levpu16EVPAccumPumpOnSubTimer2);                           
        CAN_LOG_DATA[41] = (uint8_t)(levpu16EVPAccumPumpOnSubTimer2>>8);	/*CH21*/ 
        CAN_LOG_DATA[42] = (uint8_t)(levpu16EVPAccumPumpOnTimer);                           
        CAN_LOG_DATA[43] = (uint8_t)(levpu16EVPAccumPumpOnTimer>>8);		/*CH22*/ 
        CAN_LOG_DATA[44] = (uint8_t)(levpu16ESPEngStartTimer);                           
        CAN_LOG_DATA[45] = (uint8_t)(levpu16ESPEngStartTimer>>8);			/*CH23*/ 
        CAN_LOG_DATA[46] = (uint8_t)(ebd_filt_grv);                           
        CAN_LOG_DATA[47] = (uint8_t)(ebd_filt_grv>>8);			        /*CH24 - Message 6 */ 
        CAN_LOG_DATA[48] = (uint8_t)(levps16BaroMAPPrsDiff);                                  
        CAN_LOG_DATA[49] = (uint8_t)(levps16BaroMAPPrsDiff>>8);		    /*CH25*/
        CAN_LOG_DATA[50] = (uint8_t)(lespu16BaroPressAbs);                                  
        CAN_LOG_DATA[51] = (uint8_t)(lespu16BaroPressAbs>>8);		        /*CH26*/ 
        CAN_LOG_DATA[52] = (uint8_t)(lespu16EngManfldAbsPrs);                                  
        CAN_LOG_DATA[53] = (uint8_t)(lespu16EngManfldAbsPrs>>8);	        /*CH27*/ 	
        CAN_LOG_DATA[54] = (uint8_t)(0);                                  
        CAN_LOG_DATA[55] = (uint8_t)(0>>8);					            /*CH28 - Message 7 */ 
        CAN_LOG_DATA[56] = (uint8_t)(mtp);						        /*CH29*/ 
        CAN_LOG_DATA[57] = (uint8_t)((int16_t)(lesps16EMS_TQFR)/10);	        /*CH30*/
        CAN_LOG_DATA[58] = (uint8_t)(levpu8EVPPumpTurnOnCnt);		        /*CH31*/                
        CAN_LOG_DATA[59] = (uint8_t)(levpu8EVPAccumPumpOnSubTimer1);      /*CH32*/
        CAN_LOG_DATA[60] = (uint8_t)(0);		            				/*CH33*/
        CAN_LOG_DATA[61] = (uint8_t)(0);	           						/*CH34*/
        CAN_LOG_DATA[62] = (uint8_t)(fu8VacuumModeTransition);		    /*CH35*/
        CAN_LOG_DATA[63] = (uint8_t)(lespu8SysPwrMd);			            /*CH36 - Message 8 */                                
        CAN_LOG_DATA[64] = (uint8_t)(levpu8EVPflags);			            /*CH37*/
        CAN_LOG_DATA[65] = (uint8_t)(0);						            /*CH38*/
        CAN_LOG_DATA[66] = (uint8_t)(0);						            /*CH39*/
        CAN_LOG_DATA[67] = (uint8_t)(0);						            /*CH40*/
        CAN_LOG_DATA[68] = (uint8_t)(0);						            /*CH41*/
        CAN_LOG_DATA[69] = (uint8_t)(0);						            /*CH42*/
        
        i=0;
        if (BLS_PIN             		==1) i|=0x01; 
        if (BLS                 		==1) i|=0x02; 
        if (ABS_fz              		==1) i|=0x04; 
        if (EBD_RA              		==1) i|=0x08; 
        if (BTCS_ON             		==1) i|=0x10;
        if (ETCS_ON             		==1) i|=0x20;
        if (FTCS_ON             		==1) i|=0x40;
        if (ESP_TCS_ON		         	==1) i|=0x80;
        CAN_LOG_DATA[70] = i;									 /*CH43*/

        i=0;
        if (AV_VL_fl            		==1) i|=0x01;
        if (HV_VL_fl            		==1) i|=0x02;
        if (ABS_fl              		==1) i|=0x04;
        if (BTCS_fl                     ==1) i|=0x08;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_SECONDARY            	==1) i|=0x10;
      #else
        if (((TCL_DEMAND_rr)|(TCL_DEMAND_fl)) 	==1) i|=0x10;
      #endif
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY            ==1) i|=0x20;
        if(S_VALVE_PRIMARY              ==1) i|=0x40;
      #else
        if(S_VALVE_LEFT                 ==1) i|=0x20;
        if(S_VALVE_RIGHT                ==1) i|=0x40;
      #endif       
        #if __EVP
        	i|=0x80;
        #else
        	i&=0xBF;
        #endif      
        CAN_LOG_DATA[71] = i;									 /*CH44 - Message 9 */

        i=0;
        if (AV_VL_fr            		==1) i|=0x01;
        if (HV_VL_fr            		==1) i|=0x02;
        if (ABS_fr              		==1) i|=0x04;
        if (BTCS_fr                     ==1) i|=0x08;
      #if (__SPLIT_TYPE==1)
        if (TCL_DEMAND_PRIMARY              	==1) i|=0x10;
      #else
        if (((TCL_DEMAND_rl)|(TCL_DEMAND_fr)) 	==1) i|=0x10;
      #endif
        if (lespu1TCS_TCS_REQ			==1) i|=0x20;
		if (vdc_on_flg					==1) i|=0x40;
	  #if __VDC		
        if (MPRESS_BRAKE_ON				==1) i|=0x80;
      #else  	
        if (0							==1) i|=0x80;        	
      #endif  	
        CAN_LOG_DATA[72] = i;									 /*CH45*/

        i=0;
        if (AV_VL_rl            		==1) i|=0x01;
        if (HV_VL_rl            		==1) i|=0x02;
        if (ABS_rl              		==1) i|=0x04;
        if (BTCS_rl						==1) i|=0x08;
        if (tc_on_flg					==1) i|=0x10;
        if (tc_error_flg				==1) i|=0x20;
        if (tcs_error_flg				==1) i|=0x40;
        if (ESP_ERROR_FLG				==1) i|=0x80;
        CAN_LOG_DATA[73] = i;									 /*CH46*/

        i = 0;
        if (AV_VL_rr            		==1) i|=0x01;
        if (HV_VL_rr            		==1) i|=0x02;
        if (ABS_rr              		==1) i|=0x04;
        if (BTCS_rr						==1) i|=0x08;
        if (fu1VoltageUnderErrDet		==1) i|=0x10;
        if (fu1VoltageOverErrDet		==1) i|=0x20;
        if (fu1OnDiag          			==1) i|=0x40;
        if (init_end_flg       			==1) i|=0x80;
        CAN_LOG_DATA[74] = i;									 /*CH47*/

        i = 0;
        if (fu1ECUHwErrDet          	==1) i|=0x01;
        if (fu1BLSErrDet          		==1) i|=0x02;
        if (fu1MCPErrorDet          	==1) i|=0x04;
  	  #if __BRK_SIG_MPS_TO_PEDAL_SEN        	
        if (lsespu1BrkAppSenInvalid		==1) i|=0x08;
        if (lsespu1PedalBrakeOn			==1) i|=0x10;
      #else  	
        if (0							==1) i|=0x08;
        if (0							==1) i|=0x10;
      #endif  	        	
        if (lespu1EngRunAtv        		==1) i|=0x20;
        if (lespu1BaroPressAbsV    		==1) i|=0x40;
        if (lespu1EngManfldAbsPrsV  	==1) i|=0x80;
        CAN_LOG_DATA[75] = i;									 /*CH48*/ 
                
        i = 0;
        if (levpu1EVPPumpOn          	==1) i|=0x01;
        if (levpu1BLSActPrev         	==1) i|=0x02;
        if (levpu1BLSAct           		==1) i|=0x04;
        if (levpu1EVPInitPumpOn  		==1) i|=0x08;
        if (lespu1CATLightOff 			==1) i|=0x10;
        if (fu1VoltageLowErrDet			==1) i|=0x20;
        if (feu1VacuumPumpSystemErrFlg	==1) i|=0x40;
        if (feu1VacuumPumpRelayShortErrFlg 	==1) i|=0x80;
        CAN_LOG_DATA[76] = i;									 /*CH49*/     

        i = 0;
        if (feu1VacuumPumpRelayOpenErrFlg   ==1) i|=0x01;
        if (fu1MainCanLineErrDet        ==1) i|=0x02;
        if (fu1EMSTimeOutErrDet         ==1) i|=0x04;
        if (fu1VacuumMotorDrive			==1) i|=0x08;
        if (0 			                ==1) i|=0x10; /*VACCUM_RELAY_DRV, firmware 처리 안되어 있음*/
        if (0         					==1) i|=0x20;
        if (0           				==1) i|=0x40;
        if (0       					==1) i|=0x80;
        CAN_LOG_DATA[77] = i;									 /*CH50*/     
        
        i = 0;
        if (0   						==1) i|=0x01;
        if (0        					==1) i|=0x02;
        if (0         					==1) i|=0x04;
        if (0  							==1) i|=0x08;
        if (0 							==1) i|=0x10;
        if (0         					==1) i|=0x20;
        if (0           				==1) i|=0x40;
        if (0       					==1) i|=0x80;
        CAN_LOG_DATA[78] = i;									 /*CH51*/  
        
        i = 0;
        if (0   						==1) i|=0x01;
        if (0        					==1) i|=0x02;
        if (0         					==1) i|=0x04;
        if (0  							==1) i|=0x08;
        if (0 							==1) i|=0x10;
        if (0         					==1) i|=0x20;
        if (0           				==1) i|=0x40;
        if (0       					==1) i|=0x80;
        CAN_LOG_DATA[79] = i;									 /*CH52 - Message 10 */                       
}
#endif

void LSESP_vCallROPLogData(void) /* '06.12.20 ROP View Update ...  */
{
	uint8_t i;
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);                    //ch1
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                //ch2
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                //ch3
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                //ch4
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                //ch5
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);                     //ch6
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);

esp_tempW9= alat/10;

if(esp_tempW9 > 120) esp_tempW9 =120;
else if(esp_tempW9< (-120)) esp_tempW9 = -120;
else

    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);   //ch7
    CAN_LOG_DATA[13] =(uint8_t) (inhibition_number    =INHIBITION_INDICATOR());//ch8
    CAN_LOG_DATA[14] =(uint8_t) (vref5);                              //ch9
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

    CAN_LOG_DATA[16] =(uint8_t) ((int8_t) pd_ymc_gain);//(moment_q_front<-120)?-120:(int8_t)moment_q_front;   //ch10

esp_tempW0 = delta_moment_beta/100;
    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);//(moment_q_rear<-120)?-120:(int8_t)moment_q_rear;    //ch11

#if __PRESS_EST_ACTIVE
    CAN_LOG_DATA[18] =(uint8_t) (FL.u8_Estimated_Active_Press);                       //ch12
    CAN_LOG_DATA[19] =(uint8_t) (FR.u8_Estimated_Active_Press);   //ch13
    CAN_LOG_DATA[20] =(uint8_t) (RL.u8_Estimated_Active_Press);//(uint8_t)flags_fl;                      //ch14
    CAN_LOG_DATA[21] =(uint8_t) (RR.u8_Estimated_Active_Press);
#else
    CAN_LOG_DATA[18] = (uint8_t)flags_fl;
    CAN_LOG_DATA[19] = (uint8_t)flags_fr;
    CAN_LOG_DATA[20] = (uint8_t)flags_rl;
    CAN_LOG_DATA[21] = (uint8_t)flags_rr;
#endif

    CAN_LOG_DATA[22] =(uint8_t) (flags_rl);//esp_tempW9;//(uint8_t)flags_rl;                         //ch16
    CAN_LOG_DATA[23] =(uint8_t) (flags_rr);//P_control_activation_rl;//(int8_t)q_front_enter_gain;//(uint8_t)flags_rr;                         //ch17

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if      (SLIP_CONTROL_NEED_FL==1)      esp_tempW0=FL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_FR==1)      esp_tempW0=FR_ESC.true_slip_target;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL==1)      esp_tempW1=RL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_RR==1)      esp_tempW1=RR_ESC.true_slip_target;
    else                                esp_tempW1=0;
#else
    if      (SLIP_CONTROL_NEED_FL==1)      esp_tempW0=true_slip_target_fl;
    else if (SLIP_CONTROL_NEED_FR==1)      esp_tempW0=true_slip_target_fr;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL==1)      esp_tempW1=true_slip_target_rl;
    else if (SLIP_CONTROL_NEED_RR==1)      esp_tempW1=true_slip_target_rr;
    else                                esp_tempW1=0;
#endif

    CAN_LOG_DATA[24] =(uint8_t) (esp_tempW0);                         //ch18
    CAN_LOG_DATA[25] =(uint8_t) (esp_tempW0>>8);
    CAN_LOG_DATA[26] =(uint8_t) (moment_q_front_rop); //esp_tempW1;                           //ch19
    CAN_LOG_DATA[27] =(uint8_t) (moment_q_front_rop>>8);


    CAN_LOG_DATA[28] =(uint8_t) (del_target_slip_fl);//delta_yaw_third_ems;                   //ch20
    CAN_LOG_DATA[29] =(uint8_t) (del_target_slip_fl>>8);//delta_yaw_third_ems>>8);

    CAN_LOG_DATA[30] =(uint8_t) (0);//drive_torq;                            //ch21
    CAN_LOG_DATA[31] =(uint8_t) (0>>8);//drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (Beta_MD);//cal_torq;                         //ch22
    CAN_LOG_DATA[33] =(uint8_t) (Beta_MD>>8);//cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (0);//eng_torq;                          //ch23
    CAN_LOG_DATA[35] =(uint8_t) (0>>8);//eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (del_target_slip_fr);//eng_rpm;                               //ch24
    CAN_LOG_DATA[37] =(uint8_t) (del_target_slip_fr>>8);//eng_rpm>>8);

    CAN_LOG_DATA[38] =(uint8_t) ((int8_t)TTL_time); //gear_pos;//P_control_activation_rr;//gear_pos;                            //ch25
    CAN_LOG_DATA[39] =(uint8_t) (RI_max); // mtp;                                 //ch26

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               //ch27
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

    CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch28
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                    //ch29
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
    CAN_LOG_DATA[46] =(uint8_t) (drive_torq);//slip_m_fl_current;//Beta_MD;                               //ch30
    CAN_LOG_DATA[47] =(uint8_t) (drive_torq>>8);//slip_m_fl_current>>8);//Beta_MD>>8);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 //ch31
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                           //ch32
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);//esp_mu; // nosign_delta_yaw_first;                     //ch33
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8); // nosign_delta_yaw_first>>8);
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc);//slip_m_rl_current;//;                   //ch34
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc>>8);//slip_m_rl_current>>8);//>>8);

    CAN_LOG_DATA[56] =(uint8_t) (roll_angle_rate); //det_alatm;                               //ch35
    CAN_LOG_DATA[57] =(uint8_t) (roll_angle_rate>>8);
    CAN_LOG_DATA[58] =(uint8_t) (cal_torq);//slip_m_fr_current;//esp_mu;                              //ch36
    CAN_LOG_DATA[59] =(uint8_t) (cal_torq>>8);//slip_m_fr_current>>8);//esp_mu>>8);

  if(SLIP_CONTROL_NEED_RL==1)
  {
    esp_tempW9 = 0;
  }
  else if(SLIP_CONTROL_NEED_RR==1)
  {
    esp_tempW9 = 0;
  }
  else
  {
    esp_tempW9 = 0;
  }
    CAN_LOG_DATA[60] =(uint8_t) (roll_angle); //esp_tempW9;//det_alatm;                           //ch37
    CAN_LOG_DATA[61] =(uint8_t) (roll_angle>>8);//det_alatm>>8);


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  if(SLIP_CONTROL_NEED_FL==1)
  {
    esp_tempW8 = FL_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_FR==1)
  {
    esp_tempW8 = FR_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_RL==1)
  {
        esp_tempW8 = RL_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_RR==1)
  {
        esp_tempW8 = RR_ESC.slip_control;
  }
  else
  {
        esp_tempW8 = 0;
  }
#else
  if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
  {
    esp_tempW8 = slip_control_front;
  }
  else if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
  {
        esp_tempW8 = slip_control_rear;
  }
  else
  {
        esp_tempW8 = 0;
  }
#endif

    CAN_LOG_DATA[62] =(uint8_t) (esp_tempW8);//target_pressure_rr;//det_alatc;                            //ch38
    CAN_LOG_DATA[63] =(uint8_t) (esp_tempW8>>8);//target_pressure_rr>>8);//det_alatc>>8);
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                             //ch39
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

 	i=0;
    if(ESP_ERROR_FLG==1)               i|=0x01;
    if((FLAG_1ST_CYCLE_FL==1)||(FLAG_1ST_CYCLE_FR==1)||(FLAG_1ST_CYCLE_RL==1)||(FLAG_1ST_CYCLE_RR==1))                      i|=0x02;
    if(ABS_fz==1)                      i|=0x04;
    if(EBD_RA==1)                      i|=0x08;
    if(BTC_fz==1)                      i|=0x10;
#if __TCS
    if(ETCS_ON==1)                     i|=0x20;
#endif
    if(YAW_CDC_WORK==1)                i|=0x40;
#if __TCS
    if(ESP_TCS_ON==1)                  i|=0x80;
#endif
    CAN_LOG_DATA[66] =(uint8_t) (i);

	i=0;
    if(FLAG_BETA_UNDER_DRIFT==1)       i|=0x01;
    if(ROP_REQ_ACT==1)         i|=0x02;

    if(PARTIAL_BRAKE==1)               i|=0x04;
    if(Reverse_Steer_Flg==1)           i|=0x08;
    if(MPRESS_BRAKE_ON==1)             i|=0x10;
#if __ADVANCED_MSC
    if ( (YAW_CDC_WORK==1) && (ABS_fz==0)){
        if(target_vol == MSC_14_V)  i|=0x20;
    }
    else {
        if(MOT_ON_FLG==1)              i|=0x20;
    }
#else
    if(MOT_ON_FLG==1)                  i|=0x20;

#endif
    if(AUTO_TM==1) {
        if(BACK_DIR==1)                i|=0x40;
    } else {
#if __AX_SENSOR
        if(BACKWARD_MOVE==1)           i|=0x40;
#else
//      if(BACKWARD_MOVE)           tempB4|=0x40;
#endif
    }
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((FL_ESC.FLAG_COMPULSION_HOLD==1)||(FR_ESC.FLAG_COMPULSION_HOLD==1)||(RL_ESC.FLAG_COMPULSION_HOLD==1)||(RR_ESC.FLAG_COMPULSION_HOLD==1))            i|=0x80;
#else
    if((FLAG_COMPULSION_HOLD==1)||(FLAG_COMPULSION_HOLD_REAR==1))            i|=0x80;
#endif
    
    CAN_LOG_DATA[67] =(uint8_t) (i);

	i=0;
    if(AV_VL_fl==1)                    i|=0x01;
    if(HV_VL_fl==1)                    i|=0x02;
    if(ABS_fl==1)                      i|=0x04;
#if __P_CONTROL
    if(P_control_activation_fl==1)                     i|=0x08;
#else
//  if(P_control_activation_fl)                     tempB3|=0x08;
#endif
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fl==1)               i|=0x10;
#else
    if(TCL_DEMAND_SECONDARY==1)        i|=0x10;
#endif
    if(SLIP_CONTROL_NEED_FL==1)        i|=0x20;
    if((ESP_PULSE_DUMP_FLAG_F==1)||(ESP_PULSE_DUMP_FLAG_R==1))                i|=0x40;
    if((FLAG_ACTIVE_BOOSTER==1)||(FLAG_ACTIVE_BOOSTER_PRE==1))            i|=0x80;
    CAN_LOG_DATA[68] =(uint8_t) (i);

	i=0;
    if(AV_VL_fr==1)                    i|=0x01;
    if(HV_VL_fr==1)                    i|=0x02;
    if(ABS_fr==1)                      i|=0x04;
#if __P_CONTROL
    if(P_control_activation_fr==1)                     i|=0x08;
#else
//  if(P_control_activation_fr)                     tempB2|=0x08;
#endif
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fr==1)               i|=0x10;
#else
    if(TCL_DEMAND_PRIMARY==1)          i|=0x10;
#endif
    if(SLIP_CONTROL_NEED_FR==1)        i|=0x20;
    if((FLAG_ACT_PRE_ACTION_FL==1)||(FLAG_ACT_PRE_ACTION_FR==1))          i|=0x40;
    if(FLAG_BANK_DETECTED==1)          i|=0x80;
    CAN_LOG_DATA[69] =(uint8_t) (i);

	i=0;
    if(AV_VL_rl==1)                    i|=0x01;
    if(HV_VL_rl==1)                    i|=0x02;
    if(ABS_rl)                      i|=0x04;
#if __P_CONTROL
    if(P_control_activation_rl==1)                     i|=0x08;
#else
//  if(P_control_activation_rl)                     tempB1|=0x08;
#endif
    if(EBD_rl==1)                      i|=0x10;
    if(SLIP_CONTROL_NEED_RL==1)        i|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_LEFT==1)                i|=0x40;
#else
    if(S_VALVE_SECONDARY==1)               i|=0x40;
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((FL_ESC.FULL_RISE_MODE==1)||(FR_ESC.FULL_RISE_MODE==1)||(RL_ESC.FULL_RISE_MODE==1)||(RR_ESC.FULL_RISE_MODE==1))          i|=0x80;
#else
    if(FULL_RISE_MODE==1)          i|=0x80;
#endif

    CAN_LOG_DATA[70] =(uint8_t) (i);

	i=0;
    if(AV_VL_rr==1)            i|=0x01;
    if(HV_VL_rr==1)            i|=0x02;
    if(ABS_rr==1)              i|=0x04;
#if __P_CONTROL
    if(P_control_activation_rr==1)             i|=0x08;
#else
//  if(P_control_activation_rr)             tempB0|=0x08;
#endif
    if(EBD_rr==1)               i|=0x10;
    if(SLIP_CONTROL_NEED_RR==1)                i|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_RIGHT==1)       i|=0x40;
#else
    if(S_VALVE_PRIMARY==1)     i|=0x40;
#endif

    if((FLAG_ACT_COMB_CNTR_FL==1)||(FLAG_ACT_COMB_CNTR_FR==1)||(FLAG_ACT_COMB_CNTR_RL==1)||(FLAG_ACT_COMB_CNTR_RR==1))          i|=0x80;   // steer_360_ok_flg
    CAN_LOG_DATA[71] =(uint8_t) (i);

  if(SLIP_CONTROL_NEED_RL==1)
  {
    esp_tempW6 = del_target_slip_rl;
  }
  else if(SLIP_CONTROL_NEED_RR==1)
  {
    esp_tempW6 = del_target_slip_rr;
  }
  else
  {
    esp_tempW6 = 0;
  }
    CAN_LOG_DATA[72] =(uint8_t) (alat_dot_lf); //esp_tempW6;//u16CycleTime;//esp_k9;
    CAN_LOG_DATA[73] =(uint8_t) (alat_dot_lf>>8);//u16CycleTime>>8);//esp_k9>>8);

    CAN_LOG_DATA[74] =(uint8_t) (flags_fl);//del_target_slip_fr;//afz;
    CAN_LOG_DATA[75] =(uint8_t) (flags_fr);// del_target_slip_fr>>8);//afz>>8);

    CAN_LOG_DATA[76] =(uint8_t) (FL.pwm_duty_temp);
    CAN_LOG_DATA[77] =(uint8_t) (FR.pwm_duty_temp);
    CAN_LOG_DATA[78] =(uint8_t) (RL.pwm_duty_temp);
    CAN_LOG_DATA[79] =(uint8_t) (RR.pwm_duty_temp);
}

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallESPABSLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ABS in ESP System
********************************************************************************/
void LSESP_vCallESPABSLogData(void)
{

    uint8_t i;

        CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter); /*LOG_BYTE0*/
        CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);               //1 (same channel as ESP log data)
        CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);
        CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);                           //2 (same channel as ESP log data)
        CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);
        CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);                           //3 (same channel as ESP log data)
        CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);
        CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);                           //4 (same channel as ESP log data)

        CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr); /*LOG_BYTE1*/
        CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);                           //5 (same channel as ESP log data)
        CAN_LOG_DATA[10] =(uint8_t) (vref);
        CAN_LOG_DATA[11] =(uint8_t) (vref>>8);                             //6 (same channel as ESP log data)
      #if __4WD || __AX_SENSOR
        CAN_LOG_DATA[12] =(uint8_t) (along);
        CAN_LOG_DATA[13] =(uint8_t) (along>>8);                            //7  (same channel as ESP log data)
      #else
        CAN_LOG_DATA[12] =(uint8_t) (vref5);
        CAN_LOG_DATA[13] =(uint8_t) (vref5>>8);                            //7
      #endif
      
#if  __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
		CAN_LOG_DATA[14] =(uint8_t) (lcabss16RefMpress);    // esp_mu; 
        CAN_LOG_DATA[15] =(uint8_t) (lcabss16RefMpress>>8); // esp_mu>>8);    //8
#else  
        CAN_LOG_DATA[14] =(uint8_t) (fu16CalVoltIntMOTOR);    // esp_mu; 
        CAN_LOG_DATA[15] =(uint8_t) (fu16CalVoltIntMOTOR>>8); // esp_mu>>8);    //8
#endif 

        CAN_LOG_DATA[16] =(uint8_t) (wstr); /*LOG_BYTE2*/
        CAN_LOG_DATA[17] =(uint8_t) (wstr>>8);                             //9
        CAN_LOG_DATA[18] =(uint8_t) (alat);
        CAN_LOG_DATA[19] =(uint8_t) (alat>>8);                             //10
        CAN_LOG_DATA[20] =(uint8_t) (yaw_out);
        CAN_LOG_DATA[21] =(uint8_t) (yaw_out>>8);                          //11
        
#if  __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
        CAN_LOG_DATA[22] =(uint8_t) (lcabss16RefCircuitPress);
        CAN_LOG_DATA[23] =(uint8_t) (lcabss16RefCircuitPress>>8);   //12 /* temp0 */
#else  
        CAN_LOG_DATA[22] =(uint8_t) (mpress);
        CAN_LOG_DATA[23] =(uint8_t) (mpress>>8);                           //12 /* */
#endif
        CAN_LOG_DATA[22] =(uint8_t) (RL.lcabss16RiseDeltaWhlP);
        CAN_LOG_DATA[23] =(uint8_t) (RL.lcabss16RiseDeltaWhlP>>8);                           //12 /* */
        
        CAN_LOG_DATA[24] =(uint8_t) ((afz>127)?127:((afz<-127)?-127:(int8_t)afz));  //13 /*LOG_BYTE3*/
        CAN_LOG_DATA[25] =(uint8_t) (0);//14

/*
      #if __ESC_MOTOR_PWM_CONTROL
        CAN_LOG_DATA[26] =(uint8_t) (lau8MscDuty); //15 FL.LFC_zyklus_z
      #else
        CAN_LOG_DATA[26] =(uint8_t) (u8CycleTime); //15
      #endif
*/
        /*
        CAN_LOG_DATA[26] =(uint8_t) ((lsrbcs16HydPressReduceReq/10)>127)?127:(((lsrbcs16HydPressReduceReq/10)<-127)?-127:((int8_t)(lsrbcs16HydPressReduceReq/10)));//15 FL.LFC_zyklus_z
        CAN_LOG_DATA[27] =(uint8_t) (gma_flags);                    //16

      #if __HW_TEST_MODE
        CAN_LOG_DATA[26] =(uint8_t) (target_vol/100);                    //15
        CAN_LOG_DATA[27] =(uint8_t) (las8HwTestMode);                    //16
      #endif  
      */

        CAN_LOG_DATA[26] =(uint8_t) ((FL.lcabss16TarDeltaWhlP>255)?255:((FL.lcabss16TarDeltaWhlP<0)?0:FL.lcabss16TarDeltaWhlP)); //15
        CAN_LOG_DATA[27] =(uint8_t) ((RL.lcabss16TarDeltaWhlP>255)?255:((RL.lcabss16TarDeltaWhlP<0)?0:RL.lcabss16TarDeltaWhlP)); //16

        CAN_LOG_DATA[28] =(uint8_t) (FL.flags);                            //17
        CAN_LOG_DATA[29] =(uint8_t) (FR.flags);                        //18
        CAN_LOG_DATA[30] =(uint8_t) (RL.flags);                            //19
        CAN_LOG_DATA[31] =(uint8_t) (RR.flags);                            //20

        CAN_LOG_DATA[32] =(uint8_t) (FL.rel_lam);                          //21 /*LOG_BYTE4*/
        CAN_LOG_DATA[33] =(uint8_t) (FR.rel_lam);                          //22
        CAN_LOG_DATA[34] =(uint8_t) (RL.rel_lam);                          //23
        CAN_LOG_DATA[35] =(uint8_t) (RR.rel_lam);                          //24

        CAN_LOG_DATA[36] =(uint8_t) (FL.pwm_duty_temp);                    //25
        CAN_LOG_DATA[37] =(uint8_t) (FR.pwm_duty_temp);                    //26
        CAN_LOG_DATA[38] =(uint8_t) (RL.pwm_duty_temp);                    //27
        CAN_LOG_DATA[39] =(uint8_t) (RR.pwm_duty_temp);                    //28
        
      #if (__MOTOR_MON==ENABLE)
		CAN_LOG_DATA[40] =(uint8_t) ((MonMotorAD[0]>12700)?127:((MonMotorAD[0]<-12700)?-127:(MonMotorAD[0]/100))); /*LOG_BYTE5*/
		CAN_LOG_DATA[41] =(uint8_t) ((MonMotorAD[1]>12700)?127:((MonMotorAD[1]<-12700)?-127:(MonMotorAD[1]/100)));
		CAN_LOG_DATA[42] =(uint8_t) ((MonMotorAD[2]>12700)?127:((MonMotorAD[2]<-12700)?-127:(MonMotorAD[2]/100)));
		CAN_LOG_DATA[43] =(uint8_t) ((MonMotorAD[3]>12700)?127:((MonMotorAD[3]<-12700)?-127:(MonMotorAD[3]/100)));
		CAN_LOG_DATA[44] =(uint8_t) ((MonMotorAD[4]>12700)?127:((MonMotorAD[4]<-12700)?-127:(MonMotorAD[4]/100)));
		CAN_LOG_DATA[45] =(uint8_t) ((MonMotorAD[5]>12700)?127:((MonMotorAD[5]<-12700)?-127:(MonMotorAD[5]/100)));
		CAN_LOG_DATA[46] =(uint8_t) ((MonMotorAD[6]>12700)?127:((MonMotorAD[6]<-12700)?-127:(MonMotorAD[6]/100)));
		CAN_LOG_DATA[47] =(uint8_t) ((MonMotorAD[7]>12700)?127:((MonMotorAD[7]<-12700)?-127:(MonMotorAD[7]/100)));
		CAN_LOG_DATA[48] =(uint8_t) ((MonMotorAD[8]>12700)?127:((MonMotorAD[8]<-12700)?-127:(MonMotorAD[8]/100)));
		CAN_LOG_DATA[49] =(uint8_t) ((MonMotorAD[9]>12700)?127:((MonMotorAD[9]<-12700)?-127:(MonMotorAD[9]/100)));
      #elif (__PRESS_EST && __MP_COMP)
	    if(lcabsu1ValidBrakeSensor==1)
	    {
	        CAN_LOG_DATA[40] =(uint8_t) ((int8_t)FL.LFC_MP_Cyclic_comp_duty);    //29 /*LOG_BYTE5*/
	        CAN_LOG_DATA[41] =(uint8_t) ((int8_t)RL.LFC_MP_Cyclic_comp_duty);    //30
/*
	        CAN_LOG_DATA[42] =(uint8_t) ((int8_t)FR.LFC_MP_Cyclic_comp_duty);    //31
	        CAN_LOG_DATA[43] =(uint8_t) ((int8_t)RR.LFC_MP_Cyclic_comp_duty);    //32
*/
	    }
	    else
	    {
	        CAN_LOG_DATA[40] = 255; 
	        CAN_LOG_DATA[41] = 255;
/*
	        CAN_LOG_DATA[42] = 255;
	        CAN_LOG_DATA[43] = 255;
*/
	    }

        i=0;
        if (FL.Hold_Duty_Comp_flag       ==1) i|=0x01; /* BLS */
        if (FL.Pres_Rise_Defer_flag      ==1) i|=0x02;
        if (FL.lcabsu1WhlPForcedHoldCmd  ==1) i|=0x04;
        if (FL.lcabsu1UpdDeltaPRiseAftHld ==1) i|=0x08;
        if (FR.Hold_Duty_Comp_flag       ==1) i|=0x10;
        if (FR.Pres_Rise_Defer_flag      ==1) i|=0x20;
        if (FR.lcabsu1WhlPForcedHoldCmd  ==1) i|=0x40;
        if (FR.lcabsu1UpdDeltaPRiseAftHld ==1) i|=0x80;
        CAN_LOG_DATA[42] = (i); /*FR.LFC_MP_Cyclic_comp_duty*/

        i=0;
        if (RL.Hold_Duty_Comp_flag         ==1) i|=0x01; /* BLS */
        if (RL.Pres_Rise_Defer_flag        ==1) i|=0x02;
        if (RL.lcabsu1WhlPForcedHoldCmd    ==1) i|=0x04;
        if (RL.lcabsu1UpdDeltaPRiseAftHld   ==1) i|=0x08;
        if (RR.Hold_Duty_Comp_flag         ==1) i|=0x10;
        if (RR.Pres_Rise_Defer_flag        ==1) i|=0x20;
        if (RR.lcabsu1WhlPForcedHoldCmd    ==1) i|=0x40;
        if (RR.lcabsu1UpdDeltaPRiseAftHld   ==1) i|=0x80;
        CAN_LOG_DATA[43] = (i); /*RR.LFC_MP_Cyclic_comp_duty*/

		    /*if((RL.FSF==0) || (RL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FL.GMA_SLIP==1)) 
		      {
		        CAN_LOG_DATA[42] =(uint8_t) ((RL.lcabss16YawDumpFactor>6350)?127:
		                         ((RL.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(RL.lcabss16YawDumpFactor/50))); //39
		      }
		    if((RR.FSF==0) || (RR.LFC_zyklus_z>=1) || (ABS_fz==0) || (FR.GMA_SLIP==1))
		      {
		        CAN_LOG_DATA[43] =(uint8_t) ((RR.lcabss16YawDumpFactor>6350)?127:
		                         ((RR.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(RR.lcabss16YawDumpFactor/50))); //40
		      }  */
		      	
	        CAN_LOG_DATA[44] =(uint8_t) (FL.vfiltn);                    //33
	        CAN_LOG_DATA[45] =(uint8_t) (RL.vfiltn);                    //34
	        CAN_LOG_DATA[46] =(uint8_t) ((FL.High_dump_factor>12700)?127:
	           ((FL.High_dump_factor<-12700)?-127:(int8_t)(FL.High_dump_factor/100)));  //35
	        CAN_LOG_DATA[47] =(uint8_t) ((RL.High_dump_factor>12700)?127:
	           ((RL.High_dump_factor<-12700)?-127:(int8_t)(RL.High_dump_factor/100)));  //36
	
	        CAN_LOG_DATA[48] =(uint8_t) (FL.arad);                             //37 /*LOG_BYTE6*/
	        CAN_LOG_DATA[49] =(uint8_t) (RL.arad);                             //38
	  #endif/*#if (__MOTOR_MON==ENABLE)*/
		  
	        CAN_LOG_DATA[50] =(uint8_t) ((int8_t)FL.SPECIAL_1st_Pulsedown_need); //39
	        CAN_LOG_DATA[51] =(uint8_t) ((int8_t)RL.SPECIAL_1st_Pulsedown_need); //40
	
		    if((FL.FSF==0) || (FL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FL.GMA_SLIP==1)) 
		      {
		        CAN_LOG_DATA[50] =(uint8_t) ((FL.lcabss16YawDumpFactor>6350)?127:
		                         ((FL.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FL.lcabss16YawDumpFactor/50))); //39
		      }
		    if((FR.FSF==0) || (FR.LFC_zyklus_z>=1) || (ABS_fz==0) || (FR.GMA_SLIP==1))
		      {
		        CAN_LOG_DATA[51] =(uint8_t) ((FR.lcabss16YawDumpFactor>6350)?127:
		                         ((FR.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FR.lcabss16YawDumpFactor/50))); //40
		      }

	        CAN_LOG_DATA[50] =(uint8_t) (((FL.lcabss16RiseDeltaWhlP)>255)?255:(((FL.lcabss16RiseDeltaWhlP)<0)?0:(int8_t)(FL.lcabss16RiseDeltaWhlP)));/*((int8_t)FL.SPECIAL_1st_Pulsedown_need);*/ //39
	        CAN_LOG_DATA[51] =(uint8_t) (((FR.lcabss16RiseDeltaWhlP)>255)?255:(((FR.lcabss16RiseDeltaWhlP)<0)?0:(int8_t)(FR.lcabss16RiseDeltaWhlP)));/*((int8_t)RL.SPECIAL_1st_Pulsedown_need);*/ //40


    if((0)     
      #if __HRB
        || (lchrbu1ActiveFlag==1)
      #endif
      #if __TCMF_LowVacuumBoost
        || (TCMF_LowVacuumBoost_flag==1)
      #endif
      )
      {
        CAN_LOG_DATA[50] =(uint8_t)(target_vol/100); //39
      }
        
	  #if __PRESS_EST_ACTIVE
        CAN_LOG_DATA[52] = (uint8_t)(FL.s16_Estimated_Active_Press/10);      //41  // FL.Estimated_Press (la_FLNO.lau16PwmDuty[1]/10)
        CAN_LOG_DATA[53] = (uint8_t)(FR.s16_Estimated_Active_Press/10);      //42  // FR.Estimated_Press (la_FRNO.lau16PwmDuty[1]/10)
        CAN_LOG_DATA[54] = (uint8_t)(RL.s16_Estimated_Active_Press/10);      //43  // RL.Estimated_Press (wau16PwmValueFLNO[1]/10);// 
        CAN_LOG_DATA[55] = (uint8_t)(RR.s16_Estimated_Active_Press/10);      //44  // RR.Estimated_Press (wau16PwmValueFRNO[1]/10);//
	  #endif

        CAN_LOG_DATA[56] = (uint8_t)((int8_t)FL.LFC_Conven_Reapply_Time/*FL.LFC_special_comp_duty*/);     //45 /*LOG_BYTE7*/
        CAN_LOG_DATA[57] = (uint8_t)((int8_t)FR.LFC_Conven_Reapply_Time/*FR.LFC_special_comp_duty*/);		//46
        CAN_LOG_DATA[58] = (uint8_t)((int8_t)RL.LFC_Conven_Reapply_Time/*RL.LFC_special_comp_duty*/);     //47
        CAN_LOG_DATA[59] = (uint8_t)((int8_t)RR.LFC_Conven_Reapply_Time/*RR.LFC_special_comp_duty*/);     //48

//        CAN_LOG_DATA[60] = (uint8_t)(((sign_o_delta_yaw_thres/10)>127)?127:(((sign_o_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_o_delta_yaw_thres/10))); //49 /* */
//        CAN_LOG_DATA[61] = (uint8_t)(((sign_u_delta_yaw_thres/10)>127)?127:(((sign_u_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_u_delta_yaw_thres/10))); //50 /* */
        CAN_LOG_DATA[60] = (uint8_t)((FR.lcabss16TarDeltaWhlP>255)?255:((FR.lcabss16TarDeltaWhlP<0)?0:FR.lcabss16TarDeltaWhlP)); //49 /* */
        CAN_LOG_DATA[61] = (uint8_t)((RR.lcabss16TarDeltaWhlP>255)?255:((RR.lcabss16TarDeltaWhlP<0)?0:RR.lcabss16TarDeltaWhlP)); //50 /* */

    if((FL.L_SLIP_CONTROL==0) && (FR.L_SLIP_CONTROL==0) && (RL.L_SLIP_CONTROL==0) && (RR.L_SLIP_CONTROL==0))
    {
      #if __HRB
        CAN_LOG_DATA[60] = (uint8_t) (lchrbs8CtrlMode); //39
      #endif
    }

        CAN_LOG_DATA[62] = (uint8_t)(FL.on_time_outlet_pwm); //51
        CAN_LOG_DATA[63] = (uint8_t)(RL.on_time_outlet_pwm); //52
#if __TCMF_CONTROL
  #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)
  #else
    if(TCL_DEMAND_fr==1)
  #endif
    {
        CAN_LOG_DATA[62] = (uint8_t)(MFC_PWM_DUTY_P); //51
    }
  #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_SECONDARY==1)
  #else
    if(TCL_DEMAND_fl==1)
  #endif
    {
        CAN_LOG_DATA[63] = (uint8_t)(MFC_PWM_DUTY_S); //52
    }
#endif
        /*
		CAN_LOG_DATA[64] = (uint8_t)((int8_t)mtp);   //53 (uint8_t)(FL.lcabss8DumpCase);//
        CAN_LOG_DATA[65] = (uint8_t)(esp_mu/10);     //54(RL.lcabss8DumpCase);//
        */
        CAN_LOG_DATA[64] = (uint8_t)(RL.lcabss16EstWhlPAftDump/10); //49 /*LOG_BYTE8*/
        CAN_LOG_DATA[65] = (uint8_t)(RR.lcabss16EstWhlPAftDump/10); //50 
        
        /*
        CAN_LOG_DATA[66] = (uint8_t)(rf2);
        CAN_LOG_DATA[67] = (uint8_t)(rf2>>8);                              //55
        CAN_LOG_DATA[68] = (uint8_t)(delta_moment_q);
        CAN_LOG_DATA[69] = (uint8_t)(delta_moment_q>>8);                //56
        */
        
//  #if __EST_MU_FROM_SKID
//        tempW7 = 0;
//        if     (FL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1000;}
//        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1000;}
//        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1000;}
//        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1000;}
//        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1000;}
//        else if(FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1000;}
//        else                                                             {tempW7 = tempW7 + 9*1000;}
//        if     (FR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*100;}
//        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*100;}
//        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*100;}
//        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*100;}
//        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*100;}
//        else if(FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*100;}
//        else                                                             {tempW7 = tempW7 + 9*100;}
//        if     (RL.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*10;}
//        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*10;}
//        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*10;}
//        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*10;}
//        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*10;}
//        else if(RL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*10;}
//        else                                                             {tempW7 = tempW7 + 9*10;}
//        if     (RR.lcabsu8PossibleMuFromSkid==0)                         {tempW7 = tempW7 + 0*1;}
//        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_HIGH_MU)          {tempW7 = tempW7 + 1*1;}
//        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MED_HIGH_MU)      {tempW7 = tempW7 + 2*1;}
//        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_MEDIUM_MU)        {tempW7 = tempW7 + 3*1;}
//        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_UNDER_LOW_MED_MU) {tempW7 = tempW7 + 4*1;}
//        else if(RR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU)           {tempW7 = tempW7 + 5*1;}
//        else                                                             {tempW7 = tempW7 + 9*1;}
//        
//        CAN_LOG_DATA[68] = (uint8_t)(tempW7);
//        CAN_LOG_DATA[69] = (uint8_t)(tempW7>>8);                //56
//  #endif                

        CAN_LOG_DATA[66] = (uint8_t)(FL.lcabss16EstWhlPAftDump);
        CAN_LOG_DATA[67] = (uint8_t)(FL.lcabss16EstWhlPAftDump>>8);                              //55
        CAN_LOG_DATA[68] = (uint8_t)(FR.lcabss16EstWhlPAftDump);
        CAN_LOG_DATA[69] = (uint8_t)(FR.lcabss16EstWhlPAftDump>>8);                //56

        
        /**************************mu change debugging**************************/
        /*if((LOW_to_HIGH_suspect==1)||(LOW_to_HIGH_suspect2_old==1))
        {
	        CAN_LOG_DATA[66] = (uint8_t)(L2H_sus_cnt_by_aref);//(aref_avg-aref0)
	        CAN_LOG_DATA[67] = (uint8_t)(L2H_sus_cnt_by_aref>>8);                              //55
	        CAN_LOG_DATA[68] = (uint8_t)((int16_t)lsabsu8L2HMode);
	        CAN_LOG_DATA[69] = (uint8_t)((int16_t)lsabsu8L2HMode>>8);                //56
		}*/	
        /***********************************************************************/

        if(U8_ESP_Output_Selection_Mode==71)
        {
          #if __HRB
            CAN_LOG_DATA[66] = (uint8_t)(MFC_Current_HRB_P);     
            CAN_LOG_DATA[67] = (uint8_t)(MFC_Current_HRB_P>>8);         //55

            CAN_LOG_DATA[68] = (uint8_t)(lchrbs16AssistPressure);       
            CAN_LOG_DATA[69] = (uint8_t)(lchrbs16AssistPressure>>8);    //56
          #endif
        }
        else
        {
            ;
        }
  
      #if __HW_TEST_MODE
        CAN_LOG_DATA[68] = (uint8_t)(HW_test_cnt_t);
        CAN_LOG_DATA[69] = (uint8_t)(HW_test_cnt_t>>8);                //56
      #endif  


        
        i=0;
        if (lcabsu1BrakeByBLS               ==1) i|=0x01; /* BLS */
        if (ABS_fz                          ==1) i|=0x02;
        if (AFZ_OK                          ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
          #if __CBC
        if (CBC_ENABLE						==1) i|=0x10; // BTC_fz
          #else
        if ((PBA_ON==1) || (PBA_pulsedown  ==1)	==1) i|=0x10;
          #endif
        if (YAW_CDC_WORK                    ==1) i|=0x20;
        if (BEND_DETECT2                    ==1) i|=0x40; 
        if (ESP_TCS_ON                      ==1) i|=0x80;       
		#if __PAC  
		  #if (U8_PACC_ENABLE==ENABLE)
        if (lcpacu1PACEnableFlg             ==1) i|=0x80; //ESP_TCS_ON
		  #endif
		#endif
        CAN_LOG_DATA[70] = (i);               //57

        i=0;
        if (0         ==1) i|=0x01; /* Stroke Recovery Request */
        if (lcabsu1BrakeSensorOn            ==1) i|=0x02; /* ESP_SPLIT */
        if (LFC_Split_flag                  ==1) i|=0x04;
        if (LFC_Split_suspect_flag          ==1) i|=0x08;
        if (BEND_DETECT_LEFT                ==1) i|=0x10;
        if (BEND_DETECT_RIGHT               ==1) i|=0x20;
        if (Rough_road_suspect_vehicle      ==1) i|=0x40;
        if (Rough_road_detect_vehicle       ==1) i|=0x80;

        CAN_LOG_DATA[71] = i;               //58

        i = 0;
        if (ABS_fl                          ==1) i|=0x01;
//      if (BUILT_fl                        ==1) i|=0x02;
        if (ESP_ABS_SLIP_CONTROL_fl         ==1) i|=0x02;
        if (STABIL_fl                       ==1) i|=0x04;
        if (AV_HOLD_fl                      ==1) i|=0x08;
        if (AV_DUMP_fl                      ==1) i|=0x10;
        if (GMA_SLIP_fl                     ==1) i|=0x20;
		#if __HW_TEST_MODE == ENABLE        
			if (laHwTestValveFL.u1HwTestHvVlAct		==1) i|=0x40;
        	if (laHwTestValveFL.u1HwTestAvVlAct		==1) i|=0x80;
		#else
        	if (HV_VL_fl                        ==1) i|=0x40;
        	if (AV_VL_fl                        ==1) i|=0x80;
		#endif

        CAN_LOG_DATA[72] = i;               // 59 /*LOG_BYTE9*/

        i=0;
        if (ABS_fr                          ==1) i|=0x01;
//      if (BUILT_fr                        ==1) i|=0x02;
        if (ESP_ABS_SLIP_CONTROL_fr         ==1) i|=0x02;
        if (STABIL_fr                       ==1) i|=0x04;
        if (AV_HOLD_fr                      ==1) i|=0x08;
        if (AV_DUMP_fr                      ==1) i|=0x10;
        if (GMA_SLIP_fr                     ==1) i|=0x20;
		#if __HW_TEST_MODE == ENABLE        
			if (laHwTestValveFR.u1HwTestHvVlAct		==1) i|=0x40;
        	if (laHwTestValveFR.u1HwTestAvVlAct		==1) i|=0x80;
		#else
        	if (HV_VL_fr                        ==1) i|=0x40;
        	if (AV_VL_fr                        ==1) i|=0x80;
		#endif

        CAN_LOG_DATA[73] = i;               //60

        i=0;
        if (ABS_rl                          ==1) i|=0x01;
//      if (BUILT_rl                        ==1) i|=0x02;
        if (ESP_ABS_SLIP_CONTROL_rl         ==1) i|=0x02;
        if (STABIL_rl                       ==1) i|=0x04;
        if (SPLIT_JUMP_rl                   ==1) i|=0x08;
        if (SPLIT_PULSE_rl                  ==1) i|=0x10;
        if (RL.AV_DUMP                      ==1) i|=0x20;
		#if __HW_TEST_MODE == ENABLE        
			if (laHwTestValveRL.u1HwTestHvVlAct		==1) i|=0x40;
        	if (laHwTestValveRL.u1HwTestAvVlAct		==1) i|=0x80;
		#else
        	if (HV_VL_rl                        ==1) i|=0x40;
        	if (AV_VL_rl                        ==1) i|=0x80;
		#endif

        CAN_LOG_DATA[74] = i;               //61

        i = 0;
        if(ABS_rr                           ==1) i|=0x01;
//      if(BUILT_rr                         ==1) i|=0x02;
        if(ESP_ABS_SLIP_CONTROL_rr          ==1) i|=0x02;
        if(STABIL_rr                        ==1) i|=0x04;
        if(SPLIT_JUMP_rr                    ==1) i|=0x08;
        if(SPLIT_PULSE_rr                   ==1) i|=0x10;
        if(RR.AV_DUMP                       ==1) i|=0x20;
		#if __HW_TEST_MODE == ENABLE        
			if (laHwTestValveRR.u1HwTestHvVlAct		==1) i|=0x40;
        	if (laHwTestValveRR.u1HwTestAvVlAct		==1) i|=0x80;
		#else
        	if (HV_VL_rr                        ==1) i|=0x40;
        	if (AV_VL_rr                        ==1) i|=0x80;
		#endif
        CAN_LOG_DATA[75] = i;               // 62

        i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_SECONDARY                 ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fl                        ==1) i|=0x01;
      #endif
        if(FL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FL.lcabsu1WhlPForcedHoldCmd          ==1) i|=0x04; /*LFC_big_rise_and_dump_flag*/
        if(FL.LFC_maintain_split_flag           ==1) i|=0x08;
        if(FL.In_Gear_flag                      ==1) i|=0x10;
        if(FL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(FL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fl                       ==1) i|=0x80;

        CAN_LOG_DATA[76] = i;                   // 63

         i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_PRIMARY                   ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fr                        ==1) i|=0x01;
      #endif
        if(FR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FR.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x04; /*LFC_big_rise_and_dump_flag*/
        if(FR.LFC_maintain_split_flag           ==1) i|=0x08;
        if(FR.In_Gear_flag                      ==1) i|=0x10;
        if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(FR.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fr                       ==1) i|=0x80;

        CAN_LOG_DATA[77] = i;                   // 64

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY                    ==1) i|=0x01;
      #else
        if(S_VALVE_LEFT                         ==1) i|=0x01;
      #endif
        if(RL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RL.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x04; /*LFC_big_rise_and_dump_flag*/
        if(RL.MSL_BASE                          ==1) i|=0x08;
        if(RL.Duty_Limitation_flag              ==1) i|=0x10;
        if(RL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(RL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_rl                       ==1) i|=0x80;

        CAN_LOG_DATA[78] = i;                   // 65

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_PRIMARY                      ==1) i|=0x01;
      #else
        if(S_VALVE_RIGHT                        ==1) i|=0x01;
      #endif
        if(RR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RR.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x04; /*LFC_big_rise_and_dump_flag*/
        if(RR.MSL_BASE                          ==1) i|=0x08;
        if(RR.Duty_Limitation_flag              ==1) i|=0x10;
        if(RR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   // LFC_to_ESP_flag
        if(RR.LFC_built_reapply_flag            ==1) i|=0x40;
      #if __HOB
        if(lcu1HobOnFlag                        ==1) i|=0x80;
      #else  
        if(SPOLD_RESET_rr                       ==1) i|=0x80;
      #endif

        CAN_LOG_DATA[79] = i;                   // 66

}
/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallIDBABSLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ABS in ESP System
********************************************************************************/
void LSESP_vCallIDBABSLogData(void)
{

    uint8_t i;
    int16_t s16Temp1;

		/*****************************************************************/
		/*                          LOG_BYTE0                            */
		/*****************************************************************/
        CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);
        CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);               //1 (same channel as ESP log data)
        CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);
        CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);                           //2 (same channel as ESP log data)
        CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);
        CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);                           //3 (same channel as ESP log data)
        CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);
        CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);                           //4 (same channel as ESP log data)

        /*****************************************************************/
        /*                          LOG_BYTE1                            */
        /*****************************************************************/
        CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);
        CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);                           //1 (same channel as ESP log data)
        CAN_LOG_DATA[10] =(uint8_t) (vref);
        CAN_LOG_DATA[11] =(uint8_t) (vref>>8);                              //2 (same channel as ESP log data)
        CAN_LOG_DATA[12] =(uint8_t) (along);
        CAN_LOG_DATA[13] =(uint8_t) (along>>8);                             //3  (same channel as ESP log data)
        CAN_LOG_DATA[14] =(uint8_t) (lcabss16RefMpress/10); 				//4
        CAN_LOG_DATA[15] =(uint8_t) (lcabss16InABSDesiredBoostTarP/10);				//5

        /*****************************************************************/
        /*                          LOG_BYTE2                            */
        /*****************************************************************/
        CAN_LOG_DATA[16] =(uint8_t) (wstr);
        CAN_LOG_DATA[17] =(uint8_t) (wstr>>8);                             //1
        CAN_LOG_DATA[18] =(uint8_t) (alat);
        CAN_LOG_DATA[19] =(uint8_t) (alat>>8);                             //2
        CAN_LOG_DATA[20] =(uint8_t) (yaw_out);
        CAN_LOG_DATA[21] =(uint8_t) (yaw_out>>8);                          //3
        CAN_LOG_DATA[22] =(uint8_t) (lcabss16RefCircuitPress);
        CAN_LOG_DATA[23] =(uint8_t) (lcabss16RefCircuitPress>>8);   	   //4

        /*****************************************************************/
        /*                          LOG_BYTE3                            */
        /*****************************************************************/
        CAN_LOG_DATA[24] =(uint8_t) ((afz>127)?127:((afz<-127)?-127:(int8_t)afz));	//1

        i=0;
        i|= (0xf0 & ((FL.s0_reapply_counter>15) ? 15 : FL.s0_reapply_counter)<<4);  //2
        i|= (0x0f & ((FR.s0_reapply_counter>15) ? 15 : FR.s0_reapply_counter));     //3
        CAN_LOG_DATA[25] =(uint8_t) (i);

        i=0;
        i|= (0xf0 & ((RL.s0_reapply_counter>15) ? 15 : RL.s0_reapply_counter)<<4);  //4
        i|= (0x0f & ((RR.s0_reapply_counter>15) ? 15 : RR.s0_reapply_counter));     //5
        CAN_LOG_DATA[26] =(uint8_t) (i);

        CAN_LOG_DATA[27] =(uint8_t) (gma_flags);                    				//6
        CAN_LOG_DATA[28] =(uint8_t) (FL.flags);                            			//7
        CAN_LOG_DATA[29] =(uint8_t) (FR.flags);                        				//8
        CAN_LOG_DATA[30] =(uint8_t) (RL.flags);                            			//9
        CAN_LOG_DATA[31] =(uint8_t) (RR.flags);                            			//10

        /*****************************************************************/
        /*                          LOG_BYTE4                            */
        /*****************************************************************/
        CAN_LOG_DATA[32] =(uint8_t) (FL.rel_lam);                          //1
        CAN_LOG_DATA[33] =(uint8_t) (FR.rel_lam);                          //2
        CAN_LOG_DATA[34] =(uint8_t) (RL.rel_lam);                          //3
        CAN_LOG_DATA[35] =(uint8_t) (RR.rel_lam);                          //4

        CAN_LOG_DATA[36] =(uint8_t) (FL.pwm_duty_temp);                    //5
        CAN_LOG_DATA[37] =(uint8_t) (FR.pwm_duty_temp);                    //6
        CAN_LOG_DATA[38] =(uint8_t) (RL.pwm_duty_temp);                    //7
        CAN_LOG_DATA[39] =(uint8_t) (RR.pwm_duty_temp);                    //8

        /*****************************************************************/
        /*                          LOG_BYTE5                            */
        /*****************************************************************/
        CAN_LOG_DATA[40] =(uint8_t) (FL_WP.lcabss16WhlTarDP);           //1
        CAN_LOG_DATA[41] =(uint8_t) (FR_WP.lcabss16WhlTarDP);           //2
        CAN_LOG_DATA[42] =(uint8_t) (RL_WP.lcabss16WhlTarDP);           //3
        CAN_LOG_DATA[43] =(uint8_t) (RR_WP.lcabss16WhlTarDP);           //4
        CAN_LOG_DATA[44] =(uint8_t) (FL.vfiltn);                    		//5
        CAN_LOG_DATA[45] =(uint8_t) (RL.vfiltn);                    		//6
        CAN_LOG_DATA[46] =(uint8_t) ((FL.High_dump_factor>12700)?127:
           ((FL.High_dump_factor<-12700)?-127:(int8_t)(FL.High_dump_factor/100)));  //7
        CAN_LOG_DATA[47] =(uint8_t) ((RL.High_dump_factor>12700)?127:
           ((RL.High_dump_factor<-12700)?-127:(int8_t)(RL.High_dump_factor/100)));  //8

        /*
		if((FL.FSF==0) || (FL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FL.GMA_SLIP==1))
		  {
			CAN_LOG_DATA[44] =(uint8_t) ((FL.lcabss16YawDumpFactor>6350)?127:
							 ((FL.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FL.lcabss16YawDumpFactor/50))); //5
		  }
		if((FR.FSF==0) || (FR.LFC_zyklus_z>=1) || (ABS_fz==0) || (FR.GMA_SLIP==1))
		  {
			CAN_LOG_DATA[45] =(uint8_t) ((FR.lcabss16YawDumpFactor>6350)?127:
							 ((FR.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(FR.lcabss16YawDumpFactor/50))); //6
		  }

		  if((RL.FSF==0) || (RL.LFC_zyklus_z>=1) || (ABS_fz==0) || (FL.GMA_SLIP==1))
		  {
			CAN_LOG_DATA[46] =(uint8_t) ((RL.lcabss16YawDumpFactor>6350)?127:
							 ((RL.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(RL.lcabss16YawDumpFactor/50))); //7
		  }
		if((RR.FSF==0) || (RR.LFC_zyklus_z>=1) || (ABS_fz==0) || (FR.GMA_SLIP==1))
		  {
			CAN_LOG_DATA[47] =(uint8_t) ((RR.lcabss16YawDumpFactor>6350)?127:
							 ((RR.lcabss16YawDumpFactor<-6350)?-127:(int8_t)(RR.lcabss16YawDumpFactor/50))); //8
		  }
		  */

        /*****************************************************************/
        /*                          LOG_BYTE6                            */
        /*****************************************************************/
		CAN_LOG_DATA[48] =(uint8_t) (FL.arad);                             //1
		CAN_LOG_DATA[49] =(uint8_t) (RL.arad);                             //2

        i=0;
        i|= (0xC0 & ((FL.SPECIAL_1st_Pulsedown_need>3) ? 3 : FL.SPECIAL_1st_Pulsedown_need)<<6);  //3
        i|= (0x30 & ((RL.SPECIAL_1st_Pulsedown_need>3) ? 3 : RL.SPECIAL_1st_Pulsedown_need)<<4);  //4
        i|= (0x0f & (((0)>15) ? 15 : (0)));     	 	  //5
        CAN_LOG_DATA[50] =(uint8_t) (i);

	  #if __PRESS_EST_ACTIVE
        CAN_LOG_DATA[51] = (uint8_t)(FL.s16_Estimated_Active_Press/10);      //6
        CAN_LOG_DATA[52] = (uint8_t)(FR.s16_Estimated_Active_Press/10);      //7
        CAN_LOG_DATA[53] = (uint8_t)(RL.s16_Estimated_Active_Press/10);      //8
        CAN_LOG_DATA[54] = (uint8_t)(RR.s16_Estimated_Active_Press/10);      //9
	  #endif

        i=0;
        i|= (0xf0 & ((FL.WheelCtrlMode>15) ? 15 : FL.WheelCtrlMode)<<4);  //10
        i|= (0x0f & ((FR.WheelCtrlMode>15) ? 15 : FR.WheelCtrlMode));     //11
        CAN_LOG_DATA[55] =(uint8_t)	(i);

        /*****************************************************************/
        /*                          LOG_BYTE7                            */
        /*****************************************************************/
        CAN_LOG_DATA[56] = (uint8_t)((int8_t)FL.LFC_Conven_Reapply_Time);    //1
        CAN_LOG_DATA[57] = (uint8_t)((int8_t)FR.LFC_Conven_Reapply_Time);	 //2
        CAN_LOG_DATA[58] = (uint8_t)((int8_t)RL.LFC_Conven_Reapply_Time);    //3
        CAN_LOG_DATA[59] = (uint8_t)((int8_t)RR.LFC_Conven_Reapply_Time);    //4

        i=0;
        i|= (0xC0 & ((FL.lcabss8WhlCtrlReqForStrkRec>3) ? 3 : FL.lcabss8WhlCtrlReqForStrkRec)<<6);  //5
        i|= (0x30 & ((FR.lcabss8WhlCtrlReqForStrkRec>3) ? 3 : FR.lcabss8WhlCtrlReqForStrkRec)<<4);  //6
        i|= (0x0C & ((RL.lcabss8WhlCtrlReqForStrkRec>3) ? 3 : RL.lcabss8WhlCtrlReqForStrkRec)<<2);  //7
        i|= (0x03 & ((RR.lcabss8WhlCtrlReqForStrkRec>3) ? 3 : RR.lcabss8WhlCtrlReqForStrkRec));     //8
        CAN_LOG_DATA[60] =(uint8_t)	(i);

        i=0;
        if (FL.LFC_Conven_OnOff_Flag           ==1) i|=0x01;	//9
        if (FR.LFC_Conven_OnOff_Flag           ==1) i|=0x02;	//10
        if (RL.LFC_Conven_OnOff_Flag           ==1) i|=0x04;	//11
        if (RR.LFC_Conven_OnOff_Flag           ==1) i|=0x08;	//12
        if (FL.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x10;	//13
        if (FR.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x20;	//14
        if (RL.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x40;	//15
        if (RR.lcabsu1WhlPForcedHoldCmd        ==1) i|=0x80;	//16
        CAN_LOG_DATA[61] = i;

        CAN_LOG_DATA[62] = (uint8_t)(FL.on_time_outlet_pwm); 	//17
        CAN_LOG_DATA[63] = (uint8_t)(RL.on_time_outlet_pwm); 	//18

        /*****************************************************************/
        /*                          LOG_BYTE8                            */
        /*****************************************************************/
        i=0;
        i|= (0xC0 & ((0x03 & FL.lcabss8DumpCase)<<6));  //1
        i|= (0x30 & ((0x03 & FR.lcabss8DumpCase)<<4));  //2
        i|= (0x0C & ((0x03 & RL.lcabss8DumpCase)<<2));  //3
        i|= (0x03 & ((0x03 & RR.lcabss8DumpCase)));     //4
        CAN_LOG_DATA[64] =(uint8_t)	(i);

        i=0;
        i|= (0xf0 & ((RL.WheelCtrlMode>15) ? 15 : RL.WheelCtrlMode)<<4);  //5
        i|= (0x0f & ((RR.WheelCtrlMode>15) ? 15 : RR.WheelCtrlMode));     //6
        CAN_LOG_DATA[65] =(uint8_t) (i);
/*
         i=0;
        if (FL.LFC_n_th_deep_slip_pre_flag      ==1) i|=0x01;		//7
        if (RR.LFC_n_th_deep_slip_pre_flag	    ==1) i|=0x02;		//8
        if (RL.LFC_n_th_deep_slip_pre_flag      ==1) i|=0x04;		//9
        if (SPOLD_RESET_fl      				==1) i|=0x08;		//10
        if (0                					==1) i|=0x10;		//11
        if (0               					==1) i|=0x20;		//12
        if (0      								==1) i|=0x40;		//13
        if (0       							==1) i|=0x80;		//14
        CAN_LOG_DATA[65] = i;
*/
        CAN_LOG_DATA[66] = (uint8_t)(rf2);
        CAN_LOG_DATA[67] = (uint8_t)(rf2>>8); 				//7

        i=0;
        if (lcabsu1BrakeByBLS               ==1) i|=0x01;	//8
        if (ABS_fz                          ==1) i|=0x02;	//9
        if (AFZ_OK                          ==1) i|=0x04;	//10
        if (EBD_RA                          ==1) i|=0x08;	//11
          #if __CBC
        if (CBC_ENABLE						==1) i|=0x10;   //12 BTC_fz
          #else
        if ((PBA_ON==1) || (PBA_pulsedown  ==1)	==1) i|=0x10;//12
          #endif
        if (YAW_CDC_WORK                    ==1) i|=0x20;	//13
        if (BEND_DETECT2                    ==1) i|=0x40;	//14
        if (ESP_TCS_ON                      ==1) i|=0x80;	//15
		#if __PAC
		  #if (U8_PACC_ENABLE==ENABLE)
        if (lcpacu1PACEnableFlg             ==1) i|=0x80; //15 ESP_TCS_ON
		  #endif
		#endif
        CAN_LOG_DATA[68] = (i);

        i=0;
        if (0     ==1) i|=0x01;   //16
        if (LOW_to_HIGH_suspect			    ==1) i|=0x02;	//17
        if (LFC_Split_flag                  ==1) i|=0x04;   //18
        if (LFC_Split_suspect_flag          ==1) i|=0x08;	//19
        if (BEND_DETECT_LEFT                ==1) i|=0x10;	//20
        if (BEND_DETECT_RIGHT               ==1) i|=0x20;	//21
        if (Rough_road_suspect_vehicle      ==1) i|=0x40;	//22
        if (Rough_road_detect_vehicle       ==1) i|=0x80;	//23
        CAN_LOG_DATA[69] = i;

        i=0;
        if (FL.Hold_Duty_Comp_flag       	==1) i|=0x01;	//24
        if (FL.LOW_to_HIGH_suspect2 /*Pres_Rise_Defer_flag*/      	==1) i|=0x02;	//25
        if (BUILT_fl					 	==1) i|=0x04;	//26
        if (FL.lcabsu1UpdDeltaPRiseAftHld 	==1) i|=0x08;	//27
        if (FR.Hold_Duty_Comp_flag       	==1) i|=0x10;	//28
        if (FR.LOW_to_HIGH_suspect2 /*Pres_Rise_Defer_flag*/      	==1) i|=0x20;	//29
        if (BUILT_fr					  	==1) i|=0x40;	//30
        if (FR.lcabsu1UpdDeltaPRiseAftHld 	==1) i|=0x80;	//31
        CAN_LOG_DATA[70] = (i);

        i=0;
        if (RL.Hold_Duty_Comp_flag          ==1) i|=0x01;	//32
        if (RL.LOW_to_HIGH_suspect2 /*Pres_Rise_Defer_flag*/         ==1) i|=0x02;	//33
        if (BUILT_rl     				    ==1) i|=0x04;	//34
        if (RL.lcabsu1UpdDeltaPRiseAftHld   ==1) i|=0x08;	//35
        if (RR.Hold_Duty_Comp_flag          ==1) i|=0x10;	//36
        if (RR.LOW_to_HIGH_suspect2 /*Pres_Rise_Defer_flag*/         ==1) i|=0x20;	//37
        if (BUILT_rr					    ==1) i|=0x40;	//38
        if (RR.lcabsu1UpdDeltaPRiseAftHld   ==1) i|=0x80;	//39
        CAN_LOG_DATA[71] = (i);

        /*****************************************************************/
        /*                          LOG_BYTE9                            */
        /*****************************************************************/
        i = 0;
        if (ABS_fl                              ==1) i|=0x01;	//1
        if (ESP_ABS_SLIP_CONTROL_fl             ==1) i|=0x02;	//2
        if (STABIL_fl                           ==1) i|=0x04;	//3
        if (AV_HOLD_fl                          ==1) i|=0x08;	//4
        if (AV_DUMP_fl                          ==1) i|=0x10;	//5
        if (GMA_SLIP_fl                         ==1) i|=0x20;	//6
		if (HV_VL_fl                            ==1) i|=0x40;	//7
		if (AV_VL_fl                            ==1) i|=0x80;	//8
        CAN_LOG_DATA[72] = i;

        i=0;
        if (ABS_fr                              ==1) i|=0x01;	//9
        if (ESP_ABS_SLIP_CONTROL_fr             ==1) i|=0x02;	//10
        if (STABIL_fr                           ==1) i|=0x04;	//11
        if (AV_HOLD_fr                          ==1) i|=0x08;	//12
        if (AV_DUMP_fr                          ==1) i|=0x10;	//13
        if (GMA_SLIP_fr                         ==1) i|=0x20;	//14
		if (HV_VL_fr                            ==1) i|=0x40;	//15
		if (AV_VL_fr                            ==1) i|=0x80;	//16
        CAN_LOG_DATA[73] = i;

        i=0;
        if (ABS_rl                              ==1) i|=0x01;	//17
        if (ESP_ABS_SLIP_CONTROL_rl             ==1) i|=0x02;	//18
        if (STABIL_rl                           ==1) i|=0x04;	//19
        if (SPLIT_JUMP_rl                       ==1) i|=0x08;	//20
        if (SPLIT_PULSE_rl                      ==1) i|=0x10;	//21
        if (RL.AV_DUMP                          ==1) i|=0x20;	//22
		if (HV_VL_rl                            ==1) i|=0x40;	//23
		if (AV_VL_rl                            ==1) i|=0x80;	//24
        CAN_LOG_DATA[74] = i;

        i = 0;
        if(ABS_rr                               ==1) i|=0x01;	//25
        if(ESP_ABS_SLIP_CONTROL_rr              ==1) i|=0x02;	//26
        if(STABIL_rr                            ==1) i|=0x04;	//27
        if(SPLIT_JUMP_rr                        ==1) i|=0x08;	//28
        if(SPLIT_PULSE_rr                       ==1) i|=0x10;	//29
        if(RR.AV_DUMP                           ==1) i|=0x20;	//30
		if (HV_VL_rr                            ==1) i|=0x40;	//31
		if (AV_VL_rr                            ==1) i|=0x80;	//32
        CAN_LOG_DATA[75] = i;

        i=0;
        if(FL.lcabsu1CaptureEstPressForSkid     ==1) i|=0x01;	//33
        if(FL.L_SLIP_CONTROL                    ==1) i|=0x02;	//34
        if(FL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;	//35
        if(FL.LFC_maintain_split_flag           ==1) i|=0x08;	//36
        if(FL.In_Gear_flag                      ==1) i|=0x10;	//37
        if(FL.lcabsu1CaptureEstPressAftDump     ==1) i|=0x20; 	//38			/*FL.LFC_n_th_deep_slip_pre_flag*/

        if(FL.LFC_built_reapply_flag            ==1) i|=0x40;	//39
        if(FR.lcabsu1CaptureEstPressAftDump     ==1) i|=0x80; 	//40			/*SPOLD_RESET_fl*/

        CAN_LOG_DATA[76] = i;

        i=0;
        if(FR.lcabsu1CaptureEstPressForSkid     ==1) i|=0x01;	//41
        if(FR.L_SLIP_CONTROL                    ==1) i|=0x02;	//42
        if(FR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;	//43
        if(FR.LFC_maintain_split_flag           ==1) i|=0x08;	//44
        if(FR.In_Gear_flag                      ==1) i|=0x10;	//45
        if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;	//46
        if(FR.LFC_built_reapply_flag            ==1) i|=0x40;	//47
        if(SPOLD_RESET_fr                       ==1) i|=0x80;	//48

        CAN_LOG_DATA[77] = i;

        i=0;
        if(RL.lcabsu1CaptureEstPressForSkid     ==1) i|=0x01;	//49
        if(RL.L_SLIP_CONTROL                    ==1) i|=0x02;	//50
        if(RL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;	//51
        if(RL.MSL_BASE                          ==1) i|=0x08;	//52
        if(RL.Duty_Limitation_flag              ==1) i|=0x10;	//53
        if(RL.lcabsu1CaptureEstPressAftDump     ==1) i|=0x20;   //54			/*RL.LFC_n_th_deep_slip_pre_flag*/
        if(RL.LFC_built_reapply_flag            ==1) i|=0x40;	//55
        if(RR.lcabsu1CaptureEstPressAftDump     ==1) i|=0x80;   //56			/*SPOLD_RESET_rl*/

        CAN_LOG_DATA[78] = i;

        i=0;
        if(RR.lcabsu1CaptureEstPressForSkid     ==1) i|=0x01;	//57
        if(RR.L_SLIP_CONTROL                    ==1) i|=0x02;	//58
        if(RR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;	//59
        if(RR.MSL_BASE                          ==1) i|=0x08;	//60
        if(0        ==1) i|=0x10;	//61
        if(0       	==1) i|=0x20;	//62
        if(RR.LFC_built_reapply_flag            ==1) i|=0x40;   //63			/*RR.LFC_built_reapply_flag*/
      #if __HOB
        if(lcu1HobOnFlag                        ==1) i|=0x80;   //64
      #else
        if(SPOLD_RESET_rr                       ==1) i|=0x80;	//64
      #endif

        CAN_LOG_DATA[79] = i;
}
/********************************************************************************
* FUNCTION NAME:        LSESP_vCallESCoffsetLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ESP Offset
********************************************************************************/
void LSESP_vCallESCoffsetLogData(void)
{
	uint8_t i;
	uint8_t j;

	CAN_LOG_DATA[0]	 = (uint8_t) (system_loop_counter);					//ch1
	CAN_LOG_DATA[1]	 = (uint8_t) (system_loop_counter>>8);
	CAN_LOG_DATA[2]	 = (uint8_t) (FL.vrad_resol_change);				//ch2
	CAN_LOG_DATA[3]	 = (uint8_t) (FL.vrad_resol_change>>8);
	CAN_LOG_DATA[4]	 = (uint8_t) (FR.vrad_resol_change);				//ch3
	CAN_LOG_DATA[5]	 = (uint8_t) (FR.vrad_resol_change>>8);
	CAN_LOG_DATA[6]	 = (uint8_t) (RL.vrad_resol_change);				//ch4
	CAN_LOG_DATA[7]	 = (uint8_t) (RL.vrad_resol_change>>8);
	CAN_LOG_DATA[8]	 = (uint8_t) (RR.vrad_resol_change);				//ch5
	CAN_LOG_DATA[9]	 = (uint8_t) (RR.vrad_resol_change>>8);

	CAN_LOG_DATA[10] = (uint8_t) (lsesps16EEPYawStandOfsRead);			//ch6
	CAN_LOG_DATA[11] = (uint8_t) (lsesps16EEPYawStandOfsRead>>8);

    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
	CAN_LOG_DATA[12] = (uint8_t) (lsesps16BrkAppSenFiltF);	            //ch7  /*090731 for compile */
	#else
	CAN_LOG_DATA[12] = (uint8_t) (0);	                                //ch7  /*090731 for compile */
	#endif	
	
    #if ( __FL_PRESS==ENABLE )	
	CAN_LOG_DATA[13] = (uint8_t) (PRESS_FL.s16premFiltPress);		    //ch8
	#else
	CAN_LOG_DATA[13] = (uint8_t) (0);		    //ch8	
	#endif

	CAN_LOG_DATA[14] = (uint8_t) (vref5_resol_change);					//ch9
	CAN_LOG_DATA[15] = (uint8_t) (vref5_resol_change>>8);

	CAN_LOG_DATA[16] = (uint8_t) (inhibition_number	=INHIBITION_INDICATOR()) ; //ch10
	CAN_LOG_DATA[17] = (uint8_t) (u8CycleTime);//ch11

#if __4WD || __AX_SENSOR
	CAN_LOG_DATA[18] = (uint8_t) ((along>127)?127:((along<-127)?-127:(CHAR)along) ); //ch12
#else
	CAN_LOG_DATA[18] = (uint8_t) ((afz>127)?127:((afz<-127)?-127:(CHAR)afz));   //ch12
#endif

	//ch17
    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    CAN_LOG_DATA[19] = (uint8_t) (lsesps16BrkAppSenPDTFilt);		        //ch13   /*090731 for compile */
    #else
    CAN_LOG_DATA[19] = (uint8_t) (0);	
    #endif
    
    #if ( __FL_PRESS==ENABLE )
	CAN_LOG_DATA[20] = (uint8_t) (PRESS_FL.s16premPressOfs_f);		//ch14
	#else
	CAN_LOG_DATA[20] = (uint8_t) (0);		//ch14
	#endif
	
	#if ( __FR_PRESS==ENABLE )
	CAN_LOG_DATA[21] = (uint8_t) (PRESS_FR.s16premPressOfs_f);		//ch15
	#else
	CAN_LOG_DATA[21] = (uint8_t) (0);		//ch15
	#endif
	
	#if ( __RL_PRESS==ENABLE )
	CAN_LOG_DATA[22] = (uint8_t) (PRESS_RL.s16premPressOfs_f);		//ch16
	#else
	CAN_LOG_DATA[22] = (uint8_t) (0);		//ch16
	#endif

	#if ( __RR_PRESS==ENABLE )	
	CAN_LOG_DATA[23] = (uint8_t) (PRESS_RR.s16premPressOfs_f);		//ch17	
	#else
	CAN_LOG_DATA[23] = (uint8_t) (0);		//ch17		
	#endif

	CAN_LOG_DATA[24] = (uint8_t) (steer_1_10deg2);					//ch18
	CAN_LOG_DATA[25] = (uint8_t) (steer_1_10deg2>>8);
	CAN_LOG_DATA[26] = (uint8_t) (yaw_1_100deg2);					//ch19
	CAN_LOG_DATA[27] = (uint8_t) (yaw_1_100deg2>>8);
	CAN_LOG_DATA[28] = (uint8_t) (a_lat_1_1000g2);					//ch20
	CAN_LOG_DATA[29] = (uint8_t) (a_lat_1_1000g2>>8);

    #if ( __FR_PRESS==ENABLE )
	CAN_LOG_DATA[30] = (uint8_t) (PRESS_FR.s16premFiltPress);		//ch21
	#else
	CAN_LOG_DATA[30] = (uint8_t) (0);		//ch21	
	#endif
	
    #if ( __RL_PRESS==ENABLE )	
	CAN_LOG_DATA[31] = (uint8_t) (PRESS_RL.s16premFiltPress);		//ch22
	#else
	CAN_LOG_DATA[31] = (uint8_t) (0);		//ch22	
	#endif
	
    #if ( __RR_PRESS==ENABLE )		
	CAN_LOG_DATA[32] = (uint8_t) (PRESS_RR.s16premFiltPress);		//ch23
	#else
	CAN_LOG_DATA[32] = (uint8_t) (0);		//ch23	
	#endif

	CAN_LOG_DATA[33] = (uint8_t) (mtp);								//ch24
	CAN_LOG_DATA[34] = (uint8_t) (lsesps16YawStandOffset);			//ch25
	CAN_LOG_DATA[35] = (uint8_t) (lsesps16YawStandOffset>>8);
	CAN_LOG_DATA[36] = (uint8_t) (KSTEER.offset_of_eeprom);			//ch26
	CAN_LOG_DATA[37] = (uint8_t) (KSTEER.offset_of_eeprom>>8);

	CAN_LOG_DATA[38] = (uint8_t) (ltcss16ETCSEnterExitFlags);
	CAN_LOG_DATA[39] = (uint8_t) ((Bank_Angle_Final/10)>127)?127:(((Bank_Angle_Final/10)<-127)?-127:((CHAR)(Bank_Angle_Final/10)));
	//ch28

	CAN_LOG_DATA[40] = (uint8_t) (wstr);								//ch29
	CAN_LOG_DATA[41] = (uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] = (uint8_t) (yaw_out);								//ch30
	CAN_LOG_DATA[43] = (uint8_t) (yaw_out>>8);
	CAN_LOG_DATA[44] = (uint8_t) (rf2);									//ch31
	CAN_LOG_DATA[45] = (uint8_t) (rf2>>8);

	CAN_LOG_DATA[46] = (uint8_t) ((Beta_MD/20)>127)?127:(((Beta_MD/20)<-127)?-127:((CHAR)(Beta_MD/20)));
	//ch32
	CAN_LOG_DATA[47] = (uint8_t) ((Beta_Dyn/20)>127)?127:(((Beta_Dyn/20)<-127)?-127:((CHAR)(Beta_Dyn/20)));
	//ch33


	CAN_LOG_DATA[48] = (uint8_t) (KSTEER.model_avr);					//ch34
	CAN_LOG_DATA[49] = (uint8_t) (KSTEER.model_avr>>8);                 //STR

	CAN_LOG_DATA[50] = (uint8_t) (alat);							//ch35
	CAN_LOG_DATA[51] = (uint8_t) (alat>>8);
	
	/* F0212 Signal Check */
//	#if ( __AHB_GEN3_SYSTEM==DISABLE ) && __IDB_LOGIC==DISABLE
	#if ENABLE
	CAN_LOG_DATA[52] = (uint8_t) (KYAW.model_avr);						//ch36
	CAN_LOG_DATA[53] = (uint8_t) (KYAW.model_avr>>8);
	CAN_LOG_DATA[54] = (uint8_t) (steer_offset_stable);					//ch37
	CAN_LOG_DATA[55] = (uint8_t) (steer_offset_stable>>8);  //Quick offset: stST

	CAN_LOG_DATA[56] = (uint8_t) (steer_offset_f);								//ch38
	CAN_LOG_DATA[57] = (uint8_t) (steer_offset_f>>8);
	if(!KSTEER.FIRST_OFFSET_CAL_FLG)
	{
	    CAN_LOG_DATA[58] = (uint8_t) (KSTEER.offset_at_15kph);								//ch39
	    CAN_LOG_DATA[59] = (uint8_t) (KSTEER.offset_at_15kph>>8);
	}
	else
	{
	CAN_LOG_DATA[58] = (uint8_t) (KSTEER.offset_60avr);								//ch39
	CAN_LOG_DATA[59] = (uint8_t) (KSTEER.offset_60avr>>8);
  }

	CAN_LOG_DATA[60] = (uint8_t) (KYAW.offset_60avr);							//ch40
	CAN_LOG_DATA[61] = (uint8_t) (KYAW.offset_60avr>>8);  // 정차 offset도 없는 경우
	CAN_LOG_DATA[62] = (uint8_t) (KLAT.offset_60avr);							//ch41
	CAN_LOG_DATA[63] = (uint8_t) (KLAT.offset_60avr>>8);
	/*CAN_LOG_DATA[64] = (uint8_t) (mpress);								//ch42
	CAN_LOG_DATA[65] = (uint8_t) (mpress>>8);*/
	CAN_LOG_DATA[64] = (uint8_t) (lsesps16EstBrkPressByBrkPedalF);								//ch42
	CAN_LOG_DATA[65] = (uint8_t) (lsesps16EstBrkPressByBrkPedalF>>8);
	#else
	CAN_LOG_DATA[52] = (uint8_t) (Tc_under_pressure_p);						//ch36
	CAN_LOG_DATA[53] = (uint8_t) (Tc_under_pressure_p>>8);
	CAN_LOG_DATA[54] = (uint8_t) (Tc_under_pressure_s);					//ch37
	CAN_LOG_DATA[55] = (uint8_t) (Tc_under_pressure_s>>8);  //Quick offset: stST

	CAN_LOG_DATA[56] = (uint8_t) (0);								//ch38 lsesps16circuitPress_Pri
	CAN_LOG_DATA[57] = (uint8_t) (0>>8);

	CAN_LOG_DATA[58] = (uint8_t) (0);								//ch39 lsesps16circuitPress_Scn
	CAN_LOG_DATA[59] = (uint8_t) (0>>8);

	//CAN_LOG_DATA[60] = (uint8_t) (lcans16AhbCircuitBoostPressP );							//ch40
	//CAN_LOG_DATA[61] = (uint8_t) (lcans16AhbCircuitBoostPressP>>8); 
	//CAN_LOG_DATA[62] = (uint8_t) (lcans16AhbCircuitBoostPressS );							//ch41
	//CAN_LOG_DATA[63] = (uint8_t) (lcans16AhbCircuitBoostPressS >>8);
	CAN_LOG_DATA[64] = (uint8_t) (lsesps16EstBrkPressByBrkPedalF );								//ch42
	CAN_LOG_DATA[65] = (uint8_t) (lsesps16EstBrkPressByBrkPedalF >>8);
	#endif


	tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

	if(ESP_ERROR_FLG)				   tempB5|=0x01;
	if(BLS_PIN)						   tempB5|=0x02;
	if(ABS_fz)						   tempB5|=0x04;
	if(EBD_RA)						   tempB5|=0x08;
	if(BTC_fz)						   tempB5|=0x10;
	if(ETCS_ON)						   tempB5|=0x20;
	if(YAW_CDC_WORK)				   tempB5|=0x40;
	if(ESP_TCS_ON)					   tempB5|=0x80;

	if(FLAG_BETA_UNDER_DRIFT)		   tempB4|=0x01;
	if(FLAG_ACTIVE_BRAKING)			   tempB4|=0x02;
	if(PARTIAL_BRAKE)				   tempB4|=0x04;
	if(FLAG_BANK_DETECTED)			   tempB4|=0x08;
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)	    
	if(MPRESS_BRAKE_ON)				   tempB4|=0x10;
#else
	if(lsespu1MpresBrkOnByAHBgen3)				   tempB4|=0x10;
#endif
 	if(MOT_ON_FLG)				       tempB4|=0x20;
	if(BACK_DIR)				       tempB4|=0x40;
	if(yaw_offset_srch_ok_flg)		   tempB4|=0x80;

	if(AV_VL_fl)					   tempB3|=0x01;
	if(HV_VL_fl)					   tempB3|=0x02;

	#if ( __FL_PRESS==ENABLE )	
	if(PRESS_FL.u1prem_PressOffsetOK)  tempB3|=0x04;
	#else
	if(0)                              tempB3|=0x04;
	#endif	
	
	#if ( __FR_PRESS==ENABLE )	
	if(PRESS_FR.u1prem_PressOffsetOK)  tempB3|=0x08;
	#else
	if(0)                              tempB3|=0x08;
	#endif	
	
	#if ( __RL_PRESS==ENABLE )	
	if(PRESS_RL.u1prem_PressOffsetOK)  tempB3|=0x10;		
	#else
	if(0)                              tempB3|=0x10;	
	#endif	
	
	#if ( __RR_PRESS==ENABLE )
	if(PRESS_RR.u1prem_PressOffsetOK)  tempB3|=0x20;
	#else
	if(0)                              tempB3|=0x20;
	#endif	
	if(KYAW.STRAIGHT_FLAG)			   tempB3|=0x40;    /*OVER_UNDER_JUMP*/
	if(KYAW.OFFSET_60AVR_CAL_OK_FLG)   tempB3|=0x80;    //

	if(AV_VL_fr)					   tempB2|=0x01;
	if(HV_VL_fr)					   tempB2|=0x02;
	if(yaw_lg_tmp_read_done_flg)	   tempB2|=0x04;
	
	#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))	
	if(ESP_PDT.lsespu1ESPEepOfsCalOkFlg)			   tempB2|=0x08;   /*090731 for compile */
	#else
	if(0)                       	   tempB2|=0x08;   /*090731 for compile */
	#endif	

	if(standstill_on_flg)			   tempB2|=0x10;
	if(lat_g_100ms_slope_ok_flg)	   tempB2|=0x20;
	if(steer_initial_check_flg||steer_final_check_flg)		  	tempB2|=0x40;
	if(KSTEER.MODEL_OK_AT_15KPH)	   tempB2|=0x80;

	if(AV_VL_rl)					   tempB1|=0x01;
	if(HV_VL_rl)					   tempB1|=0x02;
	if(SAS_CHECK_OK)				   tempB1|=0x04;
	if(STEER_STABLE_OK)				   tempB1|=0x08;
	if(KSTEER.FIRST_OFFSET_CAL_FLG)    tempB1|=0x10;
	if(KSTEER.OFFSET_60AVR_CAL_OK_FLG) tempB1|=0x20;
	if(KLAT.FIRST_OFFSET_CAL_FLG)	   tempB1|=0x40;
	if(KYAW.FIRST_OFFSET_CAL_FLG)	   tempB1|=0x80;


	if(AV_VL_rr)			           tempB0|=0x01;
	if(HV_VL_rr)			           tempB0|=0x02;
	if(yaw_100ms_slope_ok_flg)		   tempB0|=0x04;
	if(yaw_turn_table_sus_flg)		   tempB0|=0x08;
	if(lsespu1YawGstandstillflg)	   tempB0|=0x10;
	if(KLAT.OFFSET_60AVR_CAL_OK_FLG)   tempB0|=0x20;
	if(S_VALVE_RIGHT)		           tempB0|=0x40;
	if(S_VALVE_LEFT)			       tempB0|=0x80;


	CAN_LOG_DATA[66] = (uint8_t) (tempB5); //ch43
	CAN_LOG_DATA[67] = (uint8_t) (tempB4); //ch44
	CAN_LOG_DATA[68] = (uint8_t) (tempB3); //ch45
	CAN_LOG_DATA[69] = (uint8_t) (tempB2); //ch46
	CAN_LOG_DATA[70] = (uint8_t) (tempB1); //ch47
	CAN_LOG_DATA[71] = (uint8_t) (tempB0); //ch48

	CAN_LOG_DATA[72] = (uint8_t) (KYAW.offset_of_eeprom);          //ch49
	CAN_LOG_DATA[73] = (uint8_t) (KYAW.offset_of_eeprom>>8);
	CAN_LOG_DATA[74] = (uint8_t) (KLAT.offset_of_eeprom);          //ch49
	CAN_LOG_DATA[75] = (uint8_t) (KLAT.offset_of_eeprom>>8);

	CAN_LOG_DATA[76] = (uint8_t) (yaw_offset_f);                   //ch51
	CAN_LOG_DATA[77] = (uint8_t) (yaw_offset_f>>8);
	CAN_LOG_DATA[78] = (uint8_t) (alat_offset_f);                   //ch52
	CAN_LOG_DATA[79] = (uint8_t) (alat_offset_f>>8);

}

#if  (__Ax_OFFSET_MON_DRV==1)
void    LSESP_vCallAxOffsetLogData(void)
{
	uint8_t i;
	uint8_t j;

	CAN_LOG_DATA[0]	 = (uint8_t)(system_loop_counter);					 			 /* ch1 */
	CAN_LOG_DATA[1]	 = (uint8_t)(system_loop_counter>>8);
	CAN_LOG_DATA[2]	 = (uint8_t)(FL.vrad_resol_change);				 				 /* ch2 */
	CAN_LOG_DATA[3]	 = (uint8_t)(FL.vrad_resol_change>>8);
	CAN_LOG_DATA[4]	 = (uint8_t)(FR.vrad_resol_change);				 				 /* ch3 */
	CAN_LOG_DATA[5]	 = (uint8_t)(FR.vrad_resol_change>>8);
	CAN_LOG_DATA[6]	 = (uint8_t)(RL.vrad_resol_change);				 				 /* ch4 */
	CAN_LOG_DATA[7]	 = (uint8_t)(RL.vrad_resol_change>>8);

	CAN_LOG_DATA[8]	 = (uint8_t)(RR.vrad_resol_change);				 				 /* ch5 */
	CAN_LOG_DATA[9]	 = (uint8_t)(RR.vrad_resol_change>>8);
	CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);				     			 /* ch6 */
	CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);
	CAN_LOG_DATA[12] = (uint8_t)(yaw_out);								             /* ch7 */
	CAN_LOG_DATA[13] = (uint8_t)(yaw_out>>8);
	CAN_LOG_DATA[14] = (uint8_t)(rf2);									             /* ch8 */
	CAN_LOG_DATA[15] = (uint8_t)(rf2>>8);

	CAN_LOG_DATA[16] = (uint8_t)(wstr);								         		 /* ch9 */
	CAN_LOG_DATA[17] = (uint8_t)(wstr>>8);
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
	CAN_LOG_DATA[18] = (uint8_t)(mpress);											 /* ch10 */
	CAN_LOG_DATA[19] = (uint8_t)(mpress>>8);
#else
	CAN_LOG_DATA[18] = (uint8_t)(lsesps16MpressByAHBgen3);											 /* ch10 */
	CAN_LOG_DATA[19] = (uint8_t)(lsesps16MpressByAHBgen3>>8);
#endif
	CAN_LOG_DATA[20] = (uint8_t)(mtp);												 /* ch11 */
	CAN_LOG_DATA[21] = (uint8_t)(gs_pos);										     /* ch12 */
	CAN_LOG_DATA[22] = (uint8_t)(gs_sel);											 /* ch13 */					
	CAN_LOG_DATA[23] = (uint8_t)(gear_state);									     /* ch14 */			       

	CAN_LOG_DATA[24] = (uint8_t)(ALGoff1.lsabsu8Ax10secOffsetCnt);		             /* ch15 */
	CAN_LOG_DATA[25] = (uint8_t)(lsabsu8AxRollResist);								 /* ch16 */
	CAN_LOG_DATA[26] = (uint8_t)(lsabss16AxBrakeModelResist);					     /* ch17 */				
	CAN_LOG_DATA[27] = (uint8_t)(lsabss16AxBrakeModelResist>>8);
	CAN_LOG_DATA[28] = (uint8_t)(lsabss16AxEngModelResist);    				         /* ch18 */
	CAN_LOG_DATA[29] = (uint8_t)(lsabss16AxEngModelResist>>8); 
	CAN_LOG_DATA[30] = (uint8_t)(lsabss16AxAeroResist);								 /* ch19 */
	CAN_LOG_DATA[31] = (uint8_t)(lsabss16AxAeroResist>>8);						

	CAN_LOG_DATA[32] = (uint8_t)(lsabss16AxSenAirSus);					 			 /* ch20 */
	CAN_LOG_DATA[33] = (uint8_t)(lsabss16AxSenAirSus>>8);
	CAN_LOG_DATA[34] = (uint8_t)(lsabss16AxDynModel);				 			     /* ch21 */
	CAN_LOG_DATA[35] = (uint8_t)(lsabss16AxDynModel>>8);
	CAN_LOG_DATA[36] = (uint8_t)(lsabss16AxOfsDyModelRaw);						     /* ch22 */
	CAN_LOG_DATA[37] = (uint8_t)(lsabss16AxOfsDyModelRaw>>8);
	CAN_LOG_DATA[38] = (uint8_t)(ALGoff1.lsabss16Ax10secOffMean);	                 /* ch23 */
	CAN_LOG_DATA[39] = (uint8_t)(ALGoff1.lsabss16Ax10secOffMean>>8);

	CAN_LOG_DATA[40] = (uint8_t)(ALGoff1.lsabss16Ax100secOffMean);		             /* ch24 */
	CAN_LOG_DATA[41] = (uint8_t)(ALGoff1.lsabss16Ax100secOffMean>>8);
	CAN_LOG_DATA[42] = (uint8_t)(lsabss16AxOffsetDrvf);				 				 /* ch25 */
	CAN_LOG_DATA[43] = (uint8_t)(lsabss16AxOffsetDrvf>>8);
	CAN_LOG_DATA[44] = (uint8_t)(lsabss16AxOffsetTOTf);				 		         /* ch26 */
	CAN_LOG_DATA[45] = (uint8_t)(lsabss16AxOffsetTOTf>>8);
	CAN_LOG_DATA[46] = (uint8_t)(lsabss16AxOffsetf);				 				 /* ch27 */
	CAN_LOG_DATA[47] = (uint8_t)(lsabss16AxOffsetf>>8);

	CAN_LOG_DATA[48] = (uint8_t)(lss16absAxOffsetComp1000g);		                 /* ch28 */
	CAN_LOG_DATA[49] = (uint8_t)(lss16absAxOffsetComp1000g>>8);
	CAN_LOG_DATA[50] = (uint8_t)(wbs16AxDrivingOfsFirstEep);				 	     /* ch29 */
	CAN_LOG_DATA[51] = (uint8_t)(wbs16AxDrivingOfsFirstEep>>8);
#if	(__Ax_OFFSET_MON==1)
	CAN_LOG_DATA[52] = (uint8_t)(wbs16AxOfsFirstEep);				 		         /* ch30 */
	CAN_LOG_DATA[53] = (uint8_t)(wbs16AxOfsFirstEep>>8);
#else
	CAN_LOG_DATA[52] = (uint8_t)(0);				 		       					 /* ch30 */
	CAN_LOG_DATA[53] = (uint8_t)(0);
#endif
	CAN_LOG_DATA[54] = (uint8_t)(eng_rpm);				 					         /* ch31 */
	CAN_LOG_DATA[55] = (uint8_t)(eng_rpm>>8);  

	CAN_LOG_DATA[56] = (uint8_t)(tc_rpm);		                                     /* ch32 */
	CAN_LOG_DATA[57] = (uint8_t)(tc_rpm>>8);
	CAN_LOG_DATA[58] = (uint8_t)(eng_torq);				 	 					     /* ch33 */
	CAN_LOG_DATA[59] = (uint8_t)(eng_torq>>8);
	CAN_LOG_DATA[60] = (uint8_t)(lsabsu16AxOffCNT);				 		       	     /* ch34 */
	CAN_LOG_DATA[61] = (uint8_t)(lsabsu16AxOffCNT>>8);
	CAN_LOG_DATA[62] = (uint8_t)(alat);				 					 			 /* ch35 */
	CAN_LOG_DATA[63] = (uint8_t)(alat>>8); 
	
	CAN_LOG_DATA[64] = (uint8_t)(0);		       									 /* ch36 */
	CAN_LOG_DATA[65] = (uint8_t)(0>>8);
	CAN_LOG_DATA[66] = (uint8_t)(0);				 	 							 /* ch37 */
	CAN_LOG_DATA[67] = (uint8_t)(0>>8);
	CAN_LOG_DATA[68] = (uint8_t)(0);				 		                         /* ch38 */
	CAN_LOG_DATA[69] = (uint8_t)(0>>8); 
	CAN_LOG_DATA[70] = (uint8_t)(lcabsu16FuncStartTimeAx);				 		     /* ch39 */
	CAN_LOG_DATA[71] = (uint8_t)(lcabsu16FuncStartTimeAx>>8); 
	
	tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=tempB6=tempB7=0;

	if(ESP_ERROR_FLG)				       			tempB7|=0x01;
	if(BLS_PIN)						         	    tempB7|=0x02;
	if(ABS_fz)						         		tempB7|=0x04;
	if(EBD_RA)						         		tempB7|=0x08;
	if(BTC_fz)						         		tempB7|=0x10;
	if(ETCS_ON)						         		tempB7|=0x20;
	if(YAW_CDC_WORK)				       			tempB7|=0x40;
	if(ESP_TCS_ON)					       			tempB7|=0x80;

	if(FLAG_BETA_UNDER_DRIFT)		   			    tempB6|=0x01;
	if(FLAG_ACTIVE_BRAKING)			   			    tempB6|=0x02;
	if(PARTIAL_BRAKE)				       			tempB6|=0x04;
	if(FLAG_BANK_DETECTED)			   			    tempB6|=0x08;
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)	    
	if(MPRESS_BRAKE_ON)				     			tempB6|=0x10;
#else
	if(lsespu1MpresBrkOnByAHBgen3)				     			tempB6|=0x10;
#endif
 	if(MOT_ON_FLG)				         			tempB6|=0x20;
	if(BACK_DIR)				           			tempB6|=0x40;
	if(yaw_offset_srch_ok_flg)		 			    tempB6|=0x80;

	if(ESP_ROUGH_ROAD)					   			tempB5|=0x01;
	if(SAS_CHECK_OK)					     		tempB5|=0x02;
	if(lsabsu1AxOfsNone)					 		tempB5|=0x04;
	if(lsabsu1FreeRollFlag)		 		 			tempB5|=0x08;
	if(ALGoff1.lsabsu1Ax10secOffOK)			        tempB5|=0x10;		
	if(ALGoff1.lsabsu1Ax100secOffOK)		        tempB5|=0x20;
	if(wbu1AxDrivingOfsReadEep)				 	    tempB5|=0x40;
	if(lsabsu1AxDrvOfsEEPwriteReq)		            tempB5|=0x80;    

	if(lsabsu1AxOfsCalcInhibit)					    tempB4|=0x01;
	if(cbit_process_step)					        tempB4|=0x02;
	if(fu1BLSErrDet)	              	            tempB4|=0x04;  
	if(fu1MainCanLineErrDet)			 			tempB4|=0x08;		
	if(fu1EMSTimeOutErrDet)				   		    tempB4|=0x10;
	if(fu1TCUTimeOutErrDet)	 						tempB4|=0x20;
	if(fu1MCPErrorDet)		  						tempB4|=0x40;
	if(fu1MCPSusDet)	 							tempB4|=0x80;

	if(fu1AxErrorDet)					     		tempB3|=0x01;
	if(fu1AxSusDet)					     			tempB3|=0x02;
	if(fu1YawErrorDet)					 			tempB3|=0x04;
  if(fu1WheelFLErrDet)					 			tempB3|=0x08;
	if(fu1WheelFRErrDet)      					    tempB3|=0x10;
	if(fu1WheelRLErrDet)	        			    tempB3|=0x20;   
	if(fu1WheelRRErrDet)		 					tempB3|=0x40;   
	if(fu1ClutchSwitch)	        				    tempB3|=0x80;

	if(lsabsu1AxDyOffsetFlat)			            tempB2|=0x01;
	if(lsabsu1AxKineOffsetFlat)			            tempB2|=0x02;
	if(lsabsu1AxOfsAccAftFlag)		                tempB2|=0x04;
	if(lsabsu1AxBrakeAfter)			 				tempB2|=0x08;
	if(lsabsu1AxOfsWheelSlipFlg)			 	    tempB2|=0x10;
	if(lsabsu1AxEngineDrag)				     	    tempB2|=0x20;
	if(lsabsu1AxDriveModeDrag)		                tempB2|=0x40;	 
	if(lsabsu1AxMTPAfterFlg)			 			tempB2|=0x80;
		
	if(lsabsu1AxOfsSportModeFlg)			        tempB1|=0x01;
	if(lsabsu1AxOfsWiperOnFlg)			            tempB1|=0x02;
	if(lsabsu1AxOffsetSuspect)		                tempB1|=0x04;
	if(PARKING_BRAKE_OPERATION)			 		    tempB1|=0x08;
	if(BACKWARD_MOVE)			 				    tempB1|=0x10;
#if __TSP	
	if(TSP_ON)				     					tempB1|=0x20;
#else
	if(0)				     						tempB1|=0x20;
#endif	
	if(lsespu1WiperActFlg)		      		        tempB1|=0x40;	 
	if(ldabsu1ParkingBrkActSignal)			        tempB1|=0x80;
		
	if(fu1AxSenPwr1sOk)			  					tempB0|=0x01;
	if(0)			            					tempB0|=0x02;
	if(0)		      								tempB0|=0x04;
	if(0)			 								tempB0|=0x08;
	if(0)			 								tempB0|=0x10;
	if(0)				     						tempB0|=0x20;
	if(0)		      								tempB0|=0x40;	 
	if(0)			 								tempB0|=0x80;	 	 
 
	CAN_LOG_DATA[72] = tempB7; /* ch40 */
	CAN_LOG_DATA[73] = tempB6; /* ch41 */
	CAN_LOG_DATA[74] = tempB5; /* ch42 */
	CAN_LOG_DATA[75] = tempB4; /* ch43 */
	CAN_LOG_DATA[76] = tempB3; /* ch44 */
	CAN_LOG_DATA[77] = tempB2; /* ch45 */
	CAN_LOG_DATA[78] = tempB1; /* ch45 */
	CAN_LOG_DATA[79] = tempB0; /* ch47 */

}
#endif

#if  (__ADAPTIVE_MODEL_VCH_GEN==ENABLE) || (__ADAPTIVE_MODEL_VELO==ENABLE)
void    LSESP_vCallAdaptiveModelLogData(void)
{
		
	if(u8DataCNT<9)
	{
	    u8DataCNT=u8DataCNT+1;
	}
	else
	{
	    u8DataCNT=0;
	}
	

	CAN_LOG_DATA[0]	 = (uint8_t) ( system_loop_counter );					       //ch1
	CAN_LOG_DATA[1]	 = (uint8_t) ( system_loop_counter>>8 );                     
	CAN_LOG_DATA[2]	 = (uint8_t) ( FL.vrad_resol_change );				       //ch2
	CAN_LOG_DATA[3]	 = (uint8_t) ( FL.vrad_resol_change>>8 );                    
	CAN_LOG_DATA[4]	 = (uint8_t) ( FR.vrad_resol_change );				       //ch3
	CAN_LOG_DATA[5]	 = (uint8_t) ( FR.vrad_resol_change>>8 );                    
	CAN_LOG_DATA[6]	 = (uint8_t) ( RL.vrad_resol_change );				       //ch4
	CAN_LOG_DATA[7]	 = (uint8_t) ( RL.vrad_resol_change>>8 );                    
	CAN_LOG_DATA[8]	 = (uint8_t) ( RR.vrad_resol_change );				       //ch5
	CAN_LOG_DATA[9]	 = (uint8_t) ( RR.vrad_resol_change>>8 );                    
                                                                   
	CAN_LOG_DATA[10] = (uint8_t) ( ldesps16AdmVchGEN );    				       //ch6
	CAN_LOG_DATA[11] = (uint8_t) ( ldesps16AdmVchGEN>>8 );              
                                                                   
	CAN_LOG_DATA[12] = (uint8_t) ( ADMvelo_LH.ldespu8ADM_1SecCalcCnt );	       //ch7
	CAN_LOG_DATA[13] = (uint8_t) ( ADMvelo_RH.ldespu8ADM_1SecCalcCnt );  	       //ch8
                                                                   
	CAN_LOG_DATA[14] = (uint8_t) ( vref5_resol_change );					       //ch9
	CAN_LOG_DATA[15] = (uint8_t) ( vref5_resol_change>>8 );                      
                                                                   
	CAN_LOG_DATA[16] = (uint8_t) ( (ldesps16AdmVcrtRatioApplyF/10)  );           //ch10 		    
	CAN_LOG_DATA[17] = (uint8_t) ( ldesps16AdmVeloRatioCalc );                   //ch11
                                                                   
    CAN_LOG_DATA[18] = (uint8_t) ( ADMvelo_LH.ldespu8ADM_10SecCalcCnt );         //ch12
	CAN_LOG_DATA[19] = (uint8_t) ( ADMvelo_RH.ldespu8ADM_10SecCalcCnt );	       //ch13
	
	CAN_LOG_DATA[20] = (uint8_t) ( ADMvelo_LH.ldesps16ADM_10SecCalcMean );        //ch14
	CAN_LOG_DATA[21] = (uint8_t) ( ADMvelo_RH.ldesps16ADM_10SecCalcMean );		//ch15
	CAN_LOG_DATA[22] = (uint8_t) ( ADMvelo_LH.ldesps16ADM_100SecCalcMean );		//ch16
	CAN_LOG_DATA[23] = (uint8_t) ( ADMvelo_RH.ldesps16ADM_100SecCalcMean );		//ch17

	CAN_LOG_DATA[24] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_10SecCalcMean );		//ch18
	CAN_LOG_DATA[25] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_10SecCalcMean>>8 );
	CAN_LOG_DATA[26] = (uint8_t) ( ADMvchGEN_RH.ldesps16ADM_10SecCalcMean );		//ch19
	CAN_LOG_DATA[27] = (uint8_t) ( ADMvchGEN_RH.ldesps16ADM_10SecCalcMean>>8 );
	CAN_LOG_DATA[28] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_100SecCalcMean );      //ch20  
	CAN_LOG_DATA[29] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_100SecCalcMean>>8 );               
 
	CAN_LOG_DATA[30] = (uint8_t) ( ADMvchGEN_LH.ldespu8ADM_1SecCalcCnt );          //ch21
	CAN_LOG_DATA[31] = (uint8_t) ( ADMvchGEN_RH.ldespu8ADM_1SecCalcCnt );	         //ch22


	CAN_LOG_DATA[32] = (uint8_t) ( ((WSTR_ACC.lsesps16SignalAccel)>127)?127:(((WSTR_ACC.lsesps16SignalAccel)<-127)?-127:((CHAR)(WSTR_ACC.lsesps16SignalAccel))) );						      //ch23   // (UCHAR)(eng_torq/5) );
	CAN_LOG_DATA[33] = (uint8_t) ( ((YAWM_ACC.lsesps16SignalAccel)>127)?127:(((YAWM_ACC.lsesps16SignalAccel)<-127)?-127:((CHAR)(YAWM_ACC.lsesps16SignalAccel))) ); ////mtp );										      //ch24  
	CAN_LOG_DATA[34] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_1SecCalc[u8DataCNT] );  //ch25
	CAN_LOG_DATA[35] = (uint8_t) ( ADMvchGEN_LH.ldesps16ADM_1SecCalc[u8DataCNT]>>8 );
	CAN_LOG_DATA[36] = (uint8_t) ( ADMvchGEN_RH.ldesps16ADM_1SecCalc[u8DataCNT] );  //ch26
	CAN_LOG_DATA[37] = (uint8_t) ( ADMvchGEN_RH.ldesps16ADM_1SecCalc[u8DataCNT]>>8 );


	CAN_LOG_DATA[38] = (uint8_t) ( ((Bank_Angle_K/10)>127)?127:(((Bank_Angle_K/10)<-127)?-127:((CHAR)(Bank_Angle_K/10))) );  //ch 27
	CAN_LOG_DATA[39] = (uint8_t) ( ((Bank_Angle_Final/10)>127)?127:(((Bank_Angle_Final/10)<-127)?-127:((CHAR)(Bank_Angle_Final/10))) );
	//ch28

	CAN_LOG_DATA[40] = (uint8_t) ( wstr );								           //ch29
	CAN_LOG_DATA[41] = (uint8_t) ( wstr>>8 );

	CAN_LOG_DATA[42] = (uint8_t) ( yaw_out );								           //ch30
	CAN_LOG_DATA[43] = (uint8_t) ( yaw_out>>8 );
	CAN_LOG_DATA[44] = (uint8_t) ( rf2 );									           //ch31
	CAN_LOG_DATA[45] = (uint8_t) ( rf2>>8 );

	CAN_LOG_DATA[46] = (uint8_t) ( ((Beta_MD/20)>127)?127:(((Beta_MD/20)<-127)?-127:((CHAR)(Beta_MD/20))) );
	//ch32
	CAN_LOG_DATA[47] = (uint8_t) ( ((Beta_Dyn/20)>127)?127:(((Beta_Dyn/20)<-127)?-127:((CHAR)(Beta_Dyn/20))) );
	//ch33

	CAN_LOG_DATA[48] = (uint8_t) ( ADMvelo_LH.ldesps16ADM_1SecCalc[u8DataCNT] );		//ch34
	CAN_LOG_DATA[49] = (uint8_t) ( ADMvelo_LH.ldesps16ADM_1SecCalc[u8DataCNT]>>8 );   

	CAN_LOG_DATA[50] = (uint8_t) ( alat );							                //ch35
	CAN_LOG_DATA[51] = (uint8_t) ( alat>>8 );
	CAN_LOG_DATA[52] = (uint8_t) ( ADMvelo_RH.ldesps16ADM_1SecCalc[u8DataCNT] );		//ch36
	CAN_LOG_DATA[53] = (uint8_t) ( ADMvelo_RH.ldesps16ADM_1SecCalc[u8DataCNT]>>8 );
	
	//ay_vx_dot_sig_plus
	
	CAN_LOG_DATA[54] = (uint8_t) ( u16CycleTime );                                    //ldesps16AdmVchGENinSQRT );                         //ch37
	CAN_LOG_DATA[55] = (uint8_t) ( u16CycleTime>>8 );                                 //ldesps16AdmVchGENinSQRT>>8 ); 

	CAN_LOG_DATA[56] = (uint8_t) ( Beta_MD_Dot ); 	            		         	 //ch38
	CAN_LOG_DATA[57] = (uint8_t) ( Beta_MD_Dot>>8 );
	CAN_LOG_DATA[58] = (uint8_t) (ldesps16DetDelYawAccFilt);//( ADMvchGEN_RH.ldesps16ADM_100SecCalcMean );			 //ch39
	CAN_LOG_DATA[59] = (uint8_t) (ldesps16DetDelYawAccFilt>>8);//( ADMvchGEN_RH.ldesps16ADM_100SecCalcMean>>8 );
	CAN_LOG_DATA[60] = (uint8_t) ( ldesps16AdmVchApplyF );	                         //ch40
	CAN_LOG_DATA[61] = (uint8_t) ( ldesps16AdmVchApplyF>>8 );                 
	CAN_LOG_DATA[62] = (uint8_t) ( det_alatc_fltr );                                   //ch41
	CAN_LOG_DATA[63] = (uint8_t) ( det_alatc_fltr>>8 ); 
#if (__AHB_GEN3_SYSTEM == ENABLE)  || (__IDB_LOGIC == ENABLE)
   	if (lsespu1MpresByAHBgen3Invalid==0)
   	{
		CAN_LOG_DATA[64] = (uint8_t) ( lsesps16MpressByAHBgen3 );								             //ch42 
		CAN_LOG_DATA[65] = (uint8_t) ( lsesps16MpressByAHBgen3>>8 );
   	}
   	else if ( (fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0) )
   	{
		CAN_LOG_DATA[64] = (uint8_t) ( lsesps16EstBrkPressByBrkPedalF );								             //ch42 
		CAN_LOG_DATA[65] = (uint8_t) ( lsesps16EstBrkPressByBrkPedalF>>8 );
   	}
   	else
   	{
   		CAN_LOG_DATA[64] = (uint8_t)(0);
   		CAN_LOG_DATA[65] = (uint8_t)(0);
   	}
#else
	CAN_LOG_DATA[64] = (uint8_t) ( mpress );								             //ch42 
	CAN_LOG_DATA[65] = (uint8_t) ( mpress>>8 );
#endif

	tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

	if(ESP_ERROR_FLG)				             tempB5|=0x01;
	if(BLS_PIN)						             tempB5|=0x02;
	if(ABS_fz)						             tempB5|=0x04;
	if(EBD_RA)						             tempB5|=0x08;
	if(BTC_fz)						             tempB5|=0x10;
	if(ETCS_ON)						             tempB5|=0x20;
	if(YAW_CDC_WORK)				             tempB5|=0x40;
	if(ESP_TCS_ON)					             tempB5|=0x80;
                                                 
	if(FLAG_BETA_UNDER_DRIFT)		             tempB4|=0x01;
	if(FLAG_ACTIVE_BRAKING)			             tempB4|=0x02;
	if(ADMvelo_RH.ldespu1ADM_Update1stOK)		 tempB4|=0x04;
	if(FLAG_BANK_DETECTED)			             tempB4|=0x08;
#if (__AHB_GEN3_SYSTEM == ENABLE)
   	if (lsespu1MpresByAHBgen3Invalid==0)
   	{
		if(lsespu1MpresBrkOnByAHBgen3)	         tempB4|=0x10;
	}
	else if ( (fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0) )
	{
		if(lsespu1PedalBrakeOn)			         tempB4|=0x10;
	}
	else
		if(0)							         tempB4|=0x10;
	}
#else
	if(MPRESS_BRAKE_ON)				             tempB4|=0x10;
#endif
 	if(MOT_ON_FLG)				                 tempB4|=0x20;
	if(BACK_DIR)				                 tempB4|=0x40;
	if(ldespu1AdmVchGENCrtOK)		             tempB4|=0x80;
                                                 
	if(AV_VL_fl)					             tempB3|=0x01;
	if(HV_VL_fl)					             tempB3|=0x02;
	if(ldespu1AdmVchGENGoodCond)	             tempB3|=0x04;
	if(ADMvelo_LH.ldespu1ADM_100SecCalcOK)	     tempB3|=0x08;  //SL100
	if(ADMvelo_RH.ldespu1ADM_100SecCalcOK)	     tempB3|=0x10;
	if(ldespu1AdmVeloCrtApply)                   tempB3|=0x20;
	if(ADMvelo_LH.ldespu1ADM_10SecCalcOK)	     tempB3|=0x40;
	if(ADMvelo_RH.ldespu1ADM_10SecCalcOK)	     tempB3|=0x80;


	if(ldespu1AdmVch1stUpdateOK)				 tempB2|=0x01;
	if(ldespu1AdmVch2ndUpdateOK)				 tempB2|=0x02;
	if(ldespu1AdmVch3rdUpdateOK)	             tempB2|=0x04;
	if(ADMvelo_LH.ldespu1ADM_Update1stOK)        tempB2|=0x08;
	if(ADMvelo_RH.ldespu1ADM_Update2ndOK)	     tempB2|=0x10;
	if(ADMvelo_LH.ldespu1ADM_Update2ndOK)        tempB2|=0x20;
	if(ADMvelo_RH.ldespu1ADM_Update3rdOK)	     tempB2|=0x40;  //추가
	if(ADMvelo_LH.ldespu1ADM_Update3rdOK)	     tempB2|=0x80;  //추가

   

	if(ADMvchGEN_LH.ldespu1ADM_10SecUpdateFlg )	 tempB1|=0x01;  //AV_VL_rl
	if(ADMvchGEN_RH.ldespu1ADM_10SecUpdateFlg )  tempB1|=0x02;  //HV_VL_rl
	if(SAS_CHECK_OK)						     tempB1|=0x04;
	if(STEER_STABLE_OK)						     tempB1|=0x08;
	if(ADMvchGEN_LH.ldespu1ADM_10SecCalcOK)              tempB1|=0x10;  //KSTEER.FIRST_OFFSET_CAL_FLG
	if(ADMvchGEN_LH.ldespu1ADM_100SecCalcOK)		     tempB1|=0x20;   //KSTEER.OFFSET_60AVR_CAL_OK_FLG
#if (__AHB_GEN3_SYSTEM == ENABLE)  || (__IDB_LOGIC == ENABLE)
	if(lsespu1MpresByAHBgen3Invalid)   		     tempB1|=0x40;
#else
	if(0)										 tempB1|=0x40;
#endif
	if(ldespu1AdmVeloWheelSpin )	             tempB1|=0x80;  //KYAW.FIRST_OFFSET_CAL_FLG
                                                 
                                                 
	if(ldespu1AdmVchGENInhibit)			         tempB0|=0x01;
	if(ldespu1AdmVchGENCrtApply)                 tempB0|=0x02;  
	if(ldespu1AdmVeloRGoodCond)		             tempB0|=0x04; 
	if(ADMvchGEN_RH.ldespu1ADM_10SecCalcOK)			         tempB0|=0x08;  //ldespu1AdmVeloInhibit
	if(ldespu1AdmVeloSteerCondFlg)		         tempB0|=0x10;
	if(ADMvchGEN_RH.ldespu1ADM_100SecCalcOK)	         tempB0|=0x20; //KLAT.OFFSET_60AVR_CAL_OK_FLG
	if(ADMvelo_LH.ldespu1ADM_10SecUpdateFlg)     tempB0|=0x40;   //추가
	if(ADMvelo_RH.ldespu1ADM_10SecUpdateFlg)     tempB0|=0x80;   //추가

    


	CAN_LOG_DATA[66] = (uint8_t) ( tempB5 );                                             //ch43
	CAN_LOG_DATA[67] = (uint8_t) ( tempB4 );                                             //ch44
	CAN_LOG_DATA[68] = (uint8_t) ( tempB3 );                                             //ch45
	CAN_LOG_DATA[69] = (uint8_t) ( tempB2 );                                             //ch46
	CAN_LOG_DATA[70] = (uint8_t) ( tempB1 );                                             //ch47
	CAN_LOG_DATA[71] = (uint8_t) ( tempB0 );                                             //ch48

	CAN_LOG_DATA[72] = (uint8_t) ( det_beta_dot );                                       //ch49
	CAN_LOG_DATA[73] = (uint8_t) ( det_beta_dot>>8 );
	CAN_LOG_DATA[74] = (uint8_t) ( ldesps16AdmVchGEN_F  );                               //ch50
	CAN_LOG_DATA[75] = (uint8_t) ( ldesps16AdmVchGEN_F>>8 );                 

	CAN_LOG_DATA[76] = (uint8_t) ( lss16absAxOffsetComp1000g );      					   //ch51 
	CAN_LOG_DATA[77] = (uint8_t) ( lss16absAxOffsetComp1000g>>8 );  			
	
	
	CAN_LOG_DATA[78] = (uint8_t) ( ldesps16AdmVestUsingYawSteer );                       //ch52
	CAN_LOG_DATA[79] = (uint8_t) ( ldesps16AdmVestUsingYawSteer>>8 );    
	

}
#endif

void 	LSTCS_vCallTMPLogData(void)
{
	UCHAR i;
	UCHAR j;

	CAN_LOG_DATA[0]	 = (UCHAR)(system_loop_counter);					 			    /* ch1 */
	CAN_LOG_DATA[1]	 = (UCHAR)(system_loop_counter>>8);                
	                                                                   
	CAN_LOG_DATA[2]	 = (UCHAR)(FL.vrad_resol_change);				 				    /* ch2 */
	CAN_LOG_DATA[3]	 = (UCHAR)(FL.vrad_resol_change>>8);               
	CAN_LOG_DATA[4]	 = (UCHAR)(FR.vrad_resol_change);				 				    /* ch3 */
	CAN_LOG_DATA[5]	 = (UCHAR)(FR.vrad_resol_change>>8);               
	CAN_LOG_DATA[6]	 = (UCHAR)(RL.vrad_resol_change);				 				    /* ch4 */
	CAN_LOG_DATA[7]	 = (UCHAR)(RL.vrad_resol_change>>8);               
	CAN_LOG_DATA[8]	 = (UCHAR)(RR.vrad_resol_change);				 				    /* ch5 */
	CAN_LOG_DATA[9]	 = (UCHAR)(RR.vrad_resol_change>>8);               
	CAN_LOG_DATA[10] = (UCHAR)(vref_resol_change);				     			    /* ch6 */
	CAN_LOG_DATA[11] = (UCHAR)(vref_resol_change>>8);                  
	                                                                   
	CAN_LOG_DATA[12] = (UCHAR)(yaw_out);						                    /* ch7 */   
	CAN_LOG_DATA[13] = (UCHAR)(yaw_out>>8);                            
	CAN_LOG_DATA[14] = (UCHAR)(rf2);									         			    /* ch8 */    
	CAN_LOG_DATA[15] = (UCHAR)(rf2>>8);                                
	CAN_LOG_DATA[16] = (UCHAR)(wstr);								         				    /* ch9 */
	CAN_LOG_DATA[17] = (UCHAR)(wstr>>8);                               
	CAN_LOG_DATA[18] = (UCHAR)(mpress);											 				    /* ch10 */
	CAN_LOG_DATA[19] = (UCHAR)(mpress>>8);                             
	CAN_LOG_DATA[20] = (UCHAR)(mtp);												  			    /* ch11 */
	CAN_LOG_DATA[21] = (UCHAR)(gear_state);													    /* ch12 */
	                                                                   
	CAN_LOG_DATA[22] = (UCHAR)(alat);															      /* ch13 */					
	CAN_LOG_DATA[23] = (UCHAR)(alat>>8);													       
	CAN_LOG_DATA[24] = (UCHAR)(along);	    			      					 	    /* ch14 */
	CAN_LOG_DATA[25] = (UCHAR)(along>>8);													       
	                                                                   
	CAN_LOG_DATA[26] = (UCHAR)(ldesps16OutsideAirTempt);				        /* ch15 */ 
	CAN_LOG_DATA[27] = (UCHAR)(ldespu8OutsideAirTemptV);                /* ch16 */
	CAN_LOG_DATA[28] = (UCHAR)(ldespu8OutsideAirTemptM);    				    /* ch17 */ 
                                                                     
	CAN_LOG_DATA[29] = (UCHAR)(tc_error_flg);                           /* ch18 */
	CAN_LOG_DATA[30] = (UCHAR)(FL.TMP_ERROR);	                			    /* ch19 */ 
	CAN_LOG_DATA[31] = (UCHAR)(FR.TMP_ERROR);                           /* ch20 */
	CAN_LOG_DATA[32] = (UCHAR)(RL.TMP_ERROR);			                	    /* ch21 */ 
	CAN_LOG_DATA[33] = (UCHAR)(RR.TMP_ERROR);                           /* ch22 */
	
#if __PRESS_EST_ACTIVE
  CAN_LOG_DATA[34] = (UCHAR)(FL.s16_Estimated_Active_Press/10);       /* ch23 */
  CAN_LOG_DATA[35] = (UCHAR)(FL.s16_Estimated_Active_Press/10>>8); 
  CAN_LOG_DATA[36] = (UCHAR)(FR.s16_Estimated_Active_Press/10);       /* ch24 */
  CAN_LOG_DATA[37] = (UCHAR)(FR.s16_Estimated_Active_Press/10>>8);
	CAN_LOG_DATA[38] = (UCHAR)(RL.s16_Estimated_Active_Press/10);       /* ch25 */
	CAN_LOG_DATA[39] = (UCHAR)(RL.s16_Estimated_Active_Press/10>>8);    
	CAN_LOG_DATA[40] = (UCHAR)(RR.s16_Estimated_Active_Press/10);       /* ch26 */
	CAN_LOG_DATA[41] = (UCHAR)(RR.s16_Estimated_Active_Press/10>>8);  
#else
  CAN_LOG_DATA[34] = (UCHAR)(0);                                      /* ch23 */
  CAN_LOG_DATA[35] = (UCHAR)(0>>8);
  CAN_LOG_DATA[36] = (UCHAR)(0);                                      /* ch24 */
  CAN_LOG_DATA[37] = (UCHAR)(0>>8);
	CAN_LOG_DATA[38] = (UCHAR)(0);                                      /* ch25 */
	CAN_LOG_DATA[39] = (UCHAR)(0>>8);                       
	CAN_LOG_DATA[40] = (UCHAR)(0);                                      /* ch26 */
	CAN_LOG_DATA[41] = (UCHAR)(0>>8);                            
#endif 

#if (__BTEM == ENABLE)
	CAN_LOG_DATA[42] = (UCHAR)(FL_TEMPT.ldesps16TemptUprate);		        /* ch27 */ 
	CAN_LOG_DATA[43] = (UCHAR)(FR_TEMPT.ldesps16TemptUprate);			      /* ch28 */ 
	CAN_LOG_DATA[44] = (UCHAR)(RL_TEMPT.ldesps16TemptUprate);				    /* ch29 */ 
	CAN_LOG_DATA[45] = (UCHAR)(RR_TEMPT.ldesps16TemptUprate);				    /* ch30 */ 
#else
	CAN_LOG_DATA[42] = (UCHAR)(0);		                                  /* ch27 */ 
	CAN_LOG_DATA[43] = (UCHAR)(0);		                                  /* ch28 */ 
	CAN_LOG_DATA[44] = (UCHAR)(0);		                                  /* ch29 */ 
	CAN_LOG_DATA[45] = (UCHAR)(0);		                                  /* ch30 */ 
#endif 

#if (__BTEM == ENABLE)
	CAN_LOG_DATA[46] = (UCHAR)(FL_TEMPT.ldesps16TemptDownrate);         /* ch31 */  
	CAN_LOG_DATA[47] = (UCHAR)(FR_TEMPT.ldesps16TemptDownrate);         /* ch32 */
	CAN_LOG_DATA[48] = (UCHAR)(RL_TEMPT.ldesps16TemptDownrate);         /* ch33 */
	CAN_LOG_DATA[49] = (UCHAR)(RR_TEMPT.ldesps16TemptDownrate);         /* ch34 */
#else
	CAN_LOG_DATA[46] = (UCHAR)(cool_tmp_fl);	                          /* ch31 */
	CAN_LOG_DATA[47] = (UCHAR)(cool_tmp_fr);	                          /* ch32 */
	CAN_LOG_DATA[48] = (UCHAR)(cool_tmp_rl);	                          /* ch33 */
	CAN_LOG_DATA[49] = (UCHAR)(cool_tmp_rr);	                          /* ch34 */
#endif 

	CAN_LOG_DATA[50] = (UCHAR)(btc_tmp_fl);													    /* ch35 */
	CAN_LOG_DATA[51] = (UCHAR)(btc_tmp_fl>>8);
	CAN_LOG_DATA[52] = (UCHAR)(btc_tmp_fr);		    										  /* ch36 */
	CAN_LOG_DATA[53] = (UCHAR)(btc_tmp_fr>>8); 
	CAN_LOG_DATA[54] = (UCHAR)(btc_tmp_rl);			    									  /* ch37 */
	CAN_LOG_DATA[55] = (UCHAR)(btc_tmp_rl>>8); 
	CAN_LOG_DATA[56] = (UCHAR)(btc_tmp_rr);					    		            /* ch38 */
	CAN_LOG_DATA[57] = (UCHAR)(btc_tmp_rr>>8);

#if (__BTEM == DISABLE)
	CAN_LOG_DATA[58] = (UCHAR)(FL.btc_tmp);		                          /* ch39*/
	CAN_LOG_DATA[59] = (UCHAR)(FL.btc_tmp>>8);
	CAN_LOG_DATA[60] = (UCHAR)(FR.btc_tmp);		                   		    /* ch40 */
	CAN_LOG_DATA[61] = (UCHAR)(FR.btc_tmp>>8);
	CAN_LOG_DATA[62] = (UCHAR)(RL.btc_tmp);			                   	    /* ch41 */
	CAN_LOG_DATA[63] = (UCHAR)(RL.btc_tmp>>8);
	CAN_LOG_DATA[64] = (UCHAR)(RR.btc_tmp);				 	               	    /* ch42 */
	CAN_LOG_DATA[65] = (UCHAR)(RR.btc_tmp>>8);
#else
	CAN_LOG_DATA[58] = (UCHAR)(FL_TEMPT.ldesps16Tempt);		              /* ch39 */
	CAN_LOG_DATA[59] = (UCHAR)(FL_TEMPT.ldesps16Tempt>>8);
	CAN_LOG_DATA[60] = (UCHAR)(FR_TEMPT.ldesps16Tempt);		    		      /* ch40 */
	CAN_LOG_DATA[61] = (UCHAR)(FR_TEMPT.ldesps16Tempt>>8);
	CAN_LOG_DATA[62] = (UCHAR)(RL_TEMPT.ldesps16Tempt);		    	    	  /* ch41 */
	CAN_LOG_DATA[63] = (UCHAR)(RL_TEMPT.ldesps16Tempt>>8);
	CAN_LOG_DATA[64] = (UCHAR)(RR_TEMPT.ldesps16Tempt);				 			    /* ch42 */
	CAN_LOG_DATA[65] = (UCHAR)(RR_TEMPT.ldesps16Tempt>>8);
#endif

	CAN_LOG_DATA[66] = (UCHAR)(0);				                              /* ch43 */
	CAN_LOG_DATA[67] = (UCHAR)(0>>8);
	CAN_LOG_DATA[68] = (UCHAR)(0);				                     	        /* ch44 */
	CAN_LOG_DATA[69] = (UCHAR)(0>>8);
	CAN_LOG_DATA[70] = (UCHAR)(0);				                     	        /* ch45 */
	CAN_LOG_DATA[71] = (UCHAR)(0>>8);

	tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=tempB6=tempB7=0;

	if(ESP_ERROR_FLG)				       			tempB7|=0x01;
	if(BLS_PIN)						         			tempB7|=0x02;
	if(ABS_fz)						         			tempB7|=0x04;
	if(EBD_RA)						         			tempB7|=0x08;
	if(BTC_fz)						         			tempB7|=0x10;
	if(ETCS_ON)						         			tempB7|=0x20;
	if(YAW_CDC_WORK)				       			tempB7|=0x40;
	if(ESP_TCS_ON)					       			tempB7|=0x80;

	if(FLAG_BETA_UNDER_DRIFT)		   			tempB6|=0x01;
	if(FLAG_ACTIVE_BRAKING)			   			tempB6|=0x02;
	if(PARTIAL_BRAKE)				       			tempB6|=0x04;
	if(FLAG_BANK_DETECTED)			   			tempB6|=0x08;
	if(MPRESS_BRAKE_ON)				     			tempB6|=0x10;
 	if(MOT_ON_FLG)				         			tempB6|=0x20;
	if(BACK_DIR)				           			tempB6|=0x40;
	if(yaw_offset_srch_ok_flg)		 			tempB6|=0x80;

	if(ESP_ROUGH_ROAD)					   			tempB5|=0x01;
	if(SAS_CHECK_OK)					     			tempB5|=0x02;
	if(BLS)					 	              		tempB5|=0x04;
	if(BLS_K)		 		              			tempB5|=0x08;
	if(0)		                           	tempB5|=0x10;		
	if(0)		                            tempB5|=0x20;
	if(0)		                      		 	tempB5|=0x40;
	if(0)	                           	  tempB5|=0x80;    

	if(0)			                          tempB4|=0x01;
	if(0)					                      tempB4|=0x02;
	if(fu1BLSErrDet)	              	  tempB4|=0x04;  
	if(fu1MainCanLineErrDet)			 			tempB4|=0x08;		
	if(fu1EMSTimeOutErrDet)				   		tempB4|=0x10;
	if(fu1TCUTimeOutErrDet)	 						tempB4|=0x20;
	if(fu1MCPErrorDet)		  						tempB4|=0x40;
	if(fu1MCPSusDet)	 									tempB4|=0x80;

	if(0)					                			tempB3|=0x01;
	if(0)					     		          		tempB3|=0x02;
	if(fu1YawErrorDet)					 				tempB3|=0x04;
  if(fu1WheelFLErrDet)					 			tempB3|=0x08;
	if(fu1WheelFRErrDet)      					tempB3|=0x10;
	if(fu1WheelRLErrDet)	        			tempB3|=0x20;   
	if(fu1WheelRRErrDet)		 						tempB3|=0x40;   
	if(fu1ClutchSwitch)	        				tempB3|=0x80;

	if(vcc_off_flg)			                tempB2|=0x01;
	if(0)			                          tempB2|=0x02;
	if(0)		                            tempB2|=0x04;
	if(0)		                  	 				tempB2|=0x08;
	if(0)		                        	 	tempB2|=0x10;
	if(0)	                   			     	tempB2|=0x20;
	if(0)		                            tempB2|=0x40;	 
	if(0)	                     		 			tempB2|=0x80;
		
	if(0)		                        	  tempB1|=0x01;
	if(0)			                          tempB1|=0x02;
	if(0)		                   					tempB1|=0x04;
	if(0)			 													tempB1|=0x08;
	if(0)			  	          						tempB1|=0x10;
	if(0)			      										tempB1|=0x20;
	if(0)		          									tempB1|=0x40;	 
	if(0)			 		        							tempB1|=0x80;
		
	if(0)			  												tempB0|=0x01;
	if(0)			            							tempB0|=0x02;
	if(0)		      											tempB0|=0x04;
	if(0)			 													tempB0|=0x08;
	if(0)			 													tempB0|=0x10;
	if(0)				     										tempB0|=0x20;
	if(0)		      											tempB0|=0x40;	 
	if(0)			 													tempB0|=0x80;	 	 
 
	CAN_LOG_DATA[72] = tempB7; /* ch46 */
	CAN_LOG_DATA[73] = tempB6; /* ch47 */
	CAN_LOG_DATA[74] = tempB5; /* ch48 */
	CAN_LOG_DATA[75] = tempB4; /* ch49 */
	CAN_LOG_DATA[76] = tempB3; /* ch50 */
	CAN_LOG_DATA[77] = tempB2; /* ch51 */
	CAN_LOG_DATA[78] = tempB1; /* ch52 */
	CAN_LOG_DATA[79] = tempB0; /* ch53 */
}


/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallEECLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ESP Engine Control
********************************************************************************/
void LSESP_vCallEECLogData(void)
{
	uint8_t i;
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);

/* TOD Response Value */
#if defined(HMC_CAN) && (__CAR==KMC_HM)
    CAN_LOG_DATA[10] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR);   //4WD[6]
    CAN_LOG_DATA[11] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR>>8); //4WD[7]
#elif (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)
    tempUW0 = 0;
/*  tempUW0 =((((uint16_t)PPEI_TC_Coupling_Status.TransfrCaseCplValAct_1)&0x00ff)<<8)|(((uint16_t)PPEI_TC_Coupling_Status.TransfrCaseCplValAct_0)&0x00ff);  */
    CAN_LOG_DATA[10] =(uint8_t) ((uint16_t)tempUW0);  //4WD[6]
    CAN_LOG_DATA[11] =(uint8_t) ((uint16_t)tempUW0>>8); //4WD[7]
#else
    CAN_LOG_DATA[10] =(uint8_t) (0);
    CAN_LOG_DATA[11] =(uint8_t) (0>>8);
#endif

//    CAN_LOG_DATA[12] =(uint8_t) ((q_front<-120)?-120:(int8_t)q_front);
//    CAN_LOG_DATA[13] =(uint8_t) ((q_rear<-120)?-120:(int8_t)q_rear);
    CAN_LOG_DATA[14] =(uint8_t) (vref5);
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

    CAN_LOG_DATA[16] =(uint8_t) (u8CycleTime);
    CAN_LOG_DATA[17] =(uint8_t) (s8SetYawPDGainSelecter);
    CAN_LOG_DATA[18] =(uint8_t) (eec_stable_exit_count);

    CAN_LOG_DATA[19] =(uint8_t) (inhibition_number    =INHIBITION_INDICATOR());

    CAN_LOG_DATA[20] =(uint8_t) (EEC_YAW_flags);
    CAN_LOG_DATA[21] =(uint8_t) (EEC_flags);
    CAN_LOG_DATA[22] =(uint8_t) (d_yaw_div_delay);

/* TOD Request Flag & Value */
#if defined(HMC_CAN) && (__CAR==KMC_HM)
    CAN_LOG_DATA[23] = (uint8_t)(lespu1TCS_4WD_LIM_MODE);
    CAN_LOG_DATA[24] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM);
    CAN_LOG_DATA[25] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM>>8);
#elif (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)
    CAN_LOG_DATA[23] = (uint8_t)(lespu1_TCCRCplReqAct);   //4WD[6]
    CAN_LOG_DATA[24] =(uint8_t) ((uint16_t)lespu16_TCCRCplReqVal);    //4WD[6]
    CAN_LOG_DATA[25] =(uint8_t) ((uint16_t)lespu16_TCCRCplReqVal>>8); //4WD[7]
#else
    CAN_LOG_DATA[23] =(uint8_t) 0;
    CAN_LOG_DATA[24] =(uint8_t) ((uint16_t)0);
    CAN_LOG_DATA[25] =(uint8_t) ((uint16_t)0>>0);
#endif

    CAN_LOG_DATA[26] =(uint8_t) (esp_cmd);
    CAN_LOG_DATA[27] =(uint8_t) (esp_cmd>>8);

    CAN_LOG_DATA[28] =(uint8_t) (delta_yaw2);
    CAN_LOG_DATA[29] =(uint8_t) (delta_yaw2>>8);


    CAN_LOG_DATA[30] =(uint8_t) (drive_torq);
    CAN_LOG_DATA[31] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (cal_torq);
    CAN_LOG_DATA[33] =(uint8_t) (cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (eng_torq);
    CAN_LOG_DATA[35] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (eng_rpm);
    CAN_LOG_DATA[37] =(uint8_t) (eng_rpm>>8);

    CAN_LOG_DATA[38] =(uint8_t) (gear_pos);
    CAN_LOG_DATA[39] =(uint8_t) (mtp);

    CAN_LOG_DATA[40] =(uint8_t) (wstr);
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

    CAN_LOG_DATA[42] =(uint8_t) (yaw_out);
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[44] =(uint8_t) (rf2);
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
    CAN_LOG_DATA[46] =(uint8_t) (delta_yaw_third_ems);
    CAN_LOG_DATA[47] =(uint8_t) (delta_yaw_third_ems>>8);

    CAN_LOG_DATA[48] =(uint8_t) (o_delta_yaw_thres_eec);
    CAN_LOG_DATA[49] =(uint8_t) (o_delta_yaw_thres_eec>>8);
    CAN_LOG_DATA[50] =(uint8_t) (delta_yaw_eec);
    CAN_LOG_DATA[51] =(uint8_t) (delta_yaw_eec>>8);
    CAN_LOG_DATA[52] =(uint8_t) (s16YawErr);
    CAN_LOG_DATA[53] =(uint8_t) (s16YawErr>>8);
    CAN_LOG_DATA[54] =(uint8_t) (s16DiffYawErr);
    CAN_LOG_DATA[55] =(uint8_t) (s16DiffYawErr>>8);

    CAN_LOG_DATA[56] =(uint8_t) (tcs_cmd);
    CAN_LOG_DATA[57] =(uint8_t) (tcs_cmd>>8);
    CAN_LOG_DATA[58] =(uint8_t) (det_alatm);
    CAN_LOG_DATA[59] =(uint8_t) (det_alatm>>8);
    CAN_LOG_DATA[60] =(uint8_t) (s16SetYawPGain);
    CAN_LOG_DATA[61] =(uint8_t) (s16SetYawPGain>>8);
    CAN_LOG_DATA[62] =(uint8_t) (s16SetYawDGain);
    CAN_LOG_DATA[63] =(uint8_t) (s16SetYawDGain>>8);

    CAN_LOG_DATA[64] =(uint8_t) (mpress);
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

	i=0;
    if(ESP_ERROR_FLG==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(FTCS_ON==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fz==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(EBD_RA==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(BTC_fz==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
#if __TCS
    if(ETCS_ON==1)
    {
                                    i|=0x20;
    }
    else
    {
        ;
    }
#endif
    if(YAW_CDC_WORK==1)
    {
                                    i|=0x40;
    }
    else
    {
        ;
    }
#if __TCS
    if(ESP_TCS_ON==1)
    {
                                    i|=0x80;
    }
    else
    {
        ;
    }
#endif
	CAN_LOG_DATA[66] =(uint8_t) (i);

	i=0;

    if(FLAG_DELTAYAW_SIGNCHANGE==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(FLAG_ACTIVE_BRAKING==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ESP_SPLIT==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(VDC_DISABLE_FLG==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(MPRESS_BRAKE_ON==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
    if(MOT_ON_FLG==1)
    {
        i|=0x20;
    }
    else
    {
        ;
    }
    if(AUTO_TM==1)
    {
        if(BACK_DIR==1)
        {
            i|=0x40;
        }
        else
        {
            ;
        }
    }
    else
    {
#if __AX_SENSOR
        if(BACKWARD_MOVE==1)
        {
            i|=0x40;
        }
        else
        {
            ;
        }
#endif
    }
    if(ESP_TCS_ON_START==1)
    {
        i|=0x80;
    }
    else
    {
        ;
    }
    CAN_LOG_DATA[67] =(uint8_t) (i);

    i=0;

    if(AV_VL_fl==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_fl==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fl==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(BTCS_fl==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(TCL_DEMAND_fl==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_FL==1)
    {
        i|=0x20;
    }
    else
    {
        ;
    }
    if(GEAR_CH_TCS==1)
    {
        i|=0x40;
    }
    else
    {
        ;
    }
    if(D_YAW_ON==1)
    {
        i|=0x80;
    }
    else
    {
        ;
    }
    CAN_LOG_DATA[68] =(uint8_t) (i);

	i=0;
    if(AV_VL_fr==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_fr==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fr==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(BTCS_fr==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(TCL_DEMAND_fr==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_FR==1)
    {
        i|=0x20;
    }
    else
    {
        ;
    }
    if(YAW_CHANGE_LIMIT_ACT==1)
    {
        i|=0x40;
    }
    else
    {
        ;
    }
    if(Beta_Moment_Ctrl_Flg==1)
    {
        i|=0x80;
    }
    else
    {
        ;
    }
    CAN_LOG_DATA[69] =(uint8_t) (i);

    i=0;
    if(AV_VL_rl==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_rl==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_rl==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(BLS==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(EBD_rl==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_RL==1)
    {
        i|=0x20;
    }
    else
    {
        ;
    }
    if(S_VALVE_LEFT==1)
    {
        i|=0x40;
    }
    else
    {
        ;
    }
    if(SAS_CHECK_OK==1)
    {
        i|=0x80;
    }
    else
    {
        ;
    }
	CAN_LOG_DATA[70] =(uint8_t) (i);

	i=0;

    if(AV_VL_rr==1)
    {
        i|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_rr==1)
    {
        i|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_rr==1)
    {
        i|=0x04;
    }
    else
    {
        ;
    }
    if(SEVERE_CORNERG==1)
    {
        i|=0x08;
    }
    else
    {
        ;
    }
    if(EBD_rr==1)
    {
        i|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_RR==1)
    {
        i|=0x20;
    }
    else
    {
        ;
    }
    if(S_VALVE_RIGHT==1)
    {
        i|=0x40;
    }
    else
    {
        ;
    }
    if(D_YAW_DIV==1)
    {
        i|=0x80;
    }
    else
    {
        ;
    }
	CAN_LOG_DATA[71] =(uint8_t) (i);


    CAN_LOG_DATA[72] =(uint8_t) (curv_radius);
    CAN_LOG_DATA[73] =(uint8_t) (curv_radius>>8);

    CAN_LOG_DATA[74] =(uint8_t) ((uint16_t)delta_beta2);
    CAN_LOG_DATA[75] =(uint8_t) ((uint16_t)delta_beta2>>8);

    CAN_LOG_DATA[76] =(uint8_t) (FL.pwm_duty_temp);
    CAN_LOG_DATA[77] =(uint8_t) (FR.pwm_duty_temp);
    CAN_LOG_DATA[78] =(uint8_t) (RL.pwm_duty_temp);
    CAN_LOG_DATA[79] =(uint8_t) (RR.pwm_duty_temp);
}

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallTODLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ESP Engine Control
********************************************************************************/
void LSESP_vCallTODLogData(void)
{
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (vref5);
    CAN_LOG_DATA[11] =(uint8_t) (vref5>>8);

    CAN_LOG_DATA[12] =(uint8_t) (u8CycleTime);
    CAN_LOG_DATA[13] =(uint8_t) (inhibition_number    =INHIBITION_INDICATOR());

    CAN_LOG_DATA[14] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM);   //TCS4[1]
    CAN_LOG_DATA[15] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM>>8);    //TCS4[2]
    CAN_LOG_DATA[16] = (uint8_t)(lespu8TCS_4WD_OPEN);  //TCS4[0]
    CAN_LOG_DATA[17] = (uint8_t)(lespu8TCS_4WD_CLU_LIM);  //TCS4[3]
//
    CAN_LOG_DATA[18] = (uint8_t)(lespu8AWD_4WD_TYPE); //4WD[0]
    CAN_LOG_DATA[19] = (uint8_t)(lespu1AWD_4WD_ERR);      //4WD[1]
    CAN_LOG_DATA[20] = (uint8_t)(lespu8AWD_CLU_DUTY);     //4WD[2]
    CAN_LOG_DATA[21] = (uint8_t)(lespu8AWD_R_TIRE);           //4WD[3]
    CAN_LOG_DATA[22] = (uint8_t)(lespu1AWD_AUTO_ACT);                      //4WD[4]
    CAN_LOG_DATA[23] = (uint8_t)(lespu1AWD_4H_ACT);  //4WD[5]
    CAN_LOG_DATA[24] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR);   //4WD[6]
    CAN_LOG_DATA[25] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR>>8); //4WD[7]

    CAN_LOG_DATA[26] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM);
    CAN_LOG_DATA[27] =(uint8_t) ((uint16_t)lespu16TCS_4WD_TQC_LIM>>8);

    CAN_LOG_DATA[28] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR);
    CAN_LOG_DATA[29] =(uint8_t) ((uint16_t)lespu16AWD_4WD_TQC_CUR>>8);
/*
    tempUW0=(((uint16_t)TCS4[2]<<8)|(uint16_t)TCS4[1])
    CAN_LOG_DATA[14] = (uint16_t)tempUW0;                   //TCS4[1]
    CAN_LOG_DATA[15] = (uint16_t)tempUW0>>8);               //TCS4[2]
    CAN_LOG_DATA[16] = (uint8_t)TCS4[0];                  //TCS4[0]
    CAN_LOG_DATA[17] = (uint8_t)TCS4[3];                  //TCS4[3]

    CAN_LOG_DATA[18] = (uint8_t)4WD[0]                    //4WD[0]
    CAN_LOG_DATA[19] = (uint8_t)4WD[1];                   //4WD[1]
    CAN_LOG_DATA[20] = (uint8_t)4WD[2];                   //4WD[2]
    CAN_LOG_DATA[21] = (uint8_t)4WD[3];                   //4WD[3]
    CAN_LOG_DATA[22] = (uint8_t)4WD[4];                   //4WD[4]
    CAN_LOG_DATA[23] = (uint8_t)4WD[5];                   //4WD[5]

    tempUW0=(((uint16_t)4WD[7]<<8)|(uint16_t)4WD[6])
    CAN_LOG_DATA[24] = (uint16_t)tempUW0;                   //4WD[6]
    CAN_LOG_DATA[25] = (uint16_t)tempUW0>>8);               //4WD[7]

    tempUW0=(((uint16_t)TCS4[2]<<8)|(uint16_t)TCS4[1])
    CAN_LOG_DATA[26] = (uint16_t)tempUW0;                   //TCS4[1]
    CAN_LOG_DATA[27] = (uint16_t)tempUW0>>8);               //TCS4[2]

    tempUW0=(((uint16_t)4WD[7]<<8)|(uint16_t)4WD[6])
    CAN_LOG_DATA[28] = (uint16_t)tempUW0;                   //4WD[6]
    CAN_LOG_DATA[29] = (uint16_t)tempUW0>>8);               //4WD[7]
*/

    CAN_LOG_DATA[30] =(uint8_t) (drive_torq);
    CAN_LOG_DATA[31] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (cal_torq);
    CAN_LOG_DATA[33] =(uint8_t) (cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (eng_torq);
    CAN_LOG_DATA[35] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (eng_rpm);
    CAN_LOG_DATA[37] =(uint8_t) (eng_rpm>>8);

    CAN_LOG_DATA[38] =(uint8_t) (gear_pos);
    CAN_LOG_DATA[39] =(uint8_t) (mtp);

    CAN_LOG_DATA[40] =(uint8_t) (wstr);
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);
    CAN_LOG_DATA[42] =(uint8_t) (yaw_out);
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[44] =(uint8_t) (rf2);
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);

    CAN_LOG_DATA[46] =(uint8_t) (u_delta_yaw_thres);
    CAN_LOG_DATA[47] =(uint8_t) (u_delta_yaw_thres>>8);
    CAN_LOG_DATA[48] =(uint8_t) (o_delta_yaw_thres);
    CAN_LOG_DATA[49] =(uint8_t) (o_delta_yaw_thres>>8);
    CAN_LOG_DATA[50] =(uint8_t) (delta_yaw2);
    CAN_LOG_DATA[51] =(uint8_t) (delta_yaw2>>8);

    CAN_LOG_DATA[52] =(uint8_t) (det_alatm);
    CAN_LOG_DATA[53] =(uint8_t) (det_alatm>>8);
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc);
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc>>8);

    CAN_LOG_DATA[56] =(uint8_t) (alat);
    CAN_LOG_DATA[57] =(uint8_t) (alat>>8);
    CAN_LOG_DATA[58] =(uint8_t) (esp_mu);
    CAN_LOG_DATA[59] =(uint8_t) (esp_mu>>8);

    CAN_LOG_DATA[60] =(uint8_t) (delta_yaw);
    CAN_LOG_DATA[61] =(uint8_t) (delta_yaw>>8);
    CAN_LOG_DATA[62] =(uint8_t) (delta_yaw_first);
    CAN_LOG_DATA[63] =(uint8_t) (delta_yaw_first>>8);

    CAN_LOG_DATA[64] =(uint8_t) (mpress);
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)
    {
        tempB5|=0x01;
    }
    else
    {
        ;
    }
    if(BLS_PIN)
    {
        tempB5|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fz)
    {
        tempB5|=0x04;
    }
    else
    {
        ;
    }
    if(EBD_RA)
    {
        tempB5|=0x08;
    }
    else
    {
        ;
    }
    if(BTC_fz)
    {
        tempB5|=0x10;
    }
    else
    {
        ;
    }
#if __TCS
    if(ETCS_ON)
    {
                                    tempB5|=0x20;
    }
    else
    {
        ;
    }
#endif
    if(YAW_CDC_WORK)
    {
                                    tempB5|=0x40;
    }
    else
    {
        ;
    }
#if __TCS
    if(ESP_TCS_ON)
    {
                                    tempB5|=0x80;
    }
    else
    {
        ;
    }
#endif

    if(FLAG_BETA_UNDER_DRIFT)
    {
        tempB4|=0x01;
    }
    else
    {
        ;
    }
    if(FLAG_ACTIVE_BRAKING)
    {
        tempB4|=0x02;
    }
    else
    {
        ;
    }
    if(ESP_SPLIT)
    {
        tempB4|=0x04;
    }
    else
    {
        ;
    }
    if(VDC_DISABLE_FLG)
    {
        tempB4|=0x08;
    }
    else
    {
        ;
    }
    if(MPRESS_BRAKE_ON)
    {
        tempB4|=0x10;
    }
    else
    {
        ;
    }
    if(MOT_ON_FLG)
    {
        tempB4|=0x20;
    }
    else
    {
        ;
    }
    if(AUTO_TM)
    {
        if(BACK_DIR)
        {
            tempB4|=0x40;
        }
        else
        {
            ;
        }
    }
    else
    {
#if __AX_SENSOR
        if(BACKWARD_MOVE)
        {
            tempB4|=0x40;
        }
        else
        {
            ;
        }
#endif
    }
    if(SEVERE_CORNERG)
    {
        tempB4|=0x80;
    }
    else
    {
        ;
    }

    if(AV_VL_fl)
    {
        tempB3|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_fl)
    {
        tempB3|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fl)
    {
        tempB3|=0x04;
    }
    else
    {
        ;
    }
    if(BTCS_fl)
    {
        tempB3|=0x08;
    }
    else
    {
        ;
    }
    if(TCL_DEMAND_fl)
    {
        tempB3|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_FL)
    {
        tempB3|=0x20;
    }
    else
    {
        ;
    }
    if(D_AY_ON)
    {
        tempB3|=0x40;
    }
    else
    {
        ;
    }
    if(D_YAW_ON)
    {
        tempB3|=0x80;
    }
    else
    {
        ;
    }

    if(AV_VL_fr)
    {
        tempB2|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_fr)
    {
        tempB2|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_fr)
    {
        tempB2|=0x04;
    }
    else
    {
        ;
    }
    if(BTCS_fr)
    {
        tempB2|=0x08;
    }
    else
    {
        ;
    }
    if(TCL_DEMAND_fr)
    {
        tempB2|=0x10;
    }
    else
    {

        ;
    }
    if(SLIP_CONTROL_NEED_FR)
    {
        tempB2|=0x20;
    }
    else
    {
        ;
    }
    if(lespu1TCS_4WD_LIM_REQ)
    {
        tempB2|=0x40;
    }
    else
    {
        ;
    }
    if(lespu1TCS_4WD_LIM_MODE)
    {
        tempB2|=0x80;
    }
    else
    {
        ;
    }
    if(AV_VL_rl)
    {
        tempB1|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_rl)
    {
        tempB1|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_rl)
    {
        tempB1|=0x04;
    }
    else
    {
        ;
    }
    if(lespu1AWD_AUTO_ACT)
    {
        tempB1|=0x08;
    }
    else
    {
        ;
    }
    if(EBD_rl)
    {
        tempB1|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_RL)
    {
        tempB1|=0x20;
    }
    else
    {
        ;
    }
    if(S_VALVE_LEFT)
    {
        tempB1|=0x40;
    }
    else
    {
        ;
    }
    if(lespu1AWD_LOCK_ACT)
    {
        tempB1|=0x80;
    }
    else
    {
        ;
    }

    if(AV_VL_rr)
    {
        tempB0|=0x01;
    }
    else
    {
        ;
    }
    if(HV_VL_rr)
    {
        tempB0|=0x02;
    }
    else
    {
        ;
    }
    if(ABS_rr)
    {
        tempB0|=0x04;
    }
    else
    {
        ;
    }
    if(lespu1AWD_LOW_ACT)
    {
        tempB0|=0x08;
    }
    else
    {
        ;
    }
    if(EBD_rr)
    {
        tempB0|=0x10;
    }
    else
    {
        ;
    }
    if(SLIP_CONTROL_NEED_RR)
    {
        tempB0|=0x20;
    }
    else
    {
        ;
    }
    if(S_VALVE_RIGHT)
    {
        tempB0|=0x40;
    }
    else
    {
        ;
    }
    if(lespu1AWD_2H_ACT)
    {
        tempB0|=0x80;
    }
    else
    {
        ;
    }

    CAN_LOG_DATA[66] =(uint8_t) (tempB5);
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);

    CAN_LOG_DATA[72] =(uint8_t) (wvref_fl);
    CAN_LOG_DATA[73] =(uint8_t) (wvref_fl>>8);
    CAN_LOG_DATA[74] =(uint8_t) (wvref_fr);
    CAN_LOG_DATA[75] =(uint8_t) (wvref_fr>>8);

    CAN_LOG_DATA[76] = (uint8_t)(FL.pwm_duty_temp);
    CAN_LOG_DATA[77] = (uint8_t)(FR.pwm_duty_temp);
    CAN_LOG_DATA[78] = (uint8_t)(RL.pwm_duty_temp);
    CAN_LOG_DATA[79] = (uint8_t)(RR.pwm_duty_temp);
}

void LSHDCHSA_vCall_GSUV_LogData(void)  // GSUV_OEM ESC_view : No.19
{
	uint8_t i;
	#if __HDC || __HSA
    CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (uint8_t)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (uint8_t)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (uint8_t)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (uint8_t)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (uint8_t)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (uint8_t)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (uint8_t)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_rr>>8);
    CAN_LOG_DATA[10] = (uint8_t)(vref);                               /* 06 */
    CAN_LOG_DATA[11] = (uint8_t)(vref>>8);
    CAN_LOG_DATA[12] = (uint8_t)((INT)target_vol/1000);               /* 07 */
    CAN_LOG_DATA[13] = (uint8_t)((INT)fu16CalVoltMOTOR/1000);         /* 08 */
  #endif /* __HDC || __HSA */
  #if __HDC
    CAN_LOG_DATA[14] = (uint8_t)(lcs16HdcTargetSpeed);                /* 09 */
    CAN_LOG_DATA[15] = (uint8_t)(lcs16HdcTargetSpeed>>8);
    CAN_LOG_DATA[16] = (uint8_t)(lcu8HdcSmaThetaGCnt);                /* 10 */
    CAN_LOG_DATA[17] = (uint8_t)(lcu8HdcSmaThetaGEstTime);            /* 11 */
    CAN_LOG_DATA[18] = (uint8_t)(lcu8HdcFlatGCnt);                    /* 12 */
    CAN_LOG_DATA[19] = (uint8_t)(lcs16HdcThetaG_F_cnt);               /* 13 */
    CAN_LOG_DATA[20] = (uint8_t)(lcs16HdcMaxDiscTemp/320);            /* 14 */
    CAN_LOG_DATA[21] = (uint8_t)(lcs16HdcThetaG_B_cnt);               /* 15 */
    CAN_LOG_DATA[22] = (uint8_t)(lcu8HdcFlatPreCnt);                  /* 16 */
    CAN_LOG_DATA[23] = (uint8_t)(lcu8HdcFlatGCnt);                    /* 17 */
  #endif /* __HDC */
  #if __HDC || __HSA
    CAN_LOG_DATA[24] = (uint8_t)(TC_MFC_Voltage_Primary_dash);        /* 18 */
    CAN_LOG_DATA[25] = (uint8_t)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[26] = (uint8_t)(TC_MFC_Voltage_Secondary_dash);      /* 19 */
    CAN_LOG_DATA[27] = (uint8_t)(TC_MFC_Voltage_Secondary_dash>>8);
  #endif /* __HDC || __HSA */
  #if __HDC
    CAN_LOG_DATA[28] = (uint8_t)(lcs16HdcThetaG);                     /* 20 */
    CAN_LOG_DATA[29] = (uint8_t)(lcs16HdcThetaG>>8);
    CAN_LOG_DATA[30] = (uint8_t)(lcs16DecCtrlInput);                  /* 21 */
    CAN_LOG_DATA[31] = (uint8_t)(lcs16DecCtrlInput>>8);
    CAN_LOG_DATA[32] = (uint8_t)(lcs16HdcCtrlInput);                  /* 22 */
    CAN_LOG_DATA[33] = (uint8_t)(lcs16HdcCtrlInput>>8);
    CAN_LOG_DATA[34] = (uint8_t)(lcs16HdcSmaThetaGTh);                /* 23 */
    CAN_LOG_DATA[35] = (uint8_t)(lcs16HdcSmaThetaGTh>>8);
   #endif /* __HDC */
   #if __HDC || __HSA
    CAN_LOG_DATA[36] = (uint8_t)(mpress);                             /* 24 */
    CAN_LOG_DATA[37] = (uint8_t)(mpress>>8);
    CAN_LOG_DATA[38] = (uint8_t)(gs_sel);                             /* 25 */
    CAN_LOG_DATA[39] = (uint8_t)(mtp);                                /* 26 */
    CAN_LOG_DATA[40] = (uint8_t)(wstr);                               /* 27 */
    CAN_LOG_DATA[41] = (uint8_t)(wstr>>8);
    CAN_LOG_DATA[42] = (uint8_t)(yaw_out);                            /* 28 */
    CAN_LOG_DATA[43] = (uint8_t)(yaw_out>>8);
   #endif /* __HDC || __HSA */
   #if __HDC
    CAN_LOG_DATA[44] = (uint8_t)(lcs16DecTargetG);                    /* 29 */
    CAN_LOG_DATA[45] = (uint8_t)(lcs16DecTargetG>>8);
    CAN_LOG_DATA[46] = (uint8_t)(lcs16HdcDecel);                      /* 30 */
    CAN_LOG_DATA[47] = (uint8_t)(lcs16HdcDecel>>8);
    CAN_LOG_DATA[48] = (uint8_t)(lcs16HdcASen);                       /* 31 */
    CAN_LOG_DATA[49] = (uint8_t)(lcs16HdcASen>>8);
    CAN_LOG_DATA[50] = (uint8_t)(lcs16HdcExThetaGTh);                 /* 32 */
    CAN_LOG_DATA[51] = (uint8_t)(lcs16HdcExThetaGTh>>8);
    CAN_LOG_DATA[52] = (uint8_t)(lcu8HdcBumpGCnt);                    /* 33 */
    CAN_LOG_DATA[53] = (uint8_t)(lcu8HdcBumpGCnt>>8);
  #endif /* __HDC */
  #if __HDC || __HSA
    CAN_LOG_DATA[54] = (uint8_t)(eng_torq_rel);                       /* 34 */
    CAN_LOG_DATA[55] = (uint8_t)(eng_torq_rel>>8);
    CAN_LOG_DATA[56] = (uint8_t)(eng_rpm);                            /* 35 */
    CAN_LOG_DATA[57] = (uint8_t)(eng_rpm>>8);
  #endif /* __HDC || __HSA */
  #if __HSA
    CAN_LOG_DATA[58] = (uint8_t)(MT_FRICTION_CONST);                                  /* 36 */
    CAN_LOG_DATA[59] = (uint8_t)(MT_FRICTION_CONST>>8);
    CAN_LOG_DATA[60] = (uint8_t)(HSA_ENTER_press);                                  /* 37 */
    CAN_LOG_DATA[61] = (uint8_t)(HSA_ENTER_press>>8);
    CAN_LOG_DATA[62] = (uint8_t)(MT_ENG_TORQ_INDEX);               /* 38 */
    CAN_LOG_DATA[63] = (uint8_t)(MT_ENG_TORQ_INDEX>>8);
    CAN_LOG_DATA[64] = (uint8_t)(HSA_ax_sen_filt);                    /* 39 */
    CAN_LOG_DATA[65] = (uint8_t)(HSA_ax_sen_filt>>8);
    CAN_LOG_DATA[66] = (uint8_t)(HSA_phase_out_rate);                 /* 40 */
    CAN_LOG_DATA[67] = (uint8_t)(HSA_phase_out_rate>>8); 
    CAN_LOG_DATA[68] = (uint8_t)(HSA_retaining_press/10);             /* 41 */
    CAN_LOG_DATA[69] = (uint8_t)(HSA_accel_cnt);                      /* 42 */
    CAN_LOG_DATA[70] = (uint8_t)(HSA_accel_cnt>>8);                      
    CAN_LOG_DATA[71] = (uint8_t)(HSA_EXIT_eng_torq/10);               /* 43 */
  #endif
  #if __HDC
    CAN_LOG_DATA[72] = (uint8_t)(MFC_PWM_DUTY_P);		    /* 44 */
    CAN_LOG_DATA[73] = (uint8_t)(lcu8HdcDriverBrakeIntend);           /* 45 */    
  #endif
    /***********************************************************************/
    i=0;
    /***********************************************************************/
  #if __HDC
    if(lcu1HdcSwitchFlg==1)                 i|=0x01;    /* 01 */
    if(lcu1HdcActiveFlg==1)                 i|=0x02;    /* 02 */
    if(lcu1HdcDownhillFlg==1)               i|=0x04;    /* 03 */
    if(lcu1hdcGearR_Switch==1)              i|=0x08;    /* 04 */
    if(lcu1hdcClutchSwitch==1)              i|=0x10;    /* 05 */  
    if(lcu1HdcInhibitFlg==1)                i|=0x20;    /* 06 */
    if(lcu1DecCtrlReqFlg==1)                i|=0x40;    /* 07 */
    if(AUTO_TM==1)                          i|=0x80;    /* 08 */
  #endif  /* __HDC */
    CAN_LOG_DATA[74] = (uint8_t)(i);                                  /* 46 */
    i=0;

    /***********************************************************************/
  #if __HDC
   	if(lcu1HdcDiscTempErrFlg==1)            i|=0x01;    /* 09 */
  #endif  /* __HDC */                                   
  #if __HDC || __HSA  		                              
    if(SAS_CHECK_OK==1)                     i|=0x02;    /* 10 */
    if(ABS_fz==1)                           i|=0x04;    /* 11 */
    if(EBD_RA==1)                           i|=0x08;    /* 12 */
    if(YAW_CDC_WORK==1)                     i|=0x10;    /* 13 */
  #endif
  #if __HDC
    if(lcu1hdcBackRollHillFlg==1)                       i|=0x20;    /* 14 */    	
    if(lcu1hdcEngStallSuspect2==1)          i|=0x40;    /* 15 */
	if(lcu1hdcEngStallSuspect==1)                 i|=0x80;    /* 16 */
  #endif
    CAN_LOG_DATA[75] = (uint8_t)(i);                                 /* 47 */
    i=0;
    /***********************************************************************/
  #if __HDC
    if(lcu1hdcVehicleForward==1)            i|=0x01;    /* 17 */
	  if(lcu1hdcVehicleBackward==1)           i|=0x02;    /* 18 */
    if(lcu1hdcVehicleForwardSensor==1)      i|=0x04;    /* 19 */
    if(lcu1hdcVehicleBackwardSensor==1)     i|=0x08;    /* 20 */
    if(lcu1hdcVehicleForwardThetaG==1)      i|=0x10;    /* 21 */   	
    if(lcu1hdcVehicleBackwardThetaG==1)     i|=0x20;    /* 22 */    	
    if(lcu1HdcMtTsCompByRPM==1)         i|=0x40;    /* 23 */
    if(lcu1HdcMtTsCompByInGear==1)        i|=0x80;    /* 24 */
  #endif  /* __HDC */
    CAN_LOG_DATA[76] = (uint8_t)(i);                                /* 48 */
    i=0;
    /***********************************************************************/
  #if __HDC
    if(Rough_road_detect_vehicle==1)        i|=0x01;    /* 25 */
    if(Rough_road_suspect_vehicle==1)       i|=0x02;    /* 26 */
    if(lcu1hdcGFlatFlag==1)                 i|=0x04;    /* 27 */
    if(lcu1hdcVehicleDirectionOKFlg==1)     i|=0x08;    /* 28 */
    if(lcu1HdcQuickDownhillFlg==1)          i|=0x10;    /* 29 */
    if(lcu1hdcFlatByPress==1)               i|=0x20;    /* 30 */
  #endif  /* __HDC */
  #if __HSA    
    if(HSA_ENOUGH_ENG_TORQ==1)              i|=0x40;    /* 31 */
    if(HSA_EST_CLUTCH_POSITION==1)               i|=0x80;    /* 32 */
  #endif  /* __HSA */  
    CAN_LOG_DATA[77] = (uint8_t)(i);                                /* 49 */
    i=0;
    /***********************************************************************/
  #if __HSA    
    if(HSA_TM_TYPE==1)                      i|=0x01;    /* 33 */
    if(fu1GearR_Switch==1)                  i|=0x02;    /* 34 */
    if(fu1ClutchSwitch==1)                  i|=0x04;    /* 35 */
    if(HSA_RAPID_PHASE_OUT==1)              i|=0x08;    /* 36 */
    if(HSA_RAPID_REBRAKING_flag==1)         i|=0x10;    /* 37 */
    if(STOP_ON_HILL_flag==1)                i|=0x20;    /* 38 */
    if(HSA_INHIBITION_flag==1)              i|=0x40;    /* 39 */
    if(HSA_flg==1)                          i|=0x80;    /* 40 */
   #endif  /* __HSA */
    CAN_LOG_DATA[78] = (uint8_t)(i);                                /* 50 */
    i=0;
    /***********************************************************************/
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)               i|=0x01;    /* 41 */
    if(TCL_DEMAND_SECONDARY==1)             i|=0x02;    /* 42 */
    if(S_VALVE_PRIMARY==1)                  i|=0x04;    /* 43 */
    if(S_VALVE_SECONDARY==1)                i|=0x08;    /* 44 */
      #else	                                            
    if(TCL_DEMAND_fr==1)                    i|=0x01;    /* 41 */
    if(TCL_DEMAND_fl==1)                    i|=0x02;    /* 42 */
    if(S_VALVE_RIGHT==1)                    i|=0x04;    /* 43 */
    if(S_VALVE_LEFT==1)                     i|=0x08;    /* 44 */
    	#endif                                            
    if(ABS_fl==1)                           i|=0x10;    /* 45 */
    if(ABS_fr==1) 	                        i|=0x20;    /* 46 */
    if(ABS_rl==1)                           i|=0x40;    /* 47 */
    if(ABS_rr==1)                           i|=0x80;    /* 48 */	
     CAN_LOG_DATA[79] = (uint8_t)(i);                              /* 51 */
    /***********************************************************************/
}

#if __HSA
void LSHSA_vCall_LogData(void) 
{
	UCHAR i;
	#if __HSA
    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (UCHAR)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (UCHAR)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (UCHAR)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (UCHAR)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (UCHAR)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (UCHAR)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (UCHAR)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (UCHAR)(vrad_rr>>8);
    CAN_LOG_DATA[10] = (UCHAR)(vref);                               /* 06 */
    CAN_LOG_DATA[11] = (UCHAR)(vref>>8);
    CAN_LOG_DATA[12] = (UCHAR)((INT)target_vol/1000);               /* 07 */
    CAN_LOG_DATA[13] = (UCHAR)((INT)fu16CalVoltMOTOR/1000);         /* 08 */
    CAN_LOG_DATA[14] = (UCHAR)(lcs16HsaInitHoldCntrCnt);                /* 09 */
    CAN_LOG_DATA[15] = (UCHAR)(lcs16HsaInitHoldCntrCnt>>8);
    CAN_LOG_DATA[16] = (UCHAR)(0);                /* 10 */
    CAN_LOG_DATA[17] = (UCHAR)(HSA_Control);            /* 11 */
    CAN_LOG_DATA[18] = (UCHAR)(lcu8HsaReBrkCnt);                    /* 12 */
    CAN_LOG_DATA[19] = (UCHAR)(HSA_apply_count);               /* 13 */
    CAN_LOG_DATA[20] = (UCHAR)(lcHsau8OverloadDctCnt);            /* 14 */
    CAN_LOG_DATA[21] = (UCHAR)(0);               /* 15 */
    CAN_LOG_DATA[22] = (UCHAR)(MFC_PWM_DUTY_P);                  /* 16 */
    CAN_LOG_DATA[23] = (UCHAR)(MFC_PWM_DUTY_S);                    /* 17 */
    CAN_LOG_DATA[24] = (UCHAR)(TC_MFC_Voltage_Primary_dash);        /* 18 */
    CAN_LOG_DATA[25] = (UCHAR)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[26] = (UCHAR)(TC_MFC_Voltage_Secondary_dash);      /* 19 */
    CAN_LOG_DATA[27] = (UCHAR)(TC_MFC_Voltage_Secondary_dash>>8);
    CAN_LOG_DATA[28] = (UCHAR)(HSA_hold_counter);                     /* 20 */
    CAN_LOG_DATA[29] = (UCHAR)(HSA_hold_counter>>8);
    CAN_LOG_DATA[30] = (UCHAR)(lcs16HsaValveActMp);                  /* 21 */
    CAN_LOG_DATA[31] = (UCHAR)(lcs16HsaValveActMp>>8);
    CAN_LOG_DATA[32] = (UCHAR)(lcs16HsaMpressSlopThres);                  /* 22 */
    CAN_LOG_DATA[33] = (UCHAR)(lcs16HsaMpressSlopThres>>8);
    CAN_LOG_DATA[34] = (UCHAR)(lcs16HsaReBrkReleaseCnt);                /* 23 */
    CAN_LOG_DATA[35] = (UCHAR)(lcs16HsaReBrkReleaseCnt>>8);
  #if __BRK_SIG_MPS_TO_PEDAL_SEN
    CAN_LOG_DATA[36] = (UCHAR)(lcs16HsaMpress);                             /* 24 */
    CAN_LOG_DATA[37] = (UCHAR)(lcs16HsaMpress>>8);
  #else  
    CAN_LOG_DATA[36] = (UCHAR)(mpress);                             /* 24 */
    CAN_LOG_DATA[37] = (UCHAR)(mpress>>8);
  #endif  
    CAN_LOG_DATA[38] = (UCHAR)(gs_sel);                             /* 25 */
    CAN_LOG_DATA[39] = (UCHAR)(mtp);                                /* 26 */
    CAN_LOG_DATA[40] = (UCHAR)(wstr);                               /* 27 */
    CAN_LOG_DATA[41] = (UCHAR)(wstr>>8);
    CAN_LOG_DATA[42] = (UCHAR)(yaw_out);                            /* 28 */
    CAN_LOG_DATA[43] = (UCHAR)(yaw_out>>8);
    #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[44] = (UCHAR)(pos_fr_1_100bar);                    /* 29 */
    CAN_LOG_DATA[45] = (UCHAR)(pos_fr_1_100bar>>8);
    CAN_LOG_DATA[46] = (UCHAR)(pos_fl_1_100bar);                      /* 30 */
    CAN_LOG_DATA[47] = (UCHAR)(pos_fl_1_100bar>>8);
    #elif __BRK_SIG_MPS_TO_PEDAL_SEN
    //CAN_LOG_DATA[44] = (UCHAR)(lsahbPrimarySen.lss16BCpresFilt);                    /* 29 */
    //CAN_LOG_DATA[45] = (UCHAR)(lsahbPrimarySen.lss16BCpresFilt>>8);
    //CAN_LOG_DATA[46] = (UCHAR)(lsahbSecondarySen.lss16BCpresFilt);                      /* 30 */
    //CAN_LOG_DATA[47] = (UCHAR)(lsahbSecondarySen.lss16BCpresFilt>>8);
    #else
    CAN_LOG_DATA[44] = (UCHAR)(0);                    /* 29 */
    CAN_LOG_DATA[45] = (UCHAR)(0>>8);
    CAN_LOG_DATA[46] = (UCHAR)(0);                      /* 30 */
    CAN_LOG_DATA[47] = (UCHAR)(0>>8);
    #endif
    #if (__AX_SENSOR)    
    CAN_LOG_DATA[48] = (UCHAR)(lss16absAxOffsetComp1000g);                       /* 31 */
    CAN_LOG_DATA[49] = (UCHAR)(lss16absAxOffsetComp1000g>>8);
    #endif
    CAN_LOG_DATA[50] = (UCHAR)(HSA_EXIT_eng_torq);                 /* 32 */
    CAN_LOG_DATA[51] = (UCHAR)(HSA_EXIT_eng_torq>>8);
    CAN_LOG_DATA[52] = (UCHAR)(HSA_retaining_press);                    /* 33 */
    CAN_LOG_DATA[53] = (UCHAR)(HSA_retaining_press>>8);
    CAN_LOG_DATA[54] = (UCHAR)(eng_torq_rel);                       /* 34 */
    CAN_LOG_DATA[55] = (UCHAR)(eng_torq_rel>>8);
    CAN_LOG_DATA[56] = (UCHAR)(eng_rpm);                            /* 35 */
    CAN_LOG_DATA[57] = (UCHAR)(eng_rpm>>8);
    CAN_LOG_DATA[58] = (UCHAR)(MT_FRICTION_CONST);                                  /* 36 */
    CAN_LOG_DATA[59] = (UCHAR)(MT_FRICTION_CONST>>8);
    CAN_LOG_DATA[60] = (UCHAR)(HSA_ENTER_press);                                  /* 37 */
    CAN_LOG_DATA[61] = (UCHAR)(HSA_ENTER_press>>8);
    CAN_LOG_DATA[62] = (UCHAR)(MT_ENG_TORQ_INDEX);               /* 38 */
    CAN_LOG_DATA[63] = (UCHAR)(MT_ENG_TORQ_INDEX>>8);
    CAN_LOG_DATA[64] = (UCHAR)(HSA_ax_sen_filt);                    /* 39 */
    CAN_LOG_DATA[65] = (UCHAR)(HSA_ax_sen_filt>>8);
    CAN_LOG_DATA[66] = (UCHAR)(HSA_phase_out_rate);                 /* 40 */
    CAN_LOG_DATA[67] = (UCHAR)(HSA_phase_out_rate>>8); 
    CAN_LOG_DATA[68] = (UCHAR)(0);             /* 41 */
    CAN_LOG_DATA[69] = (UCHAR)(HSA_accel_cnt);                      /* 42 */
    CAN_LOG_DATA[70] = (UCHAR)(HSA_accel_cnt>>8);                      
    CAN_LOG_DATA[71] = (UCHAR)(0);               /* 43 */
    CAN_LOG_DATA[72] = (UCHAR)(0);		    /* 44 */
    CAN_LOG_DATA[73] = (UCHAR)(0);           /* 45 */    
  #endif
    /***********************************************************************/
    i=0;
    /***********************************************************************/
  #if __HSA
    if(lcu1HsaReBrkRelease==1)                 i|=0x01;    /* 01 */
    if(lcu1HsaReBrk==1)                 i|=0x02;    /* 02 */
    if(lcu1HsaOverloadRoll==1)               i|=0x04;    /* 03 */
    if(lcu1HsaRiseComplete==1)              i|=0x08;    /* 04 */
    if(lcu1HsaMpressOn==1)              i|=0x10;    /* 05 */  
    if(0==1)                i|=0x20;    /* 06 */
    if(0==1)                i|=0x40;    /* 07 */
    if(0==1)                          i|=0x80;    /* 08 */

    CAN_LOG_DATA[74] = (UCHAR)(i);                                  /* 46 */
    i=0;

    /***********************************************************************/

   	if(0==1)            i|=0x01;    /* 09 */
    if(0==1)                     i|=0x02;    /* 10 */
    if(ABS_fz==1)                           i|=0x04;    /* 11 */
    if(EBD_RA==1)                           i|=0x08;    /* 12 */
    if(YAW_CDC_WORK==1)                     i|=0x10;    /* 13 */
    if(0==1)                       i|=0x20;    /* 14 */    	
    if(0==1)          i|=0x40;    /* 15 */
	if(0==1)                 i|=0x80;    /* 16 */

    CAN_LOG_DATA[75] = (UCHAR)(i);                                 /* 47 */
    i=0;
    /***********************************************************************/
    #if __ISG
    if(ISG_REQ_ON==1)            i|=0x01;    /* 17 */
    if(ISG_GEAR_OK==1)           i|=0x02;    /* 18 */
    if(ISG_READY==1)      i|=0x04;    /* 19 */
    if(HSA_ISG_flg==1)     i|=0x08;    /* 20 */
    if(0==1)      i|=0x10;    /* 21 */   	
    if(0==1)     i|=0x20;    /* 22 */    	
    if(0==1)         i|=0x40;    /* 23 */
    if(0==1)        i|=0x80;    /* 24 */
    #endif
    CAN_LOG_DATA[76] = (UCHAR)(i);                                /* 48 */
    i=0;
    /***********************************************************************/
    if(0==1)        i|=0x01;    /* 25 */
    if(0==1)       i|=0x02;    /* 26 */
    if(0==1)                 i|=0x04;    /* 27 */
    if(0==1)     i|=0x08;    /* 28 */
    if(0==1)          i|=0x10;    /* 29 */
    if(0==1)               i|=0x20;    /* 30 */
    if(HSA_ENOUGH_ENG_TORQ==1)              i|=0x40;    /* 31 */
    if(HSA_EST_CLUTCH_POSITION==1)               i|=0x80;    /* 32 */

    CAN_LOG_DATA[77] = (UCHAR)(i);                                /* 49 */
    i=0;
    /***********************************************************************/
    if(HSA_TM_TYPE==1)                      i|=0x01;    /* 33 */
    if(fu1GearR_Switch==1)                  i|=0x02;    /* 34 */
    if(fu1ClutchSwitch==1)                  i|=0x04;    /* 35 */
    if(HSA_RAPID_PHASE_OUT==1)              i|=0x08;    /* 36 */
    if(HSA_RAPID_REBRAKING_flag==1)         i|=0x10;    /* 37 */
    if(STOP_ON_HILL_flag==1)                i|=0x20;    /* 38 */
    if(HSA_INHIBITION_flag==1)              i|=0x40;    /* 39 */
    if(HSA_flg==1)                          i|=0x80;    /* 40 */

    CAN_LOG_DATA[78] = (UCHAR)(i);                                /* 50 */
    i=0;
    /***********************************************************************/
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)               i|=0x01;    /* 41 */
    if(TCL_DEMAND_SECONDARY==1)             i|=0x02;    /* 42 */
    if(S_VALVE_PRIMARY==1)                  i|=0x04;    /* 43 */
    if(S_VALVE_SECONDARY==1)                i|=0x08;    /* 44 */
      #else	                                            
    if(TCL_DEMAND_fr==1)                    i|=0x01;    /* 41 */
    if(TCL_DEMAND_fl==1)                    i|=0x02;    /* 42 */
    if(S_VALVE_RIGHT==1)                    i|=0x04;    /* 43 */
    if(S_VALVE_LEFT==1)                     i|=0x08;    /* 44 */
      #endif                                            
    if(ABS_fl==1)                           i|=0x10;    /* 45 */
    if(ABS_fr==1) 	                        i|=0x20;    /* 46 */
    if(ABS_rl==1)                           i|=0x40;    /* 47 */
    if(ABS_rr==1)                           i|=0x80;    /* 48 */	
     CAN_LOG_DATA[79] = (UCHAR)(i);                              /* 51 */
    /***********************************************************************/
  #endif        
}
#endif        


#if __ACC
void LSACC_vCallAccLogData(void)  // GSUV_OEM ESC_view : No.19
{
	UCHAR i;
	#if __ACC
    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (UCHAR)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (UCHAR)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (UCHAR)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (UCHAR)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (UCHAR)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (UCHAR)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (UCHAR)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (UCHAR)(vrad_rr>>8);
    CAN_LOG_DATA[10] = (UCHAR)(vref);                               /* 06 */
    CAN_LOG_DATA[11] = (UCHAR)(vref>>8);
    CAN_LOG_DATA[12] = (UCHAR)(lcs8AccState);              			/* 07 */
    CAN_LOG_DATA[13] = (UCHAR)(lcs8DecState);         				/* 08 */
    CAN_LOG_DATA[14] = (UCHAR)(lcs16AccTargetGIf/10);              /* 09 */
    CAN_LOG_DATA[15] = (UCHAR)(lcs16AccTargetGIf/10>>8);
    CAN_LOG_DATA[16] = (UCHAR)(lcu8AccCtrlReq);                /* 10 */
    CAN_LOG_DATA[17] = (UCHAR)(0);            /* 11 */
    CAN_LOG_DATA[18] = (UCHAR)(0);                    /* 12 */
    CAN_LOG_DATA[19] = (UCHAR)(0);               /* 13 */
    CAN_LOG_DATA[20] = (UCHAR)(0);            /* 14 */
    CAN_LOG_DATA[21] = (UCHAR)(0);               /* 15 */
    CAN_LOG_DATA[22] = (UCHAR)(lcu8DecMotorMode);                  /* 16 */
    CAN_LOG_DATA[23] = (UCHAR)(lcu8DecValveMode);                    /* 17 */
    CAN_LOG_DATA[24] = (UCHAR)(TC_MFC_Voltage_Primary_dash);        /* 18 */
    CAN_LOG_DATA[25] = (UCHAR)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[26] = (UCHAR)(TC_MFC_Voltage_Secondary_dash);      /* 19 */
    CAN_LOG_DATA[27] = (UCHAR)(TC_MFC_Voltage_Secondary_dash>>8);
    CAN_LOG_DATA[28] = (UCHAR)(target_vol);                     /* 20 */
    CAN_LOG_DATA[29] = (UCHAR)(target_vol>>8);
    CAN_LOG_DATA[30] = (UCHAR)(fu16CalVoltMOTOR);                  /* 21 */
    CAN_LOG_DATA[31] = (UCHAR)(fu16CalVoltMOTOR>>8);
    CAN_LOG_DATA[32] = (UCHAR)(lcu16DecFeedForwardCnt);                  /* 22 */
    CAN_LOG_DATA[33] = (UCHAR)(lcu16DecFeedForwardCnt>>8);
    CAN_LOG_DATA[34] = (UCHAR)(lcu16DecFeedForwardTh);                /* 23 */
    CAN_LOG_DATA[35] = (UCHAR)(lcu16DecFeedForwardTh>>8);
    CAN_LOG_DATA[36] = (UCHAR)(mpress);                             /* 24 */
    CAN_LOG_DATA[37] = (UCHAR)(mpress>>8);
    CAN_LOG_DATA[38] = (UCHAR)(gs_sel);                             /* 25 */
    CAN_LOG_DATA[39] = (UCHAR)(mtp);                                /* 26 */
    CAN_LOG_DATA[40] = (UCHAR)(wstr);                               /* 27 */
    CAN_LOG_DATA[41] = (UCHAR)(wstr>>8);
    CAN_LOG_DATA[42] = (UCHAR)(lcs16AccBrkLampOnRefCnt);                            /* 28 */
    CAN_LOG_DATA[43] = (UCHAR)(lcs16AccBrkLampOnRefCnt>>8);
    CAN_LOG_DATA[44] = (UCHAR)(lcs16AccTargetG);                    /* 29 */
    CAN_LOG_DATA[45] = (UCHAR)(lcs16AccTargetG>>8);
    #if (__AX_SENSOR)    
    CAN_LOG_DATA[46] = (UCHAR)(lss16absAxOffsetComp1000g);                      /* 30 */
    CAN_LOG_DATA[47] = (UCHAR)(lss16absAxOffsetComp1000g>>8);
    #endif
    CAN_LOG_DATA[48] = (UCHAR)(lcs16DecDecel);                       /* 31 */
    CAN_LOG_DATA[49] = (UCHAR)(lcs16DecDecel>>8);
    CAN_LOG_DATA[50] = (UCHAR)(lcs16DecCtrlError);                 /* 32 */
    CAN_LOG_DATA[51] = (UCHAR)(lcs16DecCtrlError>>8);
    #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[52] = (UCHAR)(pos_fr_1_100bar);                    /* 33 */
    CAN_LOG_DATA[53] = (UCHAR)(pos_fr_1_100bar>>8);
    #else
    CAN_LOG_DATA[52] = (UCHAR)(0);                    /* 33 */
    CAN_LOG_DATA[53] = (UCHAR)(0>>8);
    #endif
    CAN_LOG_DATA[54] = (UCHAR)(eng_torq);                       /* 34 */
    CAN_LOG_DATA[55] = (UCHAR)(eng_torq>>8);
    CAN_LOG_DATA[56] = (UCHAR)(eng_rpm);                            /* 35 */
    CAN_LOG_DATA[57] = (UCHAR)(eng_rpm>>8);
    CAN_LOG_DATA[58] = (UCHAR)(lcu16DecTimeToAct);                                  /* 36 */
    CAN_LOG_DATA[59] = (UCHAR)(lcu16DecTimeToAct>>8);
    #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[60] = (UCHAR)(pos_fl_1_100bar);                                  /* 37 */
    CAN_LOG_DATA[61] = (UCHAR)(pos_fl_1_100bar>>8);
    #else
    CAN_LOG_DATA[60] = (UCHAR)(0);                                  /* 37 */
    CAN_LOG_DATA[61] = (UCHAR)(0>>8);
    #endif
    CAN_LOG_DATA[62] = (UCHAR)(lcs16DecCtrlInput);               /* 38 */
    CAN_LOG_DATA[63] = (UCHAR)(lcs16DecCtrlInput>>8);
    CAN_LOG_DATA[64] = (UCHAR)(lcs16AccTargetGOld);                    /* 39 */
    CAN_LOG_DATA[65] = (UCHAR)(lcs16AccTargetGOld>>8);
    CAN_LOG_DATA[66] = (UCHAR)(lcs16AdcTGdelaycycle);                 /* 40 */
    CAN_LOG_DATA[67] = (UCHAR)(lcs16AdcTGdelaycycle>>8); 
    CAN_LOG_DATA[68] = (UCHAR)(0);             /* 41 */
    CAN_LOG_DATA[69] = (UCHAR)(lsabss16DecelRefiltByVref5);                      /* 42 */
    CAN_LOG_DATA[70] = (UCHAR)(lsabss16DecelRefiltByVref5>>8);                      
    CAN_LOG_DATA[71] = (UCHAR)(MFC_PWM_DUTY_ESV);               /* 43 */
    CAN_LOG_DATA[72] = (UCHAR)(MFC_PWM_DUTY_P);		    /* 44 */
    CAN_LOG_DATA[73] = (UCHAR)(MFC_PWM_DUTY_S);           /* 45 */    
  #endif
    /***********************************************************************/
    i=0;
    /***********************************************************************/
  #if __ACC
    if(lcu1AccInhibitFlg==1)                 i|=0x01;    /* 01 */
    if(lcu1AccActiveFlg==1)                 i|=0x02;    /* 02 */
    if(lcu1AdcBrkLampOnReq==1)               i|=0x04;    /* 03 */
    if(0==1)              i|=0x08;    /* 04 */
    if(0==1)              i|=0x10;    /* 05 */  
    if(0==1)                i|=0x20;    /* 06 */
    if(0==1)                i|=0x40;    /* 07 */
    if(AUTO_TM==1)                          i|=0x80;    /* 08 */
  #endif  
    CAN_LOG_DATA[74] = (UCHAR)(i);                                  /* 46 */
    i=0;

    /***********************************************************************/
  #if __ACC
   	if(0==1)            i|=0x01;    /* 09 */
    if(ESP_TCS_ON==1)                     i|=0x02;    /* 10 */
    if(ABS_fz==1)                           i|=0x04;    /* 11 */
    if(EBD_RA==1)                           i|=0x08;    /* 12 */
    if(YAW_CDC_WORK==1)                     i|=0x10;    /* 13 */
    if(0==1)                       i|=0x20;    /* 14 */    	
    if(0==1)          i|=0x40;    /* 15 */
	if(0==1)                 i|=0x80;    /* 16 */
  #endif
    CAN_LOG_DATA[75] = (UCHAR)(i);                                 /* 47 */
    i=0;
    /***********************************************************************/
  #if __ACC
    if(lcu1DecCtrlReqFlg==1)            i|=0x01;    /* 17 */
    if(lcu1DecSmoothClosingReqFlg==1)           i|=0x02;    /* 18 */
    if(lcu1DecFeedForwardReqFlg==1)      i|=0x04;    /* 19 */
    if(0==1)     i|=0x08;    /* 20 */
    if(0==1)      i|=0x10;    /* 21 */   	
    if(0==1)     i|=0x20;    /* 22 */    	
    if(0==1)         i|=0x40;    /* 23 */
    if(0==1)        i|=0x80;    /* 24 */
  #endif  
    CAN_LOG_DATA[76] = (UCHAR)(i);                                /* 48 */
    i=0;
    /***********************************************************************/
  #if __ACC
    if(0==1)        i|=0x01;    /* 25 */
    if(0==1)       i|=0x02;    /* 26 */
    if(0==1)                 i|=0x04;    /* 27 */
    if(0==1)     i|=0x08;    /* 28 */
    if(0==1)          i|=0x10;    /* 29 */
    if(0==1)               i|=0x20;    /* 30 */
    if(0==1)              i|=0x40;    /* 31 */
    if(0==1)               i|=0x80;    /* 32 */
  #endif 
    CAN_LOG_DATA[77] = (UCHAR)(i);                                /* 49 */
    i=0;
    /***********************************************************************/
  #if __ACC    
    if(0==1)                      i|=0x01;    /* 33 */
    if(0==1)                  i|=0x02;    /* 34 */
    if(0==1)                  i|=0x04;    /* 35 */
    if(0==1)              i|=0x08;    /* 36 */
    if(0==1)         i|=0x10;    /* 37 */
    if(0==1)                i|=0x20;    /* 38 */
    if(0==1)              i|=0x40;    /* 39 */
    if(0==1)                          i|=0x80;    /* 40 */
   #endif
    CAN_LOG_DATA[78] = (UCHAR)(i);                                /* 50 */
    i=0;
    /***********************************************************************/
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)               i|=0x01;    /* 41 */
    if(TCL_DEMAND_SECONDARY==1)             i|=0x02;    /* 42 */
    if(S_VALVE_PRIMARY==1)                  i|=0x04;    /* 43 */
    if(S_VALVE_SECONDARY==1)                i|=0x08;    /* 44 */
      #else	                                            
    if(TCL_DEMAND_fr==1)                    i|=0x01;    /* 41 */
    if(TCL_DEMAND_fl==1)                    i|=0x02;    /* 42 */
    if(S_VALVE_RIGHT==1)                    i|=0x04;    /* 43 */
    if(S_VALVE_LEFT==1)                     i|=0x08;    /* 44 */
      #endif                                            
    if(ABS_fl==1)                           i|=0x10;    /* 45 */
    if(ABS_fr==1) 	                        i|=0x20;    /* 46 */
    if(ABS_rl==1)                           i|=0x40;    /* 47 */
    if(ABS_rr==1)                           i|=0x80;    /* 48 */	
     CAN_LOG_DATA[79] = (UCHAR)(i);                              /* 51 */
    /***********************************************************************/
}
#endif

/*******************************************************************************
* FUNCTION NAME:        LSTCS_vCallCANCheckLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of CAN Check
********************************************************************************/
void LSTCS_vCallCANCheckLogData(void)
{
	/*	uint8_t i;

#if ( (__HMC_CAN_VER == CAN_1_2) || (__HMC_CAN_VER == CAN_1_3) || (__HMC_CAN_VER == CAN_1_4) || (__HMC_CAN_VER == CAN_1_6) )
        CAN_LOG_DATA[0] =(uint8_t) (system_loop_counter);
        CAN_LOG_DATA[1] =(uint8_t) (system_loop_counter>>8);   //uint16_t,1
        CAN_LOG_DATA[2] =(uint8_t) (vrad_fl);
        CAN_LOG_DATA[3] =(uint8_t) (vrad_fl>>8);               //uint16_t,8
        CAN_LOG_DATA[4] =(uint8_t) (vrad_fr);
        CAN_LOG_DATA[5] =(uint8_t) (vrad_fr>>8);               //uint16_t,8
        CAN_LOG_DATA[6] =(uint8_t) (vrad_rl);
        CAN_LOG_DATA[7] =(uint8_t) (vrad_rl>>8);               //uint16_t,8

        CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);
        CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);              //uint16_t,8
#if __VDC
    CAN_LOG_DATA[10] =(uint8_t) (cur_cycle_time);//max_cycle_time;//vref;
    CAN_LOG_DATA[11] =(uint8_t) (cur_cycle_time>>8);//max_cycle_time>>8);//vref>>8);
#endif
        CAN_LOG_DATA[12] =(uint8_t) (vref);
        CAN_LOG_DATA[13] =(uint8_t) (vref>>8);                //uint16_t,8
        CAN_LOG_DATA[14] =(uint8_t) (vel_r);
        CAN_LOG_DATA[15] =(uint8_t) (vel_r>>8);                //uint16_t,8

    if(AUTO_TM==1) {
        CAN_LOG_DATA[16] = (uint8_t)(lespu8TCU_G_SEL_DISP);          //G_SEL_DISP
        CAN_LOG_DATA[17] = (uint8_t)(lespu8TCU_TAR_GC);          //TAR_GC
        }
    else {
        CAN_LOG_DATA[16] =(uint8_t) (0);                 //0
        CAN_LOG_DATA[17] =(uint8_t) (0);                 //0
        }

    if(EMS2_0>>6==1)
    {
        CAN_LOG_DATA[18] =(uint8_t) (EMS2_0&0x3f);      //CONV_TCU
    }

        CAN_LOG_DATA[19] =(uint8_t) (max_torq);          //TQ_STND

        CAN_LOG_DATA[20] =(uint8_t) (TCS1_0);
        CAN_LOG_DATA[21] =(uint8_t) (TCS1_1);
        CAN_LOG_DATA[22] =(uint8_t) (TCS1_2);
        CAN_LOG_DATA[23] = (uint8_t)(((uint16_t)(TCS1_3*2))/5);//TQI_TCS

        CAN_LOG_DATA[24] =(uint8_t) (EMS2_7);        //ENG_VOL
        CAN_LOG_DATA[25] =(uint8_t) (0);

        CAN_LOG_DATA[26] =(uint8_t) (((EMS2_4&0x3C)/4));    //ENG_CHR
        CAN_LOG_DATA[27] =(uint8_t) (0);

#if __VDC
  if(!vdc_on_flg) {
    CAN_LOG_DATA[28] =(uint8_t) (mpress_est);
    CAN_LOG_DATA[29] =(uint8_t) (mpress_est>>8);
  }
  else {
    CAN_LOG_DATA[28] =(uint8_t) (mpress);
    CAN_LOG_DATA[29] =(uint8_t) (mpress>>8);
  }
#else
        CAN_LOG_DATA[28] =(uint8_t) (dv);
        CAN_LOG_DATA[29] =(uint8_t) (dv>>8);
#endif

#if __VDC
  if(!vdc_on_flg) {
    CAN_LOG_DATA[30] =(uint8_t) (flpn);
    CAN_LOG_DATA[31] =(uint8_t) (flpn>>8);
    CAN_LOG_DATA[32] =(uint8_t) (frpn);
    CAN_LOG_DATA[33] =(uint8_t) (frpn>>8);
    CAN_LOG_DATA[34] =(uint8_t) (rlpn);
    CAN_LOG_DATA[35] =(uint8_t) (rlpn>>8);
    CAN_LOG_DATA[36] =(uint8_t) (rrpn);
    CAN_LOG_DATA[37] =(uint8_t) (rrpn>>8);
        }
  else {
    CAN_LOG_DATA[30] =(uint8_t) (drive_torq);
    CAN_LOG_DATA[31] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (cal_torq);
    CAN_LOG_DATA[33] =(uint8_t) (cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (eng_torq);
    CAN_LOG_DATA[35] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (eng_rpm);
    CAN_LOG_DATA[37] =(uint8_t) (eng_rpm>>8);
        }
#else
    CAN_LOG_DATA[30] =(uint8_t) (drive_torq);
    CAN_LOG_DATA[31] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[32] =(uint8_t) (cal_torq);
    CAN_LOG_DATA[33] =(uint8_t) (cal_torq>>8);
    CAN_LOG_DATA[34] =(uint8_t) (eng_torq);
    CAN_LOG_DATA[35] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[36] =(uint8_t) (eng_rpm);
    CAN_LOG_DATA[37] =(uint8_t) (eng_rpm>>8);
#endif
        CAN_LOG_DATA[38] =(uint8_t) (gear_pos);
        CAN_LOG_DATA[39] = (uint8_t)(mtp);

        CAN_LOG_DATA[40] = (uint8_t)(((uint16_t)(TCS1_4*2))/5);//TQI_MSR
        CAN_LOG_DATA[41] = (uint8_t)(((uint16_t)(TCS1_5*2))/5);//TQI_SLW_TCS
        CAN_LOG_DATA[42] =(uint8_t) (TCU1_1);
        CAN_LOG_DATA[43] =(uint8_t) (TCU1_2);
        CAN_LOG_DATA[44] =(uint8_t) (TCU1_3);
        CAN_LOG_DATA[45] =(uint8_t) (ENG_VAR);//TCU1[4];
//        CAN_LOG_DATA[46] = ITM1[2];
        CAN_LOG_DATA[47] =(uint8_t) (TCU1_0);

        CAN_LOG_DATA[48] =(uint8_t) (EMS2_0);
        CAN_LOG_DATA[49] =(uint8_t) (0);
        CAN_LOG_DATA[50] =(uint8_t) (EMS2_4);
        CAN_LOG_DATA[51] =(uint8_t) (0);
        CAN_LOG_DATA[52] =(uint8_t) (EMS2_5);
        CAN_LOG_DATA[53] =(uint8_t) (0);
        CAN_LOG_DATA[54] =(uint8_t) (EMS2_6);
        CAN_LOG_DATA[55] =(uint8_t) (0);

#if __VDC
        CAN_LOG_DATA[56] =(uint8_t) (wstr);
        CAN_LOG_DATA[57] =(uint8_t) (wstr>>8);
#else
        CAN_LOG_DATA[56] =(uint8_t) (avr_btc_slip);
        CAN_LOG_DATA[57] =(uint8_t) (avr_btc_slip>>8);
#endif
        CAN_LOG_DATA[58] =(uint8_t) (fu16CalVoltMOTOR);
        CAN_LOG_DATA[59] =(uint8_t) (fu16CalVoltMOTOR>>8);
        CAN_LOG_DATA[60] =(uint8_t) (torq_target);
        CAN_LOG_DATA[61] =(uint8_t) (torq_target>>8);
        CAN_LOG_DATA[62] =(uint8_t) (EMS1_0);
        CAN_LOG_DATA[63] =(uint8_t) (0);
        tempW0=(((int16_t)EMS1_3<<8)|(int16_t)EMS1_2)/4;
        CAN_LOG_DATA[64] =(uint8_t) (tempW0);//tempW0;//temppress
        CAN_LOG_DATA[65] =(uint8_t) (tempW0>>8);//tempW0>>8);               //N (eng_rpm)

        i=0;
        if (BLS_PIN                         ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (ABS_fz             ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
        if (BTC_fz              ==1) i|=0x10;
        if (ETCS_ON                         ==1) i|=0x20;
        if (FTCS_ON                         ==1) i|=0x40;
#if __AX_SENSOR
        if (USE_ALONG           ==1) i|=0x80;
      #else
//        if (USE_ALONG           ==1) i|=0x80;
      #endif
        CAN_LOG_DATA[66] =(uint8_t) (i);

        i=0;
        if (TCS1_0&0x01           ) i|=0x01;       //TCS_REQ
        if (TCS1_0&0x08           ) i|=0x02;       //TCS_GSC
        if (EMS1_0&0x40           ) i|=0x04;
        if (VALVE_ACT           ==1) i|=0x08;
#if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
        if (VREF_FAIL           ==1) i|=0x10;
      #else
//        if (VREF_FAIL           ==1) i|=0x10; //by anso
      #endif
        if (TC_MOT_ON           ==1) i|=0x20;
        if (BTCS_TWO_OFF        ==1) i|=0x40;
        if (LFC_Split_flag      ==1) i|=0x80;
        CAN_LOG_DATA[67] =(uint8_t) (i);

        i = 0;
        if (AV_VL_fl            ==1) i|=0x01;
        if (HV_VL_fl            ==1) i|=0x02;
        if (ABS_fl              ==1) i|=0x04;
        if (BTCS_fl             ==1) i|=0x08;
        if (TCL_DEMAND_fl       ==1) i|=0x10;
        if (POS_HOLD_fl         ==1) i|=0x20;
        if (FSF_fl              ==1) i|=0x40;
        if (BUILT_fl            ==1) i|=0x80;
        CAN_LOG_DATA[68] =(uint8_t) (i);

        i=0;
        if (AV_VL_fr                        ==1) i|=0x01;
        if (HV_VL_fr                        ==1) i|=0x02;
        if (ABS_fr              ==1) i|=0x04;
        if (BTCS_fr                         ==1) i|=0x08;
        if (TCL_DEMAND_fr       ==1) i|=0x10;
        if (POS_HOLD_fr         ==1) i|=0x20;
        if (FSF_fr              ==1) i|=0x40;
        if (BUILT_fr            ==1) i|=0x80;
        CAN_LOG_DATA[69] =(uint8_t) (i);

        i=0;
        if (AV_VL_rl                        ==1) i|=0x01;
        if (HV_VL_rl                        ==1) i|=0x02;
        if (ABS_rl              ==1) i|=0x04;
        if (BTCS_rl             ==1) i|=0x08;
        if (EBD_rl              ==1) i|=0x10;
        if (MSL_BASE_rl         ==1) i|=0x20;
        if (AUTO_TM              ==1) i|=0x40;
        if (ALONG_OFFSET        ==1) i|=0x80;
        CAN_LOG_DATA[70] =(uint8_t) (i);

        i = 0;
        if (AV_VL_rr                        ==1) i|=0x01;
        if (HV_VL_rr                        ==1) i|=0x02;
        if (ABS_rr              ==1) i|=0x04;
        if (BTCS_rr             ==1) i|=0x08;
        if (EBD_rr              ==1) i|=0x10;
        if (MSL_BASE_rr         ==1) i|=0x20;
        if (FSF_rr              ==1) i|=0x40;
#if __AX_SENSOR
        if (BACKWARD_MOVE       ==1) i|=0x80;
       #else 	
//        if (BACKWARD_MOVE       ==1) i|=0x80;
       #endif 	
        CAN_LOG_DATA[71] =(uint8_t) (i);

        CAN_LOG_DATA[72] =(uint8_t) (afz);
        CAN_LOG_DATA[73] =(uint8_t) (afz>>8);                          //int16_t,100
        CAN_LOG_DATA[74] =(uint8_t) (((uint16_t)(EMS1_4*2))/5);  //uint8_t,1   TQI
        CAN_LOG_DATA[75] =(uint8_t) (((uint16_t)(EMS1_1*2))/5);  //uint8_t,1   TQI_A
        CAN_LOG_DATA[76] =(uint8_t) (((uint16_t)(EMS1_5*2))/5);  //uint8_t,1   TQFR
        CAN_LOG_DATA[77] =(uint8_t) ((int16_t)((EMS2_5-32)*7)/15); //uint8_t,1   TPS
        CAN_LOG_DATA[78] =(uint8_t) ((EMS1_0&0x30)/16);       //TQ_COR_STAT
                                                    //00h :default
                                                    //01h :can not be adjusted precisely
                                                    //10h :cannot be reduced
                                                    //11h :torque intervention is terminated
        CAN_LOG_DATA[79] =(uint8_t) (lespu1TCS_ESP_CTL);
      #endif
*/
;
}

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallESCPlusLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          ESC Plus - delta M, Pcontrol
********************************************************************************/

void LSESP_vCallESCPlusLogData(void)
{
	int32_t CricP_temp = 0;

    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);							//ch01
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
                   																			
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                		//ch02
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
                               																			
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                		//ch03
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
                               																			
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                		//ch04
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
                              																			
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                		//ch05
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
                             																			
    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);								//ch06
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);
    
#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
if(esp_tempW9 > 120) esp_tempW9 =120;
else if(esp_tempW9< (-120)) esp_tempW9 = -120;

    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);							//ch07
    
    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());		//ch08
    
    CAN_LOG_DATA[14] =(uint8_t) (vref5);										//ch09
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

esp_tempW6= esp_mu/10;

if(esp_tempW6 > 120) esp_tempW6 =120;
else if(esp_tempW6< (-120)) esp_tempW6 = -120;

    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6); 																													//ch10
    
	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);     //ch11

    //CAN_LOG_DATA[18] =(uint8_t) (target_vol);									//ch12 for MGH only
    //CAN_LOG_DATA[19] =(uint8_t) (target_vol>>8);
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  	  esp_tempW3 = RR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[18] =(uint8_t) (esp_tempW3);									//ch12 for MGH only
    CAN_LOG_DATA[19] =(uint8_t) (esp_tempW3>>8);
                          
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  	  esp_tempW3 = FR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[20] =(esp_tempW3);              					//ch13
    CAN_LOG_DATA[21] =(esp_tempW3>>8);
    CAN_LOG_DATA[22] =(uint8_t)	(FL_ESC.lcesps16IdbTarPress/10);		//ch14

 	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  		esp_tempW3 = FR_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else
  	{
  		esp_tempW3 = FL_ESC.lcesps16IdbDelTarPress/10;
  	}
    CAN_LOG_DATA[23] =(uint8_t)	(esp_tempW3);
                          

    CAN_LOG_DATA[24] =(uint8_t) (FR_ESC.lcesps16IdbTarPress/10);//(esp_tempW0);                         //ch15
    
 	if(SLIP_CONTROL_NEED_RL==1)
  	{
 		esp_tempW3 = RL_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  		esp_tempW3 = RR_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else
  	{
  		esp_tempW3 = RL_ESC.lcesps16IdbDelTarPress/10;
  	}
    CAN_LOG_DATA[25] =(uint8_t)	(esp_tempW3);

    //CAN_LOG_DATA[26] =(uint8_t) (esp_tempW1);                         //ch16
    //CAN_LOG_DATA[27] =(uint8_t) (esp_tempW1>>8);

 	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  		esp_tempW3 = RR_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else
  	{
  		esp_tempW3 = RL_ESC.lcesps16IdbCurrentPress/10;
  	}
    CAN_LOG_DATA[26] =(uint8_t) (esp_tempW3);                         //ch16
    CAN_LOG_DATA[27] =(uint8_t) (esp_tempW3>>8);

    CAN_LOG_DATA[28] =(uint8_t) (alat);                               //ch17
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);  
    
if(SLIP_CONTROL_NEED_FL==1)
{
    esp_tempW2 = del_target_slip_fl;
}
else if(SLIP_CONTROL_NEED_FR==1)
{
    esp_tempW2 = del_target_slip_fr;
}
else
{
    esp_tempW2 = 0;
}

    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW2);                             //ch18
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW2>>8); 
    
    CAN_LOG_DATA[32] =(uint8_t) (Beta_MD);										//ch19
    CAN_LOG_DATA[33] =(uint8_t) (Beta_MD>>8);
    
    if(BTCS_fl == 1)
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16SecTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16SecTcTargetPress>>8);//(delta_moment_q_os>>8);
    }
    else if(BTCS_fr == 1)
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16PriTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16PriTcTargetPress>>8);//(delta_moment_q_os>>8);
    }
    else
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16PriTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16PriTcTargetPress>>8);//(delta_moment_q_os>>8);
    }
    
 	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  		esp_tempW3 = FR_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else
  	{
  		esp_tempW3 = FL_ESC.lcesps16IdbCurrentPress/10;
  	}
    CAN_LOG_DATA[36] =(uint8_t) (esp_tempW3);                 //ch21
    CAN_LOG_DATA[37] =(uint8_t) (esp_tempW3>>8);

    if (AUTO_TM	== 1)
    {
        tempW0 = (INT)gear_pos;
    }
    else
    {
        tempW0 = (INT)gear_state;
    }
    
    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW0=FL_ESC.lcespu8CntBbsEsc;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
        esp_tempW0=RR_ESC.lcespu8CntBbsEsc;
    }
    else
    {
        esp_tempW0=FL_ESC.lcespu8CntBbsEsc;
    }
    
    if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW1=FR_ESC.lcespu8CntBbsEsc;
    }
    else if(SLIP_CONTROL_NEED_RL==1)
    {
        esp_tempW1=RL_ESC.lcespu8CntBbsEsc;
    }
    else
    {
        esp_tempW1=FR_ESC.lcespu8CntBbsEsc;
    }
    CAN_LOG_DATA[38] =(int8_t) (RL.u8_Estimated_Active_Press);//(esp_tempW0);//(FL_ESC.u8escNoArraytime);//(RL_ESC.lcespu8IdbDumpRateCnt);//(tempW0);                               		//ch22
    CAN_LOG_DATA[39] =(int8_t) (RR.u8_Estimated_Active_Press);//(esp_tempW1);//(RR_ESC.lcespu8IdbDumpRateCnt);//(mtp);                                    //ch23

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               		//ch24
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch25
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                  	//ch26
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);

    if(SLIP_CONTROL_NEED_FL==1)
    {
    	esp_tempW1=FL_ESC.lcesps16IdbriseDelTp;
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
    	esp_tempW1=FR_ESC.lcesps16IdbriseDelTp;
    }
    else
    {
    	esp_tempW1=FL_ESC.lcesps16IdbriseDelTp;
    }
    CAN_LOG_DATA[46] =(uint8_t) (esp_tempW1);					                		//ch27

    if(SLIP_CONTROL_NEED_RL==1)
    {
    	esp_tempW1=RL_ESC.lcesps16IdbriseDelTp;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
    	esp_tempW1=RR_ESC.lcesps16IdbriseDelTp;
    }
    else
    {
    	esp_tempW1=RL_ESC.lcesps16IdbriseDelTp;
    }
    CAN_LOG_DATA[47] =(uint8_t) (esp_tempW1);
   
    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 		//ch28
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
		
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                       		//ch29
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
		
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                 		//ch30
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);
		
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);								//ch31
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);

    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                         //ch32
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);
    
    CAN_LOG_DATA[58] =(uint8_t) (FL.u8_Estimated_Active_Press);//(cal_torq);                 					    //ch33
    CAN_LOG_DATA[59] =(uint8_t) (FR.u8_Estimated_Active_Press);//(cal_torq>>8);

	if( yaw_out > 0 ) 
	{
	    esp_tempW7 = ay_vx_dot_sig_plus;
	}
	else
	{
	    esp_tempW7 = ay_vx_dot_sig_minus;
	}
    CAN_LOG_DATA[60] =(uint8_t) (RL_ESC.lcesps16IdbTarPress/10);   					                //ch34
    CAN_LOG_DATA[61] =(uint8_t) (FL_ESC.lcesps16CompDp/10);//(RL_ESC.lcesps16LimitIdbTargetP/10);					
	

    CAN_LOG_DATA[62] =(uint8_t) (RR_ESC.lcesps16IdbTarPress/10);					                 //ch35
    CAN_LOG_DATA[63] =(uint8_t) (FR_ESC.lcesps16CompDp/10);//(RR_ESC.lcesps16LimitIdbTargetP/10);//(RR_ESC.lcesps16IdbTarPress>>8);
    
    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[64] =(uint8_t) (lsesps16AHBGEN3mpress);               //ch36
    CAN_LOG_DATA[65] =(uint8_t) (lsesps16AHBGEN3mpress>>8);
    #else
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                              //ch36
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);
    #endif

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;			                 //flag1
//    if(FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)      tempB5|=0x02;			//flag2
    if(ldespu1ESC1stCycle)      tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;											 //flag3
    if(EBD_RA)                      tempB5|=0x08;											 //flag4
    if(BTC_fz==1)                   tempB5|=0x10;											 //flag5
#if __TCS
    if(ETCS_ON)                     tempB5|=0x20;											 //flag6
#endif
    if(YAW_CDC_WORK)                tempB5|=0x40;											 //flag7
#if __TCS
    if(ESP_TCS_ON)                  tempB5|=0x80;											 //flag8
#endif
    if(lcu1US2WHControlFlag)       tempB4|=0x01;											 					//flag9

#if __ROP    
    if(U1EscAfterStrokeR )          			tempB4|=0x02;											 //flag10
#else
	if(U1EscAfterStrokeR)         				tempB4|=0x02;											 //flag10
#endif
    if(PARTIAL_BRAKE)               tempB4|=0x04;											 //flag11

//   if(FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)           tempB4|=0x08;	//flag12
   if(HSA_flg)           tempB4|=0x08;	//flag12

    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn)    tempB4|=0x10;											 //flag13
    #else
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;											 //flag13
    #endif
    
    if(U1WheelValveCntr)     tempB4|=0x20;											 //flag14

    if(AUTO_TM) {
        if(BACK_DIR)                tempB4|=0x40;											 //flag15
    } else {
#if __AX_SENSOR
        if(BACKWARD_MOVE)           tempB4|=0x40;											 //flag15
#else
       if(0)           tempB4|=0x40;
#endif
    }
    
//#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;	//flag16
//#else
    if(U1EscAfterStrokeR_OLD)             tempB4|=0x80;	 //flag16
//#endif    
  

    if(AV_VL_fl)                    tempB3|=0x01;											 //flag17
    if(HV_VL_fl)                    tempB3|=0x02;											 //flag18
    if(ABS_fl)                      tempB3|=0x04;											 //flag19

    if(U1StrokeRecoveryforESC)         tempB3|=0x08;											 //flag20


//#if !__SPLIT_TYPE
    if(FL_ESC.lcespu1DctMuxPrio)               tempB3|=0x10;											 //flag21
//#else
//    if(0)        tempB3|=0x10;											 //flag21
//#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;											 //flag22										 //flag22
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)                					 tempB3|=0x40;				//flag23
//    if(INITIAL_BRAKE||FLAG_ACTIVE_BOOSTER||FLAG_ACTIVE_BOOSTER_PRE)            tempB3|=0x80;				//flag24
    if(FLAG_ON_CENTER)            tempB3|=0x80;				//flag24

    if(AV_VL_fr)                    tempB2|=0x01;											 //flag25
    if(HV_VL_fr)                    tempB2|=0x02;											 //flag26
    if(ABS_fr)                      tempB2|=0x04;											 //flag27
#if __P_CONTROL
    if(0)                     tempB2|=0x08;
#else
    if(BTCS_fr)                     tempB2|=0x08;											 //flag28
#endif
//#if !__SPLIT_TYPE
    if(FR_ESC.lcespu1DctMuxPrio)               tempB2|=0x10;											 //flag29
//#else
//    if(0)          tempB2|=0x10;											 //flag29
//#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;											 //flag30
    if(YAW_CHANGE_LIMIT)          tempB2|=0x40;						//flag31

#if __BANK_ESC_IMPROVEMENT_CONCEPT
    if(ldespu1BankLowerSuspectFlag) tempB2|=0x80;											 //flag32
#else    
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;											 //flag32
#endif

    if(AV_VL_rl)                    tempB1|=0x01;											 //flag33
    if(HV_VL_rl)                    tempB1|=0x02;											 //flag34
    if(ABS_rl)                      tempB1|=0x04;											 //flag35
    if(BTCS_fl||BTCS_rl) 						tempB1|=0x08;											 //flag36
    if(EBD_rl)                      tempB1|=0x10;											 //flag37
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;											 //flag38
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)                tempB1|=0x40;											 //flag39
#else
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)           tempB1|=0x40;											 //flag39
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(U1EscAfterStrokeR)          tempB1|=0x80;		//flag40
#else
    if(U1EscAfterStrokeR)        			tempB1|=0x80;											 //flag40
#endif

    if(AV_VL_rr)            				tempB0|=0x01;											 //flag41
    if(HV_VL_rr)            				tempB0|=0x02;											 //flag42
    if(ABS_rr)              				tempB0|=0x04;											 //flag43
#if __P_CONTROL
    if(0)             tempB0|=0x08;
#else
    if(CROSS_SLIP_CONTROL_rr||RR.U1EscOtherWheelCnt)    				tempB0|=0x08;											 //flag44
#endif
    if(EBD_rr)              				tempB0|=0x10;											 //flag45
    if(ESP_BRAKE_CONTROL_RR)  			tempB0|=0x20;											 //flag46
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)       				tempB0|=0x40;											 			//flag47
#else
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)     				tempB0|=0x40;											 //flag47
#endif

    if(CROSS_SLIP_CONTROL_rl||RL.U1EscOtherWheelCnt)          tempB0|=0x80;   //flag48


    CAN_LOG_DATA[66] =(uint8_t) (tempB5);				//ch40
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);				//ch41
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);       //ch42
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);       //ch43
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);       //ch44
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);       //ch45

if(SLIP_CONTROL_NEED_RL==1)
{
    esp_tempW6 = del_target_slip_rl;
}
else if(SLIP_CONTROL_NEED_RR==1)
{
    esp_tempW6 = del_target_slip_rr;
}
else
{
    esp_tempW6 = 0;
}
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);								//ch43						
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6>>8);

    CAN_LOG_DATA[74] =(uint8_t) (FL.flags);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[75] =(uint8_t) (FR.flags);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[76] =(uint8_t) (FL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[77] =(uint8_t) (FR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[78] =(uint8_t) (RL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[79] =(uint8_t) (RR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);


//		if(SLIP_CONTROL_NEED_FL==1)
//		{
//  		 CAN_LOG_DATA[76] =(uint8_t) (laesc_FL1HP.u8PwmDuty);	//ch46
//		}
//		else
//		{
//			 CAN_LOG_DATA[76] =(uint8_t) (FL.pwm_duty_temp);			//ch46
//		}
//
//		if(SLIP_CONTROL_NEED_FR==1)
//		{
//  		 CAN_LOG_DATA[77] =(uint8_t) (laesc_FR1HP.u8PwmDuty);	//ch47
//		}
//		else
//		{
//			 CAN_LOG_DATA[77] =(uint8_t) (FR.pwm_duty_temp);			//ch47
//		}
//
//		if(SLIP_CONTROL_NEED_RL==1)
//		{
//  		 CAN_LOG_DATA[78] =(uint8_t) (laesc_RL1HP.u8PwmDuty);	//ch48
//		}
//		else
//		{
//			 CAN_LOG_DATA[78] =(uint8_t) (RL.pwm_duty_temp);			//ch48
//		}
//
//		if(SLIP_CONTROL_NEED_RR==1)
//		{
//  		 CAN_LOG_DATA[79] =(uint8_t) (laesc_RR1HP.u8PwmDuty);	//ch49
//		}
//		else
//		{
//			 CAN_LOG_DATA[79] =(uint8_t) (RR.pwm_duty_temp);			//ch49
//		}
/*
#if defined(__EXTEND_LOGGER)
		
	CAN_LOG_DATA[80] = (uint8_t)(alat_dot_lf);
	CAN_LOG_DATA[81] = (uint8_t)(alat_dot_lf>>8);
	 
	CAN_LOG_DATA[82] = (uint8_t)(det_wstr_dot_lf); 
	CAN_LOG_DATA[83] = (uint8_t)(det_wstr_dot_lf>>8);	 
	
	CAN_LOG_DATA[84] = (uint8_t)(roll_angle); 
	CAN_LOG_DATA[85] = (uint8_t)(roll_angle>>8); 	
	
	CAN_LOG_DATA[86] = (uint8_t)(roll_angle_rate); 
	CAN_LOG_DATA[87] = (uint8_t)(roll_angle_rate>>8); 
	
	CAN_LOG_DATA[88] = (uint8_t)(RI_max);	 
	CAN_LOG_DATA[89] = (uint8_t)(TTL_time);
	
	  #if (__ROP == ENABLE) 	
	CAN_LOG_DATA[90] = (uint8_t)(moment_pre_front_rop/100);
	CAN_LOG_DATA[91] = (uint8_t)(moment_q_front_rop/100); 	
	  #else
	CAN_LOG_DATA[90] = (uint8_t)(0); 
	CAN_LOG_DATA[91] = (uint8_t)(0); 	  
	  #endif
	  
	CAN_LOG_DATA[92] = (uint8_t)(lcesps16ESCCycleCount); 
	CAN_LOG_DATA[93] = (uint8_t)(lcesps16ESCCycleCount>>8);	

	CAN_LOG_DATA[94] = (uint8_t)(yaw_error_under_dot); 
	CAN_LOG_DATA[95] = (uint8_t)(yaw_error_under_dot>>8);
	
	CAN_LOG_DATA[96] = (uint8_t)(FL.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[97] = (uint8_t)(FL.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[98] = (uint8_t)(FR.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[99] = (uint8_t)(FR.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[100] = (uint8_t)(RL.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[101] = (uint8_t)(RL.s16_Estimated_Active_Press>>8);	

	CAN_LOG_DATA[102] = (uint8_t)(RR.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[103] = (uint8_t)(RR.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[104] = (uint8_t)(FL_ESC.slip_m); 
	CAN_LOG_DATA[105] = (uint8_t)(FL_ESC.slip_m>>8); 	
	
	CAN_LOG_DATA[106] = (uint8_t)(FR_ESC.slip_m); 
	CAN_LOG_DATA[107] = (uint8_t)(FR_ESC.slip_m>>8);	 
	
	CAN_LOG_DATA[108] = (uint8_t)(RL_ESC.slip_m); 
	CAN_LOG_DATA[109] = (uint8_t)(RL_ESC.slip_m>>8);
	
	CAN_LOG_DATA[110] = (uint8_t)(RR_ESC.slip_m);   
	CAN_LOG_DATA[111] = (uint8_t)(RR_ESC.slip_m>>8);
	
	CAN_LOG_DATA[112] = (uint8_t)(yaw_control_P_gain_front); 
	CAN_LOG_DATA[113] = (uint8_t)(yaw_control_P_gain_front>>8);	 
	
	CAN_LOG_DATA[114] = (uint8_t)(yaw_control_D_gain_front); 
	CAN_LOG_DATA[115] = (uint8_t)(yaw_control_D_gain_front>>8);
		
	CAN_LOG_DATA[116] = (uint8_t)(yaw_control_P_gain_rear); 
	CAN_LOG_DATA[117] = (uint8_t)(yaw_control_P_gain_rear>>8); 	
	
	CAN_LOG_DATA[118] = (uint8_t)(yaw_control_D_gain_rear); 
	CAN_LOG_DATA[119] = (uint8_t)(yaw_control_D_gain_rear>>8); 	
	
	CAN_LOG_DATA[120] = (uint8_t)(del_target_2wheel_slip); 
	CAN_LOG_DATA[121] = (uint8_t)(del_target_2wheel_slip>>8);	 
	
	CAN_LOG_DATA[122] = (uint8_t)(lcesps16Over2wheelEnterMoment); 
	CAN_LOG_DATA[123] = (uint8_t)(lcesps16Over2wheelEnterMoment>>8);	 
	
	CAN_LOG_DATA[124] = (uint8_t)(lcesps16Over2wheelExitMoment); 
	CAN_LOG_DATA[125] = (uint8_t)(lcesps16Over2wheelExitMoment>>8);	 
	
	CAN_LOG_DATA[126] = (uint8_t)(lcs16OVER2WH_count); 
	CAN_LOG_DATA[127] = (uint8_t)(lcs16OVER2WH_count>>8);	
	
	CAN_LOG_DATA[128] = (uint8_t)(lcs16OVER2WH_FadeCount); 
	CAN_LOG_DATA[129] = (uint8_t)(lcs16OVER2WH_FadeCount>>8); 	
	
	CAN_LOG_DATA[130] = (uint8_t)(lcesps16OVER2WH_CompCount); 
	CAN_LOG_DATA[131] = (uint8_t)(lcesps16OVER2WH_CompCount>>8); 	
	
	CAN_LOG_DATA[132] = (uint8_t)(lcesps16OS2TarVol); 
	CAN_LOG_DATA[133] = (uint8_t)(lcesps16OS2TarVol>>8); 	
	
	CAN_LOG_DATA[134] = (uint8_t)(0); 
	CAN_LOG_DATA[135] = (uint8_t)(0);
		
	CAN_LOG_DATA[136] = (uint8_t)(0); 
	CAN_LOG_DATA[137] = (uint8_t)(0);	 
	
	CAN_LOG_DATA[138] = (uint8_t)(MFC_PWM_DUTY_P/10); 
	CAN_LOG_DATA[139] = (uint8_t)(MFC_PWM_DUTY_S/10); 
	
	CAN_LOG_DATA[140] = (uint8_t)(Bank_Angle_Final);
	CAN_LOG_DATA[141] = (uint8_t)(Bank_Angle_Final>>8);
	
	CAN_LOG_DATA[142] = (uint8_t)(ab_rfm); 
	CAN_LOG_DATA[143] = (uint8_t)(ab_rfm>>8); 	
	
	CAN_LOG_DATA[144] = (uint8_t)(ldesps16MulevelOverVxThreshold); 
	CAN_LOG_DATA[145] = (uint8_t)(ldesps16MulevelOverVxThreshold>>8); 
	
	CAN_LOG_DATA[146] = (uint8_t)(yaw_limit_enter); 
	CAN_LOG_DATA[147] = (uint8_t)(yaw_limit_enter>>8); 
	
	CAN_LOG_DATA[148] = (uint8_t)(yaw_out_dot3); 
	CAN_LOG_DATA[149] = (uint8_t)(yaw_out_dot3>>8); 
	
	CAN_LOG_DATA[150] = (uint8_t)(del_target_slip_fl); 
	CAN_LOG_DATA[151] = (uint8_t)(del_target_slip_fl>>8); 
	
	CAN_LOG_DATA[152] = (uint8_t)(del_target_slip_fr); 
	CAN_LOG_DATA[153] = (uint8_t)(del_target_slip_fr>>8);
	
	  #if (__TVBB == ENABLE)
	if (MFC_Current_TVBB_P>0)
	{
		esp_tempW0 = MFC_Current_TVBB_P/10;
	}
	else if (MFC_Current_TVBB_S>0)
	{
		esp_tempW0 = MFC_Current_TVBB_S/10;
	}
	else
	{
		esp_tempW0 = 0;
	}
	  #else
	esp_tempW0 = 0;
	  #endif	  
	CAN_LOG_DATA[154] = (uint8_t)(esp_tempW0);
	
	tempB3 = tempB2 = tempB1 = tempB0 = 0;
	
	if(ROP_REQ_FRT_Pre				)	tempB3|=0x01;			
    if(lcu1OVER2WHControlFlag_RL	)	tempB3|=0x02;			
    if(lcu1OVER2WHControlFlag_RR	)	tempB3|=0x04;			
    if(lcu1OVER2WHFadeout_RL		)	tempB3|=0x08;			
    if(lcu1OVER2WHFadeout_RR		)	tempB3|=0x10;			
    if(ldespu1ESC1stCycle			)	tempB3|=0x20;			
    if(ldespu1ESC2ndCycle			)	tempB3|=0x40;			
    if(lcu1ABSCBSCombCntrl_FL||lcu1ABSCBSCombCntrl_FR||lcu1ABSCBSCombCntrl_RL||lcu1ABSCBSCombCntrl_RR	)	tempB3|=0x80;

  #if __TVBB	
	if(lctvbbu1TVBB_ON				)	tempB2|=0x01;
  #else
	if(0				            )	tempB2|=0x01;
  #endif  
      			
    if(FLAG_TWO_WHEEL				)	tempB2|=0x02;			
    if(FLAG_TWO_WHEEL_RL			)	tempB2|=0x04;			
    if(FLAG_TWO_WHEEL_RR			)	tempB2|=0x08;
      #if ((__ESC_TCMF_CONTROL == ENABLE)||(__VALVE_TEST_TCMF == ENABLE)) 			
    if(FL_ESC.lcespu1TCMFControl||FR_ESC.lcespu1TCMFControl			)	tempB2|=0x10;			
    if(FL_ESC.lcespu1TCMFCon_IniFull||FR_ESC.lcespu1TCMFCon_IniFull	)	tempB2|=0x20;			
    if(FL_ESC.lcespu1TCMFCon_Rise||FR_ESC.lcespu1TCMFCon_Rise		)	tempB2|=0x40;			
    if(FL_ESC.lcespu1TCMFCon_Hold||FR_ESC.lcespu1TCMFCon_Hold		)	tempB2|=0x80;
    
    if(FL_ESC.lcespu1TCMFCon_Dump||FR_ESC.lcespu1TCMFCon_Dump				)	tempB1|=0x01;			
    if(FL_ESC.lcespu1TCMFConOld_IniFull||FR_ESC.lcespu1TCMFConOld_IniFull	)	tempB1|=0x02;			
    if(FL_ESC.lcespu1TCMFConOld_Rise||FR_ESC.lcespu1TCMFConOld_Rise			)	tempB1|=0x04;			
    if(FL_ESC.lcespu1TCMFConOld_Hold||FR_ESC.lcespu1TCMFConOld_Hold			)	tempB1|=0x08;			
    if(FL_ESC.lcespu1TCMFConOld_Dump||FR_ESC.lcespu1TCMFConOld_Dump			)	tempB1|=0x10;
      #else
    if(0)	tempB2|=0x10;			
	if(0)	tempB2|=0x20;			
	if(0)	tempB2|=0x40;			
	if(0)	tempB2|=0x80;        
	                                                                                         
	if(0)	tempB1|=0x01;
	if(0)	tempB1|=0x02;
	if(0)	tempB1|=0x04;
	if(0)	tempB1|=0x08;
	if(0)	tempB1|=0x10;
	  #endif
	  
	  #if (__VSM == ENABLE)		
    if(lcepsbBrakingCtrlFlag		)	tempB1|=0x20;			
    if(lcepsbHandlingCtrlFlag		)	tempB1|=0x40;
      #else
    if(0							)	tempB1|=0x20;			
    if(0							)	tempB1|=0x40;
      #endif  			
    if(fu1TCSDisabledBySW			)	tempB1|=0x80;
	                                 	
	if(lcescu1TCMFUnderControlRl	)	tempB0|=0x01;			
    if(lcescu1TCMFUnderControlRr	)	tempB0|=0x02;			
    if(lcu1US2WHControlFlag_RL		)	tempB0|=0x04;			
    if(lcu1US2WHControlFlag_RR		)	tempB0|=0x08;			
    if(lcu1US3WHControlFlag			)	tempB0|=0x10;			
    if(lcu1US2WHFadeout_RL			)	tempB0|=0x20;			
    if(lcu1US2WHFadeout_RR			)	tempB0|=0x40;			
    if(YAW_CHANGE_LIMIT							)	tempB0|=0x80;
    
	CAN_LOG_DATA[155] = (uint8_t)(tempB3); 
	CAN_LOG_DATA[156] = (uint8_t)(tempB2); 
	CAN_LOG_DATA[157] = (uint8_t)(tempB1); 
	CAN_LOG_DATA[158] = (uint8_t)(tempB0); 
	CAN_LOG_DATA[159] = (uint8_t)(fu16CalVoltIntMOTOR/100);
#endif
*/
}

#if(__ESC_TCMF_CONTROL ==1)
/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallESCTCMFLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          TCMF ESC
********************************************************************************/

void LSESP_vCallESCTCMFLogData(void)
{
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);   									//ch1
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
                   																			
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                					//ch2
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
                               																			
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                					//ch3
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
                               																			
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                					//ch4
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
                              																			
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                					//ch5
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
                             																			
    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);                         					//ch6
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);
    
#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
if(esp_tempW9 > 120) esp_tempW9 =120;
else if(esp_tempW9< (-120)) esp_tempW9 = -120;

    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);   //ch7
    
    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());     			//ch8
    
    CAN_LOG_DATA[14] =(uint8_t) (vref5);                              						//ch9
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

esp_tempW6= esp_mu/10;

if(esp_tempW6 > 120) esp_tempW6 =120;
else if(esp_tempW6< (-120)) esp_tempW6 = -120;

    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6); 										//ch10
    
	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);     			//ch11

    CAN_LOG_DATA[18] =(uint8_t) (target_vol);                 								//ch12
    CAN_LOG_DATA[19] =(uint8_t) (target_vol>>8);              																				
                          
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  	  esp_tempW3 = FR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}

    CAN_LOG_DATA[20] =(uint8_t)(esp_tempW3);              											//ch13
    CAN_LOG_DATA[21] =(uint8_t)(esp_tempW3>>8);
    CAN_LOG_DATA[22] =(uint8_t)	(yaw_out_dot2);												//ch14
    CAN_LOG_DATA[23] =(uint8_t)	(yaw_out_dot2>>8);
    
    if(SLIP_CONTROL_NEED_FL)
    {
    	esp_tempW0 = FL_ESC.del_slip_gain;
    	esp_tempW1 = FL_ESC.del_wheel_sp_gain; 
    }
    else if(SLIP_CONTROL_NEED_FR == 1)
    {
    	esp_tempW0 = FR_ESC.del_slip_gain;
    	esp_tempW1 = FR_ESC.del_wheel_sp_gain; 
    }
    else if(SLIP_CONTROL_NEED_RL == 1)
    {
     	esp_tempW0 = RL_ESC.del_slip_gain;
    	esp_tempW1 = RL_ESC.del_wheel_sp_gain;    	
    }
    else if(SLIP_CONTROL_NEED_RR == 1)
    {
     	esp_tempW0 = RR_ESC.del_slip_gain;
    	esp_tempW1 = RR_ESC.del_wheel_sp_gain;    	
    }
    else if(delta_yaw < 0 )
    {
    	esp_tempW0 = C1_rear;
    	esp_tempW1 = C2_rear;    	
    }     
    else
    {
    	esp_tempW0 = C1_front;
    	esp_tempW1 = C2_front;
    }

    CAN_LOG_DATA[24] =(uint8_t) (esp_tempW0);                         						//ch15
    CAN_LOG_DATA[25] =(uint8_t) (esp_tempW0>>8);

    CAN_LOG_DATA[26] =(uint8_t) (esp_tempW1);                         						//ch16
    CAN_LOG_DATA[27] =(uint8_t) (esp_tempW1>>8);

    CAN_LOG_DATA[28] =(uint8_t) (alat);                               						//ch17
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);  
    
    
	if(SLIP_CONTROL_NEED_FL==1)
	{
	    esp_tempW2 = del_target_slip_fl;
	}
	else if(SLIP_CONTROL_NEED_FR==1)
	{
	    esp_tempW2 = del_target_slip_fr;
	}
	else
	{
	    esp_tempW2 = 0;
	}

    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW2);                             					//ch18
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW2>>8); 
        
    CAN_LOG_DATA[32] =(uint8_t) (FL_ESC.MFC_Current_Inter);                 				//ch19         
    CAN_LOG_DATA[33] =(uint8_t) (FL_ESC.MFC_Current_Inter>>8);
    
    CAN_LOG_DATA[34] =(uint8_t) (FR_ESC.MFC_Current_Inter);                 				//ch20
    CAN_LOG_DATA[35] =(uint8_t) (FR_ESC.MFC_Current_Inter>>8); 
     
    CAN_LOG_DATA[36] =(uint8_t) (eng_torq);                               					//ch21
    CAN_LOG_DATA[37] =(uint8_t) (eng_torq>>8);
    
    if (AUTO_TM	== 1)
    {
        tempW0 = (INT)gear_pos;
    }
    else
    {
        tempW0 = (INT)gear_state;
    }
    
    CAN_LOG_DATA[38] =(uint8_t) (tempW0);                               					//ch22
    
	CAN_LOG_DATA[39] =(uint8_t) (mtp);                                    					//ch23

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               						//ch24
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                					//ch25
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                  					//ch26
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
   	
   	if(FL_ESC.lcespu1TCMFControl == 1)
    {
    	esp_tempW3 = FL_ESC.lcesps16TCMFCon_STEP_Reg;
    }
    else if(FR_ESC.lcespu1TCMFControl == 1)
    {
    	esp_tempW3 = FR_ESC.lcesps16TCMFCon_STEP_Reg;
    }
    else
    {
    	esp_tempW3 = 0;
    }
   	
    CAN_LOG_DATA[46] =(uint8_t) (esp_tempW3);					                			//ch27
    CAN_LOG_DATA[47] =(uint8_t) (esp_tempW3>>8);
   
    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 					//ch28
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
    
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                       					//ch29
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
    
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                 					//ch30
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);
    
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);        									//ch31
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);

    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                         					//ch32
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);
    
    //CAN_LOG_DATA[58] =(uint8_t) (cal_torq);                 					    		//ch33
    //CAN_LOG_DATA[59] =(uint8_t) (cal_torq>>8);
    
    CAN_LOG_DATA[58] =(uint8_t) (Beta_MD);                 					    			//ch33
    CAN_LOG_DATA[59] =(uint8_t) (Beta_MD>>8);
    
    //if(FL_ESC.lcespu1TCMFControl == 1)
    //{
    //	esp_tempW0 = FL_ESC.lcesps16StepIntDutyPribyCom;
    //	esp_tempW1 = FL_ESC.lcesps16StepIntDutySecbyCom;
    //}
    //else if(FR_ESC.lcespu1TCMFControl == 1)
    //{
    //	esp_tempW0 = FR_ESC.lcesps16StepIntDutyPribyCom;
    //	esp_tempW1 = FR_ESC.lcesps16StepIntDutySecbyCom;
    //}
    //else
    //{
    //	;
    //}
    //
    //CAN_LOG_DATA[56] =(uint8_t) (esp_tempW0);                         						ch32
    //CAN_LOG_DATA[57] =(uint8_t) (esp_tempW0>>8);
    //
    //CAN_LOG_DATA[58] =(uint8_t) (esp_tempW1);                 								ch33
    //CAN_LOG_DATA[59] =(uint8_t) (esp_tempW1>>8);
   
    if(FL_ESC.lcespu1TCMFControl == 1)
    {
    	esp_tempW5 = FL_ESC.lcesps16StepIntDutybyComVol;
    }
    else if(FR_ESC.lcespu1TCMFControl == 1)
    {
    	esp_tempW5 = FR_ESC.lcesps16StepIntDutybyComVol;
    }
    else
    {
    	esp_tempW5 = 0;
    }
    
    CAN_LOG_DATA[60] =(uint8_t) (esp_tempW5);   					                		//ch34
    CAN_LOG_DATA[61] =(uint8_t) (esp_tempW5>>8);

  	if(SLIP_CONTROL_NEED_RL==1)
  	{
  		esp_tempW8 = RL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  		esp_tempW8 = RR_ESC.slip_control;
  	}
  	else
  	{
  	 	esp_tempW8 = 0;
  	}

    CAN_LOG_DATA[62] =(uint8_t) (esp_tempW8);					                 			//ch35
    CAN_LOG_DATA[63] =(uint8_t) (esp_tempW8>>8);
    
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                              						//ch36
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;			                 				//flag1
    if(FL_ESC.lcespu1TCMFControl)   tempB5|=0x02;											//flag2_TCMF
    if(ABS_fz)                      tempB5|=0x04;											//flag3
    if(EBD_RA)                      tempB5|=0x08;											//flag4
    if(BTC_fz==1)                   tempB5|=0x10;											//flag5
    if(ETCS_ON)                     tempB5|=0x20;											//flag6
    if(YAW_CDC_WORK)                tempB5|=0x40;											//flag7
    if(ESP_TCS_ON)                  tempB5|=0x80;											//flag8
    
    if(FR_ESC.lcespu1TCMFControl)   tempB4|=0x01;											//flag9_TCMF	
	if(FL_ESC.lcespu1TCMFCon_IniFull||FR_ESC.lcespu1TCMFCon_IniFull )     tempB4|=0x02;		//flag10_TCMF
    if(PARTIAL_BRAKE)               tempB4|=0x04;											//flag11
    if(FL_ESC.lcespu1TCMFCon_Rise||FR_ESC.lcespu1TCMFCon_Rise)            tempB4|=0x08;		//flag12_TCMF
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;											//flag13
    if(FL_ESC.lcespu1TCMFCon_Hold||FR_ESC.lcespu1TCMFCon_Hold)     	      tempB4|=0x20;		//flag14_TCMF
	if(FL_ESC.lcespu1TCMFCon_Dump||FR_ESC.lcespu1TCMFCon_Dump)            tempB4|=0x40;		//flag15_TCMF
    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;	//flag16

    if(AV_VL_fl)                    tempB3|=0x01;											//flag17
    if(HV_VL_fl)                    tempB3|=0x02;											//flag18
    if(ABS_fl)                      tempB3|=0x04;											//flag19
    if(FL_ESC.lcespu1TCMFConSTEP_RegTrans||FR_ESC.lcespu1TCMFConSTEP_RegTrans) tempB3|=0x08;	//flag20_TCMF
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fl)               tempB3|=0x10;											//flag21
#else
    if(TCL_DEMAND_SECONDARY)        tempB3|=0x10;											//flag21
#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;											//flag22
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)             tempB3|=0x40;				//flag23
    if(FL_ESC.lcespu1TCMFConCBSCombFlag||FR_ESC.lcespu1TCMFConCBSCombFlag)   tempB3|=0x80;	//flag24_TCMF

    if(AV_VL_fr)                    tempB2|=0x01;											//flag25
    if(HV_VL_fr)                    tempB2|=0x02;											//flag26
    if(ABS_fr)                      tempB2|=0x04;											//flag27
    if(BTCS_fr)                     tempB2|=0x08;											//flag28
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fr)               tempB2|=0x10;											//flag29
#else
    if(TCL_DEMAND_PRIMARY)          tempB2|=0x10;											//flag29
#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;											//flag30
    if(FL_ESC.lcespu1TCMFCon_Dump_SLOW||FR_ESC.lcespu1TCMFCon_Dump_SLOW)    tempB2|=0x40;	//flag31_TCMF
#if __BANK_ESC_IMPROVEMENT_CONCEPT
    if(ldespu1BankLowerSuspectFlag) tempB2|=0x80;											//flag32
#else    
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;											//flag32
#endif

    if(AV_VL_rl)                    tempB1|=0x01;											//flag33
    if(HV_VL_rl)                    tempB1|=0x02;											//flag34
    if(ABS_rl)                      tempB1|=0x04;											//flag35
    if(BTCS_fl||BTCS_rl) 			tempB1|=0x08;											//flag36
    if(EBD_rl)                      tempB1|=0x10;											//flag37
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;											//flag38
#if !__SPLIT_TYPE
    if(S_VALVE_LEFT)                tempB1|=0x40;											//flag39
#else
    if(S_VALVE_SECONDARY)           tempB1|=0x40;											//flag39
#endif
    if(FL_ESC.FULL_RISE_MODE||FR_ESC.FULL_RISE_MODE||RL_ESC.FULL_RISE_MODE||RR_ESC.FULL_RISE_MODE)          tempB1|=0x80;		//flag40


    if(AV_VL_rr)            		tempB0|=0x01;									//flag41
    if(HV_VL_rr)            		tempB0|=0x02;									//flag42
    if(ABS_rr)              		tempB0|=0x04;									//flag43
    if(BTCS_fr||BTCS_rr)    		tempB0|=0x08;									//flag44
    if(EBD_rr)              		tempB0|=0x10;									//flag45
    if(ESP_BRAKE_CONTROL_RR)  		tempB0|=0x20;									//flag46
#if !__SPLIT_TYPE
    if(S_VALVE_RIGHT)       		tempB0|=0x40;									//flag47
#else
    if(S_VALVE_PRIMARY)     		tempB0|=0x40;									//flag47
#endif
    if(FL_ESC.lcespu1TCMFCon_Dump_FAST||FR_ESC.lcespu1TCMFCon_Dump_FAST)    tempB0|=0x80;  //flag48_TCMF

    CAN_LOG_DATA[66] =(uint8_t) (tempB5);		//ch40
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);		//ch41
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);       //ch42
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);       //ch43
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);       //ch44
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);       //ch45

if(SLIP_CONTROL_NEED_RL==1)
{
    esp_tempW6 = del_target_slip_rl;
}
else if(SLIP_CONTROL_NEED_RR==1)
{
    esp_tempW6 = del_target_slip_rr;
}
else
{
    esp_tempW6 = 0;
}
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);								//ch43						
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6>>8);

    CAN_LOG_DATA[74] =(uint8_t) (MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[75] =(uint8_t) (MFC_Current_ESP_S/10);			//ch45


	if(SLIP_CONTROL_NEED_FL==1)    
	{
  	 	CAN_LOG_DATA[76] =(uint8_t) (laesc_FL1HP.u8PwmDuty);	//ch46
	}
	else  
	{    
		CAN_LOG_DATA[76] =(uint8_t) (FL.pwm_duty_temp);			//ch46
	}
	
	if(SLIP_CONTROL_NEED_FR==1)    
	{
  	 	CAN_LOG_DATA[77] =(uint8_t) (laesc_FR1HP.u8PwmDuty);	//ch47
	}
	else  
	{    
		CAN_LOG_DATA[77] =(uint8_t) (FR.pwm_duty_temp);			//ch47
	}
	
	if(SLIP_CONTROL_NEED_RL==1)    
	{
  	 	CAN_LOG_DATA[78] =(uint8_t) (laesc_RL1HP.u8PwmDuty);	//ch48
	}
	else  
	{    
		CAN_LOG_DATA[78] =(uint8_t) (RL.pwm_duty_temp);			//ch48
	}
	
	if(SLIP_CONTROL_NEED_RR==1)    
	{
  	 	CAN_LOG_DATA[79] =(uint8_t) (laesc_RR1HP.u8PwmDuty);	//ch49
	}
	else  
	{    
		CAN_LOG_DATA[79] =(uint8_t) (RR.pwm_duty_temp);			//ch49
	}		

#if defined(__EXTEND_LOGGER)
		
	CAN_LOG_DATA[80] = (uint8_t)(alat_dot_lf);
	CAN_LOG_DATA[81] = (uint8_t)(alat_dot_lf>>8);
	 
	CAN_LOG_DATA[82] = (uint8_t)(det_wstr_dot_lf); 
	CAN_LOG_DATA[83] = (uint8_t)(det_wstr_dot_lf>>8);	 
	
	CAN_LOG_DATA[84] = (uint8_t)(roll_angle); 
	CAN_LOG_DATA[85] = (uint8_t)(roll_angle>>8); 	
	
	CAN_LOG_DATA[86] = (uint8_t)(roll_angle_rate); 
	CAN_LOG_DATA[87] = (uint8_t)(roll_angle_rate>>8); 
	
	CAN_LOG_DATA[88] = (uint8_t)(RI_max);	 
	CAN_LOG_DATA[89] = (uint8_t)(TTL_time);
	
	  #if (__ROP == ENABLE) 	
	CAN_LOG_DATA[90] = (uint8_t)(moment_pre_front_rop/100);
	CAN_LOG_DATA[91] = (uint8_t)(moment_q_front_rop/100); 	
	  #else
	CAN_LOG_DATA[90] = (uint8_t)(0); 
	CAN_LOG_DATA[91] = (uint8_t)(0); 	  
	  #endif
	  
	CAN_LOG_DATA[92] = (uint8_t)(lcesps16ESCCycleCount); 
	CAN_LOG_DATA[93] = (uint8_t)(lcesps16ESCCycleCount>>8);	

	CAN_LOG_DATA[94] = (uint8_t)(yaw_error_under_dot); 
	CAN_LOG_DATA[95] = (uint8_t)(yaw_error_under_dot>>8);
	
	CAN_LOG_DATA[96] = (uint8_t)(FL.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[97] = (uint8_t)(FL.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[98] = (uint8_t)(FR.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[99] = (uint8_t)(FR.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[100] = (uint8_t)(RL.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[101] = (uint8_t)(RL.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[102] = (uint8_t)(RR.s16_Estimated_Active_Press); 
	CAN_LOG_DATA[103] = (uint8_t)(RR.s16_Estimated_Active_Press>>8);	
	
	CAN_LOG_DATA[104] = (uint8_t)(FL_ESC.slip_m); 
	CAN_LOG_DATA[105] = (uint8_t)(FL_ESC.slip_m>>8); 	
	
	CAN_LOG_DATA[106] = (uint8_t)(FR_ESC.slip_m); 
	CAN_LOG_DATA[107] = (uint8_t)(FR_ESC.slip_m>>8);	 
	
	CAN_LOG_DATA[108] = (uint8_t)(RL_ESC.slip_m); 
	CAN_LOG_DATA[109] = (uint8_t)(RL_ESC.slip_m>>8);
	
	CAN_LOG_DATA[110] = (uint8_t)(RR_ESC.slip_m);   
	CAN_LOG_DATA[111] = (uint8_t)(RR_ESC.slip_m>>8);
	
	CAN_LOG_DATA[112] = (uint8_t)(yaw_control_P_gain_front); 
	CAN_LOG_DATA[113] = (uint8_t)(yaw_control_P_gain_front>>8);	 
	
	CAN_LOG_DATA[114] = (uint8_t)(yaw_control_D_gain_front); 
	CAN_LOG_DATA[115] = (uint8_t)(yaw_control_D_gain_front>>8);
		
	CAN_LOG_DATA[116] = (uint8_t)(yaw_control_P_gain_rear); 
	CAN_LOG_DATA[117] = (uint8_t)(yaw_control_P_gain_rear>>8); 	
	
	CAN_LOG_DATA[118] = (uint8_t)(yaw_control_D_gain_rear); 
	CAN_LOG_DATA[119] = (uint8_t)(yaw_control_D_gain_rear>>8); 	
	
	CAN_LOG_DATA[120] = (uint8_t)(del_target_2wheel_slip); 
	CAN_LOG_DATA[121] = (uint8_t)(del_target_2wheel_slip>>8);	 
	
	CAN_LOG_DATA[122] = (uint8_t)(lcesps16Over2wheelEnterMoment); 
	CAN_LOG_DATA[123] = (uint8_t)(lcesps16Over2wheelEnterMoment>>8);	 
	
	CAN_LOG_DATA[124] = (uint8_t)(lcesps16Over2wheelExitMoment); 
	CAN_LOG_DATA[125] = (uint8_t)(lcesps16Over2wheelExitMoment>>8);	 
	
	CAN_LOG_DATA[126] = (uint8_t)(lcs16OVER2WH_count); 
	CAN_LOG_DATA[127] = (uint8_t)(lcs16OVER2WH_count>>8);	
	
	CAN_LOG_DATA[128] = (uint8_t)(lcs16OVER2WH_FadeCount); 
	CAN_LOG_DATA[129] = (uint8_t)(lcs16OVER2WH_FadeCount>>8); 	
	
	CAN_LOG_DATA[130] = (uint8_t)(lcesps16OVER2WH_CompCount); 
	CAN_LOG_DATA[131] = (uint8_t)(lcesps16OVER2WH_CompCount>>8); 	
	
	CAN_LOG_DATA[132] = (uint8_t)(lcesps16OS2TarVol); 
	CAN_LOG_DATA[133] = (uint8_t)(lcesps16OS2TarVol>>8); 	
	
	//CAN_LOG_DATA[134] = (uint8_t)(lcesps16TCMFRiseCnt); 
	//CAN_LOG_DATA[135] = (uint8_t)(lcesps16TCMFRiseCnt>>8);
		
	//CAN_LOG_DATA[136] = (uint8_t)(lcesps16TCMFRiseIniCur); 
	//CAN_LOG_DATA[137] = (uint8_t)(lcesps16TCMFRiseIniCur>>8);	 
	
	CAN_LOG_DATA[138] = (uint8_t)(MFC_PWM_DUTY_P/10); 
	CAN_LOG_DATA[139] = (uint8_t)(MFC_PWM_DUTY_S/10); 
	
	CAN_LOG_DATA[140] = (uint8_t)(Bank_Angle_Final);
	CAN_LOG_DATA[141] = (uint8_t)(Bank_Angle_Final>>8);
	
	CAN_LOG_DATA[142] = (uint8_t)(ab_rfm); 
	CAN_LOG_DATA[143] = (uint8_t)(ab_rfm>>8); 	
	
	CAN_LOG_DATA[144] = (uint8_t)(ldesps16MulevelOverVxThreshold); 
	CAN_LOG_DATA[145] = (uint8_t)(ldesps16MulevelOverVxThreshold>>8); 
	
	CAN_LOG_DATA[146] = (uint8_t)(yaw_limit_enter); 
	CAN_LOG_DATA[147] = (uint8_t)(yaw_limit_enter>>8); 
	
	CAN_LOG_DATA[148] = (uint8_t)(yaw_out_dot3); 
	CAN_LOG_DATA[149] = (uint8_t)(yaw_out_dot3>>8); 
	
	CAN_LOG_DATA[150] = (uint8_t)(del_target_slip_fl); 
	CAN_LOG_DATA[151] = (uint8_t)(del_target_slip_fl>>8); 
	
	CAN_LOG_DATA[152] = (uint8_t)(del_target_slip_fr); 
	CAN_LOG_DATA[153] = (uint8_t)(del_target_slip_fr>>8);
	
	  #if (__TVBB == ENABLE)
	if (MFC_Current_TVBB_P>0)
	{
		esp_tempW0 = MFC_Current_TVBB_P/10;
	}
	else if (MFC_Current_TVBB_S>0)
	{
		esp_tempW0 = MFC_Current_TVBB_S/10;
	}
	else
	{
		esp_tempW0 = 0;
	}
	  #else
	esp_tempW0 = 0;
	  #endif	  
	CAN_LOG_DATA[154] = (uint8_t)(esp_tempW0);
	
	tempB3 = tempB2 = tempB1 = tempB0 = 0;
	
	if(ROP_REQ_FRT_Pre				)	tempB3|=0x01;			
    if(lcu1OVER2WHControlFlag_RL	)	tempB3|=0x02;			
    if(lcu1OVER2WHControlFlag_RR	)	tempB3|=0x04;			
    if(lcu1OVER2WHFadeout_RL		)	tempB3|=0x08;			
    if(lcu1OVER2WHFadeout_RR		)	tempB3|=0x10;			
    if(ldespu1ESC1stCycle			)	tempB3|=0x20;			
    if(ldespu1ESC2ndCycle			)	tempB3|=0x40;			
    if(lcu1ABSCBSCombCntrl_FL||lcu1ABSCBSCombCntrl_FR||lcu1ABSCBSCombCntrl_RL||lcu1ABSCBSCombCntrl_RR	)	tempB3|=0x80;

  #if __TVBB	
	if(lctvbbu1TVBB_ON				)	tempB2|=0x01;
  #else
	if(0				            )	tempB2|=0x01;
  #endif  
      			
    if(FLAG_TWO_WHEEL				)	tempB2|=0x02;			
    if(FLAG_TWO_WHEEL_RL			)	tempB2|=0x04;			
    if(FLAG_TWO_WHEEL_RR			)	tempB2|=0x08;
      #if ((__ESC_TCMF_CONTROL == ENABLE)||(__VALVE_TEST_TCMF == ENABLE)) 			
    if(FL_ESC.lcespu1TCMFControl||FR_ESC.lcespu1TCMFControl			)	tempB2|=0x10;			
    if(FL_ESC.lcespu1TCMFCon_IniFull||FR_ESC.lcespu1TCMFCon_IniFull	)	tempB2|=0x20;			
    if(FL_ESC.lcespu1TCMFCon_Rise||FR_ESC.lcespu1TCMFCon_Rise		)	tempB2|=0x40;			
    if(FL_ESC.lcespu1TCMFCon_Hold||FR_ESC.lcespu1TCMFCon_Hold		)	tempB2|=0x80;
    
    if(FL_ESC.lcespu1TCMFCon_Dump||FR_ESC.lcespu1TCMFCon_Dump				)	tempB1|=0x01;			
    if(FL_ESC.lcespu1TCMFConOld_IniFull||FR_ESC.lcespu1TCMFConOld_IniFull	)	tempB1|=0x02;			
    if(FL_ESC.lcespu1TCMFConOld_Rise||FR_ESC.lcespu1TCMFConOld_Rise			)	tempB1|=0x04;			
    if(FL_ESC.lcespu1TCMFConOld_Hold||FR_ESC.lcespu1TCMFConOld_Hold			)	tempB1|=0x08;			
    if(FL_ESC.lcespu1TCMFConOld_Dump||FR_ESC.lcespu1TCMFConOld_Dump			)	tempB1|=0x10;
      #else
    if(0)	tempB2|=0x10;			
	if(0)	tempB2|=0x20;			
	if(0)	tempB2|=0x40;			
	if(0)	tempB2|=0x80;        
	                                                                                         
	if(0)	tempB1|=0x01;
	if(0)	tempB1|=0x02;
	if(0)	tempB1|=0x04;
	if(0)	tempB1|=0x08;
	if(0)	tempB1|=0x10;
	  #endif
	  
	  #if (__VSM == ENABLE)		
    if(lcepsbBrakingCtrlFlag		)	tempB1|=0x20;			
    if(lcepsbHandlingCtrlFlag		)	tempB1|=0x40;
      #else
    if(0							)	tempB1|=0x20;			
    if(0							)	tempB1|=0x40;
      #endif  			
    if(fu1TCSDisabledBySW			)	tempB1|=0x80;
	                                 	
	if(lcescu1TCMFUnderControlRl	)	tempB0|=0x01;			
    if(lcescu1TCMFUnderControlRr	)	tempB0|=0x02;			
    if(lcu1US2WHControlFlag_RL		)	tempB0|=0x04;			
    if(lcu1US2WHControlFlag_RR		)	tempB0|=0x08;			
    if(lcu1US3WHControlFlag			)	tempB0|=0x10;			
    if(lcu1US2WHFadeout_RL			)	tempB0|=0x20;			
    if(lcu1US2WHFadeout_RR			)	tempB0|=0x40;			
    if(YAW_CHANGE_LIMIT							)	tempB0|=0x80;
    
	CAN_LOG_DATA[155] = (uint8_t)(tempB3); 
	CAN_LOG_DATA[156] = (uint8_t)(tempB2); 
	CAN_LOG_DATA[157] = (uint8_t)(tempB1); 
	CAN_LOG_DATA[158] = (uint8_t)(tempB0); 
	CAN_LOG_DATA[159] = (uint8_t)(fu16CalVoltIntMOTOR/100); 
				

	#endif
}

#endif

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallEspApplLog
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of ESP
********************************************************************************/
void LSESP_vCallEspApplLog(void)
{
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);                    //ch1
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                //ch2
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                //ch3
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                //ch4
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                //ch5
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);                         //ch6
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);
#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
if(esp_tempW9 > 120) esp_tempW9 =120;
else if(esp_tempW9< (-120)) esp_tempW9 = -120;

    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);   //ch7
    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());//(q_rear<-120)?-120:(int8_t)q_rear;     //ch8
    CAN_LOG_DATA[14] =(uint8_t) (vref5);                              //ch9
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

esp_tempW6= esp_mu/10;

if(esp_tempW6 > 120) esp_tempW6 =120;
else if(esp_tempW6< (-120)) esp_tempW6 = -120;

    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6);//((moment_q_front<-120)?-120:(int8_t)moment_q_front);    //ch10
    
	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);     //ch11

#if __PRESS_EST_ACTIVE
    CAN_LOG_DATA[18] =(uint8_t)(lcs16EscInitialPulseupDutyfl);//(uint8_t) (FL.u8_Estimated_Active_Press);              //ch12
    CAN_LOG_DATA[19] =(uint8_t)(lcs16EscInitialPulseupDutyfr);//(uint8_t) (FR.u8_Estimated_Active_Press);              //ch13
    CAN_LOG_DATA[20] =(uint8_t)(lcs16EscInitialPulseupDutyrl);//(uint8_t) (RL.u8_Estimated_Active_Press);              //ch14                    //ch14
    CAN_LOG_DATA[21] =(uint8_t)(lcs16EscInitialPulseupDutyrr);//(uint8_t) (RR.u8_Estimated_Active_Press);              //ch15
#else
  #if __ESP_DUTY_COMPEN
    CAN_LOG_DATA[18] =(uint8_t) (wheelpress_fl);                      //ch12
    CAN_LOG_DATA[19] =(uint8_t) (wheelpress_fr);            //ch13
    CAN_LOG_DATA[20] =(uint8_t) (wheelpress_rl);                       //ch14
    CAN_LOG_DATA[21] =(uint8_t) (wheelpress_rr);                      //ch15
  #else
    CAN_LOG_DATA[18] =(uint8_t) (esp_pressure_count_fl*10);                       //ch12
    CAN_LOG_DATA[19] =(uint8_t) (esp_pressure_count_fr*10);             //ch13
    CAN_LOG_DATA[20] =(uint8_t) (esp_pressure_count_rl*10);                        //ch14
    CAN_LOG_DATA[21] =(uint8_t) (esp_pressure_count_rr*10);                       //ch15
  #endif
#endif

    CAN_LOG_DATA[22] =(uint8_t)flags_rl;//((int8_t) pd_ymc_gain);//esp_tempW9;//(uint8_t)flags_rl;                         //ch16
    CAN_LOG_DATA[23] =(uint8_t)flags_rr;//((int8_t)q_front_enter_gain);//P_control_activation_rl;//(int8_t)q_front_enter_gain;//(uint8_t)flags_rr; 
                          //ch17
                          
                          
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if      (SLIP_CONTROL_NEED_FL)      esp_tempW0=FL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_FR)      esp_tempW0=FR_ESC.true_slip_target;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL)      esp_tempW1=RL_ESC.true_slip_target;
    else if (SLIP_CONTROL_NEED_RR)      esp_tempW1=RR_ESC.true_slip_target;
    else                                esp_tempW1=0;
#else
    if      (SLIP_CONTROL_NEED_FL)      esp_tempW0=true_slip_target_fl;
    else if (SLIP_CONTROL_NEED_FR)      esp_tempW0=true_slip_target_fr;
    else                                esp_tempW0=0;

    if      (SLIP_CONTROL_NEED_RL)      esp_tempW1=true_slip_target_rl;
    else if (SLIP_CONTROL_NEED_RR)      esp_tempW1=true_slip_target_rr;
    else                                esp_tempW1=0;
#endif
    
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(SLIP_CONTROL_NEED_FL)
    {
    	esp_tempW0 = FL_ESC.del_slip_gain;
    	esp_tempW1 = FL_ESC.del_wheel_sp_gain; 
    }
    else if(SLIP_CONTROL_NEED_FR)
    {
    	esp_tempW0 = FR_ESC.del_slip_gain;
    	esp_tempW1 = FR_ESC.del_wheel_sp_gain; 
    }
    else if(SLIP_CONTROL_NEED_RL)
    {
     	esp_tempW0 = RL_ESC.del_slip_gain;
    	esp_tempW1 = RL_ESC.del_wheel_sp_gain;    	
    }
    else if(SLIP_CONTROL_NEED_RR)
    {
     	esp_tempW0 = RR_ESC.del_slip_gain;
    	esp_tempW1 = RR_ESC.del_wheel_sp_gain;    	
    }
    else if(delta_yaw < 0 )
    {
    	esp_tempW0 = C1_rear;
    	esp_tempW1 = C2_rear;    	
    }     
    else
    {
    	esp_tempW0 = C1_front;
    	esp_tempW1 = C2_front;
    }
#else
    if(SLIP_CONTROL_NEED_FL || SLIP_CONTROL_NEED_FR)
    {
    	esp_tempW0 = del_slip_gain_front;
    	esp_tempW1 = del_wheel_sp_gain_front; 
    }
    else if(SLIP_CONTROL_NEED_RL || SLIP_CONTROL_NEED_RR)
    {
     	esp_tempW0 = del_slip_gain_rear;
    	esp_tempW1 = del_wheel_sp_gain_rear;    	
    }
    else if(delta_yaw < 0 )
    {
    	esp_tempW0 = C1_rear;
    	esp_tempW1 = C2_rear;    	
    }     
    else
    {
    	esp_tempW0 = C1_front;
    	esp_tempW1 = C2_front;
    }
#endif

    CAN_LOG_DATA[24] =(uint8_t) (esp_tempW0);                         //ch18
    CAN_LOG_DATA[25] =(uint8_t) (esp_tempW0>>8);
    CAN_LOG_DATA[26] =(uint8_t) (esp_tempW1);                         //ch19
    CAN_LOG_DATA[27] =(uint8_t) (esp_tempW1>>8);

if(SLIP_CONTROL_NEED_FL==1)
{
    esp_tempW2 = del_target_slip_fl;
}
else if(SLIP_CONTROL_NEED_FR==1)
{
    esp_tempW2 = del_target_slip_fr;
}
else
{
    esp_tempW2 = 0;
}


    CAN_LOG_DATA[28] =(uint8_t) (alat);                                   //ch20
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);  
    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW2);                             //ch21
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW2>>8); 
    CAN_LOG_DATA[32] =(uint8_t) (Beta_MD);                                 //ch22
    CAN_LOG_DATA[33] =(uint8_t) (Beta_MD>>8); 
    CAN_LOG_DATA[34] =(uint8_t) (Alpha_rear);                              //ch23
    CAN_LOG_DATA[35] =(uint8_t) (Alpha_rear>>8); 
    CAN_LOG_DATA[36] =(uint8_t) (eng_torq);                                 //ch24
    CAN_LOG_DATA[37] =(uint8_t) (eng_torq>>8); 

    CAN_LOG_DATA[38] =(uint8_t) (gear_pos);//P_control_activation_rr;//gear_pos;                          //ch25
    CAN_LOG_DATA[39] =(uint8_t) (mtp);                                    //ch26

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               //ch27
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

    CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch28
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                    //ch29
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
    CAN_LOG_DATA[46] =(uint8_t) (drive_torq);//slip_m_fl_current;//Beta_MD;                               //ch30
    CAN_LOG_DATA[47] =(uint8_t) (drive_torq>>8);//slip_m_fl_current>>8);//Beta_MD>>8);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 //ch31
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                           //ch32
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                     //ch33
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);//slip_m_rl_current;//;                   //ch34
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);//slip_m_rl_current>>8);//>>8);

    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                              //ch35
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);
    CAN_LOG_DATA[58] =(uint8_t) (cal_torq);//slip_m_fr_current;//esp_mu;                              //ch36
    CAN_LOG_DATA[59] =(uint8_t) (cal_torq>>8);//slip_m_fr_current>>8);//esp_mu>>8);


    CAN_LOG_DATA[60] =(uint8_t) (Alpha_front);//(esp_tempW9);//det_alatm;                         //ch37
    CAN_LOG_DATA[61] =(uint8_t) (Alpha_front>>8);//(esp_tempW9>>8);//det_alatm>>8);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  if(SLIP_CONTROL_NEED_FL==1)
  {
    esp_tempW8 = FL_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_FR==1)
  {
    esp_tempW8 = FR_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_RL==1)
  {
        esp_tempW8 = RL_ESC.slip_control;
  }
  else if(SLIP_CONTROL_NEED_RR==1)
  {
        esp_tempW8 = RR_ESC.slip_control;
  }
  else
  {
        esp_tempW8 = 0;
  }
#else
  if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
{
    esp_tempW8 = slip_control_front;
}
  else if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
{
    esp_tempW8 = slip_control_rear;
}
else
{
    esp_tempW8 = 0;
}
#endif


    CAN_LOG_DATA[62] =(uint8_t) (esp_tempW8);//target_pressure_rr;//det_alatc;                            //ch38
    CAN_LOG_DATA[63] =(uint8_t) (esp_tempW8>>8);//target_pressure_rr>>8);//det_alatc>>8);
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                             //ch39
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;
    if(FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)                      tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;
    if(EBD_RA)                      tempB5|=0x08;
    if(BTC_fz==1)                   tempB5|=0x10;
#if __TCS
    if(ETCS_ON)                     tempB5|=0x20;
#endif
    if(YAW_CDC_WORK)                tempB5|=0x40;
#if __TCS
    if(ESP_TCS_ON)                  tempB5|=0x80;
#endif

    if(lcu1espActive3rdFlg)       tempB4|=0x01;
#if __ROP    
    if(ROP_REQ_ACT )          tempB4|=0x02;
#else
	if(YAW_SAME_PHASE)          tempB4|=0x02;
#endif
    if(PARTIAL_BRAKE)               tempB4|=0x04;
    if(FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)           tempB4|=0x08;
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;
#if __ADVANCED_MSC
    if ( YAW_CDC_WORK && !ABS_fz ){
        if(target_vol == MSC_14_V)  tempB4|=0x20;
    }
    else {
        if(MOT_ON_FLG)              tempB4|=0x20;
    }
#else
    if(MOT_ON_FLG)                  tempB4|=0x20;
#endif
    if(AUTO_TM) {
        if(BACK_DIR)                tempB4|=0x40;
    } else {
#if __AX_SENSOR
        if(BACKWARD_MOVE)           tempB4|=0x40;
#else
       if(0)           tempB4|=0x40;
#endif
    }
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;
#else
    if(FLAG_COMPULSION_HOLD_REAR||FLAG_COMPULSION_HOLD)             tempB4|=0x80;
#endif

    if(AV_VL_fl)                    tempB3|=0x01;
    if(HV_VL_fl)                    tempB3|=0x02;
    if(ABS_fl)                      tempB3|=0x04;

    if(lcu1espActive3rdFlg)                     tempB3|=0x08;

#if !__SPLIT_TYPE
    if(TCL_DEMAND_fl)               tempB3|=0x10;
#else
    if(TCL_DEMAND_SECONDARY)        tempB3|=0x10;
#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)                tempB3|=0x40;
    if(INITIAL_BRAKE||FLAG_ACTIVE_BOOSTER||FLAG_ACTIVE_BOOSTER_PRE)            tempB3|=0x80;

    if(AV_VL_fr)                    tempB2|=0x01;
    if(HV_VL_fr)                    tempB2|=0x02;
    if(ABS_fr)                      tempB2|=0x04;
#if __P_CONTROL
    if(0)                     tempB2|=0x08;
#else
    if(BTCS_fr)                     tempB2|=0x08;
#endif
#if !__SPLIT_TYPE
    if(TCL_DEMAND_fr)               tempB2|=0x10;
#else
    if(TCL_DEMAND_PRIMARY)          tempB2|=0x10;
#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;
    if(FLAG_ACT_PRE_ACTION_FL||FLAG_ACT_PRE_ACTION_FR)          tempB2|=0x40;
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;

    if(AV_VL_rl)                    tempB1|=0x01;
    if(HV_VL_rl)                    tempB1|=0x02;
    if(ABS_rl)                      tempB1|=0x04;
    if(BTCS_fl||BTCS_rl)  tempB1|=0x08;

    if(BTCS_rl)                     tempB1|=0x08;

    if(EBD_rl)                      tempB1|=0x10;
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_LEFT)                tempB1|=0x40;
#else
    if(S_VALVE_SECONDARY)               tempB1|=0x40;
#endif
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(FL_ESC.FULL_RISE_MODE||FR_ESC.FULL_RISE_MODE||RL_ESC.FULL_RISE_MODE||RR_ESC.FULL_RISE_MODE)          tempB1|=0x80;
#else
    if(FULL_RISE_MODE)          tempB1|=0x80;
#endif

    if(AV_VL_rr)            tempB0|=0x01;
    if(HV_VL_rr)            tempB0|=0x02;
    if(ABS_rr)              tempB0|=0x04;
#if __P_CONTROL
    if(0)             tempB0|=0x08;
#else
    if(BTCS_fr||BTCS_rr)             tempB0|=0x08;
#endif
    if(EBD_rr)               tempB0|=0x10;
    if(ESP_BRAKE_CONTROL_RR)                tempB0|=0x20;
#if !__SPLIT_TYPE
    if(S_VALVE_RIGHT)       tempB0|=0x40;
#else
    if(S_VALVE_PRIMARY)     tempB0|=0x40;
#endif

    if(FLAG_ACT_COMB_CNTR_FL||FLAG_ACT_COMB_CNTR_FR||FLAG_ACT_COMB_CNTR_RL||FLAG_ACT_COMB_CNTR_RR)          tempB0|=0x80;   // steer_360_ok_flg


    CAN_LOG_DATA[66] =(uint8_t) (tempB5);
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);

if(SLIP_CONTROL_NEED_RL==1)
{
    esp_tempW6 = del_target_slip_rl;
}
else if(SLIP_CONTROL_NEED_RR==1)
{
    esp_tempW6 = del_target_slip_rr;
}
else
{
    esp_tempW6 = 0;
}
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);//u16CycleTime;//esp_k9;
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6>>8);//u16CycleTime>>8;//esp_k9>>8;

    CAN_LOG_DATA[74] =(uint8_t) (flags_fl);//del_target_slip_fr;//afz;
    CAN_LOG_DATA[75] =(uint8_t) (flags_fr);// del_target_slip_fr>>8;//afz>>8;

    CAN_LOG_DATA[76] =(uint8_t) (FL.pwm_duty_temp);
    CAN_LOG_DATA[77] =(uint8_t) (FR.pwm_duty_temp);
    CAN_LOG_DATA[78] =(uint8_t) (RL.pwm_duty_temp);
    CAN_LOG_DATA[79] =(uint8_t) (RR.pwm_duty_temp);
}

/*******************************************************************************
* FUNCTION NAME:        LSHBB_vCallHBBLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of HBB
********************************************************************************/
#if __TCMF_LowVacuumBoost
void LSHBB_vCallHBBLogData(void)
{
	uint8_t i;
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);				//ch1
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                            //ch2
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                            //ch3
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                            //ch4
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                            //ch5
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (vref5);                      		//ch6
    CAN_LOG_DATA[11] =(uint8_t) (vref5>>8);
    CAN_LOG_DATA[12] =(uint8_t) (yaw_out);                            //ch7
    CAN_LOG_DATA[13] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[14] =(uint8_t) (wstr);                               //ch8
    CAN_LOG_DATA[15] =(uint8_t) (wstr>>8);
    CAN_LOG_DATA[16] =(uint8_t) (alat);                               //ch9
    CAN_LOG_DATA[17] =(uint8_t) (alat>>8);
    CAN_LOG_DATA[18] =(uint8_t) (afz);								//ch10
    CAN_LOG_DATA[19] =(uint8_t) (afz>>8);

    CAN_LOG_DATA[20] =(uint8_t) (mpress);                             //ch11
    CAN_LOG_DATA[21] =(uint8_t) (mpress>>8);
    CAN_LOG_DATA[22] =(uint8_t) (mpress_slop);                 		//ch12
    CAN_LOG_DATA[23] =(uint8_t) (mpress_slop>>8);

    CAN_LOG_DATA[24] =(uint8_t) (mtp);                                //ch13
    CAN_LOG_DATA[25] =(uint8_t) (eng_rpm);                            //ch14
    CAN_LOG_DATA[26] =(uint8_t) (eng_rpm>>8);								
    CAN_LOG_DATA[27] =(uint8_t) (gs_sel);//(HBB_MSC_DEBUGGER);                           //ch15

    CAN_LOG_DATA[28] = (uint8_t)(flags_fl);                     		//ch16
    CAN_LOG_DATA[29] = (uint8_t)(flags_fr);                     		//ch17
    CAN_LOG_DATA[30] = (uint8_t)(flags_rl);                     		//ch18
    CAN_LOG_DATA[31] = (uint8_t)(flags_rr);                     		//ch19

#if ((__CAR==SYC_KYRON) || (__CAR==SYC_RAXTON)) && (__Vacuum_Sen_CBox_AD==1)
    CAN_LOG_DATA[32] =(uint8_t) (Vacuum_mon_ad);                      //ch20
    CAN_LOG_DATA[33] =(uint8_t) (Vacuum_mon_ad>>8);
#else
    CAN_LOG_DATA[32] =(uint8_t) (lcs16HbbVacuumPres);                         //ch20
    CAN_LOG_DATA[33] =(uint8_t) (lcs16HbbVacuumPres>>8);
#endif

    CAN_LOG_DATA[34] =(uint8_t) fcu8BrkPdlPos; //(AUTO_TM);//(HBB_MSC_MOTOR_ON);//(Vacuum_Pressure);                    	//ch21
    CAN_LOG_DATA[35] =(uint8_t) (lcs16HbbPedalTravel);                       //ch22
    CAN_LOG_DATA[36] =(uint8_t) (lcs16HbbPedalTravel>>8);
    
    CAN_LOG_DATA[37] =(uint8_t) (lcs16HbbTargetPressure);                    //ch23
    CAN_LOG_DATA[38] =(uint8_t) (lcs16HbbTargetPressure>>8);
    CAN_LOG_DATA[39] =(uint8_t) (lcs16HbbAssistPressureTC);                    //ch24
    CAN_LOG_DATA[40] =(uint8_t) (lcs16HbbAssistPressureTC>>8);
    CAN_LOG_DATA[41] =(uint8_t) (lcs16HbbAssistPressureRateTC);               //ch25
    CAN_LOG_DATA[42] =(uint8_t) (lcs16HbbAssistPressureRateTC>>8);
    CAN_LOG_DATA[43] =(uint8_t) (hbb_msc_target_vol);                 //ch26
    CAN_LOG_DATA[44] =(uint8_t) (hbb_msc_target_vol>>8);
    CAN_LOG_DATA[45] =(uint8_t) (target_vol);                         //ch27
    CAN_LOG_DATA[46] =(uint8_t) (target_vol>>8);
#if __ESC_MOTOR_PWM_CONTROL     
    CAN_LOG_DATA[47] =(uint8_t) (lau8MscDuty);                    //ch28
#else
	CAN_LOG_DATA[47] =(uint8_t) (0);                    //ch28
#endif    
    CAN_LOG_DATA[48] =(uint8_t) (fu16CalVoltMOTOR);                         //ch29 
    CAN_LOG_DATA[49] =(uint8_t) (fu16CalVoltMOTOR>>8);   	                
    CAN_LOG_DATA[50] =(uint8_t) (MFC_PWM_DUTY_P);                    //ch30
    CAN_LOG_DATA[51] =(uint8_t) (MFC_PWM_DUTY_S);                    //ch31

    CAN_LOG_DATA[52] =(uint8_t) (lcs16HbbAssistPressureMotor);                  //ch32     				
    CAN_LOG_DATA[53] =(uint8_t) (lcs16HbbAssistPressureMotor>>8);                      
  	CAN_LOG_DATA[54] =(uint8_t) (lcs16HbbAssistPressureRateMotor);                  //ch33  
  	CAN_LOG_DATA[55] =(uint8_t) (lcs16HbbAssistPressureRateMotor>>8);                      
  	CAN_LOG_DATA[56] =(uint8_t) (lcs16HbbMscTargetVolFactor);               	//ch34   
  	CAN_LOG_DATA[57] =(uint8_t) (lcs16HbbMscTargetVolFactor>>8);                   
  	CAN_LOG_DATA[58] =(uint8_t) (lcs16HbbTargetPressureComp);    //(fu16CalVoltFSR);   			//ch35
    CAN_LOG_DATA[59] =(uint8_t) (lcs16HbbTargetPressureComp>>8); //(fu16CalVoltFSR>>8);
    CAN_LOG_DATA[60] =(uint8_t) (lcs16HbbPedalTravelRate);          //ch36
    CAN_LOG_DATA[61] =(uint8_t) (lcs16HbbPedalTravelRate>>8);
    CAN_LOG_DATA[62] =(uint8_t) (along);//(lcs16HbbMscTargetVolFactor);		//ch37
    CAN_LOG_DATA[63] =(uint8_t) (along>>8);//(lcs16HbbMscTargetVolFactor>>8);
  #if (__VACUUM_SENSOR_ENABLE==ENABLE)  
    CAN_LOG_DATA[64] =(uint8_t) (fs16CalcVacuum); //(lss16absAxOffsetComp1000g);
    CAN_LOG_DATA[65] =(uint8_t) (fs16CalcVacuum>>8); //(lss16absAxOffsetComp1000g>>8);
  #else
    CAN_LOG_DATA[64] =(uint8_t) (0); //(fs16CalcVacuum);//(lss16absAxOffsetComp1000g);
    CAN_LOG_DATA[65] =(uint8_t) (0>>8); //(fs16CalcVacuum>>8);//(lss16absAxOffsetComp1000g>>8);
  #endif 
    CAN_LOG_DATA[66] = (uint8_t)(FL.s16_Estimated_Active_Press/10);      
    CAN_LOG_DATA[67] = (uint8_t)(FR.s16_Estimated_Active_Press/10);      
    CAN_LOG_DATA[68] = (uint8_t)(RL.s16_Estimated_Active_Press/10);      
    CAN_LOG_DATA[69] = (uint8_t)(RR.s16_Estimated_Active_Press/10);      
                       
                                                               
        i=0;
        if (BLS                             ==1) i|=0x01;
        if (ABS_fz                          ==1) i|=0x02;
        if (AFZ_OK                          ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
        if ((PBA_ON==1) || (PBA_pulsedown  ==1)) i|=0x10; 
        if (YAW_CDC_WORK                    ==1) i|=0x20;
        if (MOT_ON_FLG                      ==1) i|=0x40; 
        if (ESP_TCS_ON                      ==1) i|=0x80; 
  
        CAN_LOG_DATA[70] = (i);               //ch43

        i=0;
        if (HBB_BRAKE_PEDAL_FAST_RELEASE    ==1) i|=0x01;
        if (HBB_NEED_AdditionalBrkFluidP    ==1) i|=0x02; 
        if (LFC_Split_flag                  ==1) i|=0x04; 
        if (LFC_Split_suspect_flag          ==1) i|=0x08; 
        if (BEND_DETECT_LEFT                ==1) i|=0x10; 
        if (BEND_DETECT_RIGHT               ==1) i|=0x20; 
        if (Rough_road_suspect_vehicle      ==1) i|=0x40; 
        if (Rough_road_detect_vehicle       ==1) i|=0x80; 

        CAN_LOG_DATA[71] = i;               //ch44

        i = 0;
        if (ABS_fl                          ==1) i|=0x01;
        if (TCMF_LowVacuumBoost_flag        ==1) i|=0x02;
        if (STABIL_fl                       ==1) i|=0x04;
        if (AV_HOLD_fl                      ==1) i|=0x08;
        if (AV_DUMP_fl                      ==1) i|=0x10;
        if (GMA_SLIP_fl                     ==1) i|=0x20;
        if (HV_VL_fl                        ==1) i|=0x40;
        if (AV_VL_fl                        ==1) i|=0x80;

        CAN_LOG_DATA[72] = i;               //ch45

        i=0;
        if (ABS_fr                          ==1) i|=0x01;
        if (ESP_ABS_SLIP_CONTROL_fr         ==1) i|=0x02;
        if (STABIL_fr                       ==1) i|=0x04;
        if (AV_HOLD_fr                      ==1) i|=0x08;
        if (AV_DUMP_fr                      ==1) i|=0x10;
        if (GMA_SLIP_fr                     ==1) i|=0x20;
        if (HV_VL_fr                        ==1) i|=0x40;
        if (AV_VL_fr                        ==1) i|=0x80;

        CAN_LOG_DATA[73] = i;               //ch46

        i=0;
        if (ABS_rl                          ==1) i|=0x01;
        if (ESP_ABS_SLIP_CONTROL_rl         ==1) i|=0x02;
        if (STABIL_rl                       ==1) i|=0x04;
        if (SPLIT_JUMP_rl                   ==1) i|=0x08;
        if (SPLIT_PULSE_rl                  ==1) i|=0x10;
        if (RL.AV_DUMP                      ==1) i|=0x20;
        if (HV_VL_rl                        ==1) i|=0x40;
        if (AV_VL_rl                        ==1) i|=0x80;

        CAN_LOG_DATA[74] = i;               //ch47

        i = 0;
        if(ABS_rr                           ==1) i|=0x01;
        if(ESP_ABS_SLIP_CONTROL_rr          ==1) i|=0x02;
        if(STABIL_rr                        ==1) i|=0x04;
        if(SPLIT_JUMP_rr                    ==1) i|=0x08;
        if(SPLIT_PULSE_rr                   ==1) i|=0x10;
        if(RR.AV_DUMP                       ==1) i|=0x20;
        if(HV_VL_rr                         ==1) i|=0x40;
        if(AV_VL_rr                         ==1) i|=0x80;

        CAN_LOG_DATA[75] = i;               //ch48

        i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_SECONDARY                        ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fl                        ==1) i|=0x01;
      #endif
        if(FL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(FL.LFC_maintain_split_flag           ==1) i|=0x08;
        if(MOT_ON_FLG                           ==1) i|=0x10;
        if(FL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(FL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fl                       ==1) i|=0x80;

        CAN_LOG_DATA[76] = i;                   //ch49

         i=0;
      #if (__SPLIT_TYPE==1)
        if(TCL_DEMAND_PRIMARY                        ==1) i|=0x01;
      #else
        if(TCL_DEMAND_fr                        ==1) i|=0x01;
      #endif
        if(FR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(FR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(FR.LFC_maintain_split_flag           ==1) i|=0x08;
        if(FR.In_Gear_flag                      ==1) i|=0x10;
        if(FR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(FR.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_fr                       ==1) i|=0x80;

        CAN_LOG_DATA[77] = i;                   //ch50

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_SECONDARY                         ==1) i|=0x01;
      #else
        if(S_VALVE_LEFT                         ==1) i|=0x01;
      #endif
        if(RL.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RL.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(RL.MSL_BASE                          ==1) i|=0x08;
        if(RL.Duty_Limitation_flag              ==1) i|=0x10;
        if(RL.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(RL.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_rl                       ==1) i|=0x80;

        CAN_LOG_DATA[78] = i;                   //ch51

        i=0;
      #if (__SPLIT_TYPE==1)
        if(S_VALVE_PRIMARY                        ==1) i|=0x01;
      #else
        if(S_VALVE_RIGHT                        ==1) i|=0x01;
      #endif
        if(RR.L_SLIP_CONTROL                    ==1) i|=0x02;
        if(RR.LFC_big_rise_and_dump_flag        ==1) i|=0x04;
        if(RR.MSL_BASE                          ==1) i|=0x08;
        if(RR.Duty_Limitation_flag              ==1) i|=0x10;
        if(RR.LFC_n_th_deep_slip_pre_flag       ==1) i|=0x20;   
        if(RR.LFC_built_reapply_flag            ==1) i|=0x40;
        if(SPOLD_RESET_rr                       ==1) i|=0x80;

        CAN_LOG_DATA[79] = i;                   //ch52
}
#endif /* #if __TCMF_LowVacuumBoost*/

#if __EDC
void LSEDC_vCallEDCLogData(void)
{
	uint8_t i;
        CAN_LOG_DATA[0] = (uint8_t)(system_loop_counter);
        CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);   //uint16_t,1
        CAN_LOG_DATA[2] = (uint8_t)(vrad_fl);
        CAN_LOG_DATA[3] = (uint8_t)(vrad_fl>>8);               //uint16_t,8
        CAN_LOG_DATA[4] = (uint8_t)(vrad_fr);
        CAN_LOG_DATA[5] = (uint8_t)(vrad_fr>>8);               //uint16_t,8
        CAN_LOG_DATA[6] = (uint8_t)(vrad_rl);
        CAN_LOG_DATA[7] = (uint8_t)(vrad_rl>>8);               //uint16_t,8
        CAN_LOG_DATA[8] = (uint8_t)(vrad_rr);
        CAN_LOG_DATA[9] = (uint8_t)(vrad_rr>>8);               //uint16_t,8
	#if __VDC
        CAN_LOG_DATA[10] = (uint8_t)(vref5);
        CAN_LOG_DATA[11] = (uint8_t)(vref5>>8);               //uint16_t,8
	#else
        CAN_LOG_DATA[10] = (uint8_t)(vref);
        CAN_LOG_DATA[11] = (uint8_t)(vref>>8);                //uint16_t,8
	#endif

        CAN_LOG_DATA[12] = (uint8_t)(lcedcs16WheelSlipError);
        CAN_LOG_DATA[13] = (uint8_t)(lcedcs16WheelSlipError>>8);
        CAN_LOG_DATA[14] = (uint8_t)(lcedcs16WheelSlipErrDiff);
        CAN_LOG_DATA[15] = (uint8_t)(lcedcs16WheelSlipErrDiff>>8);
        tempW0=(int16_t)(-lcedcs16PGainEffect);
        tempW1=(int16_t)(-lcedcs16DGainEffect);
        CAN_LOG_DATA[16] = (uint8_t)(tempW0);
        CAN_LOG_DATA[17] = (uint8_t)(tempW0>>8);
        CAN_LOG_DATA[18] = (uint8_t)(tempW1);
        CAN_LOG_DATA[19] = (uint8_t)(tempW1>>8);

        CAN_LOG_DATA[20] = (uint8_t)(vref_L);
        CAN_LOG_DATA[21] = (uint8_t)(vref_L>>8);

	#if __EDC_GEAR_SHIFT_DOWN_DETECTION
        CAN_LOG_DATA[22] = (uint8_t)(ldedcs8GearShiftDownCount);
    #else
        CAN_LOG_DATA[22] = (uint8_t)(RL.ldedcu8ParkDecelCheckCnt);
    #endif    
        CAN_LOG_DATA[23] = (uint8_t)(RR.ldedcu8ParkDecelCheckCnt);
        CAN_LOG_DATA[24] = (uint8_t)(ldedcu16EngRpmAtDecelCheck_rl/100);
        CAN_LOG_DATA[25] = (uint8_t)(ldedcu16EngRpmAtDecelCheck_rr/100);
        CAN_LOG_DATA[26] = (uint8_t)(ldedcu8ParkBrkSuspectCounter_rl);
        CAN_LOG_DATA[27] = (uint8_t)(ldedcu8ParkBrkSuspectCounter_rr);

        CAN_LOG_DATA[28] = (uint8_t)(EDC_flags);
        CAN_LOG_DATA[29] = (uint8_t)(gear_state);//EDC_mon1;//gear_state;
        CAN_LOG_DATA[30] = (uint8_t)(engine_state);//EDC_mon2;//engine_state;

        CAN_LOG_DATA[31] = (uint8_t)(delta_yaw_first);
        CAN_LOG_DATA[32] = (uint8_t)(delta_yaw_first>>8);

    #if __VDC
        CAN_LOG_DATA[33] = (uint8_t)(mpress);
        CAN_LOG_DATA[34] = (uint8_t)(mpress>>8);
    #else
        CAN_LOG_DATA[33] = 0;
        CAN_LOG_DATA[34] = 0;
    #endif

        CAN_LOG_DATA[35] = (uint8_t)(drive_torq);
        CAN_LOG_DATA[36] = (uint8_t)(drive_torq>>8);
        CAN_LOG_DATA[37] = (uint8_t)(MSR_cal_torq);
        CAN_LOG_DATA[38] = (uint8_t)(MSR_cal_torq>>8);
        CAN_LOG_DATA[39] = (uint8_t)(eng_torq);
        CAN_LOG_DATA[40] = (uint8_t)(eng_torq>>8);
        CAN_LOG_DATA[41] = (uint8_t)(((int16_t)(lesps16EMS_TQFR))/10);  //TQFR
        CAN_LOG_DATA[42] = (uint8_t)(flywheel_torq);
        CAN_LOG_DATA[43] = (uint8_t)(flywheel_torq>>8);
        CAN_LOG_DATA[44] = (uint8_t)(lcedcs16AvgDrivenWheelSlip);
        CAN_LOG_DATA[45] = (uint8_t)(lcedcs16AvgDrivenWheelSlip>>8);
        CAN_LOG_DATA[46] = (uint8_t)(eng_rpm);
        CAN_LOG_DATA[47] = (uint8_t)(eng_rpm>>8);

    if(AUTO_TM==1)
    {
        CAN_LOG_DATA[48] = (uint8_t)(tc_rpm);
        CAN_LOG_DATA[49] = (uint8_t)(tc_rpm>>8);
    }
    else
    {
        CAN_LOG_DATA[48] = 0;
        CAN_LOG_DATA[49] = 0;
    }
      #if __EDC_GEAR_SHIFT_DOWN_DETECTION 
        CAN_LOG_DATA[50] = (uint8_t)(slope_eng_rpm_for_GSD);      
        CAN_LOG_DATA[51] = (uint8_t)(slope_eng_rpm_for_GSD>>8);                       
	  #else
        CAN_LOG_DATA[50] = (uint8_t)(slope_eng_rpm);       
        CAN_LOG_DATA[51] = (uint8_t)(slope_eng_rpm>>8);	  
	  #endif 

	  #if __VDC
        CAN_LOG_DATA[52] = (uint8_t)(yaw_out);
        CAN_LOG_DATA[53] = (uint8_t)(yaw_out>>8);
        CAN_LOG_DATA[54] = (uint8_t)(rf2);
        CAN_LOG_DATA[55] = (uint8_t)(rf2>>8);
        CAN_LOG_DATA[56] = (uint8_t)(wstr);
        CAN_LOG_DATA[57] = (uint8_t)(wstr>>8);
    	CAN_LOG_DATA[58] = (uint8_t)(wu16CurCycleTime);
    	CAN_LOG_DATA[59] = (uint8_t)(wu16CurCycleTime>>8);
    	CAN_LOG_DATA[60] = (uint8_t)(vref_R);
    	CAN_LOG_DATA[61] = (uint8_t)(vref_R>>8);
      #else
        CAN_LOG_DATA[52] = 0;
        CAN_LOG_DATA[53] = 0;
        CAN_LOG_DATA[54] = 0;
        CAN_LOG_DATA[55] = 0;
        CAN_LOG_DATA[56] = 0;
        CAN_LOG_DATA[57] = 0;
        CAN_LOG_DATA[58] = 0;
        CAN_LOG_DATA[59] = 0;
        CAN_LOG_DATA[60] = 0;
        CAN_LOG_DATA[61] = 0;
      #endif
        CAN_LOG_DATA[62] = (uint8_t)(lcedcs16TargetWheelSlip/2);//(EMS1[0]&0x30)/16;       //TQ_COR_STAT : 11h :torque intervention is terminated
        CAN_LOG_DATA[63] = (uint8_t)(EDC_exit_counter);//EDC_mon1;//gear_pos;
        CAN_LOG_DATA[64] = (uint8_t)(lcedcs16PGain);
        CAN_LOG_DATA[65] = (uint8_t)(lcedcs16DGain);

        i=0;
        if (BEND_DETECT2        ==1) i|=0x01;
        if (BLS                 ==1) i|=0x02;  //BLS
        if (ABS_fz              ==1) i|=0x04; //ABS_fz
        if (EBD_RA              ==1) i|=0x08; //EBD_RA
      #if __TCS
        if (BTC_fz              ==1) i|=0x10;
        if (ETCS_ON             ==1) i|=0x20;    //ETCS_ON
        if (FTCS_ON             ==1) i|=0x40;    //FTCS_ON
      #else
        if (0              		==1) i|=0x10;
        if (0             		==1) i|=0x20;
        if (0             		==1) i|=0x40;
      #endif
        if (ABS_ON_flag         ==1) i|=0x80;
        CAN_LOG_DATA[66] = i;

        i=0;
        if (lespu1TCS_TCS_REQ   ==1) i|=0x01;        //TCS_REQ
        if (ldedcu1TempTireSuspect  ==1) i|=0x02;        /* Temp Tire Suspect */
        if (lespu1TCS_MSR_C_REQ ==1) i|=0x04; 		//MSR_C_REQ
        if (EDC_SOLO_ON         ==1) i|=0x08;
        if (In_gear_flag       	==1) i|=0x10;
        if (EDC_SUSTAIN_1SEC    ==1) i|=0x20; 	//FUNCTION_LAMP_ON

        #if __EDC
        	i|=0x40;
        #else
        	i&=0xBF;
        #endif

        if (lespu1TCS_ABS_ACT   ==1) i|=0x80;        //ABS_ACT

        CAN_LOG_DATA[67] = i;

        i = 0;
        if (AV_VL_fl            ==1) i|=0x01;
        if (HV_VL_fl            ==1) i|=0x02;
        if (ABS_fl              ==1) i|=0x04;
        if (EDC_NOR_EXIT        ==1) i|=0x08;		//EDC_EXIT
	  #if __REAR_D
        if (RL.ldedcu1EdcDecelDetect   		==1) i|=0x10;
        if (RR.ldedcu1EdcDecelDetect 		==1) i|=0x20;
	  #else
        if (FL.ldedcu1EdcDecelDetect   		==1) i|=0x10;
        if (FR.ldedcu1EdcDecelDetect 		==1) i|=0x20;
      #endif
    	if (EDC_EXIT            ==1) i|=0x40;	      //TCS_MFRN   //TCS1[1]&0x80
        if (EDC_INHIBIT_flag    ==1) i|=0x80;
        CAN_LOG_DATA[68] = i;

        i=0;
        if (AV_VL_fr            ==1) i|=0x01;
        if (HV_VL_fr            ==1) i|=0x02;
        if (ABS_fr              ==1) i|=0x04;
      #if __VDC
        if (EDC_ESP_ON          ==1) i|=0x08;
        if (YAW_CDC_WORK		==1) i|=0x10;
        if (ESP_TCS_ON          ==1) i|=0x20;
      #else
        if (0             		==1) i|=0x08;
        if (0       			==1) i|=0x10;
        if (0			        ==1) i|=0x20;
      #endif
      
      #if __EDC_GEAR_SHIFT_DOWN_DETECTION
		if (ldedcu1GearShiftDownSuspectFlag ==1) i|=0x40;
	  #else
		if (Engine_braking_flag				==1) i|=0x40;
	  #endif

        if (RL.ldedcu1ParkBrkSuspectWL  ==1) i|=0x80;
        CAN_LOG_DATA[69] = i;

        i=0;
        if (AV_VL_rl            ==1) i|=0x01;
        if (HV_VL_rl            ==1) i|=0x02;
        if (ABS_rl              ==1) i|=0x04;
        if (RR.ldedcu1ParkBrkSuspectWL  ==1) i|=0x08;
        if (ldedcu1ParkBrkSuspectFlag	==1) i|=0x10;
        if (Engine_Drag_flag    		==1) i|=0x20;
        if (RL.ldedcu1ParkDecelCheckWL ==1) i|=0x40;
        if (RR.ldedcu1ParkDecelCheckWL ==1) i|=0x80;

        CAN_LOG_DATA[70] = i;

        i = 0;
        if (AV_VL_rr            ==1) i|=0x01;
        if (HV_VL_rr            ==1) i|=0x02;
        if (ABS_rr              ==1) i|=0x04;
      #if __REAR_D
        if (RL.ldedcu1EdcNorOn  ==1) i|=0x08;
        if (RR.ldedcu1EdcNorOn 	==1) i|=0x10;
      #else
        if (FL.ldedcu1EdcNorOn  ==1) i|=0x08;
        if (FR.ldedcu1EdcNorOn 	==1) i|=0x10;
      #endif
        if (EDC_ON         		==1) i|=0x20;
        if (EDC_ABS_ON          ==1) i|=0x40;
        if (EDC_NOR_ON_FLAG       	==1) i|=0x80;
        CAN_LOG_DATA[71] = i;

        CAN_LOG_DATA[72] = (uint8_t)(EDC_Exit_flags);
        CAN_LOG_DATA[73] = (uint8_t)(mtp);
        CAN_LOG_DATA[74] = (uint8_t)((int16_t)(lesps16EMS_TPS)); 	//TPS
        CAN_LOG_DATA[75] = (uint8_t)(EDC_Phase_flags);
        CAN_LOG_DATA[76] = (uint8_t)(((uint16_t)(lesps16TCS_TQI_TCS))/10);		//TQI_TCS
        CAN_LOG_DATA[77] = (uint8_t)(((uint16_t)(lesps16TCS_TQI_MSR))/10);		//TQI_MSR
        i = 0;
        if (Engine_braking_flag          ==1) i|=0x01;
        if (ldedcu1HighMuSuspectFlag     ==1) i|=0x02;
	#if ((__PARK_BRAKE_TYPE == CAN_TYPE) ||(__PARK_BRAKE_TYPE==ANALOG_TYPE)) 
		if (fu1DelayedParkingBrakeSignal ==1) i|=0x04;        
        if (fu1ParkingBrakeSusDet        ==1) i|=0x08;
    #else
        if (0                               ) i|=0x04;
        if (0								) i|=0x08;
    #endif
        if (lcedcu1ExitByExcessiveTqUp	 ==1) i|=0x10;
        if (lcedcu1CtrlTimeOver          ==1) i|=0x20;
        if (fu1ESCDisabledBySW   ==1) i|=0x40;
        if (fu1TCSDisabledBySW   ==1) i|=0x80;
        CAN_LOG_DATA[78] = i;
        CAN_LOG_DATA[79] = (uint8_t)EDC_Inhibit_flags;
}
#endif /* #if __EDC */

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallCBCLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of CBC
********************************************************************************/
#if __CBC
void LSESP_vCallCBCLogData(void)
{
    uint8_t i;
    /* ID 0x111, 0x6DC */
    CAN_LOG_DATA[0] = (uint8_t)system_loop_counter;   
    CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);           /* CH 01 : SYS_C */
    CAN_LOG_DATA[2] = (uint8_t)yaw_out;            
    CAN_LOG_DATA[3] = (uint8_t)(yaw_out>>8);                       /* CH 02 : yawm */
    CAN_LOG_DATA[4] = (uint8_t)alat;            
    CAN_LOG_DATA[5] = (uint8_t)(alat>>8);                          /* CH 03 : alat */
    CAN_LOG_DATA[6] = (uint8_t)wstr;          
    CAN_LOG_DATA[7] = (uint8_t)(wstr>>8);                          /* CH 04 : wstr */
	/* ID 0x222, 0x6DD */
    CAN_LOG_DATA[8]  = (uint8_t)vrad_fl;     
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_fl>>8);                      /* CH 05 : vrdFL */
    CAN_LOG_DATA[10] = (uint8_t)vrad_fr;     
    CAN_LOG_DATA[11] = (uint8_t)(vrad_fr>>8);                      /* CH 06 : vrdFR */
    CAN_LOG_DATA[12] = (uint8_t)vrad_rl;     
    CAN_LOG_DATA[13] = (uint8_t)(vrad_rl>>8);                      /* CH 07 : vrdRL */
    CAN_LOG_DATA[14] = (uint8_t)vrad_rr;      
    CAN_LOG_DATA[15] = (uint8_t)(vrad_rr>>8);                      /* CH 08 : vrdRR */
	/* ID 0x333, 0x6DE */
    CAN_LOG_DATA[16] = (uint8_t)vref;
    CAN_LOG_DATA[17] = (uint8_t)(vref>>8);                         /* CH 09 : vref */
  #if __4WD || __AX_SENSOR
    CAN_LOG_DATA[18] = (uint8_t)along;
    CAN_LOG_DATA[19] = (uint8_t)(along>>8);                        /* CH 10 : along */
  #else
    CAN_LOG_DATA[18] = (uint8_t)vref5;
    CAN_LOG_DATA[19] = (uint8_t)(vref5>>8);                        /* CH 10 */
  #endif
  if((lccbcFL.u1CbcFadeOut==1)||(lccbcFR.u1CbcFadeOut==1)||(lccbcRL.u1CbcFadeOut==1)||(lccbcRR.u1CbcFadeOut==1))
  {
	CAN_LOG_DATA[20] = (uint8_t)lccbcFL.u8CbcPwmDuty;			   /* CH 11 : PWMfl */
    CAN_LOG_DATA[21] = (uint8_t)lccbcFR.u8CbcPwmDuty;              /* CH 12 : PWMfr */
    CAN_LOG_DATA[22] = (uint8_t)lccbcRL.u8CbcPwmDuty;              /* CH 13 : PWMrl */
    CAN_LOG_DATA[23] = (uint8_t)lccbcRR.u8CbcPwmDuty;              /* CH 14 : PWMrr */
  }
  else
  {  
    CAN_LOG_DATA[20] = (uint8_t)FL.pwm_duty_temp;                  /* CH 11 : PWMfl */
    CAN_LOG_DATA[21] = (uint8_t)FR.pwm_duty_temp;                  /* CH 12 : PWMfr */
    CAN_LOG_DATA[22] = (uint8_t)RL.pwm_duty_temp;                  /* CH 13 : PWMrl */
    CAN_LOG_DATA[23] = (uint8_t)RR.pwm_duty_temp;                  /* CH 14 : PWMrr */
  }
    /* ID 0x444, 0x6DF */
  #if __FRONT_CBC_ENABLE
    CAN_LOG_DATA[24] = (uint8_t)(lccbcFL.s8FadeOutMode*10+lccbcFR.s8FadeOutMode);/* CH 15 : modeF */
  #endif
    CAN_LOG_DATA[25] = (uint8_t)(lccbcRL.s8FadeOutMode*10+lccbcRR.s8FadeOutMode);/* CH 16 : modeR */
  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[26] = (uint8_t)(((lsrbcs16HydPressReduceReq/10)>127)?127:(((lsrbcs16HydPressReduceReq/10)<-127)?-127:(int8_t)(lsrbcs16HydPressReduceReq/10)));/* CH 17 : engTQ */
  #endif
    CAN_LOG_DATA[27] = (uint8_t)mtp;                               /* CH 18 : mtp */
    CAN_LOG_DATA[28] = (uint8_t)FL.flags;                          /* CH 19 : flgFL */
    CAN_LOG_DATA[29] = (uint8_t)FR.flags;                          /* CH 20 : flgFR */
    CAN_LOG_DATA[30] = (uint8_t)RL.flags;                          /* CH 21 : flgRL */
    CAN_LOG_DATA[31] = (uint8_t)RR.flags;                          /* CH 22 : flgRR */
	/* ID 0x555, 0x6E0 */
  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[32] = (uint8_t)((lcabss16RefCircuitPress/10)>127)?127:(((lcabss16RefCircuitPress/10)<-127)?-127:((int8_t)(lcabss16RefCircuitPress/10)));/* CH 23 : CBCmode */
  #else
    CAN_LOG_DATA[32] = (uint8_t)lccbcu8CbcMode;					   /* CH 23 : CBCmode */
  #endif
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[33] = (uint8_t)(lccbcs16EstMpress/10);            /* CH 24 : MpresE */
    #endif
  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[34] = (uint8_t)(lcabss16RefMpress/10);            /* CH 25 : mpress */
  #else
    CAN_LOG_DATA[34] = (uint8_t)(mpress/10);                       /* CH 25 : mpress */
  #endif
    CAN_LOG_DATA[35] = (uint8_t)RL.arad;          				   /* CH 26 : ardRL */
    CAN_LOG_DATA[36] = (uint8_t)RR.arad;          				   /* CH 27 : ardRR */
    CAN_LOG_DATA[37] = (uint8_t)(CBC_target_p/10);                 /* CH 28 : tar_P */
    CAN_LOG_DATA[38] = (uint8_t)(inhibition_number = INHIBITION_INDICATOR()); /* CH 29 : Error */
    CAN_LOG_DATA[39] = (uint8_t)(CBC_target_delta_p/10);           /* CH 30 : del_P */
	/* ID 0x666, 0x6E1 */
    CAN_LOG_DATA[40] = (uint8_t)rf2;                             
    CAN_LOG_DATA[41] = (uint8_t)(rf2>>8);						   /* CH 31 : yawc */
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[42] = (uint8_t)lccbcs16AyCheckCounter;               
    CAN_LOG_DATA[43] = (uint8_t)(lccbcs16AyCheckCounter>>8);       /* CH 32 : Aycnt */
    CAN_LOG_DATA[44] = (uint8_t)lccbcs16AyCheckCounter2;               
    CAN_LOG_DATA[45] = (uint8_t)(lccbcs16AyCheckCounter2>>8);      /* CH 33 : Aycnt2 */
    CAN_LOG_DATA[46] = (uint8_t)lccbcs16AyOKCounter;    
    CAN_LOG_DATA[47] = (uint8_t)(lccbcs16AyOKCounter>>8);          /* CH 34 : OKcnt */
    #endif
    /* ID 0x117, 0x6E2 */
    CAN_LOG_DATA[48] = (uint8_t)(((sign_o_delta_yaw_thres/10)>127)?127:(((sign_o_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_o_delta_yaw_thres/10)));/* CH 35 : dyt_p */
    CAN_LOG_DATA[49] = (uint8_t)(((sign_u_delta_yaw_thres/10)>127)?127:(((sign_u_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_u_delta_yaw_thres/10)));/* CH 36 : dyt_m */
    CAN_LOG_DATA[50] = (uint8_t)(((nosign_delta_yaw_first/10)>127)?127:(((nosign_delta_yaw_first/10)<-127)?-127:(int8_t)(nosign_delta_yaw_first/10)));/* CH 37 : del_t */
    CAN_LOG_DATA[51] = (uint8_t)(((nosign_delta_yaw/10)>127)?127:(((nosign_delta_yaw/10)<-127)?-127:(int8_t)(nosign_delta_yaw/10)));                  /* CH 38 : del_y */
    CAN_LOG_DATA[52] = (uint8_t)RL.u8_Estimated_Active_Press;      /* CH 39 : PErl */
    CAN_LOG_DATA[53] = (uint8_t)RR.u8_Estimated_Active_Press;      /* CH 40 : PErr */
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[54] = (uint8_t)(lccbcs16EstWPressRL/10);          /* CH 41 : PE0rl */
    CAN_LOG_DATA[55] = (uint8_t)(lccbcs16EstWPressRR/10);          /* CH 42 : PE0rr */
    #endif
    /* ID 0x227, 0x6E3 */
    CAN_LOG_DATA[56] = (uint8_t)FL.vfiltn;                         /* CH 43 : dv_FL */
    CAN_LOG_DATA[57] = (uint8_t)FR.vfiltn;                         /* CH 44 : dv_FR */
    CAN_LOG_DATA[58] = (uint8_t)FL.u8_Estimated_Active_Press;      /* CH 45 : PEfl */
    CAN_LOG_DATA[59] = (uint8_t)FR.u8_Estimated_Active_Press;      /* CH 46 : PEfr */
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[60] = (uint8_t)lccbcs16EstAy2;                   
    CAN_LOG_DATA[61] = (uint8_t)(lccbcs16EstAy2>>8);               /* CH 47 : AyE2 */
    CAN_LOG_DATA[62] = (uint8_t)lccbcs16EstVref;
    CAN_LOG_DATA[63] = (uint8_t)(lccbcs16EstVref>>8);              /* CH 48 : vrefC*/
    #endif
    /* ID 0x337, 0x6E4 */
    CAN_LOG_DATA[64] = (uint8_t)vref_R;       
    CAN_LOG_DATA[65] = (uint8_t)(vref_R>>8);                       /* CH 49 : vrefR */
    CAN_LOG_DATA[66] = (uint8_t)vref_L;                       
    CAN_LOG_DATA[67] = (uint8_t)(vref_L>>8);                       /* CH 50 : vrefL */
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[68] = (uint8_t)lccbcs16EstAy;  
    CAN_LOG_DATA[69] = (uint8_t)(lccbcs16EstAy>>8);                /* CH 51 : AyE */
    CAN_LOG_DATA[70] = (uint8_t)lccbcs16EstAx;  
    CAN_LOG_DATA[71] = (uint8_t)(lccbcs16EstAx>>8);                /* CH 52 : AxE */
    #endif
    /* ID 0x447, 0x6E5 */
    i=0;                                  
    if (BLS                             ==1) i|=0x01;	/* Flag 01 : BLS   */
    if (ABS_fz                          ==1) i|=0x02;	/* Flag 02 : ABSfz */
    if (AFZ_OK                          ==1) i|=0x04;	/* Flag 03 : AFZOK */
    if (EBD_RA                          ==1) i|=0x08;	/* Flag 04 : EBD   */
    if (BTC_fz                          ==1) i|=0x10;	/* Flag 05 : BTC_O */
    if (YAW_CDC_WORK                    ==1) i|=0x20;	/* Flag 06 : Y_C_W */
    if (ETCS_ON                         ==1) i|=0x40;	/* Flag 07 : ETC_O */
    if (ESP_TCS_ON                      ==1) i|=0x80;	/* Flag 08 : T_E_O */

    CAN_LOG_DATA[72] = i;               /* 53 */
    
    i=0;                                  
    if (MOT_ON_FLG                      ==1) i|=0x01;	/* Flag 09 : moton */
    if (CBC_FIRST_SCAN                  ==1) i|=0x02;	/* Flag 10 : CBC_K */
    if (CBC_FADE_OUT_fl                 ==1) i|=0x04;	/* Flag 11 : FADfl */
    if (CBC_FADE_OUT_fr                 ==1) i|=0x08;	/* Flag 12 : FADfr */
    if (lccbcu1UnstableRearSlip         ==1) i|=0x10;	/* Flag 13 : Rslip */
    if (lccbcu1CbcSlipOK                ==1) i|=0x20;	/* Flag 14 : SlipOK */
    if (Rough_road_suspect_vehicle      ==1) i|=0x40;	/* Flag 15 : RRsus */
    if (Rough_road_detect_vehicle       ==1) i|=0x80;	/* Flag 16 : Rough */

    CAN_LOG_DATA[73] = i;               /* 54 */

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;	/* Flag 17 : ABSfl */
    if (CBC_ENABLE_fl                   ==1) i|=0x02;	/* Flag 18 : CBCfl */
    if (STABIL_fl                       ==1) i|=0x04;	/* Flag 19 : STBfl */
    if (SPOLD_RESET_fl                  ==1) i|=0x08;	/* Flag 20 : SPRfl */
    if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10;	/* Flag 21 : ESPfl */
    if (GMA_SLIP_fl                     ==1) i|=0x20;	/* Flag 22 : GMAfl */
    if (HV_VL_fl                        ==1) i|=0x40;	/* Flag 23 : HV_fl */
    if (AV_VL_fl                        ==1) i|=0x80;	/* Flag 24 : AV_fl */

    CAN_LOG_DATA[74] = i;               /* 55 */
    
    i=0;                                          
    if (ABS_fr                          ==1) i|=0x01;	/* Flag 25 : ABSfr */
    if (CBC_ENABLE_fr                   ==1) i|=0x02;	/* Flag 26 : CBCfr */
    if (STABIL_fr                       ==1) i|=0x04;	/* Flag 27 : STBfr */
    if (SPOLD_RESET_fr                  ==1) i|=0x08;	/* Flag 28 : SPRfr */
    if (ESP_BRAKE_CONTROL_FR	        ==1) i|=0x10;	/* Flag 29 : ESPfr */
    if (GMA_SLIP_fr                     ==1) i|=0x20;	/* Flag 30 : GMAfr */
    if (HV_VL_fr                        ==1) i|=0x40;	/* Flag 31 : HV_fr */
    if (AV_VL_fr                        ==1) i|=0x80;	/* Flag 32 : AV_fr */
    
    CAN_LOG_DATA[75] = i;               /* 56 */
    
    i=0;                                   
    if (ABS_rl                          ==1) i|=0x01;	/* Flag 33 : ABSrl */
    if (CBC_ENABLE_rl                   ==1) i|=0x02;	/* Flag 34 : CBCrl */
    if (STABIL_rl                       ==1) i|=0x04;	/* Flag 35 : STBrl */
    if (SPOLD_RESET_rl                  ==1) i|=0x08;	/* Flag 36 : SPRrl */
    if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x10;	/* Flag 37 : ESPrl */
    if (RL.MSL_BASE                     ==1) i|=0x20;	/* Flag 38 : BA_rl */
    if (HV_VL_rl                        ==1) i|=0x40;	/* Flag 39 : HV_rl */
    if (AV_VL_rl                        ==1) i|=0x80;	/* Flag 40 : AV_rl */

    CAN_LOG_DATA[76] = i;               /* 57 */

    i = 0;
    if(ABS_rr                           ==1) i|=0x01;	/* Flag 41 : ABSrr */
    if(CBC_ENABLE_rr                    ==1) i|=0x02;	/* Flag 42 : CBCrr */
    if(STABIL_rr                        ==1) i|=0x04;	/* Flag 43 : STBrr */
    if(SPOLD_RESET_rr                   ==1) i|=0x08;	/* Flag 44 : SPRrr */
    if(ESP_BRAKE_CONTROL_RR             ==1) i|=0x10;	/* Flag 45 : ESPrr */
    if(RR.MSL_BASE                      ==1) i|=0x20;	/* Flag 46 : BA_rr */
    if(HV_VL_rr                         ==1) i|=0x40;	/* Flag 47 : HV_rr */
    if(AV_VL_rr                         ==1) i|=0x80;	/* Flag 48 : AV_rr */

    CAN_LOG_DATA[77] = i;               /* 58 */

    i=0;
    if(TCL_DEMAND_fl                    ==1) i|=0x01;	/* Flag 49 : TCLfl */
    if(TCL_DEMAND_fr                    ==1) i|=0x02;	/* Flag 50 : TCLfr */
    if(S_VALVE_LEFT                     ==1) i|=0x04;	/* Flag 51 : S_V_L */
    if(S_VALVE_RIGHT                    ==1) i|=0x08;	/* Flag 52 : S_V_R */
    if(CBC_INHIBIT_ESC_FLAG             ==1) i|=0x10;	/* Flag 53 : EResc */
    if(CBC_INHIBIT_ABS_FLAG             ==1) i|=0x20;	/* Flag 54 : ERabs */
    if(lccbcu1AyCheckOK                 ==1) i|=0x40;	/* Flag 55 : Ay_ok */
    if(lccbcu1VehSteadyState            ==1) i|=0x80;	/* Flag 56 : Stabl */
    
    CAN_LOG_DATA[78] = i;               /* 59 */
    
     i=0;
    if(CBC_FADE_OUT_rl                  ==1) i|=0x01;	/* Flag 57 : FADrl */
    if(CBC_FADE_OUT_rr                  ==1) i|=0x02;	/* Flag 58 : FADrr */
    if(CBC_ACTIVE_rl                    ==1) i|=0x04;	/* Flag 59 : ACTrl */
    if(CBC_ACTIVE_rr                    ==1) i|=0x08;	/* Flag 60 : ACTrr */
    if(DETECT_LIMIT_CORNER              ==1) i|=0x10;	/* Flag 61 : Turn  */
    if(DETECT_PARTIAL_BRAKING           ==1) i|=0x20;	/* Flag 62 : Pbrk  */
    if(lccbcu1AyEstErrorSus             ==1) i|=0x40;	/* Flag 63 : AySus */
    if(lccbcu1AyEstErrorDet             ==1) i|=0x80;	/* Flag 64 : AyErr */

    CAN_LOG_DATA[79] = i;               /* 60 */
}
#endif /* #if __CBC */

/*******************************************************************************
* FUNCTION NAME:        LSESP_vCallSLSLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of SLS
********************************************************************************/
#if __SLS
void LSESP_vCallSLSLogData(void)
{
    uint8_t i;
    
    /* ID 0x111, 0x6DC */
    CAN_LOG_DATA[0] = (uint8_t)system_loop_counter;   
    CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);           /* CH 01 : SYS_C */
    CAN_LOG_DATA[2] = (uint8_t)yaw_out;            
    CAN_LOG_DATA[3] = (uint8_t)(yaw_out>>8);                       /* CH 02 : yawm */
    CAN_LOG_DATA[4] = (uint8_t)alat;            
    CAN_LOG_DATA[5] = (uint8_t)(alat>>8);                          /* CH 03 : alat */
    CAN_LOG_DATA[6] = (uint8_t)wstr;          
    CAN_LOG_DATA[7] = (uint8_t)(wstr>>8);                          /* CH 04 : wstr */
	/* ID 0x222, 0x6DD */
    CAN_LOG_DATA[8]  = (uint8_t)FL.vrad_crt;     
    CAN_LOG_DATA[9]  = (uint8_t)(FL.vrad_crt>>8);                  /* CH 05 : vrdFL */
    CAN_LOG_DATA[10] = (uint8_t)FR.vrad_crt;     
    CAN_LOG_DATA[11] = (uint8_t)(FR.vrad_crt>>8);                  /* CH 06 : vrdFR */
    CAN_LOG_DATA[12] = (uint8_t)RL.vrad_crt;     
    CAN_LOG_DATA[13] = (uint8_t)(RL.vrad_crt>>8);                  /* CH 07 : vrdRL */
    CAN_LOG_DATA[14] = (uint8_t)RR.vrad_crt;      
    CAN_LOG_DATA[15] = (uint8_t)(RR.vrad_crt>>8);                  /* CH 08 : vrdRR */
	/* ID 0x333, 0x6DE */ 
    CAN_LOG_DATA[16] = (uint8_t)vref;
    CAN_LOG_DATA[17] = (uint8_t)(vref>>8);                         /* CH 09 : vref */
  #if __4WD || __AX_SENSOR
    CAN_LOG_DATA[18] = (uint8_t)along;
    CAN_LOG_DATA[19] = (uint8_t)(along>>8);                        /* CH 10 : along */
  #else
    CAN_LOG_DATA[18] = (uint8_t)vref5;
    CAN_LOG_DATA[19] = (uint8_t)(vref5>>8);                        /* CH 10 */
  #endif
    CAN_LOG_DATA[20] = (uint8_t)FL.pwm_duty_temp;                  /* CH 11 : PWMfl */
    CAN_LOG_DATA[21] = (uint8_t)FR.pwm_duty_temp;                  /* CH 12 : PWMfr */
    CAN_LOG_DATA[22] = (uint8_t)RL.pwm_duty_temp;                  /* CH 13 : PWMrl */
    CAN_LOG_DATA[23] = (uint8_t)RR.pwm_duty_temp;                  /* CH 14 : PWMrr */
	/* ID 0x444, 0x6DF */ 
    CAN_LOG_DATA[24] = (uint8_t)ebd_filt_grv;					   /* CH 15 */
    CAN_LOG_DATA[25] = (uint8_t)SLS_target_fade_cnt;			   /* CH 16 */
    CAN_LOG_DATA[26] = (uint8_t)SLS_rate_counter_rl;               /* CH 17 */
    CAN_LOG_DATA[27] = (uint8_t)SLS_rate_counter_rr;               /* CH 18 */
    CAN_LOG_DATA[28] = (uint8_t)FL.flags;                          /* CH 19 : flgFL */
    CAN_LOG_DATA[29] = (uint8_t)FR.flags;                          /* CH 20 : flgFR */
    CAN_LOG_DATA[30] = (uint8_t)RL.flags;                          /* CH 21 : flgRL */
    CAN_LOG_DATA[31] = (uint8_t)RR.flags;                          /* CH 22 : flgRR */
	/* ID 0x555, 0x6E0 */
    CAN_LOG_DATA[32] = (uint8_t)RL.rel_lam;						   /* CH 23 : rel_lam */
    CAN_LOG_DATA[33] = (uint8_t)RR.rel_lam;            			   /* CH 24 : rel_lam */
  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[34] = (uint8_t)(lcabss16RefMpress/10);            /* CH 25 : mpress */
  #else
    CAN_LOG_DATA[34] = (uint8_t)(mpress/10);                       /* CH 25 : mpress */
  #endif
    CAN_LOG_DATA[35] = (uint8_t)SLS_fade_counter_rl;			   /* CH 26 : SLS_fade_cnt_rl */
    CAN_LOG_DATA[36] = (uint8_t)SLS_fade_counter_rr;		       /* CH 27 : SLS_fade_cnt_rr */
    CAN_LOG_DATA[37] = (uint8_t)(SLS_target_p_rear/10);            /* CH 28 : tar_P */
    CAN_LOG_DATA[38] = (uint8_t)SLS_target_hold_cnt;               /* CH 29 : SLS_target_hold_cnt */
    CAN_LOG_DATA[39] = (uint8_t)(SLS_target_delta_p_rear/10);      /* CH 30 : del_P */
	/* ID 0x666, 0x6E1 */
    CAN_LOG_DATA[40] = (uint8_t)rf2;                             
    CAN_LOG_DATA[41] = (uint8_t)(rf2>>8);						   /* CH 31 : yawc */
    CAN_LOG_DATA[42] = (uint8_t)delta_moment_q_sls;               
    CAN_LOG_DATA[43] = (uint8_t)(delta_moment_q_sls>>8);       	   /* CH 32 : SLS_Moment */
    CAN_LOG_DATA[44] = (uint8_t)sls_delta_yaw_first2;               
    CAN_LOG_DATA[45] = (uint8_t)(sls_delta_yaw_first2>>8);         /* CH 33 : SLS_dYaw */
    CAN_LOG_DATA[46] = (uint8_t)sls_yaw_error_under_dot2;    
    CAN_LOG_DATA[47] = (uint8_t)(sls_yaw_error_under_dot2>>8);     /* CH 34 : SLS_yaw_Err_dot */
    /* ID 0x117, 0x6E2 */
    CAN_LOG_DATA[48] = (uint8_t)RL.s_diff;						   /* CH 35 : sdiff_RL */
    CAN_LOG_DATA[49] = (uint8_t)RR.s_diff;						   /* CH 36 : sdiff_RR */
    CAN_LOG_DATA[50] = (uint8_t)(((nosign_delta_yaw_first/10)>127)?127:(((nosign_delta_yaw_first/10)<-127)?-127:(int8_t)(nosign_delta_yaw_first/10)));/* CH 37 : del_t */
    CAN_LOG_DATA[51] = (uint8_t)(((nosign_delta_yaw/10)>127)?127:(((nosign_delta_yaw/10)<-127)?-127:(int8_t)(nosign_delta_yaw/10)));                  /* CH 38 : del_y */
    CAN_LOG_DATA[52] = (uint8_t)RL.u8_Estimated_Active_Press;      /* CH 39 : PErl */
    CAN_LOG_DATA[53] = (uint8_t)RR.u8_Estimated_Active_Press;      /* CH 40 : PErr */
    CAN_LOG_DATA[54] = (uint8_t)RL.arad;          				   /* CH 41 : ardRL */
    CAN_LOG_DATA[55] = (uint8_t)RR.arad;          				   /* CH 42 : ardRR */
    /* ID 0x227, 0x6E3 */
    CAN_LOG_DATA[56] = (uint8_t)RL.High_dump_factor;               /* CH 43 : hdfRL */
    CAN_LOG_DATA[57] = (uint8_t)RR.High_dump_factor;               /* CH 44 : hdfRR */
    CAN_LOG_DATA[58] = (uint8_t)FL.u8_Estimated_Active_Press;      /* CH 45 : PEfl */
    CAN_LOG_DATA[59] = (uint8_t)FR.u8_Estimated_Active_Press;      /* CH 46 : PEfr */
    CAN_LOG_DATA[60] = (uint8_t)ldslsEstYawrate1;                   
    CAN_LOG_DATA[61] = (uint8_t)(ldslsEstYawrate1>>8);             /* CH 47 : Est_yaw1 */
    CAN_LOG_DATA[62] = (uint8_t)ldslsEstYawrate2;
    CAN_LOG_DATA[63] = (uint8_t)(ldslsEstYawrate2>>8);             /* CH 48 : Est_yaw2 */
	/* ID 0x337, 0x6E4 */
    CAN_LOG_DATA[64] = (uint8_t)vref_R;       
    CAN_LOG_DATA[65] = (uint8_t)(vref_R>>8);                       /* CH 49 : vrefR */
    CAN_LOG_DATA[66] = (uint8_t)vref_L;                       
    CAN_LOG_DATA[67] = (uint8_t)(vref_L>>8);                       /* CH 50 : vrefL */
    #if(__ABS_CBC_DISABLE==0)
    CAN_LOG_DATA[68] = (uint8_t)lccbcs16EstAy;  
    CAN_LOG_DATA[69] = (uint8_t)(lccbcs16EstAy>>8);                /* CH 51 */
    CAN_LOG_DATA[70] = (uint8_t)lccbcs16EstAx;  
    CAN_LOG_DATA[71] = (uint8_t)(lccbcs16EstAx>>8);                /* CH 52 */
    #endif
    
	/* ID 0x447, 0x6E5 */
    i=0;                                  
    if (BLS                             ==1) i|=0x01;	/* Flag 01 : BLS   */
    if (ABS_fz                          ==1) i|=0x02;	/* Flag 02 : ABSfz */
    if (AFZ_OK                          ==1) i|=0x04;	/* Flag 03 : AFZOK */
    if (EBD_RA                          ==1) i|=0x08;	/* Flag 04 : EBD   */
    if (BTC_fz                          ==1) i|=0x10;	/* Flag 05 : BTC_O */
    if (YAW_CDC_WORK                    ==1) i|=0x20;	/* Flag 06 : Y_C_W */
    if (CBC_ON                         ==1) i|=0x40;	/* Flag 07 : CBC_on */
    if (SLS_ENABLE                      ==1) i|=0x80;	/* Flag 08 : SLS_enable */

    CAN_LOG_DATA[72] = i;               /* 53 */
    
    i=0;                                  
    if (MOT_ON_FLG                      ==1) i|=0x01;	/* Flag 09 : moton */
    if (SLS_ON                          ==1) i|=0x02;	/* Flag 10 : SLS_on */
    if (SLS_ACTIVE_rl                   ==1) i|=0x04;	/* Flag 11 : Actrl */
    if (SLS_ACTIVE_rr                   ==1) i|=0x08;	/* Flag 12 : Actrr */
    if (SLS_FADE_OUT_rl                 ==1) i|=0x10;	/* Flag 13 : FADrl */
    if (SLS_FADE_OUT_rr                 ==1) i|=0x20;	/* Flag 14 : FADrr */
    if (SLS_PARTIAL_BRAKING             ==1) i|=0x40;	/* Flag 15 : Pbrk */
    if (SLS_STRAIGHT_CONDITION          ==1) i|=0x80;	/* Flag 16 : Straight */

    CAN_LOG_DATA[73] = i;               /* 54 */

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;	/* Flag 17 : ABSfl */
    if (CBC_ENABLE_fl                   ==1) i|=0x02;	/* Flag 18 */
    if (Rough_road_detect_vehicle_EBD   ==1) i|=0x04;	/* Flag 19 : roughEBD */
    if (lcebdu1RoughSlightBrk           ==1) i|=0x08;	/* Flag 20 : RslightEBD */
    if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10;	/* Flag 21 : ESPfl */
    if (GMA_SLIP_fl                     ==1) i|=0x20;	/* Flag 22 */
    if (HV_VL_fl                        ==1) i|=0x40;	/* Flag 23 : HV_fl */
    if (AV_VL_fl                        ==1) i|=0x80;	/* Flag 24 : AV_fl */

    CAN_LOG_DATA[74] = i;               /* 55 */
    
    i=0;                                          
    if (ABS_fr                          ==1) i|=0x01;	/* Flag 25 : ABSfr */
    if (CBC_ENABLE_fr                   ==1) i|=0x02;	/* Flag 26 */
    if (STABIL_fr                       ==1) i|=0x04;	/* Flag 27 */
    if (SPOLD_RESET_fr                  ==1) i|=0x08;	/* Flag 28 */
    if (ESP_BRAKE_CONTROL_FR            ==1) i|=0x10;	/* Flag 29 : ESPfr */
    if (GMA_SLIP_fr                     ==1) i|=0x20;	/* Flag 30 */
    if (HV_VL_fr                        ==1) i|=0x40;	/* Flag 31 : HV_fr */
    if (AV_VL_fr                        ==1) i|=0x80;	/* Flag 32 : AV_fr */

    CAN_LOG_DATA[75] = i;               /* 56 */
    
    i=0;                                   
    if (ABS_rl                          ==1) i|=0x01;	/* Flag 33 : ABSrl */
    if (CBC_ENABLE_rl                   ==1) i|=0x02;	/* Flag 34 */
    if (STABIL_rl                       ==1) i|=0x04;	/* Flag 35 */
    if (SPOLD_RESET_rl                  ==1) i|=0x08;	/* Flag 36 */
    if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x10;	/* Flag 37 : ESPrl */
    if (RL.MSL_BASE                     ==1) i|=0x20;	/* Flag 38 */
    if (HV_VL_rl                        ==1) i|=0x40;	/* Flag 39 : HV_rl */
    if (AV_VL_rl                        ==1) i|=0x80;	/* Flag 40 : AV_rl */

    CAN_LOG_DATA[76] = i;               /* 57 */

    i = 0;
    if(ABS_rr                           ==1) i|=0x01;	/* Flag 41 : ABSrr */
    if(CBC_ENABLE_rr                    ==1) i|=0x02;	/* Flag 42 */
    if(STABIL_rr                        ==1) i|=0x04;	/* Flag 43 */
    if(SPOLD_RESET_rr                   ==1) i|=0x08;	/* Flag 44 */
    if(ESP_BRAKE_CONTROL_RR             ==1) i|=0x10;	/* Flag 45 : ESPrr */
    if(RR.MSL_BASE                      ==1) i|=0x20;	/* Flag 46 */
    if(HV_VL_rr                         ==1) i|=0x40;	/* Flag 47 : HV_rr */
    if(AV_VL_rr                         ==1) i|=0x80;	/* Flag 48 : AV_rr */

    CAN_LOG_DATA[77] = i;               /* 58 */

    i=0;
    if(TCL_DEMAND_fl                    ==1) i|=0x01;	/* Flag 49 : TCLfl */
    if(TCL_DEMAND_fr                    ==1) i|=0x02;	/* Flag 50 : TCLfr */
    if(S_VALVE_LEFT                     ==1) i|=0x04;	/* Flag 51 : S_V_L */
    if(S_VALVE_RIGHT                    ==1) i|=0x08;	/* Flag 52 : S_V_R */
    if(lcebdu1Decel_rl             		==1) i|=0x10;	/* Flag 53 : Decel_rl */
    if(lcebdu1Slip_rl             		==1) i|=0x20;	/* Flag 54 : Slip_rl  */
    if(lcebdu1Arad_rl                 	==1) i|=0x40;	/* Flag 55 : Arad_rl  */
    if(lcebdu1Decel_rr            		==1) i|=0x80;	/* Flag 56 : Decel_rr */        
    
    CAN_LOG_DATA[78] = i;               /* 59 */
    
     i=0;
    if(CBC_FADE_OUT_rl                  ==1) i|=0x01;	/* Flag 57 */
    if(CBC_FADE_OUT_rr                  ==1) i|=0x02;	/* Flag 58 */
    if(lcebdu1Slip_rr                   ==1) i|=0x04;	/* Flag 59 : Slip_rr */
    if(lcebdu1Arad_rr                   ==1) i|=0x08;	/* Flag 60 : Arad_rr */
    if(SLS_FADE_OUT_ENABLE              ==1) i|=0x10;	/* Flag 61 : Fadeout_enable */
    if(SLS_INHIBIT_ESC_FLAG             ==1) i|=0x20;	/* Flag 62 : Inhibit_ESC */
    if(SLS_INHIBIT_YAWG_FLAG            ==1) i|=0x40;	/* Flag 63 : Inhibit_yawG */
    if(SLS_YAWG_FAIL_FADE_FLAG          ==1) i|=0x80;	/* Flag 64 : YawG_Fail_Fail_Fade */

    CAN_LOG_DATA[79] = i;               /* 60 */
}
#endif /* #if __SLS */

#if __TSP
/*******************************************************************************
* FUNCTION NAME:        LSTCS_vCallTSPLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of CAN Check
********************************************************************************/
void LSESP_vCallTSPLogData(void)
{
    uint8_t i;
    CAN_LOG_DATA[0] = (uint8_t)(system_loop_counter);															//ch1				
    CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);                     			
    CAN_LOG_DATA[2] = (uint8_t)(vrad_fl);                                    			//ch2
    CAN_LOG_DATA[3] = (uint8_t)(vrad_fl>>8);                                 			
    CAN_LOG_DATA[4] = (uint8_t)(vrad_fr);                                    			//ch3
    CAN_LOG_DATA[5] = (uint8_t)(vrad_fr>>8);                                 			
    CAN_LOG_DATA[6] = (uint8_t)(vrad_rl);                                    			//ch4
    CAN_LOG_DATA[7] = (uint8_t)(vrad_rl>>8);                                 			
                                                                  			
    CAN_LOG_DATA[8]  = (uint8_t)(vrad_rr);                                   			//ch5                                               
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_rr>>8);                                			
    CAN_LOG_DATA[10] = (uint8_t)(vref5);                                     			//ch6
    CAN_LOG_DATA[11] = (uint8_t)(vref5>>8);                                  			
    CAN_LOG_DATA[12] = (uint8_t)(tsp_yawc_first);                            			//ch7
    CAN_LOG_DATA[13] = (uint8_t)(tsp_yawc_first>>8);                         			
    CAN_LOG_DATA[14] = (uint8_t)(tsp_delta_yaw_first);                       			//ch8
    CAN_LOG_DATA[15] = (uint8_t)(tsp_delta_yaw_first>>8);                    			
                                                                  			
    CAN_LOG_DATA[16] = (uint8_t)(tsp_detect_oscil_count);                    			//ch9
    CAN_LOG_DATA[17] = (uint8_t)(tsp_osci_mitig_count);                      			//ch10
    CAN_LOG_DATA[18] = (uint8_t)(tsp_osci_mitig_count>>8);   	              			
    CAN_LOG_DATA[19] = (uint8_t)(tsp_yawc_count);                            			//ch11 
    CAN_LOG_DATA[20] = (uint8_t)(tsp_yawc_count>>8);                         			
    CAN_LOG_DATA[21] = (uint8_t)(tsp_dyaw_count);                            			//ch12
    CAN_LOG_DATA[22] = (uint8_t)(tsp_dyaw_count>>8);                         			
    CAN_LOG_DATA[23] = (uint8_t)(tsp_inhibit_work_count);   											//ch13
                                                                  			
    CAN_LOG_DATA[24] = (uint8_t)(wstr);                                      			//ch14
    CAN_LOG_DATA[25] = (uint8_t)(wstr>>8);                                   			  
    CAN_LOG_DATA[26] = (uint8_t)(det_alatc);                                 			//ch15
    CAN_LOG_DATA[27] = (uint8_t)(det_alatc>>8);                              			      
    CAN_LOG_DATA[28] = (uint8_t)(tsp_yaw_threshold);                         			//ch16
    CAN_LOG_DATA[29] = (uint8_t)(tsp_yaw_threshold>>8);                      			
    CAN_LOG_DATA[30] = (uint8_t)(tsp_yawc_thr);             											//ch17
    CAN_LOG_DATA[31] = (uint8_t)(tsp_yawc_thr>>8);                           			
                                                                  			
    CAN_LOG_DATA[32] = (uint8_t)(tsp_delta_yaw_dot);                         			//ch18
    CAN_LOG_DATA[33] = (uint8_t)(tsp_delta_yaw_dot>>8);                      			        
    CAN_LOG_DATA[34] = (uint8_t)(target_vol);																			//ch19
    CAN_LOG_DATA[35] = (uint8_t)(target_vol>>8);                              			
    CAN_LOG_DATA[36] = (uint8_t)(cal_torq);  					                      			//ch20
    CAN_LOG_DATA[37] = (uint8_t)(cal_torq>>8); 	                            			    
    CAN_LOG_DATA[38] = (uint8_t)(eng_torq); 									                		//ch21
    CAN_LOG_DATA[39] = (uint8_t)(eng_torq>>8);                               			
                                                                  			
    CAN_LOG_DATA[40] = (uint8_t)(tsp_torq);        																//ch22
    CAN_LOG_DATA[41] = (uint8_t)(tsp_torq>>8);                                                                              
    CAN_LOG_DATA[42] = (uint8_t)(drive_torq);																			//ch23                                                                                                                                          
    CAN_LOG_DATA[43] = (uint8_t)(drive_torq>>8);
  #if __G_SENSOR_ENABLE
    CAN_LOG_DATA[44] =(uint8_t) (along);                         									//ch24
    CAN_LOG_DATA[45] =(uint8_t) (along>>8);
  #else
    CAN_LOG_DATA[44] = (uint8_t)(lcs16DecDecel);                         					//ch24                                                                       
    CAN_LOG_DATA[45] = (uint8_t)(lcs16DecDecel>>8);
  #endif  

  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[46] = (uint8_t)(lsesps16AHBGEN3mpress);               //ch25
    CAN_LOG_DATA[47] = (uint8_t)(lsesps16AHBGEN3mpress>>8);
  #else
    CAN_LOG_DATA[46] = (uint8_t)(mpress);  																				//ch25
    CAN_LOG_DATA[47] = (uint8_t)(mpress>>8);
  #endif
    CAN_LOG_DATA[48] = (uint8_t)(tsp_max_amplitude_old/10);      									//ch26
    CAN_LOG_DATA[49] = (uint8_t)(tsp_yaw_threshold_max_amp_old/10);     					//ch27                                                               
    CAN_LOG_DATA[50] = (uint8_t)(tsp_brake_work_count);                           //ch28                  
    CAN_LOG_DATA[51] = (uint8_t)(tsp_brake_work_count>>8);                                                
    CAN_LOG_DATA[52] = (uint8_t)(tsp_plus_dyaw_peak_count);                       //ch29                     
    CAN_LOG_DATA[53] = (uint8_t)(tsp_plus_dyaw_peak_count>>8);                                               
  	CAN_LOG_DATA[54] = (uint8_t)(tsp_minus_dyaw_peak_count);                      //ch30	 						
		CAN_LOG_DATA[55] = (uint8_t)(tsp_minus_dyaw_peak_count>>8);
	                                        
  	CAN_LOG_DATA[56] = (uint8_t)(tsp_freq_rate_2nd_3rd);      										//ch31
    CAN_LOG_DATA[57] = (uint8_t)(tsp_freq_rate_3rd_4th);                          //ch32                   
    CAN_LOG_DATA[58] = (uint8_t)(tsp_fade_ctrl_cnt);                              //ch33
    CAN_LOG_DATA[59] = (uint8_t)(tsp_fade_ctrl_cnt>>8);                                                                                                         
    CAN_LOG_DATA[60] = (uint8_t)(Delta_moment_tsp);                               //ch34
    CAN_LOG_DATA[61] = (uint8_t)(Delta_moment_tsp>>8);                                                
    CAN_LOG_DATA[62] = (uint8_t)(mtp);                   													//ch35
    CAN_LOG_DATA[63] = (uint8_t)(lcs16TspTargetDecelReal/10); 										//ch36
                                                                                                                       
    CAN_LOG_DATA[64] = (inhibition_number=INHIBITION_INDICATOR());								//ch37
    
    i=0;
    if (TSP_ON                 		 		==1) i|=0x01;						  									//flag1
    if (YAW_CDC_WORK               		==1) i|=0x02;                     					//flag2
    if (ABS_ON                     		==1) i|=0x04;                     					//flag3
    if (BTCS_ON       						 		==1) i|=0x08;    														//flag4
    if (ETCS_ON                    		==1) i|=0x10;                     					//flag5
    if (FTCS_ON                    		==1) i|=0x20;                     					//flag6
    if (BLS                        		==1) i|=0x40;                     					//flag7
  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn    							==1) i|=0x80;                     //flag8
  #else
    if (MPRESS_BRAKE_ON            		==1) i|=0x80;                     					//flag8
    CAN_LOG_DATA[65] = i;          		                                  					
  #endif                                   							                                  
    i=0;                           		                                  					
    if (TSP_BRAKE_CTRL_ON          		==1) i|=0x01;						  									//flag9
    if (U1_TSP_BRAKE_CTRL_ON_READY    ==1) i|=0x02;                     					//flag10
    if (TSP_DIFF_ONLY_CTRL_ACT      	==1) i|=0x04;                     					//flag11
    if (U1_TSP_BRAKE_CTRL_ACT      		==1) i|=0x08;                     					//flag12
    if (U1_TSP_FADE_CTRL_ON        		==1) i|=0x10;                     					//flag13
    if (TSP_ENG_CTRL_ON            		==1) i|=0x20;                     					//flag14
    if (U1_TSP_DEC_DIFF_CTRL_ACT      ==1) i|=0x40;                     					//flag15
    if (TSP_MSC_MOTOR_ON           		==1) i|=0x80;                     					//flag16
    CAN_LOG_DATA[66] = i;          		                                  					
                                   		                                  					
   i=0;                            		                                  					
    if (HV_VL_fl                   		==1) i|=0x01;						  									//flag17
    if (AV_VL_fl                   		==1) i|=0x02;                     					//flag18
    if (HV_VL_fr                   		==1) i|=0x04;                     					//flag19
    if (AV_VL_fr                   		==1) i|=0x08;                     					//flag20
    if (HV_VL_rl                   		==1) i|=0x10;                     					//flag21
    if (AV_VL_rl                   		==1) i|=0x20;                     					//flag22
    if (HV_VL_rr               		 		==1) i|=0x40;                     					//flag23
    if (AV_VL_rr              		 		==1) i|=0x80;                     					//flag24
    CAN_LOG_DATA[67] = i;          		                                  					
                                   		                                  					
    i=0;                           		                                  					
#if !__SPLIT_TYPE     	           		                                  					            
    if (S_VALVE_LEFT               		==1) i|=0x01;                     					//flag25 
    if (S_VALVE_RIGHT              		==1) i|=0x02;                     					//flag26
    if (TCL_DEMAND_fl              		==1) i|=0x04;                     					//flag27
    if (TCL_DEMAND_fr              		==1) i|=0x08;                     					//flag28    	 
#else                              		                                  					            
    if (S_VALVE_PRIMARY            		==1) i|=0x01;                     					//flag25 
    if (S_VALVE_SECONDARY          		==1) i|=0x02;                     					//flag26
    if (TCL_DEMAND_PRIMARY         		==1) i|=0x04;                     					//flag27
    if (TCL_DEMAND_SECONDARY       		==1) i|=0x08;                     					//flag28	 
#endif    	  	                   		                                  					            
    if (TSP_PLUS_YAWC_THR          		==1) i|=0x10;                     					//flag29
    if (TSP_MINUS_YAWC_THR         		==1) i|=0x20;                     					//flag30
    if (TSP_PLUS_DYAW_THR          		==1) i|=0x40;                     					//flag31
    if (TSP_MINUS_DYAW_THR         		==1) i|=0x80;                     					//flag32
    CAN_LOG_DATA[68] = i;          		                                  					
                                   		                                  					
    i=0;                           		                                  					
    if (U1_TSP_ADD_TC_CURRENT_P    		==1) i|=0x01;						  									//flag33
    if (U1_TSP_ADD_TC_CURRENT_S    		==1) i|=0x02;                     					//flag34
    if (U1_TSP_FADE_CTRL_MONITOR      ==1) i|=0x04;                     					//flag35                                                                    
    if (U1_TSP_DIFF_To_DEC_ON	     		==1) i|=0x08;                     					//flag36                                                                       
    if (SLIP_CONTROL_NEED_FL       		==1) i|=0x10;                     					//flag37
    if (SLIP_CONTROL_NEED_FR       		==1) i|=0x20;                     					//flag38
    if (SLIP_CONTROL_NEED_RL       		==1) i|=0x40;                     					//flag39
    if (SLIP_CONTROL_NEED_RR       		==1) i|=0x80;                     					//flag40
    CAN_LOG_DATA[69] = i;          		                                  					
                                   		                                  					
    i = 0;                         		                                  					
    if (ESP_ERROR_FLG              		==1) i|=0x01;						  									//flag41
    if (vdc_on_flg                 		==1) i|=0x02;                     					//flag42
    if (VDC_DISABLE_FLG       		 		==1) i|=0x04;                     					//flag43
    if (tc_error_flg  						 		==1) i|=0x08;                     					//flag44
    if (tcs_error_flg         		 		==1) i|=0x10;               								//flag45
    if (cbit_process_step   			 		==1) i|=0x20;             									//flag46
    if (AY_ON       							 		==1) i|=0x40;                 							//flag47
    if (ESP_TCS_ON                 		==1) i|=0x80;                     					//flag48
    CAN_LOG_DATA[70] = i;          		                                  					
                                   		                                  					
    i = 0;                         		                                  					
	  if (TSP_PLUS_DYAW_PEAK_DETECT       				==1) i|=0x01;						  				//flag49
    if (TSP_MINUS_DYAW_PEAK_DETECT  						==1) i|=0x02;                     //flag50
    if (FLAG_BANK_DETECTED       	 		==1) i|=0x04;                     					//flag51
    if (FLAG_BANK_SUSPECT  				 		==1) i|=0x08;                     					//flag52
    if (U1_TSP_TRAILER_CONNECT_ON								==1) i|=0x10;               			//flag53
    if (U1_TSP_FIRST_DIFF_CTRL_DETECT ==1) i|=0x20;             									//flag54
    if (U1_TSP_CONTROL_MODE_INHIBIT		==1) i|=0x40;                 							//flag55
    if (U1_TSP_FRQ_FAIL  												==1) i|=0x80;                     //flag56
    CAN_LOG_DATA[71] = i;

    CAN_LOG_DATA[72] = (uint8_t)(lcs16TspMFCCurP/10);															//ch45
    CAN_LOG_DATA[73] = (uint8_t)(lcs16TspMFCCurS/10);                       			//ch46
    CAN_LOG_DATA[74] = (uint8_t)(lcs16TspDiffCtrlAddCurP/10);											//ch47
    CAN_LOG_DATA[75] = (uint8_t)(lcs16TspDiffCtrlAddCurS/10);											//ch48						      
    CAN_LOG_DATA[76] = (uint8_t)(tsp_amp_damping_ratio_1st_3rd);									//ch49
    CAN_LOG_DATA[77] = (uint8_t)(tsp_amp_damping_ratio_1st_3rd>>8);
		CAN_LOG_DATA[78] = (uint8_t)(tsp_amp_damping_ratio_2nd_4th);									//ch50
    CAN_LOG_DATA[79] = (uint8_t)(tsp_amp_damping_ratio_2nd_4th>>8);

}
#endif /* #if __TSP */


#if __TVBB
void LSESP_vCallTVBBLogData(void)// TVBB, 2011.06.20 
{
    CAN_LOG_DATA[0 ] =(uint8_t) (system_loop_counter);            /*ch01*/                    
    CAN_LOG_DATA[1 ] =(uint8_t) (system_loop_counter>>8); 
    CAN_LOG_DATA[2 ] =(uint8_t) (vrad_fl);                        /*ch02*/        
    CAN_LOG_DATA[3 ] =(uint8_t) (vrad_fl>>8);
    CAN_LOG_DATA[4 ] =(uint8_t) (vrad_fr);                        /*ch03*/        
    CAN_LOG_DATA[5 ] =(uint8_t) (vrad_fr>>8);
    CAN_LOG_DATA[6 ] =(uint8_t) (vrad_rl);                        /*ch04*/        
    CAN_LOG_DATA[7 ] =(uint8_t) (vrad_rl>>8);
    CAN_LOG_DATA[8 ] =(uint8_t) (vrad_rr);                        /*ch05*/        
    CAN_LOG_DATA[9 ] =(uint8_t) (vrad_rr>>8);
    CAN_LOG_DATA[10] =(uint8_t) (vref5);                          /*ch06*/
    CAN_LOG_DATA[11] =(uint8_t) (vref5>>8);
    CAN_LOG_DATA[12] =(uint8_t) (alat);       
    CAN_LOG_DATA[13] =(uint8_t) (alat>>8);
    CAN_LOG_DATA[14] =(uint8_t) (det_alatc_fltr);     
    CAN_LOG_DATA[15] =(uint8_t) (det_alatc_fltr>>8);

    CAN_LOG_DATA[16] =(uint8_t) (det_alatm_fltr);   
    CAN_LOG_DATA[17] =(uint8_t) (det_alatm_fltr>>8);
    CAN_LOG_DATA[18] =(uint8_t) (esp_mu/10);
    CAN_LOG_DATA[19] =(uint8_t) (det_alatm/10);
    CAN_LOG_DATA[20] =(uint8_t) (wstr);   
    CAN_LOG_DATA[21] =(uint8_t) (wstr>>8);
    CAN_LOG_DATA[22] =(uint8_t) (rf);   
    CAN_LOG_DATA[23] =(uint8_t) (rf>>8);

    CAN_LOG_DATA[24] =(uint8_t) (rf2);   
    CAN_LOG_DATA[25] =(uint8_t) (rf2>>8);
    CAN_LOG_DATA[26] =(uint8_t) (yaw_out);   
    CAN_LOG_DATA[27] =(uint8_t) (yaw_out>>8);
    CAN_LOG_DATA[28] =(uint8_t) (nosign_delta_yaw);   
    CAN_LOG_DATA[29] =(uint8_t) (nosign_delta_yaw>>8);
    CAN_LOG_DATA[30] =(uint8_t) (nosign_delta_yaw_first);   
    CAN_LOG_DATA[31] =(uint8_t) (nosign_delta_yaw_first>>8);

    CAN_LOG_DATA[32] =(uint8_t) (sign_o_delta_yaw_thres/10);
    CAN_LOG_DATA[33] =(uint8_t) (yaw_error_under_dot);   
    CAN_LOG_DATA[34] =(uint8_t) (yaw_error_under_dot>>8);
    CAN_LOG_DATA[35] =(uint8_t) (mtp);   
    CAN_LOG_DATA[36] =(uint8_t) (drive_torq);   
    CAN_LOG_DATA[37] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[38] =(uint8_t) (tvbb_torq);   
    CAN_LOG_DATA[39] =(uint8_t) (tvbb_torq>>8);
    
    CAN_LOG_DATA[40] =(uint8_t) (eng_torq);   
    CAN_LOG_DATA[41] =(uint8_t) (eng_torq>>8);
    CAN_LOG_DATA[42] =(uint8_t) (gear_pos);
    #if __TOD
    CAN_LOG_DATA[43] =(uint8_t) (lespu16AWD_4WD_TQC_CUR);      
    CAN_LOG_DATA[44] =(uint8_t) (lespu16AWD_4WD_TQC_CUR>>8);
    CAN_LOG_DATA[45] =(uint8_t) (TOD4wdTqcLimTorq);    
    CAN_LOG_DATA[46] =(uint8_t) (TOD4wdTqcLimTorq>>8);
    #else
    CAN_LOG_DATA[43] =(uint8_t) (delta_moment_q_os);
    CAN_LOG_DATA[44] =(uint8_t) (delta_moment_q_os>>8);
    CAN_LOG_DATA[45] =(uint8_t) (0);   
    CAN_LOG_DATA[46] =(uint8_t) (0);   
    #endif 
    #if __4WD_VARIANT_CODE == ENABLE
    CAN_LOG_DATA[47] =(uint8_t) (lsu8DrvMode);
    #else
    CAN_LOG_DATA[47] =(uint8_t) (0);
    #endif

    CAN_LOG_DATA[48] =(uint8_t) (mpress);   
    CAN_LOG_DATA[49] =(uint8_t) (mpress>>8);
    CAN_LOG_DATA[50] =(uint8_t) (inhibition_number =INHIBITION_INDICATOR());
    CAN_LOG_DATA[51] =(uint8_t) (ldtvbbu8InhibitID);
    CAN_LOG_DATA[52] =(uint8_t) (ldtvbbs16SlipRatioOfCtlWhl);   
    CAN_LOG_DATA[53] =(uint8_t) (ldtvbbs16SlipRatioOfCtlWhl>>8);
    CAN_LOG_DATA[54] =(uint8_t) (ldtvbbs16EnterSpeedLimit/8);
    CAN_LOG_DATA[55] =(uint8_t) (lctvbbs8ControlEnterTimer);

    CAN_LOG_DATA[56] =(uint8_t) (lctvbbu8ControlExitTimer);
    CAN_LOG_DATA[57] =(uint8_t) (lctvbbu16TVBBCtlRunningCnt);
    CAN_LOG_DATA[58] =(uint8_t) (lctvbbs16ControlMomentPGain);         
    CAN_LOG_DATA[59] =(uint8_t) (lctvbbs16ControlMomentDGain);
    CAN_LOG_DATA[60] =(uint8_t) (lctvbbs16ControlMoment);      
    CAN_LOG_DATA[61] =(uint8_t) (lctvbbs16ControlMoment>>8);
    CAN_LOG_DATA[62] =(uint8_t) (MFC_PWM_DUTY_P);
    CAN_LOG_DATA[63] =(uint8_t) (MFC_PWM_DUTY_S);

    CAN_LOG_DATA[64] =(uint8_t) (target_vol/100);  
    CAN_LOG_DATA[65] =(uint8_t) (fu16CalVoltMOTOR/100);  
    CAN_LOG_DATA[66] =(uint8_t) (delta_moment_q_under);   
    CAN_LOG_DATA[67] =(uint8_t) (delta_moment_q_under>>8);
    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW1 = del_target_slip_fl;
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW1 = del_target_slip_fr;
    }
    else
    {
        esp_tempW1 = 0;
    }
    CAN_LOG_DATA[68] =(uint8_t) (esp_tempW1);      
    CAN_LOG_DATA[69] =(uint8_t) (esp_tempW1>>8);
    if(SLIP_CONTROL_NEED_RL==1)
    {
        esp_tempW2 = del_target_slip_rl;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
        esp_tempW2 = del_target_slip_rr;
    }
    else
    {
        esp_tempW2 = 0;
    }
    CAN_LOG_DATA[70] =(uint8_t) (esp_tempW2);   
    CAN_LOG_DATA[71] =(uint8_t) (esp_tempW2>>8);

    CAN_LOG_DATA[72] =(uint8_t) (Beta_MD);   
    CAN_LOG_DATA[73] =(uint8_t) (Beta_MD>>8);
    #if __4WD ||__AX_SENSOR
	esp_tempW9= along;
    #else
	esp_tempW9= afz;
    #endif
    if(esp_tempW9 > 120) 
    {
        esp_tempW9 =120;
    }
    else if(esp_tempW9< (-120)) 
    {
        esp_tempW9 = -120;
    }
    else
    {
        ;
    }
    CAN_LOG_DATA[74] =(uint8_t) (esp_tempW9);                     /*ch07*/
 
    tempB0=tempB1=tempB2=tempB3=tempB4=0;

    if(ESP_ERROR_FLG                        ) tempB0|=0x01;
    if(EBD_RA                               ) tempB0|=0x02;
    if(ABS_fz                               ) tempB0|=0x04;
    if(BTC_fz                               ) tempB0|=0x08;
    if(ETCS_ON                              ) tempB0|=0x10; 
    if(ESP_TCS_ON                           ) tempB0|=0x20; 
    if(YAW_CDC_WORK                         ) tempB0|=0x40; 
    if(lctvbbu1TVBB_ON                      ) tempB0|=0x80;
    
    if(vdc_on_flg                           ) tempB1|=0x01;
    if(VDC_DISABLE_FLG                      ) tempB1|=0x02;
    if(tc_error_flg                         ) tempB1|=0x04;
    if(tcs_error_flg                        ) tempB1|=0x08;
    if(fu1ESCDisabledBySW                   ) tempB1|=0x10;
    if(ldtvbbu1InhibitFlag                  ) tempB1|=0x20;
    if(ldtvbbu1FrontWhlCtl                  ) tempB1|=0x40;
    if(lcu1espActive3rdFlg                  ) tempB1|=0x80;
    
    if(ESP_BRAKE_CONTROL_FL                 ) tempB2|=0x01;
    if(ESP_BRAKE_CONTROL_FR                 ) tempB2|=0x02;
    if(ESP_BRAKE_CONTROL_RL                 ) tempB2|=0x04;
    if(ESP_BRAKE_CONTROL_RR                 ) tempB2|=0x08;
    if(ldtvbbu1ExitByWstrDot                ) tempB2|=0x10;
    if(ldtvbbu1WstrDotDirChange             ) tempB2|=0x20;
    if(ldtvbbu1AccelPedalPushFlag           ) tempB2|=0x40;
    if(ldtvbbu1TurnIntentDetectFlag         ) tempB2|=0x80;
    
    if(AV_VL_fl                             ) tempB3|=0x01;
    if(HV_VL_fl                             ) tempB3|=0x02;
    if(AV_VL_fr                             ) tempB3|=0x04;
    if(HV_VL_fr                             ) tempB3|=0x08;
    if(AV_VL_rl                             ) tempB3|=0x10;
    if(HV_VL_rl                             ) tempB3|=0x20;
    if(AV_VL_rr                             ) tempB3|=0x40;
    if(HV_VL_rr                             ) tempB3|=0x80;

    #if !__SPLIT_TYPE
    if(TCL_DEMAND_fl                        ) tempB4|=0x01;
    #else
    if(TCL_DEMAND_SECONDARY                 ) tempB4|=0x01;
    #endif
    #if !__SPLIT_TYPE
    if(TCL_DEMAND_fr                        ) tempB4|=0x02;
    #else
    if(TCL_DEMAND_PRIMARY                   ) tempB4|=0x02;
    #endif
    #if !__SPLIT_TYPE
    if(S_VALVE_LEFT                         ) tempB4|=0x04;
    #else
    if(S_VALVE_SECONDARY                    ) tempB4|=0x04;
    #endif
    #if !__SPLIT_TYPE
    if(S_VALVE_RIGHT                        ) tempB4|=0x08;
    #else
    if(S_VALVE_PRIMARY                      ) tempB4|=0x08;
    #endif
    if(lctvbbu1LeftWhlCtl                   ) tempB4|=0x10;    
    if(lctvbbu1RightWhlCtl                  ) tempB4|=0x20; 
    if(In_gear_flag                         ) tempB4|=0x40;    
    if(ROP_REQ_ACT                          ) tempB4|=0x80;    
     
    
    CAN_LOG_DATA[75] =(uint8_t) (tempB0);
    CAN_LOG_DATA[76] =(uint8_t) (tempB1);
    CAN_LOG_DATA[77] =(uint8_t) (tempB2);
    CAN_LOG_DATA[78] =(uint8_t) (tempB3);
    CAN_LOG_DATA[79] =(uint8_t) (tempB4);

}
#endif

#if defined(__dDCT_INTERFACE)
void LSHRC_vCall_LogData(void)
{
	UCHAR i;

    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (UCHAR)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (UCHAR)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (UCHAR)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (UCHAR)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (UCHAR)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (UCHAR)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (UCHAR)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (UCHAR)(vrad_rr>>8);
    CAN_LOG_DATA[10] = (UCHAR)(vref);                               /* 06 */
    CAN_LOG_DATA[11] = (UCHAR)(vref>>8);
    CAN_LOG_DATA[12] = (UCHAR)(lcu8HrcGearState);              			/* 07 */
    CAN_LOG_DATA[13] = (UCHAR)(lcs8DecState);         				/* 08 */
    CAN_LOG_DATA[14] = (UCHAR)(lcs16DecTargetG);             /* 09 */
    CAN_LOG_DATA[15] = (UCHAR)(lcs16DecTargetG>>8);
    CAN_LOG_DATA[16] = (UCHAR)(HSA_Control);                /* 10 */
    CAN_LOG_DATA[17] = (UCHAR)(lcs8HdcState);            /* 11 */
    CAN_LOG_DATA[18] = (UCHAR)(0);                    /* 12 */
    CAN_LOG_DATA[19] = (UCHAR)(0);               /* 13 */
    CAN_LOG_DATA[20] = (UCHAR)(HSA_phase_out_rate);            /* 14 */
    CAN_LOG_DATA[21] = (UCHAR)(lcs16HrcReqHsaActMinSlope);               /* 15 */
    CAN_LOG_DATA[22] = (UCHAR)(lcu8DecMotorMode);                  /* 16 */
    CAN_LOG_DATA[23] = (UCHAR)(lcu8DecValveMode);                    /* 17 */
    CAN_LOG_DATA[24] = (UCHAR)(TC_MFC_Voltage_Primary_dash);        /* 18 */
    CAN_LOG_DATA[25] = (UCHAR)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[26] = (UCHAR)(TC_MFC_Voltage_Secondary_dash);      /* 19 */
    CAN_LOG_DATA[27] = (UCHAR)(TC_MFC_Voltage_Secondary_dash>>8);
    CAN_LOG_DATA[28] = (UCHAR)(target_vol);                     /* 20 */
    CAN_LOG_DATA[29] = (UCHAR)(target_vol>>8);
    CAN_LOG_DATA[30] = (UCHAR)(fu16CalVoltMOTOR);                  /* 21 */
    CAN_LOG_DATA[31] = (UCHAR)(fu16CalVoltMOTOR>>8);
    CAN_LOG_DATA[32] = (UCHAR)(lcu16DecFeedForwardCnt);                  /* 22 */
    CAN_LOG_DATA[33] = (UCHAR)(lcu16DecFeedForwardCnt>>8);
    CAN_LOG_DATA[34] = (UCHAR)(lcu16DecFeedForwardTh);                /* 23 */
    CAN_LOG_DATA[35] = (UCHAR)(lcu16DecFeedForwardTh>>8);
    CAN_LOG_DATA[36] = (UCHAR)(mpress);                             /* 24 */
    CAN_LOG_DATA[37] = (UCHAR)(mpress>>8);
    CAN_LOG_DATA[38] = (UCHAR)(gs_sel);                             /* 25 */
    CAN_LOG_DATA[39] = (UCHAR)(mtp);                                /* 26 */
    CAN_LOG_DATA[40] = (UCHAR)(wstr);                               /* 27 */
    CAN_LOG_DATA[41] = (UCHAR)(wstr>>8);
    CAN_LOG_DATA[42] = (UCHAR)(yaw_out);                            /* 28 */
    CAN_LOG_DATA[43] = (UCHAR)(yaw_out>>8);
    CAN_LOG_DATA[44] = (UCHAR)(lcs16HdcThetaG);                    /* 29 */
    CAN_LOG_DATA[45] = (UCHAR)(lcs16HdcThetaG>>8);
    CAN_LOG_DATA[46] = (UCHAR)(lcs16HrcTargetSpd);                      /* 30 */
    CAN_LOG_DATA[47] = (UCHAR)(lcs16HrcTargetSpd>>8);
    CAN_LOG_DATA[48] = (UCHAR)(lcs16DecDecel);                       /* 31 */
    CAN_LOG_DATA[49] = (UCHAR)(lcs16DecDecel>>8);
    CAN_LOG_DATA[50] = (UCHAR)(lcs16DecCtrlError);                 /* 32 */
    CAN_LOG_DATA[51] = (UCHAR)(lcs16DecCtrlError>>8);
    #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[52] = (UCHAR)(pos_fr_1_100bar);                    /* 33 */
    CAN_LOG_DATA[53] = (UCHAR)(pos_fr_1_100bar>>8);
    #else
    CAN_LOG_DATA[52] = (UCHAR)(0);                    /* 33 */
    CAN_LOG_DATA[53] = (UCHAR)(0>>8);
    #endif
    CAN_LOG_DATA[54] = (UCHAR)(eng_torq);                       /* 34 */
    CAN_LOG_DATA[55] = (UCHAR)(eng_torq>>8);
    CAN_LOG_DATA[56] = (UCHAR)(eng_rpm);                            /* 35 */
    CAN_LOG_DATA[57] = (UCHAR)(eng_rpm>>8);
    CAN_LOG_DATA[58] = (UCHAR)(lcu16DecTimeToAct);                                  /* 36 */
    CAN_LOG_DATA[59] = (UCHAR)(lcu16DecTimeToAct>>8);
    #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[60] = (UCHAR)(pos_fl_1_100bar);                                  /* 37 */
    CAN_LOG_DATA[61] = (UCHAR)(pos_fl_1_100bar>>8);
    #else
    CAN_LOG_DATA[60] = (UCHAR)(0);                                  /* 37 */
    CAN_LOG_DATA[61] = (UCHAR)(0>>8);
    #endif
    CAN_LOG_DATA[62] = (UCHAR)(lcs16DecCtrlInput);               /* 38 */
    CAN_LOG_DATA[63] = (UCHAR)(lcs16DecCtrlInput>>8);
    CAN_LOG_DATA[64] = (UCHAR)(lcs16HdcCtrlInput);                             /* 39 */
    CAN_LOG_DATA[65] = (UCHAR)(lcs16HdcCtrlInput>>8);
    CAN_LOG_DATA[66] = (UCHAR)(lcs16HdcSmaThetaGTh);                           /* 40 */
    CAN_LOG_DATA[67] = (UCHAR)(lcs16HdcSmaThetaGTh>>8); 
    CAN_LOG_DATA[68] = (UCHAR)(0);                              /* 41 */
    CAN_LOG_DATA[69] = (UCHAR)(lcs16HdcExThetaGTh);                            /* 42 */
    CAN_LOG_DATA[70] = (UCHAR)(lcs16HdcExThetaGTh>>8);                      
    CAN_LOG_DATA[71] = (UCHAR)(MFC_PWM_DUTY_ESV);               /* 43 */
    CAN_LOG_DATA[72] = (UCHAR)(MFC_PWM_DUTY_P);		    /* 44 */
    CAN_LOG_DATA[73] = (UCHAR)(MFC_PWM_DUTY_S);           /* 45 */    

    /***********************************************************************/
    i=0;
    /***********************************************************************/
  
    if(lcu1HrcInhibitFlg==1)                 i|=0x01;    /* 01 */
    if(lcu1HdcActiveFlg==1)                 i|=0x02;    /* 02 */
    if(lcu1HrcOnReqFlg==1)               i|=0x04;    /* 03 */
    if(HSA_flg==1)              i|=0x08;    /* 04 */
    if(lcu1decFFCtrl_flg==1)              i|=0x10;    /* 05 */  
    if(INHIBIT_DCT_ROLL==1)                i|=0x20;    /* 06 */
    if(0==1)                i|=0x40;    /* 07 */
    if(AUTO_TM==1)                          i|=0x80;    /* 08 */
  
    CAN_LOG_DATA[74] = (UCHAR)(i);                                  /* 46 */
    i=0;

    /***********************************************************************/
  
   	if(0==1)            i|=0x01;    /* 09 */
    if(ESP_TCS_ON==1)                     i|=0x02;    /* 10 */
    if(ABS_fz==1)                           i|=0x04;    /* 11 */
    if(EBD_RA==1)                           i|=0x08;    /* 12 */
    if(YAW_CDC_WORK==1)                     i|=0x10;    /* 13 */
    if(0==1)                       i|=0x20;    /* 14 */    	
    if(0==1)          i|=0x40;    /* 15 */
	if(0==1)                 i|=0x80;    /* 16 */
  
    CAN_LOG_DATA[75] = (UCHAR)(i);                                 /* 47 */
    i=0;
    /***********************************************************************/
  
    if(lcu1DecCtrlReqFlg==1)            i|=0x01;    /* 17 */
    if(lcu1DecSmoothClosingReqFlg==1)           i|=0x02;    /* 18 */
    if(lcu1DecFeedForwardReqFlg==1)      i|=0x04;    /* 19 */
    if(0==1)     i|=0x08;    /* 20 */
    if(0==1)      i|=0x10;    /* 21 */   	
    if(0==1)     i|=0x20;    /* 22 */    	
    if(0==1)         i|=0x40;    /* 23 */
    if(0==1)        i|=0x80;    /* 24 */
 
    CAN_LOG_DATA[76] = (UCHAR)(i);                                /* 48 */
    i=0;
    /***********************************************************************/
  
    if(lcu1hdcBackRollHillFlg==1)        i|=0x01;    /* 25 */
    if(lcu1HdcDownhillFlg==1)       i|=0x02;    /* 26 */
    if(lcu1hdcVehicleForward==1)                 i|=0x04;    /* 27 */
    if(lcu1hdcVehicleBackward==1)     i|=0x08;    /* 28 */
    if(lcu1HdcSwitchFlg==1)          i|=0x10;    /* 29 */
    if(fu1ESCDisabledBySW==1)               i|=0x20;    /* 30 */
    if(0==1)              i|=0x40;    /* 31 */
    if(0==1)               i|=0x80;    /* 32 */
  
    CAN_LOG_DATA[77] = (UCHAR)(i);                                /* 49 */
    i=0;
    /***********************************************************************/
 
    if(VEHICLE_MOVE_WO_EXIT_OK==1)                      i|=0x01;    /* 33 */
    if(0==1)                  i|=0x02;    /* 34 */
    if(0==1)                  i|=0x04;    /* 35 */
    if(0==1)              i|=0x08;    /* 36 */
    if(0==1)         i|=0x10;    /* 37 */
    if(0==1)                i|=0x20;    /* 38 */
    if(0==1)              i|=0x40;    /* 39 */
    if(0==1)                          i|=0x80;    /* 40 */
 
    CAN_LOG_DATA[78] = (UCHAR)(i);                                /* 50 */
    i=0;
    /***********************************************************************/
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)               i|=0x01;    /* 41 */
    if(TCL_DEMAND_SECONDARY==1)             i|=0x02;    /* 42 */
    if(S_VALVE_PRIMARY==1)                  i|=0x04;    /* 43 */
    if(S_VALVE_SECONDARY==1)                i|=0x08;    /* 44 */
      #else	                                            
    if(TCL_DEMAND_fr==1)                    i|=0x01;    /* 41 */
    if(TCL_DEMAND_fl==1)                    i|=0x02;    /* 42 */
    if(S_VALVE_RIGHT==1)                    i|=0x04;    /* 43 */
    if(S_VALVE_LEFT==1)                     i|=0x08;    /* 44 */
      #endif                                            
    if(ABS_fl==1)                           i|=0x10;    /* 45 */
    if(ABS_fr==1) 	                        i|=0x20;    /* 46 */
    if(ABS_rl==1)                           i|=0x40;    /* 47 */
    if(ABS_rr==1)                           i|=0x80;    /* 48 */	
     CAN_LOG_DATA[79] = (UCHAR)(i);                              /* 51 */
    /***********************************************************************/
}
#endif

#if __SPAS_INTERFACE
void LSSPAS_vCallSPASBRAKELogData(void)
{
    UCHAR i;

    /***********************************************************************/
    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (UCHAR)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (UCHAR)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (UCHAR)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (UCHAR)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (UCHAR)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (UCHAR)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (UCHAR)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (UCHAR)(vrad_rr>>8);

                                                   
    CAN_LOG_DATA[10] = (UCHAR)(lcu8SpasCtrlReq);                    /* 06 */
    CAN_LOG_DATA[11] = (UCHAR)(lcs8SpasState);                /* 07 */

    CAN_LOG_DATA[12] = (UCHAR)((int16_t)target_vol/1000);           /* 08 */
    CAN_LOG_DATA[13] = (UCHAR)((int16_t)fu16CalVoltMOTOR/1000);     /* 09 */

    CAN_LOG_DATA[14] = (UCHAR)(vref);                               /* 10 */
    CAN_LOG_DATA[15] = (UCHAR)(vref>>8);
    
    CAN_LOG_DATA[16] = (UCHAR)(lcu8SpasCompleteStopCnt);                       /* 11 */
    CAN_LOG_DATA[17] = (UCHAR)(0);             /* 12 */
    CAN_LOG_DATA[18] = (UCHAR)(0);                    /* 13 */

    CAN_LOG_DATA[19] = (UCHAR)(0);                   /* 14 */
    CAN_LOG_DATA[20] = (UCHAR)(MFC_PWM_DUTY_P);                /* 15 */
    CAN_LOG_DATA[21] = (UCHAR)(0);                 /* 16 */
    CAN_LOG_DATA[22] = (UCHAR)(0);                     /* 17 */
    CAN_LOG_DATA[23] = (UCHAR)(MFC_PWM_DUTY_S);                     /* 18 */
    CAN_LOG_DATA[24] = (UCHAR)(lcu16SpasRiseActCnt);                /* 19 */
    CAN_LOG_DATA[25] = (UCHAR)(lcu16SpasRiseActCnt>>8);
    CAN_LOG_DATA[26] = (UCHAR)(lcs16SpasCtrlError);            /* 20 */
    CAN_LOG_DATA[27] = (UCHAR)(lcs16SpasCtrlError>>8);
    CAN_LOG_DATA[28] = (UCHAR)(lcu16SpasHoldActCnt);          /* 21 */
    CAN_LOG_DATA[29] = (UCHAR)(lcu16SpasHoldActCnt>>8);  
    CAN_LOG_DATA[30] = (UCHAR)(lcu16SpasReleaseActCnt);                        /* 22 */
    CAN_LOG_DATA[31] = (UCHAR)(lcu16SpasReleaseActCnt>>8);
    CAN_LOG_DATA[32] = (UCHAR)(lcs16SpasHoldTcCurrent);                   /* 23 */
    CAN_LOG_DATA[33] = (UCHAR)(lcs16SpasHoldTcCurrent>>8);
    CAN_LOG_DATA[34] = (UCHAR)(spas_msc_target_vol);               /* 24 */
    CAN_LOG_DATA[35] = (UCHAR)(spas_msc_target_vol>>8);
    CAN_LOG_DATA[36] = (UCHAR)(lcu8SpasBrkIntRpl);             /* 25 */
    CAN_LOG_DATA[37] = (UCHAR)(lcu8SpasBrkIntPtn);                /* 26 */

    CAN_LOG_DATA[38] = (UCHAR)(gs_sel);                             /* 27 */
    CAN_LOG_DATA[39] = (UCHAR)(mtp);                                  /* 28 */

    CAN_LOG_DATA[40] = (UCHAR)(wstr);                 /* 29 */
    CAN_LOG_DATA[41] = (UCHAR)(wstr>>8);
    CAN_LOG_DATA[42] = (UCHAR)(lcs16SpasReqTargetP);                 /* 30 */
    CAN_LOG_DATA[43] = (UCHAR)(lcs16SpasReqTargetP>>8);
    CAN_LOG_DATA[44] = (UCHAR)(0);             /* 31 */
    CAN_LOG_DATA[45] = (UCHAR)(0);
    CAN_LOG_DATA[46] = (UCHAR)(TC_MFC_Voltage_Primary_dash);                  /* 32 */
    CAN_LOG_DATA[47] = (UCHAR)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[48] = (UCHAR)(lcu16SpasCtrlErrorCnt);                  /* 33 */
    CAN_LOG_DATA[49] = (UCHAR)(lcu16SpasCtrlErrorCnt>>8);
    CAN_LOG_DATA[50] = (UCHAR)(0);                    /* 34 */
    CAN_LOG_DATA[51] = (UCHAR)(0);
    CAN_LOG_DATA[52] = (UCHAR)(0);                /* 35 */
    CAN_LOG_DATA[53] = (UCHAR)(0);
    CAN_LOG_DATA[54] = (UCHAR)(0);          /* 36 */
    CAN_LOG_DATA[55] = (UCHAR)(0);

    CAN_LOG_DATA[56] = (UCHAR)(0);                /* 37 */
    CAN_LOG_DATA[57] = (UCHAR)(0);             /* 38 */

    CAN_LOG_DATA[58] = (UCHAR)(eng_rpm);             /* 39 */
    CAN_LOG_DATA[59] = (UCHAR)(eng_rpm>>8);

    CAN_LOG_DATA[60] = (UCHAR)(0);                 /* 40 */
    CAN_LOG_DATA[61] = (UCHAR)(0);
    CAN_LOG_DATA[62] = (UCHAR)(0);                 /* 41 */
    CAN_LOG_DATA[63] = (UCHAR)(0);


    CAN_LOG_DATA[64] = (UCHAR)(mpress);                             /* 42 */
    CAN_LOG_DATA[65] = (UCHAR)(mpress>>8);

    CAN_LOG_DATA[66] = (UCHAR)(eng_torq_rel);                       /* 43 */
    CAN_LOG_DATA[67] = (UCHAR)(eng_torq_rel>>8);
    CAN_LOG_DATA[68] = (UCHAR)(0);                            /* 44 */
    CAN_LOG_DATA[69] = (UCHAR)(0);

    CAN_LOG_DATA[70] = (UCHAR)(0);                /* 45 */
    CAN_LOG_DATA[71] = (UCHAR)(0);              /* 46 */
    CAN_LOG_DATA[72] = (UCHAR)(0);                        /* 47 */
    CAN_LOG_DATA[73] = (UCHAR)(0);                   /* 48 */

    /**********************************49*************************************/
    i=0;
    if(lcu1SpasActiveFlg==1)         i|=0x01;           /* 01 */
    if(lcu1SpasInhibitFlg==1)           i|=0x02;           /* 02 */
    if(lcu1SpasEntOk==1)           i|=0x04;           /* 03 */
    if(lcu1SpasExitOkFlg==1)             i|=0x08;           /* 04 */
    if(lcu1SpasRiseToHold==1)       		 i|=0x10;           /* 05 */
    if(lcu1SpasHoldToRise==1)   i|=0x20;           /* 06 */ 
    if(lcu1SpasFFControl==1)    i|=0x40;           /* 07 */     
    if(lcu1SpasFBControl==1)             i|=0x80;           /* 08 */

    CAN_LOG_DATA[74] = (UCHAR)(i);     
    /**********************************50*************************************/
    i=0;
	if(SPAS_TCL_DEMAND_fl==1)          i|=0x01;           /* 09 */
    if(SPAS_TCL_DEMAND_fr==1)              i|=0x02;           /* 10 */
    if(SPAS_S_VALVE_LEFT==1)         i|=0x04;           /* 11 */
    if(SPAS_S_VALVE_RIGHT==1)        i|=0x08;           /* 12 */
  	if(SPAS_MOTOR_ON==1)       i|=0x10;           /* 13 */
   	if(ABS_fz)           i|=0x20;           /* 14 */   	
   	if(0)           i|=0x40;           /* 15 */
    if(0)        i|=0x80;           /* 16 */
       
    CAN_LOG_DATA[75] = (UCHAR)(i);                                
    /**********************************51*************************************/    
    i=0;
    if(0)      i|=0x01;           /* 17 */
	if(0)       i|=0x02;           /* 18 */
    if(0)          i|=0x04;           /* 19 */
    if(0)        i|=0x08;           /* 20 */
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_SECONDARY==1)      i|=0x10;           /* 21 */
      #else
    if(TCL_DEMAND_fl==1)             i|=0x10;           /* 21 */
      #endif
    if(0)    		 i|=0x20;           /* 22 */
    if(0)           i|=0x40;           /* 23 */
    if(0)         i|=0x80;           /* 24 */
       
    CAN_LOG_DATA[76] = (UCHAR)(i);                                   
    /**********************************52*************************************/    
    i=0;
    if(0)        i|=0x01;           /* 25 */
    if(0)    i|=0x02;           /* 26 */
    if(0)    i|=0x04;           /* 27 */
      #if __HSA
    if(HSA_flg==1)                 	 i|=0x08;           /* 28 */
      #endif
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)        i|=0x10;           /* 29 */
      #else
    if(TCL_DEMAND_fr==1)             i|=0x10;           /* 29 */
      #endif
    if(0)       i|=0x20;           /* 30 */
    if(0)          i|=0x40;          	/* 31 */
    if(0)   i|=0x80;         	/* 32 */
       
    CAN_LOG_DATA[77] = (UCHAR)(i);                                  
    /***********************************53************************************/    
    i=0;
      #if (__SPLIT_TYPE==1)
    if(S_VALVE_SECONDARY==1)         i|=0x01;                    /* 33 */
    if(S_VALVE_PRIMARY==1)           i|=0x02;                    /* 34 */
      #else
    if(S_VALVE_LEFT==1)              i|=0x01;                    /* 33 */
    if(S_VALVE_RIGHT==1)             i|=0x02;                    /* 34 */
      #endif      
    if(0)          i|=0x04;                    /* 35 */
    if(0)            i|=0x08;                    /* 36 */
    if(0)      i|=0x10;                    /* 37 */

    if(0)          i|=0x20;                    /* 38 */
    if(0)                            i|=0x40;                    /* 39 */
    if(0)       i|=0x80;                    /* 40 */

       
    CAN_LOG_DATA[78] = (UCHAR)(i);                                     
    /***********************************54************************************/    
    i=0;
    if(0)                            i|=0x01;                    /* 41 */
    if(ESP_TCS_ON==1)                            i|=0x02;                    /* 42 */
    if(0)                            i|=0x04;                    /* 43 */
    if(0)                            i|=0x08;                    /* 44 */
    if(BLS==1)                            i|=0x10;                    /* 45 */
    if(0)                            i|=0x20;                    /* 46 */
    if(0)                            i|=0x40;                    /* 47 */
    if(0)                            i|=0x80;                    /* 48 */
    CAN_LOG_DATA[79] = (UCHAR)(i);                                     
    /***********************************************************************/

}
#endif /* #if __SPAS_INTERFACE */


#if __SCC
void	LSSCC_vCallSCCLogData(void)
{
    uint8_t i;
    
    //ID 0x111
    CAN_LOG_DATA[0]  = system_loop_counter;                 
    CAN_LOG_DATA[1]  = system_loop_counter>>8;
    CAN_LOG_DATA[2]  = vrad_fl;                             
    CAN_LOG_DATA[3]  = vrad_fl>>8;
    CAN_LOG_DATA[4]  = vrad_fr;                             
    CAN_LOG_DATA[5]  = vrad_fr>>8;
    CAN_LOG_DATA[6]  = vrad_rl;                             
    CAN_LOG_DATA[7]  = vrad_rl>>8;
    
    //ID 0x222
    CAN_LOG_DATA[8]  = vrad_rr;		                              
    CAN_LOG_DATA[9]  = vrad_rr>>8;	                
    CAN_LOG_DATA[10] = vref5;                        	
    CAN_LOG_DATA[11] = vref5>>8;                     
    CAN_LOG_DATA[12] = mtp;                   
    CAN_LOG_DATA[13] = lcu8SccDsrEngTorq*2/5; // SCC cal_torque   // 0~255% >> 0~100%
    CAN_LOG_DATA[14] = lcs16SccTargetWheelTorq_FF;            
    CAN_LOG_DATA[15] = lcs16SccTargetWheelTorq_FF>>8;   

    //ID 0x333		
  #if __CDM  
    CAN_LOG_DATA[16] = (int8_t)(-lcu8CdmDecCmd);        	
    CAN_LOG_DATA[17] = lcu8CdmHBACmd;
  #else
    CAN_LOG_DATA[16] = 0;        	
    CAN_LOG_DATA[17] = (uint8_t)lcs16_WpcTPresRataCnt;  
  #endif          	
    CAN_LOG_DATA[18] = lcs16SccRoad_slope_g_fil/10;           
    CAN_LOG_DATA[19] = lesps16EMS_TQI_ACOR/10;   // eng_torque    // 0~100%        
    CAN_LOG_DATA[20] = alat/10;                                
    CAN_LOG_DATA[21] = lcu8SccMode; //(uint8_t)(fu16CalVoltBATT2/100);//lcu8SccSetSpeed                    
    CAN_LOG_DATA[22] = gs_pos;                 
    CAN_LOG_DATA[23] = MFC_PWM_DUTY_ESV; //lesps16EMS_TQFR/10;      	// 0~100%                      

    //ID 0x444
    CAN_LOG_DATA[24] = wstr;        
    CAN_LOG_DATA[25] = wstr>>8;
    CAN_LOG_DATA[26] = lcs16SccTargetWheelTorq_PI;           
    CAN_LOG_DATA[27] = lcs16SccTargetWheelTorq_PI>>8;    
  #if (__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE)
    CAN_LOG_DATA[28] = pos_fl_1_100bar;                 
    CAN_LOG_DATA[29] = pos_fl_1_100bar>>8;           
    CAN_LOG_DATA[30] = pos_fr_1_100bar;                                        
    CAN_LOG_DATA[31] = pos_fr_1_100bar>>8;          
  #else
    CAN_LOG_DATA[28] = lsahbs16MC1presFilt_1_100Bar;
    CAN_LOG_DATA[29] = lsahbs16MC1presFilt_1_100Bar>>8; /* Front Wheel Circuit Press*/
    CAN_LOG_DATA[30] = lsahbs16MC2presFilt_1_100Bar;
    CAN_LOG_DATA[31] = lsahbs16MC2presFilt_1_100Bar>>8; /* Rear Wheel Circuit Press */
  #endif            
   
    //ID 0x555
  #if (__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE)
    CAN_LOG_DATA[32] = lcs16WpcSecPres;    
    CAN_LOG_DATA[33] = lcs16WpcSecPres>>8;  
  #else
    CAN_LOG_DATA[32] = lcans16AhbCircuitTargetPressP;    
    CAN_LOG_DATA[33] = lcans16AhbCircuitTargetPressP>>8;  
  #endif            
    CAN_LOG_DATA[34] = lcs16WpcPriPres;    
    CAN_LOG_DATA[35] = lcs16WpcPriPres>>8; 
    CAN_LOG_DATA[36] = lcs16SccTargetWheelTorq;           
    CAN_LOG_DATA[37] = lcs16SccTargetWheelTorq>>8;    
    CAN_LOG_DATA[38] = (uint8_t)(((mpress/10)>255)?255:(((mpress/10)<0)?0:(uint8_t)(mpress/10)));
  	CAN_LOG_DATA[39] = lcu8SccState;		

    //ID 0x666
    CAN_LOG_DATA[40] = fu16CalVoltMOTOR;                            
    CAN_LOG_DATA[41] = fu16CalVoltMOTOR>>8;
    CAN_LOG_DATA[42] = eng_rpm;                                          
    CAN_LOG_DATA[43] = eng_rpm>>8;        
    CAN_LOG_DATA[44] = wpc_msc_target_vol;                                         
    CAN_LOG_DATA[45] = wpc_msc_target_vol>>8;        
    CAN_LOG_DATA[46] = lcs16SccAlong;         
    CAN_LOG_DATA[47] = lcs16SccAlong>>8;  

    //ID 0x117
    CAN_LOG_DATA[48] = lcs16SccAccel_ref;                   
    CAN_LOG_DATA[49] = lcs16SccAccel_ref>>8;   
  #if __CDM    
    CAN_LOG_DATA[50] = lcs16CdmTargetG;                                   
    CAN_LOG_DATA[51] = lcs16CdmTargetG>>8;        
  #else
    CAN_LOG_DATA[50] = (int16_t)(lcs32WpcBrkReadyCtrlTime/143);                                 
    CAN_LOG_DATA[51] = (int16_t)(lcs32WpcBrkReadyCtrlTime/143)>>8;  
  #endif  
    CAN_LOG_DATA[52] = yaw_out; 
    CAN_LOG_DATA[53] = yaw_out>>8;
  #if __ESC_MOTOR_PWM_CONTROL   
    CAN_LOG_DATA[54] = lau8MscDuty;                  
    CAN_LOG_DATA[55] = lau8MscDuty>>8;
  #else
    CAN_LOG_DATA[54] = 0;                  
    CAN_LOG_DATA[55] = 0>>8;
  #endif
    //ID 0x227
    CAN_LOG_DATA[56] = target_vol;   	       
    CAN_LOG_DATA[57] = target_vol>>8;    
    CAN_LOG_DATA[58] = lcs16SccTargGfilt;                               
    CAN_LOG_DATA[59] = lcs16SccTargGfilt>>8;   
    CAN_LOG_DATA[60] = lcs16WpcPriFFInput;                                
    CAN_LOG_DATA[61] = lcs16WpcPriFFInput>>8; 
    CAN_LOG_DATA[62] = lcs16SccTargetG;                                
    CAN_LOG_DATA[63] = lcs16SccTargetG>>8;    
						
    //ID 0x337
    CAN_LOG_DATA[64] = lcs16WpcPriFBInput;                             
    CAN_LOG_DATA[65] = lcs16WpcPriFBInput>>8;

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;


    if(lcu8WpcCtrlReq)          tempB5|=0x01; 
    if(BLS)                     tempB5|=0x02;
    if(MPRESS_BRAKE_ON)         tempB5|=0x04;
    if(fu1ESCDisabledBySW)      tempB5|=0x08;
    if(lcu1SccStopReq)          tempB5|=0x10;
    if(ABS_fz)                  tempB5|=0x20;
  #if __CDM  
    if(lcu1CdmPrefill)          tempB5|=0x40;
    if(lcu1CdmRunFlg)           tempB5|=0x80;
  #else
    if(0)                       tempB5|=0x40;
    if(0)                       tempB5|=0x80;
  #endif  	

    if(YAW_CDC_WORK)            tempB4|=0x01; 
    if((PBA_ON)||(PBA_pulsedown)) tempB4|=0x02;
    if((FTCS_ON)||(BTC_fz))     tempB4|=0x04;
    if(ETCS_ON)                 tempB4|=0x08; 
    if(lcu1WpcESVCoilOverTemp)  tempB4|=0x10;
    if(lcu1WpcBrkReadyCtrl)     tempB4|=0x20;
    if(lcu1SccInhibitFlg)       tempB4|=0x40;
    if(lcu1WpcESVCoilOverTempOld) tempB4|=0x80; 

    if(AV_VL_fl)                tempB3|=0x01;
    if(HV_VL_fl)                tempB3|=0x02;
    if(EPC_ON)                  tempB3|=0x04;
    if(lcu1SccPBSwOnFlg)        tempB3|=0x08;	
    if(lcu1SccEngCtrlReq)       tempB3|=0x10;
    if(lcu1SccBrkCtrlReq)       tempB3|=0x20;			   
    if(lcu1WpcFadeOut)          tempB3|=0x40;	    
    if(lcu1SccPidHoldFlg)       tempB3|=0x80;		        

    if(AV_VL_fr)                tempB2|=0x01;
    if(HV_VL_fr)                tempB2|=0x02;
  #if __CDM  
    if(lcu1CdmInhibitFlg)       tempB2|=0x04;
  #else
    if(0)                       tempB2|=0x04;
  #endif  
    if(lcu1SccPidResetFlg)      tempB2|=0x08;
    if(WPC_MSC_PREFILL)         tempB2|=0x10;
    if(WPC_MSC_MOTOR_ON)        tempB2|=0x20;
    if(lcu1SccEngTorqUpGearFlg) tempB2|=0x40;
    if(lcu1CdmDecCmdAct)        tempB2|=0x80;

    if(AV_VL_rl)                tempB1|=0x01;
    if(HV_VL_rl)                tempB1|=0x02;
    if(lcu1CdmEngReq)           tempB1|=0x04;
    if(TCL_DEMAND_fl)           tempB1|=0x08;
    if(lcu1CdmActFadeOut)       tempB1|=0x10;   // EBD_RA
    if(WpcSec1stApplyFlg)       tempB1|=0x20;           
    if(WpcPri1stApplyFlg)       tempB1|=0x40;          
    if(lcu1SccRunFlg)           tempB1|=0x80;

    if(AV_VL_rr)                tempB0|=0x01;
    if(HV_VL_rr)                tempB0|=0x02;
    if(lcu1WpcMSCInitRiseAddOn) tempB0|=0x04;
    if(lcu1SccHillUpFlg)        tempB0|=0x08;
    if(lcu1SccHillDownFlg)      tempB0|=0x10;
    if(TCL_DEMAND_fr)        	tempB0|=0x20;
    if(S_VALVE_LEFT)         	tempB0|=0x40;
    if(S_VALVE_RIGHT)           tempB0|=0x80;  
    
    CAN_LOG_DATA[66] = tempB5;          
    CAN_LOG_DATA[67] = tempB4;          
    CAN_LOG_DATA[68] = tempB3;          
    CAN_LOG_DATA[69] = tempB2;          
    CAN_LOG_DATA[70] = tempB1;          
    CAN_LOG_DATA[71] = tempB0;          
    
    
    CAN_LOG_DATA[72] = MFC_PWM_DUTY_S;        
    CAN_LOG_DATA[73] = MFC_PWM_DUTY_P; 	               
    CAN_LOG_DATA[74] = lcu16WpcSecCur;         
    CAN_LOG_DATA[75] = lcu16WpcPriCur;	       
    CAN_LOG_DATA[76] = lesps16TCS_TQI_TCS/10;	// cal_torque
    CAN_LOG_DATA[77] = mpress_slop_filt/10; //lcu8WpcPriCtrlMode;             
    CAN_LOG_DATA[78] = lcs16WpcTargetPres;  
    CAN_LOG_DATA[79] = lcs16WpcTargetPres>>8;
    
}
#endif  /*#if __SCC end*/
	
#if (__IDB_LOGIC==ENABLE)
void LSESP_vCallESCPlusLogData_modd(void)
{
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_dump_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_dump_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}

    CAN_LOG_DATA[0]  =(uint8_t) (esp_tempW3);							//ch01
    CAN_LOG_DATA[1]  =(uint8_t) (esp_tempW3>>8);
	
    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                		//ch02
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);
	
    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                		//ch03
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);
	
    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                		//ch04
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);
	
    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                		//ch05
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);
	
    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);								//ch06
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);
    
#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
	if(esp_tempW9 > 120) esp_tempW9 =120;
	else if(esp_tempW9< (-120)) esp_tempW9 = -120;
    
    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);							//ch07
    
    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());		//ch08
    
    CAN_LOG_DATA[14] =(uint8_t) (vref5);										//ch09
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

	esp_tempW6= esp_mu/10;

	if(esp_tempW6 > 120) esp_tempW6 =120;
	else if(esp_tempW6< (-120)) esp_tempW6 = -120;

//    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6);						//ch10
//    CAN_LOG_DATA[16] =(uint8_t)(la_RL1HP.u8OutletOnTime+la_RL2HP.u8OutletOnTime);						//ch10
	CAN_LOG_DATA[16] =(uint8_t)(FL_ESC.debuger);						//ch10
    
	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
//    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);     //ch11
    CAN_LOG_DATA[17] =(uint8_t) (la_RR1HP.u8OutletOnTime+la_RR2HP.u8OutletOnTime);     //ch11

    //CAN_LOG_DATA[18] =(uint8_t) (target_vol);									//ch12 for MGH only
    //CAN_LOG_DATA[19] =(uint8_t) (target_vol>>8);
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  	  esp_tempW3 = RR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[18] =(uint8_t) (esp_tempW3);									//ch12 for MGH only
    CAN_LOG_DATA[19] =(uint8_t) (esp_tempW3>>8);
	
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_control;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[20] =(esp_tempW3);              																															//ch13
    CAN_LOG_DATA[21] =(esp_tempW3>>8);
    CAN_LOG_DATA[22] =(uint8_t)	(FL_ESC.lcesps16IdbTarPress/10);																																//ch14
    CAN_LOG_DATA[23] =(uint8_t)	(FL_ESC.lcesps16IdbDelTarPress/10);

    CAN_LOG_DATA[24] =(uint8_t) (FR_ESC.lcesps16IdbTarPress/10);//(esp_tempW0);                         //ch15
    CAN_LOG_DATA[25] =(uint8_t) (FR_ESC.lcesps16IdbDelTarPress/10);//(esp_tempW0>>8);

    CAN_LOG_DATA[26] =(uint8_t) (FL.s16_Estimated_Active_Press);                         //ch16
    CAN_LOG_DATA[27] =(uint8_t) (FL.s16_Estimated_Active_Press>>8);

    CAN_LOG_DATA[28] =(uint8_t) (alat);                               //ch17
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);  
    
//	if(SLIP_CONTROL_NEED_FL==1)
//	{
//		esp_tempW2 = del_target_slip_fl;
//	}
//	else if(SLIP_CONTROL_NEED_FR==1)
//	{
//		esp_tempW2 = del_target_slip_fr;
//	}
//	else
//	{
//		esp_tempW2 = 0;
//	}
  	if(SLIP_CONTROL_NEED_RL==1)
	{
		esp_tempW3 = RL_ESC.slip_dump_thr_change;
	}
  	else if(SLIP_CONTROL_NEED_RR==1)
	{
		esp_tempW3 = RR_ESC.slip_dump_thr_change;
	}
	else
	{
		esp_tempW3 = 0;
	}

    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW3);                             //ch18
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW3>>8);
    
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_rise_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_rise_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
    CAN_LOG_DATA[32] =(uint8_t) (esp_tempW3); //(FL_ESC.slip_m);//(mpress);//(Beta_MD);										//ch19
    CAN_LOG_DATA[33] =(uint8_t) (esp_tempW3>>8); //(FL_ESC.slip_m>>8);//(mpress>>8);//(Beta_MD>>8);

  	if(SLIP_CONTROL_NEED_RL==1)
  	{
		esp_tempW3 = RL_ESC.slip_rise_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
		esp_tempW3 = RR_ESC.slip_rise_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
    CAN_LOG_DATA[34] =(uint8_t) (esp_tempW3); //(FR_ESC.slip_m);//(delta_moment_q_os);                      //ch20
    CAN_LOG_DATA[35] =(uint8_t) (esp_tempW3>>8); //(FR_ESC.slip_m>>8);//(delta_moment_q_os);                      //ch20

    CAN_LOG_DATA[36] =(uint8_t) (FR.s16_Estimated_Active_Press);//(eng_torq);                               //ch21
    CAN_LOG_DATA[37] =(uint8_t) (FR.s16_Estimated_Active_Press>>8);//(eng_torq>>8);
    
    CAN_LOG_DATA[38] =(uint8_t) (RL_ESC.u8WhlVlvDumpTi);                               		//ch22
    CAN_LOG_DATA[39] =(uint8_t) (RR_ESC.u8WhlVlvDumpTi);                                    //ch23

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               		//ch24
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch25
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);
    
    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                  	//ch26
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);
	
//    CAN_LOG_DATA[46] =(uint8_t) (drive_torq);					                		//ch27
//    CAN_LOG_DATA[47] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[46] =(uint8_t) (FL_ESC.debuger);					                		//ch27
    CAN_LOG_DATA[47] =(uint8_t) (FR_ESC.debuger);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 		//ch28
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);
	
    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                       		//ch29
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);
	
    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                 		//ch30
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);
	
    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);								//ch31
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);
	
    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                         //ch32
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);
    
//    CAN_LOG_DATA[58] =(uint8_t) (cal_torq);                 					    //ch33
//    CAN_LOG_DATA[59] =(uint8_t) (cal_torq>>8);

    CAN_LOG_DATA[58] =(uint8_t) (RL_ESC.lcespu8IdbDumpRateCnt);                 					    //ch33
    CAN_LOG_DATA[59] =(uint8_t) (RR_ESC.lcespu8IdbDumpRateCnt);

	if( yaw_out > 0 )
	{
	    esp_tempW7 = ay_vx_dot_sig_plus;
	}
	else
	{
	    esp_tempW7 = ay_vx_dot_sig_minus;
	}
		
    CAN_LOG_DATA[60] =(uint8_t) (RL_ESC.lcesps16IdbTarPress/10);   					                //ch34
    CAN_LOG_DATA[61] =(uint8_t) (FL_ESC.lcespu8IdbDumpRateCnt);//(RL_ESC.lcesps16LimitIdbTargetP/10);
	

    CAN_LOG_DATA[62] =(uint8_t) (RR_ESC.lcesps16IdbTarPress/10);					                 //ch35
    CAN_LOG_DATA[63] =(uint8_t) (FR_ESC.lcespu8IdbDumpRateCnt);//(RR_ESC.lcesps16LimitIdbTargetP/10);//(RR_ESC.lcesps16IdbTarPress>>8);
    
#if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[64] =(uint8_t) (lis16DriverIntendedTP);               //ch36
    CAN_LOG_DATA[65] =(uint8_t) (lis16DriverIntendedTP>>8);
#else
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                              //ch36
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);
#endif

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;			                 //flag1
//    if(FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)      tempB5|=0x02;			//flag2
    if(ldespu1ESC1stCycle)      tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;											 //flag3
    if(EBD_RA)                      tempB5|=0x08;											 //flag4
    if(BTC_fz==1)                   tempB5|=0x10;											 //flag5
#if __TCS
    if(ETCS_ON)                     tempB5|=0x20;											 //flag6
#endif
    if(YAW_CDC_WORK)                tempB5|=0x40;											 //flag7
#if __TCS
    if(ESP_TCS_ON)                  tempB5|=0x80;											 //flag8
#endif
    if(lcu1US2WHControlFlag)       tempB4|=0x01;											 					//flag9

#if __ROP
//    if(lcabsu1StrkReqPossibleWhlSt )          			tempB4|=0x02;											 //flag10
#else
//	if(lcabsu1StrkReqPossibleWhlSt)         				tempB4|=0x02;											 //flag10
#endif
    if(PARTIAL_BRAKE)               tempB4|=0x04;											 //flag11
    
//   if(FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)           tempB4|=0x08;	//flag12
   if(ldespu1ESC2ndCycle)           tempB4|=0x08;	//flag12

    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn)    tempB4|=0x10;											 //flag13
    #else
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;											 //flag13
    #endif
    
    if(U1WheelValveCntr)     tempB4|=0x20;											 //flag14

    if(AUTO_TM) {
        if(BACK_DIR)                tempB4|=0x40;											 //flag15
    } else {
#if __AX_SENSOR
    if(BACKWARD_MOVE)				tempB4|=0x40;											 //flag15
#else
       if(0)           tempB4|=0x40;
#endif
    }

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;	//flag16
#else
    if(FLAG_COMPULSION_HOLD_REAR||FLAG_COMPULSION_HOLD)             tempB4|=0x80;	 //flag16
#endif

    if(AV_VL_fl)                    tempB3|=0x01;											 //flag17
    if(HV_VL_fl)                    tempB3|=0x02;											 //flag18
    if(ABS_fl)                      tempB3|=0x04;											 //flag19

    if(U1StrokeRecoveryforESC)         tempB3|=0x08;											 //flag20

#if !__SPLIT_TYPE
    if(0)               tempB3|=0x10;											 //flag21
#else
    if(0)        tempB3|=0x10;											 //flag21
#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;											 //flag22
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)                					 tempB3|=0x40;				//flag23
//    if(INITIAL_BRAKE||FLAG_ACTIVE_BOOSTER||FLAG_ACTIVE_BOOSTER_PRE)            tempB3|=0x80;				//flag24
    if(FLAG_ON_CENTER)            tempB3|=0x80;				//flag24

    if(AV_VL_fr)                    tempB2|=0x01;											 //flag25
    if(HV_VL_fr)                    tempB2|=0x02;											 //flag26
    if(ABS_fr)                      tempB2|=0x04;											 //flag27
#if __P_CONTROL
    if(0)                     tempB2|=0x08;
#else
    if(BTCS_fr)                     tempB2|=0x08;											 //flag28
#endif
#if !__SPLIT_TYPE
    if(0)               tempB2|=0x10;											 //flag29
#else
    if(0)          tempB2|=0x10;											 //flag29
#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;											 //flag30
    if(YAW_CHANGE_LIMIT)          tempB2|=0x40;						//flag31

#if __BANK_ESC_IMPROVEMENT_CONCEPT
    if(ldespu1BankLowerSuspectFlag) tempB2|=0x80;											 //flag32
#else
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;											 //flag32
#endif

    if(AV_VL_rl)                    tempB1|=0x01;											 //flag33
    if(HV_VL_rl)                    tempB1|=0x02;											 //flag34
    if(ABS_rl)                      tempB1|=0x04;											 //flag35
    if(BTCS_fl||BTCS_rl) 			tempB1|=0x08;											 //flag36
    if(EBD_rl)                      tempB1|=0x10;											 //flag37
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;											 //flag38
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)                tempB1|=0x40;											 //flag39
#else
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)           tempB1|=0x40;											 //flag39
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(U1EscAfterStrokeR)          tempB1|=0x80;		//flag40
#else
    if(U1EscAfterStrokeR)        			tempB1|=0x80;											 //flag40
#endif

	if(AV_VL_rr)            				tempB0|=0x01;											 //flag41
    if(HV_VL_rr)            				tempB0|=0x02;											 //flag42
    if(ABS_rr)              				tempB0|=0x04;											 //flag43
#if __P_CONTROL
    if(0)             tempB0|=0x08;
#else
    if(CROSS_SLIP_CONTROL_rr||RR.U1EscOtherWheelCnt)    				tempB0|=0x08;											 //flag44
#endif
    if(EBD_rr)              				tempB0|=0x10;											 //flag45
    if(ESP_BRAKE_CONTROL_RR)  			tempB0|=0x20;											 //flag46
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)       				tempB0|=0x40;											 			//flag47
#else
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)     				tempB0|=0x40;											 //flag47
#endif

    if(CROSS_SLIP_CONTROL_rl||RL.U1EscOtherWheelCnt)          tempB0|=0x80;   //flag48


    CAN_LOG_DATA[66] =(uint8_t) (tempB5);				//ch40
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);				//ch41
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);       //ch42
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);       //ch43
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);       //ch44
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);       //ch45

//if(SLIP_CONTROL_NEED_RL==1)
//{
//    esp_tempW6 = del_target_slip_rl;
//}
//else if(SLIP_CONTROL_NEED_RR==1)
//{
//    esp_tempW6 = del_target_slip_rr;
//}
//else
//{
//    esp_tempW6 = 0;
//}
    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW6 = FL_ESC.lcesps16IdbDumpInterval;
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW6 = FR_ESC.lcesps16IdbDumpInterval;
    }
    else
    {
        esp_tempW6 = 0;
    }
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);								//ch43

    if(SLIP_CONTROL_NEED_RL==1)
    {
        esp_tempW6 = RL_ESC.lcesps16IdbDumpInterval;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
        esp_tempW6 = RR_ESC.lcesps16IdbDumpInterval;
    }
    else
    {
        esp_tempW6 = 0;
    }
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6);
	
    CAN_LOG_DATA[74] =(uint8_t) (la_FL1HP.u8OutletOnTime+la_FL2HP.u8OutletOnTime);//(la_FR1HP.u8InletOnTime);//(FL.flags);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[75] =(uint8_t) (la_FR1HP.u8OutletOnTime+la_FR2HP.u8OutletOnTime);//(FR.flags);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[76] =(uint8_t) (FL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[77] =(uint8_t) (FR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[78] =(uint8_t) (FL_ESC.u8WhlVlvDumpTi);//(RL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[79] =(uint8_t) (FR_ESC.u8WhlVlvDumpTi);//(RR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
}

void LSESP_vCallESCPlusLogData_DelTP(void)
{
    CAN_LOG_DATA[0]  =(uint8_t) (system_loop_counter);							//ch01
    CAN_LOG_DATA[1]  =(uint8_t) (system_loop_counter>>8);

    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                		//ch02
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);

    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                		//ch03
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);

    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                		//ch04
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);

    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                		//ch05
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);

    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);								//ch06
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);

#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
	if(esp_tempW9 > 120) esp_tempW9 =120;
	else if(esp_tempW9< (-120)) esp_tempW9 = -120;
    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);							//ch07

    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());		//ch08

    CAN_LOG_DATA[14] =(uint8_t) (vref5);										//ch09
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

esp_tempW6= esp_mu/10;

if(esp_tempW6 > 120) esp_tempW6 =120;
else if(esp_tempW6< (-120)) esp_tempW6 = -120;

    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6); 																													//ch10

	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
    CAN_LOG_DATA[17] =(uint8_t) ((esp_tempW0<-120)?-120:(int8_t)esp_tempW0);     //ch11

    //CAN_LOG_DATA[18] =(uint8_t) (target_vol);									//ch12 for MGH only
    //CAN_LOG_DATA[19] =(uint8_t) (target_vol>>8);
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  	  esp_tempW3 = RR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[18] =(uint8_t) (esp_tempW3);									//ch12 for MGH only
    CAN_LOG_DATA[19] =(uint8_t) (esp_tempW3>>8);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  	  esp_tempW3 = FR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[20] =(esp_tempW3);              					//ch13
    CAN_LOG_DATA[21] =(esp_tempW3>>8);
    CAN_LOG_DATA[22] =(uint8_t)	(FL_ESC.lcesps16IdbTarPress/10);		//ch14

 	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  		esp_tempW3 = FR_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else
  	{
  		esp_tempW3 = FL_ESC.lcesps16IdbDelTarPress/10;
  	}
    CAN_LOG_DATA[23] =(uint8_t)	(esp_tempW3);


    CAN_LOG_DATA[24] =(uint8_t) (FR_ESC.lcesps16IdbTarPress/10);//(esp_tempW0);                         //ch15

 	if(SLIP_CONTROL_NEED_RL==1)
  	{
 		esp_tempW3 = RL_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  		esp_tempW3 = RR_ESC.lcesps16IdbDelTarPress/10;
  	}
  	else
  	{
  		esp_tempW3 = RL_ESC.lcesps16IdbDelTarPress/10;
  	}
    CAN_LOG_DATA[25] =(uint8_t)	(esp_tempW3);

    //CAN_LOG_DATA[26] =(uint8_t) (esp_tempW1);                         //ch16
    //CAN_LOG_DATA[27] =(uint8_t) (esp_tempW1>>8);

 	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  		esp_tempW3 = RR_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else
  	{
  		esp_tempW3 = RL_ESC.lcesps16IdbCurrentPress/10;
  	}
    CAN_LOG_DATA[26] =(uint8_t) (esp_tempW3);                         //ch16
    CAN_LOG_DATA[27] =(uint8_t) (esp_tempW3>>8);

    CAN_LOG_DATA[28] =(uint8_t) (alat);                               //ch17
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);

if(SLIP_CONTROL_NEED_FL==1)
{
    esp_tempW2 = del_target_slip_fl;
}
else if(SLIP_CONTROL_NEED_FR==1)
{
    esp_tempW2 = del_target_slip_fr;
}
else
{
    esp_tempW2 = 0;
}

    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW2);                             //ch18
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW2>>8);

    CAN_LOG_DATA[32] =(uint8_t) (Beta_MD);										//ch19
    CAN_LOG_DATA[33] =(uint8_t) (Beta_MD>>8);

    if(BTCS_fl == 1)
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16SecTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16SecTcTargetPress>>8);//(delta_moment_q_os>>8);
    }
    else if(BTCS_fr == 1)
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16PriTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16PriTcTargetPress>>8);//(delta_moment_q_os>>8);
    }
    else
    {
        CAN_LOG_DATA[34] =(uint8_t) (latcss16PriTcTargetPress);//(delta_moment_q_os);                      //ch20
        CAN_LOG_DATA[35] =(uint8_t) (latcss16PriTcTargetPress>>8);//(delta_moment_q_os>>8);
    }

 	if(SLIP_CONTROL_NEED_FL==1)
  	{
  	  esp_tempW3 = FL_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
  		esp_tempW3 = FR_ESC.lcesps16IdbCurrentPress/10;
  	}
  	else
  	{
  		esp_tempW3 = FL_ESC.lcesps16IdbCurrentPress/10;
  	}
    CAN_LOG_DATA[36] =(uint8_t) (esp_tempW3);                 //ch21
    CAN_LOG_DATA[37] =(uint8_t) (esp_tempW3>>8);

    if (AUTO_TM	== 1)
    {
        tempW0 = (INT)gear_pos;
    }
    else
    {
        tempW0 = (INT)gear_state;
    }

    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW0=FL_ESC.lcespu8CntBbsEsc;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
        esp_tempW0=RR_ESC.lcespu8CntBbsEsc;
    }
    else
    {
        esp_tempW0=FL_ESC.lcespu8CntBbsEsc;
    }

    if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW1=FR_ESC.lcespu8CntBbsEsc;
    }
    else if(SLIP_CONTROL_NEED_RL==1)
    {
        esp_tempW1=RL_ESC.lcespu8CntBbsEsc;
    }
    else
    {
        esp_tempW1=FR_ESC.lcespu8CntBbsEsc;
    }
    CAN_LOG_DATA[38] =(int8_t) (RL.u8_Estimated_Active_Press);//(esp_tempW0);//(FL_ESC.u8escNoArraytime);//(RL_ESC.lcespu8IdbDumpRateCnt);//(tempW0);                               		//ch22
    CAN_LOG_DATA[39] =(int8_t) (RR.u8_Estimated_Active_Press);//(esp_tempW1);//(RR_ESC.lcespu8IdbDumpRateCnt);//(mtp);                                    //ch23

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               		//ch24
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch25
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);

    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                  	//ch26
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);

    if(SLIP_CONTROL_NEED_FL==1)
    {
    	esp_tempW1=FL_ESC.lcesps16IdbriseDelTp;
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
    	esp_tempW1=FR_ESC.lcesps16IdbriseDelTp;
    }
    else
    {
    	esp_tempW1=FL_ESC.lcesps16IdbriseDelTp;
    }
    CAN_LOG_DATA[46] =(uint8_t) (esp_tempW1);					                		//ch27

    if(SLIP_CONTROL_NEED_RL==1)
    {
    	esp_tempW1=RL_ESC.lcesps16IdbriseDelTp;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
    	esp_tempW1=RR_ESC.lcesps16IdbriseDelTp;
    }
    else
    {
    	esp_tempW1=RL_ESC.lcesps16IdbriseDelTp;
    }
    CAN_LOG_DATA[47] =(uint8_t) (esp_tempW1);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 		//ch28
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);

    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                       		//ch29
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);

    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                 		//ch30
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);

    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);								//ch31
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);

    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                         //ch32
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);

    CAN_LOG_DATA[58] =(uint8_t) (FL.u8_Estimated_Active_Press);//(cal_torq);                 					    //ch33
    CAN_LOG_DATA[59] =(uint8_t) (FR.u8_Estimated_Active_Press);//(cal_torq>>8);

	if( yaw_out > 0 )
	{
	    esp_tempW7 = ay_vx_dot_sig_plus;
	}
	else
	{
	    esp_tempW7 = ay_vx_dot_sig_minus;
	}
    CAN_LOG_DATA[60] =(uint8_t) (RL_ESC.lcesps16IdbTarPress/10);   					                //ch34
    CAN_LOG_DATA[61] =(uint8_t) (FL_ESC.lcesps16CompDp/10);//(RL_ESC.lcesps16LimitIdbTargetP/10);


    CAN_LOG_DATA[62] =(uint8_t) (RR_ESC.lcesps16IdbTarPress/10);					                 //ch35
    CAN_LOG_DATA[63] =(uint8_t) (FR_ESC.lcesps16CompDp/10);//(RR_ESC.lcesps16LimitIdbTargetP/10);//(RR_ESC.lcesps16IdbTarPress>>8);

    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[64] =(uint8_t) (lsesps16AHBGEN3mpress);               //ch36
    CAN_LOG_DATA[65] =(uint8_t) (lsesps16AHBGEN3mpress>>8);
    #else
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                              //ch36
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);
    #endif

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;			                 //flag1
//    if(FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)      tempB5|=0x02;			//flag2
    if(ldespu1ESC1stCycle)      tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;											 //flag3
    if(EBD_RA)                      tempB5|=0x08;											 //flag4
    if(BTC_fz==1)                   tempB5|=0x10;											 //flag5
#if __TCS
    if(ETCS_ON)                     tempB5|=0x20;											 //flag6
#endif
    if(YAW_CDC_WORK)                tempB5|=0x40;											 //flag7
#if __TCS
    if(ESP_TCS_ON)                  tempB5|=0x80;											 //flag8
#endif
    if(lcu1US2WHControlFlag)       tempB4|=0x01;											 					//flag9

#if __ROP
    if(U1EscAfterStrokeR )          			tempB4|=0x02;											 //flag10
#else
	if(U1EscAfterStrokeR)         				tempB4|=0x02;											 //flag10
#endif
    if(PARTIAL_BRAKE)               tempB4|=0x04;											 //flag11

//   if(FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)           tempB4|=0x08;	//flag12
   if(HSA_flg)           tempB4|=0x08;	//flag12

    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn)    tempB4|=0x10;											 //flag13
    #else
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;											 //flag13
    #endif

    if(U1WheelValveCntr)     tempB4|=0x20;											 //flag14

    if(AUTO_TM) {
        if(BACK_DIR)                tempB4|=0x40;											 //flag15
    } else {
#if __AX_SENSOR
        if(BACKWARD_MOVE)           tempB4|=0x40;											 //flag15
#else
       if(0)           tempB4|=0x40;
#endif
    }

//#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;	//flag16
//#else
    if(U1EscAfterStrokeR_OLD)             tempB4|=0x80;	 //flag16
//#endif


    if(AV_VL_fl)                    tempB3|=0x01;											 //flag17
    if(HV_VL_fl)                    tempB3|=0x02;											 //flag18
    if(ABS_fl)                      tempB3|=0x04;											 //flag19

    if(U1StrokeRecoveryforESC)         tempB3|=0x08;											 //flag20


//#if !__SPLIT_TYPE
    if(FL_ESC.lcespu1DctMuxPrio)               tempB3|=0x10;											 //flag21
//#else
//    if(0)        tempB3|=0x10;											 //flag21
//#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;											 //flag22										 //flag22
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)                					 tempB3|=0x40;				//flag23
//    if(INITIAL_BRAKE||FLAG_ACTIVE_BOOSTER||FLAG_ACTIVE_BOOSTER_PRE)            tempB3|=0x80;				//flag24
    if(FLAG_ON_CENTER)            tempB3|=0x80;				//flag24

    if(AV_VL_fr)                    tempB2|=0x01;											 //flag25
    if(HV_VL_fr)                    tempB2|=0x02;											 //flag26
    if(ABS_fr)                      tempB2|=0x04;											 //flag27
#if __P_CONTROL
    if(0)                     tempB2|=0x08;
#else
    if(BTCS_fr)                     tempB2|=0x08;											 //flag28
#endif
//#if !__SPLIT_TYPE
    if(FR_ESC.lcespu1DctMuxPrio)               tempB2|=0x10;											 //flag29
//#else
//    if(0)          tempB2|=0x10;											 //flag29
//#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;											 //flag30
    if(YAW_CHANGE_LIMIT)          tempB2|=0x40;						//flag31

#if __BANK_ESC_IMPROVEMENT_CONCEPT
    if(ldespu1BankLowerSuspectFlag) tempB2|=0x80;											 //flag32
#else
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;											 //flag32
#endif

    if(AV_VL_rl)                    tempB1|=0x01;											 //flag33
    if(HV_VL_rl)                    tempB1|=0x02;											 //flag34
    if(ABS_rl)                      tempB1|=0x04;											 //flag35
    if(BTCS_fl||BTCS_rl) 						tempB1|=0x08;											 //flag36
    if(EBD_rl)                      tempB1|=0x10;											 //flag37
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;											 //flag38
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)                tempB1|=0x40;											 //flag39
#else
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)           tempB1|=0x40;											 //flag39
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(U1EscAfterStrokeR)          tempB1|=0x80;		//flag40
#else
    if(U1EscAfterStrokeR)        			tempB1|=0x80;											 //flag40
#endif

    if(AV_VL_rr)            				tempB0|=0x01;											 //flag41
    if(HV_VL_rr)            				tempB0|=0x02;											 //flag42
    if(ABS_rr)              				tempB0|=0x04;											 //flag43
#if __P_CONTROL
    if(0)             tempB0|=0x08;
#else
    if(CROSS_SLIP_CONTROL_rr||RR.U1EscOtherWheelCnt)    				tempB0|=0x08;											 //flag44
#endif
    if(EBD_rr)              				tempB0|=0x10;											 //flag45
    if(ESP_BRAKE_CONTROL_RR)  			tempB0|=0x20;											 //flag46
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)       				tempB0|=0x40;											 			//flag47
#else
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)     				tempB0|=0x40;											 //flag47
#endif

    if(CROSS_SLIP_CONTROL_rl||RL.U1EscOtherWheelCnt)          tempB0|=0x80;   //flag48


    CAN_LOG_DATA[66] =(uint8_t) (tempB5);				//ch40
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);				//ch41
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);       //ch42
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);       //ch43
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);       //ch44
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);       //ch45

    if(SLIP_CONTROL_NEED_RL==1)
    {
    	esp_tempW6 = del_target_slip_rl;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
    	esp_tempW6 = del_target_slip_rr;
    }
    else
    {
    	esp_tempW6 = 0;
    }
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);								//ch43
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6>>8);

    CAN_LOG_DATA[74] =(uint8_t) (FL.flags);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[75] =(uint8_t) (FR.flags);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[76] =(uint8_t) (FL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[77] =(uint8_t) (FR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
    CAN_LOG_DATA[78] =(uint8_t) (RL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[79] =(uint8_t) (RR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
}

void LSESP_vCallESCPlusLogData_Fadeout(void)
{
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_dump_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_dump_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}

    CAN_LOG_DATA[0]  =(uint8_t) (esp_tempW3);							//ch01
    CAN_LOG_DATA[1]  =(uint8_t) (esp_tempW3>>8);

    CAN_LOG_DATA[2]  =(uint8_t) (vrad_fl);                                		//ch02
    CAN_LOG_DATA[3]  =(uint8_t) (vrad_fl>>8);

    CAN_LOG_DATA[4]  =(uint8_t) (vrad_fr);                                		//ch03
    CAN_LOG_DATA[5]  =(uint8_t) (vrad_fr>>8);

    CAN_LOG_DATA[6]  =(uint8_t) (vrad_rl);                                		//ch04
    CAN_LOG_DATA[7]  =(uint8_t) (vrad_rl>>8);

    CAN_LOG_DATA[8]  =(uint8_t) (vrad_rr);                                		//ch05
    CAN_LOG_DATA[9]  =(uint8_t) (vrad_rr>>8);

    CAN_LOG_DATA[10] =(uint8_t) (delta_moment_q);								//ch06
    CAN_LOG_DATA[11] =(uint8_t) (delta_moment_q>>8);

#if __4WD ||__AX_SENSOR
	esp_tempW9= along;
#else
	esp_tempW9= afz;
#endif
	if(esp_tempW9 > 120) esp_tempW9 =120;
	else if(esp_tempW9< (-120)) esp_tempW9 = -120;

    CAN_LOG_DATA[12] =(uint8_t) ((int8_t)esp_tempW9);							//ch07

    CAN_LOG_DATA[13] =(uint8_t)(inhibition_number =INHIBITION_INDICATOR());		//ch08

    CAN_LOG_DATA[14] =(uint8_t) (vref5);										//ch09
    CAN_LOG_DATA[15] =(uint8_t) (vref5>>8);

	esp_tempW6= esp_mu/10;

	if(esp_tempW6 > 120) esp_tempW6 =120;
	else if(esp_tempW6< (-120)) esp_tempW6 = -120;

//    CAN_LOG_DATA[16] =(uint8_t)((int8_t)esp_tempW6);						//ch10
//    CAN_LOG_DATA[16] =(uint8_t)(la_RL1HP.u8OutletOnTime+la_RL2HP.u8OutletOnTime);						//ch10
	CAN_LOG_DATA[16] =(uint8_t)(FL_ESC.debuger);						//ch10

	if(lcu1espActive3rdFlg  == 1)
	{
		esp_tempW0 = delta_moment_q_under/100;
	}
	else
	{
		esp_tempW0 = delta_moment_beta/100;
	}
    CAN_LOG_DATA[17] =(uint8_t) (FR_ESC.debuger);     //ch11

    //CAN_LOG_DATA[18] =(uint8_t) (target_vol);									//ch12 for MGH only
    //CAN_LOG_DATA[19] =(uint8_t) (target_vol>>8);
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_RL==1)
  	{
  	  esp_tempW3 = RL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
  	  esp_tempW3 = RR_ESC.slip_control;
  	}
  	else
  	{
  	  esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[18] =(uint8_t) (esp_tempW3);									//ch12 for MGH only
    CAN_LOG_DATA[19] =(uint8_t) (esp_tempW3>>8);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_control;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_control;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
#else
  	if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))
		{
  	  esp_tempW3 = slip_control_front;
		}
		else
		{
  	  esp_tempW3 = 0;
		}
#endif
    CAN_LOG_DATA[20] =(esp_tempW3);              																															//ch13
    CAN_LOG_DATA[21] =(esp_tempW3>>8);
    CAN_LOG_DATA[22] =(uint8_t)	(FL_ESC.lcesps16IdbTarPress/10);																																//ch14
    CAN_LOG_DATA[23] =(uint8_t)	(FL_ESC.lcesps16IdbDelTarPress/10);

    CAN_LOG_DATA[24] =(uint8_t) (FR_ESC.lcesps16IdbTarPress/10);//(esp_tempW0);                         //ch15
    CAN_LOG_DATA[25] =(uint8_t) (FR_ESC.lcesps16IdbDelTarPress/10);//(esp_tempW0>>8);

    CAN_LOG_DATA[26] =(uint8_t) (FL.s16_Estimated_Active_Press);                         //ch16
    CAN_LOG_DATA[27] =(uint8_t) (FL.s16_Estimated_Active_Press>>8);

    CAN_LOG_DATA[28] =(uint8_t) (alat);                               //ch17
    CAN_LOG_DATA[29] =(uint8_t) (alat>>8);

//	if(SLIP_CONTROL_NEED_FL==1)
//	{
//		esp_tempW2 = del_target_slip_fl;
//	}
//	else if(SLIP_CONTROL_NEED_FR==1)
//	{
//		esp_tempW2 = del_target_slip_fr;
//	}
//	else
//	{
//		esp_tempW2 = 0;
//	}
  	if(SLIP_CONTROL_NEED_RL==1)
	{
		esp_tempW3 = RL_ESC.slip_dump_thr_change;
	}
  	else if(SLIP_CONTROL_NEED_RR==1)
	{
		esp_tempW3 = RR_ESC.slip_dump_thr_change;
	}
	else
	{
		esp_tempW3 = 0;
	}

    CAN_LOG_DATA[30] =(uint8_t) (esp_tempW3);                             //ch18
    CAN_LOG_DATA[31] =(uint8_t) (esp_tempW3>>8);

  	if(SLIP_CONTROL_NEED_FL==1)
  	{
		esp_tempW3 = FL_ESC.slip_rise_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_FR==1)
  	{
		esp_tempW3 = FR_ESC.slip_rise_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
    CAN_LOG_DATA[32] =(uint8_t) (esp_tempW3); //(FL_ESC.slip_m);//(mpress);//(Beta_MD);										//ch19
    CAN_LOG_DATA[33] =(uint8_t) (esp_tempW3>>8); //(FL_ESC.slip_m>>8);//(mpress>>8);//(Beta_MD>>8);

  	if(SLIP_CONTROL_NEED_RL==1)
  	{
		esp_tempW3 = RL_ESC.slip_rise_thr_change;
  	}
  	else if(SLIP_CONTROL_NEED_RR==1)
  	{
		esp_tempW3 = RR_ESC.slip_rise_thr_change;
  	}
  	else
  	{
		esp_tempW3 = 0;
  	}
    CAN_LOG_DATA[34] =(uint8_t) (esp_tempW3); //(FR_ESC.slip_m);//(delta_moment_q_os);                      //ch20
    CAN_LOG_DATA[35] =(uint8_t) (esp_tempW3>>8); //(FR_ESC.slip_m>>8);//(delta_moment_q_os);                      //ch20

    CAN_LOG_DATA[36] =(uint8_t) (FR.s16_Estimated_Active_Press);//(eng_torq);                               //ch21
    CAN_LOG_DATA[37] =(uint8_t) (FR.s16_Estimated_Active_Press>>8);//(eng_torq>>8);

    CAN_LOG_DATA[38] =(uint8_t) (RL_ESC.u8WhlVlvDumpTi);                               		//ch22
    CAN_LOG_DATA[39] =(uint8_t) (RR_ESC.u8WhlVlvDumpTi);                                    //ch23

    CAN_LOG_DATA[40] =(uint8_t) (wstr);                               		//ch24
    CAN_LOG_DATA[41] =(uint8_t) (wstr>>8);

	CAN_LOG_DATA[42] =(uint8_t) (yaw_out);                                //ch25
    CAN_LOG_DATA[43] =(uint8_t) (yaw_out>>8);

    CAN_LOG_DATA[44] =(uint8_t) (rf2);                                  	//ch26
    CAN_LOG_DATA[45] =(uint8_t) (rf2>>8);

//    CAN_LOG_DATA[46] =(uint8_t) (drive_torq);					                		//ch27
//    CAN_LOG_DATA[47] =(uint8_t) (drive_torq>>8);
    CAN_LOG_DATA[46] =(uint8_t) (RL_ESC.debuger);					                		//ch27
    CAN_LOG_DATA[47] =(uint8_t) (RR_ESC.debuger);

    CAN_LOG_DATA[48] =(uint8_t) (sign_o_delta_yaw_thres);                 		//ch28
    CAN_LOG_DATA[49] =(uint8_t) (sign_o_delta_yaw_thres>>8);

    CAN_LOG_DATA[50] =(uint8_t) (nosign_delta_yaw);                       		//ch29
    CAN_LOG_DATA[51] =(uint8_t) (nosign_delta_yaw>>8);

    CAN_LOG_DATA[52] =(uint8_t) (nosign_delta_yaw_first);                 		//ch30
    CAN_LOG_DATA[53] =(uint8_t) (nosign_delta_yaw_first>>8);

    CAN_LOG_DATA[54] =(uint8_t) (det_alatc_fltr);								//ch31
    CAN_LOG_DATA[55] =(uint8_t) (det_alatc_fltr>>8);

    CAN_LOG_DATA[56] =(uint8_t) (det_alatm_fltr);                         //ch32
    CAN_LOG_DATA[57] =(uint8_t) (det_alatm_fltr>>8);

//    CAN_LOG_DATA[58] =(uint8_t) (cal_torq);                 					    //ch33
//    CAN_LOG_DATA[59] =(uint8_t) (cal_torq>>8);

    CAN_LOG_DATA[58] =(uint8_t) (RL_ESC.lcespu8IdbDumpRateCnt);                 					    //ch33
    CAN_LOG_DATA[59] =(uint8_t) (RR_ESC.lcespu8IdbDumpRateCnt);

	if( yaw_out > 0 )
	{
	    esp_tempW7 = ay_vx_dot_sig_plus;
	}
	else
	{
	    esp_tempW7 = ay_vx_dot_sig_minus;
	}

    CAN_LOG_DATA[60] =(uint8_t) (RL_ESC.lcesps16IdbTarPress/10);   					                //ch34
    CAN_LOG_DATA[61] =(uint8_t) (FL_ESC.lcespu8IdbDumpRateCnt);//(RL_ESC.lcesps16LimitIdbTargetP/10);


    CAN_LOG_DATA[62] =(uint8_t) (RR_ESC.lcesps16IdbTarPress/10);					                 //ch35
    CAN_LOG_DATA[63] =(uint8_t) (FR_ESC.lcespu8IdbDumpRateCnt);//(RR_ESC.lcesps16LimitIdbTargetP/10);//(RR_ESC.lcesps16IdbTarPress>>8);

#if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    CAN_LOG_DATA[64] =(uint8_t) (lis16DriverIntendedTP);               //ch36
    CAN_LOG_DATA[65] =(uint8_t) (lis16DriverIntendedTP>>8);
#else
    CAN_LOG_DATA[64] =(uint8_t) (mpress);                              //ch36
    CAN_LOG_DATA[65] =(uint8_t) (mpress>>8);
#endif

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;			                 //flag1
//    if(FLAG_1ST_CYCLE_FL||FLAG_1ST_CYCLE_FR||FLAG_1ST_CYCLE_RL||FLAG_1ST_CYCLE_RR)      tempB5|=0x02;			//flag2
    if(ldespu1ESC1stCycle)      tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;											 //flag3
    if(EBD_RA)                      tempB5|=0x08;											 //flag4
    if(BTC_fz==1)                   tempB5|=0x10;											 //flag5
#if __TCS
    if(ETCS_ON)                     tempB5|=0x20;											 //flag6
#endif
    if(YAW_CDC_WORK)                tempB5|=0x40;											 //flag7
#if __TCS
    if(ESP_TCS_ON)                  tempB5|=0x80;											 //flag8
#endif
    if(lcu1US2WHControlFlag)       tempB4|=0x01;											 					//flag9

#if __ROP
//    if(lcabsu1StrkReqPossibleWhlSt )          			tempB4|=0x02;											 //flag10
#else
//	if(lcabsu1StrkReqPossibleWhlSt)         				tempB4|=0x02;											 //flag10
#endif
    if(PARTIAL_BRAKE)               tempB4|=0x04;											 //flag11

//   if(FLAG_1ST_CYCLE_WHEEL_FL||FLAG_1ST_CYCLE_WHEEL_FR||FLAG_1ST_CYCLE_WHEEL_RL||FLAG_1ST_CYCLE_WHEEL_RR)           tempB4|=0x08;	//flag12
   if(ldespu1ESC2ndCycle)           tempB4|=0x08;	//flag12

    #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn)    tempB4|=0x10;											 //flag13
    #else
    if(MPRESS_BRAKE_ON)             tempB4|=0x10;											 //flag13
    #endif

    if(U1WheelValveCntr)     tempB4|=0x20;											 //flag14

    if(AUTO_TM) {
        if(BACK_DIR)                tempB4|=0x40;											 //flag15
    } else {
#if __AX_SENSOR
    if(BACKWARD_MOVE)				tempB4|=0x40;											 //flag15
#else
       if(0)           tempB4|=0x40;
#endif
    }

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(FL_ESC.FLAG_COMPULSION_HOLD||FR_ESC.FLAG_COMPULSION_HOLD||RL_ESC.FLAG_COMPULSION_HOLD||RR_ESC.FLAG_COMPULSION_HOLD)             tempB4|=0x80;	//flag16
#else
    if(FLAG_COMPULSION_HOLD_REAR||FLAG_COMPULSION_HOLD)             tempB4|=0x80;	 //flag16
#endif

    if(AV_VL_fl)                    tempB3|=0x01;											 //flag17
    if(HV_VL_fl)                    tempB3|=0x02;											 //flag18
    if(ABS_fl)                      tempB3|=0x04;											 //flag19

    if(U1StrokeRecoveryforESC)         tempB3|=0x08;											 //flag20

#if !__SPLIT_TYPE
    if(0)               tempB3|=0x10;											 //flag21
#else
    if(0)        tempB3|=0x10;											 //flag21
#endif
    if(ESP_BRAKE_CONTROL_FL)        tempB3|=0x20;											 //flag22
    if(ESP_PULSE_DUMP_FLAG_F||ESP_PULSE_DUMP_FLAG_R)                					 tempB3|=0x40;				//flag23
//    if(INITIAL_BRAKE||FLAG_ACTIVE_BOOSTER||FLAG_ACTIVE_BOOSTER_PRE)            tempB3|=0x80;				//flag24
    if(FLAG_ON_CENTER)            tempB3|=0x80;				//flag24

    if(AV_VL_fr)                    tempB2|=0x01;											 //flag25
    if(HV_VL_fr)                    tempB2|=0x02;											 //flag26
    if(ABS_fr)                      tempB2|=0x04;											 //flag27
#if __P_CONTROL
    if(0)                     tempB2|=0x08;
#else
    if(BTCS_fr)                     tempB2|=0x08;											 //flag28
#endif
#if !__SPLIT_TYPE
    if(0)               tempB2|=0x10;											 //flag29
#else
    if(0)          tempB2|=0x10;											 //flag29
#endif
    if(ESP_BRAKE_CONTROL_FR)        tempB2|=0x20;											 //flag30
    if(YAW_CHANGE_LIMIT)          tempB2|=0x40;						//flag31

#if __BANK_ESC_IMPROVEMENT_CONCEPT
    if(ldespu1BankLowerSuspectFlag) tempB2|=0x80;											 //flag32
#else
    if(FLAG_BANK_DETECTED)          tempB2|=0x80;											 //flag32
#endif

    if(AV_VL_rl)                    tempB1|=0x01;											 //flag33
    if(HV_VL_rl)                    tempB1|=0x02;											 //flag34
    if(ABS_rl)                      tempB1|=0x04;											 //flag35
    if(BTCS_fl||BTCS_rl) 			tempB1|=0x08;											 //flag36
    if(EBD_rl)                      tempB1|=0x10;											 //flag37
    if(ESP_BRAKE_CONTROL_RL)        tempB1|=0x20;											 //flag38
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)                tempB1|=0x40;											 //flag39
#else
    if(CROSS_SLIP_CONTROL_fl||FL.U1EscOtherWheelCnt)           tempB1|=0x40;											 //flag39
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(U1EscAfterStrokeR)          tempB1|=0x80;		//flag40
#else
    if(U1EscAfterStrokeR)        			tempB1|=0x80;											 //flag40
#endif

	if(AV_VL_rr)            				tempB0|=0x01;											 //flag41
    if(HV_VL_rr)            				tempB0|=0x02;											 //flag42
    if(ABS_rr)              				tempB0|=0x04;											 //flag43
#if __P_CONTROL
    if(0)             tempB0|=0x08;
#else
    if(CROSS_SLIP_CONTROL_rr||RR.U1EscOtherWheelCnt)    				tempB0|=0x08;											 //flag44
#endif
    if(EBD_rr)              				tempB0|=0x10;											 //flag45
    if(ESP_BRAKE_CONTROL_RR)  			tempB0|=0x20;											 //flag46
#if !__SPLIT_TYPE
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)       				tempB0|=0x40;											 			//flag47
#else
    if(CROSS_SLIP_CONTROL_fr||FR.U1EscOtherWheelCnt)     				tempB0|=0x40;											 //flag47
#endif

    if(CROSS_SLIP_CONTROL_rl||RL.U1EscOtherWheelCnt)          tempB0|=0x80;   //flag48


    CAN_LOG_DATA[66] =(uint8_t) (tempB5);				//ch40
    CAN_LOG_DATA[67] =(uint8_t) (tempB4);				//ch41
    CAN_LOG_DATA[68] =(uint8_t) (tempB3);       //ch42
    CAN_LOG_DATA[69] =(uint8_t) (tempB2);       //ch43
    CAN_LOG_DATA[70] =(uint8_t) (tempB1);       //ch44
    CAN_LOG_DATA[71] =(uint8_t) (tempB0);       //ch45

    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW6 = FL_ESC.lcesps16IdbDumpInterval;
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW6 = FR_ESC.lcesps16IdbDumpInterval;
    }
    else
    {
        esp_tempW6 = 0;
    }
    CAN_LOG_DATA[72] =(uint8_t) (esp_tempW6);								//ch43

    if(SLIP_CONTROL_NEED_RL==1)
    {
        esp_tempW6 = RL_ESC.lcesps16IdbDumpInterval;
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
        esp_tempW6 = RR_ESC.lcesps16IdbDumpInterval;
    }
    else
    {
        esp_tempW6 = 0;
    }
    CAN_LOG_DATA[73] =(uint8_t) (esp_tempW6);

    CAN_LOG_DATA[74] =(uint8_t) (FL_ESC.slip_m/10);
    CAN_LOG_DATA[75] =(uint8_t) (FR_ESC.slip_m/10);
    CAN_LOG_DATA[76] =(uint8_t) (RL_ESC.slip_m/10);
    CAN_LOG_DATA[77] =(uint8_t) (RR_ESC.slip_m/10);
    CAN_LOG_DATA[78] =(uint8_t) (FL_ESC.u8WhlVlvDumpTi);//(RL.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_P/10);			//ch44
    CAN_LOG_DATA[79] =(uint8_t) (FR_ESC.u8WhlVlvDumpTi);//(RR.lcesps16IdbOtherPress/10);//(MFC_Current_ESP_S/10);
}
#endif /*#if (__IDB_LOGIC==ENABLE)*/

#endif /* #if !__VDC */
#endif /* #if defined (__LOGGER) */
#endif /* #if __LOGGER_DATA_TYPE==1 */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSCallLogData
	#include "Mdyn_autosar.h"
#endif
