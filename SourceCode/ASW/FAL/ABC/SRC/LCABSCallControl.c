/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LCABSCallControl.C
* Description: ABS control algorithm
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCABSCallControl        
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCHSACallControl.h"
#include "LCABSCallControl.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "LCHDCCallControl.h"
#include "LSESPCalSensorOffset.h"
#include "LCABSCallESPCombination.h"
#include "LSESPFilterEspSensor.h"

#include "LCABSStrokeRecovery.h"
#include "LAIDB_DecideTargetPress.h"
/* Local Definiton  **********************************************************/

#define M_TEMP_CODE_FOR_HMC_DEMO

/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/
static void LCABS_vDecideABSInhibition(void);

  #if __VDC
static void LCABS_vCooperateWithESP(void);
  #endif
  #if __UCC_COOPERATION_CONTROL
static void LCABS_vCooperateWithUCC(void);
  #endif  

/* GlobalFunction prototype **************************************************/


        
/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LCABS_vDecideABSInhibition
* CALLED BY:          LCABS_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Decide ABS control Inhibition for sensor error situation
******************************************************************************/
static void LCABS_vDecideABSInhibition(void)
{	
	ABS_INHIBITION_flag = 0;
	Front_ABS_INHIBITION_flag = 0;
	REAR_ABS_INHIBITION_flag = 0;
	REAR_EBD_INHIBITION_flag = 0;
	
  #if (__GM_FailM==0)
    if(abs_error_flg==1)
  #else
	if((fu1ABSEcuHWErrDet==1)||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1))
	/* Difference from HMC : G sensor Error, Variant Code Error, ee_ece_flg */
  #endif
	{
		if(ABS_fz==1)
		{
			if((fu1WheelFLErrDet==1) && (ABS_fr==0)&&(ABS_rl==0)&&(ABS_rr==0))
			{
				ABS_INHIBITION_flag = 1;
			}
			else if((fu1WheelFRErrDet==1) && (ABS_fl==0)&&(ABS_rl==0)&&(ABS_rr==0))
			{
				ABS_INHIBITION_flag = 1;
			}
			else if((fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1))
			{
				Front_ABS_INHIBITION_flag = 1;
				REAR_ABS_INHIBITION_flag = 1;
			}
			else if(fu1MotErrFlg==1) 
			{
				Front_ABS_INHIBITION_flag = 1;
			}
			else { }
		}
		else
		{
			ABS_INHIBITION_flag = 1;
		}
	}
	else { }
	
	if((fu1OnDiag==1)||(init_end_flg==0)||
      (fu1ECUHwErrDet==1)||(fu1RearSolErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1SameSideWSSErrDet==1)
      #if (__GM_FailM==0)
       ||(fu1DiagonalWSSErrDet==1)
      #endif
    ) /* ebd_error_flg */
	{
	    lcabsu1EbdInhibitCondition = 1;
	}
	else
	{
	    lcabsu1EbdInhibitCondition = 0;
	}
	
	if(lcabsu1EbdInhibitCondition==1)
	{
	    ABS_INHIBITION_flag = 1;
	    REAR_EBD_INHIBITION_flag = 1;
	}
	else if(fu1VoltageLowErrDet==1)
	{
		ABS_INHIBITION_flag = 1;
		REAR_EBD_INHIBITION_flag = 1;
	}
	else if(wu8IgnStat == CE_OFF_STATE)
	{
	    ABS_INHIBITION_flag = 1;
	    REAR_EBD_INHIBITION_flag = 1;
	}
  #if __DISABLE_CTRL_AT_OFF_ACC_CRANK
	else if(fu8EngineModeStep==0) /* if Key is in OFF , ACC , Crank position, Inhibit all controls */
	{
	    ABS_INHIBITION_flag = 1;
	    REAR_EBD_INHIBITION_flag = 1;
	}
  #endif
	else if(fu1VoltageUnderErrDet==1)
	{
		Front_ABS_INHIBITION_flag = 1;
	}
	else { }
	
	if((ABS_INHIBITION_flag==1)||(Front_ABS_INHIBITION_flag==1)||(REAR_ABS_INHIBITION_flag==1))
	{
	    lcabsu1AbsErrorFlag=1;
	}
	else
	{
	    lcabsu1AbsErrorFlag=0;
	}

    if(max_speed<=VREF_2_KPH_RESOL_CHANGE)
    {
        if((ABS_fz==0)&&(EBD_RA==0))
        {
            lcabsu1LowSpeedInhibitFlag=0; /* kph_7_flg=0; */
        }
        else
        {
            ;
        }
    }
    else 
    {
	  #if (ABS_CONTROL_MIN_SPEED_5KPH==ENABLE)
    	if((min_speed>=VREF_5_KPH_RESOL_CHANGE)
      #else
        if((min_speed>=VREF_7_KPH_RESOL_CHANGE)
      #endif

      #if __HDC
    	  || ((lcu1HdcActiveFlg==1) && (vref>=VREF_5_KPH))
      #endif
        )
    	{
        	lcabsu1LowSpeedInhibitFlag=1; /* kph_7_flg=1; */
        }
        else
        {
        	;
        }
    }		
    
  #if __VDC
    if((fu1YawSenPwr1sOk==1)&&(fu1AySenPwr1sOk==1)&&(fu1SteerSenPwr1sOk==1)
     &&(fu1YawErrorDet==0)&&(fu1AyErrorDet==0)&&(fu1StrErrorDet==0)
     &&(fu1StrSusDet==0)&&(fu1YawSusDet==0)&&(fu1AySusDet==0)
     &&(BACK_DIR==0)&&(SAS_CHECK_OK==1))
    {
    	lcabsu1ValidYawStrLatG = 1;
    }
    else
    {
    	lcabsu1ValidYawStrLatG = 0;
    }
    
    if((fu1McpSenPwr1sOk==1)&&(fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(lsespu1MpressOffsetOK==1))
    {
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    	lcabsu1ValidMpress = 0;
      #else	
    	lcabsu1ValidMpress = 1;
      #endif 
    }
    else
    {
    	lcabsu1ValidMpress = 0;
    }
    
   #if __BRK_SIG_MPS_TO_PEDAL_SEN
    if((fu1PedalErrDet==1) || (lsespu1BrkAppSenInvalid==1))
    {
        lcabsu1ValidPedalTravel = 0;
    }
    else
    {
        lcabsu1ValidPedalTravel = 1;
    }

	#if defined M_TEMP_CODE_FOR_HMC_DEMO
	 lcabsu1ValidPedalTravel = 1;	/*temporaly set because of pedal invalid signal*/
	#endif

   #endif/*__BRK_SIG_MPS_TO_PEDAL_SEN*/

   #if (__AHB_SYSTEM==ENABLE)
   	if(lcabsu1ValidPedalTravel==1)
   	{
   		lcabsu1ValidBrakeSensor = 1;
   		lcabsu1BrakeSensorOn = lsespu1PedalBrakeOn;
   		lcabss16RefMpress = lsesps16EstBrkPressByBrkPedalF; /* Braking intention */
   		lcabss16RefCircuitPress = mpress;					/* Braking master pressure */
   	}
   	else if(lcabsu1ValidMpress==1)
   	{
   		lcabsu1ValidBrakeSensor = 1;
   		lcabsu1BrakeSensorOn = MPRESS_BRAKE_ON;
   		lcabss16RefMpress = mpress;
   		lcabss16RefCircuitPress = mpress;
   	}
   	else
   	{
   		lcabsu1ValidBrakeSensor = 0;
   		lcabsu1BrakeSensorOn = 0;
   		lcabss16RefMpress = 0;
   		lcabss16RefCircuitPress = 0;
   	}
   	   	
   #elif ((__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE))
   	if(lcabsu1ValidPedalTravel==1)
   	{
   		lcabsu1ValidBrakeSensor = 1;
   		lcabsu1BrakeSensorOn = lsespu1PedalBrakeOn;/*MPRESS_BRAKE_ON*/
   		lcabss16RefMpressOld = lcabss16RefMpress;
		 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
   		lcabss16RefMpress = Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl;
		 #else
   		lcabss16RefMpress = lsesps16EstBrkPressByBrkPedal;
		 #endif
   	}
   	else
   	{
   		lcabsu1ValidBrakeSensor = 0;
		lcabsu1BrakeSensorOn = 0;
		lcabss16RefMpressOld = lcabss16RefMpress;
   		lcabss16RefMpress = 0;
   	}
   	
	if(lsespu1MpresByAHBgen3Invalid==0)
	{
		if(lcabsu1ValidPedalTravel==0)
		{
			lcabsu1BrakeSensorOn = lsespu1MpresBrkOnByAHBgen3;/*MPRESS_BRAKE_ON*/
		}
  		lcabss16RefCircuitPress = lsesps16MpressByAHBgen3;
  	}
  	else
  	{
   		lcabsu1BrakeSensorOn = 0;
   		lcabss16RefCircuitPress = 0;
   		if(lcabsu1ValidPedalTravel==0)
   		{
   			lcabsu1BrakeSensorOn = 0;
   		}
   	}

	if((0)
		#if __HSA ==ENABLE
		||((HSA_flg == 1) && (HSA_INHIBITION_flag == 0))
		#endif
		#if __HDC == ENABLE
		||((lcu1HdcActiveFlg == 1) && (fu1HDCErrorDet == 0))
		#endif
		#if __EPB_INTERFACE ==ENABLE
		||((lcu1EpbActiveFlg == 1) && (lcu1EpbInhibitFlg == 0))
		#endif
		#if __SCC==ENABLE
		||((lcu8WpcCtrlReq == 1) && (lcu1SccInhibitFlg == 0))
		#endif
		)
	{
		 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		lcabss16RefActiveTargetPress = ldidbs16RepresentWheelTargetP;
		 #else
		lcabss16RefActiveTargetPress = ldidbs16RepresentWheelTargetP/10; /*Mux output target Press 0.01bar resol */
		 #endif
	}
	else
	{
		lcabss16RefActiveTargetPress = 0;
	}
	
	lcabss16RefFinalTargetPres = ((lcabss16RefActiveTargetPress>lcabss16RefMpress) ? lcabss16RefActiveTargetPress  : lcabss16RefMpress);

   #else
   	if(lcabsu1ValidMpress==1)
   	{
   		lcabsu1ValidBrakeSensor = 1;
   		lcabsu1BrakeSensorOn = MPRESS_BRAKE_ON;
   		lcabss16RefMpress = mpress;
   		lcabss16RefCircuitPress = mpress;
   	}
   	else
   	{
   		lcabsu1ValidBrakeSensor = 0;
   		lcabsu1BrakeSensorOn = 0;
   		lcabss16RefMpress = 0;
   		lcabss16RefCircuitPress = 0;
   	}
   #endif/*__AHB_SYSTEM*/   
   
  #endif/*__VDC*/
  
    if((fu1BlsSusDet==0)&&(fu1BLSErrDet==0))
    {
    	lcabsu1ValidBLS = 1;
    }
    else
    {
    	lcabsu1ValidBLS = 0;
    }
    
    if((fu1BLSErrDet==1)||(fu1BlsSusDet==1))
    {
    	lcabsu1FaultBLS = 1;	
    }
    else
    {
    	lcabsu1FaultBLS = 0;
    }
    
  #if (__TCS || __ETC || __EDC)
    if((fu1MainCanLineErrDet==0)&&(fu1EMSTimeOutErrDet==0)&&(fu1TCUTimeOutErrDet==0))
    {
    	lcabsu1ValidCan = 1;
    }
    else
    {
    	lcabsu1ValidCan = 0;
    }
  #endif  
}


/******************************************************************************
* FUNCTION NAME:      LCABS_vCallControl
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Main Function for ABS control
******************************************************************************/
void LCABS_vCallControl(void)
{
	
	LCABS_vDecideABSInhibition();

  #if __VDC
	LCABS_vCooperateWithESP();
  #endif    

  #if __UCC_COOPERATION_CONTROL
	LCABS_vCooperateWithUCC();
  #endif
  	
  	LCABS_vSetWheelDumpThreshold();

	
	LCABS_vStrokeRecoveryMain();
	
	
	LCABS_vCallYawMomentReduction();
	
  #if !__VDC
    LCABS_vResetABS();                          // GK34
  #else
    if(ESP_ERROR_FLG==1) {
        LCABS_vResetABS();
    }
  #endif
  	
  #if __ABS_RBC_COOPERATION
    LCABS_vDecidePressRecoveryBfAbs();
  #endif
/*******Braking status by Mpress*******/ /*checking need to move*/
  #if __SLIGHT_BRAKING_COMPENSATION
    LCABS_vDecideBrakingStatus();
  #endif  
  #if __VDC
  	LCABS_vCheckMpressSlope();
  #endif
/**************************************/
  
  	LCABS_vSetLFCHoldTimer();
  	
  	LCABS_vDecideWheelCtrlState();

	LCABS_vDecideWheelCtrlVariable();
    	
	LCABS_vDecideRoadWhVehStatus();
	
	LCABS_vDecideVehicleCtrlState();

	LCABS_vDecideWhlTargetP();

	LCABS_vDecideCtrlActuationVars();
}

  #if __VDC
static void LCABS_vCooperateWithESP(void)
{
    LCABS_vChangeESPtoABS();
    LCABS_vChangeABStoESP();
}
  #endif    

  #if __UCC_COOPERATION_CONTROL
static void LCABS_vCooperateWithUCC(void)
{	
	LCCDC_vSetABSCyclePotHole(&FL);
	LCCDC_vSetABSCyclePotHole(&FR);
}
  #endif

/******************************************************************************
* FUNCTION NAME:      
* CALLED BY:          
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       16bit integer
* Description:        Min, Max Limitation Function
******************************************************************************/

/* <skeon OPTIME_MACFUNC_2> 110303*/
#ifndef OPTIME_MACFUNC_2

int16_t LCABS_s16LimitMinMax(int16_t LimitedVariable,int16_t MinValue,int16_t MaxValue)
{
    if(LimitedVariable < MinValue)
    {
        LimitedVariable = MinValue;
    }
    else if(LimitedVariable > MaxValue)
    {
        LimitedVariable = MaxValue;
    }
    else
    {
        ;
    }
    
    return LimitedVariable;
}

#endif
/* </skeon OPTIME_MACFUNC_2>*/

/******************************************************************************
* FUNCTION NAME:      
* CALLED BY:          
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       16bit integer
* Description:        Select Higher Value
******************************************************************************/
int16_t LCABS_s16SelectHigherValue(int16_t FirstInput,int16_t SecondInput)
{
    if(FirstInput > SecondInput)
    {
        return FirstInput;
    }
    else
    {
        return SecondInput;
    }
}

/******************************************************************************
* FUNCTION NAME:      
* CALLED BY:          
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       16bit integer
* Description:        Decrease Input Value
******************************************************************************/

/* <skeon OPTIME_MACFUNC_2> 110303*/
#ifndef OPTIME_MACFUNC_2

int16_t LCABS_s16DecreaseUnsignedCnt(int16_t Cnt_old, int16_t increasement)
{
    if((Cnt_old+increasement)>0)
    {
        return (Cnt_old+increasement);
    }
    else
    {
        return 0;
    }
}
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCABSCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
/* </skeon OPTIME_MACFUNC_2>*/
