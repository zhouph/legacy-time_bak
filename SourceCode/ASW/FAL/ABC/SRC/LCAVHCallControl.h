#ifndef __LCAVHCALLCONTROL_H__
#define __LCAVHCALLCONTROL_H__

/*includes*********************************************************************/
#include "LVarHead.h"

#if __AVH
/********************* Logcal Definiton ***************************************/

/********************* AVH Test Mode ******************************************/
#define __AVH_SWITCH_USING_ESC_SW					0
#define __AVH_SWITCH_USING_CTRL_BOX                 0
/******************************************************************************/

/******************************************************************************/
#define	__AUTONOMOUS_HOLD_ENABLE					1
#define __AVH_SWITCH_OFF_CONCEPT                    1   /* 1 : HMC concept(do not off AVH W/O brake),  0 : VW concept */
#define __AVH_HOOD_TRUNK_CONCEPT                    1   /* 1 : not related gear state  0 : D&hood, R&trunk */
#define __GEAR_POSITION_SENSOR_ENABLE               0  
#define __AVH_INFINITE_HOLD_EPB_FAIL                0

#define __SENSITIVE_ROLL_DETECT                     1
#define __AVH_EXIT_AT_SBW_GEAR_P                    1
#define __AVH_MAXHILL_UP_DW_SEPARATE                1
#define __AVH_NOISE_REDUCTION                       1
#define __AVH_ACT_1IGN_DECOUNT                      1

  #if (__CAR_MAKER==HMC_KMC) 
#define __USE_MTP_RESOL_1000                        1   /* 110321 VF sign off */
  #else
#define __USE_MTP_RESOL_1000                        0
  #endif  

#define U8_AVH_DRIVER_INSIDE_DCT_TIME        L_U8_TIME_10MSLOOP_100MS
#define U8_AVH_DRIVER_OUTSIDE_DCT_TIME       L_U8_TIME_10MSLOOP_100MS
#define U8_AVH_AT_PARK_GEAR_DELAY_TIME       L_U8_TIME_10MSLOOP_500MS

/* AHB_GEN3_SYSTEM pedal on/off detection */
#define U8_AHB_AVH_PEDAL_ON_TH_TIME   			 L_U8_TIME_10MSLOOP_100MS
#define S16_AHB_AVH_PEDAL_ON_TH       			 MPRESS_3BAR
#define S16_AHB_AVH_PEDAL_OFF_TH       			 MPRESS_2BAR
/* AHB_GEN3_SYSTEM pedal on/off detection */

/*****************************************************************************/

/*****************************************************************************/
#define AVH_EPB_NO_REQUEST                          0
#define AVH_EPB_RELEASE_REQUEST                     1
#define AVH_EPB_ACT_REQUEST                         2

#define AVH_ALARM_REQUEST_OFF                       0
#define AVH_ALARM_REQUEST_ON                        1

#define EPB_UKNOWN                                  0 
#define EPB_RELEASED                                1
#define EPB_CLAMPING                                2
#define EPB_CLAMPED                                 3
#define EPB_RELEASING                               4
#define EPB_DYNAMIC_BRK                             5
#define EPB_RELEASING_BY_SW                         6
#define EPB_CLAMPING_BY_SW                          7

/*****************************************************************************/
#define AVH_IDLETOQ_MIN_TORQ        		        -50 /*  0% */

/*************************** Roll back control *******************************/
  #if (__SMART_WHEEL_SENSOR==ENABLE)
#define SENSITIVE_ROLL_TH                            60  /*  60mm */  
  #else
#define SENSITIVE_ROLL_TH                            20  /*  20mm */
  #endif
/*****************************************************************************/

/*****************************************************************************/

#define TC_CUR_AVG_PERIOD                       L_U8_TIME_10MSLOOP_250MS 
#define TC_CUR_DECOUNT_PERIOD                   L_U8_TIME_10MSLOOP_1000MS

/* 
#define S16_AVH_ABNORMAL_F_O_MIN_DUTY           5
#define S16_AVH_DETECTION_HILL_TH               5
#define S16_AVH_REBRAKE_DCT_THRESHOLD			MPRESS_3BAR
#define U8_AVH_REBRAKE_DCT_TIME					T_150_MS
#define S16_AVH_DEFAULT_IDLE_TORQ               100
#define S16_AVH_ISG_IDLETOQ_INHIBIT_TIME 		L_U8_TIME_10MSLOOP_600MS
*/

/**********************************DO NOT CHANGE******************************/

/* tuning parameter */


  /* AHB gen3 Target press interface */
#define U8_AVH_TC_DROP_MIN_REF	    1
#define S16_AVH_TP_DROP_MIN     	MPRESS_0G2BAR
#define U8_AVH_TC_DROP_MID_REF    	10 
#define S16_AVH_TP_DROP_MID		  	MPRESS_1BAR
#define U8_AVH_TC_DROP_MAX_REF    	50
#define S16_AVH_TP_DROP_MAX 	  	MPRESS_5BAR	
  /* AHB gen3 Target press interface */

/*
#define S8_AVH_ACT_MAX_GRADE_DW                 -23
#define S16_AVH_MOVING_DETECT_CYCLE                 L_U16_TIME_10MSLOOP_60S
#define S16_AVH_ACC_PEDAL_ON_MIN_TH                 11   
#define S16_AVH_FAST_EXIT_ARAD_TH               ARAD_2G0
#define S16_AVH_IGN_DECOUNT_CYCLE	  		    L_U8_TIME_10MSLOOP_2S
#define S16_AVH_RECOVERY_IGN_CNT_REF			0
#define S16_AVH_MAX_MP_OFFSET_BLS_FAIL       	MPRESS_8BAR
#define S16_AVH_FAST_EXIT_MTP_TH                30
#define S16_AVH_ALARM_TIME_INBIT_SW_OFF         L_U16_TIME_10MSLOOP_4S
#define S16_AVH_ALARM_TIME_INBIT_SW_ON          L_U16_TIME_10MSLOOP_4S
#define S16_AVH_ALARM_TIME_EPB_OK               (L_U8_TIME_10MSLOOP_1000MS*8) 
#define S16_AVH_MIN_MP_SLOP                     MPRESS_1BAR
#define S16_AVH_VV_ACT_COMP_MIN_MP_SLP          MPRESS_3BAR
#define S16_AVH_MAX_MP_SLOP                     MPRESS_10BAR
#define S16_AVH_VV_ACT_COMP_MAX_MP_SLP          MPRESS_18BAR
#define S16_AVH_ABNORMAL_HOLD_TIME 		    	L_U8_TIME_10MSLOOP_1500MS
#define S16_AVH_ABNORMAL_HOLD_DUTY 				70
#define S16_AVH_ABNORMAL_FO_REF_SCAN            L_U8_TIME_10MSLOOP_20MS

#define S16_AVH_L_M_SLOP_MIN_H_P_UP				MPRESS_6BAR
#define S16_AVH_H_M_SLOP_MIN_H_P_UP				MPRESS_7BAR
#define S16_AVH_MAX_SLOP_MIN_H_P_UP				MPRESS_10BAR
#define S16_AVH_L_M_SLOP_SAFE_H_P_UP			MPRESS_20BAR
#define S16_AVH_H_M_SLOP_SAFE_H_P_UP			MPRESS_25BAR
#define S16_AVH_MAX_SLOP_SAFE_H_P_UP			MPRESS_35BAR
*/
/* tuning parameter */

#define AVH_MANUAL_TM    				0
#define AVH_AUTO_TM      				1

/* gear position set */
#define	AVH_PARKING 					0
#define	AVH_NEUTRAL 					6
#define	AVH_DRIVE	 					5
#define	AVH_SPORTS						8
#define	AVH_REVERSE						7
#define AVH_FORWARD                 	1	 

  #if (__CAR_MAKER==HMC_KMC)
/*AVH output data CAN&Lamp*/     
#define	AVH_OUT_ACTIVATION 				0
#define	AVH_IN_ACTIVATION				1
#define	AVH_IN_RELEASE   				2

#define	AVH_LAMP_OFF_REQUEST			0
#define	AVH_LAMP_ON_REQUEST				1
#define AVH_ALARM_CLU_MSG_OFF			0
#define AVH_ALARM_CLU_MSG_ON			1  
#define AVH_CLU_MSG_SW_PUSH_AT_ACT		2 
#define AVH_CLU_MSG_HTD_OPEN    		3 
/*EPB state define*/
#define	EPB_RELEASED_STATE	    		1
#define	EPB_CLAMPED_STATE	    		2
#define	EPB_RELEASING_STATE	    		4
#define	EPB_CLAMPING_STATE	    		3
#define	EPB_DYNAMIC_BRK_STATE   		5
#define	EPB_RELEASING_BY_SW_STATE   	6
#define	EPB_CLAMPING_BY_SW_STATE   		7

  #else /* other car maker */
/*AVH output data CAN&Lamp*/     
#define	AVH_OUT_ACTIVATION 				0
#define	AVH_IN_ACTIVATION				1
#define	AVH_IN_RELEASE   				2

#define	AVH_LAMP_OFF_REQUEST			0
#define	AVH_LAMP_ON_REQUEST				1
#define AVH_ALARM_CLU_MSG_OFF  			0
#define AVH_ALARM_CLU_MSG_ON    		1
#define AVH_CLU_MSG_SW_PUSH_AT_ACT    	2 
#define AVH_CLU_MSG_HTD_OPEN    		3 

/*EPB state define*/
#define	EPB_RELEASED_STATE	    		1
#define	EPB_CLAMPED_STATE	  			2
#define	EPB_RELEASING_STATE	    		4
#define	EPB_CLAMPING_STATE	    		3
#define	EPB_DYNAMIC_BRK_STATE   		5
#define	EPB_RELEASING_BY_SW_STATE   	6
#define	EPB_CLAMPING_BY_SW_STATE   		7
  #endif
  
#define AVH_ISG_ACTIVE          		1
#define AVH_ISG_ENG_AUTO_START  		2  
/*****************************************************************************/  
#endif /* AVH */


  #if (__EPB_INTERFACE) || (__AVH)
/*Global Type Declaration ****************************************************/

#define	lcu1AvhActFlg				AVHF0.bit7
#define	lcu1AvhInhibitFlg			AVHF0.bit6
#define	lcu1AvhEnterSatisfy 		AVHF0.bit5
#define	lcu1AvhExitSatisfy			AVHF0.bit4
#define	AVH_TCL_DEMAND_fl			AVHF0.bit3
#define	AVH_TCL_DEMAND_fr			AVHF0.bit2
#define	AVH_S_VALVE_LEFT			AVHF0.bit1
#define	AVH_S_VALVE_RIGHT			AVHF0.bit0

#define	lcu1AvhSwitchOn				AVHF1.bit7
#define	lcu1AvhUphill	   			AVHF1.bit6
#define	lcu1AvhDownhill				AVHF1.bit5
#define	lcu1AvhActReq				AVHF1.bit4
#define	lcu1AvhAxStationary			AVHF1.bit3
#define	lcu1AvhRiseModeEnd			AVHF1.bit2
#define	lcu1AvhApplySatisfy			AVHF1.bit1
#define	lcu1AvhAccelExitOK			AVHF1.bit0

#define	lcu1AvhMtGearNeutral  		AVHF2.bit7  
#define	lcu1AvhOverActTime         	AVHF2.bit6
#define	lcu1AvhDownDrive			AVHF2.bit5
#define	lcu1AvhAutonomousActFlg		AVHF2.bit4
#define	lcu1AvhEpbActReq        	AVHF2.bit3
#define	lcu1AvhILamp         		AVHF2.bit2
#define	lcu1AvhFastExitSatisfy		AVHF2.bit1
#define	lcu1AvhSlowExitSatisfy		AVHF2.bit0

#define	lcu1AvhGearChange   		AVHF3.bit7  
#define	lcu1AvhMtReverseGear	    AVHF3.bit6
#define	lcu1AvhClutchPressed   		AVHF3.bit5
#define	lcu1AvhVehicleRoll   		AVHF3.bit4
#define	lcu1AvhTcOverTemper    		AVHF3.bit3
#define	lcu1AvhDriverInVehicle      AVHF3.bit2
#define	lcu1AvhEpbFailFlg   		AVHF3.bit1
#define	lcu1AvhActReady				AVHF3.bit0

#define	lcu1AvhAutoTM            	AVHF4.bit7  
#define	lcu1AvhMaxHill              AVHF4.bit6
#define	lcu1AvhMedExitSatisfy       AVHF4.bit5
#define	lcu1AvhAutoOverActTime      AVHF4.bit4
#define	lcu1AvhComCndSatisfy        AVHF4.bit3
#define	lcu1AvhEnterSatisfy2        AVHF4.bit2
#define	lcu1AvhReBrkRelease	        AVHF4.bit1
#define	lcu1AvhReBrk     	        AVHF4.bit0

#define	lcu1AvhSWActReady 	        AVHF5.bit7  
#define	lcu1AvhReqEpbActInvalid     AVHF5.bit6
#define	lcu1AvhLampOnReq            AVHF5.bit5
#define	lcu1AvhAlarmReq             AVHF5.bit4
#define	lcu1AvhVehicleRoll2         AVHF5.bit3
#define	lcu1AvhSwitchFail           AVHF5.bit2
#define	lcu1AvhRiseRoll             AVHF5.bit1
#define	lcu1AvhRiseInit             AVHF5.bit0

#define	lcu1AvhSwitchAct 		    AVHF6.bit7  
#define	lcu1AvhSwitchActOld		    AVHF6.bit6
#define	lcu1AvhInhibitSwOn	        AVHF6.bit5
#define	lcu1AvhInhibitSwOff			AVHF6.bit4
#define	lcu1AvhForcedSwOff			AVHF6.bit3
#define	lcu1AvhEpbActOkForSwOff     AVHF6.bit2
#define	lcu1AvhSwitchOnOld          AVHF6.bit1
#define	lcu1AvhEpbActOkForSwOffOld  AVHF6.bit0

#define	lcu1AvhMpressOn			    AVHF7.bit7  
#define	lcu1AvhForcedSwOffEpbAct  	AVHF7.bit6
#define	avh_non_use_7_5  			AVHF7.bit5
#define	avh_non_use_7_4  			AVHF7.bit4
#define	avh_non_use_7_3				AVHF7.bit3
#define	avh_non_use_7_2  			AVHF7.bit2
#define	avh_non_use_7_1  			AVHF7.bit1
#define	avh_non_use_7_0  			AVHF7.bit0

#define AVH_INHIBIT                 1
#define AVH_NORMAL                  2
#define AVH_READY                   3
#define AVH_HOLD                    4
#define AVH_APPLY                   5
#define AVH_RELEASE                 6

/*****************************************************************************/
/*extern 	int16_t abs(int16_t j); */
/*Global Extern Variable  Declaration*****************************************/
extern struct	U8_BIT_STRUCT_t AVHF0, AVHF1, AVHF2, AVHF3, AVHF4, AVHF5, AVHF6, AVHF7;

extern int8_t	lcs8AvhControlState;                                                                                                    

extern uint8_t  lcu8AvhState, lcu8AvhState2, lcu8AvhChkUphillCnt, lcu8AvhChkDownhillCnt;
extern uint8_t	lcu8AvhGearPosition, lcu8AvhGearPositionOld, lcu8AvhReqEpbAct, lcu8AvhAceelIntendCnt;                                                      
extern uint8_t  AVH_debuger, lcu8AvhDownDrvLevel, lcu8AvhReBrkCnt, lcu8AvhDriverInsideCnt;                                                                                        
extern uint8_t	lcu8AvhLampRequest, lcu8AvhAlarmRequest, lcu8AvhEpbState, lcu8AvhAxStationaryCnt;                                                                                 
extern uint8_t	lcu8AvhDrvDoorOpen, lcu8AvhDrvSeatUnBelted, lcu8AvhHoodOpen, lcu8AvhTrunkOpen;
extern uint8_t  lcu8AvhGearChageCnt, lcu8AvhMpOkCnt,lcu8AvhRiseEndCnt, lcu8AvhMaxHillCnt;                                                                                    
extern uint8_t  lcu8AvhCluMsg, lcu8AvhTotalRiseAtempt, lcu8AvhPrateLevel, lcu8AvhInitCurCnt; 
extern uint8_t  lcu8AvhTcCurAvgPeriodCnt, lcu8AvhTcTemperDeCount, lcu8AvhRiseInitOffCnt, lcu8AvhLampOffdelay;
extern uint8_t  lcu8AvhAutoAct1SecCnt,lcu8AvhEntSpdOk, lcu8AvhReBrkReleaseCnt, lcu8AvhAlarmCase, lcu8AvhInitIgnOnCnt;


extern int16_t	lcs16AvhRetainingPress, lcs16AvhEnterPress, lcs16AvhDefficientPress;                              
extern int16_t	lcs16AvhAxG, lcs16AvhAxGAbs, lcs16AvhApplyCnt, lcs16AvhApplyTime;                                                                       
extern int16_t    lcs16AvhMpressSlopThres, lcs16AvhReleaseTime, lcs16AvhFilteredAxOld, lcs16AvhFilteredAx;                               
extern int16_t	lcs16AvhTargetPress, lcs16AvhFilteredTorq, lcs16AvhSafeHoldPress, lcs16AvhMinEntPres;          
extern int16_t    lcs16AvhEngIdleCnt, lcs16AvhIdleTorq, lcs16AvhEpbReqCnt;                       
extern int16_t    lcs16AvhMoveDistance_fl, lcs16AvhCompleteStopCnt_fl, lcs16AvhMoveDistance_fr, lcs16AvhCompleteStopCnt_fr;
extern int16_t    lcs16AvhMoveDistance_rl, lcs16AvhCompleteStopCnt_rl, lcs16AvhMoveDistance_rr, lcs16AvhCompleteStopCnt_rr;
extern int16_t    lcs16AvhAbnormalHoldCnt, lcs16AvhMaxHillOnCnt;
extern int16_t    lcs16AvhHoldCnt, lcs16AvhHold1SecCnt, lcs16AvhHoldCntFor1IGN; 
extern int16_t	  laavhs16TcTargetPress, lcs16AvhMpress;

extern uint16_t	lcu16AvhRollStateCnt;                                                           
extern uint16_t	lcu16AvhTcCurAvg, lcu16AvhTcCurSum, lcu16AvhTcCurTemper, lcu16AvhAutoActTotal;

extern int32_t   lcs32AvhTorqSum;

  #endif /* (__EPB_INTERFACE) || __AVH */
/*Global Extern Functions  Declaration****************************************/
  #if __AVH
extern  void 	LCAVH_vCallControl(void);
extern  void  LCAVH_vControlAvhAbnormalMode(void);
  #endif
  #if  (__AVH)||(__EPB_INTERFACE)
extern void	    LCAVH_vTxLdmState(void);
  #endif  
#endif
