#ifndef __LDABSDCT_ROAD_GRADIENT_H__
#define __LDABSDCT_ROAD_GRADIENT_H__

/* Includes ************************************************************/

#include "LVarHead.h"

//#include "LSABSEstLongAccSensorOffsetMon.h"

#if  (__Ax_OFFSET_MON==1) && (__Ax_OFFSET_MON==0)


/* Local Definition  ***********************************************/
#define Brake_Pitching_flag				        G_GRADE_0.bit7
#define Agrade_On_flag				            G_GRADE_0.bit6
#define UP_HILL_flag	            			G_GRADE_0.bit5
#define DN_HILL_flag   			                G_GRADE_0.bit4
#define HILL_RESET_flag         				G_GRADE_0.bit3
#define Brake_On_flag            				G_GRADE_0.bit2
#define G_GRADE_UNUSED_GLOBAL_FLG_1				G_GRADE_0.bit1
#define G_GRADE_UNUSED_GLOBAL_FLG_0				G_GRADE_0.bit0

/*Global Type Declaration **********************************************/
extern  struct	U8_BIT_STRUCT_t G_GRADE_0;
 
 
/* Global Variables Declaration ****************************************/

  /* Global For Other's Use */
 

extern uint8_t  HILL_RESET_CNT;	 	
extern uint8_t  UP_CNT;
extern uint8_t  DN_CNT;	 	

//extern int16_t    filt_ein_new;
//extern int16_t    filter_out_new;
//extern int16_t    filter_out_new_rest;
//extern uint16_t   voptfz_mittel_new;		

extern uint8_t  Pitch_CNT;	
extern int16_t    Agrade;	
extern int16_t    AgradF;
//extern int16_t    WheelAccelG;	
extern int16_t    PitchG;	
  
  /* Global For Logging */
  
 

  
/* Global Function Declaration *****************************************/
extern void LDABS_vDctRoadGradient(void);


#endif
#endif
