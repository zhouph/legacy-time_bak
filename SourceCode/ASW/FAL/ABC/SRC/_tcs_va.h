#include "Abc_Ctrl.h"
/**************************************************/
/*                                                */
/*    Define & Declare  Structure Varible         */
/*                                                */
/*                  ____ MANDO ABS TEAM.          */
/**************************************************/

/* =========================================== */
/* --->>>>>     FTCS Varibale         <<<<---- */
/* =========================================== */
struct TF0_BIT  TF0;
struct TF1_BIT  TF1;
struct TF2_BIT  TF2;
struct TF3_BIT  TF3;
struct TF4_BIT  TF4;
struct TF5_BIT  TF5;
struct TF6_BIT  TF6;
struct TF7_BIT  TF7;
struct TF8_BIT  TF8;
struct TF9_BIT  TF9;
struct TF10_BIT  TF10;
struct TF11_BIT  TF11;
struct TF12_BIT  TF12;
struct TF13_BIT  TF13;
struct TF14_BIT  TF14;
struct TF15_BIT  TF15;
struct TF16_BIT  TF16;
struct TF17_BIT  TF17;
struct TF18_BIT  TF18;
struct TF19_BIT  TF19;
struct TF20_BIT  TF20;
struct TF21_BIT  TF21;
struct TF22_BIT  TF22;
struct TF23_BIT  TF23;
struct TF24_BIT  TF24;
struct TF25_BIT  TF25;


	int16_t   ConvCaltorq;	/*for Ford focus c214 */
    int16_t   esp_cmd;
    int16_t   tcs_cmd;
    int16_t   trace_cmd;
    int16_t   temp_cmd;
	int16_t   cal_torq;
    int16_t   cal_torq_old;
    int16_t   can_ver;
    int16_t   tcu_torq;    
    uint16_t   hill_33_count;
    uint16_t   eng_stall_count;
    uint16_t   zero_count;
    uint16_t   mini_dec_count;
    uint16_t   mini_inc_count;
    int16_t   vmini_fl;
    int16_t   vmini_fr;
    int16_t   vmini_rl;
    int16_t   vmini_rr;

    int16_t   ay_det;
    int16_t   ayf_det;

    int16_t   ay_threshold;
    int16_t   ay_th_old;
    int16_t   ay_th_temp;
    int16_t   ay_th_tmp1;
    int16_t   ay_th_tmp2;
    int16_t   vel_f_old;
    uint16_t  eng_rpm_max;
    int16_t   dv;
    int16_t   opt_dv;
    int16_t   dv_rate;
    int16_t   dv_sum;
    uint8_t dv_rate_count;
    uint8_t hill_33_rec;
    int16_t   real_torq;
    int16_t   vel_f_mid, vel_f_mid_old;
    int16_t   vel_f_big;
    int16_t   vel_f_small;
    int16_t   vel_f;
    int16_t   vel_r;
    int16_t   vel_r_old;
    int16_t   acc_f;
    int16_t   acc_r_old;
    int16_t   acc_r;
    uint8_t tm_gear;          
    uint8_t esp_tcs_count;
    uint8_t esp_ay_count1;
    uint8_t esp_ay_count2;

    int8_t  k;
    int16_t   aref;
    int16_t   aref_old;
    int16_t   aref_count;
    int16_t   dv_alt;
    int16_t   dv_max;
    int16_t   ftc_detect_speed;
    uint8_t vel_f_same_count;
    int16_t   tcs_torq_up;
    uint16_t  trace_lamp_count;
    uint16_t  tcs_hold_count;
    int16_t   err_dv;
    int16_t   ierr_dv;
    uint8_t thsd_ay_count1;
    uint8_t thsd_ay_count2;

    int16_t   v_f[10];
    int16_t   a_f[10];
    int16_t   ay[10];
    int16_t   ayf[10];
    int16_t   opt_slip_min;
    int16_t   opt_slip;
    int16_t   vel_r_diff, vel_r_diff_old, vel_r_diff_old2, slip;
    uint8_t lowtohigh_mu_count;
    int8_t  gear_pos;
    int8_t  AT_gear_pos_old;
    int16_t   av_opt_slip;
    int16_t   av_opt_slip_old;
    int16_t   torq_target;
    int16_t   ftc_count;
    uint16_t  high_count;

    uint8_t ftcs_count1;
    uint8_t ftcs_count2;
    uint8_t ftcs_count3;
    uint8_t trace_count1;
    uint8_t trace_count2;
    uint8_t drv_tm_pos;
    int16_t   ay_diff;
    int16_t   ayf_diff;
    int16_t   ay_log;
    int16_t   ay_opt;
    int16_t   ay_aver;
    int16_t   ay_aver_alt;
    int16_t   ayf_log;
    int16_t   ayf_aver;
    int16_t   ayf_aver_alt;
    int16_t   err_ay;
    int16_t   err_ay_old;
    int16_t   derr_ay;
    int16_t   hunt_count;
    int16_t   diff_f, diff_f_old, diff_r, diff_r_old;
    int16_t   circle_count;
    int16_t   down_count;
    int16_t    lctcss16BrkCtrlMode;
    int16_t    lctcss16EngCtrlMode;
    int16_t    lctcss16EECEngCtrlMode;

	
	/* ---------------------------------------------------------- */
	/*	Variables coded for GMDAT by Kim Jeonghun in 2002.6.12    */
	/* ---------------------------------------------------------- */
	
	uint8_t DIESEL, ENG_VAR, ENG_CHR;
	uint8_t lamp_on_delay;	

    uint8_t	etcs_on_count;	
    uint8_t	ftcs_on_count;	    

	uint8_t   FTCS_flags;	

	
	uint8_t	target_gear;
	uint8_t	prev_gear;

	/* Added by YJH in 2005.05.25 */
	int16_t eng_temp;	

	
	
	
	
	

	int8_t	LOW_TO_HIGH;
	int8_t	TRACE_DISABEL;
	int8_t	TRACE_END_FIND;
/* ================================================================ */
/*	App.Field upgrade for multi-engine variables in 2003.12.08      */
/* ---------------------------------------------------------------- */
	
	
	/* New BTCS Structure					*/
	int16_t		rpm_cmd;
	int16_t		inter_tempW0, inter_tempW1, inter_tempW2, inter_tempW3, inter_tempW4; 
	int16_t		inter_tempW5, inter_tempW6, inter_tempW7, inter_tempW8, inter_tempW9;
	int16_t		unit_conversion;
	int16_t		eng_rpm_f, rpm_error, rpm_error_dif, rpm_dead_band_p, 	rpm_dead_band_n;
	int16_t		rpm_error_old1, rpm_error_old2, rpm_ctl_amt;
	int16_t		rpm_ctl_p_gain, rpm_ctl_d_gain, gain_decrease_ratio, gain_increase_ratio;
	
	uint8_t   split_flags, SplitCnt, DecreaseSplitCntInHomoMu;


	int16_t		tcs_monitor;
	
	
	int16_t	tcs_tempW0, tcs_tempW1, tcs_tempW2, tcs_tempW3, tcs_tempW4, tcs_tempW5;
	
	int16_t	tcs_tempW6, tcs_tempW7, tcs_tempW8, tcs_tempW9, tcs_tempW10, tcs_tempW11;	
	int16_t	SplitTransitionCnt, SplitTransitionCntOld;
	
	/* USA by Kim Jeonghun in 2006.02.23 */
	uint8_t	Low2HighSuspect;	
	uint8_t	Low2HighSuspectCount;
	uint8_t	Low2HighSuspectTimer;
	int16_t		Low2HighSuspectAccF;
	int16_t		Low2HighSuspectAccR;	
	int16_t		Low2HighSuspectAccFTemp;
	int16_t		Low2HighSuspectAccRTemp;	
	
	/* NEW_TCS 개발 */
	int16_t lts16FrontWheelSpinAverage;
	int16_t lts16FrontWheelSpinAverageOld;
	int16_t lts16RearWheelSpinAverage;
	int16_t lts16RearWheelSpinAverageOld; 
	int16_t	lts16WheelSpin;
	int16_t lts16TargetWheelSpin;
	int16_t lts16WheelSpinError;
	int16_t lts16WheelSpinErrorOld;
	int16_t lts16WheelSpinErrorDiff;
	int16_t lts16PGain, lts16PGain1stCycle, lts16PGainForETCS, lts16PGainForFTCS;
	int16_t lts16DGain, lts16DGain1stCycle, lts16DGainForETCS, lts16DGainForFTCS;
	int16_t ltcss16ReduceFactor;
	int16_t lts16AddPGainInTurn;
	int16_t lts16AddDGainInTurn;	
	int16_t ltcss16MinimumPgain;
	int16_t lts16PGainEffect;
	int16_t lts16DGainEffect;
	int16_t lts16MulPGainAndSpinError;
	int16_t ltcss16PDGainEffect;
	int16_t ltcss16SteadyStateCtl;	
	int16_t	tcs_monitor1, tcs_monitor2, tcs_monitor3, tcs_monitor4, tcs_monitor5, tcs_monitor6, tcs_monitor7;
	uint8_t lts8LoopCounter;
	int16_t lts16TempArray[4];	        
	int16_t lts16TempArray2By4[2][4];
	int16_t lts16TempArray3By3[3][3];	     
	int16_t lts16TempArray3By4[3][4];	     
	int16_t lts16TempArray4By3[4][3];
	uint8_t ltcsu8TargetSpinReduceThInTurn;
	int16_t ltcss16TargetSpinReduceAyTh;
	int16_t ltcss16TargetSpinReduceSteerTh;
	int16_t ltcss16DelYawThforTSpinReduce;

/* ===================================================================== */	                                   
/* Tuning parameter set For ETCS PD Control      						 */									
/*  Column : Vx            {0~5, 10, 30, 80~}KPH						 */
/*  Row    : Ax(row)       {0.05, 0.2}G			     					 */
/*  Scale factor : P Gain 80 , D Gain 8			    					 */
/*  -. P Gain : (Error : 1kph기준) 1 -> torq. 0.1%						 */
/*  -. D Gain : (Error Difference : 0.125kph기준) 80 -> torq. 0.1%   	 */
/* ===================================================================== */
	
  #if __TCS_CODE_SIZE_REDUCTION_RESTORE
	uint8_t ltu8TargetSpinArrayHomo[2][4];
	uint8_t ltu8TargetSpinArraySplit[2][4];	

	
	uint8_t ltu8AddPGainInHighSideSpin[2][4];
  #endif	/* #if __TCS_CODE_SIZE_REDUCTION_RESTORE */	
			
	int16_t ltcsu166SecCntAfterETO;
    int16_t ltcsu163SecCntAfterETO;
    int16_t ltcsu162SecCntAfterETO;
    uint8_t ltcsu81SecCntAfterETO;
    int16_t ltcsu16TimeCntAfterETO;
    uint8_t ltcsu8TorqFastRecoveryCnt;

    
    
    int16_t lts16WheelSpinFilt;
	int16_t lts16WheelSpinFiltOld;			   	

	int16_t ltcss16BothWheelSpinCnt;
	int16_t ltcss16BothSideWheelSpinCntLH;      	
	int16_t ltcss16BothSideWheelSpinCntRH;				
	int16_t ltcss16ETCSEnterExitFlags;
	
	uint16_t lctcsu16etcsthreshold1;
	uint16_t lctcsu16etcsthreshold2;

	/* BTCS_TCMF_CONTROL */	   
    int16_t ltcss16BaseTarWhlSpinDiff;                   
    int16_t ltcss16BaseStTarWhlSpinDiff;
    int16_t ltcss16BaseHillTarWhlSpinDiff;
    int16_t ltcss16BaseTurnTarWhlSpinDiff;                  
    int16_t lctcss16VehSpd4TCS;
    uint8_t lctcsu1BTCSVehAtv;
    uint8_t lctcsu1BTCSVehAtvOld;
    uint8_t lctcsu1BTCSVehAtvRsgEdg;
    //uint16_t fu16CalVoltMOTOR;
    uint8_t lctcsu1IniMtrCtlModeOld;
    uint8_t lctcsu1IniMtrCtlMode;
	int8_t lctcss8IniMtrCtlModeCnt;
    uint8_t lctcsu1IniMtrCtlModeFalEdg;   
    int16_t MFC_Current_TCS_P_OLD;
    int16_t MFC_Current_TCS_S_OLD;
   	uint8_t ltcsu1MFCInitDelayNotAllow;
	uint8_t ltcsu8MFCInitDelayNotAllowCnt;
	int16_t lctcss16BaseAsymSpnCtlTh;
	int16_t lctcss16BaseAsymSpnCtlThInSt;
	int16_t lctcss16BaseAsymSpnCtlThInTurn;
	int16_t lctcss16AsymSpnCtlThInRough;
	//int16_t lctcss16AsymSpnCtlTh;
	int16_t lctcss16BaseSymSpnCtlTh;
	int16_t lctcss16SymSpnCtlThInRough;
	int16_t lctcss16SymSpnCtlThInRTA;
	int16_t lctcss16SymSpnCtlThInESCBrk;
	int16_t lctcss16SymSpnCtlTh; 
	int16_t lctcs16BsPG4ASFBCtlPositiveF;
	int16_t lctcs16BsPG4ASFBCtlNegativeF;
	int16_t lctcs16BsPG4ASFBCtlPositiveR;
	int16_t lctcs16BsPG4ASFBCtlNegativeR;
	int16_t lctcs16BsStPG4ASFBCtlPositiveF;
    int16_t lctcs16BsStPG4ASFBCtlNegativeF;
    int16_t lctcs16BsStPG4ASFBCtlPositiveR;
    int16_t lctcs16BsStPG4ASFBCtlNegativeR;
    int16_t lctcs16BsHillPG4ASFBCtlPositF;
    int16_t lctcs16BsHillPG4ASFBCtlNegatF;
    int16_t lctcs16BsHillPG4ASFBCtlPositR;
    int16_t lctcs16BsHillPG4ASFBCtlNegatR;
    int16_t lctcs16BsTurnPG4ASFBCtlPositiveF;
    int16_t lctcs16BsTurnPG4ASFBCtlNegativeF;
    int16_t lctcs16BsTurnPG4ASFBCtlPositiveR;
    int16_t lctcs16BsTurnPG4ASFBCtlNegativeR;

	int16_t lctcs16BsIG4ASFBCtlFront;   
	int16_t lctcs16BsIG4ASFBCtlRear;	
	int16_t lctcs16BsStIG4ASFBCtlFront;   
	int16_t lctcs16BsStIG4ASFBCtlRear;	
	int16_t lctcs16BsHillIG4ASFBCtlFront;   
	int16_t lctcs16BsHillIG4ASFBCtlRear;
	int16_t lctcs16BsTurnIG4ASFBCtlFront;  
	int16_t lctcs16BsTurnIG4ASFBCtlRear;	 
	
	uint8_t ltcsu1VCAOffBySpdDiffFlg;
	
	int16_t lctcss16VehTurnIndex;
	int16_t lctcss16VehTurnIndexOld;
	int16_t lctcss16DctTurnAyEnterTh;
	int16_t lctcss16DctTurnAyFinalTh; 
    
	int16_t lctcss16IniTCVCtlModeTime; 
	int16_t lctcss16IniMtrCtlModeTime;

	uint8_t VCA_ON_TCMF;
	uint8_t VCA_ON_TCMF_OLD;

	int8_t ltcss8WhlSpinDecCntTCMF;
	uint8_t VCA_BRAKE_CTRL_END_TCMF;
	uint8_t ltcsu8VCARunningCntTCMF;
	uint8_t VCA_SUSTAIN_FLAG_TCMF;
	int16_t   ltcss16MaxVrefInVCATCMF;
	uint8_t ltcsu8CntAfterVCATCMF;
	uint8_t lctcsu1BigSpnAwdDctd;

	
	int16_t vref_test_cnt;
	int16_t test_cycle;
	int16_t lctcss16VehSpd4TCSTemp;
	  
	uint8_t lctcsu8SplitMuDecHilEnterCnt; 
    uint8_t lctcsu8SplitMuDecHilExitCnt;
    uint8_t lctcsu1DctBTCSplitMuFlag;
    uint8_t lctcsu1DctBTCSplitMuHill;
    uint8_t lctcsu1DctBTCHillByAx;
    uint16_t lctcsu16BTCStpNMovDistance;
    uint16_t lctcsu16BTCStpNMovBsDistance;
    uint16_t lctcsu16BTCStpNMovCnt;  
    
    uint8_t lctcsu1GearShiftFlag;
    uint8_t lctcsu1GearShiftFlagResetCheck;
	uint8_t lctcsu8GearShiftFlagResetCnt;
	
	uint8_t BTCS_Entry_Inhibit_ByDriver;
	uint8_t BTCS_Entry_Inhibit_ByVehSpd;
	uint8_t BTCS_Entry_Inhibit_ByRPMTorq;
	int16_t lctcss16AsymSpnCtlThAddCompMd;
    
  uint8_t  lctcsu1FlgOfAfterStall; 
  uint16_t lctcsu16CntAfterStall;
  uint8_t  lctcsu1ABSCtrlOld4BTCSInhibit;
    
    /* BTCS_TCMF_CONTROL */

	/*===================================================================*/
	/* BTCS TUNNING PARAMETER											 */
	/*===================================================================*/
  #if __TCS_CODE_SIZE_REDUCTION_RESTORE
	uint8_t	ltcsu8FrontRiseMarginHomo[2][4];
	int8_t	ltcss8FrontDumpMarginHomo[2][4];
	uint8_t	ltcsu8RearRiseMarginHomo[2][4];
	int8_t	ltcss8RearDumpMarginHomo[2][4];
	uint8_t	ltcsu8FrontRiseMarginSplit[2][4];
	int8_t	ltcss8FrontDumpMarginSplit[2][4];
	uint8_t	ltcsu8RearRiseMarginSplit[2][4];
	int8_t	ltcss8RearDumpMarginSplit[2][4];
	uint8_t	ltcsu8FrontHoldScanInState1Homo[2][4];
	uint8_t	ltcsu8RearHoldScanInState1Homo[2][4];
	uint8_t	ltcsu8FrontHoldScanInState1Split[2][4];
	uint8_t	ltcsu8RearHoldScanInState1Split[2][4];
  #endif	/* #if __TCS_CODE_SIZE_REDUCTION_RESTORE */	
	/*===================================================================*/	
    
/* Variables for ITCC Control ↘*/	
	uint16_t	LimitCardanShaftTorque;
	uint8_t   s8TODLoopCounter;
  #if (__CAR == HMC_EN)
    int16_t   s16TODLimitTorq[3][3] = {{20,20,20},
	                               {20,30,30},
	                               {30,30,30}};
  #else
    int16_t   s16TODLimitTorq[3][3] = {{1,15,15},
	                               {15,15,15},
	                               {15,15,15}};  
  
  #endif
	int16_t   s16TODLimitTorqTemp[3];                                  
    int16_t   s16SetTODSpeed[3];	
/* Variables for ITCC Control ↗*/	    

/* NEW_ENGINE_CTL Temp Tuning Parameter */
                                 	  
/* NEW_ENGINE_CTL Temp Tuning Parameter */

/* NEW_BRAKE_CTL Temp Tuning Parameter */

/* NEW_BRAKE_CTL Temp Tuning Parameter */

 #if __TCS_CONTROLLER_UNIFICATION
uint8_t lctcsu8FastTorqRecoveryStatus;
 #else
uint8_t FORCED_TORQ_RISE_MODE; 
 #endif
int16_t lts16TorqRiseThr;
uint16_t ltu16TorqRiseLimit;
uint8_t ltu8TorqRiseRate;
int16_t ltcss16DelVLRLimit;
uint8_t lespu8GearPosBeforeEECActivated;
int16_t ltcss16AccOfNonDrivenWheel;
int16_t ltcss16GoodTargetTrackingCnt;

uint8_t temp_tm_gear;			// 07SWD #6

int16_t ltcss16MiniTireTendencyCnt;
int16_t ltcss16ResetMiniTireDetectCnt;

int16_t ltcss16LowTorqueLevelCnt;
int16_t lctcss16SplitHillDctCnt;

/* #if __TCS_2WL_MINI_DETECTION */
uint8_t lctcsu8MiniTireSuspectResetCnt;

/* #endif __TCS_2WL_MINI_DETECTION */


#if __MY08_TCS_SPLIT_TORQ_CTL_PATTERN
int16_t lctcss16MinimumTorqLevel, lctcss16MinimumTorqLevelOld;
int16_t lctcss16MinimumTorqLevelDecTh, lctcss16MinimumTorqLevelMinTh;
#endif /* #if __MY08_TCS_SPLIT_TORQ_CTL_PATTERN */

#if __MY08_TCS_DCT_L2SPLIT
int16_t ltcss16SpinFL, ltcss16SpinFR, ltcss16SpinRL, ltcss16SpinRR;
int16_t lctcss16TorqInput4L2SpltDct, lctcss16BaseTorqLevel4L2SpltDct;
#endif

#if __MY08_TCS_PREVENT_SPIN_OVERSHOOT
int16_t ltcss16WheelSpinOverShootTh, ltcss16WheelSpinErrDifMax, ltcss16WheelSpinErrDifMin;
int16_t lctcss16OverShootTorqLvl;
int16_t lctcss161SpinOverShootCnt;
int16_t lctcss16OverShootTorqRedAmount;
#endif		/* #if __MY08_TCS_PREVENT_SPIN_OVERSHOOT */


#if __MY08_TCS_1ST_SCAN_CTL
int16_t lctcss161stScanTorqLevelInTurn[3][3];
int16_t lctcss161stScanTorqLevelHomo[2][4]; 
#endif	/* #if __MY08_TCS_1ST_SCAN_CTL */

int16_t lctcsu8TargetSpinInTurn[3][3];

#if __MY08_TCS_DISCRETE_ENG_CTL
int16_t	ltcss16EngTrqHoldScan;
int16_t ltcss16TCSPhase, ltcss16TCSPhaseOld, ltcss16PhaseChgTh;
int16_t ltcss16TCSPhaseMaintainCnt, ltcss16DiscreteCtrlInput;
#endif /* #if __MY08_TCS_DISCRETE_ENG_CTL */

#if __MY08_TCS_TORQUE_LEVEL_SEARCH
uint8_t	FTCS_flags_old;
int16_t		ltcss16BaseTorqueLevel, ltcss16BaseTorqueLevelOld;
int16_t		ltcss16FastTorqueInputValue;
uint8_t	ltcsu856msCnt;
uint8_t	ltcsu8TorqReductionFactor[2][4];
uint8_t	ltcsu8TorqRecoveryFactor[2][4];
#endif	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */

#if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC
int16_t	lctcss16TorqLevelAfterEEC[3][3];
int16_t	lctcss16TorqLvlAfterEEC;
int16_t	lctcss16TorqIncRateAfterEEC[3][3];
int16_t	lctcss16TorqUpRateAfterEEC;
#endif	/* #if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC */

#if __MY08_TCS_DCT_H2L
int16_t		ltcss16H2LTimer;
#endif	/* #if __MY08_TCS_DCT_H2L */


#if __GM_FailM
uint8_t	U8_TCS_FAIL_MODE_BRAKE;
uint8_t	U8_TCS_FAIL_MODE_ENGINE;
uint8_t	U8_TCS_FAIL_MODE_INHIBIT;
#endif



uint8_t	ltcsu8CntAfterHomo2SPlit;

int16_t   	cal_torq_abs;
int16_t   	cal_torq_rel;

int16_t lctcss16DiagonalWhlSpinCnt;
int16_t lctcss16ThOfDct4WDStuckByDWSpn;
int16_t ltcss16BigSpinCnt, ltcss16SpinDecCnt;

#if __TCS_CONTROLLER_UNIFICATION
uint8_t lctcsu8PosSpinErrDiffCnt1;
uint8_t lctcsu8PosSpinErrDiffCnt2;
uint8_t lctcsu8NegSpinErrDiffCnt1;
uint16_t ltcsu16TCSMuLevel;
uint16_t ltcsu16NormalizedLongG;
uint8_t	lctcsu8NormalizedLongGFactor;
#endif

#if __MU_EST_FOR_TCS
int16_t lts16TempArray3By4[3][4];
#endif

uint8_t	lctcsu8FLMuHighCnt;         /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8FRMuHighCnt;         /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8RLMuHighCnt;         /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8RRMuHighCnt;         /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8NoOfFLMuHighCnt;     /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8NoOfFRMuHighCnt;     /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8NoOfRLMuHighCnt;     /* #if __TCS_HUNTING_DETECTION */
uint8_t	lctcsu8NoOfRRMuHighCnt;     /* #if __TCS_HUNTING_DETECTION */

int16_t		ltcss16BumpCounter;		/* #if __TCS_BUMP_DETECTION */
uint16_t	ltcsu16SumOfSpin;		/* #if __TCS_BUMP_DETECTION */
uint16_t	ltcsu16AvgOfSpin;		/* #if __TCS_BUMP_DETECTION */
int16_t		ltcs16BumpArad;
#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)	
int8_t ldtcs8BumpWheelIndexByArad;
int16_t	ltcs16BumpAradFront;
int16_t	ltcs16BumpAradRear;
#endif


int16_t 	lctcss16NewEngOKTorq;		/* __VARIABLE_MINIMUM_TORQ_LEVEL */


uint8_t lctcsu8VIBEngineCtlScan;
uint16_t lctcsu16VIBEngineCtlCnt;
uint16_t lctcsu16VIBEngineCtlPeriod;
int16_t lctcss16VIBCalTorqState;
int16_t lctcss16VIBCalTorqLevel;
/*Structure 구조 사용 불가로 pragma 내 변수 선언*/


#if __TCS_DRIVELINE_PROTECT
int16_t	ltcss16FrontAxleMeanSpd, ltcss16RearAxleMeanSpd, ltcss16DeltaSpdOfFrontAndRear, ltcss16DeltaSpdOfFLandFR, ltcss16DeltaSpdOfRLandRR;
#endif	/* #if __TCS_DRIVELINE_PROTECT */

/*Structure 구조 사용 불가로 pragma 내 변수 선언*/  

/* A/T gear control correction */
uint8_t	lespu8_BSTGRReqTypeOld;
/* A/T gear control correction */

int16_t	lctcss16LowSpinCntIn1stCycle;

/* TORQ_RECOVERY_DEPEND_ON_STEER */
int8_t	lctcss8SteerToZeroCnt;
/* TORQ_RECOVERY_DEPEND_ON_STEER */

/* MAXIMUM_RPM_LIMIT */
int16_t lctcss16AllowableMaxRPM;
int16_t lctcss16OverMaxRPMCnt;
int16_t lctcss16OverMaxRPMCntold;
int16_t lctcss16Spd1ForRPMLimit,lctcss16Spd2ForRPMLimit;
int16_t lctcss16AllowableMinRPM;
int16_t lctcss16UnderMinRPMCnt;
int16_t lctcss16UnderMinRPMCntold;
/* MAXIMUM_RPM_LIMIT */

/* ENGINE_STALL_DETECION_REFINE */
int16_t ldtcss16HillDetectSustainCnt;
int16_t ldtcss16StallRiskIndex;
int16_t ldtcss16StallDctRPM;
/* ENGINE_STALL_DETECION_REFINE */

 
/*__TCS_CTRL_DEC_IN_GR_SHFT_MT*/
uint16_t lctcsu16GearShiftForMTcnt;
/*__TCS_CTRL_DEC_IN_GR_SHFT_MT*/

/*For BMW Demo*/
uint8_t lctcsu8FunctionLampOnCount;
uint16_t lctcsu16FunctionLampOnTime;
uint16_t lctcsu16FunctionLampOffCount;
/*For BMW Demo*/

uint8_t	lespu8TqIntvnTyp;
uint16_t	lespu16TorqReqVal;

int16_t lctcss16EstMuInTurn;
int16_t lctcss16EstMuInstraight;

/*TCS spin for enter threshold*/ 
int16_t lctcss16BigSpin;
int16_t lctcss16SmallSpin;

	#if  (__IDB_SYSTEM == ENABLE)
int16_t latcss16TarPreGrdtMd;
int16_t latcss16PricTarPreRate;
int16_t latcss16SeccTarPreRate;
	#endif