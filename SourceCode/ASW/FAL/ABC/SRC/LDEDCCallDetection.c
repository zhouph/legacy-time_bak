/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDEDCCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ********************************************************/
#include "LDEDCCallDetection.h"
#include "LCESPCalInterpolation.h"
#include "LCEDCCallControl.h"

#include "LDRTACallDetection.h"

/* Local Definition  ***********************************************/

#if __EDC

/* Variables Definition*********************************************/
	#if __EDC_MAX_TIME_LIMIT
static uint8_t lcedcu8ActCnt1S;
	#endif

/* Local Function prototype ****************************************/

void LDEDC_vCallDetection(void);
void LDEDC_vDetectNorEdc(void);
void LDEDC_vDetectNorEdcWheel(struct W_STRUCT *WL);
void LDEDC_vLimitEDCEntry(void);
static void LDEDC_vCalcEDCEntryThreshold(void);
void LDEDC_vCount1SecAfterEDC(void);	
void LDEDC_vInitializeEdcDetection(void);
void LDEDC_vParkingBrakeSuspect(struct W_STRUCT *WL);
void LDEDC_vParkingBrakeOperation(void);
	#if __EDC_NEUTRAL_PARKING_DETECTION
void LDEDC_vWheelLockDetect(void);
	#endif
	#if __EDC_TEMP_TIRE_DETECTION	
void LDEDC_vEnableMiniWheelDetect(void);
	#endif
	#if __EDC_HIGH_MU_DETECTION	
void LDEDC_vDetectHighMu(void);
	#endif
	#if __EDC_GEAR_SHIFT_DOWN_DETECTION
void LDEDC_vDetectGearShiftDown(void);
	#endif

#endif

/* Implementation***************************************************/

#if __EDC  /*TAG1*/

void LDEDC_vCallDetection(void)
{
    if((Engine_Drag_flag==1)&&(EDC_INHIBIT_flag==0))
    {
    	EDC_flags=2;
        	#if __REAR_D
    	if(((RL.ldedcu1EdcNorOn==1) || (RR.ldedcu1EdcNorOn==1)) && ((lcedcs16PGainEffect + lcedcs16DGainEffect) < 0)
    	      #if __ECU!=ABS_ECU_1
    		&& ( (((ABS_ON_flag==0)&&(YAW_CDC_WORK==0)) && ((delta_yaw_first<-U8_EDC_ENT_THR_DEL_YAW)||(delta_yaw_first>U8_EDC_ENT_THR_DEL_YAW))) ||
    		     ((ABS_ON_flag==1)||(YAW_CDC_WORK==1)) )
    		    #endif
    		)
    		#else
    	if(((FL.ldedcu1EdcNorOn==1) || (FR.ldedcu1EdcNorOn==1)) && ((lcedcs16PGainEffect + lcedcs16DGainEffect) < 0)
    	      #if __ECU!=ABS_ECU_1    		
    		&& ( (((ABS_ON_flag==0)&&(YAW_CDC_WORK==0)) && ((delta_yaw_first<-U8_EDC_ENT_THR_DEL_YAW)||(delta_yaw_first>U8_EDC_ENT_THR_DEL_YAW))) ||
    			 ((ABS_ON_flag==1)||(YAW_CDC_WORK==1)) )
    		    #endif    		
    		)    
    		#endif		
    	{
    		EDC_NOR_ON_FLAG =1;
    	}
    	else
    	{
    		EDC_NOR_ON_FLAG =0;			
    	}    	
    	
    	if(U8_TORQ_UP_LIMIT_SOLO>U8_TORQ_UP_LIMIT_ABS)
    	{
    		if(U8_TORQ_UP_LIMIT_SOLO>U8_TORQ_UP_LIMIT_ESC)
    		{
    			tempW0=U8_TORQ_UP_LIMIT_SOLO*10;
    		}
    		else
    		{
    			tempW0=U8_TORQ_UP_LIMIT_ESC*10;
    		}			
    	}
    	else if(U8_TORQ_UP_LIMIT_ABS>U8_TORQ_UP_LIMIT_ESC)		
    	{
    		tempW0=U8_TORQ_UP_LIMIT_ABS*10;
    	}
    	else
    	{
    		tempW0=U8_TORQ_UP_LIMIT_ESC*10;
    	}
    					
    	if(MSR_cal_torq > tempW0)    	
    	{
    		EDC_NOR_ON_FLAG =0;
    	}
    	else
    	{
    		;
    	}				
    	
    }
    else 
    {
    	LDEDC_vInitializeEdcDetection();
	}	

	if(EDC_NOR_ON_FLAG==1)
	{
		if(ABS_ON_flag==1)
		{
			EDC_ABS_ON=1;
		}
			#if __VDC
		else if(YAW_CDC_WORK==1)
		{
			EDC_ESP_ON=1;
		}			
			#endif
		else
		{
			EDC_ABS_ON=0;
				#if __VDC
			EDC_ESP_ON=0;
				#endif
			EDC_SOLO_ON=1;
		}
	}
	else
	{
		;
	}		
							
					    
			
  #if __VDC	
    if((EDC_ABS_ON==1)||(EDC_SOLO_ON==1)||(EDC_ESP_ON==1))
  #else  	
  	if((EDC_ABS_ON==1)||(EDC_SOLO_ON==1))
  #endif		
	{
		EDC_ON = 1;
	}
	else
	{
		;
	}	        
}

void LDEDC_vDetectNorEdc(void) 
{
    if((max_speed<=VREF_2_KPH_RESOL_CHANGE)&&(EDC_ON==0))
    {
        ldedcu1EDCPermitSpeed=0;
    }
    else
    {
        ;
    }	
		#if __REAR_D	
    if(min_speed>=VREF_15_KPH_RESOL_CHANGE)
    {
        ldedcu1EDCPermitSpeed=1;
    }
		#else
    if(min_speed>=VREF_20_KPH_RESOL_CHANGE)
    {
        ldedcu1EDCPermitSpeed=1;
    }
    	#endif		    
    else
    {
        ;
    }

	if(LOOP_TIME_20MS_TASK_0==1)
	{
	    LDEDC_vCalcEDCEntryThreshold();
	}
	else {}


  #if !__4WD
   #if __REAR_D
	LDEDC_vDetectNorEdcWheel(&RL);
	LDEDC_vDetectNorEdcWheel(&RR);
   #else
	LDEDC_vDetectNorEdcWheel(&FL);
	LDEDC_vDetectNorEdcWheel(&FR);
   #endif
  #else
  	LDEDC_vDetectNorEdcWheel(&FL);
	LDEDC_vDetectNorEdcWheel(&FR);
	LDEDC_vDetectNorEdcWheel(&RL);
	LDEDC_vDetectNorEdcWheel(&RR);
  #endif
}

void LDEDC_vDetectNorEdcWheel(struct W_STRUCT *WL)
{
    if(!ldedcu1EdcDecelDetect_1G5_wl) 
    {
        if(WL->arad<=(int8_t)(-ARAD_1G5)) 
        {
            if(WL->rel_lam<0) 
            {
              		#if __VDC	
              	WL->ldedcs16DeltaVelAtMinusB_1G5=WL->vrad_crt-WL->wvref;
              		#else	
                WL->ldedcs16DeltaVelAtMinusB_1G5=WL->vrad_crt-vref;
                	#endif
                ldedcu1EdcDecelDetect_1G5_wl=1;
            }
            else
            {}
        }
        else
        {}	
    }
    else
    {
			#if __VDC    	
    	if((vref5 >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
    		#else
    	if((vref >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
    		#endif    		
    	{	
    	    if( lcedcs16WheelSlipError <= 0)		
    	  	{	
        		if( (mtp < 2) || (drive_torq < fric_torq) )			/* 050114 mtp value?? (BOSCH 20??)*/		
        		{
            			#if __VDC
            	    tempW2=WL->vrad_crt-WL->wvref;
            	    	#else	
            	    tempW2=WL->vrad_crt-vref;
            	    	#endif
           	 		tempW2-=WL->ldedcs16DeltaVelAtMinusB_1G5;
               		WL->ldedcs16DeltaVelocity=tempW2;
    
            		if 	((EDC_NOR_ON_FLAG == 0)&&(EDC_SUSTAIN_1SEC==1))
            		{
                		if	(tempW2<ldedcs16EDCEntryTHR0Final) 
                		{
                    		ldedcu1EdcDecelDetect_1G5_wl=0;
                    		ldedcu1EdcDecelDetect_wl=0;
                    		ldedcu1EdcNorOn_wl=1;
                    		WL->lcedcu1EdcWheelExit=0;
                		}
                		else{}
            		}		
            		else
            		{
                		if	((tempW2<ldedcs16EDCEntryTHR0Final) && (WL->rel_lam < S8_EDC_ENT_THR_REL_LAM_AT_B_DCT)) 
                		{
                    		ldedcu1EdcDecelDetect_1G5_wl=0;
                    		ldedcu1EdcDecelDetect_wl=0;
                    		ldedcu1EdcNorOn_wl=1;
                    		WL->lcedcu1EdcWheelExit=0;
                		}
                		else{}
            		}				
    	    	}
    	    	else
            	{}	
          	}
          	else
          	{}	  
    	}
    	else
        {}	  
    }      

    if(!ldedcu1EdcDecelDetect_wl) 
    {
		if(WL->arad<=(int8_t)(-ARAD_0G5))
        {
            if(WL->rel_lam<0) 
            {
              		#if __VDC	
              	WL->ldedcs16DeltaVelAtMinusB_0G5=WL->vrad_crt-WL->wvref;
              		#else	
                WL->ldedcs16DeltaVelAtMinusB_0G5=WL->vrad_crt-vref;
                	#endif
                ldedcu1EdcDecelDetect_wl=1;
            }
            else
            {}
        }
        else
        {}	
    }
    else
    {
			#if __VDC    	
    	if((vref5 >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
    		#else    	
    	if((vref >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
    		#endif
    	{	
    	    if( lcedcs16WheelSlipError <= 0)		
    	  	{	
        		if( (mtp < 2) || (drive_torq < fric_torq) )			/* 050114 mtp value?? (BOSCH 20??)*/		
        		{
            			#if __VDC
            	    tempW1=WL->vrad_crt-WL->wvref;
            	    	#else	
            	    tempW1=WL->vrad_crt-vref;
            	    	#endif
           	 		tempW1-=WL->ldedcs16DeltaVelAtMinusB_0G5;
    
            		if 	((EDC_NOR_ON_FLAG == 0)&&(EDC_SUSTAIN_1SEC==1))
            		{
                		if	(tempW1<ldedcs16EDCEntryTHR1Final) 
                		{
            				ldedcu1EdcDecelDetect_1G5_wl=0;
                			ldedcu1EdcDecelDetect_wl=0;
                			ldedcu1EdcNorOn_wl=1;
                			WL->lcedcu1EdcWheelExit=0;
                		}
                		else{}
            		}		
            		else
            		{
                		if	((tempW1<ldedcs16EDCEntryTHR1Final) && (WL->rel_lam < S8_EDC_ENT_THR_REL_LAM_AT_B_DCT)) 
                		{
            				ldedcu1EdcDecelDetect_1G5_wl=0;
                			ldedcu1EdcDecelDetect_wl=0;
                			ldedcu1EdcNorOn_wl=1;
                			WL->lcedcu1EdcWheelExit=0;
                		}
                		else{}
            		}    
    	    	}
    	    	else
            	{}	
          	}
          	else
          	{}	  
    	}
    	else
        {}	  
    }

		#if __VDC    	
   	if((vref5 >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
   		#else     
	if((vref >= S16_EDC_INHIBIT_SPEED)&&(ldedcu1EDCPermitSpeed==1))
		#endif
	{	
	    if( lcedcs16WheelSlipError <= 0)		
	  	{	
    		if( (mtp < 2) || (drive_torq < fric_torq) )			/* 050114 mtp value?? (BOSCH 20??)*/		
    		{
        			#if __VDC
        		tempW0=WL->vrad_crt-WL->wvref;
        	    	#else	
		        tempW0=WL->vrad_crt-vref;
        	    	#endif
        		if 	((EDC_NOR_ON_FLAG == 0)&&(EDC_SUSTAIN_1SEC==1))
        		{
            		if	(tempW0<ldedcs16EDCEntryTHR2Final) 
            		{
        				ldedcu1EdcDecelDetect_1G5_wl=0;
            			ldedcu1EdcDecelDetect_wl=0;
            			ldedcu1EdcNorOn_wl=1;
            			WL->lcedcu1EdcWheelExit=0;
            		}
            		else{}
        		}		
        		else
        		{
            		if	((tempW0<ldedcs16EDCEntryTHR2Final) && (WL->rel_lam < S8_EDC_ENT_THR_REL_LAM)) 
            		{
        				ldedcu1EdcDecelDetect_1G5_wl=0;
            			ldedcu1EdcDecelDetect_wl=0;
            			ldedcu1EdcNorOn_wl=1;
            			WL->lcedcu1EdcWheelExit=0;
            		}
            		else{}
        		}
	    	}
	    	else
        	{}	
      	}
      	else
      	{}	  
	}
	else
    {}	

    if(WL->arad>=(int8_t)ARAD_0G0) 
    {
        ldedcu1EdcDecelDetect_wl=0;
		ldedcu1EdcDecelDetect_1G5_wl=0;
    }
	else
    {}	
}

static void LDEDC_vCalcEDCEntryThreshold(void)
{
	if(ABS_ON_flag==1)
	{	
    	ldedcs8EDCEntryTHR0 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR0_ABS_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR0_ABS_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR0_ABS_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR0_ABS_S4);		
    	ldedcs8EDCEntryTHR1 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR1_ABS_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR1_ABS_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR1_ABS_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR1_ABS_S4);	
    	ldedcs8EDCEntryTHR2 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR2_ABS_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR2_ABS_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR2_ABS_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR2_ABS_S4);	
	}
		#if __VDC	
	else if(YAW_CDC_WORK==1)
	{
    	ldedcs8EDCEntryTHR0 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR0_ESC_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR0_ESC_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR0_ESC_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR0_ESC_S4);		
    	ldedcs8EDCEntryTHR1 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR1_ESC_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR1_ESC_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR1_ESC_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR1_ESC_S4);	
    	ldedcs8EDCEntryTHR2 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR2_ESC_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR2_ESC_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR2_ESC_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR2_ESC_S4);
	}
		#endif	
	else		
	{
    	ldedcs8EDCEntryTHR0 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR0_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR0_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR0_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR0_S4);
    	ldedcs8EDCEntryTHR1 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR1_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR1_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR1_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR1_S4);	
    	ldedcs8EDCEntryTHR2 = (int8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_ENTRY_THR2_S1,
    				     				    			  U16_EDC_SPEED2, (int16_t)S8_EDC_ENTRY_THR2_S2,
    									  				  U16_EDC_SPEED3, (int16_t)S8_EDC_ENTRY_THR2_S3,
    									  				  U16_EDC_SPEED4, (int16_t)S8_EDC_ENTRY_THR2_S4);
    }	
									  				  
	if((EDC_NOR_ON_FLAG == 0)&&(EDC_SUSTAIN_1SEC==1))
	{
		EDC_flags=7;		
		ldedcs16EDCEntryTHR0Final = (int16_t)(ldedcs8EDCEntryTHR0 + S8_EDC_DEC_ENT_THR_INC_IN_CTL);		
		ldedcs16EDCEntryTHR1Final = (int16_t)(ldedcs8EDCEntryTHR1 + S8_EDC_DEC_ENT_THR_INC_IN_CTL);
		ldedcs16EDCEntryTHR2Final = (int16_t)(ldedcs8EDCEntryTHR2 + S8_EDC_DEC_ENT_THR_INC_IN_CTL);	
	}
	else if(ABS_ON_flag==1)							/*EDC_ABS Combination Control Entry Condition*/
	{
		EDC_flags=3;
		ldedcs16EDCEntryTHR0Final = (int16_t)(ldedcs8EDCEntryTHR0);		
		ldedcs16EDCEntryTHR1Final = (int16_t)(ldedcs8EDCEntryTHR1);
		ldedcs16EDCEntryTHR2Final = (int16_t)(ldedcs8EDCEntryTHR2);	
		LDEDC_vLimitEDCEntry();
	}
		#if __VDC
	else if(YAW_CDC_WORK==1)					/*EDC_ESC Combination Control Entry Condition*/
	{
		EDC_flags=4;
		ldedcs16EDCEntryTHR0Final = (int16_t)(ldedcs8EDCEntryTHR0);		
		ldedcs16EDCEntryTHR1Final = (int16_t)(ldedcs8EDCEntryTHR1);
		ldedcs16EDCEntryTHR2Final = (int16_t)(ldedcs8EDCEntryTHR2);		
		LDEDC_vLimitEDCEntry();
	}			
		#endif		
	else										/*EDC_SOLO Control Entry Condition*/
	{
		EDC_flags=5;
		ldedcs16EDCEntryTHR0Final = (int16_t)(ldedcs8EDCEntryTHR0);
		ldedcs16EDCEntryTHR1Final = (int16_t)(ldedcs8EDCEntryTHR1);
		ldedcs16EDCEntryTHR2Final = (int16_t)(ldedcs8EDCEntryTHR2);	
		LDEDC_vLimitEDCEntry();
	}
	
				#if __EDC_COMPENSATION_IN_TURN
			#if !__SIDE_VREF	
	  #if __REAR_D
	if(vrad_crt_fl>=vrad_crt_fr)
	{		
		tempW3 = (vrad_crt_fl - vrad_crt_fr);
	}
	else
	{
		tempW3 = (vrad_crt_fr - vrad_crt_fl);
	}			
	  #else /* FWD */
	if(vrad_crt_rl>=vrad_crt_rr)
	{		
		tempW3 = (vrad_crt_rl - vrad_crt_rr);
	}
	else
	{
		tempW3 = (vrad_crt_rr - vrad_crt_rl);
	}	
	  #endif 	  
			#else /* __SIDE_VREF */
	tempW3 = McrAbs(vref_L-vref_R);
			#endif 

	if(tempW3 > VREF_5_KPH)
	{
		tempW3 = VREF_5_KPH;
	}
	
/*	if(tempW3>=VREF_1_KPH) 
	if((tempW3>=VREF_1_KPH)&&(ABS_fz==0)&&(YAW_CDC_WORK==0))	*/	
	
	#if __VDC
	if((ABS_fz==0)&&(YAW_CDC_WORK==0))
	#else
	if(ABS_fz==0)
	#endif
	{	
		if(tempW3>=VREF_1_KPH)
		{  
			ldedcs16EDCEntryTHR0Final = ldedcs16EDCEntryTHR0Final - tempW3;
			ldedcs16EDCEntryTHR1Final = ldedcs16EDCEntryTHR1Final - tempW3;
			ldedcs16EDCEntryTHR2Final = ldedcs16EDCEntryTHR2Final - tempW3;
		}
		else { }
	}
	else 
	{
		/* detect bend -> threshold comp */
		if((BEND_DETECT2==1) && (tempW3>=VREF_2_KPH))
		{
			ldedcs16EDCEntryTHR0Final = ldedcs16EDCEntryTHR0Final - (tempW3/2);
			ldedcs16EDCEntryTHR1Final = ldedcs16EDCEntryTHR1Final - (tempW3/2);
			ldedcs16EDCEntryTHR2Final = ldedcs16EDCEntryTHR2Final - (tempW3/2);
		}
		else { }				
	}	
			#endif 	/* __EDC_COMPENSATION_IN_TURN */	
		
	if ( (Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1) )  
	{		
		EDC_flags=6;
		ldedcs16EDCEntryTHR0Final = ldedcs16EDCEntryTHR0Final + (int16_t)(S8_EDC_DEC_ENT_THR_IN_RR);
		ldedcs16EDCEntryTHR1Final = ldedcs16EDCEntryTHR1Final + (int16_t)(S8_EDC_DEC_ENT_THR_IN_RR);
		ldedcs16EDCEntryTHR2Final = ldedcs16EDCEntryTHR2Final + (int16_t)(S8_EDC_DEC_ENT_THR_IN_RR);
	}
	else
	{
		;
	}		
			
		#if __EDC_TEMP_TIRE_DETECTION	
	if (ldedcu1TempTireSuspect==1)
	{
		EDC_flags=8;
		if(ldedcu1TempTireSuspectInCtl==1)
		{
    			#if __VDC
    		tempW1 = (vref5 *3) / 10; /* 차속의 30% */
    			#else	
    		tempW1 = (vref *3) / 10; /* 차속의 30% */
    			#endif
		}
		else
		{		
    			#if __VDC
    		tempW1 = (vref5 / 50); /* Considered RTA confidence factor +-2% */
    			#else	
    		tempW1 = (vref / 50);
    			#endif
    	}		
		ldedcs16EDCEntryTHR0Final = ldedcs16EDCEntryTHR0Final - tempW1;
		ldedcs16EDCEntryTHR1Final = ldedcs16EDCEntryTHR1Final - tempW1;
		ldedcs16EDCEntryTHR2Final = ldedcs16EDCEntryTHR2Final - tempW1;
	}
	else
	{}	
		#endif
	
    }
    
void LDEDC_vLimitEDCEntry(void)
            		{
	if(ldedcs16EDCEntryTHR0Final>=S8_EDC_ENTRY_LIMIT)
                		{
		ldedcs16EDCEntryTHR0Final=S8_EDC_ENTRY_LIMIT;
            		}		
            		else
	{}

	if(ldedcs16EDCEntryTHR1Final>=S8_EDC_ENTRY_LIMIT)
            		{
		ldedcs16EDCEntryTHR1Final=S8_EDC_ENTRY_LIMIT;
    	    	}
    	    	else
	{}

	if(ldedcs16EDCEntryTHR2Final>=S8_EDC_ENTRY_LIMIT)
    {
		ldedcs16EDCEntryTHR2Final=S8_EDC_ENTRY_LIMIT;
            }
            else
	{}
        }

void LDEDC_vCount1SecAfterEDC(void)
    	{	
  #if __EDC_MAX_TIME_LIMIT	
	static uchar8_t lcedcu8ActTime;
  #endif /* __EDC_MAX_TIME_LIMIT */	
    
	if(EDC_NOR_ON_FLAG==1)
            		{
		ledcu161SecCntAfterEDC = L_U8_TIME_10MSLOOP_1000MS;
		EDC_SUSTAIN_1SEC = 1;
            		}		
            		else
            		{
		if (ledcu161SecCntAfterEDC>0)
                		{
			ledcu161SecCntAfterEDC--;
    	    	}
    	    	else
		{
			EDC_SUSTAIN_1SEC = 0;
			ledcu161SecCntAfterEDC = 0;
          	}
    }

  #if __EDC_MAX_TIME_LIMIT	
	if(EDC_ON==1)
    		{
		if(lcedcu8ActCnt1S < L_U8_TIME_10MSLOOP_1000MS)
            		{
			lcedcu8ActCnt1S = lcedcu8ActCnt1S + 1;
        		}		
        		else
        		{
			lcedcu8ActCnt1S = 0;
			lcedcu8ActTime = lcedcu8ActTime + 1;
        		}

		if(lcedcu8ActTime >= U8_EDC_CONTROL_TIME_MAX)
    {
			lcedcu1CtrlTimeOver = 1;
    }
	else
{
			lcedcu1CtrlTimeOver = 0;
	}
	}
	else
	{
		lcedcu8ActCnt1S = 0;
		lcedcu8ActTime = 0;

		if(lcedcu1CtrlTimeOver == 1)
{
			if(EDC_SUSTAIN_1SEC == 0)
	{
				lcedcu1CtrlTimeOver = 0;
		}
		}
	}
  #endif /* __EDC_MAX_TIME_LIMIT */	
}

void LDEDC_vInitializeEdcDetection(void)
{
   	FL.ldedcu1EdcNorOn=FR.ldedcu1EdcNorOn=RL.ldedcu1EdcNorOn=RR.ldedcu1EdcNorOn=ldedcu1EdcNorOn_wl=EDC_NOR_ON_FLAG=0;
	FL.ldedcu1EdcDecelDetect=FR.ldedcu1EdcDecelDetect=RL.ldedcu1EdcDecelDetect=RR.ldedcu1EdcDecelDetect=ldedcu1EdcDecelDetect_wl=0;
	FL.ldedcu1EdcDecelDetect_1G5=0;
	FR.ldedcu1EdcDecelDetect_1G5=0;
	RL.ldedcu1EdcDecelDetect_1G5=0;
	RR.ldedcu1EdcDecelDetect_1G5=0;
	ldedcu1EdcDecelDetect_1G5_wl=0;
	EDC_ON=0;
}			

void LDEDC_vParkingBrakeSuspect(struct W_STRUCT *WL)
{
    if(!ldedcu1ParkDecelCheckWL_wl) 
    {
        if(WL->arad<=(int8_t)(-ARAD_2G0))
        {
            if(WL->rel_lam<0) 
            {
                WL->ldedcu1ParkDecelCheckWL=1;
        		WL->ldedcu8ParkDecelCheckCnt++;
        		if(WL->ldedcu8ParkDecelCheckCnt==1)
        		{
        			WL->ldedcu16EngRpmAtDecelCheck=eng_rpm;
        		}
        		else if(WL->ldedcu8ParkDecelCheckCnt>=L_U8_TIME_10MSLOOP_70MS)
        		{
        			WL->ldedcu8ParkDecelCheckCnt=L_U8_TIME_10MSLOOP_70MS;
        		}
        		else
        		{}		
            }
            else
            {}
        }
        else
        {}
    }
    else 
    {
       	tempW1=eng_rpm;
       	tempW1-=WL->ldedcu16EngRpmAtDecelCheck;
            	    		
		if( ((WL->rel_lam <= (int8_t)(-LAM_50P))||(WL->vrad_crt <= VREF_3_KPH))
			&&(slope_eng_rpm<-2)&&(tempW1<S16_EDC_DEL_ENG_RPM_FOR_PARK)&&(WL->ldedcu8ParkBrkSuspectCounter<L_U8_TIME_10MSLOOP_70MS) )
		{
		    WL->ldedcu8ParkBrkSuspectCounter++;
		}
		else if(((RL.rel_lam >= (int8_t)(-LAM_5P))&&(RR.rel_lam >= (int8_t)(-LAM_5P)))||((vref-RL.vrad_crt <= VREF_2_KPH)&&(vref-RR.vrad_crt <= VREF_2_KPH)))
		{
			WL->ldedcu8ParkBrkSuspectCounter=0;
		}        		
		else
		{}

		if(WL->ldedcu8ParkBrkSuspectCounter>=L_U8_TIME_10MSLOOP_70MS)
		{
			WL->ldedcu1ParkBrkSuspectWL=1;
			WL->ldedcu1ParkDecelCheckWL=0;
			WL->ldedcu8ParkBrkSuspectCounter=L_U8_TIME_10MSLOOP_70MS;
		}
		else if(WL->ldedcu8ParkBrkSuspectCounter==0)
		{
			WL->ldedcu1ParkBrkSuspectWL=0;
		}
		else
		{}	       			
    }
    
	if(((RL.rel_lam >= (int8_t)(-LAM_5P))&&(RR.rel_lam >= (int8_t)(-LAM_5P)))||((vref-RL.vrad_crt <= VREF_2_KPH)&&(vref-RR.vrad_crt <= VREF_2_KPH)))
   	{
   		WL->ldedcu1ParkBrkSuspectWL=0;
   		WL->ldedcu1ParkDecelCheckWL=0;				
   		WL->ldedcu8ParkBrkSuspectCounter=0;    
   		WL->ldedcu8ParkDecelCheckCnt=0;
   		WL->ldedcu16EngRpmAtDecelCheck=0;
   	}
   	else
   	{}					
}

void LDEDC_vParkingBrakeOperation(void)
{
	LDEDC_vParkingBrakeSuspect(&RL);
	LDEDC_vParkingBrakeSuspect(&RR);
	
	if((RL.ldedcu1ParkBrkSuspectWL==1)||(RR.ldedcu1ParkBrkSuspectWL==1))		
    {	  		
		ldedcu1ParkBrkSuspectFlag=1;
		RL.ldedcu16EngRpmAtDecelCheck=RR.ldedcu16EngRpmAtDecelCheck=0;
	}
	else
	{
		ldedcu1ParkBrkSuspectFlag=0;
	}
#if __EDC_NEUTRAL_PARKING_DETECTION	
	LDEDC_vWheelLockDetect();
#endif	
  	
	if(vref<=S8_EDC_PARKING_SUSPECT_LIMIT)
	{
		ldedcu1ParkBrkSuspectFlag=0;
		ldedcu1WheelLockDetectFlag=0;
		ldedcs16WheelLockSuspectCnt=0;
	}
	else
	{
		;
	}				
}

#if __EDC_NEUTRAL_PARKING_DETECTION
void LDEDC_vWheelLockDetect(void)
{
	if((RL.vrad_crt < VREF_2_5_KPH) && (RR.vrad_crt < VREF_2_5_KPH))	
	{
		ldedcs16WheelLockSuspectCnt=ldedcs16WheelLockSuspectCnt+8;
	}
	else if((RL.vrad_crt < VREF_2_5_KPH) || (RR.vrad_crt < VREF_2_5_KPH))
	{
		ldedcs16WheelLockSuspectCnt=ldedcs16WheelLockSuspectCnt+4;
	}
	else if(((RL.rel_lam >= (int8_t)(-LAM_5P))&&(RR.rel_lam >= (int8_t)(-LAM_5P)))||((vref-RL.vrad_crt <= VREF_2_KPH)&&(vref-RR.vrad_crt <= VREF_2_KPH)))
   	{
		ldedcs16WheelLockSuspectCnt=0;
	}
	else
	{}
	
	if(ldedcs16WheelLockSuspectCnt>=L_U8_TIME_10MSLOOP_1000MS)
	{	
		ldedcu1WheelLockDetectFlag=1;
		ldedcs16WheelLockSuspectCnt=L_U8_TIME_10MSLOOP_1000MS;
	}			
	else if(ldedcs16WheelLockSuspectCnt==0)
	{
		ldedcu1WheelLockDetectFlag=0;
	}		
	else
	{}
}	
#endif	

	#if __EDC_HIGH_MU_DETECTION
void LDEDC_vDetectHighMu(void)
{
	if((AFZ_OK==1) && (afz <= (-U8_HIGH_MU)))
	{
		ldedcu1HighMuSuspectFlag=1;
	}
	else
	{
		ldedcu1HighMuSuspectFlag=0;
	}			
}
	#endif

	#if __EDC_GEAR_SHIFT_DOWN_DETECTION
void LDEDC_vDetectGearShiftDown(void)
{
	if( (In_gear_flag==0) && (lcedcs16AvgDrivenWheelSlip < S8_EDC_GSD_ENTRY_WHL_SLIP) )	
	{		
		if( ( (slope_eng_rpm_for_GSD>=S8_EDC_GSD_SLOPE_ENG_RPM_THR) && (lcedcs16AvgDrvnWhlSlipDiff<=-S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR) ) || ( (slope_eng_rpm_for_GSD<=-S8_EDC_GSD_SLOPE_ENG_RPM_THR) && (lcedcs16AvgDrvnWhlSlipDiff>=S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR) ) )
        {
          #if __EDC_GSD_IMPROVE == DISABLE	
            ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount+2;
          #else
            if(lcedcs16AvgDrivenWheelSlip<((INT)S8_EDC_GSD_ENTRY_WHL_SLIP*3))
            {
            	ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount+4;
            }
            else if(lcedcs16AvgDrivenWheelSlip<((INT)S8_EDC_GSD_ENTRY_WHL_SLIP*2))
            {
            	ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount+3;
            }
            else
            {
            	ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount+2;
            }            
          #endif  /* __EDC_GSD_IMPROVE__EDC_GSD_IMPROVE == DISABLE */
            if(ldedcs8GearShiftDownCount>=L_U8_TIME_10MSLOOP_850MS)
    		{
    			ldedcs8GearShiftDownCount = L_U8_TIME_10MSLOOP_850MS;
    		}
    		else
    		{}	
        }
      #if __EDC_GSD_IMPROVE == ENABLE	      
        else if( ( (slope_eng_rpm_for_GSD>=1) && (lcedcs16AvgDrvnWhlSlipDiff<=0) ) || ( (slope_eng_rpm_for_GSD<=-1) && (lcedcs16AvgDrvnWhlSlipDiff>=0) ) )
        {
            if(  (ldedcu1GearShiftDownSuspectFlag==1)
             &&( ( (slope_eng_rpm_for_GSD>=(S8_EDC_GSD_SLOPE_ENG_RPM_THR/2)) && (lcedcs16AvgDrvnWhlSlipDiff<=-(S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR/2)) ) || ( (slope_eng_rpm_for_GSD<=-(S8_EDC_GSD_SLOPE_ENG_RPM_THR/2)) && (lcedcs16AvgDrvnWhlSlipDiff>=(S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR/2)) ) )
            )
            {
	            ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount+1;
	        }
            else
            {
	            ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount;
            }
        }    
      #endif /* __EDC_GSD_IMPROVE == ENABLE	*/
        else if( ( (slope_eng_rpm_for_GSD>=S8_EDC_GSD_SLOPE_ENG_RPM_THR) && (lcedcs16AvgDrvnWhlSlipDiff>=S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR) ) || ((slope_eng_rpm_for_GSD<=-S8_EDC_GSD_SLOPE_ENG_RPM_THR) && (lcedcs16AvgDrvnWhlSlipDiff<=-S8_EDC_GSD_AVG_WHL_SLIP_DIFF_THR)) )
        {
        	ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount-2;
        }	
        else    
        {
        	ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount-1;
        }
	}
  	else if( (In_gear_flag==1) && (lcedcs16AvgDrivenWheelSlip >= S8_EDC_GSD_EXIT_WHL_SLIP) )
	{
        ldedcu1GearShiftDownSuspectFlag = 0;
        ldedcs8GearShiftDownCount = 0;
    }
  #if __EDC_GSD_IMPROVE == ENABLE	      
    else if( (In_gear_flag==1) && ( (slope_eng_rpm_for_GSD>=1) && (lcedcs16AvgDrvnWhlSlipDiff>=1) ) || ((slope_eng_rpm_for_GSD<=-1) && (lcedcs16AvgDrvnWhlSlipDiff<=-1)) )
    {
        ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount - 1;
    }
  #endif /* __EDC_GSD_IMPROVE == ENABLE	*/
	else if(lcedcs16AvgDrivenWheelSlip > S8_EDC_GSD_ENTRY_WHL_SLIP)
	{
		ldedcs8GearShiftDownCount = ldedcs8GearShiftDownCount-1;
    }    
    else    			 
	{}
	
	if(ldedcs8GearShiftDownCount<=0)
	{
		ldedcs8GearShiftDownCount = 0;	
	}
	else
	{}        	 

    if(ldedcs8GearShiftDownCount>=S8_EDC_GSD_ENTRY_CNT)
    {
    	ldedcu1GearShiftDownSuspectFlag = 1;
    }	
    else if(ldedcs8GearShiftDownCount<=0) 
    {
    	ldedcu1GearShiftDownSuspectFlag = 0;
    }	
    else
    {}
}
	#endif

	#if __EDC_TEMP_TIRE_DETECTION
void LDEDC_vEnableMiniWheelDetect(void)
{
	if(ldedcu1TempTireSuspectInCtl==0)
	{	
			#if __VDC
    	tempW0 = (vrad_crt_fl - wvref_fl);
    	tempW1 = (vrad_crt_fr - wvref_fr);
    	tempW2 = (vrad_crt_rl - wvref_rl);
    	tempW3 = (vrad_crt_rr - wvref_rr);
    	tempW4 = (vref5 * 3) / 10;
    	    #else
     	tempW0 = (vrad_crt_fl - vref);		
    	tempW1 = (vrad_crt_fr - vref);
    	tempW2 = (vrad_crt_rl - vref);
    	tempW3 = (vrad_crt_rr - vref);
    	tempW4 = (vref * 3) / 10;
    		#endif   	    

    	/* 동반경이 30% 이상 차이나는 Temp Wheel은 감지 하지 않음 (GM SSTS : 25%) */
    	/* Mini Wheel / Big Wheel 이 장착되었다고 추정되는 wheel은 2KPH 이상의 Slip이 발생하여야 하고
    	   다른 Wheel은 1KPH 이하의 Slip이 발생하여야함.							     */
	/* 1 WHEEL Temp Tire Detection */
        	#if __REAR_D    	/* Rear wheel 구동의 경우 */
    		/* Non-driven 1 wheel Mini Wheel In FL */
    	if 		( ((tempW0 > -(VREF_1_5_KPH)) && (tempW0 < (VREF_1_5_KPH))) && 
    			  ((tempW1 < -VREF_2_KPH) && (tempW1 > -tempW4)) &&
    			  ((tempW2 < -VREF_2_KPH) && (tempW2 > -tempW4)) &&
    			  ((tempW3 < -VREF_2_KPH) && (tempW3 > -tempW4)) 	  )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW1; 
    	}
    		/* Non-driven 1 wheel Mini Wheel In FR */
    	else if ( ((tempW1 > -(VREF_1_5_KPH)) && (tempW1 < (VREF_1_5_KPH))) && 
    			  ((tempW0 < -VREF_2_KPH) && (tempW0 > -tempW4)) &&
    			  ((tempW2 < -VREF_2_KPH) && (tempW2 > -tempW4)) &&
    			  ((tempW3 < -VREF_2_KPH) && (tempW3 > -tempW4)) 	  )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW0;
    	}
    		/* Driven 1 wheel Big Wheel In RL */
    	else if ( ((tempW2 < -VREF_2_KPH) && (tempW2 > -tempW4)) &&
    			  ((tempW0 > -(VREF_1_5_KPH)) && (tempW0 < (VREF_1_5_KPH))) && 
    			  ((tempW1 > -(VREF_1_5_KPH)) && (tempW1 < (VREF_1_5_KPH))) && 
    			  ((tempW3 > -(VREF_1_5_KPH)) && (tempW3 < (VREF_1_5_KPH))) )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW2;
    	}
    		/* Driven 1 wheel Big Wheel In RR */
    	else if ( ((tempW3 < -VREF_2_KPH) && (tempW3 > -tempW4)) &&
    			  ((tempW0 > -(VREF_1_5_KPH)) && (tempW0 < (VREF_1_5_KPH))) && 
    			  ((tempW1 > -(VREF_1_5_KPH)) && (tempW1 < (VREF_1_5_KPH))) && 
    			  ((tempW2 > -(VREF_1_5_KPH)) && (tempW2 < (VREF_1_5_KPH))) )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW3;
    	}
    		#else  		/* Front wheel 구동의 경우 */	
    		/* Driven 1 wheel Big Wheel In FL */
    	if 		( ((tempW0 < -VREF_2_KPH) && (tempW0 > -tempW4)) &&
    			  ((tempW1 > -(VREF_1_5_KPH)) && (tempW1 < (VREF_1_5_KPH))) && 
    			  ((tempW2 > -(VREF_1_5_KPH)) && (tempW2 < (VREF_1_5_KPH))) && 
    			  ((tempW3 > -(VREF_1_5_KPH)) && (tempW3 < (VREF_1_5_KPH))) )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW0;
    	}
    		/* Driven 1 wheel Big Wheel In FR */
    	else if ( ((tempW1 < -VREF_2_KPH) && (tempW1 > -tempW4)) &&
    			  ((tempW0 > -(VREF_1_5_KPH)) && (tempW0 < (VREF_1_5_KPH))) && 
    			  ((tempW2 > -(VREF_1_5_KPH)) && (tempW2 < (VREF_1_5_KPH))) && 
    			  ((tempW3 > -(VREF_1_5_KPH)) && (tempW3 < (VREF_1_5_KPH))) )
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW1;
    	}
    		/* Non-driven 1 wheel Mini Wheel In RL */
    	else if	( ((tempW2 > -(VREF_1_5_KPH)) && (tempW2 < (VREF_1_5_KPH))) && 
    			  ((tempW1 < -VREF_2_KPH) && (tempW1 > -tempW4)) &&
    			  ((tempW0 < -VREF_2_KPH) && (tempW0 > -tempW4)) &&
    			  ((tempW3 < -VREF_2_KPH) && (tempW3 > -tempW4)) 	  )    	
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW3;
    	}
    		/* Non-driven 1 wheel Mini Wheel In RR */
    	else if	( ((tempW3 > -(VREF_1_5_KPH)) && (tempW3 < (VREF_1_5_KPH))) && 
    			  ((tempW1 < -VREF_2_KPH) && (tempW1 > -tempW4)) &&
    			  ((tempW0 < -VREF_2_KPH) && (tempW0 > -tempW4)) &&
    			  ((tempW2 < -VREF_2_KPH) && (tempW2 > -tempW4)) 	  )      	
    	{
    		ldedcu1DetectTempTire_1WL = 1;
    		ldedcs16velgap = tempW2;
    	}
    		#endif     	
    	else
    	{
    		ldedcu1DetectTempTire_1WL = 0;
    	}
	
	/* 2 WHEEL Temp Tire Detection */
        	#if __REAR_D    	/* Rear wheel 구동의 경우 */
    		/* Non-driven 2 wheel Mini Wheel In FL , FR */
    		/* Driven 2 wheel Big Wheel In RL , RR */    		
    	if		( ((tempW0 > -(VREF_1_5_KPH)) && (tempW0 < (VREF_1_5_KPH))) && 
    			  ((tempW1 > -(VREF_1_5_KPH)) && (tempW1 < (VREF_1_5_KPH))) &&
    			  ((tempW2 < -VREF_2_KPH) && (tempW2 > -tempW4)) &&
    			  ((tempW3 < -VREF_2_KPH) && (tempW3 > -tempW4)) 	  )
    	{
    		ldedcu1DetectTempTire_2WL = 1;
    		ldedcs16velgap = tempW2;
    		ldedcs16velgap2 = tempW3;    		
    	}   	
    		#else  		/* Front wheel 구동의 경우 */	
    		/* Driven 2 wheel Big Wheel In FL , FR */
    		/* Non-driven 2 wheel Mini Wheel In RL , RR */     		
    	if		( ((tempW2 > -(VREF_1_5_KPH)) && (tempW2 < (VREF_1_5_KPH))) && 
    			  ((tempW3 > -(VREF_1_5_KPH)) && (tempW3 < (VREF_1_5_KPH))) &&
    			  ((tempW0 < -VREF_2_KPH) && (tempW0 > -tempW4)) &&
    			  ((tempW1 < -VREF_2_KPH) && (tempW1 > -tempW4)) 	  )
    	{
    		ldedcu1DetectTempTire_2WL = 1;
    		ldedcs16velgap = tempW0;
    		ldedcs16velgap2 = tempW1;     		
    	}  
    		#endif     	    	 	
    	else
    	{
    		ldedcu1DetectTempTire_2WL = 0;
    	}    	
    	
    	ledcs16VelGapSum = ledcs16VelGapSum - ledcs16VelGapBuffer[ledcs16VelGapIndex] + ldedcs16velgap;
    	ledcs16VelGapBuffer[ledcs16VelGapIndex] = ldedcs16velgap;
    	ldedcs16velgapatEDCentry = ledcs16VelGapSum / (int16_t)U8_VEL_GAP_MOVING_WINDOW_LENGTH;
    
    	ledcs16VelGapSum2 = ledcs16VelGapSum2 - ledcs16VelGapBuffer2[ledcs16VelGapIndex] + ldedcs16velgap2;
    	ledcs16VelGapBuffer2[ledcs16VelGapIndex] = ldedcs16velgap2;
    	ldedcs16velgapatEDCentry2 = ledcs16VelGapSum2 / (int16_t)U8_VEL_GAP_MOVING_WINDOW_LENGTH;
    	
    	if (ledcs16VelGapIndex==(U8_VEL_GAP_MOVING_WINDOW_LENGTH-1))
    	{
    		ledcs16VelGapIndex = 0;
    	}
    	else
    	{
    		ledcs16VelGapIndex++;
    	}    				
    	
    	/* Temp tire 미감지시, 제어 진입했을 경우 제어 해제를 위해, 일정슬립으로 3sec간 유지되면, Temp tire로 의심해, 제어 종료 유도*/
        if(EDC_ON == 0)
        {    	
            ldedcs16TempTireTendCntInCtl=0;
            ldedcu1TempTireSuspectInCtl=0;
            ldedcs16InitialEDCSlip=0;
            ldedcs16InitialEDCRellam=0;
            ldedcu8EDCInitialCnt=0;
        }
        else
        {
				#if __VDC
					#if __REAR_D
    		tempW5 = ((vrad_crt_rl - wvref_rl)+(vrad_crt_rr - wvref_rr))/2;
    		tempW6 = ((int16_t)(RL.rel_lam)+(int16_t)(RR.rel_lam))/2;    		
    				#else
    		tempW5 = ((vrad_crt_fl - wvref_fl)+(vrad_crt_fr - wvref_fr))/2;
    		tempW6 = ((int16_t)(FL.rel_lam)+(int16_t)(FR.rel_lam))/2;
    				#endif
    	    	#else
					#if __REAR_D    	    	
    		tempW5 = ((vrad_crt_rl - vref)+(vrad_crt_rr - vref))/2;
    		tempW6 = ((int16_t)(RL.rel_lam)+(int16_t)(RR.rel_lam))/2;    		    		
    				#else
    		tempW5 = ((vrad_crt_fl - vref)+(vrad_crt_fr - vref))/2;
    		tempW6 = ((int16_t)(FL.rel_lam)+(int16_t)(FR.rel_lam))/2;    		
					#endif
    			#endif   	    
    		ldedcu8EDCInitialCnt = ldedcu8EDCInitialCnt+1;
    		if(ldedcu8EDCInitialCnt>10)
    		{
    			ldedcu8EDCInitialCnt=10;
    		}
    		else{}	
    		if(ldedcu8EDCInitialCnt==1)
    		{	
        		ldedcs16InitialEDCSlip=tempW5;
        		ldedcs16InitialEDCRellam=tempW6;
        	}
        	else{}
        	
        	if( ((ldedcu1DetectTempTire_1WL==1)||(ldedcu1DetectTempTire_2WL==1)) &&
        	    ((ldedcs16InitialEDCSlip>(-VREF_10_KPH))&&(ldedcs16InitialEDCRellam>(-LAM_30P))) )
        	{	
            	if( ((ldedcs16velgap-ldedcs16velgapatEDCentry) >= -VREF_1_5_KPH) &&
            	    ((ldedcs16velgap-ldedcs16velgapatEDCentry) <= VREF_1_5_KPH) &&
            	    ((ldedcs16velgap2-ldedcs16velgapatEDCentry2) >= -VREF_1_5_KPH) &&
            	    ((ldedcs16velgap2-ldedcs16velgapatEDCentry2) <= VREF_1_5_KPH)	)
                {
                	ldedcs16TempTireTendCntInCtl++;
                    if(ldedcs16TempTireTendCntInCtl > L_U16_TIME_10MSLOOP_3S)  	
                    {
                    	ldedcs16TempTireTendCntInCtl = L_U16_TIME_10MSLOOP_3S;
                    }	
                    else{}
                }    	
                else
                {	    
                    ldedcs16TempTireTendCntInCtl=ldedcs16TempTireTendCntInCtl-1; 
                    if(ldedcs16TempTireTendCntInCtl<=0)
                    {
                    	ldedcs16TempTireTendCntInCtl=0;
                    }	
                    else{}
            	}
            	
                if(ldedcs16TempTireTendCntInCtl >= L_U16_TIME_10MSLOOP_3S)	/* 3s */		
                {
                	ldedcu1TempTireSuspectInCtl=1;    
                }	
                else if(ldedcs16TempTireTendCntInCtl==0)  
                {
                	ldedcu1TempTireSuspectInCtl=0;
                }	
                else{}
        	}
        	else
        	{
                ldedcs16TempTireTendCntInCtl=ldedcs16TempTireTendCntInCtl-5; 
                if(ldedcs16TempTireTendCntInCtl<=0)
                {
                	ldedcs16TempTireTendCntInCtl=0;
                }	
                else{}
            }               			        
        }    	
	}
	else  /* Temp tire 감지 해제 조건 : temp교체시간 고려하여 정차중 3분이상이면 reset */
	{
			#if __VDC
    	if (vref5 <= VREF_2_5_KPH)
    		#else
    	if (vref <= VREF_2_5_KPH)
    		#endif	
    	{
    		if ( ldedcs16ResetTempTireDetectCnt <= L_U16_TIME_10MSLOOP_3MIN ) 	
    		{
    			ldedcs16ResetTempTireDetectCnt++;
    		}
    		else
    		{
    			ldedcu1TempTireSuspectInCtl=0;		
    			ldedcs16ResetTempTireDetectCnt=0;		
    		}
    	}
    	else
    	{
			;			
    	}
	}	    	
 
	if ( (ldedcu1TempTireSuspectInCtl==1)  
  		#if __RTA_ENABLE		
		 || ((RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_WL==1)||(RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_WL==1)) 
		#else
		 || ((FL.MINI_SPARE_WHEEL==1)||(FR.MINI_SPARE_WHEEL==1)||(RL.MINI_SPARE_WHEEL==1)||(RR.MINI_SPARE_WHEEL==1))
		#endif
		#if __RTA_2WHEEL_DCT 
		 || ( ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_FR.RTA_SUSPECT_2WHL==1))
		      || ((RTA_RL.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))
		      || ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_RL.RTA_SUSPECT_2WHL==1))
		      || ((RTA_FR.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))		      
		      || ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))		      		      
		      || ((RTA_FR.RTA_SUSPECT_2WHL==1)&&(RTA_RL.RTA_SUSPECT_2WHL==1))		      
		    )   
		#endif
	   )
    {
    	ldedcu1TempTireSuspect=1;    
    }
  	else
    {
    	ldedcu1TempTireSuspect=0;
    }    			    

}
	#endif			


    #if __EDC_EXIT_BY_EXCESSIVE_TORQ_UP
void LDEDC_vDetectExcessiveTorqUp(void)
{

	static int16_t lcedcs16EntranceSpeed, lcedcs16VehicleAccelSum;


  #if __4WD_VARIANT_CODE==ENABLE      
    if((lsu8DrvMode==DM_2WD)&&(lsu8DrvMode==DM_2H)) /* ABS_CBC is disable for 4WD vehicle */ 
  #endif  
    {
		if(EDC_ON==1)
		{
			if(lcedcu8ActCnt1S==0)
			{
				lcedcs16VehicleAccelSum = ebd_filt_grv;
				lcedcu1ExitByExcessiveTqUp = 0;
				lcedcu1TqDownByExcessiveTqUp = 0;
				if((vref >= lcedcs16EntranceSpeed) && (MSR_cal_torq > S16_EDC_EXIT_TQ_MIN))
				{
					if(lcedcs16VehicleAccelSum >= S16_EDC_EXIT_ACCEL_SUM)
					{
						lcedcu1ExitByExcessiveTqUp = 1;
					}
					else if(lcedcs16VehicleAccelSum >= S16_EDC_TQ_DOWN_ACCEL_SUM)
					{
						lcedcu1TqDownByExcessiveTqUp = 1;
					}
					else { }
				}	
				else { }
			}
			else
			{
				lcedcs16VehicleAccelSum = lcedcs16VehicleAccelSum + ebd_filt_grv;
			}
		}
		else
		{
			lcedcs16EntranceSpeed = vref;
			lcedcu1ExitByExcessiveTqUp = 0;
			lcedcu1TqDownByExcessiveTqUp = 0;
			lcedcs16VehicleAccelSum = 0;
		}
	}
  #if __4WD_VARIANT_CODE==ENABLE      
	else
	{
		lcedcs16EntranceSpeed = vref;
		lcedcu1ExitByExcessiveTqUp = 0;
		lcedcu1TqDownByExcessiveTqUp = 0;
		lcedcs16VehicleAccelSum = 0;
	}
  #endif  
}
	#endif	/* __EDC_EXIT_BY_EXCESSIVE_TORQ_UP */
#endif		/*TAG1*/
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDEDCCallDetection
	#include "Mdyn_autosar.h"
#endif    
/* AUTOSAR --------------------------*/

