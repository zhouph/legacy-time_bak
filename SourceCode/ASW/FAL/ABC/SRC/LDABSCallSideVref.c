/******************************************************************
*  Program ID: MGH-40 Side Vref										   
*  Program description:. Side Vref Calculation					  
*  Input files:				
*  Output files:	  
*					   
*											
*  Special notes: none												
*******************************************************************
*  Modification	Log												  
*  Date		   Author			Description						 
*  ---		   ------			---------						  
*  05.11.23	   Sewoong Kim		 Initial Release				   
*******************************************************************/
/* Includes *******************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallSideVref
	#include "Mdyn_autosar.h"
#endif

#include 	"LCABSDecideCtrlState.h"
#include 	"LDABSCallSideVref.h"
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#if	__VDC  
#include 	"LDABSCallVrefCompForESP.H"
#endif	   
#include 	"LCallMain.h"
#include	"LCESPCalInterpolation.h"
#include 	"LCABSCallControl.h"

/*******************************************************************/

/* Local Definition	 ***********************************************/
#if	__ENGINE_INFORMATION==1
#define	 __CBS_SIDE_WHEEL_SELECT_HIGH	1
#define	SIDE_VREF_CBC_MTP_THR			5
#else
#define	 __CBS_SIDE_WHEEL_SELECT_HIGH	0
#endif
/*******************************************************************/

/* Variables Definition*********************************************/
/*2004.11.09 for SIDE-WHEEL	REF	*/
#if	__SIDE_VREF
U8_BIT_STRUCT_t	SVREF00;	 
U8_BIT_STRUCT_t	SVREF01;	 
U8_BIT_STRUCT_t	SVREF02;	 
U8_BIT_STRUCT_t	SVREF03;	
#endif

#if	__SIDE_VREF	&& __VREF_AUX
	struct	U8_BIT_STRUCT_t VAUXF0;
#endif

	#if	__VREF_AUX
	int16_t		vref_aux_resol_change;	  /* 051206	KSW	*/
	int16_t		vref_aux_resol_change_old;/* 051206	KSW	*/	  
	int16_t		vref_aux;	 /*	0601 KSW */	   
	#endif
	

	#if	__SIDE_VREF
	int16_t		vref_L;						/* Side	Wheel Reference	04.11.11 */
	int16_t		vref_R;	
	int16_t		vref_L_diff; 
	int16_t		vref_R_diff;

	int16_t		vref_L_old;
	int16_t		vref_R_old;		   
	int16_t		voptfz_new_side;			/* Side	Wheel Reference	04.12.01 */
	int16_t		voptfz_new3;				/* Side	Wheel Reference	04.11.11 */
	int16_t		voptfz_new4;				/* Side	Wheel Reference	04.11.11 */	
	int16_t		voptfz_new_resol_change;	/* Side	Wheel Reference	04.11.19 */	  
	int16_t		dv_yaw_est;					/* Side	Wheel Reference	05.05.31 */
	int16_t		dv_yaw_est_filt;			/* Side	Wheel Reference	05.05.31 */	
	int16_t		dv_ay_est;					/* Side	Wheel Reference	05.06.02 */	
   
	#endif		 


/*******************************************************************/

/* Local Function prototype	****************************************/	 


void	LDABS_vCallMainSideVref(void);

	#if	__SIDE_VREF

void	LDABS_vDetSidevrefWheelSel(void);
void	LDABS_vDetSidevrefInputLimit(void);
void	LDABS_vCalSidevrefFilter(void);
void	LDABS_vEstSidevrefSelfLimit(void);
void	LDABS_vEstSidevrefCrossLimit(void);

		#if	__VREF_AUX
void	LDABS_vEstAuxMainVref(void);
void	LDABS_vDetVrefAuxFirstLimit(void);
void	LDABS_vCalVrefAuxSecondLimit(void);
void	LDABS_vDetVrefAuxAFZLimit(void);
		#endif

void	LDABS_vCalFilter_1Hz(void);
void	LDABS_vCalFilter_3Hz(void);

	#endif	
	
/*******************************************************************/	 

	
void	LDABS_vCallMainSideVref(void)
{	
	#if	__SIDE_VREF					  
		LDABS_vDetSidevrefWheelSel();


		FZ = (struct V_STRUCT *) &FZ3;		/* Motorola	*/
		LEFT_SIDE=1;
			LDABS_vDetSidevrefInputLimit();				
			LDABS_vCalSidevrefFilter();				/* filter LeftSide voptfz_resol_change3	*/
			LDABS_vEstSidevrefSelfLimit();

		FZ = (struct V_STRUCT *) &FZ4;		/* Motorola	*/
		LEFT_SIDE=0; 
			LDABS_vDetSidevrefInputLimit();				
			LDABS_vCalSidevrefFilter();				/* filter RightSide	voptfz_resol_change4 */
			LDABS_vEstSidevrefSelfLimit();		   
			LDABS_vEstSidevrefCrossLimit();			   

		#if	__VREF_AUX
			LDABS_vEstAuxMainVref();	  
		#endif
	#endif
}	
	

/*******************************************************************/ 

/* Side	Vref Function Resolution Change	-start*/
	#if	__SIDE_VREF	  
void LDABS_vDetSidevrefWheelSel(void)
{
	if(ABS_fz==1) 
	{
		#if	__REAR_D
		if(	FL.vrad_crt_resol_change >=	RL.vrad_crt_resol_change ) /* Left Side	*/
		{	 
		   vrselect3_resol_change=FL.vrad_crt_resol_change;
		   DRIVE_L =0; 
		}
		else
		{  
		   vrselect3_resol_change =	RL.vrad_crt_resol_change;
		   DRIVE_L =1; 
		}
		if(	FR.vrad_crt_resol_change >=	RR.vrad_crt_resol_change ) /* Right	Side */
		{	 
		   vrselect4_resol_change =FR.vrad_crt_resol_change;
		   DRIVE_R =0; 
		}
		else
		{  
		   vrselect4_resol_change =	RR.vrad_crt_resol_change;
		   DRIVE_R =1;			  
		}
		#else
		if(	FL.vrad_crt_resol_change >=	RL.vrad_crt_resol_change ) /* Left Side	*/
		{	 
		   vrselect3_resol_change =FL.vrad_crt_resol_change;
		   DRIVE_L =1; 
		}
		else
		{  
		   vrselect3_resol_change =	RL.vrad_crt_resol_change;
		   DRIVE_L =0; 
		}
		if(	FR.vrad_crt_resol_change >=	RR.vrad_crt_resol_change ) /* Right	Side */
		{	 
		   vrselect4_resol_change =FR.vrad_crt_resol_change;
		   DRIVE_R =1; 
		}
		else
		{  
		   vrselect4_resol_change =	RR.vrad_crt_resol_change;
		   DRIVE_R =0;			  
		}
		#endif	
	}
	else 
	{
	 #if __REAR_D
		#if	(__CBS_SIDE_WHEEL_SELECT_HIGH==1)
		if(	(BLS==1)&& (mtp<= SIDE_VREF_CBC_MTP_THR	) && (ldabsu1CanErrFlg==0)	)
		{
			if(FL.vrad_crt_resol_change	>= RL.vrad_crt_resol_change	)
			{
				vrselect3_resol_change=	FL.vrad_crt_resol_change;
			}
			else
			{
				vrselect3_resol_change=	RL.vrad_crt_resol_change;
			}
			
			if(FR.vrad_crt_resol_change	>= RR.vrad_crt_resol_change	)
			{
				vrselect4_resol_change=	FR.vrad_crt_resol_change;
			}
			else
			{
				vrselect4_resol_change=	RR.vrad_crt_resol_change;
			}									
		}
		else
		{
			vrselect3_resol_change=	FL.vrad_crt_resol_change;  /* LEFT_Reference */
			vrselect4_resol_change=	FR.vrad_crt_resol_change;  /* RIGHT	Reference */			
		}	

		#else
		vrselect3_resol_change=	FL.vrad_crt_resol_change;  /* LEFT_Reference */
		vrselect4_resol_change=	FR.vrad_crt_resol_change;  /* RIGHT	Reference */		
		#endif
		DRIVE_L=0;
		DRIVE_R=0;		  
		
	 #else	  /* FRT WHEEL DRIVE , LEFT_REF=vrselect3, RIGHT_REF=vrselect4 */
		#if	(__CBS_SIDE_WHEEL_SELECT_HIGH==1)
		if(	(BLS==1)&& (mtp<= SIDE_VREF_CBC_MTP_THR	) && (ldabsu1CanErrFlg==0) )
		{
			if(FL.vrad_crt_resol_change	>= RL.vrad_crt_resol_change	)
			{
				vrselect3_resol_change=	FL.vrad_crt_resol_change;
			}
			else
			{
				vrselect3_resol_change=	RL.vrad_crt_resol_change;
			}
			
			if(FR.vrad_crt_resol_change	>= RR.vrad_crt_resol_change	)
			{
				vrselect4_resol_change=	FR.vrad_crt_resol_change;
			}
			else
			{
				vrselect4_resol_change=	RR.vrad_crt_resol_change;
			}									
		}
		else
		{
			vrselect3_resol_change=	RL.vrad_crt_resol_change;  /* LEFT_Reference */
			vrselect4_resol_change=	RR.vrad_crt_resol_change;  /* RIGHT	Reference */			
		}	

		#else
		vrselect3_resol_change=	RL.vrad_crt_resol_change;
		vrselect4_resol_change=	RR.vrad_crt_resol_change;		 
		#endif
		DRIVE_L=0;
		DRIVE_R=0;
	 #endif				
	} 
}
 /*******************************************************************/ 
void  LDABS_vDetSidevrefInputLimit(void)
{	 
/*	  Store_FILTINP: */
	LDABS_vDetVrefInputLimit();
    LDABS_vExecuteVrefInputLimit(vref_input_inc_limit, vref_input_dec_limit, 5);  
}	

 /*******************************************************************/ 
void LDABS_vCalSidevrefFilter(void)
{
	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS) 
	{
		FZ->voptfz_mittel=FZ->voptfz=FZ->vrselect;
		FZ->voptfz_mittel_resol_change=FZ->voptfz_resol_change=tempW0=FZ->vrselect_resol_change;
		FZ->voptfz_rest=0;
		
	}
	else 
	{
		if(	LEFT_SIDE == 1 )
		{
			if ( OUT_LEFT ==1 )
			{
				tempW2=10  ;		  /* filter_out=7/8	* filter_out + filt_ein	*/
			}	   
			else
			{
				if(	FZ->vrselect_resol_change >FZ->voptfz_resol_change )
				{
					tempW2=9;			 /*	filter_out=6/8 * filter_out	+ 2*filt_ein */
				}	 
				else
				{
					tempW2=10;				/* filter_out=7/8 *	filter_out + filt_ein */
				}		   
			}
		}
		else
		{
			if ( OUT_LEFT ==0 )
			{
				tempW2=10  ;			   /* filter_out=7/8 * filter_out +	filt_ein */
			}	
			else
			{
				if(	FZ->vrselect_resol_change >FZ->voptfz_resol_change )
				{
					tempW2=9;			  /* filter_out=6/8	* filter_out + 2*filt_ein */
				}	 
				else
				{
					tempW2=10;				 /*	filter_out=7/8 * filter_out	+ filt_ein */
				}		   
			}	   
		} 
	    
	    tempW1=FZ->filter_out_resol_change;  
        tempW0=11;
      	FZ->filter_out_resol_change=(int16_t)(((int32_t)tempW1*tempW2+(int32_t)(tempW0-tempW2)*8*FZ->filt_inp)/tempW0);   		  
		FZ->filter_out=	 FZ->filter_out_resol_change/8;		
/*-----------------------------------------------------------------------------------
	 voptfz_rest=((filter_out /	2) + voptfz_rest) mod 256
------------------------------------------------------------------------------------*/
		tempW3=(((FZ->filter_out_resol_change)*3)/2 + FZ->voptfz_rest)/358;
		FZ->voptfz_mittel_resol_change+=tempW3;
		FZ->voptfz_rest= (FZ->filter_out_resol_change +	FZ->voptfz_rest) - (tempW3*358);
		FZ->voptfz_mittel=FZ->voptfz_mittel_resol_change/8;
/*-----------------------------------------------------------------------------------
		tempW0=voptfz_mittel+filter_out	/	8
------------------------------------------------------------------------------------*/		
		tempW0=FZ->voptfz_mittel_resol_change+(FZ->filter_out);
	}
	
	voptfz_new_side=LCABS_s16LimitMinMax(tempW0,VREF_MIN_RESOL_CHANGE,VREF_MAX_RESOL_CHANGE); 
}
 /*******************************************************************/ 
void LDABS_vEstSidevrefSelfLimit(void)
{
	int16_t		voptfz_temp;
	int8_t		inc_limit_f;
	int8_t		inc_limit_n;
	int8_t		dec_limit_f;
	int8_t		dec_limit_n;
	
	 
	voptfz_temp=voptfz_new_side;
		
/* Side	Vref velocity is always	below the reference	velocity(vref) '05.01.25  by K.S.W.	*/
	if(	vref_resol_change >= voptfz_temp )
	{	  /*05.10.19 By	K.S.W */
		voptfz_temp=voptfz_temp;
	}
	else
	{
		voptfz_temp=vref_resol_change;
	}	 
		
	if(	ABS_fz==0)
	{ 
		if(abs_begin_timer != 0)	
		{
			if(abmittel_fz >= (int16_t)L_U8_TIME_10MSLOOP_1400MS) 
			{
			  tempW1=ALIMIT_NEG_0_G5;
			}
			else
			{
			  tempW1=ALIMIT_NEG_1_G;
			}
		}
		else if(FZ->filter_out <= (int16_t)(-820))
		{
			tempW1=ALIMIT_NEG_1_G5;	
		}
		else
		{
			tempW1=ALIMIT_NEG_1_G;				   /* set maximum allowed decrement	of vehicle reference speed */		   
		}
		
		#if	__PARKING_BRAKE_OPERATION
		if(PARKING_BRAKE_OPERATION==1) 
		{
			tempW1=ALIMIT_NEG_0_G5;
		}
		else
		{
			;
		}
		#endif
		
		tempW1 += FZ->voptfz_resol_change;				/* limit decrement */
		if(tempW1 >	voptfz_temp)	
		{
			voptfz_temp=tempW1;						  
		}
		else
		{
			;
		}
	}
	else
	{
	 
		tempW1=FZ->voptfz_resol_change;
		if(tempW1 <	FZ->vrselect_resol_change) 
		{
			tempW1 += VREF_0_5_KPH_RESOL_CHANGE;
			if(tempW1 >	voptfz_temp)	
			{
				tempW1=voptfz_temp;
			}
			else
			{
				;
			}			
			voptfz_temp=tempW1;
			if(tempW1 >	FZ->vrselect_resol_change) 
			{
				voptfz_temp=FZ->vrselect_resol_change;
			}
			else
			{
				;
			}
		}
		else	
		{
			tempW1=v_afz<<3;
			#if	__REAR_D					   
			if(tempW1 <	FL.vrad_crt_resol_change) 
			{
				tempW1=FL.vrad_crt_resol_change;		
			}
			else
			{
				;
			}
			if(tempW1 <	FR.vrad_crt_resol_change) 
			{
				tempW1=FR.vrad_crt_resol_change;
			}
			else
			{
				;
			}
			#else
			if(tempW1 <	RL.vrad_crt_resol_change) 
			{
				tempW1=RL.vrad_crt_resol_change;
			}
			else
			{
				;
			}
			if(tempW1 <	RR.vrad_crt_resol_change) 
			{
				tempW1=RR.vrad_crt_resol_change;
			}	
			else
			{
				;
			}
			#endif		   
			tempW1 += VREF_5_KPH_RESOL_CHANGE;
	
			if(voptfz_temp >	tempW1)	
			{
				voptfz_temp=tempW1;	
			}
			else
			{
				;
			}
		}
	
		if(abs_begin_timer != 0)	
		{
			if(abmittel_fz >= (int16_t)L_U8_TIME_10MSLOOP_1400MS) 
			{
				tempW1=ALIMIT_NEG_0_G5;
			}
			else
			{
				tempW1=ALIMIT_NEG_1_G;
			}
		}
		else if(FZ->filter_out <= (int16_t)(-820)) 
		{
			tempW1=ALIMIT_NEG_1_G5;	
		}
		else	
		{
			tempW1=ALIMIT_NEG_1_G;				  /* set maximum allowed decrement of vehicle reference	speed */		  
		}
	
		#if	__PARKING_BRAKE_OPERATION
		if(PARKING_BRAKE_OPERATION==1) 
		{
			tempW1=ALIMIT_NEG_0_G5;
		}
		else
		{
			;
		}
		#endif
	
		tempW1 += FZ->voptfz_resol_change;				/* limit decrement */
		if(tempW1 >	voptfz_temp)	
		{
			voptfz_temp=tempW1;				   
		}
		else
		{
			;
		}		
	
		   /* ABS_fz==1	& afz_ok_sum==1	*/
		if ( LEFT_SIDE==1	)
		{
			WDRIVE=DRIVE_L;
		}
		else 
		{
			WDRIVE=DRIVE_R;	
		}		
	
		if ( AFZ_OK	==1	) 
		{	 
			if ( WDRIVE==1 )
			{			   /* Drive	Wheel  */
				if (afz	<= -(int16_t)U8_HIGH_MU)	
				{
					inc_limit_f=ALIMIT_POS_1_G5;
					inc_limit_n=ALIMIT_POS_1_G25;
					dec_limit_f=ALIMIT_NEG_1_G;
					dec_limit_n=ALIMIT_NEG_1_G25;	
				}
				else if	( afz >=-(int16_t)U8_LOW_MU	)
				{
					inc_limit_f=ALIMIT_POS_1_G;
					inc_limit_n=ALIMIT_POS_0_G5;
					dec_limit_f=ALIMIT_NEG_0_G4;
					dec_limit_n=ALIMIT_NEG_0_G25;		
				}
				else	
				{
					inc_limit_f=ALIMIT_POS_1_G25;
					inc_limit_n=ALIMIT_POS_1_G;
					dec_limit_f=ALIMIT_NEG_1_G;
					dec_limit_n=ALIMIT_NEG_1_G;	
				}	
			}	
			else 
			{						/* Driven Wheel	*/
				if (afz	<= -(int16_t)U8_HIGH_MU)	
				{
					inc_limit_f=ALIMIT_POS_1_G5;
					inc_limit_n=ALIMIT_POS_1_G5;
					dec_limit_f=ALIMIT_NEG_1_G;
					dec_limit_n=ALIMIT_NEG_1_G25;	
				}
				else if	( afz >=-(int16_t)U8_LOW_MU	)
				{
					inc_limit_f=ALIMIT_POS_1_G;
					inc_limit_n=ALIMIT_POS_0_G7;
					dec_limit_f=ALIMIT_NEG_0_G5;
					dec_limit_n=ALIMIT_NEG_0_G3;		
				}
				else	
				{
					inc_limit_f=ALIMIT_POS_1_G25;
					inc_limit_n=ALIMIT_POS_1_G;
					dec_limit_f=ALIMIT_NEG_1_G;
					dec_limit_n=ALIMIT_NEG_1_G;	
				}			
			}	
		}				   /* AFZ_OK==0	*/
		else
		{
			inc_limit_n=ALIMIT_POS_1_G5;
			dec_limit_n=ALIMIT_NEG_1_G25;		  
		
		} 
	
		if(voptfz_temp > (FZ->voptfz_resol_change +	inc_limit_n)) 
		{
			voptfz_temp=FZ->voptfz_resol_change+inc_limit_n	;
			LMINC=1;
			LMDEC=0;
			UP_SIDE=0;
			HOLD_SIDE=0;
		}
		else if(voptfz_temp	< (FZ->voptfz_resol_change + dec_limit_n) )	
		{
			voptfz_temp=FZ->voptfz_resol_change+dec_limit_n	;
			LMINC=0;
			LMDEC=1;
			UP_SIDE=0;
			HOLD_SIDE=0;				
		}
		else 
		{
			if(voptfz_temp > FZ->voptfz_resol_change)
			{
				LMINC=0;
				LMDEC=0;
				UP_SIDE=1;
				HOLD_SIDE=0;				
			}
			else if	( voptfz_temp <FZ->voptfz_resol_change)
			{
				LMINC=0;
				LMDEC=0;
				UP_SIDE=0;
				HOLD_SIDE=0;						
			}
			else
			{
				LMINC=0;
				LMDEC=0;
				UP_SIDE=0;
				HOLD_SIDE=1;					
			}	 
		}
	}
	   
/* Side	Vref velocity is always	below the reference	velocity(vref) '05.01.25  by K.S.W.		 */
	  
	if(	vref_resol_change >= voptfz_temp )
	{
		voptfz_temp=voptfz_temp;
	}
	else
	{
		voptfz_temp=vref_resol_change;
	}		  
			
	if(	LEFT_SIDE==1 )
	{				   /*Left Side*/
		voptfz_new3=voptfz_temp;
		LMINC_L=LMINC;
		LMDEC_L=LMDEC;
		UP_SIDE_L=UP_SIDE;
		HOLD_SIDE_L=HOLD_SIDE;					
	}
	else
	{								  /*Right Side */
		voptfz_new4=voptfz_temp;
		LMINC_R=LMINC;
		LMDEC_R=LMDEC;
		UP_SIDE_R=UP_SIDE;
		HOLD_SIDE_R=HOLD_SIDE;				 
	}	
}

 /*******************************************************************/ 
void LDABS_vEstSidevrefCrossLimit(void)
{

	int16_t	inside_alimit_unl;
	int16_t	inside_alimit_lim;	
	int16_t	inside_alimit_inc;	
	int16_t	inside_alimit_dec;
	int16_t	dv_yaw;				/* Side	Wheel Reference	05.05.31 */
	int16_t	dv_Kyaw;			/* Side	Wheel Reference	05.05.31 */
	int16_t	V_m;				/* Side	Wheel Reference	05.05.31 */
	int16_t	dv_yaw_den;			/* Side	Wheel Reference	05.05.31 */
	int16_t	vref_side_temp;		/* Side	Wheel Reference	05.06.01 */
	int16_t	voptfz_out;	 
	int16_t	voptfz_in;	  
	int16_t	voptfz_out_1;
	int16_t	voptfz_in_1; 
  
	if(	ABS_fz==1 )	
	{	
		inside_alimit_unl=ALIMIT_NEG_0_G2	;
		inside_alimit_lim=ALIMIT_NEG_0_G1	;  
		inside_alimit_inc=ALIMIT_POS_0_G1	;  
		inside_alimit_dec=ALIMIT_NEG_0_G1	;
	
		if(	voptfz_new3	>	voptfz_new4)
		{
			OUT_LEFT=1;
			voptfz_out=voptfz_new3;
			voptfz_in =voptfz_new4;
			voptfz_out_1=voptfz3_resol_change;
			voptfz_in_1= voptfz4_resol_change;
			LMInc_OUT=LMINC_L;
			LMDec_OUT=LMDEC_L;
			OUTSIDE_UP=UP_SIDE_L;	
			OUTSIDE_HOLD=HOLD_SIDE_L;
			INSIDE_UP=UP_SIDE_R;
			LMInc_IN=LMINC_R;
			LMDec_IN=LMDEC_R;	
		}
		else
		{
			OUT_LEFT=0;
			voptfz_out=voptfz_new4;
			voptfz_in =voptfz_new3;
			voptfz_out_1=voptfz4_resol_change;
			voptfz_in_1	=voptfz3_resol_change;		
			LMInc_OUT=LMINC_R;
			LMDec_OUT=LMDEC_R;
			OUTSIDE_UP=UP_SIDE_R;	
			OUTSIDE_HOLD=HOLD_SIDE_R;
			INSIDE_UP=UP_SIDE_L;
			LMInc_IN=LMINC_L;
			LMDec_IN=LMDEC_L;	  
		}
	
		if(	OUTSIDE_HOLD==1)
		{
			voptfz_in= voptfz_in;		
		}
		else
		{
			if(	LMInc_OUT==1 )
			{		   /* Inc Limited */
				if ( LMInc_IN ==1 )
				{		 /*case1 */
					voptfz_in=voptfz_in_1+ (voptfz_out-voptfz_out_1+inside_alimit_lim);	
				}
				else if	( LMDec_IN==1 )
				{	 /*case	2 */
					voptfz_in=voptfz_in_1;	  
				}
				else 
				{
					if(INSIDE_UP==1)
					{		   /*case 3	*/
						voptfz_in=voptfz_in;		
					}
					else
					{					   /*case 4	*/
						voptfz_in=voptfz_in_1;	 
					}	
				}	   
			}
			else if	(	LMDec_OUT==1 )
			{	  /* Dec Limited  */
				if ( LMInc_IN ==1 )
				{		/*case5	*/
					voptfz_in=voptfz_in;	
				}
				else if	( LMDec_IN==1 )
				{	/*case 6 */
					voptfz_in=voptfz_in_1+	(voptfz_out-voptfz_out_1+inside_alimit_lim);		   
				}
				else 
				{
					if(INSIDE_UP==1)
					{		   /*case 7	*/
						voptfz_in=voptfz_in;			
					}
					else
					{					   /*case 8	*/
						voptfz_in=voptfz_in;	
					}	
				}			 
			}
			else
			{						 /*	Unlimited */
				if ( OUTSIDE_UP==1)
				{
					if ( LMInc_IN ==1 )
					{		/*case9	*/
						voptfz_in=voptfz_in_1+ (voptfz_out-voptfz_out_1+inside_alimit_unl);					   
					}
					else if	( LMDec_IN==1 )
					{	/*case 10 */
						voptfz_in=voptfz_in_1;		 
					}
					else 
					{
						if(INSIDE_UP==1)
						{		 /*case	11 */
							if(	(voptfz_in-voptfz_in_1)>(voptfz_out-voptfz_out_1)+inside_alimit_inc	)
							{
								voptfz_in=voptfz_in_1+(voptfz_out-voptfz_out_1)+inside_alimit_inc; 
							}		
							else
							{
								voptfz_in=voptfz_in;
							}
						}
						else
						{					 /*case	12 */
							voptfz_in=voptfz_in_1;	 
						} 
					}		 
					
				}
				else
				{
					if ( LMInc_IN ==1 )
					{		/*case13 */
						voptfz_in=voptfz_in; 
					}
					else if	( LMDec_IN==1 )
					{	/*case 14 */
						voptfz_in=voptfz_in_1+ (voptfz_out-voptfz_out_1+inside_alimit_unl);
					}
					else
					{
						if(INSIDE_UP==1)
						{		 /*case	15 */
							voptfz_in=voptfz_in;		 
						}
						else
						{					 /*case	16 */
							if(	(voptfz_in-voptfz_in_1)<(voptfz_out-voptfz_out_1)+inside_alimit_dec	)
							{
								voptfz_in=voptfz_in_1+(voptfz_out-voptfz_out_1)+inside_alimit_dec; 
							}		
							else
							{
								voptfz_in=voptfz_in;
							}
					   
						} 
					}				
				}		  
			}	 
		}	/*OUTSIDE_HOLD==0 end */
	
		if (OUT_LEFT==1	)
		{
			voptfz3_resol_change=voptfz_out;
			voptfz4_resol_change=voptfz_in;		 
		}
		else
		{
			voptfz3_resol_change=voptfz_in;
			voptfz4_resol_change=voptfz_out;  
		}		  
		/*	 
		added '05.01.29	  China	S.W.Kim	
		voptfz3_resol_change=voptfz_new3;	  '05.10.19	K.S.W.
		voptfz4_resol_change=voptfz_new4;	 
		*/
	
	}
	else
	{		 /*	Out	of ABS */
		voptfz3_resol_change=voptfz_new3;
		voptfz4_resol_change=voptfz_new4;
	}
 
	vref_L_old=vref_L;	 /*	1/8	Resolution */
	vref_R_old=vref_R;	 /*	1/8	Resolution */
 
	vref_L=voptfz3_resol_change;
	vref_R=voptfz4_resol_change;	
 
	vref_L=(vref_L>>3);
	vref_R=(vref_R>>3);
 

 
 /*	Del	V limitation by	velocity	05.06.01 By	K.S.W. */
	if(	vref_R>vref_L )
	{
		if(vref_R< VREF_30_KPH )
		{
			if((vref_R-vref_L)>VREF_6_KPH)	
			{
				vref_L=vref_R-VREF_6_KPH;
			}
			else
			{
				;
			}
		}
		else if((vref_R	>=VREF_30_KPH)&& (vref_R < VREF_100_KPH))
		{
			/*vref_side_temp=LCESP_s16IInter3Point(vref_R,VREF_30_KPH,VREF_7_KPH,VREF_60_KPH,VREF_3_KPH,VREF_100_KPH,VREF_2_KPH	);*/
			vref_side_temp=LCESP_s16IInter4Point(vref_R,VREF_30_KPH,VREF_7_KPH,VREF_60_KPH,VREF_3_KPH,VREF_100_KPH,VREF_2_KPH,VREF_100_KPH,VREF_2_KPH);
			if((vref_R-vref_L)>vref_side_temp)	
			{
				vref_L=vref_R-vref_side_temp;
			}
			else
			{
				;
			}	 
		} 
		else
		{
			if(	(vref_R-vref_L)>VREF_2_KPH)
			{
				vref_L=vref_R-VREF_2_KPH;
			}
			else
			{
				;
			}
		}
	}
	else
	{
		if(vref_L< VREF_30_KPH )
		{
			if((vref_L-vref_R)>VREF_6_KPH)	
			{
				vref_R=vref_L-VREF_6_KPH;
			}
			else
			{
				;
			}
		}
		else if((vref_L	>=VREF_30_KPH)&& (vref_L < VREF_100_KPH))
		{
			/*vref_side_temp=LCESP_s16IInter3Point(vref_L,VREF_30_KPH,VREF_7_KPH,VREF_60_KPH,VREF_3_KPH,VREF_100_KPH,VREF_2_KPH	);*/
			vref_side_temp=LCESP_s16IInter4Point(vref_L,VREF_30_KPH,VREF_7_KPH,VREF_60_KPH,VREF_3_KPH,VREF_100_KPH,VREF_2_KPH,VREF_100_KPH,VREF_2_KPH	);
			if((vref_L-vref_R)>vref_side_temp)	
			{
				vref_R=vref_L-vref_side_temp;
			}
			else
			{
				;
			}
		} 
		else
		{
			if(	(vref_L-vref_R)>VREF_2_KPH)	 
			{
				vref_R=vref_L-VREF_2_KPH;
			}
			else
			{
				;
			}
		}
	}

 
	vref_L_diff=vref_L-vref_L_old;	  /* 1/8 Resolution	*/
	vref_R_diff=vref_R-vref_R_old;	  /* 1/8 Resolution	*/
 

/**********************************************************************/
 
	dv_yaw=	(int16_t)( (int32_t)((vref_R-vref_L)*127*2)	);
	V_m	= (vref_R+vref_L)>>4;	
	dv_Kyaw=4000;
	dv_yaw_den=	50*2+	(int16_t)(	( ((V_m/2)*(V_m/2))/250)*((McrAbs(dv_yaw_est))/2)  ) ;
	/* 
	tempW2=dv_yaw;
	tempW1=1;
	tempW0=dv_yaw_den;
	s16mul16div16();
	dv_yaw_est=tempW3;
	*/	
	dv_yaw_est=dv_yaw/dv_yaw_den;
	dv_ay_est=(V_m/2)*(dv_yaw_est/2);
}
/**********************************************************************/

#if	__VREF_AUX
void	LDABS_vEstAuxMainVref(void)
{	
	if(ABS_fz==1)
	{
		if(	(VrefAux_Limit_flag==1)	&& (VrefAux_Limit_flag_K==0) )
		{
			LDABS_vDetVrefAuxFirstLimit();
		}
		else
		{
			if(AFZ_OK==1)
			{
				VrefAux_Limit_flag=0;
				VrefAux_Limit_flag_K=0;
/*				  vref_aux_resol_change=vref_resol_change;	 */
				LDABS_vDetVrefAuxAFZLimit();
			}
			else
			{
				if(abmittel_fz >= (int16_t)L_U8_TIME_10MSLOOP_560MS) 
				{
					VrefAux_Limit_flag=1;
					VrefAux_Limit_flag_K=0;
					vref_aux_resol_change=vref_resol_change-ALIMIT_NEG_0_G5;
				}
				else if(VrefAux_Limit_flag_K==1)
				{
					VrefAux_Limit_flag=0;
					VrefAux_Limit_flag_K=1;
					LDABS_vCalVrefAuxSecondLimit();					   
				}
				else
				{
					VrefAux_Limit_flag=0;
					VrefAux_Limit_flag_K=0;						   
					vref_aux_resol_change=vref_resol_change;
				}

			}		 
		}		
	}
	else
	{
		VrefAux_Limit_flag=0;
		VrefAux_Limit_flag_K=0;
		vref_aux_resol_change=vref_resol_change;			
	}	   
	vref_aux=vref_aux_resol_change>>3;
	vref_aux_resol_change_old=vref_aux_resol_change;	
}
/*========================================================================*/

void	LDABS_vDetVrefAuxFirstLimit(void)
{
	if(vref_aux_resol_change_old>vref_resol_change)
	{
		if(vref_aux_resol_change_old>vrselect1_resol_change)
		{
			tempW0=vref_resol_change;
			tempW1=vref_aux_resol_change_old;
			LDABS_vCalFilter_1Hz();
			vref_aux_resol_change=tempW2;
		}
		else
		{
			tempW0=vrselect1_resol_change;
			tempW1=vref_aux_resol_change_old;
			LDABS_vCalFilter_3Hz();
			vref_aux_resol_change=tempW2;
			if(vref_aux_resol_change>(vref_aux_resol_change_old+ALIMIT_POS_0_G5))
			{
				vref_aux_resol_change=vref_aux_resol_change_old+ALIMIT_POS_0_G5;			   /* +0.5g	���� */
			}
			else if( vref_aux_resol_change<(vref_aux_resol_change_old+ALIMIT_NEG_1_G))
			{
				vref_aux_resol_change=vref_aux_resol_change_old+ALIMIT_NEG_1_G;				   /* -1g ���� */
			}
			else
			{
				;
			}		  
		}		 
	}
	else
	{
		vref_aux_resol_change=vref_resol_change;
	}
	
	if(AFZ_OK==1)
	{
		VrefAux_Limit_flag=0;
		VrefAux_Limit_flag_K=1;	  
		vref_aux_resol_change=vref_resol_change;  /* Think more!! */ 
	}
	else
	{
		if(vref_aux_resol_change<vrselect1_resol_change)
		{
			VrefAux_Limit_flag=0;
			VrefAux_Limit_flag_K=1;		
		}
		else
		{
			VrefAux_Limit_flag=1;
			VrefAux_Limit_flag_K=0;
		}	
	}	 
}
/*========================================================================*/
void	LDABS_vCalVrefAuxSecondLimit(void)
{
	tempW0=vrselect1_resol_change;
	tempW1=vref_aux_resol_change_old;
	LDABS_vCalFilter_3Hz();
	vref_aux_resol_change=tempW2;
	
}
/*========================================================================*/

void	LDABS_vDetVrefAuxAFZLimit(void)
{
	int16_t	AFZLIMIT;
	/*	  
	tempW2=afz;
	tempW1=16;
	tempW0=100;
	s16muls16divs16();
	AFZLIMIT=tempW3;
	*/
	AFZLIMIT=(afz*8)/70;
	if(afz<-((int16_t)U8_HIGH_MU))
	{
		AFZLIMIT=AFZLIMIT+ALIMIT_NEG_0_G3; 
	}
	else if(afz>-((int16_t)U8_LOW_MU))
	{
		AFZLIMIT=AFZLIMIT+ALIMIT_NEG_0_G1;
	}
	else
	{
		AFZLIMIT=AFZLIMIT+ALIMIT_NEG_0_G2;
	}
	
	vref_aux_resol_change=vref_aux_resol_change_old+AFZLIMIT;
	
	if(AFZ_OK_K==1)
	{
		if(afz <= -((int16_t)U8_HIGH_MU))
		{
			vref_aux_resol_change=vref_resol_change;
		}
		else
		{
			;
		}	 
	}
	else
	{
		if((afz<=-((int16_t)U8_HIGH_MU)	)&&( afz_alt>-((int16_t)U8_HIGH_MU)	)&&(afz<=(afz_alt-(int16_t)U8_LOW_MU ))	 )
		{
			vref_aux_resol_change=vref_resol_change;
		}
		else
		{
			if(vref_aux_resol_change>vref_resol_change)
			{
				if(vref_aux_resol_change<vrselect1_resol_change)
				{
					tempW0=vrselect1_resol_change;
					tempW1=vref_aux_resol_change_old;
					LDABS_vCalFilter_3Hz();
					vref_aux_resol_change=tempW2;													  
				}
				else
				{
					;
				}
				
			}
			else
			{
				vref_aux_resol_change=vref_resol_change;
			}		 
		}		  
	}	 
}
#endif /*__VREF_AUX*/

/*========================================================================*/
/*========================================================================*/
/*
  tempW0 = input; 
  tempW1 = y_old; 
*/

void LDABS_vCalFilter_1Hz(void)
{
	tempW2 = (int16_t)(((uint32_t)123*tempW1+(uint32_t)5*tempW0)/(uint32_t)128); /*	cut-off	frequency 1Hz */
}

void LDABS_vCalFilter_3Hz(void)
{
	tempW2 = (int16_t)(((uint32_t)111*tempW1+(uint32_t)17*tempW0)/(uint32_t)128); /* cut-off frequency 3Hz */
}

	#endif	/*__SIDE_VREF*/
 /*******************************************************************/ 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallSideVref
	#include "Mdyn_autosar.h"
#endif
