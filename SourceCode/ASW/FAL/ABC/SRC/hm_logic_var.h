#ifndef __HM_LOGIC_VAR__
#define __HM_LOGIC_VAR__

#include "Abc_Ctrl.h"
#include "../CAL/VariantName/LTunePar.h"

/******
//typedef  signed    int8_t   int8_t; 
typedef  signed     int16_t   int16_t;
typedef  signed     int32_t  int32_t;       
//typedef  unsigned   int8_t  uint8_t;
typedef  unsigned   int16_t   uint16_t;
typedef  unsigned   int32_t  uint32_t;
*******/

/****
typedef  float            float16_t;
typedef  double           float32_t;
****/

typedef struct
{
	uint16_t u1bit0	:1;
	uint16_t u1bit1 :1;
	uint16_t u1bit2 :1;
	uint16_t u1bit3 :1;
	uint16_t u1bit4 :1;
	uint16_t u1bit5 :1;
	uint16_t u1bit6 :1;
	uint16_t u1bit7 :1;
}HM_BIT_t;

extern HM_BIT_t HmCanFlg0,HmCanFlg1,HmCanFlg2,HmCanFlg3,HmCanFlg4,HmCanFlg5,HmCanFlg6,HmCanFlg7, HmCanFlg8;



#define	lespu1EMS_F_N_ENG 			HmCanFlg0.u1bit0
#define lespu1EMS_F_SUB_TQI         HmCanFlg0.u1bit1
#define lespu1EMS_AT_TCU            HmCanFlg0.u1bit2
#define lespu1EMS_MT_TCU            HmCanFlg0.u1bit3
#define lespu1EMS_CVT_TCU           HmCanFlg0.u1bit4
#define lespu1EMS_TEMP_ENG_ERR      HmCanFlg0.u1bit5
#define lespu1EMS_PV_AV_CAN_ERR     HmCanFlg0.u1bit6
#define lespu1TCU_SWI_GS            HmCanFlg0.u1bit7

#define lespu1TCU_G_SEL_DISP_ERR	HmCanFlg1.u1bit0
#define lespu1TCU_TEMP_AT_ERR       HmCanFlg1.u1bit1
#define lespu1TCU_N_TC_ERR          HmCanFlg1.u1bit2
#define lespu1TCS_TCS_REQ           HmCanFlg1.u1bit3
#define lespu1TCS_MSR_C_REQ         HmCanFlg1.u1bit4
#define lespu1TCS_TCS_GSC           HmCanFlg1.u1bit5
#define lespu1TCS_TCS_CTL           HmCanFlg1.u1bit6
#define lespu1TCS_ABS_ACT           HmCanFlg1.u1bit7
	
#define lespu1EMS_TPS_ERR			HmCanFlg2.u1bit0
#define lespu1TCS_ESP_CTL			HmCanFlg2.u1bit1
#define lespu1TCS_TCS_MFRN			HmCanFlg2.u1bit2  
#define wpu1EspOnSwitch				HmCanFlg2.u1bit3
#define lespu1_TopTrvlCltchSwAct    HmCanFlg2.u1bit4
#define lespu1_TopTrvlCltchSwActV   HmCanFlg2.u1bit5
#define lespu1_TransEstGearV        HmCanFlg2.u1bit6
#define lespu1_TrnsShftLvrPosV      HmCanFlg2.u1bit7

#define lespu1ESP_ESP_INFO_BL 		HmCanFlg3.u1bit0
#define lespu1MS_M_ESP_E			HmCanFlg3.u1bit1

	#if (GMLAN_ENABLE==ENABLE) || __CAR==GM_C100 || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==PSA_C4) || (__CAR==PSA_C3) || (__CAR==PSA_C4_MECU) || __CAR==GM_CTS_MECU || __CAR==GM_TAHOE || __CAR==GM_J300||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)|| (__CAR ==GM_SILVERADO) || (__CAR==GM_T300)
	
#define lespu1_TCCDGCDecayCtrlEn 		HmCanFlg3.u1bit0
#define lespu1_TCCRCplReqAct		  	HmCanFlg3.u1bit1
#define lespu1_TTDGCDcyCntrlEnbld		HmCanFlg3.u1bit2
#define lespu1_WhlSensRghRdMagV			HmCanFlg3.u1bit3
#define lespu1_TracCntrlSysDrIntnt  	HmCanFlg3.u1bit4
#define lespu1_TracTrqDecCntAtv			HmCanFlg3.u1bit5
#define lespu1_TrnsBrkSysCltchRelRqd	HmCanFlg3.u1bit6  
#define lespu1_VehDynOvrUndrStrV        HmCanFlg3.u1bit7

		#if (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU) || (__CAR==PSA_C3)
#define lespu1_ReverseGearPosition		HmCanFlg4.u1bit0
#define lespu1_ASR_MSR_Acknowledge		HmCanFlg4.u1bit1
#define lespu1_ASR_ESP_Disconnection	HmCanFlg4.u1bit2
#define lespu1_BrakePedalSwitch			HmCanFlg4.u1bit3
#define lespu1_ReuglABS					HmCanFlg4.u1bit4
#define lespu1_RegulPBA					HmCanFlg4.u1bit5
#define lespu1_RegulASR					HmCanFlg4.u1bit6
#define lespu1_RegulBASR				HmCanFlg4.u1bit7

#define lespu1_RegulESP					HmCanFlg5.u1bit0
#define lespu1_RegulESPSeul				HmCanFlg5.u1bit1
#define lespu1_RegulEDC					HmCanFlg5.u1bit2
#define lespu1_RegulEBD					HmCanFlg5.u1bit3
#define lespu1_FunctionLamp				HmCanFlg5.u1bit4
#define lespu1HlStrAssActIO				HmCanFlg5.u1bit5
#define lespu1EngManfldAbsPrsV			HmCanFlg5.u1bit6
#define lespu1_CAN_FLAG_RESERVED_5_7	HmCanFlg5.u1bit7
		#else
#define lespu1EngRunAtv	                HmCanFlg4.u1bit0
#define lespu1BaroPressAbsV				HmCanFlg4.u1bit1
#define lespu1EngManfldAbsPrsV			HmCanFlg4.u1bit2
#define lespu1CATLightOff				HmCanFlg4.u1bit3
#define lespu1TSP_TrlrHtchSwAtvValid	HmCanFlg4.u1bit4
#define lcanu1WSWprAct					HmCanFlg4.u1bit5
#define lcescu1SCC_Ctrl_Act_Flg			HmCanFlg4.u1bit6
#define lespu1_CAN_FLAG_RESERVED_4_7	HmCanFlg4.u1bit7

#define lespu1ABSAct           			HmCanFlg5.u1bit0
#define lespu1_BrakePadWornIndicationOn	HmCanFlg5.u1bit1
#define lespu1_ACCBrakingActive		  	HmCanFlg5.u1bit2
#define lespu1_AutomaticBrkngFailed		HmCanFlg5.u1bit3
#define lespu1_IBAPrefillRequest		HmCanFlg5.u1bit4
#define lespu1HlStrAssActIO				HmCanFlg5.u1bit5
#define lcanu1HDC_HillDesCtrlMdSwAct	HmCanFlg5.u1bit6
#define lcanu1HDC_HillDesCtrlMdSwActV	HmCanFlg5.u1bit7
		#endif		

#define lespu1_EngOffTmExtRngV			HmCanFlg6.u1bit0
#define lespu1TirePressureValidityFL	HmCanFlg6.u1bit1	/* TPMS */
#define lespu1TirePressureValidityFR	HmCanFlg6.u1bit2	/* TPMS */
#define lespu1TirePressureValidityRL 	HmCanFlg6.u1bit3 	/* TPMS */
#define lespu1TirePressureValidityRR  HmCanFlg6.u1bit4	/* TPMS */
#define lespu1_CAN_FLAG_RESERVED_6_5    HmCanFlg6.u1bit5 
#define lespu1_CAN_FLAG_RESERVED_6_6    HmCanFlg6.u1bit6 
#define lespu1_CAN_FLAG_RESERVED_6_7    HmCanFlg6.u1bit7 
        
	#elif defined(CHERY_CAN)
		#if __CAR==CHINA_B12
#define lespu1ECM_EngineSts				HmCanFlg3.u1bit0
#define lespu1ECM_TQI_MAX_ValidData		HmCanFlg3.u1bit1
#define lespu1ECM_FeedbackASRVDCReq		HmCanFlg3.u1bit2
#define lespu1ECM_TQI_ValidData			HmCanFlg3.u1bit3
#define lespu1ECM_ClutchPedalAction     HmCanFlg3.u1bit4
#define lespu1TCM_AG_ratio_ERR			HmCanFlg3.u1bit5
#define lespu1BCM_ReverseGearSwitchSts  HmCanFlg3.u1bit6		
		#elif __CAR==CHINA_A13
#define lespu1FBCM_R_GEAR_SW  			HmCanFlg3.u1bit0				
		#elif __CAR==CHINA_P11
#define lespu1EMS_CLUTCH_PDL_STATUS		HmCanFlg3.u1bit0
#define lespu1EMS_NORMAL_OPERATION		HmCanFlg3.u1bit1
#define lespu1EMS_CLUTCH_PDL_STATUS_ERR	HmCanFlg3.u1bit2
#define lespu1EMS_FULL_ACCEL_SW			HmCanFlg3.u1bit3
#define lespu1FBCM_R_GEAR_SW			HmCanFlg3.u1bit4
#define lespu1ICM_BRAKE_FLUID_LEVEL		HmCanFlg3.u1bit5
		#endif

	#elif defined(SSANGYONG_CAN)
/* For SYC C200 */
  /* Transmitter signals */  
#define lespu1TCS_BRE_AKT_ESP		HmCanFlg4.u1bit1
#define lespu1ESP_BLS				HmCanFlg4.u1bit2
#define lespu1ESP_AMR_AKT_ESP		HmCanFlg4.u1bit3
#define lespu1MS_PARK_BRK_STATUS 	HmCanFlg4.u1bit4 
#define lespu1MS_VEH_CODE_EPS  		HmCanFlg4.u1bit5

#if NEW_FM_ENABLE==ENABLE  
#define lespu1TCU_MT_GEAR_POSITIONInv	HmCanFlg4.u1bit6 
#else
#define HmCanFlg46        			HmCanFlg4.u1bit6 
#endif

#define HmCanFlg47          		HmCanFlg4.u1bit7 
	#else 
#define lcanu1HDC_HillDesCtrlMdSwAct	HmCanFlg3.u1bit2
#define lcanu1HDC_HillDesCtrlMdSwActV	HmCanFlg3.u1bit3
#define lespu1TSP_TrlrHtchSwAtvValid	HmCanFlg3.u1bit4

#define lespu1CF_Clu_WiperValidFlg	HmCanFlg4.u1bit0
#define lespu1CF_Clu_WiperIntSW		HmCanFlg4.u1bit1
#define lespu1CF_Clu_WiperLow	    HmCanFlg4.u1bit2
#define lespu1CF_Clu_WiperHigh		HmCanFlg4.u1bit3
#define lu1CF_Clu_WiperInvalidFlg 	HmCanFlg4.u1bit4 
#define lespu1CF_Clu_WiperAuto  	HmCanFlg4.u1bit5
#define lcanu1CF_Ems_BrkReq    		HmCanFlg4.u1bit6 
#define lcanu1CF_Esc_BrkCtl    		HmCanFlg4.u1bit7	
	#endif	

	#if defined(SSANGYONG_CAN)
  /* Receiver signals */
extern uint8_t lespu8MS_VEH_CODE_ENG;
extern uint8_t lespu8MS_VEH_CODE_TC;
extern uint8_t lespu8MS_VEH_SUS_TYPE;
extern uint8_t lespu8TCU_CUR_GC;
extern uint8_t lespu8TCU_MT_GEAR_POSITION;
extern int16_t	lesps16EMS_TQFR_C200_DSL;
extern int16_t	lesps16EMS_TQFR_C200_GSL;
  /* Transmitter signals */
extern uint8_t lespu8ESP_GMINMAX_ART;
extern uint8_t	lespu8TCS_EBS_STATUS;
#endif
	
    #if __HMC_CAN_VER==CAN_HD_HEV
#define lespu1ARB_CasVlvACmdC			HmCanFlg4.u1bit0
#define lespu1ARB_CasVlvBCmdC			HmCanFlg4.u1bit1
#define lespu1ARB_HevErr				HmCanFlg4.u1bit2
#define lespu1ARB_Act					HmCanFlg4.u1bit3
#define lespu1ARB_CFHcuBrkOn			HmCanFlg4.u1bit4
#define lespu1ARB_CFHcuBrkFlt			HmCanFlg4.u1bit5
    #endif 		
	
#if __HMC_CAN_VER==CAN_YF_HEV || __HMC_CAN_VER==CAN_HE_KE || __HMC_CAN_VER==CAN_HD_HEV	
/* Variables and Flags For ESC	 */
extern int16_t lesps16EMS_ActIndTq_Pc;
extern uint8_t lespu8HCU_EngCltStat;
extern int16_t lesps16EMS_IndTq_Pc;
extern int16_t lesps16HCU_EngTqCmdBInv_Pc;
extern int16_t lesps16MCU_CR_Mot_EstTq_Pc;
	#if __HMC_CAN_VER==CAN_YF_HEV || __HMC_CAN_VER==CAN_HD_HEV	
/* Variables and Flags For ESC	 */


extern int16_t lesps16HCU_MotTqCmdBInv_Pc;

extern int16_t lesps16TCU_CR_Tcu_TqRedReq_Pc;
extern int16_t lesps16TCU_CR_Tcu_TqRedReqSlw_Pc;
extern int16_t lesps16TCU_CR_Tcu_TqIncReq_Pc;

#define lespu1TCU_CF_Tcu_Flt      HmCanFlg5.u1bit0
#define HmCanFlg51		HmCanFlg5.u1bit1
#define HmCanFlg52		HmCanFlg5.u1bit2
#define HmCanFlg53		HmCanFlg5.u1bit3
#define HmCanFlg54		HmCanFlg5.u1bit4
	#endif
	
	#if __HMC_CAN_VER==CAN_HE_KE
extern int16_t lesps16HCU_Mot1TqCmdBInv_Pc;		
extern int16_t lesps16HCU_Mot2TqCmdBInv_Pc;		
	#endif
#endif	

		#if (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU)
/* For Jeep Compass */
  /* Receiver signals */
#define lespu1MS_ENG_RPM_PAR			HmCanFlg4.u1bit0
#define lespu1MS_ENG_RPM_TOG			HmCanFlg4.u1bit1
#define lespu1MS_M_STA_PAR			    HmCanFlg4.u1bit2  
#define lespu1MS_M_STA_TOG				HmCanFlg4.u1bit3
#define lespu1MS_PDL_POS_PAR    		HmCanFlg4.u1bit4
#define lespu1MS_PDL_POS_TOG   			HmCanFlg4.u1bit5
#define lespu1PARK_LMP_ON        		HmCanFlg4.u1bit6
  /* Transmitter signals */ 
#define lespu1BS_ESP_DSBL      			HmCanFlg4.u1bit7
#define lespu1BS_ABS_BrkEvt				HmCanFlg5.u1bit0
#define lespu1BS_BA_ACTIVE				HmCanFlg5.u1bit1
#define lespu1BS_BRK_ACTIVE			    HmCanFlg5.u1bit2  
#define lespu1BS_FullBrk_Actv1			HmCanFlg5.u1bit3
#define lespu1BS_TM_AUS					HmCanFlg5.u1bit4

  /* Receiver signals */
extern uint8_t lespu8CVT_TRANS_RATIO;
extern uint16_t lespu16MS_OutputSpd308h;
extern uint16_t lespu16BS_REF_VEH_SPEED;
extern uint8_t lespu8VG_TCASE_STAT;
extern uint8_t lespu8DSS_FT_DIFF_MMI_ST;
extern uint8_t lespu8DSS_CNT_DIFF_MMI_ST;
extern uint8_t lespu8DSS_R_DIFF_MMI_ST;
extern uint8_t lespu8DSS_FT_DIFF_MOD;
extern uint8_t lespu8DSS_CNT_DIFF_MOD;
extern uint8_t lespu8DSS_R_DIFF_MOD;
extern uint8_t lespu8DSS_ACT_FT_DIF_TRQ;
extern uint8_t lespu8DSS_ACT_CNT_DIF_TRQ;
extern uint8_t lespu8DSS_ACT_R_DIF_TRQ;
extern uint8_t lespu8DSS_DES_FT_DIF_TRQ;
extern uint8_t lespu8DSS_DES_CNT_DIF_TRQ;
extern uint8_t lespu8DSS_DES_R_DIF_TRQ;
  /* Transmitter signals */  
extern uint8_t lespu8BS_HILL_DES_STAT;
extern uint8_t lespu8BS_ESP_BRK_SW;
extern uint8_t lespu8ESP_FT_DIFF_ESP_RQ;
extern uint8_t lespu8ESP_R_F_ESP_CONT;
extern uint8_t lespu8ESP_CENT_DIFF_ESP_RQ;
extern uint8_t lespu8ESP_L_F_ESP_CONT;
extern uint8_t lespu8ESP_RR_DIFF_ESP_RQ;
extern uint8_t lespu8ESP_R_R_ESP_CONT;
extern uint8_t lespu8ESP_CODING_TABLE;
extern uint8_t lespu8ESP_L_R_ESP_CONT;
extern uint8_t lespu8ESP_FT_DIFF_TRQ_RQ;
extern uint8_t lespu8ESP_CENT_DIFF_TRQ_RQ;
extern uint8_t lespu8ESP_RR_DIFF_TRQ_RQ;		
extern uint8_t lespu8BS_SLV_ESP;
		#endif /* (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU) */
		
		#if __CAR == RSM_QM5 || __CAR == RSM_QM5_MECU 	
#define lespu1TCS_ENG_CTL_MAP			HmCanFlg5.u1bit0
#define lespu1TCS_EBD_CTL				HmCanFlg5.u1bit1	
#define lespu1TCS_MSR_CTL				HmCanFlg5.u1bit2		
extern uint8_t		lespu8ESP_SHIFT_AUTHOR;
extern uint8_t		lespu8ESP_EARLY_GSUP;
extern uint16_t 	lespu16EMS_N;
extern uint8_t 	lespu8TCU_CUR_GC;
		#endif /* __CAR == RSM_QM5 || __CAR == RSM_QM5_MECU */
			
#if __CAR == VV_V40
#define lespu1TCS_ETCS_CTL				HmCanFlg5.u1bit0
#define lespu1TCS_BTCS_CTL				HmCanFlg5.u1bit1
#define lespu1DSR_SPLIT_CTL				HmCanFlg5.u1bit2
extern uint8_t		lespu8BCM_TEXT;
extern int16_t		lesps16TCU_CUR_G_RATIO;
extern int16_t		lcans16CSTBATS_TrqVl;
#endif /* __CAR == VV_V40 */

#if __CAR == FIAT_PUNTO
#define lespu1TCS_INHIBIT_ACT			HmCanFlg5.u1bit0
#define lespu1VDC_ACT					HmCanFlg5.u1bit1
#define lespu1_CAN_FLAG_RESERVED_5_2	HmCanFlg5.u1bit2
#endif

			
#if defined(CHERY_CAN)
/* For B12 */
extern uint16_t lespu16TCM_AG_ratio;
extern uint16_t	lespu16TCM_N_Wheel;
extern uint8_t	lespu8TCM_TC_Status;
/* For B12 */

/* For A13 */
extern uint8_t lespu8FBCM_ENG_TYPE;
extern uint16_t lespu16FBCM_TyreRollingRadius;
/* For A13 */
#endif

extern int16_t ldesps16_gsuv_os_us_sig;

extern int16_t lesps16EMS_TQI_ACOR;
extern uint16_t lesps16EMS_N;
extern int16_t lesps16EMS_TQI;
extern int16_t lesps16EMS_DRIVE_TQI;
extern int16_t lesps16EMS_TQFR;
extern int16_t lesps16EMS_CAN_VERS;
extern int16_t lesps16EMS_TQ_STND;
extern int16_t lesps16EMS_TEMP_ENG;
extern int16_t lesps16TCS_TQI_TCS;       
extern int16_t lesps16TCS_TQI_MSR;       
extern int16_t lesps16TCS_TQI_SLOW_TCS;
extern int16_t lesps16TCU_TEMP_AT;    
extern int16_t lespu16TCU_N_TC;  
extern int16_t lesps16_TCU_REQ_TORQ;    
extern int16_t  lesps16TCS_TQI_ESP;
extern uint8_t lespu8ESP_GMIN_ESP;
extern uint8_t lespu8ESP_GMAX_ESP; 
extern int16_t lesps16EMS_TPS;           
extern int16_t lesps16EMS_TPS_Resol1000;           
extern int16_t lesps16EMS_PV_AV_CAN; 
extern int16_t lesps16EMS_PV_AV_CAN_Resol1000;

extern uint8_t lespu8EMS_CONF_TCU    ;   
extern uint8_t lespu8EMS_OBD_FRF_ACK;
extern uint8_t lespu8EMS_ENG_CHR;
extern uint8_t lespu8EMS_ENG_VOL;
extern uint8_t lespu8TCU_TAR_GC;
extern uint8_t lespu8TCU_G_SEL_DISP;   

extern uint8_t ee_tmp_fl;
extern uint8_t ee_tmp_fr;
extern uint8_t ee_tmp_rl;
extern uint8_t ee_tmp_rr;

extern uint8_t lespu8TCS_4WD_OPEN ;            
extern uint8_t  lespu1TCS_4WD_LIM_REQ;          
extern uint16_t lespu16TCS_4WD_TQC_LIM;         
extern uint8_t  lespu8TCS_4WD_CLU_LIM;          
extern uint8_t  lespu1TCS_4WD_LIM_MODE;         
extern uint8_t  lespu8TCU_F_TCU;                
extern uint8_t  lespu1TCU_TCU_TYPE_AT;          
extern uint8_t  lespu1TCU_TCU_TYPE_CVT;         

extern uint8_t  lespu8TCU_GEAR_TYPE;            

extern uint8_t lespu8AWD_4WD_TYPE;             
extern uint8_t lespu8AWD_4WD_SUPPORT;          
extern uint8_t lespu1AWD_4WD_ERR;              
extern uint8_t lespu8AWD_CLU_DUTY;             
extern uint8_t lespu8AWD_R_TIRE;               

extern uint8_t lespu1AWD_2H_ACT;               
extern uint8_t lespu1AWD_4H_ACT;               
extern uint8_t lespu1AWD_LOW_ACT;              
extern uint8_t lespu1AWD_AUTO_ACT;             
extern uint8_t lespu1AWD_LOCK_ACT;             
extern uint16_t lespu16AWD_4WD_TQC_CUR; 
                                 
extern uint8_t lespu8RDCM_SecAxlStat; 
extern uint8_t lespu8RDCM_SAMDVLReqVal;  
                                 
extern uint8_t lespu1EMS_CLU_ACK;      
extern uint8_t lespu8EMS_TQ_COR_STAT;  

extern uint8_t lespu8TCS_WHEEL;
extern uint8_t lespu8TCS_WHEEL_FL_TCS;
extern uint8_t lespu8TCS_WHEEL_FR_TCS;
extern uint8_t lespu8TCS_WHEEL_RL_TCS;
extern uint8_t lespu8TCS_WHEEL_RR_TCS;
 
extern uint8_t lespu8AWD_EMS2Q;
extern uint8_t lespu8AWD_ITM_SW;
extern uint8_t lespu8AWD_ITM_HW;

extern int16_t lesps16EMS6_TQI_MAX;
extern int16_t lesps16EMS6_TQI_TARGET;
extern int16_t lesps16EMS6_TQI;
extern int16_t lesps16EMS6_TQI_MIN;
        
extern uint8_t lespu8EMS_EmsIsgStat;
        
	#if (GMLAN_ENABLE==ENABLE) || __CAR==GM_C100 || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_CTS_MECU) || (__CAR==PSA_C4) || (__CAR==PSA_C3) || (__CAR==PSA_C4_MECU) || __CAR==GM_TAHOE || __CAR==GM_J300||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)|| (__CAR ==GM_SILVERADO) || (__CAR==GM_T300)|| (__CAR==VV_V40)

extern uint16_t lespu16_TCCRCplReqVal;		  	
extern uint16_t lespu16_TracCntrlMaxTorqIncRt;

extern int16_t lesps16_BrkTemp;				

extern uint8_t lespu8_TCCDGCDecayGrad;			
extern uint16_t lespu16_TTDGCDcyGrad;	
extern uint8_t lespu8_WhlSensRghRdMag;			
extern uint8_t lespu8_BSTGRReqdGear;			
extern uint8_t lespu8_BSTGRReqType;			
extern uint8_t lespu8_ChsBrkgLoad;				
extern uint8_t lespu8_SprTireSt;				
extern uint8_t lespu8_VhDynFLWhlSt;			
extern uint8_t lespu8_VhDynFRWhlSt;			
extern uint8_t lespu8_VhDynRLWhlSt;            
extern uint8_t lespu8_VhDynRRWhlSt;            

extern int8_t lesps8_VehDynOvrUndrStr; 

extern uint8_t lespu8TCU_TAR_GC;
extern uint8_t lespu8_TrnsShftLvrPos;

		#if (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU)|| (__CAR==PSA_C3)
extern uint8_t lespu8_SteerWheelRotationSpeed;	
extern uint8_t lespu8_ShiftingAuthorization;	
extern uint8_t lespu8_BrakingInProgress;
extern uint8_t lespu8_TorqueRequestStatus;
extern uint16_t lespu16EMS_N;
		#endif

	/* Added by Kim Jeonghun in 2007.7.21 */
extern uint8_t lespu8TCSysOpMd;			
extern uint8_t lespu8VehStabEnhmntMd;	
extern uint8_t lespu1TreInfMonSysRstPrfmd;
extern uint8_t lespu1TirePrsLowIO;
extern uint8_t lespu1BrkSysBrkLtsReqd;
extern uint8_t lespu1BrkSysRedBrkTlltlReq;
extern uint8_t lespu1StWhAnVDA;
extern uint8_t lespu8BrkSysVTopSpdLimVal;
extern uint8_t lespu1ActVehAccelV;	
extern int16_t  lesps16ActVehAccel_0;	
extern uint8_t lespu8TracCntrlMaxTorqIncRt;
extern uint8_t lespu1BrkSysTrnsGrRqTnstnTp;
extern uint8_t lespu1VSELongAccV;	
extern uint8_t lespu8VSELongAcc;

extern uint8_t lespu1TCSAct;
extern uint8_t lespu1VSEAct;
extern uint8_t lespu1TracTrqDecCntAtv;
extern uint8_t lespu1DrvIndpntBrkAppAct;
extern uint8_t lespu1AutoBrkngAct;
extern uint8_t lespu1TrnsBrkSysCltchRelRqd;
extern uint8_t lespu1EngTqMinExtRngVal;
extern int16_t lesps16EngTqMinExtRng;       
extern uint16_t lespu16_EngOffTmExtRng;
extern uint8_t lespu8WhlSlpSt;
extern uint8_t lespu2TransNtrlCntrlMdStat;     

/* TPMS */
extern uint8_t lespu8TirePressureStatFL;
extern uint8_t lespu8TirePressureStatFR;
extern uint8_t lespu8TirePressureStatRL;
extern uint8_t lespu8TirePressureStatRR;
extern uint16_t lespu16TirePressureFL;
extern uint16_t lespu16TirePressureFR;
extern uint16_t lespu16TirePressureRL;
extern uint16_t lespu16TirePressureRR;

extern uint16_t lespu16BaroPressAbs;
extern uint16_t lespu16EngManfldAbsPrs;
extern uint8_t lespu8SysPwrMd;

extern uint8_t lctcsu8FunctionLampOnTime;
	#endif
	
	#if __HMC_CAN_VER==CAN_HD_HEV
extern uint8_t lespu8ARB_CasVlvCmdC_Pc;		
	#endif
	
#if (__EPB_INTERFACE) || (__AVH)
/* Rx Signal For EPBi, AVH */
extern uint8_t lcanu8EPB_DBF_REQ;				/* EPB dynamic braking request */
extern uint8_t lcanu8EPB_FAIL;					/* information about the availabilty of EPB */
extern uint8_t lcanu8EPB_DBF_DECEL;			/* requested deceleration for DBF */
extern uint8_t lcanu8EPB_STATUS;				/* force sataus of EPB */

/* Tx Signal For EPBi, AVH */
extern uint8_t lcanu8LDM_STAT;					/* longitudinal dynamic management state */
extern uint8_t lcanu8ECD_ACT;					/* ECD activation signal */
#endif	/* ( (__EPBi) || (__AVH) ) */

#if __AVH
/* Tx Signal For AVH */
extern uint8_t lcanu8AVH_STAT;					/* AVH state */
extern uint8_t lcanu8AVH_LAMP;					/* info lamp request to cluster */
extern uint8_t lcanu8AVH_ALARM;				/* Audio active status lamp request to cluster */
extern uint8_t lcanu8REQ_EPB_ACT;				/* ESC request to EPB */
extern uint8_t lcanu8REQ_EPB_STAT;			/* validity of EPB activation request */
extern uint8_t lcanu8AVH_CLU;
#endif	/* __AVH */

#if __SPAS_INTERFACE
  
extern uint8_t lcu8SpasBrkIntReq;
extern uint8_t lcu8SpasBrkIntCmd;
extern uint8_t lcu8SpasBrkIntRpl;
extern uint8_t lcu8SpasBrkIntPtn;
  
#endif	


/* Rx Signal For ACC */
extern int16_t lcans16CF_Ems_DecelReq;		/* Deceleration amount for brake control */
	
#if __AHB_SYSTEM==ENABLE
/* AHB -> ESP Signal */
#define lcanu1AhbMcpCalOffsetInvalid		HmCanFlg6.u1bit0	/* Master Cylinder Offset  Pressure Validity 0:Valid 1:Invalid */
#define lcanu1AhbBls				HmCanFlg6.u1bit1    /* Brake Light signal value 0:BLS OFF 1:0:BLS ON */
#define lcanu1AhbRbcAct				HmCanFlg6.u1bit2    /* AHB RBC Active flag 0:RBC OFF 1:RBC ON */   
#define HmCanFlg63					HmCanFlg6.u1bit3
/* ESP -> AHB Signal */
#define lespu1ActiveBrake 			HmCanFlg6.u1bit4 
#define HmCanFlg65                	HmCanFlg6.u1bit5
#define HmCanFlg66                	HmCanFlg6.u1bit6 
#define HmCanFlg67          		HmCanFlg6.u1bit7 

/* AHB -> ESP Signal */
extern	int16_t	lcans16AhbMcpRaw;				/* Master Cylinder raw pressure value : 0.01 bar Resolution */ 
extern	int16_t	lcans16AhbMcpFilt_1_100Bar;			/* Master Cylinder Offset 보정 압력 값 : 0.01 bar Resolution */
extern	int16_t	lcans16AhbMcTargetPress;		/* Master Cylinder Target Pressure : 0.01 bar Resolution */
extern	int16_t lcans16AhbRbcPress;             /* RBC Pressure Reduction: 0.1 bar Resolution */

/* ESP -> AHB Signal */
extern uint8_t	lsespu8WhlCylStatFL;
extern uint8_t	lsespu8WhlCylStatFR;
extern uint8_t	lsespu8WhlCylStatRL;
extern uint8_t	lsespu8WhlCylStatRR;

#endif	/* #if __AHB_SYSTEM==ENABLE */

#if __TSP /* TSP Signal */
extern uint8_t lespu8TSP_TrlrHtchSwAtv; 
#endif

/* EMS7 Message Signal */
extern uint8_t lcu1Ems7ActiveFlg;


#define lespu1EMS_DCT_TCU				  HmCanFlg7.u1bit0
#define lespu1EMS_HEV_AT_TCU			  HmCanFlg7.u1bit1
#define lespu1TCU_MT_R_GEAR_ERR	          HmCanFlg7.u1bit2       
#define lespu1RDCM_SAMDVLAtv              HmCanFlg7.u1bit3
#define lespu1EMS_EngRunNorm 	          HmCanFlg7.u1bit4 
#define lespu1EPB_GEAR_N_SWITCH_NEUTRAL   HmCanFlg7.u1bit5
#define lespu1EPB_GEAR_N_SWITCH_INVALID   HmCanFlg7.u1bit6 
#define lu1_gsuv_os_us_sig_valid          			  HmCanFlg7.u1bit7 

#endif

extern uint8_t lcanu1OAT_PT_EstM;
extern uint8_t lcanu1OAT_PT_EstV;
extern int8_t lcans8OAT_PT_Est;

/* BDW */
extern uint8_t lespu8CF_Clu_RainSnsStat;
/* BDW */

/* CLU */
extern uint8_t lcanu8DRV_DR_SW;				/* driver door switch */
extern uint8_t lcanu8DRV_Seat_Belt;			/* driver seat belt on/off status */
extern uint8_t lcanu8TRUNK_OPEN_STATUS;		/* Trunk latch switch signal */
extern uint8_t lcanu8CF_HoodStat;			/* Hood Latch Switch signal */

#define lcanu1DrvDoorOpenInv      		HmCanFlg8.u1bit0
#define lcanu1DrvSeatUnBeltedInv		HmCanFlg8.u1bit1
#define lcanu1HoodOpenInv				HmCanFlg8.u1bit2
#define lcanu1TrunkOpenInv			    HmCanFlg8.u1bit3 
#define lespu1_CAN_FLAG_RESERVED_8_4	HmCanFlg8.u1bit4
#define lespu1_CAN_FLAG_RESERVED_8_5	HmCanFlg8.u1bit5
#define lespu1_CAN_FLAG_RESERVED_8_6	HmCanFlg8.u1bit6 
#define lespu1_CAN_FLAG_RESERVED_8_7	HmCanFlg8.u1bit7
/* CLU */
