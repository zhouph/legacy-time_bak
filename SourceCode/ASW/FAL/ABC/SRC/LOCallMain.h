#ifndef __LOCALLMAIN_H__
#define __LOCALLMAIN_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/
extern void	LOABS_vCallCoordinator(void);

#if __TCS
extern void	LOTCS_vCallCoordinator(void);
#endif

#if __VDC
extern void	LOESP_vCallCoordinator(void);
#endif

#endif
