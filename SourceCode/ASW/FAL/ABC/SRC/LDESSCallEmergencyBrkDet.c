/*******************************************************************
* Project Name:
* File Name:
* Description:
********************************************************************/
/* Includes	********************************************************/
#if !SIM_MATLAB

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESSCallEmergencyBrkDet
	#include "Mdyn_autosar.h"
#endif
#endif
#include	"LVarHead.h"
#include	"LCallMain.H"

#include	"LSABSCallSensorSignal.H"
#include	"LDABSCallDctForVref.H"
#include	"LDABSCallVrefEst.H"
#include	"LDABSCallEstVehDecel.H"
#include	"LDABSCallVrefMainFilterProcess.H"
#include	"LCABSCallControl.h"


#if __VDC
#include	"LDABSCallVrefCompForESP.H"
#include 	"LCESPInterfaceSlipController.h"
#include	"LCESPCalLpf.h"
#endif

#include	"LDESSCallEmergencyBrkDet.h"

/*******************************************************************/
/* Local Definition  ***********************************************/

#if	__ESS
#define	U16_T_Warning_Ready				L_U8_TIME_10MSLOOP_150MS
#define	U16_T_Warning_Reset_Time		L_U8_TIME_10MSLOOP_100MS			 /*	 Reset Condition  */
#define	U16_T_Warning_Reset_Time_2		L_U8_TIME_10MSLOOP_50MS	
#define	TIMER_LIMIT						L_U8_TIME_10MSLOOP_1000MS 
#define	U16_T_Max_Hazard				L_U16_TIME_10MSLOOP_5S

/*******************************************************************/
/* Variables Definition*********************************************/
U8_BIT_STRUCT_t    ESSF0,ESSF1;



/*Global Variable Declaration*****************************************/

uint16_t	ldu16vehicleSpdForEmrBrkDet;
int16_t		    lds16VehicleDecel;
uint8_t		    ldu8EssState;
uint8_t		    ldu8EssMode;
int16_t		    lds16EssDctTimer1;
int16_t		    lds16EssDctTimer2;
uint16_t		ldu16EssHazardLampOnTime;
uint16_t		ldu16EssBlsOffTime;
uint16_t 		ldu16EssDecelOffTime;
uint16_t		ldu16EssAbsOffTime;
int16_t		    lds16EssWarningReadyTime;
#if (__ESS_EXTENDED_FN == ENABLE)
uint16_t	ldu16vehicleSpdForExdEssDet; 
uint8_t 		ldu8ExdEssDctTimer;
uint16_t 		ldu16ExdEssExitTimer1;				
uint8_t 		ldu8ExdEssExitTimer2;				
uint8_t 		ldu8ExdEssflagSetTimer;		
uint8_t 		ldu8ExdEssExitTimer3;		
uint16_t	ldu16EssExitSpeed;
uint16_t		ldu16EssIgnOfftimer;
#endif
/*******************************************************************/

/* Local Function prototype ****************************************/

static void    LDABS_vCallEmergencyBrkSysFailDet(void);
static void    LDABS_vCallEmergencyBrkStateDet(void);
static void    LDABS_vCallEmergencyBrkReset(void);

/*******************************************************************/

void LDABS_vCallEmergencyBrkMain(void)
{
    LDABS_vCallEmergencyBrkSysFailDet();
    LDABS_vCallEmergencyBrkStateDet();
}

/*******************************************************************/
static void LDABS_vCallEmergencyBrkSysFailDet(void)
{
	#if (NEW_FM_ENABLE == ENABLE) 
     if((fu1FMWheelFLErrDet==1)||(fu1FMWheelFRErrDet==1)
    	||(fu1FMWheelRLErrDet==1)||(fu1FMWheelRRErrDet==1)
    	||(fu1FMWheelFrontErrDet==1)||(fu1FMWheelRearErrDet==1)
    	||(fu1FMSameSideWSSErrDet==1)||(fu1FMDiagonalWSSErrDet==1)
    	||(fu1FMECUHwErrDet==1)||(fu1FMVoltageOverErrDet==1)
    	||(fu1FMVoltageUnderErrDet==1)||(fu1FMVoltageLowErrDet==1)
    	||(fu1FMEssRelayErrDet==1)
    	||(fu1FMBLSErrDet==1)
    	||(fu1FMVCErrDet==1) /*variant code*/
		||(ee_ece_flg==1) /* 이전 IGN time 에 wheel speed jump */
    	 )
    {
    		lcu1EssCtrlFail = 1;
    }
    else
    {
    	lcu1EssCtrlFail = 0;
    }
    #else
    if(ldu1EssSystemFail==0)  
    {
        if(flu1ESSLampErrDet==1)       /* System Error Set 조건 추가 */
        {
            ldu1EssSystemFail=1;                             
        }
        else
        {
           ;
        }          
    }
    else
    {
        if(flu1ESSLampErrDet==0)       /* System Error Reset 조건 추가 */
        {
            ldu1EssSystemFail=0;
        }
        else
        {
            ;
        }  
    }     
    #endif        
          
    #if (__ESS_EXTENDED_FN == ENABLE)
    /*Ign off timer for Ess extended mode reset*/
	if(lcu1ExdEssActiveFlag == 1)
	{	
	    if(wu8IgnStat == CE_OFF_STATE)
	    {
	    	if(ldu16EssIgnOfftimer < L_U16_TIME_10MSLOOP_2MIN)
	    	{
	    		ldu16EssIgnOfftimer = ldu16EssIgnOfftimer  + 1;
	    	}
	    	else
	    	{
	    		;
	    	}
	    	if(ldu16EssIgnOfftimer >= L_U16_TIME_10MSLOOP_1MIN)
		    {
		    	ldu1EssResetIgnOff1Min = 1;
		    }
		    else
		    {
		    	ldu1EssResetIgnOff1Min = 0;
		    }	 
	    }
	    else
	    {
	    	ldu16EssIgnOfftimer = 0;
	    	ldu1EssResetIgnOff1Min = 0;
	    }        
	}
	else
	{
		ldu16EssIgnOfftimer = 0;
		ldu1EssResetIgnOff1Min = 0;
	}
	/*Ign off -> on flag for Ess extended mode reset*/
	if(lcu1ExdEssActiveFlag == 1)
	{
	    if(wu1IgnRisingEdge == 1)
    	{
    		ldu1ESSIgnRisingEdge = 1;
    	}
    	else
    	{
    		ldu1ESSIgnRisingEdge = 0;
    	}
    }
    else
    {
    	ldu1ESSIgnRisingEdge = 0;
    }
    #endif           
}

/*******************************************************************/
static void LDABS_vCallEmergencyBrkStateDet(void)
{
	ldu16vehicleSpdForEmrBrkDet=vref_resol_change;
    #if (__ESS_EXTENDED_FN == ENABLE)
	ldu16vehicleSpdForExdEssDet=vref_resol_change; /*added by LEE.D.S*/
	#endif	
	lds16VehicleDecel=ebd_refilt_grv;
	if((speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
	||(fu1OnDiag==1)
	#if (NEW_FM_ENABLE == ENABLE) 
	||(lcu1EssCtrlFail==1)
	#endif
    	#if (__ESS_EXTENDED_FN == ENABLE)
	||(ldu1EssResetIgnOff1Min == 1)
	||(ldu1ESSIgnRisingEdge == 1)
    	#endif        					
	)
	{
		ldu8EssState=0;
		ldu8EssMode=0;
        LDABS_vCallEmergencyBrkReset();	
        lcu1EssActiveFlag=0;	
    	#if (__ESS_EXTENDED_FN == ENABLE)
   	 	/*added by lee.d.s*/
   	 	lcu1ExdEssActiveFlag=0; 	
    	#endif        		
	}
	else
	{
		#if (NEW_FM_ENABLE != ENABLE)
		if(ldu1EssSystemFail==0) 
		{
		#endif 
			if(ldu8EssState==1)
			{
				if(ldu8EssMode==1)       /*Stand By*/
				{
					if((ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min)&&(BLS==1)&&(fu1HazardSw==0))
					{
						ldu8EssMode=2; 
					}
					else
					{
						;
					}				
				}
				else if(ldu8EssMode==2) /* Warning Ready  */
				{					
					
                    if((lds16EssWarningReadyTime>=U16_T_Warning_Ready) || (BLS==0) ||(fu1HazardSw==1) )
					{
						ldu8EssMode=1;
                        LDABS_vCallEmergencyBrkReset();						
					}	
					else 
					{
						if((ABS_fz==1)&&(ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min))		
						{
							lds16EssDctTimer2++;
						}					
						else
						{
							lds16EssDctTimer2=0;
						}
						if(ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min)
						{
							if(lds16VehicleDecel<=S16_Accele_Start)						
							{
								lds16EssDctTimer1++;
							}
							else if(lds16VehicleDecel > S16_Accele_End_1  )
							{
								lds16EssDctTimer1=0;
							}	
							else if(lds16EssDctTimer1>0) /* ->S16_Accele_End_0 */
							{
								lds16EssDctTimer1--;
							}
							else
							{
								;
							}
						}
						else
						{
							lds16EssDctTimer1=0;
						}
						
						if(ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min)	
						{
							lds16EssWarningReadyTime=0;
																				
						}	
						else if((lds16VehicleDecel>S16_Accele_Start)&&(ABS_fz==0))	
						{
							if(lds16EssWarningReadyTime<U16_T_Warning_Ready)
							{
								lds16EssWarningReadyTime++;	
							}
							else
							{
								;
							}
						}
						else
						{
							lds16EssWarningReadyTime=0;
						}	
												
						lds16EssDctTimer1=LCABS_s16LimitMinMax(lds16EssDctTimer1,0,TIMER_LIMIT);
						lds16EssDctTimer2=LCABS_s16LimitMinMax(lds16EssDctTimer2,0,TIMER_LIMIT);
						
						if(lds16EssDctTimer1>=U16_T_Detect_Decel)
						{
							ldu8EssState=2;
							ldu8EssMode=1;	
							/*LDABS_vCallEmergencyBrkReset();*/	
							lds16EssDctTimer1=0;										
							lds16EssWarningReadyTime=0;
							ldu1EssActByAbs=0;
							ldu1EssActByDecel=1;		  
						}
						else if(lds16EssDctTimer2>=U16_T_Detect_ABS_ON)
							{
							ldu8EssState=2;
							ldu8EssMode=2;	
							/*LDABS_vCallEmergencyBrkReset();*/	
							lds16EssDctTimer2=0;										
							lds16EssWarningReadyTime=0;
							ldu1EssActByAbs=1;
							ldu1EssActByDecel=0;	
						}
						else
						{
							;
						}																										
					}
				}
				
				#if (__ESS_EXTENDED_FN == ENABLE)
			  	else if(ldu8EssMode==3) 
				{
					if(ldu16vehicleSpdForExdEssDet<=S16_Vehicle_Speed_ExdEss_Max)
					{
						ldu8ExdEssDctTimer++;
						
						if(ldu8ExdEssDctTimer>=U16_T_Dct_ExdEss)
						{
							ldu8EssState=3;
							if(ldu16vehicleSpdForExdEssDet>S16_Vehicle_Speed_ExdEss_Min){
							    ldu8EssMode=1;
							}
							else{
							    ldu8EssMode=2;
							}
							
							LDABS_vCallEmergencyBrkReset();	
							ldu16EssExitSpeed=vref_resol_change;
						}
						else
						{
							;
						}
					}
				    else
					{
						ldu8EssState=1;
    					ldu8EssMode=1;
    					LDABS_vCallEmergencyBrkReset();	
					}					
				}
				#endif				
				else
				{
					;
				}												
			}
			else if(ldu8EssState==2)
			{
				if(fu1HazardSw==1)
				{
					ldu16EssHazardLampOnTime++;
				}
				else
				{
					ldu16EssHazardLampOnTime=0;
				}
				
				if(BLS==0)
				{
					ldu16EssBlsOffTime++;
				}
				else
				{
					ldu16EssBlsOffTime=0;
				}
                if(ABS_fz==0)
                {
                	ldu16EssAbsOffTime++;
                }
                else
                {
                	ldu16EssAbsOffTime=0;
                }
                
                if(lds16VehicleDecel > S16_Accele_End_0  )
                {
                    ldu16EssDecelOffTime++;
                }
                else if(lds16VehicleDecel <= S16_Accele_Start)
                {
                    ldu16EssDecelOffTime=0;
                }
                else if(ldu16EssDecelOffTime>0)
                {
                	ldu16EssDecelOffTime--;
                }
                else
                {
                	;
                }
                
				ldu16EssHazardLampOnTime=LCABS_s16LimitMinMax(ldu16EssHazardLampOnTime,0,TIMER_LIMIT);
				ldu16EssBlsOffTime		=LCABS_s16LimitMinMax(ldu16EssBlsOffTime	  ,0,TIMER_LIMIT);
				ldu16EssDecelOffTime	=LCABS_s16LimitMinMax(ldu16EssDecelOffTime,0,TIMER_LIMIT);
				ldu16EssAbsOffTime		=LCABS_s16LimitMinMax(ldu16EssAbsOffTime,0,TIMER_LIMIT);
				if(ldu1EssActByAbs==0){
					if((ABS_fz==1)&&(ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min))		
					{
						lds16EssDctTimer2++;
					}					
					else
					{
						lds16EssDctTimer2=0;
					}
					if(lds16EssDctTimer2>=U16_T_Detect_ABS_ON)
					{
						lds16EssDctTimer2=0;										
						ldu1EssActByAbs=1;

					}
					else
					{
						;
					}
				}
				else{
					if(ldu16EssAbsOffTime>=	U16_T_Warning_Reset_Time){
						ldu1EssActByAbs=0;
					}
					else{}
						
				}
									
				if(ldu1EssActByDecel==0){
					if(ldu16vehicleSpdForEmrBrkDet>S16_Vehicle_Speed_Min)
					{
						if(lds16VehicleDecel<=S16_Accele_Start)						
						{
							lds16EssDctTimer1++;
						}
						else if(lds16VehicleDecel >	S16_Accele_End_1  )
						{
							lds16EssDctTimer1=0;
						}	
						else if(lds16EssDctTimer1>0) /*	->S16_Accele_End_0 */
						{
							lds16EssDctTimer1--;
						}
						else
						{
							;
						}
					}	
					else
					{
						lds16EssDctTimer1=0;
					}
					if(lds16EssDctTimer1>=U16_T_Detect_Decel)
					{
						lds16EssDctTimer1=0;
						ldu1EssActByDecel=1;																		  
					}	
					else{}			
				}
				else{
					if((ldu16EssDecelOffTime>=U16_T_Warning_Reset_Time_2)&&(lds16VehicleDecel>=S16_Accele_End_1)){
						ldu1EssActByDecel=0;
					}
					else{}
				}
				
						

				if (ldu16EssHazardLampOnTime>= U16_T_Hazard_Lamp_On){
					ldu8EssState=1;					 
					ldu8EssMode=1;	  
					LDABS_vCallEmergencyBrkReset();						
				}				 
				else if(ldu16EssBlsOffTime >= U16_T_BLS_Off){
					ldu8EssState=1;				  
					#if	(__ESS_EXTENDED_FN == ENABLE)
					if(ldu16vehicleSpdForExdEssDet<=S16_Vehicle_Speed_ExdEss_Max){
						ldu8EssMode=3;
					}
					else{
						ldu8EssMode=1;		 
					}							
					#else		  
					ldu8EssMode=1;
					#endif	
	  
					LDABS_vCallEmergencyBrkReset();	
				}
				else if((ldu1EssActByDecel==0)&&(ldu1EssActByAbs==0)){
					ldu8EssState=1;				  
					#if	(__ESS_EXTENDED_FN == ENABLE)
					if(ldu16vehicleSpdForExdEssDet<=S16_Vehicle_Speed_ExdEss_Max){
						ldu8EssMode=3;
					}
					else{
						ldu8EssMode=1;		 
					}							
					#else		  
					ldu8EssMode=1;
					#endif	
					LDABS_vCallEmergencyBrkReset();							  
				}				 
				else{
					;
				}				   
/*
                if( (ldu16EssHazardLampOnTime>= U16_T_Hazard_Lamp_On) ||
                    (ldu16EssBlsOffTime      >= U16_T_BLS_Off)        ||
                    ((ldu16EssDecelOffTime>= (U16_T_Warning_Reset_Time_2))&&(ldu16EssAbsOffTime>= U16_T_Warning_Reset_Time  )
                    &&(lds16VehicleDecel>=S16_Accele_End_1))  )                      
                {
                    ldu8EssState=1;           
                	#if (__ESS_EXTENDED_FN == ENABLE)
					if(ldu16vehicleSpdForExdEssDet<=S16_Vehicle_Speed_ExdEss_Max){
    					ldu8EssMode=3;
     				}
     				else{
                	    ldu8EssMode=1;       
     				}	                    	
                	#else         
                	ldu8EssMode=1;
                	#endif     
                    LDABS_vCallEmergencyBrkReset();  
                }
                else
                {
                    ;
                }                                    				
*/																
			}
        	#if (__ESS_EXTENDED_FN == ENABLE)
			else if(ldu8EssState==3){					
				if(fu1HazardSw==1){
					ldu16EssHazardLampOnTime++;
				}
				else{
					ldu16EssHazardLampOnTime=0;
				}
		        
		        if(ldu16EssHazardLampOnTime>=U16_T_Hazard_Lamp_On){
					ldu8EssState=1;
	        		ldu8EssMode=1;
	        		LDABS_vCallEmergencyBrkReset();   
				}
				else if(ldu8EssMode==1){	
					if(ldu16vehicleSpdForExdEssDet<=S16_Vehicle_Speed_ExdEss_Min){
						ldu8ExdEssflagSetTimer++;	
						if(ldu8ExdEssflagSetTimer>=U16_T_Dct_ExdEss){
						    ldu8EssMode=2;
							LDABS_vCallEmergencyBrkReset(); 
							ldu8ExdEssflagSetTimer=0;
						}
						else{
							;
						}
					}	
					else{
					    ldu8ExdEssflagSetTimer=0;
					}		    
			        
				  	ldu16ExdEssExitTimer1++;
				  	
					if(ldu16vehicleSpdForExdEssDet>=(ldu16EssExitSpeed+S16_Vdiff_ExdEss_Exit)){	 
						ldu8ExdEssExitTimer2++;   										    							
					}
				  	else{
				  		ldu8ExdEssExitTimer2=0;	
				  	}
				  	    						  	
					if(ldu16ExdEssExitTimer1>=U16_T_Dct_ExdEss_Exit){	
						ldu8EssState=1;
						ldu8EssMode=1;
						LDABS_vCallEmergencyBrkReset(); 
					}
					else if(ldu8ExdEssExitTimer2>=U16_T_Dct_ExdEss){
						ldu8EssState=1;
						ldu8EssMode=1;
						LDABS_vCallEmergencyBrkReset(); 
							    							
					}
				  	else{
				  		;
				  	}
				}
				else if(ldu8EssMode==2){
					if(ldu16vehicleSpdForExdEssDet>=S16_Vehicle_Speed_ExdEss_Min){
						ldu8ExdEssExitTimer3++;
						if(ldu8ExdEssExitTimer3>=U16_T_Dct_ExdEss){
							ldu8EssState=1;
							ldu8EssMode=1;
							LDABS_vCallEmergencyBrkReset(); 
							 
						}
    					else{
    						;
    					}            									
					}
					else{
						ldu8ExdEssExitTimer3=0;
					}					    
				}
				else{
				    ;
				}
					
			}     
			#endif  			
			else
			{
				ldu8EssState=1;
				ldu8EssMode=1;
				LDABS_vCallEmergencyBrkReset();
			}
			
			/* ESS Active Flag Condition */
		    lcu1EssActiveFlag=0;
		    #if (__ESS_EXTENDED_FN == ENABLE)
		    lcu1ExdEssActiveFlag=0;
		    #endif 		
		    	
			if(ldu8EssState== 2){
		        lcu1EssActiveFlag=1;				
							
			}
			#if (__ESS_EXTENDED_FN == ENABLE)
			else if (ldu8EssState==3){
			    lcu1ExdEssActiveFlag=1;
			}
			#endif
			else{
                ;
			}
		#if (NEW_FM_ENABLE != ENABLE)
		}
		else
		{
            ldu8EssState=0;
	        ldu8EssMode=0;
            LDABS_vCallEmergencyBrkReset();   
		    lcu1EssActiveFlag=0;
            #if (__ESS_EXTENDED_FN == ENABLE)
			lcu1ExdEssActiveFlag=0;
			#endif		    
		}
		#endif
	}	
}
/*******************************************************************/
static void LDABS_vCallEmergencyBrkReset(void)
{
	lds16EssDctTimer1=0;
	lds16EssDctTimer2=0;		
	lds16EssWarningReadyTime=0;
    ldu16EssHazardLampOnTime=0;
    ldu16EssBlsOffTime=0;
    ldu16EssDecelOffTime=0;
    ldu16EssAbsOffTime=0;   
	
    #if (__ESS_EXTENDED_FN == ENABLE)
	ldu8ExdEssDctTimer=0;
	ldu16ExdEssExitTimer1=0;
	ldu8ExdEssExitTimer2=0;
	ldu8ExdEssExitTimer3=0;  
	ldu16EssExitSpeed=0;
    #endif       
}

/*******************************************************************/
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESSCallEmergencyBrkDet
	#include "Mdyn_autosar.h"
#endif    

