#ifndef __LCEVPCALLCONTROL_H__
#define __LCEVPCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"
/*Global Type Declaration ****************************************************/

//#define levpu1EVPPumpOn		EVP00.bit0
//#define levpu1BLSActPrev    EVP00.bit1
//#define levpu1BLSAct    	EVP00.bit2
//#define levpu1EVPInitPumpOn	EVP00.bit3
//#define EVP_FLAG00_4    	EVP00.bit4
//#define EVP_FLAG00_5    	EVP00.bit5
//#define EVP_FLAG00_6    	EVP00.bit6
//#define EVP_FLAG00_7    	EVP00.bit7

/*Global Extern Variable  Declaration*****************************************/
//extern struct bit_position EVP00;

  #if __EVP
extern int16_t	levps16VacuumSignal;
extern uint16_t	levpu16EVPPumpOffTimer;
extern uint16_t	levpu16EVPPumpOnRunTimer;
extern uint8_t	levpu8EVPPumpTurnOnCnt;
extern uint8_t	levpu8EVPAccumPumpOnSubTimer1;
extern uint16_t	levpu16EVPAccumPumpOnSubTimer2;
extern uint16_t	levpu16EVPAccumPumpOnTimer;
extern uint16_t	levpu16ESPEngStartTimer;
extern int16_t 	levps16BaroMAPPrsDiff;
extern uint16_t	levpu16EVPDebounceTimer;
extern int16_t	levps16MaxPumpOnRunTime;
extern int16_t	levps16K_Turn_EVP_On_Threshold;
extern int16_t	levps16K_Turn_EVP_Off_Threshold;
extern uint8_t levpu1EVPPumpOn;
extern uint8_t levpu1BLSActPrev;
extern uint8_t levpu1BLSAct;
extern uint8_t levpu1EVPInitPumpOn;
extern uint8_t levpu8EVPflags;
  #endif  /* __EVP */
  #if __VACUUM_PUMP_RELAY
extern uint8_t  	lhbbu1VacuumPumpOn;
  #endif /* __VACUUM_PUMP_RELAY */

/*Global Extern Functions  Declaration****************************************/
  #if __EVP
extern void LCEVP_vCallControl(void);
  #endif  /* __EVP */
  #if __VACUUM_PUMP_RELAY  
extern void	LAEVP_vActuateVacuumPump(void);
  #endif

/*Global MACRO CONSTANT Definition    ****************************************/
  #if __EVP
#define ON	  	1
#define OFF	  	0

#define VALID 	0
#define INVALID 1

#define TRUE  	1
#define FALSE 	0

#define ACC		 1
#define RUN		 2
#define CRANK	 3

#define ClosedLoopMode	0
#define OpenLoopMode	1

/*
#define K_Turn_EVP_On_Threshold_High_A	-375
#define K_Turn_EVP_Off_Threshold_High_A	-435
*/
  #endif  /* __EVP */
#endif





