/******************************************************************************
* Project Name: Torque Vectoring Brake Based
* File: LDTVBBCallActHW.C
* Date: 
******************************************************************************/

/* Includes ******************************************************************/
 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif

#if __TVBB
#include "LACallMain.h"
#include "LCTVBBCallControl.h"

int16_t     tvbb_msc_target_vol, latvbbs16ExitStartCurrent; 

#endif      /* #if __TVBB */
 
 #if __TVBB
/* LocalFunction prototype ***************************************************/
void LCMSC_vSetTVBBTargetVoltage(void); 
void LATVBB_vCallActHW(void);
#if (__MGH_80_10MS == ENABLE)
void LATVBB_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *latvbbHP1, WHEEL_VV_OUTPUT_t *latvbbHP2);
#endif
int16_t LAMFC_s16SetMFCCurrentTVBB(uint8_t CIRCUIT, int16_t MFC_Current_old);
#endif  
  
 #if __TVBB
void LCMSC_vSetTVBBTargetVoltage(void)
{
    if (lctvbbu1TVBB_ON==1)
    {
        TVBB_MSC_MOTOR_ON = 1;
    }
    else
    {
        TVBB_MSC_MOTOR_ON = 0;
    } 
     
    if (lctvbbu1TVBB_ON==1)
    {
        if ((lctvbbu16TVBBCtlRunningCnt<U16_TVBB_INIT_FULL_ON_TIME)&&(lctvbbu8ControlExitTimer == 0))
        {
            tvbb_msc_target_vol = S16_TVBB_INIT_MOTOR_VOLTAGE;
        }
        else
        {               
            tvbb_msc_target_vol = LCESP_s16IInter5Point( vrefk, S16_TVBB_MOTOR_VOL_REF_SPD_L , S16_TVBB_MOTOR_VOLTAGE_LSP ,  
                                                                S16_TVBB_MOTOR_VOL_REF_SPD_ML, S16_TVBB_MOTOR_VOLTAGE_MLSP,  
                                                                S16_TVBB_MOTOR_VOL_REF_SPD_M , S16_TVBB_MOTOR_VOLTAGE_MSP ,
                                                                S16_TVBB_MOTOR_VOL_REF_SPD_MH, S16_TVBB_MOTOR_VOLTAGE_MHSP,
                                                                S16_TVBB_MOTOR_VOL_REF_SPD_H , S16_TVBB_MOTOR_VOLTAGE_HSP );
        }
    }
    else
    {
            tvbb_msc_target_vol = 0 ;            
    }
}

void    LATVBB_vCallActHW(void)
{
    if (ldtvbbu1FrontWhlCtl == 1)
    {
        if(lctvbbu1TVBB_ON==1)
        {
            if (lctvbbu1RightWhlCtl==1)   // FR-wheel
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fr = 1;
                S_VALVE_RIGHT = 1;
                S_VALVE_LEFT = 1;
                VALVE_ACT = 1;
                HV_VL_rl = 1;
                AV_VL_rl = 1;
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&RL, &latvbb_RL1HP, &latvbb_RL2HP);
                la_RL1HP = latvbb_RL1HP; 
		        la_RL2HP = latvbb_RL2HP; 
		          #endif
                #else
                TCL_DEMAND_PRIMARY = 1;
                S_VALVE_PRIMARY = 1;
                S_VALVE_SECONDARY = 1;
                VALVE_ACT = 1;    
                HV_VL_fl = 1;     
                AV_VL_fl = 1; 
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&FL, &latvbb_FL1HP, &latvbb_FL2HP);
                la_FL1HP = latvbb_FL1HP; 
		        la_FL2HP = latvbb_FL2HP; 
		          #endif 
                #endif   
            }
            else if (lctvbbu1LeftWhlCtl==1)   // FL-wheel
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fl = 1;
                S_VALVE_RIGHT = 1;
                S_VALVE_LEFT = 1;
                VALVE_ACT = 1;
                HV_VL_rr = 1;
                AV_VL_rr = 1;
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&RR, &latvbb_RR1HP, &latvbb_RR2HP);
                la_RR1HP = latvbb_RR1HP; 
		        la_RR2HP = latvbb_RR2HP; 
		          #endif 
                #else
                TCL_DEMAND_PRIMARY = 1;
                S_VALVE_PRIMARY = 1; 
                S_VALVE_SECONDARY = 1;
                VALVE_ACT = 1;    
                HV_VL_fr = 1;     
                AV_VL_fr = 1; 
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&FR, &latvbb_FR1HP, &latvbb_FR2HP);
                la_FR1HP = latvbb_FR1HP; 
		        la_FR2HP = latvbb_FR2HP; 
		          #endif    
                #endif
            }
            else
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fr = 0;
                S_VALVE_RIGHT = 0;
                TCL_DEMAND_fl = 0;
                S_VALVE_LEFT = 0;
                VALVE_ACT = 0;  
                HV_VL_rl = 0;
                AV_VL_rl = 0;
                HV_VL_rr = 0;
                AV_VL_rr = 0;        
                #else
                TCL_DEMAND_PRIMARY = 0;
                S_VALVE_PRIMARY = 0;
                VALVE_ACT = 0;
                HV_VL_fl = 0;
                AV_VL_fl = 0;
                HV_VL_fr = 0;
                AV_VL_fr = 0;
                #endif
    

            }
        } 
        else
        {
            ;  
        }
    }
    else
    {   
        if(lctvbbu1TVBB_ON==1)
        {
            if (lctvbbu1LeftWhlCtl==1)
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fr = 1;
                S_VALVE_RIGHT = 1;
                S_VALVE_LEFT = 1;
                VALVE_ACT = 1;
                HV_VL_fr = 1;
                AV_VL_fr = 1;
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&FR, &latvbb_FR1HP, &latvbb_FR2HP);
                la_FR1HP = latvbb_FR1HP; 
		        la_FR2HP = latvbb_FR2HP; 
		          #endif
                #else
                TCL_DEMAND_SECONDARY = 1;
                S_VALVE_PRIMARY = 1;
                S_VALVE_SECONDARY = 1;
                VALVE_ACT = 1;    
                HV_VL_rr = 1;     
                AV_VL_rr = 1; 
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&RR, &latvbb_RR1HP, &latvbb_RR2HP);
                la_RR1HP = latvbb_RR1HP; 
		        la_RR2HP = latvbb_RR2HP; 
		          #endif    
                #endif
            }
            else if (lctvbbu1RightWhlCtl==1)
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fl = 1;
                S_VALVE_LEFT = 1;
                S_VALVE_RIGHT = 1;
                VALVE_ACT = 1;
                HV_VL_fl = 1;
                AV_VL_fl = 1;
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&FL, &latvbb_FL1HP, &latvbb_FL2HP);
                la_FL1HP = latvbb_FL1HP; 
		        la_FL2HP = latvbb_FL2HP; 
		          #endif
                #else
                TCL_DEMAND_SECONDARY = 1;
                S_VALVE_PRIMARY = 1;
                S_VALVE_SECONDARY = 1;
                VALVE_ACT = 1;    
                HV_VL_rl = 1;     
                AV_VL_rl = 1;  
                  #if (__MGH_80_10MS == ENABLE)
                LATVBB_vGenerateLFCDutyWL(&RL, &latvbb_RL1HP, &latvbb_RL2HP);
                la_RL1HP = latvbb_RL1HP; 
		        la_RL2HP = latvbb_RL2HP; 
		          #endif              
                #endif
            }
            else
            {
                #if (__SPLIT_TYPE == 0)
                TCL_DEMAND_fr = 0;
                S_VALVE_RIGHT = 0;
                TCL_DEMAND_fl = 0;
                S_VALVE_LEFT = 0;
                VALVE_ACT = 0;          
                HV_VL_fl = 0;
                AV_VL_fl = 0;
                HV_VL_fr = 0;
                AV_VL_fr = 0;
                #else
                TCL_DEMAND_SECONDARY = 0;
                S_VALVE_SECONDARY = 0;
                VALVE_ACT = 0;    
                HV_VL_rl = 0;     
                AV_VL_rl = 0;     
                HV_VL_rr = 0;     
                AV_VL_rr = 0;    
                #endif 
                
            }   
        } 
        else
        {
            ;  
        }
    }    
} 
#if (__MGH_80_10MS == ENABLE)
void LATVBB_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *latvbbHP1, WHEEL_VV_OUTPUT_t *latvbbHP2)
{
    WL=WL_temp;
    
    if(HV_VL_wl==1)
    {
        latvbbHP1->u8InletOnTime = 0;            
        latvbbHP2->u8InletOnTime = 0;            
                
        if (AV_VL_wl==1)
        {
        latvbbHP1->u8PwmDuty     = PWM_duty_DUMP;
        latvbbHP2->u8PwmDuty     = PWM_duty_DUMP;
        
            latvbbHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
            latvbbHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
        }
        else
        {
            latvbbHP1->u8PwmDuty     = PWM_duty_HOLD;
            latvbbHP2->u8PwmDuty     = PWM_duty_HOLD;
        }
    }
    else
    {
        ;
    }
}
#endif

int16_t LAMFC_s16SetMFCCurrentTVBB(uint8_t CIRCUIT, int16_t MFC_Current_old) 
{
    int16_t     MFC_Current_new = 0;
    
    #if (__SPLIT_TYPE == 0) 
    if ((((CIRCUIT == 2)&&
    (((ldtvbbu1FrontWhlCtl==1)&&(lctvbbu1LeftWhlCtl==1))||(ldtvbbu1FrontWhlCtl==0)&&(lctvbbu1RightWhlCtl==1)))
    ||((CIRCUIT == 1)&&
    (((ldtvbbu1FrontWhlCtl==1)&&(lctvbbu1RightWhlCtl==1))||(ldtvbbu1FrontWhlCtl==0)&&(lctvbbu1LeftWhlCtl==1))))
    &&(lctvbbu1TVBB_ON==1))
      #else
    if (
        (((ldtvbbu1FrontWhlCtl == 1)&&(CIRCUIT == 1))||((ldtvbbu1FrontWhlCtl == 0)&&(CIRCUIT == 2)))
      &&(lctvbbu1TVBB_ON==1)
       )
      #endif
    {
        if ((lctvbbu16TVBBCtlRunningCnt<U16_TVBB_INIT_FULL_ON_TIME)&&(lctvbbu8ControlExitTimer == 0))
        {
            MFC_Current_new = S16_TVBB_INITIAL_CURRENT;      
        }
        else 
        { 
            #if (__TVBB_TC_DUTY_BY_DEL_YAW == ENABLE)
                
              esp_tempW0 = LCESP_s16FindRoadDependatGain(det_alatm, S16_TVBB_CURRENT_HIGH_HMU,    S16_TVBB_CURRENT_HIGH_MMU,    S16_TVBB_CURRENT_HIGH_LMU);
              esp_tempW1 = LCESP_s16FindRoadDependatGain(det_alatm, S16_TVBB_CURRENT_MEDHIGH_HMU, S16_TVBB_CURRENT_MEDHIGH_MMU, S16_TVBB_CURRENT_MEDHIGH_LMU);
              esp_tempW2 = LCESP_s16FindRoadDependatGain(det_alatm, S16_TVBB_CURRENT_MEDLOW_HMU,  S16_TVBB_CURRENT_MEDLOW_MMU,  S16_TVBB_CURRENT_MEDLOW_LMU);
              esp_tempW3 = LCESP_s16FindRoadDependatGain(det_alatm, S16_TVBB_CURRENT_LOW_HMU,     S16_TVBB_CURRENT_LOW_MMU,     S16_TVBB_CURRENT_LOW_LMU);
              
              MFC_Current_new = LCESP_s16IInter4Point((-lctvbbs16ControlMoment),                                
                                                       S16_TVBB_CTRL_MOMENT_LOW,     esp_tempW3,
                                                       S16_TVBB_CTRL_MOMENT_MEDLOW,  esp_tempW2,             
                                                       S16_TVBB_CTRL_MOMENT_MEDHIGH, esp_tempW1,      
                                                       S16_TVBB_CTRL_MOMENT_HIGH,    esp_tempW0);
                                                       
              /*              
              MFC_Current_new = LCESP_s16IInter4Point((-lctvbbs16ControlMoment),                                
                                                       S16_TVBB_CTRL_MOMENT_LOW,     S16_TVBB_CURRENT_LOW_HMU   ,
                                                       S16_TVBB_CTRL_MOMENT_MEDLOW,  S16_TVBB_CURRENT_MEDLOW_HMU,             
                                                       S16_TVBB_CTRL_MOMENT_MEDHIGH, S16_TVBB_CURRENT_MEDHIGH_HMU ,      
                                                       S16_TVBB_CTRL_MOMENT_HIGH,    S16_TVBB_CURRENT_HIGH_HMU    );*/
            #else
              MFC_Current_new = S16_TVBB_CURRENT_CONST;
            #endif  
                
        if (lctvbbu8ControlExitTimer > 0 )
        {
            if (lctvbbu8ControlExitTimer == 1)
            {
                if (MFC_Current_old == S16_TVBB_INITIAL_CURRENT)
                {
                    latvbbs16ExitStartCurrent = esp_tempW3;
                }
                else
                {
                    latvbbs16ExitStartCurrent = MFC_Current_new;
                }
            }
            else
            {
                ;
            }
            
            MFC_Current_new = LCESP_s16IInter2Point( lctvbbu8ControlExitTimer, 
                                                                        0,  latvbbs16ExitStartCurrent,   
                                                        U8_TVBB_EXIT_TIME,  S16_TVBB_MIN_EXIT_CURRENT );
        }
        else
        {
            latvbbs16ExitStartCurrent = 0;
        }                                                                                                                                            
     }  
    }
    else
    {
        MFC_Current_new = 0;
    }         
     
    return MFC_Current_new;
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif 