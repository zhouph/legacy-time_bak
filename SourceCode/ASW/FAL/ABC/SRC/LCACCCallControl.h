#ifndef __LCACCCALLCONTROL_H__
#define __LCACCCALLCONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition**************************/
  #if __ACC
#define S8_ACC_STANDBY                  1
#define S8_ACC_ACTIVATION               2
#define S8_ACC_TERMINATION              3
  #endif

#define __ADC			                ENABLE
#define __ADC_G_TEST			             0
/*Global MACRO FUNCTION Definition**************************/

/*20120620 tunning paratemter */
/*
#define S16_ADC_TARGET_G_MAX_LIMIT              -40 
#define S16_ADC_TARGET_G_UPDATE_CYCLE            L_U8_TIME_10MSLOOP_100MS
#define S16_ADC_TARGET_G_JUMP_LIMIT              1
#define S16_ADC_TARGET_G_INIT_MAX               -5
#define S16_ACC_INITIAL_CURRENT                  400
#define S16_ACC_INITIAL_TARGET_V				 3000
#define S16_ADC_BRK_LAMP_ON_REF_TIMER            100
#define U8_ADC_FADE_OUT_LEVEL_ACCEL_REF          5
#define U8_ADC_FADE_OUT_LEVEL                    3
#define U8_ADC_FADE_OUT_LEVEL_ACCEL              3
*/

/*Global Type Declaration **********************************/

/*Global Extern Variable  Declaration***********************/
  #if  __ACC
extern struct  U8_BIT_STRUCT_t ACC00;

#define	lcu1AccSwitchFlg          ACC00.bit0
#define lcu1AccInhibitFlg         ACC00.bit1
#define lcu1AccActiveFlg          ACC00.bit2
#define lcu1AccCtrlReqIf      ACC00.bit3
#define lcu1AdcBrkLampOnReq      ACC00.bit4
#define lcu1Acc_not_used_0_5      ACC00.bit5
#define lcu1Acc_not_used_0_6      ACC00.bit6
#define lcu1Acc_not_used_0_7      ACC00.bit7

   #if defined(__ACC_RealDevice_INTERFACE)
/********** From */
extern uint8_t  ccu8_ACC420TargetDecel;
extern uint8_t  ccu8_ACC420DesiredEngineTorqueRate;
   #endif
/********** From */
extern uint8_t  Target_DECEL_G;
extern uint16_t   DECEL_CAN_SWITCH;
/********** To */

extern int8_t     lcs8AccState; 
extern uint8_t    lcu8AccEngCtrlReq, lcu8AccCtrlReq;
extern uint16_t   lcu16AccCalTorq;
extern int16_t    lcs16AccTargetG, lcs16AccTargetGOld, lcs16AdcTGdelaycycle, lcs16AccTargetGIf;
extern int16_t    lcs16AccBrkLampOnRefCnt;
 


  #endif

/*Global Extern Functions  Declaration**********************/
  #if __ACC
extern void LCACC_vCallControl(void);
  #endif
#endif