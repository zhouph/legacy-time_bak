/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LAABSCallActHW.C
* Description: Generation PWM duty of Wheel Valve for ABS LFC control
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAABSGenerateLFCDuty
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LAABSGenerateLFCDuty.h"
/*#include "LCABSDecideCtrlState.h"*/
#include "LCABSDecideCtrlVariables.h"
#include "LCABSStrokeRecovery.h"
#include "LDABSDctWheelStatus.h"
#include "LDABSDctVehicleStatus.h"
#include "LCABSCallESPCombination.h"
#include "LAESPCallActHW.h"
#include "LCHDCCallControl.h"
#include "LAHDCCallActHW.h"
#include "LCACCCallControl.h"
#include "LAACCCallActHW.h"
#include "LCEPBCallControl.h"
#include "LAEPBCallActHW.h"
#include "LCDECCallControl.h"
#include "LADECCallActHW.h"
#include "LCBDWCallControl.h"
#include "LCEBPCallControl.h"
#include "LAEPCCallActHW.h"
#include "LAESPActuateNcNovalveF.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"
#include "LCABSCallControl.h"
#include "LAABSCallHWTest.h"
#include "LACallMain.h"
#include "LCEBDCallControl.h"
#include "Hardware_Control.h"

#if __CBC
#include "LCCBCCallControl.h"
#endif
#if __SLS
#include "LCSLSCallControl.h"
#endif
#include "LCHRBCallControl.h"



#if (__IDB_LOGIC == DISABLE)/*Refer to EOL*/
/* Local Definiton  **********************************************************/

#define __UR_DUTY_COMP_IMPROVE  1

/* Variables Definition*******************************************************/
int16_t Filtered_ref_mon_ad;

 
   #if __VDC
uint8_t    R_ptc, R_stc, R_ps, R_ss;
uint8_t R_flno;
 #if(__HEV_MP_MOVING_AVG==ENABLE)
int16_t    laabss16MPAvg;
int16_t    laabss16MPbuffer[4] = {0,0,0,0};
int16_t    laabss16MPAvgBuffer[2] = {0,0};
uint8_t   laabsu8MPArrayCnt;/* mpress_counter; */
uint8_t   laabsu8MPAvgCnt;
 #endif
   #endif
 
WHEEL_VV_OUTPUT_t laabs_FL1HP,laabs_FL2HP,laabs_FR1HP,laabs_FR2HP;
WHEEL_VV_OUTPUT_t laabs_RL1HP,laabs_RL2HP,laabs_RR1HP,laabs_RR2HP;
 
/* LocalFunction prototype ***************************************************/
void LAABS_vGenerateLFCDuty(void);
void LAABS_vInitializeLFCVarsBFABS(struct W_STRUCT *WL);
void LAABS_vDecideLFCVars(struct W_STRUCT *WL);
void LAABS_vSetDumpScantime(struct W_STRUCT *WL);
uint8_t LCABS_u8GetDumpTimePerDumpCnt(uint8_t u8DumpCounter, uint8_t DumpTim1stDumpscan, uint8_t DumpTim23Dumpscan, uint8_t DumpTim45Dumpscan, uint8_t DumpTimAb5Dumpscan);
  #if __VDC && __MP_COMP
void LAABS_vSetDumpScantime1stCycle(struct W_STRUCT *WL);
  #endif
void LAABS_vCompensateLFCDuty(void);
  #if (NO_VV_ONOFF_CTRL==DISABLE)
void LAABS_vCompLFCCyclicCompDuty(struct W_STRUCT *WL);
  #endif
void LAABS_vCompLFCSpecialCompDuty(struct W_STRUCT *WL);
  #if __VDC && __MP_COMP
void LAABS_vCompLFCMPCyclicCompDuty(struct W_STRUCT *WL);
void LAABS_vCompLFCMPFluctuation(void);
void LAABS_vCompLFCMPFluctuationWL(struct W_STRUCT *WL);
    #if __WP_DROP_COMP
void LAABS_vCompLFCWPDrop(void);
void LAABS_vCompLFCWPDropWL(struct W_STRUCT *WL);
    #endif
  #endif
void LAABS_vLimitDiffofFrontLFCDuty(void);
void LAABS_vSyncBothWheelDuty(struct W_STRUCT *pW1, struct W_STRUCT *pW2);
void LAABS_vCalcLFCInitialDuty(struct W_STRUCT *WL);
void LAABS_vGenerateLFCDutyWL(struct W_STRUCT *WL);
  #if __CHANGE_MU
void LAABS_vLimitDiffofRearLFCDuty(void);
void LAABS_vLimitDiffofRearAtSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE);
void LAABS_vLimitDiffofRearAtnSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE);
  #endif
void LAABS_vCompCurrentAndRefV(void);
void LAABS_vCompCurrentAndRefVWL(struct W_STRUCT *WL);
void LAABS_vMonitorSolResistance(void);
/*  #if __VDC
void LAABS_vMonitorSolResistanceESP(void);
  #endif
void LAABS_vAllocPWMDutyArray(void);
void LAABS_vAllocPWMDutyArrayWL(struct W_STRUCT *WL);*/

  #if(__HEV_MP_MOVING_AVG==ENABLE)
void LAABS_vCalMPMovingAvg(void);
  #endif

#if (__MGH_80_10MS == ENABLE)
void LAABS_vInterfaceValveOutputs(const struct W_STRUCT*pActWL, WHEEL_VV_OUTPUT_t *laabsHP1, WHEEL_VV_OUTPUT_t *laabsHP2);
#endif

/*Global Extern Variable  Declaration*****************************************/
#if __BDW
extern struct   U8_BIT_STRUCT_t BDWC1;
#endif

#if __EBP
extern struct   U8_BIT_STRUCT_t EBPC1;
#endif

#if __ENABLE_PREFILL
extern struct   U8_BIT_STRUCT_t EPCC1;
#endif

/* GlobalFunction prototype **************************************************/



/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAABS_vGenerateLFCDuty
* CALLED BY:          LAABS_vCallActHW()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Main Routine of LFC for ABS control
******************************************************************************/
void LAABS_vGenerateLFCDuty(void)
{
    LAABS_vInitializeLFCVarsBFABS(&FL);
    LAABS_vInitializeLFCVarsBFABS(&FR);
    LAABS_vInitializeLFCVarsBFABS(&RL);
    LAABS_vInitializeLFCVarsBFABS(&RR);

#if __VDC
    LCABS_vRestoreLFCVariable();
#endif

    LAABS_vDecideLFCVars(&FL);
    LAABS_vDecideLFCVars(&FR);
    LAABS_vDecideLFCVars(&RL);
    LAABS_vDecideLFCVars(&RR);

    LAABS_vCompensateLFCDuty();

    LAABS_vLimitDiffofFrontLFCDuty();

#if __VDC
    LCABS_vRecallLFCVariable();
#endif

    LAABS_vCalcLFCInitialDuty(&FL);
    LAABS_vCalcLFCInitialDuty(&FR);
    LAABS_vCalcLFCInitialDuty(&RL);
    LAABS_vCalcLFCInitialDuty(&RR);

#if __VDC && __MP_COMP
    if(lcabsu1ValidBrakeSensor==1) {
      #if __WP_DROP_COMP && __PRESS_EST
        LAABS_vCompLFCWPDrop();
      #endif
    }
#endif

    LAABS_vGenerateLFCDutyWL(&FL);
    LAABS_vGenerateLFCDutyWL(&FR);

    LAABS_vGenerateLFCDutyWL(&RL);
    LAABS_vGenerateLFCDutyWL(&RR);

#if __CHANGE_MU
    #if __VDC
        if((L_SLIP_CONTROL_rl==0)&&(L_SLIP_CONTROL_rr==0)&&(ABS_fz==1)) {LAABS_vLimitDiffofRearLFCDuty();}
    #else
        if(ABS_fz==1) {LAABS_vLimitDiffofRearLFCDuty();}
    #endif
#endif

#if __VDC && __MP_COMP
    if(lcabsu1ValidBrakeSensor==1) {
/*      LAABS_vCompLFCMPFluctuation();      // 조건 ?? */
    }
#endif

/*#if __HW_TEST_MODE
	LAABS_vTestWheelNoValve();
#endif*/

 #if __CURRENT_CONTROL_IC
    FL.pwm_duty_temp_dash = FL.pwm_duty_temp;
    FR.pwm_duty_temp_dash = FR.pwm_duty_temp;
    RL.pwm_duty_temp_dash = RL.pwm_duty_temp;
    RR.pwm_duty_temp_dash = RR.pwm_duty_temp;
 #else
    LAABS_vCompCurrentAndRefV();
 #endif

  #if (__MGH_80_10MS == ENABLE)
    LAABS_vInterfaceValveOutputs(&FL,&laabs_FL1HP,&laabs_FL2HP);
    LAABS_vInterfaceValveOutputs(&FR,&laabs_FR1HP,&laabs_FR2HP);
    LAABS_vInterfaceValveOutputs(&RL,&laabs_RL1HP,&laabs_RL2HP);
    LAABS_vInterfaceValveOutputs(&RR,&laabs_RR1HP,&laabs_RR2HP);
  #endif
	
	if(ABS_fz==1)
	{
		la_FL1HP = laabs_FL1HP; 
		la_FL2HP = laabs_FL2HP; 
		la_FR1HP = laabs_FR1HP; 
		la_FR2HP = laabs_FR2HP;
	}
	else
	{
		;
	}
	
	if((ABS_fz==1) || (EBD_RA==1))
	{
		la_RL1HP = laabs_RL1HP; 
		la_RL2HP = laabs_RL2HP; 
		la_RR1HP = laabs_RR1HP; 
		la_RR2HP = laabs_RR2HP;
	}
	else
	{
		;
	}

}

/*****************************************************************************/
void LAABS_vInitializeLFCVarsBFABS(struct W_STRUCT *WL)
{

    if((FSF_wl==1)&&(ABS_fz==0)) {
        WL->Holding_LFC_duty=0;                                                 /* 1228. msl control for rear wheel */
        WL->LFC_reapply_end_duty=0;
        WL->LFC_reapply_current_duty=0;

/*----------------------------------------------------------------------------Be Careful!!!!!! */
        WL->pwm_temp=0;
        WL->pwm_duty_temp=0;
        WL->pwm_duty_temp_dash=0;
/*----------------------------------------------------------------------------Be Careful!!!!!! */

        WL->MSL_long_hold_flag=0;

#if __CHANGE_MU
        WL->Duty_Limitation_flag=0;
#endif

#if __VDC && __WP_DROP_COMP
        WL->Estimated_Press_init=0;
        WL->Estimated_Press_end=0;
        WL->Drop_Press_Comp_T=0;
        WL->WP_Drop_comp_flag=0;
        WL->LFC_WP_Drop_comp_duty=0;
#endif

#if __VDC
/*      WL->LFC_to_ESP_flag=0; */
/*      WL->CROSS_SLIP_CONTROL=0; */
        WL->ESP_partial_dump_counter=0;
        WL->Forced_n_th_deep_slip_comp_flag = 0;
      #if __ESP_ABS_COOPERATION_CONTROL
        if(REAR_WHEEL_wl==1) {WL->ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;}
        else {WL->ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;}
      #endif
#endif

    }
}

/*****************************************************************************/
void LAABS_vDecideLFCVars(struct W_STRUCT *WL)
{

    WL->Holding_LFC_duty=0;                            /* 1228. msl control for rear wheel */

#if __VDC
    if((L_SLIP_CONTROL_wl!=1)&&(BTC_fz!=1)) {
#elif __BTC
    if(BTC_fz!=1) {
#endif

/*  @@@ Start of LFC Built Reapply Algorithm */
/*    WL->pwm_reapply_flag=0; */

    if((BUILT_wl==1) && (ABS_wl==1)) {      /* 03 USA High Mu */
      #if __CHANGE_MU
        if(REAR_WHEEL_wl==1) {WL->Duty_Limitation_flag=0;}
      #endif
    }

/*   @@@ End of LFC Built Reapply Algorithm */

    else {        /* BUILT_wl==0이면 */

        if(HV_VL_wl==1) {
            if(AV_VL_wl==1) {   /***************** Dump! **************************************/
                if(REAR_WHEEL_wl==0) {
                    if(LEFT_WHEEL_wl==1) {LAABS_vSetDumpScantime(&FL);}
                    else {LAABS_vSetDumpScantime(&FR);}
                }
                else {
                    if(LEFT_WHEEL_wl==1) {LAABS_vSetDumpScantime(&RL);}
                    else {LAABS_vSetDumpScantime(&RR);}
                }

             #if __REORGANIZE_ABS_FOR_MGH60
             
                if((STAB_A_wl==1)||(WL->SL_Dump_Start==1)) {          /* The end of stabil(starting dump point) */
                    if(WL->LFC_s0_reapply_counter_old >=1) {
		                if((REAR_WHEEL_wl==1) || (WL->LFC_fictitious_cycle_flag2==0))
		                {
                            WL->LFC_reapply_end_duty=WL->LFC_reapply_current_duty;
                        }
                    }
                }
              #if __CHANGE_MU
                WL->Duty_Limitation_flag=0;
              #endif
                
             #else

                if(REAR_WHEEL_wl==0) {          /* front wheel */
                    if((STAB_A_wl==1)||(WL->SL_Dump_Start==1)) {          /* The end of stabil(starting dump point) */
                        if(WL->LFC_s0_reapply_counter_old >=1) {
                            if ( WL->LFC_fictitious_cycle_flag2 == 0 ){
                                WL->LFC_reapply_end_duty=WL->LFC_reapply_current_duty;
                            }
                        }
                    }
                }
                else {                          /* rear wheel */
                    if((STAB_A_wl==1)||(WL->SL_Dump_Start==1)) {          /* The end of stabil(starting dump point) */
                        if(WL->LFC_s0_reapply_counter_old >=1) {
                            WL->LFC_reapply_end_duty=WL->LFC_reapply_current_duty;
                        }
                    }
                  #if __CHANGE_MU
                    WL->Duty_Limitation_flag=0;
                  #endif
                }
                
              #endif  
            }

            else {      /******************* Hold!!! ***********************************/

/*              WL->on_time_inlet_pwm=7;    // For STD Algorithm */

                if(WL->LFC_built_reapply_flag==0) { /* Not Built -> Hold !!!, // std hold에 따라 실제로 hold가 실행됨. */
                  #if (L_CONTROL_PERIOD>=2)
                    if(WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_01)
                    {
                        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -U8_BASE_CTRLTIME);
                    }
                    else
                    {
                        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -(U8_BASE_CTRLTIME*2));
                    }
                  #else
                  	WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -U8_BASE_CTRLTIME);
                  #endif
                    if(WL->s_diff>0) {WL->on_time_outlet_pwm=0;}

                }
                else {                  /* Built -> Hold !!! */
                      #if __CHANGE_MU
                    if((REAR_WHEEL_wl==1)&&(WL->MSL_BASE==1)) {WL->Duty_Limitation_flag=0;}
                      #endif
                    if(WL->Hold_Duty_Comp_flag==0)  {                       /* '03 USA High Mu */
                      #if __CHANGE_MU
                        if(REAR_WHEEL_wl==0) {                  /* built중 hold time을 잼.(y.w.shin) */
                            WL->Holding_LFC_duty=1;
                        }
                        else {
                            if(WL->Duty_Limitation_flag==0) {
                                WL->Holding_LFC_duty=1;
                            }
                        }
                      #else
                        WL->Holding_LFC_duty=1;
                      #endif
                    }
                }                                       /* built후 hold시 std hold는 수행하지 */
                                                        /* 않고  LFC_built_reapply_counter만 증가됨. */
            }

        }

        else {              /****************** Reapply!! ****************************/
/*            WL->on_time_inlet_pwm= WL->on_time_outlet_pwm= 0; */
            WL->on_time_outlet_pwm= 0;           /* '04 SW Winter for Code Optimization */
            if(ABS_wl==1) {
/*
                if(WL->increase_p_time !=0) {
                    WL->on_time_inlet_pwm = 7;  // For STD Algorithm
                    WL->pwm_reapply_flag=1;
                }
*/
            }
/*
          #if __EBD_LFC_MODE
            else if(WL->EBD_LFC_RISE_MODE==1) {WL->on_time_inlet_pwm = 7;}
          #endif
*/
            else { }
        }
    }


/*    if(WL->pwm_reapply_flag==1) {   */                                            /* outside abs일때와 increase_p_time !=0 */
                                                                                /*일때의 pwm_reapply_flag==1를 어떻게 구분? */
/*        WL->on_time_inlet_pwm= WL->on_time_outlet_pwm= WL->on_time_special_pwm= 0; */
            /* @@@STD Loutine 처음의 위치를 옮김. */
/*

          #if __CHANGE_MU
        if(REAR_WHEEL_wl==1) {WL->Duty_Limitation_flag=0;}
          #endif
    }
*/


    if(WL->LFC_s0_reapply_counter >=1) {
        if(WL->Holding_LFC_duty ==1) {                                          /*1228. start */
          #if __REORGANIZE_ABS_FOR_MGH60	
        	if(WL->timer_keeping_LFC_duty < 200) {WL->timer_keeping_LFC_duty = WL->timer_keeping_LFC_duty + L_1SCAN;}
          #else	
            WL->timer_keeping_LFC_duty = timer_keeping_LFC_duty + L_1SCAN;

            if(WL->timer_keeping_LFC_duty >=(uint8_t)(WL->hold_timer_new_ref2+L_1SCAN)) {
                if(WL->timer_keeping_LFC_duty > 200) {WL->timer_keeping_LFC_duty  = timer_keeping_LFC_duty - L_1SCAN;} /* added on 2002.winter */
            }
          #endif  
        }
        else {
            WL->timer_keeping_LFC_duty =0;
        }
    }

#if __VDC || __BTC
    }
#endif

}


void LAABS_vSetDumpScantime(struct W_STRUCT *WL)
{
    uint8_t Inhibit_Increase_Dumpscantime;
    /*uint8_t temp_diff_afz, temp_inc_dumptime;*/
    int16_t   DumpScanRefMu;
    
    DumpScanRefMu=afz;

    Inhibit_Increase_Dumpscantime = 0;
    WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;

    if((WL->LFC_UNSTABIL_counter>=L_TIME_170MS) && (WL->LFC_dump_counter>=8)) {
        if(WL->peak_acc<ARAD_8G0) {WL->Jump_Mu_Cascading_sus_flag = 1;}
        else {WL->Jump_Mu_Cascading_sus_flag = 0;}
    }
    else {WL->Jump_Mu_Cascading_sus_flag = 0;}

       #if __VDC && __MP_COMP
    if(((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1))
      && ((FSF_wl==1) || (WL->Jump_Mu_Cascading_sus_flag==1)))
    {
        if(REAR_WHEEL_wl==0) {
            if(LEFT_WHEEL_wl==1) {LAABS_vSetDumpScantime1stCycle(&FL);}
            else {LAABS_vSetDumpScantime1stCycle(&FR);}
        }
        else {
            if(LEFT_WHEEL_wl==1) {LAABS_vSetDumpScantime1stCycle(&RL);}
            else {LAABS_vSetDumpScantime1stCycle(&RR);}
        }
    }
    else {
   #endif

    if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1) || (Rough_road_detect_vehicle==1))
    {
        Inhibit_Increase_Dumpscantime = 1;
    }
                                 /* 020927 제동거리 */
    if(REAR_WHEEL_wl==1) {
      #if __INCREASE_SP_LOW_REAR_DUMPTIM
        if((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
           &&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)        
        )
        {
            if(U8_SP_LOW_RER_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)
            {
                DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MU;
            }
            else if(U8_SP_LOW_RER_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)
            {
                DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MED_MU;
            }
            else { } /*afz*/
        }
      #endif /*__INCREASE_SP_LOW_REAR_DUMPTIM*/

        if((AFZ_OK==1) && (afz<-(int16_t)U8_HIGH_MU)) {
            if(vref >= (int16_t)VREF_20_KPH) {
                if(STAB_A_wl==1) {
                    if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LHDF_R;} /* 7 */
                    else if(WL->High_dump_factor<=(2000)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_MHDF_R;} /* 9 */
                    else {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_HHDF_R;}
                }
                else { }
            }
            else
            {
                if(STAB_A_wl==1) {
                    if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_LHDF_B_20_R;}
                    else                             {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_B_20_R;}
                }
                else { }
            }
        }
        else if ((AFZ_OK==1) && (DumpScanRefMu>=-(int16_t)U8_PULSE_LOW_MED_MU))
        {
            if(vref >= (int16_t)VREF_10_KPH) {
                if(DumpScanRefMu>=-(int16_t)U8_PULSE_LOW_MU)
                {
                    WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMu_R,U8_2nd3rd_Dumpscantime_LMu_R,U8_4th5th_Dumpscantime_LMu_R,U8_nth_Dumpscantime_LMu_R);
                }
                else
                {
                    WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_R,U8_2nd3rd_Dumpscantime_LMMu_R,U8_4th5th_Dumpscantime_LMMu_R,U8_nth_Dumpscantime_LMMu_R);
                }

                if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
                else if((DumpScanRefMu<-(int16_t)U8_PULSE_LOW_MU) && (Inhibit_Increase_Dumpscantime==1))
                {
                    WL->on_time_outlet_pwm = 7;
                }
                else { }
            }
            else {
                if(afz>=-(int16_t)U8_PULSE_LOW_MU) {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMu_R;}
                else {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMMu_R;}
            }
        }

        else if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_MEDIUM_MU) && (Inhibit_Increase_Dumpscantime==0))
        {
            if(vref >= (int16_t)VREF_20_KPH)
            {
                if(WL->LFC_dump_counter==1)
                {
                    WL->on_time_outlet_pwm = U8_1st_Dumpscantime_MMu_R;
                }
                else
                {
                    WL->on_time_outlet_pwm = U8_nth_Dumpscantime_MMu_R;
                }
            }
            else
            {
                WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_MMu_R;
            }
        }
        else if(EBD_wl==1)
        {
            if(vref >= (int16_t)VREF_20_KPH)
            {
                if(WL->EBD_Dump_counter<=1)
                {
                    WL->on_time_outlet_pwm = U8_1st_Dumpscantime_EBD_R;
                }
                else
                {
                    WL->on_time_outlet_pwm = U8_nth_Dumpscantime_EBD_R;
                }
            }
            else { }
        }
        else 
        {
            ;
        }
    }
    else {
      #if __INCREASE_SP_LOW_FRONT_DUMPTIM
        if((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
           &&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)        
        )
        {
            if(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)
            {
                DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MU;
            }
            else if(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)
            {
                DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MED_MU;
            }
            else { } /*afz*/
        }
      #endif

        if((AFZ_OK==1) && (afz<-(int16_t)U8_HIGH_MU)) {
            if(vref >= (int16_t)VREF_20_KPH) {
                if(STAB_A_wl==1) {
                    if(WL->High_dump_factor<=(1000))      {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LHDF_F;}
                    else if(WL->High_dump_factor<=(1500)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LMHDF_F;}
                    else if(WL->High_dump_factor<=(2500)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_MHDF_F;}
                    else {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_HHDF_F;}
                }
                else if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = 5;}
                else { }
            }
            else
            {
                if(STAB_A_wl==1) {
                    if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_LHDF_B_20_F;}
                    else                             {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_B_20_F;}
                }
                else { }
            }
        }
        else if(!((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==0)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==1)))&&(WL->PULSE_DOWN_Forbid_flag==0)))
        {
          #if   __REAR_D && ((__CAR != GM_TAHOE) ||(__CAR !=GM_SILVERADO))
            if((WL->LFC_UNSTABIL_counter_old>=U8_UnstableTim_for_IncDumptime)&&(WL->peak_slip_old >= -S8_MaxSlipForIncFrontDumpT_RWD)&&(WL->LFC_dump_counter_old>=U8_DumpRefForIncFrontDumpT_RWD)&&(Inhibit_Increase_Dumpscantime==0)) {
          #else
            if((WL->LFC_UNSTABIL_counter_old>=U8_UnstableTim_for_IncDumptime)&&(Inhibit_Increase_Dumpscantime==0)) {
          #endif
                if(((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MU)) 
                    #if __INCREASE_SP_LOW_FRONT_DUMPTIM
                  || ((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
                      &&(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU))
                    #endif
                )
                {
                    if(vref >= (int16_t)VREF_10_KPH) {
                        WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMu_F,U8_2nd3rd_Dumpscantime_LMu_F,U8_4th5th_Dumpscantime_LMu_F,U8_nth_Dumpscantime_LMu_F);

                        if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
                    }
                    else {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMu_F;}
                }

                else if(((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MED_MU)) 
                    #if __INCREASE_SP_LOW_FRONT_DUMPTIM
                  || ((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
                      &&(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU))
                    #endif
                )
                {
                    if(vref >= (int16_t)VREF_20_KPH) {
                        if(afz>=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))
                        {
                            WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_F,U8_2nd3rd_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F);
                        }
                        else
                        {
                            WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F);
                        }
                        /*
                        if(WL->LFC_dump_counter==1) {
                            WL->on_time_outlet_pwm = U8_1st_Dumpscantime_LMMu_F; 
                        }
                        else if((afz>=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))&&(WL->LFC_dump_counter<=3)) {
                            WL->on_time_outlet_pwm = U8_2nd3rd_Dumpscantime_LMMu_F;
                        }
                        else {WL->on_time_outlet_pwm = U8_nth_Dumpscantime_LMMu_F;}
                        */
                        if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
                    }
                    else {WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_LMMu_F;}
                }
                else if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_MEDIUM_MU))
                {
                    if(vref >= (int16_t)VREF_20_KPH)
                    {
                        if(WL->LFC_dump_counter==1)
                        {
                            WL->on_time_outlet_pwm = U8_1st_Dumpscantime_MMu_F;
                        }
                        else
                        {
                            WL->on_time_outlet_pwm = U8_nth_Dumpscantime_MMu_F;
                        }
                    }
                    else
                    {
                        WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_MMu_F;
                    }
                }
                else { }
            }
            else { }
        }
      #if __STABLE_DUMPHOLD_ENABLE
        else if(WL->lcabsu1StabDump == 1)
        {
            if((AFZ_OK==1) && (afz <= -(int16_t)U8_PULSE_UNDER_LOW_MED_MU))
            {
                WL->on_time_outlet_pwm = U8_STABDUMP_SCANTIME_F;
            }
            else {/*default*/}
        }
      #endif
       #if __VDC && __ESP_SPLIT_2
        else if(WL->GMA_ESP_Control==1) {WL->on_time_outlet_pwm = 5;}
       #endif
        else { }
    }
  #if __VDC && __MP_COMP
    }
  #endif

  #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)  
    if(WL->lcabsu1CornerRearOutCtrl==1)
    {
    	if(WL->LFC_dump_counter==1)
    	{
    	    if(WL->on_time_outlet_pwm<21)
    	    {	
    		    WL->on_time_outlet_pwm = 21;
    	    }
    	    else{}
    	}
    	else if(WL->LFC_dump_counter==2)
    	{
    		if(WL->on_time_outlet_pwm<14)
    	    {	
    		    WL->on_time_outlet_pwm = 14;
    	    }
    	    else{}
    	} 
    	else{}
    }
    else{}
  #endif  	

  #if __PRESS_EST_ABS
    if((WL->LOW_MU_SUSPECT_by_IndexMu==1) && (LOW_to_HIGH_suspect==0))
    {
        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)(WL->on_time_outlet_pwm*2),21,35);
    }
    else
    {
        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->on_time_outlet_pwm,3,28);
    }
  #else
    WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->on_time_outlet_pwm,3,28);
  #endif
}

uint8_t LCABS_u8GetDumpTimePerDumpCnt(uint8_t u8DumpCounter, uint8_t DumpTim1stDumpscan, uint8_t DumpTim23Dumpscan, uint8_t DumpTim45Dumpscan, uint8_t DumpTimAb5Dumpscan)
{
    if(u8DumpCounter==1)
    {
        return DumpTim1stDumpscan;
    }
    else if(u8DumpCounter<=3)
    {
        return DumpTim23Dumpscan;
    }
    else if(u8DumpCounter<=5)
    {
        return DumpTim45Dumpscan;
    }
    else
    {
        return DumpTimAb5Dumpscan;
    }
}


#if __VDC && __MP_COMP
void LAABS_vSetDumpScantime1stCycle(struct W_STRUCT *WL)
{
    if(STABIL_wl==0) {
        if(WL->s16_Estimated_Active_Press<=S16_WPThres_for_IncDumptim_1st)
        {
            if(WL->s16_Estimated_Active_Press<=MPRESS_5BAR) {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_below_5bar;}
                else {tempW1 = Press_Dec_gain_Rear_below_5bar;}
                tempW4 = MPRESS_1BAR;
            }
            else if(WL->s16_Estimated_Active_Press<=MPRESS_10BAR) {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_5_10bar;}
                else {tempW1 = Press_Dec_gain_Rear_5_10bar;}
                tempW4 = MPRESS_3BAR;
            }
            else {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_10_70bar;}
                else {tempW1 = Press_Dec_gain_Rear_10_70bar;}
                tempW4 = MPRESS_2BAR + (WL->s16_Estimated_Active_Press/10);
            }

            tempW0 = WL->s16_Estimated_Active_Press - tempW4;
            if(tempW0<=0) {tempW0 = 0;}
            tempW2 = tempW1 * (int16_t)(LDABS_u16FitSQRT((uint16_t)tempW0));
            tempW3 = tempW2/4;

            if(tempW3<1){tempW3=1;} /* Prevent division by zero */
            tempW5 = (70*7)/tempW3; /* MPRESS_7BAR = 70; */

            if(tempW5<=5) {tempW5 = 5;}
            else if(tempW5>=21) {tempW5 = 21;}
            else { }

            WL->on_time_outlet_pwm = (uint8_t)tempW5;
        }
        else  {WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;}
    }
    else {WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;}
}
#endif

void LAABS_vCompensateLFCDuty(void)
{
  #if (NO_VV_ONOFF_CTRL==DISABLE)
    LAABS_vCompLFCCyclicCompDuty(&FL);
    LAABS_vCompLFCCyclicCompDuty(&FR);
    LAABS_vCompLFCCyclicCompDuty(&RL);
    LAABS_vCompLFCCyclicCompDuty(&RR);
  #endif

    LAABS_vCompLFCSpecialCompDuty(&FL);
    LAABS_vCompLFCSpecialCompDuty(&FR);
    LAABS_vCompLFCSpecialCompDuty(&RL);
    LAABS_vCompLFCSpecialCompDuty(&RR);

#if __VDC && __MP_COMP
    if(lcabsu1ValidBrakeSensor==1) {
        LAABS_vCompLFCMPCyclicCompDuty(&FL);
        LAABS_vCompLFCMPCyclicCompDuty(&FR);
        LAABS_vCompLFCMPCyclicCompDuty(&RL);
        LAABS_vCompLFCMPCyclicCompDuty(&RR);
    }
#endif

}

/*------------------------------------------------------------------------------------- */
  #if (NO_VV_ONOFF_CTRL==DISABLE)
void LAABS_vCompLFCCyclicCompDuty(struct W_STRUCT *WL)
{

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_pres_rise_delay_comp = 0;
    }
    else if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1SCAN)) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
        if((WL->LFC_zyklus_z<=1) || (WL->LFC_fictitious_cycle_flag==1)) {WL->LFC_pres_rise_delay_comp = 0;}
        else if(WL->Check_double_brake_flag==1) {WL->LFC_pres_rise_delay_comp = 0;}
        else if(WL->LFC_Cyclic_Comp_flag==1) {

            tempW0=WL->LFC_built_reapply_counter_delta/((int16_t)WL->hold_timer_new_ref_old+L_1SCAN);

            if(tempW0 > 0)
            {
                if(WL->LFC_zyklus_z==2){
                    if(tempW0 > 10) {tempW0 = 10;}
                }
			  #if (__ROUGH_COMP_IMPROVE==ENABLE)
                else if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1)){
                    if(tempW0 > 10) {tempW0 = 10;}
                }
           	  #endif
                else {
                    if(tempW0 > 5) {tempW0 = 5;}
                }
            }
            else
            {
                if(vref<=VREF_20_KPH) {
                    tempW0 = tempW0/2;
                }

              #if __UNSTABLE_REAPPLY_ENABLE
                if(WL->Reapply_Accel_cycle_old==1) {
                    if(tempW0 < -1) {tempW0 = -1;}
                }
                else
              #endif
                {
                    if(tempW0 < -4) {tempW0 = -4;}
                }
            }

            WL->LFC_pres_rise_delay_comp = WL->LFC_pres_rise_delay_comp + tempW0;
        }
        else { }
      #if   __VDC && __ABS_COMB
        }
      #endif
    }
    else { }
}
  #endif/*  #if (__IDB_LOGIC==DISABLE)*/


  #if (NO_VV_ONOFF_CTRL==DISABLE)
void LAABS_vCompLFCSpecialCompDuty(struct W_STRUCT *WL)
{

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_special_comp_duty = 0;
        WL->lau1SPHighSideCompFlag = 0;
    }
    else if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
        if(WL->Check_double_brake_flag==1) {
            WL->LFC_special_comp_duty = 0;
        }
        if((WL->LFC_zyklus_z==1) || (WL->Check_double_brake_flag==1)) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1))
            {
                ;
            }
            else
            {
          #endif
            if(WL->LFC_initial_deep_slip_comp_flag==1) {
                if(REAR_WHEEL_wl==0) {
                    WL->LFC_special_comp_duty = (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                }
                else {
                    WL->LFC_special_comp_duty = (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                }
            }
          #if (__VDC && __MP_COMP && __PRESS_EST)
           }
          #endif
        	if(WL->LFC_special_comp_duty==0)
        	{
            	if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
            	{
            		if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
            		{
                		WL->LFC_special_comp_duty = (int16_t)S8_Comp_Duty_For_SPHside_F;
            		}
            	}
        	}

      #if   __VDC && __ABS_COMB
        }
      #endif
            WL->Check_double_brake_flag=0;
        }
        else if(WL->LFC_zyklus_z>=2) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1)) {
                if(WL->LFC_big_rise_and_dump_flag==1) {
                    if(lcabss16RefMpress>=(WL->MP_Init+MPRESS_20BAR)) {	/* AHBGen3 MP Fluctuation */
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_BIG_RISE_DMP_COMP_DUTY;
                    }
                }
            }
            else {
          #endif
                if(WL->LFC_n_th_deep_slip_comp_flag==1) {
                    if((WL->LFC_zyklus_z==2) && (WL->LFC_initial_deep_slip_comp_flag==1)) {     /* '04 SW Winter test */
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_2ND_CYC_COMP_LOW;
                    }
                    else if(REAR_WHEEL_wl==0) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Jump_Down_Comp_F;
                    }
                    else {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Jump_Down_Comp_R;
                    }
                }
                else {
                    if(WL->LFC_big_rise_and_dump_flag==1) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_BIG_RISE_DMP_COMP_DUTY;
                    }
                    else if(WL->LFC_zyklus_z<=(U8_IN_GEAR_detect_cycle+1)) {
                      #if __INGEAR_VIB  /**************************************************Front Wheel Drive */
                       #if __REORGANIZE_ABS_FOR_MGH60
                          #if (__REAR_D==ENABLE)
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==1)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(RL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(RR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                            else { }                            	
                        }
                          #else
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==0)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(FL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(FR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                            else { }                            	
                        }
                          #endif/*(__REAR_D==ENABLE)*/
                       #else
                        if(REAR_WHEEL_wl==0) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(WL->In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                        }
                        else if(LEFT_WHEEL_wl==1) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FL.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                        else {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FR.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                       #endif/*#if __REORGANIZE_ABS_FOR_MGH60*/
                      #endif            /********************************************************************/
                    }
                    else { }
                }
          #if (__VDC && __MP_COMP && __PRESS_EST)
            }
          #endif

          #if __RESONANCE       /***************************************************Rezzo Resonance */
            if(WL->Shift_thresdec_flag==1) {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)5;
            }
          #endif                /********************************************************************/

      #if   __VDC && __ABS_COMB
        }
      #endif
        }
        else { }
    }
    else { }

    if(LOW_to_HIGH_suspect_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_L2H_Comp_F;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)	
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_L2H_Comp_R;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)	
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
        }
    }
    else if(LOW_to_HIGH_suspect2_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
          #endif
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
          #endif
        }
    }
  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    else if(lcabsu1L2hSuspectByWhPres_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  + (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
        }    	
    }
  #endif
    else { }

  /***********************************************************************************************/
  /* Move from LCABS_vDctLFCMuChangeWL()                                                         */
  /***********************************************************************************************/
    if((REAR_WHEEL_wl==0) && (LFC_H_to_Split_flag==1) && (WL->Front_SL_dump_flag_K==1))
    {
        if(( U8_F_High_side_SL_dump>0 )&&(WL->lau1SPHighSideCompFlag==0))
        {
            if(S8_Comp_Duty_For_SPHside_F>=(int8_t)(U8_F_High_side_SL_dump*2))
            {
	            WL->LFC_special_comp_duty=WL->LFC_special_comp_duty+(int16_t)(U8_F_High_side_SL_dump*2);
	        }
            else
            {
                WL->LFC_special_comp_duty=WL->LFC_special_comp_duty+(int16_t)S8_Comp_Duty_For_SPHside_F;
            }
            WL->lau1SPHighSideCompFlag = 1;
        }
        else{}
    }
  /***********************************************************************************************/
}
  #else
void LAABS_vCompLFCSpecialCompDuty(struct W_STRUCT *WL)
{
    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_special_comp_duty = 0;
        WL->lau1SPHighSideCompFlag = 0;
    }
    else if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
        if(WL->Check_double_brake_flag==1) {
            WL->LFC_special_comp_duty = 0;
        }
        if((WL->LFC_zyklus_z==1) || (WL->Check_double_brake_flag==1)) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1))
            {
                ;
            }
            else
            {
          #endif
            if(WL->LFC_initial_deep_slip_comp_flag==1) {
                if(REAR_WHEEL_wl==0) {
                    WL->LFC_special_comp_duty = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                }
                else {
                    WL->LFC_special_comp_duty = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                }
            }
          #if (__VDC && __MP_COMP && __PRESS_EST)
           }
          #endif
        	if(WL->LFC_special_comp_duty==0)
        	{
            	if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
            	{
            		if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
            		{
                		WL->LFC_special_comp_duty = -(int16_t)S8_Comp_Duty_For_SPHside_F;
            		}
            	}
        	}

      #if   __VDC && __ABS_COMB
        }
      #endif
            WL->Check_double_brake_flag=0;
        }
        else if(WL->LFC_zyklus_z>=2) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1)) {
                if(WL->LFC_big_rise_and_dump_flag==1) {
                    if(lcabss16RefMpress>=(WL->MP_Init+MPRESS_20BAR)) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - 3; /* consider redundunt comp */
                    }
                }
            }
            else {
          #endif
                if(WL->LFC_n_th_deep_slip_comp_flag==1) {
                    if((WL->LFC_zyklus_z==2) && (WL->LFC_initial_deep_slip_comp_flag==1)) {     /* '04 SW Winter test */
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_2ND_CYC_COMP_LOW;
                    }
                    else if(REAR_WHEEL_wl==0) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Jump_Down_Comp_F;
                    }
                    else {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Jump_Down_Comp_R;
                    }
                }
                else {
                    if(WL->LFC_big_rise_and_dump_flag==1) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_BIG_RISE_DMP_COMP_DUTY;
                    }
                    else if(WL->LFC_zyklus_z<=(U8_IN_GEAR_detect_cycle+1)) {
                      #if __INGEAR_VIB  /**************************************************Front Wheel Drive */
                       #if __REORGANIZE_ABS_FOR_MGH60
                          #if (__REAR_D==ENABLE)
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==1)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(RL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(RR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                            else { }                            	
                        }
                          #else
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==0)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(FL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(FR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                            else { }                            	
                        }
                          #endif/*(__REAR_D==ENABLE)*/
                       #else
                        if(REAR_WHEEL_wl==0) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(WL->In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                        }
                        else if(LEFT_WHEEL_wl==1) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FL.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                        else {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FR.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                       #endif/*#if __REORGANIZE_ABS_FOR_MGH60*/
                      #endif            /********************************************************************/
                    }
                    else { }
                }
          #if (__VDC && __MP_COMP && __PRESS_EST)
            }
          #endif

          #if __RESONANCE       /***************************************************Rezzo Resonance */
            if(WL->Shift_thresdec_flag==1) {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)5;
            }
          #endif                /********************************************************************/

      #if   __VDC && __ABS_COMB
        }
      #endif
        }
        else { }
    }
    else { }

    if(LOW_to_HIGH_suspect_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Comp_F;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)	
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Comp_R;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)	
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R);
            }
        }
    }
    else if(LOW_to_HIGH_suspect2_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
          #endif
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)(U8_LFC_L2H_Suspect_Comp_R);
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R);
            }
          #endif
        }
    }
  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    else if(lcabsu1L2hSuspectByWhPres_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)(U8_LFC_L2H_Suspect_Comp_R);
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R);
        	}
    	}
    }
  #endif    
    else { }

  /***********************************************************************************************/
  /* Move from LCABS_vDctLFCMuChangeWL()                                                         */
  /***********************************************************************************************/
    if((REAR_WHEEL_wl==0) && (LFC_H_to_Split_flag==1) && (WL->Front_SL_dump_flag_K==1))
    {
        if(( U8_F_High_side_SL_dump>0 )&&(WL->lau1SPHighSideCompFlag==0))
        {
            if(S8_Comp_Duty_For_SPHside_F>=(int8_t)(U8_F_High_side_SL_dump*2))
            {
	            WL->LFC_special_comp_duty=WL->LFC_special_comp_duty-(int16_t)(U8_F_High_side_SL_dump*2);
	        }
            else
            {
                WL->LFC_special_comp_duty=WL->LFC_special_comp_duty-(int16_t)S8_Comp_Duty_For_SPHside_F;
            }
            WL->lau1SPHighSideCompFlag = 1;
        }
        else{}
    }
  /***********************************************************************************************/
}
  #endif/*#if (__IDB_LOGIC==DISABLE)*/


#if __VDC && __MP_COMP
  #if (__IDB_LOGIC==DISABLE)/*MP/dP compensation for NO valve LFC*/
void LAABS_vCompLFCMPCyclicCompDuty(struct W_STRUCT *WL)
{
   /*uint8_t CYCLIC_COMP_TIMING;*/
    int16_t  	s16CompDutyForMPVariation;
    int16_t 	s16CompDutyForDiffFromHMuSkid;
    int16_t		s16MasterPress, s16DPDutyCompRef_HighMuSkid;
    int16_t		s16DPDutyCompRef_MPVariation, s16DPDutyCompRef_PDiffFromSkid;

    /*
    uint8_t DutyCompForMPVariation;
    uint8_t DutyCompForDiffFromRef;
    int16_t ReferenceWLPress;

    if(REAR_WHEEL_wl==0)
    {
        DutyCompForMPVariation =U8_DutyCompForMPVariationF;
        DutyCompForDiffFromRef =U8_DutyCompForDiffFromRefF;
        ReferenceWLPress       =S16_ReferenceWLPressF     ;
    } 
    else
    {
        DutyCompForMPVariation =U8_DutyCompForMPVariationR;
        DutyCompForDiffFromRef =U8_DutyCompForDiffFromRefR;
        ReferenceWLPress       =S16_ReferenceWLPressR     ;
    }
    */

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_MP_Cyclic_comp_duty = 0;
        WL->MP_Init = S16_ReferenceMCPress;
      #if __UNSTABLE_REAPPLY_ENABLE
        WL->Unstable_Rise_MP_comp_duty = 0;
     #endif
    }

  #if(__HEV_MP_MOVING_AVG==ENABLE)
    LAABS_vCalMPMovingAvg();
    s16MasterPress = laabss16MPAvg;
  #else
    s16MasterPress = lcabss16RefMpress; /* AHBGen3 MP Fluctuation */
  #endif

  #if (__FRONT_REAR_MP_COMP_SEPERATE == ENABLE)
  	if(REAR_WHEEL_wl == 0)
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariation;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkid;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkid*MPRESS_1BAR;
  	}
  	else
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariationR;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkidR;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkidR*MPRESS_1BAR;
  	}
  #else
	s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariation;
	s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkid;
	s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkid*MPRESS_1BAR;
  #endif/* (__FRONT_REAR_MP_COMP_SEPERATE == ENABLE)*/

	s16CompDutyForMPVariation =  ((s16MasterPress - WL->MP_Init)/s16DPDutyCompRef_MPVariation);
	s16CompDutyForDiffFromHMuSkid = ((s16DPDutyCompRef_HighMuSkid - WL->s16_Estimated_Active_Press)/s16DPDutyCompRef_PDiffFromSkid);

  #if   __VDC && __ABS_COMB
    if(WL->L_SLIP_CONTROL==0) {
  #endif

    if((WL->LFC_built_reapply_flag==1) && (ABS_DURING_MPRESS_FLG==1)) {
        if(WL->LFC_built_reapply_counter==L_1ST_SCAN) {

            tempW7 = 0;

          #if !__PRESS_EST
            tempW7 = s16CompDutyForMPVariation;
          #else
            if(WL->LFC_zyklus_z<=1) {
                if(REAR_WHEEL_wl==0) {
                  #if   __BUMP_SUSPECT
                    if(WL->LFC_Bump_Suspect_flag==1) {
                        tempW7 = s16CompDutyForMPVariation + (s16CompDutyForDiffFromHMuSkid/3);
                        if(tempW7 >= 8) {tempW7 = 8;}
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                    }
                  #else
                    tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                  #endif
                    if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<(tempW7-3)) {tempW7 = tempW7-3;}
                    else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
                    else { }
                }
                else {
                    if((FL.s16_Estimated_Active_Press>=MPRESS_60BAR) && (FR.s16_Estimated_Active_Press>=MPRESS_60BAR)) {
                        tempW7 = s16CompDutyForMPVariation;
                    }
                    else {
                      #if   __BUMP_SUSPECT
                        if(WL->LFC_Bump_Suspect_flag==1) {
                            tempW7 = s16CompDutyForMPVariation + (s16CompDutyForDiffFromHMuSkid/3);
                            if(tempW7 >= 8) {tempW7 = 8;}
                        }
                        else {
                            tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                        }

                        if((WL->MP_Init!=S16_ReferenceMCPress) || (WL->LFC_MP_Cyclic_comp_duty!=0))
                        {
                            tempW7 = s16CompDutyForMPVariation;     /* Duty Limited Rear Wheel */
                        }

                      #else
                        tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                      #endif
                        if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<(tempW7-3)) {tempW7 = tempW7-3;}
                        else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;}
                        else { }
                    }
                }
                #if (__LOW_DP_COMP == ENABLE)
                if(s16MasterPress < S16_PARTIAL_BRAKE_DETECT_TH)
                {
                	tempW7 -= LCABS_s16Interpolation2P((s16MasterPress - WL->s16_Estimated_Active_Press),MPRESS_0BAR,S16_LOW_DP_COMP_REF,U8_LOW_DP_COMP_DUTY,0);
                }
                else{}
                #endif
            }
            else {
                  #if __INGEAR_VIB  /**************************************************Front Wheel Drive */
                if((REAR_WHEEL_wl==0) && (WL->In_Gear_flag==1) && (WL->LFC_zyklus_z==(U8_IN_GEAR_detect_cycle+1)))
                {

					s16CompDutyForMPVariation =  ((s16MasterPress - S16_ReferenceMCPress)/s16DPDutyCompRef_MPVariation);
					s16CompDutyForDiffFromHMuSkid = ((s16DPDutyCompRef_HighMuSkid - WL->s16_Estimated_Active_Press)/s16DPDutyCompRef_PDiffFromSkid);
                    
                    tempW6 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;

                   #if __SEVERE_INGEAR_VIB == DISABLE	
                    if(tempW6 >= (WL->LFC_MP_Cyclic_comp_duty + 5)) {
                        tempW7 = tempW6 - WL->LFC_MP_Cyclic_comp_duty;
                    }
                    else {tempW7 = s16CompDutyForMPVariation;}

                   #else
                    if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<(tempW6-3)) {tempW6 = tempW6-3;}
                    else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<tempW6) {tempW6 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
                    else { }                    
                    tempW7 = tempW6 - WL->LFC_MP_Cyclic_comp_duty;
                   #endif /* __SEVERE_INGEAR_VIB == DISABLE	*/
                    
                }
                else {
                    tempW6 = WL->Estimated_Press_SKID - WL->s16_Estimated_Active_Press;
                    if((WL->LFC_n_th_deep_slip_comp_flag==1)&&(tempW6>=MPRESS_50BAR)) {
                      #if   __BUMP_SUSPECT
                        if(WL->LFC_Bump_Suspect_flag==1) {
                            tempW7 = s16CompDutyForMPVariation + ((tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid))/3);
                        }
                        else {
                            tempW7 = s16CompDutyForMPVariation + (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                            
                            if(REAR_WHEEL_wl==0)
                            {
                                if((tempW7+2) < (int16_t)U8_LFC_Jump_Down_Comp_F)
                                {
                                    tempW7 = tempW7+2;
                                }
                            }
                            else
                            {
                                if((tempW7+2) < (int16_t)U8_LFC_Jump_Down_Comp_R)
                                {
                                    tempW7 = tempW7+2;
                                }
                            }
                        }
                      #else
                        tempW7 = s16CompDutyForMPVariation + (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                      #endif
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation;
                    }
                }
                  #else
/*              tempW7 = (lcabss16RefCircuitPress - WL->MP_Init)/DUTY_COMP_DP; */
                tempW6 = WL->Estimated_Press_SKID - WL->s16_Estimated_Active_Press;
                if(((WL->LFC_n_th_deep_slip_comp_flag==1)&&(tempW6>=MPRESS_50BAR)) || (tempW6>=MPRESS_80BAR)) {
                  #if   __BUMP_SUSPECT
                    if(WL->LFC_Bump_Suspect_flag==1) {
                        tempW7 = s16CompDutyForMPVariation + ((tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid))/3);
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation + (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                    }
                  #else
                    tempW7 = s16CompDutyForMPVariation + (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                  #endif
                }
                else {
                    tempW7 = s16CompDutyForMPVariation;
                }
                  #endif            /********************************************************************/
            }
          #endif
            WL->LFC_MP_Cyclic_comp_duty = WL->LFC_MP_Cyclic_comp_duty + tempW7;
            WL->LFC_MP_Cyclic_comp_duty = LCABS_s16LimitMinMax(WL->LFC_MP_Cyclic_comp_duty, -MAX_MP_COMP_DUTY, MAX_MP_COMP_DUTY);
			/*if(WL->LFC_MP_Cyclic_comp_duty >= MAX_MP_COMP_DUTY) {WL->LFC_MP_Cyclic_comp_duty = MAX_MP_COMP_DUTY;}
            else if(WL->LFC_MP_Cyclic_comp_duty <= -MAX_MP_COMP_DUTY) {WL->LFC_MP_Cyclic_comp_duty = -MAX_MP_COMP_DUTY;}
            else {}*/

            WL->MP_Init = s16MasterPress;
          #if __UNSTABLE_REAPPLY_ENABLE
            WL->Unstable_Rise_MP_comp_duty = 0;
          #endif
        }
    }
      #if __UNSTABLE_REAPPLY_ENABLE
    else if((WL->Reapply_Accel_flag==1)&&(WL->Reapply_Accel_1st_scan==1) && (ABS_DURING_MPRESS_FLG==1))
    {
        if(WL->LFC_zyklus_z>=1)
        {
            tempW7 = s16CompDutyForMPVariation;
            /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
            else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
            else { }
            WL->Unstable_Rise_MP_comp_duty = (int8_t)tempW7;*/
            WL->Unstable_Rise_MP_comp_duty = LCABS_s16LimitMinMax(tempW7,-MAX_MP_COMP_DUTY,MAX_MP_COMP_DUTY);
        }
	    else
	    {
	          #if (__PRESS_EST && __EST_MU_FROM_SKID)
	        if((WL->lcabsu8PossibleMuFromSkid>0) && (WL->lcabsu8PossibleMuFromSkid<=U8_PULSE_UNDER_LOW_MED_MU))
	        {
	            tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
	            if(REAR_WHEEL_wl==0)
	            {
	                if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<(tempW7-3)) {tempW7 = tempW7-3;}
	                else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
	                else { }
	            }
	            else
	            {
	                if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<(tempW7-3)) {tempW7 = tempW7-3;}
	                else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;}
	                else { }
	            }
	        }
	        else
	        {
	            tempW7 = s16CompDutyForMPVariation;
	            /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
	            else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
	            else { }*/
	        }
	        /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
	        else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
	        else { }
	        WL->Unstable_Rise_MP_comp_duty = (int8_t)tempW7;*/
            WL->Unstable_Rise_MP_comp_duty = LCABS_s16LimitMinMax(tempW7,-MAX_MP_COMP_DUTY,MAX_MP_COMP_DUTY);
	          #endif
        }
    }
    else { }
      #endif
  #if   __VDC && __ABS_COMB
    }
  #endif
}
  #else/*MP/dP compensation for NO valve on/off control*/
void LAABS_vCompLFCMPCyclicCompDuty(struct W_STRUCT *WL)
{
    int16_t  	s16CompDutyForMPVariation;
    int16_t 	s16CompDutyForDiffFromHMuSkid;
    int16_t		s16MasterPress, s16DPDutyCompRef_HighMuSkid;
    int16_t		s16DPDutyCompRef_MPVariation, s16DPDutyCompRef_PDiffFromSkid;

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_MP_Cyclic_comp_duty = 0;
        WL->MP_Init = S16_ReferenceMCPress;
      #if __UNSTABLE_REAPPLY_ENABLE
        WL->Unstable_Rise_MP_comp_duty = 0;
     #endif
    }

#if(__HEV_MP_MOVING_AVG==ENABLE)
    LAABS_vCalMPMovingAvg();
    s16MasterPress = laabss16MPAvg;
  #else
    s16MasterPress = lcabss16RefMpress; /* AHBGen3 MP Fluctuation */
  #endif

  #if (__FRONT_REAR_MP_COMP_SEPERATE == ENABLE)
  	if(REAR_WHEEL_wl == 0)
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariation;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkid;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkid*MPRESS_1BAR;
  	}
  	else
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariationR;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkidR;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkidR*MPRESS_1BAR;
  	}
  #else
	s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariation;
	s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkid;
	s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkid*MPRESS_1BAR;
  #endif/* (__FRONT_REAR_MP_COMP_SEPERATE == ENABLE)*/

	s16CompDutyForMPVariation =  ((WL->MP_Init - s16MasterPress)/s16DPDutyCompRef_MPVariation);                                       /*comp sign is opposite to the LFC duty comp. structure*/
	s16CompDutyForDiffFromHMuSkid = ((WL->s16_Estimated_Active_Press - s16DPDutyCompRef_HighMuSkid)/s16DPDutyCompRef_PDiffFromSkid);

  #if   __VDC && __ABS_COMB
    if(WL->L_SLIP_CONTROL==0) {
  #endif

    if((WL->LFC_built_reapply_flag==1) && (ABS_DURING_MPRESS_FLG==1)) {
        if(WL->LFC_built_reapply_counter==L_1ST_SCAN) {

            tempW7 = 0;

          #if !__PRESS_EST
            tempW7 = s16CompDutyForMPVariation;
          #else
            if(WL->LFC_zyklus_z<=1) {
                if(REAR_WHEEL_wl==0) {/*front*/
                  #if   __BUMP_SUSPECT
                    if(WL->LFC_Bump_Suspect_flag==1) {
                        tempW7 = s16CompDutyForMPVariation + (s16CompDutyForDiffFromHMuSkid/3);
                        if(tempW7 <= 2) {tempW7 = 2;}
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                    }
                  #else
                    tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                  #endif
                    /*if(-(int16_t)U8_LFC_Initial_Duty_Comp_Low_F>(tempW7-3)) {tempW7 = tempW7-3;}
                    else*/ if(-(int16_t)U8_LFC_Initial_Duty_Comp_Low_F>tempW7) {tempW7 = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
                    else { }
                }
                else {/*rear*/
                    if((FL.s16_Estimated_Active_Press>=MPRESS_60BAR) && (FR.s16_Estimated_Active_Press>=MPRESS_60BAR)) {
                        tempW7 = s16CompDutyForMPVariation;
                    }
                    else {
                      #if   __BUMP_SUSPECT
                        if(WL->LFC_Bump_Suspect_flag==1) {
                            tempW7 = s16CompDutyForMPVariation + (s16CompDutyForDiffFromHMuSkid/3);
                            if(tempW7 <= 2) {tempW7 = 2;}
                        }
                        else {
                            tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                        }

                        /*if((WL->MP_Init!=S16_DP_DUTY_COMP_REF_MPRESS) || (WL->LFC_MP_Cyclic_comp_duty!=0))
                        {
                            tempW7 = s16CompDutyForMPVariation;     // Duty Limited Rear Wheel
                        }*/

                      #else
                        tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
                      #endif
                        /*if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R>(tempW7+2)) {tempW7 = tempW7+2;}
                        else*/if(-(int16_t)U8_LFC_Initial_Duty_Comp_Low_R>tempW7) {tempW7 = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_R;}
                        else { }
                    }
                }
                #if (__LOW_DP_COMP == ENABLE)
                if(s16MasterPress < S16_PARTIAL_BRAKE_DETECT_TH)
                {
                	tempW7 += LCABS_s16Interpolation2P((WL->s16_Estimated_Active_Press - s16MasterPress),MPRESS_0BAR,S16_LOW_DP_COMP_REF,U8_LOW_DP_COMP_DUTY,0);
                }
                else{}
                #endif
            }
            else {
                  #if __INGEAR_VIB  /**************************************************Front Wheel Drive */
                if((REAR_WHEEL_wl==0) && (WL->In_Gear_flag==1) && (WL->LFC_zyklus_z==(U8_IN_GEAR_detect_cycle+1)))
                {

					s16CompDutyForMPVariation =  ((S16_ReferenceMCPress - s16MasterPress)/s16DPDutyCompRef_MPVariation);
					s16CompDutyForDiffFromHMuSkid = ((WL->s16_Estimated_Active_Press - s16DPDutyCompRef_HighMuSkid)/s16DPDutyCompRef_PDiffFromSkid);
                    
                    tempW6 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;

                   #if __SEVERE_INGEAR_VIB == DISABLE	
                    if(tempW6 <= (WL->LFC_MP_Cyclic_comp_duty - 2)) {
                        tempW7 = tempW6 + WL->LFC_MP_Cyclic_comp_duty;
                    }
                    else {tempW7 = s16CompDutyForMPVariation;}

                   #else
                    /*if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F>(tempW6+3)) {tempW6 = tempW6+3;}
                    else*/ if(-(int16_t)U8_LFC_Initial_Duty_Comp_Low_F>tempW6) {tempW6 = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
                    else { }                    
                    tempW7 = tempW6 - WL->LFC_MP_Cyclic_comp_duty;
                   #endif /* __SEVERE_INGEAR_VIB == DISABLE	*/
                    
                }
                else {
                    tempW6 = WL->Estimated_Press_SKID - WL->s16_Estimated_Active_Press;
                    if((WL->LFC_n_th_deep_slip_comp_flag==1)&&(tempW6>=MPRESS_50BAR)) {
                      #if   __BUMP_SUSPECT
                        if(WL->LFC_Bump_Suspect_flag==1) {
                            tempW7 = s16CompDutyForMPVariation - ((tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid))/3);
                        }
                        else {
                            tempW7 = s16CompDutyForMPVariation - (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                            
                            if(REAR_WHEEL_wl==0)
                            {
                            if(tempW7 > -(int16_t)U8_LFC_Jump_Down_Comp_F)
                                {
                             	tempW7 = -(int16_t)U8_LFC_Jump_Down_Comp_F;
                                }
                            }
                            else
                            {
                            if(tempW7 > -(int16_t)U8_LFC_Jump_Down_Comp_R)
                                {
                                tempW7 = -(int16_t)U8_LFC_Jump_Down_Comp_R;
                                }
                            }
                        }
                      #else
                        tempW7 = s16CompDutyForMPVariation - (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                      #endif
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation;
                    }
                }
                  #else
/*              tempW7 = (mpress - WL->MP_Init)/DUTY_COMP_DP; */
                tempW6 = WL->Estimated_Press_SKID - WL->s16_Estimated_Active_Press;
                if(((WL->LFC_n_th_deep_slip_comp_flag==1)&&(tempW6>=MPRESS_50BAR)) || (tempW6>=MPRESS_80BAR)) {
                  #if   __BUMP_SUSPECT
                    if(WL->LFC_Bump_Suspect_flag==1) {
                        tempW7 = s16CompDutyForMPVariation - ((tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid))/3);
                    }
                    else {
                        tempW7 = s16CompDutyForMPVariation - (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                    }
                  #else
                    tempW7 = s16CompDutyForMPVariation - (tempW6/((int16_t)U8_DPDutyCompRef_PDiffFromSkid));
                  #endif
                }
                else {
                    tempW7 = s16CompDutyForMPVariation;
                }
                  #endif            /********************************************************************/
            }
          #endif
            WL->LFC_MP_Cyclic_comp_duty = WL->LFC_MP_Cyclic_comp_duty + tempW7;
            WL->LFC_MP_Cyclic_comp_duty = LCABS_s16LimitMinMax(WL->LFC_MP_Cyclic_comp_duty, -MAX_MP_COMP_DUTY, MAX_MP_COMP_DUTY);
			/*if(WL->LFC_MP_Cyclic_comp_duty >= MAX_MP_COMP_DUTY) {WL->LFC_MP_Cyclic_comp_duty = MAX_MP_COMP_DUTY;}
            else if(WL->LFC_MP_Cyclic_comp_duty <= -MAX_MP_COMP_DUTY) {WL->LFC_MP_Cyclic_comp_duty = -MAX_MP_COMP_DUTY;}
            else {}*/

            WL->MP_Init = s16MasterPress;
          #if __UNSTABLE_REAPPLY_ENABLE
            WL->Unstable_Rise_MP_comp_duty = 0;
          #endif
        }
    }
      #if 0//__UNSTABLE_REAPPLY_ENABLE
    else if((WL->Reapply_Accel_flag==1)&&(WL->Reapply_Accel_1st_scan==1) && (ABS_DURING_MPRESS_FLG==1))
    {
        if(WL->LFC_zyklus_z>=1)
        {
            tempW7 = s16CompDutyForMPVariation;
            /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
            else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
            else { }
            WL->Unstable_Rise_MP_comp_duty = (int8_t)tempW7;*/
            WL->Unstable_Rise_MP_comp_duty = LCABS_s16LimitMinMax(tempW7,-MAX_MP_COMP_DUTY,MAX_MP_COMP_DUTY);
        }
	    else
	    {
	          #if (__PRESS_EST && __EST_MU_FROM_SKID)
	        if((WL->lcabsu8PossibleMuFromSkid>0) && (WL->lcabsu8PossibleMuFromSkid<=U8_PULSE_UNDER_LOW_MED_MU))
	        {
	            tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
	            if(REAR_WHEEL_wl==0)
	            {
	                if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<(tempW7-3)) {tempW7 = tempW7-3;}
	                else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_F<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;}
	                else { }
	            }
	            else
	            {
	                if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<(tempW7-3)) {tempW7 = tempW7-3;}
	                else if((int16_t)U8_LFC_Initial_Duty_Comp_Low_R<tempW7) {tempW7 = (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;}
	                else { }
	            }
	        }
	        else
	        {
	            tempW7 = s16CompDutyForMPVariation;
	            /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
	            else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
	            else { }*/
	        }
	        /*if(tempW7 >= MAX_MP_COMP_DUTY) {tempW7 = MAX_MP_COMP_DUTY;}
	        else if(tempW7 <= -MAX_MP_COMP_DUTY) {tempW7 = -MAX_MP_COMP_DUTY;}
	        else { }
	        WL->Unstable_Rise_MP_comp_duty = (int8_t)tempW7;*/
            WL->Unstable_Rise_MP_comp_duty = LCABS_s16LimitMinMax(tempW7,-MAX_MP_COMP_DUTY,MAX_MP_COMP_DUTY);
	          #endif
        }
    }
    else { }
      #endif
  #if   __VDC && __ABS_COMB
    }
  #endif
}
  #endif/*(__IDB_LOGIC==DISABLE)*/
#if ((__AHB_SYSTEM==ENABLE)&&(__HEV_MP_MOVING_AVG==ENABLE))
void LAABS_vCalMPMovingAvg(void)
{
    if(speed_calc_timer<L_TIME_200MS)
    {
        laabss16MPAvg = 0;
        laabsu8MPArrayCnt = 0;
        laabsu8MPAvgCnt = 0;
    }
    else
    {
	    laabss16MPbuffer[laabsu8MPArrayCnt] = lcabss16RefCircuitPress;
	    laabsu8MPArrayCnt = laabsu8MPArrayCnt + 1;
	    if(laabsu8MPArrayCnt>=4)
	    {
	        laabsu8MPArrayCnt = 0;
	        laabss16MPAvgBuffer[laabsu8MPAvgCnt] = (laabss16MPbuffer[0]+laabss16MPbuffer[1]+laabss16MPbuffer[2]+laabss16MPbuffer[3])/4;

	        laabsu8MPAvgCnt = laabsu8MPAvgCnt + 1;
	        if(laabsu8MPAvgCnt>=2)
	        {
	            laabsu8MPAvgCnt = 0;
	            laabss16MPAvg = (laabss16MPAvgBuffer[0]+laabss16MPAvgBuffer[1])/2;
	        }
	        else { }
	    }
	    else { }
    }
}
#endif

void LAABS_vCompLFCMPFluctuation(void)
{
    LAABS_vCompLFCMPFluctuationWL(&FL);
    LAABS_vCompLFCMPFluctuationWL(&FR);
    LAABS_vCompLFCMPFluctuationWL(&RL);
    LAABS_vCompLFCMPFluctuationWL(&RR);
}

void LAABS_vCompLFCMPFluctuationWL(struct W_STRUCT *WL)
{
  #if   __VDC && __ABS_COMB
    if(WL->L_SLIP_CONTROL==0) {
  #endif
    if(WL->LFC_built_reapply_flag==1) {
        if(WL->LFC_built_reapply_counter==L_1ST_SCAN) {
            //WL->pwm_duty_temp = WL->pwm_duty_temp;
            WL->LFC_reapply_current_duty2 = WL->LFC_reapply_current_duty;
        }
        else if(WL->pwm_duty_temp < PWM_duty_HOLD) {

            tempW0 = (int16_t)WL->LFC_reapply_current_duty + ((lcabss16RefCircuitPress - WL->MP_Init)/50);

            if(tempW0 >= PWM_duty_HOLD) {
                WL->pwm_duty_temp = WL->LFC_reapply_current_duty2 = PWM_duty_HOLD;
            }
            else {
                if(tempW0 <= pwm_duty_null) {
                    WL->LFC_reapply_current_duty2 = pwm_duty_null;
                }
                else {WL->LFC_reapply_current_duty2 = (uint8_t)tempW0;}

                WL->pwm_duty_temp = WL->LFC_reapply_current_duty2;

              #if   __WP_DROP_COMP
                if((WL->WP_Drop_comp_flag==1) && (WL->LFC_s0_reapply_counter==1)) {
                    WL->pwm_duty_temp += WL->LFC_WP_Drop_comp_duty;
                }
              #endif

                if(WL->MSL_long_hold_flag==1) {
                    if(REAR_WHEEL_wl==0) {WL->pwm_duty_temp += (uint8_t)U8_LFC_Long_Hold_Comp_Duty_F;}
                    else {WL->pwm_duty_temp += (uint8_t)U8_LFC_Long_Hold_Comp_Duty_R;}
                    if(WL->pwm_duty_temp >= PWM_duty_HOLD) {WL->pwm_duty_temp = PWM_duty_HOLD;}
                }
            }
        }
        else {
            //WL->pwm_duty_temp = WL->pwm_duty_temp;
            WL->LFC_reapply_current_duty2 = WL->LFC_reapply_current_duty;
        }
    }
    else {
        //WL->pwm_duty_temp = WL->pwm_duty_temp;
        if(WL->LFC_reapply_current_duty > pwm_duty_null) {
            tempW0 = (int16_t)WL->LFC_reapply_current_duty + ((lcabss16RefCircuitPress - WL->MP_Init)/50);
            if(tempW0 >= PWM_duty_HOLD) {
                WL->LFC_reapply_current_duty2 = PWM_duty_HOLD;
            }
            else if(tempW0 <= pwm_duty_null) {
                WL->LFC_reapply_current_duty2 = pwm_duty_null;
            }
            else {
                WL->LFC_reapply_current_duty2 = (uint8_t)tempW0;
            }
        }
    }
    WL->LFC_reapply_current_duty1 = WL->LFC_reapply_current_duty;
  #if   __VDC && __ABS_COMB
    }
  #endif

}

  #if   __WP_DROP_COMP
void LAABS_vCompLFCWPDrop(void)
{
    LAABS_vCompLFCWPDropWL(&FL);
    LAABS_vCompLFCWPDropWL(&FR);
    LAABS_vCompLFCWPDropWL(&RL);
    LAABS_vCompLFCWPDropWL(&RR);
}

void LAABS_vCompLFCWPDropWL(struct W_STRUCT *WL)
{
    int16_t P_drop;

    if(WL->LFC_built_reapply_counter==L_1ST_SCAN) {
        if((WL->LFC_n_th_deep_slip_comp_flag==0) && (afz<-(int16_t)U8_LOW_MU)) {
            P_drop = WL->Estimated_Press_init - WL->s16_Estimated_Active_Press;
            if((P_drop >= MPRESS_30BAR) && (WL->Estimated_Press_end>=MPRESS_50BAR)) {
                WL->WP_Drop_comp_flag = 1;
                WL->Drop_Press_Comp_T = P_drop/28;
                if(WL->LFC_built_reapply_counter_delta <= -((int16_t)WL->hold_timer_new_ref_old+L_1SCAN)*2) {
                    WL->Drop_Press_Comp_T -= 3;
                }
                WL->LFC_WP_Drop_comp_duty = P_drop/50 - 10;
            }
            else {
                WL->WP_Drop_comp_flag = 0;
                WL->Drop_Press_Comp_T = 0;
                WL->LFC_WP_Drop_comp_duty = 0;
            }
        }
        else {
            WL->WP_Drop_comp_flag = 0;
            WL->Drop_Press_Comp_T = 0;
            WL->LFC_WP_Drop_comp_duty = 0;
        }
    }
}
  #endif /* __WP_DROP_COMP */
#endif /* __VDC && __MP_COMP */


void LAABS_vLimitDiffofFrontLFCDuty(void)
{
    if((ABS_fz==1)&&(vref>=VREF_6_KPH)) {
      #if   __VDC && __ABS_COMB
        if((FSF_fl==1) && (FL.LFC_ABS_Forced_Enter_flag==1) && (L_SLIP_CONTROL_fl==0))
      #else
        if((FSF_fl==1) && (FL.LFC_ABS_Forced_Enter_flag==1))
      #endif
        {
            if(FR.LFC_reapply_current_duty > 0)
            {
                FL.LFC_special_comp_duty = ((int16_t)(FR.LFC_reapply_current_duty)-(int16_t)(FL.LFC_initial_set_duty));
/*                afz_flags=105; */
            }
            else if(FR.LFC_reapply_end_duty > 0)
            {
                FL.LFC_special_comp_duty = ((int16_t)(FR.LFC_reapply_end_duty)-(int16_t)(FL.LFC_initial_set_duty));
/*                afz_flags=115; */
            }
            else
            {
                ;
            }
        }
      #if   __VDC && __ABS_COMB
        else if((FSF_fr==1) && (FR.LFC_ABS_Forced_Enter_flag==1) && (L_SLIP_CONTROL_fr==0))
      #else
        else if((FSF_fr==1) && (FR.LFC_ABS_Forced_Enter_flag==1))
      #endif
        {
            if(FL.LFC_reapply_current_duty>0)
            {
                FR.LFC_special_comp_duty = ((int16_t)(FL.LFC_reapply_current_duty)-(int16_t)(FR.LFC_initial_set_duty));
/*                afz_flags=104; */
            }
            else if(FL.LFC_reapply_end_duty>0)
            {
                FR.LFC_special_comp_duty = ((int16_t)(FL.LFC_reapply_end_duty)-(int16_t)(FR.LFC_initial_set_duty));
/*                afz_flags=114; */
            }
            else 
            {
                ;
            }
        }
        else {
          #if   __VDC && __ABS_COMB
            if((L_SLIP_CONTROL_fl==0) && (L_SLIP_CONTROL_fr==0)) {
          #endif
            if(((FL.Check_double_brake_flag2==1)&&(FR.Check_double_brake_flag2==1)) || (LOW_to_HIGH_suspect==1))
            {
                LAABS_vSyncBothWheelDuty(&FL, &FR);
/*                afz_flags=102; */
            }
/*
            if((FL.Check_double_brake_flag2==1)&&(FR.Check_double_brake_flag2==1)) {
                if((FL.LFC_built_reapply_flag==1)&&(FR.LFC_built_reapply_flag==1)) {
                    if(FL.LFC_reapply_current_duty > (FR.LFC_reapply_current_duty+1)) {
                        FL.LFC_special_comp_duty-=((int16_t)(FL.LFC_reapply_current_duty)-(int16_t)(FR.LFC_reapply_current_duty)-1);
                        afz_flags=102;
                    }
                    else if(FR.LFC_reapply_current_duty > (FL.LFC_reapply_current_duty+1)) {
                        FR.LFC_special_comp_duty-=((int16_t)(FR.LFC_reapply_current_duty)-(int16_t)(FL.LFC_reapply_current_duty)-1);
                        afz_flags=103;
                    }
                    else { }
                }
            }
            if((LOW_to_HIGH_suspect==1) && (FL.LFC_built_reapply_flag==1)&&(FR.LFC_built_reapply_flag==1))
            {
                if(FL.LFC_reapply_current_duty > (FR.LFC_reapply_current_duty+1))
                {
                    FL.LFC_special_comp_duty-=((int16_t)(FL.LFC_reapply_current_duty)-(int16_t)(FR.LFC_reapply_current_duty)-1);
                    afz_flags=100;
                }
                else if(FR.LFC_reapply_current_duty > (FL.LFC_reapply_current_duty+1))
                {
                    FR.LFC_special_comp_duty-=((int16_t)(FR.LFC_reapply_current_duty)-(int16_t)(FL.LFC_reapply_current_duty)-1);
                    afz_flags=101;
                }
                else { }
            }
*/
          #if   __VDC && __ABS_COMB
            }
          #endif
        }
    }
    else {
        if((ABS_LIMIT_SPEED_fl==1) && (ABS_LIMIT_SPEED_fr==1)) {
            LAABS_vSyncBothWheelDuty(&FL, &FR);
/*          afz_flags=100; */
/*            
            if((FL.LFC_built_reapply_flag==1)&&(FR.LFC_built_reapply_flag==1)) {
                if(FL.LFC_reapply_current_duty > (FR.LFC_reapply_current_duty+1)) {
                    FL.LFC_special_comp_duty-=((int16_t)(FL.LFC_reapply_current_duty)-(int16_t)(FR.LFC_reapply_current_duty)-1);
                }
                else if(FR.LFC_reapply_current_duty > (FL.LFC_reapply_current_duty+1)) {
                    FR.LFC_special_comp_duty-=((int16_t)(FR.LFC_reapply_current_duty)-(int16_t)(FL.LFC_reapply_current_duty)-1);
                }
                else { }
            }
*/
        }
    }
}

void LAABS_vSyncBothWheelDuty(struct W_STRUCT *pW1, struct W_STRUCT *pW2)
{
    if((pW1->LFC_built_reapply_flag==1)&&(pW2->LFC_built_reapply_flag==1)) {
        if(pW1->LFC_reapply_current_duty > (pW2->LFC_reapply_current_duty+1)) {
            pW1->LFC_special_comp_duty-=((int16_t)(pW1->LFC_reapply_current_duty)-(int16_t)(pW2->LFC_reapply_current_duty)-1);
        }
        else if(pW2->LFC_reapply_current_duty > (pW1->LFC_reapply_current_duty+1)) {
            pW2->LFC_special_comp_duty-=((int16_t)(pW2->LFC_reapply_current_duty)-(int16_t)(pW1->LFC_reapply_current_duty)-1);
        }
        else { }
    }
}

  #if (NO_VV_ONOFF_CTRL==DISABLE)
void LAABS_vCalcLFCInitialDuty(struct W_STRUCT *WL)
{
#if __VDC && __TCMF_CONTROL
    uint8_t  u8ActiveBrkLfcInitDuty;
    int16_t  s16TcPressMin = (la_PtcMfc.MFC_Target_Press>la_StcMfc.MFC_Target_Press) ? la_StcMfc.MFC_Target_Press : la_PtcMfc.MFC_Target_Press;
    s16TcPressMin = s16TcPressMin*MPRESS_1BAR;
#endif

#if __VDC
    if(WL->LFC_to_ESP_flag==0) {
#endif

#if __VDC
  #if __UCC_COOPERATION_CONTROL
    if(((Rough_road_detect_vehicle==1)||(UCC_DETECT_BUMP_Vehicle_flag==1))&&(YAW_CDC_WORK==0)){
  #else
    if((Rough_road_detect_vehicle==1)&&(YAW_CDC_WORK==0)){     /* 03 TRC High Mu */
  #endif
#else
    if(Rough_road_detect_vehicle==1){     /* 03 TRC High Mu */
#endif
        if(REAR_WHEEL_wl==0) {      /* Front Wheel */
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_F-U8_Initial_Duty_Rough_Comp_F);
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_R-U8_Initial_Duty_Rough_Comp_R);
        }
    }
  #if (__ROUGH_COMP_IMPROVE == ENABLE)
    else if(Rough_road_suspect_vehicle==1) {
        if(REAR_WHEEL_wl==0) {      
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_F-U8_Init_Duty_Rough_Sus_Comp_F);
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_R-U8_Init_Duty_Rough_Sus_Comp_R);
        }
    }
  #endif /*__ROUGH_COMP_IMPROVE*/
    else if((WL->LFC_zyklus_z<=1)&&(WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
        if((AFZ_OK==1) && (afz <= -(int16_t)U8_HIGH_MU)) {        /* 03 TRC High Mu */
            if(REAR_WHEEL_wl==0) {      /* Front Wheel */
                WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F-3;
            }
            else {
                WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_R-5;
            }
        }
    }
    else if((WL->LFC_initial_set_duty==0)||((FSF_wl==1)&&(ABS_fz==0))) {
        if(REAR_WHEEL_wl==0) {      /* Front Wheel */
            WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F;
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_R;
        }
        
	  #if   __VDC && __TCMF_CONTROL
	    if((ACTIVE_BRAKE_FLG==1) && (BRAKE_BY_MPRESS_FLG==0))
	    {
	        u8ActiveBrkLfcInitDuty = (uint8_t)LCABS_s16Interpolation2P(s16TcPressMin,MPRESS_20BAR,MPRESS_100BAR,(int16_t)U8_INIT_DUTY_ACTIVE_BRK_MIN, (int16_t)U8_INIT_DUTY_ACTIVE_BRK_MAX); 
	        if(WL->LFC_initial_set_duty > (int16_t)u8ActiveBrkLfcInitDuty)
	        {
	        	WL->LFC_initial_set_duty = (int16_t)u8ActiveBrkLfcInitDuty;
	        }
	    }
	  #endif        
    }
  #if   (__HDC || __ACC) && __TCMF_CONTROL2
    else if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
        if(lcu1HdcActiveFlg==1) {WL->LFC_initial_set_duty = 50;}
        else if(lcu1AccActiveFlg==1) {
            if(lcs16AccTargetG>=-30) {WL->LFC_initial_set_duty = 45;}
            else if(lcs16AccTargetG>=-50) {WL->LFC_initial_set_duty = 50;}
            else if(lcs16AccTargetG>=-70) {WL->LFC_initial_set_duty = 55;}
            else {WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F;}
        }
        else { }
    }
  #endif
    else { }

  #if __VDC && __MP_COMP
    WL->init_duty = WL->LFC_initial_set_duty + WL->LFC_MP_Cyclic_comp_duty
                    - (int16_t)(WL->LFC_pres_rise_delay_comp) + (int16_t)(WL->LFC_special_comp_duty);
    tempW0 = PWM_duty_HOLD;
  #else
    if(REAR_WHEEL_wl==0) {      /* Front Wheel */
        WL->init_duty = WL->LFC_initial_set_duty
                        - (int16_t)(WL->LFC_pres_rise_delay_comp) + (int16_t)(WL->LFC_special_comp_duty);
    }
    else {

        WL->init_duty = WL->LFC_initial_set_duty
                        - (int16_t)(WL->LFC_pres_rise_delay_comp) + (int16_t)(WL->LFC_special_comp_duty);
    }

    if(REAR_WHEEL_wl==0) {tempW0 = (int16_t)U8_Initial_Duty_Ratio_F + (int16_t)U8_LFC_Initial_Duty_Comp_Low_F + 20;}
    else {tempW0 = (int16_t)U8_Initial_Duty_Ratio_R + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R + 20;}

  #endif

   #if __ROUGH_ROAD_DUMP_IMPROVE
    if((lcabsu1TwoWlABSUneven==1) || (lcabsu1OneWlABSUneven==1))
    {
      if((WL->ABS==1) && (STABIL_wl==1) && (WL->LFC_STABIL_counter>=L_TIME_100MS) && (WL->rel_lam>=-LAM_3P))
      {
        WL->init_duty = WL->init_duty - 3;
      }
    }
   #endif

  #if (__SLIGHT_BRAKING_COMPENSATION)
    if(lcabsu1SlightBraking==1)
    {
        tempW1 = LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS,15, 30);
    }
    else
    {
        tempW1 = 40;
    }
  #else  /*(__SLIGHT_BRAKING_COMPENSATION)*/
	  #if (__ROUGH_COMP_IMPROVE==ENABLE)
    if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1))
    {
    	tempW1 = 20;
    }
	else 
	  #endif/*__ROUGH_COMP_IMPROVE*/
	{
    	tempW1 = 40;
	}
  #endif /*(__SLIGHT_BRAKING_COMPENSATION)*/

  #if   __VDC
    #if __HDC
    if(lcu1HdcActiveFlg==1)
    {
        tempW1 = 30;
    }    
    #endif
    #if __EPB_INTERFACE
    if(lcu1EpbActiveFlg==1)
    {
        tempW1 = 30;
    }
    #endif
  #endif

    if(WL->init_duty <= tempW1 ) {
        WL->LFC_special_comp_duty += (int16_t)tempW1 - WL->init_duty;
        WL->init_duty=tempW1;
    }
    else if(WL->init_duty >= tempW0 ) {                         /* '04 SW Winter test */
        WL->LFC_special_comp_duty -= WL->init_duty - tempW0;
        WL->init_duty = tempW0;
    }
    else { }

#if __VDC
    }
#endif
}
  #else /*IDB Enable*/
void LAABS_vCalcLFCInitialDuty(struct W_STRUCT *WL)
{
#if __VDC && __TCMF_CONTROL
    uint8_t  u8ActiveBrkLfcInitDuty;
    int16_t  s16TcPressMin = (la_PtcMfc.MFC_Target_Press>la_StcMfc.MFC_Target_Press) ? la_StcMfc.MFC_Target_Press : la_PtcMfc.MFC_Target_Press;
    s16TcPressMin = s16TcPressMin*MPRESS_1BAR;
#endif

#if __VDC
    if(WL->LFC_to_ESP_flag==0) {
#endif

#if __VDC
  #if __UCC_COOPERATION_CONTROL
    if(((Rough_road_detect_vehicle==1)||(UCC_DETECT_BUMP_Vehicle_flag==1))&&(YAW_CDC_WORK==0)){
  #else
    if((Rough_road_detect_vehicle==1)&&(YAW_CDC_WORK==0)){     /* 03 TRC High Mu */
  #endif
#else
    if(Rough_road_detect_vehicle==1){     /* 03 TRC High Mu */
#endif
        if(REAR_WHEEL_wl==0) {      /* Front Wheel */
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_F+U8_Initial_Duty_Rough_Comp_F);
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_R+U8_Initial_Duty_Rough_Comp_R);
        }
    }
  #if (__ROUGH_COMP_IMPROVE == ENABLE)
    else if(Rough_road_suspect_vehicle==1) {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_F+U8_Init_Duty_Rough_Sus_Comp_F);
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)(U8_Initial_Duty_Ratio_R+U8_Init_Duty_Rough_Sus_Comp_R);
        }
    }
  #endif /*__ROUGH_COMP_IMPROVE*/
    /*else if((WL->LFC_zyklus_z<=1)&&(WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
        if((AFZ_OK==1) && (afz <= -(int16_t)U8_HIGH_MU)) {        // 03 TRC High Mu 
            if(REAR_WHEEL_wl==0) {      // Front Wheel
                WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F+2;
            }
            else {
                WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_R+3;
            }
        }
    }*/
    else if((WL->LFC_initial_set_duty==0)||((FSF_wl==1)&&(ABS_fz==0))) {
        if(REAR_WHEEL_wl==0) {      /* Front Wheel */
            WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F;
        }
        else {
            WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_R;
        }

	  #if __VDC && __TCMF_CONTROL
	    if((ACTIVE_BRAKE_FLG==1) && (BRAKE_BY_MPRESS_FLG==0))
	    {
	        u8ActiveBrkLfcInitDuty = (uint8_t)LCABS_s16Interpolation2P(s16TcPressMin,MPRESS_20BAR,MPRESS_100BAR,(int16_t)U8_INIT_DUTY_ACTIVE_BRK_MIN, (int16_t)U8_INIT_DUTY_ACTIVE_BRK_MAX); 
	        if(WL->LFC_initial_set_duty > (int16_t)u8ActiveBrkLfcInitDuty)
	        {
	        	WL->LFC_initial_set_duty = (int16_t)u8ActiveBrkLfcInitDuty;
	        }
	    }
	  #endif        
    }
  /*#if   (__HDC || __ACC) && __TCMF_CONTROL2
    else if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
        if(lcu1HdcActiveFlg==1) {WL->LFC_initial_set_duty = 50;}
        else if(lcu1AccActiveFlg==1) {
            if(lcs16AccTargetG>=-30) {WL->LFC_initial_set_duty = 45;}
            else if(lcs16AccTargetG>=-50) {WL->LFC_initial_set_duty = 50;}
            else if(lcs16AccTargetG>=-70) {WL->LFC_initial_set_duty = 55;}
            else {WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F;}
        }
        else { }
    }
  #endif*/
    else { }

  #if __VDC && __MP_COMP
    WL->init_duty = WL->LFC_initial_set_duty + WL->LFC_MP_Cyclic_comp_duty
                    /*- (int16_t)(WL->LFC_pres_rise_delay_comp)*/ + (int16_t)(WL->LFC_special_comp_duty);
    tempW0 = 30;//PWM_duty_HOLD;
  #else
    if(REAR_WHEEL_wl==0) {      /* Front Wheel */
        WL->init_duty = WL->LFC_initial_set_duty
                        /*- (int16_t)(WL->LFC_pres_rise_delay_comp)*/ + (int16_t)(WL->LFC_special_comp_duty);
    }
    else {

        WL->init_duty = WL->LFC_initial_set_duty
                        /*- (int16_t)(WL->LFC_pres_rise_delay_comp)*/ + (int16_t)(WL->LFC_special_comp_duty);
    }

    /*if(REAR_WHEEL_wl==0) {tempW0 = (int16_t)U8_Initial_Duty_Ratio_F + (int16_t)U8_LFC_Initial_Duty_Comp_Low_F + 20;}
    else {tempW0 = (int16_t)U8_Initial_Duty_Ratio_R + (int16_t)U8_LFC_Initial_Duty_Comp_Low_R + 20;}*/

  #endif

   #if __ROUGH_ROAD_DUMP_IMPROVE
    if((lcabsu1TwoWlABSUneven==1) || (lcabsu1OneWlABSUneven==1))
    {
      if((WL->ABS==1) && (STABIL_wl==1) && (WL->LFC_STABIL_counter>=L_TIME_100MS) && (WL->rel_lam>=-LAM_3P))
      {
        WL->init_duty = WL->init_duty + 3;
      }
    }
   #endif

  #if (__SLIGHT_BRAKING_COMPENSATION)
    if(lcabsu1SlightBraking==1)
    {
        tempW1 = LCABS_s16Interpolation2P(mpress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS,10, 5);//15, 30
    }
    else
    {
        tempW1 = 5;//40;
    }
  #else  /*(__SLIGHT_BRAKING_COMPENSATION)*/
	  #if (__ROUGH_COMP_IMPROVE==ENABLE)
    if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1))
    {
    	tempW1 = 7;//20;
    }
	else 
	  #endif/*__ROUGH_COMP_IMPROVE*/
	{
    	tempW1 = 5;//40;
	}
  #endif /*(__SLIGHT_BRAKING_COMPENSATION)*/

  #if   __VDC
    #if 0//__HDC
    if(lcu1HdcActiveFlg==1)
    {
        tempW1 = 6;//30;
}
    #endif
    #if 0//__EPB_INTERFACE
    if(lcu1EpbActiveFlg==1)
    {
        tempW1 = 6;//30;
    }
    #endif
  #endif

    if(WL->init_duty <= tempW1 ) {
        /*WL->LFC_special_comp_duty += (int16_t)tempW1 - WL->init_duty;*/
        WL->init_duty=tempW1;
    }
    else if(WL->init_duty >= tempW0 ) {                         /* '04 SW Winter test*/
        /*WL->LFC_special_comp_duty -= WL->init_duty - tempW0;*/
        WL->init_duty = tempW0;
    }
    else { }

#if __VDC
    }
#endif
}
  #endif/*(__IDB_LOGIC==ENABLE)*/


void LAABS_vGenerateLFCDutyWL(struct W_STRUCT *WL)
{
    static int8_t s8MinConvenReapplyTim  = 3;  /* common */
    static int8_t s8MaxConvenReapplyTim  = 30; /* MGH-80 : 20ms, Gen3 : 30ms */
    
  #if (__UR_DUTY_COMP_IMPROVE==ENABLE)
    int8_t  s8URCompDutyPerLFCCycle=15;
    int8_t  s8URCompDutyPerURCycle=0;
  #endif /*(__UR_DUTY_COMP_IMPROVE==ENABLE)*/

  #if (NO_VV_ONOFF_CTRL==ENABLE)
	if(ABS_LIMIT_SPEED_wl==0)
	{
	    WL->LFC_Conven_OnOff_Flag = 1;
	}
	else
	{
		WL->LFC_Conven_OnOff_Flag = 0;
	}
  #else
    WL->LFC_Conven_OnOff_Flag = 0;
  #endif

#if __VDC
  if((WL->L_SLIP_CONTROL==0)&&(!BTC_fz)&&(!PBA_pulsedown))
#elif __BTC
  if(BTC_fz!=1)
#endif
  {

    if(WL->LFC_built_reapply_flag==0) { /* Not Built Reapply !!! */

        WL->LFC_s0_reapply_counter=0;
        if(HV_VL_wl==1) {
            if(AV_VL_wl==1) {           /* ************* Dump! ********* */
                WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
                WL->pwm_duty_temp=PWM_duty_DUMP;
            }
            else {                      /* ************* Hold!!! ******** */
                WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_Conven_Reapply_Time, -(U8_BASE_CTRLTIME*2));
/*
                if(WL->LFC_Conven_Reapply_Time > 7) {       // Reapply over 7ms (by CSH 2002 Winter)
                    WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time - 7;
                }
                else {
                    WL->LFC_Conven_Reapply_Time = 0;
                }
*/
              #if (__UNSTABLE_REAPPLY_ENABLE==ENABLE) && (NO_VV_ONOFF_CTRL==DISABLE)
                /*if((WL->Hold_Duty_Comp_flag==1) || (WL->Pres_Rise_Defer_flag==1)) {WL->pwm_duty_temp=PWM_duty_HOLD;}*/
                /*if((WL->Reapply_Accel_flag == 1) && (WL->Forced_Hold_After_Unst_Reapply==1))*/

                if(WL->Forced_Hold_After_Unst_Reapply==1)
                {
                    WL->pwm_temp = WL->init_duty + S8_DPRISE_FORCED_HOLD_DUTY;
                    WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
                    if((WL->UNSTABLE_LowMu_comp_flag==1) || (WL->UNSTABLE_LowMu_comp_flag2==1))
                    {
                        WL->pwm_duty_temp = WL->pwm_duty_temp + 10;
                    }
                  #if __VDC && __MP_COMP
                    WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);
                  #endif
                }
                else
              #endif
              #if __STABLE_DUMPHOLD_ENABLE
                if(WL->lcabsu1HoldAfterStabDumpFlg==1)
                {
                    WL->pwm_temp = PWM_duty_HOLD;
                    WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
                }
                else
              #endif

              #if (__HOLD_LFC_DUTY_CONTROL == ENABLE)
                if((WL->GMA_SLIP==1)&&(GMA_PULSE==1)&&(WL->s0_reapply_counter >= 1))
                {
                    WL->pwm_temp = U8_MIN_HOLD_DUTY;
                    WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
                }
                else
              #endif/*(__HOLD_LFC_DUTY_CONTROL == ENABLE)*/
                {
                  #if __EBD_LFC_MODE
                    if(WL->EBD_LFC_RISE_MODE==1) {
                        WL->pwm_temp=U8_EBD_LFC_INITIAL_DUTY - (int16_t)WL->s0_reapply_counter;
                        if(WL->pwm_temp<=0) {WL->pwm_temp=(int16_t)pwm_duty_null;}
                        WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
                    }
					else {WL->pwm_duty_temp=PWM_duty_HOLD;}
                  #else
                    WL->pwm_duty_temp=PWM_duty_HOLD;
                  #endif
                }
            }
        }
        else {                              /* ************ Reapply !!! */
          #if __ENABLE_202_DUTY
            if(AV_VL_wl==0) {
          #endif
              if(WL->LFC_Conven_OnOff_Flag==1) {  /* Added by CSH for LFC(03.06.17) */
                if(REAR_WHEEL_wl==1) {  /* Reapply over 7ms (by CSH 2002 Winter) */
                    if(ABS_fz==1)
                    {
	                  /*#if (__IDB_LOGIC==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=WL->init_duty;
	                  #else                    	
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
                      #endif*/
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;

                      #if (__SLIGHT_BRAKING_COMPENSATION)
                        WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, 
                                                            (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_MP_R), WL->LFC_Conven_Reapply_Time);
                        /* Further consideration for EBD */
                      #elif (__ROUGH_COMP_IMPROVE==ENABLE)
                        if(lcebdu1RoughSlightBrk==1)
                		{
                      		WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + 5;
                      	} 
					  #endif/*__SLIGHT_BRAKING_COMPENSATION*/
                    }
                  #if __CBC
                    else if(((LEFT_WHEEL_wl==1)&&(CBC_ACTIVE_rl==1))||((LEFT_WHEEL_wl==0)&&(CBC_ACTIVE_rr==1)))
                    {
                      #if __VDC  
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_R;
                      #else
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_ABS_R;
                      #endif
                    }
                    else if(((LEFT_WHEEL_wl==1)&&(CBC_FADE_OUT_rl==1))||((LEFT_WHEEL_wl==0)&&(CBC_FADE_OUT_rr==1)))
                    {
                      #if __VDC
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_FADEOUT_R;
                      #else
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_FADEOUT_ABS_R;
                      #endif
                    }
                  #endif
                  #if __SLS
                    else if(((LEFT_WHEEL_wl==1)&&(SLS_ACTIVE_rl==1))||((LEFT_WHEEL_wl==0)&&(SLS_ACTIVE_rr==1)))
                    {
                        WL->LFC_Conven_Reapply_Time=S8_SLS_REAPPLY_TIME_R;
                    }
                    else if(((LEFT_WHEEL_wl==1)&&(SLS_FADE_OUT_rl==1))||((LEFT_WHEEL_wl==0)&&(SLS_FADE_OUT_rr==1)))
                    {
                        WL->LFC_Conven_Reapply_Time=S8_SLS_REAPPLY_TIME_FADEOUT_R;
                    }
                  #endif   
                    else if(EBD_wl==1)
                    {
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R_EBD;
                    }
                    else
                    {
	                  /*#if (__IDB_LOGIC==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=WL->init_duty;
	                  #else                    	
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
                      #endif*/
                      	WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
                    }
                
                  #if __EBD_MI_ENABLE
                    if(WL->EBDMICtrlMode>=MI_LEVEL1)
                    {
                        if(WL->LFC_Conven_Reapply_Time>8)
                        {
                            WL->LFC_Conven_Reapply_Time=8;
                        }
                    }
                  #endif
                  
                  #if __HRB
                    if(lchrbu1ActiveFlag==1)
                    {
                        if(S8_HRB_ADD_REAPPLY_TIME > 0)
                        {
                        	WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + S8_HRB_ADD_REAPPLY_TIME;
                        }

                        if((lchrbu8PulseUpHoldScans == 0)&&(WL->LFC_Conven_Reapply_Time < U8_BASE_LOOPTIME))
                        {
                            WL->LFC_Conven_Reapply_Time = U8_BASE_LOOPTIME;
                        }
                    }
                  #endif
                }
                else{
                   if(GMA_SLIP_wl==1) {
                        if(vref>=S16_GMA_H_SPEED)
                        {
                            if(WL->s0_reapply_counter <= 1)
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE1;
                            }
                            else if(WL->s0_reapply_counter <= 4)
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE2;
                            }
                            else
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE3;
                            }
                        }
                        else if(vref>=S16_GMA_L_SPEED)
                        {
                            if(WL->s0_reapply_counter <= 1)
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE1;
                            }
                            else if(WL->s0_reapply_counter <= 4)
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE2;
                            }
                            else
                            {
                                WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE3;
                            }
                        }
                        else 
                        {
		                  /*if (__IDB_LOGIC==ENABLE)
		                	WL->LFC_Conven_Reapply_Time=WL->init_duty;
		                  #else	  
		                    WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
		                  #endif*/
		                  	WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
                        }

                      #if __HIGH_MPRESS_COMP
                        if((WL->LFC_Conven_Reapply_Time > 4) && (lcabsu1ValidBrakeSensor==1))
                        {
                            if(lcabss16RefCircuitPress > MPRESS_150BAR)
                            {
                                tempB5 = (int8_t)((MPRESS_150BAR - lcabss16RefCircuitPress)/MPRESS_30BAR); /* 1ms/10bar */

                            }
                            else if(lcabss16RefCircuitPress < MPRESS_100BAR)
                            {
                                tempB5 = (int8_t)((MPRESS_100BAR - lcabss16RefCircuitPress)/MPRESS_30BAR); /* 1ms/10bar */
                            }
                            else
                            {
                                tempB5 = 0;
                            }
                            WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + tempB5;
                        }
                        /*
                        if((lcabss16RefCircuitPress > MPRESS_150BAR) && (WL->LFC_Conven_Reapply_Time > 4))
                        {


                        }
                        */
                        
                          #if __EPB_INTERFACE
                       if(lcu1EpbActiveFlg==1)
                       {
                           WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P((-ebd_filt_grv),AFZ_0G2,AFZ_0G5,
                                                         (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_DECEL),WL->LFC_Conven_Reapply_Time);
                       }
                         #endif  
                                          
                      #else
                        if(WL->s0_reapply_counter>12) {WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + 1;}
                      #endif
                    }
                    else 
                    {
	                  /*#if (__IDB_LOGIC==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=WL->init_duty;
	                  #else*/	  
	                    WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
	                  /*#endif*/                    	
                    }                  

                  #if (__SLIGHT_BRAKING_COMPENSATION)
                    WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, 
                                                         (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_MP_F), WL->LFC_Conven_Reapply_Time);
                  #endif

                  #if __EPB_INTERFACE
                    if(lcu1EpbActiveFlg==1)
                    {
                        WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P((-ebd_filt_grv),AFZ_0G2,AFZ_0G5,
                                                         (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_DECEL),WL->LFC_Conven_Reapply_Time);
                    }
                  #endif  
					WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_Conven_Reapply_Time,(int16_t)s8MinConvenReapplyTim,(int16_t)s8MaxConvenReapplyTim);
                }
              }
            #if __EBD_LFC_MODE
              else if(WL->EBD_LFC_RISE_MODE==1) {
                    WL->LFC_Conven_Reapply_Time=0;
              }
            #endif
            #if __UNSTABLE_REAPPLY_ENABLE
              else if(WL->Reapply_Accel_flag == 1)
              {
                #if (NO_VV_ONOFF_CTRL==ENABLE)
                  WL->LFC_Conven_Reapply_Time=U8_DEFAULT_RISESCANTIME;
                #else
                    WL->LFC_Conven_Reapply_Time=0;
                #endif
              }
            #endif
              else {
                WL->LFC_Conven_Reapply_Time=U8_DEFAULT_RISESCANTIME;
              }

            #if (__UNSTABLE_REAPPLY_ENABLE == ENABLE)&&(NO_VV_ONOFF_CTRL == DISABLE)
              if(WL->Reapply_Accel_flag == 1)
              {
                if(WL->init_duty >= S8_DPRISE_COMP_DUTY) {
                  #if __UNSTABLE_REAPPLY_ADAPTATION
                    if((WL->UNSTABLE_LowMu_comp_flag==0) && (WL->Unstable_Rise_Pres_Cycle_cnt>=1))
                    {
                      #if (__UR_DUTY_COMP_IMPROVE==1)

                        if(WL->LFC_zyklus_z<=1)
                        {
                            s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY-5;
                        }
                        else if(WL->LFC_zyklus_z<=3)
                        {
                            s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY-3;
                        }
                        else
                        {
                        	s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY;
                        	            
                        	if(((LFC_Split_flag==1)&&(REAR_WHEEL_wl==0)&&(((MSL_BASE_rl==1)&&(LEFT_WHEEL_wl==0)) || ((MSL_BASE_rr==1)&&(LEFT_WHEEL_wl==1))))
                        		|| ((lcabsu1CertainHMu==1)&&(REAR_WHEEL_wl==0)))
                        	{
                        		if(WL->Unst_Reapply_Scan_Ref>=L_TIME_30MS)
                        		{
                            		s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY + 5;
                            	}
                            }  
                        }

						s8URCompDutyPerLFCCycle = (s8URCompDutyPerLFCCycle<5)?5:s8URCompDutyPerLFCCycle;

                        if(WL->Unstable_Rise_Pres_Cycle_cnt>=3)
                        {
                            s8URCompDutyPerURCycle = 5;
                        }
                        else if(WL->Unstable_Rise_Pres_Cycle_cnt>=2)
                        {
                            s8URCompDutyPerURCycle = 3;
                        }
                        else
                        {
                            s8URCompDutyPerURCycle = 0;
                        }
                        
                        
                        WL->pwm_duty_temp=(uint8_t)(WL->init_duty - s8URCompDutyPerLFCCycle + s8URCompDutyPerURCycle);

                      #else /* (__UR_DUTY_COMP_IMPROVE==1)*/

                        if((WL->LFC_zyklus_z<=1) || (WL->Unstable_Rise_Pres_Cycle_cnt>=3))
                        {
                            WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY + 5);
                        }
                        else if(WL->Unstable_Rise_Pres_Cycle_cnt>=2)
                        {
                            WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY + 3);
                        }
                        else {WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);}
                      #endif /* (__UR_DUTY_COMP_IMPROVE==1)*/
                    }
                    else {WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);}
                  #else
                    WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);
                  #endif
                }
                else {
                    WL->pwm_duty_temp=(uint8_t)WL->init_duty;
                }

              #if __DETECT_MU_FOR_UR
                if((WL->lcabsu1LMuForUR==1)||(WL->UNSTABLE_LowMu_comp_flag==1))
              #else
                if(WL->UNSTABLE_LowMu_comp_flag==1)
              #endif
                {
                    WL->pwm_duty_temp = WL->pwm_duty_temp + 10;
                    /*
				      #if __ABS_RBC_COOPERATION
				    if(lcabss16RBCPressBfABS>0) {WL->pwm_duty_temp = WL->pwm_duty_temp - 5;}
				      #endif 
	    	        */
	    	          #if (__SLIGHT_BRAKING_COMPENSATION)
			        if(lcabsu1SlightBraking==1) {WL->pwm_duty_temp = WL->pwm_duty_temp - 5;}
		              #endif  
                    
                  #if __VDC && __MP_COMP
                    if(WL->Unstable_Rise_MP_comp_duty < 0)
                    {
                        if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
                        else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
                    }
                    else if(WL->Unstable_Rise_MP_comp_duty > 10)
                    {
                        WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty-10);
                    }
                    else { }
                  #endif
                }
                else if(WL->UNSTABLE_LowMu_comp_flag2==1)
                {
                    WL->pwm_duty_temp = WL->pwm_duty_temp + 5;
                  #if __VDC && __MP_COMP
                    if(WL->Unstable_Rise_MP_comp_duty < 0)
                    {
                        if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
                        else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
                    }
                    else if(WL->Unstable_Rise_MP_comp_duty > 5)
                    {
                        WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty-5);
                    }
                    else { }
                  #endif
                }
                else
                {
                  #if __VDC && __MP_COMP
                    if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
                    else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
                  #else
                    ;
                  #endif
                }

		      #if __ABS_RBC_COOPERATION
			    if(lcabss16RBCPressBfABS>0) /*  && (REAR_WHEEL_wl==0) */
			    {
			        if(WL->UNSTABLE_LowMu_comp_flag==1)
			        {
			            WL->pwm_duty_temp = WL->pwm_duty_temp - 5;
			        }
			        else
			        {
                      #if(__HEV_MP_MOVING_AVG==ENABLE)
			            if(laabss16MPAvg<=MPRESS_50BAR)
                      #else
			            if(lcabss16RefCircuitPress<=MPRESS_50BAR)
					  #endif	
			            {
    			            WL->pwm_duty_temp = pwm_duty_null;
			            }
			            else
			            {
    			            WL->pwm_duty_temp = (WL->pwm_duty_temp > 40)? (WL->pwm_duty_temp - 40) : 0;
			            }
			        }
			    }
		      #endif
              }
              else {
            #endif
                #if __EBD_LFC_MODE
                  if(WL->EBD_LFC_RISE_MODE==1) {
    /*              if(WL->pwm_duty_temp>0) WL->pwm_duty_temp=(uint8_t)70 - WL->s0_reapply_counter; */
    /*              else WL->pwm_duty_temp=pwm_duty_null; */
                    WL->pwm_temp=U8_EBD_LFC_INITIAL_DUTY - (int16_t)WL->s0_reapply_counter;
                    if(WL->pwm_temp<=0) {WL->pwm_temp=(int16_t)pwm_duty_null;}
                    WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
                  }
                  else {WL->pwm_duty_temp = pwm_duty_null;}
                #else
                  WL->pwm_duty_temp = pwm_duty_null;
                #endif
            #if (__UNSTABLE_REAPPLY_ENABLE == ENABLE)&&(NO_VV_ONOFF_CTRL == DISABLE)
              }
            #endif
          #if __ENABLE_202_DUTY
            }
            else {        /* if (AV_VL_wl==1){ */
                WL->pwm_duty_temp = pwm_duty_null;
                #if __BDW
                    if (BDW_ON==1){
                    WL->pwm_duty_temp = PWM_duty_CIRC;
                    WL->LFC_Conven_Reapply_Time=0;
                }
                #endif
                #if __EBP
                    if (EBP_ON==1){
                    WL->pwm_duty_temp = PWM_duty_CIRC;
                    WL->LFC_Conven_Reapply_Time=0;
                }
                #endif
                #if __ENABLE_PREFILL
                    if (EPC_ON==1){
                    WL->pwm_duty_temp = PWM_duty_CIRC;
                    WL->LFC_Conven_Reapply_Time=0;
                }
                #endif
            }
          #endif    /* ~ __ENABLE_202_DUTY */
        }
    }
    else {          /* Built Reapply !!! */
        if(WL->LFC_Conven_OnOff_Flag==1) { /* Added by CSH for LFC(02.01.11) */

		  #if (NO_VV_ONOFF_CTRL == DISABLE)
            WL->LFC_s0_reapply_counter=0;
          #endif

            if(HV_VL_wl==1) {
                if(AV_VL_wl==1) {           /* ************* Dump! ********* */
                    WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
                    WL->pwm_duty_temp=PWM_duty_DUMP;
                }
                else {                      /* ************* Hold!!! ******** */
                    WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_Conven_Reapply_Time, -(U8_BASE_LOOPTIME));
                    WL->pwm_duty_temp=PWM_duty_HOLD;
                }
            }
            else {                          /* ************ Reapply !!! */
                if(REAR_WHEEL_wl==1)
                {  /* Reapply over 7ms (by CSH 2002 Winter) */
                    if(ABS_fz==1)
                    {
	                  #if (NO_VV_ONOFF_CTRL==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
	                	/*if((WL->init_duty-(int8_t)WL->LFC_s0_reapply_counter) > s8MinConvenReapplyTim)
	                	{
	                		WL->LFC_Conven_Reapply_Time = WL->init_duty - (int8_t)WL->LFC_s0_reapply_counter;
	                	}
	                	else
	                	{
	                		WL->LFC_Conven_Reapply_Time = s8MinConvenReapplyTim;
	                	}*/
						  #if NO_VV_ONOFF_CTRL
			            if(lcabsu1StrkRecvrCtrlReq == 1)
			            {
			            	WL->LFC_Conven_Reapply_Time=0;
			            	WL->pwm_duty_temp=PWM_duty_HOLD;
			            }
             			 #endif/*#if __IDB_LOGIC*/ 
	                  #else                    	
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
                      #endif
                    }
                  #if __CBC
                    else if(((LEFT_WHEEL_wl==1)&&(CBC_ACTIVE_rl==1))||((LEFT_WHEEL_wl==0)&&(CBC_ACTIVE_rr==1)))
                    {
                      #if __VDC  
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_R;
                      #else
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_ABS_R;
                      #endif
                    }
                    else if(((LEFT_WHEEL_wl==1)&&(CBC_FADE_OUT_rl==1))||((LEFT_WHEEL_wl==0)&&(CBC_FADE_OUT_rr==1)))
                    {
                      #if __VDC
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_FADEOUT_R;
                      #else
                        WL->LFC_Conven_Reapply_Time=S8_CBC_REAPPLY_TIME_FADEOUT_ABS_R;
                      #endif
                    }
                  #endif
                  #if __SLS
                    else if(((LEFT_WHEEL_wl==1)&&(SLS_ACTIVE_rl==1))||((LEFT_WHEEL_wl==0)&&(SLS_ACTIVE_rr==1)))
                    {
                        WL->LFC_Conven_Reapply_Time=S8_SLS_REAPPLY_TIME_R;
                    }
                    else if(((LEFT_WHEEL_wl==1)&&(SLS_FADE_OUT_rl==1))||((LEFT_WHEEL_wl==0)&&(SLS_FADE_OUT_rr==1)))
                    {
                        WL->LFC_Conven_Reapply_Time=S8_SLS_REAPPLY_TIME_FADEOUT_R;
                    }
                  #endif   
                    else if(EBD_wl==1)
                    {
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R_EBD;
                    }
                    else
                    {
	                  /*#if (__IDB_LOGIC==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=WL->init_duty;
	                  #else*/                    	
                        WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
                      /*#endif*/
                    }
                }
                else
                {
                	if(ABS_fz==1)
                	{
                  #if (NO_VV_ONOFF_CTRL==ENABLE)
	                	WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
	                	/*if((WL->init_duty-(int8_t)WL->LFC_s0_reapply_counter) > s8MinConvenReapplyTim)
	                	{
	                		WL->LFC_Conven_Reapply_Time = WL->init_duty - (int8_t)WL->LFC_s0_reapply_counter;
	                	}
	                	else
	                	{
	                		WL->LFC_Conven_Reapply_Time = s8MinConvenReapplyTim;
	                	}*/
                  #else	  
                    WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
                  #endif
                }
	              	else
	              	{
	              		WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
	              	}
                }
                WL->pwm_duty_temp = pwm_duty_null;
                WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_Conven_Reapply_Time,(int16_t)s8MinConvenReapplyTim,(int16_t)s8MaxConvenReapplyTim);
            }
        }
        else {                  /* Built Reapply !!! */
            WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */

/* Changed By CSH 030624 */
		  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
			if(WL->LFC_s0_reapply_counter==1)
			{
				WL->pwm_temp = WL->init_duty;
			}
			else
			{
				WL->pwm_temp = WL->init_duty - (((int16_t)WL->LFC_s0_reapply_counter*2) - 2);
			}
		  #else
		    //WL->pwm_temp = WL->init_duty - ((int16_t)WL->LFC_s0_reapply_counter);
		    WL->pwm_temp = 80 - ((int16_t)WL->LFC_s0_reapply_counter);
		  #endif/*(__MGH80_LFC_PATTERN_CHANGE == ENABLE)*/
                /*
                if((REAR_WHEEL_wl==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN)
               #if __UNSTABLE_REAPPLY_ENABLE
                  && (WL->Reapply_Accel_cycle==0)
                 #endif
                  )
                {
                  WL->pwm_temp = WL->pwm_temp - 5;
                }
                */
			  #if NO_VV_ONOFF_CTRL
            if((REAR_WHEEL_wl == 1) && (lcabsu1StrkRecvrCtrlReq == 1))
            {
            	WL->pwm_temp = PWM_duty_HOLD;
            }
              #endif/*#if __IDB_LOGIC*/

                if(WL->pwm_temp<=0) {WL->LFC_reapply_current_duty = 0;}
                else {WL->LFC_reapply_current_duty = (uint8_t)WL->pwm_temp;}

            #if __WP_DROP_COMP
              if((WL->WP_Drop_comp_flag==1) && (WL->LFC_s0_reapply_counter==1)) {
                  WL->pwm_temp += WL->LFC_WP_Drop_comp_duty;
                  WL->MSL_long_hold_flag=0;
              }
              else {
            #endif
                if(((WL->timer_keeping_LFC_duty >=(uint8_t)(WL->hold_timer_new_ref2+L_1SCAN))||(WL->Hold_Duty_Comp_flag==1)) /* '03 USA High Mu */
                	&&(ABS_LIMIT_SPEED_wl==0))
                {
                     if(REAR_WHEEL_wl==0) {WL->pwm_temp += (int16_t)U8_LFC_Long_Hold_Comp_Duty_F;}
                     else {WL->pwm_temp += (int16_t)U8_LFC_Long_Hold_Comp_Duty_R;}
                     WL->MSL_long_hold_flag=1;
                }
                else {
                     WL->MSL_long_hold_flag=0;
                }
            #if __WP_DROP_COMP
              }
            #endif
/* Changed By CSH 030624 */
            if(WL->pwm_temp <= 0 ) {
                WL->pwm_temp=0;
            }
            else if(WL->pwm_temp >= (int16_t)PWM_duty_HOLD ) {
               WL->pwm_temp=PWM_duty_HOLD;
            }
            else { }

            WL->pwm_duty_temp = (uint8_t)WL->pwm_temp;
        }
    }

#if !(__VDC && __PRESS_EST)
//    WL->LFC_Conven_OnOff_Flag=0;        /* Added by CSH for LFC(02.01.11) */
  #if NO_VV_ONOFF_CTRL
    WL->LFC_Conven_OnOff_Flag=1;
  #else
  	WL->LFC_Conven_OnOff_Flag=0;
  #endif
#endif
/*--------------------------------------------------------------------------------------------
*                  Added by J.K.Lee at 03 SW_winter
*
* If ESP control ended as mode 1 or 2, Forcible Hold mode is added before ABS built reaaply
*
* mode 1: reapply, 2: built reapply, 3: Hold, 4: dump
*--------------------------------------------------------------------------------------------*/

#if __VDC   /* ESP to LFC assistance control */

    if(WL->LFC_to_ESP_end_flag==1){
        if(WL->ESP_end_counter_final < L_TIME_20MS){
            if(WL->esp_final_mode <= 2){
                WL->pwm_duty_temp = PWM_duty_HOLD;
                WL->ESP_to_LFC_hold_timer = 1;
            }
        }
    }
    else{
        if(WL->ESP_end_counter_final < L_TIME_20MS){
            if(WL->esp_final_mode == 1){
                if(WL->ESP_to_LFC_hold_timer > 0){
                    WL->pwm_duty_temp = PWM_duty_HOLD;
                }
            }
        }
        if(WL->ESP_to_LFC_hold_timer != 0) {WL->ESP_to_LFC_hold_timer = 0;}
    }

#if ESC_PULSE_UP_RISE
    WL->pulseup_cycle_count = 0;
    WL->pulseup_hold_cycle_count = 0;
#endif
#endif

/*-------------------------------------------------------------------------------------------- */


  }
  #if ((__AHB_GEN3_SYSTEM==ENABLE) || (NO_VV_ONOFF_CTRL == ENABLE))
	if(WL->pwm_duty_temp < PWM_duty_HOLD)
	{
		WL->laabsu1Rise = 1;
	}
	else
	{
		WL->laabsu1Rise = 0;
	}
  #endif
}

#if __CHANGE_MU
void LAABS_vLimitDiffofRearLFCDuty(void)
{
    if((RL.LFC_reapply_end_duty==0)&&(RR.LFC_reapply_end_duty==0)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0)) {
        if((RL.LFC_Conven_OnOff_Flag==0)&&(RR.LFC_Conven_OnOff_Flag==0)) {

            if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_built_reapply_flag==1)) {

                tempC1 = 1;
              #if __LOWMU_REAR_1stCYCLE_RISE_DELAY
                if(lcabsu1Rear1stCycleRiseDelay==1)
                {
                	tempC1 = 5;
                	if(MSL_BASE_rl==1)
                	{
                		if(RL.LFC_s0_reapply_counter<=tempC1)
                		{
                            RR.LFC_reapply_current_duty = 0;
                            if(RR.LFC_built_reapply_counter>=L_2SCAN) {RR.LFC_built_reapply_counter = L_2SCAN;}
                            RR.LFC_s0_reapply_counter = 1;
                            RR.pwm_duty_temp=PWM_duty_HOLD;
                        }
                        else
                        {
                        	LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
                        }
                	}                	
                	if(MSL_BASE_rr==1)
                	{
                		if(RR.LFC_s0_reapply_counter<=tempC1)
                		{
                            RL.LFC_reapply_current_duty = 0;
                            if(RL.LFC_built_reapply_counter>=L_2SCAN) {RL.LFC_built_reapply_counter = L_2SCAN;}
                            RL.LFC_s0_reapply_counter = 1;
                            RL.pwm_duty_temp=PWM_duty_HOLD;                		
                        }
                        else
                        {
                        	LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
                        }
                	}
                }
                else
              #endif  
                if(RR.LFC_reapply_current_duty > RL.LFC_reapply_current_duty) {
                    LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
                }
                else if(RL.LFC_reapply_current_duty > RR.LFC_reapply_current_duty) {
                    LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
                }
                else { }
            }
/*          else if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_zyklus_z<=0)) { */
            else if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_zyklus_z<=0)&&(ABS_rr==1)&&(vref>=VREF_10_KPH)) {
                RL.LFC_reapply_current_duty = 0;
                if(RL.LFC_built_reapply_counter>=L_2SCAN) {RL.LFC_built_reapply_counter = L_2SCAN;}
                RL.LFC_s0_reapply_counter = 1;

              #if __UNSTABLE_REAPPLY_ENABLE
                if((RR.Reapply_Accel_flag==1) && (RR.pwm_duty_temp>0) && (RR.pwm_duty_temp<=PWM_duty_HOLD))
                {
                    RL.pwm_duty_temp = RR.pwm_duty_temp;
                }
                else
              #endif
                {
                    RL.pwm_duty_temp=PWM_duty_HOLD;
                }
            }

/*          else if((RR.LFC_built_reapply_flag==1)&&(RL.LFC_zyklus_z<=0)) { */
            else if((RR.LFC_built_reapply_flag==1)&&(RL.LFC_zyklus_z<=0)&&(ABS_rl==1)&&(vref>=VREF_10_KPH)) {
                RR.LFC_reapply_current_duty = 0;
                if(RR.LFC_built_reapply_counter>=L_2SCAN) {RR.LFC_built_reapply_counter = L_2SCAN;}
                RR.LFC_s0_reapply_counter = 1;

              #if __UNSTABLE_REAPPLY_ENABLE
                if((RL.Reapply_Accel_flag==1) && (RL.pwm_duty_temp>0) && (RL.pwm_duty_temp<=PWM_duty_HOLD))
                {
                    RR.pwm_duty_temp = RL.pwm_duty_temp;
                }
                else
              #endif
                {
                    RR.pwm_duty_temp=PWM_duty_HOLD;
                }
            }
            else { }
        }
    }
    else {

        if((RL.LFC_built_reapply_flag==1)&&(RL.LFC_Conven_OnOff_Flag==0)&&(MSL_BASE_rr==1)) {
            if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)
          #if __SELECT_LOW_FOR_LMU
            ||(lcabsu1SelectLowforNonSplit==1)
          #endif
            ){

                LAABS_vLimitDiffofRearAtSplit(&RL, &RR);
            }
            else {
                tempC1=1;
                LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
            }
        }

        if((RR.LFC_built_reapply_flag==1)&&(RR.LFC_Conven_OnOff_Flag==0)&&(MSL_BASE_rl==1)) {
            if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)
          #if __SELECT_LOW_FOR_LMU
            ||(lcabsu1SelectLowforNonSplit==1)
          #endif
            ){

                LAABS_vLimitDiffofRearAtSplit(&RR, &RL);
            }
            else {
                tempC1=1;
                LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
            }
        }

      #if __UNSTABLE_REAPPLY_ENABLE
        if((RL.LFC_built_reapply_flag==0) && (RL.SL_Unstable_Rise_flg==1))
        {
            RL.pwm_duty_temp = RR.pwm_duty_temp;
            RL.LFC_Conven_Reapply_Time = 0;
            RL.SL_Unstable_Rise_flg = 0;
        }
        else if((RR.LFC_built_reapply_flag==0) && (RR.SL_Unstable_Rise_flg==1))
        {
            RR.pwm_duty_temp = RL.pwm_duty_temp;
            RR.LFC_Conven_Reapply_Time = 0;
            RR.SL_Unstable_Rise_flg = 0;
        }
        else { }
      /*
        if((RL.LFC_built_reapply_flag==0)&&(RL.SPLIT_PULSE==1)&&(MSL_BASE_rr==1)&&(RL.state==DETECTION))
        {
            if(RR.Reapply_Accel_flag == 1)
            {
                RL.pwm_duty_temp = RR.pwm_duty_temp;
                RL.LFC_Conven_Reapply_Time = 0;
            }
        }
        else if((RR.LFC_built_reapply_flag==0)&&(RR.SPLIT_PULSE==1)&&(MSL_BASE_rl==1)&&(RR.state==DETECTION))
        {
            if(RL.Reapply_Accel_flag == 1)
            {
                RR.pwm_duty_temp = RL.pwm_duty_temp;
                RR.LFC_Conven_Reapply_Time = 0;
            }
        }
        else { }
      */
      #endif

    }
  #if __REORGANIZE_ABS_FOR_MGH60
    if((RL.ABS_LIMIT_SPEED==1) && (RR.ABS_LIMIT_SPEED==1)) 
    {
        LAABS_vSyncBothWheelDuty(&RL, &RR);
    }
  #endif 
}

void LAABS_vLimitDiffofRearAtSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE)
{
    if(pBASE->LFC_built_reapply_flag==1){
	  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
		if(pBASE->LFC_s0_reapply_counter==1)
		{
			pNBASE->pwm_temp = pBASE->init_duty;
		}
		else
		{
			pNBASE->pwm_temp = pBASE->init_duty - (((int16_t)pBASE->LFC_s0_reapply_counter*2) - 2);
		}
	  #else
	  	pNBASE->pwm_temp = (int16_t)pBASE->init_duty - (int16_t)(pBASE->LFC_s0_reapply_counter);
	  #endif  /*(__MGH80_LFC_PATTERN_CHANGE == ENABLE)*/
        if(pNBASE->pwm_temp<=0) {pNBASE->LFC_reapply_current_duty =0; pNBASE->pwm_temp = 0;}
        else {pNBASE->LFC_reapply_current_duty = (uint8_t)pNBASE->pwm_temp;}
        pNBASE->LFC_pres_rise_delay_comp = pBASE->LFC_pres_rise_delay_comp;
        pNBASE->LFC_special_comp_duty = pBASE->LFC_special_comp_duty;
      #if __VDC && __MP_COMP
        pNBASE->LFC_MP_Cyclic_comp_duty = pBASE->LFC_MP_Cyclic_comp_duty; /*2005.07.31 NZ winter */
      #endif
        pNBASE->LFC_s0_reapply_counter = pBASE->LFC_s0_reapply_counter;
        if((pBASE->LFC_built_reapply_counter>=L_2SCAN) || (pNBASE->LFC_built_reapply_counter<=0)) {
            pNBASE->LFC_built_reapply_counter = pBASE->LFC_built_reapply_counter;
        }
        else {pNBASE->LFC_built_reapply_counter = L_2SCAN;}
        if(pBASE->MSL_long_hold_flag==1) {
            pNBASE->pwm_temp = (int16_t)pNBASE->LFC_reapply_current_duty + (int16_t)U8_LFC_Long_Hold_Comp_Duty_R;
        }
        pNBASE->pwm_duty_temp = (uint8_t)pNBASE->pwm_temp;
    }
    else {
      #if !__MIRC_IMPROVE
        pNBASE->pwm_duty_temp = pBASE->pwm_duty_temp;
      #else
        if(pBASE->LFC_zyklus_z<1)
        {
        	pNBASE->pwm_duty_temp = pBASE->pwm_duty_temp;
        }	
        else if(pNBASE->MSL_long_hold_flag==0)
        {
            pNBASE->pwm_duty_temp = pNBASE->pwm_duty_temp + U8_LFC_Long_Hold_Comp_Duty_R;
        }
        else{}
      #endif
    }
}

void LAABS_vLimitDiffofRearAtnSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE)
{
    int16_t BASE_LFC_Rep_duty;

  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
  	tempC1 = tempC1*2;
  #endif
  
    if((pBASE->LFC_reapply_current_duty > pBASE->LFC_reapply_end_duty)&&(pBASE->LFC_reapply_end_duty>0)) {
        BASE_LFC_Rep_duty = (int16_t)(pBASE->LFC_reapply_end_duty - tempC1);
    }
    else if(pBASE->LFC_reapply_current_duty>tempC1) {
        BASE_LFC_Rep_duty = (int16_t)(pBASE->LFC_reapply_current_duty - tempC1);
    }
    else {BASE_LFC_Rep_duty = 0;}

    if(pNBASE->LFC_reapply_current_duty < (uint8_t)BASE_LFC_Rep_duty) {
        if(pNBASE->AV_HOLD==0){
            if(pNBASE->LFC_s0_reapply_counter==1){
                pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty-(int16_t)pNBASE->LFC_reapply_current_duty);
            }
            else if(pNBASE->LFC_s0_reapply_counter>1) {
                pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty-(int16_t)pNBASE->LFC_reapply_current_duty-1);
                pNBASE->LFC_s0_reapply_counter--;
                pNBASE->LFC_built_reapply_counter = pNBASE->LFC_built_reapply_counter - L_1SCAN;
                pNBASE->Duty_Limitation_flag=1;
            }
            else { }
        }
        else {
            pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty - (int16_t)pNBASE->LFC_reapply_current_duty);
            if(pNBASE->LFC_s0_reapply_counter>1) {pNBASE->Duty_Limitation_flag=1;}
        }

        pNBASE->LFC_reapply_current_duty = (uint8_t)BASE_LFC_Rep_duty;
        pNBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty;
        if(pNBASE->MSL_long_hold_flag==1) {
            pNBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty + (uint8_t)U8_LFC_Long_Hold_Comp_Duty_R;
        }
        if((pBASE->LFC_initial_deep_slip_comp_flag==1) && (pNBASE->LFC_initial_deep_slip_comp_flag==0)) {   /* '04 SW */
            pNBASE->LFC_initial_deep_slip_comp_flag=1;
        }
        
      #if __VDC && __MP_COMP
        if(pNBASE->LFC_zyklus_z<=1)
        {
        	if((pBASE->MP_Init - pNBASE->MP_Init) >= MPRESS_10BAR)
        	{
        		pNBASE->MP_Init = pBASE->MP_Init - MPRESS_10BAR;
        	}
        	else if((pBASE->MP_Init - pNBASE->MP_Init) <= -MPRESS_10BAR)
        	{
        		pNBASE->MP_Init = pBASE->MP_Init + MPRESS_10BAR;
        	}
        	else { }
        }
      #endif        
    }

    if((pBASE->STABIL==0) && (pBASE->s_diff<=0)) {  /*if(pBASE->STABIL==0) */
        if(pNBASE->Duty_Limitation_flag==1) {pNBASE->SPLIT_PULSE = 1;}
    }
    else{
        if(pNBASE->SPLIT_PULSE == 1) {pNBASE->SPLIT_PULSE = 0;}
    }

  #if (__REORGANIZE_ABS_FOR_MGH60==0)
    if((pBASE->ABS_LIMIT_SPEED==1) && (pNBASE->ABS_LIMIT_SPEED==1)) {
        if((pBASE->LFC_built_reapply_flag==1) && (pNBASE->LFC_built_reapply_flag==1)) {
            if((pBASE->LFC_reapply_current_duty+1) < pNBASE->LFC_reapply_current_duty) {
                pNBASE->LFC_special_comp_duty -= (int16_t)(pNBASE->LFC_reapply_current_duty - pBASE->LFC_reapply_current_duty - 1);
                pNBASE->pwm_duty_temp = pBASE->LFC_reapply_current_duty;
            }
            else if((pNBASE->LFC_reapply_current_duty+1) < pBASE->LFC_reapply_current_duty) {
                pBASE->LFC_special_comp_duty -= (int16_t)(pBASE->LFC_reapply_current_duty - pNBASE->LFC_reapply_current_duty - 1);
                pBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty;
            }
            else { }
        }
    }
  #endif    
}
#endif


void LAABS_vCompCurrentAndRefV(void)
{

    LAABS_vMonitorSolResistance();

/*******************************************************/
/*  tempW0 = ign_mon_ad;    // Changed '02 NZ Winter -- Less Noisy!!! */
    //LCK tempW0 = (int16_t)ref_mon_ad;    /* data to be filtered */
    Filtered_ref_mon_ad = LCESP_s16Lpf1Int(tempW0,Filtered_ref_mon_ad,L_U8FILTER_GAIN_5MSLOOP_10HZ);
  #if __REORGANIZE_ABS_FOR_MGH60  
    Filtered_ref_mon_ad = LCABS_s16LimitMinMax(Filtered_ref_mon_ad, IGN_9V0, IGN_16V);    
  #endif  
/*******************************************************/

    LAABS_vCompCurrentAndRefVWL(&FL);
    LAABS_vCompCurrentAndRefVWL(&FR);
    LAABS_vCompCurrentAndRefVWL(&RL);
    LAABS_vCompCurrentAndRefVWL(&RR);

}


void LAABS_vCompCurrentAndRefVWL(struct W_STRUCT *WL)
{
  /*#if __REORGANIZE_FOR_MGH60_CYCLETIM
    uint16_t  u16Temp1, u16Temp2, u16Temp3;
  #endif*/
    
  #if __REORGANIZE_ABS_FOR_MGH60	
    WL->LFC_NO_Hmon = (uint16_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_NO_Hmon, (int16_t)NO_HMON_LOWER_LIMIT, (int16_t)NO_HMON_UPPER_LIMIT);    
    WL->LFC_NO_Lmon = (uint16_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_NO_Lmon, (int16_t)NO_LMON_LOWER_LIMIT, (int16_t)NO_LMON_UPPER_LIMIT);    
  #else
    if(Filtered_ref_mon_ad > IGN_16V) {Filtered_ref_mon_ad = IGN_16V;}
    else if(Filtered_ref_mon_ad < IGN_9V0) {Filtered_ref_mon_ad=IGN_9V0;}
    else { }
    /*Reference voltage의 min, max 제한  */

    if(WL->LFC_NO_Hmon > NO_HMON_UPPER_LIMIT)   {WL->LFC_NO_Hmon=NO_HMON_UPPER_LIMIT;}
    else if(WL->LFC_NO_Hmon < NO_HMON_LOWER_LIMIT) {WL->LFC_NO_Hmon=NO_HMON_LOWER_LIMIT;}
    else { }
    if(WL->LFC_NO_Lmon > NO_LMON_UPPER_LIMIT)   {WL->LFC_NO_Lmon=NO_LMON_UPPER_LIMIT;}
    else if(WL->LFC_NO_Lmon < NO_LMON_LOWER_LIMIT) {WL->LFC_NO_Lmon=NO_LMON_LOWER_LIMIT;}
    else { }
    /*Shunt voltage의 min, max 제한 */
  #endif


    /*WL->pwm_temp_dash의  변수형을 unsigned로 두었을 때 0~65535의 값을 가질 수 있다. */

    /* Hmon, Lmon 구분되는 경우 */
    /*Shunt voltage의 min, max 제한필요 : Diode의 voltage drop은 얼마정도? */
    if((WL->LFC_built_reapply_flag==1) || (ABS_wl==1)
    )
    {      /* 마지막 Pulse_Up을 고려함...02.07.29  */

      /*#if (!__REORGANIZE_FOR_MGH60_CYCLETIM)*/	
        tempW0 = Filtered_ref_mon_ad;
        tempC1 = WL->pwm_duty_temp;
      /*#endif  */

        if(WL->pwm_duty_temp < PWM_duty_HOLD) {
/*          WL->pwm_duty_temp_dash = (uint8_t)((((((ULONG)(VOLT_RESISTER_CONST*WL->LFC_NO_Lmon)/(ULONG)(1024-WL->LFC_NO_Hmon))*128)/(ULONG)tempW0)*(ULONG)tempC1)/128); */

		  /*#if (!__REORGANIZE_FOR_MGH60_CYCLETIM)*/
            WL->pwm_duty_temp_dash = (uint8_t)(((((((uint32_t)VOLT_RESISTER_CONST*WL->LFC_NO_Lmon)/(VCC_ADC_VOLT-WL->LFC_NO_Hmon))*AD_SCALE_FACTOR)/(uint32_t)tempW0)*(uint32_t)tempC1)/AD_SCALE_FACTOR); /*4096: ADC resolution, 128:truncation scale factor*/
            if(WL->pwm_duty_temp_dash>=PWM_duty_HOLD) {WL->pwm_duty_temp_dash = PWM_duty_HOLD;}
          /*#else  
		    u16Temp1 = (uint16_t)(((uint32_t)VOLT_RESISTER_CONST*WL->LFC_NO_Lmon)/(uint16_t)(VCC_ADC_VOLT-WL->LFC_NO_Hmon));
            u16Temp2 = (uint16_t)(((uint32_t)u16Temp1*VCC_ADC_VOLT)/(uint16_t)Filtered_ref_mon_ad);
            u16Temp3 = (uint16_t)(((uint32_t)u16Temp2*(uint16_t)WL->pwm_duty_temp)/VCC_ADC_VOLT);
            WL->pwm_duty_temp_dash = (u16Temp3>PWM_duty_HOLD) ? PWM_duty_HOLD : (uint8_t)u16Temp3;
          #endif  */
          
        }
        else {
            WL->pwm_duty_temp_dash = WL->pwm_duty_temp;
        }
/*1024=(5V)*(1024/5) */
    }
    else {
        WL->pwm_duty_temp_dash = WL->pwm_duty_temp;
    }
}


void LAABS_vMonitorSolResistance(void)
{
    FL.LFC_NO_Hmon = Lfc_fl_no_Hmon;
    FR.LFC_NO_Hmon = Lfc_fr_no_Hmon;
    RL.LFC_NO_Hmon = Lfc_rl_no_Hmon;
    RR.LFC_NO_Hmon = Lfc_rr_no_Hmon;
    FL.LFC_NO_Lmon = Lfc_fl_no_Lmon;
    FR.LFC_NO_Lmon = Lfc_fr_no_Lmon;
    RL.LFC_NO_Lmon = Lfc_rl_no_Lmon;
    RR.LFC_NO_Lmon = Lfc_rr_no_Lmon;

  #if ((__VDC==ENABLE) && (__AHB_GEN3_SYSTEM == DISABLE) && (NO_VV_ONOFF_CTRL == DISABLE))
    LAABS_vMonitorSolResistanceESP();
  #endif
}

  #if ((__VDC==ENABLE) && (__AHB_GEN3_SYSTEM == DISABLE) && (NO_VV_ONOFF_CTRL == DISABLE))
void LAABS_vMonitorSolResistanceESP(void)
{
    if(Lfc_ptc_no_Hmon>=1023) {Lfc_ptc_no_Hmon = 1022;}
    if(Lfc_stc_no_Hmon>=1023) {Lfc_stc_no_Hmon = 1022;}
    if(Lfc_ps_nc_Hmon>=1023) {Lfc_ps_nc_Hmon = 1022;}
    if(Lfc_ss_nc_Hmon>=1023) {Lfc_ss_nc_Hmon = 1022;}

  #if __COIL_RESISTANCE_MONITOR
    if(Lfc_fl_no_Hmon>=1023) {Lfc_fl_no_Hmon = 1022;}

    R_ptc = (uint8_t)(((uint32_t)Lfc_ptc_no_Lmon * 403) / (1023 - Lfc_ptc_no_Hmon));  /* Rs : 40.2ohm  */
    R_stc = (uint8_t)(((uint32_t)Lfc_stc_no_Lmon * 403) / (1023 - Lfc_stc_no_Hmon));  /* (resel : 0.1ohm) */
    R_ps = (uint8_t)(((uint32_t)Lfc_ps_nc_Lmon * 403) / (1023 - Lfc_ps_nc_Hmon));
    R_ss = (uint8_t)(((uint32_t)Lfc_ss_nc_Lmon * 403) / (1023 - Lfc_ss_nc_Hmon));

    R_flno = (uint8_t)(((uint32_t)Lfc_fl_no_Lmon * 403) / (1023 - Lfc_fl_no_Hmon));
  #endif
}
  #endif /*(__VDC) && (__AHB_GEN3_SYSTEM == DISABLE)*/

#if (__MGH_80_10MS == ENABLE)
void LAABS_vInterfaceValveOutputs(const struct W_STRUCT*pActWL, WHEEL_VV_OUTPUT_t *laabsHP1, WHEEL_VV_OUTPUT_t *laabsHP2)
{
    /* ------------------- Full Rise -------------------*/
    if(pActWL->LFC_Conven_Reapply_Time>0)
    {
        laabsHP1->u8OutletOnTime = 0;
        laabsHP2->u8OutletOnTime = 0;

        laabsHP1->u8PwmDuty = pwm_duty_null;
        laabsHP2->u8PwmDuty = PWM_duty_HOLD;


        if(pActWL->LFC_Conven_Reapply_Time<=U8_BASE_CTRLTIME)
        {
            laabsHP1->u8InletOnTime = (uint8_t)pActWL->LFC_Conven_Reapply_Time;
            laabsHP2->u8InletOnTime = 0;
        }
        else
        {
            laabsHP1->u8InletOnTime = U8_BASE_CTRLTIME;
            laabsHP2->u8InletOnTime = (uint8_t)pActWL->LFC_Conven_Reapply_Time - U8_BASE_CTRLTIME;
        }
    }
    else
    {
        laabsHP1->u8InletOnTime = 0;
        laabsHP2->u8InletOnTime = 0;

        if((pActWL->LFC_built_reapply_flag==1) || (pActWL->pwm_duty_temp_dash<PWM_duty_HOLD)) /* For redundancy */        
        {
            laabsHP1->u8PwmDuty = laabsHP2->u8PwmDuty = pActWL->pwm_duty_temp_dash;

            laabsHP1->u8OutletOnTime = 0;
            laabsHP2->u8OutletOnTime = 0;
        }
        else
        {
	        /* ------------------- Dump -------------------*/
	
	        if(pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_01)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_HOLD;
	            laabsHP2->u8PwmDuty = PWM_duty_DUMP;
	
	            laabsHP1->u8OutletOnTime = 0;
	            laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	        }
	        else if (pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_10)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	            laabsHP2->u8PwmDuty = PWM_duty_HOLD;
	
	            if(pActWL->on_time_outlet_pwm<=U8_BASE_CTRLTIME)
	            {
	                laabsHP1->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	                laabsHP2->u8OutletOnTime = 0;
	            }
	            else
	            {
	                laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	                laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm - U8_BASE_CTRLTIME;
	            }
	        }
	        else if(pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_11)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	            laabsHP2->u8PwmDuty = PWM_duty_DUMP;
	            
	            laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	            laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	        }
	        else /* U8_L_DUMP_CASE_00 */
	        {
	            if(pActWL->on_time_outlet_pwm>0) /*Remaining dump scantime*/
	            {
	                if(pActWL->on_time_outlet_pwm<=U8_BASE_CTRLTIME)
	                {
	                    laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	                    laabsHP2->u8PwmDuty = PWM_duty_HOLD;
	
	                    laabsHP1->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	                    laabsHP2->u8OutletOnTime = 0;
	                }
	                else
	                {
	                    laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	                    laabsHP2->u8PwmDuty = PWM_duty_DUMP;
	
	                    laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	                    laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm - U8_BASE_CTRLTIME;
	                }
	            }
	            else
	            {
	                laabsHP1->u8PwmDuty = laabsHP2->u8PwmDuty = pActWL->pwm_duty_temp_dash;
	
	                laabsHP1->u8OutletOnTime = 0;
	                laabsHP2->u8OutletOnTime = 0;
	            }
	        }
		}
    }

  #if defined (__ACTUATION_DEBUG)
	laabsHP1->u8debugger = 4;
	laabsHP2->u8debugger = 4;
  #endif /* defined (__ACTUATION_DEBUG) */
}

#endif /* (__MGH_80_10MS == ENABLE)*/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAABSGenerateLFCDuty
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#endif/*#if (__IDB_LOGIC == DISABLE)*/
