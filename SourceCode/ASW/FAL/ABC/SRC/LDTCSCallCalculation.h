#ifndef __LCTCSCALLCALULATION_H__
#define __LCTCSCALLCALULATION_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition******************************************************/


/*Temperary Calibration Parameter Definition  *******************************************/
#define S16TCSCpDctTurnIndexBaseAyTh		LAT_0G1G
/*Definition of Developing Function ******************************************************/

/*Global MACRO FUNCTION Definition******************************************************/
#define __VEHICLE_TURN_INDEX_BY_MOMENT		ENABLE

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/

/*Global Extern Functions  Declaration******************************************************/
extern void LDTCS_vCallCalculation(void);
extern void LDTCS_vStartOfBrkCtlByAsymSlip(struct TCS_AXLE_STRUCT *AXLE_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT); 
extern void LDTCS_vStartOfBrkCtlBySymSlip(struct TCS_AXLE_STRUCT *AXLE_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT);
extern void LDTCS_vBrkCtlTrnsAsym2SymSlip(struct TCS_AXLE_STRUCT *AXLE_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT); 
extern void LDTCS_vBrkCtlTrnsSym2AsymSlip(struct TCS_AXLE_STRUCT *AXLE_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT); 
extern void LDTCS_vStartOfBrkCtlByStuck(void);
extern void LDTCS_vStartOfBrkCtlByVCA(void);
extern void LDTCS_vStartOfCordBrkCtl(void);
#endif