#ifndef __LCABSDECIDECTRLVARIABLES_H__
#define __LCABSDECIDECTRLVARIABLES_H__

/*includes********************************************************************/
#include "LVarHead.h"


/*Global MACRO CONSTANT Definition********************************************/
//temp
#define	U8_BIG_DUMP_BY_LOWMU_FASE_RISE	20
#define	U8_BIG_DUMP_GAUGE_AT_DUMP7		9
#define	U8_BIG_DUMP_GAUGE_AT_DUMP8		10
#define	U8_BIG_DUMP_GAUGE_AT_DUMP9		15
  
  #if __MGH60_ABS_IMPROVE_CONCEPT
#define __MEDMU_LOWSPEED_DECEL_IMPROVE  1
#define __DELAY_ABS_EXIT_FOR_HIGH_DUTY  1
  #else
#define __MEDMU_LOWSPEED_DECEL_IMPROVE  0
#define __DELAY_ABS_EXIT_FOR_HIGH_DUTY  0
  #endif

  #if (__ESC_PREMIUM_DV2_2) || (__ESC_PREMIUM_DV3)
#define S16_MINIMUM_EFFECTIVE_LFC_DUTY  70
  #else /*(__ESC_PREMIUM_DV2_2) || (__ESC_PREMIUM_DV3)*/
	#if (__CAR == BMW740i)
#define S16_MINIMUM_EFFECTIVE_LFC_DUTY  40
    #else /* (__CAR) */  
#define S16_MINIMUM_EFFECTIVE_LFC_DUTY  70
    #endif /* (__CAR) */
  #endif /*(__ESC_PREMIUM_DV2_2) || (__ESC_PREMIUM_DV3)*/

#define	S16_DUTY_DIFF_FOR_FORCED_HOLD	3 /* MGH-60 : 3, MGH-80 : 6 */

  #if (__CAR==HMC_EA_EV) /*|| (__CAR==KMC_TAM_EV)*/
#define __REDUCE_REAR_SLIP_AT_LMU       0
#define __REDUCE_REAR_SLIP_ADAPT_AT_LMU 0
#define __SELECT_LOW_FOR_LMU            1
#define S16_HOMO_MU_SL_ALLOW_SPEED      VREF_20_KPH
#define U8_HOMO_MU_SL_ALLOW_AFZ         U8_PULSE_MED_HIGH_MU

  #elif (__CAR==HMC_BK)
#define __REDUCE_REAR_SLIP_AT_LMU       1
#define __REDUCE_REAR_SLIP_ADAPT_AT_LMU 0
#define __SELECT_LOW_FOR_LMU            1
#define S16_HOMO_MU_SL_ALLOW_SPEED      VREF_80_KPH
#define U8_HOMO_MU_SL_ALLOW_AFZ         U8_PULSE_LOW_MU

  #elif (__CAR==KMC_KH)
#define __REDUCE_REAR_SLIP_AT_LMU       1
#define __REDUCE_REAR_SLIP_ADAPT_AT_LMU 0
#define __SELECT_LOW_FOR_LMU            1
#define S16_HOMO_MU_SL_ALLOW_SPEED      VREF_80_KPH
#define U8_HOMO_MU_SL_ALLOW_AFZ         U8_PULSE_LOW_MU

  #elif (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)
#define __REDUCE_REAR_SLIP_AT_LMU       0
#define __REDUCE_REAR_SLIP_ADAPT_AT_LMU 0
#define __SELECT_LOW_FOR_LMU            1
#define S16_HOMO_MU_SL_ALLOW_SPEED      VREF_20_KPH
#define U8_HOMO_MU_SL_ALLOW_AFZ         AFZ_1G0

  #else
#define __REDUCE_REAR_SLIP_AT_LMU       0
#define __REDUCE_REAR_SLIP_ADAPT_AT_LMU 0
#define __SELECT_LOW_FOR_LMU            0
#define S16_HOMO_MU_SL_ALLOW_SPEED      VREF_80_KPH
#define U8_HOMO_MU_SL_ALLOW_AFZ         U8_PULSE_LOW_MU
  #endif

#define	U8_L2Hsus_STB_TIM_S_DECEL_MIN	(U8_L2Hsus_STB_TIM_L_DECEL_MIN+L_TIME_10MS)

  #if (__CAR==GM_M300) || (__CAR==GM_T300) || (__CAR==GM_M350) || (__CAR==GM_GSUV)
#define __L2H_IMPROVE_SPLIT_DETECTION   DISABLE
#define __L2H_IMPROVE_VREF_TOGGLE       ENABLE
  #elif (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)
#define __L2H_IMPROVE_SPLIT_DETECTION   DISABLE
#define __L2H_IMPROVE_VREF_TOGGLE       ENABLE
  #elif (__CAR == BMW740i)
#define __L2H_IMPROVE_SPLIT_DETECTION   ENABLE
#define __L2H_IMPROVE_VREF_TOGGLE       ENABLE
  #else /* (__CAR == default ) */
#define __L2H_IMPROVE_SPLIT_DETECTION   ENABLE
#define __L2H_IMPROVE_VREF_TOGGLE       ENABLE
  #endif

#define __ONE_WHEEL_BUMP_DCT			DISABLE
#define	__L2H_IMPROVE_BY_WHEEL_PRESS	ENABLE

  #if (__VDC==ENABLE)&&(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
#define U8_L2H_DCT_WPRESS_THRES1	MPRESS_10BAR
#define U8_L2H_DCT_WPRESS_THRES2	MPRESS_12BAR
#define U8_L2H_DCT_WPRESS_DIFF		MPRESS_10BAR
  #endif

#define U8_BRD_DUMP_CNT_LEVEL1      9
#define U8_BRD_DUMP_CNT_LEVEL2      11
#define U8_BRD_DUMP_CNT_LEVEL3      12
#define U8_BRD_DETECTION_REF        30

#define S8_ARAD_GAUGE_LV5	ARAD_20G0
#define S8_ARAD_GAUGE_LV4	ARAD_15G0
#define S8_ARAD_GAUGE_LV3	ARAD_12G0
#define S8_ARAD_GAUGE_LV2	ARAD_8G0
#define S8_ARAD_GAUGE_LV1	ARAD_7G0

#define S8_BMP_SUS_ARAD_GAUGE_THRES_F1	0
#define S8_BMP_SUS_ARAD_GAUGE_THRES_F2	-5
#define S8_BMP_SUS_ARAD_GAUGE_THRES_R1	-5
#define S8_BMP_SUS_ARAD_GAUGE_THRES_R2 	10

#define S8_BMP_SUS_ARAD_THRES_F1	ARAD_10G0
#define S8_BMP_SUS_ARAD_THRES_F2	ARAD_7G0
#define S8_BMP_SUS_ARAD_THRES_R1	ARAD_8G0
#define S8_BMP_SUS_ARAD_THRES_R2	ARAD_6G0

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

 typedef struct
{
	unsigned ReleaseBwSkid				:1;
	unsigned ReleaseBwSkidScan			:1;
	unsigned BrkPedalRel          		:1;
	unsigned BrkPedalApp          		:1;
	unsigned BrkPedalReapplyInABS	 	:1;
	unsigned BrkPedalRelDuringABS    	:1;

}BRAKE_PEDAL_DETECTION_t;

extern BRAKE_PEDAL_DETECTION_t BrkPedalModulationInfo;

/*Global Type Declaration ****************************************************/

#define LOW_to_HIGH_suspect2_K      	ABSCTR01.bit7
#define LOW_to_HIGH_suspect2_old    	ABSCTR01.bit6
#define LOW_to_HIGH_suspect2_flg    	ABSCTR01.bit5
#define lcabsu1OneWlABSUneven       	ABSCTR01.bit4
#define lcabsu1TwoWlABSUneven       	ABSCTR01.bit3
#define lcabsu1NoABSinRest3WLs      	ABSCTR01.bit2
#define lcabsu1NoABSinRest2WLs      	ABSCTR01.bit1
  #if (__ONE_WHEEL_BUMP_DCT==ENABLE)
#define lcabsu1ABSForBumpByOneWhl      	ABSCTR01.bit0
  #else
#define Not_used_ABS_flag_10        	ABSCTR01.bit0
  #endif  

#if __VDC
#define MP_RELEASE_BW_4BAR      		ABSCTR02.bit7
#define MP_RELEASE_BW_6BAR      		ABSCTR02.bit6
#define MP_RELEASE_BW_10BAR     		ABSCTR02.bit5
#define MP_RELEASE_BW_4BAR_SCAN     	ABSCTR02.bit4
#define MP_RELEASE_BW_6BAR_SCAN     	ABSCTR02.bit3
#define MP_RELEASE_BW_10BAR_SCAN    	ABSCTR02.bit2
#define Not_used_ABS_flag_21        	ABSCTR02.bit1
#define Not_used_ABS_flag_20        	ABSCTR02.bit0
#endif

#define lcabsu1BrakeReleasePossible     ABSCTR03.bit7
#define lcabsu1EngageDriveline          ABSCTR03.bit6
#define lcabsu1SuspctAccelIntention     ABSCTR03.bit5
#define lcabsu1Rear1stCycleRiseDelay    ABSCTR03.bit4
#define PossibleABSForBumpVehicle   	ABSCTR03.bit3
#define lcabsu1SelectLowforNonSplit		ABSCTR03.bit2
  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
#define lcabsu1CertainHMu           	ABSCTR03.bit1
#define lcabsu1CertainHMuPre        	ABSCTR03.bit0
  #else
#define Not_used_ABS_flag_31        	ABSCTR03.bit1
#define Not_used_ABS_flag_30        	ABSCTR03.bit0
  #endif

  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
#define lcabsu1L2hSuspectByWhPres		ABSCTR04.bit7
#define lcabsu1L2hSuspectByWhPres_K		ABSCTR04.bit6
  #else
#define Not_used_ABS_flag_47			ABSCTR04.bit7
#define Not_used_ABS_flag_46			ABSCTR04.bit6
  #endif
#define Not_used_ABS_flag_45			ABSCTR04.bit5
#define Not_used_ABS_flag_44			ABSCTR04.bit4
#define Not_used_ABS_flag_43			ABSCTR04.bit3
#define Not_used_ABS_flag_42			ABSCTR04.bit2
#define Not_used_ABS_flag_41			ABSCTR04.bit1
#define Not_used_ABS_flag_40			ABSCTR04.bit0

#define lcabsu1DriverPedalRel		BrkPedalModulationInfo.BrkPedalRel
#define lcabsu1DriverPedalApp		BrkPedalModulationInfo.BrkPedalApp
#define lcabsu1DriverPedalReapply	BrkPedalModulationInfo.BrkPedalReapplyInABS
#define lcabsu1BrkPedalRelDuringABS	BrkPedalModulationInfo.BrkPedalRelDuringABS

#define MP_RELEASE_BW_SKID			BrkPedalModulationInfo.ReleaseBwSkid
#define MP_RELEASE_BW_SKID_SCAN		BrkPedalModulationInfo.ReleaseBwSkidScan

/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t ABSCTR01,ABSCTR02,ABSCTR03,ABSCTR04;

#if __TEMP_for_CAS  
extern uint8_t   NUM_OF_STABIL_WHEELS;
#endif
extern uint8_t  L2H_sus_cnt_by_aref;
extern uint8_t	lsabsu8L2HMode;
extern uint8_t  GMA_dumptime_at_H_to_Split;
#if __LOWMU_REAR_1stCYCLE_RISE_DELAY
extern uint8_t lcabsu8LowMuTendencyGauge;
#endif

/*Global Extern Functions  Declaration****************************************/
extern void LCABS_vDecideWheelCtrlVariable(void);
extern void LCABS_vDecideRoadWhVehStatus(void);
extern void LCABS_vDecideVehicleCtrlState(void);


#endif
