/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LCESPCallPressureControl.c
* Description:      Pressure Control code of ESP
* Logic version:    F026-04
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  6720         eslim           Initial Release
*  6919         eslim           PD gain add
*  7228         eslim           modify rise mode and slip interface
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCallPressureControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/


#include "LCESPCallPressureControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"
#include "LCESPCallControl.h"

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCallPressureControl
	#include "Mdyn_autosar.h"
#endif    
