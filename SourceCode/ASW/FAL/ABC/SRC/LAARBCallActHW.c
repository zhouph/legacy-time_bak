/******************************************************************************
* Project Name: Anti-Rollback for HEV
* File: LAARBCallActHW.C
* Description: Valve/Motor actuaction by Anti-Rollback Controller
* Date: Sep. 20. 2007
******************************************************************************/

/* Includes ******************************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAARBCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LAARBCallActHW.h"
#include "LCARBCallControl.h"
#include "a_struct.h"
#include "algo_var.h"
#include "Hardware_Control.h"

#if __HEV
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/


/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAARB_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Anti-Rollback Controller
******************************************************************************/
void	LAARB_vCallActHW(void)
{
	if(lcarbu1ActiveFlag==1) 
	{
	  #if (__SPLIT_TYPE==0)	
		TCL_DEMAND_fl = lcarbu1ActSecondaryTc;
		TCL_DEMAND_fr = lcarbu1ActPrimaryTc;
	  #else
		TCL_DEMAND_SECONDARY = lcarbu1ActSecondaryTc;
		TCL_DEMAND_PRIMARY = lcarbu1ActPrimaryTc;
	  #endif	
		
		VALVE_ACT = 1;
	}
  #if !(__BTC)	
	else
	{
	  #if (__SPLIT_TYPE==0)	
		TCL_DEMAND_fl = 0;
		TCL_DEMAND_fr = 0;
	  #else
		TCL_DEMAND_SECONDARY = 0;
		TCL_DEMAND_PRIMARY = 0;
	  #endif	
	}
  #endif	
} 

void	LAARB_vCallActHWOffEBD(void)
{
	if(lcarbu1ActiveFlag==1) 
	{	
		if(lcarbu1ActSecondaryTc==1)
		{
			MFC_PWM_DUTY_S = 170;
			VALVE_ACT = 1;
		}
		else
		{
			MFC_PWM_DUTY_S = 0;
		}
		if(lcarbu1ActPrimaryTc==1)
		{
			MFC_PWM_DUTY_P = 170;
			VALVE_ACT = 1;
		}
		else
		{
			MFC_PWM_DUTY_P = 0;
		}

		if((lcarbu1ActSecondaryTc==0)&&(lcarbu1ActPrimaryTc==0)) VALVE_ACT = 0;
	}
	else
	{
		MFC_PWM_DUTY_S = 0;
		MFC_PWM_DUTY_P = 0;		
		VALVE_ACT = 0;
	}
	#if ARB_MFC_DUTY_CONTROL
	LCARB_vSetDutyPattern();
	#endif
}
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAARBCallActHW
	#include "Mdyn_autosar.h"               
#endif
