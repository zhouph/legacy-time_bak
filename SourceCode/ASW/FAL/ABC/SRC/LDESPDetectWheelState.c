
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPDetectWheelState
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LDRTACallDetection.h"

#if __VDC
void  LDESP_vIdentifyMiniWheel(void);


  #if	__TCS
void 	MINI_TCS(void);                   
  #endif

#if	__VDC
   #if 	__SPARE_WHEEL
void 	DETECT_MINI_SPARE(void);
   #endif
#endif
	
void LDESP_vIdentifyMiniWheel(void)
{

/***********************************************************************/
/* MINI SPARE DETECT                                                   */
/***********************************************************************/


  #if	__TCS
	MINI_TCS();
  #endif
  #if	__VDC && __SPARE_WHEEL
    DETECT_MINI_SPARE();
  #endif

/***********************************************************************/
/* Wheel Pressure Estimation                                           */
/***********************************************************************/
/*  #if	__VDC && __PRESS_EST */
/*	Wheel_Press_Estimation(); */
/*  #endif	*/
}


#if __SPARE_WHEEL
void DETECT_MINI_SPARE(void)
{
    ESP_MINI_TIRE_SUSPECT = MINI_SPARE_WHEEL_rl | MINI_SPARE_WHEEL_rr;

    if( (ESP_MINI_TIRE==0) 
        && ( (vrefk >= 150) && (McrAbs(yaw_out) < 500 ) && (abs_wstr < 500 ) && ( abs_wstr_dot < 500 ) 
        && (ESP_BRAKE_CONTROL==0) && (ESP_TCS_ON==0) && (ABS_ON==0) && (BTCS_ON==0) && (ETCS_ON==0)) ) 
    {
	        if ( (ESP_MINI_TIRE_SUSPECT==1) && (esp_mini_tire_valid_count < L_U8_TIME_10MSLOOP_750MS) )
	        { 
	            esp_mini_tire_valid_count=esp_mini_tire_valid_count+1;            
	        }
	        else if ( (ESP_MINI_TIRE_SUSPECT==0) && (esp_mini_tire_valid_count> 0) ) 
	        {
	            esp_mini_tire_valid_count=esp_mini_tire_valid_count-1;
        	}
        	else
        	{
        		;
        	}
    } 
    else
    {
    	;
    }      
    
    if (vrefk < 50)  
    {
        ESP_MINI_TIRE = 0;
        esp_mini_tire_valid_count = 0;
    } 
    else if ( esp_mini_tire_valid_count > L_U8_TIME_10MSLOOP_500MS ) 
    {
        ESP_MINI_TIRE = 1;
    } 
    else if ( esp_mini_tire_valid_count < L_U8_TIME_10MSLOOP_200MS ) 
    {
        ESP_MINI_TIRE = 0;
    }       
    else
    {
    	;
    }
}   
#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPDetectWheelState
	#include "Mdyn_autosar.h"
#endif
