#ifndef __LDABSDCTVEHICLESTATUS_H__
#define __LDABSDCTVEHICLESTATUS_H__

/*includes********************************************************************/
#include "LVarHead.h"

#if __L_CYCLETIME == LOOP_10MS
    #define U8_BEND_DCT_TIME_PER_NDRIVEN_WL L_TIME_30MS
    #define U8_BEND_DCT_TIME_PER_SIDEVREF   L_TIME_60MS
    #define U8_BEND_DCT_TIME_PER_SIDEVREF2  L_TIME_200MS    
    #define U8_BEND_RESET_TIME_PER_SIDEVREF L_TIME_40MS
#else
    #define U8_BEND_DCT_TIME_PER_NDRIVEN_WL L_TIME_35MS
    #define U8_BEND_DCT_TIME_PER_SIDEVREF   L_TIME_70MS
    #define U8_BEND_DCT_TIME_PER_SIDEVREF2  L_TIME_210MS
    #define U8_BEND_RESET_TIME_PER_SIDEVREF L_TIME_35MS
#endif

/*Global MACRO CONSTANT Definition********************************************/



/*Global Extern Variable  Declaration*****************************************/

extern int8_t  bend_slip_comp;
#if __BEND_DETECT_using_SIDEVREF
extern int16_t Side_vref_diff;
#endif

/*Global Extern Functions  Declaration****************************************/
extern void LDABS_vDctVehicleStatus(void);


#endif
