/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LADECCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LADECCallActHW.h"
#include "LCallMain.h"
  #if __DEC
#include "LCDECCallControl.h"
#include "LCHSACallControl.h"
#include "LAHDCCallActHW.h"
#include "LCHDCCallControl.h"
#include "Hardware_Control.h"
#include "LCEPBCallControl.h"
#include "LCACCCallControl.h"
   #if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
   #endif
  #endif
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __DEC
uint8_t		lcu8DecValveMode, lcu8DecMotorMode;
DEC_VALVE_t	laDecValveP, laDecValveS;

uint8_t   lcu8DecFlNoValve, lcu8DecFlNcValve, lcu8DecFrNoValve, lcu8DecFrNcValve;
uint8_t   lcu8DecRlNoValve, lcu8DecRlNcValve, lcu8DecRrNoValve, lcu8DecRrNcValve;

uint8_t	lcu8DecPriESVCloseTime;
uint8_t	lcu8DecSecESVCloseTime;

uint8_t	lcu8DecFLWheelDumpcnt;
uint8_t   lcu8DecFRWheelDumpcnt;
uint8_t   lcu8DecRLWheelDumpcnt;
uint8_t	lcu8DecRRWheelDumpcnt;

   #if __SPLIT_TYPE
uint8_t   lcu8DecFlFrEsvValve, lcu8DecRlRrEsvValve;
uint8_t   lcu8DecFlFrTcValve, lcu8DecRlRrTcValve;
   #else
uint8_t   lcu8DecFlRrEsvValve, lcu8DecFrRlEsvValve;
uint8_t   lcu8DecFlRrTcValve, lcu8DecFrRlTcValve;
   #endif
/*uint8_t   lcu8DecValveMode, lcu8DecMotorMode;*/
int16_t     lcs16DecMscTargetVol;
  #endif

/* Local Function prototype ****************************************/
  #if __DEC
static void LADEC_vCoopEsp(void);
static void LADEC_vCoopAbs(void);
static void LADEC_vCoopEbd(void);
static void LADEC_vNormDec(void);
static void LADEC_vInheritValveCommand(void);
static void LADEC_vWheelValveDumpCheck(void);
static void LADEC_vOriginalValveCmd(void);
static void LADEC_vSetMscVoltage(void);

#if	__Decel_Ctrl_Integ
int16_t LAMFC_s16SetMFCCurrentDEC(uint8_t CIRCUIT, DEC_VALVE_t *pDec, int16_t MFC_Current_old);
#endif

  #endif

/* Implementation***************************************************/
  #if __DEC
void LADEC_vCallActHW(void)
{
    LADEC_vInheritValveCommand();

	LADEC_vWheelValveDumpCheck();

    if(lcu1DecActiveFlg==1)
    {
        if(FLAG_ESP_CONTROL_ACTION==1)
        {
            LADEC_vCoopEsp();
        }
        else if(ABS_fz==1)
        {
            LADEC_vCoopAbs();
        }
        else if(EBD_RA==1)
        {
            LADEC_vCoopEbd();
        }
        else
        {
            LADEC_vNormDec();
        }
          #if __SPLIT_TYPE
        lcu8DecFlFrTcValve=1;
        lcu8DecRlRrTcValve=1;
          #else
        lcu8DecFlRrTcValve=1;
        lcu8DecFrRlTcValve=1;
          #endif
    }
    else
    {
        ;
    }

	if(lcu1DecActiveFlg==1)
	{
	  #if !__TCMF_CONTROL
		HV_VL_fl=lcu8DecFlNoValve;
		AV_VL_fl=lcu8DecFlNcValve;
		HV_VL_fr=lcu8DecFrNoValve;
		AV_VL_fr=lcu8DecFrNcValve;
		HV_VL_rl=lcu8DecRlNoValve;
		AV_VL_rl=lcu8DecRlNcValve;
		HV_VL_rr=lcu8DecRrNoValve;
		AV_VL_rr=lcu8DecRrNcValve;
	  #endif
		  #if __SPLIT_TYPE		/* FR Split */
		S_VALVE_PRIMARY=lcu8DecFlFrEsvValve;
		S_VALVE_SECONDARY=lcu8DecRlRrEsvValve;
		TCL_DEMAND_PRIMARY=lcu8DecFlFrTcValve;
		TCL_DEMAND_SECONDARY=lcu8DecRlRrTcValve;
		  #else				/* X Split */
		S_VALVE_RIGHT=lcu8DecFrRlEsvValve;
		S_VALVE_LEFT=lcu8DecFlRrEsvValve;
		TCL_DEMAND_fr=lcu8DecFrRlTcValve;
		TCL_DEMAND_fl=lcu8DecFlRrTcValve;
		  #endif
	}
	else
	{
		;
	}
	  #if __TCMF_CONTROL
	LADEC_vOriginalValveCmd();
	  #endif


    LADEC_vSetMscVoltage();
}


static void LADEC_vCoopEsp(void)
{
/*08SWD-10*/
#if __Decel_Ctrl_Integ && __TCMF_CONTROL
	/*lcu8DecMotorMode=VDC_MOTOR_ON;*/
#else
    lcu8DecMotorMode=VDC_MOTOR_ON;
#endif

    if((YAW_CDC_WORK==1)&&(ABS_fz==0))
    {
          #if __SPLIT_TYPE
        if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1))
        {
            if((EBD_RA==0)&&(L_SLIP_CONTROL_rl==0))
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecRlNoValve=1;
                    lcu8DecRlNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecRlNoValve=0;
                    lcu8DecRlNcValve=0;
                }
                else
                {
                    lcu8DecRlNoValve=1;
                    lcu8DecRlNcValve=1;
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rr==0))
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecRrNoValve=1;
                    lcu8DecRrNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecRrNoValve=0;
                    lcu8DecRrNcValve=0;
                }
                else
                {
                    lcu8DecRrNoValve=1;
                    lcu8DecRrNcValve=1;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
          #else
        if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_RR==1))
        {
            if(L_SLIP_CONTROL_fr==0)
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecFrNoValve=1;
                    lcu8DecFrNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecFrNoValve=0;
                    lcu8DecFrNcValve=0;
                }
                else
                {
                    lcu8DecFrNoValve=1;
                    lcu8DecFrNcValve=1;
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rl==0))
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecRlNoValve=1;
                    lcu8DecRlNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecRlNoValve=0;
                    lcu8DecRlNcValve=0;
                }
                else
                {
                    lcu8DecRlNoValve=1;
                    lcu8DecRlNcValve=1;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #endif
            /*******************************************************************/
              #if __SPLIT_TYPE
        if((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))
        {
            if(L_SLIP_CONTROL_fl==0)
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecFlNoValve=1;
                    lcu8DecFlNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecFlNoValve=0;
                    lcu8DecFlNcValve=0;
                }
                else
                {
                    lcu8DecFlNoValve=1;
                    lcu8DecFlNcValve=1;
                }
            }
            else
            {
                ;
            }

            if(L_SLIP_CONTROL_fr==0)
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecFrNoValve=1;
                    lcu8DecFrNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecFrNoValve=0;
                    lcu8DecFrNcValve=0;
                }
                else
                {
                    lcu8DecFrNoValve=1;
                    lcu8DecFrNcValve=1;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #else
        if((ESP_BRAKE_CONTROL_FR==1)||(ESP_BRAKE_CONTROL_RL==1))
        {
            if(L_SLIP_CONTROL_fl==0)
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecFlNoValve=1;
                    lcu8DecFlNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecFlNoValve=0;
                    lcu8DecFlNcValve=0;
                }
                else
                {
                    lcu8DecFlNoValve=1;
                    lcu8DecFlNcValve=1;
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rr==0))
            {
                if(lcu8DecValveMode==U8_DEC_HOLD)
                {
                    lcu8DecRrNoValve=1;
                    lcu8DecRrNcValve=0;
                }
                else if(lcu8DecValveMode==U8_DEC_APPLY)
                {
                    lcu8DecRrNoValve=0;
                    lcu8DecRrNcValve=0;
                }
                else
                {
                    lcu8DecRrNoValve=1;
                    lcu8DecRrNcValve=1;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #endif
    }
    else if ((YAW_CDC_WORK==1)&&(ABS_fz==1))
    {
		;
	}
    else    /*if((YAW_CDC_WORK==0)||(ABS_fz==1))*/
    {
        ;
    }

  #if __SPLIT_TYPE
    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_FR==0))
    {
        lcu8DecFlFrEsvValve=1;
    }
    else { }
    if((ESP_BRAKE_CONTROL_RL==0)&&(ESP_BRAKE_CONTROL_RR==0))
    {
        lcu8DecRlRrEsvValve=1;
    }
    else { }
  #else
    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_RR==0))
    {
        lcu8DecFlRrEsvValve=1;
    }
    else { }
    if((ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_RL==0))
    {
        lcu8DecFrRlEsvValve=1;
    }
    else { }
  #endif

}

static void LADEC_vCoopAbs(void)
{
    lcu8DecMotorMode=VDC_MOTOR_ON;
      #if __SPLIT_TYPE
    if((HV_VL_fl==0)&&(AV_VL_fl==0))
    {
        lcu8DecMotorMode=1;
    }
    else if((HV_VL_fr==0)&&(HV_VL_fr==0))
    {
        lcu8DecMotorMode=1;
    }
    else { }
      #else
    if((HV_VL_fl==0)&&(AV_VL_fl==0))
    {
        lcu8DecMotorMode=1;
    }
    else if((HV_VL_rr==0)&&(AV_VL_rr==0))
    {
        lcu8DecMotorMode=1;
    }
    else { }
      #endif

      #if __SPLIT_TYPE
    if((HV_VL_rl==0)&&(AV_VL_rl==0))
    {
    	lcu8DecMotorMode=1;
    }
    else if((HV_VL_rr==0)&&(AV_VL_rr==0))
    {
    	lcu8DecMotorMode=1;
    }
    else { }
      #else
    if((HV_VL_fr==0)&&(AV_VL_fr==0))
    {
    	lcu8DecMotorMode=1;
    }
    else if((HV_VL_rl==0)&&(AV_VL_rl==0))
    {
    	lcu8DecMotorMode=1;
    }
    else { }
      #endif
/*  Issue List 2007-180 : ABS+HDC ���� ���� �� �з»�� �ʾ� ���� ����  */
  #if __SPLIT_TYPE
    lcu8DecFlFrEsvValve=1;
    lcu8DecRlRrEsvValve=1;
  #else
    lcu8DecFlRrEsvValve=1;
    lcu8DecFrRlEsvValve=1;
  #endif

  	if (MPRESS_BRAKE_ON_on_cnt > L_U8_TIME_10MSLOOP_300MS)
	{
		if (lcu8DecPriESVCloseTime > 0)
		{
			if (lcu8DecPriESVCloseTime > 0)
			{
				lcu8DecPriESVCloseTime--;
			}
			else { }

		  #if __SPLIT_TYPE
		    lcu8DecFlFrEsvValve=0;
		  #else
		    lcu8DecFrRlEsvValve=0;
		  #endif
		}
		else { }

		if (lcu8DecSecESVCloseTime > 0)
		{

			if (lcu8DecSecESVCloseTime > 0)
			{
				lcu8DecSecESVCloseTime--;
			}
			else { }

		  #if __SPLIT_TYPE
		    lcu8DecRlRrEsvValve=0;
		  #else
		    lcu8DecFlRrEsvValve=0;
		  #endif
		}
		else { }
	}
	else { }

}

static void LADEC_vCoopEbd(void)
{
    if(lcu8DecValveMode==U8_DEC_HOLD)
    {
        lcu8DecFlNoValve=1;
        lcu8DecFlNcValve=0;
        lcu8DecFrNoValve=1;
        lcu8DecFrNcValve=0;
    }
    else if(lcu8DecValveMode==U8_DEC_APPLY)
    {
        lcu8DecFlNoValve=0;
        lcu8DecFlNcValve=0;
        lcu8DecFrNoValve=0;
        lcu8DecFrNcValve=0;
    }
    else if(lcu8DecValveMode==U8_DEC_DUMP)
    {
        lcu8DecFlNoValve=1;
        lcu8DecFlNcValve=1;
        lcu8DecFrNoValve=1;
        lcu8DecFrNcValve=1;
    }
    else { }

      #if __SPLIT_TYPE
    lcu8DecFlFrEsvValve=1;
    lcu8DecRlRrEsvValve=1;
      #else
    lcu8DecFlRrEsvValve=1;
    lcu8DecFrRlEsvValve=1;
      #endif
}

static void LADEC_vNormDec(void)
{
    if(lcu8DecValveMode==U8_DEC_HOLD)
    {
        lcu8DecFlNoValve=1;
        lcu8DecFlNcValve=0;
        lcu8DecFrNoValve=1;
        lcu8DecFrNcValve=0;
        lcu8DecRlNoValve=1;
        lcu8DecRlNcValve=0;
        lcu8DecRrNoValve=1;
        lcu8DecRrNcValve=0;
    }
    else if(lcu8DecValveMode==U8_DEC_APPLY)
    {
        lcu8DecFlNoValve=0;
        lcu8DecFlNcValve=0;
        lcu8DecFrNoValve=0;
        lcu8DecFrNcValve=0;
        lcu8DecRlNoValve=0;
        lcu8DecRlNcValve=0;
        lcu8DecRrNoValve=0;
        lcu8DecRrNcValve=0;
    }
    else if(lcu8DecValveMode==U8_DEC_DUMP)
    {
        lcu8DecFlNoValve=1;
        lcu8DecFlNcValve=1;
        lcu8DecFrNoValve=1;
        lcu8DecFrNcValve=1;
        lcu8DecRlNoValve=1;
        lcu8DecRlNcValve=1;
        lcu8DecRrNoValve=1;
        lcu8DecRrNcValve=1;
    }
    else { }

      #if __SPLIT_TYPE
    lcu8DecFlFrEsvValve=1;
    lcu8DecRlRrEsvValve=1;
      #else
    lcu8DecFlRrEsvValve=1;
    lcu8DecFrRlEsvValve=1;
      #endif
}


static void LADEC_vInheritValveCommand(void)
{
    lcu8DecFlNoValve=HV_VL_fl;
    lcu8DecFlNcValve=AV_VL_fl;
    lcu8DecFrNoValve=HV_VL_fr;
    lcu8DecFrNcValve=AV_VL_fr;
    lcu8DecRlNoValve=HV_VL_rl;
    lcu8DecRlNcValve=AV_VL_rl;
    lcu8DecRrNoValve=HV_VL_rr;
    lcu8DecRrNcValve=AV_VL_rr;
      #if __SPLIT_TYPE      /* FR Split */
    lcu8DecFlFrEsvValve=S_VALVE_PRIMARY;
    lcu8DecRlRrEsvValve=S_VALVE_SECONDARY;
    lcu8DecFlFrTcValve=TCL_DEMAND_PRIMARY;
    lcu8DecRlRrTcValve=TCL_DEMAND_SECONDARY;
      #else             /* X Split */
    lcu8DecFrRlEsvValve=S_VALVE_RIGHT;
    lcu8DecFlRrEsvValve=S_VALVE_LEFT;
    lcu8DecFrRlTcValve=TCL_DEMAND_fr;
    lcu8DecFlRrTcValve=TCL_DEMAND_fl;
      #endif
}

  #if __TCMF_CONTROL
static void LADEC_vOriginalValveCmd(void)
{
	if(lcu8DecValveMode==U8_DEC_HOLD)
	{
		DEC_HV_VL_fl=1;
		DEC_AV_VL_fl=0;
		DEC_HV_VL_fr=1;
		DEC_AV_VL_fr=0;
		DEC_HV_VL_rl=1;
		DEC_AV_VL_rl=0;
		DEC_HV_VL_rr=1;
		DEC_AV_VL_rr=0;
	}
	else if(lcu8DecValveMode==U8_DEC_APPLY)
	{
		DEC_HV_VL_fl=0;
		DEC_AV_VL_fl=0;
		DEC_HV_VL_fr=0;
		DEC_AV_VL_fr=0;
		DEC_HV_VL_rl=0;
		DEC_AV_VL_rl=0;
		DEC_HV_VL_rr=0;
		DEC_AV_VL_rr=0;
	}
	else if(lcu8DecValveMode==U8_DEC_DUMP)
	{
		DEC_HV_VL_fl=1;
		DEC_AV_VL_fl=1;
		DEC_HV_VL_fr=1;
		DEC_AV_VL_fr=1;
		DEC_HV_VL_rl=1;
		DEC_AV_VL_rl=1;
		DEC_HV_VL_rr=1;
		DEC_AV_VL_rr=1;
	}
	else
	{
		;
	}
}
  #endif

static void LADEC_vWheelValveDumpCheck(void)
{

	if (MPRESS_BRAKE_ON_on_cnt > L_U8_TIME_10MSLOOP_300MS)
	{
    	if ((HV_VL_fl==1)&&(AV_VL_fl==1))
    	{
    	    if(lcu8DecFLWheelDumpcnt < L_U8_TIME_10MSLOOP_1750MS) {lcu8DecFLWheelDumpcnt++;}
    	}
    	else { }

    	if ((HV_VL_fr==1)&&(AV_VL_fr==1))
    	{
    	    if(lcu8DecFRWheelDumpcnt < L_U8_TIME_10MSLOOP_1750MS) {lcu8DecFRWheelDumpcnt++;}
    	}
    	else { }

    	if ((HV_VL_rl==1)&&(AV_VL_rl==1))
    	{
    	    if(lcu8DecRLWheelDumpcnt < L_U8_TIME_10MSLOOP_1750MS) {lcu8DecRLWheelDumpcnt++;}
    	}
    	else { }

    	if ((HV_VL_rr==1)&&(AV_VL_rr==1))
    	{
    	    if(lcu8DecRRWheelDumpcnt < L_U8_TIME_10MSLOOP_1750MS) {lcu8DecRRWheelDumpcnt++;}
    	}
    	else { }
    }
    else
    {
		if (lcu8DecFLWheelDumpcnt>0)
		{
			lcu8DecFLWheelDumpcnt = lcu8DecFLWheelDumpcnt-1;
		}
		else { }

		if (lcu8DecFRWheelDumpcnt>0)
		{
			lcu8DecFRWheelDumpcnt = lcu8DecFRWheelDumpcnt-1;
		}
		else { }

		if (lcu8DecRLWheelDumpcnt>0)
		{
			lcu8DecRLWheelDumpcnt = lcu8DecRLWheelDumpcnt-1;
		}
		else { }

		if (lcu8DecRRWheelDumpcnt>0)
		{
			lcu8DecRRWheelDumpcnt = lcu8DecRRWheelDumpcnt-1;
		}
		else { }
    }


  #if     __SPLIT_TYPE == 1         /* 1 : F/R-split */
    if ((((int16_t)lcu8DecFLWheelDumpcnt*2) + ((int16_t)lcu8DecFRWheelDumpcnt*2)) >= S16_DEC_ABSDump_ESV_Close_Th1)
    {
    	lcu1DecPriESVCloseflg = 1;
        lcu8DecFLWheelDumpcnt=0;
        lcu8DecFRWheelDumpcnt=0;
	}
	else
	{
		lcu1DecPriESVCloseflg = 0;
	}

   	if (((int16_t)lcu8DecRLWheelDumpcnt + (int16_t)lcu8DecRRWheelDumpcnt) >= S16_DEC_ABSDump_ESV_Close_Th1)
    {
    	lcu1DecSecESVCloseflg = 1;
        lcu8DecRLWheelDumpcnt=0;
        lcu8DecRRWheelDumpcnt=0;
    }
    else
    {
    	lcu1DecSecESVCloseflg = 0;
    }
  #else                             /* 0 : X-split */
    if ((((int16_t)lcu8DecFRWheelDumpcnt*2) + (int16_t)lcu8DecRLWheelDumpcnt) >= S16_DEC_ABSDump_ESV_Close_Th1)
    {
    	lcu1DecPriESVCloseflg = 1;
        lcu8DecFRWheelDumpcnt=0;
        lcu8DecRLWheelDumpcnt=0;
	}
	else
	{
		lcu1DecPriESVCloseflg = 0;
	}


   	if ((((int16_t)lcu8DecFLWheelDumpcnt*2) + (int16_t)lcu8DecRRWheelDumpcnt) >= S16_DEC_ABSDump_ESV_Close_Th1)
    {
    	lcu1DecSecESVCloseflg = 1;
        lcu8DecFLWheelDumpcnt=0;
        lcu8DecRRWheelDumpcnt=0;
    }
    else
    {
    	lcu1DecSecESVCloseflg = 0;
    }
  #endif

  	if ((ABS_fz==1)&&(MPRESS_BRAKE_ON_on_cnt > L_U8_TIME_10MSLOOP_300MS))
  	{
  		if ((lcu1DecPriESVCloseflg == 1)||(MSC_primary_indicator > S16_DEC_ABSDump_ESV_Close_Th2))
  	    {
  	    	lcu8DecPriESVCloseTime=(uint8_t)L_U8_TIME_10MSLOOP_500MS;
  	    }
  	    else { }
  		if ((lcu1DecSecESVCloseflg == 1)||(MSC_secondary_indicator > S16_DEC_ABSDump_ESV_Close_Th2))
  	    {
  	    	lcu8DecSecESVCloseTime=(uint8_t)L_U8_TIME_10MSLOOP_500MS;
  	    }
  	    else { }
  	}
  	else
  	{
    	lcu1DecPriESVCloseflg = 0;
		lcu1DecSecESVCloseflg = 0;
		lcu8DecPriESVCloseTime = 0;
		lcu8DecSecESVCloseTime = 0;

        lcu8DecFLWheelDumpcnt=0;
        lcu8DecFRWheelDumpcnt=0;
        lcu8DecRLWheelDumpcnt=0;
        lcu8DecRRWheelDumpcnt=0;
  	}

}


static void LADEC_vSetMscVoltage(void)
{
    if (lcu1DecActiveFlg==1)
    {
        if(lcs16DecCtrlError<0)
        {
            if(lcs16DecCtrlError<S16_DEC_MSC_1ST_ERR) {tempW1=S16_DEC_MSC_1ST_ERR;}
            else if(lcs16DecCtrlError>S16_DEC_MSC_2ND_ERR) {tempW1=S16_DEC_MSC_2ND_ERR;}
            else {tempW1=lcs16DecCtrlError;}
            tempW4=(int16_t)((((int32_t)(S16_DEC_MSC_2ND_VOL-S16_DEC_MSC_1ST_VOL))*tempW1)/(S16_DEC_MSC_2ND_ERR-S16_DEC_MSC_1ST_ERR));
			tempW3=(int16_t)((((int32_t)(S16_DEC_MSC_2ND_VOL-S16_DEC_MSC_1ST_VOL))*S16_DEC_MSC_2ND_ERR)/(S16_DEC_MSC_2ND_ERR-S16_DEC_MSC_1ST_ERR));

            lcs16DecMscTargetVol=tempW4+(S16_DEC_MSC_2ND_VOL-tempW3);

			  #if __HDC
            if ((lcu1HdcActiveFlg==1)&&(lcs16HdcActiveFlg_on_cnt < (int8_t)L_U8_TIME_10MSLOOP_100MS))
            {
             	lcs16DecMscTargetVol = (lcs16DecMscTargetVol>S16_DEC_INITIAL_TARGET_V)?lcs16DecMscTargetVol:S16_DEC_INITIAL_TARGET_V;
            }
			else { }
              #if __HSA
			if(HSA_flg==1)
			{
				lcs16DecMscTargetVol = S16_DEC_T_VOLT_At_HSA;
				if(lcs16DecMscTargetVol < MSC_0_5_V) {lcs16DecMscTargetVol = MSC_0_5_V;}
				else { }
			}
			else { }
        	  #endif
              #endif

        }
        else
        {
            lcs16DecMscTargetVol=MSC_0_V;
        }

         #if __TCMF_CONTROL
        if(TCMF_MOTOR_OFF_flag0==1) {lcs16DecMscTargetVol=MSC_0_V;}
         #endif

		/*08SWD-10*/
        if (lcu1decFFCtrl_flg ==1)
        {
        	lcs16DecMscTargetVol = (lcs16DecMscTargetVol>S16_DEC_INITIAL_TARGET_V)?lcs16DecMscTargetVol:S16_DEC_INITIAL_TARGET_V;
			  #if __HSA
			if(HSA_flg==1)
			{
				lcs16DecMscTargetVol = S16_DEC_T_VOLT_At_HSA;
				if(lcs16DecMscTargetVol < MSC_0_5_V) {lcs16DecMscTargetVol = MSC_0_5_V;}
				else { }
			}
			else { }
			  #endif
			  #if __ACC
			if(lcu1AccActiveFlg==1)
			{
				lcs16DecMscTargetVol = S16_ACC_INITIAL_TARGET_V;
				if(lcs16DecMscTargetVol < MSC_0_5_V) {lcs16DecMscTargetVol = MSC_0_5_V;}
				else { }
			}
			else { }
			  #endif
        }
        else { }

    }
    else
    {
        lcs16DecMscTargetVol=MSC_0_V;
    }
}

#if	__Decel_Ctrl_Integ
int16_t LAMFC_s16SetMFCCurrentDEC(uint8_t CIRCUIT, DEC_VALVE_t *pDec, int16_t MFC_Current_old)
{
    int16_t     MFC_Current_new = 0;
    if(lcu1DecActiveFlg==1)
    {
        if((MFC_Current_old==0)||(lcu1DecActiveFlgOld==0))
        {
            tempW0=0;	
		  #if __HDC
			if(((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)&&(vref < lcs16HdcTargetSpeed))
				||(MFC_Current_old > 350))
			{
				tempW0 = (int16_t)S16_HDC_INITIAL_CURRENT - 20;
			}
			else
			{
				tempW0 = (int16_t)S16_HDC_INITIAL_CURRENT;
			}
          #endif
          #if __EPB_INTERFACE
            if(lcu1EpbActiveFlg==1)
            {
            	tempW0=(tempW0>(int16_t)S16_EPBI_INITIAL_CURRENT)?tempW0:(int16_t)S16_EPBI_INITIAL_CURRENT;
            }
            else{ }
          #endif
          #if __ACC
            if(lcu1AccActiveFlg==1)
            {
            	tempW0=(tempW0>(int16_t)S16_ACC_INITIAL_CURRENT)?tempW0:(int16_t)S16_ACC_INITIAL_CURRENT;
            }
            else{ }
          #endif
          #if __TSP
            if(TSP_ON==1)
            {
            	tempW0=(tempW0>(int16_t)lcs16TspInitialDecelCurrent)?tempW0:(int16_t)lcs16TspInitialDecelCurrent;            	
            }
            else{ }
          #endif


			tempW0=(tempW0>250)?tempW0:250;									/* Minimum Limit	*/

            if (MFC_Current_old > tempW0 )
            {
                MFC_Current_new = MFC_Current_old;
            }
            else
            {
                MFC_Current_new=  tempW0;
            }

        }
        else
        {
            /********************************************************/
            if((pDec->lau1DecWL1No==0)&&(pDec->lau1DecWL1Nc==0))            /* REAPPLY */
            {
                tempW0=abs(lcs16DecTargetG*5)+550;
               
              #if __EPB_INTERFACE
                if(lcu1EpbActiveFlg==1)
                {
                	tempW0 = (int16_t)S16_EPBI_MAX_CURRENT;
                }
                else{ }
              #endif
                
                if(MFC_Current_old>=tempW0)
                {
                    MFC_Current_new=tempW0;
                }
                else
                {
                      #if (__HDC) && (__TARGET_SPD_ADAPTATION)
                    if((lcu1HdcActiveFlg==1)&&(dec_msc_target_vol>MSC_0_V))  /* fast response at decel rise on hdc */
                    {
                        if(MFC_Current_old < (S16_HDC_INITIAL_CURRENT - 40)) {MFC_Current_new = S16_HDC_INITIAL_CURRENT - 40;}
                        else if(MFC_Current_old < S16_HDC_INITIAL_CURRENT)   {MFC_Current_new = MFC_Current_old + 10;}
                        else                                                 {MFC_Current_new = MFC_Current_old + 3;}
                    }
                    else
                    {
                    	#if __TSP
                    	
                    	if((U1_TSP_FADE_CTRL_ON==1) || ((U1_TSP_DEC_DIFF_CTRL_ACT==1) && (U1_TSP_ADD_TC_CURRENT_P==0) && (U1_TSP_ADD_TC_CURRENT_S==0))) 
											{
												MFC_Current_new=MFC_Current_old;
											}
											else
											{
                    		MFC_Current_new = MFC_Current_old + 3;
                    	}
                    	
                    	#else                    	
                    MFC_Current_new=MFC_Current_old+3;
                    	#endif
                    	
                    }
                      #else
                      
                    #if __TSP
                      
                    if((U1_TSP_FADE_CTRL_ON==1) || ((U1_TSP_DEC_DIFF_CTRL_ACT==1) && (U1_TSP_ADD_TC_CURRENT_P==0) && (U1_TSP_ADD_TC_CURRENT_S==0))) 
										{
											MFC_Current_new=MFC_Current_old;
										}
										else
										{
                    	MFC_Current_new = MFC_Current_old + 3;
                  	}
                  	#else                    	
                    	MFC_Current_new = MFC_Current_old + 3;
                    #endif          	

                      #endif
                }
                }
                /********************************************************/
                else if((pDec->lau1DecWL1No==1)&&(pDec->lau1DecWL1Nc==0))       /* HOLD */
                {
                    MFC_Current_new=MFC_Current_old;
            }
            /********************************************************/
            else if((pDec->lau1DecWL1No==1)&&(pDec->lau1DecWL1Nc==1))       /* DUMP */
            {
                if(MFC_Current_old>200)
                {
                    if(lcs8DecState==S8_DEC_CLOSING)
                    {
                        MFC_Current_new=MFC_Current_old-(int16_t)(lcs16DecClosingCur/L_U8_TIME_10MSLOOP_200MS);
                    	  #if (__EPB_INTERFACE) && (__DISABLE_DITHER_EPBI)
                        if(lcu1EpbActiveFlg==1)
                        {                    	  
                            MFC_Current_new=MFC_Current_old-(int16_t)(lcs16DecClosingCur/L_U8_TIME_10MSLOOP_100MS);
                        }
                          #endif                        
                    }
                    else
                    {
                      #if (__HDC) && (__TARGET_SPD_ADAPTATION)
						if((lcu1HdcActiveFlg==1)&&(lcu8HdcDriverAccelIntend==1)&&(MFC_Current_old<380)) /* Limit press At HDC accel.dump */
                    	{
                    		MFC_Current_new = 380;
                    	}
                    	else {MFC_Current_new = MFC_Current_old - 3;}
                      #else
                        MFC_Current_new=MFC_Current_old-3;
                      #endif
                    }
                }
                else
                {
                    MFC_Current_new=200;
                }
                }
            /********************************************************/
                else                                /* Free Circulation */
                {
                    MFC_Current_new = MFC_Current_old;
                }
        }
        
        if (((lcu1decFFCtrl_flg==1)&&(ABS_fz==0))
		#if __HDC
    	|| ((lcu1HdcActiveFlg==1)&&(lcs16HdcActiveFlg_on_cnt < L_U8_TIME_10MSLOOP_50MS)&&(lcs16HdcActiveFlg_on_cnt > 1)
    	#if __TARGET_SPD_ADAPTATION
    	&&(lcu1HdcNoForcedHoldAtBrk==0)
    	#endif
    	)
		#endif
    	)

    {
        if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=1;}
        if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=1;}
    }
    else
    {
        if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=0;}
        if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=0;}
    }
    }
    else
    {
    	if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=0;}
        if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=0;}
        
          #if (__HDC) && (__TARGET_SPD_ADAPTATION) /* fast dump at HDC engine stall*/
        if((lcu1HdcActiveFlg==1)&&(lcu1hdcEngStallSuspect==1)) {MFC_Current_new = 0;}
        else
        {
            MFC_Current_new=LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_old);
        }
          #else
        MFC_Current_new=LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_old);
          #endif
          
    }

    return MFC_Current_new;
}
#endif

  #if __ADVANCED_MSC
void LCMSC_vSetDECTargetVoltage(void)
{
	if(lcu1DecActiveFlg==1)
	{
		dec_msc_target_vol=lcs16DecMscTargetVol;
		DEC_MSC_MOTOR_ON=1;
	}
	else
	{
		dec_msc_target_vol=0;
		DEC_MSC_MOTOR_ON=0;
	}

}
   #endif


  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LADECCallActHW
	#include "Mdyn_autosar.h"               
#endif  
