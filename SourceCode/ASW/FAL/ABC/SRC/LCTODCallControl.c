
/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCallEngineControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCESPCallEngineControl.h"
#include "LCTODCallControl.h"
#include "LCTCSCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCallMain.h"
#include "LSESPCalSlipRatio.h"
#include "hm_logic_var.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSESPCallSensorSignal.h"
#include "LAABSCallActCan.h"
#if __TVBB
#include "LCTVBBCallControl.h"
#endif

       
  #if __TOD
uint8_t	TOD4wdOpen;
uint8_t	TOD4wdLimReq;
uint8_t	TOD4wdLimMode;
uint8_t	TOD4wdCluLim;
uint16_t	TOD4wdTqcLimTorq;
uint16_t	TOD4wdCluLimTemp;
uint16_t	TOD4wdCluLimTempAlt;
int16_t		TOD4wdThrOversteer;
int16_t		TOD4wdThrUndersteer;
uint8_t	TOD_ON;
uint8_t	TODModeSelector;
uint8_t	TODYawSelector;
uint8_t	TODDisable;
uint8_t	TOD4WDLimModeSelect;

	#if __CAR==HMC_EN && __4WD
uint16_t	TOD4WDTorqRatio[5][5] = {{  0,  0,  0,  0,  0},
								 {  0,  0,  0,  0,  0},
								 {200,300,300,300,300},
								 {200,300,300,300,300},
								 {300,300,300,300,300}};

uint8_t	TOD4WDLimModeMap[5][5] ={{ 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0}};

uint16_t	TOD4WDLimModeMaxValue = 1000;
uint16_t	TOD4WDLimModeMinValue = 0;
	#elif __CAR==KMC_HM
uint16_t	TOD4WDTorqRatio[5][5] = {{  0,  0,  0,  0,  0},
								 {  0,  0,  0,  0,  0},
								 {1000,1000,1000,1000,1000},
								 {300,300,300,300,300},
								 {300,300,300,300,300}};

uint8_t	TOD4WDLimModeMap[5][5] ={{ 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0}};
uint16_t	TOD4WDLimModeMaxValue = 1400;
uint16_t	TOD4WDLimModeMinValue = 0;
	#else
uint16_t	TOD4WDTorqRatio[5][5] = {{  0,  0,  0,  0,  0},
								 {  0,  0,  0,  0,  0},
								 {150,150,150,150,150},
								 {150,150,150,150,150},
								 {150,150,150,150,150}};

uint8_t	TOD4WDLimModeMap[5][5] ={{ 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0},
								 { 0, 0, 0, 0, 0}};
uint16_t	TOD4WDLimModeMaxValue = 1000;
uint16_t	TOD4WDLimModeMinValue = 0;
	#endif
  #endif

uint8_t lstodu8ENG_VAR_OLD;
uint8_t lstodu8ENG_VAR_CHECK;
uint8_t lstodu8ENG_VAR_COUNT;

#if __VDC

#if __TOD
void LCTOD_vCallControl(void);
void LCTOD_vCallMainForTOD(void);
void LCTOD_vCallMain(void);
void LCESP_vSignalProcessingForTOD(void);
void LCTOD_vLoadParameterForTOD(void);
void LCTOD_vCallDetection(void);
void LCTOD_vCallSetControl(void);
void LCTOD_vConvertTOD2EMS(void);
void LCTOD_vCallParameter(void);
void LCTOD_vDetectThreshold(void);
void LCTOD_vDetectTOD(void);
void LCTOD_vDetectTODExit(void);
void LCTOD_vDetectDisable(void);
void LCTOD_vCallInitialize(void);

void LCTOD_vCallControl(void)
{
	#if __GM_FailM      

    if( (fu1ESCDisabledBySW==0)
        ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ABS_OK==1))               
        ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_OFF_SLEEP_MODE_CBS_OK==1))               
        ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))                  
        ||((fu1ESCDisabledBySW==1)&&(RTA_FL.ldabsu1DetSpareTire==1))
        ||((fu1ESCDisabledBySW==1)&&(RTA_FR.ldabsu1DetSpareTire==1))     
        ||((fu1ESCDisabledBySW==1)&&(RTA_RL.ldabsu1DetSpareTire==1))    
        ||((fu1ESCDisabledBySW==1)&&(RTA_RR.ldabsu1DetSpareTire==1))
        ||((fu1ESCDisabledBySW==1)&&(Flg_Flat_Tire_Detect_OK == 1)))    /* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */
    {
    	                 
        if(tcs_error_flg==0)     
        { 
            LCTOD_vCallMainForTOD();                
        }
        else
        {
            LCTOD_vCallInitialize();
        }
    }
    else
    {                               
       LCTOD_vCallInitialize();
    } 
   
    #else         

        #if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
    if(fu1ESCDisabledBySW==0)
    #else
    if((fu1ESCDisabledBySW==0)&&(tcs_error_flg==0))
    #endif	
    
    {
    	LCTOD_vCallMainForTOD(); 
    }
    else
    {  
        LCTOD_vCallInitialize();
    }                 

	#endif
}           
	
void LCTOD_vCallMainForTOD(void)
{
    	#if __4WD_VARIANT_CODE==ENABLE
   	if(lsu8DrvMode == DM_2WD)
    {
		LCTOD_vCallInitialize();
   	}
   	else
   	{
		LCTOD_vCallMain();
   	}
 		#else
	#if __4WD
		LCTOD_vCallMain();
	#else
		LCTOD_vCallInitialize();
	#endif
 		#endif
}

void LCTOD_vCallMain(void)
{
	LCESP_vSignalProcessingForTOD();
	LCTOD_vCallDetection();
	LCTOD_vCallSetControl();
	LCTOD_vConvertTOD2EMS();
}

void LCESP_vSignalProcessingForTOD(void)
{
  #if __EEC_CYCLE_TIME_REDUCTION
	if((lstodu8ENG_VAR_CHECK==0)||(lstodu8ENG_VAR_OLD==0))
	{
		lstodu8ENG_VAR_CHECK = 1;
		lstodu8ENG_VAR_OLD = ENG_VAR;
		LCTOD_vLoadParameterForTOD();
		
		lstodu8ENG_VAR_COUNT++;
	}
	else if(lstodu8ENG_VAR_OLD!=ENG_VAR)
	{
		lstodu8ENG_VAR_OLD = ENG_VAR;
		LCTOD_vLoadParameterForTOD();
		
		lstodu8ENG_VAR_COUNT++;
	}
	else
	{
		;
	}
  #else
	if(wmu1CycleTime21ms_1==1)
	{
		LCTOD_vLoadParameterForTOD();
		
		lstodu8ENG_VAR_COUNT++;
	}
	else
	{
		;
	}	
  #endif
}

void LCTOD_vLoadParameterForTOD(void)
{
	TOD4WDLimModeMaxValue = S16_TOD_MAX_TORQ;
	TOD4WDLimModeMinValue = S16_TOD_MIN_TORQ;

	TOD4WDTorqRatio[2][0] = S16_TOD_TORQ_CST_UNDER2;
	TOD4WDTorqRatio[2][1] = S16_TOD_TORQ_CST_UNDER1;
	TOD4WDTorqRatio[2][2] = S16_TOD_TORQ_CST_STABL;
	TOD4WDTorqRatio[2][3] = S16_TOD_TORQ_CST_OVER1;
	TOD4WDTorqRatio[2][4] = S16_TOD_TORQ_CST_OVER2;

	TOD4WDTorqRatio[3][0] = S16_TOD_TORQ_HOT_UNDER2;
	TOD4WDTorqRatio[3][1] = S16_TOD_TORQ_HOT_UNDER1;
	TOD4WDTorqRatio[3][2] = S16_TOD_TORQ_HOT_STABL;
	TOD4WDTorqRatio[3][3] = S16_TOD_TORQ_HOT_OVER1;
	TOD4WDTorqRatio[3][4] = S16_TOD_TORQ_HOT_OVER2;

	TOD4WDTorqRatio[4][0] = S16_TOD_TORQ_WOT_UNDER2;
	TOD4WDTorqRatio[4][1] = S16_TOD_TORQ_WOT_UNDER1;
	TOD4WDTorqRatio[4][2] = S16_TOD_TORQ_WOT_STABL;
	TOD4WDTorqRatio[4][3] = S16_TOD_TORQ_WOT_OVER1;
	TOD4WDTorqRatio[4][4] = S16_TOD_TORQ_WOT_OVER2;

	TOD4WDLimModeMap[2][0] = U8_TOD_MODE_CST_UNDER2;
	TOD4WDLimModeMap[2][1] = U8_TOD_MODE_CST_UNDER1;
	TOD4WDLimModeMap[2][2] = U8_TOD_MODE_CST_STABL;
	TOD4WDLimModeMap[2][3] = U8_TOD_MODE_CST_OVER1;
	TOD4WDLimModeMap[2][4] = U8_TOD_MODE_CST_OVER2;

	TOD4WDLimModeMap[3][0] = U8_TOD_MODE_HOT_UNDER2;
	TOD4WDLimModeMap[3][1] = U8_TOD_MODE_HOT_UNDER1;
	TOD4WDLimModeMap[3][2] = U8_TOD_MODE_HOT_STABL;
	TOD4WDLimModeMap[3][3] = U8_TOD_MODE_HOT_OVER1;
	TOD4WDLimModeMap[3][4] = U8_TOD_MODE_HOT_OVER2;

	TOD4WDLimModeMap[4][0] = U8_TOD_MODE_WOT_UNDER2;
	TOD4WDLimModeMap[4][1] = U8_TOD_MODE_WOT_UNDER1;
	TOD4WDLimModeMap[4][2] = U8_TOD_MODE_WOT_STABL;
	TOD4WDLimModeMap[4][3] = U8_TOD_MODE_WOT_OVER1;
	TOD4WDLimModeMap[4][4] = U8_TOD_MODE_WOT_OVER2;
}

void LCTOD_vCallDetection(void)
{
	LCTOD_vDetectDisable();

	if(!TODDisable)
	{
		LCTOD_vDetectThreshold();

		if(!TOD_ON)
		{
			LCTOD_vDetectTOD();
		}
		else
		{
			LCTOD_vDetectTODExit();
		}
	}
	else
	{
		LCTOD_vCallInitialize();
	}
}

void LCTOD_vDetectThreshold(void)
{
	#if (__EEC_OVERSTEER_THRESHOLD == 1)
	TOD4wdThrOversteer  = o_delta_yaw_third_ems; /*make signal from EEC*/
	#else
	TOD4wdThrOversteer  = o_delta_yaw_thres_eec; /*make signal from EEC*/
	#endif /*#if (__EEC_OVERSTEER_THRESHOLD == 1)*/
	TOD4wdThrUndersteer = delta_yaw_third_ems; /*make signal from EEC*/
}

void LCTOD_vDetectDisable(void)
{
	TODDisable = 0;

        
    #if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
	lcu1TodCtrlFail = 0;
        if((fu1FMWheelFLErrDet==1)||(fu1FMWheelFRErrDet==1)||(fu1FMWheelRLErrDet==1)||(fu1FMWheelRRErrDet==1)||(ee_ece_flg==1) 
          ||(fu1FMWheelFrontErrDet==1)||(fu1FMWheelRearErrDet==1)||(fu1FMSameSideWSSErrDet==1)||(fu1FMDiagonalWSSErrDet==1)  /*Wheel Error*/
	      ||(fu1FMSolenoidErrDet==1) || (fu1FMESCSolErrDet==1)||(fu1FMABSSolErrDet==1)||(fu1FMFrontSolErrDet==1)||(fu1FMRearSolErrDet==1) /*Solenoid Valve Error*/
	      ||(fu1FMMotorErrDet==1)||(fu1FMVRErrDet==1)||(fu1FMECUHwErrDet==1) /*Motor, Valve relay, Micom Fail*/
	      ||(fu1FMVoltageOverErrDet==1)||(fu1FMVoltageUnderErrDet==1)||(fu1FMVoltageLowErrDet==1) /*Motor voltage range error*/
          ||(fu1FMYawErrDet==1)||(fu1FMAyErrDet==1)||(fu1FMStrErrDet==1) /*yaw, ay, steer error*/
          ||(fu1FMMCPErrDet==1)/*MCP*/
          ||(ccu1EMS_N_ErrFlg==1)||(ccu1EMS_TQI_ErrFlg==1) /*rpm*/
          ||(ccu1EMS_TQI_ACOR_ErrFlg==1)||(ccu1EngVar_ErrFlg==1) /*engien torque,engine variation error*/
          ||(fu1FMVCErrDet==1)   /* variant coding error*/
          ||(ccu1AWD_4WD_ERR_ErrFlg==1) /*4WD Error*/
      	  ||(ccu1EMS_TPS_ErrFlg==1)
          ||(ccu1EMS_PV_AV_CAN_ErrFlg==1)  
	      ||(ccu1TCU_G_SEL_DISP_ErrFlg==1)
          ||(ccu1TCU_TAR_GC_ErrFlg==1)
      	  
      	  #if (__CAR_MAKER==SSANGYONG)
		  ||(ccu1TCU_SWI_GS_ErrFlg==1)
	      #else
	      /*||(fu1FMBLSErrDet==1)   /*BLS error */
	      ||(ccu1TQI_TARGET_ErrFlg==1) /*rpm, drive torque error*/      
	      #endif          
          
          )
                                                           
   		{
    		TODDisable=1;
    		lcu1TodCtrlFail=1;
    	} 
    	else
   	 	{
    		;
    	}
		#if ( __4WD_VARIANT_CODE == ENABLE)     /*Longitudinal G sensor Error*/
	   
	   		if (lsu8DrvMode ==DM_2WD)
	   		{
	     		;
	     		/* 2WD */
	   		}
	   		else
	   		{
	     		/* 4WD (2H,4H, 4L, AUTO) */
	       		if(fu1FMAxErrDet==1)
	       		{
	           		TODDisable=1;
	           		lcu1TodCtrlFail=1;
	       		}  
	       		else
	       		{
	         		;
	       		}  
	   		}
		#else
	   		#if (__4WD==ENABLE)	   
	   		/* 4WD */
	       		if(fu1FMAxErrDet==1)
	       		{
	           		TODDisable=1;	 
	           		lcu1TodCtrlFail=1;
	       		}
	       		else
	       		{
	         		;
	       		}          
	   		#else
	     	/* 2WD */
	   		#endif
		#endif       /*Longitudinal G sensor Error*/
  	
  	#else /*NEW_FM_ENABLE*/   	

  #if __GM_FailM
	if((lctcsu1EngCtrlInhibit==1)||(lctcsu1InhibitByFM==1))
	{
		TODDisable=1;
	}
	else
	{
		;
	}
  #else
	if((abs_error_flg==1)||(ebd_error_flg==1)||(tcs_error_flg==1)
	||(tc_error_flg==1)||(vdc_error_flg==1)
	||(fu1VoltageUnderErrDet==1)||(fu1VoltageLowErrDet==1)||(fu1VoltageOverErrDet==1)) 
	{
		TODDisable=1;
	}
	else
	{
		;
	}
  #endif
        
    #endif    /*NEW_FM_ENABLE*/

  	if ((wu8IgnStat==CE_OFF_STATE)||(fu1OnDiag==1)||(init_end_flg==0))
    {
    	TODDisable=1;
    }
    else
    {;}      
}

void LCTOD_vDetectTOD(void)
{
	if(laabsu1TrnsBrkSysCltchRelRqd==1)
	{
		TOD_ON=1;
	}
	else if((ESP_TCS_ON))
	{
		TOD_ON=1;
	}
	else
	{
		LCTOD_vCallInitialize();
	}
}

void LCTOD_vDetectTODExit(void)
{
	if((!laabsu1TrnsBrkSysCltchRelRqd)&&(!ESP_TCS_ON))
	{
		LCTOD_vCallInitialize();
	}
}

void LCTOD_vCallSetControl(void)
{
	if(!TODDisable)
	{
		if(TOD_ON==1)
		{
			LCTOD_vCallParameter();
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

void LCTOD_vCallParameter(void)
{
	TODModeSelector=0;
	TODYawSelector=0;

    	#if __BRK_SIG_MPS_TO_PEDAL_SEN
    if( ((laabsu1TrnsBrkSysCltchRelRqd==1)&&(lsespu1BrkAppSenInvalid==0)&&(lsesps16EstBrkPressByBrkPedal>=MPRESS_40BAR)) ||
		 ((laabsu1TrnsBrkSysCltchRelRqd==1)&&(lsespu1BrkAppSenInvalid==1)&&((fu1BLSErrDet==0)&&(BLS==1))) || 
		 ((laabsu1TrnsBrkSysCltchRelRqd==1)&&(lsespu1BrkAppSenInvalid==1)&&(fu1BLSErrDet==1))						
	  ) 
	#else 
    if((laabsu1TrnsBrkSysCltchRelRqd==1) && (mpress>=MPRESS_40BAR))	
		#endif
	{
		TODModeSelector=1;		/* Full Braking with ABS */
	}
    	#if __BRK_SIG_MPS_TO_PEDAL_SEN
    else if((laabsu1TrnsBrkSysCltchRelRqd==1)&&(lsespu1BrkAppSenInvalid==0)&&(lsesps16EstBrkPressByBrkPedal<MPRESS_40BAR))					
	#else
	else if((laabsu1TrnsBrkSysCltchRelRqd==1) && (mpress<MPRESS_40BAR))
		#endif
	{
		TODModeSelector=2;		/* Half Braking with ABS */
	}
	else if(((ESP_TCS_ON==1)||(ltcsu81SecCntAfterETO>L_U8_TIME_10MSLOOP_700MS)) && (mtp<U8_TOD_MTP_THR))
	{
		TODModeSelector=4;		/* Half Accel with ESP */
	}
	else if(((ESP_TCS_ON==1)||(ltcsu81SecCntAfterETO>L_U8_TIME_10MSLOOP_700MS)) && (mtp>=U8_TOD_MTP_THR))
	{
		TODModeSelector=5;		/* Full Accel with ESP */
	}
	else
	{
		TODModeSelector=3;		/* Coast */
	}

	if(delta_yaw_first<TOD4wdThrUndersteer*U8_TOD_UNDER_THR_G1/100)
	{
		if(delta_yaw_first<(TOD4wdThrUndersteer*U8_TOD_UNDER_THR_G2/100))
		{
			TODYawSelector=1;	/* Sever Under */
		}
		else
		{
			TODYawSelector=2;	/* Under */
		}
	}
	else if(delta_yaw_first>TOD4wdThrOversteer*U8_TOD_OVER_THR_G1/100)
	{
		if(delta_yaw_first<(TOD4wdThrOversteer*U8_TOD_OVER_THR_G2/100))
		{
			TODYawSelector=5;	/* Sever Over */
		}
		else
		{
			TODYawSelector=4;	/* Over */
		}
	}
	else
	{
		TODYawSelector=3;
	}

	if(TODModeSelector>=4)
	{
		if((TODYawSelector==1)||(TODYawSelector==2))
		{
			tempW1 = (TOD4wdThrUndersteer*2);
			tempW2 = TOD4wdThrUndersteer;
			TOD4wdCluLimTemp=(uint16_t)LCESP_s16IInter2Point(delta_yaw_first,tempW1,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][1],
                                                        tempW2,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][0]);
		}
		else if((TODYawSelector==4)||(TODYawSelector==5))
		{
			tempW1 = TOD4wdThrOversteer;
			tempW2 = (TOD4wdThrOversteer*2);
			TOD4wdCluLimTemp=(uint16_t)LCESP_s16IInter2Point(delta_yaw_first,tempW1,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][3],
                                                        tempW2,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][4]);
		}
		else
		{
			tempW1 = TOD4wdThrUndersteer;
			tempW2 = TOD4wdThrOversteer;
			TOD4wdCluLimTemp=(uint16_t)LCESP_s16IInter2Point(delta_yaw_first,tempW1,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][3],
                                                        tempW2,(int16_t)TOD4WDTorqRatio[TODModeSelector-1][3]);
		}
	}
	else
	{
		TOD4wdCluLimTemp=TOD4WDTorqRatio[TODModeSelector-1][TODYawSelector-1];
	}

	if(TOD4wdCluLimTemp > TOD4wdCluLimTempAlt + 20)
	{
		TOD4wdCluLimTemp = TOD4wdCluLimTempAlt + 20;
	}
	else if((TOD4wdCluLimTemp < TOD4wdCluLimTempAlt - 20) && (TOD4wdCluLimTemp > 0))
	{
		TOD4wdCluLimTemp = TOD4wdCluLimTempAlt - 20;
	}
	else
	{
		;
	}

	if(TOD4wdCluLimTemp < TOD4WDLimModeMinValue)
	{
		TOD4wdCluLimTemp = TOD4WDLimModeMinValue;
	}
	else if(TOD4wdCluLimTemp > TOD4WDLimModeMaxValue)
	{
		TOD4wdCluLimTemp = TOD4WDLimModeMaxValue;
	}
	else
	{
		;
	}

	if((TODModeSelector>0) && (TODYawSelector>0))
	{
		TOD4WDLimModeSelect = TOD4WDLimModeMap[TODModeSelector-1][TODYawSelector-1];
	}
	else
	{
		TOD4WDLimModeSelect = 0;
	}

	TOD4wdCluLimTempAlt = TOD4wdCluLimTemp;
}

void LCTOD_vCallInitialize(void)
{
	TODModeSelector		=0;
	TODYawSelector		=0;

	#if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
		if((lcu1TodCtrlFail==1)||(lcu1TcsDiscTempErrFlg==1))
		{
			TOD4wdTqcLimTorq	=65535;
		}
		else
		{
			TOD4wdTqcLimTorq	=64255;
		}
	#else   /*NEW_FM_ENABLE*/
  #if __GM_FailM
	if((lctcsu1EngCtrlInhibit==1)||(lctcsu1InhibitByFM==1))
	{
		TOD4wdTqcLimTorq	=65535;
	}
	else
	{
		TOD4wdTqcLimTorq	=64255;
	}
  #else
	if((abs_error_flg==1)||(ebd_error_flg==1)||(tcs_error_flg==1)
	||(tc_error_flg==1)||(vdc_error_flg==1)
	||(fu1VoltageUnderErrDet==1)||(fu1VoltageLowErrDet==1)||(fu1VoltageOverErrDet==1)) 
	{
		TOD4wdTqcLimTorq	=65535;
	}
	else
	{
		TOD4wdTqcLimTorq	=64255;
	}
  #endif
	#endif	/*NEW_FM_ENABLE*/
  
	TOD4wdCluLimTemp	=TOD4WDTorqRatio[2][2];
	TOD4wdCluLimTempAlt =TOD4WDTorqRatio[2][2];
	TOD_ON=0;
}

void LCTOD_vConvertTOD2EMS(void)
{
	#if(NEW_FM_ENABLE==ENABLE)	
	if(lcu1TodCtrlFail==1)
	#else
	if(TODDisable==1)
	#endif
	{
		TOD4wdTqcLimTorq	=0xFFFF;
		TOD4wdOpen			=3;
		TOD4wdLimReq		=0;
		TOD4wdLimMode		=0;
		TOD4wdCluLim		=0xFF;
	}
	else if(TOD_ON==1)
	{
		if(laabsu1TrnsBrkSysCltchRelRqd==1)
		{
			TOD4wdTqcLimTorq	=0x0000;
			TOD4wdOpen			=1;
			TOD4wdLimReq		=0;
			TOD4wdLimMode		=0;
			TOD4wdCluLim		=0;
		}
		else if (ESP_TCS_ON==1)	/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
		{
			TOD4wdTqcLimTorq	=(uint16_t)(TOD4wdCluLimTemp);
			TOD4wdOpen			=2;
			TOD4wdLimReq		=1;

		#if __REAR_D
		  	TOD4wdLimMode		=TOD4WDLimModeSelect;
		#else
		  	TOD4wdLimMode		=0;
		#endif
			TOD4wdCluLim		=0;
		}
		else
		{
			TOD4wdTqcLimTorq	=0xFAFF;
			TOD4wdOpen			=0;
			TOD4wdLimReq		=0;
			TOD4wdLimMode		=0;
			TOD4wdCluLim		=0;
		}
	}
	else
	{
		TOD4wdTqcLimTorq	=0xFAFF;
		TOD4wdOpen			=0;
		TOD4wdLimReq		=0;
		TOD4wdLimMode		=0;
		TOD4wdCluLim		=0;
	}
}
#endif /* End of #if __TOD */

#endif /*End of #if __VDC*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCallEngineControl
	#include "Mdyn_autosar.h"
#endif

