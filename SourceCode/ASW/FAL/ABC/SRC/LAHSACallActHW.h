#ifndef __LAHSACALLACTHW_H__
#define __LAHSACALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __HSA
/*Global Type Declaration ****************************************************/

/* BKTU tuning parameter */
/*
#define S16_HSA_TC_HOLD_PRESS_MIN          50
#define S16_HSA_TC_HOLD_PRESS_MID          250
#define S16_HSA_TC_HOLD_PRESS_MAX          600
#define S16_HSA_TC_INITIAL_COMP_MIN  		100
#define S16_HSA_TC_INITIAL_COMP_MID			-50
#define S16_HSA_TC_INITIAL_COMP_MAX			-100
#define S16_HSA_ISG_1st_Drop_Cur          50
#define S16_HSA_TC_HOLD_CUR_MIN          280  
#define S16_HSA_TC_HOLD_CUR_MID          520  
#define S16_HSA_TC_HOLD_CUR_MAX          750
#define S16_HSA_TC_CUR_COMP_1ST_MIN     -100
#define S16_HSA_TC_CUR_COMP_1ST_MID     -160
#define S16_HSA_TC_CUR_COMP_1ST_MAX     -300
#define S16_HSA_TC_CUR_MAX_AT_1ST_SCAN   800      
#define S16_HSA_TC_CUR_COMP_AT_MP_ON     100
#define S16_HSA_TC_CUR_COMP_AT_MP_ON_2   100
#define S16_HSA_MOTOR_TARGET_VOLT        MSC_2_V
#define S16_HSA_MOTOR_ON_TIME            L_U8_TIME_10MSLOOP_300MS
#define S16_HSA_CUR_AT_RISE              850
*/

/*Global Extern Variable  Declaration*****************************************/
extern int16_t    MFC_HSA_DEC_CURRENT;
extern uint8_t    HSA_phase_out_toggle_cnt;
extern int16_t    hsa_msc_target_vol;
/*Global Extern Functions  Declaration****************************************/
extern void	LAHSA_vCallActHW(void);
extern int16_t LAMFC_s16SetMFCCurrentHSA(int16_t MFC_Current_old);
extern void	LCMSC_vSetHSATargetVoltage(void);

  #endif
#endif
