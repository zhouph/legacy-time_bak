
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSCDCCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************/
#include "LVarHead.h"
/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/
extern uint8_t CDC_LOG_DATA3[8];
extern uint8_t WHL_AZ_FL_CDC  ; 
extern uint8_t WHL_AZ_FR_CDC	;
extern uint8_t AZ_FL_CDC  	; 	
extern uint8_t AZ_FR_CDC 	 	;
extern uint8_t AZ_RR_CDC    	;


/* Local Function prototype ****************************************/

  #if defined(__UCC_1)
	void LSCDC_vCallSensorSignal(void);
	void CDC_PRE_PROCESSOING(void);
	int16_t HPF_0_1Hz(int16_t ,int16_t , int16_t);
  #endif

/* Implementation***************************************************/
  #if defined(__UCC_1)
void LSCDC_vCallSensorSignal(void)
{
    CDC_PRE_PROCESSOING();
}
/******************************************************************/
int16_t HPF_0_1Hz(int16_t input, int16_t input_old, int16_t x_old)
{
    tempW2 = ( x_old + input - input_old );
    tempW1 = 1024; tempW0 = 1029;
    s16muls16divs16();
    
    return (tempW3);
}
/******************************************************************/
void CDC_PRE_PROCESSOING(void)
{
    loucFlagRoll 	= CDC_LOG_DATA3[2];
    lducFlagDive 	= CDC_LOG_DATA3[3];
    lsucFlagSquat 	= CDC_LOG_DATA3[4];
    ljucRoadCond 	= CDC_LOG_DATA3[5];

    cdc_wz_fl_old = cdc_wz_fl;
    cdc_wz_fl_HF_old = cdc_wz_fl_HF;
    cdc_wz_fl_raw_old = cdc_wz_fl_raw;
    
    cdc_wz_fr_old = cdc_wz_fr;
    cdc_wz_fr_HF_old = cdc_wz_fr_HF;
    cdc_wz_fr_raw_old = cdc_wz_fr_raw;
        
    cdc_bz_fl_old = cdc_bz_fl;
    cdc_bz_fl_HF_old = cdc_bz_fl_HF;
    cdc_bz_fl_raw_old = cdc_bz_fl_raw;
    
    cdc_bz_fr_old = cdc_bz_fr;
    cdc_bz_fr_HF_old = cdc_bz_fr_HF;
    cdc_bz_fr_raw_old = cdc_bz_fr_raw;
    
    cdc_bz_rr_old = cdc_bz_rr;
    cdc_bz_rr_HF_old = cdc_bz_rr_HF;
    cdc_bz_rr_raw_old = cdc_bz_rr_raw;

    cdc_wz_fl_raw = (WHL_AZ_FL_CDC-100)*10;     // r:0.01   g
    cdc_wz_fr_raw = (WHL_AZ_FR_CDC-100)*10;     // r:0.01   g
    cdc_bz_fl_raw = (AZ_FL_CDC-100)*10;         // r:0.001  g
    cdc_bz_fr_raw = (AZ_FR_CDC-100)*10;         // r:0.001  g
    cdc_bz_rr_raw = (AZ_RR_CDC-100)*10;         // r:0.001  g
    
    // High Pass filter to remove dc-offset              0.1 Hz
    cdc_wz_fl_HF = HPF_0_1Hz(cdc_wz_fl_raw, cdc_wz_fl_raw_old, cdc_wz_fl_HF_old); 

    cdc_wz_fr_HF = HPF_0_1Hz(cdc_wz_fr_raw, cdc_wz_fr_raw_old, cdc_wz_fr_HF_old);

    cdc_bz_fl_HF = HPF_0_1Hz(cdc_bz_fl_raw, cdc_bz_fl_raw_old, cdc_bz_fl_HF_old);

    cdc_bz_fr_HF = HPF_0_1Hz(cdc_bz_fr_raw, cdc_bz_fr_raw_old, cdc_bz_fr_HF_old);
    
    cdc_bz_rr_HF = HPF_0_1Hz(cdc_bz_rr_raw, cdc_bz_rr_raw_old, cdc_bz_rr_HF_old);
    
    // Low Pass filter to remove High-frequency noise     10 Hz
    cdc_wz_fl = LCESP_s16Lpf1Int(cdc_wz_fl_HF, cdc_wz_fl_old, 56);

    cdc_wz_fr = LCESP_s16Lpf1Int(cdc_wz_fr_HF, cdc_wz_fr_old, 56);

    cdc_bz_fl = LCESP_s16Lpf1Int(cdc_bz_fl_HF, cdc_bz_fl_old, 56);

    cdc_bz_fr = LCESP_s16Lpf1Int(cdc_bz_fr_HF, cdc_bz_fr_old, 56);
    
    cdc_bz_rr = LCESP_s16Lpf1Int(cdc_bz_rr_HF, cdc_bz_rr_old, 56);
}
/******************************************************************/

  #endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSCDCCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
