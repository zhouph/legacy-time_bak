/*
 * ActiveBrakeControlInOutIfTemp.c
 *
 *  Created on: 2014. 12. 9.
 *      Author: sohyun.ahn
 */

#include "LVarHead.h"
#include "hm_logic_var.h"
#include "LCTCSCallControl.h"
#include "LCABSDecideCtrlState.h"
#include "LAESPCallActHW.h"
#include "LATCSCallActHW.h"
#include "LABACallActHW.h"
#include "LCABSStrokeRecovery.h"
#include "LCHSACallControl.h"
#include "LSESPCalSensorOffset.h"
#include "LCallMain.h"
#include "LACallMain.h"
#include "LCESPInterfaceSlipController.h"
#include "hm_logic_var.h"

int16_t lis16MasterPress;
int16_t lis16DriverIntendedTP;
int16_t lis16PDFRaw;
int16_t lis16PDFofs;
int16_t lis16PDTRaw;
int16_t lis16PDTofs;
int16_t lis16Position;
int16_t lis16PedalTravelFilt;
int16_t lcans16AhbRbcPress;

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
int16_t lis16MCPRawPress;
int16_t lis16MCP2RawPress;
#elif __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
int16_t lis16MCPRawPress;
#endif
int16_t lis16PedalSimulPressRaw;

int16_t max_speed;
int16_t min_speed;
int16_t max_3rd_speed;
int16_t max_2nd_speed;

int16_t a_long_1_1000g;
int16_t yaw_1_100deg;
int16_t steer_1_10deg;
int16_t a_lat_1_1000g;

int16_t FeeDataReadInvalidFlg;
int16_t   read_yaw_eeprom_offset;
int16_t fs_yaw_offset_eeprom_ok_flg;
int16_t req_yaw_eeprom_write_flg;
//int16_t fs_yaw_init_rq_flg;
int16_t flu1BrakeLampErrDet;
U32_BIT_STRUCT_t ActvBrkCrtlIfFlgs_Rx;
U32_BIT_STRUCT_t ActvBrkCrtlIfFlgs_Tx;

int16_t wu1IgnRisingEdge;
int8_t lis8FRONT_SLIP_DET_THRES_SLIP;
int8_t lis8FRONT_SLIP_DET_THRES_VDIFF;
#define    WHL_VLV_TEST	DISABLE
#if WHL_VLV_TEST==ENABLE
    uint16_t WhlVlvTestCompleted;
    int16_t WhlInletVlvOnReqCntForTest;
    int16_t WhlOutletVlvOnReqCntForTest;
    int16_t WhlVlvOnDlyReqCntForTest;
    int16_t WhlVlvOnReqCntForTest;
#endif

int16_t lis16EstWheelPressFL;
int16_t lis16EstWheelPressFR;
int16_t lis16EstWheelPressRL;
int16_t lis16EstWheelPressRR;

int8_t lis8StrkRcvrCtrlState_Rx;
int8_t lis8RecommendStkRcvrLvl_Rx;
int16_t lis16StrkRcvrAbsAllowedTime_Tx;

/* Function Prototypes */
void ActvBrkCtrlInpIf(void);
static void arrange_v(void);
void ActvBrkCtrlOutpIf(void);
static void sorting_spd(UINT *spd_ptr);


void ActvBrkCtrlInpIf(void)
{
	liu1PdtSenFaultDet		= Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_PedalPDT;//Abc_CtrlBus.Abc_CtrlErrPedlInfo.PdtErr;
	liu1PdtSigSusSet		= Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT;//Abc_CtrlBus.Abc_CtrlPedlTrvlSigInvldInfo.PdtSigSuspcFlg;
	liu1PdfSenFaultDet		= Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_PedalPDF;//Abc_CtrlBus.Abc_CtrlErrPedlInfo.PdfErr;
	liu1PdfSigSusSet		= Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF;//Abc_CtrlBus.Abc_CtrlPedlTrvlSigInvldInfo.PdfSigSuspcFlg;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	liu1BCP1SenFaultDet		= Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_CirP1;//Abc_CtrlBus.Abc_CtrlPistPInvldInfo.PistPSigFaultFlg;
	lis16MasterPress		= Abc_CtrlBus.Abc_CtrlPistPFildInfo.PistPFild_1_100Bar/10; /*scale Adjustment: 0.01bar->0.1ar*/
	lis16MCPRawPress		= Abc_CtrlBus.Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd; /*used only for WPE - 0.01bar . should be removed after WPE modulization*/
	#else
	liu1BCP1SenFaultDet		= Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_CirP1;
	lis16MasterPress		= Abc_CtrlBus.Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar/10;/*scale Adjustment: 0.01bar->0.1ar*/
	lis16MCPRawPress		= Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;/*used only for WPE - 0.01bar . should be removed after WPE modulization*/
	lis16MCP2RawPress		= Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;/*used only for WPE - 0.01bar . should be removed after WPE modulization*/

#endif

	lis16PedalSimulPressRaw = Abc_CtrlBus.Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;/*used only for WPE - 0.01bar . should be removed after WPE modulization*/

	liu1AHBon				= Abc_CtrlBus.Abc_CtrlPCtrlAct;

	liu1SimVVActFlg 		= 0;//Abc_CtrlBus.Abc_CtrlIdbSimrVlvCtrlStInfo.VlvActFlg;
	liu1CutVV1ActFlg		= 0;//Abc_CtrlBus.Abc_CtrlIdbCutVlv1CtrlStInfo.VlvActFlg;
	liu1CutVV2ActFlg		= 0;//Abc_CtrlBus.Abc_CtrlIdbCutVlv2CtrlStInfo.VlvActFlg;
	liu1RelVV1ActFlg		= 0;//Abc_CtrlBus.Abc_CtrlIdbCircVlv1CtrlStInfo.VlvActFlg;
	liu1RelVV2ActFlg		= 0;// Abc_CtrlBus.Abc_CtrlIdbCircVlv2CtrlStInfo.VlvActFlg;


	lis16DriverIntendedTP  	= Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl/10;/*scale Adjustment: 0.01bar->0.1ar*/

//	liu1MtrFaultSusDet		= liu1MtrFaultSusDet_Tx;
//	liu1MtrFaultDet			= liu1MtrFaultDet_Tx;
//	liu1RBCTPReductionFlg	= liu1RBCTPReductionFlg_Tx; // RTE variable check!!


//	liu1TestModeAct_BBS			=liu1TestModeAct_BBS_Tx;
//	liu1FlgHoldFL			=liu1FlgHoldFL_Tx;
//	liu1FlgHoldFR			=liu1FlgHoldFR_Tx;
//	liu1FlgHoldRL			=liu1FlgHoldRL_Tx;
//	liu1FlgHoldRR			=liu1FlgHoldRR_Tx;
//	liu1FlgDumpFL			=liu1FlgDumpFL_Tx;
//	liu1FlgDumpFR			=liu1FlgDumpFR_Tx;
//	liu1FlgDumpRL			=liu1FlgDumpRL_Tx;
//	liu1FlgDumpRR 			=liu1FlgDumpRR_Tx;
//	liu1FlgCircFL			=liu1FlgCircFL_Tx;
//	liu1FlgCircFR			=liu1FlgCircFR_Tx;
//	liu1FlgCircRL			=liu1FlgCircRL_Tx;
//	liu1FlgCircRR 			=liu1FlgCircRR_Tx;
//	liu1ChkCircPOK			= liu1ChkCircPOK_Tx;
//	liu1InhibitRecvrByBBS	= liu1InhibitRecvrByBBS_Tx;



	lis16PDFRaw 			= Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd;
	lis16PDFofs 			= 0;
	lis16PDTRaw 			= Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd;
	lis16PDTofs 			= 0;

	lis16Position	    	= (int32_t)(Abc_CtrlBus.Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd)*100/S32_MTRPOSI_RESOL; /*0702*/
	lis16PedalTravelFilt 	= Abc_CtrlBus.Abc_CtrlPedlTrvlFinal;
	lcans16AhbRbcPress		= Abc_CtrlBus.Abc_CtrlFinalTarPFromPCtrl;/*scale Adjustment: 0.01bar->0.1ar*/
	liu1DriverIntendToRelBrk = Abc_CtrlBus.Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;

	a_long_1_1000g			= Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.A_long_1_1000g;/*Abc_CtrlBus.Abc_CtrlCanRxEscInfo.Ax;*/
	yaw_1_100deg			= Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.EstimatedYaw;/*Abc_CtrlBus.Abc_CtrlCanRxEscInfo.YawRate;*/
	steer_1_10deg			= Abc_CtrlBus.Abc_CtrlCanRxEscInfo.SteeringAngle;
	a_lat_1_1000g			= Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.EstimatedAY;/*Abc_CtrlBus.Abc_CtrlCanRxEscInfo.Ay;*/

	FL.lsabss16WhlSpdRawSignal64	= Abc_CtrlBus.Abc_CtrlWhlSpdInfo.FlWhlSpd;
	FR.lsabss16WhlSpdRawSignal64    = Abc_CtrlBus.Abc_CtrlWhlSpdInfo.FrWhlSpd;
	RL.lsabss16WhlSpdRawSignal64    = Abc_CtrlBus.Abc_CtrlWhlSpdInfo.RlWhlSpd;
	RR.lsabss16WhlSpdRawSignal64    = Abc_CtrlBus.Abc_CtrlWhlSpdInfo.RrlWhlSpd;

	FeeDataReadInvalidFlg=NvMIf_LogicEepData.Eep.ReadInvldFlg;

    /* Unused Data(for Electric Car)
    lesps16EMS_TQI_ACOR                 = Abc_CtrlBus.Abc_CtrlCanRxDataInfo.EmsTqiAcor;
    lesps16EMS_TQI                      = Abc_CtrlBus.Abc_CtrlCanRxDataInfo.EmsTqi;
    */
    /*EMS Message*/
    lespu8EMS_ENG_CHR                   = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngChr;
    lespu8EMS_ENG_VOL                   = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngVol;
    lespu1EMS_AT_TCU                    = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.AtType;
    lespu1EMS_MT_TCU                    = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MtType;
    lespu1EMS_CVT_TCU                   = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.CvtType;
    lespu1EMS_DCT_TCU					= Abc_CtrlBus.Abc_CtrlCanRxEscInfo.DctType;
    lespu1EMS_HEV_AT_TCU				= Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HevAtTcu;
	lesps16EMS_N                        = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngRpm;
    lesps16EMS_TPS                      = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ThrottleAngle;
    lesps16EMS_TPS_Resol1000            = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TpsResol1000;
    lespu1EMS_TPS_ERR                   = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ThrottleAngleErr;
    lesps16EMS_PV_AV_CAN                = Abc_CtrlBus.Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
    lesps16EMS_PV_AV_CAN_Resol1000      = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.PvAvCanResol1000;
    lespu1EMS_PV_AV_CAN_ERR             = Abc_CtrlBus.Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;/*In IVSS, Sensor Offset*/	
	lesps16EMS_TQFR                     = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngFrictionLossTq;
    lesps16EMS_TQ_STND                  = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngStdTq;
    lesps16EMS_TEMP_ENG                 = Abc_CtrlBus.Abc_CtrlCanRxEngTempInfo.EngTemp;
    lespu8HCU_EngCltStat                = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngClutchState;
    lesps16EMS_ActIndTq_Pc              = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngActIndTq;
    lesps16MCU_CR_Mot_EstTq_Pc          = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MotEstTq;
    lesps16HCU_EngTqCmdBInv_Pc          = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv;
    lesps16HCU_MotTqCmdBInv_Pc          = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv;
    lespu1EMS_TEMP_ENG_ERR              = Abc_CtrlBus.Abc_CtrlCanRxEngTempInfo.EngTempErr;
    lcans16CF_Ems_DecelReq              = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.DecelReq;/*In ACC*/
    lespu1EMS_F_N_ENG                   = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngSpdErr;/*In IVSS*/
    lesps16EMS_IndTq_Pc                 = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngIndTq;/*Non-Used*/
    
    /*TCU Message*/
    lespu8TCU_G_SEL_DISP				= Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.GearSelDisp;
    lespu8TCU_TAR_GC					= Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TarGearPosi;
    lespu1TCU_SWI_GS					= Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TcuSwiGs;
    lespu16TCU_N_TC                     = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TurbineRpm;
    lespu1_TopTrvlCltchSwAct            = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct;
    lespu1_TopTrvlCltchSwActV           = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV;
    lespu8TCU_GEAR_TYPE                 = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.GearType;/*Non-Used*/
    lesps16TCU_CR_Tcu_TqRedReq_Pc       = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIntvTCU;/*Non-Used*/
    lesps16TCU_CR_Tcu_TqRedReqSlw_Pc    = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIntvSlowTCU;/*Non-Used*/
    lesps16TCU_CR_Tcu_TqIncReq_Pc       = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIncReq;/*Non-Used*/
    lespu1TCU_G_SEL_DISP_ERR            = Abc_CtrlBus.Abc_CtrlCanRxGearSelDispErrInfo;/*Non-Used*/
    lespu1TCU_N_TC_ERR                  = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TurbineRpmErr;/*Non-Used*/

	/*Function Message*/
    lcanu1HDC_HillDesCtrlMdSwAct        = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct;/*In HDC*/
    lcanu1HDC_HillDesCtrlMdSwActV       = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV;/*In HDC*/        
    lespu1CF_Clu_WiperIntSW             = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperIntSW;/*In BDW*/
    lespu1CF_Clu_WiperLow               = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperLow;/*In BDW*/
    lespu1CF_Clu_WiperHigh              = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperHigh;/*In BDW*/
    lespu1CF_Clu_WiperValidFlg          = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperValid;/*In BDW*/
    lespu1CF_Clu_WiperAuto              = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperAuto;/*In BDW*/
    lespu1TCU_CF_Tcu_Flt                = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TcuFaultSts;/*Non-Used*/
    lespu8CF_Clu_RainSnsStat            = Abc_CtrlBus.Abc_CtrlCanRxEscInfo.RainSnsStat;/*In BDW*/

	lis8FRONT_SLIP_DET_THRES_SLIP=Abc_CtrlBus.Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip;
	lis8FRONT_SLIP_DET_THRES_VDIFF=Abc_CtrlBus.Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff;

    lis16EstWheelPressFL = Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
    lis16EstWheelPressFR = Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
    lis16EstWheelPressRL = Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP;
    lis16EstWheelPressRR = Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP;
    
    lis8StrkRcvrCtrlState_Rx = Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
    lis8RecommendStkRcvrLvl_Rx = Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;

    wu1IgnRisingEdge = Abc_CtrlBus.Abc_CtrlIgnOnOffSts;

	if((ldahbs16RBCTPReduction>S16_PCTRL_PRESS_0_BAR) )
	{
	    lcanu1AhbRbcAct = 1;
	}
	else
	{
	    lcanu1AhbRbcAct = 0;
	}

	arrange_v();
}
void ActvBrkCtrlOutpIf(void)
{
	uint8_t vlv_inx;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg			= lespu1TCS_ABS_ACT;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDefectFlg		= fu1ABSErrorDet;

	if(ABS_fz == 1)
	{
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = (FL.ABS == 1) ? 1 : 2;   /*1: inside ABS   2: outside ABS */
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = (FR.ABS == 1) ? 1 : 2;	 /*1: inside ABS   2: outside ABS */
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe = (RL.ABS == 1) ? 1 : 2;	 /*1: inside ABS   2: outside ABS */
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi = (RR.ABS == 1) ? 1 : 2;	 /*1: inside ABS   2: outside ABS */
	}
	else
	{
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = 0;
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = 0;
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe = 0;
		Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi = 0;
	}

	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe		= FL_WP.lcabss16WhlTarPress;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi       = FR_WP.lcabss16WhlTarPress;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPReLe         = RL_WP.lcabss16WhlTarPress;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPReRi         = RR_WP.lcabss16WhlTarPress;
	
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe	= S16_PRESS_RESOL_10_TO_100(FL_WP.lcabss16WhlTarDP);
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi    = S16_PRESS_RESOL_10_TO_100(FR_WP.lcabss16WhlTarDP);
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe      = S16_PRESS_RESOL_10_TO_100(RL_WP.lcabss16WhlTarDP);
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi      = S16_PRESS_RESOL_10_TO_100(RR_WP.lcabss16WhlTarDP);
    
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe	= FL_WP.lcabss16WhlTarPRate;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi   = FR_WP.lcabss16WhlTarPRate;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe     = RL_WP.lcabss16WhlTarPRate;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi     = RR_WP.lcabss16WhlTarPRate;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe	    = FL_WP.lcabsu1CmdPriority;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi       = FR_WP.lcabsu1CmdPriority;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioReLe         = RL_WP.lcabsu1CmdPriority;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioReRi         = RR_WP.lcabsu1CmdPriority;

	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.FrntWhlSlip=lcabsu1FrontWheelSlip;
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = lcabsu1InAbsAllWheelEntFadeOut; /*150323 need to make interface*/
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDesTarP = (int32_t)lcabss16InABSDesiredBoostTarP; /*scale Adjustment: 0.1bar->0.01ar*/
	Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = lcabsu1InABSDesiredBoostTarPFlg; /*150323 need to make interface*/

	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg=EBD_RA;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDefectFlg=fu1EBDErrorDet;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPFrntLe   = Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPFrntRi   = Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPReLe     = RL_WP.lcabss16WhlTarPress;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPReRi     = RR_WP.lcabss16WhlTarPress;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDelTarPReLe     = S16_PRESS_RESOL_10_TO_100(RL_WP.lcabss16WhlTarDP);
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDelTarPReRi     = S16_PRESS_RESOL_10_TO_100(RR_WP.lcabss16WhlTarDP);

	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPRateReLe = RL_WP.lcabss16WhlTarPRate;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPRateReRi = RR_WP.lcabss16WhlTarPRate;

	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdCtrlModeReLe     = RL.lcebdu8CtrlMode;
	Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdCtrlModeReRi     = RR.lcebdu8CtrlMode;


	/*TCS*/
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg=BTCS_ON ;
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDefectFlg=lctcsu1BrkCtrlInhibit;
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe=(latcss16SecTcTargetPress*10)*(int16_t)BTCS_fl;/*scale Adjustment: 0.01bar->0.1ar*/
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi=(latcss16PriTcTargetPress*10)*(int16_t)BTCS_fr;/*scale Adjustment: 0.01bar->0.1ar*/
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPReLe=(latcss16PriTcTargetPress*10)*(int16_t)BTCS_rl;/*scale Adjustment: 0.01bar->0.1ar*/
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPReRi=(latcss16SecTcTargetPress*10)*(int16_t)BTCS_rr;/*scale Adjustment: 0.01bar->0.1ar*/
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg=((FA_TCS.lctcsu1BothDrvgWhlBrkCtlReq==1)||(RA_TCS.lctcsu1BothDrvgWhlBrkCtlReq==1));
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc=latcss16TarPreGrdtMd;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc=latcss16TarPreGrdtMd;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc=latcss16TarPreGrdtMd;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc=latcss16TarPreGrdtMd;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc=latcss16SeccTarPreRate;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc=latcss16PricTarPreRate;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc=latcss16PricTarPreRate;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc=latcss16SeccTarPreRate;

	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe = 0;
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi = 0;
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe = 0;
	Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi = 0;

	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg=(ESP_BRAKE_CONTROL_FL | ESP_BRAKE_CONTROL_FR | ESP_BRAKE_CONTROL_RL | ESP_BRAKE_CONTROL_RR);
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDefectFlg=VDC_DISABLE_FLG;
	
/*	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntLe=laesps16SecTcTargetPress*(int16_t)ESP_BRAKE_CONTROL_FL;
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntRi=laesps16PriTcTargetPress*(int16_t)ESP_BRAKE_CONTROL_FR;
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReLe=laesps16PriTcTargetPress*(int16_t)ESP_BRAKE_CONTROL_RL;
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReRi=laesps16SecTcTargetPress*(int16_t)ESP_BRAKE_CONTROL_RR;*/

    if(ESP_BRAKE_CONTROL_FL==1)	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntLe = (int16_t) FL_ESC.lcesps16IdbTarPress*10;}
    else if((FL.CROSS_SLIP_CONTROL==1) || (FL.U1EscOtherWheelCnt==1))  {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntLe = (int16_t) FL.lcesps16IdbOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntLe = 0;}
    if(ESP_BRAKE_CONTROL_FR==1) {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntRi = (int16_t) FR_ESC.lcesps16IdbTarPress*10;}
    else if((FR.CROSS_SLIP_CONTROL==1) || (FR.U1EscOtherWheelCnt==1))	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntRi = (int16_t) FR.lcesps16IdbOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntRi = 0;}
    if(ESP_BRAKE_CONTROL_RL==1)	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReLe = (int16_t) RL_ESC.lcesps16IdbTarPress*10;}
    else if((RL.CROSS_SLIP_CONTROL==1) || (RL.U1EscOtherWheelCnt==1))  {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReLe = (int16_t) RL.lcesps16IdbOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReLe = 0;}
    if(ESP_BRAKE_CONTROL_RR==1) {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReRi = (int16_t) RR_ESC.lcesps16IdbTarPress*10;}
    else if((RR.CROSS_SLIP_CONTROL==1) || (RR.U1EscOtherWheelCnt==1))	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReRi = (int16_t) RR.lcesps16IdbOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReRi = 0;}


	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe = FL_ESC.lcespu8CtrlMode;    
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi = FR_ESC.lcespu8CtrlMode;    
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeReLe = RL_ESC.lcespu8CtrlMode;      
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeReRi= RR_ESC.lcespu8CtrlMode;      
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe =FL_ESC.lcesps16FinalTarPRate;    
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi=FR_ESC.lcesps16FinalTarPRate;    
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateReLe=RL_ESC.lcesps16FinalTarPRate;      
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateReRi=RR_ESC.lcesps16FinalTarPRate;      
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioFrntLe = FL_ESC.lcespu1DctMuxPrio;        
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioFrntRi = FR_ESC.lcespu1DctMuxPrio;        
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioReLe = RL_ESC.lcespu1DctMuxPrio;          
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioReRi = RR_ESC.lcespu1DctMuxPrio;          
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe = FLAG_ACT_PRE_ACTION_FL; 
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi = FLAG_ACT_PRE_ACTION_FR; 
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe = FLAG_ACT_PRE_ACTION_RL;   
	Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi = FLAG_ACT_PRE_ACTION_RR;

    if(ESP_BRAKE_CONTROL_FL==1)	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe = (int16_t) FL_ESC.lcesps16IdbDelTarPress*10;}
    else if((FL.CROSS_SLIP_CONTROL==1) || (FL.U1EscOtherWheelCnt==1))  {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe = (int16_t) FL.lcesps16IdbDelOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe = 0;}
    if(ESP_BRAKE_CONTROL_FR==1) {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi = (int16_t) FR_ESC.lcesps16IdbDelTarPress*10;}
    else if((FR.CROSS_SLIP_CONTROL==1) || (FR.U1EscOtherWheelCnt==1))	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi = (int16_t) FR.lcesps16IdbDelOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi = 0;}
    if(ESP_BRAKE_CONTROL_RL==1)	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReLe = (int16_t) RL_ESC.lcesps16IdbDelTarPress*10;}
    else if((RL.CROSS_SLIP_CONTROL==1) || (RL.U1EscOtherWheelCnt==1))  {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReLe = (int16_t) RL.lcesps16IdbDelOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReLe = 0;}
    if(ESP_BRAKE_CONTROL_RR==1) {Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReRi = (int16_t) RR_ESC.lcesps16IdbDelTarPress*10;}
    else if((RR.CROSS_SLIP_CONTROL==1) || (RR.U1EscOtherWheelCnt==1))	{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReRi = (int16_t) RR.lcesps16IdbDelOtherPress*10;}  /*scale Adjustment: 0.01bar->0.1ar*/
    else					{Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReRi = 0;}



	Abc_CtrlBus.Abc_CtrlActvBrkCtrlrActFlg=FLAG_ACTIVE_BRAKING;

    Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime = lcesps16AllowFnlStrRetime;

	Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime = lis16StrkRcvrAbsAllowedTime_Tx;
	Abc_CtrlBus.Abc_CtrlVehSpd=vref;
	Abc_CtrlBus.Abc_CtrlAy=alat;

#if __FBC == ENABLE
	Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaActFlg=(PBA_ON | PBA_EXIT_CONTROL | FBC_ON);
#else
	Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaActFlg=(PBA_ON | PBA_EXIT_CONTROL);
#endif
	Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaCDefectFlg=labau1InhibitFlg;
	Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaTarP=labas16TcTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/

#if __AVH==ENABLE
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhActFlg=(lcu1AvhActFlg | lcu1AvhAutonomousActFlg);
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhDefectFlg=lcu1AvhInhibitFlg;
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhTarP=laavhs16TcTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhActFlg=0;
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhTarP=0;
#endif

#if __EBP==ENABLE
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpActFlg=EBP_ON;
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpTarP=lcebps16CtrlTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpActFlg=0;
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpTarP=0;
#endif

#if __EPB_INTERFACE ==ENABLE
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiActFlg=lcu1EpbActiveFlg;
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg=lcu1EpbInhibitFlg;
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiTarP=laepbs16TcTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiActFlg=0;
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiTarP=0;
#endif

#if __HDC==ENABLE
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcActFlg=fu1HDCErrorDet;
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcDefectFlg=lcu1HdcActiveFlg;
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcTarP=lahdcs16TcTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcActFlg=0;
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcTarP=0;
#endif

#if __HSA ==ENABLE
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaActFlg=HSA_flg;
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaDefectFlg=HSA_INHIBITION_flag;
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaTarP=lchsas16CtrlTargetPress*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaActFlg=0;
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaTarP=0;
#endif

#if __SCC==ENABLE
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccActFlg=lcu8WpcCtrlReq;
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccDefectFlg=lcu1SccInhibitFlg;
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccTarP=lcs16SccTargetPres*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccActFlg=0;
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccTarP=0;
#endif

#if __TVBB==ENABLE
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbActFlg=lctvbbu1TVBB_ON;
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg=ldtvbbu1InhibitFlag;
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbTarP=lis16TvbbPriTP*10;/*scale Adjustment: 0.01bar->0.1ar*/
	#else
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbActFlg=0;
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg=0;
	Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbTarP=0;
#endif


	Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvTCS   =lesps16TCS_TQI_TCS;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvMsr   =lesps16TCS_TQI_MSR;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvSlowTCS =lesps16TCS_TQI_SLOW_TCS;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.MinGear =    lespu8ESP_GMIN_ESP;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.MaxGear     =lespu8ESP_GMAX_ESP;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsReq     =lespu1TCS_TCS_REQ;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsCtrl    =lespu1TCS_TCS_CTL;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.AbsAct    =lespu1TCS_ABS_ACT;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsGearShiftChr     =lespu1TCS_TCS_GSC;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.EspCtrl    =lespu1TCS_ESP_CTL;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.MsrReq   =lespu1TCS_MSR_C_REQ;
	Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsProductInfo    =lespu1TCS_TCS_MFRN;


//    Abc_CtrlBus.Abc_CtrlCanTxDataInfo.u1EBS_CF_Brk_EHBStat     =lrbcu1EBS_CF_Brk_EHBStat;
//    Abc_CtrlBus.Abc_CtrlCanTxDataInfo.u16EBS_CR_Brk_HHTCmd_Nm  =lrbcu16EBS_CR_Brk_HHTCmd_Nm;
//    Abc_CtrlBus.Abc_CtrlCanTxDataInfo.u16EBS_CR_Brk_EstTot_Nm  =lrbcu16EBS_CR_Brk_EstTot_Nm;
#if WHL_VLV_TEST==ENABLE
    if(WhlVlvTestCompleted==0)
    {
    	if(WhlInletVlvOnReqCntForTest<100)
    	{
    		WhlInletVlvOnReqCntForTest++;
            for(vlv_inx=0;vlv_inx<10;vlv_inx++)
            {
            	la_FLNO.lau16PwmDuty[vlv_inx]=1000;
        		la_FRNO.lau16PwmDuty[vlv_inx]=1000;
        		la_RLNO.lau16PwmDuty[vlv_inx]=1000;
        		la_RRNO.lau16PwmDuty[vlv_inx]=1000;
        		la_FLNC.lau16PwmDuty[vlv_inx]=0;
        		la_FRNC.lau16PwmDuty[vlv_inx]=0;
        		la_RLNC.lau16PwmDuty[vlv_inx]=0;
        		la_RRNC.lau16PwmDuty[vlv_inx]=0;
            }
        	VALVE_ACT=1;
    	}
    	else if(WhlOutletVlvOnReqCntForTest<100)
    	{
    		WhlOutletVlvOnReqCntForTest++;
            for(vlv_inx=0;vlv_inx<10;vlv_inx++)
            {
            	la_FLNO.lau16PwmDuty[vlv_inx]=0;
        		la_FRNO.lau16PwmDuty[vlv_inx]=0;
        		la_RLNO.lau16PwmDuty[vlv_inx]=0;
        		la_RRNO.lau16PwmDuty[vlv_inx]=0;
        		la_FLNC.lau16PwmDuty[vlv_inx]=1000;
        		la_FRNC.lau16PwmDuty[vlv_inx]=1000;
        		la_RLNC.lau16PwmDuty[vlv_inx]=1000;
        		la_RRNC.lau16PwmDuty[vlv_inx]=1000;
            }
        	VALVE_ACT=1;
    	}
    	else if(WhlVlvOnDlyReqCntForTest<500)
    	{
    		WhlVlvOnDlyReqCntForTest++;
            for(vlv_inx=0;vlv_inx<10;vlv_inx++)
            {
            	la_FLNO.lau16PwmDuty[vlv_inx]=0;
        		la_FRNO.lau16PwmDuty[vlv_inx]=0;
        		la_RLNO.lau16PwmDuty[vlv_inx]=0;
        		la_RRNO.lau16PwmDuty[vlv_inx]=0;
        		la_FLNC.lau16PwmDuty[vlv_inx]=0;
        		la_FRNC.lau16PwmDuty[vlv_inx]=0;
        		la_RLNC.lau16PwmDuty[vlv_inx]=0;
        		la_RRNC.lau16PwmDuty[vlv_inx]=0;
            }
        	VALVE_ACT=1;
    	}
    	else
    	{
    		WhlInletVlvOnReqCntForTest=0;
    		WhlOutletVlvOnReqCntForTest=0;
    		WhlVlvOnDlyReqCntForTest=0;
    		WhlVlvOnReqCntForTest++;
    		if(WhlVlvOnReqCntForTest>5)
    		{
    			WhlVlvTestCompleted=1;
    			WhlVlvOnReqCntForTest=0;
    		}
            for(vlv_inx=0;vlv_inx<10;vlv_inx++)
            {
            	la_FLNO.lau16PwmDuty[vlv_inx]=0;
        		la_FRNO.lau16PwmDuty[vlv_inx]=0;
        		la_RLNO.lau16PwmDuty[vlv_inx]=0;
        		la_RRNO.lau16PwmDuty[vlv_inx]=0;
        		la_FLNC.lau16PwmDuty[vlv_inx]=0;
        		la_FRNC.lau16PwmDuty[vlv_inx]=0;
        		la_RLNC.lau16PwmDuty[vlv_inx]=0;
        		la_RRNC.lau16PwmDuty[vlv_inx]=0;
            }
        	VALVE_ACT=0;
    	}
    }
    else
    {
        for(vlv_inx=0;vlv_inx<10;vlv_inx++)
        {
        	la_FLNO.lau16PwmDuty[vlv_inx]=0;
    		la_FRNO.lau16PwmDuty[vlv_inx]=0;
    		la_RLNO.lau16PwmDuty[vlv_inx]=0;
    		la_RRNO.lau16PwmDuty[vlv_inx]=0;
    		la_FLNC.lau16PwmDuty[vlv_inx]=0;
    		la_FRNC.lau16PwmDuty[vlv_inx]=0;
    		la_RLNC.lau16PwmDuty[vlv_inx]=0;
    		la_RRNC.lau16PwmDuty[vlv_inx]=0;
        }
    	VALVE_ACT=0;
    }
#endif
    for(vlv_inx=0;vlv_inx<10;vlv_inx++)
    {
    	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[vlv_inx]=la_FLNO.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[vlv_inx]=la_FRNO.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[vlv_inx]=la_RLNO.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[vlv_inx]=la_RRNO.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[vlv_inx]=la_FLNC.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[vlv_inx]=la_FRNC.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[vlv_inx]=la_RLNC.lau16PwmDuty[vlv_inx];
		Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[vlv_inx]=la_RRNC.lau16PwmDuty[vlv_inx];
    }

	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvReq = 0;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvReq = 0;


//	Abc_CtrlBus.Abc_CtrlWhlVlvDrvReqInfo.WhlInletOutletVlvDrvReqFlg=VALVE_ACT;
	Abc_CtrlBus.Abc_CtrlFunctionLamp=FUNCTION_LAMP_ON;

	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvDataLen = 10;
	Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvDataLen = 10;
    //write NVM Data
    LSTCS_vWriteEEPROMDiskTemp();
    LSESP_vWriteEEPROMSensorOffset();
}

static void arrange_v(void)
{
    int16_t temp_spd[4];

    temp_spd[0]=(int16_t)FL.lsabss16WhlSpdRawSignal64;//Abc_CtrlBus.Abc_CtrlWhlSpdRawInfo.WhlSpdRawFrntLe;
    temp_spd[1]=(int16_t)FR.lsabss16WhlSpdRawSignal64;//Abc_CtrlBus.Abc_CtrlWhlSpdRawInfo.WhlSpdRawFrntRi;
    temp_spd[2]=(int16_t)RL.lsabss16WhlSpdRawSignal64;//Abc_CtrlBus.Abc_CtrlWhlSpdRawInfo.WhlSpdRawReLe;
    temp_spd[3]=(int16_t)RR.lsabss16WhlSpdRawSignal64;//Abc_CtrlBus.Abc_CtrlWhlSpdRawInfo.WhlSpdRawReRi;

    sorting_spd(&temp_spd[0]);

    min_speed=temp_spd[0];
    max_3rd_speed=temp_spd[1];
    max_2nd_speed=temp_spd[2];
    max_speed=temp_spd[3];
}

static void sorting_spd(UINT *spd_ptr)
{
    UINT m=0;
    UCHAR i=0,j=0,k=0;

    for(i=0;i<3;i++)
    {
        k=i;
        m=spd_ptr[i];
        for(j=i+1;j<4;j++){
            if(m>spd_ptr[j])
            {
                m=spd_ptr[j];
                k=j;
            }
            else
            {
                ;
            }
        }
        spd_ptr[k]=spd_ptr[i];
        spd_ptr[i]=m;
    }
}
