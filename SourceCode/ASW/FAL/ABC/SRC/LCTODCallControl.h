#ifndef __LCTODCALLCONTROL_H__
#define __LCTODCALLCONTROL_H__

#include "LVarHead.h"
  #if __TOD
extern uint8_t	TOD4wdOpen;
extern uint8_t	TOD4wdLimReq;
extern uint8_t	TOD4wdLimMode;
extern uint8_t	TOD4wdCluLim;
extern uint8_t  lstodu8ENG_VAR_OLD;  
extern uint8_t  lstodu8ENG_VAR_CHECK;
extern uint8_t  lstodu8ENG_VAR_COUNT;
extern uint8_t	TOD_ON;
extern uint8_t	TODModeSelector;
extern uint8_t	TODYawSelector;
extern uint8_t	TODDisable;
extern uint8_t	TOD4WDLimModeSelect;
extern uint8_t	TOD4WDLimModeMap[5][5];

extern int16_t	TOD4wdThrOversteer;
extern int16_t	TOD4wdThrUndersteer;

extern uint16_t	TOD4wdTqcLimTorq;
extern uint16_t	TOD4wdCluLimTemp;
extern uint16_t	TOD4wdCluLimTempAlt;	
extern uint16_t	TOD4WDTorqRatio[5][5];
extern uint16_t	TOD4WDLimModeMaxValue;
extern uint16_t	TOD4WDLimModeMinValue;
  #endif /*__TOD*/
						
#endif
