/*******************************************************************/
/*  Program ID: MGH-40 Road Gradient Estimation                    */
/*  Program description: Road Gradient Estimation                  */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes: Will be applicated to KMC HM(SOP.07.08)         */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author           Description                    */
/*  -------       ----------    -----------------------------      */
/*  06.09.11      SeWoong Kim   Initial Release                    */
/*******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSDctRoadGradient
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************/

#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LDABSDctRoadGradient.h"
#include "LSABSCallSensorSignal.H"
#include "LCallMain.h"
#include "LCESPCalLpf.h"
#include "LDABSCallDctForVref.H"

#if (__Ax_OFFSET_MON==1) && (__Ax_OFFSET_MON==0)
/* Local Definition  ***********************************************/
#define G_GRADE_UNUSED_LOCAL_FLG_7				G_GRADE_1.bit7
#define G_GRADE_UNUSED_LOCAL_FLG_6				G_GRADE_1.bit6
#define G_GRADE_UNUSED_LOCAL_FLG_5				G_GRADE_1.bit5
#define G_GRADE_UNUSED_LOCAL_FLG_4   			G_GRADE_1.bit4
#define G_GRADE_UNUSED_LOCAL_FLG_3				G_GRADE_1.bit3
#define G_GRADE_UNUSED_LOCAL_FLG_2				G_GRADE_1.bit2
#define G_GRADE_UNUSED_LOCAL_FLG_1				G_GRADE_1.bit1
#define G_GRADE_UNUSED_LOCAL_FLG_0				G_GRADE_1.bit0

/* Variables Definition*********************************************/
struct	U8_BIT_STRUCT_t G_GRADE_0;
struct	U8_BIT_STRUCT_t G_GRADE_1;

  /* Global For Other's Use */
 


uint8_t  HILL_RESET_CNT;	 
uint8_t  UP_CNT;
uint8_t  DN_CNT;	 

//int16_t    filt_ein_new;
//int16_t    filter_out_new;
//int16_t    filter_out_new_rest;
//uint16_t   voptfz_mittel_new;

uint8_t  Pitch_CNT;
int16_t    Agrade;
int16_t    AgradF;
//int16_t    WheelAccelG_1;	 
int16_t    PitchG;
  
  /* Global For Logging */
  

  /* Local */
  

 
/* Local Function prototype ****************************************/


/* GlobalFunction prototype ****************************************/


/* Implementation **************************************************/


void    LDABS_vDctRoadGradient(void)
{

	uint8_t   HILL_SET_TIME;
	uint8_t   HILL_RESET_TIME;
	uint8_t   HILL_SET_G;
	uint8_t   HILL_RESET_G;
		
    HILL_SET_TIME=L_U8_TIME_5MSLOOP_400MS;
    HILL_RESET_TIME=L_U8_TIME_5MSLOOP_150MS;
    HILL_SET_G=AxOffset_0_08G;
    HILL_RESET_G=AxOffset_0_03G;
    
    if(speed_calc_timer < (uint8_t)T_1000_MS) 
    {         
		        
	        HILL_RESET_CNT=0;	 	    	
	    	
	    	UP_HILL_flag=0;
	    	DN_HILL_flag=0;
	    	HILL_RESET_flag=0;		        
	    	
	    	Agrade=0;	
	    	AgradF=0;	 
            Agrade_On_flag=0;
	    	PitchG=0;	
	    		    	
    }
    else
    {
        /*----------  Road Gradient Estimation------------------------ */
        /* Exclude Pitching Effect .. 1sec after braking */
        if(BRAKE_SIGNAL==1)
        {
          Brake_On_flag=1;	
          Brake_Pitching_flag=0;
          Pitch_CNT=0;
        }
        else
        {
        	if(Brake_On_flag==1)
        	{
        		Brake_Pitching_flag=1;
        	}
        	else
        	{
        		;
        	}        
        	
        	if(Brake_Pitching_flag==1)
        	{
        		if(Pitch_CNT < L_U8_TIME_5MSLOOP_1000MS)
        		{
        			Brake_Pitching_flag=1;
               		Pitch_CNT=Pitch_CNT+1;        			
        		}
        		else
        		{
        			Brake_Pitching_flag=0;
               		Pitch_CNT=0;        			
        		}        		
        	}
        	else
        	{
        		Pitch_CNT=0;
        	}
        		        	        		        	
        	Brake_On_flag=0;        	
        }
                   
//        tempW0=51;
//        tempW1=20;
//        tempW2=(filter_out_new );
//        s16muls16divs16();
//        WheelAccelG_1=tempW3;  // Resolution 100
        


        if( (BRAKE_SIGNAL==1) || (Brake_Pitching_flag==1) )  
        {
          if(abs(AxOffsetCalc1) <= AxOffset_0_05G )
          {
//          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 - along_by_drift*10 )*5 , AgradF ,5 ); //1Hz Resol 2000; 	
          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 )*5 , AgradF ,L_U8FILTER_GAIN_5MSLOOP_1HZ ); //1Hz Resol 2000; 	
          }
          else
          {
//          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 - along_by_drift*10 - AxOffsetCalc1)*5 , AgradF ,1 ); //1 Hz Resol 2000; 	
          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 - AxOffsetCalc1)*5 , AgradF ,1 ); //1 Hz Resol 2000; 	
          }

          tempW0=5;
          tempW1=1;
          tempW2=AgradF;
          s16muls16divs16(); 
          Agrade=tempW3;       
          
          if(abs(AxOffsetCalc1) <= AxOffset_0_05G )
          {
//          	PitchG=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 + along_by_drift*10 -Agrade ) , PitchG ,17 ); //3Hz Resol 1000; 	
          	PitchG=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10  -Agrade ) , PitchG ,L_U8FILTER_GAIN_5MSLOOP_3HZ ); //3Hz Resol 1000; 	          	
          }
          else
          {
//          	PitchG=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 + along_by_drift*10 - AxOffsetCalc1 -Agrade) , PitchG ,17 ); //3 Hz Resol 1000; 	
          	PitchG=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 - AxOffsetCalc1 -Agrade) , PitchG ,L_U8FILTER_GAIN_5MSLOOP_3HZ ); //3 Hz Resol 1000; 	
          }
                      
          Agrade_On_flag=0;
        }
        else
        {
          if(abs(AxOffsetCalc1) <= AxOffset_0_05G )
          {
//          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 + along_by_drift*10 )*5 , AgradF ,5 ); //1Hz Resol 2000; 	
         	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 )*5 , AgradF ,L_U8FILTER_GAIN_5MSLOOP_1HZ ); //1Hz Resol 2000; 	
          }
          else
          {
//          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 + along_by_drift*10 - AxOffsetCalc1)*5 , AgradF ,5 ); //1 Hz Resol 2000; 	
          	AgradF=LCESP_s16Lpf1Int((ax_Filter*10 - WheelAccelG_1*10 - AxOffsetCalc1)*5 , AgradF ,L_U8FILTER_GAIN_5MSLOOP_1HZ ); //1 Hz Resol 2000;
          }

          tempW0=5;
          tempW1=1;
          tempW2=AgradF;
          s16muls16divs16(); 
          Agrade=tempW3;                 	
          PitchG=0;
        	        	
          Agrade_On_flag=1;
        }

        if((AxOffsetOK1==1) ) 
	    {    	
	    	
	        if(UP_HILL_flag==1)
	        {
	    	  /*------UP HILL Reset Condition ----*/ 	
	    	  if(Agrade < HILL_RESET_G )
	    	  {
	    	    if(HILL_RESET_CNT>HILL_RESET_TIME)
	    	    {
	    	   	  UP_HILL_flag=0;
	    	   	  HILL_RESET_CNT=0;	    	   	  
	    	    }
	    	    else
	    	    {
	    	      HILL_RESET_CNT=HILL_RESET_CNT+1;
	    	    }	    	      		    	      	
	    	  }
	    	  else
	    	  {
	    	    HILL_RESET_CNT=0;
	    	  }	
	    	}    	      	        	
	        else if(DN_HILL_flag==1)
	        {
	    	  /*------DOWN HILL Reset Condition ----*/ 	
	    	  if(Agrade > -HILL_RESET_G )
	    	  {
	    	    if(HILL_RESET_CNT>=HILL_RESET_TIME)
	    	    {
	    	   	  DN_HILL_flag=0;
	    	   	  HILL_RESET_CNT=0;	    	   	  
	    	    }
	    	    else
	    	    {
	    	      HILL_RESET_CNT=HILL_RESET_CNT+1;
	    	    }	    	      		    	      	
	    	  }
	    	  else
	    	  {
	    	    HILL_RESET_CNT=0;
	    	  }		        		        	
	        }
	        else
	        {
	        /*--------HILL SET Condition ---------*/	
  	    	  if(Agrade > HILL_SET_G )	        
  	    	  {
  	    	    /*----UP HILL SET Condition ---*/
  	    	    if(UP_CNT > HILL_SET_TIME )
  	    	    {
  	    	    	UP_HILL_flag=1;
  	    	    	UP_CNT=0;
  	    	    }
  	    	    else
  	    	    {
  	    	    	UP_CNT=UP_CNT+1;
  	    	    }  	    	    	
  	    	  }
  	    	  else if(Agrade < -HILL_SET_G )
  	    	  {
  	    	    /*----DOWN HILL SET Condition ---*/  	   
  	    	    if(DN_CNT > HILL_SET_TIME )
  	    	    {
  	    	    	DN_HILL_flag=1;
  	    	    	DN_CNT=0;
  	    	    }
  	    	    else
  	    	    {
  	    	    	DN_CNT=DN_CNT+1;
  	    	    }  	    	    	  	    	     	    
  	    	  	
  	    	  }
  	    	  else
  	    	  {
  	    	  	UP_CNT=0;
  	    	  	DN_CNT=0;  	    	  	
  	    	  }
	          HILL_RESET_CNT=0;		        	
	        }			        		        

	    }
	    else
	    {

           HILL_RESET_CNT=0;	 	    	 	
 	       UP_HILL_flag=0;
 	       DN_HILL_flag=0;
		    				    	
	    }	        
                                          
         
	}	
	
}	
	
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSDctRoadGradient
	#include "Mdyn_autosar.h"
#endif    

