/*******************************************************************************
* Project Name:     MGH80_ESC
* File Name:        LSAVHCallLogData.c
* Description:      Logging Data of AVH
* Logic version:    2011, 10, 08 version 0
********************************************************************************

/* Includes ********************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSAVHCallLogData
	#include "Mdyn_autosar.h"
#endif


#include "LSAVHCallLogData.h"
  #if __AVH
#include "LSCallLogData.h"
#include "LCHSACallControl.h"
#include "LCAVHCallControl.h"
#include "LCDECCallControl.h"
#include "LCEPBCallControl.h"
#include "LADECCallActHW.h"
#include "LAAVHCallActHW.h"
#include "LADECCallActCan.h"
#include "Hardware_Control.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LSABSCallSensorSignal.h"
#include "LSESPFilterEspSensor.h"

   #if __TCMF_ENABLE
    #if !SIM_MATLAB
//#include "../System/WContlADC.h"
///*#include "../System/WChkVariantCode.h"*/
//#include "../Failsafe/FWSpeedCalc.h"
//#include "../Failsafe/FSSwitchCheck.h"
//#include "../Failsafe/FVSolPsvCheck.h"
//#include "../System/WContlSnr.h"
    #endif
#include "algo_var.h"
   #endif
  #endif

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LSAVH_vCallAVHLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of AVH Control
********************************************************************************/
  #if defined(__LOGGER) && (__LOGGER_DATA_TYPE==1)
void LSAVH_vCallAVHLogData(void)
{
    uint8_t i;

      #if __AVH
    /***********************************************************************/
    CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (uint8_t)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (uint8_t)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (uint8_t)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (uint8_t)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (uint8_t)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (uint8_t)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (uint8_t)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_rr>>8);

                                                   
    CAN_LOG_DATA[10] = (uint8_t)(lcu8AvhEpbState);                    /* 06 */
    CAN_LOG_DATA[11] = (uint8_t)(lcs8AvhControlState);                /* 07 */

    CAN_LOG_DATA[12] = (uint8_t)((int16_t)target_vol/1000);           /* 08 */
    CAN_LOG_DATA[13] = (uint8_t)((int16_t)fu16CalVoltMOTOR/1000);     /* 09 */

    CAN_LOG_DATA[14] = (uint8_t)(vref);                               /* 10 */
    CAN_LOG_DATA[15] = (uint8_t)(vref>>8);
    
    CAN_LOG_DATA[16] = (uint8_t)(lcu8AvhDownDrvLevel);                /* 11 */
    CAN_LOG_DATA[17] = (uint8_t)(fsu8NumberOfEdge_fl);                /* 12 */
    CAN_LOG_DATA[18] = (uint8_t)(lcu8AvhAlarmCase);                /* 13 */
    
    if((lcu8AvhHoodOpen==1)||(lcu8AvhTrunkOpen==1)) { tempW0=1; }
    else                                            { tempW0=0; }
    CAN_LOG_DATA[19] = (uint8_t)(tempW0);                             /* 14 */
    CAN_LOG_DATA[20] = (uint8_t)(MFC_PWM_DUTY_P);                     /* 15 */
    CAN_LOG_DATA[21] = (uint8_t)(lcu8AvhDrvDoorOpen);                 /* 16 */
    CAN_LOG_DATA[22] = (uint8_t)(lcu8AvhDrvSeatUnBelted);             /* 17 */
    CAN_LOG_DATA[23] = (uint8_t)(MFC_PWM_DUTY_S);                     /* 18 */
    CAN_LOG_DATA[24] = (uint8_t)(lcs16AvhTargetPress);                /* 19 */
    CAN_LOG_DATA[25] = (uint8_t)(lcs16AvhTargetPress>>8);
#if ((__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE))
    CAN_LOG_DATA[26] = (uint8_t)(laepbs16TcTargetPress);                /* 20 */
    CAN_LOG_DATA[27] = (uint8_t)(laepbs16TcTargetPress>>8);
#else
    CAN_LOG_DATA[26] = (uint8_t)(lcu16AvhTcCurTemper);                /* 20 */
    CAN_LOG_DATA[27] = (uint8_t)(lcu16AvhTcCurTemper>>8);
#endif
    CAN_LOG_DATA[28] = (uint8_t)(lcu16AvhAutoActTotal);               /* 21 */
    CAN_LOG_DATA[29] = (uint8_t)(lcu16AvhAutoActTotal>>8);  
    CAN_LOG_DATA[30] = (uint8_t)(lcs16AvhAxG);                        /* 22 */
    CAN_LOG_DATA[31] = (uint8_t)(lcs16AvhAxG>>8);
    CAN_LOG_DATA[32] = (uint8_t)(lcs16AvhIdleTorq);                   /* 23 */
    CAN_LOG_DATA[33] = (uint8_t)(lcs16AvhIdleTorq>>8);
    CAN_LOG_DATA[34] = (uint8_t)(lcs16AvhFilteredTorq);               /* 24 */
    CAN_LOG_DATA[35] = (uint8_t)(lcs16AvhFilteredTorq>>8);
    CAN_LOG_DATA[36] = (uint8_t)(lcu8AvhPrateLevel);                  /* 25 */
    CAN_LOG_DATA[37] = (uint8_t)(lcanu8AVH_ALARM);                    /* 26 */

    CAN_LOG_DATA[38] = (uint8_t)(gs_sel);                             /* 27 */
    CAN_LOG_DATA[39] = (uint8_t)mtp;                                  /* 28 */

    CAN_LOG_DATA[40] = (uint8_t)(lcs16AvhEnterPress);                 /* 29 */
    CAN_LOG_DATA[41] = (uint8_t)(lcs16AvhEnterPress>>8);
    CAN_LOG_DATA[42] = (uint8_t)(lcs16AvhHoldCntFor1IGN);             /* 30 */
    CAN_LOG_DATA[43] = (uint8_t)(lcs16AvhHoldCntFor1IGN>>8);
    CAN_LOG_DATA[44] = (uint8_t)(lcs16AvhHoldCnt);                    /* 31 */
    CAN_LOG_DATA[45] = (uint8_t)(lcs16AvhHoldCnt>>8);
    CAN_LOG_DATA[46] = (uint8_t)(TC_MFC_Voltage_Primary_dash);                  /* 32 */
    CAN_LOG_DATA[47] = (uint8_t)(TC_MFC_Voltage_Primary_dash>>8);
    CAN_LOG_DATA[48] = (uint8_t)(lcs16AvhMoveDistance_fl);            /* 33 */
    CAN_LOG_DATA[49] = (uint8_t)(lcs16AvhMoveDistance_fl>>8);
    CAN_LOG_DATA[50] = (uint8_t)(lcs16AvhCompleteStopCnt_fl);         /* 34 */
    CAN_LOG_DATA[51] = (uint8_t)(lcs16AvhCompleteStopCnt_fl>>8);
    CAN_LOG_DATA[52] = (uint8_t)(lcs16AvhMoveDistance_rl);            /* 35 */
    CAN_LOG_DATA[53] = (uint8_t)(lcs16AvhMoveDistance_rl>>8);
    CAN_LOG_DATA[54] = (uint8_t)(mtp_resol1000);         /* 36 */
    CAN_LOG_DATA[55] = (uint8_t)(mtp_resol1000>>8);

    CAN_LOG_DATA[56] = (uint8_t)(lcu8AvhGearPosition);                /* 37 */
    CAN_LOG_DATA[57] = (uint8_t)(lcanu8AVH_CLU);                      /* 38 */

    CAN_LOG_DATA[58] = (uint8_t)(eng_rpm);            /* 39 */
    CAN_LOG_DATA[59] = (uint8_t)(eng_rpm>>8);
  #if ((__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE))
    CAN_LOG_DATA[60] = (uint8_t)(pos_fl_1_100bar);                    /* 40 */
    CAN_LOG_DATA[61] = (uint8_t)(pos_fl_1_100bar>>8);
    CAN_LOG_DATA[62] = (uint8_t)(pos_fr_1_100bar);                    /* 41 */
    CAN_LOG_DATA[63] = (uint8_t)(pos_fr_1_100bar>>8);
  #else
    CAN_LOG_DATA[60] = (uint8_t)(lcs16AvhEngIdleCnt);                    /* 40 */
    CAN_LOG_DATA[61] = (uint8_t)(lcs16AvhEngIdleCnt>>8);
    CAN_LOG_DATA[62] = (uint8_t)(lcs16AvhAbnormalHoldCnt);                    /* 41 */
    CAN_LOG_DATA[63] = (uint8_t)(lcs16AvhAbnormalHoldCnt>>8);
  #endif
  #if ((__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE))
    CAN_LOG_DATA[60] = (uint8_t)(lsesps16EstBrkPressByBrkPedalF);                    /* 40 */
    CAN_LOG_DATA[61] = (uint8_t)(lsesps16EstBrkPressByBrkPedalF>>8); 
    CAN_LOG_DATA[62] = (uint8_t)(lsesps16MpressByAHBgen3);                    /* 41 */
    CAN_LOG_DATA[63] = (uint8_t)(lsesps16MpressByAHBgen3>>8);
  #endif
    CAN_LOG_DATA[64] = (uint8_t)(lcs16AvhMpress);                             /* 42 */
    CAN_LOG_DATA[65] = (uint8_t)(lcs16AvhMpress>>8);

    CAN_LOG_DATA[66] = (uint8_t)(eng_torq_rel);                       /* 43 */
    CAN_LOG_DATA[67] = (uint8_t)(eng_torq_rel>>8);
    CAN_LOG_DATA[68] = (uint8_t)(lcs16DecDecel);                      /* 44 */
    CAN_LOG_DATA[69] = (uint8_t)(lcs16DecDecel>>8);

    CAN_LOG_DATA[70] = (uint8_t)(lcu8epbDynamicReq);                    /* 45 */
    CAN_LOG_DATA[71] = (uint8_t)(AVH_debuger2);                       /* 46 */
    CAN_LOG_DATA[72] = (uint8_t)(AVH_debuger);                        /* 47 */
    CAN_LOG_DATA[73] = (uint8_t)(lcanu8REQ_EPB_ACT);                  /* 48 */

    /**********************************49*************************************/
    i=0;
    if(lcu1AvhInhibitFlg==1)         i|=0x01;           /* 01 */
    if(lcu1AvhSwitchOn==1)           i|=0x02;           /* 02 */
    if(lcu1AvhActReady==1)           i|=0x04;           /* 03 */
    if(lcu1AvhActFlg==1)             i|=0x08;           /* 04 */
    if(lcu1AvhActReq==1)       		 i|=0x10;           /* 05 */
    if(lcu1AvhAutonomousActFlg==1)   i|=0x20;           /* 06 */ 
    if(lcu1AvhDriverInVehicle==1)    i|=0x40;           /* 07 */     
    if(lcu1AvhAutoTM==1)             i|=0x80;           /* 08 */

    CAN_LOG_DATA[74] = (uint8_t)(i);     
    /**********************************50*************************************/
    i=0;
	if(lcu1AvhLampOnReq==1)          i|=0x01;           /* 09 */
    if(lcu1AvhILamp==1)              i|=0x02;           /* 10 */
    if(lcu1AvhSWActReady==1)         i|=0x04;           /* 11 */
    if(lcu1AvhOverActTime==1)        i|=0x08;           /* 12 */
  	if(lcu1AvhTcOverTemper==1)       i|=0x10;           /* 13 */
   	if(lcu1AvhRiseRoll==1)           i|=0x20;           /* 14 */   	
   	if(lcu1AvhRiseInit==1)           i|=0x40;           /* 15 */
    if(lcu1AvhRiseModeEnd==1)        i|=0x80;           /* 16 */
       
    CAN_LOG_DATA[75] = (uint8_t)(i);                                
    /**********************************51*************************************/    
    i=0;
    if(lcu1AvhComCndSatisfy==1)      i|=0x01;           /* 17 */
	if(lcu1AvhVehicleRoll2==1)       i|=0x02;           /* 18 */
    if(lcu1AvhDownDrive==1)          i|=0x04;           /* 19 */
    if(lcu1AvhVehicleRoll==1)        i|=0x08;           /* 20 */
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_SECONDARY==1)      i|=0x10;           /* 21 */
      #else
    if(TCL_DEMAND_fl==1)             i|=0x10;           /* 21 */
      #endif
    if(lcu1AvhUphill==1)    		 i|=0x20;           /* 22 */
    if(lcu1AvhDownhill==1)           i|=0x40;           /* 23 */
    if(lcu1AvhEpbFailFlg==1)         i|=0x80;           /* 24 */
       
    CAN_LOG_DATA[76] = (uint8_t)(i);                                   
    /**********************************52*************************************/    
    i=0;
    if(lcu1AvhAccelExitOK==1)        i|=0x01;           /* 25 */
    if(lcu1AvhSlowExitSatisfy==1)    i|=0x02;           /* 26 */
    if(lcu1AvhFastExitSatisfy==1)    i|=0x04;           /* 27 */
      #if __HSA
    if(HSA_flg==1)                 	 i|=0x08;           /* 28 */
      #endif
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)        i|=0x10;           /* 29 */
      #else
    if(TCL_DEMAND_fr==1)             i|=0x10;           /* 29 */
      #endif
    if(lcu1AvhForcedSwOffEpbAct==1)       i|=0x20;           /* 30 */
    if(lcu1AvhEpbActReq==1)          i|=0x40;          	/* 31 */
    if(lcu1AvhReqEpbActInvalid==1)   i|=0x80;         	/* 32 */
       
    CAN_LOG_DATA[77] = (uint8_t)(i);                                  
    /***********************************53************************************/    
    i=0;
      #if (__SPLIT_TYPE==1)
    if(S_VALVE_SECONDARY==1)         i|=0x01;           /* 33 */
    if(S_VALVE_PRIMARY==1)           i|=0x02;           /* 34 */
      #else
    if(S_VALVE_LEFT==1)              i|=0x01;           /* 33 */
    if(S_VALVE_RIGHT==1)             i|=0x02;           /* 34 */
      #endif      
    if(lcu1EpbActiveFlg==1)          i|=0x04;           /* 35 */
    if(lcu1AvhMaxHill==1)            i|=0x08;           /* 36 */
    if(lcu1EpbiBrkLampOnReq==1)      i|=0x10;           /* 37 */
    if(lcu1AvhEpbActOkForSwOffOld==1)          i|=0x20;           /* 38 */
    if(lcu1AvhInhibitSwOn==1)            i|=0x40;           /* 39 */
    if(lcu1AvhInhibitSwOff==1)       i|=0x80;           /* 40 */
       
    CAN_LOG_DATA[78] = (uint8_t)(i);                                     
    /***********************************54************************************/    
    i=0;
    if(lcu1AvhSwitchAct==1)    i|=0x01;           /* 41 */
    if(fu1BLSErrDet==1)                i|=0x02;           /* 42 */
    if(lcu1AvhMpressOn==1)              i|=0x04;           /* 43 */
    if(fu1AvhSwitchAct==1)           i|=0x08;           /* 44 */
    if(BLS==1)                       i|=0x10;           /* 45 */
    if(lcu1AvhForcedSwOff==1)                            i|=0x20;           /* 46 */
    if(lcu1AvhEpbActOkForSwOff==1)                            i|=0x40;           /* 47 */
    if(wu1IgnRisingEdge==1)           i|=0x80;           /* 48 */
    CAN_LOG_DATA[79] = (uint8_t)(i);                                     
    /***********************************************************************/
      #endif
}      
  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSAVHCallLogData
	#include "Mdyn_autosar.h"
#endif  
