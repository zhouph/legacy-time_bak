/*******************************************************************/
/*	Program	ID: 2nd order lpf filter    					       */
/*	Program	description: 2nd order lpf filter   		    	   */
/*	Input files:												   */
/*																   */
/*	Output files:												   */
/*																   */
/*	Special	notes: Will	be applicated to HMC DH		               */
/*******************************************************************/
/*	Modification   Log											   */
/*	Date		   Author			Description					   */
/*	-------		  ----------	-----------------------------	   */
/*	13.03.13	            	 2nd order lpf filter     	       */
/*******************************************************************/


/* Includes	*************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSABSCal2ndOrderLpf
	#include "Mdyn_autosar.h"
#endif

#include  	"LSABSCall2ndOrderLpf.h"
#include	"LDABSCallEstVehDecel.H"
#include	"LCABSCallControl.h"

/*MACRO CONSTANT Definition **********************************************/

#define MultiFactor 10

int16_t ls16tmpCoeff;

/*Variables Definition****************************************************/

struct	ACC_FILTER	*ACC_CALC;

/*Functions Declaration **************************************************/
void	LDABS_vCalcEachFilterSignalAccel(int16_t rawdata, int16_t freq, int16_t zeta, int16_t deltaMulti, int16_t diffcoefCov);

/*Functions Definition  **************************************************/
void	LDABS_vCalcEachFilterSignalAccel(int16_t rawdata, int16_t freq, int16_t zeta, int16_t deltaMulti, int16_t diffcoefCov)
{
	int16_t	 s16FilterCoeff1;
	int16_t	 s16FilterCoeff2;	
	int16_t	 s16FilterTemp1;
		
	
	s16FilterCoeff1	= (int16_t)(freq*zeta*12)/100;
	s16FilterCoeff2	= (int16_t)(zeta*320)/freq;
	
	
	/*================					 2nd Filter			 ===========================
	
	F_Deltainput(n)	= Input(n) - Output(n-1) 
	
								   1									   2*zeta*omegan*T
	F_DeltaFilter(n) =	-----------------------	*F_DeltaFilter(n-1)	+  ----------------------- *F_Deltainput(n-1)  
						 (1+ 2*zeta*omegan*T)							 (1+ 2*zeta*omegan*T) 
						 
					  
								(1000)									   s16FilterCoeff1
					 =	-------------------------*F_DeltaFilter(n-1) +	-------------------------*F_Deltainput(n-1)	
						 (1000 + s16FilterCoeff1)						 (1000 + s16FilterCoeff1)
	
	
	
	
										  (omegam*T)
		   Output(n) =	Output(n-1)	+	--------------*F_DeltaFilter(n)	
										  (2*zeta)
										  
										  
										   F_DeltaFilter(n)			 F_DeltaFilter(n)
					 =	Output(n-1)	+	---------------------	=  ---------------------
											 45*zeta/freq			  s16FilterCoeff2
									  
	==========================================================================================*/
	
	
	ACC_CALC->lsesps16SignalInp		   =  rawdata;
	ACC_CALC->lsesps16DeltalInp		   = (ACC_CALC->lsesps16SignalInp -	ACC_CALC->lsesps16SignalFilter )*deltaMulti;

	tempL0 = 100+ s16FilterCoeff1;
	tempL1 = s16FilterCoeff1;
	
	ACC_CALC->lsesps16DeltalInpFilter  = (int16_t)((((int32_t)ACC_CALC->lsesps16DeltalInpFilter*100) + ((int32_t)ACC_CALC->lsesps16DeltalInp*tempL1 ))/tempL0) ;
	
	s16FilterTemp1 =(int16_t)((((int32_t)ACC_CALC->lsesps16DeltalInpFilter +	ACC_CALC->lsesps16SignalFilterRest )*10)/(deltaMulti*s16FilterCoeff2));	
	
	ACC_CALC->lsesps16SignalFilterRest	= (int16_t)((((int32_t)ACC_CALC->lsesps16DeltalInpFilter + ACC_CALC->lsesps16SignalFilterRest)) - (((int32_t)s16FilterTemp1*s16FilterCoeff2*deltaMulti)/10));
	
	ACC_CALC->lsesps16SignalFilter	    = ACC_CALC->lsesps16SignalFilter + s16FilterTemp1;	
	
    tempL0        = (int32_t)diffcoefCov*MultiFactor*freq;
    tempL1 = tempL0/32;
    ls16tmpCoeff  = zeta*deltaMulti;
    ACC_CALC -> lsesps16SignalAccel = (int16_t)((((int32_t)ACC_CALC->lsesps16DeltalInpFilter*tempL1)/ls16tmpCoeff)/MultiFactor ) ; 

}	

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSABSCal2ndOrderLpf
	#include "Mdyn_autosar.h"
#endif 
