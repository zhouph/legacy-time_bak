/******************************************************************************
* Project Name: Traction Control System
* File: LSTCSCallSensorSignal.C
* Description: TCS sensor signal processing
* Date: 
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSTCSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCTCSCallControl.h"
#include "LSTCSCallSensorSignal.h"
#include "LSENGCallSensorSignal.h"
#include "algo_var.h"
#include "hm_logic_var.h"
#include "LDESPEstDiscTemp.h"


/* Local Definiton  **********************************************************/
UCHAR  lstcsu8TempIgnOffMSecCNT;
UINT   lstcsu16TempIgnOffSecCNT;
/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/

#if __TCS || __BTC
	#if !SIM_MATLAB
void LSTCS_vInitBTCSVariable(void);
	#endif
	
	#if (__BTEM == DISABLE)
void LSTCS_vDecreaseBTCSTemp(struct W_STRUCT *WL);
	#endif

void LSTCS_vWriteEEPROMDiskTemp(void);
#endif

#if __TCS || __BTC   /*TAG2*/
void LSTCS_vCallSensorSignal(void);
void LSTCS_vConvInputSignals(void);

/* GlobalFunction prototype **************************************************/

/* Implementation*************************************************************/


void LSTCS_vCallSensorSignal(void)
{

    LSTCS_vConvInputSignals();
  	
  #if __CAR == GM_GSUV	
	if(speed_calc_timer < L_U8_TIME_10MSLOOP_1000MS)
	{
	  	U16_TCS_SPEED1 = U16_TCS_SPEED1_CAL;
		U16_TCS_SPEED2 = U16_TCS_SPEED2_CAL;
		U16_TCS_SPEED3 = U16_TCS_SPEED3_CAL;
		U16_TCS_SPEED4 = U16_TCS_SPEED4_CAL;
		U16_TCS_SPEED5 = U16_TCS_SPEED5_CAL;
	}
  #endif	

/* Moved from L_vCallMain() */
  #if __BTC
    if((fu1OnDiag==1)||(init_end_flg==0)||
      (fu1ECUHwErrDet==1)||(fu1RearSolErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1SameSideWSSErrDet==1)
      #if (__GM_FailM==0)
       ||(fu1DiagonalWSSErrDet==1)
      #endif
      ) /* ebd_error_flg */
		{
			LSTCS_vWriteEEPROMDiskTemp();
		}	
  #endif
/* Moved from L_vCallMain() */  

}

void LSTCS_vConvInputSignals(void)
{
    /* ------------ Conv Signals from FS ----------- */

    /* tc_on_flg to be deleted */
	if (fu1TCSDisabledBySW==1)
	{
		tc_on_flg =0;
	}
	else
	{
		tc_on_flg =1;
	}
	
/*
    lstcsu1TCSDisabledBySWOld = lstcsu1TCSDisabledBySW;
    lstcsu1TCSDisabledBySW    = fu1TCSDisabledBySW;
*/

    /* ------------ Conv Signals from CAN ----------- */

}
    
#if	(__BTEM == DISABLE)    
void LSTCS_vDecreaseBTCSTemp(struct W_STRUCT *WL)
{
	
    if(WL->tmp_down == L_U8_TIME_10MSLOOP_1000MS)
    {
        WL->tmp_down = 0;
        tempW0 = (uint16_t)WL->btc_tmp>>5;   /*32*/
        if(tempW0 >= (int16_t)450)
        {
        	tempW1 = 1;
        }
        else if(tempW0 >= (int16_t)350)
        {
        	tempW1 = 2;
        }
        else if(tempW0 >= (int16_t)250)
        {
        	tempW1 = 3;
        }
        else if(tempW0 >= (int16_t)170)
        {
        	tempW1 = 4;
        }
        else if(tempW0 >= (int16_t)110)
        {
        	tempW1 = 5;
        }
        else if(tempW0 >= (int16_t)70)
        {
        	tempW1 = 6;
        }
        else if(tempW0 >= (int16_t)40)
        {
        	tempW1 = 7;
        }
        else
        {
        	tempW1 = 8;
        }
        
        tempW1 = (uint16_t)tempW1<<2;
        tempW1 += 9;
        WL->cool_tmp = tempW0/tempW1;
        WL->btc_tmp -= WL->cool_tmp;        
    }
    else
    {
    	WL->tmp_down++;
    }
 		if(WL->btc_tmp < TMP_20)
		{   
      WL->btc_tmp = (int16_t)TMP_20;
  	}
  	else
  	{
  		;
  	}  

}
#endif

void LSTCS_vWriteEEPROMDiskTemp(void)
{

	if(wu8IgnStat == CE_OFF_STATE)
	{
	#if	(__BTEM == DISABLE)
    LSTCS_vDecreaseBTCSTemp(&FL);
    LSTCS_vDecreaseBTCSTemp(&FR);
    LSTCS_vDecreaseBTCSTemp(&RL);
    LSTCS_vDecreaseBTCSTemp(&RR);
	#endif
		
		#if !SIM_MATLAB
			#if TC27X_FEE==ENABLE
				#if !__GM_ENGINE_OFF_TIME_APPLICATION
				if((btc_tmp_fl>=(int16_t)TMP_80)||(btc_tmp_fr>=(int16_t)TMP_80)||
				(btc_tmp_rl>=(int16_t)TMP_80)||(btc_tmp_rr>=(int16_t)TMP_80)||(BLS_EPR==0))
    {
					vcc_off_flg = 0;
    }
    else
    {
					vcc_off_flg = 1;
				}
				#else  /* __GM_ENGINE_OFF_TIME_APPLICATION */
				ee_btcs_data=0x20;
				NVM_btc_tmp_fl=(uint8_t)(btc_tmp_fl>>8);
				NVM_btc_tmp_fr=(uint8_t)(btc_tmp_fr>>8);
				NVM_btc_tmp_rl=(uint8_t)(btc_tmp_rl>>8);
				NVM_btc_tmp_rr=(uint8_t)(btc_tmp_rr>>8);
				NVM_ee_btcs_data= (uint8_t)ee_btcs_data;
				BLS_EPR=1;
				vcc_off_flg = 1;
				#endif
			#else  /* TC27X_FEE==DISABLE, MGH80 ESC Old concept */
    if(BLS_EPR==0)
    {
/*        if(BLS_WRITE == 1)
        {
        	ee_btcs_data |= 0x10;
        }
        else
        {
        	ee_btcs_data &= 0xef;
        }*/
        ee_btcs_data &= 0xdf;
				
				
      if((weu1EraseSector==0)&&(weu1WriteSector==0))
      {
#if __GM_ENGINE_OFF_TIME_APPLICATION
        if((WE_u8ReadEEP_Byte(U16_EEP_BTC_DATA_ADDR, &com_rx_buf[0], 8))==1)
        {
        	if((com_rx_buf[0]!=(uint8_t)((uint16_t)btc_tmp_fl>>8))||(com_rx_buf[1]!=(uint8_t)((uint16_t)btc_tmp_fr>>8))||(com_rx_buf[2]!=(uint8_t)((uint16_t)btc_tmp_rl>>8))||(com_rx_buf[3]!=(uint8_t)((uint16_t)btc_tmp_rr>>8)))
        	{
            com_rx_buf[0] =(uint8_t)((uint16_t)btc_tmp_fl>>8);
            com_rx_buf[1] =(uint8_t)((uint16_t)btc_tmp_fr>>8);
            com_rx_buf[2] =(uint8_t)((uint16_t)btc_tmp_rl>>8);
            com_rx_buf[3] =(uint8_t)((uint16_t)btc_tmp_rr>>8);
            
            weu16EepBuff[0]=*(uint16_t*)&com_rx_buf[0];
            weu16EepBuff[1]=*(uint16_t*)&com_rx_buf[2];
            weu16Eep_addr=0x170;
            weu1WriteSector=1;
        	}
          else
	        {
  	  	    ;
    	    }

        	if(com_rx_buf[4]!=(ee_btcs_data|0x20))
        	{
            com_rx_buf[4]=ee_btcs_data|0x20;
            weu16Eep_addr=0x174;
            weu16EepBuff[0]=*(uint16_t*)&com_rx_buf[4];
            weu16EepBuff[1]=*(uint16_t*)&com_rx_buf[6];
            weu1WriteSector=1;
          }
						else
						{
							BLS_EPR=1;
							vcc_off_flg = 1;
						}
					}
					else
					{
						;
					}
#else          
            if((WE_u8ReadEEP_Byte(0x174, &com_rx_buf[0], 4))==1)
            {
                if(com_rx_buf[0]!=ee_btcs_data)
                {
                    weu16Eep_addr=0x174;
                    com_rx_buf[0]=ee_btcs_data;
                    weu16EepBuff[0]=*(uint16_t*)&com_rx_buf[0];
                    weu16EepBuff[1]=*(uint16_t*)&com_rx_buf[2];
                    weu1WriteSector=1;
                }
                else
                {
                	BLS_EPR=1;
                  vcc_off_flg = 1;
                }
            }
            else
            {
            	;
            }
					#endif
					
        }
        else
        {
        	;
        }
      }
    else
    {
    	;
    }
			#endif  /* TC27X_FEE */
		#endif  /* SIM_MATLAB */

		/* ECU Shutdown backup  */
		if (lstcsu8TempIgnOffMSecCNT == T_1000_MS)
		{
			lstcsu16TempIgnOffSecCNT++;
			lstcsu8TempIgnOffMSecCNT = 0;

			if(lstcsu16TempIgnOffSecCNT > 1800)
    {
				vcc_off_flg = 1;
    }
    else
    {
				;
    }
		}
		else
		{
			lstcsu8TempIgnOffMSecCNT++;
		}
		}
	else
		{
		lstcsu8TempIgnOffMSecCNT = 0;
		lstcsu16TempIgnOffSecCNT = 0;
		}
}

		#if !SIM_MATLAB  /*TAG1*/
void LSTCS_vInitBTCSVariable(void)
{
   #if defined(BTCS_TCMF_CONTROL)	
   FA_TCS.ltcsu1RearAxle = 0; 
   RA_TCS.ltcsu1RearAxle = 1; 
   #endif

/*    av_fspeed_fl=av_fspeed_fr=av_fspeed_rl=av_fspeed_rr=(int16_t)VREF_MIN;*/
    tc_on_flg=1;
		lstcsu8TempIgnOffMSecCNT=0;
		lstcsu16TempIgnOffSecCNT=0;
		
	#if (TC27X_FEE == ENABLE)
		#if !__GM_ENGINE_OFF_TIME_APPLICATION
			btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
		#else  /* __GM_ENGINE_OFF_TIME_APPLICATION */
		    ee_tmp_fl = (uint8_t)(NVM_btc_tmp_fl);
		    ee_tmp_fr = (uint8_t)(NVM_btc_tmp_fr);
		    ee_tmp_rl = (uint8_t)(NVM_btc_tmp_rl);
		    ee_tmp_rr = (uint8_t)(NVM_btc_tmp_rr);
		    ee_btcs_data =(uint8_t)(NVM_ee_btcs_data);
	
			if((ee_btcs_data & 0x20) != 0)
			{
				btc_tmp_fl = ee_tmp_fl;
				btc_tmp_fr = ee_tmp_fr;
				btc_tmp_fl <<=8;
				btc_tmp_fr <<=8;
				btc_tmp_rl = ee_tmp_rl;
				btc_tmp_rr = ee_tmp_rr;
				btc_tmp_rl <<=8;
				btc_tmp_rr <<=8;
			}
			else
			{
				;
			}
			
			if ((ldespu8OutsideAirTemptV==0)&&(ldespu8OutsideAirTemptM==1))
			{
				if((btc_tmp_fl <= ldesps16OutsideAirTempt)&&(btc_tmp_fr <= ldesps16OutsideAirTempt)&&(btc_tmp_rl <= ldesps16OutsideAirTempt)&&(btc_tmp_rr <= ldesps16OutsideAirTempt))
				{   
					btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=ldesps16OutsideAirTempt;
				}
				else
				{
					;
				}
			}
			else
			{
				if((btc_tmp_fl <= TMP_20)&&(btc_tmp_fr <= TMP_20)&&(btc_tmp_rl <= TMP_20)&&(btc_tmp_rr <= TMP_20))
				{   
					btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
				}
				else
				{
					;
				}
			}
		#endif  /* __GM_ENGINE_OFF_TIME_APPLICATION */
	#else  /* TC27X_FEE==DISABLE, MGH80 ESC Old concept */
		#if __GM_VEHICLE
				if ((ldespu8OutsideAirTemptV==0)&&(ldespu8OutsideAirTemptM==1))
				{
						btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=ldesps16OutsideAirTempt;
				}
				else
				{
						btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
				}
		#else
				btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
		#endif



/*    if((ee_btcs_data & 0x10) != 0)
    {
    	BLS_EEPROM = 1;
    }
    else
    {
    	;
    }*/

    if((ee_btcs_data & 0x20) != 0)
    {
        btc_tmp_fl = ee_tmp_fl;
        btc_tmp_fr = ee_tmp_fr;
        btc_tmp_fl <<=8;
        btc_tmp_fr <<=8;
        btc_tmp_rl = ee_tmp_rl;
        btc_tmp_rr = ee_tmp_rr;
        btc_tmp_rl <<=8;
        btc_tmp_rr <<=8;
    }
    else
    {
    	;
    }
		
		#if __GM_ENGINE_OFF_TIME_APPLICATION       
				#if __GM_VEHICLE

						if ((ldespu8OutsideAirTemptV==0)&&(ldespu8OutsideAirTemptM==1))
						{
								if((btc_tmp_fl < ldesps16OutsideAirTempt)&&(btc_tmp_fr < ldesps16OutsideAirTempt)&&(btc_tmp_rl < ldesps16OutsideAirTempt)&&(btc_tmp_rr < ldesps16OutsideAirTempt))
								{   
										btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=ldesps16OutsideAirTempt;
								}
								else
								{
										;
								}
						}
						else
						{
								if((btc_tmp_fl < TMP_20)&&(btc_tmp_fr < TMP_20)&&(btc_tmp_rl < TMP_20)&&(btc_tmp_rr < TMP_20))
								{   
										btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
								}
								else
								{
										;
								}
						}					
				#else
						if((btc_tmp_fl < TMP_20)&&(btc_tmp_fr < TMP_20)&&(btc_tmp_rl < TMP_20)&&(btc_tmp_rr < TMP_20))
						{   
								btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
						}
						else
						{
								;
						}
				#endif
		#endif /* #if __GM_ENGINE_OFF_TIME_APPLICATION */
	#endif  /* TC27X_FEE */
}
		#endif /*TAG1*/
#endif /*TAG2*/
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSTCSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
