/******************************************************************************
* Project Name: Electronic Brake-force Distribution
* File: LCEBDCallControl.C
* Description: EBD control algorithm
* Date: November. 21. 2005
******************************************************************************/

/* Includes ********************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCEBDCallControl  
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCABSCallControl.h"
#include "LCEBDCallControl.h"
#include "LCFBCCallControl.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"

#include "LCHRBCallControl.h"
#include "LCallMain.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSABSCallSensorSignal.H"


/*Global Extern Variable  Declaration*****************************************/

/* Local Definition  ***********************************************/

/* Variables Declaration*********************************************/
U8_BIT_STRUCT_t EBDCTR01, EBDCTR02;

static uint8_t lcebdu8temp0;
#if (__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)
static int8_t  lcebds8SlipThresRaw ;
static int8_t  lcebds8DecelThresRaw;
#endif
int8_t  avr_fslip;
uint16_t  ebd_off_timer;
uint8_t ebd_motor_drv_time;
int16_t   avr_fspd;
int16_t   avr_rspd;

static uint8_t lcebdu8AdRiseHoldScansForMI;
static uint8_t lcebdu8MIDumpScans;
#if (__BLS_NO_WLAMP_FM==ENABLE)
static int8_t lcebds8SlipThresCompLevel;
#endif

/* Local Function prototype ****************************************/
void LCEBD_vCooperateWithYCWABS(void);
void LCEBD_vDecideStartControl(void);
void LCEBD_vDecideContinueControl(void);
void LCEBD_vDecideEndControl(void);

static void LCEBD_vWPresModeEBDDUMP(void);
static void LCEBD_vWPresModeEBDHOLD(void);
  #if __EBD_MI_ENABLE
static void LCEBD_vWPresModeEBDPULSEUP(uint8_t Hold_Reference);
  #else
void LCEBD_vWPresModeEBDFastPULSEUP(void);
void LCEBD_vWPresModeEBDSlowPULSEUP(void);
  #endif
void LCEBD_vWPresModeEBDREAPPLY(void);
void LCEBD_vWPresModeEBDBUILTREAPPLY(void);

int8_t LCEBD_s8CalcRefParameters(int8_t input, int8_t front_gain);

#if __EBD_MI_ENABLE
static void LCEBD_vDecideMICtrl(void);
static void LCEBD_vDecideMICtrlMode(struct W_STRUCT *pRH, struct W_STRUCT *pRL);
static void LCEBD_vMICtrl(struct W_STRUCT *pRH, const struct W_STRUCT *pRL);
#endif
  #if (__REORGANIZE_EBD_ENTRY_DECISION==ENABLE)
void LCEBD_vCheckEBDStartBySlip(void);
#if __VDC
void LCEBD_vCheckEBDStartByMpress(void);
#endif /*__VDC*/
  #endif /*(__REORGANIZE_EBD_ENTRY_DECISION==ENABLE)*/

/* GlobalFunction prototype **************************************************/


/* Implementation***************************************************/

void LCEBD_vCallControl(void)
{
   #if (__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)
    if(lcebdu1BaseCtrlWL==0)
    {
        lcebdu1BaseCtrlWL = 1;
    }
    else
    {
        lcebdu1BaseCtrlWL = 0;
    }
   #endif /*(__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)*/
    
    WL->EBD_MODE=0;

    if(REAR_WHEEL_wl==1)
    {
        #if __VDC
        if(LEFT_WHEEL_wl==1)
        {
            avr_fslip=rel_lam_fl;
        }
        else
        {
            avr_fslip=rel_lam_fr;
        }
        #else
        /*
        tempB0=rel_lam_fl+rel_lam_fr;

        if(tempB0 < 0)
        {
            tempB1=(int8_t)-tempB0;
        }
        else
        {
            tempB1=(int8_t)tempB0;
        }

        tempB2=tempB1>>1;

        if(tempB0 < 0)
        {
            avr_fslip=(int8_t)-tempB2;
        }
        else {
            avr_fslip=(int8_t)tempB2;
        }
        */
        avr_fslip=(int8_t)(((int16_t)rel_lam_fl+(int16_t)rel_lam_fr)/2);
        #endif

/*#if __VDC
        tempW8=WL->wvref-WL->vrad;
#else
        tempW8=vref-WL->vrad;
#endif */

        if(BEND_DETECT2==1)
        {
            if(LEFT_WHEEL_wl==1) {
                avr_fspd=vrad_fl;
            }
            else {
                avr_fspd=vrad_fr;
            }
            avr_rspd=WL->vrad;
        }
        else {
            avr_fspd=((vrad_fl+vrad_fr)/2);
            avr_rspd=((vrad_rl+vrad_rr)/2);
        }
        /*tempW5=WL->diff_max2;*/
        WL->EBD_LFC_RISE_MODE=0;    /* ebd_lfc_rise */

        LCEBD_vCooperateWithYCWABS(); /* Check Cooperation wit other functions */
      #if(__ROUGH_COMP_IMPROVE==ENABLE)
        if((Rough_road_detect_vehicle_EBD==1)&&(ebd_filt_grv>-AFZ_0G5)) /*Check for slight braking at rough road*/
        {
        	lcebdu1RoughSlightBrk = 1;        	
        }
		else
		{
			lcebdu1RoughSlightBrk = 0;
		}
	  #endif

        if (WL->EBD_MODE==0) {
            if (EBD_wl==0) {
                WL->THREE_SEC_FLG=0;     /* int32_t hold - need to verify */
                LCEBD_vDecideStartControl();  /* start control? if no reapply, if yes hold */
            }
            else {
                LCEBD_vDecideEndControl();    /* end control? if yes reapply */
                if (WL->EBD_MODE==0) {LCEBD_vDecideContinueControl();  /* EBD dump or EBD hold ? */}
            }
        }
    }
    if (WL->EBD_MODE==REAPPLY_MODE) {LCEBD_vWPresModeEBDREAPPLY();}
    else if (WL->EBD_MODE==BUILT_MODE) {LCEBD_vWPresModeEBDBUILTREAPPLY();}
    else if (WL->EBD_MODE==DUMP_START) {LCEBD_vWPresModeEBDDUMP();}
    else if (WL->EBD_MODE==HOLD_MODE) {LCEBD_vWPresModeEBDHOLD();}
    else {WL->flags=193;/*continue*/}

    if((WL->EBD==1) && (WL->EBD_LFC_RISE_MODE==FALSE))
	{
    	if((WL->EBD_MODE==HOLD_MODE) || (WL->EBD_MODE==DUMP_START))
		{
			WL->lcebdu8CtrlMode = 4; /* hold or dump */
		}
		else
		{
			WL->lcebdu8CtrlMode = 3; /* rise */
		}
	}
	else
	{
		WL->lcebdu8CtrlMode = 0;
	}


    if(WL->EBD_LFC_RISE_MODE==1)
    {
        WL->WheelCtrlMode=WHEEL_CTRL_EBD_FADE_OUT;
    }
    else {}
}

#if __EBD_MI_ENABLE
void    LCEBD_vCooperateWithYCWABS(void)
{
    uint8_t PULSE_UP_MODE;
    PULSE_UP_MODE=0;
    
#if __VDC
    tempW6=(vref*15)/100;
    if(tempW6>=VREF_15_KPH)
    {
        tempW6=VREF_15_KPH;
    }
    else if(tempW6<=VREF_10_KPH)
    {
        tempW6=VREF_10_KPH; /*  prevent rear big slip at 04.06 */
    }
    else {}

    if((ESP_ERROR_FLG==0)&&((vref-WL->vrad)<tempW6)) {
        if(BRAKE_BY_MPRESS_FLG==1)
        {
            if(INITIAL_BRAKE==0)
            {
                if(YAW_CDC_WORK==1)
                {
                    PULSE_UP_MODE=1;
                }
               #if __FBC
                else if(FBC_ON==1)
                {
                    PULSE_UP_MODE=1;
                }
               #endif
                else { }
            }
            else { }
        }
		else { }
    }
#endif

    if((lcabsu1AbsErrorFlag==0)&&((ABS_fl==1) || (ABS_fr==1)))
    {
        if(vref > (int16_t)VREF_5_KPH)
        {
          #if __ABS_RBC_COOPERATION
        	if(lcabsu1FastPressRecoveryBfAbs==1)
        	{
        		;
        	}
        	else
          #endif  /* __ABS_RBC_COOPERATION */
            {
              #if __SLIGHT_BRAKING_COMPENSATION
                if((lcabsu1SlightBraking==1)&&
                   (((WL->s0_reapply_counter>=U8_PULSE_UP_LIMIT_CNT_LV1)&&((mpress-RL.s16_Estimated_Active_Press)<S16_PULSE_UP_LIMIT_PRESS_DIFF)&&((mpress-RR.s16_Estimated_Active_Press)<S16_PULSE_UP_LIMIT_PRESS_DIFF))
                   ||(WL->s0_reapply_counter>=U8_PULSE_UP_LIMIT_CNT_LV2)))
				{
					;
				}
				else
				{
					PULSE_UP_MODE=1;
				}
			  #else /* __SLIGHT_BRAKING_COMPENSATION */
				PULSE_UP_MODE=1;
			  #endif /* __SLIGHT_BRAKING_COMPENSATION */
            }
        }
    }
    else
    {
     #if __REAR_D
        if(((ABS_fz==0) && (EBD_rl==0) && (EBD_rr==0)) && /* ENGINE BRAKING */
	      #if __VDC
      	(BRAKE_BY_MPRESS_FLG==0)
	      #else
        (lcabsu1BrakeByBLS==0)
	      #endif
        )
        {
            if((rel_lam_fl>=(int8_t)(-LAM_2P))||(rel_lam_fr>=(int8_t)(-LAM_2P)))
            {
                if(WL->rel_lam>(int8_t)(-LAM_25P))
                {
                    WL->flags=211;
                    WL->EBD_MODE=REAPPLY_MODE;
                }
            }
        }
     #endif
    }

    if(REAR_EBD_INHIBITION_flag==1)
    {
        WL->flags=212;
        WL->EBD_MODE=REAPPLY_MODE;
    }
    else if(REAR_ABS_INHIBITION_flag==1)
    {
    	WL->flags=213;
        WL->EBD_MODE=DUMP_START;
    }
    else if(PULSE_UP_MODE==1)
    {
       #if __HRB
        if(lchrbu1ActiveFlag==1)
        {
            lcebdu8temp0=lchrbu8PulseUpHoldScans;
        }
        else
       #endif
        {
           #if __SLIGHT_BRAKING_COMPENSATION
            if( (lcabsu1SlightBraking==1) && 
                (((RL.rel_lam >= -LAM_4P)&&(RR.rel_lam >= -LAM_4P)) || (((vref-RL.vrad)<= VREF_2_5_KPH)&&((vref-RR.vrad)<= VREF_2_5_KPH)))
              )
            {
                /* intention: to compensate low delta-P */
                if(vref < VREF_40_KPH)
                {
                    lcebdu8temp0 = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, 0, U8_EBD_PULSEUP_HOLD_TIME_DEF);
                }
                else
                {
                    lcebdu8temp0 = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, L_TIME_30MS, U8_EBD_PULSEUP_HOLD_TIME_DEF);
                }
            }
            else
           #endif
           #if __VDC
            if(ACTIVE_BRAKE_FLG==1)
            {
            	if(vref < VREF_40_KPH)
            	{
            		lcebdu8temp0 = (uint8_t)LCABS_s16Interpolation2P(vref,VREF_5_KPH,VREF_40_KPH, 0, U8_EBD_PULSEUP_HOLD_TIME_DEF);
            	}
            	else
            	{
            		lcebdu8temp0=U8_EBD_PULSEUP_HOLD_TIME_DEF;
            	}
            }
            else
           #endif /* __VDC */
            {
                lcebdu8temp0=U8_EBD_PULSEUP_HOLD_TIME_DEF;
			  #if(__ROUGH_COMP_IMPROVE==ENABLE)
                if(lcebdu1RoughSlightBrk==1)
                {
                	lcebdu8temp0= (uint8_t)LCABS_s16Interpolation2P(ebd_filt_grv,-(int16_t)U8_PULSE_LOW_MU,-(int16_t)U8_PULSE_HIGH_MU, U8_EBD_PULSEUP_HOLD_TIME_MIN, U8_EBD_PULSEUP_HOLD_TIME_DEF);
                }                
			  #endif
            }
       }
        
        if(WL->EBDMICtrlMode>=MI_LEVEL1)
        {
            if(((int16_t)lcebdu8temp0+(int16_t)lcebdu8AdRiseHoldScansForMI) > L_TIME_560MS)
            {
                lcebdu8temp0 = L_TIME_560MS; /* limit to 80 scans = 80*7ms = 560ms */
            }
            else
            {
                lcebdu8temp0 = lcebdu8temp0 + lcebdu8AdRiseHoldScansForMI;
            }
            WL->flags=178;
        }
        
        LCEBD_vWPresModeEBDPULSEUP(lcebdu8temp0);
    }
    else {}
}
#else /*__EBD_MI_ENABLE*/
void    LCEBD_vCooperateWithYCWABS(void)
{

#if __VDC
    tempW6=(vref*15)/100;
    if(tempW6>=VREF_15_KPH)
    {
        tempW6=VREF_15_KPH;
    }
    else if(tempW6<=VREF_10_KPH)
    {
        tempW6=VREF_10_KPH; /*  prevent rear big slip at 04.06 */
    }
    else {}

    if((ESP_ERROR_FLG==0)&&((vref-WL->vrad)<tempW6)) {
        /*
        if(((lcabsu1BrakeSensorOn==0) && (ACTIVE_BRAKE_FLG==0)) ||((INITIAL_BRAKE==1)&&(YAW_CDC_WORK==1)))
        {
            WL->flags=214;
            WL->EBD_MODE=REAPPLY_MODE;
        }
        */

        if(BRAKE_BY_MPRESS_FLG==1)
        {
            if(YAW_CDC_WORK==1)
            {
                if(INITIAL_BRAKE==0) {
                    if(ENT_ABS_wl==0) {
                        WL->hold_timer_new=U8_EBD_PULSEUP_HOLD_TIME_DEF;       /* MI_02H(0->4) */
                        ENT_ABS_wl=1;
                    }
                    else {}
                    LCEBD_vWPresModeEBDFastPULSEUP();
                }
                else {
                    /*
                    WL->flags=214;
                    WL->EBD_MODE=REAPPLY_MODE;*/
                    ;
                }
            }
           #if __FBC
            else if(FBC_ON==1) {
                if(ENT_ABS_wl==0) {
                    WL->hold_timer_new=U8_EBD_PULSEUP_HOLD_TIME_DEF;       /* MI_02H(0->4) */
                    ENT_ABS_wl=1;
                }
                else {}
                LCEBD_vWPresModeEBDFastPULSEUP();
            }
           #endif
            else {}
        }
		else {}
    }
#endif

    if((lcabsu1AbsErrorFlag==0)&&((ABS_fl==1) || (ABS_fr==1))) {
        if(vref > (int16_t)VREF_5_KPH) {
          #if __ABS_RBC_COOPERATION
        	if(lcabsu1FastPressRecoveryBfAbs==1)
        	{
        		;
        	}
        	else
          #endif  /* __ABS_RBC_COOPERATION */
            if((BEND_DETECT2==0) && (vref > VREF_30_KPH) && (Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0)
               && (((ABS_rl==1) && (RL.state==UNSTABLE)) || ((ABS_rr==1) && (RR.state==UNSTABLE))))
             /* Rear Wheel Pressure Limitation - if 1 rear wl is unstable, EBD pulse-up is limited (except for Bend, Rough Road and Low Speeds) */
            {
                WL->flags=177;
                WL->EBD_MODE=HOLD_MODE;
            }
            else
            {
                if(ENT_ABS_wl==0) {
                    WL->hold_timer_new=U8_EBD_PULSEUP_HOLD_TIME_DEF;       /* MI_02H(0->4) */
                    ENT_ABS_wl=1;
                }
                else
                {}
                LCEBD_vWPresModeEBDFastPULSEUP();
            }
        }
    }
    else {
        ENT_ABS_wl=0;
     #if __REAR_D
        if(((ABS_fz==0) && (EBD_rl==0) && (EBD_rr==0)) && /* ENGINE BRAKING */
	      #if __VDC
      	(BRAKE_BY_MPRESS_FLG==0)
	      #else
        (lcabsu1BrakeByBLS==0)
	      #endif
        )
        {
            if((rel_lam_fl>=(int8_t)(-LAM_2P))||(rel_lam_fr>=(int8_t)(-LAM_2P)))
            {
                if(WL->rel_lam>(int8_t)(-LAM_25P))
                {
                        WL->flags=211;
                        WL->EBD_MODE=REAPPLY_MODE;
                  }
              }
          }
     #endif
    }
    if(REAR_EBD_INHIBITION_flag==1)
    {
		WL->flags=212;
        WL->EBD_MODE=REAPPLY_MODE;
    }
    else if(REAR_ABS_INHIBITION_flag==1)
    {
    	WL->flags=213;
        WL->EBD_MODE=DUMP_START;
    }
    else { }
}
#endif /*__EBD_MI_ENABLE*/


#if (__REORGANIZE_EBD_ENTRY_DECISION==ENABLE)
void LCEBD_vDecideStartControl(void) /*  whether to start or not EBD control */
{
    lcebdu1EntrySlipSatisfied = 0;
  #if __VDC
    lcebdu1KneePointSatisfied = 0;
  #endif

    if((vref > VREF_10_KPH) && ((vref>=RL.vrad_crt) && (vref>=RR.vrad_crt))) /* general condition */
    {        
        LCEBD_vCheckEBDStartBySlip();
      #if __VDC
        LCEBD_vCheckEBDStartByMpress();
      #endif
    }
    else
    {
        ;
    }

    if(lcebdu1EntrySlipSatisfied==1)
    {
        WL->flags=172;
        WL->EBD_MODE=HOLD_MODE;
    }
  #if __VDC
    else if(lcebdu1KneePointSatisfied==1)
    {
        WL->flags=253;
        WL->EBD_MODE=HOLD_MODE;
    }
  #endif/* __VDC*/
    else
    {
        WL->flags=183;
        WL->EBD_MODE=REAPPLY_MODE;
    }
}

void LCEBD_vCheckEBDStartBySlip(void)
{
    int8_t front_avg_slip_decel_gain;
    int8_t arad_gain;
    int8_t slip_thres;
    int8_t arad_thres;
    int16_t decel_thres;
    int8_t front_rear_speed_percent;
    int8_t Low_Speed_Comp_Slip;

    front_avg_slip_decel_gain =0;
    slip_thres=0;
    arad_thres=0;
    arad_gain=0;

    if((FL.arad+FR.arad)<=0) {tempW4=avr_fspd;}
    else {tempW4=vref;}

    /*
    tempW2=WL->vrad-tempW4;
    tempW1=100;
    tempW0=tempW4;
    s16muls16divs16();
    */
/*        WL->front_rear_speed_percent=(int8_t)tempW3; */
    /*front_rear_speed_percent=(int8_t)tempW3;*/

    front_rear_speed_percent=(int8_t)(((int32_t)(WL->vrad-tempW4)*100)/tempW4);

/*    if ((holding_cycle_timer_fl!=0)||(holding_cycle_timer_fr!=0)) {
        WL->flags=170;
        WL->EBD_MODE=REAPPLY_MODE;
    }
    else {*/
  /* ----------------------------------------------------------- arad gain --------------------------------------------------------------------------*/
        if (vref<(int16_t)VREF_20_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_B_20KPH+ARAD_0G5),ARAD_0G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_B_20KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_B_20KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_B_20KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_B_20KPH;
            }
        }
        else if (vref<(int16_t)VREF_40_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_20_40KPH+ARAD_1G0),ARAD_1G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_20_40KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_20_40KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_20_40KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_20_40KPH;
            }
        }
        else if (vref<(int16_t)VREF_60_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_40_60KPH+ARAD_1G5),ARAD_1G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_40_60KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_40_60KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_40_60KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_40_60KPH;
            }
        }
        else if (vref<(int16_t)VREF_100_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_60_100KPH+ARAD_1G5),ARAD_1G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_60_100KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_60_100KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_60_100KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_60_100KPH;
            }
        }
        else if(vref < (int16_t)VREF_150_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_100_150KPH+ARAD_2G0),ARAD_2G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_100_150KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_100_150KPH;
            }
           else if ((WL->arad)>=(S8_Arad_3rdPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_100_150KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_100_150KPH;
            }
        }
        else {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_Ab_150KPH+ARAD_2G0),ARAD_2G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_Ab_150KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_Ab_150KPH;
            }
           else if ((WL->arad)>=(S8_Arad_3rdPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_Ab_150KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_Ab_150KPH;
            }
        }
        /* ----------------------------------------------- Considering front arad, front slip ----------------------------------------------------- */
        tempB5=arad_fl+arad_fr;
        if(avr_fslip>=0) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_SlipPos;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_SlipPos;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_SlipPos;
            }
        }
        else if(avr_fslip>=(int8_t)(-LAM_2P)) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_Slip0_2P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_Slip0_2P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_Slip0_2P;
            }
        }
        else if(avr_fslip>=(int8_t)(-LAM_5P)) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_Slip2_5P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_Slip2_5P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_Slip2_5P;
            }
        }
        else {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_SlipAb5P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_SlipAb5P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_SlipAb5P;
            }
        }
      /* -----------------------------------------------------------------------------------------------------------------------*/

        
      #if (__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)
        if(lcebdu1BaseCtrlWL==1)
        {
            if (vref<(int16_t)VREF_20_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_B_20kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_B_20KPH;
            }
            else if (vref<(int16_t)VREF_40_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_20_40kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_20_40KPH;
            }
            else if (vref<(int16_t)VREF_60_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_40_60kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_40_60KPH;
            }
            else if (vref<(int16_t)VREF_100_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_60_100kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_60_100KPH;
            }
            else if(vref < (int16_t)VREF_150_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_100_150kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_100_150KPH;
            }
            else {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_Ab_150kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_Ab_150KPH;
            }
        }
        else {}

        slip_thres  = lcebds8SlipThresRaw;
        decel_thres = (int16_t)lcebds8DecelThresRaw;

      #else /*(__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)*/

        if (vref<(int16_t)VREF_20_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_B_20kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_B_20KPH;
        }
        else if (vref<(int16_t)VREF_40_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_20_40kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_20_40KPH;
        }
        else if (vref<(int16_t)VREF_60_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_40_60kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_40_60KPH;
        }
        else if (vref<(int16_t)VREF_100_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_60_100kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_60_100KPH;
        }
        else if(vref < (int16_t)VREF_150_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_100_150kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_100_150KPH;
        }
        else {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_Ab_150kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_Ab_150KPH;
        }

      #endif /*(__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)*/
        
        
        slip_thres=LCEBD_s8CalcRefParameters(slip_thres,arad_gain);
     #if __VDC
        if(vref < VREF_40_KPH)
        {
            Low_Speed_Comp_Slip = (int8_t)((VREF_40_KPH - vref)/VREF_2_KPH);
        }
        else
        {
            Low_Speed_Comp_Slip = 0;
        }

        if(ACTIVE_BRAKE_FLG==0)
        {
           #if (__BLS_NO_WLAMP_FM==ENABLE)
               #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            if( ((lsespu1PedalBrakeOn==1)&&(lsespu1MpresBrkOnByAHBgen3==1)&&(lcabsu1BrakeByBLS==1))
              ||((lsespu1PedalBrakeOn==1)&&(lsespu1MpresByAHBgen3Invalid==1)&&(lcabsu1BrakeByBLS==1)) )
            {
            	lcebds8SlipThresCompLevel = 0;/*normal braking*/
            }
            else if( ((lsespu1PedalBrakeOn==1)&&(lsespu1MpresByAHBgen3Invalid==1)&&(lcabsu1BrakeByBLS==0))
                   ||((lcabsu1ValidPedalTravel==0)&&(lsespu1MpresBrkOnByAHBgen3==1)&&(lcabsu1BrakeByBLS==1)) )
            {
            	lcebds8SlipThresCompLevel = 1;/*at least one brake signal input*/
            }
            else if( ((lsespu1PedalBrakeOn==0)&&(lsespu1MpresBrkOnByAHBgen3==0))/*no braking*/
            	   ||((lcabsu1ValidPedalTravel==0)&&(lsespu1MpresByAHBgen3Invalid==1))
                   ||((lcabsu1ValidPedalTravel==0)&&(lsespu1MpresBrkOnByAHBgen3==1)&&(lcabsu1BrakeByBLS==0)) )
            {
            	lcebds8SlipThresCompLevel = 2;/*all brake signals error*/	
            }
               #else
           	if(fu1McpSenPwr1sOk==1)
		    {
		    	if(((lsespu1MpressOffsetOK==0)&&(mpress>=BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==1))
				 ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==1) &&(lcabsu1BrakeByBLS==1)))
				{
					lcebds8SlipThresCompLevel = 0;/*normal braking*/
				}			
				else if(((lsespu1MpressOffsetOK==0)&&(mpress<BRK_ON_PRESS_NO_OFFSET_COR) &&(lcabsu1BrakeByBLS==1))
				      ||((lsespu1MpressOffsetOK==0)&&(mpress>=BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==0))
				      ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==1)                &&(lcabsu1BrakeByBLS==0))
				     ||(((fu1MCPErrorDet==1)||(fu1MCPSusDet==1))&&(lcabsu1BrakeByBLS==1)))
				{
					lcebds8SlipThresCompLevel = 1;/*at least one brake signal input*/
				}
		    	else if(((lsespu1MpressOffsetOK==0)&&(mpress<BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==0))
				      ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==0)/*&&(BLS==don't care)<-no brake input*/)
				     ||(((fu1MCPErrorDet==1)||(fu1MCPSusDet==1))&&(lcabsu1BrakeByBLS==0)))
				{
					lcebds8SlipThresCompLevel = 2;/*all brake signals error*/
				}
				else{;}
			}
			  #endif/*#if  __AHB_GEN3_SYSTEM == ENABLE*/
					
			switch(lcebds8SlipThresCompLevel)
			{
				case 1: 
					tempW7 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED1, HIGH_COMP_SLIP_THRES1, LOW_COMP_SLIP_THRES1);                                      
				    slip_thres += (tempW7 + Low_Speed_Comp_Slip);
				break;
				case 2:
					tempW7 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED2, HIGH_COMP_SLIP_THRES2, LOW_COMP_SLIP_THRES2);                                    
				    slip_thres += (tempW7 + Low_Speed_Comp_Slip);   
				break;
				default:
					/*no braking input*/
				break;
			}
							    
          #else /* #if (__BLS_NO_WLAMP_FM==ENABLE) */

            if((BRAKE_BY_MPRESS_FLG==0) && (lcabsu1BrakeByBLS==0))
            {
            	tempW7 = LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED2, HIGH_COMP_SLIP_THRES2, LOW_COMP_SLIP_THRES2);                                    
            }
			else if(((BRAKE_BY_MPRESS_FLG==0) && (lcabsu1BrakeByBLS==1)) || ((BRAKE_BY_MPRESS_FLG==1) && (lcabsu1BrakeByBLS==0)))
            {
            	tempW7 = LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED1, HIGH_COMP_SLIP_THRES1, LOW_COMP_SLIP_THRES1);                                  
            }
            else
            {
				tempW7 = 0; 
            }
			slip_thres += ((int8_t)tempW7 + Low_Speed_Comp_Slip);  
			
          #endif /* #if (__BLS_NO_WLAMP_FM==ENABLE) */
        }
        else
        {
            ;
        }
     #endif

    /* --------------------------------------------------------rough road sum ------------------------------------------------*/
         tempB1 = S8_Rough_road_detector_R_level1;
         tempB2 = S8_Rough_road_detector_R_level2;

         if(Rough_road_detect_vehicle_EBD == 1)
         {
            if(WL->Rough_road_detector5 >= tempB2)      {decel_thres += VREF_17_KPH;}
            else if(WL->Rough_road_detector5 >= tempB1) {decel_thres += VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {decel_thres += VREF_12_KPH;}
            else if(WL->Rough_road_detector4 >= tempB1) {decel_thres += VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {decel_thres += VREF_6_KPH;}
            else if(WL->Rough_road_detector3 >= tempB1) {decel_thres += VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {decel_thres += VREF_4_KPH;}
            else if(WL->Rough_road_detector2 >= tempB1) {decel_thres += VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {decel_thres += VREF_3_KPH;}
            else {decel_thres += VREF_2_KPH;}
        }
        else
        {
            if(WL->Rough_road_detector5 >= tempB2)      {decel_thres += VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {decel_thres += VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {decel_thres += VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {decel_thres += VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {decel_thres += VREF_1_KPH;}
            else { }
        }
    /* ------------------------------------------------------------------------------------------------------------------------------- */

/*    if (BEND_DETECT==1) tempB7=WL->slip_using_side_vref;  /added due to EBD at cornering  dolly 05.01.05
    else tempB7=WL->rel_lam; */

    if((vref-WL->vrad)>=decel_thres) {
    	if(LEFT_WHEEL_wl==1){lcebdu1Decel_rl = 1;}
    	else			 	{lcebdu1Decel_rr = 1;}
/*       if ((tempB7<=(int8_t)(-slip_thres))||(WL->front_rear_speed_percent<=(int8_t)(-slip_thres))) {   */
        if ((WL->rel_lam<=(int8_t)(-slip_thres))||(front_rear_speed_percent<=(int8_t)(-slip_thres))) {
	    	if(LEFT_WHEEL_wl==1){lcebdu1Slip_rl = 1;}
	    	else				{lcebdu1Slip_rr = 1;}    	
            if (WL->arad<=(int8_t)(-arad_thres)) {
                if(LEFT_WHEEL_wl==1){lcebdu1Arad_rl = 1;}
		    	else				{lcebdu1Slip_rr = 1;}
		    	lcebdu1EntrySlipSatisfied = 1;             
            }
        }
    }
    else{}
    
    if(EBD_RA==0)
    {
    	lcebdu1Decel_rl = 0;	lcebdu1Decel_rr = 0;
    	lcebdu1Slip_rl  = 0;	lcebdu1Slip_rr  = 0;
    	lcebdu1Arad_rl  = 0;	lcebdu1Arad_rr  = 0;	
    }
    else{}
}

#if __VDC
void LCEBD_vCheckEBDStartByMpress(void)
{
  #if __BRK_SIG_MPS_TO_PEDAL_SEN
    int16_t s16EstDriverBrakePress;
  #endif

    if( (lcebdu1EntrySlipSatisfied==0) && (vref > VREF_30_KPH) )
    {
       #if __BRK_SIG_MPS_TO_PEDAL_SEN
        if((lcabsu1ValidPedalTravel==1) || (lcabsu1ValidMpress==1))
       #else
         #if (__BLS_NO_WLAMP_FM==ENABLE)
        if((fu1McpSenPwr1sOk==1)&&(fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(mpress > BRK_ON_PRESS_NO_OFFSET_COR)) /*B201, due to BS/BLS patch, mpress condition is added*/
	     #else
        if(lcabsu1ValidMpress==1)
         #endif
       #endif
        {
            tempW8 = S16_EBD_KNEE_POINT;

            if(tempW8 > MPRESS_150BAR)
            {
                tempW8 = MPRESS_150BAR;
            }
            else if(tempW8 < MPRESS_50BAR)
            {
                tempW8 = MPRESS_50BAR;
            }
            else {}
            
            if((lcabss16RefCircuitPress > tempW8)&&((RL.s16_Estimated_Active_Press>tempW8)&&(RR.s16_Estimated_Active_Press>tempW8)))
            {
            	if((ebd_filt_grv < -AFZ_0G3) &&
          		 (((vref-WL->vrad) >= VREF_1_KPH) || (WL->rel_lam <= -LAM_1P)) && (WL->arad < ARAD_0G0))
          		{
          			lcebdu1KneePointSatisfied=1;
          		}
            }
        }
    } /*if( (lcebdu1EntrySlipSatisfied==0) && (vref > VREF_30_KPH) )*/
}
#endif /*__VDC*/

#else /*(__REORGANIZE_EBD_ENTRY_DECISION==ENABLE)*/

void LCEBD_vDecideStartControl(void) /*  whether to start or not EBD control */
{
    int8_t front_avg_slip_decel_gain;
    int8_t arad_gain;

    int8_t slip_thres;
    int8_t arad_thres;
    int16_t decel_thres;
    int8_t front_rear_speed_percent;
    int8_t Low_Speed_Comp_Slip;
    int8_t EBD_BY_MPRESS;

  #if __BRK_SIG_MPS_TO_PEDAL_SEN
    int16_t s16EstDriverBrakePress=0;
  #endif

    front_avg_slip_decel_gain =0;
    slip_thres=0;
    arad_thres=0;
    arad_gain=0;

    if((FL.arad+FR.arad)<=0) {tempW4=avr_fspd;}
    else {tempW4=vref;}

    /*
    tempW2=WL->vrad-tempW4;
    tempW1=100;
    tempW0=tempW4;
    s16muls16divs16();
    */
/*        WL->front_rear_speed_percent=(int8_t)tempW3; */
    /*front_rear_speed_percent=(int8_t)tempW3;*/

    front_rear_speed_percent=(int8_t)(((int32_t)(WL->vrad-tempW4)*100)/tempW4);

/*    if ((holding_cycle_timer_fl!=0)||(holding_cycle_timer_fr!=0)) {
        WL->flags=170;
        WL->EBD_MODE=REAPPLY_MODE;
    }
    else {*/
  /* ----------------------------------------------------------- arad gain --------------------------------------------------------------------------*/
        if (vref<(int16_t)VREF_20_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_B_20KPH+ARAD_0G5),ARAD_0G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_B_20KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_B_20KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_B_20KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_B_20KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_B_20KPH;
            }
        }
        else if (vref<(int16_t)VREF_40_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_20_40KPH+ARAD_1G0),ARAD_1G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_20_40KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_20_40KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_20_40KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_20_40KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_20_40KPH;
            }
        }
        else if (vref<(int16_t)VREF_60_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_40_60KPH+ARAD_1G5),ARAD_1G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_40_60KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_40_60KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_40_60KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_40_60KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_40_60KPH;
            }
        }
        else if (vref<(int16_t)VREF_100_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_60_100KPH+ARAD_1G5),ARAD_1G5);
            if ((WL->arad)>=(S8_Arad_1stPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_60_100KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_60_100KPH;
            }
            else if ((WL->arad)>=(S8_Arad_3rdPoint_60_100KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_60_100KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_60_100KPH;
            }
        }
        else if(vref < (int16_t)VREF_150_KPH) {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_100_150KPH+ARAD_2G0),ARAD_2G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_100_150KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_100_150KPH;
            }
           else if ((WL->arad)>=(S8_Arad_3rdPoint_100_150KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_100_150KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_100_150KPH;
            }
        }
        else {
            arad_thres=LCEBD_s8CalcRefParameters((S8_EBD_Cut_Arad_Ab_150KPH+ARAD_2G0),ARAD_2G0);
            if ((WL->arad)>=(S8_Arad_1stPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_B_1P_Ab_150KPH;
            }
            else if ((WL->arad)>=(S8_Arad_2ndPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_1_2P_Ab_150KPH;
            }
           else if ((WL->arad)>=(S8_Arad_3rdPoint_Ab_150KPH)) {
                arad_gain=S8_Arad_Fact_2_3P_Ab_150KPH;
            }
            else {
                arad_gain=S8_Arad_Fact_Ab_3P_Ab_150KPH;
            }
        }
        /* ----------------------------------------------- Considering front arad, front slip ----------------------------------------------------- */
        tempB5=arad_fl+arad_fr;
        if(avr_fslip>=0) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_SlipPos;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_SlipPos;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_SlipPos;
            }
        }
        else if(avr_fslip>=(int8_t)(-LAM_2P)) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_Slip0_2P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_Slip0_2P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_Slip0_2P;
            }
        }
        else if(avr_fslip>=(int8_t)(-LAM_5P)) {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_Slip2_5P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_Slip2_5P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_Slip2_5P;
            }
        }
        else {
            if(tempB5 >= (int8_t)(-ARAD_1G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum1G_SlipAb5P;
            }
            else if(tempB5 >= (int8_t)(-ARAD_3G0)) {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASum3G_SlipAb5P;
            }
            else {
                front_avg_slip_decel_gain=S8_F_WL_Fact_ASumAb3G_SlipAb5P;
            }
        }
      /* -----------------------------------------------------------------------------------------------------------------------*/

        
      #if (__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)
        if(lcebdu1BaseCtrlWL==1)
        {
            if (vref<(int16_t)VREF_20_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_B_20kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_B_20KPH;
            }
            else if (vref<(int16_t)VREF_40_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_20_40kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_20_40KPH;
            }
            else if (vref<(int16_t)VREF_60_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_40_60kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_40_60KPH;
            }
            else if (vref<(int16_t)VREF_100_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_60_100kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_60_100KPH;
            }
            else if(vref < (int16_t)VREF_150_KPH) {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_100_150kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_100_150KPH;
            }
            else {
                lcebds8SlipThresRaw =LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_Ab_150kph,front_avg_slip_decel_gain);
                lcebds8DecelThresRaw=S8_EBD_Cut_Vdiff_Ab_150KPH;
            }
        }
        else {}

        slip_thres  = lcebds8SlipThresRaw;
        decel_thres = (int16_t)lcebds8DecelThresRaw;

      #else /*(__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)*/

        if (vref<(int16_t)VREF_20_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_B_20kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_B_20KPH;
        }
        else if (vref<(int16_t)VREF_40_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_20_40kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_20_40KPH;
        }
        else if (vref<(int16_t)VREF_60_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_40_60kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_40_60KPH;
        }
        else if (vref<(int16_t)VREF_100_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_60_100kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_60_100KPH;
        }
        else if(vref < (int16_t)VREF_150_KPH) {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_100_150kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_100_150KPH;
        }
        else {
            slip_thres=LCEBD_s8CalcRefParameters((int8_t)U8_EBD_Cut_Slip_Ab_150kph,front_avg_slip_decel_gain);
            decel_thres=S8_EBD_Cut_Vdiff_Ab_150KPH;
        }

      #endif /*(__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)*/
        
        
        slip_thres=LCEBD_s8CalcRefParameters(slip_thres,arad_gain);
     #if __VDC
        if(vref < VREF_40_KPH)
        {
            Low_Speed_Comp_Slip = (int8_t)((VREF_40_KPH - vref)/VREF_2_KPH);
        }
        else
        {
            Low_Speed_Comp_Slip = 0;
        }

        if(ACTIVE_BRAKE_FLG==0)
        {
           #if (__BLS_NO_WLAMP_FM==ENABLE)
           	if(fu1McpSenPwr1sOk==1)
		    {
		    	if(((lsespu1MpressOffsetOK==0)&&(mpress>=BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==1))
				 ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==1) &&(lcabsu1BrakeByBLS==1)))
				{
					lcebds8SlipThresCompLevel = 0;/*normal braking*/
				}			
				else if(((lsespu1MpressOffsetOK==0)&&(mpress<BRK_ON_PRESS_NO_OFFSET_COR) &&(lcabsu1BrakeByBLS==1))
				      ||((lsespu1MpressOffsetOK==0)&&(mpress>=BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==0))
				      ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==1)                &&(lcabsu1BrakeByBLS==0))
				     ||(((fu1MCPErrorDet==1)||(fu1MCPSusDet==1))&&(lcabsu1BrakeByBLS==1)))
				{
					lcebds8SlipThresCompLevel = 1;/*at least one brake signal input*/
				}
		    	else if(((lsespu1MpressOffsetOK==0)&&(mpress<BRK_ON_PRESS_NO_OFFSET_COR)&&(lcabsu1BrakeByBLS==0))
				      ||((lsespu1MpressOffsetOK==1)&&(MPRESS_BRAKE_ON==0)/*&&(BLS==don't care)<-no brake input*/)
				     ||(((fu1MCPErrorDet==1)||(fu1MCPSusDet==1))&&(lcabsu1BrakeByBLS==0)))
				{
					lcebds8SlipThresCompLevel = 2;/*all brake signals error*/
				}
				else{;}			
			}
					
			switch(lcebds8SlipThresCompLevel)
			{
				case 1: 
					tempW7 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED1, HIGH_COMP_SLIP_THRES1, LOW_COMP_SLIP_THRES1);                                      
				    slip_thres += (tempW7 + Low_Speed_Comp_Slip);
				break;
				case 2:
					tempW7 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED2, HIGH_COMP_SLIP_THRES2, LOW_COMP_SLIP_THRES2);                                    
				    slip_thres += (tempW7 + Low_Speed_Comp_Slip);   
				break;
				default:
					/*no braking input*/
				break;
			}
							    
           #else /* #if (__BLS_NO_WLAMP_FM==ENABLE) */

            if((BRAKE_BY_MPRESS_FLG==0) && (lcabsu1BrakeByBLS==0))
            {
            	tempW7 = LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED2, HIGH_COMP_SLIP_THRES2, LOW_COMP_SLIP_THRES2);                                    
            }
			else if(((BRAKE_BY_MPRESS_FLG==0) && (lcabsu1BrakeByBLS==1)) || ((BRAKE_BY_MPRESS_FLG==1) && (lcabsu1BrakeByBLS==0)))
            {
            	tempW7 = LCABS_s16Interpolation2P(vref,VREF_40_KPH, SLIP_COMP_H_THRES_SPEED1, HIGH_COMP_SLIP_THRES1, LOW_COMP_SLIP_THRES1);                                  
            }
            else
            {
				tempW7 = 0; 
            }
			slip_thres += ((int8_t)tempW7 + Low_Speed_Comp_Slip);  
			
           #endif /* #if (__BLS_NO_WLAMP_FM==ENABLE) */
        }
        else
        {
            ;
        }
     #endif

    /* --------------------------------------------------------rough road sum ------------------------------------------------*/
         tempB1 = S8_Rough_road_detector_R_level1;
         tempB2 = S8_Rough_road_detector_R_level2;

         if(Rough_road_detect_vehicle_EBD == 1)
         {
            if(WL->Rough_road_detector5 >= tempB2)      {decel_thres += VREF_17_KPH;}
            else if(WL->Rough_road_detector5 >= tempB1) {decel_thres += VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {decel_thres += VREF_12_KPH;}
            else if(WL->Rough_road_detector4 >= tempB1) {decel_thres += VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {decel_thres += VREF_6_KPH;}
            else if(WL->Rough_road_detector3 >= tempB1) {decel_thres += VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {decel_thres += VREF_4_KPH;}
            else if(WL->Rough_road_detector2 >= tempB1) {decel_thres += VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {decel_thres += VREF_3_KPH;}
            else {decel_thres += VREF_2_KPH;}
        }
        else
        {
            if(WL->Rough_road_detector5 >= tempB2)      {decel_thres += VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {decel_thres += VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {decel_thres += VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {decel_thres += VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {decel_thres += VREF_1_KPH;}
            else { }
        }
    /* ------------------------------------------------------------------------------------------------------------------------------- */

/*    if (BEND_DETECT==1) tempB7=WL->slip_using_side_vref;  /added due to EBD at cornering  dolly 05.01.05
    else tempB7=WL->rel_lam; */

    if((vref > VREF_10_KPH) && ((vref-WL->vrad)>=decel_thres)) {
/*       if ((tempB7<=(int8_t)(-slip_thres))||(WL->front_rear_speed_percent<=(int8_t)(-slip_thres))) {   */
        if ((WL->rel_lam<=(int8_t)(-slip_thres))||(front_rear_speed_percent<=(int8_t)(-slip_thres))) {
            if (WL->arad<=(int8_t)(-arad_thres)) {
                if ((avr_rspd-avr_fspd)>=0) {
                    WL->flags=174;
                    WL->EBD_MODE=HOLD_MODE;
                }
                else {
                    WL->flags=172;
                    WL->EBD_MODE=HOLD_MODE;
                }
            }
        }
    }

    if (WL->EBD_MODE==0)
    {
     #if __VDC
       #if __BRK_SIG_MPS_TO_PEDAL_SEN
        if((lcabsu1ValidPedalTravel==1) || (lcabsu1ValidMpress==1))
       #else
         #if (__BLS_NO_WLAMP_FM==ENABLE)
        if((fu1McpSenPwr1sOk==1)&&(fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(mpress > BRK_ON_PRESS_NO_OFFSET_COR)) /*B201, due to BS/BLS patch, mpress condition is added*/
	     #else
        if(lcabsu1ValidMpress==1)
         #endif
       #endif
        {
            tempW8 = S16_EBD_KNEE_POINT;

            if(tempW8 > MPRESS_150BAR)
            {
                tempW8 = MPRESS_150BAR;
            }
            else if(tempW8 < MPRESS_50BAR)
            {
                tempW8 = MPRESS_50BAR;
            }
            else {}
            
            if(((lcabss16RefCircuitPress > tempW8)&&((RL.s16_Estimated_Active_Press>tempW8)&&(RR.s16_Estimated_Active_Press>tempW8))) && (vref > VREF_30_KPH))
            {
                if((ebd_filt_grv < -AFZ_0G3) &&
                  (((vref-WL->vrad) >= VREF_1_KPH) || (WL->rel_lam <= -LAM_1P)) && (WL->arad < ARAD_0G0))
                {
                    EBD_BY_MPRESS=1;
                }
                else
                {
                    EBD_BY_MPRESS=0;
                }
            }
            else
            {
                EBD_BY_MPRESS=0;
            }
        }
        else
        {
            EBD_BY_MPRESS=0;
        }

        if(EBD_BY_MPRESS==1)
        {
            WL->flags=253;
            WL->EBD_MODE=HOLD_MODE;
        }
        else
     #endif
        {
            WL->flags=183;
            WL->EBD_MODE=REAPPLY_MODE;
        }
    }
}

#endif /*(__REORGANIZE_EBD_ENTRY_DECISION==ENABLE)*/

int8_t LCEBD_s8CalcRefParameters(int8_t input,int8_t front_gain)
{
    if(ebd_filt_grv<=-70){
        tempB1=input-front_gain;
    }
    else if (ebd_filt_grv<=-50){
        tempB1=input-front_gain;
    }
    else if(ebd_filt_grv<=-30){
        tempB1=input-front_gain;
    }
    else if(ebd_filt_grv<=-15){
        front_gain=(int8_t)((int16_t)front_gain/2);
        tempB1=(input*2)-front_gain;
    }
    else {
        tempB1=input*3;
    }

    if(tempB1<1) {tempB1=1;}

    return tempB1;
}

void LCEBD_vDecideContinueControl(void)
{
    tempW3 = (int16_t)(((int32_t)(WL->vrad-avr_fspd)*800)/WL->vrad);
    tempW3 = LCABS_s16LimitMinMax(tempW3,-800,800);
        
    if (WL->EBD_Dump_counter==0) {
        if (WL->High_dump_factor>=(int16_t)S16_EBD_Dump_HDF_ref) {
            WL->flags=188;
            WL->EBD_MODE=DUMP_START;
        }
    }
    else {
        if (WL->High_dump_factor>=((int16_t)S16_EBD_Dump_HDF_ref+S16_EBD_Hold_Dump_ref)) {
            if ((vref-WL->vrad)>=VREF_3_KPH)
            {
            	tempF0=0;
                if(Rough_road_suspect_vehicle==1)
                {
                    if((WL->rel_lam > -LAM_60P)&&(WL->s_diff > -S16_SDIFF_12G0))
                    {
                        if(WL->Dump_hold_scan_counter<L_TIME_20MS) {tempF0=1;}
                    }
                    else
                    {
                        if(WL->Dump_hold_scan_counter<L_TIME_10MS) {tempF0=1;}
                    }
                }
                else if(Rough_road_detect_vehicle==1)
                {
                    if((WL->rel_lam > -LAM_60P)&&(WL->s_diff > -S16_SDIFF_12G0))
                    {
                        if(WL->Dump_hold_scan_counter<L_TIME_40MS) {tempF0=1;}
                    }
                    else{
                        if(WL->Dump_hold_scan_counter<L_TIME_20MS) {tempF0=1;}
                    }
                }
                else {tempF0=0;}

                if(tempF0==1) /* rough road */
                {
                    WL->flags=203;
                    WL->EBD_MODE=HOLD_MODE;
                }
                else
                {
                    WL->flags=189;
                    WL->EBD_MODE=DUMP_START;        /* continue dump */
                }
            }
            else
            {
                ;
            }
        }
    }

    if (WL->EBD_MODE==0) {
        if((SLIP_2P_NEG_fl==0) && (SLIP_2P_NEG_fr==0)) {
            if(vref > (int16_t)VREF_60_KPH) {
                tempW4=-12;
            }
            else {
                tempW4=-20;
            }
            if ((tempW3>tempW4)&&(ebd_refilt_grv<=-AFZ_0G7)) {
              #if __EBD_MI_ENABLE
                if(ebd_refilt_grv < -AFZ_0G9) /* 030903 g별 pulse up tuning */
                {
                    lcebdu8temp0 = U8_EBD_HOLD_TIMER/3;
                }
                else if(ebd_refilt_grv < -AFZ_0G8)
                {
                    lcebdu8temp0 = U8_EBD_HOLD_TIMER/2;
                }
                else
                {
                    lcebdu8temp0 = U8_EBD_HOLD_TIMER;
                }
                if(lcebdu8temp0 < L_TIME_30MS)
                {
                    lcebdu8temp0=L_TIME_30MS;
                }
                
                LCEBD_vWPresModeEBDPULSEUP(lcebdu8temp0);
              #else
                LCEBD_vWPresModeEBDSlowPULSEUP();
              #endif
            }
        }

        if (WL->EBD_MODE==0){
            WL->flags=191;
            WL->EBD_MODE=HOLD_MODE;
        }
    }
}

void LCEBD_vDecideEndControl(void)
{
    int8_t EBD_exit_thres = 0;
    int8_t EBD_cont_thres = 0;
    uint8_t EBD_end_timer;

    if(ebd_filt_grv > -AFZ_0G15)
    {
        if(WL->s_diff_pos_timer < U8_TYPE_MAXNUM)
        {
            WL->s_diff_pos_timer = WL->s_diff_pos_timer + L_1SCAN;
        }
        else {}

      #if (__TCS || __ETC)
        if((mtp>5)&&(lcabsu1ValidCan==1))
        {
            if (vref<(int16_t)VREF_40_KPH)
            {
                EBD_exit_thres=LAM_10P;
                EBD_cont_thres=LAM_20P;
            }
            else
            {
                EBD_exit_thres=LAM_5P;
                EBD_cont_thres=LAM_10P;
            }

            if(WL->rel_lam>(-EBD_exit_thres))
            {
                if(WL->EBD_prexit_slip_counter < L_TIME_140MS) {WL->EBD_prexit_slip_counter=WL->EBD_prexit_slip_counter+L_1SCAN;}
            }
            else if(WL->rel_lam<(-EBD_cont_thres))
            {
                if(WL->EBD_prexit_slip_counter > 0) {WL->EBD_prexit_slip_counter=WL->EBD_prexit_slip_counter-L_1SCAN;}
            }
            else {}
        }
        else
        {
            WL->EBD_prexit_slip_counter=0;
        }
      #else
        WL->EBD_prexit_slip_counter=0;
      #endif
    }
    else
    {
        WL->s_diff_pos_timer=0;
        WL->EBD_prexit_slip_counter=0;
    }

  #if __VDC
    if(((ebd_filt_grv > -AFZ_0G3) &&
      ((MP_RELEASE_BW_4BAR==1)||(MP_RELEASE_BW_6BAR==1)||(MP_RELEASE_BW_10BAR==1))) || (MP_RELEASE_BW_SKID==1))
    {
        if(WL->EBD_MP_Release_counter < U8_TYPE_MAXNUM)
        {
            WL->EBD_MP_Release_counter=WL->EBD_MP_Release_counter+L_1SCAN;
        }
        else {}
    }
    else
    {
        WL->EBD_MP_Release_counter=0;
    }

	if(MP_RELEASE_BW_SKID==1)
	{
		EBD_end_timer = L_TIME_100MS;
	}
    else if(MP_RELEASE_BW_4BAR==1)
    {
        EBD_end_timer = L_TIME_150MS;
    }
    else if(MP_RELEASE_BW_6BAR==1)
    {
        EBD_end_timer = L_TIME_200MS;
    }
    else if(MP_RELEASE_BW_10BAR==1)
    {
        EBD_end_timer = L_TIME_250MS;
    }
    else
    {
        EBD_end_timer = L_TIME_1200MS;
    }
  #endif

    if((EBD_OFF==1)&&(lcabsu1BrakeByBLS==0))
    {
        WL->flags=184;
        WL->EBD_MODE=REAPPLY_MODE;  /* 5 */
    }
    else if ((WL->EBD_prexit_slip_counter>L_TIME_110MS)
      #if __VDC
        ||(WL->EBD_MP_Release_counter>EBD_end_timer)
      #endif
    )
    {
        WL->flags=182;
        WL->EBD_MODE=REAPPLY_MODE;   /* 7 */
    }
    else
    {
   /*     if (vref<=(int16_t)VREF_3_KPH) { */
        if (vref<=(int16_t)VREF_5_KPH)
        {   /* Accel to ABS rear 이상제어 2005.08.01 NZ */
            if(ebd_off_timer==0)
            {
                WL->flags=185;
                WL->EBD_MODE=REAPPLY_MODE;   /* 6 */
            }
            else
            {
                ebd_off_timer=ebd_off_timer-L_1SCAN;
            #if __EBD_LFC_MODE
                if(ebd_off_timer<LFC_RISE_TIME_AT_EBD_END)
                {
                    WL->EBD_LFC_RISE_MODE=1;  /* ebd_lfc_rise */
                  #if __EBD_MI_ENABLE
                    LCEBD_vWPresModeEBDPULSEUP(L_TIME_30MS);
                  #else /*__EBD_MI_ENABLE*/
                    if(WL->hold_timer_new>L_TIME_30MS)
                    {
                        WL->hold_timer_new=0;
                    }
                    LCEBD_vWPresModeEBDFastPULSEUP();
                  #endif /*__EBD_MI_ENABLE*/
                }
                else
            #endif
                {
                    WL->flags=186;
                    WL->EBD_MODE=HOLD_MODE;
                }
            }
        }
        else {
            if(ABS_fz==0)
            {
                if ((WL->s_diff_pos_timer>L_TIME_140MS)||(lcebdu1DoubleBrake==1))
                {
                    if (avr_fspd>=(int16_t)VREF_3_KPH)
                    {
                        WL->flags=187;
                        WL->EBD_MODE=REAPPLY_MODE;   /* 7 */
                    }
                    else {}
                }

                else {}
              #if(__ROUGH_COMP_IMPROVE==ENABLE)
                if((bls_k_timer > L_U8_TIME_10MSLOOP_20MS) 
			      #if (__CONSIDER_BLS_FAULT == ENABLE)
			    &&(lcabsu1FaultBLS==0)
			      #endif    
			    )            
                {       
    	    		if(lcebdu1RoughSlightBrk==1)
    		   		{
    		   			WL->EBD_MODE=REAPPLY_MODE; 
    		   			WL->flags=181;
    		   		}
    		   		else {}   
				}
			  #endif
			}
			else {}
        }
    }
}

void LCEBD_vWPresModeEBDREAPPLY(void)
{
	WL->WheelCtrlMode = WHEEL_CTRL_CBS;
    AV_REAPPLY_wl = 1;
    AV_DUMP_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 0;
    ENT_ABS_wl=0;
    EBD_wl=0;                           /*  MAIN03D */
    WL->hold_timer_new = 0;
    WL->s0_reapply_counter = 0;
/*    WL->holding_time=0;
     WL->p_hold_time=0; */

    WL->EBD_Dump_counter=0;             /* 030923 ebd noise */
    WL->Dump_hold_scan_counter=0;
    WL->state=DETECTION;
}

void LCEBD_vWPresModeEBDBUILTREAPPLY(void)
{
	WL->WheelCtrlMode = WHEEL_CTRL_EBD;
/* #if __PWM */ /* 2006.06.15  K.G.Y */
  #if __EBD_LFC_MODE
	#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
	#else
    if(WL->EBD_LFC_RISE_MODE==1)
    {
        WL->LFC_Conven_OnOff_Flag=0;
    }
    else
    {
        WL->LFC_Conven_OnOff_Flag=1; /*  Added by CSH for LFC(02.01.11) */
    }
	#endif
  #else
    WL->LFC_Conven_OnOff_Flag=1; /*  Added by CSH for LFC(02.01.11) */
  #endif
/* #endif */ /* 2006.06.15  K.G.Y */

    AV_REAPPLY_wl=1;
    AV_DUMP_wl=0;
    AV_HOLD_wl=0;
    BUILT_wl=1;
    EBD_wl=1;

#if __EBD_LFC_MODE
    WL->s0_reapply_counter++;                   /* ebd_lfc_rise */
#else
    if(ABS_fz==1) {WL->s0_reapply_counter++;}
#endif
/*     WL->p_hold_time=0; */
    WL->Dump_hold_scan_counter=0;

  #if __EBD_MI_ENABLE
    WL->hold_timer_new=0;
  #endif

 #if __HRB
    if(lchrbu1ActiveFlag==1)
    {
        if(WL->lchrbu8PulseUpCounter <= U8_HRB_MAX_PULSE_UP)
        {
            WL->lchrbu8PulseUpCounter = WL->lchrbu8PulseUpCounter + 1;
        }
        else { }
    }
    else
    {
        WL->lchrbu8PulseUpCounter = 0;
    }
  #endif

    WL->state=DETECTION;
}

static void LCEBD_vWPresModeEBDHOLD(void)
{
	WL->WheelCtrlMode = WHEEL_CTRL_EBD;
/* #if __PWM */ /* 2006.06.15  K.G.Y */
  #if __EBD_LFC_MODE
	#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
	#else
    if(WL->EBD_LFC_RISE_MODE==1)
    {
        WL->LFC_Conven_OnOff_Flag=0;
    }
    else
    {
        WL->LFC_Conven_OnOff_Flag=1; /*  Added by CSH for LFC(02.01.11) */
    }
	#endif
  #else
    WL->LFC_Conven_OnOff_Flag=1; /*  Added by CSH for LFC(02.01.11) */
  #endif
/* #endif */ /* 2006.06.15  K.G.Y */

    if(EBD_wl==0)
    {
        ebd_off_timer=((uint16_t)U8_EBD_STANDSTILL_HOLD_TIME*L_TIME_100MS)+LFC_RISE_TIME_AT_EBD_END; 
                      /* ((resolution change to scans)*(2 loops)) ~= 29; LFC_RISE_TIME_AT_EBD_END is the time needed for EBD LFC rise */
    /*   WL->p_hold_time=0; */
        if(lcabsu1BrakeByBLS==1) {EBD_OFF=1;}
    }
    /*    else WL->p_hold_time++; */

    AV_HOLD_wl=1;
    AV_REAPPLY_wl=0;
    AV_DUMP_wl=0;
    BUILT_wl=0;
    EBD_wl=1;
    if(WL->Dump_hold_scan_counter < U8_TYPE_MAXNUM)
    {
        WL->Dump_hold_scan_counter = WL->Dump_hold_scan_counter + L_1SCAN;
    }
    
  #if __EBD_MI_ENABLE
    WL->hold_timer_new = WL->hold_timer_new + L_1SCAN;
  #endif
    
    WL->state=DETECTION;
}

static void LCEBD_vWPresModeEBDDUMP(void) 
{
	WL->WheelCtrlMode = WHEEL_CTRL_EBD;
/* #if __PWM */ /* 2006.06.15  K.G.Y */
    WL->LFC_Conven_OnOff_Flag=1; /*  Added by CSH for LFC(02.01.11) */
/* #endif */ /* 2006.06.15  K.G.Y */

    AV_HOLD_wl=0;
    AV_REAPPLY_wl=0;
    AV_DUMP_wl=1;
    BUILT_wl=0;
    EBD_wl=1;

  #if (L_CONTROL_PERIOD>=2)
    WL->lcabss8DumpCase = U8_L_DUMP_CASE_10;
  #endif

    if(WL->EBD_Dump_counter < 1)
    {
    	WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
    	WL->lcabsu1CaptureEstPressForSkid = 1;
    }
    else {}

    if(WL->EBD_Dump_counter<U8_TYPE_MAXNUM)
    {
        WL->EBD_Dump_counter = WL->EBD_Dump_counter + 1;
    }

    WL->Dump_hold_scan_counter=0;
    WL->state=DETECTION;
    #if __EBD_LFC_MODE
    WL->s0_reapply_counter=0;   /* ebd_lfc_rise */
  #endif
  #if __EBD_MI_ENABLE
    WL->hold_timer_new=0;
  #endif
}

  #if __EBD_MI_ENABLE
static void LCEBD_vWPresModeEBDPULSEUP(uint8_t Hold_Reference)
{
    if(WL->hold_timer_new >= Hold_Reference)
    {
        WL->flags=196;
        WL->EBD_MODE=BUILT_MODE;
    }
    else
    {
        WL->flags=195;
        WL->EBD_MODE=HOLD_MODE;
    }
}
  #else /*#if __EBD_MI_ENABLE*/
void LCEBD_vWPresModeEBDFastPULSEUP(void)
{
    if(WL->pulse_up_row_timer==0)
    {
        if(WL->hold_timer_new==0)
    {
    #if __EBD_LFC_MODE
            if(WL->EBD_LFC_RISE_MODE==1)
            {
                WL->hold_timer_new=L_TIME_30MS;
            }
            else
    #endif
    #if __HRB
            if(lchrbu1ActiveFlag==1)
            {
                WL->hold_timer_new=lchrbu8PulseUpHoldScans;
            }
            else
    #endif
            {
                WL->hold_timer_new=U8_EBD_PULSEUP_HOLD_TIME_DEF;
            }

            WL->pulse_up_row_timer=1;
        }
    }

    if(WL->pulse_up_row_timer==0)
    {
        WL->hold_timer_new = WL->hold_timer_new - L_1SCAN;
        WL->flags=195;
        WL->EBD_MODE=HOLD_MODE;
    }
    else
    {
        WL->pulse_up_row_timer--;
        WL->flags=196;
        WL->EBD_MODE=BUILT_MODE;
    }
}

void LCEBD_vWPresModeEBDSlowPULSEUP(void)   /* 1sec hold 후 */
{
    if(WL->hold_timer_new==0)
    {
        WL->hold_timer_new=U8_EBD_HOLD_TIMER;/* EBD_Hold_timer;/143 ebd test(030407) */
    }

    if(WL->hold_timer_new==L_1ST_SCAN)
    {
        if(WL->THREE_SEC_FLG==0)
        {               /* long hold 구현 */
            WL->THREE_SEC_FLG=1;
            WL->hold_timer_new=U8_EBD_HOLD_TIMER;/* EBD_Hold_timer; */
            WL->EBD_MODE=HOLD_MODE;
        }
        else
        {
            WL->THREE_SEC_FLG=0;
            WL->hold_timer_new=U8_EBD_HOLD_TIMER; /* EBD_Hold_timer; */
            WL->flags=197;
            WL->EBD_MODE=BUILT_MODE;
        }
    }
    else {
        if(ebd_refilt_grv < -AFZ_0G8)
        {
            if(WL->hold_timer_new>L_2SCAN)
            {
                WL->hold_timer_new  = WL->hold_timer_new - L_2SCAN;
            }
            else
            {
                WL->hold_timer_new = L_1SCAN;
            }
        }
        else
        {
            WL->hold_timer_new = WL->hold_timer_new - L_1SCAN;
        }

        WL->flags=198;
        WL->EBD_MODE=HOLD_MODE;
    }
}

  #endif /*#if __EBD_MI_ENABLE*/

void LCEBD_vDecideEBDCtrlState(void)
{
  #if __EBD_MI_ENABLE
    LCEBD_vDecideMICtrl();
  #endif    

    if((ABS_rl==0) && (RL.state==DETECTION) && (ABS_rr==0) && (RR.state==DETECTION))
    {
		if((EBD_rl==1) || (EBD_rr==1))
		{
		    EBD_RA=1;
		}
		else
		{
			EBD_RA=EBD_OFF=0;
		}

		if(ABS_fz==0)
		{
		   #if __GM_FailM
		    if((lcabsu1AbsErrorFlag==1)&&(fu1RearWSSErrDet==1))
		   #else
		    if(lcabsu1AbsErrorFlag==1)
		   #endif
			{
				if((AV_DUMP_rl==1) || (AV_DUMP_rr==1))
				{
					AV_DUMP_rr=AV_DUMP_rl=1;
					AV_HOLD_rr=AV_HOLD_rl=0;
					AV_REAPPLY_rr=AV_REAPPLY_rl=0;
					BUILT_rr=BUILT_rl=0;
				}
			}
			if(BEND_DETECT2==0)
			{
				if((EBD_rl==1) || (EBD_rr==1))
				{
					if(EBD_DETN==0)
					{
						EBD_DETN=1;
						EBD_rl=EBD_rr=1;
						AV_DUMP_rl=AV_DUMP_rr=0;
						AV_HOLD_rl=AV_HOLD_rr=1;
						RL.WheelCtrlMode = WHEEL_CTRL_EBD;
						RR.WheelCtrlMode = WHEEL_CTRL_EBD;
						AV_REAPPLY_rl=AV_REAPPLY_rr=0;
					}
				}
				else
				{
				    EBD_DETN=0;
				}
			}
		}
		else
		{
		    EBD_DETN=0;
		}
	}
	else
	{
        EBD_rl=EBD_rr=0;
        EBD_RA=EBD_OFF=0;
	}
}


#if __EBD_MI_ENABLE
static void LCEBD_vDecideMICtrl(void)
{
    if((EBD_rl==0) && (EBD_rr==0))
    {
        RL.EBDMICtrlMode=0;
        RR.EBDMICtrlMode=0;
    }
    else
    {
        if(MSL_BASE_rl==1)
        {
            LCEBD_vDecideMICtrlMode(&RR, &RL);
            LCEBD_vMICtrl(&RR, &RL);
        }
        else if(MSL_BASE_rr==1)
        {
            LCEBD_vDecideMICtrlMode(&RL, &RR);
            LCEBD_vMICtrl(&RL, &RR);
        }
        else { }
    }
}

static void LCEBD_vDecideMICtrlMode(struct W_STRUCT *pRH, struct W_STRUCT *pRL)
{
    int8_t EBD_MI_DUMP_SLIP_REF;
    int16_t MI_DUMP_ALLOW_SPEED;
    int16_t MI_ALLOW_SPEED;
    int16_t MI_DUMP_MAX_SPEED;
    
  /*------------------- MI Allow Speed --------------------*/
    if(BEND_DETECT2==1)
    {
        MI_DUMP_ALLOW_SPEED = S16_EBD_MI_DUMP_ALLOW_SPEED + VREF_20_KPH;
        MI_DUMP_MAX_SPEED = S16_EBD_MI_DUMP_MAX_SPEED + VREF_20_KPH;
        MI_ALLOW_SPEED = S16_EBD_MI_ALLOW_SPEED + VREF_20_KPH;
    }
    else
    {
        MI_DUMP_ALLOW_SPEED = S16_EBD_MI_DUMP_ALLOW_SPEED;
        MI_DUMP_MAX_SPEED = S16_EBD_MI_DUMP_MAX_SPEED;
        MI_ALLOW_SPEED = S16_EBD_MI_ALLOW_SPEED;
    }
    

  /*------------------- Decide EBD MI Mode --------------------*/
    pRL->EBDMICtrlMode=0;

    if((Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0))
    {
        if((pRL->EBD==1) && ((pRL->EBD_MODE==BUILT_MODE)||(pRL->EBD_Dump_counter==0)) && (pRL->state==DETECTION))
        {
            pRH->EBDMICtrlMode=0;
        }
        else
        {
            tempB0 = pRL->rel_lam - pRH->rel_lam;

            EBD_MI_DUMP_SLIP_REF = (int8_t)LCABS_s16Interpolation2P(vref,MI_DUMP_ALLOW_SPEED,VREF_120_KPH,LAM_3P,LAM_1P);
            
            if(BEND_DETECT2==1) /* Very tight references for bend */
            {
                EBD_MI_DUMP_SLIP_REF = EBD_MI_DUMP_SLIP_REF + LAM_10P;
            }

            if(vref > MI_ALLOW_SPEED)
            {
                if((vref > MI_DUMP_ALLOW_SPEED) && (tempB0 < (-EBD_MI_DUMP_SLIP_REF)))
                {
                    pRH->EBDMICtrlMode=(uint8_t)LCABS_s16LimitMinMax((int16_t)pRH->EBDMICtrlMode,MI_LEVEL2,(int16_t)pRH->EBDMICtrlMode);
                }
                else
                {
                    pRH->EBDMICtrlMode=(uint8_t)LCABS_s16LimitMinMax((int16_t)pRH->EBDMICtrlMode,MI_LEVEL1,(int16_t)pRH->EBDMICtrlMode);
                }
            }
            else
            {
                pRH->EBDMICtrlMode=0;
            }
        }
    }
    else
    {
        pRH->EBDMICtrlMode=0;
    }
    
    
  /*------------------- Calc MI pulse-up rate --------------------*/
    if(BEND_DETECT2==1)
    {
        if(pRH->EBDMICtrlMode>=MI_LEVEL2)
        {
            lcebdu8AdRiseHoldScansForMI=(uint8_t)LCABS_s16Interpolation2P(vref,MI_ALLOW_SPEED,VREF_140_KPH,U8_EBD_PULSEUP_HOLD_TIME_DEF,L_TIME_220MS);
        }
        else if(pRH->EBDMICtrlMode>=MI_LEVEL1)
        {
            lcebdu8AdRiseHoldScansForMI=(uint8_t)LCABS_s16Interpolation2P(vref,MI_ALLOW_SPEED,VREF_140_KPH,U8_EBD_PULSEUP_HOLD_TIME_DEF,L_TIME_150MS);
        }
        else { }
    }
    else
    {
        if(pRH->EBDMICtrlMode>=MI_LEVEL2)
        {
            lcebdu8AdRiseHoldScansForMI=(uint8_t)LCABS_s16Interpolation2P(vref,MI_ALLOW_SPEED,VREF_120_KPH,U8_EBD_PULSEUP_HOLD_TIME_DEF,L_TIME_290MS);
        }
        else if(pRH->EBDMICtrlMode>=MI_LEVEL1)
        {
            lcebdu8AdRiseHoldScansForMI=(uint8_t)LCABS_s16Interpolation2P(vref,MI_ALLOW_SPEED,VREF_120_KPH,U8_EBD_PULSEUP_HOLD_TIME_DEF,L_TIME_150MS);
        }
        else { }
    }
    
    if((pRL->ABS==1)&&(pRL->state==UNSTABLE))
    {
        lcebdu8AdRiseHoldScansForMI = lcebdu8AdRiseHoldScansForMI*2;
    }

  /*------------------- Calc MI Dump scans --------------------*/
    lcebdu8MIDumpScans = (uint8_t)LCABS_s16Interpolation2P(vref,MI_DUMP_ALLOW_SPEED,MI_DUMP_MAX_SPEED,EBD_MI_DUMP_MIN,EBD_MI_DUMP_MAX);
    /* later to consider slip difference */
}

static void LCEBD_vMICtrl(struct W_STRUCT *pRH, const struct W_STRUCT *pRL)
{
    if(pRH->EBDMICtrlMode>=MI_LEVEL1) /* MI mode enabled */
    {
/*
//        if((pRL->ABS==1) && (pRL->EBD==1)) // MI 1st scan -- avoid 1 scan delay
//        {
//            if(pRH->EBD_MODE==BUILT_MODE) // High-side pulse-up in MI 1st scan -- must wait, not rise 
//            {
//                pRH->flags=178;
//    		    LCEBD_vWPresModeEBDHOLD();
//            }
//        }
*/

      #if __EBD_MI_DUMP_ENABLE
        if(pRH->EBDMICtrlMode>=MI_LEVEL2) /* MI dump mode */
        {
            if(pRL->AV_DUMP==1)
            {
                if(pRH->EBD_MODE==DUMP_START) /* if High-side has already dump command, then continue */
                {
                    ;
                }
                else
                {
                    if((pRH->EBD_Dump_counter<lcebdu8MIDumpScans)&&(pRH->EBD_Dump_counter<(pRL->EBD_Dump_counter + pRL->LFC_dump_counter)))
                    {
                        pRH->flags=179;
            		    LCEBD_vWPresModeEBDDUMP();
                    }
                    else
                    {
                        ;
                    }
                }
            }
        }
      #endif /* __EBD_MI_DUMP_ENABLE*/
    }
}
#endif /* __EBD_MI_ENABLE*/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCEBDCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
