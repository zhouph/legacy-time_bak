/***************************************************************************

*  Program ID: MGH-40 RTA
*  Program description:. Wheel Tolerance Compensation
*  Input files:
*  Output files:
*
*
*  Special notes: none
***************************************************************************
*  Modification	Log
*  Date		   Author			Description
*  ---		   ------			---------
*  05.11.20	   Sohyun Ahn		Initial	Release
***************************************************************************/

/* Includes	*****************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDRTACallDetection
	#include "Mdyn_autosar.h"
#endif
#include	"LVarHead.h"
#include	"LDRTACallDetection.h"
#include	"LSABSCallSensorSignal.H"
#include	"LDABSCallDctForVref.H"
#include	"LDABSCallVrefEst.H"
#include	"LDABSCallEstVehDecel.H"
#include	"LDABSCallVrefMainFilterProcess.H"

#if	__VDC
#include	"LDABSCallVrefCompForESP.H"
#endif

#include	"LCallMain.h"
#include	"LCABSCallControl.h"

//#include	"../Appl/APEspModelForm.h"

/* Local Definition**********************************************************/

#if	__RTA_ENABLE

#define	KNOWN		1
#define	UNKNOWN		0
#define	ABSCENCE	0
#define	PRESENCE	1

#define	RTA_ABSENCE_DET_RATIO	S16_RTA_ABSENCE_DET_RATIO

#define	RTA_COUNTER_MAX					T_1MIN
#define	RTA_DET_MAX_VEHICLE_ACCEL		AFZ_1G0
#define	RTA_DET_MAX_WHEEL_ACCEL_DIFF	AFZ_0G2


#define	RTA_VRAD_DIFF_MAX	VREF_1_5_KPH_RESOL_CHANGE
#define	RTA_RESET_SPEED		VREF_2_KPH_RESOL_CHANGE
#define	RTA_START_SPEED		VREF_5_KPH_RESOL_CHANGE


#define	RTA_DET_THRES		S16_RTA_DET_THRES
#define	RTA_DET_THRES_MAX	S16_RTA_DET_THRES_MAX
#define	RTA_COMP_RATIO_MIN	S16_RTA_COMP_RATIO_MIN
#define	RTA_COMP_RATIO_MAX	S16_RTA_COMP_RATIO_MAX



#define	RTA_DET_TIME	S16_RTA_DET_TIME /*3s->1.5s	due	to 2 scan cnt*/
#define	RTA_RESET_TIME	S16_RTA_RESET_TIME /*3s->1.5s due to 2 scan	cnt*/
#define	RTA_RESET_TIME2	S16_RTA_RESET_TIME2	   /*1s->500ms*/
	
#define	RTA_STATE_CONFIRM_TIME			S16_RTA_STATE_CONFIRM_TIME	 /*10s->5s due to 3	scan cnt*/
#define	RTA_STATE_UNKNOWN				0
#define	RTA_STATE_ABSENCE				1
#define	RTA_STATE_PRESENCE				2
#define	VDIFF_THR_FOR_RTA_STATE_CONFIRM	VREF_1_KPH_RESOL_CHANGE
#define	WHL_STABLE_DET_TIME_FOR_RTA		3 

#define	RTA_DET_BUF_TIMER_MAX 42 /*RTA_DET_TIME/5*/

		
#if	__RTA_2WHEEL_DCT
#define	MAX_WHL_DIFF_FOR_RTA2WHL_CLR	VREF_2_KPH_RESOL_CHANGE
#define	RTA_COMP_RATIO_MAX_DEV			20


/* == 2009.05.18 Tuning	Parameters ==
#define		RTA_2WHL_DET_TIME		T_10_S
#define		RTA_2WHL_RESET_TIME		T_3_S

#if	__CAR==GM_M300
#define		MAX_ENGINE_TORQ_RTA2WHL			800
#elif __CAR==GM_T300
#define		MAX_ENGINE_TORQ_RTA2WHL			800
#elif __CAR==KMC_HM
#define		MAX_ENGINE_TORQ_RTA2WHL			1400
#elif __CAR==PSA_C4
#define		MAX_ENGINE_TORQ_RTA2WHL			700
#else
#define		MAX_ENGINE_TORQ_RTA2WHL			700
#endif
*/
#endif

#define	RTA_SUS_DET_TIME			S16_RTA_SUS_DET_TIME
#define	RTA_SUS_CLEAR_TIME			S16_RTA_SUS_CLEAR_TIME
#define	RTA_SUS_CLEAR_TIME2			S16_RTA_SUS_CLEAR_TIME2
#define	CAR_STOP_STATE_DET_TIME		S16_CAR_STOP_STATE_DET_TIME

#define	HIGH_SPEED_FOR_RTA	VREF_100_KPH_RESOL_CHANGE

#endif
/*******************************************************************/
/***TEMP***/
/*
#define	S16_RTA_ABSENCE_DET_RATIO	20	//*2%
#define	S16_RTA_DET_THRES			30	//*3%
#define	S16_RTA_DET_THRES_MAX		300	//*30%
#define	S16_RTA_COMP_RATIO_MIN		30	//*3%
#define	S16_RTA_COMP_RATIO_MAX		300	//*30%
#define	S16_RTA_DET_TIME			L_U16_TIME_10MSLOOP_3S		//*3S_10m
#define	S16_RTA_RESET_TIME			L_U16_TIME_10MSLOOP_3S		//*3S_10m
#define	S16_RTA_RESET_TIME2			L_U16_TIME_10MSLOOP_10S		//*10S_10m	   
#define	S16_RTA_STATE_CONFIRM_TIME	L_U16_TIME_10MSLOOP_3S		//*6S_20m
#define	S16_RTA_SUS_DET_TIME		L_U8_TIME_10MSLOOP_1500MS	//*1500ms_10m
#define	S16_RTA_SUS_CLEAR_TIME		L_U16_TIME_10MSLOOP_3S		//*3S_10m
#define	S16_RTA_SUS_CLEAR_TIME2		L_U16_TIME_10MSLOOP_10S		//*10S_10m
#define	S16_CAR_STOP_STATE_DET_TIME	180							//*3min_10m
*/
/* Variables Definition*********************************************/
	#if	__RTA_ENABLE
	U8_BIT_STRUCT_t	RTA_1;
	U8_BIT_STRUCT_t	RTA_2;
	struct	RTA_STRUCT		*RTA_WL,RTA_FL,RTA_FR,RTA_RL,RTA_RR;

	int16_t	rta_ratio;
	int16_t	rta_ratio_temp;	/* MBD ETCS */
	int16_t	rta_ratio_old;
	int16_t	rta_ratio_raw;
	int16_t	rta_ratio_raw_old;
	int16_t	rta_ratio_calc_sum_0;
/*
	int16_t	rta_ratio_calc_sum_1;
	int16_t	rta_ratio_calc_sum_2;
	int16_t	rta_ratio_calc_sum_3;
	int16_t	rta_ratio_calc_sum_4;
	int16_t	rta_ratio_calc_sum_5;
*/
	uint16_t	rta_reset_speed_cnt;
	int16_t		rta_ratio_calc_sum_array[5];	
	
	int8_t	rta_ratio_calc_timer_1;
	int8_t	rta_ratio_calc_timer_2;
	
	uint8_t	ldrtau8RtaResetSpeed1SecCnt;
	uint8_t	rta_calc_cnt;

	uint8_t	rta_ratio_conv_cnt;
	int16_t	rta_ratio_avg_old;
	int16_t	rta_ratio_avg;
	int16_t	rta_ratio_rest;

	#if	__VDC
	uint16_t	rta_whl_absence_confirm_cnt;
	#endif


/*		  #if __GM_RFQ	*/
	uint16_t	ldrtau16RtaAbsnConfirmCnt;
	uint16_t	ldrtau16RtaPresConfirmCnt;
	uint8_t		ldrtau8RtaState;
	int16_t		ldrtas16WssInvalidTimerForRTA;
/*		#endif				  */
	#if	__RTA_2WHEEL_DCT

	int16_t	ldrtas16Rta2WhlDctRef;
	int16_t	ldrtas16Rta2WhlCompCalcRef;
	int16_t	ldrtas16Rta2WhlClearRef;

	uint8_t	ldrtau8Rta2WhlCompTimer1[4];
	uint8_t	ldrtau8Rta2WhlCompTimer2[4];

	uint16_t	ldrtau16RtaRatioCalcSum0[4];
	uint16_t	ldrtau16RtaRatioCalcSum[4][5];

	int16_t	wheel_diff_2nd_3rd;
	int16_t	wheel_diff_3rd_min;
	int16_t	wheel_spd_max_temp;
	int16_t	wheel_spd_2nd_temp;
	int16_t	wheel_spd_3rd_temp;
	int16_t	wheel_spd_min_temp;
	uint8_t	rta_2dct_temp_flg;

	int16_t	rta_temp1;
	uint8_t	RTA_WL_idx;
	#endif /*__RTA_2WHEEL_DCT*/

	#endif /*__RTA_ENABLE*/
/*******************************************************************/

/* Local Function prototype	****************************************/
#if	__RTA_ENABLE
void	LDRTA_vCallRTA1WheelDetection(void);
void	LDRTA_vDetRTADetectionReliableState(void);
void	LDRTA_vDetRTASuspectWheel(struct RTA_STRUCT	*pRTA_WL,struct	W_STRUCT *pRTA_Det,struct W_STRUCT *pRTA_Side,struct W_STRUCT *pRTA_Axle,struct	W_STRUCT *pRTA_Diag);
void	LDRTA_vExeRTACompensation(struct W_STRUCT *WL,struct RTA_STRUCT	*RTA_WL);
void	LDRTA_vCalRTACompRatio(void);
int16_t	LDRTA_vCalDiffRatioWheels(int16_t wheel_speed, int16_t ref_speed);
int16_t	LDRTA_sCal3WhlAvg(int16_t v_speed,int16_t whl1,int16_t whl2,int16_t	whl3);

void	LDRTA_vConfirmRTAState(void);
void	LSRTA_vCallRTALogData(void);

int16_t	LDRTA_s16IncreaseRTASuscnt(int16_t rta_input_diff_ratio, int16_t rta_cnt);
int16_t	LDRTA_s16DecreaseRTASuscnt(int16_t rta_input_diff_ratio, int16_t rta_cnt);

#if	__RTA_2WHEEL_DCT
void	LDRTA_vCalRTA2WhlCompRatio(struct RTA_STRUCT *pRTA_WL,int16_t wheel_speed,int16_t rta_comp_Ref,int16_t rta_dct_ref,uint8_t WL_idx);
void	LDRTA_vDetRTA2WhlSuspect(struct	RTA_STRUCT *pRTA_WL,struct W_STRUCT	*pRTA_Det,int16_t rta_dct_Ref);
void	LDRTA_vCallRTA2WheelDetection(void);
void	LDABS_vArrangeRawWhlSpd(int16_t	INPUT_1,int16_t	INPUT_2,int16_t	INPUT_3,int16_t	INPUT_4);
#endif


#else	/*__RTA_ENABLE*/
void	LDABS_vCompWheelSpeedForMini(struct	W_STRUCT *WL);
void	LDABS_vDctMiniWheel(struct W_STRUCT	*pMini_Det,struct W_STRUCT *pMini_Ref);
#endif	/*__RTA_ENABLE*/
void	LDRTA_vCallDetection(void);


/****************************************************************************/
void LDRTA_vCallDetection(void)
{

	#if	__RTA_ENABLE
	LDRTA_vCallRTA1WheelDetection();

	#if	__RTA_2WHEEL_DCT
	LDRTA_vCallRTA2WheelDetection();
	#endif
	  	
	LDRTA_vConfirmRTAState();		


	LDRTA_vExeRTACompensation(&FL,&RTA_FL);
	LDRTA_vExeRTACompensation(&FR,&RTA_FR);
	LDRTA_vExeRTACompensation(&RL,&RTA_RL);
	LDRTA_vExeRTACompensation(&RR,&RTA_RR);

	#else
	LDABS_vDctMiniWheel(&FL,&RL);
	LDABS_vDctMiniWheel(&FR,&RR);
	LDABS_vDctMiniWheel(&RL,&FL);
	LDABS_vDctMiniWheel(&RR,&FR);

	LDABS_vCompWheelSpeedForMini(&FL);
	LDABS_vCompWheelSpeedForMini(&FR);
	LDABS_vCompWheelSpeedForMini(&RL);
	LDABS_vCompWheelSpeedForMini(&RR);
	#endif

}
/*===============================================================================*/
#if	__RTA_ENABLE
void LDRTA_vCallRTA1WheelDetection(void)
{
	LDRTA_vDetRTADetectionReliableState();
	

	LDRTA_vDetRTASuspectWheel(&RTA_FL,&FL,&RL,&FR,&RR);
	LDRTA_vDetRTASuspectWheel(&RTA_FR,&FR,&RR,&FL,&RL);

	LDRTA_vDetRTASuspectWheel(&RTA_RL,&RL,&FL,&RR,&FR);
	LDRTA_vDetRTASuspectWheel(&RTA_RR,&RR,&FR,&RL,&FL);
	
	
	LDRTA_vCalRTACompRatio();

}
/*===============================================================================*/
void LDRTA_vDetRTADetectionReliableState(void)
{

	/*--------------------------------------------------------------
	Determine whether it is	a suitable state for RTA determine or not
	--------------------------------------------------------------
	Steasy straight	driving	state
	no control is involved
	---------------------------------------------------------------*/
	RTA_RELIABLE_CONDITON_CHECK=1;
	if((ABS_fz==1)||(BRAKE_SIGNAL==1)||(McrAbs(afz)>AFZ_0G5)||(RTA_RESET_STATE==1))
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	else if((vref2 < (int16_t)VREF_15_KPH)
	#if	__CAR==GM_GSUV
	&&((vref2 <	(int16_t)VREF_10_KPH)||((wheel_speed_max-wheel_speed_2nd) <	VREF_1_5_KPH_RESOL_CHANGE))
	#endif
	)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}

/*
	else if(vref2 <	(int16_t)VREF_15_KPH)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	else if(BRAKE_SIGNAL==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	else if(McrAbs(afz)>AFZ_0G5)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
*/
  	#if __EDC
	else if((engine_state==0)||(EDC_ON==1))
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
/*
	else if
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
*/
	#endif

  	#if __BTC
	else if(BTCS_ON==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	#endif

/*
  	#if __TCS
	else if(TCS_ON==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
  	#endif
*/

  	#if (__4WD_VARIANT_CODE==ENABLE)
	else if	((ACCEL_SPIN_FZ==1)&&((lsu8DrvMode ==DM_AUTO)||(lsu8DrvMode	==DM_4H)))
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
  	#elif	__4WD
	else if	(ACCEL_SPIN_FZ==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
   	#endif


  	#if __PARKING_BRAKE_OPERATION
	else if	(PARKING_BRAKE_OPERATION==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	#if	__RTA_2WHEEL_DCT
	else if((RL.PARKING_BRAKE_WHEEL==1)||(RR.PARKING_BRAKE_WHEEL==1))
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
	#endif
  	#endif
/*
	else if(RTA_RESET_STATE==1)
	{
		RTA_RELIABLE_CONDITON_CHECK=0;
	}
*/
  	#if __VDC
	else if(ESP_SENSOR_RELIABLE_FLG==1)
	{
		if(VDC_REF_WORK==1)
		{
			RTA_RELIABLE_CONDITON_CHECK=0;
		}
		else if(McrAbs(delta_yaw_first)	>= YAW_3DEG)
		{
			RTA_RELIABLE_CONDITON_CHECK=0;
		}
		else if((McrAbs(yaw_out)>YAW_5DEG)||(McrAbs(wstr)>WSTR_60_DEG)||(McrAbs(alat)>LAT_0G15G))
		{
			RTA_RELIABLE_CONDITON_CHECK=0;
		}
		else
		{
			;
		}
	/*-----------------------------------------------------------
	Consider yaw_out_dot &&	wstr_dot
	-----------------------------------------------------------*/
	}
  	#endif

	else
	{
		;
	}

	RTA_BTCS_ON_CHK=1;
	if((ABS_fz==1)||(BRAKE_SIGNAL==1)||(McrAbs(afz)>AFZ_0G5)||(RTA_RESET_STATE==1)){
		RTA_BTCS_ON_CHK=0;
	}
	if((vref2 <	(int16_t)VREF_15_KPH)
	#if	__CAR==GM_GSUV
	&&((vref2 <	(int16_t)VREF_10_KPH)||((wheel_speed_max-wheel_speed_2nd) <	VREF_1_5_KPH_RESOL_CHANGE))
	#endif
	){
		RTA_BTCS_ON_CHK=0;
	}
  	#if __EDC
	if((engine_state==0)||(EDC_ON==1)){
		RTA_BTCS_ON_CHK=0;
	}

  	#endif
  	#if (__4WD_VARIANT_CODE==ENABLE)
	if ((ACCEL_SPIN_FZ==1)&&((lsu8DrvMode ==DM_AUTO)||(lsu8DrvMode ==DM_4H))) {
		RTA_BTCS_ON_CHK=0;
	}
 	#elif	__4WD
	if (ACCEL_SPIN_FZ==1) {

		RTA_BTCS_ON_CHK=0;
	}
   	#endif

  	#if __PARKING_BRAKE_OPERATION
	if (PARKING_BRAKE_OPERATION==1)	{

		RTA_BTCS_ON_CHK=0;
	}
	#if	__RTA_2WHEEL_DCT
	if((RL.PARKING_BRAKE_WHEEL==1)||(RR.PARKING_BRAKE_WHEEL==1)){


		RTA_BTCS_ON_CHK=0;
	}
	#endif
  	#endif

  	#if __VDC
	if(ESP_SENSOR_RELIABLE_FLG==1)
	{
		if(VDC_REF_WORK==1)
		{
			RTA_BTCS_ON_CHK=0;
		}
		else if(McrAbs(delta_yaw_first)	>= YAW_3DEG)
		{
			RTA_BTCS_ON_CHK=0;
		}
		else if((McrAbs(yaw_out)>YAW_5DEG)||(McrAbs(wstr)>WSTR_60_DEG)||(McrAbs(alat)>LAT_0G15G))
		{
			RTA_BTCS_ON_CHK=0;
		}
		else
		{
			;
		}
	/*-----------------------------------------------------------
	Consider yaw_out_dot &&	wstr_dot
	-----------------------------------------------------------*/
	}
  	#endif

	else
	{
		;
	}


/*-------------------------------------------------------
	Determin RTA Reset State:Based on GM SSTS
---------------------------------------------------------
	1) Wheel Error State
	2) Car Stop	State during more than determined time
	3) ING Off/On  -Initialize
-------------------------------------------------------*/
	if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1))
	{
		WHEEL_ERROR_STATE=1;
	}
	else if((fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1))
	{
		WHEEL_ERROR_STATE=1;
	}
	else
	{
		WHEEL_ERROR_STATE=0;
	}

	if((FL.vrad_resol_change<RTA_RESET_SPEED)
	&&(FR.vrad_resol_change<RTA_RESET_SPEED)
	&&(RL.vrad_resol_change<RTA_RESET_SPEED)
	&&(RR.vrad_resol_change<RTA_RESET_SPEED)
	&&(vref_resol_change<RTA_RESET_SPEED)){
		if(ldrtau8RtaResetSpeed1SecCnt<L_U8_TIME_10MSLOOP_1000MS){
			ldrtau8RtaResetSpeed1SecCnt++;
		}

		if(ldrtau8RtaResetSpeed1SecCnt>=L_U8_TIME_10MSLOOP_1000MS){
			ldrtau8RtaResetSpeed1SecCnt=0;
			if(rta_reset_speed_cnt<30000)
			{
				rta_reset_speed_cnt++;
			}
		}
	}
	else if((FL.vrad_resol_change>RTA_START_SPEED)
		&&(FR.vrad_resol_change>RTA_START_SPEED)
		&&(RL.vrad_resol_change>RTA_START_SPEED)
		&&(RR.vrad_resol_change>RTA_START_SPEED)
		&&(vref_resol_change>RTA_START_SPEED)){
			rta_reset_speed_cnt=0;
			ldrtau8RtaResetSpeed1SecCnt=0;
	}
	else{
		if(ldrtau8RtaResetSpeed1SecCnt>0){
			ldrtau8RtaResetSpeed1SecCnt--;
		}

		if(ldrtau8RtaResetSpeed1SecCnt==0){
			ldrtau8RtaResetSpeed1SecCnt=L_U8_TIME_10MSLOOP_1000MS;
			if(rta_reset_speed_cnt>0)
			{
				rta_reset_speed_cnt--;
			}
		}
	}

	if((speed_calc_timer <=	(uint8_t)L_U8_TIME_10MSLOOP_1000MS)||(rta_reset_speed_cnt>(uint16_t)CAR_STOP_STATE_DET_TIME)||(WHEEL_ERROR_STATE==1))
	{
		RTA_RESET_STATE=1;
	}
/*
	else if(rta_reset_speed_cnt>CAR_STOP_STATE_DET_TIME)
	{
		RTA_RESET_STATE=1;
	}
	else if	(WHEEL_ERROR_STATE==1)
	{
		RTA_RESET_STATE=1;
	}
*/
	else
	{
		RTA_RESET_STATE=0;
	}


}
/*========================================================================*/
int16_t	LDRTA_vCalDiffRatioWheels(int16_t wheel_speed, int16_t ref_speed)
{

	if(wheel_speed<VREF_MIN_SPEED)
	{
		wheel_speed=VREF_MIN_SPEED;
	}
	if(ref_speed<VREF_MIN_SPEED)
	{
		ref_speed=VREF_MIN_SPEED;
	}
/*
	tempW2=wheel_speed-ref_speed;
	tempW1=1000;
	tempW0=wheel_speed;

	if(tempW0!=0)s16muls16divs16();
*/
	tempW3=(int16_t)(((int32_t)(wheel_speed-ref_speed)*1000)/ref_speed);
/*
	if(tempW3>1000)
	{
		tempW3=1000;
	}
	else if(tempW3<-1000)
	{
		tempW3=-1000;
	}
	else
	{
		;
	}
*/
	tempW3=	LCABS_s16LimitMinMax( tempW3, -1000, 1000);
	return (tempW3);
}
/*=======================================================================*/
void LDRTA_vDetRTASuspectWheel(struct RTA_STRUCT *pRTA_WL,struct W_STRUCT *pRTA_Det,struct W_STRUCT	*pRTA_Side,struct W_STRUCT *pRTA_Axle,struct W_STRUCT *pRTA_Diag)
{

	/*Varible Det*/
	int16_t	rta_det_wheel_speed;
	int16_t	rta_ref_side_wheel_speed;
	int16_t	rta_ref_axle_wheel_speed;
	int16_t	rta_ref_diag_wheel_speed;
	int16_t	rta_det_wheel_arad;

	int16_t	rta_side_diff_ratio;
	int16_t	rta_axle_diff_ratio;
	int16_t	rta_diag_diff_ratio;
	int16_t	vehicle_speed;
	int8_t	rta_whl_reset_flag;
	int16_t	rta_cnt;

	uint8_t	  drive_whl_flg;
	uint8_t	rta_i;
	int16_t	rta_buf_max;
	int16_t	rta_buf_min;
	int16_t	rta_buf_diff;
	int16_t	rta_slip_temp;
	rta_det_wheel_speed=pRTA_Det->vrad_resol_change;
	rta_ref_side_wheel_speed=pRTA_Side->vrad_resol_change;
	rta_ref_axle_wheel_speed=pRTA_Axle->vrad_resol_change;
	rta_ref_diag_wheel_speed=pRTA_Diag->vrad_resol_change;
	rta_det_wheel_arad=pRTA_Det->arad_avg;
	vehicle_speed=vref_resol_change;

	/*Assign Internal Variable Value*/

	rta_side_diff_ratio=LDRTA_vCalDiffRatioWheels(rta_det_wheel_speed,rta_ref_side_wheel_speed);
	rta_axle_diff_ratio=LDRTA_vCalDiffRatioWheels(rta_det_wheel_speed,rta_ref_axle_wheel_speed);
	rta_diag_diff_ratio=LDRTA_vCalDiffRatioWheels(rta_det_wheel_speed,rta_ref_diag_wheel_speed);

	/*---------------------------------------------
	Calc wheel Speed Diff beween Other 3 wheels
	----------------------------------------------*/
	tempW1=(int16_t)McrAbs(rta_ref_axle_wheel_speed-rta_ref_side_wheel_speed);
	tempW2=(int16_t)McrAbs(rta_ref_axle_wheel_speed-rta_ref_diag_wheel_speed);
	tempW3=(int16_t)McrAbs(rta_ref_diag_wheel_speed-rta_ref_side_wheel_speed);

	if (tempW1>tempW2)
	{
		tempW4=tempW1;
	}
	else
	{
		tempW4=tempW2;
	}

	if(tempW4>tempW3)
	{
		;
	}
	else
	{
		tempW4=tempW3;
	}
/*
	tempW0=vehicle_speed;
	tempW1=1000;
	tempW2=tempW4;
	if(tempW0!=0)s16muls16divs16();
*/
	tempW3=(int16_t)(((int32_t)tempW4*1000)/vehicle_speed);
/*----------------------------------------------------------
	Each Wheel RTA Reliable	Condition check
-----------------------------------------------------------*/

	pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=1;
  	#if __AX_SENSOR
	if((USE_ALONG==0)&&(McrAbs(rta_det_wheel_arad-ebd_refilt_grv)>RTA_DET_MAX_WHEEL_ACCEL_DIFF))
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
	else if((USE_ALONG==1)&&(McrAbs(rta_det_wheel_arad-along)>RTA_DET_MAX_WHEEL_ACCEL_DIFF))
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
  	#else
	if(McrAbs(rta_det_wheel_arad-ebd_refilt_grv)>RTA_DET_MAX_WHEEL_ACCEL_DIFF)
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
  	#endif
	else if((McrAbs(rta_det_wheel_arad)>RTA_DET_MAX_VEHICLE_ACCEL)||(tempW4>RTA_VRAD_DIFF_MAX)||(tempW3>RTA_ABSENCE_DET_RATIO))
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
/*
	else if(tempW4>RTA_VRAD_DIFF_MAX)
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
	else if(tempW3>RTA_ABSENCE_DET_RATIO)
	{
		pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL=0;
	}
*/
   /*---------------------------------------------------------------
   !! Addditional CHECK
   1) slip variaiton during	determined time	is bigger than THR.
   2) wheel	speed variation	during during determined time is bigger	than THR.
   -----------------------------------------------------------------*/

	else
	{
		;
	}
	tempB1=0;
	if(RTA_FL.RTA_SUSPECT_WL==1){tempB1++;}
	if(RTA_FR.RTA_SUSPECT_WL==1){tempB1++;}
	if(RTA_RL.RTA_SUSPECT_WL==1){tempB1++;}
	if(RTA_RR.RTA_SUSPECT_WL==1){tempB1++;}

	if (RTA_RESET_STATE==0)
	{
		if(pRTA_WL->RTA_SUSPECT_WL==0)
		{

/*---------------------------------------------------------------------------
	Comfirm	if there is	Positive RTA state wheel(Smaller wheel/Mini	tire) or not.
-----------------------------------------------------------------------------*/
			tempB0=0;
			tempB2=0;

			rta_cnt=pRTA_WL->det_rta_suspect_cnt;
			pRTA_WL->det_rta_suspect_cnt=LDRTA_s16IncreaseRTASuscnt(rta_side_diff_ratio, rta_cnt);


			rta_cnt=pRTA_WL->det_rta_suspect_cnt;
			pRTA_WL->det_rta_suspect_cnt=LDRTA_s16IncreaseRTASuscnt(rta_axle_diff_ratio, rta_cnt);

			rta_cnt=pRTA_WL->det_rta_suspect_cnt;
			pRTA_WL->det_rta_suspect_cnt=LDRTA_s16IncreaseRTASuscnt(rta_diag_diff_ratio, rta_cnt);

/*
			if(rta_side_diff_ratio<RTA_DET_THRES)
			{
				if(pRTA_WL->det_rta_suspect_cnt>0)
				{
					pRTA_WL->det_rta_suspect_cnt--;
				}
				if(rta_side_diff_ratio<-RTA_ABSENCE_DET_RATIO)
				{
					tempB2++;
				}
			}
			else if(rta_side_diff_ratio>RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt>10)
				{
					pRTA_WL->det_rta_suspect_cnt-=10;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt=0;
				}
			}
			else
			{
				tempB0++;
			}

			if(rta_axle_diff_ratio<RTA_DET_THRES)
			{
				if(pRTA_WL->det_rta_suspect_cnt>0)
				{
					pRTA_WL->det_rta_suspect_cnt--;
				}
				if(rta_axle_diff_ratio<-RTA_ABSENCE_DET_RATIO)
				{
					tempB2++;
				}
			}
			else if(rta_axle_diff_ratio>RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt>10)
				{
					pRTA_WL->det_rta_suspect_cnt-=10;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt=0;
				}
			}
			else
			{
				tempB0++;
			}

			if(rta_diag_diff_ratio<RTA_DET_THRES)
			{
				if(pRTA_WL->det_rta_suspect_cnt>0)
				{
					pRTA_WL->det_rta_suspect_cnt--;
				}
				if(rta_diag_diff_ratio<-RTA_ABSENCE_DET_RATIO)
				{
					tempB2++;
				}
			}
			else if(rta_diag_diff_ratio>RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt>10)
				{
					pRTA_WL->det_rta_suspect_cnt-=10;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt=0;
				}
			}
			else
			{
				tempB0++;
			}
*/
			if(tempB2==3)
			{
				pRTA_WL->det_rta_suspect_cnt=0;
			}
			if(tempB0==3)
			{
				if((pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)&&(RTA_RELIABLE_CONDITON_CHECK==1)&&(tempB1==0))
				{
					if(pRTA_WL->det_rta_suspect_cnt<L_U16_TIME_10MSLOOP_1MIN)
					{
						pRTA_WL->det_rta_suspect_cnt++;
						pRTA_WL->ldrtas16RtaSubTimerForBuf++;
						
						rta_slip_temp=(rta_side_diff_ratio+rta_axle_diff_ratio+rta_diag_diff_ratio)/3; /*1%	resol*/
						
						pRTA_WL->ldrtas32SumOfSlipRatio+=rta_slip_temp;
						if(pRTA_WL->ldrtas16RtaSubTimerForBuf>=RTA_DET_BUF_TIMER_MAX){
							if(pRTA_WL->ldrtau8RtaDetBufInx<5){
								pRTA_WL->ldrtas16RtaSusDetBuffer[pRTA_WL->ldrtau8RtaDetBufInx]=(int16_t)(pRTA_WL->ldrtas32SumOfSlipRatio/RTA_DET_BUF_TIMER_MAX);
								pRTA_WL->ldrtau8RtaDetBufInx++;
								pRTA_WL->ldrtas16RtaSubTimerForBuf=0;
								pRTA_WL->ldrtas32SumOfSlipRatio=0;
							}
							else{
								pRTA_WL->ldrtas16RtaSusDetBuffer[0]=pRTA_WL->ldrtas16RtaSusDetBuffer[1];
								pRTA_WL->ldrtas16RtaSusDetBuffer[1]=pRTA_WL->ldrtas16RtaSusDetBuffer[2];
								pRTA_WL->ldrtas16RtaSusDetBuffer[2]=pRTA_WL->ldrtas16RtaSusDetBuffer[3];
								pRTA_WL->ldrtas16RtaSusDetBuffer[3]=pRTA_WL->ldrtas16RtaSusDetBuffer[4];
								pRTA_WL->ldrtas16RtaSusDetBuffer[4]=(int16_t)(pRTA_WL->ldrtas32SumOfSlipRatio/RTA_DET_BUF_TIMER_MAX);
								pRTA_WL->ldrtas16RtaSubTimerForBuf=0;
								pRTA_WL->ldrtas32SumOfSlipRatio=0;
							}
						}

						if(pRTA_WL->det_rta_suspect_cnt>RTA_DET_TIME)
						{
							if(pRTA_WL->ldrtau8RtaDetBufInx>=5){
								rta_buf_min=pRTA_WL->ldrtas16RtaSusDetBuffer[0];
								rta_buf_max=pRTA_WL->ldrtas16RtaSusDetBuffer[0];
								for(rta_i=1;rta_i<5;rta_i++){
									if(rta_buf_min>pRTA_WL->ldrtas16RtaSusDetBuffer[rta_i]){
										rta_buf_min=pRTA_WL->ldrtas16RtaSusDetBuffer[rta_i];
									}
									if(rta_buf_max>pRTA_WL->ldrtas16RtaSusDetBuffer[rta_i]){
										rta_buf_max=pRTA_WL->ldrtas16RtaSusDetBuffer[rta_i];
									}
								}
								rta_buf_diff=(rta_buf_max-rta_buf_min);
								if(rta_buf_diff<=S16_RTA_DET_MIN_MAX_DIFF_THR){
									pRTA_WL->RTA_SUSPECT_WL=1;
									pRTA_WL->det_rta_suspect_cnt=0;
									pRTA_WL->det_rta_suspect_cnt2=0;
									pRTA_WL->det_rta_suspect_cnt3=0;
									pRTA_WL->ldrtas16RtaSubTimerForBuf=0;
									pRTA_WL->ldrtas32SumOfSlipRatio=0;	
									pRTA_WL->ldrtau8RtaDetBufInx=0;	
									
									tempW0 = 1000+ pRTA_WL->ldrtas16RtaSusDetBuffer[0];
									tempW1 = 1000+ pRTA_WL->ldrtas16RtaSusDetBuffer[1];
									tempW2 = 1000+ pRTA_WL->ldrtas16RtaSusDetBuffer[2];
									tempW3 = 1000+ pRTA_WL->ldrtas16RtaSusDetBuffer[3];
									tempW4 = 1000+ pRTA_WL->ldrtas16RtaSusDetBuffer[4];
									
									
									rta_ratio_calc_sum_array[0]=(int16_t)( ((int32_t)1000000)/tempW0 );
									rta_ratio_calc_sum_array[1]=(int16_t)( ((int32_t)1000000)/tempW1 );
									rta_ratio_calc_sum_array[2]=(int16_t)( ((int32_t)1000000)/tempW2 );
									rta_ratio_calc_sum_array[3]=(int16_t)( ((int32_t)1000000)/tempW3 );
									rta_ratio_calc_sum_array[4]=(int16_t)( ((int32_t)1000000)/tempW4 );
								}
								else{
									pRTA_WL->det_rta_suspect_cnt-=RTA_DET_BUF_TIMER_MAX;
/*									pRTA_WL->ldrtas16RtaSubTimerForBuf=0;*/
/*									pRTA_WL->ldrtas32SumOfSlipRatio=0;	*/
								}
							}
						}
					}
				}
			}

/*---------------------------------------------------------------------------
	Comfirm	if there is	Negative RTA state wheel(Bigger	Wheel) or not.
-----------------------------------------------------------------------------*/
			#if	__VDC
			tempB0=0;
			if(rta_side_diff_ratio>-RTA_DET_THRES)
			{
				if(rta_side_diff_ratio>RTA_ABSENCE_DET_RATIO)
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt2>0)
					{
						pRTA_WL->det_rta_suspect_cnt2--;
					}
				}
			}
			else if(rta_side_diff_ratio<-RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt2>L_U8_TIME_10MSLOOP_70MS)
				{
					pRTA_WL->det_rta_suspect_cnt2-=L_U8_TIME_10MSLOOP_70MS;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
			}
			else
			{
				tempB0++;
			}

			if(rta_axle_diff_ratio>-RTA_DET_THRES)
			{
				if(rta_axle_diff_ratio>RTA_ABSENCE_DET_RATIO)
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt2>0)
					{
						pRTA_WL->det_rta_suspect_cnt2--;
					}
				}
			}
			else if(rta_axle_diff_ratio<-RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt2>L_U8_TIME_10MSLOOP_70MS)
				{
					pRTA_WL->det_rta_suspect_cnt2-=L_U8_TIME_10MSLOOP_70MS;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
			}
			else
			{
				tempB0++;
			}

			if(rta_diag_diff_ratio>-RTA_DET_THRES)
			{
				if(rta_diag_diff_ratio>RTA_ABSENCE_DET_RATIO)
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt2>0)
					{
						pRTA_WL->det_rta_suspect_cnt2--;
					}
				}
			}
			else if(rta_diag_diff_ratio<-RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->det_rta_suspect_cnt2>L_U8_TIME_10MSLOOP_70MS)
				{
					pRTA_WL->det_rta_suspect_cnt2-=L_U8_TIME_10MSLOOP_70MS;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt2=0;
				}
			}
			else
			{
				tempB0++;
			}

			if(tempB0==3)
			{
				if((pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)&&(RTA_RELIABLE_CONDITON_CHECK==1)&&(tempB1==0))
				{
					if(pRTA_WL->det_rta_suspect_cnt2<L_U16_TIME_10MSLOOP_1MIN)
					{
						pRTA_WL->det_rta_suspect_cnt2++;

						if(pRTA_WL->det_rta_suspect_cnt2>(RTA_DET_TIME*2))
						{
							pRTA_WL->RTA_SUSPECT_WL=1;
							pRTA_WL->det_rta_suspect_cnt=0;
							pRTA_WL->det_rta_suspect_cnt2=0;
							pRTA_WL->det_rta_suspect_cnt3=0;
						}
					}
				}
			}
			#endif
		}
		/*-------------------------------------------------------
		Clear RTA Detection
		-------------------------------------------------------*/
		else
		{
			if(RTA_RELIABLE_CONDITON_CHECK==1)
			{

			rta_cnt=pRTA_WL->det_rta_suspect_cnt;
			pRTA_WL->det_rta_suspect_cnt=LDRTA_s16DecreaseRTASuscnt(rta_side_diff_ratio, rta_cnt);


			rta_cnt=pRTA_WL->det_rta_suspect_cnt2;
			pRTA_WL->det_rta_suspect_cnt2=LDRTA_s16DecreaseRTASuscnt(rta_axle_diff_ratio, rta_cnt);

			rta_cnt=pRTA_WL->det_rta_suspect_cnt3;
			pRTA_WL->det_rta_suspect_cnt3=LDRTA_s16DecreaseRTASuscnt(rta_diag_diff_ratio, rta_cnt);
/*
				if((McrAbs(rta_diag_diff_ratio))<RTA_ABSENCE_DET_RATIO)
				{
					if(pRTA_WL->det_rta_suspect_cnt<T_1MIN)
					{
						if(RTA_RELIABLE_CONDITON_CHECK==1)
						{
							pRTA_WL->det_rta_suspect_cnt++;
						}
					}
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt>0)
					{
						pRTA_WL->det_rta_suspect_cnt--;
					}
					else
					{
						pRTA_WL->det_rta_suspect_cnt=0;
					}
				}

				if((McrAbs(rta_axle_diff_ratio))<RTA_ABSENCE_DET_RATIO)
				{

					if(pRTA_WL->det_rta_suspect_cnt2<T_1MIN)
					{
						{
							pRTA_WL->det_rta_suspect_cnt2++;
						}
					}
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt2>0)
					{
						pRTA_WL->det_rta_suspect_cnt2--;
					}
					else
					{
						pRTA_WL->det_rta_suspect_cnt2=0;
					}
				}

				if((McrAbs(rta_side_diff_ratio))<RTA_ABSENCE_DET_RATIO)
				{
					if(pRTA_WL->det_rta_suspect_cnt3<T_1MIN)
					{
						if(RTA_RELIABLE_CONDITON_CHECK==1)
						{
							pRTA_WL->det_rta_suspect_cnt3++;
						}
					}
				}
				else
				{
					if(pRTA_WL->det_rta_suspect_cnt3>0)
					{
						pRTA_WL->det_rta_suspect_cnt3--;
					}
					else
					{
						pRTA_WL->det_rta_suspect_cnt3=0;
					}
				}
*/
			}

			rta_whl_reset_flag=0;

			if	(pRTA_WL->det_rta_suspect_cnt>RTA_RESET_TIME)
			{
				if(pRTA_WL->det_rta_suspect_cnt2>RTA_RESET_TIME)
				{
					rta_whl_reset_flag=1;
				}
				else if	(pRTA_WL->det_rta_suspect_cnt3>RTA_RESET_TIME)
				{
					rta_whl_reset_flag=1;
				}
			}
			else if(pRTA_WL->det_rta_suspect_cnt2>RTA_RESET_TIME)
			{
				if(pRTA_WL->det_rta_suspect_cnt3>RTA_RESET_TIME)
				{
					rta_whl_reset_flag=1;
				}
			}
			else
			{
				;
			}

			if((pRTA_WL->det_rta_suspect_cnt>RTA_RESET_TIME2)||(pRTA_WL->det_rta_suspect_cnt2>RTA_RESET_TIME2)||(pRTA_WL->det_rta_suspect_cnt3>RTA_RESET_TIME2))
			{
				rta_whl_reset_flag=1;
			}
/*
			else if(pRTA_WL->det_rta_suspect_cnt2>RTA_RESET_TIME2)
			{
				rta_whl_reset_flag=1;
			}
			else if(pRTA_WL->det_rta_suspect_cnt3>RTA_RESET_TIME2)
			{
				rta_whl_reset_flag=1;
			}
			else
			{
				;
			}
*/
			if (rta_whl_reset_flag==1)
			{
				pRTA_WL->RTA_SUSPECT_WL=0;
				pRTA_WL->det_rta_suspect_cnt=0;
				pRTA_WL->det_rta_suspect_cnt2=0;
				pRTA_WL->det_rta_suspect_cnt3=0;
			}
			else
			{
				;
			}
		}
		drive_whl_flg=0;
		#if	(__4WD_VARIANT_CODE==ENABLE)
		if ((lsu8DrvMode ==DM_2WD)||(lsu8DrvMode ==DM_2H)){
			#if	__REAR_D
			if(pRTA_Det->REAR_WHEEL==1)
			#else
			if(pRTA_Det->REAR_WHEEL==0)
			#endif
			{
				drive_whl_flg=1;
			}
			else
			{
				drive_whl_flg=0;
			}
		}
		else if((lsu8DrvMode ==DM_AUTO)||(lsu8DrvMode ==DM_4H)){
			drive_whl_flg=1;
		}
		else{
			drive_whl_flg=1;
		}

		#elif __4WD
		drive_whl_flg=1;
		#else
			#if	__REAR_D
			if(pRTA_Det->REAR_WHEEL==1)
			#else
			if(pRTA_Det->REAR_WHEEL==0)
			#endif
			{
				drive_whl_flg=1;
			}
			else{
				drive_whl_flg=0;
			}
		#endif

		if(pRTA_WL->RTA_SUSPECT_WL_FOR_BTCS==0)
		{
			if(drive_whl_flg==1){
				tempB0=0;
				tempB2=0;

				rta_cnt=pRTA_WL->det_rta_suspect_cnt4;
				pRTA_WL->det_rta_suspect_cnt4=LDRTA_s16IncreaseRTASuscnt(rta_side_diff_ratio, rta_cnt);


				rta_cnt=pRTA_WL->det_rta_suspect_cnt4;
				pRTA_WL->det_rta_suspect_cnt4=LDRTA_s16IncreaseRTASuscnt(rta_axle_diff_ratio, rta_cnt);

				rta_cnt=pRTA_WL->det_rta_suspect_cnt4;
				pRTA_WL->det_rta_suspect_cnt4=LDRTA_s16IncreaseRTASuscnt(rta_diag_diff_ratio, rta_cnt);

				if(tempB2==3)
				{
					pRTA_WL->det_rta_suspect_cnt4=0;
				}
				if(tempB0==3)
				{
					if((pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)&&(RTA_BTCS_ON_CHK==1))
					{
						if(pRTA_WL->det_rta_suspect_cnt4<L_U16_TIME_10MSLOOP_1MIN)
						{
							pRTA_WL->det_rta_suspect_cnt4++;
						   
							pRTA_WL->lds16RtaAvgSlipRatio=(rta_side_diff_ratio+rta_axle_diff_ratio+rta_diag_diff_ratio)/3;
							
							if(pRTA_WL->det_rta_suspect_cnt4==1)
							{
								pRTA_WL->lds16RtaDetMaxSlipRatio=pRTA_WL->lds16RtaAvgSlipRatio;
								pRTA_WL->lds16RtaDetMinSlipRatio=pRTA_WL->lds16RtaAvgSlipRatio;		
							}
							else
							{
								if(pRTA_WL->lds16RtaDetMaxSlipRatio	< pRTA_WL->lds16RtaAvgSlipRatio)
								{
									pRTA_WL->lds16RtaDetMaxSlipRatio = pRTA_WL->lds16RtaAvgSlipRatio;
								}
								else{}
								if(pRTA_WL->lds16RtaDetMinSlipRatio	> pRTA_WL->lds16RtaAvgSlipRatio	)
								{
									pRTA_WL->lds16RtaDetMinSlipRatio = pRTA_WL->lds16RtaAvgSlipRatio;
								}
								else{}
								
							}
						   
							if(pRTA_WL->det_rta_suspect_cnt4>RTA_SUS_DET_TIME)
							{
								pRTA_WL->lds16RtaCalcdiffRatio=pRTA_WL->lds16RtaDetMaxSlipRatio-pRTA_WL->lds16RtaDetMinSlipRatio;
								if(McrAbs(pRTA_WL->lds16RtaCalcdiffRatio) <	S16_RTA_DIFF_RATIO_THRES)
								{
									pRTA_WL->RTA_SUSPECT_WL_FOR_BTCS=1;
									pRTA_WL->det_rta_suspect_cnt4=0;
									pRTA_WL->det_rta_suspect_cnt5=0;
									pRTA_WL->det_rta_suspect_cnt6=0;
								}
								else
								{
									pRTA_WL->RTA_SUSPECT_WL_FOR_BTCS=0;
									pRTA_WL->det_rta_suspect_cnt4=0;
									pRTA_WL->det_rta_suspect_cnt5=0;
									pRTA_WL->det_rta_suspect_cnt6=0;
								}
							}
						}
					}
				}
			}

		}
		else
		{
			if((RTA_BTCS_ON_CHK==1)&&(pRTA_WL->RTA_RELIABLE_CONDITON_CHECK_WHEEL==1))
			{
				if(McrAbs(rta_side_diff_ratio)<RTA_ABSENCE_DET_RATIO){
					if(pRTA_WL->det_rta_suspect_cnt4<T_1MIN) {
						pRTA_WL->det_rta_suspect_cnt4++;
					}
				}
				else if(pRTA_WL->det_rta_suspect_cnt4>0){
						pRTA_WL->det_rta_suspect_cnt4--;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt4=0;
				}


				if(McrAbs(rta_axle_diff_ratio)<RTA_ABSENCE_DET_RATIO){
					if(pRTA_WL->det_rta_suspect_cnt5<T_1MIN) {
						pRTA_WL->det_rta_suspect_cnt5++;
					}
				}
				else if(pRTA_WL->det_rta_suspect_cnt5>0){
						pRTA_WL->det_rta_suspect_cnt5--;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt5=0;
				}

				if(McrAbs(rta_diag_diff_ratio)<RTA_ABSENCE_DET_RATIO){
					if(pRTA_WL->det_rta_suspect_cnt6<T_1MIN) {
						pRTA_WL->det_rta_suspect_cnt6++;
					}
				}
				else if(pRTA_WL->det_rta_suspect_cnt6>0){
						pRTA_WL->det_rta_suspect_cnt6--;
				}
				else
				{
					pRTA_WL->det_rta_suspect_cnt6=0;
				}

			}

			rta_whl_reset_flag=0;

			if ( (pRTA_WL->det_rta_suspect_cnt4>RTA_SUS_CLEAR_TIME)&&(pRTA_WL->det_rta_suspect_cnt5>RTA_SUS_CLEAR_TIME)	 ){
			   rta_whl_reset_flag=1;
			}
			else if	( (pRTA_WL->det_rta_suspect_cnt4>RTA_SUS_CLEAR_TIME)&&(pRTA_WL->det_rta_suspect_cnt6>RTA_SUS_CLEAR_TIME)){
				rta_whl_reset_flag=1;

			}
			else if((pRTA_WL->det_rta_suspect_cnt5>RTA_SUS_CLEAR_TIME)&&(pRTA_WL->det_rta_suspect_cnt6>RTA_SUS_CLEAR_TIME)){
				 rta_whl_reset_flag=1;
			}
			else
			{
				;
			}

			if((pRTA_WL->det_rta_suspect_cnt4>RTA_SUS_CLEAR_TIME2)||(pRTA_WL->det_rta_suspect_cnt5>RTA_SUS_CLEAR_TIME2)||(pRTA_WL->det_rta_suspect_cnt6>RTA_SUS_CLEAR_TIME2))
			{
				rta_whl_reset_flag=1;
			}

			if (rta_whl_reset_flag==1)
			{
				pRTA_WL->RTA_SUSPECT_WL_FOR_BTCS=0;
				pRTA_WL->det_rta_suspect_cnt4=0;
				pRTA_WL->det_rta_suspect_cnt5=0;
				pRTA_WL->det_rta_suspect_cnt6=0;
			}
			else
			{
				;
			}
		}

	}
/*-------------------------------------------------------------
 when RTA reset	state ,	clear every	related	variables
---------------------------------------------------------------*/
	else
	{
		pRTA_WL->RTA_SUSPECT_WL=0;
		pRTA_WL->RTA_SUSPECT_WL_FOR_BTCS=0;
		pRTA_WL->det_rta_suspect_cnt=0;
		pRTA_WL->det_rta_suspect_cnt2=0;
		pRTA_WL->det_rta_suspect_cnt3=0;
		pRTA_WL->det_rta_suspect_cnt4=0;
		pRTA_WL->det_rta_suspect_cnt5=0;
		pRTA_WL->det_rta_suspect_cnt6=0;
	}
}
/*========================================================================*/
int16_t	LDRTA_s16IncreaseRTASuscnt(int16_t rta_input_diff_ratio, int16_t rta_cnt)
{
	if(rta_input_diff_ratio<RTA_DET_THRES)
	{
		if(rta_cnt>0)
		{
			rta_cnt--;
		}
		if(rta_input_diff_ratio<-RTA_ABSENCE_DET_RATIO)
		{
			tempB2++;
		}
	}
	else if(rta_input_diff_ratio>RTA_DET_THRES_MAX)
	{
		if(rta_cnt>L_U8_TIME_10MSLOOP_70MS)
		{
			rta_cnt-=L_U8_TIME_10MSLOOP_70MS;
		}
		else
		{
			rta_cnt=0;
		}
	}
	else
	{
		tempB0++;
	}

	return rta_cnt;
}
/*========================================================================*/
int16_t	LDRTA_s16DecreaseRTASuscnt(int16_t rta_input_diff_ratio, int16_t rta_cnt)
{
	if((McrAbs(rta_input_diff_ratio))<RTA_ABSENCE_DET_RATIO)
	{
		if(rta_cnt<L_U16_TIME_10MSLOOP_1MIN)
		{
			if(RTA_RELIABLE_CONDITON_CHECK==1)
			{
				rta_cnt++;
			}
		}
	}
	else
	{
		if(rta_cnt>0)
		{
			rta_cnt--;
		}
		else
		{
			rta_cnt=0;
		}
	}
	return rta_cnt;
}
/*========================================================================*/
int16_t	LDRTA_sCal3WhlAvg(int16_t v_speed,int16_t whl1,int16_t whl2,int16_t	whl3)
{

	tempF0=1;
	if(v_speed>HIGH_SPEED_FOR_RTA)
	{
/*
		sum_of_whl_speed=0;
		tempW0=3;
		tempW1=1;

		tempW2=whl1;
		s16muls16divs16();
		sum_of_whl_speed+=tempW3;
		tempW2=whl2;
		s16muls16divs16();
		sum_of_whl_speed+=tempW3;
		tempW2=whl3;
		s16muls16divs16();
		sum_of_whl_speed+=tempW3;



*/
		tempW3=	(whl1/3)+(whl2/3)+(whl3/3);
/*		s16muls16divs16();*/
		return tempW3;

	}
	else
	{
/*
		sum_of_whl_speed=0;
		tempW0=3;
		tempW1=1;
		tempW2=whl1+whl2+whl3;
		s16muls16divs16();
*/
		tempW3=(whl1+whl2+whl3)/3;
		return tempW3;
	}
}
/*========================================================================*/
void LDRTA_vCalRTACompRatio(void)
{
	int16_t	rta_ratio_ref_speed;
	int16_t	rta_wheel_speed;
//	int16_t	rta_ratio_temp; MBD ETCS
	int16_t	whl_speed_fl;
	int16_t	whl_speed_fr;
	int16_t	whl_speed_rl;
	int16_t	whl_speed_rr;
	int16_t	vehicle_speed;

	int16_t	input_gain;
	int16_t	output_gain;
	int16_t	rta_temp_max;
	int16_t	rta_temp_min;
	int8_t 	rta_i;

	whl_speed_fl=FL.vrad_resol_change;
	whl_speed_fr=FR.vrad_resol_change;
	whl_speed_rl=RL.vrad_resol_change;
	whl_speed_rr=RR.vrad_resol_change;
	vehicle_speed=vref_resol_change;


	tempF0=0;
	tempF1=0;
	if((RTA_FL.RTA_SUSPECT_WL==1)||
	((RTA_FL.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_1500MS)&&(RTA_FR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)
	&&(RTA_RL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_RR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)))
	{
		rta_ratio_ref_speed=LDRTA_sCal3WhlAvg(vehicle_speed,whl_speed_fr,whl_speed_rl,whl_speed_rr);
		rta_wheel_speed=whl_speed_fl;
		if(RTA_FL.RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)
		{
			tempF1=1;
		}
	}
	else if((RTA_FR.RTA_SUSPECT_WL==1)||
	((RTA_FL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_FR.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_1500MS)
	&&(RTA_RL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_RR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)))
	{
		rta_ratio_ref_speed=LDRTA_sCal3WhlAvg(vehicle_speed,whl_speed_fl,whl_speed_rl,whl_speed_rr);
		rta_wheel_speed=whl_speed_fr;
		if(RTA_FR.RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)
		{
			tempF1=1;
		}
	}
	else if((RTA_RL.RTA_SUSPECT_WL==1)||
	((RTA_FL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_FR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)
	&&(RTA_RL.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_1500MS)&&(RTA_RR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)))
	{
		rta_ratio_ref_speed=LDRTA_sCal3WhlAvg(vehicle_speed,whl_speed_fl,whl_speed_fr,whl_speed_rr);
		rta_wheel_speed=whl_speed_rl;
		if(RTA_RL.RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)
		{
			tempF1=1;
		}
	}
	else if((RTA_RR.RTA_SUSPECT_WL==1)||
	((RTA_FL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_FR.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)
	&&(RTA_RL.det_rta_suspect_cnt<L_U8_TIME_10MSLOOP_50MS)&&(RTA_RR.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_1500MS)))
	{
		rta_ratio_ref_speed=LDRTA_sCal3WhlAvg(vehicle_speed,whl_speed_fl,whl_speed_fr,whl_speed_rl);
		rta_wheel_speed=whl_speed_rr;
		if(RTA_RR.RTA_RELIABLE_CONDITON_CHECK_WHEEL==1)
		{
			tempF1=1;
		}
	}
	else
	{
		rta_ratio_ref_speed=vehicle_speed;
		rta_wheel_speed=vehicle_speed;
	}

	if(tempF0==1)
	{
		if ((RTA_RELIABLE_CONDITON_CHECK==1)&&(tempF1==1))
		{
			rta_ratio_temp=(int16_t)(((int32_t)rta_ratio_ref_speed*1000)/rta_wheel_speed);
			/*RTA Ratio	Limitation:	75%~125%*/
			if(rta_ratio_temp<RTA_COMP_RATIO_MIN)
			{
				;
			}
			else if(rta_ratio_temp>RTA_COMP_RATIO_MAX)
			{
				;
			}
			else
			{
				rta_ratio_calc_timer_1++;
				rta_ratio_calc_sum_0+=rta_ratio_temp;

				if(rta_ratio_calc_timer_1==7)
				{
					tempW3=rta_ratio_calc_sum_0/7;
					#if	__RTA_UPDATE_VERIFY
					rta_ratio_calc_sum_0=0;
					rta_ratio_calc_timer_1=0;

					if(((McrAbs(tempW3-rta_ratio_avg)<30)&&(RTA_RATIO_CONV_FLG==1))||(RTA_RATIO_CONV_FLG==0))
					{

						rta_ratio_calc_sum_array[rta_ratio_calc_timer_2]=tempW3;
						rta_temp_max=tempW3;
						rta_temp_min=tempW3;
						for(rta_i=0;rta_i<5;rta_i++)
						{
							if(rta_temp_max<rta_ratio_calc_sum_array[rta_i])
							{
								rta_temp_max=rta_ratio_calc_sum_array[rta_i];
							}
							else{}
							if (rta_temp_min>rta_ratio_calc_sum_array[rta_i])
							{
								rta_temp_min=rta_ratio_calc_sum_array[rta_i];
							}
							else{}
						}
						
						rta_ratio_calc_timer_2++;
						
						if(rta_ratio_calc_timer_2>=5)
						{
							rta_ratio_calc_timer_2=0;
						}
						else{}
						if((rta_temp_max-rta_temp_min)<20)
		                {
		                    tempW2=rta_ratio_calc_sum_array[0]+rta_ratio_calc_sum_array[1]+rta_ratio_calc_sum_array[2]+rta_ratio_calc_sum_array[3]+rta_ratio_calc_sum_array[4];
		                    tempW3=tempW2/5;
		                    tempW3=LCABS_s16LimitMinMax(tempW3,RTA_COMP_RATIO_MIN,RTA_COMP_RATIO_MAX);
		                    rta_ratio_raw_old=rta_ratio_raw;
		                    rta_ratio_raw=tempW3;
		                    if(RTA_RATIO_CONV_FLG==0)
		                    {
		                        if((McrAbs(rta_ratio_avg-rta_ratio_raw)<10)&&(McrAbs(rta_ratio_avg_old-rta_ratio_raw)<10)
		                            &&(McrAbs(rta_ratio_avg-rta_ratio_avg_old)<5))
		                        {
		                            rta_ratio_conv_cnt++;
		                        }
		                        else if(rta_ratio_conv_cnt>0)
		                        {
		                            rta_ratio_conv_cnt--;
		                        }
		                        else{}
		                        	
		                        if(rta_ratio_conv_cnt>=100)
		                        {
		                            RTA_RATIO_CONV_FLG=1;
		                        }
		                        else
		                        {
		                            ;
		                        }
		
		                        if(rta_calc_cnt<250)
		                        {
		                            rta_calc_cnt++;
		                        }
		                        else
		                        {
		                            ;
		                        }
		
		                        tempL0=(int32_t)rta_ratio_avg*(int32_t)(rta_calc_cnt-1)+(int32_t)(rta_ratio_raw)+(int32_t)rta_ratio_rest;
		                        rta_ratio_avg_old=rta_ratio_avg;
		                        rta_ratio_avg=(int16_t)(tempL0/rta_calc_cnt);
		                        rta_ratio_rest=(int16_t)(tempL0%rta_calc_cnt);
		                    }
		                    else
		                    {
		                        ;
		                    }
		
		
		
		                    if(rta_calc_cnt>=30)
		                    {
		                        tempW3=LCABS_s16LimitMinMax(tempW3,rta_ratio_avg-20,rta_ratio_avg+20);		
		                    }
		                    else
		                    {
		                        ;
		                    }
		                    rta_ratio_old=rta_ratio;
		                    rta_ratio=tempW3;
		                }
		                else{}									
					}
					else{}
					#else
					rta_ratio_calc_sum_array[rta_ratio_calc_timer_2]=tempW3;

					rta_ratio_calc_timer_2++;
					if(rta_ratio_calc_timer_2>5)
					{
						rta_ratio_calc_timer_2=0;
					}
					else{}
					rta_ratio_calc_sum_0=0;
					rta_ratio_calc_timer_1=0;
					tempW2=rta_ratio_calc_sum_array[0]+rta_ratio_calc_sum_array[1]+rta_ratio_calc_sum_array[2]+rta_ratio_calc_sum_array[3]+rta_ratio_calc_sum_array[4];
					tempW3=tempW2/5;
					rta_ratio=LCABS_s16LimitMinMax(tempW3,RTA_COMP_RATIO_MIN,RTA_COMP_RATIO_MAX);
					#endif
				}
				else
				{
					;
				}
			}
		}
		else
		{
			;
		}
	}
	else
	{
		rta_ratio=1000;
		rta_ratio_calc_sum_0=0;

		rta_ratio_calc_sum_array[0]=rta_ratio_calc_sum_array[1]=rta_ratio_calc_sum_array[2]=rta_ratio_calc_sum_array[3]=rta_ratio_calc_sum_array[4]=0;
/*
		rta_ratio_calc_sum_1=1000;
		rta_ratio_calc_sum_2=1000;
		rta_ratio_calc_sum_3=1000;
		rta_ratio_calc_sum_4=1000;
		rta_ratio_calc_sum_5=1000;
*/
		rta_ratio_calc_timer_1=0;
		rta_ratio_calc_timer_2=0;

		#if	__RTA_UPDATE_VERIFY
		rta_ratio_old=1000;
		rta_ratio_raw=1000;
		rta_ratio_raw_old=1000;
		rta_ratio_avg=1000;
		rta_ratio_rest=0;
		rta_calc_cnt=0;
		rta_ratio_conv_cnt=0;
		RTA_RATIO_CONV_FLG=0;
		#endif
	}
}
/*========================================================================*/

void LDRTA_vConfirmRTAState(void)
{
	
/*---------------------------------------------------------------------------
	Comfirm	if there is	RTA	state wheel	or not.
-----------------------------------------------------------------------------*/
	if(RTA_RESET_STATE==1)
	{
		ldrtau8RtaState=RTA_STATE_UNKNOWN;
		ldrtau16RtaAbsnConfirmCnt=0;
		ldrtau16RtaPresConfirmCnt=0;
	}
	else
	{
		if(ldrtau8RtaState==RTA_STATE_UNKNOWN)
		{
			if(RTA_RELIABLE_CONDITON_CHECK==1)
			{
				if((RTA_FL.RTA_SUSPECT_WL==0)&&(RTA_FR.RTA_SUSPECT_WL==0)
					&&(RTA_RL.RTA_SUSPECT_WL==0)&&(RTA_RR.RTA_SUSPECT_WL==0)
					#if	__RTA_2WHEEL_DCT
					&&(RTA_FL.RTA_SUSPECT_2WHL==0)&&(RTA_FR.RTA_SUSPECT_2WHL==0)
					&&(RTA_RL.RTA_SUSPECT_2WHL==0)&&(RTA_RR.RTA_SUSPECT_2WHL==0)
					#endif
					)
				{
					if(ldrtau16RtaPresConfirmCnt>0)
					{
						ldrtau16RtaPresConfirmCnt--;
					}

					if((RTA_FL.det_rta_suspect_cnt==0)&&(RTA_FR.det_rta_suspect_cnt==0)
						&&(RTA_RL.det_rta_suspect_cnt==0)&&(RTA_RR.det_rta_suspect_cnt==0)
						#if	__RTA_2WHEEL_DCT
						&&(RTA_FL.ldrtas16Rta2WhlDctCnt==0)&&(RTA_FR.ldrtas16Rta2WhlDctCnt==0)
						&&(RTA_RL.ldrtas16Rta2WhlDctCnt==0)&&(RTA_RR.ldrtas16Rta2WhlDctCnt==0)
						#endif
						)
					{
						if(vrad_diff_max_resol_change<=VDIFF_THR_FOR_RTA_STATE_CONFIRM){

							if(ldrtau1WssInvalidFlagForRTA==0){
								ldrtau16RtaAbsnConfirmCnt++;
							}
							else if	(ldrtas16WssInvalidTimerForRTA>0){
								ldrtas16WssInvalidTimerForRTA--;
								if(ldrtas16WssInvalidTimerForRTA==0){
									ldrtau1WssInvalidFlagForRTA=0;
								}
							}
							else{
								;
							}
						}
						else{
							ldrtau1WssInvalidFlagForRTA=1;
							ldrtas16WssInvalidTimerForRTA=WHL_STABLE_DET_TIME_FOR_RTA;

						}
						if((ldrtau16RtaAbsnConfirmCnt>(uint16_t)RTA_STATE_CONFIRM_TIME)&&
							(ldrtau16RtaPresConfirmCnt==0))
						{
							ldrtau8RtaState=RTA_STATE_ABSENCE;
							ldrtau16RtaAbsnConfirmCnt=0;
							ldrtau16RtaPresConfirmCnt=0;
						}
					}
				}
				else
				{
					ldrtau16RtaPresConfirmCnt++;
					if(ldrtau16RtaAbsnConfirmCnt>0)
					{
						ldrtau16RtaAbsnConfirmCnt--;
					}
					if((ldrtau16RtaPresConfirmCnt>(uint16_t)RTA_STATE_CONFIRM_TIME)&&
						(ldrtau16RtaAbsnConfirmCnt==0))
					{
						ldrtau8RtaState=RTA_STATE_PRESENCE;
						ldrtau16RtaAbsnConfirmCnt=0;
						ldrtau16RtaPresConfirmCnt=0;
					}
				}
			}
			else
			{
				;
			}
		}
		/*Add Confirmed	State Reset	Cond*/
		else {
			if(RTA_RELIABLE_CONDITON_CHECK==1){
				if(ldrtau8RtaState==RTA_STATE_ABSENCE){
					if((RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_WL==1)
						||(RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_WL==1)
						#if	__RTA_2WHEEL_DCT
						||(RTA_FL.RTA_SUSPECT_2WHL==1)||(RTA_FR.RTA_SUSPECT_2WHL==1)
						||(RTA_RL.RTA_SUSPECT_2WHL==1)||(RTA_RR.RTA_SUSPECT_2WHL==1)
						#endif
						)
					{
						if(ldrtau16RtaAbsnConfirmCnt>0)
						{
							ldrtau16RtaAbsnConfirmCnt--;
						}
						ldrtau16RtaPresConfirmCnt++;

						if((ldrtau16RtaPresConfirmCnt>(uint16_t)(RTA_STATE_CONFIRM_TIME*2))&&
							(ldrtau16RtaAbsnConfirmCnt==0))
						{
							ldrtau8RtaState=RTA_STATE_PRESENCE;
							ldrtau16RtaAbsnConfirmCnt=0;
							ldrtau16RtaPresConfirmCnt=0;
						}
					}
					else{
						if(ldrtau16RtaPresConfirmCnt>0)
						{
							ldrtau16RtaPresConfirmCnt--;
						}

					}
				}
				else if(ldrtau8RtaState==RTA_STATE_PRESENCE){
					if((RTA_FL.RTA_SUSPECT_WL==0)&&(RTA_FR.RTA_SUSPECT_WL==0)
						&&(RTA_RL.RTA_SUSPECT_WL==0)&&(RTA_RR.RTA_SUSPECT_WL==0)
						#if	__RTA_2WHEEL_DCT
						&&(RTA_FL.RTA_SUSPECT_2WHL==0)&&(RTA_FR.RTA_SUSPECT_2WHL==0)
						&&(RTA_RL.RTA_SUSPECT_2WHL==0)&&(RTA_RR.RTA_SUSPECT_2WHL==0)
						#endif
						)
					{
						if(ldrtau16RtaPresConfirmCnt>0)
						{
							ldrtau16RtaPresConfirmCnt--;
						}

						if(vrad_diff_max_resol_change<=VDIFF_THR_FOR_RTA_STATE_CONFIRM){

							if(ldrtau1WssInvalidFlagForRTA==0){
								ldrtau16RtaAbsnConfirmCnt++;
							}
							else if	(ldrtas16WssInvalidTimerForRTA>0){
								ldrtas16WssInvalidTimerForRTA--;
								if(ldrtas16WssInvalidTimerForRTA==0){
									ldrtau1WssInvalidFlagForRTA=0;
								}
							}
							else{
								;
							}
						}
						else{
							ldrtau1WssInvalidFlagForRTA=1;
							ldrtas16WssInvalidTimerForRTA=WHL_STABLE_DET_TIME_FOR_RTA;

						}
						if((ldrtau16RtaAbsnConfirmCnt>(uint16_t)(RTA_STATE_CONFIRM_TIME*2))&&
							(ldrtau16RtaPresConfirmCnt==0))
						{
							ldrtau8RtaState=RTA_STATE_ABSENCE;
							ldrtau16RtaAbsnConfirmCnt=0;
							ldrtau16RtaPresConfirmCnt=0;
						}
					}
					else{
						if(ldrtau16RtaAbsnConfirmCnt>0)
						{
							ldrtau16RtaAbsnConfirmCnt--;
						}

					}
				}
				else{
					;
				}
			}
			else{
				;
			}
		}
	}
}

/*========================================================================*/

void LDRTA_vExeRTACompensation(struct W_STRUCT *WL,struct RTA_STRUCT *RTA_WL)
{
	if(RTA_WL->RTA_SUSPECT_WL==1)
	{
		tempW3=(int16_t)((((int32_t)WL->vrad_resol_change)*rta_ratio)/1000);
		if(tempW3<VREF_MIN_SPEED)
		{
			tempW3=VREF_MIN_SPEED;
		}
		else
		{
			;
		}
		WL->vrad_crt_resol_change=tempW3;
		tempW3=(tempW3>>3);

		if(tempW3<VREF_MIN_SPEED_RESOL_8)
		{
			tempW3=VREF_MIN_SPEED_RESOL_8;
		}
		else
		{
			;
		}
		WL->vrad_crt=tempW3;

	}
	#if	__RTA_2WHEEL_DCT
	else if(RTA_WL->RTA_SUSPECT_2WHL==1)
	{
		tempW3=(int16_t)(((int32_t)WL->vrad_resol_change*RTA_WL->ldrtas16Rta2WhlCompRatio)/1000);
		if(tempW3<VREF_MIN_SPEED)
		{
			tempW3=VREF_MIN_SPEED;
		}
		else
		{
			;
		}
		WL->vrad_crt_resol_change=tempW3;
		tempW3=(tempW3/8);

		if(tempW3<VREF_MIN_SPEED_RESOL_8)
		{
			tempW3=VREF_MIN_SPEED_RESOL_8;
		}
		else
		{
			;
		}
		WL->vrad_crt=tempW3;

	}
	#endif
	else
	{

		WL->vrad_crt_resol_change=WL->vrad_resol_change;
		WL->vrad_crt=WL->vrad;
	}

    tempW0 = (int16_t)FL.SPIN_WHEEL + (int16_t)FR.SPIN_WHEEL + (int16_t)RL.SPIN_WHEEL + (int16_t)RR.SPIN_WHEEL;
    if((WL->SPIN_WHEEL==1) && (tempW0<=3))
    {
		WL->vrad_crt_resol_change=  vref_resol_change;
		WL->vrad_crt             =  vref;
    }
	else
	{
		;
	}
	if(ldrtau8RtaState==RTA_STATE_PRESENCE)
	{
		tempW1=(int16_t)McrAbs(rta_ratio-(U8_MINI_WHEEL_DOWN_RATE*10));
		#if	__RTA_2WHEEL_DCT
		tempW2=McrAbs(RTA_WL->ldrtas16Rta2WhlCompRatio-(U8_MINI_WHEEL_DOWN_RATE*10));
		#endif
		if(RTA_WL->ldabsu1DetSpareTire==0)
		{
			if((RTA_WL->RTA_SUSPECT_WL==1)&&(tempW1<20))
			{
				RTA_WL->ldabsu1DetSpareTire=1;
			}
			#if	__RTA_2WHEEL_DCT
			else if((RTA_WL->RTA_SUSPECT_2WHL==1)&&(tempW2<20))
			{
				RTA_WL->ldabsu1DetSpareTire=1;
			}
			#endif
			else
			{
				;
			}
		}
		else
		{
			if((RTA_WL->RTA_SUSPECT_WL==0)
				#if	__RTA_2WHEEL_DCT
			&&(RTA_WL->RTA_SUSPECT_2WHL==0)
				#endif
				)

			{
				RTA_WL->ldabsu1DetSpareTire=0;
			}
		}
	}
	else
	{
		RTA_WL->ldabsu1DetSpareTire=0;
	}

	WL->MINI_SPARE_WHEEL=(RTA_WL->RTA_SUSPECT_WL)|(RTA_WL->RTA_SUSPECT_2WHL);
}

/*========================================================================*/
#if	__RTA_2WHEEL_DCT
void LDRTA_vCallRTA2WheelDetection(void)
{
	int16_t	rta_j;
	int16_t	rta_i;
	ldrtau1GoodCondForRta2WhlDct=0;
	ldrtau1GoodCondForRta2WhlSus=0;
	ldrtau1GoodCondForRta2WhlClear=0;


	LDABS_vArrangeRawWhlSpd(FL.vrad_resol_change,FR.vrad_resol_change,RL.vrad_resol_change,RR.vrad_resol_change);

	wheel_spd_max_temp=tempW4;
	wheel_spd_2nd_temp=tempW5;
	wheel_spd_3rd_temp=tempW6;
	wheel_spd_min_temp=tempW7;
	tempB0=(int8_t)(RTA_FL.RTA_SUSPECT_2WHL)+(int8_t)(RTA_FR.RTA_SUSPECT_2WHL)+(int8_t)(RTA_RL.RTA_SUSPECT_2WHL)+(int8_t)(RTA_RR.RTA_SUSPECT_2WHL);
	if(tempB0==2)
	{
		if(RTA_FL.RTA_SUSPECT_2WHL==1)
		{
			tempW0=0;
		}
		else
		{
			tempW0=FL.vrad_resol_change;
		}
		if(RTA_FR.RTA_SUSPECT_2WHL==1)
		{
			tempW1=0;
		}
		else
		{
			tempW1=FR.vrad_resol_change;
		}
		if(RTA_RL.RTA_SUSPECT_2WHL==1)
		{
			tempW2=0;
		}
		else
		{
			tempW2=RL.vrad_resol_change;
		}
		if(RTA_RR.RTA_SUSPECT_2WHL==1)
		{
			tempW3=0;
		}
		else
		{
			tempW3=RR.vrad_resol_change;
		}
		LDABS_vArrangeRawWhlSpd(tempW0,tempW1,tempW2,tempW3);
		ldrtas16Rta2WhlCompCalcRef=(tempW4+tempW5)/2;
		ldrtas16Rta2WhlDctRef=tempW4;
		ldrtas16Rta2WhlClearRef=tempW5;
	}
	else
	{
		ldrtas16Rta2WhlCompCalcRef=(wheel_spd_3rd_temp+wheel_spd_min_temp)/2;
		ldrtas16Rta2WhlDctRef=wheel_spd_3rd_temp;
		ldrtas16Rta2WhlClearRef=wheel_spd_min_temp;
	}

	rta_2dct_temp_flg=0;
	if(RTA_RELIABLE_CONDITON_CHECK==1)
	{
		rta_2dct_temp_flg=1;

		wheel_diff_3rd_min=(int16_t)((int32_t)(ldrtas16Rta2WhlDctRef-ldrtas16Rta2WhlClearRef)*1000/ldrtas16Rta2WhlDctRef);
		wheel_diff_2nd_3rd=(int16_t)((int32_t)(wheel_spd_2nd_temp-ldrtas16Rta2WhlDctRef)*1000/ldrtas16Rta2WhlDctRef);

		if((FL.arad_avg	<= AFZ_0G5)&&(FR.arad_avg <= AFZ_0G5)&&(RL.arad_avg	<= AFZ_0G5)&&(RR.arad_avg <= AFZ_0G5)
			&&(McrAbs(arad_fl)<=ARAD_1G0)&&(McrAbs(arad_fr)<=ARAD_1G0)&&(McrAbs(arad_rl)<=ARAD_1G0)&&(McrAbs(arad_rr)<=ARAD_1G0))
		{
			rta_2dct_temp_flg=2;
			if((McrAbs(yaw_out)<YAW_3DEG)&&(McrAbs(alat)<LAT_0G15G))
			{
				rta_2dct_temp_flg=3;
				if(McrAbs((wheel_diff_3rd_min)<RTA_DET_THRES)&&((wheel_spd_max_temp-wheel_spd_min_temp)<MAX_WHL_DIFF_FOR_RTA2WHL_CLR))
				{
					rta_2dct_temp_flg=10;
					ldrtau1GoodCondForRta2WhlClear=1;
				}

				if((McrAbs(wheel_diff_3rd_min)<RTA_DET_THRES)&&(wheel_diff_2nd_3rd>RTA_DET_THRES))
				{
					rta_2dct_temp_flg=4;
					if((ETCS_ON==0)&&(TCS_ON==0)&&(eng_torq_abs<MAX_ENGINE_TORQ_RTA2WHL))
					{
						ldrtau1GoodCondForRta2WhlDct=1;
					}
					ldrtau1GoodCondForRta2WhlSus=1;
				}
			}
			else
			{
				rta_2dct_temp_flg=5;
			}
		}
		else
		{
			rta_2dct_temp_flg=6;
		}
	}
	else
	{
		rta_2dct_temp_flg=11;
	}

	if((RTA_RESET_STATE==1)||((RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_WL==1)||(RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_WL==1)))
	{

		RTA_FL.RTA_SUSPECT_2WHL=0;
		RTA_FR.RTA_SUSPECT_2WHL=0;
		RTA_RL.RTA_SUSPECT_2WHL=0;
		RTA_RR.RTA_SUSPECT_2WHL=0;

		RTA_FL.ldrtas16Rta2WhlDctCnt=0;
		RTA_FR.ldrtas16Rta2WhlDctCnt=0;
		RTA_RL.ldrtas16Rta2WhlDctCnt=0;
		RTA_RR.ldrtas16Rta2WhlDctCnt=0;

		RTA_FL.ldrtas16Rta2WhlCompRatio=1000;
		RTA_FR.ldrtas16Rta2WhlCompRatio=1000;
		RTA_RL.ldrtas16Rta2WhlCompRatio=1000;
		RTA_RR.ldrtas16Rta2WhlCompRatio=1000;

		RTA_FL.ldrtas16Rta2WhlDctRatioTemp=1000;
		RTA_FR.ldrtas16Rta2WhlDctRatioTemp=1000;
		RTA_RL.ldrtas16Rta2WhlDctRatioTemp=1000;
		RTA_RR.ldrtas16Rta2WhlDctRatioTemp=1000;

		ldrtau16RtaRatioCalcSum0[0]=0;

		for(rta_j=0;rta_j<4;rta_j++)
		{
			ldrtau16RtaRatioCalcSum0[rta_j]=0;
			ldrtau8Rta2WhlCompTimer1[rta_j]=0;
			ldrtau8Rta2WhlCompTimer2[rta_j]=0;
			for(rta_i=0;rta_i<5;rta_i++)
			{
				ldrtau16RtaRatioCalcSum[rta_j][rta_i]=1000;
			}
		}
	}
	else
	{
		if(RTA_WL_idx==0)
		{
			LDRTA_vDetRTA2WhlSuspect(&RTA_FL,&FL,ldrtas16Rta2WhlDctRef);
			LDRTA_vCalRTA2WhlCompRatio(&RTA_FL,FL.vrad_resol_change,ldrtas16Rta2WhlCompCalcRef,ldrtas16Rta2WhlDctRef,0);

		}
		else if(RTA_WL_idx==1)
		{
			LDRTA_vDetRTA2WhlSuspect(&RTA_FR,&FR,ldrtas16Rta2WhlDctRef);
			LDRTA_vCalRTA2WhlCompRatio(&RTA_FR,FR.vrad_resol_change,ldrtas16Rta2WhlCompCalcRef,ldrtas16Rta2WhlDctRef,1);

		}
		else if(RTA_WL_idx==2)
		{
			LDRTA_vDetRTA2WhlSuspect(&RTA_RL,&RL,ldrtas16Rta2WhlDctRef);
			LDRTA_vCalRTA2WhlCompRatio(&RTA_RL,RL.vrad_resol_change,ldrtas16Rta2WhlCompCalcRef,ldrtas16Rta2WhlDctRef,2);

		}
		else if(RTA_WL_idx==3)
		{
			LDRTA_vDetRTA2WhlSuspect(&RTA_RR,&RR,ldrtas16Rta2WhlDctRef);
			LDRTA_vCalRTA2WhlCompRatio(&RTA_RR,RR.vrad_resol_change,ldrtas16Rta2WhlCompCalcRef,ldrtas16Rta2WhlDctRef,3);

		}
		else
		{
			;
		}

		RTA_WL_idx++;
		if(RTA_WL_idx>=4)
		{
			RTA_WL_idx=0;
		}
	}
}
/*========================================================================*/
void LDRTA_vDetRTA2WhlSuspect(struct RTA_STRUCT	*pRTA_WL,struct	W_STRUCT *pRTA_Det,int16_t rta_dct_Ref)
{

	tempW3=(int16_t)((((int32_t)(pRTA_Det->vrad_resol_change-rta_dct_Ref))*1000)/((int32_t)rta_dct_Ref));

		/*Set Condition*/
	if(pRTA_WL->RTA_SUSPECT_2WHL==0)
	{
		if(ldrtau1GoodCondForRta2WhlDct==1)
		{
			if(tempW3>RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->ldrtas16Rta2WhlDctCnt>4)
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt-=4;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
				}

			}
			else if	(tempW3>RTA_DET_THRES)
			{
				if(McrAbs(tempW3-pRTA_WL->ldrtas16Rta2WhlDctRatioTemp)<RTA_DET_THRES)
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt+=4;
					if(pRTA_WL->ldrtas16Rta2WhlDctCnt>RTA_2WHL_DET_TIME)
					{
						pRTA_WL->RTA_SUSPECT_2WHL=1;
						pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
						pRTA_WL->ldrtas16Rta2WhlSusCnt=0;
					}
				}
				else
				{
					;
				}
			}
			else
			{
				if(pRTA_WL->ldrtas16Rta2WhlDctCnt>4)
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt-=4;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
				}


			}
		}
		else if(ldrtau1GoodCondForRta2WhlClear==1)
		{
			if (McrAbs(tempW3)<RTA_DET_THRES)
			{
				if(pRTA_WL->ldrtas16Rta2WhlDctCnt>50)
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt-=50;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
				}
			}
		}
		else
		{
			;
		}

		if(ldrtau1GoodCondForRta2WhlSus==1)
		{
			if(tempW3>RTA_DET_THRES_MAX)
			{
				if(pRTA_WL->ldrtas16Rta2WhlSusCnt>4)
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt-=4;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt=0;
				}

			}
			else if	(tempW3>RTA_DET_THRES)
			{
				if(McrAbs(tempW3-pRTA_WL->ldrtas16Rta2WhlDctRatioTemp)<RTA_DET_THRES)
				{
					if(pRTA_WL->ldrtas16Rta2WhlSusCnt<(RTA_2WHL_DET_TIME*2))
					{
						pRTA_WL->ldrtas16Rta2WhlSusCnt+=4;
					}

				}
				else
				{
					;
				}
			}
			else
			{
				if(pRTA_WL->ldrtas16Rta2WhlSusCnt>4)
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt-=4;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt=0;
				}


			}
		}
		else if(ldrtau1GoodCondForRta2WhlClear==1)
		{
			if (McrAbs(tempW3)<RTA_DET_THRES)
			{
				if(pRTA_WL->ldrtas16Rta2WhlSusCnt>50)
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt-=50;
				}
				else
				{
					pRTA_WL->ldrtas16Rta2WhlSusCnt=0;
				}
			}
		}
		else
		{
			;
		}

		if(pRTA_WL->ldrtas16Rta2WhlSusCnt<pRTA_WL->ldrtas16Rta2WhlDctCnt)
		{
			pRTA_WL->ldrtas16Rta2WhlSusCnt=pRTA_WL->ldrtas16Rta2WhlDctCnt;
		}
		#if	__REAR_D
		RTA_FL.ldrtas16Rta2WhlSusCnt=0;
		RTA_FR.ldrtas16Rta2WhlSusCnt=0;
		#else
		RTA_RL.ldrtas16Rta2WhlSusCnt=0;
		RTA_RR.ldrtas16Rta2WhlSusCnt=0;
		#endif
	}
	/*Reset	Condition*/
	else
	{
		if(ldrtau1GoodCondForRta2WhlClear==1)
		{
			if(McrAbs(tempW3)<RTA_DET_THRES)
			{
				if(McrAbs(tempW3-pRTA_WL->ldrtas16Rta2WhlDctRatioTemp)<RTA_DET_THRES)
				{
					pRTA_WL->ldrtas16Rta2WhlDctCnt+=4;
					if(pRTA_WL->ldrtas16Rta2WhlDctCnt>RTA_2WHL_RESET_TIME)
					{
						pRTA_WL->RTA_SUSPECT_2WHL=0;
						pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
					}
				}
			}
			else if((tempW3>RTA_DET_THRES)&&(tempW3<RTA_DET_THRES_MAX))
			{
				if(McrAbs(tempW3-pRTA_WL->ldrtas16Rta2WhlDctRatioTemp)<RTA_DET_THRES)
				{
					if(pRTA_WL->ldrtas16Rta2WhlDctCnt>4)
					{
						pRTA_WL->ldrtas16Rta2WhlDctCnt--;
					}
					else
					{
						pRTA_WL->ldrtas16Rta2WhlDctCnt=0;
					}
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
	}

	pRTA_WL->ldrtas16Rta2WhlDctRatioTemp=tempW3;
}
/*=======================================================================*/
void	LDRTA_vCalRTA2WhlCompRatio(struct RTA_STRUCT *pRTA_WL,int16_t wheel_speed,int16_t rta_comp_Ref,int16_t rta_dct_ref,uint8_t WL_idx)
{
	int16_t	rta_ratio_temp2;
	uint16_t rta_temp_sum;
	uint16_t rta_temp_max;
	uint16_t rta_temp_min;
	int16_t	rta_i;

	pRTA_WL->ldrtau8RtaCompFlgs=0;
	if((pRTA_WL->RTA_SUSPECT_2WHL==1)||((pRTA_WL->ldrtas16Rta2WhlDctCnt>=T_5_S)&&(pRTA_WL->RTA_SUSPECT_2WHL==0)))
	{
		if((ldrtau1GoodCondForRta2WhlDct==1)&&(wheel_speed>rta_dct_ref))
		{

			pRTA_WL->ldrtau8RtaCompFlgs=1;
			rta_ratio_temp2=(int16_t)((((int32_t)(rta_comp_Ref))*1000)/((int32_t)wheel_speed));

		/*RTA Ratio	Limitation:	75%~125%*/
			if(rta_ratio_temp2<RTA_COMP_RATIO_MIN)
			{
				pRTA_WL->ldrtau8RtaCompFlgs=2;
			}
			else if(rta_ratio_temp2>1000)
			{
				pRTA_WL->ldrtau8RtaCompFlgs=3;
			}
			else
			{
				pRTA_WL->ldrtau8RtaCompFlgs=4;
				ldrtau8Rta2WhlCompTimer1[WL_idx]++;
				ldrtau16RtaRatioCalcSum0[WL_idx]+=(uint16_t)rta_ratio_temp2;

				if(ldrtau8Rta2WhlCompTimer1[WL_idx]==13)
				{
					pRTA_WL->ldrtau8RtaCompFlgs=5;

					tempW1=(int16_t)(ldrtau8Rta2WhlCompTimer2[WL_idx]);
					ldrtau16RtaRatioCalcSum[WL_idx][tempW1]=ldrtau16RtaRatioCalcSum0[WL_idx]/13;

					ldrtau16RtaRatioCalcSum0[WL_idx]=0;
					ldrtau8Rta2WhlCompTimer1[WL_idx]=0;

					ldrtau8Rta2WhlCompTimer2[WL_idx]++;
					if(ldrtau8Rta2WhlCompTimer2[WL_idx]>=5)
					{
						ldrtau8Rta2WhlCompTimer2[WL_idx]=0;
						pRTA_WL->ldrtau8RtaCompFlgs=6;
					}

					rta_temp_max=ldrtau16RtaRatioCalcSum[WL_idx][0];
					rta_temp_min=ldrtau16RtaRatioCalcSum[WL_idx][0];

					if(rta_temp_max<ldrtau16RtaRatioCalcSum[WL_idx][1])
					{
						rta_temp_max=ldrtau16RtaRatioCalcSum[WL_idx][1];
					}
					else if	(rta_temp_min>ldrtau16RtaRatioCalcSum[WL_idx][1])
					{
						rta_temp_min=ldrtau16RtaRatioCalcSum[WL_idx][1];
					}
					else
					{
						;
					}

					if(rta_temp_max<ldrtau16RtaRatioCalcSum[WL_idx][2])
					{
						rta_temp_max=ldrtau16RtaRatioCalcSum[WL_idx][2];
					}
					else if	(rta_temp_min>ldrtau16RtaRatioCalcSum[WL_idx][2])
					{
						rta_temp_min=ldrtau16RtaRatioCalcSum[WL_idx][2];
					}
					else
					{
						;
					}

					if(rta_temp_max<ldrtau16RtaRatioCalcSum[WL_idx][3])
					{
						rta_temp_max=ldrtau16RtaRatioCalcSum[WL_idx][3];
					}
					else if	(rta_temp_min>ldrtau16RtaRatioCalcSum[WL_idx][3])
					{
						rta_temp_min=ldrtau16RtaRatioCalcSum[WL_idx][3];
					}
					else
					{
						;
					}

					if(rta_temp_max<ldrtau16RtaRatioCalcSum[WL_idx][4])
					{
						rta_temp_max=ldrtau16RtaRatioCalcSum[WL_idx][4];
					}
					else if	(rta_temp_min>ldrtau16RtaRatioCalcSum[WL_idx][4])
					{
						rta_temp_min=ldrtau16RtaRatioCalcSum[WL_idx][4];
					}
					else
					{
						;
					}

					rta_temp_sum=ldrtau16RtaRatioCalcSum[WL_idx][0]+ldrtau16RtaRatioCalcSum[WL_idx][1]+ldrtau16RtaRatioCalcSum[WL_idx][2]+ldrtau16RtaRatioCalcSum[WL_idx][3]+ldrtau16RtaRatioCalcSum[WL_idx][4];


					if((rta_temp_max-rta_temp_min)<RTA_COMP_RATIO_MAX_DEV)
					{
						tempW3=(int16_t)(rta_temp_sum/5);
						pRTA_WL->ldrtas16Rta2WhlCompRatio=LCABS_s16LimitMinMax(tempW3,RTA_COMP_RATIO_MIN,1000);
					}
					else
					{
						pRTA_WL->ldrtau8RtaCompFlgs=7;
					}
				}
				else
				{
					pRTA_WL->ldrtau8RtaCompFlgs=8;
				}
			}
		}
	}
	else
	{
		pRTA_WL->ldrtau8RtaCompFlgs=9;
		pRTA_WL->ldrtas16Rta2WhlCompRatio=1000;

		ldrtau16RtaRatioCalcSum0[WL_idx]=0;
		ldrtau8Rta2WhlCompTimer1[WL_idx]=0;
		ldrtau8Rta2WhlCompTimer2[WL_idx]=0;
		for(rta_i=0;rta_i<5;rta_i++)
		{
			ldrtau16RtaRatioCalcSum[WL_idx][rta_i]=1000;
		}

	}

	if(pRTA_WL->RTA_SUSPECT_2WHL==1)
	{
		pRTA_WL->ldrtas16Rta2WhlSusCompRatio=0;
		pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt=0;
		pRTA_WL->ldrtau8Rta2WhlSusRatioRest=0;
	}
	else if(pRTA_WL->ldrtas16Rta2WhlSusCnt>T_5_S)
	{
		#if	__REAR_D
		tempW0=(int16_t)(((int32_t)FL.vrad_resol_change+(int32_t)FR.vrad_resol_change)/2);
		#else
		tempW0=(int16_t)(((int32_t)RL.vrad_resol_change+(int32_t)RR.vrad_resol_change)/2);
		#endif

		tempW3=(int16_t)(((int32_t)(wheel_speed-tempW0)*1000)/tempW0);
		if((tempW3>RTA_DET_THRES)&&(tempW3<RTA_DET_THRES_MAX)&&(pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt<250))
		{
			pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt++;
			tempL0=tempW3+((int32_t)pRTA_WL->ldrtas16Rta2WhlSusCompRatio*(pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt-1))+pRTA_WL->ldrtau8Rta2WhlSusRatioRest;

			pRTA_WL->ldrtas16Rta2WhlSusCompRatio=(int16_t)(tempL0/((int32_t)pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt));
			pRTA_WL->ldrtau8Rta2WhlSusRatioRest=(uint8_t)(tempL0%((int32_t)pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt));
		}
	}
	else
	{
		pRTA_WL->ldrtas16Rta2WhlSusCompRatio=0;
		pRTA_WL->ldrtau8Rta2WhlSusRatioCalcCnt=0;
		pRTA_WL->ldrtau8Rta2WhlSusRatioRest=0;
	}
}
/*=============================================================================*/
void	LDABS_vArrangeRawWhlSpd(int16_t	INPUT_1,int16_t	INPUT_2,int16_t	INPUT_3,int16_t	INPUT_4)
{
	if(INPUT_1>INPUT_2)
	{
		tempW0=INPUT_1;
		tempW1=INPUT_2;
	}
	else
	{
		tempW0=INPUT_2;
		tempW1=INPUT_1;
	}

	if(INPUT_3>INPUT_4)
	{
		tempW2=INPUT_3;
		tempW3=INPUT_4;
	}
	else
	{
		tempW2=INPUT_4;
		tempW3=INPUT_3;
	}

	if(tempW0>tempW2)
	{
		tempW4=tempW0;
		if(tempW1>tempW2)
		{
			tempW5=tempW1;
			tempW6=tempW2;
			tempW7=tempW3;
		}
		else
		{
			tempW5=tempW2;
			if(tempW1>tempW3)
			{
				tempW6=tempW1;
				tempW7=tempW3;
			}
			else
			{
				tempW6=tempW3;
				tempW7=tempW1;
			}
		}
	}
	else
	{
		tempW4=tempW2;
		if(tempW0>tempW3)
		{
			tempW5=tempW0;
			if(tempW1>tempW3)
			{
				tempW6=tempW1;
				tempW7=tempW3;
			}
			else
			{
				tempW6=tempW3;
				tempW7=tempW1;
			}
		}
		else
		{
			tempW5=tempW3;
			tempW6=tempW0;
			tempW7=tempW1;
		}
	}
}
#endif

/*========================================================================*/
#else/*!__RTA_ENABLE*/
void	LDABS_vCompWheelSpeedForMini(struct	W_STRUCT *WL)
{
  #if __SPARE_WHEEL == ENABLE
	if(MINI_SPARE_WHEEL_wl==1)
	{
		tempW3=(int16_t)(((int32_t)WL->vrad_resol_change*U8_MINI_WHEEL_DOWN_RATE)/100);
		WL->vrad_crt_resol_change=tempW3;
		tempW3=tempW3>>3;

		WL->vrad_crt=tempW3;

	}
	else
	{
		WL->vrad_crt_resol_change=WL->vrad_resol_change;
		WL->vrad_crt=WL->vrad;
	}
  #else
	WL->vrad_crt_resol_change=WL->vrad_resol_change;
	WL->vrad_crt=WL->vrad;
  #endif

}
/*========================================================================================*/
void	LDABS_vDctMiniWheel(struct W_STRUCT	*pMini_Det,struct W_STRUCT *pMini_Ref)
{
#if	__SPARE_WHEEL /* prevent greece	QP(00.5.15)	*/

	if((ABS_fz==0) && (vref2 >=	(int16_t)VREF_25_KPH))
	{
	  #if __VDC
		if((ESP_BRAKE_CONTROL==0)&&(delta_yaw_first	< 300)&& (delta_yaw_first >	(-300)))
		{
	  #endif

		  #if __EDC
			if(EDC_vehicle_flag==0)
			{
		  #endif
				if(pMini_Det->vrad>pMini_Ref->vrad)
				{
					tempW2=pMini_Det->vrad-pMini_Ref->vrad;
				}
				else
				{
					tempW2=0;
				}
				tempW0=pMini_Ref->vrad;
				tempW1=100;
				s16mul16div16();

				tempW4=(100-(int16_t)U8_MINI_WHEEL_DOWN_RATE);
				tempW5=(tempW4-3);
				tempW6=(tempW4+10);

				if(tempW3 >= tempW5)
				{
					 /*	20%, SPIN_RATE	*/
					if(tempW3 >= tempW6	)
					{
						pMini_Det->mini_whl_counter=0;
						pMini_Det->MINI_SPARE_WHEEL=0;
					}
					else
					{
						if(pMini_Det->mini_whl_counter < 195)
						{
							pMini_Det->mini_whl_counter++;
						}
						else
						{
							;
						}
					}
				}
				else
				{
					if(pMini_Det->mini_whl_counter != 0)
					{
						pMini_Det->mini_whl_counter--;
					}
					else
					{
						;
					}
				}

				if(pMini_Det->mini_whl_counter >= 142)
				{
				   pMini_Det->MINI_SPARE_WHEEL=1;
				}

				else if(pMini_Det->mini_whl_counter	<= 70)
				{
					pMini_Det->MINI_SPARE_WHEEL=0;
				}
				else
				{
					;
				}
		  #if __EDC
			}
			else
			{
				;
			}
		  #endif

	  #if __VDC
		}
		else
		{
			;
		}
	  #endif
	}
	else
	{
		;
	}
	if(vref	<= (int16_t)VREF_2_5_KPH)
	{
		pMini_Det->MINI_SPARE_WHEEL=0;
		pMini_Det->mini_whl_counter=0;
	}
	else
	{
		;
	}
#else
	pMini_Det->MINI_SPARE_WHEEL=0;
	pMini_Det->mini_whl_counter=0;
#endif	/*SPARE_WHEEL*/
}
#endif	/*__RTA_ENABLE*/


/*========================================================================*/
#if	defined(__LOGGER)
#if	__LOGGER_DATA_TYPE==1
#if	__VDC
#if	__RTA_ENABLE

#include	"LSCallLogData.h"

void	LSRTA_vCallRTALogData(void)
{
		uint8_t	i;
		CAN_LOG_DATA[0]	= (uint8_t)(system_loop_counter);
		CAN_LOG_DATA[1]	= (uint8_t)(system_loop_counter>>8);		  
		CAN_LOG_DATA[2]	= (uint8_t)(FL.vrad_resol_change);
		CAN_LOG_DATA[3]	= (uint8_t)(FL.vrad_resol_change>>8);
		CAN_LOG_DATA[4]	= (uint8_t)(FR.vrad_resol_change);
		CAN_LOG_DATA[5]	= (uint8_t)(FR.vrad_resol_change>>8);
		CAN_LOG_DATA[6]	= (uint8_t)(RL.vrad_resol_change);
		CAN_LOG_DATA[7]	= (uint8_t)(RL.vrad_resol_change>>8);
		CAN_LOG_DATA[8]	= (uint8_t)(RR.vrad_resol_change);
		CAN_LOG_DATA[9]	= (uint8_t)(RR.vrad_resol_change>>8);

		CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);
		CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);

		CAN_LOG_DATA[12] = (uint8_t)(FZ1.vrselect_resol_change);
		CAN_LOG_DATA[13] = (uint8_t)(FZ1.vrselect_resol_change>>8);
		CAN_LOG_DATA[14] = (uint8_t)(FL.vrad_crt_resol_change);
		CAN_LOG_DATA[15] = (uint8_t)(FL.vrad_crt_resol_change>>8);
		CAN_LOG_DATA[16] = (uint8_t)(FR.vrad_crt_resol_change);
		CAN_LOG_DATA[17] = (uint8_t)(FR.vrad_crt_resol_change>>8);
		CAN_LOG_DATA[18] = (uint8_t)(RL.vrad_crt_resol_change);
		CAN_LOG_DATA[19] = (uint8_t)(RL.vrad_crt_resol_change>>8);
		CAN_LOG_DATA[20] = (uint8_t)(RR.vrad_crt_resol_change);
		CAN_LOG_DATA[21] = (uint8_t)(RR.vrad_crt_resol_change>>8);
		CAN_LOG_DATA[22] = (uint8_t)(FL.arad);
		CAN_LOG_DATA[23] = (uint8_t)(FR.arad);
		CAN_LOG_DATA[24] = (uint8_t)(RL.arad);
		CAN_LOG_DATA[25] = (uint8_t)(RR.arad);
		CAN_LOG_DATA[26] = (int8_t)(FL.rel_lam);
		CAN_LOG_DATA[27] = (int8_t)(FR.rel_lam);
		CAN_LOG_DATA[28] = (int8_t)(RL.rel_lam);
		CAN_LOG_DATA[29] = (int8_t)(RR.rel_lam);
		#if	__RTA_2WHEEL_DCT
		CAN_LOG_DATA[30] = (uint8_t)(RTA_FL.ldrtas16Rta2WhlCompRatio/10);
		CAN_LOG_DATA[31] = (uint8_t)(RTA_FR.ldrtas16Rta2WhlCompRatio/10);
		CAN_LOG_DATA[32] = (uint8_t)(RTA_RL.ldrtas16Rta2WhlCompRatio/10);
		CAN_LOG_DATA[33] = (uint8_t)(RTA_RR.ldrtas16Rta2WhlCompRatio/10);
		#else
		CAN_LOG_DATA[30] = (uint8_t)(FL.flags);
		CAN_LOG_DATA[31] = (uint8_t)(FR.flags);
		CAN_LOG_DATA[32] = (uint8_t)(RL.flags);
		CAN_LOG_DATA[33] = (uint8_t)(RR.flags);
		#endif

		#if	__RTA_2WHEEL_DCT
		CAN_LOG_DATA[34] = (uint8_t)(RTA_FL.ldrtas16Rta2WhlDctCnt);
		CAN_LOG_DATA[35] = (uint8_t)(RTA_FL.ldrtas16Rta2WhlDctCnt>>8);
		CAN_LOG_DATA[36] = (uint8_t)(RTA_FR.ldrtas16Rta2WhlDctCnt);
		CAN_LOG_DATA[37] = (uint8_t)(RTA_FR.ldrtas16Rta2WhlDctCnt>>8);
		CAN_LOG_DATA[38] = (uint8_t)(RTA_RL.ldrtas16Rta2WhlDctCnt);
		CAN_LOG_DATA[39] = (uint8_t)(RTA_RL.ldrtas16Rta2WhlDctCnt>>8);
		CAN_LOG_DATA[40] = (uint8_t)(RTA_RR.ldrtas16Rta2WhlDctCnt);
		CAN_LOG_DATA[41] = (uint8_t)(RTA_RR.ldrtas16Rta2WhlDctCnt>>8);

		#else
		CAN_LOG_DATA[34] = (uint8_t)(FL.arad_avg);
		CAN_LOG_DATA[35] = (uint8_t)(FL.arad_avg>>8);
		CAN_LOG_DATA[36] = (uint8_t)(FR.arad_avg);
		CAN_LOG_DATA[37] = (uint8_t)(FR.arad_avg>>8);
		CAN_LOG_DATA[38] = (uint8_t)(RL.arad_avg);
		CAN_LOG_DATA[39] = (uint8_t)(RL.arad_avg>>8);
		CAN_LOG_DATA[40] = (uint8_t)(RR.arad_avg);
		CAN_LOG_DATA[41] = (uint8_t)(RR.arad_avg>>8);
		#endif


		CAN_LOG_DATA[42] = (uint8_t)(RTA_FL.det_rta_suspect_cnt);
		CAN_LOG_DATA[43] = (uint8_t)(RTA_FL.det_rta_suspect_cnt>>8);
		CAN_LOG_DATA[44] = (uint8_t)(RTA_FR.det_rta_suspect_cnt);
		CAN_LOG_DATA[45] = (uint8_t)(RTA_FR.det_rta_suspect_cnt>>8);
		CAN_LOG_DATA[46] = (uint8_t)(RTA_RL.det_rta_suspect_cnt);
		CAN_LOG_DATA[47] = (uint8_t)(RTA_RL.det_rta_suspect_cnt>>8);
		CAN_LOG_DATA[48] = (uint8_t)(RTA_RR.det_rta_suspect_cnt);
		CAN_LOG_DATA[49] = (uint8_t)(RTA_RR.det_rta_suspect_cnt>>8);
		CAN_LOG_DATA[50] = (uint8_t)(RTA_FL.det_rta_suspect_cnt4>250)?250:((RTA_FL.det_rta_suspect_cnt4<0)?0:(uint8_t)RTA_FL.det_rta_suspect_cnt4);
		CAN_LOG_DATA[51] = (uint8_t)(RTA_FR.det_rta_suspect_cnt4>250)?250:((RTA_FR.det_rta_suspect_cnt4<0)?0:(uint8_t)RTA_FR.det_rta_suspect_cnt4);		
		CAN_LOG_DATA[52] = (uint8_t)(RTA_RL.det_rta_suspect_cnt4>250)?250:((RTA_RL.det_rta_suspect_cnt4<0)?0:(uint8_t)RTA_RL.det_rta_suspect_cnt4);				
		CAN_LOG_DATA[53] = (uint8_t)(RTA_RR.det_rta_suspect_cnt4>250)?250:((RTA_RR.det_rta_suspect_cnt4<0)?0:(uint8_t)RTA_RR.det_rta_suspect_cnt4);
	
		CAN_LOG_DATA[54] = (uint8_t)(rta_ratio);
		CAN_LOG_DATA[55] = (uint8_t)(rta_ratio>>8);

		CAN_LOG_DATA[56] = (vref_output_inc_limit>250)?250:((vref_output_inc_limit<0)?0:(uint8_t)vref_output_inc_limit);
		CAN_LOG_DATA[57] = (vref_output_dec_limit>127)?127:((vref_output_dec_limit<-127)?-127:(int8_t)vref_output_dec_limit);

		CAN_LOG_DATA[58] = ((ldrtau16RtaAbsnConfirmCnt/2)>250)?250:(((ldrtau16RtaAbsnConfirmCnt/2)<0)?0:(uint8_t)(ldrtau16RtaAbsnConfirmCnt/2));
		CAN_LOG_DATA[59] = ((ldrtau16RtaPresConfirmCnt/2)>250)?250:(((ldrtau16RtaPresConfirmCnt/2)<0)?0:(uint8_t)(ldrtau16RtaPresConfirmCnt/2));

		CAN_LOG_DATA[60] = (uint8_t)(mpress/10);
		CAN_LOG_DATA[61] = (uint8_t)(yaw_out/100);
		CAN_LOG_DATA[62] = (uint8_t)((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(int8_t)(wstr/10));
		CAN_LOG_DATA[63] = (uint8_t)((alat/10)>127)?127:(((alat/10)<-127)?-127:(int8_t)(alat/10));
		CAN_LOG_DATA[64] = (uint8_t)(eng_torq);
		CAN_LOG_DATA[65] = (uint8_t)(eng_torq>>8);
		CAN_LOG_DATA[66] = (uint8_t) mtp;
		CAN_LOG_DATA[67] = (uint8_t)(ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(int8_t)ebd_refilt_grv);
		CAN_LOG_DATA[68] = (uint8_t) ldrtau8RtaState;//(inhibition_number =INHIBITION_INDICATOR());
		CAN_LOG_DATA[69] = (uint8_t)wheel_selection_mode;//wheel_selection_mode;//(int8_t)(cal_torq/10);

		

		i=0;
		if (ABS_fz							==1) i|=0x01;
		if (BRAKE_SIGNAL					==1) i|=0x02;
		if (BTCS_ON							==1) i|=0x04;
		if (ETCS_ON							==1) i|=0x08;
		if (TCS_ON							==1) i|=0x10;
		if (VDC_REF_WORK					==1) i|=0x20;
		if (YAW_CDC_WORK					==1) i|=0x40;
		if (ESP_ERROR_FLG					==1) i|=0x80;			

		CAN_LOG_DATA[70] = i;
		
		i=0;

        if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) i|=0x01;
       	if (BRAKING_STATE					==1) i|=0x02;
		if (BLS								==1) i|=0x04;
		if (RL.PARKING_BRAKE_WHEEL			==1) i|=0x05;
		if (RR.PARKING_BRAKE_WHEEL			==1) i|=0x10;
		if (PARKING_BRAKE_OPERATION			==1) i|=0x20;
#if __4WD||(__4WD_VARIANT_CODE==ENABLE)
        if (BIG_ACCEL_SPIN_FZ               ==1) i|=0x40; 
        if (ACCEL_SPIN_FZ                   ==1) i|=0x40; 
#endif						
		CAN_LOG_DATA[71] = i;

		i =	0;

		if (ABS_fl							==1) i|=0x01;
		if (ABS_fr							==1) i|=0x02;
		if (ABS_rl							==1) i|=0x04;
		if (ABS_rr							==1) i|=0x08;
		if (FL.RELIABLE_FOR_VREF						==1) i|=0x10;
		if (FR.RELIABLE_FOR_VREF						==1) i|=0x20;
		if (RL.RELIABLE_FOR_VREF						==1) i|=0x40;
		if (RR.RELIABLE_FOR_VREF						==1) i|=0x80;
		CAN_LOG_DATA[72] = i;

		i =	0;

		if (HV_VL_fl						==1) 	i|=0x01;
		if (AV_VL_fl						==1) 	i|=0x02;
		if (HV_VL_fr						==1) 	i|=0x04;
		if (AV_VL_fr						==1) 	i|=0x08;
		if (HV_VL_rl					   	==1)	i|=0x10;
		if (AV_VL_rl					   	==1)	i|=0x20;
		if (HV_VL_rr					   	==1)	i|=0x40;
		if (AV_VL_rr					   	==1)	i|=0x80;
		CAN_LOG_DATA[73] = i;

		i=0;

		if (SAS_CHECK_OK						==1) 	i|=0x01;
		if (RTA_RESET_STATE						==1) 	i|=0x02;			
		if (WHEEL_ERROR_STATE				   	==1)	i|=0x04;			
		if (RTA_BTCS_ON_CHK					  	==1) 	i|=0x08;
		if (RTA_RELIABLE_CONDITON_CHECK			==1) 	i|=0x10;
		#if	__RTA_2WHEEL_DCT	
		if (ldrtau1GoodCondForRta2WhlDct		==1)	i|=0x20;
		if (ldrtau1GoodCondForRta2WhlClear		==1)	i|=0x40;
		if (ldrtau1GoodCondForRta2WhlSus	   	==1)	i|=0x80;
		#endif
		CAN_LOG_DATA[74] = i;


		i =	0;

		if (RTA_FL.RTA_RELIABLE_CONDITON_CHECK_WHEEL						  ==1) i|=0x01;
		if (RTA_FR.RTA_RELIABLE_CONDITON_CHECK_WHEEL						  ==1) i|=0x02;
		if (RTA_RL.RTA_RELIABLE_CONDITON_CHECK_WHEEL						  ==1) i|=0x04;
		if (RTA_RR.RTA_RELIABLE_CONDITON_CHECK_WHEEL						  ==1) i|=0x08;
		if (RTA_FL.RTA_SUSPECT_WL					   	==1) i|=0x10;
		if (RTA_FR.RTA_SUSPECT_WL						==1) i|=0x20;
		if (RTA_RL.RTA_SUSPECT_WL						==1) i|=0x40;
		if (RTA_RR.RTA_SUSPECT_WL						==1) i|=0x80;

		CAN_LOG_DATA[75] = i;

		i =	0;
		#if	__RTA_2WHEEL_DCT
		if (RTA_FL.RTA_SUSPECT_2WHL							==1) i|=0x01;
		if (RTA_FR.RTA_SUSPECT_2WHL							 ==1) i|=0x02;
		if (RTA_RL.RTA_SUSPECT_2WHL							 ==1) i|=0x04;
		if (RTA_RR.RTA_SUSPECT_2WHL							 ==1) i|=0x08;
		#else
		if (FL.High_TO_Low_Suspect_flag							==1) i|=0x01;
		if (FR.High_TO_Low_Suspect_flag							 ==1) i|=0x02;
		if (RL.High_TO_Low_Suspect_flag							 ==1) i|=0x04;
		if (RR.High_TO_Low_Suspect_flag							 ==1) i|=0x08;
		#endif

		if (FL.MINI_SPARE_WHEEL						 ==1) i|=0x10;
		if (FR.MINI_SPARE_WHEEL						  ==1) i|=0x20;
		if (RL.MINI_SPARE_WHEEL						  ==1) i|=0x40;
		if (RR.MINI_SPARE_WHEEL						  ==1) i|=0x80;

		CAN_LOG_DATA[76] = i;

		i =	0;
		if (RTA_FL.ldabsu1DetSpareTire							==1) i|=0x01;
		if (RTA_FR.ldabsu1DetSpareTire							==1) i|=0x02;
		if (RTA_RL.ldabsu1DetSpareTire							==1) i|=0x04;
		if (RTA_RR.ldabsu1DetSpareTire							==1) i|=0x08;

		if (FL.WHL_SELECT_FLG							==1) i|=0x10;
		if (FR.WHL_SELECT_FLG							==1) i|=0x20;
		if (RL.WHL_SELECT_FLG							==1) i|=0x40;
		if (RR.WHL_SELECT_FLG							==1) i|=0x80;
		CAN_LOG_DATA[77] = i;

		i =	0;

		if (FL.RELIABLE_FOR_VREF2						==1) i|=0x01;
		if (FR.RELIABLE_FOR_VREF2						==1) i|=0x02;
		if (RL.RELIABLE_FOR_VREF2						==1) i|=0x04;
		if (RR.RELIABLE_FOR_VREF2						==1) i|=0x08;			
        if (RTA_FL.RTA_SUSPECT_WL_FOR_BTCS           	==1) i|=0x10;           
        if (RTA_FR.RTA_SUSPECT_WL_FOR_BTCS     			==1) i|=0x20;         
        if (RTA_RL.RTA_SUSPECT_WL_FOR_BTCS           	==1) i|=0x40;       
        if (RTA_RR.RTA_SUSPECT_WL_FOR_BTCS           	==1) i|=0x80;          

		CAN_LOG_DATA[78] = i;

		i =	0;
		if (TCL_DEMAND_fl								==1) i|=0x01;
		if (TCL_DEMAND_fl							==1) i|=0x02;
		if (S_VALVE_LEFT							==1) i|=0x04;
		if (S_VALVE_RIGHT							==1) i|=0x08;
		if (ESP_BRAKE_CONTROL_FL						==1) i|=0x10;
		if (ESP_BRAKE_CONTROL_FR						==1) i|=0x20;
		if (ESP_BRAKE_CONTROL_RL						 ==1) i|=0x40;
		if (ESP_BRAKE_CONTROL_RR						 ==1) i|=0x80;
		CAN_LOG_DATA[79] = i;
}

#endif
#endif
#endif
#endif
/*==================================================================*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDRTACallDetection
	#include "Mdyn_autosar.h"
#endif
