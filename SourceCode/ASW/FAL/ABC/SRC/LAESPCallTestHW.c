/******************************************************************************
* Project Name: Hardware Test
* File: LAESPCallTestHW.c
* Description:
* Date: 2012. 12. 13
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LAESPCallTestHW
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/
#include "LACallMain.h"
#include "LAESPCallTestHW.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"

#include "LAESPCallActHW.h"
#include "LATCSCallActHW.h"
#include "LACallMultiplex.h"
#include "LCTCSCallControl.h"
#include "LCABSCallControl.h"
/* Local Definiton  *********************************************************/

U8_BIT_STRUCT_t	ESPHWF1;

//WHEEL_VV_OUTPUT_t laesp_FL1HP, laesp_FR1HP, laesp_RL1HP, laesp_RR1HP;
//WHEEL_VV_OUTPUT_t laesp_FL2HP, laesp_FR2HP, laesp_RL2HP, laesp_RR2HP;

uint16_t laespu16BenchTest_cnt;

uint8_t laespu8TestModeID, laespu8TestModeID_old, laespu8TestModePhase, laespu8TestModePhase_old;

uint16_t	laidbu16TestMain_cnt, laidbu16TestSub_cnt;

int16_t	laidbs16WheelTPFL, laidbs16WheelTPFR, laidbs16WheelTPRL, laidbs16WheelTPRR;

int16_t laesps16PedalOverWriteTemp, laesps16PedalOverWriteTemp2;
uint8_t laespu8DumpTime, laespu8DumpTime2;

/* Variables Definition*******************************************************/

U8_BIT_STRUCT_t		ESPHWF0;
int8_t laesps8Mon01, laesps8Mon02, laesps8Mon03;
int16_t laesps16Mon11, laesps16Mon12;
int16_t laespu16MotorTargetV;

int16_t laesps16PedalOverWrite;
uint8_t laespu8CircVlvCmdOverWrite;

int16_t laesps16DelPress;
int16_t laesps16WhlPressBefore;
int16_t laesps16WhlPressAfter;
int16_t laesps16DelWhlPress;
struct CALC_AVG WhlPBefore, WhlPAfter, DelWhlP, DelP;

extern int16_t	leb_s16g_PosiRefOld;
int16_t	laidbs16Pres2iq;
int16_t	laidbs16Pres2iq_old;
int16_t laidbs16PosiHold;
int16_t	laidbs16iqHold;

int16_t liu1TestModeAct_ESC_Temp;

int16_t laesps16EscTestModeTP;

//extern PMSWF	leb_PMSWFg_Pri;
//extern PMSWF	leb_PMSWFg_Sec;

/* LocalFunction prototype ***************************************************/

static void LAIDB_vCallValveActAssistMain(void);
#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAIDB_vCallRiseRateTestMain(void);
#endif
#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
static void LAIDB_vPMatchingMain(void);
#endif
#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
static void LAIDB_vCallPulseUpDownMain(void);
#endif
#if (TEST_MODULE_SELECT==__CURR_PRES_MAPPING_MODE)
static void LAIDB_vCurrPressMapping(void);
#endif
static void LAIDB_vRunAWMapping(void);
static void LAIDB_vRunMasterMapping(void);
static void LAIDB_vRunCurrMapping(void);

static void LAESP_vInhibitTestMode(void);
static void LAESP_vResetValveMotor(void);
//static void LAESP_vDecideActWheel(void);
static void LAESP_vSetTrigger(void);
static void LAESP_vDetectTriggerAct(void);
static void	LAESP_vCountTestRun(void);
static void	LAESP_vDetectTestRunExit(void);
#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
static void	LAESP_vDetectTestRunExitByExt(void);
#endif
//static void	LAESP_vSetMotorTargetVoltage(void);
//static void LAESP_vActMotor(void);
static void LAESP_vSetTestID(void);
static void LAESP_vSetTestModePhase(void);
#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAESP_vRunRiseRate(void);
#endif
static void LAESP_vInitializeValveMotor(void);
#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAESP_vOneWheelRiseFL(void);
static void LAESP_vOneWheelRiseFR(void);
static void LAESP_vOneWheelRiseRL(void);
static void LAESP_vOneWheelRiseRR(void);

static void LAESP_vTwoWheelRisePri(void);
static void LAESP_vTwoWheelRiseSec(void);
static void LAESP_vTwoWheelRiseLeft(void);
static void LAESP_vTwoWheelRiseRight(void);
static void LAESP_vAllWheelRise(void);
#endif
//static void LAESP_vAllWheelNCOpen(void);
/*
static void LAESP_vRunTestCase01(void);
static void LAESP_vRunTestCase02(void);
static void LAESP_vRunTestCase03(void);
*/

static void LAIDB_vSelectHoldValve(void);
static void LAIDB_vSelectDumpValve(void);
static void LAIDB_vSelectCircValve(void);
static void LAIDB_vActWheelValveHold(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);
static void LAIDB_vActWheelValveDump(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);
static void LAIDB_vActWheelValveDumpScan(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8DumpTime);
static void LAIDB_vActWheelValveCirc(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);
#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
static void LAIDB_vSelectPulseActValve(void);
static void LAIDB_vCountPulseTestMode(void);
static void LAIDB_vInitializeTestCount(void);
#endif
static void LAIDB_vInitializeValve(void);
#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
static void LAESP_vRunPressMatch(void);
static void LAIDB_vPressMatch01(void);
static void LAIDB_vPressMatch02(void);
static void LAIDB_vPressMatch03(void);
static void LAIDB_vPressMatch04(void);
static void LAIDB_vPressMatch05(void);
static void LAIDB_vPressMatch06(void);
static void LAIDB_vPressMatch07(void);
static void LAIDB_vPressMatch08(void);
static void LAIDB_vPressMatch09(void);
static void LAIDB_vPressMatch10(void);
static void LAIDB_vPressMatch11(void);
static void LAIDB_vPressMatch12(void);
static void LAIDB_vPressMatch13(void);
static void LAIDB_vPressMatch14(void);
static void LAIDB_vPressMatch15(void);
static void LAIDB_vPressMatch16(void);

static void LAIDB_vIqStepRise5Bar(void);
static void LAIDB_vIqStepRise10Bar(void);
static void LAIDB_vIqStepRise20Bar(void);
#endif
#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAIDB_vMultiplexTest(void);
static void LAIDB_vGenCircTP_sine(void);
#endif
static void LAIDB_vGenCircTP_triangular(void);
#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAIDB_vGenCircTP_step(void);
//static void LAIDB_vGenCircTP_ramp(void);

static void LAIDB_vRunSineTarget(void);

static void LAIDB_vRunStepTarget(void);
static void LAIDB_vRunSineTarget_Rvr(void);
static void LAIDB_vRunStepTarget_Rvr(void);
static void LAIDB_vRunMultiTest(void);
#endif
static void LAIDB_vSetWheelTarget(void);
#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
static void LAIDB_vRetractionTest(void);

static void LAIDB_vRunRetractionTest(void);
static void LAIDB_vRunRetraction(void);
#endif
#if (__ASSIST_TYPE == __TIMMING_SYNC)
static void LAIDB_vRunSineTargetAssist(void);
static void LAIDB_vRunStepTargetAssist(void);
static void LAIDB_vRunValveAssistTest(void);
#endif
#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
static void LAIDB_vVolumeChangeTestMain(void);
static void LAESP_vRunVolChange(void);
#endif

static void LAESP_vVolFL2FR(void);
#if (TEST_MODULE_SELECT==__TEMP_MODE)
static void LAIDB_vTemporaryTestMain(void);

static void LAESP_vRunTemporaryTest(void);

static void LAIDB_vTemporaryTestMain(void);
static void LAESP_vRunTemporaryTest(void);
static void	LAESP_vAllWheelTarget(void);
#endif
#if (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE)
static void LAESP_vVlvOpenTiTestMain(void);
static void LAESP_vRunVlvOpenTiTest(void);
static void LAESP_vWhlP1PulseUp(void);
static void LAESP_vWhlP1PulseUp_pedal1(void);
static void LAESP_vWhlP1PulseUp_pedal2(void);
static void LAESP_vWhlP1PulseUp_pedal3(void);
static void LAESP_vWhlP1PulseUp_pedal4(void);
static void LAESP_vWhlP1ESCTarget1(void);
static void LAESP_vWhlP1ESCTarget2(void);
static void LAESP_vWhlP1ESCTarget3(void);
static void LAESP_vWhlP1ESCTarget4(void);
static void LAESP_vCalcTestData(void);
#endif /* (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE)
static void LAESP_vDumpTiTestMain(void);
static void LAESP_vRunDumpTiTest(void);
static void LAESP_vWhlP1Dump_pedal1(void);
static void LAESP_vWhlP1Dump_ABS(void);
static void LAESP_vAltTestPrmtByID(void);
#endif /* (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE) */
/* GlobalFunction prototype ***************************************************/

void LAIDB_vCallTestMode(void);

/* Implementation*************************************************************/

void LAIDB_vCallTestMode(void)
{
	#if (TEST_MODULE_SELECT==__VALVE_ACT_ASSIST_MODE)
	LAIDB_vCallValveActAssistMain();
	#endif

	#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
	LAIDB_vCallPulseUpDownMain();
	#endif

	#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
	LAIDB_vCallRiseRateTestMain();
	#endif

	#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
	LAIDB_vPMatchingMain();
	#endif

	#if (TEST_MODULE_SELECT==__CURR_PRES_MAPPING_MODE)
	LAIDB_vCurrPressMapping();
	#endif

	#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
	LAIDB_vMultiplexTest();
	#endif

	#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
	LAIDB_vRetractionTest();
	#endif

	#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
	LAIDB_vVolumeChangeTestMain();
	#endif

	#if (TEST_MODULE_SELECT==__TEMP_MODE)
	LAIDB_vTemporaryTestMain();
	#endif

	#if (TEST_MODULE_SELECT== VALVE_OPEN_TIME_TEST_MODE)
	LAESP_vVlvOpenTiTestMain();
	#endif
	#if (TEST_MODULE_SELECT== DUMP_TIME_TEST_MODE)
	LAESP_vDumpTiTestMain();
	#endif

}

#if (TEST_MODULE_SELECT==__TEMP_MODE)
static void LAIDB_vTemporaryTestMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vRunTemporaryTest();
	}
	else{}
}
#endif /* (TEST_MODULE_SELECT==__TEMP_MODE) */

#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
void LAIDB_vVolumeChangeTestMain(void)
{
	LAESP_vInhibitTestMode();
	//LAESP_vSetTrigger();
	//LAESP_vDetectTriggerAct();

	//LAESP_vDetectTestRunExit();
	LAESP_vDetectTestRunExitByExt();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vRunVolChange();
}
	else{}
}
#endif /* (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE) */

static void LAIDB_vCallValveActAssistMain(void)
{
#if (__ASSIST_TYPE == __TIMMING_SYNC) //BBS interface 불가로 임시조치
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAIDB_vRunValveAssistTest();
	}
	else{}
#elif (__ASSIST_TYPE == __ON_OFF_BY_TRIGGER)

	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		/*
		if(S16_RBC_LOGIC_CAL_TEMP_01==1)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		}
		else{}

		if(S16_RBC_LOGIC_CAL_TEMP_02==1)
		{
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		}
		else{}

		if(S16_RBC_LOGIC_CAL_TEMP_03==1)
		{
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		}
		else{}

		if(S16_RBC_LOGIC_CAL_TEMP_04==1)
		{
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}
		*/
		if(wstr>0) /* FL circuit, rear wheel open */
		{
			if(wstr<WSTR_30_DEG)
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr<WSTR_60_DEG)
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr<WSTR_90_DEG)
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr<WSTR_120_DEG)
			{
				//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else
	{
				//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
		}
		else /* FR circuit, front wheel open */
		{
			if(wstr>(-WSTR_30_DEG))
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr>(-WSTR_60_DEG))
			{
				//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr>(-WSTR_90_DEG))
			{
				//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else if(wstr>(-WSTR_120_DEG))
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
			}
			else
			{
				LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
				LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
		}
	}
	else{}
#else /* Interface Type */
		//if(liu1FlgTestOn==1)
	//{
		LAIDB_vInitializeValve();
		LAIDB_vSelectHoldValve();
		LAIDB_vSelectDumpValve();
		LAIDB_vSelectCircValve();
	//}
	//else
	//{
	//	;
	//}
#endif
}

#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
static void LAIDB_vCallPulseUpDownMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();
	LAESP_vDetectTestRunExit();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAIDB_vCountPulseTestMode();
		if(laidbu16TestMain_cnt>U16_PULSE_START_DELAY_TIME)
		{
			ESP_BRAKE_CONTROL_FL=1;
			//leb_u16g_FlgHoldFL=1;
			//leb_u16g_FlgHoldFR=1;
			//leb_u16g_FlgHoldRL=1;
			//leb_u16g_FlgHoldRR=1;
		}
		LAIDB_vInitializeValve();
		LAIDB_vSelectHoldValve();
		LAIDB_vSelectDumpValve();
		LAIDB_vSelectPulseActValve();
	}
	else
	{
		LAIDB_vInitializeTestCount();
		//ESP_BRAKE_CONTROL_FL=0;  overwrite
		//leb_u16g_FlgHoldFL=0;
		//leb_u16g_FlgHoldFR=0;
		//leb_u16g_FlgHoldRL=0;
		//leb_u16g_FlgHoldRR=0;
	}
}
#endif

#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
void LAIDB_vCallRiseRateTestMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vRunRiseRate();
	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
void LAIDB_vPMatchingMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vRunPressMatch();
	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT==__CURR_PRES_MAPPING_MODE)
static void LAIDB_vCurrPressMapping(void)
{

	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAIDB_vRunCurrMapping();

	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAIDB_vMultiplexTest(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAIDB_vRunMultiTest();

	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
static void LAIDB_vRetractionTest(void)
{

	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAIDB_vRunRetractionTest();

	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT== VALVE_OPEN_TIME_TEST_MODE)
static void LAESP_vVlvOpenTiTestMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vRunVlvOpenTiTest();
	}
	else{}
}
#endif

#if (TEST_MODULE_SELECT== DUMP_TIME_TEST_MODE)
static void LAESP_vDumpTiTestMain(void)
{
	LAESP_vInhibitTestMode();
	LAESP_vSetTrigger();
	LAESP_vDetectTriggerAct();

	LAESP_vDetectTestRunExit();
	LAESP_vCountTestRun();

	LAESP_vSetTestID();

	if((laidbu1TestModeOn==1)&&(laespu1InhibitTestMode==0))
	{
		LAESP_vSetTestModePhase();
		LAESP_vAltTestPrmtByID();
		LAESP_vRunDumpTiTest();
	}
	else{}
}
#endif /* (TEST_MODULE_SELECT== DUMP_TIME_TEST_MODE) */


/*==================================================================================*/
/*	Purpose : IDB Control에서 Test mode 작동 falg를 전달받아, 작동시간 count		*/
/*																					*/
/*	Input Variable :																*/
/*																		 			*/
/*	Output Variable : 																*/
/*==================================================================================*/
#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
static void LAIDB_vCountPulseTestMode(void)
{
	laidbu16TestMain_cnt++;

	if(laidbu16TestMain_cnt>=U16_PULSE_START_DELAY_TIME)
	{
		laidbu16TestSub_cnt++;
		if(laidbu16TestSub_cnt>U16_PRESSURE_HOLD_TIME)
		{
			laidbu16TestSub_cnt=0;
		}
	}
	else{}

	if(laidbu16TestMain_cnt>U16_TEST_RUNNING_TIME)
	{
		laidbu16TestMain_cnt=0;
	}
}

#endif
/*==================================================================================*/
/*	Purpose : IDB Control에서 wheel valve 작동 falg를 전달받아, 해당 valve를 hold	*/
/*																					*/
/*	Input Variable : leb_u16g_FlgCloseFL : FL hold command							*/
/*																		 			*/
/*	Output Variable : 																*/
/*==================================================================================*/
void LAIDB_vSelectHoldValve(void)
{
	if(liu1FlgHoldFL==1)
	{
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
	}
	else{}

	if(liu1FlgHoldFR==1)
	{
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
	}
	else{}

	if(liu1FlgHoldRL==1)
	{
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
	}
	else{}

	if(liu1FlgHoldRR==1)
	{
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}
/*==================================================================================*/
/*	Purpose : IDB Control에서 wheel valve 작동 falg를 전달받아, 해당 valve를 dump	*/
/*																					*/
/*	Input Variable : leb_u16g_FlgCloseFL : FL hold command							*/
/*																		 			*/
/*	Output Variable : 																*/
/*==================================================================================*/
void LAIDB_vSelectDumpValve(void)
{
	if(liu1FlgDumpFL==1)
	{
		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
	}
	else{}

	if(liu1FlgDumpFR==1)
	{
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
	}
	else{}

	if(liu1FlgDumpRL==1)
	{
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
	}
	else{}

	if(liu1FlgDumpRR==1)
	{
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAIDB_vSelectCircValve(void)
{
	if(liu1FlgCircFL==1)
	{
		LAIDB_vActWheelValveCirc(&FL, &la_FL1HP, &la_FL2HP);
	}
	else{}

	if(liu1FlgCircFR==1)
	{
		LAIDB_vActWheelValveCirc(&FR, &la_FR1HP, &la_FR2HP);
	}
	else{}

	if(liu1FlgCircRL==1)
	{
		LAIDB_vActWheelValveCirc(&RL, &la_RL1HP, &la_RL2HP);
	}
	else{}

	if(liu1FlgCircRR==1)
	{
		LAIDB_vActWheelValveCirc(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

/*==================================================================================*/
/*	Purpose : 설정시간1 동안 대기 후, 설정시간2 동안 open							*/
/*																					*/
/*	Input Variable : laidbu16TestMain_cnt : test mode 작동 시간						*/
/*																		 			*/
/*	Output Variable : 																*/
/*==================================================================================*/
#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
static void LAIDB_vSelectPulseActValve(void)
{
	if( ((laidbu16TestMain_cnt>=U16_PULSE_START_DELAY_TIME)&&(laidbu16TestMain_cnt<(U16_PULSE_START_DELAY_TIME+U8_VALVE_ACT_TIME)))
			||((laidbu16TestSub_cnt>=(U16_PRESSURE_HOLD_TIME-U8_VALVE_ACT_TIME))&&(laidbu16TestSub_cnt<U16_PRESSURE_HOLD_TIME)) )
	{
		if(ACT_VALVE_SELECT==FRONT_LEFT_NO)
		{
			LA_vResetWheelValvePwmDuty(&la_FL1HP);
			LA_vResetWheelValvePwmDuty(&la_FL2HP);
			AV_VL_fl=0;
			HV_VL_fl=0;
		}
		else{}

		if(ACT_VALVE_SELECT==FRONT_RIGHT_NO)
		{
			LA_vResetWheelValvePwmDuty(&la_FR1HP);
			LA_vResetWheelValvePwmDuty(&la_FR2HP);
			AV_VL_fr=0;
			HV_VL_fr=0;
		}
		else{}

		if(ACT_VALVE_SELECT==REAR_LEFT_NO)
			{
			LA_vResetWheelValvePwmDuty(&la_RL1HP);
			LA_vResetWheelValvePwmDuty(&la_RL2HP);
			AV_VL_rl=0;
			HV_VL_rl=0;
			}
			else{}

		if(ACT_VALVE_SELECT==REAR_RIGHT_NO)
			{
			LA_vResetWheelValvePwmDuty(&la_RR1HP);
			LA_vResetWheelValvePwmDuty(&la_RR2HP);
			AV_VL_rr=0;
			HV_VL_rr=0;
			}
			else{}

		if(ACT_VALVE_SELECT==PRIMARY_CIRC_NO)
			{
			LA_vResetWheelValvePwmDuty(&la_FR1HP);
			LA_vResetWheelValvePwmDuty(&la_FR2HP);
			AV_VL_fr=0;
			HV_VL_fr=0;
			LA_vResetWheelValvePwmDuty(&la_RL1HP);
			LA_vResetWheelValvePwmDuty(&la_RL2HP);
			AV_VL_rl=0;
			HV_VL_rl=0;
			}
			else{}

		if(ACT_VALVE_SELECT==SECONDARY_CIRC_NO)
			{
			LA_vResetWheelValvePwmDuty(&la_FL1HP);
			LA_vResetWheelValvePwmDuty(&la_FL2HP);
			AV_VL_fl=0;
			HV_VL_fl=0;
			LA_vResetWheelValvePwmDuty(&la_RR1HP);
			LA_vResetWheelValvePwmDuty(&la_RR2HP);
			AV_VL_rr=0;
			HV_VL_rr=0;
			}
			else{}
		}
	else{}
}

//static void LAIDB_vSetValveDuty(void)
//{
//	la_FL1HP=laesp_FL1HP;
//	la_FR1HP=laesp_FR1HP;
//	la_RL1HP=laesp_RL1HP;
//	la_RR1HP=laesp_RR1HP;
//	la_FL2HP=laesp_FL2HP;
//	la_FR2HP=laesp_FR2HP;
//	la_RL2HP=laesp_RL2HP;
//	la_RR2HP=laesp_RR2HP;
//}

/*======================================================================================*/
/*	Purpose : Count reset															*/
/*																						*/
/*	Input Variable : 																	*/
/*																		 				*/
/*	Output Variable :																	*/
/*======================================================================================*/
static void LAIDB_vInitializeTestCount(void)
{
	laidbu16TestMain_cnt=0;
	laidbu16TestSub_cnt=0;
}
#endif
/*======================================================================================*/
/*	Purpose : Valve Command reset															*/
/*																						*/
/*	Input Variable : 																	*/
/*																		 				*/
/*	Output Variable :																	*/
/*======================================================================================*/
static void LAIDB_vInitializeValve(void)
{
	LA_vResetWheelValvePwmDuty(&la_FL1HP);
	LA_vResetWheelValvePwmDuty(&la_FL2HP);
	LA_vResetWheelValvePwmDuty(&la_FR2HP);
	LA_vResetWheelValvePwmDuty(&la_FR2HP);
	LA_vResetWheelValvePwmDuty(&la_RL2HP);
	LA_vResetWheelValvePwmDuty(&la_RL2HP);
	LA_vResetWheelValvePwmDuty(&la_RR2HP);
	LA_vResetWheelValvePwmDuty(&la_RR2HP);
	AV_VL_fl=0;
	HV_VL_fl=0;
	AV_VL_fr=0;
	HV_VL_fr=0;
	AV_VL_rl=0;
	HV_VL_rl=0;
	AV_VL_rr=0;
	HV_VL_rr=0;
}

/*==================================================================================*/
/*	Purpose : Hold mode 동작															*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAIDB_vActWheelValveHold(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
    WL = WL_temp;

	//LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
	LA_vSetWheelVvHold_Forcing(laWL1HP, laWL2HP);
	AV_VL_wl=0;
	HV_VL_wl=1;
}

/*==================================================================================*/
/*	Purpose : Dump mode 동작															*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAIDB_vActWheelValveDump(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
    uint8_t u8abrDumpScanTime;

    WL = WL_temp;

	u8abrDumpScanTime = U8_BASE_LOOPTIME;

	LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, u8abrDumpScanTime);
	AV_VL_wl=1;
	HV_VL_wl=1;
}

static void LAIDB_vActWheelValveDumpScan(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8DumpTime)
{
    WL = WL_temp;

	LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, u8DumpTime);
	AV_VL_wl=1;
	HV_VL_wl=1;
}

/*==================================================================================*/
/*	Purpose : Circ mode 동작															*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAIDB_vActWheelValveCirc(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
    WL = WL_temp;

	LA_vSetWheelVvOnOffCircOutput(laWL1HP, laWL2HP);
	AV_VL_wl=1;
	HV_VL_wl=0;
}

/*==================================================================================*/
/*	Purpose : Test mode 동작 금지 설정												*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vInhibitTestMode(void)
{
	#if	__IDB_SIMULATOR_TEST==DISABLE
	if((vref>=VREF_1_KPH)||(lis16MasterPress>MPRESS_150BAR)||(laespu16BenchTest_cnt>=TEST_COUNT_LIMIT))
	{
		laespu1InhibitTestMode=1;
	}
	else
	{
		laespu1InhibitTestMode=0;
	}
	#else
	if((lss16BCpresFiltP>MPRESS_150BAR)||(laespu16BenchTest_cnt>=TEST_COUNT_LIMIT))
	{
		laespu1InhibitTestMode=1;
	}
	else
	{
		laespu1InhibitTestMode=0;
	}
	#endif
}

/*==================================================================================*/
/*	Purpose : Trigger 신호 정의														*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vSetTrigger(void)
{
	laespu1Trigger_old = laespu1Trigger;

	#if	__TRIGGER_TYPE ==__ESP_SWITCH_TYPE
	laespu1Trigger = (fu1ESCSwitchSignal==1) ? 1:0 ;

	#elif __TRIGGER_TYPE ==__SIMULATOR_VREF
	laespu1Trigger = (vref>vref_alt) ? 1:0 ;

#elif __TRIGGER_TYPE ==__PDT_TYPE
	laespu1Trigger = (lis16PedalTravelFilt>50) ? 1:0 ;

	#elif __TRIGGER_TYPE ==__BLS_TYPE
	laespu1Trigger = (fu1BLSSignal==1) ? 1:0 ;

	#elif __TRIGGER_TYPE ==__LOOP_CNT_TYPE

	if(system_loop_counter<300)
	{
		laespu1Trigger=0;
	}
	else if(system_loop_counter<305)
	{
		laespu1Trigger=1;
	}
	else
	{
		laespu1Trigger=0;
	}
	#else
	/* do nothing */
	#endif
}

/*==================================================================================*/
/*	Purpose : Trigger 동작 설정														*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vDetectTriggerAct(void)
{
	/* switch input type */
		static uint16_t u16TriggerCnt;

	if((laespu1Trigger_old==1)&&(laespu1Trigger==0)&&(u16TriggerCnt<TRIGGER_ACT_COUNT))
	{
		laespu1ActTrigger = 1;
	}
	else
	{
		laespu1ActTrigger = 0;
	}

	if(laespu1Trigger==1)
	{
		laespu1HoldTrigger = 1;
	}
	else
	{
		laespu1HoldTrigger = 0;
	}

	if(laespu1Trigger==1)
	{
		u16TriggerCnt++;
		u16TriggerCnt = LCTCS_u16ILimitMaximum(u16TriggerCnt,TRIGGER_COUNT_LIMIT);
	}
	else
	{
		u16TriggerCnt = 0;
	}
}

/*==================================================================================*/
/*	Purpose : Test mode 동작/중지 판단												*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void	LAESP_vDetectTestRunExit(void)
{
	laidbu1TestModeOn_old = laidbu1TestModeOn;

	if(laespu1ActTrigger==1)
	{
		if(laidbu1TestModeOn==0)
		{
			laidbu1TestModeOn = 1;
		}
		else
		{
			laidbu1TestModeOn = 0;
		}
	}
	else{}

#if (TEST_MODULE_SELECT==__PULSE_UP_DOWN_MODE)
	if((laidbu1TestModeOn_old!=0)&&(laidbu16TestMain_cnt==0))
	{
		laidbu1TestModeOn = 0;
	}
#else
	if((laespu8TestModeID==NUMBER_OF_TEST_CASE)&&(laespu8TestModePhase==255))
	{
		laidbu1TestModeOn = 0;
	}
#endif
	if(laespu1InhibitTestMode==1)
	{
		laidbu1TestModeOn = 0;
	}
	else{}

	liu1TestModeAct_ESC_Temp = laidbu1TestModeOn;

}
#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
static void	LAESP_vDetectTestRunExitByExt(void)
{
	laidbu1TestModeOn_old = laidbu1TestModeOn;

	laidbu1TestModeOn = liu1TestModeAct_BBS;

	if(laespu1InhibitTestMode==1)
	{
		laidbu1TestModeOn = 0;
	}
	else{}
}
#endif
/*==================================================================================*/
/*	Purpose : Test mode 동작	카운트													*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void	LAESP_vCountTestRun(void)
{
	if(laidbu1TestModeOn==1)
	{
		if(laespu1HoldTrigger==1) /* hold 시, 현재 test mode 완료하고 시험 진행 Pause(count hold) */
		{
			if(laespu16BenchTest_cnt!=0)
		{
			laespu16BenchTest_cnt++;
			laespu16BenchTest_cnt = LCTCS_u16ILimitMaximum(laespu16BenchTest_cnt,TEST_COUNT_LIMIT);
		}
			else{} /* hold */
		}
		else
		{
			laespu16BenchTest_cnt++;
			laespu16BenchTest_cnt = LCTCS_u16ILimitMaximum(laespu16BenchTest_cnt,TEST_COUNT_LIMIT);
		}
	}
	else
	{
	    laespu16BenchTest_cnt=0;
	}

	if((laespu8TestModePhase==255)||(laidbu1TestModeOn_old!=laidbu1TestModeOn))
	{
		laespu16BenchTest_cnt=0;
	}
	else{}
}

/*==================================================================================*/
/*	Purpose : Test 항목 분류															*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vSetTestID(void)
{
	laespu8TestModeID_old=laespu8TestModeID;

	if(laidbu1TestModeOn==1)
	{
		if(laespu8TestModeID==0)
		{
			laespu8TestModeID=1;
		}
		else if(laespu8TestModePhase==255)
		{
			laespu8TestModeID+=1;
			laespu8TestModeID =(uint8_t) LCTCS_u16ILimitMaximum(laespu8TestModeID, NUMBER_OF_TEST_CASE);
		}
		else{}
	}
	else
	{
	    laespu8TestModeID = 0;
	}
}

/*==================================================================================*/
/*	Purpose : Test 항목 별 동작 모드의 작동 시간 설정									*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
#if (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_9))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_3))
		{
			laespu8TestModePhase = 3;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_4))
		{
			laespu8TestModePhase = 4;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_8))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_8_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_8_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_8_3))
		{
			laespu8TestModePhase = 3;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==__TEMP_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_7))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_7_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_7_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_7_3))
		{
			laespu8TestModePhase = 3;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_7_4))
		{
			laespu8TestModePhase = 4;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==__TEMP_MODE) */

#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_6))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_6_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_6_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_6_3))
		{
			laespu8TestModePhase = 3;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE) */


#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

		if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_1))
		{
			laespu8TestModePhase = 0;

			if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_1_1))
			{
				laespu8TestModePhase = 1;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_1_2))
			{
				laespu8TestModePhase = 2;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_1_3))
			{
				laespu8TestModePhase = 3;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_1_4))
			{
				laespu8TestModePhase = 4;
			}
			else
			{
				laespu8TestModePhase = 255;
			}

		}
		else
		{
			laespu8TestModePhase = 0;
		}
}
#endif /* (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE) */

#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

		if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_2))
		{
			laespu8TestModePhase = 0;

			if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_1))
			{
				laespu8TestModePhase = 1;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_2))
			{
				laespu8TestModePhase = 2;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_3))
			{
				laespu8TestModePhase = 3;
			}
			else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_4))
			{
				laespu8TestModePhase = 4;
			}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_5))
		{
			laespu8TestModePhase = 5;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_6))
		{
			laespu8TestModePhase = 6;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_2_7))
		{
			laespu8TestModePhase = 7;
		}
			else
			{
				laespu8TestModePhase = 255;
			}
		}
		else
		{
			laespu8TestModePhase = 0;
		}
}
#endif /* (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE) */

#if (TEST_MODULE_SELECT==__CURR_PRES_MAPPING_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(MAPPING_START_TIME))
		{
			laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(MAPPING_RISE_TIME))
			{
				laespu8TestModePhase = 1;
			}
		else if(laespu16BenchTest_cnt<(MAPPING_HOLD_TIME))
			{
				laespu8TestModePhase = 2;
			}
		else if(laespu16BenchTest_cnt<(MAPPING_END_TIME))
			{
				laespu8TestModePhase = 3;
			}
			else
			{
				laespu8TestModePhase = 255;
			}
		}
		else
		{
			laespu8TestModePhase = 0;
		}
}
#endif /* (TEST_MODULE_SELECT==__CURR_PRES_MAPPING_MODE) */

#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_3))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_3_1))
		{
			laespu8TestModePhase = 1;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE) */

#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_4))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_4_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_4_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_4_3))
		{
			laespu8TestModePhase = 3;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE) */

#if (TEST_MODULE_SELECT==__VALVE_ACT_ASSIST_MODE)
static void LAESP_vSetTestModePhase(void)
{
	laespu8TestModePhase_old=laespu8TestModePhase;

	if(laespu16BenchTest_cnt>(TEST_MODE_START_TIME_5))
	{
		laespu8TestModePhase = 0;

		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_1))
		{
			laespu8TestModePhase = 1;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_2))
		{
			laespu8TestModePhase = 2;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_3))
		{
			laespu8TestModePhase = 3;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_4))
		{
			laespu8TestModePhase = 4;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_5))
		{
			laespu8TestModePhase = 5;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_6))
		{
			laespu8TestModePhase = 6;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_7))
		{
			laespu8TestModePhase = 7;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_8))
		{
			laespu8TestModePhase = 8;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_9))
		{
			laespu8TestModePhase = 9;
		}
		else if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_5_10))
		{
			laespu8TestModePhase = 10;
		}
		else
		{
			laespu8TestModePhase = 255;
		}
	}
	else
	{
		laespu8TestModePhase = 0;
	}
}
#endif /* (TEST_MODULE_SELECT==__VALVE_ACT_ASSIST_MODE) */

/*==================================================================================*/
/*	Purpose : Test 항목 별 시험 모드 수행												*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAESP_vRunRiseRate(void)
{
	LAESP_vInitializeValveMotor();

		switch(laespu8TestModeID)
		{
			case 1:
				LAESP_vOneWheelRiseFL();
			break;

			case 2:
				LAESP_vOneWheelRiseFR();
			break;

			case 3:
				LAESP_vOneWheelRiseRL();
			break;

			case 4:
				LAESP_vOneWheelRiseRR();
			break;

			case 5:
				//LAESP_vTwoWheelRiseLeft();
				//LAESP_vTwoWheelRisePri();
				LAESP_vAllWheelRise();
			break;

			case 6:
				//LAESP_vTwoWheelRiseRight();
				//LAESP_vTwoWheelRiseSec();
			break;

			case 7:
				//LAESP_vAllWheelRise();
			break;
/*
			case 8:
				LAESP_vRunTestCase08();
			break;

*/
			default:
            	;
        	break;
		}/* switch */
}
#endif
#if (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE)
static void LAESP_vRunVlvOpenTiTest(void)
{
	LAESP_vInitializeValveMotor();

	if(laespu8TestModeID <= 5)
	{
		LAESP_vWhlP1PulseUp_pedal1();
//		laesps16EscTestModeTP = MPRESS_11BAR;
//		LAESP_vWhlP1ESCTarget1();
//		LAESP_vCalcTestData();
	}
	else if(laespu8TestModeID <= 10)
	{
		LAESP_vWhlP1PulseUp_pedal2();
//		laesps16EscTestModeTP = MPRESS_12BAR;
//		LAESP_vWhlP1ESCTarget1();

	}
	else if(laespu8TestModeID <= 15)
	{
		LAESP_vWhlP1PulseUp_pedal3();
//		laesps16EscTestModeTP = MPRESS_13BAR;
//		LAESP_vWhlP1ESCTarget1();

	}
	else if(laespu8TestModeID <= 20)
	{
		LAESP_vWhlP1PulseUp_pedal4();
//		laesps16EscTestModeTP = MPRESS_14BAR;
//		LAESP_vWhlP1ESCTarget1();

	}
	else{}
}
#endif /* (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE)
static void LAESP_vAltTestPrmtByID(void)
{
	laesps16PedalOverWriteTemp = 0;
	laesps16PedalOverWriteTemp2 = 0;
	laespu8DumpTime = 0;
	laespu8DumpTime2 = 18;

	if(laespu8TestModeID <= 5)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 3;
//		laespu8DumpTime = 12;
		laespu8DumpTime = 2;
	}
	else if(laespu8TestModeID <= 10)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 5;
//		laespu8DumpTime = 15;
		laespu8DumpTime = 3;
	}
	else if(laespu8TestModeID <= 15)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 7;
//		laespu8DumpTime = 17;
		laespu8DumpTime = 10;
	}
	else if(laespu8TestModeID <= 20)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 10;
//		laespu8DumpTime = 20;
		laespu8DumpTime = 20;
	}
	else if(laespu8TestModeID <= 25)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 10;
//		laespu8DumpTime = 20;
		laespu8DumpTime = 30;
	}
	else if(laespu8TestModeID <= 30)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 10;
//		laespu8DumpTime = 20;
		laespu8DumpTime = 40;
	}
	else if(laespu8TestModeID <= 35)
	{
		laesps16PedalOverWriteTemp = S16_4WHL_PEDAL_10BAR;
		laesps16PedalOverWriteTemp2 = laesps16PedalOverWriteTemp+S16_CIRC_PEDAL_10BAR;
//		laespu8DumpTime = 10;
//		laespu8DumpTime = 20;
		laespu8DumpTime = 50;
	}
	else{}
}

static void LAESP_vRunDumpTiTest(void)
{
	LAESP_vInitializeValveMotor();

	if(laespu8TestModeID <= 5)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 10)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 15)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 20)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 25)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 30)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else if(laespu8TestModeID <= 35)
	{
		LAESP_vWhlP1Dump_pedal1();
//		LAESP_vWhlP1Dump_ABS();
	}
	else{}
}
#endif /* (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==__TEMP_MODE)
static void LAESP_vRunTemporaryTest(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAESP_vAllWheelTarget();
			break;

			//case 2:
			//	LAESP_vVolFL2FR();
			//	break;
			//
			//case 3:
			//	LAESP_vVolFL2FR();
			//	break;
			//
			//case 4:
			//	LAESP_vVolFL2FR();
			//	break;

		default:
			;
        	break;
	}/* switch */
}
#endif
#if (TEST_MODULE_SELECT==__VOLUME_CHANGE_TEST_MODE)
static void LAESP_vRunVolChange(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAESP_vVolFL2FR();
			break;

		//case 2:
		//	LAESP_vVolFL2FR();
		//	break;
		//
		//case 3:
		//	LAESP_vVolFL2FR();
		//	break;
		//
		//case 4:
		//	LAESP_vVolFL2FR();
		//	break;

		default:
			;
        	break;
	}/* switch */
}
#endif
#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
static void LAESP_vRunPressMatch(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			//LAIDB_vPressMatch01(); // reapply mode
			LAIDB_vPressMatch05(); // hold mode
			//LAIDB_vPressMatch09(); // posi hold mode
			//LAIDB_vIqStepRise5Bar();
		break;

		case 2:
			//LAIDB_vPressMatch02(); // reapply mode
			LAIDB_vPressMatch06(); // hold mode
			//LAIDB_vPressMatch10(); // posi hold mode
			//LAIDB_vIqStepRise10Bar();
		break;

		case 3:
			//LAIDB_vPressMatch03(); // reapply mode
			LAIDB_vPressMatch07(); // hold mode
			//LAIDB_vPressMatch11(); // posi hold mode
			//LAIDB_vIqStepRise20Bar();
		break;

		case 4:
			//LAIDB_vPressMatch04(); // reapply mode
			LAIDB_vPressMatch08(); // hold mode
			//LAIDB_vPressMatch12(); // posi hold mode
		break;

		default:
           	;
		break;
	}/* switch */
}
#endif
#if(0)
static void LAIDB_vRunCurrMapping(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAIDB_vRunAWMapping();
		break;

		case 2:
			LAIDB_vRunMasterMapping();
		break;

		default:
           	;
		break;
	}/* switch */
}

void LAIDB_vRunAWMapping(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		if(laidbs16Pres2iq>IDB_IQ_MAX_CURR)
		{
			laidbs16Pres2iq=IDB_IQ_MAX_CURR;
			laidbu1DetMaxCurr=1;
		}
		else
		{
			laidbs16Pres2iq = laidbs16Pres2iq + IDB_IQ_ICR_RATE;
			laidbu1DetMaxCurr=0;
		}
	}
	else if(laespu8TestModePhase==2)/*충돌방지*/
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq=0;
	}
	else{}
}

void LAIDB_vRunMasterMapping(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		if(laidbs16Pres2iq>IDB_IQ_MAX_CURR)
		{
			laidbs16Pres2iq=IDB_IQ_MAX_CURR;
			laidbu1DetMaxCurr=1;
		}
		else
		{
			laidbs16Pres2iq = laidbs16Pres2iq + IDB_IQ_ICR_RATE;
			laidbu1DetMaxCurr=0;
		}
	}
	else if(laespu8TestModePhase==2)/*충돌방지*/
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq=0;
	}
	else{}
}
#endif
#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAIDB_vRunMultiTest(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAIDB_vRunSineTarget();
			break;

		case 2:
			LAIDB_vRunStepTarget();
			//LAIDB_vRunSineTarget_Rvr();
			break;

		case 3:
			//LAIDB_vRunStepTarget();
			break;

		case 4:
			//LAIDB_vRunStepTarget_Rvr();
			break;

		default:
           	;
			break;
	}/* switch */
}
#endif
#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
static void LAIDB_vRunRetractionTest(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAIDB_vRunRetraction();
			break;

		default:
           	;
			break;
	}/* switch */
}
#endif
#if (__ASSIST_TYPE == __TIMMING_SYNC)
static void LAIDB_vRunValveAssistTest(void)
{
	LAESP_vInitializeValveMotor();

	switch(laespu8TestModeID)
	{
		case 1:
			LAIDB_vRunStepTargetAssist();
			break;

		case 2:
			LAIDB_vRunSineTargetAssist();
			break;

		default:
           	;
			break;
	}/* switch */
}
#endif
/*==================================================================================*/
/*	Purpose : Valve / Motor Reset													*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vInitializeValveMotor(void)
{
	if((laespu8TestModePhase!=laespu8TestModePhase_old)||(laespu8TestModePhase==0))
	{
		LAESP_vResetValveMotor();
	}
	else{}
}

/*==================================================================================*/
/*	Purpose : 각 Test mode 정의														*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
#if (TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE)
static void LAESP_vWhlP1Dump_pedal1(void)
{
	static uint8_t u8DumpTrigger=0;
	static uint8_t u8DumpTime=0;
	static uint8_t u8DumpExtdTime=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = laesps16PedalOverWrite + U8_TEST_PEDAL_RATE;
		if(laesps16PedalOverWrite >= laesps16PedalOverWriteTemp)
		{
			laesps16PedalOverWrite = laesps16PedalOverWriteTemp;
		}

		u8DumpTrigger = 0;
		u8DumpTime=laespu8DumpTime; //if TestmodeID
		u8DumpExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 나머지휠 dump
		// 서킷압력 set
		laesps16PedalOverWrite = laesps16PedalOverWriteTemp;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_1 + 50))
		{
			LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
//			LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 dump 5 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = laesps16PedalOverWriteTemp;

		if(u8DumpTime>=10)
		{
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,10);
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime = u8DumpTime-10;
		}
		else if(u8DumpTime>0)
		{
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,u8DumpTime);
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1Dump_ABS(void)
{
	static uint8_t u8DumpTrigger=0;
	static uint8_t u8DumpTime=0;
	static uint8_t u8DumpTime2=0;
	static uint8_t u8DumpExtdTime=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = laesps16PedalOverWrite + U8_TEST_PEDAL_RATE;
		if(laesps16PedalOverWrite >= laesps16PedalOverWriteTemp)
		{
			laesps16PedalOverWrite = laesps16PedalOverWriteTemp;
		}

		u8DumpTrigger = 0;
		u8DumpTime=laespu8DumpTime; //if TestmodeID
		u8DumpTime2=laespu8DumpTime2; //if TestmodeID
		u8DumpExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 나머지휠 dump
		// 서킷압력 set
		laesps16PedalOverWrite = laesps16PedalOverWriteTemp;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		if(laespu16BenchTest_cnt<(TEST_MODE_PHASE_9_1 + 50))
		{
			LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
//			LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 dump 5 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = laesps16PedalOverWriteTemp;

		if(u8DumpTime2>=10)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,10);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime2 = u8DumpTime2-10;
		}
		else if(u8DumpTime2>0)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,u8DumpTime2);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime2=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else if(laespu8TestModePhase==4)
	{
		// 휠 dump 5 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = laesps16PedalOverWriteTemp;

		if(u8DumpTime>=10)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,10);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime = u8DumpTime-10;
		}
		else if(u8DumpTime>0)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveDumpScan(&FR, &la_FR1HP, &la_FR2HP,u8DumpTime);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8DumpTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}
#endif /*(TEST_MODULE_SELECT==DUMP_TIME_TEST_MODE)*/

#if (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE)
static void LAESP_vWhlP1ESCTarget1(void)
{
	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = S16_4WHL_PEDAL_50BAR;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		laesps16PedalOverWrite = S16_4WHL_PEDAL_50BAR;
//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else if(laespu8TestModePhase==3)
	{
		// 서킷압력 set
		ESP_BRAKE_CONTROL_FL = 1;
		VDC_DISABLE_FLG = 0;
		FL_ESC.lcesps16IdbTarPress = laesps16EscTestModeTP;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1PulseUp(void)
{
	static uint8_t u8VlvOpenTrigger=0;
	if((laespu8TestModePhase==1)||(laespu8TestModePhase==4)||(laespu8TestModePhase==7))
	{
		//휠 압력 set
		ESP_BRAKE_CONTROL_FL = 1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_10BAR;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		u8VlvOpenTrigger = 0;
	}
	else if((laespu8TestModePhase==2)||(laespu8TestModePhase==5)||(laespu8TestModePhase==8))
	{
		// 휠 valve hold
		// 서킷압력 set
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		ESP_BRAKE_CONTROL_FL = 1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_20BAR;
	}
	else if((laespu8TestModePhase==3)||(laespu8TestModePhase==6)||(laespu8TestModePhase==9))
	{
		// 휠 valve open 10 ~ 20 ms
		// 서킷압력 set
		ESP_BRAKE_CONTROL_FL = 1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_20BAR;

		if(u8VlvOpenTrigger==0)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 10);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTrigger++;
		}
		else if(u8VlvOpenTrigger==1)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 5);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTrigger++;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1PulseUp_pedal1(void)
{
	static uint8_t u8VlvOpenTrigger=0;
	static uint8_t u8VlvOpenTime=0;
	static uint8_t u8VlvOpenExtdTime=0;
	static uint8_t u8CircVlvSetCnt=0;
	static uint8_t u8CircVlvReSetCnt=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = S16_4WHL_PEDAL_30BAR;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		u8VlvOpenTrigger = 0;
		u8CircVlvSetCnt = 0;
		u8CircVlvReSetCnt=0;
		u8VlvOpenTime=U8_VLV_OPEN_TIME; //if TestmodeID
		u8VlvOpenExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 서킷압력 set
		// 서킷밸브 set
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		u8CircVlvSetCnt++;
		if(u8CircVlvSetCnt>TEST_MODE_PHASE_8_2_RUNTIME-2)
		{
			laespu8CircVlvCmdOverWrite = 1;
		}

		laesps16PedalOverWrite = S16_3WHL_PEDAL_40BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 valve open 10 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = S16_3WHL_PEDAL_40BAR;
		laespu8CircVlvCmdOverWrite = 1;

		if(u8VlvOpenTime>=10)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 10);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTime = u8VlvOpenTime-10;
		}
		else if(u8VlvOpenTime>0)
		{
				LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, u8VlvOpenTime);
				LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
				u8VlvOpenTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1PulseUp_pedal2(void)
{
	static uint8_t u8VlvOpenTrigger=0;
	static uint8_t u8VlvOpenTime=0;
	static uint8_t u8VlvOpenExtdTime=0;
	static uint8_t u8CircVlvSetCnt=0;
	static uint8_t u8CircVlvReSetCnt=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = S16_4WHL_PEDAL_30BAR;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		u8VlvOpenTrigger = 0;
		u8CircVlvSetCnt = 0;
		u8CircVlvReSetCnt=0;
		u8VlvOpenTime=U8_VLV_OPEN_TIME; //if TestmodeID
		u8VlvOpenExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 서킷압력 set
		// 서킷밸브 set
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		u8CircVlvSetCnt++;
		if(u8CircVlvSetCnt>TEST_MODE_PHASE_8_2_RUNTIME-2)
		{
			laespu8CircVlvCmdOverWrite = 1;
		}

		laesps16PedalOverWrite = S16_3WHL_PEDAL_50BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 valve open 10 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = S16_3WHL_PEDAL_50BAR;
		laespu8CircVlvCmdOverWrite = 1;

		if(u8VlvOpenTime>=10)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 10);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTime = u8VlvOpenTime-10;
		}
		else if(u8VlvOpenTime>0)
		{
				LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, u8VlvOpenTime);
				LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
				u8VlvOpenTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1PulseUp_pedal3(void)
{
	static uint8_t u8VlvOpenTrigger=0;
	static uint8_t u8VlvOpenTime=0;
	static uint8_t u8VlvOpenExtdTime=0;
	static uint8_t u8CircVlvSetCnt=0;
	static uint8_t u8CircVlvReSetCnt=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = S16_4WHL_PEDAL_30BAR;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		u8VlvOpenTrigger = 0;
		u8CircVlvSetCnt = 0;
		u8CircVlvReSetCnt=0;
		u8VlvOpenTime=U8_VLV_OPEN_TIME; //if TestmodeID
		u8VlvOpenExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 서킷압력 set
		// 서킷밸브 set
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		u8CircVlvSetCnt++;
		if(u8CircVlvSetCnt>TEST_MODE_PHASE_8_2_RUNTIME-2)
		{
			laespu8CircVlvCmdOverWrite = 1;
		}

		laesps16PedalOverWrite = S16_3WHL_PEDAL_60BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 valve open 10 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = S16_3WHL_PEDAL_60BAR;
		laespu8CircVlvCmdOverWrite = 1;

		if(u8VlvOpenTime>=10)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 10);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTime = u8VlvOpenTime-10;
		}
		else if(u8VlvOpenTime>0)
		{
				LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, u8VlvOpenTime);
				LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
				u8VlvOpenTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vWhlP1PulseUp_pedal4(void)
{
	static uint8_t u8VlvOpenTrigger=0;
	static uint8_t u8VlvOpenTime=0;
	static uint8_t u8VlvOpenExtdTime=0;
	static uint8_t u8CircVlvSetCnt=0;
	static uint8_t u8CircVlvReSetCnt=0;

	if(laespu8TestModePhase==1)
	{
		//휠 압력 set
		laesps16PedalOverWrite = S16_4WHL_PEDAL_30BAR;

//		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		u8VlvOpenTrigger = 0;
		u8CircVlvSetCnt = 0;
		u8CircVlvReSetCnt=0;
		u8VlvOpenTime=U8_VLV_OPEN_TIME; //if TestmodeID
		u8VlvOpenExtdTime=0;
	}
	else if(laespu8TestModePhase==2)
	{
		// 휠 valve hold
		// 서킷압력 set
		// 서킷밸브 set
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		u8CircVlvSetCnt++;
		if(u8CircVlvSetCnt>TEST_MODE_PHASE_8_2_RUNTIME-2)
		{
			laespu8CircVlvCmdOverWrite = 1;
		}

		laesps16PedalOverWrite = S16_3WHL_PEDAL_70BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		// 휠 valve open 10 ~ 20 ms
		// 서킷압력 set
		laesps16PedalOverWrite = S16_3WHL_PEDAL_70BAR;
		laespu8CircVlvCmdOverWrite = 1;

		if(u8VlvOpenTime>=10)
		{
			LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, 10);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
			u8VlvOpenTime = u8VlvOpenTime-10;
		}
		else if(u8VlvOpenTime>0)
		{
				LA_vSetWheelVvOnOffRiseOutput(&la_FL1HP, &la_FL2HP, u8VlvOpenTime);
				LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
				LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
				LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
				u8VlvOpenTime=0;
		}
		else
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
	}
	else
	{
		;
	}
}

static void LAESP_vCalcTestData(void)
{

}
#endif /* (TEST_MODULE_SELECT==VALVE_OPEN_TIME_TEST_MODE) */

#if (TEST_MODULE_SELECT==__TEMP_MODE)
int16_t LSIDB_s16CalMovingAvg(struct CALC_AVG *Calc_avg, int16_t s16Value, int16_t s16Length)
{
	int16_t s16Temp0;
	Calc_avg->s32ValueSum = ((Calc_avg->s32ValueSum - ((int32_t)Calc_avg->s16Buffer[Calc_avg->s16Index])) + (int32_t)s16Value);
	Calc_avg->s16Buffer[Calc_avg->s16Index] = s16Value;

	s16Temp0 = s16Length;
	Calc_avg->s16MovingAvg = (int16_t)(Calc_avg->s32ValueSum / (int32_t)s16Temp0);

	if (Calc_avg->s16Index == (s16Temp0 - 1))
	{
		Calc_avg->s16Index = 0;
	}
	else
	{
		Calc_avg->s16Index++;
	}
	return Calc_avg->s16MovingAvg;
}

static void	LAESP_vAllWheelTarget(void)
{
	if(laespu8TestModePhase==1)
	{
		BTCS_fl = 1;
		BTCS_fr = 1;
		BTCS_rl = 1;
		BTCS_rr = 1;
		ESP_BRAKE_CONTROL_FL = 1;
		ESP_BRAKE_CONTROL_FR = 1;
		ESP_BRAKE_CONTROL_RL = 1;
		ESP_BRAKE_CONTROL_RR = 1;

		laesps16PriTcTargetPress = MPRESS_10BAR;
		laesps16SecTcTargetPress = MPRESS_20BAR;
		latcss16PriTcTargetPress = MPRESS_30BAR;
		latcss16SecTcTargetPress = MPRESS_40BAR;
	}
	else if(laespu8TestModePhase==2)
	{
		BTCS_fl = 1;
		BTCS_fr = 1;
		BTCS_rl = 1;
		BTCS_rr = 1;
		ESP_BRAKE_CONTROL_FL = 1;
		ESP_BRAKE_CONTROL_FR = 1;
		ESP_BRAKE_CONTROL_RL = 1;
		ESP_BRAKE_CONTROL_RR = 1;

		laesps16PriTcTargetPress = 410;//MPRESS_40BAR;
		laesps16SecTcTargetPress = MPRESS_10BAR;
		latcss16PriTcTargetPress = MPRESS_20BAR;
		latcss16SecTcTargetPress = MPRESS_30BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		BTCS_fl = 1;
		BTCS_fr = 1;
		BTCS_rl = 1;
		BTCS_rr = 1;
		ESP_BRAKE_CONTROL_FL = 1;
		ESP_BRAKE_CONTROL_FR = 1;
		ESP_BRAKE_CONTROL_RL = 1;
		ESP_BRAKE_CONTROL_RR = 1;

		laesps16PriTcTargetPress = MPRESS_30BAR;
		laesps16SecTcTargetPress = 420;//MPRESS_40BAR;
		latcss16PriTcTargetPress = MPRESS_10BAR;
		latcss16SecTcTargetPress = MPRESS_20BAR;
	}
	else if(laespu8TestModePhase==4)
	{
		BTCS_fl = 1;
		BTCS_fr = 1;
		BTCS_rl = 1;
		BTCS_rr = 1;
		ESP_BRAKE_CONTROL_FL = 1;
		ESP_BRAKE_CONTROL_FR = 1;
		ESP_BRAKE_CONTROL_RL = 1;
		ESP_BRAKE_CONTROL_RR = 1;

		laesps16PriTcTargetPress = MPRESS_20BAR;
		laesps16SecTcTargetPress = MPRESS_30BAR;
		latcss16PriTcTargetPress = 430;//MPRESS_40BAR;
		latcss16SecTcTargetPress = MPRESS_10BAR;
	}
	else{}
}
#endif
#if (__ASSIST_TYPE == __TIMMING_SYNC)
static void LAIDB_vRunStepTargetAssist(void)
{
	if((laespu8TestModePhase==1)||(laespu8TestModePhase==3)||(laespu8TestModePhase==5)||(laespu8TestModePhase==7)||(laespu8TestModePhase==9))
	{
		//Pri control
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else if((laespu8TestModePhase==2)||(laespu8TestModePhase==4)||(laespu8TestModePhase==6)||(laespu8TestModePhase==8)||(laespu8TestModePhase==10))
	{
		//Sec control
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
	//LAIDB_vSelectHoldValve();
}

static void LAIDB_vRunSineTargetAssist(void)
{
	if((laespu8TestModePhase==1)||(laespu8TestModePhase==3)||(laespu8TestModePhase==5)||(laespu8TestModePhase==7)||(laespu8TestModePhase==9))
	{
		//Pri control
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else if((laespu8TestModePhase==2)||(laespu8TestModePhase==4)||(laespu8TestModePhase==6)||(laespu8TestModePhase==8)||(laespu8TestModePhase==10))
	{
		//Sec control
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}
#endif
#if (TEST_MODULE_SELECT==__RETRACTION_TEST_MODE)
static void LAIDB_vRunRetraction(void)
{
	if(laespu8TestModePhase==1)
	{
		/* position target 30mm =' Target Pressure 100bar */
	}
	else if(laespu8TestModePhase==2)
	{
		ESP_BRAKE_CONTROL_FL=1;
		/* dump rate change */
	}
	else if(laespu8TestModePhase==3)
	{
		ESP_BRAKE_CONTROL_FL=1;
		/* rise */
	}
	else{}
}
#endif

static void LAIDB_vSetWheelTarget(void)
{
	laidbs16WheelTPFL = MPRESS_20BAR;
	laidbs16WheelTPFR = MPRESS_40BAR;
	laidbs16WheelTPRL = MPRESS_60BAR;
	laidbs16WheelTPRR = MPRESS_80BAR;
}

static void LAIDB_vGenCircTP_triangular(void)
{
	if(laespu8TestModePhase==1)
	{
		laidbs16PriTP = laidbs16PriTP + MPRESS_1BAR;
		laidbs16SecTP = laidbs16SecTP + MPRESS_1BAR;
	}
	else if(laespu8TestModePhase==2)
	{
		laidbs16PriTP = laidbs16PriTP - MPRESS_1BAR;
		laidbs16SecTP = laidbs16SecTP - MPRESS_1BAR;
	}
	else{}

	laidbs16PriTP = LCABS_s16LimitMinMax(laidbs16PriTP, MPRESS_10BAR, MPRESS_100BAR);
	laidbs16SecTP = LCABS_s16LimitMinMax(laidbs16SecTP, MPRESS_10BAR, MPRESS_100BAR);
}

static void LAIDB_vRunTriTarget(void)
{
	LAIDB_vGenCircTP_triangular();
	LAIDB_vSetWheelTarget();

	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		if(laidbu8Priority==PRIMARY_TARGET_FIRST)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else if(laidbu8Priority==SECONDARY_TARGET_FIRST)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}
	}
	else{}
}
#if (TEST_MODULE_SELECT==__MULTIPLEX_TEST_MODE)
static void LAIDB_vGenCircTP_sine(void)
{
	//laidbs16PriTP = LEB_s16SineWaveFast(&leb_PMSWFg_Pri);
	//laidbs16SecTP = LEB_s16SineWaveFast(&leb_PMSWFg_Sec);
}

static void LAIDB_vGenCircTP_step(void)
{
	laidbs16PriTP = MPRESS_20BAR;
	laidbs16SecTP = MPRESS_10BAR;
}

//static void LAIDB_vGenCircTP_ramp(void)
//{
//	;
//}

static void LAIDB_vRunSineTarget(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		if(laidbu8Priority==PRIMARY_TARGET_FIRST)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else if(laidbu8Priority==SECONDARY_TARGET_FIRST)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}


		LAIDB_vGenCircTP_sine();
		LA_vCircMultiMain();
	}
	else{}
}

static void LAIDB_vRunSineTarget_Rvr(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		if(laidbu8Priority==PRIMARY_TARGET_FIRST)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else if(laidbu8Priority==SECONDARY_TARGET_FIRST)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}


		LAIDB_vGenCircTP_sine();
		LA_vCircMultiMain();
	}
	else{}
}

static void LAIDB_vRunStepTarget(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		if(laidbu8Priority==PRIMARY_TARGET_FIRST)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else if(laidbu8Priority==SECONDARY_TARGET_FIRST)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}

		LAIDB_vGenCircTP_step();
		LA_vCircMultiMain();
	}
	else{}
}

static void LAIDB_vRunStepTarget_Rvr(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		if(laidbu8Priority==PRIMARY_TARGET_FIRST)
		{
			//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else if(laidbu8Priority==SECONDARY_TARGET_FIRST)
		{
			LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
			//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
			//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
			LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);
		}
		else{}

		LAIDB_vGenCircTP_step();
		LA_vCircMultiMain();
	}
	else{}
}
#endif
//int16_t	LAIDB_s16Pres2Iq(int16_t s16InputPres, int16_t s16RPMLevel)
//{
//	if()
//}

static void LAESP_vVolFL2FR(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		laesps16PriTcTargetPress = MPRESS_50BAR;

		//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else if(laespu8TestModePhase==2)
	{
		ESP_BRAKE_CONTROL_FL=1;
		laesps16PriTcTargetPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}
#if (TEST_MODULE_SELECT==__ESP_RISE_RATE_TEST_MODE)
static void LAESP_vOneWheelRiseFL(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vOneWheelRiseFR(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FR=1;
		FR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vOneWheelRiseRL(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_RL=1;
		RL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vOneWheelRiseRR(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_RR=1;
		RR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vTwoWheelRisePri(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FR=1;
		ESP_BRAKE_CONTROL_RL=1;
		FR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vTwoWheelRiseSec(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		ESP_BRAKE_CONTROL_RR=1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

//		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vTwoWheelRiseLeft(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		ESP_BRAKE_CONTROL_RL=1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

//		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
//		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vTwoWheelRiseRight(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FR=1;
		ESP_BRAKE_CONTROL_RR=1;
		FR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
//		LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
//		LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}

static void LAESP_vAllWheelRise(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;
		ESP_BRAKE_CONTROL_RL=1;
		ESP_BRAKE_CONTROL_FR=1;
		ESP_BRAKE_CONTROL_RR=1;
		FL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RL_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		FR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;
		RR_ESC.lcesps16IdbTarPress = MPRESS_50BAR;

		//LAIDB_vActWheelValveDump(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveDump(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveDump(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveDump(&RR, &la_RR1HP, &la_RR2HP);
	}
	else{}
}
#endif
#if (TEST_MODULE_SELECT==__PRESSURE_MATCHING_MODE)
static void LAIDB_vIqStepRise5Bar(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_5_BAR;
	}
	else if(laespu8TestModePhase==2)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_15_BAR;
	}
	else if(laespu8TestModePhase==4)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==5)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_25_BAR;
	}
	else if(laespu8TestModePhase==6)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR;
	}
	else if(laespu8TestModePhase==7)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_35_BAR;
	}
	else{}
}
static void LAIDB_vIqStepRise10Bar(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR;
	}
	else if(laespu8TestModePhase==4)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_40_BAR;
	}
	else if(laespu8TestModePhase==5)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_50_BAR;
	}
	else if(laespu8TestModePhase==6)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_60_BAR;
	}
	else if(laespu8TestModePhase==7)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_70_BAR;
	}
	else{}
}
static void LAIDB_vIqStepRise20Bar(void)
{
	if(laespu8TestModePhase==1)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_40_BAR;
	}
	else if(laespu8TestModePhase==3)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_60_BAR;
	}
	else if(laespu8TestModePhase==4)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==5)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR;
	}
	else if(laespu8TestModePhase==6)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_110_BAR;
	}
	else if(laespu8TestModePhase==7)
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		//LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		//LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		//LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_120_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch01(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR_CIRC;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_70_BAR_CIRC;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		if(laespu8TestModePhase_old==3){laidbs16Pres2iq = IDB_IQ_70_BAR_CIRC;}
		laidbs16Pres2iq = laidbs16Pres2iq+IDB_IQ_RISE_RATE_1;
		if(laidbs16Pres2iq>IDB_IQ_120_BAR){laidbs16Pres2iq=IDB_IQ_120_BAR;}
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch02(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR_CIRC;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_75_BAR_CIRC;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		if(laespu8TestModePhase_old==3){laidbs16Pres2iq = IDB_IQ_75_BAR_CIRC;}
		laidbs16Pres2iq = laidbs16Pres2iq+IDB_IQ_RISE_RATE_1;
		if(laidbs16Pres2iq>IDB_IQ_120_BAR){laidbs16Pres2iq=IDB_IQ_120_BAR;}
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch03(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR_CIRC;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_85_BAR_CIRC;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		if(laespu8TestModePhase_old==3){laidbs16Pres2iq = IDB_IQ_85_BAR_CIRC;}
		laidbs16Pres2iq = laidbs16Pres2iq+IDB_IQ_RISE_RATE_1;
		if(laidbs16Pres2iq>IDB_IQ_120_BAR){laidbs16Pres2iq=IDB_IQ_120_BAR;}
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch04(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR_CIRC;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_90_BAR_CIRC;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		if(laespu8TestModePhase_old==3){laidbs16Pres2iq = IDB_IQ_90_BAR_CIRC;}
		laidbs16Pres2iq = laidbs16Pres2iq+IDB_IQ_RISE_RATE_1;
		if(laidbs16Pres2iq>IDB_IQ_120_BAR){laidbs16Pres2iq=IDB_IQ_120_BAR;}
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch05(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR_CIRC;
		//laidbs16PosiHold = leb_s16g_Posi;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_0_BAR_CIRC;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==4) /* iq hold */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//laidbs16Pres2iq = IDB_IQ_70_BAR_CIRC;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch06(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR_CIRC;
		//laidbs16PosiHold = leb_s16g_Posi;
	}
	else if(laespu8TestModePhase==3) /* hold */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_5_BAR_CIRC;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//laidbs16Pres2iq = IDB_IQ_75_BAR_CIRC;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch07(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR_CIRC;
		//laidbs16PosiHold = leb_s16g_Posi;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_15_BAR_CIRC;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==4) /* 승압 rate 제한 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//laidbs16Pres2iq = IDB_IQ_85_BAR_CIRC;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch08(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR_CIRC;
		//laidbs16PosiHold = leb_s16g_Posi;
	}
	else if(laespu8TestModePhase==3) /* 서킷압 감압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_20_BAR_CIRC;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==4) /* hold */
	{
		ESP_BRAKE_CONTROL_FL=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//laidbs16Pres2iq = IDB_IQ_90_BAR_CIRC;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		laidbs16Pres2iq=0;
	}
	else{}
}

static void LAIDB_vPressMatch09(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR_CIRC;

		//laidbs16PosiHold = leb_s16g_Posi;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==3) /* 모터 위치 고정 NO 지연*/
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==4) /* 모터 위치 고정 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_0_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch10(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_25_BAR_CIRC;

		//laidbs16PosiHold = leb_s16g_Posi;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==3) /* 모터 위치 고정 NO 지연*/
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==4) /* 모터 위치 고정 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_0_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch11(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_15_BAR_CIRC;

		//laidbs16PosiHold = leb_s16g_Posi;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==3) /* 모터 위치 고정 NO 지연*/
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==4) /* 모터 위치 고정 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_0_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch12(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_10_BAR_CIRC;

		//laidbs16PosiHold = leb_s16g_Posi;
		laidbs16iqHold = laidbs16Pres2iq;
	}
	else if(laespu8TestModePhase==3) /* 모터 위치 고정 NO 지연*/
	{
		ESP_BRAKE_CONTROL_FL=1;

		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==4) /* 모터 위치 고정 */
	{
		ESP_BRAKE_CONTROL_FL=1;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		//leb_s16g_PosiRef = laidbs16PosiHold;
	}
	else if(laespu8TestModePhase==5) /* 충돌 방지 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_0_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch13(void)
{
	if(laespu8TestModePhase==1) /* a 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_10_BAR;
	}
	else if(laespu8TestModePhase==2) /* b 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR;
	}
	else if(laespu8TestModePhase==3) /* no open */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;

		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_120_BAR;
	}
	else if(laespu8TestModePhase==4) /* 휠 승압 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==5) /* 서킷 회복 */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_100_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch14(void)
{
	if(laespu8TestModePhase==1) /* 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_20_BAR;
	}
	else if(laespu8TestModePhase==2) /* 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==3) /* FL Matching */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_30_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch15(void)
{
	if(laespu8TestModePhase==1) /* 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_30_BAR;
	}
	else if(laespu8TestModePhase==2) /* 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==3) /* FL Matching */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_40_BAR;
	}
	else{}
}

static void LAIDB_vPressMatch16(void)
{
	if(laespu8TestModePhase==1) /* 휠압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		laidbs16Pres2iq = IDB_IQ_40_BAR;
	}
	else if(laespu8TestModePhase==2) /* 서킷압 생성 */
	{
		ESP_BRAKE_CONTROL_FL=1;
		LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_80_BAR;
	}
	else if(laespu8TestModePhase==3) /* FL Matching */
	{
		/*모터 위치 고정*/
		ESP_BRAKE_CONTROL_FL=1;
		//leb_u16g_FlgRel1ValveOn=0;
		//leb_u16g_FlgRel2ValveOn=1;
		//LAIDB_vActWheelValveHold(&FL, &la_FL1HP, &la_FL2HP);
		LAIDB_vActWheelValveHold(&FR, &la_FR1HP, &la_FR2HP);
		LAIDB_vActWheelValveHold(&RL, &la_RL1HP, &la_RL2HP);
		LAIDB_vActWheelValveHold(&RR, &la_RR1HP, &la_RR2HP);

		laidbs16Pres2iq = IDB_IQ_50_BAR;
	}
	else{}
}
#endif
/*==================================================================================*/
/*	Purpose : Valve/Motor 동작 명령 reset												*/
/*																					*/
/*	Input Variable :																*/
/*	Output Variable :																*/
/*==================================================================================*/
static void LAESP_vResetValveMotor(void)
{
	laespu8CircVlvCmdOverWrite = 0;
	laesps16PedalOverWrite = 0;
	ESP_BRAKE_CONTROL_FL=0;
	ESP_BRAKE_CONTROL_FR=0;
	ESP_BRAKE_CONTROL_RL=0;
	ESP_BRAKE_CONTROL_RR=0;

    HV_VL_fl=0;
    AV_VL_fl=0;
    HV_VL_fr=0;
    AV_VL_fr=0;
    HV_VL_rl=0;
    AV_VL_rl=0;
	HV_VL_rr=0;
    AV_VL_rr=0;
	liu1FlgHoldFL=0;
	liu1FlgHoldFR=0;
	liu1FlgHoldRL=0;
	liu1FlgHoldRR=0;
	LA_vResetWheelValvePwmDuty(&la_FL1HP);
	LA_vResetWheelValvePwmDuty(&la_FL2HP);
	LA_vResetWheelValvePwmDuty(&la_FR1HP);
	LA_vResetWheelValvePwmDuty(&la_FR2HP);
	LA_vResetWheelValvePwmDuty(&la_RL1HP);
	LA_vResetWheelValvePwmDuty(&la_RL2HP);
	LA_vResetWheelValvePwmDuty(&la_RR1HP);
	LA_vResetWheelValvePwmDuty(&la_RR2HP);

	BTCS_fl = 0;
	BTCS_fr = 0;
	BTCS_rl = 0;
	BTCS_rr = 0;

	laesps16PriTcTargetPress = 0;
	laesps16SecTcTargetPress = 0;
	latcss16PriTcTargetPress = 0;
	latcss16SecTcTargetPress = 0;

	FL_ESC.lcesps16IdbTarPress = 0;
	FR_ESC.lcesps16IdbTarPress = 0;
	RL_ESC.lcesps16IdbTarPress = 0;
	RR_ESC.lcesps16IdbTarPress = 0;

}

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAESPCallTestHW
	#include "Mdyn_autosar.h"
#endif    
