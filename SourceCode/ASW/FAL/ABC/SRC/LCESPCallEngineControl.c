
/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCallEngineControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCESPCallEngineControl.h"
#include "LCTCSCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCallMain.h"
#include "LSESPCalSlipRatio.h"

#include "hm_logic_var.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSESPCallSensorSignal.h"
#include "LAABSCallActCan.h"
#if __TVBB
#include "LCTVBBCallControl.h"
#endif
extern uint16_t system_loop_counter;

 
int16_t 	yaw_err;
int16_t 	diff_yaw_err;
int16_t 	TorqLimit, TorqLimit_alt;
uint16_t 	ETO_exit_time;
uint8_t 	quickout_counter;
int16_t 	wstr_alt3;
int16_t 	diff_yaw_err3;
int16_t 	diff_yaw_err_old;
int16_t 	diff_yaw_err_old2;

int16_t 	delta_yaw_third_ems1;
int16_t 	P_gain_effect;
int16_t 	D_gain_effect;
int16_t 	wslip_max;
int16_t 	steering_effect;
int16_t 	drive_torq_div_20;
int16_t 	esp_cmd_alt;
int16_t 	diff_delta_yaw;
int16_t 	esp_cmd_yaw, esp_cmd_initial;
/*=============================================================================*/
/*	EEC flags by Kim Jeonghun in 2005.1.15									   */
/*-----------------------------------------------------------------------------*/
uint8_t 	EEC_flags;
uint8_t 	EEC_YAW_flags;
uint8_t	EEC_STRAIGHT;
uint16_t	eec_straight_cnt;
uint8_t	d_yaw_div_delay;
int16_t		o_delta_yaw_thres_eec;
int16_t		u_delta_yaw_thres_eec;
int16_t		delta_yaw_eec;
int16_t		delta_yaw_eec_alt;

/* EEC PD 제어 구조 추가 */
int16_t 	s16YawErr;
int16_t 	s16DiffYawErr;
int16_t 	s16YawErrAlt;
int16_t 	s16DiffYawErrAlt;

int16_t		s16YawPDEffect;
int16_t		s16YawPGainEffect;
int16_t		s16YawDGainEffect;

/* Yaw PD Gain Controller */
int8_t	s8SetYawPDGainSelecter;

int16_t		s16SetYawPGainTemp[4] 			  = { 0, 0, 0, 0};
int16_t		s16SetYawDGainTemp[4] 			  = { 0, 0, 0, 0};
int16_t		s16SetYawPDGainSpeed[4]		  = { 0, 0, 0, 0};

int16_t		s16SetYawPGain;
int16_t		s16SetYawDGain;

int16_t		s16SetYawPGainUnderState[3][4]   ={{14,14,14,14},
				  								{14,14,14,14},
				  								{14,14,14,14}};

int16_t		s16SetYawDGainUnderState[3][4]   ={{10,10,10,10},
				  								{10,10,10,10},
				  								{10,10,10,10}};

int16_t		s16SetYawPGainUnstableState[3][4]={{14,14,14,14},
				  								{14,14,14,14},
				  								{14,14,14,14}};

int16_t		s16SetYawDGainUnstableState[3][4]={{10,10,10,10},
				  								{10,10,10,10},
				  								{10,10,10,10}};

int16_t		s16SetYawPGainStableState[3][4]  ={{ 4, 4, 4, 4},
				  								{ 4, 4, 4, 4},
				  								{ 4, 4, 4, 4}};

int16_t		s16SetYawDGainStableState[3][4]  ={{10,10,10,10},
				  								{10,10,10,10},
				  								{10,10,10,10}};

int16_t		s16SetYawPGainOverState[3][4] 	  ={{ 2, 2, 2, 2},
				  								{ 2, 2, 2, 2},
				  								{ 2, 2, 2, 2}};

int16_t		s16SetYawDGainOverState[3][4] 	  ={{10,10,10,10},
				  								{10,10,10,10},
				  								{10,10,10,10}};

int16_t		s16SetYawPGainOvstableState[3][4]={{14,14,14,14},
				  								{14,14,14,14},
				  								{14,14,14,14}};

int16_t		s16SetYawDGainOvstableState[3][4]={{10,10,10,10},
				  								{10,10,10,10},
				  								{10,10,10,10}};
				  								
int16_t 	wslip_max_alt;

int8_t 	s8LoopCounter;
int8_t      us_eng_con_cnt;
int16_t     delta_yaw_third_ems_old;
int16_t		LOW_MU_ESP_TORQ_CON_U_SPEED;
int16_t		MED_MU_ESP_TORQ_CON_U_SPEED;
int16_t		HIGH_MU_ESP_TORQ_CON_U_SPEED;

uint16_t	eec_stable_exit_count;

int16_t		s16SetYawPGainStablCurPer[3][4]={{100, 90, 80, 80},
		  								{ 90, 80, 80, 80},
		  								{ 100, 100, 100, 100}};
int16_t		delta_yaw_alt;

uint16_t	lcespu16EECYawFadeTime;

#if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
int16_t  ldesps16EECTargetVx, lcesps16EECDropRate, ldesps16EECTargetVxLow;
int16_t  cal_torq_div_20, eng_torq_div_20;
uint8_t  lcespu8EECStartCnt; 
uint8_t  ldespu8EECOverSpeed;                     
#endif


#if __MODEL_CHANGE    
    #define Mass      (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */
#else   
  #if (__CAR==HMC_GK) || (__CAR==ASTRA_MECU)  
    #define Mass            1330      /* Kg */  
  #elif (__CAR==SYC_RAXTON)||(__CAR==SYC_RAXTON_MECU)    
    #define Mass            2096      /* Kg */    
  #elif (__CAR==SYC_KYRON)||(__CAR==SYC_KYRON_MECU)  
    #define Mass            2026      /* Kg */  
  #else  // NF  
    #define Mass            1562      /* Kg */    
  #endif  
#endif        

#if __CAR==HMC_JM
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	1
#elif __CAR==HMC_NF
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	0
#elif __CAR==SYC_RAXTON && (__SUSPENSION_TYPE==0)
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	0
#elif __CAR==SYC_RAXTON && (__SUSPENSION_TYPE==1)
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	0
#elif __CAR==SYC_KYRON
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	0
#elif __CAR==KMC_HM
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	1
#elif __CAR==FORD_C214
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	1
#elif __CAR==HMC_PA
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	1
#else
	#define __EEC_PD_CONTROL	1
	#define EEC_CURVATURE_COMPENSATION	1
#endif

#if __EEC_PD_CONTROL
	#define U16_EEC_YAW_FADE_TIME_MAX	1420
#endif

#define __YAW_ERR_NEO			1

#define __EEC_CONTROL_CYCLE	3

#define __ESP_ETCS_CONTROL  1

int16_t		eec_target_torq_1st = 0;

uint8_t	EEC_FADE_OUT_CTRL_ON;
uint8_t	EEC_UTURN_CTRL_ON;

	#if __EEC_GEAR_INHIBIT_MODE
uint8_t	EEC_ACTIVATED				= 0;
uint8_t	EEC_ACTIVATE_CHECK 			= 0;
uint8_t	EEC_GEAR_INHIBIT_REQUEST 	= 0;
uint16_t	eec_gear_inhibit_count		= 0;

 	#endif

uint8_t lespu8ENG_VAR_OLD;
uint8_t lespu8ENG_VAR_CHECK;
uint8_t lespu8ENG_VAR_COUNT;



#if __VDC
void LCESP_vCallMainForEEC(void);
void LCESP_vSignalProcessingForEEC(void);
void LCESP_vEstimateForEEC(void);
void LCESP_vDetectForEEC(void);
void LCESP_vControlForEEC(void);
void LCESP_vLoadParameterForEEC(void);
void LCESP_vDetectDeltaYawCondition(void);
void LCESP_vDetectEECExitCondition(void);
void LCESP_vControlDeltaYawForEEC(void);
void LCESP_vCompareOutputForEEC(void);
void LCESP_vDetectStraightForEEC(void);
void LCESP_vCalTorqueLimitForEEC(void);
void LCESP_vCompensateTorqueForEEC(void);
void LCESP_vCalYawPDGainForEEC(void);

void LCESP_vInitializeEEC(void);
void LCESP_vCompensateTorqueCommand(void);

// <skeon optime_macfunc_1> 110303
//#ifndef OPTIME_MACFUNC_1  
//extern int16_t LCESP_s16IInter4Point( int input_x, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3 , int x_4, int y_4 );

//#endif   // </skeon optime_macfunc_1>
//extern int16_t LCESP_s16IInter3Point( int input_x, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3 );




#if __EEC_PD_CONTROL
void LCESP_vCompensateYawPDEffect(void);
void LCESP_vDetectFadeMode(void);
#endif

void LCESP_vFadeOutControl(void);

void LCESP_vTargetTorque1st(void);

void LCESP_vCallMainForEEC(void)
{
	LCESP_vSignalProcessingForEEC();
  	LCESP_vEstimateForEEC();
  	
  	#if defined(__UCC_1)
//     	LC_vInterfaceSportyModeEms();
	#endif

	/* MBD ETCS */
	/*LCESP_vDetectForEEC();*/
	LCESP_vControlForEEC();
}

void LCESP_vSignalProcessingForEEC(void)
{
  #if __EEC_CYCLE_TIME_REDUCTION
	if((lespu8ENG_VAR_CHECK==0)||(lespu8ENG_VAR_OLD==0))
	{
		lespu8ENG_VAR_CHECK = 1;
		lespu8ENG_VAR_OLD = ENG_VAR;
		LCESP_vLoadParameterForEEC();
		
		lespu8ENG_VAR_COUNT++;
	}
	else if(lespu8ENG_VAR_OLD!=ENG_VAR)
	{
		lespu8ENG_VAR_OLD = ENG_VAR;
		LCESP_vLoadParameterForEEC();
		
		lespu8ENG_VAR_COUNT++;
	}
	else
	{
		;
	}
  #else
	if(wmu1CycleTime21ms_1==1)
	{
		LCESP_vLoadParameterForEEC();
		
		lespu8ENG_VAR_COUNT++;
	}
	else
	{
		;
	}	
  #endif
}

void LCESP_vLoadParameterForEEC(void)
{
	#if __EEC_PD_CONTROL
 	 	s16SetYawPGainUnderState[0][0] = S16_LM_LSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[0][1] = S16_LM_MSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[0][2] = S16_LM_HSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[0][3] = S16_LM_USP_YAW_UNDER_PGAIN;

 	 	s16SetYawPGainUnderState[1][0] = S16_MM_LSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[1][1] = S16_MM_MSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[1][2] = S16_MM_HSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[1][3] = S16_MM_USP_YAW_UNDER_PGAIN;

 	 	s16SetYawPGainUnderState[2][0] = S16_HM_LSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[2][1] = S16_HM_MSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[2][2] = S16_HM_HSP_YAW_UNDER_PGAIN;
 	 	s16SetYawPGainUnderState[2][3] = S16_HM_USP_YAW_UNDER_PGAIN;

 	 	s16SetYawPGainUnstableState[0][0] = S16_LM_LSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[0][1] = S16_LM_MSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[0][2] = S16_LM_HSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[0][3] = S16_LM_USP_YAW_UNSTB_PGAIN;

 	 	s16SetYawPGainUnstableState[1][0] = S16_MM_LSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[1][1] = S16_MM_MSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[1][2] = S16_MM_HSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[1][3] = S16_MM_USP_YAW_UNSTB_PGAIN;

 	 	s16SetYawPGainUnstableState[2][0] = S16_HM_LSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[2][1] = S16_HM_MSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[2][2] = S16_HM_HSP_YAW_UNSTB_PGAIN;
 	 	s16SetYawPGainUnstableState[2][3] = S16_HM_USP_YAW_UNSTB_PGAIN;

 	 	s16SetYawPGainOverState[0][0] = S16_LM_LSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[0][1] = S16_LM_MSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[0][2] = S16_LM_HSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[0][3] = S16_LM_USP_YAW_OVER_PGAIN;

 	 	s16SetYawPGainOverState[1][0] = S16_MM_LSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[1][1] = S16_MM_MSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[1][2] = S16_MM_HSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[1][3] = S16_MM_USP_YAW_OVER_PGAIN;

 	 	s16SetYawPGainOverState[2][0] = S16_HM_LSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[2][1] = S16_HM_MSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[2][2] = S16_HM_HSP_YAW_OVER_PGAIN;
 	 	s16SetYawPGainOverState[2][3] = S16_HM_USP_YAW_OVER_PGAIN;

 	 	s16SetYawPGainStableState[0][0] = S16_LM_LSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[0][1] = S16_LM_MSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[0][2] = S16_LM_HSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[0][3] = S16_LM_USP_YAW_STB_PGAIN;

 	 	s16SetYawPGainStableState[1][0] = S16_MM_LSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[1][1] = S16_MM_MSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[1][2] = S16_MM_HSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[1][3] = S16_MM_USP_YAW_STB_PGAIN;

 	 	s16SetYawPGainStableState[2][0] = S16_HM_LSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[2][1] = S16_HM_MSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[2][2] = S16_HM_HSP_YAW_STB_PGAIN;
 	 	s16SetYawPGainStableState[2][3] = S16_HM_USP_YAW_STB_PGAIN;

 	 	s16SetYawPGainOvstableState[0][0] = S16_LM_LSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[0][1] = S16_LM_MSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[0][2] = S16_LM_HSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[0][3] = S16_LM_USP_YAW_OVSTB_PGAIN;

 	 	s16SetYawPGainOvstableState[1][0] = S16_MM_LSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[1][1] = S16_MM_MSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[1][2] = S16_MM_HSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[1][3] = S16_MM_USP_YAW_OVSTB_PGAIN;

 	 	s16SetYawPGainOvstableState[2][0] = S16_HM_LSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[2][1] = S16_HM_MSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[2][2] = S16_HM_HSP_YAW_OVSTB_PGAIN;
 	 	s16SetYawPGainOvstableState[2][3] = S16_HM_USP_YAW_OVSTB_PGAIN;
 	 	
 	 	s16SetYawDGainUnderState[0][0] = S16_LM_LSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[0][1] = S16_LM_MSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[0][2] = S16_LM_HSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[0][3] = S16_LM_USP_YAW_UNDER_DGAIN;

 	 	s16SetYawDGainUnderState[1][0] = S16_MM_LSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[1][1] = S16_MM_MSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[1][2] = S16_MM_HSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[1][3] = S16_MM_USP_YAW_UNDER_DGAIN;

 	 	s16SetYawDGainUnderState[2][0] = S16_HM_LSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[2][1] = S16_HM_MSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[2][2] = S16_HM_HSP_YAW_UNDER_DGAIN;
 	 	s16SetYawDGainUnderState[2][3] = S16_HM_USP_YAW_UNDER_DGAIN;

 	 	s16SetYawDGainUnstableState[0][0] = S16_LM_LSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[0][1] = S16_LM_MSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[0][2] = S16_LM_HSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[0][3] = S16_LM_USP_YAW_UNSTB_DGAIN;

 	 	s16SetYawDGainUnstableState[1][0] = S16_MM_LSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[1][1] = S16_MM_MSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[1][2] = S16_MM_HSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[1][3] = S16_MM_USP_YAW_UNSTB_DGAIN;

 	 	s16SetYawDGainUnstableState[2][0] = S16_HM_LSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[2][1] = S16_HM_MSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[2][2] = S16_HM_HSP_YAW_UNSTB_DGAIN;
 	 	s16SetYawDGainUnstableState[2][3] = S16_HM_USP_YAW_UNSTB_DGAIN;

 	 	s16SetYawDGainOverState[0][0] = S16_LM_LSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[0][1] = S16_LM_MSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[0][2] = S16_LM_HSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[0][3] = S16_LM_USP_YAW_OVER_DGAIN;

 	 	s16SetYawDGainOverState[1][0] = S16_MM_LSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[1][1] = S16_MM_MSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[1][2] = S16_MM_HSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[1][3] = S16_MM_USP_YAW_OVER_DGAIN;

 	 	s16SetYawDGainOverState[2][0] = S16_HM_LSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[2][1] = S16_HM_MSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[2][2] = S16_HM_HSP_YAW_OVER_DGAIN;
 	 	s16SetYawDGainOverState[2][3] = S16_HM_USP_YAW_OVER_DGAIN;

 	 	s16SetYawDGainStableState[0][0] = S16_LM_LSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[0][1] = S16_LM_MSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[0][2] = S16_LM_HSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[0][3] = S16_LM_USP_YAW_STB_DGAIN;

 	 	s16SetYawDGainStableState[1][0] = S16_MM_LSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[1][1] = S16_MM_MSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[1][2] = S16_MM_HSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[1][3] = S16_MM_USP_YAW_STB_DGAIN;

 	 	s16SetYawDGainStableState[2][0] = S16_HM_LSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[2][1] = S16_HM_MSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[2][2] = S16_HM_HSP_YAW_STB_DGAIN;
 	 	s16SetYawDGainStableState[2][3] = S16_HM_USP_YAW_STB_DGAIN;

 	 	s16SetYawDGainOvstableState[0][0] = S16_LM_LSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[0][1] = S16_LM_MSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[0][2] = S16_LM_HSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[0][3] = S16_LM_USP_YAW_OVSTB_DGAIN;

 	 	s16SetYawDGainOvstableState[1][0] = S16_MM_LSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[1][1] = S16_MM_MSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[1][2] = S16_MM_HSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[1][3] = S16_MM_USP_YAW_OVSTB_DGAIN;

 	 	s16SetYawDGainOvstableState[2][0] = S16_HM_LSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[2][1] = S16_HM_MSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[2][2] = S16_HM_HSP_YAW_OVSTB_DGAIN;
 	 	s16SetYawDGainOvstableState[2][3] = S16_HM_USP_YAW_OVSTB_DGAIN;
 	 	
	#endif
}

void LCESP_vEstimateForEEC(void)
{
/*==================================================================================*/
/* Variable Calculation for ESP Engine Control										*/
/*  -. delta_alat_offset 															*/
/*  -. delta_alat_ems																*/
/*  -. e_vref_min																	*/
/*  -. wslip_max																	*/
/*  -. Enter Threshold(delta_yaw & delta_ay)										*/
	/*------------------------*/
	/* Threshold Sign Check   */
	/*------------------------*/
	#if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
	
	esp_tempW3 = (int16_t)( (((int32_t)rf)*vrefm)/562 ) ;   /*alatc*/
    esp_tempW0 = McrAbs(esp_tempW3);
    esp_tempW6 = (int16_t)( (((int32_t)esp_tempW0)*S16_EEC_MAX_TAR_VX_GAIN)/100 );
    esp_tempW5 = (int16_t)( (((int32_t)det_alatm_fltr)*esp_tempW4)/100 );
    
    if ((esp_tempW0 >= LAT_0G1G) && (esp_tempW6 >= McrAbs(det_alatm_fltr)))
    {
        esp_tempW1 = (int16_t)( (((int32_t)(McrAbs(det_alatm_fltr)))*1000)/esp_tempW0 );
        
        esp_tempW2 = LDESP_s16CalcSQRT(esp_tempW1); /* x1000*/
        
        ldesps16EECTargetVx = (int16_t)( (((int32_t)esp_tempW2)*vref5)/1000 );    /*0.125kph*/
    }
    else
    {
        ldesps16EECTargetVx = (int16_t)( (((int32_t)vref5)*130)/100 );
    }
    
    esp_tempW4 = LCESP_s16IInter3Point(det_alatm,
                                      S16_LM_EMS, S16_EEC_LOW_TAR_VX_GAIN_LM,
                                      S16_MM_EMS, S16_EEC_LOW_TAR_VX_GAIN_MM,
                                      S16_HM_EMS, S16_EEC_LOW_TAR_VX_GAIN_HM);
    	
    ldesps16EECTargetVxLow = (int16_t)( (((int32_t)ldesps16EECTargetVx)*esp_tempW4)/100 );
    
    if (vref5 > ldesps16EECTargetVx)
    {
        ldespu8EECOverSpeed = 2;
    }
    else if (vref5 > ldesps16EECTargetVxLow)
    {
        ldespu8EECOverSpeed = 1;
    }
    else
    {
        ldespu8EECOverSpeed = 0;
    }
    
	#endif
	
	#if (__EEC_UNDERSTEER_THRESHOLD == 1)
	o_delta_yaw_thres_eec = o_delta_yaw_thres;
	u_delta_yaw_thres_eec = u_delta_yaw_thres;
	#else
	if(o_delta_yaw_thres_eec >= u_delta_yaw_thres_eec)
	{
		o_delta_yaw_thres_eec = o_delta_yaw_thres;
		u_delta_yaw_thres_eec = u_delta_yaw_thres;
	}
	else
	{
		o_delta_yaw_thres_eec = u_delta_yaw_thres;
		u_delta_yaw_thres_eec = o_delta_yaw_thres;
	}
	#endif
	/*------------------------*/
	/* delta_yaw_check Check  */
	/*------------------------*/
	delta_yaw_eec 		= delta_yaw_first;
	delta_yaw_eec_alt 	= delta_yaw_first_alt;
	/*------------------------*/
	/* Delta Ay Offset update */
	/*------------------------*/
    tempW7=(nor_alat_unlimit_ems-alat_ems);

    if(abs_delta_yaw<YAW_1DEG)
    {
        if(McrAbs(delta_yaw_first_dot)<5)
        {
            if(McrAbs(nor_alat_unlimit_ems)<LAT_0G1G)
            {
                if(McrAbs(tempW7)<LAT_0G07G)
                {
                	delta_alat_offset=0;
                }
                else
                {
                	;
                }
            }
            else
            {
               	;
            }
        }
        else
        {
           	;
        }
    }
    else
    {
       	;
    }

    delta_alat_ems_old=delta_alat_ems;
    delta_alat_ems=(tempW7-delta_alat_offset);

    if((delta_yaw_eec-delta_yaw_eec_alt)<0)
    {
    	D_YAW_DEC=1;
    }
    else
    {
    	D_YAW_DEC=0;
    }

    if(alat_ems<0)
    {
    	ALAT_POS=0;
    }
    else
    {
    	ALAT_POS=1;
    }

	delta_yaw_third_ems_alt=delta_yaw_third_ems;
	o_delta_yaw_third_ems_alt=o_delta_yaw_third_ems;

	/*------------------------*/
    /* e_vref_min Calculation */
	/*------------------------*/
    e_vref_min=LCESP_s16IInter2Point( det_alatm, S16_LM_EMS, VREF_15_KPH, S16_HM_EMS, VREF_25_KPH);

    e_vref_min-=(abs_wstr/100);

	/*-----------------------*/
	/* wslip_max Calculation */
	/*-----------------------*/
	wslip_max_alt=wslip_max;
	
	esp_tempW4=(slip_m_fl+slip_m_fr)/2;
	esp_tempW5=(slip_m_rl+slip_m_rr)/2;
	if(esp_tempW4>esp_tempW5)
	{
		wslip_max=esp_tempW4;
	}
	else
	{
		wslip_max=esp_tempW5;
	}
   	if(wslip_max<0)
   	{
   		wslip_max=0;
   	}
	else
	{
		;
	}

    wslip_max=wslip_max/100;        /* Resolution : 0.01% -> 1% */
    if(wslip_max>100)
    {
    	wslip_max=100;
    }
	else
	{
		;
	}
    steering_effect=McrAbs(wstr)/50;

    tempW1=McrAbs(delta_yaw_eec);
    tempW2=McrAbs(delta_yaw_eec_alt);

    if((tempW1-tempW2)>0)
    {
    	D_YAW_DIV=1;
    	d_yaw_div_delay=0;
    }
    else
    {
    	if(D_YAW_DIV==1)
    	{
    		if(d_yaw_div_delay>=1)
    		{
    			D_YAW_DIV=0;
    			d_yaw_div_delay=0;
    		}
    		else
    		{
    			d_yaw_div_delay++;
    		}
    	}
    	else
    	{
    		;
    	}
    }


    /*----------------------------------------------------------------------*/
    /* delta_alat_ems 증가 경향을 보는 것이, D_AY_DIV 상황				*/
    /*----------------------------------------------------------------------*/
	tempW1=McrAbs(delta_alat_ems);
    tempW2=McrAbs(delta_alat_ems_old);
    if((tempW1-tempW2)>0)
    {
    	D_AY_DIV=1;
    }
    else
    {
    	D_AY_DIV=0;
    }

    drive_torq_div_20=drive_torq/20;
      #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
    eng_torq_div_20 = eng_torq/20;
    cal_torq_div_20 = cal_torq/20;
      #endif

	/*ESC Engine Control - UnderSteer Control*/
	#if (__EEC_UNDERSTEER_THRESHOLD == 1)
    esp_tempW1= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_LSP_TH_GAIN, S16_MM_EMS, S16_MM_LSP_TH_GAIN,
                                                 S16_HM_EMS, S16_HM_LSP_TH_GAIN);
    esp_tempW2= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_MSP_TH_GAIN, S16_MM_EMS, S16_MM_MSP_TH_GAIN,
                                                 S16_HM_EMS, S16_HM_MSP_TH_GAIN);
    esp_tempW3= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_HSP_TH_GAIN, S16_MM_EMS, S16_MM_HSP_TH_GAIN,
                                                 S16_HM_EMS, S16_HM_HSP_TH_GAIN);
    	#if __TVBB
    if((det_alatm>S16_LM_EMS) && (lctvbbu1TVBB_ON == 1))
    {
    	esp_tempW1=esp_tempW1 + S16_LSP_TH_GAIN_ADD_TVBB;
    	esp_tempW2=esp_tempW2 + S16_MSP_TH_GAIN_ADD_TVBB;
    	esp_tempW3=esp_tempW3 + S16_HSP_TH_GAIN_ADD_TVBB;
    }
    else
    {
    	;
    }
		#endif /*__TVBB*/	                                                         
    esp_tempW4= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_LSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_LSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_LSP);
    esp_tempW5= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_MSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_MSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_MSP);
    esp_tempW6= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_HSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_HSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_HSP);
   
    tempW8=LCESP_s16IInter3Point(vrefk,esp_tempW4,esp_tempW1,esp_tempW5,esp_tempW2,esp_tempW6,esp_tempW3);
    #else
    if(det_alatm<S16_LM_EMS)
    {
        tempW8=S16_LM_LSP_TH_GAIN;
        esp_tempW1=S16_LM_MSP_TH_GAIN;
        esp_tempW2=S16_LM_HSP_TH_GAIN;
    }
    else if(det_alatm<S16_HM_EMS)
    {
        tempW8=S16_MM_LSP_TH_GAIN;
        esp_tempW1=S16_MM_MSP_TH_GAIN;
        esp_tempW2=S16_MM_HSP_TH_GAIN;
    }
    else
    {
        tempW8=S16_HM_LSP_TH_GAIN;
        esp_tempW1=S16_HM_MSP_TH_GAIN;
        esp_tempW2=S16_HM_HSP_TH_GAIN;
    }
    	#if __TVBB
    if((det_alatm>S16_LM_EMS) && (lctvbbu1TVBB_ON == 1))
    {
    	tempW8=tempW8 + S16_LSP_TH_GAIN_ADD_TVBB;
    	esp_tempW1=esp_tempW1 + S16_MSP_TH_GAIN_ADD_TVBB;
    	esp_tempW2=esp_tempW2 + S16_HSP_TH_GAIN_ADD_TVBB;
    }
    else
    {
    	;
    }
		#endif /*__TVBB*/	    
	/*--------------------------------------------------*/
	/* Speed Dependent Δyaw Threshold Gain Calculation */
	/*--------------------------------------------------*/
    if(det_alatm<S16_LM_EMS)
    {
    	tempW8=LCESP_s16IInter3Point(vrefk,S16_LM_ESP_TORQ_CON_LSP,tempW8,
		S16_LM_ESP_TORQ_CON_MSP,esp_tempW1,S16_LM_ESP_TORQ_CON_HSP,esp_tempW2);
    }
    else if(det_alatm<S16_HM_EMS)
    {
    	tempW8=LCESP_s16IInter3Point(vrefk,S16_MM_ESP_TORQ_CON_LSP,tempW8,
		S16_MM_ESP_TORQ_CON_MSP,esp_tempW1,S16_MM_ESP_TORQ_CON_HSP,esp_tempW2);
    }
    else
    {
    	tempW8=LCESP_s16IInter3Point(vrefk,S16_HM_ESP_TORQ_CON_LSP,tempW8,
		S16_HM_ESP_TORQ_CON_MSP,esp_tempW1,S16_HM_ESP_TORQ_CON_HSP,esp_tempW2);
	}
	#endif /*__EEC_UNDERSTEER_THRESHOLD*/

    /*-----------------------------*/
	/* Threshold Tuning Limitation */
    /*-----------------------------*/
#if ( ((__CAR == HMC_JM) && (!__4WD)) || (__CAR == HMC_GK) )
	if(FTCS_ON==1)
	{
		if(tempW8<100)
		{
			tempW8=100;
		}
		else
		{
			;
		}
	}
	else if(tempW8<60)
	{
		tempW8=60;
	}
	else
	{
		;
	}
#else
	if(FTCS_ON==1)
	{
		if(vref<=VREF_15_KPH)
		{
			if(tempW8<120)
			{
				tempW8=120;
			}
			else
			{
				;
			}
		}
		else
		{
			if(tempW8<100)
			{
				tempW8=100;
			}
			else
			{
				;
			}
		}
	}
	else if(tempW8<60)
	{
		tempW8=60;
	}
	else
	{
		;
	}
#endif

    esp_tempW1=(int16_t) ( ( (int32_t)u_delta_yaw_thres_eec*tempW8)/100);
    esp_tempW1=LCTCS_s16ILimitRange(esp_tempW1, -30000, 30000);
    
/*#if (__EEC_UNDERSTEER_THRESHOLD == 1)

    if((ESP_TCS_ON == 0)&&(delta_yaw_first < esp_tempW1)&&(us_eng_con_cnt < U8_EEC_ENTER_CNT ))
    {
    	    us_eng_con_cnt = us_eng_con_cnt + 1;
    	    if (yaw_error_under_dot >= 0)
    	    {
    	    	delta_yaw_third_ems=(esp_tempW1*5);
    	    }
    	    else
    	    {
          		delta_yaw_third_ems=(esp_tempW1*2);
          	}
    }
    else if((ESP_TCS_ON == 0)&&(delta_yaw_first < esp_tempW1)&&(us_eng_con_cnt >= U8_EEC_ENTER_CNT ))
    {
    	    if (yaw_error_under_dot >= 0)
    	    {
    	    	delta_yaw_third_ems=(esp_tempW1*5);
    	    }
    	    else
    	    {
          		delta_yaw_third_ems=esp_tempW1;
          	}
          	us_eng_con_cnt = us_eng_con_cnt;
    }
    else
    {
    	delta_yaw_third_ems=esp_tempW1;
    	us_eng_con_cnt = 0;
    }
 

#else*/ 
	
		delta_yaw_third_ems = esp_tempW1;
/*#endif __EEC_UNDERSTEER_THRESHOLD*/

delta_yaw_third_ems_old = delta_yaw_third_ems;

#if (__EEC_UNDERSTEER_THRESHOLD == 0)
    /*----------------------------------------------------------*/
    /* Δyaw Threshold Gain Calibration depends on Driving Slip */
    /*----------------------------------------------------------*/
    if(delta_yaw_eec<(int16_t)(-YAW_1DEG))
    {
        tempW3 = (int16_t) ( ( (int32_t)u_delta_yaw_thres_eec*(tempW8-10))/100);
        if(det_alatm<S16_LM_EMS)
        {
            delta_yaw_third_ems+=wslip_max*S16_LM_SLIP_TH_GAIN;
        }
        else if(det_alatm<S16_HM_EMS)
        {
            delta_yaw_third_ems+=wslip_max*S16_MM_SLIP_TH_GAIN;
        }
        else
        {
            delta_yaw_third_ems+=wslip_max*S16_HM_SLIP_TH_GAIN;
        }

        if(delta_yaw_third_ems>tempW3)
        {
        	delta_yaw_third_ems=tempW3;
        }
		else
		{
			;
		}
    }
	else
	{
		;
	}

	/*---------------------------*/
    /* Prevention Threshold Jump */
    /*---------------------------*/
 	/* 1scan당 Δyaw Threshold 변화를 0.1deg/s 로 제한	*/
	if(delta_yaw_third_ems_alt<(delta_yaw_third_ems-10))
	{
		delta_yaw_third_ems=delta_yaw_third_ems_alt+10;
	}
	else if(delta_yaw_third_ems_alt>(delta_yaw_third_ems+10))
	{
		delta_yaw_third_ems=delta_yaw_third_ems_alt-10;
	}
	else
	{
		;
	}
#endif 	/*__EEC_UNDERSTEER_THRESHOLD*/

	#if (__EEC_OVERSTEER_THRESHOLD == 1)
	/*ESC Engine Control - OverSteer Control*/
    esp_tempW1= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_LSP_TH_GAIN_IN_OVER, S16_MM_EMS, S16_MM_LSP_TH_GAIN_IN_OVER,
                                                 S16_HM_EMS, S16_HM_LSP_TH_GAIN_IN_OVER);
    esp_tempW2= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_MSP_TH_GAIN_IN_OVER, S16_MM_EMS, S16_MM_MSP_TH_GAIN_IN_OVER,
                                                 S16_HM_EMS, S16_HM_MSP_TH_GAIN_IN_OVER);
    esp_tempW3= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_HSP_TH_GAIN_IN_OVER, S16_MM_EMS, S16_MM_HSP_TH_GAIN_IN_OVER,
                                                 S16_HM_EMS, S16_HM_HSP_TH_GAIN_IN_OVER);
    
    esp_tempW4= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_LSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_LSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_LSP);
    esp_tempW5= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_MSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_MSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_MSP);
    esp_tempW6= LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_LM_ESP_TORQ_CON_HSP, S16_MM_EMS, S16_MM_ESP_TORQ_CON_HSP,
                                                 S16_HM_EMS, S16_HM_ESP_TORQ_CON_HSP);
 
    tempW8=LCESP_s16IInter3Point(vrefk,esp_tempW4,esp_tempW1,esp_tempW5,esp_tempW2,esp_tempW6,esp_tempW3);
	   
 /*-----------------------------*/
 /* Threshold Tuning Limitation */
 /*-----------------------------*/	   
 if(FTCS_ON==1)
	{
		if(vref<=VREF_15_KPH)
		{
			if(tempW8<120)
			{
				tempW8=120;
			}
			else
			{
				;
			}
		}
		else
		{
			if(tempW8<100)
			{
				tempW8=100;
			}
			else
			{
				;
			}
		}
	}
	else if(tempW8<60)
	{
		tempW8=60;
	}
	else
	{
		;
	}
    o_delta_yaw_third_ems=(int16_t) ( ( (int32_t)o_delta_yaw_thres_eec*tempW8)/100);	
    o_delta_yaw_third_ems=LCTCS_s16ILimitRange(o_delta_yaw_third_ems, -30000, 30000);
	#endif /*#(__EEC_OVERSTEER_THRESHOLD == 1)*/
}

void LCESP_vDetectForEEC(void)
{
    tempB0=2;
  
	if((mtp>tempB0)&&(!VDC_DISABLE_FLG)&&(drive_torq>=U8_EEC_START_DRIVE_TORQUE))
    {
        if(!ESP_TCS_ON)
        {   /* (D_YAW_ON==0)&&(D_AY_ON==0)	*/
            esp_cmd=esp_cmd_yaw=drive_torq;
            LCESP_vDetectDeltaYawCondition();

          	if((D_YAW_ON)||(D_AY_ON))
          	{
          	
          		ESP_TCS_ON=1;
          		ESP_TCS_ON_START=1;

          		EEC_flags = 1;
          	}
          	else
          	{
          		;
          	}
        }
        else
        {		/* ESP_TCS_ON==1 */
            if(!D_YAW_ON)
            {   /* (D_YAW_ON==0)&&(D_AY_ON==1)	*/
                esp_cmd_yaw=drive_torq;
				LCESP_vDetectDeltaYawCondition();

                EEC_flags = 2;
            }
            else
            {
            	;
            }
        }
    }
    else
    {
       	;
    }

	/* 제어중 VDC_DISABLE_FLG set되는 경우 제어 종료 위해서 every scan 수행 */
	if(ESP_TCS_ON)
	{
		LCESP_vDetectEECExitCondition();
	}
	else
	{
		LCESP_vInitializeEEC();
	}
}

void LCESP_vDetectDeltaYawCondition(void)
{
    if((delta_yaw_eec<delta_yaw_third_ems))
    {
    	D_YAW_ON=1; /* Under steer */
	}
    else
    {
       	;
    }

        #if (__EEC_OVERSTEER_THRESHOLD == 1)
    if((delta_yaw_eec>o_delta_yaw_third_ems))
        #else	
   	if((delta_yaw_eec>o_delta_yaw_thres_eec))
   		#endif /*(__EEC_OVERSTEER_THRESHOLD == 1)*/
   	{
   		D_YAW_ON=1; /* Over steer */
   	}
   	else
    {
   		;
   	}

  #if __SEVERE_CORNERG
    if(SEVERE_CORNERG)
    {
    	D_YAW_ON=1; /* Severe cornerg */
    }
	else
	{
		;
	}
  #endif
}

void LCESP_vDetectEECExitCondition(void)
{
	/* ESP_TCS_ON==0인 경우 exit_time reset */
	if(!ESP_TCS_ON)
	{
		ETO_exit_time=0;
	}
	else
	{
		;
	}

    #if __CAR==FORD_C214
    	if((esp_cmd>=drive_torq+20)&&(delta_yaw_eec>delta_yaw_third_ems)&&(delta_yaw_eec<o_delta_yaw_thres_eec)) {
    #else
      #if __MY_08 && __EEC_EXIT_CONCEPT_ADDON
        #if (__EEC_OVERSTEER_THRESHOLD == 1)
        if((esp_cmd>=drive_torq)&&(delta_yaw_eec>delta_yaw_third_ems)&&(delta_yaw_eec<o_delta_yaw_third_ems)) {
        #else
        if((esp_cmd>=drive_torq)&&(delta_yaw_eec>delta_yaw_third_ems)&&(delta_yaw_eec<o_delta_yaw_thres_eec)) {
        #endif /*(__EEC_OVERSTEER_THRESHOLD == 1)*/
      #else
        if((esp_cmd>=drive_torq)&&(delta_yaw_eec>delta_yaw_third_ems)) {
      #endif
    #endif
        ETO_exit_time++;
    }
	else
	{
		;
	}

    #if __CAR==FORD_C214
	    tempW6=143; /* 3 sec => 426*/
    #else
    	tempW6=143; /* 1 sec */
    #endif

    if((ETO_exit_time>(uint16_t)tempW6)||(mtp<5)||(drive_torq<=0)||(VDC_DISABLE_FLG))
    {
		LCESP_vInitializeEEC();
    }
  #if __GM_FailM
    else if((lctcsu1EngCtrlInhibit==1)||(lctcsu1InhibitByFM==1)||(fu1ESCDisabledBySW==1))
	{
 		LCESP_vInitializeEEC();
    }
    else if((fu1YawSenPwr1sOk==0)||(fu1AySenPwr1sOk==0)||(fu1SteerSenPwr1sOk==0)||(fu1McpSenPwr1sOk==0))
	{
 		LCESP_vInitializeEEC();
    }    
  #else
   	 #if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
    else if((vdc_on_flg==0)||(lcu1TcsCtrlFail==1))
  	 #else      /*NEW_FM_ENABLE*/
    else if((vdc_on_flg==0)||(tcs_error_flg==1))
  	 #endif	   /*NEW_FM_ENABLE*/
	{
 		LCESP_vInitializeEEC();
    }    
  #endif
  	else
	{
		;
	}

	#if __EEC_PD_CONTROL
	LCESP_vDetectFadeMode();
	#endif
}

#if __EEC_PD_CONTROL
void LCESP_vDetectFadeMode(void)
{
    if(ESP_TCS_ON)
    {
            #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
		if((s8SetYawPDGainSelecter==STATE_YAW_STABLE)&&(ldespu8EECOverSpeed==0))
		    #else
		if(s8SetYawPDGainSelecter==STATE_YAW_STABLE)
		    #endif
		{
			if((eec_stable_exit_count<U16_EEC_YAW_FADE_TIME_MAX))
			{
				eec_stable_exit_count++;
			}
			else
			{
				;
			}
		}
		else
		{
			eec_stable_exit_count=0;
		}
		lcespu16EECYawFadeTime = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,U16_EEC_YAW_FADE_TIME_LM,
					                             				 S16_MM_EMS,U16_EEC_YAW_FADE_TIME_MM,
					                             				 S16_HM_EMS,U16_EEC_YAW_FADE_TIME_HM);
			#if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
		if((eec_stable_exit_count>lcespu16EECYawFadeTime) || (lcespu1EECFadeoutEnd == 1))
			#else
		if(eec_stable_exit_count>lcespu16EECYawFadeTime)
            #endif
		{
			LCESP_vInitializeEEC();
		}
		else
		{
			;
		}
	}
	else
	{
		eec_stable_exit_count=0;
	}
}
#endif

void LCESP_vTargetTorque1st(void)
{
    /*tempW2 = (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR);*/
    tempW3 = (int16_t) ( ( (int32_t)Mass*det_alatm)/1000);
    
    tempW3 = (int16_t) ( ( (int32_t)tempW3*U16_TIRE_L)/6280);

    tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,(int16_t)U16_EEC_LM_TARGET_GAIN_1ST_CYC,
				                             S16_MM_EMS,(int16_t)U16_EEC_MM_TARGET_GAIN_1ST_CYC,
				                             S16_HM_EMS,(int16_t)U16_EEC_HM_TARGET_GAIN_1ST_CYC);

 #if (__EEC_1ST_DROP_RATE==1)

	  esp_tempW0 = LCESP_s16IInter3Point(vrefk,S16_HM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_LSP,
				                               S16_HM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_MSP,
				                               S16_HM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_HSP);
    
	 	esp_tempW1 = LCESP_s16IInter3Point(vrefk,S16_MM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_LSP,
				                               S16_MM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_MSP,
				                               S16_MM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_HSP);
				                               				                               
	  	esp_tempW2 = LCESP_s16IInter3Point(vrefk,S16_LM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_LSP,
				                               S16_LM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_MSP,
				                               S16_LM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_HSP);				                               
    
   		esp_tempW3 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS, esp_tempW2, S16_MM_EMS, esp_tempW1,S16_HM_EMS,esp_tempW0);

    eec_target_torq_1st = (int16_t) ( ( (int32_t)tempW3*tempW1)/100);
#endif


    if(cal_torq<=eng_torq)
    {
    	tempW5=cal_torq;
    }
    else
    {
    	tempW5=eng_torq;
    }

	if(eec_target_torq_1st>tempW5)
	{
		#if (__EEC_1ST_DROP_RATE==1)
		  tempW1=tempW5-(drive_torq_div_20*esp_tempW3)/5;
		#else
		tempW1=tempW5-(drive_torq_div_20*S8_EEC_1ST_CONTROL_DROP_RATE_MSP)/5;
		#endif 
	}
	else
	{
		tempW1=eec_target_torq_1st;
	}

    if(tempW1<eec_target_torq_1st)
    {
       	eec_target_torq_1st=tempW1;
    }
    else
    {
    	;
    }

    if(eec_target_torq_1st < U16_EEC_TARGET_TORQ_1ST_MIN)
    {
        eec_target_torq_1st = U16_EEC_TARGET_TORQ_1ST_MIN;
    }
    else if(eec_target_torq_1st > U16_EEC_TARGET_TORQ_1ST_MAX)
    {
        eec_target_torq_1st = U16_EEC_TARGET_TORQ_1ST_MAX;
    }
    else
    {
        ;
    }

    esp_cmd = eec_target_torq_1st;

	if(esp_cmd>eng_torq)
	{
		esp_cmd=eng_torq;
	}
	else if(esp_cmd>cal_torq)
	{
		esp_cmd=cal_torq;
	}
	else
	{
		;
	}
}

void LCESP_vControlForEEC(void)
{
	#if (__EEC_1ST_DROP_RATE==1)
	  esp_tempW0 = LCESP_s16IInter3Point(vrefk,S16_HM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_LSP,
				                               S16_HM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_MSP,
				                               S16_HM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CONTROL_DROP_RATE_HSP);
				                             
	 	esp_tempW1 = LCESP_s16IInter3Point(vrefk,S16_MM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_LSP,
				                               S16_MM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_MSP,
				                               S16_MM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_MU_HSP);
				                               				                               
	  	esp_tempW2 = LCESP_s16IInter3Point(vrefk,S16_LM_ESP_TORQ_CON_LSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_LSP,
				                               S16_LM_ESP_TORQ_CON_MSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_MSP,
				                               S16_LM_ESP_TORQ_CON_HSP,(int16_t)S8_EEC_1ST_CNTR_DROP_RATE_LU_HSP);				                               
				                             				                             
   		esp_tempW3 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS, esp_tempW2, S16_MM_EMS, esp_tempW1,S16_HM_EMS,esp_tempW0);

   #endif 
   
#if (__MGH80_EEC_IMPROVE_CONCEPT == 1)

        esp_tempW4 = LCESP_s16IInter3Point(det_alatm, 
                                          S16_LM_EMS, U8_EEC_START_CYCLE_LM,
                                          S16_MM_EMS, U8_EEC_START_CYCLE_MM,
                                          S16_HM_EMS, U8_EEC_START_CYCLE_HM);

    if(ESP_TCS_ON_START)
    {
        if (lcespu8EECStartCnt >= esp_tempW4)
        {
            ESP_TCS_ON_START = 0;
            lcespu8EECStartCnt = 0;
        } 
        else
        {
            ESP_TCS_ON_START = 1;
            lcespu8EECStartCnt = lcespu8EECStartCnt + 1;
        }
        
        if(lcespu8EECStartCnt == 1)
        {
            lcesps16EECDropRate = (eng_torq_div_20*esp_tempW3)/5;
        }
        else
        {
            ;
        }
        
        
		if((ETCS_ON==0)&&(FTCS_ON==0))
	    {
	    	if(esp_cmd>eng_torq)
    	    {
    	    	esp_cmd=eng_torq-lcesps16EECDropRate;
        		
        	}
        	else
        	{
        		esp_cmd=cal_torq-lcesps16EECDropRate;
        	}
        	esp_tcs_count = 0;
        	s8SetYawPDGainSelecter = STATE_YAW_UNDERSTEER;
        }
        else
        {
        	esp_tcs_count = __EEC_CONTROL_CYCLE;

       		esp_cmd=cal_torq-lcesps16EECDropRate;       	
        	
        }

        if(D_YAW_ON)
        {
        	esp_cmd_yaw = esp_cmd;
        }
        else
        {
        	;
        }

        esp_cmd_initial=esp_cmd;

        s16YawErr = s16YawErrAlt = delta_yaw_eec;	/* means positive value */

   	  #if __EEC_GEAR_INHIBIT_MODE
        EEC_ACTIVATE_CHECK = 1;
   	  #endif

    }                                          
#else
    if(ESP_TCS_ON_START)
    {
        ESP_TCS_ON_START=0;

		if((ETCS_ON==0)&&(FTCS_ON==0))
	    {
	    	if(esp_cmd>eng_torq)
    	    {
    	    	if(U8_EEC_1ST_CONTROL==1)
    	    	{
   	    			LCESP_vTargetTorque1st();
   	    		}
        		else
        		{
 #if (__EEC_1ST_DROP_RATE==1)
        			esp_cmd=eng_torq-(drive_torq_div_20*esp_tempW3)/5;
 #else
        			esp_cmd=eng_torq-(drive_torq_div_20*S8_EEC_1ST_CONTROL_DROP_RATE_MSP)/5;
 #endif 
        		}
        	}
        	else
        	{
        		if(U8_EEC_1ST_CONTROL==1)
        		{
 					LCESP_vTargetTorque1st();
 				}
        		else
        		{

 #if (__EEC_1ST_DROP_RATE==1)
        			esp_cmd=cal_torq-(drive_torq_div_20*esp_tempW3)/5;
 #else
        			esp_cmd=cal_torq-(drive_torq_div_20*S8_EEC_1ST_CONTROL_DROP_RATE_MSP)/5;
 #endif 
        		}
        	}
        	esp_tcs_count = 0;
        	s8SetYawPDGainSelecter = STATE_YAW_UNDERSTEER;
        }
        else
        {
        	esp_tcs_count = __EEC_CONTROL_CYCLE;

       		if(U8_EEC_1ST_CONTROL==1)
       		{
				LCESP_vTargetTorque1st();
			}
        	else
        	{
 #if (__EEC_1ST_DROP_RATE==1)
        		esp_cmd=cal_torq-(drive_torq_div_20*esp_tempW3)/5;
 #else
        		esp_cmd=cal_torq-(drive_torq_div_20*S8_EEC_1ST_CONTROL_DROP_RATE_MSP)/5;
 #endif         	
        	}
        }

        if(D_YAW_ON)
        {
        	esp_cmd_yaw = esp_cmd;
        }
        else
        {
        	;
        }

        esp_cmd_initial=esp_cmd;

        s16YawErr = s16YawErrAlt = delta_yaw_eec;	/* means positive value */

   	  #if __EEC_GEAR_INHIBIT_MODE
        EEC_ACTIVATE_CHECK = 1;
   	  #endif

    }
#endif    
	else
	{
	    #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
        lcespu8EECStartCnt = 0;
        lcesps16EECDropRate = 0;
        #else
        ;
        #endif
	}

  #if __EEC_GEAR_INHIBIT_MODE
	if(EEC_ACTIVATE_CHECK==1)
	{
		if(ESP_TCS_ON)
		{
			EEC_ACTIVATED = 1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}

	if(EEC_ACTIVATED)
	{
		if(ESP_TCS_ON)
		{
			EEC_GEAR_INHIBIT_REQUEST=1;
/*			eec_gear_inhibit_count=EEC_GEAR_INHIBIT_SCAN; */
		}
/*		else if(((ETCS_ON==1)||(FTCS_ON==1)) && (eng_rpm<4000) && (gs_pos>1) && (TORQUE_FAST_RECOVERY_AFTER_ETO==0))
		{
			EEC_GEAR_INHIBIT_REQUEST=1;
		}  */
/*		else if(eec_gear_inhibit_count>0)
		{
			eec_gear_inhibit_count--;
		}
*/		else
		{
			EEC_ACTIVATED=0;
			EEC_GEAR_INHIBIT_REQUEST=0;
			eec_gear_inhibit_count=0;
		}
	}
	else
	{
		EEC_GEAR_INHIBIT_REQUEST=0;
	}
  #endif

    if(FLAG_DELTAYAW_SIGNCHANGE)
    {
    	DYAW_SIGNCHANGE_EEC=1;
    }
	else
	{
		;
	}

    esp_tcs_count++;
    if(esp_tcs_count >= __EEC_CONTROL_CYCLE)
    {
        esp_tcs_count=0;
        esp_cmd_alt=cal_torq;
        TorqLimit_alt=TorqLimit;

        esp_tempW4=delta_yaw_eec-delta_yaw_third_ems;
        if(DYAW_SIGNCHANGE_EEC)
        {
        	diff_yaw_err=diff_yaw_err;   /* diff_yaw_err의 급격한 변화 방지	*/
        }
        else
        {
        	diff_yaw_err=esp_tempW4-yaw_err;
        }
        DYAW_SIGNCHANGE_EEC=0;
        yaw_err=esp_tempW4;

        diff_yaw_err3=(diff_yaw_err+diff_yaw_err_old+diff_yaw_err_old2)/3;
        diff_yaw_err_old2=diff_yaw_err_old;
        diff_yaw_err_old=diff_yaw_err;

	    if((mtp>tempB0)&&(!VDC_DISABLE_FLG))
	    {
            if(ESP_TCS_ON)
            {
                diff_delta_yaw=(delta_yaw_eec-delta_yaw_first_rate_alt)/10;

				tempW6=(int16_t) ( ( (int32_t)vref*11)/8);
                tempW6+=steering_effect;

                    if(D_YAW_ON)
                    {
                    	LCESP_vControlDeltaYawForEEC();
                    }
                    else
                    {
                    	;
                    }

                    LCESP_vCompareOutputForEEC();
          		/*------------------------------------------------------------------*/
                /* Torque Down Rate Limitation 										*/
                /* 전 scan의 cal_torq값과 현 scan에서 계산된 esp_cmd의 값을 비교	*/
                /* torque down rate가 설정된 limit을 넘지 못하도록 제한함		*/
                /*------------------------------------------------------------------*/
                LCESP_vCompensateTorqueCommand();

                //LCESP_vCompensateTorqueForEEC();
            }
            else
            {              /* ESP_TCS_ON==0 */
                esp_cmd=drive_torq;
            }
        }
        else
        {
        	;
        }
        delta_yaw_first_rate_alt=delta_yaw_eec;
        wstr_alt3=wstr;
    }
    else
    {
    	;
    }

    if(ESP_TCS_ON==1)
    {
    	GEAR_CH_TCS=0;
    	LCESP_vCompensateTorqueForEEC();

		esp_cmd = LCTCS_s16ICompensateEngineStall(esp_cmd);

    	if(esp_cmd>MAX_TORQUE)
    	{
       		esp_cmd=MAX_TORQUE;
    	}
		/*
    	else if(esp_cmd<fric_torq)
    	{
       		esp_cmd=fric_torq;
    	}
		*/
    	else
    	{
       		;
 		}
    }
    else
    {
       	;
    }
}

/* 노면 별 차속 별 토크 다운 기울기 변경 */
void LCESP_vCompensateTorqueCommand(void)
{
	if(esp_cmd_alt<drive_torq)
	{
		tempW4 = esp_cmd - esp_cmd_alt;

          #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
    	tempW8 = LCESP_s16IInter3Point(det_alatm,200,80,400,60,600,40);
    	tempW9 = LCESP_s16IInter3Point(vrefk,400,150,600,100,800,75);
		  #else
		tempW8 = LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_EEC_LM_TRQ_LIMIT,
    											  S16_MM_EMS, S16_EEC_MM_TRQ_LIMIT,
    											  S16_HM_EMS, S16_EEC_HM_TRQ_LIMIT);
    	tempW9 = LCESP_s16IInter3Point(vrefk,400,100,600,75,800,50);
		  #endif
    	tempW5 = (int16_t) ( ( (int32_t)tempW9*tempW8)/(-100));

 		tempW1 = LCESP_s16IInter3Point(det_alatm, S16_LM_EMS, S16_EEC_TORQ_LIMIT_MAX_LMU, 
 												  S16_MM_EMS, S16_EEC_TORQ_LIMIT_MAX_MMU,
 												  S16_HM_EMS, S16_EEC_TORQ_LIMIT_MAX_HMU); 
        
    	tempW5 = (int16_t) ( ( (int32_t)tempW9*tempW8)/(-100));

    	tempW1 = S16_EEC_TORQ_LIMIT_MAX;

		if(tempW5 <(-tempW1))
		{
			tempW5 = -tempW1;	
		}
		else
		{
			;
		}

		if((tempW4<0) && (tempW4<tempW5))
		{
			tempW4 = tempW5;
			esp_cmd = esp_cmd_alt + tempW5;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

void LCESP_vControlDeltaYawForEEC(void)
{
	#if __YAW_ERR_NEO
	s16YawErr = delta_yaw_eec;	/* means positive value */
	#else
	s16YawErr = delta_yaw_eec - delta_yaw_third_ems;	/* means positive value */
	#endif

	s16DiffYawErr = s16YawErr - s16YawErrAlt;

	if(FLAG_DELTAYAW_SIGNCHANGE)
	{
		if((s16YawErr-s16YawErrAlt)>100)
		{
			s16DiffYawErr = s16YawErr + s16YawErrAlt;
		}
		else if((s16YawErr-s16YawErrAlt)<-100)
		{
			s16DiffYawErr = -s16YawErr - s16YawErrAlt;
		}
	}
    else
    {
       	;
    }

	s16DiffYawErr 		= yaw_error_under_dot;

 	LCESP_vCalYawPDGainForEEC();

    /* P Gain Contribution Calculation */
    s16YawPGainEffect=(int16_t) ( ( (int32_t)s16YawErr*s16SetYawPGain)/1000);

    /* D Gain Contribution Calculation */
    s16YawDGainEffect=(int16_t) ( ( (int32_t)s16DiffYawErr*s16SetYawDGain)/1000);

	s16YawErrAlt	= s16YawErr;
	s16DiffYawErrAlt = s16DiffYawErr;

	s16YawPDEffect = (s16YawPGainEffect+s16YawDGainEffect);

	LCESP_vCompensateYawPDEffect();

    esp_cmd_yaw=esp_cmd_alt+s16YawPDEffect;
}

#if __EEC_PD_CONTROL
void LCESP_vCompensateYawPDEffect(void)
{

	if(s16YawPDEffect==0)
	{
		s16YawPDEffect=1;
	}
    else
    {
       	;
    }

	if(s16YawPDEffect<0)
	{
		if(esp_cmd_alt>drive_torq)
		{
			esp_cmd_alt=(drive_torq);
		}
		else
		{
			;
		}
	}
    else
    {
       	;
    }
}
#endif

void LCESP_vCompareOutputForEEC(void)
{
	/*------------------------------------------------------------------------------*/
	/* esp_cmd를 설정, 																*/
	/* 1.D_YAW_ON이 뜬 경우															*/
	/*------------------------------------------------------------------------------*/
	if(D_YAW_ON)
    {				/* D_YAW_ON=1 && D_AY_ON=0	*/
    	esp_cmd=esp_cmd_yaw;
    }
    else /* D_YAW_ON=0 && D_AY_ON=0 -> 이 경우는 EEC_COMPARE() 수행안함	*/
    {
    	;
    }
}


void LCESP_vDetectStraightForEEC(void)
{
	if((abs_wstr<WSTR_20_DEG)&&(McrAbs(delta_yaw_eec)<YAW_1DEG)
    	&&(McrAbs(delta_alat_ems)<LAT_0G07G))
    {	/* 직진판단	*/
		if(eec_straight_cnt<L_U8_TIME_10MSLOOP_50MS)
		{
			eec_straight_cnt++;
		}
		else
		{
			;
		}
	}
	else
	{
		if(eec_straight_cnt>0)
		{
			eec_straight_cnt--;
		}
		else
		{
			;
		}
	}

	if(EEC_STRAIGHT)
	{
		if(eec_straight_cnt<L_U8_TIME_10MSLOOP_30MS)
		{
			EEC_STRAIGHT=0;
		}
		else
		{
			;
		}
	}
	else
	{
		if(eec_straight_cnt>=L_U8_TIME_10MSLOOP_50MS)
		{
			EEC_STRAIGHT=1;
		}
		else
		{
			;
		}
	}

	/* When a large spin occured, but there is no torque down */
	/* spin (dv) = esp_tempW3 = 0.125kph ~ 10kph */
	esp_tempW3 = LCESP_s16IInter2Point((wslip_max*vref5)/100,VREF_0_125_KPH,VREF_0_125_KPH,VREF_10_KPH,VREF_10_KPH);
	/* spin (dv) >= 5 kph */

	if(esp_tempW3 >= (int16_t)VREF_5_KPH)
	{
		if(EEC_STRAIGHT)
		{
			esp_cmd=esp_cmd+3;
		}
		else
		{
			;
		}
	}
	else
	{
		if(EEC_STRAIGHT)
		{
			esp_cmd=esp_cmd_alt+10;
		}
		else
		{
			;
		}
	}
}

void LCESP_vCalTorqueLimitForEEC(void)
{
    if(esp_cmd_yaw<fric_torq)
    {
       	esp_cmd_yaw=fric_torq;
    }
    else
    {
       	;
    }

    if(esp_cmd_yaw>MAX_TORQUE)
    {
       	esp_cmd_yaw=MAX_TORQUE;
    }
    else
    {
       	;
    }
}

void LCESP_vFadeOutControl(void)
{                                 
	tempW2 = delta_yaw_third_ems;
	#if (__EEC_OVERSTEER_THRESHOLD == 1)
	tempW3 = o_delta_yaw_third_ems;
	#else
	tempW3 = o_delta_yaw_thres_eec;
	#endif /*(__EEC_OVERSTEER_THRESHOLD == 1)*/

	  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
	if((delta_yaw_eec>(tempW2))&&(delta_yaw_eec<(tempW3))&&(esp_cmd<drive_torq))
	{
	    tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,U8_STATE_YAW_STABLE_RISE_LM,
					                             S16_MM_EMS,U8_STATE_YAW_STABLE_RISE_MM,
					                             S16_HM_EMS,U8_STATE_YAW_STABLE_RISE_HM);
		if (drive_torq > drive_torq_old)
        {
            tempW1 = tempW1 + (drive_torq - drive_torq_old);
        }
        else
        {
            ;
        }	
	    
		EEC_FADE_OUT_CTRL_ON=1;
        lcespu1EECFadeoutEnd = 0;
        
	}
	else if (esp_cmd>=drive_torq)
	{
	    EEC_FADE_OUT_CTRL_ON=0;  
	    lcespu1EECFadeoutEnd = 1;
	}
	else
	{
		EEC_FADE_OUT_CTRL_ON=0;
		lcespu1EECFadeoutEnd = 0;
		tempW1 = 1;
	}
      #else
	if((delta_yaw_eec>(tempW2))&&(delta_yaw_eec<(tempW3))&&(esp_cmd<esp_cmd_initial))
	{
		tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,U8_STATE_YAW_STABLE_RISE_LM,
					                             S16_MM_EMS,U8_STATE_YAW_STABLE_RISE_MM,
					                             S16_HM_EMS,U8_STATE_YAW_STABLE_RISE_HM);
		EEC_FADE_OUT_CTRL_ON=1;
	}
	else
	{
		EEC_FADE_OUT_CTRL_ON=0;
		tempW1 = 1;
	}
	  #endif

	if(esp_tcs_count == 0)
	{
		if(esp_cmd < (cal_torq+tempW1))
		{
			esp_cmd=cal_torq+tempW1;
			EEC_flags = 60;
		}
		else
		{
			EEC_flags = 62;
		}
	}
	else
	{
		if(esp_cmd < (cal_torq))
		{
			esp_cmd=cal_torq;
			EEC_flags = 61;
		}
		else
		{
			EEC_flags = 63;
		}
	}
}

void LCESP_vCompensateTorqueForEEC(void)
{

    /* Compensate the output torque of TCS controller (cal_torq) */
    if((s8SetYawPDGainSelecter==STATE_YAW_UNDERSTEER)&&(esp_cmd<cal_torq))
    {
    	EEC_flags = 70;
		  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
    	EEC_FADE_OUT_CTRL_ON=0;
        lcespu1EECFadeoutEnd = 0;
		  #endif
	}
    else if((s8SetYawPDGainSelecter==STATE_YAW_OVERSTEER)&&(esp_cmd<cal_torq))
    {
    	EEC_flags = 71;
		  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
    	EEC_FADE_OUT_CTRL_ON=0;
        lcespu1EECFadeoutEnd = 0;
		  #endif
	}
    else if((s8SetYawPDGainSelecter==STATE_YAW_UNSTABLE)&&(esp_cmd<cal_torq))
    {
    	EEC_flags = 72;
		  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
    	EEC_FADE_OUT_CTRL_ON=1;
        lcespu1EECFadeoutEnd = 0;
		  #endif
	}
	else if((s8SetYawPDGainSelecter==STATE_YAW_STABLE))
	{
	      #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
	    if (ldespu8EECOverSpeed == 0)
	    {  
	        if(U8_EEC_FADE_OUT_CONTROL==1)
		    {
		    	LCESP_vFadeOutControl();
		    }
		    else
		    {
		    	esp_cmd=cal_torq+3;
		    }  
		}
		else
		{
		    if (ldespu8EECOverSpeed == 1)
            {
                tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,U8_STATE_YAW_STABLE_SPD_RISE_LM, 
	            		                                 S16_MM_EMS,U8_STATE_YAW_STABLE_SPD_RISE_MM, 
	            		                                 S16_HM_EMS,U8_STATE_YAW_STABLE_SPD_RISE_HM);                            
	        }                                                                                     
	        else if (ldespu8EECOverSpeed == 2)                                                                                 
	        {
	            tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,U8_STATE_YAW_STABLE_OSPD_RISE_LM,
                                                         S16_MM_EMS,U8_STATE_YAW_STABLE_OSPD_RISE_MM, 
                                                         S16_HM_EMS,U8_STATE_YAW_STABLE_OSPD_RISE_HM);
            }
            else
            {
                tempW1 = 3;
            }
    
        	EEC_FADE_OUT_CTRL_ON=0;
        }
        if(esp_tcs_count == 0)
	    {
	    	if(esp_cmd < (cal_torq+tempW1))
	    	{
	    		esp_cmd=cal_torq+tempW1;
	    		EEC_flags = 80;
	    	}
	    	else
	    	{
	    		EEC_flags = 81;
	    	}
	    }
	    else
	    {
	    	if(esp_cmd < (cal_torq))
	    	{
	    		esp_cmd=cal_torq;
	    		EEC_flags = 82;
	    	}
	    	else
	    	{
	    		EEC_flags = 83;
	    	}
	    }
	      #else      
		if(U8_EEC_FADE_OUT_CONTROL==1)
		{
			LCESP_vFadeOutControl();
		}
		else
		{
			esp_cmd=cal_torq+3;
		}
		  #endif
	}
	else
	{
		EEC_flags = 73;
		  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
		EEC_FADE_OUT_CTRL_ON=0;
        lcespu1EECFadeoutEnd = 0;
		  #endif
	}
}



void LCESP_vCalYawPDGainForEEC(void)
{
	/* 속도 4단계, 노면 3단계 */
	s8SetYawPDGainSelecter=STATE_YAW_DEFAULT;

	tempW0 = delta_yaw_third_ems;
	#if (__EEC_OVERSTEER_THRESHOLD == 1)
	tempW1 = o_delta_yaw_third_ems;
	#else
	tempW1 = o_delta_yaw_thres_eec;
	#endif /*(__EEC_OVERSTEER_THRESHOLD == 1)*/

	/* Understeer -> Unstable */
/*	if((delta_yaw_eec<delta_yaw_third_ems)&&(D_YAW_DIV)) */
	if((delta_yaw_eec<tempW0)&&(s16DiffYawErr<=0))
	{
		s8SetYawPDGainSelecter=STATE_YAW_UNDERSTEER;
        ETO_exit_time=0;
	}
	/* Unstable -> Stable */
/*	else if((delta_yaw_eec<delta_yaw_third_ems)&&(!D_YAW_DIV)) */
	else if((delta_yaw_eec<tempW0)&&(s16DiffYawErr>0))
	{
		s8SetYawPDGainSelecter=STATE_YAW_UNSTABLE;
	}
	/* Oversteer */
	else if((delta_yaw_eec>tempW1)&&(s16DiffYawErr>=0))
	{
		s8SetYawPDGainSelecter=STATE_YAW_OVERSTEER;
	}
	/* Oversteer -> Stable */
	else if((delta_yaw_eec>tempW1)&&(s16DiffYawErr<0))
	{
		s8SetYawPDGainSelecter=STATE_YAW_OVSTABLE;
	}	
	/* Stable */
	else
	{
		s8SetYawPDGainSelecter=STATE_YAW_STABLE;
	}

	switch(s8SetYawPDGainSelecter)
	{
		case STATE_YAW_UNDERSTEER:
			for(s8LoopCounter=0;s8LoopCounter<4;s8LoopCounter++)
			{
				s16SetYawPGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainUnderState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainUnderState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainUnderState[2][s8LoopCounter]);
				s16SetYawDGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawDGainUnderState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawDGainUnderState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawDGainUnderState[2][s8LoopCounter]);
			}
		break;

		case STATE_YAW_UNSTABLE:
			for(s8LoopCounter=0;s8LoopCounter<4;s8LoopCounter++)
			{
				s16SetYawPGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainUnstableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainUnstableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainUnstableState[2][s8LoopCounter]);
				s16SetYawDGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawDGainUnstableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawDGainUnstableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawDGainUnstableState[2][s8LoopCounter]);
			}
		break;

		case STATE_YAW_OVERSTEER:
			for(s8LoopCounter=0;s8LoopCounter<4;s8LoopCounter++)
			{
				s16SetYawPGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainOverState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainOverState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainOverState[2][s8LoopCounter]);
				s16SetYawDGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawDGainOverState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawDGainOverState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawDGainOverState[2][s8LoopCounter]);
			}
		break;

		case STATE_YAW_OVSTABLE:
			for(s8LoopCounter=0;s8LoopCounter<4;s8LoopCounter++)
			{
				s16SetYawPGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainOvstableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainOvstableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainOvstableState[2][s8LoopCounter]);
				s16SetYawDGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawDGainOvstableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawDGainOvstableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawDGainOvstableState[2][s8LoopCounter]);
			}
		break;
		
		case STATE_YAW_STABLE:
			for(s8LoopCounter=0;s8LoopCounter<4;s8LoopCounter++)
			{
				s16SetYawPGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainStableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainStableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainStableState[2][s8LoopCounter]);
				s16SetYawDGainTemp[s8LoopCounter]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawDGainStableState[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawDGainStableState[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawDGainStableState[2][s8LoopCounter]);				                                                              
				#if (EEC_CURVATURE_COMPENSATION==1)
					/* R = 100~500 => 300~500 기준변경 */
					esp_tempW1 = LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,s16SetYawPGainStablCurPer[0][s8LoopCounter],
				                                                              S16_MM_EMS,s16SetYawPGainStablCurPer[1][s8LoopCounter],
				                                                              S16_HM_EMS,s16SetYawPGainStablCurPer[2][s8LoopCounter]);
					esp_tempW2 = s16SetYawPGainTemp[s8LoopCounter];
					esp_tempW3 = esp_tempW2*esp_tempW1/100;

					s16SetYawPGainTemp[s8LoopCounter] = LCESP_s16IInter2Point(curv_radius_lf,3000,esp_tempW3,
					                                                                         5000,esp_tempW2);

				#endif

			}
		break;
	}

	LOW_MU_ESP_TORQ_CON_U_SPEED = S16_LM_ESP_TORQ_CON_HSP+200;
	MED_MU_ESP_TORQ_CON_U_SPEED = S16_MM_ESP_TORQ_CON_HSP+200;
	HIGH_MU_ESP_TORQ_CON_U_SPEED = S16_HM_ESP_TORQ_CON_HSP+200;

	s16SetYawPDGainSpeed[0]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,S16_LM_ESP_TORQ_CON_LSP,
                                                        S16_MM_EMS,S16_MM_ESP_TORQ_CON_LSP,
                                                        S16_HM_EMS,S16_HM_ESP_TORQ_CON_LSP);

	s16SetYawPDGainSpeed[1]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,S16_LM_ESP_TORQ_CON_MSP,
                                                        S16_MM_EMS,S16_MM_ESP_TORQ_CON_MSP,
                                                        S16_HM_EMS,S16_HM_ESP_TORQ_CON_MSP);

	s16SetYawPDGainSpeed[2]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,S16_LM_ESP_TORQ_CON_HSP,
                                                        S16_MM_EMS,S16_MM_ESP_TORQ_CON_HSP,
                                                        S16_HM_EMS,S16_HM_ESP_TORQ_CON_HSP);

	s16SetYawPDGainSpeed[3]=LCESP_s16IInter3Point(det_alatm,S16_LM_EMS,LOW_MU_ESP_TORQ_CON_U_SPEED,
                                                        S16_MM_EMS,MED_MU_ESP_TORQ_CON_U_SPEED,
                                                        S16_HM_EMS,HIGH_MU_ESP_TORQ_CON_U_SPEED);


	s16SetYawPGain=LCESP_s16IInter4Point(vrefk,s16SetYawPDGainSpeed[0],s16SetYawPGainTemp[0],
				                           s16SetYawPDGainSpeed[1],s16SetYawPGainTemp[1],
				                           s16SetYawPDGainSpeed[2],s16SetYawPGainTemp[2],
				                           s16SetYawPDGainSpeed[3],s16SetYawPGainTemp[3]);

	s16SetYawDGain=LCESP_s16IInter4Point(vrefk,s16SetYawPDGainSpeed[0],s16SetYawDGainTemp[0],
				                           s16SetYawPDGainSpeed[1],s16SetYawDGainTemp[1],
				                           s16SetYawPDGainSpeed[2],s16SetYawDGainTemp[2],
				                           s16SetYawPDGainSpeed[3],s16SetYawDGainTemp[3]);

    if(U8_EEC_UTURN_COMPENSATE==1)
    {
		if((delta_yaw_eec<S16_EEC_UTURN_COMP_DYAW_THR1)&&(s8SetYawPDGainSelecter==STATE_YAW_UNSTABLE))
		{
			if(s16SetYawPGain<0)
			{
				s16SetYawPGain=LCESP_s16IInter2Point(delta_yaw_eec,S16_EEC_UTURN_COMP_DYAW_THR2,0,
																   S16_EEC_UTURN_COMP_DYAW_THR1,s16SetYawPGain);
			}
			else
			{
				;
			}
			if(s16SetYawDGain>0)
			{
				s16SetYawDGain=LCESP_s16IInter2Point(delta_yaw_eec,S16_EEC_UTURN_COMP_DYAW_THR2,0,
																   S16_EEC_UTURN_COMP_DYAW_THR1,s16SetYawDGain);
			}
			else
			{
				;
			}
			
			EEC_UTURN_CTRL_ON=1;
		}
		else
		{
			EEC_UTURN_CTRL_ON=0;
		}
	}
	else
	{
		EEC_UTURN_CTRL_ON=0;
	}
}


void LCESP_vInitializeEEC(void)
{
    ESP_TCS_ON=0;
    D_YAW_ON=0;
    D_AY_ON=0;
    esp_cmd=esp_cmd_yaw=drive_torq;

    ETO_exit_time=0;
    QUICK_OUT=0;
    quickout_counter=0;
	SEN_ADJ_TH=0;
    esp_cmd_initial=0;

	SLALOM_TORQ_DOWN=0;
	SEVERE_CORNERG=0;

    esp_cmd_initial=0;

	EEC_flags=0;
	EEC_YAW_flags=0;

	EEC_STRAIGHT=0;
	eec_straight_cnt=0;

  #if __EEC_PD_CONTROL
	s16YawErr=0;
	s16DiffYawErr=0;
	s16YawErrAlt=0;
	s16DiffYawErrAlt=0;
	s16YawPDEffect=0;
	s16YawPGainEffect=0;
	s16YawDGainEffect=0;

	s16SetYawPGain=0;
	s16SetYawDGain=0;
	s16SetYawPGainTemp[0]=s16SetYawPGainTemp[1]=s16SetYawPGainTemp[2]=s16SetYawPGainTemp[3]=0;
	s16SetYawDGainTemp[0]=s16SetYawDGainTemp[1]=s16SetYawDGainTemp[2]=s16SetYawDGainTemp[3]=0;
	s16SetYawPDGainSpeed[0]=s16SetYawPDGainSpeed[1]=s16SetYawPDGainSpeed[2]=s16SetYawPDGainSpeed[3]=0;
  #endif
	eec_stable_exit_count=0;

  #if __EEC_GEAR_INHIBIT_MODE
	EEC_ACTIVATE_CHECK=0;
  #endif

	eec_target_torq_1st=drive_torq;
	EEC_FADE_OUT_CTRL_ON=0;
	  #if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
	lcespu1EECFadeoutEnd=0;
	  #endif
	EEC_UTURN_CTRL_ON=0;
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCallEngineControl
	#include "Mdyn_autosar.h"
#endif

