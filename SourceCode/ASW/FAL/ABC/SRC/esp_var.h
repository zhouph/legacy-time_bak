#ifndef __ESP_VAR_H__
#define __ESP_VAR_H__
#include "Abc_Ctrl.h"
/* ---------->>>>>>>>>                VDC Variable                <<<<<<<<<----------*/
#if __VDC
    #include "v_struct.h"
#endif
extern U8_BIT_STRUCT_t VDCF0;
extern U8_BIT_STRUCT_t VDCF1;
extern U8_BIT_STRUCT_t VDCF2;
extern U8_BIT_STRUCT_t VDCF3;
extern U8_BIT_STRUCT_t VDCF4;
extern U8_BIT_STRUCT_t VDCF5;
extern U8_BIT_STRUCT_t VDCF6;
extern U8_BIT_STRUCT_t VDCF7;
extern U8_BIT_STRUCT_t VDCF8;
extern U8_BIT_STRUCT_t VDCF9;
extern U8_BIT_STRUCT_t VDCF10;
extern U8_BIT_STRUCT_t VDCF11;
extern U8_BIT_STRUCT_t VDCF12;
extern U8_BIT_STRUCT_t VDCF13;       // 2002.06.17. for VDC sensor offset
extern U8_BIT_STRUCT_t VDCF14;       // 2002.06.17. for VDC sensor offset
extern U8_BIT_STRUCT_t VDCF15;       // 2002.07.12. Not used!!
extern U8_BIT_STRUCT_t VDCF16;
extern U8_BIT_STRUCT_t VDCF17;
extern U8_BIT_STRUCT_t VDCF18;
extern U8_BIT_STRUCT_t VDCF19;
extern U8_BIT_STRUCT_t VDCF20;
extern U8_BIT_STRUCT_t VDCF21;
extern U8_BIT_STRUCT_t VDCF22;       // 2003.06.16 mpress offset
extern U8_BIT_STRUCT_t VDCF23;       // 2003.09.03 CIRCUIT FAIL
extern U8_BIT_STRUCT_t VDCF24;       // 2003.10.17 PARTIAL PERFORMANC
extern U8_BIT_STRUCT_t VDCF25;       // 2003.11.05
extern U8_BIT_STRUCT_t VDCF26;
extern U8_BIT_STRUCT_t VDCF27;
extern U8_BIT_STRUCT_t VDCF28;
extern U8_BIT_STRUCT_t VDCF29;
extern U8_BIT_STRUCT_t VDCF30;
extern U8_BIT_STRUCT_t VDCF31;
extern U8_BIT_STRUCT_t VDCF32;
extern U8_BIT_STRUCT_t VDCF33;
extern U8_BIT_STRUCT_t VDCF34;
extern U8_BIT_STRUCT_t VDCF35;
extern U8_BIT_STRUCT_t VDCF36;
extern U8_BIT_STRUCT_t VDCF37;
extern U8_BIT_STRUCT_t VDCF38;
extern U8_BIT_STRUCT_t VDCF39;
extern U8_BIT_STRUCT_t VDCF40;

#if __VDC
extern struct  OFFSET_STRUCT *PTR, KSTEER,KYAW,KLAT;
extern struct ARRANGE_STRUCT ARRANGE[5], imsi;
#endif

#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
extern int16_t lsesps16AHBGEN3mpress;
extern int16_t lsesps16AHBGEN3mpressOld;
#endif
extern int16_t esp_mu, vrefk, det_alatm, alat, yaw_out;
    extern int16_t esp_tempW0, esp_tempW1, esp_tempW2, esp_tempW3, esp_tempW4;
    extern int16_t esp_tempW5, esp_tempW6, esp_tempW7, esp_tempW8, esp_tempW9;
    extern int16_t esp_tempW10, esp_tempW11, esp_tempW12, esp_tempW13, esp_tempW14;
    extern int16_t esp_tempW15, esp_tempW16, esp_tempW17, esp_tempW18, esp_tempW19;
    extern int16_t esp_tempW20, esp_tempW21;
    extern int16_t lsesps16MuHigh, lsesps16MuMedHigh, lsesps16MuMed, lsesps16MuLow;
    extern int16_t mpress;
    extern int16_t wstr;
    extern U8_BIT_STRUCT_t PBAF0;
    extern int16_t delta_yaw;
    extern int16_t delta_yaw_first;
    extern int16_t   wstr_dot;
    extern int16_t lsesps16MuHigh, lsesps16MuMedHigh, lsesps16MuMed, lsesps16MuLow;

    /* AHB GEN3 Mpress Replacement */
    extern int16_t  lsesps16MpressByAHBgen3;
    extern int16_t  lsesps16MpressByAHBgen3Old; 
    extern int8_t lsesps8MpressByAHBgen3OnCT;    /* mpress_on_counter      */    
    extern int16_t lsesps16MpressByAHBgen3CkCT;  /* mpress_slop_check_count*/
    extern int16_t lsesps16MpressByAHBgen3Init;  /* init_mpress            */


 //   extern uint16_t tempyawold;
 //   extern uint16_t tempyaw2;
        extern int16_t yaw, yawold, yaw_out_old;
        extern int16_t det_Beta_MD_old, det_Beta_MD;
    extern int16_t yaw_diff;                      extern int16_t yaw_zero;
    extern int16_t model_backdetect_yawrate, model_backdetect_yawrate_old;
    extern uint8_t model_back_valid_count;
    extern uint8_t  esp_mini_tire_valid_count;
    extern uint8_t yaw_rate_count, forward_dir_count, backward_dir_count;
    extern int16_t yaw_sign, yaw_sign_old;
    extern int16_t control_wheel_dir, control_rear_wheel_dir;
    extern uint16_t counter_deltayaw_signchange;
    extern int16_t yaw_out, det_yaw_out, det_yaw_out_old, abs_yaw_out;
    extern int16_t    yawm_dot, yawm_lf, yawm_lf_old;
    extern int16_t    yawc_dot, yawc_lf, yawc_lf_old;
    extern int16_t mean_abs_yawc, mean_abs_yawc_old, mean_abs_yawm, mean_abs_yawm_old;
    extern int16_t mean_abs_yawc_buffer, mean_abs_yawm_buffer, mean_abs_yawc_buffer_old, mean_abs_yawm_buffer_old;
    extern int16_t gain_adap_yawc, abs_yawc;
    extern int32_t count_adap_yawc, count_adap_model;
    extern int16_t ay_yaw, initial_yaw_out, initial_alatm;
//    extern uint16_t yaw_vref_zero_count;          extern uint16_t yaw_zero_count;
    extern int16_t yaw_1_100deg_alt;
        extern int16_t yaw_1_100deg_alt2;
    extern int16_t yaw_diff_now_to_alt2;

 //   extern uint16_t wbs16ConfLgRate;
    extern int16_t alat, alat_t, alat_ems, alat_ems_old, abs_alat_diff;
    extern int16_t alatold;                       extern int16_t alatdiff;
    extern int16_t alat_zero;                     extern int16_t nor_alat;
    extern int16_t nor_alat_old;                  extern int16_t nor_alat2;
    extern int16_t nor_alat_unlimit, nor_alat_unlimit_old, nor_alat_unlimit_ems;
    extern int16_t delta_alat_offset, delta_alat_ems, delta_alat_ems_old, ems_stable_counter;
    extern int16_t under1_ay, under2_ay;
    extern int16_t nor_alat2_old;
    extern int16_t alat_filt_value1;              extern int16_t alat_filt_value2;
    extern int16_t delta_lat;                     extern int16_t delta_lat_old;
    extern int16_t delta_lat_diff;

//    extern int16_t wbs16ConfPressRate;
    extern int16_t mpress, mpress_diff, mpress_est;
    extern int8_t mpress_on_counter;
    extern int16_t mpress_old, mpress_est_old;    extern int16_t mpress_slop_check_count, mpress_suspect_old;
    extern int16_t init_mpress, delta_mpress_fl, delta_mpress_fr, delta_mpress_rl, delta_mpress_rr;
    extern int16_t MCpress[5];

//    extern int16_t wbu16ConfSteerCnt;                     extern int16_t wstr_360_offset_value;
    extern int16_t  wstr_360_thres;
    extern int16_t fail;
    extern int16_t wstr;                          extern int16_t wstrold;
    extern int16_t wstr_c;                        extern int16_t wstr_c_old;

    extern int32_t cyctime;
    extern int16_t vrefm;
    extern int16_t vrefk, vrefk_old;
    extern int16_t k_vref;
    extern int16_t e_vref_min;
    extern int16_t c1;                            extern int16_t c2;
    extern int16_t rfd;                           extern int16_t rfdo;
    extern int16_t rf;                            extern int16_t rfo;
    extern int16_t rf2,rf2_old;
    extern int16_t filter_gain_for_speed_in_yaw_limit_enter;
    extern int16_t yaw_limit_enter_delta_ay,yaw_limit_slow_wstr_dot_con_mid;
    extern int16_t yaw_limit_exit_speed;
    extern int16_t lrf;
    extern int16_t rfd2;                          extern int16_t rfdo2;
    extern int16_t rfo2;
    extern int16_t ab_rfm;                        extern int16_t ab_rf;
    extern int16_t yaw_limit_enter, k_mu, k_mu_old;
    extern int16_t ab_yawc;                       extern int16_t ab_delta_lat;
    extern int16_t ab_yawc_o;

//    extern int8_t TARGET_SLIP_REACH_1;
    extern int16_t max_ab_wstr1, yawc_down_gain_for_wstr,yawc_down_gain_for_wstr2;

//  Beta estimation  ...2003.10.15
#if __SIDE_SLIP_ANGLE_MODULE
        extern int16_t dot_yaw_fltr_old, dot_yaw_fltr, ys1_beta, ys2_beta ;
        extern int16_t xs1_beta_old , xs1_beta , xs2_beta_old , xs2_beta ;
        extern int16_t xh1_beta_old , xh1_beta , xh2_beta_old , xh2_beta ;
        extern int16_t yh1_beta_old , yh1_beta , yh2_beta_old , yh2_beta ;        
        extern int16_t d_xh1_beta_old , deriv_beta_est , xs1_beta_delay , beta_ref ;
#endif        
		extern int16_t xh1_beta_delay , beta_est, d_xh1_beta, front_beta_est, front_beta_ref;
        extern int16_t vxm_beta, dot_yaw,sam_t_beta,dot_yaw_fltr_old, dot_yaw_fltr ;
        extern int16_t sat_alat1, sat_alat2, Ff_sat_force, Fr_sat_force, alpha_f_s, alpha_r_s, Fy_front_s, Fy_rear_s ;
        extern int16_t alpha_f_o, alpha_r_o, sign_fyf, sign_fyr, Fy_front_o, Fy_rear_o, Fyf_pre, Fyr_pre ;
        extern int16_t o_delta_beta_thres_old, o_delta_beta_thres, u_delta_beta_thres_old, u_delta_beta_thres ;
        extern int16_t delta_beta_old, delta_beta, delta_beta2_old, delta_beta2, beta_sign ;
        extern int16_t beta_thres_base , beta_thres_vel , beta_thres_wstr_dot , beta_middle_thres ;
        extern int16_t beta_integ, beta_integ_old, beta_pseudo, beta_pseudo_old, time_pseudo, beta_fltr,beta_fltr_old;
        extern int16_t NN_thres, nn_fltr, Ff_observe, Fr_observe,  front_beta_pseudo, front_beta_pseudo_old;

    extern int16_t mass_beta  ;
    extern int16_t iiz_beta  ;
    extern int16_t Lf_beta ;                      // r=0.01
    extern int16_t Lr_beta  ;                     // r=0.01
    extern int16_t L_front  ;                     // r=0.01
    extern int16_t Stratio_beta  ;
    extern int16_t L11_beta ;
    extern int16_t L12_beta ;
    extern int16_t L21_beta ;
    extern int16_t L22_beta ;
    extern int16_t Cf_beta_i ;
    extern int16_t Cr_beta_i ;
//

    extern int16_t delta_yaw, delta_yaw_second, nosign_delta_yaw;
    extern int16_t wstr_betad, delta_yaw_third_k, delta_yaw_third, delta_yaw_third_old;
    extern int16_t delta_yaw_third_ems, delta_yaw_third_ems_alt;
    extern int16_t o_delta_yaw_third_ems, o_delta_yaw_third_ems_alt;  
    extern int16_t delta_yaw_first, delta_yaw_first_alt, delta_yaw_first_rate_alt,nosign_delta_yaw_first;
    extern int16_t delta_yaw_first2, delta_yaw_first_old2;
    extern int16_t delta_yaw_diff;                extern int16_t delta_yaw_old;
    extern int16_t delta_yaw2;                    extern int16_t delta_yaw_old2;
    extern int8_t delta_yaw2_rate_count;
    extern int16_t delta_yaw_rate_cal_old;

//    extern uint8_t pulse_down_type;
//    extern uint8_t over_under_jump_delay_counter;               extern uint8_t cdc_dump_time;

//    extern int16_t front_slip;                    extern int16_t rear_slip;
//    extern int16_t m_frslip_diff;
//    extern int16_t front_slip_c;                  extern int16_t rear_slip_c;
//    extern int16_t c_frslip_diff;

//    extern uint8_t fl_solvalve_count;           extern uint8_t fr_solvalve_count;
//    extern uint8_t rl_solvalve_count;           extern uint8_t rr_solvalve_count;
//    extern uint8_t fl_press_max_value;          extern uint8_t fr_press_max_value;
//    extern uint8_t rl_press_max_value;          extern uint8_t rr_press_max_value;
//    extern uint8_t vdc_low_count,  esp_esv_count_l, esp_esv_count_r ;
    extern uint8_t esp_on_counter,esp_on_counter_1;

//    extern uint16_t out_vdc_motor_count;
//    extern uint16_t real_pc_v_use_count;
//    extern uint16_t first_precharge_count;

//    extern int16_t delta_yaw_sum;
//    extern int16_t avr_delta_yaw;
//    extern int16_t avr_delta_yaw_old;
//    extern int16_t delta_yaw_rate_cal;
//    extern uint8_t pulse_up_type;
//    extern uint8_t cdc_reapply_time;
//    extern uint8_t vdc_yaw_control_type;
//    extern uint8_t first_yaw_rate_count;
//    extern uint8_t under_rise_count;
//    extern uint8_t under_hold_count;
//    extern int16_t   vdc_large_dump_diff_value;

//    extern uint8_t wstr_count;
    extern uint8_t wstr_stable_count;
//    extern int16_t   wstr_s;
//    extern int16_t   wstr_sum;
//    extern int16_t   wstr_e;
//    extern int16_t   del_wstr;
//    extern int16_t   del_wstr_old;
//    extern int16_t   av_wstr;
//    extern int16_t   wstr_av;
//    extern uint8_t wstr_unstab_c;
//    extern uint8_t wstr_offset_count;
    extern int16_t   yawgain;
    extern int16_t   yawno;
    extern int16_t    model_change_value;
    extern uint8_t str_f_count;
    extern uint8_t un_str_f_count;
    extern int16_t   ayno;
    extern int16_t   wstr_diff;

    extern uint8_t abs_yaw_control_type, abs_dump_time, abs_reapply_time , abs_hold_time;
    extern uint8_t abs_yaw_control_count, abs_yaw_control_count2;
    extern int16_t   delta_yaw_thres, delta_yaw_thres_old;
    extern int16_t   delta_yaw_middle_thres, delta_yaw_out_thres, delta_yaw_out_thres2;
    extern int16_t   thres_wstr_dot, thres_wstr, thres_vel;
    extern int16_t   thres_base_value, u_delta_yaw_thres, o_delta_yaw_thres;
    extern int16_t  sign_o_delta_yaw_thres,  sign_u_delta_yaw_thres;
    extern int16_t   u_delta_yaw_thres_old, o_delta_yaw_thres_old;
//    extern uint8_t abs_yaw_control_type2, abs_dump_time2, abs_reapply_time2;
//    extern uint8_t abs_hold_time2, abs_yaw_control_count3;
//    extern uint8_t abs_yaw_control_type3, abs_dump_time3, abs_reapply_time3;
//    extern uint8_t abs_hold_time3, abs_yaw_control_count4;
//
//    extern int16_t   test_v, test_v2;
//    extern int16_t   YED_at_pulse_dump,DY_at_pulse_dump;

//*********************************************************************
// steer sensor offset

    extern uint8_t pm4deg_cnt_at_steer1, pm4deg_cnt_at_steer2;
    extern uint8_t pm1deg_cnt_at_yaw1, pm1deg_cnt_at_yaw2;
    extern uint8_t more_than_pm2deg_at_steer3, more_than_pm2deg_at_steer4;
    extern int16_t wstr_old;
    extern int16_t c_steer_1_10deg2, c_steer_1_10deg_alt, c_steer_1_10deg_alt2; //wstr_f_old;
    extern int16_t steer_1_10deg2, str_timer;         // 2003.03.26 �߰�
    extern int16_t yaw_1_100deg2,a_lat_1_1000g2, a_yaw_deg_ss2;      // 2003.06.04 �߰�
    extern int16_t steer_1_10deg2_old,yaw_1_100deg2_old,a_lat_1_1000g2_old;
    extern int16_t wstr_wr_cnt;
    extern int16_t yaw_wr_cnt;
    extern int16_t alat_wr_cnt;
    extern int16_t wstr_at_360_ok;
    extern uint8_t timer_since_360_ok, counter_360_really;
    extern uint8_t steer_diff_now_to_alt2;
    extern uint8_t comp_delay_timer_for_wstr, suspect_clear_delay_timer;
    extern uint8_t wr_ok_timer;
    extern int16_t f_steer_1_10deg2;

    extern int16_t mpress_avr1, mpress_avr2;
    extern int32_t mpress_sum;
    extern int16_t mpress_offset, mpress_timer, raw_mpress;
    extern uint8_t STEER_straight_cnt2, STEER_vdc_sensor_check_cnt2, STEER_offset_ok_cnt2;
    extern uint8_t STEER_offset_ok_level;
    extern uint8_t SAS_CHECK_OK_on_cnt;
    extern int16_t STEER_avr_model2,STEER_offset_by_SM3_and_SM0;
    extern int16_t STEER_offset_10sum2,STEER_offset_10avr2,STEER_offset_10avr_old2,STEER_avr_measured_by_steer,STEER_offset_10avr2_filter,STEER_offset_10avr3,STEER_offset_10avr2_cnt;
    extern int32_t STEER_sum_measured_by_steer, STEER_sum_model2;

//**********************************************************************

// sensor offset chk
    extern int16_t rf3;
    extern int16_t delta_yaw3,delta_yaw3_old,delta_yaw4,delta_yaw4_old;

    extern int16_t SAS_OFFSET_SUSPECT_cnt;
    extern int16_t SAS_OFFSET_SUSPECT_DEL_YAW_cnt;
    extern int16_t SAS_OFFSET_SUSPECT_cnt2;
    extern int16_t SAS_OFFSET_SUSPECT_DEL_YAW_cnt2;
    extern uint16_t SAS_OFFSET_SUSPECT_DEL_YAW_cnt2_time_over;

////*********************************************************************
//// sensor offset
//    extern int16_t wstr_log_data, yaw_log_data, alat_log_data;
//    extern uint8_t eeprom_counter;
//    extern int16_t steer_address_ee, yaw_address_ee, alat_address_ee;
//    extern uint8_t steer_jump_counter, yaw_jump_counter;
//    extern uint8_t STEER_suspect_counter, YAW_suspect_counter;
////********************************************************************

//*********************************************************************
// sensor offset model 2002.12.31
    extern int16_t yaw1_front_100deg;
    extern int16_t yaw2_rear_100deg;
    extern int16_t steer_offset_360,steer_offset_360_old;
    extern int16_t wheel_angle_100deg;
    extern int16_t steer_model_aver,steer_model_aver_lf,steer_model_aver_lf_old, steer_model_diff, steer_360_chk_timer, steer_360_chk_timer2;
    extern int16_t steer_360_chk_timer6, steer_360_chk_timer7;
    extern int16_t WSTR_360_JUMP_on_cnt;

    extern int16_t int1,int2;
    extern uint16_t uint1, uint2;

    extern uint8_t suspect_chk_cnt, idle_cnt;
    extern int16_t timer, eeprom_reset_check;
    extern uint16_t stable_check_cnt;
    extern uint8_t inhibition_number;

//------------------------------------------------------------------------------
// SENSORs_SUSPECT_YAW_G_GND_OPEN_CHK
//------------------------------------------------------------------------------


    extern int16_t str_suspect_level;

//------------------------------------------------------------------------------
// STEER_360_OFFSET_FAST
//------------------------------------------------------------------------------
    extern int16_t FL_vrad,FR_vrad,RL_vrad,RR_vrad;
    extern int16_t FL_vrad_old,FR_vrad_old,RL_vrad_old,RR_vrad_old;
    extern int16_t lat_model_0,lat_model_0_old,lat_model_1,lat_model_1_old,lat_model_2,lat_model_2_old,lat_model_3,lat_model_3_old;
    extern int16_t stable_str_0_deg_cnt,PHZ_detected_flg_cnt;



//------------------------------------------------------------------------------
// PARTIAL PERFORMANCE                                 diff_wstr_str_m4
//------------------------------------------------------------------------------
   extern int16_t BTCS_ON_on_cnt,BTCS_ON_off_cnt;
   extern int16_t ESP_TCS_ON_on_cnt,ESP_TCS_ON_off_cnt;
   extern int16_t ESP_working_timer, ESP_end_timer;
   extern int16_t ab_rfm_4_partial_str;
   extern int16_t rf_p,rf2_p;
   extern int16_t delta_yaw_org,delta_yaw_org_old,delta_yaw2_org,delta_yaw_first_org,delta_yaw_first2_org;
   extern int16_t delta_yaw2_p,delta_yaw2_p_old,delta_yaw_first2_p,delta_yaw_first2_p_old;
   extern int16_t delta_yaw_p,delta_yaw_first_p;


//------------------------------------------------------------------------------
//      void ESP_MU_ESTIMATION(void)
        extern int16_t  det_alatm2,det_alatm2_old,det_alatm2_cnt;
        extern int16_t  det_alatm3,det_alatm3_old,det_alatm3_cnt;
        extern int16_t T_DATA0;
//        extern int16_t T_DATA1;
//        extern int16_t T_DATA2;
//        extern int16_t T_DATA3;
//        extern int16_t T_DATA4;
//        extern int16_t T_DATA5;
//        extern int16_t T_DATA6;
//        extern int16_t T_DATA7;
        extern int16_t T_DATA8;
        extern int16_t T_DATA9;
//        extern int16_t T_DATA10;
//        extern int16_t T_DATA11;
//        extern int16_t T_DATA12;
//        extern int16_t T_DATA13;
//        extern int16_t T_DATA14;
//        extern int16_t T_DATA15;
//        extern int16_t T_DATA16;
//        extern int16_t T_DATA17;
//        extern int16_t T_DATA18;
//        extern int16_t T_DATA19;
//        extern int16_t T_DATA[10];

//------------------------------------------------------------------------------
//void ABS_PERMIT_CHECK(void)
        extern int16_t MPRESS_BRAKE_ON_on_cnt;
        extern int16_t MPRESS_BRAKE_ON_off_cnt;
        extern int16_t BRAKE_PEDAL_ON_on_cnt;
        extern int16_t BRAKE_PEDAL_ON_off_cnt;
        extern int16_t ABS_PERMIT_BY_BRAKE_BLS_off_cnt;
        extern int16_t ABS_PERMIT_BY_BRAKE_BLS_on_cnt;
        extern int16_t ABS_PERMIT_BY_BRAKE_BLS_cnt;
        extern int16_t ABS_PERMIT_BY_BRAKE_BLS_n_cycle;

//        extern int16_t SLIP_CONTROL_NEED_FR_on_cnt;
//        extern int16_t SLIP_CONTROL_NEED_FR_off_cnt;
//        extern int16_t SLIP_CONTROL_NEED_FL_on_cnt;
//        extern int16_t SLIP_CONTROL_NEED_FL_off_cnt;
//        extern int16_t SLIP_CONTROL_NEED_RR_on_cnt;
//        extern int16_t SLIP_CONTROL_NEED_RR_off_cnt;
//        extern int16_t SLIP_CONTROL_NEED_RL_on_cnt;
//        extern int16_t SLIP_CONTROL_NEED_RL_off_cnt;
//        extern int16_t test_kyk1,test_kyk2;
////        extern int16_t ABS_PERMIT_AND_ABS_cnt;
//
//        extern int16_t MPRESS_BRAKE_ON_delay;
//
//        extern int8_t i_fr_rel_lam,f_fr_rel_lam;
//        extern int8_t i_fl_rel_lam,f_fl_rel_lam;
//        extern int8_t i_rr_rel_lam,f_rr_rel_lam;
//        extern int8_t i_rl_rel_lam,f_rl_rel_lam;


//------------------------------------------------------------------------------//
// Circuit fail
//------------------------------------------------------------------------------//
    extern uint16_t ESP_bls_k_timer, ESP_bs_k_timer;
    extern uint8_t abs_permit_count, ESP_bs_filter_timer;

    extern int16_t EBD_RA_on_cnt;
    extern int16_t EBD_RA_off_cnt;
    extern int16_t circuit_p_slip,circuit_s_slip;
    extern int16_t det_circuit_p_slip,det_circuit_s_slip,det_circuit_p_slip_old,det_circuit_s_slip_old;
    extern int16_t det_rel_lam_fl,det_rel_lam_fr,det_rel_lam_rl,det_rel_lam_rr;
    extern int16_t det_rel_lam_fl_old,det_rel_lam_fr_old,det_rel_lam_rl_old,det_rel_lam_rr_old;

//------------------------------------------------------------------------------//
// MSC 4 ESP
//------------------------------------------------------------------------------//
    extern uint16_t motor_pwm_memory;
    extern int16_t  motor_pwm;






//rf gain calculation
    extern int16_t yc_v1, yc_v2;
    extern int16_t yc_st1, yc_st2;
    extern int16_t yc_g11, yc_g12, yc_g21, yc_g22, yc_g01, yc_g02;
    extern int16_t yc_v_index, yc_st_index;

    extern int16_t  yaw_limit;
    extern int16_t mu_level, mu_level_old, mu_level1, mu_factor;


    extern int16_t   t_det_alatc, t_det_alatc_old, t_det_alatc_diff  ;
    extern int16_t   det_alatc, det_alatc_old, det_alat_dec;
    extern int16_t   det_alatc_unlimit,   det_alatc_unlimit_old;
    extern uint8_t det_alatc_count, dec_alatc_count;

    extern int16_t   t_det_alatm, t_det_alatm_old, t_det_alatm_diff;
    extern int16_t   det_alatm, det_alatm_old, det_alatm_max;
    extern uint8_t det_alatm_count, dec_alatm_count;

    extern int16_t   abs_delta_yaw, det_delta_yaw, det_delta_yaw_old;
    extern int16_t   abs_delta_yaw_limit, det_delta_yaw_limit, det_delta_yaw_limit_old;
    extern int16_t   afz_x1, afz_x1_old, esp_afz;
    extern int16_t        vrefk_b[5];
    extern int16_t   esp_mu, esp_mu_old, g_body, esp_mu_k, esp_mu_k_old;
    extern int16_t   det_g_body_old,det_g_body,alat_4_mu,alat_4_mu_old;
    extern int16_t   afz_4_mu_old,afz_4_mu;
    extern int16_t      det_g_body_4_lf, det_g_body_4_lf_old;
    extern int16_t   ax, ax_old;
    extern int16_t   delta_det_lat;

    extern int16_t   abs_wstr_dot, abs_wstr;
    extern int16_t   lf_wstr, lf_wstr_old;
    extern int16_t   wstr_dot, det_wstr_dot, det_wstr_dot_old;
    extern int16_t   on_center_st_wstr, on_center_st_counter, on_center_gain, on_center_counter;
    extern int16_t   slalom_st_wstr, slalom_st_counter, slalom_counter;
    extern int16_t   det_abs_wstr, det_abs_wstr_old;
    extern int16_t   det_wstr_dot_lf, det_wstr_dot_lf_old;
    extern int16_t   det_wstr_dot_lf2, det_wstr_dot_lf2_old;
    extern int16_t        beta_dot_org[4], beta_dot_org_dc, beta_dot_dot,beta_dot_dot_old;
    extern int16_t  abs_beta_dot,beta_dot, beta_dot_old, beta_dot_f, beta_dot_f_old;
    extern int32_t   beta_dot_dc, beta_dot_dc_old;
    extern int32_t  beta, beta_old;
//    extern int16_t  under_beta, under_beta_old;
//    extern int16_t   ems_gain1, ems_gain2, ems_gain3, ems_gain4;
//    ELONG  counter_beta_inside, counter_straight;
//   extern int8_t abs_first_slop_value, abs_dump_slop_value;
//    extern uint8_t first_delta_yaw_updown_count;
//    extern uint8_t dyaw_thres_over_count;
//    extern uint8_t out_vdc_motor_on_count;
//    extern uint8_t yaw_cdc_motor_on_count;
//    extern int16_t first_reapply_count;

//    extern int16_t   com_m;
//    extern uint16_t  slip_f, slip_r;
//    extern int16_t   vdc_afz, old_vdc_afz;

    extern int16_t  vx_r, vx_r_lf, vx_r_lf_old, alat_lf, alat_lf_old, delta_banked_angle,delta_bank_angle_thres;
    extern int16_t  vx_r_lf2, vx_r_lf2_old;
    extern int16_t  counter_enable_banked, counter_disable_banked, counter_hold_banked;


//    extern uint16_t R_MPA_count, L_MPA_count;

    extern int16_t   vdc_vref;

// ESV CONTROL
    extern int16_t system_act_time;

    extern int16_t wstr_offset, wstr_offset_thr;
    extern int16_t yaw_offset, yaw_offset_old, alat_offset;
    extern int16_t alat_zero_count, init_alat_value, init_alat_value2, sum_alat_zero_diff, sum_alat_zero_diff2;
    extern int16_t PBA_mode, trans_mpress, md_delta_mpress, d_mpress, d_trans_mpress, PBA_hold_timer, PD_wheel, MCP_exit_timer, MCS_exit_timer, PWM_counter_C;
	extern int16_t BA_mode;

// ������������������������������������������������������������������������������������Կ
// Գ---------->>>>>>>>>       PBA                                  <<<<<<<<<----------Գ
// ��������������������������������������������������������������������������������������
    extern uint8_t mpress_slop_counter;
    extern int16_t    first_mpress, mpress_slop, precharge_timer, PBA_on_timer, release_delay, pba_abs_time;
    extern int16_t    yaw_error_dot, yaw_error_dot_old, yaw_error, yaw_measurement, yaw_error_buf[4];
    extern int16_t    delta_yaw_dot;
    extern int16_t    yaw_error2_dot, delta_yaw2_dot,  yaw_error2_buf[4], yaw_error2_dot_old;
    extern int16_t    delta_moment_q, delta_moment_beta , delta_beta2 ;
    extern int16_t   slip_filter_gain_varying;
    extern int16_t    yaw_error_under_dot_old, yaw_error_under_dot, yaw_error_under, yaw_error_under_buf[4];
//    extern int16_t    over_under_value, over_under_gain;
    extern int16_t    over_under_reset_counter;
    extern int16_t        over_under_enter_dot;
    extern int16_t    delta_yaw_first_dot;
//    extern int16_t    gain_ref_delta_yaw, k_ref_delta_yaw;
    extern int16_t    ref_delta_yaw_first_dot;
    extern int16_t    under_enter_Q, under_exit_Q;
    extern int16_t    under_enable_fQ, under_disable_fQ;
//    extern int16_t    under_enter_delta_yaw1, under_enter_delta_yaw2, under_enter_delta_yaw3, enter_delta_yaw;
//    extern int16_t    big_oversteer_value, temp_big_over_value;
    extern int16_t    big_over_enter_Q, big_over_exit_Q;
    extern int16_t    yaw_control_P_gain_front, yaw_control_D_gain_front, yaw_control_P_gain_rear, yaw_control_D_gain_rear;
    extern int16_t        yaw_control_P_gain_front_limit, yaw_control_D_gain_front_limit;
 //      extern int16_t    target_slip_front, target_slip_front_old, target_slip_rear, target_slip_rear_old;
    extern int16_t        enter_Q_front, enter_Q_rear, exit_Q_front, exit_Q_rear;
    extern int16_t    counter_steer_timer, prev_counter_rf2;
    extern int16_t        moment_q_front, moment_q_rear, big_spin_value;
    extern int16_t        moment_q_abs_front, moment_q_abs_rear;
    extern int16_t        moment_q_front_old, moment_q_rear_old;
    extern int16_t        under_moment_q_rear;
    extern int16_t        q_front, q_rear;
    extern int16_t        counter_2wheel_act,     control_wheel_old;
    extern int16_t        counter_2wheel_delay, counter_2wheel_decrease;
//    extern uint8_t  wheel_selection_2wheel,wheel_selection_3wheel;
    extern uint8_t      target_2wheel_rear_hold_count;
    extern int16_t        gain_speed_depend;


    extern U8_BIT_STRUCT_t PBAF0;
    extern U8_BIT_STRUCT_t PBAF1;

    #define PBA_WORK            PBAF0.bit7
    #define ABS_CHK             PBAF0.bit6
    #define PBA_ON              PBAF0.bit5
    #define PBA_LOW_MU          PBAF0.bit4
    #define MPA_SW              PBAF0.bit3
    #define PBA_MPS_ERROR       PBAF0.bit2
    #define PBA_BIG_SLOP        PBAF0.bit1
    #define PBA_ACT             PBAF0.bit0

    #define PBA_EXIT_CONTROL    PBAF1.bit7
    #define PBA_ON_old          PBAF1.bit6
    #define PBA_IN_CDM          PBAF1.bit5
    #define lcpbau1PreABSActinPBA     PBAF1.bit4
    #define PBA_FLAG3           PBAF1.bit3
    #define PBA_FLAG2           PBAF1.bit2
    #define PBA_FLAG1           PBAF1.bit1
    #define PBA_FLAG0           PBAF1.bit0


    //==========================================================
    //  Variables for PBA rise rate control.
    //                          Coded by Kim Jeonghun 2002.7.15
    //----------------------------------------------------------

    extern int16_t    mpress_slop_filt;
    extern uint8_t    PBA_ABS_timer;
    extern uint8_t    PBA_pulsedown;
    extern int16_t    PBA_MPS_exit_timer;

    //=====================================================================================

/*    ELONG tempL0;                                           ELONG tempL1; */
    extern int32_t tempL2;                                           extern int32_t tempL3;
    extern int32_t tempL4;                                           extern int32_t tempL5;
    extern int32_t tempL6;                                           extern int32_t tempL7;
    extern int32_t tempL8;                                           extern int32_t tempL9;
/*    extern uint16_t tempUW4; */                                         extern uint16_t tempUW5;

    //Cycle time ���
    /*extern int16_t esp_tempW0, esp_tempW1, esp_tempW2, esp_tempW3, esp_tempW4;
    extern int16_t esp_tempW5, esp_tempW6, esp_tempW7, esp_tempW8, esp_tempW9;*/
    extern int16_t esp_tempW10,esp_tempW11,esp_tempW12,esp_tempW13,esp_tempW14;
#if(DEL_M_IMPROVEMENT_CONCEPT)    
    extern int16_t esp_tempW21,esp_tempW22,esp_tempW23;
#endif    
    extern uint16_t esp_tempUW0, esp_tempUW1, esp_tempUW2, esp_tempUW3, esp_tempUW4;

    //�ӽú���
    extern int16_t esp_k0, esp_k1, esp_k2, esp_k3, esp_k4;
    extern int16_t esp_k5, esp_k6, esp_k7, esp_k8, esp_k9;
    extern uint16_t max_cycle_time, cur_cycle_time;

extern int16_t high_pass_latm_old, high_pass_latm, low_pass_latm_old, low_pass_latm, det_high_pass_latm_old, det_high_pass_latm, det_low_pass_latm_old, det_low_pass_latm;
extern int16_t latm_old[2];
extern int16_t esp_rough_road_counter,yaw_cdc_work_on_counter;


extern int16_t delta_alat[2], delta_alat_old;

extern int16_t delta_rf2_rf,same_phase_gain;
extern int16_t no_fillter_alat, no_fillter_alat_old;

extern int16_t   gain_wstr1, initial_ab_wstr;
extern int16_t   yaw_limit_gain1,yaw_limit_gain2;
//extern int16_t   K_Jturn_Uturn;

extern int16_t det_beta_dot_old, det_beta_dot, det_beta_dot2;
//extern int16_t current_vrad_fl,current_vrad_fr;
extern int16_t det_beta_dot_error, beta_dot_ems, beta_dot_ems2, beta_dot_ems_old, beta_dot_ems2_old ;

extern int16_t q_front_enter_gain, q_rear_enter_gain, q_front_exit_gain,q_rear_exit_gain;

extern int16_t detect_partial_brake_gain, delta_p_mu_depedant_gain, det_abs_wstr2_old , det_abs_wstr2 ;
extern int16_t det_wstr_dot2, det_wstr_dot2_old,  det_abs_wstr2_old , det_abs_wstr2 ;
extern int16_t lf_wstr2_old, lf_wstr2, abs_wstr_dot2, wstr_dot2, counter_fish_hook, counter_cornering, yawm_wf_cornering_old, yawm_wf_cornering ;
//extern int16_t esp_normal_tire_count;

extern int16_t Initial_Brake_reset_counter;

/*
//------------------------------------------------------------------------------------------
#if __MGH40_Beta_Estimation
//------------------------------------------------------------------------------------------

extern int16_t Ts ;
ELONG Fy_front,Fy_rear;
extern int16_t Beta_Dyn,Beta_Dyn_old;
extern int16_t YawE, YawE_old,Yaw_sensor;
extern int16_t Beta_MD_raw,Beta_MD,Beta_MD_old;
extern int16_t Beta_MD_Dot_raw,Beta_MD_Dot,Beta_MD_Dot_old;
extern int16_t Beta_K_Dot_raw,Beta_K_Dot,Beta_K_Dot_old;
extern int16_t Beta_MD_Front_raw,Beta_MD_Front,Beta_MD_Front_old;
extern int16_t Vy_MD,Vy_MD_old,Vx_MD;
extern int16_t alat_LPF3,alat_LPF3_old,alat_corrected;
extern int16_t Linear_Gain_F,Linear_Gain_F_old;
extern int16_t Alpha_Gain,Linear_Gain;
extern int16_t Alpha_front_Dyn,Alpha_rear_Dyn;
extern int16_t Alpha_front,Alpha_rear;
extern int16_t Beta_Mu,Beta_Mu_old,Beta_Mu_Level;
extern int16_t C_front,C_rear;
extern int16_t Beta_Nolinear_Region_Counter,Yaw_Stable_Counter;
extern int16_t Beta_Mu_Error;
extern int16_t Beta_Mu_Reset_Counter;
extern int16_t Bank_Angle_Dyn_raw,Bank_Angle_Dyn,Bank_Angle_Dyn_old;

extern int16_t Delta_wstr;
extern int16_t Integration_Gain;
extern int16_t Linear_limit_angle;
extern int16_t Nonlinear_limit_angle; // Always Nonlinear_limit_angle>Linear_limit_angle
extern int16_t Del_Yaw_Linear_Upper_Limit,Del_Yaw_Nonlinear_Lower_Limit;
extern int16_t TF_Bank_Yaw,TF_Bank_Ay;
extern int16_t Bank_Angle_Yaw_raw,Bank_Angle_Yaw,Bank_Angle_Yaw_old;
extern int16_t Bank_Angle_Ay_raw,Bank_Angle_Ay,Bank_Angle_Ay_old;
extern int16_t Bank_Angle_K_raw,Bank_Angle_K,Bank_Angle_K_old;
extern int16_t Bank_Angle_K_Dot_raw,Bank_Angle_K_Dot,Bank_Angle_K_Dot_old;
extern int16_t alat_LPF3_bank,alat_LPF3_bank_old;
extern int16_t Dynamic_Factor_raw,Dynamic_Factor,Dynamic_Factor_old,Dynamic_Factor_Total;
extern int16_t Dynamic_Factor_Dot_raw,Dynamic_Factor_Dot,Dynamic_Factor_Dot_old;
extern int16_t Bank_Angle_Final_raw,Bank_Angle_Final,Bank_Angle_Final_old;
extern int16_t Beta_Rate_Offset,Beta_Rate_Offset_old,Beta_Sensor_Offset_counter;
extern int16_t Bank_Detection_Counter;

#endif */

//extern int16_t  alat_fltr_old , alat_fltr , alat_dot, abs_alat_dot ;
//extern int16_t  beta_dot_mul_vel, abs_beta_dot_mul_vel, alat_dot_thres1, alat_dot_thres2 ;
//extern int16_t  rop_control_P_gain_front, rop_control_D_gain_front, rop_control_P_gain_rear, rop_control_D_gain_rear;
//extern int16_t  slip_target_limit_front_rop, slip_target_limit_rear_rop;
extern int16_t  moment_q_front_rop, q_front_rop, moment_q_rear_rop, q_rear_rop , control_wheel_dir_rop, moment_q_front_under, moment_q_rear_under, moment_q_rear_under_old;
extern int16_t     moment_q_front_under, moment_q_rear_under,  moment_q_rear_under_old;
//extern int16_t  slip_target_fr_rop, slip_target_fl_rop, slip_target_rr_rop, slip_target_rl_rop;
//extern int16_t  temp_slip_target_fl , temp_slip_target_fr , temp_slip_target_rl , temp_slip_target_rr ;
//extern int16_t  det_alat_dot_old, det_alat_dot, det_beta_dot_mul_vel_old, det_beta_dot_mul_vel ;
//extern int16_t  alat_dot_thres3, index_weight_1, index_weight_2, rop_index, rf_dyn ;
//extern int16_t    moment_pre_front_rop, alat_dot2_lf, alat_dot2, alat_dot2_lf_old ;

/* ROP Parameter */
extern int16_t roll_angle_old, roll_angle, roll_angle_rate_old, roll_angle_rate, xs1_roll_old, xs1_roll, xs2_roll_old, xs2_roll ;
//extern int16_t    beta_dot_lf_old, beta_dot_lf, vel_beta_dot ;
//extern int16_t  rop_torq  ;
//extern int16_t    index_weight_ay, RI_1_Weight, RI_2_Weight;
extern int16_t    RI_max ;
//extern int16_t    det_RI_max, det_RI_max_old, m_q_ay, m_q_ay_old, ROP_FADE_cnt ;
extern int16_t  abs_delta_alat, alat_dot_org, alat_dot_lf_old, alat_dot_lf, alat_dot_rop ;
//extern int16_t    RI_1, RI_2 ;
extern int8_t TTL_time ;
//extern int16_t abs_alat_rop_lf, abs_alat_rop_lf_old, abs_alat_rop ;

extern int16_t abs_yaw_rate_lf_old, abs_yaw_rate_lf, vrefm_lf_old, vrefm_lf ;
extern int16_t count_curvature, curv_radius, curv_radius_lf_old, curv_radius_lf, curv_radius_wt, wstr_bank_hold;

extern int16_t yawm_dot_lf_old, yawm_dot_lf, alat_lf_bank_max, yaw_bank_max ;

//extern int16_t fs_yaw_offset_change,fs_yaw_offset_change_old;

extern int16_t model_const_for_delta_front;
extern int16_t delta_front_wstr, abs_delta_front_wstr ;

extern int16_t same_phase_enter_ay, same_phase_enter_yaw;
extern int16_t del_p_gain_mu_depend;

extern int16_t Check_Flag_Suspect;
extern int16_t q_rear_2w_old, q_rear_2w, q_front_2w_old;
extern int8_t cnt_rf_dyn;
extern int16_t rf_dyn_temp;

//------------jehong:OFFSET
//extern int16_t CURV_WSTR_POS;
//extern int16_t CURV_WSTR_NEG;
extern int16_t CURV_KYAW_BUF;
extern int8_t KYAW_SIGN;
extern int8_t temp_curv_sign;
extern int16_t curv_pos_count;
extern int16_t curv_neg_count;
extern int16_t MAX_CURV_COUNT;
extern int16_t CURV_COUNT_CHANGE;
extern int8_t temp_curv_sign_old;
extern int16_t DET_CURV_COUNT;
extern int8_t ofst_curv_sign;
extern int16_t reset_pos_turn_count;
extern int16_t reset_neg_turn_count;
extern int16_t yaw_lg_temp,yaw_lg_temp_old,yaw_lg_temp_still;
extern int16_t yaw_lg_temp_cnt,yaw_lg_temp_d,yaw_lg_temp_d_old,yaw_lg_temp_dot,yaw_lg_temp_dot_old,yaw_lg_temp_dot2;
extern int32_t yaw_lg_temp_sum;
extern int16_t yaw_still_arr[6];
extern int8_t yaw_still_arr_cnt,yaw_lg_temp_d_cnt;
extern uint16_t vdc_stable_cnt,vdc_stable_cnt2,vdc_stable_cnt3;
extern int32_t steer_offset_stable_sum,steer_offset_stable_sum2;
extern int16_t steer_offset_stable,steer_offset_stable_old;
extern int16_t yaw_offset_f,steer_offset_f,alat_offset_f,yaw_offset_f2,alat_offset_f2;
extern uint8_t yaw_offset_reliablity,steer_offset_reliablity,alat_offset_reliablity;
extern int16_t yaw_1_10deg_ss,yaw_1_10deg_ss_old;
extern uint16_t yaw_still_sec,yaw_still_sec_cnt;
extern uint8_t yaw_still_cum,yaw_still_rep,yaw_still_tmp;
extern uint8_t vdc_low_sp_cnt;
extern int16_t yaw_still_eep_max,yaw_still_eep_min,yaw_still_eep_max_w,yaw_still_eep_min_w;

extern int16_t vm_yaw_limit_betad_en;
extern int16_t vm_yaw_limit_enter_delta_ay_b;
extern int16_t yaw_limit_enter_delta_ay;

extern int16_t gain_speed_beta_depend, yaw_control_P_gain_beta, delta_beta_y, moment_q_front_new;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_target_fl, slip_target_fr,slip_target_rl, slip_target_rr;
extern int16_t slip_target_limit_front, slip_target_limit_rear;
#else
extern int16_t slip_target_fl, slip_target_fr,slip_target_rl, slip_target_rr, slip_target_limit_front, slip_target_limit_rear;
#endif

extern int8_t   tempM0, tempM1, tempM2, tempM3, tempM4, tempM5, tempM6, tempM7;

extern int16_t det_alatc_fltr, det_alatc_fltr_old, det_alatm_fltr, det_alatm_fltr_old , delta_det_ay_fltr, delta_det_ay_fltr_old ;
extern int16_t yaw_out_dot, yaw_out_dot_old, yaw_out_buf[4];
extern int16_t yaw_out_dot2, yaw_out_dot2_old, yaw_out_buf2[4], Cntr_Yaw_Lmt_Act;
extern int16_t rf_dot2, rf_dot2_old, rf_buf2[4];
extern int16_t yaw_out_dot3, yaw_out_dot3_old;
extern int16_t deltayaw_change_count;
extern int16_t RevSteer_Wgt, RevSteerCnt ;

extern int16_t Index_Mu_delyaw_old, Index_Mu_delyaw, Index_Mu_del_ay_old, Index_Mu_del_ay ;
extern int16_t inc_A_del_yaw, del_ay_TH_delyaw, inc_A_del_ay, Mu_Weight_del_Ay, Mu_Level_Yaw_rate ;
extern int16_t delta_Ay_Lower_limit, delta_Ay_Upper_limit ; 

#if __YMC_DeltaM
//*********YMC**************
extern int16_t delta_m_stability, delta_m_stability_old;
extern int16_t delta_m_steerability, delta_m_steerability_old;
extern int16_t delta_beta_y;
extern int16_t yaw_control_P_gain_beta;
extern int16_t yaw_error_dot_st_old, yaw_error_dot_st,yaw_error_buf_st[3];
extern int16_t yaw_control_P_gain_stability, yaw_control_D_gain_stability;
extern int16_t yaw_control_P_gain_steerability, yaw_control_D_gain_steerability;
#endif
extern int16_t del_target_slip_fl,del_target_slip_fr, del_target_slip_rl, del_target_slip_rr;
extern int16_t force_f_left_n,force_f_right_n, force_r_left_n,force_r_right_n, D1, D2;
//extern int16_t force_ax_1, force_ax_2, force_ax_3, force_ax_4;
//extern int16_t force_ay_1, force_ay_2, force_ay_3, force_ay_4;
//extern int16_t del_target_slip_fl_new, del_target_slip_rl_new,del_target_slip_fr_new, del_target_slip_rr_new, D1_new,D2_new ;
//extern int16_t force_f_left_n_alpha, force_r_left_n_alpha,force_f_right_n_alpha,force_r_right_n_alpha;

//extern int16_t new_alpha_front_old, new_alpha_front, new_alpha_rear_old, new_alpha_rear;
//extern int16_t K_alpha_weight_f, K_alpha_weight_r;
//extern int16_t longitudinal_st_fl, longitudinal_st_fr,longitudinal_st_rl,longitudinal_st_rr;
//extern int16_t lateral_st_fl, lateral_st_fr, lateral_st_rl, lateral_st_rr;
//
//extern int16_t Fx_max_double_fl,Fx_max_double_fr,Fx_max_double_rl,Fx_max_double_rr;
//extern int16_t Fy_max_double_fl,Fy_max_double_fr,Fy_max_double_rl,Fy_max_double_rr;


//extern int16_t sa_index, sm_index, sa1, sa2, sm1, sm2;
//extern int16_t    gx11, gx12, gx21, gx22, gx01, gx02, Fx, Fy, fx_slop, slip_angle, slip_ratio;
//extern int16_t    gy11, gy12, gy21, gy22, gy01, gy02, fy_slop;
//extern int16_t Fx_fl,Fx_fr,Fx_rl,Fx_rr;
//extern int16_t Fy_fl,Fy_fr,Fy_rl,Fy_rr;
//extern int16_t Fx_fl_old,Fx_fr_old,Fx_rl_old,Fx_rr_old;
//extern int16_t Fy_fl_old,Fy_fr_old,Fy_rl_old,Fy_rr_old;
//extern int16_t fx_slop_fl,fx_slop_fr,fx_slop_rl,fx_slop_rr;
//extern int16_t fy_slop_fl,fy_slop_fr,fy_slop_rl,fy_slop_rr;
//
//extern int16_t fx_slop_fl_org,fx_slop_fr_org,fx_slop_rl_org,fx_slop_rr_org;

//extern int16_t esp_tsp_press_count_fl, esp_tsp_press_count_fr, esp_tsp_press_count_rl,esp_tsp_press_count_rr;
//extern int16_t esp_btcs_press_count_fl, esp_btcs_press_count_fr, esp_btcs_press_count_rl,esp_btcs_press_count_rr;
#if MGH60_NEW_TARGET_SLIP == 0
extern int16_t target_pressure_fl,target_pressure_fr,target_pressure_rl,target_pressure_rr;
extern int16_t del_target_pressure_fl,del_target_pressure_fr,del_target_pressure_rl,del_target_pressure_rr;

extern int16_t P1,P2;
extern int16_t D1_first, D2_first;
extern int16_t tire_force_fx_fl,tire_force_fx_fr,tire_force_fx_rl,tire_force_fx_rr;
#endif
extern int8_t pd_ymc_gain;


extern int16_t    HIGH_MU_EMS, LOW_MU_EMS;
/*              [[[SPEED SETTING]]] */
/*      [[[SPEED SETTING]]]     [[[[LOW MU]]]] */
extern int16_t    LOW_MU_ESP_TORQ_CON_L_SPEED, LOW_MU_ESP_TORQ_CON_M_SPEED, LOW_MU_ESP_TORQ_CON_H_SPEED;
/*      [[[SPEED SETTING]]]     [[[[MED MU]]]] */
extern int16_t    MED_MU_ESP_TORQ_CON_L_SPEED, MED_MU_ESP_TORQ_CON_M_SPEED, MED_MU_ESP_TORQ_CON_H_SPEED;
/*      [[[SPEED SETTING]]]     [[[[HIGH MU]]]] */
extern int16_t    HIGH_MU_ESP_TORQ_CON_L_SPEED, HIGH_MU_ESP_TORQ_CON_M_SPEED, HIGH_MU_ESP_TORQ_CON_H_SPEED;

 

#endif
