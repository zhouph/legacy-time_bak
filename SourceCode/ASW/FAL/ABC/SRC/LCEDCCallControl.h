#ifndef LCEDCCALLCONTROL_H
#define LCEDCCALLCONTROL_H

/*includes**************************************************/
#include "LVarHead.h"

  #if __EDC
/*Global MACRO CONSTANT Definition******************************************************/

#if (__CAR==GM_GSUV) 
#define __EDC_ENABLE_BY_CALIBRATION		ENABLE
#define __EDC_MAX_TIME_LIMIT		    ENABLE
#else
#define __EDC_ENABLE_BY_CALIBRATION		DISABLE
#define __EDC_MAX_TIME_LIMIT		    ENABLE
#endif

  #if !__4WD || (__4WD_VARIANT_CODE==ENABLE)
#define __EDC_EXIT_BY_EXCESSIVE_TORQ_UP	DISABLE
  #else
#define __EDC_EXIT_BY_EXCESSIVE_TORQ_UP	DISABLE
  #endif


/*Global MACRO FUNCTION Definition******************************************************/


/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/


/*Global Extern Functions  Declaration******************************************************/
extern void LCEDC_vCallControl(void);

  #endif /* __EDC */
#endif