/******************************************************************************
* Project Name: SPAS Brake intervention interface
* File: LCSPASCallControl.C
* Description: SPAS Brake intervention Controller
* Date: June. 17. 2011
******************************************************************************/

/* Includes ******************************************************************/
#include "LCSPASCallControl.h"
#include "Hardware_Control.h"
#include "hm_logic_var.h"

/******************************************************************************/

/* Local Definition  *********************************************************/


/* Variables Definition*******************************************************/
  #if __SPAS_INTERFACE


int8_t     lcs8SpasState;

uint8_t    lcu8SpasCtrlReq;
uint8_t    lcu8SpasCompleteStopCnt;

int16_t      lcs16SpasCtrlError;
int16_t      lcs16SpasReqTargetP;

uint16_t     lcu16SpasHoldActCnt, lcu16SpasReleaseActCnt, lcu16SpasRiseActCnt, lcu16SpasCtrlErrorCnt;

struct  	U8_BIT_STRUCT_t SPAS00;
struct  	U8_BIT_STRUCT_t SPAS01;

  #endif

/* Local Function prototype **************************************************/
  #if __SPAS_INTERFACE
static void LCSPAS_vChkInhibition(void);
static void LCSPAS_vChkSpasRequest(void);
static void LCSPAS_vChkVehicleSpeed(void);
static void LCSPAS_vChkEntCondition(void);
static void LCSPAS_vChkExitCondition(void);
static void LCSPAS_vRiseControl(void);
static void LCSPAS_vHoldControl(void);
static void LCSPAS_vReleaseControl(void);
static void LCSPAS_vChkFeedFowardControl(void);
static void LCSPAS_vChkFeedBackControl(void);
static void	LCSPAS_vCheckSpasState(void);
static void LCSPAS_vChkSpasCanTx(void);
void LCSPAS_vResetCtrlVariable(void);
  #endif
/* Implementation*************************************************************/
  #if __SPAS_INTERFACE
void LCSPAS_vCallControl(void)
{
    LCSPAS_vChkInhibition();
    LCSPAS_vCheckSpasState();
    LCSPAS_vChkSpasRequest();
    
    switch(lcs8SpasState)
    {
        case S8_SPAS_STANDBY:
            lcu1SpasActiveFlg = 0;
	          if((lcu1SpasInhibitFlg==1)||(lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_INHIBIT))
            {
                lcs8SpasState = S8_SPAS_INHIBITION;
                LCSPAS_vResetCtrlVariable();
            }
	          else if(lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_ON)
            {
                LCSPAS_vChkEntCondition();
            	if(lcu1SpasEntOk==1)
            	{
            	    lcu1SpasEntOk = 0;
                    lcu1SpasActiveFlg = 1;
                    lcs8SpasState = S8_SPAS_RISE;
                    LCSPAS_vChkVehicleSpeed();
                    LCSPAS_vRiseControl();
                }
                else
                {
                    lcs8SpasState = S8_SPAS_STANDBY;
                }
            }
            else
            {
                lcs8SpasState = S8_SPAS_STANDBY;	  
            }
            break;

        case S8_SPAS_RISE:
            lcu1SpasActiveFlg = 1;
            LCSPAS_vChkVehicleSpeed();
            LCSPAS_vChkExitCondition();
	          if((lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_READY)||(lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_INHIBIT)
	          	||(lcu1SpasExitOkFlg==1)||(lcu1SpasInhibitFlg==1)
	          )
            {
                lcs8SpasState = S8_SPAS_RELEASE;
                LCSPAS_vReleaseControl();
            }
            else if(lcu1SpasRiseToHold==1)
            {
                lcs8SpasState = S8_SPAS_HOLD;
                LCSPAS_vHoldControl();
            }
            else
            {
                lcs8SpasState = S8_SPAS_RISE;
                LCSPAS_vRiseControl();
            }
            break;

        case S8_SPAS_HOLD:
            lcu1SpasActiveFlg = 1;
            LCSPAS_vChkVehicleSpeed();
            LCSPAS_vChkExitCondition();
	          if((lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_READY)||(lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_INHIBIT)
	          	||(lcu1SpasExitOkFlg==1)||(lcu1SpasInhibitFlg==1)
	          )
            {
                lcs8SpasState = S8_SPAS_RELEASE;
                LCSPAS_vReleaseControl();
            }
            else if(lcu1SpasHoldToRise==1)
            {
                lcs8SpasState = S8_SPAS_RISE;
                LCSPAS_vRiseControl();
            }
            else
            {
                lcs8SpasState = S8_SPAS_HOLD;
                LCSPAS_vHoldControl();
            }  
            break;

        case S8_SPAS_RELEASE:
            lcu1SpasActiveFlg = 1;
            LCSPAS_vChkVehicleSpeed();
	          if(((MFC_Current_SPAS_P<200)||(MFC_Current_SPAS_S<200)) || (lcu8SpasCtrlReq==S8_SPAS_BRK_REQUEST_ON))
            {
                lcs8SpasState = S8_SPAS_STANDBY;
                LCSPAS_vResetCtrlVariable();
            }
            else
            {
                lcs8SpasState = S8_SPAS_RELEASE; 
                LCSPAS_vReleaseControl();
            }
            break;

        case S8_SPAS_INHIBITION:
            lcu1SpasActiveFlg = 0;
            if(lcu1SpasInhibitFlg==0)
            {
                lcs8SpasState=S8_SPAS_STANDBY;
                LCSPAS_vResetCtrlVariable();
            }
            else
            {
                lcs8SpasState=S8_SPAS_INHIBITION;
            }
            break;

        default:
            lcu1SpasActiveFlg=0;
            lcs8SpasState=S8_SPAS_STANDBY;
            break;
    }
    
    LCSPAS_vChkSpasCanTx();
}

static void LCSPAS_vChkInhibition(void)
{
	if((abs_error_flg==1)||(mp_hw_suspcs_flg==1)||(can_error_flg==1)||(vdc_error_flg==1)||(flu1BrakeLampErrDet==1))
    {
        lcu1SpasInhibitFlg=1;
    }
    else
    {
        lcu1SpasInhibitFlg=0;
    }
}

static void LCSPAS_vChkSpasRequest(void)
{
    lcu8SpasCtrlReq = lcu8SpasBrkIntReq;
    lcs16SpasReqTargetP = (lcu8SpasBrkIntCmd*10) + MPRESS_60BAR;

  #if __SPAS_TEST_MODE==ENABLE
    if(fu1ESCDisabledBySW==1)
    {
        lcu8SpasCtrlReq = S8_SPAS_BRK_REQUEST_ON;
    }
    else
    {
    	  lcu8SpasCtrlReq = S8_SPAS_BRK_REQUEST_READY;
    }
    lcs16SpasReqTargetP = SPAS_TEST_MODE_TARGET_P;
  #endif
}

static void LCSPAS_vChkVehicleSpeed(void)
{
    lcs16SpasCtrlError = vref;
        
    if((vrad_fl<=1)&&(vrad_fr<=1))
    {
        lcu16SpasCtrlErrorCnt = 0;
    }
    else
    {
        lcu16SpasCtrlErrorCnt = lcu16SpasCtrlErrorCnt + 1;	
    }
    
}


static void LCSPAS_vChkEntCondition(void)
{
    lcu1SpasEntOk = 1;

  #if __SPAS_TEST_MODE==ENABLE
    if((ABS_fz==0) && (vref<=VREF_7_KPH) && (vref>=VREF_1_5_KPH))
    {
    	  lcu1SpasEntOk = 1;
    }
    else 
    {
        lcu1SpasEntOk = 0;
    }
  #endif
}

static void LCSPAS_vChkExitCondition(void)
{
	  if((lcu16SpasHoldActCnt+lcu16SpasRiseActCnt)>S16_SPAS_BRK_CTRL_MAX_LIMIT)
    {
    	lcu1SpasExitOkFlg = 1;    	
	  }
    else { }
		
  #if __SPAS_TEST_MODE==ENABLE
    if((mpress>=MPRESS_20BAR) || (mtp>=6) || (ABS_fz==1)||((lcu16SpasHoldActCnt+lcu16SpasRiseActCnt)>S16_SPAS_BRK_CTRL_MAX_LIMIT))
    {
    	lcu1SpasExitOkFlg = 1;
    }
    else { }
  #endif
}

static void LCSPAS_vRiseControl(void)
{
	lcu16SpasHoldActCnt = 0;
	lcu16SpasReleaseActCnt = 0;
	
	if(lcu16SpasRiseActCnt<T_20_S) {lcu16SpasRiseActCnt = lcu16SpasRiseActCnt + 1;}
	else                           {lcu16SpasRiseActCnt = L_U16_TIME_10MSLOOP_20S;}

	if((lcu16SpasRiseActCnt<S16_SPAS_FF_CTRL_TIME)||((vref>S16_SPAS_FF_CTRL_MIN_SPD)&&(lcu16SpasRiseActCnt<S16_SPAS_FF_CTRL_MAX_TIME)))
	{
		lcu1SpasFBControl = 0;
		LCSPAS_vChkFeedFowardControl();
	}
	else
	{
		lcu1SpasFFControl = 0;
		LCSPAS_vChkFeedBackControl();
	}
	
	if((vrad_fl<=VREF_1_KPH)&&(vrad_fr<=VREF_1_KPH))
	{
		if(lcu8SpasCompleteStopCnt<L_U8_TIME_10MSLOOP_1500MS)
		{
		    lcu8SpasCompleteStopCnt = lcu8SpasCompleteStopCnt + 1;
		}
		else {lcu8SpasCompleteStopCnt = L_U8_TIME_10MSLOOP_1500MS;}
	}
	else if(lcu8SpasCompleteStopCnt>0)
	{
		lcu8SpasCompleteStopCnt = lcu8SpasCompleteStopCnt - 1;
	}
	else
	{
		lcu8SpasCompleteStopCnt = 0;
	}

    if(lcu8SpasCompleteStopCnt>U8_SPAS_RISE_TO_HOLD_DELAY_TIME)
    {
        lcu1SpasRiseToHold = 1;
    }
    else
    {
    	lcu1SpasRiseToHold = 0;
    }

    SPAS_TCL_DEMAND_fl=1;
    SPAS_TCL_DEMAND_fr=1;
    SPAS_S_VALVE_LEFT=1;
    SPAS_S_VALVE_RIGHT=1;
    SPAS_MOTOR_ON=1;
}

static void LCSPAS_vHoldControl(void)
{
    lcu1SpasFFControl = 0;
    lcu1SpasFBControl = 0;
    lcu1SpasRiseToHold = 0;
    lcu8SpasCompleteStopCnt = 0;
    lcu16SpasRiseActCnt = 0;
    lcu16SpasReleaseActCnt = 0;
    lcu16SpasHoldActCnt = lcu16SpasHoldActCnt + 1;
	
    SPAS_TCL_DEMAND_fl=1;
    SPAS_TCL_DEMAND_fr=1;
    SPAS_S_VALVE_LEFT=0;
    SPAS_S_VALVE_RIGHT=0;
    SPAS_MOTOR_ON=0;
}

static void LCSPAS_vReleaseControl(void)
{
    lcu1SpasFFControl = 0;
    lcu1SpasFBControl = 0;
    lcu16SpasRiseActCnt = 0;
    lcu16SpasHoldActCnt = 0;
    lcu16SpasReleaseActCnt = lcu16SpasReleaseActCnt + 1;
    lcs16SpasCtrlError = 0;
	
    SPAS_TCL_DEMAND_fl=1;
    SPAS_TCL_DEMAND_fr=1;
    SPAS_S_VALVE_LEFT=0;
    SPAS_S_VALVE_RIGHT=0;
    SPAS_MOTOR_ON=0;	
}

void LCSPAS_vResetCtrlVariable(void)
{
    lcu1SpasEntOk = 0;
    lcu1SpasExitOkFlg = 0;
    lcu16SpasRiseActCnt = 0;
    lcu16SpasReleaseActCnt = 0;
    lcu16SpasHoldActCnt = 0;
    lcu16SpasCtrlErrorCnt = 0;
    lcu1SpasActiveFlg = 0;
    lcs16SpasCtrlError = 0;
    lcu8SpasCompleteStopCnt = 0;
    lcu1SpasFFControl = 0;
    lcu1SpasFBControl = 0;       

    SPAS_TCL_DEMAND_fl = 0;
    SPAS_TCL_DEMAND_fr = 0;
    SPAS_S_VALVE_LEFT = 0;
    SPAS_S_VALVE_RIGHT = 0;
    SPAS_MOTOR_ON = 0;    
}

static void	LCSPAS_vCheckSpasState(void)
{
    /* Check SPAS Control state */
}

static void	LCSPAS_vChkFeedFowardControl(void)
{
    lcu1SpasFFControl = 1;
}

static void	LCSPAS_vChkFeedBackControl(void)
{
    lcu1SpasFBControl = 1;
}

static void	LCSPAS_vChkSpasCanTx(void)
{
	/* Tx */
  
	/* spas control state */
	  if((lcu1SpasInhibitFlg==1))
   	{
   	    lcu8SpasBrkIntRpl=1;	
   	}
   	else if(lcs8SpasState==S8_SPAS_STANDBY)
   	{
   	    lcu8SpasBrkIntRpl=0;	
   	}
   	else if (lcu1SpasActiveFlg==1)
   	{
   		  if(lcs8SpasState==S8_SPAS_RELEASE)
   			{
   			    lcu8SpasBrkIntRpl=0;
   			}
   		  else
   			{
   		      lcu8SpasBrkIntRpl=2;   			
   			}
   	}
	  else { }
	  	
	/* spas control press */
	
	  lcu8SpasBrkIntPtn = (lcs16SpasReqTargetP-MPRESS_60BAR)/10;
	  
  #if __SPAS_TEST_MODE==ENABLE
    lcu8SpasBrkIntPtn = SPAS_TEST_MODE_TARGET_P/100;
  #endif
	  	
}

#endif  /* __SPAS_INTERFACE */
