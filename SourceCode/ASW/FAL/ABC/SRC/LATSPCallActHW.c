
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LATSPCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LATSPCallActHW.h"
#include "LACallMain.h"
#include "LSESPCalSlipRatio.h"
#include "LCDECCallControl.h"
#include "LDTSPCallDetection.h"
#include "Hardware_Control.h"
#if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
#endif

#if __TSP
void LATSP_vCallActHW(void);
void LATSP_vCallActPrefillBrakeValve(void);
void LATSP_vCallActDecelBrakeValve(void);
void LATSP_vCallActDiffBrakeValve(void);
#if (__MGH_80_10MS == ENABLE)
void LATSP_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *latspHP1, WHEEL_VV_OUTPUT_t *latspHP2);
#endif
int16_t LAMFC_s16SetMFCCurrentTSP(uint8_t CIRCUIT, int16_t MFC_Current_temp);
#if __ADVANCED_MSC
void LCMSC_vSetTSPTargetVoltage(void);
#endif
#endif


#if __TSP
void LATSP_vCallActHW(void)
{
	if((TSP_ON==1)&&(TSP_BRAKE_CTRL_ON==1))
	{
		LATSP_vCallActDecelBrakeValve();
		LATSP_vCallActDiffBrakeValve();
	}
	else if((TSP_ON==1)&&(U1_TSP_BRAKE_CTRL_ON_READY==1)&&(U1_TSP_DIFF_To_DEC_ON==0)&&(YAW_CDC_WORK==0))
	{
		LATSP_vCallActPrefillBrakeValve();
	}
	else
	{
		;
	}
}

void LATSP_vCallActPrefillBrakeValve(void)
{
	#if !__SPLIT_TYPE
	
		TCL_DEMAND_fl=0;
  	TCL_DEMAND_fr=0;
  	S_VALVE_LEFT=1;
  	S_VALVE_RIGHT=1;
  	
  	HV_VL_fl=0;
  	AV_VL_fl=1;
  	HV_VL_fr=0;
  	AV_VL_fr=1;
  	HV_VL_rl=0;
  	AV_VL_rl=1;
  	HV_VL_rr=0;
  	AV_VL_rr=1;
  	
 		#if (__MGH_80_10MS == ENABLE)
 		   LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
  	   la_FL1HP = latsp_FL1HP; 
  	   la_FL2HP = latsp_FL2HP;  				
  	   LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
  	   la_FR1HP = latsp_FR1HP; 
 		   la_FR2HP = latsp_FR2HP; 
  	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
  	   la_RL1HP = latsp_RL1HP; 
  	   la_RL2HP = latsp_RL2HP;  				
  	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
  	   la_RR1HP = latsp_RR1HP; 
 		   la_RR2HP = latsp_RR2HP; 
  	#endif
  	
  #else
  
		TCL_DEMAND_PRIMARY=0;
  	TCL_DEMAND_SECONDARY=0;
  	S_VALVE_PRIMARY=1;
  	S_VALVE_SECONDARY=1;
  	
  	HV_VL_fl=0;
  	AV_VL_fl=1;
  	HV_VL_fr=0;
  	AV_VL_fr=1;
  	HV_VL_rl=0;
  	AV_VL_rl=1;
  	HV_VL_rr=0;
  	AV_VL_rr=1;
  	
 		#if (__MGH_80_10MS == ENABLE)
 		   LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
  	   la_FL1HP = latsp_FL1HP; 
  	   la_FL2HP = latsp_FL2HP;  				
  	   LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
  	   la_FR1HP = latsp_FR1HP; 
 		   la_FR2HP = latsp_FR2HP; 
  	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
  	   la_RL1HP = latsp_RL1HP; 
  	   la_RL2HP = latsp_RL2HP;  				
  	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
  	   la_RR1HP = latsp_RR1HP; 
 		   la_RR2HP = latsp_RR2HP; 
  	#endif
  #endif	
  	
}

void LATSP_vCallActDecelBrakeValve(void)
{
	if(TSP_DIFF_ONLY_CTRL_ACT==0)
	{
	#if !__SPLIT_TYPE
		if(U1_TSP_FIRST_DIFF_CTRL_DETECT==1)
		{
			lau16tspFirstDiffEnterTime = LCESP_s16IInter3Point(lcs16TspTargetDecel, S16_TSP_FIRST_DIFF_ENTER_LOW_DECEL, S16_TSP_FIRST_DIFF_ENTER_TIME_LOW,
																																							S16_TSP_FIRST_DIFF_ENTER_MED_DECEL, S16_TSP_FIRST_DIFF_ENTER_TIME_MED,
																																					 		S16_TSP_FIRST_DIFF_ENTER_HIGH_DECEL, S16_TSP_FIRST_DIFF_ENTER_TIME_HIGH);
			
			if( (U1_TSP_ADD_TC_CURRENT_S==1)&&(tsp_brake_work_count>=(lau16tspFirstDiffEnterTime)) )	
			{
				HV_VL_rl = 0;
				AV_VL_rl = 0;						
				HV_VL_rr = 1;
				AV_VL_rr = 0;
 				#if (__MGH_80_10MS == ENABLE)
    	     LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	     la_RL1HP = latsp_RL1HP; 
    	     la_RL2HP = latsp_RL2HP;  				
    	     LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	     la_RR1HP = latsp_RR1HP; 
 	  	     la_RR2HP = latsp_RR2HP; 
    	  #endif
    	  
    	  U1_TSP_FIRST_DIFF_CTRL_DETECT = 0;
    	} 
    	else if( (U1_TSP_ADD_TC_CURRENT_P==1)&&(tsp_brake_work_count>=(lau16tspFirstDiffEnterTime)) )
			{
				HV_VL_rl = 1;
				AV_VL_rl = 0;						
				HV_VL_rr = 0;
				AV_VL_rr = 0;
 				#if (__MGH_80_10MS == ENABLE)
    	     LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	     la_RL1HP = latsp_RL1HP; 
    	     la_RL2HP = latsp_RL2HP;  				
    	     LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	     la_RR1HP = latsp_RR1HP; 
 	  	     la_RR2HP = latsp_RR2HP; 
    	  #endif
    	  
    	  U1_TSP_FIRST_DIFF_CTRL_DETECT = 0;
    	}
    	else
			{
				;
			}
		}
		else
		{
			if(U1_TSP_ADD_TC_CURRENT_S==1)	
			{
				HV_VL_rl = 0;
				AV_VL_rl = 0;						
				HV_VL_rr = 1;
				AV_VL_rr = 0;
 				#if (__MGH_80_10MS == ENABLE)
    	     LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	     la_RL1HP = latsp_RL1HP; 
    	     la_RL2HP = latsp_RL2HP;  				
    	     LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	     la_RR1HP = latsp_RR1HP; 
 	  	     la_RR2HP = latsp_RR2HP; 
    	  #endif
    	} 
    	else if(U1_TSP_ADD_TC_CURRENT_P==1)
			{
				HV_VL_rl = 1;
				AV_VL_rl = 0;						
				HV_VL_rr = 0;
				AV_VL_rr = 0;
 				#if (__MGH_80_10MS == ENABLE)
    	     LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	     la_RL1HP = latsp_RL1HP; 
    	     la_RL2HP = latsp_RL2HP;  				
    	     LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	     la_RR1HP = latsp_RR1HP; 
 	  	     la_RR2HP = latsp_RR2HP; 
    	  #endif
    	}
    	else
			{
				;
			}	
		}
	#else
		
		if(U1_TSP_ADD_TC_CURRENT_S==1)	
		{
			HV_VL_fl = 0;
			AV_VL_fl = 0;
			HV_VL_fr = 1;
			AV_VL_fr = 0;
			HV_VL_rl = 0;
			AV_VL_rl = 0;
			HV_VL_rr = 0;
			AV_VL_rr = 0;
 			#if (__MGH_80_10MS == ENABLE)
 				 LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
    		 la_FL1HP = latsp_FL1HP; 
    		 la_FL2HP = latsp_FL2HP;
 				 LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
    		 la_FR1HP = latsp_FR1HP; 
    		 la_FR2HP = latsp_FR2HP; 
    	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	   la_RL1HP = latsp_RL1HP; 
    	   la_RL2HP = latsp_RL2HP;  				
    	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	   la_RR1HP = latsp_RR1HP; 
 	  	   la_RR2HP = latsp_RR2HP; 
    	#endif
    } 
    else if(U1_TSP_ADD_TC_CURRENT_P==1)
		{
			HV_VL_fl = 1;
			AV_VL_fl = 0;
			HV_VL_fr = 0;
			AV_VL_fr = 0;
			HV_VL_rl = 0;
			AV_VL_rl = 0;
			HV_VL_rr = 0;
			AV_VL_rr = 0;
 			#if (__MGH_80_10MS == ENABLE)
 				 LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
    		 la_FL1HP = latsp_FL1HP; 
    		 la_FL2HP = latsp_FL2HP;
 				 LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
    		 la_FR1HP = latsp_FR1HP; 
    		 la_FR2HP = latsp_FR2HP; 
    	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	   la_RL1HP = latsp_RL1HP; 
    	   la_RL2HP = latsp_RL2HP;  				
    	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	   la_RR1HP = latsp_RR1HP; 
 	  	   la_RR2HP = latsp_RR2HP; 
    	#endif
    }
    else
		{
			;
		}	
		
	#endif		
	}
	else
	{
		;
	}
}
					
void LATSP_vCallActDiffBrakeValve(void)
{
	if(TSP_DIFF_ONLY_CTRL_ACT==1)
	{
	#if !__SPLIT_TYPE	
		if(U1_TSP_ADD_TC_CURRENT_S==1)
		{
			S_VALVE_LEFT = 1;
			S_VALVE_RIGHT = 0;
			TCL_DEMAND_fl = 1;
			TCL_DEMAND_fr = 0;
			HV_VL_rl = 0;
			AV_VL_rl = 0;									
			HV_VL_rr = 1;
			AV_VL_rr = 1;
 			#if (__MGH_80_10MS == ENABLE)
    	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	   la_RL1HP = latsp_RL1HP; 
    	   la_RL2HP = latsp_RL2HP;  				
    	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	   la_RR1HP = latsp_RR1HP; 
 	  	   la_RR2HP = latsp_RR2HP; 
    	#endif
		}
		else if(U1_TSP_ADD_TC_CURRENT_P==1) 
		{
			S_VALVE_LEFT = 0;
			S_VALVE_RIGHT = 1;
			TCL_DEMAND_fl = 0;
			TCL_DEMAND_fr = 1;
			HV_VL_rl = 1;
			AV_VL_rl = 1;
			HV_VL_rr = 0;
			AV_VL_rr = 0;			
 			#if (__MGH_80_10MS == ENABLE)
    	   LATSP_vGenerateLFCDutyWL(&RL, &latsp_RL1HP, &latsp_RL2HP);
    	   la_RL1HP = latsp_RL1HP; 
    	   la_RL2HP = latsp_RL2HP;  				
    	   LATSP_vGenerateLFCDutyWL(&RR, &latsp_RR1HP, &latsp_RR2HP);
    	   la_RR1HP = latsp_RR1HP; 
 	  	   la_RR2HP = latsp_RR2HP; 
    	#endif
		}
		else
		{
			;
		}
	#else
		if(U1_TSP_ADD_TC_CURRENT_S==1)
		{
			S_VALVE_PRIMARY = 1;
			S_VALVE_SECONDARY = 0;
			TCL_DEMAND_PRIMARY = 1;
			TCL_DEMAND_SECONDARY = 0;
			HV_VL_fl = 0;
			AV_VL_fl = 0;
			HV_VL_fr = 1;
			AV_VL_fr = 1;
 			#if (__MGH_80_10MS == ENABLE)
 				 LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
      	 la_FL1HP = latsp_FL1HP; 
      	 la_FL2HP = latsp_FL2HP;
 				 LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
      	 la_FR1HP = latsp_FR1HP; 
      	 la_FR2HP = latsp_FR2HP;
      #endif
		}
		else if(U1_TSP_ADD_TC_CURRENT_P==1) 
		{
			S_VALVE_PRIMARY = 1;
			S_VALVE_SECONDARY = 0;
			TCL_DEMAND_PRIMARY = 1;
			TCL_DEMAND_SECONDARY = 0;
			HV_VL_fl = 1;
			AV_VL_fl = 1;
			HV_VL_fr = 0;
			AV_VL_fr = 0;
 			#if (__MGH_80_10MS == ENABLE)
 				 LATSP_vGenerateLFCDutyWL(&FL, &latsp_FL1HP, &latsp_FL2HP);
      	 la_FL1HP = latsp_FL1HP; 
      	 la_FL2HP = latsp_FL2HP;
 				 LATSP_vGenerateLFCDutyWL(&FR, &latsp_FR1HP, &latsp_FR2HP);
      	 la_FR1HP = latsp_FR1HP; 
      	 la_FR2HP = latsp_FR2HP;
      #endif
		}
		else
		{
			;
		}	
	#endif	
	}
	else
	{
		;
	}

}

#if __ADVANCED_MSC
void LCMSC_vSetTSPTargetVoltage(void)
{
	if((TSP_ON==1)&&(U1_TSP_BRAKE_CTRL_ON_READY==1)&&(YAW_CDC_WORK==0))
	{
		TSP_MSC_MOTOR_ON = 1;
		
		if(BRAKE_PEDAL_ON==0)
    {
		   tsp_msc_target_vol = S16_TSP_TARGET_VOLT_PREFILL;
    }
    else
    {
		   tsp_msc_target_vol = S16_TSP_TARGET_VOLT_BRK;
    }
  }	
  else if((TSP_ON==1)&&(TSP_BRAKE_CTRL_ON==1))
  {
    if(TSP_DIFF_ONLY_CTRL_ACT==1)
		{
			TSP_MSC_MOTOR_ON = 1;
			
      if(BRAKE_PEDAL_ON==0)
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_DIFFONLY;
      }
      else
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_BRK;
      }
    }
		else if((U1_TSP_DEC_DIFF_CTRL_ACT==0)&&(U1_TSP_FADE_CTRL_ON==0)&&(U1_TSP_FADE_CTRL_MONITOR==0)&&(U1_TSP_DIFF_To_DEC_ON==0))
		{
			TSP_MSC_MOTOR_ON = 1;
			
      if(BRAKE_PEDAL_ON==0)
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_INI;
      }
      else
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_BRK;
      }
    }
    else if(U1_TSP_DEC_DIFF_CTRL_ACT==1)
    {
			TSP_MSC_MOTOR_ON = 1;
			
      if(BRAKE_PEDAL_ON==0)
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_CONTROL;
      }
      else
      {
		     tsp_msc_target_vol = S16_TSP_TARGET_VOLT_BRK;
      }
    }    
    else
    {
	  	TSP_MSC_MOTOR_ON = 0;
  		tsp_msc_target_vol = 0;
    }
  }
  else
  {
 	  TSP_MSC_MOTOR_ON = 0;
  	tsp_msc_target_vol = 0;
  }
}
#endif

#if (__MGH_80_10MS == ENABLE)
void LATSP_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *latspHP1, WHEEL_VV_OUTPUT_t *latspHP2)
{
  WL=WL_temp;
  
  if(HV_VL_wl==1)
  {
    latspHP1->u8InletOnTime = 0;            
    latspHP2->u8InletOnTime = 0;            
    
    if (AV_VL_wl==1)
    {
        latspHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
        latspHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
        
        latspHP1->u8PwmDuty     = PWM_duty_DUMP;
    		latspHP2->u8PwmDuty     = PWM_duty_DUMP;
    }
    else
    {
        latspHP1->u8PwmDuty     = PWM_duty_HOLD;
    		latspHP2->u8PwmDuty     = PWM_duty_HOLD;
    }
  }
  else
  {
    latspHP1->u8InletOnTime = U8_BASE_CTRLTIME;
    latspHP2->u8InletOnTime = U8_BASE_CTRLTIME;
    latspHP1->u8PwmDuty = pwm_duty_null;
    latspHP2->u8PwmDuty = pwm_duty_null;
    latspHP1->u8OutletOnTime = 0;
    latspHP2->u8OutletOnTime = 0;
  }
}
#endif

int16_t LAMFC_s16SetMFCCurrentTSP(UCHAR CIRCUIT, INT MFC_Current_temp)
{
	int16_t MFC_Current_new;

	if((TSP_ON==1)&&(TSP_BRAKE_CTRL_ON==1))
	{
	#if !__SPLIT_TYPE
		if(TSP_DIFF_ONLY_CTRL_ACT == 1)
		{
			if((U1_TSP_ADD_TC_CURRENT_S==1)&&(CIRCUIT==0))
			{
				MFC_Current_new = lcs16TspDiffCtrlAddCurS;
			}
			else if((U1_TSP_ADD_TC_CURRENT_P==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = lcs16TspDiffCtrlAddCurP;
			}
			else
			{
				MFC_Current_new = 0;
			}
		}
		else
		{
			if((U1_TSP_DEC_DIFF_CTRL_ACT==0)&&(U1_TSP_FADE_CTRL_ON==0)&&(U1_TSP_FADE_CTRL_MONITOR==0)&&(U1_TSP_DIFF_To_DEC_ON==0))
			{
				MFC_Current_new = S16_TSP_INITIAL_RISE_CURRENT;
			}
			else if((U1_TSP_ADD_TC_CURRENT_S==1)&&(CIRCUIT==0))
			{
				MFC_Current_new = MFC_Current_temp + lcs16TspDiffCtrlAddCurS;
			}
			else if((U1_TSP_ADD_TC_CURRENT_P==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = MFC_Current_temp + lcs16TspDiffCtrlAddCurP;
			}
			else
			{
				MFC_Current_new = MFC_Current_temp;
			}
  	}
  #else
  	if(TSP_DIFF_ONLY_CTRL_ACT == 1)
		{
			if((U1_TSP_ADD_TC_CURRENT_S==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = lcs16TspDiffCtrlAddCurS;
			}
			else if((U1_TSP_ADD_TC_CURRENT_P==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = lcs16TspDiffCtrlAddCurP;
			}
			else
			{
				MFC_Current_new = 0;
			}
		}
		else
		{
			if((U1_TSP_DEC_DIFF_CTRL_ACT==0)&&(U1_TSP_FADE_CTRL_ON==0)&&(U1_TSP_FADE_CTRL_MONITOR==0)&&(U1_TSP_DIFF_To_DEC_ON==0))
			{
				MFC_Current_new = S16_TSP_INITIAL_RISE_CURRENT;
			}
			else if((U1_TSP_ADD_TC_CURRENT_S==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = MFC_Current_temp + lcs16TspDiffCtrlAddCurS;
			}
			else if((U1_TSP_ADD_TC_CURRENT_P==1)&&(CIRCUIT==1))
			{
				MFC_Current_new = MFC_Current_temp + lcs16TspDiffCtrlAddCurP;
			}
			else
			{
				MFC_Current_new = MFC_Current_temp;
			}
  	}
  #endif	 	
	}
	else
	{
		MFC_Current_new = 0;
	}
	return MFC_Current_new;
}

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LATSPCallActHW
	#include "Mdyn_autosar.h"               
#endif
