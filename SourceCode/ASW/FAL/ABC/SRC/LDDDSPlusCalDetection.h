#ifndef __DDSPLUS_STRUCT_H__
#define __DDSPLUS_STRUCT_H__
#include"LVarHead.h"

#if (__DDS_PLUS == ENABLE)
#if __ECU==ESP_ECU_1
//#pragma DATA_SEG __GPAGE_SEG PAGED_RAM
#endif
typedef struct{
    //Variables Definition
    
    UCHAR ldddspu8SumPeriodCnt;
    UCHAR ldddspu8StoredPeriodCnt;
    UCHAR ldddspu8ResPeriodCnt;
    UINT ldddspu16ElapsedTime1Rev;
    INT ldddsps16TimeReference1Rap;
    UINT ldddspu16SumPeriodTime;
    UCHAR ldddspu8WheelTeethIndex;
    UCHAR ldddspu8WheelTeethIdxFinal;
    UCHAR ldddspu8WheelTeethIndexOld;
    UCHAR ldddspu8WheelTeethInxCurrent;
    INT ldddsps16TeethIndexDetCnt;
    UINT ldddspu16TeethDeviCalcCnt;
    INT ldddsps16Time1PulseResidue;
    INT ldddsps16WradOld;
    INT ldddsps16WradResmpNum;
    INT ldddsps16Hpf2OrdOutputTemp;
    INT ldddsps16WradShiftFinalOld;
    INT ldddsps16WradDeciCounter;
    INT ldddsps16WradResmpNumDeci;
    INT ldddsps16ResmpTimeCnt;
    INT ldddsps16WradShiftDiff;
    UCHAR ldddsps16BPFInitialOkCnt;
    INT ldddsps16AngularSpeedHpfRes;
    INT ldddsps16AngularSpeedBpfRes;
    INT ldddsps16CalCalibInitOkCnt;
    UCHAR ldddsps16CalCumulAvgCalibCnt[5];
//    INT ldddsps16CalSysParaCalibCnt;
    INT ldddsps16TireFreqCalib[5];
    INT ldddsps16TireFreqCalibCur;
    INT ldddsps16CalibInitCalOkCnt;
//    UINT ldddspu16DetCalibCompleteCnt[5];
    
    INT ldddsps16SysParaDefInitCalCnt;
    INT ldddsps16TireFreqDefDct;      
    INT ldddsps16SysParaDefDctCalCnt; 
    INT ldddsps16DefDctInitCalOkCnt;
    INT ldddsps16DefDctCnt;
    INT ldddsps16TireFreqDiff;
    INT ldddsps16CosOutput;                
   
    INT ldddsps16ZoomLPFInitialOkCnt;
    UCHAR ldddspu8SysParaBufCalibTimer[5];
    UCHAR ldddspu1CalibCompleteFlg[5];
    
    UCHAR ldddspu8SysParaBufDctTimer;
    //Bit Flags Definition
    unsigned ldddspu1Tire1RapCompleteFlg: 1;
    unsigned ldddspu1SteadyStraightDrvFlg: 1;
    unsigned ldddspu1TeethIndexStatus: 1;
    unsigned ldddspu1TeethIndexStatusK: 1;
    unsigned ldddspu1WheelTeethCalcOk: 1;
    unsigned ldddspu1BPFInitialOkFlg: 1;
    unsigned ldddspu1CalCalibUpdateFlg: 1;
    unsigned ldddspu1CalibInitCalOkFlg: 1;
    unsigned ldddspu1CalibCompleteFlg_t:  1;   
    unsigned ldddspu1CalDefDctUpdateFlg:  1; 
    unsigned ldddspu1DefDctInitCalOkFlg:  1; 
    unsigned ldddspu1LowTirePressFlg:    1;      
    unsigned ldddspu1ZoomLPFInitialOkFlg: 1;   
    unsigned ldddspu1CalDefDctUpdateFlg2: 1;        
} DDSp_SP_t;

#define ldddspu8SumPeriodCntWL              DDSp_SP_WL->ldddspu8SumPeriodCnt
#define ldddspu8StoredPeriodCntWL           DDSp_SP_WL->ldddspu8StoredPeriodCnt     
#define ldddspu8ResPeriodCntWL              DDSp_SP_WL->ldddspu8ResPeriodCnt        
#define ldddspu16ElapsedTime1RevWL          DDSp_SP_WL->ldddspu16ElapsedTime1Rev    
#define ldddsps16TimeReference1RapWL        DDSp_SP_WL->ldddsps16TimeReference1Rap  
#define ldddspu16SumPeriodTimeWL            DDSp_SP_WL->ldddspu16SumPeriodTime      
#define ldddspu8WheelTeethIndexWL           DDSp_SP_WL->ldddspu8WheelTeethIndex     
#define ldddspu8WheelTeethIndexOldWL        DDSp_SP_WL->ldddspu8WheelTeethIndexOld  
#define ldddsps16TeethIndexDetCntWL         DDSp_SP_WL->ldddsps16TeethIndexDetCnt   
#define ldddspu16TeethDeviCalcCntWL         DDSp_SP_WL->ldddspu16TeethDeviCalcCnt   
#define ldddsps16Time1PulseResidueWL        DDSp_SP_WL->ldddsps16Time1PulseResidue  
#define ldddsps16WradOldWL                  DDSp_SP_WL->ldddsps16WradOld            
#define ldddsps16WradResmpNumWL             DDSp_SP_WL->ldddsps16WradResmpNum      
#define ldddsps16Hpf2OrdOutputTempWL        DDSp_SP_WL->ldddsps16Hpf2OrdOutputTemp  
#define ldddsps16WradShiftFinalOldWL        DDSp_SP_WL->ldddsps16WradShiftFinalOld  
#define ldddsps16WradDeciCounterWL          DDSp_SP_WL->ldddsps16WradDeciCounter    
#define ldddsps16WradResmpNumDeciWL         DDSp_SP_WL->ldddsps16WradResmpNumDeci   
#define ldddsps16ResmpTimeCntWL             DDSp_SP_WL->ldddsps16ResmpTimeCnt                                                 
#define ldddspu8WheelTeethIdxFinalWL        DDSp_SP_WL->ldddspu8WheelTeethIdxFinal
#define ldddspu1TeethIndexStatusKWL         DDSp_SP_WL->ldddspu1TeethIndexStatusK
#define ldddspu8WheelTeethInxCurrentWL      DDSp_SP_WL->ldddspu8WheelTeethInxCurrent
#define ldddsps16BPFInitialOkCntWL          DDSp_SP_WL->ldddsps16BPFInitialOkCnt
#define ldddsps16AngularSpeedHpfResWL       DDSp_SP_WL->ldddsps16AngularSpeedHpfRes
#define ldddsps16AngularSpeedBpfResWL       DDSp_SP_WL->ldddsps16AngularSpeedBpfRes
#define ldddspu1CalCalibUpdateFlgWL         DDSp_SP_WL->ldddspu1CalCalibUpdateFlg    
//#define ldddspu1CalCalibInitOkFlgWL         DDSp_SP_WL->ldddspu1CalCalibInitOkFlg
#define ldddsps16CalCalibInitOkCntWL        DDSp_SP_WL->ldddsps16CalCalibInitOkCnt
//#define ldddsps16CalCumulAvgCalibCntWL      DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt
#define ldddsps16CalSysParaCalibCntWL       DDSp_SP_WL->ldddsps16CalSysParaCalibCnt
#define ldddsps16TireFreqCalibWL            DDSp_SP_WL->ldddsps16TireFreqCalibCur
#define ldddsps16CalibInitCalOkCntWL        DDSp_SP_WL->ldddsps16CalibInitCalOkCnt
//#define ldddspu16DetCalibCompleteCntWL      DDSp_SP_WL->ldddspu16DetCalibCompleteCnt
#define ldddsps16SysParaDefInitCalCntWL     DDSp_SP_WL->ldddsps16SysParaDefInitCalCnt
#define ldddsps16TireFreqDefDctWL           DDSp_SP_WL->ldddsps16TireFreqDefDct      
#define ldddsps16SysParaDefDctCalCntWL      DDSp_SP_WL->ldddsps16SysParaDefDctCalCnt 
#define ldddsps16DefDctInitCalOkCntWL       DDSp_SP_WL->ldddsps16DefDctInitCalOkCnt
#define ldddsps16DefDctCntWL                DDSp_SP_WL->ldddsps16DefDctCnt
#define ldddsps16TireFreqDiffWL             DDSp_SP_WL->ldddsps16TireFreqDiff
#define ldddspu8SysParaBufDctTimerWL        DDSp_SP_WL->ldddspu8SysParaBufDctTimer
#define ldddspu1CalDefDctUpdateFlg2WL       DDSp_SP_WL->ldddspu1CalDefDctUpdateFlg2


#define ldddspu1CalDefDctUpdateFlgWL        DDSp_SP_WL->ldddspu1CalDefDctUpdateFlg 
#define ldddspu1DefDctInitCalOkFlgWL        DDSp_SP_WL->ldddspu1DefDctInitCalOkFlg  

#define ldddspu1Tire1RapCompleteFlgWL       DDSp_SP_WL->ldddspu1Tire1RapCompleteFlg 
#define ldddspu1SteadyStraightDrvFlgWL      DDSp_SP_WL->ldddspu1SteadyStraightDrvFlg
#define ldddspu1TeethIndexStatusWL          DDSp_SP_WL->ldddspu1TeethIndexStatus    
#define ldddspu1WheelTeethCalcOkWL          DDSp_SP_WL->ldddspu1WheelTeethCalcOk    
#define ldddspu1BPFInitialOkFlgWL           DDSp_SP_WL->ldddspu1BPFInitialOkFlg   
#define ldddsps16WradShiftDiffWL            DDSp_SP_WL->ldddsps16WradShiftDiff
#define ldddspu1CalibInitCalOkFlgWL         DDSp_SP_WL->ldddspu1CalibInitCalOkFlg
//#define ldddspu1CalibCompleteFlgWL          DDSp_SP_WL->ldddspu1CalibCompleteFlg
#define ldddspu1DeflationStateFlgWL           DDSp_SP_WL->ldddspu1LowTirePressFlg

#define ldddspu1ZoomLPFInitialOkFlgWL       DDSp_SP_WL->ldddspu1ZoomLPFInitialOkFlg
#define ldddsps16ZoomLPFInitialOkCntWL      DDSp_SP_WL->ldddsps16ZoomLPFInitialOkCnt

//#define ldddspu8SysParaBufCalibTimerWL      DDSp_SP_WL->ldddspu8SysParaBufCalibTimer
#define ldddsps16CosOutputWL                DDSp_SP_WL->ldddsps16CosOutput
#define ldddspu1Tire1RapCompleteFlgFL       DDSp_SP_FL.ldddspu1Tire1RapCompleteFlg
#define ldddspu1Tire1RapCompleteFlgFR       DDSp_SP_FR.ldddspu1Tire1RapCompleteFlg
#define ldddspu1Tire1RapCompleteFlgRL       DDSp_SP_RL.ldddspu1Tire1RapCompleteFlg
#define ldddspu1Tire1RapCompleteFlgRR       DDSp_SP_RR.ldddspu1Tire1RapCompleteFlg

#define ldddsps16TireFreqCalibFL           DDSp_SP_FL.ldddsps16TireFreqCalibCur
#define ldddsps16TireFreqCalibFR           DDSp_SP_FR.ldddsps16TireFreqCalibCur
#define ldddsps16TireFreqCalibRL           DDSp_SP_RL.ldddsps16TireFreqCalibCur
#define ldddsps16TireFreqCalibRR           DDSp_SP_RR.ldddsps16TireFreqCalibCur


#define ldddspu1CalibCompleteFlgFL          DDSp_SP_FL.ldddspu1CalibCompleteFlg_t
#define ldddspu1CalibCompleteFlgFR          DDSp_SP_FR.ldddspu1CalibCompleteFlg_t
#define ldddspu1CalibCompleteFlgRL          DDSp_SP_RL.ldddspu1CalibCompleteFlg_t
#define ldddspu1CalibCompleteFlgRR          DDSp_SP_RR.ldddspu1CalibCompleteFlg_t

#define ldddspu1DeflationStateFlgFL           DDSp_SP_FL.ldddspu1LowTirePressFlg
#define ldddspu1DeflationStateFlgFR           DDSp_SP_FR.ldddspu1LowTirePressFlg
#define ldddspu1DeflationStateFlgRL           DDSp_SP_RL.ldddspu1LowTirePressFlg
#define ldddspu1DeflationStateFlgRR           DDSp_SP_RR.ldddspu1LowTirePressFlg


#define ldddspu1AllWhlCalibCompleteFlg      DDSp_F.bit7      
#define ldddspu1TireDeflationStateFlg                           DDSp_F.bit6
#define DDSp_Flg5                           DDSp_F.bit5
#define ldddsu1RequestDdsPlusReset                    		DDSp_F.bit4        
#define write_ddsp_eeprom_end_flg                     		DDSp_F.bit3
#define read_ddsp_eeprom_end_flg                     		DDSp_F.bit2
#define DDSp_Flg1                     		DDSp_F.bit1       
#define DDSp_Flg0                           DDSp_F.bit0


#define ddspSysParaCumulSumCalib_eepend     DDSp_F2.bit7
#define DDSCumulSumVehSpdCalibEEPEnd        DDSp_F2.bit6
#define DDSp_SP_t_FL_EEP_END                DDSp_F2.bit5
#define DDSp_SP_t_FR_EEP_END                DDSp_F2.bit4
#define DDSp_SP_t_RL_EEP_END                DDSp_F2.bit3
#define DDSp_SP_t_RR_EEP_END                DDSp_F2.bit2
#define DDSp2_Flg1                     		DDSp_F2.bit1       
#define DDSp2_Flg0                          DDSp_F2.bit0



#define ddsp_eep_wr_ok_flg01 	DDSp_WR_OK_FLG1.bit7
#define ddsp_eep_wr_ok_flg02 	DDSp_WR_OK_FLG1.bit6
#define ddsp_eep_wr_ok_flg03 	DDSp_WR_OK_FLG1.bit5
#define ddsp_eep_wr_ok_flg04 	DDSp_WR_OK_FLG1.bit4
#define ddsp_eep_wr_ok_flg05 	DDSp_WR_OK_FLG1.bit3
#define ddsp_eep_wr_ok_flg06 	DDSp_WR_OK_FLG1.bit2
#define ddsp_eep_wr_ok_flg07 	DDSp_WR_OK_FLG1.bit1
#define ddsp_eep_wr_ok_flg08 	DDSp_WR_OK_FLG1.bit0

#define ddsp_eep_wr_ok_flg09 	DDSp_WR_OK_FLG2.bit7
#define ddsp_eep_wr_ok_flg10 	DDSp_WR_OK_FLG2.bit6
#define ddsp_eep_wr_ok_flg11 	DDSp_WR_OK_FLG2.bit5
#define ddsp_eep_wr_ok_flg12 	DDSp_WR_OK_FLG2.bit4
#define ddsp_eep_wr_ok_flg13 	DDSp_WR_OK_FLG2.bit3
#define ddsp_eep_wr_ok_flg14 	DDSp_WR_OK_FLG2.bit2
#define ddsp_eep_wr_ok_flg15 	DDSp_WR_OK_FLG2.bit1
#define ddsp_eep_wr_ok_flg16 	DDSp_WR_OK_FLG2.bit0

#define ddsp_eep_wr_ok_flg17 	DDSp_WR_OK_FLG3.bit7
#define ddsp_eep_wr_ok_flg18 	DDSp_WR_OK_FLG3.bit6
#define ddsp_eep_wr_ok_flg19 	DDSp_WR_OK_FLG3.bit5
#define ddsp_eep_wr_ok_flg20 	DDSp_WR_OK_FLG3.bit4
#define ddsp_eep_wr_ok_flg21 	DDSp_WR_OK_FLG3.bit3
#define ddsp_eep_wr_ok_flg22 	DDSp_WR_OK_FLG3.bit2
#define ddsp_eep_wr_ok_flg23 	DDSp_WR_OK_FLG3.bit1
#define ddsp_eep_wr_ok_flg24 	DDSp_WR_OK_FLG3.bit0

#define ddsp_eep_wr_ok_flg25 	DDSp_WR_OK_FLG4.bit7
#define ddsp_eep_wr_ok_flg26 	DDSp_WR_OK_FLG4.bit6
#define ddsp_eep_wr_ok_flg27 	DDSp_WR_OK_FLG4.bit5
#define ddsp_eep_wr_ok_flg28 	DDSp_WR_OK_FLG4.bit4
#define ddsp_eep_wr_ok_flg29 	DDSp_WR_OK_FLG4.bit3
#define ddsp_eep_wr_ok_flg30 	DDSp_WR_OK_FLG4.bit2
#define ddsp_eep_wr_ok_flg31 	DDSp_WR_OK_FLG4.bit1
#define ddsp_eep_wr_ok_flg32 	DDSp_WR_OK_FLG4.bit0

#define ddsp_eep_wr_ok_flg33 	DDSp_WR_OK_FLG5.bit7
#define ddsp_eep_wr_ok_flg34 	DDSp_WR_OK_FLG5.bit6
#define ddsp_eep_wr_ok_flg35 	DDSp_WR_OK_FLG5.bit5
#define ddsp_eep_wr_ok_flg36 	DDSp_WR_OK_FLG5.bit4
#define ddsp_eep_wr_ok_flg37 	DDSp_WR_OK_FLG5.bit3
#define ddsp_eep_wr_ok_flg38 	DDSp_WR_OK_FLG5.bit2
#define ddsp_eep_wr_ok_flg39 	DDSp_WR_OK_FLG5.bit1
#define ddsp_eep_wr_ok_flg40 	DDSp_WR_OK_FLG5.bit0

#define ddsp_eep_wr_ok_flg41 	DDSp_WR_OK_FLG6.bit7
#define ddsp_eep_wr_ok_flg42 	DDSp_WR_OK_FLG6.bit6
#define ddsp_eep_wr_ok_flg43 	DDSp_WR_OK_FLG6.bit5
#define ddsp_eep_wr_ok_flg44 	DDSp_WR_OK_FLG6.bit4
#define ddsp_eep_wr_ok_flg45 	DDSp_WR_OK_FLG6.bit3
#define ddsp_eep_wr_ok_flg46 	DDSp_WR_OK_FLG6.bit2
#define ddsp_eep_wr_ok_flg47 	DDSp_WR_OK_FLG6.bit1
#define ddsp_eep_wr_ok_flg48 	DDSp_WR_OK_FLG6.bit0

#define ddsp_eep_wr_ok_flg49 	DDSp_WR_OK_FLG7.bit7
#define ddsp_eep_wr_ok_flg50 	DDSp_WR_OK_FLG7.bit6
#define ddsp_eep_wr_ok_flg51 	DDSp_WR_OK_FLG7.bit5
#define ddsp_eep_wr_ok_flg52 	DDSp_WR_OK_FLG7.bit4
#define ddsp_eep_wr_ok_flg53 	DDSp_WR_OK_FLG7.bit3
#define ddsp_eep_wr_ok_flg54 	DDSp_WR_OK_FLG7.bit2
#define ddsp_eep_wr_ok_flg55 	DDSp_WR_OK_FLG7.bit1
#define ddsp_eep_wr_ok_flg56 	DDSp_WR_OK_FLG7.bit0

#define ddsp_eep_wr_ok_flg57 	DDSp_WR_OK_FLG8.bit7
#define ddsp_eep_wr_ok_flg58 	DDSp_WR_OK_FLG8.bit6
#define ddsp_eep_wr_ok_flg59 	DDSp_WR_OK_FLG8.bit5
#define ddsp_eep_wr_ok_flg60 	DDSp_WR_OK_FLG8.bit4
#define ddsp_eep_wr_ok_flg61 	DDSp_WR_OK_FLG8.bit3
#define ddsp_eep_wr_ok_flg62 	DDSp_WR_OK_FLG8.bit2
#define ddsp_eep_wr_ok_flg63 	DDSp_WR_OK_FLG8.bit1
#define ddsp_eep_wr_ok_flg64 	DDSp_WR_OK_FLG8.bit0


#define ddsp_eep_read_ok_flg01 	DDSp_READ_OK_FLG1.bit7
#define ddsp_eep_read_ok_flg02 	DDSp_READ_OK_FLG1.bit6
#define ddsp_eep_read_ok_flg03 	DDSp_READ_OK_FLG1.bit5
#define ddsp_eep_read_ok_flg04 	DDSp_READ_OK_FLG1.bit4
#define ddsp_eep_read_ok_flg05 	DDSp_READ_OK_FLG1.bit3
#define ddsp_eep_read_ok_flg06 	DDSp_READ_OK_FLG1.bit2
#define ddsp_eep_read_ok_flg07 	DDSp_READ_OK_FLG1.bit1
#define ddsp_eep_read_ok_flg08 	DDSp_READ_OK_FLG1.bit0

#define ddsp_eep_read_ok_flg09 	DDSp_READ_OK_FLG2.bit7
#define ddsp_eep_read_ok_flg10 	DDSp_READ_OK_FLG2.bit6
#define ddsp_eep_read_ok_flg11 	DDSp_READ_OK_FLG2.bit5
#define ddsp_eep_read_ok_flg12 	DDSp_READ_OK_FLG2.bit4
#define ddsp_eep_read_ok_flg13 	DDSp_READ_OK_FLG2.bit3
#define ddsp_eep_read_ok_flg14 	DDSp_READ_OK_FLG2.bit2
#define ddsp_eep_read_ok_flg15 	DDSp_READ_OK_FLG2.bit1
#define ddsp_eep_read_ok_flg16 	DDSp_READ_OK_FLG2.bit0

#define ddsp_eep_read_ok_flg17 	DDSp_READ_OK_FLG3.bit7
#define ddsp_eep_read_ok_flg18 	DDSp_READ_OK_FLG3.bit6
#define ddsp_eep_read_ok_flg19 	DDSp_READ_OK_FLG3.bit5
#define ddsp_eep_read_ok_flg20 	DDSp_READ_OK_FLG3.bit4
#define ddsp_eep_read_ok_flg21 	DDSp_READ_OK_FLG3.bit3
#define ddsp_eep_read_ok_flg22 	DDSp_READ_OK_FLG3.bit2
#define ddsp_eep_read_ok_flg23 	DDSp_READ_OK_FLG3.bit1
#define ddsp_eep_read_ok_flg24 	DDSp_READ_OK_FLG3.bit0

#define ddsp_eep_read_ok_flg25 	DDSp_READ_OK_FLG4.bit7
#define ddsp_eep_read_ok_flg26 	DDSp_READ_OK_FLG4.bit6
#define ddsp_eep_read_ok_flg27 	DDSp_READ_OK_FLG4.bit5
#define ddsp_eep_read_ok_flg28 	DDSp_READ_OK_FLG4.bit4
#define ddsp_eep_read_ok_flg29 	DDSp_READ_OK_FLG4.bit3
#define ddsp_eep_read_ok_flg30 	DDSp_READ_OK_FLG4.bit2
#define ddsp_eep_read_ok_flg31 	DDSp_READ_OK_FLG4.bit1
#define ddsp_eep_read_ok_flg32 	DDSp_READ_OK_FLG4.bit0

#define ddsp_eep_read_ok_flg33 	DDSp_READ_OK_FLG5.bit7
#define ddsp_eep_read_ok_flg34 	DDSp_READ_OK_FLG5.bit6
#define ddsp_eep_read_ok_flg35 	DDSp_READ_OK_FLG5.bit5
#define ddsp_eep_read_ok_flg36 	DDSp_READ_OK_FLG5.bit4
#define ddsp_eep_read_ok_flg37 	DDSp_READ_OK_FLG5.bit3
#define ddsp_eep_read_ok_flg38 	DDSp_READ_OK_FLG5.bit2
#define ddsp_eep_read_ok_flg39 	DDSp_READ_OK_FLG5.bit1
#define ddsp_eep_read_ok_flg40 	DDSp_READ_OK_FLG5.bit0

#define ddsp_eep_read_ok_flg41 	DDSp_READ_OK_FLG6.bit7
#define ddsp_eep_read_ok_flg42 	DDSp_READ_OK_FLG6.bit6
#define ddsp_eep_read_ok_flg43 	DDSp_READ_OK_FLG6.bit5
#define ddsp_eep_read_ok_flg44 	DDSp_READ_OK_FLG6.bit4
#define ddsp_eep_read_ok_flg45 	DDSp_READ_OK_FLG6.bit3
#define ddsp_eep_read_ok_flg46 	DDSp_READ_OK_FLG6.bit2
#define ddsp_eep_read_ok_flg47 	DDSp_READ_OK_FLG6.bit1
#define ddsp_eep_read_ok_flg48 	DDSp_READ_OK_FLG6.bit0

#define ddsp_eep_read_ok_flg49 	DDSp_READ_OK_FLG7.bit7
#define ddsp_eep_read_ok_flg50 	DDSp_READ_OK_FLG7.bit6
#define ddsp_eep_read_ok_flg51 	DDSp_READ_OK_FLG7.bit5
#define ddsp_eep_read_ok_flg52 	DDSp_READ_OK_FLG7.bit4
#define ddsp_eep_read_ok_flg53 	DDSp_READ_OK_FLG7.bit3
#define ddsp_eep_read_ok_flg54 	DDSp_READ_OK_FLG7.bit2
#define ddsp_eep_read_ok_flg55 	DDSp_READ_OK_FLG7.bit1
#define ddsp_eep_read_ok_flg56 	DDSp_READ_OK_FLG7.bit0

#define ddsp_eep_read_ok_flg57 	DDSp_READ_OK_FLG8.bit7
#define ddsp_eep_read_ok_flg58 	DDSp_READ_OK_FLG8.bit6
#define ddsp_eep_read_ok_flg59 	DDSp_READ_OK_FLG8.bit5
#define ddsp_eep_read_ok_flg60 	DDSp_READ_OK_FLG8.bit4
#define ddsp_eep_read_ok_flg61 	DDSp_READ_OK_FLG8.bit3
#define ddsp_eep_read_ok_flg62 	DDSp_READ_OK_FLG8.bit2
#define ddsp_eep_read_ok_flg63 	DDSp_READ_OK_FLG8.bit1
#define ddsp_eep_read_ok_flg64 	DDSp_READ_OK_FLG8.bit0

#define EEP_SECTOR_OFFSET_DDSp		0x102 /*0x302~0x3ff*/

#define U16_DDSp_EERPOM_DATA_01		(0x200+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_02		(0x204+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_03		(0x208+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_04		(0x20C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_05		(0x210+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_06		(0x214+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDSp_EERPOM_DATA_07		(0x218+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDSp_EERPOM_DATA_08		(0x21C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_09		(0x220+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_10		(0x224+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_11		(0x228+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_12		(0x22C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_13		(0x230+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDSp_EERPOM_DATA_14		(0x234+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDSp_EERPOM_DATA_15		(0x238+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_16		(0x23C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_17		(0x240+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_18		(0x244+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_19		(0x248+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_20		(0x24C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDSp_EERPOM_DATA_21		(0x250+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDSp_EERPOM_DATA_22		(0x254+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_23		(0x258+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_24		(0x25C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_25		(0x260+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_26		(0x264+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_27		(0x268+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDSp_EERPOM_DATA_28		(0x26C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDSp_EERPOM_DATA_29		(0x270+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_30		(0x274+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_31		(0x278+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_32		(0x27C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_33		(0x280+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_34		(0x284+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDSp_EERPOM_DATA_35		(0x288+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDSp_EERPOM_DATA_36		(0x28C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDSp_EERPOM_DATA_37		(0x290+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDSp_EERPOM_DATA_38		(0x294+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDSp_EERPOM_DATA_39		(0x298+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDSp_EERPOM_DATA_40		(0x29C+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_41		(0x2a0+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_42		(0x2a4+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    

#define U16_DDSp_EERPOM_DATA_43		(0x2a8+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_44		(0x2ac+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_45		(0x2b0+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_46		(0x2b4+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_47		(0x2b8+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_48		(0x2bc+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_49		(0x2c0+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_50		(0x2c4+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_51		(0x2c8+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_52		(0x2cc+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_53		(0x2d0+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_54		(0x2d4+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_55		(0x2d8+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_56		(0x2dc+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_57		(0x2e0+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_58		(0x2e4+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_59		(0x2e8+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDSp_EERPOM_DATA_60		(0x2ec+EEP_SECTOR_OFFSET_DDSp)        // ddsp_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    


#define DDS_SYSPARA_SUMCALIB_ADDR       U16_DDSp_EERPOM_DATA_01
#define DDS_SYSPARA_VEHSPDCALIB_ADDR    U16_DDSp_EERPOM_DATA_21
#define DDS_SYSPARA_FLWHEEL_ADDR        U16_DDSp_EERPOM_DATA_31
#define DDS_SYSPARA_FRWHEEL_ADDR        U16_DDSp_EERPOM_DATA_38
#define DDS_SYSPARA_RLWHEEL_ADDR        U16_DDSp_EERPOM_DATA_45
#define DDS_SYSPARA_RRWHEEL_ADDR        U16_DDSp_EERPOM_DATA_52
//#define ldddspu1DdsplusRunningReset ENABLE  
extern DDSp_SP_t *DDSp_SP_WL,DDSp_SP_FL,DDSp_SP_FR,DDSp_SP_RL,DDSp_SP_RR;
extern struct 	U8_BIT_STRUCT_t DDSp_F;   
#if __ECU==ESP_ECU_1	
//#pragma DATA_SEG DEFAULT
#endif 
/*===================================================*/
//typedef struct{
//    ;;
//    
//} DDSp_CAL_t;
//
///*===================================================*/
//typedef struct{
//    ;;
//    
//} DDSp_DEF_t;
/*===================================================*/
extern void LDDDSp_vCallDetection(void);
extern void LDDDSp_vWriteDDSpParameters(void);
extern void LDDDS_EEPROMWriteFunciton(void);
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1
extern void LDDDSp_vCallDDSplusLogData(void);
extern void LDDDSp_vCallDDSplusIntegLogData(void);
extern void LDDDSp_vCallDDSplusDataSet(void);
    #endif
#endif
/*===================================================*/
#endif
#endif