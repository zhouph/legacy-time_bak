#ifndef __LSBTCSCALLLOGDATA_H__
#define __LSBTCSCALLLOGDATA_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition******************************************************/


/*Definition of Developing Function ******************************************************/

/* Calibration parameters ******************************************************/

/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/

/*Global Extern Functions  Declaration******************************************************/
extern void LSTCS_vCallBTCS_Gen2_Val_Log(void);

#endif
