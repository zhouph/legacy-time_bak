#ifndef __LATSPCALLACTHW_H__
#define __LATSPCALLACTHW_H__
#include "LVarHead.h"

#if __TSP
extern int16_t LAMFC_s16SetMFCCurrentTSP(uint8_t CIRCUIT, int16_t MFC_Current_temp);
#endif

#endif
