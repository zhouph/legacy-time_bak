#ifndef __LVARHEAD_H
#define __LVARHEAD_H
#include "Abc_Ctrl.h"

#include "LBaseHead.h"

#include "_algo_va.h"

#include "_edc_va.h"

#if __VDC
    #include "_tcs_va.h"
#endif

#if __VDC
    #include "_esp_va.h"

    #if !__MODEL_CHANGE
    #if __CAR==SYC_RAXTON /*|| __CAR==HMC_EN*/ || __CAR==SYC_KYRON
    #else
    	#include "_esp_model_va.h"/*060602 for HM reconstruction by eslim*/
    #endif
    #endif

    #if __TIRE_MODEL
  //  	#include "_esp_tire_force.h"
    #endif
#endif

#if defined(__UCC_1)
    #include "..\UCC_Logic\_UCC.h"
#endif

#if __TSP
    #include "_tsp_va.h"
#endif

#endif
