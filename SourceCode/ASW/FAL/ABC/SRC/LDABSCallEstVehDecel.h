#ifndef __LDABSAFZEST_H__
#define __LDABSAFZEST_H__
/*includes********************************************************************/
#include	"LVarHead.h"
#include 	"LDABSCallDctForVref.H"

//#if __VDC
//#include 	"LDESPCalVehicleModel.h"
//#include	"LDESPEstBeta.h"
//#endif

/*Global Type Declaration ****************************************************/
#define AFZ_INCREDIBLE      	    AFZF0.bit7
#define AFZ_INCREDIBLE_K			AFZF0.bit6
#define AFZ_INCREDIBLE_A            AFZF0.bit5
#define vref_decrease_permission    AFZF0.bit4
#define LOW_MU_SUSPECT_FLG          AFZF0.bit3
#define EBD_GRV_HIGH_MU_OK          AFZF0.bit2	
#define AFZ_CAL_SUS_flag            AFZF0.bit1        
#define AFZ_CAL_SUS_K_flag          AFZF0.bit0
#define FL_SELECT_WH_OLD     	    AFZF1.bit7
#define FR_SELECT_WH_OLD			AFZF1.bit6
#define RL_SELECT_WH_OLD            AFZF1.bit5
#define RR_SELECT_WH_OLD            AFZF1.bit4
#define WHEEL_SELECT_CHANGE_FLG     AFZF1.bit3
#define WHEEL_CHANGE_HOLD_FLG       AFZF1.bit2	
#define lsabsu1HDCdecelInputChange  AFZF1.bit1        
#define ldabsu1AFZLowMuWhlBehavior  AFZF1.bit0

/*Global Extern Variable  Declaration*****************************************/

extern U8_BIT_STRUCT_t AFZF0,AFZF1;

       



    extern int16_t   	ebd_refilt_grv;
	extern int16_t		aref0;    
    extern int16_t		aref_avg;
	extern int16_t   	v_afz; 
	extern uint16_t  	t_afz;
	extern uint8_t 		afz_flags;   
    extern int16_t   	ebd_filt_grv;
	extern uint16_t		vref_increase_cnt;
	extern int16_t		vref_trough;
	
#if __VDC && __PRESS_EST 
	extern int16_t   	Vehicle_Decel_EST; 
    extern int16_t Vehicle_Decel_EST_filter;
#endif

#if __HDC
  extern uint8_t  lsabsu8WheelSelectChangeCNT;
  extern int16_t    lsabss16HDCWheelAccel; 
#endif  

#if (__GM_BRK_PEDAL_SIGNAL_DEMO == ENABLE)
    extern int16_t ldabss16VehDecelByBrkPedal;
    extern int16_t ldabss16VehDecelByBrkPedalFilt;
#endif

   	/*	LDABS_vCalVehDecelbyVref5 */	
	extern int16_t lsabss16DecelRefiltByVref5; /* 2010.04.28 Decel by vref5 for ESP Brake Lamp : 1/1000 Resolution */
	extern uint8_t lsabsu8Decelvalidity;
#if __SPIN_DET_IMPROVE == ENABLE
	extern int16_t     ls16wheelaccel;
	extern int16_t     ls16wheelgaccel;
	extern int16_t     ls16accelacc;
	extern int16_t     ls16accel;

	extern int16_t     ls16wheelaccelFL;
	extern int16_t     ls16wheelaccelFR;
	extern int16_t     ls16wheelaccelRL;
	extern int16_t     ls16wheelaccelRR;
	extern int16_t     ls16wheelgaccelFL;
	extern int16_t     ls16wheelgaccelFR;
	extern int16_t     ls16wheelgaccelRL;
	extern int16_t     ls16wheelgaccelRR;
#endif
extern  struct  ACC_FILTER V_ACC;
#if __SPIN_DET_IMPROVE == ENABLE
extern  struct  ACC_FILTER WHEEL_ACC,WHEEL_G_ACC, FL_ACC, FR_ACC, RL_ACC, RR_ACC, FL_ACC_ACC, FR_ACC_ACC, RL_ACC_ACC, RR_ACC_ACC;
#endif




  
/*Global Extern Functions  Declaration****************************************/
extern void	LDABS_vCallEstVehDecel(void);

#endif
