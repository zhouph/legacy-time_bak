#ifndef __T_STRUCT_H__
#define __T_STRUCT_H__

struct TF0_BIT
{
    uint16_t TF0_0          :1;
    uint16_t TF0_1          :1;
    uint16_t TF0_2          :1;
    uint16_t TF0_3          :1;
    uint16_t TF0_4          :1;
    uint16_t TF0_5          :1;
    uint16_t TF0_6          :1;
    uint16_t TF0_7          :1;
};

struct TF1_BIT
{
    uint16_t TF1_0          :1;
    uint16_t TF1_1          :1;
    uint16_t TF1_2          :1;
    uint16_t TF1_3          :1;
    uint16_t TF1_4          :1;
    uint16_t TF1_5          :1;
    uint16_t TF1_6          :1;
    uint16_t TF1_7          :1;
};

struct TF2_BIT
{
    uint16_t TF2_0          :1;
    uint16_t TF2_1          :1;
    uint16_t TF2_2          :1;
    uint16_t TF2_3          :1;
    uint16_t TF2_4          :1;
    uint16_t TF2_5          :1;
    uint16_t TF2_6          :1;
    uint16_t TF2_7          :1;
};

struct TF3_BIT
{
    uint16_t TF3_0          :1;
    uint16_t TF3_1          :1;
    uint16_t TF3_2          :1;
    uint16_t TF3_3          :1;
    uint16_t TF3_4          :1;
    uint16_t TF3_5          :1;
    uint16_t TF3_6          :1;
    uint16_t TF3_7          :1;
};

struct TF4_BIT
{
    uint16_t TF4_0          :1;
    uint16_t TF4_1          :1;
    uint16_t TF4_2          :1;
    uint16_t TF4_3          :1;
    uint16_t TF4_4          :1;
    uint16_t TF4_5          :1;
    uint16_t TF4_6          :1;
    uint16_t TF4_7          :1;
};

struct TF5_BIT
{
    uint16_t TF5_0          :1;
    uint16_t TF5_1          :1;
    uint16_t TF5_2          :1;
    uint16_t TF5_3          :1;
    uint16_t TF5_4          :1;
    uint16_t TF5_5          :1;
    uint16_t TF5_6          :1;
    uint16_t TF5_7          :1;
};

struct TF6_BIT
{
    uint16_t TF6_0          :1;
    uint16_t TF6_1          :1;
    uint16_t TF6_2          :1;
    uint16_t TF6_3          :1;
    uint16_t TF6_4          :1;
    uint16_t TF6_5          :1;
    uint16_t TF6_6          :1;
    uint16_t TF6_7          :1;
};

struct TF7_BIT
{
    uint16_t TF7_0          :1;
    uint16_t TF7_1          :1;
    uint16_t TF7_2          :1;
    uint16_t TF7_3          :1;
    uint16_t TF7_4          :1;
    uint16_t TF7_5          :1;
    uint16_t TF7_6          :1;
    uint16_t TF7_7          :1;
};

struct TF8_BIT
{
    uint16_t TF8_0          :1;
    uint16_t TF8_1          :1;
    uint16_t TF8_2          :1;
    uint16_t TF8_3          :1;
    uint16_t TF8_4          :1;
    uint16_t TF8_5          :1;
    uint16_t TF8_6          :1;
    uint16_t TF8_7          :1;
};

struct TF9_BIT
{
    uint16_t TF9_0          :1;
    uint16_t TF9_1          :1;
    uint16_t TF9_2          :1;
    uint16_t TF9_3          :1;
    uint16_t TF9_4          :1;
    uint16_t TF9_5          :1;
    uint16_t TF9_6          :1;
    uint16_t TF9_7          :1;
};

struct TF10_BIT
{
    uint16_t TF10_0          :1;
    uint16_t TF10_1          :1;
    uint16_t TF10_2          :1;
    uint16_t TF10_3          :1;
    uint16_t TF10_4          :1;
    uint16_t TF10_5          :1;
    uint16_t TF10_6          :1;
    uint16_t TF10_7          :1;
};

struct TF11_BIT
{
    uint16_t TF11_0          :1;
    uint16_t TF11_1          :1;
    uint16_t TF11_2          :1;
    uint16_t TF11_3          :1;
    uint16_t TF11_4          :1;
    uint16_t TF11_5          :1;
    uint16_t TF11_6          :1;
    uint16_t TF11_7          :1;
};

struct TF12_BIT
{
    uint16_t TF12_0          :1;
    uint16_t TF12_1          :1;
    uint16_t TF12_2          :1;
    uint16_t TF12_3          :1;
    uint16_t TF12_4          :1;
    uint16_t TF12_5          :1;
    uint16_t TF12_6          :1;
    uint16_t TF12_7          :1;
};

struct TF13_BIT
{
    uint16_t TF13_0          :1;
    uint16_t TF13_1          :1;
    uint16_t TF13_2          :1;
    uint16_t TF13_3          :1;
    uint16_t TF13_4          :1;
    uint16_t TF13_5          :1;
    uint16_t TF13_6          :1;
    uint16_t TF13_7          :1;
};

struct TF14_BIT
{
    uint16_t TF14_0          :1;
    uint16_t TF14_1          :1;
    uint16_t TF14_2          :1;
    uint16_t TF14_3          :1;
    uint16_t TF14_4          :1;
    uint16_t TF14_5          :1;
    uint16_t TF14_6          :1;
    uint16_t TF14_7          :1;
};

struct TF15_BIT
{
    uint16_t TF15_0          :1;
    uint16_t TF15_1          :1;
    uint16_t TF15_2          :1;
    uint16_t TF15_3          :1;
    uint16_t TF15_4          :1;
    uint16_t TF15_5          :1;
    uint16_t TF15_6          :1;
    uint16_t TF15_7          :1;
};

struct TF16_BIT
{
    uint16_t TF16_0          :1;
    uint16_t TF16_1          :1;
    uint16_t TF16_2          :1;
    uint16_t TF16_3          :1;
    uint16_t TF16_4          :1;
    uint16_t TF16_5          :1;
    uint16_t TF16_6          :1;
    uint16_t TF16_7          :1;
};

struct TF17_BIT
{
    uint16_t TF17_0          :1;
    uint16_t TF17_1          :1;
    uint16_t TF17_2          :1;
    uint16_t TF17_3          :1;
    uint16_t TF17_4          :1;
    uint16_t TF17_5          :1;
    uint16_t TF17_6          :1;
    uint16_t TF17_7          :1;
};

struct TF18_BIT
{
    uint16_t TF18_0          :1;
    uint16_t TF18_1          :1;
    uint16_t TF18_2          :1;
    uint16_t TF18_3          :1;
    uint16_t TF18_4          :1;
    uint16_t TF18_5          :1;
    uint16_t TF18_6          :1;
    uint16_t TF18_7          :1;
};

struct TF19_BIT
{
    uint16_t TF19_0          :1;
    uint16_t TF19_1          :1;
    uint16_t TF19_2          :1;
    uint16_t TF19_3          :1;
    uint16_t TF19_4          :1;
    uint16_t TF19_5          :1;
    uint16_t TF19_6          :1;
    uint16_t TF19_7          :1;
};

struct TF20_BIT
{
    uint16_t TF20_0          :1;
    uint16_t TF20_1          :1;
    uint16_t TF20_2          :1;
    uint16_t TF20_3          :1;
    uint16_t TF20_4          :1;
    uint16_t TF20_5          :1;
    uint16_t TF20_6          :1;
    uint16_t TF20_7          :1;
};

struct TF21_BIT
{
    uint16_t TF21_0          :1;
    uint16_t TF21_1          :1;
    uint16_t TF21_2          :1;
    uint16_t TF21_3          :1;
    uint16_t TF21_4          :1;
    uint16_t TF21_5          :1;
    uint16_t TF21_6          :1;
    uint16_t TF21_7          :1;
};

struct TF22_BIT
{
    uint16_t TF22_0          :1;
    uint16_t TF22_1          :1;
    uint16_t TF22_2          :1;
    uint16_t TF22_3          :1;
    uint16_t TF22_4          :1;
    uint16_t TF22_5          :1;
    uint16_t TF22_6          :1;
    uint16_t TF22_7          :1;
};

struct TF23_BIT
{
    uint16_t TF23_0          :1;
    uint16_t TF23_1          :1;
    uint16_t TF23_2          :1;
    uint16_t TF23_3          :1;
    uint16_t TF23_4          :1;
    uint16_t TF23_5          :1;
    uint16_t TF23_6          :1;
    uint16_t TF23_7          :1;
};

struct TF24_BIT
{
    uint16_t TF24_0          :1;
    uint16_t TF24_1          :1;
    uint16_t TF24_2          :1;
    uint16_t TF24_3          :1;
    uint16_t TF24_4          :1;
    uint16_t TF24_5          :1;
    uint16_t TF24_6          :1;
    uint16_t TF24_7          :1;
};

struct TF25_BIT
{
    uint16_t TF25_0          :1;
    uint16_t TF25_1          :1;
    uint16_t TF25_2          :1;
    uint16_t TF25_3          :1;
    uint16_t TF25_4          :1;
    uint16_t TF25_5          :1;
    uint16_t TF25_6          :1;
    uint16_t TF25_7          :1;
};



#if	defined(BTCS_TCMF_CONTROL) /* TAG 1 */
struct TCS_WL_STRUCT
{
    unsigned VCA_TCMF_CTL:            	  1;
    unsigned lctcsu1VCAPrevCtrlFlag:      1;
    unsigned ltcsu1VCAOffBySpdDiffWhl:    1;	
    unsigned lctcsu1BTCSWhlAtv:    	  	  1;
    unsigned lctcsu1BTCSWhlAtvOld:    	  1;
    unsigned lctcsu1BTCSWhlRsgEdg:	  	  1;
    unsigned lctcsu1ESCWhlAtv:		  	  1;
    unsigned BTCS_Entry_Inhibition_BySWESC: 1;    	
    unsigned BTCS_Entry_Inhibition_BySCESC: 1;    	
    unsigned BTCS_Entry_Inhibition_ByESC: 1;    	
    unsigned lctcsu1BrkTqFactorModeInESC: 1;    		    	
    unsigned ltcsu1VCAOffByYawRate: 1;  	
    
    int16_t lctcss16BTCSWhlOnTime;
    int16_t lctcss16WhlSpinF;
    int16_t lctcss16WhlSpinNF;
    int16_t ltcss16WheelSpeedBuffer[32];
    int32_t ltcss32SumOfMovingWindow;    
    int16_t ltcss16MovingAvgWhlSpdTCMF;
    int16_t ltcss16MovingAvgWhlSpdTCMFOld;
	int16_t ltcss16MovingAvgWhlSpdTCMFDiff;
    int16_t ltcss16Index4MovingAvgTCMF;

	uint8_t lctcsu8VIBBrkCtlPeriod_tcmf;
    
    int16_t lctcss16BsTarBrkTorq4AsymSCtl;  
    int16_t lctcss16BsTarBrkTorq4SymSCtl;
    int16_t lctcss16BsTarBrkTorq4VCACtl;
    uint8_t lctcsu8HghMuWhlSpnCnt; 
    uint8_t lctcsu8StatOneWhlOnHghMu; 
    uint8_t lctcsu1OverBrk;
    uint8_t lctcsu8OverBrkCnt;
    int16_t lctcss16BsTarBrkTorq;
    int16_t lctcss16TarWhlPre;
    int16_t lctcss16EstWhlPre;
    
    uint8_t lctcsu1VIBFlag_tcmf;
    uint8_t lctcsu1VIBFallingold_tcmf;
    uint8_t lctcsu1VIBFalling_tcmf;
    uint8_t lctcsu1VIBRisingold_tcmf;
    uint8_t lctcsu1VIBRising_tcmf;
    uint8_t lctcsu8VIBWhlIncreseCnt_tcmf;
    uint8_t lctcsu8VIBWhlDecreseCnt_tcmf;
    uint8_t lctcsu8VIBFreqencyold_tcmf;
    uint8_t lctcsu8VIBCycleTime_tcmf;
    uint16_t lctcsu16VIBvrad_crtold_tcmf;
    uint16_t lctcsu16VIBWhlAmplitudeold_tcmf;
    uint16_t lctcsu16VIBWhlAmplitude_tcmf;
    uint16_t lctcsu16VIBvrad_crt_tcmf;
    uint16_t lctcsu16VIBvradErrvref_tcmf;
    uint8_t lctcsu1VIBAmplitudeUpdCnt_tcmf;
    uint8_t lctcsu1VIBCycleStartTime_tcmf;
    uint8_t lctcsu8VIBCycleTimecnt_tcmf;
    uint8_t lctcsu8VIBBrakeCtlCheck_tcmf;
    uint8_t lctcsu8VIBBrkCtlPeriodCnt_tcmf;
    uint8_t lctcsu1VIBBrkScanReset_tcmf;
    uint8_t lctcsu8VIBBrakeMode_tcmf;
    
    uint16_t lctcsu16VIBAmplitudeold_tcmf;
    uint16_t lctcsu16VIBAmplitude_tcmf;
    uint8_t lctcsu8VIBAmplitudeUpdCnt_tcmf;
    uint8_t lctcsu8VIBFreqency_tcmf;
    
    uint8_t lctcsu8VIBFlagCycleCnt_tcmf;
    uint8_t lctcsu8VIBFlagoutCycleCnt_tcmf;  
    int8_t lctcss8VIBTimeShift_tcmf;
    int8_t ltcss8HoppingCnt;
    int8_t ltcss8HoppingIndex;
    int8_t HoppingTCSDelayIndex;
    int16_t lctcss16BsTBrkTorq4Hopping;
    int16_t lctcss16BsTBrkTorq4HopBuf[25];
    int16_t lctcss16DelayBrkTrq4Hopping;
    int16_t lctcss16BsTarBrkTorq4HOPCtl;
    int16_t lctcss16BsTarBrkTrq4StuckCtl;
    
    int8_t	lctcss8BTCSEndTmr;
    
    int16_t lctcss16AsymSpnCtlThInRTA;
    int16_t lctcss16AsymSpnCtlThInESCBrk;
    int16_t lctcss16AsymSpnCtlTh;
    int16_t tcs_wl_tempW0;
};

struct TCS_AXLE_STRUCT 
{
	unsigned ltcsu1RearAxle:            		1;
    unsigned lctcsu1DctBTCSplitMuAxle:     		1;
    unsigned lctcsu1FlgOfAfterIIntAct:   		1;	
	unsigned lctcsu1HghMuWhlUnstbInLftWhlCtl:	1;
	unsigned lctcsu1HghMuWhlUnstbInRgtWhlCtl:	1;
	unsigned lctcsu1HuntingDcted:				1;
	unsigned lctcsu1OK2AdaptFF:					1;
	unsigned lctcsu1OK2BrkTrqMaxLim:			1;
	unsigned lctcsu1FFAdaptByHghMuWhlUnstab:	1;
	unsigned lctcsu1FFAdaptUpdateByHSS:			1;
	unsigned lctcsu1BrkTrqMax4AsymUpdated:		1;
	unsigned lctcsu1BrkTrqMaxLimited:			1;		
	unsigned lctcsu1FastDecPresInHghUstb:		1;
	unsigned lctcsu1LowSpinofCtrlAxle:          1;
	unsigned lctcsu1CtrlWLSpdCross:             1;	
	unsigned lctcsu1FlgOfHSSRecvry:             1;	
	unsigned lctcsu1FFAdaptByIIntReset:         1;
	unsigned lctcsu1L2SplitSuspectDcted:        1;	
	unsigned lctcsu1L2SplitSuspectDctedOld:     1;		
	unsigned lctcsu1BTCTwoOff:                  1;			
	unsigned lctcsu1GearSftFFBrkTorqStart:      1;	
	unsigned lctcsu1HghMuWhlUnstbAftFlg:        1;
			
		
	uint8_t lctcsu8StuckControlLeftStart;			
    int16_t	lctcss16DctBTCSplitMuEnterCnt; 
    int16_t	lctcss16DctBTCSplitMuExitCnt; 

    int16_t lctcss16BsTarBrkTorqAXLEdiff;
    int16_t lctcss16BsTarBrkTorqAXLEdiffFlt;
    int16_t lctcss16BsBrkTorqTh4SplitDct;
    int16_t lctcss16BsBrkTorqTh4SplitExit;
    int16_t lctcss16HSSAfterBsTorq;
    int16_t lctcss16SymSpinDctCnt;
   	   
   	  
	int16_t ltcss16MovAvgDiffLAndR; 
    int16_t ltcss16WhlSpinDiff;
    int16_t ltcss16WhlSpinAvg;
    int16_t	ltcss16WhlAccAvg4AxleRunCnt;
    int16_t	ltcss16WhlSpdAvgBuf[6];
    int16_t	ltcss16WhlAccAvgIndex;
    int16_t	ltcss16WhlAccAvg50msIndex;
    int16_t	ltcss16WhlAccAvg;
    int16_t ltcss16WhlSpinDiffNF;
    int16_t ltcss16WhlSpinDiffBuf[6];
    int16_t ltcss16WhlSpinDiffBufNF[6];
    int16_t ltcss16WhlSpinDiffIndex;
    int16_t ltcss16WhlSpinDiffB50msIndex; /*50ms 이전값 Index*/
    int16_t ltcss8SupCROfWSCnt; /*초기 50ms 까지 zero하여 의도치 않은 값 적용 방지 */
    int16_t lctcss16BrkCtlErr;
    int16_t ltcss16ChngRateWhlSpinDiff;   
    int16_t ltcss16ChngRateWhlSpinDiffNF;
    int16_t ltcss16MonTime4AsymSpnCtlStrt;
    int16_t ltcss16Cnt4AsymSpnCtlStrt;
    int16_t ltcss16MonTime4SymSpnCtlStrt;
    int16_t ltcss16Cnt4SymSpnCtlStrt;    
    int16_t ltcss16MonTime4LongSpnCtlStrt;
    int16_t ltcss16Cnt4AsymSpnLongCtlStrt;
    
    
    int32_t lctcss32IntgBrkCtlErr;
    int32_t lctcss32IntgBrkCtlErrOld;    
    int16_t lctcss16WhlRolR;
    int16_t lctcss16MassOfOneCorn;
    uint8_t ltcsu1ModOfAsymSpnCtl;  
    uint8_t ltcsu1ModOfAsymSpnCtlOld;  
    uint8_t ltcsu1ModOfAsymSpnCtlRsgEdg;          
    int16_t lctcss16EstBrkTorqOnLowMuWhl;
    int16_t lctcss16EstBrkTorqOnLowMuWhlMem;
    int16_t lctcss16AsmdMuDiffSt;
    int16_t lctcss16AsmdMuDiffHill;   
    int16_t lctcss16AsmdMuDiffTurn; 
    int16_t	lctcss16FFBrkTorq4Asym;
    int16_t lctcss16FFBrkTorq4AsymVib;
    int16_t lctcss16FFBrkTorqSt4Asym;
    int16_t lctcss16FFBrkTorqHill4Asym;
    int16_t lctcss16FFBrkTorqTurn4Asym;
    int16_t lctcss16NorFFBrkTorq4Asym;
    int16_t lctcss16NorFFBrkTorq4AsymOld;
    int16_t lctcss16NorFFBrkTorq4AsymOldMem;
    int16_t	lctcss16FBBrkTorq4Asym; 
    int16_t	lctcss16FBBrkTorq4AsymOld; 
    int16_t lctcss16BsTBrkTorq4Asym;
    int16_t lctcss16BsTBrkTorq4AsymVib;
    int16_t lctcss16BsTBrkTorq4Turn;
    int16_t lctcss16BsTBrkTorq4VCA;
    int16_t lctcss16BsTBrkTorq4AsymOld;
    int16_t lctcss16BsIG4ASFBCtrl;
    int16_t lctcss16IGF4ASFBCtrl;
    int16_t lctcss16IGF4ASFBCtrlSt;
    int16_t lctcss16IGF4ASFBCtrlTurn;
    int16_t lctcss16IG4ASFBCtrl;
    int8_t  ltcss8IIntAct;
    int8_t  ltcss8IIntActOld;
    int16_t lctcsu16CntofAfterIIntAct;
    int16_t  ltcss16SStateCnt; 
    int16_t lctcss16BsPG4ASFBCtrl;
    int16_t lctcss16PGF4ASFBCtrl;
    int16_t lctcss16PGF4ASFBCtrlSt;
    int16_t lctcss16PGF4ASFBCtrlTurn;
    int16_t lctcss16PG4ASFBCtrl;
    int16_t lctcss16PeffOnBsTBrkTorq4Asym;
    int16_t lctcss16IeffOnBsTBrkTorq4Asym;
    int16_t tcs_axle_tempW0;
    int16_t tcs_axle_tempW1;
    int16_t tcs_axle_tempW2;
    int16_t tcs_axle_tempW3;
    int16_t lctcsu1SymSpinDctFlg;
    int16_t lctcsu1SymSpinDctFlgOld;
    int16_t lctcss16BsTBrkTorq4Sym;
    int16_t lctcss16BsTBrkTorq4SymOld;
    int8_t lctcsu8WhlOnHghMu;
    int8_t lctcsu8WhlOnHghMuOld;
    int8_t lctcsu8StatWhlOnHghMu;
    int8_t lctcsu8StatWhlOnHghMuOld;
    uint8_t lctcsu1Any2HghMuWhlUnstb;
    uint8_t lctcsu1HghMuWhlUnstb2Any;
    int16_t lctcss16MemBsTBrkTorq4Asym;
    uint8_t lctcsu1FlgOfAfterHghMuUnstb;
    int16_t lctcss16MemBsTBrkRecTrq4HSSEnd;
    uint16_t lctcsu16CntAfterHghMuUnstb;
    uint8_t lctcsu1BrkCtlActAxle;
    uint8_t ltcsu8SpinDiffIncCnt;
    uint8_t ltcsu8SpinDiffDecCnt;
    uint8_t ltcsu1SpinDiffInc;
    uint8_t ltcsu1SpinDiffDec;  
    uint8_t lctcsu1MotorOFF4Noise;  
    uint8_t lctcsu1MotorOFF4NoiseCnt;
    
    int16_t ltcss16MtrVolt;
    uint8_t lctcsu1OverBrkAxleOld;
    uint8_t lctcsu1OverBrkAxle;
    int16_t S16TCSFadeOutDctCnt;
    int16_t lctcss16FadeOutCtrlCnt;
    
    int16_t S16TCSReapplyCnt;          
    uint8_t lctcsu1BrkCtlFadeOutModeFlag;
    int16_t	lctcsu1AsymSpnCtlFFmodeCnt;    
    int16_t lctcss16BsTarBrkTorqMaxST;
    int16_t lctcss16BsTarBrkTorqMaxTurn;
    int16_t lctcss16BsTBrkLimTorq4Asym;
    int16_t lctcss16BsTBrkTorq4Stuck;
    int16_t lctcss16WhlSpdAxleAvg;
    int16_t lctcss16WhlSpinAxleAvg;
    int16_t lctcss16StuckControlCnt;
    
    int16_t ltcss16MonTime4VCASpnCtlStrt;
    int16_t ltcss16Cnt4VCASpnCtlStrt;
    int16_t lctcss16BrkWrkUntHghMuWhlUnst;
    int16_t lctcss16AdaptedFFBrkTorq4Asym;
    int16_t lctcss16AdptFFBrkTorq4IIntReset;
    uint8_t	ltcsu8MtrFstRnTmrAftHghMuWhlStb;
    int16_t lctcss16BrkTrqMax4Asym;
    int16_t	lctcss16BrkTrqMaxLimTime;
    int16_t lctcss16VhSpdAtStrtBrkTrqMaxLim;
    int16_t lctcss16VhSpd3sAftBrkTrqMaxLim;
    int16_t lctcss16BsTBrkTorq4FadOut;
    int16_t lctcss16MemBsTBrkTorq4FadOut;
    
    int16_t lctcss16FadOutCtrlDeTqST;
    int16_t lctcss16FadOutCtrlDeTqTurn;
    int16_t lctcss16FadOutCtrlDeTq;
    
    int16_t lctcss16TarAxlePres;
    
	int16_t lctcss16CntAfterL2SplitSus;
	int16_t lctcss16MemFFBrkTorq4L2SpSus;    
	int16_t lctcss16FFBrkTorq4L2SpSus;
	int16_t lctcss16NorGearSftFFBrkTrqDif;
	int16_t lctcss16GearSftFFBrkTorqCnt;
	
	int16_t lctcss16HghMuUstbRepeatCycleOld;
	int16_t lctcss16HghMuUstbRepeatCycle;
	int16_t lctcss16HghMuWhlUnstbAftCnt;
	int16_t lctcss16HghMuUstbReptReduceCnt;
	
		#if  (__IDB_SYSTEM == ENABLE)
	unsigned lctcsu1BothDrvgWhlBrkCtlReq:       1;		
		#endif
};

struct TCS_CIRCUIT_STRUCT
{
	unsigned 	lctcsu1BrkCtlActCir:            1;
	unsigned	lctcsu1BrkCtlActCirOld:			1;
	unsigned	lctcsu1BrkCtlActCirRsgEdg:		1;	
	unsigned	lctcsu1EstPresLowFlag:		    1;	
	unsigned	lctcsu1IniTCVCtlMode:			1;
	unsigned	lctcsu1IniTCVCtlModeOld:		1;
	unsigned	lctcsu1IniTCVCtlModeFalEdg:		1;
	unsigned	ltcsu1CirModOfAsymSpnCtl:		1;
		
		
	int16_t lctcss16TarCirPres;
	int8_t	lctcss8IniTCVCtlModeCnt;
	int16_t lctcss16TCVCurrent;
	int16_t lctcss16TCVCurrentOld;
	int16_t	lctcss16TCVCurrentFlt;
	int16_t lctcss16IniTcCurnt;
    int16_t lctcss16EstCirPres;
	int16_t lctcss16InitMaxVCATCCurnt;

		#if  (__IDB_SYSTEM == ENABLE)
	int16_t lctcss16TarCirPresCmd;
	int16_t lctcss16TarCirPresCmdFlt;
	int16_t lctcss16CordTarCirPres;
	int16_t lctcss16NorTarCirPres;
	int16_t lctcss16NorTarCirPresOld;
	int16_t lctcss16TarCirPresCmdOld;
	int16_t lctcss16EstCirPres_2;
		#endif	
};


#endif		/* TAG 1 */

#define TFLAG0  *(uint8_t*)&TF0
#define TFLAG1  *(uint8_t*)&TF1
#define TFLAG2  *(uint8_t*)&TF2
#define TFLAG3  *(uint8_t*)&TF3
#define TFLAG4  *(uint8_t*)&TF4
#define TFLAG5  *(uint8_t*)&TF5
#define TFLAG6  *(uint8_t*)&TF6
#define TFLAG7  *(uint8_t*)&TF7
#define TFLAG8  *(uint8_t*)&TF8
#define TFLAG9  *(uint8_t*)&TF9
#define TFLAG10  *(uint8_t*)&TF10
#define TFLAG11  *(uint8_t*)&TF11
#define TFLAG12  *(uint8_t*)&TF12
#define TFLAG13  *(uint8_t*)&TF13
#define TFLAG14  *(uint8_t*)&TF14
#define TFLAG15  *(uint8_t*)&TF15
#define TFLAG16  *(uint8_t*)&TF16
#define TFLAG17  *(uint8_t*)&TF17
#define TFLAG18  *(uint8_t*)&TF18
#define TFLAG19  *(uint8_t*)&TF19
#define TFLAG20  *(uint8_t*)&TF20
#define TFLAG21  *(uint8_t*)&TF21
#define TFLAG22  *(uint8_t*)&TF22
#define TFLAG23  *(uint8_t*)&TF23

#define ABS_ON            TF0.TF0_0
#define FTCS_ON           TF0.TF0_1
#define ENG_STALL         TF0.TF0_2
#define AFTER_BTC         TF0.TF0_3
#define MU_SPLIT          TF0.TF0_4
#define TRACE_ON          TF0.TF0_5
#define ETCS_ON           TF0.TF0_6
#define DRV_T_SMALL       TF0.TF0_7

#define AY_HIGH           TF1.TF1_0
#define AY_MID            TF1.TF1_1
#define AY_LOW            TF1.TF1_2
#define FTC_NEWSET        TF1.TF1_3
#define FTC_SET_HI        TF1.TF1_4
#define FTC_SLIP_INC      TF1.TF1_5
#define RESET_TORQUE      TF1.TF1_6
#define FTC_DETECT        TF1.TF1_7

#define TRACE_READY       TF2.TF2_0
#define OPT_TRACE         TF2.TF2_1
#define LEFT_TURN         TF2.TF2_2
#define OPEN_FLAG         TF2.TF2_3
#define LIM_HILL          TF2.TF2_4
#define FTCS_HOLD         TF2.TF2_5
#define TRACE_TC          TF2.TF2_6
#define CYCLE_2ND         TF2.TF2_7

#define DRAG_ON           TF3.TF3_0
#define GEAR_CH_TCS       TF3.TF3_1
#define TIRE_CAL          TF3.TF3_2
#define RIGHT_MU_HIGH     TF3.TF3_3
#define LEFT_MU_HIGH      TF3.TF3_4
#define BTCS_ON           TF3.TF3_5
#define AY_ON             TF3.TF3_6
#define lctcsu1DctSplitHillFlg   TF3.TF3_7

#define DETECT_MINI_TCS   TF4.TF4_0
#define MINI_LEFT         TF4.TF4_1
#define MINI_REAR         TF4.TF4_2
#define AYF_ZERO          TF4.TF4_3
#define HIGH_HOLD         TF4.TF4_4
#define EMS33L            TF4.TF4_5
#define EMS30L            TF4.TF4_6
#define EMS24L            TF4.TF4_7

#define CIRCLE_SML        TF5.TF5_0
#define CIRCLE_BIG        TF5.TF5_1
#define ESP_TCS_ON		  TF5.TF5_2
#define TCS_ON	          TF5.TF5_3
#define EMS37L            TF5.TF5_4
#define BACK_DIR          TF5.TF5_5
#define HILL_33UP         TF5.TF5_6
#define HILL_STAND        TF5.TF5_7

#define EMS23L            	TF6.TF6_0
#define EMS25L            	TF6.TF6_1
#define QUICK_OUT         	TF6.TF6_2
#define tempF3            	TF6.TF6_3
#define FUNCTION_LAMP_ON  	TF6.TF6_4
#define VCA_CTL_DIRECTION_FLG	TF6.TF6_5
#define ITM_4WD_LOCK      	TF6.TF6_6
#define DYAW_SIGNCHANGE_EEC	TF6.TF6_7

#define D_YAW_DEC         TF7.TF7_0
#define ALAT_POS          TF7.TF7_1
#define D_YAW_DIV         TF7.TF7_2
#define D_YAW_ON	      TF7.TF7_3
#define D_AY_ON           TF7.TF7_4
#define D_AY_DIV		  TF7.TF7_5
#define SEN_ADJ_TH        TF7.TF7_6
#define HIGH_TO_LOW       TF7.TF7_7

#define WGRD_DT_PRE_OVER_LF   TF8.TF8_0
#define WGRD_DT_PRE_OVER_RF   TF8.TF8_1
#define WGRD_ACCEL_LF     TF8.TF8_2
#define WGRD_ACCEL_RF	  TF8.TF8_3
#define WGRD_CONST_LF     TF8.TF8_4
#define WGRD_CONST_RF     TF8.TF8_5
#define WGRD_DT_LAST_OVER_LF   TF8.TF8_6
#define WGRD_DT_LAST_OVER_RF   TF8.TF8_7

#define TRACE_ON_START		TF9.TF9_0
#define TCS_ON_START		TF9.TF9_1
#define ESP_TCS_ON_START	TF9.TF9_2
#define TCS_FLAGS_RESERVED09_3	TF9.TF9_3	/* 051118 New BTCS Structure */
#define BTCS_ONE_SIDE_CTL	TF9.TF9_4
#define ETO_SUSTAIN_2SEC	TF9.TF9_5
#define ETO_SUSTAIN_1SEC	TF9.TF9_6
#define TORQUE_FAST_RECOVERY_AFTER_ETO			TF9.TF9_7

#define GAIN_ADD_IN_TURN		TF10.TF10_0
#define P_GAIN_WARNING			TF10.TF10_1
#define LEFT_FASTER_THAN_RIGHT	TF10.TF10_2
#define LEFT_WHEEL_IN_CTL_OLD	TF10.TF10_3
#define LEFT_WHEEL_IN_CTL		TF10.TF10_4
#define ETO_SUSTAIN_3SEC		TF10.TF10_5
#define VCA_ON					TF10.TF10_6		/* VCA */
#define VCA_ON_OLD				TF10.TF10_7

#if defined(__DURABILITY_TEST_ENABLE)
	#if __ECU==ESP_ECU_1
#define ESP_ON_OLD_FOR_ET		TF11.TF11_0
#define TCS_ON_OLD_FOR_ET		TF11.TF11_1
	#endif
#define ABS_ON_OLD_FOR_ET		TF11.TF11_2
#define HSA_ON_OLD_FOR_ET		TF11.TF11_3
#endif

#define TCS_FLAGS_RESERVED11_4				TF11.TF11_4		
#define TCS_FLAGS_RESERVED11_5		    TF11.TF11_5		
#define GOOD_TARGET_TRACKING_MODE	TF11.TF11_6  // 07SWD #3
#define ETO_SUSTAIN_6SEC		TF11.TF11_7

#define ENABLE_TCS_MINI_DETECT	TF12.TF12_0
#define TCS_MINI_TIRE_SUSPECT	TF12.TF12_1
#define ENG_RPM_LIMIT_INHIBIT_FLAG	TF12.TF12_2
#define TCS_FLAGS_RESERVED12_3		TF12.TF12_3 /* VCA_RR_CTL */
#define CBS_ON_OLD_FOR_ET		TF12.TF12_4
#define TCS_FLAGS_RESERVED12_5		TF12.TF12_5
#define TCS_FLAGS_RESERVED12_6  	TF12.TF12_6 /* #if __TCS_CTRL_AMNT_DEC_IN_GEAR_SHIFT */
#define lctcsu1QuitH2SplitSuspect	TF12.TF12_7

#define TCS_FLAGS_RESERVED13_0					TF13.TF13_0
#define TCS_FLAGS_RESERVED13_1					TF13.TF13_1
#define TCS_FLAGS_RESERVED13_2					TF13.TF13_2
#define TCS_FLAGS_RESERVED13_3					TF13.TF13_3
#define lctcsu1AllowedBrkSportsMode				TF13.TF13_4
#define lctcsu1FunctionLampReq					TF13.TF13_5
#define lctcsu1FuntionActivated					TF13.TF13_6
#define lctcsu1AllowedEngSportsMode				TF13.TF13_7

#define lctcsu1InhibitBrkCtrl4SYC				TF14.TF14_0
#define TCS_FLAGS_RESERVED14_1					TF14.TF14_1
#define TCS_FLAGS_RESERVED14_2					TF14.TF14_2
#define TCS_FLAGS_RESERVED14_3					TF14.TF14_3
#define TCS_FLAGS_RESERVED14_4					TF14.TF14_4
#define lctcsu1BackupTorqActivation				TF14.TF14_5
#define lctcsu1BackupTorqActivationold			TF14.TF14_6
#define lespu1SccActFlg							TF14.TF14_7

#if __MY08_TCS_DCT_L2SPLIT
#define lctcsu1BrkRight1WhlCtl					TF15.TF15_0
#define lctcsu1BrkLeft1WhlCtl					TF15.TF15_1
#define lctcsu1BrkRight2WhlCtl					TF15.TF15_2
#define lctcsu1BrkLeft2WhlCtl					TF15.TF15_3
#define lctcsu1Low2SplitSuspect					TF15.TF15_4
#define lctcsu1Low2SplitSuspectOld				TF15.TF15_5
#define lctcsu1Low2SplitSuspectREdge			TF15.TF15_6
#define lctcsu1Low2SplitSuspectFEdge			TF15.TF15_7
#else	/* #if __MY08_TCS_DCT_L2SPLIT */
#define TCS_FLAGS_RESERVED15_0					TF15.TF15_0
#define TCS_FLAGS_RESERVED15_1					TF15.TF15_1
#define TCS_FLAGS_RESERVED15_2					TF15.TF15_2
#define TCS_FLAGS_RESERVED15_3					TF15.TF15_3
#define TCS_FLAGS_RESERVED15_4					TF15.TF15_4
#define TCS_FLAGS_RESERVED15_5					TF15.TF15_5
#define TCS_FLAGS_RESERVED15_6					TF15.TF15_6
#define TCS_FLAGS_RESERVED15_7					TF15.TF15_7
#endif	/* #if __MY08_TCS_DCT_L2SPLIT */

#if __MY08_TCS_DCT_L2SPLIT
#define lctcsu1BrkRightWhlSpin					TF16.TF16_0
#define lctcsu1BrkLeftWhlSpin					TF16.TF16_1
#define lctcsu1BrkRightWhlSpinOld				TF16.TF16_2
#define lctcsu1BrkLeftWhlSpinOld				TF16.TF16_3
#define ldtcsu1HillDetectionByAx				TF16.TF16_4
#define lctcsu1BTCSActInLow2Split				TF16.TF16_5
#define TCS_FLAGS_RESERVED16_6					TF16.TF16_6
#define TCS_FLAGS_RESERVED16_7					TF16.TF16_7
#else	/* #if __MY08_TCS_DCT_L2SPLIT */
#define TCS_FLAGS_RESERVED16_0					TF16.TF16_0
#define TCS_FLAGS_RESERVED16_1					TF16.TF16_1
#define TCS_FLAGS_RESERVED16_2					TF16.TF16_2
#define TCS_FLAGS_RESERVED16_3					TF16.TF16_3
#define TCS_FLAGS_RESERVED16_4					TF16.TF16_4
#define TCS_FLAGS_RESERVED16_5					TF16.TF16_5
#define TCS_FLAGS_RESERVED16_6					TF16.TF16_6
#define TCS_FLAGS_RESERVED16_7					TF16.TF16_7
#endif	/* #if __MY08_TCS_DCT_L2SPLIT */

#if __MY08_TCS_PREVENT_SPIN_OVERSHOOT
#define lctcsu1SpinOverShoot					TF17.TF17_0
#define lctcsu1SpinOverShootOld					TF17.TF17_1
#else
#define TCS_FLAGS_RESERVED17_0					TF17.TF17_0
#define TCS_FLAGS_RESERVED17_1					TF17.TF17_1
#endif

#define lctcsu1OverMaxRPM		         		TF17.TF17_2
#define lctcsu1UnderMinRPM			            TF17.TF17_3
#define TCS_FLAGS_RESERVED17_4					TF17.TF17_4
#define TCS_FLAGS_RESERVED17_5					TF17.TF17_5
#define TCS_FLAGS_RESERVED17_6					TF17.TF17_6
#define TCS_FLAGS_RESERVED17_7			    TF17.TF17_7


#define TCS_FLAGS_RESERVED18_0					TF18.TF18_0
#define TCS_FLAGS_RESERVED18_1					TF18.TF18_1
#define TCS_FLAGS_RESERVED18_2					TF18.TF18_2
#define TCS_FLAGS_RESERVED18_3					TF18.TF18_3
#define TCS_FLAGS_RESERVED18_4					TF18.TF18_4

#if __MY08_TCS_DISCRETE_ENG_CTL
#define ltcsu1EmergencyHoldReset				TF18.TF18_5
#else
#define TCS_FLAGS_RESERVED18_5					TF18.TF18_5
#endif

#if __MY08_TCS_TORQUE_LEVEL_SEARCH
#define ltcsu1FastTorqReduction					TF18.TF18_6
#define ltcsu1FastTorqRecovery					TF18.TF18_7
#else	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */
#define TCS_FLAGS_RESERVED18_6					TF18.TF18_6
#define TCS_FLAGS_RESERVED18_7					TF18.TF18_7
#endif	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */

#if __MY08_TCS_TORQUE_LEVEL_SEARCH
#define ltcsu1FastTorqRecoverySus56ms			TF19.TF19_0
#else	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */
#define TCS_FLAGS_RESERVED19_0					TF19.TF19_0
#endif	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */

#if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC
#define ltcsu1TorqLvlRecoverAfterEEC			TF19.TF19_1
#else	/* #if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC */
#define TCS_FLAGS_RESERVED19_1					TF19.TF19_1
#endif	/* #if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC */

#if __MY08_TCS_DCT_H2L
#define ltcsu1DctH2L							TF19.TF19_2
#define ltcsu1HighMuSuspectForH2L				TF19.TF19_3
#else	/* #if __MY08_TCS_DCT_H2L */
#define TCS_FLAGS_RESERVED19_2					TF19.TF19_2
#define TCS_FLAGS_RESERVED19_3					TF19.TF19_3
#endif	/* #if __MY08_TCS_DCT_H2L */

#define ENG_STALL_OLD							TF19.TF19_4
#define ltcsu1Homo2SplitAct						TF19.TF19_5
#define ltcsu1AccROK							TF19.TF19_6
#define ltcsu1HomoMuCtlHavePriority				TF19.TF19_7

#define TCS_STUCK_ON							TF20.TF20_0
#define TCS_STUCK_SPIN_ADDED					TF20.TF20_1
#define TCS_FLAGS_RESERVED20_2					TF20.TF20_2
#define lctcsu1Dct4WDStuckByDiaWhlSpin			TF20.TF20_3
#define lctcsu1SpinOverThresholdOld				TF20.TF20_4
#define lctcsu1SpinOverThreshold				TF20.TF20_5
#define lctcsu1CtlRefFasterWhl					TF20.TF20_6
#define lctcsu1SteerToZero						TF20.TF20_7

#define lctcsu1FRspinInFLctl					TF21.TF21_0
#define lctcsu1FLspinInFRctl					TF21.TF21_1
#define lctcsu1RRspinInRLctl					TF21.TF21_2
#define lctcsu1RLspinInRRctl					TF21.TF21_3
#define TCS_FLAGS_RESERVED21_4					TF21.TF21_4	/* #if __TCS_INDIV_WHEEL_CTRL_IN_BTCS */
#define ENABLE_TCS_MINI_DETECT_2WL				TF21.TF21_5	/* #if 2WL Mini Tire Detection */
#define lctcsu1FastRPMInc						TF21.TF21_6	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */
#define TCS_FLAGS_RESERVED21_7					TF21.TF21_7	/* #if __MY08_TCS_TRANSITION2STATE1 */

#define TCS_FLAGS_RESERVED22_0					TF22.TF22_0 /* #if __MY08_TCS_TRANSITION2STATE1 */
#define TCS_FLAGS_RESERVED22_1					TF22.TF22_1 /* #if __MY08_TCS_TRANSITION2STATE1 */
#define TCS_FLAGS_RESERVED22_2		     		TF22.TF22_2 /* #if __MY08_TCS_TRANSITION2STATE1 */
#define lctcsu1DrivelineProtectionOn			TF22.TF22_3 /* #if __MY08_TCS_TRANSITION2STATE1 */

#define lctcsu1FLMuHigh							TF22.TF22_4 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1FRMuHigh							TF22.TF22_5 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1RLMuHigh							TF22.TF22_6 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1RRMuHigh							TF22.TF22_7 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1FLMuHighOld						TF23.TF23_0 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1FRMuHighOld						TF23.TF23_1 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1RLMuHighOld						TF23.TF23_2 /* #if __TCS_HUNTING_DETECTION */
#define lctcsu1RRMuHighOld						TF23.TF23_3 /* #if __TCS_HUNTING_DETECTION */

#define ltcsu1StopBumpDetection					TF23.TF23_4 /* #if __TCS_BUMP_DETECTION */
#define ltcsu1BumpDetect						TF23.TF23_5 /* #if __TCS_BUMP_DETECTION */
#define TCS_MINI_TIRE_SUSPECT_IN_TCS			TF23.TF23_6 /* #if 2WL Mini Tire Detection */
#define TCS_MINI_TIRE_SUSPECT_IN_TCS_OLD		TF23.TF23_7 /* #if 2WL Mini Tire Detection */

#endif

#define lctcsu1HillDctbyAxSensor	TF25.TF25_0 
#define TFD_Entry_OKCondition	    TF25.TF25_1
#define ETO_SUSTAIN_TIME     	    TF25.TF25_2
#define lctcsu1EECActSusFlag	    TF25.TF25_3
        	
#define lctcsu1EECAfterFastExitFlg	TF25.TF25_4
#define ldtcs1stAxDropFlag			TF25.TF25_5
#define TCS_FLAGS_RESERVED25_6	TF25.TF25_6
#define TCS_FLAGS_RESERVED25_7		TF25.TF25_7
