#ifndef __LCDECCALLCONTROL_H__
#define __LCDECCALLCONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition**************************/
  #if __DEC
#define     S8_DEC_STANDBY                      1
#define     S8_DEC_ACTIVATION                   2
#define     S8_DEC_CLOSING                      3
#define     S8_DEC_TERMINATION                  4

#define     __Decel_Ctrl_Integ					1

  #endif

/*Global MACRO FUNCTION Definition**************************/
  #if __DEC
#define     U16_DEC_NO_PRE_CUR                  	200
#define     U16_DEC_FEEDFORWARD_MIN             	L_U8_TIME_10MSLOOP_50MS
#define     S16_DEC_FFCTRL_ERROR_MAX				10
  #endif
/*Global Type Declaration **********************************/
  #if __DEC
/********** To */
extern struct  U8_BIT_STRUCT_t DEC00;
extern struct  U8_BIT_STRUCT_t DEC01;

#define lcu1decFFCtrl_flg              	DEC00.bit0
#define lcu1DecActiveFlg           	   	DEC00.bit1
#define lcu1DecCtrlReqFlg           	DEC00.bit2
#define lcu1DecFeedForwardReqFlg        DEC00.bit3
#define lcu1DecSmoothClosingReqFlg      DEC00.bit4
#define lcu1DecSmoothClosingEndFlg		DEC00.bit5
#define lcudecforcedMFCHoldFlg_p        DEC00.bit6
#define lcudecforcedMFCHoldFlg_s        DEC00.bit7

#define lcu1DecPriESVCloseflg           DEC01.bit0
#define lcu1DecSecESVCloseflg           DEC01.bit1
#define lcu1DecActiveFlgOld             DEC01.bit2
#define lcu1dec_not_used_1_3            DEC01.bit3
#define lcu1dec_not_used_1_4            DEC01.bit4
#define lcu1dec_not_used_1_5            DEC01.bit5
#define lcu1dec_not_used_1_6            DEC01.bit6
#define lcu1dec_not_used_1_7            DEC01.bit7

typedef struct
{
    uint8_t   lcu8DecCtrlReq;
    uint8_t   lcu8DecFeedForwardReq;
    uint8_t   lcu8DecSmoothClosingReq;
    int16_t     lcs16DecReqTargetG;

}DEC_CTRL_t;
extern DEC_CTRL_t clDecHdc, clDecAcc, clDecEpb, clDecTsp;
  #endif

/*Global Extern Variable  Declaration***********************/

  #if __DEC
 
extern int16_t	lcs16DecCtrlInput;
extern int8_t   lcs8DecState;
extern uint16_t   lcu16DecObserver, lcu16DecFeedForwardCnt, lcu16DecFeedForwardTh;
extern uint16_t   lcu16DecTimeToAct, lcu16DecClosingCnt, lcu16HdcTimeToAct;
extern int16_t    lcs16DecClosingCur;
extern int16_t    lcs16DecDecel, lcs16DecCtrlError, lcs16DecTargetG;
extern int16_t    lcs16DecCtrlInputFilt, lcs16DecCtrlInputFiltOld, lcs16DecCtrlErrorDot;
extern int16_t 	lcs16DecRoughCnt;
extern uint16_t    lcu16DecClosingNum; 

/********** From */
  #endif

/*Global Extern Functions  Declaration**********************/
  #if __DEC
extern void LCDEC_vCallControl(void);
  #endif
#endif