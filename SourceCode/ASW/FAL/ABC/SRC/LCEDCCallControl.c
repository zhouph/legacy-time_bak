/*******************************************************************/
/*  Program ID: MGH-40 EDC 						                   */
/*  Program description:. Engine Drag Control   		           */
/*  Input files: LSENGCallSensorSignal.C,esp_can.c,Speed_Calc.c,   */
/*				 Logic.c,Logic1.c,error.c,  					   */
/*				 Signal_conditioning.c,Ftcs.c,esp_ec.c,			   */
/*               ESP_Sensor_Processing.c						   */
/*  Output files: LDEDCCallDetection.C,LDENGCallDetection.C,	   */
/*				  LDENGEstimatPowertrain.C,LCEDCCallControl.C,     */
/*                LAEDCCallActCan.C                                */
/*  Special notes: none                                            */
/*******************************************************************/
/*  Modification Log                                               */
/*  Date        Author           Description                       */
/*  ---         ------           ---------                         */
/*  05.05.10    Jinhyuk Huh      Initial Release                   */
/*******************************************************************/

/* Includes ********************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCEDCCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCEDCCallControl.h"
#include "LDENGEstPowertrain.h"
#include "LDEDCCallDetection.h"
#include "LAABSCallActCan.h"


/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/
#if __EDC

void LCEDC_vCallControl(void);
void LCEDC_vControlEngDrag(void);
void LCEDC_vInhibitEdc(void);
void LCEDC_vExitEdc(void);
void LCEDC_vExitNorEdc(struct W_STRUCT *WL);
void LCEDC_vInitializeEdc(void);
void LCEDC_vEDCFadeOutmode(void);
void LCEDC_vCalSlip(void);
void LCEDC_vCalTargetSlip(void);
void LCEDC_vCalError(void);
void LCEDC_vCalPDGain(void);
void LCEDC_vCallPDController(void);
void LCEDC_vLimitTorq(void);

#endif

/* Implementation***************************************************/

#if __EDC   /* TAG1 */

void LCEDC_vCallControl(void)                   
{

#if __EDC_ENABLE_BY_CALIBRATION == ENABLE
  	
	if(LOOP_TIME_20MS_TASK_0==1)
	{
		if(U8_EDC_SYS_ENABLE>0)
		{
			lcedcu1CtrlEnable = 1;
		}
		else
		{
			lcedcu1CtrlEnable = 0;
		}
	}
	
  if(lcedcu1CtrlEnable==1)
#endif  

  {
  	
    ABS_ON_flag  = ABS_fz;
	if(Engine_braking_flag==1) 
	{
		ABS_ON_flag=0;    
	}
	  #if ((GMLAN_ENABLE==ENABLE)||(__CAR==GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)) && __VDC
	else if(laabsu1TrnsBrkSysCltchRelRqd==0)
	{
		ABS_ON_flag=0;    
	}
	  #endif /*(GMLAN_ENABLE==ENABLE)*/
	else
	{
		;
	}			
    
    LCEDC_vCalSlip();
	LCEDC_vCalTargetSlip();
	LCEDC_vCalError();
	LDEDC_vDetectHighMu();
	if(LOOP_TIME_20MS_TASK_0==1)
	{
		LCEDC_vCalPDGain();
	}
	else
	{}		
		#if __EDC_TEMP_TIRE_DETECTION	
	LDEDC_vEnableMiniWheelDetect();
		#endif
    LDEDC_vParkingBrakeOperation();
    	#if __EDC_GEAR_SHIFT_DOWN_DETECTION
	LDEDC_vDetectGearShiftDown();
		#endif
    	#if __EDC_EXIT_BY_EXCESSIVE_TORQ_UP
	LDEDC_vDetectExcessiveTorqUp();
		#endif
	LCEDC_vInhibitEdc();
	LDEDC_vDetectNorEdc();
	
	if(EDC_INHIBIT_flag==0)
	{
		if(EDC_ON==0) 
       	{
       		EDC_flags=1;
          	LCEDC_vInitializeEdc();
          	LDEDC_vCallDetection();
    	}
		else 
		{ 
        	LCEDC_vExitEdc();			
        }
        
        if(EDC_ON==1) 
        {
        	LCEDC_vControlEngDrag();
        }
        else
        {
        	;
        }	
    }
	else
	{
		if(EDC_ON==1)
		{
              #if !__4WD
               	#if __REAR_D
            LCEDC_vExitNorEdc(&RL);
            LCEDC_vExitNorEdc(&RR);
               	#else
            LCEDC_vExitNorEdc(&FL);
            LCEDC_vExitNorEdc(&FR);
               	#endif
              #else
            LCEDC_vExitNorEdc(&FL);
            LCEDC_vExitNorEdc(&FR);
            LCEDC_vExitNorEdc(&RL);
            LCEDC_vExitNorEdc(&RR);
              #endif
              	
			if(MSR_cal_torq>drive_torq_old)
			{
				if(EDC_NOR_EXIT==1)
				{
    				if(lcedcs16AvgDrivenWheelSlip<-VREF_0_25_KPH)
    				{ 					
    					if(EDC_SOLO_ON==1)
    					{
    						lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_ESO;			
    					}
    					else if(EDC_ABS_ON==1)	
    					{
    						lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_EAO;
    					}
    				    	#if __VDC
    					else if(EDC_ESP_ON==1)
    					{
    						lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_EEO;
    					}
    						#endif
    					else
    					{
    						;
    					}
  	  				}
    				else
    				{
    					if(MSR_cal_torq>fric_torq)
    					{
    						lcedcs16EDCTorqDecRate = U8_EDC_TORQ_FAST_DEC_RATE_EXIT;
    					}
    					else
    					{
    						lcedcs16EDCTorqDecRate = U8_EDC_TORQ_SLOW_DEC_RATE_EXIT;
    					}
    				}    											
					LCEDC_vEDCFadeOutmode();
				}
				else
				{
					LCEDC_vEDCFadeOutmode();
				}
			}
			else
			{
				LCEDC_vInitializeEdc();
				EDC_NOR_ON_FLAG=0;
				FL.ldedcu1EdcNorOn=FR.ldedcu1EdcNorOn=RL.ldedcu1EdcNorOn=RR.ldedcu1EdcNorOn=ldedcu1EdcNorOn_wl=0;
           		EDC_NOR_EXIT=0;
           		FL.lcedcu1EdcWheelExit=FR.lcedcu1EdcWheelExit=RL.lcedcu1EdcWheelExit=RR.lcedcu1EdcWheelExit=lcedcu1EdcWheelExit_wl=0;            						
			}
		}
		else
		{
			FL.ldedcu1EdcNorOn=FR.ldedcu1EdcNorOn=RL.ldedcu1EdcNorOn=RR.ldedcu1EdcNorOn=ldedcu1EdcNorOn_wl=0;
		}		
	}
	
	LDEDC_vCount1SecAfterEDC();

  } /* if(lcedcu1CtrlEnable==1) */

}

void LCEDC_vControlEngDrag(void) 
{
	LCEDC_vCallPDController();
    LCEDC_vLimitTorq();    
}

void LCEDC_vInhibitEdc(void)
{
	lcedcs16EDCTorqDecRate = U8_EDC_TORQ_FAST_DEC_RATE_EXIT;
	ldedcu1EDCFailMode=0;

	  #if (NEW_FM_ENABLE==ENABLE)
	lcu1EdcCtrlFail=0;
	  #endif 

#if ( NEW_FM_ENABLE==ENABLE )

	if( ( fu1FMSolenoidErrDet==1)||(fu1FMESCSolErrDet==1)||(fu1FMFrontSolErrDet==1)||(fu1FMRearSolErrDet==1)||(fu1FMABSSolErrDet==1) 
			||(fu1FMMotorErrDet==1)||(fu1FMECUHwErrDet==1)||(fu1FMVoltageOverErrDet==1)||(fu1FMVoltageUnderErrDet==1)||(fu1FMVoltageLowErrDet) 
			||(lcu1AbsCtrlFail==1) || (ee_ece_flg==1) )
	{/* motor/pump, 전륜 코일, 후륜 코일, MCU/Relay Error, 저전압/고전압/약저전압, ESC sol v/v error, Brake fluid level error, ABS_ERROR_FLAG*/
		EDC_Inhibit_flags=10;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;
	}
	else if( (fu1FMWheelFLErrDet==1)||(fu1FMWheelFRErrDet==1)||(fu1FMWheelRLErrDet==1)||(fu1FMWheelRRErrDet==1)
			||(fu1FMWheelFrontErrDet==1)||(fu1FMWheelRearErrDet==1)||(fu1FMSameSideWSSErrDet==1)||(fu1FMDiagonalWSSErrDet==1) )
	{	/* Wheel Speed Sensor Fail*/
		EDC_Inhibit_flags=11;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;
	}
	else if( (fu1FMVRErrDet==1))
	{/* valve relay, */
		EDC_Inhibit_flags=21;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;	
	}
	else if( (ccu1EMS_N_ErrFlg==1)||(ccu1EMS_TQI_ErrFlg==1)||(ccu1TQI_TARGET_ErrFlg==1)||(ccu1EMS_TQI_ACOR_ErrFlg==1)||(ccu1EMS_PV_AV_CAN_ErrFlg==1)||(ccu1EMS_TPS_ErrFlg==1)  
			)
	{/*N(engine rpm), TQFR (fric_torq), TQI (drive_torq ), TQI_TARGET (drive_torq ), TQI_ACOR (eng_torq), PV_AV_CAN( tps )*/
		EDC_Inhibit_flags=12;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;
	}
	else if((ccu1EngVar_ErrFlg==1)||(ccu1TCU_G_SEL_DISP_ErrFlg==1)||(ccu1TCU_TAR_GC_ErrFlg==1)
					#if(__CAR_MAKER!=SSANGYONG)
					||(ccu1TCU_N_TC_ErrFlg==1)
					#endif
					) 
	{/*CONF_TCU  (ENG_VAR),G_SEL_DISP, N_TC ( tc_rpm, turbine_rpm ), CF_Tcu_TarGr */
		EDC_Inhibit_flags=22;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;
	}
	else if( (fu1FMVCErrDet==1) )
	{
		EDC_Inhibit_flags=23;
		lcu1EdcCtrlFail=1;	
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;	
	}
	#if ( __4WD_VARIANT_CODE == ENABLE)     /*Longitudinal G sensor Error*/
	else if( (fu1FMAxErrDet==1) && (lsu8DrvMode !=DM_2WD) )
	{ /* Variant Code Vehicle(2H,4H,4L,AUTO) */
		EDC_Inhibit_flags=24;
		lcu1EdcCtrlFail=1;
		EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;	
	}
	#else
	 #if (__4WD==ENABLE)	/*Longitudinal G sensor Error*/
	else if( (fu1FMAxErrDet==1) )
	{/*__4WD*/
		EDC_Inhibit_flags=24;
		lcu1EdcCtrlFail=1;
    	EDC_INHIBIT_flag=1;
		ldedcu1EDCFailMode=1;
	}
	 #endif/*__4WD*/
	#endif /*__4WD_VARIANT_CODE*/
	
#else /*NEW_FM_ENABLE*/

	  #if __GM_FailM
    if( (fu1MotErrFlg==1)||(fu1RearSolErrDet==1)||(fu1FrontSolErrDet==1)||(fu1ECUHwErrDet==1)
       ||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1VoltageLowErrDet==1) ||(fu1ESCSolErrDet==1)
         #if __BRAKE_FLUID_LEVEL==ENABLE
	   ||(fu1DelayedBrakeFluidLevel==1)
	   ||(fu1LatchedBrakeFluidLevel==1)	/* 090901 for Bench Test */	   
		 #endif
      )
	   /* motor/pump, 전륜 코일, 후륜 코일, MCU/Relay Error, 저전압/고전압/약저전압, ESC sol v/v error, Brake fluid level error*/
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=10;
    	ldedcu1EDCFailMode=1;		
	}
	else if( (fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||(fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)
		     ||(fu1SameSideWSSErrDet==1)||(fu1DiagonalWSSErrDet==1)  )			  
	   /* Wheel Speed Sensor Fail*/
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=11;
    	ldedcu1EDCFailMode=1;		
	}
	else if(fu1EMSTimeOutErrDet==1)
	   /* ECM Torque Control Fail OR Acceleration Effective Position Fail */
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=12;
    	ldedcu1EDCFailMode=1;		
	}
	else if(fu1MainCanLineErrDet==1)
	   /* GMLAN(vehicle) fail */
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=13;
    	ldedcu1EDCFailMode=1;		
	}				
			#if __ECU==ESP_ECU_1
	else if(fu1TCSDisabledBySW==1)
	   /* ESC Switch Off (TCS Off or ESC Off -> EDC Off) */
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=14;
    	ldedcu1EDCFailMode=1;		
	}				
			#endif
	else if(fu1TCUTimeOutErrDet==1)
	   /* TCM Fail */
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=15;
    	ldedcu1EDCFailMode=1;		
	}
	else if(hs_subnet_config_err_flg==0)
	   /* HS Subnet Configuration Error -> A/T,M/T information is not match , 0 : error */
	{
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=16;
    	ldedcu1EDCFailMode=1;		
	}			
	  #else
    if( (fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1)||(fu1TCUTimeOutErrDet==1)
		||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1VoltageLowErrDet==1)
		||(abs_error_flg) 
	  )
    {  
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=1;
    	ldedcu1EDCFailMode=1;    	
    }
  	  #endif
#endif /*NEW_FM_ENABLE*/

	else if(vref<S16_EDC_INHIBIT_SPEED_IN_EDC)
	{
		EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=2;
	}
  	  #if __VDC
	else if((BTCS_ON)||(ESP_TCS_ON)||(ETCS_ON)||(FTCS_ON))
    {  
    	EDC_INHIBIT_flag=1;
    	EDC_Inhibit_flags=3;
    }
  	  #endif
    #if (NEW_FM_ENABLE==ENABLE)
    else if( (ccu1TCU_G_SEL_DISP_ErrFlg==0)&&(ccu1TCU_TAR_GC_ErrFlg==0)
			    	#if(__CAR_MAKER!=SSANGYONG)
			    	&&(ccu1TCU_N_TC_ErrFlg==0)
			    	#endif
    &&(AUTO_TM)&&((gs_pos==0) || (gs_pos==GEAR_POS_TM_R)) )
    #else
    else if((fu1TCUTimeOutErrDet==0)&&(AUTO_TM)&&((gs_pos==0) || (gs_pos==GEAR_POS_TM_R)))
    #endif
    {
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=4;
	}
#if __EDC_GEAR_SHIFT_DOWN_DETECTION
	else if((AUTO_TM==0)  
	#if (__GEAR_N_SWITCH_TYPE == ANALOG_TYPE )||(__GEAR_N_SWITCH_TYPE == CAN_TYPE )
	&& (((fu1GearNInvalid == 0) && (fu1GearNSwitch == 1)) || ((fu1GearNInvalid == 1) && (In_gear_flag==0)) )
	#else
	&& (In_gear_flag==0)
	#endif
	&& (U8_EDC_GSD_ENABLE_FLG==1) ) 
	{
		if(ldedcu1GearShiftDownSuspectFlag==1)
		{
    		EDC_INHIBIT_flag=0;
    		EDC_Inhibit_flags=0;
		}	
		else
		{		
    		EDC_INHIBIT_flag=1;
			EDC_Inhibit_flags=4;
		}	
	}
#endif	
#if __EDC_NEUTRAL_ABS_DETECTION
	else if(((AUTO_TM==0) 
	#if (__GEAR_N_SWITCH_TYPE == ANALOG_TYPE )||(__GEAR_N_SWITCH_TYPE == CAN_TYPE )
	&& (((fu1GearNInvalid == 0) && (fu1GearNSwitch == 1)) || ((fu1GearNInvalid == 1) && (In_gear_flag==0)) )
	#else
	&& (In_gear_flag==0) 
	#endif	
	&& ((EBD_RA==1)||(ABS_ON_flag==1))) &&  (U8_EDC_GSD_ENABLE_FLG==0))			
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=4;
	}
#endif			
	else if(mtp>U8_EDC_MTP_THR_FOR_EDC_INHIBIT)
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=5;
	}
	else if(eng_rpm>=U16_EDC_ENG_RPM_LIMIT)
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=6;
		lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_RPM;
	}	
    	#if ((__PARK_BRAKE_TYPE == CAN_TYPE) ||(__PARK_BRAKE_TYPE==ANALOG_TYPE)) 	/* Parking Brake Signal On */
    else if(((fu1ParkingBrakeSusDet==0)&&(fu1DelayedParkingBrakeSignal==1))||(ldedcu1ParkBrkSuspectFlag==1)) 	
    	#else
	else if(ldedcu1ParkBrkSuspectFlag==1)
		#endif		  
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=7; 
	}									
#if __EDC_NEUTRAL_PARKING_DETECTION		
	else if((ldedcu1WheelLockDetectFlag==1)
	#if (__GEAR_N_SWITCH_TYPE == ANALOG_TYPE )||(__GEAR_N_SWITCH_TYPE == CAN_TYPE )
	&& (((fu1GearNInvalid == 0) && (fu1GearNSwitch == 1)) || ((fu1GearNInvalid == 1) && (In_gear_flag==0))))	
	#else
	&& (In_gear_flag==0))
	#endif
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=8;
	}
#endif			
#if __EDC_MAX_TIME_LIMIT		
	else if(lcedcu1CtrlTimeOver==1)
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=100;
	}
#endif			
#if __EDC_EXIT_BY_EXCESSIVE_TORQ_UP
	else if(lcedcu1ExitByExcessiveTqUp==1)
	{
    	EDC_INHIBIT_flag=1;
		EDC_Inhibit_flags=101;
	}
#endif			
	else
    {
    	EDC_INHIBIT_flag=0;
    	EDC_Inhibit_flags=0;
    }
    if((wu8IgnStat==CE_OFF_STATE)||(fu1OnDiag==1)||(init_end_flg==0))
    {
        EDC_INHIBIT_flag=1;
        EDC_Inhibit_flags=9;
        ldedcu1EDCFailMode=1;
    }
}

void LCEDC_vExitEdc(void)
{
	if(EDC_NOR_ON_FLAG==1)
	{
          #if !__4WD
           	#if __REAR_D
        LCEDC_vExitNorEdc(&RL);
        LCEDC_vExitNorEdc(&RR);
           	#else
        LCEDC_vExitNorEdc(&FL);
        LCEDC_vExitNorEdc(&FR);
           	#endif
          #else
        LCEDC_vExitNorEdc(&FL);
        LCEDC_vExitNorEdc(&FR);
        LCEDC_vExitNorEdc(&RL);
        LCEDC_vExitNorEdc(&RR);
          #endif
          
    	if(MSR_cal_torq>drive_torq_old) 
    	{
				#if __REAR_D 
			if((RL.ldedcu1EdcNorOn==1) || (RR.ldedcu1EdcNorOn==1))
				#else
			if((FL.ldedcu1EdcNorOn==1) || (FR.ldedcu1EdcNorOn==1))
				#endif  
			{
				EDC_NOR_EXIT=0;
			}    	
			else
			{
				;
			}

    		if(EDC_NOR_EXIT==1)
    		{	
    			if(lcedcs16AvgDrivenWheelSlip<-VREF_0_25_KPH)
    			{ 
    				if(EDC_SOLO_ON==1)
    				{
    					lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_ESO;			
    				}
    				else if(EDC_ABS_ON==1)	
    				{
    					lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_EAO;
    				}
    					#if __VDC
    				else if(EDC_ESP_ON==1)
    				{
    					lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE_EXIT_EEO;
    				}
    					#endif
    				else
    				{
    					;
    				}	
    			}
    			else
    			{
    				if(MSR_cal_torq>fric_torq)
    				{
    					lcedcs16EDCTorqDecRate = U8_EDC_TORQ_FAST_DEC_RATE_EXIT;
    				}
    				else
    				{
    					lcedcs16EDCTorqDecRate = U8_EDC_TORQ_SLOW_DEC_RATE_EXIT;
    				}			
    			}    						
    			LCEDC_vEDCFadeOutmode();
    		}	
    		else
    		{
    			;
    		}				
    	}
    	else
    	{}
    	
    	if(MSR_cal_torq>drive_torq_old)
    	{
    		EDC_exit_counter=0;	
    		EDC_Exit_flags=3;	
    	}				 	
    	else 
    	{
    		MSR_cal_torq=drive_torq;
    		if(EDC_exit_counter<L_U8_TIME_10MSLOOP_50MS) 		/* 50ms */	//14
    		{
    			EDC_Exit_flags=1;
    			EDC_exit_counter++;
    		}
    		else
    		{
    			;
    		}	
    		
        	if (EDC_exit_counter>=L_U8_TIME_10MSLOOP_50MS) 	/* 50ms */ //14
        	{	
    			EDC_Exit_flags=2;
        		EDC_NOR_ON_FLAG=0;
        		EDC_exit_counter=0;
        		FL.ldedcu1EdcNorOn=FR.ldedcu1EdcNorOn=RL.ldedcu1EdcNorOn=RR.ldedcu1EdcNorOn=ldedcu1EdcNorOn_wl=0;
        		EDC_NOR_EXIT=0;
        		FL.lcedcu1EdcWheelExit=FR.lcedcu1EdcWheelExit=RL.lcedcu1EdcWheelExit=RR.lcedcu1EdcWheelExit=lcedcu1EdcWheelExit_wl=0;            		
        	}
        	else
        	{
        		;
        	}
    	}              
	}
	else
	{
		;
	}
			
	if(EDC_NOR_ON_FLAG==0)
	{
		EDC_SOLO_ON = 0;
		EDC_ABS_ON = 0;
			#if __VDC
		EDC_ESP_ON = 0;
			#endif
		EDC_ON=0;
   		EDC_NOR_EXIT=0;
   		FL.lcedcu1EdcWheelExit=FR.lcedcu1EdcWheelExit=RL.lcedcu1EdcWheelExit=RR.lcedcu1EdcWheelExit=lcedcu1EdcWheelExit_wl=0;            					
	}
	else
	{
		if(EDC_SOLO_ON==1)
    	{
    			#if __VDC
    		if(ABS_ON_flag || YAW_CDC_WORK)
    			#else
    		if(ABS_ON_flag)
    			#endif
    		{
    			EDC_EXIT=1;
    			EDC_Exit_flags=10;
    		}	
				#if __REAR_D 
			if((RL.ldedcu1EdcNorOn==1) || (RR.ldedcu1EdcNorOn==1))
				#else
			if((FL.ldedcu1EdcNorOn==1) || (FR.ldedcu1EdcNorOn==1))
				#endif  
			{
				EDC_EXIT=0;
			}    	
			else
			{
				;
			}    			
    	}
    	else
    	{
    		;
    	}		
 
    	if(EDC_EXIT==1)
    	{
    		if(MSR_cal_torq>drive_torq_old)
    		{
    			lcedcs16EDCTorqDecRate = U8_EDC_TORQ_DEC_RATE;
    			LCEDC_vEDCFadeOutmode();
    		}
    		else
    		{
                if(EDC_SOLO_ON==1)
            	{    			
					EDC_SOLO_ON=0;
					EDC_Exit_flags=13;
    			}
    			else
    			{
    				;
    			}
    			EDC_EXIT=0; 
				EDC_NOR_EXIT=0;
				LDEDC_vInitializeEdcDetection();
    		}
    	}
    	else
    	{
    		;
    	}
	}		
}

void  LCEDC_vExitNorEdc(struct W_STRUCT *WL)
{
	tempW4 = lcedcs16TargetWheelSlip + (int8_t)S8_EDC_EXIT_SPEED_THR;
	tempW5 = lcedcs16TargetWheelSlip + (int8_t)S8_EDC_EXIT_SPEED_THR_3G0;
	tempW6 = (int8_t)S8_EDC_EXIT_RELLAM_THR;
	tempW7 = (int8_t)S8_EDC_EXIT_RELLAM_THR_3G0;

		#if __EDC_TEMP_TIRE_DETECTION
	if (ldedcu1TempTireSuspect==1)		// 수정 필요
	{
		tempW4 = tempW4 - 16;
		tempW5 = tempW5 - 16;
		tempW6 = tempW6 - 3;
		tempW7 = tempW7 - 3;
	}
	else
	{}	
		#endif	

    if(WL->ldedcu1EdcNorOn == 1)
    {
   		if((WL->rel_lam >= tempW6)||(lcedcs16AvgDrivenWheelSlip>=tempW4)) 
    	{
    		if(WL->arad >= -(int8_t)ARAD_0G5)
    		{
            		WL->ldedcu1EdcNorOn=0;
            		WL->lcedcu1EdcWheelExit=1;            		
    		}
 			else
 			{}
    	}
   		else if((WL->rel_lam >= tempW7)||(lcedcs16AvgDrivenWheelSlip>=tempW5))
    	{
   			if(WL->arad >= (int8_t)ARAD_3G0)
   			{
            		WL->ldedcu1EdcNorOn=0;
            		WL->lcedcu1EdcWheelExit=1;            		
    		}
 			else
 			{}    		
        }
        else
        {}        	
	}
	else
	{}

        	
    	#if __REAR_D
	if(((RL.lcedcu1EdcWheelExit==1) && (RR.lcedcu1EdcWheelExit==1))||((RL.lcedcu1EdcWheelExit==1) && (RR.ldedcu1EdcNorOn==0))||((RR.lcedcu1EdcWheelExit==1) && (RL.ldedcu1EdcNorOn==0)))
		#else
	if(((FL.lcedcu1EdcWheelExit==1) && (FR.lcedcu1EdcWheelExit==1))||((FL.lcedcu1EdcWheelExit==1) && (FR.ldedcu1EdcNorOn==0))||((FR.lcedcu1EdcWheelExit==1) && (FL.ldedcu1EdcNorOn==0)))
		#endif		
	{
		EDC_NOR_EXIT =1;
		EDC_Exit_flags=4;
	}
	else
	{}	

}

void LCEDC_vCalSlip(void)
{
	#if __VDC
	tempW0 = vrad_crt_fl - wvref_fl;
	tempW1 = vrad_crt_fr - wvref_fr;
	tempW2 = vrad_crt_rl - wvref_rl;
	tempW3 = vrad_crt_rr - wvref_rr;
	#else
	tempW0 = vrad_crt_fl - vref;
	tempW1 = vrad_crt_fr - vref;
	tempW2 = vrad_crt_rl - vref;
	tempW3 = vrad_crt_rr - vref;
	#endif

    #if __REAR_D
        tempW7=(tempW2+tempW3)>>1;
    #else
        tempW7=(tempW0+tempW1)>>1;
    #endif

	lcedcs16AvgDrivenWheelSlipOld=lcedcs16AvgDrivenWheelSlip;   /* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
	lcedcs16AvgDrivenWheelSlip=tempW7;
		/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
    lcedcs16AvgDrvnWhlSlipDiffOld = lcedcs16AvgDrvnWhlSlipDiff;
	lcedcs16AvgDrvnWhlSlipDiff = lcedcs16AvgDrivenWheelSlip-lcedcs16AvgDrivenWheelSlipOld; 
    lcedcs16AvgDrvnWhlSlipDiff = (int16_t)( ( (((int32_t)lcedcs16AvgDrvnWhlSlipDiffOld)*(128-L_U8FILTER_GAIN_10MSLOOP_10HZ)) + (((int32_t)lcedcs16AvgDrvnWhlSlipDiff)*L_U8FILTER_GAIN_10MSLOOP_10HZ) ) / (128) ); 
		/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */	
}

void LCEDC_vCalTargetSlip(void)
{
	if(EDC_ABS_ON==1)							/*EDC_ABS Combination Control*/
	{
		tempW6 = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_TARGET_DELTA_V_ABS_S1,
										     U16_EDC_SPEED2, (int16_t)S8_EDC_TARGET_DELTA_V_ABS_S2,
											 U16_EDC_SPEED3, (int16_t)S8_EDC_TARGET_DELTA_V_ABS_S3,
											 U16_EDC_SPEED4, (int16_t)S8_EDC_TARGET_DELTA_V_ABS_S4);		
	}
		#if __VDC
	else if(EDC_ESP_ON==1)						/*EDC_ESC Combination Control*/
	{
		tempW6 = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_TARGET_DELTA_V_ESC_S1,
										     U16_EDC_SPEED2, (int16_t)S8_EDC_TARGET_DELTA_V_ESC_S2,
											 U16_EDC_SPEED3, (int16_t)S8_EDC_TARGET_DELTA_V_ESC_S3,
											 U16_EDC_SPEED4, (int16_t)S8_EDC_TARGET_DELTA_V_ESC_S4);	
	}			
		#endif
	else										/*EDC_SOLO Control*/
	{
		tempW6 = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)S8_EDC_TARGET_DELTA_V_S1,
										     U16_EDC_SPEED2, (int16_t)S8_EDC_TARGET_DELTA_V_S2,
											 U16_EDC_SPEED3, (int16_t)S8_EDC_TARGET_DELTA_V_S3,
											 U16_EDC_SPEED4, (int16_t)S8_EDC_TARGET_DELTA_V_S4);		
	}	
	
/*		#if __EDC_COMPENSATION_IN_TURN
	  #if __REAR_D
	if(vrad_crt_fl>=vrad_crt_fr)
	{		
		tempW3 = (vrad_crt_fl - vrad_crt_fr);
	}
	else
	{
		tempW3 = (vrad_crt_fr - vrad_crt_fl);
	}			
	  #else  // FWD 
	if(vrad_crt_rl>=vrad_crt_rr)
	{		
		tempW3 = (vrad_crt_rl - vrad_crt_rr);
	}
	else
	{
		tempW3 = (vrad_crt_rr - vrad_crt_rl);
	}	
	  #endif 
//	if(tempW3>=VREF_1_KPH)
	if((tempW3>=VREF_1_KPH)&&(ABS_fz==0)&&(YAW_CDC_WORK==0))	
	{	  
		tempW6 = tempW6 -  tempW3;
	}
	else{}	
		#endif 	// __EDC_COMPENSATION_IN_TURN 
*/

				#if __EDC_COMPENSATION_IN_TURN
			#if !__SIDE_VREF	
	  #if __REAR_D
	if(vrad_crt_fl>=vrad_crt_fr)
	{		
		tempW3 = (vrad_crt_fl - vrad_crt_fr);
	}
	else
	{
		tempW3 = (vrad_crt_fr - vrad_crt_fl);
	}			
	  #else /* FWD */
	if(vrad_crt_rl>=vrad_crt_rr)
	{		
		tempW3 = (vrad_crt_rl - vrad_crt_rr);
	}
	else
	{
		tempW3 = (vrad_crt_rr - vrad_crt_rl);
	}	
	  #endif 	  
			#else /* __SIDE_VREF */
	tempW3 = McrAbs(vref_L-vref_R);
			#endif 

	if(tempW3 > VREF_5_KPH)
	{
		tempW3 = VREF_5_KPH;
	}
	
/*	if(tempW3>=VREF_1_KPH) 
	if((tempW3>=VREF_1_KPH)&&(ABS_fz==0)&&(YAW_CDC_WORK==0))	*/	
	#if __VDC
	if((ABS_fz==0)&&(YAW_CDC_WORK==0))
	#else
	if(ABS_fz==0)
	#endif
	{	
		if(tempW3>=VREF_1_KPH)
		{  
			tempW6 = tempW6 -  tempW3;
		}
		else { }
	}
	else 
	{
		/* detect bend -> threshold comp */
		if((BEND_DETECT2==1) && (tempW3>=VREF_2_KPH))
		{
			tempW6 = tempW6 -  (tempW3/2);
		}
		else { }				
	}	
			#endif 	/* __EDC_COMPENSATION_IN_TURN */	

	if ( (Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1) )
	{		
		tempW6 = tempW6 + S8_EDC_DEC_TAR_DEL_V_IN_RR;
	}
	else
	{
		;
	}

		#if __EDC_TEMP_TIRE_DETECTION
	if (ldedcu1TempTireSuspect==1)
	{	
		if (ldedcu1TempTireSuspectInCtl==1)			
		{	
    			#if __VDC
    		tempW1 = (vref5 *3) / 10; /* 차속의 30% */
    			#else	
    		tempW1 = (vref *3) / 10; /* 차속의 30% */
    			#endif		
    	}
    	else
    	{
    			#if __VDC
    		tempW1 = (vref5) / 50; /* Considered RTA confidence factor +-2% */
    			#else	
    		tempW1 = (vref) / 50; 
    			#endif
    	}				
		tempW6 = tempW6 - tempW1;
	}
	else
	{
		;
	}	
		#endif	
	
	lcedcs16TargetWheelSlip = tempW6;
	
}

void LCEDC_vCalError(void)
{
    lcedcs16WheelSlipFiltOld = lcedcs16WheelSlipFilt;
 
	lcedcs16WheelSlipError = lcedcs16AvgDrivenWheelSlip - lcedcs16TargetWheelSlip;
 
  	if(!AUTO_TM)  
  	{
  		tempW4=L_U8FILTER_GAIN_10MSLOOP_3HZ;  /* 8hz:35 , 5hz:21  , 3hz : 17 */
  	}	
  	else
  	{	  
  		tempW4=L_U8FILTER_GAIN_10MSLOOP_8HZ;  /* 8hz:35 , 5hz:21  , 3hz : 17 */
  	}	  	
    
    lcedcs16WheelSlipFilt = (int16_t)( ( (((int32_t)lcedcs16WheelSlipFiltOld)*(128-tempW4)) + ((((int32_t)lcedcs16AvgDrivenWheelSlip)*10)*tempW4) ) / (128) ); 
	lcedcs16WheelSlipErrDiff = (lcedcs16WheelSlipFilt - lcedcs16WheelSlipFiltOld);
}

void LCEDC_vCalPDGain(void)
{	
	if (lcedcs16WheelSlipError < 0)
	{
		if (lcedcs16WheelSlipErrDiff < 0)
		{
        	if(ABS_ON_flag==1)
        	{	
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_P_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_P_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_P_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_P_GAIN_ABS_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_D_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_D_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_D_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_D_GAIN_ABS_S4);        	
        	}
        		#if __VDC	
        	else if(YAW_CDC_WORK==1)
        	{
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_P_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_P_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_P_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_P_GAIN_ESC_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_D_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_D_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_D_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_D_GAIN_ESC_S4);        
        	}
        		#endif	
        	else		
        	{			
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_P_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_P_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_P_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_P_GAIN_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE1_D_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE1_D_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE1_D_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE1_D_GAIN_S4);		
			}
			EDC_Phase_flags = 1;			
		}
		else
		{
        	if(ABS_ON_flag==1)
        	{	
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_P_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_P_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_P_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_P_GAIN_ABS_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_D_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_D_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_D_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_D_GAIN_ABS_S4);        	
        	}
        		#if __VDC	
        	else if(YAW_CDC_WORK==1)
        	{
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_P_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_P_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_P_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_P_GAIN_ESC_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_D_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_D_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_D_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_D_GAIN_ESC_S4);        
        	}
        		#endif	
        	else		
        	{			
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_P_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_P_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_P_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_P_GAIN_S4);		
    			                                                                             
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE2_D_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE2_D_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE2_D_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE2_D_GAIN_S4);		
			}
			EDC_Phase_flags = 2;				
		}
	}
	else
	{
		if (lcedcs16WheelSlipErrDiff > 0)
		{
        	if(ABS_ON_flag==1)
        	{	
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_P_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_P_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_P_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_P_GAIN_ABS_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_D_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_D_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_D_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_D_GAIN_ABS_S4);        	
        	}
        		#if __VDC	
        	else if(YAW_CDC_WORK==1)
        	{
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_P_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_P_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_P_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_P_GAIN_ESC_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_D_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_D_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_D_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_D_GAIN_ESC_S4);        
        	}
        		#endif	
        	else		
        	{			
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_P_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_P_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_P_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_P_GAIN_S4);		
    			                                                                            
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE3_D_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE3_D_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE3_D_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE3_D_GAIN_S4);		
			}
			EDC_Phase_flags = 3;
		}
		else
		{
        	if(ABS_ON_flag==1)
        	{	
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_P_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_P_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_P_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_P_GAIN_ABS_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_D_GAIN_ABS_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_D_GAIN_ABS_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_D_GAIN_ABS_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_D_GAIN_ABS_S4);        	
        	}
        		#if __VDC	
        	else if(YAW_CDC_WORK==1)
        	{
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_P_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_P_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_P_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_P_GAIN_ESC_S4);		
    			
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_D_GAIN_ESC_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_D_GAIN_ESC_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_D_GAIN_ESC_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_D_GAIN_ESC_S4);        
        	}
        		#endif	
        	else		
        	{			
    			lcedcs16PGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_P_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_P_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_P_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_P_GAIN_S4);		
    			                                                                            
    			lcedcs16DGain = LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_EDC_PHASE4_D_GAIN_S1,
    						     				    		U16_EDC_SPEED2, (int16_t)U8_EDC_PHASE4_D_GAIN_S2,
    											  			U16_EDC_SPEED3, (int16_t)U8_EDC_PHASE4_D_GAIN_S3,
    											  			U16_EDC_SPEED4, (int16_t)U8_EDC_PHASE4_D_GAIN_S4);		
			}
			EDC_Phase_flags = 4;
		}		
	}
	
	if ( (Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1) )
	{
		lcedcs16DGain = lcedcs16DGain / U8_D_GAIN_DEC_FACTOR_IN_RR;
	}
	else
	{
		;	/* Do Nothing */
	}
		#if __EDC_HIGH_MU_DETECTION
	if (ldedcu1HighMuSuspectFlag==1)
	{
		lcedcs16PGain = lcedcs16PGain / U8_GAIN_DEC_FACTOR_IN_HIGH_MU;
		lcedcs16DGain = lcedcs16DGain / U8_GAIN_DEC_FACTOR_IN_HIGH_MU;
	}
	else
	{
		;
	}
		#endif

	lcedcs16PGainEffect = (int16_t)((int32_t)lcedcs16PGain * lcedcs16WheelSlipError / (int16_t)U16_EDC_GAIN_SCALING_FACTOR);
	lcedcs16DGainEffect = (int16_t)((int32_t)lcedcs16DGain * lcedcs16WheelSlipErrDiff / (int16_t)U16_EDC_GAIN_SCALING_FACTOR);	

	/* EDC 제어 중 delta target slip이 target slip미만이고, engine torque가 설정된 이값 보다 작은 영역에서, 설정된 torque level로 유지를 위한 속도별 제어량 설정.*/	
	ldedcu8TorqHoldLvInEDC = (uint8_t)LCESP_s16IInter4Point(vref, U16_EDC_SPEED1, (int16_t)U8_TORQ_HOLD_LV_IN_EDC_S1,
				     				    			  	 U16_EDC_SPEED2, (int16_t)U8_TORQ_HOLD_LV_IN_EDC_S2,
									  				  	 U16_EDC_SPEED3, (int16_t)U8_TORQ_HOLD_LV_IN_EDC_S3,
									  				  	 U16_EDC_SPEED4, (int16_t)U8_TORQ_HOLD_LV_IN_EDC_S4);	

}

void LCEDC_vCallPDController(void)
{
	if((EDC_EXIT==1)||(EDC_NOR_EXIT==1))
	{
		tempW0 = 0;
	}
	else
	{
		tempW0 = (lcedcs16PGainEffect + lcedcs16DGainEffect) * -1;		
	}	
	
	if (tempW0>=0)			/* Torque Increase In MSR Mode */
	{
		if (MSR_cal_torq > drive_torq ) 
		{		
			MSR_cal_torq = MSR_cal_torq + tempW0;
		}
		else
		{
			MSR_cal_torq = drive_torq + tempW0;	
		}					
	}
	else					/* Torque Reduction In MSR Mode */
	{		
		if (MSR_cal_torq > drive_torq ) 
		{
			MSR_cal_torq = MSR_cal_torq + tempW0;
		}
		else 
		{
			MSR_cal_torq = drive_torq;		
		}
	}	

}

void LCEDC_vInitializeEdc(void) 		/* EDC initialization!!! */
{	
	EDC_ON=EDC_ABS_ON=EDC_SOLO_ON=0;
    	#if __VDC	
	EDC_ESP_ON=0;
		#endif
	EDC_cmd=MSR_cal_torq=drive_torq;
	EDC_exit_counter=0;	
}

void LCEDC_vEDCFadeOutmode(void)
{
	if((lcedcs16AvgDrivenWheelSlip<lcedcs16TargetWheelSlip)&&(MSR_cal_torq<ldedcu8TorqHoldLvInEDC)&&(eng_torq<ldedcu8TorqHoldLvInEDC)&&((EDC_NOR_EXIT==1)||(EDC_EXIT==1)))	
	{
		MSR_cal_torq=(int16_t)ldedcu8TorqHoldLvInEDC;
	}
	else	
	{
    	tempW7=MSR_cal_torq;
    	if(tempW7 > (int16_t)drive_torq_old) 
    	{
    		if(tempW7>=(int16_t)lcedcs16EDCTorqDecRate)
    		{
    			tempW7=tempW7-lcedcs16EDCTorqDecRate;
    			if(tempW7<=0)
    			{
    				tempW7=0;
    			}
    			else
    			{
    				;
    			}			
    		}
    		else
    		{
    			tempW7=drive_torq;
    		}
    	}
    	else
    	{
    		tempW7=drive_torq;
    	}
    	MSR_cal_torq=tempW7;
    }
}
	
void LCEDC_vLimitTorq(void)
{
	
	if(EDC_ON==1)
	{
    	/*Lower limit*/
    	if(MSR_cal_torq<=drive_torq)
    	{
    		MSR_cal_torq=drive_torq;
    	}
    	else
    	{
            if((lcedcs16AvgDrivenWheelSlip<lcedcs16TargetWheelSlip)&&(MSR_cal_torq<ldedcu8TorqHoldLvInEDC)&&(eng_torq<ldedcu8TorqHoldLvInEDC))	
            {
    	        MSR_cal_torq=(int16_t)ldedcu8TorqHoldLvInEDC;
            }
            else	
            {
            	;
            }	
    	}
    	
    	/* Upper limit */ 	
    	if(ABS_ON_flag==1)
    	{
				#if __EDC_HIGH_MU_DETECTION    		
    		if(ldedcu1HighMuSuspectFlag==1)
    		{
    			tempW9 = U8_TORQ_UP_LIMIT_ABS_HIGH_MU * 10;	
    		}
    		else
    		{
    			tempW9 = U8_TORQ_UP_LIMIT_ABS * 10;	
    		}
    			#else
    		tempW9 = U8_TORQ_UP_LIMIT_ABS * 10;	
    			#endif	

        	if(MSR_cal_torq>tempW9) 
        	{
        		MSR_cal_torq=tempW9;			/*05'Winter : Torque limit (050114) */
        	}
        	else
        	{
        		;
        	}
        }
      #if __VDC
    	else if(YAW_CDC_WORK==1)
    	{
    		tempW9 = U8_TORQ_UP_LIMIT_ESC * 10;	
        	if(MSR_cal_torq>tempW9) 
        	{
        		MSR_cal_torq=tempW9;			/*05'Winter : Torque limit (050114)*/
        	}
        	else
        	{
        		;
        	}			
        }
      #endif        
        else
        {
    		tempW9 = U8_TORQ_UP_LIMIT_SOLO * 10;	
           	if(MSR_cal_torq>tempW9) 
           	{
           		MSR_cal_torq=tempW9;			/*05'Winter : Torque limit (050114)*/
           	}
           	else
           	{
           		;
           	}		
        }
	}
	else
	{
		MSR_cal_torq=drive_torq;
	}     
	
}

#endif		/* TAG1 */
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCEDCCallControl
	#include "Mdyn_autosar.h"
#endif    
/* AUTOSAR --------------------------*/
