#ifndef __ETCSHEADER4IMPORT_H__
#define __ETCSHEADER4IMPORT_H__

#ifdef __ECU
//#ifndef __RTWTYPES_H__
#include "../LVarHead.h"
#include "../LCTCSCallControl.h"
#include "../LDRTACallDetection.h"

#if __REAR_D
#define VarFwd								0
#else
#define VarFwd								1
#endif
#define Ctrl_Mode							1
#else
#include "../SilHeader/SilHead_TP.h"
#include "../SilHeader/SilHead_Struct.h"
#endif

typedef struct W_STRUCT W_STRUCT_SIM;
typedef struct TCS_WL_STRUCT TCS_WL_STRUCT_SIM;
typedef struct TCS_AXLE_STRUCT TCS_AXLE_STRUCT_SIM;
typedef struct TF0_BIT TF0_BIT_SIM;
typedef struct TF12_BIT TF12_BIT_SIM;
typedef struct bit_position bit_position_SIM;
typedef struct TCS_CIRCUIT_STRUCT TCS_CIRCUIT_STRUCT_SIM;
typedef struct RTA_STRUCT RTA_WL_STRUCT_SIM;
typedef struct SCC_FLG SCC_STRUCT_SIM;

/* Interface for Calibration Parameters */
#ifndef CalParam
#define CalParam

/*Legacy Define Value*/
#define GEAR_POS_TM_1   								1
#define GEAR_POS_TM_2   								2
#define GEAR_POS_TM_3   								3
#define GEAR_POS_TM_4   								4
#define GEAR_POS_TM_5   								5
#define GEAR_POS_TM_6   								6
#define GEAR_POS_TM_PN   								0
/*#define GEAR_POS_TM_R   								7 Change by Vehicle Type*/
#define GEAR_SEL_M   									8

#define VREF_0_5_KPH									4
#define VREF_1_KPH										8
#define VREF_3_KPH										24
#define VREF_5_KPH										40
#define VREF_6_KPH										48
#define VREF_7_KPH										56
#define VREF_10_KPH										80
#define VREF_12_KPH										96
#define VREF_12_5_KPH									100
#define VREF_15_KPH                                     120
#define	VREF_25_KPH										200
#define VREF_30_KPH                                     240

#define	VREF_DECEL_0_3_G								30	
#define	GRAD_10_DEG										10	
#define	GRAD_8_DEG										8	

#define L_U8FILTER_GAIN_20MSLOOP_1HZ					14
#define L_U8FILTER_GAIN_20MSLOOP_7HZ					60
#define L_U8FILTER_GAIN_20MSLOOP_18HZ					89
#define TCSDpCtrlModeDisable							0
#define TCSDpCtrlModeNormal								1
#define TCSDpCtrlModeSports1							2
#define TCSDpCtrlModeDrvPrtn							3

/*Dont Change Space and Tab between Calibration Parameter Name and Value*/

///*ETCS_Calculation*/
//	/*Cal_AxSenAvail*/
//#define U8ETCSCpAxSenAvail 							1
//	/*Cal_VehPara*/
//#define S16ETCSCpTireRadi 							216
//#define S16ETCSCpTtlVehMass 						1680
//	/*Cal_TotalGearRatio*/
//#define S16ETCSCpTotalGearRatio_1 					1436
//#define S16ETCSCpTotalGearRatio_2 					889
//#define S16ETCSCpTotalGearRatio_3 					575
//#define S16ETCSCpTotalGearRatio_4 					434
//#define S16ETCSCpTotalGearRatio_5 					306
//#define S16ETCSCpTotalGearRatio_R 					1037
//#define S16ETCSCpTrnfrCsGrRtoHgMd 					100
//#define S16ETCSCpTrnfrCsGrRtoLwMd 					200
//#define U8ETCSCpGrTransTme 							50
//	/*Cal_TransEfficiency*/	
//#define U8ETCSCpTransEff_1 							78
//#define U8ETCSCpTransEff_2 							88
//#define U8ETCSCpTransEff_3 							92
//#define U8ETCSCpTransEff_4 							98
//#define U8ETCSCpTransEff_5 							100
//#define U8ETCSCpTransEff_R 							80
//	/*Cal_EngMdl*/
//#define S16ETCSCpEngSpd_1 							1000
//#define S16ETCSCpEngSpd_2 							2000
//#define S16ETCSCpEngSpd_3 							3000
//#define S16ETCSCpEngSpd_4 							4000
//#define S16ETCSCpEngSpd_5 							5000
//#define U8ETCSCpEngTrqFltGainCmdDec 				80
//#define U8ETCSCpEngTrqFltGainCmdInc 				68
//#define U8ETCSCpTrqDel_1 							3
//#define U8ETCSCpTrqDel_2 							2
//#define U8ETCSCpTrqDel_3 							1
//#define U8ETCSCpTrqDel_4 							0
//#define U8ETCSCpTrqDel_5 							0
//	/*Cal_VehSpdBrkPnts*/
//#define S16ETCSCpSpd_1 								0
//#define S16ETCSCpSpd_2 								24
//#define S16ETCSCpSpd_3 								80
//#define S16ETCSCpSpd_4 								240
//#define S16ETCSCpSpd_5 								640
//	/*Cal_StrtOfCtl*/
//#define U8ETCSCpCtlActTmThInLmtCnrg 				4
//	/*Cal_TarWhlSpn*/
//		/*Cal_HomoTarWhlSpn*/
//#define U8ETCSCpMinTarSpn 							8
//#define U8ETCSCpTarSpnTransTme 						5
//#define U8ETCSCpTarWhlSpin_1 						64
//#define U8ETCSCpTarWhlSpin_2 						64
//#define U8ETCSCpTarWhlSpin_3 						40
//#define U8ETCSCpTarWhlSpin_4 						40
//#define U8ETCSCpTarWhlSpin_5 						32
//		/*Cal_SplitTarWhlSpn*/
//#define U8ETCSCpAddTarWhlSpnSp 						40
//#define U8ETCSCpTarTransCplBrkTrq2Sp 				55
//#define U8ETCSCpTarTransStBrkTrq2Sp 				30
//		/*Cal_IncTarWhlSpn*/
//#define S16ETCSCpCircumferenceOfTire 				2053
//#define S16ETCSCpTrgtEngSpdStlDetd 					300	
//#define U8ETCSCpAlwAddTar4DepSnwSpd 				30
//#define U8ETCSCpNAlwAddTar4DepSnwSpd 				40
//#define U8ETCSCpTarSpnAdd4AbnmSizTire 				40
//#define U8ETCSCpTarSpnAdd4AdHiRd 					8
//#define U8ETCSCpTarSpnAdd4DepSnwHil 				40
//#define U8ETCSCpTarSpnAdd4DrvPrtnMd 				16
//#define U8ETCSCpTarSpnAdd4Hill 						32
//#define U8ETCSCpTarSpnAdd4PreHiRd 					8
//#define U8ETCSCpTarSpnAdd4Sp 						8
//		/*Cal_DecTarWhlSpn*/
//#define S8ETCSCpFnlTarSpnDecAsp_1 					-32
//#define S8ETCSCpFnlTarSpnDecAsp_2 					-32
//#define S8ETCSCpFnlTarSpnDecAsp_3 					-32
//#define S8ETCSCpFnlTarSpnDecAsp_4 					-32
//#define S8ETCSCpFnlTarSpnDecAsp_5 					-24
//#define S8ETCSCpFnlTarSpnDecIce_1 					-32
//#define S8ETCSCpFnlTarSpnDecIce_2 					-32
//#define S8ETCSCpFnlTarSpnDecIce_3 					-32
//#define S8ETCSCpFnlTarSpnDecIce_4 					-32
//#define S8ETCSCpFnlTarSpnDecIce_5 					-24
//#define S8ETCSCpFnlTarSpnDecSnw_1 					-32
//#define S8ETCSCpFnlTarSpnDecSnw_2 					-32
//#define S8ETCSCpFnlTarSpnDecSnw_3 					-32
//#define S8ETCSCpFnlTarSpnDecSnw_4 					-32
//#define S8ETCSCpFnlTarSpnDecSnw_5 					-24
//#define S8ETCSCpTarSpnDec4Vib 						-32
//#define U8ETCSCpDltYawOffset4MinTrgtSpn 			30
//	/*Cal_Cornering*/
//#define U8ETCSCpAyAsp 								70
//#define U8ETCSCpAyIce 								5
//#define U8ETCSCpAySnw 								40
//#define U8ETCSCpCnt4Tar1stIncInTrn 					6
//#define U8ETCSCpCnt4TarMaxDecInTrn 					12
//#define U8ETCSCpCrnExDctCnt 						12
//#define U8ETCSCpDltYawNVldSpd 						10
//#define U8ETCSCpDltYawStbDctCnt 					25
//#define U8ETCSCpDltYawVldSpd 						15
//#define U8ETCSCpDltYawVldVyHSpd 					70
//#define U8ETCSCpTarNChngDltYawHiBrkTrq 				50
//#define U8ETCSCpTarNChngDltYawHiSpd 				50	
//#define U8ETCSCpTarNChngDltYawLwBrkTrq 				20
//#define U8ETCSCpTarNChngDltYawLwSpd 				150
//#define U8ETCSCpTarNChngDltYawVyHSpd 				40
//	/*Cal_LoadTrq*/
//#define U8ETCSCpLdTrqByGrdTransTme 					25
//#define U8ETCSCpLdTrqChngFac 						100
//	/*Cal_SymBrkCtl*/	
//#define U8ETCSCpSymBrkCtlTh_1 						80
//#define U8ETCSCpSymBrkCtlTh_2 						80
//#define U8ETCSCpSymBrkCtlTh_3 						80
//#define U8ETCSCpSymBrkCtlTh_4 						80
//#define U8ETCSCpSymBrkCtlTh_5 						80
///*ETCS_Detection*/	
//	/*Dct_StrtAndExitOfCtl*/
//#define S8ETCSCpCtlExitCntDnTh 						-10
//#define S8ETCSCpCtlExitCntTh 						25
//#define S8ETCSCpCtlExitCntUpTh 						0
//#define U8ETCSCpCtlActCntThAtHiAxlAcc 				1
//#define U8ETCSCpCtlActCntThAtLwAxlAcc 				4
//#define U8ETCSCpCtlEnterAccPdlTh 					3
//#define U8ETCSCpCtlExitAccPdlTh 					1
//#define U8ETCSCpHiAxlAccTh 							200
//#define U8ETCSCpLwAxlAccTh 							80
//	/*Dct_EngStall*/
//#define S16ETCSCpEngStallRpm 						500
//#define S16ETCSCpHighRiskStallRpmFlat 				500
//#define S16ETCSCpHighRiskStallRpmHill 				700
//#define S16ETCSCpLowRiskStallRpmFlat 				500
//#define S16ETCSCpLowRiskStallRpmHill 				700
//#define S16ETCSCpMidRiskStallRpmFlat 				500
//#define S16ETCSCpMidRiskStallRpmHill 				700
//#define U8ETCSCpEngAvail 							1
//#define U8ETCSCpEngStallRpmOffset 					250
//	/*Dct_Reset1stCycle*/
//#define U8ETCSCpLwWhlSpnTh2ClrCyl1st 				20
//#define U8ETCSCpTimeTh2ClrCyl1st 					200
//	/*Dct_1stDropTorq*/
//#define S16ETCSCpCdrnTrqAtAspSpd_1 					3000
//#define S16ETCSCpCdrnTrqAtAspSpd_2 					3000
//#define S16ETCSCpCdrnTrqAtAspSpd_3 					3000
//#define S16ETCSCpCdrnTrqAtAspSpd_4 					2000
//#define S16ETCSCpCdrnTrqAtAspSpd_5 					2000	
//#define S16ETCSCpCdrnTrqAtIceSpd_1 					1000
//#define S16ETCSCpCdrnTrqAtIceSpd_2 					1000
//#define S16ETCSCpCdrnTrqAtIceSpd_3 					1000
//#define S16ETCSCpCdrnTrqAtIceSpd_4 					2000
//#define S16ETCSCpCdrnTrqAtIceSpd_5 					2000
//#define S16ETCSCpCdrnTrqAtSnowSpd_1 				2000
//#define S16ETCSCpCdrnTrqAtSnowSpd_2 				2000
//#define S16ETCSCpCdrnTrqAtSnowSpd_3 				2000
//#define S16ETCSCpCdrnTrqAtSnowSpd_4 				2000
//#define S16ETCSCpCdrnTrqAtSnowSpd_5 				2000
//	/*Dct_JumpUpAndDown*/
//#define U8ETCSCpFtr4JmpDnMax 						50
//#define U8ETCSCpFtr4JmpDnMin 						1
//#define U8ETCSCpFtr4JmpUpMax 						50
//#define U8ETCSCpFtr4JmpUpMin 						1
//#define U8ETCSCpFtr4RefTrqAt2ndCylStrt 				90
//#define U8ETCSCpRecTrqFacInBump 					90
//#define U8ETCSCpRefTrqNvldTm 						75
//#define U8ETCSCpRefTrqVldTm 						25
//	/*Dct_LowSpin*/
//#define U8ETCSCpLowWhlSpnCntDnTh 					12
//#define U8ETCSCpLowWhlSpnCntUpTh 					4
//	/*Dct_DeepSnow*/
//#define U8ETCSCpDetDepSnwHilCnt 					100
//#define U8ETCSCpHysDepSnwHilCnt 					50
//	/*Dct_Hill*/
//#define S16ETCSCpSplitHillClrEngSpd 				3500
//#define S16ETCSCpSplitHillDctEngSpd 				3000
//#define U8ETCSCpHillClrTime 						10
//#define U8ETCSCpHillDctAlongTh 						8
//#define U8ETCSCpHillDctTime 						25
//#define U8ETCSCpHillDctTimeAx 						25
//#define U8ETCSCpHillDiffAx 							30
//#define U8ETCSCpHsaHillTime 						100
//#define U8ETCSCpSplitHillClrAccel 					15
//#define U8ETCSCpSplitHillClrVehSpd 					102
//#define U8ETCSCpSplitHillDctAccel 					10
//#define U8ETCSCpSplitHillDctMinAx 					8
//#define U8ETCSCpSplitHillDctVehSpd 					40
//	/*Dct_H2L*/
//#define S16ETCSCpIGainIncFacH2L 					800
//#define S8ETCSCpH2LDctDltTh 						-10
//#define S8ETCSCpH2LDctHiMuTh 						20
//#define S8ETCSCpH2LDctLwMuTh 						10
//#define S8ETCSCpH2LTrnsTime 						15
//#define U8ETCSCpH2LDctHldTm 						50
//#define U8ETCSCpH2LDctSpnTh 						16
//#define U8ETCSCpPGainIncFacH2L 						200
//	/*Dct_HighMu*/
//#define S16ETCSCpLow2HighDecel 						-250
//#define S16ETCSCpPreHighRdDctCplCnt 				155
//#define S16ETCSCpPreHighRdDctStrtCnt 				150
//#define S16ETCSCpStrtCdrnTrqAtAsphalt 				2400
//#define U8ETCSCpAdaptHighRdCplCnt 					15
//#define U8ETCSCpAdaptHighRdStrtCnt 					10
//#define U8ETCSCpAreaSetLwToHiSpin 					24
//#define U8ETCSCpLwToHiDetAreaCnt 					50
//#define U8ETCSCpLwToHiDetCnt 						50
//	/*Dct_VCA*/	
//#define U8ETCSCpAWDVehicle 							0
//#define U8ETCSCpFtr4RefTrqAtVcaOfCtl 				100
//#define U8ETCSCpHysDeltAcc 							3
//#define U8ETCSCpStDeltAcc 							5
///*ETCS_Control*/	
//	/*Ctl_TypicalTrqAndAccel*/
//#define S16ETCSCpCdrnTrqAtAsphalt 					2700
//#define S16ETCSCpCdrnTrqAtIce 						650
//#define S16ETCSCpCdrnTrqAtSnow 						1700
//#define U8ETCSCpAxAtAsphalt 						35
//#define U8ETCSCpAxAtIce 							4
//#define U8ETCSCpAxAtSnow 							20
//#define U8ETCSCpAxGear2AtAsphalt 					35
//#define U8ETCSCpAxGear2AtIce 						4
//#define U8ETCSCpAxGear2AtSnow 						20
//#define U8ETCSCpAxGear3AtAsphalt 					15
//#define U8ETCSCpAxGear3AtIce 						4
//#define U8ETCSCpAxGear3AtSnow 						12
//#define U8ETCSCpAxGear4AtAsphalt 					15
//#define U8ETCSCpAxGear4AtIce 						4
//#define U8ETCSCpAxGear4AtSnow 						12
//#define U8ETCSCpAxGear5AtAsphalt 					15
//#define U8ETCSCpAxGear5AtIce 						4
//#define U8ETCSCpAxGear5AtSnow 						12
//	/*Ctl_PEffect*/	
//		/*Ctl_Pgain*/
//#define S8ETCSCpPgTransTmDrvVib 					50
//			/*Ctl_Pgain_Homo*/
//#define U8ETCSCpNegErrPgainAsp_1 					70
//#define U8ETCSCpNegErrPgainAsp_2 					20
//#define U8ETCSCpNegErrPgainAsp_3 					10
//#define U8ETCSCpNegErrPgainAsp_4 					10
//#define U8ETCSCpNegErrPgainAsp_5 					10
//#define U8ETCSCpNegErrPgainIce_1 					90
//#define U8ETCSCpNegErrPgainIce_2 					60
//#define U8ETCSCpNegErrPgainIce_3 					45
//#define U8ETCSCpNegErrPgainIce_4 					40
//#define U8ETCSCpNegErrPgainIce_5 					30
//#define U8ETCSCpNegErrPgainSnw_1 					80
//#define U8ETCSCpNegErrPgainSnw_2 					50
//#define U8ETCSCpNegErrPgainSnw_3 					30
//#define U8ETCSCpNegErrPgainSnw_4 					25
//#define U8ETCSCpNegErrPgainSnw_5 					20
//#define U8ETCSCpPosErrPgainAsp_1 					150
//#define U8ETCSCpPosErrPgainAsp_2 					100
//#define U8ETCSCpPosErrPgainAsp_3 					50
//#define U8ETCSCpPosErrPgainAsp_4 					50
//#define U8ETCSCpPosErrPgainAsp_5 					50
//#define U8ETCSCpPosErrPgainIce_1 					70
//#define U8ETCSCpPosErrPgainIce_2 					45
//#define U8ETCSCpPosErrPgainIce_3 					35
//#define U8ETCSCpPosErrPgainIce_4 					25
//#define U8ETCSCpPosErrPgainIce_5 					15
//#define U8ETCSCpPosErrPgainSnw_1 					90
//#define U8ETCSCpPosErrPgainSnw_2 					60
//#define U8ETCSCpPosErrPgainSnw_3 					40
//#define U8ETCSCpPosErrPgainSnw_4 					30
//#define U8ETCSCpPosErrPgainSnw_5 					20
//			/*Ctl_Pgain_Split*/
//#define U8ETCSCpNegErrPgainSplt_1 					70
//#define U8ETCSCpNegErrPgainSplt_2 					70
//#define U8ETCSCpNegErrPgainSplt_3 					60
//#define U8ETCSCpNegErrPgainSplt_4 					60
//#define U8ETCSCpNegErrPgainSplt_5 					60
//#define U8ETCSCpPosErrPgainSplt_1 					100
//#define U8ETCSCpPosErrPgainSplt_2 					100
//#define U8ETCSCpPosErrPgainSplt_3 					100
//#define U8ETCSCpPosErrPgainSplt_4 					100	
//#define U8ETCSCpPosErrPgainSplt_5 					100
//		/*Ctl_Pfactor*/
//			/*Ctl_Pfactor_GearChng*/
//#define U8ETCSCpGainTrnsTmAftrGearShft 				20
//#define U8ETCSCpPgFacGrChgErrNeg_1 					180
//#define U8ETCSCpPgFacGrChgErrNeg_2 					150
//#define U8ETCSCpPgFacGrChgErrNeg_3 					140
//#define U8ETCSCpPgFacGrChgErrNeg_4 					100
//#define U8ETCSCpPgFacGrChgErrNeg_5 					80
//			/*Ctl_Pfactor_Cornering*/
//#define U8ETCSCpPgFacInTrnAsp_1 					15
//#define U8ETCSCpPgFacInTrnAsp_2 					15
//#define U8ETCSCpPgFacInTrnAsp_3 					15
//#define U8ETCSCpPgFacInTrnAsp_4 					15
//#define U8ETCSCpPgFacInTrnAsp_5 					15
//#define U8ETCSCpPgFacInTrnIce_1 					30
//#define U8ETCSCpPgFacInTrnIce_2 					30
//#define U8ETCSCpPgFacInTrnIce_3 					30
//#define U8ETCSCpPgFacInTrnIce_4 					30
//#define U8ETCSCpPgFacInTrnIce_5 					30
//#define U8ETCSCpPgFacInTrnSnw_1 					20
//#define U8ETCSCpPgFacInTrnSnw_2 					20
//#define U8ETCSCpPgFacInTrnSnw_3 					20
//#define U8ETCSCpPgFacInTrnSnw_4 					20
//#define U8ETCSCpPgFacInTrnSnw_5 					20
//	/*Ctl_IEffect*/	
//		/*Ctl_Igain*/
//			/*Ctl_Igain_Homo*/
//#define U8ETCSCpNegErrIgainAsp_1 					7
//#define U8ETCSCpNegErrIgainAsp_2 					5
//#define U8ETCSCpNegErrIgainAsp_3 					3
//#define U8ETCSCpNegErrIgainAsp_4 					3
//#define U8ETCSCpNegErrIgainAsp_5 					3
//#define U8ETCSCpNegErrIgainIce_1 					25
//#define U8ETCSCpNegErrIgainIce_2 					20
//#define U8ETCSCpNegErrIgainIce_3 					10
//#define U8ETCSCpNegErrIgainIce_4 					5
//#define U8ETCSCpNegErrIgainIce_5 					5
//#define U8ETCSCpNegErrIgainSnw_1 					20
//#define U8ETCSCpNegErrIgainSnw_2 					15
//#define U8ETCSCpNegErrIgainSnw_3 					7
//#define U8ETCSCpNegErrIgainSnw_4 					4
//#define U8ETCSCpNegErrIgainSnw_5 					4
//#define U8ETCSCpPosErrIgainAsp_1 					100
//#define U8ETCSCpPosErrIgainAsp_2 					100
//#define U8ETCSCpPosErrIgainAsp_3 					50
//#define U8ETCSCpPosErrIgainAsp_4 					50
//#define U8ETCSCpPosErrIgainAsp_5 					50
//#define U8ETCSCpPosErrIgainIce_1 					30
//#define U8ETCSCpPosErrIgainIce_2 					20
//#define U8ETCSCpPosErrIgainIce_3 					15
//#define U8ETCSCpPosErrIgainIce_4 					12
//#define U8ETCSCpPosErrIgainIce_5 					12
//#define U8ETCSCpPosErrIgainSnw_1 					35
//#define U8ETCSCpPosErrIgainSnw_2 					25
//#define U8ETCSCpPosErrIgainSnw_3 					17
//#define U8ETCSCpPosErrIgainSnw_4 					15
//#define U8ETCSCpPosErrIgainSnw_5 					15
//			/*Ctl_Igain_Split*/
//#define U8ETCSCpNegErrIgainSplt_1 					10
//#define U8ETCSCpNegErrIgainSplt_2 					10
//#define U8ETCSCpNegErrIgainSplt_3 					10
//#define U8ETCSCpNegErrIgainSplt_4 					10
//#define U8ETCSCpNegErrIgainSplt_5 					10
//#define U8ETCSCpPosErrIgainSplt_1 					70
//#define U8ETCSCpPosErrIgainSplt_2 					70
//#define U8ETCSCpPosErrIgainSplt_3 					70
//#define U8ETCSCpPosErrIgainSplt_4 					70
//#define U8ETCSCpPosErrIgainSplt_5 					70
//		/*Ctl_Ifactor*/
//			/*Ctl_Ifactor_GearChng*/
//#define U8ETCSCpIgFacGrChgErrNeg_1 					30
//#define U8ETCSCpIgFacGrChgErrNeg_2 					30
//#define U8ETCSCpIgFacGrChgErrNeg_3 					30
//#define U8ETCSCpIgFacGrChgErrNeg_4 					30
//#define U8ETCSCpIgFacGrChgErrNeg_5 					30
//			/*Ctl_Ifactor_Cornering*/		
//#define U8ETCSCpIgFacInTrnAsp_1 					30
//#define U8ETCSCpIgFacInTrnAsp_2 					30
//#define U8ETCSCpIgFacInTrnAsp_3 					30
//#define U8ETCSCpIgFacInTrnAsp_4 					30
//#define U8ETCSCpIgFacInTrnAsp_5 					30
//#define U8ETCSCpIgFacInTrnIce_1 					70
//#define U8ETCSCpIgFacInTrnIce_2 					70
//#define U8ETCSCpIgFacInTrnIce_3 					70
//#define U8ETCSCpIgFacInTrnIce_4 					70
//#define U8ETCSCpIgFacInTrnIce_5 					70
//#define U8ETCSCpIgFacInTrnSnw_1 					70
//#define U8ETCSCpIgFacInTrnSnw_2 					70
//#define U8ETCSCpIgFacInTrnSnw_3 					70
//#define U8ETCSCpIgFacInTrnSnw_4 					70
//#define U8ETCSCpIgFacInTrnSnw_5 					70
//			/*Ctl_Ifactor_AftLimCornrg*/
//#define U8ETCSCpIgainFacAftrTrnAsp_1 				40
//#define U8ETCSCpIgainFacAftrTrnAsp_2 				40
//#define U8ETCSCpIgainFacAftrTrnAsp_3 				40
//#define U8ETCSCpIgainFacAftrTrnAsp_4 				40
//#define U8ETCSCpIgainFacAftrTrnAsp_5 				40
//#define U8ETCSCpIgainFacAftrTrnIce_1 				20
//#define U8ETCSCpIgainFacAftrTrnIce_2 				20
//#define U8ETCSCpIgainFacAftrTrnIce_3 				20
//#define U8ETCSCpIgainFacAftrTrnIce_4 				20
//#define U8ETCSCpIgainFacAftrTrnIce_5 				20
//#define U8ETCSCpIgainFacAftrTrnSnow_1 				30
//#define U8ETCSCpIgainFacAftrTrnSnow_2 				30
//#define U8ETCSCpIgainFacAftrTrnSnow_3 				30
//#define U8ETCSCpIgainFacAftrTrnSnow_4 				30
//#define U8ETCSCpIgainFacAftrTrnSnow_5 				30
//			/*Ctl_Ifactor_BigSpn*/
//#define U8ETCSCpBigSpnMaxIgFac 						130
//	/*Ctl_DeltaYaw*/	
//		/*Ctl_Divrg_Yawgain*/
//#define U8ETCSCpDelYawDivrgGainAsp_1 				15
//#define U8ETCSCpDelYawDivrgGainAsp_2 				15
//#define U8ETCSCpDelYawDivrgGainAsp_3 				15
//#define U8ETCSCpDelYawDivrgGainAsp_4 				15
//#define U8ETCSCpDelYawDivrgGainAsp_5 				15
//#define U8ETCSCpDelYawDivrgGainIce_1 				25
//#define U8ETCSCpDelYawDivrgGainIce_2 				25
//#define U8ETCSCpDelYawDivrgGainIce_3 				25
//#define U8ETCSCpDelYawDivrgGainIce_4 				25
//#define U8ETCSCpDelYawDivrgGainIce_5 				25
//#define U8ETCSCpDelYawDivrgGainSnw_1 				20
//#define U8ETCSCpDelYawDivrgGainSnw_2 				20
//#define U8ETCSCpDelYawDivrgGainSnw_3 				20
//#define U8ETCSCpDelYawDivrgGainSnw_4 				20
//#define U8ETCSCpDelYawDivrgGainSnw_5 				20
//		/*Ctl_Cnvrg_FastInc_Yawgain*/	
//#define U8ETCSCpDelYawFstIncGainAsp_1 				50
//#define U8ETCSCpDelYawFstIncGainAsp_2 				50
//#define U8ETCSCpDelYawFstIncGainAsp_3 				50
//#define U8ETCSCpDelYawFstIncGainAsp_4 				50
//#define U8ETCSCpDelYawFstIncGainAsp_5 				50
//#define U8ETCSCpDelYawFstIncGainIce_1 				30
//#define U8ETCSCpDelYawFstIncGainIce_2 				30
//#define U8ETCSCpDelYawFstIncGainIce_3 				30
//#define U8ETCSCpDelYawFstIncGainIce_4 				30
//#define U8ETCSCpDelYawFstIncGainIce_5 				30
//#define U8ETCSCpDelYawFstIncGainSnw_1 				40
//#define U8ETCSCpDelYawFstIncGainSnw_2 				40
//#define U8ETCSCpDelYawFstIncGainSnw_3 				40
//#define U8ETCSCpDelYawFstIncGainSnw_4 				40
//#define U8ETCSCpDelYawFstIncGainSnw_5 				40
//		/*Ctl_Cnvrg_SlowInc_Yawgain*/		
//#define U8ETCSCpDelYawSlwIncGainAsp_1 				30
//#define U8ETCSCpDelYawSlwIncGainAsp_2 				30
//#define U8ETCSCpDelYawSlwIncGainAsp_3 				30
//#define U8ETCSCpDelYawSlwIncGainAsp_4 				30
//#define U8ETCSCpDelYawSlwIncGainAsp_5 				30
//#define U8ETCSCpDelYawSlwIncGainIce_1 				20
//#define U8ETCSCpDelYawSlwIncGainIce_2 				20
//#define U8ETCSCpDelYawSlwIncGainIce_3 				20
//#define U8ETCSCpDelYawSlwIncGainIce_4 				20
//#define U8ETCSCpDelYawSlwIncGainIce_5 				20
//#define U8ETCSCpDelYawSlwIncGainSnw_1 				25
//#define U8ETCSCpDelYawSlwIncGainSnw_2 				25
//#define U8ETCSCpDelYawSlwIncGainSnw_3 				25
//#define U8ETCSCpDelYawSlwIncGainSnw_4 				25
//#define U8ETCSCpDelYawSlwIncGainSnw_5 				25
//		/*Ctl_BaseTorq_Yawgain*/		
//#define S16ETCSCpLmtCrngTarTrqAsp 					1000
//#define S16ETCSCpLmtCrngTarTrqIce 					700
//#define S16ETCSCpLmtCrngTarTrqSnw 					800
//#define U8ETCSCpDelYawLowTrqGainAsp 				100
//#define U8ETCSCpDelYawLowTrqGainIce 				50
//#define U8ETCSCpDelYawLowTrqGainSnw 				70
//	/*Ctl_SymBrkCtl*/
//#define U8ETCSCpSymBrkCtlIhbAxTh 					20
//#define U8ETCSCpSymBrkTrqDecRate 					15	
//#define U8ETCSCpSymBrkTrqMax_1 						100
//#define U8ETCSCpSymBrkTrqMax_2 						80
//#define U8ETCSCpSymBrkTrqMax_3 						60
//#define U8ETCSCpSymBrkTrqMax_4 						50
//#define U8ETCSCpSymBrkTrqMax_5 						40
//	/*Ctl_MinCtlCmd*/
//#define S16ETCSCpMinCrdnCmdTrq 						0
//	/*Ctl_FctLamp*/	
//#define S16ETCSCpOffsetTrq4FunLamp 					400
//#define U8ETCSCpFunLampOffCnt 						5
//	/*Ctl_TrsmCrtl*/
//#define U8TMCCpTurnIndex_1 							100
//#define U8TMCCpTurnIndex_2 							120
//#define U8TMCCpTurnIndex_3 							150
//#define U8TMCpAllowDnShftMinVehSpd_1 				5
//#define U8TMCpAllowDnShftMinVehSpd_2 				10
//#define U8TMCpAllowDnShftMinVehSpd_3 				30
//#define U8TMCpAllowDnShftMinVehSpd_4 				50
//#define U8TMCpAllowDnShftMinVehSpd_5 				70
//#define U8TMCpAllowDnShftRPM_1 						15
//#define U8TMCpAllowDnShftRPM_2 						17
//#define U8TMCpAllowDnShftRPM_3 						20
//#define U8TMCpAllowDnShftRPM_4 						20
//#define U8TMCpAllowDnShftRPM_5 						20
//#define U8TMCpAllowDnShftVehSpd_1 					10
//#define U8TMCpAllowDnShftVehSpd_2 					30
//#define U8TMCpAllowDnShftVehSpd_3 					50
//#define U8TMCpAllowDnShftVehSpd_4 					70
//#define U8TMCpAllowDnShftVehSpd_5 					90
//#define U8TMCpAllowUpShftMaxVehSpd_1 				40
//#define U8TMCpAllowUpShftMaxVehSpd_2 				60
//#define U8TMCpAllowUpShftMaxVehSpd_3 				80
//#define U8TMCpAllowUpShftMaxVehSpd_4 				100
//#define U8TMCpAllowUpShftMaxVehSpd_5 				120
//#define U8TMCpAllowUpShftRPM_1 						32
//#define U8TMCpAllowUpShftRPM_2 						32
//#define U8TMCpAllowUpShftRPM_3 						30
//#define U8TMCpAllowUpShftRPM_4 						30
//#define U8TMCpAllowUpShftRPM_5 						30
//#define U8TMCpAllowUpShftVehSpd_1 					20
//#define U8TMCpAllowUpShftVehSpd_2 					40
//#define U8TMCpAllowUpShftVehSpd_3 					60
//#define U8TMCpAllowUpShftVehSpd_4 					80
//#define U8TMCpAllowUpShftVehSpd_5 					100
//#define U8TMCpGearShiftTime 						5
//#define U8TMCpMaxGear 								6
//#define U8TMCpMinGear 								1
//#define U8TMCpShftChgRPMHys 						1
//#define U8TMCpShftChgRPMInTrn_1 					0
//#define U8TMCpShftChgRPMInTrn_2 					0
//#define U8TMCpShftChgRPMInTrn_3 					0
//#define U8TMCpShftChgVehSpdHys 						3
//#define U8TMCpShftChgVehSpdInHill 					5
//#define U8TMCpShftChgVehSpdInTrn_1 					0
//#define U8TMCpShftChgVehSpdInTrn_2 					0
//#define U8TMCpShftChgVehSpdInTrn_3 					0

#define U8TCSCpHghMuWhlSpinTh						12
#define S16ETCSCpOffsetTrq4LwToSplt					200
#define S16ETCSCpLwToSpltDctStrtCnt					50
#define S16ETCSCpLwToSpltDctCplCnt					150
#define	U8TCSCpAddRefRdCfFrci						8
#define	U16TCSCpDctLwHmMaxCnt						150	
#define	U8ETCSCpAddTarWhlSpnLwToSp					24

#define U8ETCSCpGrdTransTme							5
#define	U8ETCSCpGradientTrstStCnt					10	
#define	U8ETCSCpGradientTrstCplCnt					20	
	
#define	S16ETCSCpAftTrnMaxCnt 						100	

#endif
#endif		/* __ETCSHEADER4IMPORT_H__ */
