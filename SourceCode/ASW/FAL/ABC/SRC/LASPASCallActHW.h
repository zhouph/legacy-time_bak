#ifndef __LASPASCALLACTHW_H__
#define __LASPASCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __SPAS_INTERFACE
/*Global Type Declaration ****************************************************/
#define	nonaa00       	SPASF0.bit7
#define	nonaa01			SPASF0.bit6
#define	nonaa02			SPASF0.bit5
#define	nonaa03			SPASF0.bit4
#define	nonaa04			SPASF0.bit3
#define	nonaa05			SPASF0.bit2
#define	nonaa06			SPASF0.bit1
#define	nonaa07			SPASF0.bit0

/* Logcal Definiton  *********************************************************/

//#define S16_SPAS_TC_CUR_FF_RISE       1100 /* MGH60 Max : 1100mA */
//#define S16_SPAS_TC_CUR_FB_RISE       1100 /* MGH60 Max : 1100mA */
//#define S16_SPAS_PRESS_DOWN_TIME1      L_U8_TIME_10MSLOOP_500MS//T_1000_MS
//#define S16_SPAS_1ST_HOLD_TIME         L_U8_TIME_10MSLOOP_1000MS
//#define S16_SPAS_PRESS_DOWN_TIME2      L_U8_TIME_10MSLOOP_1500MS
//#define S16_SPAS_TC_CUR_DROP_1ST      50
//#define U8_SPAS_F_O_CYCLE_SCAN_REF1   2
//#define U8_SPAS_F_O_CUR_DROP1         20
//#define U8_SPAS_F_O_CYCLE_SCAN_REF2   2
//#define U8_SPAS_F_O_CUR_DROP2         5
//#define S16_SPAS_FF_MOTOR            MSC_14_V
//#define S16_SPAS_FB_MOTOR_MAX        MSC_7_V//MSC_3_V
//#define S16_SPAS_FB_MOTOR_MIN        MSC_1_V

/*Global Extern Variable  Declaration*****************************************/
extern struct	U8_BIT_STRUCT_t SPASF0;

#pragma DATA_SEG __GPAGE_SEG PAGED_RAM

extern int16_t lcs16SpasHoldTcCurrent;
extern int16_t lcs16Spas1stTargetPressCurrent, lcs16Spas2ndTargetPressCurrent, lcs16SpasTcCurThPressRate;
extern int16_t spas_msc_target_vol;

#pragma DATA_SEG DEFAULT
/*Global Extern Functions  Declaration****************************************/
extern void	LASPAS_vCallActHW(void);
extern void	LCMSC_vSetSPASTargetVoltage(void);
extern INT	LAMFC_s16SetMFCCurrentSPAS(INT MFC_Current_old);
  #endif
#endif