#ifndef __LCSLSCALLCONTROL_H__
#define __LCSLSCALLCONTROL_H__

/*includes********************************************************************/

#include "LVarHead.h"

  #if __SLS
/*Global MACRO CONSTANT Definition********************************************/

#if (__CAR==GM_GSUV) 
#define __SLS_ENABLE_BY_CALIBRATION		ENABLE
#else
#define __SLS_ENABLE_BY_CALIBRATION		DISABLE
#endif

/* Logic Tuning */
#define  S16_SLS_HIGH_SPD_ENT_MQ_ABS       1000    
#define  S16_SLS_HIGH_SPD_EXIT_MQ_ABS      1000    
#define  S16_SLS_LOW_SPD_ENT_MQ_ABS        1000    
#define  S16_SLS_LOW_SPD_EXIT_MQ_ABS       1000    
#define  S16_SLS_MED_SPD_ENT_MQ_ABS        1000    
#define  S16_SLS_MED_SPD_EXIT_MQ_ABS       1000    

#define  S16_SLS_DEL_P_WEG_L_Spd		   100    
#define  S16_SLS_DEL_P_WEG_M_Spd		   100    
#define  S16_SLS_DEL_P_WEG_H_Spd		   100   

#define  S16_SLS_YAWG_FAIL_FADE_CNT		   20

#define U16_SLS_DYAW_ENTER_THRES_L_SPD  YAW_3DEG
#define U16_SLS_DYAW_ENTER_THRES_M_SPD  YAW_2DEG
#define U16_SLS_DYAW_ENTER_THRES_H_SPD  YAW_2DEG

#define U16_SLS_DYAW_END_THRES_L_SPD	YAW_2DEG
#define U16_SLS_DYAW_END_THRES_M_SPD    YAW_1DEG
#define U16_SLS_DYAW_END_THRES_H_SPD    YAW_1DEG
/* Logic Tuning */

/*Global Type Declaration ****************************************************/
#define SLS_ON_old                      SLSC0.bit0
#define SLS_ON                          SLSC0.bit1
#define SLS_ACTIVE_rl_old               SLSC0.bit2
#define SLS_ACTIVE_rl                   SLSC0.bit3
#define SLS_ACTIVE_rr_old               SLSC0.bit4
#define SLS_ACTIVE_rr                   SLSC0.bit5
#define SLS_INHIBIT_ESC_FLAG            SLSC0.bit6
#define SLS_PARTIAL_BRAKING             SLSC0.bit7

#define SLS_STRAIGHT_CONDITION          SLSC1.bit0
#define SLS_ENABLE                      SLSC1.bit1
#define SLS_FADE_OUT_rl                 SLSC1.bit2
#define SLS_FADE_OUT_rr                 SLSC1.bit3
#define SLS_not_used_bit_1_4            SLSC1.bit4
#define SLS_not_used_bit_1_5            SLSC1.bit5
#define SLS_not_used_bit_1_6            SLSC1.bit6
#define SLS_not_used_bit_1_7            SLSC1.bit7

#define SLS_FADE_OUT_ENABLE             SLSC2.bit0
#define SLS_INHIBIT_YAWG_FLAG           SLSC2.bit1
#define SLS_INHIBIT_YAWG_FLAG_old       SLSC2.bit2
#define SLS_YAWG_FAIL_FADE_FLAG         SLSC2.bit3
#define lcslsu1CtrlEnable               SLSC2.bit4
#define SLS_not_used_bit_2_5            SLSC2.bit5
#define SLS_not_used_bit_2_6            SLSC2.bit6
#define SLS_not_used_bit_2_7            SLSC2.bit7

#define SLS_not_used_bit_3_0            SLSC3.bit0
#define SLS_not_used_bit_3_1            SLSC3.bit1
#define SLS_not_used_bit_3_2            SLSC3.bit2
#define SLS_not_used_bit_3_3            SLSC3.bit3
#define SLS_not_used_bit_3_4            SLSC3.bit4
#define SLS_not_used_bit_3_5            SLSC3.bit5
#define SLS_not_used_bit_3_6            SLSC3.bit6
#define SLS_not_used_bit_3_7            SLSC3.bit7

#define SLS_not_used_bit_4_0            SLSC4.bit0
#define SLS_not_used_bit_4_1            SLSC4.bit1
#define SLS_not_used_bit_4_2            SLSC4.bit2
#define SLS_not_used_bit_4_3            SLSC4.bit3
#define SLS_not_used_bit_4_4            SLSC4.bit4
#define SLS_not_used_bit_4_5            SLSC4.bit5
#define SLS_not_used_bit_4_6            SLSC4.bit6
#define SLS_not_used_bit_4_7            SLSC4.bit7

#define SLS_not_used_bit_5_0            SLSC5.bit0
#define SLS_not_used_bit_5_1            SLSC5.bit1
#define SLS_not_used_bit_5_2            SLSC5.bit2
#define SLS_not_used_bit_5_3            SLSC5.bit3
#define SLS_not_used_bit_5_4            SLSC5.bit4
#define SLS_not_used_bit_5_5            SLSC5.bit5
#define SLS_not_used_bit_5_6            SLSC5.bit6
#define SLS_not_used_bit_5_7            SLSC5.bit7

#define SLS_not_used_bit_6_0            SLSC6.bit0
#define SLS_not_used_bit_6_1            SLSC6.bit1
#define SLS_not_used_bit_6_2            SLSC6.bit2
#define SLS_not_used_bit_6_3            SLSC6.bit3
#define SLS_not_used_bit_6_4            SLSC6.bit4
#define SLS_not_used_bit_6_5            SLSC6.bit5
#define SLS_not_used_bit_6_6            SLSC6.bit6
#define SLS_not_used_bit_6_7            SLSC6.bit7

/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t SLSC0,SLSC1,SLSC2,SLSC3,SLSC4,SLSC5,SLSC6;

extern int16_t SLS_target_delta_p_rear, SLS_target_p_rear, sls_yawg_fail_fade_cnt, sls_yawg_fail_fade_weight, delta_moment_q_sls_old ;

extern int16_t delta_moment_q_sls;
extern int16_t SLS_fade_out_max; 

extern int8_t lcSLSu8ControlModeRL;
extern int8_t lcSLSu8ControlModeRR;
extern int8_t lcSLSu8FadeOutModeRL;
extern int8_t lcSLSu8FadeOutModeRR; 

extern uint8_t SLS_target_hold_cnt;
extern uint8_t SLS_rate_counter_rl;
extern uint8_t SLS_rate_counter_rr;
extern uint8_t SLS_FADE_OUT_rl_cnt;
extern uint8_t SLS_FADE_OUT_rr_cnt;
extern uint8_t SLS_target_fade_cnt;
extern uint8_t SLS_fade_counter_rr;
extern uint8_t SLS_fade_counter_rl;

extern int16_t ldslsEstYawrate1, ldslsEstYawrate2;
extern int16_t sls_delta_yaw_first2; 
extern int16_t sls_yaw_error_under_dot2;

/*Global Extern Functions  Declaration****************************************/
  #endif
#endif





