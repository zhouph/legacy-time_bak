#ifndef __LCABSCALLCONTROL_H__
#define __LCABSCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition********************************************/
 #if (__CAR==GM_T300) || (__CAR==GM_GSUV) || (__CAR==GM_M350)
#define __DISABLE_CTRL_AT_OFF_ACC_CRANK      1
 #else
#define __DISABLE_CTRL_AT_OFF_ACC_CRANK      0
 #endif

#define __BIG_RISE_DETECT_ENABLE_MGH_80		1

#define __REORGANIZE_ABS_FOR_MGH60           1

  #if (__BLS_NO_WLAMP_FM==ENABLE)
#define __CONSIDER_BLS_FAULT				 ENABLE
  #else
#define __CONSIDER_BLS_FAULT				 DISABLE  
  #endif

/*Global Type Declaration ****************************************************/



/*Global Extern Variable  Declaration*****************************************/



/*Global Extern Functions  Declaration****************************************/
extern void LCABS_vCallControl(void);

/* <skeon OPTIME_MACFUNC_2> 110303*/
#ifndef OPTIME_MACFUNC_2
extern int16_t LCABS_s16LimitMinMax(int16_t LimitedVariable,int16_t MinValue,int16_t MaxValue);
extern int16_t LCABS_s16DecreaseUnsignedCnt(int16_t Cnt_old, int16_t increasement);

#endif
/* </skeon OPTIME_MACFUNC_2>*/
/*extern int16_t LCABS_s16SelectHigherValue(int16_t FirstInput,int16_t SecondInput);*/
#endif
