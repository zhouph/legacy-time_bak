#include "Abc_Ctrl.h"
/**************************************************/
/*                                                */
/*    Define & Declare  Structure Varible         */
/*                                                */
/*                  ____ MANDO ABS TEAM.          */
/**************************************************/

// ===========================================
// --->>>>>     EDC  Variables        <<<<----
// ===========================================
 

#if !__TCS
    int16_t   cal_torq, cal_torq_old;
    int16_t   can_ver;
    uint8_t tm_gear;
    int8_t  gear_pos; 	   
	uint8_t	lespu8TqIntvnTyp;
	uint16_t	lespu16TorqReqVal;      
	int16_t   	cal_torq_abs;
	int16_t   	cal_torq_rel;	   
#endif

#if __TCS || __ETC || __EDC		// ABS_PLUS_ECU 적용시 macro 삭제 예정
    uint16_t    in_gear_count; 
    uint8_t 	gear_state_temp_old,gear_state_temp,gear_state,gear_state_old;			
    uint8_t 	engine_state,engine_state_old,engine_state_1,engine_state_1_old,engine_state_2,engine_state_2_old;
    uint16_t  	EDC_tm_gear; 
    int16_t   	diff_v_tc_rpm;		
    int16_t   	diff_eng_tc_rpm;		
    int16_t   	vel_f_mid_2, vel_f_mid_2_old;                
    int16_t   	vel_f_mid_rpm,vel_f_mid_rpm_old; 
	int16_t 	eng_rpm_filt_for_GSD,eng_rpm_filt_old_for_GSD;		/* For __EDC_GEAR_SHIFT_DOWN_DETECTION	*/
	int16_t		slope_eng_rpm_for_GSD,slope_eng_rpm_old_for_GSD;	/* For __EDC_GEAR_SHIFT_DOWN_DETECTION	*/
	int16_t		slope_v_rpm, slope_v_rpm_old;
	uint8_t	EDC_MTM_RATIO[6];
	uint8_t	Normalized_Gear_Ratio[6];
	int16_t		Conv_vel_f_mid_rpm[6];
	int16_t		Conv_slope_v_rpm[6];
	int16_t		diff_rpm[6];
	int16_t		diff_slope[6];
	uint8_t  	In_gear_flag,In_gear_flag_temp;
	int16_t		flywheel_torq;
	int16_t    	Normalized_flywheel_torq[6];	
	int8_t 	Engine_Drag_flag_temp1,Engine_Drag_flag_temp2;	
	int16_t   eng_rpm_filt,eng_rpm_filt_old;
	int16_t	  slope_eng_rpm,slope_eng_rpm_old; 
	uint8_t   gear_state_prev1_old,gear_state_prev1;    /* __EDC_N_ABS_UPPER_GEAR_CLEAR 관련 변수들*/
#endif

#if __EDC
	uint8_t  	EDC_flags;
	int16_t		EDC_cmd; 
	int16_t 	EDC_exit_counter;
	int16_t		EDC_mon1,EDC_mon2,EDC_mon3,EDC_mon4; 
	int16_t		msr_count;
    int16_t  	MSR_cal_torq;
	int16_t		lcedcs16AvgDrivenWheelSlip,lcedcs16TargetWheelSlip;
	int16_t		lcedcs16WheelSlipFiltOld,lcedcs16WheelSlipFilt;
	int16_t		lcedcs16WheelSlipError,lcedcs16WheelSlipErrDiff;
	int16_t		lcedcs16PGain,lcedcs16DGain;
	int16_t		lcedcs16PGainEffect,lcedcs16DGainEffect;
	uint8_t	EDC_Phase_flags,EDC_Inhibit_flags,EDC_Exit_flags;
	int8_t	lcedcs8EdcEntryThreshold1,lcedcs8EdcEntryThreshold2;
	uint8_t	lcedcs16EDCTorqDecRate;
	uint16_t	ledcu161SecCntAfterEDC;
	int8_t	ldedcs8EDCEntryTHR0,ldedcs8EDCEntryTHR1,ldedcs8EDCEntryTHR2;
	int16_t	ldedcs16EDCEntryTHR0Final,ldedcs16EDCEntryTHR1Final,ldedcs16EDCEntryTHR2Final;
	uint8_t	ldedcu8TorqHoldLvInEDC;
    int16_t	ldedcs16WheelLockSuspectCnt;
	int16_t		ldedcs16velgapatEDCentry,ldedcs16velgapatEDCentry2,ldedcs16velgap,ldedcs16velgap2;
	int16_t		ldedcs16ResetTempTireDetectCnt,ldedcs16TempTireTendCntInCtl;
	int16_t    	lcedcs16AvgDrivenWheelSlipOld;			   						/* For __EDC_GEAR_SHIFT_DOWN_DETECTION	*/
	int16_t	   	lcedcs16AvgDrvnWhlSlipDiff , lcedcs16AvgDrvnWhlSlipDiffOld;		/* For __EDC_GEAR_SHIFT_DOWN_DETECTION	*/
	int8_t		ldedcs8GearShiftDownCount;										/* For __EDC_GEAR_SHIFT_DOWN_DETECTION	*/
	int16_t 	ledcs16VelGapBuffer[14],ledcs16VelGapBuffer2[14];
	int16_t		ledcs16VelGapSum,ledcs16VelGapSum2;
	int16_t		ledcs16VelGapIndex;
	int16_t		ldedcs16InitialEDCSlip;
	int16_t		ldedcs16InitialEDCRellam;
	uint8_t		ldedcu8EDCInitialCnt;	
	
  #if ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU) || (__CAR==GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)|| (__CAR ==GM_SILVERADO))  	
	uint8_t	lespu1EngTqMaxExtRngVal;
	int16_t		lesps16EngTqMaxExtRng;	
  #endif	
#endif	

