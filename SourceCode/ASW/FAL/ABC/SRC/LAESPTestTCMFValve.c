
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif

#include "Hardware_Control.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LAESPCallActHW.h"
#include "LACallMain.h"
#include "LAESPTestTCMFValve.h"
#include "LCESPInterfaceTCMF.h"
#include "LAESPActuateTCMFESC.h"

void LAESP_vTCMFValveTest(void);
int16_t LCMSC_vTestTcmfValveTarVol(void);

int16_t tcmf_press_test_count;
int16_t tcmf_test_target_vol;

#if (__VALVE_TEST_TCMF ==1)
void LAESP_vTCMFValveTest(void)
{
	if(fu1TCSDisabledBySW==1)
    {
    	FL_ESC.lcespu1TCMFControl_Old = FL_ESC.lcespu1TCMFControl;
    	FL_ESC.lcespu1TCMFConOld_IniFull = FL_ESC.lcespu1TCMFCon_IniFull;
		FL_ESC.lcespu1TCMFConOld_Rise = FL_ESC.lcespu1TCMFCon_Rise;
		FL_ESC.lcespu1TCMFConOld_Hold = FL_ESC.lcespu1TCMFCon_Hold;
		FL_ESC.lcespu1TCMFConOld_Dump = FL_ESC.lcespu1TCMFCon_Dump;
    	
    	FR_ESC.lcespu1TCMFControl_Old = FR_ESC.lcespu1TCMFControl;
    	FR_ESC.lcespu1TCMFConOld_IniFull = FR_ESC.lcespu1TCMFCon_IniFull;
		FR_ESC.lcespu1TCMFConOld_Rise = FR_ESC.lcespu1TCMFCon_Rise;
		FR_ESC.lcespu1TCMFConOld_Hold = FR_ESC.lcespu1TCMFCon_Hold;
		FR_ESC.lcespu1TCMFConOld_Dump = FR_ESC.lcespu1TCMFCon_Dump;
    	
        tcmf_press_test_count=tcmf_press_test_count+1;
        
        esp_tempW1   = 600          + S16_TCMF_SCAN_INI_FULL;
        esp_tempW2   = esp_tempW1   + S16_TCMF_SCAN_HOLD;
        esp_tempW3   = esp_tempW2   + S16_TCMF_SCAN_RISE_MAX;
        esp_tempW4   = esp_tempW3   + S16_TCMF_SCAN_HOLD;
		esp_tempW5   = esp_tempW4   + S16_TCMF_SCAN_DUMP_FAST;
		esp_tempW6   = esp_tempW5   + S16_TCMF_SCAN_HOLD;
		
		esp_tempW7   = esp_tempW6   + S16_TCMF_SCAN_RISE_STEP_R3;
		esp_tempW8   = esp_tempW7   + S16_TCMF_SCAN_HOLD_R3;
		esp_tempW9   = esp_tempW8   + S16_TCMF_SCAN_DUMP_SLOW_R3;
		esp_tempW10  = esp_tempW9   + S16_TCMF_SCAN_HOLD_R3;
		
		esp_tempW11  = esp_tempW10  + S16_TCMF_SCAN_RISE_STEP_R2;
		esp_tempW12  = esp_tempW11  + S16_TCMF_SCAN_HOLD_R2;
		esp_tempW13  = esp_tempW12  + S16_TCMF_SCAN_DUMP_SLOW_R2;
		esp_tempW14  = esp_tempW13  + S16_TCMF_SCAN_HOLD_R2;
		
		esp_tempW15  = esp_tempW14  + S16_TCMF_SCAN_RISE_STEP_R1;
		esp_tempW16  = esp_tempW15  + S16_TCMF_SCAN_HOLD_R1;
		esp_tempW17  = esp_tempW16  + S16_TCMF_SCAN_DUMP_SLOW_R1;
		esp_tempW18  = esp_tempW17  + S16_TCMF_SCAN_HOLD_R1;
		
		esp_tempW19  = esp_tempW18  + S16_TCMF_SCAN_FADE;
        
        esp_tempW20  = esp_tempW19  + S16_TCMF_SCAN_RESET;
        
        esp_tempW21  = esp_tempW20  + 600;
        
		if((tcmf_press_test_count >500)&&(tcmf_press_test_count <600))
		{
			ESP_BRAKE_CONTROL_FL = 0;
			ESP_BRAKE_CONTROL_FR = 0;
						
			FL_ESC.lcespu1TCMFControl 			   = 0;
			FL_ESC.lcespu1TCMFCon_IniFull     	   = 0;
			FL_ESC.lcespu1TCMFCon_Rise        	   = 0;
			FL_ESC.lcespu1TCMFCon_Rise_MAX    	   = 0;
			FL_ESC.lcespu1TCMFCon_Rise_STEP   	   = 0;
			FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 0;
			FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 0;
			FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 0;
			FL_ESC.lcespu1TCMFCon_Hold        	   = 0;
			FL_ESC.lcespu1TCMFCon_Dump        	   = 0;
			FL_ESC.lcespu1TCMFCon_Dump_FAST   	   = 0;
			FL_ESC.lcespu1TCMFCon_Dump_SLOW   	   = 0;
			
			FR_ESC.lcespu1TCMFControl 			   = 0;
			FR_ESC.lcespu1TCMFCon_IniFull     	   = 0;
			FR_ESC.lcespu1TCMFCon_Rise        	   = 0;
			FR_ESC.lcespu1TCMFCon_Rise_MAX    	   = 0;
			FR_ESC.lcespu1TCMFCon_Rise_STEP   	   = 0;
			FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 0;
			FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 0;
			FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 0;
			FR_ESC.lcespu1TCMFCon_Hold        	   = 0;
			FR_ESC.lcespu1TCMFCon_Dump        	   = 0;
			FR_ESC.lcespu1TCMFCon_Dump_FAST   	   = 0;
			FR_ESC.lcespu1TCMFCon_Dump_SLOW   	   = 0;
			
			FL_ESC.MFC_Current_Inter = 0;
			FR_ESC.MFC_Current_Inter = 0;	
		}
        else if(tcmf_press_test_count >=(600))
        {
        	if(tcmf_press_test_count<(esp_tempW18))
        	{
        		FR_ESC.lcespu1TCMFControl = 1;
        	  	ESP_BRAKE_CONTROL_FR = 1;
        	  	
        		if(tcmf_press_test_count<(esp_tempW1))
            	{
            		FR_ESC.lcespu1TCMFCon_IniFull     = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW2))
            	{
            		FR_ESC.lcespu1TCMFCon_IniFull     = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW3))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Rise        = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_MAX    = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW4))
            	{
            		FR_ESC.lcespu1TCMFCon_Rise        = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_MAX    = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW5))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Dump        = 1;
            		FR_ESC.lcespu1TCMFCon_Dump_FAST   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW6))
            	{
            		FR_ESC.lcespu1TCMFCon_Dump        = 0;
            		FR_ESC.lcespu1TCMFCon_Dump_FAST   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	/*Step Rise Start*/
            	else if(tcmf_press_test_count<(esp_tempW7))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Rise        = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW8))
            	{
            		FR_ESC.lcespu1TCMFCon_Rise        = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW9))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Dump        = 1;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW10))
            	{
            		FR_ESC.lcespu1TCMFCon_Dump        = 0;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW11))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
           		
            		FR_ESC.lcespu1TCMFCon_Rise        = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW12))
            	{
            		FR_ESC.lcespu1TCMFCon_Rise        = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW13))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Dump        = 1;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW14))
            	{
            		FR_ESC.lcespu1TCMFCon_Dump        = 0;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW15))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Rise        = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW16))
            	{
            		FR_ESC.lcespu1TCMFCon_Rise        = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW17))
            	{
            		FR_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Dump        = 1;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}            	
            	else if(tcmf_press_test_count<(esp_tempW18))
            	{
            	    FR_ESC.lcespu1TCMFCon_Dump        = 0;
            		FR_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FR_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	/*Step Rise End*/
            	else
            	{
            			;
            	}
            }
        	else if(tcmf_press_test_count<(esp_tempW19))
            {
        		FR_ESC.lcespu1TCMFControl = 1;
        		
            	FR_ESC.lcespu1TCMFCon_Hold        = 0;
        		FR_ESC.lcespu1TCMFCon_FadeOut = 1;
            }
        	else if(tcmf_press_test_count<(esp_tempW20))
            {
        		FR_ESC.lcespu1TCMFControl = 0;
        		ESP_BRAKE_CONTROL_FR = 0;
        		
        		FR_ESC.lcespu1TCMFCon_FadeOut = 0;
            }
            else if(tcmf_press_test_count<(esp_tempW20+600))
            {
            	;
            }          
            else if(tcmf_press_test_count<(esp_tempW18+esp_tempW20))
        	{
        		FL_ESC.lcespu1TCMFControl = 1;
        	  	ESP_BRAKE_CONTROL_FL = 1;
        	  	
        		if(tcmf_press_test_count<(esp_tempW1+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_IniFull     = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW2+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_IniFull     = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW3+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Rise        = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_MAX    = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW4+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Rise        = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_MAX    = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW5+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Dump        = 1;
            		FL_ESC.lcespu1TCMFCon_Dump_FAST   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW6+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Dump        = 0;
            		FL_ESC.lcespu1TCMFCon_Dump_FAST   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	/*Step Rise Start*/
            	else if(tcmf_press_test_count<(esp_tempW7+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Rise        = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW8+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Rise        = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg3   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW9+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Dump        = 1;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW10+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Dump        = 0;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW11+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
           		
            		FL_ESC.lcespu1TCMFCon_Rise        = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW12+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Rise        = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg2   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW13+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Dump        = 1;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW14+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Dump        = 0;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW15+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Rise        = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 1;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW16+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Rise        = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP   = 0;
            		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg1   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	else if(tcmf_press_test_count<(esp_tempW17+esp_tempW20))
            	{
            		FL_ESC.lcespu1TCMFCon_Hold        = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Dump        = 1;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 1;
            	}            	
            	else if(tcmf_press_test_count<(esp_tempW18+esp_tempW20))
            	{
            	    FL_ESC.lcespu1TCMFCon_Dump        = 0;
            		FL_ESC.lcespu1TCMFCon_Dump_SLOW   = 0;
            		
            		FL_ESC.lcespu1TCMFCon_Hold        = 1;
            	}
            	/*Step Rise End*/
            	else
            	{
            			;
            	}
            }
        	else if(tcmf_press_test_count<(esp_tempW19+esp_tempW20))
            {
        		FL_ESC.lcespu1TCMFControl = 1;
        		
            	FL_ESC.lcespu1TCMFCon_Hold        = 0;
        		FL_ESC.lcespu1TCMFCon_FadeOut = 1;
            }
        	else if(tcmf_press_test_count<(esp_tempW20+esp_tempW20))
            {
        		FL_ESC.lcespu1TCMFControl = 0;
        		ESP_BRAKE_CONTROL_FL = 0;
        		
        		FL_ESC.lcespu1TCMFCon_FadeOut = 0;
            }
            
        	else
            {
        		;
            }
        }        
        else
        {
        	  ;
        }
        
        
        if(tcmf_press_test_count >(50))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            
            if(tcmf_press_test_count<(550))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(tcmf_press_test_count<(600))  /*reset time : 0.5sec*/ //550 ~ 600
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(tcmf_press_test_count<(esp_tempW19))  /*Valve Test : 1.4sec*//*Valve Test : 3.1sec*/ //600 ~ esp_tempW21
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;  						    
                #if (__MGH_80_10MS == ENABLE)  						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						      						
				#endif
								
                VDC_MOTOR_ON=1;
            }
            else if(tcmf_press_test_count<(esp_tempW20))  /*reset time : 2sec*/ //esp_tempW21 ~ esp_tempW22
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(tcmf_press_test_count<(50+esp_tempW20))
            {
            	VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(tcmf_press_test_count<(550+esp_tempW20))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(tcmf_press_test_count<(600+esp_tempW20))  /*reset time : 0.5sec*/ //550 ~ 600
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(tcmf_press_test_count<(esp_tempW19+esp_tempW20))  /*Valve Test : 1.4sec*//*Valve Test : 3.1sec*/ //600 ~ esp_tempW21
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;  						    
                #if (__MGH_80_10MS == ENABLE)  						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						      						
				#endif
								
                VDC_MOTOR_ON=1;
            }
            else if(tcmf_press_test_count<(esp_tempW20+esp_tempW20))  /*reset time : 2sec*/ //esp_tempW21 ~ esp_tempW22
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else
            {
            	VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        tcmf_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }		
}

int16_t LCMSC_vTestTcmfValveTarVol(void)
{	
	if (fu1TCSDisabledBySW==1)
	{
		if((FL_ESC.lcespu1TCMFCon_IniFull ==1)||(FR_ESC.lcespu1TCMFCon_IniFull ==1))
		{
			tcmf_test_target_vol = (int16_t)U8_TCMF_T_VOLT_INI_FULL_VALVE;
		}
		else if((FL_ESC.lcespu1TCMFCon_Rise ==1)||(FR_ESC.lcespu1TCMFCon_Rise ==1))
		{
			tcmf_test_target_vol = (int16_t)U8_TCMF_T_VOLT_RISE_VALVE;
		}
		else if((FL_ESC.lcespu1TCMFCon_Hold ==1)||(FR_ESC.lcespu1TCMFCon_Hold ==1))
		{
			tcmf_test_target_vol = (int16_t)U8_TCMF_T_VOLT_HOLD_VALVE;
		}
        else if((FL_ESC.lcespu1TCMFCon_Dump ==1)||(FR_ESC.lcespu1TCMFCon_Dump ==1))
        {
        	tcmf_test_target_vol = (int16_t)U8_TCMF_T_VOLT_DUMP_VALVE;
        }
        else
        {
        	;
        }
	  	  
	  	VDC_MSC_MOTOR_ON=1;
	}
	else
	{
		tcmf_test_target_vol = MSC_0_V;
		VDC_MSC_MOTOR_ON=0;
	}
	
	vdc_msc_target_vol = tcmf_test_target_vol;
	
	return tcmf_test_target_vol;
}


#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif
