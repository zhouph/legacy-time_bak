#ifndef __LDABSESSDET_H__
#define __LDABSESSDET_H__
/*includes********************************************************************/
#include "LVarHead.h"

#if __ESS

/*Global Type Declaration ****************************************************/
#if (NEW_FM_ENABLE == ENABLE) 
#define ESSF0_Unused7            		ESSF0.bit7
#else
#define ldu1EssSystemFail            	ESSF0.bit7
#endif
#define lcu1EssActiveFlag      			ESSF0.bit6
#define HAZARD_WN_SIGNAL  				ESSF0.bit5
#define lcu1ExdEssActiveFlag  			ESSF0.bit4
#define ldu1ExdEssExitspeedChgFlag		ESSF0.bit3	
#define ldu1EssActByDecel 				ESSF0.bit2
#define ldu1EssActByAbs 				ESSF0.bit1
#define ldu1ESSIgnRisingEdge		 	ESSF0.bit0


#define ESSF1_Unused7            		ESSF1.bit7
#define ESSF1_Unused6      				ESSF1.bit6
#define ESSF1_Unused5  					ESSF1.bit5
#define ESSF1_Unused4  					ESSF1.bit4
#define ESSF1_Unused3					ESSF1.bit3	
#define ESSF1_Unused2 					ESSF1.bit2
#define ESSF1_Unused1 					ESSF1.bit1
#define ldu1EssResetIgnOff1Min		 	ESSF1.bit0


/*Global Extern Variable  Declaration*****************************************/

extern U8_BIT_STRUCT_t    ESSF0 , ESSF1;

extern uint16_t		ldu16vehicleSpdForEmrBrkDet;
extern int16_t	lds16VehicleDecel;
extern uint8_t	ldu8EssState;
extern uint8_t	ldu8EssMode;
extern int16_t	lds16EssDctTimer1;
extern int16_t	lds16EssDctTimer2;
extern uint16_t	ldu16EssHazardLampOnTime;
extern uint16_t	ldu16EssBlsOffTime;
extern uint16_t ldu16EssDecelOffTime;
extern uint16_t	ldu16EssAbsOffTime;
extern int16_t	lds16EssWarningReadyTime;
#if (__ESS_EXTENDED_FN == ENABLE)
extern uint16_t		ldu16vehicleSpdForExdEssDet; 
extern uint8_t 		ldu8ExdEssDctTimer;
extern uint16_t 	ldu16ExdEssExitTimer1;				
extern uint8_t 		ldu8ExdEssExitTimer2;				
extern uint8_t 		ldu8ExdEssflagSetTimer;		
extern uint8_t 		ldu8ExdEssExitTimer3;		
extern uint16_t    	ldu16EssExitSpeed;                  
extern uint16_t		ldu16EssIgnOfftimer;              
#endif
	       

/*Global Extern Functions  Declaration****************************************/
extern void		LDABS_vCallEmergencyBrkMain(void);
#endif
#endif
