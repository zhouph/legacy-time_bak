#ifndef __V_STRUCT_H__
#define __V_STRUCT_H__
 
//  VDC variables

#define VDCFLAG0  *(uint8_t*)&VDCF0
#define VDCFLAG1  *(uint8_t*)&VDCF1
#define VDCFLAG2  *(uint8_t*)&VDCF2
#define VDCFLAG3  *(uint8_t*)&VDCF3
#define VDCFLAG4  *(uint8_t*)&VDCF4
#define VDCFLAG5  *(uint8_t*)&VDCF5
#define VDCFLAG6  *(uint8_t*)&VDCF6
#define VDCFLAG7  *(uint8_t*)&VDCF7
#define VDCFLAG8  *(uint8_t*)&VDCF8
#define VDCFLAG9  *(uint8_t*)&VDCF9
#define VDCFLAG10 *(uint8_t*)&VDCF10
#define VDCFLAG11 *(uint8_t*)&VDCF11
#define VDCFLAG12 *(uint8_t*)&VDCF12
#define VDCFLAG13 *(uint8_t*)&VDCF13
#define VDCFLAG14 *(uint8_t*)&VDCF14
#define VDCFLAG15 *(uint8_t*)&VDCF15
#define VDCFLAG16 *(uint8_t*)&VDCF16
#define VDCFLAG17 *(uint8_t*)&VDCF17
#define VDCFLAG18 *(uint8_t*)&VDCF18
#define VDCFLAG19 *(uint8_t*)&VDCF19
#define VDCFLAG20 *(uint8_t*)&VDCF20
#define VDCFLAG21 *(uint8_t*)&VDCF21
#define VDCFLAG22 *(uint8_t*)&VDCF22
#define VDCFLAG23 *(uint8_t*)&VDCF23
#define VDCFLAG24 *(uint8_t*)&VDCF24
#define VDCFLAG25 *(uint8_t*)&VDCF25
#define VDCFLAG26 *(uint8_t*)&VDCF26
#define VDCFLAG27 *(uint8_t*)&VDCF27
#define VDCFLAG28 *(uint8_t*)&VDCF28
#define VDCFLAG29 *(uint8_t*)&VDCF29
#define VDCFLAG30 *(uint8_t*)&VDCF30
#define VDCFLAG31 *(uint8_t*)&VDCF31
#define VDCFLAG32 *(uint8_t*)&VDCF32
#define VDCFLAG33 *(uint8_t*)&VDCF33
#define VDCFLAG34 *(uint8_t*)&VDCF34
#define VDCFLAG35 *(uint8_t*)&VDCF35
#define VDCFLAG36 *(uint8_t*)&VDCF36

/* VDCFLAG0 */
#define S_VALVE_RIGHT               VDCF0.bit0
#define S_VALVE_LEFT                VDCF0.bit1
#define YAW_CDC_WORK                VDCF0.bit2 
#define VDC_MOTOR_ON                VDCF0.bit3 
#define CDC_WORK_OUT                VDCF0.bit4 
#define T_S_V_RIGHT                 VDCF0.bit5
#define T_S_V_LEFT                  VDCF0.bit6 
#define YAW_CHANGE_LIMIT            VDCF0.bit7 
          
/* VDCFLAG1 */
#define WSTR_360_CHECK              VDCF1.bit0
#define ESP_ON                      VDCF1.bit1 
#define MPRESS_STABLE_FLAG          VDCF1.bit2 
#define MPRESS_BRAKE_ON             VDCF1.bit3 
#define OVER_UNDER_JUMP_DELAY       VDCF1.bit4 
#define OVER_UNDER_JUMP             VDCF1.bit5 
#define VDC_LOW_SPEED               VDCF1.bit6 
#define Flag_UNDER_CONTROL          VDCF1.bit7 

/* VDCFLAG2 */
#define SLIP_CONTROL_NEED_FL        VDCF2.bit0  
#define SLIP_CONTROL_NEED_FR        VDCF2.bit1  
#define SLIP_CONTROL_NEED_RL        VDCF2.bit2  
#define SLIP_CONTROL_NEED_RR        VDCF2.bit3  
#define BACK_DIR_DECT               VDCF2.bit4  
#define vdc_on_flg                  VDCF2.bit5  
#define VDC_REF_WORK                VDCF2.bit6  
#define ESP_SPLIT                   VDCF2.bit7  
            
/* VDCFLAG3 */
#define TCS_SPLIT                   VDCF3.bit0  
#define COUNTER_STEER               VDCF3.bit1  
#define WSTR_360_OK                 VDCF3.bit2  
#define Flag_3rd_model              VDCF3.bit3
#define Flag_REAR_CONTROL           VDCF3.bit4
#define PARTIAL_BRAKE               VDCF3.bit5  
#define INITIAL_BRAKE               VDCF3.bit6  
#define SM1_OR_2_N_SM3_OR_4         VDCF3.bit7  

/* VDCFLAG4 */
#define S_VALVE_PRIMARY             VDCF4.bit0  
#define S_VALVE_SECONDARY           VDCF4.bit1  
#define TCL_DEMAND_PRIMARY          VDCF4.bit2  
#define TCL_DEMAND_SECONDARY        VDCF4.bit3  
#define T_S_V_PRIMARY               VDCF4.bit4  
#define T_S_V_SECONDARY             VDCF4.bit5  
#define TEMP_TCL_DEMAND_PRIMARY     VDCF4.bit6  
#define TEMP_TCL_DEMAND_SECONDARY   VDCF4.bit7  

/* VDCFLAG5 */
#define SPEED_OK_FLG                VDCF5.bit0  
#define SM1_OR_SM2                  VDCF5.bit1  
#define SM3_OR_SM4                  VDCF5.bit2  
#define SM1_N_SM2                   VDCF5.bit3  
#define SM1_VALID                   VDCF5.bit4  
#define SM2_VALID                   VDCF5.bit5  
#define SM3_VALID                   VDCF5.bit6
#define SM4_VALID                   VDCF5.bit7

/* VDCFLAG6 */
#define STR_OFFSET_360_OK           VDCF6.bit0  
#define FS_YAW_OFFSET_OK_FLG        VDCF6.bit1  
#define VDC_UNDER_SWITCH_ERR        VDCF6.bit2  
#define VDC_UNDER_CAN_ERR           VDCF6.bit3  
#define VDC_UNDER_YAW_ERR           VDCF6.bit4  
#define VDC_UNDER_STR_ERR           VDCF6.bit5
#define VDC_UNDER_LAT_ERR           VDCF6.bit6  
#define VDC_UNDER_MP_ERR            VDCF6.bit7
        
/* VDCFLAG7 */
#define VDC_UNDER_BLS_ERR           VDCF7.bit0
#define STEER_AVR_CAL_OK            VDCF7.bit1
#define YAW_CDC_WORK_OLD            VDCF7.bit2  
#define YCW_K                       VDCF7.bit3
#define YCW_A                       VDCF7.bit4  
#define ESP_NOT_WORKING             VDCF7.bit5  
#define PARTIAL_PERFORM             VDCF7.bit6  
#define FLAG_BANK_DETECTED          VDCF7.bit7  

/* VDCFLAG8 */
#define FLAG_HIGH_SPEED             VDCF8.bit0  
#define ACT_TWO_WHEEL               VDCF8.bit1  
#define OVER_UNDER_JUMP_START       VDCF8.bit2  
#define ESP_ERROR_FLG               VDCF8.bit3  
#define FLAG_2WHEEL_START           VDCF8.bit4  
#define FLAG_DELTAYAW_SIGNCHANGE    VDCF8.bit5  
#define MP_AVR1_OK                  VDCF8.bit6  
#define MP_AVR2_OK                  VDCF8.bit7

/* VDCFLAG9 */
#define BRAKE_PEDAL_OFF             VDCF9.bit0  
#define MP_TIMER_DEC_MODE           VDCF9.bit1  
#define MP_TIMER_INC_MODE           VDCF9.bit2  
#define WSTR_360_SUSPECT_FLG        VDCF9.bit3
#define NEED_TO_RECHECK_MPRESS      VDCF9.bit4
#define MORE_THAN_3BAR_CHECK        VDCF9.bit5
#define BS_A                        VDCF9.bit6
#define BS_K                        VDCF9.bit7  

/* VDCFLAG10 */
#define ESP_BS                      VDCF10.bit0 
#define yaw_lg_temp_d_flg           VDCF10.bit1 
#define ABS_PERMIT_BY_BRAKE         VDCF10.bit2 
#define STABLE_MODEL_ADAPT          VDCF10.bit3 
#define BLS_OR_ESP_BS               VDCF10.bit4
#define SAS_CHECK_OK                VDCF10.bit5
#define lcu1US2WHControlOld         VDCF10.bit6 

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
#define laescu1ESCLFCTestActiveRiseFL         VDCF10.bit7
#else
#define FRONT_PRESSURE_HOLD         VDCF10.bit7
#endif 

/* VDCFLAG11 */

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
#define laescu1ESCLFCTestActiveRiseFR          VDCF11.bit0 
#else
#define REAR_PRESSURE_HOLD          VDCF11.bit0 
#endif

#define YAW_SAME_PHASE              VDCF11.bit1
#define SLIP_CONTROL_NEED_FL_old    VDCF11.bit2
#define SLIP_CONTROL_NEED_FR_old    VDCF11.bit3
#define SLIP_CONTROL_NEED_RL_old    VDCF11.bit4 
#define SLIP_CONTROL_NEED_RR_old    VDCF11.bit5 
#define ESP_PULSE_DUMP_FLAG         VDCF11.bit6 
#define FLAG_ON_CENTER              VDCF11.bit7 

/* VDCFLAG12 */
#define FLAG_SLALOM                 VDCF12.bit0 
#define YAW_CHANGE_LIMIT_ACT        VDCF12.bit1 
#define ABS_PERMIT_BY_BRAKE_BLS     VDCF12.bit2 
#define ABS_PERMIT_BY_BRAKE_SLIP    VDCF12.bit3
#define ABS_permit_SLIP_nP_NEG_fl   VDCF12.bit4 
#define ABS_permit_SLIP_nP_NEG_fr   VDCF12.bit5 
#define ABS_permit_SLIP_nP_NEG_rl   VDCF12.bit6 
#define ABS_permit_SLIP_nP_NEG_rr   VDCF12.bit7 

/*  VDCFLAG13(offset1)  */
#define EBD_RA_old                  VDCF13.bit0 
#define EBD_RA_A                    VDCF13.bit1 
#define EBD_RA_K                    VDCF13.bit2 
#define MPRESS_BRAKE_ON_A           VDCF13.bit3
#define MPRESS_BRAKE_ON_K           VDCF13.bit4 
#define MPRESS_BRAKE_ON_old         VDCF13.bit5 
#define BRAKE_PEDAL_ON_A            VDCF13.bit6 
#define FLAG_BIG_OVERSTEER          VDCF13.bit7 

/*  VDCFLAG14(offset2)  */
#define BRAKE_PEDAL_ON_K            VDCF14.bit0 
#define BRAKE_PEDAL_ON_old          VDCF14.bit1 
#define WSTR_360_JUMP               VDCF14.bit2 
#define KPH_15_FLG                  VDCF14.bit3
#define ESP_PULSE_DUMP_FL           VDCF14.bit4
#define ESP_PULSE_DUMP_FR           VDCF14.bit5 
#define ESP_PULSE_DUMP_RL           VDCF14.bit6
#define ESP_PULSE_DUMP_RR           VDCF14.bit7
/*060607 modified and added for FR Split by eslim*/
/*  VDCFLAG15 (SPLIT_TYPE)  */
#define ESP_ROUGH_ROAD              VDCF15.bit0
#define TEMP_ESP_ALAT               VDCF15.bit1
#define VDC_DISABLE_FLG             VDCF15.bit2
#define FLAG_DETECT_DRIFT           VDCF15.bit3
#define ESP_fz                      VDCF15.bit4
#define PRESSURE_DUMP_NEED          VDCF15.bit5
#define SAS_OFFSET_SUSPECT_HOLD     VDCF15.bit6
#define TARGET_SLIP_REACH           VDCF15.bit7
/*060607 modified and added for FR Split by eslim*/
/*  VDCFLAG16(offset3)  */
#define SLIP_CONTROL_ABS_NEED_FL    VDCF16.bit0   // 2003.10.13
#define SLIP_CONTROL_ABS_NEED_FR    VDCF16.bit1
#define SLIP_CONTROL_ABS_NEED_RL    VDCF16.bit2  
#define SLIP_CONTROL_ABS_NEED_RR    VDCF16.bit3
#define FLAG_COUNTER_STEER          VDCF16.bit4
#define FLAG_3WHEEL_CONTROL         VDCF16.bit5
#define ESP_BRAKE_CONTROL           VDCF16.bit6  

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
#define laescu1ESCLFCTestActiveRiseRL        VDCF16.bit7
#else
#define FLAG_COMPULSION_RISE        VDCF16.bit7
#endif  
             
/*  VDCFLAG17(offset4)  */
#define PRE_ESP_CONTROL             VDCF17.bit0
#define ACT_UNDER_2WHEEL            VDCF17.bit1
#define Low_Brk_Fluid_Enter	        VDCF17.bit2
#define YAW_G_GND_OPEN_FLG          VDCF17.bit3
#define BACK_DIR_RECHECK            VDCF17.bit4  
#define ESP_MINI_TIRE_SUSPECT       VDCF17.bit5  
#define ESP_MINI_TIRE               VDCF17.bit6

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
#define laescu1ESCLFCTestActiveRiseRR              VDCF17.bit7
#else
#define FULL_RISE_MODE              VDCF17.bit7
#endif


/*  VDCFLAG18(offset5)  */
#define INITIAL_BRAKE_old           VDCF18.bit0  
#define EXTREME_UNDER_CONTROL       VDCF18.bit1  
#define EXTR_UNDER_FL_IN            VDCF18.bit2  
#define S_VALVE_PRIMARY_TEMP        VDCF18.bit3
#define S_VALVE_SECONDARY_TEMP      VDCF18.bit4
#define lcu1espActive3rdFlg         VDCF18.bit5
#define EXTR_UNDER_FR_IN            VDCF18.bit6
#define DECLAMP_ON                  VDCF18.bit7  

/*  VDCFLAG19(partial perform)  */  
#define SLALOM_TORQ_DOWN            VDCF19.bit0
#define EXTREME_UNDER_CTRL_FADE_OUT VDCF19.bit1    // EEPROM 상태
#define SEVERE_CORNERG              VDCF19.bit2
#define YAW_CDC_WORK_U              VDCF19.bit3  
#define YAW_CDC_WORK_O              VDCF19.bit4  
#define ACT_UNDER_YAW_2WHEEL        VDCF19.bit5
#define FLAG_BETA_UNDER_DRIFT       VDCF19.bit6
#define Flg_1st_cycle_Ay            VDCF19.bit7  

/*  VDCFLAG20 */
#define ABS_PERMIT_BY_BRAKE_BLS_K   VDCF20.bit0
#define ABS_PERMIT_BY_BRAKE_BLS_A   VDCF20.bit1  
#define ABS_PERMIT_BY_BRAKE_BLS_OLD VDCF20.bit2
#define UCC_AFS_COMBINATION         VDCF20.bit3  
#define FLAG_INHIBIT_OFST_TURN      VDCF20.bit4  
#define OVER_JUMP_UNDER_CONTROL     VDCF20.bit5  
#define tempflag                    VDCF20.bit6  
#define GM_ENGINE_OFF_CONFIRM_FLAG  VDCF20.bit7

/*  VDCFLAG21(offset6) */
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define FLAG_NULL_DUMP_R            VDCF21.bit0
//#define FLAG_COMPULSION_HOLD        VDCF21.bit1
#else
#define FLAG_NULL_DUMP_R            VDCF21.bit0  
#define FLAG_COMPULSION_HOLD        VDCF21.bit1  
#endif  

#define YAW_LIMIT_BETAD             VDCF21.bit2  // 2003.03.26추가

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define FLAG_COMPULSION_HOLD_REAR   VDCF21.bit3
#else
#define FLAG_COMPULSION_HOLD_REAR   VDCF21.bit3  
#endif

#define FLAG_BANK_SUSPECT           VDCF21.bit4  
#define BETA_GAIN                   VDCF21.bit5  
#define FLAG_ESP_CONTROL_ACTION     VDCF21.bit6  
#define AY_WSTR_SAME_PHASE          VDCF21.bit7

/* VDCFLAG22(mpress offset) */                           // 2003.06.16추가
#define ESP_PULSE_DUMP_FLAG_R       VDCF22.bit0
#define ESP_PULSE_DUMP_FLAG_F       VDCF22.bit1
#define MDL_2ND_LMT_ENTER           VDCF22.bit2
#define ABS_fl_old                  VDCF22.bit3
#define ABS_fr_old                  VDCF22.bit4
#define ABS_rl_old                  VDCF22.bit5  
#define ABS_rr_old                  VDCF22.bit6

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define mpress_rise_mode            VDCF22.bit7
#else
#define mpress_rise_mode            VDCF22.bit7
#endif


/* VDCFLAG23(circuit fail) */                           // 2003.06.16추가
#define FLAG_TWO_WHEEL_RL           VDCF23.bit0
#define FLAG_TWO_WHEEL_RR           VDCF23.bit1
#define Flg_ESC_SW_ROP_OK           VDCF23.bit2
#define BRAKE_PEDAL_ON              VDCF23.bit3
#define NC_VALVE_DUTY_CONTROL_FRONT VDCF23.bit4
#define NC_VALVE_DUTY_CONTROL_REAR  VDCF23.bit5
#define FLAG_ACTIVE_BRAKING         VDCF23.bit6
#define STEER_STABLE_OK             VDCF23.bit7  // 2004.04.30

/* VDCFLAG24(PARTIAL PERFORMANCE) */                           // 2003.10.17추가
#define yaw_lg_temp_still_flg       VDCF24.bit0
#define yaw_ofst_max_wr_flg         VDCF24.bit1
#define yaw_ofst_min_wr_flg         VDCF24.bit2
#define yaw_ofst_max_rd_flg         VDCF24.bit3
#define yaw_ofst_min_rd_flg         VDCF24.bit4
#define yaw_ofst_max_init_flg       VDCF24.bit5  
#define yaw_ofst_min_init_flg       VDCF24.bit6
#define yaw_first_ok_flg            VDCF24.bit7

/* VDCFLAG25*/                   //         2003.10.17추가
#define yaw_standstill_ok_flg       VDCF25.bit0
#define yaw_ofst_max_ok_flg         VDCF25.bit1
#define yaw_ofst_min_ok_flg         VDCF25.bit2
#define Flg_ESC_SW_ABS_OK           VDCF25.bit3  
#define IN_BRAKE_CONTROL            VDCF25.bit4  
#define OUT_BRAKE_CONTROL           VDCF25.bit5
#define ESP_COMBINATION_CONTROL     VDCF25.bit6
#define Reverse_Steer_Flg           VDCF25.bit7
        
/* VDCFLAG26(ABS_PERMIT_BY_BRAKE) */                   // 2003.11.11추가
#define SLIP_CONTROL_BRAKE_FL       VDCF26.bit0
#define SLIP_CONTROL_BRAKE_FR       VDCF26.bit1
#define SLIP_CONTROL_BRAKE_RL       VDCF26.bit2
#define SLIP_CONTROL_BRAKE_RR       VDCF26.bit3
#define ESP_BRAKE_CONTROL_FL        VDCF26.bit4
#define ESP_BRAKE_CONTROL_FR        VDCF26.bit5
#define ESP_BRAKE_CONTROL_RL        VDCF26.bit6  
#define ESP_BRAKE_CONTROL_RR        VDCF26.bit7  

/* VDCFLAG27(PARTIAL PERFORMANCE) */                   // 2003.10.17추가

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define FLAG_1ST_CYCLE_FL_PRE       VDCF27.bit0                 //원래 있던것..나머지는 모두 추가.
//#define FLAG_1ST_CYCLE_FL           VDCF27.bit1
//#define FLAG_1ST_CYCLE_FR_PRE       VDCF27.bit2  
//#define FLAG_1ST_CYCLE_FR           VDCF27.bit3
#else
#define FLAG_1ST_CYCLE_FL_PRE       VDCF27.bit0                 //원래 있던것..나머지는 모두 추가.
#define FLAG_1ST_CYCLE_FL           VDCF27.bit1
#define FLAG_1ST_CYCLE_FR_PRE       VDCF27.bit2  
#define FLAG_1ST_CYCLE_FR           VDCF27.bit3  
#endif

#define Beta_Moment_Ctrl_Flg        VDCF27.bit4  
#define Dct_Vehicle_Low_Speed       VDCF27.bit5  
#define ESC_PULSE_UP_RISE_FRONT     VDCF27.bit6
#define ESC_PULSE_UP_RISE_REAR      VDCF27.bit7  

/* VDCFLAG28(PARTIAL PERFORMANCE) */                   // 2003.10.17추가
#define PARTIAL_BRAKE_fl            VDCF28.bit0  
#define PARTIAL_BRAKE_fr            VDCF28.bit1
#define PARTIAL_BRAKE_rl            VDCF28.bit2
#define PARTIAL_BRAKE_rr            VDCF28.bit3
#define FLAG_ACT_PRE_ACTION_RL      VDCF28.bit4
#define FLAG_ACT_PRE_ACTION_RL_PRE  VDCF28.bit5
#define FLAG_ACT_PRE_ACTION_RR      VDCF28.bit6
#define FLAG_ACT_PRE_ACTION_RR_PRE  VDCF28.bit7
/* VDCFLAG29*/
#define P_HV_VL_fl                  VDCF29.bit0
#define P_AV_VL_fl                  VDCF29.bit1
#define P_HV_VL_fr                  VDCF29.bit2  
#define P_AV_VL_fr                  VDCF29.bit3  
#define ESC_PULSE_UP_RISE_FL                  VDCF29.bit4  
#define ESC_PULSE_UP_RISE_FR                  VDCF29.bit5
#define ESC_PULSE_UP_RISE_RL                  VDCF29.bit6
#define ESC_PULSE_UP_RISE_RR                  VDCF29.bit7  
//#define P_HV_VL_rl                  VDCF29.bit4  
//#define P_AV_VL_rl                  VDCF29.bit5  
//#define P_HV_VL_rr                  VDCF29.bit6  
//#define P_AV_VL_rr                  VDCF29.bit7  

/* VDCFLAG30*/
#define Flg_Flat_Tire_Detect_OK     VDCF30.bit0
#define P_VDC_MOTOR_ON              VDCF30.bit1  
#define P_S_VALVE_LEFT              VDCF30.bit2  
#define P_S_VALVE_RIGHT             VDCF30.bit3  
#define P_TCL_DEMAND_fl             VDCF30.bit4  
#define P_TCL_DEMAND_fr             VDCF30.bit5
#define FLAG_TWO_WHEEL              VDCF30.bit6  
#define Yaw_Sign_Change_Hold_Flg    VDCF30.bit7
        
/* VDCFLAG31*/
#define Flg_Make_Oversteer_By_Ay    VDCF31.bit0
#define FLAG_ACTIVE_BOOSTER         VDCF31.bit1
#define FLAG_ACT_PRE_ACTION_FL      VDCF31.bit2
#define FLAG_ACT_PRE_ACTION_FR      VDCF31.bit3
#define FLAG_ACT_PRE_ACTION_FL_PRE  VDCF31.bit4  
#define FLAG_ACT_PRE_ACTION_FR_PRE  VDCF31.bit5  
#define FLAG_ACTIVE_BOOSTER_PRE     VDCF31.bit6
#define Mu_delyaw_Reach_Flg         VDCF31.bit7
                   
/* VDCFLAG32*/

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define FLAG_1ST_CYCLE_RL_PRE       VDCF32.bit0
//#define FLAG_1ST_CYCLE_RL           VDCF32.bit1
//#define FLAG_1ST_CYCLE_RR_PRE       VDCF32.bit2
//#define FLAG_1ST_CYCLE_RR           VDCF32.bit3
#else
#define FLAG_1ST_CYCLE_RL_PRE       VDCF32.bit0
#define FLAG_1ST_CYCLE_RL           VDCF32.bit1
#define FLAG_1ST_CYCLE_RR_PRE       VDCF32.bit2
#define FLAG_1ST_CYCLE_RR           VDCF32.bit3  
#endif
  
#define FLAG_ACT_COMB_CNTR_FL       VDCF32.bit4  
#define FLAG_ACT_COMB_CNTR_FR       VDCF32.bit5  
#define FLAG_ACT_COMB_CNTR_RL       VDCF32.bit6
#define FLAG_ACT_COMB_CNTR_RR 		VDCF32.bit7

/* VDCFLAG33*/

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define FLAG_1ST_CYCLE_WHEEL_FL     VDCF33.bit0
//#define FLAG_1ST_CYCLE_WHEEL_FL_PRE VDCF33.bit1  
//#define FLAG_1ST_CYCLE_WHEEL_FR     VDCF33.bit2
//#define FLAG_1ST_CYCLE_WHEEL_FR_PRE VDCF33.bit3
//#define FLAG_1ST_CYCLE_WHEEL_RL     VDCF33.bit4
//#define FLAG_1ST_CYCLE_WHEEL_RL_PRE VDCF33.bit5  
//#define FLAG_1ST_CYCLE_WHEEL_RR     VDCF33.bit6
//#define FLAG_1ST_CYCLE_WHEEL_RR_PRE VDCF33.bit7
#else
#define FLAG_1ST_CYCLE_WHEEL_FL     VDCF33.bit0
#define FLAG_1ST_CYCLE_WHEEL_FL_PRE VDCF33.bit1  
#define FLAG_1ST_CYCLE_WHEEL_FR     VDCF33.bit2
#define FLAG_1ST_CYCLE_WHEEL_FR_PRE VDCF33.bit3
#define FLAG_1ST_CYCLE_WHEEL_RL     VDCF33.bit4
#define FLAG_1ST_CYCLE_WHEEL_RL_PRE VDCF33.bit5  
#define FLAG_1ST_CYCLE_WHEEL_RR     VDCF33.bit6
#define FLAG_1ST_CYCLE_WHEEL_RR_PRE VDCF33.bit7  
#endif
 

/* VDCFLAG34*/                                  
#define PC_VALVE_RIGHT              VDCF34.bit0 
#define PC_VALVE_LEFT               VDCF34.bit1 
#define FLAG_3WHEEL_ROP_CONTROL     VDCF34.bit2 
#define PHZ_detected_flg2           VDCF34.bit3 
#define EXTR_UNDER_INHIBIT_FLAG      VDCF34.bit4 
#define EXTR_UNDER_FRT_FL_SLIP_1ST      VDCF34.bit5 
#define EXTR_UNDER_FRT_FR_SLIP_1ST      VDCF34.bit6 
#define ROUGH_ROAD_FOR_YAW      VDCF34.bit7 
        
/* VDCFLAG35*/                                  
#define Flg_ESC_OFF_SLEEP_MODE_CBS_OK  VDCF35.bit0 
#define FLAG_TIME_OPT                  VDCF35.bit1 
#define FLAG_TIME_OPT_1_3             VDCF35.bit2 
#define FLAG_TIME_OPT_2_3             VDCF35.bit3 
#define FLAG_TIME_OPT_3_3             VDCF35.bit4 
#define Flg_Yaw_Sign_Ch_Th_1             VDCF35.bit5 
#define Flg_Yaw_Sign_Ch_Th_2             VDCF35.bit6 
#define FLAG_ACT_PRE_ACTION_CDM          VDCF35.bit7

/* VDCFLAG36*/
#define lcespu1EECFadeoutEnd            VDCF36.bit0 
#define lcescu1EECFadeOutOld            VDCF36.bit1 
#define ldu1US2WHInhibitFlag            VDCF36.bit2 
#define lcu1US2WHControlFlag            VDCF36.bit3 
#define lcu1US2WHControlFlag_RL         VDCF36.bit4 
#define lcu1US2WHControlFlag_RR         VDCF36.bit5 
#define lcu1US2WHFadeout_RL               VDCF36.bit6 
#define lcu1US2WHFadeout_RR               VDCF36.bit7

/* VDCFLAG37*/
#define lcu1OVER2WHControlOld           VDCF37.bit0
#define lcu1OVER2WHControlFlag          VDCF37.bit1
#define lcu1OVER2WHControlFlag_RL       VDCF37.bit2
#define lcu1OVER2WHControlFlag_RR       VDCF37.bit3
#define ldu1OVER2WHInhibitFlag          VDCF37.bit4
#define lcu1OVER2WHFadeout_RL           VDCF37.bit5
#define lcu1OVER2WHFadeout_RR           VDCF37.bit6
#define lcu1US3WHControlFlag            VDCF37.bit7
/* VDCFLAG38*/
#define lcu1OVER2WHFadeoutOld_RL        VDCF38.bit0
#define lcu1OVER2WHFadeoutOld_RR        VDCF38.bit1
#define lcu1ABSCBSCombCntrl_FL	        VDCF38.bit2
#define lcu1ABSCBSCombCntrl_FR	        VDCF38.bit3
#define lcu1ABSCBSCombCntrl_RL	        VDCF38.bit4
#define lcu1ABSCBSCombCntrl_RR	        VDCF38.bit5
#define lsespu1AHBGEN3MpresBrkOn	      VDCF38.bit6
/* VDCFLAG39*/
#define SLIP_CONTROL_BRAKE_FL_OLD       VDCF39.bit0
#define SLIP_CONTROL_BRAKE_FR_OLD       VDCF39.bit1
#if(__ESC_CYCLE_DETECTION)
#define ldespu1CycleDetQualified        VDCF39.bit2
#define ldespu1ESC1stCycle	            VDCF39.bit3
#define ldespu1ESC2ndCycle	            VDCF39.bit4
#endif
#define lcu1OVER2WHControlOld_RL        VDCF39.bit5
#define lcu1OVER2WHControlOld_RR        VDCF39.bit6

#define ldescu1UnmotivatedDetect        VDCF40.bit0
#define ldescu1UnmotivatedReduction     VDCF40.bit1
#define SLIP_CONTROL_BRAKE_RL_OLD    	VDCF40.bit2
#define SLIP_CONTROL_BRAKE_RR_OLD  	    VDCF40.bit3
#if(__IDB_LOGIC == ENABLE)
#define U1WheelValveCntr                VDCF40.bit4
#define U1StrokeRecoveryforESC  	    VDCF40.bit5
#define U1StrokeRecoveryOk              VDCF40.bit6
#else
#define vdcf_reserved_40_4  	        VDCF40.bit4
#define vdcf_reserved_40_5  	        VDCF40.bit5
#define vdcf_reserved_40_6              VDCF40.bit6
#endif
#define vdcf_reserved_40_7              VDCF40.bit7

struct  OFFSET_STRUCT
{
    uint8_t vdc_sensor_check_cnt;  // extern uint8_t vdc_wstr_sensor_check_cnt;
    uint8_t offset_ok_cnt;
    uint8_t STRAIGHT;
    uint8_t STRAIGHT2;
    uint8_t mdl_ok_num;
    uint8_t eeprom_write_per_ign;

    int16_t ALGO_MODEL[5], ALGO_MODEL_old[5];
    int16_t model_sens_diff_offset;
    int32_t sum_model[5];           // EINT sum_STEER_model[5], sum_steer_correction, sum_steer_1_10deg;
    int16_t avr_model[5];
    int32_t sum_measured_by_sens;
    int16_t avr_measured_by_sens;
    int16_t offset_10avr,offset_10avr_old;
    int32_t offset_10sum;
    int16_t mean_offset_at_model, offset_at_15kph;
    int16_t measured_by_sens;
    int16_t measured_by_sens_old;
    int16_t raw_signal,raw_signal_old,raw_signal2;
    int16_t model_offset;
    int16_t eeprom_update_TH;
    int16_t offset_diff;
    int16_t offset_of_eeprom;
    int16_t fs_measured_offset;
    int16_t str_360_offset_at_quick;
    int16_t min, max_2nd, max_3rd, max, model_avr,model_avr2, model_diff,diff_model_to_sens;
    uint8_t mdl_suspect_cnt, pulse_suspect_cnt, suspect_delay_timer, straight_cnt;
    int16_t offset_at_15kph_nth;
    int32_t offset_nth_sum;
    int16_t offset_cal_max;
    //---------------------------
    int16_t offset_10avr_arr[6];
    int16_t offset_10_cnt;
    int16_t offset_60avr,offset_60avr_old;
    int16_t offset_10avr_first;
    int16_t threshold_of_eeprom;
    int16_t offset_10_sec_cnt;
    int32_t offset_60sum;
    //---------------------------
    int16_t offset_of_eeprom2;
    int16_t eeprom_update_limit;
    int16_t offset_filter,offset_filter_old;
    //---------------------------
    int8_t driving_accuracy,first_driving_accuracy,deviation_driving_accuracy,standstill_accuracy,eeprom_accuracy,driving_accuracy_stable;
    uint16_t deviation_driving,deviation_driving_sum,deviation_driving_threshold;

    int16_t offset_still[6];
    uint8_t offset_still_cnt,offset_still_cnt2;
    uint16_t eep_cnt;
    uint16_t eep_cnt2;
    //----------------------------
    int16_t eep_max,eep_min,eep_max_w,eep_min_w;
    int16_t offset_60_avr_limit;

    unsigned SEN_OFFSET_CAL_OK_FLG:              1;   /* bit 7  */
    unsigned OFFSET_10AVR_CAL_OK_FLG:            1;   /* bit 7  */
    unsigned NEED_TO_WR_OFFSET_FLG:              1;   /* bit 7  */
    unsigned FIRST_OFFSET_CAL_FLG:               1;   /* bit 7  */
    unsigned SUSPECT_FLG:                        1;   /* bit 7  */
    unsigned MODEL_OK_AT_15KPH:                  1;   /* bit 7  */
    unsigned NTH_OFFSET_CAL_FLG:                 1;   /* bit 7  */
    unsigned PULSE_SUSPECT_FLAG:                 1;   /* bit 7  */

    unsigned M1_FRONT_OK:                    1;   /* bit 7  */
    unsigned M2_REAR_OK:                     1;   /* bit 7  */
    unsigned M3_LAT_YAW_STR:                 1;   /* bit 7  */
    unsigned M4_LAT_YAW_STR:                 1;   /* bit 7  */
    unsigned MDL_SUSPECT_FLG:                1;   /* bit 7  */
    unsigned SUSPECT_DELAY:                  1;   /* bit 7  */
    unsigned STRAIGHT_FLAG:                  1;   /* bit 7  */
    unsigned DURING_STRAIGHT_FLG:            1;   /* bit 7  */
    unsigned OFFSET_AT_15KPH_NTH_OK:         1;   /* bit 7  */

    unsigned PULSE_SUSPECT_FLAG_F:           1;   /* bit 7  */
    unsigned PULSE_SUSPECT_FLAG_F_DELAY:     1;   /* bit 7  */
    unsigned FAILSAFE_SUSPECT_FLAG:          1;   /* bit 7  */
    unsigned EEPROM_SUSPECT_FLG:             1;   /* bit 7  */
//------------------------------------------------------------
    unsigned OFFSET_60AVR_CAL_OK_FLG:            1;   /* bit 7  */
    unsigned OFFST_10AVR_EEPROM:                    1;   /* bit 7  */
    unsigned fs_eeprom_ok_flg:                  1;   /* bit 7  */
//------------------------------------------------------------
    unsigned eep_ok_standstill:                 1;
    unsigned standstill_old:                    1;
    unsigned eep_max_w_flg:                     1;
    unsigned eep_min_w_flg:                     1;
    unsigned eep_max_ok_flg:                        1;
    unsigned eep_min_ok_flg:                        1;
    unsigned eep_max_init_flg:                      1;
    unsigned eep_min_init_flg:                      1;
    unsigned eep_max_read_flg:                      1;
    unsigned eep_min_read_flg:                      1;
    unsigned eep_first_ok_flg:                      1;

    unsigned second_straight_flg:                       1;
};

struct ARRANGE_STRUCT
{
    uint8_t lavel;
    int16_t   content;
};

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
#define slip_m_fl     FL_ESC.slip_m
#define slip_m_fr     FR_ESC.slip_m
#define slip_m_rl     RL_ESC.slip_m
#define slip_m_rr     RR_ESC.slip_m

#define lcescs16CalWheelSpeedRatefl     FL_ESC.lcescs16CalWheelSpeedRate
#define lcescs16CalWheelSpeedRatefr     FR_ESC.lcescs16CalWheelSpeedRate
#define lcescs16CalWheelSpeedRaterl     RL_ESC.lcescs16CalWheelSpeedRate
#define lcescs16CalWheelSpeedRaterr     RR_ESC.lcescs16CalWheelSpeedRate

#define FLAG_1ST_CYCLE_FL     FL_ESC.FLAG_1ST_CYCLE
#define FLAG_1ST_CYCLE_FR     FR_ESC.FLAG_1ST_CYCLE
#define FLAG_1ST_CYCLE_RL     RL_ESC.FLAG_1ST_CYCLE
#define FLAG_1ST_CYCLE_RR     RR_ESC.FLAG_1ST_CYCLE

#define FLAG_1ST_CYCLE_WHEEL_FL     FL_ESC.FLAG_1ST_CYCLE_WHEEL
#define FLAG_1ST_CYCLE_WHEEL_FR     FR_ESC.FLAG_1ST_CYCLE_WHEEL
#define FLAG_1ST_CYCLE_WHEEL_RL     RL_ESC.FLAG_1ST_CYCLE_WHEEL
#define FLAG_1ST_CYCLE_WHEEL_RR     RR_ESC.FLAG_1ST_CYCLE_WHEEL

#define esp_pressure_count_fl         FL_ESC.esp_pressure_count
#define esp_pressure_count_fr         FR_ESC.esp_pressure_count
#define esp_pressure_count_rl         RL_ESC.esp_pressure_count
#define esp_pressure_count_rr         RR_ESC.esp_pressure_count

#define esp_control_mode_fl           FL_ESC.esp_control_mode
#define esp_control_mode_fr           FR_ESC.esp_control_mode
#define esp_control_mode_rl           RL_ESC.esp_control_mode
#define esp_control_mode_rr           RR_ESC.esp_control_mode

#define esp_control_mode_old_fl     FL_ESC.esp_control_mode_old
#define esp_control_mode_old_fr     FR_ESC.esp_control_mode_old
#define esp_control_mode_old_rl     RL_ESC.esp_control_mode_old
#define esp_control_mode_old_rr     RR_ESC.esp_control_mode_old

#define slip_target_fl                FL_ESC.slip_target
#define slip_target_fr                FR_ESC.slip_target
#define slip_target_rl                RL_ESC.slip_target
#define slip_target_rr                RR_ESC.slip_target

#define slip_control_need_counter_fl     FL_ESC.slip_control_need_counter
#define slip_control_need_counter_fr     FR_ESC.slip_control_need_counter
#define slip_control_need_counter_rl     RL_ESC.slip_control_need_counter
#define slip_control_need_counter_rr     RR_ESC.slip_control_need_counter

//--------------------------------------//
#define TEMP_HV_VL_fl                    FL_ESC.TEMP_HV_VL
#define TEMP_AV_VL_fl                    FL_ESC.TEMP_AV_VL

#define TEMP_HV_VL_fr                    FR_ESC.TEMP_HV_VL
#define TEMP_AV_VL_fr                    FR_ESC.TEMP_AV_VL

#define TEMP_HV_VL_rl                    RL_ESC.TEMP_HV_VL
#define TEMP_AV_VL_rl                    RL_ESC.TEMP_AV_VL

#define TEMP_HV_VL_rr                    RR_ESC.TEMP_HV_VL
#define TEMP_AV_VL_rr                    RR_ESC.TEMP_AV_VL

//-------------------------------------------------------------------//
#define slip_target_prev_fl              FL_ESC.slip_target_prev
#define slip_target_prev_fr              FR_ESC.slip_target_prev
#define slip_target_prev_rl              RL_ESC.slip_target_prev
#define slip_target_prev_rr              RR_ESC.slip_target_prev

#define lcespCalRiserateAddDutyfl        FL_ESC.lcespCalRiserateAddDuty
#define lcespCalRiserateAddDutyfr        FR_ESC.lcespCalRiserateAddDuty

#define lcs16EscInitialPulseupDutyfl     FL_ESC.lcs16EscInitialPulseupDuty
#define lcs16EscInitialPulseupDutyfr     FR_ESC.lcs16EscInitialPulseupDuty
#define lcs16EscInitialPulseupDutyrl     RL_ESC.lcs16EscInitialPulseupDuty
#define lcs16EscInitialPulseupDutyrr     RR_ESC.lcs16EscInitialPulseupDuty

#define lcs16EscInitialPulseupDutyfl_old   FL_ESC.lcs16EscInitialPulseupDuty_old
#define lcs16EscInitialPulseupDutyfr_old   FR_ESC.lcs16EscInitialPulseupDuty_old
#define lcs16EscInitialPulseupDutyrl_old   RL_ESC.lcs16EscInitialPulseupDuty_old
#define lcs16EscInitialPulseupDutyrr_old   RR_ESC.lcs16EscInitialPulseupDuty_old

struct ESC_STRUCT_COMB
{
    unsigned FLAG_ACT_COMB_CNTR:                    1;
};

struct ESC_CIRCUIT_STRUCT
{
	int16_t s16CircPressRaw;
	int16_t s16CircPressFilt;
};

struct ESC_STRUCT
{
	unsigned FLAG_1ST_CYCLE:                        1;
	unsigned FLAG_1ST_CYCLE_PRE:                    1;
	unsigned FLAG_1ST_CYCLE_WHEEL:                  1;
	unsigned FLAG_1ST_CYCLE_WHEEL_PRE:              1;
	unsigned SLIP_CONTROL_NEED:                     1;
	unsigned FLAG_COMPULSION_RISE:                  1;
	unsigned FLAG_COMPULSION_HOLD:                  1;
	unsigned FULL_RISE_MODE:                        1;
	unsigned TEMP_HV_VL:                            1;
	unsigned TEMP_AV_VL:                            1;
	unsigned FLAG_NULL_DUMP:                        1;
	unsigned lcu1ropRearCtrlReq:                    1;
	unsigned mpress_rise_mode:                      1;
	unsigned REAR_PRESSURE_HOLD:                    1;
#if  (__IDB_LOGIC == ENABLE)
	unsigned lcespu1DctMuxPrio:                     1;
	unsigned ESP_BRK_CTRL:							1;
	unsigned U1EscDelTpComp:						1;

    unsigned U1EscActFrtTwoWheel; 
    unsigned U1StrWheelValveCnt;
    int8_t  debuger;
    int8_t  lcespu8CntBbsEsc;
    int8_t  lcespu8CtrlMode;
    int16_t lcesps16IdbTarPress;
    int16_t lcesps16IdbDelTarPress;
    int16_t lcesps16IdbTarPRate;
    int16_t lcesps16FinalTarPRate;    
    int16_t lcesps16IdbTarPressold;
    int16_t lcesps16IdbCurrentPress;

    int16_t lcesps16LimitIdbTargetP;
    int16_t lcesps16MinIdbTargetP;
    int16_t lcesps16FadedelP;
    int16_t lcescs16AllowStrRetime;
    int16_t lcesps16EstpressOld;
	int16_t lcesps16CompDp;
	
    int16_t lcesps16IdbRiseInterval;
    int16_t lcesps16IdbDumpInterval;
    int16_t lcesps16IdbriseDelTp;
    int16_t lcesps16IdbdumpDelTp;
    uint8_t lcespu8IdbRiseRateCnt;
    uint8_t lcespu8IdbDumpRateCnt; 
    
    int16_t lcesps16StoreDeltTp;
    uint8_t u8WhlVlvRiseTi;
    uint8_t u8WhlVlvRiseExtdTi;
    uint8_t u8WhlVlvRiseCmdTi;
	uint8_t u8WhlVlvRiseExtdIndex;
	uint8_t u8WhlVlvRiseExtdScan;

    uint8_t u8WhlVlvDumpTi;
    uint8_t u8WhlVlvDumpExtdTi;
    uint8_t u8WhlVlvDumpCmdTi;
	uint8_t u8WhlVlvDumpExtdIndex;
	uint8_t u8WhlVlvDumpExtdScan;

    int16_t s16CircPressFilt;
#endif	
			
	int16_t slip_m;                        
	int16_t slip_m_old;
	int16_t wvref;                             
	int16_t vrad_crt_resol_change;
	int16_t wvref_resol_change;
	int16_t vrad_crt;
	
	int16_t lcescs16CalWheelSpeedRateold;   
	int16_t lcescs16CalWheelSpeedRate;
	int16_t lcescs16CalDelWheelSpeedRate;
	int16_t lcescs16CalWheelSpeedRatear[4];
	
	int16_t act_1st_cy_cnt;                 
	int16_t true_slip_target;
	int16_t slip_target;
	
	uint8_t esp_control_mode;     
	int16_t esp_pressure_count;
	
	uint8_t slip_m_lock_cont; 
	
	int16_t slip_control_need_counter;
	
	uint8_t hold_counter;   
	
	int16_t arad;     
	
	int16_t rate_counter;  
	int16_t rate_interval;
	int16_t Extr_Under_Frt_Press_Max;
	int8_t   rop_rear_ctrl_mode;
	
	int16_t slip_error;
	int16_t slip_measurement;
	int16_t lcescs16CalDelTargetSlip;
	int16_t slip_error_dot;
	int16_t slip_measurement_prev;
	int16_t slip_error_dot_prev;
	int16_t slip_error_prev;
	int16_t slip_target_prev;
	
	int16_t slip_control; 
	
	int16_t del_slip_gain;
	int16_t del_wheel_sp_gain;
	int16_t slip_rise_thr_change;
	int16_t slip_dump_thr_change;
	uint8_t dump_counter_assurance_flag;
	
	int16_t lcescCalRiserateCountdepDelPF; 
	int16_t lcespCalRiserateAddDuty;
	int16_t lcespCaldelDuty;
	int16_t lcs16EsctTimePulseupDuty;
	int16_t lcs16EscInitialPulseupDuty;
	int16_t lcs16EscInitialPulseupDuty_old;
	int16_t lcescCalRiserateCountdepDelP_old;
	int16_t lcespCalRiserateAddDuty_old;
	
	uint8_t esp_control_mode_old; 

#if (__ESC_TCMF_CONTROL ==1)
  unsigned lcespu1TCMFControl:               1;
  unsigned lcespu1TCMFControl_Old:           1;
  
  unsigned lcespu1TCMFCon_IniFull:           1;
  unsigned lcespu1TCMFCon_Rise:              1;
  unsigned lcespu1TCMFCon_Hold:              1;
  unsigned lcespu1TCMFCon_Dump:              1;
  unsigned lcespu1TCMFCon_FadeOut:           1;		
  
  unsigned lcespu1TCMFConOld_IniFull:        1;
  unsigned lcespu1TCMFConOld_Rise:           1;
  unsigned lcespu1TCMFConOld_Hold:           1;
  unsigned lcespu1TCMFConOld_Dump:           1;	
  unsigned lcespu1TCMFConOld_FadeOut:        1;	
  
  unsigned lcespu1TCMFCon_Rise_MAX:          1;
  unsigned lcespu1TCMFCon_Rise_STEP:         1;
  unsigned lcespu1TCMFCon_Dump_SLOW:         1;
  unsigned lcespu1TCMFCon_Dump_FAST:         1;
  
  unsigned lcespu1TCMFConCBSCombFlag:        1;
  
  unsigned ESP_PULSE_DUMP:                   1; //__ESC_TCMF_CONTROL macro 밖에 선언시 #define	ESP_PULSE_DUMP_FL	FL_ESC.ESP_PULSE_DUMP	선언 가능
  unsigned lcespu1TcmfUnderToOverCompFlag:   1;
  unsigned lcespu1TCMFInhibitbyMpress:       1;
  unsigned lcespu1TCMFInhibitOld:            1;
  unsigned lcespu1TCMFInhibit:               1;
  unsigned lcespu1TCMFCon_Rise_STEP_Reg3:    1;
  unsigned lcespu1TCMFCon_Rise_STEP_Reg2:    1;
  unsigned lcespu1TCMFCon_Rise_STEP_Reg1:    1;
  unsigned lcespu1TCMFConSTEP_RegTrans:      1;
  unsigned lcespu1TCMFCon_Rise_STEPOld:      1;
    
  int16_t lcesps16TCMFControlCnt;
  int16_t lcesps16TCMFMaxRiseThr;
  int16_t slip_control_old[3];
  int16_t slip_control_dot;
  int16_t MFC_Current_Inter;

  int16_t lcesps16TCMFFastDumpThr;

  int16_t laesps16TCMFIniDutyInc;
  int16_t laesps16TCMFDutyMAXInc;
  int16_t lcesps16TCMFCompDutybyMP;
  
  int16_t lcesps16TCMFStepRiseThrReg3;
  int16_t lcesps16TCMFStepRiseThrReg2;
  int16_t lcesps16TCMFCon_STEP_Reg;
  int16_t lcesps16TCMFCon_STEP_RegOld;
 
  int16_t lcesps16TCMF_Rise_STEP_Com;
  int16_t lcesps16StepIntDutyPribyCom;
  int16_t lcesps16StepIntDutySecbyCom;
  int16_t lcesps16StepIntDutybyComVol;
#endif
};

#endif

#endif
