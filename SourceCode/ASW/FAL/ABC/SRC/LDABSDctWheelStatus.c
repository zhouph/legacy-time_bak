/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LDABSDctWheelStatus.C
* Description: Wheel Status Detection for ABS/TCS/ESP/VAF'S control
* Date: November. 21. 2005
* kim yong kil
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSDctWheelStatus
	#include "Mdyn_autosar.h"
	#include "FlexRayMain.h"
#endif
/* Includes ******************************************************************/

#include "LDABSDctWheelStatus.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LACallMain.h"
#include "Hardware_Control.h"
#include "LCABSDecideCtrlState.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCABSCallControl.h"
#include "LSESPCalSensorOffset.h"

/* swd11_kjs */
#include "LCESPCallControl.h"
/* swd11_kjs */


/* 2012_SWD_KJS */
//#include "../System/WContlActur.h"
/* 2012_SWD_KJS */

#if __HDC
#include "LCHDCCallControl.h"
#endif

/* Local Definition  ***********************************************/

#define     Full_Rise_Valve_Mode      0
#define     LFC_Rise_Valve_Mode       1
#define     Hold_Valve_Mode           2
#define     Dump_Valve_Mode           3
#define     Circulation_Valve_Mode    4

/* Macro Define */
#define     One_Scan_Cycle_Time_MS          10                            /*                                           */
#define     U16_Sampling_Time               One_Scan_Cycle_Time_MS       /*  10ms Cycle : 0.007 [sec]                 */
#define     U16_Number_Pi                   314                          /*  3.14    [Zero]                           */
#define     U16_RollBack_Time               200                          /*  2[sec]                                   */
#define     U16_BrakeOnCheckPress_1         0               /*  0   : 0[bar]                             */
#define     U16_BrakeOnCheckPress_2         100              /*  100 : 1[bar]                             */
#define     S16_WPE_MPRESS_0BAR             0               /*  0 bar                                    */
#define     S16_WPE_LPA_Pressure            0                            /*  0 bar                                    */

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
#define     S16CirculationComp              4

#define S16_BBS_Release_Gain_Press_f_1st	60
#define S16_BBS_Release_Gain_Press_f_2nd    145
#define S16_BBS_Release_Gain_Press_f_3rd    360
#define S16_BBS_Release_Gain_Press_f_4th    590
#define S16_BBS_Release_Gain_Press_f_5th    900
#define S16_BBS_Release_Gain_Press_f_6th    1200
#define S16_BBS_Release_Gain_Press_r_1st    60
#define S16_BBS_Release_Gain_Press_r_2nd    153
#define S16_BBS_Release_Gain_Press_r_3rd    310
#define S16_BBS_Release_Gain_Press_r_4th    555
#define S16_BBS_Release_Gain_Press_r_5th    935
#define S16_BBS_Release_Gain_Press_r_6th    1400

#define S16_BBS_Release_Gain_f_1st  	60
#define S16_BBS_Release_Gain_f_2nd      107
#define S16_BBS_Release_Gain_f_3rd      207
#define S16_BBS_Release_Gain_f_4th      216
#define S16_BBS_Release_Gain_f_5th      230
#define S16_BBS_Release_Gain_f_6th      240
#define S16_BBS_Release_Gain_r_1st      60
#define S16_BBS_Release_Gain_r_2nd      150
#define S16_BBS_Release_Gain_r_3rd      290
#define S16_BBS_Release_Gain_r_4th      337
#define S16_BBS_Release_Gain_r_5th      328
#define S16_BBS_Release_Gain_r_6th      310
            
#define S16_BBS_Rise_Press_f_1		50
#define S16_BBS_Rise_Press_f_2      210
#define S16_BBS_Rise_Press_f_3      435
#define S16_BBS_Rise_Press_f_4      660
#define S16_BBS_Rise_Press_f_5      840
#define S16_BBS_Rise_Press_f_6      1200
#define S16_BBS_Rise_Press_r_1      50
#define S16_BBS_Rise_Press_r_2      141
#define S16_BBS_Rise_Press_r_3      340
#define S16_BBS_Rise_Press_r_4      640
#define S16_BBS_Rise_Press_r_5      920
#define S16_BBS_Rise_Press_r_6      1200

#define S16_BBS_Rise_Gain_f_1   	 70
#define S16_BBS_Rise_Gain_f_2   	 142
#define S16_BBS_Rise_Gain_f_3        175
#define S16_BBS_Rise_Gain_f_4        203
#define S16_BBS_Rise_Gain_f_5        201
#define S16_BBS_Rise_Gain_f_6        190
#define S16_BBS_Rise_Gain_r_1        70
#define S16_BBS_Rise_Gain_r_2        130
#define S16_BBS_Rise_Gain_r_3        202
#define S16_BBS_Rise_Gain_r_4        230
#define S16_BBS_Rise_Gain_r_5        240
#define S16_BBS_Rise_Gain_r_6        220

#define S16_ABS_Rise_Press_f_1		30
#define S16_ABS_Rise_Press_f_2		100
#define S16_ABS_Rise_Press_f_3		300
#define S16_ABS_Rise_Press_f_4		600
#define S16_ABS_Rise_Press_f_5		800
#define S16_ABS_Rise_Press_f_6		1200
#define S16_ABS_Rise_Press_r_1		50
#define S16_ABS_Rise_Press_r_2		100
#define S16_ABS_Rise_Press_r_3		300
#define S16_ABS_Rise_Press_r_4		600
#define S16_ABS_Rise_Press_r_5		800
#define S16_ABS_Rise_Press_r_6		1200

#define S16_ABS_Rise_Gain_f_1		25
#define S16_ABS_Rise_Gain_f_2		35
#define S16_ABS_Rise_Gain_f_3		60
#define S16_ABS_Rise_Gain_f_4		75
#define S16_ABS_Rise_Gain_f_5		80
#define S16_ABS_Rise_Gain_f_6		75
#define S16_ABS_Rise_Gain_r_1		70
#define S16_ABS_Rise_Gain_r_2		125
#define S16_ABS_Rise_Gain_r_3		150
#define S16_ABS_Rise_Gain_r_4		180
#define S16_ABS_Rise_Gain_r_5		170
#define S16_ABS_Rise_Gain_r_6		160




#define S16_NO_Dump_Gain_Press_f_1st   	   100  
#define S16_NO_Dump_Gain_Press_f_2nd   	   160   
#define S16_NO_Dump_Gain_Press_f_3rd   	   340
#define S16_NO_Dump_Gain_Press_f_4th   	   550   
#define S16_NO_Dump_Gain_Press_f_5th   	   1027
#define S16_NO_Dump_Gain_Press_f_6th   	   1214   
                                       		     
#define S16_NO_Dump_Gain_Press_r_1st	 100	        
#define S16_NO_Dump_Gain_Press_r_2nd   	 123       
#define S16_NO_Dump_Gain_Press_r_3rd   	 340    
#define S16_NO_Dump_Gain_Press_r_4th   	 660       
#define S16_NO_Dump_Gain_Press_r_5th     945
#define S16_NO_Dump_Gain_Press_r_6th     1400   
                                         
#define S16_NO_Dump_Gain_f_1st     4        
#define S16_NO_Dump_Gain_f_2nd     16     
#define S16_NO_Dump_Gain_f_3rd     26       
#define S16_NO_Dump_Gain_f_4th     40     
#define S16_NO_Dump_Gain_f_5th     48     
#define S16_NO_Dump_Gain_f_6th     45     
                        
#define S16_NO_Dump_Gain_r_1st     4
#define S16_NO_Dump_Gain_r_2nd     12
#define S16_NO_Dump_Gain_r_3rd     34
#define S16_NO_Dump_Gain_r_4th     48
#define S16_NO_Dump_Gain_r_5th     52
#define S16_NO_Dump_Gain_r_6th     54
         
#define S16_Residual_Rise_Press_f_1         100 
#define S16_Residual_Rise_Press_f_2         210 
#define S16_Residual_Rise_Press_f_3         435 
#define S16_Residual_Rise_Press_f_4         660 
#define S16_Residual_Rise_Press_f_5         840 
#define S16_Residual_Rise_Press_f_6         1200
#define S16_Residual_Rise_Press_r_1         100 
#define S16_Residual_Rise_Press_r_2         141 
#define S16_Residual_Rise_Press_r_3         340 
#define S16_Residual_Rise_Press_r_4         640 
#define S16_Residual_Rise_Press_r_5         920 
#define S16_Residual_Rise_Press_r_6         1200
                                            
#define S16_Residual_Rise_Gain_f_1      	30                 			                
#define S16_Residual_Rise_Gain_f_2          132
#define S16_Residual_Rise_Gain_f_3          165
#define S16_Residual_Rise_Gain_f_4          193
#define S16_Residual_Rise_Gain_f_5          191
#define S16_Residual_Rise_Gain_f_6          190
#define S16_Residual_Rise_Gain_r_1          30 
#define S16_Residual_Rise_Gain_r_2          120
#define S16_Residual_Rise_Gain_r_3          192
#define S16_Residual_Rise_Gain_r_4          220
#define S16_Residual_Rise_Gain_r_5          230
#define S16_Residual_Rise_Gain_r_6          220

#define S16_Residual_Release_Press_f_1         100 
#define S16_Residual_Release_Press_f_2         145 
#define S16_Residual_Release_Press_f_3         360 
#define S16_Residual_Release_Press_f_4         590 
#define S16_Residual_Release_Press_f_5         900 
#define S16_Residual_Release_Press_f_6         1200
#define S16_Residual_Release_Press_r_1         100 
#define S16_Residual_Release_Press_r_2         153 
#define S16_Residual_Release_Press_r_3         310 
#define S16_Residual_Release_Press_r_4         555 
#define S16_Residual_Release_Press_r_5         935 
#define S16_Residual_Release_Press_r_6         1400
                                       
#define S16_Residual_Release_Gain_f_1      	   30                 			                
#define S16_Residual_Release_Gain_f_2          107
#define S16_Residual_Release_Gain_f_3          207
#define S16_Residual_Release_Gain_f_4          216
#define S16_Residual_Release_Gain_f_5          230
#define S16_Residual_Release_Gain_f_6          240
#define S16_Residual_Release_Gain_r_1          30 
#define S16_Residual_Release_Gain_r_2          150
#define S16_Residual_Release_Gain_r_3          290
#define S16_Residual_Release_Gain_r_4          337
#define S16_Residual_Release_Gain_r_5          328
#define S16_Residual_Release_Gain_r_6          310
                                            
                                            
#endif

#if __PRESS_EST_ABS
/*
#define     DumpSetLimitFrt     3
#define     DumpSetLimitRr      3
#define     DumpLowMuCount      5
#define     IndexHighMu         600
#define     IndexMidMu          400
#define     IndexBasalt         50
#define     IndexCeramic        30
#define     IndexLowMu          30
*/
#endif

/* Variables Definition*******************************************************/
U8_BIT_STRUCT_t WPE00;

/* LocalFunction prototype ***************************************************/

void LDABS_vDctWheelStatus(void);
void LDABS_vEstWheelPress(void);
void LDABS_vDisableEstWheelPress(void);
void LDABS_vCheckWheelValveMode(struct W_STRUCT *WL, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC);
uint16_t LDABS_u16FitSQRT(uint16_t tempW12);

/* 2012_SWD_KJS */
#if( (__WPE_VOLUME_MODEL==ENABLE) && (__VDC && __PRESS_EST_ACTIVE) )

void LDABS_vEstWheelPress_Volume(void);
void LDABS_vEstMotorRPM_Volume(void);
void LDABS_vEstWheelPressMode_Volume(void);
void LDABS_vEstWheelPressTWO_Volume(struct W_STRUCT *WL1, struct W_STRUCT *WL2);
void LDABS_vEstWheelPressONE_Volume(struct W_STRUCT *WL3);
void LDABS_vOutputWheelPress_Volume(void);
/* 2012_On_SWD_KJS */
void LDABS_vEstFirstCycleCtrl_Volume(struct W_STRUCT *WL4);
/* 2012_On_SWD_KJS */
/* 2012_SWD_KJS_TCMF */
void LDABS_vEstTCvvHoldPress_Volume(void);
/* 2012_SWD_KJS_TCMF */
void LDABS_vEstResolConv_Volume(void);

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
void LDABS_vCalRawCircuitPress_AHB(void);
void LDABS_vEstWheelPressMode_AHB(void);
void LDABS_vEstWheelPressONE_AHB(struct W_STRUCT *WL,struct W_STRUCT *WL2);
#endif

#endif
/* 2012_SWD_KJS */

#if __PRESS_EST_ABS
void LDABS_vEstWheelPressWLABS(struct W_STRUCT *WL);
void LDABS_vEstWheelPresSetParameterABS(struct W_STRUCT *WL);
void LDABS_vEstMuIndex(void);
void LDABS_vEstMuIndex_WL(struct W_STRUCT *WL_temp);
void LDABS_vSetMuIndex_WL(void);
uint8_t  K_dump_H, K_dump_M, K_dump_L;
#endif

#if __VDC && __PRESS_EST_ACTIVE
void LDABS_vEstWheelPresSetParameterActive(struct W_STRUCT *WL);
void LDABS_vEstWheelPressWLActive(struct W_STRUCT *WL);
void LDABS_vEstMotorRisePress(void);
void LDABS_vEstTCPressCircuit(void);
void LDABS_vEstTCPressCircuitMinMax(void);

void LDABS_vCheckPrimaryCircuit(void);
void LDABS_vCheckPrimaryTCValve(void);

void LDABS_vCheckSecondaryCircuit(void);
void LDABS_vCheckSecondaryTCValve(void);

void LDABS_vEstTCCircuitRiseComp(void);
void LDABS_vEstResolutionConv(void);
void LDABS_vEstTCHoldPress(void);
void LDABS_vOutputWheelPress(void);
#if (__FL_PRESS==ENABLE) || (__FR_PRESS==ENABLE) || (__RL_PRESS==ENABLE) || (__RR_PRESS==ENABLE)
void LDABS_vUseWheelPress(struct W_STRUCT *WL,struct PREM_PRESS_OFFSET_STRUCT  *PRESS);
#endif
void LDABS_vUseEstmPress(struct W_STRUCT *WL);

uint16_t LDABS_u16FitSQRT_res(uint16_t tempW12);

// <skeon OPTIME_MACFUNC_2> 110303
#ifndef OPTIME_MACFUNC_2

int16_t LDABS_s16s16MAX(int16_t esp_tempW1,int16_t esp_tempW2);
int16_t LDABS_s16s16MIN(int16_t esp_tempW1,int16_t esp_tempW2);

#endif
// </skeon OPTIME_MACFUNC_2>

int16_t lau8BitMaskCnt;
#endif

/* GlobalFunction prototype ***************************************************/
#if __ECU==ESP_ECU_1

#endif

int16_t Tc_under_pressure_p,Tc_under_pressure_s;
int16_t Tc_under_pressure_p_old,Tc_under_pressure_s_old;
int16_t Primary_Rise_Gain, Secondary_Rise_Gain;
int16_t Primary_Rise_Mode, Secondary_Rise_Mode;
int16_t Pressure_rise_gain_P,Pressure_rise_gain_S;
int16_t Primary_Rise_Mode_mon ,Secondary_Rise_Mode_mon ;
int16_t Primary_Hold_Rise_Gain, Secondary_Hold_Rise_Gain;
int16_t Motor_Rise_Gain;
int16_t mpress_x10;
int16_t LFC_RISE_RATE_ABS_x10;
int16_t pressure_rise_rate_rpm,pressure_rise_rate;

int16_t Primary_TC_hold_press,Secondary_TC_hold_press;
int16_t Primary_TC_hold_press_x10,Secondary_TC_hold_press_x10;
int16_t wpe_tempW0,wpe_tempW1,wpe_tempW2,wpe_tempW3,wpe_tempW4;
int16_t wpe_tempW5,wpe_tempW6,wpe_tempW7,wpe_tempW8,wpe_tempW9;

int16_t press_res;

int16_t diff_tc_and_wheel_press;

int16_t Primary_TC_valve_mode,Secondary_TC_valve_mode,TC_valve_mode;

int16_t Primary_TC_hold_press,Secondary_TC_hold_press,Max_TCMF_hold_press;

int16_t Motor_rise_press, Primary_Motor_rise_press, Secondary_Motor_rise_press;

int16_t Tc_under_press;
int16_t ESP_active_wl;

int16_t mpress_x10_LFC_Hold,LFC_Hold_press;

/* 2012_SWD_KJS */
int16_t  ldabss16MotorRPM_NEW;
int16_t  ldabss16BrakeFluidVolume_P;
int16_t  ldabss16BrakeFluidVolume_S;
uint16_t ldabsu16PistonArea;
uint16_t ldabsu16DeltaVolume;
int16_t  ldabss16MotorEfficiency_P;
int16_t  ldabss16MotorEfficiency_S;
int16_t  ldabss16CircuitPressure_P;
int16_t  ldabss16CircuitPressure_S;
/* 2012_SWD_KJS */

/* swd12_RPM_kjs */
uint16_t ldabsu16WPEMotorVolt;
uint16_t ldabsu16WPEMotorVolt_Old;
/* swd12_RPM_kjs */

/* 2012_SWD_KJS_TCMF */
int16_t ldabss16WPETCvvHoldPress_Pri;
int16_t ldabss16WPETCvvHoldPress_Scn;

int16_t ldabss16WPE_TC_Release_P_Pri;
int16_t ldabss16WPE_TC_Release_P_Scn;
/* 2012_SWD_KJS_TCMF */

/* 2012_SWD_KJS_CONV */
int16_t ldabss16WPEMpress_1_100bar;
/* 2012_SWD_KJS_CONV */

int16_t test_kjs_wpe_kuj_1;
int16_t test_kjs_wpe_kuj_2;
int16_t test_kjs_wpe_kuj_3;

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
int16_t ldabss16WPECircuitPress_Pri;
int16_t ldabss16WPECircuitPress_Scn;
int16_t ldabss16WPEPealSimulPress;
#endif

#if __ECU==ESP_ECU_1

#endif



  #if   __VDC && __PRESS_EST
    int16_t     LFC_RISE_RATE_ABS;
    int16_t     K_reapply;
    int16_t     K_dump;
  #endif

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LDABS_vDctWheelStatus
* CALLED BY:          LDABS_vCallDetection()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Wheel Status Detection for ABS control
******************************************************************************/
void LDABS_vDctWheelStatus(void)
{

  #if __VDC
    LDABS_vDisableEstWheelPress();
  #endif

    LDABS_vEstWheelPress();
    
  /* 2012_SWD_KJS */    
  #if( (__WPE_VOLUME_MODEL==ENABLE) && (__VDC && __PRESS_EST_ACTIVE) )
    LDABS_vEstWheelPress_Volume();  
  #endif
  /* 2012_SWD_KJS */    
    
  #if __PRESS_EST_ABS
    LDABS_vEstMuIndex();
  #endif

}


#if __VDC
void LDABS_vDisableEstWheelPress(void)
{
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if ((fu1FrontSolErrDet==1)||(fu1RearSolErrDet==1)||(fu1ESCSolErrDet==1)||(fu1ECUHwErrDet==1)		/* Valve	*/
		||(fu1MotErrFlg==1)																				/* Motor	*/
		||(fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(fu1McpSenPwr1sOk==0)									/* MCP		*/
		)
    #else
	if ((fu1FrontSolErrDet==1)||(fu1RearSolErrDet==1)||(fu1ESCSolErrDet==1)||(fu1ECUHwErrDet==1)		/* Valve	*/
		||(fu1MotErrFlg==1)																				/* Motor	*/
		||(fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(fu1McpSenPwr1sOk==0)									/* MCP		*/
		)
		/* fu1DelayedBrakeFluidLevel	*/
		/*	(ABS_PERMIT_BY_BRAKE==1)||	*/				
    #endif
    {
        EST_WHEEL_PRESS_OK_FLG=0;
/* 2012_SWD_KJS */        
        FL.s16_Estimated_Active_Press = 0;    /*배제조건*/
        FR.s16_Estimated_Active_Press = 0;    /*배제조건*/
        RL.s16_Estimated_Active_Press = 0;    /*배제조건*/
        RR.s16_Estimated_Active_Press = 0;    /*배제조건*/
/* 2012_SWD_KJS */
    }
    else
    {
        EST_WHEEL_PRESS_OK_FLG=1;
    }

}
#endif


/******************************************************************************
* FUNCTION NAME:      LDABS_vEstWheelPress
* CALLED BY:          LDABS_vDctWheelStatus()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Wheel Pressure Estimation for ABS control
******************************************************************************/
void LDABS_vEstWheelPress(void)
{

  #if __SPLIT_TYPE                       /*FR Split  */
    LDABS_vCheckWheelValveMode(&RL, &la_FLNO, &la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, &la_FRNC);
    LDABS_vCheckWheelValveMode(&FL, &la_RLNO, &la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, &la_RRNC);

/*
    LDABS_vCheckWheelValveMode(&RL, &la_FLNO, (uint8_t)la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, (uint8_t)la_FRNC);
    LDABS_vCheckWheelValveMode(&FL, &la_RLNO, (uint8_t)la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, (uint8_t)la_RRNC);
*/
  #else                                  /*X Split  */
    LDABS_vCheckWheelValveMode(&FL, &la_FLNO, &la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, &la_FRNC);
    LDABS_vCheckWheelValveMode(&RL, &la_RLNO, &la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, &la_RRNC);
/*
    LDABS_vCheckWheelValveMode(&FL, &la_FLNO, (uint8_t)la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, (uint8_t)la_FRNC);
    LDABS_vCheckWheelValveMode(&RL, &la_RLNO, (uint8_t)la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, (uint8_t)la_RRNC);
*/
  #endif

  #if __PRESS_EST_ABS
    LDABS_vEstWheelPresSetParameterABS(&FL);
    LDABS_vEstWheelPressWLABS(&FL);

    LDABS_vEstWheelPresSetParameterABS(&FR);
    LDABS_vEstWheelPressWLABS(&FR);

    LDABS_vEstWheelPresSetParameterABS(&RL);
    LDABS_vEstWheelPressWLABS(&RL);

    LDABS_vEstWheelPresSetParameterABS(&RR);
    LDABS_vEstWheelPressWLABS(&RR);
  #endif

  #if   __VDC && __PRESS_EST_ACTIVE

    press_res=10;

    LDABS_vEstTCHoldPress();

    LDABS_vEstResolutionConv();

    LDABS_vEstMotorRisePress();

    LDABS_vEstTCPressCircuit();

    LDABS_vEstWheelPresSetParameterActive(&FL);
    LDABS_vEstWheelPressWLActive(&FL);

    LDABS_vEstWheelPresSetParameterActive(&FR);
    LDABS_vEstWheelPressWLActive(&FR);

    LDABS_vEstWheelPresSetParameterActive(&RL);
    LDABS_vEstWheelPressWLActive(&RL);

    LDABS_vEstWheelPresSetParameterActive(&RR);
    LDABS_vEstWheelPressWLActive(&RR);

	/*
    LDABS_vEstTCCircuitRiseComp();
	*/
    LDABS_vOutputWheelPress();

  #endif

}

/* 2012_SWD_KJS */
#if( (__WPE_VOLUME_MODEL==ENABLE) && (__VDC && __PRESS_EST_ACTIVE) )

/******************************************************************************
* FUNCTION NAME:      LDABS_vEstWheelPress_NEW
* CALLED BY:          LDABS_vDctWheelStatus()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Wheel Pressure Estimation for ABS control
******************************************************************************/
void LDABS_vEstWheelPress_Volume(void)
{

    press_res=10;

    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    LDABS_vCalRawCircuitPress_AHB();
    #endif
    
   #if __SPLIT_TYPE                       
   /*  FR Split  */
    LDABS_vCheckWheelValveMode(&RL, &la_FLNO, &la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, &la_FRNC);
    LDABS_vCheckWheelValveMode(&FL, &la_RLNO, &la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, &la_RRNC);
  #else                                  
  /*  X Split  */
    LDABS_vCheckWheelValveMode(&FL, &la_FLNO, &la_FLNC);
    LDABS_vCheckWheelValveMode(&FR, &la_FRNO, &la_FRNC);
    LDABS_vCheckWheelValveMode(&RL, &la_RLNO, &la_RLNC);
    LDABS_vCheckWheelValveMode(&RR, &la_RRNO, &la_RRNC);
  #endif

  #if __PRESS_EST_ABS
    LDABS_vEstWheelPresSetParameterABS(&FL);
    LDABS_vEstWheelPressWLABS(&FL);

    LDABS_vEstWheelPresSetParameterABS(&FR);
    LDABS_vEstWheelPressWLABS(&FR);

    LDABS_vEstWheelPresSetParameterABS(&RL);
    LDABS_vEstWheelPressWLABS(&RL);

    LDABS_vEstWheelPresSetParameterABS(&RR);
    LDABS_vEstWheelPressWLABS(&RR);
  #endif
 
  #if   __VDC && __PRESS_EST_ACTIVE

    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    
    LDABS_vEstWheelPressMode_AHB();    

    /* F318 KYB 실차검증 임시조치 TODO */
    FL.ldabss16EstWheelPress_VM = lis16EstWheelPressFL;
    FR.ldabss16EstWheelPress_VM = lis16EstWheelPressFR;
    RL.ldabss16EstWheelPress_VM = lis16EstWheelPressRL;
    RR.ldabss16EstWheelPress_VM = lis16EstWheelPressRR;

    #else

    LDABS_vEstResolConv_Volume();

    LDABS_vEstTCvvHoldPress_Volume();
    
    LDABS_vEstMotorRPM_Volume();
    
    LDABS_vEstWheelPressMode_Volume();

    #endif
    
    LDABS_vOutputWheelPress_Volume();
    
    /* 2012_On_SWD_KJS */
    LDABS_vEstFirstCycleCtrl_Volume(&FL);        
    LDABS_vEstFirstCycleCtrl_Volume(&FR);
    LDABS_vEstFirstCycleCtrl_Volume(&RL);
    LDABS_vEstFirstCycleCtrl_Volume(&RR);        
    /* 2012_On_SWD_KJS */

  #endif
    
}

void LDABS_vEstMotorRPM_Volume(void)
{

    uint16_t U16WPETemp0;        
    int16_t  S16WPETemp1; 
    int32_t S32WPETemp2; 
    int32_t S32WPETemp3; 
    int32_t S32WPETemp4; 
    int32_t S32WPETemp5;

    /* swd12_RPM_kjs */   
    ldabss16MotorEfficiency_P = LCESP_s16IInter2Point(ldabss16CircuitPressure_P, (MPRESS_0BAR*10), (int16_t)U8_Pump_Efficiency_Low_P, (MPRESS_150BAR*10), (int16_t)U8_Pump_Efficiency_High_P); /* 85% */    
    ldabss16MotorEfficiency_S = LCESP_s16IInter2Point(ldabss16CircuitPressure_S, (MPRESS_0BAR*10), (int16_t)U8_Pump_Efficiency_Low_P, (MPRESS_150BAR*10), (int16_t)U8_Pump_Efficiency_High_P); /* 85% */        
    /* swd12_RPM_kjs */   
         
    U16WPETemp0 = ((uint16_t)U8_Piston_Diameter*(uint16_t)U8_Piston_Diameter)/4;                                                      /*  (1/10000)*(1/10000) = 10^(-8)  < 86*86/4=1849 >                                       */             
    ldabsu16PistonArea = (uint16_t)(((uint32_t)U16WPETemp0*U16_Number_Pi)/100);                                     /*  (1/10000)*(1/10000) = 10^(-8)  < 1849*314/100=5805 _580586/100=5805 >                 */
    
    ldabsu16DeltaVolume = (uint16_t)(((uint32_t)ldabsu16PistonArea*U8_Motor_Eccentricity)/50);                     /*  *2/100=/50  10^(-8)*10^(-5)*10^(+2) = 10^(-11)  < 5805*92/50=10681 _5805*92=534060 >  */
        
    S16WPETemp1 = (int16_t)((int16_t)U8_Motor_Eccentricity*((ldabss16CircuitPressure_P+ldabss16CircuitPressure_S)/100));    /*  (1/00000)*(100000)=1       < 92*(pressure) _92*50=4600 >                              */
    S32WPETemp2 = (int32_t)(((int32_t)S16WPETemp1*ldabsu16PistonArea)/1000);                                       /*  1*10^(-8)*10^(+3)=10^(-5)  < 4600*5805/1000=26766 _4600*5805=26766855 >               */
    S32WPETemp3 = (S32WPETemp2*U8_Motor_Resistance)/U8_Motor_K_Torque_Current;                                      /*  10^(-5) [Volt]             < 26766*35/20=46840 => 0.46840[Volt] _26766*35=936810 >    */                   

    /* swd12_RPM_kjs */
    /* fu16CalVoltIntMOTOR or fu16CalVoltMOTOR */
    ldabsu16WPEMotorVolt_Old = ldabsu16WPEMotorVolt;
    ldabsu16WPEMotorVolt = LCESP_u16Lpf1Int(fu16CalVoltIntMOTOR, ldabsu16WPEMotorVolt_Old, L_U8FILTER_GAIN_10MSLOOP_10HZ);    
    S32WPETemp4 = (int32_t)ldabsu16WPEMotorVolt*100;    
    /* swd12_RPM_kjs */
                                                                                                                    /*  10^(-5) [Volt]             < 408*100000/51=800000 => 8.00000[Volt] >                  */
    if(S32WPETemp4 > 0)
    {    
        ldabss16MotorRPM_NEW = (int16_t)((S32WPETemp4 - S32WPETemp3)/((int16_t)U8_Motor_Volt_RPM*10));                          /*  10^(-5)/10^(-4)*10^(-1)=1 [RPM]                                                       */
    }
    else
    {
        ldabss16MotorRPM_NEW = 0;
    }

    /* 12SWD_KJS */
    if(ldabss16MotorRPM_NEW < 0)
    {
        ldabss16MotorRPM_NEW = 0;     
    }
    else
    {
        ;
    }
    /* 12SWD_KJS */
        
    S32WPETemp5 = ((int32_t)ldabsu16DeltaVolume*ldabss16MotorRPM_NEW)/60;                                                        /* 10^(-11)*(1) => 10^(-11)  < 10681*4000/60=712066 >                                     */

    ldabss16BrakeFluidVolume_P = (int16_t)(((int32_t)ldabss16MotorEfficiency_P*S32WPETemp5)/10000);                              /* 10^(-11)*10^(-2)*10^(+2)*10^(+6)*10^(+2) => 1/1000[cc]  < 80*712066/10000 = 5696 >     */
    ldabss16BrakeFluidVolume_S = (int16_t)(((int32_t)ldabss16MotorEfficiency_S*S32WPETemp5)/10000);                              /* 10^(-11)*10^(-2)*10^(+2)*10^(+6)*10^(+2) => 1/1000[cc]  < 80*712066/10000 = 5696 >     */

}

void LDABS_vEstWheelPressMode_Volume(void)
{
    
    FL.ldabss16EstWheelVolumeOld_VM = FL.ldabss16EstWheelVolume_VM;
    FR.ldabss16EstWheelVolumeOld_VM = FR.ldabss16EstWheelVolume_VM;
    RL.ldabss16EstWheelVolumeOld_VM = RL.ldabss16EstWheelVolume_VM;
    RR.ldabss16EstWheelVolumeOld_VM = RR.ldabss16EstWheelVolume_VM;        
    
    FL.ldabss16EstWheelPressOld_VM = FL.ldabss16EstWheelPress_VM;
    FR.ldabss16EstWheelPressOld_VM = FR.ldabss16EstWheelPress_VM;
    RL.ldabss16EstWheelPressOld_VM = RL.ldabss16EstWheelPress_VM;
    RR.ldabss16EstWheelPressOld_VM = RR.ldabss16EstWheelPress_VM;                

    /* Straight Control Compensation (Alat_TCS_HDC) */
    if((BTCS_ON==1)
        #if __HDC
        ||(lcu1HdcActiveFlg==1)
        #endif
       )
    {
        ldabsu1WPEStraightControl = 1;
    }
    else
    {
        ldabsu1WPEStraightControl = 0;
    }    
    /* Straight Control Compensation (Alat_TCS_HDC) */
    
    /* Primary Circuit */
    /* Same Circuit Double V/V Full Rise => This Mode includes NO Dump Mode */
    if( ((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))&&(FR.Wheel_Valve_Mode==0)&&(RL.Wheel_Valve_Mode==0) )
    {
        LDABS_vEstWheelPressTWO_Volume(&FR, &RL);     
    }
    /* Same Circuit Double V/V LFC Rise on TC_VV_Close => This Mode includes NO_Dump Mode */
//    else if( ((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))&&(FR.Wheel_Valve_Mode==1)&&(RL.Wheel_Valve_Mode==1) )
//    {
//        LDABS_vEstWheelPressTWO_Volume(&FR, &RL);     
//    }    
    /* Same Circuit Single V/V Rise */    
    else
    {
        LDABS_vEstWheelPressONE_Volume(&FR);        
        LDABS_vEstWheelPressONE_Volume(&RL);
    }
    
    ldabss16CircuitPressure_P = LDABS_s16s16MAX(FR.ldabss16EstWheelPress_VM, RL.ldabss16EstWheelPress_VM);

    /* Secondary Circuit */
    /* Same Circuit Double V/V Rise */    
    if( ((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))&&(FL.Wheel_Valve_Mode==0)&&(RR.Wheel_Valve_Mode==0) )
    {
        LDABS_vEstWheelPressTWO_Volume(&FL, &RR);        
    }
    /* Same Circuit Double V/V LFC Rise on TC_VV_Close => This Mode includes NO_Dump Mode */
//    else if( ((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))&&(FL.Wheel_Valve_Mode==1)&&(RR.Wheel_Valve_Mode==1) )
//    {
//        LDABS_vEstWheelPressTWO_Volume(&FL, &RR);        
//    }
    /* Same Circuit Single V/V Rise */    
    else
    {
        LDABS_vEstWheelPressONE_Volume(&FL);
        LDABS_vEstWheelPressONE_Volume(&RR);                
    }
    
    ldabss16CircuitPressure_S = LDABS_s16s16MAX(FL.ldabss16EstWheelPress_VM, RR.ldabss16EstWheelPress_VM);
    
}

void LDABS_vEstWheelPressTWO_Volume(struct W_STRUCT *WL1, struct W_STRUCT *WL2)
{

    int16_t S16WPETemp0;
    
    int16_t S16WPETemp1; 
    int16_t S16WPETemp2; 
    int16_t S16WPETemp3;
    int16_t S16WPETemp4;
    int16_t S16WPETemp5;

    int16_t S16WPETemp6;
    int16_t S16WPETemp7;
    int16_t S16WPETemp8;
    
    int16_t S16WPEBrakeFluidVolumeCircuit;    
    
    /* 2012_SWD_KJS_TCMF */    
    int16_t S16WPETTCvvReleasePress;    
    /* 2012_SWD_KJS_TCMF */    

    /* C618_MGH80_BKT_JJJ */
    int16_t S16_Caliper_Model_Volume_No_T;
    
    int16_t S16_Caliper_Model_Volume_T_1;
    int16_t S16_Caliper_Model_Volume_T_2;
    int16_t S16_Caliper_Model_Volume_T_3;
    int16_t S16_Caliper_Model_Volume_T_4;
    int16_t S16_Caliper_Model_Volume_T_5;
    int16_t S16_Caliper_Model_Volume_T_6; 
    int16_t S16_Caliper_Model_Volume_T_7;
    int16_t S16_Caliper_Model_Volume_T_8;
    int16_t S16_Caliper_Model_Volume_T_9;
    int16_t S16_Caliper_Model_Volume_T_10;
    int16_t S16_Caliper_Model_Volume_T_11;
    int16_t S16_Caliper_Model_Volume_T_12;
    int16_t S16_Caliper_Model_Volume_T_13;
    int16_t S16_Caliper_Model_Volume_T_14;
    int16_t S16_Caliper_Model_Volume_T_15;
    int16_t S16_Caliper_Model_Volume_T_16;

    int16_t S16_Caliper_Model_Volume_T_N_1;
    int16_t S16_Caliper_Model_Volume_T_N_2;
    int16_t S16_Caliper_Model_Volume_T_N_3;
    int16_t S16_Caliper_Model_Volume_T_N_4;
    int16_t S16_Caliper_Model_Volume_T_N_5;
    int16_t S16_Caliper_Model_Volume_T_N_6; 
    int16_t S16_Caliper_Model_Volume_T_N_7;
    int16_t S16_Caliper_Model_Volume_T_N_8;
    int16_t S16_Caliper_Model_Volume_T_N_9;
    int16_t S16_Caliper_Model_Volume_T_N_10;
    int16_t S16_Caliper_Model_Volume_T_N_11;
    int16_t S16_Caliper_Model_Volume_T_N_12;
    int16_t S16_Caliper_Model_Volume_T_N_13;
    int16_t S16_Caliper_Model_Volume_T_N_14;
    int16_t S16_Caliper_Model_Volume_T_N_15;
    int16_t S16_Caliper_Model_Volume_T_N_16;

    int16_t S16_Caliper_Model_Volume_f_N_1;
    int16_t S16_Caliper_Model_Volume_f_N_2;
    int16_t S16_Caliper_Model_Volume_f_N_3;
    int16_t S16_Caliper_Model_Volume_f_N_4;
    int16_t S16_Caliper_Model_Volume_f_N_5;
    int16_t S16_Caliper_Model_Volume_f_N_6; 
    int16_t S16_Caliper_Model_Volume_f_N_7;
    int16_t S16_Caliper_Model_Volume_f_N_8;
    int16_t S16_Caliper_Model_Volume_f_N_9;
    int16_t S16_Caliper_Model_Volume_f_N_10;
    int16_t S16_Caliper_Model_Volume_f_N_11;
    int16_t S16_Caliper_Model_Volume_f_N_12;
    int16_t S16_Caliper_Model_Volume_f_N_13;
    int16_t S16_Caliper_Model_Volume_f_N_14;
    int16_t S16_Caliper_Model_Volume_f_N_15;
    int16_t S16_Caliper_Model_Volume_f_N_16;

    int16_t S16_Caliper_Model_Volume_r_N_1;
    int16_t S16_Caliper_Model_Volume_r_N_2;
    int16_t S16_Caliper_Model_Volume_r_N_3;
    int16_t S16_Caliper_Model_Volume_r_N_4;
    int16_t S16_Caliper_Model_Volume_r_N_5;
    int16_t S16_Caliper_Model_Volume_r_N_6; 
    int16_t S16_Caliper_Model_Volume_r_N_7;
    int16_t S16_Caliper_Model_Volume_r_N_8;
    int16_t S16_Caliper_Model_Volume_r_N_9;
    int16_t S16_Caliper_Model_Volume_r_N_10;
    int16_t S16_Caliper_Model_Volume_r_N_11;
    int16_t S16_Caliper_Model_Volume_r_N_12;
    int16_t S16_Caliper_Model_Volume_r_N_13;
    int16_t S16_Caliper_Model_Volume_r_N_14;
    int16_t S16_Caliper_Model_Volume_r_N_15;
    int16_t S16_Caliper_Model_Volume_r_N_16;
    
    S16_Caliper_Model_Volume_f_N_1  = (int16_t)(S16_Caliper_Model_Volume_f_1  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_2  = (int16_t)(S16_Caliper_Model_Volume_f_2  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_3  = (int16_t)(S16_Caliper_Model_Volume_f_3  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_4  = (int16_t)(S16_Caliper_Model_Volume_f_4  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_5  = (int16_t)(S16_Caliper_Model_Volume_f_5  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_6  = (int16_t)(S16_Caliper_Model_Volume_f_6  + S16_Caliper_Model_Volume_No_f); 
    S16_Caliper_Model_Volume_f_N_7  = (int16_t)(S16_Caliper_Model_Volume_f_7  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_8  = (int16_t)(S16_Caliper_Model_Volume_f_8  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_9  = (int16_t)(S16_Caliper_Model_Volume_f_9  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_10 = (int16_t)(S16_Caliper_Model_Volume_f_10 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_11 = (int16_t)(S16_Caliper_Model_Volume_f_11 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_12 = (int16_t)(S16_Caliper_Model_Volume_f_12 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_13 = (int16_t)(S16_Caliper_Model_Volume_f_13 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_14 = (int16_t)(S16_Caliper_Model_Volume_f_14 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_15 = (int16_t)(S16_Caliper_Model_Volume_f_15 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_16 = (int16_t)(S16_Caliper_Model_Volume_f_16 + S16_Caliper_Model_Volume_No_f);

    S16_Caliper_Model_Volume_r_N_1  = (int16_t)(S16_Caliper_Model_Volume_r_1  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_2  = (int16_t)(S16_Caliper_Model_Volume_r_2  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_3  = (int16_t)(S16_Caliper_Model_Volume_r_3  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_4  = (int16_t)(S16_Caliper_Model_Volume_r_4  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_5  = (int16_t)(S16_Caliper_Model_Volume_r_5  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_6  = (int16_t)(S16_Caliper_Model_Volume_r_6  + S16_Caliper_Model_Volume_No_r); 
    S16_Caliper_Model_Volume_r_N_7  = (int16_t)(S16_Caliper_Model_Volume_r_7  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_8  = (int16_t)(S16_Caliper_Model_Volume_r_8  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_9  = (int16_t)(S16_Caliper_Model_Volume_r_9  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_10 = (int16_t)(S16_Caliper_Model_Volume_r_10 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_11 = (int16_t)(S16_Caliper_Model_Volume_r_11 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_12 = (int16_t)(S16_Caliper_Model_Volume_r_12 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_13 = (int16_t)(S16_Caliper_Model_Volume_r_13 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_14 = (int16_t)(S16_Caliper_Model_Volume_r_14 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_15 = (int16_t)(S16_Caliper_Model_Volume_r_15 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_16 = (int16_t)(S16_Caliper_Model_Volume_r_16 + S16_Caliper_Model_Volume_No_r);    

    S16_Caliper_Model_Volume_No_T = (int16_t)(S16_Caliper_Model_Volume_No_f + S16_Caliper_Model_Volume_No_r);

    S16_Caliper_Model_Volume_T_1  = (int16_t)(S16_Caliper_Model_Volume_f_1  + S16_Caliper_Model_Volume_r_1);
    S16_Caliper_Model_Volume_T_2  = (int16_t)(S16_Caliper_Model_Volume_f_2  + S16_Caliper_Model_Volume_r_2);
    S16_Caliper_Model_Volume_T_3  = (int16_t)(S16_Caliper_Model_Volume_f_3  + S16_Caliper_Model_Volume_r_3);
    S16_Caliper_Model_Volume_T_4  = (int16_t)(S16_Caliper_Model_Volume_f_4  + S16_Caliper_Model_Volume_r_4);
    S16_Caliper_Model_Volume_T_5  = (int16_t)(S16_Caliper_Model_Volume_f_5  + S16_Caliper_Model_Volume_r_5);
    S16_Caliper_Model_Volume_T_6  = (int16_t)(S16_Caliper_Model_Volume_f_6  + S16_Caliper_Model_Volume_r_6);
    S16_Caliper_Model_Volume_T_7  = (int16_t)(S16_Caliper_Model_Volume_f_7  + S16_Caliper_Model_Volume_r_7);
    S16_Caliper_Model_Volume_T_8  = (int16_t)(S16_Caliper_Model_Volume_f_8  + S16_Caliper_Model_Volume_r_8);
    S16_Caliper_Model_Volume_T_9  = (int16_t)(S16_Caliper_Model_Volume_f_9  + S16_Caliper_Model_Volume_r_9);
    S16_Caliper_Model_Volume_T_10 = (int16_t)(S16_Caliper_Model_Volume_f_10 + S16_Caliper_Model_Volume_r_10);
    S16_Caliper_Model_Volume_T_11 = (int16_t)(S16_Caliper_Model_Volume_f_11 + S16_Caliper_Model_Volume_r_11);
    S16_Caliper_Model_Volume_T_12 = (int16_t)(S16_Caliper_Model_Volume_f_12 + S16_Caliper_Model_Volume_r_12);
    S16_Caliper_Model_Volume_T_13 = (int16_t)(S16_Caliper_Model_Volume_f_13 + S16_Caliper_Model_Volume_r_13);
    S16_Caliper_Model_Volume_T_14 = (int16_t)(S16_Caliper_Model_Volume_f_14 + S16_Caliper_Model_Volume_r_14);
    S16_Caliper_Model_Volume_T_15 = (int16_t)(S16_Caliper_Model_Volume_f_15 + S16_Caliper_Model_Volume_r_15);
    S16_Caliper_Model_Volume_T_16 = (int16_t)(S16_Caliper_Model_Volume_f_16 + S16_Caliper_Model_Volume_r_16);

    S16_Caliper_Model_Volume_T_N_1  = (int16_t)(S16_Caliper_Model_Volume_T_1  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_2  = (int16_t)(S16_Caliper_Model_Volume_T_2  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_3  = (int16_t)(S16_Caliper_Model_Volume_T_3  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_4  = (int16_t)(S16_Caliper_Model_Volume_T_4  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_5  = (int16_t)(S16_Caliper_Model_Volume_T_5  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_6  = (int16_t)(S16_Caliper_Model_Volume_T_6  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_7  = (int16_t)(S16_Caliper_Model_Volume_T_7  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_8  = (int16_t)(S16_Caliper_Model_Volume_T_8  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_9  = (int16_t)(S16_Caliper_Model_Volume_T_9  + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_10 = (int16_t)(S16_Caliper_Model_Volume_T_10 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_11 = (int16_t)(S16_Caliper_Model_Volume_T_11 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_12 = (int16_t)(S16_Caliper_Model_Volume_T_12 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_13 = (int16_t)(S16_Caliper_Model_Volume_T_13 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_14 = (int16_t)(S16_Caliper_Model_Volume_T_14 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_15 = (int16_t)(S16_Caliper_Model_Volume_T_15 + S16_Caliper_Model_Volume_No_T);
    S16_Caliper_Model_Volume_T_N_16 = (int16_t)(S16_Caliper_Model_Volume_T_16 + S16_Caliper_Model_Volume_No_T);
    /* C618_MGH80_BKT_JJJ */
    
    if(((WL1->LEFT_WHEEL==0)&&(WL1->REAR_WHEEL==0))||((WL2->LEFT_WHEEL==1)&&(WL2->REAR_WHEEL==1))) /* Primary : FR or RL */
	{
	    S16WPEBrakeFluidVolumeCircuit = ldabss16BrakeFluidVolume_P;       
        S16WPETTCvvReleasePress = ldabss16WPE_TC_Release_P_Pri;        
	}
	else if(((WL1->LEFT_WHEEL==1)&&(WL1->REAR_WHEEL==0))||((WL2->LEFT_WHEEL==0)&&(WL2->REAR_WHEEL==1))) /* Secondary : FL or RR */
	{
	    S16WPEBrakeFluidVolumeCircuit = ldabss16BrakeFluidVolume_S;	    
        S16WPETTCvvReleasePress = ldabss16WPE_TC_Release_P_Scn;        
	}
	else
	{
	    S16WPEBrakeFluidVolumeCircuit = 0;	    
        S16WPETTCvvReleasePress = 0;	    
	}    

    S16WPETemp1 = (int16_t)(((int32_t)S16WPEBrakeFluidVolumeCircuit*U16_Sampling_Time)/1000);

    /* Straight Control Compensation (Alat_TCS_HDC) */
    if(ldabsu1WPEStraightControl==1)
    {
        S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*U8_WPE_Straight_Cont_Comp_T)/100);
    }
    else
    {
        ;
    }    
    /* Straight Control Compensation (Alat_TCS_HDC) */

    /* ESV V/V Closed Condition */
    if((((WL1->LEFT_WHEEL==0)&&(WL1->REAR_WHEEL==0))||((WL2->LEFT_WHEEL==1)&&(WL2->REAR_WHEEL==1)))&&(S_VALVE_RIGHT==0)) /* Primary : FR or RL */
	{
        S16WPETemp1 = 0;       
	}
	else if((((WL1->LEFT_WHEEL==1)&&(WL1->REAR_WHEEL==0))||((WL2->LEFT_WHEEL==0)&&(WL2->REAR_WHEEL==1)))&&(S_VALVE_LEFT==0)) /* Secondary : FL or RR */
	{
        S16WPETemp1 = 0;       
	}
	else
	{
	    ;	    
	}
    /* ESV V/V Closed Condition */
            
    S16WPETemp2 = (WL1->ldabss16EstWheelVolume_VM + WL2->ldabss16EstWheelVolume_VM) + S16WPETemp1;

    /* 2012_ON_SWD_KJS */
    /* 1st Brake */
    if(WL1->WPE_First_Control_VM==1)                    
    {
        if(S16WPETemp2 <= S16_Caliper_Model_Volume_T_N_6)
        {    
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_N_1),(int16_t)(S16_Caliper_Model_Press_1),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_2),(int16_t)(S16_Caliper_Model_Press_2),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_3),(int16_t)(S16_Caliper_Model_Press_3),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_4),(int16_t)(S16_Caliper_Model_Press_4),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_5),(int16_t)(S16_Caliper_Model_Press_5),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_6),(int16_t)(S16_Caliper_Model_Press_6));
        }
        else if(S16WPETemp2 <= S16_Caliper_Model_Volume_T_N_11)
        {
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_N_6), (int16_t)(S16_Caliper_Model_Press_6),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_7), (int16_t)(S16_Caliper_Model_Press_7),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_8), (int16_t)(S16_Caliper_Model_Press_8),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_9), (int16_t)(S16_Caliper_Model_Press_9),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_10),(int16_t)(S16_Caliper_Model_Press_10),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_11),(int16_t)(S16_Caliper_Model_Press_11));        
        }
        else
        {
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_N_11), (int16_t)(S16_Caliper_Model_Press_11),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_12), (int16_t)(S16_Caliper_Model_Press_12),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_13), (int16_t)(S16_Caliper_Model_Press_13),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_14), (int16_t)(S16_Caliper_Model_Press_14),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_15), (int16_t)(S16_Caliper_Model_Press_15),
                        (int16_t)(S16_Caliper_Model_Volume_T_N_16), (int16_t)(S16_Caliper_Model_Press_16));        
        }
    }
    /* Nth Brake */
    else
    {
        if(S16WPETemp2 <= S16_Caliper_Model_Volume_T_6)
        {    
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_1),(int16_t)(S16_Caliper_Model_Press_1),
                        (int16_t)(S16_Caliper_Model_Volume_T_2),(int16_t)(S16_Caliper_Model_Press_2),
                        (int16_t)(S16_Caliper_Model_Volume_T_3),(int16_t)(S16_Caliper_Model_Press_3),
                        (int16_t)(S16_Caliper_Model_Volume_T_4),(int16_t)(S16_Caliper_Model_Press_4),
                        (int16_t)(S16_Caliper_Model_Volume_T_5),(int16_t)(S16_Caliper_Model_Press_5),
                        (int16_t)(S16_Caliper_Model_Volume_T_6),(int16_t)(S16_Caliper_Model_Press_6));
        }
        else if(S16WPETemp2 <= S16_Caliper_Model_Volume_T_11)
        {
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_6), (int16_t)(S16_Caliper_Model_Press_6),
                        (int16_t)(S16_Caliper_Model_Volume_T_7), (int16_t)(S16_Caliper_Model_Press_7),
                        (int16_t)(S16_Caliper_Model_Volume_T_8), (int16_t)(S16_Caliper_Model_Press_8),
                        (int16_t)(S16_Caliper_Model_Volume_T_9), (int16_t)(S16_Caliper_Model_Press_9),
                        (int16_t)(S16_Caliper_Model_Volume_T_10),(int16_t)(S16_Caliper_Model_Press_10),
                        (int16_t)(S16_Caliper_Model_Volume_T_11),(int16_t)(S16_Caliper_Model_Press_11));        
        }
        else
        {
            S16WPETemp3 = LCESP_s16IInter6Point(S16WPETemp2,
                        (int16_t)(S16_Caliper_Model_Volume_T_11), (int16_t)(S16_Caliper_Model_Press_11),
                        (int16_t)(S16_Caliper_Model_Volume_T_12), (int16_t)(S16_Caliper_Model_Press_12),
                        (int16_t)(S16_Caliper_Model_Volume_T_13), (int16_t)(S16_Caliper_Model_Press_13),
                        (int16_t)(S16_Caliper_Model_Volume_T_14), (int16_t)(S16_Caliper_Model_Press_14),
                        (int16_t)(S16_Caliper_Model_Volume_T_15), (int16_t)(S16_Caliper_Model_Press_15),
                        (int16_t)(S16_Caliper_Model_Volume_T_16), (int16_t)(S16_Caliper_Model_Press_16));        
        }        
    }                
    /* 2012_ON_SWD_KJS */
                
    WL1->ldabss16EstWheelPress_VM = S16WPETemp3;
    WL2->ldabss16EstWheelPress_VM = S16WPETemp3;                   

    /* Master Pressure Effect */
    if(WL1->ldabss16EstWheelPress_VM < ldabss16WPEMpress_1_100bar)
    {
                    
        S16WPETemp6 = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM/10,
                                     (int16_t)(S16_MP_Rise_Press_f_1_volume),(int16_t)(S16_MP_Rise_Gain_f_1_volume),
                                     (int16_t)(S16_MP_Rise_Press_f_2_volume),(int16_t)(S16_MP_Rise_Gain_f_2_volume),
                                     (int16_t)(S16_MP_Rise_Press_f_3_volume),(int16_t)(S16_MP_Rise_Gain_f_3_volume),
                                     (int16_t)(S16_MP_Rise_Press_f_4_volume),(int16_t)(S16_MP_Rise_Gain_f_4_volume),
                                     (int16_t)(S16_MP_Rise_Press_f_5_volume),(int16_t)(S16_MP_Rise_Gain_f_5_volume),
                                     (int16_t)(S16_MP_Rise_Press_f_6_volume),(int16_t)(S16_MP_Rise_Gain_f_6_volume));
                              
        S16WPETemp7 = ldabss16WPEMpress_1_100bar - WL1->ldabss16EstWheelPress_VM; 
                             
        S16WPETemp8 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp7))))*(S16WPETemp6*100))/3162);
                   
        WL1->ldabss16EstWheelPress_VM = WL1->ldabss16EstWheelPress_VM + S16WPETemp8;                       

        WL2->ldabss16EstWheelPress_VM = WL1->ldabss16EstWheelPress_VM;
        
        WL1->ldabss16EstWheelPressMON_VM = 101;    
        WL2->ldabss16EstWheelPressMON_VM = 101;
                        
    }
    else
    {
        WL1->ldabss16EstWheelPressMON_VM = 102;    
        WL2->ldabss16EstWheelPressMON_VM = 102;
    }                
    /* Master Pressure Effect */

    /* 2012_SWD_KJS_TCMF */
    if(WL1->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress)
    {

        if(WL1->REAR_WHEEL==0)
		 {                   
            S16WPETemp4 = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM/10,
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
                                            (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
        }
        else
        {
            S16WPETemp4 = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM/10,
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
                                            (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
        }                                       
        
        S16WPETemp5 = WL1->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
        
        S16WPETemp0 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp5))))*(S16WPETemp4*100))/3162);
        
        WL1->ldabss16EstWheelPress_VM = WL1->ldabss16EstWheelPress_VM - S16WPETemp0;
        WL2->ldabss16EstWheelPress_VM = WL1->ldabss16EstWheelPress_VM;

        /* Calculate the Volume using Pressure */         
        /* Front Wheel */
        /* 1st Brake */
        if(WL1->WPE_First_Control_VM==1)                    
        {                
            
		    if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
            }
            else if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
            }
            else
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
            }    

        }
        /* Nth Brake */                
        else
        {

		    if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
            }
            else if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
            }
            else
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
                   
            }
        }

        /* Rear Wheel */
        /* 1st Brake */
        if(WL2->WPE_First_Control_VM==1)                    
        {                
            
		    if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
            }
            else if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
            }
            else
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
            }    

        }
        /* Nth Brake */                
        else
        {

		    if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
            }
            else if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
            }
            else
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
                   
            }
        }
        /* Calculate the Volume using Pressure */        

        WL1->ldabss16EstWheelPressMON_VM = WL1->ldabss16EstWheelPressMON_VM + 10; /* 111 or 112 */ 
        WL2->ldabss16EstWheelPressMON_VM = WL2->ldabss16EstWheelPressMON_VM + 10; /* 111 or 112 */
 
    }
    /* Motor_Rise or MP Rise */     
    /* (WL1->ldabss16EstWheelPress_VM <= S16WPETTCvvReleasePress) */       
    else  
    {
        
        /* Calculate the Volume using Pressure */         
        /* Front Wheel */
        /* 1st Brake */
        if(WL1->WPE_First_Control_VM==1)                    
        {                
            
		    if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
            }
            else if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
            }
            else
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
            }    

        }
        /* Nth Brake */                
        else
        {

		    if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
            }
            else if(WL1->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
            }
            else
            {
                WL1->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL1->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
                   
            }
        }

        /* Rear Wheel */
        /* 1st Brake */
        if(WL2->WPE_First_Control_VM==1)                    
        {                
            
		    if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
            }
            else if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
            }
            else
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
            }    

        }
        /* Nth Brake */                
        else
        {

		    if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
            }
            else if(WL2->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
            }
            else
            {
                WL2->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL2->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
                   
            }
        }
        /* Calculate the Volume using Pressure */
                
        WL1->ldabss16EstWheelPressMON_VM = WL1->ldabss16EstWheelPressMON_VM + 20; /* 121 or 122 */ 
        WL2->ldabss16EstWheelPressMON_VM = WL2->ldabss16EstWheelPressMON_VM + 20; /* 121 or 122 */

    }
    /* 2012_SWD_KJS_TCMF */
     
}

void LDABS_vEstWheelPressONE_Volume(struct W_STRUCT *WL3)
{
//    int16_t S16WPETemp0;
//    int16_t S16WPETemp1;
//    int16_t S16WPETemp2;
//    int16_t S16WPETemp3;
//    int16_t S16WPETemp4;
//    int16_t S16WPETemp5;
//    int16_t S16WPETemp6;
//    int16_t S16WPETemp7;
//    int16_t S16WPETemp8;
//    int16_t S16WPETemp9;
//    int16_t S16WPETemp10;
    
//    int16_t S16WPELfcHoldPress;
//    int16_t S16WPELimitLfcRisePress;
    int16_t S16WPEBrakeFluidVolumeCircuit;
        
    /* 2012_SWD_KJS_TCMF */    
    int16_t S16WPETCvvHoldPress;
    int16_t S16WPETTCvvReleasePress;    
    /* 2012_SWD_KJS_TCMF */
    
    int16_t S16WPENOvvPWMDuty;        

    /* C618_MGH80_BKT_JJJ */
    int16_t S16_Caliper_Model_Volume_f_N_1;
    int16_t S16_Caliper_Model_Volume_f_N_2;
    int16_t S16_Caliper_Model_Volume_f_N_3;
    int16_t S16_Caliper_Model_Volume_f_N_4;
    int16_t S16_Caliper_Model_Volume_f_N_5;
    int16_t S16_Caliper_Model_Volume_f_N_6; 
    int16_t S16_Caliper_Model_Volume_f_N_7;
    int16_t S16_Caliper_Model_Volume_f_N_8;
    int16_t S16_Caliper_Model_Volume_f_N_9;
    int16_t S16_Caliper_Model_Volume_f_N_10;
    int16_t S16_Caliper_Model_Volume_f_N_11;
    int16_t S16_Caliper_Model_Volume_f_N_12;
    int16_t S16_Caliper_Model_Volume_f_N_13;
    int16_t S16_Caliper_Model_Volume_f_N_14;
    int16_t S16_Caliper_Model_Volume_f_N_15;
    int16_t S16_Caliper_Model_Volume_f_N_16;

    int16_t S16_Caliper_Model_Volume_r_N_1;
    int16_t S16_Caliper_Model_Volume_r_N_2;
    int16_t S16_Caliper_Model_Volume_r_N_3;
    int16_t S16_Caliper_Model_Volume_r_N_4;
    int16_t S16_Caliper_Model_Volume_r_N_5;
    int16_t S16_Caliper_Model_Volume_r_N_6; 
    int16_t S16_Caliper_Model_Volume_r_N_7;
    int16_t S16_Caliper_Model_Volume_r_N_8;
    int16_t S16_Caliper_Model_Volume_r_N_9;
    int16_t S16_Caliper_Model_Volume_r_N_10;
    int16_t S16_Caliper_Model_Volume_r_N_11;
    int16_t S16_Caliper_Model_Volume_r_N_12;
    int16_t S16_Caliper_Model_Volume_r_N_13;
    int16_t S16_Caliper_Model_Volume_r_N_14;
    int16_t S16_Caliper_Model_Volume_r_N_15;
    int16_t S16_Caliper_Model_Volume_r_N_16;
    
    S16_Caliper_Model_Volume_f_N_1  = (int16_t)(S16_Caliper_Model_Volume_f_1  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_2  = (int16_t)(S16_Caliper_Model_Volume_f_2  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_3  = (int16_t)(S16_Caliper_Model_Volume_f_3  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_4  = (int16_t)(S16_Caliper_Model_Volume_f_4  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_5  = (int16_t)(S16_Caliper_Model_Volume_f_5  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_6  = (int16_t)(S16_Caliper_Model_Volume_f_6  + S16_Caliper_Model_Volume_No_f); 
    S16_Caliper_Model_Volume_f_N_7  = (int16_t)(S16_Caliper_Model_Volume_f_7  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_8  = (int16_t)(S16_Caliper_Model_Volume_f_8  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_9  = (int16_t)(S16_Caliper_Model_Volume_f_9  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_10 = (int16_t)(S16_Caliper_Model_Volume_f_10 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_11 = (int16_t)(S16_Caliper_Model_Volume_f_11 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_12 = (int16_t)(S16_Caliper_Model_Volume_f_12 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_13 = (int16_t)(S16_Caliper_Model_Volume_f_13 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_14 = (int16_t)(S16_Caliper_Model_Volume_f_14 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_15 = (int16_t)(S16_Caliper_Model_Volume_f_15 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_16 = (int16_t)(S16_Caliper_Model_Volume_f_16 + S16_Caliper_Model_Volume_No_f);

    S16_Caliper_Model_Volume_r_N_1  = (int16_t)(S16_Caliper_Model_Volume_r_1  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_2  = (int16_t)(S16_Caliper_Model_Volume_r_2  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_3  = (int16_t)(S16_Caliper_Model_Volume_r_3  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_4  = (int16_t)(S16_Caliper_Model_Volume_r_4  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_5  = (int16_t)(S16_Caliper_Model_Volume_r_5  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_6  = (int16_t)(S16_Caliper_Model_Volume_r_6  + S16_Caliper_Model_Volume_No_r); 
    S16_Caliper_Model_Volume_r_N_7  = (int16_t)(S16_Caliper_Model_Volume_r_7  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_8  = (int16_t)(S16_Caliper_Model_Volume_r_8  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_9  = (int16_t)(S16_Caliper_Model_Volume_r_9  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_10 = (int16_t)(S16_Caliper_Model_Volume_r_10 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_11 = (int16_t)(S16_Caliper_Model_Volume_r_11 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_12 = (int16_t)(S16_Caliper_Model_Volume_r_12 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_13 = (int16_t)(S16_Caliper_Model_Volume_r_13 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_14 = (int16_t)(S16_Caliper_Model_Volume_r_14 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_15 = (int16_t)(S16_Caliper_Model_Volume_r_15 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_16 = (int16_t)(S16_Caliper_Model_Volume_r_16 + S16_Caliper_Model_Volume_No_r);   
    /* C618_MGH80_BKT_JJJ */

    ldabsu1CircuitTCvalveAct = 0;

/* 2012_On_SWD_KJS */
    if(((WL3->LEFT_WHEEL==0)&&(WL3->REAR_WHEEL==0))||((WL3->LEFT_WHEEL==1)&&(WL3->REAR_WHEEL==1))) /* Primary : FR or RL */
	{
	    
	    S16WPEBrakeFluidVolumeCircuit = ldabss16BrakeFluidVolume_P;
	      
        ldabsu1CircuitTCvalveAct = TCL_DEMAND_fr;
      
        /* 2012_SWD_KJS_TCMF */        
        S16WPETCvvHoldPress = ldabss16WPETCvvHoldPress_Pri;
        S16WPETTCvvReleasePress = ldabss16WPE_TC_Release_P_Pri;        
        /* 2012_SWD_KJS_TCMF */

        if(WL3->REAR_WHEEL==0)
        {
            S16WPENOvvPWMDuty = (int16_t)la_FR1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = (int16_t)la_RL1HP.u8PwmDuty;            
        }
	  	  
	}
	else if(((WL3->LEFT_WHEEL==1)&&(WL3->REAR_WHEEL==0))||((WL3->LEFT_WHEEL==0)&&(WL3->REAR_WHEEL==1))) /* Secondary : FL or RR */
	{
	    
	    S16WPEBrakeFluidVolumeCircuit = ldabss16BrakeFluidVolume_S;
	         
        ldabsu1CircuitTCvalveAct = TCL_DEMAND_fl;
                
        /* 2012_SWD_KJS_TCMF */        
        S16WPETCvvHoldPress = ldabss16WPETCvvHoldPress_Scn;
        S16WPETTCvvReleasePress = ldabss16WPE_TC_Release_P_Scn;        
        /* 2012_SWD_KJS_TCMF */        

        if(WL3->REAR_WHEEL==0)
        {
            S16WPENOvvPWMDuty = (int16_t)la_FL1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = (int16_t)la_RR1HP.u8PwmDuty;            
        }
    
	}
	else
	{
	    S16WPEBrakeFluidVolumeCircuit = 0;
	    S16WPETCvvHoldPress = 0;
        S16WPETTCvvReleasePress = 0; 
	    S16WPENOvvPWMDuty = 0; 
        ldabsu1CircuitTCvalveAct = 0;	    
	}         

//    switch (WL3->Wheel_Valve_Mode)
//    {
//        /* No_0 */
//        /* NO_open && NC_close */
//        /* Including Pulse_Up Rise Mode */        
//        case Full_Rise_Valve_Mode: 
//            
//            /* One V/V Motor Rise */
//            if(ldabsu1CircuitTCvalveAct==1)
//            {
//                S16WPETemp1 = (int16_t)(((int32_t)S16WPEBrakeFluidVolumeCircuit*U16_Sampling_Time)/1000);
//
//                /* Straight Control Compensation (Alat_TCS_HDC) */
//                if(ldabsu1WPEStraightControl==1)
//                {
//                    if(WL3->REAR_WHEEL==0)
//                    {
//                        S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*U8_WPE_Straight_Cont_Comp_f)/100);
//                    }
//                    else
//                    {
//                        S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*U8_WPE_Straight_Cont_Comp_r)/100);
//                    }
//                }
//                else
//                {
//                    ;
//                }    
//                /* Straight Control Compensation (Alat_TCS_HDC) */
//
//                /* TC Control Compensation (TC Valve Conrol) */
//                if(S16WPETTCvvReleasePress < S16_WPE_Max_TCMF_Hold_Press)
//                {
//                    if(WL3->REAR_WHEEL==0)
//				    {                     
//                        S16WPETemp10 = LCESP_s16IInter6Point(S16WPETTCvvReleasePress/10,
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_1),(int16_t)(S16_TC_Rels_Comp_Gain_f_1),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_2),(int16_t)(S16_TC_Rels_Comp_Gain_f_2),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_3),(int16_t)(S16_TC_Rels_Comp_Gain_f_3),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_4),(int16_t)(S16_TC_Rels_Comp_Gain_f_4),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_5),(int16_t)(S16_TC_Rels_Comp_Gain_f_5),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_6),(int16_t)(S16_TC_Rels_Comp_Gain_f_6));
//                    }
//                    else
//                    {
//                        S16WPETemp10 = LCESP_s16IInter6Point(S16WPETTCvvReleasePress/10,
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_1),(int16_t)(S16_TC_Rels_Comp_Gain_r_1),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_2),(int16_t)(S16_TC_Rels_Comp_Gain_r_2),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_3),(int16_t)(S16_TC_Rels_Comp_Gain_r_3),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_4),(int16_t)(S16_TC_Rels_Comp_Gain_r_4),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_5),(int16_t)(S16_TC_Rels_Comp_Gain_r_5),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_6),(int16_t)(S16_TC_Rels_Comp_Gain_r_6));
//                    }
//                    
//                    S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*S16WPETemp10)/100);
//                }
//                else
//                {
//                    ;
//                }                             
//                /* TC Control Compensation (TC Valve Conrol) */                
//
//                /* ESV V/V Closed Condition */
//                if((((WL3->LEFT_WHEEL==0)&&(WL3->REAR_WHEEL==0))||((WL3->LEFT_WHEEL==1)&&(WL3->REAR_WHEEL==1)))&&(S_VALVE_RIGHT==0)) /* Primary : FR or RL */
//	            {
//                    S16WPETemp1 = 0;       
//	            }
//	            else if((((WL3->LEFT_WHEEL==1)&&(WL3->REAR_WHEEL==0))||((WL3->LEFT_WHEEL==0)&&(WL3->REAR_WHEEL==1)))&&(S_VALVE_LEFT==0)) /* Secondary : FL or RR */
//	            {
//                    S16WPETemp1 = 0;       
//	            }
//	            else
//	            {
//	                ;	    
//	            }
//                /* ESV V/V Closed Condition */
//
//                /* 2012_SWD_VV_Pulse_UP_Rise_KJS */
//                if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_1st) /* 10, 9 */
//                {
//                    S16WPETemp2 = (S16WPETemp1*(WL3->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
//                }
//                else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_2nd) /* 8, 7, 6 */
//                {
//                    S16WPETemp2 = (S16WPETemp1*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_1st))/One_Scan_Cycle_Time_MS;                            
//                }
//                else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd) /* 5, 4, 3 */
//                {
//                    S16WPETemp2 = (S16WPETemp1*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_2nd))/One_Scan_Cycle_Time_MS;                            
//                }
//                else /* 2, 1 */
//                {
//                    S16WPETemp2 = 0; /* No Rise */                           
//                }
//                /* 2012_SWD_VV_Pulse_UP_Rise_KJS */
//                               
//                WL3->ldabss16EstWheelVolume_VM = WL3->ldabss16EstWheelVolume_VM + S16WPETemp2;
//
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }                    
//
//                    WL3->ldabss16EstWheelPressMON_VM = 1;
//
//                }
//                /* Nth Brake */ 
//                else
//                {    
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//
//                    WL3->ldabss16EstWheelPressMON_VM = 2;
//
//                }
//                /* 2012_ON_SWD_KJS */
//                
//                /* 2012_SWD_KJS_TCMF */
//                if(WL3->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress)
//                {
//
//                    if(WL3->REAR_WHEEL==0)
//			        {                   
//                        S16WPETemp3 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                    }
//                    else
//                    {
//                        S16WPETemp3 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                    }                                       
//                        
//                    S16WPETemp4 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                        
//                    S16WPETemp5 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp4))))*(S16WPETemp3*100))/3162);
//                        
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp5;
//                                        
//                    /* 2012_ON_SWD_KJS */
//                    /* 1st Brake */
//                    if(WL3->WPE_First_Control_VM==1)                    
//                    {                
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                            }                                                
//                        }
//                    }
//                    /* Nth Brake */                
//                    else
//                    {
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                            }                                                
//                        }                    
//                    }
//                    /* 2012_ON_SWD_KJS */
//                                       
//                    WL3->ldabss16EstWheelPressMON_VM = 3;
//                    
//                }
//                else if(WL3->ldabss16EstWheelPress_VM < ldabss16WPEMpress_1_100bar)
//                {
//                    
//                    if(WL3->REAR_WHEEL==0)
//				    {                     
//                        S16WPETemp3 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_f_1_volume),(int16_t)(S16_MP_Rise_Gain_f_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_2_volume),(int16_t)(S16_MP_Rise_Gain_f_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_3_volume),(int16_t)(S16_MP_Rise_Gain_f_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_4_volume),(int16_t)(S16_MP_Rise_Gain_f_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_5_volume),(int16_t)(S16_MP_Rise_Gain_f_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_6_volume),(int16_t)(S16_MP_Rise_Gain_f_6_volume));
//                    }
//                    else
//                    {
//                        S16WPETemp3 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_r_1_volume),(int16_t)(S16_MP_Rise_Gain_r_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_2_volume),(int16_t)(S16_MP_Rise_Gain_r_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_3_volume),(int16_t)(S16_MP_Rise_Gain_r_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_4_volume),(int16_t)(S16_MP_Rise_Gain_r_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_5_volume),(int16_t)(S16_MP_Rise_Gain_r_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_6_volume),(int16_t)(S16_MP_Rise_Gain_r_6_volume));
//                    }
//                                          
//                    S16WPETemp4 = ldabss16WPEMpress_1_100bar - WL3->ldabss16EstWheelPress_VM;
//                    
//                    S16WPETemp5 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp4))))*(S16WPETemp3*100))/3162);                    
//
//                    /* 2012_C710_VV_Pulse_UP_Rise_KJS */
//                    if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_1st) /* 10, 9 */
//                    {
//                        S16WPETemp5 = (S16WPETemp5*(WL3->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
//                    }
//                    else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_2nd) /* 8, 7, 6 */
//                    {
//                        S16WPETemp5 = (S16WPETemp5*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_1st))/One_Scan_Cycle_Time_MS;                            
//                    }
//                    else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd) /* 5, 4, 3 */
//                    {
//                        S16WPETemp5 = (S16WPETemp5*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_2nd))/One_Scan_Cycle_Time_MS;                            
//                    }
//                    else /* 2, 1 */
//                    {
//                        S16WPETemp5 = 0; /* No Rise */                           
//                    }
//                    /* C710_KOR_VV_Pulse_UP_Rise_KJS */
//                                                                         
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM + S16WPETemp5;
//
//                    /* 1st Brake */
//                    if(WL3->WPE_First_Control_VM==1)                    
//                    {                
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                            }                                                
//                        }
//                    }
//                    /* Nth Brake */                
//                    else
//                    {
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                            }                                                
//                        }                    
//                    }
//                    /* 2012_ON_SWD_KJS */
//                                       
//                    WL3->ldabss16EstWheelPressMON_VM = 6;
//                
//                }
//                else
//                {
//                    ;
//                }
//                /* 2012_SWD_KJS_TCMF */                
//                                              
//            }
//            /* One V/V TC_OFF */
//            else
//            {
//                /* One V/V TC_OFF Rise => Driver Rise */
//            #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)                
//                if(mpress > MPRESS_3BAR) /* ( Check Point : MPRESS_5BAR -> MPRESS_3BAR )_KJS */
//            #else
//                if(lsesps16MpressByAHBgen3 > MPRESS_3BAR) /* ( Check Point : MPRESS_5BAR -> MPRESS_3BAR )_KJS */            
//            #endif
//                {
//                    if(WL3->REAR_WHEEL==0)
//				    {                     
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_f_1_volume),(int16_t)(S16_MP_Rise_Gain_f_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_2_volume),(int16_t)(S16_MP_Rise_Gain_f_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_3_volume),(int16_t)(S16_MP_Rise_Gain_f_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_4_volume),(int16_t)(S16_MP_Rise_Gain_f_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_5_volume),(int16_t)(S16_MP_Rise_Gain_f_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_6_volume),(int16_t)(S16_MP_Rise_Gain_f_6_volume));
//                    }
//                    else
//                    {
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_r_1_volume),(int16_t)(S16_MP_Rise_Gain_r_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_2_volume),(int16_t)(S16_MP_Rise_Gain_r_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_3_volume),(int16_t)(S16_MP_Rise_Gain_r_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_4_volume),(int16_t)(S16_MP_Rise_Gain_r_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_5_volume),(int16_t)(S16_MP_Rise_Gain_r_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_6_volume),(int16_t)(S16_MP_Rise_Gain_r_6_volume));
//                    }                        
//                
//                    S16WPETemp2 = ldabss16WPEMpress_1_100bar - WL3->ldabss16EstWheelPress_VM;
//                    
//                    if(S16WPETemp2>=0)
//                    {
//                        
//                        S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//
//                        /* 2012_SWD_VV_Pulse_UP_Rise_KJS */
//                        if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_1st) /* 10, 9 */
//                        {
//                            S16WPETemp0 = (S16WPETemp3*(WL3->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
//                        }
//                        else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_2nd) /* 8, 7, 6 */
//                        {
//                            S16WPETemp0 = (S16WPETemp3*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_1st))/One_Scan_Cycle_Time_MS;                            
//                        }
//                        else if(WL3->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd) /* 5, 4, 3 */
//                        {
//                            S16WPETemp0 = (S16WPETemp3*((WL3->pwm_full_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_2nd))/One_Scan_Cycle_Time_MS;                            
//                        }
//                        else /* 2, 1 */
//                        {
//                            S16WPETemp0 = 0; /* No Rise */                           
//                        }
//                        /* 2012_SWD_VV_Pulse_UP_Rise_KJS */
//                        
//                        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM + S16WPETemp0;                       
//                    }
//                    else
//                    {
//
//                        S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)-S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                        
//                        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;                        
//                    } 
//                    
//                    WL3->ldabss16EstWheelPressMON_VM = 4;    
//                
//                }
//                /* One V/V TC_OFF Rise => TC Dump */
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//					{                   
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                    }
//                    else
//                    {
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                    }                                       
//                    
//                    S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                    
//                    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                    
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;
//                    
//                    WL3->ldabss16EstWheelPressMON_VM = 5;                    
//                                       
//                }
//
//                /* Calculate the Volume using Pressure */
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {                
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                        }                                                
//                    }
//                }
//                /* Nth Brake */                
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                        }                                                
//                    }                    
//                }
//                /* 2012_ON_SWD_KJS */
//                /* Calculate the Volume using Pressure */
//                           
//            }    
//                        
//        break;
//
//        /* No_1 */
//        /* One V/V LFC Rise */
//        /* NO_open && NC_close */                
//        case LFC_Rise_Valve_Mode:
//
//            /** Calculate the LFC Hold Press **/           
//		    if(WL3->REAR_WHEEL==0)
//			{
//                S16WPELfcHoldPress = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_1st),(int16_t)(S16_LFC_Hold_press_f_1st),
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_2nd),(int16_t)(S16_LFC_Hold_press_f_2nd),
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_3rd),(int16_t)(S16_LFC_Hold_press_f_3rd),
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_4th),(int16_t)(S16_LFC_Hold_press_f_4th),
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_5th),(int16_t)(S16_LFC_Hold_press_f_5th),
//                                		           (int16_t)(U8_LFC_Hold_Duty_f_6th),(int16_t)(S16_LFC_Hold_press_f_6th));
//		    }
//			else
//			{
//			    S16WPELfcHoldPress = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_1st),(int16_t)(S16_LFC_Hold_press_r_1st),
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_2nd),(int16_t)(S16_LFC_Hold_press_r_2nd),
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_3rd),(int16_t)(S16_LFC_Hold_press_r_3rd),
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_4th),(int16_t)(S16_LFC_Hold_press_r_4th),
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_5th),(int16_t)(S16_LFC_Hold_press_r_5th),
//                                		           (int16_t)(U8_LFC_Hold_Duty_r_6th),(int16_t)(S16_LFC_Hold_press_r_6th));								
//			}
//            
//            /* Delta P : Rise Limitation of Wheel_P */
//            /* Think More for ESC */            
//    		S16WPELimitLfcRisePress = (S16WPETTCvvReleasePress/10) - S16WPELfcHoldPress; /* 1_10bar */
//            /* Think More for ESC */
//											
//    		if(S16WPELimitLfcRisePress > MPRESS_0BAR) /* 1_10bar */
//    		{
//    		    ;
//    		}
//    		else
//    		{
//    		    S16WPELimitLfcRisePress = 0;
//    		}                						
//            /* Delta P : Rise Limitation of Wheel_P */						
//			/** Calculate the LFC Hold Press **/
//
//            /* ABS_LFC_RISE_MODE */
//            if(ldabsu1CircuitTCvalveAct==0)
//            { 			    
//			    /* LFC Hold or TC Dump */			
//			    if(WL3->ldabss16EstWheelPress_VM > (S16WPELimitLfcRisePress*10))  /* 1_100bar */
//			    {
//			        /* TC Dump */
//			        if(WL3->ldabss16EstWheelPress_VM > ldabss16WPEMpress_1_100bar)
//			        {
//                        if(WL3->REAR_WHEEL==0)
//					    {                   
//                            S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                        }
//                        else
//                        {
//                            S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                            (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                        }                                       
//                        
//                        S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - (S16WPELimitLfcRisePress*10);
//                        
//                        S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                        
//                        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;
//                        
//			    	    WL3->ldabss16EstWheelPressMON_VM = 10;
//			        }
//			        /* LFC Hold */			        
//			        else
//			        {
//                        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM;
//
//			    	    WL3->ldabss16EstWheelPressMON_VM = 16;
//			        }
//			        
//			    }
//			    /* ABS LFC Rise */
//			    else
//			    {
//			    	if(WL3->REAR_WHEEL==0)
//			    	{
//			    			    
//			    	    S16WPETemp1 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_1st),(int16_t)(S16_LFC_Rise_Gain_f_1st),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_2nd),(int16_t)(S16_LFC_Rise_Gain_f_2nd),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_3rd),(int16_t)(S16_LFC_Rise_Gain_f_3rd),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_4th),(int16_t)(S16_LFC_Rise_Gain_f_4th),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_5th),(int16_t)(S16_LFC_Rise_Gain_f_5th),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_f_6th),(int16_t)(S16_LFC_Rise_Gain_f_6th));
//                                    		               
//                    }
//                    else
//                    {
//			    			    
//			    		S16WPETemp1 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_1st),(int16_t)(S16_LFC_Rise_Gain_r_1st),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_2nd),(int16_t)(S16_LFC_Rise_Gain_r_2nd),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_3rd),(int16_t)(S16_LFC_Rise_Gain_r_3rd),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_4th),(int16_t)(S16_LFC_Rise_Gain_r_4th),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_5th),(int16_t)(S16_LFC_Rise_Gain_r_5th),
//                                    		        (int16_t)(U8_LFC_Rise_Duty_r_6th),(int16_t)(S16_LFC_Rise_Gain_r_6th));
//                                           
//                    }
//                
//			    	S16WPETemp2 = (S16WPELimitLfcRisePress*10) - WL3->ldabss16EstWheelPress_VM;
//			    			    
//			        S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                
//                    S16WPETemp4 = (S16WPETemp3*(WL3->pwm_lfc_rise_cnt))/One_Scan_Cycle_Time_MS;
//			    			    
//			        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM + S16WPETemp4;
//                
//			    	WL3->ldabss16EstWheelPressMON_VM = 11;
//			    			                                    		               
//			    }
//
//                /* Calculate the Volume using Pressure */
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {                
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                        }                                                
//                    }
//                }
//                /* Nth Brake */                
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                        }                                                
//                    }                    
//                }
//                /* 2012_ON_SWD_KJS */
//                /* Calculate the Volume using Pressure */
//
//            }
//            /* ESC_LFC_RISE_MODE : if(ldabsu1CircuitTCvalveAct==1) */
//            else
//            {
//                
//                /* 2012_KOR_ESC_LFC_Rise_JJJ */
//                         
//                S16WPETemp1 = (int16_t)(((int32_t)S16WPEBrakeFluidVolumeCircuit*U16_Sampling_Time)/1000);
//
//                /* Straight Control Compensation (Alat_TCS_HDC) */
//                if(ldabsu1WPEStraightControl==1)
//                {
//                    if(WL3->REAR_WHEEL==0)
//                    {
//                        S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*U8_WPE_Straight_Cont_Comp_f)/100);
//                    }
//                    else
//                    {
//                        S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*U8_WPE_Straight_Cont_Comp_r)/100);
//                    }
//                }
//                else
//                {
//                    ;
//                }    
//                /* Straight Control Compensation (Alat_TCS_HDC) */
//
//                /* TC Control Compensation (TC Valve Conrol) */
//                if(S16WPETTCvvReleasePress < S16_WPE_Max_TCMF_Hold_Press)
//                {
//                    if(WL3->REAR_WHEEL==0)
//				    {                     
//                        S16WPETemp10 = LCESP_s16IInter6Point(S16WPETTCvvReleasePress/10,
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_1),(int16_t)(S16_TC_Rels_Comp_Gain_f_1),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_2),(int16_t)(S16_TC_Rels_Comp_Gain_f_2),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_3),(int16_t)(S16_TC_Rels_Comp_Gain_f_3),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_4),(int16_t)(S16_TC_Rels_Comp_Gain_f_4),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_5),(int16_t)(S16_TC_Rels_Comp_Gain_f_5),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_f_6),(int16_t)(S16_TC_Rels_Comp_Gain_f_6));
//                    }
//                    else
//                    {
//                        S16WPETemp10 = LCESP_s16IInter6Point(S16WPETTCvvReleasePress/10,
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_1),(int16_t)(S16_TC_Rels_Comp_Gain_r_1),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_2),(int16_t)(S16_TC_Rels_Comp_Gain_r_2),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_3),(int16_t)(S16_TC_Rels_Comp_Gain_r_3),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_4),(int16_t)(S16_TC_Rels_Comp_Gain_r_4),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_5),(int16_t)(S16_TC_Rels_Comp_Gain_r_5),
//                                                  (int16_t)(S16_TC_Rels_Comp_Press_r_6),(int16_t)(S16_TC_Rels_Comp_Gain_r_6));
//                    }
//                    
//                    S16WPETemp1 = (int16_t)(((int32_t)S16WPETemp1*S16WPETemp10)/100);
//                }
//                else
//                {
//                    ;
//                }                             
//                /* TC Control Compensation (TC Valve Conrol) */  
//
//			    if(WL3->REAR_WHEEL==0)
//			    {
//			    		    
//			        S16WPETemp2 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_1st),(int16_t)(S16_LFC_ESC_Rise_Gain_f_1st),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_2nd),(int16_t)(S16_LFC_ESC_Rise_Gain_f_2nd),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_3rd),(int16_t)(S16_LFC_ESC_Rise_Gain_f_3rd),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_4th),(int16_t)(S16_LFC_ESC_Rise_Gain_f_4th),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_5th),(int16_t)(S16_LFC_ESC_Rise_Gain_f_5th),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_f_6th),(int16_t)(S16_LFC_ESC_Rise_Gain_f_6th));
//                                		               
//                }
//                else
//                {
//			    		    
//			    	S16WPETemp2 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_1st),(int16_t)(S16_LFC_ESC_Rise_Gain_r_1st),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_2nd),(int16_t)(S16_LFC_ESC_Rise_Gain_r_2nd),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_3rd),(int16_t)(S16_LFC_ESC_Rise_Gain_r_3rd),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_4th),(int16_t)(S16_LFC_ESC_Rise_Gain_r_4th),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_5th),(int16_t)(S16_LFC_ESC_Rise_Gain_r_5th),
//                                		        (int16_t)(U8_LFC_ESC_Rise_Duty_r_6th),(int16_t)(S16_LFC_ESC_Rise_Gain_r_6th));
//                                       
//                }
//
//			    if(WL3->REAR_WHEEL==0)
//			    {
//			    		    
//			        S16WPETemp7 = LCESP_s16IInter6Point((int16_t)WL3->ldabss16EstWheelPress_VM,
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_1st),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_1st),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_2nd),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_2nd),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_3rd),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_3rd),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_4th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_4th),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_5th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_5th),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_f_6th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_f_6th));
//                                		               
//                }
//                else
//                {
//			    		    
//			    	S16WPETemp7 = LCESP_s16IInter6Point((int16_t)WL3->ldabss16EstWheelPress_VM,
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_1st),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_1st),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_2nd),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_2nd),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_3rd),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_3rd),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_4th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_4th),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_5th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_5th),
//                                		        (int16_t)(S16_LFC_ESC_Rise_Press_r_6th),(int16_t)(S16_LFC_ESC_Rise_Gain_2_r_6th));
//                                       
//                }
//
//                S16WPETemp3 = (int16_t)(((int32_t)S16WPETemp1*S16WPETemp2)/100);
//                S16WPETemp8 = (int16_t)(((int32_t)S16WPETemp3*S16WPETemp7)/100);                
//
//                S16WPETemp4 = (S16WPETemp8*(WL3->pwm_lfc_rise_cnt))/One_Scan_Cycle_Time_MS;
//                
//                WL3->ldabss16EstWheelVolume_VM = WL3->ldabss16EstWheelVolume_VM + S16WPETemp4;
//
//                /* 2012_KOR_ESC_LFC_Rise_JJJ */
//                
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }                    
//                
//                    WL3->ldabss16EstWheelPressMON_VM = 12;
//                
//                }
//                /* Nth Brake */ 
//                else
//                {    
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                
//                    WL3->ldabss16EstWheelPressMON_VM = 13;
//                
//                }
//                /* 2012_ON_SWD_KJS */
//                
//                /* 2012_SWD_KJS_TCMF */
//                /* TC_Dump */
//                if(WL3->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress)
//                {
//                
//                    if(WL3->REAR_WHEEL==0)
//			        {                   
//                        S16WPETemp5 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                    }
//                    else
//                    {
//                        S16WPETemp5 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                    }                                       
//                        
//                    S16WPETemp6 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                        
//                    S16WPETemp0 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp6))))*(S16WPETemp5*100))/3162);
//                        
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp0;
//                                        
//                    /* 2012_ON_SWD_KJS */
//                    /* 1st Brake */
//                    if(WL3->WPE_First_Control_VM==1)                    
//                    {                
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                            }                                                
//                        }
//                    }
//                    /* Nth Brake */                
//                    else
//                    {
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                            }                                                
//                        }                    
//                    }
//                    /* 2012_ON_SWD_KJS */
//                                       
//                    WL3->ldabss16EstWheelPressMON_VM = 14;
//                    
//                }
//                /* ESC_LFC_MP_Rise */
//                else if(WL3->ldabss16EstWheelPress_VM < ldabss16WPEMpress_1_100bar)
//                {
//                    
//			    	if(WL3->REAR_WHEEL==0)
//			    	{
//			    			    
//			    	    S16WPETemp5 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_1st),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_1st),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_2nd),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_2nd),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_3rd),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_3rd),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_4th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_4th),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_5th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_5th),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_f_6th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_f_6th));
//                                    		               
//                    }
//                    else
//                    {
//			    			    
//			    		S16WPETemp5 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_1st),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_1st),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_2nd),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_2nd),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_3rd),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_3rd),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_4th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_4th),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_5th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_5th),
//                                    		        (int16_t)(U8_LFC_ESC_MP_Rise_Duty_r_6th),(int16_t)(S16_LFC_ESC_MP_Rise_Gain_r_6th));                                           
//                    
//                    }
//                
//			    	S16WPETemp6 = (S16WPELimitLfcRisePress*10) - WL3->ldabss16EstWheelPressOld_VM;
//			    			    
//			        S16WPETemp0 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp6))))*(S16WPETemp5*100))/3162);
//                
//                    S16WPETemp9 = (S16WPETemp0*(WL3->pwm_lfc_rise_cnt))/One_Scan_Cycle_Time_MS;
//			    			    
//			        WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPressOld_VM + S16WPETemp9;
//                             
//                    /* 1st Brake */
//                    if(WL3->WPE_First_Control_VM==1)                    
//                    {                
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                            }                                                
//                        }
//                    }
//                    /* Nth Brake */                
//                    else
//                    {
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                            }                                                
//                        }                    
//                    }
//                    /* 2012_ON_SWD_KJS */
//                                       
//                    WL3->ldabss16EstWheelPressMON_VM = 15;
//                
//                }
//                else
//                {
//                    ;
//                }
//                /* 2012_SWD_KJS_TCMF */                
//                                                  
//            }
//            												                        
//        break;
//
//        /* No_2 */
//        /* One V/V HOLD */
//        /* NO_close && NC_close */                
//        case Hold_Valve_Mode:
//
//            /* One V/V Motor Rise */
//            if(ldabsu1CircuitTCvalveAct==1)
//            {
//                WL3->ldabss16EstWheelPress_VM   = WL3->ldabss16EstWheelPress_VM;
//                WL3->ldabss16EstWheelVolume_VM  = WL3->ldabss16EstWheelVolume_VM;
//                WL3->ldabss16EstWheelPressMON_VM = 20;
//                
//                /* 2012_SWD_KJS_TCMF */
//                if(WL3->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress) /* TC_Dump */
//                {
//
//                    if(WL3->REAR_WHEEL==0)
//					{                   
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                    }
//                    else
//                    {
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                    }                                       
//                    
//                    S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                    
//                    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                    
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;
//                    
//                    /* 2012_ON_SWD_KJS */
//                    /* 1st Brake */
//                    if(WL3->WPE_First_Control_VM==1)                    
//                    {                
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                            }                                                
//                        }
//                    }
//                    /* Nth Brake */                
//                    else
//                    {
//                        if(WL3->REAR_WHEEL==0)
//			            {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                            }    
//                        }
//                        else
//                        {
//			                if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                            }
//                            else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                            }
//                            else
//                            {
//                                WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                            }                                                
//                        }                    
//                    }
//                    /* 2012_ON_SWD_KJS */
//                                       
//                    WL3->ldabss16EstWheelPressMON_VM = 21;
//                    
//                }
//                else
//                {
//                    ;
//                }
//                /* 2012_SWD_KJS_TCMF */                 
//                                
//            }
//            else if(WL3->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress) /* TC_Dump -> only MP  */
//            {
//                
//                if(WL3->REAR_WHEEL==0)
//			    {                   
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                }
//                else
//                {
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                }                                       
//                
//                S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                
//                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                
//                WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;
//                
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {                
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                        }                                                
//                    }
//
//                    WL3->ldabss16EstWheelPressMON_VM = 22; 
//
//                }
//                /* Nth Brake */                
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                        }                                                
//                    }                    
//
//                    WL3->ldabss16EstWheelPressMON_VM = 23;
//
//                }
//                /* 2012_ON_SWD_KJS */
//            						               
//            }
//            else
//            {
//                WL3->ldabss16EstWheelPress_VM   = WL3->ldabss16EstWheelPress_VM;
//                WL3->ldabss16EstWheelVolume_VM  = WL3->ldabss16EstWheelVolume_VM;
//                WL3->ldabss16EstWheelPressMON_VM = 24;                
//            }
//                        
//        break;        
//
//        /* No_3 */
//        /* One V/V Dump */
//        /* NO_close && NC_open */        
//        case Dump_Valve_Mode:
//
//            /* NC_Dump */ 
//            if(ldabsu1CircuitTCvalveAct==1)
//            {
//                if(WL3->REAR_WHEEL==0)
//			    {                   
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_1st),(int16_t)(S16_NC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_2nd),(int16_t)(S16_NC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_3rd),(int16_t)(S16_NC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_4th),(int16_t)(S16_NC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_5th),(int16_t)(S16_NC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_6th),(int16_t)(S16_NC_Dump_Gain_f_6th));
//                }
//                else
//                {
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_1st),(int16_t)(S16_NC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_2nd),(int16_t)(S16_NC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_3rd),(int16_t)(S16_NC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_4th),(int16_t)(S16_NC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_5th),(int16_t)(S16_NC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_6th),(int16_t)(S16_NC_Dump_Gain_r_6th));                        
//                }            
//
//                WL3->ldabss16EstWheelPressMON_VM = 31;
//
//            }
//            /* 2012_On_SWD_KJS */
//            else if(WL3->ldabss16EstWheelPress_VM <= (S16WPETTCvvReleasePress+100)) /* NC_Dump -> only MP */
//            {                            
//                if(WL3->REAR_WHEEL==0)
//			    {                   
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_1st),(int16_t)(S16_NC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_2nd),(int16_t)(S16_NC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_3rd),(int16_t)(S16_NC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_4th),(int16_t)(S16_NC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_5th),(int16_t)(S16_NC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_6th),(int16_t)(S16_NC_Dump_Gain_f_6th));
//                }
//                else
//                {
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_1st),(int16_t)(S16_NC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_2nd),(int16_t)(S16_NC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_3rd),(int16_t)(S16_NC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_4th),(int16_t)(S16_NC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_5th),(int16_t)(S16_NC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_6th),(int16_t)(S16_NC_Dump_Gain_r_6th));                        
//                }            
//
//                WL3->ldabss16EstWheelPressMON_VM = 32;
//
//            }
//            else /* NC_Dump + TC_Dump */
//            {
//                if(WL3->REAR_WHEEL==0)
//			    {
//			              /* NC_Dump_Gain */                   
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_1st),(int16_t)(S16_NC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_2nd),(int16_t)(S16_NC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_3rd),(int16_t)(S16_NC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_4th),(int16_t)(S16_NC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_5th),(int16_t)(S16_NC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_6th),(int16_t)(S16_NC_Dump_Gain_f_6th));
//			              /* TC_Dump_Gain */
//                    S16WPETemp5 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                }
//                else
//                {
//                    /* NC_Dump_Gain */
//                    S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_1st),(int16_t)(S16_NC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_2nd),(int16_t)(S16_NC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_3rd),(int16_t)(S16_NC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_4th),(int16_t)(S16_NC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_5th),(int16_t)(S16_NC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_6th),(int16_t)(S16_NC_Dump_Gain_r_6th));                        
//                    /* TC_Dump_Gain */
//                    S16WPETemp5 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th)); 
//                }
//
//                S16WPETemp1 = S16WPETemp1 + S16WPETemp5;
//
//                WL3->ldabss16EstWheelPressMON_VM = 33;
//
//            }
//            /* 2012_On_SWD_KJS */
//
//            S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - S16_WPE_LPA_Pressure;
//            
//			S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);                        
//
//			S16WPETemp0 = (int16_t)((((int32_t)WL3->pwm_dump_cnt)*S16WPETemp3)/One_Scan_Cycle_Time_MS);
//            
//            WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp0;
//
//            /* Calculate the Volume using Pressure */
//            /* 2012_ON_SWD_KJS */
//            /* 1st Brake */
//            if(WL3->WPE_First_Control_VM==1)                    
//            {                
//                if(WL3->REAR_WHEEL==0)
//			    {
//			        if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                    (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                    (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                    (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                    (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                    (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                    }
//                    else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                    (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                    (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                    (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                    (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                    (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                    }
//                    else
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                   (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                   (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                   (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                   (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                   (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                   (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                    }    
//                }
//                else
//                {
//			        if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                    (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                    (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                    (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                    (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                    (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                    }
//                    else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                    (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                    (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                    (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                    (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                    (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                    }
//                    else
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                   (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                   (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                   (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                   (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                   (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                   (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                    }                                                
//                }
//            }
//            /* Nth Brake */                
//            else
//            {
//                if(WL3->REAR_WHEEL==0)
//			    {
//			        if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                    (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                    (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                    (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                    (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                    (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                    }
//                    else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                    (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                    (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                    (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                    (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                    (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                    }
//                    else
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                   (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                   (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                   (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                   (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                   (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                   (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                    }    
//                }
//                else
//                {
//			        if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                    (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                    (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                    (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                    (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                    (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                    }
//                    else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                    (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                    (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                    (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                    (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                    (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                    (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                    }
//                    else
//                    {
//                        WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                   (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                   (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                   (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                   (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                   (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                   (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                    }                                                
//                }                    
//            }
//            /* 2012_ON_SWD_KJS */
//            /* Calculate the Volume using Pressure */                                       
//                        
//        break;
//
//        /* No_4 */               
//        /* NO_open && NC_open */
//        case Circulation_Valve_Mode:
//
//            /* Motor Rise */
//            if(ldabsu1CircuitTCvalveAct==1)
//            {
//                S16WPETemp1 = (int16_t)(((int32_t)S16WPEBrakeFluidVolumeCircuit*U16_Sampling_Time)/1000);
//                /* Wheel Path : 50% -- NC V/V Path : 50% */
//                S16WPETemp2 = S16WPETemp1/2; 
//                WL3->ldabss16EstWheelVolume_VM = WL3->ldabss16EstWheelVolume_VM + S16WPETemp2;
//
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_6)
//			            {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			             if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_6)
//			             {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_N_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_N_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }                    
//                }
//                /* Nth Brake */ 
//                else
//                {    
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			             if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_6)
//			             {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_f_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_f_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                    else
//                    {
//			             if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_6)
//			             {		        
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_1),(int16_t)(S16_Caliper_Model_Press_1),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_2),(int16_t)(S16_Caliper_Model_Press_2),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_3),(int16_t)(S16_Caliper_Model_Press_3),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_4),(int16_t)(S16_Caliper_Model_Press_4),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_5),(int16_t)(S16_Caliper_Model_Press_5),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6),(int16_t)(S16_Caliper_Model_Press_6));
//                        }
//                        else if(WL3->ldabss16EstWheelVolume_VM <= S16_Caliper_Model_Volume_r_11)
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_6), (int16_t)(S16_Caliper_Model_Press_6),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_7), (int16_t)(S16_Caliper_Model_Press_7),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_8), (int16_t)(S16_Caliper_Model_Press_8),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_9), (int16_t)(S16_Caliper_Model_Press_9),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_10),(int16_t)(S16_Caliper_Model_Press_10),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11),(int16_t)(S16_Caliper_Model_Press_11));                       
//                        }    
//                        else
//                        {
//                            WL3->ldabss16EstWheelPress_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelVolume_VM,
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_11), (int16_t)(S16_Caliper_Model_Press_11),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_12), (int16_t)(S16_Caliper_Model_Press_12),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_13), (int16_t)(S16_Caliper_Model_Press_13),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_14), (int16_t)(S16_Caliper_Model_Press_14),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_15), (int16_t)(S16_Caliper_Model_Press_15),
//                                                                           (int16_t)(S16_Caliper_Model_Volume_r_16), (int16_t)(S16_Caliper_Model_Press_16));                       
//                        }
//                    }
//                }
//                /* 2012_ON_SWD_KJS */
//                
//                WL3->ldabss16EstWheelPressMON_VM = 40;                 
//            }
//            else 
//            {
//                /* TC_Dump */                
//                if(WL3->ldabss16EstWheelPress_VM > S16WPETTCvvReleasePress)
//                {
//                    
//                    if(WL3->REAR_WHEEL==0)
//			        {                   
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
//                    }
//                    else
//                    {
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
//                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
//                    }                                       
//                        
//                    S16WPETemp2 = WL3->ldabss16EstWheelPress_VM - S16WPETTCvvReleasePress;
//                        
//                    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                        
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM - S16WPETemp3;
//                        
//                    WL3->ldabss16EstWheelPressMON_VM = 41;                    
//
//                }
//                /* MP Rise */
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//				    {                     
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_f_1_volume),(int16_t)(S16_MP_Rise_Gain_f_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_2_volume),(int16_t)(S16_MP_Rise_Gain_f_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_3_volume),(int16_t)(S16_MP_Rise_Gain_f_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_4_volume),(int16_t)(S16_MP_Rise_Gain_f_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_5_volume),(int16_t)(S16_MP_Rise_Gain_f_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_f_6_volume),(int16_t)(S16_MP_Rise_Gain_f_6_volume));
//                    }
//                    else
//                    {
//                        S16WPETemp1 = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM/10,
//                                                        (int16_t)(S16_MP_Rise_Press_r_1_volume),(int16_t)(S16_MP_Rise_Gain_r_1_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_2_volume),(int16_t)(S16_MP_Rise_Gain_r_2_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_3_volume),(int16_t)(S16_MP_Rise_Gain_r_3_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_4_volume),(int16_t)(S16_MP_Rise_Gain_r_4_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_5_volume),(int16_t)(S16_MP_Rise_Gain_r_5_volume),
//                                                        (int16_t)(S16_MP_Rise_Press_r_6_volume),(int16_t)(S16_MP_Rise_Gain_r_6_volume));
//                    }                                       
//                        
//                    S16WPETemp2 = S16WPETTCvvReleasePress - WL3->ldabss16EstWheelPress_VM;
//                        
//                    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
//                        
//                    WL3->ldabss16EstWheelPress_VM = WL3->ldabss16EstWheelPress_VM + S16WPETemp3;
//                        
//                    WL3->ldabss16EstWheelPressMON_VM = 42;
//                }
//                                                       
//                /* 2012_ON_SWD_KJS */
//                /* 1st Brake */
//                if(WL3->WPE_First_Control_VM==1)                    
//                {                
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
//                        }                                                
//                    }
//                }
//                /* Nth Brake */                
//                else
//                {
//                    if(WL3->REAR_WHEEL==0)
//			        {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
//                        }    
//                    }
//                    else
//                    {
//			            if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
//                                                                        (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
//                                                                        (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
//                                                                        (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
//                                                                        (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
//                                                                        (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
//                        }
//                        else if(WL3->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                        (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
//                                                                        (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
//                                                                        (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
//                                                                        (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
//                                                                        (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
//                                                                        (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
//                        }
//                        else
//                        {
//                            WL3->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL3->ldabss16EstWheelPress_VM,
//                                                                       (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
//                                                                       (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
//                                                                       (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
//                                                                       (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
//                                                                       (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
//                                                                       (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
//                        }                                                
//                    }                    
//                }
//                /* 2012_ON_SWD_KJS */
//                         
//            }
//                       
//        break;                
//
//        /* No_5 */        
//        default:
//            
//            WL3->ldabss16EstWheelPress_VM   = WL3->ldabss16EstWheelPress_VM;
//            WL3->ldabss16EstWheelVolume_VM  = WL3->ldabss16EstWheelVolume_VM;           
//            WL3->ldabss16EstWheelPressMON_VM = 50;
//        
//        break;    
//        
//    }
    
}

void LDABS_vOutputWheelPress_Volume(void)
{

    if(EST_WHEEL_PRESS_OK_FLG==1)
    {

        if(FL.ldabss16EstWheelPress_VM < S16_WPE_MPRESS_0BAR)    
        {    
            FL.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
        }
        else
        {
            ;
        }    
        
        if(FR.ldabss16EstWheelPress_VM < S16_WPE_MPRESS_0BAR)    
        {    
            FR.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
        }
        else
        {
            ;
        } 
        
        if(RL.ldabss16EstWheelPress_VM < S16_WPE_MPRESS_0BAR)    
        {    
            RL.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
        }
        else
        {
            ;
        }     
        
        if(RR.ldabss16EstWheelPress_VM < S16_WPE_MPRESS_0BAR)    
        {    
            RR.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
        }
        else
        {
            ;
        }
    
    }
    else
    {
        FL.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;        
        FR.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;        
        RL.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
        RR.ldabss16EstWheelPress_VM = S16_WPE_MPRESS_0BAR;
    } 

/* 2012_SWD_KJS <Change> */
#if __SPLIT_TYPE /*FR Split  */

#else /* X Split */   

    #if (__FL_PRESS==ENABLE)
    #else    
    /* FL */
    FL.Estimated_Active_Press = FL.ldabss16EstWheelPress_VM;                     /*  1_100[bar] */
    FL.u8_Estimated_Active_Press = (uint8_t)(FL.ldabss16EstWheelPress_VM/100);   /*  1_1[bar]   */
    FL.s16_Estimated_Active_Press = FL.ldabss16EstWheelPress_VM/10;              /*  1_10[bar]  */       
    #endif

    #if (__FR_PRESS==ENABLE)
    #else 
    /* FR */
    FR.Estimated_Active_Press = FR.ldabss16EstWheelPress_VM;                     /*  1_100[bar] */
    FR.u8_Estimated_Active_Press = (uint8_t)(FR.ldabss16EstWheelPress_VM/100);   /*  1_1[bar]   */
    FR.s16_Estimated_Active_Press = FR.ldabss16EstWheelPress_VM/10;              /*  1_10[bar]  */
    #endif

    #if (__RL_PRESS==ENABLE)
    #else
    /* RL */
    RL.Estimated_Active_Press = RL.ldabss16EstWheelPress_VM;                     /*  1_100[bar] */
    RL.u8_Estimated_Active_Press = (uint8_t)(RL.ldabss16EstWheelPress_VM/100);   /*  1_1[bar]   */
    RL.s16_Estimated_Active_Press = RL.ldabss16EstWheelPress_VM/10;              /*  1_10[bar]  */
    #endif

    #if (__RR_PRESS==ENABLE)
    #else
    /* RR */
    RR.Estimated_Active_Press = RR.ldabss16EstWheelPress_VM;                     /*  1_100[bar] */
    RR.u8_Estimated_Active_Press = (uint8_t)(RR.ldabss16EstWheelPress_VM/100);   /*  1_1[bar]   */
    RR.s16_Estimated_Active_Press = RR.ldabss16EstWheelPress_VM/10;              /*  1_10[bar]  */        
    #endif

#endif
/* 2012_SWD_KJS <Change> */
    
}

/* 2012_On_SWD_KJS */
void LDABS_vEstFirstCycleCtrl_Volume(struct W_STRUCT *WL4)
{
    WL4->ldabss16BrakeOnTimerOld_VM = WL4->ldabss16BrakeOnTimer_VM;

    /* Calculation of Brake On Time */
    if(((WL4->ldabss16BrakeOnTimer_VM==0)&&(WL4->ldabss16EstWheelPress_VM > U16_BrakeOnCheckPress_2))||((WL4->ldabss16BrakeOnTimer_VM > 0)&&(WL4->ldabss16EstWheelPress_VM > U16_BrakeOnCheckPress_1)))
    {
        WL4->ldabss16BrakeOnTimer_VM = U16_RollBack_Time;
    }
    else
    {
        WL4->ldabss16BrakeOnTimer_VM = WL4->ldabss16BrakeOnTimer_VM - 1;
    }

    if(WL4->ldabss16BrakeOnTimer_VM < 0)
    {
       WL4->ldabss16BrakeOnTimer_VM = 0;
    }
    else
    {
        ;
    }
    /* Calculation of Brake On Time */                
    
    /* Set Condition */
    if(WL4->WPE_First_Control_VM==0)       
    {
        if(WL4->ldabss16BrakeOnTimerOld_VM==0)
        {
            if(WL4->ldabss16BrakeOnTimer_VM > 0)
            {
                WL4->WPE_First_Control_VM = 1; /* Set */
            }
            else
            {
                ; /* Hold */
            }                                 
        }
        else
        {
            ; /* Hold */
        }        
    }
    /* Reset Condition */
    else
    {
        if(WL4->ldabss16BrakeOnTimerOld_VM > 0)
        {
            if(WL4->ldabss16BrakeOnTimer_VM < U16_RollBack_Time)
            {
                WL4->WPE_First_Control_VM = 0; /* Reset */
            }
            else
            {
                ; /* Hold */ 
            }                                 
        }
        else
        {
            WL4->WPE_First_Control_VM = 0; /* Reset */
        }
    }
    
}
/* 2012_On_SWD_KJS */

/* 2012_SWD_KJS_TCMF */
void LDABS_vEstTCvvHoldPress_Volume(void)
{

    #if __TCMF_ENABLE
    
	    if(la_PtcMfc.MFC_PWM_DUTY_temp <= U8_TC_Valve_Hold_Duty_V_6)
	    {		        
            ldabss16WPETCvvHoldPress_Pri = LCESP_s16IInter6Point((int16_t)la_PtcMfc.MFC_PWM_DUTY_temp,
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_1),(int16_t)(S16_TC_Valve_Hold_Press_V_1),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_2),(int16_t)(S16_TC_Valve_Hold_Press_V_2),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_3),(int16_t)(S16_TC_Valve_Hold_Press_V_3),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_4),(int16_t)(S16_TC_Valve_Hold_Press_V_4),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_5),(int16_t)(S16_TC_Valve_Hold_Press_V_5),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_6),(int16_t)(S16_TC_Valve_Hold_Press_V_6));
        }
        else if(la_PtcMfc.MFC_PWM_DUTY_temp <= U8_TC_Valve_Hold_Duty_V_11)
        {
            ldabss16WPETCvvHoldPress_Pri = LCESP_s16IInter6Point((int16_t)la_PtcMfc.MFC_PWM_DUTY_temp,
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_6), (int16_t)(S16_TC_Valve_Hold_Press_V_6),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_7), (int16_t)(S16_TC_Valve_Hold_Press_V_7),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_8), (int16_t)(S16_TC_Valve_Hold_Press_V_8),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_9), (int16_t)(S16_TC_Valve_Hold_Press_V_9),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_10),(int16_t)(S16_TC_Valve_Hold_Press_V_10),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_11),(int16_t)(S16_TC_Valve_Hold_Press_V_11));                       
        }    
        else
        {
            ldabss16WPETCvvHoldPress_Pri = LCESP_s16IInter6Point((int16_t)la_PtcMfc.MFC_PWM_DUTY_temp,
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_11), (int16_t)(S16_TC_Valve_Hold_Press_V_11),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_12), (int16_t)(S16_TC_Valve_Hold_Press_V_12),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_13), (int16_t)(S16_TC_Valve_Hold_Press_V_13),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_14), (int16_t)(S16_TC_Valve_Hold_Press_V_14),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_15), (int16_t)(S16_TC_Valve_Hold_Press_V_15),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_16), (int16_t)(S16_TC_Valve_Hold_Press_V_16));                       
        }

		if(la_StcMfc.MFC_PWM_DUTY_temp <= U8_TC_Valve_Hold_Duty_V_6)
	    {		        
            ldabss16WPETCvvHoldPress_Scn = LCESP_s16IInter6Point((int16_t)la_StcMfc.MFC_PWM_DUTY_temp,
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_1),(int16_t)(S16_TC_Valve_Hold_Press_V_1),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_2),(int16_t)(S16_TC_Valve_Hold_Press_V_2),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_3),(int16_t)(S16_TC_Valve_Hold_Press_V_3),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_4),(int16_t)(S16_TC_Valve_Hold_Press_V_4),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_5),(int16_t)(S16_TC_Valve_Hold_Press_V_5),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_6),(int16_t)(S16_TC_Valve_Hold_Press_V_6));
        }
        else if(la_StcMfc.MFC_PWM_DUTY_temp <= U8_TC_Valve_Hold_Duty_V_11)
        {
            ldabss16WPETCvvHoldPress_Scn = LCESP_s16IInter6Point((int16_t)la_StcMfc.MFC_PWM_DUTY_temp,
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_6), (int16_t)(S16_TC_Valve_Hold_Press_V_6),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_7), (int16_t)(S16_TC_Valve_Hold_Press_V_7),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_8), (int16_t)(S16_TC_Valve_Hold_Press_V_8),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_9), (int16_t)(S16_TC_Valve_Hold_Press_V_9),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_10),(int16_t)(S16_TC_Valve_Hold_Press_V_10),
                                          (int16_t)(U8_TC_Valve_Hold_Duty_V_11),(int16_t)(S16_TC_Valve_Hold_Press_V_11));                       
        }    
        else
        {
            ldabss16WPETCvvHoldPress_Scn = LCESP_s16IInter6Point((int16_t)la_StcMfc.MFC_PWM_DUTY_temp,
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_11), (int16_t)(S16_TC_Valve_Hold_Press_V_11),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_12), (int16_t)(S16_TC_Valve_Hold_Press_V_12),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_13), (int16_t)(S16_TC_Valve_Hold_Press_V_13),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_14), (int16_t)(S16_TC_Valve_Hold_Press_V_14),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_15), (int16_t)(S16_TC_Valve_Hold_Press_V_15),
                                         (int16_t)(U8_TC_Valve_Hold_Duty_V_16), (int16_t)(S16_TC_Valve_Hold_Press_V_16));                       
        }
                
        if (ldabss16WPETCvvHoldPress_Pri > S16_WPE_Max_TCMF_Hold_Press )
        {
            ldabss16WPETCvvHoldPress_Pri = S16_WPE_Max_TCMF_Hold_Press;
        }
        else if (ldabss16WPETCvvHoldPress_Pri < S16_WPE_MPRESS_0BAR)
        {
            ldabss16WPETCvvHoldPress_Pri = S16_WPE_MPRESS_0BAR;
        }
        else 
        {
            ; 
        }
        
        if (ldabss16WPETCvvHoldPress_Scn > S16_WPE_Max_TCMF_Hold_Press )
        {
            ldabss16WPETCvvHoldPress_Scn   = S16_WPE_Max_TCMF_Hold_Press;
        }
        else if (ldabss16WPETCvvHoldPress_Scn < S16_WPE_MPRESS_0BAR)
        {
            ldabss16WPETCvvHoldPress_Scn   = S16_WPE_MPRESS_0BAR;
        }
        else
        {
            ; 
        }
        
    /* Think More KJS */        
    #else

        /* FR_Split */
        #if __SPLIT_TYPE                      
         if (TCL_DEMAND_PRIMARY==1)
         {
             ldabss16WPETCvvHoldPress_Pri = S16_WPE_Max_TCMF_Hold_Press;
         }
         else
         {
             ldabss16WPETCvvHoldPress_Pri = S16_WPE_MPRESS_0BAR;
         }
       
         if (TCL_DEMAND_SECONDARY==1)
         {
             ldabss16WPETCvvHoldPress_Scn = S16_WPE_Max_TCMF_Hold_Press;
         }
         else
         {
             ldabss16WPETCvvHoldPress_Scn = S16_WPE_MPRESS_0BAR;
         }
                 
        /* X_Split */
        #else
         if ((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))
         {
             ldabss16WPETCvvHoldPress_Pri = S16_WPE_Max_TCMF_Hold_Press;
         }
         else
         {
             ldabss16WPETCvvHoldPress_Pri = S16_WPE_MPRESS_0BAR;
         }
                  
         if ((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))
         {
             ldabss16WPETCvvHoldPress_Scn = S16_WPE_Max_TCMF_Hold_Press;
         }
         else
         {
             ldabss16WPETCvvHoldPress_Scn = S16_WPE_MPRESS_0BAR;
         }         
        #endif
    
    #endif
    /* Think More KJS */    

    ldabss16WPE_TC_Release_P_Pri = ldabss16WPETCvvHoldPress_Pri + ldabss16WPEMpress_1_100bar;
    ldabss16WPE_TC_Release_P_Scn = ldabss16WPETCvvHoldPress_Scn + ldabss16WPEMpress_1_100bar;
    
}
/* 2012_SWD_KJS_TCMF */

void LDABS_vEstResolConv_Volume(void)
{

    int16_t S16WPETemp0;

    S16WPETemp0 = 10; 
 
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0) 
    #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    ldabss16WPEMpress_1_100bar = (PRESS_M.s16premRaw - PRESS_M.s16premPressOfs_f)*S16WPETemp0;
    #else
    ldabss16WPEMpress_1_100bar = (raw_mpress - mpress_offset)*S16WPETemp0;
    #endif
#else
    ldabss16WPEMpress_1_100bar = lsesps16MpressByAHBgen3*S16WPETemp0;
#endif

    /* Min Value Limitation : 0bar */
    if (ldabss16WPEMpress_1_100bar < S16_WPE_MPRESS_0BAR)
    {
        ldabss16WPEMpress_1_100bar = S16_WPE_MPRESS_0BAR;
    }
    else
    {
        ;
    }
    /* Min Value Limitation : 0bar */
                
}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
void LDABS_vCalRawCircuitPress_AHB(void)
{
#if (__IDB_LOGIC == ENABLE)
	#ifdef __IDB_2_CIRCUIT_P_SENSOR
	ldabss16WPECircuitPress_Pri = lis16MCPRawPress;
	ldabss16WPECircuitPress_Scn = lis16MCP2RawPress;
	#else
	ldabss16WPECircuitPress_Pri = lis16MCPRawPress;
	ldabss16WPECircuitPress_Scn = lis16MCPRawPress;
	#endif
#else	
    /* Primary Circuit Pressure */
    ldabss16WPECircuitPress_Pri = fs16PosPress2ndSignal_1_100Bar; // 1_100 resol 

    /* Secondary Circuit Pressure */    
    ldabss16WPECircuitPress_Scn = fs16PosPress4thSignal_1_100Bar; // 1_100 resol
#endif
	ldabss16WPEPealSimulPress=lis16PedalSimulPressRaw;
        
}

void LDABS_vEstWheelPressMode_AHB(void)
{
/* FR Split */
#if __SPLIT_TYPE 
    LDABS_vEstWheelPressONE_AHB(&FR,&FL);
    LDABS_vEstWheelPressONE_AHB(&FL,&FR);
    LDABS_vEstWheelPressONE_AHB(&RL,&RR);
    LDABS_vEstWheelPressONE_AHB(&RR,&RL);        
#else
    LDABS_vEstWheelPressONE_AHB(&FR,&RL);
    LDABS_vEstWheelPressONE_AHB(&FL,&RR);
    LDABS_vEstWheelPressONE_AHB(&RL,&FR);
    LDABS_vEstWheelPressONE_AHB(&RR,&FL);       
#endif        
}
//WPE For IDB
void LDABS_vEstWheelPressONE_AHB(struct W_STRUCT *WL,struct W_STRUCT *WL2)
{

    int16_t S16WPETemp1;
    int16_t S16WPETemp2;
    int16_t S16WPETemp3;
    int16_t S16WPETemp4;
    int16_t S16WPETemp5;            
    
    int16_t S16WPECircuitPress;
    int16_t S16WPENOvvPWMDuty;
    int16_t S16WPELfcHoldPress;
    int16_t S16WPELimitLfcRisePress;

	uint8_t	U1WPECutVVActFlg;
	uint8_t	U1WPERelVVActFlg;
	int16_t	S16WPEPedalSimulPress;
    /* C618_MGH80_BKT_JJJ */
    int16_t S16_Caliper_Model_Volume_f_N_1;
    int16_t S16_Caliper_Model_Volume_f_N_2;
    int16_t S16_Caliper_Model_Volume_f_N_3;
    int16_t S16_Caliper_Model_Volume_f_N_4;
    int16_t S16_Caliper_Model_Volume_f_N_5;
    int16_t S16_Caliper_Model_Volume_f_N_6; 
    int16_t S16_Caliper_Model_Volume_f_N_7;
    int16_t S16_Caliper_Model_Volume_f_N_8;
    int16_t S16_Caliper_Model_Volume_f_N_9;
    int16_t S16_Caliper_Model_Volume_f_N_10;
    int16_t S16_Caliper_Model_Volume_f_N_11;
    int16_t S16_Caliper_Model_Volume_f_N_12;
    int16_t S16_Caliper_Model_Volume_f_N_13;
    int16_t S16_Caliper_Model_Volume_f_N_14;
    int16_t S16_Caliper_Model_Volume_f_N_15;
    int16_t S16_Caliper_Model_Volume_f_N_16;

    int16_t S16_Caliper_Model_Volume_r_N_1;
    int16_t S16_Caliper_Model_Volume_r_N_2;
    int16_t S16_Caliper_Model_Volume_r_N_3;
    int16_t S16_Caliper_Model_Volume_r_N_4;
    int16_t S16_Caliper_Model_Volume_r_N_5;
    int16_t S16_Caliper_Model_Volume_r_N_6; 
    int16_t S16_Caliper_Model_Volume_r_N_7;
    int16_t S16_Caliper_Model_Volume_r_N_8;
    int16_t S16_Caliper_Model_Volume_r_N_9;
    int16_t S16_Caliper_Model_Volume_r_N_10;
    int16_t S16_Caliper_Model_Volume_r_N_11;
    int16_t S16_Caliper_Model_Volume_r_N_12;
    int16_t S16_Caliper_Model_Volume_r_N_13;
    int16_t S16_Caliper_Model_Volume_r_N_14;
    int16_t S16_Caliper_Model_Volume_r_N_15;
    int16_t S16_Caliper_Model_Volume_r_N_16;
    
    S16_Caliper_Model_Volume_f_N_1  = (int16_t)(S16_Caliper_Model_Volume_f_1  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_2  = (int16_t)(S16_Caliper_Model_Volume_f_2  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_3  = (int16_t)(S16_Caliper_Model_Volume_f_3  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_4  = (int16_t)(S16_Caliper_Model_Volume_f_4  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_5  = (int16_t)(S16_Caliper_Model_Volume_f_5  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_6  = (int16_t)(S16_Caliper_Model_Volume_f_6  + S16_Caliper_Model_Volume_No_f); 
    S16_Caliper_Model_Volume_f_N_7  = (int16_t)(S16_Caliper_Model_Volume_f_7  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_8  = (int16_t)(S16_Caliper_Model_Volume_f_8  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_9  = (int16_t)(S16_Caliper_Model_Volume_f_9  + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_10 = (int16_t)(S16_Caliper_Model_Volume_f_10 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_11 = (int16_t)(S16_Caliper_Model_Volume_f_11 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_12 = (int16_t)(S16_Caliper_Model_Volume_f_12 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_13 = (int16_t)(S16_Caliper_Model_Volume_f_13 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_14 = (int16_t)(S16_Caliper_Model_Volume_f_14 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_15 = (int16_t)(S16_Caliper_Model_Volume_f_15 + S16_Caliper_Model_Volume_No_f);
    S16_Caliper_Model_Volume_f_N_16 = (int16_t)(S16_Caliper_Model_Volume_f_16 + S16_Caliper_Model_Volume_No_f);

    S16_Caliper_Model_Volume_r_N_1  = (int16_t)(S16_Caliper_Model_Volume_r_1  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_2  = (int16_t)(S16_Caliper_Model_Volume_r_2  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_3  = (int16_t)(S16_Caliper_Model_Volume_r_3  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_4  = (int16_t)(S16_Caliper_Model_Volume_r_4  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_5  = (int16_t)(S16_Caliper_Model_Volume_r_5  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_6  = (int16_t)(S16_Caliper_Model_Volume_r_6  + S16_Caliper_Model_Volume_No_r); 
    S16_Caliper_Model_Volume_r_N_7  = (int16_t)(S16_Caliper_Model_Volume_r_7  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_8  = (int16_t)(S16_Caliper_Model_Volume_r_8  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_9  = (int16_t)(S16_Caliper_Model_Volume_r_9  + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_10 = (int16_t)(S16_Caliper_Model_Volume_r_10 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_11 = (int16_t)(S16_Caliper_Model_Volume_r_11 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_12 = (int16_t)(S16_Caliper_Model_Volume_r_12 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_13 = (int16_t)(S16_Caliper_Model_Volume_r_13 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_14 = (int16_t)(S16_Caliper_Model_Volume_r_14 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_15 = (int16_t)(S16_Caliper_Model_Volume_r_15 + S16_Caliper_Model_Volume_No_r);
    S16_Caliper_Model_Volume_r_N_16 = (int16_t)(S16_Caliper_Model_Volume_r_16 + S16_Caliper_Model_Volume_No_r);    
    /* C618_MGH80_BKT_JJJ */
    
	S16WPEPedalSimulPress=ldabss16WPEPealSimulPress;
/* FR Split */
#if __SPLIT_TYPE 

    if(((WL->LEFT_WHEEL==0)&&(WL->REAR_WHEEL==0))||((WL->LEFT_WHEEL==1)&&(WL->REAR_WHEEL==0))) /* Primary : FR or FL */
	{
	    
        S16WPECircuitPress = ldabss16WPECircuitPress_Pri;

        if(WL->LEFT_WHEEL==0)
        {
            S16WPENOvvPWMDuty = la_FR1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = la_FL1HP.u8PwmDuty;            
        }
	  	if(liu1CutVV1ActFlg==1){
	  		U1WPECutVVActFlg=1;
	  	}
	  	else{
	  		U1WPECutVVActFlg=0;
	  	}
	  	
	  	if(liu1RelVV1ActFlg==1){
	  		U1WPERelVVActFlg=1;
	  	}
	  	else{
	  		U1WPERelVVActFlg=0;
	  	}
	  	  
	}
	else if(((WL->LEFT_WHEEL==1)&&(WL->REAR_WHEEL==1))||((WL->LEFT_WHEEL==0)&&(WL->REAR_WHEEL==1))) /* Secondary : RL or RR */
	{
	    
        S16WPECircuitPress = ldabss16WPECircuitPress_Scn;        

        if(WL->LEFT_WHEEL==0)
        {
            S16WPENOvvPWMDuty = la_RR1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = la_RL1HP.u8PwmDuty;            
        }
    
 	  	if(liu1CutVV2ActFlg==1){
	  		U1WPECutVVActFlg=1;
	  	}
	  	else{
	  		U1WPECutVVActFlg=0;
	  	}   
	  	if(liu1RelVV2ActFlg==1){
	  		U1WPERelVVActFlg=1;
	  	}
	  	else{
	  		U1WPERelVVActFlg=0;
	  	}
	}
	else
	{
	    S16WPECircuitPress = 0;
        S16WPENOvvPWMDuty = 0;	    
        U1WPECutVVActFlg=0;
        U1WPERelVVActFlg=0;  
	}

/* X Split */ 
#else
    
    if(((WL->LEFT_WHEEL==0)&&(WL->REAR_WHEEL==0))||((WL->LEFT_WHEEL==1)&&(WL->REAR_WHEEL==1))) /* Primary : FR or RL */
	{
	    
        S16WPECircuitPress = ldabss16WPECircuitPress_Pri;

        if(WL->REAR_WHEEL==0)
        {
            S16WPENOvvPWMDuty = la_FR1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = la_RL1HP.u8PwmDuty;            
        }
	  	  
	  	if(liu1CutVV1ActFlg==1){
	  		U1WPECutVVActFlg=1;
	  	}
	  	else{
	  		U1WPECutVVActFlg=0;
	  	}
	  	
	  	if(liu1RelVV1ActFlg==1){
	  		U1WPERelVVActFlg=1;
	  	}
	  	else{
	  		U1WPERelVVActFlg=0;
	  	}	  	  
	}
	else if(((WL->LEFT_WHEEL==1)&&(WL->REAR_WHEEL==0))||((WL->LEFT_WHEEL==0)&&(WL->REAR_WHEEL==1))) /* Secondary : FL or RR */
	{
	    
        S16WPECircuitPress = ldabss16WPECircuitPress_Scn;        

        if(WL->REAR_WHEEL==0)
        {
            S16WPENOvvPWMDuty = la_FL1HP.u8PwmDuty;
        }
        else
        {
            S16WPENOvvPWMDuty = la_RR1HP.u8PwmDuty;            
        }
    
 	  	if(liu1CutVV2ActFlg==1){
	  		U1WPECutVVActFlg=1;
	  	}
	  	else{
	  		U1WPECutVVActFlg=0;
	  	}
	  	
	  	if(liu1RelVV2ActFlg==1){
	  		U1WPERelVVActFlg=1;
	  	}
	  	else{
	  		U1WPERelVVActFlg=0;
	  	}   
	}
	else
	{
	    S16WPECircuitPress = 0;
        S16WPENOvvPWMDuty = 0;	    
        
        U1WPECutVVActFlg=0;
        U1WPERelVVActFlg=0;   
	}

#endif
    
    switch (WL->Wheel_Valve_Mode)
    {
                
        /* No_0 */
        /* NO_open && NC_close */
        /* Including Pulse_Up Rise Mode */        
        case Full_Rise_Valve_Mode:

			/*Cut Valve closed*/
			if(U1WPECutVVActFlg==1){
            	/*Release valve Open - BBS/TCS/ESC*/
            	if(U1WPERelVVActFlg==1){  
            		/*Out valve Dump -Pressure Release*/
            if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress)
            {
		            	
                if(WL->REAR_WHEEL==0)
			    {                   
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_1st),(int16_t)(S16_BBS_Release_Gain_f_1st),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_2nd),(int16_t)(S16_BBS_Release_Gain_f_2nd),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_3rd),(int16_t)(S16_BBS_Release_Gain_f_3rd),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_4th),(int16_t)(S16_BBS_Release_Gain_f_4th),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_5th),(int16_t)(S16_BBS_Release_Gain_f_5th),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_6th),(int16_t)(S16_BBS_Release_Gain_f_6th));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_1st),(int16_t)(S16_BBS_Release_Gain_r_1st),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_2nd),(int16_t)(S16_BBS_Release_Gain_r_2nd),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_3rd),(int16_t)(S16_BBS_Release_Gain_r_3rd),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_4th),(int16_t)(S16_BBS_Release_Gain_r_4th),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_5th),(int16_t)(S16_BBS_Release_Gain_r_5th),
		                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_6th),(int16_t)(S16_BBS_Release_Gain_r_6th));                        
                }                                       
               
                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPECircuitPress;
                
                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
                                
                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp3;
                
                WL->ldabss16EstWheelPressMON_VM = 1;
                
            }
            /* IN V/V Rise */
            else
            {
						if(ABS_fz==0){
                if(WL->REAR_WHEEL==0)
			    {                     
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_BBS_Rise_Press_f_1),(int16_t)(S16_BBS_Rise_Gain_f_1),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_2),(int16_t)(S16_BBS_Rise_Gain_f_2),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_3),(int16_t)(S16_BBS_Rise_Gain_f_3),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_4),(int16_t)(S16_BBS_Rise_Gain_f_4),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_5),(int16_t)(S16_BBS_Rise_Gain_f_5),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_6),(int16_t)(S16_BBS_Rise_Gain_f_6));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_BBS_Rise_Press_r_1),(int16_t)(S16_BBS_Rise_Gain_r_1),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_2),(int16_t)(S16_BBS_Rise_Gain_r_2),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_3),(int16_t)(S16_BBS_Rise_Gain_r_3),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_4),(int16_t)(S16_BBS_Rise_Gain_r_4),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_5),(int16_t)(S16_BBS_Rise_Gain_r_5),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_6),(int16_t)(S16_BBS_Rise_Gain_r_6));
			                }
			            }
			            else{
							if(WL->REAR_WHEEL==0)
						    {                     
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_ABS_Rise_Press_f_1),(int16_t)(S16_ABS_Rise_Gain_f_1),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_2),(int16_t)(S16_ABS_Rise_Gain_f_2),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_3),(int16_t)(S16_ABS_Rise_Gain_f_3),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_4),(int16_t)(S16_ABS_Rise_Gain_f_4),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_5),(int16_t)(S16_ABS_Rise_Gain_f_5),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_6),(int16_t)(S16_ABS_Rise_Gain_f_6));
			                }
			                else
			                {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_ABS_Rise_Press_r_1),(int16_t)(S16_ABS_Rise_Gain_r_1),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_2),(int16_t)(S16_ABS_Rise_Gain_r_2),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_3),(int16_t)(S16_ABS_Rise_Gain_r_3),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_4),(int16_t)(S16_ABS_Rise_Gain_r_4),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_5),(int16_t)(S16_ABS_Rise_Gain_r_5),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_6),(int16_t)(S16_ABS_Rise_Gain_r_6));
			                }			            	
                }
                                      
                S16WPETemp2 = S16WPECircuitPress - WL->ldabss16EstWheelPress_VM;
                                     
                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162); 

                /* Pulse_UP_Rise */
		                if((WL->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL->u1ContinuedFullRiseModeFlg==1)) /* 10, 9 */
                {
                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
                }
		                else /* 2, 1 */
                {
		                    S16WPETemp4 = 0; /* No Rise */                           
		                }
		                /* Pulse_UP_Rise */
		                             
		                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM + S16WPETemp4;
		                
		                WL->ldabss16EstWheelPressMON_VM = 2;                
		                
		            }		           		
                }
            	else{ /*Release valve closed*/
		            if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress) /*Pressure saturation in same circuit pressure*/
                {
		            	if((WL2->Wheel_Valve_Mode==Full_Rise_Valve_Mode)&&(WL->ldabss16EstWheelPress_VM >WL2->ldabss16EstWheelPress_VM )){/*Leak to low pressure*/
			                if(WL->REAR_WHEEL==0)
						    {                   
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_1st),(int16_t)(S16_NO_Dump_Gain_f_1st),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_2nd),(int16_t)(S16_NO_Dump_Gain_f_2nd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_3rd),(int16_t)(S16_NO_Dump_Gain_f_3rd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_4th),(int16_t)(S16_NO_Dump_Gain_f_4th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_5th),(int16_t)(S16_NO_Dump_Gain_f_5th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_6th),(int16_t)(S16_NO_Dump_Gain_f_6th));
			                }
			                else
			                {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_1st),(int16_t)(S16_NO_Dump_Gain_r_1st),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_2nd),(int16_t)(S16_NO_Dump_Gain_r_2nd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_3rd),(int16_t)(S16_NO_Dump_Gain_r_3rd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_4th),(int16_t)(S16_NO_Dump_Gain_r_4th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_5th),(int16_t)(S16_NO_Dump_Gain_r_5th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_6th),(int16_t)(S16_NO_Dump_Gain_r_6th));                        
			                }                                       
			               
			                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - WL2->ldabss16EstWheelPress_VM ;
			                
			                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
				            if((WL2->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL2->u1ContinuedFullRiseModeFlg==1)) /* 10, 9 */
			                {
			                    S16WPETemp4 = (S16WPETemp3*(WL2->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
                }
                else /* 2, 1 */
                {
                    S16WPETemp4 = 0; /* No Rise */                           
                }
			                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;
			                
			                if( WL->ldabss16EstWheelPress_VM <0) WL->ldabss16EstWheelPress_VM=0;
			                	
			                WL->ldabss16EstWheelPressMON_VM = 3;
			            }
			            else{/*Hold mode*/
			            	WL->ldabss16EstWheelPress_VM=WL->ldabss16EstWheelPress_VM;
			            	WL->ldabss16EstWheelPressMON_VM = 4;
			            }
		                
		            }
		            /* IN V/V Rise */
		            else
		            {
						if(ABS_fz == 0)
						{
							if(WL->REAR_WHEEL==0)
						    {                     
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_BBS_Rise_Press_f_1),(int16_t)(S16_BBS_Rise_Gain_f_1),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_2),(int16_t)(S16_BBS_Rise_Gain_f_2),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_3),(int16_t)(S16_BBS_Rise_Gain_f_3),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_4),(int16_t)(S16_BBS_Rise_Gain_f_4),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_5),(int16_t)(S16_BBS_Rise_Gain_f_5),
			                                                    (int16_t)(S16_BBS_Rise_Press_f_6),(int16_t)(S16_BBS_Rise_Gain_f_6));
			                }
			                else
			                {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_BBS_Rise_Press_r_1),(int16_t)(S16_BBS_Rise_Gain_r_1),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_2),(int16_t)(S16_BBS_Rise_Gain_r_2),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_3),(int16_t)(S16_BBS_Rise_Gain_r_3),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_4),(int16_t)(S16_BBS_Rise_Gain_r_4),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_5),(int16_t)(S16_BBS_Rise_Gain_r_5),
			                                                    (int16_t)(S16_BBS_Rise_Press_r_6),(int16_t)(S16_BBS_Rise_Gain_r_6));
			                }
						}
						else
						{
			                if(WL->REAR_WHEEL==0)
						    {                     
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_ABS_Rise_Press_f_1),(int16_t)(S16_ABS_Rise_Gain_f_1),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_2),(int16_t)(S16_ABS_Rise_Gain_f_2),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_3),(int16_t)(S16_ABS_Rise_Gain_f_3),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_4),(int16_t)(S16_ABS_Rise_Gain_f_4),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_5),(int16_t)(S16_ABS_Rise_Gain_f_5),
			                                                    (int16_t)(S16_ABS_Rise_Press_f_6),(int16_t)(S16_ABS_Rise_Gain_f_6));
			                }
			                else
			                {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_ABS_Rise_Press_r_1),(int16_t)(S16_ABS_Rise_Gain_r_1),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_2),(int16_t)(S16_ABS_Rise_Gain_r_2),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_3),(int16_t)(S16_ABS_Rise_Gain_r_3),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_4),(int16_t)(S16_ABS_Rise_Gain_r_4),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_5),(int16_t)(S16_ABS_Rise_Gain_r_5),
			                                                    (int16_t)(S16_ABS_Rise_Press_r_6),(int16_t)(S16_ABS_Rise_Gain_r_6));
			                }
		            	}      
		                                      
		                S16WPETemp2 = S16WPECircuitPress - WL->ldabss16EstWheelPress_VM;
		                                     
		                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162); 
		
                /* Pulse_UP_Rise */
		                if((WL->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL->u1ContinuedFullRiseModeFlg==1)) /* 10, 9 */
		                {
		                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
		                }
		                else /* 2, 1 */
		                {
		                    S16WPETemp4 = 0; /* No Rise */                           
		                }
                             
                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM + S16WPETemp4;
                
		                WL->ldabss16EstWheelPressMON_VM = 5;                
		                
		            }	
		        }		
			}
			/*Cut valve Open -- Residual Brake Rise mode*/
			else{
				if(S16WPEPedalSimulPress> WL->ldabss16EstWheelPress_VM)
				{
	                if(WL->REAR_WHEEL==0)
				    {                     
	                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_Residual_Rise_Press_f_1),(int16_t)(S16_Residual_Rise_Gain_f_1),
	                                                    (int16_t)(S16_Residual_Rise_Press_f_2),(int16_t)(S16_Residual_Rise_Gain_f_2),
	                                                    (int16_t)(S16_Residual_Rise_Press_f_3),(int16_t)(S16_Residual_Rise_Gain_f_3),
	                                                    (int16_t)(S16_Residual_Rise_Press_f_4),(int16_t)(S16_Residual_Rise_Gain_f_4),
	                                                    (int16_t)(S16_Residual_Rise_Press_f_5),(int16_t)(S16_Residual_Rise_Gain_f_5),
	                                                    (int16_t)(S16_Residual_Rise_Press_f_6),(int16_t)(S16_Residual_Rise_Gain_f_6));
	                }
	                else
	                {
	                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_Residual_Rise_Press_r_1),(int16_t)(S16_Residual_Rise_Gain_r_1),
	                                                    (int16_t)(S16_Residual_Rise_Press_r_2),(int16_t)(S16_Residual_Rise_Gain_r_2),
	                                                    (int16_t)(S16_Residual_Rise_Press_r_3),(int16_t)(S16_Residual_Rise_Gain_r_3),
	                                                    (int16_t)(S16_Residual_Rise_Press_r_4),(int16_t)(S16_Residual_Rise_Gain_r_4),
	                                                    (int16_t)(S16_Residual_Rise_Press_r_5),(int16_t)(S16_Residual_Rise_Gain_r_5),
	                                                    (int16_t)(S16_Residual_Rise_Press_r_6),(int16_t)(S16_Residual_Rise_Gain_r_6));
	                }
	                                      
	                S16WPETemp2 = S16WPEPedalSimulPress - WL->ldabss16EstWheelPress_VM;
	                
	                if(S16WPETemp2<0)S16WPETemp2=0;                     
	                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162); 
	
	               	S16WPETemp4 = S16WPETemp3;                            
	                             
	                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM + S16WPETemp4;
	                
	                WL->ldabss16EstWheelPressMON_VM = 6;      
	            }
	            else
	            {
	            	if(WL->REAR_WHEEL==0)
				    {                     
	                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_Residual_Release_Press_f_1),(int16_t)(S16_Residual_Release_Gain_f_1),
	                                                    (int16_t)(S16_Residual_Release_Press_f_2),(int16_t)(S16_Residual_Release_Gain_f_2),
	                                                    (int16_t)(S16_Residual_Release_Press_f_3),(int16_t)(S16_Residual_Release_Gain_f_3),
	                                                    (int16_t)(S16_Residual_Release_Press_f_4),(int16_t)(S16_Residual_Release_Gain_f_4),
	                                                    (int16_t)(S16_Residual_Release_Press_f_5),(int16_t)(S16_Residual_Release_Gain_f_5),
	                                                    (int16_t)(S16_Residual_Release_Press_f_6),(int16_t)(S16_Residual_Release_Gain_f_6));
	                }
	                else
	                {
	                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_Residual_Release_Press_r_1),(int16_t)(S16_Residual_Release_Gain_r_1),
	                                                    (int16_t)(S16_Residual_Release_Press_r_2),(int16_t)(S16_Residual_Release_Gain_r_2),
	                                                    (int16_t)(S16_Residual_Release_Press_r_3),(int16_t)(S16_Residual_Release_Gain_r_3),
	                                                    (int16_t)(S16_Residual_Release_Press_r_4),(int16_t)(S16_Residual_Release_Gain_r_4),
	                                                    (int16_t)(S16_Residual_Release_Press_r_5),(int16_t)(S16_Residual_Release_Gain_r_5),
	                                                    (int16_t)(S16_Residual_Release_Press_r_6),(int16_t)(S16_Residual_Release_Gain_r_6));
	                }
	                                      
	                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPEPedalSimulPress;
	                
	                if(S16WPETemp2<0)S16WPETemp2=0;                     
	                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162); 
	
	               	S16WPETemp4 = S16WPETemp3;                            
                
	                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;
	                
	                WL->ldabss16EstWheelPressMON_VM = 7; 
	            }          					
            }
                                               

                                               
        break;
        
        /* No_1 */
        /* One V/V LFC Rise */
        /* NO_open && NC_close */                
        case LFC_Rise_Valve_Mode:                             

            /** Calculate the LFC Hold Press **/           
		    if(WL->REAR_WHEEL==0)
			{
                S16WPELfcHoldPress = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
                                		           (int16_t)(U8_LFC_Hold_Duty_f_1st),(int16_t)(S16_LFC_Hold_press_f_1st),
                                		           (int16_t)(U8_LFC_Hold_Duty_f_2nd),(int16_t)(S16_LFC_Hold_press_f_2nd),
                                		           (int16_t)(U8_LFC_Hold_Duty_f_3rd),(int16_t)(S16_LFC_Hold_press_f_3rd),
                                		           (int16_t)(U8_LFC_Hold_Duty_f_4th),(int16_t)(S16_LFC_Hold_press_f_4th),
                                		           (int16_t)(U8_LFC_Hold_Duty_f_5th),(int16_t)(S16_LFC_Hold_press_f_5th),
                                		           (int16_t)(U8_LFC_Hold_Duty_f_6th),(int16_t)(S16_LFC_Hold_press_f_6th));
		    }
			else
			{
			    S16WPELfcHoldPress = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
                                		           (int16_t)(U8_LFC_Hold_Duty_r_1st),(int16_t)(S16_LFC_Hold_press_r_1st),
                                		           (int16_t)(U8_LFC_Hold_Duty_r_2nd),(int16_t)(S16_LFC_Hold_press_r_2nd),
                                		           (int16_t)(U8_LFC_Hold_Duty_r_3rd),(int16_t)(S16_LFC_Hold_press_r_3rd),
                                		           (int16_t)(U8_LFC_Hold_Duty_r_4th),(int16_t)(S16_LFC_Hold_press_r_4th),
                                		           (int16_t)(U8_LFC_Hold_Duty_r_5th),(int16_t)(S16_LFC_Hold_press_r_5th),
                                		           (int16_t)(U8_LFC_Hold_Duty_r_6th),(int16_t)(S16_LFC_Hold_press_r_6th));								
			}
            
            /* Delta P : Rise Limitation of Wheel_P */
            /* Think More for ESC */            
    		S16WPELimitLfcRisePress = (S16WPECircuitPress/10) - S16WPELfcHoldPress; /* 1_10bar */
            /* Think More for ESC */
											
    		if(S16WPELimitLfcRisePress > MPRESS_0BAR) /* 1_10bar */
    		{
    		    ;
    		}
    		else
    		{
    		    S16WPELimitLfcRisePress = 0;
    		}                						
            /* Delta P : Rise Limitation of Wheel_P */						
			/** Calculate the LFC Hold Press **/

			/* LFC Hold or TC Dump */			
			if(WL->ldabss16EstWheelPress_VM > S16WPELimitLfcRisePress*10)  /* 1_100bar */
			{
                /* OUT V/V Dump */
                if(U1WPERelVVActFlg==1){
                if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress)
                {
                    if(WL->REAR_WHEEL==0)
			        {                   
                        S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
                    }
                    else
                    {
                        S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
                                                        (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
                    }                                       
                    
                    S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPECircuitPress;
                    
                    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
                    
                    WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp3;
                    
                    WL->ldabss16EstWheelPressMON_VM = 12;
                    
                }
                /* LFC Hold */
                else
                {
                    WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM;

                    WL->ldabss16EstWheelPressMON_VM = 13;                    
                }
	            }
	            else
			    {
	                    WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM;
			    			    
	                    WL->ldabss16EstWheelPressMON_VM = 14;     			    	
			    }			    
			}
            /* IN V/V LFC Rise */			
			else
			{
			    if(WL->REAR_WHEEL==0)
			    {
			    		    
			        S16WPETemp1 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
                                		        (int16_t)(U8_LFC_Rise_Duty_f_1st),(int16_t)(S16_LFC_Rise_Gain_f_1st),
                                		        (int16_t)(U8_LFC_Rise_Duty_f_2nd),(int16_t)(S16_LFC_Rise_Gain_f_2nd),
                                		        (int16_t)(U8_LFC_Rise_Duty_f_3rd),(int16_t)(S16_LFC_Rise_Gain_f_3rd),
                                		        (int16_t)(U8_LFC_Rise_Duty_f_4th),(int16_t)(S16_LFC_Rise_Gain_f_4th),
                                		        (int16_t)(U8_LFC_Rise_Duty_f_5th),(int16_t)(S16_LFC_Rise_Gain_f_5th),
                                		        (int16_t)(U8_LFC_Rise_Duty_f_6th),(int16_t)(S16_LFC_Rise_Gain_f_6th));
                                		               
                }
                else
                {
			    		    
			    	S16WPETemp1 = LCESP_s16IInter6Point((int16_t)S16WPENOvvPWMDuty,
                                		        (int16_t)(U8_LFC_Rise_Duty_r_1st),(int16_t)(S16_LFC_Rise_Gain_r_1st),
                                		        (int16_t)(U8_LFC_Rise_Duty_r_2nd),(int16_t)(S16_LFC_Rise_Gain_r_2nd),
                                		        (int16_t)(U8_LFC_Rise_Duty_r_3rd),(int16_t)(S16_LFC_Rise_Gain_r_3rd),
                                		        (int16_t)(U8_LFC_Rise_Duty_r_4th),(int16_t)(S16_LFC_Rise_Gain_r_4th),
                                		        (int16_t)(U8_LFC_Rise_Duty_r_5th),(int16_t)(S16_LFC_Rise_Gain_r_5th),
                                		        (int16_t)(U8_LFC_Rise_Duty_r_6th),(int16_t)(S16_LFC_Rise_Gain_r_6th));
                                       
                }
                
			    S16WPETemp2 = S16WPELimitLfcRisePress*10 - WL->ldabss16EstWheelPress_VM;
			    		    
			    S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
                
                /* Pulse_UP_Rise */
                if(WL->pwm_lfc_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_1st) /* 10, 9 */
                {
                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_lfc_rise_cnt))/One_Scan_Cycle_Time_MS;                            
                }
                else if(WL->pwm_lfc_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_2nd) /* 8, 7, 6 */
                {
                    S16WPETemp4 = (S16WPETemp3*((WL->pwm_lfc_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_1st))/One_Scan_Cycle_Time_MS;                            
                }
                else if(WL->pwm_lfc_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd) /* 5, 4, 3 */
                {
                    S16WPETemp4 = (S16WPETemp3*((WL->pwm_lfc_rise_cnt)-U8_WPE_ESC_Rise_Hold_Comp_2nd))/One_Scan_Cycle_Time_MS;                            
                }
                else /* 2, 1 */
                {
                    S16WPETemp4 = 0; /* No Rise */                           
                }
                /* Pulse_UP_Rise */
			    		    
			    WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM + S16WPETemp4;
                
			    WL->ldabss16EstWheelPressMON_VM = 11;
			}

        break;
        
        /* No_2 */
        /* One V/V HOLD */
        /* NO_close && NC_close */                
        case Hold_Valve_Mode:
        	/*Release vv open*/
            if(U1WPERelVVActFlg==1){
            	if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress){
                if(WL->REAR_WHEEL==0)
			    {                   
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_1st),(int16_t)(S16_BBS_Release_Gain_f_1st),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_2nd),(int16_t)(S16_BBS_Release_Gain_f_2nd),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_3rd),(int16_t)(S16_BBS_Release_Gain_f_3rd),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_4th),(int16_t)(S16_BBS_Release_Gain_f_4th),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_5th),(int16_t)(S16_BBS_Release_Gain_f_5th),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_f_6th),(int16_t)(S16_BBS_Release_Gain_f_6th));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_1st),(int16_t)(S16_BBS_Release_Gain_r_1st),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_2nd),(int16_t)(S16_BBS_Release_Gain_r_2nd),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_3rd),(int16_t)(S16_BBS_Release_Gain_r_3rd),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_4th),(int16_t)(S16_BBS_Release_Gain_r_4th),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_5th),(int16_t)(S16_BBS_Release_Gain_r_5th),
	                                                    (int16_t)(S16_BBS_Release_Gain_Press_r_6th),(int16_t)(S16_BBS_Release_Gain_r_6th));                        
                }                                       
                
                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPECircuitPress;
                
                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
	                S16WPETemp4=S16WPETemp3;
                
	                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;      
	                if(WL->ldabss16EstWheelPress_VM<0)
	                {
			    		WL->ldabss16EstWheelPress_VM=0;
	                }                                             
                WL->ldabss16EstWheelPressMON_VM = 21;
            }
            	else{
            /* Hold */
	                WL->ldabss16EstWheelPress_VM   = WL->ldabss16EstWheelPress_VM;
	                WL->ldabss16EstWheelPressMON_VM = 22;
            	}
            }
            else{/*Release vv closed*/
            	if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress){
            		if(U1WPECutVVActFlg==1){
            			/*Cut vv closed */
		            	if((WL2->Wheel_Valve_Mode==Full_Rise_Valve_Mode)&&(WL->ldabss16EstWheelPress_VM >WL2->ldabss16EstWheelPress_VM )){/*Leak to low pressure*/
			                if(WL->REAR_WHEEL==0)
						    {                   
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_1st),(int16_t)(S16_NO_Dump_Gain_f_1st),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_2nd),(int16_t)(S16_NO_Dump_Gain_f_2nd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_3rd),(int16_t)(S16_NO_Dump_Gain_f_3rd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_4th),(int16_t)(S16_NO_Dump_Gain_f_4th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_5th),(int16_t)(S16_NO_Dump_Gain_f_5th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_6th),(int16_t)(S16_NO_Dump_Gain_f_6th));
			                }
            else
            {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_1st),(int16_t)(S16_NO_Dump_Gain_r_1st),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_2nd),(int16_t)(S16_NO_Dump_Gain_r_2nd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_3rd),(int16_t)(S16_NO_Dump_Gain_r_3rd),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_4th),(int16_t)(S16_NO_Dump_Gain_r_4th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_5th),(int16_t)(S16_NO_Dump_Gain_r_5th),
			                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_6th),(int16_t)(S16_NO_Dump_Gain_r_6th));                        
			                }                                       
			               
			                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - WL2->ldabss16EstWheelPress_VM ;
			                
			                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
				            if((WL2->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL2->u1ContinuedFullRiseModeFlg==1)) /* 10, 9 */
			                {
			                    S16WPETemp4 = (S16WPETemp3*(WL2->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
			                }
			                else /* 2, 1 */
			                {
			                    S16WPETemp4 = 0; /* No Rise */                           
			                }	                                
			                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;
			                if(WL->ldabss16EstWheelPress_VM <0) WL->ldabss16EstWheelPress_VM=0;
			                WL->ldabss16EstWheelPressMON_VM = 23;
			            }
			            else{/*Hold mode*/
			            	WL->ldabss16EstWheelPress_VM=WL->ldabss16EstWheelPress_VM;
			            	WL->ldabss16EstWheelPressMON_VM = 24;
			            }            			
            		}
            		else{
            			/*Cut VV open*/
						if(S16WPEPedalSimulPress> WL->ldabss16EstWheelPress_VM)
						{
							WL->ldabss16EstWheelPress_VM   = WL->ldabss16EstWheelPress_VM;
			                WL->ldabss16EstWheelPressMON_VM = 25;      
			            }
			            else
			            {
			            	if(WL->REAR_WHEEL==0)
						    {                     
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_Residual_Release_Press_f_1),(int16_t)(S16_Residual_Release_Gain_f_1),
			                                                    (int16_t)(S16_Residual_Release_Press_f_2),(int16_t)(S16_Residual_Release_Gain_f_2),
			                                                    (int16_t)(S16_Residual_Release_Press_f_3),(int16_t)(S16_Residual_Release_Gain_f_3),
			                                                    (int16_t)(S16_Residual_Release_Press_f_4),(int16_t)(S16_Residual_Release_Gain_f_4),
			                                                    (int16_t)(S16_Residual_Release_Press_f_5),(int16_t)(S16_Residual_Release_Gain_f_5),
			                                                    (int16_t)(S16_Residual_Release_Press_f_6),(int16_t)(S16_Residual_Release_Gain_f_6));
			                }
			                else
			                {
			                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
			                                                    (int16_t)(S16_Residual_Release_Press_r_1),(int16_t)(S16_Residual_Release_Gain_r_1),
			                                                    (int16_t)(S16_Residual_Release_Press_r_2),(int16_t)(S16_Residual_Release_Gain_r_2),
			                                                    (int16_t)(S16_Residual_Release_Press_r_3),(int16_t)(S16_Residual_Release_Gain_r_3),
			                                                    (int16_t)(S16_Residual_Release_Press_r_4),(int16_t)(S16_Residual_Release_Gain_r_4),
			                                                    (int16_t)(S16_Residual_Release_Press_r_5),(int16_t)(S16_Residual_Release_Gain_r_5),
			                                                    (int16_t)(S16_Residual_Release_Press_r_6),(int16_t)(S16_Residual_Release_Gain_r_6));
            }        

			                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPEPedalSimulPress;
			                
			                if(S16WPETemp2<0)S16WPETemp2=0;                     
			                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162); 
			
			               	S16WPETemp4 = S16WPETemp3;                            
			                             
			                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;
			                
			                WL->ldabss16EstWheelPressMON_VM = 27; 
			            }        
            		}
            	}
            	else{
            		/*Hold*/
	                WL->ldabss16EstWheelPress_VM   = WL->ldabss16EstWheelPress_VM;
	                WL->ldabss16EstWheelPressMON_VM = 26;            		
            	}
            }
        break;

        /* No_3 */
        /* One V/V Dump */
        /* NO_close && NC_open */        
        case Dump_Valve_Mode:
        	/*Cut VV Close*/
        	if(U1WPECutVVActFlg==1){
                if(WL->REAR_WHEEL==0)
			    {                   
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_1st),(int16_t)(S16_NC_Dump_Gain_f_1st),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_2nd),(int16_t)(S16_NC_Dump_Gain_f_2nd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_3rd),(int16_t)(S16_NC_Dump_Gain_f_3rd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_4th),(int16_t)(S16_NC_Dump_Gain_f_4th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_5th),(int16_t)(S16_NC_Dump_Gain_f_5th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_6th),(int16_t)(S16_NC_Dump_Gain_f_6th));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_1st),(int16_t)(S16_NC_Dump_Gain_r_1st),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_2nd),(int16_t)(S16_NC_Dump_Gain_r_2nd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_3rd),(int16_t)(S16_NC_Dump_Gain_r_3rd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_4th),(int16_t)(S16_NC_Dump_Gain_r_4th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_5th),(int16_t)(S16_NC_Dump_Gain_r_5th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_6th),(int16_t)(S16_NC_Dump_Gain_r_6th));                        
                }            

	            S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16_WPE_LPA_Pressure;
	            
				S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);                        
	
                if((WL->pwm_dump_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL->u1ContinuedDumpModeFlg==1)) /* 10, 9 */
                {
                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_dump_cnt))/One_Scan_Cycle_Time_MS;                            
                }
                else /* 2, 1 */
                {
                    S16WPETemp4 = 0; /* No Rise */                           
                }
	            WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;    
	           	if(WL->ldabss16EstWheelPress_VM <0) WL->ldabss16EstWheelPress_VM=0;
                WL->ldabss16EstWheelPressMON_VM = 31;

            	/*Additional pressure drop through other path*/	
    			if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress){

	                if(U1WPERelVVActFlg==1){
		                if(WL->REAR_WHEEL==0)
					    {                   
		                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_1st),(int16_t)(S16_BBS_Release_Gain_f_1st),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_2nd),(int16_t)(S16_BBS_Release_Gain_f_2nd),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_3rd),(int16_t)(S16_BBS_Release_Gain_f_3rd),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_4th),(int16_t)(S16_BBS_Release_Gain_f_4th),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_5th),(int16_t)(S16_BBS_Release_Gain_f_5th),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_f_6th),(int16_t)(S16_BBS_Release_Gain_f_6th));
            }
            else
            {
		                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_1st),(int16_t)(S16_BBS_Release_Gain_r_1st),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_2nd),(int16_t)(S16_BBS_Release_Gain_r_2nd),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_3rd),(int16_t)(S16_BBS_Release_Gain_r_3rd),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_4th),(int16_t)(S16_BBS_Release_Gain_r_4th),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_5th),(int16_t)(S16_BBS_Release_Gain_r_5th),
		                                                        (int16_t)(S16_BBS_Release_Gain_Press_r_6th),(int16_t)(S16_BBS_Release_Gain_r_6th));                        
		                }            
		
		                   				
	
			            S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPECircuitPress;
			            
						S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);                        
					    S16WPETemp5=S16WPETemp3;
					                
			            WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM -S16WPETemp5;
			            if(WL->ldabss16EstWheelPress_VM<0) WL->ldabss16EstWheelPress_VM=0;
			            WL->ldabss16EstWheelPressMON_VM = 32; 
			        }  
			        else if((WL->ldabss16EstWheelPress_VM>WL2->ldabss16EstWheelPress_VM)&&(WL2->Wheel_Valve_Mode==Full_Rise_Valve_Mode)){
		                if(WL->REAR_WHEEL==0)
					    {                   
		                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_1st),(int16_t)(S16_NO_Dump_Gain_f_1st),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_2nd),(int16_t)(S16_NO_Dump_Gain_f_2nd),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_3rd),(int16_t)(S16_NO_Dump_Gain_f_3rd),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_4th),(int16_t)(S16_NO_Dump_Gain_f_4th),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_5th),(int16_t)(S16_NO_Dump_Gain_f_5th),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_f_6th),(int16_t)(S16_NO_Dump_Gain_f_6th));
		                }
		                else
		                {
		                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_1st),(int16_t)(S16_NO_Dump_Gain_r_1st),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_2nd),(int16_t)(S16_NO_Dump_Gain_r_2nd),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_3rd),(int16_t)(S16_NO_Dump_Gain_r_3rd),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_4th),(int16_t)(S16_NO_Dump_Gain_r_4th),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_5th),(int16_t)(S16_NO_Dump_Gain_r_5th),
		                                                    (int16_t)(S16_NO_Dump_Gain_Press_r_6th),(int16_t)(S16_NO_Dump_Gain_r_6th));                        
		                }                                       
		               
		                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - WL2->ldabss16EstWheelPress_VM ;
		                
		                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
			            if((WL2->pwm_full_rise_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL2->u1ContinuedFullRiseModeFlg==1)) /* 10, 9 */
		                {
		                    S16WPETemp5 = (S16WPETemp3*(WL2->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;                            
		                }
		                else /* 2, 1 */
		                {
		                    S16WPETemp5 = 0; /* No Rise */                           
		                }	                                
		                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp5;
		                
		                if(WL->ldabss16EstWheelPress_VM<0)WL->ldabss16EstWheelPress_VM=0;
		                WL->ldabss16EstWheelPressMON_VM = 33;			        	
			        }
			        else{
			        	;
			        }
    			}        			
        		else{
        			;
        			
        		}
        	}
        	/*Cut VV Open*/
        	else{
                if(WL->REAR_WHEEL==0)
			    {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_1st),(int16_t)(S16_NC_Dump_Gain_f_1st),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_2nd),(int16_t)(S16_NC_Dump_Gain_f_2nd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_3rd),(int16_t)(S16_NC_Dump_Gain_f_3rd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_4th),(int16_t)(S16_NC_Dump_Gain_f_4th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_5th),(int16_t)(S16_NC_Dump_Gain_f_5th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_f_6th),(int16_t)(S16_NC_Dump_Gain_f_6th));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_1st),(int16_t)(S16_NC_Dump_Gain_r_1st),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_2nd),(int16_t)(S16_NC_Dump_Gain_r_2nd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_3rd),(int16_t)(S16_NC_Dump_Gain_r_3rd),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_4th),(int16_t)(S16_NC_Dump_Gain_r_4th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_5th),(int16_t)(S16_NC_Dump_Gain_r_5th),
                                                        (int16_t)(S16_NC_Dump_Gain_Press_r_6th),(int16_t)(S16_NC_Dump_Gain_r_6th));                        
                }

	            S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16_WPE_LPA_Pressure;

				S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);                        

                if((WL->pwm_dump_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd)||(WL->u1ContinuedDumpModeFlg==1)) /* 10, 9 */
                {
                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_dump_cnt))/One_Scan_Cycle_Time_MS;                            
                }
                else /* 2, 1 */
                {
                    S16WPETemp4 = 0; /* No Rise */                           
                }
	            WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;    
	           	if(WL->ldabss16EstWheelPress_VM <0) WL->ldabss16EstWheelPress_VM=0;
	           	WL->ldabss16EstWheelPressMON_VM = 34;	
            }


        break;

        /* No_4 */               
        /* NO_open && NC_open */
        case Circulation_Valve_Mode:

            /* OUT V/V Dump */
            if(WL->ldabss16EstWheelPress_VM > S16WPECircuitPress)
            {
                if(WL->REAR_WHEEL==0)
			    {                   
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_1st),(int16_t)(S16_TC_Dump_Gain_f_1st),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_2nd),(int16_t)(S16_TC_Dump_Gain_f_2nd),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_3rd),(int16_t)(S16_TC_Dump_Gain_f_3rd),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_4th),(int16_t)(S16_TC_Dump_Gain_f_4th),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_5th),(int16_t)(S16_TC_Dump_Gain_f_5th),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_f_6th),(int16_t)(S16_TC_Dump_Gain_f_6th));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_1st),(int16_t)(S16_TC_Dump_Gain_r_1st),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_2nd),(int16_t)(S16_TC_Dump_Gain_r_2nd),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_3rd),(int16_t)(S16_TC_Dump_Gain_r_3rd),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_4th),(int16_t)(S16_TC_Dump_Gain_r_4th),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_5th),(int16_t)(S16_TC_Dump_Gain_r_5th),
                                                    (int16_t)(S16_TC_Dump_Gain_Press_r_6th),(int16_t)(S16_TC_Dump_Gain_r_6th));                        
                }                                       
                
                S16WPETemp2 = WL->ldabss16EstWheelPress_VM - S16WPECircuitPress;
                
                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);
                S16WPETemp4=S16WPETemp3;
                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM - S16WPETemp4;
                
                WL->ldabss16EstWheelPressMON_VM = 41;
                
            }
            /* IN V/V Circulation Rise */
            else
            {

                if(WL->REAR_WHEEL==0)
			    {                     
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                    (int16_t)(S16_MP_Rise_Press_f_1_volume),(int16_t)(S16_MP_Rise_Gain_f_1_volume),
                                                    (int16_t)(S16_MP_Rise_Press_f_2_volume),(int16_t)(S16_MP_Rise_Gain_f_2_volume),
                                                    (int16_t)(S16_MP_Rise_Press_f_3_volume),(int16_t)(S16_MP_Rise_Gain_f_3_volume),
                                                    (int16_t)(S16_MP_Rise_Press_f_4_volume),(int16_t)(S16_MP_Rise_Gain_f_4_volume),
                                                    (int16_t)(S16_MP_Rise_Press_f_5_volume),(int16_t)(S16_MP_Rise_Gain_f_5_volume),
                                                    (int16_t)(S16_MP_Rise_Press_f_6_volume),(int16_t)(S16_MP_Rise_Gain_f_6_volume));
                }
                else
                {
                    S16WPETemp1 = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM/10,
                                                    (int16_t)(S16_MP_Rise_Press_r_1_volume),(int16_t)(S16_MP_Rise_Gain_r_1_volume),
                                                    (int16_t)(S16_MP_Rise_Press_r_2_volume),(int16_t)(S16_MP_Rise_Gain_r_2_volume),
                                                    (int16_t)(S16_MP_Rise_Press_r_3_volume),(int16_t)(S16_MP_Rise_Gain_r_3_volume),
                                                    (int16_t)(S16_MP_Rise_Press_r_4_volume),(int16_t)(S16_MP_Rise_Gain_r_4_volume),
                                                    (int16_t)(S16_MP_Rise_Press_r_5_volume),(int16_t)(S16_MP_Rise_Gain_r_5_volume),
                                                    (int16_t)(S16_MP_Rise_Press_r_6_volume),(int16_t)(S16_MP_Rise_Gain_r_6_volume));
                }
                                      
                S16WPETemp2 = S16WPECircuitPress - WL->ldabss16EstWheelPress_VM;
                                     
                S16WPETemp3 = (int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)S16WPETemp2))))*(S16WPETemp1*100))/3162);

                /* Pulse_UP_Rise */
                if(WL->pwm_circulation_cnt > U8_WPE_ESC_Rise_Hold_Thres_1st) /* 10, 9 */
                {
                    S16WPETemp4 = (S16WPETemp3*(WL->pwm_circulation_cnt))/One_Scan_Cycle_Time_MS;                            
                }
                else if(WL->pwm_circulation_cnt > U8_WPE_ESC_Rise_Hold_Thres_2nd) /* 8, 7, 6 */
                {
                    S16WPETemp4 = (S16WPETemp3*((WL->pwm_circulation_cnt)-U8_WPE_ESC_Rise_Hold_Comp_1st))/One_Scan_Cycle_Time_MS;                            
                }
                else if(WL->pwm_circulation_cnt > U8_WPE_ESC_Rise_Hold_Thres_3rd) /* 5, 4, 3 */
                {
                    S16WPETemp4 = (S16WPETemp3*((WL->pwm_circulation_cnt)-U8_WPE_ESC_Rise_Hold_Comp_2nd))/One_Scan_Cycle_Time_MS;                            
                }
                else /* 2, 1 */
                {
                    S16WPETemp4 = 0; /* No Rise */                           
                }
                /* Pulse_UP_Rise */

                S16WPETemp5 = S16WPETemp4*S16CirculationComp/10;
                             
                WL->ldabss16EstWheelPress_VM = WL->ldabss16EstWheelPress_VM + S16WPETemp5;
                
                WL->ldabss16EstWheelPressMON_VM = 42;                
                
            }            

        break;
        
        /* No_5 */        
        default:
            
            WL->ldabss16EstWheelPress_VM   = WL->ldabss16EstWheelPress_VM;           
            WL->ldabss16EstWheelPressMON_VM = 50;
        
        break;    
                
    }    

    /* Calculate the Volume using Pressure */
    /* 1st Brake */
    if(WL->WPE_First_Control_VM==1)                    
    {                
        if(WL->REAR_WHEEL==0)
	     {
	         if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_N_6));
            }
            else if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11));                        
            }
            else
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_N_16));                        
            }    
        }
        else
        {
	         if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_N_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_N_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_N_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_N_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_N_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_N_6));
            }
            else if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_N_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_N_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_N_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_N_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_N_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11));                        
            }
            else
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_N_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_N_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_N_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_N_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_N_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_N_16));                        
            }                                                
        }
    }
    /* Nth Brake */                
    else
    {
        if(WL->REAR_WHEEL==0)
	     {
	         if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_f_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_f_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_f_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_f_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_f_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_f_6));
            }
            else if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_f_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_f_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_f_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_f_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_f_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11));                        
            }
            else
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_f_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_f_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_f_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_f_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_f_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_f_16));                        
            }    
        }
        else
        {
	         if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_6)		                   
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_1),(int16_t)(S16_Caliper_Model_Volume_r_1),
                                                            (int16_t)(S16_Caliper_Model_Press_2),(int16_t)(S16_Caliper_Model_Volume_r_2),
                                                            (int16_t)(S16_Caliper_Model_Press_3),(int16_t)(S16_Caliper_Model_Volume_r_3),
                                                            (int16_t)(S16_Caliper_Model_Press_4),(int16_t)(S16_Caliper_Model_Volume_r_4),
                                                            (int16_t)(S16_Caliper_Model_Press_5),(int16_t)(S16_Caliper_Model_Volume_r_5),
                                                            (int16_t)(S16_Caliper_Model_Press_6),(int16_t)(S16_Caliper_Model_Volume_r_6));
            }
            else if(WL->ldabss16EstWheelPress_VM <= S16_Caliper_Model_Press_11)
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                            (int16_t)(S16_Caliper_Model_Press_6), (int16_t)(S16_Caliper_Model_Volume_r_6),
                                                            (int16_t)(S16_Caliper_Model_Press_7), (int16_t)(S16_Caliper_Model_Volume_r_7),
                                                            (int16_t)(S16_Caliper_Model_Press_8), (int16_t)(S16_Caliper_Model_Volume_r_8),
                                                            (int16_t)(S16_Caliper_Model_Press_9), (int16_t)(S16_Caliper_Model_Volume_r_9),
                                                            (int16_t)(S16_Caliper_Model_Press_10),(int16_t)(S16_Caliper_Model_Volume_r_10),
                                                            (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11));                        
            }
            else
            {
                WL->ldabss16EstWheelVolume_VM = LCESP_s16IInter6Point(WL->ldabss16EstWheelPress_VM,
                                                           (int16_t)(S16_Caliper_Model_Press_11),(int16_t)(S16_Caliper_Model_Volume_r_11),
                                                           (int16_t)(S16_Caliper_Model_Press_12),(int16_t)(S16_Caliper_Model_Volume_r_12),
                                                           (int16_t)(S16_Caliper_Model_Press_13),(int16_t)(S16_Caliper_Model_Volume_r_13),
                                                           (int16_t)(S16_Caliper_Model_Press_14),(int16_t)(S16_Caliper_Model_Volume_r_14),
                                                           (int16_t)(S16_Caliper_Model_Press_15),(int16_t)(S16_Caliper_Model_Volume_r_15),
                                                           (int16_t)(S16_Caliper_Model_Press_16),(int16_t)(S16_Caliper_Model_Volume_r_16));                        
            }                                                
        }                    
    }
    /* Calculate the Volume using Pressure */
        
}
#endif

#endif
/* 2012_SWD_KJS */

void LDABS_vCheckWheelValveMode(struct W_STRUCT *WL, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC)
{
  #if __VDC
    uint8_t pwm_i;
	int16_t CNT_MAX=100;
    //wpe_tempW9 = (int16_t)la_WHEEL_NC;


    WL->pwm_full_rise_cnt   =0;
    WL->pwm_lfc_rise_cnt    =0;
    WL->pwm_hold_cnt        =0;
    WL->pwm_dump_cnt        =0;
    WL->pwm_circulation_cnt =0;

	WL->u1ContinuedFullRiseModeFlg=0;
    WL->u1ContinuedLfcRiseModeFlg=0;
    WL->u1ContinuedHoldModeFlg=0;
    WL->u1ContinuedDumpModeFlg=0;
    WL->u1ContinuedCirculationModeFlg=0;    
    for(pwm_i=0;pwm_i<(One_Scan_Cycle_Time_MS);pwm_i++) /* 7 -> 5 : kjs_swd11 */
    {
        if ((pNO->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY)
            &&( pNC->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY))
        {
            WL->pwm_full_rise_cnt++;
            if((pwm_i==0)&&(WL->pwm_full_rise_cnt2>0)){
            	WL->u1ContinuedFullRiseModeFlg=1;
            }
            
            WL->pwm_full_rise_cnt2++;
		    WL->pwm_lfc_rise_cnt2    =0;
		    WL->pwm_hold_cnt2        =0;
		    WL->pwm_dump_cnt2        =0;
		    WL->pwm_circulation_cnt2 =0;            
            
        }
        else if ((pNO->lau16PwmDuty[pwm_i] > VALVE_OFF_DUTY)&&(pNO->lau16PwmDuty[pwm_i] < NO_FULL_ON_DUTY)
            &&( pNC->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY))
        {
            WL->pwm_lfc_rise_cnt++;
            if((pwm_i==0)&&(WL->pwm_lfc_rise_cnt2>0)){
            	WL->u1ContinuedLfcRiseModeFlg=1;
            }
                        
            WL->pwm_full_rise_cnt2	 =0;
		    WL->pwm_lfc_rise_cnt2++;
		    WL->pwm_hold_cnt2        =0;
		    WL->pwm_dump_cnt2        =0;
		    WL->pwm_circulation_cnt2 =0;                   
            
            if(WL->pwm_lfc_rise_cnt2>CNT_MAX){
            	WL->pwm_lfc_rise_cnt2=CNT_MAX;
            }
            else{
            	;
            }

        }
        else if ((pNO->lau16PwmDuty[pwm_i] == NO_FULL_ON_DUTY)
            &&( pNC->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY))
        {
            WL->pwm_hold_cnt++;

            if((pwm_i==0)&&(WL->pwm_hold_cnt2>0)){
            	WL->u1ContinuedHoldModeFlg=1;
            }
                        
            WL->pwm_full_rise_cnt2	 =0;            
		    WL->pwm_lfc_rise_cnt2=0;
		    WL->pwm_hold_cnt2++;
		    WL->pwm_dump_cnt2        =0;
		    WL->pwm_circulation_cnt2 =0;
		    
            if(WL->pwm_hold_cnt2>CNT_MAX){
            	WL->pwm_hold_cnt2=CNT_MAX;
            }
            else{
            	;
            }
        }
        else if ((pNO->lau16PwmDuty[pwm_i] == NO_FULL_ON_DUTY)
            &&( pNC->lau16PwmDuty[pwm_i] == NC_FULL_ON_DUTY))
        {
            WL->pwm_dump_cnt++;
            if((pwm_i==0)&&(WL->pwm_dump_cnt2>0)){
            	WL->u1ContinuedDumpModeFlg=1;
            }
                        
            WL->pwm_full_rise_cnt2	 =0;            
		    WL->pwm_lfc_rise_cnt2	 =0;
		    WL->pwm_hold_cnt2		 =0;
		    WL->pwm_dump_cnt2++;
		    WL->pwm_circulation_cnt2 =0; 

            if(WL->pwm_dump_cnt2>CNT_MAX){
            	WL->pwm_dump_cnt2=CNT_MAX;
            }
            else{
            	;
            }	    
        }
        else if ((pNO->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY)
            &&( pNC->lau16PwmDuty[pwm_i] == NC_FULL_ON_DUTY))
        {
            WL->pwm_circulation_cnt++;
            if((pwm_i==0)&&(WL->pwm_circulation_cnt2>0)){
            	WL->u1ContinuedCirculationModeFlg=1;
            }
            WL->pwm_full_rise_cnt2	 =0;            
		    WL->pwm_lfc_rise_cnt2	 =0;
		    WL->pwm_hold_cnt2		 =0;
		    WL->pwm_dump_cnt2		 =0;
		    WL->pwm_circulation_cnt2++;  
        
            if(WL->pwm_circulation_cnt2>CNT_MAX){
            	WL->pwm_circulation_cnt2=CNT_MAX;
            }
            else{
            	;
            }
        }
        else{}
/*
        if (pwm_i==0)
        {
            lau8BitMaskCnt=1;
        }
        else
        {
            lau8BitMaskCnt=lau8BitMaskCnt*2;
        }

        if ((pNO->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY)
            &&( (wpe_tempW9&lau8BitMaskCnt) == 0))
        {
            WL->pwm_full_rise_cnt++;
        }
        else if ((pNO->lau16PwmDuty[pwm_i] > VALVE_OFF_DUTY)&&(pNO->lau16PwmDuty[pwm_i] < 200)
            &&( (wpe_tempW9&lau8BitMaskCnt) == 0))
        {
            WL->pwm_lfc_rise_cnt++;
        }
        else if ((pNO->lau16PwmDuty[pwm_i] == NO_FULL_ON_DUTY)
            &&( (wpe_tempW9&lau8BitMaskCnt) == 0))
        {
            WL->pwm_hold_cnt++;
        }
        else if ((pNO->lau16PwmDuty[pwm_i] == NO_FULL_ON_DUTY)
            &&( (wpe_tempW9&lau8BitMaskCnt) == lau8BitMaskCnt))
        {
            WL->pwm_dump_cnt++;
        }
        else if ((pNO->lau16PwmDuty[pwm_i] == VALVE_OFF_DUTY)
            &&( (wpe_tempW9&lau8BitMaskCnt) == lau8BitMaskCnt))
        {
            WL->pwm_circulation_cnt++;
        }
        else{}
*/
    }

    if (WL->pwm_full_rise_cnt > 0)
    {
        WL->Wheel_Valve_Mode = Full_Rise_Valve_Mode;
    }
    else if (WL->pwm_dump_cnt > 0)
    {
        WL->Wheel_Valve_Mode = Dump_Valve_Mode;
    }
    else if (WL->pwm_lfc_rise_cnt > 0)
    {
        WL->Wheel_Valve_Mode = LFC_Rise_Valve_Mode;
    }
    else if (WL->pwm_circulation_cnt > 0)
    {
        WL->Wheel_Valve_Mode = Circulation_Valve_Mode;
    }
    else if (WL->pwm_hold_cnt > 0)
    {
        WL->Wheel_Valve_Mode = Hold_Valve_Mode;
    }
    else
    {
        WL->Wheel_Valve_Mode = 5;
    }

  #else

  #if (__REORGANIZE_ABS_FOR_MGH60==0)
    WL->pwm_full_rise_cnt   =0;
    WL->pwm_lfc_rise_cnt    =0;
    WL->pwm_hold_cnt        =0;
    WL->pwm_dump_cnt        =0;
    WL->pwm_circulation_cnt =0;
  #endif

    if (WL->pwm_duty_temp == 0)
    {
        WL->Wheel_Valve_Mode = Full_Rise_Valve_Mode;
       #if (__REORGANIZE_ABS_FOR_MGH60==0)
        WL->pwm_full_rise_cnt   =One_Scan_Cycle_Time_MS; /* 7 -> 5 : kjs_swd11 */
       #endif
    }
    else if ((WL->pwm_duty_temp > 0)&&(WL->pwm_duty_temp < 200))
    {
        WL->Wheel_Valve_Mode = LFC_Rise_Valve_Mode;
       #if (__REORGANIZE_ABS_FOR_MGH60==0)
        WL->pwm_lfc_rise_cnt    =One_Scan_Cycle_Time_MS; /* 7 -> 5 : kjs_swd11 */
       #endif
    }
    else if (WL->pwm_duty_temp == 200)
    {
        WL->Wheel_Valve_Mode = Hold_Valve_Mode;
       #if (__REORGANIZE_ABS_FOR_MGH60==0)
        WL->pwm_hold_cnt        =One_Scan_Cycle_Time_MS; /* 7 -> 5 : kjs_swd11 */
       #endif
    }
    else if (WL->pwm_duty_temp == 201)
    {
        WL->Wheel_Valve_Mode = Dump_Valve_Mode;
       #if (__REORGANIZE_ABS_FOR_MGH60==0)
        WL->pwm_dump_cnt        =One_Scan_Cycle_Time_MS; /* 7 -> 5 : kjs_swd11 */
       #endif
    }
    else if (WL->pwm_duty_temp == 202)
    {
        WL->Wheel_Valve_Mode =Circulation_Valve_Mode;
       #if (__REORGANIZE_ABS_FOR_MGH60==0)
        WL->pwm_circulation_cnt =One_Scan_Cycle_Time_MS; /* 7 -> 5 : kjs_swd11 */
       #endif
    }
    else
    {
        WL->Wheel_Valve_Mode = 5;
    }
  #endif

  #if __VDC
    if (WL->Wheel_Valve_Mode == Full_Rise_Valve_Mode)
    {
        WL->Rise_Mode_cnt       ++;
        WL->LFC_Rise_Mode_cnt   =0;
        WL->Hold_Mode_cnt       =0;
        WL->Dump_Mode_cnt       =0;
        WL->Circ_Mode_cnt       =0;
    }
    else if (WL->Wheel_Valve_Mode == LFC_Rise_Valve_Mode)
    {
        WL->Rise_Mode_cnt       =0;
        WL->LFC_Rise_Mode_cnt   ++;
        WL->Hold_Mode_cnt       =0;
        WL->Dump_Mode_cnt       =0;
        WL->Circ_Mode_cnt       =0;
    }
    else if (WL->Wheel_Valve_Mode == Hold_Valve_Mode)
    {
        WL->Rise_Mode_cnt       =0;
        WL->LFC_Rise_Mode_cnt   =0;
        WL->Hold_Mode_cnt       ++;
        WL->Dump_Mode_cnt       =0;
        WL->Circ_Mode_cnt       =0;
    }
    else if (WL->Wheel_Valve_Mode == Dump_Valve_Mode)
    {
        WL->Rise_Mode_cnt       =0;
        WL->LFC_Rise_Mode_cnt   =0;
        WL->Hold_Mode_cnt       =0;
        WL->Dump_Mode_cnt       ++;
        WL->Circ_Mode_cnt       =0;
    }
    else if (WL->Wheel_Valve_Mode == Circulation_Valve_Mode)
    {
        WL->Rise_Mode_cnt       =0;
        WL->LFC_Rise_Mode_cnt   =0;
        WL->Hold_Mode_cnt       =0;
        WL->Dump_Mode_cnt       =0;
        WL->Circ_Mode_cnt       ++;
    }
    else
    {
        WL->Rise_Mode_cnt       =0;
        WL->LFC_Rise_Mode_cnt   =0;
        WL->Hold_Mode_cnt       =0;
        WL->Dump_Mode_cnt       =0;
        WL->Circ_Mode_cnt       =0;
    }
  #endif
}

#if __PRESS_EST_ABS
void LDABS_vEstWheelPressWLABS(struct W_STRUCT *WL)
{
    WL->P_DumpCnt_old = WL->P_DumpCnt;

    if ((WL->Wheel_Valve_Mode == Full_Rise_Valve_Mode)
        ||(WL->Wheel_Valve_Mode == LFC_Rise_Valve_Mode))
    {
        WL->Estimated_Press_H   = 1000;
      #if __VDC
        WL->Estimated_Press_M   = 700;
        WL->Estimated_Press_L   = 400;
      #endif
        WL->P_DumpCnt = 0;
    }
    else if (WL->Wheel_Valve_Mode == Dump_Valve_Mode)
    {
        WL->Estimated_Press_H  = WL->Estimated_Press_H - ((int16_t)K_dump_H * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_H)))/4;
      #if __VDC
        WL->Estimated_Press_M  = WL->Estimated_Press_M - ((int16_t)K_dump_M * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_M)))/4;
        WL->Estimated_Press_L  = WL->Estimated_Press_L - ((int16_t)K_dump_L * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_L)))/4;
      #endif
        if(WL->state == UNSTABLE) {WL->P_DumpCnt ++;}
    }
    else
    {
        if ((WL->Wheel_Valve_Mode == Hold_Valve_Mode) && (WL->on_time_outlet_pwm >= 4))
        {
            WL->Estimated_Press_H  = WL->Estimated_Press_H
                - ((K_dump_H * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_H))) * (int16_t)WL->on_time_outlet_pwm)/20; /* 28 -> 20 : kjs_swd11 */
          #if __VDC
            WL->Estimated_Press_M  = WL->Estimated_Press_M
                - ((K_dump_M * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_M))) * (int16_t)WL->on_time_outlet_pwm)/20; /* 28 -> 20 : kjs_swd11 */
            WL->Estimated_Press_L  = WL->Estimated_Press_L
                - ((K_dump_L * (int16_t)(LDABS_u16FitSQRT((uint16_t)WL->Estimated_Press_L))) * (int16_t)WL->on_time_outlet_pwm)/20; /* 28 -> 20 : kjs_swd11 */
          #endif
            if(WL->state == UNSTABLE) {WL->P_DumpCnt ++;}
        }
    }

    if (WL->Estimated_Press_H<0)
    {
    	WL->Estimated_Press_H =0;
    }
    else { }
  #if __VDC
    if (WL->Estimated_Press_M<0)
    {
    	WL->Estimated_Press_M =0;
    }
    else { }
    if (WL->Estimated_Press_L<0)
    {
    	WL->Estimated_Press_L =0;
    }
    else { }
  #endif

}


void LDABS_vEstWheelPresSetParameterABS(struct W_STRUCT *WL)
{
    if(WL->REAR_WHEEL==0)
    {
        if(WL->Estimated_Press_H <= 50)
        {
            K_dump_H = Press_Dec_gain_Front_below_5bar;
        }
        else if(WL->Estimated_Press_H <= 100)
        {
            K_dump_H = Press_Dec_gain_Front_5_10bar;
        }
        else if(WL->Estimated_Press_H <= 700)
        {
            K_dump_H = Press_Dec_gain_Front_10_70bar;
        }
        else
        {
            K_dump_H = Press_Dec_gain_Front_Above_70bar;
        }

      #if __VDC
        if(WL->Estimated_Press_M <= 50)
        {
            K_dump_M = Press_Dec_gain_Front_below_5bar;
        }
        else if(WL->Estimated_Press_M <= 100)
        {
            K_dump_M = Press_Dec_gain_Front_5_10bar;
        }
        else if(WL->Estimated_Press_M <= 700)
        {
            K_dump_M = Press_Dec_gain_Front_10_70bar;
        }
        else
        {
            K_dump_M = Press_Dec_gain_Front_Above_70bar;
        }

        if(WL->Estimated_Press_L <= 50)
        {
            K_dump_L = Press_Dec_gain_Front_below_5bar;
        }
        else if(WL->Estimated_Press_L <= 100)
        {
            K_dump_L = Press_Dec_gain_Front_5_10bar;
        }
        else if(WL->Estimated_Press_L <= 700)
        {
            K_dump_L = Press_Dec_gain_Front_10_70bar;
        }
        else
        {
            K_dump_L = Press_Dec_gain_Front_Above_70bar;
        }
      #endif
    }
    else
    {
        if(WL->Estimated_Press_H <= 50)
        {
            K_dump_H = Press_Dec_gain_Rear_below_5bar;
        }
        else if(WL->Estimated_Press_H <= 100)
        {
            K_dump_H = Press_Dec_gain_Rear_5_10bar;
        }
        else if(WL->Estimated_Press_H <= 700)
        {
            K_dump_H = Press_Dec_gain_Rear_10_70bar;
        }
        else
        {
            K_dump_H = Press_Dec_gain_Rear_Above_70bar;
        }

      #if __VDC
        if(WL->Estimated_Press_M <= 50)
        {
            K_dump_M = Press_Dec_gain_Rear_below_5bar;
        }
        else if(WL->Estimated_Press_M <= 100)
        {
            K_dump_M = Press_Dec_gain_Rear_5_10bar;
        }
        else if(WL->Estimated_Press_M <= 700)
        {
            K_dump_M = Press_Dec_gain_Rear_10_70bar;
        }
        else
        {
            K_dump_M = Press_Dec_gain_Rear_Above_70bar;
        }

        if(WL->Estimated_Press_L <= 50)
        {
            K_dump_L = Press_Dec_gain_Rear_below_5bar;
        }
        else if(WL->Estimated_Press_L <= 100)
        {
            K_dump_L = Press_Dec_gain_Rear_5_10bar;
        }
        else if(WL->Estimated_Press_L <= 700)
        {
            K_dump_L = Press_Dec_gain_Rear_10_70bar;
        }
        else
        {
            K_dump_L = Press_Dec_gain_Rear_Above_70bar;
        }
      #endif
    }
}
#endif

#if __VDC && __PRESS_EST_ACTIVE
void LDABS_vEstWheelPresSetParameterActive(struct W_STRUCT *WL)
{

    if(WL->REAR_WHEEL==0)
    {
        K_reapply = LCESP_s16IInter6Point(WL->Estimated_Active_Press_old/press_res,
                            (int16_t)(S16_MP_Rise_Gain_Press_f_1st),(int16_t)(S16_MP_Rise_Gain_f_1st),
                            (int16_t)(S16_MP_Rise_Gain_Press_f_2nd),(int16_t)(S16_MP_Rise_Gain_f_2nd),
                            (int16_t)(S16_MP_Rise_Gain_Press_f_3rd),(int16_t)(S16_MP_Rise_Gain_f_3rd),
                            (int16_t)(S16_MP_Rise_Gain_Press_f_4th),(int16_t)(S16_MP_Rise_Gain_f_4th),
                            (int16_t)(S16_MP_Rise_Gain_Press_f_5th),(int16_t)(S16_MP_Rise_Gain_f_5th),
                            (int16_t)(S16_MP_Rise_Gain_Press_f_6th),(int16_t)(S16_MP_Rise_Gain_f_6th));

        K_dump = LCESP_s16IInter6Point(WL->Estimated_Active_Press_old/press_res,
                            (int16_t)(S16_Dump_Gain_Press_f_1st),(int16_t)(S16_Dump_Gain_f_1st),
                            (int16_t)(S16_Dump_Gain_Press_f_2nd),(int16_t)(S16_Dump_Gain_f_2nd),
                            (int16_t)(S16_Dump_Gain_Press_f_3rd),(int16_t)(S16_Dump_Gain_f_3rd),
                            (int16_t)(S16_Dump_Gain_Press_f_4th),(int16_t)(S16_Dump_Gain_f_4th),
                            (int16_t)(S16_Dump_Gain_Press_f_5th),(int16_t)(S16_Dump_Gain_f_5th),
                            (int16_t)(S16_Dump_Gain_Press_f_6th),(int16_t)(S16_Dump_Gain_f_6th));

        if((AFZ_OK==0)||(WL->LFC_zyklus_z<=1))
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Before_AFZ_OK_f;  /* 1.40bar/7msec ; 200bar/sec    */
        }
        else if(afz>=-(int16_t)U8_PULSE_LOW_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Low_mu_f;         /*0.35bar/7msec ; 50bar/sec     */
        }
        else if(afz>=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Low_mu_f;     /* 0.35bar/7msec ; 50bar/sec */
        }
        else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Medium_mu_f;  /* 1.05bar/7msec ; 150bar/sec */
        }
        else if(afz>=-(int16_t)U8_HIGH_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Medium_mu_f;  /*  1.05bar/7msec ; 150bar/sec */
        }
        else
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_High_mu_f;    /*  0.70bar/7msec ; 100bar/sec */
        }
    }
    else
    {
        K_reapply = LCESP_s16IInter6Point(WL->Estimated_Active_Press_old/press_res,
                            (int16_t)(S16_MP_Rise_Gain_Press_r_1st),(int16_t)(S16_MP_Rise_Gain_r_1st),
                            (int16_t)(S16_MP_Rise_Gain_Press_r_2nd),(int16_t)(S16_MP_Rise_Gain_r_2nd),
                            (int16_t)(S16_MP_Rise_Gain_Press_r_3rd),(int16_t)(S16_MP_Rise_Gain_r_3rd),
                            (int16_t)(S16_MP_Rise_Gain_Press_r_4th),(int16_t)(S16_MP_Rise_Gain_r_4th),
                            (int16_t)(S16_MP_Rise_Gain_Press_r_5th),(int16_t)(S16_MP_Rise_Gain_r_5th),
                            (int16_t)(S16_MP_Rise_Gain_Press_r_6th),(int16_t)(S16_MP_Rise_Gain_r_6th));

        K_dump = LCESP_s16IInter6Point(WL->Estimated_Active_Press_old/press_res,
                            (int16_t)(S16_Dump_Gain_Press_r_1st),(int16_t)(S16_Dump_Gain_r_1st),
                            (int16_t)(S16_Dump_Gain_Press_r_2nd),(int16_t)(S16_Dump_Gain_r_2nd),
                            (int16_t)(S16_Dump_Gain_Press_r_3rd),(int16_t)(S16_Dump_Gain_r_3rd),
                            (int16_t)(S16_Dump_Gain_Press_r_4th),(int16_t)(S16_Dump_Gain_r_4th),
                            (int16_t)(S16_Dump_Gain_Press_r_5th),(int16_t)(S16_Dump_Gain_r_5th),
                            (int16_t)(S16_Dump_Gain_Press_r_6th),(int16_t)(S16_Dump_Gain_r_6th));

        if((AFZ_OK==0)||(WL->LFC_zyklus_z<=1))
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Before_AFZ_OK_r;  /* 1.40bar/7msec ; 200bar/sec    */
        }
        else if(afz>=-(int16_t)U8_PULSE_LOW_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Low_mu_r;         /*0.35bar/7msec ; 50bar/sec     */
        }
        else if(afz>=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Low_mu_r;     /* 0.35bar/7msec ; 50bar/sec */
        }
        else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Medium_mu_r;  /* 1.05bar/7msec ; 150bar/sec */
        }
        else if(afz>=-(int16_t)U8_HIGH_MU)
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_Medium_mu_r;  /*  1.05bar/7msec ; 150bar/sec */
        }
        else
        {
            LFC_RISE_RATE_ABS =(int16_t)S16_ABS_LFC_RISE_RATE_High_mu_r;    /*  0.70bar/7msec ; 100bar/sec */
        }

    /************************************************************************/
    /* Because of Rear Duty Limitation*/
        if((LEFT_WHEEL_wl==1) && (RR.LFC_zyklus_z < 1) && (WL-> pwm_duty_temp == 200))
        {
            LFC_RISE_RATE_ABS=0;
        }
        else if((LEFT_WHEEL_wl==0) && (RL.LFC_zyklus_z < 1) && (WL-> pwm_duty_temp == 200))
         {
            LFC_RISE_RATE_ABS=0;
        }
        else { }
    /************************************************************************/

        if(WL->Duty_Limitation_flag==1)
        {
            LFC_RISE_RATE_ABS = (int16_t)(LFC_RISE_RATE_ABS/2);
        }
        else { }
    }

    if (K_reapply<1)
    {
        K_reapply=1;
    }
    else { }

    if (K_dump<1)
    {
        K_dump=1;
    }
    else { }

	/*
    tempW2 = (int16_t)LFC_RISE_RATE_ABS;
    tempW1 = press_res;
    tempW0 = LOOPTIME_SEC(1);//T_1S;
    s16muls16divs16();
    LFC_RISE_RATE_ABS_x10 = tempW3;
	*/
 		LFC_RISE_RATE_ABS_x10=(int16_t)((((int32_t)LFC_RISE_RATE_ABS)*press_res)/L_U8_TIME_10MSLOOP_1000MS);
}

void LDABS_vEstWheelPressWLActive(struct W_STRUCT *WL)
{
		/* SWD11_KJS */
    uint8_t   U8_ESC_PulseUpRiseGain_Temp;
    uint8_t   U8_ESC_FirstOnTimeGain_Temp;
    /* SWD11_KJS */
	
    WL->Estimated_Press_mon = 0;
    WL->Estimated_Active_Press_old = WL->Estimated_Active_Press;

    if ((WL->LEFT_WHEEL==1)&&(REAR_WHEEL_wl==0))
    {
        if (ESP_BRAKE_CONTROL_FL==1)
        {
            ESP_active_wl = 1;
        }
        else
        {
            ESP_active_wl = 0;
        }
        
        /* swd11_kjs */
        if (ESC_PULSE_UP_RISE_FL==1)
        {
            ldabsu1EscPulseUpRiseMode = 1;
        }
        else
        {
            ldabsu1EscPulseUpRiseMode = 0;
        }
        /* swd11_kjs */
    }
    else if ((WL->LEFT_WHEEL==0)&&(REAR_WHEEL_wl==0))
    {
        if (ESP_BRAKE_CONTROL_FR==1)
        {
            ESP_active_wl = 1;
        }
        else
        {
            ESP_active_wl = 0;
        }
        /* swd11_kjs */
        if (ESC_PULSE_UP_RISE_FR==1)
        {
            ldabsu1EscPulseUpRiseMode = 1;
        }
        else
        {
            ldabsu1EscPulseUpRiseMode = 0;
        }
        /* swd11_kjs */
    }
    else if ((WL->LEFT_WHEEL==1)&&(REAR_WHEEL_wl==1))
    {
        if (ESP_BRAKE_CONTROL_RL==1)
        {
            ESP_active_wl = 1;
        }
        else
        {
            ESP_active_wl = 0;
        }
        /* swd11_kjs */
        if (ESC_PULSE_UP_RISE_RL==1)
        {
            ldabsu1EscPulseUpRiseMode = 1;
        }
        else
        {
            ldabsu1EscPulseUpRiseMode = 0;
        }
        /* swd11_kjs */
    }
    else if ((WL->LEFT_WHEEL==0)&&(REAR_WHEEL_wl==1))
    {
        if (ESP_BRAKE_CONTROL_RR==1)
        {
            ESP_active_wl = 1;
        }
        else
        {
            ESP_active_wl = 0;
        }
        /* swd11_kjs */
        if (ESC_PULSE_UP_RISE_RR==1)
        {
            ldabsu1EscPulseUpRiseMode = 1;
        }
        else
        {
            ldabsu1EscPulseUpRiseMode = 0;
        }
        /* swd11_kjs */
    }
    else
    {
        ; 
    }


    /*__________ caluation diff_tc_and_wheel_press*/
  #if __SPLIT_TYPE                      /*FR Split  */
    if (REAR_WHEEL_wl==0)
    {
        diff_tc_and_wheel_press = Tc_under_pressure_p - WL->Estimated_Active_Press_old;
        Motor_rise_press = Primary_Motor_rise_press;
        Tc_under_press = Tc_under_pressure_p;
        TC_valve_mode = Primary_TC_valve_mode;
    }
    else if (REAR_WHEEL_wl==1)
    {
        diff_tc_and_wheel_press = Tc_under_pressure_s - WL->Estimated_Active_Press_old;
        Motor_rise_press = Secondary_Motor_rise_press;
        Tc_under_press = Tc_under_pressure_s;
        TC_valve_mode = Secondary_TC_valve_mode;
    }
    else
    {
        diff_tc_and_wheel_press = MPRESS_0BAR;
        Motor_rise_press        = MPRESS_0BAR;
        Tc_under_press          = MPRESS_0BAR;
        TC_valve_mode           = 0;
    }
  #else                                 /*X Split  */
    if (((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==1))))
    {
        diff_tc_and_wheel_press = Tc_under_pressure_s - WL->Estimated_Active_Press_old;
        Motor_rise_press = Secondary_Motor_rise_press;
        Tc_under_press = Tc_under_pressure_s;
        TC_valve_mode = Secondary_TC_valve_mode;
    }
    else if (((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==1))))
    {
        diff_tc_and_wheel_press = Tc_under_pressure_p - WL->Estimated_Active_Press_old;
        Motor_rise_press = Primary_Motor_rise_press;
        Tc_under_press = Tc_under_pressure_p;
        TC_valve_mode = Primary_TC_valve_mode;
    }
    else
    {
        diff_tc_and_wheel_press = MPRESS_0BAR;
        Motor_rise_press        = MPRESS_0BAR;
        Tc_under_press          = MPRESS_0BAR;
        TC_valve_mode           = 0;
    }
  #endif

    WL->NO_Rise_Press_Driver =0;
    WL->NO_Rise_Press_Motor  =0;
    WL->NC_Dump_Press = 0;
    WL->NO_Rise_Press = 0;

    switch (WL->Wheel_Valve_Mode)
    {
        case Full_Rise_Valve_Mode:
            if (diff_tc_and_wheel_press >= MPRESS_0BAR )            /* 증압	*/
            {
/*
                tempW2 = ((INT)(LDABS_u16FitSQRT_res((UINT)diff_tc_and_wheel_press)));
                tempW1 = K_reapply*100;    //                    *[100]
                tempW0 = 3162;          //		31.62 = sqrt(10)*10 *[100]
                s16muls16divs16();
                tempW1 = tempW3;
*/
								tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press))))*(K_reapply*100))/3162);
/*
                tempW2 = (INT)WL->pwm_full_rise_cnt;
                tempW1 = tempW1;
                tempW0 = 7;
                s16muls16divs16();
                wpe_tempW7 = tempW3;
*/
								wpe_tempW7=(int16_t)((((int32_t)WL->pwm_full_rise_cnt)*tempW1)/One_Scan_Cycle_Time_MS);

                /* Max apply Limit 20bar */
                wpe_tempW8 = (MPRESS_20BAR)* press_res;
                WL->NO_Rise_Press_Driver = LDABS_s16s16MIN(wpe_tempW8, wpe_tempW7);

                WL->NO_Rise_Press_Driver = LDABS_s16s16MIN(WL->NO_Rise_Press_Driver, diff_tc_and_wheel_press);

                WL->Estimated_Press_mon = 101;

                /*	Motoro Rise Calulation	*/
                if ((( Tc_under_press >= mpress_x10 )||(motor_rpm>=100))
                    &&((TC_valve_mode==2)||(TC_valve_mode==3)))
                {							
										/* swd11_kjs / check PulseUpRiseMode */
										if (ldabsu1EscPulseUpRiseMode == 1) 
										{													
												/* front_rear check */
												if(REAR_WHEEL_wl==0)
												{
														U8_ESC_PulseUpRiseGain_Temp = U8_ESC_PULSE_UP_RISE_GAIN_f;	
												}
												else
												{
														U8_ESC_PulseUpRiseGain_Temp = U8_ESC_PULSE_UP_RISE_GAIN_r;
												}
																							
												if(Motor_rise_press > ((MPRESS_3BAR)*press_res))
												{						
                    				WL->NO_Rise_Press_Motor = ((Motor_rise_press/10)*(U8_ESC_PulseUpRiseGain_Temp/10)*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;
                    				WL->Estimated_Press_mon = WL->Estimated_Press_mon + 11;						/* Estimated_Press_mon = 112 */																								
												}
												else
												{
														WL->NO_Rise_Press_Motor = (((Motor_rise_press*U8_ESC_PulseUpRiseGain_Temp)/100)*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;
                    				WL->Estimated_Press_mon = WL->Estimated_Press_mon + 12;						/* Estimated_Press_mon = 113 */
												}																								
										}
										else /* by kyg */
										{ 
                    		WL->NO_Rise_Press_Motor = Motor_rise_press*(WL->pwm_full_rise_cnt)/One_Scan_Cycle_Time_MS;
                    		WL->Estimated_Press_mon = WL->Estimated_Press_mon + 10; 						  /* Estimated_Press_mon = 111 */
                    }
                    /* swd11_kjs */
                  	/* SWD11_KOR_KJS */
										if(WL->ldabss16EscOnTimerWPE==0)
										{
												/* front_rear check */
												if(REAR_WHEEL_wl==0)
												{
														U8_ESC_FirstOnTimeGain_Temp = U8_ESC_FIRST_ON_TIME_GAIN_f;	
												}
												else
												{
														U8_ESC_FirstOnTimeGain_Temp = U8_ESC_FIRST_ON_TIME_GAIN_r;
												}											
											
												if(Motor_rise_press > ((MPRESS_3BAR)*press_res))
												{						
                    				WL->NO_Rise_Press_Motor = ((Motor_rise_press/10)*(U8_ESC_FirstOnTimeGain_Temp/10)*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;
                    				WL->Estimated_Press_mon = WL->Estimated_Press_mon + 3;						/* Estimated_Press_mon = 115,116,114 */																								
												}
												else
												{
														WL->NO_Rise_Press_Motor = (((Motor_rise_press*U8_ESC_FirstOnTimeGain_Temp)/100)*(WL->pwm_full_rise_cnt))/One_Scan_Cycle_Time_MS;
                    				WL->Estimated_Press_mon = WL->Estimated_Press_mon + 6;						/* Estimated_Press_mon = 118,119,117 */
												}																
										}
										else
										{
											; 																																			/* Estimated_Press_mon = 112,113,111 */
										}
										/* SWD11_KOR_KJS */                    
                }
                else
                {
                    WL->NO_Rise_Press_Motor = 0;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 20;                   /* Estimated_Press_mon = 121 */
                }

                /*	 NO_Rise_Press Calulation	*/
                if (mpress_x10 >= WL->Estimated_Active_Press)
                {
                    WL->NO_Rise_Press = LDABS_s16s16MAX(WL->NO_Rise_Press_Motor,WL->NO_Rise_Press_Driver);
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 50;                   /* Estimated_Press_mon = 165,166,164,168,169,167,162,163,161,171 */
                }
                else
                {
                    WL->NO_Rise_Press = WL->NO_Rise_Press_Motor;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 70;                   /* Estimated_Press_mon = 185,186,184,188,189,187,182,183,181,191 */
                }
            }
            else                                                        /* 감압	*/
            {
/*
                tempW2 = ((INT)(LDABS_u16FitSQRT_res((UINT)(-diff_tc_and_wheel_press))));
                tempW1 = K_reapply*100;    	//                    *[100]
                tempW0 = 3162;          	//	31.62 = sqrt(10)*10 *[100]
                s16muls16divs16();
                tempW1 = -tempW3;
*/
								tempW1=-(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press)))))*(K_reapply*100))/3162);

                WL->NO_Rise_Press = LDABS_s16s16MAX(diff_tc_and_wheel_press, tempW1);
                WL->Estimated_Press_mon = 102;																								/* Estimated_Press_mon = 102 */
            }
        break ;

        case LFC_Rise_Valve_Mode:

            if (diff_tc_and_wheel_press >= MPRESS_0BAR )            /* 증압	*/
            {
                /*	Driver Rise Calulation	*/

                LFC_Hold_press = LCESP_s16IInter6Point((int16_t)WL->pwm_duty_temp,
                                    (int16_t)(S16_LFC_Hold_Duty_1st),(int16_t)(S16_LFC_Hold_press_1st),
                                    (int16_t)(S16_LFC_Hold_Duty_2nd),(int16_t)(S16_LFC_Hold_press_2nd),
                                    (int16_t)(S16_LFC_Hold_Duty_3rd),(int16_t)(S16_LFC_Hold_press_3rd),
                                    (int16_t)(S16_LFC_Hold_Duty_4th),(int16_t)(S16_LFC_Hold_press_4th),
                                    (int16_t)(S16_LFC_Hold_Duty_5th),(int16_t)(S16_LFC_Hold_press_5th),
                                    (int16_t)(S16_LFC_Hold_Duty_6th),(int16_t)(S16_LFC_Hold_press_6th));

                if (mpress_x10 - LFC_Hold_press*press_res > 0 )
                {
                    mpress_x10_LFC_Hold = mpress_x10 - LFC_Hold_press*press_res;
                }
                else
                {
                    mpress_x10_LFC_Hold = 0;
                }

                if (WL->Estimated_Active_Press_old > mpress_x10_LFC_Hold)
                {
                    WL->NO_Rise_Press_Driver = 0;
                    WL->Estimated_Press_mon = 201;
                }
                else if (ESP_active_wl==1)      /*	ESP Partial Rise	*/
                {

//                    tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press)));
//                    tempW1 = K_reapply*100;    /*                    *[100]	*/
//                    tempW0 = 3162;          	/*	31.62 = sqrt(10)*10 *[100]	*/
//                    s16muls16divs16();
//                    wpe_tempW1 = tempW3;

  									wpe_tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press))))*(K_reapply*100))/3162);

                    wpe_tempW1 = LDABS_s16s16MIN(diff_tc_and_wheel_press, wpe_tempW1);

                    if (REAR_WHEEL_wl==0)
                    {
                        wpe_tempW2 = LCESP_s16IInter3Point((int16_t)WL->pwm_duty_temp,
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_f_1st),
                                            (int16_t)(S16_LfcEscMPRiseGainf_1st),
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_f_2nd),
                                            (int16_t)(S16_LfcEscMPRiseGainf_2nd),
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_f_3rd),
                                            (int16_t)(S16_LfcEscMPRiseGainf_3rd));
                    }
                    else
                    {
                        wpe_tempW2 = LCESP_s16IInter3Point((int16_t)WL->pwm_duty_temp,
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_r_1st),
                                            (int16_t)(S16_LfcEscMPRiseGainr_1st),
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_r_2nd),
                                            (int16_t)(S16_LfcEscMPRiseGainr_2nd),
                                            (int16_t)(S16_LfcEscMPRiseGainDuty_r_3rd),
                                            (int16_t)(S16_LfcEscMPRiseGainr_3rd));
                    }
/*
                    tempW2 = wpe_tempW2;
                    tempW1 = wpe_tempW1;
                    tempW0 = 100;
                    s16muls16divs16();
                    WL->NO_Rise_Press_Driver = tempW3;
*/
										WL->NO_Rise_Press_Driver=(int16_t)((((int32_t)wpe_tempW2)*wpe_tempW1)/100);

                    WL->Estimated_Press_mon = 202;
                }
                else if ((WL->MSL_long_hold_flag==1)&&(WL->LFC_built_reapply_flag==1)) /*	ABS LFC HOLD	*/
                {
                    WL->NO_Rise_Press_Driver = 0;
                    WL->Estimated_Press_mon = 203;
                }
                else if (WL->Reapply_Accel_flag==1)                 					/*	Unstable          __ABS LFC Rise	*/
                {
                    if(WL->Forced_Hold_After_Unst_Reapply==1)   /*	hold              __ABS LFC Rise	*/
                    {
                        WL->NO_Rise_Press_Driver = 0;
                        WL->Estimated_Press_mon = 204;
                    }
                    else                                        /*	unstable rise     __ABS LFC Rise	*/
                    {
                    /*  WL->NO_Rise_Press_Driver = (int16_t)LFC_RISE_RATE_ABS_x10*2;	*/

                        diff_tc_and_wheel_press = mpress_x10_LFC_Hold - WL->Estimated_Active_Press_old;

                        if (diff_tc_and_wheel_press<=0)
                        {
                            diff_tc_and_wheel_press=0;
                        }
                        else { }

//                        tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press)));
//                        tempW1 = K_reapply*100;    /*                    *[100]	*/
//                        tempW0 = 3162;          /*	31.62 = sqrt(10)*10 *[100]	*/
//                        s16muls16divs16();
//                        tempW1 = tempW3;

												tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press))))*(K_reapply*100))/3162);

//                        tempW2 = (int16_t)WL->pwm_lfc_rise_cnt;
//                        tempW1 = tempW1;
//                        tempW0 = 5; /* 7 -> 5 : kjs_swd11 */
//                        s16muls16divs16();
//                        wpe_tempW2 = tempW3;

												wpe_tempW2=(int16_t)((((int32_t)WL->pwm_lfc_rise_cnt)*tempW1)/One_Scan_Cycle_Time_MS);

                    /*-----------------------*/
                        if (REAR_WHEEL_wl==0)
                        {
                            wpe_tempW1 = LCESP_s16IInter2Point((int16_t)WL->pwm_duty_temp,20,100,
                                                                    	              40,50);
                        }
                        else
                        {
                            wpe_tempW1 = LCESP_s16IInter2Point((int16_t)WL->pwm_duty_temp,30,100,
                                                                    	              40,50);
                        }
/*
                        tempW2 = wpe_tempW2;
                        tempW1 = wpe_tempW1;
                        tempW0 = 100;
                        s16muls16divs16();
                        tempW1 = tempW3;
*/
                        tempW1=(int16_t)((((int32_t)wpe_tempW2)*wpe_tempW1)/100);
                    /*-----------------------	*/

                        WL->NO_Rise_Press_Driver = LDABS_s16s16MIN(diff_tc_and_wheel_press, tempW1);

                        WL->Estimated_Press_mon = 205;
                    }
                }
                else if ((REAR_WHEEL_wl==1)&&((RR.SL_Unstable_Rise_flg==1)||(RL.SL_Unstable_Rise_flg==1))
                    &&(RL.pwm_duty_temp==RR.pwm_duty_temp)&&(((RL.Reapply_Accel_flag==1))||(RR.Reapply_Accel_flag==1)))                           /* Rear Wheel Duty Copy	*/
                {
                    if(((RR.Forced_Hold_After_Unst_Reapply==1)&&(WL->LEFT_WHEEL==1))
                        ||((RL.Forced_Hold_After_Unst_Reapply==1)&&(WL->LEFT_WHEEL==0)))
                        /*hold              __ABS LFC Rise	*/
                    {
                        WL->NO_Rise_Press_Driver = 0;
                        WL->Estimated_Press_mon = 206;
                    }

                    else                                        /*unstable rise     __ABS LFC Rise	*/
                    {
                    /*  WL->NO_Rise_Press_Driver = (int16_t)LFC_RISE_RATE_ABS_x10*2;	*/


                        diff_tc_and_wheel_press = mpress_x10_LFC_Hold - WL->Estimated_Active_Press_old;

                        if (diff_tc_and_wheel_press<=0)
                        {
                            diff_tc_and_wheel_press=0;
                        }
                        else { }

//                        tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press)));
//                        tempW1 = K_reapply*100;    /*                    *[100] */
//                        tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]	*/
//                        s16muls16divs16();
//                        tempW1 = tempW3;

												tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)diff_tc_and_wheel_press))))*(K_reapply*100))/3162);

//                        tempW2 = (int16_t)WL->pwm_lfc_rise_cnt;
//                        tempW1 = tempW1;
//                        tempW0 = 5; /* 7 -> 5 : kjs_swd11 */
//                        s16muls16divs16();
//                        wpe_tempW7 = tempW3;

												wpe_tempW7=(int16_t)((((int32_t)WL->pwm_lfc_rise_cnt)*tempW1)/One_Scan_Cycle_Time_MS);

                        wpe_tempW2 = (int16_t)WL->pwm_lfc_rise_cnt>>2;

                    /*-----------------------*/
                        wpe_tempW1 = LCESP_s16IInter2Point((int16_t)WL->pwm_duty_temp,30,100,
                                                                              	  40,50);
//                        tempW2 = wpe_tempW2;
//                        tempW1 = wpe_tempW1;
//                        tempW0 = 100;
//                        s16muls16divs16();
//                        tempW1 = tempW3;

												tempW1=(int16_t)((((int32_t)wpe_tempW2)*wpe_tempW1)/100);
                    /*-----------------------*/

                        WL->NO_Rise_Press_Driver = LDABS_s16s16MIN(diff_tc_and_wheel_press, tempW1);

                        WL->Estimated_Press_mon = 207;
                    }
                }
                else                                            /*normal LFC Rise   __Normal LFC Rise*/
                {
                    WL->NO_Rise_Press_Driver = (int16_t)LFC_RISE_RATE_ABS_x10;
                    WL->Estimated_Press_mon = 208;
                }

                /*MOTOR Rise Calulation(LFC)*/
                if ((( Tc_under_press >= mpress_x10 )||(motor_rpm>=100))
                    &&((TC_valve_mode==2)||(TC_valve_mode==3)))         /*MOTOR rise 로 수정.*/
                {
                    wpe_tempW1 = Motor_rise_press;

                    if (REAR_WHEEL_wl==0)
                    {
                        wpe_tempW2 = LCESP_s16IInter3Point((int16_t)WL->pwm_duty_temp,
                                            (int16_t)(S16_LfcEscMRiseGainDuty_f_1st),
                                            (int16_t)(S16_LfcEscMRiseGainf_1st),
                                            (int16_t)(S16_LfcEscMRiseGainDuty_f_2nd),
                                            (int16_t)(S16_LfcEscMRiseGainf_2nd),
                                            (int16_t)(S16_LfcEscMRiseGainDuty_f_3rd),
                                            (int16_t)(S16_LfcEscMRiseGainf_3rd));
                    }
                    else
                    {
                        wpe_tempW2 = LCESP_s16IInter3Point((int16_t)WL->pwm_duty_temp,
                                            (int16_t)(S16_LfcEscMRiseGainDuty_r_1st),
                                            (int16_t)(S16_LfcEscMRiseGainr_1st),
                                            (int16_t)(S16_LfcEscMRiseGainDuty_r_2nd),
                                            (int16_t)(S16_LfcEscMRiseGainr_2nd),
                                            (int16_t)(S16_LfcEscMRiseGainDuty_r_3rd),
                                            (int16_t)(S16_LfcEscMRiseGainr_3rd));
                    }

//                    tempW2 = wpe_tempW2;
//                    tempW1 = wpe_tempW1;
//                    tempW0 = 100;
//                    s16muls16divs16();
//                    WL->NO_Rise_Press_Motor = tempW3;
										WL->NO_Rise_Press_Motor=(int16_t)((((int32_t)wpe_tempW2)*wpe_tempW1)/100);
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 10;
                }
                else                                                    /*Driver Rise*/
                {
                    WL->NO_Rise_Press_Motor = 0;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 20;
                }

                if (mpress_x10_LFC_Hold > WL->Estimated_Active_Press_old)
                {
                    WL->NO_Rise_Press = LDABS_s16s16MAX(WL->NO_Rise_Press_Motor,WL->NO_Rise_Press_Driver);
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 60;
                }
                else
                {
                    WL->NO_Rise_Press = WL->NO_Rise_Press_Motor;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon + 70;
                }
            }
            else                                                        /* 감압*/
            {


//                tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press))));
//                tempW1 = K_reapply*100;    /*                    *[100]	*/
//                tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]	*/
//                s16muls16divs16();
//                tempW1 = -tempW3*2;

								tempW1 =-(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press)))))*(K_reapply*100))/3162)*2;

                WL->NO_Rise_Press = LDABS_s16s16MAX(diff_tc_and_wheel_press, tempW1);
                WL->Estimated_Press_mon = 209;
            }
        break ;

        case Hold_Valve_Mode:    /*hold*/
            if (diff_tc_and_wheel_press >= MPRESS_0BAR )
            {
                WL->NO_Rise_Press = 0;
                WL->Estimated_Press_mon = 301;
            }
            else
            {

//                tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press))));
//                tempW1 = K_reapply*100;    /*                    *[100]*/
//                tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]	*/
//                s16muls16divs16();
//                tempW1 = -tempW3*2;

								tempW1=-(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press)))))*(K_reapply*100))/3162)*2;

                WL->NO_Rise_Press = LDABS_s16s16MAX(diff_tc_and_wheel_press, tempW1);
                WL->Estimated_Press_mon = 302;
            }
        break ;

        case Dump_Valve_Mode:

            tempW0 = WL->Estimated_Active_Press_old - 250;

//            tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)tempW0)));
//            tempW1 = K_dump*100;    /*                    *[100]*/
//            tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]*/
//            s16muls16divs16();
//            tempW1 = tempW3;

						/* kjs */
						if(tempW0<0)
						{
							tempW0=0;
						}
						else
						{
							;
						}
						/* kjs */
						
//			    tempW1=(INT)((((LONG)((INT)(LDABS_u16FitSQRT_res((UINT)(WL->Estimated_Active_Press_old - 250)))))*(K_dump*100))/3162);
			      tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(tempW0)))))*(K_dump*100))/3162);

//            tempW2 = (int16_t)WL->pwm_dump_cnt;
//            tempW1 = tempW1;
//            tempW0 = 5; /* 7 -> 5 : kjs_swd11 */
//            s16muls16divs16();
//            wpe_tempW7 = tempW3;

						wpe_tempW7=(int16_t)((((int32_t)WL->pwm_dump_cnt)*tempW1)/One_Scan_Cycle_Time_MS);

            /* Max Dump Limit 20bar */
            wpe_tempW8 = MPRESS_30BAR * press_res;
            WL->NC_Dump_Press = LDABS_s16s16MIN(wpe_tempW8, wpe_tempW7);

            WL->NC_Dump_Press = LDABS_s16s16MIN(WL->NC_Dump_Press, WL->Estimated_Active_Press_old);

            WL->Estimated_Press_mon = 401;

            if (diff_tc_and_wheel_press >= MPRESS_0BAR ){}
            else
            {
//                tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press))));
//                tempW1 = K_reapply*100;    /*                    *[100]*/
//                tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]*/
//                s16muls16divs16();
//                tempW1 = -tempW3*2;

								tempW1=-(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press)))))*(K_reapply*100))/3162)*2;

                WL->NO_Rise_Press = LDABS_s16s16MAX(diff_tc_and_wheel_press, tempW1);
                WL->Estimated_Press_mon = 402;
            }
        break ;

        case Circulation_Valve_Mode:
            if (diff_tc_and_wheel_press >= MPRESS_0BAR )
            {

                wpe_tempW9 = LCESP_s16IInter2Point(motor_rpm,
                                    (int16_t)(S16_Circulation_Motor_speed_1st),(int16_t)(S16_Circulation_Press_1st),
                                    (int16_t)(S16_Circulation_Motor_speed_2nd),(int16_t)(S16_Circulation_Press_2nd));

                if (WL->Estimated_Active_Press_old > wpe_tempW9*press_res)
                {

                    tempW0 = WL->Estimated_Active_Press_old;

//                    tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)tempW0)));
//                    tempW1 = K_dump*100;    /*                    *[100]*/
//                    tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]*/
//                    s16muls16divs16();
//                    tempW1 = tempW3;

										tempW1=(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)tempW0))))*K_dump*100)/3162);

                    WL->NO_Rise_Press = LDABS_s16s16MIN(MPRESS_8BAR*press_res, tempW1);
                    WL->NO_Rise_Press = -WL->NO_Rise_Press;
                    WL->Estimated_Press_mon = 501;

                }
                else
                {
                    WL->NO_Rise_Press = Motor_rise_press/2;
                    WL->Estimated_Press_mon = 502;
                }
            }
            else
            {

//                tempW2 = ((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press))));
//                tempW1 = K_reapply*100;    /*                    *[100]*/
//                tempW0 = 3162;          /*31.62 = sqrt(10)*10 *[100]*/
//                s16muls16divs16();
//                tempW1 = -tempW3*2;

								tempW1=-(int16_t)((((int32_t)((int16_t)(LDABS_u16FitSQRT_res((uint16_t)(-diff_tc_and_wheel_press)))))*K_reapply*100)/3162)*2;

                WL->NO_Rise_Press = LDABS_s16s16MAX(diff_tc_and_wheel_press, tempW1);
                WL->Estimated_Press_mon = 503;
            }
        break ;

        default:
            tempW1 = MPRESS_0BAR;
            WL->Estimated_Press_mon = 601;
        break ;
    }

    if (REAR_WHEEL_wl==0)
    {
        wpe_tempW9 = (int16_t)S16_NO_Rise_Max_Press_f;
        wpe_tempW8 = (int16_t)S16_NC_Dump_Max_Press_f;
        wpe_tempW7 = (int16_t)S16_NO_Dump_Max_Press_f;
    }
    else
    {
        wpe_tempW9 = (int16_t)S16_NO_Rise_Max_Press_r;
        wpe_tempW8 = (int16_t)S16_NC_Dump_Max_Press_r;
        wpe_tempW7 = (int16_t)S16_NO_Dump_Max_Press_r;
    }

    if (REAR_WHEEL_wl==0)
    {
        if (WL->NO_Rise_Press > wpe_tempW9*press_res)
        {
            WL->NO_Rise_Press = wpe_tempW9*press_res;    /* Max Rate Front */
        }
        else if (WL->NO_Rise_Press < -wpe_tempW7*press_res)
        {
            WL->NO_Rise_Press = -wpe_tempW7*press_res;    /* Max Rate Front */
        }
        else {}
    }
    else
    {
        if (WL->NC_Dump_Press >wpe_tempW8*press_res)
        {
            WL->NC_Dump_Press = wpe_tempW8*press_res;    /* Max Rate Rear */
        }
        else if (WL->NC_Dump_Press < 0)
        {
            WL->NC_Dump_Press = 0 ;
        }
        else { }
    }

    WL->Estimated_Active_Press = WL->Estimated_Active_Press + WL->NO_Rise_Press - WL->NC_Dump_Press;

    if (WL->Estimated_Active_Press < MPRESS_0BAR )
    {
        WL->Estimated_Active_Press = MPRESS_0BAR;
    }
    else {}

    /*  Active 가 아닌 경우의 최대 압력 설정.   */
    /*    if((WL->L_SLIP_CONTROL==0) && (PBA_ON==0) && (vref>=VREF_8_KPH))	*/
    if((ABS_fz==1)&& (vref>=VREF_8_KPH))
    {
      #if __SPLIT_TYPE                      /*FR Split  */
        if((TCL_DEMAND_PRIMARY==0)&&(REAR_WHEEL_wl==0))
        {
            if(WL->Estimated_Active_Press >= mpress_x10)
            {
                WL->Estimated_Active_Press = mpress_x10;
                WL->Estimated_Press_mon = WL->Estimated_Press_mon +500 ;

            }
            else{ }
        }
        else{ }

        if((TCL_DEMAND_SECONDARY==0)&&(REAR_WHEEL_wl==1))
        {
            if(WL->Estimated_Active_Press >= mpress_x10)
            {
                WL->Estimated_Active_Press = mpress_x10;
                WL->Estimated_Press_mon = WL->Estimated_Press_mon +500 ;
            }
            else { }
        }
        else{ }
      #else                                 /*X Split  */
        if((TCL_DEMAND_fr==0)&&(TCL_DEMAND_rl==0))
        {
            if (((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==1))))
            {
                if(WL->Estimated_Active_Press >= mpress_x10)
                {
                    WL->Estimated_Active_Press = mpress_x10;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon +500 ;
                }
                else{ }
            }
            else{ }
        }
        else{ }

        if((TCL_DEMAND_fl==0)&&(TCL_DEMAND_rr==0))
        {
            if (((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==1))))
            {
                if(WL->Estimated_Active_Press >= mpress_x10)
                {
                    WL->Estimated_Active_Press = mpress_x10;
                    WL->Estimated_Press_mon = WL->Estimated_Press_mon +500 ;
                }
                else { }
            }
            else{ }
        }
        else{ }
      #endif
    }
    else{ }

  #if __TCMF_ENABLE

    wpe_tempW9=0;
  #if __SPLIT_TYPE                      /*FR Split  */
    if (REAR_WHEEL_wl==1)
    {
        wpe_tempW9=1;
    }
    else{ }
  #else                                 /*X Split  */
    if (((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==1))))
    {
        wpe_tempW9=1;
    }
    else{ }
  #endif

    if (wpe_tempW9==1)
    {
        if (TC_MFC_Voltage_Secondary_dash>300)
        {
            tempW1 = Secondary_TC_hold_press_x10 + mpress_x10;
            if (WL->Estimated_Active_Press > tempW1)
            {
                WL->Estimated_Active_Press = tempW1;
                WL->Estimated_Press_mon = WL->Estimated_Press_mon +1000 ;
            }
            else{ }
        }
        else{ }
    }
    else{ }

    wpe_tempW9=0;
  #if __SPLIT_TYPE                      /*FR Split  */
    if (REAR_WHEEL_wl==0)
    {
        wpe_tempW9=1;
    }
    else{ }
  #else                                 /*X Split  */
    if (((WL->LEFT_WHEEL==0)&&((REAR_WHEEL_wl==0)))||((WL->LEFT_WHEEL==1)&&((REAR_WHEEL_wl==1))))
    {
        wpe_tempW9=1;
    }
    else{ }
  #endif

    if (wpe_tempW9==1)
    {
        if (TC_MFC_Voltage_Primary_dash>300)
        {
            tempW1 = Primary_TC_hold_press_x10 + mpress_x10;
            if (WL->Estimated_Active_Press > tempW1)
            {
                WL->Estimated_Active_Press = tempW1;
                WL->Estimated_Press_mon = WL->Estimated_Press_mon +1000 ;
            }
            else{ }
        }
        else{ }
    }
    else{ }
  #endif     /* Rise            */


    if(mp_hw_suspcs_flg==1)
    {
        WL->Estimated_Active_Press = 0;    /*배제조건*/ //swd temp
    }
    else
    {
        ;
    }

}

void LDABS_vEstMotorRisePress(void)
{
/*  motor rise caluation           bar*resolution /scan  */
    pressure_rise_rate_rpm = (int16_t)S16_Motor_Rise_Rate_Max_f;    /* 6 Volt 기준 rate */

//    tempW2 = pressure_rise_rate_rpm;        /* [rpm 보정]	*/
//    tempW1 = motor_rpm;
//    tempW0 = 2688;  /*  (448*6);*/
//    s16muls16divs16();

		tempW3=(int16_t)((((int32_t)pressure_rise_rate_rpm)*motor_rpm)/2688);

//    tempW2 = tempW3;                        /* [ bar/sec]*/
//    tempW1 = 500;   /*  7*100;  */ /* 7msec -> 5msec : 5*100 = 500 : kjs_swd11 */
//    tempW0 = 1000;
//    s16muls16divs16();
//    pressure_rise_rate = tempW3;            /*  [bar/scan]*/

			pressure_rise_rate=(int16_t)((((int32_t)tempW3)*1000)/1000); /* 10ms : 10*100 */
}

void LDABS_vEstTCPressCircuit(void)
{

    Primary_Rise_Mode_mon=0;
    Secondary_Rise_Mode_mon=0;

    Primary_Motor_rise_press =0;
    Secondary_Motor_rise_press =0;
  #if __SPLIT_TYPE                      /*FR Split  */
    Tc_under_pressure_p_old = LDABS_s16s16MAX(FR.Estimated_Active_Press,FL.Estimated_Active_Press);
    Tc_under_pressure_p     = LDABS_s16s16MAX(FR.Estimated_Active_Press,FL.Estimated_Active_Press);

    Tc_under_pressure_s_old = LDABS_s16s16MAX(RL.Estimated_Active_Press,RR.Estimated_Active_Press);
    Tc_under_pressure_s     = LDABS_s16s16MAX(RL.Estimated_Active_Press,RR.Estimated_Active_Press);
  #else                                 /*X Split  */
    Tc_under_pressure_p_old = LDABS_s16s16MAX(FR.Estimated_Active_Press,RL.Estimated_Active_Press);
    Tc_under_pressure_p     = LDABS_s16s16MAX(FR.Estimated_Active_Press,RL.Estimated_Active_Press);

    Tc_under_pressure_s_old = LDABS_s16s16MAX(FL.Estimated_Active_Press,RR.Estimated_Active_Press);
    Tc_under_pressure_s     = LDABS_s16s16MAX(FL.Estimated_Active_Press,RR.Estimated_Active_Press);
  #endif

    LDABS_vCheckPrimaryCircuit();
    LDABS_vCheckPrimaryTCValve();


    /*  __________________________ fr Circuit                           */
  #if __SPLIT_TYPE                      /*FR Split  */
    if (TCL_DEMAND_PRIMARY==0)
    { }
    else
    {                              /*  TCL_DEMAND_fr Active Braking     */
        if ((S_VALVE_PRIMARY==1)&&(motor_rpm>=50))
        {
  #else                                 /*X Split  */
    if ((TCL_DEMAND_fr==0)&&(TCL_DEMAND_rl==0))
    { }
    else
    {                              /*  TCL_DEMAND_fr Active Braking     */
        if ((S_VALVE_RIGHT==1)&&(motor_rpm>=50))
        {
  #endif

            wpe_tempW8 = LCESP_s16IInter6Point(Tc_under_pressure_p_old/press_res,
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_1st),(int16_t)(S16_Motor_Rise_Gain_f_1st),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_2nd),(int16_t)(S16_Motor_Rise_Gain_f_2nd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_3rd),(int16_t)(S16_Motor_Rise_Gain_f_3rd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_4th),(int16_t)(S16_Motor_Rise_Gain_f_4th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_5th),(int16_t)(S16_Motor_Rise_Gain_f_5th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_6th),(int16_t)(S16_Motor_Rise_Gain_f_6th));

          #if __SPLIT_TYPE                      /*FR Split */

          #else                                 /*X Split  */

            wpe_tempW9 = LCESP_s16IInter6Point(Tc_under_pressure_p_old/press_res,
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_1st),(int16_t)(S16_Motor_Rise_Gain_r_1st),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_2nd),(int16_t)(S16_Motor_Rise_Gain_r_2nd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_3rd),(int16_t)(S16_Motor_Rise_Gain_r_3rd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_4th),(int16_t)(S16_Motor_Rise_Gain_r_4th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_5th),(int16_t)(S16_Motor_Rise_Gain_r_5th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_6th),(int16_t)(S16_Motor_Rise_Gain_r_6th));
          #endif

            if (wpe_tempW8 <1)
            {
                wpe_tempW8 =1;
            }
            else {}

            if (wpe_tempW9 <1)
            {
                wpe_tempW9 =1;
            }
            else {}

            if (Primary_Rise_Gain > 100)
            {
//                tempW2 = 100;       /*같이 혹은 혼자 ?  */
//                tempW1 = pressure_rise_rate;
//                tempW0 = 1000;
//                s16muls16divs16();
//                wpe_tempW0 = tempW3;
//                wpe_tempW0=(INT)((((LONG)100)*pressure_rise_rate)/1000);
              
                wpe_tempW0=(int16_t)(pressure_rise_rate)/10;
                Pressure_rise_gain_P =wpe_tempW9;
            }
            else
            {

//                tempW2 = Primary_Rise_Gain;     /*같이 혹은 혼자 ?  */
//                tempW1 = pressure_rise_rate;
//                tempW0 = 1000;
//                s16muls16divs16();
//                wpe_tempW0 = tempW3;

                wpe_tempW0=(int16_t)((((int32_t)Primary_Rise_Gain)*pressure_rise_rate)/1000);
                Pressure_rise_gain_P =wpe_tempW8;
            }

//            tempW2 = wpe_tempW0;
//            tempW1 = Pressure_rise_gain_P;
//            tempW0 = 100;
//            s16muls16divs16();
//            tempW1 = tempW3;

 							tempW1=(int16_t)((((int32_t)wpe_tempW0)*Pressure_rise_gain_P)/100);
/* _______________________________________________________________________*/
            /*  Full Rise/LFC   */
            if (Primary_Rise_Mode==1)
            {
                if (Primary_Hold_Rise_Gain==1)
                {
                    tempW1 = tempW1 *12/10;
                    Primary_Hold_Rise_Gain=0;
                }
                else
                {
                    ;
                }
            }
            /*   Hold      */
            else if (Primary_Rise_Mode==2)
            {
                tempW1 = 0;
                if (motor_rpm > 400)
                {
                    Primary_Hold_Rise_Gain=1;
                }
                else
                {
                    Primary_Hold_Rise_Gain=0;
                }
            }
            /*  Dump       */
            else if (Primary_Rise_Mode==3)
            {
                tempW1 = 0;
                Primary_Hold_Rise_Gain=0;
            }
            /*  Circulation    */
            else
            {
                Tc_under_pressure_p_old = 0;
                tempW1 = tempW1;
                Primary_Hold_Rise_Gain=0;
            }

            /*Tc_under_pressure_p = Tc_under_pressure_p_old + tempW1;*/
            Primary_Motor_rise_press = tempW1;
        }
        /*  ~ 모터구동    */
        else
        {
            tempW1=0; /*    Hold 유지   */
            Primary_Motor_rise_press = tempW1;
            /*Tc_under_pressure_p = Tc_under_pressure_p_old + tempW1;*/
            Primary_Hold_Rise_Gain=0;
        }
    }


    /*__________________________ fl Circuit     */

    LDABS_vCheckSecondaryCircuit();

    LDABS_vCheckSecondaryTCValve();


  #if __SPLIT_TYPE                      /*FR Split  */
    if (TCL_DEMAND_SECONDARY==0)
    { }
    else
    {                              /*  TCL_DEMAND_fl Active Braking    */
        if ((S_VALVE_SECONDARY==1)&&(motor_rpm>=50))
        {
  #else                                 /*X Split  */
    if ((TCL_DEMAND_fl==0)&&(TCL_DEMAND_rr==0))
    { }
    else
    {                              /*  TCL_DEMAND_fl Active Braking    */
        if ((S_VALVE_LEFT==1)&&(motor_rpm>=50))
        {
  #endif

            Motor_Rise_Gain = 100;

            wpe_tempW0 = Secondary_Rise_Gain * Motor_Rise_Gain/100;

//            tempW2 = wpe_tempW0;
//            tempW1 = pressure_rise_rate;
//            tempW0 = 1000;
//            s16muls16divs16();
//            wpe_tempW0 = tempW3;

						wpe_tempW0=(int16_t)((((int32_t)wpe_tempW0)*pressure_rise_rate)/1000);

            wpe_tempW9 = LCESP_s16IInter6Point(Tc_under_pressure_s_old/press_res,
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_1st),(int16_t)(S16_Motor_Rise_Gain_r_1st),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_2nd),(int16_t)(S16_Motor_Rise_Gain_r_2nd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_3rd),(int16_t)(S16_Motor_Rise_Gain_r_3rd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_4th),(int16_t)(S16_Motor_Rise_Gain_r_4th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_5th),(int16_t)(S16_Motor_Rise_Gain_r_5th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_r_6th),(int16_t)(S16_Motor_Rise_Gain_r_6th));

          #if __SPLIT_TYPE                      /*FR Split */

          #else                                 /*X Split  */
            wpe_tempW8 = LCESP_s16IInter6Point(Tc_under_pressure_s_old/press_res,
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_1st),(int16_t)(S16_Motor_Rise_Gain_f_1st),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_2nd),(int16_t)(S16_Motor_Rise_Gain_f_2nd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_3rd),(int16_t)(S16_Motor_Rise_Gain_f_3rd),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_4th),(int16_t)(S16_Motor_Rise_Gain_f_4th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_5th),(int16_t)(S16_Motor_Rise_Gain_f_5th),
                                        (int16_t)(S16_Motor_Rise_Gain_Press_f_6th),(int16_t)(S16_Motor_Rise_Gain_f_6th));
          #endif

            if (wpe_tempW8 <1)
            {
                wpe_tempW8 =1;
            }
            else {}

            if (wpe_tempW9 <1)
            {
                wpe_tempW9 =1;
            }
            else {}

#if __SPLIT_TYPE                      /*FR Split */
//                tempW2 = 100;       /*같이 혹은 혼자 ?  */
//                tempW1 = pressure_rise_rate;
//                tempW0 = 1000;
//                s16muls16divs16();
//                wpe_tempW0 = tempW3;
								
								wpe_tempW0=(int16_t)(pressure_rise_rate)/10;
                Pressure_rise_gain_S =wpe_tempW9;
#else                               /*X Split  */
            if (Secondary_Rise_Gain > 100)
            {
//                tempW2 = 100;       /*같이 혹은 혼자 ?  */
//                tempW1 = pressure_rise_rate;
//                tempW0 = 1000;
//                s16muls16divs16();
//                wpe_tempW0 = tempW3;

								wpe_tempW0=(int16_t)(pressure_rise_rate)/10;
                Pressure_rise_gain_S =wpe_tempW9;
            }
            else
            {
//                tempW2 = Secondary_Rise_Gain;       /*같이 혹은 혼자 ?  */
//                tempW1 = pressure_rise_rate;
//                tempW0 = 1000;
//                s16muls16divs16();
//                wpe_tempW0 = tempW3;

								wpe_tempW0=(int16_t)((((int32_t)Secondary_Rise_Gain)*pressure_rise_rate)/1000);
                Pressure_rise_gain_S =wpe_tempW8;
            }
#endif
//            tempW2 = wpe_tempW0;
//            tempW1 = Pressure_rise_gain_S;
//            tempW0 = 100;
//            s16muls16divs16();
//            tempW1 = tempW3;

							tempW1=(int16_t)((((int32_t)wpe_tempW0)*Pressure_rise_gain_S)/100);

/* _______________________________________________________________________*/
            /*  Full Rise/LFC      */
            if (Secondary_Rise_Mode==1)
            {
                if (Secondary_Hold_Rise_Gain==1)
                {
                    tempW1 = tempW1 *12/10;
                    Secondary_Hold_Rise_Gain=0;
                }
                else
                {
                    ;
                }
            }
            /* Hold   */
            else if (Secondary_Rise_Mode==2)
            {
                tempW1 = 0;
                if (motor_rpm > 400)
                {
                    Secondary_Hold_Rise_Gain=1;
                }
                else
                {
                    Secondary_Hold_Rise_Gain=0;
                }
            }
            /* Dump   */
            else if (Secondary_Rise_Mode==3)
            {
                tempW1 = 0;
                Secondary_Hold_Rise_Gain=0;
            }
            /*  Circulation */
            else
            {
                Tc_under_pressure_s_old = 0;
                tempW1 = tempW1;
                Secondary_Hold_Rise_Gain=0;
            }

            /*Tc_under_pressure_s = Tc_under_pressure_s_old + tempW1;*/
            Secondary_Motor_rise_press = tempW1;


        }/* ~ 모터구동  */
        else
        {
            tempW1=0;
            Secondary_Motor_rise_press = tempW1;
            /*Tc_under_pressure_s = Tc_under_pressure_s_old + tempW1;*/
            Secondary_Hold_Rise_Gain=0;
        }
    }

    LDABS_vEstTCPressCircuitMinMax();

}


void LDABS_vEstTCPressCircuitMinMax(void){
    /*_____________________________MIN, MAX 결정    */


    wpe_tempW0 = Primary_TC_hold_press_x10;
    wpe_tempW1 = Secondary_TC_hold_press_x10;

  #if __SPLIT_TYPE                      /*FR Split  */
    if (TCL_DEMAND_PRIMARY==1)
    {
        wpe_tempW9=1;
    }
    else
    {
        wpe_tempW9=0;
    }
  #else                                 /*X Split  */
    if ((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))
    {
        wpe_tempW9=1;
    }
    else
    {
        wpe_tempW9=0;
    }
  #endif

    if (wpe_tempW9==1)
    {
        if (Tc_under_pressure_p > wpe_tempW0 + mpress_x10)
        {
            Tc_under_pressure_p = wpe_tempW0 + mpress_x10;
        }
        else
        { }
        Tc_under_pressure_p = LDABS_s16s16MAX(Tc_under_pressure_p,mpress_x10);
    }
    else
    {
        Tc_under_pressure_p = mpress_x10;
    }


  #if __SPLIT_TYPE                      /*FR Split  */
    if (TCL_DEMAND_SECONDARY==1)
    {
        wpe_tempW9=1;
    }
    else
    {
        wpe_tempW9=0;
    }
  #else                                 /*X Split  */
    if ((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))
    {
        wpe_tempW9=1;
    }
    else
    {
        wpe_tempW9=0;
    }
  #endif

    if (wpe_tempW9==1)
    {
        if (Tc_under_pressure_s > wpe_tempW1 + mpress_x10)
        {
            Tc_under_pressure_s = wpe_tempW1 + mpress_x10;
        }
        else
        { }
        Tc_under_pressure_s = LDABS_s16s16MAX(Tc_under_pressure_s,mpress_x10);
    }
    else
    {
        Tc_under_pressure_s = mpress_x10;
    }
}


void LDABS_vEstTCCircuitRiseComp(void){

    /*  ______________________fr Circuit    */
    wpe_tempW0 = FR.Estimated_Active_Press - RL.Estimated_Active_Press;
    if (wpe_tempW0 < 0)
    {
        wpe_tempW0 = - wpe_tempW0;
    }
    else
    {
        ;
    }

    if ((Primary_Rise_Mode_mon==1)&&(wpe_tempW0 > 50*press_res)&&(TCL_DEMAND_fr==1))
    {

        wpe_tempW0 = FR.Estimated_Active_Press;
        wpe_tempW1 = RL.Estimated_Active_Press;

        if (FR.Estimated_Active_Press > RL.Estimated_Active_Press)
        {
            FR.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW1, FR.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_7_5HZ);  /*    3scan에 50% 보정            */
            RL.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW0, RL.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);  /*    4scan에 50% 보정            */
        }
        else
        {
            FR.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW1, FR.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);  /*    4scan에 50% 보정            */
            RL.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW0, RL.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_7_5HZ);  /*    3scan에 50% 보정             */
        }
    }
    else
    {
        ;
    }

    /*  ______________________fl Circuit    */
    wpe_tempW0 = FL.Estimated_Active_Press - RR.Estimated_Active_Press;
    if (wpe_tempW0 < 0)
    {
        wpe_tempW0 = - wpe_tempW0;
    }
    else
    {
        ;
    }

    if ((Secondary_Rise_Mode_mon==1)&&(wpe_tempW0 > 50*press_res)&&(TCL_DEMAND_fl==1))
    {

        wpe_tempW0 = FL.Estimated_Active_Press;
        wpe_tempW1 = RR.Estimated_Active_Press;

        if (FL.Estimated_Active_Press > RR.Estimated_Active_Press)
        {
            FL.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW1, FL.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_7_5HZ);  /*3scan에 50% 보정    */
            RR.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW0, RR.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);  /*4scan에 50% 보정    */
        }
        else
        {
            FL.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW1, FL.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);  /*4scan에 50% 보정    */
            RR.Estimated_Active_Press = LCESP_s16Lpf1Int(wpe_tempW0, RR.Estimated_Active_Press, L_U8FILTER_GAIN_10MSLOOP_7_5HZ);  /*3scan에 50% 보정    */
        }
    }
    else
    {
        ;
    }
}

void LDABS_vOutputWheelPress(void)
{

  #if (__FL_PRESS==ENABLE)
    if( (fu1FLPErrorDet==0) && (flp_hw_suspcs_flg==0 ) && (flp_signal_suspcs_flg==0) )
    {
        LDABS_vUseWheelPress(&FL,&PRESS_FL);
    }
    else 
    { 
    	LDABS_vUseEstmPress(&FL); 
    	//LDABS_vUseWheelPress(&FL,&PRESS_FL); //SWD temp  	
    }
  #else
        LDABS_vUseEstmPress(&FL);
  #endif

  #if (__FR_PRESS==ENABLE)
    if( (fu1FRPErrorDet==0) && (frp_hw_suspcs_flg==0 ) && (frp_signal_suspcs_flg==0) ) //swd temp
    {
        LDABS_vUseWheelPress(&FR,&PRESS_FR);
    }
    else 
    { 
    	LDABS_vUseEstmPress(&FR);    	
    	//LDABS_vUseWheelPress(&FR,&PRESS_FR);
    }
  #else
        LDABS_vUseEstmPress(&FR);
  #endif

  #if (__RL_PRESS==ENABLE)
    if( (fu1RLPErrorDet==0) && (rlp_hw_suspcs_flg==0 ) && (rlp_signal_suspcs_flg==0) ) 
    {
        LDABS_vUseWheelPress(&RL,&PRESS_RL);
    }
    else 
    { 
    	LDABS_vUseEstmPress(&RL);    	
    }
  #else
        LDABS_vUseEstmPress(&RL);
  #endif
  

  #if (__RR_PRESS==ENABLE)
    if( (fu1RRPErrorDet==0) && (rrp_hw_suspcs_flg==0 ) && (rrp_signal_suspcs_flg==0) ) 
    {
        LDABS_vUseWheelPress(&RR,&PRESS_RR);
    }
    else 
    { 
    	LDABS_vUseEstmPress(&RR);    	
    }
  #else
        LDABS_vUseEstmPress(&RR);
  #endif
  
      /* SWD11_KOR_KJS */
    if(((RL.ldabss16EscOnTimerWPE ==0)&&(RL.s16_Estimated_Active_Press >= (MPRESS_10BAR+MPRESS_2BAR)))
    		||((RL.ldabss16EscOnTimerWPE >0)&&(RL.s16_Estimated_Active_Press >= MPRESS_2BAR)))
    {
    	RL.ldabss16EscOnTimerWPE = L_U16_TIME_10MSLOOP_3S; /* 428 */
    }
    else    	
    {
    	RL.ldabss16EscOnTimerWPE = RL.ldabss16EscOnTimerWPE - 1;
    }
    if(RL.ldabss16EscOnTimerWPE < 0)
    {
    	RL.ldabss16EscOnTimerWPE = 0;
    }
    else    	
    {
    	;
    }
    if(((RR.ldabss16EscOnTimerWPE ==0)&&(RR.s16_Estimated_Active_Press >= (MPRESS_10BAR+MPRESS_2BAR)))
    		||((RR.ldabss16EscOnTimerWPE >0)&&(RR.s16_Estimated_Active_Press >= MPRESS_2BAR)))
    {
    	RR.ldabss16EscOnTimerWPE = L_U16_TIME_10MSLOOP_3S; /* 428 */
    }
    else    	
    {
    	RR.ldabss16EscOnTimerWPE = RR.ldabss16EscOnTimerWPE - 1;
    }
    if(RR.ldabss16EscOnTimerWPE < 0)
    {
    	RR.ldabss16EscOnTimerWPE = 0;
    }
    else    	
    {
    	;
    }
    
    if(((FR.ldabss16EscOnTimerWPE ==0)&&(FR.s16_Estimated_Active_Press >= (MPRESS_10BAR+MPRESS_2BAR)))
    		||((FR.ldabss16EscOnTimerWPE >0)&&(FR.s16_Estimated_Active_Press >= MPRESS_2BAR)))
    {
    	FR.ldabss16EscOnTimerWPE = L_U16_TIME_10MSLOOP_3S; /* 428 */
    }
    else    	
    {
    	FR.ldabss16EscOnTimerWPE = FR.ldabss16EscOnTimerWPE - 1;
    }
    if(FR.ldabss16EscOnTimerWPE < 0)
    {
    	FR.ldabss16EscOnTimerWPE = 0;
    }
    else    	
    {
    	;
    }
    if(((FL.ldabss16EscOnTimerWPE ==0)&&(FL.s16_Estimated_Active_Press >= (MPRESS_10BAR+MPRESS_2BAR)))
    		||((FL.ldabss16EscOnTimerWPE >0)&&(FL.s16_Estimated_Active_Press >= MPRESS_2BAR)))
    {
    	FL.ldabss16EscOnTimerWPE = L_U16_TIME_10MSLOOP_3S; /* 428 */
    }
    else    	
    {
    	FL.ldabss16EscOnTimerWPE = FL.ldabss16EscOnTimerWPE - 1;
    }
    if(FL.ldabss16EscOnTimerWPE < 0)
    {
    	FL.ldabss16EscOnTimerWPE = 0;
    }
    else    	
    {
    	;
    }        
    /* SWD11_KOR_KJS */

}

#if (__FL_PRESS==ENABLE) || (__FR_PRESS==ENABLE) || (__RL_PRESS==ENABLE) || (__RR_PRESS==ENABLE)
void LDABS_vUseWheelPress(struct W_STRUCT *WL,struct PREM_PRESS_OFFSET_STRUCT  *PRESS)
{
    if(PRESS->s16premFiltPress < MPRESS_200BAR)
    {
        WL->u8_Estimated_Active_Press = (uint8_t)(PRESS->s16premFiltPress/press_res);
    }
    else
    {
        WL->u8_Estimated_Active_Press = (uint8_t)(MPRESS_200BAR/press_res);
    }

    WL->s16_Estimated_Active_Press = PRESS->s16premFiltPress;

    
}
#endif /* __PREMIUM_RBC_PRESSURE_SEN_5_EA_OFFSET */

void  LDABS_vUseEstmPress(struct W_STRUCT *WL)
{
    if (WL->Estimated_Active_Press >= 255*10*press_res)
    {
        WL->u8_Estimated_Active_Press = 255;
    }
    else
    {
        WL->u8_Estimated_Active_Press = (uint8_t)(WL->Estimated_Active_Press/10/press_res);
    }
    WL->s16_Estimated_Active_Press = WL->Estimated_Active_Press/press_res;
    
    //WL->s16_Estimated_Active_Press = PRESS->s16premFiltPress;/*SWD temp*/

    if (EST_WHEEL_PRESS_OK_FLG==0)
    {
        WL->s16_Estimated_Active_Press = 0;
        WL->u8_Estimated_Active_Press = 0;
    }
    else { }
        	
}



void LDABS_vEstTCHoldPress(void)
{
  #if __TCMF_ENABLE
    Primary_TC_hold_press   = la_PtcMfc.MFC_Target_Press*10;
    Secondary_TC_hold_press = la_StcMfc.MFC_Target_Press*10;

    Max_TCMF_hold_press = (int16_t)S16_TCMF_Max_Press;

    if (Primary_TC_hold_press > Max_TCMF_hold_press )
    {
        Primary_TC_hold_press   = Max_TCMF_hold_press;
    }
    else if (Primary_TC_hold_press < MPRESS_0BAR)
    {
        Primary_TC_hold_press   = MPRESS_0BAR;
    }
    else { }

    if (Secondary_TC_hold_press > Max_TCMF_hold_press )
    {
        Secondary_TC_hold_press   = Max_TCMF_hold_press;
    }
    else if (Secondary_TC_hold_press < MPRESS_0BAR)
    {
        Secondary_TC_hold_press   = MPRESS_0BAR;
    }
    else { }

  #else

   #if __SPLIT_TYPE                      /*FR Split  */
    if (TCL_DEMAND_PRIMARY==1)
    {
        Primary_TC_hold_press = (int16_t)S16_TCMF_Max_Press;
    }
    else
    {
        Primary_TC_hold_press = MPRESS_0BAR;
    }
    if (TCL_DEMAND_SECONDARY==1)
    {
        Secondary_TC_hold_press = (int16_t)S16_TCMF_Max_Press;
    }
    else
    {
        Secondary_TC_hold_press = MPRESS_0BAR;
    }
   #else                                /*X Split  */
    if ((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))
    {
        Primary_TC_hold_press = (int16_t)S16_TCMF_Max_Press;
    }
    else
    {
        Primary_TC_hold_press = MPRESS_0BAR;
    }
    if ((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))
    {
        Secondary_TC_hold_press = (int16_t)S16_TCMF_Max_Press;
    }
    else
    {
        Secondary_TC_hold_press = MPRESS_0BAR;
    }
   #endif
  #endif
}


void LDABS_vEstResolutionConv(void)
{

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)  
    #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    mpress_x10                  = (PRESS_M.s16premRaw -PRESS_M.s16premPressOfs_f)*press_res;
    #else
    mpress_x10                  = (raw_mpress - mpress_offset)*press_res;
    #endif
#else
    mpress_x10                  = lsesps16MpressByAHBgen3*press_res;
#endif

    Primary_TC_hold_press_x10   = Primary_TC_hold_press*press_res;
    Secondary_TC_hold_press_x10 = Secondary_TC_hold_press*press_res;

    if (mpress_x10 < MPRESS_0BAR)
    {
        mpress_x10 = MPRESS_0BAR;
    }
    else { }
}

void LDABS_vCheckPrimaryCircuit(void)
{
  #if __SPLIT_TYPE                      /*FR Split  */
    /*  Front Full Rise */
    if ((HV_VL_fr==0)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==0))
    {
        /*  Full Rise   */
        if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=50;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=1;
        }
        /*  LFC Rise   */
        else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
                ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=2;
        }
        /*  Hold   */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=3;
        }
        /*  Dump   */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=4;
        }
        /*  Circulation   */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=5;
        }
    }
    /*  Front LFC Rise */
    else if (((HV_VL_fr==0)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==1))||
            ((HV_VL_fr==1)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==1)))
    {
        /*  Full Rise     */
        if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=6;
        }
        /*  LFC Rise */
        else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
                ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=7;
        }
        /*  Hold */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=8;
        }
        /*  Dump */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=9;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=10;
        }
    }
    /*  Front Hold */
    else if ((HV_VL_fr==1)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==0))
    {
        /*  Full Rise    */
        if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;    /*    200 * 50% ADD   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=11;
        }
        /*  LFC Rise */
        else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
                ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;    /*    150*1.2 =   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=12;
        }
        /*  Hold */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=13;
        }
        /*  Dump */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=14;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=15;
        }
    }
    /*  Front Dump */
    else if ((HV_VL_fr==1)&&(AV_VL_fr==1))
    {
        /*  Full Rise     */
        if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;    /*    200 * 50% ADD   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=16;
        }
        /*  LFC Rise */
        else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
                ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=17;
        }
        /*  Hold */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=18;
        }
        /*  Dump */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=3;
            Primary_Rise_Mode_mon=19;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=20;
        }
    }
    /*  Front Circulation   */
    else
    {
        /*  Full Rise       */
        if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=21;
        }
        /*  LFC Rise */
        else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
                ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=22;
        }
        /*  Hold */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=23;
        }
        /*  Dump */
        else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=24;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=25;
        }
    }
  #else                                 /*X Split  */
    /*  Front Full Rise */
    if ((HV_VL_fr==0)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==0))
    {
        /*  Full Rise   */
        if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=66;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=1;
        }
        /*  LFC Rise   */
        else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
                ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=2;
        }
        /*  Hold   */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=3;
        }
        /*  Dump   */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=4;
        }
        /*  Circulation   */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=5;
        }
    }
    /*  Front LFC Rise */
    else if (((HV_VL_fr==0)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==1))||
            ((HV_VL_fr==1)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==1)))
    {
        /*  Full Rise     */
        if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=6;
        }
        /*  LFC Rise */
        else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
                ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=7;
        }
        /*  Hold */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
//            Primary_Rise_Gain=150;
 						Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=8;
        }
        /*  Dump */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
//            Primary_Rise_Gain=150;
 						Primary_Rise_Gain=100;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=9;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=10;
        }
    }
    /*  Front Hold */
    else if ((HV_VL_fr==1)&&(AV_VL_fr==0)&&(FR.LFC_built_reapply_flag==0))
    {
        /*  Full Rise    */
        if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=200;    /*    200 * 50% ADD   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=11;
        }
        /*  LFC Rise */
        else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
                ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=200;    /*    150*1.2 =   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=12;
        }
        /*  Hold */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=13;
        }
        /*  Dump */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=14;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=15;
        }
    }
    /*  Front Dump */
    else if ((HV_VL_fr==1)&&(AV_VL_fr==1))
    {
        /*  Full Rise     */
        if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=200;    /*    200 * 50% ADD   */
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=16;
        }
        /*  LFC Rise */
        else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
                ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=200;
            Primary_Rise_Mode=1;
            Primary_Rise_Mode_mon=17;
        }
        /*  Hold */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=2;
            Primary_Rise_Mode_mon=18;
        }
        /*  Dump */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=3;
            Primary_Rise_Mode_mon=19;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=20;
        }
    }
    /*  Front Circulation   */
    else
    {
        /*  Full Rise       */
        if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=21;
        }
        /*  LFC Rise */
        else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
                ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=22;
        }
        /*  Hold */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=23;
        }
        /*  Dump */
        else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=24;
        }
        /*  Circulation */
        else
        {
            Primary_Rise_Gain=100;
            Primary_Rise_Mode=4;
            Primary_Rise_Mode_mon=25;
        }
    }
  #endif
}

void LDABS_vCheckSecondaryCircuit(void)
{
  #if __SPLIT_TYPE                      /*FR Split  */
    /*  Front Full Rise */
    if ((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
    {
        /*  Full Rise     */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=1;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=2;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=3;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=4;

        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=5;
        }
    }
    /*  Front LFC Rise */
    else if (((HV_VL_rl==0)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1))||
            ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==1)))
    {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=6;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=7;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=8;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=9;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=10;
        }
    }
    /*  Front Hold */
    else if ((HV_VL_rl==1)&&(AV_VL_rl==0)&&(RL.LFC_built_reapply_flag==0))
    {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=11;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=12;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=13;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=14;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=15;
        }
    }
    /*  Front Dump  */
    else if ((HV_VL_rl==1)&&(AV_VL_rl==1))
    {
        /*  Full Rise                 */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=16;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=17;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=18;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=3;
            Secondary_Rise_Mode_mon=19;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=20;
        }
    }
    /*  Front Circulation   */
    else {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=21;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=22;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=23;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=24;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=25;
        }
    }
  #else                                 /*X Split  */
    /*  Front Full Rise */
    if ((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
    {
        /*  Full Rise     */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Gain=66;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=1;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=2;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=3;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=4;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=5;
        }
    }
    /*  Front LFC Rise */
    else if (((HV_VL_fl==0)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1))||
            ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==1)))
    {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=6;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=7;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
//            Secondary_Rise_Gain=150;
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=8;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
//            Secondary_Rise_Gain=150;
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=9;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=10;
        }
    }
    /*  Front Hold */
    else if ((HV_VL_fl==1)&&(AV_VL_fl==0)&&(FL.LFC_built_reapply_flag==0))
    {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=200;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=11;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=200;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=12;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=13;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=14;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=15;
        }
    }
    /*  Front Dump  */
    else if ((HV_VL_fl==1)&&(AV_VL_fl==1))
    {
        /*  Full Rise                 */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=200;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=16;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=200;
            Secondary_Rise_Mode=1;
            Secondary_Rise_Mode_mon=17;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=2;
            Secondary_Rise_Mode_mon=18;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=3;
            Secondary_Rise_Mode_mon=19;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=20;
        }
    }
    /*  Front Circulation   */
    else {
        /*  Full Rise */
        if ((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=21;
        }
        /*  LFC Rise */
        else if (((HV_VL_rr==0)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1))||
                ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==1)))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=22;
        }
        /*  Hold */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==0)&&(RR.LFC_built_reapply_flag==0))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=23;
        }
        /*  Dump */
        else if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=24;
        }
        /*  Circulation */
        else
        {
            Secondary_Rise_Gain=100;
            Secondary_Rise_Mode=4;
            Secondary_Rise_Mode_mon=25;
        }
    }
  #endif
}

void LDABS_vCheckPrimaryTCValve(void)
{
  #if __SPLIT_TYPE                      /*FR Split  */
    if ((TCL_DEMAND_PRIMARY==0)&&(S_VALVE_PRIMARY==0))
    {
        Primary_TC_valve_mode = 0;
    }
    else if ((TCL_DEMAND_PRIMARY==0)&&(S_VALVE_PRIMARY==1))
    {
        Primary_TC_valve_mode = 1;
    }
    else if ((TCL_DEMAND_PRIMARY==1)&&(S_VALVE_PRIMARY==0))
    {
        Primary_TC_valve_mode = 2;
    }
    else if ((TCL_DEMAND_PRIMARY==1)&&(S_VALVE_PRIMARY==1))
    {
        Primary_TC_valve_mode = 3;
    }
    else
    {
        Primary_TC_valve_mode = 4;
    }

  #else                                 /*X Split  */

    if (((TCL_DEMAND_fr==0)&&(TCL_DEMAND_rl==0))&&(S_VALVE_RIGHT==0))
    {
        Primary_TC_valve_mode = 0;
    }
    else if (((TCL_DEMAND_fr==0)&&(TCL_DEMAND_rl==0))&&(S_VALVE_RIGHT==1))
    {
        Primary_TC_valve_mode = 1;
    }
    else if (((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))&&(S_VALVE_RIGHT==0))
    {
        Primary_TC_valve_mode = 2;
    }
    else if (((TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1))&&(S_VALVE_RIGHT==1))
    {
        Primary_TC_valve_mode = 3;
    }
    else
    {
        Primary_TC_valve_mode = 4;
    }
  #endif
}

void LDABS_vCheckSecondaryTCValve(void)
{
  #if __SPLIT_TYPE                      /*FR Split  */
    if ((TCL_DEMAND_SECONDARY==0)&&(S_VALVE_SECONDARY==0))
    {
        Secondary_TC_valve_mode = 0;
    }
    else if ((TCL_DEMAND_SECONDARY==0)&&(S_VALVE_SECONDARY==1))
    {
        Secondary_TC_valve_mode = 1;
    }
    else if ((TCL_DEMAND_SECONDARY==1)&&(S_VALVE_SECONDARY==0))
    {
        Secondary_TC_valve_mode = 2;
    }
    else if ((TCL_DEMAND_SECONDARY==1)&&(S_VALVE_SECONDARY==1))
    {
        Secondary_TC_valve_mode = 3;
    }
    else
    {
        Secondary_TC_valve_mode = 4;
    }
  #else                                 /*X Split  */
    if (((TCL_DEMAND_fl==0)&&(TCL_DEMAND_rr==0))&&(S_VALVE_LEFT==0))
    {
        Secondary_TC_valve_mode = 0;
    }
    else if (((TCL_DEMAND_fl==0)&&(TCL_DEMAND_rr==0))&&(S_VALVE_LEFT==1))
    {
        Secondary_TC_valve_mode = 1;
    }
    else if (((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))&&(S_VALVE_LEFT==0))
    {
        Secondary_TC_valve_mode = 2;
    }
    else if (((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1))&&(S_VALVE_LEFT==1))
    {
        Secondary_TC_valve_mode = 3;
    }
    else
    {
        Secondary_TC_valve_mode = 4;
    }
  #endif
}

#endif

uint16_t LDABS_u16FitSQRT(uint16_t tempW12)
{
    if(tempW12>=3000)
    {
    	tempW12=3000;
    }
    else { }

    if(tempW12<20)
    {
        tempUW2 = tempW12+5; tempUW1 = 1925;
    }
    else if(tempW12<80)
    {
        tempUW2 = tempW12+45; tempUW1 = 733;
    }
    else if(tempW12<200)
    {
        tempUW2 = tempW12+133; tempUW1 = 430;
    }
    else if(tempW12<400)
    {
        tempUW2 = tempW12+291; tempUW1 = 292;
    }
    else if(tempW12<800)
    {
        tempUW2 = tempW12+575; tempUW1 = 207;
    }
    else if(tempW12<1600)
    {
        tempUW2 = tempW12+1145; tempUW1 = 147;
    }
    else
    {
        tempUW2 = tempW12+2249; tempUW1 = 105;
    }

    tempUW0 = 10000;
    u16mulu16divu16();

    return  tempUW3;
}

#if __VDC && __PRESS_EST_ACTIVE
uint16_t LDABS_u16FitSQRT_res(uint16_t tempW12)
{

    tempW12 = tempW12/(uint16_t)press_res;

    if(tempW12>=3000)
    {
    	tempW12=3000;
    }
    else { }
	if(tempW12 < 3){
	    
	    tempUW2 = tempW12; tempUW1 = 7071; 
	}
	else if(tempW12 < 11){
		
	    
	    tempUW2 = tempW12+4; tempUW1 = 2258; 
	}
	else if(tempW12< 30){

    	tempUW2 = tempW12+20; tempUW1 = 1099; // 5 1925 / 20 1099

    }
    else if(tempW12<80)
    {
        tempUW2 = tempW12+45; tempUW1 = 733;
    }
    else if(tempW12<200)
    {
        tempUW2 = tempW12+133; tempUW1 = 430;
    }
    else if(tempW12<400)
    {
        tempUW2 = tempW12+291; tempUW1 = 292;
    }
    else if(tempW12<800)
    {
        tempUW2 = tempW12+575; tempUW1 = 207;
    }
    else if(tempW12<1600)
    {
        tempUW2 = tempW12+1145; tempUW1 = 147;
    }
    else
    {
        tempUW2 = tempW12+2249; tempUW1 = 105;
    }

    tempUW0 = 10000/(uint16_t)press_res;
    u16mulu16divu16();

    return  tempUW3;
}

// <skeon OPTIME_MACFUNC_2> 110303
#ifndef OPTIME_MACFUNC_2
int16_t LDABS_s16s16MAX(int16_t lds16Wpe_tempW1,int16_t lds16Wpe_tempW2)
{
    if (lds16Wpe_tempW1>=lds16Wpe_tempW2)
    {
        return  lds16Wpe_tempW1;
    }
    else
    {
        return  lds16Wpe_tempW2;
    }
}

int16_t LDABS_s16s16MIN(int16_t lds16Wpe_tempW1,int16_t lds16Wpe_tempW2)
{
    if (lds16Wpe_tempW1>=lds16Wpe_tempW2)
    {
        return  lds16Wpe_tempW2;
    }
    else
    {
        return  lds16Wpe_tempW1;
    }
}

#endif
// </skeon OPTIME_MACFUNC_2>

#endif

    #if __PRESS_EST_ABS
void LDABS_vEstMuIndex(void)
{
    LDABS_vEstMuIndex_WL(&FL);
    LDABS_vEstMuIndex_WL(&FR);
    LDABS_vEstMuIndex_WL(&RL);
    LDABS_vEstMuIndex_WL(&RR);
}

void LDABS_vEstMuIndex_WL(struct W_STRUCT *WL_temp)
{
    int16_t temp_value0, temp_value1;

    WL = WL_temp;

    #define Pressure_SKID  MPRESS_100BAR
    #define K_index        1000

    if(ABS_wl==1)
    {
        if(WL->state == UNSTABLE) {temp_value0 = Pressure_SKID - WL->Estimated_Press_H;}
        else if(WL->DeltaP>0) {temp_value0 = Pressure_SKID - WL->Estimated_Press_H;}
        else {temp_value0 = 0;}

        if(temp_value0 > 0)
        {
            temp_value1=LCESP_s16Lpf1Int(temp_value0, (WL->DeltaP*10), L_U8FILTER_GAIN_10MSLOOP_12_5HZ);
            WL->DeltaP = temp_value1/10;

            if(WL->DeltaP < 4)
            {
                WL->APratio = 0;
            }
            else
            {
                if( WL->arad < ARAD_0G0)
                {
                    WL->APratio = 0;
                }
                else
                {
/*
                    if(WL->arad>128) {temp_value1=128;}
                    else {temp_value1=WL->arad;}
*/
                    temp_value1 = LCABS_s16LimitMinMax((int16_t)WL->arad, 0, 128);

                    WL->APratio=(uint16_t)((int32_t)(K_index*temp_value1)/WL->DeltaP);
                    WL->APratio=(uint16_t)LCABS_s16LimitMinMax((int16_t)WL->APratio, 1, 1000);
/*
                    if(WL->APratio < 1) {WL->APratio = 1;}
                    else if(WL->APratio > 1000) {WL->APratio = 1000;}
                    else { }
*/
                    if((uint16_t)WL->APratio_sum < (uint16_t)60000)
                    {
                        WL->APratio_sum = WL->APratio_sum + WL->APratio;
                        WL->APratio_num = WL->APratio_num + 1;
                        WL->APratio_avg = (WL->APratio_sum/WL->APratio_num);
                        if(WL->APratio_avg > 30000) {WL->APratio_avg = 30000;}
                    }
                    else
                    {
                        ;
                    }

                }
            }
        }
        else
        {
            WL->DeltaP = 0;
            WL->APratio = 0;
            WL->APratio_sum = 0;  WL->APratio_num = 0;
        }

        LDABS_vSetMuIndex_WL();
    }
    else
    {
        WL->DeltaP = 0;
        WL->APratio = 0;
        WL->APratio_sum = 0;  WL->APratio_num = 0;  WL->APratio_avg = 0;
        WL->IndexMu = 0;
        WL->LOW_MU_SUSPECT_by_IndexMu = 0;
    }
}


void LDABS_vSetMuIndex_WL(void)
{
    int16_t IndexLowMu;
    uint8_t LowIndexWheels;

/*
    if(WL->P_DumpCnt >= DumpSetLimitFrt )
    {
        if(WL->APratio_avg == 0)
        {
            WL->IndexMu=WL->IndexMu;
        }
        else if(WL->APratio_avg >= IndexHighMu )
        {
            WL->IndexMu=IndexHighMu;
        }
        else if(WL->APratio_avg <= IndexLowMu )
        {
            WL->IndexMu=IndexLowMu;
        }
        else
        {
            WL->IndexMu=IndexMidMu;
        }
    }
    else if(WL->P_DumpCnt > 0)
    {
        WL->IndexMu=WL->IndexMu;
    }
    else
    {
        if((WL->P_DumpCnt_old >0) && (WL->P_DumpCnt_old < DumpSetLimitFrt))
        {
            if(WL->APratio_avg == 0)
            {
                WL->IndexMu=WL->IndexMu;
            }
            else if(WL->APratio_avg >= IndexHighMu )
            {
                WL->IndexMu=IndexHighMu;
            }
            else if(WL->APratio_avg <= (IndexLowMu/2))
            {
                WL->IndexMu=IndexLowMu;
            }
            else
            {
                WL->IndexMu=IndexMidMu;
            }
        }
    }
*/

/*
    if(vref >= VREF_100_KPH)
    {
        IndexLowMu = IndexBasalt;
    }
    else if(vref <= VREF_50_KPH)
    {
        IndexLowMu = IndexCeramic;
    }
    else
    {
        IndexLowMu = IndexCeramic + (((vref - VREF_50_KPH)*2)/VREF_5_KPH);
    }
*/
    /*IndexLowMu = LCESP_s16IInter2Point(vref, VREF_40_KPH, IndexCeramic, VREF_100_KPH, IndexBasalt);*/
    IndexLowMu = LCABS_s16Interpolation2P(vref, VREF_40_KPH, VREF_100_KPH, IndexCeramic, IndexBasalt);

	LowIndexWheels = (uint8_t)FL.LOW_MU_SUSPECT_by_IndexMu + (uint8_t)FR.LOW_MU_SUSPECT_by_IndexMu
	               + (uint8_t)RL.LOW_MU_SUSPECT_by_IndexMu + (uint8_t)RR.LOW_MU_SUSPECT_by_IndexMu;

	if(REAR_WHEEL_wl==0)
	{
    	if(LEFT_WHEEL_wl==1)
    	{
			if(RL.LOW_MU_SUSPECT_by_IndexMu == 1) {LowIndexWheels++;}
		}
		else
		{
			if(RR.LOW_MU_SUSPECT_by_IndexMu == 1) {LowIndexWheels++;}
		}
	}
	else
	{
    	if(LEFT_WHEEL_wl==1)
    	{
			if(FL.LOW_MU_SUSPECT_by_IndexMu == 1) {LowIndexWheels++;}
		}
		else
		{
			if(FR.LOW_MU_SUSPECT_by_IndexMu == 1) {LowIndexWheels++;}
		}
	}


    if((WL->P_DumpCnt==0) && (WL->P_DumpCnt_old>0))
    {
        WL->IndexMu=(int16_t)WL->APratio_avg;

        if(WL->LFC_built_reapply_flag == 1)
        {
        	tempC2 = WL->LFC_UNSTABIL_counter_old;
        }
        else
        {
        	tempC2 = WL->LFC_UNSTABIL_counter;
        }

        if(WL->LOW_MU_SUSPECT_by_IndexMu == 0)
        {
        	if(LowIndexWheels>=3)
        	{
        		IndexLowMu = IndexLowMu + 10;
        	}

            if((WL->IndexMu <= IndexLowMu) && (vref>=VREF_10_KPH))
            {
                if((tempC2 >= L_TIME_210MS) && (WL->peak_slip <= -(LAM_2P + (int8_t)((VREF_3_KPH*100)/vref))))
                {
                    WL->LOW_MU_SUSPECT_by_IndexMu = 1;
                }
/*
                if(WL->LFC_built_reapply_flag == 1)
                {
                    if((WL->LFC_UNSTABIL_counter_old >= 30) && (WL->peak_slip <= -(LAM_2P + (int8_t)((VREF_3_KPH*100)/vref))))
                    {
                        WL->LOW_MU_SUSPECT_by_IndexMu = 1;
                    }
                }
                else
                {
                    if((WL->LFC_UNSTABIL_counter >= 30) && (WL->peak_slip <= -(LAM_2P + (int8_t)((VREF_3_KPH*100)/vref))))
                    {
                        WL->LOW_MU_SUSPECT_by_IndexMu = 1;
                    }
                }
*/
            }
        }
        else
        {
        	if(LowIndexWheels>=3)
        	{
        		IndexLowMu = IndexLowMu*2;
        	}
        	else
        	{
        		IndexLowMu = IndexLowMu + 20;
        	}

            if(WL->IndexMu <= IndexLowMu)
            {
                if((tempC2 < L_TIME_100MS) || (WL->peak_slip > -(LAM_2P + (int8_t)((VREF_2_KPH*100)/vref))))
                {
                    WL->LOW_MU_SUSPECT_by_IndexMu = 0;
                }
/*
                if(WL->LFC_built_reapply_flag == 1)
                {
                    if((WL->LFC_UNSTABIL_counter_old < 15) || (WL->peak_slip > -(LAM_2P + (int8_t)((VREF_2_KPH*100)/vref))))
                    {
                        WL->LOW_MU_SUSPECT_by_IndexMu = 0;
                    }
                }
                else
                {
                    if((WL->LFC_UNSTABIL_counter < 15) || (WL->peak_slip > -(LAM_2P + (int8_t)((VREF_2_KPH*100)/vref))))
                    {
                        WL->LOW_MU_SUSPECT_by_IndexMu = 0;
                    }
                }
*/
            }
            else
            {
                WL->LOW_MU_SUSPECT_by_IndexMu = 0;
            }
        }
    }
    else { }
}

    #endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSDctWheelStatus
	#include "Mdyn_autosar.h"
#endif
