/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LSPBACallSensorSignal.c
* Description:		Sensor Signal Processiong of PBA
* Logic version:	HV27
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C22			eslim			Initial Release
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSPBACallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#include "LSPBACallSensorSignal.h"
#include "LSESPFilterEspSensor.h"

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/
#if __VDC
void LSPBA_vCallSensorSignal(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LSPBA_vCallSensorSignal
* CALLED BY:			LS_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Sensor Signal of PBA
********************************************************************************/
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
void LSPBA_vCallSensorSignal(void)
{
    MCpress[3]=MCpress[2];  /* MAIN05A */
    MCpress[2]=MCpress[1];
    MCpress[1]=MCpress[0];
    MCpress[0]=lsesps16EstBrkPressByBrkPedalF;

    /*  do filter on mpress_slop by Kim jeonghun in 2005.2.20 */
    if(speed_calc_timer >= (uint8_t)L_U8_TIME_10MSLOOP_1500MS)
    {
    	mpress_slop_filt = (mpress_slop_filt/2) + ((MCpress[0]-MCpress[3])/2);
	    mpress_slop = (MCpress[0]-MCpress[3]);
    }
    else
    {
    	;
    }
    
  /*---------------------------------------------------------------------*/

    if(mpress_slop>=MPRESS_60BAR||mpress_slop<=(-MPRESS_60BAR))
    {
    	PBA_MPS_ERROR = 1;
    }
    else
    {
    	;
    }
    
    if(lsespu1PedalBrakeOn==0) 
    {
    	PBA_MPS_ERROR = 0;
    }
    else
    {
    	;
    }

    if((fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(PBA_MPS_ERROR==1)) 
    {
        mpress_slop=mpress_slop_filt=0;
    }
    else
    {
    	;
    }

}

#else
void LSPBA_vCallSensorSignal(void)
{
    MCpress[3]=MCpress[2];  /* MAIN05A */
    MCpress[2]=MCpress[1];
    MCpress[1]=MCpress[0];
    MCpress[0]=mpress;

    /*  do filter on mpress_slop by Kim jeonghun in 2005.2.20 */
    if(speed_calc_timer >= (uint8_t)L_U8_TIME_10MSLOOP_1500MS)
    {
    	mpress_slop_filt = mpress_slop_filt/2 + (MCpress[0]-MCpress[3])/2;
	    mpress_slop = (MCpress[0]-MCpress[3]);
    }
    else
    {
    	;
    }
    
  /*---------------------------------------------------------------------*/  
    
    if(mpress_slop>=MPRESS_60BAR||mpress_slop<=(-MPRESS_60BAR))
    {
    	PBA_MPS_ERROR = 1;
    }
    else
    {
    	;
    }
    
    if(MPRESS_BRAKE_ON==0) 
    {
    	PBA_MPS_ERROR = 0;
    }
    else
    {
    	;
    }

    if((fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(PBA_MPS_ERROR==1)) 
    {
        mpress_slop=mpress_slop_filt=0;
    }
    else
    {
    	;
    }

}
#endif

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSPBACallSensorSignal
	#include "Mdyn_autosar.h"
#endif
