#ifndef __LCESPCALLPRESSURECONTROL_H__
#define __LCESPCALLPRESSURECONTROL_H__
/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
#if __P_CONTROL

extern uint8_t P_control_activation_fl;
extern uint8_t P_control_activation_fr;
extern uint8_t P_control_activation_rl;
extern uint8_t P_control_activation_rr;

 
 
extern int8_t P_control_mode_fl;
extern int8_t P_control_mode_fr;
extern int8_t P_control_mode_rl;
extern int8_t P_control_mode_rr;

extern int16_t P_hold_threshold_f;
extern int16_t P_hold_threshold_r;

extern int16_t P_control_factor_fl;
extern int16_t P_control_factor_fr;
extern int16_t P_control_factor_rl;
extern int16_t P_control_factor_rr;

extern uint8_t P_approach_cnt_f;
extern uint8_t P_approach_cnt_r;
extern uint8_t P_pulse_down_dump_cnt_f;
extern uint8_t P_pulse_down_dump_cnt_r;

extern uint8_t P_on_the_target_f;
extern uint8_t P_on_the_target_r;
 
#endif

#if defined(__ADDON_TCMF)
extern uint8_t Booster_Act;
extern int16_t Booster_Act_cnt;
extern uint16_t Booster_Current;
#endif
/*Global Extern Functions Declaration ******************************************/
#if __P_CONTROL && __VDC
extern void LCESP_vControlValves(void);
#endif

#endif
