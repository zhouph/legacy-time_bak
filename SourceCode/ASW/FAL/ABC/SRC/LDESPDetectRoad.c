
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPDetectRoad
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LDESPEstBeta.h"
#include "LCESPCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCallMain.h"
#include "LCESPCalLpf.h"
#include "LSABSCallSensorSignal.H"
#include "LDESPDetectVehicleStatus.h"
#include "LDROPCallDetection.h"
#include "LDESPCalVehicleModel.h"

#if __VDC
void LDESP_vObserveRoad(void);
  
void DETECT_BANKED_ROAD(void);
void DETECT_ROAD_CURVATURE(void);
void DETECT_ROUGH_ROAD(void);
void CHECK_SPLIT_MU(void);
void ESP_MU_ESTIMATION(void);
void COMP_G_BODY(void);
void ESP_AFZ(void);
void COMP_ALAT_4_MU(void);     
void COMP_AFZ_4_MU(void);  
void LDESP_vCalLatAccAbsolute(void);
void LDESP_vEstRoadAdhesion(void);
void LDESP_vLimitYawRateDesired(void);
void LDESP_vCalLimitedYawRate(void);
void LDESP_vDetYawLimitEntCond(void);
void LDESP_vDetermineDesiredYawRate(void);
       
#define   	S16_BANK_HIGH                         		     200                                            	        	
#define   	S16_BANK_HOLD_WSTR                    		     400                                            	        	
#define   	S16_BANK_HOLD_WSTR_V                  		     400                                            	        	
#define   	S16_BANK_LOW                          		     150                                           	        	
#define   	S16_BANK_V_LOW                        		     150                                            	        	
#define   	S16_BANK_WSTR                         		     250                                            	        	
#define   	S16_BANK_WSTR_V                       		     250
#if ( __New_Mu_Level_Yaw_Limit==1 )

    #define S16_Time_TH_1st     L_U8_TIME_10MSLOOP_500MS
    #define S16_Time_TH_2nd     L_U8_TIME_10MSLOOP_2S     
    #define S16_Time_TH_Max     L_U16_TIME_10MSLOOP_4S
#endif

#if __Mu_Level_Adaptation

    int16_t alatm_dot_lf, alatm_dot_lf_old, alatm_dot_org ; 
    int16_t	alatm_dot10[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};    
    int16_t alatm_dot10_avr ;
    int32_t alatm_dot10_sum = 0; 
    uint8_t i, cnt_alatm_dot ;
#endif

  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
int16_t ay_vx_dot_sig_1st_th, ay_vx_dot_sig_2nd_th ;
int16_t ay_vx_dot_sig_1st_th_Mmu, ay_vx_dot_sig_2nd_th_Mmu ;
int16_t ab_rfm_gain;
  #endif
int16_t ldesps16YawMaxAlatm, ldesps16YawMaxAlatmSign;
int16_t ldesps16YawOffset ;
int16_t abrfm_new_old , abrfm_new, rf2_new  ;
int16_t ldesps16AxEst, ldesps16AxEst_old, Stack_Mu_delyaw ;
int16_t det_banked_angle_G_old, det_banked_angle_G ;

int16_t ldesps16DeltaLatGUnsigned;
int16_t ldesps16DeltaLatGOver;
int16_t ldesps16FiltdDeltaLatGOver;
int16_t ldesps16DeltaLatGUnder;
int16_t ldesps16FiltdDeltaLatGUnder;
int16_t pre_filling_enter_cond;

void LDESP_vObserveRoad(void)
{
/*********************************************************************/
/* Rough Road Check                                                  */
/*********************************************************************/

/*MISRA, H22*/
/*	Rough_road_detect();*/
	
#if	__VDC
    DETECT_ROUGH_ROAD();
    
/*********************************************************************/
/* Banked Road check                                                 */
/*********************************************************************/

    DETECT_BANKED_ROAD();
    DETECT_ROAD_CURVATURE();    
#endif

/*********************************************************************/
/*Hydro-planning Check                                               */
/*********************************************************************/
/*
#if __AQUA_ENABLE
    AQUALOCK();                         // determine aquaplanning
#endif
*/
/*********************************************************************/
/*Split-mu Check                                                     */
/*********************************************************************/
/*    GMA();
    LFC_Mu_Change_Recognition();
#if	__BTC
    BTCS_SPIN_CHECK();
#endif
*/
#if	__VDC
	CHECK_SPLIT_MU();
#endif

/*********************************************************************/
/* Mu Estimation                                                     */
/*********************************************************************/
#if	__VDC
	ESP_MU_ESTIMATION();
#endif

/*	DELTA_PRESS_CAL();*/
	
}

void DETECT_ROAD_CURVATURE(void)
{

    abs_yaw_rate_lf_old = abs_yaw_rate_lf;
    curv_radius_lf_old  = curv_radius_lf;
    
    abs_yaw_rate_lf = LCESP_s16Lpf1Int( McrAbs(yaw_out), abs_yaw_rate_lf_old, L_U8FILTER_GAIN_10MSLOOP_0_5HZ);      /* 0.01 */
        
    if ( abs_yaw_rate_lf < YAW_1DEG ) 
    {
        tempW0 = YAW_1DEG ;
    }
    else
    {
        tempW0 = abs_yaw_rate_lf;
    }                   

    if(vrefm <= 42 )            /* r : 0.1, 42*3.6 = 15 kph */
    {
        tempW2=42;
    }           
    else if(vrefm >= 556 )
    {            
        tempW2=556;
    }
    else 
    {            
        tempW2=vrefm;
    }           
        

	/* Calculate Curvature_Radius ... */
	/* 
	   180/pi = 57.2958 
	   R = V/YAW 
	*/

    /*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
    {*/
	tempW1 = 5730;  
    s16muls16divs16();
    curv_radius = tempW3 ;                                  /* r = 0.1 */
                
    if ( curv_radius > 15000 )   {
    	curv_radius = 15000 ;
    } 
    else 
    {
    	;
		}  
		    	
    /*}*/     

    curv_radius_lf = LCESP_s16Lpf1Int( curv_radius, curv_radius_lf_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);     
        
}

void ESP_AFZ(void) 
{
    
    int16_t i;
    
    afz_x1_old = afz_x1;

    for( i=0;i<4;i++ )
    {
        vrefk_b[4-i] = vrefk_b[3-i];
    }
    vrefk_b[0] = vrefk;
                                        /* 1000/7*10/36/9.8 = 4.05				*/
                                        /* ( 5 + 8 - 1 )/12						*/
                                        /* ( 5 y(k) + 3 y(k-1) - 9 y(k-2) + y(k-3) ) / 12 		*/
/*    
    tempL1 = (((int32_t)vrefk_b[0])*5);        
    tempL2 = (((int32_t)vrefk_b[1])*3);
    tempL3 = (((int32_t)vrefk_b[2])*9);
    tempL4 = (((int32_t)vrefk_b[3])*1);
        
*/

    tempW2 = 2025; 
    tempW1 = vrefk_b[0]; 
    tempW0 = 12;
    s16muls16divs16();
    esp_tempW1 = tempW3;

    tempW2 = 1215; 
    tempW1 = vrefk_b[1]; 
    tempW0 = 12;
    s16muls16divs16();
    esp_tempW2 = tempW3;

    tempW2 = 3645; 
    tempW1 = vrefk_b[2]; 
    tempW0 = 12;
    s16muls16divs16();
    esp_tempW3 = tempW3;

    tempW2 = 405; 
    tempW1 = vrefk_b[3]; 
    tempW0 = 12;
    s16muls16divs16();
    esp_tempW4 = tempW3;

    esp_tempW5 = ((esp_tempW1 + esp_tempW2) - esp_tempW3) + esp_tempW4;


    if ( afz_x1 > 0 ) 
    {

        afz_x1 = LCESP_s16Lpf1Int( esp_tempW5, afz_x1_old, L_U8FILTER_GAIN_10MSLOOP_0_5HZ);         
    } 
    else 
    {   
        afz_x1 = LCESP_s16Lpf1Int( esp_tempW5, afz_x1_old, 1);      /*  r : 0.001   */
    }

    esp_afz = afz_x1;
        
    if (esp_afz >= 850 ) 
    {
    	esp_afz = 850;
    }
    else if(esp_afz <= -850)
    {
    	esp_afz = -850;
    }
    else
    {
    	;
    }
    
}

void COMP_ALAT_4_MU(void)
{
    alat_4_mu_old=alat_4_mu;
    alat_4_mu = LCESP_s16Lpf1Int( alat, alat_4_mu_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);        
}

void COMP_AFZ_4_MU(void)
{
	#if (__4WD == ENABLE)|| (__AX_SENSOR == ENABLE )
        if((fu1AxErrorDet==0)&&(fu1AxSusDet==0))
        {    	
            afz_4_mu_old=afz_4_mu;
            afz_4_mu = LCESP_s16Lpf1Int( (along*10), afz_4_mu_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);        
        }
        else
        {
            afz_4_mu_old=afz_4_mu;
            afz_4_mu = LCESP_s16Lpf1Int( (afz*10), afz_4_mu_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);            	
        }
    #else
            afz_4_mu_old=afz_4_mu;
            afz_4_mu = LCESP_s16Lpf1Int( (afz*10), afz_4_mu_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);        
    #endif 
}

void ESP_MU_ESTIMATION(void)
{
    esp_mu_old = esp_mu;   
    esp_mu_k_old = esp_mu_k; 
    ax_old = ax;

    ESP_AFZ();
    COMP_ALAT_4_MU();     
    COMP_AFZ_4_MU();        
    COMP_G_BODY();        

    /*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
    {*/
    
    tempW4 = McrAbs(u_delta_yaw_thres);
        
    esp_tempW5 = LCESP_s16IInter2Point( det_wstr_dot_lf2, 1000, 130, 1500, 80 ); 
    esp_tempW6 = LCESP_s16IInter2Point( det_wstr_dot_lf2, 1000, 110, 1500, 80 );
    tempW7 = LCESP_s16IInter2Point( vrefk, 150, esp_tempW5, 600, esp_tempW6 );

    tempW2 = tempW4; 
    tempW1 = tempW7; 
    tempW0 = 100;
    s16muls16divs16();
    esp_tempW1 = tempW3;        

    tempW2 = tempW4; 
    tempW1 = tempW7+50; 
    tempW0 = 100;
    s16muls16divs16();
    esp_tempW2 = tempW3;          

    tempW2 = esp_tempW1; 
    tempW1 = 90; 
    tempW0 = 100;
    s16muls16divs16();
    esp_tempW0 = tempW3;
        
    esp_tempW4=85;
         
    if ((YAW_CDC_WORK==1) || (FLAG_DETECT_DRIFT==1))
    {        
        tempW2 = esp_tempW1; 
        tempW1 = esp_tempW4; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW1 = tempW3;        
        tempW2 = esp_tempW2; 
        tempW1 = esp_tempW4; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW2 = tempW3;          
        tempW2 = esp_tempW0; 
        tempW1 = esp_tempW4; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW0 = tempW3;               
    }
    else if (ESP_end_timer>L_U16_TIME_10MSLOOP_4S)
    {        
        esp_tempW5 = LCESP_s16IInter3Point(ESP_end_timer, L_U16_TIME_10MSLOOP_4S, 100, L_U16_TIME_10MSLOOP_4_5S, esp_tempW4, L_U16_TIME_10MSLOOP_5S, esp_tempW4);                 
        tempW2 = esp_tempW1;
        tempW1 = esp_tempW5 ; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW1 = tempW3;        
        tempW2 = esp_tempW2; 
        tempW1 = esp_tempW5 ;
         tempW0 = 100;
        s16muls16divs16();
        esp_tempW2 = tempW3;          
        tempW2 = esp_tempW0; 
        tempW1 = esp_tempW5 ; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW0 = tempW3;               
    }              
    else
    {
        ;
    }
                                      
    /*  Hysteresis   */   
    if ( det_delta_yaw > det_delta_yaw_limit ) 
    {
    	esp_tempW9 = det_delta_yaw;    
	}
	else 
	{
		esp_tempW9 = det_delta_yaw_limit;   
	}
             
    if ( esp_tempW9 < esp_tempW0 ) 
    {
        esp_mu_k = 1000;
    }
    else if ( esp_tempW9 < esp_tempW1  ) 
    {
        
        tempW2 = esp_tempW9-esp_tempW0; tempW1 = 1000; tempW0 = esp_tempW1-esp_tempW0;
        s16muls16divs16();
        esp_mu_k = 1000 - tempW3;           
        if ( esp_mu_k < esp_mu_k_old ) 
        {
        	esp_mu_k = esp_mu_k_old;
        }
        else
        {
        	;
        }
        
    } 
    else if (  esp_tempW9 < esp_tempW2 )  
    {
       
        tempW2 = esp_tempW9-esp_tempW1; tempW1 = 1000; tempW0 = esp_tempW2-esp_tempW1;
        s16muls16divs16();
        esp_mu_k = 1000 - tempW3;       
        if ( esp_mu_k > esp_mu_k_old ) 
        {
        	esp_mu_k = esp_mu_k_old;
        }
        else
        {
        	;
        }
    } 
    else 
    {
    	esp_mu_k = 0;
    }  
      
    if ((det_wstr_dot_lf2>1000)&&(esp_mu_k_old==0))
    {
    	esp_mu_k=0;    
    }
    else
    {
    	;
    }
		/*}*/   
    if((AFZ_OK==1) || (YAW_CDC_WORK==1))
    {
        tempW4 = (McrAbs(afz_4_mu)>=McrAbs(det_g_body))?McrAbs(afz_4_mu):McrAbs(det_g_body);
/*        tempW4 = det_g_body; */      
    }
    else if((ESP_end_timer>(L_U16_TIME_10MSLOOP_5S - L_U8_TIME_10MSLOOP_100MS)))
    {                                               
        esp_tempW5 = LCESP_s16IInter2Point(ESP_end_timer, (L_U16_TIME_10MSLOOP_5S - L_U8_TIME_10MSLOOP_100MS),0, L_U16_TIME_10MSLOOP_5S,100);                                 
        tempW2 = det_g_body; 
        tempW1 = 1000-esp_mu_k; 
        tempW0 = 1000;
        s16muls16divs16();
        tempW4 = esp_mu_k + tempW3;                                
        tempW4 = LCESP_s16IInter2Point( esp_tempW5,0,tempW4,100, det_g_body);                                        
    }       
    else 
    {
        tempW2 = det_g_body; 
        tempW1 = 1000-esp_mu_k; 
        tempW0 = 1000;
        s16muls16divs16();
        tempW4 = esp_mu_k + tempW3; 
    }
        
   #if __4WD_VARIANT_CODE==ENABLE	
    if((lsu8DrvMode !=DM_2WD)&&(fu1AxErrorDet==0)&&(fu1AxSusDet==0))
    {
        tempW4 = (tempW4>=det_alatm)?tempW4:det_alatm;
        tempW4 = (tempW4>=((abs(along))*10))?tempW4:((abs(along))*10);
    }
    else
    {
    	tempW4 = (tempW4>=det_alatm)?tempW4:det_alatm;
        tempW4 = (tempW4>=abs(ax))?tempW4:abs(ax);
    }
  #else   
      #if (__4WD == ENABLE) || (__AX_SENSOR == ENABLE)
    	if((fu1AxErrorDet==0)&&(fu1AxSusDet==0))
    	{    
    	    tempW4 = (tempW4>=det_alatm)?tempW4:det_alatm;
    	    tempW4 = (tempW4>=((McrAbs(along))*10))?tempW4:((McrAbs(along))*10);
    	}
    	else
    	{
    		tempW4 = (tempW4>=det_alatm)?tempW4:det_alatm;
    	    tempW4 = (tempW4>=McrAbs(ax))?tempW4:McrAbs(ax);
    	}  
	  #else
    	tempW4 = (tempW4>=det_alatm)?tempW4:det_alatm;
    	tempW4 = (tempW4>=abs(ax))?tempW4:abs(ax);
	  #endif
  #endif

    
    tempW4=LCESP_s16LimitInt(tempW4,150,850);

    if ( tempW4 < esp_mu_old )  
    {
    	esp_mu = LCESP_s16Lpf1Int(tempW4, esp_mu_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);          
    }
    else 
    {
        if (det_alatm>esp_mu_old)       
        {
        	esp_mu = LCESP_s16Lpf1Int(tempW4, esp_mu_old, L_U8FILTER_GAIN_10MSLOOP_18HZ);    
        }
        else 
        {
        	esp_mu = LCESP_s16Lpf1Int(tempW4, esp_mu_old, L_U8FILTER_GAIN_10MSLOOP_6_5HZ);               
        }                  
    }
        
}
void CHECK_SPLIT_MU(void)
{
    tempW0=yaw_out;
    if(yaw_out<0) 
    {
    	tempW0=-yaw_out;
    }
    else
    {
    	;
    }
    
    tempW1=wstr;
    if(wstr<0) 
    {
    	tempW1=-wstr;
    }
    else
    {
    	;
    }
    
    tempW2=delta_yaw;
    if(delta_yaw<0) 
    {
    	tempW2=-delta_yaw;
    }
    else
    {
    	;
    }
    
    tempF0=0;
    if(rf>0) 
    {
        if(yaw_out<0) 
        {
        	tempF0=1;
        }
        else
        {
        	;
        }
    }
    else 
    {
        if(yaw_out>0) 
        {
        	tempF0=1;
        }
        else
        {
        	;
        }
    }

    if((BLS_K==1)&&(abs_delta_yaw<=YAW_1DEG)) 
    {
        initial_yaw_out=abs_yaw_out;
        initial_alatm=det_alatm;
    }
    else
    {
    	;
    }
    if((BRAKE_SIGNAL==0)&&(BLS==0)&&(ESP_SPLIT==0)) 
    {
        initial_yaw_out=0;
        initial_alatm=0;
    }
    else
    {
    	;
    }
    tempW3=McrAbs(abs_yaw_out-initial_yaw_out);
    tempW4=McrAbs(det_alatm-initial_alatm);
    
    if(ESP_SPLIT==0) 
    {

        if(BRAKE_SIGNAL==1) 
        {
/*            if(((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1))&&(tempW4<LAT_0G2G))  '06 TRC */ 
            if((LFC_Split_flag==1)&&(tempW4<LAT_0G2G)) 
            {

                if(YAW_CDC_WORK==0)
                {
                	ESP_SPLIT=1;
                }
                else
                {
                	;
                }
            }
            else
            {
            	;
            }
        }
        else 
        {
            if((TCS_SPLIT==1)&&(det_alatm<LAT_0G2G)) 
            {
                if(ESP_ON==0)
                {
                	ESP_SPLIT=1;
                }
                /******************************************/
                /*  for BTCS+ESP by Kim Jeonghun in 2004.3.23*/
                /******************************************/
                else if((YAW_CDC_WORK==0) && (esp_on_counter_1>L_U8_TIME_10MSLOOP_1000MS))
                {
                	ESP_SPLIT=1;
                }
                else
                {
                	;
                }
            }
            else
            {
            	;
            }
        }
    }
    else
    {
    	;
    }
    
    if(ESP_SPLIT==1)
    {
        if(BRAKE_SIGNAL==1) 
        {
          if(BTC_fz==0) 
          {
            #if defined( __UCC_1)
            if(((LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))&&(ABS_fz==0))
            #else
            if(((LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))||(ABS_fz==0))
            #endif
            {
            	ESP_SPLIT=0;
            }
            else
            {
            	;
            }
          }
          else
          {
          	;
          }
          /*************************************************************/
          /*    for BTCS+ESP by Kim Jeonghun in 2004.3.23*/
          /*************************************************************/
          if(((tempW3>YAW_10DEG)||(tempW4>LAT_0G2G))&&(delta_yaw>=1000)) 
          {
          	ESP_SPLIT=0; 
          }
          else
          {
          	;
          }
        
        }
        else 
        {
          if(ABS_fz==0) 
          {
            if((TCS_SPLIT==0)||(BTC_fz==0)) 
            {
            	ESP_SPLIT=0;
            }
            else
            {
            	;
            }
          }
          else
          {
          	;
          }
          /**********************************************************/
          /*    for BTCS+ESP by Kim Jeonghun in 2004.3.23			*/
          /**********************************************************/    
          if((tempW3>YAW_20DEG)||(tempW4>LAT_0G2G)) 
          {
          	ESP_SPLIT=0;           
          }
          else
          {
          	;
          }       
        }
    }
    else
    {
    	;
    }
}

void DETECT_BANKED_ROAD( void )
{
/*  vx_r_lf_old = vx_r_lf;
    vx_r_lf2_old = vx_r_lf2;
    alat_lf_old = alat_lf;

    tempW2 = yaw_out;  
    tempW1 = vrefm;  
    tempW0 = 562;
    s16muls16divs16();
    vx_r = tempW3;      

    vx_r_lf = LCESP_s16Lpf1Int( vx_r, vx_r_lf_old, 8);

    vx_r_lf2 = LCESP_s16Lpf1Int( vx_r_lf, vx_r_lf2_old, 3);

    alat_lf = LCESP_s16Lpf1Int( alat, alat_lf_old, 3);

// 05 China : Delta_steering_angle Cal.    05/02/19       

    if(vrefm >= 83) 
    {
    	esp_tempW6=vrefm;
    }
    else 
    {
    	esp_tempW6=83;
    }

    tempW2 = vx_r_lf - alat_lf; 
    tempW1 = 98; 
    tempW0 = 100 ;
    s16muls16divs16();
    esp_tempW1 = tempW3;        //  0.01 , m/sec2        

    tempW2 = yaw_out; 
    tempW1 = esp_tempW6; 
    tempW0 = 573 ;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    
    tempW2 = esp_tempW2 - esp_tempW1 ; 
    tempW1 = model_const_for_delta_front; 
    tempW0 = 10 ;
    s16muls16divs16();
    esp_tempW3 = tempW3;    
    
    tempW2 = (Lf_beta + Lr_beta); 
    tempW1 = yaw_out; 
    tempW0 = esp_tempW6 ;
    s16muls16divs16();
    esp_tempW4 = tempW3;
    
    tempW2 = esp_tempW4; 
    tempW1 = Stratio_beta; 
    tempW0 = 10 ;
    s16muls16divs16();
    esp_tempW5 = tempW3;
    
    delta_front_wstr  = esp_tempW3 + esp_tempW5;            // r = 0.01 
    abs_delta_front_wstr = (abs(delta_front_wstr))/10;      // r = 0.1    

    delta_banked_angle = abs( vx_r_lf2 -  alat_lf );   //    | vx*r - alat |    
    
  //--------------- Improved Bank Detection at 2005.12.2 by J.K. Lee -------------------
  
  #if __MGH40_Bank_Estimation // Monitoring Flag for comparison of MGH25 and MGH40 
  
    if(vref5 <= VREF_5_KPH )
    {
  	    Bank_Susp_MGH25_Flag = 0;
  	    Bank_Comp_MGH25_Flag = 0;  	
    }
    else
    {
    	;
    }
  
    FLAG_BANK_SUSPECT  = Bank_Susp_MGH25_Flag;  // MGH25 Monitoring Flag 
    FLAG_BANK_DETECTED = Bank_Comp_MGH25_Flag;  // MGH25 Monitoring Flag 
    
  #endif
  
  //------------------------------------------------------------------------------------
    
    if (((( vx_r_lf2 >= 0 )&&( alat_lf >= 150 ))||(( vx_r_lf2 <= 0 )&&( alat_lf <= -150 )) )
        && ( vrefk < 800 ))
    {
        esp_tempW9 = 1;
    } 
    else 
    {
        esp_tempW9 = -1;        // Detection condition
    }

    
// 2004/06/10 JM BANK Detection --> more sensitive according to Vehicle Speed 

    #if ( __CAR==HMC_JM ) || ( __CAR==KMC_KM )            
        esp_tempW1 = S16_BANK_WSTR_V ; 
        esp_tempW2 = LCESP_s16IInter2Point(vrefk, 1000, S16_BANK_WSTR, 1100, S16_BANK_WSTR + 100 );     
        esp_tempW5 = LCESP_s16IInter2Point(vrefk, 1000, S16_BANK_HIGH, 1100, S16_BANK_HIGH - 20 );     
        esp_tempW7 = S16_BANK_HOLD_WSTR ;
    #elif ( __CAR==KMC_HM ) || ( __CAR==FORD_C214 )  
        esp_tempW1 = S16_BANK_WSTR_V ; 
        esp_tempW2 = LCESP_s16IInter2Point(vrefk, 1000, S16_BANK_WSTR, 1100, S16_BANK_WSTR + 100 );     
        esp_tempW5 = LCESP_s16IInter2Point(vrefk, 1000, S16_BANK_HIGH, 1100, S16_BANK_HIGH - 20 );     
        esp_tempW7 = LCESP_s16IInter2Point(vrefk, 800, S16_BANK_HOLD_WSTR + 200, 900, S16_BANK_HOLD_WSTR ); // '06.04.20 : HMC Small-bank 
    #else                                     
        esp_tempW1 = S16_BANK_WSTR_V ; 
        esp_tempW2 = S16_BANK_WSTR ; 
        esp_tempW5 = S16_BANK_HIGH ; 
        esp_tempW7 = S16_BANK_HOLD_WSTR ;        
    #endif

    if ( FLAG_BANK_DETECTED ==1) 
    {
        tempW2 = S16_BANK_V_LOW ;  
        tempW1 = 95;  
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW3 = tempW3;           
        esp_tempW6 =LCESP_s16IInter2Point(delta_banked_angle,S16_BANK_LOW,S16_BANK_HOLD_WSTR,S16_BANK_HIGH,S16_BANK_WSTR);         

        if ( vrefk <= esp_tempW3 )  
        {
            if( counter_disable_banked < T_750MS)
            {
            	counter_disable_banked=counter_disable_banked+1;
            }
			else
			{
				;
			}
            if ( counter_disable_banked > T_70MS ) 
            {
                FLAG_BANK_SUSPECT = 0 ;                        
                FLAG_BANK_DETECTED = 0;
                counter_disable_banked = T_70MS;
            }
            else
            {
            	;
            }

        } 
        else if ((delta_banked_angle < (esp_tempW5-50))&&( det_wstr_dot_lf <= esp_tempW1 )&&(abs_wstr <= esp_tempW6)) 
        {

            if( counter_disable_banked < T_750MS)
            {
            	counter_disable_banked=counter_disable_banked+1;
            }
            else
            {
            	;
            }

            if ( counter_disable_banked > T_100MS ) 
            {
                FLAG_BANK_SUSPECT = 0 ;                        
                FLAG_BANK_DETECTED = 0;
                counter_disable_banked = T_100MS;
            }
            else
            {
            	;
            }
        }
        else if  (  ( delta_banked_angle < (S16_BANK_HIGH - 100)) && ( vrefk < 800 ) )  
        {   // 04 SWD winter  +  04/ HMC tune occur !

            if( counter_disable_banked < T_750MS) 
            {
            	counter_disable_banked=counter_disable_banked+1;
			}
			else
			{
				;
			}
            if ( counter_disable_banked > T_100MS ) 
            {
                FLAG_BANK_SUSPECT = 0 ;                        
                FLAG_BANK_DETECTED = 0;
                counter_disable_banked = T_100MS;
            }
            else
            {
            	;
            }

        } 
        else 
        {
            if( counter_disable_banked > 0 )
            {
            	counter_disable_banked=counter_disable_banked-1;
            }
            else
            {
            	;
            }
        }

        counter_enable_banked = 0;

    	counter_hold_banked = 0;

    } 
    else
    {        
        // HOLD CONDITION 
        if ( (vrefk > S16_BANK_V_LOW) 
            && ( delta_yaw_first >= 0 )                       // '06 SWD  
            && ( esp_tempW9 < 0 )
            && ( delta_banked_angle >= S16_BANK_LOW )
            && ( det_wstr_dot_lf <= S16_BANK_HOLD_WSTR_V ) 
            && ( abs_wstr <= esp_tempW7 )
            && ( (abs((delta_front_wstr/10) - wstr)) <= (WSTR_15_DEG + (abs_wstr/3))) ) 
            {       //  Offset margin ?             
                
            if ( counter_hold_banked < T_200MS ) 
            {
            	counter_hold_banked=counter_hold_banked+1;
            }
            else
            {
            	;
            }
            if ( counter_hold_banked > T_100MS )
            {
                FLAG_BANK_SUSPECT = 1 ;                
                counter_hold_banked = T_100MS;
            }
            else
            {
            	;
            }
        } 
        else 
        {
            if( counter_hold_banked > 0 ) 
            {
            	counter_hold_banked=counter_hold_banked-1;      
            }
            else
            {
            	;
            }      
	        if ( (FLAG_BANK_SUSPECT==1)  && ( counter_hold_banked < T_84MS )) 
	        {	 
                FLAG_BANK_SUSPECT = 0 ;                
            } 
            else
            {
            	;
            }      
        }
      
        // DISABLE CONDITION 
        if ( (vrefk > S16_BANK_V_LOW) 
            && ( delta_yaw_first >= 0 )                       // '06 SWD   
            && ( esp_tempW9 < 0 )     
            && ( delta_banked_angle >= esp_tempW5 )
            && ( det_wstr_dot_lf <= esp_tempW1 ) 
            && ( abs_wstr <= esp_tempW2 )
            && ( (abs((delta_front_wstr/10) - wstr)) <= (WSTR_10_DEG + (abs_wstr/3)) ) ) 
        {       // Offset margin : 10 deg ?   
                            
            if( counter_enable_banked < T_750MS) 
            {
            	counter_enable_banked=counter_enable_banked+1;
            }
            else
            {
            	;
            }

            if ( counter_enable_banked > T_500MS ) 
            {
                FLAG_BANK_DETECTED = 1;
                counter_enable_banked = T_500MS;
            }
            else
            {
            	;
            }
        } 
        else 
        {
            if( counter_enable_banked > 0 ) 
            {
            	counter_enable_banked=counter_enable_banked-1;
            }
            else
            {
            	;
            }
        }
        counter_disable_banked = 0;
    }
    
  //--------------- Improved Bank Detection at 2005.12.2 by J.K. Lee -------------------
  
  #if __MGH40_Bank_Estimation
  
    Bank_Susp_MGH25_Flag = FLAG_BANK_SUSPECT; // MGH25 Monitoring Flag 
    Bank_Comp_MGH25_Flag = FLAG_BANK_DETECTED;// MGH25 Monitoring Flag 
*/    
    FLAG_BANK_SUSPECT  = Bank_Susp_Flag;                        
    FLAG_BANK_DETECTED = Bank_Comp_Flag;

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/    
    tempW2 = Bank_Angle_Final; 
    tempW1 = 100;   
    tempW0 = 573;
    s16muls16divs16();
    delta_banked_angle = McrAbs(tempW3);
/*}*/    
/*  #endif */  
  
  /*------------------------------------------------------------------------------------*/
}

/*---------------------------------------------------------------------*/
void DETECT_ROUGH_ROAD(void)
{      
/*  hlatm = 0.735*hlatm_old + ( alat-latm_old);  */
/*  llatm = 120/128*llatm_old + 8/128*latm_old;  */
/*************************************************/   
    tempW2 = 94; 
    tempW1 = high_pass_latm_old; 
    tempW0 = 128;
    s16muls16divs16();
    esp_tempW0 = tempW3;   
    esp_tempW1= (alat-latm_old[0])*10;
    high_pass_latm=esp_tempW0+esp_tempW1;
    
    tempW2 = 120; 
    tempW1 = low_pass_latm_old; 
    tempW0 = 128;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    tempW2 = 8; 
    tempW1 = latm_old[0]; 
    tempW0 = 128;
    s16muls16divs16();
    esp_tempW3 = tempW3;
    low_pass_latm=esp_tempW2+esp_tempW3;
    
    if ( det_high_pass_latm_old < McrAbs(high_pass_latm_old) )
    {       
/*      det_hlatm =  255/256*det_hlatm_old + 1/256*abs(hlatm_old);*/

        tempW2 = 255; 
        tempW1 = det_high_pass_latm_old; 
        tempW0 = 256;
        s16muls16divs16();
        esp_tempW4 = tempW3;
        
        tempW2 = 1; 
        tempW1 = McrAbs(high_pass_latm_old);
        tempW0 = 128;
        s16muls16divs16();
        esp_tempW5 = tempW3;
        
        det_high_pass_latm=esp_tempW4+esp_tempW5;
       
    }
    else
    {
/*      det_hlatm =  122/128*det_hlatm_old + 6/128*abs(hlatm_old);*/
        tempW2 = 122; 
        tempW1 = det_high_pass_latm_old; 
        tempW0 = 128;
        s16muls16divs16();
        esp_tempW4 = tempW3;
        
        tempW2 = 6; 
        tempW1 = McrAbs(high_pass_latm_old);
        tempW0 = 128;
        s16muls16divs16();
        esp_tempW5 = tempW3;
        
        det_high_pass_latm=esp_tempW4+esp_tempW5;
    }
              
    if ( det_low_pass_latm_old < McrAbs(low_pass_latm_old) )
    {
/*      det_llatm =  255/256*det_llatm_old + 1/256*abs(llatm_old);*/
        tempW2 = 255; 
        tempW1 = det_low_pass_latm_old ; 
        tempW0 = 256;
        s16muls16divs16();
        esp_tempW6 = tempW3;
        
        tempW2 = 1; 
        tempW1 = McrAbs(low_pass_latm_old);
        tempW0 = 256;
        s16muls16divs16();
        esp_tempW7 = tempW3;
        
        det_low_pass_latm=esp_tempW6+esp_tempW7;
    }
    else
    {
/*      det_llatm =  122/128*det_llatm_old + 6/128*abs(llatm_old);*/
        tempW2 = 122; 
        tempW1 = det_low_pass_latm_old ; 
        tempW0 = 128;
        s16muls16divs16();
        esp_tempW6 = tempW3;
        
        tempW2 = 6; 
        tempW1 = McrAbs(low_pass_latm_old);
        tempW0 = 128;
        s16muls16divs16();
        esp_tempW7 = tempW3;
        
        det_low_pass_latm=esp_tempW6+esp_tempW7;
  
    }    
    esp_tempW9 = LCESP_s16FindRoadDependatGain(esp_mu,400,100,50);
    
     if( ( (ESP_ROUGH_ROAD==0)&&( (FL.Rough_road_detector>10) || (FR.Rough_road_detector>10) ) )
        || ( (ESP_ROUGH_ROAD==1)&&( (FL.Rough_road_detector>5) || (FR.Rough_road_detector>5) ) ))
     {
        ESP_ROUGH_ROAD = 1;
     }    
     else if( ( (ESP_ROUGH_ROAD==0) && (det_high_pass_latm>esp_tempW9) )
             || ( (ESP_ROUGH_ROAD==1) && (det_high_pass_latm>( esp_tempW9-30) ) ) )
     {
        ESP_ROUGH_ROAD = 1;
     }
     else 
     {    
        ESP_ROUGH_ROAD = 0; 
     }     
   delta_alat[0] = McrAbs(alat) - McrAbs(latm_old[0]);
   esp_tempW7 = delta_alat[1]*delta_alat[0];
   esp_tempW8 = delta_alat[1]*delta_alat_old;   
   esp_tempW6 = (esp_tempW7*esp_tempW8)/1000;
   
   if(esp_tempW6 < 0)
   {
        TEMP_ESP_ALAT=1;
   }
   else
   {
        TEMP_ESP_ALAT=0;
   }
     
   high_pass_latm_old=high_pass_latm;
   low_pass_latm_old=low_pass_latm;
   det_high_pass_latm_old=det_high_pass_latm;
   det_low_pass_latm_old=det_low_pass_latm;
   delta_alat[1] = delta_alat[0];
   delta_alat_old = delta_alat[1]; 
   latm_old[0]=alat;
   latm_old[1]=latm_old[0];
}

void COMP_G_BODY(void)
{

    det_g_body_old=det_g_body;
    det_g_body_4_lf_old=det_g_body_4_lf;
    
#if __4WD_VARIANT_CODE==ENABLE	
    if((lsu8DrvMode !=DM_2WD)&&(fu1AxErrorDet==0)&&(fu1AxSusDet==0)) 
    {
        g_body = (int16_t)(LCESP_u16CompSqrt((uint16_t)(abs(alat_4_mu)), (uint16_t)(abs(along))) );
    }
    else
    {
      #if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
        esp_tempW6 = LCESP_s16IInter2Point( lsesps16AHBGEN3mpress,80,30,150,100);
      #else
        esp_tempW6 = LCESP_s16IInter2Point( mpress,80,30,150,100); 
      #endif
        /*esp_tempW1 = (int16_t)( (((int32_t)filter_out1)*esp_tempW6)/100 );            */
        esp_tempW1 = (int16_t)( (((int32_t)FZ1.lss16VrefVehAccel)*esp_tempW6)/100 );   
        
        if ((ABS_fz==1)||(ESP_ROUGH_ROAD==1)||(ACT_TWO_WHEEL==1)||(FLAG_BIG_OVERSTEER==1)||(FLAG_DETECT_DRIFT==1))
        {
        	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_0_5HZ;      /*  1.1 Hz   */
        }
        else if (YAW_CDC_WORK==1)
        {
        	esp_tempW5=5;
        }
        else 
        {
        	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_3_5HZ;    /*  7   Hz  */
        }
        ax = LCESP_s16Lpf1Int( abs(esp_tempW1), ax_old, (uint8_t)esp_tempW5);                                                                     
        g_body = (int16_t)(LCESP_u16CompSqrt((uint16_t)(abs(alat_4_mu)), (uint16_t)(ax+5)/10));     /* r : 0.001 */    
    }
#else   
  #if (__4WD == ENABLE) || (__AX_SENSOR == ENABLE)
    if((fu1AxErrorDet==0)&&(fu1AxSusDet==0))
    {
        g_body = (int16_t)(LCESP_u16CompSqrt((uint16_t)(McrAbs(alat_4_mu)), (uint16_t)(McrAbs(along))) );
    }
    else
    {
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        esp_tempW6 = LCESP_s16IInter2Point( lsesps16AHBGEN3mpress,80,30,150,100); 
      #else  
        esp_tempW6 = LCESP_s16IInter2Point( mpress,80,30,150,100); 
      #endif  
        /*tempW2 = filter_out1;   */
        tempW2 = FZ1.lss16VrefVehAccel;
        tempW1 = esp_tempW6; 
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW1 = tempW3;            
        
        if ((ABS_fz==1)||(ESP_ROUGH_ROAD==1)||(ACT_TWO_WHEEL==1)||(FLAG_BIG_OVERSTEER==1)||(FLAG_DETECT_DRIFT==1))
        {
        	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_0_5HZ;      /*  1.1 Hz   */
        }
        else if (YAW_CDC_WORK==1)
        {
        	esp_tempW5=5;
        }
        else 
        {
        	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_3_5HZ;    /*  7   Hz  */
        }
        ax = LCESP_s16Lpf1Int( McrAbs(esp_tempW1), ax_old, (uint8_t)esp_tempW5);
        g_body = (int16_t)(LCESP_u16CompSqrt((uint16_t)(McrAbs(alat_4_mu)), (uint16_t)(ax+5)/10));     /* r : 0.001 */
    }
  #else
   #if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
    esp_tempW6 = LCESP_s16IInter2Point( lsesps16AHBGEN3mpress,80,30,150,100);  
   #else
    esp_tempW6 = LCESP_s16IInter2Point( mpress,80,30,150,100); 
   #endif 
    /*tempW2 = filter_out1;  */
    tempW2 = FZ1.lss16VrefVehAccel;
    tempW1 = esp_tempW6; 
    tempW0 = 100;
    s16muls16divs16();
    esp_tempW1 = tempW3;            
    
    if ((ABS_fz==1)||(ESP_ROUGH_ROAD==1)||(ACT_TWO_WHEEL==1)||(FLAG_BIG_OVERSTEER==1)||(FLAG_DETECT_DRIFT==1))
    {
    	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_0_5HZ;      /*  1.1 Hz   */
    }
    else if (YAW_CDC_WORK==1)
    {
    	esp_tempW5=5;
    }
    else 
    {
    	esp_tempW5=L_U8FILTER_GAIN_10MSLOOP_3_5HZ;    /*  7   Hz  */
    }
    ax = LCESP_s16Lpf1Int( abs(esp_tempW1), ax_old, (uint8_t)esp_tempW5);                                                                     
    g_body = (int16_t)(LCESP_u16CompSqrt((uint16_t)(abs(alat_4_mu)), (uint16_t)(ax+5)/10));     /* r : 0.001 */    
  #endif
#endif             
           
    if(det_g_body<=g_body)
    {
        det_g_body=g_body;
        det_g_body_4_lf=g_body<<4;
    }
    else
    { 
        esp_tempW5 = LCESP_s16IInter2Point( vref5, 400, 2, 560, 2); 
        if (YAW_CDC_WORK==0) 
        {
        	esp_tempW5 = 1;
        }
        else
        {
        	;
        }
        det_g_body_4_lf=g_body<<4;
        det_g_body_4_lf=LCESP_s16Lpf1Int1024(det_g_body_4_lf, det_g_body_4_lf_old, (uint8_t)esp_tempW5); 
        det_g_body=det_g_body_4_lf>>4;          
    }
}

void LDESP_vCalLatAccAbsolute(void)
{
/*----------------------------------------------------------------*/
/* DET_ALATC                                                      */
/*----------------------------------------------------------------*/

    if(nor_alat<0) 
    {
    	t_det_alatc=-nor_alat;
    }
    else 
    {
    	t_det_alatc=nor_alat;
    }

    det_alatc_old=det_alatc;
    
    if((!FLAG_COUNTER_STEER)&&(McrAbs(wstr)<=500)&&(McrAbs(yaw_out)<=600)&&(det_alatm>=100))
	{
		det_alat_dec = 3;
	}
	else if((!FLAG_COUNTER_STEER)&&(det_beta_dot <= 200)&&(det_alatm>=100))
	{
		det_alat_dec = 2;		
	}
	else
	{
		det_alat_dec = 1;		
	}
	
	if (( det_alat_dec !=1)&&(det_alatc <= ( det_alatm + S16_YAW_LIMIT_DELTA_LOW_FAST))) 
	{
		det_alat_dec =1;
	}
	else 
	{    
		;
	}    

    if(t_det_alatc<det_alatc)
    {
        /*실제의 계산치가 det_alatc보다 작을때 det_alatc의 크기에 따라서 감소하는 횟수를 정하는 부분*/

        dec_alatc_count++;

        if(det_alatc>LAT_0G4G)
        {
/*            if(dec_alatc_count>=1)
            {*/
                tempW4=det_alatc_old-det_alat_dec;
                dec_alatc_count=0;
/*            }
            else 
            {
            	tempW4=det_alatc_old;
            }*/
        }
        else if(det_alatc>LAT_0G2G)
        {
            if(dec_alatc_count>=2)
            {
                tempW4=det_alatc_old-1;
                dec_alatc_count=0;
            }
            else 
            {
            	tempW4=det_alatc_old;
            }

        }
        else
        {
            if(dec_alatc_count>=5)
            {
                tempW4=det_alatc_old-1;
                dec_alatc_count=0;
            }
            else 
            {
            	tempW4=det_alatc_old;
            }

        }
        tempW3=0;
    }
    else 
    {
        /*절대값으로 변경된 계산치가 증가하여 det_alatc보다 클때 수행되는 부분*/
        dec_alatc_count=0; //10msec 고치기

        tempW2=t_det_alatc;
        tempW1=84;
        tempW0=100;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatc_old;
        tempW1=16;
        tempW0=100;
        u16mul16div16();
    }

	/* 04/28 Kim CJ*/
    if (SAS_CHECK_OK==1)
    { 
        det_alatc=tempW4+tempW3;
    } 
    else 
    {
        det_alatc=0;
    }
    
    /*  '06.04.06 , Filter adjustment : det_alatc_fltr */
    
    det_alatc_fltr_old = det_alatc_fltr ;
    
    if(t_det_alatc<det_alatc_fltr) //10msec 고치기
    {    
        esp_tempW0 = LCESP_s16IInter3Point(det_wstr_dot, 500, 15,   /* '11. BMW Demo */
                                                        1000, 5,  
                                                        2000, 1 );   
        tempW2=t_det_alatc;
        tempW1=esp_tempW0;
        tempW0=427;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatc_fltr_old;
        tempW1=427-esp_tempW0;
        tempW0=427;
        u16mul16div16();  
        
        det_alatc_fltr=tempW4+tempW3;        
    }
    else
    {
        tempW2=t_det_alatc;
        tempW1=43;
        tempW0=128;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatc_fltr_old;
        tempW1=85;
        tempW0=128;
        u16mul16div16();  
        
        det_alatc_fltr=tempW4+tempW3;            
    }

/*----------------------------------------------------------------*/
/* DET_ALATM                                                      */
/*----------------------------------------------------------------*/

    if(alat<0) 
    {
    	t_det_alatm=-alat;
    }
    else 
    {
    	t_det_alatm=alat;
    }

    det_alatm_old=det_alatm;
    if(t_det_alatm<det_alatm)
    {
        dec_alatm_count++;
        if(det_alatm>LAT_0G4G)
        {
/*            if(dec_alatm_count>=1)
            {*/
                tempW4=det_alatm_old-1;
                dec_alatm_count=0;
/*            }
            else 
            {
            	tempW4=det_alatm_old;
            }*/
        }
        else if(det_alatm>LAT_0G2G)
        {
            if(dec_alatm_count>=2)
            {
                tempW4=det_alatm_old-1;
                dec_alatm_count=0;
            }
            else 
            {
            	tempW4=det_alatm_old;
            }

        }
        else
        {
            if(dec_alatm_count>=5)
            {
                tempW4=det_alatm_old-1;
                dec_alatm_count=0;
            }
            else 
            {
            	tempW4=det_alatm_old;
            }
        }

        tempW3=0;
/*
        tempW2=t_det_alatm;
        tempW1=3;
        tempW0=1000;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatm_old;
        tempW1=997;
        tempW0=1000;
        u16mul16div16();
*/
    }
    else 
    {
        dec_alatm_count=0;

        tempW2=t_det_alatm;  //10msec 고치기
        tempW1=84;
        tempW0=100;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatm_old;
        tempW1=16;
        tempW0=100;
        u16mul16div16();
    }
    det_alatm=tempW4+tempW3;
    
#if (__IDB_RIG_TEST==ENABLE)
	
	tempW3 = LCESP_s16IInter3Point(vref, VREF_25_KPH, 10,
								   VREF_50_KPH, 15,
								   VREF_100_KPH, 20);
	
	
	tempW1 = LCESP_s16IInter5Point(abs_wstr, STEER_15DEG, LAT_0G05G,
												STEER_100DEG, LAT_0G25G,
												STEER_200DEG, LAT_0G45G,
												STEER_300DEG, LAT_0G6G,
												STEER_450DEG, LAT_0G65G);
	det_alatm = (int16_t)((int32_t)tempW3*tempW1/10);
#endif
    
    /*  '06.04.06 , Filter adjustment : det_alatm_fltr */
    
    det_alatm_fltr_old = det_alatm_fltr ;
    
    if(t_det_alatm<det_alatm_fltr)  //10msec 고치기
    {    
        esp_tempW0 = LCESP_s16IInter3Point(det_wstr_dot, 500, 15,   /* '11. BMW Demo */
                                                        1000, 5,  
                                                        2000, 1 );   
        tempW2=t_det_alatm;
        tempW1=esp_tempW0;
        tempW0=427;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatm_fltr_old;
        tempW1=427-esp_tempW0;
        tempW0=427;
        u16mul16div16();  
        
        det_alatm_fltr=tempW4+tempW3;        
    }
    else
    {
        tempW2=t_det_alatm;
        tempW1=43;
        tempW0=128;
        u16mul16div16();
        tempW4=tempW3;

        tempW2=det_alatm_fltr_old;
        tempW1=85;
        tempW0=128;
        u16mul16div16();  
        
        det_alatm_fltr=tempW4+tempW3;            
    }    

/***********************************************************************/
/* Calculation of Delta_Alat Measured                                  */
/***********************************************************************/
/*
    if(alat<0) tempW0=-(int16_t)alat;
    else tempW0=alat;
    if(nor_alat<0) tempW1=-(int16_t)nor_alat;
    else tempW1=nor_alat;
    tempW2=tempW0-tempW1;
    delta_lat=tempW2;    */   /* r : 0.001 */
    
    delta_lat_old=delta_lat;
    delta_lat=alat-nor_alat;

/***********************************************************************/
/* Calculation of delta_lat_diff                                       */
/***********************************************************************/

    if(delta_lat<0) 
    {
    	tempW0=-(int16_t)delta_lat;
    }
    else 
    {
    	tempW0=delta_lat;
    }
    if(delta_lat_old<0) 
    {
    	tempW1=-(int16_t)delta_lat_old;
    }
    else 
    {
    	tempW1=delta_lat_old;
    }

    delta_lat_diff=tempW0-tempW1;

#if ( __DELTA_DET_AY_MOD==1 )

    /*  '06.04.20 , delta_det_lat 결정 방법 */
    
    /*esp_tempW0 = t_det_alatc - t_det_alatm ;*/
    /*esp_tempW0 = abs(nor_alat-alat);*/
    if(alat<0)
    {
    	ldesps16DeltaLatGUnsigned = -(nor_alat-alat);
    }
    else
    {
    	ldesps16DeltaLatGUnsigned = (nor_alat-alat);
    }
    
    if(ldesps16DeltaLatGUnsigned>=0)
    {
    	ldesps16DeltaLatGOver=ldesps16DeltaLatGUnsigned;
    	ldesps16DeltaLatGUnder=0;
    }
    else
    {
    	ldesps16DeltaLatGOver=0;
    	ldesps16DeltaLatGUnder=ldesps16DeltaLatGUnsigned;
    }

    if(McrAbs(ldesps16DeltaLatGOver)<McrAbs(ldesps16FiltdDeltaLatGOver))
    {    
        tempW2=ldesps16DeltaLatGOver;
        tempW1=1;
        tempW0=128;
        s16muls16divs16();
        tempW4=tempW3;

        tempW2=ldesps16FiltdDeltaLatGOver;
        tempW1=127;
        tempW0=128;
        s16muls16divs16();           
    }
    else
    {
        tempW2=ldesps16DeltaLatGOver;
        tempW1=43;
        tempW0=128;
        s16muls16divs16();
        tempW4=tempW3;

        tempW2=ldesps16FiltdDeltaLatGOver;
        tempW1=85;
        tempW0=128;
        s16muls16divs16();           
    }  
    ldesps16FiltdDeltaLatGOver=tempW4+tempW3;  
    delta_det_ay_fltr = ldesps16FiltdDeltaLatGOver;

    if(McrAbs(ldesps16DeltaLatGUnder)<McrAbs(ldesps16FiltdDeltaLatGUnder))
    {    
        tempW2=ldesps16DeltaLatGUnder;
        tempW1=1;
        tempW0=128;
        s16muls16divs16();
        tempW4=tempW3;

        tempW2=ldesps16FiltdDeltaLatGUnder;
        tempW1=127;
        tempW0=128;
        s16muls16divs16();         
    }
    else
    {
        tempW2=ldesps16DeltaLatGUnder;
        tempW1=43;
        tempW0=128;
        s16muls16divs16();
        tempW4=tempW3;

        tempW2=ldesps16FiltdDeltaLatGUnder;
        tempW1=85;
        tempW0=128;
        s16muls16divs16();       
    }
    
    ldesps16FiltdDeltaLatGUnder=tempW4+tempW3;   

    
#endif

#if ( __DET_ALAT_FLTR==1 )
    delta_det_lat = det_alatc_fltr - det_alatm_fltr ;
    
    #if ( __DELTA_DET_AY_MOD==1 )
        delta_det_lat = delta_det_ay_fltr ;        
    #endif
    
#else
    delta_det_lat = det_alatc - det_alatm;
#endif


/*----------------------------------------------------------------*/
/* DET_ALATC_Unlimited                                            */
/*----------------------------------------------------------------*/

    det_alatc_unlimit_old = det_alatc_unlimit;

    tempW2 = McrAbs(nor_alat_unlimit);

    if ( tempW2 >= det_alatc_unlimit ) 
    {

/*      tempW0 = (uint16_t)( (((uint32_t)tempW2)*57)/128);
        tempW1 = (uint16_t)( (((uint32_t)det_alatc_unlimit_old)*71)/128);    Fc 10 Hz  */        
        det_alatc_unlimit = (int16_t)LCESP_u16Lpf1Int((uint16_t)tempW2, (uint16_t)det_alatc_unlimit_old, L_U8FILTER_GAIN_10MSLOOP_18HZ);                
    }
    else 
    {

/*      tempW0 = (uint16_t)( (((uint32_t)tempW2)*1)/512);
        tempW1 = (uint16_t)( (((uint32_t)det_alatc_unlimit_old)*511)/512);        Fc 0.1 Hz  */
        det_alatc_unlimit = (int16_t)LCESP_u16Lpf1Int((uint16_t)tempW2, (uint16_t)det_alatc_unlimit_old, 1);          
    }
}

void LDESP_vEstRoadAdhesion(void)
{

    mu_level_old = mu_level;

#if __Mu_Level_Adaptation

    alatm_dot_lf_old = alatm_dot_lf ;
    
    esp_tempW1 = det_alatm - det_alatm_old ;
    tempW2 = esp_tempW1 ; tempW1 = 1000; tempW0 = 5 ;  
    s16muls16divs16();
    alatm_dot_org = tempW3;          /* r: 0.001 [g/sec]  */
   
    alatm_dot_lf = LCESP_s16Lpf1Int(alatm_dot_org, alatm_dot_lf_old, 18);      
    
	cnt_alatm_dot = cnt_alatm_dot + 1 ;
	
	if( cnt_alatm_dot > 10)
    {
    	cnt_alatm_dot = 1 ;
    }
    else
    {
    	;
    }    
    
    alatm_dot10_sum = alatm_dot10_sum - alatm_dot10[cnt_alatm_dot] + alatm_dot_lf ;    
    
    alatm_dot10[cnt_alatm_dot] = alatm_dot_lf ; 
 
    alatm_dot10_avr = (int16_t)(alatm_dot10_sum/10);

#endif

/*
    #if __4WD
            mu_level1 = LCESP_u16CompSqrt_Ne(esp_mu, along);
    #else
            mu_level1 = LCESP_u16CompSqrt_Ne(esp_mu, (ax+5)/10);
    #endif

    tempW4 = (mu_level1 < det_alatm)?det_alatm:mu_level1;
    tempW4 = tempW4 + LAT_0G02G;
*/
/* ROP model : ROP_REQ_ACT 시, Model-Limit Define... '05 NZ */   
    
    #if __ROP
        
    if ( ROP_REQ_ACT ) 
    {         
        tempW4 = S16_ROP_MODEL_MU_LEVEL ;                                      
        if( tempW4 >= ( det_alatm + LAT_0G02G ) )  
        {
        	tempW4 = det_alatm + LAT_0G02G;                                            
        }
        else 
        {    
        	;
        }    
    } 
    else 
    {        
        
        tempW4 = det_alatm + LAT_0G02G;           
         
    }    
        
    #else                    
                       
        tempW4 = det_alatm + LAT_0G02G;   
        
    #endif    

#if  __BANK_DETECT_ESC_CTRL_ENABLE == 1    
  
    det_banked_angle_G_old = det_banked_angle_G ;
    
    /* tempW2 = Bank_Angle_Final; 
    tempW1 = 100;   
    tempW0 = 573;
    s16muls16divs16();
    esp_tempW4 = McrAbs(tempW3);  */ 
    
    esp_tempW3 = (int16_t)( (((int32_t)Bank_Angle_Final)*100)/573 ) ;  /* delta_banked_angle */
    esp_tempW4 = McrAbs(esp_tempW3);
    
    esp_tempW4 = (int16_t)( (((int32_t)esp_tempW4)*S16_BANK_YAW_LMT_GAIN_WEG)/100 ) ;  /* '10.04.26 : GM_CPG Improvement */    
    
    if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
	{		  
        if ( esp_tempW4 >= det_banked_angle_G )
        {
            det_banked_angle_G = LCESP_s16Lpf1Int(esp_tempW4, det_banked_angle_G_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_10_5HZ);
        }
        else
        {
            det_banked_angle_G = LCESP_s16Lpf1Int(esp_tempW4, det_banked_angle_G_old, (uint8_t)1);
        }  
    } 
    else 
    {
            det_banked_angle_G = LCESP_s16Lpf1Int(esp_tempW4, det_banked_angle_G_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_10_5HZ);
    }    
      
    if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
	{		  
        tempW4 = tempW4 + det_banked_angle_G; 
    } 
    else 
    {
    	;
    }
   
#endif       

	#if (__GM_CPG_BANK == 1)
	/* engage condition: Bank_K(LJG) flag set,Over Med mu, Rise count>T/P, Rise mode > 2th  */
		
		if((Bank_GM_Susp == 1) && (FLAG_BANK_SUSPECT == 0) && (FLAG_BANK_DETECTED == 0))
		{
			if(ldescS16GMBankRiseNum == 2)			/*  1st after Bank_GM_Susp */
			{
				tempW0 = LAT_0G1G;
			}
			else if(ldescS16GMBankRiseNum == 3)		/*  2nd after Bank_GM_Susp */
			{
				tempW0 = LAT_0G2G;
			}
			else if(ldescS16GMBankRiseNum >= 4)		/*  over 3rd after Bank_GM_Susp */
			{
				tempW0 = LAT_0G3G;
			}
			else
			{
				tempW0 = 0;
			}			
			tempW4 = tempW4 + tempW0;
		}
		else
		{
			;
		}  
	#endif
    
/*  '08/08/14 : Yaw_Limit_Model considering braking...    */    

    /*  if((fu1AxErrorDet==0)&&(fu1AxSusDet==0))
        {
            esp_tempW9 = McrAbs(lss16absAx1000g) ;
        }
        else
        {    
        	esp_tempW9 = McrAbs(filter_out1) ;
        }	*/
    
    ldesps16AxEst_old = ldesps16AxEst ;
    
    esp_tempW9 = McrAbs(afz*10) ;
    
    ldesps16AxEst = LCESP_s16Lpf1Int(esp_tempW9, ldesps16AxEst_old, 4);     
    	 
       
	esp_tempW7 = LCESP_s16IInter4Point( ldesps16AxEst, S16_BRK_LMT_Ax_EST_1ST, S16_BRK_LMT_Ay_VALUE_1ST,
	                                                   S16_BRK_LMT_Ax_EST_2ND, S16_BRK_LMT_Ay_VALUE_2ND, 
	                                                   S16_BRK_LMT_Ax_EST_3RD, S16_BRK_LMT_Ay_VALUE_3RD, 
	                                                   S16_BRK_LMT_Ax_EST_4TH, S16_BRK_LMT_Ay_VALUE_4TH ) ;
	 
    if (AFZ_OK==1)
	{
		esp_tempW8 = esp_tempW7 ;		
	}
	else
	{
		esp_tempW8 = 0 ;		
	}
  
/* Determination of Road_limit  */  


      esp_tempW0 = (int16_t)( (((int32_t)yaw_out)*rf)/10000 );     /*  Yaw_Same_Phase */

    esp_tempW1 = -(delta_yaw_thres*2) ;       
    esp_tempW2 = -(delta_yaw_thres*3/2)  ;             
    esp_tempW3 = -(delta_yaw_thres)  ;                                      
         
    esp_tempW4 = LCESP_s16IInter3Point( delta_yaw_first, esp_tempW1, 2,
                                                         esp_tempW2, 0,
                                                         esp_tempW3, -1  ) ;

    Stack_Mu_delyaw = Stack_Mu_delyaw + esp_tempW4 ;
    
	if ( Stack_Mu_delyaw >= 500 ) 
	{
		Stack_Mu_delyaw = 500 ;
	}
	else if ( Stack_Mu_delyaw <= 0 ) 
	{
		Stack_Mu_delyaw = 0 ;
	}
	else
	{    
		;
	}        
	
    if ( (ESP_SPLIT==0) && (esp_tempW0>0) && (Stack_Mu_delyaw>200) )  /* no split, same_phase, del_yaw_first -> under 판단 --> esp_tempw8 의 제한치 설정. ( alatm + offset 만큼 ) */
	{
	    esp_tempW5 = tempW4 + LAT_0G05G ;
	    
        if (esp_tempW8>esp_tempW5)
    	{
    	    esp_tempW8 = esp_tempW5 ; 
    	}
    	else
    	{ 
    	    ;
    	}	    
	}
	else
	{ 
	    ;	
	}
  
      
      tempW4 = (tempW4>=esp_tempW8)?tempW4:esp_tempW8;
  
/*  tempW2 = (int16_t)( (((long)tempW4)*28)/128);                r : 0.001;    Fc = 5Hz
    tempW1 = (int16_t)( (((long)mu_level_old)*100)/128);

    mu_level = tempW1 + tempW2;                              r : 0.001    */
    
    if ( (YAW_CHANGE_LIMIT==1) && (YAW_CHANGE_LIMIT_ACT==1) ) 
    { 
        /*
        esp_tempW1 = LCESP_s16FindRoadDependatGain( det_alatm, 1, 5, 8); 
         */
         
    #if __Mu_Level_Adaptation         
    
        if(ESP_ROUGH_ROAD==1)
        {
            esp_tempW2 = det_alatm - mu_level; 
            esp_tempW1 = LCESP_s16IInter2Point(esp_tempW2, LAT_0G1G, 1, LAT_0G3G, L_U8FILTER_GAIN_10MSLOOP_1_5HZ); 
            esp_tempW3 = LCESP_s16FindRoadDependatGain( det_alatm, esp_tempW1, L_U8FILTER_GAIN_10MSLOOP_1HZ, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);
        }
        else 
        {
            esp_tempW3 = LCESP_s16IInter3Point(alatm_dot10_avr, 221, 1, 286, L_U8FILTER_GAIN_10MSLOOP_1_5HZ, 714, L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
        }  
        
        mu_level = LCESP_s16Lpf1Int( tempW4, mu_level_old, (uint8_t)esp_tempW3);    
        
    #else        

      #if (__MGH60_ESC_IMPROVE_CONCEPT==1) 
 
			      if( yaw_out > 0 ) 
            {
              esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, L_U8FILTER_GAIN_10MSLOOP_1_5HZ, 
            	  											         ay_vx_dot_sig_1st_th, L_U8FILTER_GAIN_10MSLOOP_0_5HZ,						                      
            			                					   ay_vx_dot_sig_2nd_th, L_U8FILTER_GAIN_10MSLOOP_0_2HZ ) ;	
            }
            else 
            {     
            	esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, L_U8FILTER_GAIN_10MSLOOP_1_5HZ, 
            	  												      ay_vx_dot_sig_1st_th, L_U8FILTER_GAIN_10MSLOOP_0_5HZ,						                      
            			                						ay_vx_dot_sig_2nd_th, L_U8FILTER_GAIN_10MSLOOP_0_2HZ ) ;		
            } 
         
        mu_level = LCESP_s16Lpf1Int( tempW4, mu_level_old, (uint8_t)esp_tempW2 );
      
    #else        
    
        esp_tempW2 = det_alatm - mu_level;
        
        esp_tempW3 = LCESP_s16IInter2Point(esp_tempW2, LAT_0G1G, 1, LAT_0G3G, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);
        
        esp_tempW1 = LCESP_s16FindRoadDependatGain( det_alatm, esp_tempW3, L_U8FILTER_GAIN_10MSLOOP_1HZ, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);
        
        mu_level = LCESP_s16Lpf1Int( tempW4, mu_level_old, (uint8_t)esp_tempW1);    
       	if(esp_tempW1 > 127) {esp_tempW1 = 127;}
	    
        mu_level   = (int16_t)( ( ((int32_t)tempW4*(int8_t)esp_tempW1) + ((int32_t)mu_level_old*(128-(int8_t)esp_tempW1)) ) /128 );


      #endif  
        
    #endif              
    } 
    else 
    {
        mu_level = LCESP_s16Lpf1Int( tempW4, mu_level_old, L_U8FILTER_GAIN_10MSLOOP_6_5HZ );    /* Fc = 5 Hz*/
    }
    
    if(mu_level<LAT_0G15G) 
    {
    	mu_level=LAT_0G15G;
    }
    else 
    {    
    	;
    }    
    
    
}

#if  MGH60_New_Yaw_Limit == 1 

void LDESP_vLimitYawRateDesired(void)  /* '08. Winter : Yaw_Limit Improvement */
{

    ab_yawc_o = ab_yawc;
    k_mu_old  = k_mu;

    ab_rf = McrAbs(rf);

#if ( __DET_ALAT_FLTR==1 )
    delta_det_lat = det_alatc_fltr - det_alatm_fltr ;

    #if ( __DELTA_DET_AY_MOD==1 )
        delta_det_lat = delta_det_ay_fltr ;
    #endif

#else
    delta_det_lat = det_alatc - det_alatm ;
#endif


    esp_tempW0 = vrefm;
    if( esp_tempW0 <= 83 )
    {
    	esp_tempW0 = 83;
    }
    else
    {
    	;
    }

	/*    if (FLAG_FISH_HOOK &&  (esp_tempW0 <= 222 ) ) esp_tempW0 = 222 ; */
	    ab_rfm = (uint16_t)((((uint32_t)mu_level)*562)/esp_tempW0);/*   r : 0.01 , vrefm >= 2.7(*3.6=9.7kph)*/

    /*tempUW2 = mu_level;
    tempUW1 = 562;
    tempUW0 = esp_tempW0;
    u16mulu16divu16();
    ab_rfm = tempUW3;*/
 
              
    esp_tempW0 = vrefm;
    
    if( esp_tempW0 <= 20 )  
    {
    	esp_tempW0 = 20;                     
    }
    else 
    {    
    	;
    }    
    
    esp_tempW1 = det_alatm;
    
    if( esp_tempW1 <= 100 )  
    {
    	esp_tempW1 = 100;                     
    }
    else 
    {    
    	;
    }    
        
    /*tempW2 = esp_tempW1 ;                   /* ?? It needs to be continuous.. */
    tempW1 = 562 ; 
    tempW0 = esp_tempW0 ;  
    s16muls16divs16();
    ldesps16YawMaxAlatm = tempW3;*/           /* Step 01.  :  Ay_max -> YawC_Max */
    ldesps16YawMaxAlatm = (int16_t)( (((int32_t)esp_tempW1)*562)/esp_tempW0 );  
          
		esp_tempW0 = McrAbs(ldesps16YawMaxAlatm);  
          
        if ( McrAbs(rf) > esp_tempW0 ) 
        {          
           ldesps16YawMaxAlatmSign = esp_tempW0;
        } 
        else   
        { 
           ldesps16YawMaxAlatmSign = McrAbs(rf);
        }
              
        if ( rf < 0 ) 
        {
            ldesps16YawMaxAlatmSign = -ldesps16YawMaxAlatmSign;
        }
        else 
        {    
                  ;
        }         
      
 /* Step.2 : Alpha_r --> YawcOffset  */
 
    esp_tempW0 = McrAbs(Alpha_rear);  
        
    esp_tempW1 = LCESP_s16IInter3Point( esp_tempW0,  200, 100,
                                                     250, 50,
                                                     300, 0  );

    if ( (( Alpha_rear > 0 ) && ( rf > 0 )) || (( Alpha_rear < 0 ) && ( rf < 0 )) )
    {
        esp_tempW1 = 100 ;
    }
    else 
    {    
              ;
    }   
        
    esp_tempW0 = LCESP_s16IInter3Point( vrefk, 200, 500,  500, 500,  800, 500);         

    /*tempW2 = esp_tempW0; tempW1 = esp_tempW1; tempW0 = 100; 
    s16muls16divs16();                                      
    ldesps16YawOffset = tempW3 ;*/           

    ldesps16YawOffset = (int16_t)( (((int32_t)esp_tempW0)*esp_tempW1)/100 );
              
    esp_tempW3 = ldesps16YawMaxAlatm + ldesps16YawOffset ; 
        
    abrfm_new_old = abrfm_new ; 
         
    abrfm_new = LCESP_s16Lpf1Int( esp_tempW3, abrfm_new_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);    
    	         

        if ( McrAbs(rf) > abrfm_new ) 
        {          
           rf2_new = abrfm_new;
           
           		  	YAW_CHANGE_LIMIT = 1;
           		  	YAW_CHANGE_LIMIT_ACT = 1;
           		  	k_mu = 0;
        } 
        else   
        { 
           rf2_new = McrAbs(rf);
           		  	YAW_CHANGE_LIMIT = 0;
           		  	YAW_CHANGE_LIMIT_ACT = 0;
           		  	k_mu = 100;
        }
              
        if ( rf < 0 ) 
        {
            rf2_new = -rf2_new;
        }
        else 
        {    
                  ;
        }   
        
        rf2 = rf2_new ; 
          
}

#else

void LDESP_vLimitYawRateDesired(void)
{
	LDESP_vCalLimitedYawRate();
	LDESP_vDetYawLimitEntCond();
	LDESP_vDetermineDesiredYawRate();
}

void LDESP_vCalLimitedYawRate(void)
{

    ab_yawc_o = ab_yawc;
    k_mu_old  = k_mu;

    ab_rf = McrAbs(rf);   

  #if ( __DET_ALAT_FLTR==1 )
    delta_det_lat = det_alatc_fltr - det_alatm_fltr ;
      #if ( __DELTA_DET_AY_MOD==1 )
    delta_det_lat = delta_det_ay_fltr ;        
      #endif
  #else
    delta_det_lat = det_alatc - det_alatm ;
  #endif


    esp_tempW0 = vrefm;
    if( esp_tempW0 <= 14 )  
    {
    	esp_tempW0 = 14;                     
    }
    else 
    {    
    	;
    }    
    
	/*    if (FLAG_FISH_HOOK &&  (esp_tempW0 <= 222 ) ) esp_tempW0 = 222 ; */
	
 	ab_rfm = (int16_t)((((uint32_t)mu_level)*562)/((uint16_t)esp_tempW0));  /* r : 0.01 , vrefm >= 2.7(*3.6=9.7kph)*/
                                                
    /*tempUW2 = mu_level; 
    tempUW1 = 562; 
    tempUW0 = esp_tempW0;  
    u16mulu16divu16();
    ab_rfm = tempUW3;*/   

  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((ABS_fz) && (lsespu1AHBGEN3MpresBrkOn == 1))
   #else
    if((ABS_fz) && (MPRESS_BRAKE_ON == 1))
   #endif
  #else
    if (ABS_fz)
  #endif
    {
        esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YAW_LIMIT_GAIN_H_ABS, S16_YAW_LIMIT_GAIN_M_ABS, S16_YAW_LIMIT_GAIN_L_ABS);     /* ABS : Yaw Limit Gain*/

        if ( esp_tempW0 < 70 ) 
        {
        	esp_tempW0 = 70;
        }
        else 
        {    
        	;
        }    
        
		ab_rfm = (int16_t)( (((int32_t)ab_rfm)*esp_tempW0)/100 );
        /*tempW2 = ab_rfm;
        tempW1 = esp_tempW0; 
        tempW0 = 100;
        s16muls16divs16();       
        ab_rfm = tempW3;*/                                         
    } 
    else 
    {
	    /*ab_rfm=(int16_t)(((int32_t)ab_rfm*YAW_LIMIT_GAIN)/100)*/
	    /* 		if (FLAG_FISH_HOOK)  {                   */
	    /*			esp_tempW0 = 10 ;                    */ 
	    /*		} else                                   */
		
		if ( (FLAG_SLALOM==1) && (!ROP_REQ_ACT ) ) 
		{
            esp_tempW0 = S16_YAW_LIMIT_GAIN_H_SL;
        } 
        else 
      #if (__MGH60_ESC_IMPROVE_CONCEPT == 1)  
        { 
         	esp_tempW0 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_H_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_H_V1 ,                                                       
        	                                            S16_YAW_LIMIT_H_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_H_V2 ,
        	                                            S16_YAW_LIMIT_H_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_H  );               
        }
        
        esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_M_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_M_V1 ,      
                                                    S16_YAW_LIMIT_M_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_M_V2 ,
                                                    S16_YAW_LIMIT_M_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_M  );            
        
        esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_M_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_L_V1 ,      
                                                    S16_YAW_LIMIT_M_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_L_V2 ,
                                                    S16_YAW_LIMIT_M_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_L  );            
          
                 
        esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);  /* s1. Yaw Limit Gain*/
      #else   
        {
            esp_tempW0 = S16_YAW_LIMIT_GAIN_H;
        }
            esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW0, 
                                S16_YAW_LIMIT_GAIN_M, S16_YAW_LIMIT_GAIN_L);     /* s1. Yaw Limit Gain*/
#endif 

	/*/        if(FLAG_FISH_HOOK) {			*/
	/*/        	esp_tempW0 = esp_tempW0 ;   */
	/*/        } else                       */
        if ( esp_tempW0 < 70 ) 
        {
        	esp_tempW0 = 70;
        }
        else 
        {    
        	;
        }    

        
	    /*      2004/01/29 KJG
        wstr의 변화와 연동된 gain 구함 */

        yaw_limit_gain1 = esp_tempW0;
        
        if(YAW_CHANGE_LIMIT_ACT==1)
        {  
            gain_wstr1 = LCESP_s16FindRoadDependatGain ( esp_mu, 250, 500, 600 );
                
            if( (((rf > YAW_1DEG) && (wstr > gain_wstr1))
                || ((rf < -YAW_1DEG) && (wstr < -gain_wstr1))) 
              && ( ((wstr>gain_wstr1) && (wstr_dot>0 )) 
                || ((wstr<-gain_wstr1) && (wstr_dot<0 ))) ) 
            {
                initial_ab_wstr = wstr; 
                yaw_limit_gain2 = yaw_limit_gain1;
                MDL_2ND_LMT_ENTER = 1 ;              
            } 
            else if( ( (MDL_2ND_LMT_ENTER==1) && (initial_ab_wstr > 0) && (wstr_dot <= 0 ) ) 
                    || ( (MDL_2ND_LMT_ENTER==1) && (initial_ab_wstr < 0) && (wstr_dot >= 0 ) )  ) 
            {
                MDL_2ND_LMT_ENTER = 1 ;                             
                /* Decrease Gain : 0% ~ 200% , 0% 이면, 감소 없음/ 클수록 감소 큼. */                        
            	esp_tempW1 = LCESP_s16FindRoadDependatGain ( esp_mu, S16_YAW_LIMIT_WSTR_DEC_GAIN_H, 
            	             S16_YAW_LIMIT_WSTR_DEC_GAIN_M, S16_YAW_LIMIT_WSTR_DEC_GAIN_L );    /*100%(50%~200%)     */
            	                        
            	esp_tempW6 = LCESP_s16IInter3Point( esp_tempW1, 0, 100, 100, 45, 200, 15);       /* y5 value. */                                    
            	esp_tempW7 = LCESP_s16IInter3Point( esp_tempW1, 0, 100, 100, 52, 200, 25);       /* y30 value.*/                                     
            	esp_tempW8 = LCESP_s16IInter3Point( esp_tempW1, 0, 100, 100, 60, 200, 35);       /* y50 value.*/                                     
            	esp_tempW9 = LCESP_s16IInter3Point( esp_tempW1, 0, 100, 100, 75, 200, 65);       /* y75 value.*/                                     
                 /*esp_tempW0 = (int16_t)(((int32_t)abs_wstr*100)/initial_ab_wstr);              end pt  */     
   					/* Divide by zero 금지, 2005.12.20 */

            	if ( wstr < 0 )  
            	{
            		esp_tempW0 = -1 ; 
            	} 
            	else 
            	{
            		esp_tempW0 = 1 ;   
            	}
            	if( McrAbs(initial_ab_wstr) < 250 ) 
            	{
            		initial_ab_wstr = esp_tempW0*250; 
            	}
            	else 
            	{    
            		;
            	}    
            	
            	/*tempW2 = wstr; 
            	tempW1 = 100; 
            	tempW0 = initial_ab_wstr;
            	s16muls16divs16();
            	esp_tempW0 = tempW3;*/

				esp_tempW0 = (int16_t)( (((int32_t)wstr)*100)/initial_ab_wstr );         	
            	                                  
            	if( esp_tempW0 < 5 ) 
            	{
            		esp_tempW0 = 5;       
            	}
            	else 
            	{    
            		;
            	}    
            	
            	esp_tempW4 = LCESP_s16IInter5Point( esp_tempW0, 5, esp_tempW6, 30, esp_tempW7, 
            	                            50, esp_tempW8, 75, esp_tempW9, 100, 100 );  
            	
#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)            	
             	esp_tempW0 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_H_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_H_V1 ,      
            	                                            S16_YAW_LIMIT_H_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_H_V2 ,
            	                                            S16_YAW_LIMIT_H_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_H  );   
            
             	esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_M_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_M_V1 ,      
            	                                            S16_YAW_LIMIT_M_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_M_V2 ,
            	                                            S16_YAW_LIMIT_M_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_M  );            
            
             	esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YAW_LIMIT_M_SPEED_V1, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_L_V1 ,      
            	                                            S16_YAW_LIMIT_M_SPEED_V2, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_L_V2 ,
            	                                            S16_YAW_LIMIT_M_SPEED_V3, (int16_t)S16_YAW_LIMIT_GAIN_LIMIT_L  );               	
            	  
            	esp_tempW5 = LCESP_s16FindRoadDependatGain ( esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);           /*50%(10~100)                   */
#else            	
            	esp_tempW5 = LCESP_s16FindRoadDependatGain ( esp_mu, S16_YAW_LIMIT_GAIN_LIMIT_H, 
            	             S16_YAW_LIMIT_GAIN_LIMIT_M, S16_YAW_LIMIT_GAIN_LIMIT_L);           /*50%(10~100)                   */
#endif            	             
            	             
            	if( esp_tempW4 < esp_tempW5 ) 
            	{
            		esp_tempW4 = esp_tempW5;                               
            	}
            	else 
            	{    
            		;
            	}    
            	yaw_limit_gain2 = (int16_t)(((int32_t)yaw_limit_gain1*esp_tempW4)/100);      
            	/*tempW2 = yaw_limit_gain1; tempW1 = esp_tempW4; tempW0 = 100;
            	s16muls16divs16();
            	yaw_limit_gain2 = tempW3;*/                                                                   
            } 
            else 
            {                                                             
                yaw_limit_gain2 = yaw_limit_gain1;
                MDL_2ND_LMT_ENTER = 0 ;                    
            }                    
        } 
        else 
        { 
            yaw_limit_gain2 = yaw_limit_gain1; 
            MDL_2ND_LMT_ENTER = 0 ;                
        }
        
    /*tempW2 = ab_rfm; 
    tempW1 = yaw_limit_gain2; 
    tempW0 = 100;
    s16muls16divs16();       
    ab_rfm = tempW3;*/                                /* s2. ab_rfm*/        
    ab_rfm = (int16_t)( (((int32_t)ab_rfm)*yaw_limit_gain2)/100 );                             
    
    }

  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
             
	ay_vx_dot_sig_1st_th = LCESP_s16IInter5Point(vrefk, S16_YC_SPEED_DEP_VLOW_V,    S16_YC_AY_VX_DOT_VLOW_V_1ST,
														S16_YC_SPEED_DEP_LOW_V,	    S16_YC_AY_VX_DOT_LOW_V_1ST,		
														S16_YC_SPEED_DEP_MED1_V,    S16_YC_AY_VX_DOT_MED1_V_1ST,  
	                                                    S16_YC_SPEED_DEP_HIG_V,     S16_YC_AY_VX_DOT_HIG_V_1ST,  
	                                                    S16_YC_SPEED_DEP_VER_HIG_V, S16_YC_AY_VX_DOT_VER_HIG_V_1ST );
	                                                               
	ay_vx_dot_sig_2nd_th = LCESP_s16IInter5Point(vrefk, S16_YC_SPEED_DEP_VLOW_V,    S16_YC_AY_VX_DOT_VLOW_V_2ND,
														S16_YC_SPEED_DEP_LOW_V,	    S16_YC_AY_VX_DOT_LOW_V_2ND,		
														S16_YC_SPEED_DEP_MED1_V,    S16_YC_AY_VX_DOT_MED1_V_2ND,  
	                                                    S16_YC_SPEED_DEP_HIG_V,     S16_YC_AY_VX_DOT_HIG_V_2ND,  
	                                                    S16_YC_SPEED_DEP_VER_HIG_V, S16_YC_AY_VX_DOT_VER_HIG_V_2ND );
	
	ay_vx_dot_sig_1st_th_Mmu = LCESP_s16IInter5Point(vrefk, S16_YC_SPEED_DEP_VLOW_V,    S16_YC_AY_VX_DOT_VLOW_V_1ST_Mmu,                                                            
											                S16_YC_SPEED_DEP_LOW_V ,    S16_YC_AY_VX_DOT_LOW_V_1ST_Mmu ,		                                                    
											                S16_YC_SPEED_DEP_MED1_V,    S16_YC_AY_VX_DOT_MED1_V_1ST_Mmu,                                                            
	                                                        S16_YC_SPEED_DEP_HIG_V ,    S16_YC_AY_VX_DOT_HIG_V_1ST_Mmu,                                                             
	                                                        S16_YC_SPEED_DEP_VER_HIG_V, S16_YC_AY_VX_DOT_VER_HIG_V_1ST_Mmu );                                                       
	                                                                                                                                                                        
	ay_vx_dot_sig_2nd_th_Mmu = LCESP_s16IInter5Point(vrefk, S16_YC_SPEED_DEP_VLOW_V,    S16_YC_AY_VX_DOT_VLOW_V_2ND_Mmu,                                                            
														    S16_YC_SPEED_DEP_LOW_V,	    S16_YC_AY_VX_DOT_LOW_V_2ND_Mmu,		                                                        
														    S16_YC_SPEED_DEP_MED1_V,    S16_YC_AY_VX_DOT_MED1_V_2ND_Mmu,                                                            
	                                                        S16_YC_SPEED_DEP_HIG_V,     S16_YC_AY_VX_DOT_HIG_V_2ND_Mmu,                                                             
	                                                        S16_YC_SPEED_DEP_VER_HIG_V, S16_YC_AY_VX_DOT_VER_HIG_V_2ND_Mmu );                                                       
	
	if( yaw_out > 0 ) 
    {
      esp_tempW1 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus,                    
                                                             0, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG, 
  	  											              ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST,						                      
  			                					        ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND ) ;	
  	
  	  esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 
  	                                                             0, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_Mmu, 
  									                      ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST_Mmu,						                      
  	                		                  ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND_Mmu ) ;
  }
  else
  {     
  	  esp_tempW1 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 
  	                                                         0, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG, 
  	  												            ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST,						                      
  			                						      ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND ) ;		
  			                			  
  	  esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 
  	                                                             0, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_Mmu,        		                			  
  	  								                    ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_1ST_Mmu,			                			  
  			                		              ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_YAW_LMT_WEG_2ND_Mmu ) ; 		                			  
  } 
      	 
  if(delta_yaw_first < S16_YC_AY_VX_DOT_DCT_OS_YAW)
  {      	 
      ab_rfm_gain = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW1, esp_tempW2, 100); 	   
  }
  else
  {
   		ab_rfm_gain = 100;	
  }
	
	ab_rfm = (int16_t)( (((int32_t)ab_rfm)*ab_rfm_gain)/100 );
  #endif
}

void LDESP_vDetYawLimitEntCond(void)
{
/***** Yaw limit enter condition *****/
/******* Enter Condition using wstr_dot **********/

    /*if(FLAG_TIME_OPT) 
    {*/
    	esp_tempW1 = S16_YAW_LIMIT_ST_SLOW_ENTER;   /* 150 deg/s*/
		/* 5/1 GK tune...*/
    	esp_tempW3 = S16_YAW_LIMIT_ST_FAST_ENTER;
    	esp_tempW4 = S16_YAW_LIMIT_ST_FAST_ENTER + S16_YAW_LIMIT_FA_ST_M_SP_ENT_AD ;
    	esp_tempW5 = S16_YAW_LIMIT_ST_FAST_ENTER + S16_YAW_LIMIT_FA_ST_H_SP_ENT_AD ;    
    	
    	esp_tempW2 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_ENTER_FA_ST_L_SP, esp_tempW3, 
                                                   S16_YAW_LIMIT_ENTER_FA_ST_M_SP, esp_tempW4, 
                                                   S16_YAW_LIMIT_ENTER_FA_ST_H_SP, esp_tempW5);   
    
		/*    esp_tempW2 = S16_YAW_LIMIT_ST_FAST_ENTER;    200 ~ 300 deg/s*/
		/*    esp_tempW3 = S16_YAW_LIMIT_DELTA_LOW_SLOW;                    */
		/*    esp_tempW4 = S16_YAW_LIMIT_DELTA_HIGH_SLOW;                   */
		                                                                    
		/*    esp_tempW6 = LCESP_s16IInter2Point( det_abs_wstr, YAW_LIMIT_H_SMALL_ANG, S16_YAW_LIMIT_DELTA_HIGH_FAST, 
		                               YAW_LIMIT_H_LARGE_ANG, S16_YAW_LIMIT_DELTA_HIGH_FAST_LARGE );     
		*/
        if(ABS_fz==1) 
        {		
	        esp_tempW6 = S16_YAW_LIMIT_DELTA_HIGH_FAST_ABS ;   /* generally 0.15 g */
	    }
	    else
        {
	        esp_tempW6 = S16_YAW_LIMIT_DELTA_HIGH_FAST ;   /* generally 0.15 g */
        }	        

    	/* S16_MU_MED_HIGH */
        if(ABS_fz==1) 
        {		
    	    esp_tempW3 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_SLOW_ABS+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                               S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_SLOW_ABS+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_SLOW_ABS);          
    	    
    	    esp_tempW5 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_FAST_ABS+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                               S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_FAST_ABS+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_FAST_ABS);     
	        esp_tempW11 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_VSLOW_ABS+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                 			    S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_VSLOW_ABS+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                 			    S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_VSLOW_ABS);                                    
	    }
	    else
        {
    	    esp_tempW3 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_SLOW+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                               S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_SLOW+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_SLOW);          
    	    
    	    esp_tempW5 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_FAST+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                               S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_FAST+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_FAST);            
            esp_tempW11 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_DELTA_LOW_VSLOW+S16_YAW_LIMIT_M_V1_INC_DEL_AY, 
    	                                                S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_DELTA_LOW_VSLOW+S16_YAW_LIMIT_M_V2_INC_DEL_AY,
    	                                                S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_DELTA_LOW_VSLOW);          
        }	   
    	                                  
    	if ( FLAG_ON_CENTER==1 ) 
    	{
    		esp_tempW5 = 350; 
    	}
    	else if (FLAG_BETA_UNDER_DRIFT==1) 
    	{
    		esp_tempW3 = esp_tempW5;
    	}
    	else 
    	{    
    		;
    	}    
    	                                           
    	if ( esp_tempW3 < esp_tempW5 ) 
    	{
    		esp_tempW3 = esp_tempW5;
    	}
    	else 
    	{    
    		;
    	}    
    	                              
    	/* HIGH                 */
        if(ABS_fz==1) 
        {		
    	    esp_tempW4 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, S16_YAW_LIMIT_DELTA_HIGH_SLOW_ABS+S16_YAW_LIMIT_H_V1_INC_DEL_AY,      
    	                                               S16_YAW_LIMIT_H_SPEED_V2, S16_YAW_LIMIT_DELTA_HIGH_SLOW_ABS+S16_YAW_LIMIT_H_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_H_SPEED_V3, S16_YAW_LIMIT_DELTA_HIGH_SLOW_ABS); 
	     	esp_tempW12 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, S16_YAW_LIMIT_DELTA_HIGH_VSLOW_ABS+S16_YAW_LIMIT_H_V1_INC_DEL_AY,      
    	                                                S16_YAW_LIMIT_H_SPEED_V2, S16_YAW_LIMIT_DELTA_HIGH_VSLOW_ABS+S16_YAW_LIMIT_H_V2_INC_DEL_AY,
    	                                                S16_YAW_LIMIT_H_SPEED_V3, S16_YAW_LIMIT_DELTA_HIGH_VSLOW_ABS); 
	    }
	    else
        {
    	    esp_tempW4 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, S16_YAW_LIMIT_DELTA_HIGH_SLOW+S16_YAW_LIMIT_H_V1_INC_DEL_AY,      
    	                                               S16_YAW_LIMIT_H_SPEED_V2, S16_YAW_LIMIT_DELTA_HIGH_SLOW+S16_YAW_LIMIT_H_V2_INC_DEL_AY,
    	                                               S16_YAW_LIMIT_H_SPEED_V3, S16_YAW_LIMIT_DELTA_HIGH_SLOW); 
      		esp_tempW12 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, S16_YAW_LIMIT_DELTA_HIGH_VSLOW+S16_YAW_LIMIT_H_V1_INC_DEL_AY,      
    	                                                S16_YAW_LIMIT_H_SPEED_V2, S16_YAW_LIMIT_DELTA_HIGH_VSLOW+S16_YAW_LIMIT_H_V2_INC_DEL_AY,
    	                                                S16_YAW_LIMIT_H_SPEED_V3, S16_YAW_LIMIT_DELTA_HIGH_VSLOW); 
        }	       
    	                                                       
    	esp_tempW6 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, esp_tempW6+S16_YAW_LIMIT_H_V1_INC_DEL_AY,      
    	                                           S16_YAW_LIMIT_H_SPEED_V2, esp_tempW6+S16_YAW_LIMIT_H_V2_INC_DEL_AY,
    	                                           S16_YAW_LIMIT_H_SPEED_V3, esp_tempW6);                                                            
    	
    	if ( FLAG_ON_CENTER==1 ) 
    	{
    		esp_tempW6 = 500;
    	}
    	else 
    	{    
    		;
    	}    
    	
    	if ( esp_tempW4 < esp_tempW6 ) 
    	{
    		esp_tempW4 = esp_tempW6;  
    	}
    	else 
    	{    
    		;
    	}    
    	
/*    	esp_tempW7 = LCESP_s16IInter2Point(vrefk,VREF_K_40_KPH,S16_YAW_LIMIT_ENT_VSLOW_ST_AY_MU_MED_L_SP, VREF_K_80_KPH,S16_YAW_LIMIT_ENT_VSLOW_ST_AY_MU_MED_H_SP); 
    	esp_tempW8 = LCESP_s16IInter2Point(vrefk,VREF_K_40_KPH,S16_YAW_LIMIT_ENT_VSLOW_ST_AY_MU_HIGH_L_SP, VREF_K_80_KPH,S16_YAW_LIMIT_ENT_VSLOW_ST_AY_MU_HIGH_H_SP); 

    	yaw_limit_slow_wstr_dot_con_mid =  LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW7,
    	                                      lsesps16MuHigh, esp_tempW8);  */
    	
/*    	if(yaw_limit_slow_wstr_dot_con_mid <= LAT_0G3G) 
    	{
    		yaw_limit_slow_wstr_dot_con_mid=LAT_0G3G;
    	}
    	else 
    	{    
    		;
    	} */   
    	
    	/*
    	// S16_MU_MED_HIGH
    	esp_tempW7 = LCESP_s16IInter3Point( det_wstr_dot,       500, yaw_limit_slow_wstr_dot_con_mid,
    	                                                 esp_tempW1, esp_tempW3,     
    	                                                 esp_tempW2, esp_tempW5);   
    	// HIGH                                          
    	esp_tempW8 = LCESP_s16IInter3Point( det_wstr_dot,       500, yaw_limit_slow_wstr_dot_con_mid,
    	                                                 esp_tempW1, esp_tempW4,
    	                                                 esp_tempW2, esp_tempW6);                            
    	    
    	yaw_limit_enter_delta_ay = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW7,
    	                                                             lsesps16MuHigh,    esp_tempW8);  
    	                                      // esp_tempW0 --> yaw_limit_enter ( 횡g 차 )
                                
    	esp_tempW7 = LCESP_s16IInter3Point( det_wstr_dot, S16_YAW_LIMIT_ST_VSLOW_ENTER_M, yaw_limit_slow_wstr_dot_con_mid,
    	                                                 esp_tempW1, esp_tempW3,     
    	                                                 esp_tempW2, esp_tempW5);   
    	esp_tempW8 = LCESP_s16IInter3Point( det_wstr_dot, S16_YAW_LIMIT_ST_VSLOW_ENTER_H, yaw_limit_slow_wstr_dot_con_mid,
    	                                                 esp_tempW1, esp_tempW4,
    	                                                 esp_tempW2, esp_tempW6);                            
    	        	
    	 */    	
     	/* S16_MU_MED_HIGH*/
       	esp_tempW7 = LCESP_s16IInter3Point( det_wstr_dot, S16_YAW_LIMIT_ST_VSLOW_ENTER_M, esp_tempW11,
 	                                                      esp_tempW1,                     esp_tempW3,     
    	                                                  esp_tempW2,                     esp_tempW5);   
    	/* HIGH                                          */
    	esp_tempW8 = LCESP_s16IInter3Point( det_wstr_dot, S16_YAW_LIMIT_ST_VSLOW_ENTER_H, esp_tempW12,
    	                                                  esp_tempW1,                     esp_tempW4,
    	                                                  esp_tempW2,                     esp_tempW6);                            
    	yaw_limit_enter_delta_ay = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW7,
    	                                                             lsesps16MuHigh,    esp_tempW8);  
    	                                      /* esp_tempW0 --> yaw_limit_enter ( 횡g 차 )*/   	                                      
    	/*  __New_Mu_Level_Yaw_Limit --> Lower_Limit  : '06.11.13 */

    	/************Pre filling using******************/
    	
    	pre_filling_enter_cond  = yaw_limit_enter_delta_ay;
    	
    	/***********************************************/   	
    	    	    
    	delta_Ay_Upper_limit = yaw_limit_enter_delta_ay ;  
    	                                                             	    	    
    	delta_Ay_Lower_limit = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW5,
    	                                                         lsesps16MuHigh,    esp_tempW6 );  
    /*}
    else 
    {    
    	;
    } */   
    
    /*******  Enter Condition using betad  **********/
#if __YMC_MGH40

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/
	    vm_yaw_limit_betad_en = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_BETAD_L_SP_CON, 
	                                                          S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_BETAD_M_SP_CON, 
	                                                          S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_BETAD_H_SP_CON);                                    
	    
	    /* High mu*/
	    esp_tempW8 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, S16_YAW_LIMIT_HMU_DEL_AY+S16_YAW_LIMIT_DELTA_HMU_AY_L,      
	                                               S16_YAW_LIMIT_H_SPEED_V2, S16_YAW_LIMIT_HMU_DEL_AY+S16_YAW_LIMIT_DELTA_HMU_AY_M,
	                                               S16_YAW_LIMIT_H_SPEED_V3, S16_YAW_LIMIT_HMU_DEL_AY);                                                            
	    
	    /*Med-Low mu*/
	    esp_tempW7 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, S16_YAW_LIMIT_MMU_DEL_AY+S16_YAW_LIMIT_DELTA_MMU_AY_L,      
	                                               S16_YAW_LIMIT_M_SPEED_V2, S16_YAW_LIMIT_MMU_DEL_AY+S16_YAW_LIMIT_DELTA_MMU_AY_M,
	                                               S16_YAW_LIMIT_M_SPEED_V3, S16_YAW_LIMIT_MMU_DEL_AY);                                                            
	    
	    esp_tempW0 = (Beta_MD/100)*beta_dot;
		
/*Bank suspect 고려~~~ 필히		*/

		if((!FLAG_BANK_SUSPECT)&&(!YAW_LIMIT_BETAD)
		   &&(FLAG_DETECT_DRIFT)&&(vrefk <= VREF_K_150_KPH))  /* Phase plane increase*/
		{
			vm_yaw_limit_enter_delta_ay_b = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW7,
	                                                                          lsesps16MuHigh,    esp_tempW8);  
	        YAW_LIMIT_BETAD=1;
	     
		}
		else if((!FLAG_BANK_SUSPECT)&&(YAW_LIMIT_BETAD)&&(McrAbs(beta_dot) >= (300))&&(vrefk <= VREF_K_150_KPH))
		{
			vm_yaw_limit_enter_delta_ay_b=LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, esp_tempW7,
	                                                                        lsesps16MuHigh,    esp_tempW8);  
	        YAW_LIMIT_BETAD=1;
	       
		}
		else
		{
			vm_yaw_limit_enter_delta_ay_b=1000;
			YAW_LIMIT_BETAD=0;
	      
		}
		
/*}*/
		
		if(yaw_limit_enter_delta_ay >= vm_yaw_limit_enter_delta_ay_b) 
		{
			yaw_limit_enter_delta_ay = vm_yaw_limit_enter_delta_ay_b;
		}
		else 
		{    
			;
		}    
#endif

/*******  Enter Condition : Del_Yaw **********/

#if ( __New_Mu_Level_Yaw_Limit==1 )  
         
    Index_Mu_delyaw_old = Index_Mu_delyaw ;
    Index_Mu_del_ay_old = Index_Mu_del_ay ;
 
    esp_tempW5 = -delta_yaw_thres;    
    esp_tempW3 = (delta_yaw_thres*6)/10;
    esp_tempW2 = -esp_tempW3;
    
    esp_tempW4 = -delta_yaw_thres*2;     
    
/*if ( LOOP_TIME_20MS_TASK_1 == 1 )
{*/    
    esp_tempW1 = LCESP_s16IInter2Point( vrefk, 300, esp_tempW4,
                                               400, esp_tempW5 ); 
                                               
    esp_tempW2 = LCESP_s16IInter2Point( vrefk, 300, esp_tempW5,
                                               400, esp_tempW2 );                                                
         
    inc_A_del_yaw = LCESP_s16IInter4Point( delta_yaw_first, esp_tempW1, -4,
                                                            esp_tempW2, 0,
                                                            0, 2,
                                                            esp_tempW3, 4 );   
/*}*/

    Index_Mu_delyaw = Index_Mu_delyaw_old - inc_A_del_yaw ;
    
	if ( Index_Mu_delyaw >= S16_Time_TH_Max ) 
	{
		Index_Mu_delyaw = S16_Time_TH_Max ;
	}
	else if ( Index_Mu_delyaw <= 0 ) 
	{
		Index_Mu_delyaw = 0 ;
	}
	else
	{    
		;
	}        
	
	if ( Mu_delyaw_Reach_Flg == 0 ) 
	{
        if ( Index_Mu_delyaw < S16_Time_TH_2nd )
        {
            del_ay_TH_delyaw = delta_Ay_Upper_limit ;
        }
        else if ( Index_Mu_delyaw < S16_Time_TH_Max ) 
        {
            del_ay_TH_delyaw = LCESP_s16IInter2Point( Index_Mu_delyaw, S16_Time_TH_2nd, delta_Ay_Upper_limit,
                                                                       S16_Time_TH_Max, delta_Ay_Lower_limit );     
        }
        else
        {
            del_ay_TH_delyaw = delta_Ay_Lower_limit ;
            Mu_delyaw_Reach_Flg = 1 ;
        }  
	} 
	else
	{    
        if ( Index_Mu_delyaw > S16_Time_TH_2nd )
        {
            del_ay_TH_delyaw = delta_Ay_Lower_limit ;
        }
        else if ( Index_Mu_delyaw > S16_Time_TH_1st ) 
        {
            del_ay_TH_delyaw = LCESP_s16IInter2Point( Index_Mu_delyaw, S16_Time_TH_1st, delta_Ay_Upper_limit,
                                                                       S16_Time_TH_2nd, delta_Ay_Lower_limit );     
        }
        else
        {
            del_ay_TH_delyaw = delta_Ay_Upper_limit ;
            Mu_delyaw_Reach_Flg = 0 ;
        }  
	}  
	     	
    if ( vrefk >= 300 )
    {
     	if(del_ay_TH_delyaw < yaw_limit_enter_delta_ay) 
    	{
    		yaw_limit_enter_delta_ay = del_ay_TH_delyaw;
    	}
    	else 
    	{    
    		;
    	}    
    }
    else
    {
       ;
    }  
			     	
#endif   /* ( __New_Mu_Level_Yaw_Limit==1 ) */

    /*  '06.12.18 : Limitation  */
    
	if ( yaw_limit_enter_delta_ay <= LAT_0G1G ) 
	{
		yaw_limit_enter_delta_ay = LAT_0G1G ;
	}
	else 
	{    
		;
	}    

/*  Flag*/
    if ( !YAW_CHANGE_LIMIT ) 
    {
    	yaw_limit_enter = yaw_limit_enter_delta_ay;
    }
    else 
    {    
    	;
    }    
/*  Hysteresis*/
}

void LDESP_vDetermineDesiredYawRate(void)
{

  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
  
    delta_rf2_rf = McrAbs(rf2-rf);
    
    if ( (SAS_CHECK_OK==1) && (!BACK_DIR) ) 
    {
    	if ((delta_det_lat >= yaw_limit_enter)&&(YAW_CHANGE_LIMIT==0))
    	{
    	    k_mu = 0 ;
    	    YAW_CHANGE_LIMIT=1 ;
    	
    	} 
    	else if ((YAW_CHANGE_LIMIT==1)&&((delta_det_lat >= (yaw_limit_enter-20))||(delta_rf2_rf>100)||(det_wstr_dot_lf>300)))  
    	{
    	    k_mu = 0 ;
    	    YAW_CHANGE_LIMIT=1 ;    	    
    	}
    	else
    	{
    	    k_mu = 100 ;
    	    YAW_CHANGE_LIMIT=0 ;    	        	  
    	}  
    }
    else 
    {    
    	    k_mu = 100;
    	    YAW_CHANGE_LIMIT=0;
    }       
 
  #else
  
    if ( SAS_CHECK_OK && ( !BACK_DIR ) ) 
    {
    	if ( delta_det_lat <= yaw_limit_enter - 100 ) 
    	{
    	    k_mu = 100;
    	    
    		if ( YAW_CHANGE_LIMIT == 1 ) {
    		  	    
        	    if ( k_mu > ( k_mu_old + 5 ) )         /* 06/08/10, NZ */
        	    {
        	        k_mu = k_mu_old + 5;
        	    }
        	    else 
        	    {    
        	    	;
        	    }
        	    
    		}       	            	    
    		    
    	} 
    	else if ( delta_det_lat <= yaw_limit_enter ) 
    	{
    	
    	    k_mu = (yaw_limit_enter  - delta_det_lat);
    	
    		if ( YAW_CHANGE_LIMIT == 1 ) {
    		  	    
        	    if ( k_mu < k_mu_old ) 
        	    {
        	    	k_mu = k_mu_old;
        	    }
        	    else if ( k_mu > ( k_mu_old + 5 ) )         /* 06/08/14, NZ */
        	    {
        	        k_mu = k_mu_old + 5;
        	    }
        	    else 
        	    {    
        	    	;
        	    }
        	    
    		}
    	
    	} 
    	else if ( delta_det_lat < yaw_limit_enter+100  )  
    	{
    	
    	    k_mu = (yaw_limit_enter + 100 - delta_det_lat);
    	
    	    if ( k_mu > k_mu_old ) 
    	    {
    	    	k_mu = k_mu_old;
    	    }
    	    else 
    	    {    
    	    	;
    	    }    
    	
    		if ( YAW_CHANGE_LIMIT != 0 ) {
    			k_mu = 0;
			}
			    			 
    	    YAW_CHANGE_LIMIT=1;
    	
    	} 
    	else 
    	{
    	    k_mu = 0;
    	    YAW_CHANGE_LIMIT=1;
    	} 
    	
    }
    else 
    {    
    	;
    }        	
    	
#endif
  
#if  __BANK_DETECT_ESC_CTRL_ENABLE == 0
    	
    if ( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) ) 
    {            
        YAW_CHANGE_LIMIT=0;        
    }
    else
    {
        ;
    } 	
    
#endif 
    
/*    if(LOOP_TIME_20MS_TASK_1==1) 
    {*/
    	yaw_limit_exit_speed = LCESP_s16FindRoadDependatGain ( esp_mu, 450, 300, 200);    
/*    }
    else 
    {    
    	;
    }  */  
	/*    if( FLAG_FISH_HOOK ) yaw_limit_exit_speed = 200;*/
	
	/*        if ( YAW_CHANGE_LIMIT )*/
	/*        Only : decrease possible */    
    if ( (YAW_CHANGE_LIMIT_ACT==1) && (k_mu>k_mu_old)&&(vrefk>yaw_limit_exit_speed) ) 
    {
    	k_mu=k_mu_old;     
    }
    else 
    {    
    	;
    }    

/*    if ( YAW_CHANGE_LIMIT_ACT && (vrefk>yaw_limit_exit_speed) ) 
    {
    	k_mu= 0;         
    }
    else 
    {    
    	;
    } */    
	  esp_tempW1 = (int16_t)((((int32_t)ab_rf)*k_mu)/100);
    /*tempUW2 = ab_rf; 
    tempUW1 = k_mu; 
    tempUW0 = 100;
    u16mulu16divu16();
    esp_tempW1 = tempUW3;*/   
        
	  esp_tempW2 = (int16_t)((((int32_t)ab_rfm)*(100-k_mu))/100);
    /*tempUW2 = ab_rfm; 
    tempUW1 = 100-k_mu; 
    tempUW0 = 100;
    u16mulu16divu16();
    esp_tempW2 = tempUW3;*/       
    

	/*  esp_tempW3 = (uint16_t)( (((uint32_t)(esp_tempW1 + esp_tempW2))*40)/128);
	    esp_tempW4 = (uint16_t)( (((uint32_t)ab_yawc_o)*88)/128);   0.01 ; Fc = 7Hz 
	    ab_yawc = tempW3 + tempW4;           0.01     */  
	/*    ab_yawc = LCESP_u16Lpf1Int(esp_tempW1+esp_tempW2, ab_yawc_o, 40);  0.01       */


	/* jgkim 20040210  Variable filter for Speed */
  

/*    if(!FLAG_TIME_OPT) 
    {                                     
    	// LOW_&&_MED MU FOR VARING SPEED                //
    	esp_tempW3 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_M_SPEED_V1, FILTER_GAIN_FOR_M_A_V1_IN_YAW_LINIT_ENTER,      
    	                                  S16_YAW_LIMIT_M_SPEED_V2, FILTER_GAIN_FOR_M_A_V2_IN_YAW_LINIT_ENTER,
    	                                  S16_YAW_LIMIT_M_SPEED_V3, FILTER_GAIN_FOR_M_A_V3_IN_YAW_LINIT_ENTER); 
    	                                                    
    	// HIGH MU FOR VARING SPEED  //
    	esp_tempW4 = LCESP_s16IInter3Point( vrefk, S16_YAW_LIMIT_H_SPEED_V1, FILTER_GAIN_FOR_H_A_V1_IN_YAW_LINIT_ENTER,      
    	                                  S16_YAW_LIMIT_H_SPEED_V2, FILTER_GAIN_FOR_H_A_V2_IN_YAW_LINIT_ENTER,
    	                                  S16_YAW_LIMIT_H_SPEED_V3, FILTER_GAIN_FOR_H_A_V3_IN_YAW_LINIT_ENTER); 
    	
    	// Total Gain For Speed//
    	filter_gain_for_speed_in_yaw_limit_enter = LCESP_s16IInter2Point( det_alatm, S16_MU_MED_HIGH, esp_tempW3,
    	                                      S16_MU_HIGH, esp_tempW4);  
    	                                      // esp_tempW0 --> yaw_limit_enter ( 횡g 차 )
    }
    else 
    {    
    	;
    }  */      

    /*  '06.04.18 , Filter adjustment */

         filter_gain_for_speed_in_yaw_limit_enter = 127;       
         
    ab_yawc = (int16_t)LCESP_u16Lpf1Int( (uint16_t)(esp_tempW1+esp_tempW2), (uint16_t)ab_yawc_o, (uint8_t)filter_gain_for_speed_in_yaw_limit_enter);   

    if ( YAW_CHANGE_LIMIT==1 ) 
    {

        if (ABS_fz==1) 
        {
        	esp_tempW1 = 0;
        }
        else        
        {
        	esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_LIMIT_WEIGHT_H, 
                                    S16_LIMIT_WEIGHT_M, S16_LIMIT_WEIGHT_L); 
        }       
                    /* 0 % => NO FILTER, 100 % => FILTER ( YAW_C )*/
        
        esp_tempW2 = McrAbs( 100 - esp_tempW1 );
		/*      esp_tempW0 = (int16_t)(( (((int32_t)yawno)*esp_tempW2) + (((int32_t)rf)*(100-esp_tempW2)) )/100);*/
        /*tempW2 = yawno; 
        tempW1 = esp_tempW2; 
        tempW0 = 100; 
        s16muls16divs16();
        esp_tempW9 = tempW3;        
        tempW2 = rf;
        tempW1 = 100-esp_tempW2; 
        tempW0 = 100;
        s16muls16divs16();      
        esp_tempW0 = esp_tempW9 + tempW3;*/            

        esp_tempW9 = (int16_t)( (((int32_t)yawno)*esp_tempW2)/100 );    
           

        esp_tempW0 = esp_tempW9 + (int16_t)( (((int32_t)rf)*(100-esp_tempW2))/100 );  
              
        esp_tempW1 = McrAbs(esp_tempW0); 
        

        if ( esp_tempW1 > ab_yawc ) 
        {

		/*        if ( YAW_CHANGE_LIMIT )                    */
		/*        Only : decrease possible                   */
		/*                                                   */
		/*    if( ab_yawc >  ab_yawc_o ) ab_yawc = ab_yawc_o;*/
          
            rf2_old=rf2;
            rf2 = ab_yawc;
            YAW_CHANGE_LIMIT_ACT = 1;
        } 
        else   
        {
            rf2_old=rf2;
            rf2 = esp_tempW1;
            YAW_CHANGE_LIMIT_ACT = 0;
        }
                    
        if ( esp_tempW0 < 0 ) 
        {
        	rf2 = -rf2;
        }
        else 
        {    
        	;
        }    
        
        
    } 
    else 
    {
                 
      /*  if ( ESP_MODEL_CHANGE ) 
        {
            rf2_old=rf2;

            if (rf >=0) 
            {
            	rf2 = rf - model_change_value;
            }
            else 
            {
            	rf2 = rf + model_change_value;
            }

            rf2 = LCESP_s16Lpf1Int(rf2, rf2_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);  /* 3 Hz ( 17 )*/

		/*      } else if(FLAG_DETECT_DRIFT &&(vrefk > yaw_limit_exit_speed)) {*/ 
		/*                                                                     */
		/*          K_Jturn_Uturn = LCESP_s16IInter4Point( det_alatm,          */
		/*          S16_MU_LOW,     S16_DRIFT_YAWC_DOWN_GAIN_L,                */
		/*          S16_MU_MED,     S16_DRIFT_YAWC_DOWN_GAIN_M,                */
		/*          S16_MU_MED_HIGH,    S16_DRIFT_YAWC_DOWN_GAIN_M,            */
		/*          S16_MU_HIGH,    S16_DRIFT_YAWC_DOWN_GAIN_H);               */
		/*      K_Jturn_Uturn = 100;                                           */
		/*      rf2_old=rf2;                                                   */
		/*      // rf2 = (int16_t)(((int32_t)K_Jturn_Uturn*rf)/100);                  */
		/*      tempW2 = K_Jturn_Uturn; tempW1 = rf; tempW0 = 100;             */
		/*      s16muls16divs16();                                             */
		/*      rf2 = tempW3;                                                  */
		/*                                                                     */
		/*                                                                     */
		/*          rf2 = LCESP_s16Lpf1Int(rf2, rf2_old, 40);                  */


       /* } 
        else 
        { */
            rf2_old=rf2;
            rf2 = rf;
 
            
        	if ( rf2 > (rf + 6) )        
        	{
        		rf2 = rf2 - 5;
        	}
        	else if ( rf2 < (rf -6) )    
        	{
        		rf2 = rf2 + 5;
        	}
        	else                       
        	{
        		rf2 = rf;        
        	}
        
      /*  }         */

        YAW_CHANGE_LIMIT_ACT = 0;
                
    }
    
    if( vrefk < yaw_limit_exit_speed ) 
    {
            if( rf2 > (rf + 11) ) 
            {
            	rf2 = rf2 - 10;
            }
            else if ( rf2 < (rf - 11) ) 
            {
            	rf2 = rf2 + 10;
            }
            else 
            {
            	rf2 = rf;
            }
    } 
    else
    {
    	;
    }
    
    
    
 
	/*  if( (McrAbs(rf2-rf)<100)&&(k_mu==100)) 
		{
		  	YAW_CHANGE_LIMIT = 0;
		}
	    else if( (McrAbs(rf2-rf)<100) && (det_wstr_dot_lf<100) 
	                        && (abs_wstr<100) ) 
	    {
	    	YAW_CHANGE_LIMIT = 0;   
	    }
	    else 
	    {    
	    	;
	    }    
	*/
                       
        
    delta_rf2_rf = McrAbs(rf2-rf);
    
    
    if( (delta_rf2_rf<100) 
        && ( (k_mu==100) || ( (det_wstr_dot_lf<100) 
                        && (abs_wstr<100) )  ) ) 
    {
    	YAW_CHANGE_LIMIT=0;
    }
    else 
    {    
    	;
    }    
    
}

#endif


#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPDetectRoad
	#include "Mdyn_autosar.h"
#endif
