#ifndef __DDS_STRUCT_H__
#define __DDS_STRUCT_H__
#include"LVarHead.h"

#if (__DDS == ENABLE)

#define __2WHL_Deflation_Detect 1
#define __3WHL_Deflation_Detect 1
#define __Hybrid_DDS    1

#define ldddsu1DeflationState        	DDS_1.bit7       
#define ldddsu1StdStrDrvStateForDDS  	DDS_1.bit6       
#define ldddsu1CompleteCalib         	DDS_1.bit5
#define read_dds_eeprom_end_flg     	DDS_1.bit4                         
#define write_dds_eeprom_end_flg     	DDS_1.bit3                       
#define ldddsu1CompleteCalibAllSpeed    DDS_1.bit2                     
#define ldddsu1PerformedDDSReset     	DDS_1.bit1                       
#define ldddsu1RequestDDSReset       	DDS_1.bit0                       
                       
                       
#define ldddsu1RunningAvgUpdateFlag        	DDS_2.bit7       
#define ldddsu1Diag2whlDeflationFLRR        DDS_2.bit6       
#define ldddsu1Diag2whlDeflationFRRL     	DDS_2.bit5
#define ldddsu1ElapsedTimeUpdateFlgVeh     	DDS_2.bit4                         //
#define ldddsu1TireWearInCompleted          DDS_2.bit3                       
#define DDS2_Not_Used_Flag_2           		DDS_2.bit2    //ldddsu1DDSLoggingStartFlag                 
#define DDS2_Not_Used_Flag_1           		DDS_2.bit1                       
#define DDS2_Not_Used_Flag_0    			DDS_2.bit0                     

#define ldddsu1ElapsedTimeUpdateFlgOldFL 	DDS_3.bit7
#define ldddsu1ElapsedTimeUpdateFlgOldFR 	DDS_3.bit6
#define ldddsu1ElapsedTimeUpdateFlgOldRL 	DDS_3.bit5
#define ldddsu1ElapsedTimeUpdateFlgOldRR 	DDS_3.bit4
#define ldddsu8ElapsedTimeUpdateFlgFL_K 	DDS_3.bit3
#define ldddsu8ElapsedTimeUpdateFlgFR_K 	DDS_3.bit2
#define ldddsu8ElapsedTimeUpdateFlgRL_K 	DDS_3.bit1
#define ldddsu8ElapsedTimeUpdateFlgRR_K 	DDS_3.bit0

#define ldddsu1LowTirePressStateFlgFL 	DDS_4.bit7
#define ldddsu1LowTirePressStateFlgFR 	DDS_4.bit6
#define ldddsu1LowTirePressStateFlgRL 	DDS_4.bit5
#define ldddsu1LowTirePressStateFlgRR 	DDS_4.bit4
#define ldddsu1ElapsedTimeUpdateFlgFL 	DDS_4.bit3
#define ldddsu1ElapsedTimeUpdateFlgFR 	DDS_4.bit2
#define ldddsu1ElapsedTimeUpdateFlgRL 	DDS_4.bit1
#define ldddsu1ElapsedTimeUpdateFlgRR 	DDS_4.bit0
      
      
#if __Hybrid_DDS
#define ldddshu1RefTirePressureV     DDSh_1.bit7
#define ldddshu1DeflationByHybridDds DDSh_1.bit6
#define ddsh_eep_wr_ok_flg1 DDSh_1.bit5
#define ddsh_eep_wr_ok_flg2 DDSh_1.bit4
#define ddsh_eep_wr_ok_flg3 DDSh_1.bit3
#define ddsh_eep_read_ok_flg1 DDSh_1.bit2
#define ddsh_eep_read_ok_flg2 DDSh_1.bit1
#define ddsh_eep_read_ok_flg3 DDSh_1.bit0

#define ldddshu1DefByHybridDdsFL     DDSh_2.bit7
#define ldddshu1DefByHybridDdsFR 	DDSh_2.bit6
#define ldddshu1DefByHybridDdsRL 	DDSh_2.bit5
#define ldddshu1DefByHybridDdsRR 	DDSh_2.bit4
#define Not_used_DDSh2_3 DDSh_2.bit3
#define Not_used_DDSh2_2 DDSh_2.bit2
#define Not_used_DDSh2_1 DDSh_2.bit1
#define Not_used_DDSh2_0 DDSh_2.bit0
#endif             

#define dds_eep_wr_ok_flg1 	DDS_WR_OK_FLG1.bit7
#define dds_eep_wr_ok_flg2 	DDS_WR_OK_FLG1.bit6
#define dds_eep_wr_ok_flg3 	DDS_WR_OK_FLG1.bit5
#define dds_eep_wr_ok_flg4 	DDS_WR_OK_FLG1.bit4
#define dds_eep_wr_ok_flg5 	DDS_WR_OK_FLG1.bit3
#define dds_eep_wr_ok_flg6 	DDS_WR_OK_FLG1.bit2
#define dds_eep_wr_ok_flg7 	DDS_WR_OK_FLG1.bit1
#define dds_eep_wr_ok_flg8 	DDS_WR_OK_FLG1.bit0

#define dds_eep_wr_ok_flg9 		DDS_WR_OK_FLG2.bit7
#define dds_eep_wr_ok_flg10 	DDS_WR_OK_FLG2.bit6
#define dds_eep_wr_ok_flg11 	DDS_WR_OK_FLG2.bit5
#define dds_eep_wr_ok_flg12 	DDS_WR_OK_FLG2.bit4
#define dds_eep_wr_ok_flg13 	DDS_WR_OK_FLG2.bit3
#define dds_eep_wr_ok_flg14 	DDS_WR_OK_FLG2.bit2
#define dds_eep_wr_ok_flg15 	DDS_WR_OK_FLG2.bit1
#define dds_eep_wr_ok_flg16 	DDS_WR_OK_FLG2.bit0

#define dds_eep_wr_ok_flg17 	DDS_WR_OK_FLG3.bit7
#define dds_eep_wr_ok_flg18 	DDS_WR_OK_FLG3.bit6
#define dds_eep_wr_ok_flg19 	DDS_WR_OK_FLG3.bit5
#define dds_eep_wr_ok_flg20 	DDS_WR_OK_FLG3.bit4
#define dds_eep_wr_ok_flg21 	DDS_WR_OK_FLG3.bit3
#define dds_eep_wr_ok_flg22 	DDS_WR_OK_FLG3.bit2
#define dds_eep_wr_ok_flg23 	DDS_WR_OK_FLG3.bit1
#define dds_eep_wr_ok_flg24 	DDS_WR_OK_FLG3.bit0

#define dds_eep_wr_ok_flg25 	DDS_WR_OK_FLG4.bit7
#define dds_eep_wr_ok_flg26 	DDS_WR_OK_FLG4.bit6
#define dds_eep_wr_ok_flg27 	DDS_WR_OK_FLG4.bit5
#define dds_eep_wr_ok_flg28 	DDS_WR_OK_FLG4.bit4
#define dds_eep_wr_ok_flg29 	DDS_WR_OK_FLG4.bit3
#define dds_eep_wr_ok_flg30 	DDS_WR_OK_FLG4.bit2
#define dds_eep_wr_ok_flg31 	DDS_WR_OK_FLG4.bit1
#define dds_eep_wr_ok_flg32 	DDS_WR_OK_FLG4.bit0

#define dds_eep_wr_ok_flg33 	DDS_WR_OK_FLG5.bit7
#define dds_eep_wr_ok_flg34 	DDS_WR_OK_FLG5.bit6
#define dds_eep_wr_ok_flg35 	DDS_WR_OK_FLG5.bit5
#define dds_eep_wr_ok_flg36 	DDS_WR_OK_FLG5.bit4
#define dds_eep_wr_ok_flg37 	DDS_WR_OK_FLG5.bit3
#define dds_eep_wr_ok_flg38 	DDS_WR_OK_FLG5.bit2
#define dds_eep_wr_ok_flg39 	DDS_WR_OK_FLG5.bit1
#define dds_eep_wr_ok_flg40 	DDS_WR_OK_FLG5.bit0

#define dds_eep_wr_ok_flg41 	DDS_WR_OK_FLG6.bit7
#define dds_eep_wr_ok_flg42 	DDS_WR_OK_FLG6.bit6
#define dds_eep_wr_ok_flg43 	DDS_WR_OK_FLG6.bit5
#define dds_eep_wr_ok_flg44 	DDS_WR_OK_FLG6.bit4
#define dds_eep_wr_ok_flg45 	DDS_WR_OK_FLG6.bit3
#define dds_eep_wr_ok_flg46 	DDS_WR_OK_FLG6.bit2
#define dds_eep_wr_ok_flg47 	DDS_WR_OK_FLG6.bit1
#define dds_eep_wr_ok_flg48 	DDS_WR_OK_FLG6.bit0

#define dds_eep_wr_ok_flg49 	DDS_WR_OK_FLG7.bit7
#define dds_eep_wr_ok_flg50 	DDS_WR_OK_FLG7.bit6
#define dds_eep_wr_ok_flg51 	DDS_WR_OK_FLG7.bit5
#define dds_eep_wr_ok_flg52 	DDS_WR_OK_FLG7.bit4
#define dds_eep_wr_ok_flg53 	DDS_WR_OK_FLG7.bit3
#define dds_eep_wr_ok_flg54 	DDS_WR_OK_FLG7.bit2
#define dds_eep_wr_ok_flg55 	DDS_WR_OK_FLG7.bit1
#define dds_eep_wr_ok_flg56 	DDS_WR_OK_FLG7.bit0

#define dds_eep_wr_ok_flg57 	DDS_WR_OK_FLG8.bit7
#define dds_eep_wr_ok_flg58 	DDS_WR_OK_FLG8.bit6
#define dds_eep_wr_ok_flg59 	DDS_WR_OK_FLG8.bit5
#define dds_eep_wr_ok_flg60 	DDS_WR_OK_FLG8.bit4
#define dds_eep_wr_ok_flg61 	DDS_WR_OK_FLG8.bit3
#define dds_eep_wr_ok_flg62 	DDS_WR_OK_FLG8.bit2
#define dds_eep_wr_ok_flg63 	DDS_WR_OK_FLG8.bit1
#define dds_eep_wr_ok_flg64 	DDS_WR_OK_FLG8.bit0

#define dds_eep_read_ok_flg1 	DDS_READ_OK_FLG1.bit7
#define dds_eep_read_ok_flg2 	DDS_READ_OK_FLG1.bit6
#define dds_eep_read_ok_flg3 	DDS_READ_OK_FLG1.bit5
#define dds_eep_read_ok_flg4 	DDS_READ_OK_FLG1.bit4
#define dds_eep_read_ok_flg5 	DDS_READ_OK_FLG1.bit3
#define dds_eep_read_ok_flg6 	DDS_READ_OK_FLG1.bit2
#define dds_eep_read_ok_flg7 	DDS_READ_OK_FLG1.bit1
#define dds_eep_read_ok_flg8 	DDS_READ_OK_FLG1.bit0

#define dds_eep_read_ok_flg9 	DDS_READ_OK_FLG2.bit7
#define dds_eep_read_ok_flg10 	DDS_READ_OK_FLG2.bit6
#define dds_eep_read_ok_flg11 	DDS_READ_OK_FLG2.bit5
#define dds_eep_read_ok_flg12 	DDS_READ_OK_FLG2.bit4
#define dds_eep_read_ok_flg13 	DDS_READ_OK_FLG2.bit3
#define dds_eep_read_ok_flg14 	DDS_READ_OK_FLG2.bit2
#define dds_eep_read_ok_flg15 	DDS_READ_OK_FLG2.bit1
#define dds_eep_read_ok_flg16 	DDS_READ_OK_FLG2.bit0

#define dds_eep_read_ok_flg17 	DDS_READ_OK_FLG3.bit7
#define dds_eep_read_ok_flg18 	DDS_READ_OK_FLG3.bit6
#define dds_eep_read_ok_flg19 	DDS_READ_OK_FLG3.bit5
#define dds_eep_read_ok_flg20 	DDS_READ_OK_FLG3.bit4
#define dds_eep_read_ok_flg21 	DDS_READ_OK_FLG3.bit3
#define dds_eep_read_ok_flg22 	DDS_READ_OK_FLG3.bit2
#define dds_eep_read_ok_flg23 	DDS_READ_OK_FLG3.bit1
#define dds_eep_read_ok_flg24 	DDS_READ_OK_FLG3.bit0

#define dds_eep_read_ok_flg25 	DDS_READ_OK_FLG4.bit7
#define dds_eep_read_ok_flg26 	DDS_READ_OK_FLG4.bit6
#define dds_eep_read_ok_flg27 	DDS_READ_OK_FLG4.bit5
#define dds_eep_read_ok_flg28 	DDS_READ_OK_FLG4.bit4
#define dds_eep_read_ok_flg29 	DDS_READ_OK_FLG4.bit3
#define dds_eep_read_ok_flg30 	DDS_READ_OK_FLG4.bit2
#define dds_eep_read_ok_flg31 	DDS_READ_OK_FLG4.bit1
#define dds_eep_read_ok_flg32 	DDS_READ_OK_FLG4.bit0

#define dds_eep_read_ok_flg33 	DDS_READ_OK_FLG5.bit7
#define dds_eep_read_ok_flg34 	DDS_READ_OK_FLG5.bit6
#define dds_eep_read_ok_flg35 	DDS_READ_OK_FLG5.bit5
#define dds_eep_read_ok_flg36 	DDS_READ_OK_FLG5.bit4
#define dds_eep_read_ok_flg37 	DDS_READ_OK_FLG5.bit3
#define dds_eep_read_ok_flg38 	DDS_READ_OK_FLG5.bit2
#define dds_eep_read_ok_flg39 	DDS_READ_OK_FLG5.bit1
#define dds_eep_read_ok_flg40 	DDS_READ_OK_FLG5.bit0

#define dds_eep_read_ok_flg41 	DDS_READ_OK_FLG6.bit7
#define dds_eep_read_ok_flg42 	DDS_READ_OK_FLG6.bit6
#define dds_eep_read_ok_flg43 	DDS_READ_OK_FLG6.bit5
#define dds_eep_read_ok_flg44 	DDS_READ_OK_FLG6.bit4
#define dds_eep_read_ok_flg45 	DDS_READ_OK_FLG6.bit3
#define dds_eep_read_ok_flg46 	DDS_READ_OK_FLG6.bit2
#define dds_eep_read_ok_flg47 	DDS_READ_OK_FLG6.bit1
#define dds_eep_read_ok_flg48 	DDS_READ_OK_FLG6.bit0

#define dds_eep_read_ok_flg49 	DDS_READ_OK_FLG7.bit7
#define dds_eep_read_ok_flg50 	DDS_READ_OK_FLG7.bit6
#define dds_eep_read_ok_flg51 	DDS_READ_OK_FLG7.bit5
#define dds_eep_read_ok_flg52 	DDS_READ_OK_FLG7.bit4
#define dds_eep_read_ok_flg53 	DDS_READ_OK_FLG7.bit3
#define dds_eep_read_ok_flg54 	DDS_READ_OK_FLG7.bit2
#define dds_eep_read_ok_flg55 	DDS_READ_OK_FLG7.bit1
#define dds_eep_read_ok_flg56 	DDS_READ_OK_FLG7.bit0

#define dds_eep_read_ok_flg57 	DDS_READ_OK_FLG8.bit7
#define dds_eep_read_ok_flg58 	DDS_READ_OK_FLG8.bit6
#define dds_eep_read_ok_flg59 	DDS_READ_OK_FLG8.bit5
#define dds_eep_read_ok_flg60 	DDS_READ_OK_FLG8.bit4       
#define dds_eep_read_ok_flg61 	DDS_READ_OK_FLG8.bit3
#define dds_eep_read_ok_flg62 	DDS_READ_OK_FLG8.bit2
#define dds_eep_read_ok_flg63 	DDS_READ_OK_FLG8.bit1
#define dds_eep_read_ok_flg64 	DDS_READ_OK_FLG8.bit0

#define EEP_SECTOR_OFFSET_DDS		0x00

#define U16_DDS_EERPOM_DATA_1		(0x200+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg1  0x200,(UINT)ldddsu16CntCalibTotalRev[0])==0)    
#define U16_DDS_EERPOM_DATA_2		(0x204+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg2  0x204,(UINT)ldddsu16CntCalibTotalRev[1])==0)    
#define U16_DDS_EERPOM_DATA_3		(0x208+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg3  0x208,(UINT)ldddsu16CntCalibTotalRev[2])==0)    
#define U16_DDS_EERPOM_DATA_4		(0x20C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg4  0x20c,(UINT)ldddsu16CntCalibTotalRev[3])==0)    
#define U16_DDS_EERPOM_DATA_5		(0x210+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg5  0x210,(UINT)ldddsu16CntCalibTotalRev[4])==0)    
#define U16_DDS_EERPOM_DATA_6		(0x214+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg6  0x214,(UINT)ldddss16CumulativeMean[0][0])==0)   
#define U16_DDS_EERPOM_DATA_7		(0x218+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg7  0x218,(UINT)ldddss16CumulativeMean[0][1])==0)   
#define U16_DDS_EERPOM_DATA_8		(0x21C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg8  0x21c,(UINT)ldddss16CumulativeMean[0][2])==0)   
#define U16_DDS_EERPOM_DATA_9		(0x220+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg9  0x220,(UINT)ldddss16CumulativeMean[0][3])==0)   
#define U16_DDS_EERPOM_DATA_10		(0x224+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg10 0x224,(UINT)ldddss16CumulativeMean[0][4])==0)   
#define U16_DDS_EERPOM_DATA_11		(0x228+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg11 0x228,(UINT)ldddss16CumulativeMean[1][0])==0)   
#define U16_DDS_EERPOM_DATA_12		(0x22C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg12 0x22c,(UINT)ldddss16CumulativeMean[1][1])==0)   
#define U16_DDS_EERPOM_DATA_13		(0x230+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg13 0x230,(UINT)ldddss16CumulativeMean[1][2])==0)   
#define U16_DDS_EERPOM_DATA_14		(0x234+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg14 0x234,(UINT)ldddss16CumulativeMean[1][3])==0)   
#define U16_DDS_EERPOM_DATA_15		(0x238+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg15 0x238,(UINT)ldddss16CumulativeMean[1][4])==0)   
#define U16_DDS_EERPOM_DATA_16		(0x23C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg16 0x23c,(UINT)ldddss16CumulativeMean[2][0])==0)   
#define U16_DDS_EERPOM_DATA_17		(0x240+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg17 0x240,(UINT)ldddss16CumulativeMean[2][1])==0)   
#define U16_DDS_EERPOM_DATA_18		(0x244+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg18 0x244,(UINT)ldddss16CumulativeMean[2][2])==0)   
#define U16_DDS_EERPOM_DATA_19		(0x248+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg19 0x248,(UINT)ldddss16CumulativeMean[2][3])==0)   
#define U16_DDS_EERPOM_DATA_20		(0x24C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg20 0x24c,(UINT)ldddss16CumulativeMean[2][4])==0)   
#define U16_DDS_EERPOM_DATA_21		(0x250+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg21 0x250,(UINT)ldddss16CumulativeMean[3][0])==0)   
#define U16_DDS_EERPOM_DATA_22		(0x254+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg22 0x254,(UINT)ldddss16CumulativeMean[3][1])==0)   
#define U16_DDS_EERPOM_DATA_23		(0x258+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg23 0x258,(UINT)ldddss16CumulativeMean[3][2])==0)   
#define U16_DDS_EERPOM_DATA_24		(0x25C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg24 0x25c,(UINT)ldddss16CumulativeMean[3][3])==0)   
#define U16_DDS_EERPOM_DATA_25		(0x260+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg25 0x260,(UINT)ldddss16CumulativeMean[3][4])==0)   
#define U16_DDS_EERPOM_DATA_26		(0x264+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg26 0x264,(UINT)ldddss16Dct1WhlDefCnt[0][0])==0)    
#define U16_DDS_EERPOM_DATA_27		(0x268+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg27 0x268,(UINT)ldddss16Dct1WhlDefCnt[0][1])==0)    
#define U16_DDS_EERPOM_DATA_28		(0x26C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg28 0x26c,(UINT)ldddss16Dct1WhlDefCnt[0][2])==0)    
#define U16_DDS_EERPOM_DATA_29		(0x270+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg29 0x270,(UINT)ldddss16Dct1WhlDefCnt[0][3])==0)    
#define U16_DDS_EERPOM_DATA_30		(0x274+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg30 0x274,(UINT)ldddss16Dct1WhlDefCnt[0][4])==0)    
#define U16_DDS_EERPOM_DATA_31		(0x278+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg31 0x278,(UINT)ldddss16TireWereInMileageLeft)==0)  
#define U16_DDS_EERPOM_DATA_32		(0x27C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg32 0x27c,(UINT)ldddss16TireWereInMileageRight)==0) 
#define U16_DDS_EERPOM_DATA_33		(0x280+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg33 0x280,(UINT)ldddss16Dct1WhlDefCnt[1][0])==0)    
#define U16_DDS_EERPOM_DATA_34		(0x284+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg34 0x284,(UINT)ldddss16Dct1WhlDefCnt[1][1])==0)    
#define U16_DDS_EERPOM_DATA_35		(0x288+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg35 0x28c,(UINT)ldddss16Dct1WhlDefCnt[1][2])==0)    
#define U16_DDS_EERPOM_DATA_36		(0x28C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg36 0x290,(UINT)ldddss16Dct1WhlDefCnt[1][3])==0)    
#define U16_DDS_EERPOM_DATA_37		(0x290+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg37 0x294,(UINT)ldddss16Dct1WhlDefCnt[1][4])==0)    
#define U16_DDS_EERPOM_DATA_38		(0x294+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg38 0x298,(UINT)ldddss16RollingCntLeft)==0)         
#define U16_DDS_EERPOM_DATA_39		(0x298+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg39 0x29c,(UINT)ldddss16RollingCntRight)==0)        
#define U16_DDS_EERPOM_DATA_40		(0x29C+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg40 0x2a0,(UINT)ldddsu1DeflationState)==0)          
#define U16_DDS_EERPOM_DATA_41		(0x2a0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg41 0x2a4,(UINT)ldddss16Dct3WhlDefCnt[0][0])==0)    
#define U16_DDS_EERPOM_DATA_42		(0x2a4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg42 0x2a8,(UINT)ldddss16Dct3WhlDefCnt[1][0])==0)    
#define U16_DDS_EERPOM_DATA_43		(0x2a8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg43 0x2ac,(UINT)ldddss16Dct3WhlDefCnt[2][0])==0)    
#define U16_DDS_EERPOM_DATA_44		(0x2aC+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg44 0x2b0,(UINT)ldddss16Dct3WhlDefCnt[3][0])==0)    
#define U16_DDS_EERPOM_DATA_45		(0x2b0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg45 0x2b4,(UINT)ldddsu1TireWearInCompleted)==0)  
#define U16_DDS_EERPOM_DATA_46		(0x2b4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg46 0x2b8,(UINT)ldddss16Dct1WhlDefCnt[2][0])==0) 
#define U16_DDS_EERPOM_DATA_47		(0x2b8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg47 0x2bc,(UINT)ldddss16Dct1WhlDefCnt[2][1])==0) 
#define U16_DDS_EERPOM_DATA_48		(0x2bC+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg48 0x2c0,(UINT)ldddss16Dct1WhlDefCnt[2][2])==0) 
#define U16_DDS_EERPOM_DATA_49		(0x2c0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg49 0x2c4,(UINT)ldddss16Dct1WhlDefCnt[2][3])==0)    
#define U16_DDS_EERPOM_DATA_50		(0x2c4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg50 0x2c8,(UINT)ldddss16Dct1WhlDefCnt[2][4])==0)    
#define U16_DDS_EERPOM_DATA_51		(0x2c8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg51 0x2cc,(UINT)ldddss16Dct1WhlDefCnt[3][0])==0)    
#define U16_DDS_EERPOM_DATA_52		(0x2cC+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg52 0x2d0,(UINT)ldddss16Dct1WhlDefCnt[3][1])==0)    
#define U16_DDS_EERPOM_DATA_53		(0x2d0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg53 0x2d4,(UINT)ldddss16Dct1WhlDefCnt[3][2])==0)    
#define U16_DDS_EERPOM_DATA_54		(0x2d4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg54 0x2d8,(UINT)ldddss16Dct1WhlDefCnt[3][3])==0)    
#define U16_DDS_EERPOM_DATA_55		(0x2d8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg55 0x2dc,(UINT)ldddss16Dct1WhlDefCnt[3][4])==0)    
#define U16_DDS_EERPOM_DATA_56		(0x2dC+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg56 0x2e0,(UINT)ldu8DDSModeEepromValue)==0)         
#define U16_DDS_EERPOM_DATA_57		(0x2e0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg57 0x2e4,(UINT)ldddsu8DeflationStateFlags)==0)     
#define U16_DDS_EERPOM_DATA_58		(0x2e4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg58 0x2e8,(UINT)ldddss16DctFLRRDefCnt)==0)          
#define U16_DDS_EERPOM_DATA_59		(0x2e8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg59 0x2ec,(UINT)ldddss16DctFRRLDefCnt)==0)          
#define U16_DDS_EERPOM_DATA_60		(0x2eC+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg60 0x2f0,(UINT)ldddss16Dct3WhlDefCnt[0][1])==0)    
#define U16_DDS_EERPOM_DATA_61		(0x2f0+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg61 0x2f4,(UINT)ldddss16Dct3WhlDefCnt[1][1])==0)    
#define U16_DDS_EERPOM_DATA_62		(0x2f4+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg62 0x2f8,(UINT)ldddss16Dct3WhlDefCnt[2][1])==0)    
#define U16_DDS_EERPOM_DATA_63		(0x2f8+EEP_SECTOR_OFFSET_DDS)        // dds_eep_wr_ok_flg63 0x2fc,(UINT)ldddss16Dct3WhlDefCnt[3][1])==0)    

#if __Hybrid_DDS                                                                           
#define U16_DDSh_EERPOM_DATA_1		(0x2ec+0x102+0x04)
#define U16_DDSh_EERPOM_DATA_2      (U16_DDSh_EERPOM_DATA_1+0x04)
#define U16_DDSh_EERPOM_DATA_3      (U16_DDSh_EERPOM_DATA_2+0x04)
#endif
extern U8_BIT_STRUCT_t      DDS_1, DDS_2,DDS_3,DDS_4;
#if __hybrid_DDS
extern U8_BIT_STRUCT_t DDSh_1,DDSh_2;;
#endif

#if __ECU==ESP_ECU_1
//#pragma DATA_SEG __GPAGE_SEG PAGED_RAM
#endif
extern int8_t ldddss8TPMSWheelIndex;
extern int8_t ldddss8TPMSTireTemperature;
extern int8_t ldddss8TPMSTirePressure;

extern uint8_t ldu8DDSMode;
extern uint8_t GoodC_Flag;

extern uint8_t ldddsu8SingleWhlDeflation[4];
extern uint8_t ldddsu8Other3whlDeflation[4];
extern int16_t ldddss16Dct1WhlDefCnt[4][5];
extern int8_t ldddss8PressEstByAxleDiff[4]; 
extern int8_t ldddss8PressEstBySideDiff[4]; 
extern int8_t ldddss8PressEstByDiagDiff[4]; 
extern int16_t ldddss16DctFLRRDefCnt;
extern int16_t ldddss16DctFRRLDefCnt;
extern int16_t CalibrationValueFL;
extern int16_t CalibrationValueFR;
extern int16_t CalibrationValueRL;
extern int16_t CalibrationValueRR;
extern uint8_t ldddsu8RunningAvgUpdateWhlIndex[3];
extern int16_t   ldddss16Dct3WhlDefCnt[4][2];
extern uint8_t ldddsu8CompleteCalib[5];
#if __ECU==ESP_ECU_1	
//#pragma DATA_SEG DEFAULT
#endif

extern void LDDDS_vCallDetection(void);
extern void LSDDS_vWriteEEPROMDDSData(void);
extern void LDDDS_vDetTireLowPressureState(void);
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1

extern void LSDDS_vCallDDSLogData(void);
extern void	LSDDS_vCallDDSPlusLogData(void);
extern void LSDDS_vCallHybridDDSLogData(void);
    #endif
#endif

#endif
#endif