#ifndef __LDBTCSCALLDETECTION_H__
#define __LDBTCSCALLDETECTION_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition******************************************************/


/*Temperary Calibration Parameter Definition  *******************************************/

/*Definition of Developing Function ******************************************************/

/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/

/*Global Extern Functions  Declaration******************************************************/

extern void LDTCS_vCallTCMFDetection(void);
#endif