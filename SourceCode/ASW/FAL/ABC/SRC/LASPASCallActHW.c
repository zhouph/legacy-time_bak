/******************************************************************************
* Project Name: SPAS interface
* File: LASPASCallActHW.C
* Description: Valve/Motor actuaction by SPAS Brake intervention
* Date: June. 17. 2011
******************************************************************************/

/* Includes ******************************************************************/
#include "LASPASCallActHW.h"
#include "LCSPASCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
/******************************************************************************/

#if __SPAS_INTERFACE
/* Variables Definition*******************************************************/
struct	U8_BIT_STRUCT_t SPASF0;

int16_t lcs16SpasHoldTcCurrent;
int16_t lcs16Spas1stTargetPressCurrent, lcs16Spas2ndTargetPressCurrent, lcs16SpasTcCurThPressRate;
int16_t spas_msc_target_vol;

/* LocalFunction prototype ***************************************************/
void	LASPAS_vCallActHW(void);
void	LCMSC_vSetSPASTargetVoltage(void);
INT	    LAMFC_s16SetMFCCurrentSPAS(INT MFC_Current_old);
/* Implementation*************************************************************/

/******************************************************************************
* FUNCTION NAME:      LASPAS_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by SPAS Brake intervention
******************************************************************************/
void	LASPAS_vCallActHW(void)
{
	if(lcu1SpasActiveFlg==1)
	{
	  #if (__SPLIT_TYPE==0)	
		TCL_DEMAND_fl = SPAS_TCL_DEMAND_fl;
		TCL_DEMAND_fr = SPAS_TCL_DEMAND_fr;
		
		S_VALVE_LEFT = SPAS_S_VALVE_LEFT;
		S_VALVE_RIGHT = SPAS_S_VALVE_RIGHT;
		VDC_MOTOR_ON = SPAS_MOTOR_ON;
	  #else
		TCL_DEMAND_SECONDARY = SPAS_TCL_DEMAND_fl;
		TCL_DEMAND_PRIMARY = SPAS_TCL_DEMAND_fr;
		
		S_VALVE_SECONDARY = SPAS_S_VALVE_LEFT;
		S_VALVE_PRIMARY = SPAS_S_VALVE_RIGHT;
		VDC_MOTOR_ON = SPAS_MOTOR_ON;
	  #endif	

		VALVE_ACT = 1;
	}
	else { }
}

INT 	LAMFC_s16SetMFCCurrentSPAS(INT MFC_Current_old)
{
    static INT MFC_SPAS_Current_new;

    if(lcu1SpasActiveFlg==1)
    {
        lcs16SpasHoldTcCurrent = LCESP_s16IInter4Point(lcs16SpasReqTargetP, MPRESS_20BAR,   440, 
                                                                            MPRESS_40BAR,   575, 
          													                MPRESS_80BAR,   735,
                                                                            MPRESS_100BAR,  850);
                                                                                 	
        lcs16Spas1stTargetPressCurrent = lcs16SpasHoldTcCurrent;
        
        if(lcs16Spas1stTargetPressCurrent<=575)
        {
            lcs16Spas2ndTargetPressCurrent = 440;
            lcs16SpasTcCurThPressRate = 300;
        }
        else
        {
            lcs16Spas2ndTargetPressCurrent = 575;
            lcs16SpasTcCurThPressRate = 440;
        }
     	          	
    	if(lcs8SpasState==S8_SPAS_RISE)
    	{
    		if(lcu1SpasFFControl==1)      {MFC_SPAS_Current_new = S16_SPAS_TC_CUR_FF_RISE;}
            else if(lcu1SpasFBControl==1) {MFC_SPAS_Current_new = S16_SPAS_TC_CUR_FB_RISE;}	
            else                          {MFC_SPAS_Current_new = MFC_Current_old;}
        }
    	else if(lcs8SpasState==S8_SPAS_HOLD)
    	{
    		if(lcu16SpasHoldActCnt<S16_SPAS_1ST_HOLD_TIME)
    			{
    		     MFC_SPAS_Current_new = LCESP_s16IInter2Point(lcu16SpasHoldActCnt, 1, MFC_Current_old,
    		                                               S16_SPAS_PRESS_DOWN_TIME1, lcs16Spas1stTargetPressCurrent); 
    	    }
    	  else
    	  	{
    	  		   MFC_SPAS_Current_new = LCESP_s16IInter2Point(lcu16SpasHoldActCnt, S16_SPAS_1ST_HOLD_TIME, MFC_Current_old,
    		                                                                         S16_SPAS_PRESS_DOWN_TIME2, lcs16Spas2ndTargetPressCurrent);  	  		
    	  		
    	  	}
    	  	
    	}
		else if(lcs8SpasState==S8_SPAS_RELEASE)
    	{
    		if(lcu16SpasReleaseActCnt==1)
    		{
    		    MFC_SPAS_Current_new = lcs16Spas2ndTargetPressCurrent;
    		}
    		else if(lcu16SpasReleaseActCnt==2)
    		{
    		    MFC_SPAS_Current_new = lcs16Spas2ndTargetPressCurrent - S16_SPAS_TC_CUR_DROP_1ST;
    		}
    		else
    		{
    			if(MFC_Current_old > lcs16SpasTcCurThPressRate)
    			{
    				if((lcu16SpasReleaseActCnt%(INT)U8_SPAS_F_O_CYCLE_SCAN_REF1)==0)
    				{
    					MFC_SPAS_Current_new = MFC_Current_old - (INT)U8_SPAS_F_O_CUR_DROP1;
    				}
    				else
    				{
    					MFC_SPAS_Current_new = MFC_Current_old;
    				}
    			}
    			else
    			{
    				if((lcu16SpasReleaseActCnt%(INT)U8_SPAS_F_O_CYCLE_SCAN_REF2)==0)
    				{
    					MFC_SPAS_Current_new = MFC_Current_old - (INT)U8_SPAS_F_O_CUR_DROP2;
    				}
    				else
    				{
    					MFC_SPAS_Current_new = MFC_Current_old;
    				}
    			}
    		}
    		
    		if (MFC_SPAS_Current_new<150) {MFC_SPAS_Current_new = 150;}
		    else { }
    	}
    	else {MFC_SPAS_Current_new = 0;}  
    }
    else {MFC_SPAS_Current_new = 0;}
	
    return MFC_SPAS_Current_new;
}

void	LCMSC_vSetSPASTargetVoltage(void)
{
	if((lcu1SpasActiveFlg==1)&&(SPAS_MOTOR_ON==1))
	{
		if(lcu1SpasFFControl==1)
		{
		    spas_msc_target_vol = S16_SPAS_FF_MOTOR;
		}
		else if(lcu1SpasFBControl==1)
		{
			spas_msc_target_vol = LCESP_s16IInter2Point(lcs16SpasCtrlError, VREF_0_125_KPH,             S16_SPAS_FB_MOTOR_MIN,
			                                                                S16_SPAS_FF_CTRL_MIN_SPD,   S16_SPAS_FB_MOTOR_MAX);    
		}
		else
		{
		    spas_msc_target_vol = MSC_0_V;	
		}
	}
	else 
	{
		spas_msc_target_vol = MSC_0_V;
	}
}
#endif