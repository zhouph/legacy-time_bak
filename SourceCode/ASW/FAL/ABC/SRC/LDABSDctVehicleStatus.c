/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LDABSDctVehicleStatus.C
* Description: Vehicle Status Detection for ABS control
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSDctVehicleStatus
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LDABSDctVehicleStatus.h"
#include "LCABSDecideCtrlState.h"
#include "LCallMain.h"

/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/
uint8_t bend_detect_count;
int8_t  bend_slip_comp;
#if __BEND_DETECT_using_SIDEVREF
int8_t bend_detect_count_left;
int8_t bend_detect_count_right;
int8_t bend_detect_count_right2;
int8_t bend_detect_count_left2;
int16_t Side_vref_diff;
#endif

/* LocalFunction prototype ***************************************************/

static void LDABS_vDctBend(void);
#if (__VDC && __MGH40_ESP_BEND_DETECT)
static void LDABS_vDctBendESP(void);
static void LDABS_vDctBendESPPerDIR(struct SIDE_STRUCT*DIR,struct SIDE_STRUCT*OPP);
#endif
#if __BEND_DETECT_using_SIDEVREF
static void LDABS_vDctBendSideVref(void);
static int8_t LDABS_vSetSideBendCntBfBend(int16_t Out_Vref, int16_t In_Vref, int8_t SideBendCnt, struct W_STRUCT *pFO, struct W_STRUCT *pRO, struct W_STRUCT *pFI, struct W_STRUCT *pRI, int8_t BendLevel);
static int8_t LDABS_vSetSideBendCntAftBend(int16_t In_Vref, int16_t S_Vref_Diff, int16_t BendResetSVDiff, int8_t SideBendCnt, struct W_STRUCT *pFI, struct W_STRUCT *pRI);
#endif

#if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
static void LDABS_vDctLimitCornerBrake(struct W_STRUCT *pRH, const struct W_STRUCT *pFL, const struct W_STRUCT *pFH);
static void LDABS_vDctLimitCornerBrakeReset(void);
#endif

/* GlobalFunction prototype ***************************************************/


/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LDABS_vDctVehicleStatus
* CALLED BY:          LDABS_vCallDetection()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Vehicle Status Detection for ABS control
******************************************************************************/
void LDABS_vDctVehicleStatus(void)
{

	LDABS_vDctBend();
	
  #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
  
    if((yaw_out <= -YAW_3DEG)&&(wstr <= -WSTR_20_DEG))
    {
    	LDABS_vDctLimitCornerBrake(&RL,&FR,&FL);
    }	
    else if((yaw_out >= YAW_3DEG)&&(wstr >= WSTR_20_DEG))
    { 
    	LDABS_vDctLimitCornerBrake(&RR,&FL,&FR);
    }
    else{}
    
    LDABS_vDctLimitCornerBrakeReset();	
    
  #endif
	
}

/******************************************************************************
* FUNCTION NAME:      LDABS_vDctBend
* CALLED BY:          LDABSDctVehicleStatus()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Bend detection for ABS Control
******************************************************************************/

static void LDABS_vDctBend(void)
{
  #if (__VDC && __MGH40_ESP_BEND_DETECT)
    LDABS_vDctBendESP();
  #endif

  #if __BEND_DETECT_using_SIDEVREF
    LDABS_vDctBendSideVref();
  #endif /*__BEND_DETECT_using_SIDEVREF*/

  #if __REAR_D
    tempW2=McrAbs(vrad_crt_fl-vrad_crt_fr);
    tempW5=McrAbs(vrad_crt_rl-vrad_crt_rr);
  #else
    tempW2=McrAbs(vrad_crt_rl-vrad_crt_rr);
    tempW5=McrAbs(vrad_crt_fl-vrad_crt_fr);
  #endif
  
    if(tempW2 > (int16_t)VREF_5_KPH) {tempW2=(int16_t)VREF_5_KPH;}

/*
  #if __REAR_D
    tempW0=vrad_fl-vrad_fr;
  #else
    tempW0=vrad_rl-vrad_rr;
  #endif
    
  #if __REAR_D
    tempW4=vrad_rl-vrad_rr;
  #else
    tempW4=vrad_fl-vrad_fr;
  #endif

    tempW2=tempW0;
    if(tempW0 < 0) {tempW2=(int16_t)-tempW0;}           // speed diff 
    if(tempW2 > (int16_t)VREF_5_KPH) {tempW2=(int16_t)VREF_5_KPH;}


    tempW5= tempW4;
    if(tempW4 < 0) {tempW5=-tempW4;}           // slip diff
*/
    tempW8 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_100_KPH,VREF_2_KPH,VREF_1_25_KPH);
    tempW9 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_100_KPH,VREF_1_KPH,VREF_0_5_KPH);

    if(BEND_DETECT==0) {
/*    	
        if(vref <= (int16_t)VREF_120_KPH) {
            if((tempW2 > 16) && (tempW5 >= (int16_t)VREF_0_5_KPH)) {bend_detect_count = bend_detect_count + L_1SCAN;} // 2%
            else {bend_detect_count=0;}
        }
        else {
             if((tempW2 > 24) && (tempW5 >= (int16_t)VREF_1_KPH)) {bend_detect_count = bend_detect_count + L_1SCAN;} // 2%
             else {bend_detect_count=0;}
        }
*/        
        if((tempW2 > tempW8) && (tempW5 >= (int16_t)VREF_0_5_KPH)) {bend_detect_count = bend_detect_count + L_1SCAN;} /* 2% */
        else {bend_detect_count=0;}
    }
    else {
        if(ABS_fz==1)
        {
            tempW9 = tempW9 + VREF_1_KPH;
        }
      #if (__REAR_D==0)
        else if(EBD_RA==1)
        {
            tempW9 = tempW9 + VREF_0_5_KPH;
        }
      #endif
	    else if(lcabsu1BrakeByBLS==1)	
        {
            tempW9 = tempW9 + VREF_0_25_KPH;
        }
        else
        {
            ;
        }
        
		if(tempW9 > (tempW8 - VREF_0_125_KPH))
        {
            tempW9 = (tempW8 - VREF_0_125_KPH); /* hysteresis 0.125 kph */
        }

        if((tempW2 <= tempW9) || (tempW5 < (int16_t)VREF_0_5_KPH)) {bend_detect_count = bend_detect_count - L_1SCAN;} /* 3% */
        else {bend_detect_count=U8_BEND_DCT_TIME_PER_NDRIVEN_WL;}
    }

    if(bend_detect_count >=U8_BEND_DCT_TIME_PER_NDRIVEN_WL) {
        if((ABS_fz==0)&&(lcabsu1BrakeByBLS==0)) {BEND_DETECT=1;}            /*  MAIN03A */
        if(ABS_fz==0) {BEND_DETECT_EBD=1;}					/* EBD pulse down Noise(030922) */
    }
    else {

	#if (__4WD_VARIANT_CODE==ENABLE)
	 	if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
 		{
 			if((zyklus_z_fl >= 2)&&(zyklus_z_fr >= 2)) {tempW6=1;}
 			else {tempW6=0;}
 		}
 		else if(lsu8DrvMode==DM_2WD)
 		{
 			if(YMR_check_counter>L_TIME_50MS) {tempW6=1;}
 			else {tempW6=0;}
 		}
 		else 
 		{
 			if(YMR_check_counter>L_TIME_50MS) {tempW6=1;}
 			else {tempW6=0;}
 		}
	#else
	  #if __4WD	
		if((zyklus_z_fl >= 2)&&(zyklus_z_fr >= 2)) {tempW6=1;}
		else {tempW6=0;}
	  #else	
		if(YMR_check_counter>L_TIME_50MS) {tempW6=1;}
		else {tempW6=0;}
	  #endif
	#endif 	 	
		
        if(bend_detect_count <= L_1SCAN) {
        	if(ABS_fz==1) {
        		if(tempW6==1)
        		{
        		    BEND_DETECT=0;
        		    BEND_DETECT_EBD=0;
        		}
        	}
        	else {
        	    BEND_DETECT=0;
        	    BEND_DETECT_EBD=0;
        	}
        }        
    }


  #if (__VDC && __MGH40_ESP_BEND_DETECT)
    if(lcabsu1ValidYawStrLatG==1)
    {
        BEND_DETECT2=BEND_DETECT_ESP;
    }
    else
    {
        BEND_DETECT2=BEND_DETECT;
    }
  #else
    BEND_DETECT2=BEND_DETECT;
  #endif

    if((BEND_DETECT2==1) 
        #if __BEND_DETECT_using_SIDEVREF
    || ((((AFZ_OK==1)&&(afz<=-(int16_t)U8_PULSE_MEDIUM_MU)) || (ABS_fl==0) || (ABS_fr==0)) && (BEND_DETECT_flag==1))
        #endif
    ) 
    {
        tempW1=McrAbs(Side_vref_diff);
        /*
        tempW2=100;
        tempW0=vref;
        s16mul16div16();
        bend_slip_comp=(int8_t)(tempW3);*/
        bend_slip_comp = (int8_t)(((int32_t)tempW1*100)/vref);
    }
    else {
        bend_slip_comp=0;
    }
}

#if (__VDC && __MGH40_ESP_BEND_DETECT)
static void LDABS_vDctBendESP(void)
{
    LEFT.sign=1;
    RIGHT.sign=-1;
    
    if(lcabsu1ValidYawStrLatG==1)
    {

        if(wstr > WSTR_0_DEG)
        {LDABS_vDctBendESPPerDIR(&LEFT,&RIGHT);}
        else
        {LDABS_vDctBendESPPerDIR(&RIGHT,&LEFT);}
            
        if((LEFT.ESP_BEND_flag==1) || (RIGHT.ESP_BEND_flag==1)) {BEND_DETECT_ESP=1;}
        else {BEND_DETECT_ESP=0;}
   
    }
    else {BEND_DETECT_ESP=0;}
}

static void LDABS_vDctBendESPPerDIR(struct SIDE_STRUCT*DIR,struct SIDE_STRUCT*OPP)
{
    if(vref < VREF_30_KPH)
    {
        tempW7=YAW_20DEG;
        tempW8=YAW_10DEG;
    }
    else if(vref < VREF_50_KPH)
    {
        tempW7=YAW_15DEG;
        tempW8=YAW_7DEG;
    }
    else if(vref < VREF_80_KPH)
    {
        tempW7=YAW_7DEG;
        tempW8=YAW_4DEG;
    }
    else if(vref < VREF_100_KPH)
    {
        tempW7=YAW_7DEG;
        tempW8=YAW_3DEG;
    }
    else
    {
        tempW7=YAW_5DEG;
        tempW8=YAW_2DEG;
    }
     
    /********************************************************************/
    
    if(DIR->ESP_BEND_flag==0)
    {
        if((((int16_t)(DIR->sign)*(alat)) > (LAT_0G4G)) && (((int16_t)(DIR->sign)*(yaw_out)) > (tempW7)))
        {
            DIR->Bend_detect_cnt_ESP = DIR->Bend_detect_cnt_ESP + L_1SCAN;
        }
        else if((((int16_t)(DIR->sign)*(alat)) < (LAT_0G15G)) && (((int16_t)(DIR->sign)*(yaw_out)) < (tempW8)))
        {
            if(DIR->Bend_detect_cnt_ESP > L_1SCAN) {DIR->Bend_detect_cnt_ESP = DIR->Bend_detect_cnt_ESP - L_1SCAN;}
        }
        else{}
    }
    else
    {
        if((((int16_t)(DIR->sign)*(alat)) < (LAT_0G15G)) && (((int16_t)(DIR->sign)*(yaw_out)) < (tempW8)))
        {
            if(DIR->Bend_detect_cnt_ESP > L_1SCAN) {DIR->Bend_detect_cnt_ESP = DIR->Bend_detect_cnt_ESP - L_1SCAN;}
        }
    }
   
    if((DIR->Bend_detect_cnt_ESP) > L_TIME_70MS)
    {
        DIR->ESP_BEND_flag=1;
        OPP->Bend_detect_cnt_ESP=0;
    }
    else if((DIR->Bend_detect_cnt_ESP) <= L_TIME_10MS)
    {
        DIR->ESP_BEND_flag=0;
    }
    else{}
    
    /* reset opposite bend counter and flag */
    OPP->Bend_detect_cnt_ESP = 0;
    OPP->ESP_BEND_flag=0;
}

#endif /*(__VDC && __MGH40_ESP_BEND_DETECT)*/

#if __BEND_DETECT_using_SIDEVREF
static int8_t LDABS_vSetSideBendCntBfBend(int16_t Out_Vref, int16_t In_Vref, int8_t SideBendCnt, struct W_STRUCT *pFO, struct W_STRUCT *pRO, struct W_STRUCT *pFI, struct W_STRUCT *pRI, int8_t BendLevel)
{
    if(BendLevel==1)
    {
        tempW2 = VREF_3_KPH;
        tempW1 = VREF_4_KPH;
    }
    else
    {
        tempW2 = VREF_6_KPH;
        tempW1 = VREF_10_KPH;
    }        
    
    if(pFI->FSF==1)
    {
        tempB2 = ARAD_7G0;
    }
    else
    {
        tempB2 = ARAD_3G0;
    }
    
    if((((pFO->STABIL==1)||(pRO->STABIL==1))&&((Out_Vref-pFO->vrad)<=VREF_2_KPH)&&((Out_Vref-pRO->vrad)<=VREF_2_KPH)&&(pFO->peak_slip>-LAM_40P)&&(pRO->peak_slip>-LAM_30P)) &&
     ((((In_Vref-pFI->vrad)<=tempW2)&&(pFI->peak_dec>-ARAD_3G0)&&(pFI->peak_dec_alt>-ARAD_3G0)&&(pRI->peak_dec_alt>-tempB2))||
      (((In_Vref-pRI->vrad)<=tempW2)&&(pRI->peak_dec>-ARAD_3G0)&&(pRI->peak_dec_alt>-ARAD_3G0)&&(pFI->peak_dec_alt>-tempB2))))
    {
        SideBendCnt = SideBendCnt + L_1SCAN;
    }
    else if(((In_Vref-pFI->vrad)>=tempW1) && ((In_Vref-pRI->vrad)>=tempW1)) 
    {
        if(SideBendCnt>L_1SCAN) {SideBendCnt = SideBendCnt - L_1SCAN;}
        else { }
    }
    else { }
    
    return SideBendCnt;
}

static int8_t LDABS_vSetSideBendCntAftBend(int16_t In_Vref, int16_t S_Vref_Diff, int16_t BendResetSVDiff, int8_t SideBendCnt, struct W_STRUCT *pFI, struct W_STRUCT *pRI)
{
    if(pFI->FSF==1)
    {
        tempB2 = ARAD_5G0;
        tempB3 = L_TIME_70MS;/*10;*/
    }
    else
    {
        tempB2 = ARAD_3G0;
        tempB3 = L_TIME_30MS;/*5;*/
    }
    
    if(S_Vref_Diff<=BendResetSVDiff)
    {
        if(SideBendCnt>L_1SCAN) {SideBendCnt = SideBendCnt - L_1SCAN;}
        else { }
    }
    else if((((In_Vref-pFI->vrad)>=VREF_10_KPH) && ((In_Vref-pRI->vrad)>=VREF_10_KPH)) ||
            (((In_Vref-pFI->vrad)>=VREF_2_KPH) && (pFI->arad<-tempB2)) || 
            (((In_Vref-pRI->vrad)>=VREF_2_KPH) && (pRI->arad<-tempB2)))
    {
        if(SideBendCnt>L_1SCAN) {SideBendCnt = SideBendCnt - L_1SCAN;}
        else { }
    }
    else {SideBendCnt = tempB3;}
    
    return SideBendCnt;    
}

static void LDABS_vDctBendSideVref(void)
{
	Side_vref_diff=vref_L-vref_R;

  #if __REAR_D
    tempW0=vrad_crt_fl-vrad_crt_fr;
    tempW4=vrad_crt_rl-vrad_crt_rr;
  #else
    tempW0=vrad_crt_rl-vrad_crt_rr;
    tempW4=vrad_crt_fl-vrad_crt_fr;
  #endif
    
    tempW6 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_100_KPH,VREF_2_5_KPH,VREF_1_KPH);
    tempW8 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_100_KPH,VREF_1_KPH,VREF_0_5_KPH);
    if((ABS_fz==1) || (lcabsu1BrakeByBLS==1))
    {
        tempW6=tempW6+VREF_0_25_KPH;
        tempW8=tempW8+VREF_0_25_KPH;
        if((ABS_fz==1)&&(tempW6<VREF_2_KPH))
        {
            tempW6=VREF_2_KPH;
        }
    }
    
    if(BEND_DETECT_flag==0)
    {
        if((ABS_fz==1) || (lcabsu1BrakeByBLS==1))
        {
            if(Side_vref_diff>tempW6)
            {
                bend_detect_count_right = LDABS_vSetSideBendCntBfBend(vref_L, vref_R, bend_detect_count_right, &FL, &RL, &FR, &RR, 1);
                bend_detect_count_right2 = LDABS_vSetSideBendCntBfBend(vref_L, vref_R, bend_detect_count_right2, &FL, &RL, &FR, &RR, 0);

                #if __VDC
                if(YAW_CDC_WORK==1) 
                {
                    if((alat<=-LAT_0G4G) && (wstr<=-WSTR_60_DEG) && (yaw_out<=-YAW_10DEG)) 
                    {
                         bend_detect_count_right = bend_detect_count_right + L_1SCAN;
                    }                    
                }
                #endif
            }
            else if(Side_vref_diff<=tempW8)
            {
                bend_detect_count_right = 0;
                bend_detect_count_right2= 0;
            }
            else { }
            
            if(Side_vref_diff<-tempW6) 
            {
                bend_detect_count_left = LDABS_vSetSideBendCntBfBend(vref_R, vref_L, bend_detect_count_left, &FR, &RR, &FL, &RL, 1);
                bend_detect_count_left2 = LDABS_vSetSideBendCntBfBend(vref_R, vref_L, bend_detect_count_left2, &FR, &RR, &FL, &RL, 0);

                #if __VDC
                if(YAW_CDC_WORK==1) 
                {
                    if((alat>=LAT_0G4G) && (wstr>=WSTR_60_DEG) && (yaw_out>=YAW_10DEG)) 
                    {
                         bend_detect_count_left = bend_detect_count_left + L_1SCAN;
                    }                    
                }
                #endif
            }
            else if(Side_vref_diff>=-tempW8) 
            {
                bend_detect_count_left = 0;
                bend_detect_count_left2= 0;
            }
            else { }
            
            if((bend_detect_count_right>=U8_BEND_DCT_TIME_PER_SIDEVREF) || (bend_detect_count_right2>=U8_BEND_DCT_TIME_PER_SIDEVREF2))
            {
                BEND_DETECT_RIGHT = 1;
                bend_detect_count_left = 0;
                BEND_DETECT_flag=1;
            }
            else if((bend_detect_count_left>=U8_BEND_DCT_TIME_PER_SIDEVREF) || (bend_detect_count_left2>=U8_BEND_DCT_TIME_PER_SIDEVREF2))
            {
                BEND_DETECT_LEFT = 1;
                bend_detect_count_right = 0;
                BEND_DETECT_flag=1;
            }
            else 
            {
                ;
            }            
        }
        else 
        {
            if((Side_vref_diff>tempW6) && (tempW0>=VREF_1_KPH)) 
            {
                if(tempW4 > 0) {bend_detect_count_right = bend_detect_count_right + L_1SCAN;}
                else 
                {
                    if(bend_detect_count_right>L_2SCAN) {bend_detect_count_right = bend_detect_count_right - L_2SCAN;}
                    else {bend_detect_count_right = 0;}
                }

                /*
                    to add ESC sensor reference
                */
                
            }
            else {bend_detect_count_right = 0;}
            
            if((Side_vref_diff<-tempW6) && (tempW0<=-VREF_1_KPH)) 
            {
                if(tempW4 < 0) {bend_detect_count_left = bend_detect_count_left + L_1SCAN;}
                else 
                {
                    if(bend_detect_count_left>L_2SCAN) {bend_detect_count_left = bend_detect_count_left - L_2SCAN;}
                    else {bend_detect_count_left = 0;}
                }
                
                /*
                    to add ESC sensor reference
                */
                
            }
            else {bend_detect_count_left = 0;}
            
            tempB7 = (int8_t)LCABS_s16Interpolation2P(tempW6,VREF_2_KPH,VREF_1_KPH,L_TIME_30MS,L_TIME_70MS);
            if(bend_detect_count_right>=tempB7) 
            {
                BEND_DETECT_RIGHT = 1;
                bend_detect_count_left = 0;
                BEND_DETECT_flag = 1;
                BEND_DETECT_before_ABS_flag = 1;
            }
            else if(bend_detect_count_left>=tempB7) 
            {
                BEND_DETECT_LEFT = 1;
                bend_detect_count_right = 0;
                BEND_DETECT_flag = 1;
                BEND_DETECT_before_ABS_flag = 1;
            }
            else 
            {
                ;
            }                
            bend_detect_count_left2 = 0;
            bend_detect_count_right2 = 0;
        }
    }
    else 
    {
        if((ABS_fz==1) || (lcabsu1BrakeByBLS==1))
        {
            if((BEND_DETECT_before_ABS_flag==0) || ((zyklus_z_fl>=2) && (zyklus_z_fr>=2)) 
              #if   __VDC
               || (((L_SLIP_CONTROL_fl==1)&&(zyklus_z_fr>=2)) || ((L_SLIP_CONTROL_fr==1)&&(zyklus_z_fl>=2)))
              #endif
              )
            {
                if(BEND_DETECT_RIGHT==1) 
                {
                    bend_detect_count_right = LDABS_vSetSideBendCntAftBend(vref_R, Side_vref_diff, tempW8, bend_detect_count_right, &FR, &RR); 
                }                    
                else if(BEND_DETECT_LEFT==1) 
                {
                    bend_detect_count_left = LDABS_vSetSideBendCntAftBend(vref_L, -Side_vref_diff, tempW8, bend_detect_count_left, &FL, &RL);
                }
                else {}
            }
            else {}
        }
        else {
            if(BEND_DETECT_RIGHT==1) 
            {
                if((Side_vref_diff<=tempW8) || (tempW0<VREF_0_5_KPH) || (tempW4<0)) 
                {
                    bend_detect_count_right = bend_detect_count_right - L_1SCAN;
                }
                else {bend_detect_count_right = U8_BEND_RESET_TIME_PER_SIDEVREF;}
                
            }
            else if(BEND_DETECT_LEFT==1) 
            {
                if((Side_vref_diff>=-tempW8) || (tempW0>-VREF_0_5_KPH) || (tempW4>0)) 
				{
					bend_detect_count_left = bend_detect_count_left - L_1SCAN;
				}
				else {bend_detect_count_left = U8_BEND_RESET_TIME_PER_SIDEVREF;}
                
            }
            else { }
        }
                    
        if(((BEND_DETECT_RIGHT==1) && (bend_detect_count_right==0)) || ((BEND_DETECT_LEFT==1) && (bend_detect_count_left==0)))
        {
            BEND_DETECT_RIGHT = 0;
        	BEND_DETECT_LEFT = 0;
            BEND_DETECT_before_ABS_flag = 0;
            BEND_DETECT_flag = 0;
        }
        else { }        	           
        bend_detect_count_left2 = 0;
        bend_detect_count_right2 = 0;
    }
}
#endif

#if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
static void LDABS_vDctLimitCornerBrake(struct W_STRUCT *pRH, const struct W_STRUCT *pFL, const struct W_STRUCT *pFH)
{
	int lcabss16HoldTime;
	
	if(pRH->LFC_built_reapply_flag==1)
	{
		pRH->lcabsu1CornerRearOutHoldPreOK = 1; /* one hold ativation per 1 cycle */
	}
	else{}
	
	if((det_alatm>=LAT_0G2G)&&(det_alatm<=S16_LCB_ENTER_MAX_LATERAL_G)&&(vref>=VREF_40_KPH)&&(pRH->ABS==1)
	  &&(delta_yaw>=((int16_t)S8_LCB_ENTER_DEL_YAW_THRES*100))&&(pRH->L_SLIP_CONTROL==0)
	  &&(pFL->Estimated_Press_SKID<=((int16_t)U8_LCB_MED_MU_DET_F_INS_SKID*10))
	  &&(pFH->Estimated_Press_SKID<=((int16_t)U8_LCB_MED_MU_DET_F_OUT_SKID*10)))
	{
		pRH->lcabsu1CornerRearOutCtrl = 1;
	}
	else if((pRH->ABS==0)||(pRH->L_SLIP_CONTROL==1))
	{
		pRH->lcabsu1CornerRearOutCtrl = 0;
	}
	else{}    		
	
	lcabss16HoldTime=LCABS_s16Interpolation2P(vref, VREF_40_KPH, VREF_100_KPH, L_TIME_300MS, L_TIME_500MS);
		
	if((pRH->lcabsu1CornerRearOutCtrl==1)&&(pRH->lcabsu8RearOutsideHoldCnt<=lcabss16HoldTime))
	{
    	if((pRH->STABIL==1)&&(pRH->lcabsu1CornerRearOutHoldPreOK==1)&&(pRH->LFC_built_reapply_flag==0))
    	{
    	    pRH->lcabsu1CornerRearOutHold   = 1;
    	    pRH->lcabsu8RearOutsideHoldCnt += L_1SCAN;
    	}
    	else
    	{
    		pRH->lcabsu1CornerRearOutHold   = 0;
    	    pRH->lcabsu8RearOutsideHoldCnt  = 0;
    	}
	}
	else
	{
		pRH->lcabsu1CornerRearOutHoldPreOK = 0; 
		pRH->lcabsu1CornerRearOutHold      = 0;
		pRH->lcabsu8RearOutsideHoldCnt     = 0;
	}
}

static void LDABS_vDctLimitCornerBrakeReset(void)
{
	if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(ABS_fz==0)
	||(det_alatm<LAT_0G1G)||(det_alatm>=S16_LCB_EXIT_MIN_LATERAL_G)||(vref<=VREF_20_KPH)||(vdc_error_flg==1)
	||(delta_yaw<((int16_t)S8_LCB_EXIT_DEL_YAW_THRES*100)))
    {
    	RL.lcabsu8RearOutsideHoldCnt  = 0;
    	RL.lcabsu1CornerRearOutCtrl   = 0;
    	RL.lcabsu1CornerRearOutHold   = 0;
    	RL.lcabsu1CornerRearOutHoldPreOK = 0;
    	RR.lcabsu8RearOutsideHoldCnt  = 0;
    	RR.lcabsu1CornerRearOutCtrl   = 0;
    	RR.lcabsu1CornerRearOutHold   = 0;
    	RR.lcabsu1CornerRearOutHoldPreOK = 0;
    }
    else{}
}
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSDctVehicleStatus
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
