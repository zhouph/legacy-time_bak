#ifndef LDENGCALLDETECTION_H
#define LDENGCALLDETECTION_H

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition******************************************************/


/*Global MACRO FUNCTION Definition******************************************************/


/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/


/*Global Extern Functions  Declaration******************************************************/
extern void LDENG_vEstimatPowertrain(void);

#endif