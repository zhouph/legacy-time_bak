/* Includes ********************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAHDCCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LAHDCCallActHW.h"
#include "LCABSDecideCtrlState.h"
#include "LCallMain.h"
  #if __HDC
#include "LADECCallActHW.h"
#include "LCDECCallControl.h"
#include "LCHDCCallControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
#include "hm_logic_var.h"
#include "LCHSACallControl.h"
   #if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
   #endif
  #if defined(__dDCT_INTERFACE)
#include "LCHRCCallControl.h"
  #endif   
  #endif

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __HDC
uint8_t       lcu8HdcValveMode, lcu8HdcMotorMode;
HDC_VALVE_t laHdcValveP, laHdcValveS;

uint8_t 	lcu8HdcEsvActOffInAbs;
  #endif

/* Local Function prototype ****************************************/
  #if __HDC
static void LAHDC_vDecelCtrl(void);
static void LAHDC_vSpeedRegulCtrl(void);
static void LAHDC_vCoopEsp(void);
static void LAHDC_vCoopAbs(void);
static void LAHDC_vCoopEbd(void);
static void LAHDC_vNormHdc(void);
   #if __TCMF_CONTROL
static void LAHDC_vOriginalValveCmd(void);
   #endif
int16_t LAMFC_s16SetMFCCurrentHDC(uint8_t CIRCUIT, HDC_VALVE_t *pHdc, int16_t MFC_Current_old);   
  #endif

/* Implementation***************************************************/
  #if __HDC
void LAHDC_vCallActHW(void)
{
    if(lcu1HdcActiveFlg==1)
    {
        if(clDecHdc.lcu8DecCtrlReq==1)
        {
            LAHDC_vDecelCtrl();
        }
        else
        {
            LAHDC_vSpeedRegulCtrl();
        }
          #if __TCMF_CONTROL
        LAHDC_vOriginalValveCmd();
          #endif
    }
    else
    {
        ;
    }
}


void LAHDC_vDecelCtrl(void)
{
  #if !__TCMF_CONTROL
    HV_VL_fl=lcu8DecFlNoValve;
    AV_VL_fl=lcu8DecFlNcValve;
    HV_VL_fr=lcu8DecFrNoValve;
    AV_VL_fr=lcu8DecFrNcValve;
    HV_VL_rl=lcu8DecRlNoValve;
    AV_VL_rl=lcu8DecRlNcValve;
    HV_VL_rr=lcu8DecRrNoValve;
    AV_VL_rr=lcu8DecRrNcValve;
  #endif
      #if __SPLIT_TYPE      /* FR Split */
    S_VALVE_PRIMARY=lcu8DecFlFrEsvValve;
    S_VALVE_SECONDARY=lcu8DecRlRrEsvValve;
    TCL_DEMAND_PRIMARY=lcu8DecFlFrTcValve;
    TCL_DEMAND_SECONDARY=lcu8DecRlRrTcValve;
      #else             /* X Split */
    S_VALVE_RIGHT=lcu8DecFrRlEsvValve;
    S_VALVE_LEFT=lcu8DecFlRrEsvValve;
    TCL_DEMAND_fr=lcu8DecFrRlTcValve;
    TCL_DEMAND_fl=lcu8DecFlRrTcValve;
      #endif
    lcu8HdcValveMode=lcu8DecValveMode;
    lcu8HdcMotorMode=lcu8DecMotorMode;
}

  #if __TCMF_CONTROL
void LAHDC_vOriginalValveCmd(void)
{
    if(lcu8HdcValveMode==U8_HDC_HOLD)
    {
        HDC_HV_VL_fl=1;
        HDC_AV_VL_fl=0;
        HDC_HV_VL_fr=1;
        HDC_AV_VL_fr=0;
        HDC_HV_VL_rl=1;
        HDC_AV_VL_rl=0;
        HDC_HV_VL_rr=1;
        HDC_AV_VL_rr=0;
    }
    else if(lcu8HdcValveMode==U8_HDC_APPLY)
    {
        HDC_HV_VL_fl=0;
        HDC_AV_VL_fl=0;
        HDC_HV_VL_fr=0;
        HDC_AV_VL_fr=0;
        HDC_HV_VL_rl=0;
        HDC_AV_VL_rl=0;
        HDC_HV_VL_rr=0;
        HDC_AV_VL_rr=0;
    }
    else if(lcu8HdcValveMode==U8_HDC_DUMP)
    {
        HDC_HV_VL_fl=1;
        HDC_AV_VL_fl=1;
        HDC_HV_VL_fr=1;
        HDC_AV_VL_fr=1;
        HDC_HV_VL_rl=1;
        HDC_AV_VL_rl=1;
        HDC_HV_VL_rr=1;
        HDC_AV_VL_rr=1;
    }
    else
    {
        ;
    }
}
  #endif

void LAHDC_vSpeedRegulCtrl(void)
{
  #if __TARGET_SPD_ADAPTATION
    if(lcu8HdcEsvActOffInAbs==0)
    {
        if((ABS_fz==1)&&(mpress>S16_MP_TH_ESV_OFF_AT_ABS))
        {
            lcu8HdcEsvActOffInAbs = 1;
        }
        else 
        {
        	;
        }
    }
    else
    {
        if((ABS_fz==0)||(MPRESS_BRAKE_ON_off_cnt<(L_U16_TIME_10MSLOOP_5S-S16_MP_OFF_TH_ESV_RE_ON_TIME)))
        {
        	  lcu8HdcEsvActOffInAbs = 0;
        }
        else 
        {
        	;
        }
    }
  #endif	
	
    if(FLAG_ESP_CONTROL_ACTION==1)
    {
        LAHDC_vCoopEsp();
    }
    else if(ABS_fz==1)
    {
        LAHDC_vCoopAbs();
    }
    else if(EBD_RA==1)
    {
        LAHDC_vCoopEbd();
    }
    else
    {
        LAHDC_vNormHdc();
    }
      #if __SPLIT_TYPE
    TCL_DEMAND_PRIMARY=1;
    TCL_DEMAND_SECONDARY=1;
      #else
    TCL_DEMAND_fr=1;
    TCL_DEMAND_fl=1;
      #endif
}


void LAHDC_vCoopEsp(void)
{
    lcu8HdcMotorMode=VDC_MOTOR_ON;
    if((YAW_CDC_WORK==1)&&(ABS_fz==0))
    {
          #if __SPLIT_TYPE
        if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1))
        {
            if((EBD_RA==0)&&(L_SLIP_CONTROL_rl==0))
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=0;
                    AV_VL_rl=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=1;
                    AV_VL_rl=1;
                  #endif
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rr==0))
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=1;
                    AV_VL_rr=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=0;
                    AV_VL_rr=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=1;
                    AV_VL_rr=1;
                  #endif
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
          #else
        if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_RR==1))
        {
            if(L_SLIP_CONTROL_fr==0)
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=1;
                    AV_VL_fr=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=0;
                    AV_VL_fr=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=1;
                    AV_VL_fr=1;
                  #endif
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rl==0))
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=0;
                    AV_VL_rl=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rl=1;
                    AV_VL_rl=1;
                  #endif
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #endif
            /*******************************************************************/
              #if __SPLIT_TYPE
		if((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))
        {
            if(L_SLIP_CONTROL_fl==0)
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=1;
                    AV_VL_fl=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=0;
                    AV_VL_fl=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=1;
                    AV_VL_fl=1;
                  #endif
                }
            }
            else
            {
                ;
            }

            if(L_SLIP_CONTROL_fr==0)
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=1;
                    AV_VL_fr=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=0;
                    AV_VL_fr=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fr=1;
                    AV_VL_fr=1;
                  #endif
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #else
        if((ESP_BRAKE_CONTROL_FR==1)||(ESP_BRAKE_CONTROL_RL==1))
        {
            if(L_SLIP_CONTROL_fl==0)
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=1;
                    AV_VL_fl=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=0;
                    AV_VL_fl=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_fl=1;
                    AV_VL_fl=1;
                  #endif
                }
            }
            else
            {
                ;
            }

            if((EBD_RA==0)&&(L_SLIP_CONTROL_rr==0))
            {
                if(lcu8HdcValveMode==U8_HDC_HOLD)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=1;
                    AV_VL_rr=0;
                  #endif
                }
                else if(lcu8HdcValveMode==U8_HDC_APPLY)
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=0;
                    AV_VL_rr=0;
                  #endif
                }
                else
                {
                  #if !__TCMF_CONTROL
                    HV_VL_rr=1;
                    AV_VL_rr=1;
                  #endif
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
              #endif
    }
    else if((YAW_CDC_WORK==1)&&(ABS_fz==1))
    {
    	;
    }
    else    /*if((YAW_CDC_WORK == 0) || (ABS_fz == 1))*/
    {
        ;
    }

  #if __SPLIT_TYPE
    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_FR==0))
    {
        S_VALVE_PRIMARY=1;
    }
    else { }
    if((ESP_BRAKE_CONTROL_RL==0)&&(ESP_BRAKE_CONTROL_RR==0))
    {
        S_VALVE_SECONDARY=1;
    }
    else { }
  #else
    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_RR==0))
    {
    	S_VALVE_LEFT=1;
    }
    else { }
    if((ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_RL==0))
    {
        S_VALVE_RIGHT=1;
    }
    else { }
  #endif

}


void LAHDC_vCoopAbs(void)
{
    lcu8HdcMotorMode=VDC_MOTOR_ON;
          #if __SPLIT_TYPE
    if((HV_VL_fl==0)&&(AV_VL_fl==0))
    {
        lcu8HdcMotorMode=1;
    }
    else if((HV_VL_fr==0)&&(AV_VL_fr==0))
    {
        lcu8HdcMotorMode=1;
    }
    else { }
          #else
    if((HV_VL_fl==0)&&(AV_VL_fl==0))
    {
        lcu8HdcMotorMode=1;
    }
    else if((HV_VL_rr==0)&&(AV_VL_rr==0))
    {
        lcu8HdcMotorMode=1;
    }
    else { }

          #endif

      #if __SPLIT_TYPE
    if((HV_VL_rl==0)&&(AV_VL_rl==0))
    {
    	lcu8HdcMotorMode=1;
    }
    else if((HV_VL_rr==0)&&(AV_VL_rr==0))
    {
        lcu8HdcMotorMode=1;
    }
    else
    {
        ;
    }
	  #else
    if((HV_VL_fr==0)&&(AV_VL_fr==0))
    {
    	lcu8HdcMotorMode=1;
    }
    else if((HV_VL_rl==0)&&(AV_VL_rl==0))
    {
        lcu8HdcMotorMode=1;
    }
    else { }
      #endif
//PPR 2007-180 : ABS+HDC ���� ���� �� �з»�� �ʾ� ���� ����

  #if __TARGET_SPD_ADAPTATION	
      #if __SPLIT_TYPE
    if(lcu8HdcEsvActOffInAbs==1)
    {
       S_VALVE_PRIMARY=0;
       S_VALVE_SECONDARY=0;
    }
    else
    {
    	S_VALVE_PRIMARY=1;
      S_VALVE_SECONDARY=1;
    }
      #else
    if(lcu8HdcEsvActOffInAbs==1)
    {
       S_VALVE_RIGHT=0;
       S_VALVE_LEFT=0;
    }
    else
    {
    	S_VALVE_RIGHT=1;
      S_VALVE_LEFT=1;
    }  
      #endif
  #else
      #if __SPLIT_TYPE
    S_VALVE_PRIMARY=1;
    S_VALVE_SECONDARY=1;
  #else
    S_VALVE_RIGHT=1;
    S_VALVE_LEFT=1;
      #endif
  #endif

}


void LAHDC_vCoopEbd(void)
{
    if(lcu8HdcValveMode==U8_HDC_HOLD)
    {

      #if !__TCMF_CONTROL
        HV_VL_fl=1;
        AV_VL_fl=0;
        HV_VL_fr=1;
        AV_VL_fr=0;
      #endif
    }
    else if(lcu8HdcValveMode==U8_HDC_APPLY)
    {
      #if !__TCMF_CONTROL
        HV_VL_fl=0;
        AV_VL_fl=0;
        HV_VL_fr=0;
        AV_VL_fr=0;
      #endif
    }
    else
    {
      #if !__TCMF_CONTROL
        HV_VL_fl=1;
        AV_VL_fl=1;
        HV_VL_fr=1;
        AV_VL_fr=1;
      #endif
    }

      #if __SPLIT_TYPE
    S_VALVE_PRIMARY=1;
    S_VALVE_SECONDARY=1;
      #else
    S_VALVE_RIGHT=1;
    S_VALVE_LEFT=1;
      #endif

}


void LAHDC_vNormHdc(void)
{
    if(lcu8HdcValveMode==U8_HDC_HOLD)
    {
      #if !__TCMF_CONTROL
        HV_VL_fl=1;
        AV_VL_fl=0;
        HV_VL_fr=1;
        AV_VL_fr=0;
        HV_VL_rl=1;
        AV_VL_rl=0;
        HV_VL_rr=1;
        AV_VL_rr=0;
      #endif
    }
    else if(lcu8HdcValveMode==U8_HDC_APPLY)
    {
      #if !__TCMF_CONTROL
        HV_VL_fl=0;
        AV_VL_fl=0;
        HV_VL_fr=0;
        AV_VL_fr=0;
        HV_VL_rl=0;
        AV_VL_rl=0;
        HV_VL_rr=0;
        AV_VL_rr=0;
      #endif
    }
    else if(lcu8HdcValveMode==U8_HDC_DUMP)
    {
      #if !__TCMF_CONTROL
        HV_VL_fl=1;
        AV_VL_fl=1;
        HV_VL_fr=1;
        AV_VL_fr=1;
        HV_VL_rl=1;
        AV_VL_rl=1;
        HV_VL_rr=1;
        AV_VL_rr=1;
      #endif
    }
    else
    {
        ;
    }

      #if __SPLIT_TYPE
    S_VALVE_PRIMARY=1;
    S_VALVE_SECONDARY=1;
      #else
    S_VALVE_RIGHT=1;
    S_VALVE_LEFT=1;
      #endif

}

int16_t LAMFC_s16SetMFCCurrentHDC(uint8_t CIRCUIT, HDC_VALVE_t *pHdc, int16_t MFC_Current_old)
{
    int16_t     MFC_Current_new=0;
    if(lcu1HdcActiveFlg==1)
    {
      #if __TARGET_SPD_ADAPTATION 
    	if(lcu1hdcEngStallSuspect2==1) /* dump rate at engine stall */
    	{
			tempW1 = (int16_t)LCESP_s16IInter3Point(lcs16HdcThetaG, LONG_0G1G,  U8_HDC_STALL_DUMP_RATE_10P,
							     							        LONG_0G2G,  U8_HDC_STALL_DUMP_RATE_20P,
								   						            LONG_0G3G,  U8_HDC_STALL_DUMP_RATE_30P);
		}
		else
		{
			tempW1 = 0;
		}
      #endif

        /* SW Issue 2007-107 */
        if(MFC_Current_old==0)
        {
          #if defined(__dDCT_INTERFACE)
		    if((lcu1HrcOnReqFlg==1)&&(HSA_flg==1)&&(HSA_Control==HSA_RELEASE))
		    {
		    	tempW0 = (MFC_Current_HSA_P>MFC_Current_HSA_S)?MFC_Current_HSA_P:MFC_Current_HSA_S;
				tempW0 = (tempW0>S16_HRC_MIN_INITIAL_CURRENT)?tempW0:S16_HRC_MIN_INITIAL_CURRENT;
		    }
		    else
		  #endif
            if(((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)&&(vref < lcs16HdcTargetSpeed))
                ||(MFC_Current_old > 350))
            {
                tempW0 = (int16_t)S16_HDC_INITIAL_CURRENT - 20;
            }
            else
            {
                tempW0 = (int16_t)S16_HDC_INITIAL_CURRENT;
            }

            if (MFC_Current_old > tempW0 )
            {
                MFC_Current_new = MFC_Current_old;
            }
            else
            {
                MFC_Current_new=  tempW0;
            }
        }
        else
        {
            if(clDecHdc.lcu8DecCtrlReq==1)
            {
                /********************************************************/
                if((pHdc->lau1HdcWL1No==0)&&(pHdc->lau1HdcWL1Nc==0))            /* REAPPLY */
                {
                    /* Issue List 2007-351 */

                    if ((lcs16HdcAvr_ThetaG > LONG_0G1G)
                        &&((ABS_fl==0)||(ABS_fr==0)))
                    {
                        tempW0=abs(lcs16DecTargetG*5)+abs(lcs16HdcAvr_ThetaG/2)+550;
                    }
                    else if (((ABS_fl==1)&&(ABS_fr==1))||((ABS_rl==1)&&(ABS_rr==1)))
                    {
                        tempW0=abs(lcs16DecTargetG*5)+475;
                    }
                    else
                    {
                        tempW0=abs(lcs16DecTargetG*5)+550;
                    }

                    /*~ Issue List 2007-351 */

                    if(MFC_Current_old>=tempW0)
                    {
                        MFC_Current_new = MFC_Current_old;
                    }
                    else
                    {
                        MFC_Current_new=MFC_Current_old+3;
                    }

                }
                /********************************************************/
                else if((pHdc->lau1HdcWL1No==1)&&(pHdc->lau1HdcWL1Nc==0))       /* HOLD */
                {
                	  #if __TARGET_SPD_ADAPTATION
                	if(lcu8HdcDriverBrakeIntend==1)
                	{
                        MFC_Current_new = LCESP_s16IInter3Point(mpress, MPRESS_10BAR,  S16_HDC_TC_CUR_AT_MP10bar, /* press Hold cur MGH60 ref. */
                                                                        MPRESS_20BAR,  S16_HDC_TC_CUR_AT_MP20bar,
                                                                        MPRESS_30BAR,  S16_HDC_TC_CUR_AT_MP30bar);
                	}
                	else {MFC_Current_new=MFC_Current_old;}
                	  #else
                  MFC_Current_new=MFC_Current_old;
                    #endif
                }
                /********************************************************/
                else if((pHdc->lau1HdcWL1No==1)&&(pHdc->lau1HdcWL1Nc==1))       /* DUMP */
                {
                    if(MFC_Current_old>S16_HDC_CONTROL_MIN_CURRENT)
                    {
                        if(lcs8HdcState==S8_HDC_CLOSING)
                        {
                                        /* SW Issue 2007-107 */
                            MFC_Current_new=MFC_Current_old-3;
                        }
                        else
                        {
                               #if __TARGET_SPD_ADAPTATION
                    	    if((lcu8HdcDriverAccelIntend==1)&&(MFC_Current_old<S16_HDC_LIMIT_CUR_AT_ACC_DUMP)) {MFC_Current_new = S16_HDC_LIMIT_CUR_AT_ACC_DUMP;} /* Limit press At HDC accel.dump */
                            else if(lcu1hdcEngStallSuspect2==1) {MFC_Current_new = MFC_Current_old - tempW1;}     /* dump at engine stall*/
                            else if((lcu8HdcDriverAccelIntend==1)&&(lcu1hdcVehicleBackward==1)) {MFC_Current_new = MFC_Current_old - 2;} 
                            else {MFC_Current_new = MFC_Current_old - 3;}
                            
                            if(MFC_Current_new<S16_HDC_CONTROL_MIN_CURRENT) {MFC_Current_new=S16_HDC_CONTROL_MIN_CURRENT;}
                               #else
                             MFC_Current_new=MFC_Current_old-3;
                               #endif
                        }
                    }
                    else
                    {
                        MFC_Current_new=S16_HDC_CONTROL_MIN_CURRENT;
                    }
                    }
                /********************************************************/
                    else                                /* Free Circulation */
                    {
                        MFC_Current_new = MFC_Current_old;
                    }
            }
                else
                {
                    /********************************************************/
                if((pHdc->lau1HdcWL1No==0)&&(pHdc->lau1HdcWL1Nc==0))            /* REAPPLY */
                {
                  #if __TARGET_SPD_ADAPTATION   /* set min current at rise */
                	  if(hdc_msc_target_vol>MSC_0_V)
                	  {
                        if(MFC_Current_old<S16_HDC_RISE_CURRENT_INIT)     {MFC_Current_new = S16_HDC_RISE_CURRENT_INIT;}
                        else if(MFC_Current_old<S16_HDC_INITIAL_CURRENT)  {MFC_Current_new = MFC_Current_old + (int16_t)U8_HDC_RISE_CURRENT_COMP;}
                	  	else                                              {MFC_Current_new = MFC_Current_old + 3;}
                	  	  }
                	  	  else
                	      {
                    	if(lcu1hdcEngStallSuspect2==1) {MFC_Current_new = MFC_Current_old - tempW1;} 
                    	else {MFC_Current_new=MFC_Current_old + 3;}
                    	
                    	if(MFC_Current_new<S16_HDC_CONTROL_MIN_CURRENT) {MFC_Current_new=S16_HDC_CONTROL_MIN_CURRENT;}
                	  }
                 	    #else
                	  MFC_Current_new=MFC_Current_old+3;
                	    #endif
                }
                /********************************************************/
                else if((pHdc->lau1HdcWL1No==1)&&(pHdc->lau1HdcWL1Nc==0))       /* HOLD */
                {
                      #if __TARGET_SPD_ADAPTATION
                	if(lcu8HdcDriverBrakeIntend==1)
                	{
                        MFC_Current_new = LCESP_s16IInter3Point(mpress, MPRESS_10BAR,  S16_HDC_TC_CUR_AT_MP10bar, /* press Hold cur MGH60 ref. */
    	                                                                MPRESS_20BAR,  S16_HDC_TC_CUR_AT_MP20bar,
                                                                        MPRESS_30BAR,  S16_HDC_TC_CUR_AT_MP30bar);
                	}
                	else {MFC_Current_new=MFC_Current_old;}
                	    #else
                    MFC_Current_new=MFC_Current_old;
                      #endif                	
                }
                /********************************************************/
                else if((pHdc->lau1HdcWL1No==1)&&(pHdc->lau1HdcWL1Nc==1))       /* DUMP */
                {
                    if(MFC_Current_old>S16_HDC_CONTROL_MIN_CURRENT)
                    {
                        if(lcs8HdcState==S8_HDC_CLOSING)
                        {
                            MFC_Current_new=MFC_Current_old-3;
                        }
                        else
                        {
                              #if __TARGET_SPD_ADAPTATION
                            if((lcu8HdcDriverAccelIntend==1)&&(MFC_Current_old<S16_HDC_LIMIT_CUR_AT_ACC_DUMP)) {MFC_Current_new = S16_HDC_LIMIT_CUR_AT_ACC_DUMP;} /* Limit press At HDC accel.dump */
                            else if(lcu1hdcEngStallSuspect2==1) {MFC_Current_new = MFC_Current_old - tempW1;}     /* dump at engine stall*/
                            else if((lcu8HdcDriverAccelIntend==1)&&(lcu1hdcVehicleBackward==1)) {MFC_Current_new = MFC_Current_old - 2;} 
                            else {MFC_Current_new = MFC_Current_old - 3;}
                            
                            if(MFC_Current_new<S16_HDC_CONTROL_MIN_CURRENT) {MFC_Current_new=S16_HDC_CONTROL_MIN_CURRENT;}
                              #else
                            MFC_Current_new=MFC_Current_old-3;
                              #endif
                        }
                    }
                    else
                    {
                        MFC_Current_new=S16_HDC_CONTROL_MIN_CURRENT;
                    }
                }
                /********************************************************/
                else                                /* Free Circulation */
                {
                    MFC_Current_new=MFC_Current_old;
                }
            }
        }

        if(((lcs16HdcActiveFlg_on_cnt < L_U8_TIME_10MSLOOP_50MS)&&(lcs16HdcActiveFlg_on_cnt > 1)
      #if __TARGET_SPD_ADAPTATION
    	&&(lcu1HdcNoForcedHoldAtBrk==0)
      #endif
       )
            ||((lcu1decFFCtrl_flg==1)&&(ABS_fz==0)))
    {
        if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=1;}
        if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=1;}
    }
    else
    {
        if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=0;}
        if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=0;}
    }
    }
    else
    {
    	if((lcu1decFFCtrl_flg==1)&&(ABS_fz==0))
    	{
    		;
    	}
    	else
    	{
    		if (CIRCUIT==1){lcudecforcedMFCHoldFlg_p=0;}
        	if (CIRCUIT==0){lcudecforcedMFCHoldFlg_s=0;}
        }
        MFC_Current_new=LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_old);
    }

    return MFC_Current_new;
}


  #if __ADVANCED_MSC
void LCMSC_vSetHDCTargetVoltage(void)
{
    if(lcu1HdcActiveFlg==1)
    {
        if(clDecHdc.lcu8DecCtrlReq==1)
        {
            if(lcu1DecActiveFlg==1)
            {
                hdc_msc_target_vol=lcs16DecMscTargetVol;
                HDC_MSC_MOTOR_ON=1;
            }
            else
            {
                hdc_msc_target_vol=0;
                HDC_MSC_MOTOR_ON=0;
            }
        }
        else
        {
            HDC_MSC_MOTOR_ON=1;
            if(lcs16HdcCtrlError<0)
            {
                if(lcs16HdcCtrlError<S16_HDC_MSC_1ST_ERR) {tempW1=S16_HDC_MSC_1ST_ERR;}
                else if(lcs16HdcCtrlError>S16_HDC_MSC_2ND_ERR) {tempW1=S16_HDC_MSC_2ND_ERR;}
                else {tempW1=lcs16HdcCtrlError;}
            	tempW4=(int16_t)((((int32_t)(S16_HDC_MSC_2ND_VOL-S16_HDC_MSC_1ST_VOL))*tempW1)/(S16_HDC_MSC_2ND_ERR-S16_HDC_MSC_1ST_ERR));
				tempW3=(int16_t)((((int32_t)(S16_HDC_MSC_2ND_VOL-S16_HDC_MSC_1ST_VOL))*S16_HDC_MSC_2ND_ERR)/(S16_HDC_MSC_2ND_ERR-S16_HDC_MSC_1ST_ERR));                

                hdc_msc_target_vol=tempW4+(S16_HDC_MSC_2ND_VOL-tempW3);

				if (lcs16HdcActiveFlg_on_cnt < L_U8_TIME_10MSLOOP_100MS)
    	        {
        	    	hdc_msc_target_vol = (hdc_msc_target_vol>S16_HDC_INITIAL_TARGET_V)?hdc_msc_target_vol:S16_HDC_INITIAL_TARGET_V;
            	}
            }
            else
            {
                hdc_msc_target_vol=MSC_0_V;
            }
              #if __TCMF_CONTROL
                if(TCMF_MOTOR_OFF_flag0==1)
                {
                    hdc_msc_target_vol=MSC_0_V;
                }
              #endif
        }
    }
    else
    {
        HDC_MSC_MOTOR_ON=0;
        hdc_msc_target_vol=MSC_0_V;
    }

    HDC_ABS_COMB_MSC_MOTOR_ON=0;
    if ((ABS_MSC_MOTOR_ON ==1)&&(HDC_MSC_MOTOR_ON==1))
    {
        if(ABS_DURING_ACTIVE_BRAKE_FLG==1)
        {
	     	lcs16_hdc_tempW9 = 0;

        	if (ABS_fl==1)
        	{
        		lcs16_hdc_tempW9 = lcs16_hdc_tempW9+2;
        	}
        	else { }

        	if (ABS_fr==1)
        	{
        		lcs16_hdc_tempW9 = lcs16_hdc_tempW9+2;
        	}
        	else { }

        	if (ABS_rl==1)
        	{
        		lcs16_hdc_tempW9 = lcs16_hdc_tempW9+1;
        	}
        	else { }

        	if (ABS_rr==1)
        	{
        		lcs16_hdc_tempW9 = lcs16_hdc_tempW9+1;
        	}
        	else { }

			lcs16_hdc_tempW9 = (MSC_3_V*lcs16_hdc_tempW9/10);

			lcs16_hdc_tempW9 = (lcs16_hdc_tempW9 > MSC_1_V)?lcs16_hdc_tempW9:MSC_1_V;

            if(abs_msc_target_vol > (hdc_msc_target_vol + lcs16_hdc_tempW9))
            {
                hdc_msc_target_vol = hdc_msc_target_vol + lcs16_hdc_tempW9;
                HDC_ABS_COMB_MSC_MOTOR_ON=1;
            }
            else { }
        }
        else
        {

        }
    }
    else { }
}
  #endif
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAHDCCallActHW
	#include "Mdyn_autosar.h"               
#endif 
