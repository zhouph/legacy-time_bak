#ifndef __LSFBCCALLLOGDATA_H__
#define __LSFBCCALLLOGDATA_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

#include "LCFBCCallControl.h"
#include "LSCallLogData.h"
#include "LDFBCCallDetection.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCallEngineControl.h"
#include "LCESPCallPressureControl.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern uint8_t mcp_unstable_cnt;
extern int16_t FBC_mode, FBC_hold_timer;
extern int16_t	FBC_MPS_exit_timer;
extern uint8_t mp_hw_err_cnt;
extern int16_t pba_msc_target_vol;
extern int16_t MFC_Current_PBA_S, MFC_Current_PBA_P;
#if __ADVANCED_MSC
extern int16_t target_vol;
#endif
	
/*Global Extern Functions Declaration ******************************************/


#endif
