
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_Hardware_Control
	#include "Mdyn_autosar.h"               
#endif
#include "Hardware_Control.h"
#include "LCACCCallControl.h"
#include "LAACCCallActHW.h"
#include "LCHDCCallControl.h"
#include "LAHDCCallActHW.h"
#include "LCEPBCallControl.h"
#include "LAEPBCallActHW.h"
#include "LCDECCallControl.h"
#include "LADECCallActHW.h"
#include "LALVBACallActHW.h"
#include "LCHSACallControl.h"
#include "LAHSACallActHW.h"
#include "LCAVHCallControl.h"
#include "LAAVHCallActHW.h"
#include "LCFBCCallControl.h"
#include "LCBDWCallControl.h"
#include "LCEBPCallControl.h"
#include "LAABSGenerateLFCDuty.h"
#include "LCHBBCallControl.h"
#include "LCLVBACallControl.h"
#include "LCESPCalInterpolation.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LAEPCCallActHW.h"
#include "LCARBCallControl.h"
#include "LCHRBCallControl.h"
#include "LAABSCallHWTest.h"
#if __ESC_TCMF_CONTROL
#include"LAESPActuateTCMFESC.h"
#endif

#include "LCABSDecideCtrlState.h"

#if __DEC
#if __TSP
#include "LDTSPCallDetection.h"
#include "LATSPCallActHW.h"
#endif
#endif
#include "LCPBACallControl.h"
#include "LAESPCallActHW.h"

  #if __SPAS_INTERFACE
#include "LASPASCallActHW.h"
#include "LCSPASCallControl.h"
  #endif

  #if __SCC
#include "../SRC/SCC/LCWPCCallControl.H"
  #endif

#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#if (__ESC_COMPETITIVE_CONTROL == 1)
#include "LDESPDetectVehicleStatus.h"
#include "LCESPCallControl.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"
#endif



/* Logcal Definiton  *********************************************************/

#define __ESPLUS_OLD_HU             0

/* Variables Definition*******************************************************/
int16_t MFC_Current_DEC, MFC_Current_BA, MFC_Current_VH;
#if __TCMF_CONTROL
uint8_t   TC_MONITOR;
int8_t    TCMF_dump_end_timer_s, TCMF_dump_end_timer_p;
uint16_t    TC_MFC_Voltage_Primary, TC_MFC_Voltage_Primary_dash;
uint16_t    TC_MFC_Voltage_Secondary, TC_MFC_Voltage_Secondary_dash;
uint16_t    lau16HcEsvActTimerPri, lau16HcEsvActTimerSec;

int16_t     MFC_Current_ESP_S, MFC_Current_ESP_P;
int16_t     MFC_Current_TCS_S, MFC_Current_TCS_P;
int16_t     MFC_Current_HDC_S, MFC_Current_HDC_P;
int16_t     MFC_Current_HSA_S, MFC_Current_HSA_P;
int16_t     MFC_Current_AVH_S, MFC_Current_AVH_P, MFC_Current_AVH_old;
int16_t     MFC_Current_HBB_S, MFC_Current_HBB_P;

#if __SCC
int16_t     MFC_Current_SCC_S, MFC_Current_SCC_P;
#endif

  #if __SPAS_INTERFACE
int16_t     MFC_Current_SPAS_S, MFC_Current_SPAS_P;
  #endif  

#if __Decel_Ctrl_Integ  /* 08SWD-10 */
int16_t     MFC_Current_DEC_S, MFC_Current_DEC_P;
#if __TSP
int16_t			MFC_Current_TSP_S, MFC_Current_TSP_P;
#endif
#else
int16_t     MFC_Current_ACC_S, MFC_Current_ACC_P;
int16_t     MFC_Current_EPB_S, MFC_Current_EPB_P;
#endif

#if __PBA_MFC_EXIT
int16_t     MFC_Current_PBA_S, MFC_Current_PBA_P;
#endif
int16_t     MFC_Current_ARB_S, MFC_Current_ARB_P;
int16_t     MFC_Current_HRB_S, MFC_Current_HRB_P;
#if __TVBB
int16_t     MFC_Current_TVBB_S, MFC_Current_TVBB_P;
#endif

#if __LVBA
int16_t    MFC_Current_LVBA_S, MFC_Current_LVBA_P;
#endif

#if __MFC_L9352B
uint8_t   MFC_PWM_DUTY_S, MFC_PWM_DUTY_P, MFC_PWM_DUTY_ESV, MFC_PWM_DUTY_ESV_temp, MFC_PWM_DUTY_ESV_P, MFC_PWM_DUTY_ESV_S;
MFC_DUTY_t *pTC, la_PtcMfc,la_StcMfc;
#endif

MFC_FLG_t   MFC_FLAGS;
#endif

/* LocalFunction prototype ***************************************************/

#if __TCMF_CONTROL
  #if __TCMF_CONTROL2
void    TCNO_CURRENT_CONTROL(void);
  #else
/*   #if __ACC
int16_t LAMFC_s16SetMFCCurrentACC(ACC_VALVE_t *pAcc, int16_t MFC_Current_old);
   #endif*/
/*   #if __HDC
int16_t LAMFC_s16SetMFCCurrentHDC(uint8_t CIRCUIT, HDC_VALVE_t *pHdc, int16_t MFC_Current_old);
   #endif*/
/*   #if __EPB_INTERFACE
int16_t LAMFC_s16SetMFCCurrentEPB(EPB_VALVE_t *pEpb, int16_t MFC_Current_old);
   #endif*/
/*int16_t LAMFC_s16SetMFCCurrentHSA(int16_t MFC_Current_old);*/
int16_t LAMFC_s16SetMFCCurrentHBB(uint8_t CIRCUIT, int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurrentARB(int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurrentHRB(uint8_t CIRCUIT, int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurrentAtTCOff(uint8_t CONTROL_ACTIVE_flg, int16_t MFC_Current_old);
void    LAMFC_vArrangeMFCCurrent(uint8_t CIRCUIT, int16_t MFC_Current_ESP, int16_t MFC_Current_ACC, int16_t MFC_Current_HDC, int16_t MFC_Current_HSA, int16_t MFC_Current_HBB);
void    LAMFC_vModifyValveCommand(struct W_STRUCT *pWL1, struct W_STRUCT *pWL2);

/* void    LAMFC_vCheckTCForcedHold(struct W_STRUCT *pWL1, struct W_STRUCT *pWL2); */
void    LAMFC_vCheckTCForcedHold(uint8_t CIRCUIT, int16_t MFC_Current_new);
void    LAMFC_vLoadCircuitParameters(void);
void    LAMFC_vSetCircuitParameters(void);
  #if __MFC_L9352B
void    LAMFC_vCalcMFCDuty(void);
  #endif
  #endif
#endif


/* GlobalFunction prototype **************************************************/
//extern int16_t abs(int16_t j);


/* Implementation*************************************************************/


/* ====================================================================== */
/*
void    VENTANB(struct W_STRUCT *WL)
{
//  if(bls_k_timer != 0) {
    if(bls_k_timer > 3) {       // Changed by CSH 030225
        WL->state=0;
        BUILT_wl=0;
//        goto REAPPLY;
        HV_VL_wl=0;
        AV_VL_wl=0;
      #if __REAR_MI
        WL->continue_dump_timer=0;
      #endif
    }
    else {
        if(REAR_WHEEL_wl == 0) {
            if(AV_DUMP_wl == 1) {
//              goto DUMP;
                HV_VL_wl=1;
                AV_VL_wl=1;
                if(ABS_fz==1) {     // Added by KGY 041007
                    SET_DUMP_wl=1;                  // SM01_1
                  #if __REAR_MI
                    if(!EBD_wl)WL->continue_dump_timer++;
                    MSL_DUMP_wl=1;                  //MI02I
                  #endif
                }
            }
            else if(AV_HOLD_wl == 1) {
//              goto HOLD;
                HV_VL_wl=1;
                AV_VL_wl=0;
                if(ABS_fz==1) {     // Added by KGY 041007
                  #if __REAR_MI
                    if(REAR_WHEEL_wl) {
                        if(WL->p_hold_time>0) WL->continue_dump_timer=0;
                    }
                    else if(WL->p_hold_time>2) WL->continue_dump_timer=0;
                  #endif
                }
            }
            else {
//              goto REAPPLY;
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
                WL->continue_dump_timer=0;
              #endif
            }
          #if __AQUA_ENABLE
            if(AQUA_END == 1) {
//              goto REAPPLY;
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
                WL->continue_dump_timer=0;
              #endif
            }
          #endif
        }
        else {
            if((AV_DUMP_wl == 1) || (SL_DUMP_wl == 1)) {
//              goto DUMP;
                HV_VL_wl=1;
                AV_VL_wl=1;
                if(ABS_fz==1) {     // Added by KGY 041007
                    SET_DUMP_wl=1;                  // SM01_1
                  #if __REAR_MI
                    if(!EBD_wl)WL->continue_dump_timer++;
                    MSL_DUMP_wl=1;                  //MI02I
                  #endif
                }
            }
            else if((AV_HOLD_wl == 1) || (SL_HOLD_wl == 1)) {
//              goto HOLD;
                HV_VL_wl=1;
                AV_VL_wl=0;
                if(ABS_fz==1) {     // Added by KGY 041007
                  #if __REAR_MI
                    if(REAR_WHEEL_wl) {
                        if(WL->p_hold_time>0) WL->continue_dump_timer=0;
                    }
                    else if(WL->p_hold_time>2) WL->continue_dump_timer=0;
                  #endif
                }
            }
            else {
//              goto REAPPLY;
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
                WL->continue_dump_timer=0;
              #endif
            }
        }
    }
}
*/


#if __TCMF_CONTROL
  #if __TCMF_CONTROL2
void    TCNO_CURRENT_CONTROL(void)
{
    TCMF_LONG_HOLD_flag_s = 0;
    TCMF_LONG_HOLD_flag_p = 0;

/*  if(TCL_DEMAND_fl==1) { */
    if((TCL_DEMAND_fl==1) || (TCL_DEMAND_rr==1)) {

        if((AV_VL_fl==1) || (AV_VL_rr==1)) {
            if(lcu1HdcActiveFlg==1) TCMF_dump_end_timer_s = L_U8_TIME_10MSLOOP_100MS;
            else TCMF_dump_end_timer_s = L_U8_TIME_10MSLOOP_70MS;
        }
        else if(TCMF_dump_end_timer_s>0) TCMF_dump_end_timer_s--;

        SOL_STCNO_DISABLE = 0;
        if(vref > VREF_2_5_KPH) S_VALVE_LEFT = 1;
        if(SLIP_CONTROL_NEED_FL==1) {
            if(TC_MFC_Voltage_Secondary == 0) {
                if(esp_control_mode_fl==1) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                else if(esp_control_mode_fl==2) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                else TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
            }
            else if((esp_control_mode_old_fl==1) && (esp_control_mode_fl==2)) {
                TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
            }

            if(HV_VL_fl==0) {
                if(AV_VL_fl==0) {                           /* REAPPLY */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + TC_CURRENT_INC_LFC;
                    if(TC_MFC_Voltage_Secondary >= TC_CURRENT_MAX) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                    TC_MONITOR = 1;
                }
            }
            else {
                if(AV_VL_fl==0) {                           /* HOLD */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    TC_MONITOR = 2;
                }
                else {                                      /* DUMP */
/*                  TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - TC_CURRENT_DEC; */
                    if(TC_MFC_Voltage_Secondary <= TC_CURRENT_MIN) TC_MFC_Voltage_Secondary = TC_CURRENT_MIN;
                    TC_MONITOR = 3;
                }
/*              if(ABS_fl==0) HV_VL_fl = AV_VL_fl = 0; */
            }
        }
        else if(SLIP_CONTROL_NEED_RR==1) {
            if(TC_MFC_Voltage_Secondary == 0) {
                if(esp_control_mode_rr==1) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                else if(esp_control_mode_rr==2) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                else TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
            }
            else if((esp_control_mode_old_rr==1) && (esp_control_mode_rr==2)) {
                TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
            }

            if(HV_VL_rr==0) {
                if(AV_VL_rr==0) {                           /* REAPPLY */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + TC_CURRENT_INC_LFC;
                    if(TC_MFC_Voltage_Secondary >= TC_CURRENT_MAX) TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                    TC_MONITOR = 51;
                }
            }
            else {
                if(AV_VL_rr==0) {                           /* HOLD */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    TC_MONITOR = 52;
                }
                else {                                      /* DUMP */
/*                  TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - TC_CURRENT_DEC; */
                    if(TC_MFC_Voltage_Secondary <= TC_CURRENT_MIN) TC_MFC_Voltage_Secondary = TC_CURRENT_MIN;
                    TC_MONITOR = 53;
                }
/*              if(ABS_rr==0) HV_VL_rr = AV_VL_rr = 0; */
            }
        }
        else if(BTCS_fl==1) {
            if(TC_MFC_Voltage_Secondary == 0) TC_MFC_Voltage_Secondary = 400;
            if(HV_VL_fl==0) {
                if(AV_VL_fl==0) {                           /* REAPPLY */
                    if(TC_MFC_Voltage_Secondary >=400) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 4;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 10;
                    if(TC_MFC_Voltage_Secondary >= 650) TC_MFC_Voltage_Secondary = 650;
                    TC_MONITOR = 4;
                }
            }
            else {
                if(AV_VL_fl==0) {                           /* HOLD */
                    /*if(POS_HOLD_fl==1) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    else */
/*                  if(TC_MFC_Voltage_Secondary>=450) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary-5;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary; */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    TC_MONITOR = 5;
                }
                else {                                      /* DUMP */
                    if(TC_MFC_Voltage_Secondary >=450) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 12;
                    else if(TC_MFC_Voltage_Secondary >=400) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 8;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 5;
                    if(TC_MFC_Voltage_Secondary <= 350) TC_MFC_Voltage_Secondary = 350;
                    AV_VL_fl = 0;
                    TC_MONITOR = 6;
                }
                HV_VL_fl = 0;
            }
        }
        else if(BTCS_rr==1) {
            if(TC_MFC_Voltage_Secondary == 0) TC_MFC_Voltage_Secondary = 420;
            if(HV_VL_rr==0) {
                if(AV_VL_rr==0) {                           /* REAPPLY */
                    if(TC_MFC_Voltage_Secondary >=400) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 4;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 10;
                    if(TC_MFC_Voltage_Secondary >= 650) TC_MFC_Voltage_Secondary = 650;
                    TC_MONITOR = 4;
                }
            }
            else {
                if(AV_VL_rr==0) {                           /* HOLD */
                    /*if(POS_HOLD_fl==1) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    else */
/*                  if(TC_MFC_Voltage_Secondary>=450) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary-5;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary; */
                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                    TC_MONITOR = 5;
                }
                else {                                      /* DUMP */
                    if(TC_MFC_Voltage_Secondary >=450) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 10;
                    else if(TC_MFC_Voltage_Secondary >=400) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 6;
                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 5;
                    if(TC_MFC_Voltage_Secondary <= 350) TC_MFC_Voltage_Secondary = 350;
                    AV_VL_rr = 0;
                    TC_MONITOR = 6;
                }
                HV_VL_rr = 0;
            }
        }
        else {      /* PBA, PreFill */
            if(PBA_ON==1) {
                tempW9 = MPRESS_150BAR - mpress;
                if(tempW9 > MPRESS_0BAR) {
                    if(tempW9 > MPRESS_100BAR) tempW9 = MPRESS_100BAR;
                    TC_MFC_Voltage_Secondary = 400 + tempW9*6/10;
                }
                else {
                    TC_MFC_Voltage_Secondary = 0;
                }

                if((ABS_fz==1) && (PBA_ABS_timer>=L_U8_TIME_10MSLOOP_200MS)) S_VALVE_LEFT = 0;
            }
            else {
          #if   __HDC || __ACC
                if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
                    if(TC_MFC_Voltage_Secondary == 0) {
                        if(lcu1AccActiveFlg==1) {
                            if(lcs16AccTargetG>=-10) TC_MFC_Voltage_Secondary = 400;
                            else if(lcs16AccTargetG>=-20) TC_MFC_Voltage_Secondary = 450;
                            else if(lcs16AccTargetG>=-30) TC_MFC_Voltage_Secondary = 500;
                            else if(lcs16AccTargetG>=-50) TC_MFC_Voltage_Secondary = 600;
                            else if(lcs16AccTargetG>=-70) TC_MFC_Voltage_Secondary = 600;
                            else TC_MFC_Voltage_Secondary = 700;
                        }
                        else TC_MFC_Voltage_Secondary = 450;
                    }
                    if(HV_VL_fl==0) {
                        if(AV_VL_fl==0) {                           /* REAPPLY */
                            if(lcu1AccActiveFlg==1) {
/*MISRA, H22*/
/*                              if(slight_tc_dump > 0) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                                else {*/
                                    if(lcs16AccTargetG>=-30) {tempW8 = 500; tempW9 = TC_CURRENT_INC_VAFS;}
                                    else if(lcs16AccTargetG>=-50) {tempW8 = 600; tempW9 = 4;}
                                    else if(lcs16AccTargetG>=-70) {tempW8 = 650; tempW9 = 4;}
                                    else {tempW8 = 750; tempW9 = 5;}

                                    if(vref<=VREF_8_KPH) {tempW8 = 400; tempW9 = 1;}

                                    if(decel_closing_counter==0) {
                                        if(TC_MFC_Voltage_Secondary >= (uint16_t)tempW8) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + (uint16_t)tempW9;
                                        else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + TC_CURRENT_INC_BTC2;
                                    }
                                    else {
                                        if(TC_MFC_Voltage_Secondary > 200) {
                                            if(decel_closing_counter<L_U8_TIME_10MSLOOP_350MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 1;
                                            else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 3;
                                        }
                                        else TC_MFC_Voltage_Secondary = 200;
                                    }
/*MISRA, H22*/
/*                              }*/
/*                              if(vref<=VREF_5_KPH) {
                                    if(vref<=VREF_2_KPH) {
                                        TC_MFC_Voltage_Secondary = 0;
                                    }
                                    else {
                                        TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - TC_CURRENT_DEC;
                                        if(TC_MFC_Voltage_Secondary <= 200) TC_MFC_Voltage_Secondary = 200;
                                    }
                                }  */
                            }
                            else {
                                if(ABS_fz==1) {
                                    if(lcs16HdcTargetSpeed-vrefk>=30) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 5;
                                    else if(lcs16HdcTargetSpeed-vrefk>=20) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 3;
                                    else if(lcs16HdcTargetSpeed-vrefk>=-10) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 2;
                                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 1;
                                }
                                else if(lcs16HdcTargetSpeed-vrefk>=10) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                                else if(lcs16HdcTargetSpeed-vrefk>=-10) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 3;
                                else if(lcs16HdcTargetSpeed-vrefk>=-20) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 4;
                                else if(lcs16HdcTargetSpeed-vrefk>=-30) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 5;
                                else if(lcu16HdcWorkingTime<=T_500_MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 2;
                                else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 5;
                            }

                            if(lcu1AccActiveFlg==1) {
                                if(lcs16AccTargetG>=-30) tempW1 = 700;
                                else if(lcs16AccTargetG>=-50) tempW1 = 850;
                                else if(lcs16AccTargetG>=-70) tempW1 = 1000;
                                else tempW1 = TC_CURRENT_MAX;
                            }
                            else tempW1 = 600;

                            if(TC_MFC_Voltage_Secondary >= (uint16_t)tempW1) TC_MFC_Voltage_Secondary  = (uint16_t)tempW1;
                            TC_MONITOR = 4;
                        }
                    }
                    else {
                        if(AV_VL_fl==0) {                           /* HOLD */
                            if(lcu1AccActiveFlg==1) {
                                if(decel_closing_counter==0) {
                                    TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                                }
                                else {
                                    if(TC_MFC_Voltage_Secondary > 200) {
                                        if(decel_closing_counter<L_U8_TIME_10MSLOOP_350MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 1;
                                        else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 3;
                                    }
                                    else TC_MFC_Voltage_Secondary = 200;
                                }
                            }
                            else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;

                            TC_MONITOR = 5;
                            if(ABS_fl==0) HV_VL_fl = 0;

                            if((TCMF_MOTOR_OFF_flag==1) && (TCMF_dump_end_timer_s==0)) TCMF_LONG_HOLD_flag_s = 1;
                            else TCMF_LONG_HOLD_flag_s = 0;

                        }
                        else {                                      /* DUMP */
                            if(ABS_fl==0) {
                                if(lcu1HdcActiveFlg==1) {
                                /*
                                    if(lcs16HdcTargetSpeed-vrefk>=30) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 6;
                                    else if(lcs16HdcTargetSpeed-vrefk>=20) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 4;
                                    else if(lcs16HdcTargetSpeed-vrefk>=10) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 3;
                                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 10;
                                */
                                    if(vrefk-lcs16HdcTargetSpeed>=30) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 30;
                                    else if(vrefk-lcs16HdcTargetSpeed>=10) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 20;
                                    else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 15;
                                    if(lcs16HdcTargetSpeed-vrefk >= 30) {
                                        if(TC_MFC_Voltage_Secondary <= 200) TC_MFC_Voltage_Secondary = 200;
                                    }
                                    else if(lcs16HdcTargetSpeed-vrefk >= 15) {
                                        if(TC_MFC_Voltage_Secondary <= 300) TC_MFC_Voltage_Secondary = 300;
                                    }
                                    else {
                                        if(TC_MFC_Voltage_Secondary <= TC_CURRENT_MIN) TC_MFC_Voltage_Secondary = TC_CURRENT_MIN;
                                    }
                                }
                                else {
                                    if(vrefk>50) {
                                        TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 6;
                                        if(TC_MFC_Voltage_Secondary <= TC_CURRENT_MIN) TC_MFC_Voltage_Secondary = TC_CURRENT_MIN;
                                    }
                                    else {
                                        TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - TC_CURRENT_DEC;
                                        if(TC_MFC_Voltage_Secondary <= 200) TC_MFC_Voltage_Secondary = 200;
                                    }
                                }
                                HV_VL_fl = AV_VL_fl = 0;
                            }
                            else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary;
                            TC_MONITOR = 6;
                        }
                    }
                    if((ABS_rr==0) && (EBD_RA==0)) {
                        HV_VL_rr=AV_VL_rr=0;
                    }
                    if((lcu1HdcActiveFlg==1)&&(mpress>=MPRESS_5BAR)) TC_MFC_Voltage_Secondary = 200;
                }
                else {
                  #if   __HSA
                    if(HSA_flg==1) {
                        if(HSA_pressure_release_counter>0) {
                            if(HSA_pressure_release_counter==1) {
                                if(HSA_hold_counter>L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 100;
                            }
                            if(TC_MFC_Voltage_Secondary>200) {
                                if(HSA_pressure_release_counter>=L_U8_TIME_10MSLOOP_700MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 1;
                                else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 2;
                            }
                            else TC_MFC_Voltage_Secondary = 200;
                        }
                        else {
                            if(TC_MFC_Voltage_Secondary==0) {
                                TC_MFC_Voltage_Secondary = (uint16_t)HSA_ENTER_press + 300;
                            }
                            else if(HSA_hold_counter==L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 100;
                        }
                    }
                    else {
                  #endif
                    TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
/*                  TC_MFC_Voltage_Secondary = 400; */
                    TC_MONITOR = 111;
                  #if   __HSA
                    }
                  #endif
                }
              #else
                  #if   __HSA
                if(HSA_flg==1) {
                    if(HSA_pressure_release_counter>0) {
                        if(HSA_pressure_release_counter==1) {
                            if(HSA_hold_counter>L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 100;
                        }
                        if(TC_MFC_Voltage_Secondary>200) {
                            if(HSA_pressure_release_counter>=L_U8_TIME_10MSLOOP_700MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 1;
                            else TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - 2;
                        }
                        else TC_MFC_Voltage_Secondary = 200;
                    }
                    else {
                        if(TC_MFC_Voltage_Secondary==0) {
                            TC_MFC_Voltage_Secondary = (uint16_t)HSA_ENTER_press + 300;
                        }
                        else if(HSA_hold_counter==L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary + 100;
                    }
                }
                else {
                  #endif
                TC_MFC_Voltage_Secondary = TC_CURRENT_MAX;
                TC_MONITOR = 101;
                  #if   __HSA
                }
                  #endif
              #endif
            }
        }
    }
    else {
      #if   __HDC || __ACC
        if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
            TCL_DEMAND_fl=1;
            SOL_STCNO_DISABLE = 0;
            S_VALVE_LEFT = 1;
            if((lcu1HdcActiveFlg==1)&&(mpress>=MPRESS_5BAR)) TC_MFC_Voltage_Secondary = 0;
            else {
                if(TC_MFC_Voltage_Secondary > TC_CURRENT_DEC) TC_MFC_Voltage_Secondary = TC_MFC_Voltage_Secondary - TC_CURRENT_DEC;
                else TC_MFC_Voltage_Secondary = 0;
/*              if(TC_MFC_Voltage_Secondary <= TC_CURRENT_MIN) TC_MFC_Voltage_Secondary = TC_CURRENT_MIN; */
            }
            if(ABS_fl==0) HV_VL_fl = AV_VL_fl = 0;
            if((ABS_rr==0) && (EBD_RA==0)) HV_VL_rr = AV_VL_rr = 0;
        }
        else {
            if(ABS_fz==1) {
              #if   defined(__CAN_SW)
/*              if((RX_OK_flag==1) && (HSA_CAN_SWITCH==1)) TC_MFC_Voltage_Secondary = 450; // about 20bar */
/*              if((RX_OK_flag==1) && (HDC_CAN_SWITCH==1))  */
                if(HSA_CAN_SWITCH==1) {
                    TC_MFC_Voltage_Secondary = 500; /* about 20bar */
                    TCL_DEMAND_fl=1;
                    SOL_STCNO_DISABLE = 0;
                }
                else TC_MFC_Voltage_Secondary = 0;
              #else
                TC_MFC_Voltage_Secondary = 0;
              #endif
            }
            else {
                TC_MFC_Voltage_Secondary = 0;
                if(BTCS_fl==1) TC_MONITOR = 7;
            }
        }
      #else
        if(ABS_fz==1) {
          #if   defined(__CAN_SW)
/*          if((RX_OK_flag==1) && (HSA_CAN_SWITCH==1)) TC_MFC_Voltage_Secondary = 450; // about 20bar */
/*          if((RX_OK_flag==1) && (HDC_CAN_SWITCH==1))  */
            if(HSA_CAN_SWITCH==1) {
                TC_MFC_Voltage_Secondary = 500; /* about 20bar */
                TCL_DEMAND_fl=1;
                SOL_STCNO_DISABLE = 0;
            }
            else TC_MFC_Voltage_Secondary = 0;
          #else
            TC_MFC_Voltage_Secondary = 0;
          #endif
        }
        else {
            TC_MFC_Voltage_Secondary = 0;
            if(BTCS_fl==1) TC_MONITOR = 7;
        }
      #endif
    }


    if((TCL_DEMAND_fr==1) || (TCL_DEMAND_rl==1)) {

        if((AV_VL_fr==1) || (AV_VL_rl==1)) {
            if(lcu1HdcActiveFlg==1) TCMF_dump_end_timer_p = L_U8_TIME_10MSLOOP_100MS;
            else TCMF_dump_end_timer_p = L_U8_TIME_10MSLOOP_70MS;
        }
        else if(TCMF_dump_end_timer_p>0) TCMF_dump_end_timer_p--;

        SOL_PTCNO_DISABLE = 0;
        if(vref > VREF_2_5_KPH) S_VALVE_RIGHT = 1;
        if(SLIP_CONTROL_NEED_FR==1) {
            if(TC_MFC_Voltage_Primary == 0) {
                if(esp_control_mode_fr==1) TC_MFC_Voltage_Primary = TC_CURRENT_MAX; /* full rise */
                else if(esp_control_mode_fr==2) TC_MFC_Voltage_Primary = TC_CURRENT_MAX;            /* LFC */
                else TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
            }
            else if((esp_control_mode_old_fr==1) && (esp_control_mode_fr==2)) {
                TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
            }
            if(HV_VL_fr==0) {
                if(AV_VL_fr==0) {                           /* REAPPLY */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + TC_CURRENT_INC_LFC;
                    if(TC_MFC_Voltage_Primary >= TC_CURRENT_MAX) TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
                    TC_MONITOR = 11;
                }
            }
            else {
                if(AV_VL_fr==0) {                           /* HOLD */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    TC_MONITOR = 12;
                }
                else {                                      /* DUMP */
/*                  TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - TC_CURRENT_DEC; */
                    if(TC_MFC_Voltage_Primary <= TC_CURRENT_MIN) TC_MFC_Voltage_Primary = TC_CURRENT_MIN;
                    TC_MONITOR = 13;
                }
/*              if(ABS_fr==0) HV_VL_fr = AV_VL_fr = 0; */
            }
        }
        else if(SLIP_CONTROL_NEED_RL==1) {
            if(TC_MFC_Voltage_Primary == 0) {
                if(esp_control_mode_rl==1) TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
                else if(esp_control_mode_rl==2) TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
                else TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
            }
            else if((esp_control_mode_old_rl==1) && (esp_control_mode_rl==2)) {
                TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
            }

            if(HV_VL_rl==0) {
                if(AV_VL_rl==0) {                           /* REAPPLY */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + TC_CURRENT_INC_LFC;
                    if(TC_MFC_Voltage_Primary >= TC_CURRENT_MAX) TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
                    TC_MONITOR = 61;
                }
            }
            else {
                if(AV_VL_rl==0) {                           /* HOLD */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    TC_MONITOR = 62;
                }
                else {                                      /* DUMP */
/*                  TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - TC_CURRENT_DEC; */
                    if(TC_MFC_Voltage_Primary <= TC_CURRENT_MIN) TC_MFC_Voltage_Primary = TC_CURRENT_MIN;
                    TC_MONITOR = 63;
                }
/*              if(ABS_rl==0) HV_VL_rl = AV_VL_rl = 0; */
            }
        }
        else if(BTCS_fr==1) {
            if(TC_MFC_Voltage_Primary == 0) TC_MFC_Voltage_Primary = 400;
            if(HV_VL_fr==0) {
                if(AV_VL_fr==0) {                           /* REAPPLY */
                    if(TC_MFC_Voltage_Primary >=400) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 4;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 10;
                    if(TC_MFC_Voltage_Primary >= 650) TC_MFC_Voltage_Primary = 650;
                    TC_MONITOR = 14;
                }
            }
            else {
                if(AV_VL_fr==0) {                           /* HOLD */
                    /*if(POS_HOLD_fr==1) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    else */
/*                  if(TC_MFC_Voltage_Primary>=450) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary-5;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary; */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    TC_MONITOR = 15;
                }
                else {                                      /* DUMP */
                    if(TC_MFC_Voltage_Primary >=450) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 12;
                    else if(TC_MFC_Voltage_Primary >=400) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 8;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 5;
                    if(TC_MFC_Voltage_Primary <= 350) TC_MFC_Voltage_Primary = 350;
                    AV_VL_fr = 0;
                    TC_MONITOR = 16;
                }
                HV_VL_fr = 0;
            }
        }
        else if(BTCS_rl==1) {
            if(TC_MFC_Voltage_Primary == 0) TC_MFC_Voltage_Primary = 420;
            if(HV_VL_rl==0) {
                if(AV_VL_rl==0) {                           /* REAPPLY */
                    if(TC_MFC_Voltage_Primary >=400) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 4;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 10;
                    if(TC_MFC_Voltage_Primary >= 650) TC_MFC_Voltage_Primary = 650;
                    TC_MONITOR = 14;
                }
            }
            else {
                if(AV_VL_rl==0) {                           /* HOLD */
                    /*if(POS_HOLD_fr==1) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    else */
/*                  if(TC_MFC_Voltage_Primary>=450) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary-5;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary; */
                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                    TC_MONITOR = 15;
                }
                else {                                      /* DUMP */
                    if(TC_MFC_Voltage_Primary >=450) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 10;
                    else if(TC_MFC_Voltage_Primary >=400) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 6;
                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 5;
                    if(TC_MFC_Voltage_Primary <= 350) TC_MFC_Voltage_Primary = 350;
                    AV_VL_rl = 0;
                    TC_MONITOR = 16;
                }
                HV_VL_rl = 0;
            }
        }
        else {/* PBA, PreFill */
            if(PBA_ON==1) {
                tempW9 = MPRESS_150BAR - mpress;
                if(tempW9 > MPRESS_0BAR) {
                    if(tempW9 > MPRESS_100BAR) tempW9 = MPRESS_100BAR;
                    TC_MFC_Voltage_Primary = 400 + tempW9*6/10;
                }
                else {
                    TC_MFC_Voltage_Primary = 0;
                }

                if((ABS_fz==1) && (PBA_ABS_timer>=L_U8_TIME_10MSLOOP_200MS)) S_VALVE_RIGHT=0;
            }
            else {
              #if   __HDC || __ACC
                if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
                    if(TC_MFC_Voltage_Primary == 0) {
                        if(lcu1AccActiveFlg==1) {
                            if(lcs16AccTargetG>=-10) TC_MFC_Voltage_Primary = 400;
                            else if(lcs16AccTargetG>=-20) TC_MFC_Voltage_Primary = 450;
                            else if(lcs16AccTargetG>=-30) TC_MFC_Voltage_Primary = 500;
                            else if(lcs16AccTargetG>=-50) TC_MFC_Voltage_Primary = 600; /* 500 */
                            else if(lcs16AccTargetG>=-70) TC_MFC_Voltage_Primary = 600; /* 600 */
                            else TC_MFC_Voltage_Primary = 700; /* 700 */
                        }
                        else TC_MFC_Voltage_Primary = 450;
                    }
                    if(HV_VL_fr==0) {
                        if(AV_VL_fr==0) {                           /* REAPPLY */
                            if(lcu1AccActiveFlg==1) {
/*MISRA, H22*/
/*                              if(slight_tc_dump > 0) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                                else {*/
                                    if(lcs16AccTargetG>=-30) {tempW8 = 500; tempW9 = TC_CURRENT_INC_VAFS;}
                                    else if(lcs16AccTargetG>=-50) {tempW8 = 600; tempW9 = 4;}
                                    else if(lcs16AccTargetG>=-70) {tempW8 = 650; tempW9 = 4;}
                                    else {tempW8 = 750; tempW9 = 5;}

                                    if(vref<=VREF_8_KPH) {tempW8 = 400; tempW9 = 1;}

                                    if(decel_closing_counter==0) {
                                        if(TC_MFC_Voltage_Primary >= (uint16_t)tempW8) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + (uint16_t)tempW9;
                                        else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + TC_CURRENT_INC_BTC2;
                                    }
                                    else {
                                        if(TC_MFC_Voltage_Primary > 200) {
                                            if(decel_closing_counter<L_U8_TIME_10MSLOOP_350MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 1;
                                            else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 3;
                                        }
                                        else TC_MFC_Voltage_Primary = 200;
                                    }
/*MISRA, H22*/
/*                              }*/
/*                              if(vref<=VREF_5_KPH) {
                                    if(vref<=VREF_2_KPH) {
                                        TC_MFC_Voltage_Primary = 0;
                                    }
                                    else {
                                        TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - TC_CURRENT_DEC;
                                        if(TC_MFC_Voltage_Primary <= 200) TC_MFC_Voltage_Primary = 200;
                                    }
                                } */
                            }
                            else {
                                if(ABS_fz==1) {
                                    if(lcs16HdcTargetSpeed-vrefk>=30) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 5;
                                    else if(lcs16HdcTargetSpeed-vrefk>=20) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 3;
                                    else if(lcs16HdcTargetSpeed-vrefk>=-10) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 2;
                                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 1;
                                }
                                else if(lcs16HdcTargetSpeed-vrefk>=10) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                                else if(lcs16HdcTargetSpeed-vrefk>=-10) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 3;
                                else if(lcs16HdcTargetSpeed-vrefk>=-20) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 4;
                                else if(lcs16HdcTargetSpeed-vrefk>=-30) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 5;
                                else if(lcu16HdcWorkingTime<=T_500_MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 2;
                                else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 5;
                            }

                            if(lcu1AccActiveFlg==1) {
                                if(lcs16AccTargetG>=-30) tempW1 = 700;
                                else if(lcs16AccTargetG>=-50) tempW1 = 850; /* 700 */
                                else if(lcs16AccTargetG>=-70) tempW1 = 1000; /* 800 */
                                else tempW1 = TC_CURRENT_MAX; /* 900 */
                            }
                            else tempW1 = 600; /* 600 */

                            if(TC_MFC_Voltage_Primary >= (uint16_t)tempW1) TC_MFC_Voltage_Primary = (uint16_t)tempW1;
                            TC_MONITOR = 14;
                        }
                    }
                    else {
                        if(AV_VL_fr==0) {                           /* HOLD */
                            if(lcu1AccActiveFlg==1) {
                                if(decel_closing_counter==0) {
                                    TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                                }
                                else {
                                    if(TC_MFC_Voltage_Primary > 200) {
                                        if(decel_closing_counter<L_U8_TIME_10MSLOOP_350MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 1;
                                        else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 3;
                                    }
                                    else TC_MFC_Voltage_Primary = 200;
                                }
                            }
                            else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;

                            TC_MONITOR = 15;
                            if(ABS_fr==0) HV_VL_fr = 0;

                            if((TCMF_MOTOR_OFF_flag==1) && (TCMF_dump_end_timer_p==0)) TCMF_LONG_HOLD_flag_p = 1;
                            else TCMF_LONG_HOLD_flag_p = 0;
                        }
                        else {                                      /* DUMP */
                            if(ABS_fr==0) {
                                if(lcu1HdcActiveFlg==1) {
                                 /*
                                    if(lcs16HdcTargetSpeed-vrefk>=30) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 6;
                                    else if(lcs16HdcTargetSpeed-vrefk>=20) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 4;
                                    else if(lcs16HdcTargetSpeed-vrefk>=10) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 3;
                                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 10;
                                */
                                    if(vrefk-lcs16HdcTargetSpeed>=30) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 30;
                                    else if(vrefk-lcs16HdcTargetSpeed>=10) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 20;
                                    else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 15;
                                    if(lcs16HdcTargetSpeed-vrefk >= 30) {
                                        if(TC_MFC_Voltage_Primary <= 200) TC_MFC_Voltage_Primary = 200;
                                    }
                                    else if(lcs16HdcTargetSpeed-vrefk >= 15) {
                                        if(TC_MFC_Voltage_Primary <= 300) TC_MFC_Voltage_Primary = 300;
                                    }
                                    else {
                                        if(TC_MFC_Voltage_Primary <= TC_CURRENT_MIN) TC_MFC_Voltage_Primary = TC_CURRENT_MIN;
                                    }
                                }
                                else {
                                    if(vrefk>50) {
                                        TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 6;
                                        if(TC_MFC_Voltage_Primary <= TC_CURRENT_MIN) TC_MFC_Voltage_Primary = TC_CURRENT_MIN;
                                    }
                                    else {
                                        TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - TC_CURRENT_DEC;
                                        if(TC_MFC_Voltage_Primary <= 200) TC_MFC_Voltage_Primary = 200;
                                    }
                                }
                                HV_VL_fr = AV_VL_fr = 0;
                            }
                            else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary;
                            TC_MONITOR = 16;
                        }
                    }
                    if((ABS_rl==0) && (EBD_RA==0)) {
                        HV_VL_rl=AV_VL_rl=0;
                    }
                    if((lcu1HdcActiveFlg==1)&&(mpress>=MPRESS_5BAR)) TC_MFC_Voltage_Primary = 200;
                }
                else {
                  #if   __HSA
                    if(HSA_flg==1) {
                        if(HSA_pressure_release_counter>0) {
                            if(HSA_pressure_release_counter==1) {
                                if(HSA_hold_counter>L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 100;
                            }
                            if(TC_MFC_Voltage_Primary>200) {
                                if(HSA_pressure_release_counter>=L_U8_TIME_10MSLOOP_700MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 1;
                                else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 2;
                            }
                            else TC_MFC_Voltage_Primary = 200;
                        }
                        else {
                            if(TC_MFC_Voltage_Primary==0) {
                                TC_MFC_Voltage_Primary = (uint16_t)HSA_ENTER_press + 300;
                            }
                            else if(HSA_hold_counter==L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 100;
                        }
                    }
                    else {
                  #endif
                    TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
/*                      TC_MFC_Voltage_Primary = 400; */
                    TC_MONITOR = 111;
                  #if   __HSA
                    }
                  #endif
                }
              #else
                  #if   __HSA
                if(HSA_flg==1) {
                    if(HSA_pressure_release_counter>0) {
                        if(HSA_pressure_release_counter==1) {
                            if(HSA_hold_counter>L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 100;
                        }
                        if(TC_MFC_Voltage_Primary>200) {
                            if(HSA_pressure_release_counter>=L_U8_TIME_10MSLOOP_700MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 1;
                            else TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - 2;
                        }
                        else TC_MFC_Voltage_Primary = 200;
                    }
                    else {
                        if(TC_MFC_Voltage_Primary==0) {
                            TC_MFC_Voltage_Primary = (uint16_t)HSA_ENTER_press + 300;
                        }
                        else if(HSA_hold_counter==L_U8_TIME_10MSLOOP_200MS) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary + 100;
                    }
                }
                else {
                  #endif
                TC_MFC_Voltage_Primary = TC_CURRENT_MAX;
                TC_MONITOR = 111;
                  #if   __HSA
                }
                  #endif
              #endif
            }
        }
    }
    else {
      #if   __HDC || __ACC
        if((lcu1HdcActiveFlg==1) || (lcu1AccActiveFlg==1)) {
            TCL_DEMAND_fr=1;
            SOL_PTCNO_DISABLE = 0;
            S_VALVE_RIGHT = 1;

            if((lcu1HdcActiveFlg==1)&&(mpress>=MPRESS_5BAR)) TC_MFC_Voltage_Primary = 0;
            else {
                if(TC_MFC_Voltage_Primary > TC_CURRENT_DEC) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Primary - TC_CURRENT_DEC;
                else TC_MFC_Voltage_Primary = 0;
/*              if(TC_MFC_Voltage_Primary <= TC_CURRENT_MIN) TC_MFC_Voltage_Primary = TC_CURRENT_MIN; */
            }
            if(ABS_fr==0) HV_VL_fr = AV_VL_fr = 0;
            if((ABS_rl==0) && (EBD_RA==0)) HV_VL_rl=AV_VL_rl=0;
        }
        else {
            if(ABS_fz==1) {
              #if   defined(__CAN_SW)
/*              if((RX_OK_flag==1) && (HSA_CAN_SWITCH==1)) TC_MFC_Voltage_Primary = 450; // about 20bar */
/*              if((RX_OK_flag==1) && (HDC_CAN_SWITCH==1))  */
                if(HSA_CAN_SWITCH==1) {
                    TC_MFC_Voltage_Primary = 500; /* about 20bar */
                    TCL_DEMAND_fr=1;
                    SOL_PTCNO_DISABLE = 0;
                }
                else TC_MFC_Voltage_Primary = 0;
              #else
                TC_MFC_Voltage_Primary = 0;
              #endif
            }
            else {
                TC_MFC_Voltage_Primary = 0;
                if(BTCS_fr==1) TC_MONITOR = 7;
            }
        }
      #else
        if(ABS_fz==1) {
          #if   defined(__CAN_SW)
/*          if((RX_OK_flag==1) && (HSA_CAN_SWITCH==1)) TC_MFC_Voltage_Primary = 450; // about 20bar */
/*          if((RX_OK_flag==1) && (HDC_CAN_SWITCH==1))  */
            if(HSA_CAN_SWITCH==1) {
                TC_MFC_Voltage_Primary = 500; /* about 20bar */
                TCL_DEMAND_fr=1;
                SOL_PTCNO_DISABLE = 0;
            }
            else TC_MFC_Voltage_Primary = 0;
          #else
            TC_MFC_Voltage_Primary = 0;
          #endif
        }
        else {
        TC_MFC_Voltage_Primary = 0;
        if(BTCS_fr==1) TC_MONITOR = 17;
        }
      #endif
    }

    if(PBA_ON==0) {
        if(mpress>=MPRESS_150BAR) {
          if(ABS_fz==0) TC_MFC_Voltage_Primary = TC_MFC_Voltage_Secondary = 0;
         #if    !__LVBA
          else {
            if(TC_MFC_Voltage_Primary > 500) TC_MFC_Voltage_Primary=500;
                if(TC_MFC_Voltage_Secondary > 500) TC_MFC_Voltage_Secondary=500;
          }
         #endif
        }
      #if   !__LVBA
        else if(mpress>=MPRESS_100BAR) {
            if((AFZ_OK==1)&&(afz>=-(int16_t)(U8_PULSE_LOW_MED_MU))) {
                if(TC_MFC_Voltage_Primary > 500) TC_MFC_Voltage_Primary=500;
                if(TC_MFC_Voltage_Secondary > 500) TC_MFC_Voltage_Secondary=500;
            }
            else {
                if(TC_MFC_Voltage_Primary > 500) TC_MFC_Voltage_Primary=500;
                if(TC_MFC_Voltage_Secondary > 500) TC_MFC_Voltage_Secondary=500;
            }
        }
        else if(mpress>=MPRESS_70BAR) {
            if(TC_MFC_Voltage_Primary > 700) TC_MFC_Voltage_Primary=700;
            if(TC_MFC_Voltage_Secondary > 700) TC_MFC_Voltage_Secondary=700;
        }
        else if(mpress>=MPRESS_50BAR) {
            if(TC_MFC_Voltage_Primary > 850) TC_MFC_Voltage_Primary=850;
            if(TC_MFC_Voltage_Secondary > 850) TC_MFC_Voltage_Secondary=850;
        }
        else if(mpress>=MPRESS_10BAR) {
            if(TC_MFC_Voltage_Primary > 1000) TC_MFC_Voltage_Primary=1000;
            if(TC_MFC_Voltage_Secondary > 1000) TC_MFC_Voltage_Secondary=1000;
        }
      #endif
    }

    esp_control_mode_old_fl = esp_control_mode_fl;
    esp_control_mode_old_fr = esp_control_mode_fr;
    esp_control_mode_old_rl = esp_control_mode_rl;
    esp_control_mode_old_rr = esp_control_mode_rr;

    if(TCMF_LONG_HOLD_flag_p==1) {
        if(TC_MFC_Voltage_Primary>=900) TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+200;
        else if(TC_MFC_Voltage_Primary>=700) TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+150;
        else if(TC_MFC_Voltage_Primary>=500) TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+100;
        else TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+50;
    }
    else TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary;
    if(TCMF_LONG_HOLD_flag_s==1) {
        if(TC_MFC_Voltage_Secondary>=900) TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+200;
        else if(TC_MFC_Voltage_Secondary>=700) TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+150;
        else if(TC_MFC_Voltage_Secondary>=500) TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+100;
        else TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+50;
    }
    else TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary;

}


  #else  /* ! HMC_JM*/
void    LAMFC_vSetMFCCurrent(void)
{
//    int16_t MFC_Current_DEC, MFC_Current_BA, MFC_Current_VH;

    LAMFC_vLoadCircuitParameters();

    TCMF_LONG_HOLD_flag_s = 0;
    TCMF_LONG_HOLD_flag_p = 0;

    if(MFC_TCNO_Secondary_ACT==1) {
        if(
          #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)  
            #if __SPLIT_TYPE
            ((MFC_ESP_Secondary_CTRL==1)&&(lcescu1TCMFUnderControlRl == 0)&&(lcescu1TCMFUnderControlRr == 0)&&(lcu1OVER2WHControlFlag == 0)&& (lcu1OVER2WHFadeout_RL == 0)&& (lcu1OVER2WHFadeout_RR == 0)&&(lcu1US3WHControlFlag == 0)
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
            	)
            #else
            ((MFC_ESP_Secondary_CTRL==1)&&(lcescu1TCMFUnderControlRr == 0)&&((lcu1US2WHControlFlag_RL == 0) && (lcu1US2WHFadeout_RL == 0)&&(lcu1OVER2WHControlFlag_RR == 0) && (lcu1OVER2WHFadeout_RR == 0) &&(lcu1US3WHControlFlag == 0))
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
            	)
            #endif
          #else
            ((MFC_ESP_Secondary_CTRL==1)
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
            	)
          #endif 
          #if __VDC
          	|| (EPC_ON==1)
          	#if __TSP == 0
             || (PBA_ON==1)
            #endif            
          #endif
          #if __FBC
            || (FBC_ON==1)
          #endif
          #if __HOB
            || (lcu1HobOnFlag==1)
          #endif
          #if (__TSP && (__Decel_Ctrl_Integ==0)) /* 08SWD-10 */
            || (TSP_ON==1)
          #endif
          #if __BDW
            || (BDW_ON==1)
          #endif
          #if __EBP
            || (EBP_ON==1)
          #endif
          ) {
/*          MFC_Current_ESP_S = LAMFC_s16SetMFCCurrentESP(); */
          #if (__ESC_TCMF_CONTROL ==1)
            if(FL_ESC.lcespu1TCMFControl ==1)
            {
            	  MFC_Current_ESP_S = LAMFC_s16SetMFCCurESP_TCMF_Sec(U16_SECONDARY_CIRCUIT, MFC_Current_ESP_S);
            }
            else
            {
            	  MFC_Current_ESP_S = TC_CURRENT_MAX;
            }
          #else
            MFC_Current_ESP_S = TC_CURRENT_MAX;
          #endif

          #if __PBA_MFC_EXIT
            if(PBA_ON==1) {MFC_Current_PBA_S = MFC_Current_ESP_S;}
            else {MFC_Current_PBA_S = 0;}
          #endif
        }
              #if ((__TCMF_UNDERSTEER_CONTROL == ENABLE) ||(__ESC_COMPETITIVE_CONTROL == 1))
        	#if(__OS2WHEEL_IN_COMBINATION ==1)
        		#if __SPLIT_TYPE
        else if ((MFC_ESP_Secondary_CTRL == 1)&&((((lcu1OVER2WHControlFlag_RL ==1)||(lcu1OVER2WHFadeout_RL ==1))&&(ABS_fz ==1))||(((lcu1OVER2WHControlFlag_RR ==1)||(lcu1OVER2WHFadeout_RR ==1))&&(ABS_fz ==1))))
        {
        	MFC_Current_ESP_S = TC_CURRENT_MAX;
        }		
        		#else
        else if ((MFC_ESP_Secondary_CTRL == 1)&&((lcu1OVER2WHControlFlag_RR ==1)||(lcu1OVER2WHFadeout_RR ==1))&&(ABS_fz ==1))
        {
        	MFC_Current_ESP_S = TC_CURRENT_MAX;
        }
        		#endif
        	#endif
        else if (MFC_ESP_Secondary_CTRL == 1)
        {      
            MFC_Current_ESP_S = LAMFC_s16SetMFCCurrentESP(U16_SECONDARY_CIRCUIT, MFC_Current_ESP_S);
        }
              #endif
		#if __TSP && __VDC && __PBA_MFC_EXIT
        else if((lctspu1TspPbaComb==1) || (PBA_ON==1))
        {
            if(lctspu1TspPbaComb==1) 
            	{
            	MFC_Current_PBA_S = LCESP_s16IInter3Point(vref5, VREF_50_KPH, 650, VREF_60_KPH, 500, VREF_80_KPH, S16_TSP_PBA_ADD_CURRENT);
            	}
            else {MFC_Current_PBA_S = TC_CURRENT_MAX;}
				}
		#endif
        else {
            MFC_Current_ESP_S = 0;  /*lamfcs16TCCurrentESPSnd = MFC_Current_ESP_S; */
/*        #if __PBA_MFC_EXIT
            MFC_Current_PBA_S = 0;
          #endif
*/

          #if __PBA_MFC_EXIT
            if((PBA_EXIT_CONTROL==1) && (PBA_EXIT_CONTROL_cnt>0))
            {
                if(MFC_Current_PBA_S>850)
                {
                    MFC_Current_PBA_S = 850;
                }

                if(MFC_Current_PBA_S>400)
                {
                    if(mpress<(int16_t)U16_PBA_PRESS_EXIT_THR1)
                    {
                        MFC_Current_PBA_S = MFC_Current_PBA_S - 50;
                    }
                    else
                    {
                        MFC_Current_PBA_S = MFC_Current_PBA_S - 10;
                    }
                }
                else
                {
                    MFC_Current_PBA_S = 0;
                }
            }
            else
            {
                MFC_Current_PBA_S = 0;
            }
          #endif

        #if __Decel_Ctrl_Integ  /* 08SWD-10 */
                #if __DEC
            MFC_Current_DEC_S = LAMFC_s16SetMFCCurrentDEC(0,&laDecValveS, MFC_Current_DEC_S);
                  #if __TSP
            MFC_Current_TSP_S = LAMFC_s16SetMFCCurrentTSP(0, MFC_Current_DEC_S);
                  #endif
                #else
            MFC_Current_DEC_S = 0;
                #endif
        #else
                #if __ACC
            MFC_Current_ACC_S = LAMFC_s16SetMFCCurrentACC(&laAccValveS, MFC_Current_ACC_S);
                #else
            MFC_Current_ACC_S = 0;
                #endif

                #if __EPB_INTERFACE
            MFC_Current_EPB_S = LAMFC_s16SetMFCCurrentEPB(&laEpbValveS, MFC_Current_EPB_S);
                #else
            MFC_Current_EPB_S = 0;
                #endif
        #endif
                #if __HDC
            MFC_Current_HDC_S = LAMFC_s16SetMFCCurrentHDC(0,&laHdcValveS, MFC_Current_HDC_S);
                #else
            MFC_Current_HDC_S = 0;
                #endif
                
            #if __SCC
            if (lcu8WpcActiveFlg==1)
            {                
            	MFC_Current_SCC_S = ((int16_t)lcu16WpcSecCur*10);
            }
            else
            {
            	MFC_Current_SCC_S = 0;
            }		
            #endif                

            #if __HSA
        #if (__SPLIT_TYPE==0)
            MFC_Current_HSA_S = LAMFC_s16SetMFCCurrentHSA(MFC_Current_HSA_S);
        #else
            MFC_Current_HSA_S = LAMFC_s16SetMFCCurrentHSA(MFC_Current_HSA_S);
        #endif
            #endif

              #if __AVH
            MFC_Current_AVH_old = MFC_Current_AVH_S;  
            MFC_Current_AVH_S = LAMFC_s16SetMFCCurrentAVH(MFC_Current_AVH_S);
              #else
            MFC_Current_AVH_S = 0;
              #endif

			#if __SPAS_INTERFACE
            MFC_Current_SPAS_S = LAMFC_s16SetMFCCurrentSPAS(MFC_Current_SPAS_S);
            #endif

            tempC1 = U16_SECONDARY_CIRCUIT;
            MFC_Current_HBB_S = LAMFC_s16SetMFCCurrentHBB(tempC1, MFC_Current_HBB_S);

            MFC_Current_ARB_S = LAMFC_s16SetMFCCurrentARB(MFC_Current_ARB_S);

            MFC_Current_HRB_S = LAMFC_s16SetMFCCurrentHRB(tempC1, MFC_Current_HRB_S);
             
             #if __TVBB                                                                             
            MFC_Current_TVBB_S = LAMFC_s16SetMFCCurrentTVBB(tempC1, MFC_Current_TVBB_S);            
             #endif                                                                                 
            
              #if __LVBA
            MFC_Current_LVBA_S = LAMFC_s16SetMFCCurrentLVBA(tempC1, MFC_Current_LVBA_S);
              #endif
        }

        if((MFC_Current_HDC_S==0) && (MFC_Current_HSA_S==0)  && (MFC_Current_HBB_S==0)
            && (MFC_Current_ARB_S==0) && (MFC_Current_HRB_S==0)
          #if __Decel_Ctrl_Integ  /* 08SWD-10 */
            && (MFC_Current_DEC_S==0)
              #if __TSP
            && (MFC_Current_TSP_S==0)
              #endif
          #else
            && (MFC_Current_ACC_S==0) && (MFC_Current_EPB_S==0) 
          #endif
          #if __AVH
            && (MFC_Current_AVH_S==0)
          #endif
          #if __SPAS_INTERFACE
            && (MFC_Current_SPAS_S==0)
          #endif
          #if __SCC
            && (MFC_Current_SCC_S==0)
          #endif
          #if __TVBB
            && (MFC_Current_TVBB_S==0)
          #endif
             #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
           && (MFC_Current_ESP_S == 0)
             #endif  
          
        )
        {
             #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
            if ((lcescu1TCMFUnderControlRl == 0) && (lcescu1TCMFUnderControlRr == 0) && (lcu1US2WHControlFlag_RL == 0) && (lcu1US2WHControlFlag_RR == 0) && (lcu1US2WHFadeout_RL == 0) && (lcu1US2WHFadeout_RR == 0)
            	&&(lcu1OVER2WHControlFlag_RL == 0) && (lcu1OVER2WHControlFlag_RR == 0) && (lcu1OVER2WHFadeout_RL == 0) && (lcu1OVER2WHFadeout_RR == 0)) 
            {
                #if (__ESC_TCMF_CONTROL ==1)
            	  if(FL_ESC.lcespu1TCMFControl ==1)
            	  {
            	  	  MFC_Current_ESP_S = LAMFC_s16SetMFCCurESP_TCMF_Sec(U16_SECONDARY_CIRCUIT, MFC_Current_ESP_S);
            	  }
            	  else
            {
                 MFC_Current_ESP_S = TC_CURRENT_MAX;
            }
            	  #else
                MFC_Current_ESP_S = TC_CURRENT_MAX;
                #endif
                
            		#if (__ESC_COMPETITIVE_CONTROL == 1)
              	  if(ldespu1CompetitiveModeESC==1)
              	  {
              	      MFC_Current_ESP_S = LAMFC_s16SetMFCCurrentESP(U16_SECONDARY_CIRCUIT, MFC_Current_ESP_S);
              	  }
            else
            {
                ;
            }
            		#endif    
            }
            else
            {
                ;
            }
             #else
            MFC_Current_ESP_S = TC_CURRENT_MAX;
            	 
            #if (__ESC_COMPETITIVE_CONTROL == 1)
             if(ldespu1CompetitiveModeESC==1)
             {
                 MFC_Current_ESP_S = LAMFC_s16SetMFCCurrentESP(U16_SECONDARY_CIRCUIT, MFC_Current_ESP_S);
            }
            else
            {
                ;
            }
            #endif
             #endif
        }
        else { }
    }
    else {
        MFC_Current_ESP_S = 0;

    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
      #if __DEC
        tempC1 = (uint8_t)lcu1DecActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_DEC_S = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_DEC_S);
        #if __TSP
        MFC_Current_TSP_S = LAMFC_s16SetMFCCurrentTSP(0, 0);
        #endif
    #else
      #if __ACC
        tempC1 = (uint8_t)lcu1AccActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_ACC_S = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_ACC_S);

        MFC_Current_EPB_S = 0;
    #endif

      #if __HDC
        tempC1 = (uint8_t)lcu1HdcActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_HDC_S = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_HDC_S);

        MFC_Current_HSA_S = 0;

        MFC_Current_AVH_S = 0;

      #if __SPAS_INTERFACE
        MFC_Current_SPAS_S = 0;
      #endif

        MFC_Current_HBB_S = 0;

        MFC_Current_ARB_S = 0;

        MFC_Current_HRB_S = LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_HRB_S);
        
      #if __TVBB
        MFC_Current_TVBB_S = 0;
      #endif        

      #if __PBA_MFC_EXIT
        if((PBA_EXIT_CONTROL==1) && (PBA_EXIT_CONTROL_cnt>0))
        {
            if(MFC_Current_PBA_S>850)
            {
                MFC_Current_PBA_S = 850;
            }

            if(MFC_Current_PBA_S>400)
            {
                if(mpress<(int16_t)U16_PBA_PRESS_EXIT_THR1)
                {
                    MFC_Current_PBA_S = MFC_Current_PBA_S - 50;
                }
                else
                {
                    MFC_Current_PBA_S = MFC_Current_PBA_S - 10;
                }
            }
            else
            {
                MFC_Current_PBA_S = 0;
            }
        }
        else
        {
            MFC_Current_PBA_S = 0;
        }
      #endif
      #if __SCC
    	MFC_Current_SCC_S = 0;
      #endif                

    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
        if((MFC_Current_DEC_S>0) || (MFC_Current_HDC_S>0) || (MFC_Current_HSA_S>0) || (MFC_Current_HBB_S>0)
        #if __TSP	
        	|| (MFC_Current_TSP_S>0)
        #endif
    #else
        if((MFC_Current_ACC_S>0) || (MFC_Current_HDC_S>0) || (MFC_Current_HSA_S>0) || (MFC_Current_HBB_S>0)
    #endif
            || (MFC_Current_ARB_S>0)
            || (MFC_Current_HRB_S>0)
          #if __PBA_MFC_EXIT
            || (MFC_Current_PBA_S>0)
          #endif
          #if __AVH
            || (MFC_Current_AVH_S>0)
          #endif
          #if __SPAS_INTERFACE
            || (MFC_Current_SPAS_S>0)
          #endif          
          #if __SCC
            || (MFC_Current_SCC_S>0)
          #endif
          #if __TVBB
            || (MFC_Current_TVBB_S>0)
          #endif
        )
        {
            MFC_TCNO_Secondary_ACT = 1;
        }
        else { }
    }

    if(MFC_TCNO_Primary_ACT==1) {
        if(
          #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
            #if !__SPLIT_TYPE
            ((MFC_ESP_Primary_CTRL==1)&&(lcescu1TCMFUnderControlRl == 0)&&(lcu1US2WHControlFlag_RR == 0)&&(lcu1US2WHFadeout_RR == 0)&&(lcu1OVER2WHControlFlag_RL == 0) && (lcu1OVER2WHFadeout_RL == 0)
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
             	)
            #else
            ((MFC_ESP_Primary_CTRL==1)&&(lcu1US2WHControlFlag_RL == 0)&&(lcu1US2WHControlFlag_RR == 0)&&(lcu1US2WHFadeout_RL == 0)&&(lcu1US2WHFadeout_RR == 0)
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
            	)
            #endif
          #else
            ((MFC_ESP_Primary_CTRL==1)
            	#if (__ESC_COMPETITIVE_CONTROL == 1)
            		&&(ldespu1CompetitiveModeESC==0)
            	#endif
             )
          #endif 
          #if __VDC
            || (EPC_ON==1) 
            #if (__TSP==0)
            || (PBA_ON==1)
            #endif
          #endif
          #if __FBC
            || (FBC_ON==1)
          #endif
          #if __HOB
            || (lcu1HobOnFlag==1)
          #endif
          #if (__TSP && (__Decel_Ctrl_Integ==0))
            || (TSP_ON==1)
          #endif
          #if __BDW
            || (BDW_ON==1)
          #endif
          #if __EBP
            || (EBP_ON==1)
          #endif
          ) {
/*          MFC_Current_ESP_P = LAMFC_s16SetMFCCurrentESP(); */
          #if (__ESC_TCMF_CONTROL ==1)
            if(FR_ESC.lcespu1TCMFControl ==1)
            {
            	MFC_Current_ESP_P = LAMFC_s16SetMFCCurESP_TCMF_Pri(U16_PRIMARY_CIRCUIT, MFC_Current_ESP_P);
        	}
            else
            {
            MFC_Current_ESP_P = TC_CURRENT_MAX;
        }
          #else
            MFC_Current_ESP_P = TC_CURRENT_MAX;
          #endif  
        }
              #if ((__TCMF_UNDERSTEER_CONTROL == ENABLE)||(__ESC_COMPETITIVE_CONTROL == 1))
        	#if(__OS2WHEEL_IN_COMBINATION ==1)
        		#if !__SPLIT_TYPE
        else if ((MFC_ESP_Primary_CTRL == 1)&&((lcu1OVER2WHControlFlag_RL ==1)||(lcu1OVER2WHFadeout_RL ==1))&&(ABS_fz ==1))
        {      
            MFC_Current_ESP_P = TC_CURRENT_MAX;
        }
        		#endif
        	#endif
        else if (MFC_ESP_Primary_CTRL == 1)
        {      
            MFC_Current_ESP_P = LAMFC_s16SetMFCCurrentESP(U16_PRIMARY_CIRCUIT, MFC_Current_ESP_P);
        }
              #endif
		#if __TSP && __VDC && __PBA_MFC_EXIT
        else if((lctspu1TspPbaComb==1) && (PBA_ON==1))
        {
            if(lctspu1TspPbaComb==1)
            	{
            	MFC_Current_PBA_P = LCESP_s16IInter3Point(vref5, VREF_50_KPH, 650, VREF_60_KPH, 500, VREF_80_KPH, S16_TSP_PBA_ADD_CURRENT);
            	}
            else {MFC_Current_PBA_P = TC_CURRENT_MAX;}
				}
		#endif
        else {
            MFC_Current_ESP_P = 0;

        #if __Decel_Ctrl_Integ  /* 08SWD-10 */
                #if __DEC
            MFC_Current_DEC_P = LAMFC_s16SetMFCCurrentDEC(1,&laDecValveP, MFC_Current_DEC_P);
            		#if __TSP
            MFC_Current_TSP_P = LAMFC_s16SetMFCCurrentTSP(1, MFC_Current_DEC_P);
                #endif
                #else
            MFC_Current_DEC_P = 0;
                #endif
        #else
                #if __ACC
            MFC_Current_ACC_P = LAMFC_s16SetMFCCurrentACC(&laAccValveP, MFC_Current_ACC_P);
                #else
            MFC_Current_ACC_P = 0;
                #endif

                #if __EPB_INTERFACE
            MFC_Current_EPB_P = LAMFC_s16SetMFCCurrentEPB(&laEpbValveP, MFC_Current_EPB_P);
                #else
            MFC_Current_EPB_P = 0;
                #endif
        #endif

                #if __HDC
            MFC_Current_HDC_P = LAMFC_s16SetMFCCurrentHDC(1,&laHdcValveP, MFC_Current_HDC_P);
                #else
            MFC_Current_HDC_P = 0;
                #endif

            #if __SCC
            if (lcu8WpcActiveFlg==1)
            {                
            	MFC_Current_SCC_P = ((int16_t)lcu16WpcPriCur*10);
            }
            else
            {
            	MFC_Current_SCC_P = 0;
            }		
            #endif   

              #if __HSA
            MFC_Current_HSA_P = LAMFC_s16SetMFCCurrentHSA(MFC_Current_HSA_P);
              #endif
            
              #if __AVH
            MFC_Current_AVH_P = LAMFC_s16SetMFCCurrentAVH(MFC_Current_AVH_P);
              #else
            MFC_Current_AVH_P = 0;  
              #endif
              
              #if __SPAS_INTERFACE
            MFC_Current_SPAS_P = LAMFC_s16SetMFCCurrentSPAS(MFC_Current_SPAS_P);
              #endif
              
            tempC1 = U16_PRIMARY_CIRCUIT;
            MFC_Current_HBB_P = LAMFC_s16SetMFCCurrentHBB(tempC1, MFC_Current_HBB_P);

            MFC_Current_ARB_P = LAMFC_s16SetMFCCurrentARB(MFC_Current_ARB_P);

            MFC_Current_HRB_P = LAMFC_s16SetMFCCurrentHRB(tempC1, MFC_Current_HRB_P);
            
             #if __TVBB
            MFC_Current_TVBB_P = LAMFC_s16SetMFCCurrentTVBB(tempC1, MFC_Current_TVBB_P);
             #endif

             #if __LVBA
            MFC_Current_LVBA_P = LAMFC_s16SetMFCCurrentLVBA(tempC1, MFC_Current_LVBA_P);
             #endif

        }

        if((MFC_Current_HDC_P==0) && (MFC_Current_HSA_P==0) && (MFC_Current_HBB_P==0)
            && (MFC_Current_ARB_P==0) && (MFC_Current_HRB_P==0)
          #if __Decel_Ctrl_Integ  /* 08SWD-10 */
            && (MFC_Current_DEC_P==0)
            	#if __TSP
            && (MFC_Current_TSP_P==0)
              #endif
          #else
            && (MFC_Current_ACC_P==0) && (MFC_Current_EPB_P==0) 
          #endif
          #if __AVH
            && (MFC_Current_AVH_P==0)
          #endif
          #if __SPAS_INTERFACE
            && (MFC_Current_SPAS_P==0)
          #endif          
          #if __SCC
            && (MFC_Current_SCC_P==0)
          #endif
          #if __TVBB
            && (MFC_Current_TVBB_P==0) 
          #endif
        ) 
        {
              #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
            if ((lcescu1TCMFUnderControlRl == 0) && (lcescu1TCMFUnderControlRr == 0) && (lcu1US2WHControlFlag_RL == 0) && (lcu1US2WHControlFlag_RR == 0) && (lcu1US2WHFadeout_RL == 0) && (lcu1US2WHFadeout_RR == 0)
            	&&(lcu1OVER2WHControlFlag_RL == 0) && (lcu1OVER2WHControlFlag_RR == 0) && (lcu1OVER2WHFadeout_RL == 0) && (lcu1OVER2WHFadeout_RR == 0))
            {
                #if (__ESC_TCMF_CONTROL ==1)
            	  if(FR_ESC.lcespu1TCMFControl ==1)
            	  {
                	  MFC_Current_ESP_P = LAMFC_s16SetMFCCurESP_TCMF_Pri(U16_PRIMARY_CIRCUIT, MFC_Current_ESP_P);
            	  }
            	  else
            {
                MFC_Current_ESP_P = TC_CURRENT_MAX;
            }
            	  #else
            	      MFC_Current_ESP_P = TC_CURRENT_MAX;
            	    #endif

            		#if (__ESC_COMPETITIVE_CONTROL == 1)
            		 	if(ldespu1CompetitiveModeESC==1)
            		 	{
            		 	    MFC_Current_ESP_P = LAMFC_s16SetMFCCurrentESP(U16_PRIMARY_CIRCUIT, MFC_Current_ESP_P);
            		 	}
            else
            {
              ;
            }
            		#endif
            }
            else
            {
                ;
            }
              #else
            MFC_Current_ESP_P = TC_CURRENT_MAX;
            
            #if (__ESC_COMPETITIVE_CONTROL == 1)
             	if(ldespu1CompetitiveModeESC==1)
             	{
             	    MFC_Current_ESP_P = LAMFC_s16SetMFCCurrentESP(U16_PRIMARY_CIRCUIT, MFC_Current_ESP_P);
            }
            else
            {
                ;
            }
            #endif
              #endif
        }
        else { }

      #if __PBA_MFC_EXIT
        MFC_Current_PBA_P = MFC_Current_PBA_S;
      #endif
    }
    else {
        MFC_Current_ESP_P = 0;
    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
      #if __DEC
        tempC1 = (uint8_t)lcu1DecActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_DEC_P = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_DEC_P);
      #if __TSP  
        MFC_Current_TSP_P = LAMFC_s16SetMFCCurrentTSP(1, 0);
      #endif
    #else
      #if __ACC
        tempC1 = (uint8_t)lcu1AccActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_ACC_P = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_ACC_P);

        MFC_Current_EPB_P = 0;
    #endif

      #if __HDC
        tempC1 = (uint8_t)lcu1HdcActiveFlg;
      #else
        tempC1 = 0;
      #endif
        MFC_Current_HDC_P = LAMFC_s16SetMFCCurrentAtTCOff(tempC1, MFC_Current_HDC_P);

        MFC_Current_HSA_P = 0;

        MFC_Current_AVH_P = 0;
        
      #if __SPAS_INTERFACE
        MFC_Current_SPAS_P = 0;
      #endif

        MFC_Current_HBB_P = 0;

        MFC_Current_ARB_P = 0;

        MFC_Current_HRB_P = LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_HRB_P);
        
      #if __TVBB
        MFC_Current_TVBB_P = 0;
      #endif  

      #if __PBA_MFC_EXIT
        MFC_Current_PBA_P = MFC_Current_PBA_S;
      #endif
      #if __SCC
    	MFC_Current_SCC_P = 0;
      #endif                

    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
        if((MFC_Current_DEC_P>0) || (MFC_Current_HDC_P>0) || (MFC_Current_HSA_P>0) || (MFC_Current_HBB_P>0)
        	  #if __TSP
        	|| (MFC_Current_TSP_P>0)
        	  #endif
    #else
        if((MFC_Current_ACC_P>0) || (MFC_Current_HDC_P>0) || (MFC_Current_HSA_P>0) || (MFC_Current_HBB_P>0)
    #endif
            || (MFC_Current_ARB_P>0)
            || (MFC_Current_HRB_P>0)
          #if __PBA_MFC_EXIT
            || (MFC_Current_PBA_P>0)
          #endif
          #if __AVH
            || (MFC_Current_AVH_P>0)
          #endif
          #if __SPAS_INTERFACE
            || (MFC_Current_SPAS_P>0)
          #endif          
          #if __SCC
            || (MFC_Current_SCC_P>0)
          #endif
          #if __TVBB
        	|| (MFC_Current_TVBB_P>0)
          #endif
        )
        {
            MFC_TCNO_Primary_ACT = 1;
        }
        else { }
    }

    if(MFC_Current_ESP_S>0)
    {
        if(MFC_Current_ESP_P>0)
        {
    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
            MFC_Current_DEC_S = MFC_Current_DEC_S;
            MFC_Current_DEC_P = MFC_Current_DEC_P;
    #else
            MFC_Current_ACC_S = MFC_Current_ACC_S;
            MFC_Current_ACC_P = MFC_Current_ACC_P;
            MFC_Current_EPB_S = MFC_Current_EPB_S;
            MFC_Current_EPB_P = MFC_Current_EPB_P;
    #endif
            MFC_Current_HDC_S = MFC_Current_HDC_S;
            MFC_Current_HDC_P = MFC_Current_HDC_P;
            MFC_Current_HSA_S = MFC_Current_HSA_S;
            MFC_Current_HSA_P = MFC_Current_HSA_P;
            MFC_Current_AVH_S = MFC_Current_AVH_S;
            MFC_Current_AVH_P = MFC_Current_AVH_P;
    #if __SPAS_INTERFACE       
            MFC_Current_SPAS_S = MFC_Current_SPAS_S;
            MFC_Current_SPAS_P = MFC_Current_SPAS_P;
    #endif      
        }
        else
        {
    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
            MFC_Current_DEC_S = MFC_Current_DEC_P;
    #else
            MFC_Current_ACC_S = MFC_Current_ACC_P;
            MFC_Current_EPB_S = MFC_Current_EPB_P;
    #endif
            MFC_Current_HDC_S = MFC_Current_HDC_P;
            MFC_Current_HSA_S = MFC_Current_HSA_P;
            MFC_Current_AVH_S = MFC_Current_AVH_P;
    #if __SPAS_INTERFACE       
            MFC_Current_SPAS_S = MFC_Current_SPAS_P;
    #endif        
          #if __PBA_MFC_EXIT
            MFC_Current_PBA_S = MFC_Current_PBA_P;
          #endif
            MFC_Current_HRB_S = MFC_Current_HRB_P;
        }
    }
    else
    {
        if(MFC_Current_ESP_P>0)
        {
    #if __Decel_Ctrl_Integ  /* 08SWD-10 */
            MFC_Current_DEC_P = MFC_Current_DEC_S;
    #else
            MFC_Current_ACC_P = MFC_Current_ACC_S;
            MFC_Current_EPB_P = MFC_Current_EPB_S;
    #endif
            MFC_Current_HDC_P = MFC_Current_HDC_S;
            MFC_Current_HSA_P = MFC_Current_HSA_S;
            MFC_Current_AVH_P = MFC_Current_AVH_S;
    #if __SPAS_INTERFACE       
            MFC_Current_SPAS_P = MFC_Current_SPAS_S;
    #endif                      
          #if __PBA_MFC_EXIT
            MFC_Current_PBA_P = MFC_Current_PBA_S;
          #endif
            MFC_Current_HRB_P = MFC_Current_HRB_S;
        }
        else
        {
            ;
        }
    }

      MFC_Current_DEC=0; 
    tempC1 = U16_SECONDARY_CIRCUIT;
  #if   __Decel_Ctrl_Integ  /* 08SWD-10 */
    if(MFC_Current_DEC > MFC_Current_DEC_S)     {MFC_Current_DEC = MFC_Current_DEC;}
    else                                        {MFC_Current_DEC = MFC_Current_DEC_S;}
      #if __TSP
	    if(MFC_Current_TSP_S > MFC_Current_DEC)   {MFC_Current_DEC = MFC_Current_TSP_S;}
        else                                      {MFC_Current_DEC = MFC_Current_DEC;}
        lcs16TspMFCCurS = MFC_Current_DEC;
      #endif

			
  #else
    if(MFC_Current_EPB_S > MFC_Current_ACC_S)   {MFC_Current_DEC = MFC_Current_EPB_S;}
    else                                        {MFC_Current_DEC = MFC_Current_ACC_S;}
  #endif
  #if __SCC  
    if(MFC_Current_DEC > MFC_Current_SCC_S)     {MFC_Current_DEC = MFC_Current_DEC;}  
	else                                        {MFC_Current_DEC = MFC_Current_SCC_S;}
  #endif

  #if __PBA_MFC_EXIT
    if(MFC_Current_PBA_S > MFC_Current_HBB_S)
    {
        if(MFC_Current_PBA_S > MFC_Current_HRB_S) {MFC_Current_BA = MFC_Current_PBA_S;}
        else                                      {MFC_Current_BA = MFC_Current_HRB_S;}
    }
    else
  #endif
    {
        if(MFC_Current_HBB_S > MFC_Current_HRB_S) {MFC_Current_BA = MFC_Current_HBB_S;}
        else                                      {MFC_Current_BA = MFC_Current_HRB_S;}
    }
  
  #if __TVBB
    if (MFC_Current_TVBB_S > MFC_Current_BA)
    {
        MFC_Current_BA = MFC_Current_TVBB_S;
    }
    else
    {
        MFC_Current_BA = MFC_Current_BA;
    }
  #endif

  #if __LVBA  
    if(MFC_Current_BA > MFC_Current_LVBA_S)     {MFC_Current_BA = MFC_Current_BA;}  
	else                                        {MFC_Current_BA = MFC_Current_LVBA_S;}
  #endif

    if(MFC_Current_ARB_S > MFC_Current_HSA_S)   {MFC_Current_VH = MFC_Current_ARB_S;}
    else                                        {MFC_Current_VH = MFC_Current_HSA_S;}

    if(MFC_Current_AVH_S > MFC_Current_VH)  	{MFC_Current_VH = MFC_Current_AVH_S;}
    else                                        {MFC_Current_VH = MFC_Current_VH;}
  #if __SPAS_INTERFACE       
    if(MFC_Current_SPAS_S > MFC_Current_VH)  	{MFC_Current_VH = MFC_Current_SPAS_S;}
    else                                        {MFC_Current_VH = MFC_Current_VH;}
  #endif

    LAMFC_vArrangeMFCCurrent(tempC1, MFC_Current_ESP_S, MFC_Current_DEC, MFC_Current_HDC_S, MFC_Current_VH, MFC_Current_BA);

    tempC1 = U16_PRIMARY_CIRCUIT;
      MFC_Current_DEC=0; 
  #if   __Decel_Ctrl_Integ  /* 08SWD-10 */
    if(MFC_Current_DEC > MFC_Current_DEC_P)     {MFC_Current_DEC = MFC_Current_DEC;}
    else                                        {MFC_Current_DEC = MFC_Current_DEC_P;}
      #if __TSP
	    if(MFC_Current_TSP_P > MFC_Current_DEC)   {MFC_Current_DEC = MFC_Current_TSP_P;}
        else                                      {MFC_Current_DEC = MFC_Current_DEC;}
		lcs16TspMFCCurP = MFC_Current_DEC;
	  #endif
  #else
    if(MFC_Current_EPB_P > MFC_Current_ACC_P)   {MFC_Current_DEC = MFC_Current_EPB_P;}
    else                                        {MFC_Current_DEC = MFC_Current_ACC_P;}
  #endif
  #if __SCC    
    if(MFC_Current_DEC > MFC_Current_SCC_P)     {MFC_Current_DEC = MFC_Current_DEC;}  
	else                                        {MFC_Current_DEC = MFC_Current_SCC_P;}
  #endif
  
  #if __PBA_MFC_EXIT
    if(MFC_Current_PBA_P > MFC_Current_HBB_P)
    {
        if(MFC_Current_PBA_P > MFC_Current_HRB_P) {MFC_Current_BA = MFC_Current_PBA_P;}
        else                                      {MFC_Current_BA = MFC_Current_HRB_P;}
    }
    else
  #endif
    {
        if(MFC_Current_HBB_P > MFC_Current_HRB_P) {MFC_Current_BA = MFC_Current_HBB_P;}
        else                                      {MFC_Current_BA = MFC_Current_HRB_P;}
    }

  #if __TVBB
    if (MFC_Current_TVBB_P > MFC_Current_BA)
    {
        MFC_Current_BA = MFC_Current_TVBB_P;
    }
    else
    {
        MFC_Current_BA = MFC_Current_BA;
    }
  #endif 

  #if __LVBA  
    if(MFC_Current_BA > MFC_Current_LVBA_P)     {MFC_Current_BA = MFC_Current_BA;}  
	else                                        {MFC_Current_BA = MFC_Current_LVBA_P;}
  #endif

    if(MFC_Current_ARB_P > MFC_Current_HSA_P)   {MFC_Current_VH = MFC_Current_ARB_P;}
    else                                        {MFC_Current_VH = MFC_Current_HSA_P;}

    if(MFC_Current_AVH_P > MFC_Current_VH)  	{MFC_Current_VH = MFC_Current_AVH_P;}
    else                                        {MFC_Current_VH = MFC_Current_VH;}
  #if __SPAS_INTERFACE       
    if(MFC_Current_SPAS_P > MFC_Current_VH)  	{MFC_Current_VH = MFC_Current_SPAS_P;}
    else                                        {MFC_Current_VH = MFC_Current_VH;}
  #endif
    LAMFC_vArrangeMFCCurrent(tempC1, MFC_Current_ESP_P, MFC_Current_DEC, MFC_Current_HDC_P, MFC_Current_VH, MFC_Current_BA);


  #if __MFC_L9352B

    LAMFC_vCalcMFCDuty();

  #endif

  #if __HW_TEST_MODE
    LAMFC_vTestTcNoValve();
  #endif
  
    LAMFC_vSetCircuitParameters();

}
   
int16_t LAMFC_s16SetMFCCurrentAtTCOff(uint8_t CONTROL_ACTIVE_flg, int16_t MFC_Current_old)
{
    int16_t MFC_Current_new = 0;

#if __VDC
  #if __HBB
    if(CONTROL_ACTIVE_flg==1)
  #else
    if((CONTROL_ACTIVE_flg==1) || (MFC_Current_old>150))
  #endif
    {
        if(MFC_Current_old>150) {
/*          MFC_Current_new = MFC_Current_old - 3; */
            if((mpress>MPRESS_5BAR) || (mtp > 5)) {
                if(MFC_Current_old>=600) {MFC_Current_new = MFC_Current_old - 20;}
                else {MFC_Current_new = MFC_Current_old - 10;}
            }
            else {
                MFC_Current_new = MFC_Current_old - 3;
            }
        }
        else {
            MFC_Current_new = 0;
        }
    }
  #if __HBB
    else if(MFC_Current_old>300)
    {
        if(ABS_fz == 1) 
        {
        	MFC_Current_new = 0;
        }
        else if(MFC_Current_old>=450) 
        {
        	MFC_Current_new = MFC_Current_old - 100;
        }
        else 
        {
        	MFC_Current_new = MFC_Current_old - 50;
        }
    }
  #endif
    else {
        MFC_Current_new = 0;
    }
#endif

    return MFC_Current_new;
}

int16_t LAMFC_s16SetMFCCurrentHBB(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
    int16_t MFC_Current_new = 0;
    int16_t MFC_Current_new_release = 0;

  #if   __HBB                           /* For demo */

    if(TCMF_LowVacuumBoost_flag==1) 
    {
        if((CIRCUIT==U16_PRIMARY_CIRCUIT) || (CIRCUIT==U16_SECONDARY_CIRCUIT))
        {
            if(HBB_ON_cnt <= L_U8_TIME_10MSLOOP_70MS)
            {
                tempW7 = MPRESS_150BAR;
            }
            else if(HBB_ON_cnt <= L_U8_TIME_10MSLOOP_300MS)
            {
                tempW7 = lcs16HbbAssistPressTC2/10*10;
                tempW9 = MPRESS_180BAR - lcs16HbbMCPressureAvg;

                if(tempW7 > tempW9) 
                {
                	tempW7 = tempW9;
                }
                else if(tempW7 < S16_HBB_INIT_LIMIT_ASSIST_PRESS) 
                {
                	tempW7 = S16_HBB_INIT_LIMIT_ASSIST_PRESS; /* MPRESS_20BAR */
                }
                else 
                {
                	;
                }
            }
            else
            {
                tempW7 = lcs16HbbAssistPressTC2/10*10;
                tempW9 = MPRESS_180BAR - lcs16HbbMCPressureAvg;

                if(tempW7 > tempW9) 
                {
                	tempW7 = tempW9;
                }
                else if(tempW7 < S16_HBB_MIN_ASSIST_PRESS)
                {
                	tempW7 = S16_HBB_MIN_ASSIST_PRESS; /* MPRESS_5BAR */
                }
                else 
                {
                	;
                }
            }
            
            if(tempW7 == MPRESS_0BAR)
            {
            	MFC_Current_new = 0;
            }
            else if(tempW7 < MPRESS_45BAR)
            {
                MFC_Current_new = 375 + (((tempW7)/10)*5);
            }
            else
            {
                MFC_Current_new = 600 + (((tempW7-MPRESS_45BAR)/10)*4);
            }
        }
        else
        {
            MFC_Current_new = 0;
        }
    }
    else 
    {
        MFC_Current_new = 0;
    }

  #else
    MFC_Current_new = 0;
  #endif

    return MFC_Current_new;
}

int16_t LAMFC_s16SetMFCCurrentARB(int16_t MFC_Current_old)
{
    int16_t MFC_Current_new = 0;

  #if __HEV
    if(lcarbu1ActiveFlag==1)
    {
        MFC_Current_new = TC_CURRENT_MAX;
    }
    else
    {
        MFC_Current_new = 0;
    }
  #endif

    return MFC_Current_new;
}


int16_t     LAMFC_s16SetMFCCurrentHRB(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
    int16_t     MFC_Current_new = 0;

  #if __HRB
   #if __SPLIT_TYPE == 0
    if((CIRCUIT==U16_SECONDARY_CIRCUIT) || (CIRCUIT==U16_PRIMARY_CIRCUIT))
    {
        if(lchrbu1ActiveFlag==1)
        {
            if(lchrbs16AssistPressure >= MPRESS_45BAR)
            {
                MFC_Current_new = ((lchrbs16AssistPressure*TC_I_4mA)/MPRESS_1BAR)+(TC_I_400mA + TC_I_20mA);
            }
            else if(lchrbs16AssistPressure >= MPRESS_10BAR)
            {
                MFC_Current_new = ((lchrbs16AssistPressure*TC_I_5mA)/MPRESS_1BAR)+(TC_I_375mA);
            }
            else { }
        }
    }
   #else
    if(CIRCUIT==U16_SECONDARY_CIRCUIT)
    {
        if(lchrbu1ActiveFlag==1)
        {
            if(lchrbs16AssistPressure >= MPRESS_45BAR)
            {
                MFC_Current_new = ((lchrbs16AssistPressure*TC_I_4mA)/MPRESS_1BAR)+(TC_I_400mA + TC_I_20mA);
            }
            else if(lchrbs16AssistPressure >= MPRESS_10BAR)
            {
                MFC_Current_new = ((lchrbs16AssistPressure*TC_I_5mA)/MPRESS_1BAR)+(TC_I_375mA);
            }
            else { }
        }
    }
   #endif
  #endif

    return MFC_Current_new;
}


void    LAMFC_vArrangeMFCCurrent(uint8_t CIRCUIT, int16_t MFC_Current_ESP, int16_t MFC_Current_ACC, int16_t MFC_Current_HDC, int16_t MFC_Current_HSA, int16_t MFC_Current_HBB)
{
    int16_t MFC_Current_Final = 0;
    uint8_t NEED_TC_FORCED_HOLD = 0;

    if(MFC_Current_ESP>0) {
        MFC_Current_Final = MFC_Current_ESP;
    }
    else {
        if((MFC_Current_ACC > 0) && (MFC_Current_HDC > 0)) {
            if(MFC_Current_ACC>=MFC_Current_HDC) {
                MFC_Current_Final = MFC_Current_ACC;
            }
            else {
                MFC_Current_Final = MFC_Current_HDC;
            }
            NEED_TC_FORCED_HOLD = 1;
        }
        else if(MFC_Current_ACC>0)
        {        
            if(MFC_Current_HSA>0)
            {
                if(MFC_Current_ACC>=MFC_Current_HSA) 
                {
                    MFC_Current_Final = MFC_Current_ACC;
                }
                else 
                {
                    MFC_Current_Final = MFC_Current_HSA;
                }
            }
            else 
            {
                 MFC_Current_Final = MFC_Current_ACC;
            }
              #if __DEC
            if(lcu1DecActiveFlg==1)
            {
            	NEED_TC_FORCED_HOLD = 1;
            }
            else{ }	
              #endif            
        }        
        else if(MFC_Current_HDC>0) {
            MFC_Current_Final = MFC_Current_HDC;
            NEED_TC_FORCED_HOLD = 1;
        }
        else if(MFC_Current_HSA>0) {
            MFC_Current_Final = MFC_Current_HSA;
        }
        else if(MFC_Current_HBB>0) {
            MFC_Current_Final = MFC_Current_HBB;
        }
        else {
            MFC_Current_Final = 0;
        }

        if(NEED_TC_FORCED_HOLD==1)
        {
            LAMFC_vCheckTCForcedHold(CIRCUIT, MFC_Current_Final);
        }
        else
        {
            TCMF_LONG_HOLD_flag_s = 0;
            TCMF_LONG_HOLD_flag_p = 0;
        }
    }
 #if __VDC
    if(mpress>=MPRESS_5BAR) {
        tempW7 = 400 + (((MPRESS_150BAR + MPRESS_100BAR - mpress)/10)*6) ;
        if(MFC_Current_Final > tempW7) {MFC_Current_Final = tempW7;}
        else { }
    }
    else { }
#endif
    if(CIRCUIT==U16_SECONDARY_CIRCUIT) {
        TC_MFC_Voltage_Secondary = (uint16_t)MFC_Current_Final;
        if(TCMF_LONG_HOLD_flag_s==0) {TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary;}
        else {
            if(TC_MFC_Voltage_Secondary>=900) {TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+300;}
            else if(TC_MFC_Voltage_Secondary>=700) {TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+250;}
            else if(TC_MFC_Voltage_Secondary>=500) {TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+150;}
            else {TC_MFC_Voltage_Secondary_dash = TC_MFC_Voltage_Secondary+100;}
        }
      #if __DEC
        if ((lcudecforcedMFCHoldFlg_s==1)&&(TC_MFC_Voltage_Secondary_dash < 900)) {TC_MFC_Voltage_Secondary_dash = 900;}
        else { }
      #endif
    }
    else if(CIRCUIT==U16_PRIMARY_CIRCUIT) {
        TC_MFC_Voltage_Primary = (uint16_t)MFC_Current_Final;
        if(TCMF_LONG_HOLD_flag_p==0) {TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary;}
        else {
            if(TC_MFC_Voltage_Primary>=900) {TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+300;}
            else if(TC_MFC_Voltage_Primary>=700) {TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+250;}
            else if(TC_MFC_Voltage_Primary>=500) {TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+150;}
            else {TC_MFC_Voltage_Primary_dash = TC_MFC_Voltage_Primary+100;}
        }
      #if __DEC
        if ((lcudecforcedMFCHoldFlg_p==1)&&(TC_MFC_Voltage_Primary_dash < 900)) {TC_MFC_Voltage_Primary_dash = 900;}
        else { }
      #endif
    }
    else { }
}


void    LAMFC_vModifyValveCommand(struct W_STRUCT *pWL1, struct W_STRUCT *pWL2)
{
#if __VDC
  #if (__SPLIT_TYPE==0)

    if((TCMF_MOTOR_OFF_flag==0) && (YAW_CDC_WORK==0)) {
        if(pWL1->LEFT_WHEEL==1) {
            if(vref>VREF_2_5_KPH) {MFC_ESV_Secondary_ACT = 1;}
            else { }
        }
        else {
            if(vref>VREF_2_5_KPH) {MFC_ESV_Primary_ACT = 1;}
            else { }
        }
    }
    else { }

    if(pWL1->ABS==0) {
        pWL1->HV_VL = 0; pWL1->AV_VL = 0;
    }
    else { }
    if((pWL2->ABS==0)&&(EBD_RA==0)) {
        pWL2->HV_VL = 0; pWL2->AV_VL = 0;
    }
    else { }

  #else

    if((TCMF_MOTOR_OFF_flag==0) && (YAW_CDC_WORK==0)) {
        if(pWL1->REAR_WHEEL==1) {
            if(vref>VREF_2_5_KPH) {MFC_ESV_Secondary_ACT = 1;}
            else { }
        }
        else {
            if(vref>VREF_2_5_KPH) {MFC_ESV_Primary_ACT = 1;}
            else { }
        }
    }
    else { }

    if(pWL1->REAR_WHEEL==0) {
        if(pWL1->ABS==0) {
            pWL1->HV_VL = 0; pWL1->AV_VL = 0;
        }
        else { }
        if(pWL2->ABS==0) {
            pWL2->HV_VL = 0; pWL2->AV_VL = 0;
        }
        else { }
    }
    else {
        if((pWL1->ABS==0)&&(EBD_RA==0)) {
            pWL1->HV_VL = 0; pWL1->AV_VL = 0;
        }
        else { }
        if((pWL2->ABS==0)&&(EBD_RA==0)) {
            pWL2->HV_VL = 0; pWL2->AV_VL = 0;
        }
        else { }
    }
  #endif
#endif
}

void    LAMFC_vCheckTCForcedHold(uint8_t CIRCUIT, int16_t MFC_Current_new)
{
    if(CIRCUIT==U16_SECONDARY_CIRCUIT)
    {
        if(MFC_Current_new < (int16_t)TC_MFC_Voltage_Secondary)
        {
          #if __HDC
            if(lcu1HdcActiveFlg==1) {TCMF_dump_end_timer_s = L_U8_TIME_10MSLOOP_150MS;}
            else {TCMF_dump_end_timer_s = L_U8_TIME_10MSLOOP_70MS;}
          #else
            TCMF_dump_end_timer_s = L_U8_TIME_10MSLOOP_70MS;
          #endif
        }
        else if(TCMF_dump_end_timer_s>0) {TCMF_dump_end_timer_s--;}
        else { }

        if(MFC_Current_new == (int16_t)TC_MFC_Voltage_Secondary)
        {
            if((TCMF_MOTOR_OFF_flag==1) && (TCMF_dump_end_timer_s==0))
        	{
        		  #if (__HDC && __TARGET_SPD_ADAPTATION)
        		if(lcu1HdcActiveFlg==1)
        		{
        			if((lcu1HdcNoForcedHoldAtBrk==0)&&(lcu1hdcEngStallSuspect==0)&&(lcu8HdcDriverAccelIntend==0)) 
        			{
        				TCMF_LONG_HOLD_flag_s = 1;
        			}
        			else {TCMF_LONG_HOLD_flag_s = 0;}
        		}
        		else {TCMF_LONG_HOLD_flag_s = 1;}
        		  #else
        		TCMF_LONG_HOLD_flag_s = 1;
        		  #endif
        	}
        	else {TCMF_LONG_HOLD_flag_s = 0;}
        }
        else {TCMF_LONG_HOLD_flag_s = 0;}
    }
    else if(CIRCUIT==U16_PRIMARY_CIRCUIT)
    {
        if(MFC_Current_new < (int16_t)TC_MFC_Voltage_Primary)
        {
          #if __HDC
            if(lcu1HdcActiveFlg==1) {TCMF_dump_end_timer_p = L_U8_TIME_10MSLOOP_150MS;}
            else {TCMF_dump_end_timer_p = L_U8_TIME_10MSLOOP_70MS;}
          #else
            TCMF_dump_end_timer_p = L_U8_TIME_10MSLOOP_70MS;
          #endif
        }
        else if(TCMF_dump_end_timer_p>0) {TCMF_dump_end_timer_p--;}
        else { }

        if(MFC_Current_new == (int16_t)TC_MFC_Voltage_Primary)
        {
            if((TCMF_MOTOR_OFF_flag==1) && (TCMF_dump_end_timer_p==0))
        	{
        		  #if (__HDC && __TARGET_SPD_ADAPTATION)
        		if(lcu1HdcActiveFlg==1)
        		{
        			if((lcu1HdcNoForcedHoldAtBrk==0)&&(lcu1hdcEngStallSuspect==0)&&(lcu8HdcDriverAccelIntend==0)) 
        			{
        				TCMF_LONG_HOLD_flag_p = 1;
        			}
        			else {TCMF_LONG_HOLD_flag_p = 0;}
        		}
        		else {TCMF_LONG_HOLD_flag_p = 1;}
        		  #else
        		TCMF_LONG_HOLD_flag_p = 1;
        		  #endif
        	}
        	else {TCMF_LONG_HOLD_flag_p = 0;}
        }
        else {TCMF_LONG_HOLD_flag_p = 0;}
    }
    else { }
}

void    LAMFC_vLoadCircuitParameters(void)
{
#if __VDC
  #if (__SPLIT_TYPE==0)

    if((TCL_DEMAND_fr==1) || (TCL_DEMAND_rl==1)) {MFC_TCNO_Primary_ACT=1;}
    else {MFC_TCNO_Primary_ACT=0;}
    if((TCL_DEMAND_fl==1) || (TCL_DEMAND_rr==1)) {MFC_TCNO_Secondary_ACT=1;}
    else {MFC_TCNO_Secondary_ACT=0;}

    if(S_VALVE_RIGHT==1) {MFC_ESV_Primary_ACT=1;}
    else {MFC_ESV_Primary_ACT=0;}
    if(S_VALVE_LEFT==1) {MFC_ESV_Secondary_ACT=1;}
    else {MFC_ESV_Secondary_ACT=0;}

    if((ESP_BRAKE_CONTROL_FR==1) 
    || (ESP_BRAKE_CONTROL_RL==1)
    ) {MFC_ESP_Primary_CTRL=1;}
    else {MFC_ESP_Primary_CTRL=0;}
    if((ESP_BRAKE_CONTROL_FL==1) 
    || (ESP_BRAKE_CONTROL_RR==1)
    ) {MFC_ESP_Secondary_CTRL=1;}
    else {MFC_ESP_Secondary_CTRL=0;}

    if((BTCS_fr==1) || (BTCS_rl==1)) {MFC_TCS_Primary_CTRL=1;}
    else {MFC_TCS_Primary_CTRL=0;}
    if((BTCS_fl==1) || (BTCS_rr==1)) {MFC_TCS_Secondary_CTRL=1;}
    else {MFC_TCS_Secondary_CTRL=0;}

  #else

    if(TCL_DEMAND_PRIMARY==1) {MFC_TCNO_Primary_ACT=1;}
    else {MFC_TCNO_Primary_ACT=0;}
    if(TCL_DEMAND_SECONDARY==1) {MFC_TCNO_Secondary_ACT=1;}
    else {MFC_TCNO_Secondary_ACT=0;}

    if(S_VALVE_PRIMARY==1) {MFC_ESV_Primary_ACT=1;}
    else {MFC_ESV_Primary_ACT=0;}
    if(S_VALVE_SECONDARY==1) {MFC_ESV_Secondary_ACT=1;}
    else {MFC_ESV_Secondary_ACT=0;}

    if((ESP_BRAKE_CONTROL_FR==1) || (ESP_BRAKE_CONTROL_FL==1)) {MFC_ESP_Primary_CTRL=1;}
    else {MFC_ESP_Primary_CTRL=0;}
    if((ESP_BRAKE_CONTROL_RL==1) || (ESP_BRAKE_CONTROL_RR==1)) {MFC_ESP_Secondary_CTRL=1;}
    else {MFC_ESP_Secondary_CTRL=0;}

    if((BTCS_fr==1) || (BTCS_fl==1)) {MFC_TCS_Primary_CTRL=1;}
    else {MFC_TCS_Primary_CTRL=0;}
    if((BTCS_rl==1) || (BTCS_rr==1)) {MFC_TCS_Secondary_CTRL=1;}
    else {MFC_TCS_Secondary_CTRL=0;}

  #endif

#elif __HEV
    if((TCL_DEMAND_fr==1) || (TCL_DEMAND_rl==1)) {MFC_TCNO_Primary_ACT=1;}
    else {MFC_TCNO_Primary_ACT=0;}
    if((TCL_DEMAND_fl==1) || (TCL_DEMAND_rr==1)) {MFC_TCNO_Secondary_ACT=1;}
    else {MFC_TCNO_Secondary_ACT=0;}

    MFC_ESV_Primary_ACT=0;
    MFC_ESV_Secondary_ACT=0;
#endif

}

void    LAMFC_vSetCircuitParameters(void)
{
#if __VDC
  #if (__SPLIT_TYPE==0)

    if(MFC_TCNO_Primary_ACT==1) {TCL_DEMAND_fr=1;}
    if(MFC_TCNO_Secondary_ACT==1) {TCL_DEMAND_fl=1;}

    if(MFC_ESV_Primary_ACT==1) {S_VALVE_RIGHT=1;}
    if(MFC_ESV_Secondary_ACT==1) {S_VALVE_LEFT=1;}

  #else

    if(MFC_TCNO_Primary_ACT==1) {TCL_DEMAND_PRIMARY=1;}
    if(MFC_TCNO_Secondary_ACT==1) {TCL_DEMAND_SECONDARY=1;}

    if(MFC_ESV_Primary_ACT==1) {S_VALVE_PRIMARY=1;}
    if(MFC_ESV_Secondary_ACT==1) {S_VALVE_SECONDARY=1;}

  #endif
#elif __HEV
    if(MFC_TCNO_Primary_ACT==1) {TCL_DEMAND_fr=1;}
    if(MFC_TCNO_Secondary_ACT==1) {TCL_DEMAND_fl=1;}
#endif
}


  #if __MFC_L9352B
void    LAMFC_u8EstMFCTargetPress(int16_t MFC_Current)
{
    if(MFC_Current > 0)
    {
        if(MFC_Current > TC_CURRENT_MAX)
        {
            pTC->MFC_Target_Press = MFC_PRESS_160BAR;
            MFC_Current = TC_CURRENT_MAX;
        }
        else if(MFC_Current > TC_CURRENT_MIN)
        {
            pTC->MFC_Target_Press = (uint8_t)((MFC_Current-TC_CURRENT_MIN)/(TC_CURRENT_MAX-TC_CURRENT_MIN))*MFC_PRESS_160BAR;
        }
        else
        {
            pTC->MFC_Target_Press = MFC_PRESS_0BAR;
        }

 		pTC->MFC_PWM_DUTY_temp = MFC_Current/10;

        if (pTC->MFC_Duty_Dither_cnt >=(TC_DITHER_PERIOD-1))
        {
            pTC->MFC_Duty_Dither_cnt = 0;
        }
        else
        {
            pTC->MFC_Duty_Dither_cnt = pTC->MFC_Duty_Dither_cnt + 1;
        }
    }
    else
    {
        pTC->MFC_Target_Press = 0;
        pTC->MFC_PWM_DUTY_temp = 0;
        pTC->MFC_Duty_Dither_cnt = 0;
    }
}


uint8_t   LAMFC_u8GenerateMFCDuty(MFC_DUTY_t *pTC_temp, int16_t MFC_Current)
{
    uint8_t MFC_DUTY, MFC_dither_duty_temp;
    pTC = pTC_temp;

    pTC->MFC_PWM_DUTY_temp_old = pTC->MFC_PWM_DUTY_temp;

    LAMFC_u8EstMFCTargetPress(MFC_Current);

  #if __TC_ORIFICE_0P9  /* HM */
    if(pTC->MFC_PWM_DUTY_temp > 72)
    {
        MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
    }
    else if(pTC->MFC_PWM_DUTY_temp > 62)
    {
        MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY1 + (((int16_t)(pTC->MFC_PWM_DUTY_temp-62)*(MFC_DITHER_DUTY2-MFC_DITHER_DUTY1))/(72-62)));

        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
        else {MFC_DUTY = pTC->MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
    }
    else
    {
        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_DITHER_DUTY1;}
        else {MFC_DUTY = pTC->MFC_PWM_DUTY_temp - MFC_DITHER_DUTY1;}

        if(pTC->MFC_PWM_DUTY_temp==0) {MFC_DUTY = 0;}
    }
  #elif __MGH80_ESC_DV2  
    if(pTC->MFC_PWM_DUTY_temp > 55)
    {
        MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
    }
    else if(pTC->MFC_PWM_DUTY_temp > 20)
    {
        MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY1 + (((int16_t)(pTC->MFC_PWM_DUTY_temp-20)*(MFC_DITHER_DUTY2-MFC_DITHER_DUTY1))/(55-20)));

        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
        else 
        {
        	if(pTC->MFC_PWM_DUTY_temp<=MFC_dither_duty_temp) {MFC_DUTY = 1;}
        	else                                             {MFC_DUTY = pTC->MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
        }
    }
    else
    {
        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_DITHER_DUTY1;}
        else
        {
        	MFC_DUTY = 1;
        }

        if(pTC->MFC_PWM_DUTY_temp==0) {MFC_DUTY = 0;}
    }             
  #else 
    if(pTC->MFC_PWM_DUTY_temp > 70)
    {
        MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
    }
    else if(pTC->MFC_PWM_DUTY_temp > 60)
    {
        MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY1 + (((int16_t)(pTC->MFC_PWM_DUTY_temp-60)*(MFC_DITHER_DUTY2-MFC_DITHER_DUTY1))/(70-60)));

        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
        else {MFC_DUTY = pTC->MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
    }
    else
    {
        if((pTC->MFC_Duty_Dither_cnt%TC_DITHER_PERIOD)<TC_DITHER_PERIOD_HALF) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp + MFC_DITHER_DUTY1;}
        else {MFC_DUTY = pTC->MFC_PWM_DUTY_temp - MFC_DITHER_DUTY1;}

        if(pTC->MFC_PWM_DUTY_temp==0) {MFC_DUTY = 0;}
    }
  #endif

    #if __HSA
    if(HSA_flg==1) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif

    #if (__EPB_INTERFACE)&&(__DISABLE_DITHER_EPBI)
    if(lcu1EpbiDitherDisable==1)
    {
    	MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
    }
    #endif

    #if (__HDC) && (__TARGET_SPD_ADAPTATION) /* prohibit v/v noise at MP off on HDC act */
      if((lcu1HdcActiveFlg==1)&&((lcu1HdcNoForcedHoldAtBrk==1)||(MPRESS_BRAKE_ON_off_cnt>L_U16_TIME_10MSLOOP_4_5S)))
      {
      	MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
      }
    #endif

    #if __AVH
    if((lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1)) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif
    
    #if __SPAS_INTERFACE       
    if(lcu1SpasActiveFlg==1) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif

    #if (__SCC) && (__SCC_DITHER_ENABLE == 0)
    if(lcu8WpcActiveFlg==1) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif

    #if __LVBA
    if((lclvbau1ActiveFlg==1)&&(target_vol<=MSC_0_V)) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif

    #if __HBB
    if((TCMF_LowVacuumBoost_flag==1)&&(target_vol<=MSC_0_V)) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}

    if((TCMF_LowVacuumBoost_flag==0)
      #if __HDC
      &&(lcu1HdcActiveFlg==0)
      #endif
    ) {MFC_DUTY = pTC->MFC_PWM_DUTY_temp;}
    #endif
    
    #if (__TCMF_DITHER_DISABLE == 1)
      MFC_DUTY = pTC->MFC_PWM_DUTY_temp;
    #endif

    return MFC_DUTY;
}


uint8_t   LAMFC_u8CompESVDuty(uint8_t ESV_Primary_ACT, uint8_t ESV_Secondary_ACT, uint8_t ESV_DUTY)
{
    uint8_t   ESV_DUTY_COMP, ESV_DUTY_COMP_P, ESV_DUTY_COMP_S;

    if(ESV_Primary_ACT==0) {ESV_DUTY_COMP_P = 0;}
    else
    {
      #if ((__AHB_GEN3_SYSTEM==DISABLE) && (__IDB_LOGIC==DISABLE))
        ESV_DUTY_COMP_P = (uint8_t)(((((((uint32_t)VOLT_RESISTER_CONST*Lfc_ps_nc_Lmon)/(1024-Lfc_ps_nc_Hmon))*128)/(uint32_t)Filtered_ref_mon_ad)*(uint32_t)ESV_DUTY)/128);
/*      ESV_DUTY_COMP_P = (uint8_t)(((uint32_t)ESV_DUTY*714)/(uint32_t)Filtered_ref_mon_ad); */
      #else
        ESV_DUTY_COMP_P = ESV_DUTY;
      #endif
        if(ESV_DUTY_COMP_P>=100) {ESV_DUTY_COMP_P = 100;}
        else if(ESV_DUTY_COMP_P<ESV_DUTY) {ESV_DUTY_COMP_P = ESV_DUTY;}
        else { }
    }

    if(ESV_Secondary_ACT==0) {ESV_DUTY_COMP_S = 0;}
    else
    {
      #if ((__AHB_GEN3_SYSTEM==DISABLE) && (__IDB_LOGIC==DISABLE))
        ESV_DUTY_COMP_S = (uint8_t)(((((((uint32_t)VOLT_RESISTER_CONST*Lfc_ss_nc_Lmon)/(1024-Lfc_ss_nc_Hmon))*128)/(uint32_t)Filtered_ref_mon_ad)*(uint32_t)ESV_DUTY)/128);
/*      ESV_DUTY_COMP_S = (uint8_t)(((uint32_t)ESV_DUTY*714)/(uint32_t)Filtered_ref_mon_ad); */
      #else
        ESV_DUTY_COMP_S = ESV_DUTY;
      #endif
        if(ESV_DUTY_COMP_S>=100) {ESV_DUTY_COMP_S = 100;}
        else if(ESV_DUTY_COMP_S<ESV_DUTY) {ESV_DUTY_COMP_S = ESV_DUTY;}
        else { }
    }

    if(ESV_DUTY_COMP_P > ESV_DUTY_COMP_S) {ESV_DUTY_COMP = ESV_DUTY_COMP_P;}
    else {ESV_DUTY_COMP = ESV_DUTY_COMP_S;}

    return ESV_DUTY_COMP;
}


void    LAMFC_vCalcMFCDuty(void)
{
   #if __ESPLUS_OLD_HU
    if(TC_MFC_Voltage_Secondary_dash > 2250) {MFC_PWM_DUTY_S=225;}
    else {MFC_PWM_DUTY_S = (uint8_t)(TC_MFC_Voltage_Secondary_dash/10);}

    if(TC_MFC_Voltage_Primary_dash > 2250) {MFC_PWM_DUTY_P = 225;}
    else {MFC_PWM_DUTY_P = (uint8_t)(TC_MFC_Voltage_Primary_dash/10);}
   #else


    MFC_PWM_DUTY_S = LAMFC_u8GenerateMFCDuty(&la_StcMfc, (int16_t)TC_MFC_Voltage_Secondary_dash);
    MFC_PWM_DUTY_P = LAMFC_u8GenerateMFCDuty(&la_PtcMfc, (int16_t)TC_MFC_Voltage_Primary_dash);

   #endif

   #if __VDC
    if((MFC_ESV_Secondary_ACT==1) || (MFC_ESV_Primary_ACT==1))
    {
    	if((MFC_ESV_Primary_ACT==1)&&(lau8_S_VALVE_PRIMARY_in_cnt==0))
    	{
    		if(lau16HcEsvActTimerPri > U16_ESV_DRIVE_CYCLE) {lau16HcEsvActTimerPri = 1;}
    		else 										    {lau16HcEsvActTimerPri = lau16HcEsvActTimerPri + 1;}
    	}
    	else {lau16HcEsvActTimerPri = 0;}

		if((MFC_ESV_Secondary_ACT==1)&&(lau8_S_VALVE_PRIMARY_in_cnt==0))
    	{
    		if(lau16HcEsvActTimerSec > U16_ESV_DRIVE_CYCLE) {lau16HcEsvActTimerSec = 1;}
    		else 											{lau16HcEsvActTimerSec = lau16HcEsvActTimerSec + 1;}
    	}
    	else {lau16HcEsvActTimerSec = 0;}

        if (lau8_S_VALVE_PRIMARY_in_cnt!=0)
        {
            MFC_PWM_DUTY_ESV_temp  = (uint8_t)LCABS_s16Interpolation2P((int16_t)lau8_S_VALVE_PRIMARY_in_cnt,0,ESV_FADE_CTRL_IN_TIME,ESV_FADE_CTRL_INI_DUTY,ESV_DRIVE_DUTY_MIN);
        }
        else
        {
            MFC_PWM_DUTY_ESV_temp  = U8_ESV_DRIVE_DUTY_MAX;/*100;*/
        }

        if((mpress<MPRESS_2BAR) && (mp_hw_suspcs_flg==0))
        {
            if((MFC_ESP_Secondary_CTRL==1) || (MFC_ESP_Primary_CTRL==1)
                || (EPC_ON==1) || (PBA_ON==1)
              #if __FBC
                || (FBC_ON==1)
              #endif
              #if __HOB
                || (lcu1HobOnFlag==1)
              #endif
/*08SWD-10*/
#if __Decel_Ctrl_Integ == 0
              #if __TSP
                || (TSP_ON==1)
              #endif
#endif
              #if __BDW
                || (BDW_ON==1)
              #endif

              #if __EBP
                || (EBP_ON==1)
              #endif
            )
            {
                MFC_PWM_DUTY_ESV = MFC_PWM_DUTY_ESV_temp;
				MFC_PWM_DUTY_ESV_P = MFC_PWM_DUTY_ESV_temp;
				MFC_PWM_DUTY_ESV_S = MFC_PWM_DUTY_ESV_temp;
            }
          #if __SCC
            else if((lcu8WpcActiveFlg==1)||(lcu1WpcBrkReadyCtrl==1)) 
            {
            	MFC_PWM_DUTY_ESV = lcu8WpcESVduty;
            	MFC_PWM_DUTY_ESV_P = lcu8WpcESVduty;
				MFC_PWM_DUTY_ESV_S = lcu8WpcESVduty;
            }
          #endif
            else if((0)
                || (MFC_TCS_Secondary_CTRL==1) || (MFC_TCS_Primary_CTRL==1) /* TCS */
#if __Decel_Ctrl_Integ
              #if __DEC
                || (lcu1DecActiveFlg==1)
              #endif
#else
              #if __ACC
                || (lcu1AccActiveFlg==1)
              #endif
              #if __EPB_INTERFACE
                || (lcu1EpbActiveFlg==1)
              #endif
#endif
/*           #if __SCC
                || (lcu8WpcActiveFlg==1)
              #endif */
              #if __HDC
                || (lcu1HdcActiveFlg==1)
              #endif  
              #if __HBB
                || (TCMF_LowVacuumBoost_flag==1)
              #endif
              #if __LVBA
                || (lclvbau1ActiveFlg==1)
              #endif
			  #if __TVBB
                ||(lctvbbu1TVBB_ON==1)
              #endif
            )
            {
                MFC_PWM_DUTY_ESV = ESV_DRIVE_DUTY_MIN;
                if(lau16HcEsvActTimerPri<1)
                {
                	MFC_PWM_DUTY_ESV_P = 0;
                }
                else if(lau16HcEsvActTimerPri<=(uint16_t)U8_ESV_DRIVE_MAX_DUTY_TIME) 
                {
                	MFC_PWM_DUTY_ESV_P = U8_ESV_DRIVE_DUTY_MAX;
                }
                else											  		        
                {
                	MFC_PWM_DUTY_ESV_P = U8_ESV_DRIVE_MIN_OPEN_DUTY;
                }
                
                if(lau16HcEsvActTimerSec<1) 
                {
                	MFC_PWM_DUTY_ESV_S = 0;
                }
                else if(lau16HcEsvActTimerSec<=(uint16_t)U8_ESV_DRIVE_MAX_DUTY_TIME) 
                {
                	MFC_PWM_DUTY_ESV_S = U8_ESV_DRIVE_DUTY_MAX;
                }
                else
                {
                	MFC_PWM_DUTY_ESV_S = U8_ESV_DRIVE_MIN_OPEN_DUTY;
                }
            }
            else
            {
                MFC_PWM_DUTY_ESV = MFC_PWM_DUTY_ESV_temp;
                MFC_PWM_DUTY_ESV_P = MFC_PWM_DUTY_ESV_temp;
				MFC_PWM_DUTY_ESV_S = MFC_PWM_DUTY_ESV_temp;
            }
        }
        else
        {
            MFC_PWM_DUTY_ESV = MFC_PWM_DUTY_ESV_temp;
            MFC_PWM_DUTY_ESV_P = MFC_PWM_DUTY_ESV_temp;
			MFC_PWM_DUTY_ESV_S = MFC_PWM_DUTY_ESV_temp;
        }
    }
    else 
    {
    	MFC_PWM_DUTY_ESV = 0;
    	MFC_PWM_DUTY_ESV_P = 0;
		MFC_PWM_DUTY_ESV_S = 0;
		lau16HcEsvActTimerPri = 0;
		lau16HcEsvActTimerSec = 0;
    }
   #endif
}

  #endif
 #endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_Hardware_Control
	#include "Mdyn_autosar.h"               
#endif

