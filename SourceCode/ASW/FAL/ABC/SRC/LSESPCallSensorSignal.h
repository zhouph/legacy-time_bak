#ifndef __LSESPCALLSENSORSIGNAL_H__
#define __LSESPCALLSENSORSIGNAL_H__

/*includes********************************************************************/
#include "LVarHead.h"




#if __VDC


/*Global Extern Variable  Declaration*****************************************/
#if __CAR == GM_GSUV  
extern int16_t S16_HM_EMS;
extern int16_t S16_LM_EMS;
extern int16_t S16_MM_EMS;
#endif

/*Global Extern Functions  Declaration****************************************/
extern void LSESP_vCallSensorSignal(void);
/*MISRA, H22*/

#endif /* __VDC */

#endif