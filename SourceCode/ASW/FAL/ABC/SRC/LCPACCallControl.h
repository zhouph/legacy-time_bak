#ifndef __LCPACCALLCONTROL_H__
#define __LCPACCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __PAC
/*Global Type Declaration ****************************************************/

/* for PAC Debug */
#define __PAC_DEBUG 
#define	__PAC_REAPPLY_TIME_TUNING		ENABLE
/* for PAC Debug */

#define S8_PAC_READY		         	0
#define S8_PAC_ACTUATION          		1
#define S8_PAC_FADEOUT         			2
#define S8_PAC_INHIBIT          		3

#define PAC_FADEOUT_OFF		         	0
#define PAC_FADEOUT_MAX_RISE          	1
#define PAC_FADEOUT_MAX_ALAT         	2

#define	S16_PAC_ENTER_MP_TH_SLOW		MPRESS_60BAR
#define	S16_PAC_ENTER_MP_TH_FAST		MPRESS_100BAR

#define	U8_PAC_REAPPLY_CNT_REF_PH3		6
#define	U8_PAC_REAPPLY_CNT_REF_PH2		3

#define U8_DV_BASE_LOW_FOR_PAC			VREF_1_KPH

#define lcpacu1PACEnableFlg			        PACC0.bit0
#define lcpacu1PacActiveFlg 	            PACC0.bit1
#define lcpacu1FadeOutFlg         	        PACC0.bit2
#define lcpacu1FadeOutEndFlg                PACC0.bit3
#define lcpacu1InhibitFlg          		    PACC0.bit4
#define lcpacu1DctPacMpressThres            PACC0.bit5
#define lcpacu1PACExitFlg	                PACC0.bit6
#define PAC_not_used_bit_0_7                PACC0.bit7

#define PAC_not_used_bit_1_0		        PACC1.bit0
#define PAC_not_used_bit_1_1		        PACC1.bit1
#define PAC_not_used_bit_1_2		        PACC1.bit2
#define PAC_not_used_bit_1_3		        PACC1.bit3
#define PAC_not_used_bit_1_4                PACC1.bit4
#define PAC_not_used_bit_1_5                PACC1.bit5
#define PAC_not_used_bit_1_6                PACC1.bit6
#define PAC_not_used_bit_1_7                PACC1.bit7

typedef struct {
	
	uint8_t	u8ConvenReapplyTime;
	uint8_t	u8HoldCounterRef;
	uint8_t u8RiseHoldCnt;
	uint8_t u8ReapplyCounter;
  #if defined(__PAC_DEBUG)
    uint8_t  u8Debugger;
  #endif  	
	
	uint16_t unused00			:1;
	uint16_t unused01			:1;
	uint16_t unused02			:1;
	uint16_t unused03			:1;
	uint16_t unused04			:1;
	uint16_t unused05			:1;
	uint16_t unused06			:1;
	uint16_t unused07			:1;	
} PAC_WHL_CTRL_t;


/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t PACC0,PACC1;
extern PAC_WHL_CTRL_t LCPACFL, LCPACFR;

extern uint8_t 	lcpacu8PacControlState;
extern uint16_t	lcpacuu16MpFaseRiseIndex;  
extern uint8_t	lcpacu8FadeOutMode;    

/*Global Extern Functions  Declaration****************************************/

extern void LCPAC_vCallControl(void);
extern void LCPAC_vResetControl(void);


  #endif /* #ifndef __LCPACCALLCONTROL_H__ */
#endif	/* #if __PAC */





