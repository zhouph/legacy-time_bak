
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAESPCallActHW
	#include "Mdyn_autosar.h"               
#endif

/************************************************************
*LAESPCallActHW.h must move
************************************************************/
#include "Hardware_Control.h"
#include "LADECCallActHW.h"
#include "LCTCSCallControl.h"
#include "LCDECCallControl.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCHSACallControl.h"
#include "LCESPCallPressureControl.h"
#include "LDESPDetectVehicleStatus.h"
#include "LCESPCallControl.h"
#include "LAESPActuateNcNovalveF.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"
#include "LDRTACallDetection.h"
#include "LAESPCallActHW.h"
#include "LACallMain.h"
#include "LDROPCallDetection.h"
#include "LCESPCalInterpolation.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LCTCSCallBrakeControl.h"
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
#include "LATCSCallActHW.h"
#endif

#if (__ESC_TCMF_CONTROL ==1)
#include "LCESPInterfaceTCMF.h"
#include "LAESPActuateTCMFESC.h"
#endif


/************************************************************/

/************************************************************/

#if __FBC
extern U8_BIT_STRUCT_t FBCF00;
#define FBC_ON                          FBCF00.bit2
#endif

#if __HSA
extern    U8_BIT_STRUCT_t HSAF0;
#endif
#if __BDW
#include "LCBDWCallControl.h"
extern    U8_BIT_STRUCT_t BDWC1;
#endif

#if __TVBB
#include "LCTVBBCallControl.h"
#endif


int16_t esp_ebd_comb_count;
int16_t S_VALVE_PRIMARY_TEMP_cnt;
uint8_t lau8_S_VALVE_PRIMARY_in_cnt;
int16_t lcesps16OS2TarVol;
int16_t lcesps16EscCbsCount;

  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
int16_t laesps16PriTcTargetPress, laesps16SecTcTargetPress;
  #endif /* __AHB_GEN3_SYSTEM == ENABLE */

int16_t lcesps16AHB_FadeOutCount;
int16_t lcesps16AHB_CurTargetPress;
int16_t lcesps16AHB_UscTargetPress;


/******************************************************/
#if __VDC
void LAESP_vCallActHW(void);
void LAESP_vActuateEsvAfterEsp(void);
void LAESP_vResetValve(void);
void LAESP_vTestValveMode(void);
#if (__MGH_80_10MS == ENABLE)
void LAVALVETEST_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *lavalvetestHP1, WHEEL_VV_OUTPUT_t *lavalvetestHP2);
  #if __VALVE_TEST_ESC_LFC
static void  LAESP_vDetectActiveRiseMode(struct W_STRUCT *WL_temp);
  #endif
#endif

void LAESP_vGenerateLFCDuty(void);
void LAESP_vGenerateOnOffDutyWL(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laescHP1, WHEEL_VV_OUTPUT_t *laescHP2);
void LAESP_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laescHP1, WHEEL_VV_OUTPUT_t *laescHP2);


void LCESP_vDetectEspCommand(void);
void LAESP_vResetVdc(void);

//extern void LAABS_vGenerateLFCDuty(void);
//extern void LCPBA_vCallControl(void);
//extern void LCFBC_vCallControl(void);
//extern void LCFBC_vInitializeVariables(void);
//extern void LCPBA_vInitializeVariables(void);


#if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
extern int16_t motor_test_mode;
extern void LCMSC_vActValveCommand(void);
void LCMSC_vSetVDCTargetVoltage(void);
#endif
void LAESP_vCallCombination(void);
#if(__IDB_LOGIC==ENABLE)
void LAESP_vActuateOhterWheelOS(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL_temp,struct W_STRUCT *WL);
void LAESP_vActuateOhterWheelUS(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL_temp,struct W_STRUCT *WL);
void LAESP_vActuateAfterESCforIDB(void);
void LAESP_vSetCtrlMode(void);
void LAESP_vSetFinalTpRate(void);
//void LAESP_vSetRearCtrlMode(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);
#endif
void LAESP_vActuateCrossWheelOS(struct ESC_STRUCT *WL_ESC, struct ESC_STRUCT_COMB *WL_ESC_COMB, struct W_STRUCT *WL);
void LAESP_vActuateCrossWheelUS(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);

#if !__SPLIT_TYPE
void LAESP_vCallActXsplitEsvTc(void);
#else
void LAESP_vCallActFRsplitEsvTc(void);
#endif

//  #if(__ESC_TCMF_CONTROL == 1)
//void LAESP_vSetTCMFOSCMotorVoltage(void);
//  #endif

   #if ((__TCMF_UNDERSTEER_CONTROL == 1) || (__ESC_COMPETITIVE_CONTROL == 1))
int16_t LAMFC_s16SetMFCCurrentESP(uint8_t CIRCUIT, int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurrentESPRear(int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurrentESPUS2WL(int16_t MFC_Current_old);     
   #endif
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
static void LAESP_vTcTagetPress(void);
void LAESP_vTcTagetPressRear(void);
#endif /* __AHB_GEN3_SYSTEM == ENABLE */
   
extern uint8_t motor_pressure_need;

void LAESP_vCallActHW(void)
{
    LDESP_vDetectCombControl();

#if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F || __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
    LAESP_vTestValveMode();
#else

#if ESC_PULSE_UP_RISE
	LCESP_vCalEscRiseRate();
#endif
        #if(__IDB_LOGIC == ENABLE)
	U1WheelValveCntr = 0;
    U1WheelValveCntr |= (ABS_fz==1);    
    U1WheelValveCntr |= (MPRESS_BRAKE_ON==1);
//    U1WheelValveCntr |= (BTC_fz==1);
    U1WheelValveCntr |= (FL_ESC.U1StrWheelValveCnt==1);
    U1WheelValveCntr |= (FR_ESC.U1StrWheelValveCnt==1);
    U1WheelValveCntr |= (RL_ESC.U1StrWheelValveCnt==1);
    U1WheelValveCntr |= (RR_ESC.U1StrWheelValveCnt==1);   
//    U1WheelValveCntr |= (lcu1US2WHControlFlag==1);

    if(lcidbs16activebrakepress > MPRESS_0G5BAR)
    {
       U1WheelValveCntr = 1; 
    }
    else
    {
        ;   
    }
 	CROSS_SLIP_CONTROL_old_fl = CROSS_SLIP_CONTROL_fl;
	CROSS_SLIP_CONTROL_old_fr = CROSS_SLIP_CONTROL_fr;
	CROSS_SLIP_CONTROL_old_rl = CROSS_SLIP_CONTROL_rl;
	CROSS_SLIP_CONTROL_old_rr = CROSS_SLIP_CONTROL_rr;  
 
    CROSS_SLIP_CONTROL_fl = 0;
    CROSS_SLIP_CONTROL_fr = 0;
    CROSS_SLIP_CONTROL_rl = 0;
    CROSS_SLIP_CONTROL_rr = 0;

    FL.U1EscOtherWheelCnt = 0;
    FR.U1EscOtherWheelCnt = 0;
    RL.U1EscOtherWheelCnt = 0;
    RR.U1EscOtherWheelCnt = 0;

        #endif
        
  LCESP_vDetectLowSpeed();           /* ESP CONTROL Limit using Vehicle speed*/
  LCESP_vDetectWstrJump();

 #if (__CAR == GM_T300 )||(__CAR == GM_GSUV )||(__CAR == GM_M350)
    if(((ESP_ERROR_FLG==0) && (fu8EngineModeStep >= 2))||(ROP_REQ_ACT==1))
 #else
    if((ESP_ERROR_FLG==0)||(ROP_REQ_ACT==1))
 #endif     
    {
        CDC_WORK_OUT=0;

        LCESP_vDetectControlSignal();      /* ESP CONTROL SIGNAL*/
        
    #if __GM_FailM      
    
        if( (fu1ESCDisabledBySW==0)
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ABS_OK==1))     
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_OFF_SLEEP_MODE_CBS_OK==1))     
       		||((fu1ESCDisabledBySW==1)&&(lctcss16BrkCtrlMode!=S16_TCS_CTRL_MODE_DISABLE))/*(lctcsu1DrivelineProtectionOn==1))*/
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))                                
            ||((fu1ESCDisabledBySW==1)&&(RTA_FL.ldabsu1DetSpareTire==1))
            ||((fu1ESCDisabledBySW==1)&&(RTA_FR.ldabsu1DetSpareTire==1))     
            ||((fu1ESCDisabledBySW==1)&&(RTA_RL.ldabsu1DetSpareTire==1))    
            ||((fu1ESCDisabledBySW==1)&&(RTA_RR.ldabsu1DetSpareTire==1))
            ||((fu1ESCDisabledBySW==1)&&(Flg_Flat_Tire_Detect_OK == 1)))    /* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */
        
    #else          
    
       		#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        			if((fu1ESCDisabledBySW==0)
        	         ||((fu1ESCDisabledBySW==1)&&(lsespu1AHBGEN3MpresBrkOn==1))
			 		 ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1)))
			#else
          		#if (__RWD_SPORTSMODE==SPORTMODE_2) /*__SPORTSMODE==2*/
			        if((fu1ESCDisabledBySW==0)
			        	||((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==1))
			 		    ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))			        	
			            ||((fu1ESCDisabledBySW==1)&&(Flg_SPORTSMODE_1_ESP_OK==1)))
        		#else
        			if((fu1ESCDisabledBySW==0)
			 		    ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))	        				
        				||((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==1)))
                #endif
			#endif
						
    #endif              
            
        {
            if((YAW_CDC_WORK==1)||((ESP_ON==1)&&(ESP_PULSE_DUMP_FLAG==1)))
            {
/***********************NC, NO Valve Act***********************************/

                if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FL_old==1))
                {
                      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
                    #if (__ESC_TCMF_CONTROL ==1)
                    if ((lcu1US2WHControlFlag_RL == 1)||(lcu1US2WHFadeout_RL == 1)||(FL_ESC.lcespu1TCMFControl == 1))
                    {
                        HV_VL_fl=0;
                        AV_VL_fl=0;
                        #if(__IDB_LOGIC == ENABLE)
                        if((ESP_PULSE_DUMP_FL ==1)&&(SLIP_CONTROL_NEED_FR==1))
                        {
                            HV_VL_fl=TEMP_HV_VL_fl;
                            AV_VL_fl=TEMP_AV_VL_fl;  
                        }
                        else if(U1WheelValveCntr == 1)
                        {
                            HV_VL_fl=TEMP_HV_VL_fl;
                            AV_VL_fl=TEMP_AV_VL_fl;                              
                        }
                         else if(FL_ESC.U1StrWheelValveCnt==1)
                       {
                            HV_VL_fl=TEMP_HV_VL_fl;
                            AV_VL_fl=TEMP_AV_VL_fl;
                        }
                        else
                        {
                            ;   
                        }                        
                        #endif
                    }
                    #else

                    if ((lcu1US2WHControlFlag_RL == 1)||(lcu1US2WHFadeout_RL == 1))
                    {
                        HV_VL_fl=0;
                        AV_VL_fl=0;
                    }
                    #endif 
                    else
                    {
                        HV_VL_fl=TEMP_HV_VL_fl;
                        AV_VL_fl=TEMP_AV_VL_fl;
                    }
                      #else    
                    HV_VL_fl=TEMP_HV_VL_fl;
                    AV_VL_fl=TEMP_AV_VL_fl;
                      #endif
                }
                else
                {
                    ;
                }
                
                if((SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FR_old==1))
                {
                      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
                    #if (__ESC_TCMF_CONTROL ==1)
                    if ((lcu1US2WHControlFlag_RR == 1)||(lcu1US2WHFadeout_RR == 1)||(FR_ESC.lcespu1TCMFControl == 1))
                    {
                        HV_VL_fr=0;
                        AV_VL_fr=0;
                        #if(__IDB_LOGIC == ENABLE)
                        if((ESP_PULSE_DUMP_FR ==1)&&(SLIP_CONTROL_NEED_FL==1))
                        {
                            HV_VL_fr=TEMP_HV_VL_fr;
                            AV_VL_fr=TEMP_AV_VL_fr;  
                        }
                        else if(U1WheelValveCntr == 1)
                        {
                            HV_VL_fr=TEMP_HV_VL_fr;
                            AV_VL_fr=TEMP_AV_VL_fr;                              
                        }
                        else if(FR_ESC.U1StrWheelValveCnt==1)
                        {
                            HV_VL_fr=TEMP_HV_VL_fr;
                            AV_VL_fr=TEMP_AV_VL_fr;                           	
                        }
                        else
                        {
                            ;   
                        }                        
                        #endif
                    }
                    #else
                    if ((lcu1US2WHControlFlag_RR == 1)||(lcu1US2WHFadeout_RR == 1))
                    {
                        HV_VL_fr=0;
                        AV_VL_fr=0;
                    }
                    #endif
                    else
                    {
                        HV_VL_fr=TEMP_HV_VL_fr;
                        AV_VL_fr=TEMP_AV_VL_fr;
                    }
                      #else
                    HV_VL_fr=TEMP_HV_VL_fr;
                    AV_VL_fr=TEMP_AV_VL_fr;
                      #endif
                }
                else
                {
                    ;
                }
                
                if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RL_old==1)||((SLIP_CONTROL_NEED_RR==1)&&(lcu1US3WHControlFlag==1)))
                {
                        #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
                    if ((lcescu1TCMFUnderControlRl == 1)||(lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHFadeout_RL == 1)||(lcu1US3WHControlFlag == 1))
                    {
                        HV_VL_rl=0;
                        AV_VL_rl=0;
                         #if(__IDB_LOGIC == ENABLE)
                        if(U1WheelValveCntr == 1)
                        {
                            HV_VL_rl=TEMP_HV_VL_rl;
                            AV_VL_rl=TEMP_AV_VL_rl;                              
                        }
                        else if(RL_ESC.U1StrWheelValveCnt==1)
                        {
                            HV_VL_rl=TEMP_HV_VL_rl;
                            AV_VL_rl=TEMP_AV_VL_rl;                           	
                        }
                        #endif
                    }
                    else
                    {
                        HV_VL_rl=TEMP_HV_VL_rl;
                        AV_VL_rl=TEMP_AV_VL_rl;
                    }
                        #else       
                    HV_VL_rl=TEMP_HV_VL_rl;
                    AV_VL_rl=TEMP_AV_VL_rl;
                        #endif
                }
                else
                {
                    ;
                }
                if((SLIP_CONTROL_NEED_RR==1)||(SLIP_CONTROL_NEED_RR_old==1)||((SLIP_CONTROL_NEED_RL==1)&&(lcu1US3WHControlFlag==1)))
                {
                        #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
                    if ((lcescu1TCMFUnderControlRr == 1)||(lcu1OVER2WHControlFlag_RR == 1)||(lcu1OVER2WHFadeout_RR == 1)||(lcu1US3WHControlFlag == 1))
                    {
                        HV_VL_rr=0;
                        AV_VL_rr=0;
                        #if(__IDB_LOGIC == ENABLE)
                        if(U1WheelValveCntr == 1)
                        {
                            HV_VL_rr=TEMP_HV_VL_rr;
                            AV_VL_rr=TEMP_AV_VL_rr;                              
                        }
                        else if(RR_ESC.U1StrWheelValveCnt==1)
                        {
                            HV_VL_rr=TEMP_HV_VL_rr;
                            AV_VL_rr=TEMP_AV_VL_rr;                           	
                        }
                        #endif
                    }
                    else
                    {
                        HV_VL_rr=TEMP_HV_VL_rr;
                        AV_VL_rr=TEMP_AV_VL_rr;
                    }
                        #else
                    HV_VL_rr=TEMP_HV_VL_rr;
                    AV_VL_rr=TEMP_AV_VL_rr;
                        #endif
                }
                else
                {
                    ;
                }
                LAESP_vCallCombination();
#if !__SPLIT_TYPE
                LAESP_vCallActXsplitEsvTc();
#else
                LAESP_vCallActFRsplitEsvTc();
#endif

            }
            else
            {
                FL.lcesps16IdbOtherPress =0;
                FR.lcesps16IdbOtherPress =0;
                RL.lcesps16IdbOtherPress =0;
                RR.lcesps16IdbOtherPress =0;
                LAESP_vActuateAfterESCforIDB();                              
            }
        }
        else
        {
            LAESP_vResetVdc();
            FL.lcesps16IdbOtherPress =0;
            FR.lcesps16IdbOtherPress =0;
            RL.lcesps16IdbOtherPress =0;
            RR.lcesps16IdbOtherPress =0;
            CROSS_SLIP_CONTROL_fl = 0;
            CROSS_SLIP_CONTROL_fr = 0;
            CROSS_SLIP_CONTROL_rl = 0;
            CROSS_SLIP_CONTROL_rr = 0;
            
            CROSS_SLIP_CONTROL_old_fl =0;  
			CROSS_SLIP_CONTROL_old_fr =0;  
			CROSS_SLIP_CONTROL_old_rl =0;             
			CROSS_SLIP_CONTROL_old_rr =0;             
        }
    }
    else
    {
       LAESP_vResetVdc();
       FL.lcesps16IdbOtherPress =0;
       FR.lcesps16IdbOtherPress =0;
       RL.lcesps16IdbOtherPress =0;
       RR.lcesps16IdbOtherPress =0;
       CROSS_SLIP_CONTROL_fl = 0;
       CROSS_SLIP_CONTROL_fr = 0;
       CROSS_SLIP_CONTROL_rl = 0;
       CROSS_SLIP_CONTROL_rr = 0;
       CROSS_SLIP_CONTROL_old_fl =0;  
		CROSS_SLIP_CONTROL_old_fr =0;  
		CROSS_SLIP_CONTROL_old_rl =0;             
		CROSS_SLIP_CONTROL_old_rr =0; 
      #if __ADVANCED_MSC
        if ((motor_test_mode >=1)&&(motor_test_mode <=20))
        {
            LCMSC_vActValveCommand();
        }
        else
        {
            ;
        }
      #endif
    }

#endif

#if ((__VALVE_TEST_L == 0) && (__VALVE_TEST_E == 0) && (__VALVE_TEST_A == 0) && (__VALVE_TEST_B == 0) && (__VALVE_TEST_F == 0) && (__BLEEDING_E == 0) && (__VALVE_TEST_CPS == 0) && (__VALVE_TEST_ESC_LFC == 0) && (__VALVE_TEST_ESC_ABS_LFC == 0))
    LAESP_vActuateEsvAfterEsp();
#endif 
    
#if !__SPLIT_TYPE
    if((TCL_DEMAND_fl==1)||(TCL_DEMAND_fr==1)||(TCL_DEMAND_rl==1)||(TCL_DEMAND_rr==1)||
       (S_VALVE_LEFT==1)||(S_VALVE_RIGHT==1))
    {
        VALVE_ACT=1;
    }
#else
    if((TCL_DEMAND_PRIMARY==1)||(TCL_DEMAND_SECONDARY==1)||
       (S_VALVE_PRIMARY==1)||(S_VALVE_SECONDARY==1))
    {
        VALVE_ACT=1;
    }
#endif
    else
    {
        ;
    }


  #if __VDC
    LAESP_vGenerateLFCDuty(); /* moved from LAABS_vGenerateLFCDuty() */
  #endif

  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
  LAESP_vTcTagetPressRear();
	LAESP_vTcTagetPress();
	LAESP_vSetCtrlMode();
	LAESP_vSetFinalTpRate();
//	LAESP_vSetFrntCtrlMode(&FL_ESC,&FL);
//	LAESP_vSetFrntCtrlMode(&FR_ESC,&FR);
//	LAESP_vSetRearCtrlMode(&RL_ESC,&RL);
//	LAESP_vSetRearCtrlMode(&RR_ESC,&RR);
  #endif /* __AHB_GEN3_SYSTEM == ENABLE */

}

#if __VDC

void LAESP_vGenerateLFCDuty(void)
{ 
	#if (__IDB_LOGIC==DISABLE) 
    LAESP_vGenerateLFCDutyWL(&FL, &laesc_FL1HP, &laesc_FL2HP);    
    LAESP_vGenerateLFCDutyWL(&FR, &laesc_FR1HP, &laesc_FR2HP);    
    LAESP_vGenerateLFCDutyWL(&RL, &laesc_RL1HP, &laesc_RL2HP);    
    LAESP_vGenerateLFCDutyWL(&RR, &laesc_RR1HP, &laesc_RR2HP);    
	#else /* #if (__IDB_LOGIC==DISABLE)  */
	LAESP_vGenerateOnOffDutyWL(&FL_ESC,&FL, &laesc_FL1HP, &laesc_FL2HP);
    LAESP_vGenerateOnOffDutyWL(&FR_ESC,&FR, &laesc_FR1HP, &laesc_FR2HP);    
    LAESP_vGenerateOnOffDutyWL(&RL_ESC,&RL, &laesc_RL1HP, &laesc_RL2HP);
    LAESP_vGenerateOnOffDutyWL(&RR_ESC,&RR, &laesc_RR1HP, &laesc_RR2HP);
	#endif

    if(L_SLIP_CONTROL_fl==1)
    {
		la_FL1HP = laesc_FL1HP; 
		la_FL2HP = laesc_FL2HP; 
    }

    if(L_SLIP_CONTROL_fr==1)
    {
		la_FR1HP = laesc_FR1HP; 
		la_FR2HP = laesc_FR2HP;
    }
    
    if(L_SLIP_CONTROL_rl==1)
    {
		la_RL1HP = laesc_RL1HP; 
		la_RL2HP = laesc_RL2HP; 
    }
    
    if(L_SLIP_CONTROL_rr==1)
    {
		la_RR1HP = laesc_RR1HP; 
		la_RR2HP = laesc_RR2HP;
    }
}

void LAESP_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laescHP1, WHEEL_VV_OUTPUT_t *laescHP2)
{
    WL=WL_temp;
    
    if(WL->L_SLIP_CONTROL==1)
    /********************/
    {
      
    #if defined (__ACTUATION_DEBUG)
  	laescHP1->u8debugger = 7;
  	laescHP2->u8debugger = 7;
    #endif /* defined (__ACTUATION_DEBUG) */      

    WL->TCS_built_reapply_time=0;

      #if   __TCMF_CONTROL2
        WL->pwm_duty_temp_old = WL->pwm_duty_temp;
      #endif

        WL->TCS_built_reapply_time=0;

            if(HV_VL_wl==1)
            {
                if(AV_VL_wl==1)
                {           /* ************* Dump! ********* */
                    WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
                    WL->on_time_outlet_pwm=10;           /* '04 SW Winter for ESP exit Pulse-down dump */
                    WL->pwm_duty_temp=PWM_duty_DUMP;
                    WL->pulseup_cycle_count = 0;
                    WL->pulseup_hold_cycle_count = 0;
                }
                else
                {                      /* ************* Hold!!! ******** */
                    if(WL->LFC_Conven_Reapply_Time > 10)
                    {       /* Reapply over 7ms (by CSH 2002 Winter) */
                        WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time - 10;
                    }
                    else
                    {
                        WL->LFC_Conven_Reapply_Time = 0;
                    }
                    WL->pwm_duty_temp=PWM_duty_HOLD;
                    
/*                    if(WL->pulseup_hold_cycle_count < WL->pulseup_cycle_scan_time) */
                    if(WL->pulseup_cycle_count > 0)
                    {
			                WL->pulseup_hold_cycle_count ++;
			                tempW1 = (int16_t)(WL->pulseup_cycle_count + WL->pulseup_hold_cycle_count);
			                if( tempW1 >= WL->pulseup_cycle_scan_time)
			                {
			                	WL->pulseup_cycle_count = 0;
			                	WL->pulseup_hold_cycle_count = 0;
			                }
			              }
                }
            }
            else
            {                          /* ************ Reapply !!! */
                if(REAR_WHEEL_wl==0) {              /* Front */
                    if(ESC_PULSE_UP_RISE_FRONT == 1)
                    {           /*  Pulse up rise  */

                        if(LEFT_WHEEL_wl == 1)
                        {
                            WL->LFC_Conven_Reapply_Time = (int8_t)lcs16EscInitialPulseupDutyfl;
                        }
                        else
                        {
                            WL->LFC_Conven_Reapply_Time = (int8_t)lcs16EscInitialPulseupDutyfr;
                        }

                        WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time - (int8_t)(10*WL->pulseup_cycle_count);
                        if(WL->LFC_Conven_Reapply_Time <= 0)
                        {
                            WL->LFC_Conven_Reapply_Time = 0;
                            WL->pwm_duty_temp = PWM_duty_HOLD;
                        }
                        else
                        {
                            WL->pwm_duty_temp = pwm_duty_null;
                        }

                        WL->pulseup_cycle_count++;
                        if(WL->pulseup_cycle_count >= WL->pulseup_cycle_scan_time)
                        {
                            WL->pulseup_cycle_count = 0;
                        }
                    }
                    else
                    {                                           /* Full rise */
                        WL->LFC_Conven_Reapply_Time= 10;
                        WL->pwm_duty_temp = pwm_duty_null;
                        WL->pulseup_cycle_count = 0;
                    }
                }
                else
                {                              /* Rear */
                    if(ESC_PULSE_UP_RISE_REAR == 1)
                    {           /*  Pulse up rise  */

                        if(LEFT_WHEEL_wl == 1)
                        {
                            WL->LFC_Conven_Reapply_Time = (int8_t)lcs16EscInitialPulseupDutyrl;
                        }
                        else
                        {
                            WL->LFC_Conven_Reapply_Time = (int8_t)lcs16EscInitialPulseupDutyrr;
                        }

                        WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time - (int8_t)(10*WL->pulseup_cycle_count);
                        if(WL->LFC_Conven_Reapply_Time <= 0)
                        {
                            WL->LFC_Conven_Reapply_Time = 0;
                            WL->pwm_duty_temp = PWM_duty_HOLD;
                        }
                        else
                        {
                            WL->pwm_duty_temp = pwm_duty_null;
                        }

                        WL->pulseup_cycle_count++;
                        if(WL->pulseup_cycle_count >= WL->pulseup_cycle_scan_time)
                        {
                            WL->pulseup_cycle_count = 0;
                        }
                    }
                    else
                    {                                           /* Full rise */
                        WL->LFC_Conven_Reapply_Time= 10;
                        WL->pwm_duty_temp = pwm_duty_null;
                        WL->pulseup_cycle_count = 0;
                    }
                }

								WL->pulseup_hold_cycle_count = 0;
            }            

        if(HV_VL_wl==0)    /* ------------------- Rise -------------------*/
        {
            laescHP1->u8OutletOnTime = 0;
            laescHP2->u8OutletOnTime = 0;
            
              #if __ESC_LFC_DUTY_COMP
            if (((WL->REAR_WHEEL == 0) && (ESC_PULSE_UP_RISE_FRONT == 1)) || ((WL->REAR_WHEEL == 1) && (ESC_PULSE_UP_RISE_REAR == 1)))
            {
                if (PARTIAL_BRAKE == 1)
                {
                    if(target_vol == 0)
                    {
                        esp_tempW6 = MSC_14_V;
                    } 
                    else
                    {
                        esp_tempW6 = target_vol;
                    }
                    
                    if (WL->REAR_WHEEL == 0)
                    {
                        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_H, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_M, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_L);  //Initial Duty : 80                        
                        esp_tempW5 = LCESP_s16IInter4Point(esp_tempW6, S16_ESC_LFC_REF_MTR_VOLT_F_1, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_F_1,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_F_2, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_F_2,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_F_3, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_F_3,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_F_4, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_F_4); //Motor Target Volt Compensation Front
                    }
                    else                                        //Rear Wheel
                    {
                        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_H, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_M, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_L);  //Initial Duty : 100
                        esp_tempW5 = LCESP_s16IInter4Point(esp_tempW6, S16_ESC_LFC_REF_MTR_VOLT_R_1, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_R_1,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_R_2, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_R_2,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_R_3, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_R_3,
                                                                       S16_ESC_LFC_REF_MTR_VOLT_R_4, (int16_t)S8_ESC_LFC_MTR_VOLT_COMP_R_4); //Motor Target Volt Compensation Rear
                    } 
                      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                    esp_tempW1 = MPRESS_100BAR - WL->s16_Estimated_Active_Press; //Delta Pressure
                      #else
                    esp_tempW1 = (mpress + MPRESS_160BAR) - WL->s16_Estimated_Active_Press; //Delta Pressure
                      #endif
                    
                    esp_tempW2 = LCESP_s16IInter3Point(esp_tempW1, S16_ESC_LFC_REF_DEL_PRESS_F_1, (int16_t)S8_ESC_LFC_DEL_PRESS_COMP_F_1,      //Delta Pressure Compensation : low Del_P low duty
                                                                   S16_ESC_LFC_REF_DEL_PRESS_F_2, (int16_t)S8_ESC_LFC_DEL_PRESS_COMP_F_2,
                                                                   S16_ESC_LFC_REF_DEL_PRESS_F_3, (int16_t)S8_ESC_LFC_DEL_PRESS_COMP_F_3);
                      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                    esp_tempW3 = LCESP_s16IInter3Point(lsesps16AHBGEN3mpress, S16_ESC_LFC_REF_MC_PRESS_F_1, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_1,           //Master Pressure Compensation : high MP high duty
                                                                              S16_ESC_LFC_REF_MC_PRESS_F_2, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_2,
                                                                              S16_ESC_LFC_REF_MC_PRESS_F_3, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_3);  
                      #else
                    esp_tempW3 = LCESP_s16IInter3Point(mpress,   S16_ESC_LFC_REF_MC_PRESS_F_1, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_1,           //Master Pressure Compensation : high MP high duty
                                                                 S16_ESC_LFC_REF_MC_PRESS_F_2, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_2,
                                                                 S16_ESC_LFC_REF_MC_PRESS_F_3, (int16_t)S8_ESC_LFC_MC_PRESS_COMP_F_3);
                     #endif                                            
                }
                else
                {
                    if (REAR_WHEEL_wl == 0)                     //Front Wheel
                    {
                        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_ABS_H, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_ABS_M, (int16_t)U8_ESC_LFC_RISE_DUTY_FRONT_ABS_L);  //Initial Duty : 80                        
                    }
                    else                                        //Rear Wheel
                    {
                        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_ABS_H, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_ABS_M, (int16_t)U8_ESC_LFC_RISE_DUTY_REAR_ABS_L);  //Initial Duty : 80                        
                    }
                      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                    esp_tempW1 = MPRESS_100BAR - (WL->s16_Estimated_Active_Press);                 //Delta Pressure
                      #else
                    esp_tempW1 = mpress - (WL->s16_Estimated_Active_Press);                 //Delta Pressure
                      #endif
                    
                    esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  S16_ESC_ABS_LFC_REF_DEL_P_F_1, (int16_t)S8_ESC_ABS_LFC_DEL_P_COMP_F_1,      //Delta Pressure Compensation
                                                                    S16_ESC_ABS_LFC_REF_DEL_P_F_2, (int16_t)S8_ESC_ABS_LFC_DEL_P_COMP_F_2,
                                                                    S16_ESC_ABS_LFC_REF_DEL_P_F_3, (int16_t)S8_ESC_ABS_LFC_DEL_P_COMP_F_3,
                                                                    S16_ESC_ABS_LFC_REF_DEL_P_F_4, (int16_t)S8_ESC_ABS_LFC_DEL_P_COMP_F_4);
                                                                   
                    esp_tempW3 = 0;                                                         //Master Pressure Compensation : 0 
                    esp_tempW5 = 0;                                                         //Motor Target Volt Compensation : 0                                                  
                }
                
                esp_tempW0 = esp_tempW4 + esp_tempW2 + esp_tempW3 + esp_tempW5;  //Initial Duty + Delta_P Comp + MP Comp + Motor Comp
                
                if (esp_tempW0 > 120)
                {
                    esp_tempW0 = 120;
                }
                else if (esp_tempW0 < 50)
                {
                    esp_tempW0 = 50;
                } 
                else
                {
                    ;
                } 
                
                laescHP1->u8InletOnTime = 0; 						
	            laescHP2->u8InletOnTime = 0; 
                laescHP1->u8PwmDuty     = (uint8_t)esp_tempW0;
                laescHP2->u8PwmDuty     = (uint8_t)esp_tempW0;
            }
            else
            {
                laescHP1->u8InletOnTime = U8_BASE_CTRLTIME; 						
	            laescHP2->u8InletOnTime = U8_BASE_CTRLTIME;              
	            laescHP1->u8PwmDuty = pwm_duty_null; 
	            laescHP2->u8PwmDuty = pwm_duty_null;
            }
              #else
                #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if (lsespu1AHBGEN3MpresBrkOn==1)
                #else
            if (MPRESS_BRAKE_ON==1)
                #endif
            {         	
		  		if(WL->REAR_WHEEL==0)  
          		{
          			if(ABS_fz==1)
          			{
	                    laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT_ABS; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT_ABS;  
	                }
	                else
              	    {
	                    laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT_CBS; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT_CBS;  
              	    }
                }
                else  
            	{
          			if(ABS_fz==1)
          			{
	                    laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR_ABS; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR_ABS;  
	                }
	                else
              	    {
	                    laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR_CBS; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR_CBS;  
              	    }        		
            	}
            }
            else
        	{
        		if(WL->REAR_WHEEL==0)  
          		{ 
		  	        if(ESC_PULSE_UP_RISE_FRONT == 1)    
                    {         
	                    laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_FRONT;  
              	    }
              	    else
            		{
	                    laescHP1->u8InletOnTime = U8_BASE_CTRLTIME; 						
	                    laescHP2->u8InletOnTime = U8_BASE_CTRLTIME;              
	                    laescHP1->u8PwmDuty = pwm_duty_null; 
	                    laescHP2->u8PwmDuty = pwm_duty_null;            			
            		}
                }
                else  
            	{
              	    if (ESC_PULSE_UP_RISE_REAR == 1)
            		{
            		  	laescHP1->u8InletOnTime = 0; 						
	                    laescHP2->u8InletOnTime = 0;              
	                    laescHP1->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR; 
	                    laescHP2->u8PwmDuty = S16_ESC_LFC_RISE_DUTY_REAR;
            	    }     		
	                else
	                {
	                    laescHP1->u8InletOnTime = U8_BASE_CTRLTIME; 						
	                    laescHP2->u8InletOnTime = U8_BASE_CTRLTIME;              
	                    laescHP1->u8PwmDuty = pwm_duty_null; 
	                    laescHP2->u8PwmDuty = pwm_duty_null;
        	        }
                }
          	}
          	  #endif
        }
        else
        {   
            laescHP1->u8InletOnTime = 0; 
            laescHP2->u8InletOnTime = 0;  
            
            if(AV_VL_wl==0)  /* ------------------- Hold -------------------*/ 
            {        
                laescHP1->u8OutletOnTime = 0;    
                laescHP2->u8OutletOnTime = 0;          
                laescHP1->u8PwmDuty = PWM_duty_HOLD;
                laescHP2->u8PwmDuty = PWM_duty_HOLD;                        
            }        
            else       /* ------------------- Dump -------------------*/       
            {
             
                if( (WL->REAR_WHEEL==0) && ( ((SLIP_CONTROL_NEED_FL==1)&&(WL->LEFT_WHEEL==1))
                	|| ((SLIP_CONTROL_NEED_FR==1)&&(WL->LEFT_WHEEL==0)) )  )
                {
                    if(NC_VALVE_DUTY_CONTROL_FRONT==1)
                    {                
                      if(esp_slip_nc_duty_front<U8_BASE_CTRLTIME)
                      {
                          laescHP1->u8OutletOnTime = (uint8_t)esp_slip_nc_duty_front;    /* dump */
                          laescHP2->u8OutletOnTime = 0;
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_HOLD;      
                      }
                      else
                      {
                          laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
                          laescHP2->u8OutletOnTime = (uint8_t)(esp_slip_nc_duty_front - U8_BASE_CTRLTIME);
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_DUMP;                        
                      }                 
                    }
                    else
                    {
                          laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;  
                          laescHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_DUMP;                        
                    }
                }
                else if( (WL->REAR_WHEEL==1) && ( ((SLIP_CONTROL_NEED_RL==1)&&(WL->LEFT_WHEEL==1))
                	|| ((SLIP_CONTROL_NEED_RR==1)&&(WL->LEFT_WHEEL==0)) )  )
                {
                    if(NC_VALVE_DUTY_CONTROL_REAR==1)
                    {
                      if(esp_slip_nc_duty_rear<U8_BASE_CTRLTIME)
                      {
                          laescHP1->u8OutletOnTime = (uint8_t)esp_slip_nc_duty_rear;
                          laescHP2->u8OutletOnTime = 0;
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_HOLD;      
                      }
                      else
                      {
                          laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
                          laescHP2->u8OutletOnTime = (uint8_t)(esp_slip_nc_duty_rear - U8_BASE_CTRLTIME);
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_DUMP;      
                      }                 
                    }
                    else
                    {
                          laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
                          laescHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
                          laescHP1->u8PwmDuty = PWM_duty_DUMP;
                          laescHP2->u8PwmDuty = PWM_duty_DUMP;      
                    }                  
                }
                else
                {
                    if( (WL->REAR_WHEEL==0) && (ESP_PULSE_DUMP_FLAG_F)
                     && ( ((SLIP_CONTROL_NEED_FL_old)&&(WL->LEFT_WHEEL==1))
                    	  ||((SLIP_CONTROL_NEED_FR_old)&&(WL->LEFT_WHEEL==0)) ) )
                    { 
                        if(pulse_dump_nc_duty_front<U8_BASE_CTRLTIME)
                        {
                            laescHP1->u8OutletOnTime = (uint8_t)pulse_dump_nc_duty_front;
                            laescHP2->u8OutletOnTime = 0;
                            laescHP1->u8PwmDuty = PWM_duty_DUMP;
                            laescHP2->u8PwmDuty = PWM_duty_HOLD;      
                        }
                        else
                        {
                            laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
                            laescHP2->u8OutletOnTime = (uint8_t)(pulse_dump_nc_duty_front - U8_BASE_CTRLTIME);
                            laescHP1->u8PwmDuty = PWM_duty_DUMP;
                            laescHP2->u8PwmDuty = PWM_duty_DUMP;      
                        }  
                    }
                    else if( (WL->REAR_WHEEL==1) && (ESP_PULSE_DUMP_FLAG_R)
                     && ( ((SLIP_CONTROL_NEED_RL_old)&&(WL->LEFT_WHEEL==1))
                    	  ||((SLIP_CONTROL_NEED_RR_old)&&(WL->LEFT_WHEEL==0)) ) )
                    {
                        if(pulse_dump_nc_duty_rear<U8_BASE_CTRLTIME)
                        {
                            laescHP1->u8OutletOnTime = (uint8_t)pulse_dump_nc_duty_rear;
                            laescHP2->u8OutletOnTime = 0;
                            laescHP1->u8PwmDuty = PWM_duty_DUMP;
                            laescHP2->u8PwmDuty = PWM_duty_HOLD;      
                        }
                        else
                        {
                            laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
                            laescHP2->u8OutletOnTime = (uint8_t)(pulse_dump_nc_duty_rear - U8_BASE_CTRLTIME);
                            laescHP1->u8PwmDuty = PWM_duty_DUMP;
                            laescHP2->u8PwmDuty = PWM_duty_DUMP;      
                        }                   
                    }       
                    else
                    {
                        laescHP1->u8OutletOnTime = U8_BASE_CTRLTIME; 
                        laescHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
                        laescHP1->u8PwmDuty = PWM_duty_DUMP;
                        laescHP2->u8PwmDuty = PWM_duty_DUMP;      
                    }   
                }          
            }
        }       
 
    }
    else
    {
          WL->pulseup_cycle_count = 0;
          WL->pulseup_hold_cycle_count = 0;
    }
}

void LAESP_vGenerateOnOffDutyWL(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laescHP1, WHEEL_VV_OUTPUT_t *laescHP2)
{
	if( WL_ESC->SLIP_CONTROL_NEED == 1)
    {
         esp_tempW1= WL_ESC->u8WhlVlvDumpCmdTi;
    }
    else
    {
         esp_tempW1 = U8_BASE_LOOPTIME;
    }

	if(WL_temp->HV_VL==1)
	{
		if(WL_temp->AV_VL==1)
		{
			LA_vSetWheelVvOnOffDumpOutput(laescHP1, laescHP2, esp_tempW1);
		}
		else
		{
			LA_vSetWheelVvOnOffHoldOutput(laescHP1, laescHP2);
		}
	}
	else
	{
		if(WL_temp->AV_VL==1)
		{
			//circ
		}
		else
		{
			LA_vSetWheelVvOnOffRiseOutput(laescHP1, laescHP2, U8_BASE_LOOPTIME);
		}
	}
}

#if __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
static void  LAESP_vDetectActiveRiseMode(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if (lsespu1AHBGEN3MpresBrkOn == 0)
      #else
    if (MPRESS_BRAKE_ON == 0)
      #endif
    {
        WL->laescu1ESCLFCTestActiveRise = 1;
    }
    else
    {
          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        if((lsesps16AHBGEN3mpress - WL->s16_Estimated_Active_Press)>MPRESS_30BAR)
          #else
        if((mpress - WL->s16_Estimated_Active_Press)>MPRESS_30BAR)
          #endif
        {
            WL->laescu1ESCLFCTestActiveRise = 0;
        }
        else
        {
            #if __VALVE_TEST_ESC_ABS_LFC
            WL->laescu1ESCLFCTestActiveRise = 1;
            #else
            WL->laescu1ESCLFCTestActiveRise = 1;
            #endif
        }
    }    
}
#endif

#if (__MGH_80_10MS == ENABLE)
void LAVALVETEST_vGenerateLFCDutyWL(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *lavalvetestHP1, WHEEL_VV_OUTPUT_t *lavalvetestHP2)
{
    WL=WL_temp;
    
    if(HV_VL_wl==1)
    {
        lavalvetestHP1->u8InletOnTime = 0;            
        lavalvetestHP2->u8InletOnTime = 0;            
        
        if (AV_VL_wl==1)
        {
            lavalvetestHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
            lavalvetestHP2->u8OutletOnTime = U8_BASE_CTRLTIME;
            lavalvetestHP1->u8PwmDuty     = PWM_duty_DUMP;
            lavalvetestHP2->u8PwmDuty     = PWM_duty_DUMP;
        }
        else
        {
            lavalvetestHP1->u8OutletOnTime = 0;
            lavalvetestHP2->u8OutletOnTime = 0;
            lavalvetestHP1->u8PwmDuty     = PWM_duty_HOLD;
            lavalvetestHP2->u8PwmDuty     = PWM_duty_HOLD;
        }
    }
    else
    {
          #if __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
        lavalvetestHP1->u8InletOnTime = 0;            
        lavalvetestHP2->u8InletOnTime = 0;   
        lavalvetestHP1->u8OutletOnTime = 0;
        lavalvetestHP2->u8OutletOnTime = 0;
        
        if (WL->laescu1ESCLFCTestActiveRise == 1)       /*Suction Condition*/ 
        {
            if(target_vol == 0)
            {
                esp_tempW6 = MSC_14_V;
            } 
            else
            {
                esp_tempW6 = target_vol;
            }
            
            if (REAR_WHEEL_wl == 0)                     //Front Wheel
            {
                esp_tempW4 = S16_ESC_PULSE_UP_ADD_CYCLE_FRONT;  //Initial Duty : 80
                esp_tempW5 = LCESP_s16IInter4Point(esp_tempW6, MSC_2_V, -10,
                                                               MSC_4_V,  -5,
                                                               MSC_6_V,  -5,
                                                               MSC_8_V,   0); //Motor Target Volt Compensation Front                                                            
            }                                                 
            else                                        //Rear Wheel
            {
                esp_tempW4 = S16_ESC_PULSE_UP_ADD_CYCLE_REAR;  //Initial Duty : 100
                esp_tempW5 = LCESP_s16IInter4Point(esp_tempW6, MSC_2_V, -15,
                                                               MSC_3_V, -10,
                                                               MSC_4_V,   0,
                                                               MSC_4_V,   0); //Motor Target Volt Compensation Rear
            }
            
              #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
            esp_tempW1 = (lsesps16AHBGEN3mpress + MPRESS_160BAR) - WL->s16_Estimated_Active_Press; //Delta Pressure
              #else
            esp_tempW1 = (mpress + MPRESS_160BAR) - WL->s16_Estimated_Active_Press; //Delta Pressure
              #endif
            
            esp_tempW2 = LCESP_s16IInter3Point(esp_tempW1,   MPRESS_0BAR, -30,      //Delta Pressure Compensation : low Del_P low duty
                                                            MPRESS_60BAR, -10,
                                                           MPRESS_110BAR,   0);
              #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
            esp_tempW3 = LCESP_s16IInter3Point(lsesps16AHBGEN3mpress, MPRESS_4BAR, 0,           //Master Pressure Compensation : high MP high duty
                                                                      MPRESS_20BAR, 20,
                                                                      MPRESS_50BAR, 30);  
              #else                                            
            esp_tempW3 = LCESP_s16IInter3Point(mpress,    MPRESS_4BAR, 0,           //Master Pressure Compensation : high MP high duty
                                                         MPRESS_20BAR, 20,
                                                         MPRESS_50BAR, 30); 
              #endif                                             
                                                                                                              
        }
        else                                            /*Non Suction Condition*/
        {
            if (REAR_WHEEL_wl == 0)                     //Front Wheel
            {
                esp_tempW4 = S16_ESC_PULSE_UP_ADD_CYCLE_FRONT_ABS;  //Initial Duty : 80
            }
            else                                        //Rear Wheel
            {
                esp_tempW4 = S16_ESC_PULSE_UP_ADD_CYCLE_REAR_ABS;   //Initial Duty : 80
            }
            
              #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
            esp_tempW1 = mpress - (WL->s16_Estimated_Active_Press);                 //Delta Pressure
              #else
            esp_tempW1 = lsesps16AHBGEN3mpress - (WL->s16_Estimated_Active_Press);                 //Delta Pressure
              #endif
            
            esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  MPRESS_20BAR, -30,      //Delta Pressure Compensation
                                                            MPRESS_50BAR, -20,
                                                            MPRESS_70BAR, -10,
                                                            MPRESS_90BAR,   0);
                                                           
            esp_tempW3 = 0;                                                         //Master Pressure Compensation : 0 
            esp_tempW5 = 0;                                                         //Motor Target Volt Compensation : 0                                                  
        }
        
        esp_tempW0 = esp_tempW4 + esp_tempW2 + esp_tempW3;  //Initial Duty + Delta_P Comp + MP Comp
        
        lavalvetestHP1->u8PwmDuty     = (uint8_t)esp_tempW0;
        lavalvetestHP2->u8PwmDuty     = (uint8_t)esp_tempW0;
          #else
        lavalvetestHP1->u8InletOnTime = U8_BASE_CTRLTIME; 
        lavalvetestHP2->u8InletOnTime = U8_BASE_CTRLTIME; 
        lavalvetestHP1->u8OutletOnTime = 0;
        lavalvetestHP2->u8OutletOnTime = 0;
        lavalvetestHP1->u8PwmDuty     = 0;
        lavalvetestHP2->u8PwmDuty     = 0;
          #endif
    }
}
#endif

#endif /*__VDC*/

#if __ADVANCED_MSC
void LCMSC_vSetVDCTargetVoltage(void)
{
    #if (__CAR==GM_TAHOE)||(__CAR==GM_SILVERADO)
        msc_rise_scan = 49;
    #endif

    if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1))    /* Initial scan Driving  */
    {
        esp_tempW0 = msc_rise_scan ;
    }
    else if((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))
    {
        esp_tempW0 = 1;
    }
    else
    {
        esp_tempW0 = 1;
    }

    if ( YAW_CDC_WORK==1 )
    {
            #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
        if ((lcescu1TCMFUnderControlRl == 1) || (lcescu1TCMFUnderControlRr == 1))
        {
            /* High-speed ( H_mu, M_mu, L_mu )  */                
            esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_USC_TARGET_VOL_HMU_HSP,
                                                               S16_TCMF_USC_TARGET_VOL_MMU_HSP,  
                                                               S16_TCMF_USC_TARGET_VOL_LMU_HSP);
                                     
            /* Med-speed ( H_mu, M_mu, L_mu )  */                
            esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_USC_TARGET_VOL_HMU_MSP,
                                                               S16_TCMF_USC_TARGET_VOL_MMU_MSP,  
                                                               S16_TCMF_USC_TARGET_VOL_LMU_MSP);
                                                                                                                                  
            /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
            esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_USC_TARGET_VOL_HMU_LSP,
                                                               S16_TCMF_USC_TARGET_VOL_MMU_LSP,  
                                                               S16_TCMF_USC_TARGET_VOL_LMU_LSP);    
                                                            
            vdc_msc_target_vol = LCESP_s16IInter4Point( vrefk,  S16_ESP_SLIP_SPEED_GAIN_DEP_S_V,  esp_tempW3,
                                                                S16_ESP_SLIP_SPEED_GAIN_DEP_L_V,  esp_tempW2,
                                                                S16_ESP_SLIP_SPEED_GAIN_DEP_M_V,  esp_tempW2,
                                                                S16_ESP_SLIP_SPEED_GAIN_DEP_H_V,  esp_tempW1 ); 
            
            #if __TVBB
                    if (lcescu1TCMFUCSafterTVBBMotor == 1)
                    {
                        if (vdc_msc_target_vol > (target_vol + S16_TCMF_USC_TAR_V_INC_AFT_TVBB))
                        {
                            vdc_msc_target_vol = target_vol + S16_TCMF_USC_TAR_V_INC_AFT_TVBB;
                        }
                        else
                        {
                            lcescu1TCMFUCSafterTVBBMotor = 0;
                        }
                    }
                    else
                    {
                        ;
                    }
                       
            #endif     
            if ((lcu1US2WHControlFlag_RL == 1)||(lcu1US2WHControlFlag_RR == 1))
            {
                if (lcu8US2WH_count <= U8_UND_2WH_MSC_INITIAL_TIME)
                {
                    vdc_msc_target_vol = (int16_t)U8_UND_2WH_MSC_INITIAL_TARGET_V*100;
                }
                else
                {
                    vdc_msc_target_vol = vdc_msc_target_vol + (int16_t)U8_UND_2WH_MSC_ADD_TARGET_VOLT*100;
            }
            }
            else
            {
                ;
            }
                 
                   
        }
        /* OVER 2WH*/
        else if ((lcu1OVER2WHControlFlag_RL == 1) || (lcu1OVER2WHControlFlag_RR == 1))
        {
            /* High-speed ( H_mu, M_mu, L_mu )  */                
            esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OVER_2WH_TARGET_VOL_HMU_HSP,
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_MMU_HSP,  
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_LMU_HSP);
                                     
            /* Med-speed ( H_mu, M_mu, L_mu )  */                
            esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OVER_2WH_TARGET_VOL_HMU_MSP,
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_MMU_MSP,  
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_LMU_MSP);
                                                                                                                                  
            /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
            esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OVER_2WH_TARGET_VOL_HMU_LSP,
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_MMU_LSP,  
                                                               (int16_t)U8_OVER_2WH_TARGET_VOL_LMU_LSP);    
                                                            
            esp_tempW5 = LCESP_s16IInter4Point( vrefk,  S16_ESP_SLIP_SPEED_GAIN_DEP_S_V,  esp_tempW3,
                                                        S16_ESP_SLIP_SPEED_GAIN_DEP_L_V,  esp_tempW2,
                                                        S16_ESP_SLIP_SPEED_GAIN_DEP_M_V,  esp_tempW2,
                                                        S16_ESP_SLIP_SPEED_GAIN_DEP_H_V,  esp_tempW1 ); 
            
            vdc_msc_target_vol = esp_tempW5 * 100;
            
            /*     
            if ((lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHControlFlag_RR == 1))
            {
                vdc_msc_target_vol = vdc_msc_target_vol + S16_OVER_2WH_MSC_ADD_TARGET_VOLT;
            }
            else
            {
                ;
            }
            */
                   
        }
        else
        {
            #endif

        if(   ((ESP_BRAKE_CONTROL_FL==1)&&(slip_control_need_counter_fl<esp_tempW0))        /* Initial Full */
            ||((ESP_BRAKE_CONTROL_FR==1)&&(slip_control_need_counter_fr<esp_tempW0))
            ||((ESP_BRAKE_CONTROL_RL==1)&&(slip_control_need_counter_rl<esp_tempW0))
            ||((ESP_BRAKE_CONTROL_RR==1)&&(slip_control_need_counter_rr<esp_tempW0))  )
        {
              #if (__INITIAL_MTR_DRIVE_IMPROVE == 1)             
            
            esp_tempW0 = LCESP_s16IInter3Point(McrAbs(yaw_out_dot2), S16_ESC_INIT_REF_YAWD_HMU_1, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_HMU_1,
                                                                     S16_ESC_INIT_REF_YAWD_HMU_2, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_HMU_2,
                                                                     S16_ESC_INIT_REF_YAWD_HMU_3, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_HMU_3);
            
            esp_tempW1 = LCESP_s16IInter3Point(McrAbs(yaw_out_dot2), S16_ESC_INIT_REF_YAWD_MMU_1, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_MMU_1, 
                                                                     S16_ESC_INIT_REF_YAWD_MMU_2, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_MMU_2, 
                                                                     S16_ESC_INIT_REF_YAWD_MMU_3, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_MMU_3);
            
            esp_tempW2 = LCESP_s16IInter3Point(McrAbs(yaw_out_dot2), S16_ESC_INIT_REF_YAWD_LMU_1, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_LMU_1, 
                                                                     S16_ESC_INIT_REF_YAWD_LMU_2, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_LMU_2, 
                                                                     S16_ESC_INIT_REF_YAWD_LMU_3, (int16_t)U8_ESC_INIT_TAR_VOLT_YAWD_LMU_3);                                                          
            
            esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);
            
            vdc_msc_target_vol = esp_tempW3 * 100;      
            
            if(esp_mu > lsesps16MuHigh)
            {
                if (vdc_msc_target_vol < S16_MSC_RISE_TARGET_VOL_HMU)
                {
                    vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_HMU;
                }
                else
                {
                    ;
                }
            }
            else if(esp_mu > lsesps16MuMed)
            {
                if (vdc_msc_target_vol < S16_MSC_RISE_TARGET_VOL_MMU)
                {
                    vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_MMU;
                }
                else
                {
                    ;
                }
            }                                                        
            else
            {
                if (vdc_msc_target_vol < S16_MSC_RISE_TARGET_VOL_LMU)
                {
                    vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_LMU;
                }
                else
                {
                    ;
                }
            }                                                      
              #else
            vdc_msc_target_vol = MSC_14_V;
              #endif
        }
        else
        {
            if(   ((ESP_BRAKE_CONTROL_FL==1)&&(esp_control_mode_fl<3))      /* Rise mode */
                ||((ESP_BRAKE_CONTROL_FR==1)&&(esp_control_mode_fr<3))
                ||((ESP_BRAKE_CONTROL_RL==1)&&(esp_control_mode_rl<3))
                ||((ESP_BRAKE_CONTROL_RR==1)&&(esp_control_mode_rr<3)) )
            {
                if(esp_mu > lsesps16MuHigh)
                {
                 vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_HMU;
                }
                else if(esp_mu > lsesps16MuMed)
                {
                    vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_MMU;
                }
                else
                {
                	  vdc_msc_target_vol = S16_MSC_RISE_TARGET_VOL_LMU;
                }
            }
            else
            {
                 vdc_msc_target_vol = S16_MSC_HOLD_TARGET_VOL;

            /* '08 SWD : Rear Hold -> MSC */
                 
                if( ( ACT_TWO_WHEEL == 0 ) && ((ESP_BRAKE_CONTROL_RR==1) || (ESP_BRAKE_CONTROL_RL==1)) )
                { 
                    
/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/                    
                /* High-speed ( H_mu, M_mu, L_mu )  */                
                esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_MSC_REAR_TARGET_VOL_HMU_HSP,
                                                                   S16_MSC_REAR_TARGET_VOL_MMU_HSP,  
                                                                   S16_MSC_REAR_TARGET_VOL_LMU_HSP);
                                         
                /* Med-speed ( H_mu, M_mu, L_mu )  */                
                esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_MSC_REAR_TARGET_VOL_HMU_MSP,
                                                                   S16_MSC_REAR_TARGET_VOL_MMU_MSP,  
                                                                   S16_MSC_REAR_TARGET_VOL_LMU_MSP);
                                                                                                                                      
                /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
                esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_MSC_REAR_TARGET_VOL_HMU_LSP,
                                                                   S16_MSC_REAR_TARGET_VOL_MMU_LSP,  
                                                                   S16_MSC_REAR_TARGET_VOL_LMU_LSP);    
                                                                
                vdc_msc_target_vol = LCESP_s16IInter4Point( vrefk,  S16_ESP_SLIP_SPEED_GAIN_DEP_S_V,  esp_tempW3,
                                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_L_V,  esp_tempW2,
                                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_M_V,  esp_tempW2,
                                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_H_V,  esp_tempW1 );
/*}*/                                         
                }
                else
                {
                   ;
                } 

            }
        }
            #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
        }
            #endif
        VDC_MSC_MOTOR_ON=1;
    }
    else
    {
        vdc_msc_target_vol = MSC_0_V;
        VDC_MSC_MOTOR_ON=0;
    }

}
#endif

void LAESP_vActuateEsvAfterEsp(void)
{
		S_VALVE_PRIMARY_TEMP=0;
    S_VALVE_SECONDARY_TEMP=0;

#if __HSA
    if((FLAG_ACTIVE_BRAKING==1) && (HSA_flg==0))
#else
    if(FLAG_ACTIVE_BRAKING==1)
#endif
    {
        system_act_time=840;
        S_VALVE_PRIMARY_TEMP_cnt=0;
    }
    else {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if((system_act_time>0)&&(!lsespu1AHBGEN3MpresBrkOn)&&(motor_init_on_flg)&&(
            ((vref>VREF_30_KPH)&&(S_VALVE_PRIMARY_TEMP_cnt==0)&&(mtp==0))||
            ((vref>VREF_30_KPH)&&(S_VALVE_PRIMARY_TEMP_cnt>0))||
            ((vref>VREF_55_KPH)&&(mtp>5))
            ))  
          #else
        if((system_act_time>0)&&(!MPRESS_BRAKE_ON)&&(motor_init_on_flg)&&(
            ((vref>VREF_30_KPH)&&(S_VALVE_PRIMARY_TEMP_cnt==0)&&(mtp==0))||
            ((vref>VREF_30_KPH)&&(S_VALVE_PRIMARY_TEMP_cnt>0))||
            ((vref>VREF_55_KPH)&&(mtp>5))
            )) 
          #endif  
            
            {
            system_act_time--;

            if(system_act_time<798)
            {
                S_VALVE_PRIMARY_TEMP=1;
                if (S_VALVE_PRIMARY_TEMP_cnt<L_U8_TIME_10MSLOOP_900MS)
                {
                    S_VALVE_PRIMARY_TEMP_cnt++;
                }
                else
                {
                    ;
                }            
            }
            else
            {
                S_VALVE_PRIMARY_TEMP_cnt=0;
            }
            if(S_VALVE_PRIMARY_TEMP_cnt>L_U8_TIME_10MSLOOP_100MS)  
            {
                S_VALVE_SECONDARY_TEMP=1;            /*delay */
            }
            else
            {
                ;
            }            

            if(system_act_time<L_U8_TIME_10MSLOOP_110MS)
            {
                S_VALVE_PRIMARY_TEMP=0;
            }
            else
            {
                ;
            }            
            if(system_act_time<L_U8_TIME_10MSLOOP_10MS)
            {
                S_VALVE_SECONDARY_TEMP=0;
            }
            else
            {
                ;
            }            
        }
        else
        {
            S_VALVE_PRIMARY_TEMP_cnt=0;
        }
    }

    if ((S_VALVE_PRIMARY_TEMP ==0) && (S_VALVE_SECONDARY_TEMP == 0))
    {
        lau8_S_VALVE_PRIMARY_in_cnt =0;
    }
    else if (lau8_S_VALVE_PRIMARY_in_cnt<L_U8_TIME_10MSLOOP_1000MS)
    {
        lau8_S_VALVE_PRIMARY_in_cnt = lau8_S_VALVE_PRIMARY_in_cnt+1;
    }
    else
    {
    	;
    }

  #if !__SPLIT_TYPE		/* X-Split	*/
  	if (S_VALVE_PRIMARY_TEMP ==1 ) 		
    {
       S_VALVE_RIGHT = 1;
    }
    else
    {
      if((FLAG_ACTIVE_BRAKING==0)&&(S_VALVE_RIGHT==1))
    	{ 
       	S_VALVE_RIGHT = 0;
    	}
    	else
    	{
        ;
    	}      
    }            
  	if (S_VALVE_SECONDARY_TEMP ==1 ) 	
    {
       S_VALVE_LEFT = 1;
    }
    else
    {
      if((FLAG_ACTIVE_BRAKING==0)&&(S_VALVE_LEFT==1))
    	{ 
       	S_VALVE_LEFT = 0;
    	}
    	else
    	{
        ;
    	}
    }            
  #else					/* FR-Split	*/
  	if (S_VALVE_PRIMARY_TEMP ==1 ) 		
    {
       S_VALVE_PRIMARY = 1;
    }
    else
    {
    	if((FLAG_ACTIVE_BRAKING==0)&&(S_VALVE_PRIMARY==1))
    	{ 
       	S_VALVE_PRIMARY = 0;
    	}
    	else
    	{
        ;
    	}     
    }            
  	if (S_VALVE_SECONDARY_TEMP ==1 ) 	
    {
       S_VALVE_SECONDARY = 1;
    }
    else
    {
    	if((FLAG_ACTIVE_BRAKING==0)&&(S_VALVE_SECONDARY==1))
    	{
       	S_VALVE_SECONDARY = 0;
    	}
    	else
    	{
        ;
    	}
    }            
  #endif  
}

void LAESP_vResetValve(void)
{
/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
    TCL_DEMAND_fl=0;
    TCL_DEMAND_fr=0;
    S_VALVE_RIGHT=0;
    S_VALVE_LEFT=0;
#else
    TCL_DEMAND_PRIMARY=0;
    TCL_DEMAND_SECONDARY=0;
    S_VALVE_PRIMARY=0;
    S_VALVE_SECONDARY=0;
#endif
/*060607 modified and added for FR Split by eslim*/
    BUILT_fl=0;
    BUILT_fr=0;
    BUILT_rl=0;
    BUILT_rr=0;
    HV_VL_fl=0;
    AV_VL_fl=0;
    HV_VL_fr=0;
    AV_VL_fr=0;
    HV_VL_rl=0;
    AV_VL_rl=0;
    HV_VL_rr=0;
    AV_VL_rr=0;
}

void LAESP_vTestValveMode(void)
{
#if __VALVE_TEST_L
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;
        if(esc_press_test_count>280)
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();

            if(esc_press_test_count<1540)
            {
                tempW0=14;
                tempW1=14;
                if(esc_press_test_count<378)
                {
                    S_VALVE_RIGHT=TCL_DEMAND_fr=1;
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                    if(esc_press_test_count>350)
                    {
                        S_VALVE_RIGHT=0;
                    }
                    else
                    {
                        ;
                    }
                    if(esc_press_test_count<294)
                    {
                        VDC_MOTOR_ON=1;
                    }
                    else
                    {
                        if((esc_press_test_count%tempW0)>tempW1)
                        {
                            VDC_MOTOR_ON=0;
                        }
                        else
                        {
                            VDC_MOTOR_ON=1;
                        }
                    }
                }
                else if(esc_press_test_count<560)
                {
                    ;
                }
                else if(esc_press_test_count<658)
                {
                    S_VALVE_RIGHT=TCL_DEMAND_fr=1;
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                    if(esc_press_test_count>630)
                    {
                        S_VALVE_RIGHT=0;
                    }
                    else
                    {
                        ;
                    }
                    if(esc_press_test_count<574)
                    {
                        VDC_MOTOR_ON=1;
                    }
                    else
                    {
                        if((esc_press_test_count%tempW0)>tempW1)
                        {
                            VDC_MOTOR_ON=0;
                        }
                        else
                        {
                            VDC_MOTOR_ON=1;
                        }
                    }
                }
                else if(esc_press_test_count<840)
                {
                    ;
                }
                else if(esc_press_test_count<938)
                {
                    S_VALVE_RIGHT=TCL_DEMAND_fr=1;
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                    if(esc_press_test_count>910)
                    {
                        S_VALVE_RIGHT=0;
                    }
                    else
                    {
                        ;
                    }
                    if(esc_press_test_count<854)
                    {
                        VDC_MOTOR_ON=1;
                    }
                    else
                    {
                        if((esc_press_test_count%tempW0)>tempW1)
                        {
                            VDC_MOTOR_ON=0;
                        }
                        else
                        {
                            VDC_MOTOR_ON=1;
                        }
                    }
                }
                else if(esc_press_test_count<1120)
                {
                    ;
                }
                else if(esc_press_test_count<1218)
                {
                    S_VALVE_RIGHT=TCL_DEMAND_fr=1;
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                    if(esc_press_test_count>1190)
                    {
                        S_VALVE_RIGHT=0;
                    }
                    else
                    {
                        ;
                    }
                    if(esc_press_test_count<1134)
                    {
                        VDC_MOTOR_ON=1;
                    }
                    else
                    {
                        if((esc_press_test_count%tempW0)>tempW1)
                        {
                            VDC_MOTOR_ON=0;
                        }
                        else
                        {
                            VDC_MOTOR_ON=1;
                        }
                    }
                }
                else if(esc_press_test_count<1400)
                {
                    ;
                }
                else if(esc_press_test_count<1498)
                {
                    S_VALVE_RIGHT=TCL_DEMAND_fr=1;
                    HV_VL_rl=1;
                    AV_VL_rl=0;
                    if(esc_press_test_count>1470)
                    {
                        S_VALVE_RIGHT=0;
                    }
                    else
                    {
                        ;
                    }
                    if(esc_press_test_count<1414)
                    {
                        VDC_MOTOR_ON=1;
                    }
                    else
                    {
                        if((esc_press_test_count%tempW0)>tempW1)
                        {
                            VDC_MOTOR_ON=0;
                        }
                        else
                        {
                            VDC_MOTOR_ON=1;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    }
    else {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#elif __VALVE_TEST_E
#if !__SPLIT_TYPE
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(650))  /*FR rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;  						    
                #if (__MGH_80_10MS == ENABLE)  						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						      						
								#endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(850))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(910))  /*FL rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;   						    
                #if (__MGH_80_10MS == ENABLE)   						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                   
								#endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1110))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1170))  /*RL rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;   
                #if (__MGH_80_10MS == ENABLE)   
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;    						        
								#endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1370))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1430))  /*RR rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;                        
								#endif                       
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1630))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1680))  /*ESV Valve open time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
            }
            else if(esc_press_test_count<(1880))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1940))  /*LEFT side rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;    						        						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                   
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2140))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2200))  /*Right side rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						                      
								#endif   						                      
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2400))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2900))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#else
		if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(650))  /*FR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(850))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(910))  /*FL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;                        
								#endif                     
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1110))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1170))  /*RL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                   
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1370))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1430))  /*RR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1630))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1680))  /*ESV Valve open time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(1880))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1940))  /*LEFT side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;    						        						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2140))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2200))  /*Right side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						        
								#endif    						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2400))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2900))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#endif
#elif __VALVE_TEST_ESC_LFC
#if !__SPLIT_TYPE
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;
        
        LAESP_vDetectActiveRiseMode(&FL);
        LAESP_vDetectActiveRiseMode(&FR);
        LAESP_vDetectActiveRiseMode(&RL);
        LAESP_vDetectActiveRiseMode(&RR);
        
        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(670))  /*FR rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=FR.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=FR.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                						    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						      						
				  #endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(870))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(950))  /*FL rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=FL.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=FL.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;       						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;                   
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1150))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1210))  /*RL rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=RL.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=RL.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;     
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1410))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1470))  /*RR rise time : 0.6sec*/
            {
                TCL_DEMAND_fl=RR.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=RR.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                
                  #if (__MGH_80_10MS == ENABLE)
    			LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;
				LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;                        
				  #endif                       
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1670))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1720))  /*ESV Valve open time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
            }
            else if(esc_press_test_count<(1920))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2420))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#else
		if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(650))  /*FR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(850))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(910))  /*FL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;                        
								#endif                     
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1110))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1170))  /*RL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                   
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1370))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1430))  /*RR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1630))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1680))  /*ESV Valve open time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(1880))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1940))  /*LEFT side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;    						        						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2140))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2200))  /*Right side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						        
								#endif    						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2400))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2900))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#endif    
#elif __VALVE_TEST_ESC_ABS_LFC
#if !__SPLIT_TYPE
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;
        
        LAESP_vDetectActiveRiseMode(&FL);
        LAESP_vDetectActiveRiseMode(&FR);
        LAESP_vDetectActiveRiseMode(&RL);
        LAESP_vDetectActiveRiseMode(&RR);
        
        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(140))  /*ESV Valve open time : 1sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 4.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(600))  /*FR dump time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                						    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						      						
				  #endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(630))  /*FR hold time : 0.3sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;       						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;                   
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(650))  /*FR rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=FR.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=FR.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;     
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(660))  /*FR hold time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;       						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;                   
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(680))  /*FR rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=FR.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=FR.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;     
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(880))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(890))  /*FL dump time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;
                						    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						      						
				  #endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(920))  /*FL hold time : 0.3sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						      						
				  #endif                
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(940))  /*FL rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=FL.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=FL.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;   						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(950))  /*FL hold time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						      						
				  #endif                
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(970))  /*FL rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=FL.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=FL.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;   						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1170))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1180))  /*RR dump time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;
                						    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						      						
				  #endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1210))  /*RR hold time : 0.3sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;                    
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1230))  /*RR rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=RR.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=RR.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1240))  /*RR hold time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;                    
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1260))  /*RR rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=RR.laescu1ESCLFCTestActiveRise;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=RR.laescu1ESCLFCTestActiveRise;
                S_VALVE_RIGHT=0;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    			la_FL1HP = lavalvetest_FL1HP; 
				la_FL2HP = lavalvetest_FL2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    			la_RR1HP = lavalvetest_RR1HP; 
				la_RR2HP = lavalvetest_RR2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1460))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1470))  /*RL dump time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;
                						    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;    						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						      						
				  #endif   						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1500))  /*RL hold time : 0.3sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;       						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;                   
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1520))  /*RL rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=RL.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=RL.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;     
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1530))  /*RL hold time : 0.1sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;   
               				    
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;       						    
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;                   
				  #endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1550))  /*RL rise time : 0.2sec*/
            {
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=RL.laescu1ESCLFCTestActiveRise;
                S_VALVE_LEFT=0;
                S_VALVE_RIGHT=RL.laescu1ESCLFCTestActiveRise;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;  
                	 
                  #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    			la_FR1HP = lavalvetest_FR1HP; 
				la_FR2HP = lavalvetest_FR2HP;     
    			LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    			la_RL1HP = lavalvetest_RL1HP; 
				la_RL2HP = lavalvetest_RL2HP;    						        
				  #endif   						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1750))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            //else if(esc_press_test_count<(655))  /*FR hold time : 0.1sec*/
            //{
            //    TCL_DEMAND_fl=0;
            //    TCL_DEMAND_fr=FR.laescu1ESCLFCTestActiveRise;;
            //    S_VALVE_LEFT=0;
            //    S_VALVE_RIGHT=FR.laescu1ESCLFCTestActiveRise;;
            //    HV_VL_fl=0;
            //    AV_VL_fl=0;
            //    HV_VL_fr=1;
            //    AV_VL_fr=0;
            //    HV_VL_rl=1;
            //    AV_VL_rl=0;
            //    HV_VL_rr=0;
            //    AV_VL_rr=0;
            //    
            //      #if (__MGH_80_10MS == ENABLE)
    		//	LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    		//	la_FR1HP = lavalvetest_FR1HP; 
			//	la_FR2HP = lavalvetest_FR2HP;
			//	LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    		//	la_RL1HP = lavalvetest_RL1HP; 
			//	la_RL2HP = lavalvetest_RL2HP;                        
			//	  #endif                       
            //    VDC_MOTOR_ON=1;
            //}
            //else if(esc_press_test_count<(675))  /*FR rise time : 0.2sec*/
            //{
            //    TCL_DEMAND_fl=0;
            //    TCL_DEMAND_fr=FR.laescu1ESCLFCTestActiveRise;;
            //    S_VALVE_LEFT=0;
            //    S_VALVE_RIGHT=FR.laescu1ESCLFCTestActiveRise;;
            //    HV_VL_fl=0;
            //    AV_VL_fl=0;
            //    HV_VL_fr=0;
            //    AV_VL_fr=0;
            //    HV_VL_rl=1;
            //    AV_VL_rl=0;
            //    HV_VL_rr=0;
            //    AV_VL_rr=0;
            //    
            //      #if (__MGH_80_10MS == ENABLE)
    		//	LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    		//	la_FR1HP = lavalvetest_FR1HP; 
			//	la_FR2HP = lavalvetest_FR2HP;
			//	LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    		//	la_RL1HP = lavalvetest_RL1HP; 
			//	la_RL2HP = lavalvetest_RL2HP;                        
			//	  #endif                       
            //    VDC_MOTOR_ON=1;
            //}
            //else if(esc_press_test_count<(722))  /*ESV Valve open time : 0.5sec*/
            //{
            //    VDC_MOTOR_ON=0;
            //    LAESP_vResetValve();
            //    S_VALVE_RIGHT=1;
            //    S_VALVE_LEFT=1;
            //}
            
            else if(esc_press_test_count<(1950))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }    
#else
		if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(650))  /*FR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(850))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(910))  /*FL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=0;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;                        
								#endif                     
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1110))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1170))  /*RL rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                   
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1370))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1430))  /*RR rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=0;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;    						    
                #if (__MGH_80_10MS == ENABLE)    						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						                        
								#endif   						                        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1630))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1680))  /*ESV Valve open time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(1880))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1940))  /*LEFT side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=1;    
                #if (__MGH_80_10MS == ENABLE)    
    						LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;    						        						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;                    
								#endif                  
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2140))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2200))  /*Right side rise time : 0.6sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=1;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=0;
                #if (__MGH_80_10MS == ENABLE)
    						LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;    						        
								#endif    						        
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(2400))  /*reset time : 2sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(2900))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#endif
#elif __VALVE_TEST_B
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;
        if(esc_press_test_count>140)
        {
            if(esc_press_test_count<420)
            {
                TCL_DEMAND_fl=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<980)
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<1260)
            {
                TCL_DEMAND_fr=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                VDC_MOTOR_ON=1;

            }
            else if(esc_press_test_count<1820)
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<2100)
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=1;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=1;
                AV_VL_rr=0;
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<2660)
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<2940)
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
                HV_VL_fl=1;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                VDC_MOTOR_ON=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#elif __BLEEDING_E
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;
        if(esc_press_test_count>140)
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<1120)
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<1400)
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_RIGHT=1;
                S_VALVE_LEFT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#elif __VALVE_TEST_CPS
#if !__SPLIT_TYPE
	if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(L_U8_TIME_10MSLOOP_300MS))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            
            if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1200MS))
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1750MS))
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1960MS))
            {
                /*prefill*/
                TCL_DEMAND_fl=0;
                TCL_DEMAND_fr=0;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=1;
                #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP; 
								LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;
								LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;  
								#endif    						    
                VDC_MOTOR_ON=1; 
                PREFILL_TEST_FLAG = 1;              
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_4S))
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                VDC_MOTOR_ON=1;
                PREFILL_TEST_FLAG = 0;  
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_5S))
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_6S))
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#else
	if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(L_U8_TIME_10MSLOOP_300MS))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            
            if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1200MS))
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1750MS))
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(L_U8_TIME_10MSLOOP_1960MS))
            {
                /*prefill*/
                TCL_DEMAND_PRIMARY=0;
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=1;
                HV_VL_fr=0;
                AV_VL_fr=1;
                HV_VL_rl=0;
                AV_VL_rl=1;
                HV_VL_rr=0;
                AV_VL_rr=1;
                #if (__MGH_80_10MS == ENABLE)
                LAVALVETEST_vGenerateLFCDutyWL(&FL, &lavalvetest_FL1HP, &lavalvetest_FL2HP);
    						la_FL1HP = lavalvetest_FL1HP; 
								la_FL2HP = lavalvetest_FL2HP; 
								LAVALVETEST_vGenerateLFCDutyWL(&FR, &lavalvetest_FR1HP, &lavalvetest_FR2HP);
    						la_FR1HP = lavalvetest_FR1HP; 
								la_FR2HP = lavalvetest_FR2HP;
								LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;       						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;  
								#endif    						    
                VDC_MOTOR_ON=1; 
                PREFILL_TEST_FLAG = 1;              
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_4S))
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=0;
                AV_VL_rl=0;
                HV_VL_rr=0;
                AV_VL_rr=0;
                VDC_MOTOR_ON=1;
                PREFILL_TEST_FLAG = 0;  
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_5S))
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(L_U16_TIME_10MSLOOP_6S))
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#endif
#elif __VALVE_TEST_F
#if !__SPLIT_TYPE
    if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=S_VALVE_RIGHT=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1090))  /*FL/FR rise time : 5sec*/
            {
                TCL_DEMAND_fl=1;
                TCL_DEMAND_fr=1;
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=1;
                AV_VL_rr=1;
                #if (__MGH_80_10MS == ENABLE)  						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;
								LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;  
								#endif  						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1140))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1640))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_LEFT=1;
                S_VALVE_RIGHT=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#else
if(fu1ESCDisabledBySW==1)
    {
        esc_press_test_count=esc_press_test_count+1;

        if(esc_press_test_count>(40))
        {
            LAESP_vResetValve();
            VDC_MOTOR_ON=0;
            if(esc_press_test_count<(540))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=S_VALVE_SECONDARY=1;
            }
            else if(esc_press_test_count<(590))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1090))  /*FL rise time : 5sec*/
            {
                TCL_DEMAND_PRIMARY=1;
                TCL_DEMAND_SECONDARY=1;
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
                HV_VL_fl=0;
                AV_VL_fl=0;
                HV_VL_fr=0;
                AV_VL_fr=0;
                HV_VL_rl=1;
                AV_VL_rl=1;
                HV_VL_rr=1;
                AV_VL_rr=1;
                #if (__MGH_80_10MS == ENABLE)  						    
    						LAVALVETEST_vGenerateLFCDutyWL(&RL, &lavalvetest_RL1HP, &lavalvetest_RL2HP);
    						la_RL1HP = lavalvetest_RL1HP; 
								la_RL2HP = lavalvetest_RL2HP;
								LAVALVETEST_vGenerateLFCDutyWL(&RR, &lavalvetest_RR1HP, &lavalvetest_RR2HP);
    						la_RR1HP = lavalvetest_RR1HP; 
								la_RR2HP = lavalvetest_RR2HP;  
								#endif  						      						
                VDC_MOTOR_ON=1;
            }
            else if(esc_press_test_count<(1140))  /*reset time : 0.5sec*/
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
            else if(esc_press_test_count<(1640))  /*ESV Valve open time : 5sec*/
            {
                S_VALVE_PRIMARY=1;
                S_VALVE_SECONDARY=1;
            }
            else
            {
                VDC_MOTOR_ON=0;
                LAESP_vResetValve();
            }
        }
        else
        {
            VDC_MOTOR_ON=0;
            LAESP_vResetValve();
        }
    }
    else
    {
        esc_press_test_count=0;
        VDC_MOTOR_ON=0;
        LAESP_vResetValve();
    }
#endif
#endif
}

void LAESP_vResetVdc(void)
{
    ESP_ON=0;
    YAW_CDC_WORK=0;
  #if __HDC
    if(lcu1HdcActiveFlg==0) {VDC_MOTOR_ON=0;}
  #else
    VDC_MOTOR_ON=0;
  #endif

/*******************/
/*Shinae 06' China Winter*/
    if(FLAG_ACTIVE_BRAKING ==0)
    {
/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
        TCL_DEMAND_fl=TCL_DEMAND_fr=0;
        CDC_WORK_OUT=1;
        S_VALVE_RIGHT=0;
        S_VALVE_LEFT=0;
#else
        TCL_DEMAND_PRIMARY=TCL_DEMAND_SECONDARY=0;
        CDC_WORK_OUT=1;
        S_VALVE_PRIMARY=0;
        S_VALVE_SECONDARY=0;
#endif
/*060607 modified and added for FR Split by eslim*/
    }
    else
    {
        ;
    }
}

void LAESP_vCallCombination(void)
{
#if !__SPLIT_TYPE
    #define esp_ebd_comb_rise_scan 5
#else
    #define esp_ebd_comb_rise_scan_front  4
    #define esp_ebd_comb_rise_scan_rear  5
#endif

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lis16DriverIntendedTP > MPRESS_0G5BAR)
#else
    if((MPRESS_BRAKE_ON==1)
#endif    
        ||((ESP_BRAKE_CONTROL_FL==1)&&(BTCS_rr==1))
        ||((ESP_BRAKE_CONTROL_FR==1)&&(BTCS_rl==1))
        ||((ESP_BRAKE_CONTROL_RL==1)&&(BTCS_fr==1))
        ||((ESP_BRAKE_CONTROL_RR==1)&&(BTCS_fl==1))
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __DEC
        ||(lcu1DecActiveFlg==1)
#endif
#if __TSP
        ||(TSP_ON==1)
#endif
    )
    {
        ESP_COMBINATION_CONTROL = 1;
    }
    else
    {
        ESP_COMBINATION_CONTROL = 0;
    }
/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE /* X-Split */
    if(ESP_COMBINATION_CONTROL==1)
    {
        /* 061018 modified by eslim for reset problem */
        CROSS_SLIP_CONTROL_fl=0;
        CROSS_SLIP_CONTROL_fr=0;
        CROSS_SLIP_CONTROL_rl=0;
        CROSS_SLIP_CONTROL_rr=0;
        /* 061018 modified by eslim for reset problem */
        FL.U1EscOtherWheelCnt = 0;
        FR.U1EscOtherWheelCnt = 0;
        RL.U1EscOtherWheelCnt = 0;
        RR.U1EscOtherWheelCnt = 0;
        /* 061018 modified by eslim for reset problem */
        
        if((ESP_BRAKE_CONTROL_FL==1)&&(SLIP_CONTROL_NEED_RR==0))    /* Control Wheel: FL Cross Wheel: RR */
        {
            LAESP_vActuateCrossWheelOS(&RR_ESC, &FL_ESC_COMB, &RR);
            #if(__IDB_LOGIC==ENABLE)
            if(ESP_BRAKE_CONTROL_FR==0)
            {
                LAESP_vActuateOhterWheelOS(&FL_ESC,&FR,&FR);
            }
            else
            {
                ;   
            }
            
            if(ESP_BRAKE_CONTROL_RL==0)
            {
                LAESP_vActuateOhterWheelOS(&FL_ESC,&RL,&RL);
            }
            else
            {
                ;   
            }
            #endif /* __IDB_LOGIC */        
        }
        else
        {
            ;
        }
        
        if((ESP_BRAKE_CONTROL_FR==1)&&(SLIP_CONTROL_NEED_RL==0))    /* Control Wheel: FR Cross Wheel: RL */
        {
            LAESP_vActuateCrossWheelOS(&RL_ESC, &FR_ESC_COMB, &RL);
            #if(__IDB_LOGIC==ENABLE)
            if(ESP_BRAKE_CONTROL_FL==0)
            {
                LAESP_vActuateOhterWheelOS(&FR_ESC,&FL,&FL);
            }
            else
            {
                ;   
            }
            
            if(ESP_BRAKE_CONTROL_RR==0)
            {
                LAESP_vActuateOhterWheelOS(&FR_ESC,&RR,&RR);
            }
            else
            {
                ;   
            }
            #endif /* __IDB_LOGIC */        
        }
        else
        {
            ;
        }
                
        if((ESP_BRAKE_CONTROL_RL==1)&&(SLIP_CONTROL_NEED_FR==0))    /* Control Wheel: RL Cross Wheel: FR */
        {
            LAESP_vActuateCrossWheelUS(&FR_ESC, &FR);
            #if(__IDB_LOGIC==ENABLE)
            if(ESP_BRAKE_CONTROL_FL==0)
            {
                LAESP_vActuateOhterWheelUS(&RL_ESC,&FL,&FL);
            }
            else
            {
                ;   
            }
            
            if(ESP_BRAKE_CONTROL_RR==0)
            {
                LAESP_vActuateOhterWheelUS(&RL_ESC,&RR,&RR);
            }
            else
            {
                ;   
            }
            #endif /* __IDB_LOGIC */  
        }
        else
        {
            ;
        }
             
        if((ESP_BRAKE_CONTROL_RR==1)&&(SLIP_CONTROL_NEED_FL==0))    /* Control Wheel: RR Cross Wheel: FL */
        {
            LAESP_vActuateCrossWheelUS(&FL_ESC, &FL);
            #if(__IDB_LOGIC==ENABLE)
            if(ESP_BRAKE_CONTROL_FR==0)
            {
                LAESP_vActuateOhterWheelUS(&RR_ESC,&FR,&FR);
            }
            else
            {
                ;   
            }
            
            if(ESP_BRAKE_CONTROL_RL==0)
            {
                LAESP_vActuateOhterWheelUS(&RR_ESC,&RL,&RL);
            }
            else
            {
                ;   
            }
            #endif /* __IDB_LOGIC */  
        }
        else
        {
            ;
        } 
      
    }
    else
    {
	#if(__IDB_LOGIC==ENABLE)
	    CROSS_SLIP_CONTROL_fl=0;
        CROSS_SLIP_CONTROL_fr=0;
        CROSS_SLIP_CONTROL_rl=0;
        CROSS_SLIP_CONTROL_rr=0;
        
        FL.U1EscOtherWheelCnt = 0;
        FR.U1EscOtherWheelCnt = 0;
        RL.U1EscOtherWheelCnt = 0;
        RR.U1EscOtherWheelCnt = 0;
        
        FL.lcesps16IdbOtherPress =0;
        FR.lcesps16IdbOtherPress =0;
        RL.lcesps16IdbOtherPress =0;
        RR.lcesps16IdbOtherPress =0;
		if((ESP_BRAKE_CONTROL_FL==1)&&(SLIP_CONTROL_NEED_RR==0))
        {
            HV_VL_rr=1;
            AV_VL_rr=1;
            CROSS_SLIP_CONTROL_rr = 1;  /* Opposite wheel: RR */
			LA_vSetWheelVvOnOffDumpOutput(&la_RR1HP, &la_RR2HP, U8_BASE_LOOPTIME);
            
//			if((ESP_PULSE_DUMP_FL==1)&&(SLIP_CONTROL_NEED_FR==1)) 
			if((SLIP_CONTROL_NEED_FR==1)||(BTCS_fr==1))
			{
				;	/* Non-control wheel: FL->Fadeout, ESC control priority: FR */
			}
			else
			{
				CROSS_SLIP_CONTROL_fr = 0;  /* Non-control wheel */
				LA_vSetWheelVvOnOffDumpOutput(&la_FR1HP, &la_FR2HP, U8_BASE_LOOPTIME);
				HV_VL_fr=1;
				AV_VL_fr=1;
			}
			
			if((SLIP_CONTROL_NEED_RL==1)||(BTCS_rl==1))
			{
				;	/* Non-control wheel: FL->Fadeout, ESC control priority: RL */
			}
			else
			{
            	CROSS_SLIP_CONTROL_rl = 0;  /* Non-control wheel */
				LA_vSetWheelVvOnOffDumpOutput(&la_RL1HP, &la_RL2HP, U8_BASE_LOOPTIME);
				HV_VL_rl=1;
            	AV_VL_rl=1;
			}
        }
		else if((ESP_BRAKE_CONTROL_FL==1)&&(SLIP_CONTROL_NEED_RR==1))
        {
            CROSS_SLIP_CONTROL_rr = 0;
        }
        else
        {
            ;
        }

		if((ESP_BRAKE_CONTROL_FR==1)&&(SLIP_CONTROL_NEED_RL==0))
        {
            HV_VL_rl=1;
            AV_VL_rl=1;
            CROSS_SLIP_CONTROL_rl = 1;  /* Opposite wheel: RL */
			LA_vSetWheelVvOnOffDumpOutput(&la_RL1HP, &la_RL2HP, U8_BASE_LOOPTIME);

			if((SLIP_CONTROL_NEED_FL==1)||(BTCS_fl==1)) 
			{
				;	/* Non-control wheel: FR->Fadeout, ESC control priority: FL */
			}
			else
			{
				CROSS_SLIP_CONTROL_fl = 0;  /* Non-control wheel */
				LA_vSetWheelVvOnOffDumpOutput(&la_FL1HP, &la_FL2HP, U8_BASE_LOOPTIME);
				HV_VL_fl=1;
				AV_VL_fl=1;
			}
            
            if((SLIP_CONTROL_NEED_RR==1)||(BTCS_rr==1)) 
			{
				;	/* Non-control wheel: FR->Fadeout, ESC control priority: RR */
			}
			else
			{
				CROSS_SLIP_CONTROL_rr = 0;  /* Non-control wheel */
				LA_vSetWheelVvOnOffDumpOutput(&la_RR1HP, &la_RR2HP, U8_BASE_LOOPTIME);
				HV_VL_rr=1;
            	AV_VL_rr=1;
			}
        }
		else if((ESP_BRAKE_CONTROL_FR==1)&&(SLIP_CONTROL_NEED_RL==1))
        {
            CROSS_SLIP_CONTROL_rr = 0;
        }
        else
        {
            ;
        }

		if((ESP_BRAKE_CONTROL_RL == 1)&&(SLIP_CONTROL_NEED_FR==0))
        {
            HV_VL_fr=1;
            AV_VL_fr=1;
            CROSS_SLIP_CONTROL_fr = 1;  /* 대각휠 */
			LA_vSetWheelVvOnOffDumpOutput(&la_FR1HP, &la_FR2HP, U8_BASE_LOOPTIME);
            
            if((SLIP_CONTROL_NEED_FL==1)||(BTCS_fl==1))
            {
                ;
            }
            else
            {
    			CROSS_SLIP_CONTROL_fl = 1;  /* 비제어휠 */
    			LA_vSetWheelVvOnOffDumpOutput(&la_FL1HP, &la_FL2HP, U8_BASE_LOOPTIME);
    			HV_VL_fl=1;
                AV_VL_fl=1;
            }
			
			CROSS_SLIP_CONTROL_rr = 1;  /* 비제어휠 */
			LA_vSetWheelVvOnOffDumpOutput(&la_RR1HP, &la_RR2HP, U8_BASE_LOOPTIME);  
            HV_VL_rr=1;
            AV_VL_rr=1;
        }
        else if((ESP_BRAKE_CONTROL_RL==1)&&(SLIP_CONTROL_NEED_FR==1))
        {
            CROSS_SLIP_CONTROL_fr = 0;
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_RR == 1)&&(SLIP_CONTROL_NEED_FL==0))
        {
            HV_VL_fl=1;
            AV_VL_fl=1;
            CROSS_SLIP_CONTROL_fl = 1;  /* 대각휠 */
			LA_vSetWheelVvOnOffDumpOutput(&la_FL1HP, &la_FL2HP, U8_BASE_LOOPTIME);
			
            if((SLIP_CONTROL_NEED_FR==1)||(BTCS_fr==1))
            {
                ;
            }
            else
            {
                HV_VL_fr=1;
                AV_VL_fr=1;
                CROSS_SLIP_CONTROL_fr = 1;  /* 비제어휠 */
    			LA_vSetWheelVvOnOffDumpOutput(&la_FR1HP, &la_FR2HP, U8_BASE_LOOPTIME);  
			}
            HV_VL_rl=1;
            AV_VL_rl=1;
            CROSS_SLIP_CONTROL_rl = 1;  /* 비제어휠 */
			LA_vSetWheelVvOnOffDumpOutput(&la_RL1HP, &la_RL2HP, U8_BASE_LOOPTIME);
        }
        else if((ESP_BRAKE_CONTROL_RR==1)&&(SLIP_CONTROL_NEED_FL==1))
        {
            CROSS_SLIP_CONTROL_fl = 0;
        }
        else
        {
            ;
        }
	#else/*IDB_LOGIC*/
		if((ESP_BRAKE_CONTROL_FL == 1)&&(SLIP_CONTROL_NEED_RR==0))
        {
            HV_VL_rr=1;
            AV_VL_rr=1;
            CROSS_SLIP_CONTROL_rr = 1;
        }
        else if((ESP_BRAKE_CONTROL_FL==1)&&(SLIP_CONTROL_NEED_RR==1))
        {
            CROSS_SLIP_CONTROL_rr = 0;
        }
        else
        {
            ;
        }
			
        if((ESP_BRAKE_CONTROL_FR == 1)&&(SLIP_CONTROL_NEED_RL==0))
        {
            HV_VL_rl=1;
            AV_VL_rl=1;
            CROSS_SLIP_CONTROL_rl = 1;
        }
        else if((ESP_BRAKE_CONTROL_FR==1)&&(SLIP_CONTROL_NEED_RL==1))
        {
            CROSS_SLIP_CONTROL_rl = 0;
        }
        else
        {
            ;
        }
        
        if((ESP_BRAKE_CONTROL_RL == 1)&&(SLIP_CONTROL_NEED_FR==0))
        {
            HV_VL_fr=1;
            AV_VL_fr=1;
            CROSS_SLIP_CONTROL_fr = 1;
        }
        else if((ESP_BRAKE_CONTROL_RL==1)&&(SLIP_CONTROL_NEED_FR==1))
        {
            CROSS_SLIP_CONTROL_fr = 0;
        }
        else
        {
            ;
        }
			
        if((ESP_BRAKE_CONTROL_RR == 1)&&(SLIP_CONTROL_NEED_FL==0))
        {
            HV_VL_fl=1;
            AV_VL_fl=1;
            CROSS_SLIP_CONTROL_fl = 1;
        }
        else if((ESP_BRAKE_CONTROL_RR==1)&&(SLIP_CONTROL_NEED_FL==1))
        {
            CROSS_SLIP_CONTROL_fl = 0;
        }
        else
        {
            ;
        }
	#endif
    }
#else /* FR-split */
    if(ESP_COMBINATION_CONTROL==1)
    {
        /* 061018 modified by eslim for reset problem */
        CROSS_SLIP_CONTROL_fl=0;
        CROSS_SLIP_CONTROL_fr=0;
        CROSS_SLIP_CONTROL_rl=0;
        CROSS_SLIP_CONTROL_rr=0;
        /* 061018 modified by eslim for reset problem */
        if((ESP_BRAKE_CONTROL_FL==1)&&(SLIP_CONTROL_NEED_FR==0))
        {
            if(ABS_fz == 0)
            {
                if(ESP_BRAKE_CONTROL_FR == 0)
                {
                    if(AV_VL_fr==0)
                    {
                          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                          #else
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                          #endif
                        {
                            HV_VL_fr=1;
                            AV_VL_fr=0;
                            FR.flags=237;
                            CROSS_SLIP_CONTROL_fr=1;
                        }
                        else if(INITIAL_BRAKE==1)
                        {
                            ;
                        }
                        else
                        {
                            HV_VL_fr=1;
                            AV_VL_fr=0;
                            FR.flags=237;
                            CROSS_SLIP_CONTROL_fr=1;
                        }
                    }
                    else
                    {
                          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                          #else
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                          #endif
                        {
                            HV_VL_fr=1;
                            AV_VL_fr=1;
                            FR.flags=237;
                            CROSS_SLIP_CONTROL_fr=1;
                        }
                        else
                        {
                            ;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            else
            {
                if(PARTIAL_BRAKE == 1)
                {

                    if(ESP_BRAKE_CONTROL_FR == 0)
                    {
                        if(AV_VL_fr==0)
                        {
                              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                            if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                              #else
                            if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                              #endif
                            {
                                HV_VL_fr=1;
                                AV_VL_fr=0;
                                FR.flags=237;
                                CROSS_SLIP_CONTROL_fr=1;
                            }
                            else
                            {
                                HV_VL_fr=1;
                                AV_VL_fr=0;
                                FR.flags=237;
                                CROSS_SLIP_CONTROL_fr=1;
                            }
                        }
                        else
                        {
                            HV_VL_fr=1;
                            AV_VL_fr=1;
                            FR.flags=237;
                            CROSS_SLIP_CONTROL_fr=1;
                        }
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    ;
                }
            }
            
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if((PARTIAL_BRAKE == 1)&&(lsespu1AHBGEN3MpresBrkOn==1)
                &&(ESP_PULSE_DUMP_FL)&&(ESP_BRAKE_CONTROL_FR == 0)
                &&(lsesps16AHBGEN3mpress >= MPRESS_10BAR))  
              #else
            if((PARTIAL_BRAKE == 1)&&(MPRESS_BRAKE_ON==1)
                &&(ESP_PULSE_DUMP_FL)&&(ESP_BRAKE_CONTROL_FR == 0)
                &&(mpress >= MPRESS_10BAR))
              #endif  
            {
                 esp_tempW5 = esp_ebd_comb_count%esp_ebd_comb_rise_scan_front ;
                 esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, 1500, 2000, 2000 );
                 if(esp_tempW5 <1 )
                 {
                     HV_VL_fr = 0;
                     AV_VL_fr = 0;
                     FR.flags=237;
                     CROSS_SLIP_CONTROL_fr=1;
                 }
                 else if(slip_m_fr >= (-WHEEL_SLIP_4))
                 {
                     CROSS_SLIP_CONTROL_fr=0;
                 }
#if __PRESS_EST_ACTIVE
                 else if ( FR.s16_Estimated_Active_Press >= FL.s16_Estimated_Active_Press)
                 {
                     HV_VL_fr = 1;
                     AV_VL_fr = 0;
                     FR.flags=237;
                     CROSS_SLIP_CONTROL_fr=1;
                 }
#endif
                 else if(slip_m_fr <= (-esp_tempW6))
                 {
                     HV_VL_fr = 1;
                     AV_VL_fr = 1;
                     FR.flags=237;
                     CROSS_SLIP_CONTROL_fr=1;
                 }
                 else
                 {
                     HV_VL_fr = 1;
                     AV_VL_fr = 0;
                     FR.flags=237;
                    CROSS_SLIP_CONTROL_fr=1;
                 }
                 esp_ebd_comb_count=esp_ebd_comb_count+1;
            }
            else
            {
                esp_ebd_comb_count = 0;
            }
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_FR==1)&&(SLIP_CONTROL_NEED_FL==0))
        {
            if(ABS_fz == 0)
            {

                if(ESP_BRAKE_CONTROL_FL==0)
                {
                    if(AV_VL_fl==0)
                    {
                          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                          #else 
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                          #endif
                        {
                            HV_VL_fl=1;
                            AV_VL_fl=0;
                            FL.flags=237;
                            CROSS_SLIP_CONTROL_fl=1;
                        }
                        else if(INITIAL_BRAKE==1)
                        {
                            ;
                        }
                        else
                        {
                            HV_VL_fl=1;
                            AV_VL_fl=0;
                            FL.flags=237;
                            CROSS_SLIP_CONTROL_fl=1;
                        }
                    }
                    else
                    {
                          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                          #else
                        if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                          #endif
                        {
                            HV_VL_fl=1;
                            AV_VL_fl=1;
                            FL.flags=237;
                            CROSS_SLIP_CONTROL_fl=1;
                        }
                        else
                        {
                            ;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            else
            {
                if(PARTIAL_BRAKE == 1)
                {

                    if(ESP_BRAKE_CONTROL_FL==0)
                    {

                        if(AV_VL_fl==0)
                        {
                              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                            if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
                              #else
                            if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
                              #endif
                            {
                                HV_VL_fl=1;
                                AV_VL_fl=0;
                                FL.flags=237;
                                CROSS_SLIP_CONTROL_fl=1;
                            }
                            else
                            {
                                HV_VL_fl=1;
                                AV_VL_fl=0;
                                FL.flags=237;
                                CROSS_SLIP_CONTROL_fl=1;
                            }
                         }
                         else
                         {
                            HV_VL_fl=1;
                            AV_VL_fl=1;
                            FL.flags=237;
                            CROSS_SLIP_CONTROL_fl=1;
                         }
                     }
                     else
                     {
                        ;
                     }
                 }
                 else
                 {
                    ;
                 }
            }
              #if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if((PARTIAL_BRAKE == 1)&&(lsespu1AHBGEN3MpresBrkOn==1)
                &&(ESP_PULSE_DUMP_FR)&&(ESP_BRAKE_CONTROL_FL == 0)
                &&(lsesps16AHBGEN3mpress >= MPRESS_10BAR)) 
              #else
            if((PARTIAL_BRAKE == 1)&&(MPRESS_BRAKE_ON==1)
                &&(ESP_PULSE_DUMP_FR)&&(ESP_BRAKE_CONTROL_FL == 0)
                &&(mpress >= MPRESS_10BAR))
              #endif  
            {
                 esp_tempW5 = esp_ebd_comb_count%esp_ebd_comb_rise_scan_front ;
                 esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, 1500, 2000, 2000 );
                 if(esp_tempW5 <1 )
                 {
                     HV_VL_fl = 0;
                     AV_VL_fl = 0;
                     FL.flags=237;
                     CROSS_SLIP_CONTROL_fl=1;
                 }
                 else if(slip_m_fl >= (-WHEEL_SLIP_4))
                 {
                     CROSS_SLIP_CONTROL_fl=0;
                 }

#if __PRESS_EST_ACTIVE
                 else if ( FL.s16_Estimated_Active_Press >= FR.s16_Estimated_Active_Press)
                 {
                     HV_VL_fl = 1;
                     AV_VL_fl = 0;
                     FL.flags=237;
                     CROSS_SLIP_CONTROL_fl=1;
                 }
#endif
                 else if(slip_m_fl <= (-esp_tempW6))
                 {
                     HV_VL_fl = 1;
                     AV_VL_fl = 1;
                     FL.flags=237;
                     CROSS_SLIP_CONTROL_fl=1;
                 }
                 else
                 {
                     HV_VL_fl = 1;
                     AV_VL_fl = 0;
                     FL.flags=237;
                     CROSS_SLIP_CONTROL_fl=1;
                 }


                 esp_ebd_comb_count=esp_ebd_comb_count+1;
            }
            else
            {
                  esp_ebd_comb_count = 0;
            }
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_RL==1)&&(SLIP_CONTROL_NEED_RR==0))
        {
            if(ABS_fz == 0)
            {

                if(ESP_BRAKE_CONTROL_RR==0)
                {
                    if(AV_VL_rr==0)
                    {
                        HV_VL_rr=1;
                        AV_VL_rr=0;
                        RR.flags=237;
                        CROSS_SLIP_CONTROL_rr=1;
                    }
                    else
                    {
                        HV_VL_rr=1;
                        AV_VL_rr=1;
                        RR.flags=237;
                        CROSS_SLIP_CONTROL_rr=1;
                    }
                }
                else
                {
                    ;
                }

            }
            else
            {
                if(PARTIAL_BRAKE==1)
                {

                    if(ESP_BRAKE_CONTROL_RR==0)
                    {
                        if(AV_VL_rr==0)
                        {
                            HV_VL_rr=1;
                            AV_VL_rr=0;
                            RR.flags=237;
                            CROSS_SLIP_CONTROL_rr=1;
                        }
                        else
                        {
                            HV_VL_rr=1;
                            AV_VL_rr=1;
                            RR.flags=237;
                            CROSS_SLIP_CONTROL_rr=1;
                        }
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    ;
                }
            }
            
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if((PARTIAL_BRAKE == 1)&&(lsespu1AHBGEN3MpresBrkOn==1)
                &&(ESP_PULSE_DUMP_RL)&&(ESP_BRAKE_CONTROL_RR == 0)
                &&(lsesps16AHBGEN3mpress >= MPRESS_10BAR))  
              #else
            if((PARTIAL_BRAKE == 1)&&(MPRESS_BRAKE_ON==1)
                &&(ESP_PULSE_DUMP_RL)&&(ESP_BRAKE_CONTROL_RR == 0)
                &&(mpress >= MPRESS_10BAR))
              #endif  
            {
                 esp_tempW5 = esp_ebd_comb_count%esp_ebd_comb_rise_scan_rear ;
                 esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, 1500, 2000, 2000 );
                 if(esp_tempW5 <1 )
                 {
                     HV_VL_rr = 0;
                     AV_VL_rr= 0;
                     RR.flags=237;
                     CROSS_SLIP_CONTROL_rr=1;
                 }
                 else if(slip_m_rr >= (-WHEEL_SLIP_4))
                 {
                     CROSS_SLIP_CONTROL_rr=0;
                 }

#if __PRESS_EST_ACTIVE
                 else if ( RR.s16_Estimated_Active_Press >= RL.s16_Estimated_Active_Press)
                 {
                     HV_VL_rr = 1;
                     AV_VL_rr = 0;
                     RR.flags=237;
                     CROSS_SLIP_CONTROL_rr=1;
                 }
#endif
                 else if(slip_m_rr <= (-esp_tempW6))
                 {
                     HV_VL_rr = 1;
                     AV_VL_rr = 1;
                     RR.flags=237;
                     CROSS_SLIP_CONTROL_rr=1;
                 }
                 else
                 {
                     HV_VL_rr = 1;
                     AV_VL_rr = 0;
                     RR.flags=237;
                     CROSS_SLIP_CONTROL_rr=1;
                 }

                 esp_ebd_comb_count=esp_ebd_comb_count+1;
            }
            else
            {
                  esp_ebd_comb_count = 0;
            }
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_RR==1)&&(SLIP_CONTROL_NEED_RL==0))
        {
            if(ABS_fz==0)
            {

                if(ESP_BRAKE_CONTROL_RL==0)
                {
                    if(AV_VL_rl==0)
                    {
                        HV_VL_rl=1;
                        AV_VL_rl=0;
                        RL.flags=237;
                        CROSS_SLIP_CONTROL_rl=1;
                    }
                    else
                    {
                        HV_VL_rl=1;
                        AV_VL_rl=1;
                        RL.flags=237;
                        CROSS_SLIP_CONTROL_rl=1;
                    }
                }
                else
                {
                    ;
                }
            }
            else
            {
                if(PARTIAL_BRAKE==1)
                {

                    if(ESP_BRAKE_CONTROL_RL==0)
                    {
                        if(AV_VL_rl==0)
                        {
                            HV_VL_rl=1;
                            AV_VL_rl=0;
                            RL.flags=237;
                            CROSS_SLIP_CONTROL_rl=1;
                        }
                        else
                        {
                            HV_VL_rl=1;
                            AV_VL_rl=1;
                            RL.flags=237;
                            CROSS_SLIP_CONTROL_rl=1;
                        }
                    }
                    else
                    {
                        ;
                    }
                 }
                 else
                 {
                    ;
                 }
            }
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if((PARTIAL_BRAKE == 1)&&(lsespu1AHBGEN3MpresBrkOn==1)
                &&(ESP_PULSE_DUMP_RR)&&(ESP_BRAKE_CONTROL_RL == 0)
                &&(lsesps16AHBGEN3mpress >= MPRESS_10BAR))  
              #else  
            if((PARTIAL_BRAKE == 1)&&(MPRESS_BRAKE_ON==1)
                &&(ESP_PULSE_DUMP_RR)&&(ESP_BRAKE_CONTROL_RL == 0)
                &&(mpress >= MPRESS_10BAR))
              #endif  
            {
                 esp_tempW5 = esp_ebd_comb_count%esp_ebd_comb_rise_scan_rear ;
                 esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, 1500, 2000, 2000 );
                 if(esp_tempW5 <1 )
                 {
                     HV_VL_rl = 0;
                     AV_VL_rl = 0;
                     RL.flags=237;
                     CROSS_SLIP_CONTROL_rl=1;
                 }
                 else if(slip_m_rl >= (-WHEEL_SLIP_4))
                 {
                     CROSS_SLIP_CONTROL_rl=0;
                 }

#if __PRESS_EST_ACTIVE
                 else if ( RL.s16_Estimated_Active_Press >= RR.s16_Estimated_Active_Press)
                 {
                     HV_VL_rl = 1;
                     AV_VL_rl = 0;
                     RL.flags=237;
                     CROSS_SLIP_CONTROL_rl=1;
                 }
#endif
                 else if(slip_m_rl <= (-esp_tempW6))
                 {
                     HV_VL_rl = 1;
                     AV_VL_rl = 1;
                     RL.flags=237;
                     CROSS_SLIP_CONTROL_rl=1;
                 }
                 else
                 {
                     HV_VL_rl = 1;
                     AV_VL_rl = 0;
                     RL.flags=237;
                     CROSS_SLIP_CONTROL_rl=1;
                 }


                 esp_ebd_comb_count=esp_ebd_comb_count+1;
            }
            else
            {
                  esp_ebd_comb_count = 0;
            }

        }
        else
        {
            ;
        }
    }
    else
    {
        if((ESP_BRAKE_CONTROL_FL)&&(SLIP_CONTROL_NEED_FR==0))
        {
            HV_VL_fr=1;
            AV_VL_fr=1;
            FR.flags = 237;
            CROSS_SLIP_CONTROL_fr = 1;
            #if defined(__ADDON_TCMF)
            if((Booster_Act==1)||(FLAG_ACTIVE_BOOSTER_PRE==1))
            {
                if((AV_VL_rl==0)&&(ESP_BRAKE_CONTROL_RL==0))
                {
                    HV_VL_rl=1;
                    AV_VL_rl=1;
                }
                if(BTCS_rr==1)
                {
                    ;
                }
                else
                {
                    HV_VL_rr=1;
                    AV_VL_rr=1;
                }
            }
            #endif
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_FR)&&(SLIP_CONTROL_NEED_FL==0))
        {
            HV_VL_fl=1;
            AV_VL_fl=1;
            FL.flags = 237;           
            CROSS_SLIP_CONTROL_fl = 1;
            #if defined(__ADDON_TCMF)
            if((Booster_Act==1)||(FLAG_ACTIVE_BOOSTER_PRE==1))
            {
                if((AV_VL_rr==0)&&(ESP_BRAKE_CONTROL_RR==0))
                {
                    HV_VL_rr=1;
                    AV_VL_rr=1;
                }
                if(BTCS_rl==1)
                {
                    ;
                }
                else
                {
                    HV_VL_rl=1;
                    AV_VL_rl=1;
                }
            }
            #endif
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_RL)&&(SLIP_CONTROL_NEED_RR==0)&&(lcu1US3WHControlFlag==0))
        {
            HV_VL_rr=1;
            AV_VL_rr=1;
            RR.flags = 237;           
            CROSS_SLIP_CONTROL_rr = 1;
        }
        else
        {
            ;
        }

        if((ESP_BRAKE_CONTROL_RR)&&(SLIP_CONTROL_NEED_RL==0)&&(lcu1US3WHControlFlag==0))
        {
            HV_VL_rl=1;
            AV_VL_rl=1;
            RL.flags = 237;           
            CROSS_SLIP_CONTROL_rl = 1;
        }
        else
        {
            ;
        }
    }
#endif
/*060607 modified and added for FR Split by eslim*/
}
void LAESP_vActuateOhterWheelOS(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL_temp,struct W_STRUCT *WL)
{
	if( WL->lcesps16IdbOtherPressOld ==0)
	{
		WL->lcesps16IdbOtherPressOld =  WL->s16_Estimated_Active_Press;
	}
	else
    {
    	WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;						
	}
	
    WL->U1EscOtherWheelCnt = 1;

	
	if(WL->ABS == 1)
	{
		 WL->lcesps16IdbOtherPress = lcidbs16activebrakepress;	
	}
	else
	{
		WL->CROSS_SLIP_CONTROL=1;
		if(WL_ESC->esp_control_mode < 3)
		{
			if(WL_ESC->lcespu8IdbRiseRateCnt < 4)
			{				
				WL->lcesps16IdbOtherPress =  WL->lcesps16IdbOtherPressOld; 
				WL_temp->HV_VL=1;
		        WL_temp->AV_VL=0;				
			}
			else if((WL->EBD == 1)&&(WL->s16_Estimated_Active_Press > lcidbs16activebrakepress))
        {
				WL->lcesps16IdbOtherPress = WL->lcesps16IdbOtherPressOld;
            WL_temp->HV_VL=1;
            WL_temp->AV_VL=0;
        }
        else
        {
				if(WL->s16_Estimated_Active_Press > lcidbs16activebrakepress)
				{
					WL->lcesps16IdbOtherPress = WL->lcesps16IdbOtherPressOld;
					WL_temp->HV_VL=1;
			        WL_temp->AV_VL=0;				
				}
				else
				{
					WL->lcesps16IdbOtherPress = lcidbs16activebrakepress; 
					WL_temp->HV_VL=0;
			        WL_temp->AV_VL=0;
			    }			
        }
    }
		else
    {
			if(WL->s16_Estimated_Active_Press > lcidbs16activebrakepress)
        {
				WL->lcesps16IdbOtherPress = WL->lcesps16IdbOtherPressOld;
				WL_temp->HV_VL=1;
		        WL_temp->AV_VL=0;				
			}
			else
			{
				WL->lcesps16IdbOtherPress = lcidbs16activebrakepress; 
            WL_temp->HV_VL=0;
            WL_temp->AV_VL=0;
        }
		    
		    if(WL->EBD == 1)
			{
				if(WL->AV_VL == 1)
				{
					WL->lcesps16IdbOtherPress = lcidbs16activebrakepress;
					WL_temp->HV_VL=1;
		        	WL_temp->AV_VL=1;					
				}
        else
        {
            ;
        }
    }
		}
	}
	
   	if(WL->lcesps16IdbOtherPress >= WL->lcesps16IdbOtherPressOld)
    {
    	WL->lcesps16IdbOtherPressRte = S16_TARGET_PRESS_RATE_100BAR;
    }
    else
    {
    	WL->lcesps16IdbOtherPressRte = -S16_TARGET_PRESS_RATE_100BAR;
    }

    WL->lcesps16IdbDelOtherPress = WL->lcesps16IdbOtherPress - WL->lcesps16IdbOtherPressOld;
    
//    WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;
//    if(WL_ESC->lcespu8CntBbsEsc ==1)/*4 scan */
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=1;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                   
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==2)
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
// 
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                        
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==3)
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
// 
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                   
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==4)/*BTCS wheel structure*/
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        WL_temp->HV_VL=1;
//        WL_temp->AV_VL=0;   
//        WL->lcesps16IdbOtherPress = WL->s16_Estimated_Active_Press;         
//    }
//    else
//    {
//        WL->U1EscOtherWheelCnt = 0;
//        WL->lcesps16IdbOtherPress =lis16DriverIntendedTP;               
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }                       
//    }
//        
} 
void LAESP_vActuateOhterWheelUS(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL_temp,struct W_STRUCT *WL)
{
	if( WL->lcesps16IdbOtherPressOld ==0)
   	{
		WL->lcesps16IdbOtherPressOld =  WL->s16_Estimated_Active_Press;
    }
    else
    {
    	WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;						
    }
 
	WL->U1EscOtherWheelCnt = 1;

	if(ABS_fz == 1)
	{
		 WL->lcesps16IdbOtherPress = lcidbs16activebrakepress;	
    }
	else
    {
		WL->CROSS_SLIP_CONTROL=1;	
		if(WL_ESC->esp_control_mode < 3)
		{
			if(WL_ESC->lcespu8IdbRiseRateCnt < 4)
			{				
				WL->lcesps16IdbOtherPress =  WL->lcesps16IdbOtherPressOld; 
		        WL_temp->HV_VL=1;
		        WL_temp->AV_VL=0;   
    		}
		    else
		    {
				if(WL->s16_Estimated_Active_Press <lcidbs16activebrakepress )
		        {
					WL->lcesps16IdbOtherPress = lcidbs16activebrakepress; 
		            WL_temp->HV_VL=0;
		            WL_temp->AV_VL=0;
		        }
		        else
		        {
					WL->lcesps16IdbOtherPress =  WL->lcesps16IdbOtherPressOld; 
					WL_temp->HV_VL=1;
					WL_temp->AV_VL=0;	
		        }                       
		    }
		} 
		else
    	{
			if(WL->s16_Estimated_Active_Press <lcidbs16activebrakepress )
	        {
				WL->lcesps16IdbOtherPress = lcidbs16activebrakepress; 
	            WL_temp->HV_VL=0;
	            WL_temp->AV_VL=0;
	        }
	        else
	        {
			    WL->lcesps16IdbOtherPress =  WL->lcesps16IdbOtherPressOld; 
				WL_temp->HV_VL=1;
			    WL_temp->AV_VL=0;	
	        }
    	}
	}
	if(WL->lcesps16IdbOtherPress >= WL->lcesps16IdbOtherPressOld)
    {
    	WL->lcesps16IdbOtherPressRte = S16_TARGET_PRESS_RATE_100BAR;
    }
    else
    {
    	WL->lcesps16IdbOtherPressRte = -S16_TARGET_PRESS_RATE_100BAR;
    }
   
    WL->lcesps16IdbDelOtherPress = WL->lcesps16IdbOtherPress - WL->lcesps16IdbOtherPressOld;

//    WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;
//    if(WL_ESC->lcespu8CntBbsEsc ==1)/*4 scan */
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                   
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==2)
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
// 
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                        
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==3)
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }
// 
//        WL->lcesps16IdbOtherPress = lis16DriverIntendedTP;                   
//    }
//    else if(WL_ESC->lcespu8CntBbsEsc ==4)/*BTCS wheel structure*/
//    {
//        WL->U1EscOtherWheelCnt = 1;
//        WL_temp->HV_VL=1;
//        WL_temp->AV_VL=0;   
//        WL->lcesps16IdbOtherPress = WL->s16_Estimated_Active_Press;         
//    }
//    else
//    {
//        WL->U1EscOtherWheelCnt = 0;
//        WL->lcesps16IdbOtherPress =lis16DriverIntendedTP;               
//        if((PARTIAL_BRAKE == 1)&&(ABS_fz==0))
//        {
//            WL_temp->HV_VL=0;
//            WL_temp->AV_VL=0;
//        }
//        else
//        {
//            ;
//        }                          
//    }          
} 
void LAESP_vActuateAfterESCforIDB(void)
    {
	if((CROSS_SLIP_CONTROL_fl == 0)&&(CROSS_SLIP_CONTROL_old_fl==1))
        {
		 HV_VL_fl=1;
         AV_VL_fl=0;	
        }
        else
        {
            ;
        }
	if((CROSS_SLIP_CONTROL_fr == 0)&&(CROSS_SLIP_CONTROL_old_fr==1))
	{
		 HV_VL_fr=1;
         AV_VL_fr=0;	
    }
	else
    {
		;	
	}
	if((CROSS_SLIP_CONTROL_rl == 0)&&(CROSS_SLIP_CONTROL_old_rl==1))
        {
		 HV_VL_rl=1;
         AV_VL_rl=0;	
        }
        else
        {
            ;
        }
	if((CROSS_SLIP_CONTROL_rr == 0)&&(CROSS_SLIP_CONTROL_old_rr==1))
    {
		 HV_VL_rr=1;
         AV_VL_rr=0;	
    }
    else
    {
		;	
	}
}
void LAESP_vSetCtrlMode(void)
{
	FL_ESC.lcespu8CtrlMode = 0;	
	FR_ESC.lcespu8CtrlMode = 0;
	RL_ESC.lcespu8CtrlMode = 0;	
	RR_ESC.lcespu8CtrlMode = 0;
		
	if(ESP_BRAKE_CONTROL_FL==1)
	{
		FL_ESC.lcespu8CtrlMode = 1;	
		RR_ESC.lcespu8CtrlMode = 3;
		if(ESP_BRAKE_CONTROL_FR==1)
		{
			FR_ESC.lcespu8CtrlMode = 1;
		}
		else
		{
			FR_ESC.lcespu8CtrlMode = 4;
		}
		
		if(ESP_BRAKE_CONTROL_RL==1)
		{
			RL_ESC.lcespu8CtrlMode = 2;
		}
		else
		{
			RL_ESC.lcespu8CtrlMode = 4;
		}
	}
	else 
	{
		;
	}  
	
	if(ESP_BRAKE_CONTROL_FR==1)
	{
		FR_ESC.lcespu8CtrlMode = 1;	
		RL_ESC.lcespu8CtrlMode = 3;
		if(ESP_BRAKE_CONTROL_FL==1)
		{
			FL_ESC.lcespu8CtrlMode = 1;
		}
		else
		{
			FL_ESC.lcespu8CtrlMode = 4;
		}
		
		if(ESP_BRAKE_CONTROL_RR==1)
		{
			RR_ESC.lcespu8CtrlMode = 2;
		}
		else
		{
			RR_ESC.lcespu8CtrlMode = 4;
		}
	}
	else 
	{
		;
	}
		
	if(ESP_BRAKE_CONTROL_RL==1)
	{
		RL_ESC.lcespu8CtrlMode = 2;	
		FR_ESC.lcespu8CtrlMode = 3;
		if(ESP_BRAKE_CONTROL_FL==1)
		{
			FL_ESC.lcespu8CtrlMode = 1;
			RR_ESC.lcespu8CtrlMode = 3;
		}
		else
		{
			FL_ESC.lcespu8CtrlMode = 4;
			RR_ESC.lcespu8CtrlMode = 4;
		}
	}
	else 
	{
		;
	}   
	
	if(ESP_BRAKE_CONTROL_RR==1)
	{
		RR_ESC.lcespu8CtrlMode = 2;	
		FL_ESC.lcespu8CtrlMode = 3;
		if(ESP_BRAKE_CONTROL_FR==1)
		{
			FR_ESC.lcespu8CtrlMode = 1;
			RL_ESC.lcespu8CtrlMode = 3;
		}
		else
		{
			FR_ESC.lcespu8CtrlMode = 4;
			RL_ESC.lcespu8CtrlMode = 4;
		}
	}
	else 
	{
		;
	}                       
}  
void LAESP_vSetFinalTpRate(void)
{
	FL_ESC.lcesps16FinalTarPRate=0;
	FR_ESC.lcesps16FinalTarPRate=0;
	RL_ESC.lcesps16FinalTarPRate=0;
	RR_ESC.lcesps16FinalTarPRate=0;
	
	if(ESP_BRAKE_CONTROL_FL==1)	
	{
		FL_ESC.lcesps16FinalTarPRate= FL_ESC.lcesps16IdbTarPRate;
	}
    else if((FL.CROSS_SLIP_CONTROL==1) || (FL.U1EscOtherWheelCnt==1))  
    {
    	FL_ESC.lcesps16FinalTarPRate = FL.lcesps16IdbOtherPressRte;
    }  
    else					
    {
    	FL_ESC.lcesps16FinalTarPRate =0;
    }
    
    if(ESP_BRAKE_CONTROL_FR==1) 
    {
    	FR_ESC.lcesps16FinalTarPRate = FR_ESC.lcesps16IdbTarPRate;
    }
    else if((FR.CROSS_SLIP_CONTROL==1) || (FR.U1EscOtherWheelCnt==1))	
    {
    	FR_ESC.lcesps16FinalTarPRate = FR.lcesps16IdbOtherPressRte;
    }  
    else					
    {
    	FR_ESC.lcesps16FinalTarPRate = 0;
    }
    
    if(ESP_BRAKE_CONTROL_RL==1)	
    {
    	RL_ESC.lcesps16FinalTarPRate = RL_ESC.lcesps16IdbTarPRate;
    }
    else if((RL.CROSS_SLIP_CONTROL==1) || (RL.U1EscOtherWheelCnt==1))  
    {
    	RL_ESC.lcesps16FinalTarPRate = RL.lcesps16IdbOtherPressRte;
    }  
    else					
    {
    	RL_ESC.lcesps16FinalTarPRate = 0;
    }
    
    if(ESP_BRAKE_CONTROL_RR==1) 
    {
    	RR_ESC.lcesps16FinalTarPRate = RR_ESC.lcesps16IdbTarPRate;
    }
    else if((RR.CROSS_SLIP_CONTROL==1) || (RR.U1EscOtherWheelCnt==1))	
    {
    	RR_ESC.lcesps16FinalTarPRate = RR.lcesps16IdbOtherPressRte;
    }  
    else
    {
 		RR_ESC.lcesps16FinalTarPRate = 0;
    }

	
} 
void LAESP_vActuateCrossWheelOS(struct ESC_STRUCT *WL_ESC, struct ESC_STRUCT_COMB *WL_ESC_COMB, struct W_STRUCT *WL)
{
  #if !__SPLIT_TYPE /* X-Split */
  	WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;
    WL->lcesps16IdbOtherPress = WL->s16_Estimated_Active_Press;
    WL->lcesps16IdbDelOtherPress = WL->lcesps16IdbOtherPress - WL->lcesps16IdbOtherPressOld;
    
    if(WL->lcesps16IdbOtherPress >= WL->lcesps16IdbOtherPressOld)
    {
    	WL->lcesps16IdbOtherPressRte = S16_TARGET_PRESS_RATE_100BAR;
    }
    else
    {
    	WL->lcesps16IdbOtherPressRte = -S16_TARGET_PRESS_RATE_100BAR;
    }
    if(PARTIAL_BRAKE==1)
    {  
        /* Default Setting: Hold */
        WL->HV_VL=1;
        WL->AV_VL=0;
        WL->flags=236;
        WL->CROSS_SLIP_CONTROL=1;
        
        /* ABS에 의한 Dump */
        if(ABS_fz==1)
        {
            if(WL->AV_DUMP==1)
            {
                WL->HV_VL=1;
                WL->AV_VL=1;
                WL->flags=237;
                WL->CROSS_SLIP_CONTROL=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }

        /* delP에 의한 대각휠 Dump */
        esp_tempW0 = WL->s16_Estimated_Active_Press - lis16DriverIntendedTP ;
    
        if(esp_tempW0<0)
        {
            esp_tempW0 = 0 ; 
        }
        
        esp_tempW1 = LCESP_s16IInter4Point( lis16DriverIntendedTP,  S16_COMB_MPRESS_V_L_TH , (int16_t)S16_REAR_DEL_PRESS_V_L_TH,
                                                     S16_COMB_MPRESS_L_TH ,   (int16_t)S16_REAR_DEL_PRESS_L_TH,
                                                     S16_COMB_MPRESS_M_TH ,   (int16_t)S16_REAR_DEL_PRESS_M_TH,  
                                                     S16_COMB_MPRESS_H_TH ,   (int16_t)S16_REAR_DEL_PRESS_H_TH);                      
     
        if(esp_tempW0 >= esp_tempW1)
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else if(WL->EBD == 1)
        {
        	if(WL->AV_VL==1)
        	{
        		WL->HV_VL=1;
	            WL->AV_VL=1;
	            WL->flags=237;
	            WL->CROSS_SLIP_CONTROL=1;
        	}
        	else
        	{
        		;
        	}

        }
        /*else if(AV_VL_rr==0)
        {
            HV_VL_rr=1;
            AV_VL_rr=0;
            RR.flags=236;
            CROSS_SLIP_CONTROL_rr=1;
        }*/
        else
        {
        	;
        }
    
        /* Big OS 판단에 의한 Dump */
        if(WL_ESC_COMB->FLAG_ACT_COMB_CNTR==1)
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else
        {
        	;
        }
    
        /* 대각휠 슬립에 의한 Dump */            
        if(WL_ESC->slip_m<=(-2500))
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    
    /* FADE OUT 시 대각휠 HOLD */
    /*if(ESP_PULSE_DUMP_FLAG_F==1)
    {
        WL->HV_VL=1;
        WL->AV_VL=0;
        WL->flags=236;
        WL->CROSS_SLIP_CONTROL=1;
    }
    else
    {
    	;
    }*/

  #else /* FR-Split */
    if(PARTIAL_BRAKE==1)
    {  
        /* Default Setting: Hold */
        WL->HV_VL=1;
        WL->AV_VL=0;
        WL->flags=236;
        WL->CROSS_SLIP_CONTROL=1;
  
        /* ABS에 의한 Dump */
        if(ABS_fz==1)
        {
            if(WL->AV_DUMP==1)
            {
                WL->HV_VL=1;
                WL->AV_VL=1;
                WL->flags=237;
                WL->CROSS_SLIP_CONTROL=1;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
        
        /* delP에 의한 대각휠 Dump */
        esp_tempW0 = WL->s16_Estimated_Active_Press - mpress ; 
    
        if(esp_tempW0<0)
        {
            esp_tempW0 = 0 ; 
        }
        
        esp_tempW1 = LCESP_s16IInter4Point( mpress,  S16_COMB_MPRESS_V_L_TH , S16_REAR_DEL_PRESS_V_L_TH,
                                                     S16_COMB_MPRESS_L_TH ,   S16_REAR_DEL_PRESS_L_TH,
                                                     S16_COMB_MPRESS_M_TH ,   S16_REAR_DEL_PRESS_M_TH,  
                                                     S16_COMB_MPRESS_H_TH ,   S16_REAR_DEL_PRESS_H_TH);                      
     
        if(esp_tempW0 >= esp_tempW1)
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        /*else if(AV_VL_rr==0)
        {
            HV_VL_rr=1;
            AV_VL_rr=0;
            RR.flags=236;
            CROSS_SLIP_CONTROL_rr=1;
        }*/
        else
        {
            ;
        }
    
        /* Big OS 판단에 의한 Dump */
        if(WL_ESC_COMB->FLAG_ACT_COMB_CNTR==1)
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else
        {
            ;
        }
    
        /* 대각휠 슬립에 의한 Dump */            
        if(WL_ESC->slip_m<=(-1500))
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }  
    
    /* FADE OUT 시 대각휠 HOLD */
    if(ESP_PULSE_DUMP_FLAG_F==1)
    {
        WL->HV_VL=1;
        WL->AV_VL=0;
        WL->flags=236;
        WL->CROSS_SLIP_CONTROL=1;
    }
    else
    {
    	;
    }
  
  #endif
}

void LAESP_vActuateCrossWheelUS(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL)
{
 #if !__SPLIT_TYPE /* X-Split */
    /* Default Setting: Hold */
     /*
    WL->HV_VL=1;
    WL->AV_VL=0;
    WL->flags=236;
    WL->CROSS_SLIP_CONTROL=1;
    */
    /* INITIAL_BRAKE에 의한 Rise */
  	WL->lcesps16IdbOtherPressOld = WL->lcesps16IdbOtherPress;
    WL->lcesps16IdbOtherPress = WL->s16_Estimated_Active_Press;
    WL->lcesps16IdbDelOtherPress = WL->lcesps16IdbOtherPress - WL->lcesps16IdbOtherPressOld;
    if(WL->lcesps16IdbOtherPress >= WL->lcesps16IdbOtherPressOld)
    {
    	WL->lcesps16IdbOtherPressRte = S16_TARGET_PRESS_RATE_100BAR;
    }
    else
    {
    	WL->lcesps16IdbOtherPressRte = -S16_TARGET_PRESS_RATE_100BAR;
    }
    if(INITIAL_BRAKE==1)
    {
        WL->HV_VL=0;
        WL->AV_VL=0;
        WL->flags=235;
        WL->CROSS_SLIP_CONTROL=1;
    }
	else if(ABS_fz==1)
    {
		if(PARTIAL_BRAKE == 1)
    	{
        	if(WL->AV_DUMP==1)
        	{
            	WL->HV_VL=1;
            	WL->AV_VL=1;
            	WL->flags=237;
            	WL->CROSS_SLIP_CONTROL=1;
        	}
        	else
        	{
	            WL->HV_VL=1;
			    WL->AV_VL=0;
			    WL->flags=236;
			    WL->CROSS_SLIP_CONTROL=1;
	        }
	    }
	    else
	    {
            ;
        }
    }
    else
    {
        ;
    }
    
    /* 대각휠 슬립에 의한 Dump */            
    if(WL_ESC->slip_m<=(-1500))
    {
        WL->HV_VL=1;
        WL->AV_VL=1;
        WL->flags=237;
        WL->CROSS_SLIP_CONTROL=1;
    }
    else
    {
        ;
    }
  #else /* FR-Split */
    /* Default Setting: Hold */
    WL->HV_VL=1;
    WL->AV_VL=0;
    WL->flags=236;
    WL->CROSS_SLIP_CONTROL=1;
    
    /* INITIAL_BRAKE에 의한 Rise */
    if(INITIAL_BRAKE==1)
    {
        WL->HV_VL=0;
        WL->AV_VL=0;
        WL->flags=235;
        WL->CROSS_SLIP_CONTROL=1;
    }
    else
    {
        ;
    }
    
    /* ABS에 의한 Dump */
    if(ABS_fz==1)
    {
        if(WL->AV_DUMP==1)
        {
            WL->HV_VL=1;
            WL->AV_VL=1;
            WL->flags=237;
            WL->CROSS_SLIP_CONTROL=1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
  
    /* 대각휠 슬립에 의한 Dump */            
    if(WL_ESC->slip_m<=(-1500))
    {
        WL->HV_VL=1;
        WL->AV_VL=1;
        WL->flags=237;
        WL->CROSS_SLIP_CONTROL=1;
    }
    else
    {
        ;
    }  
  
  #endif
}

/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
void LAESP_vCallActXsplitEsvTc(void)
{
  #if __HDC || __DEC
	uint8_t TCL_DEMAND_fr_org, TCL_DEMAND_fl_org;

	TCL_DEMAND_fr_org = 0;
    TCL_DEMAND_fl_org = 0;

	  #if __DEC
        if (lcu1DecActiveFlg==1)
        {
			TCL_DEMAND_fr_org = TCL_DEMAND_fr;
        	TCL_DEMAND_fl_org = TCL_DEMAND_fl;
        }
        else { }
	  #endif

      #if __HDC
      	if (lcu1HdcActiveFlg==1)
        {
			TCL_DEMAND_fr_org = TCL_DEMAND_fr;
        	TCL_DEMAND_fl_org = TCL_DEMAND_fl;
        }
        else { }
      #endif
  #endif

    /* 060118 added by eslim */
    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW0 = esp_pressure_count_fl * 80 / 7;  /* for 5ms, *8 --> *80/14 , for 10ms, *80/7 */
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW0 = esp_pressure_count_fr * 80 / 7;  /* for 5ms, *8 --> *80/14  */
    }
    else
    {
        ;
    }

    if(ESP_COMBINATION_CONTROL==1)
    {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if((ABS_fz==0)&&(lsesps16AHBGEN3mpress>esp_tempW0))        /* mpress > Wheel press */
          #else
        if((ABS_fz==0)&&(mpress>esp_tempW0))        /* mpress > Wheel press */
          #endif
        {  /* 060210 modified by eslim */
            
            #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
            if((FL_ESC.mpress_rise_mode==1)||(FR_ESC.mpress_rise_mode==1))
            #else
            if(mpress_rise_mode==1)             /* mpress_slop >> motor_press_slop */
            #endif
            {                                   /* then use mpress force */
                TCL_DEMAND_fl=0; TCL_DEMAND_fr=0;
                S_VALVE_RIGHT=0; S_VALVE_LEFT=0;
            }
            else
            {
                if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
                {
                    TCL_DEMAND_fl=TEMP_TCL_DEMAND_fl;
                    S_VALVE_LEFT=T_S_V_LEFT;
                }
                else
                {
                    ;
                }
                if((ESP_BRAKE_CONTROL_FR ==1 )||(ESP_BRAKE_CONTROL_RL ==1))
                {
                    TCL_DEMAND_fr=TEMP_TCL_DEMAND_fr;
                    S_VALVE_RIGHT=T_S_V_RIGHT;
                }
                else
                {
                    ;
                }
            }
        } /* 060210 modified by eslim */
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        else if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
          #else
        else if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
          #endif
        {
            if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_RR==1))
            {
                TCL_DEMAND_fl=TEMP_TCL_DEMAND_fl;
                S_VALVE_LEFT=T_S_V_LEFT;
            }
            else
            {
                ;
            }
            if((ESP_BRAKE_CONTROL_FR)||(ESP_BRAKE_CONTROL_RL))
            {
                TCL_DEMAND_fr=TEMP_TCL_DEMAND_fr;
                S_VALVE_RIGHT=T_S_V_RIGHT;
            }
            else
            {
                ;
            }
        }
        else if((INITIAL_BRAKE==1)&&(((SLIP_CONTROL_NEED_FL==1)&&(esp_control_mode_fl<3))
            ||((SLIP_CONTROL_NEED_RR==1)&&(esp_control_mode_rr<3))))
        {
            TCL_DEMAND_fl=1; TCL_DEMAND_fr=0; /* pressure rise by motor in initial brake but rear not */
            S_VALVE_LEFT=1; S_VALVE_RIGHT=0;
        }
        else if((INITIAL_BRAKE==1)&&(((SLIP_CONTROL_NEED_FR==1)&&(esp_control_mode_fr<3))
            ||((SLIP_CONTROL_NEED_RL==1)&&(esp_control_mode_rl<3))))
        {
            TCL_DEMAND_fl=0; TCL_DEMAND_fr=1; /* pressure rise by motor in initial brake but rear not */
            S_VALVE_LEFT=0; S_VALVE_RIGHT=1;

        }
        /* 060118 added by eslim */
        else
        {
            if((ABS_fz==1)&&(PARTIAL_BRAKE==0)
#if __ACC
            &&(lcu1AccActiveFlg==0)
#endif
#if __HDC
            &&(lcu1HdcActiveFlg==0)
#endif
#if __DEC
            &&(lcu1DecActiveFlg==0)
#endif
#if __CRAM
            &&(ldu1espDetectCRAM==0)
#endif
#if __TSP
            &&(TSP_ON==0)
#endif
            )
            {
                ;
            }
            else
            {
                if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
                {
                    TCL_DEMAND_fl=TEMP_TCL_DEMAND_fl;
                    S_VALVE_LEFT=T_S_V_LEFT;
                }
                else
                {
                    ;
                }
                if((ESP_BRAKE_CONTROL_FR ==1 )||(ESP_BRAKE_CONTROL_RL ==1))
                {
                    TCL_DEMAND_fr=TEMP_TCL_DEMAND_fr;
                    S_VALVE_RIGHT=T_S_V_RIGHT;
                }
                else
                {
                    ;
                }
            }
        }
	  #if __DEC
        if (lcu1DecActiveFlg==1)
        {
	        if (TCL_DEMAND_fr_org == 1 )
	        {
	        	TCL_DEMAND_fr =1;
	        }
	        else {}
	    	if (TCL_DEMAND_fl_org == 1 )
	    	{
	     		TCL_DEMAND_fl =1;
	        }
	        else { }
	    }
 		else {}
	  #endif

	  #if __HDC
        if (lcu1HdcActiveFlg==1)
        {
	        if (TCL_DEMAND_fr_org == 1 )
	        {
	        	TCL_DEMAND_fr =1;
	        }
	        else {}
	    	if (TCL_DEMAND_fl_org == 1 )
	    	{
	     		TCL_DEMAND_fl =1;
	        }
	        else { }
	    }
 		else {}
	  #endif
    }
    else
    {
        if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
        {
            TCL_DEMAND_fl=TEMP_TCL_DEMAND_fl;
            S_VALVE_LEFT=T_S_V_LEFT;

            if((BTCS_fr==0)&&(BTCS_rl==0)&&(ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_RL==0)
            #if ((__TVBB == ENABLE) && (__TVBB_USC_COMB_CONTROL == ENABLE))
            &&((lctvbbu1TVBB_ON == 0) || (ldtvbbu1FrontWhlCtl == 0))
            #endif
            )
            {
                TCL_DEMAND_fr=0;
                S_VALVE_RIGHT=0;
            }
        }
        else
        {
            ;
        }
        if((ESP_BRAKE_CONTROL_FR ==1 )||(ESP_BRAKE_CONTROL_RL ==1))
        {
            TCL_DEMAND_fr=TEMP_TCL_DEMAND_fr;
            S_VALVE_RIGHT=T_S_V_RIGHT;

            if((BTCS_fl==0)&&(BTCS_rr==0)&&(ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_RR ==0)
            #if ((__TVBB == ENABLE) && (__TVBB_USC_COMB_CONTROL == ENABLE))
            &&((lctvbbu1TVBB_ON == 0) || (ldtvbbu1FrontWhlCtl == 0))
            #endif
            )
            {
                TCL_DEMAND_fl=0;
                S_VALVE_LEFT=0;
            }
        }
        else
        {
            ;
        }
    }
}
#else
void LAESP_vCallActFRsplitEsvTc(void)
{

  #if __HDC || __DEC
	uint8_t TCL_DEMAND_PRIMARY_org, TCL_DEMAND_SECONDARY_org;

	TCL_DEMAND_PRIMARY_org = 0;
    TCL_DEMAND_SECONDARY_org = 0;

	  #if __DEC
        if (lcu1DecActiveFlg==1)
        {
			TCL_DEMAND_PRIMARY_org = TCL_DEMAND_PRIMARY;
        	TCL_DEMAND_SECONDARY_org = TCL_DEMAND_SECONDARY;
        }
        else { }
	  #endif

      #if __HDC
      	if (lcu1HdcActiveFlg==1)
        {
			TCL_DEMAND_PRIMARY_org = TCL_DEMAND_PRIMARY;
        	TCL_DEMAND_SECONDARY_org = TCL_DEMAND_SECONDARY;
        }
        else { }
      #endif
  #endif

    /* 060118 added by eslim */
    if(SLIP_CONTROL_NEED_FL==1)
    {
        esp_tempW0 = esp_pressure_count_fl * 80 / 7;  /* for 5ms, *8 --> *80/14  */
    }
    else if(SLIP_CONTROL_NEED_FR==1)
    {
        esp_tempW0 = esp_pressure_count_fr * 80 / 7;  /* for 5ms, *8 --> *80/14  */
    }
    else
    {
        ;
    }

    if(ESP_COMBINATION_CONTROL==1)
    {
          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        if((ABS_fz==0)&&(lsesps16AHBGEN3mpress>esp_tempW0))        /* mpress > Wheel press */
          #else 
        if((ABS_fz==0)&&(mpress>esp_tempW0))        /* mpress > Wheel press */
          #endif
        {  /* 060210 modified by eslim */
            
            #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
            if((FL_ESC.mpress_rise_mode==1)||(FR_ESC.mpress_rise_mode==1))
            #else
            if(mpress_rise_mode==1)             /* mpress_slop >> motor_press_slop */
            #endif
            {                                   /* then use mpress force */
                TCL_DEMAND_PRIMARY=0; TCL_DEMAND_SECONDARY=0;
                S_VALVE_PRIMARY=0; S_VALVE_SECONDARY=0;
            }
            else
            {
                if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_FR ==1))
                {
                    TCL_DEMAND_PRIMARY=TEMP_TCL_DEMAND_PRIMARY;
                    S_VALVE_PRIMARY=T_S_V_PRIMARY;
                }
                else
                {
                    ;
                }
                if((ESP_BRAKE_CONTROL_RL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
                {
                    TCL_DEMAND_SECONDARY=TEMP_TCL_DEMAND_SECONDARY;
                    S_VALVE_SECONDARY=T_S_V_SECONDARY;
                }
                else
                {
                    ;
                }
            }
        } /* 060210 modified by eslim */
          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        else if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(lsespu1AHBGEN3MpresBrkOn==1))
          #else
        else if((PARTIAL_BRAKE==1)&&(INITIAL_BRAKE==0)&&(MPRESS_BRAKE_ON==1))
          #endif
        {
            if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1))
            {
                TCL_DEMAND_PRIMARY=TEMP_TCL_DEMAND_PRIMARY;
                S_VALVE_PRIMARY=T_S_V_PRIMARY;
            }
            else
            {
                ;
            }
            if((ESP_BRAKE_CONTROL_RL)||(ESP_BRAKE_CONTROL_RR))
            {
                TCL_DEMAND_SECONDARY=TEMP_TCL_DEMAND_SECONDARY;
                S_VALVE_SECONDARY=T_S_V_SECONDARY;
            }
            else
            {
                ;
            }
        }
        else if((INITIAL_BRAKE==1)&&(((SLIP_CONTROL_NEED_FL==1)&&(esp_control_mode_fl<3))
            ||((SLIP_CONTROL_NEED_FR==1)&&(esp_control_mode_fr<3))))
        {
            TCL_DEMAND_PRIMARY=1; TCL_DEMAND_SECONDARY=0; /* pressure rise by motor in initial brake but rear not */
            S_VALVE_PRIMARY=1; S_VALVE_SECONDARY=0;
        }
        else if((INITIAL_BRAKE==1)&&(((SLIP_CONTROL_NEED_RL==1)&&(esp_control_mode_rl<3))
            ||((SLIP_CONTROL_NEED_RR==1)&&(esp_control_mode_rr<3))))
        {
            TCL_DEMAND_PRIMARY=0; TCL_DEMAND_SECONDARY=1; /* pressure rise by motor in initial brake but rear not */
            S_VALVE_PRIMARY=0; S_VALVE_SECONDARY=1;

        }
        /* 060118 added by eslim */
        else
        {
            if((ABS_fz==1)&&(PARTIAL_BRAKE==0)
#if __ACC
            &&(lcu1AccActiveFlg==0)
#endif
#if __HDC
            &&(lcu1HdcActiveFlg==0)
#endif
#if __DEC
            &&(lcu1DecActiveFlg==0)
#endif
#if __TSP
            &&(TSP_ON==0)
#endif
            )
            {
                ;
            }
            else
            {
                if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_FR ==1))
                {
                    TCL_DEMAND_PRIMARY=TEMP_TCL_DEMAND_PRIMARY;
                    S_VALVE_PRIMARY=T_S_V_PRIMARY;
                }
                else
                {
                    ;
                }
                if((ESP_BRAKE_CONTROL_RL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
                {
                    TCL_DEMAND_SECONDARY=TEMP_TCL_DEMAND_SECONDARY;
                    S_VALVE_SECONDARY=T_S_V_SECONDARY;
                }
                else
                {
                    ;
                }
            }
        }
	  #if __DEC
        if (lcu1DecActiveFlg==1)
        {
	        if (TCL_DEMAND_PRIMARY_org == 1 )
	        {
	        	TCL_DEMAND_fr =1;
	        }
	        else {}
	    	if (TCL_DEMAND_SECONDARY_org == 1 )
	    	{
	     		TCL_DEMAND_fl =1;
	        }
	        else { }
	    }
 		else {}
	  #endif

	  #if __HDC
        if (lcu1HdcActiveFlg==1)
        {
	        if (TCL_DEMAND_PRIMARY_org == 1 )
	        {
	        	TCL_DEMAND_fr =1;
	        }
	        else {}
	    	if (TCL_DEMAND_SECONDARY_org == 1 )
	    	{
	     		TCL_DEMAND_fl =1;
	        }
	        else { }
	    }
 		else {}
	  #endif

    }
    else
    {
        if((ESP_BRAKE_CONTROL_FL ==1 )||(ESP_BRAKE_CONTROL_FR ==1))
        {
            TCL_DEMAND_PRIMARY=TEMP_TCL_DEMAND_PRIMARY;
            S_VALVE_PRIMARY=T_S_V_PRIMARY;
/*      #if __PRESS_EST_ACTIVE
          esp_tempW0 = FL.s16_Estimated_Active_Press + 100;
          esp_tempW1 = FR.s16_Estimated_Active_Press + 100;
      #else */
            esp_tempW0 = esp_pressure_count_fl*10 + 100;	/* for 5ms, *10 --> *100/14  */
            esp_tempW1 = esp_pressure_count_fr*10 + 100;  /* for 5ms, *10 --> *100/14  */
/*      #endif */

            #if defined(__ADDON_TCMF)
              #if(__AHB_GEN3_SYSTEM == ENABLE)  || (__IDB_LOGIC == ENABLE)
            if((Booster_Act==1)
            &&(((SLIP_CONTROL_NEED_FL==1)&&(esp_tempW0 <= lsesps16AHBGEN3mpress)&&(slip_control_need_counter_fl <42))
               ||((SLIP_CONTROL_NEED_FR==1)&&(esp_tempW1 <= lsesps16AHBGEN3mpress)&&(slip_control_need_counter_fr <42)))
            )  
              #else
            if((Booster_Act==1)
            &&(((SLIP_CONTROL_NEED_FL==1)&&(esp_tempW0 <= mpress)&&(slip_control_need_counter_fl <42))
               ||((SLIP_CONTROL_NEED_FR==1)&&(esp_tempW1 <= mpress)&&(slip_control_need_counter_fr <42)))
            )
              #endif
            {
                TCL_DEMAND_PRIMARY=0;
            }
            #endif
        }
        else
        {
            if((BTCS_fr==0)&&(BTCS_fl==0)&&(ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_FL==0)
             #if ((__TVBB == ENABLE) && (__TVBB_USC_COMB_CONTROL == ENABLE))
            &&((lctvbbu1TVBB_ON == 0) || (ldtvbbu1FrontWhlCtl == 0))
            #endif
            )
            {
                TCL_DEMAND_PRIMARY=0;
                S_VALVE_PRIMARY=0;
            }
            else
            {
                ;
            }
        }
        if((ESP_BRAKE_CONTROL_RL ==1 )||(ESP_BRAKE_CONTROL_RR ==1))
        {
            TCL_DEMAND_SECONDARY=TEMP_TCL_DEMAND_SECONDARY;
            S_VALVE_SECONDARY=T_S_V_SECONDARY;
        }
        else
        {
            if((BTCS_rr==0)&&(BTCS_rl==0)&&(ESP_BRAKE_CONTROL_RR==0)&&(ESP_BRAKE_CONTROL_RL==0))
            {
                TCL_DEMAND_SECONDARY=0;
                S_VALVE_SECONDARY=0;
            }
            else
            {
                ;
            }
        }
    }
}
#endif
/*060607 modified and added for FR Split by eslim*/

   #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
int16_t LAMFC_s16SetMFCCurrentESP(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
    int16_t  MFC_Current_new = 0;
      #if !__SPLIT_TYPE
    if (
          ((CIRCUIT == U16_PRIMARY_CIRCUIT) && ((lcescu1TCMFUnderControlRl == 1)||(lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHFadeout_RL == 1)))
        ||((CIRCUIT == U16_SECONDARY_CIRCUIT) && ((lcescu1TCMFUnderControlRr == 1)||(lcu1OVER2WHControlFlag_RR == 1)||(lcu1OVER2WHFadeout_RR == 1)))
       )
      #else
    if ((CIRCUIT == U16_SECONDARY_CIRCUIT) && (((lcescu1TCMFUnderControlRl == 1)||(lcescu1TCMFUnderControlRr == 1))||((lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHControlFlag_RR == 1))||((lcu1OVER2WHFadeout_RL == 1)||(lcu1OVER2WHFadeout_RR == 1))))
      #endif
    {
        MFC_Current_new = LAMFC_s16SetMFCCurrentESPRear(MFC_Current_old);
    }
      #if !__SPLIT_TYPE
    else if (
       ((CIRCUIT == U16_SECONDARY_CIRCUIT)   && ((lcu1US2WHControlFlag_RL == 1) || (lcu1US2WHFadeout_RL == 1)))
     ||((CIRCUIT == U16_PRIMARY_CIRCUIT) && ((lcu1US2WHControlFlag_RR == 1) || (lcu1US2WHFadeout_RR == 1)))
       )
      #else
    else if ((CIRCUIT == U16_PRIMARY_CIRCUIT) && ((lcu1US2WHControlFlag_RL == 1) || (lcu1US2WHControlFlag_RR == 1) || (lcu1US2WHFadeout_RL == 1) || (lcu1US2WHFadeout_RR == 1)))
      #endif
    {
        MFC_Current_new = LAMFC_s16SetMFCCurrentESPUS2WL(MFC_Current_old);
    }
    else
    {
        MFC_Current_new = TC_CURRENT_MAX;

			  #if (__ESC_COMPETITIVE_CONTROL == 1)

			    if(ldespu1CompetitiveModeESC==1)
			    {
			    	MFC_Current_new = S16_COMP_MODE_TC_CUR_LIMIT;
			    }
			    else
			    {
			    	;
			    }

			  #endif
    }
        
    return MFC_Current_new;
}

int16_t LAMFC_s16SetMFCCurrentESPRear(int16_t MFC_Current_old)
{
    int16_t  MFC_Current_new = 0;
        
        esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_USC_H_LIMIT_TC_CUR_HM, S16_TCMF_USC_H_LIMIT_TC_CUR_MM, S16_TCMF_USC_H_LIMIT_TC_CUR_LM);
        esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_USC_L_LIMIT_TC_CUR_HM, S16_TCMF_USC_L_LIMIT_TC_CUR_MM, S16_TCMF_USC_L_LIMIT_TC_CUR_LM);
        
        if (
              ((SLIP_CONTROL_NEED_RR == 0) && (lcescu1TCMFUnderControlRr == 1))
            ||((SLIP_CONTROL_NEED_RL == 0) && (lcescu1TCMFUnderControlRl == 1))
           )
        {
            if ((MFC_Current_old == TC_CURRENT_MAX) && (lcescs16TCMFUnderControlCnt <= S16_TCMF_USC_INITIAL_FULL_TIME))
            {
                MFC_Current_new = esp_tempW6;
            }
            else if ((MFC_Current_old > esp_tempW6) && (MFC_Current_old > S16_TCMF_USC_FADE_OUT_DEC))
            {
                esp_tempW7 = MFC_Current_old - S16_TCMF_USC_FADE_OUT_DEC;
                
                if (esp_tempW7 > esp_tempW6)
                {
                    MFC_Current_new = esp_tempW7;
                }
                else
            {
                    MFC_Current_new = esp_tempW6;
                }
            }
            else
            {
                MFC_Current_new = esp_tempW6;
            }
        }
          #if __TVBB
        else if ((lcescu1TCMFUCSafterTVBBTC == 1) && (lcescs16TCMFUnderControlCnt == 1) && (MFC_Current_TVBB_S<=S16_TCMF_USC_H_LIMIT_TC_CUR_HM))
        {
            MFC_Current_new = MFC_Current_TVBB_S + S16_TCMF_USC_INITIAL_ADD_DUTY;
            lcescs16TCMFUnderControlCnt = S16_TCMF_USC_INITIAL_FULL_TIME;
        }
          #endif         
        else if (
            (lcescs16TCMFUnderControlCnt < S16_TCMF_USC_INITIAL_FULL_TIME)
          #if __TVBB
         && (lcescu1TCMFUCSafterTVBBTC == 0)
          #endif         
        )
        {
            MFC_Current_new = TC_CURRENT_MAX;
        }
        else 
        {
        		#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        		if(lcescu1TCMFUnderControlRl == 1)
        		{
        			  esp_tempW10 = RL_ESC.slip_control;
        		}
        		else if(lcescu1TCMFUnderControlRr == 1)
        		{
        			  esp_tempW10 = RR_ESC.slip_control;
        		}
        		else
        		{
        			;
        		}
        		
        		
            esp_tempW0 = LCESP_s16IInter5Point(-esp_tempW10, S16_TCMF_USC_INI_USC_R_HM_1,  S16_TCMF_USC_INI_TC_CUR_HM_1,
                                                             S16_TCMF_USC_INI_USC_R_HM_2,  S16_TCMF_USC_INI_TC_CUR_HM_2,
                                                             S16_TCMF_USC_INI_USC_R_HM_3,  S16_TCMF_USC_INI_TC_CUR_HM_3,
                                                             S16_TCMF_USC_INI_USC_R_HM_4,  S16_TCMF_USC_INI_TC_CUR_HM_4,
                                                             S16_TCMF_USC_INI_USC_R_HM_5,  S16_TCMF_USC_INI_TC_CUR_HM_5);
            
            esp_tempW1 = LCESP_s16IInter5Point(-esp_tempW10, S16_TCMF_USC_INI_USC_R_MM_1,  S16_TCMF_USC_INI_TC_CUR_MM_1, 
                                                             S16_TCMF_USC_INI_USC_R_MM_2,  S16_TCMF_USC_INI_TC_CUR_MM_2, 
                                                             S16_TCMF_USC_INI_USC_R_MM_3,  S16_TCMF_USC_INI_TC_CUR_MM_3, 
                                                             S16_TCMF_USC_INI_USC_R_MM_4,  S16_TCMF_USC_INI_TC_CUR_MM_4, 
                                                             S16_TCMF_USC_INI_USC_R_MM_5,  S16_TCMF_USC_INI_TC_CUR_MM_5);
                                                                   
                                                                   
            esp_tempW2 = LCESP_s16IInter5Point(-esp_tempW10, S16_TCMF_USC_INI_USC_R_LM_1,  S16_TCMF_USC_INI_TC_CUR_LM_1, 
                                                             S16_TCMF_USC_INI_USC_R_LM_2,  S16_TCMF_USC_INI_TC_CUR_LM_2, 
                                                             S16_TCMF_USC_INI_USC_R_LM_3,  S16_TCMF_USC_INI_TC_CUR_LM_3, 
                                                             S16_TCMF_USC_INI_USC_R_LM_4,  S16_TCMF_USC_INI_TC_CUR_LM_4, 
                                                             S16_TCMF_USC_INI_USC_R_LM_5,  S16_TCMF_USC_INI_TC_CUR_LM_5);
            #else
            esp_tempW0 = LCESP_s16IInter5Point(-slip_control_rear, S16_TCMF_USC_INI_USC_R_HM_1,  S16_TCMF_USC_INI_TC_CUR_HM_1,
                                                                   S16_TCMF_USC_INI_USC_R_HM_2,  S16_TCMF_USC_INI_TC_CUR_HM_2,
                                                                   S16_TCMF_USC_INI_USC_R_HM_3,  S16_TCMF_USC_INI_TC_CUR_HM_3,
                                                                   S16_TCMF_USC_INI_USC_R_HM_4,  S16_TCMF_USC_INI_TC_CUR_HM_4,
                                                                   S16_TCMF_USC_INI_USC_R_HM_5,  S16_TCMF_USC_INI_TC_CUR_HM_5);
            
            esp_tempW1 = LCESP_s16IInter5Point(-slip_control_rear, S16_TCMF_USC_INI_USC_R_MM_1,  S16_TCMF_USC_INI_TC_CUR_MM_1, 
                                                                   S16_TCMF_USC_INI_USC_R_MM_2,  S16_TCMF_USC_INI_TC_CUR_MM_2, 
                                                                   S16_TCMF_USC_INI_USC_R_MM_3,  S16_TCMF_USC_INI_TC_CUR_MM_3, 
                                                                   S16_TCMF_USC_INI_USC_R_MM_4,  S16_TCMF_USC_INI_TC_CUR_MM_4, 
                                                                   S16_TCMF_USC_INI_USC_R_MM_5,  S16_TCMF_USC_INI_TC_CUR_MM_5);
                                                                   
                                                                   
            esp_tempW2 = LCESP_s16IInter5Point(-slip_control_rear, S16_TCMF_USC_INI_USC_R_LM_1,  S16_TCMF_USC_INI_TC_CUR_LM_1, 
                                                                   S16_TCMF_USC_INI_USC_R_LM_2,  S16_TCMF_USC_INI_TC_CUR_LM_2, 
                                                                   S16_TCMF_USC_INI_USC_R_LM_3,  S16_TCMF_USC_INI_TC_CUR_LM_3, 
                                                                   S16_TCMF_USC_INI_USC_R_LM_4,  S16_TCMF_USC_INI_TC_CUR_LM_4, 
                                                                   S16_TCMF_USC_INI_USC_R_LM_5,  S16_TCMF_USC_INI_TC_CUR_LM_5);
            #endif
            
            MFC_Current_new = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);
            
            
              #if __TVBB
            if (lcescu1TCMFUCSafterTVBBTC == 1)
            {
                esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TVBB_CURRENT_HIGH_HMU,    S16_TVBB_CURRENT_HIGH_MMU,    S16_TVBB_CURRENT_HIGH_LMU);

                if (MFC_Current_new > (esp_tempW0 + S16_TCMF_USC_INC_RATE_AFT_TVBB))
                {
                    MFC_Current_new = (esp_tempW0 + S16_TCMF_USC_INC_RATE_AFT_TVBB);
                }
                else
                {
                    lcescu1TCMFUCSafterTVBBTC = 0;
                }
            }
            else
            {
                ;
            }
              #endif
            
        MFC_Current_new = LCTCS_s16ILimitRange(MFC_Current_new, esp_tempW6, esp_tempW5);
        
        }
        
        /* OVER 2WH */
        if((lcu1OVER2WHControlFlag_RL ==1)||(lcu1OVER2WHControlFlag_RR ==1)||(lcu1OVER2WHFadeout_RL ==1)||(lcu1OVER2WHFadeout_RR ==1)||((lcu1OVER2WHControlFlag ==0)&&(lcu1OVER2WHControlOld ==1)))
        {
        	  esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_OVER_2WH_H_LIMIT_TC_CUR_HM, S16_OVER_2WH_H_LIMIT_TC_CUR_MM, S16_OVER_2WH_H_LIMIT_TC_CUR_LM);
            esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu, S16_OVER_2WH_L_LIMIT_TC_CUR_HM, S16_OVER_2WH_L_LIMIT_TC_CUR_MM, S16_OVER_2WH_L_LIMIT_TC_CUR_LM);
            
            #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        		if(lcu1OVER2WHControlFlag_RL == 1)
        		{
        			  esp_tempW10 = RL_ESC.slip_control;
        		}
        		else if(lcu1OVER2WHControlFlag_RR == 1)
        		{
        			  esp_tempW10 = RR_ESC.slip_control;
        		}
        		else
        		{
        		    ;
        		}    
        		
        		
        		esp_tempW0 = LCESP_s16IInter5Point(-esp_tempW10, S16_OVER_2WH_USC_F_HM_1,  S16_OVER_2WH_USC_TC_CUR_HM_1,
                                                             S16_OVER_2WH_USC_F_HM_2,  S16_OVER_2WH_USC_TC_CUR_HM_2,
                                                             S16_OVER_2WH_USC_F_HM_3,  S16_OVER_2WH_USC_TC_CUR_HM_3,
                                                             S16_OVER_2WH_USC_F_HM_4,  S16_OVER_2WH_USC_TC_CUR_HM_4,
                                                             S16_OVER_2WH_USC_F_HM_5,  S16_OVER_2WH_USC_TC_CUR_HM_5);
            
            esp_tempW1 = LCESP_s16IInter5Point(-esp_tempW10, S16_OVER_2WH_USC_F_MM_1,  S16_OVER_2WH_USC_TC_CUR_MM_1, 
                                                             S16_OVER_2WH_USC_F_MM_2,  S16_OVER_2WH_USC_TC_CUR_MM_2, 
                                                             S16_OVER_2WH_USC_F_MM_3,  S16_OVER_2WH_USC_TC_CUR_MM_3, 
                                                             S16_OVER_2WH_USC_F_MM_4,  S16_OVER_2WH_USC_TC_CUR_MM_4, 
                                                             S16_OVER_2WH_USC_F_MM_5,  S16_OVER_2WH_USC_TC_CUR_MM_5);
                                                               
                                                               
            esp_tempW2 = LCESP_s16IInter5Point(-esp_tempW10, S16_OVER_2WH_USC_F_LM_1,  S16_OVER_2WH_USC_TC_CUR_LM_1, 
                                                             S16_OVER_2WH_USC_F_LM_2,  S16_OVER_2WH_USC_TC_CUR_LM_2, 
                                                             S16_OVER_2WH_USC_F_LM_3,  S16_OVER_2WH_USC_TC_CUR_LM_3, 
                                                             S16_OVER_2WH_USC_F_LM_4,  S16_OVER_2WH_USC_TC_CUR_LM_4, 
                                                             S16_OVER_2WH_USC_F_LM_5,  S16_OVER_2WH_USC_TC_CUR_LM_5);
            #else
            esp_tempW0 = LCESP_s16IInter5Point(-slip_control_rear, S16_OVER_2WH_USC_F_HM_1,  S16_OVER_2WH_USC_TC_CUR_HM_1,
                                                                   S16_OVER_2WH_USC_F_HM_2,  S16_OVER_2WH_USC_TC_CUR_HM_2,
                                                                   S16_OVER_2WH_USC_F_HM_3,  S16_OVER_2WH_USC_TC_CUR_HM_3,
                                                                   S16_OVER_2WH_USC_F_HM_4,  S16_OVER_2WH_USC_TC_CUR_HM_4,
                                                                   S16_OVER_2WH_USC_F_HM_5,  S16_OVER_2WH_USC_TC_CUR_HM_5);
            
            esp_tempW1 = LCESP_s16IInter5Point(-slip_control_rear, S16_OVER_2WH_USC_F_MM_1,  S16_OVER_2WH_USC_TC_CUR_MM_1, 
                                                                   S16_OVER_2WH_USC_F_MM_2,  S16_OVER_2WH_USC_TC_CUR_MM_2, 
                                                                   S16_OVER_2WH_USC_F_MM_3,  S16_OVER_2WH_USC_TC_CUR_MM_3, 
                                                                   S16_OVER_2WH_USC_F_MM_4,  S16_OVER_2WH_USC_TC_CUR_MM_4, 
                                                                   S16_OVER_2WH_USC_F_MM_5,  S16_OVER_2WH_USC_TC_CUR_MM_5);
                                                               
                                                               
            esp_tempW2 = LCESP_s16IInter5Point(-slip_control_rear, S16_OVER_2WH_USC_F_LM_1,  S16_OVER_2WH_USC_TC_CUR_LM_1, 
                                                                   S16_OVER_2WH_USC_F_LM_2,  S16_OVER_2WH_USC_TC_CUR_LM_2, 
                                                                   S16_OVER_2WH_USC_F_LM_3,  S16_OVER_2WH_USC_TC_CUR_LM_3, 
                                                                   S16_OVER_2WH_USC_F_LM_4,  S16_OVER_2WH_USC_TC_CUR_LM_4, 
                                                                   S16_OVER_2WH_USC_F_LM_5,  S16_OVER_2WH_USC_TC_CUR_LM_5);
            #endif
        
            esp_tempW11 = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);
            
            esp_tempW3 = S16_OVER_2WH_USC_INITIAL_FULL_TIME;
            esp_tempW4 = S16_OVER_2WH_USC_TRANSIENT_TIME;
            esp_tempW7 = S16_OVER_2WH_USC_TC_CUR_FACTOR;
            esp_tempW8 = S16_OVER_2WH_TC_CUR_CYCLE_FACTOR;
            esp_tempW9 = S16_OVER_2WH_FADE_OUT_DEC;

            #if (__ESC_COMPETITIVE_CONTROL == 1)

                if(ldespu1CompetitiveModeESC==1)
                {
                    esp_tempW3 = (int16_t)U8_COMP_MODE_OS_2_WH_INI_RISE_TIME;
                    esp_tempW4 = (int16_t)U8_COMP_MODE_OS_2_WH_TRANS_TIME;
                    esp_tempW7 = (int16_t)((((int32_t)esp_tempW7)*U8_COMP_MODE_OS_2_WH_CUR_GAIN_1ST)/100);
                    esp_tempW8 = (int16_t)((((int32_t)esp_tempW8)*U8_COMP_MODE_OS_2_WH_CUR_GAIN_2ND)/100);
                    esp_tempW9 = (int16_t)U8_COMP_MODE_2WH_FADE_OUT_DEC;
                    esp_tempW11 = (int16_t)((((int32_t)esp_tempW11)*U8_COMP_MODE_OS_2_WH_USC_TC_CUR_GAIN)/100);
                }
                else
                {
                    ;
                }

            #endif
            if(
            	((lcu1OVER2WHFadeout_RL == 1)||(lcu1OVER2WHFadeout_RR == 1)) //FadeOut Stage
            	||((lcu1OVER2WHControlFlag ==0)&&(lcu1OVER2WHControlOld ==1))
            	||(del_target_2wheel_slip ==0)
            	) 
            
            {
                if (MFC_Current_old == TC_CURRENT_MAX) /*&& (lcs16OVER2WH_count <= esp_tempW3))*/
                {
                    MFC_Current_new = esp_tempW6;
                }
                else if ((MFC_Current_old > esp_tempW6)&&(MFC_Current_old > esp_tempW9))
                {
                    MFC_Current_new = MFC_Current_old - esp_tempW9;
                    
                MFC_Current_new = LCTCS_s16ILimitMinimum(MFC_Current_new, esp_tempW6);
                }
                else
                {
                	  if(del_target_2wheel_slip ==0)
                	  {
                	  	  MFC_Current_new = 0;
                	  }
                	  else
                	  {
                          MFC_Current_new = esp_tempW6;
                	  }
                }
            }         
            else if((lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHControlFlag_RR == 1))
            {            	  
        	      #if(__OS2WHEEL_IN_COMBINATION ==1)
        	      if((lcs16OVER2WH_count <= esp_tempW3)&&(MPRESS_BRAKE_ON ==0))
        	      #else
        	      if(lcs16OVER2WH_count <= esp_tempW3)
        	      #endif
        	      {
                  	MFC_Current_new = TC_CURRENT_MAX;
        	      }
        	      #if(__OS2WHEEL_IN_COMBINATION ==1)
        	    else if ((lcs16OVER2WH_count <= (esp_tempW3+esp_tempW4))&&(MPRESS_BRAKE_ON ==0))
        	      #else
        	    else if ((lcs16OVER2WH_count <= (esp_tempW3+esp_tempW4)))
        	      #endif
        	      {
        	      	
        	      	  #if(__ESC_CYCLE_DETECTION)
        	          if(ldespu1ESC1stCycle == 1)
                    {
        	              MFC_Current_new = esp_tempW11 + (int16_t)((((int32_t)esp_tempW11)*esp_tempW7)/100);
                    }
                    else if(ldespu1ESC2ndCycle == 1)
        	      {
        	              MFC_Current_new = esp_tempW11 + (int16_t)((((int32_t)esp_tempW11)*esp_tempW7)/100) + (int16_t)((((int32_t)esp_tempW11)*esp_tempW8)/100);
        	      }
        	      else 
        	      {
        	              ;
                    }
        	          #else
        	          MFC_Current_new = esp_tempW11 + (int16_t)((((int32_t)esp_tempW11)*esp_tempW7)/100);
                  	#endif
        	      }
        	      else 
                  	{
                  	#if(__ESC_CYCLE_DETECTION)
        	          if(ldespu1ESC1stCycle == 1)
                    {
                  	MFC_Current_new = esp_tempW11;
                  	}
                    else if(ldespu1ESC2ndCycle == 1)
                  	{
        	              MFC_Current_new = esp_tempW11 + (int16_t)((((int32_t)esp_tempW11)*esp_tempW8)/100);
                  	}
                  	else
                  	{
                      	;
                  	}
        	          #else
                  	MFC_Current_new = esp_tempW11;
                  	#endif
                                                                                     
                MFC_Current_new = LCTCS_s16ILimitRange(MFC_Current_new, esp_tempW6, esp_tempW5);
              	
        	      }   	  
        	  
            }
            else
            {
            	;
            }                 
        /************/
        	#if(__ESC_TCMF_CONTROL ==1)        	
        	if((lcu1OVER2WHControlFlag_RL ==1)&&(FR_ESC.lcespu1TCMFCon_FadeOut ==1))
        	{
        		FR_ESC.MFC_Current_Inter = 0;
        	}
        	else if((lcu1OVER2WHControlFlag_RR ==1)&&(FL_ESC.lcespu1TCMFCon_FadeOut ==1))
        	{
        		FL_ESC.MFC_Current_Inter = 0;
        }
        else
        {
        	  ;
        }
        	#endif
        }
        else
        {
        	  ;
    }
    
    return MFC_Current_new;
        
}
        
int16_t LAMFC_s16SetMFCCurrentESPUS2WL(int16_t MFC_Current_old)
    {
    int16_t  MFC_Current_new = 0;
    
        esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_UND_2WH_H_LIMIT_TC_CUR_HM, S16_UND_2WH_H_LIMIT_TC_CUR_MM, S16_UND_2WH_H_LIMIT_TC_CUR_LM);
        esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu, S16_UND_2WH_L_LIMIT_TC_CUR_HM, S16_UND_2WH_L_LIMIT_TC_CUR_MM, S16_UND_2WH_L_LIMIT_TC_CUR_LM);


        if ((lcu1US2WHFadeout_RL == 1) || (lcu1US2WHFadeout_RR == 1))
        {
            if ((MFC_Current_old == TC_CURRENT_MAX) && (lcu8US2WH_count <= U8_UND_2WH_USC_INITIAL_FULL_TIME))
            {
                MFC_Current_new = esp_tempW6;
            }
            else if (MFC_Current_old > (esp_tempW6+S16_UND_2WH_FADE_OUT_DEC))
            {                                      
                MFC_Current_new = MFC_Current_old - S16_UND_2WH_FADE_OUT_DEC;
            }                                      
            else                                   
            {
                MFC_Current_new = esp_tempW6;
            }            
        }
        else if (lcu8US2WH_count <= U8_UND_2WH_USC_INITIAL_FULL_TIME)
        {
            MFC_Current_new = TC_CURRENT_MAX;
        }
        else
        {
              #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        	if(lcu1US2WHControlFlag_RL == 1)
        	{
        		  esp_tempW10 = FL_ESC.slip_control;
        	}
        	else if(lcu1US2WHControlFlag_RR == 1)
        	{
        		  esp_tempW10 = FR_ESC.slip_control;
        	}
        	else
        	{
        	    ;
        	}
        	  #else
        	esp_tempW10 = slip_control_front
        	  #endif  
        		
            esp_tempW0 = LCESP_s16IInter5Point(-esp_tempW10, S16_UND_2WH_USC_F_HM_1,  S16_UND_2WH_USC_TC_CUR_HM_1,
                                                             S16_UND_2WH_USC_F_HM_2,  S16_UND_2WH_USC_TC_CUR_HM_2,
                                                             S16_UND_2WH_USC_F_HM_3,  S16_UND_2WH_USC_TC_CUR_HM_3,
                                                             S16_UND_2WH_USC_F_HM_4,  S16_UND_2WH_USC_TC_CUR_HM_4,
                                                             S16_UND_2WH_USC_F_HM_5,  S16_UND_2WH_USC_TC_CUR_HM_5);
            
            esp_tempW1 = LCESP_s16IInter5Point(-esp_tempW10, S16_UND_2WH_USC_F_MM_1,  S16_UND_2WH_USC_TC_CUR_MM_1, 
                                                             S16_UND_2WH_USC_F_MM_2,  S16_UND_2WH_USC_TC_CUR_MM_2, 
                                                             S16_UND_2WH_USC_F_MM_3,  S16_UND_2WH_USC_TC_CUR_MM_3, 
                                                             S16_UND_2WH_USC_F_MM_4,  S16_UND_2WH_USC_TC_CUR_MM_4, 
                                                             S16_UND_2WH_USC_F_MM_5,  S16_UND_2WH_USC_TC_CUR_MM_5);                                                                   
                                                                   
            esp_tempW2 = LCESP_s16IInter5Point(-esp_tempW10, S16_UND_2WH_USC_F_LM_1,  S16_UND_2WH_USC_TC_CUR_LM_1, 
                                                             S16_UND_2WH_USC_F_LM_2,  S16_UND_2WH_USC_TC_CUR_LM_2, 
                                                             S16_UND_2WH_USC_F_LM_3,  S16_UND_2WH_USC_TC_CUR_LM_3, 
                                                             S16_UND_2WH_USC_F_LM_4,  S16_UND_2WH_USC_TC_CUR_LM_4, 
                                                             S16_UND_2WH_USC_F_LM_5,  S16_UND_2WH_USC_TC_CUR_LM_5);
            
            MFC_Current_new = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);
        }
       
    return MFC_Current_new;

    }
    
   #endif

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
static void LAESP_vTcTagetPress(void)
{
  #if (__SPLIT_TYPE==1)
    if((ESP_BRAKE_CONTROL_FL==1) || (ESP_BRAKE_CONTROL_FR==1))
    {
		    laesps16PriTcTargetPress = MPRESS_100BAR;
	  }
	  else
	  {
	  	  laesps16PriTcTargetPress = MPRESS_0BAR;
	  }
  #else
    if((ESP_BRAKE_CONTROL_RL==1) || (ESP_BRAKE_CONTROL_FR==1))
    {
    	  if(ESP_BRAKE_CONTROL_FR==1)
    	  {
    	  	#if(__IDB==ENABLE)
			  esp_tempW1 = FR_ESC.MFC_Current_Inter;
			  laesps16PriTcTargetPress = LCESP_s16IInter5Point(esp_tempW1, S16TCSCpTCcrnt_1,S16TCSCpWhlPre_1,
																			S16TCSCpTCcrnt_2,S16TCSCpWhlPre_2,
																			S16TCSCpTCcrnt_3,S16TCSCpWhlPre_3,
																			S16TCSCpTCcrnt_4,S16TCSCpWhlPre_4,
																			S16TCSCpTCcrnt_5, S16TCSCpWhlPre_5);
    	  	#else
    	  	  laesps16PriTcTargetPress = MPRESS_100BAR;
    	  	#endif
    	  }
    	  else if(SLIP_CONTROL_BRAKE_RL==1)
    	  {
    	  	  laesps16PriTcTargetPress = lcesps16AHB_CurTargetPress;
    	  }
    	  else
    	  {
    	  	  ;
    	  }
    	  
    	  if((ESP_PULSE_DUMP_FR ==1)&&(SLIP_CONTROL_BRAKE_FR==0)&&(SLIP_CONTROL_BRAKE_RL==1)) //FR FadeOut + RL Control
    	  {
    	  	  laesps16PriTcTargetPress = lcesps16AHB_CurTargetPress;
    	  }
    	  else if((ESP_PULSE_DUMP_FR ==1)&&(SLIP_CONTROL_BRAKE_FR==0)) //FR FadeOut
    	  {
    	  	  ;//laesps16PriTcTargetPress = laesps16PriTcTargetPress - S16_AHB_REAR_TAR_DEC_DEL_PRE;
    	  }
    	  else if((ESP_PULSE_DUMP_RL ==1)&&(SLIP_CONTROL_BRAKE_RL==0)) //RL FadeOut
    	  {
    	  	  laesps16PriTcTargetPress = lcesps16AHB_CurTargetPress;
    	  }
    	  else
    	  {
    	  	  ;
    	  }
    	  
    	  if ((ESP_BRAKE_CONTROL_RL == 1) && (BTCS_fr == 1))
    	  {
    	      latcss16PriTcTargetPress = laesps16PriTcTargetPress;
    	  }
    	  else
    	  {
    	    ;
    	  }
		  
		  if(laesps16PriTcTargetPress <= MPRESS_0BAR)
		  {
			  laesps16PriTcTargetPress = MPRESS_0BAR;
		  }
		  else
		  {
			  ;
		  }	  
	  }
	  else
	  {
	  	  laesps16PriTcTargetPress = MPRESS_0BAR;
	  }
  #endif /* (__SPLIT_TYPE==1) */


  #if (__SPLIT_TYPE==1)
    if((ESP_BRAKE_CONTROL_RL==1) || (ESP_BRAKE_CONTROL_RR==1))
	  {
	  	  if(SLIP_CONTROL_BRAKE_RL==1)
	  	  {
	  	  	  laesps16SecTcTargetPress = lcesps16AHB_CurTargetPress;
	  	  }
	  	  else if(SLIP_CONTROL_BRAKE_RR==1)
	  	  {
	  	  	  laesps16SecTcTargetPress = lcesps16AHB_CurTargetPress;
	  	  }
	  	  else
	  	  {
	  	  	  ;
	  	  }
	  }
	  else
	  {
	  	  laesps16SecTcTargetPress = MPRESS_0BAR;
	  }
  #else
    if((ESP_BRAKE_CONTROL_FL==1) || (ESP_BRAKE_CONTROL_RR==1))
	  {
	  	  if(ESP_BRAKE_CONTROL_FL==1)
	  	  {
	  	  	#if(__IDB_LOGIC==ENABLE)
			  esp_tempW1 = FL_ESC.MFC_Current_Inter;
			  laesps16SecTcTargetPress = LCESP_s16IInter5Point(esp_tempW1, S16TCSCpTCcrnt_1,S16TCSCpWhlPre_1,
																			S16TCSCpTCcrnt_2,S16TCSCpWhlPre_2,
																			S16TCSCpTCcrnt_3,S16TCSCpWhlPre_3,
																			S16TCSCpTCcrnt_4,S16TCSCpWhlPre_4,
																			S16TCSCpTCcrnt_5, S16TCSCpWhlPre_5);
    	  	#else
	  	  	  laesps16SecTcTargetPress = MPRESS_100BAR;
    	  	#endif
	  	  }
	  	  else if(SLIP_CONTROL_BRAKE_RR==1)
	  	  {
	  	  	  laesps16SecTcTargetPress = lcesps16AHB_CurTargetPress;
	  	  }
	  	  else
	  	  {
	  	  	  ;
	  	  }
	  	  
	  	  if((ESP_PULSE_DUMP_FL ==1)&&(SLIP_CONTROL_BRAKE_FL==0)&&(SLIP_CONTROL_BRAKE_RR==1)) //FL FadeOut + RR Control
    	  {
    	  	  laesps16SecTcTargetPress = lcesps16AHB_CurTargetPress;
    	  }
    	  else if((ESP_PULSE_DUMP_FL ==1)&&(SLIP_CONTROL_BRAKE_FL==0)) //FL FadeOut
    	  {
    	  	  ;//laesps16SecTcTargetPress = laesps16SecTcTargetPress - S16_AHB_REAR_TAR_DEC_DEL_PRE;
    	  }
    	  else if((ESP_PULSE_DUMP_RR ==1)&&(SLIP_CONTROL_BRAKE_RR==0)) //RR FadeOut
    	  {
    	  	  laesps16SecTcTargetPress = lcesps16AHB_CurTargetPress;
    	  }
    	  else
    	  {
    	  	  ;
    	  }
    	  if ((ESP_BRAKE_CONTROL_RR == 1) && (BTCS_fl == 1))
    	  {
    	      latcss16SecTcTargetPress = laesps16SecTcTargetPress;
    	  }
    	  else
    	  {
    	    ;
    	  }
		  
		  if(laesps16SecTcTargetPress <= MPRESS_0BAR)
		  {
			  laesps16SecTcTargetPress = MPRESS_0BAR;
		  }
		  else
		  {
			  ;
		  }	 
	  }
	  else
	  {
	  	  laesps16SecTcTargetPress = MPRESS_0BAR;
	  }
  #endif /* (__SPLIT_TYPE==1) */

}

void LAESP_vTcTagetPressRear(void)
{	  
	  lcespu1AHB_Rear_ControlOld = lcespu1AHB_Rear_Control;
	  
	  /***** Setting Inhibition Flag *****/
	  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	  if((VDC_DISABLE_FLG==1)||(lsespu1AHBGEN3MpresBrkOn==1))
	  #else
	  if((VDC_DISABLE_FLG==1)||(MPRESS_BRAKE_ON==1))
	  #endif
    {
        lcespu1AHB_Rear_ControlInhibitFlag = 1;
    }
    else
    {
    	  lcespu1AHB_Rear_ControlInhibitFlag = 0;
    }
	  
	  /*****Manipulating USC Target Pressure*****/
	  if((SLIP_CONTROL_BRAKE_RL ==1)||(SLIP_CONTROL_BRAKE_RR ==1))
	  {
	  	  if(SLIP_CONTROL_BRAKE_RL ==1)
        {
        	  esp_tempW10 = RL_ESC.slip_control;
        }
        else if(SLIP_CONTROL_BRAKE_RR ==1)
        {
        	  esp_tempW10 = RR_ESC.slip_control;
        }
        else
        {
        	  ;
        }
        
        
        esp_tempW0 = LCESP_s16IInter5Point(-esp_tempW10, S16_AHB_REF_USC_R_HM_1,  S16_AHB_TCMF_TARGET_PRE_HM_1,
                                                         S16_AHB_REF_USC_R_HM_2,  S16_AHB_TCMF_TARGET_PRE_HM_2,
                                                         S16_AHB_REF_USC_R_HM_3,  S16_AHB_TCMF_TARGET_PRE_HM_3,
                                                         S16_AHB_REF_USC_R_HM_4,  S16_AHB_TCMF_TARGET_PRE_HM_4,
                                                         S16_AHB_REF_USC_R_HM_5,  S16_AHB_TCMF_TARGET_PRE_HM_5);
        
        esp_tempW1 = LCESP_s16IInter5Point(-esp_tempW10, S16_AHB_REF_USC_R_MM_1,  S16_AHB_TCMF_TARGET_PRE_MM_1, 
                                                         S16_AHB_REF_USC_R_MM_2,  S16_AHB_TCMF_TARGET_PRE_MM_2, 
                                                         S16_AHB_REF_USC_R_MM_3,  S16_AHB_TCMF_TARGET_PRE_MM_3, 
                                                         S16_AHB_REF_USC_R_MM_4,  S16_AHB_TCMF_TARGET_PRE_MM_4, 
                                                         S16_AHB_REF_USC_R_MM_5,  S16_AHB_TCMF_TARGET_PRE_MM_5);
                                                                                                                      
        esp_tempW2 = LCESP_s16IInter5Point(-esp_tempW10, S16_AHB_REF_USC_R_LM_1,  S16_AHB_TCMF_TARGET_PRE_LM_1, 
                                                         S16_AHB_REF_USC_R_LM_2,  S16_AHB_TCMF_TARGET_PRE_LM_2, 
                                                         S16_AHB_REF_USC_R_LM_3,  S16_AHB_TCMF_TARGET_PRE_LM_3,
                                                         S16_AHB_REF_USC_R_LM_4,  S16_AHB_TCMF_TARGET_PRE_LM_4, 
                                                         S16_AHB_REF_USC_R_LM_5,  S16_AHB_TCMF_TARGET_PRE_LM_5);
	  
	      lcesps16AHB_UscTargetPress = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2);
	  }
	  else
	  {
	  	  lcesps16AHB_UscTargetPress = 0;
	  }
	  
	  
	  /***** Setting Control Phase *****/	  	  
	  if(
	  	((SLIP_CONTROL_BRAKE_RL == 1)||(SLIP_CONTROL_BRAKE_RR == 1))
	    &&(lcespu1AHB_Rear_ControlInhibitFlag ==0)
	    )
	  {
	  	  lcespu1AHB_Rear_Control = 1;
	  	  
	  	  if((lcesps16AHB_CurTargetPress < lcesps16AHB_UscTargetPress)&&(lcespu1AHB_ControlPhase ==0))
	      {
	  	      lcespu1AHB_InitialPhase = 1;
	  	      lcespu1AHB_ControlPhase = 0;
	  	      lcespu1AHB_FadeOutPhase = 0;
	      }
	      else
	      {	      	
	      	  lcespu1AHB_InitialPhase = 0;
	  	      lcespu1AHB_ControlPhase = 1;
	  	      lcespu1AHB_FadeOutPhase = 0;
	      }
	  }
	else if(((ESP_PULSE_DUMP_RL ==1)||(ESP_PULSE_DUMP_RR ==1))&&(lcespu1AHB_Rear_ControlInhibitFlag ==0))
	{
		lcespu1AHB_Rear_Control = 0;
		
		lcespu1AHB_InitialPhase = 0;
	  	lcespu1AHB_ControlPhase = 0;
	  	lcespu1AHB_FadeOutPhase = 1;
	}
	else
	{
		lcespu1AHB_Rear_Control = 0;
		
		lcespu1AHB_InitialPhase = 0;
		lcespu1AHB_ControlPhase = 0;
		lcespu1AHB_FadeOutPhase = 0;
	}
	  	
	  /*
	  else
	  {
	  	  lcespu1AHB_Rear_Control = 0;
	  	    	  
	  	  if(
	  	  	(lcespu1AHB_Rear_ControlInhibitFlag ==0)
	  	  	&&(lcespu1AHB_Rear_ControlOld ==1)&&(lcespu1AHB_Rear_Control ==0)&&(lcespu1AHB_FadeOutPhase ==0)
	  	  	&&(lcesps16AHB_FadeOutCount <= S16_AHB_REAR_FADEOUT_TIME)
	  	  	)
	  	  {
	  	  	  lcesps16AHB_FadeOutCount = lcesps16AHB_FadeOutCount +1;
	  	  	  
	  	  	  lcespu1AHB_InitialPhase = 0;
	  	      lcespu1AHB_ControlPhase = 0;
	  	      lcespu1AHB_FadeOutPhase = 1;
	  	  }
	  	  else if(
	  	  	     (lcespu1AHB_Rear_ControlInhibitFlag ==0)
	  	  	     &&(lcespu1AHB_Rear_Control ==0)&&(lcespu1AHB_FadeOutPhase ==1)
	  	  	     &&(lcesps16AHB_FadeOutCount <= S16_AHB_REAR_FADEOUT_TIME)
	  	  	     &&(lcesps16AHB_CurTargetPress >0)
	  	  	     )
	  	  {
	  	  	  lcesps16AHB_FadeOutCount = lcesps16AHB_FadeOutCount +1;
	  	  	  
	  	  	  lcespu1AHB_InitialPhase = 0;
	  	      lcespu1AHB_ControlPhase = 0;
	  	      lcespu1AHB_FadeOutPhase = 1;
	  	  }
	  	  else
	  	  {
	  	  	  lcesps16AHB_FadeOutCount = L_U8_TIME_10MSLOOP_0MS;
	  	  	  
	  	  	  lcespu1AHB_InitialPhase = 0;
	  	      lcespu1AHB_ControlPhase = 0;
	  	      lcespu1AHB_FadeOutPhase = 0;
	  	  }
	  	  
	  	  if (lcesps16AHB_FadeOutCount >= L_U16_TIME_10MSLOOP_10S)
        {
        	  lcesps16AHB_FadeOutCount = L_U16_TIME_10MSLOOP_10S;
        }
        else
        {
        	  ;
        }
	  	  
	  }	  
	  */	  
	  
	  /*****Manipulating Current Target Pressure*****/ 
	  if(lcespu1AHB_InitialPhase ==1)
	  {
	  	  lcesps16AHB_CurTargetPress = lcesps16AHB_CurTargetPress + S16_AHB_REAR_TAR_INC_DEL_PRE;
	  }
	  else if(lcespu1AHB_ControlPhase ==1)
	  {
	  	  lcesps16AHB_CurTargetPress = lcesps16AHB_UscTargetPress;
	  }
	  else if(lcespu1AHB_FadeOutPhase ==1)
	  {
	  	  lcesps16AHB_CurTargetPress = lcesps16AHB_CurTargetPress - S16_AHB_REAR_TAR_DEC_DEL_PRE;
	  }
	  else
	  {
	  	  lcesps16AHB_CurTargetPress = MPRESS_0BAR;
	  }
	  
	  if(lcesps16AHB_CurTargetPress <= MPRESS_0BAR)
	  {
	  	  lcesps16AHB_CurTargetPress = MPRESS_0BAR;
	  }
	  else
	  {
	  	  ;
	  }	    
}
#endif /* __AHB_GEN3_SYSTEM == ENABLE */

#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAESPCallActHW
	#include "Mdyn_autosar.h"               
#endif
