#ifndef __LCRDCMCALLCONTROL_H__
#define __LCRDCMCALLCONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"
#include "hm_logic_var.h"
/*Global MACRO CONSTANT Definition******************************************************/

/*Global Extern Variable  Declaration*******************************************************************/


  	#if __RDCM_PROTECTION

extern struct  bit_position RDCMflag_00;
extern struct  bit_position RDCMflag_01;
extern struct  bit_position RDCMflag_02;
extern struct  bit_position RDCMflag_03;
extern struct  bit_position RDCMflag_04;
extern struct  bit_position RDCMflag_05;
extern struct  bit_position RDCMflag_06;
extern struct  bit_position RDCMflag_07;

#define ltu1RDCMProtENGOn			RDCMflag_00.bit0
#define ltu1RDCMProtBRKOn			RDCMflag_01.bit1
#define ltu1RDCMProtectionReq		RDCMflag_02.bit2
#define ltu1RDCMProtENGSustain1Sec	RDCMflag_03.bit3
#define ltu1RDCMActReference	 	RDCMflag_04.bit4
#define RDCM_FLAGS_RESERVED00_5		RDCMflag_05.bit5
#define RDCM_FLAGS_RESERVED00_6		RDCMflag_06.bit6
#define RDCM_FLAGS_RESERVED00_7		RDCMflag_07.bit7

extern int16_t    RDCM_cmd;					
extern int16_t	lts16DeltaRPMForRDCM; 		
extern int16_t	lts16DeltaRPMErrForRDCM; 	
extern int16_t	lts16TargetRPMForRDCM;		
extern int16_t	lts16DeltaRPMErrDiffForRDCM;
extern int16_t	lts16PGainForRDCM;			
extern int16_t	lts16DGainForRDCM;			
extern int16_t	lts16PDGainEffectForRDCM;	
extern int16_t	lts16TorqRiseforRDCM;
extern int16_t	lts16RDCMOKTorq;
extern uint8_t  ltu8RDCMStatus;				
extern uint8_t  ltcsu81SecCntAfterRDCM;
extern uint8_t  ltu8RDCM1stControlCount;
extern uint8_t  RDCM_FTCS_flags;

extern int16_t lts16DeltaRPMFiltOldForRDCM;   
extern int16_t lts16DeltaRPMFiltForRDCM;      
extern uint16_t ltu16DrivenWheelAvgSpeed;     
extern uint16_t ltu16NonDrivenWheelAvgSpeed;  
extern uint16_t ltu16TireDynamicLength;       
extern uint16_t ltu16MainWheelAvgRPM;         
extern uint16_t ltu16SubWheelAvgRPM;          
extern int16_t lts16PGainEffectForRDCM;       
extern int16_t lts16DGainEffectForRDCM;	    

	#endif /*__RDCM_PROTECTION*/

/*Global MACRO FUNCTION Definition******************************************************/
#endif
