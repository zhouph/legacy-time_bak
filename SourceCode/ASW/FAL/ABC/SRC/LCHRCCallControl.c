/******************************************************************************
* Project Name: Hill Roll Back control
* File: LCHRCCallControl.C
* Description: Hill Roll Back control Controller
* Date: June. 4. 2011
******************************************************************************/

/* Includes ******************************************************************/
#include "LCHRCCallControl.h"
#include "LCHSACallControl.h"
#include "LSABSCallSensorSignal.h"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LCESPCalLpf.h"
#include "LCESPCalInterpolation.h"
#include "LCHDCCallControl.h"
#include "Hardware_Control.h"
#include "LSESPFilterEspSensor.h"
#include "LCEPBCallControl.h"
#include "LSESPCalSensorOffset.h"
#include "hm_logic_var.h"



#if defined(__dDCT_INTERFACE)
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/

struct	U8_BIT_STRUCT_t HRCF0;
struct	U8_BIT_STRUCT_t HRCF1;
struct	U8_BIT_STRUCT_t HRCF2;
struct	U8_BIT_STRUCT_t HRCF3;
struct	U8_BIT_STRUCT_t HRCF4;
struct	U8_BIT_STRUCT_t HRCF5;


   


uint8_t	    lcu8HrcTmReqHsaActLevel, lcu8HrcTmReqHsaDecayLevel;
uint8_t     lcu8HrcRollingDirection, lcu8HrcGearState;
uint8_t		lcu8HrcTmReqHrcTargetSpdLevel;

int16_t		lcs16HrcReqHsaActMinSlope, lcs16HrcReqHsaDecayRate;
int16_t		lcs16HrcTargetSpd;






/*Global Variable Declaration*************************************************/



/* LocalFunction prototype ***************************************************/
void	LCHRC_vChkInhibition(void);
static void	LCHRC_vDctHrcGearCondition(void);
static void	LCHRC_vDctHrcEnterCondition(void);
static void	LCHRC_vSetHsaActMinGrade(void);
static void	LCHRC_vSetHsaDecayRate(void);
static void	LCHRC_vSetTargetSpeed(void);
/* GlobalFunction prototype **************************************************/
extern 	int16_t abs(int16_t j); 


/* Implementation*************************************************************/




/******************************************************************************
* FUNCTION NAME:      LCHRC_vCallControl
* CALLED BY:          StartupLogic()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        HRC controller main function
******************************************************************************/
void 	LCHRC_vCallControl(void)
{
		LCHRC_vChkInhibition();
		LCHRC_vDctHrcGearCondition();
		LCHRC_vDctHrcEnterCondition();
		LCHRC_vSetHsaActMinGrade(); 
		LCHRC_vSetHsaDecayRate();
		LCHRC_vSetTargetSpeed();
}

void	LCHRC_vChkInhibition(void)
{
	if((HSA_INHIBITION_flag==1) || (lcu1HdcInhibitFlg==1))
	{
		lcu1HrcInhibitFlg = 1;
	}
	else
	{
	    lcu1HrcInhibitFlg = 0;
	}
}

static void	LCHRC_vDctHrcTmType(void)
{
	if((lespu1EMS_AT_TCU==1)||(lespu1EMS_CVT_TCU==1)||(lespu1EMS_DCT_TCU==1)||(lespu1EMS_HEV_AT_TCU==1))
	{
		HRC_TM_TYPE = HRC_AUTO_TM;
	}
	else
	{
		HRC_TM_TYPE = HRC_MANUAL_TM;
	}
}

static void	LCHRC_vDctHrcGearCondition(void)
{
    /* gs_sel -> 0:P 1:L 2:2 3:3 4:DS mode 5:D 6:N 7:R 8:sport mode/manual */		
    if((gs_sel==1)||(gs_sel==2)||(gs_sel==3)||(gs_sel==4)||(gs_sel==5)||(gs_sel==8))
    {
        lcu8HrcGearState = HRC_DRIVE;
    }
    else if(gs_sel==7)
    {
    	lcu8HrcGearState = HRC_REVERSE;
    }
    else
    {
    	lcu8HrcGearState = HRC_NEUTRAL;
    }
}

static void	LCHRC_vDctHrcEnterCondition(void)
{
  #if __TOSS_SIGNAL_USE == ENABLE
	/*lcu8HrcRollingDirection = toss ��ȣ*/

	if((lcu8HrcRollingDirection==HRC_FORWARD_ROLL)&&(lcu8HrcGearState==HRC_REVERSE))
	{
		lcu1HrcOnReqFlg = 1;
	}
	else if((lcu8HrcRollingDirection==HRC_BACKWARD_ROLL)&&(lcu8HrcGearState==HRC_DRIVE))
	{
		lcu1HrcOnReqFlg = 1;
	}
	else
	{
		lcu1HrcOnReqFlg = 0;
	}
  #else

    if((lcu1HrcOnReqFlg==0)&&(lcu1HrcInhibitFlg==0))
    {
    	#if __TEST_MODE_ESC_DISABLE_SW==1
    	if((fu1ESCDisabledBySW==1)&&(((HSA_flg==1)&&(HSA_Control==HSA_RELEASE)&&(mtp<3)&&(lcu8HrcGearState==HRC_NEUTRAL))
    	||((lcu1hdcBackRollHillFlg==1)&&(lcu8HrcGearState==HRC_NEUTRAL))
    	||((lcu8HrcGearState==HRC_NEUTRAL)&&(lcu1hdcVehicleBackward==1)&&(lcu1HdcDownhillFlg==1))
    	||((lcu8HrcGearState==HRC_NEUTRAL)&&(lcu1hdcVehicleForward==1)&&(lcu1HdcDownhillFlg==1))
    	))    	
    	#else
    	if(((HSA_flg==1)&&(HSA_Control==HSA_RELEASE)&&(mtp<3)&&(lcu8HrcGearState==HRC_NEUTRAL))
    	||((lcu1hdcBackRollHillFlg==1)&&(lcu8HrcGearState==HRC_NEUTRAL))
    	||((lcu8HrcGearState==HRC_NEUTRAL)&&(lcu1hdcVehicleBackward==1)&&(lcu1HdcDownhillFlg==1))
    	||((lcu8HrcGearState==HRC_NEUTRAL)&&(lcu1hdcVehicleForward==1)&&(lcu1HdcDownhillFlg==1))
    	)
    	#endif
    	{
    		lcu1HrcOnReqFlg = 1;
    	}
    	else
    	{
    		lcu1HrcOnReqFlg = 0;
    	}
    }
    else
    {
    	if(((lcu8HrcGearState==HRC_REVERSE)&&(lcu1hdcVehicleBackward==1))
    	||((lcu8HrcGearState==HRC_DRIVE)&&(lcu1hdcVehicleForward==1))
    	||((mtp>=3)&&(vref<=VREF_0_5_KPH))
        ||(vref>=VREF_10_KPH)
        ||(HSA_Control==HSA_HOLD)
    	||((lcu1HdcActiveFlg==1)&&(lcu1HdcSwitchFlg==1))
    	#if __TEST_MODE_ESC_DISABLE_SW==1
    	||(fu1ESCDisabledBySW==0)
    	#endif
    	)
    	{
    		lcu1HrcOnReqFlg = 0;
    	}
	    else
	    {
	    	lcu1HrcOnReqFlg = 1;
	    }
    }
  #endif
}

static void	LCHRC_vSetHsaActMinGrade(void)
{
	lcu8HrcTmReqHsaActLevel = 0;  /* need to add TM CAN signal */
	
	if(lcu8HrcTmReqHsaActLevel==3)
	{
		lcs16HrcReqHsaActMinSlope = (int16_t)U8_dDCT_HSA_ACT_LEVEL3_SLOPE;
	}
	else if(lcu8HrcTmReqHsaActLevel==2)
	{
		lcs16HrcReqHsaActMinSlope = (int16_t)U8_dDCT_HSA_ACT_LEVEL2_SLOPE;
	}
	else if(lcu8HrcTmReqHsaActLevel==1)
	{
		lcs16HrcReqHsaActMinSlope = (int16_t)U8_dDCT_HSA_ACT_LEVEL1_SLOPE;
	}
	else
	{
		lcs16HrcReqHsaActMinSlope = S16_HSA_a_uphill_detection_along;
	}
}  

static void	LCHRC_vSetHsaDecayRate(void)
{
	lcu8HrcTmReqHsaDecayLevel = 0; /* need to add TM CAN signal */
}

static void	LCHRC_vSetTargetSpeed(void)
{
	lcu8HrcTmReqHrcTargetSpdLevel = 0; /* need to add TM CAN signal */
	
	if(lcu8HrcTmReqHrcTargetSpdLevel==3)
	{
		lcs16HrcTargetSpd = S16_HRC_TARGET_SPD_LV3;
	}
	else if(lcu8HrcTmReqHrcTargetSpdLevel==2)
	{
		lcs16HrcTargetSpd = S16_HRC_TARGET_SPD_LV2;
	}
	else if(lcu8HrcTmReqHrcTargetSpdLevel==1)
	{
		lcs16HrcTargetSpd = S16_HRC_TARGET_SPD_LV1;
	}
	else /* normal condition */
	{
		lcs16HrcTargetSpd = S16_HRC_TARGET_SPD_LV0;
	}	
	
	/* Low Limit */
	if(lcs16HrcTargetSpd<VREF_2_KPH) 
	{
		lcs16HrcTargetSpd = VREF_2_KPH;
	}
	else { }
}

  
  
#endif /* __dDCT_INTERFACE */
