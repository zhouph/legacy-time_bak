/*******************************************************************/
/*	Program	ID:	MGH-40 AFZ [Decleration]						   */
/*	Program	description: Vehicle Deceleration Estimation		   */
/*	Input files:												   */
/*																   */
/*	Output files:												   */
/*																   */
/*	Special	notes: Will	be applicated to KMC HM(SOP.07.08)		   */
/*******************************************************************/
/*	Modification   Log											   */
/*	Date		   Author			Description					   */
/*	-------		  ----------	-----------------------------	   */
/*	06.10.23	  SeWoong Kim	   ESP Pressure	Using Decel	Est	   */
/*																   */
/*******************************************************************/
/* Includes	********************************************************/
#if !SIM_MATLAB

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallEstVehDecel
	#include "Mdyn_autosar.h"
#endif
#endif
#include	"LVarHead.h"
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#include 	"LCABSDecideCtrlVariables.h"
#if	__VDC
#include 	"LDABSCallVrefCompForESP.H"
#include 	"LDESPCalVehicleModel.h"
#include	"LDESPEstBeta.h"
#include    "LSESPFilterEspSensor.h"
#endif


#include 	"LCallMain.h"
#include 	"LCESPCalInterpolation.h"
#include 	"LCESPCalLpf.h"
#include 	"LCABSCallControl.h"
#include    "LSABSCall2ndOrderLpf.h"

#if	 (__CAR==HMC_GK) ||	(__CAR==SYC_KYRON) || (__CAR==SYC_RAXTON) || (__CAR==HMC_FC)
#define	 __PRESS_MODEL_VREF	   0
#else
#define	 __PRESS_MODEL_VREF	   1
#endif


/* Temp	
#define	 S16_PRESS_COEFF_FRT   88
#define	 S16_PRESS_COEFF_RR	   45
*/

#if	__VDC && __PRESS_EST
  #if  __PRESS_MODEL_VREF

	  #define S16Mass			  (pEspModel->S16_VEHICLE_MASS_FL +	pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL	+ pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */
	  #define a_coeff_FRT		  S16_PRESS_COEFF_FRT				  /* [N/bar]   */
	  #define a_coeff_RR		  S16_PRESS_COEFF_RR				  /* [N/bar]   */
	  #define Wheel_Inertia_FRT	  16*2								  /* ??	10*[N-m-s^2] */
	  #define Wheel_Inertia_RR	  16								  /* ??	10*[N-m-s^2] */
	  #define RollRadius		  (int16_t)(((int32_t)U16_TIRE_L*10)/63)	 /*	[mm] */	  /* opttime*/

  #else
  #if (__CAR==HMC_GK) || (__CAR==ASTRA_MECU)

	#define	S16Mass				1330	   /* Kg */
	#define	a_coeff_FRT			75		/* [N/bar]	 */
	#define	a_coeff_RR			26		/* [N/bar]	 */
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2] */
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2] */
	#define	RollRadius			308		/* [mm]	*/

  #elif	(__CAR==HMC_JM)||(__CAR==HMC_SM)

	#define	S16Mass			   	1760		 /*	Kg */
	#define	a_coeff_FRT			62		/* [N/bar]	 */
	#define	a_coeff_RR			27		/* [N/bar]	 */
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			325		/* [mm]	*/

  #elif	(__CAR==HMC_NF)||(__CAR==HMC_NF_MECU)

	#define	S16Mass			   	1562		 /*	Kg */

	#define	MassFRT				46		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				32		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			81		/* [N/bar]	 */
	#define	a_coeff_RR			26		/* [N/bar]	 */
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			326		/* [mm]	*/

  #elif	(__CAR==SYC_RAXTON)||(__CAR==SYC_RAXTON_MECU)

	#define	S16Mass			   	2096		 /*	Kg */

	#define	MassFRT				58		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				46		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			90		/* ?? [N/bar]	*/
	#define	a_coeff_RR			40		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			369		/* [mm]	*/

  #elif	(__CAR==FORD_C214)||(__CAR==FORD_C214_MECU)

	#define	S16Mass			   	1508		 /*	Kg */

	#define	MassFRT				44		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				31		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			81		/* ?? [N/bar]	*/
	#define	a_coeff_RR			26		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			326		/* ??[mm] */


  #elif	(__CAR==SYC_KYRON)||(__CAR==SYC_KYRON_MECU)

	#define	S16Mass				2026		 /*	Kg */

	#define	MassFRT				55		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				46		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			90		/* ?? [N/bar]	*/
	#define	a_coeff_RR			40		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			368		/* [mm]	*/

  #elif	__CAR==KMC_HM

	#define	S16Mass				2065	   /* Kg */
	#define	a_coeff_FRT			97		/* ?? [N/bar]	*/
	#define	a_coeff_RR			40		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			371		/* [mm]	*/

  #elif	__CAR==HMC_PA

	#define	S16Mass			   	934		 /*	Kg */

	#define	MassFRT				30		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				18		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			75		/* ?? [N/bar]	*/
	#define	a_coeff_RR			26		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			267		/* [mm]	*/

  #elif	(__CAR==GM_C100)||(__CAR==GM_C100_MECU)

	#define	S16Mass			   	1824		 /*	Kg */

	#define	MassFRT				51		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				40		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			85		/* ?? [N/bar]	*/
	#define	a_coeff_RR			40		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			350		/* [mm]	*/

  #elif	(__CAR==GM_TAHOE)||(__CAR==GM_TAHOE_MECU)|| (__CAR ==GM_SILVERADO)

	#define	S16Mass			   	2631		 /*	Kg */

	#define	MassFRT				69		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				63		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			97		/* ?? [N/bar]	*/
	#define	a_coeff_RR			40		/* ?? [N/bar]	*/
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			390		/* [mm]	*/

  #else	 /*	GK */

	#define	S16Mass				1330	   /* Kg */

	#define	MassFRT				43		/* MassFRT/10 [kg/1wheel]	*/
	#define	MassRR				24		/* MassRR/10  [kg/1wheel]	*/
	#define	a_coeff_FRT			75		/* [N/bar]	 */
	#define	a_coeff_RR			26		/* [N/bar]	 */
	#define	Wheel_Inertia_FRT	16*2	/* ?? 10*[N-m-s^2]*/
	#define	Wheel_Inertia_RR	16		/* ?? 10*[N-m-s^2]*/
	#define	RollRadius			308		/* [mm]	*/

  #endif

#endif


#endif


/*******************************************************************/

/* Local Definition	 ***********************************************/
#define		T_AFZ_FREEZE		L_U8_TIME_10MSLOOP_50MS				 /*	7.08 msec resolution */
#define		T_AFZ_CALC_MIN		L_U8_TIME_10MSLOOP_100MS
#define		T_AFZ_CALC_LOW		L_U8_TIME_10MSLOOP_400MS
#define		T_AFZ_CALC_MEDIUM	L_U8_TIME_10MSLOOP_500MS
#define		T_AFZ_CACL_HIGH		L_U8_TIME_10MSLOOP_1000MS
/*
#define		FILTER_1Hz			L_U8FILTER_GAIN_10MSLOOP_1HZ
#define		FILTER_2Hz			L_U8FILTER_GAIN_10MSLOOP_2HZ
#define		FILTER_3Hz			L_U8FILTER_GAIN_10MSLOOP_3HZ
*/
#define		HDC_ACCEL_2G		2000

#define s16VELFilterFreq    20   /* resol 10, 10--> 1Hz */
#define s16VELFilterZeta    10   /* resol 10, 10--> 1   damping coeff  */   
#define s16VELAccMulti      10   /* Diff    multiply        */
#define s16VELdiffcoefCov   43   /* 1scan 1/64kph  => 0.0625g  7ms->10ms 1/16*7/10 */

#define s16WHLFilterFreq    30   /* resol 10, 10--> 1Hz  */
#define s16WHLFilterZeta    10   /* resol 10, 10--> 1   damping coeff  */   
#define s16WHLAccMulti      10   /* Diff multiply        */
#define s16WHLdiffcoefCov   43   /* 1scan 1/64kph  => 0.0625g   */  

/*******************************************************************/

/* Variables Definition*********************************************/
	U8_BIT_STRUCT_t	AFZF0,AFZF1;








#if	__VDC && __PRESS_EST
	int16_t		Vehicle_Decel_EST;
	int16_t		Vehicle_Decel_EST_filter;
    int16_t     Vehicle_Decel_EST_filter_temp;
	int16_t		Vehicle_Decel_Max;
#endif
/* LDABS_vCalVehDecelForEBD	*/
	int16_t		ebd_filt_grv;
	uint8_t		ebd_filt_count;
	int16_t		ebd_refilt_grv;
	int16_t		ebd_sum_array[4];/*	7ms->5ms*/
	int8_t		ebd_high_mu_cnt;

/* LDABS_vCalVehDecelForDoubleB	*/
	int16_t		aref0;
	int16_t		aref_temp[5];
	uint8_t		aref_timer;
	uint8_t		aref_avg_counter;
	int16_t		vref0;
	int16_t		aref_avg;

/* LDABS_vCalVehicleDecelAccel */
	uint16_t	t_afz;
	int16_t		v_afz;
	uint8_t		afz_flags;
	int16_t		v_afz_old, v_afz_old2;
	int16_t		afz_temp;
	uint16_t	t_afz_old, t_afz_old2;
	int8_t		v_afz_time;
	uint8_t		AFZ_SUS_CNT;
	uint8_t		afz_cal_delay;

/* LDABS_vDctVrefIncrement */
	uint16_t	vref_increase_cnt;
	int16_t		vref_trough;
	int16_t		lsabss16DecelRefiltByVref5;
	uint8_t		lsabsu8Decelvalidity;
/* LDABS_vCalVehDecelForHDC	 */
#if	__HDC
	uint8_t	 	lsabsu8WheelSelectChangeCNT;
	uint8_t	 	lsabsu8HDCdecelInputChangeCNT;
	int16_t	   	lsabss16HDCWheelAccel;
	int16_t	   	lsabss16VrselectOLD;
	int16_t	   	sumArrayHDC[4];
	int16_t	   	lsabss16HDCvelocity;
	int16_t	   	lsabss16HDCveloDiff;
	int16_t	   	lsabss16HDCveloDiffFinal;
#endif

#if __SPIN_DET_IMPROVE == ENABLE
	int16_t     ls16wheelaccel;
	int16_t     ls16wheelgaccel;
	int16_t     ls16accelacc;
	int16_t     ls16accel;

	int16_t     ls16wheelaccelFL;
	int16_t     ls16wheelaccelFR;
	int16_t     ls16wheelaccelRL;
	int16_t     ls16wheelaccelRR;
	int16_t     ls16wheelgaccelFL;
	int16_t     ls16wheelgaccelFR;
	int16_t     ls16wheelgaccelRL;
	int16_t     ls16wheelgaccelRR;
#endif


struct  ACC_FILTER  V_ACC;
#if __SPIN_DET_IMPROVE == ENABLE
struct  ACC_FILTER WHEEL_G_ACC , ACC_ACC, ACC_ACC_2, FL_ACC, FR_ACC, RL_ACC, RR_ACC, FL_ACC_ACC, FR_ACC_ACC, RL_ACC_ACC, RR_ACC_ACC;
#endif
/*******************************************************************/

/* Local Function prototype	****************************************/
#if	__VDC && __PRESS_EST
void	LDABS_vEstVehicleDecelMu(void);
void	LDABS_vEstFrontDecelMu(struct W_STRUCT *WL);
void	LDABS_vEstRearDecelMu(struct W_STRUCT *WL);
#endif

#if (__4WD ||(__4WD_VARIANT_CODE==ENABLE))	&& __AX_SENSOR
void	LDABS_vDetLongAccUse(void);
#endif


void	LDABS_vCalVehDecelForEBD(void);
void	LDABS_vCalVehDecelForDoubleB(void);
void	LDABS_vCalVehicleDecelAccel(void);
void	LDABS_vDctVrefIncrement(void);
void	LDABS_vCallEstVehDecel(void);


#if	__HDC
void	LDABS_vCalVehDecelForHDC(void);
void	LDABS_vCalVehicleAccelInputChange(void);
void	LDABS_vCalWheelSelectChange(void);
#endif

void	LDABS_vCalVehDecelbyVref5(void); /*	2010.04.28 */


/*******************************************************************/
void	LDABS_vCallEstVehDecel(void)
{
	LDABS_vCalVehicleDecelAccel();
	LDABS_vCalVehDecelForEBD();
	LDABS_vCalVehDecelForDoubleB();
	#if	__VDC && __PRESS_EST
	LDABS_vEstVehicleDecelMu();
	#endif
	LDABS_vCalVehDecelbyVref5(); /*	2010.04.28 */

}
/*******************************************************************/
#if	__VDC && __PRESS_EST
void	LDABS_vEstVehicleDecelMu(void)
{

	LDABS_vEstFrontDecelMu(&FL);
	LDABS_vEstFrontDecelMu(&FR);
	LDABS_vEstRearDecelMu(&RL);
	LDABS_vEstRearDecelMu(&RR);
	/*
	tempL0=(int32_t)(FL.Estimated_Force) + (int32_t)(FR.Estimated_Force) +
		   (int32_t)(RL.Estimated_Force) + (int32_t)(RR.Estimated_Force) ;
	tempL1=	((tempL0*10)/(S16Mass));
	Vehicle_Decel_EST= -(int16_t)(tempL1);
	
	tempW0=(Vehicle_Decel_EST);
	tempW1=(Vehicle_Decel_EST_filter);
	Vehicle_Decel_EST_filter=LCESP_s16Lpf1Int(tempW0, tempW1, L_U8FILTER_GAIN_10MSLOOP_6_5HZ); //6.5Hz
	*/
	Vehicle_Decel_EST= -(int16_t)((((int32_t)(FL.Estimated_Force + FR.Estimated_Force +	RL.Estimated_Force + RR.Estimated_Force))*10)/(S16Mass));  /*opttime : ksw*/
    /*
	Vehicle_Decel_EST_filter=LCESP_s16Lpf1Int(Vehicle_Decel_EST, Vehicle_Decel_EST_filter, L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
    */
    
	Vehicle_Decel_EST_filter_temp=LCESP_s16Lpf1Int((10*Vehicle_Decel_EST), Vehicle_Decel_EST_filter_temp, L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
    Vehicle_Decel_EST_filter = Vehicle_Decel_EST_filter_temp/10;
    
    
    
}
/*******************************************************************/
void	LDABS_vEstFrontDecelMu(struct W_STRUCT *WL)
{

	int16_t	BrakeForce;
	int16_t	InertiaForce;
	int16_t	WheelOmegaDot;
/*
	tempW0=10;
	tempW1=a_coeff_FRT;
	tempW2=WL->s16_Estimated_Active_Press;
	s16muls16divs16();
	BrakeForce=tempW3;
*/
	BrakeForce=(int16_t) ( ((int32_t) WL->s16_Estimated_Active_Press * a_coeff_FRT)	/ 10 );
/*
	tempW0=RollRadius;
	tempW1=(int16_t)WL->arad;
	tempW2=25*100;
	s16muls16divs16();
	WheelOmegaDot=tempW3;*/	 /*[rad/s^2] */
	WheelOmegaDot=(int16_t)	( ((int32_t) WL->arad*2500 ) / RollRadius );
/*
	tempW0=RollRadius;
	tempW1=WheelOmegaDot;
	tempW2=Wheel_Inertia_FRT*100;
	s16muls16divs16();
	InertiaForce=tempW3;
*/
	InertiaForce=(int16_t) ( ((int32_t)	Wheel_Inertia_FRT*100 *	WheelOmegaDot)/	RollRadius );
	WL->Estimated_Force	= BrakeForce+InertiaForce;

	if(WL->Estimated_Force < 0 )
	{
		WL->Estimated_Force=0;
	}
	else
	{
		;
	}


}
/*******************************************************************/
void	LDABS_vEstRearDecelMu(struct W_STRUCT *WL)
{

	int16_t	BrakeForce;
	int16_t	InertiaForce;
	int16_t	WheelOmegaDot;
/*
	tempW0=10;
	tempW1=a_coeff_RR;
	tempW2=WL->s16_Estimated_Active_Press;
	s16muls16divs16();
	BrakeForce=tempW3;
*/
	BrakeForce=(int16_t) ( ((int32_t) WL->s16_Estimated_Active_Press * a_coeff_RR) / 10	);
/*
	tempW0=RollRadius;
	tempW1=(int16_t)WL->arad;
	tempW2=25*100;
	s16muls16divs16();
	WheelOmegaDot=tempW3;  // [rad/s^2]	
	WheelOmegaDot=(int16_t)	( ((int32_t) WL->arad*2500)	/ RollRadius );
*/
    WheelOmegaDot=(int16_t) ((int32_t)(WL->arad*2500) /RollRadius );  /*opttime : ksw*/
/*
	tempW0=RollRadius;
	tempW1=WheelOmegaDot;
	tempW2=Wheel_Inertia_RR*100;
	s16muls16divs16();
	InertiaForce=tempW3;
	InertiaForce=(int16_t) ( ((int32_t)	Wheel_Inertia_RR*100 * WheelOmegaDot)/ RollRadius );
*/
	InertiaForce=(int16_t) ( ((int32_t)	Wheel_Inertia_RR*100 * WheelOmegaDot)/RollRadius );	 /*opttime : ksw*/
 
	WL->Estimated_Force	= BrakeForce+InertiaForce;
	if(WL->Estimated_Force < 0 )
	{
		WL->Estimated_Force=0;
	}
	else
	{
		;
	}

}

#endif
/*******************************************************************/
void	LDABS_vCalVehDecelForEBD(void)
{
 

	#if	__HDC
	lsabss16HDCvelocity=vref5_resol_change;
	#endif

	#if	__4WD_VARIANT_CODE==ENABLE
	if(lsu8DrvMode != DM_2WD)
	{
		tempW0=FZ1.voptfz_resol_change;
	}
	else
	{
	   	#if __REAR_D
		if ((fu1WheelFLSusDet == 1)||(fu1WheelFRSusDet == 1))
/*		if((plus_70g_number_fl!=0)||(plus_70g_number_fr!=0)) */
		{
			tempW0=FZ2.voptfz_resol_change;
		}
	  	#else
		if ((fu1WheelRLSusDet == 1)||(fu1WheelRRSusDet == 1))
/*		if((plus_70g_number_rl!=0)||(plus_70g_number_rr!=0)) */
		{
			tempW0=FZ2.voptfz_resol_change;
		}
	  	#endif
		else
		{
			tempW0=FZ1.voptfz_resol_change;
		}
	}
	#else	/*	__4WD_VARIANT_CODE==ENABLE  */
	#if __4WD
	tempW0=FZ1.voptfz_resol_change;
	#else	 /*	4WD	*/
   	#if __REAR_D
	if ((fu1WheelFLSusDet == 1)||(fu1WheelFRSusDet == 1))
/*	  if((plus_70g_number_fl!=0)||(plus_70g_number_fr!=0)) */
	{
		tempW0=FZ2.voptfz_resol_change;
	}
  	#else
	if ((fu1WheelRLSusDet == 1)||(fu1WheelRRSusDet == 1))
/*	  if((plus_70g_number_rl!=0)||(plus_70g_number_rr!=0)) */
	{
		tempW0=FZ2.voptfz_resol_change;
	}
 	#endif
	else
	{
		tempW0=FZ1.voptfz_resol_change;
	}
	#endif	/*4WD */
	#endif	/*__4WD_VARIANT_CODE==ENABLE  */

	tempW2=tempW0-ebd_sum_array[ebd_filt_count];
	ebd_sum_array[ebd_filt_count]=tempW0;
	
	#if	__HDC
	lsabss16HDCveloDiff=lsabss16HDCvelocity-sumArrayHDC[ebd_filt_count];
	sumArrayHDC[ebd_filt_count]=lsabss16HDCvelocity;
	#endif	
	ebd_filt_count++;
	if(ebd_filt_count>=4) /*10ms 5->7->4*/
	{
		ebd_filt_count=0;
	}
/*	  
	if(ebd_filt_count==1)
	{
		tempW2=tempW0-suma;
		suma=tempW0;

		#if	__HDC
		lsabss16HDCveloDiff=lsabss16HDCvelocity-sumHDCa;
		sumHDCa=lsabss16HDCvelocity;
		#endif

	}
	else if(ebd_filt_count==2)
	{
		tempW2=tempW0-sumb;
		sumb=tempW0;

		#if	__HDC
		lsabss16HDCveloDiff=lsabss16HDCvelocity-sumHDCb;
		sumHDCb=lsabss16HDCvelocity;
		#endif

	}
	else if(ebd_filt_count==3)
	{
		tempW2=tempW0-sumc;
		sumc=tempW0;

		#if	__HDC
		lsabss16HDCveloDiff=lsabss16HDCvelocity-sumHDCc;
		sumHDCc=lsabss16HDCvelocity;
		#endif

	}
	else if(ebd_filt_count==4)
	{
		tempW2=tempW0-sumd;
		sumd=tempW0;

		#if	__HDC
		lsabss16HDCveloDiff=lsabss16HDCvelocity-sumHDCd;
		sumHDCd=lsabss16HDCvelocity;
		#endif

	}
	else
	{
		tempW2=tempW0-sume;
		sume=tempW0;

		#if	__HDC
		lsabss16HDCveloDiff=lsabss16HDCvelocity-sumHDCe;
		sumHDCe=lsabss16HDCvelocity;
		#endif

		ebd_filt_count=0;

	}
*/
/*
	tempW1=5;
	tempW0=4;
	tempW3=0;
	if(tempW0 !=0)
	{
		s16muls16divs16();
	}
	else
	{
		;
	}
	ebd_filt_grv=(ebd_filt_grv+tempW3)>>1;
*/
	ebd_filt_grv=(int16_t)(((7*(int32_t)ebd_filt_grv)+((int32_t)tempW2*11))/17);
	#if	__HDC
	LDABS_vCalVehDecelForHDC();
	#endif




/*
	tempW2=ebd_filt_grv-ebd_refilt_grv;
	tempW1=14;
	tempW0=100;
	s16muls16divs16();
	ebd_refilt_grv=ebd_refilt_grv+tempW3;
*/
	ebd_refilt_grv=LCESP_s16Lpf1Int(ebd_filt_grv,ebd_refilt_grv,L_U8FILTER_GAIN_10MSLOOP_3_5HZ);	/*3.5Hz	7ms:17*/
	if(ebd_refilt_grv<-(AFZ_0G7))
	{
		if(ebd_high_mu_cnt<L_U8_TIME_10MSLOOP_700MS)
		{
			ebd_high_mu_cnt++;
		}
		else
		{
			;
		}
		if(ebd_high_mu_cnt>L_U8_TIME_10MSLOOP_70MS)
		{
			EBD_GRV_HIGH_MU_OK=1;
		}
		else
		{
			;
		}
	}
	else
	{
		ebd_high_mu_cnt=0;
		EBD_GRV_HIGH_MU_OK=0;
	}


}
/*******************************************************************/
void	LDABS_vCalVehDecelForDoubleB(void)
{
	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		aref_timer=0;
		aref_avg_counter=0;
		vref0=0;
		aref0=aref_temp[0]=aref_temp[1]=aref_temp[2]=aref_temp[3]=aref_temp[4]=aref_avg=0;
	}
	else
	{
		if(aref_timer>=7) /*10->14->7*/
		{
			aref_timer=0;
		}
		else
		{
			;
		}

		#if	(__L2H_IMPROVE_VREF_TOGGLE==ENABLE)
		if(VrefFilot_Toggle_flag==1)
		{
			aref_timer = 6;
		}
		else if(aref_timer==0)
		#else 
		if(aref_timer==0)
		#endif
		{
			vref0=vref_alt;
		}
		else if(aref_timer==6)
		{
			aref_temp[aref_avg_counter]=aref0;
			aref_avg_counter++;
			if(aref_avg_counter==5)
			{
				aref_avg_counter=0;
			}
/*			
			if(aref_avg_counter==1)
			{
					aref10=aref0;
			}
			else if(aref_avg_counter==2)
			{
				  aref20=aref0;
			}
			else if(aref_avg_counter==3)
			{
					aref30=aref0;
			}
			else if(aref_avg_counter==4)
			{
					aref40=aref0;
			}
			else
			{
				aref50=aref0;
				aref_avg_counter=0;
			}
*/
/*
			tempW2=(aref10+aref20+aref30+aref40+aref50);
			tempW1=1;
			tempW0=5;
			s16muls16divs16();
			aref_avg=tempW3;
*/
			aref_avg=(aref_temp[0]+aref_temp[1]+aref_temp[2]+aref_temp[3]+aref_temp[4])/5;
/*
			tempW5=vref;
			tempW2=tempW5-vref0;
			tempW1=50;
			tempW0=10;
			s16muls16divs16();
			aref0=tempW3;
*/
			aref0=(int16_t)(((int32_t)(vref-vref0)*35)/7);	/*7ms->5ms*/
		}
		else
		{
			;
		}

		aref_timer++;
	}
}
/*******************************************************************/
   #if(	(__4WD_VARIANT_CODE==ENABLE)	||(__4WD) )	 &&(__AX_SENSOR==1)
void	LDABS_vDetLongAccUse(void)
{
	int16_t afz_wheel;	
	afz_wheel=afz;
	
	if(USE_ALONG==1)
	{
		afz=afz_alt=along;
	}
	else
	{
		;
	}
}
   #endif
/*******************************************************************/
void	LDABS_vCalVehicleDecelAccel(void)
{
	int8_t EndAFZ;

	AFZ_OK_K=0;	  /* Clear AFZ_OK_K	*/
	EndAFZ=0;

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		afz=afz_alt=0;
		t_afz_old2=t_afz_old=t_afz=0;
		v_afz_old2=v_afz_old=v_afz=voptfz2;
		AFZ_SUS_CNT=0;
	}
	else
	{

		INIT_NEU=0;	  /* Store new afz and set INIT_NEU	*/
		AFZ_INCREDIBLE_K=0;
		AFZ_INCREDIBLE_A=0;
		AFZ_AQUA=0;
		AFZ_CAL_SUS_K_flag=0;				/*051108 AFZ Update	By KSW*/
	
		tempW5=voptfz1;						/* Select the voptfz and voptfz_diff for afz_calculation */
		tempW6=voptfz_diff1;
	
		t_afz++;							/* Increment t_afz */

		if(t_afz==0)
		{					   /* When t_afz > 464 sec,	clear afz */
			afz=afz_alt=0;
			v_afz=tempW5;
		}
		else
		{
	
			tempW2=tempW5-v_afz;
			
			tempW1=35; /*7ms->5ms*/
			tempW0=(int16_t)(t_afz);
			s16muls16divs16();
	
			/*
			if(tempW3 <	(int16_t)AFZ_NEG_LIMIT)
			{
				tempW3=AFZ_NEG_LIMIT;
			}
			else if(tempW3 > (int16_t)AFZ_POS_LIMIT)
			{
				tempW3=AFZ_POS_LIMIT;
			}
			else
			{
				;
			}
			
			afz_temp = tempW3;
			*/
			afz_temp=LCABS_s16LimitMinMax(tempW3,AFZ_NEG_LIMIT,AFZ_POS_LIMIT);
			LDABS_vDctVrefIncrement();
	
	
			/* Check Aquaplaning */
			if(V_INST_80==1)
			{
				if(ab_zaehl_fl > (uint8_t)L_U8_TIME_10MSLOOP_200MS)
				{
					if(ab_zaehl_fr > (uint8_t)L_U8_TIME_10MSLOOP_200MS)
					{
						if(STABIL_rl==1)
						{
							if(STABIL_rr==1)
							{
								AFZ_AQUA=1;
							}
							else
							{
								;
							}
						}
						else
						{
							;
						}
					}
					else
					{
						;
					}
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		
		
			if(ABS_fz==0)
			{
				INIT_AFZ_fl=INIT_AFZ_fr=0;
				AFZ_OK=0;
				AFZ_INCREDIBLE=0;
				afz_cal_delay=L_U8_TIME_10MSLOOP_100MS;
				#if	__CASCADING_WEAKEN
				AFZ_LOW_to_MEDIUM_flag=0;
				AFZ_LOW_to_HIGH_flag=0;
				AFZ_MEDIUM_to_HIGH_flag=0;
				#endif
		
				if(v_afz_time <= 0)
				{
                    tempW4=((spold_fl)|(spold_fr)|( spold_rl )|(spold_rr));
					v_afz_time=0;
                    if((tempW4 != 0) && (((v_afz-tempW5)>=VREF_2_KPH) || (t_afz>=(uint16_t)T_AFZ_CALC_MIN)))
					{
						v_afz_time=T_AFZ_FREEZE;
					}
					else
					{
						;
					}
				}
				else
				{
					v_afz_time--;
				}
		
				if(tempW5 <= (int16_t)VREF_MIN)
				{
					afz=afz_alt=0;
					t_afz=0;
					v_afz=tempW5;
					EndAFZ=1;
				}
				else
				{
					if((tempW2>=(int16_t)VREF_3_KPH)||(tempW2<=(int16_t)(-VREF_3_KPH)))
					{
						INIT_NEU=1;
						afz=afz_alt=afz_temp;
		
				   #if __4WD_VARIANT_CODE==ENABLE
					#if	__AX_SENSOR
					if(lsu8DrvMode != DM_2WD)
					{
						LDABS_vDetLongAccUse();
					}
					#endif
				   #else
					 #if __4WD && (__AX_SENSOR==1)
						LDABS_vDetLongAccUse();
					 #endif
				   #endif
		
		
					}
					else if(t_afz >= (uint16_t)(T_AFZ_CACL_HIGH*2))
					{
						INIT_NEU=1;
						afz=afz_alt=afz_temp;
		
				   #if __4WD_VARIANT_CODE==ENABLE
					#if	__AX_SENSOR
					if(lsu8DrvMode != DM_2WD)
					{
						LDABS_vDetLongAccUse();
					}
					#endif
				   #else
					 #if __4WD && (__AX_SENSOR==1)
						LDABS_vDetLongAccUse();
					 #endif
				   #endif
		
					}
					else
					{
						;
					}
				}
			}
			else
			{
				if(v_afz_time==0)
				{
					v_afz_time=T_AFZ_FREEZE;
				}
				else
				{
					v_afz_time=-1;
				}
		
				tempF0=0;
				tempF1=0;
				tempB0=0;							/* Count stab_wheels and the wheels	with zyklus_z >	0 */
				tempB1=0;
				tempB2=0;
				tempB3=0;							/*AFZ PPR 2009-171 */
				tempF2=0;							/*051108 AFZ Update	By KSW */
		
				tempB0=(int8_t)(STABIL_fl+STABIL_fr+STABIL_rl+STABIL_rr);
				tempB3=(int8_t)(ABS_fl+ABS_fr+ABS_rl+ABS_rr);		/*AFZ PPR 2009-171 */
		
				if(zyklus_z_fl != 0) tempB1++;
				if(zyklus_z_fr != 0) tempB1++;
				if(zyklus_z_rl != 0) tempB1++;
				if(zyklus_z_rr != 0) tempB1++;
		
		
		 /*	afz	update point selection */
				if(AFZ_OK==0)
				{
					if(AFZ_AQUA==0)
					{
						if(((t_afz >= (uint16_t)T_AFZ_CACL_HIGH)&&(tempB0 >= 1))||
						   ((t_afz >= (uint16_t)T_AFZ_CALC_MEDIUM)&&(tempB0	>= 1)&&(tempB1 >= 2))||
						   ((t_afz >= (uint16_t)T_AFZ_CALC_LOW)&&(tempB0 >=	2)&&(tempB1	>= 2))||
						   /* ((t_afz >= (uint16_t)T_AFZ_CALC_MIN)&&(tempB0==4))) */
						   ((t_afz >= (uint16_t)T_AFZ_CALC_MIN)&&(tempB0==4)&&(tempB3==4)))			/*AFZ PPR 2009-171 */
						{
							tempF0=1;
						}
						else
						{
							;
						}
					}
					else
					{
						if(AFZ_OK==0)
						{
							AFZ_OK_K=1;
						}
						else
						{
							;
						}
		
						AFZ_OK=1;
						afz=afz_temp;
		
					   	#if __4WD_VARIANT_CODE==ENABLE
							#if	__AX_SENSOR
						if(lsu8DrvMode != DM_2WD)
						{
							LDABS_vDetLongAccUse();
						}
							#endif
					   	#else
						 #if __4WD && (__AX_SENSOR==1)
							LDABS_vDetLongAccUse();
						 #endif
					   	#endif
		
					}
				}
				else
				{
					if(tempW2 <= (int16_t)(-VREF_3_KPH))
					{
						if(LOW_to_HIGH_suspect==1)
						{
							if(t_afz >=	(uint16_t)L_U8_TIME_10MSLOOP_100MS)
							{
								tempF0=1;
							}
							else
							{
								;
							}
						}
						else
						{
							if(((t_afz >= (uint16_t)T_AFZ_CACL_HIGH)&&(tempB0 >= 1))||
							   ((t_afz >= (uint16_t)T_AFZ_CALC_MEDIUM)&&(tempB0	>= 2)))
							{
							   tempF0=1;
							}
							else
							{
								;
							}
						}
					}
					else
					{
						;
					}
				}
		
		/*	  AFZ ���� 051111 By KSW temporary	   */
		
				if(tempF0==1)
				{
					if(vref_increase_cnt>=3)
					{
						tempB2=1;
						/*afz_flags=20;*/
					}
					/*
					else if	( (AFZ_OK==1) && (afz>-AFZ_0G5)	&& (t_afz<=L_U8_TIME_10MSLOOP_1000MS) &&	(vref_limit_dec_counter_frt>0) )  
					{
						tempB2=1;
						//afz_flags=21;
					}
					*/
					else if	( (AFZ_OK==1) && (vrselect1	>= tempW5) && (	arad_select_wheel >=ARAD_0G0  )	)
					{
						tempB2=1;
						/*afz_flags=22;*/
					}
					else
					{
						tempB2=0;
					}
		
					if(tempB2==1)
					{
						if(AFZ_INCREDIBLE==0)
						{
							AFZ_INCREDIBLE_K=1;
						}
						else
						{
							;
						}
		
						AFZ_INCREDIBLE=1;
					/*afz_flags=1;*/
					}
					else if(((tempW5-vref_trough)>=(int16_t)(VREF_2_KPH))&&(vref_trough>0)&&(tempW6>0))
					{  /*05	NZ6.0 Update */
						if(AFZ_INCREDIBLE==0)
						{
							AFZ_INCREDIBLE_K=1;
						}
						else
						{
							;
						}
		
						AFZ_INCREDIBLE=1;
						/*afz_flags=2;*/
					}
					else
					{
						if(AFZ_INCREDIBLE==1)
						{
							AFZ_INCREDIBLE_A=1;
						}
						else
						{
							;
						}
		
						AFZ_INCREDIBLE=0;
						/*afz_flags=3;*/
					}
		
					if((AFZ_INCREDIBLE==0) && (AFZ_INCREDIBLE_A==0))
					{
						if(t_afz >=	(uint16_t)(T_AFZ_CALC_MIN))
						{
							 tempF1=1;
							 AFZ_CAL_SUS_flag=0;
						}
						else
						{
							;
						}
					}
					else if(AFZ_INCREDIBLE_K==1)
					{
						tempF1=0;
					}
					else if(AFZ_INCREDIBLE_A==1)
					{
						if(t_afz >=	(uint16_t)(T_AFZ_CALC_MIN))
						{
							 tempF1=1;
							 AFZ_CAL_SUS_flag=0;
						}
						else
						{
							;
						}
					}
					else
					{
						;
					}
				}
				else if(AFZ_OK==1)
				{
					if(	(vref_increase_cnt>=10)	&& ((tempW5-vref_trough)>=VREF_1_5_KPH)	&& (vref_trough>0) )
					{
						if(AFZ_INCREDIBLE==0)
						{
							AFZ_INCREDIBLE_K=1;
						}
						else
						{
							;
						}
		
						AFZ_INCREDIBLE=1;
						/*afz_flags=4;*/
					}
					else if((AFZ_INCREDIBLE==1)&&(vref_increase_cnt==0))
					{
						AFZ_INCREDIBLE_A=1;
						AFZ_INCREDIBLE=0;
						tempF1=1;
		
						/*afz_flags=5;*/
					}
					else if(LOW_to_HIGH_suspect_K==1)
					{
						AFZ_INCREDIBLE_A=1;
						AFZ_INCREDIBLE=0;
						tempF1=1;
						/*afz_flags=7;*/
					}
					else
					{
						AFZ_INCREDIBLE=0;
						/*afz_flags=6;*/
					}
		
				}
				else
				{
					;
				}
		
				if((tempF1==1)
				&&((AFZ_INCREDIBLE_A==1)||((t_afz<=L_U8_TIME_10MSLOOP_300MS)&&(t_afz_old<=L_U8_TIME_10MSLOOP_500MS)&&(t_afz_old2<=L_U8_TIME_10MSLOOP_500MS))))
				{
					if(	(tempW5-tempW6-v_afz) <= (int16_t)(-VREF_3_KPH))
					{
		/*
						tempW2=tempW5-tempW6-v_afz;
						tempW0=(int16_t)(t_afz-1);
		*/
						/*opttime :	ksw*/
						tempW2 = (tempW5-tempW6-v_afz);
						tempW0 = (t_afz-1);
						
						tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);
					}
					else if( (tempW5-tempW6-v_afz_old) <= (int16_t)(-VREF_3_KPH))
					{
						tempW2=tempW5-tempW6-v_afz_old;
                        tempW0=((t_afz_old+t_afz)-1);
		
						tempW3=(int16_t)( ((int32_t)tempW2*35)/tempW0 );
					}
					else if( (tempW5-tempW6-v_afz_old2)	<= (int16_t)(-VREF_3_KPH))
					{
		/*
						tempW2=tempW5-tempW6-v_afz_old2;
						tempW0=(int16_t)(t_afz_old2+t_afz_old+t_afz-1);
		*/
						tempW2=tempW5-tempW6-v_afz_old2;
                        tempW0=(((t_afz_old2+t_afz_old)+t_afz)-1);
												
						tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);	
					}
					else
					{
						if(v_afz>=v_afz_old)
						{
							if(v_afz>=v_afz_old2)
							{
		/*
								tempW2=tempW5-tempW6-v_afz;
								tempW0=(int16_t)(t_afz-1);
		*/
								tempW2=tempW5-tempW6-v_afz;
								tempW0=(t_afz-1);							 
							
								tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);	
							}
							else
							{
		/*
								tempW2=tempW5-tempW6-v_afz_old2;
								tempW0=(int16_t)(t_afz_old2+t_afz_old+t_afz-1);
		*/
								tempW2=tempW5-tempW6-v_afz_old2;
                                tempW0=(((t_afz_old2+t_afz_old)+t_afz)-1);                               
							   
								tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);
							}
						}
						else if(v_afz_old>=v_afz_old2)
						{
		/*
							tempW2=tempW5-tempW6-v_afz_old;
							tempW0=(int16_t)(t_afz_old+t_afz-1);
		*/
							tempW2=tempW5-tempW6-v_afz_old;
                            tempW0=((t_afz_old+t_afz)-1);
														
							tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);
						}
						else
						{
		/*
							tempW2=tempW5-tempW6-v_afz_old2;
							tempW0=(int16_t)(t_afz_old2+t_afz_old+t_afz-1);
		*/
							/*opttime :	ksw*/								
							tempW2=(tempW5-tempW6-v_afz_old2);
                            tempW0=(((t_afz_old2+t_afz_old)+t_afz)-1);
														
							tempW3=(int16_t)(((int32_t)tempW2*35)/tempW0);
						}
					}
		/*
					tempW1=70; //7ms->5ms
					s16muls16divs16();
		*/					
		/*
					if(tempW3 <	(int16_t)AFZ_NEG_LIMIT)
					{
						tempW3=AFZ_NEG_LIMIT;
					}
					else if(tempW3 > (int16_t)AFZ_POS_LIMIT)
					{
						tempW3=AFZ_POS_LIMIT;
					}
					else
					{
						;
					}
		
					afz_temp = tempW3;
		*/
					afz_temp=LCABS_s16LimitMinMax( tempW3, AFZ_NEG_LIMIT, AFZ_POS_LIMIT);
				}
				else
				{
					;
				}
		
				if(tempF1==1)
				{
					INIT_NEU=1;
					if((AFZ_OK==0)&&(AFZ_AQUA==0))
					{
						if(afz_temp	< -(int16_t)U8_HIGH_MU)
						{
		/*							 tempB0=(ab_z_pr_fl	+ ab_z_pr_fr)>>1; */
		
							if((ABS_fl==1) && (ABS_fr==1))
							{
								if(vref>=VREF_120_KPH)
								{
									if(ab_z_pr_fl >= U8_Ref_Dump1_for_AFZ_Lim_H_V_F)
									{
										if(peak_acc_afz_fl < (int8_t)S8_Ref_Pk_Accel1_AFZ_Lim_H_V_F)
										{
											INIT_AFZ_fl=1;
										}
										else
										{
											;
										}
									}
									else
									{
										if(peak_acc_afz_fl < (int8_t)S8_Ref_Pk_Accel2_AFZ_Lim_H_V_F)
										{
											if(ab_z_pr_fl >= U8_Ref_Dump2_for_AFZ_Lim_H_V_F)
											{
												if(gma_counter_fl <	L_U8_TIME_10MSLOOP_100MS)
												{
													INIT_AFZ_fl=1;
												}
												else
												{
													;
												}
											}
											else
											{
												;
											}
										}
										else
										{
											;
										}
									}
									if(ab_z_pr_fr >= U8_Ref_Dump1_for_AFZ_Lim_H_V_F)
									{
										if(peak_acc_afz_fr < (int8_t)S8_Ref_Pk_Accel1_AFZ_Lim_H_V_F)
										{
											INIT_AFZ_fr=1;
										}
										else
										{
											;
										}
									}
									else
									{
										if(peak_acc_afz_fr < (int8_t)S8_Ref_Pk_Accel2_AFZ_Lim_H_V_F)
										{
											if(ab_z_pr_fr >= U8_Ref_Dump2_for_AFZ_Lim_H_V_F)
											{
												if(gma_counter_fr <	L_U8_TIME_10MSLOOP_100MS)
												{
													INIT_AFZ_fr=1;
												}
												else
												{
													;
												}
											}
											else
											{
												;
											}
										}
										else
										{
											;
										}
									}
								}
								else
								{
									if(ab_z_pr_fl >= U8_Ref_Dump1_for_AFZ_Lim_F)
									{
										if(peak_acc_afz_fl < (int8_t)S8_Ref_Pk_Accel1_AFZ_Lim_F)
										{
											INIT_AFZ_fl=1;
										}
										else
										{
											;
										}
									}
									else
									{  /* 00.6.15 7g->5g, tempB0->wheel	*/
										if(peak_acc_afz_fl < (int8_t)S8_Ref_Pk_Accel2_AFZ_Lim_F)
										{
											if(ab_z_pr_fl >= U8_Ref_Dump2_for_AFZ_Lim_F)
											{
												if(gma_counter_fl <	L_U8_TIME_10MSLOOP_100MS)
												{
													INIT_AFZ_fl=1;
												}
												else
												{
													;
												}
											}
											else
											{
												;
											}
										}
										else
										{
											;
										}
									}
		
									if(ab_z_pr_fr >= U8_Ref_Dump1_for_AFZ_Lim_F)
									{
										if(peak_acc_afz_fr < (int8_t)S8_Ref_Pk_Accel1_AFZ_Lim_F)
										{
											INIT_AFZ_fr=1;
										}
										else
										{
											;
										}
									}
									else
									{  /* 00.6.15 7g->5g, tempB->wheel */
										if(peak_acc_afz_fr < (int8_t)S8_Ref_Pk_Accel2_AFZ_Lim_F)
										{
											if(ab_z_pr_fr >= U8_Ref_Dump2_for_AFZ_Lim_F)
											{
												if(gma_counter_fr <	L_U8_TIME_10MSLOOP_100MS)
												{
													INIT_AFZ_fr=1;
												}
												else
												{
													;
												}
											}
											else
											{
												;
											}
										}
										else
										{
											;
										}
									}
								}
								if((INIT_AFZ_fl==1)	&& (INIT_AFZ_fr==1))
								{
									afz_temp=-(int16_t)U8_HIGH_MU;
								}
								else
								{
									;
								}
							}
							else
							{
								;
							}
						}
						else
						{
							;
						}
		
						if((ab_z_pr_fl >= L_U8_TIME_10MSLOOP_100MS)&&(ab_z_pr_fr >= L_U8_TIME_10MSLOOP_100MS))
						{
							if((afz_temp<=-(int16_t)U8_LOW_MU) || (afz_temp	>=ARAD_0G0)	)
							{
								afz_temp=-(int16_t)U8_LOW_MU;
								/*afz_flags=9;*/
							}
							else
							{
								;
							}
		
								/*afz_flags=10;*/
						}
						else
						{
							;
						}
		
					}
					else
					{
						;
					}
		
					if(AFZ_OK==0)
					{
						AFZ_OK_K=1;
					}
					else
					{
						;
					}
		
					AFZ_OK=1;
		
					if(afz > -(int16_t)U8_HIGH_MU)
					{
						if(afz_temp	>= -(int16_t)U8_HIGH_MU)
						{ /* 1st afz Low_mu?, yes. */
		
						  #if	__CASCADING_WEAKEN
							if((AFZ_OK==1) && (AFZ_OK_K==0)	&& (afz	>= -(int16_t)U8_LOW_MU))
							{
								if(afz_temp	< -(int16_t)U8_LOW_MU)
								{
									AFZ_LOW_to_MEDIUM_flag=1;
								}
								else
								{
									;
								}
							}
							else
							{
								AFZ_LOW_to_MEDIUM_flag=0;
							}
						  #endif
		
							if(afz_temp<AFZ_0G0)
							{
								afz=afz_temp;				   /* High_mu?,	wait 100ms.	*/
								afz_alt=afz;
								afz_cal_delay=0;
							}
							else
							{
								afz=afz_alt;
								afz_alt=afz_temp;
								afz_cal_delay=0;
							}
						   #if __4WD_VARIANT_CODE==ENABLE
							if(lsu8DrvMode != DM_2WD)
							{
								LDABS_vDetLongAccUse();
							}
						   #else
							 #if __4WD
								LDABS_vDetLongAccUse();
							 #endif
						   #endif
						}
						else
						{							 /*	00 winter,XD_39	*/
							if(vref	< (int16_t)VREF_50_KPH)
							{
								if((ABS_fl==1) && (ABS_fr==0))
								{
									if(rel_lam_fl >	(int8_t)(-LAM_30P))
									{
										afz_cal_delay=0;
									}
									else
									{
										;
									}
								}
								else
								{
									;
								}
		
								if((ABS_fr==1) && (ABS_fl==0))
								{
									if(rel_lam_fr >	(int8_t)(-LAM_30P))
									{
										afz_cal_delay=0;
									}
									else
									{
										;
									}
								}
								else
								{
									;
								}
							}
							else
							{
								;
							}
		
							if(afz_cal_delay ==	0)
							{
		
							  #if	__CASCADING_WEAKEN
								if((AFZ_OK==1) && (AFZ_OK_K==0)	&& (afz	>= -(int16_t)U8_LOW_MU))
								{
									AFZ_LOW_to_HIGH_flag=1;
								}
								else if((AFZ_OK==1)	&& (AFZ_OK_K==0) &&	(afz < -(int16_t)U8_LOW_MU))
								{
									AFZ_LOW_to_MEDIUM_flag = 0;
									AFZ_MEDIUM_to_HIGH_flag	= 1;
								}
								else
								{
									;
								}
							  #endif
		
								afz=afz_temp;
		
							   #if __4WD_VARIANT_CODE==ENABLE
									#if	__AX_SENSOR
								if(lsu8DrvMode != DM_2WD)
								{
									LDABS_vDetLongAccUse();
								}
									#endif
							   #else
								 #if __4WD && (__AX_SENSOR==1)
									LDABS_vDetLongAccUse();
								 #endif
							   #endif
		
								afz_alt=afz;
							}
							else
							{
								INIT_NEU=0;
								AFZ_OK=0;
								afz_cal_delay--;
							}
						}
					} /* afz > -(int16_t)U8_HIGH_MU	*/
					else
					{
						if(afz_temp >= AFZ_0G0)
						{
							if(ldabsu1AFZLowMuWhlBehavior==0)	/* PPR 2007-139	*/
							{
								afz=afz_alt;
							}
							else
							{
								afz=-(int16_t)U8_LOW_MU;							
								ldabsu1AFZLowMuWhlBehavior=0;
							}
							afz_alt=afz_temp;
							afz_cal_delay=0;
						}
						else
						{
						
							afz=afz_temp;
							afz_alt=afz;
							afz_cal_delay=0;
							
							ldabsu1AFZLowMuWhlBehavior=0;	/* PPR 2007-139	*/
						}
					   	#if __4WD_VARIANT_CODE==ENABLE
							#if	__AX_SENSOR
						if(lsu8DrvMode != DM_2WD)
						{
							LDABS_vDetLongAccUse();
						}
							#endif
					   	#else
						 	#if __4WD && (__AX_SENSOR==1)
							LDABS_vDetLongAccUse();
						 	#endif
					   	#endif
		
					  	#if	__CASCADING_WEAKEN
						AFZ_LOW_to_HIGH_flag = 0;
						AFZ_MEDIUM_to_HIGH_flag	= 0;
					  	#endif
					}
				}
				else
				{
					;
				}
						
			  	/* PPR 2007-139 */
				if(AFZ_OK==0)
				{
					if((FL.peak_slip < (int8_t)(-LAM_30P))&&(FR.peak_slip <	(int8_t)(-LAM_30P))&&
						(RL.peak_slip <	(int8_t)(-LAM_10P))&&(RR.peak_slip < (int8_t)(-LAM_10P)))
					{
						if((((PEAK_ACC_PASS_fl==1)&&(FL.peak_acc <=	ARAD_5G0))&&(FR.peak_acc <=	ARAD_5G0))||
							(((PEAK_ACC_PASS_fr==1)&&(FR.peak_acc <= ARAD_5G0))&&(FL.peak_acc <= ARAD_5G0)))
						{
							ldabsu1AFZLowMuWhlBehavior=1;  /* wheel	recovery arad <= 5.0g --> Low-mu wheel behavior	Determination */
						}
						else if((FL.peak_acc > ARAD_5G0)||(FR.peak_acc > ARAD_5G0))
						{
							ldabsu1AFZLowMuWhlBehavior=0;
						}
						else
						{
							;
						}
					}
					else
					{
						;
					}
				}
				else
				{
					ldabsu1AFZLowMuWhlBehavior=0;
				}								
		
			}	/* ABS_fz==0 */
		
			if(EndAFZ==0)
			{							 /*	Check v_afz_timer and initialize t_afz and v_afz */
			#if	__4WD_VARIANT_CODE==ENABLE
				if(lsu8DrvMode != DM_2WD)
				{
					if((INIT_NEU==1)||(v_afz_time==T_AFZ_FREEZE))
					{
						v_afz_old2=v_afz_old;
						v_afz_old=v_afz;
						v_afz=tempW5;
		
						t_afz_old2=t_afz_old;
						t_afz_old=t_afz;
						t_afz=0;
					}
					else
					{
						;
					}
				}
				else
				{
					if(((v_afz_time	<= 0)&&(INIT_NEU==1))||(v_afz_time==T_AFZ_FREEZE))
					{
						v_afz_old2=v_afz_old;
						v_afz_old=v_afz;
						v_afz=tempW5;
		
						t_afz_old2=t_afz_old;
						t_afz_old=t_afz;
						t_afz=0;
					}
					else
					{
						;
					}
				}
			#else /*__4WD_VARIANT_CODE==ENABLE*/
		#if	__4WD
				if((INIT_NEU==1)||(v_afz_time==T_AFZ_FREEZE))
		#else
				if(((v_afz_time	<= 0)&&(INIT_NEU==1))||(v_afz_time==T_AFZ_FREEZE))
		#endif
		
				{
					v_afz_old2=v_afz_old;
					v_afz_old=v_afz;
					v_afz=tempW5;
		
					t_afz_old2=t_afz_old;
					t_afz_old=t_afz;
					t_afz=0;
				}
				else
				{
					;
				}
			#endif/*__4WD_VARIANT_CODE==ENABLE*/
			}
			else
			{
				;
			}
	   	}	/* t_afz==0	*/
  	}	   /*  speed_calc_timer	*/

/*	  End_AFZ:;	 */
}
/*******************************************************************/

void	LDABS_vDctVrefIncrement(void)
{
	if(ABS_fz==0)
	{
		vref_increase_cnt=0;
		vref_trough=0;
		vref_decrease_permission=0;
	}
	else
	{
		if(tempW6>0)
		{
			vref_decrease_permission=0;
			if(vref_increase_cnt<=L_U8_CNT_200)
			{
				vref_increase_cnt++;
			}
			else
			{
				;
			}
			if(vref_increase_cnt==1)
			{
				vref_trough=tempW5-tempW6;
			}
			else
			{
				;
			}
		}
		else if(tempW6<0)
		{
			if((vref_increase_cnt>0)&&(tempW6>=-2)&&(vref_decrease_permission==0))
			{
				vref_decrease_permission=1;
			}
			else
			{
				vref_increase_cnt=0;
				vref_decrease_permission=0;
			}
		}
		else if(vref_increase_cnt>0)
		{
			if(vref_decrease_permission==0)
			{
				if(vref_increase_cnt<=L_U8_CNT_200)
				{
					vref_increase_cnt++;
				}
				else
				{
					;
				}
			}
			else
			{
				vref_increase_cnt=0;
				vref_decrease_permission=0;
			}
		}
		else
		{
			vref_decrease_permission=0;
		}
	}
}
#if	__HDC
/*******************************************************************/
void	LDABS_vCalVehDecelForHDC(void)
{
	uint8_t	lsabsu8FilterMulti;
	uint8_t	lsabsu8FilterCoeff;
	int16_t	lsabss16VehicleAccelInput;

	lsabsu8FilterMulti=10;

	if(speed_calc_timer <= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		lsabsu8WheelSelectChangeCNT=0;
		WHEEL_CHANGE_HOLD_FLG=0;
		lsabss16HDCWheelAccel=0;
		lsabss16VrselectOLD=FZ1.vrselect_resol_change;
	}
	else
	{
		;
	}
  	/*-------	vref5 to vref change -----------------*/
  	LDABS_vCalVehicleAccelInputChange();


	lsabss16VehicleAccelInput=lsabss16HDCveloDiffFinal;

  	/*-------- Wheel Selection Change	Dectection ---*/
  	LDABS_vCalWheelSelectChange();

  	/*-------- Filter	Coefficient	Selection---------*/

	if(FZ1.voptfz_resol_change <= VREF_1_5_KPH_RESOL_CHANGE	)
	{
		lsabsu8FilterCoeff=L_U8FILTER_GAIN_10MSLOOP_1HZ;
		lsabsu8WheelSelectChangeCNT=0;
	}
	else
	{
		if(WHEEL_CHANGE_HOLD_FLG==1)
		{
			if(lsabsu8WheelSelectChangeCNT >= L_U8_TIME_10MSLOOP_1000MS)
			{
				lsabsu8WheelSelectChangeCNT=0;
				lsabsu8FilterCoeff=L_U8FILTER_GAIN_10MSLOOP_2HZ;
				WHEEL_CHANGE_HOLD_FLG=0;
			}
			else
			{
				if(WHEEL_SELECT_CHANGE_FLG==1)
				{
				  lsabsu8WheelSelectChangeCNT=0;
				}
				else
				{
					lsabsu8WheelSelectChangeCNT= lsabsu8WheelSelectChangeCNT+1;
				}
				lsabsu8FilterCoeff=L_U8FILTER_GAIN_10MSLOOP_1HZ;
			}
		}
		else
		{
			if(WHEEL_SELECT_CHANGE_FLG==1)
			{
			  	WHEEL_CHANGE_HOLD_FLG=1;
			}
			else
			{
				WHEEL_CHANGE_HOLD_FLG=0;
			}
			lsabsu8FilterCoeff=L_U8FILTER_GAIN_10MSLOOP_2HZ;
	  	}
	}

	/*-------- Filtering Process -------------------*/

	lsabss16HDCWheelAccel=LCESP_s16Lpf1Int(	(lsabss16VehicleAccelInput*lsabsu8FilterMulti*11)/(10) , lsabss16HDCWheelAccel , lsabsu8FilterCoeff);

	/*-------- Longitudinal	Accelration	limit-------*/
	if(lsabss16HDCWheelAccel>= HDC_ACCEL_2G	)
	{
		lsabss16HDCWheelAccel= HDC_ACCEL_2G;
	}
	else if(lsabss16HDCWheelAccel< (-HDC_ACCEL_2G) )
	{
		lsabss16HDCWheelAccel= -HDC_ACCEL_2G;
	}
	else
	{
		;
	}

}

/*******************************************************************/

void  LDABS_vCalWheelSelectChange(void)
{

	if(	( (FL.WHL_SELECT_FLG !=	FL_SELECT_WH_OLD) || (FR.WHL_SELECT_FLG	!= FR_SELECT_WH_OLD) ||
			(RL.WHL_SELECT_FLG != RL_SELECT_WH_OLD)	|| (RR.WHL_SELECT_FLG != RR_SELECT_WH_OLD) ) &&
		  (McrAbs(lsabss16VrselectOLD-FZ1.vrselect_resol_change)> VREF_1_KPH_RESOL_CHANGE) )
	{
		WHEEL_SELECT_CHANGE_FLG=1;
	}
	else
	{
		WHEEL_SELECT_CHANGE_FLG=0;
	}


	lsabss16VrselectOLD=FZ1.vrselect_resol_change;
	FL_SELECT_WH_OLD   =FL.WHL_SELECT_FLG;
	FR_SELECT_WH_OLD   =FR.WHL_SELECT_FLG;
	RL_SELECT_WH_OLD   =RL.WHL_SELECT_FLG;
	RR_SELECT_WH_OLD   =RR.WHL_SELECT_FLG;

}

/*******************************************************************/

void  LDABS_vCalVehicleAccelInputChange(void)
{
	if(lsabsu1HDCdecelInputChange == 1)
	{
		if(vref5_resol_change == vref_resol_change)
		{
			lsabss16HDCveloDiffFinal= tempW2;
			lsabsu8HDCdecelInputChangeCNT=0;

		}
		else
		{
			if(lsabsu8HDCdecelInputChangeCNT >=	6)
			{
				lsabss16HDCveloDiffFinal=lsabss16HDCveloDiff;
				lsabsu8HDCdecelInputChangeCNT=0;
				lsabsu1HDCdecelInputChange=0;
			}
			else
			{
			  	lsabss16HDCveloDiffFinal=	tempW2;
				lsabsu8HDCdecelInputChangeCNT=lsabsu8HDCdecelInputChangeCNT+1;
			}

		}

	}
	else
	{
		if(YAW_CORRECTION_MODE_CHANGE==1)
		{
		  lsabsu1HDCdecelInputChange=1;
		  lsabss16HDCveloDiffFinal=tempW2;
		}
		else
		{
			lsabsu1HDCdecelInputChange=0;
			lsabss16HDCveloDiffFinal=lsabss16HDCveloDiff;
		}

		lsabsu8HDCdecelInputChangeCNT=0;
	}

}
#endif
/*******************************************************************/
void	LDABS_vCalVehDecelbyVref5(void)
{
	if(speed_calc_timer	< (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
	{
		lsabss16DecelRefiltByVref5=0;
		/*lsabs16wheelaccel = 0;*/
	}
	else
	{
		
		  if((FL.WhlErrFlg==1)||(FR.WhlErrFlg==1)||(RL.WhlErrFlg==1)||(RR.WhlErrFlg==1))
		{
			lsabsu8Decelvalidity=1;		
		}
		else
		{
			lsabsu8Decelvalidity=0;
		}	
	
		ACC_CALC =	(struct	ACC_FILTER *)&V_ACC;
		#if	__VDC
		LDABS_vCalcEachFilterSignalAccel( vref5_resol_change, s16VELFilterFreq,	s16VELFilterZeta , s16VELAccMulti ,	s16VELdiffcoefCov )	;
		#else
		LDABS_vCalcEachFilterSignalAccel( vref_resol_change, s16VELFilterFreq, s16VELFilterZeta	, s16VELAccMulti , s16VELdiffcoefCov ) ;
		#endif
	
        lsabss16DecelRefiltByVref5 = V_ACC.lsesps16SignalAccel; 
        lsabss16DecelRefiltByVref5 = LCABS_s16LimitMinMax(lsabss16DecelRefiltByVref5,-1500,1500);
		

		#if __SPIN_DET_IMPROVE == ENABLE
		
		ACC_CALC = (struct ACC_FILTER *)&ACC_ACC;
		LDABS_vCalcEachFilterSignalAccel( lsabss16RawAxSignalTemp ,30, s16VELFilterZeta , s16VELAccMulti , 10 ) ;
		ls16accelacc = ACC_ACC.lsesps16SignalAccel;
		
		ACC_CALC = (struct ACC_FILTER *)&ACC_ACC_2;
		LDABS_vCalcEachFilterSignalAccel( lsabss16RawAxSignalTemp ,60, s16VELFilterZeta , s16VELAccMulti , 10 ) ;
		ls16accel = ACC_ACC_2.lsesps16SignalFilter;
        
		ACC_CALC = (struct ACC_FILTER *)&FL_ACC;
		LDABS_vCalcEachFilterSignalAccel( FL.vrad_resol_change, 40, s16VELFilterZeta , s16VELAccMulti , s16VELdiffcoefCov ) ;//vrselect_resol_change
		ls16wheelaccelFL = FL_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&FR_ACC;
		LDABS_vCalcEachFilterSignalAccel( FR.vrad_resol_change, 40, s16VELFilterZeta , s16VELAccMulti , s16VELdiffcoefCov ) ;//vrselect_resol_change
		ls16wheelaccelFR = FR_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&RL_ACC;
		LDABS_vCalcEachFilterSignalAccel( RL.vrad_resol_change, 40, s16VELFilterZeta , s16VELAccMulti , s16VELdiffcoefCov ) ;//vrselect_resol_change
		ls16wheelaccelRL = RL_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&RR_ACC;
		LDABS_vCalcEachFilterSignalAccel( RR.vrad_resol_change, 40, s16VELFilterZeta , s16VELAccMulti , s16VELdiffcoefCov ) ;//vrselect_resol_change
		ls16wheelaccelRR = RR_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&WHEEL_G_ACC;
		LDABS_vCalcEachFilterSignalAccel( ls16wheelaccel, 30, s16VELFilterZeta , s16VELAccMulti , 10 ) ;
		ls16wheelgaccel = WHEEL_G_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&FL_ACC_ACC;
		LDABS_vCalcEachFilterSignalAccel( ls16wheelaccelFL,  30, s16VELFilterZeta ,  s16VELAccMulti , 10 ) ;
		ls16wheelgaccelFL = FL_ACC_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&FR_ACC_ACC;
		LDABS_vCalcEachFilterSignalAccel( ls16wheelaccelFR,  30, s16VELFilterZeta ,  s16VELAccMulti , 10 ) ;
		ls16wheelgaccelFR = FR_ACC_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&RL_ACC_ACC;
		LDABS_vCalcEachFilterSignalAccel( ls16wheelaccelRL,  30, s16VELFilterZeta ,  s16VELAccMulti , 10 ) ;
		ls16wheelgaccelRL = RL_ACC_ACC.lsesps16SignalAccel;

		ACC_CALC = (struct ACC_FILTER *)&RR_ACC_ACC;
		LDABS_vCalcEachFilterSignalAccel( ls16wheelaccelRR,  30, s16VELFilterZeta ,  s16VELAccMulti , 10 ) ;
		ls16wheelgaccelRR = RR_ACC_ACC.lsesps16SignalAccel;
        #endif        		
	}
}

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallEstVehDecel
	#include "Mdyn_autosar.h"
#endif
