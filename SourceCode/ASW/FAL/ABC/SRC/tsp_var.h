

extern U8_BIT_STRUCT_t TSPF0;
extern U8_BIT_STRUCT_t TSPF1;
extern U8_BIT_STRUCT_t TSPF2;
extern U8_BIT_STRUCT_t TSPF3;

extern int16_t 	tsp_tempW0, tsp_tempW1, tsp_tempW2, tsp_tempW3, tsp_tempW4; 
extern int16_t 	tsp_tempW5, tsp_tempW6, tsp_tempW7, tsp_tempW8, tsp_tempW9;
                
extern int16_t 	tsp_yaw_threshold, tsp_yawc_thr;   
extern int16_t 	tsp_dyaw_add_thr, tsp_dyaw_add_LS_thr, tsp_dyaw_add_HS_thr, tsp_dyaw_add_CS_thr;
extern int16_t 	tsp_yawc_add_thr, tsp_yawc_add_LS_thr, tsp_yawc_add_HS_thr, tsp_yawc_add_CS_thr;
                
extern int16_t 	tsp_delta_yaw_first;                    
extern int16_t 	tsp_delta_yaw_lf_old, tsp_delta_yaw_lf; 
extern int16_t 	tsp_delta_yaw_dot;
                
extern int16_t 	tsp_yawc_first;
extern int16_t 	tsp_yawc_first_old, tsp_yawc_first_old2, tsp_yawc_first_old3; 
extern int16_t 	tsp_yawc_dot;

extern int16_t	tsp_initial_speed;                
extern uint8_t 	ldtspu8TrlrHtchSwAtv;
extern uint8_t 	ldtspu8DetectOscilCycle;
extern int16_t 	lds16TspFreqMaxTime;

extern int16_t 	lau16tspFirstDiffEnterTime;
                
extern uint8_t  tsp_detect_oscil_count;
                
extern uint16_t	tsp_dyaw_count, tsp_dyaw_count_old, tsp_dyaw_count_old2;
extern uint16_t	tsp_plus_dyaw_peak_count, tsp_minus_dyaw_peak_count;
extern uint16_t	tsp_freq_rate_2nd_3rd, tsp_freq_rate_3rd_4th;

extern uint16_t tsp_max_amplitude_1st, tsp_max_amplitude_3rd;
extern uint16_t tsp_max_amplitude_2nd, tsp_max_amplitude_4th;
extern uint16_t tsp_max_amplitude, tsp_max_amplitude_old;
extern uint16_t tsp_yaw_threshold_max_amp, tsp_yaw_threshold_max_amp_old;
extern int16_t 	tsp_delta_max_amplitude_old;
extern int16_t 	tsp_amplitude_rate_1st_3rd;
extern int16_t 	tsp_amplitude_rate_2nd_4th;
extern int16_t 	tsp_amp_damping_ratio_1st_3rd;
extern int16_t 	tsp_amp_damping_ratio_2nd_4th;
                
extern uint16_t tsp_yawc_count;

extern int16_t	tsp_osci_mitig_count;
extern int16_t	tsp_inhibit_work_count;
extern uint16_t	tsp_brake_work_count;

extern int16_t 	lcs16TspTargetDecel;
extern int16_t 	lcs16TspTargetDecelAmp;
extern int16_t 	lcs16TspTargetDecelSpeedGain;
extern int16_t 	lcs16TspTargetDecelInit;
extern int16_t 	lcs16TspTargetDecelReal;
extern int16_t	lcs16tspTargetDecelMonitor;
extern int16_t	lcs16TspDecelSpeed;
extern int16_t	lcs16TspDeltaDecelSpeed;  

extern int16_t	tsp_fade_ctrl_cnt;
extern int16_t	tsp_decel_ctrl_cnt;

extern int16_t	lcs16TspInitialDecelCurrent;
extern int16_t	lcs16TspDiffOnlyCurrent;
extern int16_t	Delta_moment_tsp;

extern int16_t	lcs16TspDiffCtrlAddCurP;
extern int16_t	lcs16TspDiffCtrlAddCurS;
extern int16_t	lcs16TspMFCCurP;
extern int16_t	lcs16TspMFCCurS;

extern uint8_t 	lcu8TspTorqDownRate;
extern int16_t 	lcu8TspTorqlimit;
extern int16_t  tsp_torq;

#define U1_TSP_CONTROL_MODE_INHIBIT  							TSPF0.bit0
#define ldtspu1TrlrHtchSwAtvValid    							TSPF0.bit1
#define TSP_PLUS_DYAW_THR													TSPF0.bit2
#define TSP_MINUS_DYAW_THR 												TSPF0.bit3
#define TSP_PLUS_YAWC_THR   											TSPF0.bit4
#define TSP_MINUS_YAWC_THR  											TSPF0.bit5
#define TSP_ON  																	TSPF0.bit6
#define TSP_ENG_CTRL_ON			 											TSPF0.bit7
                                      						
#define TSP_BRAKE_CTRL_ON													TSPF1.bit0
#define U1_TSP_BRAKE_CTRL_ON_READY								TSPF1.bit1
#define U1_TSP_BRAKE_CTRL_ACT											TSPF1.bit2
#define TSP_DIFF_ONLY_CTRL_ACT			  						TSPF1.bit3
#define U1_TSP_ADD_TC_CURRENT_P 									TSPF1.bit4
#define U1_TSP_ADD_TC_CURRENT_S   								TSPF1.bit5
#define U1_TSP_DEC_DIFF_CTRL_ACT     							TSPF1.bit6
#define U1_TSP_FADE_CTRL_ON     									TSPF1.bit7
                                      						
#define U1_TSP_FIRST_DIFF_CTRL_DETECT							TSPF2.bit0
#define U1_TSP_FADE_CTRL_MONITOR									TSPF2.bit1
#define U1_TSP_DIFF_To_DEC_ON											TSPF2.bit2
#define U1_TSP_FRQ_FAIL														TSPF2.bit3
#define U1_TSP_1st_3rd_AMP_REDUCE	 								TSPF2.bit4
#define U1_TSP_2nd_4th_AMP_REDUCE									TSPF2.bit5
#define U1_TSP_TRAILER_CONNECT_ON 								TSPF2.bit6
#define U1_TSP_FRQ_RATE_FAIL 											TSPF2.bit7

#define TSP_PLUS_DYAW_PEAK_DETECT									TSPF3.bit0
#define TSP_MINUS_DYAW_PEAK_DETECT								TSPF3.bit1
#define TSP_DYAW_CHANGE														TSPF3.bit2
#define U1_TSP_UNUSED_00													TSPF3.bit3
#define U1_TSP_UNUSED_01	 												TSPF3.bit4
#define U1_TSP_UNUSED_02													TSPF3.bit5
#define U1_TSP_UNUSED_03 													TSPF3.bit6
#define U1_TSP_UNUSED_04													TSPF3.bit7
    
    
    
    
    
    

