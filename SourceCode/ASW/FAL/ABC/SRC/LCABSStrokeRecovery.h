#ifndef __LCABSSTROKERECOVERY_H__
#define __LCABSSTROKERECOVERY_H__

/*includes********************************************************************/
#include "LVarHead.h"



/*Global MACRO CONSTANT Definition********************************************/
#define __ABS_STK_RCVR_CTRL_MON				DISABLE

#define S8_SR_WHLCTRLREQ_HOLD				1
#define S8_SR_WHLCTRLREQ_PRERISE			2
#define S8_SR_WHLCTRLREQ_POSTRISE			3
#define S8_SR_WHLCTRLREQ_NONE				0

#define U8_SR_REF_FL_SIDE					0
#define U8_SR_REF_FR_SIDE					1

#define S16_SR_REF_SLIP_THD_BFAFZOK			(-LAM_4P)
#define S16_SR_REF_SLIP_THD_LOW_MU			(-LAM_1P)
#define S16_SR_REF_SLIP_THD_MED_MU			(-LAM_3P)
#define S16_SR_REF_SLIP_THD_MED_H_MU		(-LAM_3P)
#define S16_SR_REF_SLIP_THD_HIGH_MU			(-LAM_4P)

#define S16_SR_REF_ARAD_THD_BFAFZOK			(-ARAD_2G0)
#define S16_SR_REF_ARAD_THD_LOW_MU			(-ARAD_1G0)
#define S16_SR_REF_ARAD_THD_MED_MU			(-ARAD_1G5)
#define S16_SR_REF_ARAD_THD_M_HIGH_MU		(-ARAD_1G5)
#define S16_SR_REF_ARAD_THD_HIGH_MU			(-ARAD_2G0)

#define S16_SR_REF_PULSEUP_THD_BFAFZOK		(3)
#define S16_SR_REF_PULSEUP_THD_LOW_MU		(2)
#define S16_SR_REF_PULSEUP_THD_MED_MU		(2)
#define S16_SR_REF_PULSEUP_THD_MED_H_MU		(2)
#define S16_SR_REF_PULSEUP_THD_HIGH_MU		(3)

#define S16_SR_REF_RCVR_P_THD_BFAFZOK		(40)
#define S16_SR_REF_RCVR_P_THD_LOW_MU		(40)
#define S16_SR_REF_RCVR_P_THD_MED_MU		(50)
#define S16_SR_REF_RCVR_P_THD_MED_H_MU		(50)
#define S16_SR_REF_RCVR_P_THD_HIGH_MU		(50)

#define S16_SR_ALLOW_SUPPL_GRADE_WHL_ST		(10)
#define S16_SR_ALLOW_SUPPL_GRADE_WHL_P		(10)

#define U8_SR_REF_WHL_ST_L1_THD				(100)
#define U8_SR_REF_WHL_ST_L2_THD				(100)
#define U8_SR_REF_WHL_ST_L3_THD				(80)
#define U8_SR_REF_WHL_P_L1_THD				(100)
#define U8_SR_REF_WHL_P_L2_THD				(100)
#define U8_SR_REF_WHL_P_L3_THD				(80)

#define U8_SEVERE_SR_REF_WHL_ST_L1_THD		(80)
#define U8_SEVERE_SR_REF_WHL_ST_L2_THD		(80)
#define U8_SEVERE_SR_REF_WHL_ST_L3_THD		(60)
#define U8_SEVERE_SR_REF_WHL_P_L1_THD		(90)
#define U8_SEVERE_SR_REF_WHL_P_L2_THD		(80)
#define U8_SEVERE_SR_REF_WHL_P_L3_THD		(60)

/*Global Type Declaration ****************************************************/
typedef struct
{
	uint8_t s8RefRatioEstValueOfSpd;  /*mon*/
	uint8_t s8RefRatioEstValueOfPress; /*mon*/
	int16_t s16SlipGradeOfRefWhlSt;
	int16_t s16AradGradeOfRefWhlSt;
	int16_t s16PulseUpGradeOfRefWhlP;
	int16_t s16RecoverPGradeOfRefWhlP;
	int16_t s16GradeOfRefWhlSt;
	int16_t s16GradeOfRefWhlP;
	int16_t s16DumpRecoveryRatio;
	int16_t s16AbsStrkRcvrCntForCycle;

	unsigned u1AbsWhlStable		:1;
	unsigned u1AbsWhlStableOld	:1;
}lcStrkRecvryWhlSt_t;

/*Global external Variable  Declaration*****************************************/

 #if __ABS_STK_RCVR_CTRL_MON == ENABLE
extern struct W_STRUCT WhlMon;
extern int16_t lcabss16StrkRecSlipThd;
extern int16_t lcabss16StrkRecAradThd;
extern int16_t lcabss16StrkRecDUMPRecvRateThd;
extern int16_t lcabss16StrkRecPulseUpCntThd;
extern uint8_t lcabsu8strkrcvrdebugger;
extern lcStrkRecvryWhlSt_t lcStrkRecvryWhlStFL, lcStrkRecvryWhlStFR, referencemonwhl;
extern uint8_t u8ReferenceWheelOfSR;
extern int16_t lcabss16StrkRcvrAftActCnt;
extern int8_t lcabss8FrontRiseTimeAfStrkRcvr;
  #endif

extern	void LCABS_vStrokeRecoveryMain(void);

#endif
