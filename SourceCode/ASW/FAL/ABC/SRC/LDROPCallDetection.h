#ifndef __LDROPCALLDETECTION_H__
#define __LDROPCALLDETECTION_H__

#include "LVarHead.h"

/*Global Extern Variable  Declaration*****************************************/

/*  ROP Flag1   */
#define ROP_REQ_ACT						ROPF0.bit7
#define ROP_REQ_ENG						ROPF0.bit6
#define ROP_FADE_MODE					ROPF0.bit5
#define ROP_REQ_FRT_Pre_FR				ROPF0.bit4
#define ROP_REQ_FRT_Pre_FL				ROPF0.bit3
#define ROP_REQ_FRT_Pre				    ROPF0.bit2
#define ROP_REQ_ACT_old				    ROPF0.bit1
#define flag_ay_start_FL				ROPF0.bit0
 
#define flag_ay_start_FR				ROPF1.bit7
#define flag_wstr_start_FR				ROPF1.bit6
#define flag_wstr_start_FL				ROPF1.bit5
#define flag_1st_cycle_POS				ROPF1.bit4
#define flag_ay_sign_POS_old			ROPF1.bit3
#define flag_1st_cycle_NEG				ROPF1.bit2
#define flag_ay_sign_NEG_old		    ROPF1.bit1
#define ROP_DYN_STABLE_PHASE				ROPF1.bit0 
 
#define  lcu1ropPressLmt1stFL       			ROPF2.bit7
#define  lcu1ropPressLmt2ndFL       			ROPF2.bit6
#define  lcu1ropPressLmt1stFR       			ROPF2.bit5
#define  lcu1ropPressLmt2ndFR       			ROPF2.bit4
#define lcu1ropFrtPressHold_FL			    ROPF2.bit3
#define lcu1ropFrtPressHold_FR				ROPF2.bit2

#define lcu1ropRearCtrlReqRL		        ROPF2.bit1
#define lcu1ropRearCtrlReqRR				ROPF2.bit0 
 
#define  lcu1ropFrtCtrlReqFL       			ROPF3.bit7
#define  lcu1ropFrtCtrlReqFR       			ROPF3.bit6
#define  not_rop_3_5       			ROPF3.bit5
#define  not_rop_3_4       			ROPF3.bit4
#define not_rop_3_3			      ROPF3.bit3
#define not_rop_3_2				    ROPF3.bit2
#define not_rop_3_1		          ROPF3.bit1
#define not_rop_3_0				      ROPF3.bit0  

extern U8_BIT_STRUCT_t ROPF0, ROPF1, ROPF2, ROPF3   ;
 
 /* ROP Parameter */
//extern int16_t    roll_angle_old, roll_angle, roll_angle_rate_old, roll_angle_rate, xs1_roll_old, xs1_roll, xs2_roll_old, xs2_roll ; 	
extern int16_t    beta_dot_lf_old, beta_dot_lf, vel_beta_dot ;
extern int16_t    index_weight_ay, RI_1_Weight;
//extern int16_t    RI_max ;
extern int16_t    det_RI_max, det_RI_max_old, ROP_FADE_cnt ;
extern int16_t    RI_1, RI_2 ;
extern int16_t    alat_dot2_lf, alat_dot2, alat_dot2_lf_old ;
extern int16_t    moment_pre_front_rop;
extern int8_t     ttp_cnt_fr, ttp_cnt_fl, time_to_prefill;
//extern int16_t    moment_q_front_rop ;
//extern int8_t     TTL_time;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 0)
extern int8_t   rop_rear_ctrl_mode_rl, rop_rear_ctrl_mode_rr  ;
#endif

extern int16_t  rop_frt_pre_ctrl_fl, rop_frt_pre_ctrl_fr ;

extern int16_t    rop_torq ;
extern int16_t    m_q_ay, m_q_ay_old ;
//extern int16_t    abs_delta_alat, alat_dot_org, alat_dot_lf_old, alat_dot_lf, alat_dot_rop ; 
extern int16_t    abs_alat_rop_lf, abs_alat_rop_lf_old, abs_alat_rop ; 
extern int16_t    ROP_FrtPrefill_WstrDot_Ent, ROP_FrtPrefill_WstrDot_Ext, ROP_FrtPrefill_YawDot_Ent, ROP_FrtPrefill_YawDot_Ext  ;

 

#if __ROP

extern  void 	LDROP_vCallDetection(void);
extern  void 	LCROP_vCallControl(void);
extern  void 	LCROP_vInterfaceMoment(void); 

#endif 
 
#endif


