#ifndef __LALVBACALLACTHW_H__
#define __LALVBACALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"


/*Global Type Declaration ****************************************************/
#define	S16_INIT_TARGET_V_AT_VAC_FAIL1	MSC_2_V
#define	S16_INIT_TARGET_V_AT_VAC_FAIL2	MSC_3_V
#define	S16_INIT_TARGET_V_AT_VAC_FAIL3	MSC_4_V
#define	S16_INIT_TARGET_V_AT_VAC_FAIL4	MSC_7_V     

#define S16_INIT_TARGET_V_W_EST_P1		MSC_4_V
#define S16_INIT_TARGET_V_W_EST_P2		MSC_3_5_V
#define S16_INIT_TARGET_V_W_EST_P3		MSC_3_V
#define	S16_INIT_TARGET_VMAX_W_EST_P3	MSC_5_V

#define	S16_INIT_TARGET_V_APR_VAC10_1	MSC_1_V  
#define	S16_INIT_TARGET_V_APR_VAC10_2	MSC_1_5_V
#define	S16_INIT_TARGET_V_APR_VAC10_3	MSC_3_V
                              
#define	S16_INIT_TARGET_V_AP_VAC10_1 	MSC_1_V
#define	S16_INIT_TARGET_V_AP_VAC10_2 	MSC_1_V
#define	S16_INIT_TARGET_V_AP_VAC10_3 	MSC_4_V
#define	S16_INIT_TARGET_V_AP_VAC10_4 	MSC_5_V     

#define	S16_TARGET_V_AP_BW_VAC10_1   	MSC_1_V
#define	S16_TARGET_V_AP_BW_VAC10_2   	MSC_2_V
#define	S16_TARGET_V_AP_BW_VAC10_3   	MSC_3_V
#define	S16_TARGET_V_AP_BW_VAC10_4   	MSC_6_V

#define	S16_TARGET_V_AP_AB_VAC10_1  	MSC_1_V
#define	S16_TARGET_V_AP_AB_VAC10_2		MSC_1_V    
#define	S16_TARGET_V_AP_AB_VAC10_3		MSC_1_5_V    
#define	S16_TARGET_V_AP_AB_VAC10_4		MSC_3_V    
                                    	
#define	U16_INIT_TARGET_VOLT_AT_STOP_L1 MSC_7_V
#define	U16_INIT_TARGET_VOLT_AT_STOP_L2 MSC_5_V
#define	U16_INIT_TARGET_VOLT_AT_STOP_L3	MSC_3_V

#define	U16_TARGET_VOLT_AT_STOP_L1		MSC_4_V  
#define	U16_TARGET_VOLT_AT_STOP_L2		MSC_3_V 
#define	U16_TARGET_VOLT_AT_STOP_L3		MSC_1_V

#define	S16_TARGET_V_COMP_SPEED_H 		MSC_2_V
#define	S16_TARGET_V_COMP_SPEED_M 		MSC_1_5_V
#define	S16_TARGET_V_COMP_SPEED_L 		MSC_1_V
#define S16_TARGET_V_ON_CAR_STOP		MSC_1_V /*car stops after running*/

#define	U8_TC_INIT_CURR_AT_LVBA_END	350

/*Global Extern Variable  Declaration*****************************************/
extern  struct	U8_BIT_STRUCT_t LVBA03;
extern int16_t lvba_msc_target_vol_old;

/*Global Extern Functions  Declaration****************************************/
  #if __LVBA
extern void	LALVBA_vCallActHW(void);
extern int16_t LAMFC_s16SetMFCCurrentLVBA(uint8_t CIRCUIT, int16_t MFC_Current_old);
 #if __ADVANCED_MSC
extern void LCMSC_vSetLVBATargetVoltage(void);
 #endif
  #endif/*__LVBA*/
  
  
#endif
