/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LOCallMain.c
* Description:		Coordination Main Code of Logic
* Logic version:	HV25
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C12			eslim			Initial Release
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LOCallMain
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/

#include "LOCallMain.h"

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/
void LO_vCallMainCoordinator(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LO_vCallMainCoordinator
* CALLED BY:			L_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Coordination Code of System
********************************************************************************/
void LO_vCallMainCoordinator(void)
{

#if __VDC
	LOABS_vCallCoordinator();
	LOTCS_vCallCoordinator();
	LOESP_vCallCoordinator();
#endif

}	
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LOCallMain
	#include "Mdyn_autosar.h"
#endif