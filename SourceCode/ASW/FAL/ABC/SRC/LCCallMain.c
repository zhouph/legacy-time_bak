/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LCCallMain.c
* Description:      Main Control of Logic
* Logic version:    HV25
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C12         eslim           Initial Release
********************************************************************************/

/* Includes ********************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCCallMain 
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/



#include "LCCallMain.h"
#include "LCABSCallControl.h"
#include "LCHBBCallControl.h"
#include "LCHSACallControl.h"
#include "LCHRBCallControl.h"
#include "LCSPASCallControl.h"
#include "LCAVHCallControl.h"
#include "hm_logic_var.h"
#include "LCACCCallControl.h"
#if __SCC
#include "../SRC/SCC/LCSCCCallControl.h"
#endif
#if __TVBB
#include "LCTVBBCallControl.h"
#endif
#if __ROP
#include "LDROPCallDetection.h"
#endif
#if __PAC
#include "LCPACCallControl.h"
#endif
#if __EVP || __VACUUM_PUMP_RELAY
#include "LCEVPCallControl.h"
#endif
#include "LCPBACallControl.h"


/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LC_vCallMainControl
* CALLED BY:            L_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Control Main of System
********************************************************************************/
void LC_vCallMainControl(void)
{

#if __ROP
    LCROP_vCallControl();   
#endif	
	
#if __VDC
    LCESP_vCallControl(); /* putting ESP before ABS for logic flow structure */
#endif

#if __TOD
	LCTOD_vCallControl();
#endif

    LCABS_vCallControl();

#if __ETC
    LCETC_vCallControl();
#endif

#if __TCS
    LCTCS_vCallControl();
#endif

#if __ETSC
 	LCETSC_vCallControl();
#endif

#if __VSM
   	LCEPS_vCallControl();
#endif

#if __RDCM_PROTECTION
	LCRDCM_vCallControl();
#endif

#if defined(__CDC)   	
  	LCCDC_vCallControl(); 
#endif    	

#if __EDC
    LCEDC_vCallControl();
#endif

#if defined(__dDCT_INTERFACE)
	LCHRC_vCallControl();
#endif

#if __HSA
    LCHSA_vCallControl();
#endif

#if __AVH
    LCAVH_vCallControl();
#endif

#if __ACC
    LCACC_vCallControl();
#endif

#if __HDC
    LCHDC_vCallControl();
#endif

#if __EPB_INTERFACE
    LCEPB_vCallControl();
#endif

#if __DEC
    LCDEC_vCallControl();
#endif

#if __TSP
    LCTSP_vCallControl();
#endif

#if __BDW
    LCBDW_vCallControl();
#endif

#if __EBP
    LCEBP_vCallControl();
#endif

#if __CBC
    LCCBC_vCallControl();
#endif

  #if __PAC
if (U8_PACC_ENABLE == 1)
{
    LCPAC_vCallControl();
}
  #endif

#if __SLS
    LCSLS_vCallControl();
#endif

#if __TVBB
    LCTVBB_vCallControl();
#endif
#if __TCMF_LowVacuumBoost
    LCHBB_vCallControl();
#endif

#if __HRB
    LCHRB_vCallControl();
#endif

#if __HEV
	LCARB_vCallControl();
#endif
#if __VDC
	LCPBA_vCallControl();
#endif
#if __FBC
	LCFBC_vCallControl();
#endif
#if __SBC
    LCSPAS_vCallControl();
#endif

#if __SCC
	LCSCC_vCallControl();
  #if ((__AHB_GEN3_SYSTEM == DISABLE)  && (__IDB_LOGIC == DISABLE))
	LCWPC_vCallControl();
  #endif /* (__AHB_GEN3_SYSTEM == DISABLE) */
#endif

#if __EVP
	LCEVP_vCallControl();
#endif

#if __HOB /* Hydraulic Over Boost */
    LCHOB_vCallControl();
#endif

}
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCCallMain
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
