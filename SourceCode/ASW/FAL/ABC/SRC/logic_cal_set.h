///*060602 for HM reconstruction by eslim*/
///* all are moving to ����.par */
//
//#if __VDC
//#if __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_JM_2WD_EU.ch"
//#include _tuning\ESP_LFC_JM_2WD_EU.ch2"
//#elif __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==1)
//    #if defined(__CCP_ENABLE)
//        #if !SIM_MATLAB
//            #include Cal_data\LTuneJmEsp2wdKr.h"
//        #else
//            #include Cal_data\LTuneJmEsp2wdKrSils.h"
//        #endif
//    #endif
//#elif __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp2wdUs.h"
//    #endif
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_JM_4WD_EU.ch"
//#include _tuning\ESP_LFC_JM_4WD_EU.ch2"
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==1)
//    #if defined(__CCP_ENABLE)
//        #if !SIM_MATLAB
//            #if __YMC_DeltaM
//                #include Cal_data\LTuneJmEsp4wdKrYmc.h"
//            #else
//                #include Cal_data\LTuneJmEsp4wdKr.h"
//            #endif
//        #else
//            #include Cal_data\LTuneJmEsp4wdKrSils.h"
//        #endif
//    #endif
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp4wdUs.h"
//    #endif
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_KM_2WD_EU.ch"
//#include _tuning\ESP_LFC_KM_2WD_EU.ch2"
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_KM_2WD_KR.ch"
//#include _tuning\ESP_LFC_KM_2WD_KR.ch2"
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneKmEsp2wdUs.h"
//    #endif
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_KM_4WD_EU.ch"
//#include _tuning\ESP_LFC_KM_4WD_EU.ch2"
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_KM_4WD_KR.ch"
//#include _tuning\ESP_LFC_KM_4WD_KR.ch2"
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneKmEsp4wdUs.h"
//    #endif
//#elif __CAR == HMC_JB
//#include _tuning\ESP_LFC_JB.ch"
//#include _tuning\ESP_LFC_JB.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif (__CAR==HMC_SM || __CAR==HMC_SM_MGH_40) && !__4WD
//*/
//#elif __CAR==HMC_SM && !__4WD
//#include _tuning\ESP_LFC_SM.ch"
//#include _tuning\ESP_LFC_SM.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif (__CAR==HMC_SM || __CAR==HMC_SM_MGH_40) && __4WD
//*/
//#elif __CAR==HMC_SM && __4WD
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneSmEsp4wd.h"
//    #endif
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR==HMC_SM_SIGMA && !__4WD
//#include _tuning\ESP_LFC_SM35_2WD.ch"
//#include _tuning\ESP_LFC_SM35_2WD.ch2"
//#elif __CAR==HMC_SM_SIGMA && __4WD
//#include _tuning\ESP_LFC_SM35_4WD.ch"
//#include _tuning\ESP_LFC_SM35_4WD.ch2"
//*/
//#elif __CAR == HMC_EF
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneEfEsp.h"
//    #endif
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR == HMC_EF_UCC
//// #include _tuning\ESP_LFC_EF_UCC.ch"
//// #include _tuning\ESP_LFC_EF_UCC.ch2"
//*/
//#elif __CAR==HMC_GK &&  (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_GK_NORMAL.ch"
//#include _tuning\ESP_LFC_GK_NORMAL.ch2"
//#elif __CAR==HMC_GK &&  (__SUSPENSION_TYPE==0)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneGkSpoEsp.h"
//    #endif
//#elif __CAR == HMC_XG_UCC
//#include _tuning\ESP_LFC_XG_UCC.ch"
//#include _tuning\ESP_LFC_XG_UCC.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR==HMC_XG_BBW
//#include _tuning\ESP_LFC_XG_UCC.ch"
//#include _tuning\ESP_LFC_XG_UCC.ch2"
//#elif __CAR==HMC_SM_BBW && __4WD
//#include _tuning\ESP_LFC_SM35_4WD.ch"
//#include _tuning\ESP_LFC_SM35_4WD.ch2"
//#elif __CAR==AUDI_A6
//#include _tuning\ESP_LFC_AUDI_A6.ch"
//#include _tuning\ESP_LFC_AUDI_A6.ch2"
//*/
//#elif __CAR==BMW_X5
//#include _tuning\ESP_LFC_BMW_X5.ch"
//#include _tuning\ESP_LFC_BMW_X5.ch2"
//#elif __CAR == KMC_MG
//#include _tuning\ESP_LFC_MG.ch"
//#include _tuning\ESP_LFC_MG.ch2"
//#elif __CAR == KMC_UN
//#include _tuning\ESP_LFC_UN.ch"
//#include _tuning\ESP_LFC_UN.ch2"
//#elif __CAR==KMC_GH || __CAR==KMC_GH_MECU
//#include _tuning\ESP_LFC_GH.ch"
//#include _tuning\ESP_LFC_GH.ch2"
//#elif __CAR == HMC_FC
//#include _tuning\ESP_LFC_FC.ch"
//#include _tuning\ESP_LFC_FC.ch2"
//
//
//#elif __CAR==HMC_NF || __CAR==HMC_NF_MECU
//    #if defined(__CCP_ENABLE)
//        #if __YMC_DeltaM
//            #include Cal_data\LTuneNfEspYmc.h"
//        #else
//            #include Cal_data\LTuneNfEsp.h"
//        #endif
//    #endif
//#elif ((__CAR==SYC_RAXTON) && (__SUSPENSION_TYPE==1)) || ((__CAR==SYC_RAXTON_MECU) && (__SUSPENSION_TYPE==1))
//    #if defined(__CCP_ENABLE)
//        #if __YMC_DeltaM
//            #include Cal_data\LTuneRxEsp4wdYmc.h"
//        #else
//            #include Cal_data\LTuneRxEsp4wd.h"
//        #endif
//    #endif
//#elif ((__CAR==SYC_RAXTON) && (__SUSPENSION_TYPE==0)) || ((__CAR==SYC_RAXTON_MECU) && (__SUSPENSION_TYPE==0))
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneRxEspRwd.h"
//    #endif
//#elif ((__CAR==SYC_KYRON) || (__CAR==SYC_KYRON_MECU))
//    #if defined(__CCP_ENABLE)
//        #if __YMC_DeltaM
//            #include Cal_data\LTuneKyEsp4wdYmc.h"
//        #else
//            #include Cal_data\LTuneKyEsp4wd.h"
//        #endif
//    #endif
//#elif __CAR==ASTRA_MECU
//#include _tuning\ESP_LFC_ASTRA_MECU.ch"
//#include _tuning\ESP_LFC_ASTRA_MECU.ch2"
//#elif __CAR==FORD_C214 || __CAR==FORD_C214_MECU
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneFocusEsp.h"
//    #endif
//#elif __CAR==GM_C100 || __CAR==GM_C100_MECU
//    #if defined(__CCP_ENABLE)
//		#include Cal_data\LTuneC100Esp.h"
//    #endif
//#elif __CAR==GM_TAHOE || __CAR==GM_TAHOE_MECU
//    #if defined(__CCP_ENABLE)
//		#include Cal_data\LTuneTahoeEsp.h"
//    #endif    
//#elif __CAR==HMC_EN && !__4WD
//#include _tuning\ESP_LFC_EN_2WD.ch"
//#include _tuning\ESP_LFC_EN_2WD.ch2"
//#elif __CAR==HMC_EN && __4WD
//#include _tuning\ESP_LFC_EN_4WD.ch"
//#include _tuning\ESP_LFC_EN_4WD.ch2"
//#elif __CAR==HMC_TQ
//#include _tuning\ESP_LFC_TQ.ch"
//#include _tuning\ESP_LFC_TQ.ch2"
//
//#elif (((__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==0)) || ((__CAR==KMC_HM) && (!__4WD)) || ((__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==3)))  // HM, RWD(2H,4H)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneHmEspRwd.h"
//    #endif
//#elif ((__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==1))
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneHmEsp4wd.h"
//    #endif
//#elif __CAR == GM_DAT_MAGNUS
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneMagnusEsp.h"
//    #endif
//#elif __CAR == HMC_PA
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTunePAEsp.h"
//    #endif
//#endif
//
//#else
//
//#if __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_JM_2WD_EU.ch"
//#include _tuning\ESP_LFC_JM_2WD_EU.ch2"
//#elif __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==1)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp2wdKr.h"
//    #endif
//#elif __CAR==HMC_JM && !__4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp2wdUs.h"
//    #endif
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_JM_4WD_EU.ch"
//#include _tuning\ESP_LFC_JM_4WD_EU.ch2"
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==1)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp4wdKr.h"
//    #endif
//#elif __CAR==HMC_JM && __4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneJmEsp4wdUs.h"
//    #endif
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_KM_2WD_EU.ch"
//#include _tuning\ESP_LFC_KM_2WD_EU.ch2"
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_KM_2WD_KR.ch"
//#include _tuning\ESP_LFC_KM_2WD_KR.ch2"
//#elif __CAR==KMC_KM && !__4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneKmEsp2wdUs.h"
//    #endif
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==0)
//#include _tuning\ESP_LFC_KM_4WD_EU.ch"
//#include _tuning\ESP_LFC_KM_4WD_EU.ch2"
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_KM_4WD_KR.ch"
//#include _tuning\ESP_LFC_KM_4WD_KR.ch2"
//#elif __CAR==KMC_KM && __4WD && (__SUSPENSION_TYPE==2)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneKmEsp4wdUs.h"
//    #endif
//#elif __CAR == HMC_JB
//#include _tuning\ESP_LFC_JB.ch"
//#include _tuning\ESP_LFC_JB.ch2"
//#elif __CAR==HMC_SM && !__4WD
//#include _tuning\ESP_LFC_SM.ch"
//#include _tuning\ESP_LFC_SM.ch2"
//#elif __CAR==HMC_SM && __4WD
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneSmEsp4wd.h"
//    #endif
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR==HMC_SM_SIGMA && !__4WD
//#include _tuning\ESP_LFC_SM35_2WD.ch"
//#include _tuning\ESP_LFC_SM35_2WD.ch2"
//#elif __CAR==HMC_SM_SIGMA && __4WD
//#include _tuning\ESP_LFC_SM35_4WD.ch"
//#include _tuning\ESP_LFC_SM35_4WD.ch2"
//*/
//#elif __CAR == HMC_EF
//#include _tuning\ESP_LFC_EF.ch"
//#include _tuning\ESP_LFC_EF.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR == HMC_EF_UCC
//// #include _tuning\ESP_LFC_EF_UCC.ch"
//// #include _tuning\ESP_LFC_EF_UCC.ch2"
//*/
//#elif __CAR==HMC_GK &&  (__SUSPENSION_TYPE==1)
//#include _tuning\ESP_LFC_GK_NORMAL.ch"
//#include _tuning\ESP_LFC_GK_NORMAL.ch2"
//#elif __CAR==HMC_GK &&  (__SUSPENSION_TYPE==0)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneGkSpoEsp.h"
//    #endif
//#elif __CAR == HMC_XG_UCC
//#include _tuning\ESP_LFC_XG_UCC.ch"
//#include _tuning\ESP_LFC_XG_UCC.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR==HMC_XG_BBW
//#include _tuning\ESP_LFC_XG_UCC.ch"
//#include _tuning\ESP_LFC_XG_UCC.ch2"
//#elif __CAR==HMC_SM_BBW && __4WD
//#include _tuning\ESP_LFC_SM35_4WD.ch"
//#include _tuning\ESP_LFC_SM35_4WD.ch2"
//#elif __CAR == AUDI_A6
//#include _tuning\ESP_LFC_AUDI_A6.ch"
//#include _tuning\ESP_LFC_AUDI_A6.ch2"
//*/
//#elif __CAR == HMC_TB
//#include _tuning\ESP_LFC_TB.ch"
//#include _tuning\ESP_LFC_TB.ch2"
//#elif __CAR == HMC_XD
//#include _tuning\ESP_LFC_XD.ch"
//#include _tuning\ESP_LFC_XD.ch2"
//#elif __CAR == HMC_FC
//#include _tuning\ESP_LFC_FC.ch"
//#include _tuning\ESP_LFC_FC.ch2"
//#elif __CAR == HMC_SA
//#include _tuning\ESP_LFC_SA.ch"
//#include _tuning\ESP_LFC_SA.ch2"
//#elif __CAR == HMC_A1 && __4WD
//#include _tuning\ESP_LFC_A1_4WD.ch"
//#include _tuning\ESP_LFC_A1_4WD.ch2"
//#elif __CAR == HMC_A1 && !__4WD
//#include _tuning\ESP_LFC_A1.ch"
//#include _tuning\ESP_LFC_A1.ch2"
//#elif __CAR == HMC_FO
//#include _tuning\ESP_LFC_FO.ch"
//#include _tuning\ESP_LFC_FO.ch2"
//#elif __CAR == DCX_PT
//#include _tuning\ESP_LFC_PT.ch"
//#include _tuning\ESP_LFC_PT.ch2"
//#elif __CAR == GMC_ION
//#include _tuning\ESP_LFC_ION.ch"
//#include _tuning\ESP_LFC_ION.ch2"
//#elif __CAR == GM_DAT_KALOS
//#include _tuning\ESP_LFC_GM_DAT_KALOS.ch"
//#include _tuning\ESP_LFC_GM_DAT_KALOS.ch2"
//#elif __CAR == GM_DAT_REZZO
//#include _tuning\ESP_LFC_GM_DAT_REZZO.ch"
//#include _tuning\ESP_LFC_GM_DAT_REZZO.ch2"
//#elif __CAR == GM_DAT_MAGNUS
//#include _tuning\ESP_LFC_GM_DAT_MAGNUS.ch"
//#include _tuning\ESP_LFC_GM_DAT_MAGNUS.ch2"
//#elif __CAR == KMC_CT
//#include _tuning\ESP_LFC_CT.ch"
//#include _tuning\ESP_LFC_CT.ch2"
//#elif __CAR == KMC_MG
//#include _tuning\ESP_LFC_MG.ch"
//#include _tuning\ESP_LFC_MG.ch2"
//#elif __CAR == KMC_UN
//#include _tuning\ESP_LFC_UN.ch"
//#include _tuning\ESP_LFC_UN.ch2"
//#elif __CAR == HMC_MC
//#include _tuning\ESP_LFC_MC.ch"
//#include _tuning\ESP_LFC_MC.ch2"
///*  HV21, for deleting warning msg by eslim */
///*
//#elif __CAR == CHANG_AN_CV9
//#include _tuning\ESP_LFC_CHANG_AN_CV9.ch"
//#include _tuning\ESP_LFC_CHANG_AN_CV9.ch2"
//*/
//#elif (((__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==0)) || ((__CAR==KMC_HM) && (!__4WD)) || ((__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==3)))  // HM, RWD(2H,4H)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneHmEspRwd.h"
//    #endif
//#elif (__CAR==KMC_HM) && (__4WD)&& (__SUSPENSION_TYPE==1)
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneHmEsp4wd.h"
//    #endif
//#elif ((__CAR==SYC_KYRON) || (__CAR==SYC_KYRON_MECU))
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTuneKyEsp4wd.h"
//    #endif
//#elif __CAR == HMC_PA
//    #if defined(__CCP_ENABLE)
//        #include Cal_data\LTunePAEsp.h"
//    #endif
//#endif
//
//#endif
//
