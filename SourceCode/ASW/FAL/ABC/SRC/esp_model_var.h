/*060602 for HM reconstruction by eslim*/
/* all are divided to files of each vehicle */
#include "Abc_Ctrl.h"
    const extern int16_t Yc_Vx[20];
    const extern int16_t Yc_St[22];
    const extern int16_t Yc_Map[22][20];
   
    
    extern int16_t M_Den1_10N,  M_Den2_10N,  M_Num_10N;  
    
	extern int16_t C_1, C_22, C_21; 
	
	extern int16_t Sp_f1, Sp_f2, Sp_f3, Sp_f4;	

	extern int16_t Alat_speed_v1, Alat_filt_value_v1_from_speed; 
    extern int16_t Alat_speed_v2, Alat_filt_value_v2_from_speed;
    extern int16_t Alat_speed_v3, Alat_filt_value_v3_from_speed;
    extern int16_t Alat_speed_v4, Alat_filt_value_v4_from_speed;
    extern int16_t Alat_speed_v5, Alat_filt_value_v5_from_speed;

