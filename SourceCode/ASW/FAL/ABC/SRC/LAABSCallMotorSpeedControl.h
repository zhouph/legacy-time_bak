#ifndef __LAABSCALLMOTORSPEEDCONTROL_H__
#define __LAABSCALLMOTORSPEEDCONTROL_H__

#include "LVarHead.h"
/*Global CONSTANT Declaration*************************************************/


  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
#define __MSC_DIRECT_DUTY_CONTROL         ENABLE
  #endif


  #if (__CAR == BMW740i)
#define	MGH80_6P_PREMIUM		ENABLE
  #else
#define	MGH80_6P_PREMIUM		DISABLE  
  #endif

#if __L_CYCLETIME == LOOP_10MS

    #define L_MSC_ACT_PERIOD    10
  #if __VDC
    #define S8_MSC_OFF_TIME_AFTER_DUMP  L_TIME_80MS
  #else
    #define S8_MSC_OFF_TIME_AFTER_DUMP  L_TIME_90MS
  #endif

#else /*__L_CYCLETIME == LOOP_10MS*/

    #define L_MSC_ACT_PERIOD    5
    #define S8_MSC_OFF_TIME_AFTER_DUMP  L_TIME_85MS

#endif /*__L_CYCLETIME == LOOP_10MS*/

  #if __VDC
#define S16_ABS_MSC_TARGET_VOL_MIN  MSC_1_V
  #else
#define S16_ABS_MSC_TARGET_VOL_MIN  MSC_2_V
  #endif/*__VDC*/
  
#define U8_ABS_ESC_COOP_TAR_VOL_RATIO_PB    20
#define U8_ABS_ESC_COOP_TAR_VOL_RATIO		10/*resolution:10 ex) 20: 200%, 10: 100%*/

  #if (__CAR == BMW740i)
#define U16_MSC_INITIAL_TARGET_VOL	MSC_10_V
  #else
#define U16_MSC_INITIAL_TARGET_VOL	MSC_14_V
  #endif

 #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
#define U8_MSC_PERIOD_SCAN			10 /* Control period: 10scan-->100ms */
#define U8_MSC_MOTOR_OFF_SCAN		1  /* MSC off scan for checking BEMF: 1scan-->10ms */
#define U8_MSC_BEMF_MONITOR_MS      8  /* BEMF check at 8ms after motor off*/

  #if (__CAR == BMW740i)
#define MSC_ADAPT_DUTY_MIN  (-50)
#define MSC_ADAPT_DUTY_MAX  50
  #else
#define MSC_ADAPT_DUTY_MIN  (-40)
#define MSC_ADAPT_DUTY_MAX  40
  #endif
  
#define U8_MSC_DUTY_90P                   180
#define U8_MSC_DUTY_10P                   20
#define U8_MSC_DUTY_MAX                  U8_MSC_DUTY_90P
#define U8_MSC_DUTY_MIN                  U8_MSC_DUTY_10P

  #if (MGH80_6P_PREMIUM==ENABLE)
#define S16_COMP_VOLT_FOR_DUTY_ADAPT	MSC_0_4_V
  #else
#define S16_COMP_VOLT_FOR_DUTY_ADAPT	MSC_0_2_V
  #endif/*(MGH80_6P_PREMIUM==ENABLE)*/
  
 #endif/*(__ESC_MOTOR_PWM_CONTROL==ENABLE)*/

  #if __VDC
#define __MOTOR_OFF_AT_LOW_BRAKING_ESC  ENABLE
#define __DELAY_MOTOR_DRV_AT_LOW_MP     ENABLE
#define __ADD_MTR_DRV_T_AF_INIT_DELAY	DISABLE
  #else
#define __MOTOR_OFF_AT_LOW_BRAKING_ESC  DISABLE
#define __DELAY_MOTOR_DRV_AT_LOW_MP     DISABLE
#define __ADD_MTR_DRV_T_AF_INIT_DELAY	DISABLE
  #endif



#define     __MOTOR_MON                 DISABLE       /*  1MSEC MSC Mon   */

#define		__BEMF_LPF_ENABLE			DISABLE

/*Global Extern Variable  Declaration*****************************************/

typedef struct
{
    uint16_t MSC_BIT_07     :1;
    uint16_t MSC_BIT_06     :1;
    uint16_t MSC_BIT_05     :1;
    uint16_t MSC_BIT_04     :1;
    uint16_t MSC_BIT_03     :1;
    uint16_t MSC_BIT_02     :1;
    uint16_t MSC_BIT_01     :1;
    uint16_t MSC_BIT_00     :1;
}MSC_FLAG_t;

extern  U8_BIT_STRUCT_t MSC00;
#if __VDC
extern  U8_BIT_STRUCT_t MSC01;
extern  U8_BIT_STRUCT_t MSC02;
extern  U8_BIT_STRUCT_t MSC03;
extern  U8_BIT_STRUCT_t MSC04;
extern  U8_BIT_STRUCT_t MSC05;
extern  U8_BIT_STRUCT_t MSC06;
#endif

#define EBD_MSC_MOTOR_ON                    MSC00.bit0
#define ABS_MSC_MOTOR_ON                    MSC00.bit1
#define ADVANCED_MSC_ON_FLG                 MSC00.bit2
#define not_used_bit_msc_00_3               MSC00.bit3
#define EBD_MOTOR_DETECT                    MSC00.bit4
#define AVH_MSC_MOTOR_ON                    MSC00.bit5
#define bump_low_speed_motor_stop           MSC00.bit6
#define RBC_MSC_MOTOR_ON               		MSC00.bit7

#if __VDC

#define EBP_MSC_MOTOR_ON                    MSC01.bit0
#define EPC_MSC_MOTOR_ON                    MSC01.bit1
#define TSP_MSC_MOTOR_ON                    MSC01.bit2
#define ROP_MSC_MOTOR_ON                    MSC01.bit3
#define HBB_MSC_MOTOR_ON                    MSC01.bit4
#define PBA_MSC_MOTOR_ON                    MSC01.bit5
#define HRB_MSC_MOTOR_ON                    MSC01.bit6
#define AUX_MSC_MOTOR_ON                    MSC01.bit7

#define EPB_MSC_MOTOR_ON                    MSC02.bit0
#define TCS_MSC_MOTOR_ON                    MSC02.bit1
#define VDC_MSC_MOTOR_ON                    MSC02.bit2
#define ACC_MSC_MOTOR_ON                    MSC02.bit3
#define HDC_MSC_MOTOR_ON                    MSC02.bit4
#define FBC_MSC_MOTOR_ON                    MSC02.bit5
#define BDW_MSC_MOTOR_ON                    MSC02.bit6
#define DEC_MSC_MOTOR_ON               		MSC02.bit7

#define HBB_ABS_COMB_MSC_MOTOR_ON           MSC03.bit0
#define PBA_ABS_COMB_MSC_MOTOR_ON           MSC03.bit1
#define HRB_ABS_COMB_MSC_MOTOR_ON           MSC03.bit2
#define HDC_ABS_COMB_MSC_MOTOR_ON           MSC03.bit3
#define ACC_ABS_COMB_MSC_MOTOR_ON           MSC03.bit4
#define FBC_ABS_COMB_MSC_MOTOR_ON           MSC03.bit5
#define EPB_ABS_COMB_MSC_MOTOR_ON           MSC03.bit6
#define DEC_ABS_COMB_MSC_MOTOR_ON           MSC03.bit7

#define MSC_S_VALVE_PRIMARY                 MSC04.bit0
#define MSC_S_VALVE_SECONDARY               MSC04.bit1
#define MSC_TCL_DEMAND_PRIMARY              MSC04.bit2
#define MSC_TCL_DEMAND_SECONDARY            MSC04.bit3
#define LVBA_ABS_COMB_MSC_MOTOR_ON          MSC04.bit4
#define LVBA_MSC_MOTOR_ON                   MSC04.bit5
#define SCC_ABS_COMB_MSC_MOTOR_ON           MSC04.bit6
#define not_used_bit_msc_04                 MSC04.bit7

#define MSC_HV_VL_fl                        MSC05.bit0
#define MSC_AV_VL_fl                        MSC05.bit1
#define MSC_HV_VL_fr                        MSC05.bit2
#define MSC_AV_VL_fr                        MSC05.bit3
#define MSC_HV_VL_rl                        MSC05.bit4
#define MSC_AV_VL_rl                        MSC05.bit5
#define MSC_HV_VL_rr                        MSC05.bit6
#define MSC_AV_VL_rr                        MSC05.bit7

#endif /* __VDC */

#define ADVANCED_MSC_ON_FF                  MSC06.bit0
#define ADVANCED_MSC_ON_FB               	MSC06.bit1
#define not_used_bit_msc_05_2               MSC06.bit2
#define not_used_bit_msc_05_3               MSC06.bit3
#define not_used_bit_msc_05_4               MSC06.bit4
#define not_used_bit_msc_05_5               MSC06.bit5
#define not_used_bit_msc_05_6               MSC06.bit6
#define not_used_bit_msc_05_7               MSC06.bit7

#if __DELAY_MOTOR_DRV_AT_LOW_MP

#define lamscu1DelayMtrDriveInABS       	lamscCtrlFlg0.MSC_BIT_07
#define lamscu1MtrStopByDlayMtrDrvInABS 	lamscCtrlFlg0.MSC_BIT_06
#define lcmscu1ABSAfterBLS              	lamscCtrlFlg0.MSC_BIT_05
#define lamscu1ESVActivatedWoBrkBfABS   	lamscCtrlFlg0.MSC_BIT_04
#define lamscu1ABSAfESVActWoBraking     	lamscCtrlFlg0.MSC_BIT_03
#define lamscu1ESVActWoBraking          	lamscCtrlFlg0.MSC_BIT_02
#define lamscu1DelayMtrDriveInABSOld    	lamscCtrlFlg0.MSC_BIT_01

#endif /*#if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)*/

/* GlobalFunction prototype ****************************************/
#if __MOTOR_MON==1
/* MOTORO 1MSEC MON     */
/*  *JUHO**  Motor Monitoring variable  */
extern int16_t MonMotorAD_temp[10], MonMotorAD[10];
extern uint16_t Mon1mscnt;
#endif

/*extern uint8_t LAMSC_vCallActHW_interupt(uint16_t fu16CalVoltInt_MOTOR);*/
extern uint8_t LAMSC_vCallActHW_interupt(uint16_t mtp_ad);
extern void LAABS_vSetMotorCommand(void);

#if __HDC
extern void LCMSC_vSetHDCTargetVoltage(void);
#endif

#if __ACC
extern void LCMSC_vSetACCTargetVoltage(void);
#endif

#if __EPB_INTERFACE
extern struct   U8_BIT_STRUCT_t EPBF0;
#endif

#if __EBP
extern void LCMSC_vSetEBPTargetVoltage(void);
#endif

#if __ENABLE_PREFILL
extern void LCMSC_vSetEPCTargetVoltage(void);
#endif

#if __TCS
extern void LCMSC_vSetTCSTargetVoltage(void);
#endif

#if __TSP
extern void LCMSC_vSetTSPTargetVoltage(void);
#endif

extern MSC_FLAG_t lamscCtrlFlg0;

extern int16_t    target_vol, motor_on_time_msec;
extern int16_t    abs_msc_target_vol;
extern int16_t	  motor_rpm;

extern int16_t    msec_counter;
extern int8_t 	  additional_abs_motor_on;
extern uint8_t    MOT_ON_FLG_counter;

  #if (__ESC_MOTOR_PWM_CONTROL == ENABLE)
extern uint8_t	  lau8MscDuty, lau8MscDuty_old;  
extern int16_t	  laabss16BemfForDutyAdapt;
extern uint8_t    lau8MscSetDuty, laabsu8MscScanCounter;
  #endif

extern int16_t  MSC_ref_voltage;
extern int16_t 	MSC_indicator;
extern int16_t MSC_primary_indicator;
extern int16_t MSC_secondary_indicator;
extern int8_t las8MSCAdaptDuty;

  #if __VDC
/*
extern int16_t tcs_msc_target_vol;
extern int16_t vdc_msc_target_vol;

extern int16_t rop_msc_target_vol;
*/
extern int16_t tsp_msc_target_vol;


extern int16_t acc_msc_target_vol;
extern int16_t hdc_msc_target_vol;

extern int16_t ebp_msc_target_vol;
extern int16_t epc_msc_target_vol;

extern int16_t epb_msc_target_vol;

extern int16_t aux_msc_target_vol;
  #endif

/*Global Type Declaration ****************************************************/


/* Tuning Parameter ���п���    */

#define	     __Voltage_Mon_Change	ENABLE
#define     S16_ALLOW_MOTOR_OFF_SPEED   VREF_50_KPH

  #if __VDC
#define U8_LPA_FILL_DUMP_MAX    	  13
#define U8_LPA_FILL_DUMP_MAX_MPFAIL    4
#define U8_ADD_FULL_DRV_T_AF_INIT_DELAY  0
  #else
#define U8_LPA_FILL_DUMP_MAX    0
#define U8_ADD_FULL_DRV_T_AF_INIT_DELAY  L_TIME_50MS
  #endif


  /* -------- __DELAY_MOTOR_DRV_AT_LOW_MP parameters  ---------*/

    #define S16_MSC_DELAY_MTR_DRV_MPRESS    MPRESS_50BAR  /* Decel 0.3g , minimum 30bar*/
    #define S16_MSC_DELAY_MTR_DRV_MP_RATE   MPRESS_8BAR   /* Mpress 284bar/sec, minimum 3bar*/
    
  #if __VDC

    #define S16_MSC_DELAY_MTR_DRV_DECEL     AFZ_0G5       /* MP fail or ABS only */
    #define S16_MSC_DELAY_MTR_DRV_BFABS_TIM L_TIME_500MS      /* MP fail or ABS only */
    
      #if((S16_MSC_DELAY_MTR_DRV_MPRESS >= MPRESS_3BAR) && (S16_MSC_DELAY_MTR_DRV_MP_RATE >= MPRESS_3BAR))
        #define S16_MSC_DELAY_MTR_DRV_VREF      VREF_80_KPH
      #else
        #define S16_MSC_DELAY_MTR_DRV_VREF      VREF_50_KPH
      #endif
    
  #else /*__VDC*/

    #define S16_MSC_DELAY_MTR_DRV_DECEL     AFZ_0G3       /* MP fail or ABS only */
    #define S16_MSC_DELAY_MTR_DRV_BFABS_TIM L_TIME_600MS      /* MP fail or ABS only */
    #define S16_MSC_DELAY_MTR_DRV_VREF      VREF_60_KPH

  #endif /*__VDC*/

  /* -------- __DELAY_MOTOR_DRV_AT_LOW_MP parameters  ---------*/

#define     MSC_0_V             0
#define     MSC_0_1_V           100
#define		MSC_0_2_V			200
#define     MSC_0_25_V          250
#define		MSC_0_3_V			300
#define		MSC_0_4_V			400
#define     MSC_0_5_V           500
#define     MSC_0_75_V          750
#define     MSC_1_V             1000
#define     MSC_1_25_V          1250
#define     MSC_1_5_V           1500
#define     MSC_1_75_V          1750
#define     MSC_2_V             2000
#define     MSC_2_25_V          2250
#define     MSC_2_5_V           2500
#define     MSC_2_75_V          2750
#define     MSC_3_V             3000
#define     MSC_3_25_V          3250
#define     MSC_3_5_V           3500
#define     MSC_3_75_V          3750
#define     MSC_4_V             4000
#define     MSC_4_25_V          4250
#define     MSC_4_5_V           4500
#define     MSC_4_75_V          4750
#define     MSC_5_V             5000
#define     MSC_5_25_V          5250
#define     MSC_5_5_V           5500
#define     MSC_5_75_V          5750
#define     MSC_6_V             6000
#define     MSC_6_25_V          6250
#define     MSC_6_5_V           6500
#define     MSC_6_75_V          6750
#define     MSC_7_V             7000
#define     MSC_7_25_V          7250
#define     MSC_7_5_V           7500
#define     MSC_7_75_V          7750
#define     MSC_8_V             8000
#define     MSC_8_25_V          8250
#define     MSC_8_5_V           8500
#define     MSC_8_75_V          8750
#define     MSC_9_V             9000
#define     MSC_9_25_V          9250
#define     MSC_9_5_V           9500
#define     MSC_9_75_V          9750
#define     MSC_10_V            10000
#define     MSC_10_25_V         10250
#define     MSC_10_5_V          10500
#define     MSC_10_75_V         10750
#define     MSC_11_V            11000
#define     MSC_11_25_V         11250
#define     MSC_11_5_V          11500
#define     MSC_11_75_V         11750
#define     MSC_12_V            12000
#define     MSC_12_25_V         12250
#define     MSC_12_5_V          12500
#define     MSC_12_75_V         12750
#define     MSC_13_V            13000
#define     MSC_13_25_V         13250
#define     MSC_13_5_V          13500
#define     MSC_13_75_V         13750
#define     MSC_14_V            14000
#define     MSC_14_25_V         14250
#define     MSC_14_5_V          14500
#define     MSC_14_75_V         14750
#define     MSC_15_V            15000
#define     MSC_15_25_V         15250
#define     MSC_15_5_V          15500
#define     MSC_15_75_V         15750
#define     MSC_16_V            16000

#define     MSC_0_RPM           0
#define     MSC_125_RPM         125
#define     MSC_200_RPM         200
#define     MSC_250_RPM         250
#define     MSC_375_RPM         375
#define     MSC_500_RPM         500
#define     MSC_625_RPM         625
#define     MSC_750_RPM         750
#define     MSC_875_RPM         875
#define     MSC_1000_RPM        1000
#define     MSC_1125_RPM        1125
#define     MSC_1250_RPM        1250
#define     MSC_1375_RPM        1375
#define     MSC_1500_RPM        1500
#define     MSC_1625_RPM        1625
#define     MSC_1750_RPM        1750
#define     MSC_1875_RPM        1875
#define     MSC_2000_RPM        2000
#define     MSC_2125_RPM        2125
#define     MSC_2250_RPM        2250
#define     MSC_2375_RPM        2375
#define     MSC_2500_RPM        2500
#define     MSC_2625_RPM        2625
#define     MSC_2750_RPM        2750
#define     MSC_2875_RPM        2875
#define     MSC_3000_RPM        3000
#define     MSC_3125_RPM        3125
#define     MSC_3250_RPM        3250
#define     MSC_3375_RPM        3375
#define     MSC_3400_RPM        3400
#define     MSC_3500_RPM        3500
#define     MSC_3625_RPM        3625
#define     MSC_3750_RPM        3750
#define     MSC_3875_RPM        3875
#define     MSC_4000_RPM        4000
#define     MSC_4125_RPM        4125
#define     MSC_4250_RPM        4250
#define     MSC_4375_RPM        4375
#define     MSC_4500_RPM        4500
#define     MSC_4625_RPM        4625
#define     MSC_4750_RPM        4750
#define     MSC_4875_RPM        4875
#define     MSC_5000_RPM        5000

#endif      /* ~ __LAABSCALLMOTORSPEEDCONTROL_H__ */

