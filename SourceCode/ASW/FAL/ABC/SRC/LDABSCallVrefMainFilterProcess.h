#ifndef __LDABSVREFFILTER_H__
#define __LDABSVREFFILTER_H__
/*includes********************************************************************/
#include	"LVarHead.h"

/*Global Type Declaration ****************************************************/
#define     __VREF_LIMIT_1              1

#if  ((__PRESS_DECEL_VREF_APPLY == 1)&&(__MPLESS_CONTROL_TYPE == 0))
#define     __VREF_LIMIT_2              1
#else
#define     __VREF_LIMIT_2              0
#endif

#define     __VRSELECT_NEW_CONCEPT		1

#define     __IMPROVED_VREF_IN_ABS      0  /*1st cycle 1000MS -> 1500MS*/
#define     __IMPROVED_VREF_IN_ABS_2    0  /*Nth L2h*/

#if __VREF_LIMIT_1
#define VrefFilot_Limit_flag                      VREF_F0.bit7
#define VrefFilot_Toggle_flag                     VREF_F0.bit6
#define Vref_L2H_suspect_flag                     VREF_F0.bit5
#define ldabsu1Arad10gUpperflagFL				  VREF_F0.bit4
#define ldabsu1Arad10gUpperflagFR				  VREF_F0.bit3
#define ldabsu1Arad5gUpperflagFL				  VREF_F0.bit2
#define ldabsu1Arad5gUpperflagFR				  VREF_F0.bit1
#define ldu1HighMuSuspectFlag					  VREF_F0.bit0
#endif
#if __VREF_LIMIT_2
#define VrefPress_Limit_flag                      VREF_F1.bit7
#define VrefPress_Toggle_flag                     VREF_F1.bit6
#define ABS_START_flag                            VREF_F1.bit5
#define VrefPress_Limit_Hold_flag                 VREF_F1.bit4
#define reserved_for_VrefLimitP3                  VREF_F1.bit3
#define reserved_for_VrefLimitP2                  VREF_F1.bit2
#define reserved_for_VrefLimitP1                  VREF_F1.bit1
#define reserved_for_VrefLimitP0                  VREF_F1.bit0
#endif


#define ALL_WHEEL_INCREDIBLE			VREF_F2.bit7
#define RELIABILITY_MAX_WL				VREF_F2.bit6
#define RELIABILITY_2ND_WL				VREF_F2.bit5
#define RELIABILITY_3RD_WL				VREF_F2.bit4
#define RELIABILITY_MIN_WL				VREF_F2.bit3
#define YAW_CORRECTIOIN_PERFORMED		VREF_F2.bit2
#define LowMuLimitFlag					VREF_F2.bit1
#define vfil_unused7					VREF_F2.bit0

#define ldu1VrefMaxupperlim             VREF_F3.bit7
#define ldu1VrefMaxupperlim_old         VREF_F3.bit6
#define reserved_for_Vref_impoved5      VREF_F3.bit5
#define reserved_for_Vref_impoved4      VREF_F3.bit4
#define reserved_for_Vref_impoved3      VREF_F3.bit3
#define reserved_for_Vref_impoved2      VREF_F3.bit2
#define reserved_for_Vref_impoved1      VREF_F3.bit1
#define reserved_for_Vref_impoved0      VREF_F3.bit0



/*Global Extern Variable  Declaration*****************************************/
#if __VREF_LIMIT_1
extern U8_BIT_STRUCT_t VREF_F0;
#endif

#if __VREF_LIMIT_2
extern U8_BIT_STRUCT_t VREF_F1;
#endif


extern U8_BIT_STRUCT_t VREF_F2;
extern U8_BIT_STRUCT_t VREF_F3;

    extern int8_t  	arad_select_wheel;
    extern int16_t 	vrad_diff_max;
  	extern int16_t 	vrad_diff_max_resol_change;
    extern int16_t 	vref_input_inc_limit;
    extern int16_t 	vref_input_dec_limit;   
    extern int16_t 	vref_output_inc_limit;
    extern int16_t 	vref_output_dec_limit;
    
   
    extern int16_t 	vref_limit_inc_counter;
    extern int16_t 	vref_limit_dec_counter_frt;        
    extern int16_t 	vref_limit_dec_counter_rr; 
   	extern int8_t 	lds8Outputdeclimit;
    extern int8_t 	lds8Outputinclimit;
    extern int8_t 	lds8Inputdeclimit;
   	extern int8_t 	lds8Inputinclimit;
	extern int16_t  lds16VrefRawSignal;

	#if	__VREF_LIMIT_1
	extern int16_t	lds16FiloutDecelG;
	#endif

	#if	__VREF_LIMIT_2
	extern int16_t	lds16VrefPressDecelG;     
    extern uint16_t lds16Abs1StVreflimCnt;     
	#endif 
      
    #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
    extern int16_t 	vref_inc_limit_by_g;
    extern int16_t 	vref_dec_limit_by_g;
    extern uint16_t lds16TcsVreflimCnt;
    #endif
	extern uint8_t vrsel_crt_flg;
 	extern int16_t wstr_correction;   
 	
       	
	extern int16_t wheel_speed_max;
	extern int16_t wheel_speed_2nd;
	extern int16_t wheel_speed_3rd;
	extern int16_t wheel_speed_min;	
	extern uint8_t wheel_speed_arrange_flg;
	extern uint8_t wheel_selection_mode;		
	

/*Global Extern Functions  Declaration****************************************/
	
	#if __4WD || (__4WD_VARIANT_CODE==ENABLE)
extern void     LDABS_vDetVrefLimitByLongAcc(int16_t inc_limit, int16_t dec_limit,int16_t fzcase);  
	#endif

extern void		LDABS_vCallVrefMainFilterprocess(void);
extern void    	LDABS_vDetVrefInputLimit(void);
extern void     LDABS_vExecuteVrefInputLimit(int16_t inc_limit, int16_t dec_limit,int16_t fzcase);

	#if __VDC
extern void 	LDABS_vArrangeWheelSpeed(int16_t whl_spd_fl,int16_t whl_spd_fr,int16_t whl_spd_rl,int16_t whl_spd_rr);
	#else
extern void 	LDABS_vArrangeWheelSpeed2(void);
	#endif
#endif
