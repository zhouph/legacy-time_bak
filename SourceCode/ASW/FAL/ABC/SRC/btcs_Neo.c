
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_btcs_Neo
	#include "Mdyn_autosar.h"               
#endif


#include "LCESPCalInterpolation.h"

#define __BTCS_NEO_STRUCTURE 0

/* BTCS PARAMETER */
#define     THRESHOLD_BTC       2               /* 0.25g resolution */

#define     XD_VIB_MAX          26
#define     XD_VIB_MIN          16
#define     SM_VIB_MAX          30
#define     SM_VIB_MIN          18

#if __BTCS_NEO_STRUCTURE

#if __BTC
extern 	void    BTCSET(void);
extern	void    EXITBTCS(void);
extern	void    BTCS_SPIN_CHECK(void);
#if __4WD
extern	void    BTCSTAGET(void);
#endif
extern	void    TCSTATE1(void);
extern	void    TCSTATE2(void);
/* extern	void    BTCTEMP(void);  */
#endif

#if __BTC
void	BTCS_Main(void);
void	BTCS_Control(struct W_STRUCT *WL_temp);
void	BTCS_Entry(void);
void	BTCS_Accel_Threshold_Detect_Clear(void);
void	BTCS_Accel_Threshold_Check(void);
void	BTCS_dV_Check(void);
void	BTCS_Entry_Inhibition_Check(void);
void	BTCS_DETECTION_MODE(void);
void	BTCS_DISC_TEMP_EST(void);
void	BTCS_REAPPLY_MODE(struct W_STRUCT *WL);
void	COUNT_WHEEL_CONTROL(struct W_STRUCT *pBTCwl, struct W_STRUCT *pCOUNTwl);
void	BTCS_SPEED_CALC_wl(void);
void	Wheel_Speed_Diff1(void);
void	Average_Driven_Wheel_Speed_update(void);
  #if	__4WD
void	Average_Driven_Wheel_Speed_4WD(void);
  #else
void	BTC_Average_Slip_wl(void);
void	TARGET_THR_MIN_Set(void);
  #endif
void	Wheel_Speed_Diff2(void);
void	BTC_VIB_SET_wl(void);
void	DET_VIB_BTC_wl(void);
void	BTC_HILL_DETECT_wl(void);
#endif
/* void	FULL_REAPPLY_MODE(struct W_STRUCT *WL);  */
void	Prohibit_ABScontrol_by_Engine_Drag(void);

void	BTCS_LOAD_PARAMETER(void);

/* ====================================================================== */

#if __BTC
void	BTCS_Main(void)
{
	/*
    int8_t BTCS_Entry_Inhibition;
    */
	BTCS_LOAD_PARAMETER();
	/*
	if(BTCS_INIT_OK==0)
	{
		BTCS_INIT_OK=1;
		BTCS_LOAD_PARAMETER();  
	}
	else if((system_loop_counter%142)==0) 
	{
		BTCS_LOAD_PARAMETER();  
	}
	else
	{
		;	
	}
	*/
	
  #if	__4WD	
	BTCSTAGET();
  #endif
  
  	BTCS_SPIN_CHECK();
  	
  	BTCS_Entry_Inhibition_Check();
  	
  #if __4WD
  	BTCS_Control(&FL);
  	BTCS_Control(&FR);
  	BTCS_Control(&RL);
  	BTCS_Control(&RR);
  #else
   #if __REAR_D
  	BTCS_Control(&RL);
  	BTCS_Control(&RR);
   #else
  	BTCS_Control(&FL);
  	BTCS_Control(&FR);
   #endif
  #endif

  #if __4WD
	if(BTCS_fl==1) 
	{
		if(BTCS_fr==1) 
		{
			if(BTCS_rl==0) 
			{
				COUNT_WHEEL_CONTROL(&FR, &RL);
			}
			else
			{
				;
			}		
		}
		else if(BTCS_rl==1) 
		{
			COUNT_WHEEL_CONTROL(&RL, &FR);
		}	
		else 
		{
			FR.flags=RL.flags=43;
			BTCS_REAPPLY_MODE(&FR);
			BTCS_REAPPLY_MODE(&RL);
		}
		
		if(BTCS_rr==0) 
		{
			COUNT_WHEEL_CONTROL(&FL, &RR);
		}
		else
		{
			;
		}		
	}
	else if(BTCS_fr==1) 
	{
		if(BTCS_rl==0) 
		{
			COUNT_WHEEL_CONTROL(&FR, &RL);
		}
		else
		{
			;
		}	
			
		if(BTCS_rr==1) 
		{
			COUNT_WHEEL_CONTROL(&RR, &FL);
		}	
		else 
		{
			FL.flags=RR.flags=43;
			BTCS_REAPPLY_MODE(&FL);
			BTCS_REAPPLY_MODE(&RR);
		}
	}
	else 
	{
		if(BTCS_rl==1) 
		{
			COUNT_WHEEL_CONTROL(&RL, &FR);
			if(BTCS_rr==1) 
			{
				COUNT_WHEEL_CONTROL(&RR, &FL);
			}	
			else 
			{
				FL.flags=RR.flags=43;
				BTCS_REAPPLY_MODE(&FL);
				BTCS_REAPPLY_MODE(&RR);
			}
		}
		else if(BTCS_rr==1) 
		{
			COUNT_WHEEL_CONTROL(&RR, &FL);
			FR.flags=RL.flags=43;
			BTCS_REAPPLY_MODE(&FR);
			BTCS_REAPPLY_MODE(&RL);
		}
		else 
		{
			BTC_fz=0;
		}
	}
  #else
   #if __REAR_D
	 if(BTCS_rl==1) 
	 {
	 	COUNT_WHEEL_CONTROL(&RL, &FR);
	 	if(BTCS_rr==1) 
	 	{
	 		COUNT_WHEEL_CONTROL(&RR, &FL);
	 	}	
	 	else 
	 	{
	 		FL.flags=RR.flags=43;
	 		BTCS_REAPPLY_MODE(&FL);
	 		BTCS_REAPPLY_MODE(&RR);
	 	}
	}
	else if(BTCS_rr==1) 
	{
		COUNT_WHEEL_CONTROL(&RR, &FL);
		FR.flags=RL.flags=43;
		BTCS_REAPPLY_MODE(&FR);
	 	BTCS_REAPPLY_MODE(&RL);
	}
	else 
	{
		BTC_fz=0;
	}
   #else
	if(BTCS_fl==1) 
	{
	 	COUNT_WHEEL_CONTROL(&FL, &RR);
	 	if(BTCS_fr==1) 
	 	{
	 		COUNT_WHEEL_CONTROL(&FR, &RL);
	 	}
	 	else 
	 	{
	 		FR.flags=RL.flags=43;
	 		BTCS_REAPPLY_MODE(&FR);
	 		BTCS_REAPPLY_MODE(&RL);
	 	}
	}
	else if(BTCS_fr==1) 
	{
		COUNT_WHEEL_CONTROL(&FR, &RL);
		FL.flags=RR.flags=43;
		BTCS_REAPPLY_MODE(&FL);
	 	BTCS_REAPPLY_MODE(&RR);
	}
	else 
	{
		BTC_fz=0;
	}
   #endif
  #endif
}


void	BTCS_Control(struct W_STRUCT *WL_temp)
{
	WL = WL_temp;

	if((BTCS_wl == 0)&&(BTCS_Entry_Inhibition==0)) 
	{
		BTCS_Entry();
	}
	else 
	{
		DETECT_BTC_wl=0; 	
	}	
	
	if(BTCS_wl == 1) 
	{
		EXITBTCS();
		if(BTCS_wl==1) 
		{
			BTCS_SPEED_CALC_wl();
		  #if	!__4WD	
			BTC_Average_Slip_wl();
		  #endif	
			BTC_HILL_DETECT_wl();
			if(WL->state == BTCS_STATE1) 
			{
				WL->flags=41;
				TCSTATE1();
			}
			else 
			{
				WL->flags=42;

				#if SIM_MATLAB && !__4WD
				TARGET_THR_MIN_Set();
				#endif
								
				TCSTATE2();
			}
		}
		else 
		{
			DETECT_BTC_wl=0; 	
		}
     /*	else goto ABS_DETECTION;  */
	}
	else
	{
		;
	}	      
	
}



void	BTCS_Entry(void)
{
#if !__TCS	
  #if __4WD
	if(((LEFT_WHEEL_wl==1)&&(BTC_RIGHT==1)) || ((LEFT_WHEEL_wl==0)&&(BTC_LEFT==1)))
  #else
	if(BTC_fz==1) 
  #endif
	{
		DETECT_BTC_wl=0;
     /*	FULL_REAPPLY_MODE(); */
	} 
	else 
	{
#endif  
		if(DETECT_BTC_wl==0) 
		{
			BTCS_Accel_Threshold_Check();
		}
		else 
		{
			BTCS_dV_Check();
		}
		
	  #if	!__4WD	
		if(BTCS_wl==0) 
		{
			Prohibit_ABScontrol_by_Engine_Drag();
		}
		else
		{
			;
		}		
	  #endif
#if !__TCS	
	}
#endif  
}



void	BTCS_Entry_Inhibition_Check(void)
{
	BTCS_Entry_Inhibition = 0;
#if !SIM_MATLAB	
  #if (__TCS && __4WD)		/*jt_031015*/
    if (mtp < BTCS_INH_MTP) 
    {
    	BTCS_Entry_Inhibition=1;
    }
    else
    {
    	;
    }		
  #endif
#endif
    if(((!tc_on_flg)||(tc_error_flg)||(ABS_fz)) 
      || ((tcs_error_flg)||(!under_v_flg)))	  /*	Brake two wheel on ice by Kim Jeonghun in 2004.2.24 */
    {
        BTCS_Entry_Inhibition=1;
    }
    else
    {
    	;
    }	

  #if __CAR==HMC_SM_SIGMA
	tempW0 = VREF_10_KPH;
  #elif	__BTCS_TWO_ON_HOMO_MU				/*	Brake two wheel on ice by Kim Jeonghun in 2004.2.24 */
	tempW0 = VREF_10_KPH;
  #else
	tempW0 = VREF_2_KPH;
  #endif
    if((BTCS_TWO_OFF)&&(vref2>tempW0)) 
    {
        BTCS_Entry_Inhibition=1;
    }
    else
    {
    	;
    }	

#if __VDC
	if(!ESP_ERROR_FLG) 
	{
    	if(MPRESS_BRAKE_ON) 
    	{
    		DETECT_BTC_wl=0;
    	}
    	else
    	{
    		;
    	}
    		
    	if((ESP_ON)&&(esp_on_counter_1<142)) 
    	{
    		BTCS_Entry_Inhibition=1;		/* ����#4 */
    	}
    	else
    	{
    		;
    	}		
	  #if (__CAR==HMC_JM || __CAR==KMC_KM) && __4WD
		if(ITM_4WD_LOCK) 
		{
			BTCS_Entry_Inhibition=1;		/* ITM 4WD LOCK MODE -> BTCS INHIBITION 040323 Baek Jongtak */
		}
    	else
    	{
    		;
    	}		
	  #endif
	}
	else 
	{
    	if((!BLS_ERROR)&&(!BLS_EEPROM)) 
    	{
        	if(BLS) 
        	{
        		BTCS_Entry_Inhibition=1;
        	}
        	else
        	{
        		;
        	}		
    	}
    	else
    	{
    		;
    	}	
  	}
#else
    if((!BLS_ERROR)&&(!BLS_EEPROM)) 
    {
        if(BLS) 
        {
        	BTCS_Entry_Inhibition=1;
        }
        else
        {
        	;
        }		
    }
    else
    {
    	;
    }	
#endif

#if __TCS
    if(vref2>(int16_t)(BTCS_INH_VREF_MAX*8)) 
    {
    	BTCS_Entry_Inhibition=1;         /* GK29 */
    }
    else
    {
    	;
    }		
#else
    if(vref2>(int16_t)(BTCS_INH_VREF_BTCS*8)) 
    {
    	BTCS_Entry_Inhibition=1;
    }
    else
    {
    	;
    }    	
#endif

}




void	BTCS_Accel_Threshold_Check(void)
{
	if(WL->arad>=(uint8_t)BTCS_ENT_THR_ARAD) 
	{
		if(WL->rel_lam>0) 
		{
  #if __VDC
			WL->b_rad=WL->vrad-WL->wvref;
  #else
			WL->b_rad=WL->vrad-vref2;
  #endif
			WL->fspeed_alt=WL->fspeed;
			WL->fspeed=WL->vrad;
			DETECT_BTC_wl=1;
    	}
    	else
    	{
    		;
    	}		
    }
    else
    {
    	;
    }	
}





void	BTCS_dV_Check(void)
{
  #if __BTCS_CORNER_CONTROL
    #if __VDC
    	tempW6=LCESP_s16IInter2Point(vref5, VREF_0_KPH, VREF_6_KPH, VREF_30_KPH, VREF_0_KPH);
    #else
    	tempW6=LCESP_s16IInter2Point(vref, VREF_0_KPH, VREF_6_KPH, VREF_30_KPH, VREF_0_KPH);
    #endif
  #endif
  
  #if __VDC
	tempW0=WL->vrad-WL->wvref;
  #elif	__4WD
	tempW0=WL->vrad-vref2;
  #else
	tempW0=WL->vrad-vref;
  #endif
  
	if(tempW0>(int16_t)VREF_MIN) 
	{
		
		tempW1=tempW0;		/* delta V after detect +b */
		if(tempW0>WL->b_rad) 
		{
			tempW1-=WL->b_rad;
		}
		else
		{
			;
		}		
		
		tempW4=(int16_t)BTCS_ENT_THR_DV_1ST;              /* VREF_10_KPH */
		tempW5=(int16_t)BTCS_ENT_THR_DV_2ND;              /* VREF_15_KPH */
		
	  #if !__VDC
		if((BLS_ERROR)||(BLS_EEPROM)) 
		{
			tempW4=(int16_t)BTCS_ENT_THR_DV_2ND;
		}
		else
		{
			;
		}		
	  #endif

    	tempW4 = tempW4 + tempW6;
    	tempW5 = tempW5 + tempW6;
/*	  	
	  	tempW0=VREF_1_KPH;
	  	tempW1=VREF_1_KPH;	  	
	  	tempW4=VREF_10_KPH;
	  	tempW5=VREF_15_KPH;
*/
			  	
		if((tempW1>tempW4)||(tempW0>tempW5)) 
		{	  
			BTCS_ENT_THR_DV_1ST=tempW0;
			BTCS_ENT_THR_DV_2ND=tempW1;
		
			WL->fspeed_alt=WL->fspeed;
			WL->fspeed=WL->vrad;
			WL->ffspeed=WL->vrad;
			WL->av_fspeed_alt=WL->av_fspeed;
			WL->av_fspeed=(uint16_t)(WL->fspeed+WL->fspeed_alt)>>1;
		  #if	__4WD
			if(LEFT_WHEEL_wl==1) 
			{
				BTC_LEFT=1;
				if(REAR_WHEEL_wl==1) 
				{
					avr_fspeed=WL->av_fspeed+av_fspeed_fl;
				}
				else 
				{
					avr_fspeed=WL->av_fspeed+av_fspeed_rl;
				}
			}
			else 
			{
				BTC_RIGHT=1;
				if(REAR_WHEEL_wl==1) 
				{
					avr_fspeed=WL->av_fspeed+av_fspeed_fr;
				}
				else 
				{
					avr_fspeed=WL->av_fspeed+av_fspeed_rr;
				}
			}
			avr_fspeed=(uint16_t)avr_fspeed>>1;
		  #endif
			WL->btc_speed_alt=WL->btc_speed;
			WL->btc_speed=WL->av_fspeed-WL->av_fspeed_alt;
			POS_COUNT_wl=0;
			DETECT_BTC_wl=0;
			ARAD_POSITIVE_wl=1;
			ARAD_RISE_wl=1;
			WL->flags=40;
			BTCS_DETECTION_MODE();
		}
		else
		{
			;
		}	
	}
	else
	{
		;
	}
		
	if(WL->arad<=(int8_t)ARAD_0G0) 
	{
		WL->fspeed=(int16_t)VREF_MIN;
		WL->ffspeed=(int16_t)VREF_MIN;
		DETECT_BTC_wl=0;
	}
	else
	{
		;
	}
	
  #if	__4WD  
/*    if((BTC_fz==1) && (BTCS_wl==0)) FULL_REAPPLY_MODE();  */
  #endif

/*    goto ABS_CHECK;    */
}

void	Prohibit_ABScontrol_by_Engine_Drag(void)
{
    if(BTC_LOW_SLIP_wl == 1) 
    {
        if(BLS_K == 1) 
        {
        	BTC_LOW_SLIP_wl = 0;
        }
        else
        {
        	;
        }		
      #if __REAR_D
        if(((arad_fl <= thres_decel_fl) || (arad_fr <= thres_decel_fr)) 
        	|| ((LEFT_WHEEL_wl == 1) && (arad_rr <= thres_decel_rr))
        	|| ((LEFT_WHEEL_wl == 0) && (arad_rl <= thres_decel_rl)))
      #else
        if(((arad_rl <= thres_decel_rl) || (arad_rr <= thres_decel_rr)) 
        	|| ((LEFT_WHEEL_wl == 1) && (arad_fr <= thres_decel_fr))
        	|| ((LEFT_WHEEL_wl == 0) && (arad_fl <= thres_decel_fl)))
      #endif
        {
            BTC_LOW_SLIP_wl = 0;
        }
        else
        {
        	;
        }	
        WL->hold_count ++;
        if(WL->hold_count > 1500) 
        {
        	BTC_LOW_SLIP_wl = 0;
        }
        else
        {
        	;
        }		
/*        if(BTC_LOW_SLIP_wl == 1) FULL_REAPPLY_MODE();   */
    }
    else
    {
    	;
    }	
}	

void	BTCS_DETECTION_MODE(void)
{
  #if __4WD

    BTCS_wl = 1;
    BTC_fz=1;	/* added by KGY*/
    TC_MOT_ON = 1;
    TCL_DEMAND_wl = 1;
    AV_REAPPLY_wl = 1;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    WL->state = BTCS_STATE1;
    if(REAR_WHEEL_wl == 0) 
    {
        TCL_INCF_wl = 0;
        TCL_INC_wl = 1;
    }
    else 
    {
        TCL_INC_wl = 0;
        TCL_INCF_wl = 1;
    }
    COUNT20_wl = 0;
    POS_HOLD_wl = 0;
    LOW_TO_HIGH1_wl = 0;
    FIRST_INC_wl = 0;
    DETVIB_BTC_wl = 0;
    XMT_VIB_wl = 0;
    CHANGE_SLIP = 0;
    HILL_wl = 0;
    WL->hold_timer = 0;
    if(REAR_WHEEL_wl == 1) 
    {
    	WL->hold_timer = 1;          /* MAIN04E*/
    }
    else
    {
    	;
    }		
    WL->average7 = 0;
    WL->count7 = 0;
    WL->btcdec_count = 0;
    if(REAR_WHEEL_wl == 0) 
    {
    	WL->btcinc_count = 2;
    }	
    else 
    {
    	WL->btcinc_count = 0;
    }	
    WL->btc_count1 = 0;
    WL->btc_count2 = 0;
    WL->btc_count3 = 0;
    WL->btc_count4 = 0;
    WL->vib_count1 = 0;
    WL->vib_count2 = 0;
    WL->slip_count1 = 0;
    WL->slip_count2 = 0;
    WL->slip_count3 = 0;
    WL->exit_timer1 = 0;
    WL->exit_timer2 = 0;
    WL->exit_timer3 = 0;
    WL->exit_timer4 = 0;
	WL->exit_timer5 = 0;
    WL->barad_max = 0;
    WL->barad_alt = 0;
    av_ref_max_fl = av_ref_max_fr = av_ref_max_rl = av_ref_max_rr = 0;
    WL->tmp_state = 1;
    WL->inc_count3 = 0;

  #else

    BTCS_wl = 1;
    BTC_fz=1;	/* added by KGY*/
    TC_MOT_ON = 1;
    TCL_DEMAND_wl = 1;
    AV_REAPPLY_wl = 1;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    WL->state = BTCS_STATE1;
    TCL_INC_wl = 0;
    TCL_INCF_wl = 1;
    COUNT20_wl = 0;
    POS_HOLD_wl = 0;
    LOW_TO_HIGH1_wl = 0;
    OPT_SET_HIGH_wl = 0;
    OPT_NEWSET_wl = 0;
    FIRST_INC_wl = 0;
    HILL_wl = 0;
    VIB_SET_wl = 0;
    DETVIB_BTC_wl = 0;
    XMT_VIB_wl = 0;
    CHANGE_SLIP = 0;
    BTC_LOW_SLIP_wl = 0;
    WL->b_rad = 0;
  #if __VDC
    WL->btc_slip = (int16_t)(WL->vrad - WL->wvref);
  #else
    WL->btc_slip = (int16_t)(WL->vrad - vref);
  #endif
    WL->slip_min = (int16_t)VREF_10_KPH;
    WL->hold_timer = 0;
    WL->average7 = 0;
    WL->count7 = 0;
    WL->count14 = 0;
    WL->hold_count = 0;
    WL->btcinc_count = 0;
    WL->btcdec_count = 0;
    WL->btc_count1 = 0;
    WL->btc_count2 = 0;
    WL->vib_count1 = 0;
    WL->vib_count2 = 0;
    WL->vib_count3 = 0;
    WL->vib_count4 = 0;
    WL->slip_count1 = 0;
    WL->slip_count2 = 0;
    WL->slip_count3 = 0;
    WL->exit_timer3 = 0;
    WL->exit_timer2 = 0;
    WL->exit_timer1 = 0;
    WL->av_btc_slip = 0;
    WL->av_btc_slip_alt = 0;
    WL->refdiff = 0;
    WL->refdiff_alt1 = 0;
    WL->refdiff_alt2 = 0;
    WL->av_ref = 0;
    WL->av_ref_alt = 0;
    WL->tmp_state = 1;
    WL->inc_count3 = 0;
   #if __REAR_D
    av_ref_max_rl=av_ref_max_rr=0;  
   #else
    av_ref_max_fl=av_ref_max_fr=0;
   #endif
  #endif
  
	if(U16_BTCS_LFC_RISE_CONTROL==1)
	{
		WL->BTCS_built_reapply_pattern=0;	
	}
	else
	{
		;
	}	
}	

void	BTCS_DISC_TEMP_EST(void)
{
  #if !__REAR_D
    BTCTEMP(&FL);
    BTCTEMP(&FR);
  #endif
  
  #if __4WD || __REAR_D
    BTCTEMP(&RL);
    BTCTEMP(&RR);
  #endif
}

void	BTCS_REAPPLY_MODE(struct W_STRUCT *WL)
{
	WL->AV_REAPPLY=1;
	WL->AV_HOLD=0;
	WL->BUILT=0;
	WL->AV_DUMP=0;
	
	if(U16_BTCS_LFC_RISE_CONTROL==1)
	{
		WL->BTCS_built_reapply_pattern=3;	
	}
	else
	{
		;
	}	

}
	
void	COUNT_WHEEL_CONTROL(struct W_STRUCT *pBTCwl, struct W_STRUCT *pCOUNTwl)
{
/* Transplantation from ABDMOTOR()  */
  #if	__4WD || __REAR_D
    pCOUNTwl->AV_DUMP = 1;
    pCOUNTwl->AV_REAPPLY = 0;
    pCOUNTwl->AV_HOLD = 0;
    pCOUNTwl->BUILT = 0;
  #else
  	if(pBTCwl->valve_delay_count>7) 
  	{		/* Noise */
	    pCOUNTwl->AV_REAPPLY = 1;
	    pCOUNTwl->AV_DUMP = 0;
	    pCOUNTwl->AV_HOLD = 0;
	    pCOUNTwl->BUILT = 0;
  	}
  	else if(pBTCwl->valve_delay_count>=2) 
  	{
	    pCOUNTwl->AV_DUMP = 1;
	    pCOUNTwl->AV_REAPPLY = 0;
	    pCOUNTwl->AV_HOLD = 0;
	    pCOUNTwl->BUILT = 0;
  	}
  	else
  	{
  		;
  	}	
  #endif
  	pCOUNTwl->flags=44;  
}

void	BTCS_SPEED_CALC_wl(void)
{	
	Wheel_Speed_Diff1();
	
  #if	__4WD
    tempF1 = 0;
  #endif
  
  	Wheel_Speed_Diff2();
  	Average_Driven_Wheel_Speed_update();
}

void	BTC_VIB_SET_wl(void)
{
  #if	__4WD	
    if(VIB_COUNT_wl == 0) 
    {
    	WL->btc_count3 = 0;
    }	
    else 
    {
        if(WL->btc_count3 >= 30) 
        {
        	DETVIB_BTC_wl = 0;
        }
        else
        {
        	;
        }		
        
        if(WL->btc_count3 > 100) 
        {
        	WL->btc_count3 = 100;
        }	
        else 
        {
        	WL->btc_count3 ++;
        }	
    }
  #endif
  	
    if(ARAD_POSITIVE_wl == 1) 
    {
        WL->vib_count2 ++;
        if(VIB_SET_wl == 0) 
        {
        	WL->vib_count1 ++;
        }
        else
        {
        	;
        }
        		
        if(WL->arad < WL->arad_alt) 
        {
            if(VIB_SET_wl == 1) 
            {
                VIB_SET_wl = 0;
                WL->vib_count3 = WL->vib_count1 + WL->vib_count2;
                WL->vib_count1 = 0;
                DET_VIB_BTC_wl();
            }
            else
            {
            	;
            }	
        }
        else
        {
        	;
        }	
    }
    else 
    {
        VIB_SET_wl = 1;
        WL->vib_count1 ++;
        WL->vib_count2 = 0;
        WL->vib_count3 = 0;
    }	       
}

void	DET_VIB_BTC_wl(void)
{
	DETVIB_BTC_wl = 0;

  #if	__4WD  
    if(WL->vib_count3 >= (uint8_t)SM_VIB_MIN) 
    {
        if(WL->vib_count3 <= (uint8_t)SM_VIB_MAX) 
        {
            DETVIB_BTC_wl = 1;
        }
        else
        {
        	;
        }	
    }
    else
    {
    	;
    }
    	
    if(DETVIB_BTC_wl == 0) 
    {
        VIB_COUNT_wl = 1;
        if(LEFT_WHEEL_wl == 1) 
        {
            if((DETVIB_BTC_fl==1)||(DETVIB_BTC_rl==1)) 
            {
                if(WL->btc_count3 > 30) 
                {
                	DETVIB_BTC_wl = 0;
                }	
                else 
                {
                	DETVIB_BTC_wl = 1;
                }	
            }
            else
            {
            	;
            }	
        }
        else 
        {
            if((DETVIB_BTC_fr==1)||(DETVIB_BTC_rr==1)) 
            {
                if(WL->btc_count3 > 30) 
                {
                	DETVIB_BTC_wl = 0;
                }	
                else 
                {
                	DETVIB_BTC_wl = 1;
                }	
            }
            else
            {
            	;
            }	
        }
    }
    else 
    {
    	VIB_COUNT_wl = 0;    
    }	
  #else  
  	if(WL->state==BTCS_STATE1) 
  	{
  		tempC1=3;
  	}
  	else if(WL->state==BTCS_STATE2) 
  	{
  		tempC1=4;
  	}
  	else
  	{
  		;
  	}		
  	
    if(WL->vib_count3 > (uint8_t)XD_VIB_MIN) 
    {
        if(WL->vib_count3 < (uint8_t)XD_VIB_MAX) 
        {
            if(WL->arad_alt > (int8_t)ARAD_3G0) 
            {
            	DETVIB_BTC_wl = 1;
            }
            else
            {
            	;
            }
            		
            if(WL->arad_alt > (int8_t)ARAD_5G0) 
            {
                WL->vib_count4 ++;
                if(WL->vib_count4 >= tempC1) 
                {
                    XMT_VIB_wl = 1;
                    if(WL->state==BTCS_STATE1) 
                    {
                    	WL->b_rad = (int16_t)VREF_12_KPH;
                    }
                    else
                    {
                    	;
                    }		
                }
                else
                {
                	;
                }	
            }
            else
            {
            	;
            }	
        }
        else
        {
        	;
        }	
    }
    else
    {
    	;
    }	
  #endif	
}

void	Wheel_Speed_Diff1(void)
{
	/*=====================================================================*/
    /*	offset setting for M/T by Kim Jeonghun in 2004.4.22                */
    /*---------------------------------------------------------------------*/
	tempF0 = 0;
    tempW1 = WL->vdiff;

    if(WL->count7 == 0) 
    {
        WL->average7 = 0;
    }
    else
    {
    	;
    }
    	
    if(WL->count7 <= 5) 
    {
        WL->average7 += tempW1;
        WL->count7++;
    }
    else 
    {
        WL->average7 += tempW1;
        WL->count7 = 0;
        WL->ffspeed = WL->vrad;
        tempF0 = 1;
    }
}

void	Average_Driven_Wheel_Speed_update(void)
{
    tempF2 = 0;
    AVR_DRV_WHEEL_SPEED_Update=0;
    
    if(ARAD_POSITIVE_wl == 1) 
    {
	  #if	__4WD 
        if(WL->state == BTCS_STATE2) 
        {
        	if(WL->arad > WL->barad_max) 
        	{
        		WL->barad_max = WL->arad;
        	}
        	else
        	{
        		;
        	}		
        }
        else
        {
        	;
        }	
	  #endif
        if(WL->arad < WL->arad_alt) 
        {
            if(ARAD_RISE_wl == 1) 
            {
                ARAD_RISE_wl = 0;
                WL->fspeed_alt = WL->fspeed;
                WL->fspeed = WL->vrad;
                tempF2 = 1;
            }
            else 
            {
                if(WL->arad < (int8_t)ARAD_0G0) 
                {
                	ARAD_POSITIVE_wl = 0;
                  #if	__4WD	
                    if(WL->state == BTCS_STATE2) 
                    {
                    	WL->barad_alt = WL->barad_max;
                    	WL->barad_max = 0;
                    }
                    else
                    {
                    	;
                    }	
                  #endif
				}
				else
				{
					;
				}	                	
            }
        }
        else
        {
        	;
        }	
    }
    else 
    {
      #if	__4WD
      	tempB2 = (int8_t)ARAD_0G0;
      #else
        if((WL->state==BTCS_STATE2) && (DETVIB_BTC_wl==1)) 
        {
        	tempB2 = (int8_t)ARAD_0G25;
        }	
        else 
        {
        	tempB2 = (int8_t)ARAD_0G0;
        }	
      #endif
    	
        if(WL->arad > WL->arad_alt + tempB2) 
        {
            if(ARAD_RISE_wl == 0) 
            {
                ARAD_RISE_wl = 1;
                WL->fspeed_alt = WL->fspeed;
                WL->fspeed = WL->vrad;
                tempF2 = 1;
            }
            else 
            {
                if(WL->arad > (int8_t)ARAD_0G0) 
                {
                	ARAD_POSITIVE_wl = 1;
                }
                else
                {
                	;
                }		
            }
        }
        else
        {
        	;
        }	
    }
    
  #if	!__4WD
    if(tempF2 == 1) 
    {
        if(WL->count20 < 4) 
        {
        	tempF2 = 0;
        }
        else
        {
        	;
        }		
    }
    else
    {
    	;
    }	
  #endif	
    if(tempF2 == 0) 
    {
    	WL->count20 ++;
    }	
    else 
    {
    	WL->count20 = 0;
    }	

    if(WL->count20 >= 18) 
    {
        WL->fspeed_alt = WL->fspeed;
        WL->fspeed = WL->vrad;
        WL->count20 = 0;
        tempF2 = 1;
    }
    else
    {
    	;
    }
    	
    if(tempF2 == 1) 
    {
        WL->av_fspeed_alt = WL->av_fspeed;
        WL->av_fspeed = (uint16_t)(WL->fspeed + WL->fspeed_alt)>>1;
        WL->btc_speed_alt = WL->btc_speed;
        WL->btc_speed = WL->av_fspeed - WL->av_fspeed_alt;
        if(WL->state == BTCS_STATE2) 
        {
	        if(COUNT20_wl == 0) 
	        {
	            if(WL->btc_speed < (int16_t)VREF_1_5_KPH) 
	            {
	            	COUNT20_wl = 1;
	            }
	            else
	            {
	            	;
	            }		
	        }
	        else
	        {
	        	;
	        }	
        }
        else
        {
        	;
        }	
      #if	__4WD
      	Average_Driven_Wheel_Speed_4WD();
      #endif
      	AVR_DRV_WHEEL_SPEED_Update=1;
    }
    else
    {
    	;
    }	
}	

void	Wheel_Speed_Diff2(void)
{
	tempF1 = 0;
    tempB3 = (int8_t)(vref2 - vref_alt);
	
    if(WL->count14 == 0) 
    {
        WL->refdiff_alt2 = WL->refdiff_alt1;
        WL->refdiff_alt1 = WL->refdiff;
        WL->refdiff = 0;
    }
    else
    {
    	;
    }
    	
    if(WL->count14 <= 12) 
    {
        WL->refdiff += tempB3;
        WL->count14++;
    }
    else 
    {
        WL->refdiff += tempB3;
        WL->av_ref_alt = WL->av_ref;
        WL->av_ref = WL->refdiff + WL->refdiff_alt1 + WL->refdiff_alt2;
        WL->count14 = 0;
        tempF1 = 1;
    }
}
  #if	__4WD
void	Average_Driven_Wheel_Speed_4WD(void)
{
	avr_fspeed_alt = avr_fspeed;
	if(LEFT_WHEEL_wl == 1) 
	{
	    if(REAR_WHEEL_wl == 1) 
	    {
	        avr_fspeed = WL->av_fspeed + av_fspeed_fl;
	    }
	    else 
	    {
	        avr_fspeed = WL->av_fspeed + av_fspeed_rl;
	    }
	}
	else 
	{
	    if(REAR_WHEEL_wl == 1) 
	    {
	        avr_fspeed = WL->av_fspeed + av_fspeed_fr;
	    }
	    else 
	    {
	        avr_fspeed = WL->av_fspeed + av_fspeed_rr;
	    }
	}
	avr_fspeed = (uint16_t)avr_fspeed>>1;
	VREF_4WD = 1;
}
  #endif
  
void	BTC_HILL_DETECT_wl(void)
{

    if(WL->inc_count3 > T_900_MS) 
    {
		/*===========================================================================*/
		/*	Prevent hill detection on ice by Kim Jeonghun in 2004.3.2				 */
		/*---------------------------------------------------------------------------*/
	#if __4WD
		#if __TCS
	        if(WL->av_ref < ACCEL_X_0G05G && vref<VREF_15_KPH && acc_r<ACCEL_X_0G05G) 
	        {
	        	HILL_wl = 1;
	        }
	        else
	        {
	        	;
	        }		
		#else
	        if(WL->av_ref < ACCEL_X_0G05G && vref<VREF_15_KPH) 
	        {
	        	HILL_wl = 1;		
	        }
	        else
	        {
	        	;
	        }		
		#endif   
	#else
		#if __TCS
	        if(WL->av_ref < ACCEL_X_0G05G && vref<VREF_15_KPH && acc_r<ACCEL_X_0G05G) 
	        {
	        	HILL_wl = 1;
	        }
	        else
	        {
	        	;
	        }		
		#else
	        if(WL->av_ref < ACCEL_X_0G05G && vref<VREF_15_KPH) 
	        {
	        	HILL_wl = 1;		
	        }
	        else
	        {
	        	;
	        }		
		#endif		
		
	#endif     
		/*===========================================================================*/
		/*	Brake two wheel on ice by Kim Jeonghun in 2004.2.24						 */
		/*---------------------------------------------------------------------------*/
		#if	__BTCS_TWO_ON_HOMO_MU    
			if(WL->inc_count3 <= T_1750_MS)
			{
				WL->inc_count3++;
			}
			else
			{
				;
			}		
		#endif        
    }
    else 
    {
    	WL->inc_count3 ++;
    }	
	
}

  #if	!__4WD
void	BTC_Average_Slip_wl(void)
{
  #if __VDC
	tempW3 = (int16_t)(WL->av_fspeed - WL->wvref);
  #else
	tempW3 = (int16_t)(WL->av_fspeed - vref);
  #endif
	
	if(WL->state==BTCS_STATE1) 
	{
	    if(POS_HOLD_wl == 1) 
	    {
	        if(AVR_DRV_WHEEL_SPEED_Update == 1) 
	        {
	            if(OPT_NEWSET_wl == 1) 
	            {
	                WL->av_btc_slip += tempW3;
	                WL->slip_count3 ++;
	            }
	            else
	            {
	            	;
	            }	
	        }
	        else
	        {
	        	;
	        }
	        	
	        if(WL->count14 == 0) 
	        {
	            if(WL->av_ref > WL->av_ref_alt) 
	            {
	                FIRST_INC_wl = 1;
	                OPT_NEWSET_wl = 1;
	                WL->av_btc_slip = (int16_t)VREF_0_KPH;
	                WL->av_btc_slip += tempW3;
	                WL->slip_count3 = 0;
	                WL->slip_count3 ++;
	            }
	            else if(WL->av_ref < WL->av_ref_alt) 
	            {
	                FIRST_INC_wl = 0;
	                OPT_NEWSET_wl = 0;
	                WL->av_btc_slip = (int16_t)VREF_0_KPH;
	                WL->slip_count3 = 0;
	            }
	            else
	            {
	            	;
	            }	
	        }
	        else
	        {
	        	;
	        }	
	    }
	    else
	    {
	    	;
	    }	
	}
	else if(WL->state==BTCS_STATE2) 
	{
	  #if __REAR_D
	    if((BTCS_rl == 1) && (BTCS_rr == 1))
	  #else
	    if((BTCS_fl == 1) && (BTCS_fr == 1))
	  #endif
		{
	        FIRST_INC_wl = 0;
	        OPT_NEWSET_wl = 0;
	        OPT_SET_HIGH_wl = 0;
	        WL->slip_count1 = 0;
	        WL->slip_count2 = 0;
	        WL->slip_count3 = 0;
	        WL->av_btc_slip = (int16_t)VREF_0_KPH;
	        WL->av_btc_slip_alt = (int16_t)VREF_0_KPH;
	    }
	    else 
	    {
		    if(AVR_DRV_WHEEL_SPEED_Update == 1) 
		    {
		        if(OPT_NEWSET_wl == 1) 
		        {
		            if(tempW3 > WL->btc_slip) 
		            {
		            	WL->av_btc_slip += WL->btc_slip;
		            }		            
		            else 
		            {
		            	WL->av_btc_slip += tempW3;
		            }	
		            WL->slip_count3 ++;
		        }
		        else if(OPT_SET_HIGH_wl == 1) 
		        {
		            if(tempW3 < WL->av_btc_slip_alt) 
		            {
		                WL->av_btc_slip += tempW3;
		                WL->slip_count1 ++;
		            }
		            else
		            {
		            	;
		            }	
		        }
		        else
		        {
		        	;
		        }	
		    }
		    else
		    {
		    	;
		    }
		    	
		    if(WL->count14 == 0) 
		    {
		    	if(WL->av_ref < WL->av_ref_alt) 
		 		{
		 			TARGET_THR_MIN_Set();
		 		}
		 		else
		 		{
		 			;
		 		}		
		    	
		        if(tempW3 < WL->btc_slip) 
		        {
		            if(WL->av_ref > WL->av_ref_alt) 
		            {
		            	FIRST_INC_wl = 1;
		            }
		            else
		            {
		            	;
		            }		
		        }
		        else
		        {
		        	;
		        }
		        	
		        if(FIRST_INC_wl == 0) 
		        {
		            if(tempW3 > WL->btc_slip) 
		            {
		                OPT_NEWSET_wl = 0;
		                WL->slip_count3 = 0;
		                if(WL->av_ref > WL->av_ref_alt) 
		                {
		                    if(WL->slip_count2 >= 3) 
		                    {
		                    	OPT_SET_HIGH_wl = 1;
		                    }	
		                    else 
		                    {
		                    	WL->slip_count2 ++;
		                    }	
		                }
		                else 
		                {
		                	WL->slip_count2 = 0;
		                }	
		
		                if(OPT_SET_HIGH_wl == 1) 
		                {
		                    if(WL->av_ref > WL->av_ref_alt) 
		                    {
		                        WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                        WL->av_btc_slip_alt = tempW3;
		                        WL->av_btc_slip += tempW3;
		                        WL->slip_count1 = 0;
		                        WL->slip_count1 ++;
		                    }
		                    else if(WL->av_ref < WL->av_ref_alt) 
		                    {
		                        WL->btc_slip = (WL->av_btc_slip / (int8_t)WL->slip_count1);
		                        if(WL->btc_slip > WL->slip_min + (int16_t)VREF_5_KPH) 
		                        {
		                            WL->btc_slip = WL->slip_min + (int16_t)VREF_5_KPH;
		                        }
		                        else if(WL->btc_slip < WL->slip_min) 
		                        {
		                            WL->btc_slip = WL->slip_min;
		                            WL->slip_min -= (int16_t)VREF_2_KPH;
		                        }
		                        else
		                        {
		                        	;
		                        }	
		                        OPT_SET_HIGH_wl = 0;
		                        WL->slip_count1 = 0;
		                        WL->slip_count2 = 0;
		                        WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                    }
		                    else
		                    {
		                    	;
		                    }	
		                }
		                else
		                {
		                	;
		                }	
		            }
		            else
		            {
		            	;
		            }	
		        }
		        else 
		        {
		            OPT_SET_HIGH_wl = 0;
		            WL->slip_count1 = 0;
		            WL->slip_count2 = 0;
		            if(WL->av_ref > WL->av_ref_alt) 
		            {
		            	OPT_NEWSET_wl = 1;
		            }
		            else
		            {
		            	;
		            }
		            		
		            if(OPT_NEWSET_wl == 1) 
		            {
		                if(WL->av_ref > WL->av_ref_alt) 
		                {
		                    WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                    WL->av_btc_slip += tempW3;
		                    WL->slip_count3 = 0;
		                    WL->slip_count3 ++;
		                }
		                else if(WL->av_ref < WL->av_ref_alt) 
		                {
		                    WL->btc_slip = (WL->av_btc_slip / (int8_t)WL->slip_count3);
		                    if(WL->btc_slip > WL->slip_min + (int16_t)VREF_5_KPH) 
		                    {
		                        WL->btc_slip = WL->slip_min + (int16_t)VREF_5_KPH;
		                    }
		                    else if(WL->btc_slip < WL->slip_min) 
		                    {
		                        WL->btc_slip = WL->slip_min;
		                        WL->slip_min -= (int16_t)VREF_2_KPH;
		                    }
		                    else
		                    {
		                    	;
		                    }	
		                    FIRST_INC_wl = 0;
		                    OPT_NEWSET_wl = 0;
		                    WL->slip_count3 = 0;
		                    WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                }
		                else
		                {
		                	;
		                }	
		            }
		            else
		            {
		            	;
		            }	
		        }
		        if(tempW3 <= WL->btc_slip) 
		        {
		            OPT_SET_HIGH_wl = 0;
		            WL->slip_count1 = 0;
		            WL->slip_count2 = 0;
		            if(WL->av_ref > WL->av_ref_alt) 
		            {
		            	OPT_NEWSET_wl = 1;
		            }
		            else
		            {
		            	;
		            }
		            		
		            if(OPT_NEWSET_wl == 1) 
		            {
		                if(WL->av_ref > WL->av_ref_alt) 
		                {
		                    WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                    WL->av_btc_slip += tempW3;
		                    WL->slip_count3 = 0;
		                    WL->slip_count3 ++;
		                }
		                else if(WL->av_ref < WL->av_ref_alt) 
		                {
		                    WL->btc_slip = (WL->av_btc_slip / (int8_t)WL->slip_count3);
		                    if(WL->btc_slip > WL->slip_min + (int16_t)VREF_5_KPH) 
		                    {
		                        WL->btc_slip = WL->slip_min + (int16_t)VREF_5_KPH;
		                    }
		                    else if(WL->btc_slip < WL->slip_min) 
		                    {
		                        WL->btc_slip = WL->slip_min;
		                        WL->slip_min -= (int16_t)VREF_2_KPH;
		                    }
		                    FIRST_INC_wl = 0;
		                    OPT_NEWSET_wl = 0;
		                    WL->slip_count3 = 0;
		                    WL->av_btc_slip = (int16_t)VREF_0_KPH;
		                }
		                else
		                {
		                	;
		                }	
		            }
		            else
		            {
		            	;
		            }	
		        }
		        else
		        {
		        	;
		        }	
		    }
		    else
		    {
		    	;
		    }	
		}
	}
	else
	{
		;
	}	

    if(WL->btc_slip > BTCS_TAR_DV_MAX_FLAT) 
    {
    	WL->btc_slip = BTCS_TAR_DV_MAX_FLAT;
    }
    else
    {
    	;
    }		
}

void	TARGET_THR_MIN_Set(void)
{
    if(HILL_wl == 1) 
    {
        if(WL->slip_min < BTCS_TAR_DV_MIN_HILL) 
        {
        	WL->slip_min = BTCS_TAR_DV_MIN_HILL;        	    		
        }
        else
        {
        	;
        }	
    }
    else 
    {
        if(WL->slip_min < BTCS_TAR_DV_MIN_FLAT) 
        {
        	WL->slip_min = BTCS_TAR_DV_MIN_FLAT;
        }
        else
        {
        	;
        }        
    }
}	
  #endif
  
  
void	BTCS_LOAD_PARAMETER(void)
{
	BTCS_INH_MTP				=5;		/*5%   */
	BTCS_INH_VREF_MAX			=70;	/*70kph*/
	BTCS_INH_VREF_BTCS			=45;	/*45kph*/

	BTCS_EXT_MTP				=5;		/*5%   */
	BTCS_EXT_VREF_MAX			=80;	/*80kph*/
	BTCS_EXT_VREF_BTCS			=50;	/*50kph*/
	
	BTCS_ENT_THR_ARAD			=2;		/*0.5G */
	BTCS_ENT_THR_DV_1ST			=80;	/*10kph*/
	BTCS_ENT_THR_DV_2ND			=120;	/*15kph*/
	
	BTCS_TAR_DV_MAX_FLAT		=120;	/*15kph*/
	BTCS_TAR_DV_MAX_HILL		=120;	/*15kph*/
	BTCS_TAR_DV_MIN_FLAT		=40;	/*5kph */ 
	BTCS_TAR_DV_MIN_HILL		=80;	/*7kph */
	
	BTCS_CMP_DV_HILL			=40;	/*5kph */
	BTCS_CMP_DV_FLAT			=0;		/*0kph */
                                        
	BTCS_CTRL_HLD_SCAN_MIN		=1;
	BTCS_CTRL_HLD_SCAN_MAX		=70;
		
	BTCS_1ST_DMP_DV_HOMO_MU		=64;	/*8kph, vref of BTCS_TWO_OFF dump   */
	BTCS_1ST_DMP_DV_LOW_SIDE	=40;	/*5kph, low side spin check         */
	BTCS_1ST_DMP_DV_HIGH_SIDE	=40;	/*5kph, high side spin check        */
	BTCS_1ST_DMP_LOW_RPM		=100;	/*1000rpm, first cycle low rpm      */
	
	BTCS_1ST_RIS_FULL_ON_SCAN	=28;	/*0.2 sec, for build pattern        */
	BTCS_1ST_RIS_DV_HOLD_MAX	=144;	/*18kph, for (int16_t)U16_HOLD_TIME_MAX */

	BTCS_1ST_HLD_TIME1			=7;
	BTCS_1ST_HLD_TIME2			=14;
		
	BTCS_2ND_DMP_DV_LOW_SIDE1	=64;	/*8kph, low side spin check with current spin for pulse DUMP */
	BTCS_2ND_DMP_DV_LOW_SIDE2	=16;	/*2kph, low side spin check with current spin for full DUMP */
	BTCS_2ND_DMP_DV_HIGH_SIDE	=40;	/*5kph, high side spin check */
	BTCS_2ND_DMP_LOW_RPM		=100;	/*1000rpm, second cycle low rpm */	

	BTCS_2ND_RIS_DV_LOW_SIDE	=160;	/*20kph, low side spin check */
	
	BTCS_2ND_HLD_TIME1			=7;
	BTCS_2ND_HLD_TIME2			=14;	

	BTCS_ESV_ON_DELAY			=1;		/*1 scan, ESV open delay timer in 2nd cycle rise control */
}
  
#endif

#endif /*end of #if __BTCS_NEO_STRUCTURE*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_btcs_Neo
	#include "Mdyn_autosar.h"               
#endif
