#ifndef __LSESPCALSLIPRATIO_H__
#define __LSESPCALSLIPRATIO_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
 
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_m_fl_old, slip_m_fl;
//extern int16_t slip_m_fr_old, slip_m_fr;
//extern int16_t slip_m_rl_old, slip_m_rl;
//extern int16_t slip_m_rr_old, slip_m_rr;
#else
extern int16_t slip_m_fl_old, slip_m_fl;
extern int16_t slip_m_fr_old, slip_m_fr;
extern int16_t slip_m_rl_old, slip_m_rl;
extern int16_t slip_m_rr_old, slip_m_rr;
#endif

extern int16_t slip_filter_gain;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t del_speed_fl, del_speed_fr, del_speed_rl, del_speed_rr;

//extern int16_t lcescs16CalDelWheelSpeedRatefl;
//extern int16_t lcescs16CalDelWheelSpeedRatefr;
//extern int16_t lcescs16CalDelWheelSpeedRaterl;
//extern int16_t lcescs16CalDelWheelSpeedRaterr;

//extern int16_t lcescs16CalWheelSpeedRatefl ; 
//extern int16_t lcescs16CalWheelSpeedRatefr ; 
//extern int16_t lcescs16CalWheelSpeedRaterl ; 
//extern int16_t lcescs16CalWheelSpeedRaterr ;
#else
extern int16_t del_speed_fl, del_speed_fr, del_speed_rl, del_speed_rr;

extern int16_t lcescs16CalDelWheelSpeedRatefl;
extern int16_t lcescs16CalDelWheelSpeedRatefr;
extern int16_t lcescs16CalDelWheelSpeedRaterl;
extern int16_t lcescs16CalDelWheelSpeedRaterr;

extern int16_t lcescs16CalWheelSpeedRatefl ; 
extern int16_t lcescs16CalWheelSpeedRatefr ; 
extern int16_t lcescs16CalWheelSpeedRaterl ; 
extern int16_t lcescs16CalWheelSpeedRaterr ; 
#endif 

/*Global Extern Functions Declaration ******************************************/


#endif
