 /***************************************************************************

*  Program ID: MGH-80 DDS PLUS                                          
*  Program description:. Enhanced Deflation Detection System                   
*  Input files:             
*  Output files:      
*                      
*                                           
*  Special notes: none                                          
***************************************************************************
*  Modification Log                                               
*  Date        Author           Description                      
*  ---         ------           ---------                         
*  10.12.10    Sohyun Ahn       Initial Release                   
*Includes *****************************************************************/


#include "LDDDSPlusCalDetection.h"
#include "LDDDSCallDetection_AP.H"
#include "LDDDSCallDetection.H"
#include "LCESPCalInterpolation.h"


#include "LSABSCallSensorSignal.h"
#include "LDABSCallDctForVref.H"
#include "LDABSCallVrefEst.H"
#include "LDABSCallEstVehDecel.H"
#include "LDABSCallVrefMainFilterProcess.H"

#include "LDRTACallDetection.h"

#if __VDC
#include "LDABSCallVrefCompForESP.h"
#endif


#include "DDSdef.par"
#if (__DDS_PLUS == ENABLE)
/*====Macro Declartion==================================================*/

//#define DDSplus_DATA_LOG_MODE


#define TEETH_DEVI_RESOL 1000
#define DDS_PLUS_SAMPLING_TIME 250 /*2.5ms 400HZ*/
#define FILTER_ORDER 4
#define FILTER_BUFFER_SIZE 9//FILTER_ORDER+1
#define DOWNSAMPLING_RATE 2
#define TEETH_DEVIATION_DET_TIME 10000


#define DDS_PLUS_MIN_SPEED  VREF_20_KPH_RESOL_CHANGE
#define DDS_PLUS_MAX_SPEED  VREF_120_KPH_RESOL_CHANGE

#define TEETH_INDEX_DET_TIME    5
#define TEETH_DEVIATION_CALC_OK_TIME    10
#define SHIFT_FREQ 25
#define TEETH_DEVI_SUM_MAX  15

#define RLS_WEIGHT_FACTOR 100 



#define DDS_RESET_HOLD_TIME 	T_5_S

#define FREQ_THR_FOR_TIRE_LOW_PRESSURE_DET  10
#define FREQ_THR_FOR_TIRE_LOW_PRESSURE_RESET 5
#define FREQ_MAX_FOR_TIRE_LOW_PRESSURE_DET  100
#define TIRE_LOW_PRESSURE_DET_TIME      10
#define CALIB_COMPLETE_TIME 4        
#define SYS_PARA_MAX_THR 3000
/*====Variables & Flags==================================================*/

#if __ECU==ESP_ECU_1
//#pragma DATA_SEG __GPAGE_SEG PAGED_RAM
#endif
/*8 order filter gain
INT  HpfInputGain[FILTER_BUFFER_SIZE]={4,-30,102,-199, 247 ,-199,102,-30, 4};//{402,-3036,10214,-19950, 24742 ,-19950,10214,-3036, 402};
INT  HpfOutputGain[FILTER_BUFFER_SIZE]={10,-58, 153 ,-237,236, -154,64, -16 ,2};//{1000,-5810, 15312 ,-23737,23599, -15381,	6420, -1574 ,174};

INT  BpfInputGain[FILTER_BUFFER_SIZE]={12,-14,36,-22,41,-22,36,-14 ,12 };
INT  BpfOutputGain[FILTER_BUFFER_SIZE]={ 10,-46,108,-159,160,-112,53,-15,2};//{ 1000,-4590,10761,-15906,16042,-11191,5266,-1527,211};
*/
/*4odr filter gain*/
INT  HpfInputGain[FILTER_BUFFER_SIZE]={6,-23,34,-23,6};
INT  HpfOutputGain[FILTER_BUFFER_SIZE]={10,-29,33,-17,3};

INT  BpfInputGain[FILTER_BUFFER_SIZE]={35,78,104,78,35};
INT  BpfOutputGain[FILTER_BUFFER_SIZE]={10,-17,17,-8,2};


INT  ldddsps16Hpf2OrderOutputGain;
INT  ldddsps16Hpf2OrderInputGain[5];
    
UCHAR WHLp;
UCHAR inxp;
UCHAR inxp2;

INT ldddsps16StoredPeriodTime[4][10];
UINT ldddspu16ResPeriod[4][10];
INT ldddsps16ElapsedTime1Pulse[4][50];
INT ldddsps16WhlTeethDeviationF[4][50];
INT ldddsps16ElapsedTime1PulseCrt[4][10];
INT ldddsps16AngularSpeedCrt[4][10];
INT ldddsps16AngularSpeedResmp[4][20];
INT ldddsps16WradInputBufferBpf[4][FILTER_BUFFER_SIZE];
INT ldddsps16WradOutputBufferBpf[4][FILTER_BUFFER_SIZE];
LONG ldddsps16WradInputBufferHpf[4][FILTER_BUFFER_SIZE];
INT ldddsps16WradOutputBufferHpf[4][FILTER_BUFFER_SIZE];
INT ldddsps16AngularSpeedHpf[4][20];
INT ldddsps16AngularSpeedBpf[4][20];
//INT ldddsps16AngularSpeedFil[4][20];
INT ldddsps16AngularSpeedShift[4][20];
INT ldddsps16WradShiftFinal[4][20];
INT ldddsps16WradZoom[4][10];
INT InputMatArray[4][5];
UCHAR TEETH_NUM;
UCHAR ldddspu1StdStrDrvStateForDDS;
INT ldddsps16WhlTeethDeviationMod[4][50];

LONG ldddsps16WradInputBufferLpf[4][5];
LONG ldddsps16WradOutputBufferLpf[4][5];

//INT ldddsps16SysParaMovingAvgDef[4][2][5];
//INT ldddsps16SysParaAvgDefBuffer[4][5][2][5];
//INT ldddsps16SysParaAvgDefBuffer2[4][5][2][5];
INT ldddsps16SysParaDefDct[4];
INT ldddspu8SysParaBufDefDct2[4][5];
INT ldddspu8SysParaBufDefDct[4][5];

INT ldddsps16VehSpeedBufCalib[4][10][5];
INT ldddspu8SysParaCalibBuffer[4][10][2][5];
//INT ldddsps16SysParaCurDefDctRes[4][2];
INT ldddsps16SysParaDefCalBuffer[4][3];

UINT ldddspu16DdsPlusResetHoldCnt;
INT ldddsps16SysParaCalBuffer[4][3];

INT ldddsps16SysParaCumulAvgCalib[4][2][5];
LONG ldddsps16SysParaSumCalib[4][2];
INT ldddsps16VehSpdCalibAvg[4];
//INT ldddsps16SysParaCurCalibRes[4][2];
//INT ldddsps16SysParaCumulResCalib[4][2][5];
INT ldddsps16CumulVehSpdCalib[4][5];
//INT ldddsps16CumulVehSpdCalibRes[4][5];


INT ldddsps16SysParaCumulSumCalib[4][2][5];                   
INT ldddsps16CumulSumVehSpdCalib[4][5];

UCHAR ldddspu8DdsPlusMode,ldddspu8DdsPlusSubMode;

UCHAR ldddspu8DdsPlusModeEepromValue,ldddspu8DdsPlusModeOld;

CHAR ldddspu8VehSpdInx,ldddspu8VehSpdInxTemp;
//temp
//LONG Error_Temp_Sq_Sum[50];
//    INT Error_Temp[50][50];
//    LONG Error_Temp_sq[50][50];
//INT TEETH_INIT_FL[50]={0,1,0,1,0,1,0,0,1,1,2,1,1,1,0,0,-1,0,1,0,0,0,0,0,0,1,2,2,1,1,0,-1,-1,0,0,0,-1,0,0,0,-1,0,-1,-1,-1,-2,-2,-3,0,0};
//INT TEETH_INIT_FR[50]={0,0,0,1,1,0,0,1,0,1,2,1,2,2,1,1,2,1,2,1,1,0,1,1,1,1,1,1,1,0,0,-1,-1,-1,-2,-2,-2,-1,-1,-2,-1,-2,-1,-1,-1,-2,-2,-2,0,0};
//INT TEETH_INIT_RL[50]={1,1,0,0,1,1,0,1,1,0,0,1,0,0,-1,-1,-1,0,-1,-1,0,-1,0,1,0,1,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,-1,0,-1,0,0,-1,-1,-1,0,0};
//INT TEETH_INIT_RR[50]={3,2,2,2,2,1,1,1,2,0,1,0,1,0,0,0,0,-1,-1,-1,0,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,0,-1,0,0,1,0,0,1,1,0,0,0,0,0};
//INT ldddsps16WhlTeethDeviTemp2[50];
//INT ldddsps16WhlTeethDeviCurrent[50];
//INT ldddsps16WhlTeethDeviTemp[50];
INT TeethDeviationSum;
INT ldddsps16SysParaTemp2[2];

INT ldddsps16SysParaTemp2_cal[4][2];
INT cos_cnt;
INT cos_in;
INT cos_out;

LONG ldddsps16SysParaDefCurrent[4][2];

LONG ldddsps16SysParaCalibCurrent[4][2];
LONG ldddsps32VelSpdSumCalib[4];
LONG ldddsps32VelSpdSumDct[4];
//UCHAR U8_TEETH=48;
//UCHAR U8_TEETH_R=48; //Temp

struct 	U8_BIT_STRUCT_t DDSp_F,DDSp_F2;    
struct  U8_BIT_STRUCT_t DDSp_READ_OK_FLG1,DDSp_READ_OK_FLG2,DDSp_READ_OK_FLG3,DDSp_READ_OK_FLG4;
struct  U8_BIT_STRUCT_t DDSp_READ_OK_FLG5,DDSp_READ_OK_FLG6,DDSp_READ_OK_FLG7,DDSp_READ_OK_FLG8;
struct  U8_BIT_STRUCT_t DDSp_WR_OK_FLG1,DDSp_WR_OK_FLG2,DDSp_WR_OK_FLG3,DDSp_WR_OK_FLG4;
struct  U8_BIT_STRUCT_t DDSp_WR_OK_FLG5,DDSp_WR_OK_FLG6,DDSp_WR_OK_FLG7,DDSp_WR_OK_FLG8;

DDSp_SP_t *DDSp_SP_WL,DDSp_SP_FL,DDSp_SP_FR,DDSp_SP_RL,DDSp_SP_RR; 
#if __ECU==ESP_ECU_1	
//#pragma DATA_SEG DEFAULT
#endif

#if defined __MATLAB_COMPILE
WhlStoredData_t	DDS_FL,DDS_FR,DDS_RL,DDS_RR;
WhlIntStoredData_t INT_DDS_FL,INT_DDS_FR,INT_DDS_RL,INT_DDS_RR;
#else
extern UCHAR 		com_rx_buf[8];  
#endif

/*====Function Prototype=================================================*/
void LDDDSp_vCallDetection(void);
void LDDDSp_vDetGoodDrvCond(void);
void LDDDSp_vPrepareTireData(void);
void LDDDSp_vPrepareTireDataEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp);
void LDDDSp_vSignalPreprocessing(void);
void LDDDSp_vSignalPreprocessEachWL(DDSp_SP_t *DDSp_SP_WL_temp,UCHAR WHL_temp);
void LDDDSp_vDetWheelTeethIndex(void);
void LDDDSp_vCalcWhlTeethDeviation(void);
void LDDDSp_vCompWheelTeethDeviation(void);
void LDDDSp_vConv2AngularSpeed(void);
void LDDDSp_vEqualizeSamplingTime(void);
void LDDDSp_vBandPassFilteringWrad(void);
void LSDDSp_vDet2OrderHpfGain(int Zeta,int Fc,int Ts);
void LSDDSp_vDet2OrderLpfGain(int Zeta,int Fc,int Ts);
void LDDDSp_vExe2OrderHpfWrad(void);
//INT LSDDSp_s16Inter10pForCosineCal(INT Input,INT CosInput10Interpol[10],INT CosOutput10Interpol[10]);
INT LSDDSp_s16Inter10pForCosineCal(INT Input);
INT LSDDSp_s16CalCosineValue(INT cos_in);
void LDDDSp_vZoomInputSignal(void);

void LDDDSp_vIndentifySysParaCalib(void); 
void LDDDSp_vIdSysParaCalibEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp); 
INT LDDDSp_vConvSysPara2Freq(INT SysPara1,INT SysPara2);
void LDDDSp_vIndentifySysParaDefDct(void);
void LDDDSp_vIdSysParaDefDctEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp);
void LDDDSp_vDetDdsPlusMode(void);

void LDDDSp_vLoadDDSpTireTeethData(void);
void LDDDSp_vLoadDDSpCalibPara(void);
void LDDDSp_vLoadDDSpDefDctPara(void);
void LDDDSp_vClearDDSpTire1RapData(void);
void LDDDSp_vClearDDSpTireTeethInx(void);
void LDDDSp_vClearSignalPreprocess(void);
void LDDDSp_vClearDDSpCalibPara(void);
void LDDDSp_vClearDDSpDefDctPara(void);
void LDDDSp_vDetDdsPlusRunningReset(void);	
void LDDDSp_vDetTireDeflationState(void);
void LDDDSp_vDetTireDefStateEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp,CHAR DCT_THR,CHAR RESET_THR,CHAR MAX_THR);	
void LDDDSp_vLoadDDSpEepromData(void);
void LDDDSp_vWriteDDSpParameters(void);	
void LDDDSp_vCallDDSplusLogData(void);
void LDDDSp_vCallDDSplusIntegLogData(void);
void LDDDSp_vCallDDSplusDataSet(void);
void LDDDSp_vDetCalibrCompletion(void);
UINT LSDDS_u16WriteEEPROM(UINT addr,UINT data,UINT data2);
void LDDDS_EEPROMReadFunciton(void);
void LDDDS_EEPROMWriteFunciton(void);
/*====Main Code==========================================================*/


void LDDDSp_vCallDetection(void)
{
    LDDDSp_vDetGoodDrvCond();
    LDDDSp_vDetDdsPlusMode();
	switch(ldddspu8DdsPlusMode)
    {
        case FUNCTION_IGNON:
            #if defined __MATLAB_COMPILE
            #else
            //LDDDSp_vLoadDDSpEepromData();
            LDDDS_EEPROMReadFunciton();

                
                
            #endif
			
		break;

		case FUNCTION_DDS_RESET:
		    
            LDDDSp_vClearDDSpTire1RapData();
            LDDDSp_vClearDDSpTireTeethInx();
            LDDDSp_vClearSignalPreprocess();
            LDDDSp_vClearDDSpCalibPara();
            LDDDSp_vClearDDSpDefDctPara();         
		break;
		
        /*case TireTeethDevi determined?? cf tire wear-in mode*/
				
		case FUNCTION_DDS_READY:
			
			switch(ldddspu8DdsPlusSubMode)
			{
		
				case SUB_FUNCTION_CALIBRATION:
				     
				    LDDDSp_vPrepareTireData();	
				    LDDDSp_vSignalPreprocessing();
				    LDDDSp_vIndentifySysParaCalib();
				break;
								
				case SUB_FUNCTION_DETECTION:
				       
				    LDDDSp_vPrepareTireData();	
				    LDDDSp_vSignalPreprocessing();
				    LDDDSp_vIndentifySysParaDefDct();		        																																													
					LDDDSp_vDetTireDeflationState(); 
	
				break;	
						
				/*case TireTeethIndexNotdetermined??*/	
				case SUB_FUNCTION_STANDBY: //below 20kph //Reset Sum Variable
				    
                    LDDDSp_vClearDDSpTire1RapData();
                    LDDDSp_vClearDDSpTireTeethInx();
                    LDDDSp_vClearSignalPreprocess();
                    LDDDSp_vClearDDSpCalibPara();
                    LDDDSp_vClearDDSpDefDctPara();
				break;
			}
			
		break;

		case FUNCTION_DDS_LOW_TIRE_PRESSURE:
			if(ldddsu1DDSRunningReset==ENABLE)
			{
				switch(ldddspu8DdsPlusSubMode)
				{				
   			    		
					case SUB_FUNCTION_RUNNING_RESET:  
					    LDDDSp_vPrepareTireData();	
				        LDDDSp_vSignalPreprocessing();
				        LDDDSp_vIndentifySysParaDefDct();
				        LDDDSp_vDetTireDeflationState();// for test		        																																													
                        LDDDSp_vDetDdsPlusRunningReset();
					break;
					
					case SUB_FUNCTION_STANDBY:
					
                        LDDDSp_vClearDDSpTire1RapData();
                        LDDDSp_vClearDDSpTireTeethInx();
                        LDDDSp_vClearSignalPreprocess();
                        LDDDSp_vClearDDSpCalibPara();
                        LDDDSp_vClearDDSpDefDctPara();					
                    break;
				}
			}		
		break;

		case FUNCTION_INHIBIT:
			;;
		break;

        default:
			;;
        break;
	}    
    
}
/*======================================================================*/
void LDDDSp_vDetDdsPlusMode(void)
{	  
/*	
	#if defined ( __H2DCL) 
	ldddsu1RequestDdsPlusReset=ctu1DDS;
	#elif (__DDS_RESET_BY_ESC_SW == ENABLE)
	ldddsu1RequestDdsPlusReset=fu1DdsSwitchDriverIntent;
	#else
	if(speed_calc_timer>T_1000_MS)
	{
		ldddsu1RequestDdsPlusReset=0;
	}
	else
	{
		ldddsu1RequestDdsPlusReset=1;
	}
	#endif
	
*/
	
	if(ldddspu8DdsPlusMode==FUNCTION_IGNON){
		if(ldddsu1RequestDDSReset==1){
			ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
		}
		else if(speed_calc_timer>T_1000_MS){
			if((ldddspu8DdsPlusModeEepromValue==FUNCTION_IGNON)&&(ldddspu8DdsPlusSubMode==0)){
				ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
			}
			else{
				ldddspu8DdsPlusMode=ldddspu8DdsPlusModeEepromValue; 
			}
		}
		else{
			;
		}
	}
	else if(ldddspu8DdsPlusMode==FUNCTION_DDS_RESET){
		ldddspu16DdsPlusResetHoldCnt++;
		if(ldddspu16DdsPlusResetHoldCnt>DDS_RESET_HOLD_TIME){
			ldddspu8DdsPlusMode=FUNCTION_DDS_READY;
			ldddspu8DdsPlusModeEepromValue=ldddspu8DdsPlusMode;
			ldddspu8DdsPlusModeOld=ldddspu8DdsPlusMode;
			ldddspu8DdsPlusSubMode=0;
			ldddspu16DdsPlusResetHoldCnt=0;
		}
		else{
			;
		}
	} 
	/*
	else if(ldddspu8DdsPlusMode==FUNCTION_INHIBIT)
	{
	   
		if((fu1WheelFLSusDet==0)&&(fu1WheelFRSusDet==0)&&(fu1WheelRLSusDet==0)&&(fu1WheelRRSusDet==0)&&
			(fu1WheelFLErrDet==0)&&(fu1WheelFRErrDet==0)&&(fu1WheelRLErrDet==0)&&(fu1WheelRRErrDet==0)&&
			(fu1MainCanSusDet==0)&&(fu1MainCanLineErrDet==0)&&(fu1EMSTimeOutSusDet==0) &&(fu1EMSTimeOutErrDet==0))
		{
			ldddspu8DdsPlusMode=ldddspu8DdsPlusModeOld;
		}
		
	}	
	*/	
	else if(ldddspu8DdsPlusMode==FUNCTION_DDS_READY){
		/*Add EEPROM ERROR CONDITION*/
		/*Add SWITCH SIGNAL ERROR CONDITION*/
		/*Add Low Voltage Check CONDITION*/
		
		/*if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)||
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
			(fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
		{
			ldddspu8DdsPlusModeOld=ldddspu8DdsPlusMode;
			ldddspu8DdsPlusMode=FUNCTION_INHIBIT;
		}	

		else */
            

        LDDDSp_vDetCalibrCompletion();    
		if(ldddsu1RequestDDSReset==1){
			ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
		}
		else if(ldddsu1DeflationState==1){
			ldddspu8DdsPlusMode=FUNCTION_DDS_LOW_TIRE_PRESSURE;
		}			
		else if((vref_resol_change<=DDS_PLUS_MIN_SPEED)||(vref_resol_change>=DDS_PLUS_MAX_SPEED)){
		    ldddspu8DdsPlusSubMode=SUB_FUNCTION_STANDBY;
		}			
		else{
		    
	
            
			if(ldddspu1AllWhlCalibCompleteFlg==1){
				ldddspu8DdsPlusSubMode=SUB_FUNCTION_DETECTION;
			}
			else{
				ldddspu8DdsPlusSubMode=SUB_FUNCTION_CALIBRATION;
			}
		}
	}
	else if(ldddspu8DdsPlusMode==FUNCTION_DDS_LOW_TIRE_PRESSURE){
		/*if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)||
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
			(fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
		{
			ldddspu8DdsPlusModeOld=ldddspu8DdsPlusMode;
			ldddspu8DdsPlusMode=FUNCTION_INHIBIT;
		}			
		else */
		if(ldddsu1RequestDDSReset==1){
			ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
		}	
		else if(ldddsu1DDSRunningReset==ENABLE){
//	        if((ldddspu1CalibCompleteFlgFL==1)&&(ldddspu1CalibCompleteFlgFR==1)&&(ldddspu1CalibCompleteFlgRL==1)&&(ldddspu1CalibCompleteFlgRR==1)){
//                ldddspu1AllWhlCalibCompleteFlg=1;
//            }   
//            else{
//                ldddspu1AllWhlCalibCompleteFlg=0;
//            }
            LDDDSp_vDetCalibrCompletion();
			if((vref_resol_change>DDS_PLUS_MIN_SPEED)&&(vref_resol_change<DDS_PLUS_MAX_SPEED)){
				

                	
            	if(ldddsu1DeflationState==0){
					ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
				}
				else{
					ldddspu8DdsPlusSubMode=SUB_FUNCTION_RUNNING_RESET;
				}
			}
			else{
				ldddspu8DdsPlusSubMode=SUB_FUNCTION_STANDBY;				
			}
		}
		else{
			;
		}		
	}
	else{
		if(ldddsu1RequestDDSReset==1){
			ldddspu8DdsPlusMode=FUNCTION_DDS_RESET;
		}
	}
}
/*======================================================================*/
void LDDDSp_vDetCalibrCompletion(void)
{
    INT VrefKph;
    CHAR spd_i;
    
    
	ldddspu1CalibCompleteFlgFL=0;
	ldddspu1CalibCompleteFlgFR=0;
	ldddspu1CalibCompleteFlgRL=0;
	ldddspu1CalibCompleteFlgRR=0;    
	VrefKph=(vref_resol_change/64);
	//if(VrefKph>35){
    	spd_i=(VrefKph-40)/10;
    	if(spd_i>4){
    	    spd_i=4;
    	}
    	else if(spd_i<0){
    	    spd_i=0;
    	}
    	else{
    	    ;
    	}
    	
    	if(DDSp_SP_FL.ldddspu1CalibCompleteFlg[spd_i]==1){
    	    ldddspu1CalibCompleteFlgFL=1;
    	}
    	if(DDSp_SP_FR.ldddspu1CalibCompleteFlg[spd_i]==1){
    	    ldddspu1CalibCompleteFlgFR=1;
    
    	}
    	if(DDSp_SP_RL.ldddspu1CalibCompleteFlg[spd_i]==1){
    	    ldddspu1CalibCompleteFlgRL=1;
    	    
    	}
    	if(DDSp_SP_RR.ldddspu1CalibCompleteFlg[spd_i]==1){
    	    ldddspu1CalibCompleteFlgRR=1;
    	    
    	}
    
        if((ldddspu1CalibCompleteFlgFL==1)&&(ldddspu1CalibCompleteFlgFR==1)
            &&(ldddspu1CalibCompleteFlgRL==1)&&(ldddspu1CalibCompleteFlgRR==1)){
            ldddspu1AllWhlCalibCompleteFlg=1;
            
        }   
        else{
            ldddspu1AllWhlCalibCompleteFlg=0;
        }
    //}
    //else{
    //    spd_i=0;
    //}
    
    ldddspu8VehSpdInxTemp=spd_i;
}
/*======================================================================*/
       
/*
Time Resolution : 10us unit(10^-5 s)
Wrad Resolution : 0.01 rad/s 
*/
/*======================================================================*/
void LDDDSp_vDetGoodDrvCond(void)
{
    INT VradMinMaxDiffRatioTemp,VradDiffMax;
    VradDiffMax=abs(FL.vrad_resol_change-FR.vrad_resol_change);
    tempW0=abs(FL.vrad_resol_change-RL.vrad_resol_change);
    if(VradDiffMax<tempW0) VradDiffMax=tempW0;
    tempW0=abs(FL.vrad_resol_change-RR.vrad_resol_change);
    if(VradDiffMax<tempW0) VradDiffMax=tempW0;        
    tempW0=abs(FR.vrad_resol_change-RL.vrad_resol_change);
    if(VradDiffMax<tempW0) VradDiffMax=tempW0;
    tempW0=abs(FR.vrad_resol_change-RR.vrad_resol_change);
    if(VradDiffMax<tempW0) VradDiffMax=tempW0;
    tempW0=abs(RL.vrad_resol_change-RR.vrad_resol_change);
    if(VradDiffMax<tempW0) VradDiffMax=tempW0;
        
    if(vref_resol_change!=0){                                
	    VradMinMaxDiffRatioTemp=(INT)(((ULONG)VradDiffMax*1000)/vref_resol_change);
	}
	else{
	    VradMinMaxDiffRatioTemp=0;
	}
	/*
	if(mtp>5)
	{
		ldddsu8AccelPedalReleaseCnt=T_500_MS;
	}
	else if(ldddsu8AccelPedalReleaseCnt>0)
	{
		ldddsu8AccelPedalReleaseCnt--;
	}
	else
	{
		;
	}
	*/

    ldddspu1StdStrDrvStateForDDS=1;
 	if(BRAKE_SIGNAL==1){
        ldddspu1StdStrDrvStateForDDS=0;
    }
    else if(abs(afz)>AFZ_0G15){
        ldddspu1StdStrDrvStateForDDS=0;
    }
	else if(VradMinMaxDiffRatioTemp>30){
		ldddspu1StdStrDrvStateForDDS=0; 	
	}    
	else if(ebd_refilt_grv>=10){
		ldddspu1StdStrDrvStateForDDS=0; 
	}
    #if __VDC
    else if((abs(yaw_out)>YAW_3DEG)||(abs(alat)>LAT_0G1G)||(abs(wstr)>WSTR_30_DEG)){
    	ldddspu1StdStrDrvStateForDDS=0;
	}
    #endif 
    else if(ldddsu1StdStrDrvStateForDDS==0){
        ldddspu1StdStrDrvStateForDDS=0;
    }
    else{
        ;
    }       
}
/*======================================================================*/
void LDDDSp_vPrepareTireData(void)
{
    
    UCHAR i;
    DDSp_SP_FL.ldddspu8StoredPeriodCnt=DDS_FL.PeriodStoreCnt;    
    DDSp_SP_FR.ldddspu8StoredPeriodCnt=DDS_FR.PeriodStoreCnt;
    DDSp_SP_RL.ldddspu8StoredPeriodCnt=DDS_RL.PeriodStoreCnt;
    DDSp_SP_RR.ldddspu8StoredPeriodCnt=DDS_RR.PeriodStoreCnt;
    
    for(i=0;i<10;i++){
        
        ldddsps16StoredPeriodTime[FL_WL][i]=(DDS_FL.StoredPeriod2[i])/10;   
        ldddsps16StoredPeriodTime[FR_WL][i]=(DDS_FR.StoredPeriod2[i])/10; 
        ldddsps16StoredPeriodTime[RL_WL][i]=(DDS_RL.StoredPeriod2[i])/10; 
        ldddsps16StoredPeriodTime[RR_WL][i]=(DDS_RR.StoredPeriod2[i])/10;     
    }
    
    //    TEETH_NUM=U8_TEETH;
    TEETH_NUM=U8_TEETH; 
    
    LDDDSp_vPrepareTireDataEachWL(&DDSp_SP_FL,FL_WL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    LDDDSp_vPrepareTireDataEachWL(&DDSp_SP_FR,FR_WL);
    
    //    if (U8_F_R_DIFF==1)TEETH_NUM=U8_TEETH_R;
    if (U8_F_R_DIFF==1) TEETH_NUM=U8_TEETH_R; 
    LDDDSp_vPrepareTireDataEachWL(&DDSp_SP_RL,RL_WL);
    LDDDSp_vPrepareTireDataEachWL(&DDSp_SP_RR,RR_WL);
}     
/*======================================================================*/     
void LDDDSp_vPrepareTireDataEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp)
{    /*
    1 Rap Complete Flag
    Det Good Condition during 1 Rap
    Calc 1 Rap Time
    Calc Tref(1Rap Average Pulse time)    
    */
    if(vref_resol_change<=DDS_PLUS_MIN_SPEED){
        //1period flag set for each wheel    
        //1period time
        ldddspu16SumPeriodTimeWL=0;
        ldddspu8SumPeriodCntWL=0;
        ldddspu1Tire1RapCompleteFlgWL=0;
        ldddspu1SteadyStraightDrvFlgWL=1;

    }
    else{
        /*1period flag Initialize--> only 1 scantime flag set*/
        if(ldddspu1Tire1RapCompleteFlgWL==1){
            ldddspu1SteadyStraightDrvFlgWL=1;/*Set at New Revolution start*/
        }
        ldddspu1Tire1RapCompleteFlgWL=0;
        /*Move Residual buffer*/
        if(ldddspu8ResPeriodCntWL!=0){                        
            for(inxp2=0;inxp2<ldddspu8ResPeriodCntWL;inxp2++){
               ldddspu16SumPeriodTimeWL+=(UINT)ldddspu16ResPeriod[WHLp][inxp2];
               ldddsps16ElapsedTime1Pulse[WHLp][inxp2]=(INT)ldddspu16ResPeriod[WHLp][inxp2];  
               ldddspu8SumPeriodCntWL++;    
               ldddspu16ResPeriod[WHLp][inxp2]=0;          
            }
            ldddspu8ResPeriodCntWL=0;
        }
        for(inxp=0;inxp<ldddspu8StoredPeriodCntWL;inxp++){

            if(ldddspu1Tire1RapCompleteFlgWL==1){
            //Save Residual Period
                ldddspu16ResPeriod[WHLp][ldddspu8ResPeriodCntWL]=(UINT)ldddsps16StoredPeriodTime[WHLp][inxp];
            //Count Residual
                ldddspu8ResPeriodCntWL++;    
            }
            else{
                
                ldddsps16ElapsedTime1Pulse[WHLp][ldddspu8SumPeriodCntWL]=(INT)ldddsps16StoredPeriodTime[WHLp][inxp];
                ldddspu16SumPeriodTimeWL+=(UINT)ldddsps16StoredPeriodTime[WHLp][inxp]; 
                
                ldddspu8SumPeriodCntWL++;
      
                if(ldddspu8SumPeriodCntWL>=TEETH_NUM){
                    ldddspu1Tire1RapCompleteFlgWL=1;
                    ldddspu16ElapsedTime1RevWL=ldddspu16SumPeriodTimeWL;
                    ldddsps16TimeReference1RapWL=(INT)(ldddspu16ElapsedTime1RevWL/TEETH_NUM);
                    if(ldddsps16TimeReference1RapWL<=0){
                        ldddsps16TimeReference1RapWL=1;
                    }
                    ldddspu16SumPeriodTimeWL=0;
                    ldddspu8SumPeriodCntWL=0;
                    
                }
                else{
                    ;
                }
            }                            
        }
        /*Check Good Condition? during 1 rap*/
        if(ldddspu1StdStrDrvStateForDDS==0){
            ldddspu1SteadyStraightDrvFlgWL=0;
        }                                
    }
}
/*======================================================================*/
void LDDDSp_vSignalPreprocessing(void)
{    
    TEETH_NUM=U8_TEETH;
    LDDDSp_vSignalPreprocessEachWL(&DDSp_SP_FL,FL_WL);
    LDDDSp_vSignalPreprocessEachWL(&DDSp_SP_FR,FR_WL);

    if (U8_F_R_DIFF==1)TEETH_NUM=U8_TEETH_R;
    LDDDSp_vSignalPreprocessEachWL(&DDSp_SP_RL,RL_WL);
    LDDDSp_vSignalPreprocessEachWL(&DDSp_SP_RR,RR_WL);   
             
}
/*======================================================================*/
void    LDDDSp_vSignalPreprocessEachWL(DDSp_SP_t *DDSp_SP_WL_temp,UCHAR WHL_temp)
{
    DDSp_SP_WL=DDSp_SP_WL_temp;
    WHLp=WHL_temp;
    
//    if((ldddspu1Tire1RapCompleteFlgWL==1)&&(ldddspu1SteadyStraightDrvFlgWL==1)){
//        LDDDSp_vDetWheelTeethIndex();
//        LDDDSp_vCalcWhlTeethDeviation();
//    }
    ldddspu1WheelTeethCalcOkWL=0;
    LDDDSp_vCompWheelTeethDeviation();
    LDDDSp_vConv2AngularSpeed();
    LDDDSp_vEqualizeSamplingTime();
    LDDDSp_vBandPassFilteringWrad();
//    LSDDSp_vDet2OrderHpfGain(65,10,DDS_PLUS_SAMPLING_TIME);/*10hz zeta 0.65, 2.5ms Ts*/
//    LDDDSp_vExe2OrderHpfWrad();
    LDDDSp_vZoomInputSignal();
    
//
//    cos_cnt+=1;
//    if(cos_cnt>32000)cos_cnt=0;
//    cos_in=(cos_cnt*10)%PI2;
//    cos_out=LSDDSp_s16CalCosineValue(cos_in);
       
//     
}
/*======================================================================*/
void LDDDSp_vDetWheelTeethIndex(void)
{
    LONG Error_Temp_Sq_Sum[50];
    INT Error_Temp[50][50];
   LONG Error_Temp_sq[50][50];
    LONG Error_Temp_Sq_Sum_min;
    INT ldddsps16WhlTeethDeviTemp[50];
    INT ldddsps16WhlTeethDeviTemp2[50];
    UCHAR j,ldddspu8WheelTeethIndexTemp;
    
    if(ldddspu1TeethIndexStatusWL==KNOWN){
        /*-------------------------
        Index Reset Condition: 
        1. Min Speed Range
        2. IGN Off-On
        ----------------------------*/
        if(vref_resol_change<=DDS_PLUS_MIN_SPEED){
            ldddspu8WheelTeethIndexWL=0;
            ldddspu8WheelTeethIdxFinalWL=0;
            ldddspu8WheelTeethIndexOldWL=0;
            ldddspu1TeethIndexStatusWL=UNKNOWN;
        }
        else{
            ;
        }
        ldddspu1TeethIndexStatusKWL=0;
    }
    else{   
        /*for every 1 revolution*/
        /*no EEPROM Value -->Index 0*/
        /*Reset Requset --> Inx 1 Start Again*/
        
        for(j=0;j<TEETH_NUM;j++){
            Error_Temp_Sq_Sum[j]=0;
            for(inxp=0;inxp<TEETH_NUM;inxp++){
                /*Devi Ratio 0.1% Resolution*/
                ldddsps16WhlTeethDeviTemp2[inxp]=(INT)((LONG)(ldddsps16ElapsedTime1Pulse[WHLp][inxp]-ldddsps16TimeReference1RapWL)*TEETH_DEVI_RESOL/ldddsps16TimeReference1RapWL);
                inxp2=inxp+j;
                if(inxp2>=TEETH_NUM){
                    inxp2=inxp2-TEETH_NUM;
                }
                 
                Error_Temp[j][inxp]=ldddsps16WhlTeethDeviTemp2[inxp]-ldddsps16WhlTeethDeviationF[WHLp][inxp2];
                Error_Temp_sq[j][inxp]=((LONG)Error_Temp[j][inxp]*Error_Temp[j][inxp]);
                Error_Temp_Sq_Sum[j]+=Error_Temp_sq[j][inxp];            
            }
            
            if(j==0){
                Error_Temp_Sq_Sum_min=Error_Temp_Sq_Sum[j];
                ldddspu8WheelTeethIndexTemp=j;
            }
            else{
                if(Error_Temp_Sq_Sum_min>Error_Temp_Sq_Sum[j]){
                    Error_Temp_Sq_Sum_min=Error_Temp_Sq_Sum[j];
                    ldddspu8WheelTeethIndexTemp=j;
                }               
            }
        }
        
        ldddspu8WheelTeethIndexOldWL=ldddspu8WheelTeethIndexWL;
        ldddspu8WheelTeethIndexWL=ldddspu8WheelTeethIndexTemp;
        ldddspu8WheelTeethIdxFinalWL=0;
        ldddspu1TeethIndexStatusKWL=0;
        if(ldddspu8WheelTeethIndexWL==ldddspu8WheelTeethIndexOldWL){
            ldddsps16TeethIndexDetCntWL++;
            if(ldddsps16TeethIndexDetCntWL>TEETH_INDEX_DET_TIME){
                ldddspu1TeethIndexStatusWL=KNOWN;
                ldddspu1TeethIndexStatusKWL=1;
                ldddspu8WheelTeethIdxFinalWL=ldddspu8WheelTeethIndexWL;
            }
        }
        else if(ldddsps16TeethIndexDetCntWL>0){
            ldddsps16TeethIndexDetCntWL--;
        }
        else{
            ;;//decount??
        }
    }        
}
/*======================================================================*/
/*Call Condition??-> every 1 Revolution Complete*/
void LDDDSp_vCalcWhlTeethDeviation(void)
{
    INT ldddsps16WhlTeethDeviCurrent[50];
    INT ldddsps16WhlTeethDeviTemp[50];
////    INT ldddsps16WhlTeethDeviTemp[50];
//    INT TeethDeviationSum;
    INT ldddsps16WhlTeethDeviModTemp[50];
    
    if(ldddspu1TeethIndexStatusKWL==1){
        for(inxp=0;inxp<TEETH_NUM;inxp++){
            ldddsps16WhlTeethDeviTemp[inxp]=ldddsps16WhlTeethDeviationF[WHLp][inxp];
            ldddsps16WhlTeethDeviModTemp[inxp]=ldddsps16WhlTeethDeviationMod[WHLp][inxp];
            inxp2=inxp+ldddspu8WheelTeethIdxFinalWL;
            if(inxp2>=TEETH_NUM){
                inxp2=inxp2-TEETH_NUM;
            }
            ldddsps16WhlTeethDeviationF[WHLp][inxp2]= ldddsps16WhlTeethDeviTemp[inxp];
            ldddsps16WhlTeethDeviationMod[WHLp][inxp2]=ldddsps16WhlTeethDeviModTemp[inxp];
        }
    }
        
    TeethDeviationSum=0;
    
    for(inxp=0;inxp<TEETH_NUM;inxp++){
        inxp2=inxp+ldddspu8WheelTeethIdxFinalWL;
        if(inxp2>=TEETH_NUM){
            inxp2=inxp2-TEETH_NUM;
        }           
        ldddsps16WhlTeethDeviCurrent[inxp]=(INT)((LONG)(ldddsps16ElapsedTime1Pulse[WHLp][inxp]-ldddsps16TimeReference1RapWL)*TEETH_DEVI_RESOL/ldddsps16TimeReference1RapWL);
        tempL0=(LONG)ldddsps16WhlTeethDeviationMod[WHLp][inxp2]+(LONG)(ldddsps16WhlTeethDeviationF[WHLp][inxp2])*(LONG)ldddspu16TeethDeviCalcCntWL+(LONG)ldddsps16WhlTeethDeviCurrent[inxp];
        ldddsps16WhlTeethDeviTemp[inxp2]=(INT)(tempL0/(ldddspu16TeethDeviCalcCntWL+1));
        ldddsps16WhlTeethDeviationMod[WHLp][inxp2]=(INT)(tempL0%(ldddspu16TeethDeviCalcCntWL+1));
        TeethDeviationSum+=ldddsps16WhlTeethDeviCurrent[inxp];
     }
     

     /*Devi Sum Zero Condition Check*/
     if(abs(TeethDeviationSum)<TEETH_DEVI_SUM_MAX)
     {
        for(inxp=0;inxp<TEETH_NUM;inxp++){
            ldddsps16WhlTeethDeviationF[WHLp][inxp]=ldddsps16WhlTeethDeviTemp[inxp];    
        }
         ldddspu16TeethDeviCalcCntWL++;
         if(ldddspu16TeethDeviCalcCntWL>TEETH_DEVIATION_CALC_OK_TIME){
            if(ldddspu1WheelTeethCalcOkWL==0){
                ldddspu1WheelTeethCalcOkWL=1;
                ldddspu8WheelTeethInxCurrentWL=ldddspu8ResPeriodCntWL+ldddspu8WheelTeethIdxFinalWL;
            }
            if(ldddspu8WheelTeethInxCurrentWL>=TEETH_NUM){
                ldddspu8WheelTeethInxCurrentWL=ldddspu8WheelTeethInxCurrentWL-TEETH_NUM;
            }  
         }        
     }


    //at Counter overflow condition
     if(ldddspu16TeethDeviCalcCntWL>TEETH_DEVIATION_DET_TIME){
        ldddspu16TeethDeviCalcCntWL=1;
     }  
}
/*=====================================================================*/
void LDDDSp_vCompWheelTeethDeviation(void)
{
    INT T_Comp;
    
    if(ldddspu1WheelTeethCalcOkWL==1){

        for(inxp=0;inxp<ldddspu8StoredPeriodCntWL;inxp++){

            inxp2=inxp+ldddspu8WheelTeethInxCurrentWL;
            if(inxp2>=TEETH_NUM){
                inxp2=inxp2-TEETH_NUM;
            }
            T_Comp=(INT)((LONG)ldddsps16WhlTeethDeviationF[WHLp][inxp2]*ldddsps16TimeReference1RapWL/TEETH_DEVI_RESOL);
            ldddsps16ElapsedTime1PulseCrt[WHLp][inxp]=ldddsps16StoredPeriodTime[WHLp][inxp]-T_Comp; 
        }
        ldddspu8WheelTeethInxCurrentWL+=ldddspu8StoredPeriodCntWL;
        if(ldddspu8WheelTeethInxCurrentWL>=TEETH_NUM){
            ldddspu8WheelTeethInxCurrentWL=ldddspu8WheelTeethInxCurrentWL-TEETH_NUM;
        }
    }
    else{
        for(inxp=0;inxp<ldddspu8StoredPeriodCntWL;inxp++){       
            ldddsps16ElapsedTime1PulseCrt[WHLp][inxp]=ldddsps16StoredPeriodTime[WHLp][inxp];
        }
    }
}
/*=====================================================================*/
void LDDDSp_vConv2AngularSpeed(void)
{
    /*Wrad scale rad/s resolution 0.1rad/s ?? */
    for(inxp=0;inxp<ldddspu8StoredPeriodCntWL;inxp++){
        if(ldddsps16ElapsedTime1PulseCrt[WHLp][inxp]!=0){
            ldddsps16AngularSpeedCrt[WHLp][inxp]=(INT)((LONG)PI2*1000/((LONG)ldddsps16ElapsedTime1PulseCrt[WHLp][inxp]*TEETH_NUM));
        }
        else{
            ldddsps16AngularSpeedCrt[WHLp][inxp]=0;
        }
    
    }
}
/*=====================================================================*/
void LDDDSp_vEqualizeSamplingTime(void)
{
    INT ldddsps16TimeCurrent;
    INT ldddsps16WradCurrent;
    INT ldddsps16TimeNew;
    INT ldddsps16ResampleNum;
    INT ldddsps16WradGradient;
    INT ldddsps16WradCorr;
    UCHAR p;
    inxp2=0;
    
    for(inxp=0;inxp<ldddspu8StoredPeriodCntWL;inxp++){
        ldddsps16TimeCurrent=ldddsps16ElapsedTime1PulseCrt[WHLp][inxp];
        ldddsps16WradCurrent=ldddsps16AngularSpeedCrt[WHLp][inxp];
        ldddsps16TimeNew=ldddsps16Time1PulseResidueWL+ldddsps16TimeCurrent;
        
        ldddsps16ResampleNum=ldddsps16TimeNew/DDS_PLUS_SAMPLING_TIME;
        
        if (ldddsps16ResampleNum==0){
            ldddsps16Time1PulseResidueWL=ldddsps16TimeNew;
            ldddsps16WradOldWL=ldddsps16WradCurrent;
        }
        else{
            /*Angular Acceleration resol 0.01 rad/s^*/
            if(ldddsps16TimeCurrent<=0)ldddsps16TimeCurrent=1;
            ldddsps16WradGradient=(INT)((LONG)(ldddsps16WradCurrent -ldddsps16WradOldWL)*100000/ldddsps16TimeCurrent);

            for (p=1;p<=ldddsps16ResampleNum;p++){
                if (p==1){
                    ldddsps16WradCorr=(INT)((LONG)ldddsps16WradGradient*(LONG)(DDS_PLUS_SAMPLING_TIME-ldddsps16Time1PulseResidueWL)/100000);                    
                    ldddsps16AngularSpeedResmp[WHLp][inxp2]=(ldddsps16WradOldWL+ldddsps16WradCorr);
                    inxp2++;
                }    
                else{
                    ldddsps16WradCorr=(INT)(((LONG)ldddsps16WradGradient*DDS_PLUS_SAMPLING_TIME)/100000);                    
                    ldddsps16AngularSpeedResmp[WHLp][inxp2]=(ldddsps16AngularSpeedResmp[WHLp][inxp2-1]+ldddsps16WradCorr);
                    inxp2++;                
                }
            }    
            ldddsps16WradOldWL=ldddsps16WradCurrent;
            //ldddsps16Time1PulseResidueWL=ldddsps16TimeNew%DDS_PLUS_SAMPLING_TIME;  
            ldddsps16Time1PulseResidueWL=ldddsps16TimeNew-(ldddsps16ResampleNum*DDS_PLUS_SAMPLING_TIME);  
	    }	
    }
    ldddsps16WradResmpNumWL=inxp2;	    
    for (inxp=ldddsps16WradResmpNumWL;inxp<20;inxp++){
        ldddsps16AngularSpeedResmp[WHLp][inxp]=0;
    }

    /*Maxumum Resample array Num: Low Speed  20kph -->buffer size 130*/
}
/*=================================================================*/
void LDDDSp_vBandPassFilteringWrad(void)
{
    LONG HpfInputBufferSumTemp,HpfOutputBufferSumTemp;
    LONG BpfInputBufferSumTemp,BpfOutputBufferSumTemp;
    LONG FilterMultiTemp0,FilterMultiTemp1;
    
    for(inxp=0;inxp<ldddsps16WradResmpNumWL;inxp++){   
        if(ldddspu1BPFInitialOkFlgWL==0){
            ldddsps16WradInputBufferBpf[WHLp][ldddsps16BPFInitialOkCntWL]=0;//ldddsps16AngularSpeedResmp[WHLp][inxp];
            ldddsps16WradOutputBufferBpf[WHLp][ldddsps16BPFInitialOkCntWL]=0;//ldddsps16AngularSpeedResmp[WHLp][inxp];
            ldddsps16WradInputBufferHpf[WHLp][ldddsps16BPFInitialOkCntWL]=0;//ldddsps16AngularSpeedResmp[WHLp][inxp];
            ldddsps16WradOutputBufferHpf[WHLp][ldddsps16BPFInitialOkCntWL]=0;//ldddsps16AngularSpeedResmp[WHLp][inxp];
            ldddsps16BPFInitialOkCntWL++;
            if(ldddsps16BPFInitialOkCntWL>=FILTER_ORDER){
                ldddspu1BPFInitialOkFlgWL=1;
                ldddsps16BPFInitialOkCntWL=0;     
            }
        }
        else{
            /*High Pass Filter Routine*/
            /*Buffer shift*/
            /*Inx 0: Lastest Input*/
            /*Consider Data Jump & Initialize */
//            ldddsps16WradInputBufferHpf[WHLp][8]=ldddsps16WradInputBufferHpf[WHLp][7];
//            ldddsps16WradInputBufferHpf[WHLp][7]=ldddsps16WradInputBufferHpf[WHLp][6];
//            ldddsps16WradInputBufferHpf[WHLp][6]=ldddsps16WradInputBufferHpf[WHLp][5];
//            ldddsps16WradInputBufferHpf[WHLp][5]=ldddsps16WradInputBufferHpf[WHLp][4];
            ldddsps16WradInputBufferHpf[WHLp][4]=ldddsps16WradInputBufferHpf[WHLp][3];
            ldddsps16WradInputBufferHpf[WHLp][3]=ldddsps16WradInputBufferHpf[WHLp][2];
            ldddsps16WradInputBufferHpf[WHLp][2]=ldddsps16WradInputBufferHpf[WHLp][1];
            ldddsps16WradInputBufferHpf[WHLp][1]=ldddsps16WradInputBufferHpf[WHLp][0];
            ldddsps16WradInputBufferHpf[WHLp][0]=(LONG)ldddsps16AngularSpeedResmp[WHLp][inxp]*100;


//            ldddsps16WradOutputBufferHpf[WHLp][8]=ldddsps16WradOutputBufferHpf[WHLp][7]; 
//            ldddsps16WradOutputBufferHpf[WHLp][7]=ldddsps16WradOutputBufferHpf[WHLp][6];
//            ldddsps16WradOutputBufferHpf[WHLp][6]=ldddsps16WradOutputBufferHpf[WHLp][5];
//            ldddsps16WradOutputBufferHpf[WHLp][5]=ldddsps16WradOutputBufferHpf[WHLp][4];
            ldddsps16WradOutputBufferHpf[WHLp][4]=ldddsps16WradOutputBufferHpf[WHLp][3];
            ldddsps16WradOutputBufferHpf[WHLp][3]=ldddsps16WradOutputBufferHpf[WHLp][2];
            ldddsps16WradOutputBufferHpf[WHLp][2]=ldddsps16WradOutputBufferHpf[WHLp][1];
            ldddsps16WradOutputBufferHpf[WHLp][1]=ldddsps16WradOutputBufferHpf[WHLp][0];               
            
            HpfInputBufferSumTemp=0;
            
            HpfInputBufferSumTemp+=(LONG)HpfInputGain[0]*ldddsps16WradInputBufferHpf[WHLp][0];
            HpfInputBufferSumTemp+=(LONG)HpfInputGain[1]*ldddsps16WradInputBufferHpf[WHLp][1];
            HpfInputBufferSumTemp+=(LONG)HpfInputGain[2]*ldddsps16WradInputBufferHpf[WHLp][2];
            HpfInputBufferSumTemp+=(LONG)HpfInputGain[3]*ldddsps16WradInputBufferHpf[WHLp][3];
            HpfInputBufferSumTemp+=(LONG)HpfInputGain[4]*ldddsps16WradInputBufferHpf[WHLp][4];
//            HpfInputBufferSumTemp+=HpfInputGain[5]*(LONG)ldddsps16WradInputBufferHpf[WHLp][5];
//            HpfInputBufferSumTemp+=HpfInputGain[6]*(LONG)ldddsps16WradInputBufferHpf[WHLp][6];
//            HpfInputBufferSumTemp+=HpfInputGain[7]*(LONG)ldddsps16WradInputBufferHpf[WHLp][7];
//            HpfInputBufferSumTemp+=HpfInputGain[8]*(LONG)ldddsps16WradInputBufferHpf[WHLp][8];
            
            HpfOutputBufferSumTemp=0;
            HpfOutputBufferSumTemp+=(LONG)HpfOutputGain[1]*ldddsps16WradOutputBufferHpf[WHLp][1];
            HpfOutputBufferSumTemp+=(LONG)HpfOutputGain[2]*ldddsps16WradOutputBufferHpf[WHLp][2];
            HpfOutputBufferSumTemp+=(LONG)HpfOutputGain[3]*ldddsps16WradOutputBufferHpf[WHLp][3];
            HpfOutputBufferSumTemp+=(LONG)HpfOutputGain[4]*ldddsps16WradOutputBufferHpf[WHLp][4];
//            HpfOutputBufferSumTemp+=HpfOutputGain[5]*(LONG)ldddsps16WradOutputBufferHpf[WHLp][5];
//            HpfOutputBufferSumTemp+=HpfOutputGain[6]*(LONG)ldddsps16WradOutputBufferHpf[WHLp][6];
//            HpfOutputBufferSumTemp+=HpfOutputGain[7]*(LONG)ldddsps16WradOutputBufferHpf[WHLp][7];
//            HpfOutputBufferSumTemp+=HpfOutputGain[8]*(LONG)ldddsps16WradOutputBufferHpf[WHLp][8];
            
            FilterMultiTemp0=HpfInputBufferSumTemp-HpfOutputBufferSumTemp;
            ldddsps16WradOutputBufferHpf[WHLp][0]=(INT)(FilterMultiTemp0/10);
            ldddsps16AngularSpeedHpf[WHLp][inxp]=ldddsps16WradOutputBufferHpf[WHLp][0];
                    
            /* Low Pass Filter Routine*/
//            ldddsps16WradInputBufferBpf[WHLp][8]=ldddsps16WradInputBufferBpf[WHLp][7];
//            ldddsps16WradInputBufferBpf[WHLp][7]=ldddsps16WradInputBufferBpf[WHLp][6];
//            ldddsps16WradInputBufferBpf[WHLp][6]=ldddsps16WradInputBufferBpf[WHLp][5];
//            ldddsps16WradInputBufferBpf[WHLp][5]=ldddsps16WradInputBufferBpf[WHLp][4];
            ldddsps16WradInputBufferBpf[WHLp][4]=ldddsps16WradInputBufferBpf[WHLp][3];
            ldddsps16WradInputBufferBpf[WHLp][3]=ldddsps16WradInputBufferBpf[WHLp][2];
            ldddsps16WradInputBufferBpf[WHLp][2]=ldddsps16WradInputBufferBpf[WHLp][1];
            ldddsps16WradInputBufferBpf[WHLp][1]=ldddsps16WradInputBufferBpf[WHLp][0];
            ldddsps16WradInputBufferBpf[WHLp][0]=ldddsps16AngularSpeedHpf[WHLp][inxp];
            
            
//            ldddsps16WradOutputBufferBpf[WHLp][8]=ldddsps16WradOutputBufferBpf[WHLp][7];
//            ldddsps16WradOutputBufferBpf[WHLp][7]=ldddsps16WradOutputBufferBpf[WHLp][6];
//            ldddsps16WradOutputBufferBpf[WHLp][6]=ldddsps16WradOutputBufferBpf[WHLp][5];
//            ldddsps16WradOutputBufferBpf[WHLp][5]=ldddsps16WradOutputBufferBpf[WHLp][4];
            ldddsps16WradOutputBufferBpf[WHLp][4]=ldddsps16WradOutputBufferBpf[WHLp][3];
            ldddsps16WradOutputBufferBpf[WHLp][3]=ldddsps16WradOutputBufferBpf[WHLp][2];
            ldddsps16WradOutputBufferBpf[WHLp][2]=ldddsps16WradOutputBufferBpf[WHLp][1];
            ldddsps16WradOutputBufferBpf[WHLp][1]=ldddsps16WradOutputBufferBpf[WHLp][0];
                       
            BpfInputBufferSumTemp=0;                 
            BpfInputBufferSumTemp+=(LONG)BpfInputGain[0]*ldddsps16WradInputBufferBpf[WHLp][0];
            BpfInputBufferSumTemp+=(LONG)BpfInputGain[1]*ldddsps16WradInputBufferBpf[WHLp][1];
            BpfInputBufferSumTemp+=(LONG)BpfInputGain[2]*ldddsps16WradInputBufferBpf[WHLp][2];
            BpfInputBufferSumTemp+=(LONG)BpfInputGain[3]*ldddsps16WradInputBufferBpf[WHLp][3];
            BpfInputBufferSumTemp+=(LONG)BpfInputGain[4]*ldddsps16WradInputBufferBpf[WHLp][4];
//            BpfInputBufferSumTemp+=BpfInputGain[5]*(LONG)ldddsps16WradInputBufferBpf[WHLp][5];
//            BpfInputBufferSumTemp+=BpfInputGain[6]*(LONG)ldddsps16WradInputBufferBpf[WHLp][6];
//            BpfInputBufferSumTemp+=BpfInputGain[7]*(LONG)ldddsps16WradInputBufferBpf[WHLp][7];
//            BpfInputBufferSumTemp+=BpfInputGain[8]*(LONG)ldddsps16WradInputBufferBpf[WHLp][8];
            
            BpfOutputBufferSumTemp=0;
            BpfOutputBufferSumTemp+=(LONG)BpfOutputGain[1]*ldddsps16WradOutputBufferBpf[WHLp][1];
            BpfOutputBufferSumTemp+=(LONG)BpfOutputGain[2]*ldddsps16WradOutputBufferBpf[WHLp][2];
            BpfOutputBufferSumTemp+=(LONG)BpfOutputGain[3]*ldddsps16WradOutputBufferBpf[WHLp][3];
            BpfOutputBufferSumTemp+=(LONG)BpfOutputGain[4]*ldddsps16WradOutputBufferBpf[WHLp][4];
//            BpfOutputBufferSumTemp+=BpfOutputGain[5]*(LONG)ldddsps16WradOutputBufferBpf[WHLp][5];
//            BpfOutputBufferSumTemp+=BpfOutputGain[6]*(LONG)ldddsps16WradOutputBufferBpf[WHLp][6];
//            BpfOutputBufferSumTemp+=BpfOutputGain[7]*(LONG)ldddsps16WradOutputBufferBpf[WHLp][7];
//            BpfOutputBufferSumTemp+=BpfOutputGain[8]*(LONG)ldddsps16WradOutputBufferBpf[WHLp][8];   
            
            BpfInputBufferSumTemp=BpfInputBufferSumTemp+ldddsps16AngularSpeedBpfResWL;
            //ldddsps16AngularSpeedBpfResWL=(INT)(BpfInputBufferSumTemp%100);
            ldddsps16AngularSpeedBpfResWL=(INT)(BpfInputBufferSumTemp-(LONG)(BpfInputBufferSumTemp/100)*100);
            FilterMultiTemp0=(BpfInputBufferSumTemp/100)-BpfOutputBufferSumTemp;
            ldddsps16WradOutputBufferBpf[WHLp][0]=(INT)(FilterMultiTemp0/10);
            
            ldddsps16AngularSpeedBpf[WHLp][inxp]=ldddsps16WradOutputBufferBpf[WHLp][0];
        }
    }
}
/*=================================================================*/
void LSDDSp_vDet2OrderHpfGain(int Zeta,int Fc,int Ts)
{
    /*Set Gain Scale 10*/

//    int Wn;
//    Wn = (int)(PI2*(long)Fc/100);/*Scale 0.1 rad/s*/
//    tempW1=(int)(Ts*(long)Wn/10000);/*Time 0.1ms scale-->Total scale 1000 */
//    /*set Zeta Scale 100 */
//    ldddsps16Hpf2OrderOutputGain=(int)((((long)tempW1*tempW1)+(40*Zeta*(long)tempW1))/100000)+40;
//    ldddsps16Hpf2OrderInputGain[0]=80-(int)(2*((long)tempW1*tempW1)/100000);
//    ldddsps16Hpf2OrderInputGain[1]=(int)(((40*Zeta*(long)tempW1)-((long)tempW1*tempW1))/100000)-40;
//    ldddsps16Hpf2OrderInputGain[2]=40;
//    ldddsps16Hpf2OrderInputGain[3]=-80;
//    ldddsps16Hpf2OrderInputGain[4]=40;

    ldddsps16Hpf2OrderOutputGain=44;
    ldddsps16Hpf2OrderInputGain[0]=79;
    ldddsps16Hpf2OrderInputGain[1]=-36;
    ldddsps16Hpf2OrderInputGain[2]=40;
    ldddsps16Hpf2OrderInputGain[3]=-80;
    ldddsps16Hpf2OrderInputGain[4]=40;
}
/*=================================================================*/
void LSDDSp_vDet2OrderLpfGain(int Zeta,int Fc,int Ts)
{
    ;
}
/*=================================================================*/
void LDDDSp_vExe2OrderHpfWrad(void)
{
/*Remove BPF Signal DC Offset
    INT OutputMulti=1;
    LONG tempsum;
    
    for(inxp=0;inxp<ldddsps16WradResmpNumWL;inxp++){        
        InputMatArray[WHLp][1]=InputMatArray[WHLp][0];
        InputMatArray[WHLp][0]=ldddsps16Hpf2OrdOutputTempWL;
        InputMatArray[WHLp][4]=InputMatArray[WHLp][3];
        InputMatArray[WHLp][3]=InputMatArray[WHLp][2];
        InputMatArray[WHLp][2]=ldddsps16AngularSpeedBpf[WHLp][inxp];
        tempsum=0;
        tempsum+=(LONG)ldddsps16Hpf2OrderInputGain[0]*InputMatArray[WHLp][0];
        tempsum+=(LONG)ldddsps16Hpf2OrderInputGain[1]*InputMatArray[WHLp][1];
        tempsum+=(LONG)ldddsps16Hpf2OrderInputGain[2]*InputMatArray[WHLp][2];
        tempsum+=(LONG)ldddsps16Hpf2OrderInputGain[3]*InputMatArray[WHLp][3];
        tempsum+=(LONG)ldddsps16Hpf2OrderInputGain[4]*InputMatArray[WHLp][4];
        
        ldddsps16Hpf2OrdOutputTempWL=(INT)(tempsum/ldddsps16Hpf2OrderOutputGain);
        
        ldddsps16AngularSpeedFil[WHLp][inxp]=ldddsps16Hpf2OrdOutputTempWL*OutputMulti;        
    }
*/    
}
/*=================================================================*/
void LDDDSp_vZoomInputSignal(void)
{
    INT cos_input;
    /*Signal Shift*/ 
    INT TempDiff;
    UCHAR ScaleFactor;
    INT Fs=400; 
    INT LpfInputGain[5]={91,-5,123,-5,91};
    INT LpfOutputGain[5]={100,-297,354,-197,43};
    LONG LpfInputBufferSumTemp,LpfOutputBufferSumTemp;
    LONG FilterMultiTemp0,FilterMultiTemp1;
    
    inxp2=0;
    ScaleFactor=100;
    for(inxp=0;inxp<ldddsps16WradResmpNumWL;inxp++){   
        ldddsps16ResmpTimeCntWL++;
        //Fs=(INT)((LONG)100000/DDS_PLUS_SAMPLING_TIME);
        if(ldddsps16ResmpTimeCntWL>=(INT)(Fs/SHIFT_FREQ))ldddsps16ResmpTimeCntWL=0;
        
//        cos_input=(INT)((LONG)ldddsps16ResmpTimeCntWL*PI2*SHIFT_FREQ*DDS_PLUS_SAMPLING_TIME/100000);/*How can we det time??*/
        cos_input=ldddsps16ResmpTimeCntWL;
        ldddsps16CosOutputWL=LSDDSp_s16CalCosineValue(cos_input);
        ldddsps16AngularSpeedShift[WHLp][inxp]=(INT)(((LONG)ldddsps16AngularSpeedBpf[WHLp][inxp])*ldddsps16CosOutputWL/ScaleFactor);
        
        if(ldddspu1ZoomLPFInitialOkFlgWL==0){  
            ldddsps16ZoomLPFInitialOkCntWL++;
            ldddsps16WradInputBufferLpf[WHLp][4]=ldddsps16WradInputBufferLpf[WHLp][3];
            ldddsps16WradInputBufferLpf[WHLp][3]=ldddsps16WradInputBufferLpf[WHLp][2];
            ldddsps16WradInputBufferLpf[WHLp][2]=ldddsps16WradInputBufferLpf[WHLp][1];
            ldddsps16WradInputBufferLpf[WHLp][1]=ldddsps16WradInputBufferLpf[WHLp][0];
            ldddsps16WradInputBufferLpf[WHLp][0]=((LONG)ldddsps16AngularSpeedShift[WHLp][inxp])*5;
    
            ldddsps16WradOutputBufferLpf[WHLp][4]=ldddsps16WradOutputBufferLpf[WHLp][3];
            ldddsps16WradOutputBufferLpf[WHLp][3]=ldddsps16WradOutputBufferLpf[WHLp][2];
            ldddsps16WradOutputBufferLpf[WHLp][2]=ldddsps16WradOutputBufferLpf[WHLp][1];
            ldddsps16WradOutputBufferLpf[WHLp][1]=ldddsps16WradOutputBufferLpf[WHLp][0];             
            ldddsps16WradOutputBufferLpf[WHLp][0]=((LONG)ldddsps16AngularSpeedShift[WHLp][inxp])*5;
            
            ldddsps16WradShiftFinal[WHLp][inxp]=(INT)(ldddsps16WradOutputBufferLpf[WHLp][0]);
            if(ldddsps16ZoomLPFInitialOkCntWL>=6){
                ldddspu1ZoomLPFInitialOkFlgWL=1;
                ldddsps16ZoomLPFInitialOkCntWL=0;     
            }            
        }
        else{    
        /*LPF2 to remove shifted high freq*/
            ldddsps16WradInputBufferLpf[WHLp][4]=ldddsps16WradInputBufferLpf[WHLp][3];
            ldddsps16WradInputBufferLpf[WHLp][3]=ldddsps16WradInputBufferLpf[WHLp][2];
            ldddsps16WradInputBufferLpf[WHLp][2]=ldddsps16WradInputBufferLpf[WHLp][1];
            ldddsps16WradInputBufferLpf[WHLp][1]=ldddsps16WradInputBufferLpf[WHLp][0];
            ldddsps16WradInputBufferLpf[WHLp][0]=((LONG)ldddsps16AngularSpeedShift[WHLp][inxp])*5;
    
            ldddsps16WradOutputBufferLpf[WHLp][4]=ldddsps16WradOutputBufferLpf[WHLp][3];
            ldddsps16WradOutputBufferLpf[WHLp][3]=ldddsps16WradOutputBufferLpf[WHLp][2];
            ldddsps16WradOutputBufferLpf[WHLp][2]=ldddsps16WradOutputBufferLpf[WHLp][1];
            ldddsps16WradOutputBufferLpf[WHLp][1]=ldddsps16WradOutputBufferLpf[WHLp][0];
                       
            LpfInputBufferSumTemp=0;                 
            LpfInputBufferSumTemp+=((LONG)ldddsps16WradInputBufferLpf[WHLp][0]*(LONG)LpfInputGain[0]);
            LpfInputBufferSumTemp+=((LONG)ldddsps16WradInputBufferLpf[WHLp][1]*(LONG)LpfInputGain[1]);
            LpfInputBufferSumTemp+=((LONG)ldddsps16WradInputBufferLpf[WHLp][2]*(LONG)LpfInputGain[2]);
            LpfInputBufferSumTemp+=((LONG)ldddsps16WradInputBufferLpf[WHLp][3]*(LONG)LpfInputGain[3]);
            LpfInputBufferSumTemp+=((LONG)ldddsps16WradInputBufferLpf[WHLp][4]*(LONG)LpfInputGain[4]);
            
            LpfOutputBufferSumTemp=0;
            LpfOutputBufferSumTemp+=((LONG)ldddsps16WradOutputBufferLpf[WHLp][1]*(LONG)LpfOutputGain[1]);
            LpfOutputBufferSumTemp+=((LONG)ldddsps16WradOutputBufferLpf[WHLp][2]*(LONG)LpfOutputGain[2]);
            LpfOutputBufferSumTemp+=((LONG)ldddsps16WradOutputBufferLpf[WHLp][3]*(LONG)LpfOutputGain[3]);
            LpfOutputBufferSumTemp+=((LONG)ldddsps16WradOutputBufferLpf[WHLp][4]*(LONG)LpfOutputGain[4]);
            
            //LpfInputBufferSumTemp=LpfInputBufferSumTemp+ldddsps16AngularSpeedBpfResWL;
            //ldddsps16AngularSpeedBpfResWL=0;//(INT)(BpfInputBufferSumTemp%100);
            FilterMultiTemp0=(LpfInputBufferSumTemp/100)-LpfOutputBufferSumTemp;
            ldddsps16WradOutputBufferLpf[WHLp][0]=(FilterMultiTemp0/100);
            if (ldddsps16WradOutputBufferLpf[WHLp][0]>32000){
                ldddsps16WradShiftFinal[WHLp][inxp]=32000;
            }
            else if (ldddsps16WradOutputBufferLpf[WHLp][0]<-32000){
                ldddsps16WradShiftFinal[WHLp][inxp]=-32000;
            }
            else{
                ldddsps16WradShiftFinal[WHLp][inxp]=(INT)(ldddsps16WradOutputBufferLpf[WHLp][0]);
            }            
        }        
    /*Decimation--> BPF Singnal No Need to LPF bofore decimation*/
        ldddsps16WradDeciCounterWL++;
        if(ldddsps16WradDeciCounterWL>=DOWNSAMPLING_RATE){
            ldddsps16WradZoom[WHLp][inxp2]=ldddsps16WradShiftFinal[WHLp][inxp];
            inxp2++;
            ldddsps16WradDeciCounterWL=0;
        }
   }
   ldddsps16WradResmpNumDeciWL=inxp2; 
    
}
/*=================================================================*/
INT LSDDSp_s16CalCosineValue(INT cos_in)
{
    INT cos_in2,cos_out;
    /* Set Cos Value SF 100/
    cos_in_temp=cos_in%(PI2);//Consider Resolution 1000->2pi=6283
    /*Scale Factor 100*/
    /*10 Point Interpolation for Cosine Implementation*/
    /*0~pi/2*/
    INT CosInput10Interpol[16]={100,92,71,38,0,-38,-71,-92,-100,-92,-71,-38,0,38,71,92};
    
    cos_out=CosInput10Interpol[cos_in];
  
//    cos_in=cos_in%PI2;
//    if(cos_in<(PI2/4)){
//        cos_in2=cos_in;
//        //cos_out=LSDDSp_s16Inter10pForCosineCal(cos_in2,CosInput10Interpol[10],CosOutput10Interpol[10]);
//        cos_out=LSDDSp_s16Inter10pForCosineCal(cos_in2);
//    }
//    /*pi/2~pi*/
//    else if (cos_in<(PI2/2)){
//        cos_in2=(PI2/2)-cos_in;
//        //cos_out=-LSDDSp_s16Inter10pForCosineCal(cos_in2,CosInput10Interpol[10],CosOutput10Interpol[10]);
//        cos_out=-LSDDSp_s16Inter10pForCosineCal(cos_in2);
//    }
//    /*pi~pi*3/2*/
//    else if (cos_in<(PI2*3/4)){
//        cos_in2=cos_in-(PI2/2);
//        //cos_out=-LSDDSp_s16Inter10pForCosineCal(cos_in2,CosInput10Interpol[10],CosOutput10Interpol[10]);
//        cos_out=-LSDDSp_s16Inter10pForCosineCal(cos_in2);
//    }
//    /*pi*3/2*pi*/
//    else{
//        cos_in2=PI2-cos_in;
//        //cos_out=LSDDSp_s16Inter10pForCosineCal(cos_in2,CosInput10Interpol[10],CosOutput10Interpol[10]);
//        cos_out=LSDDSp_s16Inter10pForCosineCal(cos_in2);
//    }
    return cos_out;
}
/*=================================================================*/
//INT LSDDSp_s16Inter10pForCosineCal(INT Input,INT CosInput10Interpol[10],INT CosOutput10Interpol[10])
INT LSDDSp_s16Inter10pForCosineCal(INT Input)
{

    INT CosInput10Interpol[10]={0,174,348,523,698,872,1047,1221,1395,1570};
    INT CosOutput10Interpol[10]={100,98,94,87,77,64,50,34,17,0};
    INT Output_Val_F;
    if(Input<CosInput10Interpol[1]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[1]-CosOutput10Interpol[0])*(Input-CosInput10Interpol[0])/(CosInput10Interpol[1]-CosInput10Interpol[0]))+CosOutput10Interpol[0];
    }                            
    else if(Input<CosInput10Interpol[2]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[2]-CosOutput10Interpol[1])*(Input-CosInput10Interpol[1])/(CosInput10Interpol[2]-CosInput10Interpol[1]))+CosOutput10Interpol[1];
    }                            
    else if(Input<CosInput10Interpol[3]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[3]-CosOutput10Interpol[2])*(Input-CosInput10Interpol[2])/(CosInput10Interpol[3]-CosInput10Interpol[2]))+CosOutput10Interpol[2];
    }                            
    else if(Input<CosInput10Interpol[4]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[4]-CosOutput10Interpol[3])*(Input-CosInput10Interpol[3])/(CosInput10Interpol[4]-CosInput10Interpol[3]))+CosOutput10Interpol[3];
    }                            
    else if(Input<CosInput10Interpol[5]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[5]-CosOutput10Interpol[4])*(Input-CosInput10Interpol[4])/(CosInput10Interpol[5]-CosInput10Interpol[4]))+CosOutput10Interpol[4];
    }                            
    else if(Input<CosInput10Interpol[6]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[6]-CosOutput10Interpol[5])*(Input-CosInput10Interpol[5])/(CosInput10Interpol[6]-CosInput10Interpol[5]))+CosOutput10Interpol[5];
    }                            
    else if(Input<CosInput10Interpol[7]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[7]-CosOutput10Interpol[6])*(Input-CosInput10Interpol[6])/(CosInput10Interpol[7]-CosInput10Interpol[6]))+CosOutput10Interpol[6];
    }                            
    else if(Input<CosInput10Interpol[8]){
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[8]-CosOutput10Interpol[7])*(Input-CosInput10Interpol[7])/(CosInput10Interpol[8]-CosInput10Interpol[7]))+CosOutput10Interpol[7];
    }                            
    else {                       
        Output_Val_F=(INT)((LONG)(CosOutput10Interpol[9]-CosOutput10Interpol[8])*(Input-CosInput10Interpol[8])/(CosInput10Interpol[9]-CosInput10Interpol[8]))+CosOutput10Interpol[8];
    }
   
    return Output_Val_F;
}
/*=================================================================*/
void LDDDSp_vIndentifySysParaCalib(void) 
{
    /*Calc if only Teeth Devi & Teeth Inx Det*/
    TEETH_NUM=U8_TEETH;
    LDDDSp_vIdSysParaCalibEachWL(&DDSp_SP_FL,FL_WL);
    LDDDSp_vIdSysParaCalibEachWL(&DDSp_SP_FR,FR_WL);

    if (U8_F_R_DIFF==1)TEETH_NUM=U8_TEETH_R;
    LDDDSp_vIdSysParaCalibEachWL(&DDSp_SP_RL,RL_WL);
    LDDDSp_vIdSysParaCalibEachWL(&DDSp_SP_RR,RR_WL);
    

}
/*=================================================================*/
void LDDDSp_vIdSysParaCalibEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp) 
{
    /*Local*/
    LONG Det_P_mat;
    INT InvPmat[2][2];
    INT Phi_Mat[2];
    INT ldddsps16SysParaTemp[2];
//    INT ldddsps16SysParaTemp2[2];
    INT Theta_Mat[2];
//    INT Psub1_mat[2][2];
//    INT Num_Mat[2][2];
    LONG Den;
    INT Error_Term;
    INT Corr_F[2];
    INT P_mat[2][2]; 
    
    LONG SysParaCalibCalSumTemp1;
    LONG SysParaCalibCalSumTemp2;            
    LONG SysParaCalibCalSumTemp3;
    INT SysParaCalibCalMinTemp0;  
    INT SysParaCalibCalMinTemp1;    
    INT SysParaCalibCalMaxTemp0;
    INT SysParaCalibCalMaxTemp1;
    UCHAR i,buf_i;
    INT temp_cnt,TempVehSpdAvg;               
    /*A1, A2 Scale 10*/            
    ldddspu1CalCalibUpdateFlgWL=0;
//    ldddspu8VehSpdInxTemp=(vref_resol_change-40*64)/64/10;
//    if(ldddspu8VehSpdInxTemp>4){
//        ldddspu8VehSpdInxTemp=4;
//    }
//    else if( ldddspu8VehSpdInxTemp<0){
//        ldddspu8VehSpdInxTemp=0;
//    }
//    else{
//        ;
//    }
    if(DDSp_SP_WL->ldddspu1CalibCompleteFlg[ldddspu8VehSpdInxTemp]==0){
        for(inxp=0;inxp<ldddsps16WradResmpNumDeciWL;inxp++){
            if(ldddspu1CalibInitCalOkFlgWL==0){
                if(abs(ldddsps16WradZoom[WHLp][inxp])<500){
                    ldddsps16CalibInitCalOkCntWL++;
                }
                else{
                    ldddsps16CalibInitCalOkCntWL=0;
                }
                    
                if(ldddsps16CalibInitCalOkCntWL>=20){
                    ldddsps16CalibInitCalOkCntWL=0;
                    ldddspu1CalibInitCalOkFlgWL=1;
                }
                ldddsps16SysParaCalBuffer[WHLp][2]=ldddsps16SysParaCalBuffer[WHLp][1];
                ldddsps16SysParaCalBuffer[WHLp][1]=ldddsps16SysParaCalBuffer[WHLp][0];
                ldddsps16SysParaCalBuffer[WHLp][0]=ldddsps16WradZoom[WHLp][inxp];
            }
            else{
                P_mat[0][0]=ldddsps16SysParaCalBuffer[WHLp][1];
                P_mat[0][1]=ldddsps16SysParaCalBuffer[WHLp][2];
                P_mat[1][0]=ldddsps16SysParaCalBuffer[WHLp][0];
                P_mat[1][1]=ldddsps16SysParaCalBuffer[WHLp][1];
            
                tempB0=0;
                if(abs(ldddsps16SysParaCalBuffer[WHLp][0])>500)tempB0++;            
                if(abs(ldddsps16SysParaCalBuffer[WHLp][1])>500)tempB0++;
                if(abs(ldddsps16SysParaCalBuffer[WHLp][2])>500)tempB0++;
                if(abs(ldddsps16WradZoom[WHLp][inxp])>500)tempB0++; 
                
                if ((tempB0==0)&&(ldddspu1StdStrDrvStateForDDS==1)&&(vref_resol_change>(35*64))){
                    Phi_Mat[0]=-ldddsps16SysParaCalBuffer[WHLp][0];
                    Phi_Mat[1]=-ldddsps16WradZoom[WHLp][inxp];
                    
                    Det_P_mat=(((LONG)P_mat[0][0]*P_mat[1][1])-((LONG)P_mat[0][1]*P_mat[1][0]));
                    InvPmat[0][0]=P_mat[1][1];
                    InvPmat[0][1]=-P_mat[0][1];
                    InvPmat[1][0]=-P_mat[1][0];
                    InvPmat[1][1]=P_mat[0][0];
                                     
                    if(Det_P_mat!=0){
                        /*System Parameter resolution : 1000*/
                        ldddsps16SysParaTemp[0]=(INT)((((LONG)InvPmat[0][0]*Phi_Mat[0])+((LONG)InvPmat[0][1]*Phi_Mat[1]))*1000/Det_P_mat);
                        ldddsps16SysParaTemp[1]=(INT)((((LONG)InvPmat[1][0]*Phi_Mat[0])+((LONG)InvPmat[1][1]*Phi_Mat[1]))*1000/Det_P_mat);
                                           
                        if( (abs(ldddsps16SysParaTemp[0])<SYS_PARA_MAX_THR) && (abs(ldddsps16SysParaTemp[1])<SYS_PARA_MAX_THR)){
                            ldddsps16SysParaCalibCurrent[WHLp][0]+=(LONG)ldddsps16SysParaTemp[0];
                            ldddsps16SysParaCalibCurrent[WHLp][1]+=(LONG)ldddsps16SysParaTemp[1];
                            ldddsps16CalCalibInitOkCntWL++;
                            ldddsps32VelSpdSumCalib[WHLp]+=(vref_resol_change/64);
                            /*
                            tempL0=((LONG)ldddsps16SysParaCalibCurrent[WHLp][0]*ldddsps16CalCalibInitOkCntWL)+ldddsps16SysParaTemp[0]+ldddsps16SysParaCurCalibRes[WHLp][0];
                            tempL1=((LONG)ldddsps16SysParaCalibCurrent[WHLp][1]*ldddsps16CalCalibInitOkCntWL)+ldddsps16SysParaTemp[1]+ldddsps16SysParaCurCalibRes[WHLp][1];
        
                            ldddsps16SysParaTemp2[0]=(INT)(tempL0/(ldddsps16CalCalibInitOkCntWL+1));
                            ldddsps16SysParaTemp2[1]=(INT)(tempL1/(ldddsps16CalCalibInitOkCntWL+1));

                                ldddsps16SysParaCalibCurrent[WHLp][0]=ldddsps16SysParaTemp2[0];
                                ldddsps16SysParaCalibCurrent[WHLp][1]=ldddsps16SysParaTemp2[1];
        
                                ldddsps16SysParaCurCalibRes[WHLp][0]=(INT)(tempL0%(ldddsps16CalCalibInitOkCntWL+1));
                                ldddsps16SysParaCurCalibRes[WHLp][1]=(INT)(tempL1%(ldddsps16CalCalibInitOkCntWL+1));
                                ldddsps16CalCalibInitOkCntWL++; 
                                */
                        }                    
                    }
    
                    if(ldddsps16CalCalibInitOkCntWL>=1500){
                        ldddsps16SysParaTemp2[0]=(ldddsps16SysParaCalibCurrent[WHLp][0]/1500);
                        ldddsps16SysParaTemp2[1]=(ldddsps16SysParaCalibCurrent[WHLp][1]/1500);
                        ldddsps16SysParaTemp2_cal[WHLp][0]=ldddsps16SysParaTemp2[0];
                        ldddsps16SysParaTemp2_cal[WHLp][1]=ldddsps16SysParaTemp2[1];
        
                        TempVehSpdAvg=(INT)(ldddsps32VelSpdSumCalib[WHLp]/1500);

                        if(TempVehSpdAvg<50){
                            ldddspu8VehSpdInx=0;
                        }
                         else if(TempVehSpdAvg<60){
                            ldddspu8VehSpdInx=1;
                        }
                        else if(TempVehSpdAvg<70){
                            ldddspu8VehSpdInx=2;
                        }                        
                        else if(TempVehSpdAvg<80){
                            ldddspu8VehSpdInx=3;
                        }
                        else {
                            ldddspu8VehSpdInx=4;
                        }                     
                        ldddsps16SysParaCalibCurrent[WHLp][0]=0;
                        ldddsps16SysParaCalibCurrent[WHLp][1]=0;
                        ldddsps32VelSpdSumCalib[WHLp]=0;
                        ldddsps16CalCalibInitOkCntWL=0;
                        ldddspu1CalCalibUpdateFlgWL=1;
                    }
                }
                ldddsps16SysParaCalBuffer[WHLp][2]=ldddsps16SysParaCalBuffer[WHLp][1];
                ldddsps16SysParaCalBuffer[WHLp][1]=ldddsps16SysParaCalBuffer[WHLp][0];
                ldddsps16SysParaCalBuffer[WHLp][0]=ldddsps16WradZoom[WHLp][inxp];                      
            }
        }
        
        if(ldddspu1CalCalibUpdateFlgWL==1){
            buf_i=DDSp_SP_WL->ldddspu8SysParaBufCalibTimer[ldddspu8VehSpdInx];
            ldddspu8SysParaCalibBuffer[WHLp][buf_i][0][ldddspu8VehSpdInx]=ldddsps16SysParaTemp2[0];
            ldddspu8SysParaCalibBuffer[WHLp][buf_i][1][ldddspu8VehSpdInx]=ldddsps16SysParaTemp2[1];
            ldddsps16VehSpeedBufCalib[WHLp][buf_i][ldddspu8VehSpdInx]=TempVehSpdAvg;
            
            if(DDSp_SP_WL->ldddspu8SysParaBufCalibTimer[ldddspu8VehSpdInx]==9){           
                SysParaCalibCalSumTemp1=ldddspu8SysParaCalibBuffer[WHLp][0][0][ldddspu8VehSpdInx];
                SysParaCalibCalSumTemp2=ldddspu8SysParaCalibBuffer[WHLp][0][1][ldddspu8VehSpdInx];        
                
                SysParaCalibCalMinTemp0=ldddspu8SysParaCalibBuffer[WHLp][0][0][ldddspu8VehSpdInx];  
                SysParaCalibCalMinTemp1=ldddspu8SysParaCalibBuffer[WHLp][0][1][ldddspu8VehSpdInx];
                
                SysParaCalibCalMaxTemp0=ldddspu8SysParaCalibBuffer[WHLp][0][0][ldddspu8VehSpdInx];
                SysParaCalibCalMaxTemp1=ldddspu8SysParaCalibBuffer[WHLp][0][1][ldddspu8VehSpdInx];
                        
                SysParaCalibCalSumTemp3=ldddsps16VehSpeedBufCalib[WHLp][0][ldddspu8VehSpdInx];      
                for(i=1;i<10;i++){
                    SysParaCalibCalSumTemp1+=ldddspu8SysParaCalibBuffer[WHLp][i][0][ldddspu8VehSpdInx];
                    SysParaCalibCalSumTemp2+=ldddspu8SysParaCalibBuffer[WHLp][i][1][ldddspu8VehSpdInx];
                    SysParaCalibCalSumTemp3+=ldddsps16VehSpeedBufCalib[WHLp][i][ldddspu8VehSpdInx]; 
                    if(ldddspu8SysParaCalibBuffer[WHLp][i][0][ldddspu8VehSpdInx]>SysParaCalibCalMaxTemp0){
                        SysParaCalibCalMaxTemp0=ldddspu8SysParaCalibBuffer[WHLp][i][0][ldddspu8VehSpdInx];
                    }
                    else{
                        ;
                    }
                    if(ldddspu8SysParaCalibBuffer[WHLp][i][0][ldddspu8VehSpdInx]<SysParaCalibCalMinTemp0){
                        SysParaCalibCalMinTemp0=ldddspu8SysParaCalibBuffer[WHLp][i][0][ldddspu8VehSpdInx];
                    }
                    else{
                        ;
                    }
                    
                    if(ldddspu8SysParaCalibBuffer[WHLp][i][1][ldddspu8VehSpdInx]>SysParaCalibCalMaxTemp1){
                        SysParaCalibCalMaxTemp1=ldddspu8SysParaCalibBuffer[WHLp][i][1][ldddspu8VehSpdInx];
                    }
                    else{
                        ;
                    } 
                    if(ldddspu8SysParaCalibBuffer[WHLp][i][1][ldddspu8VehSpdInx]<SysParaCalibCalMinTemp1){
                        SysParaCalibCalMinTemp1=ldddspu8SysParaCalibBuffer[WHLp][i][1][ldddspu8VehSpdInx];
                    }
                    else{
                        ;
                    }            
                }
                /*Calc Cumulative Avg*/
                                     
                ldddsps16SysParaSumCalib[WHLp][0]=((SysParaCalibCalSumTemp1-SysParaCalibCalMinTemp0-SysParaCalibCalMaxTemp0)/8);
                ldddsps16SysParaSumCalib[WHLp][1]=((SysParaCalibCalSumTemp2-SysParaCalibCalMinTemp1-SysParaCalibCalMaxTemp1)/8);
                ldddsps16VehSpdCalibAvg[WHLp]=SysParaCalibCalSumTemp3/10;  
               
                
                DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]++;
                if(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]<=1){
                    ldddsps16SysParaCumulSumCalib[WHLp][0][ldddspu8VehSpdInx]=ldddsps16SysParaSumCalib[WHLp][0];
                    ldddsps16SysParaCumulSumCalib[WHLp][1][ldddspu8VehSpdInx]=ldddsps16SysParaSumCalib[WHLp][1];                    
                    ldddsps16SysParaCumulAvgCalib[WHLp][0][ldddspu8VehSpdInx]=ldddsps16SysParaSumCalib[WHLp][0];
                    ldddsps16SysParaCumulAvgCalib[WHLp][1][ldddspu8VehSpdInx]=ldddsps16SysParaSumCalib[WHLp][1];
                    ldddsps16CumulSumVehSpdCalib[WHLp][ldddspu8VehSpdInx]=ldddsps16VehSpdCalibAvg[WHLp];
                    ldddsps16CumulVehSpdCalib[WHLp][ldddspu8VehSpdInx]=ldddsps16VehSpdCalibAvg[WHLp];
                }
                else{
                    ldddsps16SysParaCumulSumCalib[WHLp][0][ldddspu8VehSpdInx]+=ldddsps16SysParaSumCalib[WHLp][0];
                    ldddsps16SysParaCumulSumCalib[WHLp][1][ldddspu8VehSpdInx]+=ldddsps16SysParaSumCalib[WHLp][1];                    
                    ldddsps16CumulSumVehSpdCalib[WHLp][ldddspu8VehSpdInx]+=ldddsps16VehSpdCalibAvg[WHLp];
                    ldddsps16SysParaCumulAvgCalib[WHLp][0][ldddspu8VehSpdInx]=(ldddsps16SysParaCumulSumCalib[WHLp][0][ldddspu8VehSpdInx])/(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);
                    ldddsps16SysParaCumulAvgCalib[WHLp][1][ldddspu8VehSpdInx]=(ldddsps16SysParaCumulSumCalib[WHLp][1][ldddspu8VehSpdInx])/(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);                   
                    ldddsps16CumulVehSpdCalib[WHLp][ldddspu8VehSpdInx]=(ldddsps16CumulSumVehSpdCalib[WHLp][ldddspu8VehSpdInx])/(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);
                }
                          
                DDSp_SP_WL->ldddsps16TireFreqCalib[ldddspu8VehSpdInx]=LDDDSp_vConvSysPara2Freq(ldddsps16SysParaCumulAvgCalib[WHLp][0][ldddspu8VehSpdInx],ldddsps16SysParaCumulAvgCalib[WHLp][1][ldddspu8VehSpdInx]);
                //DDSp_SP_WL->ldddspu16DetCalibCompleteCnt[ldddspu8VehSpdInx]++;
                if(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]>CALIB_COMPLETE_TIME){
                    DDSp_SP_WL->ldddspu1CalibCompleteFlg[ldddspu8VehSpdInx]=1;
                    DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]=0;
                }   
            }

            DDSp_SP_WL->ldddspu8SysParaBufCalibTimer[ldddspu8VehSpdInx]++;
            if(DDSp_SP_WL->ldddspu8SysParaBufCalibTimer[ldddspu8VehSpdInx]>=10)DDSp_SP_WL->ldddspu8SysParaBufCalibTimer[ldddspu8VehSpdInx]=0;            
        }   
    }         
}
/*=================================================================*/
INT LDDDSp_vConvSysPara2Freq(INT SysPara1,INT SysPara2)
{
//    LONG Num,Den;
    INT Freq_Temp,Freq_Det; 

    INT FreqInputArray[12]={25,30,48,89,119,193,302,375,456,522,568,617};
    INT FreqOutputArray[12]={100,110,140,190,220,280,350,390,430,460,480,500};

    
    /*a1,a2--> Wres*/
//    Num=(LONG)(SysPara1+SysPara2+1000)*1000;
//    Den=(LONG)(1000-SysPara1+SysPara2);

    Freq_Temp=(INT)(((LONG)(SysPara1+SysPara2+1000)*1000)/(1000-SysPara1+SysPara2));
    if(Freq_Temp<FreqInputArray[0]){
        Freq_Det=100;//MIN FREQ
    }
    else if(Freq_Temp<FreqInputArray[1]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[1]-FreqOutputArray[0])*(Freq_Temp-FreqInputArray[0])/(FreqInputArray[1]-FreqInputArray[0]))+FreqOutputArray[0];
    }
    else if(Freq_Temp<FreqInputArray[2]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[2]-FreqOutputArray[1])*(Freq_Temp-FreqInputArray[1])/(FreqInputArray[2]-FreqInputArray[1]))+FreqOutputArray[1];
    }
    else if(Freq_Temp<FreqInputArray[3]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[3]-FreqOutputArray[2])*(Freq_Temp-FreqInputArray[2])/(FreqInputArray[3]-FreqInputArray[2]))+FreqOutputArray[2];
    }
    else if(Freq_Temp<FreqInputArray[4]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[4]-FreqOutputArray[3])*(Freq_Temp-FreqInputArray[3])/(FreqInputArray[4]-FreqInputArray[3]))+FreqOutputArray[3];
    }
    else if(Freq_Temp<FreqInputArray[5]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[5]-FreqOutputArray[4])*(Freq_Temp-FreqInputArray[4])/(FreqInputArray[5]-FreqInputArray[4]))+FreqOutputArray[4];
    }
    else if(Freq_Temp<FreqInputArray[6]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[6]-FreqOutputArray[5])*(Freq_Temp-FreqInputArray[5])/(FreqInputArray[6]-FreqInputArray[5]))+FreqOutputArray[5];
    }
    else if(Freq_Temp<FreqInputArray[7]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[7]-FreqOutputArray[6])*(Freq_Temp-FreqInputArray[6])/(FreqInputArray[7]-FreqInputArray[6]))+FreqOutputArray[6];
    }
    else if(Freq_Temp<FreqInputArray[8]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[8]-FreqOutputArray[7])*(Freq_Temp-FreqInputArray[7])/(FreqInputArray[8]-FreqInputArray[7]))+FreqOutputArray[7];
    }    
    else if(Freq_Temp<FreqInputArray[9]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[9]-FreqOutputArray[8])*(Freq_Temp-FreqInputArray[8])/(FreqInputArray[9]-FreqInputArray[8]))+FreqOutputArray[8];
    }
    else if(Freq_Temp<FreqInputArray[10]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[10]-FreqOutputArray[9])*(Freq_Temp-FreqInputArray[9])/(FreqInputArray[10]-FreqInputArray[9]))+FreqOutputArray[9];
    }
    else if(Freq_Temp<FreqInputArray[11]){
        Freq_Det=(INT)((LONG)(FreqOutputArray[11]-FreqOutputArray[10])*(Freq_Temp-FreqInputArray[10])/(FreqInputArray[11]-FreqInputArray[10]))+FreqOutputArray[10];
    }     
//    else if(Freq_Temp<FreqInputArray[12]){
//        Freq_Det=(INT)((LONG)(FreqOutputArray[12]-FreqOutputArray[11])*(Freq_Temp-FreqInputArray[11])/(FreqInputArray[12]-FreqInputArray[11]))+FreqOutputArray[11];
//    }       
    else{
        Freq_Det=500;//MAX FREQ
    }
    return Freq_Det;
    
}
/*=================================================================*/
void LDDDSp_vIndentifySysParaDefDct(void)
{
        /*Calc if only Teeth Devi & Teeth Inx Det*/
    TEETH_NUM=U8_TEETH;
    LDDDSp_vIdSysParaDefDctEachWL(&DDSp_SP_FL,FL_WL);
    LDDDSp_vIdSysParaDefDctEachWL(&DDSp_SP_FR,FR_WL);

    if (U8_F_R_DIFF==1)TEETH_NUM=U8_TEETH_R;
    LDDDSp_vIdSysParaDefDctEachWL(&DDSp_SP_RL,RL_WL);
    LDDDSp_vIdSysParaDefDctEachWL(&DDSp_SP_RR,RR_WL);
        
}
/*=================================================================*/
void LDDDSp_vIdSysParaDefDctEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp)
{
    /*Local*/
    LONG Det_P_mat;
    INT P_mat[2][2];
    INT InvPmat[2][2];
    INT Phi_Mat[2];
    INT ldddsps16SysParaTemp[2];
    INT ldddsps16SysParaTemp3;
//    INT ldddsps16SysParaTemp2[2];
    INT Theta_Mat[2];
    LONG Den;
    INT Error_Term;
    LONG SysParaDefDctCalSumTemp1;
    LONG SysParaDefDctCalSumTemp2;  
    LONG SysParaDefDctCalSumTemp3; 
    INT SysParaDefDctCalMinTemp0;  
    INT SysParaDefDctCalMinTemp1;
    INT SysParaDefDctCalMaxTemp0;  
    INT SysParaDefDctCalMaxTemp1;
         
     
    UCHAR i,buf_i;
    INT TempVehSpdAvg;  
    INT VrefKph;
    UCHAR spd_i;
    INT ldddsps16TireFreqDefDctTemp;
    /*A1, A2 Scale 10*/            
    ldddspu1CalDefDctUpdateFlgWL=0;
    ldddspu1CalDefDctUpdateFlg2WL=0;
    for(inxp=0;inxp<ldddsps16WradResmpNumDeciWL;inxp++){
        if(ldddspu1DefDctInitCalOkFlgWL==0){
            if(abs(ldddsps16WradZoom[WHLp][inxp])<500){
                ldddsps16DefDctInitCalOkCntWL++;
            }
            else{
                ldddsps16DefDctInitCalOkCntWL=0;
            }
                
            if(ldddsps16DefDctInitCalOkCntWL>=20){
                ldddsps16DefDctInitCalOkCntWL=0;
                ldddspu1DefDctInitCalOkFlgWL=1;
            }
            ldddsps16SysParaDefCalBuffer[WHLp][2]=ldddsps16SysParaDefCalBuffer[WHLp][1];
            ldddsps16SysParaDefCalBuffer[WHLp][1]=ldddsps16SysParaDefCalBuffer[WHLp][0];
            ldddsps16SysParaDefCalBuffer[WHLp][0]=ldddsps16WradZoom[WHLp][inxp];
        }
        else{
            P_mat[0][0]=ldddsps16SysParaDefCalBuffer[WHLp][1];
            P_mat[0][1]=ldddsps16SysParaDefCalBuffer[WHLp][2];
            P_mat[1][0]=ldddsps16SysParaDefCalBuffer[WHLp][0];
            P_mat[1][1]=ldddsps16SysParaDefCalBuffer[WHLp][1];

            tempB0=0;
            if(abs(ldddsps16SysParaDefCalBuffer[WHLp][0])>500)tempB0++;            
            if(abs(ldddsps16SysParaDefCalBuffer[WHLp][1])>500)tempB0++;
            if(abs(ldddsps16SysParaDefCalBuffer[WHLp][2])>500)tempB0++;
            if(abs(ldddsps16WradZoom[WHLp][inxp])>500)tempB0++; 
            
            if ((tempB0==0)&&(ldddspu1StdStrDrvStateForDDS==1)){
                Phi_Mat[0]=-ldddsps16SysParaDefCalBuffer[WHLp][0];
                Phi_Mat[1]=-ldddsps16WradZoom[WHLp][inxp];
                
                Det_P_mat=(((LONG)P_mat[0][0]*P_mat[1][1])-((LONG)P_mat[0][1]*P_mat[1][0]));
                InvPmat[0][0]=P_mat[1][1];
                InvPmat[0][1]=-P_mat[0][1];
                InvPmat[1][0]=-P_mat[1][0];
                InvPmat[1][1]=P_mat[0][0];
                                 
                if(Det_P_mat!=0){
                    /*System Parameter resolution : 1000*/
                    ldddsps16SysParaTemp[0]=(INT)((((LONG)InvPmat[0][0]*Phi_Mat[0])+((LONG)InvPmat[0][1]*Phi_Mat[1]))*1000/Det_P_mat);
                    ldddsps16SysParaTemp[1]=(INT)((((LONG)InvPmat[1][0]*Phi_Mat[0])+((LONG)InvPmat[1][1]*Phi_Mat[1]))*1000/Det_P_mat);
                                       
                    if( (abs(ldddsps16SysParaTemp[0])<SYS_PARA_MAX_THR) && (abs(ldddsps16SysParaTemp[1])<SYS_PARA_MAX_THR)){
                        ldddsps16SysParaDefCurrent[WHLp][0]+=ldddsps16SysParaTemp[0];
                        ldddsps16SysParaDefCurrent[WHLp][1]+=ldddsps16SysParaTemp[1];
                        ldddsps16SysParaDefDctCalCntWL++;
                        ldddsps32VelSpdSumDct[WHLp]+=(vref_resol_change/64);
                        /*
                        tempL0=((LONG)ldddsps16SysParaDefCurrent[WHLp][0]*ldddsps16SysParaDefDctCalCntWL)+ldddsps16SysParaTemp[0]+ldddsps16SysParaCurDefDctRes[WHLp][0];
                        tempL1=((LONG)ldddsps16SysParaDefCurrent[WHLp][1]*ldddsps16SysParaDefDctCalCntWL)+ldddsps16SysParaTemp[1]+ldddsps16SysParaCurDefDctRes[WHLp][1];
                        
                        ldddsps16SysParaDefCurrent[WHLp][0]=(INT)(tempL0/(ldddsps16SysParaDefDctCalCntWL+1));
                        ldddsps16SysParaDefCurrent[WHLp][1]=(INT)(tempL1/(ldddsps16SysParaDefDctCalCntWL+1));

                        ldddsps16SysParaCurDefDctRes[WHLp][0]=(INT)(tempL0%(ldddsps16SysParaDefDctCalCntWL+1));
                        ldddsps16SysParaCurDefDctRes[WHLp][1]=(INT)(tempL1%(ldddsps16SysParaDefDctCalCntWL+1));
                        ldddsps16SysParaDefDctCalCntWL++;
                        */ 
                    }                    
                }

                if(ldddsps16SysParaDefDctCalCntWL>=1500){
    
                    ldddsps16SysParaTemp2[0]=(INT)(ldddsps16SysParaDefCurrent[WHLp][0]/1500);
                    ldddsps16SysParaTemp2[1]=(INT)(ldddsps16SysParaDefCurrent[WHLp][1]/1500);
                    ldddsps16TireFreqDefDctTemp=LDDDSp_vConvSysPara2Freq(ldddsps16SysParaTemp2[0],ldddsps16SysParaTemp2[1]);                 
                    DDSp_SP_WL->ldddsps16TireFreqDefDct=ldddsps16TireFreqDefDctTemp;
                    TempVehSpdAvg=(INT)(ldddsps32VelSpdSumDct[WHLp]/1500);
                    
                    if(TempVehSpdAvg<50){
                        ldddspu8VehSpdInx=0;
                    }
                     else if(TempVehSpdAvg<60){
                        ldddspu8VehSpdInx=1;
                    }
                    else if(TempVehSpdAvg<70){
                        ldddspu8VehSpdInx=2;
                    }                        
                    else if(TempVehSpdAvg<80){
                        ldddspu8VehSpdInx=3;
                    }
                    else {
                        ldddspu8VehSpdInx=4;
                    }    
                        
                    ldddsps16SysParaDefCurrent[WHLp][0]=0;
                    ldddsps16SysParaDefCurrent[WHLp][1]=0;
                    ldddsps32VelSpdSumDct[WHLp]=0;
                    ldddsps16SysParaDefDctCalCntWL=0;
                    ldddspu1CalDefDctUpdateFlgWL=1;
                }
            }
            ldddsps16SysParaDefCalBuffer[WHLp][2]=ldddsps16SysParaDefCalBuffer[WHLp][1];
            ldddsps16SysParaDefCalBuffer[WHLp][1]=ldddsps16SysParaDefCalBuffer[WHLp][0];
            ldddsps16SysParaDefCalBuffer[WHLp][0]=ldddsps16WradZoom[WHLp][inxp];                      
        }
    }
    
    if(ldddspu1CalDefDctUpdateFlgWL==1){
        VrefKph=TempVehSpdAvg;
    	spd_i=ldddspu8VehSpdInx;
        if(DDSp_SP_WL->ldddspu1CalibCompleteFlg[spd_i]==1){
        
    	    if(ldddsps16CumulVehSpdCalib[WHLp][spd_i]>VrefKph){
    	        if(spd_i==0){
    	            DDSp_SP_WL->ldddsps16TireFreqCalibCur=DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i];
    	        }
    	        else if(DDSp_SP_WL->ldddspu1CalibCompleteFlg[spd_i-1]==1){
                    DDSp_SP_WL->ldddsps16TireFreqCalibCur=LCESP_s16IInter2Point(VrefKph,ldddsps16CumulVehSpdCalib[WHLp][spd_i-1],DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i-1]
                    ,ldddsps16CumulVehSpdCalib[WHLp][spd_i],DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i]);	            
    	        }
    	        else{
    	            DDSp_SP_WL->ldddsps16TireFreqCalibCur=DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i];
    	        }
    	    }
    	    else if(ldddsps16CumulVehSpdCalib[WHLp][spd_i]<VrefKph){
    	        if(spd_i==4){
    	            DDSp_SP_WL->ldddsps16TireFreqCalibCur=DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i];
    	        }
    	        else if(DDSp_SP_WL->ldddspu1CalibCompleteFlg[spd_i+1]==1){
                    DDSp_SP_WL->ldddsps16TireFreqCalibCur=LCESP_s16IInter2Point(VrefKph,ldddsps16CumulVehSpdCalib[WHLp][spd_i],DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i]
                    ,ldddsps16CumulVehSpdCalib[WHLp][spd_i+1],DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i+1]);	            
    	        }
    	        else{
    	            DDSp_SP_WL->ldddsps16TireFreqCalibCur=DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i];
    	        }	        
    	    }
    	    else{
    	        DDSp_SP_WL->ldddsps16TireFreqCalibCur=DDSp_SP_WL->ldddsps16TireFreqCalib[spd_i];
    	    }
    	}
        buf_i=ldddspu8SysParaBufDctTimerWL;
        ldddspu8SysParaBufDefDct[WHLp][buf_i]=DDSp_SP_WL->ldddsps16TireFreqCalibCur-ldddsps16TireFreqDefDctTemp;
        ldddspu8SysParaBufDctTimerWL++;
        if(ldddspu8SysParaBufDctTimerWL>=5){
            ldddspu1CalDefDctUpdateFlg2WL=1;
            ldddspu8SysParaBufDctTimerWL=0;
              /*Calc Moving Avg*/            
            SysParaDefDctCalSumTemp3=ldddspu8SysParaBufDefDct[WHLp][0];
            
            
            SysParaDefDctCalMinTemp0=ldddspu8SysParaBufDefDct[WHLp][0];  
            SysParaDefDctCalMaxTemp0=ldddspu8SysParaBufDefDct[WHLp][0];  
                    
            for(i=1;i<5;i++){
                
                SysParaDefDctCalSumTemp3+=ldddspu8SysParaBufDefDct[WHLp][i];
                if(ldddspu8SysParaBufDefDct[WHLp][i]>SysParaDefDctCalMaxTemp0){
                    SysParaDefDctCalMaxTemp0=ldddspu8SysParaBufDefDct[WHLp][i];
                }
                else{
                    ;
                } 
                if(ldddspu8SysParaBufDefDct[WHLp][i]<SysParaDefDctCalMinTemp0){
                    SysParaDefDctCalMinTemp0=ldddspu8SysParaBufDefDct[WHLp][i];
                }
                else{
                    ;
                }            
            }
            ldddsps16SysParaTemp3=(INT)((SysParaDefDctCalSumTemp3-SysParaDefDctCalMinTemp0-SysParaDefDctCalMaxTemp0)/(5-2));
            
            SysParaDefDctCalSumTemp3=ldddsps16SysParaTemp3;
            for(i=4;i>0;i--){

                ldddspu8SysParaBufDefDct2[WHLp][i]=ldddspu8SysParaBufDefDct2[WHLp][i-1]; 
                SysParaDefDctCalSumTemp3+=ldddspu8SysParaBufDefDct2[WHLp][i];         
            }                    
            ldddspu8SysParaBufDefDct2[WHLp][0]= ldddsps16SysParaTemp3; 
            ldddsps16SysParaDefInitCalCntWL++;
            if(ldddsps16SysParaDefInitCalCntWL>5){
                ldddsps16SysParaDefInitCalCntWL=5;
            }
            if(ldddsps16SysParaDefInitCalCntWL<5){
                 ldddsps16SysParaDefDct[WHLp]=0;
                 ldddsps16TireFreqDiffWL=0;
            }
            else{
                ldddsps16SysParaDefDct[WHLp]=(INT)(SysParaDefDctCalSumTemp3/5);
                ldddsps16TireFreqDiffWL=ldddsps16SysParaDefDct[WHLp];
            } 
             
        }        
    }    
            
}
/*=================================================================*/
void LDDDSp_vDetTireDeflationState(void)
{
    
    TEETH_NUM=U8_TEETH;
    LDDDSp_vDetTireDefStateEachWL(&DDSp_SP_FL,FL_WL,10,5,100);
    LDDDSp_vDetTireDefStateEachWL(&DDSp_SP_FR,FR_WL,10,5,100);

    if (U8_F_R_DIFF==1)TEETH_NUM=U8_TEETH_R;
    LDDDSp_vDetTireDefStateEachWL(&DDSp_SP_RL,RL_WL,7,4,100);
    LDDDSp_vDetTireDefStateEachWL(&DDSp_SP_RR,RR_WL,7,4,100);
   /* 
    if((ldddspu1LowTirePressFlgFL==1)|| (ldddspu1LowTirePressFlgFR==1)||(ldddspu1LowTirePressFlgRL==1)||(ldddspu1LowTirePressFlgRR==1)){
        ldddsu1DeflationState=1;
    }
    else{
        ldddsu1DeflationState=0;
    }
    */    
}
/*=================================================================*/
void LDDDSp_vDetTireDefStateEachWL(DDSp_SP_t *DDSp_SP_WL,UCHAR WHLp,CHAR DCT_THR,CHAR RESET_THR,CHAR MAX_THR)
{

//    ldddsps16TireFreqDiffWL=ldddsps16TireFreqCalibWL-ldddsps16TireFreqDefDctWL;
    if(ldddsps16SysParaDefInitCalCntWL<5){
        ldddsps16TireFreqDiffWL=0;
    }
    if(ldddspu1DeflationStateFlgWL==0){
        if((ldddspu1CalDefDctUpdateFlg2WL==1)&&(ldddsps16SysParaDefInitCalCntWL>=5)){
            if(ldddsps16TireFreqDiffWL>MAX_THR){
                ;
            }
            else if(ldddsps16TireFreqDiffWL>DCT_THR){
                ldddsps16DefDctCntWL++;
                if(ldddsps16TireFreqDiffWL>((INT)DCT_THR*15/10)){
                    ldddsps16DefDctCntWL++;
                }
                if(ldddsps16DefDctCntWL>TIRE_LOW_PRESSURE_DET_TIME){/*Cnt 1--> 2.5s*/
                    ldddspu1DeflationStateFlgWL=1;
                    ldddsps16DefDctCntWL=0;
                }
            }
            else if(ldddsps16TireFreqDiffWL>RESET_THR){
                ;
            }        
            else if((ldddsps16TireFreqDiffWL>0)&&(ldddsps16DefDctCntWL>0)){
                ldddsps16DefDctCntWL--;
            }
            else if(ldddsps16TireFreqDiffWL<(-RESET_THR)){
                ldddsps16DefDctCntWL=0;
            }
            else if(ldddsps16DefDctCntWL>3){
                ldddsps16DefDctCntWL-=3;
            }
            else{
                ;
            }
        }
        else{
            ;
        }
    }
    else{
        ldddsps16DefDctCntWL=0;
    }
}
/*=================================================================*/
void LDDDSp_vDetDdsPlusRunningReset(void)
{
    /*
    if((ldddspu1LowTirePressFlgFL==1)|| (ldddspu1LowTirePressFlgFR==1)||(ldddspu1LowTirePressFlgRL==1)||(ldddspu1LowTirePressFlgRR==1)){
        ldddsu1DeflationState=1;
    }
    else{
        ldddsu1DeflationState=0;
    }
    */
}
/*=================================================================*/
#if defined __MATLAB_COMPILE
#else
void LDDDSp_vLoadDDSpEepromData(void)
{
/*    
	if((ddsp_eep_read_ok_flg01==0)||(ddsp_eep_read_ok_flg02==0)||(ddsp_eep_read_ok_flg03==0)||(ddsp_eep_read_ok_flg04==0)||
    (ddsp_eep_read_ok_flg05==0)||(ddsp_eep_read_ok_flg06==0)||(ddsp_eep_read_ok_flg07==0)||(ddsp_eep_read_ok_flg08==0)){
	    read_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_read_ok_flg09==0)||(ddsp_eep_read_ok_flg10==0)||(ddsp_eep_read_ok_flg11==0)||(ddsp_eep_read_ok_flg12==0)||
    (ddsp_eep_read_ok_flg13==0)||(ddsp_eep_read_ok_flg14==0)||(ddsp_eep_read_ok_flg15==0)||(ddsp_eep_read_ok_flg16==0)){
	    read_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_read_ok_flg17==0)||(ddsp_eep_read_ok_flg18==0)||(ddsp_eep_read_ok_flg19==0)||(ddsp_eep_read_ok_flg20==0)||
    (ddsp_eep_read_ok_flg21==0)||(ddsp_eep_read_ok_flg22==0)||(ddsp_eep_read_ok_flg23==0)||(ddsp_eep_read_ok_flg24==0)){
	    read_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_read_ok_flg25==0)||(ddsp_eep_read_ok_flg26==0)||(ddsp_eep_read_ok_flg27==0)||(ddsp_eep_read_ok_flg28==0)||
    (ddsp_eep_read_ok_flg29==0)||(ddsp_eep_read_ok_flg30==0)||(ddsp_eep_read_ok_flg31==0)||(ddsp_eep_read_ok_flg32==0)){
	    read_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_read_ok_flg33==0)||(ddsp_eep_read_ok_flg34==0)||(ddsp_eep_read_ok_flg35==0)||(ddsp_eep_read_ok_flg36==0)||
    (ddsp_eep_read_ok_flg37==0)||(ddsp_eep_read_ok_flg38==0)||(ddsp_eep_read_ok_flg39==0)||(ddsp_eep_read_ok_flg40==0)){
	    read_ddsp_eeprom_end_flg=0;
	}	
	else if(ddsp_eep_read_ok_flg41==0){
	    read_ddsp_eeprom_end_flg=0;
	}						
	else{
	    read_ddsp_eeprom_end_flg=1;
	}

	
	if(read_ddsp_eeprom_end_flg==0){
		if(ddsp_eep_read_ok_flg01==0){
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_01,&com_rx_buf[0],4)==1){  
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg01=1;
	        }
	        else {
	        	;
	        }
	    }
		if(ddsp_eep_read_ok_flg02==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_02,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	DDSp_SP_FL.ldddsps16TireFreqCalib = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg02=1;
	        }
	        else{
	        	;
	        }	        
	    }
		if(ddsp_eep_read_ok_flg03==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_03,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FL.ldddspu16DetCalibCompleteCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg03=1;
	        }
	        else{
	            ;
	        }
	    }
		if(ddsp_eep_read_ok_flg04==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_04,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FL.ldddspu1CalibCompleteFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg04=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg05==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_05,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg05=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg06==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_06,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddsps16TireFreqCalib = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg06=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg07==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_07,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddspu16DetCalibCompleteCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg07=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg08==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_08,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddspu1CalibCompleteFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg08=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg09==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_09,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg09=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg10==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_10,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RL.ldddsps16TireFreqCalib = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg10=1;
	        }
	        else{
	            ;
	        }
	    }	
	    
		if(ddsp_eep_read_ok_flg11==0){
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_11,&com_rx_buf[0],4)==1){  
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	DDSp_SP_RL.ldddspu16DetCalibCompleteCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg11=1;
	        }
	        else {
	        	;
	        }
	    }
		if(ddsp_eep_read_ok_flg12==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_12,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	DDSp_SP_RL.ldddspu1CalibCompleteFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg12=1;
	        }
	        else{
	        	;
	        }	        
	    }
		if(ddsp_eep_read_ok_flg13==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_13,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg13=1;
	        }
	        else{
	            ;
	        }
	    }
		if(ddsp_eep_read_ok_flg14==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_14,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddsps16TireFreqCalib = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg14=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg15==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_15,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddspu16DetCalibCompleteCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg15=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg16==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_16,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddspu1CalibCompleteFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg16=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg17==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_17,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[0][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg17=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg18==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_18,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[1][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg18=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg19==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_19,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[2][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg19=1;
	        }
	        else{
	            ;
	        }
	    }	
	    
	    if(ddsp_eep_read_ok_flg20==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_20,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[3][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg20=1;
	        }
	        else{
	            ;
	        }
	    }
	    if(ddsp_eep_read_ok_flg21==0){
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_21,&com_rx_buf[0],4)==1){  
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	ldddsps16SysParaCumulAvgCalib[0][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg21=1;
	        }
	        else {
	        	;
	        }
	    }
		if(ddsp_eep_read_ok_flg22==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_22,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	ldddsps16SysParaCumulAvgCalib[1][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg22=1;
	        }
	        else{
	        	;
	        }	        
	    }
		if(ddsp_eep_read_ok_flg23==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_23,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[2][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg23=1;
	        }
	        else{
	            ;
	        }
	    }
		if(ddsp_eep_read_ok_flg24==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_24,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulAvgCalib[3][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg24=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg25==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_25,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[0][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg25=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg26==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_26,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[1][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg26=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg27==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_27,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[2][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg27=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg28==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_28,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[3][0] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg28=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg29==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_29,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[0][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg29=1;
	        }
	        else{
	            ;
	        }
	    }	

	    if(ddsp_eep_read_ok_flg30==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_30,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddsps16SysParaCumulResCalib[1][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg30=1;
	        }
	        else{
	            ;
	        }
	    }
	    if(ddsp_eep_read_ok_flg31==0){
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_31,&com_rx_buf[0],4)==1){  
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	ldddsps16SysParaCumulResCalib[2][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg31=1;
	        }
	        else {
	        	;
	        }
	    }
		if(ddsp_eep_read_ok_flg32==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_32,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff){
	            	ldddsps16SysParaCumulResCalib[3][1] = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg32=1;
	        }
	        else{
	        	;
	        }	        
	    }
		if(ddsp_eep_read_ok_flg33==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_33,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FL.ldddsps16DefDctCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg33=1;
	        }
	        else{
	            ;
	        }
	    }
		if(ddsp_eep_read_ok_flg34==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_34,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FL.ldddspu1LowTirePressFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg34=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg35==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_35,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddsps16DefDctCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg35=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg36==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_36,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_FR.ldddspu1LowTirePressFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg36=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg37==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_37,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RL.ldddsps16DefDctCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg37=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg38==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_38,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RL.ldddspu1LowTirePressFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg38=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg39==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_39,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddsps16DefDctCnt = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg39=1;
	        }
	        else{
	            ;
	        }
	    }	
	    if(ddsp_eep_read_ok_flg40==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_40,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	DDSp_SP_RR.ldddspu1LowTirePressFlg = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg40=1;
	        }
	        else{
	            ;
	        }
	    }
	    if(ddsp_eep_read_ok_flg41==0){		
			if(WE_u8ReadEEP_Byte(U16_DDSp_EERPOM_DATA_41,&com_rx_buf[0],4)==1){ 
	        	if((*(UINT*)&com_rx_buf[0]+*(UINT*)&com_rx_buf[2])==0xffff) {
	            	ldddspu8DdsPlusModeEepromValue = *(INT*)&com_rx_buf[0];	
	            }
	            ddsp_eep_read_ok_flg41=1;
	        }
	        else{
	            ;
	        }
	    }	    
	    	    	    		    
	}
*/	       
}
/*=================================================================*/
void LDDDSp_vWriteDDSpParameters(void)
{

/* TBD
ldddsps16WhlTeethDeviationF[4][50];
ldddsps16WhlTeethDeviationMod[4][50];
ldddspu16TeethDeviCalcCnt;
ldddspu1WheelTeethCalcOk: 1;
INT ldddsps16SysParaDefInitCalCnt;
INT ldddsps16SysParaAvgDefBuffer[4][10][2];
INT ldddsps16SysParaMovingAvgDef[4][2];
*/
/*                
    if(ldddspu8DdsPlusMode!=FUNCTION_IGNON)
	{
		ldddspu8DdsPlusModeEepromValue=ldddspu8DdsPlusMode;
	}

    if((ddsp_eep_wr_ok_flg01==0)||(ddsp_eep_wr_ok_flg02==0)||(ddsp_eep_wr_ok_flg03==0)||(ddsp_eep_wr_ok_flg04==0)||
    (ddsp_eep_wr_ok_flg05==0)||(ddsp_eep_wr_ok_flg06==0)||(ddsp_eep_wr_ok_flg07==0)||(ddsp_eep_wr_ok_flg08==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg09==0)||(ddsp_eep_wr_ok_flg10==0)||(ddsp_eep_wr_ok_flg11==0)||(ddsp_eep_wr_ok_flg12==0)||
    (ddsp_eep_wr_ok_flg13==0)||(ddsp_eep_wr_ok_flg14==0)||(ddsp_eep_wr_ok_flg15==0)||(ddsp_eep_wr_ok_flg16==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg17==0)||(ddsp_eep_wr_ok_flg18==0)||(ddsp_eep_wr_ok_flg19==0)||(ddsp_eep_wr_ok_flg20==0)||
    (ddsp_eep_wr_ok_flg21==0)||(ddsp_eep_wr_ok_flg22==0)||(ddsp_eep_wr_ok_flg23==0)||(ddsp_eep_wr_ok_flg24==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg25==0)||(ddsp_eep_wr_ok_flg26==0)||(ddsp_eep_wr_ok_flg27==0)||(ddsp_eep_wr_ok_flg28==0)||
    (ddsp_eep_wr_ok_flg29==0)||(ddsp_eep_wr_ok_flg30==0)||(ddsp_eep_wr_ok_flg31==0)||(ddsp_eep_wr_ok_flg32==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg33==0)||(ddsp_eep_wr_ok_flg34==0)||(ddsp_eep_wr_ok_flg35==0)||(ddsp_eep_wr_ok_flg36==0)||
    (ddsp_eep_wr_ok_flg37==0)||(ddsp_eep_wr_ok_flg38==0)||(ddsp_eep_wr_ok_flg39==0)||(ddsp_eep_wr_ok_flg40==0)){
	    write_ddsp_eeprom_end_flg=0;
	}	
	else if(ddsp_eep_wr_ok_flg41==0){
	    write_ddsp_eeprom_end_flg=0;
	}							
	else{
	    write_ddsp_eeprom_end_flg=1;
	}
	
	if(write_ddsp_eeprom_end_flg==0)
	{	
		if((weu1EraseSector==0)&&(weu1WriteSector==0)){
			if(ddsp_eep_wr_ok_flg01==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_01,(UINT)DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt)==0){
					ddsp_eep_wr_ok_flg01=1;
				}
				else{
					;
				}
			}	
			else if(ddsp_eep_wr_ok_flg02==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_02,(UINT)DDSp_SP_FL.ldddsps16TireFreqCalib)==0){
					ddsp_eep_wr_ok_flg02=1;
				}
			}		
			else if(ddsp_eep_wr_ok_flg03==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_03,(UINT)DDSp_SP_FL.ldddspu16DetCalibCompleteCnt)==0){
					ddsp_eep_wr_ok_flg03=1;
				}
			}	
			else if(ddsp_eep_wr_ok_flg04==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_04,(UINT)DDSp_SP_FL.ldddspu1CalibCompleteFlg)==0){
					ddsp_eep_wr_ok_flg04=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg05==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_05,(UINT)DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt)==0){
					ddsp_eep_wr_ok_flg05=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg06==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_06,(UINT)DDSp_SP_FR.ldddsps16TireFreqCalib)==0){
					ddsp_eep_wr_ok_flg06=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg07==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_07,(UINT)DDSp_SP_FR.ldddspu16DetCalibCompleteCnt)==0){
					ddsp_eep_wr_ok_flg07=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg08==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_08,(UINT)DDSp_SP_FR.ldddspu1CalibCompleteFlg)==0){
					ddsp_eep_wr_ok_flg08=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg09==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_09,(UINT)DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt)==0){
					ddsp_eep_wr_ok_flg09=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg10==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_10,(UINT)DDSp_SP_RL.ldddsps16TireFreqCalib)==0){
					ddsp_eep_wr_ok_flg10=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg11==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_11,(UINT)DDSp_SP_RL.ldddspu16DetCalibCompleteCnt)==0){
					ddsp_eep_wr_ok_flg11=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg12==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_12,(UINT)DDSp_SP_RL.ldddspu1CalibCompleteFlg)==0){
					ddsp_eep_wr_ok_flg12=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg13==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_13,(UINT)DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt)==0){
					ddsp_eep_wr_ok_flg13=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg14==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_14,(UINT)DDSp_SP_RR.ldddsps16TireFreqCalib)==0){
					ddsp_eep_wr_ok_flg14=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg15==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_15,(UINT)DDSp_SP_RR.ldddspu16DetCalibCompleteCnt)==0){
					ddsp_eep_wr_ok_flg15=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg16==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_16,(UINT)DDSp_SP_RR.ldddspu1CalibCompleteFlg)==0){
					ddsp_eep_wr_ok_flg16=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg17==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_17,(UINT)ldddsps16SysParaCumulAvgCalib[0][0])==0){
					ddsp_eep_wr_ok_flg17=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg18==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_18,(UINT)ldddsps16SysParaCumulAvgCalib[1][0])==0){
					ddsp_eep_wr_ok_flg18=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg19==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_19,(UINT)ldddsps16SysParaCumulAvgCalib[2][0])==0){
					ddsp_eep_wr_ok_flg19=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg20==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_20,(UINT)ldddsps16SysParaCumulAvgCalib[3][0])==0){
					ddsp_eep_wr_ok_flg20=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg21==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_21,(UINT)ldddsps16SysParaCumulAvgCalib[0][1])==0){
					ddsp_eep_wr_ok_flg21=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg22==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_22,(UINT)ldddsps16SysParaCumulAvgCalib[1][1])==0){
					ddsp_eep_wr_ok_flg22=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg23==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_23,(UINT)ldddsps16SysParaCumulAvgCalib[2][1])==0){
					ddsp_eep_wr_ok_flg23=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg24==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_24,(UINT)ldddsps16SysParaCumulAvgCalib[3][1])==0){
					ddsp_eep_wr_ok_flg24=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg25==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_25,(UINT)ldddsps16SysParaCumulResCalib[0][0])==0){
					ddsp_eep_wr_ok_flg25=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg26==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_26,(UINT)ldddsps16SysParaCumulResCalib[1][0])==0){
					ddsp_eep_wr_ok_flg26=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg27==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_27,(UINT)ldddsps16SysParaCumulResCalib[2][0])==0){
					ddsp_eep_wr_ok_flg27=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg28==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_28,(UINT)ldddsps16SysParaCumulResCalib[3][0])==0){
					ddsp_eep_wr_ok_flg28=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg29==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_29,(UINT)ldddsps16SysParaCumulResCalib[0][1])==0){
					ddsp_eep_wr_ok_flg29=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg30==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_30,(UINT)ldddsps16SysParaCumulResCalib[1][1])==0){
					ddsp_eep_wr_ok_flg30=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg31==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_31,(UINT)ldddsps16SysParaCumulResCalib[2][1])==0){
					ddsp_eep_wr_ok_flg31=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg32==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_32,(UINT)ldddsps16SysParaCumulResCalib[3][1])==0){
					ddsp_eep_wr_ok_flg32=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg33==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_33,(UINT)DDSp_SP_FL.ldddsps16DefDctCnt)==0){
					ddsp_eep_wr_ok_flg33=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg34==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_34,(UINT)DDSp_SP_FL.ldddspu1LowTirePressFlg)==0){
					ddsp_eep_wr_ok_flg34=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg35==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_35,(UINT)DDSp_SP_FR.ldddsps16DefDctCnt)==0){
					ddsp_eep_wr_ok_flg35=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg36==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_36,(UINT)DDSp_SP_FR.ldddspu1LowTirePressFlg)==0){
					ddsp_eep_wr_ok_flg36=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg37==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_37,(UINT)DDSp_SP_RL.ldddsps16DefDctCnt)==0){
					ddsp_eep_wr_ok_flg37=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg38==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_38,(UINT)DDSp_SP_RL.ldddspu1LowTirePressFlg)==0){
					ddsp_eep_wr_ok_flg38=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg39==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_39,(UINT)DDSp_SP_RR.ldddsps16DefDctCnt)==0){
					ddsp_eep_wr_ok_flg39=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg40==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_40,(UINT)DDSp_SP_RR.ldddspu1LowTirePressFlg)==0){
					ddsp_eep_wr_ok_flg40=1;
				}
			}
			else if(ddsp_eep_wr_ok_flg41==0){
				if(LSESP_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_41,(UINT)ldddspu8DdsPlusModeEepromValue)==0){
					ddsp_eep_wr_ok_flg41=1;
				}
			}						
		    else{
		        ;
		    }
		
		}
	}	
	*/	
}



// INT ldddsps16SysParaCumulSumCalib[4][2][5];       80            


UINT LSDDS_u16WriteEEPROM(UINT addr,UINT data,UINT data2)
{
    if(WE_u8ReadEEP_Byte(addr,&com_rx_buf[0],4)==1)
    {
        if((*(UINT*)&com_rx_buf[0]!=data)
            ||(*(UINT*)&com_rx_buf[2])!=(data2))
        {
            weu16EepBuff[0]=data;
            weu16EepBuff[1]=data2;
            weu16Eep_addr=addr;
            weu1WriteSector=1;
            return 0; /*writing*/
        }
        else
        {
        	return 1; /*written already*/
        }
    }
    else
    {
    	return 2; /*read fail*/
    }
}
/*=======================================================*/
void LDDDS_EEPROMReadFunciton(void)
{
    UINT ReadEepBuffer[100];
    UCHAR i,j,k,cnt,Retval=0;
    UINT Addr;
    
    if (read_ddsp_eeprom_end_flg==1) {

        for(j=0;j<4;j++){
            if(j==0){
                DDSp_SP_WL=&DDSp_SP_FL;
            }
            else if(j==1){
                DDSp_SP_WL=&DDSp_SP_FR;
            }
            else if(j==2){
                DDSp_SP_WL=&DDSp_SP_RL;
            }
            else{
                DDSp_SP_WL=&DDSp_SP_RR;
            }                        
            for(i=0;i<5;i++){
                if(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[i]<=1){
                    ldddsps16CumulVehSpdCalib[j][i]=ldddsps16CumulSumVehSpdCalib[j][i];
                }
                else{
                     ldddsps16CumulVehSpdCalib[j][i]=(ldddsps16CumulSumVehSpdCalib[j][i])/(DDSp_SP_WL->ldddsps16CalCumulAvgCalibCnt[i]);
                }  
            }   
        }   
        return;    
    }
    
    if(read_ddsp_eeprom_end_flg==0){    
        for(i=0; i<20; i++)
        {
            // U16_DDSp_EERPOM_DATA_20;
            Addr=DDS_SYSPARA_SUMCALIB_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<4; i++)
        {
           for(j=0; j<2; j++)
           {
               for(k=0; k<5; k++)
               {
                    // 80byte --> address 20
                    ldddsps16SysParaCumulSumCalib[i][j][k]=(UINT)ReadEepBuffer[cnt];              
                    cnt++;
               }
           }
        }       
        
        for(i=0; i<10; i++)
        {
            // U16_DDSp_EERPOM_DATA_20;
            Addr=DDS_SYSPARA_VEHSPDCALIB_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<4; i++)
        {
           for(j=0; j<5; j++)
           {
                // 40byte --> address 10
                ldddsps16CumulSumVehSpdCalib[i][j]=(UINT)ReadEepBuffer[cnt];                                 
                cnt++;
           }
        }         
        
        
        /////////////////////////////////////////////////////////////
        // WHEEL DATA FL\
        /////////////////////////////////////////////////////////////
        for(i=0; i<10; i++)
        {
            // U16_DDSp_EERPOM_DATA_20;
            Addr=DDS_SYSPARA_FLWHEEL_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<5; i++)
        {
            DDSp_SP_FL.ldddsps16TireFreqCalib[i]=ReadEepBuffer[cnt];
            cnt++;
        }
        
        for(i=0; i<5; i++)
        {
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[i]=0xFF&(ReadEepBuffer[cnt]>>8);
            DDSp_SP_FL.ldddspu1CalibCompleteFlg[i]=0xFF&((ReadEepBuffer[cnt]<<8)>>8);
            cnt++;
        }
        DDSp_SP_FL.ldddsps16DefDctCnt=ReadEepBuffer[cnt];
        cnt++;
        
        DDSp_SP_FL.ldddspu1LowTirePressFlg=ReadEepBuffer[cnt];
        cnt++;
        
        /////////////////////////////////////////////////////////////
        // WHEEL DATA FR
        /////////////////////////////////////////////////////////////
        for(i=0; i<10; i++)
        {
             Addr=DDS_SYSPARA_FRWHEEL_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<5; i++)
        {
            DDSp_SP_FR.ldddsps16TireFreqCalib[i]=ReadEepBuffer[cnt];
            cnt++;
        }
        
        for(i=0; i<5; i++)
        {
            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[i]=0xFF&(ReadEepBuffer[cnt]>>8);
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[i]=0xFF&((ReadEepBuffer[cnt]<<8)>>8);
            cnt++;
        }
        DDSp_SP_FR.ldddsps16DefDctCnt=ReadEepBuffer[cnt];
        cnt++;
        
        DDSp_SP_FR.ldddspu1LowTirePressFlg=ReadEepBuffer[cnt];
        cnt++;
          
        /////////////////////////////////////////////////////////////
        // WHEEL DATA RL\
        /////////////////////////////////////////////////////////////
        for(i=0; i<10; i++)
        {
             Addr=DDS_SYSPARA_RLWHEEL_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<5; i++)
        {
            DDSp_SP_RL.ldddsps16TireFreqCalib[i]=ReadEepBuffer[cnt];
            cnt++;
        }
        
        for(i=0; i<5; i++)
        {
            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[i]=0xFF&(ReadEepBuffer[cnt]>>8);
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[i]=0xFF&((ReadEepBuffer[cnt]<<8)>>8);
            cnt++;
        }
        DDSp_SP_RL.ldddsps16DefDctCnt=ReadEepBuffer[cnt];
        cnt++;
        
        DDSp_SP_RL.ldddspu1LowTirePressFlg=ReadEepBuffer[cnt];
        cnt++;
                  
        /////////////////////////////////////////////////////////////
        // WHEEL DATA RR
        /////////////////////////////////////////////////////////////
        for(i=0; i<10; i++)
        {
             Addr=DDS_SYSPARA_RRWHEEL_ADDR+(4*i);
            if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
            {
                ReadEepBuffer[(2*i)]=*(INT*)&com_rx_buf[0];	
                ReadEepBuffer[(2*i)+1]=*(INT*)&com_rx_buf[2];	
            }    
        }
        
        // Decode;
        cnt=0;
        for(i=0; i<5; i++)
        {
            DDSp_SP_RR.ldddsps16TireFreqCalib[i]=ReadEepBuffer[cnt];
            cnt++;
        }
        
        for(i=0; i<5; i++)
        {
            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[i]=0xFF&(ReadEepBuffer[cnt]>>8);
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[i]=0xFF&((ReadEepBuffer[cnt]<<8)>>8);//??
            cnt++;
        }
        DDSp_SP_RR.ldddsps16DefDctCnt=ReadEepBuffer[cnt];
        cnt++;
        
        DDSp_SP_RR.ldddspu1LowTirePressFlg=ReadEepBuffer[cnt];
        cnt++;
        
             
        Addr=U16_DDSp_EERPOM_DATA_59;
        if(WE_u8ReadEEP_Byte(Addr,&com_rx_buf[0],4)==1)
        {
            ReadEepBuffer[0]=*(INT*)&com_rx_buf[0];	
            ReadEepBuffer[1]=*(INT*)&com_rx_buf[2];	
        } 
        ldddspu8DdsPlusModeEepromValue=ReadEepBuffer[0];
        
        
        
        read_ddsp_eeprom_end_flg=1;  
    }
}
/*=============================================================================*/    
void LDDDS_EEPROMWriteFunciton(void)
{
    UINT EepBuffer[100];
    UCHAR i,j,k,cnt,Retval=0;
    cnt=0;
  if(ldddspu8DdsPlusMode!=FUNCTION_IGNON)
	{
		ldddspu8DdsPlusModeEepromValue=ldddspu8DdsPlusMode;
	}

    if((ddsp_eep_wr_ok_flg01==0)||(ddsp_eep_wr_ok_flg02==0)||(ddsp_eep_wr_ok_flg03==0)||(ddsp_eep_wr_ok_flg04==0)||
    (ddsp_eep_wr_ok_flg05==0)||(ddsp_eep_wr_ok_flg06==0)||(ddsp_eep_wr_ok_flg07==0)||(ddsp_eep_wr_ok_flg08==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg09==0)||(ddsp_eep_wr_ok_flg10==0)||(ddsp_eep_wr_ok_flg11==0)||(ddsp_eep_wr_ok_flg12==0)||
    (ddsp_eep_wr_ok_flg13==0)||(ddsp_eep_wr_ok_flg14==0)||(ddsp_eep_wr_ok_flg15==0)||(ddsp_eep_wr_ok_flg16==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg17==0)||(ddsp_eep_wr_ok_flg18==0)||(ddsp_eep_wr_ok_flg19==0)||(ddsp_eep_wr_ok_flg20==0)||
    (ddsp_eep_wr_ok_flg21==0)||(ddsp_eep_wr_ok_flg22==0)||(ddsp_eep_wr_ok_flg23==0)||(ddsp_eep_wr_ok_flg24==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg25==0)||(ddsp_eep_wr_ok_flg26==0)||(ddsp_eep_wr_ok_flg27==0)||(ddsp_eep_wr_ok_flg28==0)||
    (ddsp_eep_wr_ok_flg29==0)||(ddsp_eep_wr_ok_flg30==0)||(ddsp_eep_wr_ok_flg31==0)||(ddsp_eep_wr_ok_flg32==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg33==0)||(ddsp_eep_wr_ok_flg34==0)||(ddsp_eep_wr_ok_flg35==0)||(ddsp_eep_wr_ok_flg36==0)||
    (ddsp_eep_wr_ok_flg37==0)||(ddsp_eep_wr_ok_flg38==0)||(ddsp_eep_wr_ok_flg39==0)||(ddsp_eep_wr_ok_flg40==0)){
	    write_ddsp_eeprom_end_flg=0;
	}	
	else if((ddsp_eep_wr_ok_flg41==0)||(ddsp_eep_wr_ok_flg42==0)||(ddsp_eep_wr_ok_flg43==0)||(ddsp_eep_wr_ok_flg44==0)||
    (ddsp_eep_wr_ok_flg45==0)||(ddsp_eep_wr_ok_flg46==0)||(ddsp_eep_wr_ok_flg47==0)||(ddsp_eep_wr_ok_flg48==0)){
	    write_ddsp_eeprom_end_flg=0;
	}	
	else if((ddsp_eep_wr_ok_flg49==0)||(ddsp_eep_wr_ok_flg50==0)||(ddsp_eep_wr_ok_flg51==0)||(ddsp_eep_wr_ok_flg52==0)||
    (ddsp_eep_wr_ok_flg53==0)||(ddsp_eep_wr_ok_flg54==0)||(ddsp_eep_wr_ok_flg55==0)||(ddsp_eep_wr_ok_flg56==0)){
	    write_ddsp_eeprom_end_flg=0;
	}
	else if((ddsp_eep_wr_ok_flg57==0)||(ddsp_eep_wr_ok_flg58==0)||(ddsp_eep_wr_ok_flg59==0)){
	    write_ddsp_eeprom_end_flg=0;
	}									
	else{
	    write_ddsp_eeprom_end_flg=1;
	}   
    if(write_ddsp_eeprom_end_flg==1) return;

	if(ddspSysParaCumulSumCalib_eepend==0)
	{	
		if((weu1EraseSector==0)&&(weu1WriteSector==0))
		{
           for(i=0; i<4; i++)
           {
               for(j=0; j<2; j++)
               {
                   for(k=0; k<5; k++)
                   {
                        // 80byte --> address 20
                        EepBuffer[cnt]=(UINT)ldddsps16SysParaCumulSumCalib[i][j][k];              
                        cnt++;
                   }
               }
           }   
   
            if(ddsp_eep_wr_ok_flg01==0)      ddsp_eep_wr_ok_flg01=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_01,EepBuffer[0],EepBuffer[1]);
            else if(ddsp_eep_wr_ok_flg02==0) ddsp_eep_wr_ok_flg02=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_02,EepBuffer[2],EepBuffer[3]);
            else if(ddsp_eep_wr_ok_flg03==0) ddsp_eep_wr_ok_flg03=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_03,EepBuffer[4],EepBuffer[5]);
            else if(ddsp_eep_wr_ok_flg04==0) ddsp_eep_wr_ok_flg04=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_04,EepBuffer[6],EepBuffer[7]);
            else if(ddsp_eep_wr_ok_flg05==0) ddsp_eep_wr_ok_flg05=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_05,EepBuffer[8],EepBuffer[9]);
            else if(ddsp_eep_wr_ok_flg06==0) ddsp_eep_wr_ok_flg06=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_06,EepBuffer[10],EepBuffer[11]);
            else if(ddsp_eep_wr_ok_flg07==0) ddsp_eep_wr_ok_flg07=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_07,EepBuffer[12],EepBuffer[13]);
            else if(ddsp_eep_wr_ok_flg08==0) ddsp_eep_wr_ok_flg08=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_08,EepBuffer[14],EepBuffer[15]);
            else if(ddsp_eep_wr_ok_flg09==0) ddsp_eep_wr_ok_flg09=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_09,EepBuffer[16],EepBuffer[17]);
            else if(ddsp_eep_wr_ok_flg10==0) ddsp_eep_wr_ok_flg10=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_10,EepBuffer[18],EepBuffer[19]);
            else if(ddsp_eep_wr_ok_flg11==0) ddsp_eep_wr_ok_flg11=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_11,EepBuffer[20],EepBuffer[21]);
            else if(ddsp_eep_wr_ok_flg12==0) ddsp_eep_wr_ok_flg12=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_12,EepBuffer[22],EepBuffer[23]);
            else if(ddsp_eep_wr_ok_flg13==0) ddsp_eep_wr_ok_flg13=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_13,EepBuffer[24],EepBuffer[25]);
            else if(ddsp_eep_wr_ok_flg14==0) ddsp_eep_wr_ok_flg14=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_14,EepBuffer[26],EepBuffer[27]);
            else if(ddsp_eep_wr_ok_flg15==0) ddsp_eep_wr_ok_flg15=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_15,EepBuffer[28],EepBuffer[29]);
            else if(ddsp_eep_wr_ok_flg16==0) ddsp_eep_wr_ok_flg16=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_16,EepBuffer[30],EepBuffer[31]);
            else if(ddsp_eep_wr_ok_flg17==0) ddsp_eep_wr_ok_flg17=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_17,EepBuffer[32],EepBuffer[33]);
            else if(ddsp_eep_wr_ok_flg18==0) ddsp_eep_wr_ok_flg18=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_18,EepBuffer[34],EepBuffer[35]);
            else if(ddsp_eep_wr_ok_flg19==0) ddsp_eep_wr_ok_flg19=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_19,EepBuffer[36],EepBuffer[37]);
            else if(ddsp_eep_wr_ok_flg20==0) ddsp_eep_wr_ok_flg20=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_20,EepBuffer[38],EepBuffer[39]);
            else ddspSysParaCumulSumCalib_eepend=1;    
       }
    }
    else if(DDSCumulSumVehSpdCalibEEPEnd==0)
    {	
    	if((weu1EraseSector==0)&&(weu1WriteSector==0))
    	{
    	   cnt=0;     
           for(i=0; i<4; i++)
           {
               for(j=0; j<5; j++)
               {
                    // 40byte --> address 10
                    EepBuffer[cnt]=(UINT)ldddsps16CumulSumVehSpdCalib[i][j];                                 
                    cnt++;
               }
           }   
    
            if(ddsp_eep_wr_ok_flg21==0)      ddsp_eep_wr_ok_flg21=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_21,EepBuffer[0],EepBuffer[1]);   
            else if(ddsp_eep_wr_ok_flg22==0) ddsp_eep_wr_ok_flg22=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_22,EepBuffer[2],EepBuffer[3]);   
            else if(ddsp_eep_wr_ok_flg23==0) ddsp_eep_wr_ok_flg23=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_23,EepBuffer[4],EepBuffer[5]);   
            else if(ddsp_eep_wr_ok_flg24==0) ddsp_eep_wr_ok_flg24=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_24,EepBuffer[6],EepBuffer[7]);   
            else if(ddsp_eep_wr_ok_flg25==0) ddsp_eep_wr_ok_flg25=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_25,EepBuffer[8],EepBuffer[9]);   
            else if(ddsp_eep_wr_ok_flg26==0) ddsp_eep_wr_ok_flg26=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_26,EepBuffer[10],EepBuffer[11]); 
            else if(ddsp_eep_wr_ok_flg27==0) ddsp_eep_wr_ok_flg27=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_27,EepBuffer[12],EepBuffer[13]); 
            else if(ddsp_eep_wr_ok_flg28==0) ddsp_eep_wr_ok_flg28=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_28,EepBuffer[14],EepBuffer[15]); 
            else if(ddsp_eep_wr_ok_flg29==0) ddsp_eep_wr_ok_flg29=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_29,EepBuffer[16],EepBuffer[17]); 
            else if(ddsp_eep_wr_ok_flg30==0) ddsp_eep_wr_ok_flg30=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_30,EepBuffer[18],EepBuffer[19]); 
            else DDSCumulSumVehSpdCalibEEPEnd=1;                                                                
       }                                                                                                        
    } 
    else if(DDSp_SP_t_FL_EEP_END==0)
    {
    	if((weu1EraseSector==0)&&(weu1WriteSector==0)){
        cnt=0;                                                                                                       
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=DDSp_SP_FL.ldddsps16TireFreqCalib[i];
            cnt++;
        }
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=((UINT)DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[i]<<8)|(DDSp_SP_FL.ldddspu1CalibCompleteFlg[i]);
            cnt++;
        }
        EepBuffer[cnt]=(UINT)DDSp_SP_FL.ldddsps16DefDctCnt;
        cnt++;
        EepBuffer[cnt]=(UINT)DDSp_SP_FL.ldddspu1LowTirePressFlg;
        cnt++;
        
        if(ddsp_eep_wr_ok_flg31==0)      ddsp_eep_wr_ok_flg31=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_31,EepBuffer[0],EepBuffer[1]);   
        else if(ddsp_eep_wr_ok_flg32==0) ddsp_eep_wr_ok_flg32=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_32,EepBuffer[2],EepBuffer[3]);   
        else if(ddsp_eep_wr_ok_flg33==0) ddsp_eep_wr_ok_flg33=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_33,EepBuffer[4],EepBuffer[5]);   
        else if(ddsp_eep_wr_ok_flg34==0) ddsp_eep_wr_ok_flg34=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_34,EepBuffer[6],EepBuffer[7]);   
        else if(ddsp_eep_wr_ok_flg35==0) ddsp_eep_wr_ok_flg35=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_35,EepBuffer[8],EepBuffer[9]);   
        else if(ddsp_eep_wr_ok_flg36==0) ddsp_eep_wr_ok_flg36=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_36,EepBuffer[10],EepBuffer[11]); 
        else if(ddsp_eep_wr_ok_flg37==0) ddsp_eep_wr_ok_flg37=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_37,EepBuffer[12],EepBuffer[13]); 
        else DDSp_SP_t_FL_EEP_END=1;     
        }                                                           
    }                                                                                       
    else if(DDSp_SP_t_FR_EEP_END==0)
    {
    	if((weu1EraseSector==0)&&(weu1WriteSector==0)){
        cnt=0;                                                                                                       
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=DDSp_SP_FR.ldddsps16TireFreqCalib[i];
            cnt++;
        }
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=((UINT)DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[i]<<8)|(DDSp_SP_FR.ldddspu1CalibCompleteFlg[i]);
            cnt++;
        }
        EepBuffer[cnt]=(UINT)DDSp_SP_FR.ldddsps16DefDctCnt;
        cnt++;
        EepBuffer[cnt]=(UINT)DDSp_SP_FR.ldddspu1LowTirePressFlg;
        cnt++;
        
        if(ddsp_eep_wr_ok_flg38==0)      ddsp_eep_wr_ok_flg38=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_38,EepBuffer[0],EepBuffer[1]);   
        else if(ddsp_eep_wr_ok_flg39==0) ddsp_eep_wr_ok_flg39=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_39,EepBuffer[2],EepBuffer[3]);   
        else if(ddsp_eep_wr_ok_flg40==0) ddsp_eep_wr_ok_flg40=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_40,EepBuffer[4],EepBuffer[5]);   
        else if(ddsp_eep_wr_ok_flg41==0) ddsp_eep_wr_ok_flg41=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_41,EepBuffer[6],EepBuffer[7]);   
        else if(ddsp_eep_wr_ok_flg42==0) ddsp_eep_wr_ok_flg42=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_42,EepBuffer[8],EepBuffer[9]);   
        else if(ddsp_eep_wr_ok_flg43==0) ddsp_eep_wr_ok_flg43=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_43,EepBuffer[10],EepBuffer[11]); 
        else if(ddsp_eep_wr_ok_flg44==0) ddsp_eep_wr_ok_flg44=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_44,EepBuffer[12],EepBuffer[13]); 
        else DDSp_SP_t_FR_EEP_END=1;        
        }                                                        
    }                                                                                                                 
    else if(DDSp_SP_t_RL_EEP_END==0)
    {
    	if((weu1EraseSector==0)&&(weu1WriteSector==0)){
        cnt=0;                                                                                                       
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=DDSp_SP_RL.ldddsps16TireFreqCalib[i];
            cnt++;
        }
        for(i=0; i<5; i++)
        {
            EepBuffer[cnt]=((UINT)DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[i]<<8)|(DDSp_SP_RL.ldddspu1CalibCompleteFlg[i]);
            cnt++;
        }
        EepBuffer[cnt]=(UINT)DDSp_SP_RL.ldddsps16DefDctCnt;
        cnt++;
        EepBuffer[cnt]=(UINT)DDSp_SP_RL.ldddspu1LowTirePressFlg;
        cnt++;
        
        if(ddsp_eep_wr_ok_flg45==0)      ddsp_eep_wr_ok_flg45=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_45,EepBuffer[0],EepBuffer[1]);   
        else if(ddsp_eep_wr_ok_flg46==0) ddsp_eep_wr_ok_flg46=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_46,EepBuffer[2],EepBuffer[3]);   
        else if(ddsp_eep_wr_ok_flg47==0) ddsp_eep_wr_ok_flg47=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_47,EepBuffer[4],EepBuffer[5]);   
        else if(ddsp_eep_wr_ok_flg48==0) ddsp_eep_wr_ok_flg48=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_48,EepBuffer[6],EepBuffer[7]);   
        else if(ddsp_eep_wr_ok_flg49==0) ddsp_eep_wr_ok_flg49=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_49,EepBuffer[8],EepBuffer[9]);   
        else if(ddsp_eep_wr_ok_flg50==0) ddsp_eep_wr_ok_flg50=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_50,EepBuffer[10],EepBuffer[11]); 
        else if(ddsp_eep_wr_ok_flg51==0) ddsp_eep_wr_ok_flg51=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_51,EepBuffer[12],EepBuffer[13]); 
        else DDSp_SP_t_RL_EEP_END=1;   
        }                                                             
    }                                                                                                                 
    else if(DDSp_SP_t_RR_EEP_END==0)                                                                                                                                                                                                                        
    {                     
    	if((weu1EraseSector==0)&&(weu1WriteSector==0)){                                                                                                                                                                                                                                  
        cnt=0;                                                                                                                                                                                                                                            
        for(i=0; i<5; i++)                                                                                                                
        {                                                                                                                                 
            EepBuffer[cnt]=DDSp_SP_RR.ldddsps16TireFreqCalib[i];                                                                          
            cnt++;                                                                                                                        
        }                                                                                                                                 
        for(i=0; i<5; i++)                                                                                                                
        {                                                                                                                                 
            EepBuffer[cnt]=((UINT)DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[i]<<8)|(DDSp_SP_RR.ldddspu1CalibCompleteFlg[i]);                
            cnt++;                                                                                                                        
        }                                                                                                                                 
        EepBuffer[cnt]=(UINT)DDSp_SP_RR.ldddsps16DefDctCnt;                                                                               
        cnt++;                                                                                                                            
        EepBuffer[cnt]=(UINT)DDSp_SP_RR.ldddspu1LowTirePressFlg;                                                                          
        cnt++;                                                                                                                            
                                                                                                                                          
        if(ddsp_eep_wr_ok_flg52==0)      ddsp_eep_wr_ok_flg52=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_52,EepBuffer[0],EepBuffer[1]);    
        else if(ddsp_eep_wr_ok_flg53==0) ddsp_eep_wr_ok_flg53=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_53,EepBuffer[2],EepBuffer[3]);    
        else if(ddsp_eep_wr_ok_flg54==0) ddsp_eep_wr_ok_flg54=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_54,EepBuffer[4],EepBuffer[5]);    
        else if(ddsp_eep_wr_ok_flg55==0) ddsp_eep_wr_ok_flg55=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_55,EepBuffer[6],EepBuffer[7]);    
        else if(ddsp_eep_wr_ok_flg56==0) ddsp_eep_wr_ok_flg56=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_56,EepBuffer[8],EepBuffer[9]);    
        else if(ddsp_eep_wr_ok_flg57==0) ddsp_eep_wr_ok_flg57=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_57,EepBuffer[10],EepBuffer[11]);  
        else if(ddsp_eep_wr_ok_flg58==0) ddsp_eep_wr_ok_flg58=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_58,EepBuffer[12],EepBuffer[13]);  
        else DDSp_SP_t_RR_EEP_END=1;    
        }                                                                                                  
    }
    else if(ddsp_eep_wr_ok_flg59==0){
    	if((weu1EraseSector==0)&&(weu1WriteSector==0)){
        ddsp_eep_wr_ok_flg59=LSDDS_u16WriteEEPROM(U16_DDSp_EERPOM_DATA_59,ldddspu8DdsPlusModeEepromValue,ldddspu8DdsPlusModeEepromValue);
    	}
    } 
    else  write_ddsp_eeprom_end_flg=1; 
}                                                                                                                                    
#endif
/*=================================================================*/

void LDDDSp_vClearDDSpTire1RapData(void)
{
    UCHAR i,j;
    
    /*1Rap Data -struct*/
    DDSp_SP_FL.ldddspu8SumPeriodCnt=0;
    DDSp_SP_FL.ldddspu8StoredPeriodCnt=0;
    DDSp_SP_FL.ldddspu8ResPeriodCnt=0;
    DDSp_SP_FL.ldddspu16ElapsedTime1Rev=0;
    DDSp_SP_FL.ldddsps16TimeReference1Rap=0;
    DDSp_SP_FL.ldddspu16SumPeriodTime=0;
    DDSp_SP_FL.ldddspu1Tire1RapCompleteFlg=0;

    DDSp_SP_FR.ldddspu8SumPeriodCnt=0;
    DDSp_SP_FR.ldddspu8StoredPeriodCnt=0;
    DDSp_SP_FR.ldddspu8ResPeriodCnt=0;
    DDSp_SP_FR.ldddspu16ElapsedTime1Rev=0;
    DDSp_SP_FR.ldddsps16TimeReference1Rap=0;
    DDSp_SP_FR.ldddspu16SumPeriodTime=0;
    DDSp_SP_FR.ldddspu1Tire1RapCompleteFlg=0;

    DDSp_SP_RL.ldddspu8SumPeriodCnt=0;
    DDSp_SP_RL.ldddspu8StoredPeriodCnt=0;
    DDSp_SP_RL.ldddspu8ResPeriodCnt=0;
    DDSp_SP_RL.ldddspu16ElapsedTime1Rev=0;
    DDSp_SP_RL.ldddsps16TimeReference1Rap=0;
    DDSp_SP_RL.ldddspu16SumPeriodTime=0;
    DDSp_SP_RL.ldddspu1Tire1RapCompleteFlg=0;
    
    DDSp_SP_RR.ldddspu8SumPeriodCnt=0;
    DDSp_SP_RR.ldddspu8StoredPeriodCnt=0;
    DDSp_SP_RR.ldddspu8ResPeriodCnt=0;
    DDSp_SP_RR.ldddspu16ElapsedTime1Rev=0;
    DDSp_SP_RR.ldddsps16TimeReference1Rap=0;
    DDSp_SP_RR.ldddspu16SumPeriodTime=0;
    DDSp_SP_RR.ldddspu1Tire1RapCompleteFlg=0;    
    
    if(ldddspu8DdsPlusMode==FUNCTION_DDS_RESET){
        for(i=0;i<4;i++){
            for(j=0;j<10;j++){
                ldddsps16StoredPeriodTime[i][j]=0;
                ldddspu16ResPeriod[i][j]=0;
            }
        }
    }     
}
/*=================================================================*/
void LDDDSp_vClearDDSpCalibPara(void)
{
    UCHAR i,j;

    DDSp_SP_FL.ldddsps16CalCalibInitOkCnt=0;
    DDSp_SP_FL.ldddsps16CalibInitCalOkCnt=0;
    DDSp_SP_FL.ldddspu1CalibInitCalOkFlg=0; 
    
    DDSp_SP_FR.ldddsps16CalCalibInitOkCnt=0;
    DDSp_SP_FR.ldddsps16CalibInitCalOkCnt=0;
    DDSp_SP_FR.ldddspu1CalibInitCalOkFlg=0;   
    
    DDSp_SP_RL.ldddsps16CalCalibInitOkCnt=0;
    DDSp_SP_RL.ldddsps16CalibInitCalOkCnt=0;
    DDSp_SP_RL.ldddspu1CalibInitCalOkFlg=0;   
    
    DDSp_SP_RR.ldddsps16CalCalibInitOkCnt=0;
    DDSp_SP_RR.ldddsps16CalibInitCalOkCnt=0;
    DDSp_SP_RR.ldddspu1CalibInitCalOkFlg=0;     
        
    for(i=0;i<4;i++){     
//        ldddsps16SysParaCurCalibRes[i][0]=0;
//        ldddsps16SysParaCurCalibRes[i][1]=0;
        
        ldddsps16SysParaCalBuffer[i][0]=0;
        ldddsps16SysParaCalBuffer[i][1]=0;
        ldddsps16SysParaCalBuffer[i][2]=0;
        

        ldddsps16SysParaCalibCurrent[i][0]=0;
        ldddsps16SysParaCalibCurrent[i][1]=0;

        ldddsps32VelSpdSumCalib[i]=0;
        ldddsps32VelSpdSumDct[i]=0;


    }                                     
    
   
    if(ldddspu8DdsPlusMode==FUNCTION_DDS_RESET){
        ldddspu1AllWhlCalibCompleteFlg=0;
        ldddsps16TireFreqCalibFL=0;
        ldddsps16TireFreqCalibFR=0;
        ldddsps16TireFreqCalibRL=0;
        ldddsps16TireFreqCalibRR=0;
        
        
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[0]=0;
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[1]=0;
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[2]=0;
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[3]=0;
            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[4]=0;
            
            DDSp_SP_FL.ldddsps16TireFreqCalib[0]=0;
            DDSp_SP_FL.ldddsps16TireFreqCalib[1]=0;
            DDSp_SP_FL.ldddsps16TireFreqCalib[2]=0;
            DDSp_SP_FL.ldddsps16TireFreqCalib[3]=0;
            DDSp_SP_FL.ldddsps16TireFreqCalib[4]=0;


            DDSp_SP_FL.ldddspu1CalibCompleteFlg[0]=0;   
            DDSp_SP_FL.ldddspu1CalibCompleteFlg[1]=0;   
            DDSp_SP_FL.ldddspu1CalibCompleteFlg[2]=0;   
            DDSp_SP_FL.ldddspu1CalibCompleteFlg[3]=0;   
            DDSp_SP_FL.ldddspu1CalibCompleteFlg[4]=0;   

            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[0]=0;
            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[1]=0;
            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[2]=0;
            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[3]=0;
            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[4]=0;



            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[0]=0;
            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[1]=0;
            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[2]=0;
            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[3]=0;
            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[4]=0;
                     
            DDSp_SP_FR.ldddsps16TireFreqCalib[0]=0;
            DDSp_SP_FR.ldddsps16TireFreqCalib[1]=0;
            DDSp_SP_FR.ldddsps16TireFreqCalib[2]=0;
            DDSp_SP_FR.ldddsps16TireFreqCalib[3]=0;
            DDSp_SP_FR.ldddsps16TireFreqCalib[4]=0;
                     
                     
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[0]=0;   
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[1]=0;   
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[2]=0;   
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[3]=0;   
            DDSp_SP_FR.ldddspu1CalibCompleteFlg[4]=0;   
                     
            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[0]=0;
            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[1]=0;
            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[2]=0;
            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[3]=0;
            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[4]=0;
            

            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[0]=0;
            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[1]=0;
            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[2]=0;
            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[3]=0;
            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[4]=0;
                    
            DDSp_SP_RL.ldddsps16TireFreqCalib[0]=0;
            DDSp_SP_RL.ldddsps16TireFreqCalib[1]=0;
            DDSp_SP_RL.ldddsps16TireFreqCalib[2]=0;
            DDSp_SP_RL.ldddsps16TireFreqCalib[3]=0;
            DDSp_SP_RL.ldddsps16TireFreqCalib[4]=0;
                    
                    
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[0]=0;   
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[1]=0;   
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[2]=0;   
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[3]=0;   
            DDSp_SP_RL.ldddspu1CalibCompleteFlg[4]=0;   
                    
            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[0]=0;
            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[1]=0;
            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[2]=0;
            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[3]=0;
            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[4]=0;
            

            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[0]=0;
            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[1]=0;
            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[2]=0;
            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[3]=0;
            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[4]=0;
                    
            DDSp_SP_RR.ldddsps16TireFreqCalib[0]=0;
            DDSp_SP_RR.ldddsps16TireFreqCalib[1]=0;
            DDSp_SP_RR.ldddsps16TireFreqCalib[2]=0;
            DDSp_SP_RR.ldddsps16TireFreqCalib[3]=0;
            DDSp_SP_RR.ldddsps16TireFreqCalib[4]=0;
                    
                    
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[0]=0;   
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[1]=0;   
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[2]=0;   
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[3]=0;   
            DDSp_SP_RR.ldddspu1CalibCompleteFlg[4]=0;   
                    
            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[0]=0;
            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[1]=0;
            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[2]=0;
            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[3]=0;
            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[4]=0;                        

//        for(j=0;j<5;j++){
//            DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[j]=0;
//            DDSp_SP_FL.ldddsps16TireFreqCalib[j]=0;
//    //        DDSp_SP_FL.ldddspu16DetCalibCompleteCnt[5]=0;
//            DDSp_SP_FL.ldddspu1CalibCompleteFlg[j]=0;   
//            DDSp_SP_FL.ldddspu8SysParaBufCalibTimer[j]=0;
//            
//            DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[j]=0;
//            DDSp_SP_FR.ldddsps16TireFreqCalib[j]=0;
//    //        DDSp_SP_FR.ldddspu16DetCalibCompleteCnt[5]=0;
//            DDSp_SP_FR.ldddspu1CalibCompleteFlg[j]=0;   
//            DDSp_SP_FR.ldddspu8SysParaBufCalibTimer[j]=0;
//            
//            DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[j]=0;
//            DDSp_SP_RL.ldddsps16TireFreqCalib[j]=0;
//    //        DDSp_SP_RL.ldddspu16DetCalibCompleteCnt[5]=0;
//            DDSp_SP_RL.ldddspu1CalibCompleteFlg[j]=0;   
//            DDSp_SP_RL.ldddspu8SysParaBufCalibTimer[j]=0;
//            
//            DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[j]=0;
//            DDSp_SP_RR.ldddsps16TireFreqCalib[j]=0;
//    //        DDSp_SP_RR.ldddspu16DetCalibCompleteCnt[5]=0;
//            DDSp_SP_RR.ldddspu1CalibCompleteFlg[j]=0;   
//            DDSp_SP_RR.ldddspu8SysParaBufCalibTimer[j]=0;
//            
//            
//        }
        for(i=0;i<4;i++){
            for(j=0;j<5;j++){
                ldddsps16SysParaCumulAvgCalib[i][0][j]=0;
//                ldddsps16SysParaCumulResCalib[i][0][j]=0;
                ldddsps16SysParaCumulAvgCalib[i][1][j]=0;
 //               ldddsps16SysParaCumulResCalib[i][1][j]=0;  
                ldddsps16SysParaCumulSumCalib[i][0][j]=0; 
                ldddsps16SysParaCumulSumCalib[i][1][j]=0; 
                ldddsps16CumulSumVehSpdCalib[i][j]=0;
            }
            ldddsps16SysParaSumCalib[i][0]=0;
            ldddsps16SysParaSumCalib[i][1]=0;          
        }
    }
}
/*=================================================================*/
void LDDDSp_vClearDDSpDefDctPara(void)
{
    UCHAR i,j,p;
    for(i=0;i<4;i++){
        ldddsps16SysParaDefCalBuffer[i][0]=0;
        ldddsps16SysParaDefCalBuffer[i][1]=0;
        ldddsps16SysParaDefCalBuffer[i][2]=0;
        
        ldddsps16SysParaDefCurrent[i][0]=0;
        ldddsps16SysParaDefCurrent[i][1]=0;
        ldddsps32VelSpdSumCalib[i]=0;
        ldddsps32VelSpdSumDct[i]=0;
    }
          
    DDSp_SP_FL.ldddsps16SysParaDefDctCalCnt=0; 
    DDSp_SP_FL.ldddsps16DefDctInitCalOkCnt=0;
    DDSp_SP_FL.ldddspu1DefDctInitCalOkFlg=0; 
       
    DDSp_SP_FR.ldddsps16SysParaDefDctCalCnt=0; 
    DDSp_SP_FR.ldddsps16DefDctInitCalOkCnt=0;
    DDSp_SP_FR.ldddspu1DefDctInitCalOkFlg=0; 
    
    DDSp_SP_RL.ldddsps16SysParaDefDctCalCnt=0; 
    DDSp_SP_RL.ldddsps16DefDctInitCalOkCnt=0;
    DDSp_SP_RL.ldddspu1DefDctInitCalOkFlg=0; 
    
    DDSp_SP_RR.ldddsps16SysParaDefDctCalCnt=0; 
    DDSp_SP_RR.ldddsps16DefDctInitCalOkCnt=0;
    DDSp_SP_RR.ldddspu1DefDctInitCalOkFlg=0;     

/*Reset*/
    if(ldddspu8DdsPlusMode==FUNCTION_DDS_RESET){
        
        DDSp_SP_FL.ldddsps16DefDctCnt=0;
        DDSp_SP_FL.ldddspu1LowTirePressFlg=0;   
        
        DDSp_SP_FR.ldddsps16DefDctCnt=0;
        DDSp_SP_FR.ldddspu1LowTirePressFlg=0;   
        
        DDSp_SP_RL.ldddsps16DefDctCnt=0;
        DDSp_SP_RL.ldddspu1LowTirePressFlg=0;   
        
        DDSp_SP_RR.ldddsps16DefDctCnt=0;
        DDSp_SP_RR.ldddspu1LowTirePressFlg=0;  

            DDSp_SP_FL.ldddspu8SysParaBufDctTimer=0;          
            DDSp_SP_FR.ldddspu8SysParaBufDctTimer=0; 
            DDSp_SP_RL.ldddspu8SysParaBufDctTimer=0;            
        DDSp_SP_RR.ldddspu8SysParaBufDctTimer=0;
            
            DDSp_SP_FL.ldddsps16SysParaDefInitCalCnt=0; 
            DDSp_SP_FR.ldddsps16SysParaDefInitCalCnt=0;
            DDSp_SP_RL.ldddsps16SysParaDefInitCalCnt=0;
            DDSp_SP_RR.ldddsps16SysParaDefInitCalCnt=0; 
            
                    
        for(p=0;p<5;p++){

            
                                       
            for(i=0;i<4;i++){
//                ldddsps16SysParaMovingAvgDef[i][0][p]=0;
//                ldddsps16SysParaMovingAvgDef[i][1][p]=0;
                    ldddsps16SysParaDefDct[i]=0;
                     ldddspu8SysParaBufDefDct[i][p]=0;
                    ldddspu8SysParaBufDefDct2[i][p]=0;               
//                for(j=0;j<5;j++){                                  
//                    ldddsps16SysParaAvgDefBuffer[i][j][0][p]=0;
//                    ldddsps16SysParaAvgDefBuffer[i][j][1][p]=0;
//                    ldddsps16SysParaAvgDefBuffer2[i][j][0][p]=0;
//                    ldddsps16SysParaAvgDefBuffer2[i][j][1][p]=0;
//
//                }
            }
        }
    }   
}
/*=================================================================*/
void LDDDSp_vClearDDSpTireTeethInx(void)
{
    UCHAR i,j;
    DDSp_SP_FL.ldddspu8WheelTeethIndex=0;
    DDSp_SP_FL.ldddspu8WheelTeethIdxFinal=0;
    DDSp_SP_FL.ldddspu8WheelTeethIndexOld=0;
    DDSp_SP_FL.ldddspu8WheelTeethInxCurrent=0;
    DDSp_SP_FL.ldddsps16TeethIndexDetCnt=0;
    DDSp_SP_FL.ldddspu1TeethIndexStatus=0;
    DDSp_SP_FL.ldddspu1TeethIndexStatusK=0;
   
    DDSp_SP_FR.ldddspu8WheelTeethIndex=0;
    DDSp_SP_FR.ldddspu8WheelTeethIdxFinal=0;
    DDSp_SP_FR.ldddspu8WheelTeethIndexOld=0;
    DDSp_SP_FR.ldddspu8WheelTeethInxCurrent=0;
    DDSp_SP_FR.ldddsps16TeethIndexDetCnt=0;
    DDSp_SP_FR.ldddspu1TeethIndexStatus=0;
    DDSp_SP_FR.ldddspu1TeethIndexStatusK=0;
    
    DDSp_SP_RL.ldddspu8WheelTeethIndex=0;
    DDSp_SP_RL.ldddspu8WheelTeethIdxFinal=0;
    DDSp_SP_RL.ldddspu8WheelTeethIndexOld=0;
    DDSp_SP_RL.ldddspu8WheelTeethInxCurrent=0;
    DDSp_SP_RL.ldddsps16TeethIndexDetCnt=0;
    DDSp_SP_RL.ldddspu1TeethIndexStatus=0;
    DDSp_SP_RL.ldddspu1TeethIndexStatusK=0;           
        
    DDSp_SP_RR.ldddspu8WheelTeethIndex=0;
    DDSp_SP_RR.ldddspu8WheelTeethIdxFinal=0;
    DDSp_SP_RR.ldddspu8WheelTeethIndexOld=0;
    DDSp_SP_RR.ldddspu8WheelTeethInxCurrent=0;
    DDSp_SP_RR.ldddsps16TeethIndexDetCnt=0;
    DDSp_SP_RR.ldddspu1TeethIndexStatus=0;
    DDSp_SP_RR.ldddspu1TeethIndexStatusK=0;
    
    
    if(ldddspu8DdsPlusMode==FUNCTION_DDS_RESET){
        DDSp_SP_FL.ldddspu16TeethDeviCalcCnt=0;
        DDSp_SP_FL.ldddspu1WheelTeethCalcOk=0;
        
        DDSp_SP_FR.ldddspu16TeethDeviCalcCnt=0;
        DDSp_SP_FR.ldddspu1WheelTeethCalcOk=0;   
             
        DDSp_SP_RL.ldddspu16TeethDeviCalcCnt=0;
        DDSp_SP_RL.ldddspu1WheelTeethCalcOk=0;
        
        DDSp_SP_RR.ldddspu16TeethDeviCalcCnt=0;
        DDSp_SP_RR.ldddspu1WheelTeethCalcOk=0;                
        for(i=0;i<4;i++){
            for(j=0;j<50;j++){
                ldddsps16WhlTeethDeviationF[i][j]=0;
                ldddsps16WhlTeethDeviationMod[i][j]=0;                
            }
        }
    }
}
/*=================================================================*/
void LDDDSp_vClearSignalPreprocess(void)
{

    DDSp_SP_FL.ldddsps16Time1PulseResidue=0;
    DDSp_SP_FL.ldddsps16WradOld=0;
    DDSp_SP_FL.ldddsps16BPFInitialOkCnt=0;
    DDSp_SP_FL.ldddsps16AngularSpeedHpfRes=0;
    DDSp_SP_FL.ldddsps16AngularSpeedBpfRes=0;
    DDSp_SP_FL.ldddspu1BPFInitialOkFlg=0;
    DDSp_SP_FL.ldddsps16ResmpTimeCnt=0;
    DDSp_SP_FL.ldddspu1ZoomLPFInitialOkFlg=0;
    DDSp_SP_FL.ldddsps16ZoomLPFInitialOkCnt=0;   
                
    DDSp_SP_FR.ldddsps16Time1PulseResidue=0;
    DDSp_SP_FR.ldddsps16WradOld=0;
    DDSp_SP_FR.ldddsps16BPFInitialOkCnt=0;
    DDSp_SP_FR.ldddsps16AngularSpeedHpfRes=0;
    DDSp_SP_FR.ldddsps16AngularSpeedBpfRes=0;
    DDSp_SP_FR.ldddspu1BPFInitialOkFlg=0;
    DDSp_SP_FR.ldddsps16ResmpTimeCnt=0;
    DDSp_SP_FR.ldddspu1ZoomLPFInitialOkFlg=0;
    DDSp_SP_FR.ldddsps16ZoomLPFInitialOkCnt=0;   
        
    DDSp_SP_RL.ldddsps16Time1PulseResidue=0;
    DDSp_SP_RL.ldddsps16WradOld=0;
    DDSp_SP_RL.ldddsps16BPFInitialOkCnt=0;
    DDSp_SP_RL.ldddsps16AngularSpeedHpfRes=0;
    DDSp_SP_RL.ldddsps16AngularSpeedBpfRes=0;
    DDSp_SP_RL.ldddspu1BPFInitialOkFlg=0;
    DDSp_SP_RL.ldddsps16ResmpTimeCnt=0;
    DDSp_SP_RL.ldddspu1ZoomLPFInitialOkFlg=0;
    DDSp_SP_RL.ldddsps16ZoomLPFInitialOkCnt=0;   
        
    DDSp_SP_RR.ldddsps16Time1PulseResidue=0;
    DDSp_SP_RR.ldddsps16WradOld=0;
    DDSp_SP_RR.ldddsps16BPFInitialOkCnt=0;
    DDSp_SP_RR.ldddsps16AngularSpeedHpfRes=0;
    DDSp_SP_RR.ldddsps16AngularSpeedBpfRes=0;
    DDSp_SP_RR.ldddspu1BPFInitialOkFlg=0;
    DDSp_SP_RR.ldddsps16ResmpTimeCnt=0;
    DDSp_SP_RR.ldddspu1ZoomLPFInitialOkFlg=0;
    DDSp_SP_RR.ldddsps16ZoomLPFInitialOkCnt=0;    
    

   
}
/*=================================================================*/
#if defined __MATLAB_COMPILE
#else
    #if defined(__LOGGER)
        #if __LOGGER_DATA_TYPE==1
#include "LSCallLogData.h"
            #if defined DDSplus_DATA_LOG_MODE
void LDDDSp_vCallDDSplusLogData(void)
{
    UCHAR i;
    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter); 
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);          
    CAN_LOG_DATA[2]  = (UCHAR)(FL.vrad_resol_change);   
    CAN_LOG_DATA[3]  = (UCHAR)(FL.vrad_resol_change>>8);   
    CAN_LOG_DATA[4]  = (UCHAR)(FR.vrad_resol_change);
    CAN_LOG_DATA[5]  = (UCHAR)(FR.vrad_resol_change>>8); 
    CAN_LOG_DATA[6]  = (UCHAR)(RL.vrad_resol_change);
    CAN_LOG_DATA[7]  = (UCHAR)(RL.vrad_resol_change>>8);                         
    CAN_LOG_DATA[8]  = (UCHAR)(RR.vrad_resol_change);
    CAN_LOG_DATA[9]  = (UCHAR)(RR.vrad_resol_change>>8);                     
    CAN_LOG_DATA[10] = (UCHAR)(vref_resol_change);
    CAN_LOG_DATA[11] = (UCHAR)(vref_resol_change>>8);    
    
    CAN_LOG_DATA[12] = (UCHAR)(((eng_torq_abs/20)>120)?120:(((eng_torq_abs/20)<-120)?-120:(CHAR)(eng_torq/20)));       
    CAN_LOG_DATA[13] = (UCHAR)(GoodC_Flag);//(mtp);  		                                                                                                                                  
    CAN_LOG_DATA[14] = (UCHAR)(((alat/10)>127)?127:(((alat/10)<-127)?-127:(CHAR)(alat/10)));          
    CAN_LOG_DATA[15] = (UCHAR)(((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(CHAR)(wstr/10)));       
    CAN_LOG_DATA[16] = (UCHAR)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(CHAR)(yaw_out/10))); 
    CAN_LOG_DATA[17] = (UCHAR)(((mpress/10)>250)?250:(((mpress/10)<0)?0:(CHAR)(mpress/10)));   
    /*
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16AngularSpeedResmp[FL_WL][0]); 
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16AngularSpeedResmp[FL_WL][0]>>8); 
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][0]); 
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][0]>>8);     
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16AngularSpeedResmp[RL_WL][0]); 
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16AngularSpeedResmp[RL_WL][0]>>8);     
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16AngularSpeedResmp[RR_WL][0]); 
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16AngularSpeedResmp[RR_WL][0]>>8);     
    
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16AngularSpeedBpf[FL_WL][0]); 
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16AngularSpeedBpf[FL_WL][0]>>8); 
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][0]); 
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][0]>>8);     
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16AngularSpeedBpf[RL_WL][0]); 
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16AngularSpeedBpf[RL_WL][0]>>8);     
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16AngularSpeedBpf[RR_WL][0]); 
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16AngularSpeedBpf[RR_WL][0]>>8);      
    
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16WradZoom[FL_WL][0]); 
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16WradZoom[FL_WL][0]>>8); 
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16WradZoom[FR_WL][0]); 
    CAN_LOG_DATA[37] = (UCHAR)(ldddsps16WradZoom[FR_WL][0]>>8);     
    CAN_LOG_DATA[38] = (UCHAR)(ldddsps16WradZoom[RL_WL][0]); 
    CAN_LOG_DATA[39] = (UCHAR)(ldddsps16WradZoom[RL_WL][0]>>8);     
    CAN_LOG_DATA[40] = (UCHAR)(ldddsps16WradZoom[RR_WL][0]); 
    CAN_LOG_DATA[41] = (UCHAR)(ldddsps16WradZoom[RR_WL][0]>>8);      
        
    CAN_LOG_DATA[42] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[43] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib>>8);
    CAN_LOG_DATA[44] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[45] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib>>8);
    CAN_LOG_DATA[46] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[47] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib>>8);   
    CAN_LOG_DATA[48] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[49] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib>>8);
    
    CAN_LOG_DATA[50] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[51] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[52] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[53] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[54] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[55] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct>>8);   
    CAN_LOG_DATA[56] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[57] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct>>8);
    */        
   
/*       
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]); 
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]>>8);    
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]); 
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]>>8);  
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]); 
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]>>8);    
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]); 
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]>>8);  
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][4]); 
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][4]>>8); 
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]); 
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]>>8);    
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]); 
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]>>8);  
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]); 
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]>>8);    
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]); 
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]>>8);  
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][4]); 
    CAN_LOG_DATA[37] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][4]>>8);      

    CAN_LOG_DATA[38] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]); 
    CAN_LOG_DATA[39] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]>>8);    
    CAN_LOG_DATA[40] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]); 
    CAN_LOG_DATA[41] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]>>8);  
    CAN_LOG_DATA[42] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]); 
    CAN_LOG_DATA[43] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]>>8);    
    CAN_LOG_DATA[44] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]); 
    CAN_LOG_DATA[45] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]>>8);  
    CAN_LOG_DATA[46] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][4]); 
    CAN_LOG_DATA[47] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][4]>>8); 
   
    CAN_LOG_DATA[48] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]); 
    CAN_LOG_DATA[49] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]>>8);    
    CAN_LOG_DATA[50] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]); 
    CAN_LOG_DATA[51] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]>>8);  
    CAN_LOG_DATA[52] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]); 
    CAN_LOG_DATA[53] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]>>8);    
    CAN_LOG_DATA[54] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]); 
    CAN_LOG_DATA[55] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]>>8);  
    CAN_LOG_DATA[56] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][4]); 
    CAN_LOG_DATA[57] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][4]>>8);     
    
    CAN_LOG_DATA[58] = (UCHAR)(DDSp_SP_FL.ldddspu8StoredPeriodCnt);  
    CAN_LOG_DATA[59] = (UCHAR)(DDSp_SP_FR.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[60] = (UCHAR)(DDSp_SP_RL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[61] = (UCHAR)(DDSp_SP_RR.ldddspu8StoredPeriodCnt);      
*/ 
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][0]); 
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][0]>>8);    
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][1]); 
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][1]>>8);  
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][2]); 
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][2]>>8);    
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][3]); 
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16ElapsedTime1PulseCrt[FR_WL][3]>>8);  
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][0]);    
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][0]>>8);  
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][1]);    
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][1]>>8); 
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][2]);    
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][2]>>8); 
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][3]);    
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16AngularSpeedCrt[FR_WL][3]>>8); 
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][0]);   
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][0]>>8); 
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][1]);   
    CAN_LOG_DATA[37] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][1]>>8);      
    CAN_LOG_DATA[38] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][2]);   
    CAN_LOG_DATA[39] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][2]>>8);    
    CAN_LOG_DATA[40] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][3]);   
    CAN_LOG_DATA[41] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][3]>>8);  
    CAN_LOG_DATA[42] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][4]);   
    CAN_LOG_DATA[43] = (UCHAR)(ldddsps16AngularSpeedResmp[FR_WL][4]>>8);    
    CAN_LOG_DATA[44] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][0]);     
    CAN_LOG_DATA[45] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][0]>>8);  
    CAN_LOG_DATA[46] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][1]);     
    CAN_LOG_DATA[47] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][1]>>8);  
    CAN_LOG_DATA[48] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][2]);     
    CAN_LOG_DATA[49] = (UCHAR)(ldddsps16AngularSpeedBpf[FR_WL][2]>>8);   
    CAN_LOG_DATA[50] = (UCHAR)(ldddsps16WradZoom[FR_WL][0]);       
    CAN_LOG_DATA[51] = (UCHAR)(ldddsps16WradZoom[FR_WL][0]>>8);                              
    CAN_LOG_DATA[52] = (UCHAR)(ldddsps16WradZoom[FR_WL][1]);                                             
    CAN_LOG_DATA[53] = (UCHAR)(ldddsps16WradZoom[FR_WL][1]>>8);                              
    CAN_LOG_DATA[54] = (UCHAR)(DDSp_SP_FR.ldddsps16AngularSpeedBpfRes);                           
    CAN_LOG_DATA[55] = (UCHAR)(DDSp_SP_FR.ldddsps16AngularSpeedBpfRes>>8);  
    CAN_LOG_DATA[56] = (UCHAR)(DDSp_SP_FR.ldddsps16AngularSpeedHpfRes); 
    CAN_LOG_DATA[57] = (UCHAR)(DDSp_SP_FR.ldddsps16AngularSpeedHpfRes>>8);                         
    
    CAN_LOG_DATA[58] = (UCHAR)(DDSp_SP_FR.ldddsps16WradResmpNum);  
    CAN_LOG_DATA[59] = (UCHAR)(DDSp_SP_FR.ldddsps16WradResmpNumDeci);
    CAN_LOG_DATA[60] = (UCHAR)(DDSp_SP_FR.ldddsps16Time1PulseResidue);/*UCHAR*/
    CAN_LOG_DATA[61] = (UCHAR)(DDSp_SP_FR.ldddspu8StoredPeriodCnt); 
    
    
    CAN_LOG_DATA[62] = (UCHAR)(ldddspu8DdsPlusMode);
    CAN_LOG_DATA[63] = (UCHAR)(ldddspu8DdsPlusSubMode);    

    CAN_LOG_DATA[64] = (UCHAR)((DDSp_SP_FL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[65] = (UCHAR)((DDSp_SP_FR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FR.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[66] = (UCHAR)((DDSp_SP_RL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[67] = (UCHAR)((DDSp_SP_RR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RR.ldddsps16TireFreqDiff));  
if(ldddspu1AllWhlCalibCompleteFlg==0){
    CAN_LOG_DATA[68] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[69] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib>>8);
    CAN_LOG_DATA[70] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][0][ldddspu8VehSpdInx]);
    CAN_LOG_DATA[71] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][0][ldddspu8VehSpdInx]>>8);
    CAN_LOG_DATA[72] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][1][ldddspu8VehSpdInx]);
    CAN_LOG_DATA[73] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][1][ldddspu8VehSpdInx]>>8);   
    CAN_LOG_DATA[74] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib);
    CAN_LOG_DATA[75] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib>>8);
    
}
else{
    CAN_LOG_DATA[68] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[69] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[70] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][0]);
    CAN_LOG_DATA[71] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][0]>>8);
    CAN_LOG_DATA[72] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][1]);
    CAN_LOG_DATA[73] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][1]>>8);   
    CAN_LOG_DATA[74] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[75] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct>>8);
}
        i=0;
        if (ABS_fz                          ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (ldddspu1StdStrDrvStateForDDS                          ==1) i|=0x04;
        if (YAW_CDC_WORK                    ==1) i|=0x08;
        if (ESP_TCS_ON                     ==1) i|=0x10;
        if (ESP_ERROR_FLG                   ==1) i|=0x20;
        if (BRAKING_STATE                   ==1) i|=0x40;
        if (BRAKE_SIGNAL                    ==1) i|=0x80;
        CAN_LOG_DATA[76] =(UCHAR) (i);      

        i=0;
        if (DDSp_SP_FL.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x08;
if(ldddspu1AllWhlCalibCompleteFlg==0){            
        if (DDSp_SP_FL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x80;
}
else{
        if (DDSp_SP_FL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x80;    
}            
        CAN_LOG_DATA[77] =(UCHAR) (i);    
        
        i=0;
 if(ldddspu1AllWhlCalibCompleteFlg==0){         
        if (DDSp_SP_FL.ldddspu1CalibCompleteFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1CalibCompleteFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1CalibCompleteFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1CalibCompleteFlg                   ==1) i|=0x08;
}
else{
        if (DDSp_SP_FL.ldddspu1LowTirePressFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1LowTirePressFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1LowTirePressFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1LowTirePressFlg                   ==1) i|=0x08;    
}            
        if (DDSp_SP_FL.ldddspu1ZoomLPFInitialOkFlg                 ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1ZoomLPFInitialOkFlg                 ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1ZoomLPFInitialOkFlg                 ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1ZoomLPFInitialOkFlg                 ==1) i|=0x80;
        CAN_LOG_DATA[78] =(UCHAR) (i);    
        
        i=0;
if(ldddspu1AllWhlCalibCompleteFlg==0){        
        if (DDSp_SP_FL.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x08;
}
else{
        if (DDSp_SP_FL.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x08;    
}            
        if (ldddsu1StdStrDrvStateForDDS                 ==1) i|=0x10;
        if (ldddsu1DeflationState                 ==1) i|=0x20;
        if (ldddspu1AllWhlCalibCompleteFlg                 ==1) i|=0x40;
        if (ldddsu1RequestDDSReset                 ==1) i|=0x80;
        CAN_LOG_DATA[79] =(UCHAR) (i);                                   	    
}
            #else
void LDDDSp_vCallDDSplusLogData(void)
{
    UCHAR i;
    CAN_LOG_DATA[0]  = (UCHAR)(system_loop_counter); 
    CAN_LOG_DATA[1]  = (UCHAR)(system_loop_counter>>8);          
    CAN_LOG_DATA[2]  = (UCHAR)(ldddsu16ElapsedTimePerRev[FL_WL]);//(FL.vrad_resol_change);   
    CAN_LOG_DATA[3]  = (UCHAR)(ldddsu16ElapsedTimePerRev[FL_WL]>>8);//(FL.vrad_resol_change>>8);   
    CAN_LOG_DATA[4]  = (UCHAR)(ldddsu16ElapsedTimePerRev[FR_WL]);//(FR.vrad_resol_change);
    CAN_LOG_DATA[5]  = (UCHAR)(ldddsu16ElapsedTimePerRev[FR_WL]>>8);//(FR.vrad_resol_change>>8); 
    CAN_LOG_DATA[6]  = (UCHAR)(ldddsu16ElapsedTimePerRev[RL_WL]);//(RL.vrad_resol_change);
    CAN_LOG_DATA[7]  = (UCHAR)(ldddsu16ElapsedTimePerRev[RL_WL]>>8);//(RL.vrad_resol_change>>8);                         
    CAN_LOG_DATA[8]  = (UCHAR)(ldddsu16ElapsedTimePerRev[RR_WL]);//(RR.vrad_resol_change);
    CAN_LOG_DATA[9]  = (UCHAR)(ldddsu16ElapsedTimePerRev[RR_WL]>>8);//(RR.vrad_resol_change>>8);                     
    CAN_LOG_DATA[10] = (UCHAR)(vref_resol_change);
    CAN_LOG_DATA[11] = (UCHAR)(vref_resol_change>>8);    
    
    CAN_LOG_DATA[12] = (UCHAR)(((eng_torq_abs/20)>120)?120:(((eng_torq_abs/20)<-120)?-120:(CHAR)(eng_torq/20)));       
    CAN_LOG_DATA[13] = (UCHAR)(GoodC_Flag);//(mtp);  		                                                                                                                                  
    CAN_LOG_DATA[14] = (UCHAR)(((alat/10)>127)?127:(((alat/10)<-127)?-127:(CHAR)(alat/10)));          
    CAN_LOG_DATA[15] = (UCHAR)(((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(CHAR)(wstr/10)));       
    CAN_LOG_DATA[16] = (UCHAR)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(CHAR)(yaw_out/10))); 
    CAN_LOG_DATA[17] = (UCHAR)(((mpress/10)>250)?250:(((mpress/10)<0)?0:(CHAR)(mpress/10)));   
    
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]); 
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]>>8);    
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]); 
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]>>8);  
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]); 
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]>>8);    
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]); 
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]>>8);  
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]);    
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]>>8);      
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]);    
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]>>8);    
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]);    
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]>>8);  
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]);    
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]>>8);    
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]);   
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]>>8); 
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]);   
    CAN_LOG_DATA[37] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]>>8);        
    CAN_LOG_DATA[38] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]);   
    CAN_LOG_DATA[39] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]>>8);    
    CAN_LOG_DATA[40] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]);   
    CAN_LOG_DATA[41] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]>>8);  
    CAN_LOG_DATA[42] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]);   
    CAN_LOG_DATA[43] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]>>8);    
    CAN_LOG_DATA[44] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]);   
    CAN_LOG_DATA[45] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]>>8);  
    CAN_LOG_DATA[46] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]);   
    CAN_LOG_DATA[47] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]>>8);   
    CAN_LOG_DATA[48] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]);   
    CAN_LOG_DATA[49] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]>>8);    
    CAN_LOG_DATA[50] = (UCHAR)(DDSp_SP_FL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[51] = (UCHAR)(DDSp_SP_FR.ldddspu8StoredPeriodCnt);  
    CAN_LOG_DATA[52] = (UCHAR)(DDSp_SP_RL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[53] = (UCHAR)(DDSp_SP_RR.ldddspu8StoredPeriodCnt);    
 
if(ldddspu1AllWhlCalibCompleteFlg==0){
    CAN_LOG_DATA[54] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[55] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]>>8);
    CAN_LOG_DATA[56] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[57] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]>>8);
    CAN_LOG_DATA[58] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[59] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]>>8);   
    CAN_LOG_DATA[60] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[61] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib[ldddspu8VehSpdInx]>>8);
    CAN_LOG_DATA[62] = (UCHAR)(DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[63] = (UCHAR)(DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[64] = (UCHAR)(DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);
    CAN_LOG_DATA[65] = (UCHAR)(DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInx]);    
}        
else{
    CAN_LOG_DATA[54] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[55] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[56] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[57] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[58] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[59] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct>>8);   
    CAN_LOG_DATA[60] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[61] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct>>8);        
    CAN_LOG_DATA[62] = (UCHAR)(DDSp_SP_FL.ldddsps16DefDctCnt);  
    CAN_LOG_DATA[63] = (UCHAR)(DDSp_SP_FR.ldddsps16DefDctCnt);
    CAN_LOG_DATA[64] = (UCHAR)(DDSp_SP_RL.ldddsps16DefDctCnt);
    CAN_LOG_DATA[65] = (UCHAR)(DDSp_SP_RR.ldddsps16DefDctCnt); 
}
    
    CAN_LOG_DATA[66] = (UCHAR)(ldddspu8DdsPlusMode);
    CAN_LOG_DATA[67] = (UCHAR)(ldddspu8DdsPlusSubMode);    
/*
    CAN_LOG_DATA[64] = (UCHAR)((DDSp_SP_FL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[65] = (UCHAR)((DDSp_SP_FR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FR.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[66] = (UCHAR)((DDSp_SP_RL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[67] = (UCHAR)((DDSp_SP_RR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RR.ldddsps16TireFreqDiff));  
*/


    if(ldddspu1AllWhlCalibCompleteFlg==0){
        CAN_LOG_DATA[68] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][0][ldddspu8VehSpdInx]);
        CAN_LOG_DATA[69] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][0][ldddspu8VehSpdInx]>>8);
        CAN_LOG_DATA[70] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[RR_WL][0][ldddspu8VehSpdInx]);
        CAN_LOG_DATA[71] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[RR_WL][0][ldddspu8VehSpdInx]>>8);
        CAN_LOG_DATA[72] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][1][ldddspu8VehSpdInx]);    
        CAN_LOG_DATA[73] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[FR_WL][1][ldddspu8VehSpdInx]>>8);
        CAN_LOG_DATA[74] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[RR_WL][1][ldddspu8VehSpdInx]);
        CAN_LOG_DATA[75] = (UCHAR)(ldddsps16SysParaCumulAvgCalib[RR_WL][1][ldddspu8VehSpdInx]>>8);
    }
    else{
        CAN_LOG_DATA[68] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][0]);    
        CAN_LOG_DATA[69] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][0]>>8);
        CAN_LOG_DATA[70] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[RR_WL][0]);
        CAN_LOG_DATA[71] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[RR_WL][0]>>8);
        CAN_LOG_DATA[72] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][1]);    
        CAN_LOG_DATA[73] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[FR_WL][1]>>8);
        CAN_LOG_DATA[74] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[RR_WL][1]);
        CAN_LOG_DATA[75] = (UCHAR)0;//(ldddsps16SysParaMovingAvgDef[RR_WL][1]>>8);
        
    }                

        i=0;
        if (ABS_fz                          ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (ldddspu1StdStrDrvStateForDDS                          ==1) i|=0x04;
        if (YAW_CDC_WORK                    ==1) i|=0x08;
        if (ESP_TCS_ON                     ==1) i|=0x10;
        if (ldddsu1DeflationState                   ==1) i|=0x20;
        if (ldddsu1Diag2whlDeflationFLRR                   ==1) i|=0x40;
        if (ldddsu1Diag2whlDeflationFRRL                    ==1) i|=0x80;
        CAN_LOG_DATA[76] =(UCHAR) (i);      

        i=0;
        
        /*
        if (DDSp_SP_FL.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1SteadyStraightDrvFlg                 ==1) i|=0x08;
        */
        if (ldddsu8Other3whlDeflation[FL_WL]                 ==1) i|=0x01;
        if (ldddsu8Other3whlDeflation[FR_WL]                 ==1) i|=0x02;
        if (ldddsu8Other3whlDeflation[RL_WL]                 ==1) i|=0x04;
        if (ldddsu8Other3whlDeflation[RR_WL]                 ==1) i|=0x08;
            
                        
if(ldddspu1AllWhlCalibCompleteFlg==0){            
        if (DDSp_SP_FL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x80;
}
else{
        if (DDSp_SP_FL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x80;    
}            
        CAN_LOG_DATA[77] =(UCHAR) (i);    
        
        i=0;
 if(ldddspu1AllWhlCalibCompleteFlg==0){         
        if (ldddspu1CalibCompleteFlgFL                   ==1) i|=0x01;
        if (ldddspu1CalibCompleteFlgFR                   ==1) i|=0x02;
        if (ldddspu1CalibCompleteFlgRL                   ==1) i|=0x04;
        if (ldddspu1CalibCompleteFlgRR                   ==1) i|=0x08;
}
else{
        if (DDSp_SP_FL.ldddspu1LowTirePressFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1LowTirePressFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1LowTirePressFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1LowTirePressFlg                   ==1) i|=0x08;    
}            
        if (ldddsu8ElapsedTimeUpdateFlg[FL_WL]                 ==1) i|=0x10;
        if (ldddsu8ElapsedTimeUpdateFlg[FR_WL]                 ==1) i|=0x20;
        if (ldddsu8ElapsedTimeUpdateFlg[RL_WL]                 ==1) i|=0x40;
        if (ldddsu8ElapsedTimeUpdateFlg[RR_WL]                 ==1) i|=0x80;
        CAN_LOG_DATA[78] =(UCHAR) (i);    
        
        i=0;
/*
if(ldddspu1AllWhlCalibCompleteFlg==0){        
        if (DDSp_SP_FL.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1CalibInitCalOkFlg                   ==1) i|=0x08;
}
else{
        if (DDSp_SP_FL.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1DefDctInitCalOkFlg                   ==1) i|=0x08;    
}            
*/
        if (ldddsu8SingleWhlDeflation[FL_WL]                   ==1) i|=0x01;
        if (ldddsu8SingleWhlDeflation[FR_WL]                   ==1) i|=0x02;
        if (ldddsu8SingleWhlDeflation[RL_WL]                   ==1) i|=0x04;
        if (ldddsu8SingleWhlDeflation[RR_WL]                   ==1) i|=0x08;


        if (ldddsu1StdStrDrvStateForDDS                 ==1) i|=0x10;
        if (ldddsu1DeflationState                 ==1) i|=0x20;
        if (ldddspu1AllWhlCalibCompleteFlg                 ==1) i|=0x40;
        if (ldddsu1RequestDDSReset                 ==1) i|=0x80;
        CAN_LOG_DATA[79] =(UCHAR) (i);                                   	    
}
            #endif
void LDDDSp_vCallDDSplusIntegLogData(void)
{
    UCHAR i,whl_i;
    
    CAN_LOG_DATA[0] = (UCHAR)(system_loop_counter); 
    CAN_LOG_DATA[1] = (UCHAR)(system_loop_counter>>8);    
    CAN_LOG_DATA[2] = (UCHAR)(vref_resol_change/32);
    CAN_LOG_DATA[3] = (UCHAR)(((eng_torq_abs/20)>120)?120:(((eng_torq_abs/20)<-120)?-120:(CHAR)(eng_torq/20)));          
    CAN_LOG_DATA[4] = (UCHAR)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(CHAR)(yaw_out/10))); 
    
    CAN_LOG_DATA[5] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]); 
    CAN_LOG_DATA[6] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]>>8);    
    CAN_LOG_DATA[7] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]); 
    CAN_LOG_DATA[8] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]>>8);  
    CAN_LOG_DATA[9] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]); 
    CAN_LOG_DATA[10] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]>>8);    
    CAN_LOG_DATA[11] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]); 
    CAN_LOG_DATA[12] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]>>8);  
    CAN_LOG_DATA[13] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]);    
    CAN_LOG_DATA[14] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]>>8);      
    CAN_LOG_DATA[15] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]);    
    CAN_LOG_DATA[16] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]>>8);    
    CAN_LOG_DATA[17] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]);    
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]>>8);  
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]);    
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]>>8);    
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]);   
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]>>8); 
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]);   
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]>>8);        
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]);   
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]>>8);    
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]);   
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]>>8);  
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]);   
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]>>8);    
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]);   
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]>>8);  
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]);   
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]>>8);   
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]);   
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]>>8);    
    CAN_LOG_DATA[37] = (UCHAR)(DDSp_SP_FL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[38] = (UCHAR)(DDSp_SP_FR.ldddspu8StoredPeriodCnt);  
    CAN_LOG_DATA[39] = (UCHAR)(DDSp_SP_RL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[40] = (UCHAR)(DDSp_SP_RR.ldddspu8StoredPeriodCnt);    
 
if(ldddspu1AllWhlCalibCompleteFlg==0){
    CAN_LOG_DATA[41] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[42] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]>>8);
    CAN_LOG_DATA[43] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[44] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]>>8);
    CAN_LOG_DATA[45] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[46] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]>>8);   
    CAN_LOG_DATA[47] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[48] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqCalib[ldddspu8VehSpdInxTemp]>>8);
    CAN_LOG_DATA[49] = (UCHAR)(DDSp_SP_FL.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[50] = (UCHAR)(DDSp_SP_FR.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[51] = (UCHAR)(DDSp_SP_RL.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInxTemp]);
    CAN_LOG_DATA[52] = (UCHAR)(DDSp_SP_RR.ldddsps16CalCumulAvgCalibCnt[ldddspu8VehSpdInxTemp]);    
}        
else{
    CAN_LOG_DATA[41] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[42] = (UCHAR)(DDSp_SP_FL.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[43] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[44] = (UCHAR)(DDSp_SP_FR.ldddsps16TireFreqDefDct>>8);
    CAN_LOG_DATA[45] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[46] = (UCHAR)(DDSp_SP_RL.ldddsps16TireFreqDefDct>>8);   
    CAN_LOG_DATA[47] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct);
    CAN_LOG_DATA[48] = (UCHAR)(DDSp_SP_RR.ldddsps16TireFreqDefDct>>8);        
    CAN_LOG_DATA[49] = (UCHAR)(DDSp_SP_FL.ldddsps16DefDctCnt);  
    CAN_LOG_DATA[50] = (UCHAR)(DDSp_SP_FR.ldddsps16DefDctCnt);
    CAN_LOG_DATA[51] = (UCHAR)(DDSp_SP_RL.ldddsps16DefDctCnt);
    CAN_LOG_DATA[52] = (UCHAR)(DDSp_SP_RR.ldddsps16DefDctCnt); 
}
    
    CAN_LOG_DATA[53] = (UCHAR)(ldddspu8DdsPlusMode);
    CAN_LOG_DATA[54] = (UCHAR)(ldddspu8DdsPlusSubMode);  
      
/*
    CAN_LOG_DATA[64] = (UCHAR)((DDSp_SP_FL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[65] = (UCHAR)((DDSp_SP_FR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FR.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[66] = (UCHAR)((DDSp_SP_RL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[67] = (UCHAR)((DDSp_SP_RR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RR.ldddsps16TireFreqDiff));  
*/
/*
	CAN_LOG_DATA[55] = (UCHAR)((ldddss8PressEstByAxleDiff[FL_WL]>127)?127:((ldddss8PressEstByAxleDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[FL_WL])); 
	CAN_LOG_DATA[56] = (UCHAR)((ldddss8PressEstBySideDiff[FL_WL]>127)?127:((ldddss8PressEstBySideDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[FL_WL])); 
	CAN_LOG_DATA[57] = (UCHAR)((ldddss8PressEstByDiagDiff[FL_WL]>127)?127:((ldddss8PressEstByDiagDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[FL_WL])); 
	CAN_LOG_DATA[58] = (UCHAR)((ldddss8PressEstByAxleDiff[FR_WL]>127)?127:((ldddss8PressEstByAxleDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[FR_WL]));                                                   
	CAN_LOG_DATA[59] = (UCHAR)((ldddss8PressEstBySideDiff[FR_WL]>127)?127:((ldddss8PressEstBySideDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[FR_WL])); 
	CAN_LOG_DATA[60] = (UCHAR)((ldddss8PressEstByDiagDiff[FR_WL]>127)?127:((ldddss8PressEstByDiagDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[FR_WL]));                                                   
	CAN_LOG_DATA[61] = (UCHAR)((ldddss8PressEstByAxleDiff[RL_WL]>127)?127:((ldddss8PressEstByAxleDiff[RL_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[RL_WL])); 
	CAN_LOG_DATA[62] = (UCHAR)((ldddss8PressEstBySideDiff[RL_WL]>127)?127:((ldddss8PressEstBySideDiff[RL_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[RL_WL])); 
	CAN_LOG_DATA[63] = (UCHAR)((ldddss8PressEstByDiagDiff[RL_WL]>127)?127:((ldddss8PressEstByDiagDiff[RL_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[RL_WL])); 
	CAN_LOG_DATA[64] = (UCHAR)((ldddss8PressEstByAxleDiff[RR_WL]>127)?127:((ldddss8PressEstByAxleDiff[RR_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[RR_WL]));                                                   
	CAN_LOG_DATA[65] = (UCHAR)((ldddss8PressEstBySideDiff[RR_WL]>127)?127:((ldddss8PressEstBySideDiff[RR_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[RR_WL])); 
	CAN_LOG_DATA[66] = (UCHAR)((ldddss8PressEstByDiagDiff[RR_WL]>127)?127:((ldddss8PressEstByDiagDiff[RR_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[RR_WL]));                                                   
*/
	CAN_LOG_DATA[55] = (UCHAR)(ldddsps16CumulVehSpdCalib[FL_WL][0]); 
	CAN_LOG_DATA[56] = (UCHAR)(ldddsps16CumulVehSpdCalib[FL_WL][1]); 
	CAN_LOG_DATA[57] = (UCHAR)(ldddsps16CumulVehSpdCalib[FL_WL][2]); 
	CAN_LOG_DATA[58] = (UCHAR)(ldddsps16CumulVehSpdCalib[FL_WL][3]);                                                   
	CAN_LOG_DATA[59] = (UCHAR)(ldddsps16CumulVehSpdCalib[FL_WL][4]); 
	CAN_LOG_DATA[60] = (UCHAR)(ldddsps16TireFreqCalibFL);                                                   
	CAN_LOG_DATA[61] = (UCHAR)(ldddsps16TireFreqCalibFL>>8); 
	CAN_LOG_DATA[62] = (UCHAR)(ldddsps16TireFreqCalibFR); 
	CAN_LOG_DATA[63] = (UCHAR)(ldddsps16TireFreqCalibFR>>8); 
	CAN_LOG_DATA[64] = (UCHAR)(ldddsps16TireFreqCalibRL);                                                   
	CAN_LOG_DATA[65] = (UCHAR)(ldddsps16TireFreqCalibRL>>8); 
	CAN_LOG_DATA[66] = (UCHAR)(ldddsps16TireFreqCalibRR/10);                                                   

    CAN_LOG_DATA[67] = (UCHAR)(ldddspu8VehSpdInx);
    CAN_LOG_DATA[68] = (UCHAR)((DDSp_SP_FL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[69] = (UCHAR)((DDSp_SP_FR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_FR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_FR.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[70] = (UCHAR)((DDSp_SP_RL.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RL.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RL.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[71] = (UCHAR)((DDSp_SP_RR.ldddsps16TireFreqDiff>127)?127:((DDSp_SP_RR.ldddsps16TireFreqDiff<-127)?-127:(CHAR)DDSp_SP_RR.ldddsps16TireFreqDiff));  
    CAN_LOG_DATA[72] = (UCHAR)(ldddspu8VehSpdInxTemp);
    CAN_LOG_DATA[73] = (UCHAR)(ldddsps16CumulSumVehSpdCalib[0][0]);
    CAN_LOG_DATA[74] = (UCHAR)(ldddsps16CumulSumVehSpdCalib[0][0]>>8);
/*          
   	CAN_LOG_DATA[67] = (UCHAR)ldddsu8RunningAvgUpdateWhlIndex[0];
    
if(ldddsu8RunningAvgUpdateWhlIndex[0]<4){
    whl_i=ldddsu8RunningAvgUpdateWhlIndex[0];
	CAN_LOG_DATA[68] = (UCHAR)((ldddss16Dct1WhlDefCnt[whl_i][0]>250)?250:((ldddss16Dct1WhlDefCnt[whl_i][0]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[whl_i][0])); 
	CAN_LOG_DATA[69] = (UCHAR)((ldddss16Dct1WhlDefCnt[whl_i][1]>250)?250:((ldddss16Dct1WhlDefCnt[whl_i][1]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[whl_i][1]));  	
	CAN_LOG_DATA[70] = (UCHAR)((ldddss16Dct1WhlDefCnt[whl_i][2]>250)?250:((ldddss16Dct1WhlDefCnt[whl_i][2]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[whl_i][2]));                         
	CAN_LOG_DATA[71] = (UCHAR)((ldddss16Dct1WhlDefCnt[whl_i][3]>250)?250:((ldddss16Dct1WhlDefCnt[whl_i][3]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[whl_i][3]));       					                           
	CAN_LOG_DATA[72] = (UCHAR)((ldddss16Dct1WhlDefCnt[whl_i][4]>250)?250:((ldddss16Dct1WhlDefCnt[whl_i][4]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[whl_i][4]));   					 
	CAN_LOG_DATA[73] = (UCHAR)((ldddss16Dct3WhlDefCnt[whl_i][0]>250)?250:((ldddss16Dct3WhlDefCnt[whl_i][0]<0)?0:(UCHAR)ldddss16Dct3WhlDefCnt[whl_i][0]));       					                           
	CAN_LOG_DATA[74] = (UCHAR)((ldddss16Dct3WhlDefCnt[whl_i][1]>250)?250:((ldddss16Dct3WhlDefCnt[whl_i][1]<0)?0:(UCHAR)ldddss16Dct3WhlDefCnt[whl_i][1]));   					 

}
else if (ldddsu8RunningAvgUpdateWhlIndex[0]==4){
	CAN_LOG_DATA[68] = (UCHAR)((ldddss16DctFLRRDefCnt>250)?250:((ldddss16DctFLRRDefCnt<0)?0:(UCHAR)ldddss16DctFLRRDefCnt)); 	
	CAN_LOG_DATA[69] = (UCHAR)((ldddss16DctFRRLDefCnt>250)?250:((ldddss16DctFRRLDefCnt<0)?0:(UCHAR)ldddss16DctFRRLDefCnt)); 

    CAN_LOG_DATA[70] = (UCHAR)((CalibrationValueFL>127)?127:((CalibrationValueFL<-127)?-127:(CHAR)CalibrationValueFL));
    CAN_LOG_DATA[71] = (UCHAR)((CalibrationValueFR>127)?127:((CalibrationValueFR<-127)?-127:(CHAR)CalibrationValueFR));
    CAN_LOG_DATA[72] = (UCHAR)((CalibrationValueRL>127)?127:((CalibrationValueRL<-127)?-127:(CHAR)CalibrationValueRL));
    CAN_LOG_DATA[73] = (UCHAR)((CalibrationValueRR>127)?127:((CalibrationValueRR<-127)?-127:(CHAR)CalibrationValueRR));

}
else{
    ;
}
*/
        i=0;
        if (ABS_fz                          ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (YAW_CDC_WORK                          ==1) i|=0x04;
        if (ESP_TCS_ON                    ==1) i|=0x08;
        if (ldddspu1StdStrDrvStateForDDS                     ==1) i|=0x10;
        if (ldddsu1DeflationState                 ==1) i|=0x20;
        if (ldddspu1AllWhlCalibCompleteFlg                 ==1) i|=0x40;
        if (ldddsu1RequestDDSReset                 ==1) i|=0x80;
        CAN_LOG_DATA[75] =(UCHAR) (i);      
        
        i=0;
        if (ldddsu8CompleteCalib[0]                          ==1) i|=0x01;
        if (ldddsu8CompleteCalib[1]                             ==1) i|=0x02;
        if (ldddsu8CompleteCalib[2]                          ==1) i|=0x04;
        if (ldddsu1CompleteCalib                    ==1) i|=0x08;
        if (ldddsu1StdStrDrvStateForDDS                     ==1) i|=0x10;
        if (ldddsu1RunningAvgUpdateFlag                   ==1) i|=0x20;
        if (ldddsu1Diag2whlDeflationFLRR                   ==1) i|=0x40;
        if (ldddsu1Diag2whlDeflationFRRL                    ==1) i|=0x80;
        CAN_LOG_DATA[76] =(UCHAR) (i);      

        i=0;
        
        if (ldddsu8Other3whlDeflation[FL_WL]                 ==1) i|=0x01;
        if (ldddsu8Other3whlDeflation[FR_WL]                 ==1) i|=0x02;
        if (ldddsu8Other3whlDeflation[RL_WL]                 ==1) i|=0x04;
        if (ldddsu8Other3whlDeflation[RR_WL]                 ==1) i|=0x08;
        if (ldddsu8SingleWhlDeflation[FL_WL]                   ==1) i|=0x10;
        if (ldddsu8SingleWhlDeflation[FR_WL]                   ==1) i|=0x20;
        if (ldddsu8SingleWhlDeflation[RL_WL]                   ==1) i|=0x40;
        if (ldddsu8SingleWhlDeflation[RR_WL]                   ==1) i|=0x80;
                                               
        CAN_LOG_DATA[77] =(UCHAR) (i);    
        
        i=0;
 if(ldddspu1AllWhlCalibCompleteFlg==0){         
        if (ldddspu1CalibCompleteFlgFL                   ==1) i|=0x01;
        if (ldddspu1CalibCompleteFlgFR                   ==1) i|=0x02;
        if (ldddspu1CalibCompleteFlgRL                   ==1) i|=0x04;
        if (ldddspu1CalibCompleteFlgRR                   ==1) i|=0x08;
        if (DDSp_SP_FL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x80;            
}
else{
        if (DDSp_SP_FL.ldddspu1LowTirePressFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1LowTirePressFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1LowTirePressFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1LowTirePressFlg                   ==1) i|=0x08;    
        if (DDSp_SP_FL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x80;               
}            

        CAN_LOG_DATA[78] =(UCHAR) (i);    
        
        i=0;

        if (ldddsu1LowTirePressStateFlgFL                   ==1) i|=0x01;
        if (ldddsu1LowTirePressStateFlgFR                   ==1) i|=0x02;
        if (ldddsu1LowTirePressStateFlgRL                   ==1) i|=0x04;
        if (ldddsu1LowTirePressStateFlgRR                   ==1) i|=0x08;

        if (ldddsu8ElapsedTimeUpdateFlg[0]                 	==1) i|=0x10;   
        if (ldddsu8ElapsedTimeUpdateFlg[1]    				==1) i|=0x20;   
        if (ldddsu8ElapsedTimeUpdateFlg[2]                  ==1) i|=0x40;   
        if (ldddsu8ElapsedTimeUpdateFlg[3]                  ==1) i|=0x80;   
        CAN_LOG_DATA[79] =(UCHAR) (i);                                   	    
}
/*==========================================*/
void LDDDSp_vCallDDSplusDataSet(void)
{
    UCHAR i,whl_i;
    
    CAN_LOG_DATA[0] = (UCHAR)(system_loop_counter); 
    CAN_LOG_DATA[1] = (UCHAR)(system_loop_counter>>8);    
    
    CAN_LOG_DATA[2] = (UCHAR)(FL.lsabss16WhlSpdRawSignal64);
    CAN_LOG_DATA[3] = (UCHAR)(FL.lsabss16WhlSpdRawSignal64>>8);                       
    CAN_LOG_DATA[4] = (UCHAR)(FR.lsabss16WhlSpdRawSignal64);
    CAN_LOG_DATA[5] = (UCHAR)(FR.lsabss16WhlSpdRawSignal64>>8);                       
    CAN_LOG_DATA[6] = (UCHAR)(RL.lsabss16WhlSpdRawSignal64);
    CAN_LOG_DATA[7] = (UCHAR)(RL.lsabss16WhlSpdRawSignal64>>8);                       
    CAN_LOG_DATA[8] = (UCHAR)(RR.lsabss16WhlSpdRawSignal64);
    CAN_LOG_DATA[9] = (UCHAR)(RR.lsabss16WhlSpdRawSignal64>>8);                       
    CAN_LOG_DATA[10] = (UCHAR)(vref_resol_change);
    CAN_LOG_DATA[11] = (UCHAR)(vref_resol_change>>8);                              
    
    CAN_LOG_DATA[12] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]); 
    CAN_LOG_DATA[13] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][0]>>8);    
    CAN_LOG_DATA[14] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]); 
    CAN_LOG_DATA[15] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][1]>>8);  
    CAN_LOG_DATA[16] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]); 
    CAN_LOG_DATA[17] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][2]>>8);    
    CAN_LOG_DATA[18] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]); 
    CAN_LOG_DATA[19] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][3]>>8);  
    CAN_LOG_DATA[20] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][4]); 
    CAN_LOG_DATA[21] = (UCHAR)(ldddsps16StoredPeriodTime[FL_WL][4]>>8);      
    CAN_LOG_DATA[22] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]);    
    CAN_LOG_DATA[23] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][0]>>8);      
    CAN_LOG_DATA[24] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]);    
    CAN_LOG_DATA[25] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][1]>>8);    
    CAN_LOG_DATA[26] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]);    
    CAN_LOG_DATA[27] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][2]>>8);  
    CAN_LOG_DATA[28] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]);    
    CAN_LOG_DATA[29] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][3]>>8);    
    CAN_LOG_DATA[30] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][4]);    
    CAN_LOG_DATA[31] = (UCHAR)(ldddsps16StoredPeriodTime[FR_WL][4]>>8);     
    CAN_LOG_DATA[32] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]);   
    CAN_LOG_DATA[33] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][0]>>8); 
    CAN_LOG_DATA[34] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]);   
    CAN_LOG_DATA[35] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][1]>>8);        
    CAN_LOG_DATA[36] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]);   
    CAN_LOG_DATA[37] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][2]>>8);    
    CAN_LOG_DATA[38] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]);   
    CAN_LOG_DATA[39] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][3]>>8);  
    CAN_LOG_DATA[40] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][4]);   
    CAN_LOG_DATA[41] = (UCHAR)(ldddsps16StoredPeriodTime[RL_WL][4]>>8);   
    CAN_LOG_DATA[42] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]);   
    CAN_LOG_DATA[43] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][0]>>8);    
    CAN_LOG_DATA[44] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]);   
    CAN_LOG_DATA[45] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][1]>>8);  
    CAN_LOG_DATA[46] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]);   
    CAN_LOG_DATA[47] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][2]>>8);   
    CAN_LOG_DATA[48] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]);   
    CAN_LOG_DATA[49] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][3]>>8);    
    CAN_LOG_DATA[50] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][4]);   
    CAN_LOG_DATA[51] = (UCHAR)(ldddsps16StoredPeriodTime[RR_WL][4]>>8);
    CAN_LOG_DATA[52] = (UCHAR)(DDSp_SP_FL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[53] = (UCHAR)(DDSp_SP_FR.ldddspu8StoredPeriodCnt);  
    CAN_LOG_DATA[54] = (UCHAR)(DDSp_SP_RL.ldddspu8StoredPeriodCnt);
    CAN_LOG_DATA[55] = (UCHAR)(DDSp_SP_RR.ldddspu8StoredPeriodCnt);    

    
    CAN_LOG_DATA[56] = (UCHAR)(ldddspu8DdsPlusMode);
    CAN_LOG_DATA[57] = (UCHAR)(ldddspu8DdsPlusSubMode);  
    CAN_LOG_DATA[58] = (UCHAR)(eng_torq_abs);   
    CAN_LOG_DATA[59] = (UCHAR)(eng_torq_abs>>8);            
	CAN_LOG_DATA[60] = (UCHAR)((yaw_out/100)>127)?127:(((yaw_out/100)<-127)?-127:(CHAR)(yaw_out/100)); 
	CAN_LOG_DATA[61] = (UCHAR)((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(CHAR)(wstr/10));
	CAN_LOG_DATA[62] = (UCHAR)((alat/10)>127)?127:(((alat/10)<-127)?-127:(CHAR)(alat/10));
	CAN_LOG_DATA[63] = (UCHAR)(ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(CHAR)ebd_refilt_grv);                                                        
	CAN_LOG_DATA[64] = (UCHAR)((CalibrationValueFL>127)?127:((CalibrationValueFL<-127)?-127:(CHAR)CalibrationValueFL));                                          
    CAN_LOG_DATA[65] = (UCHAR)((CalibrationValueFR>127)?127:((CalibrationValueFR<-127)?-127:(CHAR)CalibrationValueFR));                                          
    CAN_LOG_DATA[66] = (UCHAR)((CalibrationValueRL>127)?127:((CalibrationValueRL<-127)?-127:(CHAR)CalibrationValueRL));                                          
    CAN_LOG_DATA[67] = (UCHAR)((CalibrationValueRR>127)?127:((CalibrationValueRR<-127)?-127:(CHAR)CalibrationValueRR));                                          
    CAN_LOG_DATA[68] = (UCHAR)((ldddss8PressEstByAxleDiff[FL_WL]>127)?127:((ldddss8PressEstByAxleDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[FL_WL]));
	CAN_LOG_DATA[69] = (UCHAR)((ldddss8PressEstBySideDiff[FL_WL]>127)?127:((ldddss8PressEstBySideDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[FL_WL])); 
	CAN_LOG_DATA[70] = (UCHAR)((ldddss8PressEstByDiagDiff[FL_WL]>127)?127:((ldddss8PressEstByDiagDiff[FL_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[FL_WL])); 
	CAN_LOG_DATA[71] = (UCHAR)((ldddss8PressEstByAxleDiff[RL_WL]>127)?127:((ldddss8PressEstByAxleDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[FR_WL])); 
	CAN_LOG_DATA[72] = (UCHAR)((ldddss8PressEstBySideDiff[FR_WL]>127)?127:((ldddss8PressEstBySideDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstBySideDiff[FR_WL])); 
	CAN_LOG_DATA[73] = (UCHAR)((ldddss8PressEstByDiagDiff[FR_WL]>127)?127:((ldddss8PressEstByDiagDiff[FR_WL]<-127)?-127:(CHAR)ldddss8PressEstByDiagDiff[FR_WL])); 
	CAN_LOG_DATA[74] = (UCHAR)(mtp);//(UCHAR)((ldddss8PressEstByAxleDiff[RL_WL]>127)?127:((ldddss8PressEstByAxleDiff[RL_WL]<-127)?-127:(CHAR)ldddss8PressEstByAxleDiff[RL_WL])); 





        i=0;
        if (ABS_fz                          ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (YAW_CDC_WORK                          ==1) i|=0x04;
        if (ESP_TCS_ON                    ==1) i|=0x08;
        if (ldddspu1StdStrDrvStateForDDS                     ==1) i|=0x10;
        if (ldddsu1DeflationState                 ==1) i|=0x20;
        if (ldddspu1AllWhlCalibCompleteFlg                 ==1) i|=0x40;
        if (ldddsu1RequestDDSReset                 ==1) i|=0x80;
        CAN_LOG_DATA[75] =(UCHAR) (i);      
        
        i=0;
        if (ldddsu8CompleteCalib[0]                          ==1) i|=0x01;
        if (ldddsu8CompleteCalib[1]                             ==1) i|=0x02;
        if (ldddsu8CompleteCalib[2]                          ==1) i|=0x04;
        if (ldddsu1CompleteCalib                    ==1) i|=0x08;
        if (ldddsu1StdStrDrvStateForDDS                     ==1) i|=0x10;
        if (ldddsu1RunningAvgUpdateFlag                   ==1) i|=0x20;
        if (ldddsu1Diag2whlDeflationFLRR                   ==1) i|=0x40;
        if (ldddsu1Diag2whlDeflationFRRL                    ==1) i|=0x80;
        CAN_LOG_DATA[76] =(UCHAR) (i);      

        i=0;
        
        if (ldddsu8Other3whlDeflation[FL_WL]                 ==1) i|=0x01;
        if (ldddsu8Other3whlDeflation[FR_WL]                 ==1) i|=0x02;
        if (ldddsu8Other3whlDeflation[RL_WL]                 ==1) i|=0x04;
        if (ldddsu8Other3whlDeflation[RR_WL]                 ==1) i|=0x08;
        if (ldddsu8SingleWhlDeflation[FL_WL]                   ==1) i|=0x10;
        if (ldddsu8SingleWhlDeflation[FR_WL]                   ==1) i|=0x20;
        if (ldddsu8SingleWhlDeflation[RL_WL]                   ==1) i|=0x40;
        if (ldddsu8SingleWhlDeflation[RR_WL]                   ==1) i|=0x80;
                                               
        CAN_LOG_DATA[77] =(UCHAR) (i);    
        
        i=0;
 if(ldddspu1AllWhlCalibCompleteFlg==0){         
        if (ldddspu1CalibCompleteFlgFL                   ==1) i|=0x01;
        if (ldddspu1CalibCompleteFlgFR                   ==1) i|=0x02;
        if (ldddspu1CalibCompleteFlgRL                   ==1) i|=0x04;
        if (ldddspu1CalibCompleteFlgRR                   ==1) i|=0x08;
        if (DDSp_SP_FL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalCalibUpdateFlg                    ==1) i|=0x80;            
}
else{
        if (DDSp_SP_FL.ldddspu1LowTirePressFlg                   ==1) i|=0x01;
        if (DDSp_SP_FR.ldddspu1LowTirePressFlg                   ==1) i|=0x02;
        if (DDSp_SP_RL.ldddspu1LowTirePressFlg                   ==1) i|=0x04;
        if (DDSp_SP_RR.ldddspu1LowTirePressFlg                   ==1) i|=0x08;    
        if (DDSp_SP_FL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x10;
        if (DDSp_SP_FR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x20;
        if (DDSp_SP_RL.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x40;
        if (DDSp_SP_RR.ldddspu1CalDefDctUpdateFlg                    ==1) i|=0x80;               
}            

        CAN_LOG_DATA[78] =(UCHAR) (i);    
        
        i=0;

        if (ldddsu1LowTirePressStateFlgFL                   ==1) i|=0x01;
        if (ldddsu1LowTirePressStateFlgFR                   ==1) i|=0x02;
        if (ldddsu1LowTirePressStateFlgRL                   ==1) i|=0x04;
        if (ldddsu1LowTirePressStateFlgRR                   ==1) i|=0x08;

        if (ldddsu8ElapsedTimeUpdateFlg[0]                 	==1) i|=0x10;   
        if (ldddsu8ElapsedTimeUpdateFlg[1]    				==1) i|=0x20;   
        if (ldddsu8ElapsedTimeUpdateFlg[2]                  ==1) i|=0x40;   
        if (ldddsu8ElapsedTimeUpdateFlg[3]                  ==1) i|=0x80;   
        CAN_LOG_DATA[79] =(UCHAR) (i);                                   	    
}       
        #endif
    #endif
#endif
/*Check Point
1. Cal file setting-mg/km
2. control box
3. view setting
4. eeprom read/write
*/
#endif



