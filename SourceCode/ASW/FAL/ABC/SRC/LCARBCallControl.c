/******************************************************************************
* Project Name: Anti-Rollback for HEV
* File: LCARBCallControl.C
* Description: Anti-Rollback Controller
* Date: Sep. 20. 2007
******************************************************************************/

/* Includes ******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCARBCallControl 
	#include "Mdyn_autosar.h"
#endif


#include "LCARBCallControl.h"

#include "Hardware_Control.h"
#if __HEV
#include "hm_logic_var.h"

/* Logcal Definiton  *********************************************************/
#define TIME_3S			L_U16_TIME_10MSLOOP_3S

/* Variables Definition*******************************************************/
int16_t lcarbs16ActiveCnt;
ARB_FLAG_t lcarbCtrlFlags01;
	#if ARB_MFC_DUTY_CONTROL
ARB_FLAG_t lcarbCtrlFlags02;
int16_t lcarbu16MFCDutyP;
int16_t lcarbu16MFCDutyS;
	#endif
/* LocalFunction prototype ***************************************************/
static void	LCARB_vLoadArbSignals(void); 
static void	LCARB_vCheckInhibition(void);
static void	LCARB_vDetectActiveCondition(void);
static void	LCARB_vDecidePressureMode(void);
static void	LCARB_vSetArbSignals(void); 

	#if ARB_MFC_DUTY_CONTROL
void	LCARB_vSetDutyPattern(void);
void	LCARB_vAllocValveDutyArray(void);
	#endif
/* Implementation*************************************************************/

/******************************************************************************
* FUNCTION NAME:      LCARB_vCallControl
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        ARB controller main function
******************************************************************************/
void 	LCARB_vCallControl(void)
{
	LCARB_vLoadArbSignals();
	
	LCARB_vCheckInhibition();
	
	LCARB_vDetectActiveCondition();
	
	LCARB_vDecidePressureMode();
	
	LCARB_vSetArbSignals();

	#if ARB_MFC_DUTY_CONTROL
	LCARB_vSetDutyPattern();
	#endif
}

static void	LCARB_vLoadArbSignals(void) 
{
	lcarbu1HevErrorFlag = lespu1ARB_HevErr;
	lcarbu1ActVlvACmd = lespu1ARB_CasVlvACmdC;
	lcarbu1ActVlvBCmd = lespu1ARB_CasVlvBCmdC;
}

static void	LCARB_vCheckInhibition(void) 
{
	if((fu1OnDiag==1)||(init_end_flg==0))
	{
		lcarbu1InhibitionFlag=1;
	}
	else if(lcarbu1HevErrorFlag==1) 
	{
		lcarbu1InhibitionFlag=1;
	}
	else 
	{
		lcarbu1InhibitionFlag = 0;
	}
}

static void	LCARB_vDetectActiveCondition(void) 
{
    if((ABS_fz==0) && (lcarbu1InhibitionFlag==0)) 
    {
        if((lcarbu1ActVlvACmd==1) || (lcarbu1ActVlvBCmd==1)) 
        {
            lcarbu1ActiveFlag = 1;
        }
        else 
        {
            lcarbu1ActiveFlag = 0;
        }
    }
    else 
    {
        lcarbu1ActiveFlag = 0;
    }
    
}

static void	LCARB_vDecidePressureMode(void)
{
	if(lcarbu1ActiveFlag==1) 
	{
		if(lcarbs16ActiveCnt < TIME_3S)
		{
			lcarbs16ActiveCnt = lcarbs16ActiveCnt + 1;
		}
		
	    if(lcarbu1ActVlvACmd==1) 
	    {
	    	lcarbu1ActSecondaryTc = 1;
	    }
	    else 
	    {
	    	lcarbu1ActSecondaryTc = 0;
	    }
	    
	    if(lcarbu1ActVlvBCmd==1) 
	    {
	    	lcarbu1ActPrimaryTc = 1;
	    }
	    else 
	    {
	    	lcarbu1ActPrimaryTc = 0;
	    }
	}
	else
	{
		lcarbs16ActiveCnt = 0;
		lcarbu1ActSecondaryTc = 0;
		lcarbu1ActPrimaryTc = 0;
	}

}

static void	LCARB_vSetArbSignals(void) 
{
    lespu1ARB_Act = lcarbu1ActiveFlag;
}

	#if ARB_MFC_DUTY_CONTROL
void	LCARB_vSetDutyPattern(void)
{ 
	if ( (lespu1ARB_CFHcuBrkOn==1) && (lespu1ARB_CFHcuBrkFlt==0) )
	{
		lcarbu1BrkOnFlag = 1;
	}
	else
	{
		lcarbu1BrkOnFlag = 0;
	}
	
	if(lespu1ARB_CFHcuBrkFlt==1)	/* lespu1ARB_CFHcuBrkFlt = 1, when brake fault */
	{
		if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvACmd==1))
		{
			lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_MAX;
			
			lcarbu1ControlPFlag = 1;
		}
		else
		{
			if(lcarbu1ControlPFlag==1)
			{				
				if((lcarbu16MFCDutyP)>(U16_ARB_MFC_DUTY_OFF+U8_ARB_MFC_DUTY_GRAD2))				
				{					
					lcarbu16MFCDutyP = lcarbu16MFCDutyP-U8_ARB_MFC_DUTY_GRAD2;
				}
				else
				{
					lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_OFF;
					lcarbu1ControlPFlag = 0;
				}
			}
			else
			{
				lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_OFF;							
			}
		}
		
		if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvBCmd==1))
		{
			lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_MAX;
			
			lcarbu1ControlSFlag = 1;
		}
		else
		{
			if(lcarbu1ControlSFlag==1)
			{
				if((lcarbu16MFCDutyS)>(U16_ARB_MFC_DUTY_OFF+U8_ARB_MFC_DUTY_GRAD2))				
				{					
					lcarbu16MFCDutyS = lcarbu16MFCDutyS-U8_ARB_MFC_DUTY_GRAD2;
				}
				else
				{
					lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_OFF;
					lcarbu1ControlSFlag = 0;
				}				
			}
			else
			{
				lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_OFF;
			}
		}			
	}
	else	/* lespu1ARB_CFHcuBrkFlt = 0, when brake system has no fault */
	{
		if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvACmd==1)&&(lcarbu1BrkOnFlag==1))
		{
			lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_MAX;
			
			lcarbu1ControlPFlag = 1;
			lcarbu1BrakeOffPFlag = 1;
		}
		else if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvACmd==1))
		{
			if(lcarbu1BrakeOffPFlag==1)
			{
				if((lcarbu16MFCDutyP)>(U16_ARB_MFC_DUTY_SETTING+U8_ARB_MFC_DUTY_GRAD1))				
				{					
					lcarbu16MFCDutyP = lcarbu16MFCDutyP-U8_ARB_MFC_DUTY_GRAD1;
				}
				else
				{
					lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_SETTING;
					lcarbu1BrakeOffPFlag = 0;
				}	
			}
			else
			{
				lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_SETTING;
			}			
		}
		else
		{			
			if(lcarbu1ControlPFlag==1)
			{				
				if((lcarbu16MFCDutyP)>(U16_ARB_MFC_DUTY_OFF+U8_ARB_MFC_DUTY_GRAD2))				
				{					
					lcarbu16MFCDutyP = lcarbu16MFCDutyP-U8_ARB_MFC_DUTY_GRAD2;
				}
				else
				{
					lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_OFF;
					lcarbu1ControlPFlag = 0;
				}
			}
			else
			{
				lcarbu16MFCDutyP = U16_ARB_MFC_DUTY_OFF;							
				lcarbu1ControlPFlag = 0;
				lcarbu1BrakeOffPFlag = 0;
			}
		}
		
		if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvBCmd==1)&&(lcarbu1BrkOnFlag==1))
		{
			lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_MAX;
			
			lcarbu1ControlSFlag = 1;
			lcarbu1BrakeOffSFlag = 1;
		}
		else if((lcarbu1ActiveFlag==1)&&(lcarbu1ActVlvBCmd==1))
		{
			if(lcarbu1BrakeOffSFlag==1)
			{
				if((lcarbu16MFCDutyS)>(U16_ARB_MFC_DUTY_SETTING+U8_ARB_MFC_DUTY_GRAD1))				
				{					
					lcarbu16MFCDutyS = lcarbu16MFCDutyS-U8_ARB_MFC_DUTY_GRAD1;
				}
				else
				{
					lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_SETTING;
					lcarbu1BrakeOffSFlag = 0;
				}				
			}
			else
			{
				lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_SETTING;
			}
			
		}		
		else
		{
			if(lcarbu1ControlSFlag==1)
			{				
				if((lcarbu16MFCDutyS)>(U16_ARB_MFC_DUTY_OFF+U8_ARB_MFC_DUTY_GRAD2))				
				{					
					lcarbu16MFCDutyS = lcarbu16MFCDutyS-U8_ARB_MFC_DUTY_GRAD2;
				}
				else
				{
					lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_OFF;
					lcarbu1ControlSFlag = 0;
				}
			}
			else
			{
				lcarbu16MFCDutyS = U16_ARB_MFC_DUTY_OFF;	
				lcarbu1ControlSFlag = 0;
				lcarbu1BrakeOffSFlag = 0;
			}
		}
	}
}

void	LCARB_vAllocValveDutyArray(void)
{ 
	if(lcarbu1ControlPFlag==1)
	{
		MFC_PWM_DUTY_P = lcarbu16MFCDutyP/100;
	}
	else
	{
		MFC_PWM_DUTY_P = 0;
	}
	if(lcarbu1ControlSFlag==1)
	{
		MFC_PWM_DUTY_S = lcarbu16MFCDutyS/100;
	}
	else
	{
		MFC_PWM_DUTY_S = 0;
	}	
}
	#endif
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCARBCallControl
	#include "Mdyn_autosar.h"
#endif    

