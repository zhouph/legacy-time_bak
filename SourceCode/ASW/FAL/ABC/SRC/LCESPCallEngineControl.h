#ifndef __LCESPCALLENGINECONTROL_H__
#define __LCESPCALLENGINECONTROL_H__
#include "LVarHead.h"
extern int16_t  delta_yaw_third_ems_old;
extern int16_t 	yaw_err;
extern int16_t 	diff_yaw_err;
extern int16_t 	TorqLimit, TorqLimit_alt;
extern uint16_t 	ETO_exit_time;
extern uint8_t 	quickout_counter;
extern int16_t 	wstr_alt3;
extern int16_t 	diff_yaw_err3;
extern int16_t 	diff_yaw_err_old;
extern int16_t 	diff_yaw_err_old2;

extern int16_t 	delta_yaw_third_ems1;
extern int16_t 	P_gain_effect;
extern int16_t 	D_gain_effect;    
extern int16_t 	wslip_max;
extern int16_t 	steering_effect;
extern int16_t 	drive_torq_div_20;
extern int16_t 	esp_cmd_alt;      
extern int16_t 	diff_delta_yaw;
extern int16_t 	esp_cmd_yaw, esp_cmd_initial;       
/*=============================================================================*/
/*	EEC flags by Kim Jeonghun in 2005.1.15									   */
/*-----------------------------------------------------------------------------*/
extern uint8_t 	EEC_flags;
extern uint8_t 	EEC_YAW_flags;
extern uint8_t	EEC_STRAIGHT;
extern uint16_t	eec_straight_cnt;
extern uint8_t	d_yaw_div_delay;
extern int16_t	o_delta_yaw_thres_eec;
extern int16_t	u_delta_yaw_thres_eec;
extern int16_t	delta_yaw_eec;
extern int16_t	delta_yaw_eec_alt;

/* EEC PD 제어 구조 추가 */ 
extern int16_t 	s16YawErr;
extern int16_t 	s16DiffYawErr;
extern int16_t 	s16YawErrAlt;
extern int16_t 	s16DiffYawErrAlt;

extern int16_t		s16YawPDEffect;
extern int16_t		s16YawPGainEffect;
extern int16_t		s16YawDGainEffect;

/* Yaw PD Gain Controller */ 
extern int8_t	s8SetYawPDGainSelecter;

extern int16_t		s16SetYawPGainTemp[4]; 		
extern int16_t		s16SetYawDGainTemp[4]; 		
extern int16_t		s16SetYawPDGainSpeed[4];		
		  								
extern int16_t		s16SetYawPGain;
extern int16_t		s16SetYawDGain;					

extern int16_t		s16SetYawPGainUnderState[3][4];   
				  								
				  								
		
extern int16_t		s16SetYawDGainUnderState[3][4];  
				  							
				  							
		
extern int16_t		s16SetYawPGainUnstableState[3][4];
				  								
				  								
		
extern int16_t		s16SetYawDGainUnstableState[3][4];
				  								
				  								
		
extern int16_t		s16SetYawPGainStableState[3][4]; 
				  								
				  								
		
extern int16_t		s16SetYawDGainStableState[3][4]; 
				  								
				  								
		
extern int16_t		s16SetYawPGainOverState[3][4]; 	
				  							
				  							
				  								
extern int16_t		s16SetYawDGainOverState[3][4]; 				  											  										

		
extern int16_t 	wslip_max_alt;

extern int8_t 	s8LoopCounter;

extern int8_t   us_eng_con_cnt;

extern int16_t		MED_MU_EMS;
extern int16_t		LOW_MU_ESP_TORQ_CON_U_SPEED;
extern int16_t		MED_MU_ESP_TORQ_CON_U_SPEED;
extern int16_t		HIGH_MU_ESP_TORQ_CON_U_SPEED;

extern uint16_t	eec_stable_exit_count;

extern int16_t		s16SetYawPGainStablCurPer[3][4];

extern uint16_t		lcespu16EECYawFadeTime;

#define __MGH80_EEC_IMPROVE_CONCEPT     0

#if (__MGH80_EEC_IMPROVE_CONCEPT == 1)
extern int16_t  ldesps16EECTargetVx, lcesps16EECDropRate, ldesps16EECTargetVxLow;
extern int16_t  cal_torq_div_20, eng_torq_div_20;
extern uint8_t  lcespu8EECStartCnt;
extern uint8_t  ldespu8EECOverSpeed;        



#define S16_EEC_MAX_TAR_VX_GAIN             169
  #if (__CAR == HMC_BH)
#define U8_STATE_YAW_STABLE_SPD_RISE_LM     1
#define U8_STATE_YAW_STABLE_SPD_RISE_MM     4
#define U8_STATE_YAW_STABLE_SPD_RISE_HM     10
#define U8_STATE_YAW_STABLE_OSPD_RISE_LM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_MM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_HM    0
#define U8_EEC_START_CYCLE_LM               3
#define U8_EEC_START_CYCLE_MM               3
#define U8_EEC_START_CYCLE_HM               3
#define U8_EEC_START_CYCLE_IN_FO_LM         1         
#define U8_EEC_START_CYCLE_IN_FO_MM         1
#define U8_EEC_START_CYCLE_IN_FO_HM         1
#define S16_EEC_LOW_TAR_VX_GAIN_LM          85
#define S16_EEC_LOW_TAR_VX_GAIN_MM          85
#define S16_EEC_LOW_TAR_VX_GAIN_HM          100

  #elif (__CAR == HMC_VI)
    #if (__4WD)
#define U8_STATE_YAW_STABLE_SPD_RISE_LM     10
#define U8_STATE_YAW_STABLE_SPD_RISE_MM     20
#define U8_STATE_YAW_STABLE_SPD_RISE_HM     50
#define U8_STATE_YAW_STABLE_OSPD_RISE_LM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_MM    10
#define U8_STATE_YAW_STABLE_OSPD_RISE_HM    20
#define U8_EEC_START_CYCLE_LM               10
#define U8_EEC_START_CYCLE_MM               10
#define U8_EEC_START_CYCLE_HM               3
#define U8_EEC_START_CYCLE_IN_FO_LM         5
#define U8_EEC_START_CYCLE_IN_FO_MM         5
#define U8_EEC_START_CYCLE_IN_FO_HM         3  
#define S16_EEC_LOW_TAR_VX_GAIN_LM          85 
#define S16_EEC_LOW_TAR_VX_GAIN_MM          85 
#define S16_EEC_LOW_TAR_VX_GAIN_HM          100

    #else
#define U8_STATE_YAW_STABLE_SPD_RISE_LM     10
#define U8_STATE_YAW_STABLE_SPD_RISE_MM     10
#define U8_STATE_YAW_STABLE_SPD_RISE_HM     50
#define U8_STATE_YAW_STABLE_OSPD_RISE_LM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_MM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_HM    0
#define U8_EEC_START_CYCLE_LM               10
#define U8_EEC_START_CYCLE_MM               10
#define U8_EEC_START_CYCLE_HM               3
#define U8_EEC_START_CYCLE_IN_FO_LM         5
#define U8_EEC_START_CYCLE_IN_FO_MM         5
#define U8_EEC_START_CYCLE_IN_FO_HM         3
#define S16_EEC_LOW_TAR_VX_GAIN_LM          85 
#define S16_EEC_LOW_TAR_VX_GAIN_MM          85 
#define S16_EEC_LOW_TAR_VX_GAIN_HM          100  
    #endif    
  #elif  (__CAR == DCX_COMPASS)
#define U8_STATE_YAW_STABLE_SPD_RISE_LM     0
#define U8_STATE_YAW_STABLE_SPD_RISE_MM     0
#define U8_STATE_YAW_STABLE_SPD_RISE_HM     0
#define U8_STATE_YAW_STABLE_OSPD_RISE_LM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_MM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_HM    0
#define U8_EEC_START_CYCLE_LM               3
#define U8_EEC_START_CYCLE_MM               3
#define U8_EEC_START_CYCLE_HM               3
#define U8_EEC_START_CYCLE_IN_FO_LM         1         
#define U8_EEC_START_CYCLE_IN_FO_MM         1
#define U8_EEC_START_CYCLE_IN_FO_HM         1
#define S16_EEC_LOW_TAR_VX_GAIN_LM          82
#define S16_EEC_LOW_TAR_VX_GAIN_MM          82
#define S16_EEC_LOW_TAR_VX_GAIN_HM          100
  #else
#define U8_STATE_YAW_STABLE_SPD_RISE_LM     1
#define U8_STATE_YAW_STABLE_SPD_RISE_MM     4
#define U8_STATE_YAW_STABLE_SPD_RISE_HM     10
#define U8_STATE_YAW_STABLE_OSPD_RISE_LM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_MM    0
#define U8_STATE_YAW_STABLE_OSPD_RISE_HM    0
#define U8_EEC_START_CYCLE_LM               3
#define U8_EEC_START_CYCLE_MM               3
#define U8_EEC_START_CYCLE_HM               3
#define U8_EEC_START_CYCLE_IN_FO_LM         1         
#define U8_EEC_START_CYCLE_IN_FO_MM         1
#define U8_EEC_START_CYCLE_IN_FO_HM         1
#define S16_EEC_LOW_TAR_VX_GAIN_LM          85
#define S16_EEC_LOW_TAR_VX_GAIN_MM          85
#define S16_EEC_LOW_TAR_VX_GAIN_HM          100
  #endif
#endif /*(__MGH80_EEC_IMPROVE_CONCEPT == 1)*/       

/*Refactoring Undefined TP MBD ETCS*/
#define S8_EEC_1ST_CNTR_DROP_RATE_MU_HSP	5
#define S8_EEC_1ST_CNTR_DROP_RATE_MU_LSP	5
#define S8_EEC_1ST_CNTR_DROP_RATE_MU_MSP	5
#define S8_EEC_1ST_CNTR_DROP_RATE_LU_HSP	5
#define S8_EEC_1ST_CNTR_DROP_RATE_LU_LSP	5
#define S8_EEC_1ST_CNTR_DROP_RATE_LU_MSP	5
#define S16_EEC_TORQ_LIMIT_MAX_LMU			50
#define S16_EEC_TORQ_LIMIT_MAX_MMU			50
#define S16_EEC_TORQ_LIMIT_MAX_HMU			50

#if __MY_08
	#define __EEC_GEAR_INHIBIT_MODE	1
#else
	#define __EEC_GEAR_INHIBIT_MODE	0
#endif

	#if __EEC_GEAR_INHIBIT_MODE
extern uint8_t	EEC_ACTIVATED;
extern uint8_t	EEC_ACTIVATE_CHECK;
extern uint8_t	EEC_GEAR_INHIBIT_REQUEST;
extern uint16_t	eec_gear_inhibit_count;

#define EEC_GEAR_INHIBIT_SCAN		142
	#endif

#define STATE_YAW_DEFAULT		0
#define STATE_YAW_STABLE		1
#define STATE_YAW_UNSTABLE		2
#define STATE_YAW_UNDERSTEER	3
#define STATE_YAW_OVERSTEER		4
#define STATE_YAW_OVSTABLE		7

#define STATE_SPIN_DEFAULT		0
#define STATE_SPIN_STABLE		1
#define STATE_SPIN_UNSTABLE		2

#define __EEC_OVERSTEER_THRESHOLD	1
#define __EEC_UNDERSTEER_THRESHOLD	1
#define __EEC_1ST_DROP_RATE         1
	
extern int16_t		eec_target_torq_1st;

extern uint8_t	EEC_FADE_OUT_CTRL_ON;
extern uint8_t	EEC_UTURN_CTRL_ON;

#if __MY_08
#define __EEC_EXIT_CONCEPT_ADDON	1		
	
#endif	/* End of __MY_08 */

#define __EEC_CYCLE_TIME_REDUCTION	1
extern uint8_t lespu8ENG_VAR_OLD;                  
extern uint8_t lespu8ENG_VAR_CHECK;
extern uint8_t lespu8ENG_VAR_COUNT;

						
#endif
