/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LSMSCCallLogData.c
* Description:      Logging Data of MSC
* Logic version:    HV26
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSMSCCallLogData
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/
#include "LSMSCCallLogData.h"
#include "LCCBCCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LDABSDctWheelStatus.h"

/* 2012_SWD_KJS */
#include "LACallMain.h"
#include "Hardware_Control.h"
/* 2012_SWD_KJS */
#include "LSCallLogData.h"

//extern int16_t MonMotorAD_7ms[7];
//
//extern int16_t ign_mon_ad,ref_mon_ad,batt2_mon_ad;


extern uint16_t TC_MFC_Voltage_Primary_dash,TC_MFC_Voltage_Secondary_dash;

/* #if __LOGGER && (LOGGER_DATA_FORMAT==1)  */
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1

#include "LSMSCCallLogData.h"

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/
void LSMSC_vCallMSCLogData(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LSMSC_vCallMSCLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of MSC
********************************************************************************/
#if __VDC

void LSMSC_vCallMSCLogData(void)
{

    CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);                      /* ch1  */
    CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (uint8_t)(FL.ldabss16EstWheelPress_VM);              /* ch2  */
    CAN_LOG_DATA[3]  = (uint8_t)(FL.ldabss16EstWheelPress_VM>>8);
    CAN_LOG_DATA[4]  = (uint8_t)(FR.ldabss16EstWheelPress_VM);              /* ch3  */
    CAN_LOG_DATA[5]  = (uint8_t)(FR.ldabss16EstWheelPress_VM>>8);
    CAN_LOG_DATA[6]  = (uint8_t)(RL.ldabss16EstWheelPress_VM);              /* ch4  */
    CAN_LOG_DATA[7]  = (uint8_t)(RL.ldabss16EstWheelPress_VM>>8);
    
    CAN_LOG_DATA[8]  = (uint8_t)(RR.ldabss16EstWheelPress_VM);              /* ch5  */
    CAN_LOG_DATA[9]  = (uint8_t)(RR.ldabss16EstWheelPress_VM>>8);

    CAN_LOG_DATA[10] = (uint8_t)(ldabss16WPECircuitPress_Pri);                                     /* ch6  */
    CAN_LOG_DATA[11] = (uint8_t)(ldabss16WPECircuitPress_Pri>>8);
    CAN_LOG_DATA[12] = (uint8_t)(ldabss16WPECircuitPress_Scn);                         /* ch7  */
    CAN_LOG_DATA[13] = (uint8_t)(ldabss16WPECircuitPress_Scn>>8);   
    CAN_LOG_DATA[14] = (uint8_t)(ldabss16MotorRPM_NEW);                                /* ch8  */
    CAN_LOG_DATA[15] = (uint8_t)(ldabss16MotorRPM_NEW>>8);

//    CAN_LOG_DATA[10] = (uint8_t)(ldabss16MotorRPM_NEW);                                     /* ch6  */
//    CAN_LOG_DATA[11] = (uint8_t)(ldabss16MotorRPM_NEW>>8);
//    CAN_LOG_DATA[12] = (uint8_t)(ldabsu16WPEMotorVolt);                         /* ch7  */
//    CAN_LOG_DATA[13] = (uint8_t)(ldabsu16WPEMotorVolt>>8);   
//    CAN_LOG_DATA[14] = (uint8_t)(motor_rpm);                                /* ch8  */
//    CAN_LOG_DATA[15] = (uint8_t)(motor_rpm>>8);

    CAN_LOG_DATA[16] = (uint8_t)FL.pwm_full_rise_cnt;                       /* ch9  */
    CAN_LOG_DATA[17] = (uint8_t)FL.pwm_lfc_rise_cnt;                        /* ch10 */
    CAN_LOG_DATA[18] = (uint8_t)FL.pwm_hold_cnt;                            /* ch11 */
    CAN_LOG_DATA[19] = (uint8_t)FL.pwm_dump_cnt;                            /* ch12 */
    CAN_LOG_DATA[20] = (uint8_t)FL.pwm_circulation_cnt;                     /* ch13 */
    CAN_LOG_DATA[21] = (uint8_t)FL.Wheel_Valve_Mode;                        /* ch14 */
    CAN_LOG_DATA[22] = (uint8_t)(FL.ldabss16EstWheelVolume_VM/100);         /* ch15 */  // 1/10[cc]
    CAN_LOG_DATA[23] = (uint8_t)FL.ldabss16EstWheelPressMON_VM;             /* ch16 */

    CAN_LOG_DATA[24] = (uint8_t)FR.pwm_full_rise_cnt;                       /* ch17 */
    CAN_LOG_DATA[25] = (uint8_t)FR.pwm_lfc_rise_cnt;                        /* ch18 */
    CAN_LOG_DATA[26] = (uint8_t)FR.pwm_hold_cnt;                            /* ch19 */
    CAN_LOG_DATA[27] = (uint8_t)FR.pwm_dump_cnt;                            /* ch20 */
    CAN_LOG_DATA[28] = (uint8_t)FR.pwm_circulation_cnt;                     /* ch21 */
    CAN_LOG_DATA[29] = (uint8_t)FR.Wheel_Valve_Mode;                        /* ch22 */
    CAN_LOG_DATA[30] = (uint8_t)(FR.ldabss16EstWheelVolume_VM/100);         /* ch23 */
    CAN_LOG_DATA[31] = (uint8_t)FR.ldabss16EstWheelPressMON_VM;             /* ch24 */

    CAN_LOG_DATA[32] = (uint8_t)RL.pwm_full_rise_cnt;                       /* ch25 */
    CAN_LOG_DATA[33] = (uint8_t)RL.pwm_lfc_rise_cnt;                        /* ch26 */
    CAN_LOG_DATA[34] = (uint8_t)RL.pwm_hold_cnt;                            /* ch27 */
    CAN_LOG_DATA[35] = (uint8_t)RL.pwm_dump_cnt;                            /* ch28 */
    CAN_LOG_DATA[36] = (uint8_t)RL.pwm_circulation_cnt;                     /* ch29 */
    CAN_LOG_DATA[37] = (uint8_t)RL.Wheel_Valve_Mode;                        /* ch30 */
    CAN_LOG_DATA[38] = (uint8_t)(RL.ldabss16EstWheelVolume_VM/100);         /* ch31 */
    CAN_LOG_DATA[39] = (uint8_t)RL.ldabss16EstWheelPressMON_VM;             /* ch32 */

    CAN_LOG_DATA[40] = (uint8_t)(wstr);                                     /* ch33 */
    CAN_LOG_DATA[41] = (uint8_t)(wstr>>8);
    CAN_LOG_DATA[42] = (uint8_t)(yaw_out);                                  /* ch34 */
    CAN_LOG_DATA[43] = (uint8_t)(yaw_out>>8);
    CAN_LOG_DATA[44] = (uint8_t)(rf2);                                      /* ch35 */
    CAN_LOG_DATA[45] = (uint8_t)(rf2>>8);
    CAN_LOG_DATA[46] = (uint8_t)(alat);                                     /* ch36 */
    CAN_LOG_DATA[47] = (uint8_t)(alat>>8);

    CAN_LOG_DATA[48] = (uint8_t)(FL.Estimated_Active_Press/100);            /* ch37 */
    CAN_LOG_DATA[49] = (uint8_t)(FR.Estimated_Active_Press/100);            /* ch38 */
    CAN_LOG_DATA[50] = (uint8_t)(RL.Estimated_Active_Press/100);            /* ch39 */
    CAN_LOG_DATA[51] = (uint8_t)(RR.Estimated_Active_Press/100);            /* ch40 */    
    CAN_LOG_DATA[52] = (uint8_t)(ldabss16WPEMpress_1_100bar);               /* ch41 */
    CAN_LOG_DATA[53] = (uint8_t)(ldabss16WPEMpress_1_100bar>>8);                     
    CAN_LOG_DATA[54] = (uint8_t)(la_PtcMfc.MFC_PWM_DUTY_temp);              /* ch42 */
    CAN_LOG_DATA[55] = (uint8_t)(la_StcMfc.MFC_PWM_DUTY_temp);              /* ch43 */

    CAN_LOG_DATA[56] = (uint8_t)(vref/8);                      /* ch44 */
    CAN_LOG_DATA[57] = (uint8_t)(mtp);                                      /* ch45 */ 

//    CAN_LOG_DATA[56] = (uint8_t)(MFC_PWM_DUTY_P);                         /* Temp */
//    CAN_LOG_DATA[57] = (uint8_t)(MFC_PWM_DUTY_S);                         /* Temp */ 
    
    CAN_LOG_DATA[58] = (uint8_t)(ldabss16WPETCvvHoldPress_Pri/100);          /* ch46 */
    CAN_LOG_DATA[59] = (uint8_t)(ldabss16WPETCvvHoldPress_Scn/100);          /* ch47 */
   
//    CAN_LOG_DATA[60] = (uint8_t)FL.pwm_duty_temp;                            /* ch48 */
//    CAN_LOG_DATA[61] = (uint8_t)FR.pwm_duty_temp;                            /* ch49 */
//    CAN_LOG_DATA[62] = (uint8_t)RL.pwm_duty_temp;                            /* ch50 */
//    CAN_LOG_DATA[63] = (uint8_t)RR.pwm_duty_temp;                            /* ch51 */

    CAN_LOG_DATA[60] = (uint8_t)la_FL1HP.u8PwmDuty;                            /* ch48 */
    CAN_LOG_DATA[61] = (uint8_t)la_FR1HP.u8PwmDuty;                            /* ch49 */
    CAN_LOG_DATA[62] = (uint8_t)la_RL1HP.u8PwmDuty;                            /* ch50 */
    CAN_LOG_DATA[63] = (uint8_t)la_RR1HP.u8PwmDuty;                            /* ch51 */

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)
    CAN_LOG_DATA[64] = (uint8_t)(mpress);                                    /* ch52 */
    CAN_LOG_DATA[65] = (uint8_t)(mpress>>8);
#else
    CAN_LOG_DATA[64] = (uint8_t)(lsesps16MpressByAHBgen3);                                    /* ch52 */
    CAN_LOG_DATA[65] = (uint8_t)(lsesps16MpressByAHBgen3>>8);
#endif    

    tempB0=tempB1=tempB2=tempB3=tempB4=tempB5=0;

    if(ESP_ERROR_FLG)               tempB5|=0x01;
    if(BLS)                         tempB5|=0x02;
    if(ABS_fz)                      tempB5|=0x04;
    if(EBD_RA)                      tempB5|=0x08;
    if(BTC_fz)                      tempB5|=0x10;
    if(ETCS_ON)                     tempB5|=0x20;
    if(YAW_CDC_WORK)                tempB5|=0x40;
    if(ESP_TCS_ON)                  tempB5|=0x80;
    
    if(AV_VL_fr)                    tempB4|=0x01;
    if(HV_VL_fr)                    tempB4|=0x02;
    if(AV_VL_fl)                    tempB4|=0x04;
    if(HV_VL_fl)                    tempB4|=0x08;
    if(AV_VL_rl)                    tempB4|=0x10;
    if(HV_VL_rl)                    tempB4|=0x20;
    if(AV_VL_rr)                    tempB4|=0x40;
    if(HV_VL_rr)                    tempB4|=0x80;  
    
    if(TCL_DEMAND_fr)               tempB3|=0x01;
    if(TCL_DEMAND_fl)               tempB3|=0x02;
    if(TCL_DEMAND_rl)               tempB3|=0x04;
    if(TCL_DEMAND_rr)               tempB3|=0x08;
    if(S_VALVE_RIGHT)               tempB3|=0x10;
    if(S_VALVE_LEFT)                tempB3|=0x20;
    if(MOT_ON_FLG)                  tempB3|=0x40;
    if(REAR_WHEEL_wl)               tempB3|=0x80;
        
    if(FL.REAR_WHEEL)               tempB2|=0x01;
    if(FR.REAR_WHEEL)               tempB2|=0x02;
    if(RL.REAR_WHEEL)               tempB2|=0x04;
    if(RR.REAR_WHEEL)               tempB2|=0x08;
    if(FL.LEFT_WHEEL)               tempB2|=0x10;
    if(FR.LEFT_WHEEL)               tempB2|=0x20;
    if(RL.LEFT_WHEEL)               tempB2|=0x40;
    if(RR.LEFT_WHEEL)               tempB2|=0x80;
       
    if(FL.WPE_First_Control_VM)                           tempB1|=0x01;
    if(FR.WPE_First_Control_VM)                           tempB1|=0x02;
    if(RL.WPE_First_Control_VM)                           tempB1|=0x04;
    if(RR.WPE_First_Control_VM)                           tempB1|=0x08;
    if(0)                           tempB1|=0x10;
    if(ldabsu1WPEStraightControl)                           tempB1|=0x20;
    if(FLAG_ACTIVE_BRAKING)         tempB1|=0x40;
    if(EST_WHEEL_PRESS_OK_FLG)      tempB1|=0x80;                
       
    if(fu1McpSenPwr1sOk)            tempB0|=0x01;
    if(fu1FrontSolErrDet)           tempB0|=0x02;
    if(fu1RearSolErrDet)            tempB0|=0x04;
    if(fu1ESCSolErrDet)             tempB0|=0x08;
    if(fu1ECUHwErrDet)              tempB0|=0x10;
    if(fu1MotErrFlg)                tempB0|=0x20;
    if(fu1MCPErrorDet)              tempB0|=0x40;
    if(fu1MCPSusDet)                tempB0|=0x80;

    CAN_LOG_DATA[66] = tempB5;                                              /* ch53 */
    CAN_LOG_DATA[67] = tempB4;                                              /* ch54 */
    CAN_LOG_DATA[68] = tempB3;                                              /* ch55 */
    CAN_LOG_DATA[69] = tempB2;                                              /* ch56 */
    CAN_LOG_DATA[70] = tempB1;                                              /* ch57 */
    CAN_LOG_DATA[71] = tempB0;                                              /* ch58 */

    CAN_LOG_DATA[72] = (uint8_t)RR.pwm_full_rise_cnt;                       /* ch59 */
    CAN_LOG_DATA[73] = (uint8_t)RR.pwm_lfc_rise_cnt;                        /* ch60 */
    CAN_LOG_DATA[74] = (uint8_t)RR.pwm_hold_cnt;                            /* ch61 */
    CAN_LOG_DATA[75] = (uint8_t)RR.pwm_dump_cnt;                            /* ch62 */ 
    CAN_LOG_DATA[76] = (uint8_t)RR.pwm_circulation_cnt;                     /* ch63 */
    CAN_LOG_DATA[77] = (uint8_t)RR.Wheel_Valve_Mode;                        /* ch64 */ 
    CAN_LOG_DATA[78] = (uint8_t)(RR.ldabss16EstWheelVolume_VM/100);         /* ch65 */
    CAN_LOG_DATA[79] = (uint8_t)RR.ldabss16EstWheelPressMON_VM;             /* ch66 */   

//    CAN_LOG_DATA[72] = (uint8_t)la_PTC.lau16PwmDuty[0];                   /* Temp */
//    CAN_LOG_DATA[73] = (uint8_t)la_PTC.lau16PwmDuty[6];                   /* Temp */
//    CAN_LOG_DATA[74] = (uint8_t)la_STC.lau16PwmDuty[0];                   /* Temp */
//    CAN_LOG_DATA[75] = (uint8_t)la_STC.lau16PwmDuty[6];                   /* Temp */ 
//    CAN_LOG_DATA[76] = (uint8_t)la_PSV.lau16PwmDuty[0];                   /* Temp */
//    CAN_LOG_DATA[77] = (uint8_t)la_SSV.lau16PwmDuty[0];                   /* Temp */ 

}

#endif /* __VDC */
#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSMSCCallLogData
	#include "Mdyn_autosar.h"
#endif
