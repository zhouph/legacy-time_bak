/******************************************************************************
* Project Name: ESP partial performance
* File: LDESPPartialCallDetection.C
* Date: Dec. 12. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCallPartialDetection
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/

#include "LDESPCallPartialDetection.h"
#include "LCESPCallControl.h"
#include "LCESPCalLpf.h"
#include "LCESPCalInterpolation.h"

#include "LCHDCCallControl.h"
#include "LCEPBCallControl.h"
#include "LCACCCallControl.h"
#if __VDC
extern uint16_t str_hw_err_cnt;
extern uint8_t mp_hw_err_cnt;

 
int16_t circuit_fail_initial_rel_lam_rl;
int16_t circuit_fail_initial_rel_lam_rr;
int16_t PARTIAL_PERFORM_on_cnt;
 

#if __EBP
#include "LCEBPCallControl.h"
extern struct   U8_BIT_STRUCT_t EBPC1;
#include "LDEBPCallDetection.h"
extern struct   U8_BIT_STRUCT_t EBPD0;
#endif

/*void ESP_ON_OFF_TIME_CAL(void);
void ESP_ON_OFF_TIME_CAL_wl(struct W_STRUCT *WL);*/
void LDESPPartial_vDetectCircuitFail(void);

#if __PARTIAL_PERFORMANCE
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/



/* LocalFunction prototype ***************************************************/
void LDESPPartial_vCallDetection(void);
void LDESPPartial_vVariableSet(void);
void LDESPPartial_vDetectFault(void);



/* Implementation*************************************************************/



void LDESPPartial_vCallDetection(void)
{

}

void LDESPPartial_vVariableSet(void)
{
    if (BTCS_ON==1)
    {
        BTCS_ON_on_cnt++;
        BTCS_ON_off_cnt=LOOPTIME_SEC(5);//T_5S;
    }
    else
    {
        BTCS_ON_off_cnt--;
        BTCS_ON_on_cnt=0;
    }
    BTCS_ON_on_cnt=LCESP_s16LimitInt(BTCS_ON_on_cnt,0,L_U16_TIME_10MSLOOP_5S);
    BTCS_ON_off_cnt=LCESP_s16LimitInt(BTCS_ON_off_cnt,0,L_U16_TIME_10MSLOOP_5S);

    if (ESP_TCS_ON==1)
    {
        ESP_TCS_ON_on_cnt++;
        ESP_TCS_ON_off_cnt=L_U16_TIME_10MSLOOP_5S;
    }
    else
    {
        ESP_TCS_ON_off_cnt--;
        ESP_TCS_ON_on_cnt=0;
    }
    ESP_TCS_ON_on_cnt=LCESP_s16LimitInt(ESP_TCS_ON_on_cnt,0,L_U16_TIME_10MSLOOP_5S);
    ESP_TCS_ON_off_cnt=LCESP_s16LimitInt(ESP_TCS_ON_off_cnt,0,L_U16_TIME_10MSLOOP_5S);

    YCW_K=0;
    YCW_A=0;

    if(ESP_end_timer !=0)
    {
        ESP_end_timer--;
    }
    else
    {
        ;
    }

    if(YAW_CDC_WORK_OLD==0)
    {
        if(YAW_CDC_WORK==1)
        {             /* start of ESP SLIP CONTROL */
            YCW_K=1;
            ESP_NOT_WORKING=0;
        }
        else
        {
            ;
        }
    }
    else
    {                             /* end of ESP SLIP CONTROL   */
        if(YAW_CDC_WORK==0)
        {
            YCW_A=1;
            ESP_NOT_WORKING=1;
            ESP_end_timer = L_U16_TIME_10MSLOOP_5S;
        }
        else
        {
            ;
        }
    }

    if(YAW_CDC_WORK==1)
    {
        if( ((uint16_t)ESP_working_timer) < 0xffff)
        {
            ESP_working_timer++;
            ESP_end_timer=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        ESP_working_timer=0;
    }

    if((ESP_NOT_WORKING==1)&&(standstill_flg==1))
    {
        ESP_NOT_WORKING=0;
    }
    else
    {
        ;
    }
    if((YAW_CDC_WORK_OLD)||(YAW_CDC_WORK)||(ESP_end_timer >L_U8_TIME_10MSLOOP_2S))
    {
        ESP_fz=1;
    }
    else
    {
        ESP_fz=0;
    }

}

void LDESPPartial_vDetectFault(void)
{

    if( ((PARTIAL_PERFORM==0)&&(YAW_CDC_WORK_OLD) && (ESP_ERROR_FLG==0))
        || ((PARTIAL_PERFORM==1) && (ESP_ERROR_FLG==0)) )
    {

      #if __STEER_SENSOR_TYPE == CAN_TYPE   /* Absolute */
        if(fu1StrSusDet==1)
        {
            VDC_UNDER_STR_ERR=1;
        }
        else
        {
            VDC_UNDER_STR_ERR=0;
        }
      #elif __STEER_SENSOR_TYPE==ANALOG_TYPE
        if (fu1StrSusDet==1)
        {
            VDC_UNDER_STR_ERR=1;
        }
        else
        {
            VDC_UNDER_STR_ERR=0;
        }
      #endif


        if( fu1YawSusDet==1 )
        {
            VDC_UNDER_YAW_ERR=1;
        }
        else
        {
            VDC_UNDER_YAW_ERR=0;
        }

        if( fu1AySusDet==1 )
        {
            VDC_UNDER_LAT_ERR=1;
        }
        else
        {
            VDC_UNDER_LAT_ERR=0;
        }

        if( fu1MCPSusDet==1 )  /* mp_hw_suspcs_flg */
        {
            VDC_UNDER_MP_ERR=1;
        }
        else
        {
            VDC_UNDER_MP_ERR=0;
        }
    }
    else
    {
        VDC_UNDER_STR_ERR    =0;
        VDC_UNDER_YAW_ERR    =0;
        VDC_UNDER_LAT_ERR    =0;
        VDC_UNDER_MP_ERR     =0;
    }

    if( (VDC_UNDER_STR_ERR)||(VDC_UNDER_YAW_ERR)
        ||(VDC_UNDER_LAT_ERR)||(VDC_UNDER_MP_ERR) )
    {
        PARTIAL_PERFORM=1;
    }
    else
    {
        PARTIAL_PERFORM=0;
    }

}

#endif

/* 변경 내역
**************************************************************************************
 1. BRAKE_PEDAL_ON=1조건 변경            2003.10.06. y.w.shin
 {  if((fu1BLSSignal==1)&&(fu1BSSignal==0)) BRAKE_PEDAL_ON=1;
      if((ESP_bls_k_timer >0)||(ESP_bs_k_timer >0)) BRAKE_PEDAL_ON=1;
  ==> if((fu1BLSSignal==1)&&(fu1BSSignal==0)) BRAKE_PEDAL_ON=1;
      else if((ESP_bls_k_timer >0)||(ESP_bs_k_timer >0)) BRAKE_PEDAL_ON=1;

                    2003.12.08 kim yong kil
 2. ESP 작동하면 각 wheel별로 slip chk delay 해서 ABS_permit chk한다.
 3. ABS_permit 작동후에 ABS_fz이 작동하면 계속해서 ABS_permit를 발생해준다.
*/

void LDESPPartial_vDetectCircuitFail(void)
{

/*    초기 진입 조건 변경 30%slip일때 진입 5%에서 빠짐.1st 30/nth 10%   */
 /*   if(ABS_PERMIT_BY_BRAKE_BLS==1)
    {
        ABS_permit_SLIP_nP_NEG_fl=SLIP_5P_NEG_fl;
        ABS_permit_SLIP_nP_NEG_fr=SLIP_5P_NEG_fr;
        ABS_permit_SLIP_nP_NEG_rl=SLIP_5P_NEG_rl;
        ABS_permit_SLIP_nP_NEG_rr=SLIP_5P_NEG_rr;
    }
    else if (ABS_PERMIT_BY_BRAKE_BLS_n_cycle==1)
    {
        if (ABS_PERMIT_BY_BRAKE_BLS_off_cnt>(L_U16_TIME_5MSLOOP_4_5S))
        {
            ABS_permit_SLIP_nP_NEG_fl=SLIP_10P_NEG_fl;
            ABS_permit_SLIP_nP_NEG_fr=SLIP_10P_NEG_fr;
            ABS_permit_SLIP_nP_NEG_rl=SLIP_10P_NEG_rl;
            ABS_permit_SLIP_nP_NEG_rr=SLIP_10P_NEG_rr;
        }
        else
        {
            ABS_permit_SLIP_nP_NEG_fl=SLIP_20P_NEG_fl;
            ABS_permit_SLIP_nP_NEG_fr=SLIP_20P_NEG_fr;
            ABS_permit_SLIP_nP_NEG_rl=SLIP_20P_NEG_rl;
            ABS_permit_SLIP_nP_NEG_rr=SLIP_20P_NEG_rr;
        }
    }
    else if (ABS_PERMIT_BY_BRAKE_BLS_n_cycle>=2)
    {
        ABS_permit_SLIP_nP_NEG_fl=SLIP_10P_NEG_fl;
        ABS_permit_SLIP_nP_NEG_fr=SLIP_10P_NEG_fr;
        ABS_permit_SLIP_nP_NEG_rl=SLIP_10P_NEG_rl;
        ABS_permit_SLIP_nP_NEG_rr=SLIP_10P_NEG_rr;
    }
    else
    {
        if (det_alatm > LAT_0G8G)
        {
            if (alat > LAT_0G5G)
            {
                ABS_permit_SLIP_nP_NEG_fl=0;
                ABS_permit_SLIP_nP_NEG_fr=SLIP_30P_NEG_fr;
                ABS_permit_SLIP_nP_NEG_rl=0;
                ABS_permit_SLIP_nP_NEG_rr=SLIP_30P_NEG_rr;
            }
            else if (alat < -LAT_0G5G)
            {
                ABS_permit_SLIP_nP_NEG_fl=SLIP_30P_NEG_fl;
                ABS_permit_SLIP_nP_NEG_fr=0;
                ABS_permit_SLIP_nP_NEG_rl=SLIP_30P_NEG_rl;
                ABS_permit_SLIP_nP_NEG_rr=0;
            }
            else
            {
                ABS_permit_SLIP_nP_NEG_fl=SLIP_30P_NEG_fl;
                ABS_permit_SLIP_nP_NEG_fr=SLIP_30P_NEG_fr;
                ABS_permit_SLIP_nP_NEG_rl=SLIP_30P_NEG_rl;
                ABS_permit_SLIP_nP_NEG_rr=SLIP_30P_NEG_rr;
            }
        }
        else
        {
            if (alat > LAT_0G5G)
            {
                ABS_permit_SLIP_nP_NEG_fl=SLIP_30P_NEG_fl;
                ABS_permit_SLIP_nP_NEG_fr=SLIP_30P_NEG_fr;
                ABS_permit_SLIP_nP_NEG_rl=0;
                ABS_permit_SLIP_nP_NEG_rr=SLIP_30P_NEG_rr;
            }
            else if (alat < -LAT_0G5G)
            {
                ABS_permit_SLIP_nP_NEG_fl=SLIP_30P_NEG_fl;
                ABS_permit_SLIP_nP_NEG_fr=SLIP_30P_NEG_fr;
                ABS_permit_SLIP_nP_NEG_rl=SLIP_30P_NEG_rl;
                ABS_permit_SLIP_nP_NEG_rr=0;
            }
            else
            {
                ABS_permit_SLIP_nP_NEG_fl=SLIP_30P_NEG_fl;
                ABS_permit_SLIP_nP_NEG_fr=SLIP_30P_NEG_fr;
                ABS_permit_SLIP_nP_NEG_rl=SLIP_30P_NEG_rl;
                ABS_permit_SLIP_nP_NEG_rr=SLIP_30P_NEG_rr;
            }
        }
    }
*/
/*
-------------------------------------------------Circuit Fail 감지중 ESP작동으로 추가. 2005.01.25
-------------------------------------------------------------------ESP 작동후 감지 시간 조건 변경
*/
 /*   esp_tempW9=L_U8_TIME_5MSLOOP_350MS;
    if ((ABS_PERMIT_BY_BRAKE_BLS_n_cycle>=1)&&
        ((delta_yaw>u_delta_yaw_thres)&&(delta_yaw<o_delta_yaw_thres)))
    {
        esp_tempW9=L_U8_TIME_5MSLOOP_50MS;
    }
    else if (ABS_PERMIT_BY_BRAKE_BLS_n_cycle>=1)
    {
        esp_tempW9=L_U8_TIME_5MSLOOP_100MS;
    }
    else
    {
        ;
    }

    if (FL.ESP_off_timer2>(L_U16_TIME_5MSLOOP_5S-esp_tempW9))
    {
        ABS_permit_SLIP_nP_NEG_fl=0;
    }
    else
    {
        ;
    }
    if (FR.ESP_off_timer2>(L_U16_TIME_5MSLOOP_5S-esp_tempW9))
    {
        ABS_permit_SLIP_nP_NEG_fr=0;
    }
    else
    {
        ;
    }
    if (RL.ESP_off_timer2>(L_U16_TIME_5MSLOOP_5S-esp_tempW9))
    {
        ABS_permit_SLIP_nP_NEG_rl=0;
    }
    else
    {
        ;
    }
    if (RR.ESP_off_timer2>(L_U16_TIME_5MSLOOP_5S-esp_tempW9))
    {
        ABS_permit_SLIP_nP_NEG_rr=0;
    }
    else
    {
        ;
    }
*/
/*
------------------------------------
 04 Brake pedal on/off count start
*/
    BRAKE_PEDAL_ON=0;
    if((fu1DelayedBLS==1)
        #if __BRAKE_SWITCH_ENABLE
        &&(fu1DelayedBS==0)
        #endif
    ) {
        BRAKE_PEDAL_ON=1;
    }
    else if(BLS_OR_ESP_BS==1)
    {
        BRAKE_PEDAL_ON=1;
    }
    else
    {
        BRAKE_PEDAL_ON=0;
    }

/*  if ((BRAKE_PEDAL_ON_old==0)&&(BRAKE_PEDAL_ON==1))
    {
        BRAKE_PEDAL_ON_K=1;
    }
    else if ((BRAKE_PEDAL_ON_old==1)&&(BRAKE_PEDAL_ON==0))
    {
        BRAKE_PEDAL_ON_A=1;
    }
    else
    {
        BRAKE_PEDAL_ON_A=0;
        BRAKE_PEDAL_ON_K=0;
    }
    BRAKE_PEDAL_ON_old=BRAKE_PEDAL_ON;

    if(BRAKE_PEDAL_ON==1)
    {
        BRAKE_PEDAL_ON_on_cnt++;
        if (BRAKE_PEDAL_ON_on_cnt>L_U16_TIME_5MSLOOP_5S)
        {
            BRAKE_PEDAL_ON_on_cnt=L_U16_TIME_5MSLOOP_5S;
        }
        else
        {
            ;
        }
        BRAKE_PEDAL_ON_off_cnt=L_U16_TIME_5MSLOOP_5S;
    }
    else
    {
        BRAKE_PEDAL_ON_off_cnt--;
        if (BRAKE_PEDAL_ON_off_cnt<0)
        {
            BRAKE_PEDAL_ON_off_cnt=0;
        }
        else
        {
            ;
        }
        BRAKE_PEDAL_ON_on_cnt=0;
    }*/
/*
 04 Brake pedal on/off count end

 05 MPRESS_BRAKE_ON의 압력 감소에 의한 영향 없애기 위해 MPRESS_BRAKE_ON_on/off_cnt적용
*/
/*   if ((MPRESS_BRAKE_ON_old==0)&&(MPRESS_BRAKE_ON==1))
    {
        MPRESS_BRAKE_ON_K=1;
    }
    else if ((MPRESS_BRAKE_ON_old==1)&&(MPRESS_BRAKE_ON==0))
    {
        MPRESS_BRAKE_ON_A=1;
    }
    else
    {
        MPRESS_BRAKE_ON_A=0;
        MPRESS_BRAKE_ON_K=0;
    }
*/
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    MPRESS_BRAKE_ON_old=lsespu1AHBGEN3MpresBrkOn;
      #else 
    MPRESS_BRAKE_ON_old=MPRESS_BRAKE_ON;
      #endif
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if(lsespu1AHBGEN3MpresBrkOn==1)
      #else
    if(MPRESS_BRAKE_ON==1)
      #endif
    {
        MPRESS_BRAKE_ON_on_cnt++;
        if (MPRESS_BRAKE_ON_on_cnt>L_U16_TIME_10MSLOOP_5S)
        {
            MPRESS_BRAKE_ON_on_cnt=L_U16_TIME_10MSLOOP_5S;
        }
        else
        {
            ;
        }
        MPRESS_BRAKE_ON_off_cnt=L_U16_TIME_10MSLOOP_5S;
    }
    else
    {
        MPRESS_BRAKE_ON_off_cnt--;
        if (MPRESS_BRAKE_ON_off_cnt<0)
        {
            MPRESS_BRAKE_ON_off_cnt=0;
        }
        else
        {
            ;
        }
        MPRESS_BRAKE_ON_on_cnt=0;
    }
/*
    if ((EBD_RA_old==0)&&(EBD_RA==1))
    {
        EBD_RA_K=1;
    }
    else if ((EBD_RA_old==1)&&(EBD_RA==0))
    {
        EBD_RA_A=1;
    }
    else
    {
        EBD_RA_A=0;
        EBD_RA_K=0;
    }

    EBD_RA_old=EBD_RA;

    if(EBD_RA==1)
    {
        EBD_RA_on_cnt++;
        if (EBD_RA_on_cnt>L_U16_TIME_5MSLOOP_5S)
        {
            EBD_RA_on_cnt=L_U16_TIME_5MSLOOP_5S;
        }
        else
        {
            ;
        }
        EBD_RA_off_cnt=L_U16_TIME_5MSLOOP_5S;
    }
    else {
        EBD_RA_off_cnt--;
        if (EBD_RA_off_cnt<0)
        {
            EBD_RA_off_cnt=0;
        }
        else
        {
            ;
        }
        EBD_RA_on_cnt=0;
    }*/
/*
  Parking Barking에 의한 Wheel Slip 발생시
  BLS 에 의한 민감 감지 해결
*/
/*  if (BRAKE_PEDAL_ON_on_cnt==0)
    {
        circuit_fail_initial_rel_lam_rl = rel_lam_rl;
        circuit_fail_initial_rel_lam_rr = rel_lam_rr;
    }
    else
    {
        ;
    }

    if (circuit_fail_initial_rel_lam_rl < (rel_lam_rl + LAM_5P))
    {
        ABS_permit_SLIP_nP_NEG_rl = 0;
    }
    else
    {
        ;
    }
    if (circuit_fail_initial_rel_lam_rr < (rel_lam_rr + LAM_5P))
    {
        ABS_permit_SLIP_nP_NEG_rr = 0;
    }
    else
    {
        ;
    }
*/
/*
circuit fail시 wet steel 경우 급제동일시에도 slip 발생하기 위해서는 160msec 필요.
------------------------------ABS_PERMIT_BY_BRAKE_BLS-------------------
*/
/*
    esp_tempW3=0;
  #if __HDC
    if (lcu1HdcActiveFlg==1)
    {
        esp_tempW3=1;
    }
    else {}
  #endif

  #if __ACC
    if (lcu1AccActiveFlg==1)
    {
        esp_tempW3=1;
    }
    else {}
  #endif

  #if defined(__EPB_INTERFACE)
    if (lcu1EpbActiveFlg==1)
    {
        esp_tempW3=1;
    }
    else {}
  #endif

    ABS_PERMIT_BY_BRAKE_BLS=0;
    if((BRAKE_PEDAL_ON_on_cnt>L_U8_TIME_5MSLOOP_100MS)&&(esp_tempW3==0))
    {
        if((ABS_permit_SLIP_nP_NEG_fl)||(ABS_permit_SLIP_nP_NEG_fr)||(ABS_permit_SLIP_nP_NEG_rl)||(ABS_permit_SLIP_nP_NEG_rr))
        {
            ABS_PERMIT_BY_BRAKE_BLS=1;
        }
        else {}
    }
    else {}

    if((ABS_PERMIT_BY_BRAKE_BLS_n_cycle>=3)&&(ABS_fz))
    {
        ABS_PERMIT_BY_BRAKE_BLS=1;
    }
    else {}
*/
/*  ------------------------------ABS_PERMIT_BY_BRAKE_SLIP-------------------           */

    /*    Pri,Sec circuit 술립차 확인 최대 50% 같은 circuit 5%미만. */
/*   det_circuit_p_slip_old=det_circuit_p_slip;
    det_circuit_s_slip_old=det_circuit_s_slip;

    circuit_p_slip=(abs(rel_lam_fr)>abs(rel_lam_rl))?abs(rel_lam_fr):abs(rel_lam_rl);
    circuit_s_slip=(abs(rel_lam_fl)>abs(rel_lam_rr))?abs(rel_lam_fl):abs(rel_lam_rr);  */

/*  2hz         */
/*  if (circuit_p_slip>det_circuit_p_slip)
    {
        det_circuit_p_slip=circuit_p_slip;
    }
    else
    {
         det_circuit_p_slip=LCESP_s16Lpf1Int(circuit_p_slip,det_circuit_p_slip_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
    }

    if (circuit_s_slip>det_circuit_s_slip)
    {
        det_circuit_s_slip=circuit_s_slip;
    }
    else
    {
        det_circuit_s_slip=LCESP_s16Lpf1Int(circuit_s_slip,det_circuit_s_slip_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
    }

    if (det_circuit_s_slip>det_circuit_p_slip)
    {
        det_rel_lam_fl_old=det_rel_lam_fl;
        if (abs(rel_lam_fl)>det_rel_lam_fl)
        {
            det_rel_lam_fl=abs(rel_lam_fl);
        }
        else
        {
            det_rel_lam_fl=LCESP_s16Lpf1Int(abs(rel_lam_fl),det_rel_lam_fl_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
        }
        det_rel_lam_rr_old=det_rel_lam_rr;
        if (abs(rel_lam_rr)>det_rel_lam_rr)
        {
            det_rel_lam_rr=abs(rel_lam_rr);
        }
        else
        {
            det_rel_lam_rr=LCESP_s16Lpf1Int(abs(rel_lam_rr),det_rel_lam_rr_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
        }

        if((det_rel_lam_fl>(int8_t)LAM_10P)&&(det_rel_lam_rr>(int8_t)LAM_10P))
        {
            esp_tempW5=1;
        }
        else
        {
            esp_tempW5=0;
        }
    }
    else
    {
        det_rel_lam_fr_old=det_rel_lam_fr;
        if (abs(rel_lam_fr)>det_rel_lam_fr)
        {
            det_rel_lam_fr=abs(rel_lam_fr);
        }
        else
        {
            det_rel_lam_fr=LCESP_s16Lpf1Int(abs(rel_lam_fr),det_rel_lam_fr_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
        }
        det_rel_lam_rl_old=det_rel_lam_rl;
        if (abs(rel_lam_rl)>det_rel_lam_rl)
        {
            det_rel_lam_rl=abs(rel_lam_rl);
        }
        else
        {
            det_rel_lam_rl=LCESP_s16Lpf1Int(abs(rel_lam_rl),det_rel_lam_rl_old,L_U8FILTER_GAIN_5MSLOOP_2HZ);
        }

        if((det_rel_lam_fr>(int8_t)LAM_10P)&&(det_rel_lam_rl>(int8_t)LAM_10P))
        {
            esp_tempW5=1;
        }
        else
        {
            esp_tempW5=0;
        }
    }
    if ((rel_lam_fl > (int8_t)LAM_5P)||(rel_lam_fr > (int8_t)LAM_5P))
    {
        esp_tempW5=0;              
    }

    esp_tempW9=abs(det_circuit_p_slip-det_circuit_s_slip);
    esp_tempW8=abs(abs(rel_lam_fl)-abs(rel_lam_rr));
    esp_tempW7=abs(abs(rel_lam_fr)-abs(rel_lam_rl));


  #if __SPLIT_TYPE      
    esp_tempW6=0;
  #else                  
    if((esp_tempW9>(int8_t)LAM_50P)&&(esp_tempW5)&&((esp_tempW8<(int8_t)LAM_10P)||(esp_tempW7<(int8_t)LAM_10P)))
    {
        esp_tempW6=1;
    }
    else
    {
        esp_tempW6=0;
    } */
    /*  esp_tempW5   x-split인 경우 해당함
        esp_tempW9
        esp_tempW8
        esp_tempW7
        esp_tempW6                              */
 /* #endif      ~__SPLIT_TYPE   */
 /*
    esp_tempW4 = LCESP_s16IInter2Point(vref, VREF_15_KPH, LAM_100P, VREF_45_KPH, LAM_50P);

    if((!ABS_fz)&&(!YAW_CDC_WORK)&&(ESP_end_timer < L_U16_TIME_5MSLOOP_4S)&&(vref>VREF_15_KPH))
    { */   /* ESP 동작 없었던지 아니면 ESP 동작 후 1초 경과이면    */
/*     tempB1=(int8_t)(abs(rel_lam_fl-rel_lam_fr));
        tempB2=(int8_t)(abs(rel_lam_rl-rel_lam_rr));

        if (AUTO_TM==1)
        {
            if((tempB1 >= (int8_t)esp_tempW4)&&(rel_lam_fl < (int8_t)LAM_5P)&&(rel_lam_fr< (int8_t)LAM_5P))
            {
                if(abs_permit_count < 0xff)
                {
                    abs_permit_count++;
                }
                else
                {
                    ;
                }
            }
            else if((tempB2 >= (int8_t)esp_tempW4)&&(rel_lam_rl < (int8_t)LAM_5P)&&(rel_lam_rr< (int8_t)LAM_5P))
            {
                if((abs_permit_count < 0xff)&&(esp_tempW6))
                {
                    abs_permit_count++;
                }
                else
                {
                    ;
                }
            }
            else
            {
                abs_permit_count=0;
            }

            if(abs_permit_count > L_U8_TIME_5MSLOOP_200MS)
            {
                ABS_PERMIT_BY_BRAKE_SLIP=1;           초기 감지 21msec ==> 200msec      
            }
            else
            {
                ;
            }

        }
        else
        {
            if((tempB1 >= (int8_t)esp_tempW4)&&(rel_lam_fl < (int8_t)LAM_5P)&&(rel_lam_fr< (int8_t)LAM_5P)
                &&((vrad_fl < VREF_3_KPH)||(vrad_fr < VREF_3_KPH)))
            {
                if(abs_permit_count < 0xff)
                {
                    abs_permit_count++;
                }
                else
                {
                    ;
                }

            }
            else if((tempB2 >= (int8_t)esp_tempW4)&&(rel_lam_rl < (int8_t)LAM_5P)&&(rel_lam_rr< (int8_t)LAM_5P))
            {
                if((abs_permit_count < 0xff)&&(esp_tempW6))
                {
                    abs_permit_count=abs_permit_count+2;
                }
                else
                {
                    ;
                }
            }
            else
            {
                abs_permit_count=0;
            }

            if(abs_permit_count > L_U8_TIME_5MSLOOP_300MS)
            {
                ABS_PERMIT_BY_BRAKE_SLIP=1;           초기 감지 21msec ==> 200msec(Wheel Lock Time 추가)  
            }
            else
            {
                ;
            }

        }
    }

 
================================================================================================
 bls 고장시 ABS permit check하기 위함  2004.10.13 y.w.shin
 slip 조건 변경 2005.01.14    KYK
 감지 최소 시간 생성500msec 2006.01.21 KYK
================================================================================================
 
    if (FL.ESP_on_timer >= L_U8_TIME_5MSLOOP_500MS)
    {
        FL.Circuit_fail_chk_enable=1;
    }
    else if (FL.ESP_off_timer2 <= (L_U16_TIME_5MSLOOP_5S - L_U8_TIME_5MSLOOP_300MS))
    {
        FL.Circuit_fail_chk_enable=0;
    }
    else
    {
        ;
    }
    if (FR.ESP_on_timer >= L_U8_TIME_5MSLOOP_500MS)
    {
        FR.Circuit_fail_chk_enable=1;
    }
    else if (FR.ESP_off_timer2 <= (L_U16_TIME_5MSLOOP_5S - L_U8_TIME_5MSLOOP_300MS))
    {
        FR.Circuit_fail_chk_enable=0;
    }
    else
    {
        ;
    }

    if((!ABS_fz)&&(vref>VREF_15_KPH))
    {
        if((FL.ESP_on_timer ==0)&&(FL.ESP_off_timer > (L_U16_TIME_5MSLOOP_3S-L_U8_TIME_5MSLOOP_300MS))&&(ESP_BRAKE_CONTROL_FR==0)&&(FL.Circuit_fail_chk_enable==1))
        {
            if((rel_lam_fr <= -(int8_t)LAM_50P)&&(rel_lam_fl< (int8_t)LAM_5P)&&(rel_lam_fr<rel_lam_fl))
            {
                if(FL.abs_permit_count0 < 0xff)
                {
                    FL.abs_permit_count0++;
                }
                else
                {
                    ;
                }
            }
            else
            {
                FL.abs_permit_count0=0;
            }

            if(FL.abs_permit_count0 > L_U8_TIME_5MSLOOP_20MS)
            {
                ABS_PERMIT_BY_BRAKE_SLIP=1;
            }
            else
            {
                ;
            }
        }
        else
        {
            FL.abs_permit_count0=0;
        }

        if((FR.ESP_on_timer ==0)&&(FR.ESP_off_timer > (L_U16_TIME_5MSLOOP_3S-L_U8_TIME_5MSLOOP_300MS))&&(ESP_BRAKE_CONTROL_FL==0)&&(FR.Circuit_fail_chk_enable==1)) {

            if((rel_lam_fl <= -(int8_t)LAM_50P)&&(rel_lam_fr< (int8_t)LAM_5P)&&(rel_lam_fl<rel_lam_fr))
            {
                if(FR.abs_permit_count0 < 0xff)
                {
                    FR.abs_permit_count0++;
                }
                else
                {
                    ;
                }
            }
            else
            {
                FR.abs_permit_count0=0;
            }

            if(FR.abs_permit_count0 > L_U8_TIME_5MSLOOP_20MS)
            {
                ABS_PERMIT_BY_BRAKE_SLIP=1;
            }
            else
            {
                ;
            }
        }
        else
        {
            FR.abs_permit_count0=0;
        }

    }
    else
    {
        ;
    }

    if (mtp>15)
    {
        ABS_PERMIT_BY_BRAKE_SLIP=0;
    }
    else
    {
        ;
    }

 ================================================================================================  
    if((MPRESS_BRAKE_ON_off_cnt>(L_U16_TIME_5MSLOOP_5S-42))||(standstill_flg==1))
    {
        ABS_PERMIT_BY_BRAKE_SLIP=ABS_PERMIT_BY_BRAKE_BLS=0;
    }
    else
    {
        ;
    }

    if((ABS_PERMIT_BY_BRAKE_BLS)||(ABS_PERMIT_BY_BRAKE_SLIP))
    {
        ABS_PERMIT_BY_BRAKE=1;
    }
    else
    {
        ABS_PERMIT_BY_BRAKE=0;
    }

 ------------------------------- ABS_PERMIT_BY_BRAKE_BLS_on_cnt;_off_cnt;_K;_A  
    if(ABS_PERMIT_BY_BRAKE_BLS==1)
    {
        ABS_PERMIT_BY_BRAKE_BLS_on_cnt++;
        if (ABS_PERMIT_BY_BRAKE_BLS_on_cnt>L_U16_TIME_5MSLOOP_5S)
        {
            ABS_PERMIT_BY_BRAKE_BLS_on_cnt=L_U16_TIME_5MSLOOP_5S;
        }
        else
        {
            ;
        }
        ABS_PERMIT_BY_BRAKE_BLS_off_cnt=L_U16_TIME_5MSLOOP_5S;
    }
    else
    {
        ABS_PERMIT_BY_BRAKE_BLS_off_cnt--;
        if (ABS_PERMIT_BY_BRAKE_BLS_off_cnt<0)
        {
            ABS_PERMIT_BY_BRAKE_BLS_off_cnt=0;
        }
        else
        {
            ;
        }
        ABS_PERMIT_BY_BRAKE_BLS_on_cnt=0;
    }

    ABS_PERMIT_BY_BRAKE_BLS_A=ABS_PERMIT_BY_BRAKE_BLS_K=0;
    if ((!ABS_PERMIT_BY_BRAKE_BLS_OLD)&&(ABS_PERMIT_BY_BRAKE_BLS))
    {
        ABS_PERMIT_BY_BRAKE_BLS_K=1;
    }
    else
    {
        ;
    }
    if ((ABS_PERMIT_BY_BRAKE_BLS_OLD)&&(!ABS_PERMIT_BY_BRAKE_BLS))
    {
        ABS_PERMIT_BY_BRAKE_BLS_A=1;
    }
    else
    {
        ;
    }

    ABS_PERMIT_BY_BRAKE_BLS_OLD=ABS_PERMIT_BY_BRAKE_BLS;

 ---------------------------------- ABS_PERMIT_BY_BRAKE_BLS_cnt    
    if (ABS_PERMIT_BY_BRAKE_BLS_K==1)
    {
        ABS_PERMIT_BY_BRAKE_BLS_cnt=L_U16_TIME_5MSLOOP_5S;
    }
    else
    {
        if(ABS_PERMIT_BY_BRAKE_BLS_cnt>0)
        {
            ABS_PERMIT_BY_BRAKE_BLS_cnt--;
        }
        else
        {
            ABS_PERMIT_BY_BRAKE_BLS_cnt=0;
        }
    }
 --------------------------------- ABS_PERMIT_BY_BRAKE_BLS_n_cycle  
    if ( (ABS_PERMIT_BY_BRAKE_BLS_cnt>(L_U16_TIME_5MSLOOP_2S))&&(ABS_PERMIT_BY_BRAKE_BLS_K==1) )
    {
        ABS_PERMIT_BY_BRAKE_BLS_n_cycle++;
    }
    else
    {
        ;
    }

    if (((ABS_PERMIT_BY_BRAKE_BLS_cnt==0)&&(ABS_fz==0))||(MPRESS_BRAKE_ON==1)||(standstill_flg==1)||(mtp>8))
    {
        ABS_PERMIT_BY_BRAKE_BLS_n_cycle=0;
    }
    else
    {
        ;
    }
*/
/*
 MPRESS_BRAKE_ON의 압력 감소에 의한 영향 없애기 위해 MPRESS_BRAKE_ON_on/off_cnt적용 70msec 적용
 2003.12.08 kim yong kil
 350msec -> 210msec 지연
 초기 진입 30%로 증가하면서 210msec로 변경.
 ABS_PERMIT_BY_BRAKE_BLS 이후 200msec 이내 30% -> 10%로 변경 2004.07.27
 */
}

/*void ESP_ON_OFF_TIME_CAL(void)
{
    FL.PF_SLIP_CONTROL = ESP_BRAKE_CONTROL_FL;
    FR.PF_SLIP_CONTROL = ESP_BRAKE_CONTROL_FR;
    RL.PF_SLIP_CONTROL = ESP_BRAKE_CONTROL_RL;
    RR.PF_SLIP_CONTROL = ESP_BRAKE_CONTROL_RR;

    ESP_ON_OFF_TIME_CAL_wl(&FL);
    ESP_ON_OFF_TIME_CAL_wl(&FR);
    ESP_ON_OFF_TIME_CAL_wl(&RL);
    ESP_ON_OFF_TIME_CAL_wl(&RR);
}

void ESP_ON_OFF_TIME_CAL_wl(struct W_STRUCT *WL)
{
 --------------------------------------------------------------
  ESP ON OFF 시간동안 K,A 생성
 
    WL->PF_SLIP_K=0;
    WL->PF_SLIP_A=0;

    if(WL->PF_SLIP_CONTROL_OLD==0)
    {
        if(WL->PF_SLIP_CONTROL == 1)
        {      start of ESP SLIP CONTROL  
            WL->PF_SLIP_K=1;
        }
        else {}
    }
    else
    {                               end of ESP SLIP CONTROL   
        if(WL->PF_SLIP_CONTROL == 0)
        {
                WL->PF_SLIP_A=1;
        }
        else {}
    }
 
--------------------------------------------------------------
    ESP ON,OFF Timer    on_timer는 정상 작동 최대 128이고
    off_timer는 esp off 부터 3S에서 감소 하는 것.
    off_timer2는 esp on 하면 714(5S)로 set한 후 off 하면 감소 하는 것.
 
    if(WL->ESP_off_timer !=0)
    {
        WL->ESP_off_timer--;
    }
    else {}

    if(WL->ESP_off_timer2 !=0)
    {
        WL->ESP_off_timer2--;
    }
    else {}

    if(WL->PF_SLIP_CONTROL==1)
    {
        if(WL->ESP_on_timer < 0xff)
        {
            WL->ESP_on_timer++;
        }
        else {}
        WL->ESP_off_timer=0;
        WL->ESP_off_timer2 = L_U16_TIME_5MSLOOP_5S;
            ABS_PE참조, PREFILL참조   
    }
    else if(WL->PF_SLIP_A==1)
    {
        WL->ESP_off_timer =L_U16_TIME_5MSLOOP_3S;
     if(WL->ESP_on_timer >=T_200MS) WL->ESP_off_timer =T_500MS;    
        WL->ESP_on_timer=0;
    }
    else {}
    WL->PF_SLIP_CONTROL_OLD =WL->PF_SLIP_CONTROL;
} */
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCallPartialDetection
	#include "Mdyn_autosar.h"
#endif
