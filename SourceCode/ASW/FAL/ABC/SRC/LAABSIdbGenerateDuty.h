/****************************************************
 *  LAABSIdbGenerateDuty.h                                         
 *  Created on: 25-9-2014 PM 7:27:56
 *  Implementation of the Class LAABSIdbGenerateDuty       
 *  Original author: seongyon.ryu                     
 ****************************************************/

#ifndef __LAABSIDBGENERATEDUTY_H_
#define __LAABSIDBGENERATEDUTY_H_

#include "LVarHead.h"
#define __DESIRED_BOOST_ONTIME_MAP   ENABLE

/* DeltaP Compensation : MP_Cyclic_Comp*/
#define U8_Initial_Duty_F	120
#define U8_Initial_Duty_R	100
#define U8_Initial_Duty_Comp_Low_F	20
#define U8_Initial_Duty_Comp_Low_R	20

#define NO_VV_ONOFF_CTRL	1 /*0: NO valve LFC / 1: NO valve on off*/

#define U8_DEFAULT_RISESCANTIME	10
#define U8_EBD_LFC_INITIAL_DUTY 		70 /* MGH-60 : 70 */


extern void LAABS_vGenerateLFCDuty(void);

#endif /*!defined(__LAABSIDBGENERATEDUTY_H_)*/
 
