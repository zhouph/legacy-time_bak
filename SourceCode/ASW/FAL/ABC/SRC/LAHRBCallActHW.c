/******************************************************************************
* Project Name: Hydraulic Rear Wheel Boost control
* File: LAHRBCallActHW.C
* Description: Valve/Motor actuaction by Hydraulic Rear Wheel Boost Controller
* Date: Dec. 11. 2006
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAHRBCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* Includes ******************************************************************/
#include "LAHRBCallActHW.h"
#include "LCHRBCallControl.h"

#if __HRB
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/


/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAHRB_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Hydraulic Rear Wheel Boost Controller
******************************************************************************/
void	LAHRB_vCallActHW(void)
{
	if(lchrbu1ActiveFlag==1) {
	  #if (__SPLIT_TYPE==0)	
    		TCL_DEMAND_fl = lchrbu1TCSecondary;		
    		TCL_DEMAND_fr = lchrbu1TCPrimary;
	    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_RR==0)
	     &&(ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_RL==0))
	    {
    		S_VALVE_LEFT = lchrbu1ESVSecondary;
    		S_VALVE_RIGHT = lchrbu1ESVPrimary;
    	}
	  #else
		TCL_DEMAND_SECONDARY = lchrbu1TCSecondary;	
		if((ESP_BRAKE_CONTROL_RL==0)&&(ESP_BRAKE_CONTROL_RR==0))
		{
    		S_VALVE_SECONDARY = lchrbu1ESVSecondary;		    
		}
	  #endif	
		
		VALVE_ACT = 1;
	}
} 
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAHRBCallActHW
	#include "Mdyn_autosar.h"               
#endif