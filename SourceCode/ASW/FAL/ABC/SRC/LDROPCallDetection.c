
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDROPCallDetection
	#include "Mdyn_autosar.h"
#endif

#if GMLAN_ENABLE==DISABLE
#endif


#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LDROPCallDetection.h"
#include "LDESPDetectVehicleStatus.h"
#include "LDESPCalVehicleModel.h"

U8_BIT_STRUCT_t ROPF0;
U8_BIT_STRUCT_t ROPF1;
U8_BIT_STRUCT_t ROPF2;
U8_BIT_STRUCT_t ROPF3;

  
int16_t    alat_dot2_lf, alat_dot2, alat_dot2_lf_old ;


#if __ROP
void LDROP_vCallDetection(void);
void LDROP_vCompRollModel(void);
void LDROP_vCompVelBetaDot(void);
void LDROP_vCompROPIndex(void);
void LDROP_vCheckFadeMode(void);
void LDROP_vCheckPreROP(void);
void LDROP_vCheck_Ay_1st_Cycle(void);

   

//int16_t    roll_angle_old, roll_angle, roll_angle_rate_old, roll_angle_rate, xs1_roll_old, xs1_roll, xs2_roll_old, xs2_roll ; 	
int16_t    beta_dot_lf_old, beta_dot_lf, vel_beta_dot ;
int16_t    index_weight_ay, RI_1_Weight;
//int16_t    RI_max ;
int16_t    det_RI_max, det_RI_max_old, ROP_FADE_cnt ;
int16_t    RI_1, RI_2 ;
int16_t    moment_pre_front_rop;
//int16_t    moment_q_front_rop ; 
//int8_t   	 TTL_time;
int16_t    ROP_FrtPrefill_WstrDot_Ent, ROP_FrtPrefill_WstrDot_Ext, ROP_FrtPrefill_YawDot_Ent, ROP_FrtPrefill_YawDot_Ext  ;
int8_t   	 ttp_cnt_fr, ttp_cnt_fl, time_to_prefill  ; 
 
 
 /*  Vehicle's Roll-Model parameter  */
 
    #define	sam_t_roll      10       
        
/*      #define	mass_roll       137                                          
        #define	iix_roll        759                                          
        #define	roll_damping    760                                          
        #define	roll_stiffness  16731                                        
        #define	h_roll          168
*/
        
/*    
    #if  __CAR == KMC_HM
    
        #define	mass_roll       192          Ms , mass(1/10)  
        #define	iix_roll        506           ii_phi, iix(1)  
        #define	roll_damping    1267           Cphi, roll_damping(1/10)  
        #define	roll_stiffness  16731          Kphi, roll_stiffness(1/10)  
        #define	sam_t_roll      5              
        #define	h_roll          84             hs , h_roll(100) 
                                               
    #elif  __CAR== KMC_XM                      
                                               
        #define	mass_roll       195            Ms , mass(1/10)  
        #define	iix_roll        459            ii_phi, iix(1)  
        #define	roll_damping    1110            Cphi, roll_damping(1/10)  
        #define	roll_stiffness  18731          Kphi, roll_stiffness(1/10)  
        #define	sam_t_roll      5              
        #define	h_roll          80             hs , h_roll(100)  
                                               
    #elif  __CAR==KMC_UN
    
        #define	mass_roll       137           Ms , mass(1/10) 
        #define	iix_roll        759           ii_phi, iix(1)  
        #define	roll_damping    810           Cphi, roll_damping(1/10)  
        #define	roll_stiffness  16731         Kphi, roll_stiffness(1/10)  
        #define	sam_t_roll      5      
        #define	h_roll          116            hs , h_roll(100)       
                    
    #else 

        #define	mass_roll       137            Ms , mass(1/10)   
        #define	iix_roll        759            ii_phi, iix(1)    
        #define	roll_damping    760           Cphi, roll_damping(1/10)  
        #define	roll_stiffness  16731          Kphi, roll_stiffness(1/10) 
        #define	sam_t_roll      5             
        #define	h_roll          168            hs , h_roll(100)  
                                                   
     #endif	                                     
*/                                               

#endif


#if __ROP
void LDROP_vCallDetection(void)
{
     LDROP_vCompRollModel();
     LDROP_vCompVelBetaDot();   
     LDROP_vCompROPIndex();        
     LDROP_vCheckFadeMode();     
     LDROP_vCheckPreROP();
     
     LDROP_vCheck_Ay_1st_Cycle();
}

void LDROP_vCompRollModel(void)
{
	xs1_roll_old = xs1_roll ;				    /* r=0.01   */
	xs2_roll_old = xs2_roll ; 	                /* r=0.1	*/ 	
	roll_angle_old = roll_angle ;				/* r=0.01   */
	roll_angle_rate_old = roll_angle_rate ; 	/* r=0.1	*/           

/*****  Roll-motion : 2-DOF  *****/
	/* tempW2 = xs2_roll_old ; tempW1 = sam_t_roll ; tempW0 = 100; 
	s16muls16divs16();
	esp_tempW4 = tempW3 ;   */
	esp_tempW4 = (int16_t)( (((int32_t)xs2_roll_old)*sam_t_roll)/100 );  /* roll_angle_rate(5000,0.01),sam_t(7,0.001) */ 

	xs1_roll = esp_tempW4 + xs1_roll_old  ;            /* r=0.01  */

	roll_angle = LCESP_s16Lpf1Int(xs1_roll, roll_angle_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_17HZ);          /* Key Var, r = 1/100 */
	
/*****  Overflow prevention *****/
        if (roll_angle > 30000)                   
        {
             roll_angle = 30000 ;
        }
        else if (roll_angle < -30000)
        {
            roll_angle = -30000 ;
        }
        else
        {
            roll_angle = roll_angle ;      	
        }   	
            
/* mass(137,10), iix(759,1), h_roll(168,0.01), roll_damping(760,10), roll_stiffness(16731,10)    */
   	/* tempW2 = mass_roll*sam_t_roll ;  tempW1 = alat  ; tempW0 = 1780 ;        
    s16muls16divs16();
    esp_tempW2 = tempW3;    */
    esp_tempW2 = (int16_t)( (((int32_t)(mass_roll*sam_t_roll))*alat)/1780 ); 
    
    /* tempW2 = esp_tempW2  ; tempW1 = h_roll  ; tempW0 = iix_roll ;     
    s16muls16divs16();
    esp_tempW5 = tempW3;    */
    esp_tempW5 = (int16_t)( (((int32_t)esp_tempW2)*h_roll)/iix_roll );  /* [1] */
    
    /* tempW2 = roll_damping*sam_t_roll ; tempW1 = xs2_roll_old  ; tempW0 = iix_roll;     
    s16muls16divs16();
    esp_tempW3 = tempW3;    */    
    esp_tempW3 = (int16_t)( (((int32_t)(roll_damping*sam_t_roll))*xs2_roll_old)/iix_roll ); 
    
    /* tempW2 = esp_tempW3 ; tempW1 = 1 ; tempW0 = 100 ;       
    s16muls16divs16();
    esp_tempW6 = tempW3;    */    
    esp_tempW6 = (int16_t)( (((int32_t)esp_tempW3)*1)/100 );  /* [2] */
    
    /* tempW2 = sam_t_roll  ; tempW1 = roll_stiffness  ; tempW0 = 100;     
    s16muls16divs16();
    esp_tempW4 = tempW3;    */ 
    esp_tempW4 = (int16_t)( (((int32_t)sam_t_roll)*roll_stiffness)/100 );
          
    /* tempW2 = esp_tempW4 ; tempW1 = xs1_roll_old ; tempW0 = iix_roll*10 ;       
    s16muls16divs16();
    esp_tempW7 = tempW3;    */   
    esp_tempW7 = (int16_t)( (((int32_t)esp_tempW4)*xs1_roll_old)/(iix_roll*10) );   /* [3] */

	xs2_roll = xs2_roll_old + esp_tempW5 - esp_tempW6 - esp_tempW7 ;            /* r=0.1   */
	roll_angle_rate = LCESP_s16Lpf1Int(xs2_roll, roll_angle_rate_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_17HZ);             /* Key Var, r = 1/10 */
	
/*****  Overflow prevention *****/
        if (roll_angle_rate > 30000)   
        {                
             roll_angle_rate = 30000 ;
        }
        else if (roll_angle_rate < -30000)
        {
            roll_angle_rate = -30000 ;
        }
        else
        {
            roll_angle_rate = roll_angle_rate ;      		
        }
   		
}

void LDROP_vCompVelBetaDot(void)
{    
    beta_dot_lf_old = beta_dot_lf;
    
/*  Overflow prevention , vel(15kph), vref5(15*8) Lower_Limit */
    if ( vref5 < 120 ) {
        esp_tempW7 = 120 ;
    } else  {
        esp_tempW7 = vref5 ;        
    }    

/*    esp_tempW1 = (int16_t)((((int32_t)alat)*1619)/vref5);    r : 0.01 , 1619 = 9.81*8*3.6*180/10/pi */
    /* tempW2 = alat; tempW1 = 1619; tempW0 = esp_tempW7;
    s16muls16divs16();
    esp_tempW1 = tempW3;           */                                         
    esp_tempW1 = (int16_t)( (((int32_t)alat)*1619)/esp_tempW7 );  
                                                    
    esp_tempW2 = esp_tempW1 - yaw_out ;
    
/*      Fc=1.1Hz(T=0.15,122,6) | Fc=2Hz(T=0.081,117,11) | Fc=3Hz(T=0.053,111,17)        */

    beta_dot_lf = LCESP_s16Lpf1Int(esp_tempW2, beta_dot_lf_old, L_U8FILTER_GAIN_10MSLOOP_7HZ);         /*  r=0.01 */
    
    if ( beta_dot_lf > 30000 ) {
        beta_dot_lf = 30000;
    } else if ( beta_dot_lf < -30000 ) {
        beta_dot_lf = -30000;    
    } else  {
        beta_dot_lf = beta_dot_lf ;        
    }           

    /* tempW2 = beta_dot_lf; tempW1 = esp_tempW7; tempW0 = -1619;
    s16muls16divs16();
    vel_beta_dot = tempW3;          */
    vel_beta_dot = (int16_t)( (((int32_t)beta_dot_lf)*esp_tempW7)/(-1619) );     /* r=0.001 ; Sign Inverse !!!  */
    
}     

void LDROP_vCompROPIndex(void)
{    
     
    ROP_REQ_ACT_old = ROP_REQ_ACT ;        

    /* Phase_Plane Judgement  */  			
    /* tempW2 = roll_angle; tempW1 = roll_angle_rate; tempW0 = 1000;
    s16muls16divs16();
    esp_tempW3 = tempW3;	*/	       
    esp_tempW3 = (int16_t)( (((int32_t)roll_angle)*roll_angle_rate)/1000 ); 
    
/*  Overflow prevention , roll_angle ( 0.5 deg, r =100) : Lower_Limit */
    if ( McrAbs(roll_angle) < 50 ) {
        esp_tempW7 = 50 ;
    } else  {
        esp_tempW7 = McrAbs(roll_angle) ;        
    }    
                     
    /* tempW2 = McrAbs(roll_angle_rate) ; tempW1 = 100; tempW0 = esp_tempW7;
    s16muls16divs16();
    esp_tempW4 = tempW3;	  */       
    esp_tempW4 = (int16_t)( (((int32_t)(McrAbs(roll_angle_rate)))*100)/esp_tempW7 );  /*  phase judgement ( 20, 5, 1.5 ) r =10  */
                                              
/* '06.3.13 : Stationay vs. Dynamic Handling  */        

/*  Steady-state Cornering : RI_1  */                         
    if ( esp_tempW3 >= 0 ) {
        
        /* PHI_threshold, r = 100 */
    	esp_tempW8 = LCESP_s16IInter4Point( esp_tempW4, 0,  S16_SS_PHI_TH_1st,
    	                                       15, S16_SS_PHI_TH_2nd,
    					                       50, S16_SS_PHI_TH_3rd,						                      
    				                           200, S16_SS_PHI_TH_4th ) ;		    
    
        /* PHI_DOT_threshold,  r = 10 */
    	esp_tempW9 = LCESP_s16IInter4Point( esp_tempW4, 0, S16_SS_PHI_DOT_TH_1st,
    						                   15, S16_SS_PHI_DOT_TH_2nd,		    	
    						                   50, S16_SS_PHI_DOT_TH_3rd,						                      
    					                       200, S16_SS_PHI_DOT_TH_4th ) ;	        
    } else {   

    	esp_tempW8 = S16_SS_PHI_TH_1st ;		    
    	esp_tempW9 = 2000 ;	 
    }                                      
    					                              
	/* RI_1	 */
    /* tempW2 = McrAbs(roll_angle); tempW1 = 100; tempW0 = esp_tempW8;
    s16muls16divs16();
    esp_tempW1 = tempW3; */
    esp_tempW1 = (int16_t)( (((int32_t)(McrAbs(roll_angle)))*100)/esp_tempW8 ) ;
                     
    /* tempW2 = McrAbs(roll_angle_rate); tempW1 = 100; tempW0 = esp_tempW9;
    s16muls16divs16();
    esp_tempW2 = tempW3;   */
    esp_tempW2 = (int16_t)( (((int32_t)(McrAbs(roll_angle_rate)))*100)/esp_tempW9 ) ;
                                                    
    RI_1 = McrAbs(esp_tempW1 + esp_tempW2) ;       /* r = 100  */
        
/*  Dynamic Handling ( Fishhook, DLC, ... ) : RI_2  */                         
    if ( esp_tempW3 >= 0 ) {
        
        /* PHI_threshold, r = 100 */
    	esp_tempW8 = LCESP_s16IInter4Point( esp_tempW4, 0,   S16_DYN_PHI_TH_1st,
    	                                       15,  S16_DYN_PHI_TH_2nd,
    					                       50,  S16_DYN_PHI_TH_3rd,						                      
    				                           200, S16_DYN_PHI_TH_4th ) ;		    
    
        /* PHI_DOT_threshold,  r = 10 */
    	esp_tempW9 = LCESP_s16IInter4Point( esp_tempW4, 0,   S16_DYN_PHI_DOT_TH_1st,
    						                   15,  S16_DYN_PHI_DOT_TH_2nd,		    	
    						                   50,  S16_DYN_PHI_DOT_TH_3rd,						                      
    					                       200, S16_DYN_PHI_DOT_TH_4th ) ;	        
    } else {   

    	esp_tempW8 = S16_DYN_PHI_TH_1st ;		    
    	esp_tempW9 = 2000 ;	 
    }                   
    		    					                              
	/* RI_2 */
    /* tempW2 = McrAbs(roll_angle); tempW1 = 100; tempW0 = esp_tempW8;
    s16muls16divs16();
    esp_tempW1 = tempW3;  */
    esp_tempW1 = (int16_t)( (((int32_t)(McrAbs(roll_angle)))*100)/esp_tempW8 ) ;
                     
    /* tempW2 = McrAbs(roll_angle_rate); tempW1 = 100; tempW0 = esp_tempW9;
    s16muls16divs16();
    esp_tempW2 = tempW3;  */
    esp_tempW2 = (int16_t)( (((int32_t)(McrAbs(roll_angle_rate)))*100)/esp_tempW9 ) ;
                                                    
    RI_2 = McrAbs(esp_tempW1 + esp_tempW2) ;       /* r = 100   */
    
/*  RI_max : dep. on Wstr_rate &   */    
	esp_tempW7 = LCESP_s16IInter2Point( det_wstr_dot_lf, S16_SS_WSTR_DOT_MAX,  0, 	
					                                     S16_DYN_WSTR_DOT_MIN, 100 ) ;		
					                            
    /* tempW2 = 100 - esp_tempW7;  tempW1 = RI_1;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW1 = tempW3;  */
    esp_tempW1 = (int16_t)( (((int32_t)(100 - esp_tempW7))*RI_1)/100 ) ;
    
    /* tempW2 = esp_tempW7;  tempW1 = RI_2;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW2 = tempW3;  */   
    esp_tempW2 = (int16_t)( (((int32_t)esp_tempW7)*RI_2)/100 ) ;
    
    RI_1_Weight = esp_tempW1 + esp_tempW2  ;  
        
    /* Weighting Factor w/ Ay    */
					                           
	index_weight_ay = LCESP_s16IInter5Point( det_alatm, S16_ROP_ENTER_ALAT_TH_1st,  S16_ROP_ENTER_ALAT_WEG_1st, 	
						                       S16_ROP_ENTER_ALAT_TH_2nd,  S16_ROP_ENTER_ALAT_WEG_2nd,
						                       S16_ROP_ENTER_ALAT_TH_3rd,  S16_ROP_ENTER_ALAT_WEG_3rd,						                      
					                           S16_ROP_ENTER_ALAT_TH_4th,  S16_ROP_ENTER_ALAT_WEG_4th,
					                           S16_ROP_ENTER_ALAT_TH_5th, S16_ROP_ENTER_ALAT_WEG_5th ) ;						                           		                          				                          
					  				 
    /* tempW2 = RI_1_Weight; tempW1 = index_weight_ay; tempW0 = 100 ;     
    s16muls16divs16();
    RI_max = tempW3;   */ 
    RI_max = (int16_t)( (((int32_t)RI_1_Weight)*index_weight_ay)/100 ) ;
    
    det_RI_max_old = det_RI_max;

    if ( RI_max >= det_RI_max ) {
        det_RI_max = LCESP_s16Lpf1Int(RI_max, det_RI_max_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_10_5HZ); /* Fc 7 Hz */
    } else {
        det_RI_max = LCESP_s16Lpf1Int(RI_max, det_RI_max_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_1HZ);  /* [11, 2Hz,80msec],[5, 1Hz,150msec],[2, 0.5Hz,300msec] */
    }    
        
    /*  Entrance Sensitivity dependent on Velocity  */        

    esp_tempW5 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,  (int16_t)S16_ROP_RI_THRES_VEL_20K, 	
    						                    VREF_K_40_KPH,  (int16_t)S16_ROP_RI_THRES_VEL_40K,
					                            VREF_K_60_KPH,  (int16_t)S16_ROP_RI_THRES_VEL_60K,
					                            VREF_K_80_KPH,  (int16_t)S16_ROP_RI_THRES_VEL_80K,					                            
					                            VREF_K_100_KPH, (int16_t)S16_ROP_RI_THRES_VEL_100K ) ;	      
					                            
    /* tempW2 = S16_RI_THRES; 
    tempW1 = esp_tempW5; 
    tempW0 = 100 ;     
    s16muls16divs16();
    esp_tempW6 = tempW3;   */
    
  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
  		      if( yaw_out > 0 ) 
            {
                esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, U8_ROP_AY_VX_DOT_WEG , 
            	  											          ay_vx_dot_sig_1st_th, U8_ROP_AY_VX_DOT_1ST_WEG ,						                      
            			                					    ay_vx_dot_sig_2nd_th, U8_ROP_AY_VX_DOT_2ND_WEG  ) ;	
            			                					              			                					     
            }
            else 
            {     
            	  esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, U8_ROP_AY_VX_DOT_WEG, 
            	  												        ay_vx_dot_sig_1st_th, U8_ROP_AY_VX_DOT_1ST_WEG,						                      
            			                						  ay_vx_dot_sig_2nd_th, U8_ROP_AY_VX_DOT_2ND_WEG ) ;		
            } 
            
    esp_tempW5 = (int16_t)( (((int32_t)esp_tempW2)*esp_tempW5)/100 ) ; 					  
             
  #endif    
    
    /* '11.02.09 : ROP in ESC-OFF by Switch  */
    
#if __ESC_SW_OFF_ROP_ACT_ENABLE    

    if (fu1ESCDisabledBySW==1)
    {
    	esp_tempW6 = (int16_t)( (((int32_t)S16_RI_THRES_WEG_IN_ESC_OFF)*esp_tempW5)/100 ) ; 				
    } 
    else 
    {      
    	esp_tempW6 = (int16_t)( (((int32_t)S16_RI_THRES)*esp_tempW5)/100 ) ; 	
    }       			                              
         
#else
    
    esp_tempW6 = (int16_t)( (((int32_t)S16_RI_THRES)*esp_tempW5)/100 ) ; 					  
             
#endif
         
    /* Count RI_max  */
   
     if ( (ROP_REQ_ACT==0) && ( RI_max >= (esp_tempW6-10)) ) 
    {
	    esp_tempW3 = LCESP_s16IInter4Point( RI_max,  esp_tempW6-1,   0, 	
	    						            esp_tempW6,     1,
						                    esp_tempW6+10,  2,
						                    esp_tempW6+30,  4 ) ;	
    }
/*    else if ( ( ROP_REQ_ACT && ( RI_max >= (S16_RI_THRES - 20)))
         || ( YAW_CDC_WORK && !Flag_REAR_CONTROL && !Flag_UNDER_CONTROL && !ACT_UNDER_2WHEEL ))    */
    else if ( (ROP_REQ_ACT==1) && ( RI_max >= (esp_tempW6 - 20)))
    {
	    esp_tempW3 = LCESP_s16IInter5Point( RI_max,  esp_tempW6-20,  -1, 	
						                    esp_tempW6-19,  1,	 /* Only Front Outside control  */						                    
						                    esp_tempW6-10,  1, 	
						                    esp_tempW6,     1,
						                    esp_tempW6+20,  3 ) ;	
    } 
    else  
    {
        esp_tempW3 = -1 ; 
    }
   
    if ( vrefk < VREF_K_20_KPH ) 
    {
        TTL_time = (int8_t)S16_TTL_max ;
    } 
    else  
    {
        TTL_time = TTL_time - (int8_t)esp_tempW3 ;         
    }  
    
    if ( TTL_time > S16_TTL_max ) {
        TTL_time = (int8_t)S16_TTL_max ;
    } else if ( TTL_time < S16_TTL_min ) {
        TTL_time = (int8_t)S16_TTL_min ;
    } else  {
        TTL_time = TTL_time ;        
    }           
    		                            
    /* ROP disable condition. '05/9 */
    
#if defined(__H2DCL)  
  #if (__CAR==GM_TAHOE) || (__CAR==GM_C100)|| (__CAR ==GM_SILVERADO)
    if ( (ctu1ROP==1)  || (VDC_DISABLE_FLG==1) || (SAS_CHECK_OK==0) || (PARTIAL_PERFORM==1) ) {
  #else
    if ( (ctu1ROP==0)  || (VDC_DISABLE_FLG==1) || (SAS_CHECK_OK==0) || (PARTIAL_PERFORM==1) ) {
  #endif  	
#else

	#if (__ESC_SW_OFF_ROP_ACT_ENABLE == 1)  
    if (((VDC_DISABLE_FLG==1)&&(fu1ESCDisabledBySW==0)) || (SAS_CHECK_OK==0) || (PARTIAL_PERFORM==1)) {
	#else
    if ((VDC_DISABLE_FLG==1) || (SAS_CHECK_OK==0) || (PARTIAL_PERFORM==1)) {
#endif        
    			  
#endif        
    		ROP_REQ_ACT = 0 ; 
    		ROP_REQ_ENG = 0 ; 		
            moment_q_front_rop = 0 ;
            
    } else {
         
     	if ( ((ROP_REQ_ACT==0)&&(TTL_time <= S16_TTL_thres)&&(delta_yaw>o_delta_yaw_thres))            		
            || ((ROP_REQ_ACT==1) && ( TTL_time < S16_TTL_max )) )
       	{                                           /* EN ROP test : USA Hatci '06/10  */     
    		ROP_REQ_ACT = 1 ; 
    		ROP_REQ_ENG = 0 ; 				
    	} else {				
        	ROP_REQ_ACT = 0 ;        	
    	}	  
    }          			
				    
	FLAG_3WHEEL_ROP_CONTROL = 0;
								    
}

void LDROP_vCheckFadeMode(void)
{		             
    
    if ( (( alat > LAT_0G3G ) && ( wstr > WSTR_100_DEG) && ( vrefk > 500 ))
        || (( alat < -LAT_0G3G ) && ( wstr < -WSTR_100_DEG) && ( vrefk > 500 )) ) {  
            
        AY_WSTR_SAME_PHASE = 1 ;
        
    } else {
        
        AY_WSTR_SAME_PHASE = 0 ;
        
    }
    
               
    if ( ( (ROP_FADE_MODE==0) && (ROP_REQ_ACT_old==1) && (ROP_REQ_ACT==0)
              && ( McrAbs(alat) > LAT_0G4G )
              && ( ROP_FADE_cnt > 0 )              
              && ( AY_WSTR_SAME_PHASE ))          
         || ( (ROP_FADE_MODE==1) && (ROP_REQ_ACT==0)
              && ( McrAbs(alat) > LAT_0G3G )
              && ( ROP_FADE_cnt > 0 )               
              && ( AY_WSTR_SAME_PHASE==1 )) )  {
                             
		ROP_FADE_MODE = 1 ;	
        ROP_FADE_cnt = ROP_FADE_cnt - 1 ;		
		
    } else {           
        
		ROP_FADE_MODE = 0 ;	            
        ROP_FADE_cnt = L_U8_TIME_10MSLOOP_500MS ;		        
		
    }      	
	
}

void LDROP_vCheckPreROP(void)
{	
    
   
                   
    ROP_FrtPrefill_WstrDot_Ent = LCESP_s16IInter5Point( vrefk,  VREF_K_40_KPH,  S16_ROP_WSTR_DOT_ENT_40K,
					                                            VREF_K_50_KPH,  S16_ROP_WSTR_DOT_ENT_50K,    
					                                            VREF_K_60_KPH,  S16_ROP_WSTR_DOT_ENT_60K,
					                                            VREF_K_70_KPH,  S16_ROP_WSTR_DOT_ENT_70K,					                            
					                                            VREF_K_80_KPH,  S16_ROP_WSTR_DOT_ENT_80K ) ;	 
                   
    ROP_FrtPrefill_WstrDot_Ext = LCESP_s16IInter5Point( vrefk,  VREF_K_40_KPH,  S16_ROP_WSTR_DOT_EXT_40K,
					                                            VREF_K_50_KPH,  S16_ROP_WSTR_DOT_EXT_50K,    
					                                            VREF_K_60_KPH,  S16_ROP_WSTR_DOT_EXT_60K,
					                                            VREF_K_70_KPH,  S16_ROP_WSTR_DOT_EXT_70K,					                            
					                                            VREF_K_80_KPH,  S16_ROP_WSTR_DOT_EXT_80K ) ;	 
					                                            
    ROP_FrtPrefill_YawDot_Ent = LCESP_s16IInter5Point( vrefk,  VREF_K_40_KPH,   S16_ROP_YAW_DOT_ENT_40K,
					                                            VREF_K_50_KPH,  S16_ROP_YAW_DOT_ENT_50K,    
					                                            VREF_K_60_KPH,  S16_ROP_YAW_DOT_ENT_60K,
					                                            VREF_K_70_KPH,  S16_ROP_YAW_DOT_ENT_70K,					                            
					                                            VREF_K_80_KPH,  S16_ROP_YAW_DOT_ENT_80K ) ;	 
                   
    ROP_FrtPrefill_YawDot_Ext = LCESP_s16IInter5Point( vrefk,  VREF_K_40_KPH,   S16_ROP_YAW_DOT_EXT_40K,
					                                            VREF_K_50_KPH,  S16_ROP_YAW_DOT_EXT_50K,    
					                                            VREF_K_60_KPH,  S16_ROP_YAW_DOT_EXT_60K,
					                                            VREF_K_70_KPH,  S16_ROP_YAW_DOT_EXT_70K,					                            
					                                            VREF_K_80_KPH,  S16_ROP_YAW_DOT_EXT_80K ) ;	 					                                            
					                                                               
    esp_tempW1 = LCESP_s16IInter2Point( vrefk,  S16_ROP_TTP_VEL_SLOW,  S16_ROP_TTP_SLOW_VEL_DELAY, 	 
					                            S16_ROP_TTP_VEL_FAST,  S16_ROP_TTP_FAST_VEL_DELAY ) ;	      
					                            
    esp_tempW2 = LCESP_s16IInter2Point( McrAbs(wstr_dot),  S16_ROP_TTP_WSTR_DOT_SLOW, S16_ROP_TTP_SLOW_WSTR_DOT_DELAY, 	 
					                                    S16_ROP_TTP_WSTR_DOT_FAST, S16_ROP_TTP_FAST_WSTR_DOT_DELAY ) ;						                                         
    
    time_to_prefill = esp_tempW1 + esp_tempW2 ;               
                   
    alat_dot2_lf_old = alat_dot2_lf ;  
    
    /* tempW2 = alat - alatold ; tempW1 = 1000; tempW0 = 10 ;     
    s16muls16divs16();
    alat_dot2 = tempW3;      */   
    alat_dot2 = (int16_t)( (((int32_t)(alat - alatold))*1000)/10 ) ; 	 /* r: 0.001 [g/sec]   */
    
    alat_dot2_lf = LCESP_s16Lpf1Int(alat_dot2, alat_dot2_lf_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);    
    
    /*  Pre_ROP_Control : FL-Pre_wheel  */
        
    if  ( (flag_wstr_start_FL== 0) && (wstr>500) && (wstr_dot < -ROP_FrtPrefill_WstrDot_Ent) )
    {
        flag_wstr_start_FL = 1; 
    }
    else if ( ((flag_wstr_start_FL==1) && (wstr_dot < -ROP_FrtPrefill_WstrDot_Ext)) 
            || ((flag_wstr_start_FL==1) && (wstr<-1800)) )
    {            
        flag_wstr_start_FL = 1; 
    }
    else 
    {
        flag_wstr_start_FL = 0; 
    }
         
/*    if( (flag_wstr_start_FL==1) && (flag_ay_start_FL==0) 
         && ( alat>100) && (det_alatm>750) && (alat_dot2_lf < -S16_ROP_AY_DOT_TH_1st) ) */

    if( (flag_wstr_start_FL==1) && (flag_ay_start_FL==0) && (det_alatm>S16_ROP_Pre_Frt_Alatm_TH)
         && ( (( alat>100 ) && (alat_dot2_lf < -S16_ROP_AY_DOT_TH_1st)) || (( yaw_out>100 ) && (yaw_out_dot2 < -ROP_FrtPrefill_YawDot_Ent)) ) )    
    {            
        flag_ay_start_FL = 1; 
    }
    else if ( (flag_wstr_start_FL == 1) &&  (flag_ay_start_FL == 1)
                && ( (alat_dot2_lf < -S16_ROP_AY_DOT_TH_2nd)||(yaw_out_dot2 < -ROP_FrtPrefill_YawDot_Ext) ) )
    {    
        flag_ay_start_FL = 1; 
    }
    else 
    {    
        flag_ay_start_FL = 0; 
    }  
    
    if ( (ROP_REQ_FRT_Pre_FL==0) && (flag_ay_start_FL==1) && ( wstr<-S16_ROP_PRE_FRT_ENT_WSTR_TH ) && (wstr_dot < -ROP_FrtPrefill_WstrDot_Ent) )      
    {
        ttp_cnt_fl = ttp_cnt_fl + 1;
        
        if ( ttp_cnt_fl > L_U8_TIME_10MSLOOP_700MS )
        {
            ttp_cnt_fl = L_U8_TIME_10MSLOOP_700MS ; 
        }
        else
        {
           ;
        }
        
        if ( ttp_cnt_fl > time_to_prefill )
        {
            ROP_REQ_FRT_Pre_FL = 1; 
        }
        else
        {
           ;
        }
    }
    else if ( (ROP_REQ_FRT_Pre_FL==1) && (flag_ay_start_FL==1) && (wstr_dot < -ROP_FrtPrefill_WstrDot_Ext) )
    {
        ROP_REQ_FRT_Pre_FL = 1;        
        ttp_cnt_fl = 0 ;         
    }
    else
    {
        ROP_REQ_FRT_Pre_FL = 0;
        ttp_cnt_fl = 0 ;  
    }
    
    /*  Pre_ROP_Control : FR-Pre_wheel  */ 
    
    if  ( (flag_wstr_start_FR== 0) && (wstr<-500) && (wstr_dot > ROP_FrtPrefill_WstrDot_Ent) )
    {
        flag_wstr_start_FR = 1; 
    }
    else if ( ((flag_wstr_start_FR==1) && (wstr_dot > ROP_FrtPrefill_WstrDot_Ext)) 
            || ((flag_wstr_start_FR==1) && (wstr>1800)) )
    {            
        flag_wstr_start_FR = 1; 
    }
    else 
    {
        flag_wstr_start_FR = 0; 
    }
         
/*    if( (flag_wstr_start_FR==1) && (flag_ay_start_FR==0) 
         && ( alat<-100) && (det_alatm>750) && (alat_dot2_lf > S16_ROP_AY_DOT_TH_1st) ) */

    if( (flag_wstr_start_FR==1) && (flag_ay_start_FR==0) && (det_alatm>S16_ROP_Pre_Frt_Alatm_TH)
         && ( (( alat<-100 ) && (alat_dot2_lf > S16_ROP_AY_DOT_TH_1st)) || (( yaw_out<-100 ) && (yaw_out_dot2 > ROP_FrtPrefill_YawDot_Ent)) ) )    
    {   
        flag_ay_start_FR = 1; 
    }
    else if ( (flag_wstr_start_FR == 1) &&  (flag_ay_start_FR == 1) 
                && ( (alat_dot2_lf > S16_ROP_AY_DOT_TH_2nd)||(yaw_out_dot2 > ROP_FrtPrefill_YawDot_Ext) ) )   
    {    
        flag_ay_start_FR = 1; 
    }
    else 
    {    
        flag_ay_start_FR = 0; 
    }  
    
    if ( (ROP_REQ_FRT_Pre_FR==0) && (flag_ay_start_FR==1) && ( wstr>S16_ROP_PRE_FRT_ENT_WSTR_TH) && (wstr_dot > ROP_FrtPrefill_WstrDot_Ent) )
    { 
        ttp_cnt_fr = ttp_cnt_fr + 1;
        
        if ( ttp_cnt_fr > L_U8_TIME_10MSLOOP_700MS )
        {
            ttp_cnt_fr = L_U8_TIME_10MSLOOP_700MS ; 
        }
        else
        {
           ;
        }
        
        if ( ttp_cnt_fr > time_to_prefill )
        {
            ROP_REQ_FRT_Pre_FR = 1; 
        }
        else
        {
           ;
        }        
    }
    else if ( (ROP_REQ_FRT_Pre_FR==1) && (flag_ay_start_FR==1) && (wstr_dot > ROP_FrtPrefill_WstrDot_Ext) )
    {
        ROP_REQ_FRT_Pre_FR = 1;    
        ttp_cnt_fr = 0 ;                
    }
    else
    {
        ROP_REQ_FRT_Pre_FR = 0;
        ttp_cnt_fr = 0 ;           
    }    
        
    if (  (__ROP_Pre_Front_W_CRTL==1)
           && ((ROP_REQ_FRT_Pre_FL==1)||(ROP_REQ_FRT_Pre_FR==1)) 
           && (ROP_REQ_ACT==1) 
           && ( mpress < S16_ROP_PREFILL_MPRESS_TH )
           && ( ABS_fz == 0 )                      
           && (vrefk > S16_ROP_Pre_Frt_Vel_TH) 
           && ( det_alatm > S16_ROP_Pre_Frt_Alatm_TH )  )
    {
        ROP_REQ_FRT_Pre = ROP_REQ_FRT_Pre_FL|ROP_REQ_FRT_Pre_FR;
    } 
    else  
    {
        ROP_REQ_FRT_Pre_FL = 0;     
        ROP_REQ_FRT_Pre_FR = 0;     
        ROP_REQ_FRT_Pre = 0 ;        
    }
    
    /*  When the driver handles dynamically, it is needed to stabilize more. */    
    if ((ROP_DYN_STABLE_PHASE==0)&&(ROP_REQ_ACT==1)&&(ROP_REQ_FRT_Pre==1)) 
    {
        ROP_DYN_STABLE_PHASE = 1 ;
    } 
    else if ((ROP_DYN_STABLE_PHASE==1)&&(ROP_REQ_ACT==1))
    {        
        ROP_DYN_STABLE_PHASE = 1 ;        
    }    
    else  
    {
        ROP_DYN_STABLE_PHASE = 0 ;     
    }
        
    if ( ROP_REQ_FRT_Pre==1 ) 
    {
        
        if ( wstr_dot >= 10000 )
        {
            esp_tempW3 = 10000; 
        }
        else if ( wstr_dot <= -10000 )
        {
            esp_tempW3 = -10000;         
        }
        else
        {
           esp_tempW3 = wstr_dot ;
        }
        
        if ( yaw_out_dot2 >= 4000 )
        {
            esp_tempW4 = 4000; 
        }
        else if ( yaw_out_dot2 <= -4000 )
        {
            esp_tempW4 = -4000;         
        }
        else
        {
            esp_tempW4 = yaw_out_dot2 ;
        }        
        
        /* tempW2 = esp_tempW3;  tempW1 = -1;  tempW0 = 4;
        s16muls16divs16();
        esp_tempW3 = tempW3;  */
        esp_tempW3 = (int16_t)( (((int32_t)(esp_tempW3))*(-1))/4 ) ; 
                
        moment_pre_front_rop = esp_tempW3 - esp_tempW4 ;
                    
        esp_tempW5 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_PREFILL_MQ_WEG_VEL_100K ) ;	
    					                                                
        /* tempW2 = moment_pre_front_rop;  tempW1 = esp_tempW5;  tempW0 = 100;
        s16muls16divs16();
        moment_pre_front_rop = tempW3;      */
        moment_pre_front_rop = (int16_t)( (((int32_t)moment_pre_front_rop)*esp_tempW5)/100 ) ; 
        
        if ( moment_pre_front_rop >= 3000 )
        {
            moment_pre_front_rop = 3000; 
        }
        else if ( moment_pre_front_rop <= -3000 )
        {
            moment_pre_front_rop = -3000;         
        }
        else
        {
             ;
        }        
             
    } 
    else  
    {
        moment_pre_front_rop = 0;       
    }    
}    

void LDROP_vCheck_Ay_1st_Cycle(void)
{	
    
    /*  Ay_1st_Cycle for Postive Sign */
        
    if  ( (flag_1st_cycle_POS == 0) && (flag_ay_sign_POS_old == 0) 
          && (alat>100) && (alat_dot2_lf > 100) )
    {
        flag_1st_cycle_POS = 1; 
        flag_ay_sign_POS_old = 1;         
    }
    else if ( (flag_1st_cycle_POS==1) && (alat > 100)
              && (alat_dot2_lf > 0) )
    {            
        flag_1st_cycle_POS = 1; 
        
          if (alat > 100)
          {
            flag_ay_sign_POS_old = 1 ;
          }
          else
          {
            flag_ay_sign_POS_old = 0 ;    
          }
        
    }
    else 
    {
        flag_1st_cycle_POS = 0; 
        
          if (alat > 100)
          {
            flag_ay_sign_POS_old = 1 ;
          }
          else
          {
            flag_ay_sign_POS_old = 0 ;    
          }        
        
    }
          
    /*  Ay_1st_Cycle for Negative Sign */
        
    if  ( (flag_1st_cycle_NEG == 0) && (flag_ay_sign_NEG_old == 0) 
          && (alat< -100) && (alat_dot2_lf < -100) )
    {
        flag_1st_cycle_NEG = 1; 
        flag_ay_sign_NEG_old = 1;         
    }
    else if ( (flag_1st_cycle_NEG==1) && (alat < -100)
              && (alat_dot2_lf < 0) )
    {            
        flag_1st_cycle_NEG = 1; 
        
          if (alat < -100)
          {
            flag_ay_sign_NEG_old = 1 ;
          }
          else
          {
            flag_ay_sign_NEG_old = 0 ;    
          }
        
    }
    else 
    {
        flag_1st_cycle_NEG = 0; 
        
          if (alat < -100)
          {
            flag_ay_sign_NEG_old = 1 ;
          }
          else
          {
            flag_ay_sign_NEG_old = 0 ;    
          }        
        
    } 
  
    if ( (flag_1st_cycle_NEG==1)  
            || (flag_1st_cycle_POS==1) )
    {            
        Flg_1st_cycle_Ay = 1; 
    }
    else 
    {
        Flg_1st_cycle_Ay = 0; 
    }
               
} 
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDROPCallDetection
	#include "Mdyn_autosar.h"
#endif
