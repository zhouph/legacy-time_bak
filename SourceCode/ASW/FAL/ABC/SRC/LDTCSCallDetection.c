
/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDTCSCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCallMain.h"
#include "LCTCSCallControl.h"
#include "LDTCSCallDetection.h"
#include "LDBTCSCallDetection.h"
#include "LCRDCMCallControl.h"
#include "LCHSACallControl.h"
#include "hm_logic_var.h"
#include "LCTCSCallBrakeControl.h"
#include "LCESPCalLpf.h"

#if __HDC
#include "LCHDCCallControl.h"
#include "LCDECCallControl.h"
#endif
#if __SCC
#include "SCC/LCSCCCallControl.h"
#include "SCC/LCWPCCallControl.h"
#endif
#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#include "LDESPEstDiscTemp.h"
#include "LDRTACallDetection.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"

#if (__TCS_MU_DETECTION == ENABLE)
#include "LCESPCalInterpolation.h"
#include "LSABSCallSensorSignal.h"

int16_t lctcss16ALongNorm;
int16_t lctcs161stEstMu;  
int16_t ldtcs16ALongFilt;
int16_t lctcs161stEstMuWtAlat;
uint8_t TCS_MU_CONT_FLAG; 
int16_t TCS_MU_CONT_CMD;

#endif /*__TCS_MU_DETECTION*/

#if __BTC
void LDTCS_vCallDetection(void);
void LDTCS_vEstimateBTCSDiskTemp(void);

#if (__BTEM == DISABLE)
#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
	void LDTCS_vEstimateBTCSDiskTemp4WD(struct W_STRUCT *WL_temp);
	void LDTCS_vCalBTCSDiskTemp4WD(void);
	void LDTCS_vEndBTCTEMP4WD(void);
#endif

#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==0)
	void LDTCS_vEstimateBTCSDiskTemp2WD(struct W_STRUCT *WL_temp);
	void LDTCS_vCalBTCSDiskTemp2WD(void);
	void LDTCS_vEndBTCTEMP2WD(void);
#endif
#endif

#if (__TCS_MU_DETECTION == ENABLE)

static void LDTCS_vCall1stMuDetection(void);
static void LDTCS_vAx1stDropContFlag(void);
static void LDTCS_vMuSetbyAx(void);
static void LDTCS_vEstMuSetWithAccr(void);

#endif  /* #if __TCS_MU_DETECTION */


#if __SCC																
	/*void LDTCS_vConfirmSCCBrkFlag(void);*/
	uint8_t lcu1SccBrkDiskRise, lcu1SccBrkDiskRise_k;
/************* SCC tuning parameter ******************/
	#define SCC_ACC_PERC_1st 50
	#define SCC_ACC_PERC_2nd 80
	#define SCC_ACC_PERC_3rd 100
/*****************************************************/
#endif

#if __TVBB                                                              
    void LDTCS_vConfirmTVBBFlag(void);
    uint8_t lcu1TVBBDiskRise, lcu1TVBBDiskRise_k;
/************* TVBB tuning parameter ******************/
    #define WEG_TVBB_TMP_ACC  30  
    #define TVBB_ACC_PERC_1st 80
    #define TVBB_ACC_PERC_2nd 100
    #define TVBB_ACC_PERC_3rd 130
/*****************************************************/
#endif

#endif /* __BTC */

#if __TCS
void    MINI_TCS(void);
  #if   __SPARE_WHEEL
void    NON_DRIVEN_MINI_DETECT(struct W_STRUCT *pD1, struct W_STRUCT *pND1, struct W_STRUCT *pND2);
void    MINI_NON_DRIVEN_WHEEL_CLEAR(void);
  #endif
#endif

#if __TCS_BUMP_DETECTION
static void LDTCS_vDetectBump(void);
#endif	/* #if __TCS_BUMP_DETECTION */

#if __GM_ENGINE_OFF_TIME_APPLICATION
#if (__BTEM == ENABLE)
void LDTCS_vDetectEngineOFF(struct WL_TEMPT *TEMPT);
#else
void LDTCS_vDetectEngineOFF(struct W_STRUCT *WL_temp);
#endif
#endif

#if ENGINE_STALL_DETECION_REFINE
static void LDTCS_vDctHillByAxSensor(void);
static void	LDTCS_vCalStallDctRPMthreshold(void);
static void LDTCS_vCalStallRiskIndex(void);
static void LDTCS_vCalStallDctRPM(void);
#endif	/* #if ENGINE_STALL_DETECION_REFINE */    

#if __BTC   /* TAG1 */
void LDTCS_vCallDetection(void)
{
  #if __TCS_BUMP_DETECTION	
	LDTCS_vDetectBump();
  #endif	/* #if __TCS_BUMP_DETECTION */	

  #if ENGINE_STALL_DETECION_REFINE
  	LDTCS_vDctHillByAxSensor();  
  	LDTCS_vCalStallDctRPMthreshold();
  #endif	/* #if ENGINE_STALL_DETECION_REFINE */    
  
  #if	defined(BTCS_TCMF_CONTROL) /* BTCS_TCMF_CONTROL */
    LDTCS_vCallTCMFDetection();        
  #endif         /* BTCS_TCMF_CONTROL */
  
	#if (__TCS_MU_DETECTION == ENABLE)
	LDTCS_vCall1stMuDetection();
	#endif /* __TCS_MU_DETECTION*/

}

#if __TCS_BUMP_DETECTION
static void LDTCS_vDetectBump(void)
{
	if ( (ETCS_ON==1) || (FTCS_ON==1) )
	{
	
	#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
	   	tcs_tempW0 = LCTCS_s16IFindMinimum(arad_rl,arad_rr);
	   	tcs_tempW1 = LCTCS_s16IFindMinimum(arad_fl,arad_fr);
	   	
	   	ltcs16BumpAradRear = LCTCS_s16IFindMinimum(ltcs16BumpAradRear,tcs_tempW0);
	   	ltcs16BumpAradFront = LCTCS_s16IFindMinimum(ltcs16BumpAradFront,tcs_tempW1);
	   	tcs_tempW2 = LCTCS_s16IFindMinimum(ltcs16BumpAradRear,ltcs16BumpAradFront);
		
	#else
		#if __REAR_D
		tcs_tempW2 = LCTCS_s16IFindMinimum(arad_rl,arad_rr);
		#else
		tcs_tempW2 = LCTCS_s16IFindMinimum(arad_fl,arad_fr);
		#endif
	#endif

		ltcs16BumpArad = LCTCS_s16IFindMinimum(ltcs16BumpArad,tcs_tempW2);
		
		if 			(ltcss16BumpCounter<(int16_t)U8_TCS_BUMP_DETECT_TIME)
		{
			ltcss16BumpCounter++;						
	#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
			tcs_tempW3 = vrad_crt_rl - wvref_rl;
    		tcs_tempW4 = vrad_crt_rr - wvref_rr;
    		tcs_tempW5 =LCTCS_s16IFindMaximum(tcs_tempW3, tcs_tempW4);
    		
    		tcs_tempW6 = vrad_crt_fl - wvref_fl;
    		tcs_tempW7 = vrad_crt_fr - wvref_fr;
    		tcs_tempW8 =LCTCS_s16IFindMaximum(tcs_tempW6, tcs_tempW7);
	#else	  
		  #if __REAR_D
			tcs_tempW5 = vrad_crt_rl - wvref_rl;
    		tcs_tempW8 = vrad_crt_rr - wvref_rr;
  		  #else
    		tcs_tempW5 = vrad_crt_fl - wvref_fl;
    		tcs_tempW8 = vrad_crt_fr - wvref_fr;
  		  #endif
  		  #endif
  		  	if 		(ltcsu16SumOfSpin > (uint16_t)60000)
  		  	{	/* Bump Detection inhibit condition1  ltcsu16SumOfSpin Overflow  */
  		  		ltcsu1StopBumpDetection = 1;
  		  	}
  		  	else if (ESP_TCS_ON==1)
			{	/* Bump Detection inhibit condition2   ESC Engine control */
				ltcsu1StopBumpDetection = 1;
	  		}  		
			else
			{
				ltcsu16SumOfSpin = ltcsu16SumOfSpin + (uint16_t)LCTCS_s16IFindMaximum(tcs_tempW5, tcs_tempW8);
			}
		}
		else if 	(ltcss16BumpCounter==(int16_t)U8_TCS_BUMP_DETECT_TIME)
		{
			ltcss16BumpCounter++;
			tcs_tempW9 = LCTCS_s16IFindMaximum( (int16_t)U8_TCS_BUMP_DETECT_TIME , 1 );
			ltcsu16AvgOfSpin = ltcsu16SumOfSpin / (uint16_t)tcs_tempW9;

			/*  Detection Condition */
			if ( (ltcsu16AvgOfSpin > 0)
				 &&(ltcsu16AvgOfSpin <= U16_TCS_BUMP_DETECT_SPIN)
				 &&(ltcsu1StopBumpDetection==0)
				 &&(ltcs16BumpArad <= S16_TCS_BUMP_DETECT_ARAD) ) /*-15g*/
			{
				ltcsu1BumpDetect = 1;
			
			#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)				
				if(ltcs16BumpAradRear < ltcs16BumpAradFront)
				{
					ldtcs8BumpWheelIndexByArad = 1;	/*rear*/
				}
				else
				{
					ldtcs8BumpWheelIndexByArad = 2;	/*front*/
				}
			#endif

			}
			else
			{
				ltcsu1BumpDetect = 0;
			}
		}
		else
		{
			/* Bump Detection exit condition */
			if ( (ltcsu1StopBumpDetection==1) || 
				 ( (lts16WheelSpinError > 0) && (lts16WheelSpinErrorDiff > 0) ) )
    		{
    			
    		#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
    			if(ltcsu1BumpDetect == 1)
    			{
					tcs_tempW10 = (int16_t)(lts16FrontWheelSpinAverage - lts16TargetWheelSpin);
					tcs_tempW11 = (int16_t)(lts16RearWheelSpinAverage - lts16TargetWheelSpin);
										
					if (  ((ldtcs8BumpWheelIndexByArad==2)&&(tcs_tempW10 > 0))	 /*front bump reset*/
						||((ldtcs8BumpWheelIndexByArad==1)&&(tcs_tempW11 > 0)) ) /*rear bump reset*/
					{
						ltcsu1BumpDetect = 0;
					}
						
    			}
    			else
    			{
    			ltcsu1BumpDetect = 0;
    		}
    		#else
    			ltcsu1BumpDetect = 0;
    		#endif
    		    		
    		}
    		else
    		{
    			;
    		}
		}
	}
	else
	{
		ltcs16BumpArad=0;
		ltcss16BumpCounter = 0;
		ltcsu16SumOfSpin = 0;
		ltcsu1StopBumpDetection = 0;
		ltcsu1BumpDetect = 0;
		ltcsu16AvgOfSpin = 0;
		#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
		ltcs16BumpAradFront = 0;
		ltcs16BumpAradRear = 0;
		ldtcs8BumpWheelIndexByArad = 0;
		#endif
	}		
}
#endif	/* #if __TCS_BUMP_DETECTION */

#if ENGINE_STALL_DETECION_REFINE
static void LDTCS_vDctHillByAxSensor(void)
{
  #if __HSA	
	/* HSA flag*/
	if ((HSA_flg==1)&& (McrAbs(HSA_ax_sen_filt)>=U8TCSHillDctAlongTh)) /*STOP_ON_HILL_flag 신호가 민감작동하여 교체*/
	{
		ldtcss16HillDetectSustainCnt = T_5_S;
		ldtcsu1HillDetectionByAx = 1;
	}
	else
	{
		if (ldtcss16HillDetectSustainCnt>0)
		{
			ldtcss16HillDetectSustainCnt = ldtcss16HillDetectSustainCnt - 1;
		}
		else
		{
			ldtcss16HillDetectSustainCnt = 0;
			ldtcsu1HillDetectionByAx = 0;
		}
	}
  #else		/* #if __HSA */
  	/* HSA*/
  	ldtcsu1HillDetectionByAx = 0;
  #endif	/* #if __HSA */
}

static void	LDTCS_vCalStallDctRPMthreshold(void)
{
	LDTCS_vCalStallRiskIndex();
	LDTCS_vCalStallDctRPM();
}

static void LDTCS_vCalStallRiskIndex(void)
{
    if((ETCS_ON==1)||(FTCS_ON==1)||(BTCS_ON==1)
	#if __RDCM_PROTECTION
    ||(ltu1RDCMProtENGOn==1)
    #endif    	
    )    	
	{
		if (ENG_STALL==0)
		{
			/* Depends on vehicle speed : low speed ==> high risk to stall */
			tcs_tempW0 = LCESP_s16IInter3Point(vref5, VREF_6_KPH, 2, VREF_10_KPH, 1, 81/*10.125KPH*/, 0);
			
			/* Depends on RPM decrease rate : fast Rpm decrease ==> high risk to stall */
			/* risk index : -20rpm/scan -> 2, -15rpm/scan -> 1, -10rpm/scan -> 0, -5rpm/scan -> -2 */
			tcs_tempW1 = LCESP_s16IInter4Point(slope_eng_rpm, -20, 2, -15, 1, -10, 0, -5, -2);
			
			/* Depends on road friction : very slippery flat road ==> low risk to stall */
			/* Ice 2단 출발시 stall 감지 chattering 문제 개선을 위하여 very low mu가 감지되면 stall risk index를 -2로 설정하여 Stall 감지 둔감화 필요 */
			/* Very low mu 감지 필요 */
			
			/* Stall Risk Index */
			ldtcss16StallRiskIndex = LCTCS_s16ILimitMinimum((tcs_tempW0 + tcs_tempW1), 0);
		}
		else
		{	/* Stall 감지 중에는 stall 해제 threshold의 가변을 방지하기 위하여 lctcss16StallRiskIndex의 update 하지 않음 */
			;
		}
	}
	else
	{
		ldtcss16StallRiskIndex = 0;
	}
}

static void LDTCS_vCalStallDctRPM(void)
{
	if (lctcsu1DctSplitHillFlg==1)
	{	/* Hill detected by Ax sensor */
		ldtcss16StallDctRPM = LCESP_s16IInter3Point(ldtcss16StallRiskIndex, STALL_LOW_RISK, S16_LOW_RISK_STALL_RPM_HILL, 
																			STALL_MID_RISK, S16_MID_RISK_STALL_RPM_HILL,
																			STALL_HIGH_RISK,S16_HIGH_RISK_STALL_RPM_HILL);
	}
	else
	{	/* Flat */
		ldtcss16StallDctRPM = LCESP_s16IInter3Point(ldtcss16StallRiskIndex, STALL_LOW_RISK, S16_LOW_RISK_STALL_RPM_FLAT, 
																			STALL_MID_RISK, S16_MID_RISK_STALL_RPM_FLAT,
																			STALL_HIGH_RISK,S16_HIGH_RISK_STALL_RPM_FLAT);
	}
}
#endif	/* #if ENGINE_STALL_DETECION_REFINE */    

void LDTCS_vEstimateBTCSDiskTemp(void)
{
#if __GM_ENGINE_OFF_TIME_APPLICATION
if((lespu1_EngOffTmExtRngV == 1)&&(GM_ENGINE_OFF_CONFIRM_FLAG ==0)&&(cc_u8EngineStatus == 1))
{
	#if (__BTEM == ENABLE)
		LDTCS_vDetectEngineOFF(&FL_TEMPT);
		LDTCS_vDetectEngineOFF(&FR_TEMPT);
		LDTCS_vDetectEngineOFF(&RL_TEMPT);
		LDTCS_vDetectEngineOFF(&RR_TEMPT);
 	 GM_ENGINE_OFF_CONFIRM_FLAG = 1;	        	              
	#else
		LDTCS_vDetectEngineOFF(&FL);
		LDTCS_vDetectEngineOFF(&FR);
		LDTCS_vDetectEngineOFF(&RL);
		LDTCS_vDetectEngineOFF(&RR);
 	 GM_ENGINE_OFF_CONFIRM_FLAG = 1;	        	              
	#endif	
}
else
{
	;
}
#endif	

/*#if __SCC
		LDTCS_vConfirmSCCBrkFlag(); 
#endif
*/
#if __TVBB
    LDTCS_vConfirmTVBBFlag(); 
#endif 


#if (__BTEM == DISABLE)


#if __4WD_VARIANT_CODE==ENABLE

	  #if __HDC
		/* 4WD MODE */
		LDTCS_vEstimateBTCSDiskTemp4WD(&FL);
		LDTCS_vEstimateBTCSDiskTemp4WD(&FR);
		LDTCS_vEstimateBTCSDiskTemp4WD(&RL);
		LDTCS_vEstimateBTCSDiskTemp4WD(&RR);
	  #else
	  if(lsu8DrvMode == DM_2WD)
	  {
	   /* 2WD MODE */
	   #if !__REAR_D
	    LDTCS_vEstimateBTCSDiskTemp2WD(&FL);
	    LDTCS_vEstimateBTCSDiskTemp2WD(&FR);
	   #else
	    LDTCS_vEstimateBTCSDiskTemp2WD(&RL);
	    LDTCS_vEstimateBTCSDiskTemp2WD(&RR);
	   #endif
	  }
	  else
	  {
	    /* 4WD MODE */
	    LDTCS_vEstimateBTCSDiskTemp4WD(&FL);
	    LDTCS_vEstimateBTCSDiskTemp4WD(&FR);
	    LDTCS_vEstimateBTCSDiskTemp4WD(&RL);
	    LDTCS_vEstimateBTCSDiskTemp4WD(&RR);
	  }
	  #endif

	#else
  	#if __4WD
  	   LDTCS_vEstimateBTCSDiskTemp4WD(&FL);
  	   LDTCS_vEstimateBTCSDiskTemp4WD(&FR);
  	   LDTCS_vEstimateBTCSDiskTemp4WD(&RL);
  	   LDTCS_vEstimateBTCSDiskTemp4WD(&RR);
  	#else
  	  #if !__REAR_D
  	    LDTCS_vEstimateBTCSDiskTemp2WD(&FL);
  	    LDTCS_vEstimateBTCSDiskTemp2WD(&FR);
  	  #else
  	    LDTCS_vEstimateBTCSDiskTemp2WD(&RL);
  	    LDTCS_vEstimateBTCSDiskTemp2WD(&RR);
  	  #endif
  	#endif
  #endif
#endif /* #if (__BTEM == DISABLE) */
}
/*
#if __SCC
void LDTCS_vConfirmSCCBrkFlag(void) 
{
	  if(lcu1SccBrkCtrlReq==1)
    {
        lcu1SccBrkDiskRise_k=0;
        if(lcu1SccBrkDiskRise==0)
        {
        	lcu1SccBrkDiskRise=1;
          lcu1SccBrkDiskRise_k=1;
        }
        else
        {
            ;
        }
    }
    else
    {
	   	lcu1SccBrkDiskRise=0;
      lcu1SccBrkDiskRise_k=0;
    }
}
#endif
*/
#if __TVBB
void LDTCS_vConfirmTVBBFlag(void) 
{
    if(lctvbbu1TVBB_ON==1)
    {
        lcu1TVBBDiskRise_k=0;
    
        if(lcu1TVBBDiskRise==0)
        {
            lcu1TVBBDiskRise=1;
            lcu1TVBBDiskRise_k=1;
        }
        else
        {
            ;
        }
    }
    else
    {
        lcu1TVBBDiskRise=0;
        lcu1TVBBDiskRise_k=0;
    }
}
#endif

#if (__BTEM == DISABLE)
#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
void LDTCS_vEstimateBTCSDiskTemp4WD(struct W_STRUCT *WL_temp)
{
	WL = WL_temp;
  tempF0 = 0;
  tempF1 = 0;
  tempflag = 0;

	tempW7 = LCTCS_s16ILimitRange( K_Brake_temperature_Overheated_Threshold/*125*/, -10, 245 );		/* -40Deg C ~ 980Deg C */
	tempW7 = tempW7 * 128;	/* resolution 1/32, 2^7=128*/ 
	tempW8 = LCTCS_s16ILimitRange( K_Brake_temperature_Overheated_Threshold_Low/*100*/, -10, 245 );	/* -40Deg C ~ 980Deg C */
	tempW8 = tempW8 * 128;	/* resolution 1/32 */	
  
  if(WL->btc_tmp > tempW7)
  {
  	TMP_ERROR_wl = 1;
  	/*tc_error_flg = 1;*/	
    if(WL->btc_tmp > TMP_980) WL->btc_tmp = (int16_t)TMP_980; 
	}
  else if(WL->btc_tmp < (int16_t)TMP_20)
  {
  	WL->btc_tmp = (int16_t)TMP_20;
	}
  else if(WL->btc_tmp < tempW8)
  {
  	TMP_ERROR_wl = 0;
  }
  else
  {
		;
  }

#if __HDC
/* CBS/ABS heating */
	if(fu1BLSErrDet == 0)
	{
	  if(lcu1HdcActiveFlg==1) /* HDC Activation */
    {
  	  TMP_RISE_wl = 1;
      tempF0 = 1;
    }
      #if __TVBB
        else if(lctvbbu1TVBB_ON == 1)
        {
            tempF0 = 1;
        
            if (ldtvbbu1FrontWhlCtl == 1)
            {
                if (ldtvbbu1LeftWhlCtl == 1)
                {
                    TMP_RISE_fl = 1;
                }
                else
                {
                    TMP_RISE_fr = 1;
                }
            }
            else
            {
                if (ldtvbbu1LeftWhlCtl == 1)
                {
                    TMP_RISE_rl = 1;
                }
                else
                {
                    TMP_RISE_rr = 1;
                }
            }
        }
      #endif
    else if(BLS == 1)
    {
	    TMP_RISE_wl = 1;
      tempF0 = 1;
    }
    else
    {
  	  ;
    }

   	if(lcu8HdcActiveFlg_K==1)
		{
    	WL->btc_tmp_alt = WL->btc_tmp;
     	WL->tmprad_alt = vref;
     	WL->tmp_time = 0;
		}
        #if __TVBB
        else if (lcu1TVBBDiskRise_k == 1)
        {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
        #endif
		else if((lcu1HdcActiveFlg==0)&&(BLS_K == 1))
		{
     	WL->btc_tmp_alt = WL->btc_tmp;
       WL->tmprad_alt = vref;
       WL->tmp_time = 0;
		}
     else
     {
     	;
     }
	}
	else
	{
	   if((ABS_fz==1)||(EBD_RA==1)||(lcu1HdcActiveFlg==1)) /* HDC Activation */ 
     {
   	  TMP_RISE_wl = 1;
      tempF0 = 1;
     }
     #if __TVBB
        else if(lctvbbu1TVBB_ON == 1)
        {
            tempF0 = 1;
            
            if (ldtvbbu1FrontWhlCtl == 1)
            {
                if (ldtvbbu1LeftWhlCtl == 1)
                {
                    TMP_RISE_fl = 1;
                }
                else
                {
                    TMP_RISE_fr = 1;
                }
            }
            else
            {
                if (ldtvbbu1LeftWhlCtl == 1)
                {
                    TMP_RISE_rl = 1;
                }
                else
                {
                    TMP_RISE_rr = 1;
                }
            }
        }
      #endif     
     else
     {
     	;
     }
        if((TMP_RISE_wl == 0) && (BTC_fz == 0) && (lcu1HdcActiveFlg==0)
        #if __TVBB
        && (lctvbbu1TVBB_ON == 0)
        #endif
        )
    {
    	WL->btc_tmp_alt = WL->btc_tmp;
      WL->tmprad_alt = vref;
      WL->tmp_time = 0;
    }
    else
    {
      ;
    }
	}
 

#else
	if(fu1BLSErrDet == 0)
	{
    	if(BLS == 1)
      {
      	TMP_RISE_wl = 1;
        tempF0 = 1;
			}
      else
      {
            ;
      }
        if(BLS_K == 1)
        {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
		  if((ABS_fz==1)||(EBD_RA==1))
      {
    	  TMP_RISE_wl = 1;
        tempF0 = 1;
      }
      else
      {
      	;
      }
			  if((TMP_RISE_wl == 0) && (BTC_fz == 0))
        {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
        else
        {
            ;
        }
		}
#endif
 
  if(vref <= (int16_t)18) 
  {
  	if(WL->inc_count1 > L_U8_TIME_10MSLOOP_1120MS)
    {
    	TMP_RISE_wl = 0;
    }
    else
    {
    	WL->inc_count1++;
    }
  }
  else
  {
      WL->inc_count1 = 0;
  }

  if(TMP_RISE_wl == 1)
  {
  	if(tempF0 == 0)
    {
    	if(WL->inc_count2 > L_U8_TIME_10MSLOOP_1120MS)
      {
      	TMP_RISE_wl = 0;
      }
      	else
      {
      	WL->inc_count2++;
      }
	}
    else
    {
    	;
    }
  }
  else
  {
  	WL->inc_count2 = 0;
  }

  if(TMP_RISE_wl == 1)
  {
      if(WL->tmp_time == L_U8_TIME_10MSLOOP_980MS) 
      {
          WL->tmp_time = 0;
          WL->btc_tmp_alt = WL->btc_tmp;
          tempF1 = 1;
      }
      else
      {
          WL->tmp_time++;
      }
  }
  else
  {
      WL->tmp_time = 0;
  }

	if(tempF1 == 1)
  {
#if __HDC
  	if( lcu1HdcActiveFlg==1 )
    {
	   	esp_tempW1 = LCESP_s16IInter4Point(vref, VREF_7_KPH ,  WEG_THETA_G_2_TMP_ACC_7KPH,
                                               VREF_12_KPH,  WEG_THETA_G_2_TMP_ACC_12KPH,
                                               VREF_18_KPH,  WEG_THETA_G_2_TMP_ACC_18KPH,
                                               VREF_27_KPH,  WEG_THETA_G_2_TMP_ACC_27KPH  );
                                                        
      WL->tmp_acc = (int16_t)((((int32_t)(McrAbs(lcs16HdcThetaG)))*esp_tempW1)/100);
    }
    #if __TVBB
    else if (lctvbbu1TVBB_ON == 1)
    {
        WL->tmp_acc = WEG_TVBB_TMP_ACC;
    }
    #endif
    else
    {
	    if(WL->tmprad_alt > vref)
  	  {
    	  tempW0 = WL->tmprad_alt - vref;
 			}
			else
		  {
		  	tempflag = 1;
		  }
			if(tempflag == 0){
     		tempW0 = (int16_t)((uint16_t)tempW0<<4);
                                         /* temp_acc*16 */
     		WL->tmp_acc = tempW0/(int16_t)29;
    
	      WL->tmp_acc = (int16_t)((((int32_t)WL->tmp_acc)*(int16_t)S16_WEG_TMP_ACC_4WD)/100);
			}
			else
			{
				;
			}
		}
		
			if(tempflag == 0){
		    if(WL->tmp_acc < 1)
		    {
		    	WL->tmp_acc = 1;
		    }
		    else
		    {
		    	;
		    }
		                                             /* % calculate*100 */
		    if(ABS_fz == 1)
		    {
		    	if(WL->tmp_acc >= (int16_t)78)
		    	{
		      	WL->acc_perc = (int16_t)80;	 /* 64; */
		      }
		      else
		      {
		      	WL->acc_perc = (int16_t)73;	 /* 59;*/
		      }
		    }
		    else
		    {
					if( lcu1HdcActiveFlg==1 )
			    {
				  	esp_tempW7 = WL->tmp_acc ;
			      esp_tempW8 = LCESP_s16IInter3Point( esp_tempW7, 50, ACC_PERC_1st,
			  		                                                80, ACC_PERC_2nd  ,
			                                                     300, ACC_PERC_3rd   ) ;
			
			      WL->acc_perc = esp_tempW8;
					}
                    #if __TVBB
                else if( lctvbbu1TVBB_ON==1 )
                {
                    esp_tempW7 = vrefk ;
                    esp_tempW8 = LCESP_s16IInter3Point( esp_tempW7,300, TVBB_ACC_PERC_1st,
                                                                   500, TVBB_ACC_PERC_2nd,
                                                                   800, TVBB_ACC_PERC_3rd);
                    WL->acc_perc = esp_tempW8;
                }
              #endif
			    else
			    {
			    	if(WL->tmp_acc >= (int16_t)78)
			      {
			      	WL->acc_perc = (int16_t)128; /* 85;*/
			      }
			      else
			      {
			      	WL->acc_perc = (int16_t)117; /* 78;*/
			      }
			    }
				}
						                                             /* ts calculate*/
				tempW1 = (int16_t)((uint16_t)WL->tmprad_alt<<4);
				tempW2 = WL->tmp_acc*29;
				WL->out_time = (int16_t)((uint16_t)(tempW1/tempW2));
				WL->out_time += 1;
				                                     /* tmprad_alt(kph)*0.259*8  */
				                                     /* (1-2t/3ts)*64  */
				tempW4 = (int16_t)((int16_t)42/WL->out_time);
				tempW4 = (int16_t)64 - tempW4;
				                                     /* /100 /8192   */
				
				tempW6 = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->tmp_acc)*(uint16_t)WL->acc_perc)/(uint16_t)100));           /* 2004 Sweden*/

				tempW3 = (int16_t)((uint16_t)WL->tmprad_alt>>2);   /* 2004 Sweden*/
				
				if(tempW6 < 1)
				{
				    tempW6 = 1;
				}
				else
				{
				    ;
				}
				
				/*tempW5 = (int16_t)(((uint32_t)tempW3 * (uint16_t)tempW4)>>3); */
				tempW5 = (int16_t)(((uint32_t)tempW3 * tempW4)>>3);/*optime 0303 pbj*/

				/*tempW5 = (int16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>5);   */ /* 2004 Sweden*/
				tempW5 = (int16_t)(((uint32_t)tempW5 * tempW6)>>5);/* optime 0303 pbj */
				
				WL->btc_tmp = (int16_t)(tempW5/WL->out_time);
				esp_k5 = WL->btc_tmp;

				WL->btc_tmp += WL->btc_tmp_alt;
				WL->tmprad_alt = vref;
				tempflag = 1;				

			}
#else
    if(WL->tmprad_alt > vref)
    {
      tempW0 = WL->tmprad_alt - vref;
			}
	    else
	    {
	    	tempflag = 1;
	    }
if(tempflag == 0){
     	tempW0 = (int16_t)((uint16_t)tempW0<<4);
                                         /* temp_acc*16 */
     	WL->tmp_acc = tempW0/(int16_t)29;
    
     	WL->tmp_acc = (int16_t)((((int32_t)WL->tmp_acc)*(int16_t)S16_WEG_TMP_ACC_4WD)/100);

	    if(WL->tmp_acc < 1)
	    {
	    	WL->tmp_acc = 1;
	    }
	    else
	    {
	    	;
	    }
	                                             /* % calculate*100 */
	    if(ABS_fz == 1)
	    {
	    	if(WL->tmp_acc >= (int16_t)78)
	    	{
	      	WL->acc_perc = (int16_t)80;	 /* 64; */
	      }
	      else
	      {
	      	WL->acc_perc = (int16_t)73;	 /* 59;*/
	      }
	    }
	    else
	    {
		    	if(WL->tmp_acc >= (int16_t)78)
		      {
		      	WL->acc_perc = (int16_t)128; /* 85;*/
		      }
		      else
		      {
		      	WL->acc_perc = (int16_t)117; /* 78;*/
		      }
			}
				                                             /* ts calculate*/
			tempW1 = (int16_t)((uint16_t)WL->tmprad_alt<<4);
			tempW2 = WL->tmp_acc*29;
			WL->out_time = (int16_t)((uint16_t)(tempW1/tempW2));
			WL->out_time += 1;
			                                     /* tmprad_alt(kph)*0.259*8  */
			                                     /* (1-2t/3ts)*64  */
			tempW4 = (int16_t)((int16_t)42/WL->out_time);
			tempW4 = (int16_t)64 - tempW4;
			                                     /* /100 /8192   */
			
			tempW6 = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->tmp_acc)*(uint16_t)WL->acc_perc)/(uint16_t)100));           /* 2004 Sweden*/
			
			
			tempW3 = (int16_t)((uint16_t)WL->tmprad_alt>>2);   /* 2004 Sweden*/
			
			if(tempW6 < 1)
			{
			    tempW6 = 1;
			}
			else
			{
			    ;
			}
			
			tempW5 = (int16_t)(((uint32_t)tempW3 * (uint16_t)tempW4)>>3);

			tempW5 = (int16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>5);    /* 2004 Sweden*/
			
			WL->btc_tmp = (int16_t)(tempW5/WL->out_time);
			esp_k5 = WL->btc_tmp;
			
			WL->btc_tmp += WL->btc_tmp_alt;
			WL->tmprad_alt = vref;
			tempflag = 1;
		}
#endif
	}

/* DISC cooling*/
if(tempflag == 0){
	  if(TMP_RISE_wl == 1)
	  {
	      WL->tmp_up = 0;
	  }
	  else
	  {
	/* BTCS heating*/
			if(BTCS_wl == 1)
	    {
	      if((WL->state == BTCS_STATE1) && (WL->state1_running_cnt==1))
        {
	          WL->btc_tmp_alt = WL->btc_tmp;
	          WL->tmprad_alt = vref2;      /* WL->vrad;*/
	          WL->tmprad_max = vref2;      /* WL->vrad;*/
	          WL->tmp_count = 0;
				}else if(WL->state == BTCS_STATE1)
        {
	        WL->tmp_count++;
        }
        else if((WL->state == BTCS_STATE2) && (WL->state2_running_cnt==1))
        {
	        WL->tmprad_max = WL->tmprad_alt;
	        WL->tmprad_max += WL->spin;
		      if(WL->tmprad_max > WL->tmprad_alt)
          {
	          tempW0 = WL->tmprad_max - WL->tmprad_alt;
            WL->tmprad_max = WL->spin;
	     		}
          else
          {
	          tempflag = 1;
          }
					if(tempflag == 0)
					{
	        	LDTCS_vCalBTCSDiskTemp4WD();
	          WL->btc_tmp = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->btc_tmp)*(uint16_t)WL->tmp_count)/(uint16_t)141));      /* 2004 Sweden*/
	         
	         if(HILL_wl == 1)
	         {
		         	WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<1);
	          }
	          else
	          {
							;
	          }
		        
		        WL->btc_tmp += WL->btc_tmp_alt;
	  	      WL->tmprad_alt = vref2;
	    	    WL->max_spin = 0;
	    	    	
	    	    tempflag = 1;
		      }
		      else
		      {
		      	;
		      }
		    }
					
        else if(WL->state == BTCS_STATE2)
        {
	        tempW0 = WL->vrad - vref2;
	        if(tempW0 > WL->max_spin)
	        {
	            WL->max_spin = tempW0;
	        }
	        else
	        {
	      	  ;
	        }
	
	        if(WL->tmp_up == L_U8_TIME_10MSLOOP_980MS)
	        {
	      	  WL->tmp_up = 0;
	          WL->btc_tmp_alt = WL->btc_tmp;
	          WL->tmprad_max = vref2;
	          if(vref2 >= (int16_t)VREF_30_KPH)
	          {
	      	    if(WL->max_spin > (int16_t)VREF_20_KPH)
	            {
	        	    WL->max_spin = (int16_t)((uint16_t)WL->max_spin>>1);
	            }
	            else
	            {
	          	  ;
	            }
	          }
	          else
	          {
	          	;
	          }
	          WL->tmprad_max += (int16_t)((uint16_t)WL->max_spin>>2);
	          if(WL->tmprad_max > WL->tmprad_alt)
	          {
	          	tempW0 = WL->tmprad_max - WL->tmprad_alt;
	          }
	          else
	          {
		        	tempflag = 1;
	          }
		      if(tempflag == 0)
			  {
	          	LDTCS_vCalBTCSDiskTemp4WD();
		       	if(HILL_wl == 1)
	  	        {
	    	      	WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<1);
	      	    }
	        	  else
	          	{
								;
	            }
	            WL->btc_tmp += WL->btc_tmp_alt;
	            WL->tmprad_alt = vref2;
	            WL->max_spin = 0;
	            
	           	tempflag = 1;	
	          }
	        }
	        else
	        {
	        	WL->tmp_up++;
	        }
	      }
		  }
			else
			{
				;
			}
		}
	}
	
	if(wu8IgnStat == CE_ON_STATE)
  	LDTCS_vEndBTCTEMP4WD();
	else
	{
		;
	}	
}

void LDTCS_vEndBTCTEMP4WD(void)
{
    tempflag = 1;
    if(WL->tmp_down == L_U8_TIME_10MSLOOP_1000MS)
    {
        WL->tmp_down = 0;
        tempW0 = (int16_t)((uint16_t)WL->btc_tmp>>5);   /* 32*/
        if(tempW0 >= (int16_t)450)
        {
            tempW1 = 1;
        }
        else if(tempW0 >= (int16_t)350)
        {
            tempW1 = 2;
        }
        else if(tempW0 >= (int16_t)250)
        {
            tempW1 = 3;
        }
        else if(tempW0 >= (int16_t)170)
        {
            tempW1 = 4;
        }
        else if(tempW0 >= (int16_t)110)
        {
            tempW1 = 5;
        }
        else if(tempW0 >= (int16_t)70)
        {
            tempW1 = 6;
        }
        else if(tempW0 >= (int16_t)40)
        {
            tempW1 = 7;
        }
        else
        {
            tempW1 = 8;
        }

        if(vref2 <= (int16_t)VREF_10_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1<<2);
            tempW1 += 4;
        }
        else if(vref2 <= (int16_t)VREF_30_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1<<1);
            tempW1 += 2;
        }
        else if(vref2 <= (int16_t)VREF_60_KPH)
        {
            tempW1 += 1;
        }
        else if(vref2 <= (int16_t)VREF_90_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1>>1);
            tempW1 += 1;
        }
        else
        {
            tempW1 = (int16_t)((uint16_t)tempW1>>1);
        }
        
        tempW1 += 5;
        
        if(tempW1<1)
        	{
        		tempW1=1;
        	}
       	else
       		{
       			;
       		}
        WL->cool_tmp = tempW0/tempW1;
        
        esp_tempW5 = LCESP_s16IInter3Point( vrefk, VREF_K_30_KPH, S16_WEG_TMP_COOL_4WD_30K, 
        						                   VREF_K_60_KPH, S16_WEG_TMP_COOL_4WD_60K,	    
        						                   VREF_K_90_KPH, S16_WEG_TMP_COOL_4WD_90K  ) ;	      
        
        WL->cool_tmp = (int16_t)((((int32_t)WL->cool_tmp)*esp_tempW5)/100);  /* App.Paramter : this increase -> Temp. Quickly decrease */          
        WL->btc_tmp -= WL->cool_tmp;
    }
    else
    {
        WL->tmp_down++;
    }
}

void LDTCS_vCalBTCSDiskTemp4WD(void)
{
    tempW0 = (int16_t)((uint16_t)tempW0<<4);
                                         /* temp_acc*16  */
    WL->tmp_acc = tempW0/(int16_t)29;
    
    WL->tmp_acc = (int16_t)((((int32_t)WL->tmp_acc)*(int16_t)S16_WEG_TMP_BTCS_4WD)/100); /* Application Paramter  */
    
    if(WL->tmp_acc < 1)
    {
        WL->tmp_acc = 1;
    }
    else
    {
        ;
    }
                                         /* % calculate*100  */
    if(WL->tmp_acc >= (int16_t)78)
    {
        WL->acc_perc = (int16_t)85;
    }
    else
    {
        WL->acc_perc = (int16_t)78;
    }
                                         /* ts calculate  */
    tempW1 = (int16_t)((uint16_t)WL->tmprad_max<<4);
    tempW2 = WL->tmp_acc*29;
    WL->out_time = (int16_t)((uint16_t)(tempW1/tempW2));
    WL->out_time += 1;
                                         /* tmprad_alt(kph)*0.259*8  */
/*    tempW3 = (uint16_t)WL->tmprad_max>>2;*/
                                         /* (1-2t/3ts)*64  */
    tempW4 = (int16_t)((int16_t)42/WL->out_time);
    tempW4 = (int16_t)64 - tempW4;
                                         /* /100 /8192  */
    tempW6 = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->tmp_acc)*(uint16_t)WL->acc_perc)/(uint16_t)100));           /* 2004 Sweden*/

    tempW3 = (int16_t)((uint16_t)WL->tmprad_max>>2);   /* 2004 Sweden*/

    if(tempW6 < 1)
    {
        tempW6 = 1;
    }
    else
    {
        ;
    }
    /*tempW5 = (int16_t)((uint32_t)tempW3 * (uint16_t)tempW4);*/
    tempW5 = (int16_t)((uint32_t)tempW3 * tempW4);/*optime 0303*/
    /*tempW5 = (int16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>13); */
    tempW5 = (int16_t)(((uint32_t)tempW5 * tempW6)>>13);/*optime 0303*/

    WL->btc_tmp = (int16_t)(tempW5/WL->out_time);
    WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<4);
}
#endif  /* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) */
#endif  /* #if (__BTEM == DISABLE) */

#if (__BTEM == DISABLE)
#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==0)
void LDTCS_vEstimateBTCSDiskTemp2WD(struct W_STRUCT *WL_temp)
{
    WL = WL_temp;

    tempF0 = 0;
    tempF1 = 0;
    tempflag = 0;

    if(BTC_fz == 1)
    {
        if(WL->arad > WL->av_ref_max)
        {
            WL->av_ref_max = WL->arad;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
	tempW7 = LCTCS_s16ILimitRange( K_Brake_temperature_Overheated_Threshold, -10, 245 );		/* -40Deg C ~ 980Deg C */
	tempW7 = tempW7 * 128;	/* resolution 1/32 */
	tempW8 = LCTCS_s16ILimitRange( K_Brake_temperature_Overheated_Threshold_Low, -10, 245 );	/* -40Deg C ~ 980Deg C */
	tempW8 = tempW8 * 128;	/* resolution 1/32 */	
    if(WL->btc_tmp > tempW7)
    {
        TMP_ERROR_wl = 1;
        /*tc_error_flg = 1;*/
        if(WL->btc_tmp > (int16_t)TMP_980)
        {
            WL->btc_tmp = (int16_t)TMP_980;
        }
        else
        {
            ;
        }
    }
    else if(WL->btc_tmp < (int16_t)TMP_20)
    {
        WL->btc_tmp = (int16_t)TMP_20;
    }
    else if(WL->btc_tmp < tempW8)
    {
        TMP_ERROR_wl = 0;
    }
    else
    {
        ;
    }

/* CBS/ABS heating  */
    if(fu1BLSErrDet == 0)
    {
#if __HDC
        if(((BLS == 1)||(lcu1HdcActiveFlg==1)) /* HDC Activation */
#else
        if((BLS == 1)
#endif
#if __SCC
	 	||(lcu8WpcCtrlReq==1) /* SCC Activation */
#endif
        )
	 	{
		  TMP_RISE_wl = 1;
	 	  tempF0 = 1;
	 	}
	 	else
	 	{
		  ;
	 	}

#if __HDC
        if(((BLS_K == 1)||(lcu8HdcActiveFlg_K==1))
#else
        if((BLS_K == 1)
#endif
#if __SCC
	    ||(lcu1SccBrkDiskRise_k==1)
#endif
        )
        {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
#if __HDC
        if(((ABS_fz==1)||(EBD_RA==1)||(lcu1HdcActiveFlg==1)) /* HDC Activation */
#else
        if(((ABS_fz==1)||(EBD_RA==1))
#endif
#if __SCC
		||(lcu8WpcCtrlReq==1) /* SCC Activation */
#endif
        )
        {
            TMP_RISE_wl = 1;
            tempF0 = 1;
        }
        else
        {
            ;
        }

#if __HDC
        if(((TMP_RISE_wl == 0) && (BTC_fz == 0) && (lcu1HdcActiveFlg==0))
#else
        if(((TMP_RISE_wl == 0) && (BTC_fz == 0))
#endif
#if __SCC
		 && (lcu8WpcCtrlReq==0)
#endif
        )
        {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
        else
        {
            ;
        }
    }
	
    if(vref <= (int16_t)18)
    {
        if(WL->inc_count1 > L_U8_TIME_10MSLOOP_1120MS)
        {
            TMP_RISE_wl = 0; /*tempF0 = 0; */
        }
        else
        {
            WL->inc_count1++;
        }
    }
    else
    {
        WL->inc_count1 = 0;
    }

    if(TMP_RISE_wl == 1)
    {
        if(tempF0 == 0)
        {
            if(WL->inc_count2 > L_U8_TIME_10MSLOOP_1120MS)
            {
                TMP_RISE_wl = 0;
            }
            else
            {
                WL->inc_count2++;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        WL->inc_count2 = 0;
    }

    if(TMP_RISE_wl == 1)
    {
        if(WL->tmp_time == L_U8_TIME_10MSLOOP_980MS)
        {
            WL->tmp_time = 0;
            WL->btc_tmp_alt = WL->btc_tmp;
            tempF1 = 1;
        }
        else
        {
            WL->tmp_time++;
        }
    }
    else
    {
        WL->tmp_time = 0;
    }

    if(tempF1 == 1) 
    {
        if(WL->tmprad_alt > vref)
        {
            tempW0 = WL->tmprad_alt - vref;
        }
        else
        {
						tempflag=1;
        }
        if(tempflag == 0)
        {
        tempW0 = (int16_t)((uint16_t)tempW0<<4);
                                             /* temp_acc*16  */
        WL->tmp_acc = tempW0/(int16_t)29;
        
        WL->tmp_acc = (int16_t)((((int32_t)WL->tmp_acc)*(int16_t)S16_WEG_TMP_ACC_2WD)/100);  /* Application Paramter  */                       
        
        if(WL->tmp_acc < 1)
        {
            WL->tmp_acc = 1;
        }
        else
        {
            ;
        }
                                             /* % calculate*100 */
        if(ABS_fz == 1)
        {
            if(WL->tmp_acc >= (int16_t)78)
            {
                WL->acc_perc = (int16_t)80; /*64;  */
            }
            else
            {
                WL->acc_perc = (int16_t)73; /*59; */
            }
        }
        else
        {
#if __HDC
            if((lcu1HdcActiveFlg==1)&&(lcu1DecCtrlReqFlg ==0))
            {
                if(WL->tmp_acc >= (int16_t)78)
                {
                    WL->acc_perc = (int16_t)45; /* 85;*/
                }
                else
                {
                    WL->acc_perc = (int16_t)30; /* 78;*/
                }
            }
            else
            {
                if(WL->tmp_acc >= (int16_t)78)
                {
                    WL->acc_perc = (int16_t)128; /* 85;*/
                }
                else
                {
                    WL->acc_perc = (int16_t)117; /* 78;*/
                }
            }
 #else
            if(WL->tmp_acc >= (int16_t)78)
            {
                WL->acc_perc = (int16_t)128; /* 85;*/
            }
            else
            {
                WL->acc_perc = (int16_t)117; /* 78;*/
            }
#endif
         }
                                             /* ts calculate  */
        tempW1 = (int16_t)((uint16_t)WL->tmprad_alt<<4);
        tempW2 = WL->tmp_acc*29;
        WL->out_time = (tempW1/tempW2);
        WL->out_time += 1;
                                             /* tmprad_alt(kph)*0.259*8  */
/*        tempW3 = (uint16_t)WL->tmprad_alt>>2;  */
                                             /* (1-2t/3ts)*64  */
        tempW4 = (int16_t)((int16_t)42/WL->out_time);
        tempW4 = (int16_t)64 - tempW4;
                                             /* /100 /8192  */

		    tempW6 = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->tmp_acc)*(uint16_t)WL->acc_perc)/(uint16_t)100));           /* 2004 Sweden*/

        tempW3 = (int16_t)((uint16_t)WL->tmprad_alt>>2);   /* 2004 Sweden*/

        if(tempW6 < 1)
        {
            tempW6 = 1;
        }
        else
        {
            ;
        }
        /*tempW5 = (int16_t)((uint32_t)tempW3 * (uint16_t)tempW4);*/
        tempW5 = (int16_t)((uint32_t)tempW3 * tempW4);/*optime 0303*/
        /*tempW5 = (int16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>13);*/
        tempW5 = (int16_t)(((uint32_t)tempW5 * tempW6)>>13);/*optime 0303*/

        WL->btc_tmp = (int16_t)(tempW5/WL->out_time);
        WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<5);
        WL->btc_tmp += WL->btc_tmp_alt;

        WL->tmprad_alt = vref;

				tempflag=1;
    }
  }
    else
    {
        ;
    }

/* DISC cooling*/
	if(tempflag == 0){
    if(TMP_RISE_wl == 1)
    {
        WL->tmp_up = 0;
    }
    else
    {
/* BTCS heating*/
    	if(BTCS_wl == 1)
      {
	      if((WL->state == BTCS_STATE1) && (WL->state1_running_cnt==1))
        {
  	      WL->btc_tmp_alt = WL->btc_tmp;
          WL->tmprad_alt = vref;      /* WL->vrad;*/
          WL->tmprad_max = vref;      /* WL->vrad;*/
          WL->tmp_count = 0;
    		}        
        else if(WL->state == BTCS_STATE1)
        {
        	WL->tmp_count++;
        }
        else if((WL->state == BTCS_STATE2) && (WL->state2_running_cnt==1))
        {
	       	WL->tmprad_max = WL->tmprad_alt;
          WL->tmprad_max += WL->spin;
          if(WL->tmprad_max > WL->tmprad_alt)
          {
   	      	tempW0 = WL->tmprad_max - WL->tmprad_alt;
            WL->tmprad_max = WL->spin;
		      }
		      else
		      {
			      tempflag = 1;
	        }
					if(tempflag == 0)
					{
          	LDTCS_vCalBTCSDiskTemp2WD();

            WL->btc_tmp = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->btc_tmp)*(uint16_t)WL->tmp_count)/(uint16_t)141));      /* 2004 Sweden*/

            if(HILL_wl == 1)
            {
  	          WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<1);
            }
            else
            {
							;
						}
            WL->btc_tmp += WL->btc_tmp_alt;
            WL->tmprad_alt = vref;
            WL->max_spin = 0;
						tempflag=1;
	        }
		 }
		 else if(WL->state == BTCS_STATE2)
		 {
		    tempW0 = WL->vrad - vref;
          if(tempW0 > WL->max_spin)
          {
          	WL->max_spin = tempW0;
          }
          else
          {
          	;
          }
          if(WL->tmp_up == L_U8_TIME_10MSLOOP_980MS)
          {
          	WL->tmp_up = 0;
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_max = vref;
            if(vref >= (int16_t)VREF_30_KPH)
            {
          	 	if(WL->max_spin > (int16_t)VREF_20_KPH)
              {
            	 	WL->max_spin = (int16_t)((uint16_t)WL->max_spin>>1);
              }
              else
              {
             		;
              }
            }
            else
            {
            	;
            }
            WL->tmprad_max += (int16_t)((uint16_t)WL->max_spin>>2);
            if(WL->tmprad_max > WL->tmprad_alt)
            {
          	 	tempW0 = WL->tmprad_max - WL->tmprad_alt;
		        }
		        else
		        {
	                tempflag = 1;
		        }
		    if(tempflag == 0)
			{
            	LDTCS_vCalBTCSDiskTemp2WD();
                if(HILL_wl == 1)
                {
	               WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<1);
                }
                else
                {
			        ;
                }
                WL->btc_tmp += WL->btc_tmp_alt;
                WL->tmprad_alt = vref;
                WL->max_spin = 0;
				tempflag=1;
            }
          }
          else
        	{
	        	WL->tmp_up++;
          }
        }
        else
        {
        	;
        }
      }
      else
      {
				;
      }
	  }
  }
  
  if(wu8IgnStat == CE_ON_STATE)
		LDTCS_vEndBTCTEMP2WD();
	else
	{
		;
	}	
}

void LDTCS_vEndBTCTEMP2WD(void)
{	
    tempflag = 1;
    if(WL->tmp_down == L_U8_TIME_10MSLOOP_1000MS)
    {
        WL->tmp_down = 0;
        tempW0 = (int16_t)((uint16_t)WL->btc_tmp>>5);   /*32*/
        if(tempW0 >= (int16_t)450)
        {
            tempW1 = 1;
        }
        else if(tempW0 >= (int16_t)350)
        {
            tempW1 = 2;
        }
        else if(tempW0 >= (int16_t)250)
        {
            tempW1 = 3;
        }
        else if(tempW0 >= (int16_t)170)
        {
            tempW1 = 4;
        }
        else if(tempW0 >= (int16_t)110)
        {
            tempW1 = 5;
        }
        else if(tempW0 >= (int16_t)70)
        {
            tempW1 = 6;
        }
        else if(tempW0 >= (int16_t)40)
        {
            tempW1 = 7;
        }
        else
        {
            tempW1 = 8;
        }

        if(vref <= (int16_t)VREF_10_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1<<2);
            tempW1 += 4;
        }
        else if(vref <= (int16_t)VREF_30_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1<<1);
            tempW1 += 2;
        }
        else if(vref <= (int16_t)VREF_60_KPH)
        {
            tempW1 += 1;
        }
        else if(vref <= (int16_t)VREF_90_KPH)
        {
            tempW1 = (int16_t)((uint16_t)tempW1>>1);
            tempW1 += 1;
        }
        else
        {
            tempW1 = (int16_t)((uint16_t)tempW1>>1);
        }

        tempW1 += 5;

        WL->cool_tmp = tempW0/tempW1;
        
        esp_tempW5 = LCESP_s16IInter3Point( vrefk, VREF_K_30_KPH, S16_WEG_TMP_COOL_2WD_30K, 
        						                   VREF_K_60_KPH, S16_WEG_TMP_COOL_2WD_60K,	    
        						                   VREF_K_90_KPH, S16_WEG_TMP_COOL_2WD_90K  ) ;	      
        
        WL->cool_tmp = (int16_t)((((int32_t)WL->cool_tmp)*esp_tempW5)/100);  /* App.Paramter : Bigger then More Cooling -> Quickly decrease */    
        WL->btc_tmp -= WL->cool_tmp;                   
    }
    else
    {
        WL->tmp_down++;
    }
}


void LDTCS_vCalBTCSDiskTemp2WD(void)
{
    tempW0 = (int16_t)((uint16_t)tempW0<<4);
                                         /* temp_acc*16  */
    WL->tmp_acc = tempW0/(int16_t)29;
    
    WL->tmp_acc = (int16_t)((((int32_t)WL->tmp_acc)*(int16_t)S16_WEG_TMP_BTCS_2WD)/100);
    
    if(WL->tmp_acc < 1)
    {
        WL->tmp_acc = 1;
    }
    else
    {
        ;
    }
                                         /* % calculate*100  */
    if(WL->tmp_acc >= (int16_t)78)
    {
        WL->acc_perc = (int16_t)85;
    }
    else
    {
        WL->acc_perc = (int16_t)78;
    }
                                         /* ts calculate  */
    tempW1 = (int16_t)((uint16_t)WL->tmprad_max<<4);
    tempW2 = WL->tmp_acc*29;
    WL->out_time = (tempW1/tempW2);
    WL->out_time += 1;
                                         /* tmprad_alt(kph)*0.259*8  */
/*    tempW3 = (uint16_t)WL->tmprad_max>>2; */
                                         /* (1-2t/3ts)*64  */
    tempW4 = (int16_t)((int16_t)42/WL->out_time);
    tempW4 = (int16_t)64 - tempW4;
                                         /* /100 /8192 */
	  tempW6 = (int16_t)((uint16_t)((((uint32_t)(uint16_t)WL->tmp_acc)*(uint16_t)WL->acc_perc)/(uint16_t)100));           /* 2004 Sweden*/

    tempW3 = (int16_t)((uint16_t)WL->tmprad_max>>2);   /* 2004 Sweden*/

    if(tempW6 < 1)
    {
        tempW6 = 1;
    }
    else
    {
        ;
    }
    /*tempW5 = (int16_t)((uint32_t)tempW3 * (uint16_t)tempW4);*/
    tempW5 = (int16_t)((uint32_t)tempW3 * tempW4);/*optime 0303*/
    /*tempW5 = (int16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>13);*/
    tempW5 = (int16_t)(((uint32_t)tempW5 * tempW6)>>13);/*optime 0303*/

    WL->btc_tmp = (int16_t)(tempW5/WL->out_time);
    WL->btc_tmp = (int16_t)((uint16_t)WL->btc_tmp<<4);
}

#endif  /* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==0) */
#endif  /* #if (__BTEM == DISABLE) */

#if __GM_ENGINE_OFF_TIME_APPLICATION
#if (__BTEM == ENABLE)
void LDTCS_vDetectEngineOFF(struct WL_TEMPT *TEMPT)
{
        tempW1 = 5;
        tempW1 = (int16_t)((uint16_t)tempW1<<2);
        tempW1 += 9;
				if(lespu16_EngOffTmExtRng <= 60)
				{
					tempW0 = (int16_t)lespu16_EngOffTmExtRng * 60; /*change resolution : 4min -> 1sec, max: 31680sec */
				}
				else
				{
					tempW0 = 3600;
				}
				tempW2 = TEMPT->ldesps16Tempt/32;
        TEMPT->ldesps16cal_tmp = tempW2/tempW1;
        
        if(TEMPT->ldesps16cal_tmp < 1)
        {
        	TEMPT->ldesps16cal_tmp = 1;
        }
        else
        {
        	;
        }
        
        tempW3 = tempW2-(TEMPT->ldesps16cal_tmp*(tempW0/32));
        TEMPT->ldesps16Tempt = tempW3*32;
}

#else

void LDTCS_vDetectEngineOFF(struct W_STRUCT *WL_temp)
{
				WL = WL_temp;
        tempW1 = 5;
        tempW1 = (int16_t)((uint16_t)tempW1<<2);
        tempW1 += 9;
				if(lespu16_EngOffTmExtRng <= 60)
				{
					tempW0 = (int16_t)lespu16_EngOffTmExtRng * 60; /*change resolution : 4min -> 1sec, max: 31680sec */
				}
				else
				{
					tempW0 = 3600;
				}
		tempW2 = WL->btc_tmp/32;
        WL->cal_tmp = tempW2/tempW1;
        
        if(WL->cal_tmp < 1)
        {
        	WL->cal_tmp = 1;
        }
        else
        {
        	;
        }
        
        tempW3 = tempW2-(WL->cal_tmp*(tempW0/32));
        WL->btc_tmp = tempW3*32;
}
#endif  /* #if __BTEM */
#endif	/* #if __GM_ENGINE_OFF_TIME_APPLICATION */
#endif  /* TAG1*/

#if __TCS   /* TAG 3 */
void    MINI_TCS(void)
{
  #if   __SPARE_WHEEL
    if((ABS_ON && BLS_PIN && BTCS_ON && ETCS_ON && FTCS_ON)==0) {
        if((vel_r>=(int16_t)VREF_10_KPH) && (vel_r<=(int16_t)VREF_70_KPH)) {

            tempW4=(100-(int16_t)U8_MINI_WHEEL_DOWN_RATE);
            tempW5=(tempW4-5);
            tempW6=(tempW4+5);

          #if   __REAR_D
            NON_DRIVEN_MINI_DETECT(&RL, &FL, &FR);
            NON_DRIVEN_MINI_DETECT(&RR, &FR, &FL);
          #else
            NON_DRIVEN_MINI_DETECT(&FL, &RL, &RR);
            NON_DRIVEN_MINI_DETECT(&FR, &RR, &RL);
          #endif
            MINI_NON_DRIVEN_WHEEL_CLEAR();
        }
    }
  #else
    DETECT_MINI_TCS=0;
  #endif
}

#if   __SPARE_WHEEL       /* TAG 4 */
void    NON_DRIVEN_MINI_DETECT(struct W_STRUCT *pD1, struct W_STRUCT *pND1, struct W_STRUCT *pND2)
{
    if(pND1->vrad > pND2->vrad) {
        tempW0 = pND1->vrad - pND2->vrad;
        if(pND1->vrad > pD1->vrad) {
            if(tempW0>tempW5) {
                if(tempW0<tempW6) {
                    pND1->mini_count++;
                }
                else pND1->mini_count=0;
            }
            else pND1->mini_count=0;
        }
        else pND1->mini_count=0;
    }
    else pND1->mini_count=0;

    if(ayf_aver > 100) pND1->mini_count=0;

    if(pND1->mini_count > L_U8_TIME_10MSLOOP_490MS) {
        pND1->mini_count=0;
        DETECT_MINI_TCS=1;
        if(pND1->REAR_WHEEL == 0) MINI_REAR=0;
        else MINI_REAR=1;
        if(pND1->LEFT_WHEEL == 0) MINI_LEFT=0;
        else MINI_LEFT=1;
    }
}

void    MINI_NON_DRIVEN_WHEEL_CLEAR(void)
{
    if(DETECT_MINI_TCS == 1) {
        if(MINI_REAR == 0) {
            if(MINI_LEFT == 0) {    
                if((vrad_fr < vrad_fl) && (vrad_fr < vrad_rr)) DETECT_MINI_TCS=0;
            }
            else {                  
                if((vrad_fl < vrad_fr) && (vrad_fl < vrad_rl)) DETECT_MINI_TCS=0;
            }
        }
        else {
            if(MINI_LEFT == 0) {    
                if((vrad_rr < vrad_rl) && (vrad_rr < vrad_fr)) DETECT_MINI_TCS=0;
            }
            else {                  
                if((vrad_rl < vrad_rr) && (vrad_rl < vrad_fl)) DETECT_MINI_TCS=0;
            }
        }
    }
}
#endif    /* TAG 4 */ 

#if (__TCS_MU_DETECTION == ENABLE)

static void LDTCS_vCall1stMuDetection(void)
{
	LDTCS_vAx1stDropContFlag();
	LDTCS_vMuSetbyAx();
	LDTCS_vEstMuSetWithAccr();
}


static void LDTCS_vAx1stDropContFlag(void)
{
	if (
	#if __AX_SENSOR
		
		#if (NEW_FM_ENABLE == ENABLE)
		(fu1FMAxErrDet==1)
		#else
		((fu1AxSusDet==1)||(fu1AxErrorDet==1))
		#endif		
		#if(__GEAR_SWITCH_TYPE == ANALOG_TYPE)
		|| (fsu1GearRSwitch_error_flg == 1)
		#elif (__GEAR_SWITCH_TYPE == CAN_TYPE)
		|| (ccu1GearRSwitchInv == 1)
		#else
		#endif
	   )	
	{
		ldtcs1stAxDropFlag = 0; /* Ax or gear switch fail : use acc_r*/
	}
	else
	{
		ldtcs1stAxDropFlag = 1;
	}
	
	#else

	ldtcs1stAxDropFlag = 0;

	#endif

}

static void LDTCS_vMuSetbyAx(void)
{
	ldtcs16ALongFilt = (int16_t)LCESP_s16Lpf1Int(a_long_1_1000g,ldtcs16ALongFilt,L_U8FILTER_GAIN_10MSLOOP_8HZ);

	if(ldtcs1stAxDropFlag == 0)
		{
			lctcss16ALongNorm = acc_r;
	 	}
	 	else
		{
			if((fu1GearR_Switch == 1) || (gs_pos == GEAR_POS_TM_R))
			{   	 
				lctcss16ALongNorm = (int16_t)(((int32_t)ldtcs16ALongFilt * 2)/10) * -1;
			}
				else
			{
				lctcss16ALongNorm = (int16_t)(((int32_t)ldtcs16ALongFilt * 2)/10);
			}

		}
}

static void LDTCS_vEstMuSetWithAccr(void)
{	
	if(((TCS_ON == 0) || (lctcsu8TCSEngCtlPhase <= 1)) && (ldtcs1stAxDropFlag == 1))
	{
		lctcs161stEstMu = lctcss16ALongNorm;
		
		lctcs161stEstMuWtAlat = LCESP_s16IInter3Point(lctcss16ALongNorm,	(int16_t)U16_AX_RANGE_MIN,	S16_LM_EMS,
																			(int16_t)U16_AX_RANGE_MAX,	S16_MM_EMS,
																			(int16_t)U16_AX_RANGE_HIGH_MU_MAX,	S16_HM_EMS); 
	}
	else
	{
		lctcs161stEstMu = acc_r;
		lctcs161stEstMuWtAlat = det_alatm;
	}
	


}

#endif /*__TCS_MU_DETECTION*/

#endif      /* TAG 3 */

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDTCSCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

