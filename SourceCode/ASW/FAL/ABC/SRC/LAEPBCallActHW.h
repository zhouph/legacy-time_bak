#ifndef __LAEPBCALLACTHW_H__
#define __LAEPBCALLACTHW_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition**************************/
  #if __EPB_INTERFACE
#define U8_EPB_HOLD         1
#define U8_EPB_APPLY        2
#define U8_EPB_DUMP         3
  #endif

/*Global MACRO FUNCTION Definition**************************/

/*Global Type Declaration **********************************/
  #if __EPB_INTERFACE
typedef struct
{
    uint16_t lau1EpbWL1No :1;
    uint16_t lau1EpbWL1Nc :1;
    uint16_t lau1EpbWL2No :1;
    uint16_t lau1EpbWL2Nc :1;
}EPB_VALVE_t;

   #if (__SPLIT_TYPE==0)
#define EPB_HV_VL_fr            laEpbValveP.lau1EpbWL1No
#define EPB_AV_VL_fr            laEpbValveP.lau1EpbWL1Nc
#define EPB_HV_VL_rl            laEpbValveP.lau1EpbWL2No
#define EPB_AV_VL_rl            laEpbValveP.lau1EpbWL2Nc

#define EPB_HV_VL_fl            laEpbValveS.lau1EpbWL1No
#define EPB_AV_VL_fl            laEpbValveS.lau1EpbWL1Nc
#define EPB_HV_VL_rr            laEpbValveS.lau1EpbWL2No
#define EPB_AV_VL_rr            laEpbValveS.lau1EpbWL2Nc
   #else
#define EPB_HV_VL_fr            laEpbValveP.lau1EpbWL1No
#define EPB_AV_VL_fr            laEpbValveP.lau1EpbWL1Nc
#define EPB_HV_VL_fl            laEpbValveP.lau1EpbWL2No
#define EPB_AV_VL_fl            laEpbValveP.lau1EpbWL2Nc

#define EPB_HV_VL_rl            laEpbValveS.lau1EpbWL1No
#define EPB_AV_VL_rl            laEpbValveS.lau1EpbWL1Nc
#define EPB_HV_VL_rr            laEpbValveS.lau1EpbWL2No
#define EPB_AV_VL_rr            laEpbValveS.lau1EpbWL2Nc
   #endif
  #endif
/*Global Extern Variable  Declaration***********************/
/********** From */
  #if __EPB_INTERFACE
   #if __ADVANCED_MSC
/*extern int16_t        epb_msc_target_vol, epb_initial_motor_on;*/
   #endif
/********** To */
 
extern uint8_t  lcu8EpbMotorMode;
 

extern EPB_VALVE_t  laEpbValveP, laEpbValveS;
  #endif
/*Global Extern Functions  Declaration**********************/
  #if __EPB_INTERFACE
extern void LAEPB_vCallActHW(void);
   #if __ADVANCED_MSC
extern void LCMSC_vSetEPBTargetVoltage(void);
   #endif
extern int16_t LAMFC_s16SetMFCCurrentEPB(EPB_VALVE_t *pEpb, int16_t MFC_Current_old);   
  #endif
#endif
