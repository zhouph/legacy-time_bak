#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCalVlvOpenTiForComb
	#include "Mdyn_autosar.h"
#endif

/* Includes ******************************************************************/
#include "LCESPCalVlvOpenTiForComb.h"
#include "LACallMain.h" /* U8_BASE_LOOPTIME */
#include "LCESPCalInterpolation.h"

#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)

/* Global Function prototype **************************************************/
/*main function */
void LCESP_vCalcExtdVlvRiseTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
void LCESP_vResetVlvRiseTi(struct ESC_STRUCT *ESC_WL);

void LCESP_vCalcExtdVlvDumpTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
void LCESP_vResetVlvDumpTi(struct ESC_STRUCT *ESC_WL);

/* Local Function prototype **************************************************/
/* sub function */
static void LCESP_vCalVlvRiseTiForEscComb(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
static void LCESP_vCalcExtdVlvRiseTiWhl(uint8_t u8InputTi, struct ESC_STRUCT *ESC_WL);
static void LCESP_vPauseVlvRiseTi(struct ESC_STRUCT *ESC_WL);

static void LCESP_vCalVlvDumpTiForEscComb(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL);
static void LCESP_vCalcExtdVlvDumpTiWhl(uint8_t u8InputTi, struct ESC_STRUCT *ESC_WL);
static void LCESP_vPauseVlvDumpTi(struct ESC_STRUCT *ESC_WL);

#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

/* common function */
uint8_t LESP_u8Sel3ValueBy2Point(int16_t s16Input, int16_t s16Point_1, uint8_t u8OutValue_1, int16_t s16Point_2, uint8_t u8OutValue_2, uint8_t u8OutValue_3);
int16_t  LESP_s16LimitMinMax(int16_t s16InputValue, int16_t s16MinValue, int16_t s16MaxValue);


/* Implementation*************************************************************/
#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
void LCESP_vCalcExtdVlvRiseTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL)
{
	if(ESC_WL->u8WhlVlvRiseExtdIndex == U8_INTVAL_RISE_INITIALIZED)
	{
		LCESP_vCalVlvRiseTiForEscComb(WL, ESC_WL);
		LCESP_vCalcExtdVlvRiseTiWhl(ESC_WL->u8WhlVlvRiseTi, ESC_WL);
	}
	else if(ESC_WL->u8WhlVlvRiseExtdIndex == U8_INTVAL_RISE_EXTENDED)
	{
		LCESP_vCalcExtdVlvRiseTiWhl(ESC_WL->u8WhlVlvRiseExtdTi, ESC_WL);
	}
	else /* (ESC_WL->u8WhlVlvRiseExtdIndex == U8_INTVAL_RISE_EXIT)  */
	{
		LCESP_vPauseVlvRiseTi(ESC_WL);
	}
}

void LCESP_vCalcExtdVlvDumpTi(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL)
{
	if(ESC_WL->u8WhlVlvDumpExtdIndex == U8_INTVAL_DUMP_INITIALIZED)
	{
		LCESP_vCalVlvDumpTiForEscComb(WL, ESC_WL);
		LCESP_vCalcExtdVlvDumpTiWhl(ESC_WL->u8WhlVlvDumpTi, ESC_WL);
	}
	else if(ESC_WL->u8WhlVlvDumpExtdIndex == U8_INTVAL_DUMP_EXTENDED)
	{
		LCESP_vCalcExtdVlvDumpTiWhl(ESC_WL->u8WhlVlvDumpExtdTi, ESC_WL);
	}
	else /* (ESC_WL->u8WhlVlvDumpExtdIndex == U8_INTVAL_RISE_EXIT)  */
	{
		LCESP_vPauseVlvDumpTi(ESC_WL);
	}
}

void LCESP_vResetVlvRiseTi(struct ESC_STRUCT *ESC_WL)
{
	ESC_WL->u8WhlVlvRiseTi = 0;
	ESC_WL->u8WhlVlvRiseExtdTi = 0;
	ESC_WL->u8WhlVlvRiseCmdTi = 0;
	ESC_WL->u8WhlVlvRiseExtdIndex = U8_INTVAL_RISE_INITIALIZED;
}

void LCESP_vResetVlvDumpTi(struct ESC_STRUCT *ESC_WL)
{
	ESC_WL->u8WhlVlvDumpTi = 0;
	ESC_WL->u8WhlVlvDumpExtdTi = 0;
	ESC_WL->u8WhlVlvDumpCmdTi = 0;
	ESC_WL->u8WhlVlvDumpExtdIndex = U8_INTVAL_DUMP_INITIALIZED;
}

static void LCESP_vPauseVlvRiseTi(struct ESC_STRUCT *ESC_WL)
{
	ESC_WL->u8WhlVlvRiseTi = 0;
	ESC_WL->u8WhlVlvRiseExtdTi = 0;
	ESC_WL->u8WhlVlvRiseCmdTi = 0;
	ESC_WL->u8WhlVlvRiseExtdIndex = U8_INTVAL_RISE_EXIT;
}

static void LCESP_vPauseVlvDumpTi(struct ESC_STRUCT *ESC_WL)
{
	ESC_WL->u8WhlVlvDumpTi = 0;
	ESC_WL->u8WhlVlvDumpExtdTi = 0;
	ESC_WL->u8WhlVlvDumpCmdTi = 0;
	ESC_WL->u8WhlVlvDumpExtdIndex = U8_INTVAL_DUMP_EXIT;
}

static void LCESP_vCalcExtdVlvRiseTiWhl(uint8_t u8InputTi, struct ESC_STRUCT *ESC_WL)
{
	if(u8InputTi > U8_BASE_LOOPTIME)
	{
		ESC_WL->u8WhlVlvRiseCmdTi = U8_BASE_LOOPTIME;
		ESC_WL->u8WhlVlvRiseExtdTi = u8InputTi - U8_BASE_LOOPTIME;
		ESC_WL->u8WhlVlvRiseExtdIndex = U8_INTVAL_RISE_EXTENDED;
	}
	else
	{
		ESC_WL->u8WhlVlvRiseCmdTi = u8InputTi;
		ESC_WL->u8WhlVlvRiseExtdTi = 0;
	    ESC_WL->u8WhlVlvRiseExtdIndex = U8_INTVAL_RISE_EXIT;
	}

}

static void LCESP_vCalcExtdVlvDumpTiWhl(uint8_t u8InputTi, struct ESC_STRUCT *ESC_WL)
{
	if(u8InputTi > U8_BASE_LOOPTIME)
	{
		ESC_WL->u8WhlVlvDumpCmdTi = U8_BASE_LOOPTIME;
		ESC_WL->u8WhlVlvDumpExtdTi = u8InputTi - U8_BASE_LOOPTIME;
		ESC_WL->u8WhlVlvDumpExtdIndex = U8_INTVAL_DUMP_EXTENDED;
	}
	else
	{
		ESC_WL->u8WhlVlvDumpCmdTi = u8InputTi;
		ESC_WL->u8WhlVlvDumpExtdTi = 0;
	    ESC_WL->u8WhlVlvDumpExtdIndex = U8_INTVAL_DUMP_EXIT;
	}

}

static void LCESP_vCalVlvRiseTiForEscComb(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL)
{
	int16_t s16EstWhlP = WL->s16_Estimated_Active_Press;
	int16_t s16CircP = ESC_WL->s16CircPressFilt/10; /* resol change 100->10 */
	int16_t s16TargetP = ESC_WL->lcesps16IdbTarPress;
	int16_t s16DelTP = ESC_WL->lcesps16IdbriseDelTp;
	int16_t s16CircPDiff = 0;
	uint8_t u8VlvOpenTi_WhlP_1=0;
	uint8_t u8VlvOpenTi_WhlP_2=0;
	uint8_t u8VlvOpenTi_WhlP_3=0;
	uint8_t u8VlvOpenTi_DelTP_1=0;
	uint8_t u8VlvOpenTi_DelTP_2=0;
	uint8_t u8VlvOpenTi_DelTP_3=0;
	uint8_t u8VlvOpenTi_DelTP_4=0;


	s16CircPDiff = s16CircP - WL->s16_Estimated_Active_Press;
	s16CircPDiff = LESP_s16LimitMinMax(s16CircPDiff, MPRESS_0BAR, MPRESS_100BAR);

	if(s16CircPDiff > MPRESS_10BAR)
	{
		u8VlvOpenTi_WhlP_1 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP1_OPEN_TI_1_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP1_OPEN_TI_1_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP1_OPEN_TI_1_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP1_OPEN_TI_1_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP1_OPEN_TI_1_5);

		u8VlvOpenTi_WhlP_2 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP1_OPEN_TI_2_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP1_OPEN_TI_2_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP1_OPEN_TI_2_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP1_OPEN_TI_2_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP1_OPEN_TI_2_5);

		u8VlvOpenTi_WhlP_3 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP1_OPEN_TI_3_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP1_OPEN_TI_3_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP1_OPEN_TI_3_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP1_OPEN_TI_3_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP1_OPEN_TI_3_5);

		u8VlvOpenTi_DelTP_1 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1, u8VlvOpenTi_WhlP_1,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2, u8VlvOpenTi_WhlP_2,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_3, u8VlvOpenTi_WhlP_3);

		u8VlvOpenTi_WhlP_1 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP2_OPEN_TI_1_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP2_OPEN_TI_1_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP2_OPEN_TI_1_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP2_OPEN_TI_1_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP2_OPEN_TI_1_5);

		u8VlvOpenTi_WhlP_2 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP2_OPEN_TI_2_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP2_OPEN_TI_2_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP2_OPEN_TI_2_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP2_OPEN_TI_2_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP2_OPEN_TI_2_5);

		u8VlvOpenTi_WhlP_3 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP2_OPEN_TI_3_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP2_OPEN_TI_3_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP2_OPEN_TI_3_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP2_OPEN_TI_3_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP2_OPEN_TI_3_5);

		u8VlvOpenTi_DelTP_2 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1, u8VlvOpenTi_WhlP_1,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2, u8VlvOpenTi_WhlP_2,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_3, u8VlvOpenTi_WhlP_3);


		u8VlvOpenTi_WhlP_1 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP3_OPEN_TI_1_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP3_OPEN_TI_1_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP3_OPEN_TI_1_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP3_OPEN_TI_1_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP3_OPEN_TI_1_5);

		u8VlvOpenTi_WhlP_2 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP3_OPEN_TI_2_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP3_OPEN_TI_2_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP3_OPEN_TI_2_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP3_OPEN_TI_2_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP3_OPEN_TI_2_5);

		u8VlvOpenTi_WhlP_3 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP3_OPEN_TI_3_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP3_OPEN_TI_3_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP3_OPEN_TI_3_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP3_OPEN_TI_3_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP3_OPEN_TI_3_5);

		u8VlvOpenTi_DelTP_3 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1, u8VlvOpenTi_WhlP_1,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2, u8VlvOpenTi_WhlP_2,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_3, u8VlvOpenTi_WhlP_3);

		u8VlvOpenTi_WhlP_1 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP4_OPEN_TI_1_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP4_OPEN_TI_1_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP4_OPEN_TI_1_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP4_OPEN_TI_1_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP4_OPEN_TI_1_5);

		u8VlvOpenTi_WhlP_2 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP4_OPEN_TI_2_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP4_OPEN_TI_2_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP4_OPEN_TI_2_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP4_OPEN_TI_2_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP4_OPEN_TI_2_5);

		u8VlvOpenTi_WhlP_3 = (uint8_t)LCESP_s16IInter5Point(s16CircPDiff,	S16_ESC_ABS_COMB_DIFFP_LEVEL_1, U8_ESC_ABS_DelTP4_OPEN_TI_3_1,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_2, U8_ESC_ABS_DelTP4_OPEN_TI_3_2,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_3, U8_ESC_ABS_DelTP4_OPEN_TI_3_3,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_4, U8_ESC_ABS_DelTP4_OPEN_TI_3_4,
																			S16_ESC_ABS_COMB_DIFFP_LEVEL_5, U8_ESC_ABS_DelTP4_OPEN_TI_3_5);

		u8VlvOpenTi_DelTP_4 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_1, u8VlvOpenTi_WhlP_1,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_2, u8VlvOpenTi_WhlP_2,
																			S16_ESC_ABS_COMB_WHL_PRESS_LEVEL_3, u8VlvOpenTi_WhlP_3);


		ESC_WL->u8WhlVlvRiseTi = (uint8_t)LCESP_s16IInter4Point(s16DelTP,	S16_ESC_ABS_COMB_DEL_TP_LEVEL_1, u8VlvOpenTi_DelTP_1,
																			S16_ESC_ABS_COMB_DEL_TP_LEVEL_2, u8VlvOpenTi_DelTP_2,
																			S16_ESC_ABS_COMB_DEL_TP_LEVEL_3, u8VlvOpenTi_DelTP_3,
																			S16_ESC_ABS_COMB_DEL_TP_LEVEL_4, u8VlvOpenTi_DelTP_4);

		/*
		if(s16EstWhlP > S16_ESC_COMB_WHL_PRESS_LEVEL_ABS_2)
		{
			ESC_WL->u8WhlVlvRiseTi = LESP_u8Sel3ValueBy2Point(s16CircPDiff,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_1,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_0,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_2,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_1,
																									U8_ESC_COMB_VALVE_OPEN_TIME_ABS_2_2);
		}
		else if(s16EstWhlP > S16_ESC_COMB_WHL_PRESS_LEVEL_ABS_1)
		{
			ESC_WL->u8WhlVlvRiseTi = LESP_u8Sel3ValueBy2Point(s16CircPDiff,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_1,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_0,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_2,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_1,
																									U8_ESC_COMB_VALVE_OPEN_TIME_ABS_1_2);
		}
		else
		{
			ESC_WL->u8WhlVlvRiseTi = LESP_u8Sel3ValueBy2Point(s16CircPDiff,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_1,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_0,
															S16_ESC_COMB_DIFF_PRESS_LEVEL_ABS_2,	U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_1,
																									U8_ESC_COMB_VALVE_OPEN_TIME_ABS_0_2);
		}
		*/
	} /* if(s16CircPDiff > MPRESS_10BAR) */
	else
	{
		ESC_WL->u8WhlVlvRiseTi = (uint8_t)LCESP_s16IInter4Point(s16DelTP,	S16_ESC_BBS_COMB_DEL_TP_LEVEL_1, U8_ESC_BBS_COMB_VALVE_OPEN_TIME_1,
																			S16_ESC_BBS_COMB_DEL_TP_LEVEL_2, U8_ESC_BBS_COMB_VALVE_OPEN_TIME_2,
																			S16_ESC_BBS_COMB_DEL_TP_LEVEL_3, U8_ESC_BBS_COMB_VALVE_OPEN_TIME_3,
																			S16_ESC_BBS_COMB_DEL_TP_LEVEL_4, U8_ESC_BBS_COMB_VALVE_OPEN_TIME_4);
	}
	
	esp_tempW0 = (ESC_WL->u8WhlVlvRiseTi/U8_BASE_LOOPTIME);
	esp_tempW1 = (ESC_WL->u8WhlVlvRiseTi%U8_BASE_LOOPTIME);
	esp_tempW2 = (esp_tempW1==0)? 0 : 1;

	ESC_WL->u8WhlVlvRiseExtdScan = esp_tempW0+esp_tempW2;
}

static void LCESP_vCalVlvDumpTiForEscComb(struct W_STRUCT *WL, struct ESC_STRUCT *ESC_WL)
{
	int16_t s16EstWhlP = WL->s16_Estimated_Active_Press;
	int16_t s16DelTP = ESC_WL->lcesps16IdbdumpDelTp;
	int16_t u8VlvDumpTi_DelTP_1 = 0;
	int16_t u8VlvDumpTi_DelTP_2 = 0;
	int16_t u8VlvDumpTi_DelTP_3 = 0;
	int16_t u8VlvDumpTi_DelTP_4 = 0;

	u8VlvDumpTi_DelTP_1 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_DUMP_WHL_PRESS_LEVEL_1, S16_DUMP_TIME_1_1,
																		S16_DUMP_WHL_PRESS_LEVEL_2, S16_DUMP_TIME_1_2,
																		S16_DUMP_WHL_PRESS_LEVEL_3, S16_DUMP_TIME_1_3);

	u8VlvDumpTi_DelTP_2 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_DUMP_WHL_PRESS_LEVEL_1, S16_DUMP_TIME_2_1,
																		S16_DUMP_WHL_PRESS_LEVEL_2, S16_DUMP_TIME_2_2,
																		S16_DUMP_WHL_PRESS_LEVEL_3, S16_DUMP_TIME_2_3);

	u8VlvDumpTi_DelTP_3 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_DUMP_WHL_PRESS_LEVEL_1, S16_DUMP_TIME_3_1,
																		S16_DUMP_WHL_PRESS_LEVEL_2, S16_DUMP_TIME_3_2,
																		S16_DUMP_WHL_PRESS_LEVEL_3, S16_DUMP_TIME_3_3);

	u8VlvDumpTi_DelTP_4 = (uint8_t)LCESP_s16IInter3Point(s16EstWhlP,	S16_DUMP_WHL_PRESS_LEVEL_1, S16_DUMP_TIME_4_1,
																		S16_DUMP_WHL_PRESS_LEVEL_2, S16_DUMP_TIME_4_2,
																		S16_DUMP_WHL_PRESS_LEVEL_3, S16_DUMP_TIME_4_3);


	ESC_WL->u8WhlVlvDumpTi = (uint8_t)LCESP_s16IInter4Point(s16DelTP,	S16_DUMP_DEL_TP_LEVEL_1, u8VlvDumpTi_DelTP_1,
																		S16_DUMP_DEL_TP_LEVEL_2, u8VlvDumpTi_DelTP_2,
																		S16_DUMP_DEL_TP_LEVEL_3, u8VlvDumpTi_DelTP_3,
																		S16_DUMP_DEL_TP_LEVEL_4, u8VlvDumpTi_DelTP_4);
	esp_tempW0 = (ESC_WL->u8WhlVlvDumpTi/U8_BASE_LOOPTIME);
	esp_tempW1 = (ESC_WL->u8WhlVlvDumpTi%U8_BASE_LOOPTIME);
	esp_tempW2 = (esp_tempW1==0)? 0 : 1;

	ESC_WL->u8WhlVlvDumpExtdScan = esp_tempW0+esp_tempW2;
}

#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

uint8_t LESP_u8Sel3ValueBy2Point(int16_t s16Input, int16_t s16Point_1, uint8_t u8OutValue_1, int16_t s16Point_2, uint8_t u8OutValue_2, uint8_t u8OutValue_3)
{
	uint8_t u8Output;

	if(s16Input > s16Point_2)
	{
		u8Output = u8OutValue_3;
	}
	else if(s16Input > s16Point_1)
	{
		u8Output = u8OutValue_2;
	}
	else
	{
		u8Output = u8OutValue_1;
	}

	return u8Output;
}

int16_t  LESP_s16LimitMinMax(int16_t s16InputValue, int16_t s16MinValue, int16_t s16MaxValue)
{
	int16_t OutputValue;

	if 		(s16InputValue > s16MaxValue)
	{
		OutputValue = s16MaxValue;
	}
	else if (s16InputValue < s16MinValue)
	{
		OutputValue = s16MinValue;
	}
	else
	{
		OutputValue = s16InputValue;
   	}

	return OutputValue;
}
