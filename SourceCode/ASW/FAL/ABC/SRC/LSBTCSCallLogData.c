/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSBTCSCallLogData
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ******************************************************************/

#include "LSBTCSCallLogData.h"
#include "LCTCSCallControl.h"
#include "LCHSACallControl.h"
#include "LDESPEstDiscTemp.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"

/* Local Definiton  **********************************************************/


/* Variables Definition*******************************************************/

/* LocalFunction prototype ***************************************************/
#if defined(__LOGGER)    /*__LOGGER*/
    #if __LOGGER_DATA_TYPE==1  /* __LOGGER_DATA_TYPE*/

uint16_t LSBTCS_u16PackData12_1_1_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2, uint16_t Flag3, uint16_t Flag4);
uint16_t LSBTCS_u16PackData12_2_2(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2);
uint8_t LSBTCS_u8PackData4_4(uint8_t data1, uint8_t data2);
uint8_t LSBTCS_u8PackFlags(uint8_t Flag1, uint8_t Flag2, uint8_t Flag3, uint8_t Flag4, uint8_t Flag5, uint8_t Flag6, uint8_t Flag7, uint8_t Flag8);

/* GlobalFunction prototype **************************************************/
void LSTCS_vCallBTCS_Gen2_Val_Log(void);

/* Implementation*************************************************************/
#if	defined(BTCS_TCMF_CONTROL)
void LSTCS_vCallBTCS_Gen2_Val_Log(void)
{
	#if defined(__EXTEND_LOGGER)
    uint8_t 	u8TempMem4Log=0;
    uint16_t 	u16TempMem4Log=0;
	uint8_t 	Reserved1Bit=0;
	uint8_t 	Reserved2Bit=0;
	uint8_t 	Reserved8Bit=0;
	uint16_t 	Reserved16Bit=0;
	
	/* Byte 0 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(vrad_crt_fl, HV_VL_fl, AV_VL_fl, FL_TCS.lctcsu1BTCSWhlAtv, FL_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[0] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[1] =	(uint8_t) (u16TempMem4Log>>8);	
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(vrad_crt_fr, HV_VL_fr, AV_VL_fr, FR_TCS.lctcsu1BTCSWhlAtv, FR_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[2] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[3] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(vrad_crt_rl, HV_VL_rl, AV_VL_rl, RL_TCS.lctcsu1BTCSWhlAtv, RL_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[4] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[5] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(vrad_crt_rr, HV_VL_rr, AV_VL_rr, RR_TCS.lctcsu1BTCSWhlAtv, RR_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[6] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[7] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 1 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(lctcss16VehSpd4TCS, TCL_DEMAND_PRIMARY, TCL_DEMAND_SECONDARY, S_VALVE_PRIMARY, S_VALVE_SECONDARY);
	CAN_LOG_DATA[8] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[9] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(PC_TCS.lctcss16TCVCurrent/10, PC_TCS.lctcsu1IniTCVCtlMode, PC_TCS.ltcsu1CirModOfAsymSpnCtl, PC_TCS.lctcsu1EstPresLowFlag, Reserved1Bit);
	CAN_LOG_DATA[10] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[11] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(SC_TCS.lctcss16TCVCurrent/10, SC_TCS.lctcsu1IniTCVCtlMode, SC_TCS.ltcsu1CirModOfAsymSpnCtl, SC_TCS.lctcsu1EstPresLowFlag, Reserved1Bit);
	CAN_LOG_DATA[12] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[13] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(wstr/10, FA_TCS.lctcsu1FFAdaptByHghMuWhlUnstab, RA_TCS.lctcsu1FFAdaptByHghMuWhlUnstab, FA_TCS.lctcsu1FFAdaptUpdateByHSS, RA_TCS.lctcsu1FFAdaptUpdateByHSS);
	CAN_LOG_DATA[14] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[15] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 2 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FA_TCS.lctcss16FFBrkTorq4Asym, FA_TCS.lctcsu1DctBTCSplitMuAxle, FA_TCS.ltcsu1SpinDiffInc, FA_TCS.ltcsu1SpinDiffDec, FA_TCS.lctcsu1FlgOfAfterHghMuUnstb);
	CAN_LOG_DATA[16] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[17] =	(uint8_t) (u16TempMem4Log>>8);	
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RA_TCS.lctcss16FFBrkTorq4Asym, RA_TCS.lctcsu1DctBTCSplitMuAxle, RA_TCS.ltcsu1SpinDiffInc, RA_TCS.ltcsu1SpinDiffDec, RA_TCS.lctcsu1FlgOfAfterHghMuUnstb);
	CAN_LOG_DATA[18] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[19] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FA_TCS.lctcss16PeffOnBsTBrkTorq4Asym, FA_TCS.lctcsu1SymSpinDctFlg, FA_TCS.lctcsu1OverBrkAxle, FA_TCS.ltcsu1ModOfAsymSpnCtl, FA_TCS.ltcss8IIntAct);
	CAN_LOG_DATA[20] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[21] =	(uint8_t) (u16TempMem4Log>>8);	
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RA_TCS.lctcss16PeffOnBsTBrkTorq4Asym, RA_TCS.lctcsu1SymSpinDctFlg, RA_TCS.lctcsu1OverBrkAxle, RA_TCS.ltcsu1ModOfAsymSpnCtl, RA_TCS.ltcss8IIntAct);
	CAN_LOG_DATA[22] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[23] =	(uint8_t) (u16TempMem4Log>>8);		
	
	/* Byte 3 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FA_TCS.lctcss16IeffOnBsTBrkTorq4Asym, FA_TCS.lctcsu1BrkCtlFadeOutModeFlag, BTCS_Entry_Inhibition, tc_on_flg, tc_error_flg);
	CAN_LOG_DATA[24] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[25] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RA_TCS.lctcss16IeffOnBsTBrkTorq4Asym, RA_TCS.lctcsu1BrkCtlFadeOutModeFlag, tcs_error_flg, fu1VoltageUnderErrDet, vdc_on_flg);
	CAN_LOG_DATA[26] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[27] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FA_TCS.lctcss16EstBrkTorqOnLowMuWhl, ESP_ERROR_FLG, BLS_ERROR, BLS_EEPROM, lctcsu1RoughRoad);
	CAN_LOG_DATA[28] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[29] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RA_TCS.lctcss16EstBrkTorqOnLowMuWhl, gs_ena, ENG_STALL, VCA_ON_TCMF, lctcsu1DriverAccelIntend);
	CAN_LOG_DATA[30] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[31] =	(uint8_t) (u16TempMem4Log>>8);		
	
	/* Byte 4 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FL_TCS.lctcss16BsTarBrkTorq, ABS_ON, EBD_RA, ETCS_ON, FTCS_ON);
	CAN_LOG_DATA[32] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[33] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FR_TCS.lctcss16BsTarBrkTorq, YAW_CDC_WORK, ESP_TCS_ON, TCS_STUCK_ON, BTCS_ON);
	CAN_LOG_DATA[34] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[35] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RL_TCS.lctcss16BsTarBrkTorq, MPRESS_BRAKE_ON, BLS, AUTO_TM, lctcsu1BTCSVehAtv);
	CAN_LOG_DATA[36] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[37] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RR_TCS.lctcss16BsTarBrkTorq, lctcsu1IniMtrCtlMode, lctcsu1DctBTCSplitMuFlag, lctcsu1DctBTCSplitMuHill, lctcsu1BigSpnAwdDctd);
	CAN_LOG_DATA[38] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[39] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 5 */
	u16TempMem4Log = LSBTCS_u16PackData12_2_2(FL.ltcss16MovingAveragedWhlSpd, FA_TCS.lctcsu8WhlOnHghMu, RA_TCS.lctcsu8WhlOnHghMu);
	CAN_LOG_DATA[40] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[41] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_2_2(FR.ltcss16MovingAveragedWhlSpd, FA_TCS.lctcsu8StatWhlOnHghMu, RA_TCS.lctcsu8StatWhlOnHghMu);
	CAN_LOG_DATA[42] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[43] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RL.ltcss16MovingAveragedWhlSpd, FL.BTCS, FR.BTCS, RL.BTCS, RR.BTCS);
	CAN_LOG_DATA[44] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[45] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RR.ltcss16MovingAveragedWhlSpd, FA_TCS.lctcsu1BrkCtlActAxle, RA_TCS.lctcsu1BrkCtlActAxle, PC_TCS.lctcsu1BrkCtlActCir, SC_TCS.lctcsu1BrkCtlActCir);
	CAN_LOG_DATA[46] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[47] =	(uint8_t) (u16TempMem4Log>>8);	
	
	/* Byte 6 */
	CAN_LOG_DATA[48] =	(uint8_t) (FA_TCS.ltcss16Cnt4AsymSpnCtlStrt);
	CAN_LOG_DATA[49] =	(uint8_t) (RA_TCS.ltcss16Cnt4AsymSpnCtlStrt);
	CAN_LOG_DATA[50] =	(uint8_t) (FA_TCS.ltcss16MonTime4AsymSpnCtlStrt);
	CAN_LOG_DATA[51] =	(uint8_t) (RA_TCS.ltcss16MonTime4AsymSpnCtlStrt);
	CAN_LOG_DATA[52] =	(uint8_t) (FA_TCS.ltcss16WhlAccAvg/10);
	CAN_LOG_DATA[53] =	(uint8_t) (RA_TCS.ltcss16WhlAccAvg/10);
	CAN_LOG_DATA[54] =	(uint8_t) (FA_TCS.ltcss16MonTime4SymSpnCtlStrt);
	CAN_LOG_DATA[55] =	(uint8_t) (RA_TCS.ltcss16MonTime4SymSpnCtlStrt);
	
	/* Byte 7 */
	CAN_LOG_DATA[56] =	(uint8_t) (FA_TCS.lctcss16WhlSpinAxleAvg/4);
	CAN_LOG_DATA[57] =	(uint8_t) (RA_TCS.lctcss16WhlSpinAxleAvg/4);
	tcs_tempW0 = LCTCS_s16ILimitRange(yaw_out/100, -125, 125);	/* 125 = 125deg/s */
	CAN_LOG_DATA[58] =	(uint8_t) (tcs_tempW0);
	tcs_tempW0 = LCTCS_s16ILimitRange(alat/10, -125, 125);	/* 125 = 1.25g */
	CAN_LOG_DATA[59] =	(uint8_t) (tcs_tempW0);
	CAN_LOG_DATA[60] =	(uint8_t) (FA_TCS.ltcss16Cnt4SymSpnCtlStrt);
	CAN_LOG_DATA[61] =	(uint8_t) (RA_TCS.ltcss16Cnt4SymSpnCtlStrt);
	CAN_LOG_DATA[62] =	(uint8_t) (FA_TCS.lctcss16WhlSpdAxleAvg/8);
	CAN_LOG_DATA[63] =	(uint8_t) (RA_TCS.lctcss16WhlSpdAxleAvg/8);
	
	/* Byte 8 */
	CAN_LOG_DATA[64] =	(uint8_t) (FL.arad);
	CAN_LOG_DATA[65] =	(uint8_t) (FR.arad);
	CAN_LOG_DATA[66] =	(uint8_t) (RL.arad);
	CAN_LOG_DATA[67] =	(uint8_t) (RR.arad);
	CAN_LOG_DATA[68] =	(uint8_t) (FA_TCS.ltcss16ChngRateWhlSpinDiffNF/10);
	CAN_LOG_DATA[69] =	(uint8_t) (RA_TCS.ltcss16ChngRateWhlSpinDiffNF/10);
	CAN_LOG_DATA[70] =	(uint8_t) (FA_TCS.ltcss16ChngRateWhlSpinDiff/10);
	CAN_LOG_DATA[71] =	(uint8_t) (RA_TCS.ltcss16ChngRateWhlSpinDiff/10);
	
	/* Byte 9 */
	CAN_LOG_DATA[72] =	(uint8_t) (FL_TCS.lctcsu8HghMuWhlSpnCnt);
	CAN_LOG_DATA[73] =	(uint8_t) (FR_TCS.lctcsu8HghMuWhlSpnCnt);
	CAN_LOG_DATA[74] =	(uint8_t) (RL_TCS.lctcsu8HghMuWhlSpnCnt);
	CAN_LOG_DATA[75] =	(uint8_t) (RR_TCS.lctcsu8HghMuWhlSpnCnt);
	CAN_LOG_DATA[76] =	(uint8_t) (FL_TCS.lctcss16EstWhlPre/10);
	CAN_LOG_DATA[77] =	(uint8_t) (FR_TCS.lctcss16EstWhlPre/10);
	CAN_LOG_DATA[78] =	(uint8_t) (RL_TCS.lctcss16EstWhlPre/10);
	CAN_LOG_DATA[79] =	(uint8_t) (RR_TCS.lctcss16EstWhlPre/10);
	
	/* Byte 10 */
	CAN_LOG_DATA[80] =	(uint8_t) (FL_TCS.lctcss16TarWhlPre/10);
	CAN_LOG_DATA[81] =	(uint8_t) (FR_TCS.lctcss16TarWhlPre/10);
	CAN_LOG_DATA[82] =	(uint8_t) (RL_TCS.lctcss16TarWhlPre/10);
	CAN_LOG_DATA[83] =	(uint8_t) (RR_TCS.lctcss16TarWhlPre/10);
	CAN_LOG_DATA[84] =	(uint8_t) (FA_TCS.lctcss16DctBTCSplitMuEnterCnt);
	CAN_LOG_DATA[85] =	(uint8_t) (RA_TCS.lctcss16DctBTCSplitMuEnterCnt);
	CAN_LOG_DATA[86] =	(uint8_t) (FA_TCS.lctcss16DctBTCSplitMuExitCnt);
	CAN_LOG_DATA[87] =	(uint8_t) (RA_TCS.lctcss16DctBTCSplitMuExitCnt);
	
	/* Byte 11 */
	CAN_LOG_DATA[88] =	(uint8_t) (lctcsu8SplitMuDecHilEnterCnt);
	CAN_LOG_DATA[89] =	(uint8_t) (lctcss16VehTurnIndex);
	CAN_LOG_DATA[90] =	(uint8_t) (FL_TCS.lctcss16AsymSpnCtlTh/4);
	//CAN_LOG_DATA[91] =	(uint8_t) (RL_TCS.lctcss16SymSpnCtlTh/4);
	CAN_LOG_DATA[92] =	(uint8_t) (ltcss16BaseStTarWhlSpinDiff/4);
	CAN_LOG_DATA[93] =	(uint8_t) (ltcss16BaseHillTarWhlSpinDiff/4);
	CAN_LOG_DATA[94] =	(uint8_t) (ltcss16BaseTurnTarWhlSpinDiff/4);
	CAN_LOG_DATA[95] =	(uint8_t) (ltcss16BaseTarWhlSpinDiff/4);
	
	/* Byte 12 */
	u8TempMem4Log =  LSBTCS_u8PackData4_4(FL_TCS.lctcss8BTCSEndTmr, FR_TCS.lctcss8BTCSEndTmr);
	CAN_LOG_DATA[96] =	(uint8_t) (u8TempMem4Log);
	u8TempMem4Log =  LSBTCS_u8PackData4_4(RL_TCS.lctcss8BTCSEndTmr, RR_TCS.lctcss8BTCSEndTmr);
	CAN_LOG_DATA[97] =	(uint8_t) (u8TempMem4Log);
	CAN_LOG_DATA[98] =	(uint8_t) (FA_TCS.lctcss16BsPG4ASFBCtrl);
	CAN_LOG_DATA[99] =	(uint8_t) (RA_TCS.lctcss16BsPG4ASFBCtrl);
	CAN_LOG_DATA[100] =	(uint8_t) (FA_TCS.lctcss16PGF4ASFBCtrl);
	CAN_LOG_DATA[101] =	(uint8_t) (RA_TCS.lctcss16PGF4ASFBCtrl);
	CAN_LOG_DATA[102] =	(uint8_t) (FA_TCS.lctcss16PG4ASFBCtrl);
	CAN_LOG_DATA[103] =	(uint8_t) (RA_TCS.lctcss16PG4ASFBCtrl);
	
	/* Byte 13 */
	CAN_LOG_DATA[104] =	(uint8_t) (FA_TCS.lctcss16BsIG4ASFBCtrl);
	CAN_LOG_DATA[105] =	(uint8_t) (RA_TCS.lctcss16BsIG4ASFBCtrl);
	CAN_LOG_DATA[106] =	(uint8_t) (FA_TCS.lctcss16IGF4ASFBCtrl);
	CAN_LOG_DATA[107] =	(uint8_t) (RA_TCS.lctcss16IGF4ASFBCtrl);
	CAN_LOG_DATA[108] =	(uint8_t) (FA_TCS.lctcss16IG4ASFBCtrl);
	CAN_LOG_DATA[109] =	(uint8_t) (RA_TCS.lctcss16IG4ASFBCtrl);
	CAN_LOG_DATA[110] =	(uint8_t) (FA_TCS.lctcss16BsTBrkLimTorq4Asym/20);
	CAN_LOG_DATA[111] =	(uint8_t) (RA_TCS.lctcss16BsTBrkLimTorq4Asym/20); 
	
	/* Byte 14 */
	CAN_LOG_DATA[112] =	(uint8_t) (FA_TCS.lctcss16MemBsTBrkTorq4Asym/20);
	CAN_LOG_DATA[113] =	(uint8_t) (RA_TCS.lctcss16MemBsTBrkTorq4Asym/20);
	CAN_LOG_DATA[114] =	(uint8_t) (FA_TCS.lctcss16MemBsTBrkRecTrq4HSSEnd/20);
	CAN_LOG_DATA[115] =	(uint8_t) (RA_TCS.lctcss16MemBsTBrkRecTrq4HSSEnd/20);
	CAN_LOG_DATA[116] =	(uint8_t) (FA_TCS.lctcsu16CntAfterHghMuUnstb);
	CAN_LOG_DATA[117] =	(uint8_t) (RA_TCS.lctcsu16CntAfterHghMuUnstb);
	CAN_LOG_DATA[118] =	(uint8_t) (FA_TCS.ltcss16SStateCnt);
	CAN_LOG_DATA[119] =	(uint8_t) (RA_TCS.ltcss16SStateCnt);
	
	/* Byte 15 */
	CAN_LOG_DATA[120] =	(uint8_t) (FA_TCS.lctcss16BrkWrkUntHghMuWhlUnst/10);
	CAN_LOG_DATA[121] =	(uint8_t) (RA_TCS.lctcss16BrkWrkUntHghMuWhlUnst/10);
	CAN_LOG_DATA[122] =	(uint8_t) (FA_TCS.lctcss16BrkTrqMax4Asym/20);
	CAN_LOG_DATA[123] =	(uint8_t) (RA_TCS.lctcss16BrkTrqMax4Asym/20);
	CAN_LOG_DATA[124] =	(uint8_t) (FA_TCS.lctcss16BrkTrqMaxLimTime/10);
	CAN_LOG_DATA[125] =	(uint8_t) (RA_TCS.lctcss16BrkTrqMaxLimTime/10);
	CAN_LOG_DATA[126] =	(uint8_t) (FA_TCS.S16TCSFadeOutDctCnt);
	CAN_LOG_DATA[127] =	(uint8_t) (RA_TCS.S16TCSFadeOutDctCnt);
	
	/* Byte 16 */
	CAN_LOG_DATA[128] =	(uint8_t) (PC_TCS.lctcss16EstCirPres/10);
	CAN_LOG_DATA[129] =	(uint8_t) (SC_TCS.lctcss16EstCirPres/10);
	CAN_LOG_DATA[130] =	(uint8_t) (FA_TCS.lctcss16BsBrkTorqTh4SplitDct/10);
	CAN_LOG_DATA[131] =	(uint8_t) (RA_TCS.lctcss16BsBrkTorqTh4SplitDct/10);
	CAN_LOG_DATA[132] =	(uint8_t) (FA_TCS.lctcss16BsTarBrkTorqAXLEdiffFlt/10);
	CAN_LOG_DATA[133] =	(uint8_t) (RA_TCS.lctcss16BsTarBrkTorqAXLEdiffFlt/10);
	CAN_LOG_DATA[134] =	(uint8_t) (gear_pos);
	tcs_tempW0 = LCTCS_s16ILimitRange(acc_r, -125, 125);	/* 125 = 0.625g */
	CAN_LOG_DATA[135] =	(uint8_t) (tcs_tempW0);
	
	/* Byte 17 */
	CAN_LOG_DATA[136] =	(uint8_t) (FL_TEMPT.ldesps16Tempt/320);
	CAN_LOG_DATA[137] =	(uint8_t) (FR_TEMPT.ldesps16Tempt/320);
	CAN_LOG_DATA[138] =	(uint8_t) (RL_TEMPT.ldesps16Tempt/320);
	CAN_LOG_DATA[139] =	(uint8_t) (RR_TEMPT.ldesps16Tempt/320);
	CAN_LOG_DATA[140] =	(uint8_t) (target_vol/100);
	CAN_LOG_DATA[141] =	(uint8_t) (fu16CalVoltMOTOR/100);
	CAN_LOG_DATA[142] =	(uint8_t) (lctcss16BrkCtrlMode);
	tcs_tempW0 = LCTCS_s16ILimitRange(nor_alat/10, -125, 125);	/* 125 = 1.25g */
	CAN_LOG_DATA[143] =	(uint8_t) (tcs_tempW0);
	
	/* Byte 18 */
	u16TempMem4Log = FA_TCS.lctcss32IntgBrkCtlErr/10;
	CAN_LOG_DATA[144] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[145] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = RA_TCS.lctcss32IntgBrkCtlErr/10;	
	CAN_LOG_DATA[146] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[147] =	(uint8_t) (u16TempMem4Log>>8);
	
	CAN_LOG_DATA[148] =	(uint8_t) (FA_TCS.lctcss16BrkCtlErr);
	CAN_LOG_DATA[149] =	(uint8_t) (FA_TCS.lctcss16BrkCtlErr>>8);
	CAN_LOG_DATA[150] =	(uint8_t) (RA_TCS.lctcss16BrkCtlErr);
	CAN_LOG_DATA[151] =	(uint8_t) (RA_TCS.lctcss16BrkCtlErr>>8);	
	
	/* Byte 19 */
	u8TempMem4Log = LSBTCS_u8PackFlags(lctcsu1WhlSpdCompByRTADetect, lctcsu1InvalidWhlSpdBfRTADetect, lctcsu1DrvMode2Wheel, lctcsu1DrvMode4Wheel,
									   lctcsu1DctBTCHillByAx, Reserved1Bit, Reserved1Bit, Reserved1Bit);		   
	CAN_LOG_DATA[152] =	(uint8_t) (u8TempMem4Log);
	tcs_tempW0 = LCTCS_s16ILimitRange(ax_Filter, -125, 125);	/* 125 = 1.25g */
	CAN_LOG_DATA[153] =	(uint8_t) (tcs_tempW0);
	u8TempMem4Log = LSBTCS_u8PackFlags(FA_TCS.lctcsu1HghMuWhlUnstbInLftWhlCtl, RA_TCS.lctcsu1HghMuWhlUnstbInLftWhlCtl, FA_TCS.lctcsu1HghMuWhlUnstbInRgtWhlCtl, RA_TCS.lctcsu1HghMuWhlUnstbInRgtWhlCtl,
									   FA_TCS.lctcsu1HuntingDcted, RA_TCS.lctcsu1HuntingDcted, FA_TCS.lctcsu1OK2AdaptFF, RA_TCS.lctcsu1OK2AdaptFF);
	CAN_LOG_DATA[154] =	(uint8_t) (u8TempMem4Log);	
	CAN_LOG_DATA[155] =	(uint8_t) (FA_TCS.lctcss16AdaptedFFBrkTorq4Asym/20);
	CAN_LOG_DATA[156] =	(uint8_t) (RA_TCS.lctcss16AdaptedFFBrkTorq4Asym/20);
	CAN_LOG_DATA[157] =	(uint8_t) (lctcsu16BTCStpNMovDistance/1000);
	CAN_LOG_DATA[158] =	(uint8_t) (lctcsu8SplitMuDecHilExitCnt);
	u8TempMem4Log = LSBTCS_u8PackFlags(FA_TCS.lctcsu1OK2BrkTrqMaxLim, RA_TCS.lctcsu1OK2BrkTrqMaxLim, FA_TCS.lctcsu1BrkTrqMax4AsymUpdated, RA_TCS.lctcsu1BrkTrqMax4AsymUpdated,
									   FA_TCS.lctcsu1BrkTrqMaxLimited, RA_TCS.lctcsu1BrkTrqMaxLimited, Reserved1Bit, Reserved1Bit);
	CAN_LOG_DATA[159] =	(uint8_t) (u8TempMem4Log);
	
	#else  /*80byte logger*/
	
	uint8_t i=0;
	uint8_t Reserved1Bit=0;
	uint8_t Reserved2Bit=0;
	uint8_t Reserved8Bit=0;
	
	/* Byte 0 */	
	CAN_LOG_DATA[0] =	(uint8_t) (lctcss16VehSpd4TCS);
	CAN_LOG_DATA[1] =	(uint8_t) (lctcss16VehSpd4TCS>>8);
	CAN_LOG_DATA[2] =	(uint8_t) (FA_TCS.lctcss16BrkCtlErr);	     
	CAN_LOG_DATA[3] =	(uint8_t) (FA_TCS.lctcss16BrkCtlErr>>8);  
	CAN_LOG_DATA[4] =	(uint8_t) (RA_TCS.lctcss16BrkCtlErr);	    
	CAN_LOG_DATA[5] =	(uint8_t) (RA_TCS.lctcss16BrkCtlErr>>8);  
	CAN_LOG_DATA[6] =	(uint8_t) (FA_TCS.lctcss16BsIG4ASFBCtrl);   //temp
	CAN_LOG_DATA[7] =	(uint8_t) (FA_TCS.lctcss16BsIG4ASFBCtrl>>8);
	
	/* Byte 1 */        
	CAN_LOG_DATA[8] =	(uint8_t) (FA_TCS.ltcss16ChngRateWhlSpinDiff);          
	CAN_LOG_DATA[9] =	(uint8_t) (FA_TCS.ltcss16ChngRateWhlSpinDiff>>8);    
	CAN_LOG_DATA[10] =	(uint8_t) (RA_TCS.ltcss16ChngRateWhlSpinDiff);          
	CAN_LOG_DATA[11] =	(uint8_t) (RA_TCS.ltcss16ChngRateWhlSpinDiff>>8);    
	CAN_LOG_DATA[12] =	(uint8_t) (FA_TCS.lctcss16EstBrkTorqOnLowMuWhl);          
	CAN_LOG_DATA[13] =	(uint8_t) (FA_TCS.lctcss16EstBrkTorqOnLowMuWhl>>8); 		
	CAN_LOG_DATA[14] =	(uint8_t) (RA_TCS.lctcss16EstBrkTorqOnLowMuWhl);          
	CAN_LOG_DATA[15] =	(uint8_t) (RA_TCS.lctcss16EstBrkTorqOnLowMuWhl>>8); 

	/* Byte 2 */	
	CAN_LOG_DATA[16] =	(uint8_t) (FL_TCS.lctcss16BsTarBrkTorq);          
	CAN_LOG_DATA[17] =	(uint8_t) (FL_TCS.lctcss16BsTarBrkTorq>>8); 
	CAN_LOG_DATA[18] =	(uint8_t) (FR_TCS.lctcss16BsTarBrkTorq);          
	CAN_LOG_DATA[19] =	(uint8_t) (FR_TCS.lctcss16BsTarBrkTorq>>8); 
	CAN_LOG_DATA[20] =	(uint8_t) (RL_TCS.lctcss16BsTarBrkTorq);          
	CAN_LOG_DATA[21] =	(uint8_t) (RL_TCS.lctcss16BsTarBrkTorq>>8); 
	CAN_LOG_DATA[22] =	(uint8_t) (RR_TCS.lctcss16BsTarBrkTorq);          
	CAN_LOG_DATA[23] =	(uint8_t) (RR_TCS.lctcss16BsTarBrkTorq>>8); 

	/* Byte 3 */	
	CAN_LOG_DATA[24] =	(uint8_t) (FA_TCS.lctcss16PeffOnBsTBrkTorq4Asym);          
	CAN_LOG_DATA[25] =	(uint8_t) (FA_TCS.lctcss16PeffOnBsTBrkTorq4Asym>>8);
	CAN_LOG_DATA[26] =	(uint8_t) (RA_TCS.lctcss16PeffOnBsTBrkTorq4Asym);          
	CAN_LOG_DATA[27] =	(uint8_t) (RA_TCS.lctcss16PeffOnBsTBrkTorq4Asym>>8); 										        
	CAN_LOG_DATA[28] =	(uint8_t) (FA_TCS.lctcss16IeffOnBsTBrkTorq4Asym);          
	CAN_LOG_DATA[29] =	(uint8_t) (FA_TCS.lctcss16IeffOnBsTBrkTorq4Asym>>8);
	CAN_LOG_DATA[30] =	(uint8_t) (RA_TCS.lctcss16IeffOnBsTBrkTorq4Asym);          
	CAN_LOG_DATA[31] =	(uint8_t) (RA_TCS.lctcss16IeffOnBsTBrkTorq4Asym>>8); 										       		

	/* Byte 4 */	
	CAN_LOG_DATA[32] =	(uint8_t) (FA_TCS.lctcss16FFBrkTorq4Asym);
	CAN_LOG_DATA[33] =	(uint8_t) (FA_TCS.lctcss16FFBrkTorq4Asym>>8);    
	CAN_LOG_DATA[34] =	(uint8_t) (RA_TCS.lctcss16FFBrkTorq4Asym);
	CAN_LOG_DATA[35] =	(uint8_t) (RA_TCS.lctcss16FFBrkTorq4Asym>>8);    
						tcs_tempW2 = FA_TCS.lctcss32IntgBrkCtlErr/10;
    					tcs_tempW3 = RA_TCS.lctcss32IntgBrkCtlErr/10;		
	CAN_LOG_DATA[36] =	(uint8_t) (tcs_tempW2);     
	CAN_LOG_DATA[37] =	(uint8_t) (tcs_tempW2>>8);      
	CAN_LOG_DATA[38] =	(uint8_t) (tcs_tempW3);     
	CAN_LOG_DATA[39] =	(uint8_t) (tcs_tempW3>>8);    						
	
	/* Byte 5 */
	CAN_LOG_DATA[40] =	(uint8_t)(target_vol/100);
	CAN_LOG_DATA[41] =	(uint8_t)(fu16CalVoltMOTOR/100);    
	CAN_LOG_DATA[42] =	(uint8_t)(MFC_Current_TCS_P/10);
	CAN_LOG_DATA[43] =	(uint8_t)(MFC_Current_TCS_S/10); 
	CAN_LOG_DATA[44] =	(uint8_t)(PC_TCS.lctcss16EstCirPres/10);
	CAN_LOG_DATA[45] =	(uint8_t)(SC_TCS.lctcss16EstCirPres/10);   
	CAN_LOG_DATA[46] =	(uint8_t)(FA_TCS.lctcss16IGF4ASFBCtrl);
	CAN_LOG_DATA[47] =	(uint8_t)(RA_TCS.lctcss16IGF4ASFBCtrl);
	
	/* Byte 6 */	
	CAN_LOG_DATA[48] =	(uint8_t)(FL_TCS.lctcss16TarWhlPre/10);   
	CAN_LOG_DATA[49] =	(uint8_t)(FR_TCS.lctcss16TarWhlPre/10);   
	CAN_LOG_DATA[50] =	(uint8_t)(RL_TCS.lctcss16TarWhlPre/10);                   
	CAN_LOG_DATA[51] =	(uint8_t)(RR_TCS.lctcss16TarWhlPre/10);   
	CAN_LOG_DATA[52] =	(uint8_t)(FA_TCS.lctcss16BsPG4ASFBCtrl);   
	CAN_LOG_DATA[53] =	(uint8_t)(RA_TCS.lctcss16BsPG4ASFBCtrl);  
	CAN_LOG_DATA[54] =	(uint8_t)(FA_TCS.lctcss16PGF4ASFBCtrl);   
	CAN_LOG_DATA[55] =	(uint8_t)(RA_TCS.lctcss16PGF4ASFBCtrl);  
	
	/* Byte 7 */
	CAN_LOG_DATA[56] =	(uint8_t)(FA_TCS.lctcss16PG4ASFBCtrl);   
	CAN_LOG_DATA[57] =	(uint8_t)(RA_TCS.lctcss16PG4ASFBCtrl);                  
	CAN_LOG_DATA[58] =	(uint8_t)(FA_TCS.S16TCSFadeOutDctCnt);	
	CAN_LOG_DATA[59] =	(uint8_t)(RA_TCS.S16TCSFadeOutDctCnt);	
	CAN_LOG_DATA[60] =	(uint8_t)(FA_TCS.ltcss16MonTime4AsymSpnCtlStrt);   
	CAN_LOG_DATA[61] =	(uint8_t)(RA_TCS.ltcss16MonTime4AsymSpnCtlStrt);                  
	CAN_LOG_DATA[62] =	(uint8_t)(FA_TCS.ltcss16Cnt4AsymSpnCtlStrt);   
	CAN_LOG_DATA[63] =	(uint8_t)(RA_TCS.ltcss16Cnt4AsymSpnCtlStrt);                  
	
	/* Byte 8 */
	CAN_LOG_DATA[64] =	(uint8_t)(FA_TCS.lctcss16IG4ASFBCtrl);
	CAN_LOG_DATA[65] =	(uint8_t)(RA_TCS.lctcss16IG4ASFBCtrl);
	CAN_LOG_DATA[66] =	(uint8_t)(gear_pos);	 //temp
	CAN_LOG_DATA[67] =	(uint8_t)(gear_pos>>8);	
	CAN_LOG_DATA[68] =	(uint8_t)( (FA_TCS.lctcsu8WhlOnHghMu<<6) | (RA_TCS.lctcsu8WhlOnHghMu<<4) | (FA_TCS.lctcsu8StatWhlOnHghMu<<2) | (RA_TCS.lctcsu8StatWhlOnHghMu) );
	CAN_LOG_DATA[69] =	(uint8_t)( (Reserved2Bit<<6) | (Reserved2Bit<<4) | (Reserved2Bit<<2) | (Reserved2Bit) );
	
	i=0;
	if (FA_TCS.lctcsu1SymSpinDctFlg            		==1) i|=0x01;
	if (RA_TCS.lctcsu1SymSpinDctFlg         		==1) i|=0x02;
	if (FA_TCS.lctcsu1Any2HghMuWhlUnstb     		==1) i|=0x04;
	if (RA_TCS.lctcsu1Any2HghMuWhlUnstb     		==1) i|=0x08;
	if (FA_TCS.lctcsu1HghMuWhlUnstb2Any            	        ==1) i|=0x10;
	if (RA_TCS.lctcsu1HghMuWhlUnstb2Any       		==1) i|=0x20;
	if (FA_TCS.lctcsu1BrkCtlActAxle       			==1) i|=0x40;
	if (RA_TCS.lctcsu1BrkCtlActAxle               		==1) i|=0x80;
	CAN_LOG_DATA[70] =(uint8_t) (i);                 


	i=0;
	if (FL.BTCS/*FL_TCS.lctcsu1OverBrk*/          				==1) i|=0x01;
	if (FR.BTCS/*FR_TCS.lctcsu1OverBrk*/       				==1) i|=0x02; 					 
	if (RL.BTCS/*RL_TCS.lctcsu1OverBrk*/        				==1) i|=0x04;                     
	if (RR.BTCS/*RR_TCS.lctcsu1OverBrk*/              			==1) i|=0x08;                     
	if (FA_TCS.lctcsu1Any2HghMuWhlUnstb         	==1) i|=0x10;                     
	if (RA_TCS.lctcsu1Any2HghMuWhlUnstb       		==1) i|=0x20;                     
	if (FA_TCS.lctcsu1HghMuWhlUnstb2Any      		==1) i|=0x40;                     
	if (RA_TCS.lctcsu1HghMuWhlUnstb2Any         	==1) i|=0x80;                     
	CAN_LOG_DATA[71] =(uint8_t) (i);                                          
        
	/* Byte 9 */	           
	i=0;
	if (FL_TCS.lctcsu1BTCSWhlAtv          			==1) i|=0x01;
	if (FR_TCS.lctcsu1BTCSWhlAtv        			==1) i|=0x02; 					 
	if (RL_TCS.lctcsu1BTCSWhlAtv        			==1) i|=0x04;                     
	if (RR_TCS.lctcsu1BTCSWhlAtv             		==1) i|=0x08;                     
	if (FA_TCS.ltcsu1SpinDiffInc        			==1) i|=0x10;                     
	if (RA_TCS.ltcsu1SpinDiffInc       				==1) i|=0x20;                     
	if (FA_TCS.ltcsu1SpinDiffDec      				==1) i|=0x40;                     
	if (RA_TCS.ltcsu1SpinDiffDec        			==1) i|=0x80;                     
	CAN_LOG_DATA[72] =(uint8_t) (i);                               

        i=0;
        if (MPRESS_BRAKE_ON                 ==1) i|=0x01;
        if (BLS                             ==1) i|=0x02;
        if (ABS_ON                          ==1) i|=0x04;
        if (EBD_RA                          ==1) i|=0x08;
        if (BTCS_ON                         ==1) i|=0x10;
        if (ETCS_ON                         ==1) i|=0x20;
        if (FTCS_ON                         ==1) i|=0x40;
	if (fu1ESCDisabledBySW							==1) i|=0x80;
	CAN_LOG_DATA[73] =(uint8_t) (i);
        
  
        i=0;
    if (VCA_ON_TCMF          		    			==1) i|=0x01;
    if (VCA_BRAKE_CTRL_END_TCMF						==1) i|=0x02;
    if (FL_TCS.VCA_TCMF_CTL	            			==1) i|=0x04;
    if (FR_TCS.VCA_TCMF_CTL    						==1) i|=0x08;	//new
    if (RL_TCS.VCA_TCMF_CTL 						==1) i|=0x10;
    if (RR_TCS.VCA_TCMF_CTL 						==1) i|=0x20;
    if (FA_TCS.lctcsu1L2SplitSuspectDcted				==1) i|=0x40;
    if (RA_TCS.lctcsu1L2SplitSuspectDcted				==1) i|=0x80;
    CAN_LOG_DATA[74] =(uint8_t) (i);

    i=0;
    if (ENG_STALL                       			==1) i|=0x01;
    if (CYCLE_2ND                       			==1) i|=0x02;
    if (TMP_ERROR_fl                    			==1) i|=0x04;
    if (TMP_ERROR_fr                    			==1) i|=0x08;
    if (TMP_ERROR_rl                			    ==1) i|=0x10;
    if (TMP_ERROR_rr    			                ==1) i|=0x20;
    if (FA_TCS.ltcsu1ModOfAsymSpnCtl				==1) i|=0x40;
    if (RA_TCS.ltcsu1ModOfAsymSpnCtl				==1) i|=0x80;
       	
    CAN_LOG_DATA[75] =(uint8_t) (i);

        i = 0;
  //    #if (__SPLIT_TYPE==1)
    if (TCL_DEMAND_SECONDARY                       	==1) i|=0x01;
    if (TCL_DEMAND_PRIMARY                       	==1) i|=0x02;
    if (S_VALVE_SECONDARY                    		==1) i|=0x04;
    if (S_VALVE_PRIMARY                    			==1) i|=0x08;
    if (FA_TCS.ltcss8IIntAct						==1) i|=0x10;	
    if (RA_TCS.ltcss8IIntAct                    	==1) i|=0x20;	
    if (FA_TCS.lctcsu1BrkCtlFadeOutModeFlag			==1) i|=0x40;	
    if (RA_TCS.lctcsu1BrkCtlFadeOutModeFlag			==1) i|=0x80;	
//  #else
//    if (((TCL_DEMAND_rr)|(TCL_DEMAND_fl))			==1) i|=0x01;
//    if (((TCL_DEMAND_rl)|(TCL_DEMAND_fr))			==1) i|=0x02;
//    if (S_VALVE_LEFT                    			==1) i|=0x04;
//    if (S_VALVE_RIGHT                    			==1) i|=0x08;
//    if (FA_TCS.ltcss8IIntAct						==1) i|=0x10;
//    if (RA_TCS.ltcss8IIntAct                   		==1) i|=0x20;
//    if (FA_TCS.lctcsu1BrkCtlFadeOutModeFlag			==1) i|=0x40;
//    if (RA_TCS.lctcsu1BrkCtlFadeOutModeFlag			==1) i|=0x80;
//      #endif
    CAN_LOG_DATA[76] =(uint8_t) (i);                      // CH46


        i=0;
    if (AV_VL_fl                        			==1) i|=0x01;
    if (AV_VL_fr                        			==1) i|=0x02;
    if (AV_VL_rl                        			==1) i|=0x04;
    if (AV_VL_rr                        			==1) i|=0x08;
    if (HV_VL_fl                        			==1) i|=0x10;
    if (HV_VL_fr                        			==1) i|=0x20;
    if (HV_VL_rl                        			==1) i|=0x40;
    if (HV_VL_rr                        			==1) i|=0x80;
    CAN_LOG_DATA[77] =(uint8_t) (i);        	        	        	

        i = 0;
    if (AUTO_TM                         			==1) i|=0x01;
    if (YAW_CDC_WORK                    			==1) i|=0x02;
    if (ESP_TCS_ON                      			==1) i|=0x04;
    if (tc_on_flg                       			==1) i|=0x08;
    if (tc_error_flg                    			==1) i|=0x10;
    if (tcs_error_flg                   			==1) i|=0x20;
    if (fu1VoltageUnderErrDet						==1) i|=0x40;
    if (BTCS_Entry_Inhibition           			==1) i|=0x80;
    CAN_LOG_DATA[78] =(uint8_t) (i);

        i = 0;
        if (vdc_on_flg                      ==1) i|=0x01;
        if (ESP_ERROR_FLG                   ==1) i|=0x02;
        if (BLS_ERROR                       ==1) i|=0x20;
        if (Rough_road_detect_vehicle       ==1) i|=0x08;
        if (Rough_road_suspect_vehicle      ==1) i|=0x10;
    if (FA_TCS.lctcsu1FlgOfAfterHghMuUnstb			==1) i|=0x20;	
    if (RA_TCS.lctcsu1FlgOfAfterHghMuUnstb        	==1) i|=0x40;	
    if (lctcsu1DctSplitHillFlg						==1) i|=0x80;
    CAN_LOG_DATA[79] =(uint8_t) (i);
	
	#endif   /*80byte logger*/
	
}

uint16_t LSBTCS_u16PackData12_1_1_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2, uint16_t Flag3, uint16_t Flag4)
{
	uint16_t	OutputValue;
	uint16_t 	TempMem16Bits_1=0;
	uint16_t 	TempMem16Bits_2=0;	
	
	TempMem16Bits_1 = Variable2ReduceSize&(0x0FFF); 			/* 0FFF = 0000 1111 1111 1111 */
	TempMem16Bits_2 = ( (Flag4<<15)|(Flag3<<14)|(Flag2<<13)|(Flag1<<12) );
	OutputValue = (TempMem16Bits_1|TempMem16Bits_2);
	return OutputValue;
}

uint16_t LSBTCS_u16PackData12_2_2(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2)
{
	uint16_t	OutputValue;
	uint16_t 	TempMem16Bits_1=0;
	uint16_t 	TempMem16Bits_2=0;
	
	TempMem16Bits_1 = Variable2ReduceSize&(0x0FFF); 			/* 0FFF = 0000 1111 1111 1111 */
	TempMem16Bits_2 = ( (Flag2<<14)|(Flag1<<12) );
	OutputValue = (TempMem16Bits_2|TempMem16Bits_1);
	return OutputValue;
}

uint8_t LSBTCS_u8PackData4_4(uint8_t data1, uint8_t data2)
{
	uint8_t	OutputValue;
	uint8_t	TempMem8Bits_1=0;
	uint8_t	TempMem8Bits_2=0;
	
	TempMem8Bits_1 = data1&(0x0F);	/* 0F = 0000 1111 */
	TempMem8Bits_2 = data2&(0x0F);
	OutputValue = ( (TempMem8Bits_2<<4)|(TempMem8Bits_1) );
	return OutputValue;
}	

uint8_t LSBTCS_u8PackFlags(uint8_t Flag1, uint8_t Flag2, uint8_t Flag3, uint8_t Flag4, uint8_t Flag5, uint8_t Flag6, uint8_t Flag7, uint8_t Flag8)
{
	uint8_t	OutputValue;
	uint8_t	TempMem8Bits=0;
	
	TempMem8Bits = ( (Flag8<<7)|(Flag7<<6)|(Flag6<<5)|(Flag5<<4)|(Flag4<<3)|(Flag3<<2)|(Flag2<<1)|(Flag1) );
	OutputValue = TempMem8Bits;
	return OutputValue;
}	
#endif

#endif  /* __LOGGER_DATA_TYPE*/
#endif  /*__LOGGER*/
