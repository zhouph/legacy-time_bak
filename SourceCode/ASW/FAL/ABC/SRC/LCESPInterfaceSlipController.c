
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPInterfaceSlipController
	#include "Mdyn_autosar.h"
#endif
 
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"
#include "LCLVBACallControl.h"
#include "LCallMain.h"
#include "LDROPCallDetection.h"
#include "LSESPCalSlipRatio.h"
#include "LAESPActuateNcNovalveF.h"
#include "LDESPDetectVehicleStatus.h"
#include "LCESPInterfaceSlipController.h"
#include "LCABSStrokeRecovery.h"
#include "LCESPCalVlvOpenTiForComb.h"

#if __HDC
#include "LCHDCCallControl.h"
#endif
#if __ACC
#include "LCACCCallControl.h"
#endif
#if __HSA
#include "LCHSACallControl.h"
#endif
#if __EPB_INTERFACE
#include "LCEPBCallControl.h"
#endif
#if __FBC
#include "LCFBCCallControl.h"
#endif
#if __DEC
#include "LCDECCallControl.h"
#endif
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif
#if (__ESC_TCMF_CONTROL ==1)
#include"LCESPInterfaceTCMF.h"
#include"LAESPActuateTCMFESC.h"
#endif

/********************************************/    
 
/*************** Parameter declare *********************/
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t rate_counter_front;
//int16_t rate_counter_rear;
//int16_t rate_counter_fl;
//int16_t rate_counter_fr;
//int16_t rate_counter_rl;
//int16_t rate_counter_rr;
//int16_t slip_control_rear;
//int16_t slip_control_P_gain_rear;
//int16_t slip_control_D_gain_rear;
//int16_t slip_control_front;
//int16_t slip_control_P_gain_front;
//int16_t slip_control_D_gain_front;
//int16_t slip_error_dot_prev_fl;
//int16_t slip_error_dot_prev_fr;
//int16_t slip_error_dot_prev_rl;
//int16_t slip_error_dot_prev_rr;
//int16_t slip_error_dot_prev_front;
//int16_t slip_error_dot_prev_rear;
//int16_t slip_error_prev_fl;
//int16_t slip_error_prev_fr;
//int16_t slip_error_prev_rl;
//int16_t slip_error_prev_rr;
//int16_t rate_interval_front;
//int16_t rate_interval_rear;
//int16_t rate_interval_fl;
//int16_t rate_interval_fr;
//int16_t rate_interval_rl;
//int16_t rate_interval_rr;
//int16_t slip_measurement_front;
//int16_t slip_measurement_rear;
//int16_t slip_target_front;
//int16_t slip_target_rear;
//int16_t slip_measurement_front_prev;
//int16_t slip_measurement_rear_prev;
//int16_t slip_error_front;
//int16_t slip_error_rear;
//int16_t slip_error_dot_front;
//int16_t slip_error_dot_rear;
//int16_t slip_control_front_FL; 
//int16_t slip_control_front_FR;
#else
int16_t rate_counter_front;
int16_t rate_counter_rear;
int16_t rate_counter_fl;
int16_t rate_counter_fr;
int16_t rate_counter_rl;
int16_t rate_counter_rr;
int16_t slip_control_rear;
int16_t slip_control_P_gain_rear;
int16_t slip_control_D_gain_rear;
int16_t slip_control_front;
int16_t slip_control_P_gain_front;
int16_t slip_control_D_gain_front;
int16_t slip_error_dot_prev_fl;
int16_t slip_error_dot_prev_fr;
int16_t slip_error_dot_prev_rl;
int16_t slip_error_dot_prev_rr;
int16_t slip_error_dot_prev_front;
int16_t slip_error_dot_prev_rear;
int16_t slip_error_prev_fl;
int16_t slip_error_prev_fr;
int16_t slip_error_prev_rl;
int16_t slip_error_prev_rr;
int16_t rate_interval_front;
int16_t rate_interval_rear;
int16_t rate_interval_fl;
int16_t rate_interval_fr;
int16_t rate_interval_rl;
int16_t rate_interval_rr;
int16_t slip_measurement_front;
int16_t slip_measurement_rear;
int16_t slip_target_front;
int16_t slip_target_rear;
int16_t slip_measurement_front_prev;
int16_t slip_measurement_rear_prev;
int16_t slip_error_front;
int16_t slip_error_rear;
int16_t slip_error_dot_front;
int16_t slip_error_dot_rear;
int16_t slip_control_front_FL; 
int16_t slip_control_front_FR; 
#endif

int16_t gain_speed_slip_depend;
int16_t slip_target_f_offset;
int16_t slip_target_r_offset;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t slip_target_prev_front;
//int16_t slip_target_prev_rear;
//int16_t slip_target_prev_fl;
//int16_t slip_target_prev_fr;
//int16_t slip_target_prev_rl;
//int16_t slip_target_prev_rr;
#else
int16_t slip_target_prev_front;
int16_t slip_target_prev_rear;
int16_t slip_target_prev_fl;
int16_t slip_target_prev_fr;
int16_t slip_target_prev_rl;
int16_t slip_target_prev_rr;
#endif

int16_t slip_target_prev;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t slip_error_prev_front;
//int16_t slip_error_prev_rear;
//int16_t slip_error_prev_fl;
//int16_t slip_error_prev_fr;
//int16_t slip_error_prev_rl;
//int16_t slip_error_prev_rr;
#else
int16_t slip_error_prev_front;
int16_t slip_error_prev_rear;
int16_t slip_error_prev_fl;
int16_t slip_error_prev_fr;
int16_t slip_error_prev_rl;
int16_t slip_error_prev_rr;
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t true_slip_target_fl, true_slip_target_fr;
//int16_t true_slip_target_rl, true_slip_target_rr;
#else
int16_t true_slip_target_fl, true_slip_target_fr;
int16_t true_slip_target_rl, true_slip_target_rr;
#endif

int16_t slip_m_fl_current_old, slip_m_fl_current;
int16_t slip_m_fr_current_old, slip_m_fr_current;
int16_t slip_m_rl_current_old, slip_m_rl_current;
int16_t slip_m_rr_current_old, slip_m_rr_current;

int16_t del_speed_fl_cur, del_speed_fl_cur_old;
int16_t del_speed_fr_cur, del_speed_fr_cur_old;
int16_t del_speed_rl_cur, del_speed_rl_cur_old;
int16_t del_speed_rr_cur, del_speed_rr_cur_old;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t slip_control_need_counter_fl;
//int16_t slip_control_need_counter_fr;
//int16_t slip_control_need_counter_rl;
//int16_t slip_control_need_counter_rr;
//int16_t esp_pressure_count_fl;
//int16_t esp_pressure_count_fr;
//int16_t esp_pressure_count_rl;
//int16_t esp_pressure_count_rr;
#else
int16_t slip_control_need_counter_fl;
int16_t slip_control_need_counter_fr;
int16_t slip_control_need_counter_rl;
int16_t slip_control_need_counter_rr;
int16_t esp_pressure_count_fl;
int16_t esp_pressure_count_fr;
int16_t esp_pressure_count_rl;
int16_t esp_pressure_count_rr;
#endif

int16_t esp_pulse_down_dump_rate_confirm;
int16_t esp_pulse_down_dump_max_slip;
int16_t variable_rise_scan;
int16_t variable_rise_scan_rear;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//uint8_t hold_counter_fl,hold_counter_fr, slip_m_fl_lock_cont, slip_m_fr_lock_cont;
//uint8_t hold_counter_rl,hold_counter_rr, slip_m_rl_lock_cont, slip_m_rr_lock_cont;
//int16_t slip_r_rise_thr_change;
//int16_t slip_f_rise_thr_change;
#else
uint8_t hold_counter_fl,hold_counter_fr, slip_m_fl_lock_cont, slip_m_fr_lock_cont;
uint8_t hold_counter_rl,hold_counter_rr, slip_m_rl_lock_cont, slip_m_rr_lock_cont;

int16_t slip_r_rise_thr_change;
int16_t slip_f_rise_thr_change;
#endif

int16_t esp_pressure_count_fl_old;
int16_t esp_pressure_count_fr_old;
int16_t esp_pressure_count_rl_old;
int16_t esp_pressure_count_rr_old;

int16_t pulse_dump_time_front_fl;
int16_t pulse_dump_time_front_fr;
int16_t pulse_dump_time_rear_rl;
int16_t pulse_dump_time_rear_rr;
int16_t new_pulse_down_rate_fl;
int16_t new_pulse_down_rate_fr;
int16_t new_pulse_down_rate_rl;
int16_t new_pulse_down_rate_rr;
int16_t tc_close_time_fl;
int16_t tc_close_time_fr;
int16_t tc_close_time_rl;
int16_t tc_close_time_rr;

int16_t index_dump_rate_fl;
int16_t index_dump_rate_fr;
int16_t index_dump_rate_rl;
int16_t index_dump_rate_rr;
int16_t index_tc_close_time_fl;
int16_t index_tc_close_time_fr;
int16_t index_tc_close_time_rl;
int16_t index_tc_close_time_rr;
int16_t esp_pulse_down_dump_rate_confirm_fl;
int16_t esp_pulse_down_dump_rate_confirm_fr;
int16_t esp_pulse_down_dump_rate_confirm_rl;
int16_t esp_pulse_down_dump_rate_confirm_rr;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t slip_f_dump_thr_change;
//int16_t slip_r_dump_thr_change;
#else
int16_t slip_f_dump_thr_change;
int16_t slip_r_dump_thr_change; 
#endif
 
int16_t slip_m_fl_drive, slip_m_fr_drive, slip_m_rl_drive, slip_m_rr_drive; 
int16_t slip_m_fl_drive_old, slip_m_fr_drive_old, slip_m_rl_drive_old, slip_m_rr_drive_old;

int16_t lcs16espSlipratearrayfl[4];
int16_t lcs16espSlipratearrayfr[4];
int16_t lcs16espSlipratearrayrl[4];
int16_t lcs16espSlipratearrayrr[4];

int16_t lcs16espSlipratefl;
int16_t lcs16espSliprateflold;

int16_t lcs16espSlipratefr;
int16_t lcs16espSlipratefrold;

int16_t lcs16espSlipraterl;
int16_t lcs16espSlipraterlold;

int16_t lcs16espSlipraterr;
int16_t lcs16espSlipraterrold;

int16_t max_pulse_down_rate_fl;
int16_t max_pulse_down_rate_fr;
int16_t max_pulse_down_rate_rl;
int16_t max_pulse_down_rate_rr;

/*  CRAM  */
int16_t slip_status_position ;

  
/*********** Parameter Deifne **********************/

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t lcescs16CalDelTargetSlipf; 
//int16_t lcescs16CalDelTargetSlipr; 
//int16_t del_slip_gain_front; 
//int16_t del_wheel_sp_gain_front; 
//int16_t del_slip_gain_rear; 
//int16_t del_wheel_sp_gain_rear;
#else
int16_t lcescs16CalDelTargetSlipf; 
int16_t lcescs16CalDelTargetSlipr; 

int16_t del_slip_gain_front; 
int16_t del_wheel_sp_gain_front; 
int16_t del_slip_gain_rear; 
int16_t del_wheel_sp_gain_rear; 
#endif

int16_t gain_speed_whrate_depend;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t lcescCalRiserateCountdepDelPF;  
//int16_t lcescCalRiserateCountdepDelPfl_old;
#else
int16_t lcescCalRiserateCountdepDelPF;  
int16_t lcescCalRiserateCountdepDelPfl_old;
#endif

int16_t lcespCalCountfl;
int16_t lcespCalCountfl_old;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t lcespCalRiserateAddDutyfl;
//int16_t lcespCalRiserateAddDutyfl_old;
//int16_t lcespCaldelDutyfl;
//int16_t lcescCalRiserateCountdepDelPfr_old;
#else
int16_t lcespCalRiserateAddDutyfl;
int16_t lcespCalRiserateAddDutyfl_old;
int16_t lcespCaldelDutyfl;
int16_t lcescCalRiserateCountdepDelPfr_old;
#endif

int16_t lcespCalCountfr;
int16_t lcespCalCountfr_old;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t lcespCalRiserateAddDutyfr;
//int16_t lcespCalRiserateAddDutyfr_old;
//int16_t lcespCaldelDutyfr;
#else
int16_t lcespCalRiserateAddDutyfr;
int16_t lcespCalRiserateAddDutyfr_old;
int16_t lcespCaldelDutyfr;
#endif

int16_t lcesps16EstPressFromDelM_FL;
int16_t lcesps16EstPressFromDelM_FR;
int16_t lcesps16EstPressFromDelM_RL;
int16_t lcesps16EstPressFromDelM_RR;
int16_t	lcesps16KeepEstPress;
int16_t lcesps16SlipThreshold;

#if (__IDB_LOGIC == ENABLE)
U8_BIT_STRUCT_t IdbEscSTATE_0;
int16_t lcidbs16activebrakepress;
int8_t u8EscStrokeState;
int8_t u8EscStrokeState_old;     
int16_t lcesps16AllowFnlStrRetime;
#endif
#if __VDC
void LCESP_vInterfaceSlipController(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vSlipControlFL(void);
void LCESP_vSlipControlFR(void);
void LCESP_vSlipControlRL(void);
void LCESP_vSlipControlRR(void);
void LCESP_vCalSlipErrorF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalSlipErrorR(struct ESC_STRUCT *WL_ESC);
	#if  (__IDB_LOGIC == ENABLE)
void LCESP_vCalFrtRiseIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);    
void LCESP_vCalFrtDumpIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);    
void LCESP_vCalFrtdelTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalRearRiseIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);    
void LCESP_vCalRearDumpIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);    
void LCESP_vCalReardelTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vDctBBSTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vMaxLimitFrtTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vMaxLimitRearTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vMinLimitFrtTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vMinLimitRearTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalCurrentPresssforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalTargetPressforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalTargetPressComp(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalTargetPressRate(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vCalDelTargetPress(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalStorkeRecorverytime(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalInletValveforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESP_vDetStorkeRecorveryforESC(struct ESC_STRUCT *WL_ESC);
	#endif
#else
void LCESP_vResetVariable(void);
void LCESP_vSlipControlF(void);
void LCESP_vSlipControlR(void);
void LCESP_vCalSlipErrorF(void);
void LCESP_vCalSlipErrorR(void);
#endif

void LCESP_vCalSpeedSlipGain(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalSlipSignalF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalSlipSignalR(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalPressureRateF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalPressureRateR(struct ESC_STRUCT *WL_ESC);
#else
void LCESP_vCalSlipSignalF(void);
void LCESP_vCalSlipSignalR(void);
void LCESP_vCalPressureRateF(void);
void LCESP_vCalPressureRateR(void);
#endif

void LCESP_vRememberCurrentSlip(void);
void LCESP_vAddTargetSlip(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vDetectCompulsionRise(struct ESC_STRUCT *WL_ESC);
void LCESP_vDetectCompulsionHoldFL(void);
void LCESP_vDetectCompulsionHoldFR(void);
void LCESP_vSetCompulsionHoldF(struct ESC_STRUCT *WL_ESC);
void LCESP_vDetectCompulsionHoldRL(void);
void LCESP_vDetectCompulsionHoldRR(void);
void LCESP_vSetCompulsionHoldR(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalVariableThresholdF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalVariableThresholdR(struct ESC_STRUCT *WL_ESC);
void LCESP_vCountPressureF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCountPressureR(struct ESC_STRUCT *WL_ESC);
void LCESP_vCountLockF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCountLockR(struct ESC_STRUCT *WL_ESC);
#else
void LCESP_vDetectCompulsionRise(void);
void LCESP_vDetectCompulsionHoldF(void);
void LCESP_vDetectCompulsionHoldR(void);
void LCESP_vCalVariableThresholdF(void);
void LCESP_vCalVariableThresholdR(void);
void LCESP_vCountPressureF(void);
void LCESP_vCountPressureR(void);
void LCESP_vCountLockF(void);
void LCESP_vCountLockR(void);
#endif

void LAESP_vCalMototTime(void);
void LAESP_vResetTempValve(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LAESP_vActuateNcNovalveF(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LAESP_vActuateNcNovalveR(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
#else
void LAESP_vActuateNcNovalveF(void);
void LAESP_vActuateNcNovalveR(void);
#endif

/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
void LAESP_vActXsplitEsvTcvalve(void);
#else
void LAESP_vActFRsplitEsvTcvalve(void);
#endif
/*060607 modified and added for FR Split by eslim*/  
void LAESP_vCalNcDutyInEsp(void);  
/* #if __PWM 061113 deleted by eslim */

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
    void LCESP_vCheckPressureCycleR(void);
    void LCESP_vCheckPressureCycleF(void);
#endif

    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    void LCESP_vDetectFullRise(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);
#else
    void LCESP_vDetectFullRise(void);
#endif

/* #endif 061113 deleted by eslim */
#if ENABLE_PULSE_DOWN_DUMP   
	void LCESP_vCalSliprRatioRate(void);
	void LAESP_vActDumpAtEndEsp(void);
	void LCESP_vSetPulseDumpFront(void);
	void LCESP_vSetPulseDumpRear(void);
	void LAESP_vCalNcDutyAtEndEsp(void);
#endif

/* 09 SWD */

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vDetectNullDump(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);
#else
void LCESP_vDetectNullDump(void);
#endif

#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//void LSESP_vCompWheelSlip(void);
#else
void LSESP_vCompWheelSlip(void); //삭제 필요
#endif

void LCESP_vFindSpeedDependantGain( void );
void LCESC_vCalFadeoutTime(void);
void LCESC_vSetFadeoutControlFront(void);
void LCESC_vSetFadeoutControlRear(void);
	#if  (__IDB_LOGIC == ENABLE)
void LCESC_vCalFadeoutFrontTPforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LCESC_vCalFadeoutRearTPforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
	#endif
void LCESC_vCalFadeoutDumprateFront(void);
void LCESC_vCalFadeoutDumprateRear(void);
void LCESC_vCalFadeoutTctime(void);
extern void LAESP_vActFadeout(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalSlipGainPressureF(struct ESC_STRUCT *WL_ESC);
void LCESP_vCalSlipGainPressureR(struct ESC_STRUCT *WL_ESC);
#else
void LCESP_vCalSlipGainPressureF(void);
void LCESP_vCalSlipGainPressureR(void);
#endif

void LCESP_vCalSpeedWHRateGain(void);

#if ESC_PULSE_UP_RISE==1
  #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    void LCESP_vCalVariableRiseRateF(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
  #else
void LCESP_vCalVariableRiseRateF(void);
#endif
#endif

#if (__ESC_TCMF_CONTROL ==1)
void LCESP_vInterfaceTCMFControlF(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);
void LCESP_vInterfaceTCMFControlR(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL);
#endif


#if __VDC
void LCESP_vRememberCurrentSlip(void)
{
    slip_m_fl_current_old = slip_m_fl_current;
    slip_m_fr_current_old = slip_m_fr_current;
    slip_m_rl_current_old = slip_m_rl_current;
    slip_m_rr_current_old = slip_m_rr_current;
    
    if(slip_m_fl <= 0 )
    {
    	slip_m_fl_current = slip_m_fl;
    }
    else 
    {
    	slip_m_fl_current = 0;
    }

    if(slip_m_fr <= 0 ) 
    {
    	slip_m_fr_current = slip_m_fr;
    }
    else 
    {
    	slip_m_fr_current = 0;
    }  

    if(slip_m_rl <= 0 ) 
    {
    	slip_m_rl_current = slip_m_rl;
    }
    else 
    {
    	slip_m_rl_current = 0;
    }
    if(slip_m_rr <= 0 )     
    {
       	slip_m_rr_current = slip_m_rr;
    }
    else 
    {
      	slip_m_rr_current = 0;
    }    

    if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FL_old==1)||(ABS_fz==1)) 
    {
    	slip_m_fl_current = LCESP_s16Lpf1Int(slip_m_fl_current,  slip_m_fl_current_old, 1); 
    }
    else 
    {
    	slip_m_fl_current = slip_m_fl_current; 
    }
    
    if((SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FR_old==1)||(ABS_fz==1)) 
    {
    	slip_m_fr_current = LCESP_s16Lpf1Int(slip_m_fr_current,  slip_m_fr_current_old, 1); 
    }
    else 
    {
    	slip_m_fr_current = slip_m_fr_current; 
    }
    
    if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RL_old==1)||(ABS_fz==1)) 
    {
    	slip_m_rl_current = LCESP_s16Lpf1Int(slip_m_rl_current,  slip_m_rl_current_old, 1); 
    }
    else 
    {
    	slip_m_rl_current = slip_m_rl_current; 
    }
    
    if((SLIP_CONTROL_NEED_RR==1)||(SLIP_CONTROL_NEED_RR_old==1)||(ABS_fz==1)) 
    {
    	slip_m_rr_current = LCESP_s16Lpf1Int(slip_m_rr_current,  slip_m_rr_current_old, 1);
	}
    else 
    {
    	slip_m_rr_current = slip_m_rr_current; 
    }
}


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vAddTargetSlip(void)
{
    FL_ESC.true_slip_target     = FL_ESC.slip_target - ((McrAbs(slip_m_fl_current)>>6)<<6);
    FR_ESC.true_slip_target     = FR_ESC.slip_target - ((McrAbs(slip_m_fr_current)>>6)<<6);
    RL_ESC.true_slip_target     = RL_ESC.slip_target - ((McrAbs(slip_m_rl_current)>>6)<<6); 
    RR_ESC.true_slip_target     = RR_ESC.slip_target - ((McrAbs(slip_m_rr_current)>>6)<<6);
    
    if(FL_ESC.true_slip_target < (-WHEEL_SLIP_40))
    {
    	FL_ESC.true_slip_target = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }
    if(FR_ESC.true_slip_target < (-WHEEL_SLIP_40))
    {
    	FR_ESC.true_slip_target = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }
    if(RL_ESC.true_slip_target < (-WHEEL_SLIP_40))
    {
    	RL_ESC.true_slip_target = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }    
    if(RR_ESC.true_slip_target < (-WHEEL_SLIP_40))
    {
    	RR_ESC.true_slip_target = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }   
}
#else
void LCESP_vAddTargetSlip(void)
{
    true_slip_target_fl     = slip_target_fl - ((McrAbs(slip_m_fl_current)>>6)<<6);
    true_slip_target_fr     = slip_target_fr - ((McrAbs(slip_m_fr_current)>>6)<<6);
    true_slip_target_rl     = slip_target_rl - ((McrAbs(slip_m_rl_current)>>6)<<6); 
    true_slip_target_rr     = slip_target_rr - ((McrAbs(slip_m_rr_current)>>6)<<6);
    
    if(true_slip_target_fl < (-WHEEL_SLIP_40))
    {
    	true_slip_target_fl = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }
    if(true_slip_target_fr < (-WHEEL_SLIP_40))
    {
    	true_slip_target_fr = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }
    if(true_slip_target_rl < (-WHEEL_SLIP_40))
    {
    	true_slip_target_rl = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }    
    if(true_slip_target_rr < (-WHEEL_SLIP_40))
    {
    	true_slip_target_rr = -WHEEL_SLIP_40 ; 
    }
    else
    {
    	;	
    }   
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vDetectCompulsionRise(struct ESC_STRUCT *WL_ESC)
{
    if(ABS_fz==1)
    { 
        if(((WL_ESC->SLIP_CONTROL_NEED==1)&&(moment_q_front<(-40))&&(WL_ESC->slip_error_dot<(-150)))
            ||((WL_ESC->SLIP_CONTROL_NEED==1)&&(vrefk<400)&&(moment_q_front<(-40))&&(WL_ESC->slip_m>(-500))&&(WL_ESC->slip_error_dot<0)))
        {
             WL_ESC->FLAG_COMPULSION_RISE = 1;
        }
        else 
        {
        	WL_ESC->FLAG_COMPULSION_RISE = 0;
        }
    }
    else 
    {
    	WL_ESC->FLAG_COMPULSION_RISE=0;
    }
}
#else
void LCESP_vDetectCompulsionRise(void)
{
    if(ABS_fz==1)
    { 
        if(((SLIP_CONTROL_NEED_FL==1)&&(moment_q_front<(-40))&&(slip_error_dot_front<(-150)))
            ||((SLIP_CONTROL_NEED_FL==1)&&(vrefk<400)&&(moment_q_front<(-40))&&(slip_m_fl>(-500))&&(slip_error_dot_front<0)))
        {
             FLAG_COMPULSION_RISE = 1;
        }
        else if(((SLIP_CONTROL_NEED_FR==1)&&(moment_q_front<(-40))&&(slip_error_dot_front<(-150)))
            ||((SLIP_CONTROL_NEED_FR==1)&&(vrefk<400)&&(moment_q_front<(-40))&&(slip_m_fr>(-500))&&(slip_error_dot_front<0)))
        {
            FLAG_COMPULSION_RISE = 1;
        }
        else 
        {
        	FLAG_COMPULSION_RISE = 0;
        }
    }
    else 
    {
    	FLAG_COMPULSION_RISE=0;
    }
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalVariableThresholdF(struct ESC_STRUCT *WL_ESC)
{	
	esp_tempW0 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_RISE_THRESHOLD, SLIP_F_RISE_THRESHOLD_M, SLIP_F_RISE_THRESHOLD_L );	
	esp_tempW9 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD, SLIP_F_DUMP_THRESHOLD_M, SLIP_F_DUMP_THRESHOLD_L );	
	esp_tempW8 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_HOLD_THRESHOLD, SLIP_F_HOLD_THRESHOLD_M, SLIP_F_HOLD_THRESHOLD_L );
#if(__IDB_LOGIC==1)	
	if(lcu1US2WHControlFlag == 1)
    {
        esp_tempW0 = -100 ;
        esp_tempW9 = - 50 ;
        esp_tempW8 = - 70 ;
    }          
	else
	{
	    ;
	}
#endif   
	
    #if ( __Ext_Under_Ctrl == 1 )
    
        if (EXTREME_UNDER_CONTROL == 1) 
        {
            esp_tempW0 = -100 ;
            esp_tempW9 = - 50 ;
            esp_tempW8 = - 70 ;
        }
        else
        {
            ;
        }
    #endif            
	
	
	if(WL_ESC->SLIP_CONTROL_NEED==1)
	{				
		if((WL_ESC->FLAG_1ST_CYCLE==1)||(WL_ESC->FLAG_1ST_CYCLE_WHEEL ==1))
		{
			WL_ESC->slip_rise_thr_change = esp_tempW0;
			
			if(WL_ESC->slip_rise_thr_change < esp_tempW0 )
			{
				WL_ESC->slip_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}
		}
		else if(WL_ESC->lcescs16CalWheelSpeedRate > 50)				/* 08.02.09  SWD  YJH  lcescs16CalDelWheelSpeedRatefl -> lcescs16CalWheelSpeedRatefl */
		{
			if(ESP_ROUGH_ROAD==1)
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD/2;
			}
			else 
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD;
			}
			
			WL_ESC->slip_rise_thr_change = esp_tempW0 + esp_tempW1;		
		}
		else if((WL_ESC->lcescs16CalDelTargetSlip > 0)&&(WL_ESC->lcescs16CalWheelSpeedRate > 0))				/* 08.02.09  SWD  YJH  */
		{
			WL_ESC->slip_rise_thr_change = esp_tempW8;
		}
		else
		{
			WL_ESC->slip_rise_thr_change = esp_tempW0;
		}		
	}	

		WL_ESC->slip_dump_thr_change = esp_tempW9;	
		
		#if(__ESC_TCMF_CONTROL ==1)		
		esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_MAX_RISE_THR_HM
						                                 , S16_TCMF_MAX_RISE_THR_MM
						                                 , S16_TCMF_MAX_RISE_THR_LM);
		
		esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_DUMP_FAST_DEC_TH_HM
						                                 , S16_TCMF_DUMP_FAST_DEC_TH_MM
						                                 , S16_TCMF_DUMP_FAST_DEC_TH_LM);
		
		WL_ESC->lcesps16TCMFMaxRiseThr = WL_ESC->slip_rise_thr_change - esp_tempW3;
		WL_ESC->lcesps16TCMFFastDumpThr = WL_ESC->slip_dump_thr_change + esp_tempW5;
		
		esp_tempW7 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_RISE_STEP_THR_R3_HM
						                                 , S16_TCMF_RISE_STEP_THR_R3_MM
						                                 , S16_TCMF_RISE_STEP_THR_R3_LM);
		
		esp_tempW9 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_RISE_STEP_THR_R2_HM
						                                 , S16_TCMF_RISE_STEP_THR_R2_MM
						                                 , S16_TCMF_RISE_STEP_THR_R2_LM);
		
		WL_ESC->lcesps16TCMFStepRiseThrReg3 = WL_ESC->slip_rise_thr_change - esp_tempW7;
		WL_ESC->lcesps16TCMFStepRiseThrReg2 = WL_ESC->slip_rise_thr_change - esp_tempW9;
		
		//#endif
				
		#endif
}
#else
void LCESP_vCalVariableThresholdF(void)
{	
	esp_tempW0 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_RISE_THRESHOLD, SLIP_F_RISE_THRESHOLD_M, SLIP_F_RISE_THRESHOLD_L );	
	esp_tempW9 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD, SLIP_F_DUMP_THRESHOLD_M, SLIP_F_DUMP_THRESHOLD_L );	
	esp_tempW8 = - LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_HOLD_THRESHOLD, SLIP_F_HOLD_THRESHOLD_M, SLIP_F_HOLD_THRESHOLD_L );
	
    #if ( __Ext_Under_Ctrl == 1 )
    
        if (EXTREME_UNDER_CONTROL == 1) 
        {
            esp_tempW0 = -100 ;
            esp_tempW9 = - 50 ;
            esp_tempW8 = - 70 ;
        }
        else
        {
            ;
        }
    #endif            
	
	
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_status_position==1))
	{				
		if((FLAG_1ST_CYCLE_FL==1)||(FLAG_1ST_CYCLE_WHEEL_FL ==1))
		{
			slip_f_rise_thr_change = esp_tempW0;
			
			if(slip_f_rise_thr_change < esp_tempW0 )
			{
				slip_f_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}
		}
		else if(lcescs16CalWheelSpeedRatefl > 50)				/* 08.02.09  SWD  YJH  lcescs16CalDelWheelSpeedRatefl -> lcescs16CalWheelSpeedRatefl */
		{
			if(ESP_ROUGH_ROAD==1)
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD/2;
			}
			else 
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD;
			}
			
			slip_f_rise_thr_change = esp_tempW0 + esp_tempW1;		
		}
		else if((lcescs16CalDelTargetSlipf > 0)&&(lcescs16CalWheelSpeedRatefl > 0))				/* 08.02.09  SWD  YJH  */
		{
			slip_f_rise_thr_change = esp_tempW8;
		}
		else
		{
			slip_f_rise_thr_change = esp_tempW0;
		}		
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_status_position==2))				/* 08.02.09  SWD  */
	{		
		if((FLAG_1ST_CYCLE_FR==1)||(FLAG_1ST_CYCLE_WHEEL_FR==1))
		{			
			slip_f_rise_thr_change = esp_tempW0;
			
			if(slip_f_rise_thr_change < esp_tempW0 )
			{
				slip_f_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}
		}
		else if(lcescs16CalWheelSpeedRatefr > 50)				/* 08.02.09  SWD  YJH  */
		{
			if(ESP_ROUGH_ROAD==1)
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD/2;
			}
			else 
			{
				esp_tempW1 = S16_SLIP_F_RISE_TH_ADD;
			}
			
			slip_f_rise_thr_change = esp_tempW0 + esp_tempW1;		
		}
		else if((lcescs16CalDelTargetSlipf > 0)&&(lcescs16CalWheelSpeedRatefr > 0))				/* 08.02.09  SWD  YJH  */
		{
			slip_f_rise_thr_change = esp_tempW8;
		}
		else
		{
			slip_f_rise_thr_change = esp_tempW0;
		}		
	}
	else
	{
		slip_f_rise_thr_change = esp_tempW0;
	}
	
	
	if((SLIP_CONTROL_NEED_FL)&&(slip_status_position==1))
	{
		slip_f_dump_thr_change = esp_tempW9;		
	}
	else if((SLIP_CONTROL_NEED_FR)&&(slip_status_position==2))
	{
		slip_f_dump_thr_change = esp_tempW9;
	}
	else
	{
		slip_f_dump_thr_change = esp_tempW9;	
	}
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalVariableThresholdR(struct ESC_STRUCT *WL_ESC)
{
	esp_tempW0 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_RISE_THRESHOLD, SLIP_R_RISE_THRESHOLD_M, SLIP_R_RISE_THRESHOLD_L );	
	esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD, SLIP_R_DUMP_THRESHOLD_M, SLIP_R_DUMP_THRESHOLD_L );	
	esp_tempW8 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_HOLD_THRESHOLD, SLIP_R_HOLD_THRESHOLD_M, SLIP_R_HOLD_THRESHOLD_L );

	if(WL_ESC->FLAG_COMPULSION_HOLD==1)
	{
		WL_ESC->slip_rise_thr_change = esp_tempW0-S16_SLIP_R_RISE_TH_ADD;
	}
	else if(WL_ESC->SLIP_CONTROL_NEED==1)
	{		
		if((WL_ESC->FLAG_1ST_CYCLE==1)||(WL_ESC->FLAG_1ST_CYCLE_WHEEL==1))
		{
			WL_ESC->slip_rise_thr_change = esp_tempW0;
			
			if(WL_ESC->slip_rise_thr_change < esp_tempW0 )
			{
				WL_ESC->slip_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}			
		}
		else if((WL_ESC->lcescs16CalDelTargetSlip > 0)&&(WL_ESC->lcescs16CalWheelSpeedRate > 0))
		{
			WL_ESC->slip_rise_thr_change = esp_tempW8;
		}
		else if(WL_ESC->lcescs16CalWheelSpeedRate > 50)
		{
			WL_ESC->slip_rise_thr_change = esp_tempW0+S16_SLIP_R_RISE_TH_ADD;		
			
		}
		else
		{
			WL_ESC->slip_rise_thr_change = esp_tempW0;
		}		
	}
	else
	{
		WL_ESC->slip_rise_thr_change = esp_tempW0;				
	}
	
	WL_ESC->slip_dump_thr_change = esp_tempW2;

}
#else
void LCESP_vCalVariableThresholdR(void)
{
	esp_tempW0 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_RISE_THRESHOLD, SLIP_R_RISE_THRESHOLD_M, SLIP_R_RISE_THRESHOLD_L );	
	esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD, SLIP_R_DUMP_THRESHOLD_M, SLIP_R_DUMP_THRESHOLD_L );	
	esp_tempW8 = -LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_HOLD_THRESHOLD, SLIP_R_HOLD_THRESHOLD_M, SLIP_R_HOLD_THRESHOLD_L );

	if(FLAG_COMPULSION_HOLD_REAR==1)
	{
		slip_r_rise_thr_change = esp_tempW0-S16_SLIP_R_RISE_TH_ADD;
	}
	else if((SLIP_CONTROL_NEED_RL==1)&&(slip_status_position==3))
	{		
	
		if((FLAG_1ST_CYCLE_RL==1)||(FLAG_1ST_CYCLE_WHEEL_RL==1))
		{
			slip_r_rise_thr_change = esp_tempW0;
			
			if(slip_r_rise_thr_change < esp_tempW0 )
			{
				slip_r_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}			
		}
		else if((lcescs16CalDelTargetSlipr > 0)&&(lcescs16CalWheelSpeedRaterl > 0))
		{
			slip_r_rise_thr_change = esp_tempW8;
		}
		else if(lcescs16CalWheelSpeedRaterl > 50)
		{
			slip_r_rise_thr_change = esp_tempW0+S16_SLIP_R_RISE_TH_ADD;		
			
		}
		else
		{
			slip_r_rise_thr_change = esp_tempW0;
		}		
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(slip_status_position==4))
	{
	
		
		if((FLAG_1ST_CYCLE_RR==1)||(FLAG_1ST_CYCLE_WHEEL_RR==1))
		{
			slip_r_rise_thr_change = esp_tempW0;
			
			if(slip_r_rise_thr_change < esp_tempW0 )
			{
				slip_r_rise_thr_change = esp_tempW0;
			}
			else
			{
				;
			}			
		}
		else if((lcescs16CalDelTargetSlipr > 0)&&(lcescs16CalWheelSpeedRaterr > 0))
		{
			slip_r_rise_thr_change = esp_tempW8;
		}
		else if(lcescs16CalWheelSpeedRaterr > 50)
		{
			slip_r_rise_thr_change = esp_tempW0+S16_SLIP_R_RISE_TH_ADD;		
			
		}
		else
		{
			slip_r_rise_thr_change = esp_tempW0;
		}		
	}
	else
	{
		slip_r_rise_thr_change = esp_tempW0;				
	}
	
	slip_r_dump_thr_change = esp_tempW2;

}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCountLockF(struct ESC_STRUCT *WL_ESC)
{ 
	if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m<= -WHEEL_SLIP_80))
	{		
		WL_ESC->slip_m_lock_cont =WL_ESC->slip_m_lock_cont+1;
	}
	else if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m>= -WHEEL_SLIP_80)&&(WL_ESC->slip_m <= -WHEEL_SLIP_70))
	{
		WL_ESC->slip_m_lock_cont = WL_ESC->slip_m_lock_cont;
	}
	else
	{
		WL_ESC->slip_m_lock_cont =0;
	}
}
#else
void LCESP_vCountLockF(void)
{ 
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
/***************FL***************/
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_status_position!=1))
	{
		;
	}
	else if((SLIP_CONTROL_NEED_FL==1)&&(slip_m_fl<= -WHEEL_SLIP_80)&&(slip_status_position==1))
	{		
		slip_m_fl_lock_cont =slip_m_fl_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_FL==1)&&(slip_m_fl>= -WHEEL_SLIP_80)&&(slip_m_fl <= -WHEEL_SLIP_70)&&(slip_status_position==1))
	{
		slip_m_fl_lock_cont = slip_m_fl_lock_cont;
	}
	else
	{
		slip_m_fl_lock_cont =0;
	}
	
/***************FR**************/		
	if((SLIP_CONTROL_NEED_FR==1)&&(slip_status_position!=2))
	{
		;
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_m_fr<= -WHEEL_SLIP_80)&&(slip_status_position==2))
	{		
		slip_m_fr_lock_cont =slip_m_fr_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_m_fr>= -WHEEL_SLIP_80)&&(slip_m_fr <= -WHEEL_SLIP_70)&&(slip_status_position==2))
	{
		slip_m_fr_lock_cont = slip_m_fr_lock_cont;
	}
	else 
	{
		slip_m_fr_lock_cont =0;	
	}
#else
/***************FL***************/
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_m_fl<= -WHEEL_SLIP_80))
	{		
		slip_m_fl_lock_cont =slip_m_fl_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_FL==1)&&(slip_m_fl>= -WHEEL_SLIP_80)&&(slip_m_fl <= -WHEEL_SLIP_70))
	{
		slip_m_fl_lock_cont = slip_m_fl_lock_cont;
	}
	else
	{
		slip_m_fl_lock_cont =0;
	}
	
/***************FR**************/		
	if((SLIP_CONTROL_NEED_FR==1)&&(slip_m_fr<= -WHEEL_SLIP_80))
	{		
		slip_m_fr_lock_cont =slip_m_fr_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_m_fr>= -WHEEL_SLIP_80)&&(slip_m_fr <= -WHEEL_SLIP_70))
	{
		slip_m_fr_lock_cont = slip_m_fr_lock_cont;
	}
	else 
	{
		slip_m_fr_lock_cont =0;	
	}
#endif	
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCountLockR(struct ESC_STRUCT *WL_ESC)
{ 
	if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m<= -WHEEL_SLIP_60))
	{		
		WL_ESC->slip_m_lock_cont =WL_ESC->slip_m_lock_cont+1;
	}
	else if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m>= -WHEEL_SLIP_60)&&(WL_ESC->slip_m <= -WHEEL_SLIP_50))
	{
		WL_ESC->slip_m_lock_cont = WL_ESC->slip_m_lock_cont;
	}
	else
	{
		WL_ESC->slip_m_lock_cont =0;	
	}
}
#else
void LCESP_vCountLockR(void)
{ 
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
/***************RL***************/
	if((SLIP_CONTROL_NEED_RL==1)&&(slip_status_position!=3))
	{
		;
	}
	else if((SLIP_CONTROL_NEED_RL==1)&&(slip_m_rl<= -WHEEL_SLIP_60)&&(slip_status_position==3))
	{		
		slip_m_rl_lock_cont =slip_m_rl_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_RL==1)&&(slip_m_rl>= -WHEEL_SLIP_60)&&(slip_m_rl <= -WHEEL_SLIP_50)&&(slip_status_position==3))
	{
		slip_m_rl_lock_cont = slip_m_rl_lock_cont;
	}
	else
	{
		slip_m_rl_lock_cont =0;	
	}

/***************RR***************/
	if((SLIP_CONTROL_NEED_RR==1)&&(slip_status_position!=4))
	{
		;
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(slip_m_rr<= -WHEEL_SLIP_60)&&(slip_status_position==4))
	{		
		slip_m_rr_lock_cont =slip_m_rr_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(slip_m_rr>= -WHEEL_SLIP_60)&&(slip_m_rr <= -WHEEL_SLIP_50)&&(slip_status_position==4))
	{
		slip_m_rr_lock_cont = slip_m_rr_lock_cont;
	}
	else 
	{
		slip_m_rr_lock_cont =0;	
	}
#else
/***************RL***************/
	if((SLIP_CONTROL_NEED_RL==1)&&(slip_m_rl<= -WHEEL_SLIP_60))
	{		
		slip_m_rl_lock_cont =slip_m_rl_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_RL==1)&&(slip_m_rl>= -WHEEL_SLIP_60)&&(slip_m_rl <= -WHEEL_SLIP_50))
	{
		slip_m_rl_lock_cont = slip_m_rl_lock_cont;
	}
	else
	{
		slip_m_rl_lock_cont =0;	
	}

/***************RR***************/
	if((SLIP_CONTROL_NEED_RR==1)&&(slip_m_rr<= -WHEEL_SLIP_60))
	{		
		slip_m_rr_lock_cont =slip_m_rr_lock_cont+1;
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(slip_m_rr>= -WHEEL_SLIP_60)&&(slip_m_rr <= -WHEEL_SLIP_50))
	{
		slip_m_rr_lock_cont = slip_m_rr_lock_cont;
	}
	else 
	{
		slip_m_rr_lock_cont =0;	
	}
#endif	
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vDetectCompulsionHoldFL(void)
{
	if(FL_ESC.SLIP_CONTROL_NEED==1)
	{
		LCESP_vSetCompulsionHoldF(&FL_ESC);
	}
	else
	{
		FL_ESC.FLAG_COMPULSION_HOLD =	0;	
		FL_ESC.hold_counter = 0;
		FL_ESC.slip_m_lock_cont =0;
	}
}
void LCESP_vDetectCompulsionHoldFR(void)
{
	if(FR_ESC.SLIP_CONTROL_NEED==1)
	{
		LCESP_vSetCompulsionHoldF(&FR_ESC);
	}
	else
	{
		FR_ESC.FLAG_COMPULSION_HOLD =	0;
		FR_ESC.hold_counter = 0;		 
		FR_ESC.slip_m_lock_cont =0;
	}
}
void LCESP_vSetCompulsionHoldF(struct ESC_STRUCT *WL_ESC)
{
	if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		{ 
			if((esp_mu <= lsesps16MuMed)&&(WL_ESC->esp_pressure_count<=S16_COMP_HOLD_PRE_CNT_F)
			&&(WL_ESC->slip_control_need_counter>=S16_COMP_HOLD_EN_F)&&(WL_ESC->slip_m_lock_cont <= S16_COMP_HOLD_LOCK_ACCEPT_F)
			&&(WL_ESC->slip_control_need_counter>(WL_ESC->esp_pressure_count+3)))
			{
				 WL_ESC->FLAG_COMPULSION_HOLD= 1;
				 WL_ESC->hold_counter = WL_ESC->hold_counter+1;
			}
			else 
			{
				WL_ESC->FLAG_COMPULSION_HOLD =	0;
				WL_ESC->hold_counter=0;
			}

			if((WL_ESC->FLAG_COMPULSION_HOLD==1)&&(esp_mu <= lsesps16MuMed)&&(WL_ESC->esp_pressure_count<=S16_COMP_HOLD_PRE_CNT_F)
			&&(WL_ESC->hold_counter<=S16_COMP_HOLD_FRONT)&&(WL_ESC->slip_control_need_counter>(WL_ESC->esp_pressure_count+3)))
			{
				 WL_ESC->FLAG_COMPULSION_HOLD= 1;
				 WL_ESC->hold_counter=WL_ESC->hold_counter;
			}
			else 
			{
				WL_ESC->FLAG_COMPULSION_HOLD =	0;
				WL_ESC->hold_counter=0;
			}
		}	
		else 
		{
			WL_ESC->FLAG_COMPULSION_HOLD=0;	
		}
}
#else
void LCESP_vDetectCompulsionHoldF(void)
{
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_status_position==1))
	{	
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		{ 
			if((esp_mu <= lsesps16MuMed)&&(esp_pressure_count_fl<=S16_COMP_HOLD_PRE_CNT_F)
			&&(slip_control_need_counter_fl>=S16_COMP_HOLD_EN_F)&&(slip_m_fl_lock_cont <= S16_COMP_HOLD_LOCK_ACCEPT_F)
			&&(slip_control_need_counter_fl>(esp_pressure_count_fl+3)))
			{
				 FLAG_COMPULSION_HOLD= 1;
				 hold_counter_fl = hold_counter_fl+1;
			}
			else 
			{
				FLAG_COMPULSION_HOLD =	0;
				hold_counter_fl=0;
			}
			
			if((FLAG_COMPULSION_HOLD==1)&&(esp_mu <= lsesps16MuMed)&&(esp_pressure_count_fl<=S16_COMP_HOLD_PRE_CNT_F)
			&&(hold_counter_fl<=S16_COMP_HOLD_FRONT)&&(slip_control_need_counter_fl>(esp_pressure_count_fl+3)))
			{
				 FLAG_COMPULSION_HOLD= 1;
				 hold_counter_fl=hold_counter_fl;
			}
			else 
			{
				FLAG_COMPULSION_HOLD =	0;
				hold_counter_fl=0;
			}
		}	
		else 
		{
			FLAG_COMPULSION_HOLD=0;	
		}
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_status_position==2))
	{
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		{
			if((esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_fr<= S16_COMP_HOLD_PRE_CNT_F)
			&&(slip_control_need_counter_fr>=S16_COMP_HOLD_EN_F)&&(slip_m_fr_lock_cont <= S16_COMP_HOLD_LOCK_ACCEPT_F)
			&&(slip_control_need_counter_fr>(esp_pressure_count_fr+3)))
			{
				FLAG_COMPULSION_HOLD = 1;
				hold_counter_fr = hold_counter_fr+1;
			}
			else
			{
				 FLAG_COMPULSION_HOLD =	0;
				 hold_counter_fr = 0;
			}
			
			if((FLAG_COMPULSION_HOLD==1)&&(esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_fr<= S16_COMP_HOLD_PRE_CNT_F)
			&&(hold_counter_fr<=S16_COMP_HOLD_FRONT)&&(slip_control_need_counter_fr>(esp_pressure_count_fr+3)))
			{
				FLAG_COMPULSION_HOLD = 1;
				hold_counter_fr = hold_counter_fr;
			}
			else
			{
				 FLAG_COMPULSION_HOLD =	0;
				 hold_counter_fr =0;
			}
		}
		else
		{
			 FLAG_COMPULSION_HOLD=0;
		}
	}
	else
	{
		FLAG_COMPULSION_HOLD =	0;
		hold_counter_fr = 0;
		hold_counter_fl = 0;		 
		slip_m_fr_lock_cont =0;
		slip_m_fl_lock_cont =0;
	}
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vDetectCompulsionHoldRL(void)
{
	if((RL_ESC.SLIP_CONTROL_NEED==1)&&(ACT_TWO_WHEEL==0))
	{
		LCESP_vSetCompulsionHoldR(&RL_ESC);	      
	}
	else
	{
		RL_ESC.FLAG_COMPULSION_HOLD = 0;		
		RL_ESC.hold_counter = 0;
		RL_ESC.slip_m_lock_cont = 0;
	}
}
void LCESP_vDetectCompulsionHoldRR(void)
{
	if((RR_ESC.SLIP_CONTROL_NEED==1)&&(ACT_TWO_WHEEL==0))
	{
		LCESP_vSetCompulsionHoldR(&RR_ESC);	  
	}
	else
	{
		RR_ESC.FLAG_COMPULSION_HOLD = 0;
		RR_ESC.hold_counter = 0;
		RR_ESC.slip_m_lock_cont = 0;
	}
}
void LCESP_vSetCompulsionHoldR(struct ESC_STRUCT *WL_ESC)
{
	#if	( __Ext_Under_Ctrl == 1 )	
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0)&&(EXTREME_UNDER_CONTROL==0))
		  #else
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		  #endif
		{
			if((esp_mu	<= lsesps16MuMed)&&(WL_ESC->esp_pressure_count<= S16_COMP_HOLD_PRE_CNT_R)
			&&(WL_ESC->slip_control_need_counter>=S16_COMP_HOLD_EN_R)&&(WL_ESC->slip_m_lock_cont <= S16_COMP_HOLD_LOCK_ACCEPT_R)
			&&(WL_ESC->slip_control_need_counter>(WL_ESC->esp_pressure_count+3)))
			{
				WL_ESC->FLAG_COMPULSION_HOLD = 1;
				WL_ESC->hold_counter = WL_ESC->hold_counter+1;
			}
			else
			{
				 WL_ESC->FLAG_COMPULSION_HOLD =	0;
				 WL_ESC->hold_counter = 0;
			}
			
			if((WL_ESC->FLAG_COMPULSION_HOLD==1)&&(esp_mu	<= lsesps16MuMed)&&(WL_ESC->esp_pressure_count<= S16_COMP_HOLD_PRE_CNT_R)
			&&(WL_ESC->hold_counter<=S16_COMP_HOLD_REAR)&&(WL_ESC->slip_control_need_counter>(WL_ESC->esp_pressure_count+3)))
			{
				WL_ESC->FLAG_COMPULSION_HOLD = 1;
				WL_ESC->hold_counter = WL_ESC->hold_counter;
			}
			else
			{
				 WL_ESC->FLAG_COMPULSION_HOLD =	0;
				 WL_ESC->hold_counter =0;
			}
		}
		else
		{
			WL_ESC->FLAG_COMPULSION_HOLD=0;
		}
}
#else
void LCESP_vDetectCompulsionHoldR(void)
{

	if((SLIP_CONTROL_NEED_RL==1)&&(ACT_TWO_WHEEL==0)&&(slip_status_position==3))
	{	
	      #if	( __Ext_Under_Ctrl == 1 )	
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0)&&(EXTREME_UNDER_CONTROL==0))
		  #else
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		  #endif
		{
			if((esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_rl<= S16_COMP_HOLD_PRE_CNT_R)
			&&(slip_control_need_counter_rl>=S16_COMP_HOLD_EN_R)&&(slip_m_rl_lock_cont <= S16_COMP_HOLD_LOCK_ACCEPT_R)
			&&(slip_control_need_counter_rl>(esp_pressure_count_rl+3)))
			{
				FLAG_COMPULSION_HOLD_REAR = 1;
				hold_counter_rl = hold_counter_rl+1;
			}
			else
			{
				 FLAG_COMPULSION_HOLD_REAR =	0;
				 hold_counter_rl = 0;
			}
			
			if((FLAG_COMPULSION_HOLD_REAR==1)&&(esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_rl<= S16_COMP_HOLD_PRE_CNT_R)
			&&(hold_counter_rl<=S16_COMP_HOLD_REAR)&&(slip_control_need_counter_rl>(esp_pressure_count_rl+3)))
			{
				FLAG_COMPULSION_HOLD_REAR = 1;
				hold_counter_rl = hold_counter_rl;
			}
			else
			{
				 FLAG_COMPULSION_HOLD_REAR =	0;
				 hold_counter_rl =0;
			}
		}
		else
		{
			FLAG_COMPULSION_HOLD_REAR=0;
		}
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(ACT_TWO_WHEEL==0)&&(slip_status_position==4))
	{
	      #if	( __Ext_Under_Ctrl == 1 )	
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0)&&(EXTREME_UNDER_CONTROL==0))
		  #else
		if((ABS_fz==0)&&(FLAG_ON_CENTER==0))
		  #endif
		{
			if((esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_rr<= S16_COMP_HOLD_PRE_CNT_R)
			&&(slip_control_need_counter_rr>=S16_COMP_HOLD_EN_R)&&(slip_m_rr_lock_cont <=S16_COMP_HOLD_LOCK_ACCEPT_R)
			&&(slip_control_need_counter_rr>(esp_pressure_count_rr+3)))
			{
				FLAG_COMPULSION_HOLD_REAR = 1;
				hold_counter_rr = hold_counter_rr+1;
			}
			else
			{
				 FLAG_COMPULSION_HOLD_REAR =	0;
				 hold_counter_rr = 0;
			}
			
			if((FLAG_COMPULSION_HOLD_REAR==1)&&(esp_mu	<= lsesps16MuMed)&&(esp_pressure_count_rr<= S16_COMP_HOLD_PRE_CNT_R)
			&&(hold_counter_rr<=S16_COMP_HOLD_REAR)&&(slip_control_need_counter_rr>(esp_pressure_count_rr+3)))
			{
				FLAG_COMPULSION_HOLD_REAR = 1;
				hold_counter_rr = hold_counter_rr;
			}
			else
			{
				 FLAG_COMPULSION_HOLD_REAR =	0;
				 hold_counter_rr =0;
			}
		}
		else
		{
			FLAG_COMPULSION_HOLD_REAR=0;
		}
	}
	else
	{
		FLAG_COMPULSION_HOLD_REAR = 0;
		hold_counter_rr = 0;
		hold_counter_rl = 0;
		slip_m_rr_lock_cont = 0;
		slip_m_rl_lock_cont = 0;
	}
}
#endif

void LCESP_vInterfaceSlipController(void)
{	
    LAESP_vResetTempValve();
    #if(__IDB_LOGIC==ENABLE)
    
    if(lis16DriverIntendedTP > MPRESS_0G5BAR)
    {
    	lcidbs16activebrakepress = lis16DriverIntendedTP;
    }
//    else if(liu1AebActiveFlg == 1)
//    {
//    	lcidbs16activebrakepress = lis16ActiveTarPress; /*ldahbAEB.lds16ActiveBrkTP*/
//    }
    else
    {
    	lcidbs16activebrakepress = 0;
    }


    #endif
    
    
if((SLIP_CONTROL_NEED_FR == 1)||(SLIP_CONTROL_NEED_FL == 1))  
{
//if ( LOOP_TIME_20MS_TASK_1 == 1 ) /* Cycle by Cho */
//{    
    variable_rise_scan = LCESP_s16IInter2Point( esp_mu, lsesps16MuMedHigh, S16_VARIABLE_RISE_SCAN_START_M, lsesps16MuHigh, S16_VARIABLE_RISE_SCAN_START_H );
/* 06 SWD : Need to tune each speeds and roads friction  */

    esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ADD_SCAN_H_SP_H_MU, 
                                S16_ADD_SCAN_H_SP_M_MU, S16_ADD_SCAN_H_SP_L_MU); 
                                
    esp_tempW2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ADD_SCAN_M_SP_H_MU, 
                                S16_ADD_SCAN_M_SP_M_MU, S16_ADD_SCAN_M_SP_L_MU);       

    esp_tempW3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ADD_SCAN_L_SP_H_MU, 
                                S16_ADD_SCAN_L_SP_M_MU, S16_ADD_SCAN_L_SP_L_MU);      
                                 
    esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ADD_SCAN_V_L_SP_H_MU, 
                                S16_ADD_SCAN_V_L_SP_M_MU, S16_ADD_SCAN_V_L_SP_L_MU);      
                                                                           
    esp_tempW4 = LCESP_s16IInter4Point(vrefk,  S16_ADD_RISE_SCAN_V_L_SP , esp_tempW5,
                       S16_ADD_RISE_SCAN_L_SP , esp_tempW3,
                       S16_ADD_RISE_SCAN_M_SP , esp_tempW2,  
                       S16_ADD_RISE_SCAN_H_SP , esp_tempW1);        
    
    variable_rise_scan = variable_rise_scan + esp_tempW4 ;
}
#if	( __Ext_Under_Ctrl == 1 )	

  if( EXTREME_UNDER_CONTROL ==1 )
  {	
		variable_rise_scan = S16_EX_UND_VAR_RISE_SCAN ;
	}
	else
	{
		  ;
	}
	
#endif	    
    
/*}*/
    
#if Drum_Brake   

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/
    variable_rise_scan_rear = LCESP_s16IInter2Point( esp_mu, lsesps16MuMedHigh, S16_VARIABLE_RISE_SCAN_START_M_R, lsesps16MuHigh, S16_VARIABLE_RISE_SCAN_START_H_R );
/*
    esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_Rear_ADD_SCAN_H_SP_H_M, 
                                S16_Rear_ADD_SCAN_H_SP_M_M, S16_Rear_ADD_SCAN_H_SP_L_M); 
                                
    esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, S16_Rear_ADD_SCAN_M_SP_H_M, 
                                S16_Rear_ADD_SCAN_M_SP_M_M, S16_Rear_ADD_SCAN_M_SP_L_M);       

    esp_tempW7 = LCESP_s16FindRoadDependatGain( esp_mu, S16_Rear_ADD_SCAN_L_SP_H_M, 
                                S16_Rear_ADD_SCAN_L_SP_M_M, S16_Rear_ADD_SCAN_L_SP_L_M);                                                            

    esp_tempW8 = LCESP_s16IInter3Point(vrefk, S16_ADD_RISE_SCAN_L_SP , esp_tempW5,
                       S16_ADD_RISE_SCAN_M_SP , esp_tempW6,  S16_ADD_RISE_SCAN_H_SP , esp_tempW7);        
    
    variable_rise_scan_rear = variable_rise_scan_rear + esp_tempW8 ;
*/
/*}*/    
 
#endif
        
#if __ROP

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{ */ 
   	if(ROP_REQ_ACT==1) 
   	{
   		variable_rise_scan = variable_rise_scan + S16_ROP_ADD_VARIABLE_RISE_SCAN ; 
   	}
   	else
  	{
  		;
		}
/*}*/
#endif 
/*    if(wmu1CycleTime21ms_2==1) 
    {*/
     
        LCESP_vCalSpeedSlipGain();    
        LCESP_vCalSpeedWHRateGain();    
/*    }*/
        
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//        slip_control_front_FL = 0 ;  //불용변수
//        slip_control_front_FR = 0 ;
#else
        slip_control_front_FL = 0 ;  
        slip_control_front_FR = 0 ;         
#endif        
  
  if(SLIP_CONTROL_NEED_FL==0) 
	{
    lcu1ABSCBSCombCntrl_FL = 0;
	}
	
	if(SLIP_CONTROL_NEED_FR==0) 
	{
    lcu1ABSCBSCombCntrl_FR = 0;
  }
  if(SLIP_CONTROL_NEED_RL==0) 
	{
    lcu1ABSCBSCombCntrl_RL = 0;
  }
  if(SLIP_CONTROL_NEED_RR==0) 
	{
		lcu1ABSCBSCombCntrl_RR = 0;
	}
	
	
	
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  if(SLIP_CONTROL_NEED_FL==1) 
	{
		slip_status_position = 1;
    FL_ESC.slip_target=(FL_ESC.true_slip_target);
    if((ESP_SPLIT==1)&&(RL.MSL_BASE==1)&&(FL_ESC.slip_target < -S16_SLIP_TARGET_LIMIT_SPLIT_F))
		{
			FL_ESC.slip_target= - S16_SLIP_TARGET_LIMIT_SPLIT_F;
		}
		else if((ABS_fz==1)&&(FL_ESC.slip_target < -WHEEL_SLIP_50)) 
		{
			FL_ESC.slip_target= -WHEEL_SLIP_50; /* 060118 added by eslim */
		}
		else if((ABS_fz==0)&&(FL_ESC.slip_target < -WHEEL_SLIP_40)) 
		{
			FL_ESC.slip_target= -WHEEL_SLIP_40; /* 060118 added by eslim */
		}
		else
		{
			;
		}
        FL_ESC.slip_measurement= slip_m_fl;
        
        LCESP_vSlipControlFL();
        
        esp_pressure_count_fl_old = esp_pressure_count_fl;
        
    }
    else
    {
    	;
    }
    
    if(SLIP_CONTROL_NEED_FL==0)
    {
      	FL_ESC.slip_target =0 ;      	      	
       	FL_ESC.slip_target_prev =0;       	
        FL_ESC.slip_error_prev = 0;        
        FL_ESC.slip_error_dot_prev = 0;        
        FL_ESC.slip_measurement= 0;        
        FL_ESC.slip_measurement_prev =0;        
        FL_ESC.rate_counter=0;        
        FL_ESC.rate_interval=0;        
        TCS_built_reapply_pattern_fl=0;        
        FL_ESC.esp_control_mode=1;   		
		    FL_ESC.esp_pressure_count=0;		
//		    esp_control_cycle_fl = 0;		
		    FL_ESC.FLAG_COMPULSION_HOLD=0;
		    FL_ESC.lcespu8IdbDumpRateCnt=0;
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
  		//LCESP_vResetVlvRiseTi(&FL_ESC);
  		LCESP_vResetVlvDumpTi(&FL_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */


    }
    else
    {
    	;
    }
    
    #if ((__ESC_TCMF_CONTROL ==1)&&(__VALVE_TEST_TCMF ==0))
    if(ESP_BRAKE_CONTROL_FL ==0)
    {
    	FL_ESC.lcespu1TCMFControl = 0;
    	FL_ESC.lcespu1TCMFControl_Old = 0;
    	FL_ESC.MFC_Current_Inter = 0;
    	FL_ESC.lcespu1TCMFCon_FadeOut = 0;
    	FL_ESC.lcespu1TCMFConOld_FadeOut = 0;
    	FL_ESC.lcesps16StepIntDutyPribyCom = 0;
    	FL_ESC.lcesps16StepIntDutySecbyCom = 0;
    	FL_ESC.lcesps16StepIntDutybyComVol = 0;    	
    	FL_ESC.slip_control_dot = 0;
        FL_ESC.lcespu1TCMFCon_IniFull = 0;
        FL_ESC.lcespu1TCMFCon_Rise = 0;
        FL_ESC.lcespu1TCMFCon_Hold = 0;
        FL_ESC.lcespu1TCMFCon_Dump = 0;
        FL_ESC.lcespu1TCMFConOld_IniFull = 0;
		FL_ESC.lcespu1TCMFConOld_Rise = 0;
		FL_ESC.lcespu1TCMFConOld_Hold = 0;
		FL_ESC.lcespu1TCMFConOld_Dump = 0;
		FL_ESC.lcespu1TCMFCon_Rise_MAX = 0;
		FL_ESC.lcespu1TCMFCon_Rise_STEP = 0;
		FL_ESC.lcespu1TCMFCon_Dump_SLOW = 0;
		FL_ESC.lcespu1TCMFCon_Dump_FAST = 0;
		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg3 =0;
		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg2 =0;
		FL_ESC.lcespu1TCMFCon_Rise_STEP_Reg1 =0;
		FL_ESC.lcespu1TCMFConSTEP_RegTrans =0;
		FL_ESC.lcesps16TCMFCon_STEP_Reg =0;
		FL_ESC.lcesps16TCMFCon_STEP_RegOld =0;
    	FL_ESC.lcesps16TCMFControlCnt = 0;
    	FL_ESC.lcespu1TCMFConCBSCombFlag = 0;
		FL_ESC.lcespu1TcmfUnderToOverCompFlag = 0;
		lcespu8TCMFControlWheel = 0;
		FL_ESC.lcespu1TCMFInhibitbyMpress = 0;
		FL_ESC.lcesps16TCMFCompDutybyMP = 0;
		FL_ESC.lcespu1TCMFInhibit = 0;
    }
    else
    {
    	;
    }
    #endif
    
#else
	if(SLIP_CONTROL_NEED_FL==1) 
	{
	    slip_status_position = 1;
       	slip_target_front=(true_slip_target_fl);
        if((ESP_SPLIT==1)&&(RL.MSL_BASE==1)&&(slip_target_front < -S16_SLIP_TARGET_LIMIT_SPLIT_F))
		{
			slip_target_front= - S16_SLIP_TARGET_LIMIT_SPLIT_F;
		}
		else if((ABS_fz==1)&&(slip_target_front < -WHEEL_SLIP_50)) 
		{
			slip_target_front= -WHEEL_SLIP_50; /* 060118 added by eslim */
		}
		else if((ABS_fz==0)&&(slip_target_front < -WHEEL_SLIP_40)) 
		{
			slip_target_front= -WHEEL_SLIP_40; /* 060118 added by eslim */
		}
		else
		{
			;
		}
        slip_target_prev_front =slip_target_prev_fl;
        slip_error_prev_front = slip_error_prev_fl;
        slip_error_dot_prev_front = slip_error_dot_prev_fl;
        slip_measurement_front= slip_m_fl;

        rate_counter_front=rate_counter_fl;
        rate_interval_front=rate_interval_fl;
       
        LCESP_vSlipControlF();
        
        slip_target_prev_fl=slip_target_prev_front;
        slip_error_prev_fl=slip_error_prev_front;
        slip_error_dot_prev_fl = slip_error_dot_prev_front;
        rate_counter_fl=rate_counter_front;
        rate_interval_fl=rate_interval_front;
        esp_pressure_count_fl_old = esp_pressure_count_fl;
        
        slip_control_front_FL = slip_control_front ; 
    }
    else
    {
    	;
    }
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(SLIP_CONTROL_NEED_FR==1) 
    {
    	slip_status_position = 2;
      	FR_ESC.slip_target=(FR_ESC.true_slip_target);
        if((ESP_SPLIT==1)&&(RR.MSL_BASE==1)&&(FR_ESC.slip_target < -S16_SLIP_TARGET_LIMIT_SPLIT_F))
		{
			FR_ESC.slip_target= -S16_SLIP_TARGET_LIMIT_SPLIT_F;
		}	
		else if((ABS_fz==1)&&(FR_ESC.slip_target < -WHEEL_SLIP_50)) 
		{
			FR_ESC.slip_target= -WHEEL_SLIP_50; /* 060118 added by eslim */
		}
		else if((ABS_fz==0)&&(FR_ESC.slip_target < -WHEEL_SLIP_40)) 
		{
			FR_ESC.slip_target= -WHEEL_SLIP_40; /* 060118 added by eslim */
		}
		else
		{
			;
		}
        FR_ESC.slip_measurement= slip_m_fr;

        LCESP_vSlipControlFR();
        
        esp_pressure_count_fr_old = esp_pressure_count_fr;
    }
    else
    {
    	;
    }
    
    if(SLIP_CONTROL_NEED_FR==0)
    {
      	FR_ESC.slip_target =0 ;      	      	
       	FR_ESC.slip_target_prev =0;       	
        FR_ESC.slip_error_prev = 0;        
        FR_ESC.slip_error_dot_prev = 0;        
        FR_ESC.slip_measurement= 0;        
        FR_ESC.slip_measurement_prev =0;        
        FR_ESC.rate_counter=0;        
        FR_ESC.rate_interval=0;        
        TCS_built_reapply_pattern_fr=0;        
  		  FR_ESC.esp_control_mode=1;   		
		    FR_ESC.esp_pressure_count=0;		
//		    esp_control_cycle_fr = 0;			
		    FR_ESC.FLAG_COMPULSION_HOLD=0;
		    FR_ESC.lcespu8IdbDumpRateCnt=0;
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
  		//LCESP_vResetVlvRiseTi(&FR_ESC);
  		LCESP_vResetVlvDumpTi(&FR_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

    } 
    else
    {
    	;
    }
    
    #if ((__ESC_TCMF_CONTROL ==1)&&(__VALVE_TEST_TCMF ==0))
    if(ESP_BRAKE_CONTROL_FR ==0)
    {
    	FR_ESC.lcespu1TCMFControl = 0;
    	FR_ESC.lcespu1TCMFControl_Old = 0;
    	FR_ESC.MFC_Current_Inter = 0;
    	FR_ESC.lcespu1TCMFCon_FadeOut = 0;
    	FR_ESC.lcespu1TCMFConOld_FadeOut = 0;
    	FR_ESC.lcesps16StepIntDutyPribyCom = 0;
    	FR_ESC.lcesps16StepIntDutySecbyCom = 0;
    	FR_ESC.lcesps16StepIntDutybyComVol = 0;
    	FR_ESC.slip_control_dot = 0;
        FR_ESC.lcespu1TCMFCon_IniFull = 0;
        FR_ESC.lcespu1TCMFCon_Rise = 0;
        FR_ESC.lcespu1TCMFCon_Hold = 0;
        FR_ESC.lcespu1TCMFCon_Dump = 0;
        FR_ESC.lcespu1TCMFConOld_IniFull = 0;
		FR_ESC.lcespu1TCMFConOld_Rise = 0;
		FR_ESC.lcespu1TCMFConOld_Hold = 0;
		FR_ESC.lcespu1TCMFConOld_Dump = 0;
		FR_ESC.lcespu1TCMFCon_Rise_MAX = 0;
		FR_ESC.lcespu1TCMFCon_Rise_STEP = 0;
		FR_ESC.lcespu1TCMFCon_Dump_SLOW = 0;
		FR_ESC.lcespu1TCMFCon_Dump_FAST = 0;
		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg3 =0;
		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg2 =0;
		FR_ESC.lcespu1TCMFCon_Rise_STEP_Reg1 =0;
		FR_ESC.lcespu1TCMFConSTEP_RegTrans =0;
		FR_ESC.lcesps16TCMFCon_STEP_Reg =0;
		FR_ESC.lcesps16TCMFCon_STEP_RegOld =0;		
    	FR_ESC.lcesps16TCMFControlCnt = 0;
    	FR_ESC.lcespu1TCMFConCBSCombFlag = 0;
		FR_ESC.lcespu1TcmfUnderToOverCompFlag = 0;
		lcespu8TCMFControlWheel = 0;
		FR_ESC.lcespu1TCMFInhibitbyMpress = 0;
		FR_ESC.lcesps16TCMFCompDutybyMP = 0;
		FR_ESC.lcespu1TCMFInhibit = 0;
    }
    else
    {
    	;
    }
    #endif
    
#else
    if(SLIP_CONTROL_NEED_FR==1) 
    {
        slip_status_position = 2;
      	slip_target_front=(true_slip_target_fr);
        if((ESP_SPLIT==1)&&(RR.MSL_BASE==1)&&(slip_target_front < -S16_SLIP_TARGET_LIMIT_SPLIT_F))
		{
			slip_target_front= -S16_SLIP_TARGET_LIMIT_SPLIT_F;
		}	
		else if((ABS_fz==1)&&(slip_target_front < -WHEEL_SLIP_50)) 
		{
			slip_target_front= -WHEEL_SLIP_50; /* 060118 added by eslim */
		}
		else if((ABS_fz==0)&&(slip_target_front < -WHEEL_SLIP_40)) 
		{
			slip_target_front= -WHEEL_SLIP_40; /* 060118 added by eslim */
		}
		else
		{
			;
		}

        slip_target_prev_front =slip_target_prev_fr;
        slip_error_prev_front = slip_error_prev_fr;
        slip_error_dot_prev_front = slip_error_dot_prev_fr;
        slip_measurement_front= slip_m_fr;
        rate_counter_front=rate_counter_fr;
        rate_interval_front=rate_interval_fr;
     
        LCESP_vSlipControlF();
        
        slip_target_prev_fr=slip_target_prev_front;
        slip_error_prev_fr=slip_error_prev_front;
        slip_error_dot_prev_fr = slip_error_dot_prev_front;
        rate_counter_fr=rate_counter_front;
        rate_interval_fr=rate_interval_front;
        esp_pressure_count_fr_old = esp_pressure_count_fr;
        slip_control_front_FR = slip_control_front ; 
    }
    else
    {
    	;
    }
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
    if((SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FL==0))
    {
      	slip_target_front =0 ;
       	slip_target_prev_front =0;
        slip_error_prev_front = 0;
        slip_error_dot_prev_front = 0;
        slip_measurement_front= 0;
        slip_measurement_front_prev =0;
        rate_counter_front=0;
        rate_interval_front=0;
        TCS_built_reapply_pattern_fl=0;
        TCS_built_reapply_pattern_fr=0;
        esp_control_mode_fl=1;
  		esp_control_mode_fr=1; 
		esp_pressure_count_fl=0;
		esp_pressure_count_fr=0;
		esp_control_cycle_fl = 0;
		esp_control_cycle_fr = 0;	
		
		FLAG_COMPULSION_HOLD=0;   
    }
    else
    {
    	;
    }
#endif    
    
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(SLIP_CONTROL_NEED_RL==1)
    {
	    slip_status_position = 3;        

    	/*if( ACT_TWO_WHEEL==1 )   
    	{
    		slip_target_rear= (true_slip_target_rl);
    	}
    	else
    	{ */
    		 RL_ESC.slip_target= (RL_ESC.true_slip_target );
    	/*}*/
		if((ESP_SPLIT==1)&&(RL.MSL_BASE==1)&&(RL_ESC.slip_target < -S16_SLIP_TARGET_LIMIT_SPLIT_R))
		{
			RL_ESC.slip_target= -S16_SLIP_TARGET_LIMIT_SPLIT_R;
		}
		else if(RL_ESC.slip_target	< -WHEEL_SLIP_20)
		{
			RL_ESC.slip_target= -WHEEL_SLIP_20;
		}
		else
		{
			;
		}				
        RL_ESC.slip_measurement =slip_m_rl; 
        
        LCESP_vDetectNullDump(&RL_ESC, &RL);		/* 09 SWD */
       	LCESP_vSlipControlRL();
       	
        esp_pressure_count_rl_old = esp_pressure_count_rl;

	}       
    else
    {
    	;
    }
    
    if(SLIP_CONTROL_NEED_RL==0)
    {
		    RL_ESC.slip_target =0 ;		
       	RL_ESC.slip_target_prev =0;      	
        RL_ESC.slip_error_prev = 0;        
        RL_ESC.slip_error_dot_prev = 0;        
        RL_ESC.slip_measurement= 0;        
        RL_ESC.slip_measurement_prev =0;        
        RL_ESC.rate_counter=0;        
        RL_ESC.rate_interval=0; 		    
 		    TCS_built_reapply_pattern_rl=0;        
        RL_ESC.esp_control_mode=2;         
        RL_ESC.esp_pressure_count=0;		    
//		    esp_control_cycle_rl = 0;		
		    RL_ESC.FLAG_COMPULSION_HOLD=0;		    
		    RL_ESC.FLAG_NULL_DUMP = 0;
		    RL_ESC.lcespu8IdbDumpRateCnt=0;
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
  		//LCESP_vResetVlvRiseTi(&RL_ESC);
  		LCESP_vResetVlvDumpTi(&RL_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

	  }
	  else
	  {
		  ;
	  }
    
    if(SLIP_CONTROL_NEED_RR==1)
    {
	    slip_status_position = 4; 

		/*if( ACT_TWO_WHEEL==1)  
		{
			slip_target_rear= (true_slip_target_rr);
		}
		else
		{*/
			RR_ESC.slip_target= (RR_ESC.true_slip_target );
		/*}*/
		
		if((ESP_SPLIT==1)&&(RR.MSL_BASE==1)&&(RR_ESC.slip_target < -S16_SLIP_TARGET_LIMIT_SPLIT_R)) 
		{
			RR_ESC.slip_target= -S16_SLIP_TARGET_LIMIT_SPLIT_R;
		}
		else if(RR_ESC.slip_target	< -WHEEL_SLIP_20)
		{
			RR_ESC.slip_target= -WHEEL_SLIP_20;
		}
		else
		{
			;
		}

		RR_ESC.slip_measurement= slip_m_rr;
				
    LCESP_vDetectNullDump(&RR_ESC, &RR);		/* 09 SWD */
		LCESP_vSlipControlRR();
		
    esp_pressure_count_rr_old = esp_pressure_count_rr;
	}
 	else
    {
    	;
    }
    
    if(SLIP_CONTROL_NEED_RR == 0)
	  {
		    RR_ESC.slip_target =0;		
       	RR_ESC.slip_target_prev =0;       	
        RR_ESC.slip_error_prev = 0;        
        RR_ESC.slip_error_dot_prev = 0;        
        RR_ESC.slip_measurement= 0;        
        RR_ESC.slip_measurement_prev =0;        
        RR_ESC.rate_counter=0;        
 		    RR_ESC.rate_interval=0; 		    
        TCS_built_reapply_pattern_rr=0;        
  		  RR_ESC.esp_control_mode=2;         
		    RR_ESC.esp_pressure_count=0;		    
//		    esp_control_cycle_rr = 0;			        
		    RR_ESC.FLAG_COMPULSION_HOLD=0;		    
		    RR_ESC.FLAG_NULL_DUMP = 0;
		    RR_ESC.lcespu8IdbDumpRateCnt=0;
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
  		//LCESP_vResetVlvRiseTi(&RR_ESC);
  		LCESP_vResetVlvDumpTi(&RR_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

	  }  
	  else
	  {
		  ;
	  }

#else
    if(SLIP_CONTROL_NEED_RL==1)
    {
	    slip_status_position = 3;        

    	/*if( ACT_TWO_WHEEL==1 )   
    	{
    		slip_target_rear= (true_slip_target_rl);
    	}
    	else
    	{ */
    		 slip_target_rear= (true_slip_target_rl );
    	/*}*/
		if((ESP_SPLIT==1)&&(RL.MSL_BASE==1)&&(slip_target_rear < -S16_SLIP_TARGET_LIMIT_SPLIT_R))
		{
			slip_target_rear= -S16_SLIP_TARGET_LIMIT_SPLIT_R;
		}
		else if(slip_target_rear	< -WHEEL_SLIP_20)
		{
			slip_target_rear= -WHEEL_SLIP_20;
		}
		else
		{
			;
		}				
        slip_target_prev_rear= slip_target_prev_rl;
        slip_error_prev_rear = slip_error_prev_rl;
        slip_error_dot_prev_rear = slip_error_dot_prev_rl;
        slip_measurement_rear =slip_m_rl; 

        rate_counter_rear=rate_counter_rl;
        rate_interval_rear=rate_interval_rl;
        
        LCESP_vDetectNullDump();		/* 09 SWD */
       	LCESP_vSlipControlR();
       	
        slip_target_prev_rl=slip_target_prev_rear;
        slip_error_prev_rl=slip_error_prev_rear;
        slip_error_dot_prev_rl = slip_error_dot_prev_rear;
        rate_counter_rl=rate_counter_rear;
        rate_interval_rl=rate_interval_rear;
        esp_pressure_count_rl_old = esp_pressure_count_rl;

	}       
    else
    {
    	;
    }
    if(SLIP_CONTROL_NEED_RR==1)
    {
	    slip_status_position = 4; 

		/*if( ACT_TWO_WHEEL==1)  
		{
			slip_target_rear= (true_slip_target_rr);
		}
		else
		{*/
			slip_target_rear= (true_slip_target_rr );
		/*}*/
		
		if((ESP_SPLIT==1)&&(RR.MSL_BASE==1)&&(slip_target_rear < -S16_SLIP_TARGET_LIMIT_SPLIT_R)) 
		{
			slip_target_rear= -S16_SLIP_TARGET_LIMIT_SPLIT_R;
		}
		else if(slip_target_rear	< -WHEEL_SLIP_20)
		{
			slip_target_rear= -WHEEL_SLIP_20;
		}
		else
		{
			;
		}
		slip_target_prev_rear=slip_target_prev_rr;
		slip_error_prev_rear = slip_error_prev_rr;
		slip_error_dot_prev_rear = slip_error_dot_prev_rr;
		slip_measurement_rear= slip_m_rr;
		
		rate_counter_rear=rate_counter_rr;
		rate_interval_rear=rate_interval_rr;
		
    LCESP_vDetectNullDump();		/* 09 SWD */
		LCESP_vSlipControlR();
		
		slip_target_prev_rr=slip_target_prev_rear;
		slip_error_prev_rr=slip_error_prev_rear;
        slip_error_dot_prev_rr = slip_error_dot_prev_rear;
        rate_counter_rr=rate_counter_rear;
        rate_interval_rr=rate_interval_rear;
        esp_pressure_count_rr_old = esp_pressure_count_rr;
	}
 	else
    {
    	;
    }

    if((SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RL==0))
    {
		slip_target_rear =0 ;
       	slip_target_prev_rear =0;
        slip_error_prev_rear = 0;
        slip_error_dot_prev_rear = 0;
        slip_measurement_rear= 0;
        slip_measurement_rear_prev =0;
        rate_counter_rear=0;
        rate_interval_rear=0;
 		TCS_built_reapply_pattern_rl=0;
        TCS_built_reapply_pattern_rr=0;
        esp_control_mode_rl=2;
  		esp_control_mode_rr=2; 
        esp_pressure_count_rl=0;
		esp_pressure_count_rr=0;
		esp_control_cycle_rl = 0;
		esp_control_cycle_rr = 0;
		
		FLAG_COMPULSION_HOLD_REAR=0;	        
		FLAG_NULL_DUMP_R = 0;        
	}  
	else
	{
		;
	}     
#endif    
 
/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
	LAESP_vActXsplitEsvTcvalve();
#else
	LAESP_vActFRsplitEsvTcvalve();
#endif
/*060607 modified and added for FR Split by eslim*/

    LAESP_vCalNcDutyInEsp();
   
	LCESC_vCalFadeoutTime();
	LCESC_vSetFadeoutControlFront();
	LCESC_vSetFadeoutControlRear();
	LCESC_vCalFadeoutDumprateFront();
	LCESC_vCalFadeoutDumprateRear();
	LCESC_vCalFadeoutTctime();
	if((ESP_PULSE_DUMP_FLAG_F==1)||(ESP_PULSE_DUMP_FLAG_R==1))
    {
    	ESP_PULSE_DUMP_FLAG=1;
    }
    else 
    {
    	ESP_PULSE_DUMP_FLAG = 0;
    }

	if(ESP_PULSE_DUMP_FLAG==1)  
    {
    		LAESP_vActFadeout();
    }
    else
    {
    	;
    }         

    if((YAW_CDC_WORK==1)||(ESP_PULSE_DUMP_FLAG==1)) 
    {
    	ESP_BRAKE_CONTROL=1;
    }
    else 
    {
    	ESP_BRAKE_CONTROL=0;
    }
         
    LAESP_vCalMototTime();               
    #if  (__IDB_LOGIC == ENABLE)
    
    
    if((SLIP_CONTROL_NEED_FL ==1)||(SLIP_CONTROL_NEED_FR ==1)||(SLIP_CONTROL_NEED_RL ==1)||(SLIP_CONTROL_NEED_RR ==1))
    {
    	if((FL_ESC.lcescs16AllowStrRetime < 0)||(FR_ESC.lcescs16AllowStrRetime < 0)||(RL_ESC.lcescs16AllowStrRetime < 0)||(RR_ESC.lcescs16AllowStrRetime < 0))
    	{
    		lcesps16AllowFnlStrRetime = -10;	
    	}
    	else
    	{
    		if((SLIP_CONTROL_NEED_FL ==1)||(SLIP_CONTROL_NEED_FR ==1))
    		{
				if(FL_ESC.lcescs16AllowStrRetime >FR_ESC.lcescs16AllowStrRetime)
    			{
    				lcesps16AllowFnlStrRetime = FL_ESC.lcescs16AllowStrRetime;
    			}
    			else
    			{
     				lcesps16AllowFnlStrRetime = FR_ESC.lcescs16AllowStrRetime;   					
    			}
    		}
    		else
    		{
    			if(RL_ESC.lcescs16AllowStrRetime >RR_ESC.lcescs16AllowStrRetime)
    			{
    				lcesps16AllowFnlStrRetime = RL_ESC.lcescs16AllowStrRetime;
    			}
    			else
    			{
     				lcesps16AllowFnlStrRetime = RR_ESC.lcescs16AllowStrRetime;   					
    			}
    		}
       	}  			
    }
    else
    {
    	lcesps16AllowFnlStrRetime = 0;
    }
    
    if((SLIP_CONTROL_NEED_FL ==0)&&(SLIP_CONTROL_NEED_FL_old==0))
    {
        FL_ESC.lcesps16IdbTarPress = 0;
        FL_ESC.lcesps16IdbCurrentPress = 0;
        FL_ESC.lcesps16IdbriseDelTp = 0;
        FL_ESC.lcesps16IdbTarPressold = 0;
        FL_ESC.lcesps16CompDp = 0;
        FL_ESC.lcesps16EstpressOld = 0;        
        FL_ESC.lcesps16IdbRiseInterval = 0;
        FL_ESC.lcesps16IdbDumpInterval = 0;
        FL_ESC.lcesps16IdbriseDelTp = 0;
        FL_ESC.lcesps16IdbdumpDelTp = 0;
        FL_ESC.lcespu8IdbRiseRateCnt = 0;
        FL_ESC.U1EscActFrtTwoWheel = 0; 
        FL_ESC.U1StrWheelValveCnt = 0;
        FL_ESC.lcespu8CntBbsEsc = 0;
        FL_ESC.lcesps16FadedelP = 0;
        FL_ESC.lcesps16LimitIdbTargetP =0;
        FL_ESC.lcesps16IdbTarPRate = 0;
        FL_ESC.lcespu1DctMuxPrio  = 0;
        FL_ESC.lcescs16AllowStrRetime = 0;
        FL_ESC.U1EscDelTpComp = 0;
    }
    else if(SLIP_CONTROL_NEED_FL ==0)
    {
        FL_ESC.lcespu1DctMuxPrio  = 0;    	
        FL_ESC.lcesps16IdbCurrentPress = 0;
        FL_ESC.lcesps16IdbriseDelTp = 0;
        FL_ESC.lcesps16CompDp = 0;
        FL_ESC.lcesps16EstpressOld = 0;        
        FL_ESC.U1EscDelTpComp = 0;

    }
    else
    {
        ;
    }
    if((SLIP_CONTROL_NEED_FR ==0)&&(SLIP_CONTROL_NEED_FR_old==0))
    {
        FR_ESC.lcesps16IdbTarPress = 0;
        FR_ESC.lcesps16IdbTarPressold = 0; 
        FR_ESC.lcesps16IdbCurrentPress = 0;
        FR_ESC.lcesps16IdbriseDelTp = 0;
        FR_ESC.lcesps16CompDp = 0;
        FR_ESC.lcesps16EstpressOld = 0;        
        FR_ESC.lcesps16IdbRiseInterval = 0;
        FR_ESC.lcesps16IdbDumpInterval = 0;
        FR_ESC.lcesps16IdbriseDelTp = 0;
        FR_ESC.lcesps16IdbdumpDelTp = 0;
        FR_ESC.lcespu8IdbRiseRateCnt = 0;
        FR_ESC.U1EscActFrtTwoWheel = 0; 
        FR_ESC.U1StrWheelValveCnt = 0;
        FR_ESC.lcespu8CntBbsEsc = 0;
        FR_ESC.lcesps16FadedelP = 0;
        FR_ESC.lcesps16LimitIdbTargetP =0;
        FR_ESC.lcesps16IdbTarPRate = 0;
        FR_ESC.lcescs16AllowStrRetime = 0;
        FR_ESC.lcespu1DctMuxPrio  = 0;    	
        FR_ESC.U1EscDelTpComp = 0;

    }
    else if(SLIP_CONTROL_NEED_FR ==0)
    {
        FR_ESC.lcespu1DctMuxPrio  = 0;    	
        FR_ESC.lcesps16IdbCurrentPress = 0;
        FR_ESC.lcesps16IdbriseDelTp = 0;
        FR_ESC.lcesps16CompDp = 0;
        FR_ESC.lcesps16EstpressOld = 0;        
        FR_ESC.U1EscDelTpComp = 0;

    }
    else
    {
        ;
    }
    if((SLIP_CONTROL_NEED_RL ==0)&&(SLIP_CONTROL_NEED_RL_old==0))
    {
        RL_ESC.lcesps16IdbTarPress = 0;
        RL_ESC.lcesps16IdbTarPressold = 0; 
        RL_ESC.lcesps16IdbCurrentPress = 0;
        RL_ESC.lcesps16IdbriseDelTp = 0;
        RL_ESC.lcesps16CompDp = 0;
        RL_ESC.lcesps16EstpressOld = 0;        
        RL_ESC.lcesps16IdbRiseInterval = 0;
        RL_ESC.lcesps16IdbDumpInterval = 0;
        RL_ESC.lcesps16IdbriseDelTp = 0;
        RL_ESC.lcesps16IdbdumpDelTp = 0;
        RL_ESC.lcespu8IdbRiseRateCnt = 0;
        RL_ESC.U1StrWheelValveCnt = 0;
        RL_ESC.lcespu8CntBbsEsc = 0;
        RL_ESC.lcesps16FadedelP = 0;
        RL_ESC.lcesps16LimitIdbTargetP =0;
        RL_ESC.lcesps16IdbTarPRate = 0;
        RL_ESC.lcescs16AllowStrRetime = 0;
        RL_ESC.lcespu1DctMuxPrio  = 0;    	
        RL_ESC.U1EscDelTpComp = 0;

    }
    else if(SLIP_CONTROL_NEED_RL ==0)
    {
        RL_ESC.lcespu1DctMuxPrio  = 0;    	
        RL_ESC.lcesps16IdbCurrentPress = 0;
        RL_ESC.lcesps16IdbriseDelTp = 0;
        RL_ESC.lcesps16CompDp = 0;
        RL_ESC.lcesps16EstpressOld = 0;        
        RL_ESC.U1EscDelTpComp = 0;
    }
    else
    {
        ;
    }
    if((SLIP_CONTROL_NEED_RR ==0)&&(SLIP_CONTROL_NEED_RR_old==0))
    {
        RR_ESC.lcesps16IdbTarPress = 0;
        RR_ESC.lcesps16IdbTarPressold = 0; 
        RR_ESC.lcesps16IdbCurrentPress = 0;
        RR_ESC.lcesps16IdbriseDelTp = 0;
        RR_ESC.lcesps16CompDp = 0;
        RR_ESC.lcesps16EstpressOld = 0;        
        RR_ESC.lcesps16IdbRiseInterval = 0;
        RR_ESC.lcesps16IdbDumpInterval = 0;
        RR_ESC.lcesps16IdbriseDelTp = 0;
        RR_ESC.lcesps16IdbdumpDelTp = 0;
        RR_ESC.lcespu8IdbRiseRateCnt = 0;
        RR_ESC.U1StrWheelValveCnt = 0;
        RR_ESC.lcespu8CntBbsEsc = 0;
        RR_ESC.lcesps16FadedelP = 0;    
        RR_ESC.lcesps16LimitIdbTargetP =0;
        RR_ESC.lcesps16IdbTarPRate = 0;
        RR_ESC.lcescs16AllowStrRetime = 0;
        RR_ESC.lcespu1DctMuxPrio  = 0;    	
        RR_ESC.U1EscDelTpComp = 0;

    }
    else if(SLIP_CONTROL_NEED_RR ==0)
    {
        RR_ESC.lcespu1DctMuxPrio  = 0;    	
        RR_ESC.lcesps16IdbCurrentPress = 0;
        RR_ESC.lcesps16IdbriseDelTp = 0;
        RR_ESC.lcesps16CompDp = 0;
        RR_ESC.lcesps16EstpressOld = 0;         
        RR_ESC.U1EscDelTpComp = 0;

    }
    else
    {
        ;
    } 
    
    if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_FR==0))
    {
        U1StrokeRecoveryforESC = 0;
        U1StrokeRecoveryOk=0;  
        U1EscAfterStrokeR = 0;  
        U1EscAfterStrokeR_OLD = 0;
    }
    else
    {
        ;
    }   
    #endif             
}

  
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
void LCESP_vResetVariable(void)
{
     
    if((SLIP_CONTROL_NEED_FL_old==1)&&(esp_on_counter_1<2)) 
    {
    	esp_pressure_count_fl=esp_pressure_count_fl;
    }
    else 
    {
    	esp_pressure_count_fl=0;        
    }             
    
    if((SLIP_CONTROL_NEED_FR_old==1)&&(esp_on_counter_1<2)) 
    {
    	esp_pressure_count_fr=esp_pressure_count_fr;
    }
    else 
    {
    	esp_pressure_count_fr=0; 
    }                  
   
    if((SLIP_CONTROL_NEED_RL_old==1)&&(esp_on_counter_1<2)) 
    {
    	esp_pressure_count_rl=esp_pressure_count_rl;
    }
    else 
    {
    	esp_pressure_count_rl=0;  
    }                 
   
    if((SLIP_CONTROL_NEED_RR_old==1)&&(esp_on_counter_1<2)) 
    {
    	esp_pressure_count_rr=esp_pressure_count_rr;
    }
    else 
    {
    	esp_pressure_count_rr=0;                   
    }
}
#endif


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vSlipControlFL(void)
{
	LCESP_vCalSlipErrorF(&FL_ESC);
	LCESP_vCalSlipGainPressureF(&FL_ESC);
	LCESP_vCalSlipSignalF(&FL_ESC);
	LCESP_vCalVariableRiseRateF(&FL_ESC, &FL);
	LCESP_vCalPressureRateF(&FL_ESC);
	
	if(ABS_fz==0)
  {
    LCESP_vCountPressureF(&FL_ESC);        
    LCESP_vCountLockF(&FL_ESC);
  }
  else
  {
  	;
  }
  
  LCESP_vDetectCompulsionRise(&FL_ESC);
  LCESP_vCalVariableThresholdF(&FL_ESC);    
  LCESP_vDetectCompulsionHoldFL();
  LCESP_vDetectFullRise(&FL_ESC, &RL);
  LAESP_vActuateNcNovalveF(&FL_ESC, &FL);
    #if  (__IDB_LOGIC == ENABLE)
    if(lcu1US2WHControlFlag_RL==1)
    {
        FL_ESC.U1EscActFrtTwoWheel = 1;
        
        if((RL_ESC.lcespu8IdbRiseRateCnt < 1)&&(RL_ESC.esp_control_mode < 2))
        {
        	if(FL_ESC.lcespu8IdbRiseRateCnt == 0)
        {
        		FL_ESC.lcespu8IdbRiseRateCnt = FL_ESC.lcespu8IdbRiseRateCnt + 2;
        	}
        	else
        	{
        		;
        	}
        }
        else
        {
            ;
        } 
    }    
#if (__CRAM==1) && (__CRAM_PRE_FRONT_W_CRTL == 1)&&(__EUROPE_ADAC ==1)
    else if(lcu1espCramPreact_FR==1)
    {
        FL_ESC.U1EscActFrtTwoWheel = 1;
        if(FL_ESC.lcespu8IdbRiseRateCnt == FR_ESC.lcespu8IdbRiseRateCnt)
        {
           FL_ESC.lcespu8IdbRiseRateCnt = FL_ESC.lcespu8IdbRiseRateCnt +2; 
        }
        else
        {
            ;
        }   
    }
#endif
#if(__ROP == 1)
    else if(ROP_REQ_FRT_Pre_FR==1)
    {
        FL_ESC.U1EscActFrtTwoWheel = 1;
        if(FL_ESC.lcespu8IdbRiseRateCnt == FR_ESC.lcespu8IdbRiseRateCnt)
        {
           FL_ESC.lcespu8IdbRiseRateCnt =FL_ESC.lcespu8IdbRiseRateCnt +2; 
        }
        else
        {
            ;
        } 
    }
#endif
    else
    {
        FL_ESC.U1EscActFrtTwoWheel = 0;    
    }
    
    LCESP_vCalFrtRiseIntervalforIDB(&FL_ESC,&FL);    
    LCESP_vCalFrtDumpIntervalforIDB(&FL_ESC,&FL);    
    LCESP_vCalFrtdelTargetPressureforIDB(&FL_ESC,&FL);
    LCESP_vDctBBSTargetPressureforIDB(&FL_ESC, &FL); 
    LCESP_vMaxLimitFrtTargetPressIDB(&FL_ESC,&FL);
    LCESP_vMinLimitFrtTargetPressIDB(&FL_ESC,&FL);
    LCESP_vCalStorkeRecorverytime(&FL_ESC);
    LCESP_vDetStorkeRecorveryforESC(&FL_ESC);
	LCESP_vCalCurrentPresssforIDB(&FL_ESC,&FL);
    LCESP_vCalTargetPressforIDB(&FL_ESC,&FL);
    LCESP_vCalTargetPressRate(&FL_ESC,&FL);
	LCESP_vCalDelTargetPress(&FL_ESC);
	LCESP_vCalInletValveforIDB(&FL_ESC,&FL);
	#endif


  if(SLIP_CONTROL_NEED_FL==0)
  {
  	FL_ESC.mpress_rise_mode = 0;
  }
  else 
  {
  	;
  }
}
void LCESP_vSlipControlFR(void)
{
	LCESP_vCalSlipErrorF(&FR_ESC);
	LCESP_vCalSlipGainPressureF(&FR_ESC);
	LCESP_vCalSlipSignalF(&FR_ESC);
	LCESP_vCalVariableRiseRateF(&FR_ESC, &FR);
	LCESP_vCalPressureRateF(&FR_ESC);
	
	if(ABS_fz==0)
  {
    LCESP_vCountPressureF(&FR_ESC);
    LCESP_vCountLockF(&FR_ESC);
  }
  else
  {
  	;
  }
    
    LCESP_vDetectCompulsionRise(&FR_ESC);
    LCESP_vCalVariableThresholdF(&FR_ESC);
    
    LCESP_vDetectCompulsionHoldFR();
    
    LCESP_vDetectFullRise(&FR_ESC, &RR);
    LAESP_vActuateNcNovalveF(&FR_ESC, &FR);
    #if  (__IDB_LOGIC == ENABLE)
    
    if(lcu1US2WHControlFlag_RR==1)
    {
        FR_ESC.U1EscActFrtTwoWheel = 1;
        
       if((RR_ESC.lcespu8IdbRiseRateCnt < 1)&&(RR_ESC.esp_control_mode < 2 ))
        {
        	if(FR_ESC.lcespu8IdbRiseRateCnt == 0)
        	{	
        		FR_ESC.lcespu8IdbRiseRateCnt = FR_ESC.lcespu8IdbRiseRateCnt + 2;
        	}
        	else
        	{
        		;
        	}
        }
        else
        {
            ;
        } 
    }
#if (__CRAM==1) && (__CRAM_PRE_FRONT_W_CRTL == 1)&&(__EUROPE_ADAC ==1)
    else if(lcu1espCramPreact_FL==1)
    {
        FR_ESC.U1EscActFrtTwoWheel = 1;
        
        if(FR_ESC.lcespu8IdbRiseRateCnt == FL_ESC.lcespu8IdbRiseRateCnt)
        {
           FR_ESC.lcespu8IdbRiseRateCnt =FR_ESC.lcespu8IdbRiseRateCnt +2; 
        }
        else
        {
            ;
        } 
    }
#endif
#if(__ROP == 1)
    else if(ROP_REQ_FRT_Pre_FL==1)
    {
        FR_ESC.U1EscActFrtTwoWheel = 1;
        if(FR_ESC.lcespu8IdbRiseRateCnt == FL_ESC.lcespu8IdbRiseRateCnt)
        {
           FR_ESC.lcespu8IdbRiseRateCnt =FR_ESC.lcespu8IdbRiseRateCnt +2; 
        }
        else
        {
            ;
        }
    }
#endif
    else
    {
        FR_ESC.U1EscActFrtTwoWheel = 0;    
    }
    LCESP_vCalFrtRiseIntervalforIDB(&FR_ESC,&FR);    
    LCESP_vCalFrtDumpIntervalforIDB(&FR_ESC,&FR);    
    LCESP_vCalFrtdelTargetPressureforIDB(&FR_ESC,&FR);    
    LCESP_vDctBBSTargetPressureforIDB(&FR_ESC, &FR); 
    LCESP_vMaxLimitFrtTargetPressIDB(&FR_ESC,&FR);
    LCESP_vMinLimitFrtTargetPressIDB(&FR_ESC,&FR);
    LCESP_vCalStorkeRecorverytime(&FR_ESC);
	LCESP_vDetStorkeRecorveryforESC(&FR_ESC);
	LCESP_vCalCurrentPresssforIDB(&FR_ESC,&FR);
  	LCESP_vCalTargetPressforIDB(&FR_ESC,&FR);
    LCESP_vCalTargetPressRate(&FR_ESC,&FR);
	LCESP_vCalDelTargetPress(&FR_ESC);
	LCESP_vCalInletValveforIDB(&FR_ESC,&FR);
	#endif
    
    if(SLIP_CONTROL_NEED_FR==0)
    {
    	FR_ESC.mpress_rise_mode = 0;
    }
    else 
    {
    	;
    }
}

void LCESP_vSlipControlRL(void)
{
	LCESP_vCalSlipErrorR(&RL_ESC);
	LCESP_vCalSlipGainPressureR(&RL_ESC);
	LCESP_vCalSlipSignalR(&RL_ESC);
	LCESP_vCalPressureRateR(&RL_ESC);
	
	if(ABS_fz==0)
  {
    LCESP_vCountPressureR(&RL_ESC);   
    LCESP_vCountLockR(&RL_ESC);
  }
  else
  {
  	;
  }
  
  LCESP_vCalVariableThresholdR(&RL_ESC);
  LCESP_vDetectCompulsionHoldRL();
  LAESP_vActuateNcNovalveR(&RL_ESC, &RL);
  #if  (__IDB_LOGIC == ENABLE)

  LCESP_vCalRearRiseIntervalforIDB(&RL_ESC,&RL);    
  LCESP_vCalRearDumpIntervalforIDB(&RL_ESC,&RL);    
  LCESP_vCalReardelTargetPressureforIDB(&RL_ESC,&RL);    
  LCESP_vDctBBSTargetPressureforIDB(&RL_ESC, &RL); 
  LCESP_vMaxLimitRearTargetPressIDB(&RL_ESC,&RL);
  LCESP_vMinLimitRearTargetPressIDB(&RL_ESC,&RL);
  LCESP_vCalStorkeRecorverytime(&RL_ESC);
  LCESP_vDetStorkeRecorveryforESC(&RL_ESC);
  	LCESP_vCalCurrentPresssforIDB(&RL_ESC,&RL);
  LCESP_vCalTargetPressforIDB(&RL_ESC, &RL);
  LCESP_vCalTargetPressRate(&RL_ESC,&RL);
  	LCESP_vCalDelTargetPress(&RL_ESC);
  LCESP_vCalInletValveforIDB(&RL_ESC, &RL);
  #endif  
}
void LCESP_vSlipControlRR(void)
{
	LCESP_vCalSlipErrorR(&RR_ESC);
	LCESP_vCalSlipGainPressureR(&RR_ESC);
	LCESP_vCalSlipSignalR(&RR_ESC);  
  	LCESP_vCalPressureRateR(&RR_ESC); 
    
  	if(ABS_fz==0)
  	{
    	LCESP_vCountPressureR(&RR_ESC);
    	LCESP_vCountLockR(&RR_ESC);
  	}
  	else
  	{
  		;
  	}
    
  	LCESP_vCalVariableThresholdR(&RR_ESC);    
  	LCESP_vDetectCompulsionHoldRR();
  	LAESP_vActuateNcNovalveR(&RR_ESC, &RR);
  	#if  (__IDB_LOGIC == ENABLE)

  	LCESP_vCalRearRiseIntervalforIDB(&RR_ESC,&RR);    
  	LCESP_vCalRearDumpIntervalforIDB(&RR_ESC,&RR);    
  	LCESP_vCalReardelTargetPressureforIDB(&RR_ESC,&RR);
  	LCESP_vDctBBSTargetPressureforIDB(&RR_ESC, &RR);     
  	LCESP_vMaxLimitRearTargetPressIDB(&RR_ESC, &RR);
  	LCESP_vMinLimitRearTargetPressIDB(&RR_ESC,&RR);
  	LCESP_vCalStorkeRecorverytime(&RR_ESC);
  	LCESP_vDetStorkeRecorveryforESC(&RR_ESC);
	LCESP_vCalCurrentPresssforIDB(&RR_ESC,&RR);
  	LCESP_vCalTargetPressforIDB(&RR_ESC, &RR);
  	LCESP_vCalTargetPressRate(&RR_ESC,&RR);
  	LCESP_vCalDelTargetPress(&RR_ESC);
	LCESP_vCalInletValveforIDB(&RR_ESC, &RR);
  	#endif  
  
}

#else
void LCESP_vSlipControlF(void)
{
	LCESP_vCalSlipErrorF();
    LCESP_vCalSlipGainPressureF();
    LCESP_vCalSlipSignalF();
    LCESP_vCalPressureRateF();      
}
void LCESP_vSlipControlR(void)
{
	LCESP_vCalSlipErrorR();
    LCESP_vCalSlipGainPressureR();
    LCESP_vCalSlipSignalR();
    LCESP_vCalPressureRateR(); 
      
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalSlipErrorF(struct ESC_STRUCT *WL_ESC)
{
    WL_ESC->slip_error=WL_ESC->slip_target-WL_ESC->slip_measurement;

    WL_ESC->lcescs16CalDelTargetSlip =  WL_ESC->slip_error; 

    WL_ESC->slip_error_dot = WL_ESC->slip_measurement_prev - WL_ESC->slip_measurement;
    
    if(ABS_fz==0) 
    {
    	WL_ESC->slip_error_dot= LCESP_s16Lpf1Int(WL_ESC->slip_error_dot,WL_ESC->slip_error_dot_prev,L_U8FILTER_GAIN_10MSLOOP_3_5HZ); 
    }
    else
    {
    	;
    }
    WL_ESC->slip_error_dot_prev = WL_ESC->slip_error_dot;  

    WL_ESC->slip_target_prev=WL_ESC->slip_target;
    WL_ESC->slip_error_prev=WL_ESC->slip_error;
    WL_ESC->slip_measurement_prev=WL_ESC->slip_measurement;

}
void LCESP_vCalSlipErrorR(struct ESC_STRUCT *WL_ESC)
{
    WL_ESC->slip_error=WL_ESC->slip_target-WL_ESC->slip_measurement;
    
    WL_ESC->lcescs16CalDelTargetSlip =  WL_ESC->slip_error; 
   	
   	WL_ESC->slip_error_dot = WL_ESC->slip_measurement_prev - WL_ESC->slip_measurement;
   
    if(ABS_fz==0) 
    {
    	WL_ESC->slip_error_dot= LCESP_s16Lpf1Int(WL_ESC->slip_error_dot,WL_ESC->slip_error_dot_prev,L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
    }
    else
    {
    	;
    }
	
    WL_ESC->slip_error_dot_prev = WL_ESC->slip_error_dot;  

    WL_ESC->slip_target_prev=WL_ESC->slip_target;
    WL_ESC->slip_error_prev=WL_ESC->slip_error;
	WL_ESC->slip_measurement_prev = WL_ESC->slip_measurement;
}
	#if  (__IDB_LOGIC == ENABLE)
void LCESP_vCalFrtRiseIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)    
{
   

    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_HM_Frt_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_HM_Frt_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_HM_Frt_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_MM_Frt_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_MM_Frt_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_MM_Frt_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_LM_Frt_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_LM_Frt_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_LM_Frt_Rise_rate);        
    }
    else if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_HM_Frt_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_HM_Frt_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_HM_Frt_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_MM_Frt_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_MM_Frt_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_MM_Frt_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_LM_Frt_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_LM_Frt_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_LM_Frt_Rise_rate);
    }
    else
    {    
        esp_tempW0 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_HM_Frt_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_HM_Frt_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_HM_Frt_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_MM_Frt_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_MM_Frt_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_MM_Frt_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_LM_Frt_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_LM_Frt_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_LM_Frt_Rise_rate);  
    }
                                                                                                                                               
    WL_ESC->lcesps16IdbRiseInterval = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0,
                                    esp_tempW1, esp_tempW2);
                                        
    if(WL_ESC->U1EscActFrtTwoWheel==1)
    {
        WL_ESC->lcesps16IdbRiseInterval = WL_ESC->lcesps16IdbRiseInterval+2;      
    }
    else if(lcu1US2WHControlFlag == 1)
    {        
        WL_ESC->lcesps16IdbRiseInterval = WL_ESC->lcesps16IdbRiseInterval+2;      
    }
    else
    {
	    ;
    }
  
}
void LCESP_vCalRearRiseIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL) 
{
    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_HM_Rear_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_HM_Rear_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_HM_Rear_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_MM_Rear_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_MM_Rear_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_MM_Rear_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30bar_LM_Rear_Rise_rate, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60bar_LM_Rear_Rise_rate,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90bar_LM_Rear_Rise_rate);
    }        
    else if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_HM_Rear_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_HM_Rear_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_HM_Rear_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_MM_Rear_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_MM_Rear_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_MM_Rear_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10bar_LM_Rear_Rise_rate, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20bar_LM_Rear_Rise_rate,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30bar_LM_Rear_Rise_rate);
    }
    else
    {
        esp_tempW0 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_HM_Rear_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_HM_Rear_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_HM_Rear_Rise_rate);
        esp_tempW1 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_MM_Rear_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_MM_Rear_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_MM_Rear_Rise_rate);
        esp_tempW2 = LCESP_s16IInter3Point(vrefk,  VREF_K_40_KPH,(int16_t)U8_40Kph_LM_Rear_Rise_rate, 
                                                   VREF_K_60_KPH,(int16_t)U8_60Kph_LM_Rear_Rise_rate,
                                                   VREF_K_80_KPH,(int16_t)U8_80Kph_LM_Rear_Rise_rate);
    }      
   
    WL_ESC->lcesps16IdbRiseInterval = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0,
                                    esp_tempW1, esp_tempW2);
                                              
    if(lcu1US2WHControlFlag == 1)
    {        
        WL_ESC->lcesps16IdbRiseInterval = WL_ESC->lcesps16IdbRiseInterval + 3;      
    }
    else
    {
       ;
    }
  
}
void LCESP_vCalFrtDumpIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)   
{
    if(WL_ESC->U1EscActFrtTwoWheel==1)
    {
        WL_ESC->lcesps16IdbDumpInterval = WL_ESC->rate_interval + 2;        
    }
    else
    {
        WL_ESC->lcesps16IdbDumpInterval = WL_ESC->rate_interval;
    }
}
void LCESP_vCalRearDumpIntervalforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)    
{
    if(lcu1US2WHControlFlag == 1)
    {        
        WL_ESC->lcesps16IdbDumpInterval = WL_ESC->rate_interval;        
    }
    else
    {
        WL_ESC->lcesps16IdbDumpInterval = WL_ESC->rate_interval;
    }   
}
void LCESP_vCalFrtdelTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    
    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_HM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_HM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_HM_Frt_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_MM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_MM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_MM_Frt_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_LM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_LM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_LM_Frt_Rise_delP);  
    }
    else if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_HM_Frt_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_HM_Frt_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_HM_Frt_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_MM_Frt_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_MM_Frt_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_MM_Frt_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_LM_Frt_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_LM_Frt_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_LM_Frt_Rise_delP);
    }    
    else
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        
        esp_tempW7 = LCESP_s16IInter3Point(vrefk,       VREF_K_40_KPH,(int16_t)U8_40Kph_HM_Frt_Rise_delP, 
                                                        VREF_K_60_KPH,(int16_t)U8_60Kph_HM_Frt_Rise_delP,
                                                        VREF_K_80_KPH,(int16_t)U8_80Kph_HM_Frt_Rise_delP);
        esp_tempW8 = LCESP_s16IInter3Point(vrefk,       VREF_K_40_KPH,(int16_t)U8_40Kph_MM_Frt_Rise_delP, 
                                                        VREF_K_60_KPH,(int16_t)U8_60Kph_MM_Frt_Rise_delP,
                                                        VREF_K_80_KPH,(int16_t)U8_80Kph_MM_Frt_Rise_delP);
        esp_tempW9 = LCESP_s16IInter3Point(vrefk,       VREF_K_40_KPH,(int16_t)U8_40Kph_LM_Frt_Rise_delP, 
                                                        VREF_K_60_KPH,(int16_t)U8_60Kph_LM_Frt_Rise_delP,
                                                        VREF_K_80_KPH,(int16_t)U8_80Kph_LM_Frt_Rise_delP);
                                                        
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_HM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_HM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_HM_Frt_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_MM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_MM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_MM_Frt_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_LM_Frt_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_LM_Frt_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_LM_Frt_Rise_delP);
        esp_tempW0 = esp_tempW0+esp_tempW7;
        esp_tempW1 = esp_tempW1+esp_tempW8;
        esp_tempW2 = esp_tempW2+esp_tempW9;                                            
                                                        
    } 
    
    WL_ESC->lcesps16IdbriseDelTp = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0,
                                    esp_tempW1, esp_tempW2);
    if((ldespu1ESC2ndCycle==1)&&(WL->s16_Estimated_Active_Press < MPRESS_20BAR))
    {
        WL_ESC->lcesps16IdbriseDelTp = WL_ESC->lcesps16IdbriseDelTp + MPRESS_3BAR;   
    }
    else if(lcu1US2WHControlFlag==1)
    {
/*        WL_ESC->lcesps16IdbriseDelTp = MPRESS_4BAR;  */
        WL_ESC->lcesps16IdbriseDelTp = LCESP_s16IInter3Point(WL->s16_Estimated_Active_Press, MPRESS_5BAR,MPRESS_4BAR, 
                                                       										MPRESS_10BAR,MPRESS_4BAR,
                                                       										MPRESS_20BAR,MPRESS_2BAR);
    }
    else
    {
        ;   
    }   

    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_HM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_HM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_HM_Frt_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_MM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_MM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_MM_Frt_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_LM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_LM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_LM_Frt_Dump_delP);  
    }
    else if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3, MPRESS_10BAR,(int16_t)U8_CBS_10Bar_HM_Frt_Dump_delP, 
                                                       MPRESS_20BAR,(int16_t)U8_CBS_20Bar_HM_Frt_Dump_delP,
                                                       MPRESS_30BAR,(int16_t)U8_CBS_30Bar_HM_Frt_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3, MPRESS_10BAR,(int16_t)U8_CBS_10Bar_MM_Frt_Dump_delP, 
                                                       MPRESS_20BAR,(int16_t)U8_CBS_20Bar_MM_Frt_Dump_delP,
                                                       MPRESS_30BAR,(int16_t)U8_CBS_30Bar_MM_Frt_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3, MPRESS_10BAR,(int16_t)U8_CBS_10Bar_LM_Frt_Dump_delP, 
                                                       MPRESS_20BAR,(int16_t)U8_CBS_20Bar_LM_Frt_Dump_delP,
                                                       MPRESS_30BAR,(int16_t)U8_CBS_30Bar_LM_Frt_Dump_delP);
    }
    else
    { 
        esp_tempW3= WL->s16_Estimated_Active_Press;                                       
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_HM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_HM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_HM_Frt_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_MM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_MM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_MM_Frt_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_LM_Frt_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_LM_Frt_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_LM_Frt_Dump_delP);                                            
    }
    WL_ESC->lcesps16IdbdumpDelTp = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW4,
                                    esp_tempW5, esp_tempW6);
}   
void LCESP_vCalReardelTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{

    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_HM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_HM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_HM_Rear_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_MM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_MM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_MM_Rear_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_LM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_LM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_LM_Rear_Rise_delP);  
    }
    if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_HM_Rear_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_HM_Rear_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_HM_Rear_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_MM_Rear_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_MM_Rear_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_MM_Rear_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_LM_Rear_Rise_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_LM_Rear_Rise_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_LM_Rear_Rise_delP);
    }
    else
    {
        esp_tempW3= WL->s16_Estimated_Active_Press; 
        esp_tempW0 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_HM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_HM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_HM_Rear_Rise_delP);
        esp_tempW1 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_MM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_MM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_MM_Rear_Rise_delP);
        esp_tempW2 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_LM_Rear_Rise_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_LM_Rear_Rise_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_LM_Rear_Rise_delP);
    }
        
    WL_ESC->lcesps16IdbriseDelTp = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0,
                                    esp_tempW1, esp_tempW2); 
                            

    if((PARTIAL_BRAKE == 0)&&(ABS_fz==1))
    {
        esp_tempW3= lcidbs16activebrakepress - WL->s16_Estimated_Active_Press;
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_HM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_HM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_HM_Rear_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_MM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_MM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_MM_Rear_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_ABS_30Bar_LM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_ABS_60Bar_LM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_ABS_90Bar_LM_Rear_Dump_delP);  
    }
    else if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR))
    {
        esp_tempW3= WL->s16_Estimated_Active_Press;
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_HM_Rear_Dump_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_HM_Rear_Dump_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_HM_Rear_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_MM_Rear_Dump_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_MM_Rear_Dump_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_MM_Rear_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_10BAR,(int16_t)U8_CBS_10Bar_LM_Rear_Dump_delP, 
                                                        MPRESS_20BAR,(int16_t)U8_CBS_20Bar_LM_Rear_Dump_delP,
                                                        MPRESS_30BAR,(int16_t)U8_CBS_30Bar_LM_Rear_Dump_delP);
    }    
    else
    { 
        esp_tempW3= WL->s16_Estimated_Active_Press;
        esp_tempW4 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_HM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_HM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_HM_Rear_Dump_delP);
        esp_tempW5 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_MM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_MM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_MM_Rear_Dump_delP);
        esp_tempW6 = LCESP_s16IInter3Point(esp_tempW3,  MPRESS_30BAR,(int16_t)U8_30Bar_LM_Rear_Dump_delP, 
                                                        MPRESS_60BAR,(int16_t)U8_60Bar_LM_Rear_Dump_delP,
                                                        MPRESS_90BAR,(int16_t)U8_90Bar_LM_Rear_Dump_delP);
    }
        
    WL_ESC->lcesps16IdbdumpDelTp = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW4,
                                    esp_tempW5, esp_tempW6);                                     
                                  
}
void LCESP_vDctBBSTargetPressureforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    if((PARTIAL_BRAKE == 1)&&(lcidbs16activebrakepress > MPRESS_0G5BAR)&&(ABS_fz == 0))/*ABS_fz == 0 ?? */
        {
/*    	if((WL->s16_Estimated_Active_Press) > (lcidbs16activebrakepress))
            {
            if((ldahbs16TargetPressRate10ms) < (MPRESS_4BAR)) 
            {
                WL_ESC->lcespu8CntBbsEsc = 1; 
            }
            else 
            if((WL->s16_Estimated_Active_Press > MPRESS_30BAR)&&(lcidbs16activebrakepress < MPRESS_10BAR))
            {
                WL_ESC->lcespu8CntBbsEsc = 2; 
            }
            else
            {
                WL_ESC->lcespu8CntBbsEsc = 5; 
            }
        }
        else
        {*/
            WL_ESC->lcespu8CntBbsEsc = 3;   
/*        }*/
    }
    else if(BTC_fz == 1)
    {
        WL_ESC->lcespu8CntBbsEsc = 4;  
    }
    else
    {
        WL_ESC->lcespu8CntBbsEsc = 0; 
    }
    
}
void LCESP_vMaxLimitFrtTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{   
	esp_tempW3 = McrAbs(WL_ESC->true_slip_target);
    
	if(lcu1US2WHControlFlag)
    {
		esp_tempW0= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_15BAR,
	                                                   WHEEL_SLIP_20, MPRESS_20BAR,
	                                                   WHEEL_SLIP_40, MPRESS_30BAR);
	                                                   
		esp_tempW1= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_15BAR,
	                                                   WHEEL_SLIP_20, MPRESS_20BAR,
	                                                   WHEEL_SLIP_40, MPRESS_30BAR);
	                                                   
		esp_tempW2= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_15BAR,
	                                                   WHEEL_SLIP_20, MPRESS_20BAR,
	                                                   WHEEL_SLIP_40, MPRESS_30BAR);
    }
	else
    {
		esp_tempW0= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_30BAR,
	                                                   WHEEL_SLIP_20, MPRESS_60BAR,
	                                                   WHEEL_SLIP_40, MPRESS_120BAR);
	                                                   
		esp_tempW1= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_20BAR,
	                                                   WHEEL_SLIP_20, MPRESS_50BAR,
	                                                   WHEEL_SLIP_40, MPRESS_100BAR);
	                                                   
		esp_tempW2= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_20BAR,
	                                                   WHEEL_SLIP_20, MPRESS_50BAR,
	                                                   WHEEL_SLIP_40, MPRESS_100BAR);		
    }

    WL_ESC->lcesps16LimitIdbTargetP = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW0, 
                                esp_tempW1, esp_tempW2);     
                                                               
    if(lcidbs16activebrakepress > MPRESS_0G5BAR)
    {
    	WL_ESC->lcesps16LimitIdbTargetP = MPRESS_200BAR;
    }                                              
}
void LCESP_vMaxLimitRearTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
	esp_tempW3 = McrAbs(WL_ESC->true_slip_target);
	
	esp_tempW0= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_20BAR,
                                                   WHEEL_SLIP_20, MPRESS_30BAR,
                                                   WHEEL_SLIP_40, MPRESS_40BAR);
                                                   
	esp_tempW1= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_20BAR,
                                                   WHEEL_SLIP_20, MPRESS_30BAR,
                                                   WHEEL_SLIP_40, MPRESS_40BAR);
                                                   
	esp_tempW2= LCESP_s16IInter3Point(esp_tempW3,  WHEEL_SLIP_10, MPRESS_20BAR,
                                                   WHEEL_SLIP_20, MPRESS_30BAR,
                                                   WHEEL_SLIP_40, MPRESS_40BAR);

    WL_ESC->lcesps16LimitIdbTargetP = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW0, 
                                esp_tempW1, esp_tempW2);
    
    if(lcidbs16activebrakepress > MPRESS_0G5BAR)
    {
    	WL_ESC->lcesps16LimitIdbTargetP = MPRESS_200BAR;
    }                                                           
}
void LCESP_vMinLimitFrtTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    WL_ESC->lcesps16MinIdbTargetP = LCESP_s16FindRoadDependatGain( esp_mu, MPRESS_20BAR, 
                                MPRESS_20BAR, MPRESS_20BAR);                                                    
                                                   
}
void LCESP_vMinLimitRearTargetPressIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    WL_ESC->lcesps16MinIdbTargetP = LCESP_s16FindRoadDependatGain( esp_mu, MPRESS_20BAR, 
                                MPRESS_20BAR, MPRESS_20BAR);                                                    
                                                   
}
void LCESP_vCalCurrentPresssforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{   
	
    WL_ESC->lcesps16IdbTarPressold = WL_ESC->lcesps16IdbTarPress;
    
	if(WL_ESC->slip_control_need_counter < 1)
    {
        WL_ESC->lcesps16IdbTarPressold =  WL->s16_Estimated_Active_Press; 
    }
    else
    {
        ;       
    }
         
	if((liu1BCP1SenFaultDet == 0)&&(ABS_fz == 1))/*Valid estimated press*/
    {
		WL_ESC->lcesps16IdbCurrentPress = WL_ESC->lcesps16IdbTarPressold;//WL->s16_Estimated_Active_Press;
    }
    else
    {
		WL_ESC->lcesps16IdbCurrentPress = WL_ESC->lcesps16IdbTarPressold;
	}
}
void LCESP_vCalTargetPressforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{   

    WL_ESC->lcesps16IdbriseDelTp = WL_ESC->lcesps16IdbriseDelTp+WL_ESC->lcesps16StoreDeltTp;
    
       
    
    if((WL_ESC->slip_control <= WL_ESC->slip_rise_thr_change)
    	||((WL_ESC->slip_control_need_counter<L_U8_TIME_10MSLOOP_100MS)&&(lcidbs16activebrakepress < MPRESS_0G5BAR)&&(WL->s16_Estimated_Active_Press < WL_ESC->lcesps16LimitIdbTargetP)))
    {
//    	WL_ESC->lcesps16IdbriseDelTp = WL_ESC->lcesps16IdbriseDelTp+WL_ESC->lcesps16CompDp;
        WL_ESC->U1StrWheelValveCnt = 0;
        
        WL_ESC->lcespu8IdbDumpRateCnt=0;
        WL_ESC->lcespu8IdbRiseRateCnt = WL_ESC->lcespu8IdbRiseRateCnt +1;           
            
        WL_ESC->esp_control_mode=1;  
        
        if(WL_ESC->lcespu8IdbRiseRateCnt < 2)
        {
        	LCESP_vCalTargetPressComp(WL_ESC,WL);
        	
        	if(U1EscAfterStrokeR == 1)
        	{
        		WL_ESC->TEMP_HV_VL=1;
                WL_ESC->TEMP_AV_VL=0;
                WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress;
                WL_ESC->lcespu1DctMuxPrio = 0;
            }
            else if(WL_ESC->lcespu8CntBbsEsc ==3)
            {
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0;
                WL_ESC->U1StrWheelValveCnt = 1;   
                WL_ESC->lcespu1DctMuxPrio = 1; 
                if(WL_ESC->lcesps16IdbCurrentPress > lcidbs16activebrakepress)                
                {
                    WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress + WL_ESC->lcesps16IdbriseDelTp;  /*WL_ESC->lcesps16IdbTarPressold +*/                 
                
                }
                else
                {
                    WL_ESC->lcesps16IdbTarPress = lcidbs16activebrakepress + WL_ESC->lcesps16IdbriseDelTp;
                }
            }
            else
            {
            	if(WL_ESC->lcesps16IdbTarPress > WL_ESC->lcesps16LimitIdbTargetP)
				{
				  	WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress;
				  	WL_ESC->TEMP_HV_VL=1;
	                WL_ESC->TEMP_AV_VL=0; 
	                WL_ESC->lcespu1DctMuxPrio = 0;
                }
                else
                { 
	                WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress +WL_ESC->lcesps16IdbriseDelTp;               
	                WL_ESC->TEMP_HV_VL=0;
	                WL_ESC->TEMP_AV_VL=0;  
	                WL_ESC->lcespu1DctMuxPrio = 1;                 
                }                   
            }                 
        }
        else
        {
            if(U1EscAfterStrokeR_OLD== 1)
            {
                WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress+WL_ESC->lcesps16StoreDeltTp; 
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0; 
                WL_ESC->U1StrWheelValveCnt = 1;             
                WL_ESC->lcespu1DctMuxPrio = 1;            
            }
            else
            {
                WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress; 
                WL_ESC->TEMP_HV_VL=1;
                WL_ESC->TEMP_AV_VL=0;           
                WL_ESC->lcespu1DctMuxPrio = 0;          
            }
        }                
               
        if(WL_ESC->lcespu8IdbRiseRateCnt > WL_ESC->lcesps16IdbRiseInterval)
        {
           WL_ESC->lcespu8IdbRiseRateCnt = 0; 
        }
        else
        {
            ;   
        }            
    }
    else if((WL_ESC->slip_control>WL_ESC->slip_rise_thr_change)&&(WL_ESC->slip_control<WL_ESC->slip_dump_thr_change))
    {
       WL_ESC->lcespu8IdbDumpRateCnt=0;
       WL_ESC->lcespu8IdbRiseRateCnt=0;
       WL_ESC->U1StrWheelValveCnt = 0;
	   WL_ESC->lcespu1DctMuxPrio = 0;
		if((U1EscAfterStrokeR_OLD == 1)&&(WL_ESC->lcesps16StoreDeltTp >0))
		{
		   WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress+WL_ESC->lcesps16StoreDeltTp;     		
	       WL_ESC->TEMP_HV_VL=0;
	       WL_ESC->TEMP_AV_VL=0;
		}
		else
		{              
	    	WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress;     		
        	WL_ESC->TEMP_HV_VL=1;
        	WL_ESC->TEMP_AV_VL=0;
	    }
        if((WL_ESC->U1EscActFrtTwoWheel==1) ||(lcu1US2WHControlFlag ==1))
	    {
	    	WL_ESC->U1StrWheelValveCnt = 1;
	    }
	    else
	    {
	        ;
	    } 
	}
    else
    {
        WL_ESC->lcespu8IdbDumpRateCnt= WL_ESC->lcespu8IdbDumpRateCnt+1;
        WL_ESC->lcespu8IdbRiseRateCnt=0;
        WL_ESC->esp_control_mode=4;
        if(WL_ESC->lcespu8IdbDumpRateCnt< 2)
        {
        	if((lcu1US2WHControlFlag==1)&&(WL_ESC->slip_measurement >  WL_ESC->true_slip_target))
        	{
        		WL_ESC->lcesps16IdbTarPress	= WL_ESC->lcesps16IdbCurrentPress ;
        	}
        	else
        	{                    
            WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress - WL_ESC->lcesps16IdbdumpDelTp;    
				if(WL_ESC->lcesps16IdbTarPress < MPRESS_0G5BAR)
				{
					WL_ESC->lcesps16IdbTarPress	= MPRESS_0G5BAR;//WL_ESC->lcesps16IdbTarPressold ;  	
				}
				else
				{
					;	
				}
			}         
        }
        else
        {
             WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbCurrentPress;   
        }
        
        if(WL_ESC->lcespu8IdbDumpRateCnt> WL_ESC->lcesps16IdbDumpInterval)
        {
            WL_ESC->lcespu8IdbDumpRateCnt=0;
        }
        else
        {
            ;   
        }
        if((WL_ESC->U1EscActFrtTwoWheel==1) ||(lcu1US2WHControlFlag ==1))
        {
            WL_ESC->U1StrWheelValveCnt = 1;
        }
        else
        {
	        ;
        }
                    
    }
	
    WL_ESC->lcesps16EstpressOld = WL->s16_Estimated_Active_Press;

	}
void LCESP_vCalDelTargetPress(struct ESC_STRUCT *WL_ESC)
{
	WL_ESC->lcesps16IdbDelTarPress  = WL_ESC->lcesps16IdbTarPress - WL_ESC->lcesps16IdbTarPressold;
}
void LCESP_vCalTargetPressComp(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{				
	static int16_t lcesps16ThresDelp;
	static int16_t lcesps16DelEstPress;
				
	lcesps16DelEstPress = 0;
	lcesps16ThresDelp = 0;	
	
	lcesps16DelEstPress= WL_ESC->lcesps16IdbTarPress - WL->s16_Estimated_Active_Press;
	
	lcesps16ThresDelp = ((int32_t)(WL_ESC->lcesps16IdbriseDelTp * 90)/100);
	
	if(lcesps16DelEstPress > 0)
	{	
		if(lcesps16DelEstPress > lcesps16ThresDelp)
		{
			 		WL_ESC->lcesps16CompDp = 0;
		}
		else
		{
			WL_ESC->lcesps16CompDp =  WL_ESC->lcesps16IdbriseDelTp - lcesps16DelEstPress;
		}
	}
	else
	{
		WL_ESC->lcesps16CompDp = 0;	
	}
}

void LCESP_vCalInletValveforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    if((WL_ESC->slip_control <= WL_ESC->slip_rise_thr_change)
    	||((WL_ESC->slip_control_need_counter<L_U8_TIME_10MSLOOP_300MS)&&(lcidbs16activebrakepress < MPRESS_0G5BAR)&&(WL->s16_Estimated_Active_Press < WL_ESC->lcesps16LimitIdbTargetP)))
    {
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
    	LCESP_vResetVlvDumpTi(WL_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */
    }
    else if((WL_ESC->slip_control>WL_ESC->slip_rise_thr_change)&&(WL_ESC->slip_control<WL_ESC->slip_dump_thr_change))
    {
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
    	LCESP_vResetVlvDumpTi(WL_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */
	}
    else
    {
		#if (VALVE_OPEN_TIME_EXTENTION == ENABLE)
    	LCESP_vCalcExtdVlvDumpTi(WL, WL_ESC);
		#endif /* (VALVE_OPEN_TIME_EXTENTION == ENABLE) */

        if((WL_ESC->lcespu8IdbDumpRateCnt <= WL_ESC->u8WhlVlvDumpExtdScan)&&(WL_ESC->lcespu8IdbDumpRateCnt > 0))
        {
        	if((lcu1US2WHControlFlag==1)&&(WL_ESC->slip_measurement >  WL_ESC->true_slip_target))
        	{
        		LCESP_vResetVlvDumpTi(WL_ESC);
 				WL_ESC->TEMP_HV_VL=1;
	            WL_ESC->TEMP_AV_VL=0;
        	}
        	else
        	{
        		WL_ESC->TEMP_HV_VL=1;
        		WL_ESC->TEMP_AV_VL=1;
        	}
        }
        else
        {
        	LCESP_vResetVlvDumpTi(WL_ESC);
        	WL_ESC->TEMP_HV_VL=1;
        	WL_ESC->TEMP_AV_VL=0;
        }
    }
}

void LCESP_vCalTargetPressRate(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
	static uint16_t lcespu16MaxTprate;
	 
	if(WL_ESC->esp_control_mode < 3)
    {
		WL_ESC->lcesps16IdbTarPRate = ((int32_t)(WL_ESC->lcesps16IdbriseDelTp*10)/WL_ESC->lcesps16IdbRiseInterval);
		
		lcespu16MaxTprate = LCESP_s16IInter3Point(WL->s16_Estimated_Active_Press,  MPRESS_5BAR,100, 
                                                        MPRESS_10BAR,200,
                                                        MPRESS_60BAR,400);
                                                        
        if( WL_ESC->lcesps16IdbTarPRate > lcespu16MaxTprate)  WL_ESC->lcesps16IdbTarPRate = lcespu16MaxTprate;                                          
	}
	else if(WL_ESC->esp_control_mode > 3)
    {
		WL_ESC->lcesps16IdbTarPRate = -((int32_t)(WL_ESC->lcesps16IdbdumpDelTp*10)/WL_ESC->lcesps16IdbDumpInterval);
    }
    else
    {
		WL_ESC->lcesps16IdbTarPRate = WL_ESC->lcesps16IdbTarPRate;	
    }  
}
void LCESP_vCalStorkeRecorverytime(struct ESC_STRUCT *WL_ESC)
	{
	if(lcidbs16activebrakepress > MPRESS_0G5BAR)
    {
    	if(WL_ESC->slip_control_need_counter > 30)
    	{
    		if(WL_ESC->esp_control_mode >= 3)
    		{
    			WL_ESC->lcescs16AllowStrRetime = 90;
    	}
    	else
    	{
    			WL_ESC->lcescs16AllowStrRetime = -10;
    	} 
    }
    else
    {
    		WL_ESC->lcescs16AllowStrRetime = -10;
    	}
	}
	else
	{
    	if((delta_yaw < (-YAW_4DEG))&&(WL_ESC->slip_control_need_counter > 20))
    	{
    		if(WL_ESC->esp_control_mode >= 3)
    		{
    			WL_ESC->lcescs16AllowStrRetime = 100;
	} 
    		else
    	{
    			WL_ESC->lcescs16AllowStrRetime = -10;
    		}    			
    	}
    	else
    	{
    		WL_ESC->lcescs16AllowStrRetime = -10;
    	}
    	}         
    }
void LCESP_vDetStorkeRecorveryforESC(struct ESC_STRUCT *WL_ESC)
{    
    u8EscStrokeState_old = u8EscStrokeState;      
    u8EscStrokeState = lis8StrkRcvrCtrlState_Rx;//(int8_t)Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
    	
    if((U1EscAfterStrokeR ==0)&&(u8EscStrokeState > 0)&&(u8EscStrokeState < 100))
    {
    	WL_ESC->lcesps16StoreDeltTp = WL_ESC->lcesps16IdbriseDelTp;
    	U1EscAfterStrokeR = 1; 
    	U1EscAfterStrokeR_OLD = 0;
    }
    else if(U1EscAfterStrokeR == 1)
    {
    	if(u8EscStrokeState == 0)
    	{
    		WL_ESC->lcesps16StoreDeltTp = WL_ESC->lcesps16IdbriseDelTp;
    		U1EscAfterStrokeR = 0;
	    	U1EscAfterStrokeR_OLD = 1;
    }
    else
    {
	    	WL_ESC->lcesps16StoreDeltTp = WL_ESC->lcesps16IdbriseDelTp;
	    	U1EscAfterStrokeR = 1;
	   		U1EscAfterStrokeR_OLD = 0;
    }
}
    else
{
    	WL_ESC->lcesps16StoreDeltTp = 0; 
    	U1EscAfterStrokeR = 0; 
    	U1EscAfterStrokeR_OLD = 0;
    }	
}
    #endif /*__IDB_LOGIC*/
#else
void LCESP_vCalSlipErrorF(void)
{
    slip_error_front=slip_target_front-slip_measurement_front;

    lcescs16CalDelTargetSlipf =  slip_error_front; 

    slip_error_dot_front = slip_measurement_front_prev - slip_measurement_front;
    
    if(ABS_fz==0) 
    {
    	slip_error_dot_front= LCESP_s16Lpf1Int(slip_error_dot_front,slip_error_dot_prev_front,L_U8FILTER_GAIN_10MSLOOP_3_5HZ); 
    }
    else
    {
    	;
    }
    slip_error_dot_prev_front = slip_error_dot_front;  

    slip_target_prev_front=slip_target_front;
    slip_error_prev_front=slip_error_front;
    slip_measurement_front_prev=slip_measurement_front;

}
void LCESP_vCalSlipErrorR(void)
{
    slip_error_rear=slip_target_rear-slip_measurement_rear;
    
    lcescs16CalDelTargetSlipr =  slip_error_rear; 
   	
   	slip_error_dot_rear = slip_measurement_rear_prev - slip_measurement_rear;
   
    if(ABS_fz==0) 
    {
    	slip_error_dot_rear= LCESP_s16Lpf1Int(slip_error_dot_rear,slip_error_dot_prev_rear,L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
    }
    else
    {
    	;
    }
	
    slip_error_dot_prev_rear = slip_error_dot_rear;  

    slip_target_prev_rear=slip_target_rear;
    slip_error_prev_rear=slip_error_rear;
	slip_measurement_rear_prev = slip_measurement_rear;
}
#endif

void LCESP_vCalSpeedSlipGain(void)
{
    int16_t v_v1 = S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, v_v2 = S16_ESP_SLIP_SPEED_GAIN_DEP_L_V; 
    int16_t v_v3 = S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, v_v4 =S16_ESP_SLIP_SPEED_GAIN_DEP_H_V; 
    int16_t v_g1, v_g2, v_g3;
   
    v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_L, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_L, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_L); 
    v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_M, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_M, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_M);
    v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_H, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_H, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_H); 
            
	#if	( __Ext_Under_Ctrl == 1 )  
		
    if( EXTREME_UNDER_CONTROL == 1 )
    {
  	  v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_LSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_LSP, S16_EXT_UND_SLIP_SP_WEG_LMU_LSP); 
      v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_MSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_MSP, S16_EXT_UND_SLIP_SP_WEG_LMU_MSP);
      v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_HSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_HSP, S16_EXT_UND_SLIP_SP_WEG_LMU_HSP); 
    }
    else
    {
  	  v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_L, 
                                  S16_ESP_SLIP_SP_M_MU_GAIN_DEP_L, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_L); 
      v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_M, 
                                  S16_ESP_SLIP_SP_M_MU_GAIN_DEP_M, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_M);
      v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_H, 
                                  S16_ESP_SLIP_SP_M_MU_GAIN_DEP_H, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_H); 
    }  
	
	#else
	
	  v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_L, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_L, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_L); 
    v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_M, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_M, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_M);
    v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SLIP_SP_H_MU_GAIN_DEP_H, 
                                S16_ESP_SLIP_SP_M_MU_GAIN_DEP_H, S16_ESP_SLIP_SP_L_MU_GAIN_DEP_H); 
            
	#endif 
	
    if( vrefk <= v_v1 )
    {
        gain_speed_slip_depend = v_g1;
    }
    else if ( vrefk < v_v2 )
    {
        if( v_v2==v_v1 ) 
        {
        	v_v2 = v_v2 + 1;
        }
        else
        {
        	;
        }
        gain_speed_slip_depend = v_g1 + (int16_t)( (((int32_t)(v_g2-v_g1))*(vrefk-v_v1))/(v_v2-v_v1) );               
    }
    else if(vrefk < v_v3)
    {
        gain_speed_slip_depend = v_g2;
    }
    else if ( vrefk < v_v4 )
    {
        if( v_v4==v_v3 ) 
        {
        	v_v4 = v_v4 + 1;
        }
        else
        {
        	;
        }
        gain_speed_slip_depend = v_g2 + (int16_t)( (((int32_t)(v_g3-v_g2))*(vrefk-v_v3))/(v_v4-v_v3) );         
    } 
    else
    {
        gain_speed_slip_depend = v_g3;
    }

}

void LCESP_vCalSpeedWHRateGain(void)
{
    int16_t v_v1 = S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, v_v2 = S16_ESP_SLIP_SPEED_GAIN_DEP_L_V; 
    int16_t v_v3 = S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, v_v4 =S16_ESP_SLIP_SPEED_GAIN_DEP_H_V; 
    int16_t v_g1, v_g2, v_g3;
   
	#if	( __Ext_Under_Ctrl == 1 )  
		
    if( EXTREME_UNDER_CONTROL == 1 )
    {
  	  v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_LSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_LSP, S16_EXT_UND_SLIP_SP_WEG_LMU_LSP); 
      v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_MSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_MSP, S16_EXT_UND_SLIP_SP_WEG_LMU_MSP);
      v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EXT_UND_SLIP_SP_WEG_HMU_HSP, 
                                  S16_EXT_UND_SLIP_SP_WEG_MMU_HSP, S16_EXT_UND_SLIP_SP_WEG_LMU_HSP); 
    }
    else
    {
      v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_L, 
                                  S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_L, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_L); 
      v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_M, 
                                  S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_M, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_M);
      v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_H, 
                                  S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_H, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_H); 
    }  
	
	#else
	
    v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_L, 
                                S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_L, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_L); 
    v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_M, 
                                S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_M, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_M);
    v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_WHRATE_SP_H_MU_GAIN_DEP_H, 
                                S16_ESP_WHRATE_SP_M_MU_GAIN_DEP_H, S16_ESP_WHRATE_SP_L_MU_GAIN_DEP_H); 
	#endif  
	            
    if( vrefk <= v_v1 )
    {
        gain_speed_whrate_depend = v_g1;
    }
    else if ( vrefk < v_v2 )
    {
        if( v_v2==v_v1 ) 
        {
        	v_v2 = v_v2 + 1;
        }
        else
        {
        	;
        }
        gain_speed_whrate_depend = v_g1 + (int16_t)( (((int32_t)(v_g2-v_g1))*(vrefk-v_v1))/(v_v2-v_v1) );               
    }
    else if(vrefk < v_v3)
    {
        gain_speed_whrate_depend = v_g2;
    }
    else if ( vrefk < v_v4 )
    {
        if( v_v4==v_v3 ) 
        {
        	v_v4 = v_v4 + 1;
        }
        else
        {
        	;
        }
        gain_speed_whrate_depend = v_g2 + (int16_t)( (((int32_t)(v_g3-v_g2))*(vrefk-v_v3))/(v_v4-v_v3) );         
    } 
    else
    {
        gain_speed_whrate_depend = v_g3;
    }
	
}

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
  #if ESC_PULSE_UP_RISE==1
void LCESP_vCalVariableRiseRateF(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{

/********************GAIN ****************************/
	esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_DEL_P_RISE_RATE_GAIN_H, S16_F_DEL_P_RISE_RATE_GAIN_M, S16_F_DEL_P_RISE_RATE_GAIN_L);

/*****************************************************/
/********************Hold 영역************************/
	esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, SLIP_F_HOLD_THRESHOLD, SLIP_F_HOLD_THRESHOLD_M, SLIP_F_HOLD_THRESHOLD_L);
/*****************************************************/
/********************Full Rise 영역 ******************/
	esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_APP_RISE_DEL_P_H, S16_F_APP_RISE_DEL_P_M, S16_F_APP_RISE_DEL_P_L);
/*****************************************************/
/********************Add Duty       ******************/
	esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_DEL_P_RISE_RATE_ADD_DUTY_H, S16_F_DEL_P_RISE_RATE_ADD_DUTY_M, S16_F_DEL_P_RISE_RATE_ADD_DUTY_L);
/*****************************************************/
	if(esp_tempW0 <= esp_tempW1)
	{
	    esp_tempW0 = esp_tempW1;
	}
	else
	{
	    ;
	}

	esp_tempW3 = esp_tempW2 - esp_tempW1;   /* Full - Hold */
	
	if(WL_ESC->slip_control < 0 )
	{
		esp_tempW9 = McrAbs(WL_ESC->slip_control);
	}
	else
	{
		esp_tempW9 = 0;
    }
    
    if(esp_tempW9 > 0)
    {
    	esp_tempW5 = esp_tempW3-esp_tempW9; /*100*/
    	
    	if(esp_tempW2 < esp_tempW9)
    	{
    		WL_ESC->lcescCalRiserateCountdepDelPF = 0 ; /*Full rise*/
    	}
    	else
    	{
    		WL_ESC->lcescCalRiserateCountdepDelPF = (esp_tempW9 - esp_tempW1) / esp_tempW0 ; /*1*/    		
    	}

    }
    else
    {
    	WL_ESC->lcescCalRiserateCountdepDelPF = 0;	
    }
    
    if((ABS_fz==0)
#if __FBC
        &&(FBC_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif
#if __EPB_INTERFACE
        &&(lcu1EpbActiveFlg==0)
#endif
#if __TCMF_LowVacuumBoost
        &&(TCMF_LowVacuumBoost_flag==0)
#endif    
#if __LVBA
        &&(lclvbau1ActiveFlg==0)
#endif
		&&(WL_ESC->SLIP_CONTROL_NEED == 1)&&(WL_ESC->esp_pressure_count >= variable_rise_scan))
    {
	   	if(esp_tempW9 > esp_tempW2)
    	{
    		/*Full Rise 구간*/
    		if (WL->pulseup_cycle_scan_time <4 )
    		{
    			WL_ESC->lcespCalRiserateAddDuty = 30;
			}
			else if(WL->pulseup_cycle_scan_time < 7)
			{
				WL_ESC->lcespCalRiserateAddDuty = WL->pulseup_cycle_scan_time*10;
			}
			else
			{
				WL_ESC->lcespCalRiserateAddDuty = 35;
    		}
    		
    		WL_ESC->lcespCaldelDuty = WL_ESC->lcespCalRiserateAddDuty - WL_ESC->lcs16EsctTimePulseupDuty;
    		
			WL_ESC->lcespCalRiserateAddDuty = WL_ESC->lcespCaldelDuty;			    		

    	}
    	else if(WL_ESC->lcescCalRiserateCountdepDelPF > 0)
    	{
    		WL_ESC->lcespCaldelDuty = WL_ESC->lcescCalRiserateCountdepDelPF*esp_tempW8;
    		
    		if(WL_ESC->lcs16EscInitialPulseupDuty != WL_ESC->lcs16EscInitialPulseupDuty_old)
    		{

				WL_ESC->lcespCalRiserateAddDuty = WL_ESC->lcespCaldelDuty;
    		
  			}
  			else
  			{
  				WL_ESC->lcespCalRiserateAddDuty = WL_ESC->lcespCalRiserateAddDuty;  
  			}		
    	}
    	else
    	{
  				WL_ESC->lcespCalRiserateAddDuty = WL_ESC->lcespCalRiserateAddDuty;  
    	}
			
			WL_ESC->lcescCalRiserateCountdepDelP_old = WL_ESC->lcescCalRiserateCountdepDelPF;

    }
    else
	{
		WL_ESC->lcespCalRiserateAddDuty = 0;

		WL_ESC->lcescCalRiserateCountdepDelP_old = 0;
		WL_ESC->lcespCalRiserateAddDuty_old = 0;
		WL_ESC->lcespCaldelDuty = 0;
	}
}
  #endif
#else
#if ESC_PULSE_UP_RISE==1
void LCESP_vCalVariableRiseRateF(void)
{

/********************GAIN ****************************/
	esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_DEL_P_RISE_RATE_GAIN_H, S16_F_DEL_P_RISE_RATE_GAIN_M, S16_F_DEL_P_RISE_RATE_GAIN_L);

/*****************************************************/
/********************Hold 영역************************/
	esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, SLIP_F_HOLD_THRESHOLD, SLIP_F_HOLD_THRESHOLD_M, SLIP_F_HOLD_THRESHOLD_L);
/*****************************************************/
/********************Full Rise 영역 ******************/
	esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_APP_RISE_DEL_P_H, S16_F_APP_RISE_DEL_P_M, S16_F_APP_RISE_DEL_P_L);
/*****************************************************/
/********************Add Duty       ******************/
	esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu, S16_F_DEL_P_RISE_RATE_ADD_DUTY_H, S16_F_DEL_P_RISE_RATE_ADD_DUTY_M, S16_F_DEL_P_RISE_RATE_ADD_DUTY_L);
/*****************************************************/
	if(esp_tempW0 <= esp_tempW1)
	{
	    esp_tempW0 = esp_tempW1;
	}
	else
	{
	    ;
	}

	esp_tempW3 = esp_tempW2 - esp_tempW1;   /* Full - Hold */
	
	if(slip_control_front < 0 )
	{
		esp_tempW9 = McrAbs(slip_control_front);
	}
	else
	{
		esp_tempW9 = 0;
    }
    
    if(esp_tempW9 > 0)
    {
    	esp_tempW5 = esp_tempW3-esp_tempW9; /*100*/
    	
    	if(esp_tempW2 < esp_tempW9)
    	{
    		lcescCalRiserateCountdepDelPF = 0 ; /*Full rise*/
    	}
    	else
    	{
    		lcescCalRiserateCountdepDelPF = (esp_tempW9 - esp_tempW1) / esp_tempW0 ; /*1*/    		
    	}

    }
    else
    {
    	lcescCalRiserateCountdepDelPF = 0;	
    }
    
    if((ABS_fz==0)
#if __FBC
        &&(FBC_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif
#if __EPB_INTERFACE
        &&(lcu1EpbActiveFlg==0)
#endif
#if __TCMF_LowVacuumBoost
        &&(TCMF_LowVacuumBoost_flag==0)
#endif    
#if __LVBA
        &&(lclvbau1ActiveFlg==0)
#endif
		&&(SLIP_CONTROL_NEED_FL == 1)&&(esp_pressure_count_fl >= variable_rise_scan))
    {
	#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(slip_status_position==1)
			{
	#endif
    	if(esp_tempW9 > esp_tempW2)
    	{
    		/*Full Rise 구간*/
    		if (pulseup_cycle_scan_time_fl <4 )
    		{
    			lcespCalRiserateAddDutyfl = 30;
			}
			else if(pulseup_cycle_scan_time_fl < 7)
			{
				lcespCalRiserateAddDutyfl = pulseup_cycle_scan_time_fl*10;
			}
			else
			{
				lcespCalRiserateAddDutyfl = 35;
    		}
    		
    		lcespCaldelDutyfl = lcespCalRiserateAddDutyfl - lcs16EsctTimePulseupDutyfl;
    		
			lcespCalRiserateAddDutyfl = lcespCaldelDutyfl;			    		

    	}
    	else if(lcescCalRiserateCountdepDelPF > 0)
    	{
    		lcespCaldelDutyfl = lcescCalRiserateCountdepDelPF*esp_tempW8;
    		
    		if(lcs16EscInitialPulseupDutyfl != lcs16EscInitialPulseupDutyfl_old)
    		{
/*    			if(lcescCalRiserateCountdepDelPF == lcescCalRiserateCountdepDelPfl_old)
    			{ 
    				lcespCalRiserateAddDutyfl = lcespCalRiserateAddDutyfl;
    			}
    			else
    			{
    				lcespCalRiserateAddDutyfl = lcescCalRiserateCountdepDelPF*esp_tempW8;
    			}   */
				lcespCalRiserateAddDutyfl = lcespCaldelDutyfl;
    		
  			}
  			else
  			{
  				lcespCalRiserateAddDutyfl = lcespCalRiserateAddDutyfl;  
  			}		
    	}
    	else
    	{
  				lcespCalRiserateAddDutyfl = lcespCalRiserateAddDutyfl;  
    	}

	/*		if(lcespCalRiserateAddDutyfl <= lcespCalRiserateAddDutyfl_old )
			{
				lcespCalRiserateAddDutyfl_old = lcespCalRiserateAddDutyfl;
				lcespCalRiserateAddDutyfl = 0;
			}
			else
			{
				lcespCalRiserateAddDutyfl_old = lcespCalRiserateAddDutyfl;
			} */
			
			lcescCalRiserateCountdepDelPfl_old = lcescCalRiserateCountdepDelPF;

	#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			}
			else
			{
				;
			}
	#endif

    }
    else
	{
		lcespCalRiserateAddDutyfl = 0;

		lcescCalRiserateCountdepDelPfl_old = 0;
		lcespCalRiserateAddDutyfl_old = 0;
		lcespCaldelDutyfl = 0;
	}

    if((ABS_fz==0)
#if __FBC
        &&(FBC_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif
#if __EPB_INTERFACE
        &&(lcu1EpbActiveFlg==0)
#endif
#if __TCMF_LowVacuumBoost
        &&(TCMF_LowVacuumBoost_flag==0)
#endif
#if __LVBA
        &&(lclvbau1ActiveFlg==0)
#endif
	    &&(SLIP_CONTROL_NEED_FR == 1)&&(esp_pressure_count_fr >= variable_rise_scan))
    {
	#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(slip_status_position==2)
			{
	#endif
    	if(esp_tempW9 > esp_tempW2)
    	{
    		/*Full Rise 구간*/
    		if (pulseup_cycle_scan_time_fr <4 )
    		{
    			lcespCalRiserateAddDutyfr = 30;
			}
			else if(pulseup_cycle_scan_time_fr < 7)
			{
				lcespCalRiserateAddDutyfr = pulseup_cycle_scan_time_fr*10;
 			}
			else
			{
				lcespCalRiserateAddDutyfr = 35;
			}
  			lcespCaldelDutyfr = lcespCalRiserateAddDutyfr - lcs16EsctTimePulseupDutyfr;
    		
    		lcespCalRiserateAddDutyfr = lcespCaldelDutyfr;			    		

    	}
    	else if(lcescCalRiserateCountdepDelPF > 0)
    	{
    		lcespCaldelDutyfr = lcescCalRiserateCountdepDelPF*esp_tempW8;
    		if(lcs16EscInitialPulseupDutyfr != lcs16EscInitialPulseupDutyfr_old)
    		{
/*    			if(lcescCalRiserateCountdepDelPF == lcescCalRiserateCountdepDelPfr_old)
    			{ 
    				lcespCalRiserateAddDutyfr = lcespCalRiserateAddDutyfr;
    			}
    			else
    			{
    				lcespCalRiserateAddDutyfr = lcescCalRiserateCountdepDelPF*esp_tempW8;
    			}
    		
  				lcespCaldelDutyfr = lcespCalRiserateAddDutyfr;  */
					lcespCalRiserateAddDutyfr = lcespCaldelDutyfr;  
  			}
  			else
  			{
  				lcespCalRiserateAddDutyfr = lcespCalRiserateAddDutyfr;  
  			}		
    		
    	}
    	else
    	{
  				lcespCalRiserateAddDutyfr = lcespCalRiserateAddDutyfr;  
	   	}
    	
/*		if(lcespCalRiserateAddDutyfr <= lcespCalRiserateAddDutyfr_old )
		{
			lcespCalRiserateAddDutyfr_old = lcespCalRiserateAddDutyfr;
			lcespCalRiserateAddDutyfr = 0;
		}
		else
		{
			lcespCalRiserateAddDutyfr_old = lcespCalRiserateAddDutyfr;
		} */ 
		lcescCalRiserateCountdepDelPfr_old = lcescCalRiserateCountdepDelPF;
	#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
		}
		else
		{
			;
		}
	#endif
   }
    else
	{
		lcespCalRiserateAddDutyfr = 0;
		lcescCalRiserateCountdepDelPfr_old = 0;
		lcespCalRiserateAddDutyfr_old = 0;
		lcespCaldelDutyfr=0;
	}
}
#endif
#endif


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalSlipGainPressureF(struct ESC_STRUCT *WL_ESC)
{
	if((PARTIAL_BRAKE==0)&&(ABS_fz==1))
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_f_ABS,
								sc_add_gain_1st_cycle_f_MMU_ABS,sc_add_gain_1st_cycle_f_LMU_ABS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_f_ABS,
								sc_add_gain_1st_cycle_D_f_MMU_ABS,sc_add_gain_1st_cycle_D_f_LMU_ABS);
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{       
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F_ABS,
								S16_DEL_SLIP_MMU_NEG_WEG_F_ABS,S16_DEL_SLIP_LMU_NEG_WEG_F_ABS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))			
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_ABS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_ABS);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F_ABS,
								S16_DEL_SLIP_MMU_POS_WEG_F_ABS,S16_DEL_SLIP_LMU_POS_WEG_F_ABS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_ABS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_ABS);
											 				 
			} 		
		}		
	}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	else if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1))
#else
	else if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1))
#endif
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_f_CBS,
								sc_add_gain_1st_cycle_f_MMU_CBS,sc_add_gain_1st_cycle_f_LMU_CBS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_f_CBS,
								sc_add_gain_1st_cycle_D_f_MMU_CBS, sc_add_gain_1st_cycle_D_f_LMU_CBS);
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{       
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F_CBS,
								S16_DEL_SLIP_MMU_NEG_WEG_F_CBS,S16_DEL_SLIP_LMU_NEG_WEG_F_CBS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_CBS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_CBS);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F_CBS,
								S16_DEL_SLIP_MMU_POS_WEG_F_CBS,S16_DEL_SLIP_LMU_POS_WEG_F_CBS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_CBS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_CBS);
											 				 
			} 		
		}		
	}
	else
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_front,
								sc_add_gain_1st_cycle_front_MMU,sc_add_gain_1st_cycle_front_LMU);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_front,
								sc_add_gain_1st_cycle_D_front_MMU,sc_add_gain_1st_cycle_D_front_LMU);
		
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{       
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F,
								S16_DEL_SLIP_MMU_NEG_WEG_F,S16_DEL_SLIP_LMU_NEG_WEG_F);
		
/*			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0))) */

			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F,S16_DEL_WHEEL_SP_INC_LMU_WEG_F);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F,
								S16_DEL_SLIP_MMU_POS_WEG_F,S16_DEL_SLIP_LMU_POS_WEG_F);
		
/*			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0))) */

			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F,S16_DEL_WHEEL_SP_INC_LMU_WEG_F);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F);
											 				 
			} 		
		}
	}
	
	if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->FLAG_1ST_CYCLE_WHEEL==1))
	{
		WL_ESC->del_slip_gain = WL_ESC->del_slip_gain + esp_tempW0;
		WL_ESC->del_wheel_sp_gain = WL_ESC->del_wheel_sp_gain - esp_tempW1;
		
		if(WL_ESC->del_wheel_sp_gain < 0)
		{
		    WL_ESC->del_wheel_sp_gain = 1;
		}
		else
		{
		    ;
		}        
	}
	else
	{
		;	
	}

	WL_ESC->del_slip_gain = (int16_t)( (((int32_t)WL_ESC->del_slip_gain)*gain_speed_slip_depend)/100 );
	
  WL_ESC->del_wheel_sp_gain = (int16_t)( (((int32_t)WL_ESC->del_wheel_sp_gain)*gain_speed_whrate_depend)/100 );
	
}
void LCESP_vCalSlipGainPressureR(struct ESC_STRUCT *WL_ESC)
{
	if((PARTIAL_BRAKE==0)&&(ABS_fz==1))
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_r_ABS,
								sc_add_gain_1st_cycle_r_MMU_ABS,sc_add_gain_1st_cycle_r_LMU_ABS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_r_ABS,
								sc_add_gain_1st_cycle_D_r_MMU_ABS,sc_add_gain_1st_cycle_D_r_LMU_ABS);
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R_ABS,
								S16_DEL_SLIP_MMU_NEG_WEG_R_ABS,S16_DEL_SLIP_LMU_NEG_WEG_R_ABS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_ABS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_ABS);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R_ABS,
								S16_DEL_SLIP_MMU_POS_WEG_R_ABS,S16_DEL_SLIP_LMU_POS_WEG_R_ABS);
		
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_ABS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_ABS);
											 				 
			} 
		}
		
	}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	else if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==0))
#else
	else if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==0))
#endif
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_r_CBS,
								sc_add_gain_1st_cycle_r_MMU_CBS,sc_add_gain_1st_cycle_r_LMU_CBS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_r_CBS,
								sc_add_gain_1st_cycle_D_r_MMU_CBS,sc_add_gain_1st_cycle_D_r_LMU_CBS);
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R_CBS,
								S16_DEL_SLIP_MMU_NEG_WEG_R_CBS,S16_DEL_SLIP_LMU_NEG_WEG_R_CBS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_CBS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_CBS);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R_CBS,
								S16_DEL_SLIP_MMU_POS_WEG_R_CBS,S16_DEL_SLIP_LMU_POS_WEG_R_CBS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_CBS);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_CBS);
											 				 
			} 
		}
		
	}
	else
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_rear,
								sc_add_gain_1st_cycle_rear_MMU,sc_add_gain_1st_cycle_rear_LMU);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_rear,
								sc_add_gain_1st_cycle_D_rear_MMU,sc_add_gain_1st_cycle_D_rear_LMU);
		if(WL_ESC->lcescs16CalDelTargetSlip <0)
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R,
								S16_DEL_SLIP_MMU_NEG_WEG_R,S16_DEL_SLIP_LMU_NEG_WEG_R);
		

			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R,S16_DEL_WHEEL_SP_INC_LMU_WEG_R);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R);
											 				 
			} 
		}
		else
		{
			WL_ESC->del_slip_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R,
								S16_DEL_SLIP_MMU_POS_WEG_R,S16_DEL_SLIP_LMU_POS_WEG_R);
		

			if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->lcescs16CalWheelSpeedRate < 0))
			{  
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R,S16_DEL_WHEEL_SP_INC_LMU_WEG_R);
											
			}
			else
			{ 
				WL_ESC->del_wheel_sp_gain=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R);
											 				 
			} 
		}
	}
	
	if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->FLAG_1ST_CYCLE_WHEEL==1))
	{
		WL_ESC->del_slip_gain = WL_ESC->del_slip_gain + esp_tempW0;
		WL_ESC->del_wheel_sp_gain = WL_ESC->del_wheel_sp_gain - esp_tempW1;

		if(WL_ESC->del_wheel_sp_gain<0)
		{
		    WL_ESC->del_wheel_sp_gain=1;
		}
		else
		{
		    ;
		}    
	}
	else
	{
		;	
	}

	WL_ESC->del_slip_gain = (int16_t)( (((int32_t)WL_ESC->del_slip_gain)*gain_speed_slip_depend)/100 );
	
  WL_ESC->del_wheel_sp_gain = (int16_t)( (((int32_t)WL_ESC->del_wheel_sp_gain)*gain_speed_whrate_depend)/100 );
        	
}
#else
void LCESP_vCalSlipGainPressureF(void)
{
	if((PARTIAL_BRAKE==0)&&(ABS_fz==1))
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_f_ABS,
								sc_add_gain_1st_cycle_f_MMU_ABS,sc_add_gain_1st_cycle_f_LMU_ABS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_f_ABS,
								sc_add_gain_1st_cycle_D_f_MMU_ABS,sc_add_gain_1st_cycle_D_f_LMU_ABS);
		if(lcescs16CalDelTargetSlipf <0)
		{       
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F_ABS,
								S16_DEL_SLIP_MMU_NEG_WEG_F_ABS,S16_DEL_SLIP_LMU_NEG_WEG_F_ABS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))			
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_ABS);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_ABS);
											 				 
			} 
		}
		else
		{
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F_ABS,
								S16_DEL_SLIP_MMU_POS_WEG_F_ABS,S16_DEL_SLIP_LMU_POS_WEG_F_ABS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_ABS);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_ABS);
											 				 
			} 		
		}		
	}
#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	else if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1))
#else
	else if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1))
#endif
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_f_CBS,
								sc_add_gain_1st_cycle_f_MMU_CBS,sc_add_gain_1st_cycle_f_LMU_CBS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_f_CBS,
								sc_add_gain_1st_cycle_D_f_MMU_CBS, sc_add_gain_1st_cycle_D_f_LMU_CBS);
		if(lcescs16CalDelTargetSlipf <0)
		{       
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F_CBS,
								S16_DEL_SLIP_MMU_NEG_WEG_F_CBS,S16_DEL_SLIP_LMU_NEG_WEG_F_CBS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))			
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_CBS);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_CBS);
											 				 
			} 
		}
		else
		{
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F_CBS,
								S16_DEL_SLIP_MMU_POS_WEG_F_CBS,S16_DEL_SLIP_LMU_POS_WEG_F_CBS);
		
			/*if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0)))*/
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_F_CBS);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F_CBS);
											 				 
			} 		
		}		
	}
	else
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_front,
								sc_add_gain_1st_cycle_front_MMU,sc_add_gain_1st_cycle_front_LMU);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_front,
								sc_add_gain_1st_cycle_D_front_MMU,sc_add_gain_1st_cycle_D_front_LMU);
		
		if(lcescs16CalDelTargetSlipf <0)
		{       
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_F,
								S16_DEL_SLIP_MMU_NEG_WEG_F,S16_DEL_SLIP_LMU_NEG_WEG_F);
		
/*			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0))) */
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0)&&(slip_status_position==1))
				||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)&&(slip_status_position==2)))
#else
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))
#endif
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F,S16_DEL_WHEEL_SP_INC_LMU_WEG_F);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F);
											 				 
			} 
		}
		else
		{
			del_slip_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_F,
								S16_DEL_SLIP_MMU_POS_WEG_F,S16_DEL_SLIP_LMU_POS_WEG_F);
		
/*			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalDelWheelSpeedRatefl > 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalDelWheelSpeedRatefr > 0))) */
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)	
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0)&&(slip_status_position==1))
				||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)&&(slip_status_position==2)))
#else
			if(((SLIP_CONTROL_NEED_FL==1)&&(lcescs16CalWheelSpeedRatefl < 0))||((SLIP_CONTROL_NEED_FR==1)&&(lcescs16CalWheelSpeedRatefr < 0)))
#endif
			{  
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_F,S16_DEL_WHEEL_SP_INC_LMU_WEG_F);
											
			}
			else
			{ 
				del_wheel_sp_gain_front=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_F,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_F,S16_DEL_WHEEL_SP_DEC_LMU_WEG_F);
											 				 
			} 		
		}
	}
	
	if(((FLAG_1ST_CYCLE_FL==1)&&(FLAG_1ST_CYCLE_WHEEL_FL==1))||((FLAG_1ST_CYCLE_FR==1)&&(FLAG_1ST_CYCLE_WHEEL_FR==1)))
	{
		del_slip_gain_front = del_slip_gain_front + esp_tempW0;
		del_wheel_sp_gain_front = del_wheel_sp_gain_front - esp_tempW1;
		
		if(del_wheel_sp_gain_front < 0)
		{
		    del_wheel_sp_gain_front = 1;
		}
		else
		{
		    ;
		}        
	}
	else
	{
		;	
	}

	del_slip_gain_front = (int16_t)( (((int32_t)del_slip_gain_front)*gain_speed_slip_depend)/100 );
	
  del_wheel_sp_gain_front = (int16_t)( (((int32_t)del_wheel_sp_gain_front)*gain_speed_whrate_depend)/100 );
	
}
void LCESP_vCalSlipGainPressureR(void)
{
	if((PARTIAL_BRAKE==0)&&(ABS_fz==1))
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_r_ABS,
								sc_add_gain_1st_cycle_r_MMU_ABS,sc_add_gain_1st_cycle_r_LMU_ABS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_r_ABS,
								sc_add_gain_1st_cycle_D_r_MMU_ABS,sc_add_gain_1st_cycle_D_r_LMU_ABS);
		if(lcescs16CalDelTargetSlipr <0)
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R_ABS,
								S16_DEL_SLIP_MMU_NEG_WEG_R_ABS,S16_DEL_SLIP_LMU_NEG_WEG_R_ABS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_ABS);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_ABS);
											 				 
			} 
		}
		else
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R_ABS,
								S16_DEL_SLIP_MMU_POS_WEG_R_ABS,S16_DEL_SLIP_LMU_POS_WEG_R_ABS);
		
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_ABS);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_ABS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_ABS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_ABS);
											 				 
			} 
		}
		
	}
#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	else if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==0))
#else
	else if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==0))
#endif
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_r_CBS,
								sc_add_gain_1st_cycle_r_MMU_CBS,sc_add_gain_1st_cycle_r_LMU_CBS);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_r_CBS,
								sc_add_gain_1st_cycle_D_r_MMU_CBS,sc_add_gain_1st_cycle_D_r_LMU_CBS);
		if(lcescs16CalDelTargetSlipr <0)
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R_CBS,
								S16_DEL_SLIP_MMU_NEG_WEG_R_CBS,S16_DEL_SLIP_LMU_NEG_WEG_R_CBS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_CBS);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_CBS);
											 				 
			} 
		}
		else
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R_CBS,
								S16_DEL_SLIP_MMU_POS_WEG_R_CBS,S16_DEL_SLIP_LMU_POS_WEG_R_CBS);
		
			/*if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalDelWheelSpeedRaterl > 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalDelWheelSpeedRaterr > 0)))*/
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_INC_LMU_WEG_R_CBS);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R_CBS,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R_CBS,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R_CBS);
											 				 
			} 
		}
		
	}
	else
	{
		esp_tempW0=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_rear,
								sc_add_gain_1st_cycle_rear_MMU,sc_add_gain_1st_cycle_rear_LMU);
								
		esp_tempW1=LCESP_s16FindRoadDependatGain(esp_mu,sc_add_gain_1st_cycle_D_rear,
								sc_add_gain_1st_cycle_D_rear_MMU,sc_add_gain_1st_cycle_D_rear_LMU);
		if(lcescs16CalDelTargetSlipr <0)
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_NEG_WEG_R,
								S16_DEL_SLIP_MMU_NEG_WEG_R,S16_DEL_SLIP_LMU_NEG_WEG_R);
		
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0)&&(slip_status_position==3))
				||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)&&(slip_status_position==4)))
#else
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
#endif
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R,S16_DEL_WHEEL_SP_INC_LMU_WEG_R);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R);
											 				 
			} 
		}
		else
		{
			del_slip_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_SLIP_HMU_POS_WEG_R,
								S16_DEL_SLIP_MMU_POS_WEG_R,S16_DEL_SLIP_LMU_POS_WEG_R);
		
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0)&&(slip_status_position==3))
				||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)&&(slip_status_position==4)))
#else
			if(((SLIP_CONTROL_NEED_RL==1)&&(lcescs16CalWheelSpeedRaterl < 0))||((SLIP_CONTROL_NEED_RR==1)&&(lcescs16CalWheelSpeedRaterr < 0)))
#endif
			{  
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_INC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_INC_MMU_WEG_R,S16_DEL_WHEEL_SP_INC_LMU_WEG_R);
											
			}
			else
			{ 
				del_wheel_sp_gain_rear=LCESP_s16FindRoadDependatGain(esp_mu,S16_DEL_WHEEL_SP_DEC_HMU_WEG_R,
									S16_DEL_WHEEL_SP_DEC_MMU_WEG_R,S16_DEL_WHEEL_SP_DEC_LMU_WEG_R);
											 				 
			} 
		}
	}
	
	if(((FLAG_1ST_CYCLE_RL==1)&&(FLAG_1ST_CYCLE_WHEEL_RL==1)) || ((FLAG_1ST_CYCLE_RR==1)&&(FLAG_1ST_CYCLE_WHEEL_RR==1)))
	{
		del_slip_gain_rear = del_slip_gain_rear + esp_tempW0;
		del_wheel_sp_gain_rear = del_wheel_sp_gain_rear - esp_tempW1;

		if(del_wheel_sp_gain_rear<0)
		{
		    del_wheel_sp_gain_rear=1;
		}
		else
		{
		    ;
		}    
	}
	else
	{
		;	
	}

	del_slip_gain_rear = (int16_t)( (((int32_t)del_slip_gain_rear)*gain_speed_slip_depend)/100 );
	
  del_wheel_sp_gain_rear = (int16_t)( (((int32_t)del_wheel_sp_gain_rear)*gain_speed_whrate_depend)/100 );
        	
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalSlipSignalF(struct ESC_STRUCT *WL_ESC)
{
	#if(__ESC_TCMF_CONTROL ==1)
	WL_ESC->slip_control_old[2] = WL_ESC->slip_control_old[1];
	WL_ESC->slip_control_old[1] = WL_ESC->slip_control_old[0];
	WL_ESC->slip_control_old[0] = WL_ESC->slip_control;
	#endif
	
	tempW2 = (int16_t)( (((int32_t)WL_ESC->lcescs16CalDelTargetSlip)*C1_front)/1000 ); 
	esp_tempW1 = (int16_t)( (((int32_t)tempW2)*WL_ESC->del_slip_gain)/100 );
	
	
	if(WL_ESC->SLIP_CONTROL_NEED==1)
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRatefl;*/
		esp_tempW2 = WL_ESC->lcescs16CalWheelSpeedRate;
	}
	else
	{
		esp_tempW2 = 0;
	}
	
	tempW2 = (int16_t)( (((int32_t)esp_tempW2)*C2_front)/1000 ); 
	esp_tempW3 = (int16_t)( (((int32_t)tempW2)*WL_ESC->del_wheel_sp_gain)/100 ); 	

	WL_ESC->slip_control = esp_tempW1 - esp_tempW3;	
	
	#if(__ESC_TCMF_CONTROL ==1)
	WL_ESC->slip_control_dot = WL_ESC->slip_control - WL_ESC->slip_control_old[2];
	#endif
	
	/*
	 ESC-ABS Combination Control: Wheel Pressure Compulsion Rise 
  	*/

    esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu,  S16_COMB_ABS_PRESSURE_GAIN_HMU,     
                                                        S16_COMB_ABS_PRESSURE_GAIN_MMU,     S16_COMB_ABS_PRESSURE_GAIN_LMU);    /* ABS pressure gain */
	esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu,  S16_COMB_CBS_PRESSURE_GAIN_HMU,     
                                                        S16_COMB_CBS_PRESSURE_GAIN_MMU,     S16_COMB_CBS_PRESSURE_GAIN_LMU);    /* CBS pressure gain */
	
	tempW2 = S16_COMB_ABS_PRESSURE_OFFSET;            /* ABS pressure offset */
	tempW1 = S16_COMB_CBS_PRESSURE_OFFSET;            /* CBS pressure offset */
	tempW0 = 1000;
	
	if (SLIP_CONTROL_NEED_FL == 1)
	{
		if (SLIP_CONTROL_NEED_FL_old == 0)
		{
			esp_tempW4 = (FR.s16_Estimated_Active_Press*2)/10;       /* guaranteed the minimum wheel pressure, T/P 추후 등록 */
			
			if (FL.s16_Estimated_Active_Press < esp_tempW4)
			{
			    lcesps16KeepEstPress = esp_tempW4;
			}
			else
			{
			lcesps16KeepEstPress = FL.s16_Estimated_Active_Press;
		}
			
		}
		else
		{
			;
		}
	            	
		if (ABS_fl == 1)	/*ABS Combination Control*/
	 	{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_FL = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #if __ACC
	    else if ((lcu1AccActiveFlg==1)||(lcu1DecActiveFlg==1))      /*CPS Combination Control*/	
		{
		    esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_FL = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
      #endif
		else				/*CBS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW6)/tempW0 );
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
      lcesps16EstPressFromDelM_FL = lsesps16AHBGEN3mpress + tempW1 + esp_tempW5;
    #else
      lcesps16EstPressFromDelM_FL = mpress + tempW1 + esp_tempW5;
    #endif			
	    }
	}
	else
	{
		;
	}
	
	if (SLIP_CONTROL_NEED_FR == 1)
	{
		if (SLIP_CONTROL_NEED_FR_old == 0)
		{
			esp_tempW4 = (FL.s16_Estimated_Active_Press*2)/10;       /* guaranteed the minimum wheel pressure, T/P 추후 등록 */
			
			if (FR.s16_Estimated_Active_Press < esp_tempW4)
			{
			    lcesps16KeepEstPress = esp_tempW4;
			}
			else
			{
			lcesps16KeepEstPress = FR.s16_Estimated_Active_Press;
		}
		}
		else
		{
			;
		}
		
		if (ABS_fr == 1)	/*ABS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_FR = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #if __ACC
	    else if ((lcu1AccActiveFlg==1)||(lcu1DecActiveFlg==1))      /*CPS Combination Control*/
		{
		    esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_FR = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #endif
		else				/*CBS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW6)/tempW0 );
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
      lcesps16EstPressFromDelM_FR = lsesps16AHBGEN3mpress + tempW1 + esp_tempW5;
    #else
      lcesps16EstPressFromDelM_FR = mpress + tempW1 + esp_tempW5;
    #endif			
	    }	
	}	
	else
	{
		;
	}

	esp_tempW0 = - LCESP_s16FindRoadDependatGain(esp_mu,    S16_ABS_CBS_COMB_HMU_LIMIT_SLIP,	
	                                                        S16_ABS_CBS_COMB_MMU_LIMIT_SLIP,	S16_ABS_CBS_COMB_LMU_LIMIT_SLIP);	        /* slip limitation by esp_mu */
	esp_tempW9 = - LCESP_s16FindRoadDependatGain(esp_mu,    S16_ABS_CBS_COMB_HMU_LIMIT_SLIP_MAX,	
	                                                        S16_ABS_CBS_COMB_MMU_LIMIT_SLIP_MAX,	S16_ABS_CBS_COMB_LMU_LIMIT_SLIP_MAX);	/* maximum slip limitation by esp_mu */
		
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if ((lsespu1AHBGEN3MpresBrkOn == 1)&&(SLIP_CONTROL_NEED_FL == 1)&&(FL_ESC.slip_control > FL_ESC.slip_rise_thr_change ))
#else
  if ((MPRESS_BRAKE_ON == 1)&&(SLIP_CONTROL_NEED_FL == 1)&&(FL_ESC.slip_control > FL_ESC.slip_rise_thr_change))
#endif		
	{
		
		esp_tempW1 = FR_ESC.slip_m;
		lcesps16SlipThreshold = esp_tempW0;
		
		if ( lcesps16SlipThreshold > esp_tempW1 )
		{
			lcesps16SlipThreshold = esp_tempW1;								// target slip =  opponent wheel slip
		}
		else
		{
			;
		}
		
		if ( lcesps16SlipThreshold < esp_tempW9 )
		{
		    lcesps16SlipThreshold = esp_tempW9;                                // Set the maximum target slip 
		}
		else
		{
		    ;
		}
			
		if  (FL_ESC.slip_m > lcesps16SlipThreshold)  
		{  
		
			if ((lcu1ABSCBSCombCntrl_FL == 0)&&(FL.s16_Estimated_Active_Press <= (lcesps16EstPressFromDelM_FL - S16_ABS_CBS_COMB_PRESSURE_MARGIN)))   /* Set */
			{
				lcu1ABSCBSCombCntrl_FL = 1;
			}							
			else
			{
				;
			}
	  		
			if ((lcu1ABSCBSCombCntrl_FL == 1)&&(FL.s16_Estimated_Active_Press > lcesps16EstPressFromDelM_FL))   /* Reset */
			{
				lcu1ABSCBSCombCntrl_FL = 0;
			}							
			else 			
			{
				;
			}
			
		}
		else
		{
			lcu1ABSCBSCombCntrl_FL = 0;
		}
	}
	else
	{
		lcu1ABSCBSCombCntrl_FL = 0;
 	}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
  if ((lsespu1AHBGEN3MpresBrkOn == 1)&&(SLIP_CONTROL_NEED_FR == 1)&&(FR_ESC.slip_control > FR_ESC.slip_rise_thr_change ))
#else
  if ((MPRESS_BRAKE_ON == 1)&&(SLIP_CONTROL_NEED_FR == 1)&&(FR_ESC.slip_control > FR_ESC.slip_rise_thr_change ))
#endif		
	{
		esp_tempW1 = FL_ESC.slip_m;
		lcesps16SlipThreshold = esp_tempW0;
		
		if ( lcesps16SlipThreshold > esp_tempW1 )
		{
			lcesps16SlipThreshold = esp_tempW1;								// target slip =  opponent wheel slip
		}
		else
		{
			;
		}
		
		if ( lcesps16SlipThreshold < esp_tempW9 )
		{
		    lcesps16SlipThreshold = esp_tempW9;                                // Set the maximum target slip 
		}
		else
		{
		    ;
		}
			
		if (FR_ESC.slip_m > lcesps16SlipThreshold)  
		{  
		
			if ((lcu1ABSCBSCombCntrl_FR == 0)&&(FR.s16_Estimated_Active_Press <= (lcesps16EstPressFromDelM_FR - S16_ABS_CBS_COMB_PRESSURE_MARGIN)))   /* Set */
			{
				lcu1ABSCBSCombCntrl_FR = 1;
			}							
			else
			{
				;
			}
	  		
			if ((lcu1ABSCBSCombCntrl_FR == 1)&&(FR.s16_Estimated_Active_Press > lcesps16EstPressFromDelM_FR))   /* Reset */
			{
				lcu1ABSCBSCombCntrl_FR = 0;
			}							
			else 			
			{
				;
			}
			
		}
		else
		{
			lcu1ABSCBSCombCntrl_FR = 0;
		}
			
	}
	else
	{
		lcu1ABSCBSCombCntrl_FR = 0;
	}
		
}

void LCESP_vCalSlipSignalR(struct ESC_STRUCT *WL_ESC)
{	
	#define Wheel_Iz_r		16;   /*Nm^2*/
	#define wheel_radiusB_r	30;  /*0.3m     resol 100 */

	tempW2 = (int16_t)( (((int32_t)WL_ESC->lcescs16CalDelTargetSlip)*C1_rear)/1000 );
	esp_tempW1 = (int16_t)( (((int32_t)tempW2)*WL_ESC->del_slip_gain)/100 ); 	
	if(WL_ESC->SLIP_CONTROL_NEED==1)
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRaterl;*/
		esp_tempW2 = WL_ESC->lcescs16CalWheelSpeedRate;
	}
	else
	{
		esp_tempW2 = 0;
	}

	tempW2 = (int16_t)( (((int32_t)esp_tempW2)*C2_rear)/1000 );
	esp_tempW3 = (int16_t)( (((int32_t)tempW2)*WL_ESC->del_wheel_sp_gain)/100 ); 	
	WL_ESC->slip_control = esp_tempW1-esp_tempW3;


	/*
	 ESC-ABS Combination Control: Wheel Pressure Compulsion Rise 
  	*/
	
    esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu,  S16_COMB_ABS_PRESSURE_GAIN_HMU,     
                                                        S16_COMB_ABS_PRESSURE_GAIN_MMU,     S16_COMB_ABS_PRESSURE_GAIN_LMU);    /* ABS pressure gain */
	esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu,  S16_COMB_CBS_PRESSURE_GAIN_HMU,     
                                                        S16_COMB_CBS_PRESSURE_GAIN_MMU,     S16_COMB_CBS_PRESSURE_GAIN_LMU);    /* CBS pressure gain */
	
	tempW2 = S16_COMB_ABS_PRESSURE_OFFSET;            /* ABS pressure offset */
	tempW1 = S16_COMB_CBS_PRESSURE_OFFSET;            /* CBS pressure offset */
	tempW0 = 1000;
	
	
	
	
	if (SLIP_CONTROL_NEED_RL == 1)
	{
		if (SLIP_CONTROL_NEED_RL_old == 0)
		{
			esp_tempW4 = (RR.s16_Estimated_Active_Press*2)/10;       /* guaranteed the minimum wheel pressure, T/P 추후 등록 */
			
			if (RL.s16_Estimated_Active_Press < esp_tempW4)
			{
			    lcesps16KeepEstPress = esp_tempW4;
			}
			else
			{
			lcesps16KeepEstPress = RL.s16_Estimated_Active_Press;
		}
			
		}
		else
		{
			;
		}
		
		if (ABS_rl == 1)	/*ABS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_RL = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #if __ACC
		else if ((lcu1AccActiveFlg==1)||(lcu1DecActiveFlg==1))       /*CPS Combination Control*/
		{
		    esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_RL = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #endif
		else				/*CBS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW6)/tempW0 );
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
      lcesps16EstPressFromDelM_RL = lsesps16AHBGEN3mpress + tempW1 + esp_tempW5;
    #else
      lcesps16EstPressFromDelM_RL = mpress + tempW1 + esp_tempW5; 
    #endif
	    }
	}
	else
	{
		;
	}
	
	if (SLIP_CONTROL_NEED_RR == 1)
	{
		if (SLIP_CONTROL_NEED_RR_old == 0)
		{
			esp_tempW4 = (RL.s16_Estimated_Active_Press*2)/10;       /* guaranteed the minimum wheel pressure, T/P 추후 등록 */
			
			if (RR.s16_Estimated_Active_Press < esp_tempW4)
			{
			    lcesps16KeepEstPress = esp_tempW4;
			}
			else
			{
			lcesps16KeepEstPress = RR.s16_Estimated_Active_Press;
		}
		}
		else
		{
			;
		}
		
		if (ABS_rr == 1)	/*ABS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_RR = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #if __ACC
	    else if ((lcu1AccActiveFlg==1)||(lcu1DecActiveFlg==1))      /*CPS Combination Control*/
		{
		    esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW8)/tempW0 );
			lcesps16EstPressFromDelM_RR = lcesps16KeepEstPress + tempW2 + esp_tempW5;
		}
	  #endif
		else				/*CBS Combination Control*/
		{
			esp_tempW5 = (int16_t)(((int32_t)(McrAbs(delta_moment_q))* esp_tempW6)/tempW0 );
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
      lcesps16EstPressFromDelM_RR = lsesps16AHBGEN3mpress + tempW1 + esp_tempW5;
    #else
      lcesps16EstPressFromDelM_RR = mpress + tempW1 + esp_tempW5;
    #endif			
	    }	
	}	
	else
	{
		;
	}

    esp_tempW0 = - LCESP_s16FindRoadDependatGain(esp_mu,    S16_ABS_CBS_COMB_HMU_LIMIT_SLIP,	
	                                                        S16_ABS_CBS_COMB_MMU_LIMIT_SLIP,	S16_ABS_CBS_COMB_LMU_LIMIT_SLIP);	    /* slip limitation by esp_mu */
	esp_tempW9 = - LCESP_s16FindRoadDependatGain(esp_mu,  S16_ABS_CBS_COMB_HMU_LIMIT_SLIP_MAX,	
	                                                      S16_ABS_CBS_COMB_MMU_LIMIT_SLIP_MAX,	S16_ABS_CBS_COMB_LMU_LIMIT_SLIP_MAX);	/* maximum slip limitation by esp_mu */
		
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
  if ((lsespu1AHBGEN3MpresBrkOn == 1)&&(SLIP_CONTROL_NEED_RL == 1)&&(RL_ESC.slip_control > RL_ESC.slip_rise_thr_change))
#else
  if ((MPRESS_BRAKE_ON == 1)&&(SLIP_CONTROL_NEED_RL == 1)&&(RL_ESC.slip_control > RL_ESC.slip_rise_thr_change))
#endif		
	{
		
		esp_tempW1 = RR_ESC.slip_m;
		lcesps16SlipThreshold = esp_tempW0;
		
		if ( lcesps16SlipThreshold > esp_tempW1 )
		{
			lcesps16SlipThreshold = esp_tempW1;								// target slip =  opponent wheel slip
		}
		else
		{
			;
		}
		
		if ( lcesps16SlipThreshold < esp_tempW9 )
		{
		    lcesps16SlipThreshold = esp_tempW9;                                // Set the maximum target slip 
		}
		else
		{
			;
		}
			
		if (RL_ESC.slip_m > lcesps16SlipThreshold)  
		{  
						
			if ((lcu1ABSCBSCombCntrl_RL == 0)&&(RL.s16_Estimated_Active_Press <= (lcesps16EstPressFromDelM_RL - S16_ABS_CBS_COMB_PRESSURE_MARGIN)))   /* Set */
			{
				lcu1ABSCBSCombCntrl_RL = 1;
			}							
			else
			{
				;
			}
			
			if ((lcu1ABSCBSCombCntrl_RL == 1)&&(RL.s16_Estimated_Active_Press > lcesps16EstPressFromDelM_RL))   /* Reset */
			{
				lcu1ABSCBSCombCntrl_RL = 0;
			}							
			else 			
			{
				;
			}
			
		}
		else
		{
			lcu1ABSCBSCombCntrl_RL = 0;
		}
	}
	else
	{
		lcu1ABSCBSCombCntrl_RL = 0;
	}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
  if ((lsespu1AHBGEN3MpresBrkOn == 1)&&(SLIP_CONTROL_NEED_RR == 1)&&(RR_ESC.slip_control > RR_ESC.slip_rise_thr_change ))
#else
  if ((MPRESS_BRAKE_ON == 1)&&(SLIP_CONTROL_NEED_RR == 1)&&(RR_ESC.slip_control > RR_ESC.slip_rise_thr_change ))
#endif		
	{
		esp_tempW1 = RL_ESC.slip_m;
		lcesps16SlipThreshold = esp_tempW0;
		
		if ( lcesps16SlipThreshold > esp_tempW1 )
		{
			lcesps16SlipThreshold = esp_tempW1;								// target slip =  opponent wheel slip
		}
		else
		{
			;
		}
		
		if ( lcesps16SlipThreshold < esp_tempW9 )
		{
		    lcesps16SlipThreshold = esp_tempW9;                                // Set the maximum target slip 
		}
		else
		{
			;
		}
			
		if (RR_ESC.slip_m > lcesps16SlipThreshold)  
		{  
					
			if ((lcu1ABSCBSCombCntrl_RR == 0)&&(RR.s16_Estimated_Active_Press <= (lcesps16EstPressFromDelM_RR - S16_ABS_CBS_COMB_PRESSURE_MARGIN)))   /* Set */
			{
				lcu1ABSCBSCombCntrl_RR = 1;
			}							
			else
			{
				;
			}
	  		
			if ((lcu1ABSCBSCombCntrl_RR == 1)&&(RR.s16_Estimated_Active_Press > lcesps16EstPressFromDelM_RR))   /* Reset */
			{
				lcu1ABSCBSCombCntrl_RR = 0;
			}							
			else 			
			{
				;
			}
			
		}
		else
		{
			lcu1ABSCBSCombCntrl_RR = 0;
		}
			
	}
	else
	{
		lcu1ABSCBSCombCntrl_RR = 0;
	}
	
}
#else
void LCESP_vCalSlipSignalF(void)
{
	tempW2 = (int16_t)( (((int32_t)lcescs16CalDelTargetSlipf)*C1_front)/1000 ); 
	esp_tempW1 = (int16_t)( (((int32_t)tempW2)*del_slip_gain_front)/100 );
	
	
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_status_position==1))
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRatefl;*/
		esp_tempW2 = lcescs16CalWheelSpeedRatefl;
	}
	else if((SLIP_CONTROL_NEED_FR==1)&&(slip_status_position==2))
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRatefr;		*/
		esp_tempW2 = lcescs16CalWheelSpeedRatefr;		
	}
	else
	{
		esp_tempW2 = 0;
	}
	
	tempW2 = (int16_t)( (((int32_t)esp_tempW2)*C2_front)/1000 ); 
	esp_tempW3 = (int16_t)( (((int32_t)tempW2)*del_wheel_sp_gain_front)/100 ); 	

	slip_control_front = esp_tempW1 - esp_tempW3;
	
#if ESC_PULSE_UP_RISE==1
	LCESP_vCalVariableRiseRateF();
#endif	
}

void LCESP_vCalSlipSignalR(void)
{	
	#define Wheel_Iz_r		16;   /*Nm^2*/
	#define wheel_radiusB_r	30;  /*0.3m     resol 100 */

	tempW2 = (int16_t)( (((int32_t)lcescs16CalDelTargetSlipr)*C1_rear)/1000 );
	esp_tempW1 = (int16_t)( (((int32_t)tempW2)*del_slip_gain_rear)/100 ); 	
	if((SLIP_CONTROL_NEED_RL==1)&&(slip_status_position==3))
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRaterl;*/
		esp_tempW2 = lcescs16CalWheelSpeedRaterl;
	}
	else if((SLIP_CONTROL_NEED_RR==1)&&(slip_status_position==4))
	{
		/*esp_tempW2 = lcescs16CalDelWheelSpeedRaterr;*/		
		esp_tempW2 = lcescs16CalWheelSpeedRaterr;		
	}
	else
	{
		esp_tempW2 = 0;
	}
	
	tempW2 = (int16_t)( (((int32_t)esp_tempW2)*C2_rear)/1000 );
	esp_tempW3 = (int16_t)( (((int32_t)tempW2)*del_wheel_sp_gain_rear)/100 ); 	
	slip_control_rear = esp_tempW1-esp_tempW3;

}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCalPressureRateF(struct ESC_STRUCT *WL_ESC)
{       
	#define   	S16_PULSE_DUMP_DRIFT_ADD_ABS          		       0
	#define   	S16_PULSE_DUMP_DRIFT_ADD_ESP          		       3
    
    if(WL_ESC->rate_counter==0)
    {    	
				esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD,
																														SLIP_F_DUMP_THRESHOLD_M,
																														SLIP_F_DUMP_THRESHOLD_L );
       	esp_tempW1 = WL_ESC->slip_control+esp_tempW0;
				
				if(esp_tempW1<1)
       	{
       		esp_tempW1 = 1;
       	}
       	else
       	{
       		;
       	}
        if(esp_tempW1>S16_RATE_INTERVAL_GAIN)
        {
        	WL_ESC->rate_interval=S16_RATE_INTERVAL_FRONT_MIN-1;
        }
        else
        {
       		WL_ESC->rate_interval=McrAbs(S16_RATE_INTERVAL_GAIN/esp_tempW1);
        }
        	
		
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_MAX_H,
																			                    S16_RATE_INTERVAL_MAX_M,
																			                    S16_RATE_INTERVAL_MAX_L );
                                  
        if(WL_ESC->rate_interval>esp_tempW0)
        {
        	WL_ESC->rate_interval=esp_tempW0;
        }
        else
        {
        	;
        }
        
#if __ROP         

/* '06.04.19 : According to Wheel-Slip, Dump-rate Adaptation */

     esp_tempW8 = LCESP_s16IInter3Point( McrAbs(alat), S16_ROP_DUMP_ALAT_1st, S16_ROP_DUMP_RATE_1st, 
                                                    S16_ROP_DUMP_ALAT_2nd, S16_ROP_DUMP_RATE_2nd, 
                                                    S16_ROP_DUMP_ALAT_3rd, S16_ROP_DUMP_RATE_3rd ) ;  
    if ( ROP_DYN_STABLE_PHASE == 1 )
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_DUMP_DYN_WEG_VEL_100K ) ;	               
    }
    else
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_DUMP_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_DUMP_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_DUMP_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_DUMP_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_DUMP_WEG_VEL_100K ) ;	     
    }        
    
    esp_tempW8 = (int16_t)( (((int32_t)esp_tempW8)*esp_tempW1)/100 );                
    
	if( esp_tempW8 < 1 )
	{
		esp_tempW8 = 1 ;
	}
	else
	{
		;
	}                                                                                              
 
     esp_tempW9 = LCESP_s16IInter3Point( McrAbs(alat), S16_ROP_DUMP_ALAT_1st, 1, 
                                                    S16_ROP_DUMP_ALAT_2nd, 3, 
                                                    S16_ROP_DUMP_ALAT_3rd, 6 ) ; 
                                                     
     if ( Flg_Make_Oversteer_By_Ay==1 )  
     {                            
        esp_tempW8 = esp_tempW8 + S16_ROP_DUMP_RATE_ADD ;       
     }
     else
     {
        ;
     }
                                                    
#if __CAR==GM_TAHOE || __CAR ==GM_SILVERADO
     esp_tempW6 = LCESP_s16IInter2Point( WL_ESC->slip_measurement, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW8 ) ;  
                                                  
     esp_tempW7 = LCESP_s16IInter2Point( WL_ESC->slip_measurement, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
      															 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW9 ) ;   
#else

     esp_tempW6 = LCESP_s16IInter2Point( WL_ESC->slip_measurement, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW8 ) ;  
                                                  
     esp_tempW7 = LCESP_s16IInter2Point( WL_ESC->slip_measurement, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW9 ) ;                                                    

#endif

        if( ROP_REQ_ACT==1 ) 
        {
             WL_ESC->rate_interval = esp_tempW6 ;   
        } 
        else if( ROP_FADE_MODE==1 ) 
        {             
             WL_ESC->rate_interval = esp_tempW7 ;   
        } 
        else 
        {
             WL_ESC->rate_interval = WL_ESC->rate_interval ;   
        }              
#endif 

        if(ABS_fz==1)
        {	    	        	
					esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD,
																															SLIP_F_DUMP_THRESHOLD_M,
																															SLIP_F_DUMP_THRESHOLD_L );
        	esp_tempW1 = WL_ESC->slip_control+esp_tempW0;
					
					if(esp_tempW1<1)
	       	{
  					esp_tempW1 = 1;
    			}
    			else
    			{
    				;
    			}
        	if(esp_tempW1>=S16_RATE_INTERVAL_GAIN_ABS)
          {
  					WL_ESC->rate_interval=S16_RATE_INTERVAL_FRONT_MIN_ABS-1;
          }
          else
          {
    				WL_ESC->rate_interval=McrAbs(S16_RATE_INTERVAL_GAIN_ABS/esp_tempW1);
          }
        
          esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_ABS_MAX_H,
                      																			S16_RATE_INTERVAL_ABS_MAX_M,
                      																			S16_RATE_INTERVAL_ABS_MAX_L );                        
          if(WL_ESC->rate_interval>esp_tempW1)
          {
          	WL_ESC->rate_interval=esp_tempW1;  
          }
          else
	        {
           	;
          }        
        }
        if(FLAG_DETECT_DRIFT==1)
        {
        	if(ABS_fz==1)
        	{
                WL_ESC->rate_interval = WL_ESC->rate_interval+S16_PULSE_DUMP_DRIFT_ADD_ABS;
            }
            else
            {
            	WL_ESC->rate_interval = WL_ESC->rate_interval+S16_PULSE_DUMP_DRIFT_ADD_ESP;
            }
        }

        if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m<(-WHEEL_SLIP_60))&&(WL_ESC->slip_error_dot>0) )
        {
        	WL_ESC->rate_interval = S16_RATE_INTERVAL_FRONT_MIN-1;
        }
        else
        {
        	;
        }
       
    }
    
}
void LCESP_vCalPressureRateR(struct ESC_STRUCT *WL_ESC)
{
    if(WL_ESC->rate_counter==0)
    {    	
			esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD,
																													SLIP_R_DUMP_THRESHOLD_M,
																													SLIP_R_DUMP_THRESHOLD_L );
     	esp_tempW1 = WL_ESC->slip_control+esp_tempW0;

      if(esp_tempW1<1)
     	{
     		esp_tempW1 = 1;
     	}
     	else
     	{
     		;
     	}
			if(esp_tempW1>=S16_RATE_INTERVAL_REAR_GAIN)
      {
     		WL_ESC->rate_interval=S16_RATE_INTERVAL_REAR_MIN-1;
      }
      else
      {
     		WL_ESC->rate_interval=McrAbs(S16_RATE_INTERVAL_REAR_GAIN/esp_tempW1);
      }

      esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_REAR_MAX_H,
																		                    S16_RATE_INTERVAL_REAR_MAX_M,
																		                    S16_RATE_INTERVAL_REAR_MAX_L );                    
      if(WL_ESC->rate_interval>esp_tempW0) 
      {
      	WL_ESC->rate_interval=esp_tempW0;
      }
      else
      {
      	;
      }

      if(ABS_fz==1)
      {				
				esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD,
																														SLIP_R_DUMP_THRESHOLD_M,
																														SLIP_R_DUMP_THRESHOLD_L );
		   	esp_tempW1 = WL_ESC->slip_control+esp_tempW0;
				
				if(esp_tempW1<1)
		   	{
		   		esp_tempW1 = 1;
		   	}		
       	else
       	{
       		;
       	}
				if(esp_tempW1>=S16_RATE_INTERVAL_REAR_GAIN_ABS)
		    {
  					WL_ESC->rate_interval=S16_RATE_INTERVAL_REAR_MIN_ABS-1;
		    }
		    else
		    {
					WL_ESC->rate_interval=McrAbs(S16_RATE_INTERVAL_REAR_GAIN_ABS/esp_tempW1);
		    }
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_REAR_ABS_MAXH,
                     																			S16_RATE_INTERVAL_REAR_ABS_MAXM,
                     																			S16_RATE_INTERVAL_REAR_ABS_MAXL );                                  
		    if(WL_ESC->rate_interval>esp_tempW1) 
		    {
		    	WL_ESC->rate_interval=esp_tempW1;  
		    }
		    else
		    {
		    	;
		    }        
      }
        
         if((WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->slip_m<(-WHEEL_SLIP_50))&&(WL_ESC->slip_error_dot>0))
        {
      	WL_ESC->rate_interval = S16_RATE_INTERVAL_REAR_MIN-1;
        }
        else
        {
        	;
        }
    }
}
#else
void LCESP_vCalPressureRateF(void)
{       
	#define   	S16_PULSE_DUMP_DRIFT_ADD_ABS          		       0
	#define   	S16_PULSE_DUMP_DRIFT_ADD_ESP          		       3
    
    if(rate_counter_front==0){
    /*	if(S16_RATE_INTERVAL_MAX_L > S16_RATE_INTERVAL_MAX_M)
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_MAX_L;
    	}
    	else
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_MAX_M;
    	}
    	
    	if(esp_tempW9 < S16_RATE_INTERVAL_MAX_H)
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_MAX_H;
    	}
    	else 
    	{
    		;
    	} */
    	
				esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD,
																														SLIP_F_DUMP_THRESHOLD_M,
																														SLIP_F_DUMP_THRESHOLD_L );
       	esp_tempW1 = slip_control_front+esp_tempW0;
				
				if(esp_tempW1<1)
       	{
       		esp_tempW1 = 1;
       	}
       	else
       	{
       		;
       	}
        if(esp_tempW1>S16_RATE_INTERVAL_GAIN)
        {
        	rate_interval_front=S16_RATE_INTERVAL_FRONT_MIN-1;
        }
        else
        {
       		rate_interval_front=McrAbs(S16_RATE_INTERVAL_GAIN/esp_tempW1);
        }
        	
		
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_MAX_H,
																			                    S16_RATE_INTERVAL_MAX_M,
																			                    S16_RATE_INTERVAL_MAX_L );
                                  
        if(rate_interval_front>esp_tempW0)
        {
        	rate_interval_front=esp_tempW0;
        }
        else
        {
        	;
        }
        
#if __ROP         

/* '06.04.19 : According to Wheel-Slip, Dump-rate Adaptation */

     esp_tempW8 = LCESP_s16IInter3Point( McrAbs(alat), S16_ROP_DUMP_ALAT_1st, S16_ROP_DUMP_RATE_1st, 
                                                    S16_ROP_DUMP_ALAT_2nd, S16_ROP_DUMP_RATE_2nd, 
                                                    S16_ROP_DUMP_ALAT_3rd, S16_ROP_DUMP_RATE_3rd ) ;  
    if ( ROP_DYN_STABLE_PHASE == 1 )
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_DUMP_DYN_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_DUMP_DYN_WEG_VEL_100K ) ;	               
    }
    else
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_DUMP_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_DUMP_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_DUMP_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_DUMP_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_DUMP_WEG_VEL_100K ) ;	     
    }        
    
    esp_tempW8 = (int16_t)( (((int32_t)esp_tempW8)*esp_tempW1)/100 );                
    
	if( esp_tempW8 < 1 )
	{
		esp_tempW8 = 1 ;
	}
	else
	{
		;
	}                                                                                              
 
     esp_tempW9 = LCESP_s16IInter3Point( McrAbs(alat), S16_ROP_DUMP_ALAT_1st, 1, 
                                                    S16_ROP_DUMP_ALAT_2nd, 3, 
                                                    S16_ROP_DUMP_ALAT_3rd, 6 ) ; 
                                                     
     if ( Flg_Make_Oversteer_By_Ay==1 )  
     {                            
        esp_tempW8 = esp_tempW8 + S16_ROP_DUMP_RATE_ADD ;       
     }
     else
     {
        ;
     }
                                                    
#if __CAR==GM_TAHOE || __CAR ==GM_SILVERADO
     esp_tempW6 = LCESP_s16IInter2Point( slip_measurement_front, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW8 ) ;  
                                                  
     esp_tempW7 = LCESP_s16IInter2Point( slip_measurement_front, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
      															 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW9 ) ;   
#else

     esp_tempW6 = LCESP_s16IInter2Point( slip_measurement_front, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW8 ) ;  
                                                  
     esp_tempW7 = LCESP_s16IInter2Point( slip_measurement_front, S16_ROP_DUMP_RATE_SLIP_MAX, 1, 
                                                                 S16_ROP_DUMP_RATE_SLIP_MIN, esp_tempW9 ) ;                                                    

#endif

        if( ROP_REQ_ACT==1 ) 
        {
             rate_interval_front = esp_tempW6 ;   
        } 
        else if( ROP_FADE_MODE==1 ) 
        {             
             rate_interval_front = esp_tempW7 ;   
        } 
        else 
        {
             rate_interval_front = rate_interval_front ;   
        }              
#endif 

        if(ABS_fz==1)
        {
        	/*if(S16_RATE_INTERVAL_ABS_MAX_L > S16_RATE_INTERVAL_ABS_MAX_M)
		    	{
		    		esp_tempW8 = S16_RATE_INTERVAL_ABS_MAX_L;
		    	}
		    	else
		    	{
		    		esp_tempW8 = S16_RATE_INTERVAL_ABS_MAX_M;
		    	}
		    	
		    	if(esp_tempW8 < S16_RATE_INTERVAL_ABS_MAX_H)
		    	{
		    		esp_tempW8 = S16_RATE_INTERVAL_ABS_MAX_H;
		    	}
		    	else
		    	{
		    		;
		    	}        */
	    	        	
					esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_F_DUMP_THRESHOLD,
																															SLIP_F_DUMP_THRESHOLD_M,
																															SLIP_F_DUMP_THRESHOLD_L );
        	esp_tempW1 = slip_control_front+esp_tempW0;
					
					if(esp_tempW1<1)
	       	{
  					esp_tempW1 = 1;
    			}
    			else
    			{
    				;
    			}
        	if(esp_tempW1>=S16_RATE_INTERVAL_GAIN_ABS)
          {
  					rate_interval_front=S16_RATE_INTERVAL_FRONT_MIN_ABS-1;
          }
          else
          {
    				rate_interval_front=McrAbs(S16_RATE_INTERVAL_GAIN_ABS/esp_tempW1);
          }
        
          esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_ABS_MAX_H,
                      																			S16_RATE_INTERVAL_ABS_MAX_M,
                      																			S16_RATE_INTERVAL_ABS_MAX_L );                        
          if(rate_interval_front>esp_tempW1)
          {
          	rate_interval_front=esp_tempW1;  
          }
          else
	        {
           	;
          }        
        }
        if(FLAG_DETECT_DRIFT==1)
        {
        	if(ABS_fz==1)
        	{
                rate_interval_front = rate_interval_front+S16_PULSE_DUMP_DRIFT_ADD_ABS;
            }
            else
            {
            	rate_interval_front = rate_interval_front+S16_PULSE_DUMP_DRIFT_ADD_ESP;
            }
        }

        if( ((SLIP_CONTROL_NEED_FL==1)&&(slip_m_fl<(-WHEEL_SLIP_60))&&(slip_error_dot_front>0) )
         || ((SLIP_CONTROL_NEED_FR==1)&&(slip_m_fr<(-WHEEL_SLIP_60))&&(slip_error_dot_front>0) ) )
        {
        	rate_interval_front = S16_RATE_INTERVAL_FRONT_MIN-1;
        }
        else
        {
        	;
        }
       
    }
    if(ABS_fz==0)
    {
        LCESP_vCountPressureF();
        LCESP_vCountLockF();
    }
    else
    {
    	;
    }
/*    #if __PWM 061113 deleted by eslim */
    LCESP_vCheckPressureCycleF();
/*    #endif 061113 deleted by eslim */
    
    LCESP_vDetectCompulsionRise(); 
    LCESP_vCalVariableThresholdF();    
    LCESP_vDetectCompulsionHoldF();
    LAESP_vActuateNcNovalveF();
    

    
}
void LCESP_vCalPressureRateR(void)
{
    if(rate_counter_rear==0)
    {
    	
    	/* if(S16_RATE_INTERVAL_REAR_MAX_L > S16_RATE_INTERVAL_REAR_MAX_M)
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_REAR_MAX_L;
    	}
    	else
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_REAR_MAX_M;
    	}
    	
    	if(esp_tempW9 < S16_RATE_INTERVAL_REAR_MAX_H)
    	{
    		esp_tempW9 = S16_RATE_INTERVAL_REAR_MAX_H;
    	}
    	else
    	{
    		;
    	} */
    	
			esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD,
																													SLIP_R_DUMP_THRESHOLD_M,
																													SLIP_R_DUMP_THRESHOLD_L );
     	esp_tempW1 = slip_control_rear+esp_tempW0;
      
      if(esp_tempW1<1)
     	{
     		esp_tempW1 = 1;
     	}
     	else
     	{
     		;
     	}
			if(esp_tempW1>=S16_RATE_INTERVAL_REAR_GAIN)
      {
     		rate_interval_rear=S16_RATE_INTERVAL_REAR_MIN-1;
      }
      else
      {
     		rate_interval_rear=McrAbs(S16_RATE_INTERVAL_REAR_GAIN/esp_tempW1);
      }

      esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_REAR_MAX_H,
																		                    S16_RATE_INTERVAL_REAR_MAX_M,
																		                    S16_RATE_INTERVAL_REAR_MAX_L );                    
      if(rate_interval_rear>esp_tempW0) 
      {
      	rate_interval_rear=esp_tempW0;
      }
      else
      {
      	;
      }

      if(ABS_fz==1)
      {
      	    /* if(S16_RATE_INTERVAL_REAR_ABS_MAXL > S16_RATE_INTERVAL_REAR_ABS_MAXM)
		   	{
		   		esp_tempW8 = S16_RATE_INTERVAL_REAR_ABS_MAXL;
		   	}
		   	else
		   	{
		   		esp_tempW8 = S16_RATE_INTERVAL_REAR_ABS_MAXM;
		   	}
		   	
		   	if(esp_tempW8 < S16_RATE_INTERVAL_REAR_MAX_H)
		   	{
		   		esp_tempW8 = S16_RATE_INTERVAL_REAR_ABS_MAXH;
		   	}
		   	else
		   	{
		   		;
		   	} */
				
				esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, SLIP_R_DUMP_THRESHOLD,
																														SLIP_R_DUMP_THRESHOLD_M,
																														SLIP_R_DUMP_THRESHOLD_L );
		   	esp_tempW1 = slip_control_rear+esp_tempW0;
				
				if(esp_tempW1<1)
		   	{
		   		esp_tempW1 = 1;
		   	}		
       	else
       	{
       		;
       	}
				if(esp_tempW1>=S16_RATE_INTERVAL_REAR_GAIN_ABS)
		    {
  					rate_interval_front=S16_RATE_INTERVAL_REAR_MIN_ABS-1;
		    }
		    else
		    {
					rate_interval_rear=McrAbs(S16_RATE_INTERVAL_REAR_GAIN_ABS/esp_tempW1);
		    }
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_RATE_INTERVAL_REAR_ABS_MAXH,
                     																			S16_RATE_INTERVAL_REAR_ABS_MAXM,
                     																			S16_RATE_INTERVAL_REAR_ABS_MAXL );                                  
		    if(rate_interval_rear>esp_tempW1) 
		    {
		    	rate_interval_rear=esp_tempW1;  
		    }
		    else
		    {
		    	;
		    }        
      }
        
         if( ((SLIP_CONTROL_NEED_RL==1)&&(slip_m_rl<(-WHEEL_SLIP_50))&&(slip_error_dot_rear>0))
          ||((SLIP_CONTROL_NEED_RR==1)&&(slip_m_rr<(-WHEEL_SLIP_50))&&(slip_error_dot_rear>0)))
        {
      	rate_interval_rear = S16_RATE_INTERVAL_REAR_MIN-1;
        }
        else
        {
        	;
        }
    }
    if(ABS_fz==0)
    {
        LCESP_vCountPressureR();
        LCESP_vCountLockR();
    }
    else
    {
    	;
    }
/*    #if __PWM 061113 deleted by eslim */
    LCESP_vCheckPressureCycleR();
/*    #endif 061113 deleted by eslim */
    
    LCESP_vCalVariableThresholdR();
    LCESP_vDetectCompulsionHoldR();     
    LAESP_vActuateNcNovalveR();
    
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCESP_vCountPressureF(struct ESC_STRUCT *WL_ESC)
{
    	if(WL_ESC->SLIP_CONTROL_NEED==1)
        {
        	if(WL_ESC->esp_control_mode<3)
        	{
            	WL_ESC->esp_pressure_count++;
            }
            else if(WL_ESC->esp_control_mode<4)
            {
            	WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count;
            }
            else
            {
				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(WL_ESC->esp_pressure_count>21)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-5;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(WL_ESC->esp_pressure_count>13)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-4;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}					
				}
				else
				{
					if(WL_ESC->esp_pressure_count>13)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-3;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}		
				}
				/* 060210 added by eslim */
				if(WL_ESC->esp_pressure_count<0)
				{
					WL_ESC->esp_pressure_count=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
		}
		else
		{
			WL_ESC->esp_pressure_count=0;
		}   
			
}
void LCESP_vCountPressureR(struct ESC_STRUCT *WL_ESC)
{
		if((WL_ESC->SLIP_CONTROL_NEED==1)&&(ACT_TWO_WHEEL==0))
		{
			if(WL_ESC->esp_control_mode<3)
			{
 				WL_ESC->esp_pressure_count++;
			}
			else if(WL_ESC->esp_control_mode<4)
 			{
				WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count;
            }
			else
 			{
				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(WL_ESC->esp_pressure_count>13)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-6;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(WL_ESC->esp_pressure_count>13)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-5;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}
				}
				else
				{
					if(WL_ESC->esp_pressure_count>13)
					{	
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-3;
					}
					else
					{
						WL_ESC->esp_pressure_count=WL_ESC->esp_pressure_count-1;
					}
				}
				/* 060210 added by eslim */
				if(WL_ESC->esp_pressure_count<0)
				{
					WL_ESC->esp_pressure_count=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
		}			
		else 
		{
			WL_ESC->esp_pressure_count=0;
		}			                         	
}
#else
void LCESP_vCountPressureF(void)
{
    	if(SLIP_CONTROL_NEED_FL==1)
        {
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
 				if(slip_status_position==1)        	
 				{
#endif
        	if(esp_control_mode_fl<3)
        	{
            	esp_pressure_count_fl++;
            }
            else if(esp_control_mode_fl<4)
            {
            	esp_pressure_count_fl=esp_pressure_count_fl;
            }
            else
            {
				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_fl>21)
					{	
						esp_pressure_count_fl=esp_pressure_count_fl-5;
					}
					else
					{
						esp_pressure_count_fl=esp_pressure_count_fl-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_fl>13)
					{	
						esp_pressure_count_fl=esp_pressure_count_fl-4;
					}
					else
					{
						esp_pressure_count_fl=esp_pressure_count_fl-1;
					}					
				}
				else
				{
					if(esp_pressure_count_fl>13)
					{	
						esp_pressure_count_fl=esp_pressure_count_fl-3;
					}
					else
					{
						esp_pressure_count_fl=esp_pressure_count_fl-1;
					}		
				}
				/* 060210 added by eslim */
				if(esp_pressure_count_fl<0)
				{
					esp_pressure_count_fl=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
 		 }
		 else
		 {
			 ; 
		 }
#endif
		}
		else
		{
			esp_pressure_count_fl=0;
		}   
		if(SLIP_CONTROL_NEED_FR==1)
		{
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(slip_status_position==2)        	
			{
#endif
			if(esp_control_mode_fr<3)
			{
				esp_pressure_count_fr++;
			}
			else if(esp_control_mode_fr<4)
			{
				esp_pressure_count_fr=esp_pressure_count_fr;
			}
			else
			{
 				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_fr>13)
					{	
						esp_pressure_count_fr=esp_pressure_count_fr-5;
					}
					else
					{
						esp_pressure_count_fr=esp_pressure_count_fr-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_fr>13)
					{	
						esp_pressure_count_fr=esp_pressure_count_fr-4;
					}
					else
					{
						esp_pressure_count_fr=esp_pressure_count_fr-1;
					}
				}
				else
				{
					if(esp_pressure_count_fr>13)
					{	
						esp_pressure_count_fr=esp_pressure_count_fr-3;
					}
					else
					{
						esp_pressure_count_fr=esp_pressure_count_fr-1;
					}
				}
				/* 060210 added by eslim */
				if(esp_pressure_count_fr<0)
				{
					esp_pressure_count_fr=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
 		 }
		 else
		 {
			 ; 
		 }
#endif
		}
		else
		{
			esp_pressure_count_fr=0;
		}	
}
void LCESP_vCountPressureR(void)
{
		if((SLIP_CONTROL_NEED_RL==1)&&(ACT_TWO_WHEEL==0))
		{
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(slip_status_position==3)        	
			{
#endif
			if(esp_control_mode_rl<3)
			{
 				esp_pressure_count_rl++;
			}
			else if(esp_control_mode_rl<4)
 			{
				esp_pressure_count_rl=esp_pressure_count_rl;
            }
			else
 			{
				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_rl>13)
					{	
						esp_pressure_count_rl=esp_pressure_count_rl-6;
					}
					else
					{
						esp_pressure_count_rl=esp_pressure_count_rl-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_rl>13)
					{	
						esp_pressure_count_rl=esp_pressure_count_rl-5;
					}
					else
					{
						esp_pressure_count_rl=esp_pressure_count_rl-1;
					}
				}
				else
				{
					if(esp_pressure_count_rl>13)
					{	
						esp_pressure_count_rl=esp_pressure_count_rl-3;
					}
					else
					{
						esp_pressure_count_rl=esp_pressure_count_rl-1;
					}
				}
				/* 060210 added by eslim */
				if(esp_pressure_count_rl<0)
				{
					esp_pressure_count_rl=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
 		 }
		 else
		 {
			 ; 
		 }
#endif
		}			
		else 
		{
			esp_pressure_count_rl=0;
		}			                         
		if((SLIP_CONTROL_NEED_RR==1)&&(ACT_TWO_WHEEL==0))
		{
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
			if(slip_status_position==4)        	
			{
#endif
			if(esp_control_mode_rr<3)
			{
				esp_pressure_count_rr++;
			}
			else if(esp_control_mode_rr<4)
			{
				esp_pressure_count_rr=esp_pressure_count_rr;
			}
			else
			{
				if((esp_mu<=lsesps16MuMed)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_rr>13)
					{	
						esp_pressure_count_rr=esp_pressure_count_rr-6;
					}
					else
					{
						esp_pressure_count_rr=esp_pressure_count_rr-1;
					}
				}
				else if((esp_mu<=lsesps16MuMedHigh)&&(FLAG_ON_CENTER==0))
				{
					if(esp_pressure_count_rr>13)
					{	
						esp_pressure_count_rr=esp_pressure_count_rr-5;
					}
					else
					{
						esp_pressure_count_rr=esp_pressure_count_rr-1;
					}
				}
				else
				{
					if(esp_pressure_count_rr>13)
					{	
						esp_pressure_count_rr=esp_pressure_count_rr-3;
					}
					else
					{
						esp_pressure_count_rr=esp_pressure_count_rr-1;
					}
				}
				/* 060210 added by eslim */
				if(esp_pressure_count_rr<0)
				{
					esp_pressure_count_rr=0;
				}
				else
				{
					;
				}
				/* 060210 added by eslim */
			}						
#if (__CRAM == 1)||(__Pro_Active_Roll_Ctrl==1)||(__ROP==1)
 		 }
		 else
		 {
			 ; 
		 }
#endif
		}
		else 
		{
			esp_pressure_count_rr=0;
		}	
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
/* #if __PWM 061113 deleted by eslim */
void LCESP_vDetectFullRise(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL)
{   	
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if(( lsespu1AHBGEN3MpresBrkOn==0 )&&( ABS_fz == 0 )&&(vrefk >=VREF_K_20_KPH))
#else
	if(( MPRESS_BRAKE_ON==0 )&&( ABS_fz == 0 )&&(vrefk >=VREF_K_20_KPH))
#endif
	{
		if(ROP_REQ_ACT == 1)
		{
			WL_ESC->FULL_RISE_MODE = 1;
		}
		else if((WL_ESC->SLIP_CONTROL_NEED==1)&&(esp_mu > lsesps16MuMedHigh)&&( delta_moment_beta > 200 )
		&&(WL_ESC->true_slip_target <= (- FULL_RISE_ENTER_TAR_SLIP))&&(yaw_error_dot >= FULL_RISE_ENTER_YAW_ERR_D))
		{
				WL_ESC->FULL_RISE_MODE=1;
		}		
		else 
		{
			WL_ESC->FULL_RISE_MODE=0;
		}
	}
	else if((vrefk >=VREF_K_20_KPH)&&(ABS_fz==1))
	{
		if((WL->MSL_BASE==0)&&(WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->arad>30)&&(WL_ESC->slip_error_dot<(-200))&&(esp_mu > lsesps16MuMed)
			&&(yaw_error_dot >= 50)&&(WL_ESC->true_slip_target<=(-WHEEL_SLIP_30)))
			{
				WL_ESC->FULL_RISE_MODE=1;
			}
		else 
		{
			WL_ESC->FULL_RISE_MODE=0;
		}
	}
	else
	{
		WL_ESC->FULL_RISE_MODE = 0;
	}
}
#else
/* #if __PWM 061113 deleted by eslim */
void LCESP_vDetectFullRise(void)
{   
#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	if(( lsespu1AHBGEN3MpresBrkOn==0 )&&( ABS_fz == 0 )&&(vrefk >=VREF_K_20_KPH))
#else
	if(( MPRESS_BRAKE_ON==0 )&&( ABS_fz == 0 )&&(vrefk >=VREF_K_20_KPH))
#endif
	{
		if(ROP_REQ_ACT == 1)
		{
			FULL_RISE_MODE = 1;
		}
		else if((SLIP_CONTROL_NEED_FL==1)&&(esp_mu > lsesps16MuMedHigh)&&( delta_moment_beta > 200 )
		&&(true_slip_target_fl <= (- FULL_RISE_ENTER_TAR_SLIP))&&(yaw_error_dot >= FULL_RISE_ENTER_YAW_ERR_D))
		{
				FULL_RISE_MODE=1;
		}
		else if((SLIP_CONTROL_NEED_FR==1)&&(esp_mu > lsesps16MuMedHigh)&&( delta_moment_beta < (-200) )
		&&(true_slip_target_fr <= (- FULL_RISE_ENTER_TAR_SLIP))&&(yaw_error_dot >= FULL_RISE_ENTER_YAW_ERR_D))
		{
				FULL_RISE_MODE=1;
		}		
		else 
		{
			FULL_RISE_MODE=0;
		}
	}
	else if((vrefk >=VREF_K_20_KPH)&&(ABS_fz==1))
	{
		if((RL.MSL_BASE==0)&&(SLIP_CONTROL_NEED_FL==1)&&(arad_fl>30)&&(slip_error_dot_front<(-200))&&(esp_mu > lsesps16MuMed)
			&&(yaw_error_dot >= 50)&&(true_slip_target_fl<=(-WHEEL_SLIP_30)))
			{
				FULL_RISE_MODE=1;
			}
		else if((RR.MSL_BASE==0)&&(SLIP_CONTROL_NEED_FR==1)&&(arad_fr>30)&&(slip_error_dot_front<(-200))&&(esp_mu > lsesps16MuMed)
			&&(yaw_error_dot >= 50)&&(true_slip_target_fr<=(-WHEEL_SLIP_30)))		
			{
				FULL_RISE_MODE=1;
			}
		else 
		{
			FULL_RISE_MODE=0;
		}
	}
	else
	{
		FULL_RISE_MODE = 0;
	}
}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
void LCESP_vCheckPressureCycleF(void)
{
#if ESC_PULSE_UP_RISE==1 

    if(SLIP_CONTROL_NEED_FL==1) 
    {
#if __LFC_AFTER_HOLD
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fl >=3)&&(yaw_cdc_work_on_counter>4)) 	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#else    	
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fl==4)&&(yaw_cdc_work_on_counter>4)) 	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */ 
#endif
			{
				esp_control_cycle_fl = 1;
			}
			else
			{
				esp_control_cycle_fl = 0;	
			}
	    }
	  	else if(ESP_PULSE_DUMP_FL==1)
		{
		    	esp_control_cycle_fl=1;
		}
  
#if __TSP
    else if (TSP_ON==1) 
    {
    	esp_control_cycle_fl=1;
    }
#endif
    else 
    {
    	esp_control_cycle_fl=0;
    }
  
  
    if(SLIP_CONTROL_NEED_FR==1) 
    {
#if __LFC_AFTER_HOLD
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fr >=3)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#else    	
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fr==4)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#endif
		{
			esp_control_cycle_fr = 1;
		}
		else
		{
			esp_control_cycle_fr = 0;
		}
	}
		else if (ESP_PULSE_DUMP_FR==1) 
	    {
	    	esp_control_cycle_fr=1;
	    }
#if __TSP
    else if (TSP_ON==1) 
    {
    	esp_control_cycle_fr=1;
    }
#endif
    else 
    {
    	esp_control_cycle_fr=0;
    }
#else
    if(SLIP_CONTROL_NEED_FL==1) 
    {
#if __LFC_AFTER_HOLD
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fl >=3)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#else    	
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fl==4)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#endif
		{
			esp_control_cycle_fl=1;
		}
		else if(ESP_PULSE_DUMP_FL==1)
	    {
	    	esp_control_cycle_fl=1;
	    }
		else
		{
			;	
		}
    }
  
#if __TSP
    else if (TSP_ON==1) 
    {
    	esp_control_cycle_fl=1;
    }
#endif
    else 
    {
    	esp_control_cycle_fl=0;
    }
  
  
    if(SLIP_CONTROL_NEED_FR==1) 
    {
#if __LFC_AFTER_HOLD
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fr >=3)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#else    	
		if((ROP_REQ_ACT==0)&&(esp_control_mode_fr==4)&&(yaw_cdc_work_on_counter>4))  	/* &&(((FLAG_BIG_OVERSTEER==0)&&(esp_mu>=S16_MU_MED))||(esp_mu<S16_MU_MED)) @ 9305 */
#endif
		{
			esp_control_cycle_fr=1;
		}
		else if (ESP_PULSE_DUMP_FR==1) 
	    {
	    	esp_control_cycle_fr=1;
	    }
		else
		{
			;
		}
    }    
#if __TSP
    else if (TSP_ON==1) 
    {
    	esp_control_cycle_fr=1;
    }
#endif
    else 
    {
    	esp_control_cycle_fr=0;
    }
#endif

}
void LCESP_vCheckPressureCycleR(void)
{
    if(SLIP_CONTROL_NEED_RL==1) 
    {
#if __LFC_AFTER_HOLD
        if(esp_control_mode_rl>=3) 
#else
        if(esp_control_mode_rl==4) 
#endif   
        {
        	esp_control_cycle_rl=1;
        }
        else if (ESP_PULSE_DUMP_RL==1) 
	    {
	    	esp_control_cycle_rl=1;
	    }
        else
        {
        	;
        }
    }
    else 
    {
    	esp_control_cycle_rl=0;
    }

    if(SLIP_CONTROL_NEED_RR==1) 
    {
#if __LFC_AFTER_HOLD
        if(esp_control_mode_rr>=3) 
#else
        if(esp_control_mode_rr==4) 
#endif
        {
        	esp_control_cycle_rr=1;
        }
        else if (ESP_PULSE_DUMP_RR==1) 
	    {
	    	esp_control_cycle_rr=1;
	    }
        else
        {
        	;
        }
    }
    else 
    {
    	esp_control_cycle_rr=0;
    }

}
#endif


#if ENABLE_PULSE_DOWN_DUMP  
void LCESP_vCalSliprRatioRate(void)
{
/******************************************************/
	lcs16espSlipratearrayfl[0] = lcs16espSlipratearrayfl[1];
	lcs16espSlipratearrayfl[1] = lcs16espSlipratearrayfl[2];
	lcs16espSlipratearrayfl[2] = lcs16espSlipratearrayfl[3];
	lcs16espSlipratearrayfl[3] = slip_m_fl;
	
	esp_tempW0 = lcs16espSlipratearrayfl[3] - lcs16espSlipratearrayfl[1];
	
	lcs16espSlipratefl = LCESP_s16Lpf1Int(esp_tempW0,lcs16espSliprateflold, (uint8_t)slip_filter_gain_varying);
	lcs16espSliprateflold = lcs16espSlipratefl;
/******************************************************/
	lcs16espSlipratearrayfr[0] = lcs16espSlipratearrayfr[1];
	lcs16espSlipratearrayfr[1] = lcs16espSlipratearrayfr[2];
	lcs16espSlipratearrayfr[2] = lcs16espSlipratearrayfr[3];
	lcs16espSlipratearrayfr[3] = slip_m_fr;

	esp_tempW1 = lcs16espSlipratearrayfr[3] - lcs16espSlipratearrayfr[1];
	
	lcs16espSlipratefr = LCESP_s16Lpf1Int(esp_tempW1,lcs16espSlipratefrold, (uint8_t)slip_filter_gain_varying);
	lcs16espSlipratefrold = lcs16espSlipratefr;
/******************************************************/

	lcs16espSlipratearrayrl[0] = lcs16espSlipratearrayrl[1];
	lcs16espSlipratearrayrl[1] = lcs16espSlipratearrayrl[2];
	lcs16espSlipratearrayrl[2] = lcs16espSlipratearrayrl[3];
	lcs16espSlipratearrayrl[3] = slip_m_rl;
	
	esp_tempW2 = lcs16espSlipratearrayrl[3] - lcs16espSlipratearrayrl[1];
	
	lcs16espSlipraterl = LCESP_s16Lpf1Int(esp_tempW2,lcs16espSlipraterlold, (uint8_t)slip_filter_gain_varying);
	lcs16espSlipraterlold = lcs16espSlipraterl;
/******************************************************/

	lcs16espSlipratearrayrr[0] = lcs16espSlipratearrayrr[1];
	lcs16espSlipratearrayrr[1] = lcs16espSlipratearrayrr[2];
	lcs16espSlipratearrayrr[2] = lcs16espSlipratearrayrr[3];
	lcs16espSlipratearrayrr[3] = slip_m_rr;

	esp_tempW3 = lcs16espSlipratearrayrr[3] - lcs16espSlipratearrayrr[1];
	
	lcs16espSlipraterr = LCESP_s16Lpf1Int(esp_tempW3,lcs16espSlipraterrold, (uint8_t)slip_filter_gain_varying);
	lcs16espSlipraterrold = lcs16espSlipraterr;
/******************************************************/
	
}

void LCESP_vSetPulseDumpFront(void)
{

   	/* esp_tempW0 = pulse_dump_time_front; */
   	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_FL==0)
      &&(SLIP_CONTROL_NEED_FL_old==1)&&(esp_on_counter_fl<pulse_dump_time_front_fl))
   	{   
       	ESP_PULSE_DUMP_FL=1;
           
       if(ABS_fz==1)
       {
            if((SLIP_CONTROL_NEED_FL_old==1)&&(slip_m_fl >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_FL=0;
            }
            else
            {
            	;
            }
           
       } 
       else if(BTCS_ON==1)
       {
            if((SLIP_CONTROL_NEED_FL_old==1)&&(slip_m_fl <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_FL=0;
            }
            else
            {
            	;
            }
  	  }
  	  else
  	  {
  	  	;
  	  }
   }
   else    
   {
   		ESP_PULSE_DUMP_FL=0;
   } 
	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_FR==0)
      	&&(SLIP_CONTROL_NEED_FR_old==1)&&(esp_on_counter_fr<pulse_dump_time_front_fr))
   	{   
       	ESP_PULSE_DUMP_FR=1;
           
       	if(ABS_fz==1)
       	{
			if((SLIP_CONTROL_NEED_FR_old==1)&&(slip_m_fr >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_FR=0;
            }
            else
            {
            	;
            }
           
       	} 
       	else if(BTCS_ON==1)
       	{
            if((SLIP_CONTROL_NEED_FR_old==1)&&(slip_m_fr <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_FR=0;
            }
            else
            {
            	;
            }
  	  	}
  	  	else
  	  	{
  	  		;
  		}
    }
    else    
    {
   		ESP_PULSE_DUMP_FR=0;
    } 
   
	if((ESP_PULSE_DUMP_FL==1)||(ESP_PULSE_DUMP_FR==1))
	{
   		ESP_PULSE_DUMP_FLAG_F = 1;
	}
	else
	{
		ESP_PULSE_DUMP_FLAG_F = 0;
	}

    if((SLIP_CONTROL_NEED_FL==1)&&(ESP_PULSE_DUMP_FL==0))
    {
        SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL;
    	esp_pressure_count_fl_old = esp_pressure_count_fl;           
    }
    else if(ESP_PULSE_DUMP_FL==1)
	{
    	if(SLIP_CONTROL_NEED_FL==1)
    	{
            SLIP_CONTROL_NEED_FL_old = 0;
         
        }
        else if((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FL_old==1))
        {
        	SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL_old;
        }
        else
        {
        	;
        }        
        esp_pressure_count_fl_old = esp_pressure_count_fl_old;
     }
     else
     {
            SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL;
            esp_pressure_count_fl_old = esp_pressure_count_fl;
     } 
     
    if((SLIP_CONTROL_NEED_FR==1)&&(ESP_PULSE_DUMP_FR==0))
    {
            SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR;
            esp_pressure_count_fr_old = esp_pressure_count_fr;           
    }
    else if(ESP_PULSE_DUMP_FR==1)
    {
    	if(SLIP_CONTROL_NEED_FR==1)
    	{
            SLIP_CONTROL_NEED_FR_old = 0;
         
        }
        else if((SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FR_old==1))
        {
        	SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR_old;
        }
        else
        {
        	;
        }        
        esp_pressure_count_fr_old = esp_pressure_count_fr_old;
     }
     else
     {
            SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR;
            esp_pressure_count_fr_old = esp_pressure_count_fr;
     }    
}
void LCESP_vSetPulseDumpRear(void)
{
  	/*esp_tempW1 = pulse_dump_time_rear;*/
   
   if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_RL==0)
      &&(SLIP_CONTROL_NEED_RL_old==1)&&(esp_on_counter_rl<pulse_dump_time_rear_rl))
   {   
       ESP_PULSE_DUMP_RL=1;
               
       if(ABS_fz==1)
       {
            if((SLIP_CONTROL_NEED_RL_old==1)&&(slip_m_rl >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_RL=0;
			}
            else
            {
            	;
            }
           
       } 
       else if(BTCS_ON==1)
       {
            if((SLIP_CONTROL_NEED_RL_old==1)&&(slip_m_rl <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_RL=0;
            }
            else
            {
            	;
            }
  	  }
  	  else
  	  {
  	  	;
  	  }
    }
    else    
    {
   		ESP_PULSE_DUMP_RL=0; 
    }
	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_RR==0)
      	&&(SLIP_CONTROL_NEED_RR_old==1)&&(esp_on_counter_rr<pulse_dump_time_rear_rr))
    {   
       	ESP_PULSE_DUMP_RR=1;
           
       	if(ABS_fz==1)
       	{
			if((SLIP_CONTROL_NEED_RR_old==1)&&(slip_m_rr >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_RR=0;
            }
            else
            {
            	;
            }           
       	} 
       	else if(BTCS_ON==1)
       	{
            if((SLIP_CONTROL_NEED_RR_old==1)&&(slip_m_rr <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_RR=0;
            }
            else
            {
            	;
            }
  	   	}
  	   	else
  	   	{
			;
  	   	}
   	}
   	else    
   	{
   		ESP_PULSE_DUMP_RR=0; 
   	} 
   
	if((ESP_PULSE_DUMP_RL==1)||(ESP_PULSE_DUMP_RR==1))
	{
   		ESP_PULSE_DUMP_FLAG_R = 1;
	}
	else
	{
		ESP_PULSE_DUMP_FLAG_R = 0;
	}

    if((SLIP_CONTROL_NEED_RL==1)&&(ESP_PULSE_DUMP_RL==0))
    {
        SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL;
        esp_pressure_count_rl_old = esp_pressure_count_rl;           
    }
    else if(ESP_PULSE_DUMP_RL==1)
	{
    	if(SLIP_CONTROL_NEED_RL==1)
    	{
            SLIP_CONTROL_NEED_RL_old = 0; 
        }
        else if((SLIP_CONTROL_NEED_RL==0)&&(SLIP_CONTROL_NEED_RL_old==1))
        {
        	SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL_old;
        }
        else
        {
        	;
        }        
        esp_pressure_count_rl_old = esp_pressure_count_rl_old;
	}
	else
    {
    	SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL;
        esp_pressure_count_rl_old = esp_pressure_count_rl;
    } 
     
	if((SLIP_CONTROL_NEED_RR==1)&&(ESP_PULSE_DUMP_RR==0))
    {
        SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR;
    	esp_pressure_count_rr_old = esp_pressure_count_rr;           
    }
    else if(ESP_PULSE_DUMP_RR==1)
    {
    	if(SLIP_CONTROL_NEED_RR==1)
    	{
            SLIP_CONTROL_NEED_RR_old = 0;
         
        }
        else if((SLIP_CONTROL_NEED_RR==0)&&(SLIP_CONTROL_NEED_RR_old==1))
        {
        	SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR_old;
        }
        else
        {
        	;
        }        
        esp_pressure_count_rr_old = esp_pressure_count_rr_old;
     }
     else
     {
            SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR;
            esp_pressure_count_rr_old = esp_pressure_count_rr;
     }

}
#endif

void LCESC_vCalFadeoutTime(void)
{
	if((SLIP_CONTROL_NEED_FL==1)&&(slip_control_need_counter_fl==0))
	{
		esp_tempW0 = slip_m_fl_current;	
		esp_tempW4 = FL.s16_Estimated_Active_Press;			
	}
	else
	{
		esp_tempW0 = 0;
		esp_tempW4 = 0;			
	}
	
	if((SLIP_CONTROL_NEED_FR==1)&&(slip_control_need_counter_fr==0))
	{
		esp_tempW1 = slip_m_fr_current;	
		esp_tempW5 = FR.s16_Estimated_Active_Press;			
	}
	else
	{
		esp_tempW1 = 0;
		esp_tempW5 = 0;
	}	
	
	if((SLIP_CONTROL_NEED_RL==1)&&(slip_control_need_counter_rl==0))
	{
		esp_tempW2 = slip_m_rl_current;	
		esp_tempW6 = RL.s16_Estimated_Active_Press;			
	}
	else
	{
		esp_tempW2 = 0;
		esp_tempW6 = 0;
	}	
	
	if((SLIP_CONTROL_NEED_RR==1)&&(slip_control_need_counter_rr==0))
	{
		esp_tempW3 = slip_m_rr_current;	
		esp_tempW7 = RR.s16_Estimated_Active_Press;			
	}
	else
	{
		esp_tempW3 = 0;
		esp_tempW7 = 0;
	}			
	
	
	if((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FL_old==1))
	{
		FL_ESC.debuger = 0;
		if((ABS_fz == 1)&&(PARTIAL_BRAKE == 0)&&(slip_m_fl < (-S16_FADE_OUT_EXIT_SLIP_ABS)))
		{
			if ( esp_mu >=lsesps16MuHigh )
			{
				pulse_dump_time_front_fl = S16_FADE_OUT_TIME_ABS;
				FL_ESC.debuger = 1;
			}
			else
			{
				pulse_dump_time_front_fl = 28;
				FL_ESC.debuger = 2;
			}
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if(((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1)&&(FL.s16_Estimated_Active_Press  >= lsesps16AHBGEN3mpress))
#else
		else if(((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1)&&(FL.s16_Estimated_Active_Press  >= mpress))
#endif
#if __TSP
        ||((TSP_ON==1)&&(FL.s16_Estimated_Active_Press  >= (esp_tempW4+MPRESS_10BAR)))
#endif
#if __HDC
        ||((lcu1HdcActiveFlg==1)&&(FL.s16_Estimated_Active_Press  >= (esp_tempW4+MPRESS_10BAR)))
#endif
#if __ACC
        ||((lcu1AccActiveFlg==1)&&(FL.s16_Estimated_Active_Press  >= (esp_tempW4+MPRESS_10BAR)))
#endif
		)		
		{
			pulse_dump_time_front_fl = 28;
			FL_ESC.debuger = 3;
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if((lsespu1AHBGEN3MpresBrkOn==0)
#else
		else if((MPRESS_BRAKE_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif		
		&&((FL.s16_Estimated_Active_Press  > 50)
			||(slip_m_fl <= (-WHEEL_SLIP_4))))
		{
			pulse_dump_time_front_fl = 28;
			FL_ESC.debuger = 4;
		}		
		else
		{
			pulse_dump_time_front_fl = 0;
			FL_ESC.debuger = 5;
		}		
	}
	else
	{
		pulse_dump_time_front_fl = 0;
		FL_ESC.debuger = 6;
	}
	
	if((SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FR_old==1))
	{
		FR_ESC.debuger = 0;
		if((ABS_fz == 1)&&(PARTIAL_BRAKE == 0)&&(slip_m_fr < (-S16_FADE_OUT_EXIT_SLIP_ABS)))
		{
			if ( esp_mu >=lsesps16MuHigh )
			{
				pulse_dump_time_front_fr = S16_FADE_OUT_TIME_ABS;
				FR_ESC.debuger = 1;
			}
			else
			{
				pulse_dump_time_front_fr = 28;
				FR_ESC.debuger = 2;
			}
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if(((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1)&&(FR.s16_Estimated_Active_Press  >= lsesps16AHBGEN3mpress))
#else
		else if(((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1)&&(FR.s16_Estimated_Active_Press  >= mpress))
#endif
#if __TSP
        ||((TSP_ON==1)&&(FR.s16_Estimated_Active_Press  >= (esp_tempW5+MPRESS_10BAR)))
#endif
#if __HDC
        ||((lcu1HdcActiveFlg==1)&&(FR.s16_Estimated_Active_Press  >= (esp_tempW5+MPRESS_10BAR)))
#endif
#if __ACC
        ||((lcu1AccActiveFlg==1)&&(FR.s16_Estimated_Active_Press  >= (esp_tempW5+MPRESS_10BAR)))
#endif
		)		
		{
			pulse_dump_time_front_fr = 28;
			FR_ESC.debuger = 3;
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if((lsespu1AHBGEN3MpresBrkOn==0)
#else
		else if((MPRESS_BRAKE_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif	
		&&((FR.s16_Estimated_Active_Press  > 50)
		  ||(slip_m_fr <= (-WHEEL_SLIP_4))))
		{
			pulse_dump_time_front_fr = 28;
			FR_ESC.debuger = 4;
		}			
		else
		{
			pulse_dump_time_front_fr = 0;
			FR_ESC.debuger = 5;
		}		
	}
	else
	{
		pulse_dump_time_front_fr = 0;
		FR_ESC.debuger = 6;
	}
	
	if((SLIP_CONTROL_NEED_RL==0)&&(SLIP_CONTROL_NEED_RL_old==1))
	{
		RL_ESC.debuger = 0;
		if((ABS_fz == 1)&&(PARTIAL_BRAKE == 0)&&(slip_m_rl < (-WHEEL_SLIP_4)))
		{
			pulse_dump_time_rear_rl = 28;
			RL_ESC.debuger = 1;
		}	
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if(((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1)&&(RL.s16_Estimated_Active_Press  >= lsesps16AHBGEN3mpress))
#else
		else if(((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1)&&(RL.s16_Estimated_Active_Press  >= mpress))
#endif
#if __TSP
        ||((TSP_ON==1)&&(RL.s16_Estimated_Active_Press  >= (esp_tempW6+MPRESS_10BAR)))
#endif
#if __HDC
        ||((lcu1HdcActiveFlg==1)&&(RL.s16_Estimated_Active_Press  >= (esp_tempW6+MPRESS_10BAR)))
#endif
#if __ACC
        ||((lcu1AccActiveFlg==1)&&(RL.s16_Estimated_Active_Press  >= (esp_tempW6+MPRESS_10BAR)))
#endif
		)		
		{
			pulse_dump_time_rear_rl = 28;
			RL_ESC.debuger = 2;
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if((lsespu1AHBGEN3MpresBrkOn==0)
#else
		else if((MPRESS_BRAKE_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif	
		&&((RL.s16_Estimated_Active_Press  > 50)
			||(slip_m_rl <= (-WHEEL_SLIP_4))))
		{
			pulse_dump_time_rear_rl = 28;
			RL_ESC.debuger = 3;
		}				
		else
		{
			pulse_dump_time_rear_rl = 0;
			RL_ESC.debuger = 4;
		}		
	}
	else
	{
		pulse_dump_time_rear_rl = 0;
		RL_ESC.debuger = 5;
	}
	
	if((SLIP_CONTROL_NEED_RR==0)&&(SLIP_CONTROL_NEED_RR_old==1))
	{
		RR_ESC.debuger = 0;
		if((ABS_fz == 1)&&(PARTIAL_BRAKE == 0)&&(slip_m_rr < (-WHEEL_SLIP_4)))
		{
			pulse_dump_time_rear_rr = 28;
			RR_ESC.debuger = 1;
		}	
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if(((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE==1)&&(RR.s16_Estimated_Active_Press  >= lsesps16AHBGEN3mpress))
#else
		else if(((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE==1)&&(RR.s16_Estimated_Active_Press  >= mpress))
#endif
#if __TSP
        ||((TSP_ON==1)&&(RR.s16_Estimated_Active_Press  >= esp_tempW7+MPRESS_10BAR))
#endif
#if __HDC
        ||((lcu1HdcActiveFlg==1)&&(RR.s16_Estimated_Active_Press  >= esp_tempW7+MPRESS_10BAR))
#endif
#if __ACC
        ||((lcu1AccActiveFlg==1)&&(RR.s16_Estimated_Active_Press  >= esp_tempW7+MPRESS_10BAR))
#endif
		)		
		{
			pulse_dump_time_rear_rr = 28;
			RR_ESC.debuger = 2;
		}
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		else if((lsespu1AHBGEN3MpresBrkOn==0)
#else
		else if((MPRESS_BRAKE_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif	
		&&((RR.s16_Estimated_Active_Press  > 50)
			||(slip_m_rr <= (-WHEEL_SLIP_4))))			
		{
			pulse_dump_time_rear_rr = 28;
			RR_ESC.debuger = 3;
		}	
		else
		{
			pulse_dump_time_rear_rr = 0;
			RR_ESC.debuger = 4;
		}		
	}
	else
	{
		pulse_dump_time_rear_rr = 0;
		RR_ESC.debuger = 5;
	}
	
	if(FL_ESC.lcesps16IdbTarPress < MPRESS_0G2BAR)
	{
        pulse_dump_time_front_fl= 0 ;
        FL_ESC.debuger = 7;
	}
	else
	{
	    ;   
	}
	if(FR_ESC.lcesps16IdbTarPress < MPRESS_0G2BAR)
	{
        pulse_dump_time_front_fr= 0;
        FR_ESC.debuger = 7;
	}
	else
	{
	    ;   
	}
    if(RL_ESC.lcesps16IdbTarPress < MPRESS_0G2BAR)
	{
        pulse_dump_time_rear_rl= 0 ;
        RL_ESC.debuger = 6;
	}
	else
	{
	    ;   
	}
	if(RR_ESC.lcesps16IdbTarPress < MPRESS_0G2BAR)
	{
        pulse_dump_time_rear_rr= 0 ;
        RR_ESC.debuger = 6;
	}
	else
	{
	    ;   
	}	
	
}

void LCESC_vSetFadeoutControlFront(void)
{
	 if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_FL==0)
      &&(SLIP_CONTROL_NEED_FL_old==1)&&(esp_on_counter_fl<pulse_dump_time_front_fl))
   	{   
       	ESP_PULSE_DUMP_FL=1;
       	FL_ESC.debuger = FL_ESC.debuger + 10;
           
       if(ABS_fz==1)
       {
            if((SLIP_CONTROL_NEED_FL_old==1)&&(slip_m_fl >(-S16_FADE_OUT_EXIT_SLIP_ABS))) 
            { 
            	ESP_PULSE_DUMP_FL=0;
            	FL_ESC.debuger = FL_ESC.debuger + 20;
            }
            else
            {
            	;
            }
           
       } 
/*       else if(BTCS_ON == 1) //(BTCS_fl==1) 	// 08.02.17 SWD YJH         
       {
            if((SLIP_CONTROL_NEED_FL_old==1)&&(slip_m_fl <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_FL=0;
            }
            else
            {
            	;
            }
  	  } */
  	  else
  	  {
  	  	;
  	  }
  	  	#if (__IDB_LOGIC == ENABLE)   
	    LCESC_vCalFadeoutFrontTPforIDB(&FL_ESC,&FL);	
	    #endif
   }
   else    
   {
   		ESP_PULSE_DUMP_FL=0;
   		FL_ESC.debuger = FL_ESC.debuger + 30;
   } 
	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_FR==0)
      	&&(SLIP_CONTROL_NEED_FR_old==1)&&(esp_on_counter_fr<pulse_dump_time_front_fr))
   	{   
       	ESP_PULSE_DUMP_FR=1;
       	FR_ESC.debuger = FR_ESC.debuger + 10;
           
       	if(ABS_fz==1)
       	{
			if((SLIP_CONTROL_NEED_FR_old==1)&&(slip_m_fr >(-S16_FADE_OUT_EXIT_SLIP_ABS))) 
            { 
            	ESP_PULSE_DUMP_FR=0;
            	FR_ESC.debuger = FR_ESC.debuger + 20;
            }
            else
            {
            	;
            }
           
       	} 
/*       	else if(BTCS_ON==1)	//(BTCS_fr==1) 	// 08.02.17 SWD	YJH 
       	{
            if((SLIP_CONTROL_NEED_FR_old==1)&&(slip_m_fr <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_FR=0;
            }
            else
            {
            	;
            }
  	  	}   */
  	  	else
  	  	{
  	  		;
  		}
  		#if (__IDB_LOGIC == ENABLE)   
	    LCESC_vCalFadeoutFrontTPforIDB(&FR_ESC,&FR);	
	    #endif
    }
    else    
    {
   		ESP_PULSE_DUMP_FR=0;
   		FR_ESC.debuger = FR_ESC.debuger + 30;
    }

   #if(__ESC_TCMF_CONTROL ==1)
   FL_ESC.ESP_PULSE_DUMP = ESP_PULSE_DUMP_FL;
   FR_ESC.ESP_PULSE_DUMP = ESP_PULSE_DUMP_FR;
   #endif
   
	if((ESP_PULSE_DUMP_FL==1)||(ESP_PULSE_DUMP_FR==1))
	{
   		ESP_PULSE_DUMP_FLAG_F = 1;
	}
	else
	{
		ESP_PULSE_DUMP_FLAG_F = 0;
	}

    if((SLIP_CONTROL_NEED_FL==1)&&(ESP_PULSE_DUMP_FL==0)&&(lcu1US2WHControlFlag==0))
    {
        SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL;
    	esp_pressure_count_fl_old = esp_pressure_count_fl;           
    }
    else if(ESP_PULSE_DUMP_FL==1)
	{
    	if(SLIP_CONTROL_NEED_FL==1)
    	{
            SLIP_CONTROL_NEED_FL_old = 0;
         
        }
        /* else if((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FL_old==1))
        {
        	SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL_old;
        } */
        else
        {
        	;
        }        
        esp_pressure_count_fl_old = esp_pressure_count_fl_old;
     }
     else
     {
     	if(lcu1US2WHControlFlag==0)
     	{
            SLIP_CONTROL_NEED_FL_old = SLIP_CONTROL_NEED_FL;
            esp_pressure_count_fl_old = esp_pressure_count_fl;
     } 
     } 
     
    if((SLIP_CONTROL_NEED_FR==1)&&(ESP_PULSE_DUMP_FR==0)&&(lcu1US2WHControlFlag==0))
    {
            SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR;
            esp_pressure_count_fr_old = esp_pressure_count_fr;           
    }
    else if(ESP_PULSE_DUMP_FR==1)
    {
    	if(SLIP_CONTROL_NEED_FR==1)
    	{
            SLIP_CONTROL_NEED_FR_old = 0;
         
        }
        /* else if((SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FR_old==1))
        {
        	SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR_old;
        } */
        else
        {
        	;
        }        
        esp_pressure_count_fr_old = esp_pressure_count_fr_old;
     }
     else
     {
     	if(lcu1US2WHControlFlag==0)
     	{
            SLIP_CONTROL_NEED_FR_old = SLIP_CONTROL_NEED_FR;
            esp_pressure_count_fr_old = esp_pressure_count_fr;
        }
     }    
     
     if(ESP_PULSE_DUMP_FLAG_F==1)
     {
     	if( FLAG_DELTAYAW_SIGNCHANGE == 1 )
		{     
     		Yaw_Sign_Change_Hold_Flg = 1;
     	}
     }
     else 
     {
    	Yaw_Sign_Change_Hold_Flg = 0;
     }
}
void LCESC_vSetFadeoutControlRear(void)
{
	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_RL==0)
      &&(SLIP_CONTROL_NEED_RL_old==1)&&(esp_on_counter_rl<pulse_dump_time_rear_rl))
   {   
       ESP_PULSE_DUMP_RL=1;
       RL_ESC.debuger = RL_ESC.debuger + 10;
               
       if(ABS_fz==1)
       {
            if((SLIP_CONTROL_NEED_RL_old==1)&&(slip_m_rl >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_RL=0;
            	RL_ESC.debuger = RL_ESC.debuger + 20;
			}
            else
            {
            	;
            }
           
       } 
/*       else if(BTCS_ON == 1)  //(BTCS_rl==1)  // 08.02.17 SWD YJH 
       {
            if((SLIP_CONTROL_NEED_RL_old==1)&&(slip_m_rl <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_RL=0;
            }
            else
            {
            	;
            }
  	  } */
  	  else
  	  {
  	  	;
  	  }
  	  
  	  if(((lcu1OVER2WHControlOld ==1)||(lcu1OVER2WHFadeoutOld_RL ==1))&&(lcu1OVER2WHFadeout_RL ==0))
  	  {  	  	  
  	  	  ESP_PULSE_DUMP_RL = 0;
  	  	  RL_ESC.debuger = RL_ESC.debuger + 30;
  	  }
  	  else
  	  {
  	  	  ;
  	  }		
  	  #if (__IDB_LOGIC == ENABLE)   
	  LCESC_vCalFadeoutRearTPforIDB(&RL_ESC,&RL);	
	  #endif	  	  
    }
    else    
    {
   		ESP_PULSE_DUMP_RL=0;
   		RL_ESC.debuger = RL_ESC.debuger + 40;
    }
	if(((VDC_DISABLE_FLG==0)||(PARTIAL_PERFORM==1))&&(SLIP_CONTROL_NEED_RR==0)
      	&&(SLIP_CONTROL_NEED_RR_old==1)&&(esp_on_counter_rr<pulse_dump_time_rear_rr))
    {   
       	ESP_PULSE_DUMP_RR=1;
       	RR_ESC.debuger = RR_ESC.debuger + 10;
           
       	if(ABS_fz==1)
       	{
			if((SLIP_CONTROL_NEED_RR_old==1)&&(slip_m_rr >(-WHEEL_SLIP_4))) 
            { 
            	ESP_PULSE_DUMP_RR=0;
            	RR_ESC.debuger = RR_ESC.debuger + 20;
            }
            else
            {
            	;
            }           
       	} 
/*       	else if(BTCS_ON == 1) //(BTCS_rr==1)  // 08.02.17 SWD YJH 
       	{
            if((SLIP_CONTROL_NEED_RR_old==1)&&(slip_m_rr <(WHEEL_SLIP_2))) 
            { 
            	ESP_PULSE_DUMP_RR=0;
            }
            else
            {
            	;
            }
  	   	}   */
  	   	else
  	   	{
			;
  	   	}
  	   	
  	   	if(((lcu1OVER2WHControlOld ==1)||(lcu1OVER2WHFadeoutOld_RR ==1))&&(lcu1OVER2WHFadeout_RR ==0))
  	   	{
  	   		  ESP_PULSE_DUMP_RR = 0;
  	   		RR_ESC.debuger = RR_ESC.debuger + 30;
  	   	}
  	   	else
  	    {
  	    	  ;
  	    }
  	    #if (__IDB_LOGIC == ENABLE)   
	    LCESC_vCalFadeoutRearTPforIDB(&RR_ESC,&RR);	
	    #endif
   	}
   	else    
   	{
   		ESP_PULSE_DUMP_RR=0;
   		RR_ESC.debuger = RR_ESC.debuger + 40;
   	} 
   
	if((ESP_PULSE_DUMP_RL==1)||(ESP_PULSE_DUMP_RR==1))
	{
   		ESP_PULSE_DUMP_FLAG_R = 1;
	}
	else
	{
		ESP_PULSE_DUMP_FLAG_R = 0;
	}

    if((SLIP_CONTROL_NEED_RL==1)&&(ESP_PULSE_DUMP_RL==0))
    {
        SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL;
        esp_pressure_count_rl_old = esp_pressure_count_rl;           
    }
    else if(ESP_PULSE_DUMP_RL==1)
	{
    	if(SLIP_CONTROL_NEED_RL==1)
    	{
            SLIP_CONTROL_NEED_RL_old = 0; 
        }
        /* else if((SLIP_CONTROL_NEED_RL==0)&&(SLIP_CONTROL_NEED_RL_old==1))
        {
        	SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL_old;
        } */
        else
        {
        	;
        }        
        esp_pressure_count_rl_old = esp_pressure_count_rl_old;
	}
	else
    {
    	SLIP_CONTROL_NEED_RL_old = SLIP_CONTROL_NEED_RL;
        esp_pressure_count_rl_old = esp_pressure_count_rl;
    } 
     
	if((SLIP_CONTROL_NEED_RR==1)&&(ESP_PULSE_DUMP_RR==0))
    {
        SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR;
    	esp_pressure_count_rr_old = esp_pressure_count_rr;           
    }
    else if(ESP_PULSE_DUMP_RR==1)
    {
    	if(SLIP_CONTROL_NEED_RR==1)
    	{
            SLIP_CONTROL_NEED_RR_old = 0;
         
        }
        /*else if((SLIP_CONTROL_NEED_RR==0)&&(SLIP_CONTROL_NEED_RR_old==1))
        {
        	SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR_old;
        } */
        else
        {
        	;
        }        
        esp_pressure_count_rr_old = esp_pressure_count_rr_old;
     }
     else
     {
            SLIP_CONTROL_NEED_RR_old = SLIP_CONTROL_NEED_RR;
            esp_pressure_count_rr_old = esp_pressure_count_rr;
     }
}
#if (__IDB_LOGIC == ENABLE)
void LCESC_vCalFadeoutFrontTPforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
 	esp_tempW1 = abs(wstr_dot);
 	
 	if(MPRESS_BRAKE_ON == 1)
 	{ 	
        esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_1BAR,
                           								500  , MPRESS_1BAR,
                           								1000 , MPRESS_1BAR,  
                           								2000 , MPRESS_2BAR  );  //Low Speed      
        
        esp_tempW3 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_1BAR,
                           								500  , MPRESS_1BAR,
                           								1000 , MPRESS_1BAR,  
                           								2000 , MPRESS_2BAR  ); //High Speed
                           
        esp_tempW4 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_1BAR,
                           								500  , MPRESS_1BAR,
                           								1000 , MPRESS_1BAR,  
                           								2000 , MPRESS_2BAR  );    //Med Speed
    }
    else
    {
        esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_1BAR,
                           								500  , MPRESS_2BAR,
                           								1000 , MPRESS_3BAR,  
                           								2000 , MPRESS_4BAR  );  //Low Speed      
        
        esp_tempW3 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_1BAR,
                           								500  , MPRESS_2BAR,
                           								1000 , MPRESS_3BAR,  
                           								2000 , MPRESS_4BAR  ); //Med Speed
                           
        esp_tempW4 = LCESP_s16IInter4Point(esp_tempW1,  10 ,    MPRESS_1BAR,
                           								500  ,  MPRESS_2BAR,
                           								1000 ,  MPRESS_3BAR,  
                           								2000 ,  MPRESS_4BAR   );    //High Speed    
    }        

                       								
    esp_tempW5 = LCESP_s16IInter3Point(vrefk,  VREF_K_50_KPH , esp_tempW2,
                       						   VREF_K_80_KPH , esp_tempW3,  
                       						   VREF_K_100_KPH , esp_tempW4);
                       						    
    if(Yaw_Sign_Change_Hold_Flg==1)
    {
        esp_tempW5 = 60;      
    }
//    WL_ESC->lcesps16IdbTarPressold = WL_ESC->lcesps16IdbTarPress;                   								  
//	WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbTarPressold - esp_tempW5;
	
	WL_ESC->lcesps16FadedelP = esp_tempW5;
	    
//	if(WL_ESC->lcesps16IdbTarPress < MPRESS_0BAR)
//	{
//        WL_ESC->lcesps16IdbTarPress	= MPRESS_0BAR ;    
//	}
//	else
//	{
//	    ;   
//	}
//	    
} 
void LCESC_vCalFadeoutRearTPforIDB(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
  	esp_tempW1 = abs(wstr_dot);
     	if(MPRESS_BRAKE_ON == 1)
 	{ 	
        esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G2BAR,
                           								500  , MPRESS_0G5BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  );  //Low Speed      
        
        esp_tempW3 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G2BAR,
                           								500  , MPRESS_0G5BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  ); //High Speed
                           
        esp_tempW4 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G2BAR,
                           								500  , MPRESS_0G5BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  );    //Med Speed
    }
    else
    {
        esp_tempW2 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G8BAR,
                           								500  , MPRESS_0G8BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  );  //Low Speed      
        
        esp_tempW3 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G8BAR,
                           								500  , MPRESS_0G8BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  ); //Med Speed
                           
        esp_tempW4 = LCESP_s16IInter4Point(esp_tempW1,  10 ,   MPRESS_0G8BAR,
                           								500  , MPRESS_0G8BAR,
                           								1000 , MPRESS_0G8BAR,  
                           								2000 , MPRESS_2BAR  );    //High Speed    
    }        

                       								
    esp_tempW5 = LCESP_s16IInter3Point(vrefk,  VREF_K_50_KPH , esp_tempW2,
                       						    VREF_K_80_KPH , esp_tempW3,  
                       						    VREF_K_100_KPH , esp_tempW4);
    
//    WL_ESC->lcesps16IdbTarPressold = WL_ESC->lcesps16IdbTarPress;
//    WL_ESC->lcesps16IdbTarPress = WL_ESC->lcesps16IdbTarPressold  - esp_tempW5;
	WL_ESC->lcesps16FadedelP = esp_tempW5;	
//	if(WL_ESC->lcesps16IdbTarPress < MPRESS_0BAR)
//	{
//        WL_ESC->lcesps16IdbTarPress	= MPRESS_0BAR ;    
//	}
//	else
//	{
//	    ;   
//	}
}	  
#endif
void LCESC_vCalFadeoutDumprateFront(void)
{

	int16_t v_f1 = S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, v_f2 = S16_ESP_SLIP_SPEED_GAIN_DEP_L_V; 
    int16_t v_f3 = S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, v_f4 =S16_ESP_SLIP_SPEED_GAIN_DEP_H_V; 
    int16_t v_gain1, v_gain2, v_gain3;
    int16_t gain_speed_fade_depend;
 
   	v_gain1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_LSP_F_YAW, 
                                S16_FADE_OUT_DUMP_MMU_LSP_F_YAW, S16_FADE_OUT_DUMP_LMU_LSP_F_YAW); 
   	v_gain2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_MSP_F_YAW, 
                                S16_FADE_OUT_DUMP_MMU_MSP_F_YAW, S16_FADE_OUT_DUMP_LMU_MSP_F_YAW);
   	v_gain3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_HSP_F_YAW, 
                                S16_FADE_OUT_DUMP_MMU_HSP_F_YAW, S16_FADE_OUT_DUMP_LMU_HSP_F_YAW); 
            
    if( vrefk <= v_f1 )
    {
        gain_speed_fade_depend = v_gain1;
    }
    else if ( vrefk < v_f2 )
    {
        if( v_f2==v_f1 ) 
        {
        	v_f2 = v_f2 + 1;
        }
        else
        {
        	;
        }
        gain_speed_fade_depend = v_gain1 + (int16_t)( (((int32_t)(v_gain2-v_gain1))*(vrefk-v_f1))/(v_f2-v_f1) );               
    }
    else if(vrefk < v_f3)
    {
        gain_speed_fade_depend = v_gain2;
    }
    else if ( vrefk < v_f4 )
    {
        if( v_f4==v_f3 ) 
        {
        	v_f4 = v_f4 + 1;
        }
        else
        {
        	;
        }
        gain_speed_fade_depend = v_gain2 + (int16_t)( (((int32_t)(v_gain3-v_gain2))*(vrefk-v_f3))/(v_f4-v_f3) );         
    } 
    else
    {
        gain_speed_fade_depend = v_gain3;
    }
    
    esp_tempW0 = McrAbs(yaw_out);
    
    esp_tempW9 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_GAIN_F_HMU, 
                                S16_FADE_OUT_DUMP_GAIN_F_MMU, S16_FADE_OUT_DUMP_GAIN_F_LMU); 
                                
#if	( __Ext_Under_Ctrl == 1 )	

  if( EXTREME_UNDER_CTRL_FADE_OUT ==1 )
  {	
		esp_tempW1 = esp_tempW0/S16_FADE_OUT_DUMP_GAIN_F_EXT_UND;
	}
	else
	{
    esp_tempW1 = (esp_tempW0+gain_speed_fade_depend)/esp_tempW9;
	}
	
#else
	
    esp_tempW1 = (esp_tempW0+gain_speed_fade_depend)/esp_tempW9;
    
#endif 
    
    if(esp_tempW1 <= 0)
    {
        esp_tempW1 = 1;
    }
    else
    {
        ;
    }            
    
    esp_tempW3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_HMU_FULL_DUMP, 
                                S16_FADE_OUT_MMU_FULL_DUMP, S16_FADE_OUT_LMU_FULL_DUMP); 
 
    esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_RATE_MIN_COMB_HMU_F, 
                                S16_FADE_OUT_RATE_MIN_COMB_MMU_F, S16_FADE_OUT_RATE_MIN_COMB_LMU_F); 
 
    if((esp_tempW0 <esp_tempW3) 
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
     	&&(lsespu1AHBGEN3MpresBrkOn==0)
#else
     	&&(MPRESS_BRAKE_ON==0)
#endif
			&&(ABS_fz==0)&&(BTCS_ON==0)&&(PBA_ON==0)	
#if __FBC
        &&(FBC_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif
#if __EPB_INTERFACE
        &&(lcu1EpbActiveFlg==0)
#endif
#if __TCMF_LowVacuumBoost
        &&(TCMF_LowVacuumBoost_flag==0)
#endif
#if __LVBA
        &&(lclvbau1ActiveFlg==0)
#endif
		)

    {
    	esp_tempW1 = 1;	
    }
    else if((esp_tempW1 < esp_tempW5)
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    	&&((lsespu1AHBGEN3MpresBrkOn==1)
#else
    	&&((MPRESS_BRAKE_ON==1)
#endif
    	||(ABS_fz==1)||(BTCS_ON==1)
#if __TSP
        ||(TSP_ON==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __EPB_INTERFACE
        ||(lcu1EpbActiveFlg==1)
#endif
#if __TCMF_LowVacuumBoost
        ||(TCMF_LowVacuumBoost_flag==1)
#endif 
#if __LVBA
        ||(lclvbau1ActiveFlg==1)
#endif   
	   ))
    {
    	esp_tempW1 = esp_tempW5;
    }
    else
    {
    	;
    }

    if((ESP_PULSE_DUMP_FL == 1)&&(SLIP_CONTROL_NEED_FL_old==1))
    {
    	if(fade_rate_counter_fl==0)
    	{
    		if((ABS_fz==0)&&( FL.s16_Estimated_Active_Press <= 0)) 
    		{
    			new_pulse_down_rate_fl = 1;	
    		}
    		else
    		{
    			new_pulse_down_rate_fl = esp_tempW1;	
    		}
    	}
    	else
    	{
    		;
    	}
    	if ( Yaw_Sign_Change_Hold_Flg == 1) 
    	{
    		 new_pulse_down_rate_fl = 1;
    	}
    }
    else
    {
    	new_pulse_down_rate_fl = 0;
    	fade_rate_counter_fl=0;
    }
    
    if((ESP_PULSE_DUMP_FR == 1)&&(SLIP_CONTROL_NEED_FR_old==1))
    {
    	if(fade_rate_counter_fr==0)
    	{
	    	if((ABS_fz==0)&&( FR.s16_Estimated_Active_Press <= 0))
	    	{
	    		new_pulse_down_rate_fr = 1;	
	    	}
	    	else
	    	{
	    		new_pulse_down_rate_fr = esp_tempW1;	
	    	}
	    }
	    else
	    {
	    	;
	    }
	    if ( Yaw_Sign_Change_Hold_Flg == 1) 
    	{
    		 new_pulse_down_rate_fr = 1;
    	}
    }
    else
    {
    	new_pulse_down_rate_fr = 0;
    	fade_rate_counter_fr = 0;
    }
	

}
void LCESC_vCalFadeoutDumprateRear(void)
{
		
	int16_t v_r1 = S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, v_r2 = S16_ESP_SLIP_SPEED_GAIN_DEP_L_V; 
    int16_t v_r3 = S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, v_r4 =S16_ESP_SLIP_SPEED_GAIN_DEP_H_V; 
    int16_t v_gainr1, v_gainr2, v_gainr3;  
    int16_t gain_speed_fade_depend_r;
 
   	v_gainr1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_LSP_R_YAW, 
                                S16_FADE_OUT_DUMP_MMU_LSP_R_YAW, S16_FADE_OUT_DUMP_LMU_LSP_R_YAW); 
   	v_gainr2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_MSP_R_YAW, 
                                S16_FADE_OUT_DUMP_MMU_MSP_R_YAW, S16_FADE_OUT_DUMP_LMU_MSP_R_YAW);
   	v_gainr3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_HMU_HSP_R_YAW, 
                                S16_FADE_OUT_DUMP_MMU_HSP_R_YAW, S16_FADE_OUT_DUMP_LMU_HSP_R_YAW); 
            
    if( vrefk <= v_r1 )
    {
        gain_speed_fade_depend_r = v_gainr1;
    }
    else if ( vrefk < v_r2 )
    {
        if( v_r2==v_r1 ) 
        {
        	v_r2 = v_r2 + 1;
        }
        else
        {
        	;
        }
        gain_speed_fade_depend_r = v_gainr1 + (int16_t)( (((int32_t)(v_gainr2-v_gainr1))*(vrefk-v_r1))/(v_r2-v_r1) );               
    }
    else if(vrefk < v_r3)
    {
        gain_speed_fade_depend_r = v_gainr2;
    }
    else if ( vrefk < v_r4 )
    {
        if( v_r4==v_r3 ) 
        {
        	v_r4 = v_r4 + 1;
        }
        else
        {
        	;
        }
        gain_speed_fade_depend_r = v_gainr2 + (int16_t)( (((int32_t)(v_gainr3-v_gainr2))*(vrefk-v_r3))/(v_r4-v_r3) );         
    } 
    else
    {
        gain_speed_fade_depend_r = v_gainr3;
    }
    
    esp_tempW0 = McrAbs(yaw_out);
    esp_tempW9 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_DUMP_GAIN_R_HMU, 
                                S16_FADE_OUT_DUMP_GAIN_R_MMU, S16_FADE_OUT_DUMP_GAIN_R_LMU); 
                                
    esp_tempW1 = (esp_tempW0+gain_speed_fade_depend_r)/ esp_tempW9;
    
    if(esp_tempW1 <=0)
    {
        esp_tempW1=1;
    }
    else
    {
        ;
    }            
    
    esp_tempW3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_HMU_FULL_DUMP, 
                                S16_FADE_OUT_MMU_FULL_DUMP, S16_FADE_OUT_LMU_FULL_DUMP); 

    esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_FADE_OUT_RATE_MIN_COMB_HMU_R, 
                                S16_FADE_OUT_RATE_MIN_COMB_MMU_R, S16_FADE_OUT_RATE_MIN_COMB_LMU_R); 


    if((esp_tempW0 <esp_tempW3) 
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
     	&&(lsespu1AHBGEN3MpresBrkOn==0)
#else
     	&&(MPRESS_BRAKE_ON==0)
#endif
			&&(ABS_fz==0)&&(BTCS_ON==0)&&(PBA_ON==0)
#if __FBC
        &&(FBC_ON==0)
#endif
#if __TSP
        &&(TSP_ON==0)
#endif
#if __HDC
        &&(lcu1HdcActiveFlg==0)
#endif
#if __ACC
        &&(lcu1AccActiveFlg==0)
#endif
#if __EPB_INTERFACE
        &&(lcu1EpbActiveFlg==0)
#endif
#if __TCMF_LowVacuumBoost
        &&(TCMF_LowVacuumBoost_flag==0)
#endif
#if __LVBA
        &&(lclvbau1ActiveFlg==0)
#endif
		)
    {
    	esp_tempW1 = 1;	
    }
    else if((esp_tempW1 < esp_tempW5)
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    	&&((lsespu1AHBGEN3MpresBrkOn==1)
#else
    	&&((MPRESS_BRAKE_ON==1)
#endif
    	||(ABS_fz==1)||(BTCS_ON==1)
#if __TSP
        ||(TSP_ON==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __EPB_INTERFACE
        ||(lcu1EpbActiveFlg==1)
#endif
#if __TCMF_LowVacuumBoost
        ||(TCMF_LowVacuumBoost_flag==1)
#endif    
#if __LVBA
        ||(lclvbau1ActiveFlg==1)
#endif
	   ))
    {
    	esp_tempW1 = esp_tempW5;
    }    
    else
    {
    	;
    }
    
    if((ESP_PULSE_DUMP_RL == 1)&&(SLIP_CONTROL_NEED_RL_old==1))
    {
    	if(fade_rate_counter_rl==0)
    	{
	    	if((ABS_fz==0)&&( RL.s16_Estimated_Active_Press <= 0))
	    	{
	    		new_pulse_down_rate_rl = 1;	
	    	}
	    	else
	    	{
	    		new_pulse_down_rate_rl =esp_tempW1;	
	    	}
	    }
	    else
	    {
	    	;
	    }
    }
    else
    {
    	new_pulse_down_rate_rl = 0;
    	fade_rate_counter_rl = 0;
    }
    
    if((ESP_PULSE_DUMP_RR == 1)&&(SLIP_CONTROL_NEED_RR_old==1))
    {
    	if(fade_rate_counter_rr==0)
    	{
	    	if((ABS_fz==0)&&( RR.s16_Estimated_Active_Press <= 0) )
	    	{
	    		new_pulse_down_rate_rr = 1;	
	    	}
	    	else
	    	{
	    		new_pulse_down_rate_rr = esp_tempW1;	
	    	}
	    }
	    else
	    {
	    	;
	    }
    }
    else
    {
    	new_pulse_down_rate_rr = 0;
    	fade_rate_counter_rr = 0;
    }
	
}
void LCESC_vCalFadeoutTctime(void)
{
	if( pulse_dump_time_front_fl > 0)
	{
		if(new_pulse_down_rate_fl <= 1)
		{
			tc_close_time_fl = 0;
		}
		else
		{
			tc_close_time_fl = pulse_dump_time_front_fl;	
		}
		
	}
	else
	{
		tc_close_time_fl = 0;
	}
	
	if( pulse_dump_time_front_fr > 0)
	{
		if(new_pulse_down_rate_fr <= 1)
		{
			tc_close_time_fr = 0;
		}
		else
		{
			tc_close_time_fr = pulse_dump_time_front_fr;	
		}
		
	}
	else
	{
		tc_close_time_fr = 0;
	}
	
	if( pulse_dump_time_rear_rl > 0)
	{
		if(new_pulse_down_rate_rl <= 1)
		{
			tc_close_time_rl = 0;
		}
		else
		{
			tc_close_time_rl = pulse_dump_time_rear_rl;	
		}
		
	}
	else
	{
		tc_close_time_rl = 0;
	}
	
	if( pulse_dump_time_rear_rr > 0)
	{
		if(new_pulse_down_rate_rr <= 1)
		{
			tc_close_time_rr = 0;
		}
		else
		{
			tc_close_time_rr = pulse_dump_time_rear_rr;	
		}
		
	}
	else
	{
		tc_close_time_rr = 0;
	}
	
}


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
/* 09 SWD */
void LCESP_vDetectNullDump(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL)
{
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn == 0)
#else
	if((MPRESS_BRAKE_ON == 0)
#endif
#if __ACC
        && (lcu1AccActiveFlg==0)
#endif
#if __HDC
        && (lcu1HdcActiveFlg==0)
#endif
#if __DEC
        && (lcu1DecActiveFlg==0)
#endif
#if __TSP
        && (TSP_ON==0)
#endif
	)
	{	
		if((WL_ESC->SLIP_CONTROL_NEED==1) && (WL->BTCS == 0) && (WL_ESC->esp_pressure_count == 0) && (WL->u8_Estimated_Active_Press <= 1))
		{
			WL_ESC->FLAG_NULL_DUMP = 1;
		}
		else
		{
			WL_ESC->FLAG_NULL_DUMP = 0;
		}
		
	}
}		
#else
/* 09 SWD */
void LCESP_vDetectNullDump(void)
{
#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	if((lsespu1AHBGEN3MpresBrkOn == 0)
#else
	if((MPRESS_BRAKE_ON == 0)
#endif
#if __ACC
        && (lcu1AccActiveFlg==0)
#endif
#if __HDC
        && (lcu1HdcActiveFlg==0)
#endif
#if __DEC
        && (lcu1DecActiveFlg==0)
#endif
#if __TSP
        && (TSP_ON==0)
#endif
	)
	{	
		if(((SLIP_CONTROL_NEED_RL==1) && (BTCS_rl == 0) && (esp_pressure_count_rl == 0) && (RL.u8_Estimated_Active_Press <= 1))
			|| ((SLIP_CONTROL_NEED_RR==1) && (BTCS_rr == 0) && (esp_pressure_count_rr == 0) && (RR.u8_Estimated_Active_Press <= 1)))
		{
			FLAG_NULL_DUMP_R = 1;
		}
		else
		{
			FLAG_NULL_DUMP_R = 0;
		}
		
	}
}		
 
#endif

 


#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPInterfaceSlipController
	#include "Mdyn_autosar.h"
#endif
