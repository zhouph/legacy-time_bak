/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCTCSCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "hm_logic_var.h"
#include "LCTCSCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCallEngineControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LDTCSCallDetection.h"
#include "LDBTCSCallDetection.h"
#include "LCRDCMCallControl.h"
#include "LDRTACallDetection.h"
#include "LCESPCallControl.h"
#include "LCEDCCallControl.h"
#include "LDABSCallDctForVref.H"
#include "LDENGEstPowertrain.h"
#include "LSABSCallSensorSignal.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSENGCallSensorSignal.h"
#include "LCTCSCallBrakeControl.h"
#include "LDTCSCallCalculation.h"
#include "LDABSDctRoadGradient.h"

#include "ETCSGenCode/LCETCS_vCallMain.h" /* MBD ETCS */

	#if __MU_EST_FOR_TCS
#include "LDABSCallEstVehDecel.H"	
	#endif

#if !SIM_MATLAB

/* Temporary Tuning Parameter */

#if __MODEL_CHANGE
    #define Lfront    (pEspModel->S16_LENGTH_CG_TO_FRONT)/10   /* m*100 */
    #define Lrear     (pEspModel->S16_LENGTH_CG_TO_REAR)/10    /* m*100 */
    #define Mass      (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */
    #define Iz         pEspModel->S16_MOMENT_OF_INERTIA        /* Kg*m */
    #define Str_ratio  pEspModel->S16_STEERING_RATIO_10        /* ratio*10 */
#endif
/* Temporary Tuning Parameter */

	#if __TCS_DRIVELINE_PROTECT
void LCTCS_vDetDrivelineProtection(void);
void LCTCS_vCalVariable4DP(void);
void LCTCS_vCalTargetDVForDP(void);
static void LCTCS_vDecMinTorqInDP(void);
	#endif

#endif /*!SIM_MATLAB*/
void LDTCS_vEstimateBTCSDiskTemp(void);

  #if __TCS_PREVENT_FAST_RPM_INCREASE
static void LCTCS_vDctFastRPMIncrease(void);
static void LCTCS_vIncGainInFastRPMInc(void);
  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */

  #if __TCS_BUMP_DETECTION  	
static void	LCTCS_vCompensateGainForBump(void);
  #endif	/* __TCS_BUMP_DETECTION */

#if !SIM_MATLAB
extern uint16_t system_loop_counter;
#endif

#if __BTC
void LCTCS_vCallControl(void);
void LCTCS_vDecideEngineVariation(void);


#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
void LCTCS_vCalMaxSpin4WD(void);
#endif




void LCTCS_vCalVelr(void);
static void LCTCS_vCalVelrFor2WD(void);
static void LCTCS_vCalVelrFor4WD(void);
#endif /* #if __BTC */

#if __TCS || __ETC
extern void LATCS_vConvertEMS2TCS(void);
void LCTCS_vSignalProcessing(void);
void LCTCS_vEstimateTraction(void);
void LCTCS_vEstimateTrace(void);
void LCTCS_vCallDetectTraction(void);

void LCTCS_vIncTorqAfterEEC(void);

#if !__VDC
void LCTCS_vCallDetectTrace(void);
#endif /* #if !__VDC */
void LCTCS_vControlTraction(void);
void LCTCS_v1stScanControl(void);
#if !__VDC
void LCTCS_vControlTrace(void);
#endif	/* #if !__VDC */
void LCTCS_vControlCommand(void);
void LCTCS_vDctSplitHillTendency(void);
void LCTCS_vCalculateVelocity(void);
void LCTCS_vCalTractionWheelSpeed(void);
void LCTCS_vCalFilteredWheelSpeed(void);
void LCTCS_vCalculateACC(void);
void LCTCS_vEstmateTMGear(void);
#if !__VDC
void LCTCS_vFindAyThreshold(void);
#endif	/* #if !__VDC */
void LCTCS_vInitialize(void);
static void LCTCS_vDecideTractionEntry(void);
void LCTCS_vDetectTraction(void);
#if !__VDC
void LCTCS_vInitializeTrace(void);
void LCTCS_vDetectTrace(void);
#endif	/* #if !__VDC */
void LCTCS_vCompensateGearChSlip(void);
void LCTCS_vDetectEngineStall(void);
int16_t  LCTCS_s16ICompensateEngineStall(int16_t InputValue);
void LCTCS_vCalMinimumTorqueLevel(void);
void LCTCS_vCalTargetTorqInHomoMu(void);
void LCTCS_vCalTargetTorqInSplitMu(void);
#if !__VDC
void LCTCS_vCallTraceLogic(void);
#endif	/* #if !__VDC */
void LCTCS_vInitializeFTCS(void);
void LCTCS_vCalculateETCErrorDV(void);

#if !__VDC
void LCTCS_vCalculateAY(void);
void LCTCS_vCalculateAYF(void);
#endif	/* #if !__VDC */
void LCTCS_vTurnOffFTCS(void);
void LCTCS_vChangeCtlMode(void);
void LCTCS_vChangeCtlModeSplit2Homo(void);
void LCTCS_vChangeCtlModeHomo2Split(void);
void LCTCS_vResetVariableOfH2Splt(void);
void LCTCS_vResetVariableOfSplt2H(void);
static void LCTCS_vChangeCtlHomoByHunting(void);

void LCTCS_vDetectTractionInTurn(void);
    #if __EMS_TEST_T
void LCTCS_vTestEngineMode(void);
    #endif

void LCTCS_vCalculateSpin(void);
void LCTCS_vCalculateTargetSpin(void);
void LCTCS_vIncTargetSpin(void);

void LCTCS_vCalculateError(void);
static void LCTCS_vDecideTractionPhase(void);

void LCTCS_vDetectMiniWheelTendency(void);
void LCTCS_vEnableMiniWheelDetect(void);
void LCTCS_vCntMiniWheelTendency(void);
void LCTCS_vSetMiniTireSuspect(void);
void LCTCS_vResetMiniTireSuspect(void);

void LCTCS_vSetResetCycle2nd(void);
void LCTCS_vCalculatePDGain(void);
void LCTCS_vIncPGainInHighSideSpin(void);
void LCTCS_vCalculatePDGain1stCycle(void);
void LCTCS_vGainDecreaseInHighMu(void);
void LCTCS_vGain1stCycleTransition(void);
void LCTCS_vCalPDGain2ndCycForETCS(void);
void LCTCS_vCalPDGain2ndCycForFTCS(void);
void LCTCS_vGain2ndCycleTransition(void);
void LCTCS_vCallController(void);
void LCTCS_vCallEngPDController(void);
void LCTCS_vMinimizeTruncationError(void);
void LCTCS_vSetBaseLevelForTorqCtl(void);

void LCTCS_vCompensateTorqForL2Splt(void);

void LCTCS_vCompTorqInGoodTracking(void);

void LCTCS_vSearchGoodTorqueLevel(void);
void LCTCS_vDetectTransitionPoint(void);
void LCTCS_vCalTorqueValue(void);
void LCTCS_vPreventBigSpin(void);

void LCTCS_vGainIncreaseInHighMu(void);

static void LCTCS_vGainDecreaseInGearShift(void);

static void LCTCS_vEstMuRefCoordinator(void);
	  
void LCTCS_vCheckPGainValidity(void);

#if TORQ_RECOVERY_DEPEND_ON_STEER
static void LCTCS_vDctSteerToZero(void);
#endif	/* #if TORQ_RECOVERY_DEPEND_ON_STEER */      

#if MAXIMUM_RPM_LIMIT
static void LDTCS_vCalAllowableMaxRPM(void);
static void LDTCS_vSetOverMaxRPMFlag(void);
static void LCTCS_vDecTorqInExcessiveRPM(void);
#endif		/* #if MAXIMUM_RPM_LIMIT */
            
/* <skeon OPTIME_MACFUNC_2> 110303*/
#ifndef OPTIME_MACFUNC_2

int16_t  LCTCS_s16ILimitRange( int16_t InputValue, int16_t MinValue, int16_t MaxValue );
int16_t  LCTCS_s16ILimitMinimum( int16_t InputValue, int16_t MinValue );
int16_t  LCTCS_s16IFindMaximum( int16_t InputValue1, int16_t InputValue2);
int16_t  LCTCS_s16IFindMinimum( int16_t InputValue1, int16_t InputValue2);
uint16_t LCTCS_u16ILimitMaximum( uint16_t InputValue, uint16_t MaxValue );
int16_t  LCTCS_s16ILimitMaximum( int16_t InputValue, int16_t MaxValue );

#endif
/* </skeon OPTIME_MACFUNC_2>      */

int16_t  LCTCS_s16IInterpDepAccel2by4( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_0_3, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_1_3);
int16_t  LCTCS_s16IInterpDepAccel3by4( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_0_3, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_1_3, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2, int16_t InputValue_2_3);
int16_t  LCTCS_s16IInterpDepAy4by3( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2, int16_t InputValue_3_0, int16_t InputValue_3_1, int16_t InputValue_3_2);
int16_t  LCTCS_s16IInterpDepRPM3by3( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2);


    #if __VDC
void LCTCS_vCalTargetSpinInTurn(void);
void LCTCS_vDetectFadeModeAfterETO(void);

void LCTCS_vDctTargetGoodTracking(void);

    #if __MY08_TCS_DCT_H2L
void LCTCS_vDctH2L(void);
void LCTCS_vCompensateGainForH2L(void);
    #endif /* #if __MY08_TCS_DCT_H2L */

void LCTCS_vCalPDGainInTurn(void);
void LCTCS_vCalFinalPDGain(void);
void LCTCS_vPreventLongIntervention(void);
void LCTCS_vSustainCtlHomo2Split(void);
void LCTCS_vDecideGainAddInTurn(void);
void LCTCS_vDecideHomoMuPriority(void);


void LCTCS_vCount1SecAfterETO(void);
  #if __COMPETITIVE_EXIT_CONTROL
void LCTCS_vCountTimeAfterETO(void);
  #endif
	#endif

static void LCTCS_vDecideTorqRecoveryLevel(void);
static void LCTCS_vCalPDGainInTurnFor1stCyc(void);
static void LCTCS_vCalPDGainInTurnFor2ndCyc(void);
static void LCTCS_vCalPDGain1stCycForETCS(void);
static void LCTCS_vCalPDGain1stCycForFTCS(void);
static void LCTCS_vForcedTorqRiseCtrl	(void);
static void LCTCS_vCallTCSCtrlCnt(void);
    
	#if __VARIABLE_MINIMUM_TORQ_LEVEL
static void LCTCS_vDecideMinTorqLvl(void);	
	#endif  /* __VARIABLE_MINIMUM_TORQ_LEVEL */  
#endif      /* #if __TCS || __ETC */

#if __VDC
void LCTCS_vCount3SecAfterETO(void);
void LCTCS_vCount6SecAfterETO(void);
#endif  /* #if __VDC */

/* NEW TCS STRUCTURE*/
void LCTCS_vCalSpin(struct W_STRUCT *WL_temp);					/*060619 New BTCS Structure*/

#if __TCS_VIBRATION_DETECTION
static void LCTCS_vCtlEngineVibration(void);
#endif /*__TCS_VIBRATION_DETECTION*/

int16_t  LCTCS_s16IInter6Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6  );/*051110 New BTCS Structure*/
/* NEW TCS STRUCTURE*/

#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
/* __4WDStuckByDiagonalWheelSpin */
void LCTCS_vDct4WDStuckByDiaWhlSpin(void);
#endif

  #if __TCS_STUCK_ALGO_ENABLE
void LCTCS_vControlStuck(void);
void LCTCS_vDetectVehicleStuck(void);
void LCTCS_vControlTargetDVForStuck(void);
static void LCTCS_vDecTorqInStuck(void);

int16_t	 tcs_stuck_timer;
  #endif
  
static void LCTCS_vCheckCtrlEnableCondition(void);
void LCTCS_vCheckDriversIntention(void);
void LCTCS_vCheckVehicleState(void);
static void LCTCS_vSetHigherSpinWheel(struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);
static void LCTCS_vCalLeftRightWhlSpdDiff(void);
static void LCTCS_vCalcSpinAndRefSpeed(void);


  
#if __TCS_CONTROLLER_UNIFICATION
/* For Fast torque recovery mode */
#define READY_MODE 					0
#define FAST_TORQ_RECOVERY_MODE   	1   
#define CYCLE_2ND_TRANSITION_MODE 	2
/* For Fast torque recovery mode */ 
#endif /* __TCS_CONTROLLER_UNIFICATION */  

#if __CAR == GM_GSUV  
uint16_t U16_TCS_SPEED1;
uint16_t U16_TCS_SPEED2;
uint16_t U16_TCS_SPEED3;
uint16_t U16_TCS_SPEED4;
uint16_t U16_TCS_SPEED5;
  #endif

int16_t lctcss16RawWhlSpdDiffDriven, lctcss16RawWhlSpdDiffNonDriven;
int16_t lctcss16AvgWhlSpdDiffDriven, lctcss16AvgWhlSpdDiffNonDriven;
int16_t	lctcss16WhlSpdreferanceByMiniT;
uint8_t lctcsu8ClearTurnPriorityCnt;
uint8_t lctcsu8TCSEngCtlPhase;
uint8_t lctcsu8TCSEngCtlPhase_old;

uint8_t lctcsu8BTCSHoldResetOkScan;
int16_t lctcss16AxFilterDelta;
uint8_t lctcsu8SplitHillDctCntbyAx;

uint8_t lctcsu8TargetSpinTurnFactor;

#if __TCS_REFER_TARGET_TORQ_CTL
uint8_t  lctcsu8TargetSpinIncCount;
uint8_t  lctcsu8TargetSpinIncTime;
uint8_t	 lctcsu8TargetSpinIncInHomo;
uint8_t	 lctcsu8TargetSpinIncInSplit;
uint8_t	 lctcsu8DeltaTriHomoTargetSpin;
uint8_t	 lctcsu8DeltaTriSplitTargetSpin;

int16_t  delta_yaw_first_rate[3];
int16_t  delta_yaw_first_diff;
uint8_t  lctcsu8AddYawThrbyYawDiff;

int16_t  alat_ems_rate[3];
int16_t  alat_ems_diff;
uint8_t  lctcsu8AddAyThrbySteer;
uint8_t  lctcsu8AddAyThrbyAyDiff;
uint8_t  lctcsu8AddAyThrbyWstrAyDiff;
#endif /*__TCS_REFER_TARGET_TORQ_CTL*/

uint16_t lctcsu16SpinOverThInFast;

/*Refactoring Undefined Signal MBD ETCS*/
uint8_t  lcu8SccMode;
#if !__HSA
U8_BIT_STRUCT_t HSAF0;
#endif /*!__HSA*/

TCS_FLAG_t   lcTcsCtrlFlg0, lcTcsCtrlFlg1, lcTcsCtrlFlg2;


void LCTCS_vFailManagement(void)
{
	lctcsu1BrkCtrlInhibit = 0;
	lctcsu1EngCtrlInhibit = 0;
	lctcsu1InhibitByFM = 0;

#if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
	lcu1TcsCtrlFail = 0;
 
    if((fu1FMWheelFLErrDet==1)||(fu1FMWheelFRErrDet==1)||(fu1FMWheelRLErrDet==1)||(fu1FMWheelRRErrDet==1)||(ee_ece_flg==1) 
      ||(fu1FMWheelFrontErrDet==1)||(fu1FMWheelRearErrDet==1)||(fu1FMSameSideWSSErrDet==1)||(fu1FMDiagonalWSSErrDet==1)  /*Wheel Error*/
	  ||(fu1FMSolenoidErrDet==1) || (fu1FMESCSolErrDet==1)||(fu1FMABSSolErrDet==1)||(fu1FMFrontSolErrDet==1)||(fu1FMRearSolErrDet==1) /*Solenoid Valve Error*/
	  ||(fu1FMMotorErrDet==1)||(fu1FMVRErrDet==1)||(fu1FMECUHwErrDet==1) /*Motor, Valve relay, Micom Fail*/
	  ||(fu1FMVoltageOverErrDet==1)||(fu1FMVoltageUnderErrDet==1)||(fu1FMVoltageLowErrDet==1) /*Motor voltage range error*/
      ||(fu1FMYawErrDet==1)||(fu1FMAyErrDet==1)||(fu1FMStrErrDet==1) /*yaw, ay, steer error*/
      ||(fu1FMMCPErrDet==1)/*MCP*/
      
      #if (__CAR_MAKER==SSANGYONG)
      ||(ccu1TCU_SWI_GS_ErrFlg==1)
      #else
      /*||(fu1FMBLSErrDet==1)   /*BLS error */
      ||(ccu1TQI_TARGET_ErrFlg==1) /*rpm, drive torque error*/            
      #endif
      
      ||(ccu1EMS_N_ErrFlg==1)||(ccu1EMS_TQI_ErrFlg==1)        
      ||(ccu1EMS_TQI_ACOR_ErrFlg==1)||(ccu1EngVar_ErrFlg==1) /*engien torque,engine variation error*/
      ||(fu1FMVCErrDet==1)   /*throttle pedal signal, variant coding error*/                                                 
      ||(ccu1EMS_TPS_ErrFlg==1)    
      ||(ccu1EMS_PV_AV_CAN_ErrFlg==1)
	    ||(ccu1TCU_G_SEL_DISP_ErrFlg==1)
      ||(ccu1TCU_TAR_GC_ErrFlg==1)
      )						
   	{
    	lctcsu1BrkCtrlInhibit = 1;
		lctcsu1EngCtrlInhibit = 1;
		lcu1TcsCtrlFail = 1;
    } 
    else
    {
    	;
    }
    
	   

    if(((TMP_ERROR_fl==1)||(TMP_ERROR_fr==1)||(TMP_ERROR_rl==1)||(TMP_ERROR_rr==1))&&(BTCS_ON==1)) /* 1 scan delay */
    {
    	lcu1TcsDiscTempErrFlg = 1;
    }
    else if (((TMP_ERROR_fl==1)||(TMP_ERROR_fr==1)||(TMP_ERROR_rl==1)||(TMP_ERROR_rr==1))&&(lcu1TcsDiscTempErrFlg == 1)) 
	{
    	lcu1TcsDiscTempErrFlg = 1;
	}
	else
	{
		lcu1TcsDiscTempErrFlg = 0;
	}
	
	
	
#if (__CAR_MAKER==SSANGYONG)
	if((lcu1TcsDiscTempErrFlg==1) ||(fu1FMTCSTempErrDet==1))
	{
		lctcsu1BrkCtrlInhibit = 1;
	}
	else
	{
		;
	}
#else
	if((lcu1TcsDiscTempErrFlg==1) ||(fu1FMTCSTempErrDet==1))
	{
		lctcsu1BrkCtrlInhibit = 1;
		lcu1TcsCtrlFail = 1;
	}
	else
	{
		;
	}
#endif
	    
	#if ( __4WD_VARIANT_CODE == ENABLE)     /*Longitudinal G sensor Error*/
	   
	   if ((lsu8DrvMode ==DM_2WD))
	   {
	     ;
	     /* 2WD */
	   }
	   else
	   {
	     /* 4WD (2H,4H, 4L, AUTO) */
	       if(fu1FMAxErrDet==1)
	       {
	           lctcsu1BrkCtrlInhibit = 1;
		  	   lctcsu1EngCtrlInhibit = 1;
		  	   lcu1TcsCtrlFail = 1; 
	       }  
	       else
	       {
	         ;
	       }  
	     
	   }
	#else
	   #if (__4WD==ENABLE)	   
	   /* 4WD */
	       if(fu1FMAxErrDet==1)
	       {
	           lctcsu1BrkCtrlInhibit = 1;
		       lctcsu1EngCtrlInhibit = 1;
		       lcu1TcsCtrlFail = 1;	 
	       }
	       else
	       {
	         ;
	       }          
	   #else
	     /* 2WD */
	   #endif
	#endif       /*Longitudinal G sensor Error*/
	  
  #else /*NEW_FM_ENABLE*/
    
    if(((TMP_ERROR_fl==1)||(TMP_ERROR_fr==1)||(TMP_ERROR_rl==1)||(TMP_ERROR_rr==1))&&(BTCS_ON==1)) /* 1 scan delay */
    {
        tc_error_flg = 1;
    }
    else if (((TMP_ERROR_fl==1)||(TMP_ERROR_fr==1)||(TMP_ERROR_rl==1)||(TMP_ERROR_rr==1))&&(tc_error_flg == 1)) 
    {
        tc_error_flg = 1;
    }
    else
    {
        tc_error_flg = 0;
    }

  #if __GM_FailM
	if((fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)
	||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||(fu1SameSideWSSErrDet==1)
	||(fu1DiagonalWSSErrDet==1)||(fu1ESCSolErrDet==1)
	||(fu1FrontSolErrDet==1)||(fu1RearSolErrDet==1)||(fu1ECUHwErrDet==1)
	||(fu1MotErrFlg==1)
	||(fu1VoltageLowErrDet==1)||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)
	||(fu8EngineModeStep==0)||(fu8EngineModeStep==1)
	#if __BRAKE_FLUID_LEVEL==ENABLE
	||(fu1DelayedBrakeFluidLevel==1)
	||(fu1LatchedBrakeFluidLevel==1)	/* 090901 for Bench Test */				
	#endif	
	)
	{
    		lctcsu1BrkCtrlInhibit = 1;
    		lctcsu1EngCtrlInhibit = 1;
    		lctcsu1InhibitByFM    = 1;
	}
	else
	{
		;
	}

	if((fu1YawErrorDet==1)||(fu1BLSErrDet==1)||(fu1AyErrorDet==1)
	#if __AX_SENSOR
	||(fu1AxErrorDet==1)
	#endif
	||(fu1MCPErrorDet==1)||(fu1StrErrorDet==1)||(fu1ESCSwitchFail==1)
	||(fu1FWDTimeOutErrDet==1)||(fu1TCUTimeOutErrDet==1))
	{
		;
	}
	else
	{
		;
	}

    if(tc_error_flg==1) /* 1 scan delay */
	{
    		lctcsu1BrkCtrlInhibit = 1;
	}
	else
	{
		;
	}

	if((fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1))
	{
    		lctcsu1EngCtrlInhibit = 1;
	}
	else
	{
		;
	}

	if((fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1))
	{
    	lctcsu1EngInformInvalid = 1;
	}
	else
	{
		lctcsu1EngInformInvalid = 0;
	}  
	
  #else /* __GM_FailM */
    
  /* ---- Check Inhibition -------------- */
    
    if((tcs_error_flg==1) || (tc_error_flg==1) || (fu1VoltageUnderErrDet==1)) /* temp error : 1 scan delay */
    {
        lctcsu1BrkCtrlInhibit = 1;
    }
    
    if((tcs_error_flg==1) || (fu1VoltageUnderErrDet==1))
    {
        lctcsu1EngCtrlInhibit = 1;
    }
    
  #endif /* __GM_FailM */
  
#endif /*NEW_FM_ENABLE*/

  	if ((wu8IgnStat==CE_OFF_STATE)||(fu1OnDiag==1)||(init_end_flg==0))
    {
        lctcsu1EngCtrlInhibit = 1;
        lctcsu1BrkCtrlInhibit = 1;
    }

}


static void LCTCS_vCheckCtrlEnableCondition(void)
{
/* --- Check TCS enable ---------------- */

  #if   (__RWD_SPORTSMODE==SPORTMODE_1) /*(__SPORTSMODE==1)*/

    if(fu1ESCDisabledBySW==1)
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    }
    else if(fu1TCSDisabledBySW==1)
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    }
    else
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
    }
    
  #elif (__RWD_SPORTSMODE==SPORTMODE_2) /*(__SPORTSMODE==2)*/
	if(fu1TCSDisabledBySW == 0)
	{
		lctcsu1AllowedEngSportsMode = 0;
	}
	else
	{
		if(lctcsu1AllowedEngSportsMode == 0)
		{
			if((vref5 > VREF_40_KPH)&&(det_alatm>LAT_0G6G))			 
			{
				lctcsu1AllowedEngSportsMode = 1;
			}
		}
		else
		{
			if(vref5 <= VREF_35_KPH)
			{
				lctcsu1AllowedEngSportsMode = 0;
			}
		}
	}
	
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;

	if(fu1TCSDisabledBySW == 0)
    {
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
    }
    else
    {
		lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		if(lctcsu1AllowedEngSportsMode == 1)
        {
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        }
        else
        {
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        }
    }

  #elif   (__RWD_SPORTSMODE==SPORTMODE_3) /*(__SPORTSMODE==3)*/

	if(fu1TCSDisabledBySW == 0)
	{
		lctcsu1AllowedBrkSportsMode = 0;
	}
	else
	{
		if(vref5 > VREF_60_KPH)
		{
			lctcsu1AllowedBrkSportsMode = 0;
		}
		else
		{
			lctcsu1AllowedBrkSportsMode = 1;
		}
	}

	if(fu1ESCDisabledBySW==1)
	{
		#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) || (__REAR_D == 0)
			lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		#else
		if(lctcsu1AllowedBrkSportsMode == 1)
		{
			lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
		}
		else
		{
			lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		}
		#endif

		lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;

	}
	else if(fu1TCSDisabledBySW==1)
	{
		lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
		lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
	}
	else
	{
		lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
		lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
		lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
	}

  #elif (__TCS_DRIVELINE_PROTECT==1)

	#if __TCS_COMPETITIVE_CONTROL
	if(fu1ESCDisabledBySW==1)
	{
	  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
		if(lctcsu1DrvMode4Wheel==1)
		{	/* 4WD */
            lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_SP;
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DP;
            lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		}
		else
		{
            lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_SP;
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
            lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        }
      #else   
      lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_SP;
      lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
      lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
      #endif   
    }
    else
	{
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
	}
	#else		/* #if __TCS_COMPETITIVE_CONTROL */ 
	if((fu1TCSDisabledBySW==1)
		#if __RDCM_PROTECTION
		&&(ltu1RDCMProtectionReq==0)
		#endif
		)
	{
	  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
		if(lctcsu1DrvMode4Wheel==1)
		{	/* 4WD */
            lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DP;
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DP;
		}
		else
		{
            lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DP;
            lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
		}
	  #else
	    
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DP;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
      
      #endif
        if(fu1ESCDisabledBySW==1)
        {
        	lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        }
        else
        {
        	lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
        }
	}
	else
	{
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
	}
	#endif		/* #if __TCS_COMPETITIVE_CONTROL */  	

  #elif (__CAR_MAKER==SSANGYONG)
    
    if(fu1TCSDisabledBySW==0)
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
    }
    else
    {
       	
			if(vref5 >= VREF_40_KPH)			 
			{
				lctcsu1InhibitBrkCtrl4SYC = 1;
			}
			else
			{
				lctcsu1InhibitBrkCtrl4SYC = 0;
			}
		
       
        if(lctcsu1InhibitBrkCtrl4SYC == 0)
        {
        	lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1; 
        }
        else
        {
        	lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        }
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    	
    }  
  
  #else
  
    if((fu1ESCDisabledBySW==0) && (fu1TCSDisabledBySW==0))
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_1;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_1;
    }
    else
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    }

#endif

    if(lctcsu1BrkCtrlInhibit==1)
    {
        lctcss16BrkCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    }
    else
    {
        ;
    }    
    
    if(lctcsu1EngCtrlInhibit==1)
    {
        lctcss16EngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
        lctcss16EECEngCtrlMode = S16_TCS_CTRL_MODE_DISABLE;
    }
    else
    {
        ;
    }
}

#if __BTC   /* TAG1 */
void LCTCS_vCallControl(void)
{
	LCTCS_vFailManagement();
	
	LCTCS_vCheckVehicleState();
	LCTCS_vCheckCtrlEnableCondition();
	LCTCS_vCheckDriversIntention();
	
	LCTCS_vCalcSpinAndRefSpeed();

	LCTCS_vBrakeControl(); /* BTCS_TCMF_CONTROL */
    
    LDTCS_vEstimateBTCSDiskTemp();


  #if __TCS
    BTCS_ON = BTC_fz;
    ABS_ON  = ABS_fz;

  #if __TCS_DRIVELINE_PROTECT
    if ( (BTCS_fl==1) || (BTCS_fr==1) || (BTCS_rl==1) || (BTCS_rr==1) )
    {
    	lctcsu1DrivelineProtectionOn = 1;
    }
	else
	{
    	lctcsu1DrivelineProtectionOn = 0;		
	}
  #endif	/* #if __TCS_DRIVELINE_PROTECT */

    #if !SIM_MATLAB
    /*LATCS_vConvertEMS2TCS();*/
    #endif

    LCTCS_vSignalProcessing();

    LCTCS_vEstimateTraction();
  #if !__VDC
    LCTCS_vEstimateTrace();
  #endif	/* #if !__VDC */    

    //LCTCS_vCallDetectTraction();	/* MBD ETCS */
  #if !__VDC    
    LCTCS_vCallDetectTrace();
  #endif	/* #if !__VDC */    
    //LCTCS_vControlTraction();		/* MBD ETCS */
  #if !__VDC
    LCTCS_vControlTrace();
  #endif	/* #if !__VDC */

    LCTCS_vControlCommand();
  #endif    /* #if __TCS */
  
    /* MBD ETCS */
//    if (system_loop_counter%2==0)
//    {
    if (LOOP_TIME_20MS_TASK_0==1)
    {	
    	LCETCS_vCallMain_step();
    }
    cal_torq = ETCS_ENG_CMD.lcetcss16EngTrqCmd;
    ETCS_ON = ETCS_CTL_ACT.lcetcsu1CtlAct;       
    lctcsu1DctSplitHillFlg = ETCS_SPLIT_HILL.lcetcsu1SplitHillDct;
}

	
void LCTCS_vCheckDriversIntention(void)
  {
  #if (__TCS)      
	if ((mtp>(int16_t)S8_COMPLETE_CLOSE_THROTTLE_POS) || (lespu1SccActFlg==1))
    {
    	lctcsu1DriverAccelIntend = 1;
    }
    else
    {
	    lctcsu1DriverAccelIntend = 0;
    }
    #endif

#if __IDB_RIG_TEST
	if(vref>VREF_1_KPH)
  {
		lctcsu1DriverAccelIntend = 1;
  }
  #endif
  
  #if __VDC
  #if ((__AHB_GEN3_SYSTEM == ENABLE)||(__IDB_LOGIC==ENABLE))
    if( lsespu1AHBGEN3MpresBrkOn==1 )
	  #else
    if( (fu1MCPErrorDet==0) && (MPRESS_BRAKE_ON==1) )
	  #endif
    {
	  #if ((__AHB_GEN3_SYSTEM == ENABLE)||(__IDB_LOGIC==ENABLE))
	    if( (lsesps16AHBGEN3mpress>=MPRESS_14BAR) || ((fu1BLSErrDet==0) && (fu1BlsSusDet==0) && (BLS==1)) ) /* && (BLS_EEPROM==0)*/
	 #else
	    if( (mpress>=MPRESS_14BAR)                        || ((fu1BLSErrDet==0) && (fu1BlsSusDet==0) && (BLS==1)) ) /* && (BLS_EEPROM==0)*/
  #endif
    {
	    	lctcsu1DriverBrakeIntend = 1;
	    	
    }
    else
    {
	    	lctcsu1DriverBrakeIntend = 0;
	    	
	}
    }
    else
    {
	    	lctcsu1DriverBrakeIntend = 0;
	    }
#else
    if( (fu1BLSErrDet==0) && (fu1BlsSusDet==0) && (BLS==1)) /* && (BLS_EEPROM==0)*/ 
	    {
        lctcsu1DriverBrakeIntend = 1;
	    }
	    else
	    {
        lctcsu1DriverBrakeIntend = 0;
	    }
#endif	

    if( (fu1BLSErrDet==1) || (fu1BlsSusDet==1) /* && (BLS_EEPROM==0)*/ 
      #if __VDC
		#if __BRK_SIG_MPS_TO_PEDAL_SEN
	    || (lsespu1BrkAppSenInvalid==1)
		#else
	    || (fu1MCPErrorDet==1) 
		#endif
	  #endif
	)
	    {
    	lctcsu1DriverBrakeIntendInvalid = 1;
	    }
	    else
	    {
    	lctcsu1DriverBrakeIntendInvalid = 0;
	    }

    /* ESP_ERROR_FLG is replaced by lsespu1BrkAppSenInvalid and fu1MCPErrorDet
	if( (fu1BLSErrDet==1) || (BLS_EEPROM==1) || (BLS==1) 
      #if __VDC
		#if __BRK_SIG_MPS_TO_PEDAL_SEN
	    || (lsespu1BrkAppSenInvalid==1) || ((lsespu1BrkAppSenInvalid==0)&&(lsespu1PedalBrakeOn==1))
		#else
	    || (fu1MCPErrorDet==1) || (MPRESS_BRAKE_ON==1) 
		#endif
	  #endif
	)
	    {
		lctcsu1DriverBrkIntendOrBrkErr = 1;
	    }
	    else
	    {
		lctcsu1DriverBrkIntendOrBrkErr = 0;
	}
    */
	        }
void LCTCS_vCheckVehicleState(void)
	        {
	if((Rough_road_suspect_vehicle==0)&&(Rough_road_detect_vehicle==0))
	    {
		lctcsu1RoughRoad = 0;
	    }
	    else
	    {
		lctcsu1RoughRoad = 1;
}

	#if __RTA_ENABLE
    if ( (RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_WL==1)||
         (RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_WL==1) 
          #if __RTA_2WHEEL_DCT 
          || ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_FR.RTA_SUSPECT_2WHL==1))
	      || ((RTA_RL.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))
	      || ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_RL.RTA_SUSPECT_2WHL==1))
	      || ((RTA_FR.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))		      
	      || ((RTA_FL.RTA_SUSPECT_2WHL==1)&&(RTA_RR.RTA_SUSPECT_2WHL==1))		      		      
	      || ((RTA_FR.RTA_SUSPECT_2WHL==1)&&(RTA_RL.RTA_SUSPECT_2WHL==1))	
 	      #endif	          
       )  
	#else
    if ( (FL.MINI_SPARE_WHEEL==1)||(FR.MINI_SPARE_WHEEL==1)||
         (RL.MINI_SPARE_WHEEL==1)||(RR.MINI_SPARE_WHEEL==1) )
	#endif			
    	{
		lctcss16WhlSpdreferanceByMiniT = ( vref5 / 50 );		/*  RTA confidence factor before control detection */
		lctcsu1WhlSpdCompByRTADetect = 1;
		lctcsu1InvalidWhlSpdBfRTADetect = 0;
	}
	else if ((TCS_MINI_TIRE_SUSPECT==1)||
		    (RTA_FL.RTA_SUSPECT_WL_FOR_BTCS==1)||(RTA_FR.RTA_SUSPECT_WL_FOR_BTCS==1)||
		    (RTA_RL.RTA_SUSPECT_WL_FOR_BTCS==1)||(RTA_RR.RTA_SUSPECT_WL_FOR_BTCS==1))
	{
		lctcss16WhlSpdreferanceByMiniT = ( vref5 * 3 ) / 10;	/* enter delay during control */
		lctcsu1WhlSpdCompByRTADetect = 0;
		lctcsu1InvalidWhlSpdBfRTADetect = 1;
	}
	else
	{
		lctcsu1WhlSpdCompByRTADetect = 0;
		lctcsu1InvalidWhlSpdBfRTADetect = 0;
		lctcss16WhlSpdreferanceByMiniT = 0;
}

	#if (__4WD_VARIANT_CODE==ENABLE)
	if ((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H))
	{
		lctcsu1DrvMode4Wheel = 1;
		lctcsu1DrvMode2Wheel = 0;
	}
	else
	{
		lctcsu1DrvMode4Wheel = 0;
		lctcsu1DrvMode2Wheel = 1;
	}
	#elif (__4WD==1)
	lctcsu1DrvMode4Wheel = 1;
	lctcsu1DrvMode2Wheel = 0;
	#else /*2WD*/
	lctcsu1DrvMode4Wheel = 0;
	lctcsu1DrvMode2Wheel = 1;
	#endif
}

static void LCTCS_vSetHigherSpinWheel(struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)
	{
    if(WL_LEFT->vrad_crt > WL_RIGHT->vrad_crt)
	{
		WL_LEFT->lctcsu1HigherRawSpdWhl = 1;
		WL_RIGHT->lctcsu1HigherRawSpdWhl = 0;
	}
	else if(WL_LEFT->vrad_crt < WL_RIGHT->vrad_crt)
	{
		WL_LEFT->lctcsu1HigherRawSpdWhl = 0;
		WL_RIGHT->lctcsu1HigherRawSpdWhl = 1;
	}
	else
	{
		WL_LEFT->lctcsu1HigherRawSpdWhl = 0;
		WL_RIGHT->lctcsu1HigherRawSpdWhl = 1;
	}

    if(WL_LEFT->ltcss16MovingAveragedWhlSpd > WL_RIGHT->ltcss16MovingAveragedWhlSpd)
	{
		WL_LEFT->lctcsu1HigherAvgSpdWhl = 1;
		WL_RIGHT->lctcsu1HigherAvgSpdWhl = 0;
	}
	else if(WL_LEFT->ltcss16MovingAveragedWhlSpd < WL_RIGHT->ltcss16MovingAveragedWhlSpd)
	{
		WL_LEFT->lctcsu1HigherAvgSpdWhl = 0;
		WL_RIGHT->lctcsu1HigherAvgSpdWhl = 1;
	}
	else
	{
		WL_LEFT->lctcsu1HigherAvgSpdWhl = 0;
		WL_RIGHT->lctcsu1HigherAvgSpdWhl = 1;
	}
}

static void LCTCS_vCalLeftRightWhlSpdDiff(void)
	{
  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) || (__REAR_D==1)
    LCTCS_vSetHigherSpinWheel(&RL, &RR);
  #endif
  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) || (__REAR_D==0)
    LCTCS_vSetHigherSpinWheel(&FL, &FR);
  #endif
    
  #if __REAR_D
    lctcss16RawWhlSpdDiffDriven    = (int16_t)McrAbs(vrad_crt_rl - vrad_crt_rr);
    lctcss16RawWhlSpdDiffNonDriven = (int16_t)McrAbs(vrad_crt_fl - vrad_crt_fr);
    lctcss16AvgWhlSpdDiffDriven    = (int16_t)McrAbs(ltcss16MovingAveragedWhlSpd_rl - ltcss16MovingAveragedWhlSpd_rr);
    lctcss16AvgWhlSpdDiffNonDriven = (int16_t)McrAbs(ltcss16MovingAveragedWhlSpd_fl - ltcss16MovingAveragedWhlSpd_fr);
  #else
    lctcss16RawWhlSpdDiffDriven    = (int16_t)McrAbs(vrad_crt_fl - vrad_crt_fr);
    lctcss16RawWhlSpdDiffNonDriven = (int16_t)McrAbs(vrad_crt_rl - vrad_crt_rr);
    lctcss16AvgWhlSpdDiffDriven    = (int16_t)McrAbs(ltcss16MovingAveragedWhlSpd_fl - ltcss16MovingAveragedWhlSpd_fr);
    lctcss16AvgWhlSpdDiffNonDriven = (int16_t)McrAbs(ltcss16MovingAveragedWhlSpd_rl - ltcss16MovingAveragedWhlSpd_rr);
  #endif	
	   }

static void LCTCS_vCalcSpinAndRefSpeed(void)
	   {
	
	LCTCS_vCalLeftRightWhlSpdDiff();	
	
	LCTCS_vCalSpin(&FL);
	LCTCS_vCalSpin(&FR);
	LCTCS_vCalSpin(&RL);
	LCTCS_vCalSpin(&RR);	
	
	
  #if __TCS || __ETC	
    LCTCS_vCalTractionWheelSpeed();

    LCTCS_vCalFilteredWheelSpeed();

	LCTCS_vCalculateSpin();
	
    LCTCS_vCalculateACC();

    #if __TCS_DRIVELINE_PROTECT	
	LCTCS_vCalVariable4DP();    
    #endif	/* #if __TCS_DRIVELINE_PROTECT */
  #endif  
  
	}


#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)


void LCTCS_vCalMaxSpin4WD(void)
{
	if(vrad_crt_fl>vrad_crt_fr)
	{
    	tempW0=vrad_crt_fl;
    	/*tempW2=vrad_crt_fr;*/
	}
	else
	{
    	tempW0=vrad_crt_fr;
    	/*tempW2=vrad_crt_fl;*/
}
    if(vrad_crt_rl>vrad_crt_rr)
	    {
    	tempW1=vrad_crt_rl;
    	/*tempW3=vrad_crt_rr;*/
	    }
	    else
	    {
    	tempW1=vrad_crt_rr;
    	/*tempW3=vrad_crt_rl;*/
	}
    if(tempW0>=tempW1)
	{
    	vel_f_big=tempW0;
    	/*vel_f_small=tempW2;*/
	}
	else
	{
    	vel_f_big=tempW1;
    	/*vel_f_small=tempW3;*/
	}
}


#endif	/* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) */




int16_t     LCTCS_s16IInter6Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6  )
            {
    if( input_x <= x_1 )
    {
        inter_tempW9 = y_1;
            }
    else if ( input_x < x_2 )
    {
    /*  tempW9 =  y_1 + (int16_t)((((long)(y_2-y_1))*(input_x-x_1))/(x_2-x_1)); */
        inter_tempW2 = (y_2-y_1);
        inter_tempW1 = (input_x-x_1);
        inter_tempW0 = (x_2-x_1);
        if ( inter_tempW0==0 )
            {
            inter_tempW0=1;
        }
        else
        {
            ;
        }
        inter_tempW3 = (inter_tempW2*inter_tempW1)/inter_tempW0;
    /*  s16muls16divs16(); */
        inter_tempW9 = y_1 + inter_tempW3;
    }
    else if ( input_x < x_3 )
            {
    /*  tempW9 =  y_2 + (int16_t)((((long)(y_3-y_2))*(input_x-x_2))/(x_3-x_2)); */
        inter_tempW2 = (y_3-y_2);
        inter_tempW1 = (input_x-x_2);
        inter_tempW0 = (x_3-x_2);
        if ( inter_tempW0==0 )
                {
            inter_tempW0=1;
                }
                else
                {
                    ;
                }
        inter_tempW3 = (inter_tempW2*inter_tempW1)/inter_tempW0;
    /*  s16muls16divs16(); */
        inter_tempW9 = y_2 + inter_tempW3;
            }
    else if ( input_x < x_4 )
            {
    /*  tempW9 =  y_3 + (int16_t)((((long)(y_4-y_3))*(input_x-x_3))/(x_4-x_3)); */
        inter_tempW2 = (y_4-y_3);
        inter_tempW1 = (input_x-x_3);
        inter_tempW0 = (x_4-x_3);
        if ( inter_tempW0==0 )
            {
            inter_tempW0=1;
        }
        else
        {
            ;
        }
        inter_tempW3 = (inter_tempW2*inter_tempW1)/inter_tempW0;
    /*  s16muls16divs16(); */
        inter_tempW9 = y_3 + inter_tempW3;
    }
    else if ( input_x < x_5 )
    {
    /*  tempW9 =  y_4 + (int16_t)((((long)(y_5-y_4))*(input_x-x_4))/(x_5-x_4)); */
        inter_tempW2 = (y_5-y_4);
        inter_tempW1 = (input_x-x_4);
        inter_tempW0 = (x_5-x_4);
        if ( inter_tempW0==0 )
        {
            inter_tempW0=1;
        }
        else
        {
    	    ;
        }
        inter_tempW3 = (inter_tempW2*inter_tempW1)/inter_tempW0;
    /*  s16muls16divs16(); */
        inter_tempW9 = y_4 + inter_tempW3;
    }
    else if ( input_x < x_6 )
    {
    /*  tempW9 =  y_4 + (int16_t)((((long)(y_5-y_4))*(input_x-x_4))/(x_5-x_4)); */
        inter_tempW2 = (y_6-y_5);
        inter_tempW1 = (input_x-x_5);
        inter_tempW0 = (x_6-x_5);
        if ( inter_tempW0==0 )
        {
            inter_tempW0=1;
        }
        else
        {
            ;
        }
        inter_tempW3 = (inter_tempW2*inter_tempW1)/inter_tempW0;
    /*  s16muls16divs16(); */
        inter_tempW9 = y_5 + inter_tempW3;
    }
    else
    {
        inter_tempW9 = y_6;
}

    return inter_tempW9;
}

void LCTCS_vCalVelr(void)
{
  #if (__4WD_VARIANT_CODE==ENABLE)
    if (lctcsu1DrvMode4Wheel==1)
	{
    	LCTCS_vCalVelrFor4WD();
	}
	else
	{
    	LCTCS_vCalVelrFor2WD();
	}
  #elif (__4WD==1)
	LCTCS_vCalVelrFor4WD();
  #else
    LCTCS_vCalVelrFor2WD();
  #endif
}
#endif  /* TAG1 */  

static void LCTCS_vCalVelrFor2WD(void)
    {
    #if __REAR_D
    if(vrad_crt_fl>=vrad_crt_fr)
        {
        tempW4 = vrad_crt_fl;
        tempW5 = vrad_crt_fr;
        }
        else
        {
        tempW4 = vrad_crt_fr;
        tempW5 = vrad_crt_fl;
        }

        if(tempW4 > tempW5 + VREF_6_KPH)
        {
            tempW6 = tempW4 - VREF_3_KPH;
    }
    else
    {
            tempW6 = (tempW4+tempW5)/2;
    }

    #else
    if(vrad_crt_rl>=vrad_crt_rr)
                {
        tempW4 = vrad_crt_rl;
        tempW5 = vrad_crt_rr;
                }
                else
                {
        tempW4 = vrad_crt_rr;
        tempW5 = vrad_crt_rl;
                }

        if(tempW4 > tempW5 + VREF_6_KPH)
        {
            tempW6 = tempW4 - VREF_3_KPH;
            }
            else
            {
            tempW6 = (tempW4+tempW5)/2;
            }

    #endif
	if (speed_calc_timer < L_U8_TIME_10MSLOOP_1500MS)
        {
		vel_r=tempW6;	/* Ign on 이후 1초간은 vel_r 계산시 Filtering 하지 않음 */
		                /* 주행 중 Ign Off -> On시 TCS 이상 제어 진입 방지		*/
        }
        else
        {
        vel_r=(int16_t)((((int32_t)tempW6*L_U8FILTER_GAIN_10MSLOOP_7_5HZ)+((int32_t)vel_r*(128-L_U8FILTER_GAIN_10MSLOOP_7_5HZ)))/128); 
                }
}

static void LCTCS_vCalVelrFor4WD(void)	  
            {
	if (speed_calc_timer < L_U8_TIME_10MSLOOP_1500MS)
                {
		vel_r=vref;		/* Ign on 이후 1초간은 vel_r 계산시 Filtering 하지 않음 */
		                /* 주행 중 Ign Off -> On시 TCS 이상 제어 진입 방지		*/
                }
                else
                {
        tempW6=vref;                                            /* NSM03*/
        vel_r=(int16_t)((((int32_t)tempW6*L_U8FILTER_GAIN_10MSLOOP_7_5HZ)+((int32_t)vel_r*(128-L_U8FILTER_GAIN_10MSLOOP_7_5HZ)))/128); 
                }
            }

#if __TCS || __ETC      /* TAG2 */
void LCTCS_vSignalProcessing(void)
            {
#if __VDC
	LCTCS_vCount6SecAfterETO();
    LCTCS_vCount3SecAfterETO();
    LCTCS_vCount1SecAfterETO();
    #if __COMPETITIVE_EXIT_CONTROL
    LCTCS_vCountTimeAfterETO();
    #endif
	
	LCTCS_vEstMuRefCoordinator();    

#endif
    }

void LCTCS_vEstimateTraction(void)
    {
	ENG_STALL_OLD = ENG_STALL;

  #if __VARIABLE_MINIMUM_TORQ_LEVEL			
	if( LOOP_TIME_20MS_TASK_1 == 1 )	/* Cycle time opt. */
{
	LCTCS_vDecideMinTorqLvl();
    }
    else{}		
  #endif

	if ( (U8_TCS_ENABLE_SPLT_HILL_DCT == 1) && (FTCS_ON == 1) )
    {
		LCTCS_vDctSplitHillTendency();
    }
	else if (ldtcsu1HillDetectionByAx == 1)
	{
		lctcsu1DctSplitHillFlg = 1;	
	}
    else
    {
		lctcsu1DctSplitHillFlg = 0;
		lctcss16SplitHillDctCnt = 0;
		#if __AX_SENSOR
		lctcsu8SplitHillDctCntbyAx = 0;
		#endif
    }
	
    /*LCTCS_vCalculateVelocity(); */
    LCTCS_vEstmateTMGear();
    LCTCS_vDetectMiniWheelTendency();
    /*LCTCS_vCalculateSpin();*/
    
    LCTCS_vCalculateTargetSpin();
    #if __VDC
    LCTCS_vCalTargetSpinInTurn();
    #endif     	
    		
    #if __VDC    
	LCTCS_vDetectFadeModeAfterETO();
    #endif

    LCTCS_vDctTargetGoodTracking();
    LCTCS_vIncTargetSpin();

	#if __TCS_DRIVELINE_PROTECT
	LCTCS_vCalTargetDVForDP();
	#endif    
    
    #if __MY08_TCS_DCT_H2L
    LCTCS_vDctH2L();
    #endif /* #if __MY08_TCS_DCT_H2L */ 
    
  #if __TCS_STUCK_ALGO_ENABLE
  	LCTCS_vControlStuck();
  #endif
	
    LCTCS_vCalculateError();
    
  #if __TCS_PREVENT_FAST_RPM_INCREASE
	LCTCS_vDctFastRPMIncrease();
  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */
  
  #if TORQ_RECOVERY_DEPEND_ON_STEER
  	LCTCS_vDctSteerToZero();
  #endif	/* #if TORQ_RECOVERY_DEPEND_ON_STEER */    
    }
 
  #if __TCS_PREVENT_FAST_RPM_INCREASE
static void LCTCS_vDctFastRPMIncrease(void)
    {
	if 			( (vref > VREF_8_KPH) && 
				  (slope_eng_rpm > (int16_t)U8_TCS_SET_FAST_RPM_INC_TH) )	/* 18rpm/scan */
	{	/* 속도 조건 : M/T 차량 정차 중 Hard Launch때 토크 과다 저감 방지 */
		lctcsu1FastRPMInc = 1;
    }
	else if 	(slope_eng_rpm < (int16_t)U8_TCS_CLR_FAST_RPM_INC_TH)	/* 10rpm/scan */
    {
		lctcsu1FastRPMInc = 0;
    }
    else
    {
		;
    }
}
  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */

  #if __TCS_STUCK_ALGO_ENABLE
void LCTCS_vControlStuck(void)
    {
	LCTCS_vDetectVehicleStuck();
	LCTCS_vControlTargetDVForStuck();
    }

void LCTCS_vDetectVehicleStuck(void)
    {
    	#if __NEW_STUCK_ALGO	
	if(ETCS_ON == 1)
    {
		tcs_tempW0 = (int16_t)U16_STUCK_DETECT_SCAN;
    }
    else
    {
		tcs_tempW0 = (int16_t)U16_STUCK_DETECT_SCAN_SP;
    }
		#endif /* __NEW_STUCK_ALGO */					
	if(TCS_STUCK_ON==0)
    {
        if(lctcsu1DriverBrakeIntend==1) 
    {
			tcs_stuck_timer=0;
}
		else if((ETCS_ON==1)||(FTCS_ON==1))
{
			if(vref5<(int16_t)U16_STUCK_DETECT_SPEED)
	{
					#if __NEW_STUCK_ALGO
				if(tcs_stuck_timer < tcs_tempW0)
	    {
					tcs_stuck_timer=tcs_stuck_timer+1;
	    }
    else
	    {
					TCS_STUCK_ON=1;
    }
					#else	/* __NEW_STUCK_ALGO */		
				if(tcs_stuck_timer<(int16_t)U16_STUCK_DETECT_SCAN)
	        {
					if(ETCS_ON==1)
	            {
						tcs_stuck_timer=tcs_stuck_timer+2;
	            }
					else
	            {
						tcs_stuck_timer=tcs_stuck_timer+1;
	            }
	            }
	            else
	            {
					TCS_STUCK_ON=1;
	            }
					#endif  /* __NEW_STUCK_ALGO */
	        }
	        else
	        {
				tcs_stuck_timer=0;
	        }
	    }
	    else
	    {
			tcs_stuck_timer=0;
	    }
	}
	else
	{
		if((ETCS_ON==0)&&(FTCS_ON==0))
	    {
			TCS_STUCK_ON=0;
	    }
		else if(vref5>=(int16_t)U16_STUCK_EXIT_SPEED)
	            {
			TCS_STUCK_ON=0;
	            }
		else if(lctcsu1DriverBrakeIntend==1)
	            {
			TCS_STUCK_ON=0;
	            }
	            else
	            {
	                ;
	            }

		tcs_stuck_timer=0;
	        }
	        }

void LCTCS_vControlTargetDVForStuck(void)
{
	if(TCS_STUCK_ON==1)
	{
			#if __NEW_STUCK_ALGO
		if(ETCS_ON == 1)
		{		
			tempW0 = lts16TargetWheelSpin + (int16_t)U16_STUCK_TARGET_DV_COMP;			
	    }
	    else
	    {
			tempW0 = lts16TargetWheelSpin + (int16_t)U16_STUCK_TARGET_DV_COMP_SP;		
	    }
			#else
		tempW0 = lts16TargetWheelSpin + (int16_t)U16_STUCK_TARGET_DV_COMP;
			#endif

		tempW1 = LCTCS_s16ILimitMinimum(tempW0,(int16_t)U16_STUCK_TARGET_DV_MIN);
		tempW2 = LCTCS_s16ILimitMaximum(tempW1,(int16_t)U16_STUCK_TARGET_DV_MAX);

		lts16TargetWheelSpin = tempW2;
	}
	else
	{
	    ;
	}
}

static void LCTCS_vDecTorqInStuck(void)
{
	if(TCS_STUCK_ON==1)
    {
    	if(tcs_cmd<=cal_torq)
        {
    		if(tcs_cmd>eng_torq)
            {
    			tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd,eng_torq-(cal_torq-tcs_cmd));
            }
            else
            {
                ;
            }
				#if __NEW_STUCK_ALGO
			if(ETCS_ON == 1)
        {
				tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd,(int16_t)U16_STUCK_TORQ_LIMIT);
    }
    else
    {
				tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd,(int16_t)U16_STUCK_TORQ_LIMIT_SP);		
    }
				#else    		
  			tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd,(int16_t)U16_STUCK_TORQ_LIMIT);   
  				#endif 		
}
    	else
        {
				#if __NEW_STUCK_ALGO
			if(ETCS_ON == 1)
            {
				tcs_cmd = LCTCS_s16IFindMinimum(tcs_cmd,(cal_torq+(int16_t)U8_STUCK_TORQ_RISE_MAX));
            }
            else
            {
				tcs_cmd = LCTCS_s16IFindMinimum(tcs_cmd,(cal_torq+(int16_t)U8_STUCK_TORQ_RISE_MAX_SP));		
			}	
				#else    		
  			tcs_cmd = LCTCS_s16IFindMinimum(tcs_cmd,(cal_torq+(int16_t)U8_STUCK_TORQ_RISE_MAX));  
  				#endif     		
            }
        }
        else
        {
            ;
        }
    }
  #endif

#if __TCS_DRIVELINE_PROTECT
void LCTCS_vDetDrivelineProtection(void)
    {
  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
	if(lctcsu1DrvMode4Wheel==1)
        {
		if (lctcsu1DriverAccelIntend==1)
            {
			if (ltcss16DeltaSpdOfFrontAndRear>(int16_t)U16_DP_FRONT_REAR_DV_THR)
                {
				ETCS_ON=1;
				CYCLE_2ND=1;        		
                }
                else
                {
                    ;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
  #else		/* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) */
  /* 2WD 의 경우는 Driveline Protection 기능 작동시 Engine 제어 금지 */
  #endif	/* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) */		
    }

void LCTCS_vCalVariable4DP(void)
        {
	if ( (lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DP) || (lctcss16BrkCtrlMode == S16_TCS_CTRL_MODE_DP) )
            {
		ltcss16FrontAxleMeanSpd = ((int16_t)ltcss16MovingAveragedWhlSpd_fl + (int16_t)ltcss16MovingAveragedWhlSpd_fr) / 2;
		ltcss16RearAxleMeanSpd = ((int16_t)ltcss16MovingAveragedWhlSpd_rl + (int16_t)ltcss16MovingAveragedWhlSpd_rr) / 2;
	  #if __REAR_D
		ltcss16DeltaSpdOfFrontAndRear = ltcss16RearAxleMeanSpd - ltcss16FrontAxleMeanSpd;
	  #else		/* #if __REAR_D */
		ltcss16DeltaSpdOfFrontAndRear = ltcss16FrontAxleMeanSpd - ltcss16RearAxleMeanSpd;
	  #endif	/* #if __REAR_D */
	    ltcss16DeltaSpdOfFrontAndRear = LCTCS_s16ILimitMinimum(ltcss16DeltaSpdOfFrontAndRear, 0);
		ltcss16DeltaSpdOfFLandFR = (int16_t)ltcss16MovingAveragedWhlSpd_fl - (int16_t)ltcss16MovingAveragedWhlSpd_fr;
		ltcss16DeltaSpdOfRLandRR = (int16_t)ltcss16MovingAveragedWhlSpd_rl - (int16_t)ltcss16MovingAveragedWhlSpd_rr;
            }
            else
            {
		ltcss16FrontAxleMeanSpd = 0;
		ltcss16RearAxleMeanSpd = 0;
		ltcss16DeltaSpdOfFrontAndRear = 0;
		ltcss16DeltaSpdOfFLandFR = 0;
		ltcss16DeltaSpdOfRLandRR = 0;
            }
        }

void LCTCS_vCalTargetDVForDP(void)
        {
	if (lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DP)
        {
		tempW5 = LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, (int16_t)U16_DP_COMP_TARGET_DV_HOMO_S1,
									   	    (int16_t)U16_TCS_SPEED2, (int16_t)U16_DP_COMP_TARGET_DV_HOMO_S2,
						 				    (int16_t)U16_TCS_SPEED3, (int16_t)U16_DP_COMP_TARGET_DV_HOMO_S3,
										    (int16_t)U16_TCS_SPEED4, (int16_t)U16_DP_COMP_TARGET_DV_HOMO_S4);
	
		lts16TargetWheelSpin = tempW5;
		SplitTransitionCnt = 0;
        }
        else
        {
            ;
        }
    }

static void LCTCS_vDecMinTorqInDP(void)
            {
	if ((lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DP)&&(ETCS_ON==1))
            {
  		tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd,(int16_t)U16_DP_COMP_TORQ_LIMIT);
        }
        else
        {
            ;
        }
    }


#endif		/* #if __TCS_DRIVELINE_PROTECT */

#if !__VDC
void LCTCS_vEstimateTrace(void)
    {
    LCTCS_vFindAyThreshold();
    LCTCS_vCalculateAY();
    LCTCS_vCalculateAYF();
    }

void LCTCS_vCalculateAY(void)
    {

    int8_t i;

#if __REAR_D
    vmini_fl=vrad_fl;                                       /* GK17*/
    vmini_fr=vrad_fr;

    if(DETECT_MINI_TCS==1)
        {
        if(MINI_REAR==0)
            {
            if(MINI_LEFT==1)
                {
                /* vmini_fl=(int16_t)((int32_t)vrad_fl*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_fl;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_fl=tempW3;
                }
                else
                {
                /* vmini_fr=(int16_t)((int32_t)vrad_fr*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_fr;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_fr=tempW3;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    tempW0=vmini_fr-vmini_fl;                               /* vref-vref2;*/

    tempW2=vel_r/100;                                       /* GK14_1*/
    if(vmini_fr-vmini_fl>0)
    {
        tempW0-=tempW2;
    }
    else
    {
        tempW0+=tempW2;
            }

    if(vmini_fr-vmini_fl>=-tempW2)
        {
        if(vmini_fr-vmini_fl<=tempW2)
            {
            tempW0=0;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
       #else
    vmini_rl=vrad_rl;                                       /* GK17*/
    vmini_rr=vrad_rr;

    if(DETECT_MINI_TCS==1)
    {
        if(MINI_REAR==1)
        {
            if(MINI_LEFT==1)
            {
                /*vmini_rl=(int16_t)((int32_t)vrad_rl*U8_MINI_WHEEL_DOWN_RATE/100)*/
                tempW2=vrad_rl;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_rl=tempW3;
        }
        else
        {
                /*vmini_rr=(int16_t)((int32_t)vrad_rr*U8_MINI_WHEEL_DOWN_RATE/100)*/
                tempW2=vrad_rr;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_rr=tempW3;
        }
    }
            else
    {
                ;
            }
    }
    else
    {
        ;
    }
    tempW0=vmini_rr-vmini_rl;                               /* vref-vref2;*/

    tempW2=vel_r/100;                                       /* GK14_1*/
    if(vmini_rr-vmini_rl>0)
	{
        tempW0-=tempW2;
	}
	else
	{
        tempW0+=tempW2;
	}		

    if(vmini_rr-vmini_rl>=-tempW2)
		{
        if(vmini_rr-vmini_rl<=tempW2)
		    {
            tempW0=0;
		    }
		    else
		    {
                ;
			}
	    }
	    else 
	    {
            ;
        }
		          #endif
    if     (tempW0>80)
    {
        tempW0=80;
    }
    else if(tempW0<-80)
		        {
        tempW0=-80;
		        }
		        else
		        {
		            ;
		        }		        
    /* ay[0]=(int16_t)(((int32_t)vel_r*(int32_t)tempW0)/GK_TREAD) */
    tempW2=vel_r;
    tempW1=tempW0;
    tempW0=GK_TREAD;
    s16muls16divs16();
    ay[0]=tempW3;
    if     (ay[0]>1000)
    {
        ay[0]=1000;
    }
    else if(ay[0]<-1000)
    {
        ay[0]=-1000;
    }
    else
    {
        ;
    }

    tempW1=ay_log;
    ay_log=0;
    for( i=0; i<10; i++ )
            {
        ay_log+=ay[i];
            }
    for( i=9; i>0; i-- )
            {
        ay[i]=ay[i-1];
            }

    if     (ay_log> 1000)
                {
        ay_log=1000;
                }
    else if(ay_log< -1000)
                {
        ay_log=-1000;
            }
            else
            {
                ;
            }
    ay_log=(ay_log+tempW1*5)/6;
    if(ay_log>0)
        {
        ay_aver=ay_log;
    }
    else
    {
        ay_aver=-ay_log;
    }
}

void LCTCS_vCalculateAYF(void)
{

    int8_t i;

   #if __REAR_D
    vmini_rl=vrad_rl;                                       /* GK17*/
    vmini_rr=vrad_rr;

    if(DETECT_MINI_TCS==1)
{
        if(MINI_REAR==1)
	{
            if(MINI_LEFT==1)
	    {
                /* vmini_rl=(int16_t)((int32_t)vrad_rl*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_rl;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_rl=tempW3;
	    }
            else
	            {
                /* vmini_rr=(int16_t)((int32_t)vrad_rr*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_rr;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_rr=tempW3;
	            }
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            ;
	        }
    tempW0=vmini_rr-vmini_rl;
    tempW1=vmini_fr-vmini_fl;

    tempW2=vel_r/100;                                       /* GK14_1*/
    if(vmini_rr-vmini_rl>0)
    {
        tempW0-=tempW2;
	    }
	    else
	    {
        tempW0+=tempW2;
	}
    if(vmini_rr-vmini_rl>=-tempW2)
	{
        if(vmini_rr-vmini_rl<=tempW2)
	    {
            tempW0=0;
	    }
        else
	            {
            ;
	            }
	            }
	            else
	            {
	                ;
	            }
#else
    vmini_fl=vrad_fl;                                       /* GK17*/
    vmini_fr=vrad_fr;

    if(DETECT_MINI_TCS==1)
	{
        if(MINI_REAR==0)
	    {
            if(MINI_LEFT==1)
	    {
                /* vmini_fl=(int16_t)((int32_t)vrad_fl*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_fl;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_fl=tempW3;
	        }
	        else
	        {
                /* vmini_fr=(int16_t)((int32_t)vrad_fr*U8_MINI_WHEEL_DOWN_RATE/100) */
                tempW2=vrad_fr;
                tempW1=(int16_t)U8_MINI_WHEEL_DOWN_RATE;
                tempW0=100;
                s16muls16divs16();
                vmini_fr=tempW3;
	        }
	    }
	    else
	    {
	        ;
	    }
	}
	else
	{
	    ;
	}
    tempW0=vmini_fr-vmini_fl;
    tempW1=vmini_rr-vmini_rl;
	
    tempW2=vel_r/100;                                       /* GK14_1*/
    if(vmini_fr-vmini_fl>0)
	{
        tempW0-=tempW2;
	}
    else
	{
        tempW0+=tempW2;
	}
    if(vmini_fr-vmini_fl>=-tempW2)
	{
        if(vmini_fr-vmini_fl<=tempW2)
		{
            tempW0=0;
		}
	            else
		{
	                ;
	            }
		}
		else
	        {
	            ;
	        }
	  #endif	
    if     (tempW0>80)
		{
        tempW0=80;
		}
    else if(tempW0<-80)
		{
        tempW0=-80;
		}
	else
		{
	    ;
		}
    /* ayf[0]=(int16_t)(((int32_t)vel_r*(int32_t)tempW0)/GK_TREAD) */
    tempW2=vel_r;
    tempW1=tempW0;
    tempW0=GK_TREAD;
    s16muls16divs16();
    ayf[0]=tempW3;
    if     (ayf[0]>1000)
	{
        ayf[0]=1000;
	}
    else if(ayf[0]<-1000)
	{
        ayf[0]=-1000;
	}
	else
	{
        ;
	}
	
    tempW1=ayf_log;
    ayf_log=0;
    for( i=0; i<10; i++ )
	{
        ayf_log+=ayf[i];
}
    for( i=9; i>0; i-- )
{
        ayf[i]=ayf[i-1];
		}
	
    if     (ayf_log>1000)
	{
        ayf_log=1000;
	}
    else if(ayf_log<-1000)
	{
        ayf_log=-1000;
	}
	else
	{
		;
	}
		
    ayf_log=(ayf_log+tempW1*5)/6;
					 						 
    ayf_det=ayf_log;                                        /* GK18, GK20_1*/
    if(ay_log<0)
			{
        ayf_det=-ayf_log;
			}
			else
			{
	                ;
			}
	
    if     ( (ay_log>0) && (ayf_log<0) )
		{
        ayf_log=0;                 /* GK18, GK20_1*/
    }
    else if( (ay_log<0) && (ayf_log>0) )
			{
        ayf_log=0;
			}
			else
			{	
				;
			}	
    if(ayf_log>0)
        {
        ayf_aver=ayf_log;
        }
        else
        {
        ayf_aver=-ayf_log;
    }
        
}	
#endif	/* #if !__VDC */
			    	   	
void LCTCS_vCallDetectTraction(void)
{			    	   	
	if ((__TCS_DISABLE)
  		#if __ENGINE_MODE_STEP
    || ((fu8EngineModeStep==0) || (fu8EngineModeStep==1))
	#endif
	   )		
	{
		ETCS_ON = 0;
		FTCS_ON = 0;
	}
	else
	{
		LCTCS_vDecideHomoMuPriority();
	
		if((ETCS_ON==0)&&(FTCS_ON==0))
		{
		    LCTCS_vInitialize();
		    LCTCS_vDecideTractionEntry();
		}
	    else
	    {
		    TCS_ON_START=0;
      	
			LCTCS_vDecideGainAddInTurn();

			#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
			/*  __4WDStuckByDiagonalWheelSpin */
	 		LCTCS_vDct4WDStuckByDiaWhlSpin();
	 		#endif
			}

	  #if MAXIMUM_RPM_LIMIT
	  	LDTCS_vCalAllowableMaxRPM();
	  	LDTCS_vSetOverMaxRPMFlag();
	  #endif		/* #if MAXIMUM_RPM_LIMIT */
		}
	}
	
#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
void LCTCS_vDct4WDStuckByDiaWhlSpin(void)
	{
		if (lctcsu1DrvMode4Wheel==1)
	{/* Parameter Setting */
		/* Spin Calculation */
		tcs_tempW0 = FL.spin;
		tcs_tempW1 = FR.spin;
		tcs_tempW2 = RL.spin;
		tcs_tempW3 = RR.spin;
    	
		/* Parameter Setting */
		tcs_tempW9 = (int16_t)U8_TCS_DIAG_WHL_SPIN_TH;	/* Spin Threshold */
		lctcss16ThOfDct4WDStuckByDWSpn = (int16_t)U16_TCS_DIAG_WHL_SPIN_CNT;
    	
		/* Counting lctcss16DiagonalWhlSpinCnt */
		if (vref < U16_TCS_DIAG_WHL_SPIN_END_SPD)
		{
			if 		( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 < tcs_tempW9) && 
				 	  (tcs_tempW2 < tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* FL, RR Spin */
				lctcss16DiagonalWhlSpinCnt++;
		}
			else if ( (tcs_tempW0 < tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 < tcs_tempW9) )
			{	/* FR, RL Spin */
				lctcss16DiagonalWhlSpinCnt++;	
		}
			else if ( (tcs_tempW0 < tcs_tempW9) && (tcs_tempW1 < tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* Same Axis Wheel Both Spin */
				lctcss16DiagonalWhlSpinCnt = lctcss16DiagonalWhlSpinCnt - 2;
	}
			else if ( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 < tcs_tempW9) && (tcs_tempW3 < tcs_tempW9) )
			{	/* Same Axis Wheel Both Spin */
				lctcss16DiagonalWhlSpinCnt = lctcss16DiagonalWhlSpinCnt - 2;
			}	
			else if ( (tcs_tempW0 < tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* 3Wheel Spin */
				lctcss16DiagonalWhlSpinCnt--;			
		}
			else if ( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 < tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* 3Wheel Spin */
				lctcss16DiagonalWhlSpinCnt--;			
        }
			else if ( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 < tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* 3Wheel Spin */
				lctcss16DiagonalWhlSpinCnt--;			
        }
			else if ( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 < tcs_tempW9) )
			{	/* 3Wheel Spin */
				lctcss16DiagonalWhlSpinCnt--;			
    }
			else if ( (tcs_tempW0 >= tcs_tempW9) && (tcs_tempW1 >= tcs_tempW9) && 
				 	  (tcs_tempW2 >= tcs_tempW9) && (tcs_tempW3 >= tcs_tempW9) )
			{	/* 4Wheel Spin */
				lctcss16DiagonalWhlSpinCnt = lctcss16DiagonalWhlSpinCnt - 4;
    }
    else
    {
				;	/* lctcss16DiagonalWhlSpinCnt Hold */
    }
}	
	else
	{
			lctcss16DiagonalWhlSpinCnt = 0;
	}
		lctcss16DiagonalWhlSpinCnt = LCTCS_s16ILimitRange(lctcss16DiagonalWhlSpinCnt, 0, lctcss16ThOfDct4WDStuckByDWSpn);
	  	
		/* Set/Reset lctcsu1Dct4WDStuckByDiaWhlSpin */
		if (lctcsu1Dct4WDStuckByDiaWhlSpin==0)
	{
			if (lctcss16DiagonalWhlSpinCnt >= lctcss16ThOfDct4WDStuckByDWSpn)
		{	
				lctcsu1Dct4WDStuckByDiaWhlSpin = 1;
		}
	    else
	    {
				lctcsu1Dct4WDStuckByDiaWhlSpin = 0;
		}
	}       		
		else
		{	/* lctcsu1Dct4WDStuckByDiaWhlSpin == 1 */
			if (lctcss16DiagonalWhlSpinCnt <= 0)
			{
				lctcsu1Dct4WDStuckByDiaWhlSpin = 0;
			}
		  	else
		    {
				lctcsu1Dct4WDStuckByDiaWhlSpin = 1;
			}
		}			
	}
	else {}
}
#endif

#if !__VDC
void LCTCS_vCallDetectTrace(void)
	        {
	if ((__TCS_DISABLE)
  		#if __ENGINE_MODE_STEP
    || ((fu8EngineModeStep==0) || (fu8EngineModeStep==1))
		#endif	
	   )		
	            {
		AY_ON = 0;
	            }
	            else
	            {
		if(AY_ON==0)
	                {
		    LCTCS_vInitializeTrace();
		    LCTCS_vDetectTrace();
	                }
	                else
	                {
        ;
	                }
	            }
	        }
#endif /* #if !__VDC */
	  	
void LCTCS_vControlTraction(void)
	        {
    if(TCS_ON_START)
	{
		LCTCS_v1stScanControl();
		ltcss16BaseTorqueLevel = cal_torq;	/* 선회 시 Discrete Input 적용 위함 BJT*/
	    }
	    else
	    {
        ;
	    }

    if( (TCS_ON_START==0)&&((ETCS_ON==1)||(FTCS_ON==1)) )
    {
		LCTCS_vChangeCtlMode();
      	LCTCS_vCallTCSCtrlCnt();
	  
		LCTCS_vSetResetCycle2nd();
	  	LCTCS_vDecideTractionPhase();
		LCTCS_vCalculatePDGain();
		LCTCS_vIncPGainInHighSideSpin();
		#if __VDC
		LCTCS_vCalPDGainInTurn();
		LCTCS_vCalFinalPDGain();
			#endif
		LCTCS_vCheckPGainValidity();
		LCTCS_vCallController();
	}
	else
	{
		;
	}
	
    #if __TCS_DRIVELINE_PROTECT
    LCTCS_vDecMinTorqInDP();
    #endif

    #if __TCS_STUCK_ALGO_ENABLE
    LCTCS_vDecTorqInStuck();
    #endif

	tcs_cmd = LCTCS_s16ICompensateEngineStall(tcs_cmd);		/* 07SWD #9*/
}
  
void LCTCS_v1stScanControl(void)
{
	tcs_tempW1 = (int16_t)McrAbs(delta_yaw_first);
	tcs_tempW2 = (int16_t)McrAbs(wstr);
	tcs_tempW3 = (int16_t)McrAbs(alat);	

	tcs_tempW0 = LCTCS_s16ILimitMinimum(S16_TCS_TURN_DET_BY_ALAT_TH, LAT_0G1G); /*14.11.06*/

	if (ETCS_ON==1)
    {
        if ( (tcs_tempW1>YAW_3DEG)||(tcs_tempW2>ltcss16TargetSpinReduceSteerTh)||(tcs_tempW3>tcs_tempW0) )
        {
			tcs_cmd = LCTCS_s16IInterpDepAy4by3( (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_0_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_0_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_0_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_1_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_2_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_2_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HTURN_2_2 );
    }
    else
		{
			tcs_cmd = LCTCS_s16IInterpDepAccel3by4( (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_0_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_0_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_0_2, U8_TCS_1ST_SCAN_TRQ_HOMO_0_3,
		                                            (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_1_2, U8_TCS_1ST_SCAN_TRQ_HOMO_1_3,
		                                            (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_2_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_2_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_HOMO_2_2, U8_TCS_1ST_SCAN_TRQ_HOMO_2_3);
    }
}
	        else
{
        if ( (tcs_tempW1>YAW_3DEG)||(tcs_tempW2>ltcss16TargetSpinReduceSteerTh)||(tcs_tempW3>tcs_tempW0) )
		{
			tcs_cmd = LCTCS_s16IInterpDepAy4by3( (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_0_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_0_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_0_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_1_2, 
		                                         (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_2_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_2_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_STURN_2_2 );
	        }
		else
		{
			tcs_cmd = LCTCS_s16IInterpDepAccel3by4( (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_0_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_0_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_0_2, U8_TCS_1ST_SCAN_TRQ_SPLIT_0_3,
		                                            (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_1_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_1_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_1_2, U8_TCS_1ST_SCAN_TRQ_SPLIT_1_3,
		                                            (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_2_0, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_2_1, (int16_t)U8_TCS_1ST_SCAN_TRQ_SPLIT_2_2, U8_TCS_1ST_SCAN_TRQ_SPLIT_2_3);
		}
	}	            		                          			

	tcs_cmd=(int16_t)((((int32_t)eng_torq)*tcs_cmd)/100);	

		#if __TCS_HILL_DETECION_CONTROL	
	if (lctcsu1DctSplitHillFlg==1)
	{	/* Hill detected by Ax sensor */
		tcs_cmd = tcs_cmd + (int16_t)U16_TCS_1ST_SCAN_TRQ_MOD_HILL;
	}
	else{}
		#endif /*__TCS_HILL_DETECION_CONTROL*/

		#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
	if(lctcsu1GearShiftFlag==1)
	{
		tcs_cmd = tcs_cmd + U16_TCS_1ST_SCAN_TRQ_MOD_IN_GS;
	}
	else
	{
		;
	}
		#endif  /* #if __TCS_CTRL_DEC_IN_GR_SHFT_MT */	
  
	tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , eng_torq - 30 );   
    }
  
/* 07SWD #9*/
void LCTCS_vDetectEngineStall(void)
{
  #if __ENG_RPM_DISABLE
	ENG_STALL=0;		/* Engine stall not detect for EV */
  #else			
	if ( (ETCS_ON==0) && 
		 ((PC_TCS.lctcsu1EstPresLowFlag==1)&&(SC_TCS.lctcsu1EstPresLowFlag==1)) )
	{	
		ENG_STALL=0;
}
    #if ENGINE_STALL_DETECION_REFINE	
    else
{
		if 		( (eng_rpm < ldtcss16StallDctRPM) && (slope_eng_rpm <= 0) )
	{
			ENG_STALL=1;
	}
		else if ( (eng_rpm > (ldtcss16StallDctRPM + U8_TCS_ENG_STALL_RPM_OFFSET)) && (slope_eng_rpm >= 0) )
	{
		    ENG_STALL=0;
	}
	else
	{
			; /* Do Nothing */
	}
	}
    #else	/* #if ENGINE_STALL_DETECION_REFINE */
  
	else if (lts16WheelSpin > lts16TargetWheelSpin)
    {
		ENG_STALL=0;
    }
    else
    {
		if (ENG_STALL_RPM < ENG_OK_RPM)
	{
			if (eng_rpm < ENG_STALL_RPM - U8_TCS_ENG_STALL_RPM_OFFSET)
	    {
				ENG_STALL=1;
	    }
			else if ( (eng_rpm < ENG_STALL_RPM) && (slope_eng_rpm <= 0) )
	{
			    ENG_STALL=1;
	}
			else if ( (eng_rpm > ENG_OK_RPM) && (slope_eng_rpm >= 0) )
	    {
			    ENG_STALL=0;
	    }
			else if (eng_rpm > ENG_OK_RPM + U8_TCS_ENG_STALL_RPM_OFFSET)
		{
			    ENG_STALL=0;
	    }
	    else
	    {
				; /* Do Nothing */
		}
	}
	    else
		{	
			ENG_STALL=0;		
		}
}
		#endif	/* #if ENGINE_STALL_DETECION_REFINE */

  #endif /*#if __ENG_RPM_DISABLE*/	
}
    
int16_t  LCTCS_s16ICompensateEngineStall(int16_t InputValue)
{
	/*=============================================================================*/
	/* Purpose : 이전 토크 명령값을 input으로 받아서, engine stall 감지시          */
	/*             토크 보상후 output으로 내보냄 							       */
	/* Input Variable : InputValue, cal_torq									   */
	/* Output Variable : OutputValue 						   				       */
	/* 2007. 1. 3 By HJH													       */
	/*=============================================================================*/
	int16_t OutputValue;
      
	LCTCS_vDetectEngineStall();

    if(ENG_STALL==1)
    {
    		  #if ENGINE_STALL_DETECION_REFINE
		if (ENG_STALL_OLD==0)
		{
			if (lctcsu1DctSplitHillFlg==1)
			{	/* Hill detected by Ax sensor */
				tcs_tempW0 = LCESP_s16IInter3Point(ldtcss16StallRiskIndex, STALL_LOW_RISK, S16_LOW_RISK_STALL_TORQ_HILL, 
																		   STALL_MID_RISK, S16_MID_RISK_STALL_TORQ_HILL,
																		   STALL_HIGH_RISK,S16_HIGH_RISK_STALL_TORQ_HILL);
			}
			else
			{	/* Flat */
				tcs_tempW0 = LCESP_s16IInter3Point(ldtcss16StallRiskIndex, STALL_LOW_RISK, S16_LOW_RISK_STALL_TORQ_FLAT, 
																		   STALL_MID_RISK, S16_MID_RISK_STALL_TORQ_FLAT,
																		   STALL_HIGH_RISK,S16_HIGH_RISK_STALL_TORQ_FLAT);
			}
			OutputValue = InputValue + tcs_tempW0;
		}    	
	  #else		/* #if ENGINE_STALL_DETECION_REFINE */

    	tempW0 = (int16_t)ENG_STALL_TORQ;
    
        if(InputValue<(int16_t)tempW0)
    {
            OutputValue = (int16_t)tempW0;
        }
        	  #endif 	/* #if ENGINE_STALL_DETECION_REFINE */        

        else
		    {
			tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U8_ETCS_ENG_STL_TORQ_RISE_RATE, 1);
			if(cal_torq < InputValue)
			{	/* 통상적인 제어에 의하여 토크 증가하는 경우 : 제어 로직에 의한 토크 증가 */
				OutputValue = InputValue;
		    }
		    else
	        {	/* 통상적인 제어에 의하여 토크가 증가하지 않는 경우 : 강제로 토크 Rise Rate 형성 */
				if ((system_loop_counter%tcs_tempW0)==0)
		    {
					OutputValue = cal_torq + 1;
		}
		else
		{
        			OutputValue = cal_torq;
		    }
		}
		}
	}
	else
	{
		LCTCS_vCalMinimumTorqueLevel();
		OutputValue = LCTCS_s16ILimitMinimum(InputValue, lctcss16MinimumTorqLevel);
	}

	return OutputValue;
}

void LCTCS_vCalMinimumTorqueLevel(void)
{
	/*=============================================================================*/
	/* Purpose : Stall이 아닌 상황에서 TCS 제어 중 Minimum Torque Level을 설정     */
	/* Input Variable : ESP_TCS_ON, ETCS_ON, gear_pos, AUTO_TM, FTCS_ON			   */
	/*				    lctcsu1Low2SplitSuspect, lctcss16BaseTorqLevel4L2SpltDct   */
	/*					lctcss16TorqInput4L2SpltDct								   */
	/* Output Variable : lctcss16MinimumTorqLevel								   */
	/* 2007. 7. 12 By Jongtak													   */
	/*=============================================================================*/
	lctcss16MinimumTorqLevelOld = lctcss16MinimumTorqLevel;
	  #if __VARIABLE_MINIMUM_TORQ_LEVEL	
	if(GAIN_ADD_IN_TURN	== 1)
    	{
        lctcss16NewEngOKTorq = LCESP_s16IInter4Point(vref5, (int16_t)U16_TCS_SPEED1, S16_NEW_ENG_OK_TORQ_IN_TURN_0,
		 			   									    (int16_t)U16_TCS_SPEED2, S16_NEW_ENG_OK_TORQ_IN_TURN_1,
					     			  					    (int16_t)U16_TCS_SPEED3, S16_NEW_ENG_OK_TORQ_IN_TURN_2,
					     			  					    (int16_t)U16_TCS_SPEED4, S16_NEW_ENG_OK_TORQ_IN_TURN_3);  	
        lctcss16MinimumTorqLevel = lctcss16NewEngOKTorq;
		}
    	else
    	{
	  #endif /* #if __VARIABLE_MINIMUM_TORQ_LEVEL */					
		if ( (ESP_TCS_ON==0) && (AY_ON==0) )
				{
		  #if __MY08_TCS_DCT_H2L	
			if ( (ETCS_ON==1) || (ltcsu1DctH2L==1) || (ltcsu1HomoMuCtlHavePriority==1) )
		  #else			/* #if __MY08_TCS_DCT_H2L */
			if (ETCS_ON==1)
		  #endif		/* #if __MY08_TCS_DCT_H2L */	
				{
				LCTCS_vCalTargetTorqInHomoMu();
			}
			else if (FTCS_ON==1)
				{
				LCTCS_vCalTargetTorqInSplitMu();
				}
				else
				{
				  #if __VARIABLE_MINIMUM_TORQ_LEVEL
				lctcss16MinimumTorqLevel = lctcss16NewEngOKTorq;
				  #else
				lctcss16MinimumTorqLevel = (int16_t)ENG_OK_TORQ;
				  #endif
			}
		}
		else
		{			
			  #if __VARIABLE_MINIMUM_TORQ_LEVEL
			lctcss16MinimumTorqLevel = lctcss16NewEngOKTorq;
			  #else
			lctcss16MinimumTorqLevel = (int16_t)ENG_OK_TORQ;
			  #endif
		}
	  #if __VARIABLE_MINIMUM_TORQ_LEVEL	    	
}
	  #endif /*#if __VARIABLE_MINIMUM_TORQ_LEVEL    */	

		#if __TCS_VIBRATION_DETECTION
	if (lctcsu1DetectVibration==1)
{
	  	#if __VARIABLE_MINIMUM_TORQ_LEVEL
		lctcss16MinimumTorqLevel = lctcss16NewEngOKTorq;
    #else
		lctcss16MinimumTorqLevel = (int16_t)ENG_OK_TORQ;
    #endif
    }
    else
    {
					;
    }
		#endif /*__TCS_VIBRATION_DETECTION*/      
    }    
    
void LCTCS_vCalTargetTorqInHomoMu(void)
	    {
	if (lctcsu1Low2SplitSuspect == 1)
	{	/* Low-to-Split Suspect 상황인 경우 */
		lctcss16MinimumTorqLevel = lctcss16BaseTorqLevel4L2SpltDct + lctcss16TorqInput4L2SpltDct;
	    }
	    else
	        {
		  #if __VARIABLE_MINIMUM_TORQ_LEVEL
		lctcss16MinimumTorqLevel = lctcss16NewEngOKTorq;
		  #else
		lctcss16MinimumTorqLevel = (int16_t)ENG_OK_TORQ;
		  #endif		
	        }
	    }

void LCTCS_vCalTargetTorqInSplitMu(void)
    {
	/* tcs_tempW0 : Default lctcss16MinimumTorqLevel */
	tcs_tempW0 = LCESP_s16IInter4Point(vref, (int16_t)U16_TCS_SPEED1, S16_TCS_TRQ_LIMIT_SPLIT_0,
								   			 (int16_t)U16_TCS_SPEED2, S16_TCS_TRQ_LIMIT_SPLIT_1,
	                                         (int16_t)U16_TCS_SPEED3, S16_TCS_TRQ_LIMIT_SPLIT_2,
	                                         (int16_t)U16_TCS_SPEED4, S16_TCS_TRQ_LIMIT_SPLIT_3);

	                                                       
	lctcss16MinimumTorqLevelDecTh = LCESP_s16IInter4Point(vref, (int16_t)U16_TCS_SPEED1, S16_TCS_T_TRQ_DEC_TH_SPLIT_0,
								   				 		        (int16_t)U16_TCS_SPEED2, S16_TCS_T_TRQ_DEC_TH_SPLIT_1,
	                                                            (int16_t)U16_TCS_SPEED3, S16_TCS_T_TRQ_DEC_TH_SPLIT_2,
	                                                            (int16_t)U16_TCS_SPEED4, S16_TCS_T_TRQ_DEC_TH_SPLIT_3);

	lctcss16MinimumTorqLevelMinTh = LCESP_s16IInter4Point(vref, (int16_t)U16_TCS_SPEED1, S16_TCS_T_TRQ_MIN_TH_SPLIT_0,
								   				 		        (int16_t)U16_TCS_SPEED2, S16_TCS_T_TRQ_MIN_TH_SPLIT_1,
	                                                            (int16_t)U16_TCS_SPEED3, S16_TCS_T_TRQ_MIN_TH_SPLIT_2,
	                                                            (int16_t)U16_TCS_SPEED4, S16_TCS_T_TRQ_MIN_TH_SPLIT_3);	
	
			#if __TCS_HILL_DETECION_CONTROL
	if (lctcsu1DctSplitHillFlg==1)
	{	/* Hill detected by Ax sensor */
		tcs_tempW0 = tcs_tempW0 + S16_TCS_TRQ_LIM_MOD_SPLIT_HILL;
		lctcss16MinimumTorqLevelDecTh = lctcss16MinimumTorqLevelDecTh + U8_TCS_TRQ_DEC_TH_SPLIT_HILL;
	}
	else{}
		#endif /*__TCS_HILL_DETECION_CONTROL*/
	
	tcs_tempW1 = lctcss16MinimumTorqLevelDecTh + lctcss16MinimumTorqLevelMinTh;			                                                            		                                                            

	  #if __VARIABLE_MINIMUM_TORQ_LEVEL			                                                            		                                                            
	lctcss16MinimumTorqLevel = LCESP_s16IInter2Point(lts16WheelSpin , lctcss16MinimumTorqLevelDecTh, tcs_tempW0,
		                                                              tcs_tempW1, lctcss16NewEngOKTorq);
    #else
	lctcss16MinimumTorqLevel = LCESP_s16IInter2Point(lts16WheelSpin , lctcss16MinimumTorqLevelDecTh, tcs_tempW0,
		                                                              tcs_tempW1, (int16_t)ENG_OK_TORQ);
    #endif
	/* Homo-to-Split 감지시 MinimumTorqueLevel Jump 방지 */
	if ( (ltcsu1Homo2SplitAct==1) || ((ftcs_on_count>L_U8_TIME_10MSLOOP_20MS) && (lctcss16MinimumTorqLevel > lctcss16MinimumTorqLevelOld)) )
    {
		lctcss16MinimumTorqLevel = LCTCS_s16ILimitMaximum(lctcss16MinimumTorqLevel, lctcss16MinimumTorqLevelOld + (int16_t)U8_TCS_TARGET_TORQ_INC_RATE);
    }
    else
    {
        ;
    }

	/* High Side Spin 발생시 Target Torque 저감 */
	if ((FL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(FR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)
	  ||(RL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(RR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable))
    {
		/*if (lctcsu1BTCSHighSideSpinLeft == 1)*/
		if ((FL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(RL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable))
		{	/* High-Mu인 Left Side Wheel Spin */
			tcs_tempW1 = LCTCS_s16IFindMaximum(ltcss16SpinFL,ltcss16SpinRL);
	    }
	    else
		{	/* High-Mu인 Right Side Wheel Spin */ 
			tcs_tempW1 = LCTCS_s16IFindMaximum(ltcss16SpinFR,ltcss16SpinRR);
		}
	  	  #if __VARIABLE_MINIMUM_TORQ_LEVEL		
		lctcss16MinimumTorqLevel = LCESP_s16IInter2Point(tcs_tempW1 , VREF_0_KPH , lctcss16MinimumTorqLevel,
											                          (int16_t)U8_TCS_TARGET_TORQ_DISABLE_HSS, (int16_t)lctcss16NewEngOKTorq);		
		  #else
		lctcss16MinimumTorqLevel = LCESP_s16IInter2Point(tcs_tempW1 , VREF_0_KPH , lctcss16MinimumTorqLevel,
											                          (int16_t)U8_TCS_TARGET_TORQ_DISABLE_HSS, (int16_t)ENG_OK_TORQ);		
	  #endif  
    }
    else
    {
		;
    }
}


#if !__VDC
void LCTCS_vControlTrace(void)
	{

    if(TRACE_ON_START)
        {
        TRACE_ON_START=0;
        if((ETCS_ON==0)&&(FTCS_ON==0))
        {
            trace_cmd=cal_torq-cal_torq/8;
        }
        else
        {
            trace_cmd=tcs_cmd-cal_torq/16;
}

        if(trace_cmd>eng_torq)
            {
            trace_cmd=eng_torq;  /*03NZ*/
            }
            else
            {
                ;
            }
        if(trace_cmd<180)
            {
            trace_cmd=180;
            }
            else
            {
                ;
            }
        zero_count=0;
        AYF_ZERO=0;
        AY_ON=1;
        if(trace_cmd<fric_torq)
            {
            trace_cmd=fric_torq;
            }
            else
            {
                ;
            }
    }
    else if(AY_ON==1)
    {

        LCTCS_vCallTraceLogic();
    	trace_cmd = LCTCS_s16ICompensateEngineStall(trace_cmd);		/* 07SWD #9*/
        if(trace_lamp_count<100)
        {                  /* 200=1.4sec */
            trace_lamp_count++;
            TRACE_ON=0;
        }
        else
    {
            TRACE_ON=1;
		}
    }
    else
    {
        ;
	  	}
}

void LCTCS_vCallTraceLogic(void)
{
    trace_count1++;
    if(trace_count1>=L_U8_TIME_10MSLOOP_100MS)
    {                                  /* Loop Time : 100 msec */
        trace_count1=0;
        trace_cmd=cal_torq;

        ay_diff=ay_aver-ay_aver_alt;
        ay_aver_alt=ay_aver;

        ayf_diff=ayf_aver-ayf_aver_alt;
        ayf_aver_alt=ayf_aver;

        tempW1=err_ay;

            err_ay=ay_aver-ayf_aver;
            derr_ay=err_ay-tempW1;

        if(vel_r<VREF_20_KPH)
{
            if(ay_threshold==AY_THSD_LOW)
{
                if(err_ay<ay_threshold)
	{
                    FTCS_flags = 97;
                    trace_cmd+=5;       /* new*/
	}
	else
	{
                ;
	}
            }
            else
    	{
                if(err_ay<ay_threshold)
            {
                    FTCS_flags = 98;
                    trace_cmd+=25;      /* GK10*/
        }
        else
        {
            ;
        }
	}
        }
	else
	{
            if( (TRACE_TC==0) && (err_ay>=ay_threshold) )
		    {
                TRACE_TC=1;
                ay_opt=ay_aver;
	        }
	        else
	        {
                ;
	    }
            if(AY_HIGH==1)
            {                                /* GK18*/
                if(OPT_TRACE==0)
                {                          /* unstable*/
                    if(derr_ay>=-50)
	    {
                        FTCS_flags = 99;
                        trace_cmd-=derr_ay/3+(err_ay-ay_threshold)/5;    /* 6,2*/
}
                    else if(derr_ay<-300)
	    {
                        FTCS_flags = 100;
                        trace_cmd+=80;
	    }
                    else if(derr_ay<-200)
	    {
                        FTCS_flags = 101;
                        trace_cmd+=50;
		}
                    else if(derr_ay<-150)
			{
                        FTCS_flags = 102;
                        trace_cmd+=30;
    }
                    else if(derr_ay<-100)
				{
                        FTCS_flags = 103;
                        trace_cmd+=15;
	    		}
	else
	    		{											      					
                        ;
	    		}	

                    if( (derr_ay<-20) && ((ayf_aver>ay_opt)||(err_ay<(ay_threshold/2))))
				{
                        OPT_TRACE=1;                        /* recover, go to stable*/
				}
				else
				{
            ;
				}
	    	}
	    	else
                {                                      /* stable, OPT_TRACE=1*/
                    if( (ay_diff<-60) && (ayf_diff<-60) )
	    	{
                        FTCS_flags = 104;
                        trace_cmd+=30;        /* new*/
	    	}
                    else if(err_ay>ay_threshold)
                    {                       /* >200*/
                        FTCS_flags = 105;
                        trace_cmd-=derr_ay/12+(err_ay-ay_threshold)/16; /* 10, 12*/
    	}
                    else
        {    	
                        if(err_ay>(ay_threshold-50))
		{
                            FTCS_flags = 106;
                            if(derr_ay>=0)
 		{
                                trace_cmd+=3;
		}
	    else
		{
                                trace_cmd+=6;
		}
	}
                        else if(err_ay>(ay_threshold-120))
        {
                            FTCS_flags = 107;
                            if(derr_ay>=0)
	    {
                                trace_cmd+=5;
        }
        else
        {
                                trace_cmd+=8;                          /* 8*/
        }
    }
                        else if(err_ay>0)
			{
                            FTCS_flags = 108;
                            if(derr_ay>=0)
				{
                                trace_cmd+=8;                /* 8*/
	    		}
    else
    {
                                trace_cmd+=16;                         /* 15*/
    }
}
				else
{
                            FTCS_flags = 109;
                            if(derr_ay>= 0)
	{
                                trace_cmd+=15;
	    	}
	    	else
        {
                                trace_cmd+=30;
	    	}
    	}
                        if(ay_aver<(ay_threshold-80))
            {
                            trace_cmd+=50;    /* 40*/
        }
                        else if(ay_aver<(ay_threshold-50))
                {
                            trace_cmd+=30;   /* 20*/
                }
                else
                {
                    ;
                }
            }
                    if(ay_diff<-100)
            {
                        trace_cmd+=30;                     /* GK35c, GK36*/
        }
        else
        {
            ;
        }
    }
            #if __VDC
                if((cal_torq-trace_cmd)>20)
            {
                    trace_cmd=cal_torq-20;
    }
                else if((cal_torq-trace_cmd)<-20)
                {
                    trace_cmd=cal_torq+20;
                }
                else
                {
                    ;
                }
            #else
                if((cal_torq-trace_cmd)>100)
                {
                    trace_cmd=cal_torq-100;    /* GK35c */
            }
            else
            {
                ;
            }
            #endif
                if(trace_cmd<200)
                {
                    trace_cmd=200;
        }
        else
        {
            ;
        }        
	}
            else if(ay_threshold>=AY_THSD_MID)
    	{	
                if(OPT_TRACE==0)
                {                                      /* unstable*/
                    FTCS_flags = 110;
        
                    if(derr_ay>=-50)
            {
                        trace_cmd-=derr_ay/4+(err_ay-ay_threshold)/5;   /* 3,5, 6,2*/
            }
                    else if(derr_ay<-300)
            {
                        trace_cmd+=80;
            }
                    else if(derr_ay<-200)
                    {
                        trace_cmd+=50;
    	}
                    else if(derr_ay<-150)
    	{	
                        trace_cmd+=30;
                    }
                    else if(derr_ay<-100)
            {
                        trace_cmd+=15;
            }
            else
            {
                ;
            }			
                    if( (derr_ay<-20) && ((ayf_aver>ay_opt)||(err_ay<(ay_threshold/2))))
                    {
                        OPT_TRACE=1;                  /* recover, go to stable*/
    	}
            else
            {
                ;
    }	
}
        else
                {                                      /* stable, OPT_TRACE=1*/
                    FTCS_flags = 111;

                    if( (ay_diff<-60) && (ayf_diff<-60) )
{
                        trace_cmd+=20;
        }        
                    else if(err_ay>ay_threshold)
	{
                        trace_cmd-=derr_ay/4+(err_ay-ay_threshold)/2;   /* 6, 4*/
	}
                    else
        {
                        if(err_ay>ay_threshold-80)
            {
                            if(derr_ay>=0)
                {
                                trace_cmd+=2;    /* 5*/
                }
                else
                {
                                trace_cmd+=4;              /* ef 3, pbs*/ /* 8*/
                }
            }
                        else if(err_ay>0)
            {
                            if(derr_ay>=0)
    	{	
                                trace_cmd+=4;    /* ef 3, pbs*/ /* 8*/
        }
        else
        {
                                trace_cmd+=6;              /* ef 5, pbs*/ /* 12*/
        }
	}
            else
                        {                              /* 25*/
                            if(derr_ay>=0)
        {
                                trace_cmd+=8;
        }
        else
        {
                                trace_cmd+=15;
    }  
}
	
                        if(ay_aver<(ay_threshold-80))
                        {     /* 30*/
                            if( (err_ay>-50) && (err_ay<50) )
	{
                                trace_cmd+=25;
	}
	else
	{
                                trace_cmd+=15;
	}
}	
                        else if(ay_aver<(ay_threshold-30))
{
                            if( (err_ay>-50) && (err_ay<50) )
	{
                                trace_cmd+=15;
	}
                else
	{
                                trace_cmd+=8;
	}
	}
 	else
	{
                ;
	}
}
                }
                #if __VDC
                    if((cal_torq-trace_cmd)>20)
{
                        trace_cmd=cal_torq-20;
                    }
                    else if((cal_torq-trace_cmd)<-20)
	{	
                        trace_cmd=cal_torq+20;
                    }
        else
		{
            ;
		}
                #else
                    if((cal_torq-trace_cmd)>100)
                    {
                        trace_cmd=cal_torq-100;    /* GK35c*/
	}
                    else
		{
                        ;
		}
	  #endif	
                if(trace_cmd<180)
        {
                    trace_cmd=180;
        }
		else
		{
            ;
		}
	}
	else
            {                                          /* Ice, 70*/
                if(OPT_TRACE==0)
                {                          /* unstable*/
                    FTCS_flags = 112;
                    if((ayf_aver>ay_opt)||(derr_ay<-200)||(err_ay<ay_threshold))
                    {  /* 60*/
                        OPT_TRACE=1;                        /* recover, to stable*/
                        trace_cmd+=10;
                    }
                    else
	{
                        trace_cmd-=derr_ay/2+(err_ay-ay_threshold)*2/3;  /* 2,1 | winter*/
	}		
}
	else
{
                    FTCS_flags = 113;        /*stable*/
                    if(err_ay>ay_threshold)
    {
                        trace_cmd-=derr_ay/3+(err_ay-ay_threshold)/2;
    }
    else
    {
                        if(tm_gear>=TM_1_RATIO)
{
                            if(err_ay>ay_threshold-50)
    {
                                if(derr_ay>0)
	{
                                    trace_cmd+=1;
    }
    else
    {
                                    trace_cmd+=2;
    }
}
                            else if(err_ay>=0)
{
                                if(derr_ay>0)
    {
                                    trace_cmd+=2;
    }
		else
        {
                                    trace_cmd+=3;
		}
        }
        else
        {
                                trace_cmd+=4;                              /* 4*/
    }
	
                            if(ay_aver<(ay_threshold/2))
    {
                                if( (err_ay>-20) && (err_ay<20) )
        {
                                    trace_cmd+=7;     /* 5*/
        }
        else
        {
                                    trace_cmd+=4;                          /* 3*/
        }
    }
                            else if(ay_aver<(ay_threshold-20))
    {
                                if( (err_ay>-20) && (err_ay<20) )
        {
                                    trace_cmd+=4;     /* 3*/
    }
    else
    {
                                    trace_cmd+=2;                          /* 1*/
                                }
        }
        else
        {
            ;
        }
    }
                        else
                        {                                              /* new (4/29)*/
                            if(err_ay>ay_threshold-50)
    {
                                if(derr_ay>0)
        {
                                    trace_cmd+=2;
        }
        else
        {
                                    trace_cmd+=3;
        }
    }
                            else if(err_ay>=0)
    {
                                if(derr_ay>0)
        {
                                    trace_cmd+=3;
        }
        else
        {
                                    trace_cmd+=5;
        }
    }
    else
    {
                                trace_cmd+=6;
}

                            if(ay_aver<(ay_threshold/2))
{
                                if( (err_ay>-20) && (err_ay<20) )
	{
                                    trace_cmd+=8;     /* 10*/
	}
	else
	{
                                    trace_cmd+=6;
    }
}
                            else if(ay_aver<(ay_threshold-20))
{
                                if( (err_ay>-20) && (err_ay<20) )
        {
                                    trace_cmd+=6;
        }
        else
        {
                                    trace_cmd+=4;
        }
        }
        else
        {
            ;
        }
        }
        }
                }
                if(trace_cmd<130)
        {
                    trace_cmd=130;            /* ef 50*/
        }
        else
        {
            ;
        }
            }                                               /* end of ay_threshold=70*/
        }                                                   /* TRACE_TC=1*/
	}
	else
	{
        ;
    }
}
#endif	/* #if !__VDC */

void LCTCS_vControlCommand(void)
{
    if(lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DISABLE)
	{
		LCTCS_vInitializeFTCS();
	}
	else
	{
		#if !ETCS_ENABLE_IN_ABS
    	if(ABS_ON==1)
	{   		
    		LCTCS_vInitializeFTCS();
    }
    	else
  #endif
		if ( (lctcsu1DriverBrakeIntend==1)||(lctcsu1DriverAccelIntend==0) )

	{
    		LCTCS_vInitializeFTCS();
	}
	else
	{
        	;
    	}
	}
    		
    #if __VDC    
    if((ETCS_ON==0)&&(FTCS_ON==0)&&(AY_ON==0)&&(ESP_TCS_ON==0))
#else
    if((ETCS_ON==0)&&(FTCS_ON==0)&&(AY_ON==0))
  #endif
	{
        eng_stall_count=0;
        down_count=0;
	}
	else
	{
		;
	}

    TCS_ON=0;
    if((ETCS_ON==1)||(FTCS_ON==1)||(BTCS_ON==1)
	#if __RDCM_PROTECTION
    ||(ltu1RDCMProtENGOn==1)
    #endif    	
    )    	
	{
        TCS_ON=1;  /* NSM03*/
	}
	else
	{
        ;
		}

#if __VDC
    if((ETCS_ON==0)&&(FTCS_ON==0)&&(AY_ON==0)&&(ESP_TCS_ON==0))
#else
    if((ETCS_ON==0)&&(FTCS_ON==0)&&(AY_ON==0))
#endif
				{
        cal_torq=tcs_cmd=trace_cmd=drive_torq;
				}
				else
				{
        if(((ETCS_ON==1)||(FTCS_ON==1))&&(AY_ON==1))
				{
            if(tcs_cmd>trace_cmd)
					{
                ETCS_ON=FTCS_ON=0;
                tcs_cmd=drive_torq;
                cal_torq=trace_cmd;
					}
					else
            {                                          /* new(4/29)*/
                AY_ON=TRACE_ON=0;
                trace_cmd=drive_torq;
                cal_torq=tcs_cmd;
					}
				}
        else if((ETCS_ON==1)||(FTCS_ON==1))
				{
            if(ENG_STALL==1)
            {                              /* GK23*/
                if(cal_torq>tcs_cmd)
			{
                    tcs_cmd=cal_torq;
		}
		else
		{
                    ;
		}
	}
	else
	{
                ;
		}
            cal_torq=tcs_cmd;
            trace_cmd=drive_torq;
		}
        else if(AY_ON==1)
		{
            cal_torq=trace_cmd;
            tcs_cmd=drive_torq;
		}		
		else
        {       /* ESP_TCS_ON만 1인 경우*/
            tcs_cmd=trace_cmd=drive_torq;
		}
	}
}

void LCTCS_vInitializeFTCS(void)
	{
    if(cal_torq<drive_torq)
		{		
        cal_torq+=20;                   /* 8*/
		}
		else
		{		
        cal_torq=drive_torq;
		}	

    ETCS_ON=AY_ON=TRACE_TC=FTCS_ON=FTC_DETECT=0;
    OPT_TRACE=CYCLE_2ND=MU_SPLIT=0;                         /* GK9*/
    FTC_NEWSET=FTC_SLIP_INC=FTC_SET_HI=GEAR_CH_TCS=0;
    tcs_cmd=trace_cmd=drive_torq;

/* Cycle time opt. */
/*  #if __4WD_VARIANT_CODE==ENABLE
    if((lsu8DrvMode == DM_2H)||(lsu8DrvMode == DM_2WD))
    {*/
    	/* 2WD MODE */
/*    	opt_dv=opt_slip=opt_slip_min=LCESP_s16IInter4Point(vel_r,(int16_t)U16_ETCS_TORQ_CON_S_SPEED,(int16_t)U16_TARGET_THR_SSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_L_SPEED,(int16_t)U16_TARGET_THR_LSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_M_SPEED,(int16_t)U16_TARGET_THR_MSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_H_SPEED,(int16_t)U16_TARGET_THR_HSPD_2WD);
	}
	else
    {*/
  		/* 4WD MODE */
/*    	opt_dv=opt_slip=opt_slip_min=LCESP_s16IInter4Point(vel_r,(int16_t)U16_ETCS_TORQ_CON_S_SPEED,(int16_t)U16_TARGET_THR_SSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_L_SPEED,(int16_t)U16_TARGET_THR_LSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_M_SPEED,(int16_t)U16_TARGET_THR_MSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_H_SPEED,(int16_t)U16_TARGET_THR_HSPD_4WD);
}
	#else
	#if __4WD
	opt_dv=opt_slip=opt_slip_min=LCESP_s16IInter4Point(vel_r,(int16_t)U16_ETCS_TORQ_CON_S_SPEED,(int16_t)U16_TARGET_THR_SSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_L_SPEED,(int16_t)U16_TARGET_THR_LSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_M_SPEED,(int16_t)U16_TARGET_THR_MSPD_4WD,(int16_t)U16_ETCS_TORQ_CON_H_SPEED,(int16_t)U16_TARGET_THR_HSPD_4WD);
				#else    		
    opt_dv=opt_slip=opt_slip_min=LCESP_s16IInter4Point(vel_r,(int16_t)U16_ETCS_TORQ_CON_S_SPEED,(int16_t)U16_TARGET_THR_SSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_L_SPEED,(int16_t)U16_TARGET_THR_LSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_M_SPEED,(int16_t)U16_TARGET_THR_MSPD_2WD,(int16_t)U16_ETCS_TORQ_CON_H_SPEED,(int16_t)U16_TARGET_THR_HSPD_2WD);
  				#endif 		
  #endif*/

    ftcs_count1=ftcs_count2=ftcs_count3=ftc_count=0;
    trace_count1=trace_count2=0;                            /* GK9, 200*/
    ay_diff=ayf_diff=0;
    ay_threshold=ay_th_temp=AY_THSD_MID;                    /* 0.15G(snow)*/
    trace_lamp_count=0;
    thsd_ay_count1=thsd_ay_count2=0;
    tcs_torq_up=0;
    LEFT_MU_HIGH=RIGHT_MU_HIGH=0;
    ay_th_tmp1=ay_th_tmp2=AY_THSD_MID;
    hill_33_count=0;
    hill_33_rec=0;
    HILL_33UP=HILL_STAND=0;

    FTCS_flags = 0;
    lctcsu8TCSEngCtlPhase = 0;
    
    etcs_on_count=0;
    ftcs_on_count=0;
    target_gear = (uint8_t)gs_pos;
    prev_gear = (uint8_t)gs_pos;
    temp_tm_gear = (uint8_t)gs_pos;
    
    Low2HighSuspect=0;
    Low2HighSuspectCount=0;
    Low2HighSuspectTimer=0;
    Low2HighSuspectAccF=0;
    Low2HighSuspectAccR=0;
    Low2HighSuspectAccFTemp=0;
    Low2HighSuspectAccRTemp=0;
  
    YAW_SET = 0;
	ltcss16ETCSEnterExitFlags = 0;
	ltcss16LowTorqueLevelCnt = 0;
	lctcsu1BackupTorqActivation = 0;
	lctcsu1BackupTorqActivationold = 0;
	lctcsu1TCSGainIncInHighmu = 0;
}
 
void LCTCS_vDctSplitHillTendency(void)
    	{
	/*=============================================================================*/
	/* Purpose : Split-mu  제어 중  Split Hill 경향 감지하여 Hunting에 의한 	   */
	/*           Homo-mu 제어로의 전환 방지										   */
	/* Input Variable : WL->BTCS, WL->u8_Estimated_Active_Press, vref, acc_r,      */
	/*                  eng_rpm												       */
	/* Output Variable : BTCS_ONE_SIDE_CTL, lctcss16SplitHillDctCnt, 			   */
	/*                   lctcsu1DctSplitHillFlg									   */
	/* 2007. 11. 15 By Jongtak												       */
	/*=============================================================================*/

	if 			( (BTCS_fl==0) && (BTCS_fr==0) && (BTCS_rl==0) && (BTCS_rr==1) )
			{		
		BTCS_ONE_SIDE_CTL = 1;
			}
	else if 	( (BTCS_fl==0) && (BTCS_fr==0) && (BTCS_rl==1) && (BTCS_rr==0) )
			{		
		BTCS_ONE_SIDE_CTL = 1;
			}	
	else if 	( (BTCS_fl==0) && (BTCS_fr==1) && (BTCS_rl==0) && (BTCS_rr==0) )
	{
		BTCS_ONE_SIDE_CTL = 1;
	}
	else if 	( (BTCS_fl==0) && (BTCS_fr==1) && (BTCS_rl==0) && (BTCS_rr==1) )
	{
		BTCS_ONE_SIDE_CTL = 1;
	}
	else if 	( (BTCS_fl==1) && (BTCS_fr==0) && (BTCS_rl==0) && (BTCS_rr==0) )
	{
		BTCS_ONE_SIDE_CTL = 1;
    	}
	else if 	( (BTCS_fl==1) && (BTCS_fr==0) && (BTCS_rl==1) && (BTCS_rr==0) )
	{
		BTCS_ONE_SIDE_CTL = 1;
  	}
  	else
  	{
		BTCS_ONE_SIDE_CTL = 0;
		}

	/* Wheel Pressure Condition */
	#if __REAR_D
		/* Rear Wheel Drive */
		tcs_tempW0 = LCTCS_s16IFindMaximum((int16_t)RL.u8_Estimated_Active_Press, (int16_t)RR.u8_Estimated_Active_Press);
		tcs_tempW1 = (int16_t)U8_TCS_SPLT_HILL_DCT_PRESS_REAR;
	
	#else
		/* Front Wheel Drive */
		tcs_tempW0 = LCTCS_s16IFindMaximum((int16_t)FL.u8_Estimated_Active_Press, (int16_t)FR.u8_Estimated_Active_Press);
		tcs_tempW1 = (int16_t)U8_TCS_SPLT_HILL_DCT_PRESS_FRNT;
	
	#endif

	/*Longitudinal G Sensor Condition*/
	tcs_tempW2 = (int16_t)McrAbs(lss16absAx1000gDiff);
	#if ((__AX_SENSOR) && (__TCS_HILL_DETECION_CONTROL))
	lctcss16AxFilterDelta = (int16_t)( ((int32_t)(lctcss16AxFilterDelta + (10*(ax_Filter -ax_Filter_old)))*42)/43 );
	if ( (ax_Filter >= (int16_t)S8_TCS_HILL_SLOPE) 
	   &&(tcs_tempW2/*McrAbs(lss16absAx1000gDiff)*/ <= (int16_t)S8_TCS_HILL_AX_DIFF) 
	   &&(lctcss16AxFilterDelta <= (int16_t)S8_TCS_HILL_DELTA_AX) )
	{
		lctcsu8SplitHillDctCntbyAx = lctcsu8SplitHillDctCntbyAx + 1;
		lctcsu8SplitHillDctCntbyAx = (uint8_t)LCTCS_s16ILimitMaximum((int16_t)lctcsu8SplitHillDctCntbyAx, (int16_t)U8_TCS_HILL_DCT_TIMES_BY_AX);
  	}  
				else
	{ 	if (lctcsu8SplitHillDctCntbyAx >= 1)
		{
			lctcsu8SplitHillDctCntbyAx = lctcsu8SplitHillDctCntbyAx - 1;
		}
		else{}
}
	#else
	lctcsu8SplitHillDctCntbyAx = 0;
	#endif /*__AX_SENSOR*/

	/* lctcss16SplitHillDctCnt Counting Condition */
	if (lctcsu1DctSplitHillFlg == 0)
	{
		if ( (BTCS_ONE_SIDE_CTL==1) &&
			 (tcs_tempW0 > tcs_tempW1) &&
			 (vref < (int16_t)U8_TCS_SPLT_HILL_DCT_SPD) &&
			 (acc_r < (int16_t)U8_TCS_SPLT_HILL_DCT_ACCEL) 
				#if !__ENG_RPM_DISABLE
			 && (eng_rpm < (uint16_t)S16_TCS_SPLT_HILL_DCT_RPM) 
			    #endif
		   )		 
		{
			if (lctcsu8SplitHillDctCntbyAx >= U8_TCS_HILL_DCT_TIMES_BY_AX)
			{
				lctcss16SplitHillDctCnt = lctcss16SplitHillDctCnt + 2;
			}
			else
			{
			lctcss16SplitHillDctCnt = lctcss16SplitHillDctCnt + 1;
			}
			lctcss16SplitHillDctCnt = LCTCS_s16ILimitMaximum(lctcss16SplitHillDctCnt, S16_TCS_SPLT_HILL_DCT_TIME);
					}
		else if ( (BTCS_ONE_SIDE_CTL==0) ||
				  (vref >= (int16_t)U8_TCS_SPLT_HILL_CLR_SPD) ||
			      (acc_r >= (int16_t)U8_TCS_SPLT_HILL_CLR_ACCEL) 
					#if !__ENG_RPM_DISABLE
			      || (eng_rpm >= (uint16_t)S16_TCS_SPLT_HILL_CLR_RPM) 
			        #endif
			    ) 
			{
			lctcss16SplitHillDctCnt = 0;
			}
			else
			{
			lctcss16SplitHillDctCnt = lctcss16SplitHillDctCnt - 1;
			lctcss16SplitHillDctCnt = LCTCS_s16ILimitMinimum(lctcss16SplitHillDctCnt, 0);
			}
		}
		else
		{
		if ( (vref < (int16_t)U8_TCS_SPLT_HILL_DCT_SPD) &&
			 (acc_r < (int16_t)U8_TCS_SPLT_HILL_DCT_ACCEL) 
				#if !__ENG_RPM_DISABLE
			 && (eng_rpm < (uint16_t)S16_TCS_SPLT_HILL_DCT_RPM) 
			    #endif
		   )
		{
			lctcss16SplitHillDctCnt = lctcss16SplitHillDctCnt + 1;
			lctcss16SplitHillDctCnt = LCTCS_s16ILimitMaximum(lctcss16SplitHillDctCnt, S16_TCS_SPLT_HILL_DCT_TIME);
		}
		else if ( (vref >= (int16_t)U8_TCS_SPLT_HILL_CLR_SPD) ||
			      (acc_r >= (int16_t)U8_TCS_SPLT_HILL_CLR_ACCEL) 
			      #if !__ENG_RPM_DISABLE
			       || (eng_rpm >= (uint16_t)S16_TCS_SPLT_HILL_CLR_RPM)
			      #endif
			    ) 
		{
			lctcss16SplitHillDctCnt = 0;
	}
	else
	{
			lctcss16SplitHillDctCnt = lctcss16SplitHillDctCnt - 1;
			lctcss16SplitHillDctCnt = LCTCS_s16ILimitMinimum(lctcss16SplitHillDctCnt, 0);
	}
}

	#if __SPIN_DET_IMPROVE
	/*Estimation 구배 조건 추가*/
	if((lss16RoadGradSig_1_100g >= U8TCSHillDctAlongTh)&&( lctcss16VehSpd4TCS <= VREF_3_KPH))
	{
		lctcsu1HillDctbyAxSensor = 1;
	}
	else if ((lss16RoadGradSig_1_100g < (U8TCSHillDctAlongTh-3))||( lctcss16VehSpd4TCS > VREF_5_KPH))
	{
		lctcsu1HillDctbyAxSensor = 0;
	}
	else
	{
		;
	}
	#endif /* Estimation 구배 조건 추가*/
	

	if ( (lctcss16SplitHillDctCnt>=S16_TCS_SPLT_HILL_DCT_TIME)
	    #if __SPIN_DET_IMPROVE	
		|| (lctcsu1HillDctbyAxSensor == 1) 
		#else
		||(ldtcsu1HillDetectionByAx==1)
		#endif
		)
	{
    	lctcsu1DctSplitHillFlg = 1;
	}
    else if (lctcss16SplitHillDctCnt<=S16_TCS_SPLT_HILL_CLR_TIME)
	{
    	lctcsu1DctSplitHillFlg = 0;
	}
	else
	{
		; /* lctcsu1DctSplitHillFlg 유지 */
	}
}

static void LCTCS_vChangeCtlHomoByHunting(void)
	{
    int16_t s16HuntingDetectSpeedDiff = S16_HUNTING_DETECT_SPEED_DIF;
    int16_t s16ReferForDetectFLHighmu = (vrad_crt_fl + VREF_8_KPH);
    int16_t s16ReferForDetectFRHighmu = (vrad_crt_fr + VREF_8_KPH);
    int16_t s16ReferForDetectRLHighmu = (vrad_crt_rl + VREF_8_KPH);
    int16_t s16ReferForDetectRRHighmu = (vrad_crt_rr + VREF_8_KPH);
	
	#if __TCS_HUNTING_DETECTION
    if((BTCS_ON==0)||(FTCS_ON==0)||(lctcsu1DctSplitHillFlg==1))
    {
    	lctcsu1FLMuHigh = 0;
    	lctcsu1FRMuHigh = 0;
    	lctcsu1RLMuHigh = 0;
    	lctcsu1RRMuHigh = 0;
    	lctcsu1FLMuHighOld = 0;
    	lctcsu1FRMuHighOld = 0;
    	lctcsu1RLMuHighOld = 0;
    	lctcsu1RRMuHighOld = 0;
        lctcsu8FLMuHighCnt = 0;
        lctcsu8FRMuHighCnt = 0;
        lctcsu8RLMuHighCnt = 0;
        lctcsu8RRMuHighCnt = 0;
        lctcsu8NoOfFLMuHighCnt = 0;
        lctcsu8NoOfFRMuHighCnt = 0;
        lctcsu8NoOfRLMuHighCnt = 0;
        lctcsu8NoOfRRMuHighCnt = 0;
	}
	else
	{
    	/* FR High-mu detection  */
        lctcsu1FRMuHighOld = lctcsu1FRMuHigh;
        if( vrad_crt_fl > s16ReferForDetectFRHighmu )
{		 
            if(lctcsu8FRMuHighCnt > L_U8_TIME_10MSLOOP_50MS)
  	{
                lctcsu1FRMuHigh = 1;
  	}
  	else
  	{
                lctcsu8FRMuHighCnt++;
}
			}
			else
			{
				;
			}

		if( (lctcsu1FRMuHigh==1) && (vrad_crt_fl<(vrad_crt_fr-(s16HuntingDetectSpeedDiff/2))) )
			{
            lctcsu1FRMuHigh = 0;             
            lctcsu8FRMuHighCnt = 0;
			}
			else
			{
				;
			}
        
        if( (lctcsu1FRMuHighOld==0) && (lctcsu1FRMuHigh==1) )
			{
            lctcsu8NoOfFRMuHighCnt++;
			}
			else
			{
				;
			}			
    
    	/* FL High-mu detection  */
        lctcsu1FLMuHighOld = lctcsu1FLMuHigh;
        if( vrad_crt_fr > s16ReferForDetectFLHighmu )
        {
            if(lctcsu8FLMuHighCnt > L_U8_TIME_10MSLOOP_50MS)
			{
                lctcsu1FLMuHigh = 1;
			}
			else
			{
                lctcsu8FLMuHighCnt++;
		}
	}
	else
	{
		;
	}

	    if( (lctcsu1FLMuHigh==1) && (vrad_crt_fr<(vrad_crt_fl-(s16HuntingDetectSpeedDiff/2))) )
{
            lctcsu1FLMuHigh = 0;             
            lctcsu8FLMuHighCnt = 0;
  	}
  	else
			{		
  		;
  	}	
	
        if( (lctcsu1FLMuHighOld==0) && (lctcsu1FLMuHigh==1) )
{
            lctcsu8NoOfFLMuHighCnt++;
}
  	else
{
  		;
  	}  

    	/* RR High-mu detection  */
        lctcsu1RRMuHighOld = lctcsu1RRMuHigh;
        if( vrad_crt_rl > s16ReferForDetectRRHighmu )
        {
            if(lctcsu8RRMuHighCnt > L_U8_TIME_10MSLOOP_50MS)
            {
                lctcsu1RRMuHigh = 1;
            }
            else
            {
                lctcsu8RRMuHighCnt++;
            }
        }
        else
        {
            ;
        }
        
		if( (lctcsu1RRMuHigh==1) && (vrad_crt_rl<(vrad_crt_rr-(s16HuntingDetectSpeedDiff/2))) )
        {	
            lctcsu1RRMuHigh = 0;             
            lctcsu8RRMuHighCnt = 0;
    }
    else
    {
        ;
    }

        if( (lctcsu1RRMuHighOld==0) && (lctcsu1RRMuHigh==1) )
    {
            lctcsu8NoOfRRMuHighCnt++;
    }
    else
    {
            ;
    }

    	/* RL High-mu detection  */
        lctcsu1RLMuHighOld = lctcsu1RLMuHigh;
        if( vrad_crt_rr > s16ReferForDetectRLHighmu )
    {
            if(lctcsu8RLMuHighCnt > L_U8_TIME_10MSLOOP_50MS)
        {
                lctcsu1RLMuHigh = 1;
        }
        else
        {
                lctcsu8RLMuHighCnt++;
        }
    }
    else
    {
        ;
    }

	    if( (lctcsu1RLMuHigh==1) && (vrad_crt_rr<(vrad_crt_rl-(s16HuntingDetectSpeedDiff/2))) )
            {
            lctcsu1RLMuHigh = 0;             
            lctcsu8RLMuHighCnt = 0;
        }
        else
        {
            ;
        }
        
        if( (lctcsu1RLMuHighOld==0) && (lctcsu1RLMuHigh==1) )
			{
            lctcsu8NoOfRLMuHighCnt++;
    }
    else
    {
        ;
    }

        /* Hunting Detection */    
    
        if( ( (lctcsu8NoOfFLMuHighCnt>=3) && (lctcsu8NoOfFRMuHighCnt>=2) ) || 
        	( (lctcsu8NoOfFRMuHighCnt>=3) && (lctcsu8NoOfFLMuHighCnt>=2) ) || 
        	( (lctcsu8NoOfRLMuHighCnt>=3) && (lctcsu8NoOfRRMuHighCnt>=2) ) || 
        	( (lctcsu8NoOfRRMuHighCnt>=3) && (lctcsu8NoOfRLMuHighCnt>=2) ) ||
        	( (lctcsu8NoOfFLMuHighCnt>=1) && (lctcsu8NoOfFRMuHighCnt>=1) && (lctcsu8NoOfRLMuHighCnt>=1) && (lctcsu8NoOfRRMuHighCnt>=1) )
          )
    {
       		FTCS_ON=0;
        	ETCS_ON=1;
    }
    else
    {
				;
			}			
    }
	#endif /* __TCS_HUNTING_DETECTION */

    if((BTCS_ON==0)||(FTCS_ON==0)||(lctcsu1DctSplitHillFlg==1))
        {
        LEFT_MU_HIGH=RIGHT_MU_HIGH=0;
        hunt_count=0;
        }
        else
        {
        tempW6=L_U8_TIME_10MSLOOP_200MS;                                          /* GK=57*/
        if((EMS37L==1)||(DIESEL==1))
        {
            tempW6=L_U8_TIME_10MSLOOP_100MS;
    }
    else
    {
        ;
    }

        #if __VDC
            tempW6=L_U8_TIME_10MSLOOP_50MS;
#endif
	
  #if __REAR_D
        if( (LEFT_MU_HIGH==0) && (RIGHT_MU_HIGH==0) )
    {
            if(vrad_crt_rl>s16ReferForDetectRRHighmu)
    {
                if(hunt_count>tempW6)
            {
                    LEFT_MU_HIGH=0;
                    RIGHT_MU_HIGH=1;
    }
    else
    {
                    hunt_count++;
    }
    }
            else if(vrad_crt_rr>s16ReferForDetectRLHighmu)
    {
                if(hunt_count>tempW6)
        {
                    LEFT_MU_HIGH=1;
                    RIGHT_MU_HIGH=0;
    }
    else
    {
                    hunt_count++;
    }
    }
    else
    {
                hunt_count=0;
    }
        }
        else
    {
            ;
    }

        if(((RIGHT_MU_HIGH==1)&&(vrad_crt_rl<(vrad_crt_rr-s16HuntingDetectSpeedDiff)))||
           ((LEFT_MU_HIGH==1)&&(vrad_crt_rr<(vrad_crt_rl-s16HuntingDetectSpeedDiff))))
    {
            FTCS_ON=0;
            ETCS_ON=1;
    }
    else
    {
        ;
    }
  #else
        if(LEFT_MU_HIGH==0 && RIGHT_MU_HIGH==0)
    {
            if(vrad_crt_fl>s16ReferForDetectFRHighmu)
        {
                if(hunt_count>tempW6)
            {
                    LEFT_MU_HIGH=0;
                    RIGHT_MU_HIGH=1;
    }
    else
    {
                    hunt_count++;
    }

}
            else if(vrad_crt_fr>s16ReferForDetectFLHighmu)
        {
                if(hunt_count>tempW6)
            {
                    LEFT_MU_HIGH=1;
                    RIGHT_MU_HIGH=0;
            }
            else
            {
                    hunt_count++;
            }
        }
        else
        {
                hunt_count=0;
        }
    }
    else
    {
        ;
    }

        if(((RIGHT_MU_HIGH==1)&&(vrad_crt_fl<(vrad_crt_fr-s16HuntingDetectSpeedDiff)))||
           ((LEFT_MU_HIGH==1)&&(vrad_crt_fr<(vrad_crt_fl-s16HuntingDetectSpeedDiff))))
        {
       		FTCS_ON=0;
        	ETCS_ON=1;
        }
        else
        {
            ;
        }
  #endif
    }
    }

void LCTCS_vCalTractionWheelSpeed(void)
        {
  #if __REAR_D
    tempW2=vrad_crt_rl;
    tempW3=vrad_crt_rr;
  #else
    tempW2=vrad_crt_fl;
    tempW3=vrad_crt_fr;
  #endif

    if(tempW2>tempW3)
            {
    	vel_f_big=tempW2;
    	vel_f_small=tempW3;
    	LEFT_FASTER_THAN_RIGHT = 1;		/* '06 NZ 060815 */
            }
            else
            {
    	vel_f_big=tempW3;
    	vel_f_small=tempW2;
        }

  #if __4WD_VARIANT_CODE==ENABLE
    if(lctcsu1DrvMode2Wheel==1)
        {
    	/* 2WD MODE */
    }
    else
    {
  		/* 4WD MODE */
    	LCTCS_vCalMaxSpin4WD();
    }
  #else
	#if __4WD
    	LCTCS_vCalMaxSpin4WD();
	#endif
  #endif
    }

void LCTCS_vCalFilteredWheelSpeed(void)
    {
    vel_f=(int16_t)((((int32_t)vel_f_big*L_U8FILTER_GAIN_10MSLOOP_3HZ)+((int32_t)vel_f*(128-L_U8FILTER_GAIN_10MSLOOP_3HZ)))/128);   /* vel_f=vel_f_big*/
    vel_r_old=vel_r;
#if __VDC
    if(!ESP_ERROR_FLG)
    {
        tempW6=vref5;                                            /* 040107*/
		if (speed_calc_timer < L_U8_TIME_10MSLOOP_1500MS)
    {
			vel_r=tempW6;		/* Ign on 이후 1초간은 vel_r 계산시 Filtering 하지 않음 */
			               		/* 주행 중 Ign Off -> On시 TCS 이상 제어 진입 방지		*/
    }
    else
    {
			vel_r=(int16_t)((((int32_t)tempW6*L_U8FILTER_GAIN_10MSLOOP_7_5HZ)+((int32_t)vel_r*(128-L_U8FILTER_GAIN_10MSLOOP_7_5HZ)))/128);			
    }
        #if SIM_MATLAB
		if (speed_calc_timer < L_U8_TIME_10MSLOOP_1500MS)
        {
        	vel_r=vref;
        }
        else
        {
        	vel_r=(int16_t)((((int32_t)tempW6*L_U8FILTER_GAIN_10MSLOOP_7_5HZ)+((int32_t)vel_r*(128-L_U8FILTER_GAIN_10MSLOOP_7_5HZ)))/128);			
        }
        #endif
    }
    else
    {
    	LCTCS_vCalVelr();
    }
#else
	LCTCS_vCalVelr();
#endif

    if(((ETCS_ON==1)||(FTCS_ON==1))&&(vel_f<vel_r))
    {
        vel_f=vel_r;
    }
    else
    {
        ;
    }

    }

void LCTCS_vCalculateACC(void)
    {
    int8_t i;

    tempW4=acc_f;                                           /* GK22b*/

    a_f[0]=tempW0-v_f[9];
    tempW1=0;
    for( i=0; i<10; i++ )
    {
        tempW1+=a_f[i];
    }
    acc_f=(int16_t)((((int32_t)tempW1*L_U8FILTER_GAIN_10MSLOOP_5HZ)+((int32_t)acc_f*(128-L_U8FILTER_GAIN_10MSLOOP_5HZ)))/128);    
    for(i=9; i>0; i--)
    {
        v_f[i]=v_f[i-1];
    }
    v_f[0]=tempW0;
    for( i=9; i>0; i-- )
    {
        a_f[i]=a_f[i-1];
    }

    vel_r_diff+=(vel_r-vel_r_old);
    aref_count++;
    if(aref_count>=L_U8_TIME_10MSLOOP_100MS)
    {
        aref_count=0;
        aref_old=aref;
        aref=vel_r_diff+vel_r_diff_old+vel_r_diff_old2;
        vel_r_diff_old2=vel_r_diff_old;
        vel_r_diff_old=vel_r_diff;
        vel_r_diff=0;
        tempW0=acc_r_old=acc_r;
        acc_r=(aref*7)/3;                                   /* x 200*/
        if(CYCLE_2ND==1)
    {
            if(acc_r>tempW0+10)
        {
                acc_r=tempW0+10;            /* 10=0.05G*/
    }
            else if(acc_r<tempW0-10)
    {
                acc_r=tempW0-10;
    }
    else
    {
        ;
    }
        }
        else
    {
            ;
        }
    }
    else
    {
        ;
    }
  	ltcss16AccOfNonDrivenWheel = LCTCS_s16ILimitMinimum((int16_t)acc_r, 0);
	  #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
	if ( (lts16WheelSpin > VREF_0_5_KPH) || (McrAbs(nor_alat) > ltcss16TargetSpinReduceAyTh) || (BIG_ACCEL_SPIN_FZ==1) )
	  #else		/* #if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) */
	if ( (lts16WheelSpin > VREF_0_5_KPH) || (McrAbs(nor_alat) > ltcss16TargetSpinReduceAyTh) )
	  #endif	/* #if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) */
	{	/* acc_r 신뢰할 수 있음 */
		ltcss16AccOfNonDrivenWheel = ltcss16AccOfNonDrivenWheel;
		ltcsu1AccROK = 1;
		
	}
	else
	{	/* acc_r 신뢰할 수 없음 */
		ltcss16AccOfNonDrivenWheel = LCTCS_s16IFindMaximum(ltcss16AccOfNonDrivenWheel, (int16_t)U16_AX_RANGE_MAX);
		ltcsu1AccROK = 0;
	}
}

void LCTCS_vEstmateTMGear(void)
    {
#if !__ENG_RPM_DISABLE
#if __REAR_D
    tempW0=(vrad_crt_rl+vrad_crt_rr)>>1;
#else
    tempW0=(vrad_crt_fl+vrad_crt_fr)>>1;
#endif
    vel_f_mid=(int16_t)((((int32_t)tempW0*L_U8FILTER_GAIN_10MSLOOP_3HZ)+((int32_t)vel_f_mid*(128-L_U8FILTER_GAIN_10MSLOOP_3HZ)))/128);    

    if(AUTO_TM==1)
    {
        gear_pos=(int8_t)gs_pos;
        if((gs_sel==0)||(gs_sel==6))
        {
        	tm_gear=0;
    }
        else if(gs_sel==7)
    {
            tm_gear=TM_BACK_RT;
    }
        else if(gs_pos==1)
    {
            tm_gear=TM_1_RATIO;
    }
        else if(gs_pos==2)
    {
            tm_gear=TM_2_RATIO;
        }
        else if(gs_pos==3)
        {
            tm_gear=TM_3_RATIO;
    }
    else
    {
            tm_gear=TM_4_RATIO;
    }

}
    else
{
        if(vel_f_big<VREF_2_5_KPH)
	{
            tm_gear=0;
            gear_pos=0;
	}
	else
	{
        		tcs_tempW0 = LCTCS_s16ILimitMinimum(vel_f_mid, 1);
            tempW1=(int16_t)eng_rpm/tcs_tempW0;
						#if (__CAR==FIAT_PUNTO)
						if(tempW1>=13)
						#else   
            if(tempW1>=11)
            #endif
		{
                tm_gear=MTM_1_RATIO;
                gear_pos=1; 
		}
            else if(acc_f<acc_r+50)
		{
				if(tempW1>=7)
            {
                    tm_gear=MTM_2_RATIO;
                    gear_pos=2;
		}
                else if(tempW1>=5)
		{
                    tm_gear=MTM_3_RATIO;
                    gear_pos=3;
			}
                else if(tempW1>=4)
                {
                    tm_gear=MTM_4_RATIO;
                    gear_pos=4;
			}
        else
        {
                    tm_gear=MTM_5_RATIO;
                    gear_pos=4;
			}
			}
    else
    {
        ;
			}

			}
			}						

#if !__VDC
    if(AUTO_TM)
    {
        if((vrad_crt_rl<25)&&(vrad_crt_rr<25))
        {   /* Creep Vibration*/
            if(BACK_DIR==0)
            {
                if(gs_sel==7)
    {
                    BACK_DIR=1;
			}
			else
			{
                    ;
			}
		}
		else
		{
                if((gs_sel>0)&&(gs_sel<6))
			{
                    BACK_DIR=0;
			}
			else
			{
            ;
        }
			}
		}
		else
			{
        ;
    }
			}
			else
			{
        ;
			}
    #if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
    if(lcu1TcsCtrlFail==1)
    #else
    if(tcs_error_flg)
    #endif
    {
        BACK_DIR=0;
		}
    else
    {
        ;
	}
#endif
#endif /*#if !__ENG_RPM_DISABLE*/
}

#if !__VDC
void LCTCS_vFindAyThreshold(void)
{
    TRACE_END_FIND = 0;

    ay_th_old=ay_threshold;                                 /* GK13*/
    if(AY_ON==0)
	{
        ay_threshold=AY_THSD_HIGH;                 /* GK19 GK24b*/
	}
	else
	{
        ;
    }
    if(AY_HIGH==1)
    {                                        /* 000613*/
        if(ay_aver>50)
		{
            if(ay_aver>=563)
            {                              /* GK18*/
                ay_threshold=(ay_aver*8)/10;                /* GK35*/
		}
		else
		{
                ay_threshold=AY_THSD_HIGH;
	}
            TRACE_END_FIND = 1;
}
        else
    {
            AY_HIGH=0;
            AY_MID=0;                                       /* 000927*/
    }
    }
    else
    {
        ;
    }

    if(TRACE_END_FIND==0)
    {
		#if __VDC
        tempW1=det_alatm;
    #else

	  #if __4WD_VARIANT_CODE==ENABLE
	    if(lctcsu1DrvMode2Wheel==1)
    {
	    	/* 2WD MODE */
			tempW1=ay_aver;
    }
    else
    {
	  		/* 4WD MODE */
			tempW1=0;
    }
	  #else
		#if __4WD
			tempW1=0;
		#else
            tempW1=ay_aver;
		#endif
    #endif
    #endif
    
        if((vel_r<(int16_t)VREF_90_KPH)&&(vel_r>(int16_t)VREF_30_KPH))
        {    /* GK24d, GK32*/
            if(CIRCLE_SML==0)
            {
                if(tempW1>100)
                {
                    circle_count++;
}
                else
{
                    circle_count=0;
    }
                if(circle_count>L_U8_TIME_10MSLOOP_500MS)
	{
                    CIRCLE_SML=1;
		}
		else
    {
        ;
		}
	}
	else
	{
                if(CIRCLE_BIG==0)
                {                             /* GK27*/
                    if(tempW1>150)
    {
                        down_count++;
		}	            		                          			
    else
    {
                        down_count=0;
	}		
                    if(down_count>L_U8_TIME_10MSLOOP_500MS)
	{
                        CIRCLE_BIG=1;
	}
	else
	{
		;
	}			
}
		else
{
                    if(tempW1<60)
    		{
                        down_count--;
	}
    else
    {
                        down_count=L_U8_TIME_10MSLOOP_1000MS;                        /* GK28*/
		}	
                    if(down_count<L_U8_TIME_10MSLOOP_20MS)
		{
                        CIRCLE_BIG=0;
		}
		else
		{
                        ;
		}
   	}
                if(tempW1<10)
	{
                    circle_count--;
	}	
	else
	{
                    circle_count=L_U8_TIME_10MSLOOP_1000MS;
			}
                if(circle_count<L_U8_TIME_10MSLOOP_20MS)
			{
                    CIRCLE_SML=0;
                    CIRCLE_BIG=0;
			}	
                else
			{
                    ;
			}
			}						
			}
			else
			{
            CIRCLE_SML=0;
            CIRCLE_BIG=0;
            circle_count=0;
            down_count=0;
			}		

        if(lctcsu1DriverAccelIntend==1)
			{
            if(ay_aver>=563)
            {                                  /* GK12*/
                thsd_ay_count1=thsd_ay_count2=0;
                ay_th_tmp2=AY_THSD_HIGH;                        /* 0.2G ==> 0.3G*/
                ay_threshold=AY_THSD_HIGH;
                AY_HIGH=1;                                      /* 000613*/
			}
			else
			{
                ;
			}
		
		}
		else
			{
            ;
		}
	}
		else
			{
        ;
}

    if(AY_ON==1)
{
    if(ENG_STALL==1)
        {                                  /* GK8*/
            if(eng_torq>eng_torq_old+100)
    {
                eng_stall_count++;
	}
	else
		{
                ;
            }
            if(eng_stall_count!=0)
		{
                ay_threshold+=(int16_t)eng_stall_count*50;
			}
			else
		{
		    ;
			}
		}    	
        else
        {
            eng_stall_count=0;
        }
        }
        else
        {
        ;
    }

    if(AY_HIGH==1)
    {
        if(ay_threshold>600)
    {
            ay_threshold=600;                      /* GK35*/
			}
	        else
    	    	{
        ;
    }
 				}
 				else
 				{
        if(ay_threshold>450)
        {
            ay_threshold=450;                      /* GK35*/
				}
	else
	{
            ;
			}
		}
    if((AY_MID!=1)&&(AY_LOW!=1))
    {
        if(ay_threshold<450)
	{
            ay_threshold=450;                      /* GK18, GK35*/
    }
    else
    {
		;
    }

	}
    else
    {
        ;
}

    if(ay_aver-ayf_aver<-20)
    {                                      /* GK14*/
        if(ay_aver-ayf_aver>20)
{
            if(ay_th_old-ay_threshold>2)
	{
                ay_threshold=ay_th_old-2;  /* GK13*/
	}
	else
	{
                ;
			}
	
            if(ay_threshold>450)
            {                                  /* GK13_1, GK35*/
                if(ay_threshold-ay_th_old>2)
			{
                    ay_threshold=ay_th_old+2;
			}
			else
			{
                    ;
			}
		}
		else
		{
                ;
		}
  }
			else
	{
            ;
			}
	}
	else
	{
		;
	}

}
#endif	/* #if !__VDC */

void LCTCS_vInitialize(void)
{
    CYCLE_2ND=LIM_HILL=0;
    FTC_NEWSET=FTC_SLIP_INC=FTC_SET_HI=MU_SPLIT=GEAR_CH_TCS=0;
    ETCS_ON=FTCS_ON=0;
    tcs_cmd=drive_torq;

    ftcs_count1=ftcs_count2=ftcs_count3=ftc_count=0;
    tcs_torq_up=0;
    #if __RDCM_PROTECTION
    if (ltu1RDCMProtENGOn==0)
    {
    	tcs_hold_count=0;
	}
	else
	{
    	;
			}
		  #else
    tcs_hold_count=0;	
		  #endif		
    LEFT_MU_HIGH=RIGHT_MU_HIGH=0;

    FTCS_flags = 0;
    lctcsu8TCSEngCtlPhase = 0;
    etcs_on_count = 0;
    ftcs_on_count = 0;
	                                                       
    target_gear = (uint8_t)gs_pos;
    prev_gear = (uint8_t)gs_pos;
    temp_tm_gear = (uint8_t)gs_pos;
	                                                            
    Low2HighSuspect=0;
    Low2HighSuspectCount=0;
    Low2HighSuspectTimer=0;
    Low2HighSuspectAccF=0;
    Low2HighSuspectAccR=0;
    Low2HighSuspectAccFTemp=0;
    Low2HighSuspectAccRTemp=0;

    YAW_SET = 0;
    lctcsu8FastTorqRecoveryStatus = READY_MODE;

    lctcsu8PosSpinErrDiffCnt1 = 0;
    lctcsu8PosSpinErrDiffCnt2 = 0;
    lctcsu8NegSpinErrDiffCnt1 = 0;

	GAIN_ADD_IN_TURN = 0;
	SplitTransitionCnt = 0;
	lctcsu1QuitH2SplitSuspect = 0;	
	lctcsu1BTCSActInLow2Split = 0;
	ltcss16BothWheelSpinCnt	= 0;
	ltcss16BothSideWheelSpinCntLH = 0;
	ltcss16BothSideWheelSpinCntRH = 0;
	ltcss16LowTorqueLevelCnt = 0;
	lctcsu1BackupTorqActivation = 0;
	lctcsu1BackupTorqActivationold = 0;
	lctcsu1DctSplitHillFlg = 0;
	lctcss16SplitHillDctCnt	= 0;


	lctcsu1BrkRight1WhlCtl = 0; 	lctcsu1BrkLeft1WhlCtl = 0;
	lctcsu1BrkRight2WhlCtl = 0; 	lctcsu1BrkLeft2WhlCtl = 0;
	lctcsu1BrkRightWhlSpin = 0; 	lctcsu1BrkLeftWhlSpin = 0;
	lctcsu1BrkRightWhlSpinOld = 0;	lctcsu1BrkLeftWhlSpinOld = 0;
	lctcsu1Low2SplitSuspect = 0;	lctcsu1Low2SplitSuspectOld = 0;
	lctcsu1Low2SplitSuspectREdge = 0; lctcsu1Low2SplitSuspectFEdge = 0;
	lctcss16TorqInput4L2SpltDct = 0;	
	lctcss16BaseTorqLevel4L2SpltDct	= 0;


  	lctcsu1DctSplitHillFlg = 0;
	lctcss16SplitHillDctCnt	= 0;

	ltcss16BaseTorqueLevelOld = 0;
  	ltcss16BaseTorqueLevel = 0;
	ltcsu1FastTorqReduction = 0;
	ltcsu1FastTorqRecovery = 0;
	ltcss16FastTorqueInputValue = 0;		  	

	ltcsu1Homo2SplitAct = 0;
	ltcsu8CntAfterHomo2SPlit = 0;
	
    /* __4WDStuckByDiagonalWheelSpin */
	lctcss16DiagonalWhlSpinCnt = 0;
	lctcsu1Dct4WDStuckByDiaWhlSpin = 0;
	
	lctcsu1SpinOverThresholdOld = 0;
	lctcsu1SpinOverThreshold = 0;  
	ltcss16BigSpinCnt = 0;
	ltcss16SpinDecCnt = 0;

  #if __TCS_HUNTING_DETECTION	  
	lctcsu1FLMuHigh = 0;
	lctcsu1FRMuHigh = 0;
	lctcsu1RLMuHigh = 0;
	lctcsu1RRMuHigh = 0;
	lctcsu1FLMuHighOld = 0;
	lctcsu1FRMuHighOld = 0;
	lctcsu1RLMuHighOld = 0;
	lctcsu1RRMuHighOld = 0;
    lctcsu8FLMuHighCnt = 0;
    lctcsu8FRMuHighCnt = 0;
    lctcsu8RLMuHighCnt = 0;
    lctcsu8RRMuHighCnt = 0;
    lctcsu8NoOfFLMuHighCnt = 0;
    lctcsu8NoOfFRMuHighCnt = 0;
    lctcsu8NoOfRLMuHighCnt = 0;
    lctcsu8NoOfRRMuHighCnt = 0;
      #endif		                                                              	      		                                                              

	lctcss16LowSpinCntIn1stCycle = 0;

	}

static void LCTCS_vDecideTractionEntry(void)
	{
  #if __GM_FailM
    if(lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DISABLE)
	{
		FTC_DETECT=0;
		ETCS_ON = 0;
		FTCS_ON = 0;

		LCTCS_vInitialize();
	}		                                      
  #endif
	                        
    	if(lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DISABLE)
	{
            FTC_DETECT=0;
		}
  #if !__ENG_RPM_DISABLE
    #if __VDC
        else if( (eng_rpm<=U16_TCS_START_RPM) || (eng_torq<=(int16_t)U16_TCS_START_ENGINE_TORQUE) )
		{
            FTC_DETECT=0;           /* GK22b*/
		}
		  #else
        else if(eng_rpm<1500)
	{
            FTC_DETECT=0;           /* GK22b*/
}
    #endif
  #endif /*#if !__ENG_RPM_DISABLE*/   
			else
{
    	if (FTC_DETECT == 0)
    {
    		#if __REAR_D
    		if((arad_rl>=ARAD_0G5)||(arad_rr>=ARAD_0G5))
				  #else
  			if((arad_fl>=ARAD_0G5)||(arad_fr>=ARAD_0G5))
				  #endif
        {
        		FTC_DETECT=1;
        }
        else
        {
        		;
        }
        }
        else
        {
            ;
        }
        }

  	if (FTC_DETECT==1)
        {
  	#if (__TCS_DRIVELINE_PROTECT==1)
		if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DP)
        {
			LCTCS_vDetDrivelineProtection();
        }
        else
 	#endif
        {
			LCTCS_vDetectTraction();
        }
    }
	else{}

  #if __VDC
	LCTCS_vDetectTractionInTurn();	
  #endif
	                                                            
    if((FTCS_ON==1)||(ETCS_ON==1))
        {
        TCS_ON_START=1;
    }
    else
    {
        ;
    }
}

void LCTCS_vDetectTraction(void)
{
    if(lctcsu1DriverAccelIntend == 1)
        {
        tempW0=vel_f_small-vel_r;
        tempW1=vel_f_big-vel_r;
                
        lctcss16SmallSpin = tempW0;
        lctcss16BigSpin = tempW1;
        
		if (lctcsu1InvalidWhlSpdBfRTADetect == 1)
            {
			tcs_tempW0 = lctcss16WhlSpdreferanceByMiniT;
			tcs_tempW1 = lctcss16WhlSpdreferanceByMiniT;
 		}
		else if (lctcsu1WhlSpdCompByRTADetect == 1)
                {
			tcs_tempW0 = lctcss16WhlSpdreferanceByMiniT;
			tcs_tempW1 = lctcss16WhlSpdreferanceByMiniT;
                }
                else
                {
			tcs_tempW0 = 0;
			tcs_tempW1 = 0;
                }
						
		lctcsu16etcsthreshold1 = (uint16_t)LCESP_s16IInter5Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)ETCS_THR1_0,
			  						   	    	                        (int16_t)U16_TCS_SPEED2, (int16_t)ETCS_THR1_1,
	            		                                                (int16_t)U16_TCS_SPEED3, (int16_t)ETCS_THR1_2,
	            		                                                (int16_t)U16_TCS_SPEED4, (int16_t)ETCS_THR1_3,
	                    		                                        (int16_t)U16_TCS_SPEED5, (int16_t)ETCS_THR1_4);
		lctcsu16etcsthreshold2 = (uint16_t)LCESP_s16IInter5Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)ETCS_THR2_0,
			  						   		                            (int16_t)U16_TCS_SPEED2, (int16_t)ETCS_THR2_1,
	            		                                                (int16_t)U16_TCS_SPEED3, (int16_t)ETCS_THR2_2,
	            		                                                (int16_t)U16_TCS_SPEED4, (int16_t)ETCS_THR2_3,
	                    		                                        (int16_t)U16_TCS_SPEED5, (int16_t)ETCS_THR2_4);	
		/*#if __REAR_D*/
		tcs_tempW2 = (int16_t)McrAbs(wstr);
		tcs_tempW3 = LCTCS_s16ILimitMinimum(S16_TCS_TURN_DET_BY_ALAT_TH, LAT_0G1G); /*14.11.06*/
        tcs_tempW4 = LCTCS_s16ILimitMinimum(S16_TCS_TURN_DET_4_ENT_BY_ALAT_TH, LAT_0G4G); /*14.11.06*/
        tcs_tempW5 = (int16_t)McrAbs(nor_alat);

		if (((tcs_tempW2 > ltcss16TargetSpinReduceSteerTh)||(tcs_tempW5 > tcs_tempW3))&&(det_alatm < tcs_tempW4))
	{
	    	uint8_t	u8etcsthr1decinturn;
	    	uint8_t	u8etcsthr2decinturn;
	    	
	    	u8etcsthr1decinturn = (uint8_t)LCESP_s16IInter3Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)U8_ETCS_THR1_DEC_IN_TURN_0,
				  						   	    	                     (int16_t)U16_TCS_SPEED2, (int16_t)U8_ETCS_THR1_DEC_IN_TURN_1,
		            		                                            (int16_t)U16_TCS_SPEED3, (int16_t)U8_ETCS_THR1_DEC_IN_TURN_2);
	    	u8etcsthr2decinturn = (uint8_t)LCESP_s16IInter3Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)U8_ETCS_THR2_DEC_IN_TURN_0,
				  						   	    	                     (int16_t)U16_TCS_SPEED2, (int16_t)U8_ETCS_THR2_DEC_IN_TURN_1,
		            		                                            (int16_t)U16_TCS_SPEED3, (int16_t)U8_ETCS_THR2_DEC_IN_TURN_2);	        	            		                                     

	    	if (lctcsu16etcsthreshold1 >= (uint16_t)u8etcsthr1decinturn)
	    	{
	    		lctcsu16etcsthreshold1 = lctcsu16etcsthreshold1 - (uint16_t)u8etcsthr1decinturn;
	    	}
	    	else{}
	    	if 	(lctcsu16etcsthreshold2 >= (uint16_t)u8etcsthr2decinturn)
	    	{	
	    		lctcsu16etcsthreshold2 = lctcsu16etcsthreshold2 - (uint16_t)u8etcsthr2decinturn;
	    	}
	    	else{}
			lctcsu16etcsthreshold1 = (uint16_t)LCTCS_s16ILimitMinimum((int16_t)lctcsu16etcsthreshold1, VREF_2_KPH);	
	        lctcsu16etcsthreshold2 = (uint16_t)LCTCS_s16ILimitMinimum((int16_t)lctcsu16etcsthreshold2, VREF_2_KPH);	
            }
	    else{}
	    	/*#endif /*__REAR_D*/

	
        if( (tempW0>((int16_t)lctcsu16etcsthreshold1 + tcs_tempW1)) || (tempW1>((int16_t)lctcsu16etcsthreshold2 + tcs_tempW0)) )
            {
			ltcss16ETCSEnterExitFlags = 1;
		  	
            if(BTCS_TWO_OFF)
                {
                ETCS_ON=1;
                }
            else if(ltcsu1HomoMuCtlHavePriority==1)
            {	/* 선회시는 Homo-mu 제어 우선 진입 */
                ETCS_ON=1;
                }
            else if(BTCS_ON==1)
        {
                FTCS_ON=1;
                dv=((vel_f_big+vel_f_small*2)/3)-vel_r;
                /* Prevent ETCS_ON at Split-mu  by Kim Jeonghun in 2005.11.7*/
            }
            else if((BTCS_Entry_Inhibition==0) && (tempW0<=(int16_t)lctcsu16etcsthreshold1) && (tempW1>(int16_t)lctcsu16etcsthreshold2))
        {
                FTCS_ON=1;
        }
        else
        {
                ETCS_ON=1;
        }
            }
            else
            {
                ;
            }
                    }
        else
                    {
            ;
                    }
}

#if !__VDC
void LCTCS_vInitializeTrace(void)
                    {
    TRACE_TC=OPT_TRACE=TRACE_ON=0;
    trace_cmd=drive_torq;
    trace_count1=0;
    ay_diff=ayf_diff=0;
    trace_lamp_count=0;
                    }

void LCTCS_vDetectTrace(void)
                    {
    TRACE_DISABEL = 0;
    
    if(lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DISABLE)
    {
        TRACE_DISABEL = 1;
                    }
    else
                    {
#if __VDC
        if((YAW_CDC_WORK)||(VDC_REF_WORK))
    {
            TRACE_DISABEL = 1;
                    }
                    else
                    {
                        ;
                    }
#endif
        }

#if (!__4WD || (__CAR==HMC_JM && __4WD_TRACE)) && !__VDC

    if((ETCS_ON==1)||(FTCS_ON==1))
                    {
        TRACE_DISABEL = 1;
                    }
                    else
                    {
                        ;
                    }

    if((AY_ON==0)&&(TRACE_DISABEL==0))
                    {
        TRACE_TC=OPT_TRACE=0;
        trace_count1=0;
        if(AY_HIGH==1)
        {                                    /* GK11_2*/
            tempW1=ay_threshold;
            tempW1+=ay_threshold/16;
                    }
                    else
                    {
            tempW1=ay_aver;
        }

        if(LEFT_TURN==0)
                        {
    #if __REAR_D
            if(vmini_fr-vmini_fl>=VREF_0_KPH)
    #else
            if(vmini_rr-vmini_rl>=VREF_0_KPH)
    #endif
                            {
                LEFT_TURN=1;
                trace_count2=0;
                TRACE_DISABEL = 1;
            }
            else if(trace_count2<L_U8_TIME_10MSLOOP_350MS)
            {                      /* GK35 40to50*/
                trace_count2++;
                TRACE_DISABEL = 1;
                            }
                            else
                            {
                    ;
                            }
                        }
            else
                        {
    #if __REAR_D
            if(vmini_fl-vmini_fr>=VREF_0_KPH)
    #else
            if(vmini_rl-vmini_rr>=VREF_0_KPH)
    #endif
                            {
                LEFT_TURN=0;
                trace_count2=0;
                TRACE_DISABEL = 1;
            }
            else if(trace_count2<50)
            {                      /* GK35 40to50*/
                trace_count2++;
                TRACE_DISABEL = 1;
                            }
                            else
                            {
                    ;
                            }
                        }

        if(TRACE_DISABEL == 0)
                        {
            if(ayf_aver<50)
            {                                   /* GK10*/
                zero_count++;
                if(zero_count>L_U8_TIME_10MSLOOP_500MS)
                            {
                    zero_count=L_U8_TIME_10MSLOOP_550MS;
                    AYF_ZERO=1;
                            }
                            else
                            {
                ;
                            }
                        }
                        else
                        {
                zero_count=0;
                AYF_ZERO=0;
                    }
            if((AYF_ZERO==0)&&(ayf_aver<=5)&&(ay_aver>=500)&&(ay_threshold>=500))
                            {
                TRACE_DISABEL = 1;
                            }
                            else
                            {
                ;
                            }
            if((AYF_ZERO==0)&&(ayf_aver<=5)&&(ay_aver>=175)&&(ay_threshold>=175)&&(ay_threshold<450))
            {    /* GK19 */
                TRACE_DISABEL = 1;
                        }
                    else
                        {
                        ;
                        }
            if(vel_r<=(int16_t)VREF_30_KPH)&&(ay_aver<563))
                        {
                TRACE_DISABEL = 1;
                        }
                        else
                        {
                            ;
                        }
                    }
                    else
                    {
                        ;
                    }

        if(TRACE_DISABEL == 0)
                {
            if(AY_HIGH==1)
            {                                    /* GK18*/
                if(ay_aver>tempW1&&(ay_aver-ayf_aver)>tempW1)
                {
                    if(dv>(int16_t)VREF_2_KPH)
                        {
                        TRACE_ON_START=1;
                }
                else
                {
                    ;
                }
                }
                else
                {
                    ;
                }
                }
                else
                {
                    ;
                }
            }
                    else
                    {
                        ;
                    }
                    }
                    else
                    {
                        ;
                    }
#endif
                }
#endif	/* #if !__VDC */

void LCTCS_vChangeCtlMode(void) /* 060202 06 SWD */
                    {
    if(FTCS_ON)
                    {
    	/* FTCS -> ETCS Transition Condition */
    	LCTCS_vResetVariableOfH2Splt();
    	LCTCS_vChangeCtlModeSplit2Homo();
                    }
    else    /* ETCS_ON = 1 */
                        {
        /* ETCS -> FTCS Transition Condition */
        LCTCS_vResetVariableOfSplt2H();
		if (ltcsu1HomoMuCtlHavePriority==0)
                            {
    		LCTCS_vChangeCtlModeHomo2Split();
                            }
                            else
                            {
                            ;
                            }
                        }
    LCTCS_vSustainCtlHomo2Split();
    LCTCS_vChangeCtlHomoByHunting();    
                            }

void LCTCS_vChangeCtlModeSplit2Homo(void)
                            {
	#if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))
	/*U16_BOTH_SIDE_WHEEL_SPIN_LIMIT consider to current engine torque*/
      #if (__4WD_VARIANT_CODE==ENABLE)
       if (lctcsu1DrvMode4Wheel==1)
      #endif
                            {
		tcs_tempW2 = (vrad_crt_rl-vref)/2;
		tcs_tempW3 = (vrad_crt_rr-vref)/2;
			
		if ((vrad_crt_fl > vrad_crt_fr) && (tcs_tempW2 < (vrad_crt_rr-vref)) 
		&& ((vrad_crt_rr-vref) > (int16_t)U8_SP2HOMO_DETECT_THR_4WD))
                    {
			ltcss16BothSideWheelSpinCntRH++;
                            }
                            else
                            {
			ltcss16BothSideWheelSpinCntRH--;
                        }

		ltcss16BothSideWheelSpinCntRH = LCTCS_s16ILimitRange(ltcss16BothSideWheelSpinCntRH, 0, L_U8_TIME_10MSLOOP_1000MS);	
    	
		if ((vrad_crt_fr > vrad_crt_fl) && (tcs_tempW3 < (vrad_crt_rl-vref)) 
		&& ((vrad_crt_rl-vref) > (int16_t)U8_SP2HOMO_DETECT_THR_4WD))
                            {
			ltcss16BothSideWheelSpinCntLH++;
                            }
                            else
                            {
			ltcss16BothSideWheelSpinCntLH--;
                        }
    	
		ltcss16BothSideWheelSpinCntLH = LCTCS_s16ILimitRange(ltcss16BothSideWheelSpinCntLH, 0, L_U8_TIME_10MSLOOP_1000MS);	
    	
		if ((ltcss16BothSideWheelSpinCntRH > (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT_4WD)
		 || (ltcss16BothSideWheelSpinCntLH > (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT_4WD))
                            {
			FTCS_ON = 0;
			ETCS_ON = 1;
			etcs_on_count = 0;
			ftcs_on_count = 0;
                            }
                            else
                            {
                    ;
                            }
                        }
	 #if (__4WD_VARIANT_CODE==ENABLE)
                        else
                        {
                            ;
                        }
            #endif
	#endif /*((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))*/				
	
	tcs_tempW0 = lctcss16RawWhlSpdDiffDriven;
	#if (__REAR_D)
	tcs_tempW1 = LCTCS_s16IFindMinimum( (vrad_crt_rl-vref) , (vrad_crt_rr-vref) );
	#else
	tcs_tempW1 = LCTCS_s16IFindMinimum( (vrad_crt_fl-vref) , (vrad_crt_fr-vref) );
	#endif
	
/* 07SWD #5	: Count 증가 조건 Tuning parameter 추가*/
	if ( (tcs_tempW0 < (int16_t)U8_SP2HOMO_DETECT_THR1) && (tcs_tempW1 > (int16_t)U8_SP2HOMO_DETECT_THR2) )
                {
		ltcss16BothWheelSpinCnt++;
                    }
	else if (ltcsu1HomoMuCtlHavePriority==1)
	{	/* 선회 중이면 Homo-mu 제어로 전환 */
		ltcss16BothWheelSpinCnt = ltcss16BothWheelSpinCnt + (int16_t)U8_TCS_HOMO_TRAN_ADD_CNT;
                }
                else
                    {
		ltcss16BothWheelSpinCnt--;
                    }
	ltcss16BothWheelSpinCnt = LCTCS_s16ILimitRange( ltcss16BothWheelSpinCnt, 0, (int16_t)L_U16_TIME_10MSLOOP_4S );

	if (ltcss16BothWheelSpinCnt > (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT)
                    {
		FTCS_ON = 0;
		ETCS_ON = 1;
		etcs_on_count = 0;
		ftcs_on_count = 0;
	}
  #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)
	else if( (lctcsu1DrvMode4Wheel==1) && (BIG_ACCEL_SPIN_FZ == 1) )
	{	/* 4WD */
		/* 4WD 에서는 Big spin 감지하면 Homo mu로 제어 전환 */
		FTCS_ON = 0;		
	  	ETCS_ON = 1;       
		etcs_on_count = 0; 
		ftcs_on_count = 0; 
                    }
  #endif	/* #if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1) */	
                    else
                    {
                        ;
                    }
	/*temp HSS 참조 ETCS 전환 15 SWD*/
	if((FTCS_ON==1)&&((FA_TCS.lctcss16HghMuUstbReptReduceCnt>=U8TCSHghMuUstbReptSP2HomoCntTh)||(RA_TCS.lctcss16HghMuUstbReptReduceCnt>=U8TCSHghMuUstbReptSP2HomoCntTh))
		&&(lctcsu1DctSplitHillFlg==0)&&(FA_TCS.lctcsu1OK2AdaptFF==0)&&(RA_TCS.lctcsu1OK2AdaptFF==0)
		#if __HSA
		&&(McrAbs(HSA_ax_sen_filt)<8)/*8deg 미만 평지에서만 ETCS전환*/
		#endif
		)
	{
		FTCS_ON=0;
		ETCS_ON=1;
		etcs_on_count = 0; 
		ftcs_on_count = 0; 
	}
	else
	{
		;
	}
}

void LCTCS_vChangeCtlModeHomo2Split(void)
                    {
	/* 문제점1 : 선회 중에 감지 해야하는가? */
	/* 문제점2 : 필요 브레이크 압력 계산시 Parameter를 어떻게 받을 것인가? */

	/* Variable Initialize */
	lctcsu1BrkRight1WhlCtl = 0; lctcsu1BrkLeft1WhlCtl = 0;
	lctcsu1BrkRight2WhlCtl = 0;	lctcsu1BrkLeft2WhlCtl = 0;

	lctcsu1BrkRightWhlSpinOld = lctcsu1BrkRightWhlSpin;
	lctcsu1BrkLeftWhlSpinOld = lctcsu1BrkLeftWhlSpin;

	/* Brake Control 상태 확인 */
	if 		( ((BTCS_fl==0)&&(BTCS_fr==0)&&(BTCS_rl==0)&&(BTCS_rr==1))
	       || ((BTCS_fl==0)&&(BTCS_fr==1)&&(BTCS_rl==0)&&(BTCS_rr==0)) )
                    {
		tcs_tempW3 = LCTCS_s16IFindMaximum((int16_t)inc_count3_fr,(int16_t)inc_count3_rr);
		lctcsu1BrkRight1WhlCtl = 1;
                    }
	else if	( ((BTCS_fl==0)&&(BTCS_fr==0)&&(BTCS_rl==1)&&(BTCS_rr==0))
	       || ((BTCS_fl==1)&&(BTCS_fr==0)&&(BTCS_rl==0)&&(BTCS_rr==0)) )
                    {
		tcs_tempW3 = LCTCS_s16IFindMaximum((int16_t)inc_count3_fl,(int16_t)inc_count3_rl);
		lctcsu1BrkLeft1WhlCtl = 1;
                    }
	else if	((BTCS_fl==0)&&(BTCS_fr==1)&&(BTCS_rl==0)&&(BTCS_rr==1))
                {
		tcs_tempW3 = LCTCS_s16IFindMaximum((int16_t)inc_count3_fr,(int16_t)inc_count3_rr);
		lctcsu1BrkRight2WhlCtl = 1;
                }
	else if	((BTCS_fl==1)&&(BTCS_fr==0)&&(BTCS_rl==1)&&(BTCS_rr==0))
                {
		tcs_tempW3 = LCTCS_s16IFindMaximum((int16_t)inc_count3_fl,(int16_t)inc_count3_rl);
		lctcsu1BrkLeft2WhlCtl = 1;
                }
	else if ((BTCS_fl==0)&&(BTCS_fr==0)&&(BTCS_rl==0)&&(BTCS_rr==0))
	{		/* No Brake Intervention */
		tcs_tempW0 = 0; tcs_tempW1 = 0; tcs_tempW3 = 0;
		SplitTransitionCnt = 0;
            }
            else
	{		/* Both Wheel Control OR Diagonal Wheel Control */
		tcs_tempW0 = 0; tcs_tempW1 = 0; tcs_tempW3 = 0;
		SplitTransitionCnt = 0;
                    }

  #if __REAR_D
  	if 		( ltcss16SpinRL > (ltcss16SpinRR + VREF_3_KPH) )
                    {
		tcs_tempW0 = ltcss16SpinRR;		/* Spin of High-mu Side Wheel */
		tcs_tempW1 = ltcss16SpinRL;		/* Spin of Low-mu Side Wheel  */
		lctcsu1BrkLeftWhlSpin = 1;		lctcsu1BrkRightWhlSpin = 0;
                }
	else if ( ltcss16SpinRR > (ltcss16SpinRL + VREF_3_KPH) )
                    {
		tcs_tempW0 = ltcss16SpinRL;		/* Spin of High-mu Side Wheel */
		tcs_tempW1 = ltcss16SpinRR;		/* Spin of Low-mu Side Wheel  */
		lctcsu1BrkLeftWhlSpin = 0;		lctcsu1BrkRightWhlSpin = 1;
                    }
                    else
                    {
		tcs_tempW0 = LCTCS_s16IFindMinimum(ltcss16SpinRL, ltcss16SpinRR); /* Spin of High-mu Side Wheel */
		tcs_tempW1 = LCTCS_s16IFindMaximum(ltcss16SpinRL, ltcss16SpinRR); /* Spin of Low-mu Side Wheel  */
		/* lctcsu1BrkLeftWhlSpin, lctcsu1BrkRightWhlSpin는 변동 없음 */
                                }
  #else
  	if 		( ltcss16SpinFL > (ltcss16SpinFR + VREF_3_KPH) )
                                {
		tcs_tempW0 = ltcss16SpinFR;		/* Spin of High-mu Side Wheel */
		tcs_tempW1 = ltcss16SpinFL;		/* Spin of Low-mu Side Wheel  */
		lctcsu1BrkLeftWhlSpin = 1;		lctcsu1BrkRightWhlSpin = 0;
                            }
	else if ( ltcss16SpinFR > (ltcss16SpinFL + VREF_3_KPH) )
                                {
		tcs_tempW0 = ltcss16SpinFL;		/* Spin of High-mu Side Wheel */
		tcs_tempW1 = ltcss16SpinFR;		/* Spin of Low-mu Side Wheel  */
		lctcsu1BrkLeftWhlSpin = 0;		lctcsu1BrkRightWhlSpin = 1;
                                }
                                else
                                {
		tcs_tempW0 = LCTCS_s16IFindMinimum(ltcss16SpinFL, ltcss16SpinFR); /* Spin of High-mu Side Wheel */
		tcs_tempW1 = LCTCS_s16IFindMaximum(ltcss16SpinFL, ltcss16SpinFR); /* Spin of Low-mu Side Wheel  */
		/* lctcsu1BrkLeftWhlSpin, lctcsu1BrkRightWhlSpin는 변동 없음 */
                            }
  #endif

	tcs_tempW2 = McrAbs(vrad_crt_fl - vrad_crt_fr);   
  	tcs_tempW4 = McrAbs(vrad_crt_rl - vrad_crt_rr);

	/* SplitTransitionCnt Reset Condition*/
	if ( (lctcsu1BrkRightWhlSpinOld == 1) && (lctcsu1BrkLeftWhlSpin == 1) )
	{	/* Spin 좌우 교번시 Counter Reset */
		SplitTransitionCnt = 0;
                                }
	else if ( (lctcsu1BrkLeftWhlSpinOld == 1) && (lctcsu1BrkRightWhlSpin == 1) )
	{	/* Spin 좌우 교번시 Counter Reset */
		SplitTransitionCnt = 0;
                        }
  #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
    else if ( (BIG_ACCEL_SPIN_FZ==1) && (lctcsu1DrvMode4Wheel==1) )
                                {
    	SplitTransitionCnt = 0;
                                }
  #endif    /* #if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) */	
	else if	(lctcsu1QuitH2SplitSuspect == 1)
	{	/* Homo2Split 탐색입력중 좌우 속도차가 일정값  이내로 들어오면 탐색 입력 종료  */
		SplitTransitionCnt = 0;
		lctcsu1QuitH2SplitSuspect = 0;
		lctcsu1BTCSActInLow2Split = 0;
                            }
	/* SplitTransitionCnt Counting Condition*/
	else if ( (tcs_tempW0<VREF_0_5_KPH) && (tcs_tempW1>VREF_13_KPH) &&
	     (tcs_tempW3>L_U8_TIME_10MSLOOP_1500MS) && (acc_r<ACCEL_X_0G03G) )
	{	/* 추월 등판시 Homo-μ 오감지시 제어 모드 전환 */
	    SplitTransitionCnt++;
                                }
	else if ( ((lctcsu1BrkRight1WhlCtl==1) || (lctcsu1BrkLeft1WhlCtl==1))
		   && (tcs_tempW0>(int16_t)-U8_HOMO2SP_DETECT_THR1) && (tcs_tempW0<(int16_t)U8_HOMO2SP_DETECT_THR1) 
		   && (tcs_tempW1>(int16_t)U8_HOMO2SP_DETECT_THR2) )
	{  /* 2WD Split-mu Tendency */
	    SplitTransitionCnt = SplitTransitionCnt + 1;
                                }
	else if ( ((lctcsu1BrkRight2WhlCtl==1) || (lctcsu1BrkLeft2WhlCtl==1))
		   && (tcs_tempW0>(int16_t)-U8_HOMO2SP_DETECT_THR1) && (tcs_tempW0<(int16_t)U8_HOMO2SP_DETECT_THR1) 
		   && (tcs_tempW1>(int16_t)U8_HOMO2SP_DETECT_THR2) )
	{  /* 4WD Split-mu Tendency */
	    SplitTransitionCnt = SplitTransitionCnt + 2;
                            }
	else if ( ((lctcsu1BrkRight1WhlCtl==1) || (lctcsu1BrkRight2WhlCtl==1))
		   && (tcs_tempW0>=(int16_t)U8_HOMO2SP_DETECT_THR1) )
	{	/* High Side Wheel Spin 발생한 경우 Counter Decrease */
		SplitTransitionCnt = SplitTransitionCnt - 20;
	}
	else if ( ((lctcsu1BrkLeft1WhlCtl==1) || (lctcsu1BrkLeft2WhlCtl==1))
		   && (tcs_tempW0>=(int16_t)U8_HOMO2SP_DETECT_THR1) )
	{	/* High Side Wheel Spin 발생한 경우 Counter Decrease */
		SplitTransitionCnt = SplitTransitionCnt - 20;
                            }
	else if ( (lctcsu1BrkRightWhlSpin == 1)
           && (   (ltcss16SpinFL > (int16_t)-U8_HOMO2SP_DETECT_THR1) && (ltcss16SpinFL < U8_HOMO2SP_DETECT_WO_B_CTL_THR1) 
           	   && (ltcss16SpinRL > (int16_t)-U8_HOMO2SP_DETECT_THR1) && (ltcss16SpinRL < U8_HOMO2SP_DETECT_WO_B_CTL_THR1) )
           && ( (ltcss16SpinFR > U8_HOMO2SP_DETECT_WO_B_CTL_THR2) || (ltcss16SpinRR > U8_HOMO2SP_DETECT_WO_B_CTL_THR2) ) )
    {	/* Brake 제어 없는 경우 Spin 조건에 의한 Counter Increase */
		SplitTransitionCnt++;
                        }
    else if ( (lctcsu1BrkLeftWhlSpin == 1)
           && (   (ltcss16SpinFR > (int16_t)-U8_HOMO2SP_DETECT_THR1) && (ltcss16SpinFR < U8_HOMO2SP_DETECT_WO_B_CTL_THR1) 
               && (ltcss16SpinRR > (int16_t)-U8_HOMO2SP_DETECT_THR1) && (ltcss16SpinRR < U8_HOMO2SP_DETECT_WO_B_CTL_THR1) )
           && ( (ltcss16SpinFL > U8_HOMO2SP_DETECT_WO_B_CTL_THR2) || (ltcss16SpinRL > U8_HOMO2SP_DETECT_WO_B_CTL_THR2) ) )
    {	/* Brake 제어 없는 경우 Spin 조건에 의한 Counter Increase */
		SplitTransitionCnt++;
                                }
    else if ((tcs_tempW2 < (int16_t)U8_H2S_SUSPECT_QUIT_DELTA_SPD) &&(tcs_tempW4 < (int16_t)U8_H2S_SUSPECT_QUIT_DELTA_SPD)) 
	{	/* Brake 제어 없이 두 바퀴 spin 이 동일할 경우 Counter Decrease*/
		SplitTransitionCnt = SplitTransitionCnt - 20;
	}
                                else
                                {
		/* SplitTransitionCnt Hold */
                            }
	SplitTransitionCnt = LCTCS_s16ILimitRange(SplitTransitionCnt, 0, (int16_t)U16_ONE_WHEEL_SPIN_LIMIT);

	/* ETCS -> FTCS Transition Condition */
	lctcsu1Low2SplitSuspectOld = lctcsu1Low2SplitSuspect;
	lctcsu1Low2SplitSuspectFEdge = 0;
	if (BTCS_ON==1)
	{
		lctcsu1Low2SplitSuspectREdge = 0; 
	}
	else{}
	if 		(SplitTransitionCnt >= (int16_t)U16_ONE_WHEEL_SPIN_LIMIT)
                                {
	    FTCS_ON = 1;
	    ETCS_ON = 0;
	    etcs_on_count = 0;
	    ftcs_on_count = 0;
	    SplitTransitionCnt = 0;
	    lctcsu1QuitH2SplitSuspect = 0;
	    lctcsu1BTCSActInLow2Split = 0;
	    ltcsu1Homo2SplitAct = 1;
	    ltcsu8CntAfterHomo2SPlit = L_U8_TIME_10MSLOOP_1000MS;
                                }
	else if (SplitTransitionCnt >= (int16_t)U16_TCS_L2SPLIT_SUSPECT1)
                                {
		lctcsu1Low2SplitSuspect = 1;
                            }
                            else
                            {
		lctcsu1Low2SplitSuspect = 0;
                            }

	/* Edge Detection */
	if 		( (lctcsu1Low2SplitSuspectOld == 0) && (lctcsu1Low2SplitSuspect == 1) )
                                {
		lctcsu1Low2SplitSuspectREdge = 1;
		/* Searching Input 가하기 전 토크 Level 저장 */
		lctcss16BaseTorqLevel4L2SpltDct = cal_torq;
                                }
	else if ( (lctcsu1Low2SplitSuspectOld == 1) && (lctcsu1Low2SplitSuspect == 0) )
                                {
		lctcsu1Low2SplitSuspectFEdge = 1;
                            }
                        else
                                {
                            ;
                                }

	/* Calculation of Serching input for Homo-to-Split Detection */
	lctcss16TorqInput4L2SpltDct = (int16_t)U8_TCS_TORQ_INPUT_FOR_L2SPLT;		/* NF 1단 기준 */
	
	/* Quit Homo-to-Split Detection : moved from LCTCS_vDetectBTCSExitQuick */
	
	if	(lctcsu1Low2SplitSuspect == 1)
	{	/* 좌우 속도차가 일정값 이내로 들어오면 즉시 Dump 하여 두 wheel 속도가 같아 지도록 유도함 */
		if ((BTCS_rl==1) || (BTCS_rr==1)||(lctcsu1BTCSActInLow2Split==1))
		{	/* Rear Wheel */ 
			tcs_tempW0 = McrAbs(vrad_crt_rl - vrad_crt_rr);
			tcs_tempW1 = LCTCS_s16IFindMinimum(vrad_crt_rl, vrad_crt_rr);
			tcs_tempW1 = tcs_tempW1 - vref5;

			lctcsu1BTCSActInLow2Split = 1;
			
			if ( (tcs_tempW0 < (int16_t)U8_H2S_SUSPECT_QUIT_DELTA_SPD) && (tcs_tempW1 > VREF_3_KPH) )
                                {
				lctcsu1QuitH2SplitSuspect = 1;
                                }
                            }
		
		if ((BTCS_fl==1) || (BTCS_fr==1)||(lctcsu1BTCSActInLow2Split==1))
		{   /* Front Wheel */
			tcs_tempW0 = McrAbs(vrad_crt_fl - vrad_crt_fr);
			tcs_tempW1 = LCTCS_s16IFindMinimum(vrad_crt_fl, vrad_crt_fr);
			tcs_tempW1 = tcs_tempW1 - vref5;			

			lctcsu1BTCSActInLow2Split = 1;

			if ( (tcs_tempW0 < (int16_t)U8_H2S_SUSPECT_QUIT_DELTA_SPD) && (tcs_tempW1 > VREF_3_KPH) )
                            {
				lctcsu1QuitH2SplitSuspect = 1;
                        }
                    }
                }
	else if (lctcsu1Low2SplitSuspectFEdge==1)
                {
		lctcsu1QuitH2SplitSuspect = 1;		
                }
                else
                {
                    ;
                }

    }

void LCTCS_vResetVariableOfH2Splt(void)
    {
	/*=============================================================================*/
	/* Purpose : Split-mu 제어 시 Homo-to-Split 감지에 사용 변수                   */
	/*			 Reset									   						   */
	/* Input Variable :                                                            */
	/* Output Variable :                                                           */
	/* 2007. 7. 20 By Jongtak												       */
	/*=============================================================================*/
	SplitTransitionCnt = 0;
	lctcsu1QuitH2SplitSuspect = 0;	
	lctcsu1BrkRight1WhlCtl = 0; 	lctcsu1BrkLeft1WhlCtl = 0;
	lctcsu1BrkRight2WhlCtl = 0; 	lctcsu1BrkLeft2WhlCtl = 0;
	lctcsu1BrkRightWhlSpin = 0; 	lctcsu1BrkLeftWhlSpin = 0;
	lctcsu1BrkRightWhlSpinOld = 0;	lctcsu1BrkLeftWhlSpinOld = 0;
	lctcsu1Low2SplitSuspect = 0;	lctcsu1Low2SplitSuspectOld = 0;
	lctcsu1Low2SplitSuspectREdge = 0; lctcsu1Low2SplitSuspectFEdge = 0;
	lctcss16TorqInput4L2SpltDct = 0;	
	lctcss16BaseTorqLevel4L2SpltDct	= 0;
	lctcsu1BTCSActInLow2Split = 0;
	
}

void LCTCS_vResetVariableOfSplt2H(void)
	{
	/*=============================================================================*/
	/* Purpose : Homo-mu 제어 시 Split-to-Homo 감지에 사용 변수                    */
	/*			 Reset									   						   */
	/* Input Variable :                                                            */
	/* Output Variable :                                                           */
	/* 2007. 7. 20 By Jongtak												       */
	/*=============================================================================*/	
	ltcss16BothWheelSpinCnt = 0;
	ltcss16BothSideWheelSpinCntLH = 0;
	ltcss16BothSideWheelSpinCntRH = 0;
    }

void LCTCS_vDetectTractionInTurn(void)
    {
	if(ESP_TCS_ON)
    	{    	
    	ETCS_ON = 1;
    	CYCLE_2ND = 0;	
		ltcss16ETCSEnterExitFlags = 3;
        				
    	}
    	else
    	{
        	;
    	}
    }


void LCTCS_vTurnOffFTCS(void)
    {
    ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=0;

    #if !SIM_MATLAB
    LATCS_vConvertTCS2EMS();
    #endif    	
    }

    #if __EMS_TEST_T
void LCTCS_vTestEngineMode(void)
    {
#if !SIM_MATLAB

#if __VDC
    #if __GM_FailM
    if(fu1ESCDisabledBySW==1)
#else
    if(vdc_on_flg==0)
    #endif
  #else
    #if __GM_FailM
    if(fu1TCSDisabledBySW==1)
    #else
    if(tc_on_flg==0)
                #endif
#endif
    {
        press_test_count++; tempW0=0;

        if(press_test_count>2500)
                {
            press_test_count=2500;
    }
    else
    {
                    ;
                }
        if((press_test_count>100)&&(press_test_count<2500))
        {
            if(press_test_count<300)
            {
                tempW0=500;
        }
            else if(press_test_count<700)
        {
                tempW0=100;
            }
            else if(press_test_count<900)
                {
                tempW0=300;
                }
            else if(press_test_count<1100)
                {
                tempW0=600;
                }
            else if(press_test_count<1500)
            {
                tempW0=950;
            }
            else if(press_test_count<1700)
            {
                tempW0=500;
            }
            else if(press_test_count<1720)
                    {
                tempW0=400;
        }
            else if(press_test_count<1740)
        {
                tempW0=300;
        }
            else if(press_test_count<1760)
                        {
                tempW0=200;
        }
            else if(press_test_count<2100)
                            {
                tempW0=100;
    }
            else if(press_test_count<2140)
                                {
                tempW0=150;
}
            else if(press_test_count<2160)
{
                tempW0=200;
                                }
            else if(press_test_count<2180)
    {
                tempW0=250;
    }
            else if(press_test_count<2200)
    {
                tempW0=300;
    }
            else if(press_test_count<2220)
                                {
                tempW0=350;
    }
            else if(press_test_count<2240)
                                {
                tempW0=400;
    }
            else if(press_test_count<2260)
            {
                tempW0=450;
}
            else if(press_test_count<2280)
{
                tempW0=500;
                            }
            else if(press_test_count<2300)
	{
                tempW0=550;
	}
            else if(press_test_count<2320)
	{
                tempW0=600;
	}
            else if(press_test_count<2340)
	{
                tempW0=650;
	}
            else if(press_test_count<2360)
	{
                tempW0=700;
	}
            else if(press_test_count<2380)
	{
                tempW0=750;
	}
            else if(press_test_count<2400)
	{
                tempW0=800;
	}
	else
	{
                tempW0=950;
	}

        	lespu1TCS_TCS_REQ = 1;             /* set TCS_REQ*/
        	lespu1TCS_TCS_CTL = 1;             /* set TCS_CTL*/
        	lesps16TCS_TQI_TCS = tempW0;       /* set TQI_TCS*/
            lesps16TCS_TQI_MSR = 0;		       /* clear TQI_MSR*/
	}
	else
		{
            lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
            lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
            lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */
            lesps16TCS_TQI_TCS = 0x3fc;                  /* clear TQI_TCS		  */
            lesps16TCS_TQI_MSR = 0;                     /* clear TQI_MSR		  */
		}
		}
		else
		{
        press_test_count=0;
	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
        lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
        lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */
        lesps16TCS_TQI_TCS = 0x3fc;                  /* clear TQI_TCS		  */
        lesps16TCS_TQI_MSR = 0;                     /* clear TQI_MSR		  */
		}

#endif

	}
			      #endif

void LCTCS_vCalculateSpin(void)
    {
	/*=============================================================================*/
	/* Purpose : TCS Engine Intervention에서 사용할 Wheel Spin 계산      	 	   */
	/* Input Variable : ltcss16MovingAveragedWhlSpd_fl, wvref_fl, vref             */
	/*                  ltcss16MovingAveragedWhlSpd_fr, wvref_fr,                  */
	/*                  ltcss16MovingAveragedWhlSpd_rl,	wvref_rl, 			       */
	/*                  ltcss16MovingAveragedWhlSpd_rr, wvref_rr,                  */
	/* Output Variable : lts16FrontWheelSpinAverage, lts16FrontWheelSpinAverageOld */
    /*                   lts16RearWheelSpinAverage, lts16RearWheelSpinAverageOld   */
	/*                   lts16WheelSpin											   */
	/*                   ltcss16SpinFL, ltcss16SpinFR, ltcss16SpinRL, ltcss16SpinRR*/
	/* 2006. 4. 3 By Jongtak												       */
	/*=============================================================================*/

	ltcss16SpinFL = FL.spin;
	ltcss16SpinFR = FR.spin;
	ltcss16SpinRL = RL.spin;
	ltcss16SpinRR = RR.spin;

	/* Update Old Value */
	lts16FrontWheelSpinAverageOld = lts16FrontWheelSpinAverage;
	lts16RearWheelSpinAverageOld = lts16RearWheelSpinAverage;

  	/* Averaging */
  	lts16FrontWheelSpinAverage = ( ltcss16SpinFL + ltcss16SpinFR ) / 2;
	lts16RearWheelSpinAverage = ( ltcss16SpinRL + ltcss16SpinRR ) / 2;

  #if __TCS_DRIVELINE_PROTECT
	if (lctcss16EngCtrlMode == S16_TCS_CTRL_MODE_DP)
    {
	  	lts16WheelSpin = ltcss16DeltaSpdOfFrontAndRear;
    }
    else
	{
    /* Comparison */
	lts16WheelSpin = LCTCS_s16IFindMaximum(lts16FrontWheelSpinAverage,lts16RearWheelSpinAverage);
	}
  #else		/* #if __TCS_DRIVELINE_PROTECT */
    /* Comparison */
	lts16WheelSpin = LCTCS_s16IFindMaximum(lts16FrontWheelSpinAverage,lts16RearWheelSpinAverage);	
  #endif	/* #if __TCS_DRIVELINE_PROTECT */	
	
	/* Min & Max Limitation */
	lts16WheelSpin = LCTCS_s16ILimitRange(lts16WheelSpin, VREF_0_KPH, VREF_200_KPH);
}

void LCTCS_vCalculateTargetSpin(void)
{
	/*=============================================================================*/
	/* Purpose : Target Wheel Spin 계산      	 	   							   */
	/* Input Variable : vref, ltcss16AccOfNonDrivenWheel						   */
	/* Output Variable : lts16TargetWheelSpin									   */
	/* 2006. 4. 3 By Jongtak												       */
	/*=============================================================================*/

	/* Target Spin Calculation Depends on Vehicle Speed & Road Condition */

	/* Homo-mu Target Spin Calculation */
	tcs_tempW0 = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_TARGET_SPIN_HOMO_MU_0_0, (int16_t)U8_TARGET_SPIN_HOMO_MU_0_1, (int16_t)U8_TARGET_SPIN_HOMO_MU_0_2, (int16_t)U8_TARGET_SPIN_HOMO_MU_0_3,
	 									       (int16_t)U8_TARGET_SPIN_HOMO_MU_1_0, (int16_t)U8_TARGET_SPIN_HOMO_MU_1_1, (int16_t)U8_TARGET_SPIN_HOMO_MU_1_2, (int16_t)U8_TARGET_SPIN_HOMO_MU_1_3);


	tcs_tempW1 = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_TARGET_SPIN_SPLIT_MU_0_0, (int16_t)U8_TARGET_SPIN_SPLIT_MU_0_1, (int16_t)U8_TARGET_SPIN_SPLIT_MU_0_2, (int16_t)U8_TARGET_SPIN_SPLIT_MU_0_3,
	 									       (int16_t)U8_TARGET_SPIN_SPLIT_MU_1_0, (int16_t)U8_TARGET_SPIN_SPLIT_MU_1_1, (int16_t)U8_TARGET_SPIN_SPLIT_MU_1_2, (int16_t)U8_TARGET_SPIN_SPLIT_MU_1_3);

		#if __TCS_HILL_DETECION_CONTROL
	if (lctcsu1DctSplitHillFlg==1)
    {
		tcs_tempW1 = tcs_tempW1 + (int16_t)U8_TARGET_SPIN_INC_SPLIT_HILL;
    }
	else{}
		#endif /*__TCS_HILL_DETECION_CONTROL*/
		
		#if __TCS_REFER_TARGET_TORQ_CTL
	tcs_tempW2 = LCESP_s16IInter3Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)U8_RED_TCS_TIME_TARGET_SPIN_1,  
                                              (int16_t)U16_TCS_SPEED2, (int16_t)U8_RED_TCS_TIME_TARGET_SPIN_2,  
                                              (int16_t)U16_TCS_SPEED3, (int16_t)U8_RED_TCS_TIME_TARGET_SPIN_3);
	tcs_tempW3 = LCESP_s16IInter3Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)U8_INC_TARGET_SPIN_HOMO_MU_1,  
                                              (int16_t)U16_TCS_SPEED2, (int16_t)U8_INC_TARGET_SPIN_HOMO_MU_2,  
                                              (int16_t)U16_TCS_SPEED3, (int16_t)U8_INC_TARGET_SPIN_HOMO_MU_3);
	tcs_tempW4 = LCESP_s16IInter3Point(vref5, (int16_t)U16_TCS_SPEED1, (int16_t)U8_INC_TARGET_SPIN_SPLIT_MU_1,  
                                              (int16_t)U16_TCS_SPEED2, (int16_t)U8_INC_TARGET_SPIN_SPLIT_MU_2,  
                                              (int16_t)U16_TCS_SPEED3, (int16_t)U8_INC_TARGET_SPIN_SPLIT_MU_3);                     	                                                             
    if (TCS_ON_START==1)
    {
    	lctcsu8TargetSpinIncTime = (uint8_t)tcs_tempW2;
    	lctcsu8TargetSpinIncInHomo = (uint8_t)tcs_tempW3;
    	lctcsu8TargetSpinIncInSplit = (uint8_t)tcs_tempW4;
    }
    else{}
                                                               
	if (U8_TCS_REFER_TAR_TORQ_IMPROVE==1)		
	{
		if ( ((ETCS_ON==1) || (FTCS_ON==1)) )
		{
			if (lctcsu8TargetSpinIncCount<=lctcsu8TargetSpinIncTime)
			{
				lctcsu8TargetSpinIncCount = lctcsu8TargetSpinIncCount + 1;
				lctcsu8TargetSpinIncCount = (uint8_t)LCTCS_s16ILimitMaximum((int16_t)lctcsu8TargetSpinIncCount, (int16_t)lctcsu8TargetSpinIncTime);
			}
			else{}
		}
		else
		{
			lctcsu8TargetSpinIncCount = 0;
		}
		if ((GAIN_ADD_IN_TURN==0) && ((ETCS_ON==1) || (FTCS_ON==1)))
		{
			tcs_tempW5 = LCESP_s16IInter2Point((int16_t)lctcsu8TargetSpinIncCount, 0, (tcs_tempW0 + (int16_t)lctcsu8TargetSpinIncInHomo),
											                                       (int16_t)lctcsu8TargetSpinIncTime, tcs_tempW0);
			tcs_tempW6 = LCESP_s16IInter2Point((int16_t)lctcsu8TargetSpinIncCount, 0, (tcs_tempW1 + (int16_t)lctcsu8TargetSpinIncInSplit),
											                                       (int16_t)lctcsu8TargetSpinIncTime, tcs_tempW1);
			lctcsu8DeltaTriHomoTargetSpin = (uint8_t)(tcs_tempW5 - tcs_tempW0);
			lctcsu8DeltaTriSplitTargetSpin = (uint8_t)(tcs_tempW6 - tcs_tempW1);
			tcs_tempW0 = tcs_tempW5;
			tcs_tempW1 = tcs_tempW6;
		}
		else
		{
			lctcsu8DeltaTriHomoTargetSpin = 0;
			lctcsu8DeltaTriSplitTargetSpin = 0;
		}
	}
	else{}
		#endif /*__TCS_REFER_TARGET_TORQ_CTL*/
		
	/* Split Hill에서 Brake 제어만 작동하는 경우 Target은 Split Target으로 사용하기 위함 */
	if (ETCS_ON == 1)				/* Homo-mu */
            {
		/* Split-mu 경향에 따른 Target Spin 변화 고려 */
		/* 탐색 입력 Concept에 적용에 의한 Target 변경하는 concept 삭제 */
		lts16TargetWheelSpin = tcs_tempW0;
            }    
            else
            {	
		/* 07SWD #8 : High Side Spin 발생시 Target Spin 저감 */
		if ((FL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(FR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)
	      ||(RL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(RR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable))
		{	/* U8_TCS_TARGET_SPIN_VARI_F_HSS : 100->Homo-mu Target Spin, 0->Split-mu Target Spin */
			lts16TargetWheelSpin = LCESP_s16IInter2Point((int16_t)U8_TCS_TARGET_SPIN_VARI_F_HSS, 0, tcs_tempW1,
		 								  					                               100, tcs_tempW0);
        }    
        else
        {	
			lts16TargetWheelSpin = tcs_tempW1;
        }        		
        }		
        
	/* Min & Max Limitation */
	if (lctcsu1RoughRoad==1)
        {	
		tcs_tempW7 = lts16TargetWheelSpin + U8_TARGET_SPIN_INC_ROUGH_ROAD;
		tcs_tempW7 = LCTCS_s16ILimitMinimum(tcs_tempW7, VREF_3_KPH);
        }    
        else
        {	
		tcs_tempW7 = VREF_2_KPH;
                                }
	lts16TargetWheelSpin = LCTCS_s16ILimitRange(lts16TargetWheelSpin, tcs_tempW7, VREF_20_KPH);
        }
    
void LCTCS_vDctTargetGoodTracking(void)
        {
	/*=============================================================================*/
	/* Purpose : Target Spin을 잘 추종 여부 판단								   */
	/*           Target Spin을 지속적으로 잘 추종하면 주기적으로 Torque Up 수행    */
	/* Input Variable : GAIN_ADD_IN_TURN, lts16WheelSpinError,   				   */
	/*                  lts16WheelSpinErrorDiff								       */
	/* Output Variable : ltcss16GoodTargetTrackingCnt, lts16TargetWheelSpin		   */
	/* 2007. 10. 31 By Jongtak												       */
	/*=============================================================================*/
	/* Target Good Tracking Counter */
	if (GOOD_TARGET_TRACKING_MODE == 0)
            {
		if ( (GAIN_ADD_IN_TURN==0) && (McrAbs(lts16WheelSpinError) < VREF_1_KPH) && (McrAbs(lts16WheelSpinErrorDiff) < SPIN_DIFF_0_1KPH) )
                                {
			ltcss16GoodTargetTrackingCnt++;
			ltcss16GoodTargetTrackingCnt = LCTCS_s16ILimitMaximum(ltcss16GoodTargetTrackingCnt, (int16_t)L_U16_TIME_10MSLOOP_20S);
            }    
            else
            {	
			ltcss16GoodTargetTrackingCnt--;
			ltcss16GoodTargetTrackingCnt = LCTCS_s16ILimitMinimum(ltcss16GoodTargetTrackingCnt, 0);
            }
        }    
        else
        {	
		if 	( (McrAbs(lts16WheelSpinErrorDiff) > SPIN_DIFF_0_2KPH) || (McrAbs(lts16WheelSpinError) > VREF_2_KPH) )
		{	/* Reset Condition */
			ltcss16GoodTargetTrackingCnt = 0;
        }        
        else
        {
			ltcss16GoodTargetTrackingCnt++;
                        }
		ltcss16GoodTargetTrackingCnt = LCTCS_s16ILimitRange(ltcss16GoodTargetTrackingCnt, 0, (int16_t)L_U16_TIME_10MSLOOP_20S);
        }		
        
	if(ltcss16GoodTargetTrackingCnt >= U8_TCS_GD_TRACK_SET_TIME)
        {	
		GOOD_TARGET_TRACKING_MODE = 1;
        }    
        else
        {	
		GOOD_TARGET_TRACKING_MODE = 0;
    }
        }

void LCTCS_vIncTargetSpin(void)
        {
	/*=============================================================================*/
	/* Purpose : Spin Error가 1KPH 이내이고 lts16WheelSpinErrorDiff가 8(0.1KPH)    */
	/*           인 경우, Target Spin을 잘 추종하고 있는 것으로 판단하고 가속력    */
	/*           향상 및 제어 조기 종료를 위하여 Target Spin을 증가				   */
	/*           (선회 Gain 적용 시는 Target spin 적용하지 않음)				   */
	/*           Mini Tire 감지시 Target Spin을 증가							   */
	/* Input Variable : GAIN_ADD_IN_TURN, lts16WheelSpinError,   				   */
	/*                  lts16WheelSpinErrorDiff, TCS_MINI_TIRE_SUSPECT, vref5      */
	/* Output Variable : ltcss16GoodTargetTrackingCnt, lts16TargetWheelSpin		   */
	/* 2006. 10. 30 By Jongtak												       */
	/*=============================================================================*/

	if (GOOD_TARGET_TRACKING_MODE == 1)
            {
		if (ETCS_ON == 1)
		{	/* Homo-mu */
			lts16TargetWheelSpin = LCESP_s16IInter2Point(ltcss16GoodTargetTrackingCnt, (int16_t)U8_TCS_GD_TRACK_SET_TIME, lts16TargetWheelSpin,
														 L_U16_TIME_10MSLOOP_5S,    lts16TargetWheelSpin + U8_TARGET_SPIN_INC_GT_HOMO);
            }    
            else
		{	/* Spit-mu */
			lts16TargetWheelSpin = LCESP_s16IInter2Point(ltcss16GoodTargetTrackingCnt, (int16_t)U8_TCS_GD_TRACK_SET_TIME, lts16TargetWheelSpin,
														 L_U16_TIME_10MSLOOP_5S,    lts16TargetWheelSpin + U8_TARGET_SPIN_INC_GT_SPLT);
            }
        }    
        else
        {	
		;	/* lts16TargetWheelSpin 변화 없음 */
        } 
        
	/* Mini Tire 감지 시 Target ΔV 증대 */
	if (lctcsu1InvalidWhlSpdBfRTADetect == 1)
        {	
		/* 30% Mini Tire 고려 : tcs_tempW1 = (tcs_tempW1 * 3) / 10 / 2(Spin은 좌우 평균이므로) */
		tcs_tempW1 = (lctcss16WhlSpdreferanceByMiniT / 2);
		lts16TargetWheelSpin = lts16TargetWheelSpin + tcs_tempW1;
        }        		
	else if (lctcsu1WhlSpdCompByRTADetect == 1)
        {
		tcs_tempW1 = lctcss16WhlSpdreferanceByMiniT;
		lts16TargetWheelSpin = lts16TargetWheelSpin + tcs_tempW1;
        }		
 	else{}
        
    /* __4WDStuckByDiagonalWheelSpin */
	if (lctcsu1Dct4WDStuckByDiaWhlSpin==1)
        {	
		lts16TargetWheelSpin = (int16_t)U8_T_SPIN_INC_IN_DIA_WHL_SPIN;
        }    
        else
        {	
            ;
        }
    
	lts16TargetWheelSpin = LCTCS_s16ILimitRange(lts16TargetWheelSpin, VREF_0G125_KPH, VREF_200_KPH);
															 							
}

#if __VDC	/* TAG4 */
void LCTCS_vCalTargetSpinInTurn(void)
            {
	/*=============================================================================*/
	/* Purpose : 선회시 Target Wheel Spin 계산      	 	   					   */
	/* Input Variable : delta_yaw_thres, delta_yaw_first, det_alatm				   */
	/*					VDC_DISABLE_FLG											   */
	/* Output Variable : ltcsu8TargetSpinReduceThInTurn, lts16TargetWheelSpin	   */
	/*                   ltcss16DelYawThforTSpinReduce, ltcss16TargetSpinReduceAyTh*/
	/* 2006. 8. 3 By Jongtak												       */
	/*=============================================================================*/

	/* Delta Yaw에 의한 Target Spin 감소량 계산 */
  #if __TCS_REFER_TARGET_TORQ_CTL
	delta_yaw_first_rate[2] = delta_yaw_first_rate[1];
	delta_yaw_first_rate[1] = delta_yaw_first_rate[0];
	delta_yaw_first_rate[0] = (int16_t)McrAbs(delta_yaw_first);
	delta_yaw_first_diff = ((int16_t)McrAbs(delta_yaw_first)) - delta_yaw_first_rate[2];	
	
	alat_ems_rate[2] = alat_ems_rate[1];
	alat_ems_rate[1] = alat_ems_rate[0];
	alat_ems_rate[0] = (int16_t)McrAbs(alat_ems);
	alat_ems_diff = ((int16_t)McrAbs(alat_ems)) - alat_ems_rate[2];	
	#endif /*__TCS_REFER_TARGET_TORQ_CTL*/
	
	if(FTCS_ON==1)
    	{    	
	    ltcsu8TargetSpinReduceThInTurn = (uint8_t)LCESP_s16IInter4Point(vref5, VREF_3_KPH, 20, VREF_15_KPH, 20, VREF_40_KPH, 15, VREF_50_KPH, 15);
            }    
            else
            {	
		ltcsu8TargetSpinReduceThInTurn = (uint8_t)LCESP_s16IInter4Point(vref5, VREF_3_KPH, 20, VREF_15_KPH, 10, VREF_40_KPH, 8, VREF_50_KPH, 6);
		#if __TCS_REFER_TARGET_TORQ_CTL
		lctcsu8AddYawThrbyYawDiff = (uint8_t)LCESP_s16IInter3Point(delta_yaw_first_diff, (int16_t)S8_TARGET_SPIN_YAW_RATE_DIFF_1, 15,
																       		    	     (int16_t)S8_TARGET_SPIN_YAW_RATE_DIFF_2, 12,
																                         (int16_t)S8_TARGET_SPIN_YAW_RATE_DIFF_3, 8);
		
		ltcsu8TargetSpinReduceThInTurn = (uint8_t)((int16_t)ltcsu8TargetSpinReduceThInTurn*lctcsu8AddYawThrbyYawDiff)/10;														  
		#endif /*__TCS_REFER_TARGET_TORQ_CTL*/						 										 		
            }
        				
	ltcss16DelYawThforTSpinReduce = (delta_yaw_thres * ltcsu8TargetSpinReduceThInTurn) / 10;


	/* 속도, 노면 별 Target Spin Tuning 구조 */
	tcs_tempW0 = LCTCS_s16IInterpDepAy4by3( (int16_t)U8_TARGET_SPIN_IN_TURN_0_0, (int16_t)U8_TARGET_SPIN_IN_TURN_0_1, (int16_t)U8_TARGET_SPIN_IN_TURN_0_2,
											(int16_t)U8_TARGET_SPIN_IN_TURN_1_0, (int16_t)U8_TARGET_SPIN_IN_TURN_1_1, (int16_t)U8_TARGET_SPIN_IN_TURN_1_2,
											(int16_t)U8_TARGET_SPIN_IN_TURN_1_0, (int16_t)U8_TARGET_SPIN_IN_TURN_1_1, (int16_t)U8_TARGET_SPIN_IN_TURN_1_2,
											(int16_t)U8_TARGET_SPIN_IN_TURN_2_0, (int16_t)U8_TARGET_SPIN_IN_TURN_2_1, (int16_t)U8_TARGET_SPIN_IN_TURN_2_2);
	
	tcs_tempW1 = LCTCS_s16ILimitRange( (McrAbs(delta_yaw_first) - ltcss16DelYawThforTSpinReduce), 0, YAW_1DEG);

	tcs_tempW3 = LCESP_s16IInter2Point(tcs_tempW1, 0	   , 100,
												   YAW_1DEG, tcs_tempW0);  											   

	/* 횡가속도에 의한 Target Spin 감소량 계산 */
	if (FTCS_ON==1)
    	{   
		ltcss16TargetSpinReduceAyTh = LAT_0G15G;
		lctcsu8AddAyThrbyWstrAyDiff = LAT_0G05G;
        }    
        else
        {	
		ltcss16TargetSpinReduceAyTh = LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED1, LAT_0G1G,
																 (int16_t)U16_TCS_SPEED2, LAT_0G1G,
						 										 (int16_t)U16_TCS_SPEED3, LAT_0G05G);		
	#if __TCS_REFER_TARGET_TORQ_CTL
		lctcsu8AddAyThrbySteer = (uint8_t)LCESP_s16IInter3Point((int16_t)McrAbs(wstr), WSTR_0_DEG,  LAT_0G1G,
											 				                           WSTR_15_DEG, LAT_0G07G,
												  				                       WSTR_30_DEG, LAT_0G05G);
	
		lctcsu8AddAyThrbyAyDiff = (uint8_t)LCESP_s16IInter3Point(alat_ems_diff, (int16_t)S8_TARGET_SPIN_ALAT_DIFF_1, 15,
															       				(int16_t)S8_TARGET_SPIN_ALAT_DIFF_2, 12,
																       			(int16_t)S8_TARGET_SPIN_ALAT_DIFF_3, 8);
	
		lctcsu8AddAyThrbyWstrAyDiff = (uint8_t)((int16_t)lctcsu8AddAyThrbySteer*lctcsu8AddAyThrbyAyDiff)/10;														  
	#else
		lctcsu8AddAyThrbyWstrAyDiff = LAT_0G05G;
	#endif /*__TCS_REFER_TARGET_TORQ_CTL*/
        } 
        
	tcs_tempW5 = LCESP_s16IInter2Point((int16_t)McrAbs(alat_ems), ltcss16TargetSpinReduceAyTh, 100,
										                 ltcss16TargetSpinReduceAyTh+lctcsu8AddAyThrbyWstrAyDiff,  tcs_tempW0);
											     
	if(FTCS_ON==1)
        {	
	    ltcss16TargetSpinReduceSteerTh = LCESP_s16IInter4Point(vref, VREF_10_KPH, S16_DETECT_STEER_SPLIT_LOW_SPD,   /* 140~160deg steer */
		                                                             VREF_20_KPH, S16_DETECT_STEER_SPLIT_LOWMED_SPD,   /* 35~45deg steer */
		                                                             VREF_50_KPH, S16_DETECT_STEER_SPLIT_MED_SPD,   /* 25~35deg steer */
    										                         VREF_80_KPH, S16_DETECT_STEER_SPLIT_HIGH_SPD);  /* 25~30deg steer */
        }        
        else
        {
		ltcss16TargetSpinReduceSteerTh = LCESP_s16IInter4Point(vref, VREF_10_KPH, S16_DETECT_STEER_HOMO_LOW_SPD,   /* 80~90deg steer */
		                                                             VREF_20_KPH, S16_DETECT_STEER_HOMO_LOWMED_SPD,   /* 35~45deg steer */
		                                                             VREF_50_KPH, S16_DETECT_STEER_HOMO_MED_SPD,   /* 15~20deg steer */
    										                         VREF_80_KPH, S16_DETECT_STEER_HOMO_HIGH_SPD);  /* 15~20deg steer */
        }		
        
	if( ltcss16TargetSpinReduceSteerTh <= WSTR_20_DEG )
        {	
		ltcss16TargetSpinReduceSteerTh = WSTR_20_DEG;
        }    
        else
        {	
            ;
        }
                
	/* Delta Yaw에 의한 Target Spin 감소량과 횡가속도에 의한 Target Spin 감소량을
	   비교하여 작은 값을 Target Spin으로 설정									  */
	/* yaw_sw_err_flg, lg_sw_err_flg, yaw_hw_err_flg, lg_hw_err_flg => 0이면 고장 */
	if ( (fu1YawErrorDet==0) && (fu1AyErrorDet==0)	)
	/*if ( (yaw_sw_err_flg==1) && (yaw_hw_err_flg==1) && (lg_sw_err_flg==1) && (lg_hw_err_flg==1)	)*/
        {	
		if((McrAbs(det_alatm)<=LAT_0G15G)&&(GAIN_ADD_IN_TURN==1))
    {
			tcs_tempW6 = tcs_tempW0;
        }    
        else
        {	
			tcs_tempW6 = LCTCS_s16IFindMinimum(tcs_tempW3, tcs_tempW5);
    }	
    }
    else
    {
		tcs_tempW6 = 100;
	}
	
	
	if ( (vref > VREF_5_KPH)
	   #if TORQ_RECOVERY_DEPEND_ON_STEER
		 && (lctcsu1SteerToZero==0)
       #endif	/* #if TORQ_RECOVERY_DEPEND_ON_STEER */
       )
        {
		lts16TargetWheelSpin = ( lts16TargetWheelSpin * tcs_tempW6 ) / 100;
        }
        else
	{}	/* Deep Snow 탈출시 Delta yaw에 의한 Target Spin 감소로 Stall 유발되어 5KPH 이하에서는 Target Spin 감소시키지 않음 */
}

void LCTCS_vDetectFadeModeAfterETO(void)
        {
	/*=============================================================================*/
	/* Purpose : Med-μ / High-μ 에서 ESP 제어후 Fade Mode 진입 여부 결정		   */
	/*           (ESP 제어 후 Torque Fast Recovery를 위함)		      	 	       */
	/* Input Variable : det_alatm, alat, ETO_SUSTAIN_3SEC, ESP_TCS_ON, 			   */
    /*	                abs_wstr_dot , lts16WheelSpin, delta_yaw_first, 		   */
    /*                  ltcss16DelYawThforTSpinReduce, cal_torq, drive_torq	       */
    /*  				lts16TargetWheelSpin		   						       */
	/* Output Variable : TORQUE_FAST_RECOVERY_AFTER_ETO, ltcsu8TorqFastRecoveryCnt */
	/*                   	   								                       */
	/* 2006. 8. 5 By Jongtak												       */
	/*=============================================================================*/
	tcs_tempW1 = McrAbs(det_alatm - McrAbs(alat));
	if (TORQUE_FAST_RECOVERY_AFTER_ETO==0)
            {
		if ( (ETO_SUSTAIN_3SEC==1) && (ESP_TCS_ON==0) )
                {
			/* 조건에 부합한 시간 Counting */
			if ( (abs_wstr_dot<1000)
				 && (lts16WheelSpin<VREF_3_KPH)
				 && (lts16WheelSpinErrorDiff<12)	/* 0.15KPH/scan */
				 && (tcs_tempW1 > LAT_0G1G)
				 && ((McrAbs(delta_yaw_first)<ltcss16DelYawThforTSpinReduce))
				 && (cal_torq<drive_torq) )
			{
				ltcsu8TorqFastRecoveryCnt++;
                }
                else
                {
				if (ltcsu8TorqFastRecoveryCnt>0) ltcsu8TorqFastRecoveryCnt--;
				else ltcsu8TorqFastRecoveryCnt = 0;
            }

			/* TORQUE_FAST_RECOVERY_AFTER_ETO Set */
/* 07SWD #4	: Tuning parameter 이름 변경*/
			if (ltcsu8TorqFastRecoveryCnt>=(uint8_t)U8_TORQ_FAST_UP_AFTER_EEC_SCAN)
            {
				TORQUE_FAST_RECOVERY_AFTER_ETO = 1;
                }
                else
                {
				TORQUE_FAST_RECOVERY_AFTER_ETO = 0;
                }
                }
                else
                {
			TORQUE_FAST_RECOVERY_AFTER_ETO = 0;
                }
            }
            else
            {
		if ((ETO_SUSTAIN_3SEC==0) || ( (lts16WheelSpin>=VREF_3_KPH)&&(lts16WheelSpinErrorDiff>=12)))
		{	/* 즉시 TORQUE_FAST_RECOVERY_AFTER_ETO Clear 조건 */
			TORQUE_FAST_RECOVERY_AFTER_ETO = 0;
			ltcsu8TorqFastRecoveryCnt = 0;
            }
        else
		{
			/* TORQUE_FAST_RECOVERY_AFTER_ETO Clear 조건 */
			if ( (abs_wstr_dot<1000)
				 && (lts16WheelSpin<VREF_3_KPH)
				 && (lts16WheelSpinErrorDiff<12)	/* 0.15KPH/scan */
		     	 && (tcs_tempW1 > LAT_0G1G)
			 	 && ((McrAbs(delta_yaw_first)<ltcss16DelYawThforTSpinReduce)) )
			{
				tcs_tempW2 = U8_TORQ_FAST_UP_AFTER_EEC_SCAN / 3;
				ltcsu8TorqFastRecoveryCnt = (uint8_t)LCTCS_s16ILimitRange(tcs_tempW2, L_U8_TIME_10MSLOOP_40MS, L_U8_TIME_10MSLOOP_50MS);
        }
        else
        {
				if (ltcsu8TorqFastRecoveryCnt>0) ltcsu8TorqFastRecoveryCnt--;
				else ltcsu8TorqFastRecoveryCnt = 0;
        }

			/* TORQUE_FAST_RECOVERY_AFTER_ETO Clear */
			if (ltcsu8TorqFastRecoveryCnt==0)
        {
				TORQUE_FAST_RECOVERY_AFTER_ETO = 0;
        }
        else
        {
				TORQUE_FAST_RECOVERY_AFTER_ETO = 1;
        }
		}
    }
}


void LCTCS_vCalPDGainInTurn(void)
            {
	if (CYCLE_2ND==0)
                {
		LCTCS_vCalPDGainInTurnFor1stCyc();		
                }
                else
                {
		LCTCS_vCalPDGainInTurnFor2ndCyc();
                }
            }

void LCTCS_vCalFinalPDGain(void)
            {
	/*=============================================================================*/
	/* Purpose : 직진시와 선회시의 P / D Gain을 종합한 Final  P / D Gain 계산      */
	/*           ESP Engine제어 종료후 Fade Mode 작동 시 P Gain 증대			   */
	/* Input Variable : TORQUE_FAST_RECOVERY_AFTER_ETO, det_alatm, alat,		   */
	/*                  lts16PGain, lts16DGain, lts16AddPGainInTurn   			   */
	/* Output Variable : lts16PGain, lts16DGain			 						   */
	/* 2006. 8. 17 By Jongtak												       */
	/*=============================================================================*/
	if ( (TORQUE_FAST_RECOVERY_AFTER_ETO==1) && (lts16WheelSpinError < 0) )
                {
		tcs_tempW1 = McrAbs(det_alatm - McrAbs(alat));
		tcs_tempW2 = LCESP_s16IInter2Point(tcs_tempW1, LAT_0G1G, (int16_t)U8_P_GAIN_INC_AFTER_EEC_0G1
													 , LAT_0G8G, (int16_t)U8_P_GAIN_INC_AFTER_EEC_0G8);
		lts16PGain = lts16PGain + LCTCS_s16IFindMaximum(lts16AddPGainInTurn, tcs_tempW2);
		lts16DGain = lts16DGain;  
                }
                else
                {
		lts16PGain = lts16PGain + lts16AddPGainInTurn;
		lts16DGain = lts16DGain + lts16AddDGainInTurn;
                }

	if (lctcsu1RoughRoad==1)
	{
		/* 07SWD #2	: rough road amplitude 연동구조*/
		tcs_tempW3 = LCTCS_s16ILimitMinimum((int16_t)U8_D_GAIN_COMP_RATIO_IN_RR, 1);
		lts16DGain = lts16DGain / tcs_tempW3;
            }
            else
            {
		;	/* Do Nothing */
        }
	
	if ( (lctcsu1InvalidWhlSpdBfRTADetect == 1) && (lts16WheelSpinError < 0) )
        {
		lts16PGain = LCTCS_s16ILimitMinimum(lts16PGain, 5);
		lts16DGain = 0; /* Phase 5,6 에서는 Torque 저감 허용 안함 */		
        }
	else{}

	if ( (FLAG_BANK_DETECTED==1) && (lts16WheelSpinError < 0) )
        {
		/* BANK 감지한 경우 Phase 5,6에서 0.3%/Scan 이상의 토크 증가량 확보 */
		tcs_tempW4 = ( (3 * (int16_t)U16_TCS_GAIN_SCALING_FACTOR) / McrAbs(lts16WheelSpinError) ) + 1;
		lts16PGain = LCTCS_s16ILimitMinimum(lts16PGain, tcs_tempW4);
        }
        else
        {
            ;
        }

  #if __MY08_TCS_DCT_H2L
  	LCTCS_vCompensateGainForH2L();
  #endif	/* #if __MY08_TCS_DCT_H2L */	  	

  #if __TCS_PREVENT_FAST_RPM_INCREASE  	
	LCTCS_vIncGainInFastRPMInc();
  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */

	LCTCS_vGainDecreaseInGearShift();

  #if __TCS_BUMP_DETECTION  	
  	LCTCS_vCompensateGainForBump();
  #endif	/* __TCS_BUMP_DETECTION */
    	    	
	LCTCS_vPreventLongIntervention();

	/* Gain Minimum Value Limitation */
	lts16PGain = LCTCS_s16ILimitRange(lts16PGain, 1, 100);

	if (lctcsu1InvalidWhlSpdBfRTADetect == 1)
    {
		lts16DGain = LCTCS_s16ILimitMinimum(lts16DGain, 0);
    }
    else
    {
		lts16DGain = LCTCS_s16ILimitMinimum(lts16DGain, 1);
		}
    }


void LCTCS_vPreventLongIntervention(void)
{
	/*=============================================================================*/
	/* Purpose : Engine Intervention이 너무 오래 지속되는 것을 방지				   */
	/* Input Variable : delta_yaw_first, lts16WheelSpin, lts16TargetWheelSpin	   */
	/*                  lts16PGain												   */
	/* Output Variable : lts16PGain						 						   */
	/* 2007. 4. 26 By Jongtak												       */
	/*=============================================================================*/

	/* Back-up Logic, gear shift 시점 고려 요망*/
	lctcsu1BackupTorqActivationold = lctcsu1BackupTorqActivation;

	if ( (McrAbs(delta_yaw_first) < S16_TCS_BACKUP_ENTER_YAW) 
		&& (lts16WheelSpin < (lts16TargetWheelSpin/2)) 
		&& (slope_eng_rpm < U8_TCS_BACKUP_ENTER_RPM_SLOPE)
        && ((det_abs_wstr < ltcss16TargetSpinReduceSteerTh)&&(McrAbs(alat_ems) < S16_TCS_BACKUP_ENTER_AY))
        && (lts16WheelSpin < U8_TCS_BACKUP_ENTER_THR1) && (McrAbs(lts16WheelSpinErrorDiff) < U8_TCS_BACKUP_ENTER_THR2) )
    {
		ltcss16LowTorqueLevelCnt++;

		/* tcs_tempW4 : Spin이 0인 경우 0.1%/Scan의 Torque Rise Rate를 만들어 내는 P gain */
		if( ((int16_t)U16_TCS_GAIN_SCALING_FACTOR%lts16TargetWheelSpin)>0 )
		{
			tcs_tempW4 = ((int16_t)U16_TCS_GAIN_SCALING_FACTOR/lts16TargetWheelSpin) + 1;
		}
		else
		{
			tcs_tempW4 = ((int16_t)U16_TCS_GAIN_SCALING_FACTOR/lts16TargetWheelSpin);
    	}

		/* Torque Control Pattern */
		if 		(ltcss16LowTorqueLevelCnt<S16_TCS_BACKUP_RISE_START_TIME)
		{
			/* Do Nothing */
        }
		else if (ltcss16LowTorqueLevelCnt<(S16_TCS_BACKUP_RISE_START_TIME + (int16_t)U8_TCS_BACKUP_1ST_RISE_DURATION) )
        {
			tcs_tempW4 = tcs_tempW4 * U8_TCS_BACKUP_1ST_RISE_RATE;
			lts16PGain = LCTCS_s16IFindMaximum(tcs_tempW4,lts16PGain);
			lctcsu1BackupTorqActivation = 1;
        }
		else if (ltcss16LowTorqueLevelCnt<(S16_TCS_BACKUP_RISE_START_TIME + (int16_t)U8_TCS_BACKUP_1ST_RISE_DURATION + (int16_t)U8_TCS_BACKUP_MONITOR_TIME) )
            {	
			/* Do Nothing */
    }
    else
    {
			if (cal_torq <= drive_torq)
			{
			tcs_tempW4 = tcs_tempW4 * U8_TCS_BACKUP_LAST_RISE_RATE;
			lts16PGain = LCTCS_s16IFindMaximum(tcs_tempW4,lts16PGain);
    }
			else{}
		}
        }        		
        else
    {
		lctcsu1BackupTorqActivation = 0;
		if (ltcss16LowTorqueLevelCnt >= S16_TCS_BACKUP_RISE_START_TIME)
        {	
			ltcss16LowTorqueLevelCnt = S16_TCS_BACKUP_RISE_START_TIME - 1;
    }
    else
    {
			ltcss16LowTorqueLevelCnt = ltcss16LowTorqueLevelCnt - L_U8_TIME_10MSLOOP_200MS;
		}
    }
	ltcss16LowTorqueLevelCnt = LCTCS_s16ILimitRange(ltcss16LowTorqueLevelCnt, 0, L_U16_TIME_10MSLOOP_20S);
}

void LCTCS_vIncPGainInHighSideSpin(void)
{
	/*=============================================================================*/
	/* Purpose : Split-mu 제어 중 High-side Spin 발생시 High-side Spin의 크기에    */
	/*           비례하여 P Gain을 증가 시키고, D Gain을 0으로 Setting 하여 	   */
	/*           High-side Spin 발생시 토크 저감 유도							   */
	/* Input Variable : FTCS_ON, lts16WheelSpinError, vel_f_small,		   		   */
	/*                  lts16PGain, lts16DGain,    			   					   */
	/* Output Variable : lts16PGain, lts16DGain			 						   */
	/* 2006. 10. 30 By Jongtak												       */
	/*=============================================================================*/
	/* 07SWD #8 : Torque Compensation in High side spin*/

	if (((FL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(FR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)
	  ||(RL_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable)||(RR_TCS.lctcsu8StatOneWhlOnHghMu==TCSHighMuUnStable))
	  &&(FTCS_ON == 1)&&(CYCLE_2ND == 1)) /* next event add on 14.08 */
        {
		/* P Gain Compensation */
		tcs_tempW5 = lts16PGain;
		tcs_tempW6 = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_0, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_1, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_2, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_0_3,
												   (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_0, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_1, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_2, (int16_t)U8_ADD_P_GAIN_HIGH_SIDE_SPIN_1_3);

		if (lts16WheelSpinError > 0)
    {
			tcs_tempW7 = lts16PGainForFTCS + tcs_tempW6;
			lts16PGain = LCTCS_s16IFindMaximum(tcs_tempW5,tcs_tempW7);
    }
            else
    {
			lts16PGain = lts16PGainForFTCS;
    }
        }    
	else {} /* Do Nothing */
    }

void LCTCS_vCount1SecAfterETO(void)
        {
	/*=============================================================================*/
	/* Purpose : ESP_TCS_ON이 Clear 된 후 1초 Set 상태가 지속되는 Flag 생성		   */
	/* 			 (Gear Shift Inhibition 신호 / TOD 제어 신호의 연속성을 위해 사용) */
	/* Input Variable : ESP_TCS_ON												   */
	/* Output Variable : ltcsu81SecCntAfterETO, ETO_SUSTAIN_1SEC                   */
	/* 2006. 8. 5 By Jongtak												       */
	/*=============================================================================*/
	if(ESP_TCS_ON==1)
            {
		ltcsu81SecCntAfterETO = L_U8_TIME_10MSLOOP_1000MS;
		ETO_SUSTAIN_1SEC = 1;
            }
        else
            {
		if (ltcsu81SecCntAfterETO>0)
        {	
			ltcsu81SecCntAfterETO--;
            }
            else
            {
			ETO_SUSTAIN_1SEC = 0;
			ltcsu81SecCntAfterETO = 0;
            }
        }
}

void LCTCS_vCount3SecAfterETO(void)
        {
	/*=============================================================================*/
	/* Purpose : ESP_TCS_ON이 Clear 된 후 3초 Set 상태가 지속되는 Flag 생성		   */
	/* 			 (ESP 엔진 제어후 Torque Fast Recovery를 위해 사용)                */
	/* Input Variable : ESP_TCS_ON												   */
	/* Output Variable : ltcsu163SecCntAfterETO, ETO_SUSTAIN_3SEC                   */
	/* 2006. 8. 5 By Jongtak												       */
	/*=============================================================================*/
	if(ESP_TCS_ON==1)
        {
		ltcsu163SecCntAfterETO = L_U16_TIME_10MSLOOP_3S;
		ETO_SUSTAIN_3SEC = 1;
        }
            else
            {	
		if (ltcsu163SecCntAfterETO>0)
		{
			ltcsu163SecCntAfterETO--;
    }
    else
    {
			ETO_SUSTAIN_3SEC = 0;
			ltcsu163SecCntAfterETO = 0;
    }
	}
}

void LCTCS_vCount6SecAfterETO(void)
    {
	/*=============================================================================*/
	/* Purpose : ESP_TCS_ON이 Clear 된 후 6초 Set 상태가 지속되는 Flag 생성		   */
	/* Input Variable : ESP_TCS_ON												   */
	/* Output Variable : ltcsu166SecCntAfterETO, ETO_SUSTAIN_6SEC                  */
	/* 2007. 1. 12 By Jongtak												       */
	/*=============================================================================*/
	if(ESP_TCS_ON==1)
        {
		ltcsu166SecCntAfterETO = L_U16_TIME_10MSLOOP_6S;	/* 6초 : 857*0.007 */
		ETO_SUSTAIN_6SEC = 1;
        }
        else
        {
		if (ltcsu166SecCntAfterETO>0)
        {
			ltcsu166SecCntAfterETO--;
        }
        else
        {
			ETO_SUSTAIN_6SEC = 0;
			ltcsu166SecCntAfterETO = 0;
        }
        }
}
    
#if __COMPETITIVE_EXIT_CONTROL
void LCTCS_vCountTimeAfterETO(void)
{
	if(ESP_TCS_ON==1)
	{
		ltcsu16TimeCntAfterETO = U16TCSGearInhibitScanAftEEC;	/*  */
		ETO_SUSTAIN_TIME = 1;
	}
	else
	{
		if (ltcsu16TimeCntAfterETO>0)
		{
			ltcsu16TimeCntAfterETO--;
		}
		else
		{
			ETO_SUSTAIN_TIME = 0;
			ltcsu16TimeCntAfterETO = 0;
		}
	}
}
#endif

void LCTCS_vSustainCtlHomo2Split(void)
        {
	/* Homo-mu 제어 중 Split-mu 제어 변환시 지속 Flag */
	if (ltcsu1Homo2SplitAct==1)
        {
		ltcsu8CntAfterHomo2SPlit = (uint8_t)LCTCS_s16ILimitMinimum((int16_t)ltcsu8CntAfterHomo2SPlit,1);
		if (ltcsu8CntAfterHomo2SPlit == 1)
            {
			ltcsu1Homo2SplitAct = 0;
        }
        else
        {
			ltcsu8CntAfterHomo2SPlit--;
        }
    }
    else
    {
			;
        }
            }
        
void LCTCS_vDecideGainAddInTurn(void)
            {
	/* Split-mu 제어 중에는 선회 Gain 적용되지 않음 */
  	/* det_alatc 고려 : 작은 보타의 경우는 선회 Gain 적용되지 않음 */
	/* yaw_sw_err_flg, lg_sw_err_flg, yaw_hw_err_flg, g_hw_err_flg => 0이면 고장 */

	tcs_tempW0 = LCTCS_s16ILimitMinimum(S16_TCS_TURN_DET_BY_ALAT_TH, LAT_0G1G); /*14.11.06*/

	if ( (ETCS_ON==1) && (fu1YawErrorDet==0) && (fu1AyErrorDet==0) &&
	     ( ((ETO_SUSTAIN_1SEC==1) && (det_abs_wstr > ltcss16TargetSpinReduceSteerTh)) ||
	        (McrAbs(nor_alat)>tcs_tempW0)) )
                {
		GAIN_ADD_IN_TURN = 1;		
                }
        else
                {
		GAIN_ADD_IN_TURN = 0;
                }
                }
        
void LCTCS_vDecideHomoMuPriority(void)
                {
	/* Homo-mu 감지를 우선으로 하는 선회 조건 */
	if ( (fu1YawErrorDet==0) && (fu1AyErrorDet==0) && (fu1StrErrorDet==0)
		 && ( (McrAbs(yaw_out)>S16_TCS_HOMO_TRAN_YAWRATE) || (McrAbs(nor_alat)>(int16_t)U8_TCS_CTL_START_HOMO_AY)
		 ) && (vref5 >= U8_TCS_HOMO_TRAN_MAX_SPD) )
	{	/* Yaw rate : 차량의 거동, nor_alat : 운전자의 의지 */
		ltcsu1HomoMuCtlHavePriority = 1;
                }
                else
                {
		ltcsu1HomoMuCtlHavePriority = 0;
                }
                
            }
    
#endif		/* TAG4 */
	
void LCTCS_vCalculateError(void)
            {
	/*=============================================================================*/
	/* Purpose : Error & Error의 변화량 계산      	 	   						   */
	/* Input Variable : lts16TargetWheelSpin, lts16WheelSpin                       */
	/* Output Variable : lts16WheelSpinError, lts16WheelSpinErrorOld               */
	/*                   lts16WheelSpinErrorDiff 								   */
	/* 				     ltcss16WheelSpinErrDifMax, ltcss16WheelSpinErrDifMin	   */
	/* 2006. 4. 4 By Jongtak												       */
	/* 2007.10.19 By Jongtak ltcss16WheelSpinErrDifMax, ltcss16WheelSpinErrDifMin  */
	/*                       계산 추가											   */
	/*=============================================================================*/
	/*lts16WheelSpinErrorOld = lts16WheelSpinError;*/
    lts16WheelSpinFiltOld = lts16WheelSpinFilt;

	lts16WheelSpinError = lts16WheelSpin - lts16TargetWheelSpin;
	lts16WheelSpinError = LCTCS_s16ILimitMaximum(lts16WheelSpinError, VREF_40_KPH);
    tcs_tempW2 = LCTCS_s16ILimitRange(lts16WheelSpin, VREF_0G125_KPH, VREF_40_KPH);
    tcs_tempW2 = tcs_tempW2*100;
  if(!AUTO_TM)
    {
  	tcs_tempW1=L_U8FILTER_GAIN_10MSLOOP_3HZ;  /* 8hz:35 , 5hz:21  , 3hz : 17*/
    }
    else
    {
  	tcs_tempW1=L_U8FILTER_GAIN_10MSLOOP_8HZ;  /* 8hz:35 , 5hz:21  , 3hz : 17*/
        }
    lts16WheelSpinFilt = LCESP_s16Lpf1Int( tcs_tempW2, lts16WheelSpinFiltOld, (uint8_t)tcs_tempW1);
    /*lts16WheelSpinError = lts16WheelSpinFilt/100 - lts16TargetWheelSpin;*/
	lts16WheelSpinErrorDiff = (lts16WheelSpinFilt - lts16WheelSpinFiltOld)/10;
    }

static void LCTCS_vDecideTractionPhase(void)
    {
	if (CYCLE_2ND == 0)
            {
		LCTCS_vDecideTorqRecoveryLevel();	

		if (lts16WheelSpinError > 0)
                {
			if (lts16WheelSpinErrorDiff > 0)
        {
				lctcsu8TCSEngCtlPhase = 1;
				if(lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE)  /* 강제 2nd cycle 전환 */
            {
    				lctcsu8TCSEngCtlPhase = 21;
    		    	lctcsu8PosSpinErrDiffCnt1++;
    		    	lctcsu8PosSpinErrDiffCnt1 = (uint8_t)LCTCS_s16IFindMinimum((int16_t)lctcsu8PosSpinErrDiffCnt1,L_U8_TIME_10MSLOOP_70MS);
    		    	if(lctcsu8PosSpinErrDiffCnt1>=L_U8_TIME_10MSLOOP_40MS)
                {
    			    	lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
                }
                else
                {
                    ;
                }
            }
        		else /* phase 1 */
                {
                    ;
                }
            }
			else
			{
				if (ETCS_ON == 1)
            {
					if ( (GAIN_ADD_IN_TURN == 1)||(ltcss16ETCSEnterExitFlags == 4) )	        
			   			/* In a turn */ 
                {
						lts16TorqRiseThr = (int16_t)LCTCS_s16IInterpDepAy4by3( (int16_t)U8_ENTER_PHASE2_THR_TURN_0_0, (int16_t)U8_ENTER_PHASE2_THR_TURN_0_1, (int16_t)U8_ENTER_PHASE2_THR_TURN_0_2,
			    		                           		                   (int16_t)U8_ENTER_PHASE2_THR_TURN_1_0, (int16_t)U8_ENTER_PHASE2_THR_TURN_1_1, (int16_t)U8_ENTER_PHASE2_THR_TURN_1_2,
			    		                           		                   (int16_t)U8_ENTER_PHASE2_THR_TURN_1_0, (int16_t)U8_ENTER_PHASE2_THR_TURN_1_1, (int16_t)U8_ENTER_PHASE2_THR_TURN_1_2,
			            		                                           (int16_t)U8_ENTER_PHASE2_THR_TURN_2_0, (int16_t)U8_ENTER_PHASE2_THR_TURN_2_1, (int16_t)U8_ENTER_PHASE2_THR_TURN_2_2);
        }
        else
        {
						lts16TorqRiseThr = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_ENTER_PHASE2_THRESHOLD_0_0, (int16_t)U8_ENTER_PHASE2_THRESHOLD_0_1, (int16_t)U8_ENTER_PHASE2_THRESHOLD_0_2, (int16_t)U8_ENTER_PHASE2_THRESHOLD_0_3,
						                                                 (int16_t)U8_ENTER_PHASE2_THRESHOLD_1_0, (int16_t)U8_ENTER_PHASE2_THRESHOLD_1_1, (int16_t)U8_ENTER_PHASE2_THRESHOLD_1_2, (int16_t)U8_ENTER_PHASE2_THRESHOLD_1_3);
        }
    }
    else
    {
					lts16TorqRiseThr = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_FTCS_ENTER_PHASE2_THR_0_0, (int16_t)U8_FTCS_ENTER_PHASE2_THR_0_1, (int16_t)U8_FTCS_ENTER_PHASE2_THR_0_2, (int16_t)U8_FTCS_ENTER_PHASE2_THR_0_3,
				    	                                             (int16_t)U8_FTCS_ENTER_PHASE2_THR_1_0, (int16_t)U8_FTCS_ENTER_PHASE2_THR_1_1, (int16_t)U8_FTCS_ENTER_PHASE2_THR_1_2, (int16_t)U8_FTCS_ENTER_PHASE2_THR_1_3);
    }
				#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
				if(lctcsu1GearShiftFlag==1)
    {
					lts16TorqRiseThr = lts16TorqRiseThr + U8_TORQ_RISE_THRESHOLD_MODIFY;
    }
    else
    {
        ;
    }
				#endif  /* __TCS_CTRL_DEC_IN_GR_SHFT_MT */									


				if (lts16WheelSpin >= (lts16TargetWheelSpin + lts16TorqRiseThr))
    {
					lctcsu8TCSEngCtlPhase = 20;
    }
    else
    {
					if( cal_torq < (int16_t)ltu16TorqRiseLimit ) 
        {
            			lctcsu8FastTorqRecoveryStatus = FAST_TORQ_RECOVERY_MODE;
            			lctcsu8TCSEngCtlPhase = 2;	
            }
            else
    	    		{	/* InitForcedTorqRiseMode 종료 (한번 목표치까지 도달했으면 재진입 하지 않음) */
    		    		lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
    		    		lctcsu8TCSEngCtlPhase = 2;
            }
        }
        }
    }
    else
    {
			lctcsu8TCSEngCtlPhase = 0;
			if(lts16WheelSpinErrorDiff > 0)
    {
				if( (lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE) && (lts16WheelSpinError >= -VREF_1_KPH) )   /* 강제 2nd cycle 전환 */
	    {
    			    lctcsu8PosSpinErrDiffCnt2++;
    			    lctcsu8PosSpinErrDiffCnt2 = (uint8_t)LCTCS_s16IFindMinimum((int16_t)lctcsu8PosSpinErrDiffCnt2,L_U8_TIME_10MSLOOP_70MS);
    			    if(lctcsu8PosSpinErrDiffCnt2>=L_U8_TIME_10MSLOOP_40MS)
                {
    				    lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
	    }
	    else
	    {
    	    			;
	    }
                }
                else
                {
                    ;
                }
				lctcsu8NegSpinErrDiffCnt1 = 0; /* 연속 count 증가에 대해서만 반영키 위함 */				
            }
			else	/* 강제 2nd cycle 전환 */
            {
			    lctcsu8NegSpinErrDiffCnt1++;
			    lctcsu8NegSpinErrDiffCnt1 = (uint8_t)LCTCS_s16IFindMinimum((int16_t)lctcsu8NegSpinErrDiffCnt1,L_U8_TIME_10MSLOOP_70MS);
			    if(lctcsu8NegSpinErrDiffCnt1>=L_U8_TIME_10MSLOOP_40MS)
                    {
    		    	if( cal_torq < (int16_t)ltu16TorqRiseLimit ) 
    		    	{
    	        		lctcsu8FastTorqRecoveryStatus = FAST_TORQ_RECOVERY_MODE;
    	        		lctcsu8TCSEngCtlPhase = 2;	
                    }
                    else
    		    	{	/* InitForcedTorqRiseMode 종료 (한번 목표치까지 도달했으면 재진입 하지 않음) */
    			    	lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
    			    	lctcsu8TCSEngCtlPhase = 2;
                    }
                    }
                    else
                    {
                        ;
                    }
                }
		}
		if( ((lctcsu8FastTorqRecoveryStatus != FAST_TORQ_RECOVERY_MODE) && (lctcss16LowSpinCntIn1stCycle >= L_U8_TIME_10MSLOOP_1500MS))	
			||((lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE) && (cal_torq >= (int16_t)ltu16TorqRiseLimit)) )
		{  /* 1st cycle Time limit or ForcedTorqRiseMode 종료 (한번 목표치까지 도달했으면 재진입 하지 않음) */
			lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
    }
                else
                {
			;		
    }
		if(lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE)
                    {
			lctcsu8TCSEngCtlPhase = 2;
			if(cal_torq >= (int16_t)ltu16TorqRiseLimit)
			{/* ForcedTorqRiseMode 종료 (한번 목표치까지 도달했으면 재진입 하지 않음) */
				lctcsu8FastTorqRecoveryStatus = CYCLE_2ND_TRANSITION_MODE;
			}
			else{}
		}
		else{}
                    }
                    else
                    {
		lctcsu8FastTorqRecoveryStatus = READY_MODE;
		if (lts16WheelSpinError > 0)
                    {
			if (lts16WheelSpinErrorDiff > 0)
		{
				lctcsu8TCSEngCtlPhase = 3;
                    }
                    else
                    {
				lctcsu8TCSEngCtlPhase = 4;
                }
                }
                else
                {
			if (lts16WheelSpinErrorDiff <= 0)
                {
				lctcsu8TCSEngCtlPhase = 5;
                }
                else
                {
				lctcsu8TCSEngCtlPhase = 6;
                }
            }
        }
        }


#if __TCS_VIBRATION_DETECTION
static void    LCTCS_vCtlEngineVibration(void)
                {
	/*===============================================================================*/
	/* Purpose : Wheel Vibration 감지시 Engine Torque Control  			 			 */
	/* Input Variable : lctcsu16VIBEngineCtlCnt, lctcsu8VIBEngineCtlScan,         	 */
	/*					lctcsu16VIBEngineCtlPeriod							 		 */
	/* Output Variable : lctcss16VIBETCSCalTorqState, lctcss16VIBFTCSCalTorqState,   */
	/*                   cal_torq,    	 									 		 */     
	/* 2010. 12. 27 By ByoungJin Park   											 */
	/*===============================================================================*/
    if ( ((ETCS_ON) || (FTCS_ON)) && (ENG_STALL == 0) && (lctcsu1DetectVibration==1) )
            {
   		tcs_tempW0 = LCTCS_s16ILimitRange((int16_t)U8_TCS_VIB_FREQUENCY_THR, 1, L_U8_TIME_10MSLOOP_250MS);
   		tcs_tempW1 = LCTCS_s16ILimitRange((int16_t)U8_TCS_VIB_FREQUENCY_ENTER, 1, tcs_tempW0);
   		lctcsu16VIBEngineCtlCnt = lctcsu16VIBEngineCtlCnt + 1;
		lctcsu16VIBEngineCtlCnt = LCTCS_u16ILimitMaximum(lctcsu16VIBEngineCtlCnt,U16_MAX_LIMIT);
		lctcsu8VIBEngineCtlScan = (uint8_t)(1000/(LOOP_10MS*tcs_tempW1));
		if (ETCS_ON == 1)
                {
			lctcsu16VIBEngineCtlPeriod = (uint16_t)(lctcsu8VIBEngineCtlScan*U8_TCS_VIB_ETCS_ENG_PERIOD);
                }
                else
                {
			lctcsu16VIBEngineCtlPeriod = (uint16_t)(lctcsu8VIBEngineCtlScan*U8_TCS_VIB_FTCS_ENG_PERIOD);
                }
		if ( (lctcsu16VIBEngineCtlCnt == lctcsu16VIBEngineCtlPeriod) || (lctcsu16VIBEngineCtlCnt == 1) )
                {
			if (ETCS_ON == 1)
                {
				tcs_cmd = cal_torq - (int16_t)(((int32_t)cal_torq*S8_TCS_VIB_ETCS_ENG_TORQUE)/100);
            }
            else
            {
				tcs_cmd = cal_torq - (int16_t)(((int32_t)cal_torq*S8_TCS_VIB_FTCS_ENG_TORQUE)/100);
            }
			lctcss16VIBCalTorqState = tcs_cmd;
			lctcsu16VIBEngineCtlCnt = 1;
			lctcss16VIBCalTorqLevel = lctcss16VIBCalTorqState;			
        }
        else
        {
			if(lctcsu1FastRPMInc==0)
			{
				tcs_cmd = lctcss16VIBCalTorqState;
			}
			else
			{			
				if(ltcss16PDGainEffect < 0)
				{
					lctcss16VIBCalTorqState = tcs_cmd;
				}
				else
				{
			tcs_cmd = lctcss16VIBCalTorqState;
        }
    }
		}
	}
    else
    {
		lctcsu16VIBEngineCtlCnt = 0;
		lctcsu16VIBEngineCtlPeriod = 0;
		lctcss16VIBCalTorqState = cal_torq;
		lctcss16VIBCalTorqLevel = cal_torq;		
    }

}
#endif /*__TCS_VIBRATION_DETECTION*/

void LCTCS_vIncTorqAfterEEC(void)
{
	/*=============================================================================*/
	/* Purpose : EEC 제어 종료 후 토크 일정 Level 까지 토크 회복 모드              */
	/* Input Variable : ESP_TCS_ON, ETO_SUSTAIN_2SEC, lts16WheelSpin               */
	/* Output Variable : tcs_cmd, ltcsu1TorqLvlRecoverAfterEEC                     */
	/* 2008. 6. 11 By Jongtak												       */
	/*=============================================================================*/	
	/* lctcss16TorqLvlAfterEEC : EEC 제어후 토크 회복 Level */  
	lctcss16TorqLvlAfterEEC = LCTCS_s16IInterpDepAy4by3( S16_TCS_TORQ_LVL_AFTER_EEC_0_0, S16_TCS_TORQ_LVL_AFTER_EEC_0_1, S16_TCS_TORQ_LVL_AFTER_EEC_0_2,
	       	                                             S16_TCS_TORQ_LVL_AFTER_EEC_1_0, S16_TCS_TORQ_LVL_AFTER_EEC_1_1, S16_TCS_TORQ_LVL_AFTER_EEC_1_2,
	       	                                             S16_TCS_TORQ_LVL_AFTER_EEC_1_0, S16_TCS_TORQ_LVL_AFTER_EEC_1_1, S16_TCS_TORQ_LVL_AFTER_EEC_1_2,
	   	                                                 S16_TCS_TORQ_LVL_AFTER_EEC_2_0, S16_TCS_TORQ_LVL_AFTER_EEC_2_1, S16_TCS_TORQ_LVL_AFTER_EEC_2_2);

	/* lctcss16TorqUpRateAfterEEC : EEC 제어후 토크 회복 기울기 */
	lctcss16TorqUpRateAfterEEC = LCTCS_s16IInterpDepAy4by3( (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_0_0, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_0_1, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_0_2,
	                                                       	(int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_0, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_1, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_2,
	                                                       	(int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_0, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_1, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_1_2,
	                                                       	(int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_2_0, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_2_1, (int16_t)U8_TCS_TORQ_INC_RATE_A_EEC_2_2);		
  	if ( (ESP_TCS_ON==0) && (ETO_SUSTAIN_3SEC==1) && (tcs_cmd < lctcss16TorqLvlAfterEEC) && (lts16WheelSpin < VREF_2_KPH) )
    {
		ltcsu1TorqLvlRecoverAfterEEC = 1;
  		tcs_tempW0 = cal_torq + lctcss16TorqUpRateAfterEEC;
  		tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd , tcs_tempW0);
    }
  /*#if TORQ_RECOVERY_DEPEND_ON_STEER
  	else if ( (ESP_TCS_ON==1) && (s8SetYawPDGainSelecter==STATE_YAW_UNSTABLE) && (tcs_cmd < lctcss16TorqLvlAfterEEC) && (lts16WheelSpin < lts16TargetWheelSpin) )
  	{	
		ltcsu1TorqLvlRecoverAfterEEC = 1;
  		tcs_tempW0 = cal_torq + lctcss16TorqUpRateAfterEEC;
  		tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd , tcs_tempW0);  		
  	}
  #endif	TORQ_RECOVERY_DEPEND_ON_STEER */  	  	
    else
    {
		ltcsu1TorqLvlRecoverAfterEEC = 0;
                }
}

#if __MY08_TCS_DCT_H2L
void LCTCS_vDctH2L(void)
{
	tempW0 = acc_r / 2;
	
	if (ltcsu1DctH2L==0)
        {
		if (ltcsu1HighMuSuspectForH2L==0)
    {
			ltcss16H2LTimer = 0;
			if (tempW0 /*ebd_filt_grv*/ > (int16_t)U8_TCS_HIGH_MU_TH_FOR_H2L_DCT)
    {
				ltcsu1HighMuSuspectForH2L =	1;
    }
    else
    {
				ltcsu1HighMuSuspectForH2L =	0;
    }
    }
    else
		{	/* ltcsu1HighMuSuspectForH2L = 1 */
			if ( (tempW0 /*ebd_filt_grv*/ <= (int16_t)U8_TCS_LOW_MU_TH_FOR_H2L_DCT) && (lts16WheelSpin>=VREF_3_KPH) && (lts16WheelSpinErrorDiff > 0) )
			{	/* H2L 감지 조건 */
				ltcsu1DctH2L =	1;
				ltcsu1HighMuSuspectForH2L =	0;
				ltcss16H2LTimer = 0;
    }
			else if (tempW0 /*ebd_filt_grv*/ <= (int16_t)U8_TCS_H2L_SUSPECT_RESET_TH)
			{	/* ltcsu1HighMuSuspectForH2L reset 조건 1 */
				if (ltcss16H2LTimer >= (int16_t)U16_TCS_H2L_SUSPECT_RESET_TIME)
        {
					ltcsu1HighMuSuspectForH2L =	0;
            }
            else
            {
					ltcss16H2LTimer++;
				}
    }
				#if ((__AHB_GEN3_SYSTEM == ENABLE)||(__IDB_LOGIC==ENABLE))
			else if ( (mtp<30) || ((lsespu1AHBGEN3MpresBrkOn==1) ||
					((lsespu1MpresByAHBgen3Invalid==1)&&(fu1BLSErrDet==0)&&(BLS==1))) )				
    #else
			else if ( (mtp<30) || (MPRESS_BRAKE_ON==1) )
    #endif	
			{	/* ltcsu1HighMuSuspectForH2L reset 조건 2 */
				ltcsu1HighMuSuspectForH2L =	0;
        }
        else
        {
				ltcss16H2LTimer--;
				ltcss16H2LTimer = LCTCS_s16ILimitMinimum(ltcss16H2LTimer, 0);
			}
        }
    }
    else
	{	/* ltcsu1DctH2L = 1*/
		if (lts16WheelSpinErrorDiff<=0)
		{	/* ltcsu1DctH2L Reset 조건 1 */
			if (ltcss16H2LTimer >= (int16_t)U16_TCS_H2L_RESET_TIME)
    {
				ltcsu1DctH2L =	0;
    }
			else
    {
				ltcss16H2LTimer++;
	    }
	    }
		else if ( 
					  #if __VARIABLE_MINIMUM_TORQ_LEVEL	
				  (cal_torq <= lctcss16NewEngOKTorq) 					  
	  #else
				  (cal_torq <= ENG_OK_TORQ) 
					  #endif	
			      || (mtp<30)  
					  #if ((__AHB_GEN3_SYSTEM == ENABLE)||(__IDB_LOGIC==ENABLE))			
			      || ( (lsespu1AHBGEN3MpresBrkOn==1)||
					   ((lsespu1MpresByAHBgen3Invalid==1)&&(fu1BLSErrDet==0)&&(BLS==1)) )	
		#else
				  || (MPRESS_BRAKE_ON==1)			
		#endif
			      || (tempW0 /*ebd_filt_grv*/ > (int16_t)U8_TCS_HIGH_MU_TH_FOR_H2L_DCT) )				
		{	/* ltcsu1DctH2L Reset 조건 2 */
			ltcsu1DctH2L = 0;
                }
                else
                {
			ltcss16H2LTimer--;
			ltcss16H2LTimer = LCTCS_s16ILimitMinimum(ltcss16H2LTimer, 0);
                }

                }
}

void LCTCS_vCompensateGainForH2L(void)
{
	if ( (ltcsu1DctH2L==1) && (CYCLE_2ND==0) )
                {
		if ( ((ltcss16SpinFL > VREF_3_KPH) && (ltcss16SpinFR > VREF_3_KPH)) ||
			 ((ltcss16SpinRL > VREF_3_KPH) && (ltcss16SpinRR > VREF_3_KPH)) )
		{	/* 구동륜 두 휠 동시 Spin */
			if (lts16WheelSpinErrorDiff > 0)
            {
				lts16DGain = lts16DGain + (int16_t)U8_TCS_D_GAIN_ADD_SPIN_INC_H2L;
                    }
                    else
                    {
				lts16DGain = lts16DGain + (int16_t)U8_TCS_D_GAIN_ADD_SPIN_DEC_H2L;
                    }
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
		;
                    }
}
#endif /* #if __MY08_TCS_DCT_H2L */

#if __TCS_PREVENT_FAST_RPM_INCREASE  	
static void LCTCS_vIncGainInFastRPMInc(void)
	{
	if (lctcsu1FastRPMInc==1)
    {
		if (CYCLE_2ND==0)
        {
			tcs_tempW0 = (int16_t)(U8_TCS_SET_FAST_RPM_INC_TH + U8_TCS_SET_FAST_RPM_INC_TH_1);
			
			if ((slope_eng_rpm > tcs_tempW0)&&(lts16WheelSpinErrorDiff > 0))
			{
				if ((lctcsu8TCSEngCtlPhase==0)||(lctcsu8TCSEngCtlPhase==1))
				{/*Phase0 or Phase1*/
					lts16PGain = (int16_t)(((int32_t)lts16PGain * S16_TCS_P_GAIN_F_RPM_FAST_INC_1)/100);
					lts16DGain = (int16_t)(((int32_t)lts16DGain * S16_TCS_D_GAIN_F_RPM_FAST_INC_1)/100);
				}
			}
		}
		else
		{
			if (lts16WheelSpin > lts16TargetWheelSpin)
			{/*Phase3 or Phase4*/
				lts16PGain = (int16_t)(((int32_t)lts16PGain * S16_TCS_P_GAIN_F_RPM_FAST_INC_N)/100);	
			}
			else
			{/*Phase5 or Phase6 no P Gain increase*/
                        ;
        }
			
			if(lts16WheelSpinErrorDiff > 0)
        {
				lts16DGain = (int16_t)(((int32_t)lts16DGain * S16_TCS_D_GAIN_F_RPM_FAST_INC_N)/100); 
    }
    else
			{
				;
        }
        }
        }
        else
        {
            ;
        }
	}
#endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */
  		
#if __TCS_BUMP_DETECTION
static void	LCTCS_vCompensateGainForBump(void)
  	{	
	if ( (ltcsu1BumpDetect==1) && (lts16WheelSpinError <= 0) )
		{
		tcs_tempW0 = LCESP_s16IInter2Point(ltcss16AccOfNonDrivenWheel, (int16_t)U16_AX_RANGE_MIN, (int16_t)U8_TCS_BUMP_ADD_P_GAIN_LOW_AX,
                                                                       (int16_t)U16_AX_RANGE_MAX, (int16_t)U8_TCS_BUMP_ADD_P_GAIN_HIGH_AX);		
		lts16PGain = lts16PGain + tcs_tempW0;
		}
		else
		{
                ;
		}
	
	}   
#endif	/* #if __TCS_BUMP_DETECTION */
	   
		
void LCTCS_vSearchGoodTorqueLevel(void)
    {
	LCTCS_vDetectTransitionPoint();
	LCTCS_vCalTorqueValue();
}

void LCTCS_vDetectTransitionPoint(void)
		{
	if (CYCLE_2ND==0)
 		{
		ltcss16BaseTorqueLevel = cal_torq;		
		ltcsu1FastTorqReduction = 0;
		ltcsu1FastTorqRecovery = 0;
		ltcss16FastTorqueInputValue = 0;		
		}           
	else if	( ((lctcsu8TCSEngCtlPhase_old == 5)||(lctcsu8TCSEngCtlPhase_old == 6)) && (lctcsu8TCSEngCtlPhase == 3) )
	{	/* Fast Torque Reduction is Required */
		ltcsu1FastTorqReduction = 1;
		ltcss16BaseTorqueLevelOld = ltcss16BaseTorqueLevel;
		ltcss16BaseTorqueLevel = cal_torq;
		tcs_tempW0 = ltcss16BaseTorqueLevel - ltcss16BaseTorqueLevelOld;
		tcs_tempW0 = LCTCS_s16ILimitRange(tcs_tempW0, 0, 3200);
	  	tcs_tempW3 = LCTCS_s16ILimitRange(lts16WheelSpinErrorDiff, 0, 100);
			#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
		if 		( (gs_ena == 1) || (lctcsu1GearShiftFlag == 1) )
		    #else
		if 		(gs_ena == 1)		  
			#endif		  
		{	/* 기어 변속시 Discrete 제어에 의한 토크 저감 하지 않음 */
			tcs_tempW1 = 0;
		}
		else if (ETCS_ON==1)
	    {
			tcs_tempW1 = LCESP_s16IInter3Point(tcs_tempW3, (int16_t)U8_TCS_FAST_REDUCTION_LOW_H, U8_TCS_FAST_RED_TORQ_LOW_H,
													 	   (int16_t)U8_TCS_FAST_REDUCTION_MID_H, U8_TCS_FAST_RED_TORQ_MID_H,
													 	   (int16_t)U8_TCS_FAST_REDUCTION_HIGH_H, U8_TCS_FAST_RED_TORQ_HIGH_H);				
	    }
            else
            {
			tcs_tempW1 = LCESP_s16IInter3Point(tcs_tempW3, (int16_t)U8_TCS_FAST_REDUCTION_LOW_SP, U8_TCS_FAST_RED_TORQ_LOW_SP,
													 	   (int16_t)U8_TCS_FAST_REDUCTION_MID_SP, U8_TCS_FAST_RED_TORQ_MID_SP,
													 	   (int16_t)U8_TCS_FAST_REDUCTION_HIGH_SP, U8_TCS_FAST_RED_TORQ_HIGH_SP);
            }
		tcs_tempW4=(int16_t)(((int32_t)tcs_tempW0*tcs_tempW1)/100);
		  #if __TCS_CONV_ABS_TORQ
		tcs_tempW5=(int16_t)((int32_t)max_torq*U8_TCS_DISCRETE_TORQ_LIMIT);
		  #else
		tcs_tempW5=(int16_t)(U8_TCS_DISCRETE_TORQ_LIMIT*10);  
		  #endif
		tcs_tempW6=LCTCS_s16IFindMinimum(tcs_tempW4,tcs_tempW5);
		ltcss16FastTorqueInputValue = ltcss16BaseTorqueLevel - tcs_tempW6;
            }
	else if	( ((lctcsu8TCSEngCtlPhase_old == 3)||(lctcsu8TCSEngCtlPhase_old == 4)) && (lctcsu8TCSEngCtlPhase == 5) )
	{	/* Fast Torque Recovery is Required */
		ltcsu1FastTorqRecovery = 1;
		ltcss16BaseTorqueLevelOld = ltcss16BaseTorqueLevel;		
		ltcss16BaseTorqueLevel = cal_torq;	
		tcs_tempW0 = ltcss16BaseTorqueLevel - ltcss16BaseTorqueLevelOld;
		tcs_tempW0 = LCTCS_s16ILimitRange(tcs_tempW0, -3200, 0);
		tcs_tempW3 = LCTCS_s16ILimitRange(lts16WheelSpinErrorDiff, -100, 0);
		tcs_tempW3 = -tcs_tempW3;
			#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
		if 		( (gs_ena == 1) || (lctcsu1GearShiftFlag == 1) )
		    #else
		if 		(gs_ena == 1)		  
			#endif 	  		  
		{	/* 기어변속 초기의 토크 수준까지 Discrete 제어로 토크 회복 */
			tcs_tempW1 = 100;
            }
		else if (ETCS_ON==1)
            {
			tcs_tempW1 = LCESP_s16IInter3Point(tcs_tempW3, (int16_t)U8_TCS_FAST_RECOVERY_LOW_H, U8_TCS_FAST_REC_TORQ_LOW_H,
													 	   (int16_t)U8_TCS_FAST_RECOVERY_MID_H, U8_TCS_FAST_REC_TORQ_MID_H,
													 	   (int16_t)U8_TCS_FAST_RECOVERY_HIGH_H, U8_TCS_FAST_REC_TORQ_HIGH_H);
        }
        else
        {
			tcs_tempW1 = LCESP_s16IInter3Point(tcs_tempW3, (int16_t)U8_TCS_FAST_RECOVERY_LOW_SP, U8_TCS_FAST_REC_TORQ_LOW_SP,
													 	   (int16_t)U8_TCS_FAST_RECOVERY_MID_SP, U8_TCS_FAST_REC_TORQ_MID_SP,
													 	   (int16_t)U8_TCS_FAST_RECOVERY_HIGH_SP, U8_TCS_FAST_REC_TORQ_HIGH_SP);
        }
		tcs_tempW4=(int16_t)(((int32_t)tcs_tempW0*tcs_tempW1)/100);
		  #if __TCS_CONV_ABS_TORQ
		tcs_tempW5=(int16_t)((int32_t)(-max_torq)*U8_TCS_DISCRETE_TORQ_LIMIT);
		  #else
		tcs_tempW5=(int16_t)(U8_TCS_DISCRETE_TORQ_LIMIT*(-10));  
		  #endif
		tcs_tempW6=LCTCS_s16IFindMaximum(tcs_tempW4,tcs_tempW5);
		ltcss16FastTorqueInputValue = ltcss16BaseTorqueLevel - tcs_tempW6;		
    }
    else
    {
		ltcsu1FastTorqReduction = 0;
		ltcsu1FastTorqRecovery = 0;
		ltcss16FastTorqueInputValue = 0;
}
}

void LCTCS_vCalTorqueValue(void)
{
	if 		(ltcsu1FastTorqReduction == 1)
    {
		tcs_cmd = LCTCS_s16IFindMinimum(tcs_cmd, ltcss16FastTorqueInputValue);
    }
	else if	(ltcsu1FastTorqRecovery == 1)
        {
		tcs_cmd = LCTCS_s16IFindMaximum(tcs_cmd, ltcss16FastTorqueInputValue);		
        }
        else
        {
		;	/* Do Nothing */
        }
    }

void LCTCS_vPreventBigSpin(void)
    {
	lctcsu1SpinOverThresholdOld = lctcsu1SpinOverThreshold;
	if (CYCLE_2ND==0)
    {
		lctcsu16SpinOverThInFast = U16_TCS_DIAG_WHL_SPIN_CNT*8;
		lctcsu16SpinOverThInFast = LCTCS_s16ILimitRange(lctcsu16SpinOverThInFast, VREF_0_KPH, VREF_15_KPH);
		
		/* tcs_tempW0 : Threshold for Spin Increase (Calibration) */
		tcs_tempW0 = LCESP_s16IInter3Point(lts16WheelSpinErrorDiff, SPIN_DIFF_0_1KPH, VREF_25_KPH,
																	SPIN_DIFF_0_3KPH, VREF_15_KPH,
																	SPIN_DIFF_0_5KPH, lctcsu16SpinOverThInFast);
		/* tcs_tempW1 : Threshold for Spin Decrease (Calibration) */
		tcs_tempW1 = LCESP_s16IInter3Point(McrAbs(lts16WheelSpinErrorDiff), SPIN_DIFF_0_1KPH, lctcsu16SpinOverThInFast,
																		    SPIN_DIFF_0_3KPH, VREF_15_KPH,
																		    SPIN_DIFF_0_5KPH, VREF_25_KPH);																	

		/* tcs_tempW2 : Torque Decrease Input for Spin Overshoot Prevention (Calibration)*/
		tcs_tempW2 = S16_TCS_TRQ_DEC4BIG_SPIN_CTL;		
        }
        else
        {
		/* tcs_tempW0 : Threshold for Spin Increase (Calibration) */
		tcs_tempW0 = LCESP_s16IInter2Point(lts16WheelSpinErrorDiff, SPIN_DIFF_0_1KPH, VREF_3_KPH,
																	SPIN_DIFF_0_2KPH, VREF_1_KPH);
		/* tcs_tempW1 : Threshold for Spin Decrease (Calibration) */
		tcs_tempW1 = LCESP_s16IInter2Point(McrAbs(lts16WheelSpinErrorDiff), SPIN_DIFF_0_1KPH, VREF_1_KPH,
																		 SPIN_DIFF_0_2KPH, VREF_3_KPH);																	
																			
		/* tcs_tempW2 : Torque Decrease Input for Spin Overshoot Prevention (Calibration)*/
		tcs_tempW2 = S16_TCS_TRQ_DEC4BIG_SPIN_CTL;		
        }

	/* Big Spin Detection */
		#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
	if 		( (gs_ena == 1) || (lctcsu1GearShiftFlag == 1) )
    #else
	if 		(gs_ena == 1)		  
    #endif
  	{	/* 기어 변속 중에는 Big Spin 감지 하지 않음 */
		lctcsu1SpinOverThreshold = 0;
    }
	else if ( (lts16WheelSpinErrorDiff > 0 ) && (lts16WheelSpin >= (lts16TargetWheelSpin + tcs_tempW0)) )
            {
		lctcsu1SpinOverThreshold = 1;
            }
	else if ( (lctcsu1SpinOverThreshold == 1) 
		      && (lts16WheelSpinErrorDiff <= 0 ) && (lts16WheelSpin <= (lts16TargetWheelSpin + tcs_tempW1)) )
        {
		lctcsu1SpinOverThreshold = 0;
            }
            else
            {
                ;
            }

	/* Torque Input for Spin Overshoot */
	if (lctcsu1SpinOverThreshold == 1)
	{
		if 		( (lctcsu1SpinOverThresholdOld==0) && (lctcsu1SpinOverThreshold==1) )
    {
			tcs_cmd = cal_torq - tcs_tempW2;
			ltcss16BigSpinCnt = 0;			
    }
		else if	( ltcss16BigSpinCnt >= S16_TCS_WAIT_T4BIG_SPIN_CTL )
        {
			tcs_cmd = cal_torq - tcs_tempW2;
			ltcss16BigSpinCnt = 0;
        }
        else
        {
			if (lts16WheelSpinErrorDiff >=0)
			{	/* Spin 기울기가 클수록 Big Spin 감지 시간 단축 */
				tcs_tempW6 = LCESP_s16IInter2Point(lts16WheelSpinErrorDiff, SPIN_DIFF_0_025KPH, 1,
				                                                            SPIN_DIFF_0_1KPH, 4);
				                                                            
				/* Spin 크기가 클수록 Big Spin 감지 시간 단축 */
				tcs_tempW7 = LCESP_s16IInter2Point(lts16WheelSpinError, VREF_3_KPH, 1,
				                                              			VREF_6_KPH, 4);

				tcs_tempW8 = LCTCS_s16IFindMaximum(tcs_tempW6, tcs_tempW7); 						                                               
                }
                else
            {
				tcs_tempW8 = 0;
			}
		  	ltcss16BigSpinCnt = ltcss16BigSpinCnt + tcs_tempW8;
            }
            }
            else
            {
	  /* Torque Recovery는 Discrete Input에 일임 */
	  ltcss16BigSpinCnt = 0;
            }
        }

void LCTCS_vDetectMiniWheelTendency(void)
        {
	/*=============================================================================*/
	/* Purpose : TCS제어 중 Mini Tire 감지										   */
	/* Input Variable : TCS_MINI_TIRE_SUSPECT									   */
	/* Output Variable : None													   */
	/* 2007. 1. 25 By Jongtak												       */
	/*=============================================================================*/
	if (TCS_MINI_TIRE_SUSPECT == 0)
                {
		LCTCS_vEnableMiniWheelDetect();
		LCTCS_vCntMiniWheelTendency();
		LCTCS_vSetMiniTireSuspect();
                }
                else
                {
		LCTCS_vResetMiniTireSuspect();
                }
            }

void LCTCS_vEnableMiniWheelDetect(void)
            {
	/*=============================================================================*/
	/* Purpose : Wheel 속도 조건 판단하여 Mini Tire 감지 Enable 및 Disable		   */
	/* Input Variable : WL->ltcss16MovingAveragedWhlSpd, WL->wvref, vref5, 		   */
	/* Output Variable : ENABLE_TCS_MINI_DETECT						   		       */
	/* 2007. 1. 25 By Jongtak												       */
	/*=============================================================================*/

	/* Temp tire 감지를 위한, 각 휠별 spin량 계산 */
	tcs_tempW0 = LCTCS_s16ILimitMinimum((FL.lctcss16SpinRaw), 0);
	tcs_tempW1 = LCTCS_s16ILimitMinimum((FR.lctcss16SpinRaw), 0);
	tcs_tempW2 = LCTCS_s16ILimitMinimum((RL.lctcss16SpinRaw), 0);
	tcs_tempW3 = LCTCS_s16ILimitMinimum((RR.lctcss16SpinRaw), 0);

	/* 동반경이 30% 이상 차이나는 Mini Wheel은 감지 하지 않음 (GM SSTS : 25%) */
	tcs_tempW4 = (vref5 * 3) / 10;

	/* Mini Wheel이 장착되었다고 추정되는 wheel은 2KPH 이상의 Spin이 발생하여야 하고
	   다른 Wheel은 1KPH 이하의 Spin이 발생하여야함.							     */

  /* 1 Wheel Mini Tire */
	/* Mini Wheel In FL */
	if 		( (tcs_tempW0 > VREF_2_KPH) && (tcs_tempW0 < tcs_tempW4) &&
			  (tcs_tempW1 < VREF_1_KPH) && (tcs_tempW2 < VREF_1_KPH) && (tcs_tempW3 < VREF_1_KPH) )
            {
		ENABLE_TCS_MINI_DETECT = 1;
            }
	/* Mini Wheel In FR */
	else if ( (tcs_tempW1 > VREF_2_KPH) && (tcs_tempW1 < tcs_tempW4) &&
			  (tcs_tempW0 < VREF_1_KPH) && (tcs_tempW2 < VREF_1_KPH) && (tcs_tempW3 < VREF_1_KPH) )
            {
		ENABLE_TCS_MINI_DETECT = 1;
            }
	/* Mini Wheel In RL */
	else if ( (tcs_tempW2 > VREF_2_KPH) && (tcs_tempW2 < tcs_tempW4) &&
			  (tcs_tempW0 < VREF_1_KPH) && (tcs_tempW1 < VREF_1_KPH) && (tcs_tempW3 < VREF_1_KPH) )
            {
		ENABLE_TCS_MINI_DETECT = 1;
            }
	/* Mini Wheel In RR */
	else if ( (tcs_tempW3 > VREF_2_KPH) && (tcs_tempW3 < tcs_tempW4) &&
			  (tcs_tempW0 < VREF_1_KPH) && (tcs_tempW1 < VREF_1_KPH) && (tcs_tempW2 < VREF_1_KPH) )
            {
		ENABLE_TCS_MINI_DETECT = 1;
            }
            else
            {
		ENABLE_TCS_MINI_DETECT = 0;
        }
        }

void LCTCS_vCntMiniWheelTendency(void)
        {
	/*=============================================================================*/
	/* Purpose : 직진 TCS제어 중 Spin의 변화량이 미소한 경우 그 시간을 Counting    */
	/*           사용된 조건은 다른 주행 조건에서 Mini Tire 장착한 것으로 오감지   */
	/*           하지 않을 조건으로 Simulation을 통하여 구성한 것임				   */
	/* Input Variable : ETCS_ON, FTCS_ON, GAIN_ADD_IN_TURN,						   */
	/*					lts16WheelSpinErrorDiff, BTCS_ON, 						   */
	/*     				WL->ltcss8BTCSWheelCtlStatus			   				   */
	/* Output Variable : ltcss16MiniTireTendencyCnt		   						   */
	/* 2007. 1. 25 By Jongtak												       */
	/*=============================================================================*/
	if ( (ETCS_ON==1) || (FTCS_ON==1) || (BTCS_ON==1) )
                {
		/* 직진 주행 중, lts16WheelSpinErrorDiff Resolution : 80 (2 -> 0.025KPH/scan) */
		if ( (ENABLE_TCS_MINI_DETECT==1) && (GAIN_ADD_IN_TURN==0) && (McrAbs(lts16WheelSpinErrorDiff)< 2) )
                    {
			if (BTCS_ON==1)
    {
					ltcss16MiniTireTendencyCnt = ltcss16MiniTireTendencyCnt + 1;
                    }
                    else
                    {
				ltcss16MiniTireTendencyCnt = ltcss16MiniTireTendencyCnt + 1;
                    }
			ltcss16MiniTireTendencyCnt = LCTCS_s16ILimitMaximum(ltcss16MiniTireTendencyCnt, L_U16_TIME_10MSLOOP_14S);
                }
                else
                {
			ltcss16MiniTireTendencyCnt = ltcss16MiniTireTendencyCnt - 3;
			ltcss16MiniTireTendencyCnt = LCTCS_s16ILimitMinimum(ltcss16MiniTireTendencyCnt, 0);
                }
            }
            else
            {
		ltcss16MiniTireTendencyCnt = 0;
            }
        }


void LCTCS_vSetMiniTireSuspect(void)
        {
	/*=============================================================================*/
	/* Purpose : Counter에  의하여 TCS_MINI_TIRE_SUSPECT를 Set 시킴				   */
	/* Input Variable : ltcss16MiniTireTendencyCnt					 		   	   */
	/* Output Variable : TCS_MINI_TIRE_SUSPECT, ltcss16ResetMiniTireDetectCnt	   */
	/* 2007. 1. 25 By Jongtak												       */
	/*=============================================================================*/
	/* 600 : Simulation 결과 설정된 값으로 최소 200Scan 만에 감지  */
	/* 2WD 감지시 : Simulation 결과 설정된 값으로 최소 5s 만에 감지 */
	if (ltcss16MiniTireTendencyCnt > L_U16_TIME_10MSLOOP_4_2S)
    {
		TCS_MINI_TIRE_SUSPECT = 1;
		/* 감지 해제 시간 증가시 overflow 주의 할 것 */
		ltcss16ResetMiniTireDetectCnt = 1;
        }
	else if (lctcsu1WhlSpdCompByRTADetect == 1)
    {
		TCS_MINI_TIRE_SUSPECT = 1;
		/* 감지 해제 시간 증가시 overflow 주의 할 것 */
		ltcss16ResetMiniTireDetectCnt = 1;
    }
    else
    {
		TCS_MINI_TIRE_SUSPECT = 0;
		ltcss16ResetMiniTireDetectCnt = 0;
    }
}

void LCTCS_vResetMiniTireSuspect(void)
{
	/*=============================================================================*/
	/* Purpose : TCS_MINI_TIRE_SUSPECT가 Set 된 경우, 정차 후 3분이 지나면         */
	/*           Mini Tire 감지 해제(3분 : 타이어 교체에 필요한 시간)			   */
	/* Input Variable : ltcss16ResetMiniTireDetectCnt							   */
	/* Output Variable : TCS_MINI_TIRE_SUSPECT, ltcss16MiniTireTendencyCnt	   	   */
	/*                   ltcss16ResetMiniTireDetectCnt							   */
	/* 2007. 1. 25 By Jongtak												       */
	/*=============================================================================*/
	if (vref5 <= VREF_1_KPH)
    {
		if (ltcss16ResetMiniTireDetectCnt <= 0)
    {
			if (lctcsu1WhlSpdCompByRTADetect == 0)
		{
				TCS_MINI_TIRE_SUSPECT = 0;
				ltcss16MiniTireTendencyCnt = 0;
    	}
    	else
    	{
				ltcss16MiniTireTendencyCnt = 0;
    }
  	  	  	lctcsu8MiniTireSuspectResetCnt=0;
}
	        else
		{
			ltcss16ResetMiniTireDetectCnt--;
	        }
		}
		else 
		{
		/* 감지 해제 시간 증가시 overflow 주의 할 것 */
		ltcss16ResetMiniTireDetectCnt = 1;
		}
    	
}
    	
void LCTCS_vSetResetCycle2nd(void)
		{
	/*=============================================================================*/
	/* Purpose : 2nd Cycle Set/Reset 			     	 	   					   */
	/* Input Variable : CYCLE_2ND, slope_eng_rpm, lts16WheelSpin,    			   */
	/*                  lts16TargetWheelSpin 									   */
	/* Output Variable : CYCLE_2ND			 						               */
	/* 2006. 4. 17 By Jongtak												       */
	/*=============================================================================*/
    if (lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE)  
    {
      	CYCLE_2ND = 0;
		}
		else
		{
      	if (CYCLE_2ND)
		{
      		;/* CYCLE_2ND Reset 조건 */
		}
		else
		{
      		if (lctcsu8FastTorqRecoveryStatus == CYCLE_2ND_TRANSITION_MODE)
    {
      			CYCLE_2ND = 1;
	}
	else
	{
		;
	}
	        }
	        }
}
	
void LCTCS_vCalculatePDGain(void)
{
	/*=============================================================================*/
	/* Purpose :P, D Gain 계산 			     	 	   			   				   */
	/* Input Variable : CYCLE_2ND                                                  */
	/* Output Variable : None						               				   */
	/* 2006. 4. 26 By Jongtak												       */
	/*=============================================================================*/
	
	if (CYCLE_2ND==0)
	{
		LCTCS_vCalculatePDGain1stCycle();
		LCTCS_vGainDecreaseInHighMu();
		LCTCS_vGain1stCycleTransition();
	}
	else
	{	/* Cycle 1st 인 경우 다음 코드에 의한 lctcsu8TCSEngCtlPhase Overwrite 방지하기 위함 */
		LCTCS_vCalPDGain2ndCycForETCS();
		LCTCS_vCalPDGain2ndCycForFTCS();
		LCTCS_vGainIncreaseInHighMu();
		LCTCS_vGain2ndCycleTransition();
    }
	}

void LCTCS_vCalculatePDGain1stCycle(void)
	{
	/*=============================================================================*/
	/* Purpose : 1nd Cycle의 P, D Gain 계산 			     	 	   			   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff               */
	/* Output Variable : lts16PGain1stCycle, lts16DGain1stCycle					   */
	/* 2006. 4. 26 By Jongtak												       */
	/*=============================================================================*/
	/* 제어 시작시 lts16WheelSpinError>0 인 경우가 대부분이지만 4WD차량의 경우는
	   SNOW 가속시 lts16WheelSpinError<0 인 경우가 발생할 수 있음. */          
			
	if(ETCS_ON == 1)
    {
		LCTCS_vCalPDGain1stCycForETCS();
	}   
	else /* FTCS_ON==1 */
	{
		LCTCS_vCalPDGain1stCycForFTCS();		
	}
}

static void LCTCS_vCalPDGain1stCycForETCS(void)
{
	/*=============================================================================*/
	/* Purpose : ETCS 1st Cycle의 P, D Gain 계산 			     	 	   			   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff               */
	/* Output Variable : lts16PGain1stCycle, lts16DGain1stCycle					   */
	/* 2006. 4. 26 By Jongtak												       */
	/*=============================================================================*/
	/* 제어 시작시 lts16WheelSpinError>0 인 경우가 대부분이지만 4WD차량의 경우는
	   SNOW 가속시 lts16WheelSpinError<0 인 경우가 발생할 수 있음. */          

	if (lctcsu8TCSEngCtlPhase==0)
	{
	  #if __TCS_PREVENT_FAST_RPM_INCREASE
		lts16PGain1stCycle = 0;		
	  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */				
		lts16DGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_0_0, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_0_1, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_0_2, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_0_3,
		   	                                               (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_1_0, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_1_1, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_1_2, (int16_t)U8_PHASE0_1ST_CYCLE_D_GAIN_1_3);
	}
	else if (lctcsu8TCSEngCtlPhase==1)
	{
		lts16PGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_0, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_1, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_2, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_3,
		                                                   (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_0, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_1, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_2, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_3);
		lts16DGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_0_0, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_0_1, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_0_2, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_0_3,
		                                                   (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_1_0, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_1_1, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_1_2, (int16_t)U8_PHASE1_1ST_CYCLE_D_GAIN_1_3);
	}
	else if (lctcsu8TCSEngCtlPhase==20)
	{
		/* ICE에서 Split 오감지후 HOMO로 재 판단시 Target이 변하기 때문에
		   P Gain이 0이면 높은 수준에서 Torque Hold됨(0810150904.das/16.888sec) */		
		lts16PGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_0, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_1, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_2, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_0_3,
		                                                   (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_0, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_1, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_2, (int16_t)U8_PHASE1_1ST_CYCLE_P_GAIN_1_3);
		lts16DGain1stCycle = 0;
	}
   		    	else
	{/*lctcsu8TCSEngCtlPhase==2*/
   		    		;
   		    	}	
}	

static void LCTCS_vCalPDGain1stCycForFTCS(void)		
	{
	/*=============================================================================*/
	/* Purpose : FTCS 1st Cycle의 P, D Gain 계산 			     	 	   			   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff               */
	/* Output Variable : lts16PGain1stCycle, lts16DGain1stCycle					   */
	/* 2006. 4. 26 By Jongtak												       */
	/*=============================================================================*/
	/* 제어 시작시 lts16WheelSpinError>0 인 경우가 대부분이지만 4WD차량의 경우는
	   SNOW 가속시 lts16WheelSpinError<0 인 경우가 발생할 수 있음. */          
	
	if (lctcsu8TCSEngCtlPhase==0)
	{
	  #if __TCS_PREVENT_FAST_RPM_INCREASE
		lts16PGain1stCycle = 0;		
	  #endif	/* #if __TCS_PREVENT_FAST_RPM_INCREASE */				
		lts16DGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)S8_FTCS_PHASE0_D_GAIN_0_0, (int16_t)S8_FTCS_PHASE0_D_GAIN_0_1, (int16_t)S8_FTCS_PHASE0_D_GAIN_0_2, (int16_t)S8_FTCS_PHASE0_D_GAIN_0_3,
		                                                   (int16_t)S8_FTCS_PHASE0_D_GAIN_1_0, (int16_t)S8_FTCS_PHASE0_D_GAIN_1_1, (int16_t)S8_FTCS_PHASE0_D_GAIN_1_2, (int16_t)S8_FTCS_PHASE0_D_GAIN_1_3);
	}
	else if (lctcsu8TCSEngCtlPhase==1)
   		    {
		lts16PGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_FTCS_PHASE1_P_GAIN_0_0, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_1, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_2, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_3,
		                                                   (int16_t)U8_FTCS_PHASE1_P_GAIN_1_0, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_1, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_2, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_3);
		lts16DGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_FTCS_PHASE1_D_GAIN_0_0, (int16_t)U8_FTCS_PHASE1_D_GAIN_0_1, (int16_t)U8_FTCS_PHASE1_D_GAIN_0_2, (int16_t)U8_FTCS_PHASE1_D_GAIN_0_3,
		                                                   (int16_t)U8_FTCS_PHASE1_D_GAIN_1_0, (int16_t)U8_FTCS_PHASE1_D_GAIN_1_1, (int16_t)U8_FTCS_PHASE1_D_GAIN_1_2, (int16_t)U8_FTCS_PHASE1_D_GAIN_1_3);
   		    }
	else if (lctcsu8TCSEngCtlPhase==20)	                                                   
   		{
		/* ICE에서 Split 오감지후 HOMO로 재 판단시 Target이 변하기 때문에
		   P Gain이 0이면 높은 수준에서 Torque Hold됨(0810150904.das/16.888sec) */
		lts16PGain1stCycle = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_FTCS_PHASE1_P_GAIN_0_0, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_1, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_2, (int16_t)U8_FTCS_PHASE1_P_GAIN_0_3,
		                                                   (int16_t)U8_FTCS_PHASE1_P_GAIN_1_0, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_1, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_2, (int16_t)U8_FTCS_PHASE1_P_GAIN_1_3);
		lts16DGain1stCycle = 0;
	}
	else
	{/*lctcsu8TCSEngCtlPhase==2*/
   		;
   	}
	}

static void LCTCS_vDecideTorqRecoveryLevel(void)
  	{
	if ( (ETCS_ON==1) && ((GAIN_ADD_IN_TURN == 1)||(ltcss16ETCSEnterExitFlags == 4)) )	        
	   /* In a turn */     
   	{
		/*if(gear_pos==1) 
   		{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_1ST_GEAR;
	}
		else if(gear_pos==2)
	{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_2ND_GEAR;
	}
		else if(gear_pos==3)
	{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_3RD_GEAR;
	}
		else if(gear_pos==4)
  	{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_4TH_GEAR;
	}
		else if(gear_pos==5)
	{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_5TH_GEAR;
	}
	else
	{
			lctcsu8NormalizedLongGFactor = (uint8_t)U8_LONG_G_NORM_FACTOR_6TH_GEAR;
		}*/

		/*ltcsu16NormalizedLongG = (uint16_t)( (ltcss16AccOfNonDrivenWheel*5) / lctcsu8NormalizedLongGFactor ) ; (ltcss16AccOfNonDrivenWheel*10) / U8_LONG_G_NORMALIZE_FACTOR / 2(->acc_r을 afz scale로 맞추기 위함 )
		ltcsu16TCSMuLevel = (uint16_t)(LCESP_u16CompSqrt((uint16_t)(det_alatm), (uint16_t)(ltcsu16NormalizedLongG)));*/
		/* ltcsu16TCSMuLevel에 따른 ltu16TorqRiseLimit계산 필요, 현재 Ay에 따른 구조 적용*/
		ltu16TorqRiseLimit = (uint16_t)LCTCS_s16IInterpDepAy4by3( (int16_t)U16_TORQ_RISE_LIMIT_TURN_0_0, (int16_t)U16_TORQ_RISE_LIMIT_TURN_0_1, (int16_t)U16_TORQ_RISE_LIMIT_TURN_0_2,
	                               		                         (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_0, (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_1, (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_2,
	                               		                         (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_0, (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_1, (int16_t)U16_TORQ_RISE_LIMIT_TURN_1_2,
	                                                             (int16_t)U16_TORQ_RISE_LIMIT_TURN_2_0, (int16_t)U16_TORQ_RISE_LIMIT_TURN_2_1, (int16_t)U16_TORQ_RISE_LIMIT_TURN_2_2);        		                                           
	}
   	else
    {
		if( vref < VREF_5_KPH )  /*정차중 ; VREF_5_KPH : TBD */
   	{
			/* GVW 기준 15% Hill 등판 가능 토크량으로 튜닝 */
			if(ETCS_ON ==1)  /* ETCS */
   		{
				ltu16TorqRiseLimit = (uint16_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_0_0, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_0_1, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_0_2, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_0_3,
       					                                       	   (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_1_0, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_1_1, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_1_2, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_SS_1_3);
    }
			else			 /* FTCS */
   		    	{
				ltu16TorqRiseLimit = (uint16_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_0_0, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_0_1, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_0_2, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_0_3,
           					                                       (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_1_0, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_1_1, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_1_2, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_SS_1_3);                
	}  	
	}
		else					 /*주행중*/
	{
			if(ETCS_ON ==1)  /* ETCS */
	{
				ltu16TorqRiseLimit = (uint16_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U16_ETCS_TORQ_RISE_LIMIT_0_0, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_0_1, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_0_2, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_0_3,
           				                                       	         (int16_t)U16_ETCS_TORQ_RISE_LIMIT_1_0, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_1_1, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_1_2, (int16_t)U16_ETCS_TORQ_RISE_LIMIT_1_3);
	}
			else			 /* FTCS */
	{
				ltu16TorqRiseLimit = (uint16_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U16_FTCS_TORQ_RISE_LIMIT_0_0, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_0_1, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_0_2, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_0_3,
   	        				                                             (int16_t)U16_FTCS_TORQ_RISE_LIMIT_1_0, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_1_1, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_1_2, (int16_t)U16_FTCS_TORQ_RISE_LIMIT_1_3);
	}
	}
	  			#if __MU_EST_FOR_TCS
		if(lsabsu1MuEstReliableOK == 1)
	{
			if(lsabss16RoadGradGfilter > 5)		/* Snow or DA Hill */
	{
        		ltu16TorqRiseLimit = ltu16TorqRiseLimit + U16_ETCS_TORQ_RISE_LIMIT_ADD;    		       					                                       	   
	}
	else
	{
		;
	}
	}	
	else
	{
		;
	}	
    		#endif	
}
}

static void LCTCS_vCalPDGainInTurnFor1stCyc(void)
{
	/*=============================================================================*/
	/* Purpose : 선회시 P,D Gain 증대					   							   */
	/* Input Variable : ETO_SUSTAIN_1SEC, det_alatm, ltcss16TargetSpinReduceAyTh   */
	/*					lts16WheelSpinError, lts16WheelSpinErrorDiff			   */
	/*                  lts16Phase7AddPgainInTurn, lts16Phase8AddPgainInTurn	   */
	/* Output Variable : lts16AddPGainInTurn, lctcsu8TCSEngCtlPhase						   */
	/* 2006. 8. 14 By Jongtak												       */
	/*=============================================================================*/	
	if (GAIN_ADD_IN_TURN==1)
{
    	if (lctcsu8TCSEngCtlPhase==0)
    {
    		lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_0_2,
	    		                           		             (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_2,
	    		                           		             (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_1_2,
	            		                                     (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE0_COMP_TURN_D_GAIN_2_2);    			                                                 
	}
    	else if (lctcsu8TCSEngCtlPhase==1)
    {
    		lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_2,
	    	                           		                 (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_2,
	    	                           		                 (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_2,
	           		                                         (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_2);    			                                                 
    		lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_0_2,
	    	                           		                 (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_2,
	    	                           		                 (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_1_2,
	           		                                         (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE1_COMP_TURN_D_GAIN_2_2);    			                                                 
   	}
   		else if (lctcsu8TCSEngCtlPhase==20)
		{
   			/* ICE에서 Split 오감지후 HOMO로 재 판단시 Target이 변하기 때문에
    		   P Gain이 0이면 높은 수준에서 Torque Hold됨(0810150904.das/16.888sec) */
    		lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_0_2,
	        	                       		                 (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_2,
	        	                       		                 (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_1_2,
	       			                                         (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE1_COMP_TURN_P_GAIN_2_2);    			                                                 
    		lts16AddDGainInTurn = 0;
		}
		else
    	{/*lctcsu8TCSEngCtlPhase=2*/
		    ;
		}
	}
	else
	{
		lts16AddPGainInTurn = 0;
		lts16AddDGainInTurn = 0;
	}
}

static void LCTCS_vCalPDGainInTurnFor2ndCyc(void)
{
	/*=============================================================================*/
	/* Purpose : 선회시 P Gain 증대					   							   */
	/* Input Variable : ETO_SUSTAIN_1SEC, det_alatm, ltcss16TargetSpinReduceAyTh   */
	/*					lts16WheelSpinError, lts16WheelSpinErrorDiff			   */
	/*                  lts16Phase7AddPgainInTurn, lts16Phase8AddPgainInTurn	   */
	/* Output Variable : lts16AddPGainInTurn, lctcsu8TCSEngCtlPhase						   */
	/* 2006. 8. 14 By Jongtak												       */
	/*=============================================================================*/

	if (GAIN_ADD_IN_TURN==1)
	{
		if (lctcsu8TCSEngCtlPhase==3)
	{
    		/* lts16WheelSpin > lts16TargetWheelSpin AND lts16WheelSpin Increase Tendency */
    		/* O_TARGET_P_SLOPE_P_GAIN */			  
	        lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE3_COMP_TURN_P_GAIN_2_2);			  
			/* O_TARGET_P_SLOPE_D_GAIN */				                                                 
			lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE3_COMP_TURN_D_GAIN_2_2);
	}
    	else if (lctcsu8TCSEngCtlPhase==4)
 	{
    		/* lts16WheelSpin > lts16TargetWheelSpin AND lts16WheelSpin Decrease Tendency */
    		/* O_TARGET_N_SLOPE_P_GAIN */
			lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE4_COMP_TURN_P_GAIN_2_2);				                                                 
			/* O_TARGET_P_SLOPE_D_GAIN */				                                                 
			lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE4_COMP_TURN_D_GAIN_2_2);
	}
		else if (lctcsu8TCSEngCtlPhase==5)
	{
    		/* lts16WheelSpin <= lts16TargetWheelSpin AND lts16WheelSpin Decrease Tendency */
    		/* O_TARGET_P_SLOPE_P_GAIN */
			lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE5_COMP_TURN_P_GAIN_2_2);				                                                 
    		/* O_TARGET_P_SLOPE_D_GAIN */				                                                 
			lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE5_COMP_TURN_D_GAIN_2_2);
	}
    	else if (lctcsu8TCSEngCtlPhase==6)
		{		
    		/* lts16WheelSpin <= lts16TargetWheelSpin AND lts16WheelSpin Increase Tendency */
    		/* O_TARGET_N_SLOPE_P_GAIN */
			lts16AddPGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_0_0, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_0_1, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_0, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_1, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_2_0, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_2_1, (int16_t)S8_PHASE6_COMP_TURN_P_GAIN_2_2);				                                                 
    		/* O_TARGET_P_SLOPE_D_GAIN */
			lts16AddDGainInTurn = LCTCS_s16IInterpDepAy4by3( (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_0_0, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_0_1, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_0_2,
	                           		                      	 (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_2,
	                           		                      	 (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_0, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_1, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_1_2,
	                                                       	 (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_2_0, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_2_1, (int16_t)S8_PHASE6_COMP_TURN_D_GAIN_2_2);
		}
		else{}
		}
		else
		{		
		lts16AddPGainInTurn = 0;
		lts16AddDGainInTurn = 0;
		}
	}

static void LCTCS_vForcedTorqRiseCtrl	(void)
	{
	if(lctcsu8FastTorqRecoveryStatus == FAST_TORQ_RECOVERY_MODE)
		{
    	lts16PGain1stCycle = 0;
    	lts16DGain1stCycle = 0;
    	
		if( (ETCS_ON==1) && ((GAIN_ADD_IN_TURN == 1)||(ltcss16ETCSEnterExitFlags == 4)) ) 
		{		
			ltu8TorqRiseRate = (uint8_t)LCTCS_s16IInterpDepAy4by3( (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_0_0, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_0_1, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_0_2,
	                               		                         (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_0, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_1, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_2,
	                               		                         (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_0, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_1, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_1_2,
	                                                             (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_2_0, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_2_1, (int16_t)U8_TORQ_RISE_RATE_1ST_TURN_2_2);        		                                           			                                                 		
		}
		else if(ETCS_ON==1)
		{
			ltu8TorqRiseRate = (uint8_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_TORQUE_RISE_RATE_1ST_0_0, (int16_t)U8_TORQUE_RISE_RATE_1ST_0_1, (int16_t)U8_TORQUE_RISE_RATE_1ST_0_2, (int16_t)U8_TORQUE_RISE_RATE_1ST_0_3,
			                                                 		(int16_t)U8_TORQUE_RISE_RATE_1ST_1_0, (int16_t)U8_TORQUE_RISE_RATE_1ST_1_1, (int16_t)U8_TORQUE_RISE_RATE_1ST_1_2, (int16_t)U8_TORQUE_RISE_RATE_1ST_1_3);
		}
		else  /* FTCS_ON==1 */
		{		
			ltu8TorqRiseRate = (uint8_t)LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_0_0, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_0_1, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_0_2, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_0_3,
		      	                                            		(int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_1_0, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_1_1, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_1_2, (int16_t)U8_FTCS_TORQ_RISE_RATE_1ST_1_3);
}
			#if __MU_EST_FOR_TCS
		if(lsabsu1MuEstReliableOK == 1)
		{
			if(lsabss16RoadGradGfilter > 5)		/* Hill */
		{
				ltu8TorqRiseRate = ltu8TorqRiseRate + U8_TORQUE_RISE_RATE_ADD;
		}
		else
		{
			;
		}	
		}
		else
		{
			;
		}
		    #endif					
		
			#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
		if(lctcsu1GearShiftFlag==1)
	{
			ltu8TorqRiseRate = ltu8TorqRiseRate + U8_TORQ_RISE_RATE_MODIFY;
		}
		else
		{
			;
		}
			#endif  /* __TCS_CTRL_DEC_IN_GR_SHFT_MT */	
					
   		tcs_cmd = cal_torq + ltu8TorqRiseRate;
	}
	else
	{
   		;
	}				
}
	
static void	LCTCS_vCallTCSCtrlCnt(void)
	{
    if(ETCS_ON==1)
	{
	    if(etcs_on_count < L_U8_TIME_10MSLOOP_1750MS )
		{
			etcs_on_count++;
		}
		else
		{
			;
		}	
	}
    else  /* FTCS_ON */
    {
	    if(ftcs_on_count < L_U8_TIME_10MSLOOP_1750MS )
    		{
			ftcs_on_count++;
    	}
	else
	{
		;
	}
	}

    if (CYCLE_2ND == 0)
	{
    	if  (lts16WheelSpin < lts16TargetWheelSpin)
		{
    		lctcss16LowSpinCntIn1stCycle = lctcss16LowSpinCntIn1stCycle + 1;
		}
		else
		{
    		lctcss16LowSpinCntIn1stCycle = lctcss16LowSpinCntIn1stCycle - 1;
		}	
    	lctcss16LowSpinCntIn1stCycle = LCTCS_s16ILimitRange(lctcss16LowSpinCntIn1stCycle, 0, L_U16_TIME_10MSLOOP_10S);
	}
	else
	{
    	lctcss16LowSpinCntIn1stCycle = 0;
	}	
	}

	#if __VARIABLE_MINIMUM_TORQ_LEVEL
static void LCTCS_vDecideMinTorqLvl(void)
		{
	lctcss16NewEngOKTorq = LCTCS_s16IInterpDepRPM3by3( S16_NEW_ENG_OK_TORQ_0_0, S16_NEW_ENG_OK_TORQ_0_1, S16_NEW_ENG_OK_TORQ_0_2, 
												       S16_NEW_ENG_OK_TORQ_1_0, S16_NEW_ENG_OK_TORQ_1_1, S16_NEW_ENG_OK_TORQ_1_2, 
	 											       S16_NEW_ENG_OK_TORQ_2_0, S16_NEW_ENG_OK_TORQ_2_1, S16_NEW_ENG_OK_TORQ_2_2);
		}
	#endif /* __VARIABLE_MINIMUM_TORQ_LEVEL */
	
int16_t  LCTCS_s16IInterpDepRPM3by3( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2)
		{
	/*=============================================================================*/
	/* Purpose : Vehicle Speed & Engine RPM별 Minimum Engine Torque 적용		   */
	/* Input Variable : InputValue_0_0, InputValue_0_1, InputValue_0_2, 		   */
	/*					InputValue_1_0, InputValue_1_1, InputValue_1_2,			   */
	/*					InputValue_2_0, InputValue_2_1, InputValue_2_2			   */
	/* Output Variable : OutputValue 						   				       */
	/* 2011. 1. 17 By HJH    												       */
	/*=============================================================================*/	
	int16_t OutputValue;	
	
/*skeon 110331 interpolation optime test*/
#ifndef OPTIME_MACFUNC_3	
	
	lts16TempArray3By3[0][0] = InputValue_0_0;
	lts16TempArray3By3[0][1] = InputValue_0_1;
	lts16TempArray3By3[0][2] = InputValue_0_2;
	lts16TempArray3By3[1][0] = InputValue_1_0;
	lts16TempArray3By3[1][1] = InputValue_1_1;
	lts16TempArray3By3[1][2] = InputValue_1_2;
	lts16TempArray3By3[2][0] = InputValue_2_0;
	lts16TempArray3By3[2][1] = InputValue_2_1;
	lts16TempArray3By3[2][2] = InputValue_2_2;
	
	for(lts8LoopCounter=0;lts8LoopCounter<3;lts8LoopCounter++)
	{
		lts16TempArray[lts8LoopCounter]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED1, lts16TempArray3By3[lts8LoopCounter][0],
						   										   (int16_t)U16_TCS_SPEED3, lts16TempArray3By3[lts8LoopCounter][1],
			 													   (int16_t)U16_TCS_SPEED4, lts16TempArray3By3[lts8LoopCounter][2]);
	}
	
#else
		lts16TempArray[0]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_0_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_0_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_0_2);
			 													   
		lts16TempArray[1]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_1_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_1_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_1_2);
			 													   
		lts16TempArray[2]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_2_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_2_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_2_2);

#endif	

	OutputValue = LCESP_s16IInter3Point((int16_t)eng_rpm, (int16_t)ENG_OK_RPM, lts16TempArray[0],
																	  											   	 2000,  lts16TempArray[1],
							   				     																	 3000,  lts16TempArray[2]);
	return OutputValue;	
	}
	
void LCTCS_vGainDecreaseInHighMu(void)
	{
	/*=============================================================================*/
	/* Purpose : 종가속도가 U16_AX_RANGE_MAX보다 큰 경우 1st Cycle의 P, D Gain을   */
	/*           줄여서 High-mu 가속시의 Hesitation 방지						   */
	/* Input Variable : acc_r, lts16WheelSpinError, lts16WheelSpinErrorDiff,       */
	/*                  lts16PGain1stCycle, lts16DGain1stCycle					   */
	/* Output Variable : lts16PGain1stCycle, lts16DGain1stCycle					   */
	/* 2006. 4. 26 By Jongtak												       */
	/*=============================================================================*/
#if (__TCS_MU_DETECTION == ENABLE)
	uint8_t lctcsu8TurnState4mu;

	tcs_tempW1 = (int16_t)McrAbs(delta_yaw_first);
	tcs_tempW2 = (int16_t)McrAbs(wstr);
	tcs_tempW3 = (int16_t)McrAbs(alat);	

	tcs_tempW0 = LCTCS_s16ILimitMinimum(S16_TCS_TURN_DET_BY_ALAT_TH, LAT_0G1G); /*14.11.06*/


	if ( (tcs_tempW1>YAW_3DEG)||(tcs_tempW2>ltcss16TargetSpinReduceSteerTh)||(tcs_tempW3>tcs_tempW0) )
	{
		lctcsu8TurnState4mu = 1;
	}
	else
	{
		lctcsu8TurnState4mu = 0;
	}
#endif
	
#if __MY08_TCS_DCT_H2L
	if ( (ltcsu1DctH2L==0) && ((ltcss16AccOfNonDrivenWheel > (int16_t)U16_AX_RANGE_MAX)) )
#else	/* #if __MY08_TCS_DCT_H2L */
		#if __MU_EST_FOR_TCS
	if ( lsabss16RoadMuEstbyEngFilt > (int16_t)U16_AX_RANGE_MAX )
		#else	
	
		#if (__TCS_MU_DETECTION == ENABLE)
	if (((lctcsu8TurnState4mu == 0)&&(lctcss16EstMuInstraight > (int16_t)U16_AX_RANGE_MAX))
		||((lctcsu8TurnState4mu == 1)&&(lctcss16EstMuInTurn > S16_MM_EMS))
		&&(lctcsu1FastRPMInc==0))
		#else
	if ((ltcss16AccOfNonDrivenWheel > (int16_t)U16_AX_RANGE_MAX)&&(lctcsu1FastRPMInc==0))
		#endif
	
		#endif
#endif	/* #if __MY08_TCS_DCT_H2L */
		{
		#if (__TCS_MU_DETECTION == ENABLE)
		if ((lctcsu8TCSEngCtlPhase==1)||(lctcsu8TCSEngCtlPhase==0))
		#else
		if (lctcsu8TCSEngCtlPhase==1)
		#endif
		{
			/* Gain Decrease Rate In High-mu */
			/* 07SWD #7 */
				#if __MU_EST_FOR_TCS			
			tcs_tempW0 = LCESP_s16IInter2Point(lsabss16RoadMuEstbyEngFilt, (int16_t)U16_AX_RANGE_MAX, 100,
                                                                           (int16_t)U16_AX_RANGE_HIGH_MU_MAX,  (int16_t)U8_AX_HIGH_MU_MAX_GAIN_FACTOR);
            	#else                                                               
			#if (__TCS_MU_DETECTION == ENABLE)
			
			if(lctcsu8TurnState4mu == 0)
			{
				tcs_tempW0 = LCESP_s16IInter2Point(lctcss16EstMuInstraight,	(int16_t)U16_AX_RANGE_MAX, 100,
                	                                            			(int16_t)U16_AX_RANGE_HIGH_MU_MAX,  (int16_t)U8_AX_HIGH_MU_MAX_GAIN_FACTOR);
			}
			else
			{
				tcs_tempW0 = LCESP_s16IInter2Point(lctcss16EstMuInTurn,	(int16_t)S16_MM_EMS, 100,
                	                                            		(int16_t)S16_HM_EMS,  (int16_t)U8_AX_HIGH_MU_MAX_GAIN_FACTOR);
            }
            
			#else
			tcs_tempW0 = LCESP_s16IInter2Point(ltcss16AccOfNonDrivenWheel, (int16_t)U16_AX_RANGE_MAX, 100,
                                                                           (int16_t)U16_AX_RANGE_HIGH_MU_MAX,  (int16_t)U8_AX_HIGH_MU_MAX_GAIN_FACTOR);
                #endif                                                           
                #endif                                                          
			lts16PGain1stCycle = (lts16PGain1stCycle*tcs_tempW0)/100;
			lts16DGain1stCycle = (lts16DGain1stCycle*tcs_tempW0)/100;
			lts16PGain1stCycle = LCTCS_s16ILimitMinimum(lts16PGain1stCycle, 1);
			lts16DGain1stCycle = LCTCS_s16ILimitMinimum(lts16DGain1stCycle, 1);
	}
		else {}
	}
	else {}
}

void LCTCS_vGainIncreaseInHighMu(void)
{
	/*=============================================================================*/
	/* Purpose : 종가속도가 U16_AX_RANGE_MAX보다 큰 경우 Phase 5,6의 P Gain을      */
	/*           증가시켜 High-mu 가속시의 Hesitation 방지						   */
	/* Input Variable : ltcss16AccOfNonDrivenWheel, lts16WheelSpinError,           */
	/*                  lts16WheelSpinErrorDiff, lts16PGainForETCS				   */
	/* Output Variable : lts16PGainForETCS  					   				   */
	/* 2008. 5. 19 By Jongtak												       */
	/*=============================================================================*/

		tcs_tempW3 = ltcss16AccOfNonDrivenWheel; 

		#if __MU_EST_FOR_TCS
	if (lsabss16RoadMuEstbyEngFilt > (int16_t)U16_AX_RANGE_MAX)
		#else
	if (tcs_tempW3 > (int16_t)U16_AX_RANGE_MAX)
    	#endif
	{
		if ( (lctcsu8TCSEngCtlPhase==5) || (lctcsu8TCSEngCtlPhase==6) )
    {
			if (ETCS_ON == 1)
    	{
				tcs_tempW1 = (int16_t)U8_AX_HIGH_MU_MAX_GAIN_INC_F;
	}
	else
	{
				tcs_tempW1 = (int16_t)U8_AX_HIGH_MU_MAX_GAIN_INC_F_S;
	}
				#if __MU_EST_FOR_TCS
			tcs_tempW0 = LCESP_s16IInter2Point(lsabss16RoadMuEstbyEngFilt, (int16_t)U16_AX_RANGE_MAX, 10,
                                                                           (int16_t)U16_AX_RANGE_HIGH_MU_MAX,  tcs_tempW1);
            	#else                                                               
			tcs_tempW0 = LCESP_s16IInter2Point(tcs_tempW3, (int16_t)U16_AX_RANGE_MAX, 10,
                                                                           (int16_t)U16_AX_RANGE_HIGH_MU_MAX,  tcs_tempW1);
    	#endif
			
			tcs_tempW0 = LCTCS_s16ILimitMaximum(tcs_tempW0, 600);
			if (ETCS_ON == 1)
	{
				lts16PGainForETCS = LCTCS_s16ILimitMaximum(lts16PGainForETCS, 50);
				lts16PGainForETCS = (lts16PGainForETCS*tcs_tempW0)/10;
				lts16PGainForETCS = LCTCS_s16ILimitMinimum(lts16PGainForETCS, 1);
	}
	else
	{
				lts16PGainForFTCS = LCTCS_s16ILimitMaximum(lts16PGainForFTCS, 50);
				lts16PGainForFTCS = (lts16PGainForFTCS*tcs_tempW0)/10;
				lts16PGainForFTCS = LCTCS_s16ILimitMinimum(lts16PGainForFTCS, 1);						
	}
			lctcsu1TCSGainIncInHighmu = 1;
	}
	else
	{
			lctcsu1TCSGainIncInHighmu = 0;
	}
	}
	else
	{
		lctcsu1TCSGainIncInHighmu = 0;
	}						
}
	
static void LCTCS_vGainDecreaseInGearShift(void)
{
	
	if(AUTO_TM == 1)
	{
    	if(gs_ena == 1)
 	{
    		if ((lctcsu8TCSEngCtlPhase == 3)||(lctcsu8TCSEngCtlPhase == 6))	/*14.11.06*/
    		{
    		if(ETCS_ON==1)
	{
    			lts16PGain = (int16_t)(((int32_t)lts16PGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E) / 100);
    			lts16DGain = (int16_t)(((int32_t)lts16DGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E) / 100);
	}
    		else /* FTCS */
	{
    			lts16PGain = (int16_t)(((int32_t)lts16PGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F) / 100);
    			lts16DGain = (int16_t)(((int32_t)lts16DGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F) / 100);
		}
    		}
    		else{}
    		/*CYCLE_2ND = 1;	*/	/*14.11.06*/
		}
		else
		{
    	;
}
        }
    else /* Manual Transmission */
        {
	      #if __TCS_CTRL_DEC_IN_GR_SHFT_MT    	
    	if(lctcsu1GearShiftFlag == 1)
        {
    		if ((lctcsu8TCSEngCtlPhase != 4)&&(lctcsu8TCSEngCtlPhase != 5))
    		{    		
    		if(ETCS_ON==1)
            {
    			lts16PGain = (int16_t)(((int32_t)lts16PGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E) / 100);
    			lts16DGain = (int16_t)(((int32_t)lts16DGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_E) / 100);
            }
    		else /* FTCS */
            {
    			lts16PGain = (int16_t)(((int32_t)lts16PGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F) / 100);
    			lts16DGain = (int16_t)(((int32_t)lts16DGain * U8_GAIN_DEC_FACTOR_IN_GR_SHFT_F) / 100);
            }
            }
    		else{}
    	}
	    	else
            {
    			;
            }
    	  #endif /* __TCS_CTRL_DEC_IN_GR_SHFT_MT */				
            }
            }

void LCTCS_vCalPDGain2ndCycForETCS(void)
            {
	/*=============================================================================*/
	/* Purpose : Homo mu에서 2nd Cycle의 P, D Gain 계산 			   			   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff, vref,        */
	/*                  ltcss16AccOfNonDrivenWheel								   */
	/* Output Variable : lts16PGainForETCS, lts16DGainForETCS					   */
	/* 2006. 4. 4 By Jongtak												       */
	/*=============================================================================*/
	if (lctcsu8TCSEngCtlPhase==3)
            {
		lts16PGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_0_0, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_0_1, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_0_2, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_1_0, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_1_1, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_1_2, (int16_t)U8_PHASE3_HOMO_MU_P_GAIN_1_3);
		lts16DGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_0_0, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_0_1, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_0_2, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_1_0, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_1_1, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_1_2, (int16_t)U8_PHASE3_HOMO_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==4)
            {
		lts16PGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_0_0, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_0_1, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_0_2, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_1_0, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_1_1, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_1_2, (int16_t)U8_PHASE4_HOMO_MU_P_GAIN_1_3);
		lts16DGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_0_0, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_0_1, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_0_2, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_1_0, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_1_1, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_1_2, (int16_t)U8_PHASE4_HOMO_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==5)
            {
		/* lts16WheelSpin <= lts16TargetWheelSpin AND lts16WheelSpin Decrease Tendency */
		/* B_TARGET_N_SLOPE_P_GAIN */
		lts16PGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_0_0, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_0_1, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_0_2, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_1_0, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_1_1, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_1_2, (int16_t)U8_PHASE5_HOMO_MU_P_GAIN_1_3);
		lts16DGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_0_0, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_0_1, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_0_2, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_1_0, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_1_1, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_1_2, (int16_t)U8_PHASE5_HOMO_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==6)
            {
		/* lts16WheelSpin <= lts16TargetWheelSpin AND lts16WheelSpin Increase Tendency */
		/* B_TARGET_P_SLOPE_P_GAIN */
		lts16PGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_0_0, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_0_1, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_0_2, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_1_0, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_1_1, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_1_2, (int16_t)U8_PHASE6_HOMO_MU_P_GAIN_1_3);
		lts16DGainForETCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_0_0, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_0_1, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_0_2, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_1_0, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_1_1, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_1_2, (int16_t)U8_PHASE6_HOMO_MU_D_GAIN_1_3);
            }
	else{}
            }

void LCTCS_vCalPDGain2ndCycForFTCS(void)
            {
	/*=============================================================================*/
	/* Purpose : Split mu에서2nd Cycle의 P, D Gain 계산      	 	   			   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff, vref,        */
	/*                  ltcss16AccOfNonDrivenWheel								   */
	/* Output Variable : lts16PGainForFTCS, lts16DGainForFTCS					   */
	/* 2006. 4. 4 By Jongtak												       */
	/*=============================================================================*/
	if (lctcsu8TCSEngCtlPhase==3)
            {
		/* lts16WheelSpin > lts16TargetWheelSpin AND lts16WheelSpin Increase Tendency */
		/* O_TARGET_P_SLOPE_P_GAIN */
		lts16PGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_0_0, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_0_1, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_0_2, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_1_0, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_1_1, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_1_2, (int16_t)U8_PHASE3_SPLIT_MU_P_GAIN_1_3);
		lts16DGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_0_0, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_0_1, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_0_2, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_1_0, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_1_1, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_1_2, (int16_t)U8_PHASE3_SPLIT_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==4)
            {
		/* lts16WheelSpin > lts16TargetWheelSpin AND lts16WheelSpin Decrease Tendency */
		/* O_TARGET_N_SLOPE_P_GAIN */
		lts16PGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_0_0, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_0_1, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_0_2, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_1_0, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_1_1, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_1_2, (int16_t)U8_PHASE4_SPLIT_MU_P_GAIN_1_3);
		lts16DGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_0_0, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_0_1, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_0_2, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_1_0, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_1_1, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_1_2, (int16_t)U8_PHASE4_SPLIT_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==5)
            {
		lts16PGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_0_0, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_0_1, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_0_2, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_1_0, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_1_1, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_1_2, (int16_t)U8_PHASE5_SPLIT_MU_P_GAIN_1_3);
		lts16DGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_0_0, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_0_1, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_0_2, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_1_0, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_1_1, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_1_2, (int16_t)U8_PHASE5_SPLIT_MU_D_GAIN_1_3);
            }
	else if (lctcsu8TCSEngCtlPhase==6)
            {
		/* lts16WheelSpin <= lts16TargetWheelSpin AND lts16WheelSpin Increase Tendency */
		/* B_TARGET_P_SLOPE_P_GAIN */
		lts16PGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_0_0, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_0_1, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_0_2, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_0_3,
		                                                  (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_1_0, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_1_1, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_1_2, (int16_t)U8_PHASE6_SPLIT_MU_P_GAIN_1_3);
		lts16DGainForFTCS = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_0_0, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_0_1, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_0_2, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_0_3,
		                                                  (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_1_0, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_1_1, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_1_2, (int16_t)U8_PHASE6_SPLIT_MU_D_GAIN_1_3);
            }
	else{}

  #if (__CAR == GM_TAHOE) || (__CAR == GM_C100)|| (__CAR ==GM_SILVERADO)
	if ( (vel_f_small - vref) < (int16_t)VREF_4_KPH )
            {
		if (lctcsu8TCSEngCtlPhase == 3)
            {
			lts16PGainForFTCS = lts16PGainForFTCS / 3;
			lts16DGainForFTCS = lts16DGainForFTCS / 3;
            }
            }
  #endif
            }

void LCTCS_vGain1stCycleTransition(void)
            {
	/*=============================================================================*/
	/* Purpose : 1st Cycle의 Homo-mu Tendency Counter와 Split-mu Tendency Counter에*/
	/*           의한 P,D Gain Transition(Homo-mu Gain <-> Split-mu Gain)		   */
	/* Input Variable : ETCS_ON, SplitTransitionCnt, lts16PGainForETCS, 		   */
	/*					lts16PGainForFTCS, lts16DGainForETCS, lts16DGainForFTCS,   */
	/*                  lts16WheelSpinError, ltcss16BothWheelSpinCnt,  			   */
	/*                  lts16PGain1stCycle, lts16DGain1stCycle	   				   */
	/* Output Variable : lts16PGain, lts16DGain				 					   */
	/* 2006. 10. 25 By Jongtak												       */
	/*=============================================================================*/

	if (ETCS_ON==1)
            {
		/* ETCS 1st Cycle Gain -> FTCS 1st Cycle Gain */   /* ETCS/FTCS Gain 구분 필요, 현재는 동일 */
		lts16PGain = LCESP_s16IInter2Point((int16_t)SplitTransitionCnt, 0, lts16PGain1stCycle,
										   (int16_t)U16_ONE_WHEEL_SPIN_LIMIT, lts16PGain1stCycle);
		lts16DGain = LCESP_s16IInter2Point((int16_t)SplitTransitionCnt, 0, lts16DGain1stCycle,
										   (int16_t)U16_ONE_WHEEL_SPIN_LIMIT, lts16DGain1stCycle);
            }
	else	/* FTCS_ON==1 */
            {
		/* FTCS 1st Cycle Gain -> ETCS 1st Cycle Gain */
		lts16PGain = LCESP_s16IInter2Point((int16_t)ltcss16BothWheelSpinCnt, 0, lts16PGain1stCycle,
										   (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT, lts16PGain1stCycle);
		lts16DGain = LCESP_s16IInter2Point((int16_t)ltcss16BothWheelSpinCnt, 0, lts16DGain1stCycle,
										   (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT, lts16DGain1stCycle);
            }
            }

void LCTCS_vGain2ndCycleTransition(void)
        {
	/*=============================================================================*/
	/* Purpose : 2nd Cycle의 Homo-mu Tendency Counter와 Split-mu Tendency Counter에*/
	/*           의한 P,D Gain Transition(Homo-mu Gain <-> Split-mu Gain)		   */
	/* Input Variable : ETCS_ON, SplitTransitionCnt, lts16PGainForETCS, 		   */
	/*					lts16PGainForFTCS, lts16DGainForETCS, lts16DGainForFTCS,   */
	/*                  lts16WheelSpinError, ltcss16BothWheelSpinCnt,  			   */
	/*                  lts16PGain1stCycle, lts16DGain1stCycle	   				   */
	/* Output Variable : lts16PGain, lts16DGain				 					   */
	/* 2006. 10. 25 By Jongtak												       */
	/*=============================================================================*/

	if (ETCS_ON==1)
    {
    	/* ETCS 2nd Cycle Gain -> FTCS 2nd Cycle Gain */
    	lts16PGain = LCESP_s16IInter2Point((int16_t)SplitTransitionCnt, 0, lts16PGainForETCS,
    									   (int16_t)U16_ONE_WHEEL_SPIN_LIMIT, lts16PGainForFTCS);
    	lts16DGain = LCESP_s16IInter2Point((int16_t)SplitTransitionCnt, 0, lts16DGainForETCS,
    									   (int16_t)U16_ONE_WHEEL_SPIN_LIMIT, lts16DGainForFTCS);
    }
	else	/* FTCS_ON==1 */
    {
    	/* FTCS 2nd Cycle Gain -> ETCS 2nd Cycle Gain */
    	lts16PGain = LCESP_s16IInter2Point((int16_t)ltcss16BothWheelSpinCnt, 0, lts16PGainForFTCS,
    									   (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT, lts16PGainForETCS);
    	lts16DGain = LCESP_s16IInter2Point((int16_t)ltcss16BothWheelSpinCnt, 0, lts16DGainForFTCS,
    									   (int16_t)U16_BOTH_WHEEL_SPIN_LIMIT, lts16DGainForETCS);
    }
}

void LCTCS_vCallController(void)
{
	/*=============================================================================*/
	/* Purpose : 토크 제어량 계산												   */
	/* Input Variable : , ,			   */
    /*                  ,  									   */
	/* Output Variable : , ,    */
	/* 2006. 4. 4 By Jongtak												       */
	/*=============================================================================*/

    LCTCS_vCallEngPDController();				/* ltcss16PDGainEffect 계산 */
    LCTCS_vMinimizeTruncationError();			/* ltcss16SteadyStateCtl 계산 */
	LCTCS_vSetBaseLevelForTorqCtl();

	LCTCS_vForcedTorqRiseCtrl();


	LCTCS_vCompensateTorqForL2Splt();

  	LCTCS_vCompTorqInGoodTracking();

	LCTCS_vSearchGoodTorqueLevel();
	LCTCS_vPreventBigSpin();	

  #if __TCS_VIBRATION_DETECTION
    LCTCS_vCtlEngineVibration();
  #endif /*__TCS_VIBRATION_DETECTION*/          	

  	LCTCS_vIncTorqAfterEEC();

  #if MAXIMUM_RPM_LIMIT
  	LCTCS_vDecTorqInExcessiveRPM();
  #endif	/* #if MAXIMUM_RPM_LIMIT */

	lctcsu8TCSEngCtlPhase_old = lctcsu8TCSEngCtlPhase;
}


void LCTCS_vCallEngPDController(void)
	{
	/*=============================================================================*/
	/* Purpose : PD Controller에 의한  토크 제어량 계산							   */
	/* Input Variable : lts16WheelSpinError, lts16WheelSpinErrorDiff,			   */
    /*                  lts16PGain, lts16DGain 									   */
	/* Output Variable : lts16MulPGainAndSpinError, lts16PGainEffect, 			   */
	/*                   lts16DGainEffect, ltcss16PDGainEffect                     */
	/* 2006. 4. 4 By Jongtak												       */
	/*=============================================================================*/

	lts16MulPGainAndSpinError = (int16_t)((int32_t)lts16PGain * lts16WheelSpinError);
	lts16PGainEffect = lts16MulPGainAndSpinError / (int16_t)U16_TCS_GAIN_SCALING_FACTOR;
	lts16DGainEffect = (int16_t)((int32_t)lts16DGain * lts16WheelSpinErrorDiff / (int16_t)U16_TCS_GAIN_SCALING_FACTOR);
	ltcss16PDGainEffect = (lts16PGainEffect + lts16DGainEffect) * -1;
	}
		
void LCTCS_vMinimizeTruncationError(void)
	{
	/*=============================================================================*/
	/* Purpose : Truncation Error에 의한 Dead Band서의 Steady State Error 방지	   */
	/* Input Variable : lts16PGainEffect, lts16DGainEffect, 				       */
	/*                  lts16MulPGainAndSpinError, U16_TCS_GAIN_SCALING_FACTOR     */
	/*					lts16WheelSpinError, system_loop_counter				   */
	/* Output Variable : ltcss16SteadyStateCtl									   */
	/* 2006. 8. 11 By Jongtak												       */
	/*=============================================================================*/
	if ( (lts16PGainEffect==0) && (lts16DGainEffect==0) )
    		{
		tcs_tempW0 = (McrAbs(lts16MulPGainAndSpinError)%(int16_t)U16_TCS_GAIN_SCALING_FACTOR);
		tcs_tempW1 = (int16_t)((int16_t)((int16_t)U16_TCS_GAIN_SCALING_FACTOR*2)/10);
		tcs_tempW2 = (int16_t)((int16_t)((int16_t)U16_TCS_GAIN_SCALING_FACTOR*5)/10);
		tcs_tempW3 = LCESP_s16IInter2Point(tcs_tempW0,tcs_tempW1, 5,
					 								  tcs_tempW2, 2);

		if (lts16WheelSpinError>0)	/* Spin > Target_Spin */
            	{
			if ( (system_loop_counter%(uint16_t)tcs_tempW3)==0 )
    		{
				ltcss16SteadyStateCtl = -1;		/* Torque Decrease */
	}
	else
	{
				ltcss16SteadyStateCtl = 0;
    	}
    }
		else						/* Spin <= Target_Spin */
    {
			if ( (system_loop_counter%(uint16_t)tcs_tempW3)==0 )
    {
				ltcss16SteadyStateCtl = 1;		/* Torque Increase */
		}
		else
		{
				ltcss16SteadyStateCtl = 0;
		}
	}
	}
	else
	{
		ltcss16SteadyStateCtl = 0;
	}
}

void LCTCS_vSetBaseLevelForTorqCtl(void)
{
	/*=============================================================================*/
	/* Purpose : 제어 시작시 토크 저감의 기준이 되는 Level 설정					   */
	/* Input Variable : ltcss16PDGainEffect, ltcss16SteadyStateCtl, 			   */
	/*                  cal_torq, drive_torq, eng_torq,     			           */
	/* Output Variable : tcs_cmd									               */
	/* 2006. 11. 1 By Jongtak												       */
	/*=============================================================================*/

	tcs_tempW0 = ltcss16PDGainEffect + ltcss16SteadyStateCtl;

  #if (__CAR == GM_TAHOE) || (__CAR == GM_C100)|| (__CAR ==GM_SILVERADO)
	/* 현재 Concept과 부합되지 않는 부분으로 임시 로직임 */
	if ( (FTCS_ON==1) && (lts16WheelSpinError < VREF_3_KPH) )
		{
		tcs_tempW0 = 1;
		}
	else {}
	/* 현재 Concept과 부합되지 않는 부분으로 임시 로직임 */
  #endif

	if (tcs_tempW0>=0)		/* Torque Increase */
		{
		tcs_cmd = cal_torq + tcs_tempW0;
	}
	else					/* Torque Reduction */
	{
		if (cal_torq > S16_TCS_ENG_TORQUE_MAX)
   	        {
			tcs_cmd = S16_TCS_ENG_TORQUE_MAX;				  				
		}
		else
		{
			tcs_cmd = cal_torq + tcs_tempW0;
	}

	  #if __CAR == GM_TAHOE || __CAR ==GM_SILVERADO
		if ( (lespu1TCU_SWI_GS == 1) && (lsu8DrvMode != DM_2H) )
	{
			/* AUTO MODE 또는 4H 모드 에서 Gear Up Shift 중인 상황에서
			   TCU에 의한 토크 저감 시점에 TCS 작동하게 되면 토크를
			   과도하게 저감하게 되므로, 이 경우는 drive_torq 기준으로
			   토크 저감 시작 함 */
			if (cal_torq >= drive_torq )
   	    	{
				tcs_cmd = drive_torq - 20 + tcs_tempW0;
	}
	else
	{
				tcs_cmd = cal_torq + tcs_tempW0;
	}
}
		else {}
	  #endif
   	    	}


	if ( ((lctcsu1BackupTorqActivationold == 1)&&(lctcsu1BackupTorqActivation == 0)&&(lctcsu1TCSGainIncInHighmu == 0)) 
	&& (U8_TCS_BACKUP_TORQUE_IMPROVE == 1) )
	{
		if (cal_torq > drive_torq)
		{
			tcs_cmd = drive_torq;
		}
		else
		{
		if (AUTO_TM == 1)
   	    {
			if (gs_ena == 0)
   	    {
				tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , eng_torq - 30 );
		}
		else
   	    {
				if ( (cal_torq - eng_torq) > U16_TCS_RED_ENG_TORQ_IN_GSC )
   	    {
					tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , cal_torq - 30 );
	}
	else
	{
					tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , eng_torq - 30 );
	}
	}		
   	}   	
   	else
	{
			#if __TCS_CTRL_DEC_IN_GR_SHFT_MT
				if (lctcsu1GearShiftFlag == 0)
   	{
				tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , eng_torq - 30 );
 	}
			else
	{
				if ( (cal_torq - eng_torq) > U16_TCS_RED_ENG_TORQ_IN_GSC )
   	        {
					tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , cal_torq - 30 );
	}
	else
	{
					tcs_cmd = LCTCS_s16IFindMinimum( tcs_cmd , eng_torq - 30 );
				}			
   	        }
			#endif /*__TCS_CTRL_DEC_IN_GR_SHFT_MT*/
   	    }
   	    }
	}
	else {}   
	}
					 							

void LCTCS_vCompensateTorqForL2Splt(void)
	{
	/*=============================================================================*/
	/* Purpose : Low-to-Split Suspect 감지 해제시 토크 레벨 원상 복귀			   */
	/* Input Variable : cal_torq, eng_torq, lctcss16TorqInput4L2SpltDct            */
	/* Output Variable : tcs_cmd												   */
	/* 2007. 7. 13 By Jongtak												       */
	/*=============================================================================*/
	if 		(lctcsu1Low2SplitSuspectFEdge == 1)
		{
		if (cal_torq > drive_torq)
		{
			tcs_cmd = eng_torq - lctcss16TorqInput4L2SpltDct;
	}
	else
	{
			tcs_cmd = tcs_cmd - lctcss16TorqInput4L2SpltDct;
	}
	
		tcs_cmd = LCTCS_s16ILimitMinimum(tcs_cmd, lctcss16BaseTorqLevel4L2SpltDct);
	}
	else
   	    {
		;
	}
}

void LCTCS_vCompTorqInGoodTracking(void)
{
	/*=============================================================================*/
	/* Purpose : Target Spin을 잘 추종하는 경우 주기적으로 Torque Up			   */
	/* Input Variable : GOOD_TARGET_TRACKING_MODE, ltcss16GoodTargetTrackingCnt,   */
	/*                  tcs_cmd			                                           */
	/* Output Variable : tcs_cmd 												   */
	/* 2007. 10. 30 By Jongtak												       */
	/*=============================================================================*/
	if (GOOD_TARGET_TRACKING_MODE == 1)
	{
		/* ltcss16GoodTargetTrackingCnt가 고착되었을 때 연속적인 토크 up 방지 */
		tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U8_TCS_GD_TRACK_SET_TIME, 1); 
		
		if ( (ltcss16GoodTargetTrackingCnt < L_U16_TIME_10MSLOOP_20S) && ((ltcss16GoodTargetTrackingCnt%tcs_tempW0)==1) )
		{
			tcs_cmd = tcs_cmd + U8_TCS_TORQ_UP_IN_GD_TRACKING;
   	    		}
   	    		else
			{
   	    			;
   	    		}
			}
			else
			{
   	      	;
   	    }       
			}


void LCTCS_vCheckPGainValidity(void)
			{
	/*=============================================================================*/
	/* Purpose : ESP Engine 제어 후에 작동하는 Fade Out 모드 종료후 부터 3초동안   */
	/*           토크 Rise Rate가 최소한 0.1%/scan 이 되도록 P Gain 조정           */
	/* Input Variable : lts16WheelSpinError, lts16PGain, lts16TargetWheelSpin,	   */
	/*                  U16_TCS_GAIN_SCALING_FACTOR, ETO_SUSTAIN_3SEC 			   */
	/*					ETO_SUSTAIN_6SEC										   */
	/* Output Variable :ltcss16MinimumPgain, P_GAIN_WARNING						   */
	/* 2006. 8. 11 By Jongtak												       */
	/*=============================================================================*/
	/* ltcss16MinimumPgain : 허용가능한 최소 P Gain 계산 */
	if (lts16WheelSpinError > 0)
	{	/* lts16WheelSpin > lts16TargetWheelSpin */
		ltcss16MinimumPgain = 1;
			}
			else
	{	/* lts16WheelSpin <= lts16TargetWheelSpin */
		if(lts16TargetWheelSpin>=(int16_t)U16_TCS_GAIN_SCALING_FACTOR*2)
			{
			ltcss16MinimumPgain = 1;
		}
		else
		{
			tcs_tempW0 = LCTCS_s16ILimitMinimum(lts16TargetWheelSpin, 1);
			if ( (ETO_SUSTAIN_3SEC==0)&&(ETO_SUSTAIN_6SEC==1) )
			{	/* ESP Engine 제어 후에 작동하는 Fade Out 모드 종료후부터
				   3초간 P Gain이 최소한 0.1%/scan이 되도록 조정		   */
				/* 올림 구현 */
				
				if( ((int16_t)U16_TCS_GAIN_SCALING_FACTOR%tcs_tempW0)>0 )
	{
					ltcss16MinimumPgain = ((int16_t)U16_TCS_GAIN_SCALING_FACTOR/lts16TargetWheelSpin) + 1;
		}
		else
		{
					ltcss16MinimumPgain = ((int16_t)U16_TCS_GAIN_SCALING_FACTOR/lts16TargetWheelSpin);
			}
			}
			else if ( (GAIN_ADD_IN_TURN==1) && (lts16TargetWheelSpin<VREF_10_KPH) )
			{
				/* 선회 시는 최소 2scan 당 0.1% 이상의 Torque Rise Rate 확보 */
				ltcss16MinimumPgain = (((int16_t)U16_TCS_GAIN_SCALING_FACTOR / 2) / tcs_tempW0) + 1;
			}
			else
			{
				ltcss16MinimumPgain = 1;
		}
	}
}

	/* Check P Gain Validity */
	if(	lts16PGain < ltcss16MinimumPgain)
	{	
		lts16PGain = ltcss16MinimumPgain;
		P_GAIN_WARNING = 1;
	}
	else 
	{
		P_GAIN_WARNING = 0;
	}
}

	
/* <skeon OPTIME_MACFUNC_2> 110303 */
#ifndef OPTIME_MACFUNC_2	

int16_t  LCTCS_s16ILimitRange( int16_t InputValue, int16_t MinValue, int16_t MaxValue )
{
	/*=============================================================================*/
	/* Purpose : InputValue의 최대값과 최소값을 제한							   */
	/* Input Variable : InputValue, MinValue, MaxValue	  		   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2006. 4. 10 By Jongtak												       */
	/*=============================================================================*/
	int16_t OutputValue;

	if 		(InputValue > MaxValue)
	{
		OutputValue = MaxValue;
	}
	else if (InputValue < MinValue)
	{
		OutputValue = MinValue;
	}
	else
	{
		OutputValue = InputValue;
   	    }
	return OutputValue;
	}
	
int16_t  LCTCS_s16ILimitMinimum( int16_t InputValue, int16_t MinValue )
	{
	/*=============================================================================*/
	/* Purpose : InputValue의 최소값을 제한										   */
	/* Input Variable : InputValue, MinValue	  				   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2006. 8. 3 By Jongtak												       */
	/*=============================================================================*/
	int16_t OutputValue;

	if (InputValue < MinValue)
	{
		OutputValue = MinValue;
	}
	else
	{
		OutputValue = InputValue;
	}
	return OutputValue;
  	}

uint16_t  LCTCS_u16ILimitMaximum( uint16_t InputValue, uint16_t MaxValue )
{
	/*=============================================================================*/
	/* Purpose : InputValue의 최대값을 제한										   */
	/* Input Variable : InputValue, MaxValue	  				   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2010. 8. 11 By ByoungJin Park  										       */
	/*=============================================================================*/
	uint16_t OutputValue;
	
	if (InputValue > MaxValue)
	{
		OutputValue = MaxValue;
	}
	else
	{
		OutputValue = InputValue;
	}	  
	return OutputValue;
}

int16_t  LCTCS_s16ILimitMaximum( int16_t InputValue, int16_t MaxValue )
{
	/*=============================================================================*/
	/* Purpose : InputValue의 최대값을 제한										   */
	/* Input Variable : InputValue, MaxValue	  				   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2006. 8. 8 By Jongtak												       */
	/*=============================================================================*/
	int16_t OutputValue;

	if (InputValue > MaxValue)
	{
		OutputValue = MaxValue;
	}
	else
	{
		OutputValue = InputValue;
	}
	return OutputValue;
}

int16_t  LCTCS_s16IFindMaximum( int16_t InputValue1, int16_t InputValue2)
{
	/*=============================================================================*/
	/* Purpose : InputValue1와 InputValue2 중 큰 값을 Return 					   */
	/* Input Variable : InputValue1, InputValue2		  		   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2006. 4. 10 By Jongtak												       */
	/*=============================================================================*/
	int16_t OutputValue;

	if 		(InputValue1 > InputValue2)
	{
		OutputValue = InputValue1;
	}
   	else
	{
		OutputValue = InputValue2;
	}
	return OutputValue;
}

int16_t  LCTCS_s16IFindMinimum( int16_t InputValue1, int16_t InputValue2)
{
	/*=============================================================================*/
	/* Purpose : InputValue1와 InputValue2 중 작은 값을 Return 					   */
	/* Input Variable : InputValue1, InputValue2		  		   				   */
	/* Output Variable : OutputValue 						   				       */
	/* 2006. 8. 13 By Jongtak												       */
	/*=============================================================================*/
	int16_t OutputValue;

	if 	(InputValue1 > InputValue2)
		{
		OutputValue = InputValue2;
		}
		else
		{
		OutputValue = InputValue1;
		}
	return OutputValue;
		}

#endif
/* </skeon OPTIME_MACFUNC_2> */


int16_t  LCTCS_s16IInterpDepAccel2by4( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_0_3, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_1_3)
		{
	int16_t OutputValue;	
	
/* skeon 110325 interpolation optime test */
#ifndef OPTIME_MACFUNC_3	
	
	lts16TempArray2By4[0][0] = InputValue_0_0;
	lts16TempArray2By4[0][1] = InputValue_0_1;
	lts16TempArray2By4[0][2] = InputValue_0_2;
	lts16TempArray2By4[0][3] = InputValue_0_3;
	lts16TempArray2By4[1][0] = InputValue_1_0;
	lts16TempArray2By4[1][1] = InputValue_1_1;
	lts16TempArray2By4[1][2] = InputValue_1_2;
	lts16TempArray2By4[1][3] = InputValue_1_3;
	
	
	for(lts8LoopCounter=0;lts8LoopCounter<2;lts8LoopCounter++)
		{
		lts16TempArray[lts8LoopCounter]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, lts16TempArray2By4[lts8LoopCounter][0],
					   											   (int16_t)U16_TCS_SPEED2, lts16TempArray2By4[lts8LoopCounter][1],
		 														   (int16_t)U16_TCS_SPEED3, lts16TempArray2By4[lts8LoopCounter][2],
															       (int16_t)U16_TCS_SPEED4, lts16TempArray2By4[lts8LoopCounter][3]);
		}
#else

		lts16TempArray[0]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_0_0,
					   											   (int16_t)U16_TCS_SPEED2, InputValue_0_1,
		 														   (int16_t)U16_TCS_SPEED3, InputValue_0_2,
															       (int16_t)U16_TCS_SPEED4, InputValue_0_3);

		lts16TempArray[1]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_1_0,
					   											   (int16_t)U16_TCS_SPEED2, InputValue_1_1,
		 														   (int16_t)U16_TCS_SPEED3, InputValue_1_2,
															       (int16_t)U16_TCS_SPEED4, InputValue_1_3);

#endif	
	
  		#if __MU_EST_FOR_TCS	
	OutputValue = LCESP_s16IInter2Point(lsabss16RoadMuEstbyEngFilt, (int16_t)U16_AX_RANGE_MIN, lts16TempArray[0],
                                                                    (int16_t)U16_AX_RANGE_MAX, lts16TempArray[1]);							   				       
		#else
	OutputValue = LCESP_s16IInter2Point(ltcss16AccOfNonDrivenWheel, (int16_t)U16_AX_RANGE_MIN, lts16TempArray[0],
                                                                    (int16_t)U16_AX_RANGE_MAX, lts16TempArray[1]);							   				       
		#endif                                                                    		                                                                    
	return OutputValue;	
	}

int16_t  LCTCS_s16IInterpDepAccel3by4( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_0_3, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_1_3, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2, int16_t InputValue_2_3)
		{
	int16_t OutputValue;
	
/* skeon 110325 interpolation optime test */
#ifndef OPTIME_MACFUNC_3	
	
	lts16TempArray3By4[0][0] = InputValue_0_0;
	lts16TempArray3By4[0][1] = InputValue_0_1;
	lts16TempArray3By4[0][2] = InputValue_0_2;
	lts16TempArray3By4[0][3] = InputValue_0_3;
	lts16TempArray3By4[1][0] = InputValue_1_0;
	lts16TempArray3By4[1][1] = InputValue_1_1;
	lts16TempArray3By4[1][2] = InputValue_1_2;
	lts16TempArray3By4[1][3] = InputValue_1_3;
	lts16TempArray3By4[2][0] = InputValue_2_0;
	lts16TempArray3By4[2][1] = InputValue_2_1;
	lts16TempArray3By4[2][2] = InputValue_2_2;
	lts16TempArray3By4[2][3] = InputValue_2_3;
		
	for(lts8LoopCounter=0;lts8LoopCounter<3;lts8LoopCounter++)
		{
		lts16TempArray[lts8LoopCounter]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, lts16TempArray3By4[lts8LoopCounter][0],
					   											   (int16_t)U16_TCS_SPEED2, lts16TempArray3By4[lts8LoopCounter][1],
		 														   (int16_t)U16_TCS_SPEED3, lts16TempArray3By4[lts8LoopCounter][2],
															       (int16_t)U16_TCS_SPEED4, lts16TempArray3By4[lts8LoopCounter][3]);
	}
#else

		lts16TempArray[0]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_0_0,
					   											   (int16_t)U16_TCS_SPEED2, InputValue_0_1,
		 														   (int16_t)U16_TCS_SPEED3, InputValue_0_2,
															       (int16_t)U16_TCS_SPEED4, InputValue_0_3);

		lts16TempArray[1]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_1_0,
					   											   (int16_t)U16_TCS_SPEED2, InputValue_1_1,
		 														   (int16_t)U16_TCS_SPEED3, InputValue_1_2,
															       (int16_t)U16_TCS_SPEED4, InputValue_1_3);

		lts16TempArray[2]=LCESP_s16IInter4Point(vref,(int16_t)U16_TCS_SPEED1, InputValue_2_0,
					   								 (int16_t)U16_TCS_SPEED2, InputValue_2_1,
		 											 (int16_t)U16_TCS_SPEED3, InputValue_2_2,
													 (int16_t)U16_TCS_SPEED4, InputValue_2_3);
#endif


	OutputValue = LCESP_s16IInter3Point(lctcss16EstMuInstraight, (int16_t)U16_AX_RANGE_MIN,         lts16TempArray[0],
                                                    			 (int16_t)U16_AX_RANGE_MAX,         lts16TempArray[1],
                                                    			 (int16_t)U16_AX_RANGE_HIGH_MU_MAX, lts16TempArray[2]);
	return OutputValue;	
}



int16_t  LCTCS_s16IInterpDepAy4by3( int16_t InputValue_0_0, int16_t InputValue_0_1, int16_t InputValue_0_2, int16_t InputValue_1_0, int16_t InputValue_1_1, int16_t InputValue_1_2, int16_t InputValue_2_0, int16_t InputValue_2_1, int16_t InputValue_2_2, int16_t InputValue_3_0, int16_t InputValue_3_1, int16_t InputValue_3_2)
{
	int16_t OutputValue;
	
/* skeon 110331 interpolation optime test*/
#ifndef OPTIME_MACFUNC_3	
		
	lts16TempArray4By3[0][0] = InputValue_0_0;
	lts16TempArray4By3[0][1] = InputValue_0_1;
	lts16TempArray4By3[0][2] = InputValue_0_2;
	lts16TempArray4By3[1][0] = InputValue_1_0;
	lts16TempArray4By3[1][1] = InputValue_1_1;
	lts16TempArray4By3[1][2] = InputValue_1_2;
	lts16TempArray4By3[2][0] = InputValue_2_0;
	lts16TempArray4By3[2][1] = InputValue_2_1;
	lts16TempArray4By3[2][2] = InputValue_2_2;
	lts16TempArray4By3[3][0] = InputValue_3_0;
	lts16TempArray4By3[3][1] = InputValue_3_1;
	lts16TempArray4By3[3][2] = InputValue_3_2;	
	
	for(lts8LoopCounter=0;lts8LoopCounter<4;lts8LoopCounter++)
	{
		lts16TempArray[lts8LoopCounter]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED2, lts16TempArray4By3[lts8LoopCounter][0],
						   										   (int16_t)U16_TCS_SPEED3, lts16TempArray4By3[lts8LoopCounter][1],
			 													   (int16_t)U16_TCS_SPEED4, lts16TempArray4By3[lts8LoopCounter][2]);
	}
	
#else	
		lts16TempArray[0]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED2, InputValue_0_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_0_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_0_2);

		lts16TempArray[1]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED2, InputValue_1_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_1_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_1_2);

		lts16TempArray[2]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED2, InputValue_2_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_2_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_2_2);
			 													   
		lts16TempArray[3]=LCESP_s16IInter3Point(vref,(int16_t)U16_TCS_SPEED2, InputValue_3_0,
						   										   (int16_t)U16_TCS_SPEED3, InputValue_3_1,
			 													   (int16_t)U16_TCS_SPEED4, InputValue_3_2);			 													   

#endif
	
	
	OutputValue = LCESP_s16IInter4Point(lctcss16EstMuInTurn, S16_LM_EMS, lts16TempArray[0],
											   	   S16_MM_EMS, lts16TempArray[1],
											   	   S16_MHM_EMS,lts16TempArray[2],
							   				       S16_HM_EMS, lts16TempArray[3]);
	return OutputValue;	
		}


	
#if TORQ_RECOVERY_DEPEND_ON_STEER
static void LCTCS_vDctSteerToZero(void)
{
	/* 조향각이 0도로 가는 경향 판단 */
	if 		( (wstr>WSTR_25_DEG) && (wstr_dot<0) )
{
		lctcss8SteerToZeroCnt = lctcss8SteerToZeroCnt + 1;
}
	else if ( (wstr>WSTR_25_DEG) && (wstr_dot>0) )
	{
		lctcss8SteerToZeroCnt = 0;
}
	else if ( (wstr<-WSTR_25_DEG) && (wstr_dot>0) )
{	
		lctcss8SteerToZeroCnt = lctcss8SteerToZeroCnt + 1;		
	}
	else if ( (wstr<-WSTR_25_DEG) && (wstr_dot<0) )
	{
		lctcss8SteerToZeroCnt = 0;
	}
	else
	{
		lctcss8SteerToZeroCnt = 0;				
				}
	lctcss8SteerToZeroCnt = (int8_t)LCTCS_s16ILimitRange((int16_t)lctcss8SteerToZeroCnt, 0, 127);

	if (lctcss8SteerToZeroCnt > (int8_t)U8_TCS_STEER_TO_ZERO_CNT)
				{
		lctcsu1SteerToZero = 1;
	}
	else
				{
		lctcsu1SteerToZero = 0;		
	}
        }
#endif	/* #if TORQ_RECOVERY_DEPEND_ON_STEER */  

#if MAXIMUM_RPM_LIMIT
static void LDTCS_vCalAllowableMaxRPM(void)
        {
	if (gear_pos==GEAR_POS_TM_1)
        	{
		tcs_tempW1 = (int16_t)U8_EDC_TM_1_GEAR_RATIO;
		if ( (tcs_tempW1 > 0) && (TCS_STUCK_ON == 0) && (ENG_STALL == 0) )
        		{
			/* x[rpm] * 60 * 8 * 10  / GearRatio * Circumference of Tire / 1000 / 1000 */
			/*if (vref5 > VREF_5_KPH)*/
			/*{	*/
				/*Maximum RPM Limit Control */
				lctcss16Spd1ForRPMLimit = (int16_t)(((((int32_t)S16_LOW_SPD_RPM * 600) / 1000) * U16_TIRE_L) / 1000);
				lctcss16Spd1ForRPMLimit = lctcss16Spd1ForRPMLimit / (int16_t)U8_EDC_TM_1_GEAR_RATIO;
				lctcss16Spd1ForRPMLimit = lctcss16Spd1ForRPMLimit * 8 - VREF_9_KPH;
			    lctcss16Spd1ForRPMLimit = LCTCS_s16ILimitMinimum(lctcss16Spd1ForRPMLimit, 0);
			
				lctcss16Spd2ForRPMLimit = (int16_t)(((((int32_t)S16_RED_ZONE_RPM * 600) / 1000) * U16_TIRE_L) / 1000);
				lctcss16Spd2ForRPMLimit = lctcss16Spd2ForRPMLimit / (int16_t)U8_EDC_TM_1_GEAR_RATIO;
				lctcss16Spd2ForRPMLimit = lctcss16Spd2ForRPMLimit * 8 - VREF_9_KPH;		
				lctcss16Spd2ForRPMLimit = LCTCS_s16ILimitMinimum(lctcss16Spd2ForRPMLimit, 0);

				lctcss16AllowableMaxRPM = LCESP_s16IInter2Point(vref5, lctcss16Spd1ForRPMLimit, S16_LOW_SPD_RPM,
																   lctcss16Spd2ForRPMLimit, S16_RED_ZONE_RPM);
			/*}
        		else
        		{
				lctcss16AllowableMaxRPM = 8000;
			}*/
			/*Minimum RPM Limit Control */													   
			lctcss16Spd1ForRPMLimit = (int16_t)(((((int32_t)S16_LOW_SPD_MIN_RPM * 600) / 1000) * U16_TIRE_L) / 1000);
			lctcss16Spd1ForRPMLimit = lctcss16Spd1ForRPMLimit / (int16_t)U8_EDC_TM_1_GEAR_RATIO;
			lctcss16Spd1ForRPMLimit = lctcss16Spd1ForRPMLimit * 8 + VREF_3_KPH;
			lctcss16Spd1ForRPMLimit = LCTCS_s16ILimitMinimum(lctcss16Spd1ForRPMLimit, 0);
			
			lctcss16Spd2ForRPMLimit = (int16_t)(((((int32_t)S16_HIGH_SPD_MIN_RPM * 600) / 1000) * U16_TIRE_L) / 1000);
			lctcss16Spd2ForRPMLimit = lctcss16Spd2ForRPMLimit / (int16_t)U8_EDC_TM_1_GEAR_RATIO;
			lctcss16Spd2ForRPMLimit = lctcss16Spd2ForRPMLimit * 8 + VREF_3_KPH;		
			lctcss16Spd2ForRPMLimit = LCTCS_s16ILimitMinimum(lctcss16Spd2ForRPMLimit, 0);
																			   
			lctcss16AllowableMinRPM = LCESP_s16IInter2Point(vref5, lctcss16Spd1ForRPMLimit, S16_LOW_SPD_MIN_RPM,
																   lctcss16Spd2ForRPMLimit, S16_HIGH_SPD_MIN_RPM);																   
        	}
        	else
        	{
			lctcss16AllowableMaxRPM = 8000;
			lctcss16AllowableMinRPM = (int16_t)ENG_STALL_RPM;
        	}
        }
        else
	{	/* No Limit */
		lctcss16AllowableMaxRPM = 8000;
		lctcss16AllowableMinRPM = (int16_t)ENG_STALL_RPM;
        }
        }

static void LDTCS_vSetOverMaxRPMFlag(void)
        {
	if ( (ETCS_ON==1) || (FTCS_ON==1) )
        {
		if (lctcsu1OverMaxRPM == 0)
        	{
			if ((eng_rpm > (uint16_t)lctcss16AllowableMaxRPM) && (slope_eng_rpm > (int16_t)S8_TCS_RPM_SLOPE_MAXLIMIT))
        	{
				lctcsu1OverMaxRPM = 1;
    	}
    	else
    	{
    		;
    	}		        		        
			lctcss16OverMaxRPMCnt = 0;
			lctcss16OverMaxRPMCntold = 0;
    }   	
    else
		{	/* lctcsu1OverMaxRPM == 1 */
			tcs_tempW0 = lctcss16AllowableMaxRPM - 200;
			tcs_tempW1 = (int16_t)S8_TCS_RPM_SLOPE_MAXLIMIT -1;
			if 		((eng_rpm < (uint16_t)tcs_tempW0) || (slope_eng_rpm < tcs_tempW1))
    {
				lctcsu1OverMaxRPM = 0;
            }
			else if (eng_rpm < (uint16_t)lctcss16AllowableMaxRPM)
            {
				/* lctcss16OverMaxRPMCnt hold */
            }
            else
            {
				lctcss16OverMaxRPMCnt = lctcss16OverMaxRPMCnt + 1;
        }
        }
                
		if (lctcsu1UnderMinRPM == 0)
        	{
			if ((eng_rpm < (uint16_t)lctcss16AllowableMinRPM) && (slope_eng_rpm < (int16_t)S8_TCS_RPM_SLOPE_MINLIMIT))
        		{
				lctcsu1UnderMinRPM = 1;
        		}
        		else
        		{
        			;
        		}
			lctcss16UnderMinRPMCnt = 0;
        	}
        	else
		{	/* lctcsu1UnderMinRPM == 1 */
			tcs_tempW2 = lctcss16AllowableMinRPM + 200;
			tcs_tempW3 = (int16_t)S8_TCS_RPM_SLOPE_MINLIMIT +1;
			if 		((eng_rpm > (uint16_t)tcs_tempW2) || (slope_eng_rpm > tcs_tempW3))
        	{
				lctcsu1UnderMinRPM = 0;
        }
			else if (eng_rpm > (uint16_t)lctcss16AllowableMinRPM)
        {
				/* lctcss16UnderMinRPMCnt hold */
        }
        else
        {
				lctcss16UnderMinRPMCnt = lctcss16UnderMinRPMCnt + 1;
        	}
        	}		        
    	}
    	else
    	{
		lctcsu1OverMaxRPM = 0;
		lctcss16OverMaxRPMCnt = 0;
		lctcss16OverMaxRPMCntold = 0;
		lctcsu1UnderMinRPM = 0;
		lctcss16UnderMinRPMCnt = 0;
		lctcss16UnderMinRPMCntold = 0;
    }   	
	lctcss16OverMaxRPMCnt = LCTCS_s16ILimitMaximum(lctcss16OverMaxRPMCnt, L_U16_TIME_10MSLOOP_3MIN);
	lctcss16UnderMinRPMCnt = LCTCS_s16ILimitMaximum(lctcss16UnderMinRPMCnt, L_U16_TIME_10MSLOOP_3MIN);
    }       

static void LCTCS_vDecTorqInExcessiveRPM(void)
        	{
	int16_t s16RpmCtrlDropTq;	
	int16_t s16RpmCtrlUpTq;	
	if (lctcsu1OverMaxRPM==1)
            {
		tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U16_TORQ_DROP_PERIOD_EX_RPM, 10);
		tcs_tempW0 = lctcss16OverMaxRPMCnt%tcs_tempW0;
		s16RpmCtrlDropTq = LCESP_s16IInter4Point(slope_eng_rpm,0, (int16_t)U16_TORQ_DROP_AMOUNT_EX_RPM,
		                                                       5, (int16_t)U16_TORQ_DROP_AMOUNT_EX_RPM*2,
		                                                       10,(int16_t)U16_TORQ_DROP_AMOUNT_EX_RPM*3,
		                                                       20,(int16_t)U16_TORQ_DROP_AMOUNT_EX_RPM*4);
		s16RpmCtrlDropTq = LCTCS_s16ILimitMaximum(s16RpmCtrlDropTq, 200);
		if ((tcs_tempW0==1)&&(lctcss16OverMaxRPMCntold!=lctcss16OverMaxRPMCnt))
            {
			if((cal_torq - s16RpmCtrlDropTq) < tcs_cmd)
            {
				tcs_cmd = cal_torq - s16RpmCtrlDropTq;
            }
			else { }
        }
        else
        {
        	;
        }
        		}
	else if (lctcsu1UnderMinRPM==1)
        		{
		tcs_tempW1 = LCTCS_s16ILimitMinimum((int16_t)U16_TORQ_UP_PERIOD_UN_RPM, 10);
		tcs_tempW1 = lctcss16UnderMinRPMCnt%tcs_tempW1;
		s16RpmCtrlUpTq = LCESP_s16IInter4Point(slope_eng_rpm,-20,(int16_t)U16_TORQ_UP_AMOUNT_UN_RPM*4,
		                                                     -10,(int16_t)U16_TORQ_UP_AMOUNT_UN_RPM*3,
		                                                     -5, (int16_t)U16_TORQ_UP_AMOUNT_UN_RPM*2,
		                                                     0,  (int16_t)U16_TORQ_UP_AMOUNT_UN_RPM);
        s16RpmCtrlUpTq = LCTCS_s16ILimitMaximum(s16RpmCtrlUpTq, 200);		                                                     
		if ((tcs_tempW1==1)&&(lctcss16UnderMinRPMCntold!=lctcss16UnderMinRPMCnt))
        	{
			if((cal_torq + s16RpmCtrlUpTq) > tcs_cmd)
        {
				tcs_cmd = cal_torq + s16RpmCtrlUpTq;
        }
			else { }
        }
        else
        {
          	;
        }       
    	}
    	else
    	{
    		;
    	}		        		        
	lctcss16OverMaxRPMCntold = lctcss16OverMaxRPMCnt;
	lctcss16UnderMinRPMCntold = lctcss16UnderMinRPMCnt;
    }   	

#endif		/* #if MAXIMUM_RPM_LIMIT */  
/* NEW ETCS STRUCTURE ↗*/


#endif      /* TAG2 */
	  		
/* NEW TCS STRUCTURE*/
#if __TCS   /* TAG3 */
	  

void LCTCS_vCalSpin(struct W_STRUCT *WL_temp)
    {
	WL = WL_temp;
  #if __VDC
	WL->spin = (int16_t)WL->ltcss16MovingAveragedWhlSpd - WL->wvref;
	WL->lctcss16SpinRaw = WL->vrad_crt - WL->wvref;
  #else	
	WL->spin = (int16_t)WL->ltcss16MovingAveragedWhlSpd - vref;
	WL->lctcss16SpinRaw = WL->vrad_crt - vref;
    	#endif    			           
    /* For 4WD, vref2 and vref are same -> distinction between 2WD and 4WD is needless */
}    	


static void LCTCS_vEstMuRefCoordinator(void)
{
	  		
#if (__TCS_MU_DETECTION == ENABLE)
	if(lctcsu8TCSEngCtlPhase <= 1)
	{		
		tcs_tempW0 = (int16_t)McrAbs(nor_alat);
		lctcss16EstMuInTurn = LCTCS_s16IFindMaximum(lctcs161stEstMuWtAlat, tcs_tempW0); 
		lctcss16EstMuInstraight = lctcs161stEstMu;
	}
	else
	{
		lctcss16EstMuInTurn	= det_alatm;
		lctcss16EstMuInstraight = acc_r;
	}
#else
		lctcss16EstMuInTurn	= det_alatm;
		lctcss16EstMuInstraight = acc_r;
#endif
	  
}

#endif      /* TAG3 */
/* NEW TCS STRUCTURE */
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCTCSCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
