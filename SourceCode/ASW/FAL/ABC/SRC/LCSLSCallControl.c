/******************************************************************************
* Project Name: Straight Line Stability Control
* File: LDSLSCallActHW.C
* Date: 06 March 2008
***************************************************************************** 
*  Modification   Log                                             
*  Date           Author           Description                    
*  -------       ----------    --------------------------------------------      
*  08.03.06      Youngjoo Cho    Initial Release
*******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCSLSCallControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/



#include "LCSLSCallControl.h"
#include "LCCBCCallControl.h"
#include "LCABSCallControl.h"

#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LSESPFilterEspSensor.h"



/* Local Definiton  *********************************************************/
        
                              
/* Tuning Parameter *********************************************************/



/* Variables Definition*******************************************************/


#if __SLS

struct  U8_BIT_STRUCT_t SLSC0;
struct  U8_BIT_STRUCT_t SLSC1;
struct  U8_BIT_STRUCT_t SLSC2;
struct  U8_BIT_STRUCT_t SLSC3;
struct  U8_BIT_STRUCT_t SLSC4;
struct  U8_BIT_STRUCT_t SLSC5;
struct  U8_BIT_STRUCT_t SLSC6;

int16_t delta_moment_q_sls;
int16_t SLS_fade_out_max; 

int8_t lcSLSu8ControlModeRL;
int8_t lcSLSu8ControlModeRR;
int8_t lcSLSu8FadeOutModeRL;
int8_t lcSLSu8FadeOutModeRR; 

uint8_t SLS_target_hold_cnt; 
uint8_t SLS_rate_counter_rl; 
uint8_t SLS_rate_counter_rr; 
uint8_t SLS_FADE_OUT_rl_cnt; 
uint8_t SLS_FADE_OUT_rr_cnt; 
uint8_t SLS_target_fade_cnt;  
uint8_t SLS_fade_counter_rr; 
uint8_t SLS_fade_counter_rl; 
          
int16_t ldslsEstYawrate1, ldslsEstYawrate2;              
int16_t ldslsEstYawrate1Old,  ldslsEstYawrate2Old;
              
int16_t sls_delta_yaw_first2_buf[3]; 
int16_t sls_delta_yaw_first2, sls_delta_yaw_first2_old ;
int16_t sls_yaw_error_under_dot2 ;
int16_t sls_yaw_error_under_dot2_old ;
int16_t sls_delyaw_enter_th, sls_delyaw_exit_th, sls_delyaw_out_th ;

int16_t SLS_target_delta_p_rear, SLS_target_p_rear, sls_yawg_fail_fade_cnt, sls_yawg_fail_fade_weight, delta_moment_q_sls_old ;

#endif	/* #if __SLS */

/* LocalFunction prototype ***************************************************/ 
#if __SLS

void    LDSLS_vEstYawrate(void);
void    LCSLS_vCallControl(void); 
void    LCSLS_vCallDetection(void);  
void    LCSLS_vCallCalControlPressure(void); 
void    LCSLS_vCallControlFadeOut(void);

#endif

/* Implementation*************************************************************/
#if __SLS

void LCSLS_vCallControl(void)
{ 

#if __SLS_ENABLE_BY_CALIBRATION == ENABLE
  	
	if(U8_SLS_ENABLE>0)
	{
		lcslsu1CtrlEnable = 1;
	}
	else
	{
		lcslsu1CtrlEnable = 0;
	}
	if(lcslsu1CtrlEnable == 1)
#endif  

	{

        LDSLS_vEstYawrate();   /* ---Yaw-rate Estimation--- */     
        LCSLS_vCallDetection();   /* SLS Entrance Condition */
        LCSLS_vCallCalControlPressure();  
        LCSLS_vCallControlFadeOut(); 
        
        SLS_ON_old = SLS_ON ;   
        SLS_ACTIVE_rl_old = SLS_ACTIVE_rl ;   
        SLS_ACTIVE_rr_old = SLS_ACTIVE_rr ;     

	}
}

void LDSLS_vEstYawrate(void)
{  
  /*---------------------------------------------------------------------*/
  /*                vrad_out - vrad_in     1000             180          */
  /*    Yaw-Rate = -------------------- * ------- (rad/s)* ----- (deg/s) */
  /*                   Track_Width         8*3.6             pi          */
  /*---------------------------------------------------------------------*/    
  
  /*cycle time   
  #if __REAR_D
    esp_tempW0 = TRACK_WIDTH_F;
    esp_tempW5 = TRACK_WIDTH_R;
    esp_tempW6 = RR.vrad_crt - RL.vrad_crt;
  #else
    esp_tempW0 = TRACK_WIDTH_R;
    esp_tempW5 = TRACK_WIDTH_F;
    esp_tempW6 = FR.vrad_crt - FL.vrad_crt;
  #endif
  */  
                                            
    /* tempW2 = 100;
    tempW1 = RR.vrad_crt - RL.vrad_crt;
    tempW0 = 1;
    s16muls16divs16();    
    
    tempW2 = 2000;
    tempW1 = tempW3;
    tempW0 = TRACK_WIDTH_R;
    s16muls16divs16(); */
    
    ldslsEstYawrate1Old = ldslsEstYawrate1 ;
    
    esp_tempW2 = (int16_t)( (((int32_t)100)*(RR.vrad_crt - RL.vrad_crt))/1 );   
    esp_tempW3 = (int16_t)( (((int32_t)2000)*(esp_tempW2))/TRACK_WIDTH_R );   

    ldslsEstYawrate1 = LCESP_s16Lpf1Int(esp_tempW3,  ldslsEstYawrate1Old, L_U8FILTER_GAIN_10MSLOOP_3HZ);/* MGH-60 : 15, MGH-80 : 3Hz*/
       
    ldslsEstYawrate2Old = ldslsEstYawrate2 ;
            
    /* tempW2 = 100;
    tempW1 = vref_R - vref_L  ; 
    tempW0 = 1;
    s16muls16divs16();    
    
    tempW2 = 2000;
    tempW1 = tempW3;
    tempW0 = TRACK_WIDTH_R;
    s16muls16divs16();
    esp_tempW7 = tempW3;     */
    
    esp_tempW2 = (int16_t)( (((int32_t)100)*(vref_R - vref_L))/1 );   
    esp_tempW7 = (int16_t)( (((int32_t)2000)*(esp_tempW2))/TRACK_WIDTH_R );       
    
    ldslsEstYawrate2 = LCESP_s16Lpf1Int(esp_tempW7,  ldslsEstYawrate2Old, L_U8FILTER_GAIN_10MSLOOP_2HZ);    
}
 
void LCSLS_vCallDetection(void)
{
	int16_t s16PgainMu, s16DgainMu, s16PDSpdWeg;
  #if __BRK_SIG_MPS_TO_PEDAL_SEN
    int16_t s16EstDriverBrakePress;
  #endif
	
  #if __GM_FailM

	if((init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||
	    (fu1ABSEcuHWErrDet==1)||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||
		(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
		(fu1SameSideWSSErrDet==1)||(fu1DiagonalWSSErrDet==1)||
		(fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(fu1MotErrFlg==1)||(fu1VoltageLowErrDet==1)	/* woong 2011.04.06 fu1VoltageLowErrDet ���� �߰� */
	  #if ((__CAR==GM_T300)||(__CAR == GM_M350))
		||(fu8EngineModeStep<=1)
	  #endif
	  #if __BRAKE_FLUID_LEVEL==ENABLE
	  ||(fu1DelayedBrakeFluidLevel==1)
	  ||(fu1LatchedBrakeFluidLevel==1)	/* 090901 for Bench Test */
      #endif
		||(fu1SubCanLineErrDet==1)
		||(fu1ESCEcuHWErrDet==1)
		||(fu1StrErrorDet==1)
		||(fu1SteerSenPwr1sOk==0)||(fu1McpSenPwr1sOk==0)
		||(fu1StrSusDet==1))
	{
		SLS_INHIBIT_ESC_FLAG = 1;
	}
	else
	{
		SLS_INHIBIT_ESC_FLAG = 0;
		
		SLS_INHIBIT_YAWG_FLAG_old = SLS_INHIBIT_YAWG_FLAG ;
	
		if((fu1YawErrorDet==1)||(fu1AyErrorDet==1)||(fu1YawSenPwr1sOk==0)||(fu1AySenPwr1sOk==0)
		   ||(fu1YawSusDet==1)||(fu1AySusDet==1))
		{
			SLS_INHIBIT_YAWG_FLAG = 1;
		}
		else
		{
			SLS_INHIBIT_YAWG_FLAG = 0;
		}
	}
        
  #else  /* HMC  */
    if((fu1OnDiag==1)||(init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||
       (vdc_error_flg==1)||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1)||(fu1MCPSusDet==1))
    {
        SLS_INHIBIT_ESC_FLAG = 1;
    }
    else
    {
    	SLS_INHIBIT_ESC_FLAG = 0;
    }                                             
  #endif 
  
    /* Partial Brake Entrance Condition */  
  #if __BRK_SIG_MPS_TO_PEDAL_SEN
	if ( ( SLS_PARTIAL_BRAKING == 0 ) && (ABS_fz==0) && ( lsesps16EstBrkPressByBrkPedalF < S16_SLS_PARTIAL_BRK_ENTER_MAX ) && ( lsesps16EstBrkPressByBrkPedalF >= S16_SLS_PARTIAL_BRK_ENTER_MIN ) )  
    {
        SLS_PARTIAL_BRAKING = 1;
    }
    else if ( ( SLS_PARTIAL_BRAKING == 1 ) && (ABS_fz==0) && ( lsesps16EstBrkPressByBrkPedalF < S16_SLS_PARTIAL_BRK_EXIT_MAX ) && ( lsesps16EstBrkPressByBrkPedalF >= S16_SLS_PARTIAL_BRK_EXIT_MIN ) )
    {
        SLS_PARTIAL_BRAKING = 1;
    }
    else
    {
        SLS_PARTIAL_BRAKING = 0;
    }	
  #else 
    if ( ( SLS_PARTIAL_BRAKING == 0 ) && (ABS_fz==0) && ( mpress < S16_SLS_PARTIAL_BRK_ENTER_MAX ) && ( mpress >= S16_SLS_PARTIAL_BRK_ENTER_MIN ) )  
    {
        SLS_PARTIAL_BRAKING = 1;
    }
    else if ( ( SLS_PARTIAL_BRAKING == 1 ) && (ABS_fz==0) && ( mpress < S16_SLS_PARTIAL_BRK_EXIT_MAX ) && ( mpress >= S16_SLS_PARTIAL_BRK_EXIT_MIN ) )
    {
        SLS_PARTIAL_BRAKING = 1;
    }
    else
    {
        SLS_PARTIAL_BRAKING = 0;
    }    
  #endif          
    /* Straight-Braking Judgement Condition */

  #if __CBC                            
    if ( ( SLS_STRAIGHT_CONDITION == 0 ) &&  ( CBC_ON == 0 ) && ( YAW_CDC_WORK==0 ) && ( McrAbs(rf) < S16_SLS_STRAIGHT_YAWC_ENTER ) )   /* ( McrAbs(alat) < LAT_0G2G ) */
  #else   
    if ( ( SLS_STRAIGHT_CONDITION == 0 ) && ( YAW_CDC_WORK==0 ) && ( McrAbs(rf) < S16_SLS_STRAIGHT_YAWC_ENTER ) )   /* ( McrAbs(alat) < LAT_0G2G ) */
  #endif                   
    {
		SLS_STRAIGHT_CONDITION = 1;
    }
  #if __CBC                            
    else if ( ( SLS_STRAIGHT_CONDITION == 1 ) &&  ( CBC_ON == 0 ) && ( YAW_CDC_WORK==0 ) && ( McrAbs(rf) < S16_SLS_STRAIGHT_YAWC_EXIT ) )
  #else   
    else if ( ( SLS_STRAIGHT_CONDITION == 1 ) && ( YAW_CDC_WORK==0 ) && ( McrAbs(rf) < S16_SLS_STRAIGHT_YAWC_EXIT ) )
  #endif          
	{
		SLS_STRAIGHT_CONDITION = 1;
	}
	else
	{
		SLS_STRAIGHT_CONDITION = 0;
	}

	/* SLS enable condition */

    if ( ( SLS_ENABLE==0 ) && ( SLS_INHIBIT_ESC_FLAG==0 ) && ( SLS_PARTIAL_BRAKING==1 ) &&
         ( SLS_STRAIGHT_CONDITION==1 ) && (vrefk > S16_SLS_ENTRANCE_SPD ) )    /* Entrance Speed */
	{
		SLS_ENABLE=1;
	}
	else if ( ( SLS_ENABLE==1 ) && ( SLS_INHIBIT_ESC_FLAG==0 ) && ( SLS_PARTIAL_BRAKING==1 ) &&
	        ( SLS_STRAIGHT_CONDITION==1 ) && (vrefk > S16_SLS_EXIT_SPD ) )    /* Exit Speed */
	{
		SLS_ENABLE=1;
	}
	else
	{
		SLS_ENABLE=0;
	}
                    
/* --------------- Calculate sls_delta_yaw_first / sls_yaw_error_under_dot -----------------  */

    sls_delta_yaw_first2_old = sls_delta_yaw_first2;
    sls_delta_yaw_first2 = ldslsEstYawrate2-rf;

    if ( ESP_ROUGH_ROAD ==1)
    {
        sls_delta_yaw_first2 = LCESP_s16Lpf1Int( sls_delta_yaw_first2, sls_delta_yaw_first2_old, L_U8FILTER_GAIN_10MSLOOP_3HZ);/* MGH-60 : 17, MGH-80 : 3Hz*/
    }
    else
    {
        sls_delta_yaw_first2 = LCESP_s16Lpf1Int( sls_delta_yaw_first2, sls_delta_yaw_first2_old, L_U8FILTER_GAIN_10MSLOOP_6_5HZ);/* MGH-60 : 28, MGH-80 : 6.5Hz*/
	} 
    
    /*sls_delta_yaw_first2 = LCESP_s16Lpf1Int( ldslsEstYawrate2-rf, sls_delta_yaw_first2_old, 17(UCHAR)esp_tempW1);*/


  /*   sls_delta_yaw_first = sls_delta_yaw_first2*yaw_sign;    sls_delta_yaw_first, r=0.01  */
 
 
    /*   sls_yaw_error_under_dot    */ 
    
    sls_yaw_error_under_dot2_old = sls_yaw_error_under_dot2;

    sls_delta_yaw_first2_buf[2]=sls_delta_yaw_first2_buf[1];
    sls_delta_yaw_first2_buf[1]=sls_delta_yaw_first2_buf[0];
    sls_delta_yaw_first2_buf[0]=sls_delta_yaw_first2;

    esp_tempW1=(sls_delta_yaw_first2_buf[0]-sls_delta_yaw_first2_buf[2]);     /*  r: 0.01*/

    if ( (ESP_ROUGH_ROAD==1) ||( ABS_fz==1) )
    {
        sls_yaw_error_under_dot2 = LCESP_s16Lpf1Int( esp_tempW1, sls_yaw_error_under_dot2_old, L_U8FILTER_GAIN_10MSLOOP_3HZ); /* MGH-60 : 17, MGH-80 : 3Hz*/
    }
    else
    {
        sls_yaw_error_under_dot2 = LCESP_s16Lpf1Int( esp_tempW1, sls_yaw_error_under_dot2_old, L_U8FILTER_GAIN_10MSLOOP_8_5HZ);/* MGH-60 : 35, MGH-80 : 8.5Hz*/
    }
        
/* -----------------------------------------------------------------------------------------------  */

    if ((SLS_YAWG_FAIL_FADE_FLAG==0)&&(SLS_INHIBIT_YAWG_FLAG_old==0)&&(SLS_INHIBIT_YAWG_FLAG==1))
    {   
        SLS_YAWG_FAIL_FADE_FLAG = 1 ;
        sls_yawg_fail_fade_cnt = sls_yawg_fail_fade_cnt + 1 ;
        sls_yawg_fail_fade_weight = (int16_t)((((int32_t)sls_yawg_fail_fade_cnt)*100)/S16_SLS_YAWG_FAIL_FADE_CNT) ;
    }
    else if ((SLS_YAWG_FAIL_FADE_FLAG==1)&&(SLS_INHIBIT_YAWG_FLAG==1)&&(sls_yawg_fail_fade_cnt < S16_SLS_YAWG_FAIL_FADE_CNT))
    {
        SLS_YAWG_FAIL_FADE_FLAG = 1 ;
        sls_yawg_fail_fade_cnt = sls_yawg_fail_fade_cnt + 1 ; 
        sls_yawg_fail_fade_weight = (int16_t)((((int32_t)sls_yawg_fail_fade_cnt)*100)/S16_SLS_YAWG_FAIL_FADE_CNT) ;
    }
    else
    { 
        SLS_YAWG_FAIL_FADE_FLAG = 0 ;
        sls_yawg_fail_fade_cnt = 0 ;
        sls_yawg_fail_fade_weight = 0 ;
    }
  
    if ( sls_yawg_fail_fade_weight <= 0)
    {
        sls_yawg_fail_fade_weight = 0 ;
    }
    else if ( sls_yawg_fail_fade_weight >= 100)
    {
        sls_yawg_fail_fade_weight = 100 ;
    }
    else
    {
        ;
    }
  
    if( SLS_INHIBIT_YAWG_FLAG==0 ) 
    {   
        sls_delta_yaw_first2 = delta_yaw_first2;
        sls_yaw_error_under_dot2 = yaw_error2_dot ;
    }
    else 
    { 
        ;
    }

/* ------------------------  SLS control threshold   ---------------------------  */


    if( SLS_INHIBIT_YAWG_FLAG==1 )
    {   
        sls_delyaw_enter_th = LCESP_s16IInter3Point ( vrefk, S16_SLS_LOW_SPEED,    700,   /* [TP], deg/sec  */
                                                             S16_SLS_MED_SPEED,    700,
                                                             S16_SLS_HIGH_SPEED,   700  ) ;

        sls_delyaw_exit_th = LCESP_s16IInter3Point ( vrefk,  S16_SLS_LOW_SPEED,    600,   /* [TP], deg/sec  */
                                                             S16_SLS_MED_SPEED,    600,
                                                             S16_SLS_HIGH_SPEED,   600  ) ;
    }
    else
    { 
        sls_delyaw_enter_th = LCESP_s16IInter3Point ( vrefk, S16_SLS_LOW_SPEED,    U16_SLS_DYAW_ENTER_THRES_L_SPD,   /* [TP], deg/sec  */
                                                             S16_SLS_MED_SPEED,    U16_SLS_DYAW_ENTER_THRES_M_SPD,
                                                             S16_SLS_HIGH_SPEED,   U16_SLS_DYAW_ENTER_THRES_H_SPD  ) ;

        sls_delyaw_exit_th = LCESP_s16IInter3Point ( vrefk,  S16_SLS_LOW_SPEED,    U16_SLS_DYAW_END_THRES_L_SPD,   /* [TP], deg/sec  */
                                                             S16_SLS_MED_SPEED,    U16_SLS_DYAW_END_THRES_M_SPD,
                                                             S16_SLS_HIGH_SPEED,   U16_SLS_DYAW_END_THRES_H_SPD  ) ;
    }
    
    if(sls_delyaw_enter_th < YAW_1G5DEG) 
    {
    	sls_delyaw_enter_th = YAW_1G5DEG;
    }
    else {}
    
    if(sls_delyaw_exit_th < YAW_1DEG) 
    {
    	sls_delyaw_exit_th = YAW_1DEG;
    }
    else {}
        

    if ( SLS_ON==1 )
    {
        sls_delyaw_out_th = sls_delyaw_exit_th ;
    }
    else
    {
        sls_delyaw_out_th = sls_delyaw_enter_th ;
    }
 
/* ------------------------  SLS Moment calculation   ---------------------------  */

    if( sls_delta_yaw_first2 >= 0 )
    {    
        if ( sls_yaw_error_under_dot2 >= 0 )
        {    
            if ( sls_delta_yaw_first2 >= sls_delyaw_out_th )   /* Outside */
            {
                s16PgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_P_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_P_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_P_GAIN_OUT_INC_L_mu ) ;
                                                                            
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_INC_L_mu ) ;
            }
            else    /* Inside */
            {
                s16PgainMu = 0 ;   

                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_INC_L_mu ) ;
            }       
        }
        else
        { 
            if ( sls_delta_yaw_first2 >= sls_delyaw_out_th )   /* Outside */
            {
                s16PgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_P_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_P_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_P_GAIN_OUT_DEC_L_mu ) ;
                                                                                
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_DEC_L_mu ) ;
            }
            else    /* Inside */
            {
                s16PgainMu = 0 ; 
                                                                                
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_DEC_L_mu ) ;
            }      
        }
    }
    else
    {
        if ( sls_yaw_error_under_dot2 <= 0 )        
        {     
            if ( sls_delta_yaw_first2 <= -sls_delyaw_out_th )   /* Outside */
            {
                s16PgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_P_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_P_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_P_GAIN_OUT_INC_L_mu ) ;    
                                                                            
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_INC_L_mu ) ;
            }
            else    /* Inside */
            {
                s16PgainMu = 0 ; 
                                                                                
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_INC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_INC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_INC_L_mu ) ;
            }      
        }
        else
        { 
            if ( sls_delta_yaw_first2 <= -sls_delyaw_out_th )   /* Outside */
            {
                s16PgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_P_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_P_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_P_GAIN_OUT_DEC_L_mu ) ;   
                                                                                
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_DEC_L_mu ) ;
            }
            else    /* Inside */
            {
                s16PgainMu = 0 ; 
                                                                                
                s16DgainMu = LCESP_s16FindRoadDependatGain( esp_mu, S16_SLS_D_GAIN_OUT_DEC_H_mu,   /* [TP] */
                                                                    S16_SLS_D_GAIN_OUT_DEC_M_mu, 
                                                                    S16_SLS_D_GAIN_OUT_DEC_L_mu ) ;
            }                

        }
    }                

    s16PDSpdWeg = LCESP_s16IInter3Point(vrefk,  S16_SLS_LOW_SPEED,  S16_SLS_LOW_SPD_PD_WEG,
                                                S16_SLS_MED_SPEED,  S16_SLS_MED_SPD_PD_WEG,
                                                S16_SLS_HIGH_SPEED, S16_SLS_HIGH_SPD_PD_WEG  );


    if (SLS_INHIBIT_YAWG_FLAG==0)
    { 
        delta_moment_q_sls_old = delta_moment_q_sls ; 
    }
    else
    {
        ;
    }

        s16PgainMu = (int16_t)( ((int32_t)s16PgainMu*s16PDSpdWeg) / 100);
        s16DgainMu = (int16_t)( ((int32_t)s16DgainMu*s16PDSpdWeg) / 100);
        esp_tempW3 = (int16_t)( ((int32_t)s16PgainMu*sls_delta_yaw_first2) / 10);
        esp_tempW4 = (int16_t)( ((int32_t)s16DgainMu*sls_yaw_error_under_dot2) / 1);        

        delta_moment_q_sls = ( esp_tempW3 + esp_tempW4 ) ;     /* SLS moment */

     
    if (SLS_YAWG_FAIL_FADE_FLAG==1)
    {
        esp_tempW4 = (int16_t)((((int32_t)delta_moment_q_sls_old)*(100-sls_yawg_fail_fade_weight))/100) ;
        esp_tempW5 = (int16_t)((((int32_t)delta_moment_q_sls)*sls_yawg_fail_fade_weight)/100) ;
        delta_moment_q_sls = esp_tempW4 + esp_tempW5 ;        
    }
    else
    {
        ;
    }

          
    /* Activation of SLS Condition */

    if( SLS_INHIBIT_YAWG_FLAG==1 ) 
    { 
    	esp_tempW1 = LCESP_s16IInter3Point(vrefk,  S16_SLS_LOW_SPEED ,  S16_SLS_LOW_SPD_ENT_MQ_ABS, 
        	                                       S16_SLS_MED_SPEED ,  S16_SLS_MED_SPD_ENT_MQ_ABS,
            	                                   S16_SLS_HIGH_SPEED,  S16_SLS_HIGH_SPD_ENT_MQ_ABS );  /* Enter condition */
                                                
    	esp_tempW2 = LCESP_s16IInter3Point(vrefk,   S16_SLS_LOW_SPEED ,  S16_SLS_LOW_SPD_EXIT_MQ_ABS, 
        	                                        S16_SLS_MED_SPEED ,  S16_SLS_MED_SPD_EXIT_MQ_ABS,
              	                                    S16_SLS_HIGH_SPEED,  S16_SLS_HIGH_SPD_EXIT_MQ_ABS );  /* Exit condition */
    }
    else
    { 
    	esp_tempW1 = LCESP_s16IInter3Point(vrefk,   S16_SLS_LOW_SPEED ,  S16_SLS_LOW_SPD_ENT_MQ, 
        	                                        S16_SLS_MED_SPEED ,  S16_SLS_MED_SPD_ENT_MQ,
            	                                    S16_SLS_HIGH_SPEED, S16_SLS_HIGH_SPD_ENT_MQ );  /* Enter condition */
                                                
    	esp_tempW2 = LCESP_s16IInter3Point(vrefk,   S16_SLS_LOW_SPEED ,  S16_SLS_LOW_SPD_EXIT_MQ, 
        	                                        S16_SLS_MED_SPEED ,  S16_SLS_MED_SPD_EXIT_MQ,
            	                                    S16_SLS_HIGH_SPEED, S16_SLS_HIGH_SPD_EXIT_MQ );  /* Exit condition */
    }

                           
    if ( SLS_ENABLE==1 )  
    { 
        if( ( SLS_ACTIVE_rl==0 ) && ( sls_delta_yaw_first2 >= 0 ) && ( delta_moment_q_sls >= esp_tempW1 ) )
        { 
            SLS_ACTIVE_rl =1 ; 
        }
        else if ( ( SLS_ACTIVE_rl==1 ) && ( sls_delta_yaw_first2 >= 0 ) && ( delta_moment_q_sls >= esp_tempW2 ) )
        { 
            SLS_ACTIVE_rl =1 ;
        }
        else
        {
            SLS_ACTIVE_rl =0 ; 
        }
            
        if( ( SLS_ACTIVE_rr==0 ) && ( sls_delta_yaw_first2 < 0 ) && ( delta_moment_q_sls <= -esp_tempW1 ) )
        { 
            SLS_ACTIVE_rr =1 ; 
        }
        else if ( ( SLS_ACTIVE_rr==1 ) && ( sls_delta_yaw_first2 <= 0 ) && ( delta_moment_q_sls <= -esp_tempW2 ) )
        { 
            SLS_ACTIVE_rr =1 ;        
        }
        else
        {
            SLS_ACTIVE_rr =0 ; 
        }
    }
    else
    {
        SLS_ACTIVE_rl =0 ; 
        SLS_ACTIVE_rr =0 ;                                             
    }
    
    if ( (SLS_ACTIVE_rl==1) || (SLS_ACTIVE_rr==1) )
    {
        SLS_ON=1;
    }
    else
    {
        SLS_ON=0; 
    }      
}

void LCSLS_vCallCalControlPressure(void)
{      
    if(SLS_ON==1)
    {                                                            
        esp_tempW0 = McrAbs(delta_moment_q_sls) ;
     
        if ( esp_tempW0 >= S16_SLS_MQ_TH_3RD )
        { 
            SLS_target_hold_cnt = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_HOLD_CNT_4th_MQ_L_Spd,
        												        S16_SLS_MED_SPEED,  S16_SLS_HOLD_CNT_4th_MQ_M_Spd,
        												        S16_SLS_HIGH_SPEED, S16_SLS_HOLD_CNT_4th_MQ_H_Spd );      
        }             
        else if ( ( esp_tempW0 >= S16_SLS_MQ_TH_2ND ) && ( esp_tempW0 < S16_SLS_MQ_TH_3RD ) )
        { 
            SLS_target_hold_cnt = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_HOLD_CNT_3rd_MQ_L_Spd,
        												        S16_SLS_MED_SPEED,  S16_SLS_HOLD_CNT_3rd_MQ_M_Spd,
        												        S16_SLS_HIGH_SPEED, S16_SLS_HOLD_CNT_3rd_MQ_H_Spd );        
        }             
        else if ( ( esp_tempW0 >= S16_SLS_MQ_TH_1ST ) && ( esp_tempW0 < S16_SLS_MQ_TH_2ND ) )
        { 
            SLS_target_hold_cnt = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_HOLD_CNT_2nd_MQ_L_Spd,
        												        S16_SLS_MED_SPEED,  S16_SLS_HOLD_CNT_2nd_MQ_M_Spd,
        												        S16_SLS_HIGH_SPEED, S16_SLS_HOLD_CNT_2nd_MQ_H_Spd );      
        }      
        else
        {
            SLS_target_hold_cnt = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_HOLD_CNT_1st_MQ_L_Spd,
        												        S16_SLS_MED_SPEED,  S16_SLS_HOLD_CNT_1st_MQ_M_Spd,
        												        S16_SLS_HIGH_SPEED, S16_SLS_HOLD_CNT_1st_MQ_H_Spd );      
        }  
        
        esp_tempW1 = LCESP_s16IInter3Point(vrefk, S16_SLS_LOW_SPEED, S16_SLS_DEL_P_WEG_L_Spd,  /* Weighting */
                                                  S16_SLS_MED_SPEED, S16_SLS_DEL_P_WEG_M_Spd,
                                                  S16_SLS_HIGH_SPEED,S16_SLS_DEL_P_WEG_H_Spd );  

        /* tempW2 = esp_tempW0;
        tempW1 = esp_tempW1;
        tempW0 = 1000;
        s16muls16divs16();
        SLS_target_delta_p_rear = tempW3;    */     
        SLS_target_delta_p_rear = (int16_t)( (((int32_t)esp_tempW0)*esp_tempW1)/1000 );    /* r = 0.1 */   


        if (SLS_ACTIVE_rl==1)
        { 
            esp_tempW2 = RL.s16_Estimated_Active_Press;
        }
        else  
        {
            esp_tempW2 = RR.s16_Estimated_Active_Press;        
        } 
         
        SLS_target_p_rear = esp_tempW2 - SLS_target_delta_p_rear;    
         
        if (SLS_target_p_rear<MPRESS_3BAR)   
        {
            SLS_target_p_rear = MPRESS_3BAR;
        }
        else if (SLS_target_p_rear>esp_tempW2)
        {
            SLS_target_p_rear = esp_tempW2;
        }
        else
        {
            ;   
        }
     
    }
    else 
    {
        SLS_target_hold_cnt = 0 ;
        SLS_target_delta_p_rear = 0 ;
        SLS_target_p_rear = 0 ;
    }

    if (SLS_ACTIVE_rl==1) 
    { 
        if ((SLS_rate_counter_rl==0)&&(RL.s16_Estimated_Active_Press > SLS_target_p_rear))    /* Dump */
        {
            SLS_rate_counter_rl = SLS_rate_counter_rl + 1 ;               
        }
        else                         /* Hold */
        {
            if( SLS_rate_counter_rl >= SLS_target_hold_cnt )
            {
                SLS_rate_counter_rl = 0 ;
            }            
            else
            {
                SLS_rate_counter_rl = SLS_rate_counter_rl + 1 ;                
            }
        }
    }
    else
    { 
        SLS_rate_counter_rl = 0 ; 
    }       
    
    
    if (SLS_ACTIVE_rr==1) 
    { 
        if ((SLS_rate_counter_rr==0)&&(RR.s16_Estimated_Active_Press > SLS_target_p_rear))    /* Dump */
        {
            SLS_rate_counter_rr = SLS_rate_counter_rr + 1 ;               
        }
        else                           /* Hold */
        {
            if( SLS_rate_counter_rr >= SLS_target_hold_cnt )
            {
                SLS_rate_counter_rr = 0 ;
            }            
            else
            {
                SLS_rate_counter_rr = SLS_rate_counter_rr + 1 ;                
            }
        }
    }
    else
    { 
        SLS_rate_counter_rr = 0 ; 
    }               
}
  
void LCSLS_vCallControlFadeOut(void)
{ 
    SLS_fade_out_max = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_FADE_OUT_TIME_L_Spd,
												     S16_SLS_MED_SPEED,  S16_SLS_FADE_OUT_TIME_M_Spd,
												     S16_SLS_HIGH_SPEED, S16_SLS_FADE_OUT_TIME_H_Spd );
    
    if( (SLS_ON==0) && ( SLS_INHIBIT_ESC_FLAG==0 )  && ( SLS_PARTIAL_BRAKING==1 ) 
     && ( SLS_STRAIGHT_CONDITION==1 ) && (vrefk > ( S16_SLS_EXIT_SPD - 100)) )  
    {
        SLS_FADE_OUT_ENABLE = 1 ; 
    }
    else
    {
        SLS_FADE_OUT_ENABLE = 0 ;         
    }
    
    if((SLS_FADE_OUT_ENABLE==1)&&(SLS_FADE_OUT_rl==0)&&(SLS_ACTIVE_rl_old==1)&&(SLS_ACTIVE_rl==0)&&(SLS_FADE_OUT_rl_cnt<SLS_fade_out_max))
    {
    	SLS_FADE_OUT_rl=1;
        SLS_FADE_OUT_rl_cnt = SLS_FADE_OUT_rl_cnt + 1 ;    	
    }
    else if((SLS_FADE_OUT_ENABLE==1)&&((SLS_FADE_OUT_rl==1)&&(SLS_ACTIVE_rl==0))&&(SLS_FADE_OUT_rl_cnt<SLS_fade_out_max))
    { 
    	SLS_FADE_OUT_rl=1;
        SLS_FADE_OUT_rl_cnt = SLS_FADE_OUT_rl_cnt + 1 ;    	   
    }          
    else
    {
    	SLS_FADE_OUT_rl=0;
        SLS_FADE_OUT_rl_cnt = 0 ;    	
    }     
     
    if((SLS_FADE_OUT_ENABLE==1)&&(SLS_FADE_OUT_rr==0)&&(SLS_ACTIVE_rr_old==1)&&(SLS_ACTIVE_rr==0)&&(SLS_FADE_OUT_rr_cnt<SLS_fade_out_max))
    {
    	SLS_FADE_OUT_rr=1;
        SLS_FADE_OUT_rr_cnt = SLS_FADE_OUT_rr_cnt + 1 ;    	
    }
    else if ((SLS_FADE_OUT_ENABLE==1)&&((SLS_FADE_OUT_rr==1)&&(SLS_ACTIVE_rr==0))&&(SLS_FADE_OUT_rr_cnt<SLS_fade_out_max))
    { 
    	SLS_FADE_OUT_rr=1;
        SLS_FADE_OUT_rr_cnt = SLS_FADE_OUT_rr_cnt + 1 ;    	   
    }          
    else
    {
    	SLS_FADE_OUT_rr=0;
        SLS_FADE_OUT_rr_cnt = 0 ;    	
    }          
     
    SLS_target_fade_cnt = LCESP_s16IInter3Point( vrefk, S16_SLS_LOW_SPEED,  S16_SLS_FADE_HOLD_CNT_L_Spd,
												        S16_SLS_MED_SPEED,  S16_SLS_FADE_HOLD_CNT_M_Spd,
												        S16_SLS_HIGH_SPEED, S16_SLS_FADE_HOLD_CNT_H_Spd );

	if(SLS_FADE_OUT_ENABLE==1)
	{												        
		if (SLS_FADE_OUT_rl==1) 
		{ 
	        if (SLS_fade_counter_rl==0)  /* rise */
		    {
				SLS_fade_counter_rl = SLS_fade_counter_rl + 1 ;               
		    }
		    else                        /*  Hold */
		    {
		        if( SLS_fade_counter_rl >= SLS_target_fade_cnt )
		        {
		            SLS_fade_counter_rl = 0 ;
		        }            
		        else
		        {
		            SLS_fade_counter_rl = SLS_fade_counter_rl + 1 ;                
		        }
		    }
		}
		else
		{ 
		    SLS_fade_counter_rl = 0 ; 
		}   
		 
		if (SLS_FADE_OUT_rr==1) 
		{ 
	        if (SLS_fade_counter_rr==0)  /* rise */
		    {
		        SLS_fade_counter_rr = SLS_fade_counter_rr + 1 ;               
		    }
		    else                        /*  Hold */
		    {
		        if( SLS_fade_counter_rr >= SLS_target_fade_cnt )
		        {
		            SLS_fade_counter_rr = 0 ;
		        }            
		        else
		        {
		            SLS_fade_counter_rr = SLS_fade_counter_rr + 1 ;                
		        }
		    }
		}
		else
		{ 
		    SLS_fade_counter_rr = 0 ; 
		}       
		
	}
	else if( SLS_ON==0 ) 
	{
		SLS_fade_counter_rl = 0 ;        
		SLS_fade_counter_rr = 0 ;    
	} 
	else{}
}     

#endif /* #if __SLS */
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCSLSCallControl
	#include "Mdyn_autosar.h"
#endif 
/* AUTOSAR --------------------------*/
