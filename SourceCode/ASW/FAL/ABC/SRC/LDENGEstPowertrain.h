#ifndef LDENGESTPOWERTRAIN_H
#define LDENGESTPOWERTRAIN_H

/*includes**************************************************/
#include "LVarHead.h"


/*Global MACRO CONSTANT Definition******************************************************/


/*Global MACRO FUNCTION Definition******************************************************/


/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/

#if __TCS || __ETC || __EDC	/* ABS_PLUS_ECU 적용시 macro 삭제 예정 */

/* Conversion Ratio (TM_RATIO * FINAL_RATIO) for EDC */
/* Resolution : 0.1 */
  #if __CAR==HMC_NF
    /* for NF 2.4 AT  */
    #define		EDC_TM_1_RATIO			107				/*2.842*3.770*/
    #define		EDC_TM_2_RATIO			58				/*1.529*3.770*/
    #define		EDC_TM_3_RATIO			38				/*1.000*3.770*/
    #define		EDC_TM_4_RATIO			27				/*0.712*3.770*/
    #define		EDC_TM_BACK_RT			93				/*2.480*3.770*/
                
    /* temp */
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770*/
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770*/
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770*/
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770*/
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770*/
    #define     EDC_MTM_6_RATIO         25 				 /* temp         */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770*/
    
  #elif __CAR==HMC_TQ	//temporarily defined macro
    /* temp */
    #define		EDC_TM_1_RATIO			128				/*2.842*4.520     */
    #define		EDC_TM_2_RATIO			68				/*1.495*4.520     */
    #define		EDC_TM_3_RATIO			45				/*1.000*4.520     */
    #define		EDC_TM_4_RATIO			33				/*0.731*4.520     */
    #define		EDC_TM_BACK_RT			123				/*2.720*4.520     */  

    /*measured gear ratio*/
    /* TQ 2.5 A-II WGT */
    #define     EDC_MTM_1_RATIO         166              
    #define     EDC_MTM_2_RATIO          86               
    #define     EDC_MTM_3_RATIO          50               
    #define     EDC_MTM_4_RATIO          37               
    #define     EDC_MTM_5_RATIO          29               
    #define     EDC_MTM_6_RATIO          25 			
    #define     EDC_MTM_BACK_RT         157             /* temp */ 
    
  #elif __CAR==HMC_EF
    /* for EF 2.0 AT */
    #define		EDC_TM_1_RATIO			107				/*2.842*3.770*/
    #define		EDC_TM_2_RATIO			58				/*1.529*3.770*/
    #define		EDC_TM_3_RATIO			38				/*1.000*3.770*/
    #define		EDC_TM_4_RATIO			27				/*0.712*3.770*/
    #define		EDC_TM_BACK_RT			93				/*2.480*3.770*/       

    /* temp */
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770*/
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770*/
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770*/
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770*/
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770*/
    #define     EDC_MTM_6_RATIO         25 				 /* temp         */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770*/

  #elif __CAR==HMC_SM
    /* for SM 2.7 AT*/
    #define		EDC_TM_1_RATIO			128				/*2.842*4.520     */
    #define		EDC_TM_2_RATIO			68				/*1.495*4.520     */
    #define		EDC_TM_3_RATIO			45				/*1.000*4.520     */
    #define		EDC_TM_4_RATIO			33				/*0.731*4.520     */
    #define		EDC_TM_BACK_RT			123				/*2.720*4.520     */  

    /*temp*/
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770  */
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770  */
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770  */
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770  */
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770  */
    #define     EDC_MTM_6_RATIO         25 				 /* temp           */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770  */

  #elif __CAR==HMC_JM
    /* for JM 2.0 Diesel AT  */    
    #define		EDC_TM_1_RATIO			115				/*2.842*4.042    */
    #define		EDC_TM_2_RATIO			62				/*1.529*4.042    */
    #define		EDC_TM_3_RATIO			40				/*1.000*4.042    */
    #define		EDC_TM_4_RATIO			29				/*0.712*4.042    */
    #define		EDC_TM_BACK_RT			100				/*2.480*4.042    */                   
    
    /*temp*/
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770  */
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770  */
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770  */
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770  */
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770  */
    #define     EDC_MTM_6_RATIO         25 				 /* temp           */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770  */

  #elif __CAR==KMC_MG 
    /* for MG 2.7 AT Gasoline */ 
    #define     EDC_TM_1_RATIO          126              /* 3.79  *3.33	  */ 
    #define     EDC_TM_2_RATIO          69               /* 2.06  *3.33   */
    #define     EDC_TM_3_RATIO          47               /* 1.42  *3.33   */
    #define     EDC_TM_4_RATIO          33               /* 1.00  *3.33   */
    #define     EDC_TM_5_RATIO          24               /* 0.73  *3.33   */
    #define     EDC_TM_BACK_RT          128              /* 3.87  *3.33   */
    
    /*temp*/
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770    */
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770    */
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770    */
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770    */
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770    */
    #define     EDC_MTM_6_RATIO         25 				 /* temp             */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770    */

  #elif __CAR==HMC_GK
    /* for GK 2.7 sporty AT */
    #define     EDC_TM_1_RATIO          115	            
    #define     EDC_TM_2_RATIO          62              
    #define     EDC_TM_3_RATIO          40              
    #define     EDC_TM_4_RATIO          29              
    #define     EDC_TM_BACK_RT          100             
    
    /* for GK2.7 6-speed MT */
    #define     EDC_MTM_1_RATIO         142		/* Calibrated value : 140 (3.153  *4.428)   */           
    #define     EDC_MTM_2_RATIO         86 		/*					  86  (1.944  *4.428)   */           
    #define     EDC_MTM_3_RATIO         59  	/*					  59  (1.333  *4.428)   */           
    #define     EDC_MTM_4_RATIO         47 		/*					  47  (1.055  *4.428)   */           
    #define     EDC_MTM_5_RATIO         38   	/*					  38  (0.857  *4.428)   */           
    #define     EDC_MTM_6_RATIO         31 		/*					  31  (0.704  *4.428)   */           
    #define     EDC_MTM_BACK_RT         138 	/*              	  131 (3.002  *4.428)   */           

  #elif __CAR==HMC_BK 
   
    #define     EDC_TM_1_RATIO          107              /* temp  */
    #define     EDC_TM_2_RATIO          58               /* temp  */
    #define     EDC_TM_3_RATIO          38               /* temp  */
    #define     EDC_TM_4_RATIO          27               /* temp  */
    #define     EDC_TM_BACK_RT          93               /* temp  */
	/* BK 3.8G */	    
    #define     EDC_MTM_1_RATIO         136              /* 3.848  * 3.538 */
    #define     EDC_MTM_2_RATIO         82               /* 2.317  * 3.538 */
    #define     EDC_MTM_3_RATIO         57               /* 1.623  * 3.538 */
    #define     EDC_MTM_4_RATIO         44               /* 1.233  * 3.538 */
    #define     EDC_MTM_5_RATIO         35               /* 1      * 3.538 */
    #define     EDC_MTM_6_RATIO         28 				 /* 0.794  * 3.538 */
    #define     EDC_MTM_BACK_RT         125              /* 3.538  * 3.538 */
	/* BK 2.0G */     
    #define     EDC_MTM_1_RATIO_2       165              /* 4.229  * 3.909 */
    #define     EDC_MTM_2_RATIO_2       96               /* 2.467  * 3.909 */
    #define     EDC_MTM_3_RATIO_2       65               /* 1.671  * 3.909 */
    #define     EDC_MTM_4_RATIO_2       48               /* 1.233  * 3.909 */
    #define     EDC_MTM_5_RATIO_2       39               /* 1      * 3.909 */
    #define     EDC_MTM_6_RATIO_2       31 				 /* 0.794  * 3.909 */
    #define     EDC_MTM_BACK_RT_2       153              /* 3.909  * 3.909 */

  #elif __CAR==KMC_XM

    /* Temp. */
    #define     EDC_TM_1_RATIO          115
    #define     EDC_TM_2_RATIO          62
    #define     EDC_TM_3_RATIO          40
    #define     EDC_TM_4_RATIO          29
    #define     EDC_TM_BACK_RT          100

    /* for XM Theta2.4 6-speed MT */
    #define     EDC_MTM_1_RATIO         192
    #define     EDC_MTM_2_RATIO         98
    #define     EDC_MTM_3_RATIO         63
    #define     EDC_MTM_4_RATIO         48
    #define     EDC_MTM_5_RATIO         38
    #define     EDC_MTM_6_RATIO         38		/* Temp. */
    #define     EDC_MTM_BACK_RT         183

    /* for XM R2.2 6-speed MT */
    #define     EDC_MTM_1_RATIO_2         169
    #define     EDC_MTM_2_RATIO_2         91
    #define     EDC_MTM_3_RATIO_2         56
    #define     EDC_MTM_4_RATIO_2         39
    #define     EDC_MTM_5_RATIO_2         30
    #define     EDC_MTM_6_RATIO_2         26
    #define     EDC_MTM_BACK_RT_2         160

  #elif __CAR==HMC_LM
    /* Temp. */
    #define     EDC_TM_1_RATIO          115
    #define     EDC_TM_2_RATIO          62
    #define     EDC_TM_3_RATIO          40
    #define     EDC_TM_4_RATIO          29
    #define     EDC_TM_BACK_RT          100

    /* for LM Theta II 2.4 GSL 6-speed MT */
    #define     EDC_MTM_1_RATIO         167
    #define     EDC_MTM_2_RATIO         91
    #define     EDC_MTM_3_RATIO         58
    #define     EDC_MTM_4_RATIO         44
    #define     EDC_MTM_5_RATIO         36
    #define     EDC_MTM_6_RATIO         31		
    #define     EDC_MTM_BACK_RT         175

    /* for LM Theta II 2.0 GSL 5-speed MT */
    #define     EDC_MTM_1_RATIO_2         166
    #define     EDC_MTM_2_RATIO_2         95
    #define     EDC_MTM_3_RATIO_2         61
    #define     EDC_MTM_4_RATIO_2         48
    #define     EDC_MTM_5_RATIO_2         37
    #define     EDC_MTM_6_RATIO_2         37	/* Temp. */
    #define     EDC_MTM_BACK_RT_2         157  

    /* for LM 2.0 DSL 6-speed MT */
    #define     EDC_MTM_1_RATIO_3         168
    #define     EDC_MTM_2_RATIO_3         83
    #define     EDC_MTM_3_RATIO_3         53
    #define     EDC_MTM_4_RATIO_3         40
    #define     EDC_MTM_5_RATIO_3         33
    #define     EDC_MTM_6_RATIO_3         28
    #define     EDC_MTM_BACK_RT_3         159  
     
  #elif __CAR==SYC_KYRON
    /* Temp. */
    #define     EDC_TM_1_RATIO          115
    #define     EDC_TM_2_RATIO          62
    #define     EDC_TM_3_RATIO          40
    #define     EDC_TM_4_RATIO          29
    #define     EDC_TM_BACK_RT          100

    /* for KYRON	*/
    #define     EDC_MTM_1_RATIO         169        
    #define     EDC_MTM_2_RATIO         97         
    #define     EDC_MTM_3_RATIO         60         
    #define     EDC_MTM_4_RATIO         39         
    #define     EDC_MTM_5_RATIO         32         
    #define     EDC_MTM_6_RATIO         10         
    #define     EDC_MTM_BACK_RT         153        

  #elif (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU)
    /* for PSA C4 1.6 MT  */
    #define		EDC_TM_1_RATIO			134				/*3.416*3.941*/
    #define		EDC_TM_2_RATIO			70				/*1.783*3.941*/
    #define		EDC_TM_3_RATIO			44				/*1.121*3.941*/
    #define		EDC_TM_4_RATIO			31				/*0.795*3.941*/
    #define		EDC_TM_BACK_RT			134				/*3.416*3.941*/

    /* for PSA C4 1.6 MT  */
    #define		EDC_MTM_1_RATIO			134				/*3.416*3.941*/
    #define		EDC_MTM_2_RATIO			70				/*1.783*3.941*/
    #define		EDC_MTM_3_RATIO			44				/*1.121*3.941*/
    #define		EDC_MTM_4_RATIO			31				/*0.795*3.941*/
    #define     EDC_MTM_5_RATIO         25              /*0.647*3.941*/
    #define     EDC_MTM_6_RATIO         21 				/*0.534*3.941*/
    #define		EDC_MTM_BACK_RT			134				/*3.416*3.941*/
    
  #elif (__CAR==KMC_TF)
    /* for TF TEMP  */
    #define		EDC_TM_1_RATIO			134				/*3.416*3.941*/
    #define		EDC_TM_2_RATIO			70				/*1.783*3.941*/
    #define		EDC_TM_3_RATIO			44				/*1.121*3.941*/
    #define		EDC_TM_4_RATIO			31				/*0.795*3.941*/
    #define		EDC_TM_BACK_RT			134				/*3.416*3.941*/

    /* for TF 2.0 GSL 6SPEED MT  */
    #define		EDC_MTM_1_RATIO			166				/*3.416*3.941*/
    #define		EDC_MTM_2_RATIO			98				/*1.783*3.941*/
    #define		EDC_MTM_3_RATIO			63				/*1.121*3.941*/
    #define		EDC_MTM_4_RATIO			48				/*0.795*3.941*/
    #define     EDC_MTM_5_RATIO         38              /*0.647*3.941*/
    #define     EDC_MTM_6_RATIO         34 				/*0.534*3.941*/
    #define		EDC_MTM_BACK_RT			140				/*3.416*3.941*/

  #elif (__CAR==HMC_DM)
    #define     EDC_TM_1_RATIO          107              /* 4.639  *3.648  임시*/
    #define     EDC_TM_2_RATIO          58               /* 2.826  *3.648  임시*/
    #define     EDC_TM_3_RATIO          38               /* 1.841  *3.648  임시*/
    #define     EDC_TM_4_RATIO          27               /* 1.386  *3.648  임시*/
    #define     EDC_TM_BACK_RT          93               /* 3.385  *3.648  임시*/
    
    #define     EDC_MTM_1_RATIO         168              /* 4.639  *3.648  */
    #define     EDC_MTM_2_RATIO         91              /* 2.826  *3.648  */
    #define     EDC_MTM_3_RATIO         56               /* 1.841  *3.648  */
    #define     EDC_MTM_4_RATIO         39               /* 1.386  *3.648  */
    #define     EDC_MTM_5_RATIO         29               /* 1.000  *3.648  */
    #define     EDC_MTM_6_RATIO         25 				 /* 0.772  *3.648  */
    #define     EDC_MTM_BACK_RT         123              /* 3.385  *3.648  */  

  #else /*temp*/
    #define     EDC_TM_1_RATIO          107              /* 2.842  *3.770  */
    #define     EDC_TM_2_RATIO          58               /* 1.529  *3.770  */
    #define     EDC_TM_3_RATIO          38               /* 1.0    *3.770  */
    #define     EDC_TM_4_RATIO          27               /* 0.712  *3.770  */
    #define     EDC_TM_BACK_RT          93               /* 2.48   *3.770  */
    
    #define     EDC_MTM_1_RATIO         140              /* 2.842  *3.770  */
    #define     EDC_MTM_2_RATIO         90               /* 1.529  *3.770  */
    #define     EDC_MTM_3_RATIO         60               /* 1.0    *3.770  */
    #define     EDC_MTM_4_RATIO         40               /* 0.712  *3.770  */
    #define     EDC_MTM_5_RATIO         30               /* 0.712  *3.770  */
    #define     EDC_MTM_6_RATIO         25 				 /* temp           */
    #define     EDC_MTM_BACK_RT         110              /* 2.48   *3.770  */
  #endif

/*Global Extern Functions  Declaration******************************************************/
#endif

#endif
