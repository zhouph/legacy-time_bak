
/**************************************************/
/*                                                */
/*    Define & Declare  Structure Varible         */
/*                                                */
/*                  ____ MANDO ABS TEAM.          */
/**************************************************/

/* =========================================== */
/* --->>>>>     FTCS Varibale         <<<<---- */
/* =========================================== */
extern struct TF0_BIT  TF0;
extern struct TF1_BIT  TF1;
extern struct TF2_BIT  TF2;
extern struct TF3_BIT  TF3;
extern struct TF4_BIT  TF4;
extern struct TF5_BIT  TF5;
extern struct TF6_BIT  TF6;
extern struct TF7_BIT  TF7;
extern struct TF8_BIT  TF8;

extern struct TF9_BIT  TF9;
extern struct TF10_BIT  TF10;
extern struct TF11_BIT  TF11;
extern struct TF12_BIT  TF12;
extern struct TF13_BIT  TF13;
extern struct TF14_BIT  TF14;
extern struct TF15_BIT  TF15;
extern struct TF16_BIT  TF16;
extern struct TF17_BIT  TF17;
extern struct TF18_BIT  TF18;
extern struct TF19_BIT  TF19;
extern struct TF20_BIT  TF20;
extern struct TF21_BIT  TF21;
extern struct TF22_BIT  TF22;
extern struct TF23_BIT  TF23;
extern struct TF24_BIT  TF24;
extern struct TF25_BIT  TF25;


	extern int16_t   ConvCaltorq;	/*for Ford focus c214 */
    extern int16_t   esp_cmd;
    extern int16_t   tcs_cmd;
    extern int16_t   trace_cmd;
    extern int16_t   temp_cmd;
	extern int16_t   cal_torq;
    extern int16_t   cal_torq_old;
    extern int16_t   can_ver;
    extern int16_t   tcu_torq;    
    extern uint16_t   hill_33_count;
    extern uint16_t   eng_stall_count;
    extern uint16_t   zero_count;
    extern uint16_t   mini_dec_count;
    extern uint16_t   mini_inc_count;
    extern int16_t   vmini_fl;
    extern int16_t   vmini_fr;
    extern int16_t   vmini_rl;
    extern int16_t   vmini_rr;

    extern int16_t   ay_det;
    extern int16_t   ayf_det;

    extern int16_t   ay_threshold;
    extern int16_t   ay_th_old;
    extern int16_t   ay_th_temp;
    extern int16_t   ay_th_tmp1;
    extern int16_t   ay_th_tmp2;
    extern int16_t   vel_f_old;
    extern uint16_t  eng_rpm_max;
    extern int16_t   dv;
    extern int16_t   opt_dv;
    extern int16_t   dv_rate;
    extern int16_t   dv_sum;
    extern uint8_t dv_rate_count;
    extern uint8_t hill_33_rec;
    extern int16_t   real_torq;
    extern int16_t   vel_f_mid, vel_f_mid_old;
    extern int16_t   vel_f_big;
    extern int16_t   vel_f_small;
    extern int16_t   vel_f;
    extern int16_t   vel_r;
    extern int16_t   vel_r_old;
    extern int16_t   acc_f;
    extern int16_t   acc_r_old;
    extern int16_t   acc_r;
    extern uint8_t  tm_gear;          
    extern uint8_t esp_tcs_count;   
    extern uint8_t esp_ay_count1;
    extern uint8_t esp_ay_count2;

    extern int8_t  k;
    extern int16_t   aref;
    extern int16_t   aref_old;
    extern int16_t   aref_count;
    extern int16_t   dv_alt;
    extern int16_t   dv_max;
    extern int16_t   ftc_detect_speed;
    extern uint8_t vel_f_same_count;
    extern int16_t   tcs_torq_up;
    extern uint16_t  trace_lamp_count;
    extern uint16_t  tcs_hold_count;
    extern int16_t   err_dv;
    extern int16_t   ierr_dv;
    extern uint8_t thsd_ay_count1;
    extern uint8_t thsd_ay_count2;

    extern int16_t   v_f[10];
    extern int16_t   a_f[10];
    extern int16_t   ay[10];
    extern int16_t   ayf[10];
    extern int16_t   opt_slip_min;
    extern int16_t   opt_slip;
    extern int16_t   vel_r_diff, vel_r_diff_old, vel_r_diff_old2, slip;
    extern uint8_t lowtohigh_mu_count;
    extern int8_t  gear_pos;
    extern int8_t  AT_gear_pos_old;
    extern int16_t   av_opt_slip;
    extern int16_t   av_opt_slip_old;
    extern int16_t   torq_target;
    extern int16_t   ftc_count;
    extern uint16_t  high_count;

    extern uint8_t ftcs_count1;
    extern uint8_t ftcs_count2;
    extern uint8_t ftcs_count3;
    extern uint8_t trace_count1;
    extern uint8_t trace_count2;
    extern uint8_t drv_tm_pos;
    extern int16_t   ay_diff;
    extern int16_t   ayf_diff;
    extern int16_t   ay_log;
    extern int16_t   ay_opt;
    extern int16_t   ay_aver;
    extern int16_t   ay_aver_alt;
    extern int16_t   ayf_log;
    extern int16_t   ayf_aver;
    extern int16_t   ayf_aver_alt;
    extern int16_t   err_ay;
    extern int16_t   err_ay_old;
    extern int16_t   derr_ay;
    extern int16_t   hunt_count;
    extern int16_t   diff_f, diff_f_old, diff_r, diff_r_old;
    extern int16_t   circle_count;
    extern int16_t   down_count;
    extern int16_t    lctcss16BrkCtrlMode;
    extern int16_t    lctcss16EngCtrlMode;
    extern int16_t    lctcss16EECEngCtrlMode;

		
	/* ---------------------------------------------------------- */
	/*	Variables coded for GMDAT by Kim Jeonghun in 2002.6.12    */
	/* ---------------------------------------------------------- */

	extern uint8_t DIESEL, ENG_VAR, ENG_CHR;
    extern uint8_t lamp_on_delay;	

    extern uint8_t	etcs_on_count;
    extern uint8_t	ftcs_on_count;

	extern uint8_t  FTCS_flags;	

	
	extern uint8_t	target_gear;
	extern uint8_t	prev_gear;

	/* Added by YJH in 2005.05.25 */
	extern int16_t eng_temp;

		
	
	
	
	
	extern int8_t	LOW_TO_HIGH;
	extern int8_t	TRACE_DISABEL;
	extern int8_t	TRACE_END_FIND;
/* ================================================================ */
/*	App.Field upgrade for multi-engine variables in 2003.12.08      */
/* ---------------------------------------------------------------- */

	
	/* New BTCS Structure					*/
	extern int16_t	rpm_cmd;
	extern int16_t	inter_tempW0, inter_tempW1, inter_tempW2, inter_tempW3, inter_tempW4; 
	extern int16_t	inter_tempW5, inter_tempW6, inter_tempW7, inter_tempW8, inter_tempW9;
	extern int16_t	unit_conversion;
	extern int16_t	eng_rpm_f, rpm_error, rpm_error_dif, rpm_dead_band_p, rpm_dead_band_n;
	extern int16_t	rpm_error_old1, rpm_error_old2, rpm_ctl_amt;
	extern int16_t	rpm_ctl_p_gain, rpm_ctl_d_gain, gain_decrease_ratio, gain_increase_ratio;
	
	extern uint8_t  split_flags, SplitCnt, DecreaseSplitCntInHomoMu;
	
	extern int16_t	tcs_monitor;
	extern int16_t	tcs_tempW0, tcs_tempW1, tcs_tempW2, tcs_tempW3, tcs_tempW4, tcs_tempW5;
	extern int16_t	tcs_tempW6, tcs_tempW7, tcs_tempW8, tcs_tempW9, tcs_tempW10, tcs_tempW11;
	extern int16_t	SplitTransitionCnt, SplitTransitionCntOld;		
	
	/* USA by Kim Jeonghun in 2006.02.23 */
	extern uint8_t	Low2HighSuspect;	
	extern uint8_t	Low2HighSuspectCount;
	extern uint8_t	Low2HighSuspectTimer;
	extern int16_t	Low2HighSuspectAccF;
	extern int16_t	Low2HighSuspectAccR;
	extern int16_t	Low2HighSuspectAccFTemp;
	extern int16_t	Low2HighSuspectAccRTemp;
	
	/* NEW_TCS 개발 */
	extern int16_t lts16FrontWheelSpinAverage;
	extern int16_t lts16FrontWheelSpinAverageOld;
	extern int16_t lts16RearWheelSpinAverage;
	extern int16_t lts16RearWheelSpinAverageOld; 
	extern int16_t lts16WheelSpin;
	extern int16_t lts16TargetWheelSpin;	
	extern int16_t lts16WheelSpinError;
	extern int16_t lts16WheelSpinErrorOld;
	extern int16_t lts16WheelSpinErrorDiff;	
	extern int16_t lts16PGain, lts16PGain1stCycle, lts16PGainForETCS, lts16PGainForFTCS;
	extern int16_t lts16DGain, lts16DGain1stCycle, lts16DGainForETCS, lts16DGainForFTCS;	
	extern int16_t ltcss16ReduceFactor;
	extern int16_t lts16AddPGainInTurn;
	extern int16_t lts16AddDGainInTurn;	
	extern int16_t ltcss16MinimumPgain;
	extern int16_t lts16PGainEffect;
	extern int16_t lts16DGainEffect;	
	extern int16_t lts16MulPGainAndSpinError;
	extern int16_t ltcss16PDGainEffect;
	extern int16_t ltcss16SteadyStateCtl;
	extern int16_t tcs_monitor1, tcs_monitor2, tcs_monitor3, tcs_monitor4, tcs_monitor5, tcs_monitor6, tcs_monitor7;		/*임시*/
	extern uint8_t lts8LoopCounter;	
	extern int16_t lts16TempArray[4];	
	extern int16_t lts16TempArray2By4[2][4];
	extern int16_t lts16TempArray3By3[3][3];
	extern int16_t lts16TempArray3By4[3][4];
	extern int16_t lts16TempArray4By3[4][3];
	extern uint8_t ltcsu8TargetSpinReduceThInTurn;
	extern int16_t ltcss16TargetSpinReduceAyTh;
	extern int16_t ltcss16TargetSpinReduceSteerTh;
	extern int16_t ltcss16DelYawThforTSpinReduce;

  #if __TCS_CODE_SIZE_REDUCTION_RESTORE
	extern uint8_t ltu8TargetSpinArrayHomo[2][4];
	extern uint8_t ltu8TargetSpinArraySplit[2][4];	

	
	extern uint8_t ltu8AddPGainInHighSideSpin[2][4];	// 07SWD #8	
  #endif	/* #if __TCS_CODE_SIZE_REDUCTION_RESTORE */		
	
	extern int16_t ltcsu166SecCntAfterETO;
	extern int16_t ltcsu163SecCntAfterETO;
	extern int16_t ltcsu162SecCntAfterETO;
    extern uint8_t ltcsu81SecCntAfterETO;	
    extern int16_t ltcsu16TimeCntAfterETO;
    extern uint8_t ltcsu8TorqFastRecoveryCnt;
	
    
    
    extern int16_t lts16WheelSpinFilt;
	extern int16_t lts16WheelSpinFiltOld; 

	extern int16_t ltcss16BothWheelSpinCnt;	
	extern int16_t ltcss16BothSideWheelSpinCntLH;		
	extern int16_t ltcss16BothSideWheelSpinCntRH;		
	extern int16_t ltcss16ETCSEnterExitFlags;	
	
	extern uint16_t lctcsu16etcsthreshold1;
	extern uint16_t lctcsu16etcsthreshold2;
	
	/* BTCS_TCMF_CONTROL */
	extern int16_t ltcss16BaseTarWhlSpinDiff;
	extern int16_t ltcss16BaseStTarWhlSpinDiff;
	extern int16_t ltcss16BaseHillTarWhlSpinDiff;
	extern int16_t ltcss16BaseTurnTarWhlSpinDiff;
    extern int16_t lctcss16VehSpd4TCS;
    extern uint8_t lctcsu1BTCSVehAtv;
    extern uint8_t lctcsu1BTCSVehAtvOld;
    extern uint8_t lctcsu1BTCSVehAtvRsgEdg;
    //extern uint16_t fu16CalVoltMOTOR;
    extern uint8_t lctcsu1IniMtrCtlModeOld;
    extern uint8_t lctcsu1IniMtrCtlMode;
	extern int8_t lctcss8IniMtrCtlModeCnt;
    extern uint8_t lctcsu1IniMtrCtlModeFalEdg;
    extern int16_t MFC_Current_TCS_P_OLD;
    extern int16_t MFC_Current_TCS_S_OLD;
	extern uint8_t ltcsu1MFCInitDelayNotAllow;
	extern uint8_t ltcsu8MFCInitDelayNotAllowCnt;
	extern int16_t lctcss16BaseAsymSpnCtlTh;
	extern int16_t lctcss16BaseAsymSpnCtlThInSt;
	
	extern int16_t lctcss16BaseAsymSpnCtlThInTurn;
	extern int16_t lctcss16AsymSpnCtlThInRough;
	//extern int16_t lctcss16AsymSpnCtlTh;
	extern int16_t lctcss16BaseSymSpnCtlTh;
	extern int16_t lctcss16SymSpnCtlThInRough;
	extern int16_t lctcss16SymSpnCtlThInRTA;
	extern int16_t lctcss16SymSpnCtlThInESCBrk;	
	extern int16_t lctcss16SymSpnCtlTh;
	extern int16_t lctcs16BsPG4ASFBCtlPositiveF; 
	extern int16_t lctcs16BsPG4ASFBCtlNegativeF;
	extern int16_t lctcs16BsPG4ASFBCtlPositiveR;
	extern int16_t lctcs16BsPG4ASFBCtlNegativeR;
	extern int16_t lctcs16BsStPG4ASFBCtlPositiveF;
    extern int16_t lctcs16BsStPG4ASFBCtlNegativeF;
    extern int16_t lctcs16BsStPG4ASFBCtlPositiveR;
    extern int16_t lctcs16BsStPG4ASFBCtlNegativeR;
    extern int16_t lctcs16BsHillPG4ASFBCtlPositF;  
    extern int16_t lctcs16BsHillPG4ASFBCtlNegatF;  
    extern int16_t lctcs16BsHillPG4ASFBCtlPositR;  
    extern int16_t lctcs16BsHillPG4ASFBCtlNegatR;  
    extern int16_t lctcs16BsTurnPG4ASFBCtlPositiveF;
    extern int16_t lctcs16BsTurnPG4ASFBCtlNegativeF;
    extern int16_t lctcs16BsTurnPG4ASFBCtlPositiveR;
    extern int16_t lctcs16BsTurnPG4ASFBCtlNegativeR;
	
	extern uint8_t ltcsu1VCAOffBySpdDiffFlg;
	
	extern int16_t lctcs16BsIG4ASFBCtlFront;   
	extern int16_t lctcs16BsIG4ASFBCtlRear;	   
	extern int16_t lctcs16BsStIG4ASFBCtlFront; 
  extern int16_t lctcs16BsStIG4ASFBCtlRear;
  extern int16_t lctcs16BsHillIG4ASFBCtlFront;   
	extern int16_t lctcs16BsHillIG4ASFBCtlRear;	 
  extern int16_t lctcs16BsTurnIG4ASFBCtlFront; 
  extern int16_t lctcs16BsTurnIG4ASFBCtlRear;	 
  
	extern int16_t lctcss16VehTurnIndex; 
	extern int16_t lctcss16VehTurnIndexOld; 
	extern int16_t lctcss16DctTurnAyEnterTh;
	extern int16_t lctcss16DctTurnAyFinalTh;
	extern int16_t lctcss16IniTCVCtlModeTime;
	extern int16_t lctcss16IniMtrCtlModeTime;
	

	extern uint8_t VCA_ON_TCMF;
	extern uint8_t VCA_ON_TCMF_OLD;
	extern int8_t ltcss8WhlSpinDecCntTCMF;
	extern uint8_t VCA_BRAKE_CTRL_END_TCMF;
	extern uint8_t ltcsu8VCARunningCntTCMF;
	extern uint8_t VCA_SUSTAIN_FLAG_TCMF;
	extern int16_t   ltcss16MaxVrefInVCATCMF;
	extern uint8_t ltcsu8CntAfterVCATCMF;        
	extern uint8_t lctcsu1BigSpnAwdDctd;
	
	
	extern int16_t vref_test_cnt;
	extern int16_t test_cycle;
	extern int16_t lctcss16VehSpd4TCSTemp;
	
	extern uint8_t lctcsu8SplitMuDecHilEnterCnt;
  	extern uint8_t lctcsu8SplitMuDecHilExitCnt;
	extern uint8_t lctcsu1DctBTCSplitMuFlag;
	extern uint8_t lctcsu1DctBTCSplitMuHill;
	extern uint8_t lctcsu1DctBTCHillByAx;
	extern uint16_t lctcsu16BTCStpNMovDistance;
	extern uint16_t lctcsu16BTCStpNMovBsDistance;
	extern uint16_t lctcsu16BTCStpNMovCnt;        
	
	extern uint8_t lctcsu1GearShiftFlag;
	extern uint8_t lctcsu1GearShiftFlagResetCheck;
	extern uint8_t lctcsu8GearShiftFlagResetCnt;
	
	extern uint8_t BTCS_Entry_Inhibit_ByDriver;
	extern uint8_t BTCS_Entry_Inhibit_ByVehSpd;	
	extern uint8_t BTCS_Entry_Inhibit_ByRPMTorq;
	extern int16_t lctcss16AsymSpnCtlThAddCompMd;
	
  extern uint8_t  lctcsu1FlgOfAfterStall; 
  extern uint16_t lctcsu16CntAfterStall;
  extern uint8_t  lctcsu1ABSCtrlOld4BTCSInhibit;
	
	/* BTCS_TCMF_CONTROL */ 
	
	/*===================================================================*/
	/* BTCS TUNNING PARAMETER											 */
	/*===================================================================*/
  #if __TCS_CODE_SIZE_REDUCTION_RESTORE	
	extern uint8_t	ltcsu8FrontRiseMarginHomo[2][4];
	extern int8_t	ltcss8FrontDumpMarginHomo[2][4];
	extern uint8_t	ltcsu8RearRiseMarginHomo[2][4];
	extern int8_t	ltcss8RearDumpMarginHomo[2][4];
	extern uint8_t	ltcsu8FrontRiseMarginSplit[2][4];
	extern int8_t	ltcss8FrontDumpMarginSplit[2][4];
	extern uint8_t	ltcsu8RearRiseMarginSplit[2][4];
	extern int8_t	ltcss8RearDumpMarginSplit[2][4];
	extern uint8_t	ltcsu8FrontHoldScanInState1Homo[2][4];
	extern uint8_t	ltcsu8RearHoldScanInState1Homo[2][4];
	extern uint8_t	ltcsu8FrontHoldScanInState1Split[2][4];
	extern uint8_t	ltcsu8RearHoldScanInState1Split[2][4];
  #endif	/* #if __TCS_CODE_SIZE_REDUCTION_RESTORE */	
	/*===================================================================*/	
	
/* Variables for ITCC Control ↘*/	
	extern uint16_t	LimitCardanShaftTorque;
	extern uint8_t   s8TODLoopCounter;
  #if (__CAR == HMC_EN)
    extern int16_t   s16TODLimitTorq[3][3] ;
  #else
    extern int16_t   s16TODLimitTorq[3][3] ;  
  
  #endif
	extern int16_t   s16TODLimitTorqTemp[3];                                  
    extern int16_t   s16SetTODSpeed[3];	
    
    
    
/* Variables for ITCC Control ↗*/		

extern int16_t 	HIGH_MU_EMS, LOW_MU_EMS;
/*              [[[SPEED SETTING]]] */
/*		[[[SPEED SETTING]]]		[[[[LOW MU]]]] */
extern int16_t 	LOW_MU_ESP_TORQ_CON_L_SPEED, LOW_MU_ESP_TORQ_CON_M_SPEED, LOW_MU_ESP_TORQ_CON_H_SPEED;
/*		[[[SPEED SETTING]]]		[[[[MED MU]]]] */
extern int16_t 	MED_MU_ESP_TORQ_CON_L_SPEED, MED_MU_ESP_TORQ_CON_M_SPEED, MED_MU_ESP_TORQ_CON_H_SPEED;
/*		[[[SPEED SETTING]]]		[[[[HIGH MU]]]] */
extern int16_t 	HIGH_MU_ESP_TORQ_CON_L_SPEED, HIGH_MU_ESP_TORQ_CON_M_SPEED, HIGH_MU_ESP_TORQ_CON_H_SPEED;
 #if __TCS_CONTROLLER_UNIFICATION
extern uint8_t lctcsu8FastTorqRecoveryStatus;
 #else
extern uint8_t FORCED_TORQ_RISE_MODE; 
 #endif
extern int16_t lts16TorqRiseThr;
extern uint8_t ltu8TorqRiseRate;
extern uint16_t ltu16TorqRiseLimit;
extern int16_t ltcss16DelVLRLimit;
extern uint8_t lespu8GearPosBeforeEECActivated;
extern int16_t ltcss16AccOfNonDrivenWheel;
extern int16_t ltcss16GoodTargetTrackingCnt;

extern uint8_t temp_tm_gear;		// 07SWD #6

extern int16_t ltcss16MiniTireTendencyCnt;
extern int16_t ltcss16ResetMiniTireDetectCnt;

extern int16_t ltcss16LowTorqueLevelCnt;
extern int16_t lctcss16SplitHillDctCnt;

/* #if __TCS_2WL_MINI_DETECTION */
extern uint8_t lctcsu8MiniTireSuspectResetCnt;

/* #endif __TCS_2WL_MINI_DETECTION */


#if __MY08_TCS_SPLIT_TORQ_CTL_PATTERN
extern int16_t lctcss16MinimumTorqLevel, lctcss16MinimumTorqLevelOld;
extern int16_t lctcss16MinimumTorqLevelDecTh, lctcss16MinimumTorqLevelMinTh;
#endif	/* #if __MY08_TCS_SPLIT_TORQ_CTL_PATTERN */

#if __MY08_TCS_DCT_L2SPLIT
extern int16_t ltcss16SpinFL, ltcss16SpinFR, ltcss16SpinRL, ltcss16SpinRR;
extern int16_t lctcss16TorqInput4L2SpltDct, lctcss16BaseTorqLevel4L2SpltDct;
#endif

#if __MY08_TCS_PREVENT_SPIN_OVERSHOOT
extern int16_t ltcss16WheelSpinOverShootTh, ltcss16WheelSpinErrDifMax, ltcss16WheelSpinErrDifMin;
extern int16_t lctcss16OverShootTorqLvl;
extern int16_t lctcss161SpinOverShootCnt;
extern int16_t lctcss16OverShootTorqRedAmount;
#endif		/* #if __MY08_TCS_PREVENT_SPIN_OVERSHOOT */


#if __MY08_TCS_1ST_SCAN_CTL
extern int16_t lctcss161stScanTorqLevelInTurn[3][3];
extern int16_t lctcss161stScanTorqLevelHomo[2][4]; 
#endif	/* #if __MY08_TCS_1ST_SCAN_CTL */

extern int16_t lctcsu8TargetSpinInTurn[3][3];

#if __MY08_TCS_DISCRETE_ENG_CTL
extern int16_t ltcss16EngTrqHoldScan;
extern int16_t ltcss16TCSPhase, ltcss16TCSPhaseOld, ltcss16PhaseChgTh;
extern int16_t ltcss16TCSPhaseMaintainCnt, ltcss16DiscreteCtrlInput;
#endif /* #if __MY08_TCS_DISCRETE_ENG_CTL */

#if __MY08_TCS_TORQUE_LEVEL_SEARCH
extern uint8_t	FTCS_flags_old;
extern int16_t	ltcss16BaseTorqueLevel, ltcss16BaseTorqueLevelOld;
extern int16_t	ltcss16FastTorqueInputValue;
extern uint8_t	ltcsu856msCnt;
extern uint8_t	ltcsu8TorqReductionFactor[2][4];
extern uint8_t	ltcsu8TorqRecoveryFactor[2][4];
#endif	/* #if __MY08_TCS_TORQUE_LEVEL_SEARCH */

#if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC
extern int16_t	lctcss16TorqLevelAfterEEC[3][3];
extern int16_t	lctcss16TorqLvlAfterEEC;
extern int16_t	lctcss16TorqIncRateAfterEEC[3][3];
extern int16_t	lctcss16TorqUpRateAfterEEC;
#endif	/* #if __MY08_TCS_TORQ_UP_LVL_AFTER_EEC */

#if __MY08_TCS_DCT_H2L
extern int16_t	ltcss16H2LTimer;
#endif	/* #if __MY08_TCS_DCT_H2L */


#if __GM_FailM
extern uint8_t	U8_TCS_FAIL_MODE_BRAKE;
extern uint8_t	U8_TCS_FAIL_MODE_ENGINE;
extern uint8_t	U8_TCS_FAIL_MODE_INHIBIT;
#endif


  
extern uint8_t	ltcsu8CntAfterHomo2SPlit;

extern int16_t   	cal_torq_abs;
extern int16_t   	cal_torq_rel;

extern int16_t lctcss16DiagonalWhlSpinCnt;
extern int16_t lctcss16ThOfDct4WDStuckByDWSpn;
extern int16_t ltcss16BigSpinCnt, ltcss16SpinDecCnt;

#if __TCS_PREVENT_LONG_PHASE0
extern int16_t lctcss16Phase0RunCnt;
#endif	/* #if __TCS_PREVENT_LONG_PHASE0 */

#if __TCS_CONTROLLER_UNIFICATION
extern uint8_t lctcsu8PosSpinErrDiffCnt1;
extern uint8_t lctcsu8PosSpinErrDiffCnt2;
extern uint8_t lctcsu8NegSpinErrDiffCnt1;
extern uint16_t ltcsu16TCSMuLevel;
extern uint16_t ltcsu16NormalizedLongG;
extern uint8_t	lctcsu8NormalizedLongGFactor;
#endif

#if __MU_EST_FOR_TCS
extern int16_t lts16TempArray3By4[3][4];
#endif


extern uint8_t	lctcsu8FLMuHighCnt;			 /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8FRMuHighCnt;			 /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8RLMuHighCnt;          /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8RRMuHighCnt;          /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8NoOfFLMuHighCnt;      /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8NoOfFRMuHighCnt;      /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8NoOfRLMuHighCnt;      /* #if __TCS_HUNTING_DETECTION */
extern uint8_t	lctcsu8NoOfRRMuHighCnt;      /* #if __TCS_HUNTING_DETECTION */


extern int16_t	ltcss16BumpCounter;		/* #if __TCS_BUMP_DETECTION */
extern uint16_t	ltcsu16SumOfSpin;		/* #if __TCS_BUMP_DETECTION */
extern uint16_t	ltcsu16AvgOfSpin;		/* #if __TCS_BUMP_DETECTION */
extern int16_t	ltcs16BumpArad;

#if (__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)	
extern	int8_t ldtcs8BumpWheelIndexByArad;
extern int16_t	ltcs16BumpAradFront;
extern int16_t	ltcs16BumpAradRear;
#endif

extern int16_t 	lctcss16NewEngOKTorq;	/* __VARIABLE_MINIMUM_TORQ_LEVEL */



extern uint8_t lctcsu8VIBEngineCtlScan;
extern uint16_t lctcsu16VIBEngineCtlCnt;
extern uint16_t lctcsu16VIBEngineCtlPeriod;
extern int16_t lctcss16VIBCalTorqState;
extern int16_t lctcss16VIBCalTorqLevel;
/*Structure 구조 사용 불가로 pragma 내 변수 선언*/


#if __TCS_DRIVELINE_PROTECT
extern int16_t	ltcss16FrontAxleMeanSpd, ltcss16RearAxleMeanSpd, ltcss16DeltaSpdOfFrontAndRear, ltcss16DeltaSpdOfFLandFR, ltcss16DeltaSpdOfRLandRR;
#endif	/* #if __TCS_DRIVELINE_PROTECT */

/*Structure 구조 사용 불가로 pragma 내 변수 선언*/   

/* A/T gear control correction */
extern uint8_t	lespu8_BSTGRReqTypeOld;
/* A/T gear control correction */

extern int16_t	lctcss16LowSpinCntIn1stCycle;


/* TORQ_RECOVERY_DEPEND_ON_STEER */
extern int8_t	lctcss8SteerToZeroCnt;
/* TORQ_RECOVERY_DEPEND_ON_STEER */

/* MAXIMUM_RPM_LIMIT */
extern int16_t lctcss16AllowableMaxRPM;
extern int16_t lctcss16OverMaxRPMCnt;
extern int16_t lctcss16OverMaxRPMCntold;
extern int16_t lctcss16Spd1ForRPMLimit,lctcss16Spd2ForRPMLimit;
extern int16_t lctcss16AllowableMinRPM;
extern int16_t lctcss16UnderMinRPMCnt;
extern int16_t lctcss16UnderMinRPMCntold;
/* MAXIMUM_RPM_LIMIT */

/* ENGINE_STALL_DETECION_REFINE */
extern int16_t ldtcss16HillDetectSustainCnt;
extern int16_t ldtcss16StallRiskIndex;
extern int16_t ldtcss16StallDctRPM;
/* ENGINE_STALL_DETECION_REFINE */


/*__TCS_CTRL_DEC_IN_GR_SHFT_MT*/
extern uint16_t lctcsu16GearShiftForMTcnt;
/*__TCS_CTRL_DEC_IN_GR_SHFT_MT*/

/*For BMW Demo*/
extern uint8_t lctcsu8FunctionLampOnCount;
extern uint16_t lctcsu16FunctionLampOnTime;
extern uint16_t lctcsu16FunctionLampOffCount;
/*For BMW Demo*/

extern uint8_t	lespu8TqIntvnTyp;
extern uint16_t	lespu16TorqReqVal;

extern int16_t lctcss16EstMuInTurn;
extern int16_t lctcss16EstMuInstraight;

/*TCS spin for enter threshold*/ 
extern int16_t lctcss16BigSpin;
extern int16_t lctcss16SmallSpin;

	#if  (__IDB_SYSTEM == ENABLE)
extern int16_t latcss16TarPreGrdtMd;
extern int16_t latcss16PricTarPreRate;
extern int16_t latcss16SeccTarPreRate;
	#endif