#ifndef __LSMSCCALLLOGDATA_H__
#define __LSMSCCALLLOGDATA_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/



/*Global Extern Functions Declaration ******************************************/
extern void LSMSC_vCallMSCLogData(void);

#endif
