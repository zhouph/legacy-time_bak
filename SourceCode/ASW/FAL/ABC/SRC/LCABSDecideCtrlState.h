#ifndef __LCABSDECIDECTRLSTATE_H__
#define __LCABSDECIDECTRLSTATE_H__

/*includes********************************************************************/
#include "LVarHead.h"



/*Global MACRO CONSTANT Definition********************************************/

#if __L_CYCLETIME==LOOP_10MS
    #define U8_DUMP_CNT_FOR_CLEAR_GMA_L1    L_TIME_100MS
#else
    #define U8_DUMP_CNT_FOR_CLEAR_GMA_L1    L_TIME_105MS
#endif

#define __REF_P_RESOL_CHANGED_10_TO_100		ENABLE

#define __REORGANIZE_STABLE_DCT             ENABLE

#define __MIRC_IMPROVE          1
#define __RESET_ABS_BY_ACCEL    1

  #if __VDC && __MP_COMP
#define __HIGH_MPRESS_COMP      1
  #else
#define __HIGH_MPRESS_COMP      0  
  #endif

  #if __MGH60_ABS_CONCEPT_RELEASED
#if __VDC
#define __SPLIT_STABILITY_IMPROVE_GMA       1
#else
#define __SPLIT_STABILITY_IMPROVE_GMA       0
#endif
#define __RISE_DELAY_SP_FHSIDE_FRONT        1
#define __ROUGH_ROAD_DUMP_IMPROVE           0
#define __REDUCE_SPLIT_LOWSIDE_SLIP         0
#define __CALC_EFFECTIVE_DUMPSCANS          1
#define __OUT_ABS_PULSEUP_FOR_T2B_AT_LMU    0
#define __LOW_SPEED_DECEL_IMPROVE           0
#define __INHIBIT_SPLIT_LOW_FRONT_UR        1
#if __OUT_ABS_P_RATE_COMPENSATION
  #define __OUT_ABS_STABLE_DUMP_ENABLE      1
#else
  #define __OUT_ABS_STABLE_DUMP_ENABLE      0
#endif

  #elif __MGH60_ABS_IMPROVE_CONCEPT
#if __VDC
#define __SPLIT_STABILITY_IMPROVE_GMA       1
#else
#define __SPLIT_STABILITY_IMPROVE_GMA       0
#endif
#define __RISE_DELAY_SP_FHSIDE_FRONT        1
#define __ROUGH_ROAD_DUMP_IMPROVE           1
#define __REDUCE_SPLIT_LOWSIDE_SLIP         1
#define __CALC_EFFECTIVE_DUMPSCANS          1
#define __OUT_ABS_PULSEUP_FOR_T2B_AT_LMU    1
#define __LOW_SPEED_DECEL_IMPROVE           1
#define __INHIBIT_SPLIT_LOW_FRONT_UR        1
#if __OUT_ABS_P_RATE_COMPENSATION
  #define __OUT_ABS_STABLE_DUMP_ENABLE      1
#else
  #define __OUT_ABS_STABLE_DUMP_ENABLE      0
#endif
  #else
#if __VDC
#define __SPLIT_STABILITY_IMPROVE_GMA       1
#else
#define __SPLIT_STABILITY_IMPROVE_GMA       0
#endif
#define __RISE_DELAY_SP_FHSIDE_FRONT        1
#define __ROUGH_ROAD_DUMP_IMPROVE           0
#define __REDUCE_SPLIT_LOWSIDE_SLIP         0
#define __CALC_EFFECTIVE_DUMPSCANS          0
#define __OUT_ABS_PULSEUP_FOR_T2B_AT_LMU    0
#define __LOW_SPEED_DECEL_IMPROVE           0
#define __INHIBIT_SPLIT_LOW_FRONT_UR        1
#define __OUT_ABS_STABLE_DUMP_ENABLE        0
  #endif

  #if __VDC
#define __FRONT_OUT_ABS_SL_DUMP         	ENABLE
  #endif

#define 	WHEEL_CTRL_NON				     0
#define 	WHEEL_CTRL_CBS				     1
#define		WHEEL_CTRL_OUT_ABS			     2
#define		WHEEL_CTRL_NORMAL_ABS		     3
#define		WHEEL_CTRL_YMR_ABS			     4
#define		WHEEL_CTRL_MSL_REAR			     5
#define		WHEEL_CTRL_UNSTABLE_REAPPLY	     6
#define		WHEEL_CTRL_ABS_FADE_OUT		     7
#define		WHEEL_CTRL_EBD				     8
#define		WHEEL_CTRL_EBD_FADE_OUT			 9
#define		WHEEL_CTRL_ESC_COOP			     10

#define __CHANGED_TO_DECIDE_ONOFF 			ENABLE
#define __DISTRIBUTION_INIT_RISE_INABS		DISABLE
#define __EST_PRESS_COMP_RISE_INABS    		DISABLE
#define __EST_PRESS_COMP_INIT_RISE_INABS    DISABLE
#define __EST_PRESS_COMP_NTH_RISE_INABS		DISABLE
#define __WHEEL_CONDI_COMP_RISE_INABS	    ENABLE
#define __INABS_INIT_RISE_DP_FROM_DUMP		ENABLE
#define __MASTER_PATTERN_CYCLIC_COMP		DISABLE
#define __MASTER_PATTERN_SPECIAL_COMP		ENABLE
#define __MASTER_PATTERN_MP_CYCLIC_COMP		DISABLE
#define __ABS_DESIRED_BOOST_TARGET_P		ENABLE

/* -------- roughness compensation in high speed ---------*/ 
#define __ROUGH_COMP_IMPROVE				ENABLE
#define __BUMP_COMP_IMPROVE					ENABLE
#define __OUT_ABS_DV_THRESHOLD_CHANGE		ENABLE

#if __IDB_LOGIC == ENABLE
  #define M_ON_OFF_BUILT_PATTERN            ENABLE
  #define M_DP_BASE_BUILT_RISE              ENABLE
  

  #define DEFAULT_PULSE_UP_DELTAP           MPRESS_5BAR
  #define S16_HOLD_MODE_FORCED      1
  #define S16_HOLD_MODE_PATTERN     0
  
#endif

/************** LOGIC TUNING ***************************/
#define S16_GMA_H_MAX_SPEED    VREF_120_KPH
#define S16_GMA_H_OVER_SPEED   VREF_100_KPH

  #if (__CAR==GM_M300) || (__CAR==GM_T300) || (__CAR==GM_M350)
#define SP_LOW_FRT_UR_ALLOW_ARAD         ARAD_15G0
  #elif (__CAR==IRAN_SAMAND)
#define SP_LOW_FRT_UR_ALLOW_ARAD         ARAD_5G0
  #elif (__CAR==HMC_PA)||(__CAR==KMC_SA)
#define SP_LOW_FRT_UR_ALLOW_ARAD         ARAD_5G0
  #elif (__CAR==HMC_JB)||(__CAR==HMC_MC)
#define SP_LOW_FRT_UR_ALLOW_ARAD         ARAD_5G0
  #else
#define SP_LOW_FRT_UR_ALLOW_ARAD         ARAD_5G0
  #endif

  /* -------- Outside ABS press limitation tuning ---------*/
#if !__VDC
  #if (__CAR==HMC_LM)

#define S8_L1_LOWSIDE_PEAK_DEC      	ARAD_3G0
#define S8_L1_SLIP_DIFF_50_80      		LAM_10P
#define S8_L1_SLIP_DIFF_80_M        	LAM_5P

#define S8_L2_LOWSIDE_PEAK_DEC      	16 /*4g*/
#define S8_L2_SLIP_DIFF_50_80       	18
#define S8_L2_SLIP_DIFF_80_M        	13

#define S16_OUTABS_YMR_ALLOW_SPEED      VREF_50_KPH
#define S16_OUTABS_YMR_DUMP_ALLOW_SPEED VREF_50_KPH
#define S16_OUTABS_YMR_DUMP_HIGH_SPEED  VREF_100_KPH
#define S16_OUTABS_YMR_DUMP_MIN         1
#define S16_OUTABS_YMR_DUMP_MAX         1

  #elif (__CAR==HMC_EA_EV)

#define S8_L1_LOWSIDE_PEAK_DEC      	ARAD_3G0
#define S8_L1_SLIP_DIFF_50_80       	LAM_10P
#define S8_L1_SLIP_DIFF_80_M        	LAM_5P

#define S8_L2_LOWSIDE_PEAK_DEC      	22 /*5.5g*/
#define S8_L2_SLIP_DIFF_50_80       	LAM_20P
#define S8_L2_SLIP_DIFF_80_M        	LAM_12P

#define S16_OUTABS_YMR_ALLOW_SPEED      VREF_30_KPH
#define S16_OUTABS_YMR_DUMP_ALLOW_SPEED VREF_40_KPH
#define S16_OUTABS_YMR_DUMP_HIGH_SPEED  VREF_100_KPH
#define S16_OUTABS_YMR_DUMP_MIN         1
#define S16_OUTABS_YMR_DUMP_MAX         4

  #else /*(__CAR==default)*/

#define S8_L1_LOWSIDE_PEAK_DEC      	ARAD_3G0
#define S8_L1_SLIP_DIFF_50_80       	LAM_10P
#define S8_L1_SLIP_DIFF_80_M        	LAM_5P

#define S8_L2_LOWSIDE_PEAK_DEC      	22 /*5.5g*/
#define S8_L2_SLIP_DIFF_50_80       	LAM_15P
#define S8_L2_SLIP_DIFF_80_M        	LAM_10P

#define S16_OUTABS_YMR_ALLOW_SPEED      VREF_30_KPH
#define S16_OUTABS_YMR_DUMP_ALLOW_SPEED VREF_40_KPH
#define S16_OUTABS_YMR_DUMP_HIGH_SPEED  VREF_100_KPH
#define S16_OUTABS_YMR_DUMP_MIN         1
#define S16_OUTABS_YMR_DUMP_MAX         1

  #endif /* __CAR==default */
#endif/* !__VDC */

  /* -------- Low braking compensation tuning ---------*/
  #if __SLIGHT_BRAKING_COMPENSATION
#define S16_SLIGHT_BRAKING_PRESS        MPRESS_40BAR
#define S16_VERY_SLIGHT_BRAKING_PRESS   MPRESS_10BAR
#define U8_SLIGHT_BRAKING_DET_TIME		L_TIME_300MS/* MGH-80 : 200ms, Gen3 : 300ms */
#define U8_MIN_dP_RISE_AT_LOW_MP_F      6
#define U8_MIN_dP_RISE_AT_LOW_MP_R      4
#define U8_MIN_INIT_dP_RISE_AT_LOW_MP_F 6
#define U8_MIN_INIT_dP_RISE_AT_LOW_MP_R 4
  #endif

#define S8_GMA_RiseDP_H_SPEED_PHASE1		MPRESS_10BAR
#define S8_GMA_RiseDP_H_SPEED_PHASE2		MPRESS_6BAR
#define S8_GMA_RiseDP_H_SPEED_PHASE3		MPRESS_4BAR
#define S8_GMA_RiseDP_ML_SPEED_PHASE1   	MPRESS_10BAR
#define S8_GMA_RiseDP_ML_SPEED_PHASE2   	MPRESS_8BAR
#define S8_GMA_RiseDP_ML_SPEED_PHASE3		MPRESS_4BAR
#define S8_LFC_Conven_Reapply_DP_F			MPRESS_5BAR

#define S8_LFC_Conven_Reapply_DP_R_EBD		MPRESS_8BAR

  #if __ABS_RBC_COOPERATION
#define U8_MAX_RBC_dP_RISESCAN          8
#define U8_MIN_RBC_dP_RISESCAN          2
/* ------ Front Slip Det for RBC ------ */
#define S8_WHEEL_SPEED_DIFF 			20
/*#define S8_FRONT_SLIP_DET_THRES_SLIP    LAM_5P*/
/*#define S8_FRONT_SLIP_DET_THRES_VDIFF   S8_WHEEL_SPEED_DIFF*/
/* ------------------------------------ */
#define	S16_RBC_DP_COMP_RATIO_1			MPRESS_2BAR
#define	S16_RBC_DP_COMP_RATIO_2			MPRESS_10BAR
  #endif

  #if __REDUCE_ABS_TIME_AT_BUMP
    #if __CAR==HMC_VF
#define U8_DV_OVERSHOOT_MAX_AT_BUMP_F     VREF_5_KPH
#define U8_DV_INC_RATIO_AT_BUMP_F         16  /* resolution 10% - multiply with vfilt_comp_overshoot*/
#define U8_DV_OVERSHOOT_MAX_AT_BUMP_R     VREF_4_KPH
#define U8_DV_INC_RATIO_AT_BUMP_R         40   /* resolution 10% - multiply with vfiltn*/
    #else /*__CAR==HMC_VF*/
#define U8_DV_OVERSHOOT_MAX_AT_BUMP_F     VREF_4_KPH
#define U8_DV_INC_RATIO_AT_BUMP_F         15  /* resolution 10% - multiply with vfilt_comp_overshoot*/
#define U8_DV_OVERSHOOT_MAX_AT_BUMP_R     VREF_3_KPH
#define U8_DV_INC_RATIO_AT_BUMP_R         25   /* resolution 10% - multiply with vfiltn*/
    #endif /*__CAR==HMC_VF*/
  #endif /* #if __REDUCE_ABS_TIME_AT_BUMP */

  #if (__OUT_ABS_DV_THRESHOLD_CHANGE==ENABLE)
#define U8_F_OUT_DV_COMP_LOW_SPD  25
#define U8_F_OUT_DV_COMP_HI_SPD   10
  #endif /* #if (__OUT_ABS_DV_THRESHOLD_CHANGE==ENABLE) */
  
  #if (__BUMP_COMP_IMPROVE==ENABLE)
#define U8_R_DV_COMP_LOW_SPD_BMP 	  40
#define U8_R_DV_COMP_HI_SPD_BMP       20
#define U8_R_DV_COMP_LOW_SPD_WHL_BMP  25
#define U8_R_DV_COMP_HI_SPD_WHL_BMP   10
  #endif /* #if (__BUMP_COMP_IMPROVE==ENABLE) */
  
  #if (__CAR==BMW_740i)||(__CAR==GM_MALIBU)||(__CAR==HMC_DH)
#define	U8_REAR_SL_START_CYCLE  1
  #else
#define	U8_REAR_SL_START_CYCLE  2
  #endif

#define U8_HOLD_TIMER_INIT_RISE_DISTBN			L_TIME_20MS
#define U8_HOLD_TIMER_NTH_REDUCED_HOLD    		L_TIME_40MS

#define S16_INIT_RISE_DISTBN_F_TH_LOW			MPRESS_8BAR
#define S16_INIT_RISE_DISTBN_F_TH_HIGH			MPRESS_10BAR
#define S16_INIT_RISE_DISTBN_R_TH_LOW			MPRESS_8BAR
#define S16_INIT_RISE_DISTBN_R_TH_HIGH			MPRESS_10BAR

 #if __EST_PRESS_COMP_RISE_INABS == ENABLE

#define S8_1ST_RISE_EST_P_POS_COMP_TH			MPRESS_3BAR
#define S8_1ST_RISE_EST_P_NEG_COMP_TH			(-MPRESS_3BAR)
#define S8_NTH_RISE_EST_P_POS_COMP_TH			MPRESS_3BAR
#define S8_NTH_RISE_EST_P_NEG_COMP_TH			(-MPRESS_3BAR)
#define S8_1ST_RISE_EST_P_ERR_POS_COMP			3 /*30%*/
#define S8_1ST_RISE_EST_P_ERR_NEG_COMP			3 /*30%*/
#define S8_NTH_RISE_EST_P_ERR_POS_COMP			6 /*30%*/
#define S8_NTH_RISE_EST_P_ERR_NEG_COMP			4 /*30%*/
 #endif

#define S16_INIT_DISTBN_CYC_COMP_F_LOW			10
#define S16_INIT_DISTBN_CYC_COMP_F_HIGH			10
#define S16_INIT_DISTBN_CYC_COMP_R_LOW			10
#define S16_INIT_DISTBN_CYC_COMP_R_HIGH			10

#define S16_RISE_COMP_AFT_STRK_RCVR				MPRESS_2BAR

/************** LOGIC TUNING ***************************/
  
/***************************** Yaw ���� �̿� GMA Control *********************************/
#define USE_INC_AND_DEC 3 /* Not used since MGH-60 */
#define USE_INC_ONLY    2 /* Not used since MGH-60 */
#define USE_DEC_ONLY    1 /* Not used since MGH-60 */
#define USE_NONE        0
#define FEEDBACK_FOR_GMA        1    /* Enable GMA Control adaptation per yaw  */
#define FEEDBACK_FOR_MIRC       2    /* Enable MIRC Control adaptation per yaw */
#define FEEDBACK_FOR_ALL_CTRL   3    /* Enable All Control per yaw */

#define STB_DUMP_LEVEL_HIGH                 8
#define STB_DUMP_LEVEL_MED_HIGH             6
#define STB_DUMP_LEVEL_MED                  4
#define STB_DUMP_LEVEL_LOW                  2

/************** LOGIC TUNING ***************************/
#define S16_SP_FHSIDE_RISE_DELAY_SP_H       VREF_100_KPH
#define S16_SP_FHSIDE_RISE_DELAY_SP_L       VREF_20_KPH
#define S8_SP_FHSIDE_RISE_DELAY_ACCEL_H     ARAD_10G0
#define S8_SP_FHSIDE_RISE_DELAY_ACCEL_L     ARAD_6G0
#define S8_SP_FHSIDE_RISE_DELAY_SLIP_H      -LAM_50P
#define S8_SP_FHSIDE_RISE_DELAY_SLIP_L      -LAM_20P

/*#define S8_STABLE_DUMP_MAX                  10*/

#define	U8_HDC_ABS_MSL_DUMP_MAX		3

/*
#define	S16_SP_HI_SIDE_STB_DMP_YDF_TH_H		500
#define	S16_SP_HI_SIDE_STB_DMP_YDF_TH_M		700
#define	S16_SP_HI_SIDE_STB_DMP_YDF_TH_L		900
#define	S16_SP_HI_SIDE_STB_DMP_YDF_TH_BL	900	
*/

#define S16_HI_SIDE_HOLD_WH_PRESS	MPRESS_90BAR

/************** LOGIC TUNING ***************************/

#if (__CAR==KMC_SA) || (__CAR==HMC_PA)
#define BumpDct_ARef_At_LDump  ARAD_12G0
#define BumpDct_ARef_At_HDump  ARAD_18G0
#define APAVG_BUMP 800
#define APAVG_LMU  50
#else
#define BumpDct_ARef_At_LDump  ARAD_10G0
#define BumpDct_ARef_At_HDump  ARAD_15G0
#define APAVG_BUMP 800
#define APAVG_LMU  50
#endif

  /* for Mu Estimation from skid */
#define LMU_FACTOR  1
#define MMU_FACTOR  2
#define MHMU_FACTOR 3
#define HMU_FACTOR  4

#if __CAR==HMC_BH
    #define U8_HIGH_MU_SKID_F    70
    #define U8_HIGH_MU_SKID_R    60
    
    #define S8_HIGH_MU_SLIP_F    20 
    #define S8_HIGH_MU_SLIP_R    15 
    
    #define U8_LOW_MU_SKID_F     50
    #define U8_LOW_MU_SKID_R     40
    
    #define S8_LOW_MU_SLIP_F     40
    #define S8_LOW_MU_SLIP_R     20
#else
    #define U8_HIGH_MU_SKID_F    90
    #define U8_HIGH_MU_SKID_R    80
    
    #define S8_HIGH_MU_SLIP_F    20 
    #define S8_HIGH_MU_SLIP_R    15 
    
    #define U8_LOW_MU_SKID_F     60
    #define U8_LOW_MU_SKID_R     65
    
    #define S8_LOW_MU_SLIP_F     40
    #define S8_LOW_MU_SLIP_R     20
#endif

  /* reduce rise delay for cascading */
#define F_RISE_DELAY_FOR_CASCADING  10
#define R_RISE_DELAY_FOR_CASCADING  10

  /* reduce cycle time@outside ABS*/
#define U8_Hold_Timer_New_Ref_Default L_TIME_50MS 

/*****************ABS ALLOW SLIP LOGIC TUNING*****************/
/*
#define S16_ABS_ALLOW_SLIP_REF_LSPD			VREF_30_KPH
#define S16_ABS_ALLOW_SLIP_REF_HSPD			VREF_100_KPH
*/

/********************** Stable Forced Hold by vref *************************/
#define	__STABLE_HOLD_THRESHOLD_BY_SPD			ENABLE

/*#define	S8_FORCED_HOLD_SLIP_SP_HIGH_F	S8_FORCED_HOLD_SLIP_HIGH_F*/
/***************************************************************************/  

  #if (__CAR == HMC_BH)
#define RISE_PER_SCAN_LMU_F   3
#define RISE_PER_SCAN_LMU_R   4

#define RISE_PER_SCAN_HMU_F   5
#define RISE_PER_SCAN_HMU_R   4
  #else
#define RISE_PER_SCAN_LMU_F   2
#define RISE_PER_SCAN_LMU_R   3

#define RISE_PER_SCAN_HMU_F   4
#define RISE_PER_SCAN_HMU_R   4
  #endif

    /* ------ UR Logic Tuning ------ */
#define U8_MIN_DP_RISE_FOR_BUMP_OR_WA   L_TIME_30MS

/*****************moved from state.c *********************************************/
#define CLEAR_GMA1      10
#define CLEAR_GMA       5
#define End_GMA         1

  #if __UNSTABLE_REAPPLY_dV_COMP
#define dV_COMP_At_dP_Ctrl_CYCLE_F  16
#define dV_COMP_At_dP_Ctrl_CYCLE_R  12
  #endif

#define LOWEST_PRESS_LEVEL MPRESS_3BAR
#define S16_MINUS_B_MAX    -ARAD32_0G5 

#define LEVEL1 1
#define LEVEL2 2
#define LEVEL2_DUMP 3



/*------- stroke recovery parameter setting ----------*/
#define U8_HOLD_DURING_STRK_RECVRY  (S8_SR_WHLCTRLREQ_HOLD)
#define U8_RISE_BF_STRK_RECVRY      (S8_SR_WHLCTRLREQ_PRERISE)
#define U8_RISE_AF_STRK_RECVRY      (S8_SR_WHLCTRLREQ_POSTRISE)


/*Global Type Declaration ****************************************************/
typedef struct
{
    uint16_t ABS_BIT_07     :1;
    uint16_t ABS_BIT_06     :1;
    uint16_t ABS_BIT_05     :1;
    uint16_t ABS_BIT_04     :1;
    uint16_t ABS_BIT_03     :1;
    uint16_t ABS_BIT_02     :1;
    uint16_t ABS_BIT_01     :1;
    uint16_t ABS_BIT_00     :1;
}ABS_FLAG_t;

#define BRAKE_BY_MPRESS_FLG         	lcAbsBrakingFlg.ABS_BIT_07  
#define BRAKE_BY_MPRESS_FLG_OLD     	lcAbsBrakingFlg.ABS_BIT_06  
#define ABS_DURING_MPRESS_FLG       	lcAbsBrakingFlg.ABS_BIT_05  
#define MPRESS_RELEASE_DURING_ABS   	lcAbsBrakingFlg.ABS_BIT_04  
#define ABS_BEFORE_MPRESS_BRAKE     	lcAbsBrakingFlg.ABS_BIT_03  
#define BRAKE_RELEASE2              	lcAbsBrakingFlg.ABS_BIT_02  
#define ACTIVE_BRAKE_FLG            	lcAbsBrakingFlg.ABS_BIT_01  
#define ACTIVE_BRAKE_FLG_OLD        	lcAbsBrakingFlg.ABS_BIT_00  

#define ABS_DURING_ACTIVE_BRAKE_FLG     lcAbsBrakingFlg2.ABS_BIT_07 
#define ACTIVE_BRAKE_RELEASE_DURING_ABS lcAbsBrakingFlg2.ABS_BIT_06 
#define ABS_INHIBITION_flag             lcAbsBrakingFlg2.ABS_BIT_05 
#define Front_ABS_INHIBITION_flag       lcAbsBrakingFlg2.ABS_BIT_04 
#define REAR_ABS_INHIBITION_flag        lcAbsBrakingFlg2.ABS_BIT_03 
#define REAR_EBD_INHIBITION_flag        lcAbsBrakingFlg2.ABS_BIT_02
#define lcabsu1AbsErrorFlag             lcAbsBrakingFlg2.ABS_BIT_01
#define lcabsu1LowSpeedInhibitFlag      lcAbsBrakingFlg2.ABS_BIT_00

#if __UNSTABLE_REAPPLY_ENABLE
#define SL_UNSTABLE_RISE_R              lcAbsCtrlFlg0.ABS_BIT_07
#endif
#define lcabsu1OverInsideFrontCtrl      lcAbsCtrlFlg0.ABS_BIT_06
#define lcabsu1OverInsideRearCtrl       lcAbsCtrlFlg0.ABS_BIT_05
#define lcabsu1UnderOutsideFrontCtrl    lcAbsCtrlFlg0.ABS_BIT_04

#if __BRK_SIG_MPS_TO_PEDAL_SEN
#define lcabsu1ValidPedalTravel         lcAbsCtrlFlg0.ABS_BIT_03
#endif
#define lcabsu1ValidYawStrLatG          lcAbsCtrlFlg0.ABS_BIT_02
#define lcabsu1ValidMpress              lcAbsCtrlFlg0.ABS_BIT_01
#define lcabsu1ValidCan                 lcAbsCtrlFlg0.ABS_BIT_00

#if __ABS_RBC_COOPERATION
#define lcabsu1FastPressRecoveryBfAbs   lcAbsCtrlFlg1.ABS_BIT_07
#define lcabsu11stFastPressRecovery     lcAbsCtrlFlg1.ABS_BIT_06
#endif

#if __SLIGHT_BRAKING_COMPENSATION
#define lcabsu1SlightBraking            lcAbsCtrlFlg1.ABS_BIT_05
#define lcabsu1VerySlightBraking        lcAbsCtrlFlg1.ABS_BIT_04
#endif

#define lcabsu1MaintainABSatBlsStart    lcAbsCtrlFlg1.ABS_BIT_03
#define lcabsu1ValidBLS					lcAbsCtrlFlg1.ABS_BIT_02
#define	lcabsu1FaultBLS					lcAbsCtrlFlg1.ABS_BIT_01
#define	lcabsu1BrakeByBLS				lcAbsCtrlFlg1.ABS_BIT_00

#define lcabsu1ReAbsByMpresDuringHDC    lcAbsCtrlFlg2.ABS_BIT_07
#define lcabsu1InAbsAllWheelEntFadeOut  lcAbsCtrlFlg2.ABS_BIT_06
#define lcabsu1EbdInhibitCondition      lcAbsCtrlFlg2.ABS_BIT_04

#define	lcabsu1ValidBrakeSensor			lcAbsCtrlFlg2.ABS_BIT_03
#define	lcabsu1BrakeSensorOn            lcAbsCtrlFlg2.ABS_BIT_02
#if __ABS_RBC_COOPERATION
#define lcabsu1FrontWheelSlip	        lcAbsCtrlFlg2.ABS_BIT_01
#endif
#define Not_used_ABS_Ctrl_flag_20       lcAbsCtrlFlg2.ABS_BIT_00
#if (__ABS_DESIRED_BOOST_TARGET_P == ENABLE)
#define lcabsu1InABSDesiredBoostTarPFlg lcAbsCtrlFlg3.ABS_BIT_00
#endif
typedef struct
{
	int16_t lcabss16WhlTarPress;
	int16_t lcabss16WhlTarPRate;
	int16_t lcabss16WhlTarDP;
	int16_t lcabss16WhlBaseTarDP;
	int16_t lcabss16InitTotalDeltaP;
	int16_t lcabss16InABSInitRiseDPRefEstP;
	int16_t lcabss16InABSInitRiseDPRefDump; /*Mon*/
	int16_t lcabss16WhlAccumulatedTarDP;
	int16_t lcabss16WhlInABSDesiredNthTarDP;
	int16_t lcabss16WhlCompTarDP;
	int16_t lcabss16WhlCyclicCompDP;
	int16_t lcabss16WhlSpecialCompDP; /*Mon*/
	int16_t lcabss16WhlMPCyclicCompDP; /*Mon*/
	int16_t lcabss16InABSNthPressError;
	int16_t lcabss16WhlDesriedTarDP;
	int16_t lcabsu16InABSNthRiseCnt;


	uint32_t lcabsu1InABSNthReduceHoldTimReq  : 1;
	uint32_t lcabsu1InABSInitialRiseDone	  : 1;
	uint32_t lcabsu1CmdPriority : 1;
}Whl_TP_t;



/*Global Extern Variable  Declaration*****************************************/
extern Whl_TP_t FL_WP, FR_WP, RL_WP, RR_WP, *WL_WP;
extern ABS_FLAG_t   lcAbsBrakingFlg, lcAbsBrakingFlg2, lcAbsCtrlFlg0, lcAbsCtrlFlg1, lcAbsCtrlFlg2, lcAbsCtrlFlg3, lcAbsValidityFlg0;
extern uint8_t abs_begin_timer;
extern uint8_t gma_flags;
extern uint8_t YMR_check_counter;
extern int16_t lcabss16PreAbsMpressSlope;  
extern int16_t lcabss16MpressSlope, lcabss16MpressSlopePeak;
#if (__IDB_LOGIC==ENABLE)
extern int16_t lcabss16RefMpress, lcabss16RefMpressOld, lcabss16RefCircuitPress, lcabss16RefActiveTargetPress, lcabss16RefFinalTargetPres;
#else
extern int16_t lcabss16RefMpress, lcabss16RefCircuitPress, lcabss16RefMpressOld;
#endif
#if (__ABS_DESIRED_BOOST_TARGET_P == ENABLE)
extern int16_t lcabss16InABSDesiredBoostTarP;
extern int16_t lcabss16EstFrontWhlSkidPress;
extern int16_t lcabss16EstRearWhlSkidPress;
extern int16_t lcabss16TotalEstWhlSkidPress;
#endif
#if __ECU==ESP_ECU_1
 
#if __SPLIT_STABILITY_IMPROVE_GMA
extern uint8_t lcabsu8DelaYawExcessCnt;
extern uint8_t lcabsu8DelaYawStableCnt; 
extern uint8_t GMA_STATE;
extern int16_t Yaw_Gain, Yaw_Dot_Gain, lcabss16TargetDeltaYaw, lcabss16TargetDeltaYaw2;
extern int16_t lcabss16YDFSplitLHcnt, lcabss16YDFSplitRHcnt;
#endif
#if __ABS_RBC_COOPERATION
extern int16_t lcabss16RBCPressBfABS;
#endif	/* __ABS_RBC_COOPERATION */
  

#endif


  #if defined (__STAB_DCT_LOGIC_DEBUG)
extern int16_t  AradLowerThresFL,AradLowerThresFR                   ;
extern int16_t  AradHigherThresFL,AradHigherThresFR                 ;
extern int16_t  StabDctdVThresFL,StabDctdVThresFR                   ;
extern int16_t  StabDctSlipThresFL,StabDctSlipThresFR               ;
extern int16_t  BendDiffCompensatedTempFL, BendDiffCompensatedTempFR;
extern int16_t  BendSlipCompensatedTempFL, BendSlipCompensatedTempFR;
  #endif /*defined (__STAB_DCT_LOGIC_DEBUG)*/


/*Global Extern Functions  Declaration****************************************/
extern void LCABS_vSetWheelDumpThreshold(void);
extern void LCABS_vCallYawMomentReduction(void);
extern void LCABS_vSetLFCHoldTimer(void);
extern void LCABS_vDecideWheelCtrlState(void);
extern void LCABS_vResetABS(void);
extern int16_t LCABS_s16Interpolation2P(int16_t input,int16_t mu_1,int16_t mu_2,int16_t gain_1,int16_t gain_2);

#if __ABS_RBC_COOPERATION
extern void LCABS_vDecidePressRecoveryBfAbs(void);
#endif	/* __ABS_RBC_COOPERATION */

#if (__SLIGHT_BRAKING_COMPENSATION)
extern void LCABS_vDecideBrakingStatus(void);
#endif
#if __VDC
extern void LCABS_vCheckMpressSlope(void);
#endif

extern void LCABS_vDecideWhlTargetP(void);
extern void LCABS_vDecideCtrlActuationVars(void);
/*
  #if (__IDB_LOGIC==ENABLE)
extern	void LCABS_vDcdStrkRecvrCtrl(void);
  #endif
*/

extern int16_t LCABS_s16GetParamFromRef(int16_t input, int16_t Reference1, int16_t Reference2, int16_t Reference3, int16_t OutParam1, int16_t OutParam2, int16_t OutParam3, int16_t OutParamMax);

  #if (L_CONTROL_PERIOD>=2)
extern uint8_t LCABS_u8CountDumpNum(uint8_t u8DumpCounter, uint8_t s8DumpCase);
extern uint8_t LCABS_u8DecountStableDumpNum(uint8_t u8StableDumpCounter);
  #else
#define LCABS_u8CountDumpNum(x)             (((x) < U8_TYPE_MAXNUM)? ((x)+1) : U8_TYPE_MAXNUM)
#define LCABS_u8DecountStableDumpNum(x)     (((x)>0) ? ((x)-1) : 0)
  #endif

#endif
