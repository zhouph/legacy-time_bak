/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCEPBCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCEPBCallControl.h"
#include "LCAVHCallControl.h"
#include "hm_logic_var.h"
#include "Hardware_Control.h"
  #if __EPB_INTERFACE
#include "LCDECCallControl.h"
   #if !SIM_MATLAB
#include "../Failsafe/FEErrorProcessing.h"
#include "../Failsafe/ESC/FPMcpBlsHWCheck.h"
#include "../Failsafe/F_VariableLinkSet.h"
//#include "../System/WChkVariantCode.h"
#include "../System/WContlVariantCode.h"  /*jyh*/
#include "../Failsafe/FLLampRequest.h"
#include "../Failsafe/FVSolPsvCheck.h"

    #if __CAR==FORD_C214
#include "../CAN/CAN_DRV/CCDrvFORDCan.h"
    /*#elif __CAR==GM_DAT_MAGNUS
#include "../CAN/CAN_DRV/CCDrvGMDWCan.h"*/
    #endif
   #endif
  #endif

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if (__EPB_INTERFACE) || (__AVH)

 
uint8_t       lcu8EpbCtrlReq, lcu8EpbFailureDar;
uint16_t        clu16EpbObserver;

int8_t        lcs8EpbState;
int16_t       lcs16EpbTargetG;

uint8_t 		lcu8epbInfoLamp;
uint8_t 		lcu8epbFailureLamp;
uint8_t 		lcu8epbAudioOutput;
uint8_t 		lcu8epbOutputCluster;
uint8_t 		lcu8epbStatusCtrlSW;
uint8_t 		lcu8epbStatusEPB;
uint8_t 		lcu8epbStatusError;
uint8_t 		lcu8epbReqDecel;
uint8_t         lcu8epbDynamicReq;
uint8_t         lcu8epbFail;
uint8_t         lcu8epbLdmstate;
uint8_t       lcu8EpbiEpbState;
int16_t			lcs16epbActFroce;
uint8_t       lcu8EpbiDiableDitherCnt;
int16_t      laepbs16TcTargetPress;

 

struct  	U8_BIT_STRUCT_t EPB00;
struct  	U8_BIT_STRUCT_t EPB01;

  #endif

/* Local Function prototype ****************************************/
  #if __EPB_INTERFACE
static void LCEPB_vChkInhibition(void);
static void LCEPB_vChkEpbRequest(void);
/*static*/ void LCEPB_vResetCtrlVariable(void);
static void LCEPB_vSetDecelCtrl(uint8_t,uint8_t,uint8_t,int16_t);
static void	LCEPB_vCheckEpbState(void);
static void	LCEPB_vCheckSpdForSoftStop(void);
static void LCEPB_vChkEpbiBrkLampOnReq(void);
static void LCEPB_vCalcTargetPress(void);
  #endif

/* Implementation***************************************************/
  #if __EPB_INTERFACE
void LCEPB_vCallControl(void)
{
    LCEPB_vChkInhibition();
    LCEPB_vCheckEpbState();
    LCEPB_vChkEpbRequest();
    switch(lcs8EpbState)
    {
        case S8_EPB_STANDBY:
            lcu1EpbActiveFlg = 0;
            if((lcu8EpbCtrlReq==0)||(lcu1EpbInhibitFlg==1))
            {
                lcs8EpbState=S8_EPB_TERMINATION;
                LCEPB_vResetCtrlVariable();
                clu16EpbObserver=4;
            }
            else if(lcs16EpbTargetG<0)
            {
                lcu1EpbActiveFlg=1;
                lcs8EpbState=S8_EPB_ACTIVATION;
                  #if (__TCMF_CONTROL) && (__EPB_FF_Ctrl_Enable == 0)
                LCEPB_vSetDecelCtrl(1,0,1,(lcs16EpbTargetG*10));
                  #else
                LCEPB_vSetDecelCtrl(1,1,1,(lcs16EpbTargetG*10));
                  #endif
                clu16EpbObserver=5;
            }
            else
            {
                lcs8EpbState=S8_EPB_STANDBY;
                clu16EpbObserver=6;
            }
            break;

        case S8_EPB_ACTIVATION:
            lcu1EpbActiveFlg=1;
            if((lcu8EpbCtrlReq==0)||(lcs16EpbTargetG>=0)||(lcu1EpbInhibitFlg==1))
            {
                if(lcu1DecSmoothClosingEndFlg==1)
                {
                    lcu1EpbActiveFlg=0;
                    lcs8EpbState=S8_EPB_TERMINATION;
                    LCEPB_vSetDecelCtrl(0,0,0,0);
                    clu16EpbObserver=7;
                }
                else
                {
                    lcu1EpbActiveFlg=1;
                    lcs8EpbState=S8_EPB_ACTIVATION;
                    LCEPB_vSetDecelCtrl(0,0,1,0);
                    clu16EpbObserver=8;
                }
            }
            else
            {
                lcs8EpbState=S8_EPB_ACTIVATION;
                  #if (__TCMF_CONTROL) && (__EPB_FF_Ctrl_Enable == 0)
                LCEPB_vSetDecelCtrl(1,0,1,(lcs16EpbTargetG*10));
                  #else
                if((vref<S16_EPBI_FORCED_HOLD_SPD)||(lcu8EpbiEpbState==EPBI_EPB_CLAMPING))
                {
                	LCEPB_vSetDecelCtrl(1,0,1,(lcs16EpbTargetG*10));
                	clu16EpbObserver=9;
                }
                else
                {                  
                    LCEPB_vSetDecelCtrl(1,1,1,(lcs16EpbTargetG*10));
                    clu16EpbObserver=10;
                }
                  #endif

            }
            break;

        case S8_EPB_TERMINATION:
            lcu1EpbActiveFlg = 0;
            if((lcu8EpbCtrlReq>0)&&(lcu1EpbInhibitFlg==0))
            {
                lcs8EpbState=S8_EPB_STANDBY;
                LCEPB_vResetCtrlVariable();
                clu16EpbObserver=2;
            }
            else
            {
                lcs8EpbState=S8_EPB_TERMINATION;
                LCEPB_vResetCtrlVariable();
                clu16EpbObserver=3;
            }
            break;

        default:
            lcu1EpbActiveFlg=0;
            lcs8EpbState=S8_EPB_TERMINATION;
            clu16EpbObserver=1;
            break;
    }
    LCEPB_vChkEpbiBrkLampOnReq();
    LCAVH_vTxLdmState();
    LCEPB_vCalcTargetPress();
}


static void LCEPB_vChkInhibition(void)
{

	#if __GM_FailM
	if ((fu1OnDiag==1)
	    ||(init_end_flg==0)
	    ||((wu8IgnStat==CE_OFF_STATE)&&(U8_EPBI_ACT_ENABLE_IGN_OFF==DISABLE))
	    ||(ebd_error_flg==1)
	    ||(fu1ESCEcuHWErrDet==1)																			/* ESC HW	*/
		||(fu1MCPErrorDet==1)																			/* MP		*/
		||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)	/* Wheel	*/
		||(fu1MainCanLineErrDet==1)																		/* GMLAN (Vehicle)	*/
																										/* GMLAN (ESC Sensor)	*/
      #if __BRAKE_FLUID_LEVEL
		||(fu1DelayedBrakeFluidLevel==1)
        ||(fu1LatchedBrakeFluidLevel==1)	/* 091030 for Bench Test */
      #endif
	  #if (EPB_VARIANT_ENABLE==ENABLE)
	    ||(wu8EpbVariantCodingEndFlag==0)
	  #endif
	    ||(flu1BrakeLampErrDet==1)
	    ||(EPB1_timeout_err_flg==0)  /* EPB msg timeout */
		)
  	#else
	if((fu1OnDiag==1)||(init_end_flg==0)
		||((wu8IgnStat==CE_OFF_STATE)&&(U8_EPBI_ACT_ENABLE_IGN_OFF==DISABLE))
    ||(abs_error_flg==1)||(mp_hw_suspcs_flg==1)||(can_error_flg==1)||(vdc_error_flg==1)
	  #if (EPB_VARIANT_ENABLE==ENABLE)
	  ||(wu8EpbVariantCodingEndFlag==0)
	  #endif
	  ||(flu1BrakeLampErrDet==1)  
	  ||(EPB1_timeout_err_flg==0)  /* EPB msg timeout */         	
	)
  	#endif
    {
        lcu1EpbInhibitFlg=1;
        LCEPB_vResetCtrlVariable();
    }
    else
    {
        lcu1EpbInhibitFlg=0;
    }
}

static void LCEPB_vChkEpbRequest(void)
{
	lcu8EpbCtrlReq=lcu8epbDynamicReq;

	if(lcu8EpbCtrlReq==1)
	{
        lcs16EpbTargetG = S16_EPBI_TARGET_DECEL;
   	}
   	else
   	{
   		lcs16EpbTargetG = 0;
   	}


    if(lcu1EpbiDitherDisable==0)
    {
        if((lcu1EpbActiveFlg==1)&&(lcu8EpbCtrlReq==0))
        {
        	lcu1EpbiDitherDisable = 1;
        	lcu8EpbiDiableDitherCnt = lcu8EpbiDiableDitherCnt + 1;
        }
        else 
        {
        	lcu8EpbiDiableDitherCnt = 0;
        }
    }
    else
    {
    	if(lcu8EpbiDiableDitherCnt<L_U8_TIME_10MSLOOP_1500MS)
    	{
    		lcu8EpbiDiableDitherCnt = lcu8EpbiDiableDitherCnt + 1;
    	}
    	else { }
    	
    	if(((lcu1EpbActiveFlg==1)&&(lcu8EpbCtrlReq==1))  /*re enter*/
    	  ||((MFC_PWM_DUTY_S<1)&&(MFC_PWM_DUTY_P<1))     /*TC off */
    	  ||(lcu8EpbiDiableDitherCnt>=185))              /*max 1.3sec*/
    	{
    		lcu1EpbiDitherDisable = 0;
    		lcu8EpbiDiableDitherCnt = 0;
    	}
    	else { }
    }

        	

    #if __EPB_TEST_MODE_ENABLE
      if((vref>VREF_1_KPH)&&(fu1ESCDisabledBySW==1))
      {
          lcu8EpbCtrlReq = 1;
          lcs16EpbTargetG = S16_EPBI_TARGET_DECEL;
      }
      else 
      {
          lcu8EpbCtrlReq = 0;
          lcs16EpbTargetG = 0;
      }
    #endif

}

static void LCEPB_vChkEpbiBrkLampOnReq(void)
{
    if(lcu1EpbActiveFlg==1)
    {
   		lcu1EpbiBrkLampOnReq = 1;
    }
    else
    {
    	lcu1EpbiBrkLampOnReq = 0;
    }	
}

/*static*/ void LCEPB_vResetCtrlVariable(void)
{
    LCEPB_vSetDecelCtrl(0,0,0,0);
    lcu1EpbActiveFlg = 0;
    lcu1EpbiBrkLampOnReq = 0;
    lcs16EpbTargetG = 0;
}


static void LCEPB_vSetDecelCtrl(uint8_t lcu8EpbDecReq,uint8_t lcu8EpbDecFfReq,uint8_t lcu8EpbDecClosingReq,int16_t lcs16EpbDecTargetG)
{
    clDecEpb.lcu8DecCtrlReq=lcu8EpbDecReq;
    clDecEpb.lcu8DecFeedForwardReq=lcu8EpbDecFfReq;
    clDecEpb.lcu8DecSmoothClosingReq=lcu8EpbDecClosingReq;
    clDecEpb.lcs16DecReqTargetG=lcs16EpbDecTargetG;
}

static void	LCEPB_vCheckEpbState(void)
{
    /* Check EPB Error */
    if (fu1EPBErrorDet==1)
    {
    	lcu8EpbFailureDar=1;
    }
    else
    {
    	lcu8EpbFailureDar=0;
    }	
  
	/* Check EPB State */
    if(lcu8epbStatusEPB==EPBI_EPB_RELEASED_STATE)
    {
    	lcu8EpbiEpbState = EPBI_EPB_RELEASED;
    }
    else if(lcu8epbStatusEPB==EPBI_EPB_CLAMPED_STATE)
    {
    	lcu8EpbiEpbState = EPBI_EPB_CLAMPED;
    }
    else if(lcu8epbStatusEPB==EPBI_EPB_RELEASING_STATE)
    {
    	lcu8EpbiEpbState = EPBI_EPB_RELEASING;
    }    
    else if(lcu8epbStatusEPB==EPBI_EPB_CLAMPING_STATE)
    {
    	lcu8EpbiEpbState = EPBI_EPB_CLAMPING;
    }
    else if(lcu8epbStatusEPB==EPBI_EPB_DYNAMIC_BRK_STATE)
    {
    	lcu8EpbiEpbState = EPBI_EPB_DYNAMIC_BRK;
    }        
    else
    {
    	lcu8EpbiEpbState = EPBI_EPB_UKNOWN;
    }
    
    #if __EPB_TEST_MODE_ENABLE
      lcu8EpbFailureDar=0;
      lcu8EpbiEpbState = EPBI_EPB_RELEASED;
    #endif     
}

static void LCEPB_vCalcTargetPress(void)
{
	tempW1 = 0;
	if(lcu1EpbActiveFlg==1)
	{
		tempW1 = MFC_Current_DEC_S - 150;   /*  0bar@150mA */
	}
	else
	{
		if(laepbs16TcTargetPress > MPRESS_0BAR)
		{
			tempW1 = laepbs16TcTargetPress - MPRESS_0G5BAR;
		}
		else
		{
			 tempW1 = MPRESS_0BAR;
		}
	}

	if(tempW1 < MPRESS_0BAR)
	{
		tempW1 = MPRESS_0BAR;
	}
	else { }

	laepbs16TcTargetPress = tempW1;
}

#endif  /* EPBi */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCEPBCallControl
	#include "Mdyn_autosar.h"
#endif