#ifndef __LCEBDCALLCONTROL_H__
#define __LCEBDCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

/*Global Type Declaration ****************************************************/
#define lcebdu1DoubleBrake			EBDCTR01.bit7
#define lcebdu1RoughSlightBrk   	EBDCTR01.bit6
#define lcebdu1BaseCtrlWL       	EBDCTR01.bit5
#define lcebdu1EntrySlipSatisfied   EBDCTR01.bit4
#define lcebdu1KneePointSatisfied   EBDCTR01.bit3
#define lcebdu1Decel_rl 			EBDCTR01.bit2
#define lcebdu1Slip_rl  			EBDCTR01.bit1
#define lcebdu1Arad_rl  			EBDCTR01.bit0

#define lcebdu1Decel_rr 			EBDCTR02.bit7
#define lcebdu1Slip_rr  			EBDCTR02.bit6
#define lcebdu1Arad_rr  			EBDCTR02.bit5
#define EBD_Not_Used_FLAG_24		EBDCTR02.bit4
#define EBD_Not_Used_FLAG_23		EBDCTR02.bit3
#define EBD_Not_Used_FLAG_22		EBDCTR02.bit2
#define EBD_Not_Used_FLAG_21		EBDCTR02.bit1
#define EBD_Not_Used_FLAG_20 		EBDCTR02.bit0

/* ------------ Logic Tuning -------------------*/

#define LFC_RISE_TIME_AT_EBD_END L_TIME_5S

#if __L_CYCLETIME == LOOP_10MS
    #define U8_EBD_PULSEUP_HOLD_TIME_DEF    L_TIME_50MS /* Default EBD pulse-up hold time */
    #define U8_EBD_PULSEUP_HOLD_TIME_MIN    L_TIME_10MS
#else
    #define U8_EBD_PULSEUP_HOLD_TIME_DEF    L_TIME_55MS /* Default EBD pulse-up hold time */
    #define U8_EBD_PULSEUP_HOLD_TIME_MIN    L_TIME_5MS  /* EBD pulse-up hold time */
#endif

  #if (__CAR==HMC_EA_EV)

     /* ---- EBD MI parameters ---- */
    #define S16_EBD_MI_ALLOW_SPEED      VREF_20_KPH
    #define S16_EBD_MI_DUMP_ALLOW_SPEED VREF_30_KPH
    #define S16_EBD_MI_DUMP_MAX_SPEED   VREF_80_KPH
    
    #define EBD_MI_DUMP_MIN             3
    #define EBD_MI_DUMP_MAX             8
    
     /* ---- EBD Decel Improve parameters ---- */
    #define U8_EBD_INC_PULSEUP_RATE_START   5
    #define U8_EBD_INC_PULSEUP_RATE_END     10

  #elif (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)

     /* ---- EBD MI parameters ---- */
    #define S16_EBD_MI_ALLOW_SPEED      VREF_20_KPH
    #define S16_EBD_MI_DUMP_ALLOW_SPEED VREF_30_KPH
    #define S16_EBD_MI_DUMP_MAX_SPEED   VREF_80_KPH
    
    #define EBD_MI_DUMP_MIN             3
    #define EBD_MI_DUMP_MAX             8
    
     /* ---- EBD Decel Improve parameters ---- */
    #define U8_EBD_INC_PULSEUP_RATE_START   5
    #define U8_EBD_INC_PULSEUP_RATE_END     10

  #else /*(__CAR == default)*/

     /* ---- EBD MI parameters ---- */
    #define S16_EBD_MI_ALLOW_SPEED      VREF_30_KPH
    #define S16_EBD_MI_DUMP_ALLOW_SPEED VREF_40_KPH
    #define S16_EBD_MI_DUMP_MAX_SPEED   VREF_120_KPH
    
    #define EBD_MI_DUMP_MIN             1
    #define EBD_MI_DUMP_MAX             4
    
     /* ---- EBD Decel Improve parameters ---- */
    #define U8_EBD_INC_PULSEUP_RATE_START   5
    #define U8_EBD_INC_PULSEUP_RATE_END     10

  #endif /*(__CAR == default)*/

/* #if (__BLS_NO_WLAMP_FM==ENABLE)) */
	#define BRK_ON_PRESS_NO_OFFSET_COR		MPRESS_15BAR	
	
	#define SLIP_COMP_H_THRES_SPEED1 		VREF_100_KPH
	#define	HIGH_COMP_SLIP_THRES1			LAM_10P
	#define LOW_COMP_SLIP_THRES1			LAM_3P	
	
	#define SLIP_COMP_H_THRES_SPEED2		VREF_100_KPH
	#define HIGH_COMP_SLIP_THRES2			LAM_15P
	#define LOW_COMP_SLIP_THRES2			LAM_5P	
/* #endif */

#if __SLIGHT_BRAKING_COMPENSATION
#define U8_PULSE_UP_LIMIT_CNT_LV1		10
#define U8_PULSE_UP_LIMIT_CNT_LV2		15
#define S16_PULSE_UP_LIMIT_PRESS_DIFF	MPRESS_5BAR
#endif

/* ------------- Logic Tuning End --------------*/

#define MI_LEVEL1   1
#define MI_LEVEL2   2
#define MI_LEVEL3   3

#if __MGH60_ABS_CONCEPT_RELEASED

  #if __EBD_MI_ENABLE
    #define     __EBD_MI_DUMP_ENABLE            1
  #else
    #define     __EBD_MI_DUMP_ENABLE            0
  #endif

#elif __MGH60_ABS_IMPROVE_CONCEPT

  #if __EBD_MI_ENABLE
    #define     __EBD_MI_DUMP_ENABLE            1
  #else
    #define     __EBD_MI_DUMP_ENABLE            0
  #endif

#else

    #define     __EBD_MI_DUMP_ENABLE            0
#endif

#define __REORGANIZE_EBD_ENTRY_DECISION     ENABLE

/*Global Extern Variable  Declaration*****************************************/
extern uint8_t ebd_motor_drv_time;

/*Global Extern Functions  Declaration****************************************/
extern void LCEBD_vDecideEBDCtrlState(void);
extern void LCEBD_vCallControl(void);
extern U8_BIT_STRUCT_t EBDCTR01, EBDCTR02; 

#endif
