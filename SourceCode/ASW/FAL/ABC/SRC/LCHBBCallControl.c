/******************************************************************************
* Project Name: Hydraulic Brake Boost
* File: LCHBBCallControl.C
* Description: HBB control algorithm
* Date: October. 13. 2006
******************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCHBBCallControl
	#include "Mdyn_autosar.h"
#endif

/* Includes ******************************************************************/
#include "LCHBBCallControl.h"
#include "LCFBCCallControl.h"
#include "LCHSACallControl.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCEPBCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "Hardware_Control.h"
#include "LSESPCalSensorOffset.h"
#include "LCAVHCallControl.h"


#if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
#endif

#if defined(__CAN_SW)
    #if __Vacuum_Sen_CBox_AD
extern UINT Vacuum_mon_ad;
    #endif
#endif

  #if   __TCMF_LowVacuumBoost
/* Local Definiton  **********************************************************/

#define __HBB_MSC_DEBUG     1

/* Variables Definition*******************************************************/
struct  U8_BIT_STRUCT_t HBB01, HBB02, HBB03;

uint8_t		TCMF_LowVacuumBoost_cnt;
int16_t     HBB_release_cnt,HBB_release_mpress_cnt, HBB_motor_off_cnt, HBB_motor_delay_cnt, HBB_OFF_cnt, HBB_ON_cnt;
int16_t     assist_pressure_inc_cnt;
int16_t     WHEEL_PRESS_AVG;
int8_t    	lcs8HbbOtherCtrlEnable;
uint8_t   	lcu8HbbPedalTravelAvgCnt, lcu8HbbPedalTravelCnt, lcu8HbbAssistPressGain_Vel;
int16_t    	lcs16HbbMscTargetVolFactor, lcs16HbbKneePointRatio;
int16_t     lcs16HbbPedalTravelRate, lcs16HbbVacuumLevel; 
int16_t     lcs16HbbPedalTravelAvg, lcs16HbbPedalTravelBuff10[5], lcs16HbbPedalTravelBuff[3], lcs16HbbPedalTravelSum ;
int16_t 	lcs16HbbAssistPressureMotor,lcs16HbbAssistPressureOldMotor, lcs16HbbAssistPressureRateMotor;
int16_t 	lcs16HbbAssistPressureTC,lcs16HbbAssistPressureOldTC, lcs16HbbAssistPressureRateTC, lcs16HbbAssistPressTC2;
int16_t     lcs16HbbTargetPressure, lcs16HbbTargetPressureOld, lcs16HbbTargetPressureTemp, lcs16HbbTargetPressureComp;
int16_t     lcs16HbbMCPressureAvg, lcs16HbbMCPressureSum, lcu8HbbMCPressureCnt, lcs16HbbMCPressureBuff[10], lcs16HbbMCPressureAvgOld;
int16_t		lcs16HbbKneePointEst,lcs16HbbKneePointEstOld;
uint8_t   	lcu8HbbNeedMotorOffCnt;
int16_t     hbb_msc_target_vol;
int16_t    lcs16HbbVacuumPres, lcs16HbbPedalTravel;

/* LocalFunction prototype ***************************************************/
static void LCHBB_vInterfaceOtherSystems(void);
static void LCHBB_vCheckInhibitCondition(void);
static void LCHBB_vCalcBrakePedalTravelRate(void);
static void LCHBB_vEstTargetPressByBPTravel(void);
static void LCHBB_vCalcAvgMCPressure(void);
static void LCHBB_vDecideAssistPressure(void);
static void LCHBB_vDecideBrakeBoost(void);
static void LCHBB_vCalcSensorSignal(void);
void LCHBB_vCallControl(void);
void LCMSC_vSetHBBTargetVoltage(void);

/* GlobalFunction prototype **************************************************/

/* Implementation*************************************************************/

/******************************************************************************
* FUNCTION NAME:      LCHBB_vInterfaceOtherSystems
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Interface With Other Systems
******************************************************************************/

static void LCHBB_vInterfaceOtherSystems(void)
{
    lcs8HbbOtherCtrlEnable = 0;
    
    if(PBA_ON==1) 
    {
        lcs8HbbOtherCtrlEnable = 1;
    }
  #if __FBC
    if(FBC_ON==1) 
    {
        lcs8HbbOtherCtrlEnable = 2;
    }
  #endif /*__FBC*/
  #if __TSP
    if(TSP_ON==1) 
    {
        lcs8HbbOtherCtrlEnable = 3;
    }
  #endif /*__TSP*/
}


/******************************************************************************
* FUNCTION NAME:      LCHBB_vCheckInhibitCondition
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Check Inhibition Condition
******************************************************************************/

static void LCHBB_vCheckInhibitCondition(void)
{
    lcu1HbbInhibitFlag = 0;
    
	if((fu1MCPErrorDet==1)                  /* MP error     */
	|| (fu1ESCEcuHWErrDet==1)               /* ECU ERROR    */
	|| (fu1VoltageLowErrDet==1)
	|| (fu1WheelFLErrDet==1)                /* FL WSS error */ 
	|| (fu1WheelFRErrDet==1)                /* FR WSS error */
	|| (fu1WheelRLErrDet==1)                /* RL WSS error */
	|| (fu1WheelRRErrDet==1)                /* RR WSS error */
	|| (fu1OnDiag==1)                       /* DIAG ON      */
	|| (init_end_flg==0)                    /* VV Check END */
	|| (wu8IgnStat==CE_OFF_STATE)           /* IGN OFF      */
	#if __VACUUM_SENSOR_ENABLE==ENABLE
	|| (fu1VacuumErrDet==1)
	#endif
	|| (fu1PedalErrDet==1)
  #if __GM_FailM  
    || (fu1MainCanLineErrDet==1)            /* Main CAN ERR */
    #if (__BRAKE_FLUID_LEVEL==ENABLE)
    ||(fu1DelayedBrakeFluidLevel==1)
	||(fu1LatchedBrakeFluidLevel==1)  	
    #endif
    #if (__CAR_MAKER==GM_KOREA)
    ||(fu8EngineModeStep==0)
    #endif
  #endif
    )
    {
        lcu1HbbInhibitFlag=1;
    }
    else 
    {
        ;
    }
    
}

/******************************************************************************
* FUNCTION NAME:      LCHBB_vCalcBrakePedalTravelRate
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Calculate Brake Pedal Travel rate
******************************************************************************/
static void LCHBB_vCalcBrakePedalTravelRate(void)
{    
    lcs16HbbPedalTravelSum = lcs16HbbPedalTravelSum - lcs16HbbPedalTravelBuff10[lcu8HbbPedalTravelAvgCnt] + lcs16HbbPedalTravel;
    lcs16HbbPedalTravelAvg = lcs16HbbPedalTravelSum/5;           
    
    lcs16HbbPedalTravelBuff10[lcu8HbbPedalTravelAvgCnt] = lcs16HbbPedalTravel;
    lcu8HbbPedalTravelAvgCnt = lcu8HbbPedalTravelAvgCnt + 1;
    
    if(lcu8HbbPedalTravelAvgCnt>=5) 
    {
        lcu8HbbPedalTravelAvgCnt=0;
    }
    else 
    {
    	;
    }

    lcs16HbbPedalTravelRate = LCESP_s16Lpf1Int((lcs16HbbPedalTravelAvg - lcs16HbbPedalTravelBuff[lcu8HbbPedalTravelCnt]), lcs16HbbPedalTravelRate, 39);
    lcs16HbbPedalTravelBuff[lcu8HbbPedalTravelCnt] = lcs16HbbPedalTravelAvg;
    
    lcu8HbbPedalTravelCnt = lcu8HbbPedalTravelCnt + 1;
    
    if(lcu8HbbPedalTravelCnt>=3) 
    {
        lcu8HbbPedalTravelCnt = 0;
    }
    else
    {
        ;
    } 
    
    if (lcs16HbbPedalTravelRate < HbbPedalStateReleaseThres)
    {
    	lcu1HbbPedalState = HbbBrakePedalRelease;    
    }
    else if (lcs16HbbPedalTravelRate > HbbPedalStateApplyThres)
    {
    	lcu1HbbPedalState = HbbBrakePedalApply;
    }
    else
    {
    	;
    }
    
    if((lcu1HbbPedalRateOK==0)
     &&(TCMF_LowVacuumBoost_flag==0)
     &&(lcs16HbbPedalTravelRate>=S16_HBB_ENTER_INIT_PEDAL_RATE)
     &&(vref>=HBB_ENTER_SPEED_TH) 
     &&(MPRESS_BRAKE_ON==1))
    {
    	lcu1HbbPedalRateOK=1;
    }
    else if((TCMF_LowVacuumBoost_flag==1)
     ||(vref<HBB_EXIT_SPEED_TH) 
     ||(MPRESS_BRAKE_ON==0))
    {
    	lcu1HbbPedalRateOK=0;
    }
    else{}
    	  
}
/******************************************************************************
* FUNCTION NAME:      LCHBB_vCalcAvgMCPressure
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Calculate Mpress average during 100ms
******************************************************************************/

static void LCHBB_vCalcAvgMCPressure(void)
{
    if(speed_calc_timer > 10)
    {
    	if(lsespu1MpressOffsetOK==1)
    	{
    		lcs16HbbMCPressureAvgOld = lcs16HbbMCPressureAvg;
        	
        	if(speed_calc_timer<=20)
        	{
        	    lcs16HbbMCPressureSum = lcs16HbbMCPressureBuff[0] + lcs16HbbMCPressureBuff[1] 
        	                    		+ lcs16HbbMCPressureBuff[2] + lcs16HbbMCPressureBuff[3]
        	                    		+ lcs16HbbMCPressureBuff[4] + lcs16HbbMCPressureBuff[5]
        	                    		+ lcs16HbbMCPressureBuff[6] + lcs16HbbMCPressureBuff[7]
        	                    		+ lcs16HbbMCPressureBuff[8] + lcs16HbbMCPressureBuff[9] + mpress;
        	                    		
        	    lcs16HbbMCPressureAvg = lcs16HbbMCPressureSum/(lcu8HbbMCPressureCnt+1);                          
        	}
        	else 
        	{
        	    lcs16HbbMCPressureSum = lcs16HbbMCPressureSum - lcs16HbbMCPressureBuff[lcu8HbbMCPressureCnt] + mpress;
        	    lcs16HbbMCPressureAvg = lcs16HbbMCPressureSum/10;           
        	}
        	
        	lcs16HbbMCPressureBuff[lcu8HbbMCPressureCnt] = mpress;
        	lcu8HbbMCPressureCnt = lcu8HbbMCPressureCnt + 1;
        	if(lcu8HbbMCPressureCnt>=10) 
        	{
        	    lcu8HbbMCPressureCnt=0;
        	}
        	else
        	{
        	    ;
        	}
    	}
    	else
    	{
    		lcs16HbbMCPressureAvg=0;
    	    lcs16HbbMCPressureSum=0;
    	    lcu8HbbMCPressureCnt=0;
    	    lcs16HbbMCPressureAvgOld=0;
    	    lcs16HbbMCPressureBuff[0]=0;
    	    lcs16HbbMCPressureBuff[1]=0;
    	    lcs16HbbMCPressureBuff[2]=0;
    	    lcs16HbbMCPressureBuff[3]=0;
    	    lcs16HbbMCPressureBuff[4]=0;
    	    lcs16HbbMCPressureBuff[5]=0;
    	    lcs16HbbMCPressureBuff[6]=0;
    	    lcs16HbbMCPressureBuff[7]=0;
    	    lcs16HbbMCPressureBuff[8]=0;
    	    lcs16HbbMCPressureBuff[9]=0;
    	}
	}
    else
    {
        lcs16HbbMCPressureAvg=0;
        lcs16HbbMCPressureSum=0;
        lcu8HbbMCPressureCnt=0;
        lcs16HbbMCPressureAvgOld=0;
        lcs16HbbMCPressureBuff[0]=0;
        lcs16HbbMCPressureBuff[1]=0;
        lcs16HbbMCPressureBuff[2]=0;
        lcs16HbbMCPressureBuff[3]=0;
        lcs16HbbMCPressureBuff[4]=0;
        lcs16HbbMCPressureBuff[5]=0;
        lcs16HbbMCPressureBuff[6]=0;
        lcs16HbbMCPressureBuff[7]=0;
        lcs16HbbMCPressureBuff[8]=0;
        lcs16HbbMCPressureBuff[9]=0;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHBB_vEstTargetPressByBPTravel
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Estimate Driver's Target MPress By Brake Pedal Travel
******************************************************************************/
static void LCHBB_vEstTargetPressByBPTravel(void)
{
    lcs16HbbTargetPressureTemp = LCESP_s16IInter4Point (lcs16HbbPedalTravel,
    												 HbbPedalTravel1, HbbTargetPressure1,
    												 HbbPedalTravel2, HbbTargetPressure2,
    												 HbbPedalTravel3, HbbTargetPressure3,
    												 HbbPedalTravel4, HbbTargetPressure4);
    												
    lcs16HbbTargetPressureOld = lcs16HbbTargetPressure;
    												
    if (lcu1HbbPedalState == HbbBrakePedalApply)
    {
    	lcs16HbbTargetPressure = lcs16HbbTargetPressureTemp;
    }
    else
    {
    	lcs16HbbTargetPressure = lcs16HbbTargetPressureTemp - MPRESS_10BAR;
    }

    lcs16HbbTargetPressure = LCESP_s16Lpf1Int(lcs16HbbTargetPressure,lcs16HbbTargetPressureOld, 39); 
    
    if (lcs16HbbTargetPressure < MPRESS_0BAR)
    {
    	lcs16HbbTargetPressure = 0;
    }
    else if (lcs16HbbTargetPressure > MPRESS_180BAR)
    {
    	lcs16HbbTargetPressure = MPRESS_180BAR;
    }
    else
    {
    	;
    }
    
    if (lcs16HbbPedalTravelRate > Pedal_Travel_Rate_UPPER_LIMIT)
    {
    	lcs16HbbTargetPressureComp = mpress;
    }
    else if (lcs16HbbPedalTravelRate >= Pedal_Travel_Rate_0)
    {
    	lcs16HbbTargetPressureComp = (int16_t)(((int32_t)lcs16HbbTargetPressure * (Pedal_Travel_Rate_UPPER_LIMIT - lcs16HbbPedalTravelRate) 
    								+ (int32_t)mpress * lcs16HbbPedalTravelRate)/Pedal_Travel_Rate_UPPER_LIMIT);
    }
    else
    {
    	lcs16HbbTargetPressureComp = lcs16HbbTargetPressure;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHBB_vDecideAssistPressure
* CALLED BY:          LCHBB_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide HBB Assist Pressure
******************************************************************************/
static void LCHBB_vDecideAssistPressure(void)
{
													 
	
    lcu8HbbAssistPressGain_Vel = (uchar8_t)LCESP_s16IInter3Point(vref, S16_HBB_VEHICLE_VELOCITY1, (int16_t)U8_HBB_ASSIST_PRESS_VEL_GAIN1,
    															       S16_HBB_VEHICLE_VELOCITY2, (int16_t)U8_HBB_ASSIST_PRESS_VEL_GAIN2,
    																   S16_HBB_VEHICLE_VELOCITY3, (int16_t)U8_HBB_ASSIST_PRESS_VEL_GAIN3);
/*    if((lcs16HbbVacuumPres < lcs16HbbVacuumLevel)
   	 ||((lcs16HbbVacuumPres >= lcs16HbbVacuumLevel)&&(TCMF_LowVacuumBoost_flag==1)))
    {*/
        lcs16HbbAssistPressureOldTC = lcs16HbbAssistPressureTC;                       
        lcs16HbbAssistPressureTC = ((lcs16HbbTargetPressure - mpress)*(int16_t)lcu8HbbAssistPressGain_Vel/10);
        lcs16HbbAssistPressureTC = LCESP_s16Lpf1Int(lcs16HbbAssistPressureTC, lcs16HbbAssistPressureOldTC, 28);
    
    	if (lcs16HbbAssistPressureTC < 0)
    	{
    		lcs16HbbAssistPressureTC = 0;
    	}
    	else
    	{
    		;
    	}
    	
    	lcs16HbbAssistPressTC2 = lcs16HbbAssistPressureTC;
    	    	
    	if (lcs16HbbAssistPressTC2 <= MPRESS_1BAR)
    	{
    		lcs16HbbAssistPressTC2 = MPRESS_1BAR;
    	}
    	else if (lcs16HbbAssistPressTC2 <= S16_HBB_ASSIST_PRES_COMP_MIN)
    	{
    		lcs16HbbAssistPressTC2 = lcs16HbbAssistPressTC2;
    	}
    	else
    	{
    		lcs16HbbAssistPressTC2 = S16_HBB_ASSIST_PRES_COMP_MIN + (lcs16HbbAssistPressTC2-S16_HBB_ASSIST_PRES_COMP_MIN)*S16_HBB_ASSIST_PRES_COMP_GAIN/10;
    		
    		if (lcs16HbbAssistPressTC2 >= S16_HBB_ASSIST_PRES_MAX)
    		{
    			lcs16HbbAssistPressTC2 = S16_HBB_ASSIST_PRES_MAX;
    		}
    		else
    		{
    			;
    		}
    		
    	}
    	
    	lcs16HbbAssistPressureRateTC = (lcs16HbbAssistPressureTC - lcs16HbbAssistPressureOldTC)*10; /* 100/10 */
    	
    	lcs16HbbAssistPressureOldMotor = lcs16HbbAssistPressureMotor;
    	
      #if (__FL_PRESS==ENABLE) && (__FR_PRESS==ENABLE)	
    	WHEEL_PRESS_AVG = (pos_fl_1_100bar/10 + pos_fr_1_100bar/10)/2;
      #else	
    	WHEEL_PRESS_AVG = (FL.s16_Estimated_Active_Press + FR.s16_Estimated_Active_Press)/2;
      #endif
      
    	if (ABS_fz == 0)
    	{
    		lcs16HbbAssistPressureMotor = lcs16HbbTargetPressure - WHEEL_PRESS_AVG;    		
    	}
    	else
    	{
    		lcs16HbbAssistPressureMotor = lcs16HbbAssistPressureTC;
    	}

    	if(EBD_RA == 1)
    	{
    		lcs16HbbAssistPressureMotor = lcs16HbbAssistPressureMotor*S16_HBB_MSC_EBD_COMP_GAIN/10;
    	}
    	else
    	{
    		;
    	}
    	/*EBD시 Front Wheel 압력 과다 -> Motor 감소*/
    	
    	lcs16HbbAssistPressureMotor = LCESP_s16Lpf1Int(lcs16HbbAssistPressureMotor, lcs16HbbAssistPressureOldMotor, 28);
    	
    	if (lcs16HbbAssistPressureMotor <= MPRESS_0BAR )
    	{
    		lcs16HbbAssistPressureMotor = MPRESS_0BAR;

            if (lcu8HbbNeedMotorOffCnt >= T_200_MS)
            {
            	lcu1HbbMotorOffFlg = 1;
            	HBB_ESV_Primary_ACT = 0;  
            	HBB_ESV_Secondary_ACT = 0;
            	lcu8HbbNeedMotorOffCnt = T_200_MS;
            }
            else
            {
            	lcu8HbbNeedMotorOffCnt++;
            }
    	}
    	else
    	{
    		if (lcu1HbbMotorOffFlg == 1)
    		{
    			if (lcs16HbbAssistPressureMotor > MPRESS_3BAR)
    			{
    				lcu1HbbMotorOffFlg = 0;
    				HBB_ESV_Primary_ACT = 1;      		
    				HBB_ESV_Secondary_ACT = 1;
    				lcu8HbbNeedMotorOffCnt = 0;
    			}
    			else
    			{
    				;
    			}
    		}
    		else
    		{
    			lcu1HbbMotorOffFlg = 0;
    			HBB_ESV_Primary_ACT = 1;      		
    			HBB_ESV_Secondary_ACT = 1;
    			lcu8HbbNeedMotorOffCnt = 0;
    		}
    	}
    	
    	lcs16HbbAssistPressureRateMotor = (lcs16HbbAssistPressureMotor - lcs16HbbAssistPressureOldMotor)*10; /* 100/10 */                    
    /*}
    else
    {
        lcs16HbbAssistPressureTC = 0;
        lcs16HbbAssistPressureOldTC = 0;
        lcs16HbbAssistPressureRateTC = 0;
        lcs16HbbAssistPressureMotor = 0;
        lcs16HbbAssistPressureOldMotor = 0;
        lcs16HbbAssistPressureRateMotor = 0;
        lcs16HbbAssistPressTC2 = 0;
    }*/
}

/******************************************************************************
* FUNCTION NAME:      LCHBB_vDecideBrakeBoost
* CALLED BY:          LCHBB_vDecideHydraulicBrakeBoost()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Brake Boost in case of X-Split
******************************************************************************/
static void LCHBB_vDecideBrakeBoost(void)
{
   #if defined(__CAN_SW)
    if(ctu1HBB==0)
    {
   #endif /*__CAN_SW*/

		lcs16HbbVacuumLevel = LCESP_s16IInter3Point(vref,S16_HBB_VEHICLE_VELOCITY1,S16_HBB_VACUUM_PRESS_LEVEL1,
														 S16_HBB_VEHICLE_VELOCITY2,S16_HBB_VACUUM_PRESS_LEVEL2,
														 S16_HBB_VEHICLE_VELOCITY3,S16_HBB_VACUUM_PRESS_LEVEL3);
        
        lcs16HbbKneePointRatio = LCESP_s16IInter4Point(lcs16HbbVacuumPres,S16_HBB_VACUUM_PRESS_LEVEL1,S16_HBB_KNEE_POINT_RATIO_1,
        																  S16_HBB_VACUUM_PRESS_LEVEL2,S16_HBB_KNEE_POINT_RATIO_2,
        																  S16_HBB_VACUUM_PRESS_LEVEL3,S16_HBB_KNEE_POINT_RATIO_3,
        																  HbbNormalVacuumPressure,S16_HBB_KNEE_POINT_RATIO_4);
        
        lcs16HbbKneePointEstOld = lcs16HbbKneePointEst;
        lcs16HbbKneePointEst = (int16_t)((int32_t)lcs16HbbVacuumPres*lcs16HbbKneePointRatio/10);
        lcs16HbbKneePointEst = LCESP_s16Lpf1Int(lcs16HbbKneePointEst, lcs16HbbKneePointEstOld, 28);
    
        if(TCMF_LowVacuumBoost_flag==0)
        {
       
            if((lcs16HbbTargetPressureComp > lcs16HbbKneePointEst)/*||(lcs16HbbPedalTravel > HBB_MAX_PEDAL_TRAVEL))*/
              	/*&&(lcs16HbbAssistPressureTC > HBB_ENTER_ASSIST_PRESS_TH)*/
              	&&(lcs16HbbPedalTravel>S16_HBB_ENTER_PEDAL_TRAVEL)
                &&(vref>=HBB_ENTER_SPEED_TH) 
                &&(ABS_fz == 0)
                &&(lcs8HbbOtherCtrlEnable == 0)
                &&(lcu1HbbInhibitFlag==0)
                &&(lcu1HbbPedalRateOK==1)
                &&(lcs16HbbVacuumPres < lcs16HbbVacuumLevel)
                &&(U8_HBB_ENABLE_FLG==1))
            {
                TCMF_LowVacuumBoost_flag = 1;
                Low_Vacuum_Boost_Assist_flag = 1;
                HBB_ESV_Primary_ACT = 1;
                HBB_ESV_Secondary_ACT = 1;
                HBB_TC_Primary_ACT = 1;
                HBB_TC_Secondary_ACT = 1;
                lcu1HbbMotorOffFlg = 0;
                HBB_START_flg = 1;
                HBB_ON_cnt = 1;
                lcu1HbbInhibitFlag=0;
            }
            else 
            {
                if((HBB_OFF_cnt>0) && (HBB_OFF_cnt<100))
                {
                    HBB_OFF_cnt = HBB_OFF_cnt + 1;
                }
                else
                {
                    HBB_OFF_cnt = 0;
                }
                HBB_ON_cnt = 0;
            }
        }
        else if ((lcs16HbbPedalTravel < HBB_EXIT_PEDAL_TRAVEL)
        		/*||(HBB_release_cnt > T_1MIN) */
        		||((vref <= HBB_EXIT_SPEED_TH)&&(ABS_fz == 0))
        		||(vref <= VREF_0_125_KPH)
        		/*||(mpress>lcs16HbbTargetPressure)*/
        		||(lcs16HbbTargetPressureComp < lcs16HbbKneePointEst))
        {
            TCMF_LowVacuumBoost_flag = 0;
            Low_Vacuum_Boost_Assist_flag = 0;
            TCMF_LowVacuumBoost_cnt = 0;
            HBB_ESV_Primary_ACT = 0;  
            HBB_ESV_Secondary_ACT = 0;
            HBB_TC_Primary_ACT = 0;   
            HBB_TC_Secondary_ACT = 0; 
            HBB_release_mpress_cnt = 0;
            HBB_release_cnt = 0;
            HBB_OFF_cnt = 1;
            HBB_ON_cnt = 0;
            HBB_MP_VALID = 0;
            HBB_ABS_COMB_MSC_MOTOR_ON = 0;
        }
        else if(lcu1HbbInhibitFlag==1)
        {
            TCMF_LowVacuumBoost_flag = 0;
            Low_Vacuum_Boost_Assist_flag = 0;
            TCMF_LowVacuumBoost_cnt = 0;
            HBB_ESV_Primary_ACT = 0;  
            HBB_ESV_Secondary_ACT = 0;
            HBB_TC_Primary_ACT = 0;   
            HBB_TC_Secondary_ACT = 0; 
            HBB_release_mpress_cnt = 0;
            HBB_release_cnt = 0;
            HBB_OFF_cnt = 1;
            HBB_ON_cnt = 0;
            HBB_MP_VALID = 0;
            HBB_ABS_COMB_MSC_MOTOR_ON = 0;
        }
        else
        {
        	HBB_START_flg = 0;
        	
        	if(ABS_fz == 1)
        	{
        		HBB_ESV_Primary_ACT = 0;  
        	    HBB_ESV_Secondary_ACT = 0;
        	}
        	
            if(lcs16HbbVacuumPres>500)
            {
                if(vref<=VREF_0_5_KPH)
                {
                    HBB_release_cnt++;
                }
                else
                {
                    HBB_release_cnt = 0;
                }
            }
            else 
            {
                if(vref<=VREF_0_125_KPH)
                {
                    HBB_release_cnt++;
                }
                else if((vref<=VREF_0_5_KPH) && (HBB_release_cnt>2))
                {
                    HBB_release_cnt = HBB_release_cnt - 2;
                }
                else
                {
                    HBB_release_cnt = 0;
                }
            }

            if(ABS_fz==1) 
            {
                if(TCMF_LowVacuumBoost_cnt<=200) 
                {
                    TCMF_LowVacuumBoost_cnt++;
                }
                else { }
            }
            else if(TCMF_LowVacuumBoost_cnt>0) 
            {
                TCMF_LowVacuumBoost_cnt = 100;
            }
            else 
            {
                TCMF_LowVacuumBoost_cnt = 0;
            }

            if(HBB_ON_cnt < 200) 
            {
                HBB_ON_cnt = HBB_ON_cnt+1;
            }
            else
            {
            	;
            }
            
            if(HBB_MP_VALID==0)
            {
                if(mpress>=MPRESS_0G5BAR)
                {
                    HBB_MP_VALID=1;
                }
            }
        }
    
   #if defined(__CAN_SW)
    }
    else 
    {
        TCMF_LowVacuumBoost_flag = 0;
        Low_Vacuum_Boost_Assist_flag = 0;
        TCMF_LowVacuumBoost_cnt = 0;
        HBB_ESV_Primary_ACT = 0;  
        HBB_ESV_Secondary_ACT = 0;
        HBB_TC_Primary_ACT = 0;   
        HBB_TC_Secondary_ACT = 0; 
        HBB_release_mpress_cnt = 0;
        HBB_release_cnt = 0;
    }
   #endif /*(__CAN_SW)*/
}

/******************************************************************************
* FUNCTION NAME:      LCHBB_vCalcSensorSignal
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Main Function of HBB Control
******************************************************************************/

static void LCHBB_vCalcSensorSignal(void)
{
	
  /* -------- Vacuum Sensor ------------------------------------------------- */             
  
  /* -- HMC & GM --                                                           */  
  
  /* fs16CalcVacuum = (INT)-vacuum_1_10kpa                                    */  
  /* fs16CalcVacuum : -1000 ~ 0 (resolution : 0.1 kPa, -100 ~ 0 kPa )         */
    
  /* Tahoe : -600 ~ -700, JM : -800 at normal condition                       */
  /* GSUV  : -600 at normal condition, EVP control below -400                 */
  
  /* ------------------------------------------------------------------------ */
  
  #if (__VACUUM_SENSOR_ENABLE==ENABLE)
	
    lcs16HbbVacuumPres = -fs16CalcVacuum;

  #endif
    
  /* ---------- Pedal Travel ------------------------------------------------- */   
  
  /* GM  : fcu8BrkPdlPos (CAN) : 0 ~ 255 ( 0 ~ 100% )                          */
  
  /* HMC : fs16CalcPDT (Analog): 0 ~ 1550 ( 0 ~ 155ms )                        */
  
  /* ------------------------------------------------------------------------  */
  
    lcs16HbbPedalTravel = lsesps16BrkAppSenFiltF;
	
}

   
/******************************************************************************
* FUNCTION NAME:      LCHBB_vCallControl
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Main Function of HBB Control
******************************************************************************/

void LCHBB_vCallControl(void)
{
	
	LCHBB_vCalcSensorSignal();
    LCHBB_vInterfaceOtherSystems();
    LCHBB_vCheckInhibitCondition();
    
  #if defined(__CAN_SW)
    if(ctu1HBB==1) 
    {
        Low_Vacuum_Boost_Assist_flag=1;
        Vacuum_level = 90;
    }
    else 
    {
        Low_Vacuum_Boost_Assist_flag=0;
        Vacuum_level = 0;
    }
  #endif /*defined(__CAN_SW)*/

    LCHBB_vCalcBrakePedalTravelRate(); 
    LCHBB_vCalcAvgMCPressure();
    LCHBB_vEstTargetPressByBPTravel();
    LCHBB_vDecideAssistPressure();
    LCHBB_vDecideBrakeBoost();
    
    if (TCMF_LowVacuumBoost_flag == 1)
    {
    	HBB_ON = 1;
    }
    else
    {
    	HBB_ON = 0;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCMSC_vSetHBBTargetVoltage
* CALLED BY:          LCMSC_vInterfaceTargetvol()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set Motor Target BEMF Voltage for HBB
******************************************************************************/

    #if __ADVANCED_MSC
void LCMSC_vSetHBBTargetVoltage(void)
{
    int16_t hbb_msc_target_vol_old;
    int16_t DELTA_ASSIST_PRESS_PT_MP;
    
    hbb_msc_target_vol_old = hbb_msc_target_vol;
        
    if(lcs8HbbOtherCtrlEnable==0) 
    {
        HBB_MSC_MOTOR_ON = TCMF_LowVacuumBoost_flag;
    }
    else 
    {
        HBB_MSC_MOTOR_ON = 0;
    }

    lcs16HbbMscTargetVolFactor = ((lcs16HbbAssistPressureMotor * HBB_Assist_Press_Motor_D_Gain) + (lcs16HbbAssistPressureRateMotor * HBB_Assist_Press_Motor_P_Gain))/2;

    if (lcs16HbbMscTargetVolFactor < 0)
    {
    	lcs16HbbMscTargetVolFactor = MPRESS_1BAR;
    }
    else if (lcs16HbbMscTargetVolFactor > MPRESS_50BAR)
    {
    	lcs16HbbMscTargetVolFactor = MPRESS_50BAR;
    }
    else
    {
    	;
    }

    if((HBB_MSC_MOTOR_ON==1)&&(lcu1HbbMotorOffFlg==0)) 
    {
        if(HBB_ON_cnt<=S16_HBB_MSC_INIT_TIME) /*((HBB_motor_delay_cnt > 0) || ((HBB_START_flg==1) && ((HBB_OFF_cnt==0) || (HBB_OFF_cnt>T_200_MS))))*/
        {
            hbb_msc_target_vol = LCESP_s16IInter4Point(lcs16HbbAssistPressureMotor,
            									  		MPRESS_1BAR, 	HBB_INITIAL_TARGET_VOL_1BAR,  
            									  		MPRESS_10BAR,	HBB_INITIAL_TARGET_VOL_10BAR, 
            									  		MPRESS_30BAR,	HBB_INITIAL_TARGET_VOL_30BAR, 
            									  		MPRESS_100BAR,	HBB_INITIAL_TARGET_VOL_100BAR);
            HBB_ABS_COMB_MSC_MOTOR_ON = 0;
            									  		           									  								  		
        }
        else
        {
            	if (ABS_fz == 1)                  
            	{                                 
            		HBB_ABS_COMB_MSC_MOTOR_ON = 1;
            		hbb_msc_target_vol = HBB_ABS_COMB_TARGET_VOL;  
            	}                                 
            	else                              
            	{                                 
            		HBB_ABS_COMB_MSC_MOTOR_ON = 0;
            		hbb_msc_target_vol = LCESP_s16IInter4Point(lcs16HbbAssistPressureMotor,                   
            												  			MPRESS_1BAR, 	HBB_TARGET_VOL_1BAR,  
            		                                          			MPRESS_10BAR,	HBB_TARGET_VOL_10BAR, 
            		                                          			MPRESS_30BAR,	HBB_TARGET_VOL_30BAR, 
            		                                          			MPRESS_100BAR,	HBB_TARGET_VOL_100BAR);                                       			
            	}
        }

/*        
        if((((lcs16HbbPedalTravelRate <= 0) && (WHEEL_PRESS_AVG>=((lcs16HbbTargetPressure*4)/5))) 
            || ((HBB_NEED_AdditionalBrkFluidP==0) && (HBB_NEED_AdditionalBrkFluidS==0) && (assist_pressure_inc_cnt==0)))&& (ABS_fz==0))
        {
            if(lcs16HbbPedalTravelRate>=1) 
            {
                HBB_motor_off_cnt=HBB_motor_off_cnt-1;
            }
            else 
            {
                HBB_motor_off_cnt++;
            }
            
        }
        else
        {
            if(HBB_motor_off_cnt>=5)
            {
                HBB_motor_off_cnt = HBB_motor_off_cnt - 5;
            }
            else
            {
                HBB_motor_off_cnt = 0;
            }
        }

        if(HBB_motor_delay_cnt==0)
        {
            if(lcs16HbbAssistPressureRateMotor>=MPRESS_150BAR) 
            {
                HBB_motor_delay_cnt = HBB_motor_delay_cnt+2;
            }
            else
            {
                ;
            }
        }
        else
        {
            if(lcs16HbbAssistPressureRateMotor>=MPRESS_150BAR) 
            {
            	HBB_motor_delay_cnt = HBB_motor_delay_cnt+2;
            }
            else if(lcs16HbbAssistPressureRateMotor>=MPRESS_100BAR) 
            {
            	HBB_motor_delay_cnt = HBB_motor_delay_cnt+1;
            }
            else if(hbb_msc_target_vol > MSC_5_V) 
            {
            	HBB_motor_delay_cnt = HBB_motor_delay_cnt+2;
            }
            else if(hbb_msc_target_vol > MSC_3_V) 
            {
            	HBB_motor_delay_cnt = HBB_motor_delay_cnt+1;
            }
            else 
            {
            	HBB_motor_delay_cnt = HBB_motor_delay_cnt-1;
            }
        }
*/
    }
    else 
    {
        hbb_msc_target_vol = 0;
        HBB_motor_off_cnt=0;
        HBB_motor_delay_cnt=0;
    }

    if((HBB_MSC_MOTOR_ON==1)&&(hbb_msc_target_vol>0)&&(ABS_MSC_MOTOR_ON==1))
    {
        HBB_ABS_COMB_MSC_MOTOR_ON=1;
/*
        if((TCMF_LowVacuumBoost_flag==1) && (lcs8HbbPressMode==HBB_RAPID_FADE_OUT) && (lcs8HbbOtherCtrlEnable==0))
        {
            if(abs_msc_target_vol > MSC_2_V)
            {
                hbb_msc_target_vol = MSC_2_V;
                if(lcs8HbbMpressFastDecCnt>5)
                {
                    hbb_msc_target_vol = abs_msc_target_vol - ((lcs8HbbMpressFastDecCnt-5)*2);
                }
                else {;}
            }
            else {;}
        }
        else {;}
*/
    }
    else
    {
        HBB_ABS_COMB_MSC_MOTOR_ON=0;
    }

    if(hbb_msc_target_vol==0) 
    {
        HBB_MSC_MOTOR_ON = 0;
    }
    else 
    {
        ;
    }
    if((hbb_msc_target_vol<MSC_0_1_V)&&(hbb_msc_target_vol>0)) 
    {
        hbb_msc_target_vol = MSC_0_1_V;
    }
    else 
    {
    	;
    }
}
    #endif /*__ADVANCED_MSC*/
    #endif /*__TCMF_LowVacuumBoost*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCHBBCallControl
	#include "Mdyn_autosar.h"
#endif
