#ifndef __LCESPCALLCONTROL_H__
#define __LCESPCALLCONTROL_H__

/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/
#define	  __MGH60_NEW_SLIP_CONTROL		    0
#define     __YAW_SS_MODEL_ENABLE   	    ENABLE
#define   __MGH80_ESC_ENTER_EXIT_IMPROVE    1

#define __MOMENT_TO_TARGET_SLIP_NEW 1
#define __SMOOTH_REAR_DEL_M_GAIN    1
#define DELTA_COMPENSATION_DUTY_U_REAR 8

/* BMW-7 : Tuning Parameters */
#define S16_YA_FB_F_OS_WEG_VLOW      30 
#define S16_YA_FB_F_OS_WEG_LOW       35 
#define S16_YA_FB_F_OS_WEG_MED       40 
#define S16_YA_FB_F_OS_WEG_HIG       45 
#define S16_YA_FB_F_OS_WEG_VHIG      50 

#define S16_YA_FB_F_OS_TH1      150 
#define S16_YA_FB_F_OS_TH2      250 

#define	S16_OU_ENTER_D_HIGH		 70
#define	S16_OU_ENTER_D     		 40
#define	S16_OU_ENTER_D_LOW 		 50
#define	S16_O_U_PERMIT_THR 		200
#define	S16_OU_EXIT_D_HIGH 		 15
#define	S16_OU_EXIT_D      		 13
#define	S16_OU_EXIT_D_LOW  		 10


#define S16_YC_OU_GAIN_F_OS_HIGH 	14
#define S16_YC_OU_GAIN_F_OS      	14
#define S16_YC_OU_GAIN_F_OS_LOW  	14
#define S16_SLALOM_OVER_GAIN     	100

/* MGH60US GM SSTS - Flat Tire Detection */
#if __CAR==GM_S4500 || __CAR==GM_T300
#define		S16_Common_Temp1	100
#define		S16_Common_Temp2	100
#define		S16_Common_Temp3	100
#define		S16_Common_Temp4	100
#endif

/*imsi calibration for compile IDB*/
#define S16TCSCpWhlPre_1    0  
#define S16TCSCpWhlPre_2    10 
#define S16TCSCpWhlPre_3    100
#define S16TCSCpWhlPre_4    250
#define S16TCSCpWhlPre_5    900
                        
#define S16TCSCpTCcrnt_1    0   
#define S16TCSCpTCcrnt_2    380 
#define S16TCSCpTCcrnt_3    530 
#define S16TCSCpTCcrnt_4    800 
#define S16TCSCpTCcrnt_5    1450


#define	U8_TCMF_INC_STEP_INT_REG1           10     
#define	U8_TCMF_INC_STEP_INT_REG2           20
#define	U8_TCMF_INC_STEP_INT_REG3           30
#define	U8_TCMF_INITIAL_DUTY_INC            25
#define	U8_TCMF_RISE_DUTY_INC_MAX           25
#define	U8_TCMF_INITIAL_DUTY_INC_04         20
#define	U8_TCMF_INITIAL_DUTY_INC_06         25
#define	U8_TCMF_INITIAL_DUTY_INC_08         27
#define	U8_TCMF_INITIAL_DUTY_INC_10         30
#define	U8_TCMF_INITIAL_DUTY_INC_12         32
#define	U8_TCMF_RISE_DUTY_INC_MAX_04        15
#define	U8_TCMF_RISE_DUTY_INC_MAX_06        20
#define	U8_TCMF_RISE_DUTY_INC_MAX_08        25
#define	U8_TCMF_RISE_DUTY_INC_MAX_10        27
#define	U8_TCMF_RISE_DUTY_INC_MAX_12        28
#define	U8_TCMF_STEP_INT_DUTY_04_10          7
#define	U8_TCMF_STEP_INT_DUTY_04_20         12
#define	U8_TCMF_STEP_INT_DUTY_04_30         14
#define	U8_TCMF_STEP_INT_DUTY_04_40         15
#define	U8_TCMF_STEP_INT_DUTY_04_50         15
#define	U8_TCMF_STEP_INT_DUTY_06_10          7
#define	U8_TCMF_STEP_INT_DUTY_06_20         13
#define	U8_TCMF_STEP_INT_DUTY_06_30         16
#define	U8_TCMF_STEP_INT_DUTY_06_40         20
#define	U8_TCMF_STEP_INT_DUTY_06_50         21
#define	U8_TCMF_STEP_INT_DUTY_08_10          4
#define	U8_TCMF_STEP_INT_DUTY_08_20         10
#define	U8_TCMF_STEP_INT_DUTY_08_30         14
#define	U8_TCMF_STEP_INT_DUTY_08_40         18
#define	U8_TCMF_STEP_INT_DUTY_08_50         19
#define	U8_TCMF_STEP_INT_DUTY_10_10          9
#define	U8_TCMF_STEP_INT_DUTY_10_20         12
#define	U8_TCMF_STEP_INT_DUTY_10_30         22
#define	U8_TCMF_STEP_INT_DUTY_10_40         24
#define	U8_TCMF_STEP_INT_DUTY_10_50         24
#define	U8_TCMF_STEP_INT_DUTY_12_10          9
#define	U8_TCMF_STEP_INT_DUTY_12_20         12
#define	U8_TCMF_STEP_INT_DUTY_12_30         19
#define	U8_TCMF_STEP_INT_DUTY_12_40         19
#define	U8_TCMF_STEP_INT_DUTY_12_50         20
#define	S16_TCMF_T_VOLT_DUMP              2000
#define	S16_TCMF_T_VOLT_HOLD              2000
#define	S16_TCMF_T_VOLT_INI_FULL_HMU     10000
#define	S16_TCMF_T_VOLT_INI_FULL_LMU     10000
#define	S16_TCMF_T_VOLT_INI_FULL_MMU     10000
#define	S16_TCMF_T_VOLT_MAX_RISE_HM      10000
#define	S16_TCMF_T_VOLT_MAX_RISE_LM      10000
#define	S16_TCMF_T_VOLT_MAX_RISE_MM      10000
#define	S16_TCMF_T_VOLT_STEP_RISE_R1_H    6000
#define	S16_TCMF_T_VOLT_STEP_RISE_R1_L    6000
#define	S16_TCMF_T_VOLT_STEP_RISE_R1_M    6000
#define	S16_TCMF_T_VOLT_STEP_RISE_R2_H    9000
#define	S16_TCMF_T_VOLT_STEP_RISE_R2_L    7000
#define	S16_TCMF_T_VOLT_STEP_RISE_R2_M    7000
#define	S16_TCMF_T_VOLT_STEP_RISE_R3_H   10000
#define	S16_TCMF_T_VOLT_STEP_RISE_R3_L    8000
#define	S16_TCMF_T_VOLT_STEP_RISE_R3_M    8000
#define	S16_TCMF_DUMP_FAST_DEC_TH_HM       300
#define	S16_TCMF_DUMP_FAST_DEC_TH_LM       300
#define	S16_TCMF_DUMP_FAST_DEC_TH_MM       300
#define	S16_TCMF_MAX_RISE_THR_HM           500
#define	S16_TCMF_MAX_RISE_THR_LM           500
#define	S16_TCMF_MAX_RISE_THR_MM           500
#define	S16_TCMF_RISE_STEP_THR_R2_HM       100
#define	S16_TCMF_RISE_STEP_THR_R2_LM       100
#define	S16_TCMF_RISE_STEP_THR_R2_MM       100
#define	S16_TCMF_RISE_STEP_THR_R3_HM       300
#define	S16_TCMF_RISE_STEP_THR_R3_LM       300
#define	S16_TCMF_RISE_STEP_THR_R3_MM       300
#define	S16_TCMF_RISE_STEP_USC_DOT_HM     1000
#define	S16_TCMF_RISE_STEP_USC_DOT_LM     1000
#define	S16_TCMF_RISE_STEP_USC_DOT_MM     1000
#define	S16_TCMF_TC_DUTY_L_LIMIT           150
#define	S16_TCMF_DUMP_DUTY_DIS_DEC          70
#define	S16_TCMF_FADE_DUTY_DIS_DEC          70
#define	U8_TCMF_DUMP_DUTY_DEC_FAST          35
#define	U8_TCMF_DUMP_DUTY_DEC_SLOW          20
#define	U8_TCMF_FADE_DUTY_DEC               30
#define	U8_TCMF_INC_STEP_COM_REG1_HM        10
#define	U8_TCMF_INC_STEP_COM_REG1_LM        10
#define	U8_TCMF_INC_STEP_COM_REG1_MM        10
#define	U8_TCMF_INC_STEP_COM_REG2_HM        30
#define	U8_TCMF_INC_STEP_COM_REG2_LM        30
#define	U8_TCMF_INC_STEP_COM_REG2_MM        30
#define	U8_TCMF_INC_STEP_COM_REG3_HM        50
#define	U8_TCMF_INC_STEP_COM_REG3_LM        50
#define	U8_TCMF_INC_STEP_COM_REG3_MM        50
#define	U8_TCMF_INITIAL_FULL_TIME           25
#define	S16_TCMF_DUMP_DUTY_DIS_DEC_CBS     100
#define	S16_TCMF_FADE_DUTY_DIS_DEC_CBS     100
#define	S16_TCMF_HOLD_DUTY_DIS_DEC_CBS     100
#define	S16_TCMF_RISE_DUTY_CBS_START       400
#define	U8_TCMF_DUMP_DUTY_DEC_FAST_CBS      70
#define	U8_TCMF_DUMP_DUTY_DEC_SLOW_CBS      40
#define	U8_TCMF_FADE_DUTY_DEC_CBS           50
#define	U8_TCMF_RISE_DUTY_STEP_CBS_HM       25
#define	U8_TCMF_RISE_DUTY_STEP_CBS_LM        5
#define	U8_TCMF_RISE_DUTY_STEP_CBS_MM       15
#define	S16_ESC_RESERVED_01               1000
#define	S16_ESC_RESERVED_02               1000
#define	S16_ESC_RESERVED_03               1000
#define	S16_ESC_RESERVED_04                500
#define	S16_ESC_RESERVED_05                500
#define	S16_ESC_RESERVED_06                500
#define	S16_ESC_RESERVED_07                 10
#define	S16_ESC_RESERVED_08                 50
#define	S16_ESC_RESERVED_09                 10
#define	S16_ESC_RESERVED_10                  7

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/
#define   __TCMF_UNDERSTEER_CONTROL   ENABLE

/*Global Extern Variable Declaration *******************************************/
extern int16_t alpha_r;
extern int16_t m1;
extern int16_t m2;
extern int16_t m4;
extern int16_t m5;
extern int16_t m7;
extern int16_t m8;
extern int16_t m9;
extern int16_t cal_target_slip_front;
extern int16_t cal_target_slip_rear;
extern int16_t target_moment_f;
extern int16_t target_moment_r;
extern int16_t target_slip_r_0;
extern int16_t target_moment_r_0;
extern int16_t lcesps16OVER2WH_CompCount;
     
extern int16_t  delta_moment_q_os, delta_moment_q_us ; 
extern int16_t  ExtUnder_Req_Decel;
extern int16_t  ExtUnder_Req_Press;
extern int16_t  Extr_Under_Frt_Moment_In; 
extern int16_t  Extr_Under_Frt_Moment_Out;
extern int16_t  Extr_Under_Frt_Moment_FL;
extern int16_t  Extr_Under_Frt_Moment_FR; 
extern int8_t  ext_under_on_counter ; 

extern int16_t  Extr_Under_Frt_fr_Press_1st ;
extern int16_t  Extr_Under_Frt_fl_Press_1st ;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t  Extr_Under_Frt_fl_Press_Max ;
//extern int16_t  Extr_Under_Frt_fr_Press_Max ;
#else
extern int16_t  Extr_Under_Frt_fl_Press_Max ;
extern int16_t  Extr_Under_Frt_fr_Press_Max ;
#endif

extern int16_t esp_on_counter_f,esp_on_counter_r;
extern int16_t esp_on_counter_fl;
extern int16_t esp_on_counter_fr;
extern int16_t esp_on_counter_rl;
extern int16_t esp_on_counter_rr;
extern int16_t s16_pressure_gain_front;
extern int16_t tire_radius;
extern int16_t f_normal_half_front;
extern int16_t f_normal_half_rear;
extern int16_t C1_front;
extern int16_t C1_rear;
extern int16_t C2_front;
extern int16_t C2_rear;

extern int16_t delta_moment_q_under;
extern int16_t ldesps16YawcOffset; 

extern int16_t lcesps16Yaw3rdMoment;

extern int8_t flg_esc_ctrl_01 ; 

extern int8_t comp_duty4PU_rr, comp_duty4PU_rl;

extern int16_t rear_high_speed;
extern int16_t rear_med1_speed;
extern int16_t rear_med2_speed;
extern int16_t rear_low_speed;

extern int16_t rear_low_speed_gain;
extern int16_t rear_med_speed_gain;
extern int16_t rear_high_speed_gain;    

extern uint8_t ldespu8Cnt1stRR;
extern uint8_t ldespu8Cnt1stRL;

extern uint8_t ldespu8Cnt2ndRR;
extern uint8_t ldespu8Cnt2ndRL;

extern int16_t wheelpress_fl, wheelpress_fr, wheelpress_rl, wheelpress_rr;

#if ESC_PULSE_UP_RISE
	
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//  extern int16_t lcs16EscInitialPulseupDutyfl;
//	extern int16_t lcs16EscInitialPulseupDutyfr;
//	extern int16_t lcs16EscInitialPulseupDutyrl;
//	extern int16_t lcs16EscInitialPulseupDutyrr;
//	extern int16_t lcs16EsctTimePulseupDutyfl;
//	extern int16_t lcs16EsctTimePulseupDutyfr;
//	extern int16_t lcs16EscInitialPulseupDutyfl_old;
//	extern int16_t lcs16EscInitialPulseupDutyfr_old;
#else
	extern int16_t lcs16EscInitialPulseupDutyfl;
	extern int16_t lcs16EscInitialPulseupDutyfr;
	extern int16_t lcs16EscInitialPulseupDutyrl;
	extern int16_t lcs16EscInitialPulseupDutyrr;
	extern int16_t lcs16EsctTimePulseupDutyfl;
	extern int16_t lcs16EsctTimePulseupDutyfr;
	extern int16_t lcs16EscInitialPulseupDutyfl_old;
	extern int16_t lcs16EscInitialPulseupDutyfr_old;
#endif

	extern int16_t esc_pulse_up_rise_scan_fl;
	extern int16_t esc_pulse_up_rise_scan_fr;
	extern int16_t esc_pulse_up_rise_scan_rl;
	extern int16_t esc_pulse_up_rise_scan_rr;

  extern int16_t pu_full_T_fl;
  extern int16_t pu_full_T_fr;
  extern int16_t pu_full_T_rl;
  extern int16_t pu_full_T_rr;	
#endif

extern int8_t comp_duty_fl, comp_duty_fr, comp_duty_rl, comp_duty_rr;

extern int16_t lcs16OVER2WH_count;
extern int16_t lcs16OVER2WH_FadeCount;

#if (__CRAM == 1)
extern int16_t moment_pre_front_cram;
extern int16_t moment_pre_rear_cram;
#endif

#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
extern U8_BIT_STRUCT_t LAESC00;

#define lcescu1TCMFUnderControlRl		LAESC00.bit0
#define lcescu1TCMFUnderControlRr       LAESC00.bit1
#define laescu1TCMFUnderControlEnable   LAESC00.bit2
#define lcescu1TCMFUCSafterTVBBTC       LAESC00.bit3
#define lcescu1TCMFUCSafterTVBBMotor    LAESC00.bit4
#define laescu1reserved05			    LAESC00.bit5
#define laescu1reserved06               LAESC00.bit6
#define laescu1reserved07               LAESC00.bit7

extern int16_t lcescs16TCMFUnderControlCnt;
#endif

extern	void LCESP_vDetectControlSignal(void);
extern	void LCESP_vDetectLowSpeed(void);
extern	void LCESP_vDetectWstrJump(void);

/*Global Extern Functions Declaration ******************************************/
extern int16_t LCESP_s16FindRoadDependatGain( int16_t road_mu, int16_t gain_high, int16_t gain_med, int16_t gain_low );
extern int16_t LCESP_s16LimitInt(int16_t x_input, int16_t temp_local1, int16_t temp_local2);
extern int16_t LCESP_s16MuldivInt(int16_t temp_local2,int16_t temp_local1,int16_t temp_local0);
extern uint16_t LCESP_u16CompSqrt(uint16_t tempW11, uint16_t tempW12);

#if ESC_PULSE_UP_RISE
extern void LCESP_vCalEscRiseRate(void);
#endif
#endif

