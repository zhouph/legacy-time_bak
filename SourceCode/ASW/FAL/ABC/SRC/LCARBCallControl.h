#ifndef __LCARBCALLCONTROL_H__
#define __LCARBCALLCONTROL_H__

/*includes********************************************************************/

#include"LVarHead.h"

  #if __HEV
/*Global Type Declaration ****************************************************/

typedef struct
{
	uint16_t lcarbu1Bit07		:1;
	uint16_t lcarbu1Bit06		:1;
	uint16_t lcarbu1Bit05		:1;
	uint16_t lcarbu1Bit04		:1;
	uint16_t lcarbu1Bit03		:1;
	uint16_t lcarbu1Bit02		:1;
	uint16_t lcarbu1Bit01		:1;
	uint16_t lcarbu1Bit00		:1;
}ARB_FLAG_t;

#define	lcarbu1HevErrorFlag   		lcarbCtrlFlags01.lcarbu1Bit07	
#define	lcarbu1ActVlvACmd     		lcarbCtrlFlags01.lcarbu1Bit06	
#define	lcarbu1ActVlvBCmd     		lcarbCtrlFlags01.lcarbu1Bit05	
#define	lcarbu1InhibitionFlag		lcarbCtrlFlags01.lcarbu1Bit04	
#define	lcarbu1ActiveFlag			lcarbCtrlFlags01.lcarbu1Bit03	
#define	lcarbu1ActSecondaryTc		lcarbCtrlFlags01.lcarbu1Bit02	
#define	lcarbu1ActPrimaryTc			lcarbCtrlFlags01.lcarbu1Bit01	

/*ARB MFC Duty Control********************************************************/	
#define ARB_MFC_DUTY_CONTROL	1

	#if ARB_MFC_DUTY_CONTROL
/*Tunning Parameter***********************************************************/	
#define	U16_ARB_MFC_DUTY_MAX		8000
#define	U16_ARB_MFC_DUTY_OFF		0
#define	U16_ARB_MFC_DUTY_SETTING	7000
#define	U8_ARB_MFC_DUTY_GRAD1		35
#define	U8_ARB_MFC_DUTY_GRAD2		98
	
#define	lcarbu1BrkFltFlag   		lcarbCtrlFlags02.lcarbu1Bit05
#define	lcarbu1BrkOnFlag 	  		lcarbCtrlFlags02.lcarbu1Bit04
#define	lcarbu1ControlPFlag   		lcarbCtrlFlags02.lcarbu1Bit03
#define	lcarbu1ControlSFlag   		lcarbCtrlFlags02.lcarbu1Bit02
#define	lcarbu1BrakeOffPFlag   		lcarbCtrlFlags02.lcarbu1Bit01
#define	lcarbu1BrakeOffSFlag   		lcarbCtrlFlags02.lcarbu1Bit00
/*Global Extern Variable  Declaration*****************************************/

extern int16_t lcarbs16ActiveCnt;
extern ARB_FLAG_t lcarbCtrlFlags01;
extern ARB_FLAG_t lcarbCtrlFlags02;
extern int16_t lcarbu16MFCDutyP;
extern int16_t lcarbu16MFCDutyS;
	#endif
/*Global Extern Functions  Declaration****************************************/
extern  void 	LCARB_vCallControl(void);

	#if ARB_MFC_DUTY_CONTROL
extern	void	LCARB_vSetDutyPattern(void);
extern	void	LCARB_vAllocValveDutyArray(void);
	#endif
  #endif
#endif
