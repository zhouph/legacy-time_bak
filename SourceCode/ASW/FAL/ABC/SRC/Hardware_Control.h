#ifndef __HARDWARE_CONTROL_H__
#define __HARDWARE_CONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

#if __TCMF_CONTROL
/*Global Type Declaration ****************************************************/
#define U16_PRIMARY_CIRCUIT         1
#define U16_SECONDARY_CIRCUIT       2

#define __TC_ORIFICE_0P9            0
#define __MGH80_ESC_DV2             1

#define __ESV_MIN_DUTY_CYCLE_CTRL  	1

#define TC_CURRENT_INC  10
#define TC_CURRENT_INC_BTC1 10
#define TC_CURRENT_INC_BTC2 4
#define TC_CURRENT_INC_VAFS 2
#define TC_CURRENT_INC_LFC 4
#define TC_CURRENT_DEC  15

#define TC_I_4mA            4
#define TC_I_5mA            5
#define TC_I_10mA           10
#define TC_I_20mA           20
#define TC_I_50mA           50
#define TC_I_100mA          100
#define TC_I_150mA          150
#define TC_I_170mA          170
#define TC_I_200mA          200
#define TC_I_300mA          300
#define TC_I_350mA          350
#define TC_I_375mA          375
#define TC_I_400mA          400
#define TC_I_500mA          500
#define TC_I_600mA          600
#define TC_I_700mA          700
#define TC_I_800mA          800
#define TC_I_900mA          900
#define TC_I_1000mA         1000
#define TC_I_1400mA         1400

#define MFC_PRESS_0BAR      0
#define MFC_PRESS_10BAR     10
#define MFC_PRESS_20BAR     20
#define MFC_PRESS_50BAR     50
#define MFC_PRESS_70BAR     70
#define MFC_PRESS_160BAR    160

#define ESV_DRIVE_DUTY_MIN  165//95 /* Old ESV:80, New ESV:95 */
#define	U8_ESV_DRIVE_DUTY_MAX	255
/*__ESV_MIN_DUTY_CYCLE_CTRL*/
#define U16_ESV_DRIVE_CYCLE     	   L_U8_TIME_10MSLOOP_1000MS
#define U8_ESV_DRIVE_MAX_DUTY_TIME     L_U8_TIME_10MSLOOP_50MS
#define U8_ESV_DRIVE_MIN_OPEN_DUTY     70

#if __TC_ORIFICE_0P9
#define MFC_DITHER_DUTY1    20
#define MFC_DITHER_DUTY2    0
#define TC_DITHER_PERIOD			    4
#define TC_DITHER_PERIOD_HALF		    2
#define TC_CURRENT_MIN  350    /* min relief(0bar) TC current */
#define TC_CURRENT_MAX  1000   /* max relief(160bar) TC current */
#elif __MGH80_ESC_DV2
#define MFC_DITHER_DUTY1    20
#define MFC_DITHER_DUTY2    			4
#define TC_DITHER_PERIOD			    2
#define TC_DITHER_PERIOD_HALF		    1
#define TC_CURRENT_MIN  250    /* min relief(0bar) TC current */
#define TC_CURRENT_MAX  1550   /* max relief(160bar) TC current */
#else
#define MFC_DITHER_DUTY1    12
#define MFC_DITHER_DUTY2    2
#define TC_DITHER_PERIOD			    2
#define TC_DITHER_PERIOD_HALF		    1
#define TC_CURRENT_MIN  350    /* min relief(0bar) TC current */
#define TC_CURRENT_MAX  1000   /* max relief(160bar) TC current */
#endif

#if __VDC
#define __PBA_MFC_EXIT          1
#endif

#if __SCC
#define __SCC_DITHER_ENABLE     0
#endif

#define __TCMF_DITHER_DISABLE   1

typedef struct
{
    uint8_t MFC_Target_Press;
    uint8_t MFC_PWM_DUTY_temp;
    uint8_t MFC_PWM_DUTY_temp_old;
    uint8_t MFC_Duty_Dither_cnt;
}MFC_DUTY_t;

typedef struct
{
    uint16_t    clu1MfcTcNoPrimaryACT       :1;
    uint16_t    clu1MfcTcNoSecondaryACT     :1;
    uint16_t    clu1MfcEsvPrimaryACT        :1;
    uint16_t    clu1MfcEsvSecondaryACT      :1;
    uint16_t    clu1MfcEspPrimaryCTRL       :1;
    uint16_t    clu1MfcEspSecondaryCTRL     :1;
    uint16_t    clu1MfcTcsPrimaryCTRL       :1;
    uint16_t    clu1MfcTcsSecondaryCTRL     :1;
}MFC_FLG_t;

#define     MFC_TCNO_Primary_ACT            MFC_FLAGS.clu1MfcTcNoPrimaryACT
#define     MFC_TCNO_Secondary_ACT          MFC_FLAGS.clu1MfcTcNoSecondaryACT
#define     MFC_ESV_Primary_ACT             MFC_FLAGS.clu1MfcEsvPrimaryACT
#define     MFC_ESV_Secondary_ACT           MFC_FLAGS.clu1MfcEsvSecondaryACT
#define     MFC_ESP_Primary_CTRL            MFC_FLAGS.clu1MfcEspPrimaryCTRL
#define     MFC_ESP_Secondary_CTRL          MFC_FLAGS.clu1MfcEspSecondaryCTRL
#define     MFC_TCS_Primary_CTRL            MFC_FLAGS.clu1MfcTcsPrimaryCTRL
#define     MFC_TCS_Secondary_CTRL          MFC_FLAGS.clu1MfcTcsSecondaryCTRL

/*Global Extern Variable  Declaration*****************************************/
extern int16_t MFC_Current_DEC, MFC_Current_BA, MFC_Current_VH;
extern  MFC_DUTY_t la_PtcMfc,la_StcMfc;
extern  MFC_FLG_t   MFC_FLAGS;
extern uint16_t   TC_MFC_Voltage_Primary, TC_MFC_Voltage_Primary_dash;
extern uint16_t   lau16HcEsvActTimerPri, lau16HcEsvActTimerSec;
extern uint16_t   TC_MFC_Voltage_Secondary, TC_MFC_Voltage_Secondary_dash;
extern int16_t    MFC_Current_HBB_S, MFC_Current_HBB_P;
extern int16_t    MFC_Current_ESP_S, MFC_Current_ESP_P;
extern int16_t    MFC_Current_TCS_S, MFC_Current_TCS_P;
extern int16_t    MFC_Current_ACC_S, MFC_Current_ACC_P;
extern int16_t    MFC_Current_HDC_S, MFC_Current_HDC_P;
extern int16_t    MFC_Current_HSA_S, MFC_Current_HSA_P;
extern int16_t    MFC_Current_EPB_S, MFC_Current_EPB_P;
extern int16_t    MFC_Current_AVH_S, MFC_Current_AVH_P, MFC_Current_AVH_old;
#if __SPAS_INTERFACE
extern int16_t     MFC_Current_SPAS_S, MFC_Current_SPAS_P;
#endif
/*#if __Decel_Ctrl_Integ*/
extern int16_t    MFC_Current_DEC_S, MFC_Current_DEC_P;
extern int16_t		MFC_Current_TSP_S, MFC_Current_TSP_P;
/*#endif*/
#if __PBA_MFC_EXIT
extern int16_t    MFC_Current_PBA_S, MFC_Current_PBA_P;
#endif
extern int16_t    MFC_Current_ARB_S, MFC_Current_ARB_P;
extern int16_t    MFC_Current_HRB_S, MFC_Current_HRB_P;
#if __SCC
extern	int16_t   MFC_Current_SCC_S, MFC_Current_SCC_P;
#endif
#if __MFC_L9352B
extern uint8_t  MFC_PWM_DUTY_S, MFC_PWM_DUTY_P, MFC_PWM_DUTY_ESV, MFC_PWM_DUTY_ESV_P, MFC_PWM_DUTY_ESV_S;
#endif
#if __TVBB
extern	int16_t    MFC_Current_TVBB_S, MFC_Current_TVBB_P;
#endif

#if __LVBA
extern int16_t    MFC_Current_LVBA_S, MFC_Current_LVBA_P;
#endif

/*Global Extern Functions  Declaration****************************************/
   #if __TCMF_CONTROL2
extern void TCNO_CURRENT_CONTROL(void);
   #else
extern void LAMFC_vSetMFCCurrent(void);
   #endif


#endif
#endif
