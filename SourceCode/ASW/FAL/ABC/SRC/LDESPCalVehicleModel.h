#ifndef __LDESPCALVEHICLEMODEL_H__
#define __LDESPCALVEHICLEMODEL_H__

#include "LVarHead.h"

typedef struct
{
	uint16_t vehicle_model_flg_0	:1;
	uint16_t vehicle_model_flg_1	:1;
	uint16_t vehicle_model_flg_2	:1;
	uint16_t vehicle_model_flg_3	:1;
	uint16_t vehicle_model_flg_4	:1;
	uint16_t vehicle_model_flg_5	:1;
	uint16_t vehicle_model_flg_6	:1;
	uint16_t vehicle_model_flg_7	:1;
}VEHICLE_MODEL_FLG;

extern VEHICLE_MODEL_FLG lsu8VehicleModelFlg1;



#if  __ADAPTIVE_MODEL_VELO == ENABLE
extern VEHICLE_MODEL_FLG lsu8VehicleModelFlg4, lsu8VehicleModelFlg5;
#endif


#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
extern VEHICLE_MODEL_FLG lsu8VehicleModelFlg6 , lsu8VehicleModelFlg7;
#endif

#define ldespu1ModelSteerStbOk			lsu8VehicleModelFlg1.vehicle_model_flg_0
#define ldespu1ModelUnused02			lsu8VehicleModelFlg1.vehicle_model_flg_1
#define ldespu1ModelUnused03			lsu8VehicleModelFlg1.vehicle_model_flg_2
#define ldespu1ModelUnused04			lsu8VehicleModelFlg1.vehicle_model_flg_3
#define ldespu1ModelUnused05			lsu8VehicleModelFlg1.vehicle_model_flg_4
#define ldespu1ModelUnused06			lsu8VehicleModelFlg1.vehicle_model_flg_5
#define ldespu1ModelUnused07			lsu8VehicleModelFlg1.vehicle_model_flg_6
#define ldespu1ModelUnused08			lsu8VehicleModelFlg1.vehicle_model_flg_7

#if  __ADAPTIVE_MODEL_VELO == ENABLE
#define	ldespu1AdmVeloRGoodCond         lsu8VehicleModelFlg4.vehicle_model_flg_0
#define	ldespu1AdmVeloR1SecCalLHFlg     lsu8VehicleModelFlg4.vehicle_model_flg_1
#define	ldespu1AdmVeloInhibit           lsu8VehicleModelFlg4.vehicle_model_flg_2
#define	ldespu1AdmVeloCrtApply  	    lsu8VehicleModelFlg4.vehicle_model_flg_3  
#define	ldespu1AdmVeloCrtOK		        lsu8VehicleModelFlg4.vehicle_model_flg_4 
#define	ldespu1AdmVeloR1SecCalRHFlg     lsu8VehicleModelFlg4.vehicle_model_flg_5
#define	ldespu1AdmVeloeepReadOK	        lsu8VehicleModelFlg4.vehicle_model_flg_6
#define	ldespu1AdmVeloeepWriteReq       lsu8VehicleModelFlg4.vehicle_model_flg_7

#define	ldespu1AdmVeloQuickUpdate       lsu8VehicleModelFlg5.vehicle_model_flg_0
#define	ldespu1AdmVeloWheelSpin         lsu8VehicleModelFlg5.vehicle_model_flg_1
#define	ldespu1AdmVeloCrtDuring         lsu8VehicleModelFlg5.vehicle_model_flg_2
#define	ldespu1AdmVelo1stUpdateOK       lsu8VehicleModelFlg5.vehicle_model_flg_3  
#define	ldespu1AdmVelo2ndUpdateOK       lsu8VehicleModelFlg5.vehicle_model_flg_4 
#define	ldespu1AdmVelo3rdUpdateOK       lsu8VehicleModelFlg5.vehicle_model_flg_5
#define	ldespu1AdmVeloSteerCondFlg      lsu8VehicleModelFlg5.vehicle_model_flg_6
#define	ldespu1Adm5Unused07             lsu8VehicleModelFlg5.vehicle_model_flg_7
#endif


#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
#define	ldespu1AdmVchGENGoodCond        lsu8VehicleModelFlg6.vehicle_model_flg_0
#define	ldespu1AdmVchGEN1SecCalFlg      lsu8VehicleModelFlg6.vehicle_model_flg_1
#define	ldespu1AdmVchGENInhibit         lsu8VehicleModelFlg6.vehicle_model_flg_2
#define	ldespu1AdmVchGENCrtApply  	    lsu8VehicleModelFlg6.vehicle_model_flg_3  
#define	ldespu1AdmVchGENCrtOK		    lsu8VehicleModelFlg6.vehicle_model_flg_4 
#define	ldespu1AdmVchGENeepReadOK       lsu8VehicleModelFlg6.vehicle_model_flg_5
#define	ldespu1AdmVchGENeepWriteReq     lsu8VehicleModelFlg6.vehicle_model_flg_6
#define	ldespu1AdmVchGENCrtDuring       lsu8VehicleModelFlg6.vehicle_model_flg_7

#define	ldespu1AdmVch1stUpdateOK        lsu8VehicleModelFlg7.vehicle_model_flg_0
#define	ldespu1AdmVch2ndUpdateOK        lsu8VehicleModelFlg7.vehicle_model_flg_1
#define	ldespu1AdmVch3rdUpdateOK        lsu8VehicleModelFlg7.vehicle_model_flg_2
#define	ldespu1AdmVchSteerCondOK        lsu8VehicleModelFlg7.vehicle_model_flg_3  
#define	ldespu1Adm7VchUnused04          lsu8VehicleModelFlg7.vehicle_model_flg_4 
#define	ldespu1Adm7VchUnused05          lsu8VehicleModelFlg7.vehicle_model_flg_5
#define	ldespu1Adm7VchUnused06          lsu8VehicleModelFlg7.vehicle_model_flg_6
#define	ldespu1Adm7VchUnused07          lsu8VehicleModelFlg7.vehicle_model_flg_7
#endif

extern int16_t ldesps16TireAngle;

/* GM Bank  */
#if (__GM_CPG_BANK == 1)
extern uint8_t Bank_GM_Susp;
extern int16_t ldescS16GMBankPrsRiseCount;
extern int16_t ldescS16GMBankNextRiseCount;
extern int16_t ldescS16GMBankRiseInterval;
extern int16_t ldescS16GMBankRiseNum;
#endif

#if __MODEL_CHANGE
extern int16_t ldesps16BetaDynModel;
extern int16_t ldesps16YawDynModel;
extern int16_t ldesps16DeltaDynModel;
extern int16_t ldesps16LatGDynModel;
extern int16_t ldesps16BetarateDynModel;/*모니터링용 외부사용불가*/
extern int16_t ldesps16YawRfDynModel;
extern int16_t ldesps16YawDynGain;/*모니터링용 외부사용불가*/
extern int16_t ldesps16FiltdYawDynModel;/*모니터링용 외부사용불가*/
extern int16_t ldesps16FiltdLatGDynModel;/*모니터링용 외부사용불가*/
extern int16_t ldesps16VehicleVelocity; /*모니터링용 외부사용불가*/

extern int16_t ldesps16GainBetadBeta;
extern int32_t ldesps32GainBetadYaw;
extern int16_t ldesps16GainBetadDelta;
extern int16_t ldesps16GainYawdBeta;
extern int16_t ldesps16GainYawdYaw;
extern int16_t ldesps16GainYawdDelta;
extern int16_t ldesps16ackermannyaw;
  #if __YAW_SS_MODEL_ENABLE
extern int16_t lsesps16YawSS;
  #endif
#endif
 
#if  (__ADAPTIVE_MODEL_VELO == ENABLE )
extern int16_t ldesps16AdmVcrtVeloDiff;

extern int32_t ldesps32AdmVcrtVeloCalcLHSum;   
extern int32_t ldesps32AdmVcrtDiffdotVeloLHSum;    
extern uint8_t ldespu8AdmVcrtDataLHCNT;
extern int16_t ldesps16AdmVcrtRatioLHRaw;   
extern int16_t ldesps16AdmVcrtRatioLH;              /* 1--> 0.1 %  */

extern int32_t ldesps32AdmVcrtVeloCalcRHSum;   
extern int32_t ldesps32AdmVcrtDiffdotVeloRHSum;    
extern uint8_t ldespu8AdmVcrtDataRHCNT;
extern int16_t ldesps16AdmVcrtRatioRHRaw;   
extern int16_t ldesps16AdmVcrtRatioRH;              /* 1--> 0.1 %  */

extern int16_t ldesps16AdmVcrtRatioApply;
extern int16_t ldesps16AdmVcrtRatioApplyF;

extern int16_t ldesps16AdmVestUsingYawSteer;
extern int16_t ldesps16AdmVeloRatioCalc;

extern int16_t ldesps16TireAngleVch;
               
extern int16_t ldesps16AdmVcrtEEPread;
extern int16_t ldesps16AdmVcrtRadius;
               
extern uint8_t ldespu8AdmVeloWheelSpinCNT ;
#endif



#if (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)
extern int16_t ldesps16AdmVchGEN;
extern int16_t ldesps16AdmVchGEN_F;
extern int16_t ldesps16AdmVchGENinSQRT;
extern int16_t ldesps16AdmVchApplyF;
extern uint8_t ldespu8AdmVchGEN_FChgCNT;

extern int16_t ldesps16AdmVchGEN1secLHMean;
extern int32_t ldesps32AdmVchGENCalcLHSum;
extern uint8_t ldespu8AdmVchGENDataLHCNT;

extern int16_t ldesps16AdmVchGEN1secRHMean;
extern int32_t ldesps32AdmVchGENCalcRHSum;
extern uint8_t ldespu8AdmVchGENDataRHCNT;

extern int16_t ldesps16AdmVchGENEEPread;

/* extern int16_t    ldesps16AdmVchBankYaw; */
extern int16_t ldesps16AdmVchAyComp;
extern int16_t ldesps16DetYawAccFilt;
extern int16_t ldesps16DetDelYawAccFilt;


#endif

#if  (  (__ADAPTIVE_MODEL_VELO == ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)  )

extern uint8_t u8DataCNT;  /* For Data Logging */
#endif

#if ( (__ADAPTIVE_MODEL_VELO == ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE) )
	extern struct ACC_FILTER WSTR_ACC, YAWM_ACC, DEL_YAW_ACC, YAWC_ACC;
#endif

#if  (  (__ADAPTIVE_MODEL_VELO == ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)  )
struct ADAPTIVE_M{

       int16_t ldesps16ADM_1SecCalc[10];
       int16_t ldesps16ADM_10SecCalc[10];
       int16_t ldesps16ADM_1SecCalcMAX;
       int16_t ldesps16ADM_1SecCalcMIN;
       int16_t ldesps16ADM_1SecCalcMean;         
       int16_t ldesps16ADM_10SecCalcMAX;
       int16_t ldesps16ADM_10SecCalcMIN;     
       int16_t ldesps16ADM_10SecCalcMean;
       int16_t ldesps16ADM_100SecCalcMean;
       int16_t ldesps16ADM_100SecCalcMeanOld;
       uint8_t ldespu8ADM_1SecCalcCnt;
       uint8_t ldespu8ADM_10SecCalcCnt;
       uint8_t ldespu8ADM_UpdateGradeCnt;
       
       int16_t ldesps16ADM_1secSum;  
       int16_t ldesps16ADM_100secSum;          

       uint16_t ldespu1ADM_1SecFirstOK     :1;
       uint16_t ldespu1ADM_10SecFirstOK    :1;
       uint16_t ldespu1ADM_100SecFirstOK   :1;
          
       uint16_t ldespu1ADM_10SecCalcOK     :1;	       
       uint16_t ldespu1ADM_100SecCalcOK    :1;       	

       uint16_t ldespu1ADM_10SecCheckOK    :1;
       uint16_t ldespu1ADM_10SecUpdateFlg  :1;
       uint16_t ldespu1ADM_Update1stOK     :1;	
       uint16_t ldespu1ADM_Update2ndOK     :1;
       uint16_t ldespu1ADM_Update3rdOK     :1;
}; 

extern struct  ADAPTIVE_M	*ADM, ADMvelo_LH,ADMvelo_RH , ADMvchGEN_LH, ADMvchGEN_RH ;

#endif
#if (__GM_CPG_BANK == 1)
	#define 		U8_CPG_BANK_RISE_CNT_THR			 20		/* If the 1cycle rise cnt is more than this, the num of rise cycle +1. def: 20sc */
	#define 		S16_CPG_BANK_NEXT_RISE_INTV		286		/* If the interval between last and current control is less than this, rise count contiue. def: 2sec */
	#define 		S16_CPG_BANK_CNTR_ENT_RISE_CNT	70	/* If the accumulated rise cnt more than this, control enter. */

extern void LDESP_vDetGMBank(void);
#endif
extern int16_t   LDESP_s16CalcSQRT(int16_t s16Input );
#endif
