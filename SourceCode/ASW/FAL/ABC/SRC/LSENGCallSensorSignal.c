/******************************************************************************
* Project Name: MGH-80 ESC
* File: LSENGCallSensorSignal.C
* Description: CAN Signal Conversion
* Date: Decemver. 27. 2011
******************************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSENGCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ********************************************************/


#include "hm_logic_var.h"
#include "LSENGCallSensorSignal.h"
#include "LCABSCallControl.h"
#include "LCTCSCallControl.h"
#include "LCEPBCallControl.h"
#include "LCAVHCallControl.h"
#include "LCHDCCallControl.h"
#include "LCRDCMCallControl.h"
#include "LDBDWCallDetection.h"
#include "LDESPEstDiscTemp.h"
#include "LCESPCalLpf.h"
//#include "EPB/L_InterfaceLinkData.h"

/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/
    int16_t   mtp;
    int16_t   mtp_resol1000;        
    int16_t   drive_torq;

    int16_t   max_torq;
    int16_t   drive_torq_old;
    int16_t   gs_ena;
    int16_t   gs_pos;
    int16_t   gs_sel;
    int16_t   eng_torq, eng_torq_old;
    uint16_t  eng_rpm;
    int16_t   fric_torq;
	int16_t   drive_torq_abs, eng_torq_abs, fric_torq_abs;
	int16_t   drive_torq_rel, eng_torq_rel, fric_torq_rel;
	uint16_t  tc_rpm, tc_rpm_old;	

	int16_t eng_torq_filt_old;
	int16_t eng_torq_filt;
	int16_t slope_eng_torq_old;
	int16_t slope_eng_torq;	

	int16_t cal_torq_filt_old;
	int16_t cal_torq_filt;
	int16_t slope_cal_torq_old;
	int16_t slope_cal_torq;	
	
	int16_t acc_r_filt_old;
	int16_t acc_r_filt;
	int16_t slope_acc_r_old;
	int16_t slope_acc_r;		
	
	uint8_t lctcsu8FLampCntForOff;
	uint8_t lctcsu1FLampOffByDeltaTorq;
	uint8_t lctcsu1FLampOffByTorqRate;
	uint8_t lctcsu1FLampOffByAx;
	uint8_t lctcsu1FLampOffByEstimatedBrk;
	uint8_t lctcsu1FLampOffByAccelForce;

	int16_t lscans16DrvTqSelection;

/* Local Function prototype ****************************************/

static void LSENG_vCalcEngRpmSlope(void);

static void LSCAN_vConvertEngTmSignal(void);
static void LSCAN_vConvertVafsSignal(void);
static uint16_t LSCAN_u16ConvertTqConverterRpm(void);
static uint16_t LSCAN_u16ConvertEngRpm(void);
static int16_t LSCAN_s16SelectDrvTQRawSignal(void);
static int16_t LSCAN_s16ConvertDrvTQ(void);
static int16_t LSCAN_s16ConvertEngTQ(void);
static int16_t LSCAN_s16ConvertFrcTQ(void);
static int16_t LSCAN_s16ConvertMaxTQ(void);
static int16_t LSCAN_s16ConvertMtp(void);
static int16_t LSCAN_s16ConvertGearPosition(void);
static int16_t LSCAN_s16ConvertGearChange(void);

/* Implementation***************************************************/

void LSCAN_vProcessCANSignal(void)
{
  #if !SIM_MATLAB
    LSCAN_vConvertEngTmSignal();
    LSCAN_vConvertVafsSignal();
  #endif   
}

static void LSCAN_vConvertEngTmSignal(void)
{

/* TCU Signal Convert *****************************************************************/
/* gs_sel : Driver's AT Gear selection                                                */
/*          0:P 1:L 2:2 3:3 5:D 6:N 7:R 8:sport mode/manual                           */
/* gs_pos : TM Target Gear                                                            */
/*          0:N,P 1-4:D 7:R                                                           */
/* gs_ena : TM gear change                                                            */
/*          0:no gearchange 1:gearchange is active                                    */
/**************************************************************************************/

    #if defined(__AUTO_TM_VEHICLE)
    AUTO_TM = 1;
    #else  
    AUTO_TM = ((lespu1EMS_AT_TCU==1) || (lespu1EMS_CVT_TCU==1) || (lespu1EMS_HEV_AT_TCU==1) || (lespu1EMS_DCT_TCU==1)) ? 1 : 0;
    #endif  	
    
    
    gs_sel = (int16_t)lespu8TCU_G_SEL_DISP; 
	/*  0:P 1:L 2:2 3:3 5:D 6:N 7:R 8:sport mode/manual*/
    gs_pos = LSCAN_s16ConvertGearPosition();
    /*  0:N,P 1-4:D 7:R   */
  
    gs_ena = LSCAN_s16ConvertGearChange();
    
    tc_rpm = LSCAN_u16ConvertTqConverterRpm(); 
    
/* EMS Signal Convert *****************************************************************/
#if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
		eng_torq_old = eng_torq;
	    drive_torq_old = drive_torq;
	    max_torq   = LSCAN_s16ConvertMaxTQ();
	    eng_torq   = LSCAN_s16ConvertEngTQ();
	    drive_torq = LSCAN_s16ConvertDrvTQ();
	    fric_torq  = LSCAN_s16ConvertFrcTQ();	    
	    eng_rpm    = LSCAN_u16ConvertEngRpm();		
	    mtp        = LSCAN_s16ConvertMtp();
#else
   	if((fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1))
   	{
   		drive_torq = 0;
   		eng_torq = 0;
   		fric_torq = 0;
   		max_torq = 0;
   		eng_rpm = 0;
   		mtp = 0;
   		drive_torq_old = 0;
   		eng_torq_old = 0;
	}
	else
	{
		eng_torq_old = eng_torq;
	    drive_torq_old = drive_torq;

	    max_torq   = LSCAN_s16ConvertMaxTQ();
	    eng_torq   = LSCAN_s16ConvertEngTQ();
	    drive_torq = LSCAN_s16ConvertDrvTQ();
	    fric_torq  = LSCAN_s16ConvertFrcTQ();
	    
	    eng_rpm    = LSCAN_u16ConvertEngRpm();
		
	    mtp        = LSCAN_s16ConvertMtp();
	}
#endif /*NEW_FM_ENABLE*/
}


static uint16_t LSCAN_u16ConvertTqConverterRpm(void)
{
	uint16_t u16TcRpm;
	
    #if !defined(__CAN_TC_RPM_NOT_AVAILABLE)  
    if(AUTO_TM == 1)
    {
	    u16TcRpm = lespu16TCU_N_TC;
	    
	    u16TcRpm = LCESP_u16Lpf1Int(u16TcRpm, tc_rpm, L_U8FILTER_GAIN_10MSLOOP_67HZ);
	    u16TcRpm = LCESP_u16Lpf1Int(u16TcRpm, tc_rpm, L_U8FILTER_GAIN_10MSLOOP_1HZ);
	}
	else
	{
		u16TcRpm = 0;
	}   
    #endif
		
	return u16TcRpm;
}

static uint16_t LSCAN_u16ConvertEngRpm(void)
{
	uint16_t u16EngRpm;
	
  #if !defined (__CAN_ENG_RPM_NOT_AVAILABLE)  
    u16EngRpm = (uint16_t)lesps16EMS_N;
    
   #if defined(__CAN_ENG_RPM_FILTERING)  
    u16EngRpm = LCESP_u16Lpf1Int(u16EngRpm, eng_rpm, L_U8FILTER_GAIN_10MSLOOP_67HZ);
   #endif 
   
    u16EngRpm = (uint16_t)LCABS_s16LimitMinMax( (int16_t)u16EngRpm, 0, 9000 );
   
  #endif
	
	return u16EngRpm;
}

static int16_t LSCAN_s16SelectDrvTQRawSignal(void)
{
	int16_t s16DrvTqSelection = 2; /*default value TQI-> TQI_TARGET  14.12.23*/
	
	/*
  #if __CAR == KMC_HM
	if (lespu8EMS_ENG_VOL == 46)
	{
		s16DrvTqSelection = 2;
	}
  #elif __CAR == HMC_RBK
	if ( (lespu8EMS_ENG_VOL == 16) && (lespu8EMS_ENG_CHR == 4) )
	{
		s16DrvTqSelection = 2;
	}
  #elif (__CAR == HMC_HG) || (__CAR == KMC_VG)
	if ( (lespu8EMS_ENG_VOL == 30) || (lespu8EMS_ENG_VOL == 33) )
	{
		s16DrvTqSelection = 2;
	}
  #elif __CAR == KMC_FS
	if ( (lespu8EMS_ENG_CHR == 4) && ((lespu1EMS_DCT_TCU == 1) || (lespu1EMS_CVT_TCU == 1)) )
	{
		s16DrvTqSelection = 2;
	}
  #elif (__CAR == HMC_LM) || (__CAR == KMC_SL) || (__CAR == KMC_SLZ) 
	if (lespu8EMS_ENG_VOL == 16)
	{
		s16DrvTqSelection = 2;
	}
  #elif	__CAR == HMC_MD
	if ( ((lespu8EMS_ENG_VOL == 16) && (lesps16EMS_TQ_STND == 23)) || (lespu8EMS_ENG_VOL == 20) )
	{
		s16DrvTqSelection = 2;
	}
  #elif	(__CAR == KMC_TD) || (__CAR == KMC_UB) || (__CAR == KMC_AM_FL)
	if ( (lespu8EMS_ENG_VOL == 16) && (lespu8EMS_ENG_CHR == 4) && (lesps16EMS_TQ_STND == 23) )
	{
		s16DrvTqSelection = 2;
	}
  #elif	(__CAR == HMC_DM)  
    if ((lespu8EMS_ENG_VOL == 22)
    #if (__SUSPENSION_TYPE == GENERAL)
	||( (lespu8EMS_ENG_VOL == 30) || (lespu8EMS_ENG_VOL == 33) )
	#endif
	)
	{
		s16DrvTqSelection = 2;
	}
	else
	{;}
  #elif (__CAR == HMC_DML)
    if (lespu8EMS_ENG_VOL == 22)
	{
		s16DrvTqSelection = 2;
	}
	else
	{;}
  #elif __CAR == HMC_VI
	if ( ((lespu8EMS_ENG_VOL == 38) && (lesps16EMS_TQ_STND == 48)) || ((lespu8EMS_ENG_VOL == 50) && (lesps16EMS_TQ_STND == 42)) )
	{
		s16DrvTqSelection = 2;
	}
  #elif __CAR == GM_LAMBDA
		s16DrvTqSelection = 2;    
  
  #elif __CAR == HMC_GC
   	s16DrvTqSelection = 2;
   	
  #elif __CAR == HMC_DH
		s16DrvTqSelection = 2;	
  #elif __CAR == KMC_PF
  		s16DrvTqSelection = 2;		
  #elif	__CAR == KMC_KH
	if ( (lespu8EMS_ENG_VOL == 50) && (lesps16EMS_TQ_STND == 42) )
	{
		s16DrvTqSelection = 2;
	}		
	else
	{;}		
	#elif	__CAR == HMC_TL
	if ( (lespu8EMS_ENG_VOL == 20) && (lesps16EMS_TQ_STND == 28) )
	{
		s16DrvTqSelection = 2;
	}
	else
	{;}	
     if ( (lespu8EMS_ENG_VOL == 17) && (lesps16EMS_TQ_STND == 40) )
	{
		s16DrvTqSelection = 2;
	}
	else
	{;}	
     if ( (lespu8EMS_ENG_VOL == 16) && (lesps16EMS_TQ_STND == 35) )
	{
		s16DrvTqSelection = 2;
	}
	else
	{;}	
     if ( (lespu8EMS_ENG_VOL == 16) && (lesps16EMS_TQ_STND == 23) )
	{
		s16DrvTqSelection = 2;
	}
	else
	{
		;
	}
  #endif	*/
    
    return s16DrvTqSelection;
}
	
static int16_t LSCAN_s16ConvertDrvTQ(void)
{
	int16_t s16DrvTq;
	int16_t s16DrvTqAbsOld;
	
  #if defined(__CAN_HYBRID_VEHICLE_TQ)
    if(lespu8HCU_EngCltStat==0)
    {
		s16DrvTq = lesps16HCU_MotTqCmdBInv_Pc + lesps16EMS_TQFR;
    }
    else
    {
    	s16DrvTq = lesps16HCU_MotTqCmdBInv_Pc + lesps16HCU_EngTqCmdBInv_Pc;
    }
  #elif defined(__CAN_HYBRID_VEHICLE_TQ_NON_AHB)  
  	s16DrvTq = lesps16HCU_EngTqCmdBInv_Pc;
  #elif defined(__CAN_ELECTRIC_VEHICLE_TQ)
    if(gs_sel==7) /* Reverse Gear : Motor Torque is negative value */
    {
   		s16DrvTq = -lesps16EMS_TQI;                       /* driver engine torque, 100%=255 */
   	}
   	else
   	{		
   		s16DrvTq = lesps16EMS_TQI;                        /* driver engine torque, 100%=255 */
   	}	
  #elif defined(__CAN_DRV_TQ_SELECTION)
    if ((lscans16DrvTqSelection==0) || (speed_calc_timer < L_U8_TIME_10MSLOOP_1000MS))
    {
		lscans16DrvTqSelection = LSCAN_s16SelectDrvTQRawSignal();
	}
	
	if(lscans16DrvTqSelection == 1)
	{
		s16DrvTq = lesps16EMS_TQI;
	}
	else if(lscans16DrvTqSelection == 2)
	{
		s16DrvTq = lesps16EMS6_TQI_TARGET;
	}
	else
	{
		s16DrvTq = lesps16EMS_TQI;
	}	
  #else	
	
	#if __CAR==VV_V40
	lesps16TCU_CUR_G_RATIO = LCTCS_s16ILimitMinimum(lesps16TCU_CUR_G_RATIO, 1);
	s16DrvTq = (int16_t)(((int32_t)lesps16EMS_TQI*100)/lesps16TCU_CUR_G_RATIO);
	#else
	s16DrvTq = lesps16EMS_TQI;
  #endif  	
	
  #endif  	
	
  #if (__TCS_CAN_EMS_TORQ_DEFAULT_REL == 1)
    drive_torq_rel = s16DrvTq;
    drive_torq_abs = (int16_t)(((int32_t)s16DrvTq*max_torq) / 10);
  #else
    drive_torq_abs = s16DrvTq;
    drive_torq_rel = (int16_t)(((int32_t)s16DrvTq*10) / max_torq);  
  #endif

  #if (__USE_ABS_TORQ == 1)
	s16DrvTq = drive_torq_abs;
  #else
    s16DrvTq = drive_torq_rel;
  #endif  
	
  #if defined(__CAN_HYBRID_VEHICLE_TQ_NON_AHB)  
  	s16DrvTq = lesps16HCU_MotTqCmdBInv_Pc + s16DrvTq;
  #endif		
	
  #if defined(__CAN_DRV_TQ_FILTERING)	
    if(McrAbs(drive_torq-s16DrvTq)>200)
    {
    	s16DrvTq = LCESP_s16Lpf1Int(s16DrvTq, drive_torq, L_U8FILTER_GAIN_10MSLOOP_67HZ); /* 7ms loop 1:3 filtering */
    }
    else
    {
    	;
    }
  #endif
    
	return s16DrvTq;
}


static int16_t LSCAN_s16ConvertEngTQ(void)
{
	int16_t s16EngTq;
	
  #if defined(__CAN_HYBRID_VEHICLE_TQ)
    if(lespu8HCU_EngCltStat==0)
    {
		s16EngTq = lesps16MCU_CR_Mot_EstTq_Pc + lesps16EMS_TQFR; 
    }
    else
    {
    	s16EngTq = lesps16MCU_CR_Mot_EstTq_Pc + lesps16EMS_ActIndTq_Pc;
    }					
  #elif defined(__CAN_HYBRID_VEHICLE_TQ_NON_AHB)
    s16EngTq = lesps16EMS_ActIndTq_Pc;
  #elif defined(__CAN_ELECTRIC_VEHICLE_TQ)
    if(gs_sel==7) /* Reverse Gear : Motor Torque is negative value */
    {
   		s16EngTq = -lesps16EMS_TQI_ACOR;                        /* real engine torque */
   	}
   	else
   	{		
   		s16EngTq = lesps16EMS_TQI_ACOR;                        /* real engine torque */
   	}	
  #else
	
	
	#if __CAR==VV_V40
	lesps16TCU_CUR_G_RATIO = LCTCS_s16ILimitMinimum(lesps16TCU_CUR_G_RATIO, 1);	
	s16EngTq = (int16_t)(((int32_t)lesps16EMS_TQI_ACOR*100)/lesps16TCU_CUR_G_RATIO);
	#else
	s16EngTq = lesps16EMS_TQI_ACOR;
  #endif
	
  	
  #endif
	
  #if (__TCS_CAN_EMS_TORQ_DEFAULT_REL == 1)
    eng_torq_rel = s16EngTq;
    eng_torq_abs = (int16_t)(((int32_t)s16EngTq*max_torq) / 10);
  #else
    eng_torq_abs = s16EngTq;
    eng_torq_rel = (int16_t)(((int32_t)s16EngTq*10) / max_torq);  
  #endif
	
  #if (__USE_ABS_TORQ == 1)
	s16EngTq = eng_torq_abs;
  #else
    s16EngTq = eng_torq_rel;
  #endif  
	
  #if defined(__CAN_HYBRID_VEHICLE_TQ_NON_AHB)
    s16EngTq = lesps16MCU_CR_Mot_EstTq_Pc + s16EngTq;
  #endif  
	
	return s16EngTq;
}

static int16_t LSCAN_s16ConvertFrcTQ(void)
{
	int16_t s16FrcTq;
	
  #if defined (__CAN_FRC_TQ_SELECTION)
    if(__DIESEL_VEHICLE)
    {
	    s16FrcTq = lesps16EMS_TQFR_C200_DSL;
	}
	else
	{
		s16FrcTq = lesps16EMS_TQFR_C200_GSL;
	}
  #else	
	
	#if __CAR==VV_V40
	lesps16TCU_CUR_G_RATIO = LCTCS_s16ILimitMinimum(lesps16TCU_CUR_G_RATIO, 1);	
	s16FrcTq = (int16_t)(((int32_t)lesps16EMS_TQFR*100)/lesps16TCU_CUR_G_RATIO);	
	#else
	s16FrcTq = lesps16EMS_TQFR;
  #endif
  	
  #endif
  	
  #if (__TCS_CAN_EMS_TORQ_DEFAULT_REL == 1)
    fric_torq_rel = s16FrcTq;
    fric_torq_abs = (int16_t)(((int32_t)s16FrcTq*max_torq) / 10);
  #else
    fric_torq_abs = s16FrcTq;
    fric_torq_rel = (int16_t)(((int32_t)s16FrcTq*10) / max_torq);  
  #endif
	
  #if (__USE_ABS_TORQ == 1)
	s16FrcTq = fric_torq_abs;
  #else
    s16FrcTq = fric_torq_rel;
  #endif  
	
	return s16FrcTq;
}

static int16_t LSCAN_s16ConvertMaxTQ(void)
{
	int16_t s16MaxTq;
	
  #if defined(__CAN_MAX_TQ_AVAILABLE)
  
   #if defined(__CAN_MAX_TQ_SCALING)
    if((lespu8EMS_ENG_VOL>= U8_HIGH_POWER_ENG_VOL)
    	||((lespu8EMS_ENG_VOL == 33)&&(lesps16EMS_TQ_STND == 42)))
    {
	    s16MaxTq = (lesps16EMS_TQ_STND*15)/10;
	}
	else
	{
		s16MaxTq = lesps16EMS_TQ_STND;
	}
   #else
    s16MaxTq = lesps16EMS_TQ_STND;
   #endif 
   
  #else
  
 	#if __TCS 
    s16MaxTq = LCABS_s16LimitMinMax(S16_TCS_ENG_TORQUE_MAX, 100, MAX_TORQUE);
 	#else
    s16MaxTq = LCABS_s16LimitMinMax(3000, 100, MAX_TORQUE); 
    #endif
    
    s16MaxTq = (s16MaxTq/100);
    
  #endif
  
    return s16MaxTq;
}

static int16_t LSCAN_s16ConvertMtp(void)
{
	int16_t s16Mtp, s16MtpRs1000;

  #if defined(__CAN_ACCEL_PDL_TRAVL_AVAILABLE) && defined(__CAN_THROTTLE_ANGLE_AVAILABLE)
  
    s16Mtp = lesps16EMS_PV_AV_CAN;
    s16MtpRs1000 = lesps16EMS_PV_AV_CAN_Resol1000;
    if((lespu8EMS_ENG_CHR <= 3) && (lespu1EMS_TPS_ERR == 0))
    {
    	s16Mtp = lesps16EMS_TPS;
    	s16MtpRs1000 = lesps16EMS_TPS_Resol1000;
    }
    
  #elif defined(__CAN_THROTTLE_ANGLE_AVAILABLE)
  
    s16Mtp = lesps16EMS_TPS;
    s16MtpRs1000 = lesps16EMS_TPS_Resol1000;
    
  #else
  
    s16Mtp = lesps16EMS_PV_AV_CAN;
    s16MtpRs1000 = lesps16EMS_PV_AV_CAN_Resol1000;
    
  #endif
  
  #if defined(__CAN_MTP_FILTERING)
  	s16Mtp = LCESP_s16Lpf1Int(s16Mtp, mtp, L_U8FILTER_GAIN_10MSLOOP_67HZ);
  #endif
  	
  	s16Mtp = LCABS_s16LimitMinMax(s16Mtp, 0, 100);
 	mtp_resol1000 = s16MtpRs1000;   	
  	
	return s16Mtp;	
}

static int16_t LSCAN_s16ConvertGearPosition(void)
{
	int16_t s16GsPos;

  #if defined (__CAN_TM_GEAR_POS_NOT_AVAILABLE)
  	if ((gs_sel == 0) || (gs_sel == 6))
  	{
  		s16GsPos = 0;
  	}
  	else if (gs_sel == 7)
  	{
  		s16GsPos = 7;
  	}
  	else
  	{
  		s16GsPos = gs_sel;
  	}
  #else 
    s16GsPos = (int16_t)lespu8TCU_TAR_GC;
    
    #if __CAR_MAKER==SSANGYONG
    if((lespu8TCU_TAR_GC==10)||(lespu8TCU_TAR_GC==11)||(lespu8TCU_TAR_GC==12)) /*target Gear GZC_218h   10=R   11=R2 12=R3 */
    {
    	s16GsPos = 11;
    }
    else
    {
    	;
    }
    #endif
    
  #endif	
	
	return s16GsPos;
}

static int16_t LSCAN_s16ConvertGearChange(void)
{
	int16_t s16GsEna;

  /*  0:no gearchange 1:gearchange is active*/
  
  #if defined (__CAN_TM_GEAR_SW_NOT_AVAILABLE)
  	s16GsEna = (lespu8TCU_CUR_GC == lespu8TCU_TAR_GC)? 0 : 1;
  #else 
    s16GsEna = (int16_t)lespu1TCU_SWI_GS;
  #endif	
	
	return s16GsEna;
}

static void LSCAN_vConvertVafsSignal(void)
{
	
  #if __LVBA
	if(lespu1EMS_TEMP_ENG_ERR==0)
	{
		eng_temp = lesps16EMS_TEMP_ENG; /*[r:0.01'C] */
	}
	else
	{
		;
	}
  #endif
  
  #if (__EPB_INTERFACE) || (__AVH)
	/* Rx Signal For EPBi, AVH */
	  #if __MGH80_MOCi==ENABLE
      	lcu8epbDynamicReq	= lcu1epbDynamicReq;				/* EPB dynamic braking request */	  
		lcu8epbStatusEPB	= lcu8EPBStatus;				    /* force sataus of EPB */	
	  	lcu8epbReqDecel		= (uint8_t)((lcu16epbReqDecel)/10);	/* requested deceleration for DBF */		
	  #else
	lcu8epbDynamicReq = lcanu8EPB_DBF_REQ;				/* EPB dynamic braking request */
		lcu8epbStatusEPB	= lcanu8EPB_STATUS;				    /* force sataus of EPB */	
	  	lcu8epbReqDecel		= lcanu8EPB_DBF_DECEL;				/* requested deceleration for DBF */		
	  #endif
	lcu8epbFail = lcanu8EPB_FAIL;						/* information about the availabilty of EPB */


  #endif	/* #if (__EPB_INTERFACE) || (__AVH) */
  
  #if __RDCM_PROTECTION  
	lts16TargetRPMForRDCM = (int16_t)lespu8RDCM_SAMDVLReqVal;	/* Target Delta RPM for RDCM Protection */
	ltu1RDCMProtectionReq = lespu1RDCM_SAMDVLAtv;		/* RDCM Protection Request */
	ltu8RDCMStatus = lespu8RDCM_SecAxlStat;				/* RDCM Status ; 0 : normal 1~3 : abnormal */
  #endif    
  
  #if __HDC   
  	/*lcu1HdcSwFlgByCan = lcanu1HDC_HillDesCtrlMdSwAct;     // HDC Switch OnOff */
  	/*lcu1HdcSwFlgByCanInvalid = lcanu1HDC_HillDesCtrlMdSwActV; //HDC Switch OnOff Signal Valid*/
  #endif  
  
  #if __TSP
	ldtspu8TrlrHtchSwAtv = lespu8TSP_TrlrHtchSwAtv;
	ldtspu1TrlrHtchSwAtvValid = lespu1TSP_TrlrHtchSwAtvValid;
  #endif    
  
  #if __BDW
	lsbdwu1WiperIntSW  = lespu1CF_Clu_WiperIntSW;
	lsbdwu1WiperLow = lespu1CF_Clu_WiperLow;
	lsbdwu1WiperHigh = lespu1CF_Clu_WiperHigh;
	lsbdwu1Clu2WiperMsgRxOkFlg = lespu1CF_Clu_WiperValidFlg;
	lsbdwu1CluWiperMsgInvalid = lu1CF_Clu_WiperInvalidFlg; 
	lsbdwu1WiperAuto = lespu1CF_Clu_WiperAuto;
	lsbdwu8RainSnsStat = lespu8CF_Clu_RainSnsStat;
  #endif    
  
  #if __GM_VEHICLE
  	ldespu8OutsideAirTemptM  = lcanu1OAT_PT_EstM;
  	ldespu8OutsideAirTemptV  = lcanu1OAT_PT_EstV;
  	ldesps16OutsideAirTempt  = lcans8OAT_PT_Est*32;
    
	#if ( (__Ax_OFFSET_MON==1) || (__Ax_OFFSET_MON_DRV==1 ) )	
  	lsespu1WiperActFlg = lcanu1WSWprAct; /*For Long accel offset calibration*/
  	#endif
  #endif  /* __GM_VEHICLE */  
  	
}


void LSENG_vCallSensorSignal(void)
{
	LSENG_vCalcEngRpmSlope();		
}



static void LSENG_vCalcEngRpmSlope(void)
{

  #if __TCS || __ETC || __EDC
    /* eng_rpm filtering. (041116) ---------------------------------*/
    eng_rpm_filt_old = eng_rpm_filt;
    eng_rpm_filt=(int16_t)eng_rpm;
		#if !SIM_MATLAB
    eng_rpm_filt = LCESP_s16Lpf1Int(eng_rpm_filt, eng_rpm_filt_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
    	#else
	eng_rpm_filt = LCESP_s16Lpf1Int(eng_rpm_filt, eng_rpm_filt_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
		#endif
    /* -------------------------------------------------------------*/

    /* eng_rpm slope calculation. (041116) -------------------------*/
    slope_eng_rpm_old = slope_eng_rpm;    	
    slope_eng_rpm = eng_rpm_filt - eng_rpm_filt_old;   
    slope_eng_rpm = LCESP_s16Lpf1Int(slope_eng_rpm, slope_eng_rpm_old, L_U8FILTER_GAIN_10MSLOOP_18HZ);
    /* -------------------------------------------------------------*/
      #if __EDC_GEAR_SHIFT_DOWN_DETECTION	
    eng_rpm_filt_old_for_GSD = eng_rpm_filt_for_GSD;
    eng_rpm_filt_for_GSD=eng_rpm;
    eng_rpm_filt_for_GSD = LCESP_s16Lpf1Int(eng_rpm_filt_for_GSD, eng_rpm_filt_old_for_GSD, L_U8FILTER_GAIN_10MSLOOP_29HZ);   
             
    slope_eng_rpm_old_for_GSD = slope_eng_rpm_for_GSD;    	
    slope_eng_rpm_for_GSD = eng_rpm_filt_for_GSD - eng_rpm_filt_old_for_GSD;   
    slope_eng_rpm_for_GSD = LCESP_s16Lpf1Int(slope_eng_rpm_for_GSD, slope_eng_rpm_old_for_GSD, L_U8FILTER_GAIN_10MSLOOP_54HZ);
      #endif		

    eng_torq_filt_old = eng_torq_filt;
    eng_torq_filt=(int16_t)eng_torq;

    eng_torq_filt = LCESP_s16Lpf1Int(eng_torq_filt, eng_torq_filt_old, L_U8FILTER_GAIN_10MSLOOP_5HZ);

    slope_eng_torq_old = slope_eng_torq;    	
    slope_eng_torq = eng_torq_filt - eng_torq_filt_old;   
    slope_eng_torq = LCESP_s16Lpf1Int(slope_eng_torq, slope_eng_torq_old, L_U8FILTER_GAIN_10MSLOOP_5HZ);    
    
  #endif    

}


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSENGCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
