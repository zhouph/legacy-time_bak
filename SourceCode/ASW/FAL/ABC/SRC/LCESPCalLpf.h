#ifndef __LCESPCALLPF_H__
#define __LCESPCALLPF_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/
extern int16_t LCESP_s16Lpf1Int(int16_t x_input, int16_t x_old, uint8_t input_gain);
extern uint16_t LCESP_u16Lpf1Int(uint16_t x_input, uint16_t x_old, uint8_t input_gain);
extern int16_t LCESP_s16Lpf1Int1024(int16_t x_input, int16_t x_old, uint8_t input_gain);

#endif
