/******************************************************************************
* Project Name: BRAKE DISC WIPING
* File: LCBDWCallControl.C
* Description:
* Date: Nov. 30. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCBDWCallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/


#include "LDBDWCallDetection.h"
#include "LCBDWCallControl.h"
#include "LAABSCallMotorSpeedControl.h"


/* Logcal Definiton  *********************************************************/
#define     __BDW_ACTIVE_ALL_WHEEL      0


#if __BDW

/* Variables Definition*******************************************************/
struct  U8_BIT_STRUCT_t BDWC0;
struct  U8_BIT_STRUCT_t BDWC1;
struct  U8_BIT_STRUCT_t BDWC2;
struct  U8_BIT_STRUCT_t BDWC3;
struct  U8_BIT_STRUCT_t BDWC4;

 
static int16_t bdw_tempC9,bdw_tempC8,bdw_tempC7,bdw_tempC6,bdw_tempC5;
static int16_t bdw_tempC4,bdw_tempC3,bdw_tempC2,bdw_tempC1,bdw_tempC0;
static int16_t BDW_mode_cnt;
static int16_t BDW_Control_mode;
static int16_t BDW_MSC_cnt;

int16_t bdw_active_counter;
 



/* LocalFunction prototype ***************************************************/
void    LCBDW_vCallControl(void);
void    LCBDW_vCallClearValveMotor(void);
void    LCBDW_vCallControlValve(void);
void    LCBDW_vCallControlMotor(void);

/* Implementation*************************************************************/


void    LCBDW_vCallControl(void)
{
    /*
    On;Off Time set
  bdw_tempC9 = T_10S;       Total period Time SET
  bdw_tempC8 = T_3S;        On Time SET
  bdw_tempC7 = T_200MS;     state 1 Time set
    */
    if (DISC_CLEAN_NEED==1)
    {
        bdw_tempC9 = bdw_contorl_priod/10;
        bdw_tempC8 = bdw_active_time/2;
        bdw_tempC7 = L_U8_TIME_10MSLOOP_200MS; //T_200MS;
        bdw_tempC6 = bdw_tempC9 - bdw_tempC8;
    }
    else
    {
        bdw_tempC9 = bdw_contorl_priod;
        bdw_tempC8 = bdw_active_time;
        bdw_tempC7 = L_U8_TIME_10MSLOOP_200MS; //T_200MS;
        bdw_tempC6 = bdw_tempC9 - bdw_tempC8;
    }

    if (BDW_ACT_CONDTION==1)
    {
        if (BDW_mode_cnt < 0)
        {
            BDW_mode_cnt=0;
        }
        else
        {
            BDW_mode_cnt++;
        }

        switch (BDW_Control_mode)
        {
            case 1:
                if (BDW_mode_cnt >= bdw_tempC8)
                {
                    BDW_Control_mode    = 2 ;
                    BDW_mode_cnt        = 0 ;
                }
                else { }

            break ;

            case 2:
                if (BDW_mode_cnt >= bdw_tempC6)
                {
                    BDW_Control_mode    = 3 ;
                    BDW_mode_cnt        = 0 ;
                }
                else { }

            break ;

            case 3:
                if (BDW_mode_cnt >= bdw_tempC8)
                {
                    BDW_Control_mode    = 4 ;
                    BDW_mode_cnt        = 0 ;
                    if (bdw_active_counter < 30000)
                    {
                        bdw_active_counter  = bdw_active_counter + 1;
                    }
                    else { }
                }
                else { }

            break ;

            case 4:
                if (BDW_mode_cnt >= bdw_tempC6)
                {
                    BDW_Control_mode    = 1 ;
                    BDW_mode_cnt        = 0 ;
                }
                else { }

            break ;

            default:
                BDW_Control_mode    = 1 ;
                BDW_mode_cnt        = 0 ;
            break ;

        }
      #if __BDW_ACTIVE_ALL_WHEEL ==1
        switch (BDW_Control_mode)
        {
            case 1:
                BDW_FL_ON_FLG = 1;
                BDW_FR_ON_FLG = 1;
                BDW_RL_ON_FLG = 1;
                BDW_RR_ON_FLG = 1;
            break ;

            case 2:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

            case 3:
                BDW_FL_ON_FLG = 1;
                BDW_FR_ON_FLG = 1;
                BDW_RL_ON_FLG = 1;
                BDW_RR_ON_FLG = 1;
            break ;

            case 4:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

            default:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;
        }
      #else
        switch (BDW_Control_mode)
        {
          #if __SPLIT_TYPE      /* FR Split */
            case 1:
                BDW_FL_ON_FLG = 1;
                BDW_FR_ON_FLG = 1;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

            case 2:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

            case 3:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 1;
                BDW_RR_ON_FLG = 1;
            break ;

            case 4:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

          #else                         /* X Split  */

            case 1:
                BDW_FL_ON_FLG = 1;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 1;
            break ;

            case 2:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

            case 3:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 1;
                BDW_RL_ON_FLG = 1;
                BDW_RR_ON_FLG = 0;
            break ;

            case 4:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;

          #endif                    /*  ~__SPLIT_TYPE   */

            default:
                BDW_FL_ON_FLG = 0;
                BDW_FR_ON_FLG = 0;
                BDW_RL_ON_FLG = 0;
                BDW_RR_ON_FLG = 0;
            break ;
        }
      #endif
    }
    else
    {
        /*  비작동 조건         */
        BDW_FL_ON_FLG=0;
        BDW_FR_ON_FLG=0;
        BDW_RL_ON_FLG=0;
        BDW_RR_ON_FLG=0;
        BDW_mode_cnt=0;
    }


    if ((BDW_FL_ON_FLG==1)||(BDW_FR_ON_FLG==1)||(BDW_RL_ON_FLG==1)||(BDW_RR_ON_FLG==1))
    {
        BDW_ON=1;
    }
    else
    {
        BDW_ON=0;
    }

    LCBDW_vCallClearValveMotor();

    LCBDW_vCallControlValve();

    LCBDW_vCallControlMotor();

}

void LCBDW_vCallClearValveMotor(void){

    BDW_HV_VL_fl=0;
    BDW_AV_VL_fl=0;
    BDW_HV_VL_fr=0;
    BDW_AV_VL_fr=0;
    BDW_HV_VL_rl=0;
    BDW_AV_VL_rl=0;
    BDW_HV_VL_rr=0;
    BDW_AV_VL_rr=0;

    BDW_S_VALVE_PRIMARY     =0;
    BDW_S_VALVE_SECONDARY   =0;
    BDW_TCL_DEMAND_PRIMARY  =0;
    BDW_TCL_DEMAND_SECONDARY=0;

    BDW_MOTOR_ON=0;
}

void LCBDW_vCallControlValve(void){

    bdw_tempC9=0;
    if (BDW_FL_ON_FLG==1)
    {
        bdw_tempC9++;
    }
    else  { }

    if (BDW_FR_ON_FLG==1)
    {
        bdw_tempC9++;
    }
    else { }

    if (BDW_RL_ON_FLG==1)
    {
        bdw_tempC9++;
    }
    else { }

    if (BDW_RR_ON_FLG==1)
    {
        bdw_tempC9++;
    }
    else { }


    if (bdw_tempC9==1)
    {
        if (BDW_FL_ON_FLG==1)
        {
          #if __SPLIT_TYPE
            BDW_AV_VL_fl=1;
            BDW_HV_VL_fr=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
          #else
            BDW_AV_VL_fl=1;
            BDW_HV_VL_rr=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
          #endif
        }
        else { }

        if (BDW_FR_ON_FLG==1)
        {
          #if __SPLIT_TYPE
            BDW_AV_VL_fr=1;
            BDW_HV_VL_fl=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
          #else
            BDW_AV_VL_fr=1;
            BDW_HV_VL_rl=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
          #endif
        }
        else { }

        if (BDW_RL_ON_FLG==1)
        {
          #if __SPLIT_TYPE
            BDW_AV_VL_rl=1;
            BDW_HV_VL_rr=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
          #else
            BDW_AV_VL_rl=1;
            BDW_HV_VL_fr=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
          #endif
        }
        else { }

        if (BDW_RR_ON_FLG==1)
        {
          #if __SPLIT_TYPE
            BDW_AV_VL_rr=1;
            BDW_HV_VL_rl=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
          #else
            BDW_AV_VL_rr=1;
            BDW_HV_VL_fl=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
          #endif
        }
        else { }

    }
    else if (bdw_tempC9==2)
    {
      #if __SPLIT_TYPE                          /* FR Split */
        if  ((BDW_FL_ON_FLG==1)&&(BDW_FR_ON_FLG==1))
        {
            BDW_AV_VL_fr=1;
            BDW_AV_VL_fl=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
        }
        else { }
        if ((BDW_RL_ON_FLG==1)&&(BDW_RR_ON_FLG==1))
        {
            BDW_AV_VL_rl=1;
            BDW_AV_VL_rr=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
        }
        else { }
      #else                                         /* X Split */
        if ((BDW_FL_ON_FLG==1)&&(BDW_RR_ON_FLG==1))
        {
            BDW_AV_VL_fl=1;
            BDW_AV_VL_rr=1;
            BDW_S_VALVE_SECONDARY=1;
            BDW_TCL_DEMAND_SECONDARY=1;
        }
        else { }
        if  ((BDW_FR_ON_FLG==1)&&(BDW_RL_ON_FLG==1))
        {
            BDW_AV_VL_fr=1;
            BDW_AV_VL_rl=1;
            BDW_S_VALVE_PRIMARY=1;
            BDW_TCL_DEMAND_PRIMARY=1;
        }
        else { }
      #endif
    }
    else if (bdw_tempC9==4)
    {
        BDW_AV_VL_fl=1;
        BDW_AV_VL_fr=1;
        BDW_AV_VL_rr=1;
        BDW_AV_VL_rl=1;
        BDW_S_VALVE_SECONDARY=1;
        BDW_S_VALVE_PRIMARY=1;
        BDW_TCL_DEMAND_SECONDARY=1;
        BDW_TCL_DEMAND_PRIMARY=1;
    }
    else { }

}

void LCBDW_vCallControlMotor(void){

/*
motor Duty state1 SET    1/6%    1/4 부터
min valve
max valve
MSC_3_V;
*/
    bdw_tempC7=4;//6
    bdw_tempC6=7;//10
    bdw_tempC5= MSC_3_V;


    if (BDW_ON==1)
    {
        BDW_MSC_cnt++;
        if (((mot_mon_ad<bdw_tempC5)&&(BDW_MSC_cnt>bdw_tempC7))||(BDW_MSC_cnt>bdw_tempC6))
        {
            BDW_MOTOR_ON=1;
            BDW_MSC_cnt=0;
        }
        else
        {
            BDW_MOTOR_ON=0;
        }
    }
    else
    {
        BDW_MSC_cnt=0;
        BDW_MOTOR_ON=0;
    }
}
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCBDWCallControl
	#include "Mdyn_autosar.h"
#endif
