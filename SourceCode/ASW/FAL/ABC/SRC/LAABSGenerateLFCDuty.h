#ifndef __LAABSGENERATELFCDUTY_H__
#define __LAABSGENERATELFCDUTY_H__

/*includes********************************************************************/
#include "LVarHead.h"
//#include "LCABSDecideCtrlState.h"

#if (__IDB_LOGIC == DISABLE)/*Refer to EOL*/
/*Global MACRO CONSTANT Definition********************************************/
//temp
#define	U8_Initial_Duty_Ratio_HDC_MIN	15
#define	U8_Initial_Duty_Ratio_HDC_MAX	50

#define U8_DEFAULT_DUMPSCANTIME   U8_BASE_LOOPTIME
#define U8_DEFAULT_RISESCANTIME   U8_BASE_CTRLTIME

  #if (__CAR==GM_M300) || (__CAR==GM_T300) || (__CAR==GM_GSUV) || (__CAR==GM_M350)
#define __INCREASE_SP_LOW_FRONT_DUMPTIM     ENABLE
  #else
#define __INCREASE_SP_LOW_FRONT_DUMPTIM     DISABLE
  #endif
#define __INCREASE_SP_LOW_REAR_DUMPTIM      ENABLE

#define __HEV_MP_MOVING_AVG   				DISABLE

#define __FRONT_REAR_MP_COMP_SEPERATE		ENABLE

#define	__HOLD_LFC_DUTY_CONTROL				DISABLE

#define __LOW_DP_COMP						DISABLE /* MGH-60 : below dP 40bar:6duty/10bar, above dP 40bar: 4duty/10bar */

#define __CURRENT_CONTROL_IC    			ENABLE /*DISABLE: pwm control, ENABLE: current control*/
#define __COIL_RESISTANCE_MONITOR			DISABLE
#define __MGH80_LFC_PATTERN_CHANGE			DISABLE /*enable: duty -2 during LFC, disable: duty -1 during LFC*/

/*Logic Parameter ************************************************************/

#if (__LOW_DP_COMP==ENABLE)  
  #if (__CAR==GM_LAMBDA)||(__CAR==BMW740i)
#define S16_LOW_DP_COMP_REF  			MPRESS_30BAR /* MGH-60 : MPRESS_40BAR */
#define U8_LOW_DP_COMP_DUTY  			35 /* MGH-60 : 8 */
#define S16_PARTIAL_BRAKE_DETECT_TH		MPRESS_80BAR
  #else
#define S16_LOW_DP_COMP_REF  			MPRESS_40BAR
#define U8_LOW_DP_COMP_DUTY  			5
#define S16_PARTIAL_BRAKE_DETECT_TH		MPRESS_200BAR
  #endif
#endif
  
  #if (__CAR==GM_M300) && (__ECU==ABS_ECU_1)
#define SP_LOW_FRT_DUMPTIM_AFZ           U8_PULSE_LOW_MU
  #else
#define SP_LOW_FRT_DUMPTIM_AFZ           U8_SP_LOW_FRT_DUMPTIM_AFZ
  #endif

  #if (__CAR==KMC_HM)
#define S8_MaxSlipForIncFrontDumpT_RWD  LAM_25P
#define U8_DumpRefForIncFrontDumpT_RWD  2
  #else
#define S8_MaxSlipForIncFrontDumpT_RWD  LAM_25P
#define U8_DumpRefForIncFrontDumpT_RWD  5
  #endif

 /* DP Duty Compensation Parameters */
  #if (__CAR == GM_TAHOE)|| (__CAR ==GM_SILVERADO)
#define S16_ReferenceMCPress        MPRESS_130BAR
  #else
#define S16_ReferenceMCPress        MPRESS_150BAR
  #endif

#define U8_BIG_RISE_DMP_COMP_DUTY	7
#define	U8_LFC_2ND_CYC_COMP_LOW		6

  #if (__HOLD_LFC_DUTY_CONTROL == ENABLE)
#define	U8_MIN_HOLD_DUTY			145
  #endif

  #if (__AHB_GEN3_SYSTEM == ENABLE)
#define S8_DPRISE_COMP_DUTY         15/* premium:15, MGH-80:20 */
#define S8_DPRISE_FORCED_HOLD_DUTY  25/* premium:20, MGH-80:30 */
#define MAX_MP_COMP_DUTY            5 /* orifice 0.9 */
  #elif (__IDB_LOGIC == ENABLE)
//#define S8_DPRISE_COMP_DUTY         15/* premium:15, MGH-80:20 */
//#define S8_DPRISE_FORCED_HOLD_DUTY  25/* premium:20, MGH-80:30 */
#define MAX_MP_COMP_DUTY            5 /* orifice 0.9 */  
  #else
#define S8_DPRISE_COMP_DUTY         20/* premium:15, MGH-80:20 */
#define S8_DPRISE_FORCED_HOLD_DUTY  30/* premium:20, MGH-80:30 */
#define MAX_MP_COMP_DUTY    		40/* MGH-80 */
  #endif


#define S8_ADDON_TIME_FOR_LOW_DECEL 7

  #if __SLIGHT_BRAKING_COMPENSATION
#define S8_ADDON_TIME_FOR_LOW_MP_F  5
#define S8_ADDON_TIME_FOR_LOW_MP_R  4
  #endif

#define U8_STABDUMP_SCANTIME_F  7
#define	U8_INIT_DUTY_ACTIVE_BRK_MIN		U8_Initial_Duty_Ratio_HDC_MIN /* MGH-80: MGH-60: 15 */
#define U8_INIT_DUTY_ACTIVE_BRK_MAX		U8_Initial_Duty_Ratio_HDC_MAX /* MGH-80: MGH-60: 50 */

#define U8_EBD_LFC_INITIAL_DUTY 		70 /* MGH-60 : 70 */
  
/*Global Type Declaration ****************************************************/


/*Global Extern Variable  Declaration*****************************************/
extern uint8_t pwm_flg;				/* From F/W V 4.24, F/S V ESP 2.8 */

extern int16_t	Filtered_ref_mon_ad;
extern int16_t	laabss16MPAvg;

/*Global Extern Functions  Declaration****************************************/
extern void LAABS_vGenerateLFCDuty(void);
extern void LAABS_vAllocPWMDutyArray(void);

#endif/*#if (__IDB_LOGIC == DISABLE)*/
#endif
