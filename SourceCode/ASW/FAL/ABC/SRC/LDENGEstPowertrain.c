/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDENGEstPowertrain
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ********************************************************/

#include "hm_logic_var.h"

#include "LDENGEstPowertrain.h"
#include "LCESPCalInterpolation.h"

#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LCTCSCallControl.h"

#include "LCESPCalLpf.h"
#if __TCS || __ETC || __EDC  /* ABS_PLUS_ECU 적용시 macro 삭제 예정 */

/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/


void LDENG_vEstimatPowertrain(void);
void LDENG_vEstimatGearRatio(void);
void LDENG_vCalcVar(void);
void LDENG_vDetectGearSt(void);
void LDENG_vEstimatEngSt(void);
void LDENG_vDetectEngDrag(void);
void LDENG_vDetectEngBrk(void);
void LDENG_vDetectGearForMT(void);
void LDENG_vTerminatGearForMT(void);


/* Implementation***************************************************/


void LDENG_vEstimatPowertrain(void)
{

   	/*if (LOOP_TIME_20MS_TASK_0 == 1)  Cycle time opt.
   	{*/
		LDENG_vEstimatGearRatio();

	/*}

	else{}*/
	LDENG_vCalcVar();	

   	/*if (LOOP_TIME_20MS_TASK_0 == 1)  적용 시 in_gear_count 확인 Cycle time opt.
   	{*/
   		LDENG_vDetectGearSt();   		
   		LDENG_vEstimatEngSt();   			
   		LDENG_vDetectEngBrk();
   		LDENG_vDetectEngDrag();

   	/*}

   	else{}*/
}

void LDENG_vEstimatGearRatio(void)
{
    if(AUTO_TM==1) 
    {
      #if ((__CAR==GM_T300) || (__CAR == GM_M350)  || (GMLAN_ENABLE==ENABLE) || (__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU))
    		if((gs_sel==0)||(gs_sel==6)) 	
    		{
    			EDC_tm_gear=0;
    		}	
    		else if(gs_sel==7) 
    		{
    			EDC_tm_gear=U8_EDC_TM_R_GEAR_RATIO;
    		}	
    		else if(gs_pos==1) 
    		{
    			EDC_tm_gear=U8_EDC_TM_1_GEAR_RATIO;
    		}	
    		else if(gs_pos==2) 
    		{
    			EDC_tm_gear=U8_EDC_TM_2_GEAR_RATIO;
    		}	
    		else if(gs_pos==3) 
    		{
    			EDC_tm_gear=U8_EDC_TM_3_GEAR_RATIO;
    		}
    		else if(gs_pos==4) 
    		{
    			EDC_tm_gear=U8_EDC_TM_4_GEAR_RATIO;
    		}
    		else if(gs_pos==5) 
    		{
    			EDC_tm_gear=U8_EDC_TM_5_GEAR_RATIO;
    		}    		    
    		else if(gs_pos==6) 
    		{
    			EDC_tm_gear=U8_EDC_TM_6_GEAR_RATIO;
    		}    					
    		else 
    		{
    			EDC_tm_gear=0;
    		}      		
	  #elif __CAR==HMC_NF	|| __CAR==HMC_GK || __CAR==HMC_SM || __CAR==HMC_EF || __CAR==HMC_JM	
    		if((gs_sel==0)||(gs_sel==6)) 	
    		{
    			EDC_tm_gear=0;
    		}	
    		else if(gs_sel==7) 
    		{
    			EDC_tm_gear=EDC_TM_BACK_RT;
    		}	
    		else if(gs_pos==1) 
    		{
    			EDC_tm_gear=EDC_TM_1_RATIO;
    		}	
    		else if(gs_pos==2) 
    		{
    			EDC_tm_gear=EDC_TM_2_RATIO;
    		}	
    		else if(gs_pos==3) 
    		{
    			EDC_tm_gear=EDC_TM_3_RATIO;
    		}	
    		else 
    		{
    			EDC_tm_gear=EDC_TM_4_RATIO;
    		}	
	  #elif __CAR==KMC_MG		    	
    	if(lespu8EMS_ENG_VOL==27)			/*for EDC test with MG 2.7 Gasoline A/T 040907*/
    	{	
    		if((gs_sel==0)||(gs_sel==6)) 	
    		{
    			EDC_tm_gear=0;
    		}	
    		else if(gs_sel==7) 
    		{
    			EDC_tm_gear=EDC_TM_BACK_RT;
    		}	
    		else if(gs_pos==1) 
    		{
    			EDC_tm_gear=EDC_TM_1_RATIO;
    		}	
    		else if(gs_pos==2) 
    		{
    			EDC_tm_gear=EDC_TM_2_RATIO;
    		}	
    		else if(gs_pos==3) 
    		{
    			EDC_tm_gear=EDC_TM_3_RATIO;
    		}	
    		else if(gs_pos==4) 
    		{
    			EDC_tm_gear=EDC_TM_4_RATIO;
    		}	
    		else 			   
    		{
    			EDC_tm_gear=EDC_TM_5_RATIO;
    		}	
    	}
    	else 
    	{
    		if((gs_sel==0)||(gs_sel==6)) 	
    		{
    			EDC_tm_gear=0;
    		}	
    		else if(gs_sel==7) 
    		{
    			EDC_tm_gear=EDC_TM_BACK_RT;
    		}	
    		else if(gs_pos==1) 
    		{
    			EDC_tm_gear=EDC_TM_1_RATIO;
    		}	
    		else if(gs_pos==2) 
    		{
    			EDC_tm_gear=EDC_TM_2_RATIO;
    		}	
    		else if(gs_pos==3) 
    		{
    			EDC_tm_gear=EDC_TM_3_RATIO;
    		}	
    		else 			   
    		{
    			EDC_tm_gear=EDC_TM_4_RATIO;
    		}	
    	}
	  #else  	
    		if((gs_sel==0)||(gs_sel==6)) 	
    		{
    			EDC_tm_gear=0;
    		}	
    		else if(gs_sel==7) 
    		{
    			EDC_tm_gear=EDC_TM_BACK_RT;
    		}	
    		else if(gs_pos==1) 
    		{
    			EDC_tm_gear=EDC_TM_1_RATIO;
    		}	
    		else if(gs_pos==2) 
    		{
    			EDC_tm_gear=EDC_TM_2_RATIO;
    		}	
    		else if(gs_pos==3) 
    		{
    			EDC_tm_gear=EDC_TM_3_RATIO;
    		}	
    		else 			   
    		{
    			EDC_tm_gear=EDC_TM_4_RATIO;
    		}	
      #endif
    }
    else		/* M/T */
    {
      #if ((__CAR==GM_T300) || (__CAR == GM_M350) || (GMLAN_ENABLE==ENABLE) || (__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU))
        if(speed_calc_timer<L_U8_TIME_10MSLOOP_200MS) /* for cycletime */
        {
        	EDC_MTM_RATIO[0]=U8_EDC_TM_1_GEAR_RATIO;
        	EDC_MTM_RATIO[1]=U8_EDC_TM_2_GEAR_RATIO;    
        	EDC_MTM_RATIO[2]=U8_EDC_TM_3_GEAR_RATIO;
        	EDC_MTM_RATIO[3]=U8_EDC_TM_4_GEAR_RATIO;    
        	EDC_MTM_RATIO[4]=U8_EDC_TM_5_GEAR_RATIO;
        	EDC_MTM_RATIO[5]=U8_EDC_TM_6_GEAR_RATIO;       	
        }
	  #elif __CAR==HMC_BK		    	
    	if(lespu8EMS_ENG_VOL==38)			
    	{    	
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;            
        }
        else if(lespu8EMS_ENG_VOL==20)
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO_2;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO_2;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO_2;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO_2;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO_2;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO_2;
        }
        else
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;
        }
      #elif  __CAR==KMC_XM
    	if( (lespu8EMS_ENG_VOL == 24) && (lespu8EMS_ENG_CHR == 4) )			
    	{    	
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;            
        }
        else if( (lespu8EMS_ENG_VOL == 22) && (lespu8EMS_ENG_CHR == 2) )
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO_2;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO_2;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO_2;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO_2;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO_2;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO_2;
        }
        else
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;
        } 
      #elif  __CAR==HMC_LM
    	if( (lespu8EMS_ENG_VOL == 24) && (lespu8EMS_ENG_CHR == 4) && (lesps16EMS_TQ_STND == 33) )			
    	{    	
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;            
        }
        else if( (lespu8EMS_ENG_VOL == 20) && (lespu8EMS_ENG_CHR == 4) && (lesps16EMS_TQ_STND == 28) )
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO_2;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO_2;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO_2;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO_2;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO_2;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO_2;
        }
        else if( (lespu8EMS_ENG_VOL == 20) && (lespu8EMS_ENG_CHR == 2) && (lesps16EMS_TQ_STND == 45) )
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO_3;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO_3;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO_3;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO_3;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO_3;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO_3;
        }        
        else
        {
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;
        }              	
      #elif  __CAR==HMC_DM
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;            
      #else
        	EDC_MTM_RATIO[0]=EDC_MTM_1_RATIO;
        	EDC_MTM_RATIO[1]=EDC_MTM_2_RATIO;    
        	EDC_MTM_RATIO[2]=EDC_MTM_3_RATIO;
        	EDC_MTM_RATIO[3]=EDC_MTM_4_RATIO;    
        	EDC_MTM_RATIO[4]=EDC_MTM_5_RATIO;
        	EDC_MTM_RATIO[5]=EDC_MTM_6_RATIO;      
	  #endif          					
    }

}

void LDENG_vCalcVar(void)	
{
	int8_t i;
	
	#if !__4WD	
        #if __REAR_D
            vel_f_mid_2=(vrad_crt_rl+vrad_crt_rr)>>1;
        #else
            vel_f_mid_2=(vrad_crt_fl+vrad_crt_fr)>>1;
        #endif
	#else		/*4WD 수정요(transfer ratio적용)*/
        #if __REAR_D
            vel_f_mid_2=(vrad_crt_rl+vrad_crt_rr)>>1;
        #else
            vel_f_mid_2=(vrad_crt_fl+vrad_crt_fr)>>1;
        #endif
    #endif	
			    
    if(AUTO_TM==1) 		/*FOR AT*/
    {	
		/* conversed vel_f_mid_rpm calc.*/
      #if !SIM_MATLAB         
		tempW3 = (int16_t)( (((int32_t)vel_f_mid_2)*2083) / (int32_t)(U16_TIRE_L*10) );	/* 2083 = 10e6 / 60 / 8(speed resolution) ,  vel_f_mid_2 : filtered value  */
		vel_f_mid_rpm =(int16_t)(((int32_t)tempW3)*(int32_t)EDC_tm_gear); /*(uint16_t)10);*/ 
				
      #else
		tempW3 = (int16_t)( ((vel_f_mid_2)*2083) / (U16_TIRE_L*10) );    	


		vel_f_mid_rpm =(int16_t)(tempW3*EDC_tm_gear); /*(uint16_t)10);*/
      #endif	
        diff_v_tc_rpm=vel_f_mid_rpm-(int16_t)tc_rpm;
        diff_eng_tc_rpm=(int16_t)eng_rpm-(int16_t)tc_rpm;
    }
    else				/*FOR MT*/
    {
    	/* conversed vel_f_mid_rpm calc.*/
      #if !SIM_MATLAB         
		tempW3 = (int16_t)( (((int32_t)vel_f_mid_2)*2083) / (int32_t)(U16_TIRE_L) );	/* 2083 = 10e6 / 60 / 8(speed resolution) ,  vel_f_mid_2 : filtered value  */    	
      #else
		tempW3 = (int16_t)( ((vel_f_mid_2)*2083) / (U16_TIRE_L*10) );     	



      #endif	
        vel_f_mid_rpm_old = vel_f_mid_rpm;
      #if !SIM_MATLAB                 
        	#if ((__CAR==GM_T300) || (__CAR == GM_M350) || (GMLAN_ENABLE==ENABLE) || (__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU))
        vel_f_mid_rpm =(int16_t)( (((int32_t)tempW3)*(uint8_t)U8_EDC_TM_1_GEAR_RATIO) / 10 );
			#else
    	vel_f_mid_rpm =(int16_t)( (((int32_t)tempW3)*(uint8_t)EDC_MTM_1_RATIO) / 10 );
    		#endif
      #else
    	vel_f_mid_rpm =(int16_t)(tempW3*EDC_MTM_1_RATIO);    	
      #endif
        /* vel_f_mid_rpm filtering*/
	  #if !SIM_MATLAB         
		vel_f_mid_rpm = LCESP_s16Lpf1Int(vel_f_mid_rpm, vel_f_mid_rpm_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
      #else	
        vel_f_mid_rpm=LCESP_s16Lpf1Int(vel_f_mid_rpm, vel_f_mid_rpm_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
      #endif
    	/* slope_v_rpm calc.*/
        slope_v_rpm_old = slope_v_rpm;    	
    	slope_v_rpm = vel_f_mid_rpm - vel_f_mid_rpm_old;
	  #if !SIM_MATLAB
		slope_v_rpm = LCESP_s16Lpf1Int(slope_v_rpm, slope_v_rpm_old, L_U8FILTER_GAIN_10MSLOOP_18HZ);      
	  #else
	    tempW5 =LCESP_s16Lpf1Int(slope_v_rpm, slope_v_rpm_old, L_U8FILTER_GAIN_10MSLOOP_18HZ); 
		if(tempW5<=0)
		{
			slope_v_rpm=tempW5-2;
		}	
		else 
		{
			slope_v_rpm=tempW5;
		}	
      #endif        

    	for( i=0; i<6; i++ ) 
    	{
		  #if !SIM_MATLAB 
            #ifndef __MGH60_TCS_OPTIMIZE
        		#if ((__CAR==GM_T300) || (__CAR == GM_M350) || (GMLAN_ENABLE==ENABLE) || (__CAR==GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU))
    		Conv_vel_f_mid_rpm[i]=(int16_t)(((int32_t)vel_f_mid_rpm*(uint8_t)EDC_MTM_RATIO[i])/U8_EDC_TM_1_GEAR_RATIO);
    		Conv_slope_v_rpm[i]=(int16_t)(((int32_t)slope_v_rpm*(uint8_t)EDC_MTM_RATIO[i])/U8_EDC_TM_1_GEAR_RATIO);
    			#else
    		Conv_vel_f_mid_rpm[i]=(int16_t)(((int32_t)vel_f_mid_rpm*(uint8_t)EDC_MTM_RATIO[i])/EDC_MTM_1_RATIO);
    		Conv_slope_v_rpm[i]=(int16_t)(((int32_t)slope_v_rpm*(uint8_t)EDC_MTM_RATIO[i])/EDC_MTM_1_RATIO);
    			#endif    			
    		diff_rpm[i]=(int16_t)(((int32_t)(Conv_vel_f_mid_rpm[i]-eng_rpm_filt)));
    		diff_slope[i]=(int16_t)(((int32_t)(Conv_slope_v_rpm[i]-slope_eng_rpm)));
		  #else
    		diff_rpm[i]=(int16_t)(((int32_t)vel_f_mid_rpm*(uint8_t)EDC_MTM_RATIO[i])/EDC_MTM_RATIO[0]) - eng_rpm_filt;
    		diff_slope[i]=(int16_t)(((int32_t)slope_v_rpm*(uint8_t)EDC_MTM_RATIO[i])/EDC_MTM_RATIO[0]) - slope_eng_rpm;
            #endif /*__MGH60_TCS_OPTIMIZE */
		  #else
    		Conv_vel_f_mid_rpm[i]=((vel_f_mid_rpm*EDC_MTM_RATIO[i])/EDC_MTM_1_RATIO);
    		Conv_slope_v_rpm[i]=((slope_v_rpm*EDC_MTM_RATIO[i])/EDC_MTM_1_RATIO);
    		diff_rpm[i]=((int32_t)(Conv_vel_f_mid_rpm[i]-eng_rpm)*10);
    		diff_slope[i]=((int32_t)(Conv_slope_v_rpm[i]-slope_eng_rpm)*10);    		
		  #endif     		
    	}   
    } 
}	

void LDENG_vDetectGearSt(void)		
{
    if(AUTO_TM==1) 
    {
        gear_pos=(int8_t)gs_pos;                  
        if(gs_sel==7) 
        {
        	gear_pos=(int8_t)gs_sel;    
        }
        else
        {
        	;
        }
        
   	    gear_state=(uint8_t)gs_pos;		

		if(gs_pos==0)
		{
			gear_state=0;
		}
		else 
		{
			if(gs_ena==0)
			{		
                gear_state=gear_state;
    		}				
			else 
			{
		 		gear_state=0;	
		 	}
		}
	}
	else
	{
      	gear_state_temp_old=gear_state_temp;

		if (In_gear_flag_temp==0)
		{
			LDENG_vDetectGearForMT();
		}
		else
		{
			LDENG_vTerminatGearForMT();			
		}

		/* in-gear detection   */   
    	if(gear_state_temp==0) 
    	{
    		In_gear_flag_temp=0;
    		in_gear_count=0;
    	}
    	else 
    	{
    		In_gear_flag_temp=1;
    		in_gear_count++;
    	}

    	gear_state_old=gear_state;
		gear_state=gear_state_temp;
		if(in_gear_count<L_U8_TIME_10MSLOOP_100MS)
		{
			gear_state=0;
			In_gear_flag=0;
		}
		else 
		{
			gear_state=gear_state_temp;
		}	 	    	   	    	  	    	
	}

	gear_state_prev1_old = 	gear_state_prev1;
    if(vref < VREF_2_5_KPH)
    {
    	gear_state_prev1 = 0;
    }		
    else
    {	
    	if((gear_state!=0)&&(BLS==0))
    	{
    		gear_state_prev1 = gear_state;
    	}
    	else if( (gear_state==0) || (BLS==1) || (ldedcu1ParkBrkSuspectFlag==1) )
    	{
    		gear_state_prev1 = gear_state_prev1_old;
    	}
    	else
    	{
    		;
    	}		
    }

#if __EDC_N_ABS_UPPER_GEAR_CLEAR
	if(BLS==1 || ldedcu1ParkBrkSuspectFlag==1)
	{
		if(gear_state==0)
		{
			N_gear_flag=1;
		}		
		else
		{
    		if((gear_state >= gear_state_prev1_old)&&(gear_state_old==0))
    		{
    			N_gear_flag=1;
    		}
    		else if(gear_state < gear_state_prev1_old)
    		{
    			N_gear_flag=0;
    		}
    		else
    		{;}
		}
	}						
	else
	{
		N_gear_flag=0;
	}
#endif	
	
	if(gear_state!=0) 
	{
		In_gear_flag=1;
	}		
	else 
	{
		In_gear_flag=0;
	}	
}

void LDENG_vEstimatEngSt(void)
{
    int8_t i;	
	engine_state_1_old=engine_state_1;
	engine_state_1_old=engine_state_2;	    
	engine_state_old=engine_state;

    if(AUTO_TM==1) 
    {
    	if((diff_eng_tc_rpm <= S16_AT_Eng_Status_Threshold)&&(diff_eng_tc_rpm >= -S16_AT_Eng_Status_Threshold))
    	{   
    		engine_state_1=1;	/*free-rolling*/
    	}
    	else if(diff_eng_tc_rpm > S16_AT_Eng_Status_Threshold)
    	{
    		engine_state_1=2;	/*traction*/
    	}
    	else
    	{
    		engine_state_1=0;	/*drag*/
    	}
    	
    	/* hysteresis */
    	if(((engine_state_1_old==2) && (engine_state_1==1)) && (diff_eng_tc_rpm > (S16_AT_Eng_Status_Threshold - 20)) && (diff_eng_tc_rpm <= (S16_AT_Eng_Status_Threshold)))
    	{
    		engine_state_1=2;
    	}
    	else if(((engine_state_1_old==0) && (engine_state_1==1)) && (diff_eng_tc_rpm >= (-S16_AT_Eng_Status_Threshold)) && (diff_eng_tc_rpm < (-S16_AT_Eng_Status_Threshold + 20)))	
    	{
    		engine_state_1=0;
    	}
    	else
    	{
    		;
    	}

    	
   		flywheel_torq = eng_torq - fric_torq;
		i = (int8_t)gear_state - 1;

    	Normalized_flywheel_torq[i]=(int16_t)flywheel_torq;
       	if(McrAbs(Normalized_flywheel_torq[i]) <= S8_MT_Eng_Status_Threshold)
       	{
       		engine_state_2=1;
       	}
       	else if(Normalized_flywheel_torq[i] > S8_MT_Eng_Status_Threshold)
       	{
       		engine_state_2=2;
       	}
       	else 
       	{
      		engine_state_2=0;
       	}

    	/* hysteresis */
    	if(((engine_state_2_old==2) && (engine_state_2==1)) && (Normalized_flywheel_torq[i] > (S8_MT_Eng_Status_Threshold - 4)) && (Normalized_flywheel_torq[i] <= (S8_MT_Eng_Status_Threshold)))
    	{
    		engine_state_2=2;
    	}
    	else if(((engine_state_2_old==0) && (engine_state_2==1)) && (Normalized_flywheel_torq[i] >= (-S8_MT_Eng_Status_Threshold)) && (Normalized_flywheel_torq[i] < (-S8_MT_Eng_Status_Threshold + 4)))	
    	{
    		engine_state_2=0;
    	}
    	else
    	{
    		;
    	} 
    	
    	if((engine_state_1==0) || (engine_state_2==0)) 
    	{
    		engine_state = 0;
    	}	
        else if((engine_state_1==1) && (engine_state_2==1)) 
        {
        	engine_state = 1;
        }	
        else 
        {
        	engine_state = 2;
        }	
    	        
    }
    else
    {
       	flywheel_torq = eng_torq - fric_torq;
    	i = (int8_t)gear_state - 1;

    	Normalized_flywheel_torq[i]=(int16_t)flywheel_torq;	
       	if(McrAbs(Normalized_flywheel_torq[i]) <= S8_MT_Eng_Status_Threshold)
       	{
       		engine_state=1;
       	}
       	else if(Normalized_flywheel_torq[i] > S8_MT_Eng_Status_Threshold)
       	{
       		engine_state=2;
       	}
       	else 
       	{
      		engine_state=0;
       	}

    	/* hysteresis */
    	if(((engine_state_old==2) && (engine_state==1)) && (Normalized_flywheel_torq[i] > (S8_MT_Eng_Status_Threshold - 4)) && (Normalized_flywheel_torq[i] <= (S8_MT_Eng_Status_Threshold)))
    	{
    		engine_state=2;
    	}
    	else if(((engine_state_old==0) && (engine_state==1)) && (Normalized_flywheel_torq[i] >= (-S8_MT_Eng_Status_Threshold)) && (Normalized_flywheel_torq[i] < (-S8_MT_Eng_Status_Threshold + 4)))	
    	{
    		engine_state=0;
    	}
    	else
    	{
    		;
    	}         
	}
}

void LDENG_vDetectEngDrag(void)	/* For Engine Drag check!!!*/
{
	if(engine_state==0) 
	{
		Engine_Drag_flag_temp1=1;
	}	
	else if((engine_state!=2)&&(Engine_braking_flag==1)) 
	{
		Engine_Drag_flag_temp1=1;
	}	
	else if(engine_state==2) 
	{
		Engine_Drag_flag_temp1=0;
	}
	else
	{
		;
	}		
	
	if( (Engine_Drag_flag_temp2==0) &&
	 #if !__4WD	
	  #if __REAR_D								/*05'winter 0110*/
		( (rel_lam_rl <= (-(LAM_6P)))||(rel_lam_rr <= (-(LAM_6P))) )  )	
	  #else 
		( (rel_lam_fl <= (-(LAM_6P)))||(rel_lam_fr <= (-(LAM_6P))) )  )
	  #endif
	 #else
	    ( (rel_lam_fl <= (-(LAM_6P)))||(rel_lam_fr <= (-(LAM_6P)))
	    ||(rel_lam_rl <= (-(LAM_6P)))||(rel_lam_rr <= (-(LAM_6P))) )  )
	 #endif         	    
	{
		Engine_Drag_flag_temp2=1;	
	}
	else if( (EDC_ON==0) && 
     #if !__4WD
       #if __REAR_D
   		 ( (rel_lam_rl >= (-(LAM_3P)))&&(rel_lam_rr >= (-(LAM_3P))) )  )
   	   #else
   	     ( (rel_lam_fl >= (-(LAM_3P)))&&(rel_lam_fr >= (-(LAM_3P))) )  )
   	   #endif
   	 #else
   	     ( (rel_lam_fl >= (-(LAM_3P)))&&(rel_lam_fr >= (-(LAM_3P)))
         &&(rel_lam_rl >= (-(LAM_3P)))&&(rel_lam_rr >= (-(LAM_3P))) )  )
     #endif        		  		
	{
		Engine_Drag_flag_temp2=0;
	}
	else
	{
		;
	}
	
	if((Engine_Drag_flag_temp1==1) || (Engine_Drag_flag_temp2==1))
	{
		Engine_Drag_flag=1;
	}
	else
	{
		Engine_Drag_flag=0;	
	}							
}


void LDENG_vDetectEngBrk(void) 
{
	tempW8 = LCESP_s16IInter2Point ( vref , VREF_20_KPH , -LAM_30P , VREF_40_KPH, -LAM_6P );
	if (vref < S16_EDC_INHIBIT_SPEED_IN_EDC) 
	{
		Engine_braking_flag=0;
	}	
	else
	{ 
      /* ENGINE BRAKING*/		
      #if __REAR_D
        if((fu1BLSErrDet==0)&&(BLS_EEPROM!=1))
        {	
          #if __VDC
           #if __GM_FailM
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN  
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else            
            if( (fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(fu1BLSErrDet==0)&&(fu1BlsSusDet==0) )
            	#endif	
           #else 
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if( (lsespu1BrkAppSenInvalid==0)&& (!ESP_ERROR_FLG) )    				
    			#else           	
      		if(!ESP_ERROR_FLG)
            	#endif      			
      	   #endif		 
    		{
    				#if __BRK_SIG_MPS_TO_PEDAL_SEN
            	if((lsespu1PedalBrakeOn==0) && (BLS==0))        	 			
    				#else    			
            	if((!MPRESS_BRAKE_ON) && (BLS==0))
            		#endif
            	{          
		  #else                	        
                if(BLS==0) 
                {
  		  #endif
                    if((rel_lam_fl >= (int8_t)(-LAM_2P))||(rel_lam_fr >= (int8_t)(-LAM_2P))) 
                    {
                        if((rel_lam_rl <= (int8_t)(tempW8)) || (rel_lam_rr <= (int8_t)(tempW8))) 
                        {
                            if((ABS_fl==0) && (ABS_fr==0)) 
                            {
                                Engine_braking_flag=1;
                            }
                            else 
                            {
                            	Engine_braking_flag=0;
                            }	
                        }
                        else if((rel_lam_rl >= (-(LAM_3P)))&&(rel_lam_rr >= (-(LAM_3P))))
                        {
                        	 Engine_braking_flag=0;
                        }	
                        else
                        {
                        	;
                        }		
                    }
                    else 
                    {
                    	Engine_braking_flag=0;
                    }	
                }
                else 
                {
                	Engine_braking_flag=0;
                }
          #if __VDC
        	}
            else 
            {
            	Engine_braking_flag=0;
            }        	  
          #endif
        }
        else
        {
          #if __VDC
           #if __GM_FailM
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else           
            if( (fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(fu1BLSErrDet==0)&&(fu1BlsSusDet==0) )
            	#endif
           #else 	
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else            
      		if(!ESP_ERROR_FLG)
      			#endif
      	   #endif
    		{
    				#if __BRK_SIG_MPS_TO_PEDAL_SEN
            	if(lsespu1PedalBrakeOn==0)            		   	 			
    				#else    			
            	if(!MPRESS_BRAKE_ON)
            		#endif
            	{          
  		  #endif
                    if((rel_lam_fl >= (int8_t)(-LAM_2P))||(rel_lam_fr >= (int8_t)(-LAM_2P))) 
                    {
                        if((rel_lam_rl <= (int8_t)(tempW8)) || (rel_lam_rr <= (int8_t)(tempW8))) 
                        {
                            if((ABS_fl==0) && (ABS_fr==0)) 
                            {
                                Engine_braking_flag=1;
                            }
                            else 
                            {
                            	Engine_braking_flag=0;
                            }	
                        }
                        else if((rel_lam_rl >= (-(LAM_3P)))&&(rel_lam_rr >= (-(LAM_3P))))
                        {
                        	 Engine_braking_flag=0;
                        }	
                        else
                        {
                        	;
                        }			
                    }
                    else 
                    {
                    	Engine_braking_flag=0;
                    }
          #if __VDC      	
                }
                else 
                {
                	Engine_braking_flag=0;
                }
        	}
            else 
            {
            	Engine_braking_flag=0;
            }        	  
          #endif        	
    	}		          	
      #else		/* FWD */
        if((fu1BLSErrDet==0)&&(BLS_EEPROM!=1))
        {	
          #if __VDC
           #if __GM_FailM
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else            
            if( (fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(fu1BLSErrDet==0)&&(fu1BlsSusDet==0) )
            	#endif
           #else 	
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if( (lsespu1BrkAppSenInvalid==0)&& (!ESP_ERROR_FLG) )    				
    			#else            
      		if(!ESP_ERROR_FLG)
      			#endif
      	   #endif
    		{
    				#if __BRK_SIG_MPS_TO_PEDAL_SEN
            	if((lsespu1PedalBrakeOn==0) && (BLS==0))        	 			
    				#else     			
            	if((!MPRESS_BRAKE_ON) && (BLS==0))
            		#endif
            	{          
		  #else                	        
                if(BLS==0) 
                {
  		  #endif
                    if((rel_lam_rl >= (int8_t)(-LAM_2P))||(rel_lam_rr >= (int8_t)(-LAM_2P))) 
                    {
                        if((rel_lam_fl <= (int8_t)(tempW8)) || (rel_lam_fr <= (int8_t)(tempW8))) 
                        {
                            if((ABS_rl==0) && (ABS_rr==0)) 
                            {
                                Engine_braking_flag=1;
                            }
                            else 
                            {
                            	Engine_braking_flag=0;
                            }	
                        }
                        else if((rel_lam_fl >= (-(LAM_3P)))&&(rel_lam_fr >= (-(LAM_3P))))
                        {
                        	Engine_braking_flag=0;
                        }	
                        else
                        {
                        	;
                        }			
                    }
                    else 
                    {
                    	Engine_braking_flag=0;
                    }	
                }
                else 
                {
                	Engine_braking_flag=0;
                }
          #if __VDC
        	} 
            else 
            {
            	Engine_braking_flag=0;
            }        	 
          #endif
        }
        else
        {
         #if __VDC
           #if __GM_FailM
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else           
            if( (fu1MCPErrorDet==0)&&(fu1MCPSusDet==0)&&(fu1BLSErrDet==0)&&(fu1BlsSusDet==0) )
            	#endif
           #else 	
    			#if __BRK_SIG_MPS_TO_PEDAL_SEN
            if(lsespu1BrkAppSenInvalid==0)    				
    			#else            
      		if(!ESP_ERROR_FLG)
      			#endif
      	   #endif
    		{
    				#if __BRK_SIG_MPS_TO_PEDAL_SEN
            	if(lsespu1PedalBrakeOn==0)            		   	 			
    				#else     			
            	if(!MPRESS_BRAKE_ON)
            		#endif
            	{          
  		  #endif
                    if((rel_lam_rl >= (int8_t)(-LAM_2P))||(rel_lam_rr >= (int8_t)(-LAM_2P))) 
                    {
                        if((rel_lam_fl <= (int8_t)(tempW8)) || (rel_lam_fr <= (int8_t)(tempW8))) 
                        {
                            if((ABS_rl==0) && (ABS_rr==0)) 
                            {
                                Engine_braking_flag=1;
                            }
                            else 
                            {
                            	Engine_braking_flag=0;
                            }	
                        }
                        else if((rel_lam_fl >= (-(LAM_3P)))&&(rel_lam_fr >= (-(LAM_3P))))
                        {
                        	 Engine_braking_flag=0;
                        }	
                        else
                        {
                        	;
                        }			
                    }
                    else 
                    {
                    	Engine_braking_flag=0;
                    }
          #if __VDC      	
                }
                else 
                {
                	Engine_braking_flag=0;
                }
        	}
            else 
            {
            	Engine_braking_flag=0;
            }        	  
          #endif        	
    	}                        	
      #endif
	}

}

void LDENG_vDetectGearForMT(void)
{
	if     ((McrAbs(diff_rpm[0])<=(INT)S16_MT_In_Gear_Vel_Threshold_1to3) && (McrAbs(diff_slope[0])<=(INT)S8_MT_In_Gear_Accel_Threshold))
	{
		gear_state_temp=1;
	}
	else if((McrAbs(diff_rpm[1])<=(INT)S16_MT_In_Gear_Vel_Threshold_1to3) && (McrAbs(diff_slope[1])<=(INT)S8_MT_In_Gear_Accel_Threshold))
	{
	    gear_state_temp=2;
    }
    else if((McrAbs(diff_rpm[2])<=(INT)S16_MT_In_Gear_Vel_Threshold_1to3) && (McrAbs(diff_slope[2])<=(INT)S8_MT_In_Gear_Accel_Threshold))
  	{
  	   	gear_state_temp=3;
  	}
    else if((McrAbs(diff_rpm[3])<=(INT)S16_MT_In_Gear_Vel_Threshold_4to6) && (McrAbs(diff_slope[3])<=(INT)S8_MT_In_Gear_Accel_Threshold))
  	{
  	   	gear_state_temp=4;
  	}      	
    else if((McrAbs(diff_rpm[4])<=(INT)S16_MT_In_Gear_Vel_Threshold_4to6) && (McrAbs(diff_slope[4])<=(INT)S8_MT_In_Gear_Accel_Threshold))
  	{
  	   	gear_state_temp=5;
  	}      	
    else if((McrAbs(diff_rpm[5])<=(INT)S16_MT_In_Gear_Vel_Threshold_4to6) && (McrAbs(diff_slope[5])<=(INT)S8_MT_In_Gear_Accel_Threshold))
  	{
  	   	gear_state_temp=6;
  	}
  	else
  	{
  		;
  	}	      	
}		

void LDENG_vTerminatGearForMT(void)
{
	if( ((gear_state_temp_old==1) && ((McrAbs(diff_rpm[0])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[0])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold)))
	  ||((gear_state_temp_old==2) && ((McrAbs(diff_rpm[1])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[1])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold))) 
	  ||((gear_state_temp_old==3) && ((McrAbs(diff_rpm[2])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[2])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold)))
	  ||((gear_state_temp_old==4) && ((McrAbs(diff_rpm[3])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[3])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold)))
	  ||((gear_state_temp_old==5) && ((McrAbs(diff_rpm[4])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[4])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold)))
	  ||((gear_state_temp_old==6) && ((McrAbs(diff_rpm[5])> (int16_t)S16_MT_In_Gear_Out_Vel_Threshold)||(McrAbs(diff_slope[5])>(int16_t)S8_MT_In_Gear_Out_Accel_Threshold))) )
	{
		gear_state_temp=0;
	}
	else
	{
		;
	}	
}


#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDENGEstPowertrain
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
