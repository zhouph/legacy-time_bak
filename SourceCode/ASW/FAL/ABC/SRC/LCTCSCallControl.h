#ifndef __LCTCSCALLCONTROL_H__
#define __LCTCSCALLCONTROL_H__


/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition******************************************************/
#define __MGH60_TCS_OPTIMIZE

/*New BTCS Structure↘*/
#define NEW_BRAKE_CTL   1       /* 0:Existing BTCS Logic , 1:New BTCS Logic */
#define S8_BTCS_QUICK_EXIT      1
#define S8_BTCS_FADEOUT_EXIT    2
/*New BTCS Structure↗*/

/*New ETCS Structure↘*/
#define NEW_ENGINE_CTL	1				   /* 0:Existing ETCS Logic , 1:New ETCS Logic */
#define	TORQ_RECOVERY_DEPEND_ON_STEER	1
	
/*EV Structure*/
	#if (__HMC_CAN_VER==CAN_EV) || (__HMC_CAN_VER==CAN_FCEV)
#define __ENG_RPM_DISABLE   1                             /* Engine RPM CAN 정보 유무*/
#define __TCS_PREVENT_FAST_RPM_INCREASE			0		/* EV 차량에서는 engine rpm 정보 없어서, __TCS_PREVENT_FAST_RPM_INCREASE 컨셉 적용 안함 */
	#else
#define __ENG_RPM_DISABLE   0
#define __TCS_PREVENT_FAST_RPM_INCREASE			1		/* RPM 기울기 급격한 상승시 토크 저감량을 증대*/
	#endif

/* Added By HJH in 07 SWD Winter*/
	#if __CAR == HMC_PA
#define ENG_RPM_LIMIT   1	/* split-mu에서 엔진 rpm 일정수준이하로 떨어뜨리지 않는 제어*/
	#else
#define ENG_RPM_LIMIT   0
    #endif

   		#if __CAR == HMC_PA	|| __CAR == KMC_SA
    #define FTCS_INIT_TORQ_DOWN_AMT 0
    #define FTCS_ENTRY_ENG_RPM_DSL 2000
    #define FTCS_ENG_RPM_LOWER_LIMIT_DSL 2000
    #define FTCS_ENG_RPM_UPPER_LIMIT_DSL 2300
    #define FTCS_ENTRY_ENG_RPM_GSL 2300
    #define FTCS_ENG_RPM_LOWER_LIMIT_GSL 2300
    #define FTCS_ENG_RPM_UPPER_LIMIT_GSL 3300
    	#elif __CAR == KMC_HM
    #define FTCS_INIT_TORQ_DOWN_AMT 20
    #define FTCS_ENTRY_ENG_RPM_DSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_DSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_DSL 2300
    #define FTCS_ENTRY_ENG_RPM_GSL 2500
    #define FTCS_ENG_RPM_LOWER_LIMIT_GSL 2500
    #define FTCS_ENG_RPM_UPPER_LIMIT_GSL 3300
    	#elif __CAR == HMC_BK
    #define FTCS_INIT_TORQ_DOWN_AMT 20
    #define FTCS_ENTRY_ENG_RPM_DSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_DSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_DSL 2300
    #define FTCS_ENTRY_ENG_RPM_GSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_GSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_GSL 2300
    	#elif __CAR == HMC_JM
    #define FTCS_INIT_TORQ_DOWN_AMT 50
    #define FTCS_ENTRY_ENG_RPM_DSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_DSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_DSL 2300
    #define FTCS_ENTRY_ENG_RPM_GSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_GSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_GSL 2300
    	#else
    #define FTCS_INIT_TORQ_DOWN_AMT 50
    #define FTCS_ENTRY_ENG_RPM_DSL 1800
    #define FTCS_ENG_RPM_LOWER_LIMIT_DSL 1800
    #define FTCS_ENG_RPM_UPPER_LIMIT_DSL 2300
    #define FTCS_ENTRY_ENG_RPM_GSL 2300
    #define FTCS_ENG_RPM_LOWER_LIMIT_GSL 2300
    #define FTCS_ENG_RPM_UPPER_LIMIT_GSL 3300
    	#endif
    #define	FTCS_UNDER_RPM_TORQ_INC_AMT  1
    #define	FTCS_OVER_RPM_TORQ_DEC_AMT   1
    #define ETCS_TO_FTCS_INIT_TORQ_INC_AMT 2

#define ETCS_ENABLE_IN_ABS 1
/* Added By HJH in 07 SWD Winter*/

/*New ETCS Structure↗*/

#define     BTCS_STATE1         3
#define     BTCS_STATE2         4


/*MISRA, H22*/
#define     TM_1_RATIO          11              /* 2.842  *3.770*/
#define     TM_2_RATIO          6               /* 1.529  *3.770*/
#define     TM_3_RATIO          4               /* 1.0    *3.770*/
#define     TM_4_RATIO          3               /* 0.712  *3.770*/
#define     TM_BACK_RT          9               /* 2.48   *3.770*/

#if (__CAR == FIAT_PUNTO)
#define     MTM_1_RATIO         20              /* 2.842  *3.770*/
#define     MTM_2_RATIO         11               /* 1.529  *3.770*/
#define     MTM_3_RATIO         7               /* 1.0    *3.770*/
#define     MTM_4_RATIO         5               /* 0.712  *3.770*/
#define     MTM_5_RATIO         4               /* 0.712  *3.770*/
#define     MTM_BACK_RT         19              /* 2.48   *3.770*/
#else
#define     MTM_1_RATIO         14              /* 2.842  *3.770*/
#define     MTM_2_RATIO         9               /* 1.529  *3.770*/
#define     MTM_3_RATIO         6               /* 1.0    *3.770*/
#define     MTM_4_RATIO         4               /* 0.712  *3.770*/
#define     MTM_5_RATIO         3               /* 0.712  *3.770*/
#define     MTM_BACK_RT         11              /* 2.48   *3.770*/
#endif
#define     GK_TREAD            121             // unit:1.49m*8^2*3.6^2(vel)*9.81(g)/100
#define     AY_THSD_HIGH        450             /* 200:00.06.15, 300:00.11.27, 400:01.03.22*/
#define     AY_THSD_MID         250             /* 150:00.03.07, 170:00.11.29, 175:01.02.04*/
#define     AY_THSD_LOW         70

#define     VREF_TO_TORQ_PERCENT            1
#define     GAIN_TO_TORQ_PERCENT           10
#define     GAIN_OFFSET_TO_TOQR_PERCENT     1
#define     ACCEL_X_TO_TORQ_PERCENT         1

/* Resolution : TPS_PERCENT_10PRO = 10 */
#define     TPS_PERCENT_10PRO              10
/* 기본 파라미터 중 없는 부분 추가   */
/*#define     T_0_MS                        0
#define     T_28_MS                       4
#define     T_70_MS                       10
#define     T_214_MS                      30
#define     T_1480_MS                     210
#define     T_1750_MS                   248
#define     T_3000_MS                     426
#define     T_3500_MS                     496*/

#define     VREF_12_5_KPH                 100
#define     VREF_18_75_KPH                150
#define     VREF_200_KPH                 1600

#define     S16_MAX_LIMIT				30000	/* VREF_MAX(255KPH) (32767 - 255*8 => 30000 ) */
#define     U16_MAX_LIMIT				60000	/* VREF_MAX(255KPH) (65536 - 255*8 => 60000 ) */
/*  Resolution : ACCEL_X_1G0G = 200*/
#define     ACCEL_X_0G0G                    0
#define     ACCEL_X_0G01G                   2
#define     ACCEL_X_0G015G                  3
#define     ACCEL_X_0G02G                   4
#define     ACCEL_X_0G025G                  5
#define     ACCEL_X_0G03G                   6
#define     ACCEL_X_0G04G                   8
#define     ACCEL_X_0G05G                  10
#define     ACCEL_X_0G06G                  12
#define     ACCEL_X_0G07G                  14
#define     ACCEL_X_0G075G                 15
#define     ACCEL_X_0G1G                   20
#define     ACCEL_X_0G15G                  30
#define     ACCEL_X_0G2G                   40
#define     ACCEL_X_0G25G                  50
#define     ACCEL_X_0G3G                   60
#define     ACCEL_X_0G32G                  64
#define     ACCEL_X_0G35G                  70
#define     ACCEL_X_0G4G                   80
#define     ACCEL_X_0G45G                  90
#define     ACCEL_X_0G5G                  100
#define     ACCEL_X_0G6G                  120
#define     ACCEL_X_0G7G                  140
#define     ACCEL_X_0G75G                 150
#define     ACCEL_X_0G8G                  160
#define     ACCEL_X_0G9G                  180
#define     ACCEL_X_1G0G                  200
#define     ACCEL_X_1G25G                 250
#define     ACCEL_X_1G5G                  300
#define     ACCEL_X_1G6G                  320
#define     ACCEL_X_2G0G                  400

#define __RWD_ENG_CONTROL   0 /* RWD engine control*/

/*  Resolution : SPIN_DIFF_0_1KPH = 8*/
#define     SPIN_DIFF_0_025KPH            2
#define     SPIN_DIFF_0_05KPH             4
#define     SPIN_DIFF_0_1KPH              8
#define		SPIN_DIFF_0_15KPH             12
#define     SPIN_DIFF_0_2KPH              16
#define     SPIN_DIFF_0_3KPH              24
#define     SPIN_DIFF_0_4KPH              32
#define     SPIN_DIFF_0_5KPH              40
#define     SPIN_DIFF_0_8KPH              64

#if __MY_08
	#if (__CAR==KMC_UN) || (__CAR==KMC_XM) || (__CAR==KMC_AM)  || (__CAR==KMC_TD) || (__CAR == GM_C100) || (__CAR == HMC_JM) || (__CAR == HMC_NF) || (__HMC_CAN_VER==CAN_HE_KE)
#define __TCS_CONV_ABS_TORQ		0		
	#elif __HMC_CAN_VER==CAN_1_2||__HMC_CAN_VER==CAN_1_3||__HMC_CAN_VER==CAN_1_4||__HMC_CAN_VER==CAN_1_6||__HMC_CAN_VER==CAN_2_0||__HMC_CAN_VER==CAN_HD_HEV||__HMC_CAN_VER==CAN_YF_HEV||__HMC_CAN_VER==CAN_EV||__HMC_CAN_VER==CAN_FCEV||__CAR==BYD_5A||__CAR==FIAT_PUNTO
#define __TCS_CONV_ABS_TORQ		1	
	#else
#define __TCS_CONV_ABS_TORQ		0
	#endif
#endif	

#define     MAX_TORQUE          9900

	#if __GM_FailM
#define 	GM_FAIL_MODE_ENABLE	0
	#else
#define 	GM_FAIL_MODE_ENABLE	0
	#endif

	#if __TCS_STUCK_ALGO_ENABLE
extern int16_t	tcs_stuck_timer;
	#endif

	#if __VDC
#define __TCS_DYAW_INTEG_CTRL	0
	#else
#define __TCS_DYAW_INTEG_CTRL	0
	#endif

	#if __VDC && __TCS_DYAW_INTEG_CTRL
#define TCS_YAWC_YAWM_MULTI_THR		4	
#define TCS_DYAW_INTEG_SCAN			L_U8_TIME_10MSLOOP_1000MS /*142*/	
#define TCS_DYAW_INTEG_SCAN_MAX		L_U16_TIME_10MSLOOP_2S /*284*/
extern uint16_t	ltcsu16DyawIntegCtrlCnt;
extern int16_t	ltcss16DyawInteg;
extern uint8_t	ltcsu8DyawIntegFlag;
	#endif	
	
	#if __TCS_DRIVELINE_PROTECT
extern uint16_t	ltcsu16FrontAxleSpeed;
extern uint16_t	ltcsu16RearAxleSpeed;
extern uint16_t	ltcsu16LeftSideSpeed;
extern uint16_t	ltcsu16RightSideSpeed;
	#endif	

#define __TCS_CAN_EMS_TORQ_SCALE_CONV	1
	
	#if __HMC_CAN_VER==CAN_1_2||__HMC_CAN_VER==CAN_1_3||__HMC_CAN_VER==CAN_1_4||__HMC_CAN_VER==CAN_1_6||__HMC_CAN_VER==CAN_2_0||__HMC_CAN_VER==CAN_HD_HEV||__HMC_CAN_VER==CAN_YF_HEV||__HMC_CAN_VER==CAN_EV||__HMC_CAN_VER==CAN_FCEV||__CAR==CHINA_B12||__CAR==BYD_5A ||__CAR == FIAT_PUNTO
#define __TCS_CAN_EMS_TORQ_DEFAULT_REL	1
	#elif (GMLAN_ENABLE==ENABLE) ||( __CAR==SYC_RAXTON) || (__CAR==SYC_RAXTON_MECU) || (__CAR == SYC_KYRON) || defined(SSANGYONG_CAN) || (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU) || (__HMC_CAN_VER==CAN_HE_KE) ||(__CAR==GM_J300)||(__CAR == GM_LAMBDA)
#define __TCS_CAN_EMS_TORQ_DEFAULT_REL	0	
	#else
#define __TCS_CAN_EMS_TORQ_DEFAULT_REL	0		
	#endif	

	#if __CAR == GM_T300 || __CAR == GM_GSUV || (GMLAN_ENABLE==ENABLE) || __CAR == GM_M350 ||(__CAR==GM_J300)||(__CAR == GM_LAMBDA)	
#define	__ENGINE_MODE_STEP				1
	#else
#define	__ENGINE_MODE_STEP				0
	#endif			

#define __BTCS_FADE_OUT_LOW_PRESS		0
	#if __BTCS_FADE_OUT_LOW_PRESS
#define U8_BTCS_FADE_OUT_LOW_PRESS		10
#define U8_BTCS_FADE_OUT_MIN_PRESS		3
#define U8_BTCS_FADE_OUT_LOW_DIV		10
#define U8_BTCS_FADE_OUT_MIN_DIV		3
extern uint16_t	ltcsu16FadeOutScanSetting;
	#endif	

/*15 SWD Demo SW*/
#define SWD_15_DEMO_VEHICLE 0
#define U8_TCS_MIN_PRESS_TRANS_HOMO     80 /*8bar split->homo 전환 제어 압력*/
/*15 SWD Demo SW*/

#define  __COMPETITIVE_EXIT_CONTROL 	1
#define U16TCSGearInhibitScanAftEEC 	100 /*1초 EEC 이후 기어변속 금지 시간*/
#define U8_D_GAIN_COMP_RATIO_IN_RR 		3 /*Rough Road 감지시 Engine 제어에 사용되는 D Gain을 감소시키는 Factor(원래 D Gain을 이 값으로 나눔)*/
#define U16_TCS_GAIN_SCALING_FACTOR 	80 /*P/D Effect = (P_Gain * WheelSpinError + D_Gain * WheelSpinErrorDiff) / U16_TCS_GAIN_SCALING_FACTOR*/

/*Refactoring Undefined TP MBD ETCS*/
#define S16_NEW_ENG_OK_TORQ_IN_TURN_0		200
#define S16_NEW_ENG_OK_TORQ_IN_TURN_1		400
#define S16_NEW_ENG_OK_TORQ_IN_TURN_2		500
#define S16_NEW_ENG_OK_TORQ_IN_TURN_3		600
#define S16_TCS_TURN_DET_BY_ALAT_TH			200
#define U8_TCS_TRQ_DEC_TH_SPLIT_HILL		0
#define S8_TCS_HILL_AX_DIFF  				50
#define S8_TCS_HILL_DELTA_AX 				50
#define S8_TCS_HILL_SLOPE    				12
#define U8_TCS_HILL_DCT_TIMES_BY_AX			50
#define S16_TCS_TURN_DET_4_ENT_BY_ALAT_TH	400
#define S8_TARGET_SPIN_YAW_RATE_DIFF_1		-50
#define S8_TARGET_SPIN_YAW_RATE_DIFF_2		-10
#define S8_TARGET_SPIN_YAW_RATE_DIFF_3		50
#define U8_TCS_SET_FAST_RPM_INC_TH_1		16
#define S16_TCS_P_GAIN_F_RPM_FAST_INC_1		400
#define S16_TCS_D_GAIN_F_RPM_FAST_INC_1		400
#define S16_NEW_ENG_OK_TORQ_0_0				0
#define S16_NEW_ENG_OK_TORQ_0_1				0
#define S16_NEW_ENG_OK_TORQ_0_2				0
#define S16_NEW_ENG_OK_TORQ_1_0				0
#define S16_NEW_ENG_OK_TORQ_1_1				0
#define S16_NEW_ENG_OK_TORQ_1_2				0
#define S16_NEW_ENG_OK_TORQ_2_0				0
#define S16_NEW_ENG_OK_TORQ_2_1				0
#define S16_NEW_ENG_OK_TORQ_2_2				0
#define U8_TCS_STEER_TO_ZERO_CNT			30                   
#define S8_TARGET_SPIN_ALAT_DIFF_1			-50
#define S8_TARGET_SPIN_ALAT_DIFF_2			-10
#define S8_TARGET_SPIN_ALAT_DIFF_3			50   

#define U16_TCS_OFF_OFFSET_IN_TURN			50
#define S16TCSGearShiftInhibitLATAcc		200
#define U16_TCS_GSC_RPM_ENTER				4000
#define U16_TCS_GSC_RPM_EXIT 				4200

/*Definition of Developing Function ******************************************************/
#define __TCS_BUMP_DETECTION					1		/* Bump detection */

#define __TCS_HUNTING_DETECTION					1		/* Hunting Detection -> 2WD Feasibility 확인후 4WD 확장 필요 */

	#if (__CAR_MAKER==GM_KOREA) || (__CAR==HMC_BK)
#define __TCS_2WL_MINI_DETECTION	0
	#else
#define __TCS_2WL_MINI_DETECTION	0
	#endif	

	#if __ENG_RPM_DISABLE	
#define __VARIABLE_MINIMUM_TORQ_LEVEL	0		/* Engine RPM Disable System에서는 Concept 적용 불가 -> 수정 필요 */
	#else
#define __VARIABLE_MINIMUM_TORQ_LEVEL	1		/* Engine RPM Disable System에서는 Concept 적용 불가 -> 수정 필요 */	
	#endif
	
#define __TCS_VIBRATION_DETECTION	   	1   	/* Detect VIBRATION of Wheel Speed */							
#define U8_TCS_VIB_OVERFLOW_SCAN		50

#define __NEW_STUCK_ALGO	1

/* __GEAR_SHIFT_CHARACTERISTIC*/
	#if __CAR == SYC_Q150
#define __GEAR_SHIFT_INHIBIT		1
	#else
#define __GEAR_SHIFT_INHIBIT		0
	#endif	

#define U8_TCS_HOLD_COUNT_TIME		L_U8_TIME_10MSLOOP_100MS

/*	#if ((__CAR == GM_GSUV) || (__CAR == PSA_C3))*/
#define __TCS_CTRL_DEC_IN_GR_SHFT_MT     1 /*15 SWD*/
/*	#else 
#define __TCS_CTRL_DEC_IN_GR_SHFT_MT     0 
	#endif
*/

	#if (__CAR == GM_GSUV)
#define MAXIMUM_RPM_LIMIT	             1
	#else
#define MAXIMUM_RPM_LIMIT	             0
	#endif	

	#if (__HSA == ENABLE)
#define	ENGINE_STALL_DETECION_REFINE	 1	
	#else
#define	ENGINE_STALL_DETECION_REFINE	 0
	#endif	

#define U16_ETCS_LOHI_ICE_ACCEL 1
#define LOW_TO_HIGH_DETECT 1
#define LS_RISE_RATE_LIMIT 1

/*'13SWD Development Logic Macro*/
#define __TCS_REFER_TARGET_TORQ_CTL 	 1
#define U8_TCS_REFER_TAR_TORQ_IMPROVE	 1
#define	__TCS_HILL_DETECION_CONTROL	 	 1

/* NEW STRUCTURE Tuning Parameter ******************************************************/


/* ETCS Tuning Parameters **************************************************************/
#define U8_VEHICLE_TREAD_LENGTH	16		// unit : 1.6m

/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/
typedef struct
{
    uint16_t TCS_BIT_07     :1;
    uint16_t TCS_BIT_06     :1;
    uint16_t TCS_BIT_05     :1;
    uint16_t TCS_BIT_04     :1;
    uint16_t TCS_BIT_03     :1;
    uint16_t TCS_BIT_02     :1;
    uint16_t TCS_BIT_01     :1;
    uint16_t TCS_BIT_00     :1;
}TCS_FLAG_t;

#define lctcsu1BrkCtrlInhibit       lcTcsCtrlFlg0.TCS_BIT_07
#define lctcsu1EngCtrlInhibit       lcTcsCtrlFlg0.TCS_BIT_06
#define lctcsu1InhibitByFM          lcTcsCtrlFlg0.TCS_BIT_05
#define lctcsu1DriverAccelIntend    lcTcsCtrlFlg0.TCS_BIT_04
#define lctcsu1DriverBrakeIntend    lcTcsCtrlFlg0.TCS_BIT_03
#define lctcsu1DriverBrakeIntendSus lcTcsCtrlFlg0.TCS_BIT_02
#define lctcsu1EngInformInvalid     lcTcsCtrlFlg0.TCS_BIT_01
#define lctcsu1RoughRoad            lcTcsCtrlFlg0.TCS_BIT_00

#define lctcsu1WhlSpdCompByRTADetect    lcTcsCtrlFlg1.TCS_BIT_07
#define lctcsu1InvalidWhlSpdBfRTADetect lcTcsCtrlFlg1.TCS_BIT_06
#define lctcsu1DriverAccelIntendInvalid lcTcsCtrlFlg1.TCS_BIT_05
#define lctcsu1DriverBrakeIntendInvalid lcTcsCtrlFlg1.TCS_BIT_04
#define lctcsu1DetectTurnPriority       lcTcsCtrlFlg1.TCS_BIT_03
#define lctcsu1DetectVibration          lcTcsCtrlFlg1.TCS_BIT_02
#define lctcsu1DrvMode4Wheel           	lcTcsCtrlFlg1.TCS_BIT_01
#define lctcsu1DrvMode2Wheel           	lcTcsCtrlFlg1.TCS_BIT_00

#define lctcsu1TCSGainIncInHighmu      	lcTcsCtrlFlg2.TCS_BIT_07
#define lctcsu1ReservedFlag06		 	lcTcsCtrlFlg2.TCS_BIT_06
#define lctcsu1ReservedFlag07		 	lcTcsCtrlFlg2.TCS_BIT_05
#define lctcsu1ReservedFlag04		 	lcTcsCtrlFlg2.TCS_BIT_04
#define lctcsu1ReservedFlag03      		lcTcsCtrlFlg2.TCS_BIT_03
#define lctcsu1ReservedFlag02          	lcTcsCtrlFlg2.TCS_BIT_02
#define lctcsu1ReservedFlag01          	lcTcsCtrlFlg2.TCS_BIT_01
#define lctcsu1ReservedFlag00          	lcTcsCtrlFlg2.TCS_BIT_00


#define 	BTCS_RISE_MODE			0
#define 	BTCS_DUMP_MODE			1
#define 	BTCS_HOLD_MODE			2
#define 	BTCS_IRISE_MODE			3

#define		BTCS_SPIN_INCREASE		1
#define		BTCS_SPIN_DECREASE		0

#define S16_TCS_CTRL_MODE_DISABLE   0
#define S16_TCS_CTRL_MODE_1         1
#define S16_TCS_CTRL_MODE_DP        10
#define S16_TCS_CTRL_MODE_SP        20

/*Global Extern Variable  Declaration*******************************************************************/
extern TCS_FLAG_t   lcTcsCtrlFlg0, lcTcsCtrlFlg1, lcTcsCtrlFlg2;

#if __CAR == GM_GSUV  
extern uint16_t U16_TCS_SPEED1;
extern uint16_t U16_TCS_SPEED2;
extern uint16_t U16_TCS_SPEED3;
extern uint16_t U16_TCS_SPEED4;
extern uint16_t U16_TCS_SPEED5;
#endif

extern	int16_t	lctcss16WhlSpdreferanceByMiniT;
extern	uint8_t lctcsu8ClearTurnPriorityCnt;
extern	uint8_t lctcsu8TCSEngCtlPhase;
extern	uint8_t lctcsu8TCSEngCtlPhase_old;

extern	int16_t lctcss16AxFilterDelta;
extern	uint8_t lctcsu8SplitHillDctCntbyAx;

extern  uint8_t lctcsu8TargetSpinTurnFactor;

#if __TCS_REFER_TARGET_TORQ_CTL
extern  uint8_t lctcsu8TargetSpinIncCount;
extern  uint8_t lctcsu8TargetSpinIncTime;
extern  uint8_t lctcsu8TargetSpinIncInHomo;
extern  uint8_t lctcsu8TargetSpinIncInSplit;
extern  uint8_t lctcsu8DeltaTriHomoTargetSpin;
extern  uint8_t lctcsu8DeltaTriSplitTargetSpin;
extern  int16_t  delta_yaw_first_rate[3];
extern  int16_t  delta_yaw_first_diff;
extern  uint8_t  lctcsu8AddYawThrbyYawDiff;
extern  int16_t  alat_ems_rate[3];
extern  int16_t  alat_ems_diff;
extern  uint8_t  lctcsu8AddAyThrbySteer;
extern  uint8_t  lctcsu8AddAyThrbyAyDiff;
extern  uint8_t  lctcsu8AddAyThrbyWstrAyDiff;
#endif /*__TCS_REFER_TARGET_TORQ_CTL*/

extern  uint16_t lctcsu16SpinOverThInFast;

/*Refactoring Undefined Signal MBD ETCS*/
extern  uint8_t  lcu8SccMode;
#if !__HSA
#define	HSA_flg						            HSAF0.bit7
#define	HSA_EST_CLUTCH_POSITION					HSAF0.bit6
#define	INHIBIT_DCT_ROLL			            HSAF0.bit5
#define	HSA_TCL_DEMAND_fl			            HSAF0.bit4
#define	HSA_TCL_DEMAND_fr			            HSAF0.bit3
#define	HSA_S_VALVE_LEFT			            HSAF0.bit2
#define	HSA_S_VALVE_RISHT			            HSAF0.bit1
#define	HSA_MSC_MOTOR_ON			            HSAF0.bit0
extern 	U8_BIT_STRUCT_t HSAF0;
#endif /*!__HSA*/

/*Global Extern Functions  Declaration******************************************************/
// <skeon OPTIME_MACFUNC_2> 110303
#ifndef OPTIME_MACFUNC_2
extern INT LCTCS_s16ILimitRange( INT InputValue, INT MinValue, INT MaxValue );
extern INT LCTCS_s16ILimitMinimum( INT InputValue, INT MinValue );
extern INT LCTCS_s16ILimitMaximum( INT InputValue, INT MaxValue );
extern INT  LCTCS_s16IFindMaximum( INT InputValue1, INT InputValue2);
extern INT  LCTCS_s16IFindMinimum( INT InputValue1, INT InputValue2);
extern UINT LCTCS_u16ILimitMaximum( UINT InputValue, UINT MaxValue );
extern INT  LCTCS_s16IInterpDepRPM3by3( INT InputValue_0_0, INT InputValue_0_1, INT InputValue_0_2, INT InputValue_1_0, INT InputValue_1_1, INT InputValue_1_2, INT InputValue_2_0, INT InputValue_2_1, INT InputValue_2_2);
#endif
// </skeon OPTIME_MACFUNC_2>

//extern INT LCTCS_s16ILimitRange( INT InputValue, INT MinValue, INT MaxValue );
//extern INT LCTCS_s16ILimitMinimum( INT InputValue, INT MinValue );
//extern UINT LCTCS_u16ILimitMaximum( UINT InputValue, UINT MaxValue );
//extern INT LCTCS_s16ILimitMaximum( INT InputValue, INT MaxValue );
//extern INT  LCTCS_s16IFindMaximum( INT InputValue1, INT InputValue2);
//extern INT  LCTCS_s16IFindMinimum( INT InputValue1, INT InputValue2);
extern void LATCS_vConvertTCS2EMS(void);
extern INT  LCTCS_s16ICompensateEngineStall(INT InputValue);
extern void LCTCS_vTurnOffFTCS(void);
	#if __TCS_DRIVELINE_PROTECT
extern void LCTCS_vCalTargetDVForDP(void);
    #endif
extern void LCTCS_vDecideEngineVariation(void);
#endif
