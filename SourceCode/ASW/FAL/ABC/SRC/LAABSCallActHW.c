/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LAABSCallActHW.C
* Description: Hardware Actuation for ABS control
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAABSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LAABSCallActHW.h"
  #if (__IDB_LOGIC == DISABLE)
#include "LAABSGenerateLFCDuty.h"
  #else
#include "LAABSIdbGenerateDuty.h"
  #endif/*#if (__IDB_LOGIC == DISABLE)*/
#include "LCABSCallControl.h"
#include "LSABSCallSensorSignal.h"
#include "LCABSDecideCtrlState.h"
/* Local Definiton  **********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/
/*void LAABS_vSetWheelValveCommand(struct W_STRUCT *WL);*/
static void LAABS_vSetWheelValveCommand(struct W_STRUCT *WL_temp);

/* GlobalFunction prototype **************************************************/


/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAABS_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Main Function for ABS control
******************************************************************************/
void LAABS_vCallActHW(void)
{
/************************************************************************
* NO, NC Valve Command Setting                                          *
************************************************************************/   
    LAABS_vSetWheelValveCommand(&FL);
    LAABS_vSetWheelValveCommand(&FR);
    LAABS_vSetWheelValveCommand(&RL);
    LAABS_vSetWheelValveCommand(&RR);
  
  #if !__BTC
    vcc_off_flg = 1;  
  #endif

/*#if !__VDC*/
/*  #if __PWM */ /* 2006.06.15  K.G.Y */
  LAABS_vGenerateLFCDuty();              /* PWM Pattern Generation for LSV(Ver0.0)->???us */
/*  #endif */ /* 2006.06.15  K.G.Y */
/*#endif  */

/************************************************************************
* VALVE_ACT Command Setting                                             *
************************************************************************/   
/* #if  __PWM  */ /* 2006.06.15  K.G.Y */
    if((ABS_fz==1)||(EBD_RA==1)) {VALVE_ACT=1;}
/* #endif  */ /* 2006.06.15  K.G.Y */  

}


/* ====================================================================== */
#if __REORGANIZE_ABS_FOR_MGH60
static void LAABS_vSetWheelValveCommand(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;

    HV_VL_wl=0;
    AV_VL_wl=0;

    if((bls_k_timer > L_U8_TIME_10MSLOOP_20MS)
      #if (__CONSIDER_BLS_FAULT == ENABLE)
    &&(lcabsu1FaultBLS==0)
      #endif
    )
    {       /* Changed by CSH 030225 */
        WL->state=0;
        BUILT_wl=0;
    }
    else {
        if(AV_DUMP_wl == 1) {
            HV_VL_wl=1;
            AV_VL_wl=1;
            if(ABS_fz==1) {     /* Added by KGY 041007 */
                SET_DUMP_wl=1;                  /* SM01_1 */
              #if __REAR_MI
                MSL_DUMP_wl=1;                  /*MI02I */
              #endif
            }
            else { }
        }
        else if(AV_HOLD_wl == 1) {
            HV_VL_wl=1;
        }
        else {
            ;
        }

      #if __AQUA_ENABLE
        if((AQUA_END == 1) && (REAR_WHEEL_wl == 0))
        {
            HV_VL_wl=0;
            AV_VL_wl=0;
        }
        else { }
      #endif
    }
}

#else
static void LAABS_vSetWheelValveCommand(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;
    
    if(bls_k_timer > L_U8_TIME_10MSLOOP_20MS) {       /* Changed by CSH 030225 */
        WL->state=0;
        BUILT_wl=0;
        HV_VL_wl=0;
        AV_VL_wl=0;
      #if __REAR_MI
/*      WL->continue_dump_timer=0; */
      #endif
    }
    else {
        if(REAR_WHEEL_wl == 0) {
            if(AV_DUMP_wl == 1) {
                HV_VL_wl=1;
                AV_VL_wl=1;
                if(ABS_fz==1) {     /* Added by KGY 041007 */
                    SET_DUMP_wl=1;                  /* SM01_1 */
                  #if __REAR_MI
/*
                    if(!EBD_wl) {WL->continue_dump_timer++;}
                    else { }
*/                  
                    MSL_DUMP_wl=1;                  /*MI02I */
                  #endif
                }
                else { }
            }
            else if(AV_HOLD_wl == 1) {
                HV_VL_wl=1;
                AV_VL_wl=0;
                if(ABS_fz==1) {     /* Added by KGY 041007 */
                  #if __REAR_MI
/*
                    if(REAR_WHEEL_wl == 1) {
                        if(WL->p_hold_time>0) {WL->continue_dump_timer=0;}
                        else { }
                    }
                    else if(WL->p_hold_time>2) {WL->continue_dump_timer=0;}
                    else { }
*/
                  #endif
                }
                else { }
            }
            else {
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
/*              WL->continue_dump_timer=0; */
              #endif
            }
          #if __AQUA_ENABLE
            if(AQUA_END == 1) {
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
/*              WL->continue_dump_timer=0; */
              #endif
            }
            else { }
          #endif
        }
        else {
            if((AV_DUMP_wl == 1) || (SL_DUMP_wl == 1)) {
                HV_VL_wl=1;
                AV_VL_wl=1;
                if(ABS_fz==1) {     /* Added by KGY 041007 */
                    SET_DUMP_wl=1;                  /* SM01_1 */
                  #if __REAR_MI
/*
                    if(!EBD_wl) {WL->continue_dump_timer++;}
                    else { }
*/                  
                    MSL_DUMP_wl=1;                  /*MI02I */
                  #endif
                }
                else { }
            }
            else if(AV_HOLD_wl == 1) {
                HV_VL_wl=1;
                AV_VL_wl=0;
                if(ABS_fz==1) {     /* Added by KGY 041007 */
                  #if __REAR_MI
/*
                    if(REAR_WHEEL_wl == 1) {
                        if(WL->p_hold_time>0) {WL->continue_dump_timer=0;}
                        else { }
                    }
                    else if(WL->p_hold_time>2) {WL->continue_dump_timer=0;}
                    else { }
*/
                  #endif
                }
                else { }
            }
            else {
                HV_VL_wl=0;
                AV_VL_wl=0;
              #if __REAR_MI
/*              WL->continue_dump_timer=0; */
              #endif
            }
        }
    }
}
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAABSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
