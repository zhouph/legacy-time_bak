#ifndef __LDESPESTDISCTEMP_H__
#define __LDESPESTDISCTEMP_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

extern    INT       ldesps16OutsideAirTempt ;
extern    UCHAR     ldespu8OutsideAirTemptV ;
extern    UCHAR     ldespu8OutsideAirTemptM ;




#if	(__BTEM == ENABLE)
#define	 ldespu1TemptUnusede0_7       TEMPT_0.bit7
#define	 ldespu1TemptUnusede0_6    TEMPT_0.bit6 
#define	 ldespu1TemptUnusede0_5    TEMPT_0.bit5 
#define	 ldespu1TemptUnusede0_4    TEMPT_0.bit4 
#define	 ldespu1TemptUnusede0_3    TEMPT_0.bit3 
#define	 ldespu1TemptUnusede0_2    TEMPT_0.bit2 
#define	 ldespu1TemptUnusede0_1    TEMPT_0.bit1
#define	 ldespu1TemptUnusede0_0    TEMPT_0.bit0 

struct WL_TEMPT
{
    INT    ldesps16PressInputBar;
	INT    ldesps16Vrad1secOld;
    INT    ldesps16PressEstBar;
    INT    ldesps16PressSumBar;
    INT    ldesps16TemptUprate;   
    INT    ldesps16TemptDownrate;   
    INT    ldesps16Tempt;   
    LONG   ldesps32TemptTemp;
    INT    ldesps16cal_tmp;
    unsigned   ldespu1TemptEstInhibit:     1;
};

extern struct WL_TEMPT *TEMPT, FL_TEMPT, FR_TEMPT , RL_TEMPT, RR_TEMPT;


extern    U8_BIT_STRUCT_t  TEMPT_0;
extern    UCHAR     ldespu8TemptCalcCnt;

/*Define ***********************************************************************/

#define  btc_tmp_fl				FL_TEMPT.ldesps16Tempt
#define  btc_tmp_fr				FR_TEMPT.ldesps16Tempt
#define  btc_tmp_rl				RL_TEMPT.ldesps16Tempt
#define  btc_tmp_rr				RR_TEMPT.ldesps16Tempt
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/


/*Global Extern Variable Declaration *******************************************/



/*Global Extern Functions Declaration ******************************************/
extern void LDESPEstDiscTempMain(void);

#endif /* #if	((__BTEM == ENABLE)) */

#endif
