

#include "hm_logic_var.h"

HM_BIT_t HmCanFlg0,HmCanFlg1,HmCanFlg2,HmCanFlg3,HmCanFlg4,HmCanFlg5, HmCanFlg6, HmCanFlg7, HmCanFlg8;

int16_t lesps16EMS_TQI_ACOR;
uint16_t lesps16EMS_N;
int16_t lesps16EMS_TQI;
int16_t lesps16EMS_DRIVE_TQI;
int16_t lesps16EMS_TQFR;
int16_t lesps16EMS_CAN_VERS;
int16_t lesps16EMS_TQ_STND;
int16_t lesps16EMS_TEMP_ENG;
int16_t lesps16TCS_TQI_TCS;       
int16_t lesps16TCS_TQI_MSR;       
int16_t lesps16TCS_TQI_SLOW_TCS;
int16_t lesps16TCU_TEMP_AT;    
int16_t lespu16TCU_N_TC; 
int16_t lesps16_TCU_REQ_TORQ;   
int16_t  lesps16TCS_TQI_ESP;
uint8_t lespu8ESP_GMIN_ESP;
uint8_t lespu8ESP_GMAX_ESP; 
int16_t lesps16EMS_TPS;           
int16_t lesps16EMS_TPS_Resol1000;            
int16_t lesps16EMS_PV_AV_CAN;    
int16_t lesps16EMS_PV_AV_CAN_Resol1000;
 
uint8_t lespu8EMS_CONF_TCU;  
uint8_t lespu8EMS_OBD_FRF_ACK;
uint8_t lespu8EMS_ENG_CHR;
uint8_t lespu8EMS_ENG_VOL;
uint8_t lespu8TCU_TAR_GC;
uint8_t lespu8TCU_G_SEL_DISP; 
		#if (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU)|| (__CAR==PSA_C3)
uint8_t lespu8_SteerWheelRotationSpeed;	
uint8_t lespu8_ShiftingAuthorization;	
uint8_t lespu8_BrakingInProgress;
uint8_t lespu8_TorqueRequestStatus;
uint16_t lespu16EMS_N;
		#endif

uint8_t ee_tmp_fl;
uint8_t ee_tmp_fr;
uint8_t ee_tmp_rl;
uint8_t ee_tmp_rr;


uint8_t lcanu1OAT_PT_EstM;
uint8_t lcanu1OAT_PT_EstV;
int8_t  lcans8OAT_PT_Est;



uint8_t lespu8TCS_4WD_OPEN ;   
uint8_t lespu1TCS_4WD_LIM_REQ; 
uint16_t lespu16TCS_4WD_TQC_LIM;
uint8_t lespu8TCS_4WD_CLU_LIM; 
uint8_t lespu1TCS_4WD_LIM_MODE;
uint8_t lespu8TCU_F_TCU;       
uint8_t lespu1TCU_TCU_TYPE_AT; 
uint8_t lespu1TCU_TCU_TYPE_CVT;

uint8_t lespu8TCU_GEAR_TYPE;   

uint8_t lespu8AWD_4WD_TYPE;    
uint8_t lespu8AWD_4WD_SUPPORT; 
uint8_t lespu1AWD_4WD_ERR;     
uint8_t lespu8AWD_CLU_DUTY;    
uint8_t lespu8AWD_R_TIRE;      

uint8_t lespu1AWD_2H_ACT;      
uint8_t lespu1AWD_4H_ACT;      
uint8_t lespu1AWD_LOW_ACT;     
uint8_t lespu1AWD_AUTO_ACT;    
uint8_t lespu1AWD_LOCK_ACT;    
uint16_t lespu16AWD_4WD_TQC_CUR; 

uint8_t lespu8RDCM_SecAxlStat; 
uint8_t lespu8RDCM_SAMDVLReqVal;  

uint8_t lespu1EMS_CLU_ACK;
uint8_t lespu8EMS_TQ_COR_STAT;

uint8_t lespu8TCS_WHEEL;
uint8_t lespu8TCS_WHEEL_FL_TCS;
uint8_t lespu8TCS_WHEEL_FR_TCS;
uint8_t lespu8TCS_WHEEL_RL_TCS;
uint8_t lespu8TCS_WHEEL_RR_TCS;

uint8_t lespu8AWD_EMS2Q	;
uint8_t lespu8AWD_ITM_SW;
uint8_t lespu8AWD_ITM_HW;

int16_t lesps16EMS6_TQI_MAX;
int16_t lesps16EMS6_TQI_TARGET;
int16_t lesps16EMS6_TQI;
int16_t lesps16EMS6_TQI_MIN;

uint8_t lespu8EMS_EmsIsgStat;

uint8_t lcu1Ems7ActiveFlg;

#if (GMLAN_ENABLE==ENABLE) || __CAR==GM_C100 || __CAR==GM_V275 || __CAR==GM_CTS || __CAR==GM_CTS_MECU || __CAR==GM_TAHOE || __CAR==GM_J300 || __CAR==GM_LAMBDA ||__CAR==GM_MALIBU ||__CAR==GM_SILVERADO || __CAR==PSA_C3 || __CAR==GM_T300 || __CAR==VV_V40 || __CAR==SGM_328
uint16_t lespu16_TCCRCplReqVal;		  	
uint16_t lespu16_TracCntrlMaxTorqIncRt;

int16_t lesps16_BrkTemp;				

uint8_t lespu8_TCCDGCDecayGrad;			
uint16_t lespu16_TTDGCDcyGrad;			
uint8_t lespu8_WhlSensRghRdMag;			
uint8_t lespu8_BSTGRReqdGear;			
uint8_t lespu8_BSTGRReqType;			
uint8_t lespu8_ChsBrkgLoad;				
uint8_t lespu8_SprTireSt;				
uint8_t lespu8_VhDynFLWhlSt;			
uint8_t lespu8_VhDynFRWhlSt;			
uint8_t lespu8_VhDynRLWhlSt;            
uint8_t lespu8_VhDynRRWhlSt;            

int8_t lesps8_VehDynOvrUndrStr; 

uint8_t lespu8TCU_TAR_GC;
uint8_t lespu8_TrnsShftLvrPos;

	/* Added by Kim Jeonghun in 2007.7.21 */
uint8_t lespu8TCSysOpMd;			
uint8_t lespu8VehStabEnhmntMd;	
uint8_t lespu1TreInfMonSysRstPrfmd;
uint8_t lespu1TirePrsLowIO;
uint8_t lespu1BrkSysBrkLtsReqd;
uint8_t lespu1BrkSysRedBrkTlltlReq;
uint8_t lespu1StWhAnVDA;
uint8_t lespu8BrkSysVTopSpdLimVal;
uint8_t lespu1ActVehAccelV;	
int16_t  lesps16ActVehAccel_0;	
uint8_t lespu8TracCntrlMaxTorqIncRt;
uint8_t lespu1BrkSysTrnsGrRqTnstnTp;
uint8_t lespu1VSELongAccV;	
uint8_t lespu8VSELongAcc;

uint8_t lespu1TCSAct;
uint8_t lespu1VSEAct;
uint8_t lespu1TracTrqDecCntAtv;
uint8_t lespu1DrvIndpntBrkAppAct;
uint8_t lespu1AutoBrkngAct;
uint8_t lespu1TrnsBrkSysCltchRelRqd;
uint8_t lespu1EngTqMinExtRngVal;
int16_t	 lesps16EngTqMinExtRng;
uint16_t lespu16_EngOffTmExtRng;
uint8_t lespu8WhlSlpSt;
uint8_t lespu2TransNtrlCntrlMdStat;

/* TPMS */
uint8_t lespu8TirePressureStatFL;
uint8_t lespu8TirePressureStatFR;
uint8_t lespu8TirePressureStatRL;
uint8_t lespu8TirePressureStatRR;
uint16_t lespu16TirePressureFL;
uint16_t lespu16TirePressureFR;
uint16_t lespu16TirePressureRL;
uint16_t lespu16TirePressureRR;

uint16_t lespu16BaroPressAbs;
uint16_t lespu16EngManfldAbsPrs;
uint8_t lespu8SysPwrMd;

uint8_t lctcsu8FunctionLampOnTime;
#endif
 #if defined(CHERY_CAN)
/* For B12 */
uint16_t lespu16TCM_AG_ratio;
uint16_t lespu16TCM_N_Wheel;
uint8_t lespu8TCM_TC_Status;
/* For B12 */

/* For A13 */
uint8_t lespu8FBCM_ENG_TYPE;
uint16_t lespu16FBCM_TyreRollingRadius;

/* For A13 */
	#endif

	#if defined(SSANGYONG_CAN)
/* For SYC C200 */
  /* Receiver signals */
uint8_t lespu8MS_VEH_CODE_ENG;
uint8_t lespu8MS_VEH_CODE_TC;
uint8_t lespu8MS_VEH_SUS_TYPE;
uint8_t lespu8TCU_CUR_GC;
uint8_t lespu8TCU_MT_GEAR_POSITION;
int16_t	 lesps16EMS_TQFR_C200_DSL;
int16_t	 lesps16EMS_TQFR_C200_GSL;
  /* Transmitter signals */
uint8_t lespu8ESP_GMINMAX_ART;
uint8_t lespu8TCS_EBS_STATUS;
	#endif
	
	#if __HMC_CAN_VER==CAN_HD_HEV
uint8_t lespu8ARB_CasVlvCmdC_Pc;		
	#endif
	
	#if __HMC_CAN_VER==CAN_YF_HEV || __HMC_CAN_VER==CAN_HE_KE || __HMC_CAN_VER==CAN_HD_HEV
/* Variables and Flags For ESC	 */
int16_t lesps16EMS_ActIndTq_Pc;
uint8_t lespu8HCU_EngCltStat;
int16_t lesps16EMS_IndTq_Pc;
int16_t lesps16HCU_EngTqCmdBInv_Pc;
int16_t lesps16MCU_CR_Mot_EstTq_Pc;

	#if __HMC_CAN_VER==CAN_YF_HEV || __HMC_CAN_VER==CAN_HD_HEV
int16_t lesps16HCU_MotTqCmdBInv_Pc;
int16_t lesps16TCU_CR_Tcu_TqRedReq_Pc;
int16_t lesps16TCU_CR_Tcu_TqRedReqSlw_Pc;
int16_t lesps16TCU_CR_Tcu_TqIncReq_Pc;
	#endif 
	
	#if __HMC_CAN_VER==CAN_HE_KE
int16_t lesps16HCU_Mot1TqCmdBInv_Pc;
int16_t lesps16HCU_Mot2TqCmdBInv_Pc;	
	#endif 	
#endif
#if (__EPB_INTERFACE) || (__AVH)
/* Rx Signal For EPBi, AVH */
uint8_t lcanu8EPB_DBF_REQ;				/* EPB dynamic braking request */
uint8_t lcanu8EPB_FAIL;				/* information about the availabilty of EPB */
uint8_t lcanu8EPB_DBF_DECEL;			/* requested deceleration for DBF */
uint8_t lcanu8EPB_STATUS;				/* force sataus of EPB */

/* Tx Signal For EPBi, AVH */
uint8_t lcanu8LDM_STAT;				/* longitudinal dynamic management state */
uint8_t lcanu8ECD_ACT;					/* ECD activation signal */
#endif	/* ( (__EPBi) || (__AVH) ) */

#if __AVH
/* Tx Signal For AVH */
uint8_t lcanu8AVH_STAT;				/* AVH state */
uint8_t lcanu8AVH_LAMP;				/* info lamp request to cluster */
uint8_t lcanu8AVH_ALARM;			/* Audio active status lamp request to cluster */
uint8_t lcanu8REQ_EPB_ACT;			/* ESC request to EPB */
uint8_t lcanu8REQ_EPB_STAT;			/* validity of EPB activation request */
uint8_t lcanu8AVH_CLU;
#endif	/* __AVH */

#if __SPAS_INTERFACE
  
uint8_t lcu8SpasBrkIntReq; /*SPAS Brake Intervention Request*/
uint8_t lcu8SpasBrkIntCmd; /*SPAS Brake Intervention Command*/
uint8_t lcu8SpasBrkIntRpl; /*SPAS Brake Intervention Reply*/
uint8_t lcu8SpasBrkIntPtn; /*SPAS Brake Intervention Pattern*/
 
#endif


/* Rx Signal For ACC */
int16_t lcans16CF_Ems_DecelReq;		/* Deceleration amount for brake control */

#if __AHB_SYSTEM==ENABLE
/* AHB -> ESP Signal */
int16_t	lcans16AhbMcpRaw;				/* Master Cylinder raw pressure value : 0.01 bar Resolution */ 
int16_t	lcans16AhbMcpFilt_1_100Bar;			/* Master Cylinder Offset º¸d ¾з °ª : 0.01 bar Resolution */
int16_t	lcans16AhbMcTargetPress;		/* Master Cylinder Target Pressure : 0.01 bar Resolution */
int16_t lcans16AhbRbcPress;             /* RBC Pressure Reduction: 0.1 bar Resolution */

/* ESP -> AHB Signal */
uint8_t	lsespu8WhlCylStatFL;
uint8_t	lsespu8WhlCylStatFR;
uint8_t	lsespu8WhlCylStatRL;
uint8_t	lsespu8WhlCylStatRR;
#endif	/* #if __AHB_SYSTEM==ENABLE */

#if __TSP /* TSP Signal */
uint8_t lespu8TSP_TrlrHtchSwAtv; 
#endif
	
		#if (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU)
/* For Jeep Compass*/
  /* Receiver signals */
uint8_t lespu8CVT_TRANS_RATIO;
uint16_t lespu16MS_OutputSpd308h;
uint16_t lespu16BS_REF_VEH_SPEED;
uint8_t lespu8VG_TCASE_STAT;
uint8_t lespu8DSS_FT_DIFF_MMI_ST;
uint8_t lespu8DSS_CNT_DIFF_MMI_ST;
uint8_t lespu8DSS_R_DIFF_MMI_ST;
uint8_t lespu8DSS_FT_DIFF_MOD;
uint8_t lespu8DSS_CNT_DIFF_MOD;
uint8_t lespu8DSS_R_DIFF_MOD;
uint8_t lespu8DSS_ACT_FT_DIF_TRQ;
uint8_t lespu8DSS_ACT_CNT_DIF_TRQ;
uint8_t lespu8DSS_ACT_R_DIF_TRQ;
uint8_t lespu8DSS_DES_FT_DIF_TRQ;
uint8_t lespu8DSS_DES_CNT_DIF_TRQ;
uint8_t lespu8DSS_DES_R_DIF_TRQ;
  /* Transmitter signals */  
uint8_t lespu8BS_HILL_DES_STAT;
uint8_t lespu8BS_ESP_BRK_SW;
uint8_t lespu8ESP_FT_DIFF_ESP_RQ;
uint8_t lespu8ESP_R_F_ESP_CONT;
uint8_t lespu8ESP_CENT_DIFF_ESP_RQ;
uint8_t lespu8ESP_L_F_ESP_CONT;
uint8_t lespu8ESP_RR_DIFF_ESP_RQ;
uint8_t lespu8ESP_R_R_ESP_CONT;
uint8_t lespu8ESP_CODING_TABLE;
uint8_t lespu8ESP_L_R_ESP_CONT;
uint8_t lespu8ESP_FT_DIFF_TRQ_RQ;
uint8_t lespu8ESP_CENT_DIFF_TRQ_RQ;
uint8_t lespu8ESP_RR_DIFF_TRQ_RQ;		
uint8_t lespu8BS_SLV_ESP;	
		#endif
 	#if __CAR == RSM_QM5 || __CAR == RSM_QM5_MECU 		
uint8_t lespu8ESP_SHIFT_AUTHOR;
uint8_t lespu8ESP_EARLY_GSUP;	
uint16_t lespu16EMS_N;
uint8_t lespu8TCU_CUR_GC;
	#endif	

/* BDW */
uint8_t lespu8CF_Clu_RainSnsStat;
/* BDW */
 
/* CLU */
uint8_t lcanu8DRV_DR_SW;				/* driver door switch */
uint8_t lcanu8DRV_Seat_Belt;			/* driver seat belt on/off status */
uint8_t lcanu8TRUNK_OPEN_STATUS;		/* Trunk latch switch signal */
uint8_t lcanu8CF_HoodStat;				/* Hood Latch Switch signal */
/*CLU */

#if __CAR == VV_V40
uint8_t		lespu8BCM_TEXT;
int16_t		lesps16TCU_CUR_G_RATIO;
int16_t		lcans16CSTBATS_TrqVl;
#endif /* __CAR == VV_V40 */
 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_hm_logic_var
	#include "Mdyn_autosar.h"               
#endif