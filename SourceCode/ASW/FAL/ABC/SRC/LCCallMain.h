#ifndef __LCCALLMAIN_H__
#define __LCCALLMAIN_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/
extern void LC_vCallMainControl(void);

#if __ETC
extern void LCETC_vCallControl(void);
#endif

#if __TCS
extern void LCTCS_vCallControl(void);
#endif

#if __ETSC
extern void LCETSC_vCallControl(void);
#endif

#if __RDCM_PROTECTION
extern void LCRDCM_vCallControl(void);
#endif

#if __VDC
extern void LCESP_vCallControl(void);
#endif

#if __TOD
extern void	LCTOD_vCallControl(void);
#endif

#if defined(__CDC)
extern void LCCDC_vCallControl(void);
extern void LC_CalcSportModeControl(void);
#endif

#if __VSM
extern void LCEPS_vCallControl(void);
#endif

#if __AFS
extern void LCAFS_vCallControl(void);
#endif


#if __EDC
extern void LCEDC_vCallControl(void);
#endif

#if __ACC
extern void LCACC_vCallControl(void);
#endif

#if __HDC
extern void LCHDC_vCallControl(void);
#endif

#if __DEC
extern void LCDEC_vCallControl(void);
#endif

#if __EPB_INTERFACE
extern void LCEPB_vCallControl(void);
#endif

#if __TSP
extern void LCTSP_vCallControl(void);
#endif

#if __BDW
extern void LCBDW_vCallControl(void);
#endif

#if __EBP
extern void LCEBP_vCallControl(void);
#endif

#if __CBC
extern void LCCBC_vCallControl(void);
#endif

#if __SLS
extern void LCSLS_vCallControl(void);
#endif

#endif
