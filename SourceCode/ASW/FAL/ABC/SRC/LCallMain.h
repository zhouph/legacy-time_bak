#ifndef __LCALLMAIN_H__
#define __LCALLMAIN_H__

/*Includes *********************************************************************/

#include"LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern uint8_t wmu8DrvMode;

# if defined(__DURABILITY_TEST_ENABLE)
     
	extern uint16_t	ESPCtlNumCnt;
	extern uint16_t	TCSCtlNumCnt;
	extern uint16_t ABSCtlNumCnt;
	extern uint16_t	HSACtlNumCnt;
	extern uint32_t	HDCCtlCnt;
	extern uint32_t	CBSCtlCnt;
  
#endif

/*Global Extern Functions Declaration ******************************************/
extern void	ebd_off(void);

#if __BTC
extern void	btc_off(void);

extern void	LSTCS_vWriteEEPROMDiskTemp(void);

  #if !SIM_MATLAB
extern void	LSTCS_vInitBTCSVariable(void);
  #endif	/* !SIM_MATLAB */
#endif

extern void	LS_vCallMainSensorSignal(void);
extern void	LD_vCallMainDetection(void);
extern void	LC_vCallMainControl(void);
extern void	abs_off(void);
extern void	LO_vCallMainCoordinator(void);
extern void	LA_vCallMainValve(void);
extern void	LA_vCallMainCan(void);
extern void	LFC_initial_parameter_set(void);
extern void L_vInitializeVariables(void);
extern void L_vCallMain(void);
#if __VDC
extern void L_vCallMainIgnOffMode(void);
#endif

extern void L_vSetInterface(void);
  
extern void s16mul16div(void);
extern void u16mul16div(void);
extern void s16mul16div16(void);
extern void u16mul16div16(void);
extern void s16muls16divs16(void);
extern void u16mulu16divu16(void);

#endif
