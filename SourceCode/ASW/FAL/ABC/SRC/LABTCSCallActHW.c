/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LABTCSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

/* Header file include *******************************************************/
#include "LCESPCalInterpolation.h"
#include "LATCSCallActHW.h"
#include "LABTCSCallActHW.h"
#include "LCTCSCallControl.h"
#include "LSENGCallSensorSignal.h"
#include "LCRDCMCallControl.h"
#include "LACallMain.h"
#include "Hardware_Control.h"
#include "LCTCSCallBrakeControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "algo_var.h"
#include "t_struct.h"

/* Variables Definition*******************************************************/
WHEEL_VV_OUTPUT_t latcs_FL1HP, latcs_FL2HP, latcs_FR1HP, latcs_FR2HP;
WHEEL_VV_OUTPUT_t latcs_RL1HP, latcs_RL2HP, latcs_RR1HP, latcs_RR2HP;

int16_t tcs_msc_target_vol;
int16_t latcss16PriTcTargetPress, latcss16SecTcTargetPress;
int16_t latcss16TarPreRateMd;
int16_t latcss16premFiltPressPri;
int16_t latcss16premFiltPressOldPri;
int16_t latcss16premFiltPressSec;
int16_t latcss16premFiltPressOldSec;



/* GlobalFunction prototype **************************************************/

/* LocalFunction prototype ***************************************************/
static void LDTCS_vDctBothDrvgWhlBrkCtlReq(struct TCS_AXLE_STRUCT *Axle_TCS);

static void LCTCS_vCalSameTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR);

#if (MULTIPLEX_TCS == DISABLE)
static void LCTCS_vCalAxleTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
static void LCTCS_vCalSideTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR);
static void LCTCS_vCalSpHssTarCirPres(struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR);
static void LCTCS_vCalSpVibTarCirPres(struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR);
#endif

static void LATCS_vGenNorPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT);
static void LATCS_vCordGenPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT);

#if (MULTIPLEX_TCS == ENABLE)
static void LATCS_vDctMuxBrkCtrl(void);
static void LATCS_vDctOverBrkInMuxCtrl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *Wl_TCS);
static void LATCS_vDctHssBrkInMuxCtrl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *Wl_TCS);
static void LATCS_vSetPreActCir(void);
static void LATCS_vCalActCirCnt(void);
static void LATCS_vCalMeMryTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT);
static void LCTCS_vCalTarPresCmd(void);
#else
static void LCTCS_vCalTarPresInfCmd(void);
#endif

/*static void LATCS_vLimitTarPresRate(struct TCS_CIRCUIT_STRUCT *CIRCUIT);*/
static void LATCS_vTagetPress(void);
static void LATCS_vEstCirPresByTarPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT);
static void LSESP_vCalPressurePri(void);
static void LSESP_vCalPressureSec(void);

static void LAMSC_vSetTCSTargetVoltage(void);
static void LATCS_vSetMtrVolt4Axle(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LATCS_vSetMtrVoltAddInSpHill(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LATCS_vSetMtrVolt4VCA(void);
static void LATCS_vSetMtrVolt4Veh(void);
static void LATCS_vSetTarPreGrdtMd(void);
static void LATCS_vSetTarPreGrdtMdEdge(void);

void LATCS_vCtlNoNcValve(void);
void LCTCS_vControlBTCSReapplyMode(struct W_STRUCT *W_L);
void LCTCS_vControlBTCSDumpMode(struct W_STRUCT *W_L);

static void LATCS_vSetWheelValveCommand(void);
static void LATCS_vSetWheelValveOutput(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);
	
/* Implementation*************************************************************/
#if __BTC /* tag 1 */
#if	defined(BTCS_TCMF_CONTROL) /* tag 2 */
void LATCS_vCallActHWGen2(void)
{
    LDTCS_vDctBothDrvgWhlBrkCtlReq(&FA_TCS);
    LDTCS_vDctBothDrvgWhlBrkCtlReq(&RA_TCS);
    	
	LCTCS_vCalSameTarCirPres(&PC_TCS, &FR_TCS, &RL_TCS);
    LCTCS_vCalSameTarCirPres(&SC_TCS, &FL_TCS, &RR_TCS);
    
	#if (MULTIPLEX_TCS == DISABLE)
	LCTCS_vCalAxleTarCirPres(&PC_TCS, &RL_TCS, &RR_TCS);	
	LCTCS_vCalAxleTarCirPres(&SC_TCS, &RL_TCS, &RR_TCS);
	LCTCS_vCalAxleTarCirPres(&PC_TCS, &FR_TCS, &FL_TCS);
	LCTCS_vCalAxleTarCirPres(&SC_TCS, &FR_TCS, &FL_TCS);	
	LCTCS_vCalSideTarCirPres(&PC_TCS, &FR_TCS, &RR_TCS);
	LCTCS_vCalSideTarCirPres(&SC_TCS, &FR_TCS, &RR_TCS);
	LCTCS_vCalSideTarCirPres(&PC_TCS, &FL_TCS, &RL_TCS);
	LCTCS_vCalSideTarCirPres(&SC_TCS, &FL_TCS, &RL_TCS);	
	LCTCS_vCalSpHssTarCirPres(&FL_TCS, &RL_TCS);
	LCTCS_vCalSpHssTarCirPres(&FR_TCS, &RR_TCS);		
	LCTCS_vCalSpVibTarCirPres(&FL_TCS, &RL_TCS);
	LCTCS_vCalSpVibTarCirPres(&FR_TCS, &RR_TCS);
	#endif
    
    LATCS_vGenNorPres(&PC_TCS);
    LATCS_vGenNorPres(&SC_TCS);
	LATCS_vCordGenPres(&PC_TCS);
	LATCS_vCordGenPres(&SC_TCS);    

    #if (MULTIPLEX_TCS == ENABLE)
    LATCS_vDctMuxBrkCtrl();
    LATCS_vDctOverBrkInMuxCtrl(&FA_TCS, &FR_TCS);
    LATCS_vDctOverBrkInMuxCtrl(&RA_TCS, &RL_TCS);
    LATCS_vDctOverBrkInMuxCtrl(&FA_TCS, &FL_TCS);
    LATCS_vDctOverBrkInMuxCtrl(&RA_TCS, &RR_TCS);
    
    LATCS_vDctHssBrkInMuxCtrl(&FA_TCS, &FR_TCS);
    LATCS_vDctHssBrkInMuxCtrl(&RA_TCS, &RL_TCS);
    LATCS_vDctHssBrkInMuxCtrl(&FA_TCS, &FL_TCS);
    LATCS_vDctHssBrkInMuxCtrl(&RA_TCS, &RR_TCS);
    
    LATCS_vSetPreActCir();
    LATCS_vCalActCirCnt();
    LATCS_vCalMeMryTarCirPres(&PC_TCS);
    LATCS_vCalMeMryTarCirPres(&SC_TCS);
    LCTCS_vCalTarPresCmd();
    #else
    LCTCS_vCalTarPresInfCmd();
    #endif
	
    /*LATCS_vLimitTarPresRate(&PC_TCS);
    LATCS_vLimitTarPresRate(&SC_TCS);*/
    LATCS_vTagetPress();
    LATCS_vEstCirPresByTarPres(&PC_TCS);
    LATCS_vEstCirPresByTarPres(&SC_TCS);
    LSESP_vCalPressurePri();
    LSESP_vCalPressureSec();

	LAMSC_vSetTCSTargetVoltage();
    
    LATCS_vSetTarPreGrdtMd();
    LATCS_vSetTarPreGrdtMdEdge();
    
	LATCS_vCtlNoNcValve();
  	LATCS_vSetWheelValveCommand();
}

static void LDTCS_vDctBothDrvgWhlBrkCtlReq (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if((Axle_TCS->lctcsu1SymSpinDctFlg==1)&&(Axle_TCS->lctcsu1BrkCtlActAxle==1))
	{
		Axle_TCS->lctcsu1BothDrvgWhlBrkCtlReq = 1;
	}
	else
	{
		Axle_TCS->lctcsu1BothDrvgWhlBrkCtlReq = 0;
	}
}

static void LCTCS_vCalSameTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR)
{
	if		( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==0) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_FRONT->lctcss16TarWhlPre;
	}
	else if	( (WL_FRONT->lctcsu1BTCSWhlAtv==0) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
	}
	else if ( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		/* Rear wheel의 target wheel pressure를 사용하면 Front에 
		과도한 brake torque가 생성 되므로 front wheel의 target wheel pressure를 사용함 */
		CIRCUIT->lctcss16TarCirPres = WL_FRONT->lctcss16TarWhlPre;	
	}
	else
	{
		CIRCUIT->lctcss16TarCirPres = 0;
	}
}

#if (MULTIPLEX_TCS == DISABLE)
static void LCTCS_vCalAxleTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if ( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		CIRCUIT->lctcss16TarCirPres = LCTCS_s16IFindMaximum(WL_LEFT->lctcss16TarWhlPre, WL_RIGHT->lctcss16TarWhlPre);
	}
	else
	{
		;
	}
}

static void LCTCS_vCalSideTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR)
{
	if ( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		/* Rear wheel의 target wheel pressure를 사용하면 Front에 
		과도한 brake torque가 생성 되므로 front wheel의 target wheel pressure를 사용함 */
		CIRCUIT->lctcss16TarCirPres = WL_FRONT->lctcss16TarWhlPre;	
	}
	else
	{
		;
	}
}

static void LCTCS_vCalSpHssTarCirPres(struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR)
{
	if ( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		if (   (FA_TCS.lctcsu8StatWhlOnHghMu!=TCSHighMuUnStable)
			&& (FA_TCS.lctcsu1FlgOfHSSRecvry==0)	)
		{
			if (   (RA_TCS.lctcsu8StatWhlOnHghMu==TCSHighMuUnStable)
				|| (RA_TCS.lctcsu1FlgOfHSSRecvry==1)	)
			{
				/*Rear Wheel에만 Hopping 발생시 Rear Wheel의 Target Wheel Pressure를 사용함*/	
				PC_TCS.lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
				SC_TCS.lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
			}
			else
			{
				;
			}
		}
	}
	else
	{
		;
	}			
}

static void LCTCS_vCalSpVibTarCirPres(struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR)
{
	if ( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		if ( (WL_FRONT->lctcsu1VIBFlag_tcmf==0) && (WL_REAR->lctcsu1VIBFlag_tcmf==1) )	
		{
			/*Rear Wheel에만 Hopping 발생시 Rear Wheel의 Target Wheel Pressure를 사용함*/	
			PC_TCS.lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
			SC_TCS.lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}			
}
#endif /*(MULTIPLEX_TCS == DISABLE)*/

static void LATCS_vGenNorPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
{
	CIRCUIT->lctcss16NorTarCirPresOld = CIRCUIT->lctcss16NorTarCirPres;

	if (CIRCUIT->lctcsu1BrkCtlActCir==1)
	{
		if 		(CIRCUIT->lctcsu1IniTCVCtlMode==1)
		{		/* Initial control mode */
			CIRCUIT->lctcss16NorTarCirPres = (int16_t)U8TCSCpInitTarPres;
		}
		else if ( (FA_TCS.lctcsu1SymSpinDctFlg==1) || (RA_TCS.lctcsu1SymSpinDctFlg==1) )
		{		/* Symmetric spin control */
			CIRCUIT->lctcss16NorTarCirPres = CIRCUIT->lctcss16TarCirPres;
		}
		else
		{		/* Asymmetric spin control */
			if 		( (CIRCUIT->lctcsu1BrkCtlActCirRsgEdg == 1) || (CIRCUIT->lctcsu1IniTCVCtlModeFalEdg == 1) )
			{	/* Setting of starting point after initial control mode */
				CIRCUIT->lctcss16NorTarCirPres = (int16_t)U8TCSCpFFStartPointPres ;
			}
			else if	(CIRCUIT->ltcsu1CirModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)
			{	/* Feed forward mode */
				CIRCUIT->lctcss16NorTarCirPres = CIRCUIT->lctcss16NorTarCirPresOld + U8TCSCpPresIncRateInFF;
			}
			else
			{	/* Feed back mode */
				CIRCUIT->lctcss16NorTarCirPres = CIRCUIT->lctcss16TarCirPres;
			}
		}
		CIRCUIT->lctcss16NorTarCirPres = LCTCS_s16ILimitRange(CIRCUIT->lctcss16NorTarCirPres , MPRESS_0BAR, MPRESS_160BAR );
	}
	else
	{
		CIRCUIT->lctcss16NorTarCirPres = MPRESS_0BAR;
	}
}

static void LATCS_vCordGenPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
{
	  if ( VCA_ON_TCMF == 1 )
	  {
	  	if (ltcsu8VCARunningCntTCMF<U8TCSCpIniVCACtlModeTime) 
	  	{
	  	  	CIRCUIT->lctcss16CordTarCirPres = LCTCS_vConvCurntOnTC2Pres(CIRCUIT->lctcss16InitMaxVCATCCurnt);
	  	}
	  	else
	  	{ 
	  		CIRCUIT->lctcss16CordTarCirPres = CIRCUIT->lctcss16NorTarCirPres;
	  	}
	  }
	  else
	  {
	  	CIRCUIT->lctcss16CordTarCirPres = CIRCUIT->lctcss16NorTarCirPres;
	  }
}

#if (MULTIPLEX_TCS == ENABLE)
static void LATCS_vDctMuxBrkCtrl(void)
{
	if (   ((FR_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==1) && (FL_TCS.lctcsu1BTCSWhlAtv==0) && (RL_TCS.lctcsu1BTCSWhlAtv==0)) 
	    || ((FR_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) && (FL_TCS.lctcsu1BTCSWhlAtv==1) && (RL_TCS.lctcsu1BTCSWhlAtv==1)) )
	{
		lctcsu1MuxBTCSWhlAtv = 1;
	}
	else
	{
		lctcsu1MuxBTCSWhlAtv = 0;
	}
}	
	
static void LATCS_vDctOverBrkInMuxCtrl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *Wl_TCS)
{
	if ((Axle_TCS->lctcsu1OverBrkAxle == 1)&&(Wl_TCS->lctcsu1BTCSWhlAtv == 1))
	{
		Wl_TCS->lctcsu1OverBrkWhl = 1;
	}
	else
	{
		Wl_TCS->lctcsu1OverBrkWhl = 0;
	}
}

static void LATCS_vDctHssBrkInMuxCtrl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *Wl_TCS)
{
	if (	((Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)||(Axle_TCS->lctcsu1FlgOfHSSRecvry == 1))
		 && (Wl_TCS->lctcsu1BTCSWhlAtv == 1)	)
	{
		Wl_TCS->lctcsu1HSSBrkWhl = 1;
	}
	else
	{
		Wl_TCS->lctcsu1HSSBrkWhl = 0;
	}
}	      

static void LATCS_vSetPreActCir(void)
{
	if (lctcsu1MuxBTCSWhlAtv == 1)
	{
		if (ENG_STALL == 1)
		{
			if (PC_TCS.lctcss16TarCirPres >= SC_TCS.lctcss16TarCirPres)
			{
				PC_TCS.lctcsu1PreActTarCirPres = 1;
				SC_TCS.lctcsu1PreActTarCirPres = 0;
				PC_TCS.lctcsu1PreActReset = 1;
				SC_TCS.lctcsu1PreActReset = 0;
			}
			else
			{
				PC_TCS.lctcsu1PreActTarCirPres = 0;
				SC_TCS.lctcsu1PreActTarCirPres = 1;
				PC_TCS.lctcsu1PreActReset = 0;
				SC_TCS.lctcsu1PreActReset = 1;	
			}
		}
		else if (	(FR_TCS.lctcsu1OverBrkWhl == 1)||(RL_TCS.lctcsu1OverBrkWhl == 1)	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 1;
			SC_TCS.lctcsu1PreActTarCirPres = 0;
			PC_TCS.lctcsu1PreActReset = 1;
			SC_TCS.lctcsu1PreActReset = 0;		
		}
		else if (	(FL_TCS.lctcsu1OverBrkWhl == 1)||(RR_TCS.lctcsu1OverBrkWhl == 1)	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 0;
			SC_TCS.lctcsu1PreActTarCirPres = 1;
			PC_TCS.lctcsu1PreActReset = 0;
			SC_TCS.lctcsu1PreActReset = 1;		
		}
		else if (	(FR_TCS.lctcsu1HSSBrkWhl == 1)||(RL_TCS.lctcsu1HSSBrkWhl == 1)	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 1;
			SC_TCS.lctcsu1PreActTarCirPres = 0;
			PC_TCS.lctcsu1PreActReset = 1;
			SC_TCS.lctcsu1PreActReset = 0;		
		}
		else if (	(FL_TCS.lctcsu1HSSBrkWhl == 1)||(RR_TCS.lctcsu1HSSBrkWhl == 1)	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 0;
			SC_TCS.lctcsu1PreActTarCirPres = 1;
			PC_TCS.lctcsu1PreActReset = 0;
			SC_TCS.lctcsu1PreActReset = 1;		
		}
		else if (   ((PC_TCS.lctcsu1IniTCVCtlMode == 1) && (SC_TCS.lctcsu1IniTCVCtlMode == 0))	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 1;
			SC_TCS.lctcsu1PreActTarCirPres = 0;
			PC_TCS.lctcsu1PreActReset = 1;
			SC_TCS.lctcsu1PreActReset = 0;	
		}
		else if (   ((PC_TCS.lctcsu1IniTCVCtlMode == 0) && (SC_TCS.lctcsu1IniTCVCtlMode == 1))	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 0;
			SC_TCS.lctcsu1PreActTarCirPres = 1;
			PC_TCS.lctcsu1PreActReset = 0;
			SC_TCS.lctcsu1PreActReset = 1;	
		}
		else if (   ((PC_TCS.lctcsu1IniTCVCtlMode == 1) && (SC_TCS.lctcsu1IniTCVCtlMode == 1))
			     || (FL_TCS.lctcsu1VIBFlag_tcmf == 1)
			     || (FR_TCS.lctcsu1VIBFlag_tcmf == 1) 
			     || (RL_TCS.lctcsu1VIBFlag_tcmf == 1)
			     || (RR_TCS.lctcsu1VIBFlag_tcmf == 1)	)
		{
			PC_TCS.lctcsu1PreActTarCirPres = 0;
			SC_TCS.lctcsu1PreActTarCirPres = 0;
			PC_TCS.lctcsu1PreActReset = 0;
			SC_TCS.lctcsu1PreActReset = 0;	
		}
		else
		{
			PC_TCS.lctcsu1PreActTarCirPres = 1;
			SC_TCS.lctcsu1PreActTarCirPres = 0;
			PC_TCS.lctcsu1PreActReset = 0;
			SC_TCS.lctcsu1PreActReset = 0;	
		}
	}
	else
	{
		PC_TCS.lctcsu1PreActTarCirPres = 0;
		SC_TCS.lctcsu1PreActTarCirPres = 0;
		PC_TCS.lctcsu1PreActReset = 0;
		SC_TCS.lctcsu1PreActReset = 0;	
	}
}

static void LATCS_vCalActCirCnt(void)
{
	PC_TCS.lctcss16PreActCirCntOld = PC_TCS.lctcss16PreActCirCnt;
	SC_TCS.lctcss16PreActCirCntOld = SC_TCS.lctcss16PreActCirCnt;

	if (lctcsu1MuxBTCSWhlAtv == 1)
	{
		if ( (PC_TCS.lctcss16PreActCirCntOld == 0) && (SC_TCS.lctcss16PreActCirCntOld == 0) )
		{
			if (PC_TCS.lctcsu1PreActTarCirPres == 1)
			{	
				PC_TCS.lctcss16PreActCirCnt	= 1;
				SC_TCS.lctcss16PreActCirCnt = 0;
			}
			else if (SC_TCS.lctcsu1PreActTarCirPres == 1)
			{	
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt	= 1;
			}
			else
			{
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt = 0;	
			}
		}
		else
		{
			if ((PC_TCS.lctcsu1PreActTarCirPres == 0)&&(SC_TCS.lctcsu1PreActTarCirPres == 0))
			{
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt = 0;
			}	
			else if (    (PC_TCS.lctcss16PreActCirCntOld >= 5)
				     ||  ((SC_TCS.lctcsu1PreActReset == 1)&&(PC_TCS.lctcss16PreActCirCntOld >= 3)) )
			{
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt = 1;
			}
			else if (PC_TCS.lctcss16PreActCirCntOld > 0)
			{
				PC_TCS.lctcss16PreActCirCnt	= PC_TCS.lctcss16PreActCirCntOld + 1;	
				SC_TCS.lctcss16PreActCirCnt = 0;
			}
			else if (   (SC_TCS.lctcss16PreActCirCntOld >= 5)
					 || ((PC_TCS.lctcsu1PreActReset == 1)&&(SC_TCS.lctcss16PreActCirCntOld >= 3)) )			
			{
				PC_TCS.lctcss16PreActCirCnt = 1;
				SC_TCS.lctcss16PreActCirCnt = 0;
			}
			else if (SC_TCS.lctcss16PreActCirCntOld > 0)
			{
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt	= SC_TCS.lctcss16PreActCirCntOld + 1;		
			}
			else
			{
				PC_TCS.lctcss16PreActCirCnt = 0;
				SC_TCS.lctcss16PreActCirCnt = 0;	
			}	
		}
	}
	else
	{
		PC_TCS.lctcss16PreActCirCnt = 0;
		SC_TCS.lctcss16PreActCirCnt = 0;		
	}	
}

static void LATCS_vCalMeMryTarCirPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
{
	if (lctcsu1MuxBTCSWhlAtv == 1)
	{
		if (CIRCUIT->lctcss16PreActCirCnt == 1)
		{
			CIRCUIT->lctcss16MeMryTarCirPres = CIRCUIT->lctcss16CordTarCirPres;		
		}
		else
		{
			;
		}
	}
	else
	{		
		CIRCUIT->lctcss16MeMryTarCirPres = 0;
	}
}

static void LCTCS_vCalTarPresCmd(void)
{
	if (lctcsu1MuxBTCSWhlAtv == 1)
	{
		if (PC_TCS.lctcss16PreActCirCnt > 0)
		{
			PC_TCS.lctcss16TarCirPresCmd = PC_TCS.lctcss16CordTarCirPres;
			SC_TCS.lctcss16TarCirPresCmd = SC_TCS.lctcss16MeMryTarCirPres;
		}
		else if (SC_TCS.lctcss16PreActCirCnt > 0)
		{
			PC_TCS.lctcss16TarCirPresCmd = PC_TCS.lctcss16MeMryTarCirPres;
			SC_TCS.lctcss16TarCirPresCmd = SC_TCS.lctcss16CordTarCirPres;
		}
		else
		{
			if (FL_TCS.lctcsu1VIBFlag_tcmf == 1)
			{
				PC_TCS.lctcss16TarCirPresCmd = FL_TCS.lctcss16TarWhlPre;
				SC_TCS.lctcss16TarCirPresCmd = FL_TCS.lctcss16TarWhlPre;
			}
			else if (FR_TCS.lctcsu1VIBFlag_tcmf == 1)
			{
				PC_TCS.lctcss16TarCirPresCmd = FR_TCS.lctcss16TarWhlPre;
				SC_TCS.lctcss16TarCirPresCmd = FR_TCS.lctcss16TarWhlPre;
			}	
			else if (   (PC_TCS.lctcsu1IniTCVCtlMode == 1) 
			         && (SC_TCS.lctcsu1IniTCVCtlMode == 1) )
			{/*생각해봐야 할 부분*/         
				PC_TCS.lctcss16TarCirPresCmd = LCTCS_s16IFindMinimum(PC_TCS.lctcss16CordTarCirPres,SC_TCS.lctcss16CordTarCirPres);
				SC_TCS.lctcss16TarCirPresCmd = LCTCS_s16IFindMinimum(PC_TCS.lctcss16CordTarCirPres,SC_TCS.lctcss16CordTarCirPres);
			}
			else
			{
				if (FL_TCS.lctcsu1BTCSWhlAtv == 1)
				{
					PC_TCS.lctcss16TarCirPresCmd = FL_TCS.lctcss16TarWhlPre;
					SC_TCS.lctcss16TarCirPresCmd = FL_TCS.lctcss16TarWhlPre;
				}
				else
				{
					PC_TCS.lctcss16TarCirPresCmd = FR_TCS.lctcss16TarWhlPre;
					SC_TCS.lctcss16TarCirPresCmd = FR_TCS.lctcss16TarWhlPre;
				}
			}		
		}
	}
	else
	{
		PC_TCS.lctcss16TarCirPresCmd = PC_TCS.lctcss16CordTarCirPres;
		SC_TCS.lctcss16TarCirPresCmd = SC_TCS.lctcss16CordTarCirPres;
	}
}

#else
static void LCTCS_vCalTarPresInfCmd(void)
{
	PC_TCS.lctcss16TarCirPresCmd = PC_TCS.lctcss16CordTarCirPres;
	SC_TCS.lctcss16TarCirPresCmd = SC_TCS.lctcss16CordTarCirPres;
}
#endif /*(MULTIPLEX_TCS == ENABLE)*/


//static void LATCS_vLimitTarPresRate(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
//{
//	#if (TAR_PRES_RATE_LIMIT == ENABLE)
//	int16_t s16TarPresRate = 0;
//
//	CIRCUIT->lctcss16TarCirPresRateLmtdOld = CIRCUIT->lctcss16TarCirPresRateLmtd;
//
//	if(CIRCUIT->lctcss16TarCirPresRateLmtd > S16_TARGET_PRES_LEVEL_2)
//	{
//		s16TarPresRate = S16_TARGET_PRES_RATE_3;
//	}
//	else if(CIRCUIT->lctcss16TarCirPresRateLmtd > S16_TARGET_PRES_LEVEL_1)
//	{
//		s16TarPresRate = S16_TARGET_PRES_RATE_2;
//	}
//	else
//	{
//		s16TarPresRate = S16_TARGET_PRES_RATE_1;
//	}
//
//	if((CIRCUIT->lctcss16CordTarCirPres - CIRCUIT->lctcss16TarCirPresRateLmtdOld) > s16TarPresRate)
//	{
//		CIRCUIT->lctcss16TarCirPresRateLmtd = CIRCUIT->lctcss16TarCirPresRateLmtdOld + s16TarPresRate;
//	}
//	else
//	{
//		CIRCUIT->lctcss16TarCirPresRateLmtd = CIRCUIT->lctcss16CordTarCirPres;
//	}
//	CIRCUIT->lctcss16TarCirPresCmd = CIRCUIT->lctcss16TarCirPresRateLmtd;
//	#else
//	CIRCUIT->lctcss16TarCirPresCmd = CIRCUIT->lctcss16CordTarCirPres;
//	#endif /* (TAR_PRES_RATE_LIMIT == ENABLE) */
//}

static void LATCS_vTagetPress(void)
{
  latcss16PriTcTargetPress = PC_TCS.lctcss16TarCirPresCmd;
  latcss16SecTcTargetPress = SC_TCS.lctcss16TarCirPresCmd;
}

static void LATCS_vEstCirPresByTarPres(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
{
//		#ifdef __IDB_2_CIRCUIT_P_SENSOR
	PC_TCS.lctcss16EstCirPres = latcss16premFiltPressPri/10;
	SC_TCS.lctcss16EstCirPres = latcss16premFiltPressSec/10;
//		#else
    if (CIRCUIT->lctcsu1BrkCtlActCir == 1)
    {
        if ( ((CIRCUIT->lctcsu1IniTCVCtlMode==0) && (CIRCUIT->lctcsu1IniTCVCtlModeFalEdg==0)&&(VCA_ON_TCMF==0))
        	  ||((ltcsu8VCARunningCntTCMF>=U8TCSCpIniVCACtlModeTime) && (VCA_ON_TCMF==1 )) )
        {
            CIRCUIT->lctcss16TarCirPresCmdFlt = LCESP_s16Lpf1Int(CIRCUIT->lctcss16TarCirPresCmd, CIRCUIT->lctcss16TarCirPresCmdFlt, U8TCSCpCurntFiltGain);
            
            if(CIRCUIT->lctcss16TarCirPresCmdFlt <= TCSMinimumPressureLevel) /*Minimum Target Pressure*/
            {
            	CIRCUIT->lctcss16EstCirPres_2 = 0;
            }
            else
            {
            	CIRCUIT->lctcss16EstCirPres_2 = CIRCUIT->lctcss16TarCirPresCmdFlt;
            }
        }
        else
        {
            CIRCUIT->lctcss16TarCirPresCmdFlt = 0;
            CIRCUIT->lctcss16EstCirPres_2 = 0;
        }
    }
    else
    {
        CIRCUIT->lctcss16TarCirPresCmdFlt = 0;
        CIRCUIT->lctcss16EstCirPres_2 = 0;
    }
//    	#endif
}


static void LSESP_vCalPressurePri(void)
{
	latcss16premFiltPressOldPri = latcss16premFiltPressPri ;
	tempW3 = lis16MCPRawPress;
	latcss16premFiltPressPri = LCESP_s16Lpf1Int(tempW3,latcss16premFiltPressOldPri,L_U8FILTER_GAIN_10MSLOOP_7HZ);


	if(latcss16premFiltPressPri <= 0)
	{
		latcss16premFiltPressPri = 0;
	}
	else
	{
		;
	}


}

static void LSESP_vCalPressureSec(void)
{
	latcss16premFiltPressOldSec = latcss16premFiltPressSec ;
	tempW3 = lis16MCP2RawPress;
	latcss16premFiltPressSec = LCESP_s16Lpf1Int(tempW3,latcss16premFiltPressOldSec,L_U8FILTER_GAIN_10MSLOOP_7HZ);


	if(latcss16premFiltPressSec <= 0)
	{
		latcss16premFiltPressSec = 0;
	}
	else
	{
		;
	}


}







static void LAMSC_vSetTCSTargetVoltage(void)
{
	LATCS_vSetMtrVolt4Axle(&FA_TCS);
	LATCS_vSetMtrVolt4Axle(&RA_TCS);
	LATCS_vSetMtrVoltAddInSpHill(&FA_TCS);
	LATCS_vSetMtrVoltAddInSpHill(&RA_TCS);
	LATCS_vSetMtrVolt4VCA();
	LATCS_vSetMtrVolt4Veh();
}

void LCMSC_vSetTCSTargetVoltage(void)
{
	/*Don't Call Function LCMSC_vSetTCSTargetVoltage() In LAABSCallMotorSpeedControl.c*/	
}

static void LATCS_vSetMtrVolt4Axle(struct TCS_AXLE_STRUCT *Axle_TCS)
{	
	if(Axle_TCS->lctcsu1BrkCtlActAxle==1)
	{
		if(Axle_TCS->lctcsu1SymSpinDctFlg==0)
		{
			if(lctcsu1IniMtrCtlMode==1)
			{
					Axle_TCS->ltcss16MtrVolt=LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)S16TCSCpIniRMdeMtrV_ASC,
                    	                                                       100, (int16_t)S16TCSCpIniRMdeMtrV_Turn);
			}
			else if(Axle_TCS->ltcsu1ModOfAsymSpnCtl == S16TCSAsymSpinFwdCtlMode)
			{
				 Axle_TCS->ltcss16MtrVolt=LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)S16TCSCpFFMtrV_ASC,
                                                                          100, (int16_t)S16TCSCpFFMtrV_Turn);
			}
			else
			{
				Axle_TCS->ltcss16MtrVolt=LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)S16TCSCpFBMtrV_ASC,
                                                                          100, (int16_t)S16TCSCpFBMtrV_Turn);
			}
		}
		else
		{
			if(lctcsu1IniMtrCtlMode==1)
			{
				Axle_TCS->ltcss16MtrVolt = (int16_t)S16TCSCpIniRMdeMtrV_SC;
			}
			else
			{
				Axle_TCS->ltcss16MtrVolt = (int16_t)S16TCSCpNormMtrV_SC;
			}
		}
	}
	else
	{
		Axle_TCS->ltcss16MtrVolt=0;
	}
}

static void LATCS_vSetMtrVoltAddInSpHill(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if( (Axle_TCS->lctcsu1BrkCtlActAxle==1)&&(FA_TCS.lctcsu1SymSpinDctFlg==0)&&(RA_TCS.lctcsu1SymSpinDctFlg==0) )
	{
		if ( lctcsu1DctSplitHillFlg == 1 )
		{
			if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuStable)
			{	/* Wheel on high-mu is stable */
				if (Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb > 0)
				{
					Axle_TCS->ltcss16MtrVolt = Axle_TCS->ltcss16MtrVolt + S16TCSCpAddMtrV4SPHillHghMuUnSt;
					Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb = Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb - 1;
				}
				else
			{
				Axle_TCS->ltcss16MtrVolt = Axle_TCS->ltcss16MtrVolt + S16TCSCpAddMtrV4SPHillHghMuStab;
					Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb = 0;
				}
				
			}
			else
			{	/* Wheel on high-mu is unstable */
				Axle_TCS->ltcss16MtrVolt = Axle_TCS->ltcss16MtrVolt + S16TCSCpAddMtrV4SPHillHghMuUnSt;
				Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb = U8TCSCpMtrFstRnTmrAftHghMuWhStb;				
			}
		}
		else
		{
			Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb = 0;
		}
	}
	else
	{
		Axle_TCS->ltcsu8MtrFstRnTmrAftHghMuWhlStb = 0;
	}	
}

static void LATCS_vSetMtrVolt4VCA(void)
{
	tcs_tempW0 = (int16_t)U8TCSCpRearCtlOfVCA;
	
	if (VCA_ON_TCMF == 1)
	{
		if (ltcsu8VCARunningCntTCMF<U8TCSCpIniVCACtlModeMtorTime) 
	  	{
	  		if (tcs_tempW0 == 1)
	  		{
	  			FA_TCS.ltcss16MtrVolt = 0; 
		  		RA_TCS.ltcss16MtrVolt = S16TCSCpInitMaxMotorV_VCA;
	  		}
	  		else
	  		{	
		  		FA_TCS.ltcss16MtrVolt = S16TCSCpInitMaxMotorV_VCA; 
		  		RA_TCS.ltcss16MtrVolt = 0;
		  	}
	  	}
	  	else
	  	{	
	  		if (tcs_tempW0 == 1)
	  		{	
		  		FA_TCS.ltcss16MtrVolt = 0; 
		  		RA_TCS.ltcss16MtrVolt = S16TCSCpCtlMotorV_VCA;
	  		}
		  	else
		  	{
		  		FA_TCS.ltcss16MtrVolt = S16TCSCpCtlMotorV_VCA; 
		  		RA_TCS.ltcss16MtrVolt = 0;
		  	}
	  	}
	}
	else
	{
		;
	}
}

static void LATCS_vSetMtrVolt4Veh(void)
{
	TCS_MSC_MOTOR_ON = BTC_fz; 
	
	tcs_msc_target_vol = LCTCS_s16IFindMaximum(FA_TCS.ltcss16MtrVolt,RA_TCS.ltcss16MtrVolt);

}

static void LATCS_vSetTarPreGrdtMd(void)
{
	if(tcs_msc_target_vol >= MSC_8_V)
	{
		latcss16TarPreGrdtMd  = 2;/*TCS Fast Rise Function(300bar/sec)*/
		latcss16TarPreRateMd = 300;/*TCS Target Pressure Rate(300bar/sec)*/
	}
	else if(tcs_msc_target_vol >= MSC_5_V)
	{
		latcss16TarPreGrdtMd  = 3;/*TCS Fast Rise Function(200bar/sec)*/
		latcss16TarPreRateMd = 200;/*TCS Target Pressure Rate(200bar/sec)*/
	}
	else if(tcs_msc_target_vol >= MSC_3_V)
	{
		latcss16TarPreGrdtMd  = 6;/*TCS Rise Function(150bar/sec)*/
		latcss16TarPreRateMd = 150;/*TCS Target Pressure Rate(150bar/sec)*/
	}
	else if(tcs_msc_target_vol > MSC_0_V)
	{
		latcss16TarPreGrdtMd  = 7;/*TCS Hold Function(100bar/sec)*/
		latcss16TarPreRateMd = 100;/*TCS Target Pressure Rate(100bar/sec)*/
	}
	else
	{
		latcss16TarPreGrdtMd  = 0;
	}
}


static void LATCS_vSetTarPreGrdtMdEdge(void)
{
  	PC_TCS.lctcss16TarCirPresCmdOld = PC_TCS.lctcss16TarCirPresCmd;
  	SC_TCS.lctcss16TarCirPresCmdOld = SC_TCS.lctcss16TarCirPresCmd;

  	/* Primary Circuit */

	if (PC_TCS.lctcss16TarCirPresCmd >= PC_TCS.lctcss16TarCirPresCmdOld)
	{
		latcss16PricTarPreRate = latcss16TarPreRateMd;
	}
	else
	{
		latcss16PricTarPreRate = latcss16TarPreRateMd*(-1);
	}

	/* Secondary Circuit */

	if (SC_TCS.lctcss16TarCirPresCmd >= SC_TCS.lctcss16TarCirPresCmdOld)
	{
		latcss16SeccTarPreRate = latcss16TarPreRateMd;
	}
	else
	{
		latcss16SeccTarPreRate = latcss16TarPreRateMd*(-1);
	}
}


void LATCS_vCtlNoNcValve(void)
{
	/* BTCS is not activated */
	if	( (FL.BTCS==0) && (FR.BTCS==0) && (RL.BTCS==0) && (RR.BTCS==0) )
	{
		/* MUST NOT try to control any valves : ABS has authority to control the valves in this case */
	}
	else if	( (FL.BTCS==1) && (FR.BTCS==0) && (RL.BTCS==0) && (RR.BTCS==0) )  /*FL*/
	{
		/*FL*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSDumpMode(&RR);
	}
	else if ( (FL.BTCS==0) && (FR.BTCS==1) && (RL.BTCS==0) && (RR.BTCS==0) )  /*FR*/
	{
		/*FR*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSDumpMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}
	else if ( (FL.BTCS==0) && (FR.BTCS==0) && (RL.BTCS==1) && (RR.BTCS==0) )  /*RL*/
	{
		/*RL*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSDumpMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}
	else if ( (FL.BTCS==0) && (FR.BTCS==0) && (RL.BTCS==0) && (RR.BTCS==1) )  /*RR*/
	{
		/*RR*/
		LCTCS_vControlBTCSDumpMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}	
	/* 2 Wheels Control */
	else if ( (FL.BTCS==1) && (FR.BTCS==1) && (RL.BTCS==0) && (RR.BTCS==0) )  /*FL-FR*/
	{
		/*FL-FR*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSDumpMode(&RL);
		LCTCS_vControlBTCSDumpMode(&RR);
	}
	else if ( (FL.BTCS==1) && (FR.BTCS==0) && (RL.BTCS==1) && (RR.BTCS==0) )  /*FL-RL*/
	{
		/*FL-RL*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSDumpMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSDumpMode(&RR);
	}		
	else if ( (FL.BTCS==1) && (FR.BTCS==0) && (RL.BTCS==0) && (RR.BTCS==1) )  /*FL-RR*/
	{
		/*FL-RR*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}			
	else if ( (FL.BTCS==0) && (FR.BTCS==1) && (RL.BTCS==1) && (RR.BTCS==0) )  /*FR, RL*/
	{
		/*FR-RL*/
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}				
	else if ( (FL.BTCS==0) && (FR.BTCS==1) && (RL.BTCS==0) && (RR.BTCS==1) )  /*FR, RR*/
	{
		/*FR-RR*/
		LCTCS_vControlBTCSDumpMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSDumpMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}					
	else if ( (FL.BTCS==0) && (FR.BTCS==0) && (RL.BTCS==1) && (RR.BTCS==1) )  /*RL, RR*/
	{
		/*RL-RR*/
		LCTCS_vControlBTCSDumpMode(&FL);
		LCTCS_vControlBTCSDumpMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);
	}
	/* 3 or 4 Wheel Control : Not Allowed */
	else
	{
		LCTCS_vControlBTCSReapplyMode(&FL);
		LCTCS_vControlBTCSReapplyMode(&FR);
		LCTCS_vControlBTCSReapplyMode(&RL);
		LCTCS_vControlBTCSReapplyMode(&RR);		
	}	        
}

void LCTCS_vControlBTCSReapplyMode(struct W_STRUCT *W_L)
{
    W_L->AV_REAPPLY=1;
    W_L->AV_HOLD=0;
    W_L->BUILT=0;
    W_L->AV_DUMP=0;
}

void LCTCS_vControlBTCSDumpMode(struct W_STRUCT *W_L)
{
    W_L->AV_REAPPLY=0;
    W_L->AV_HOLD=0;
    W_L->BUILT=0;
    W_L->AV_DUMP=1;
}

static void LATCS_vSetWheelValveCommand(void)
{
	if((BTCS_fr==1) || (BTCS_rl==1) || (BTCS_fl==1) || (BTCS_rr==1))
	{
		LATCS_vSetWheelValveOutput(&FL, &latcs_FL1HP, &latcs_FL2HP);
		LATCS_vSetWheelValveOutput(&FR, &latcs_FR1HP, &latcs_FR2HP);
        LATCS_vSetWheelValveOutput(&RL, &latcs_RL1HP, &latcs_RL2HP);
		LATCS_vSetWheelValveOutput(&RR, &latcs_RR1HP, &latcs_RR2HP);
		la_FR1HP = latcs_FR1HP;
		la_FR2HP = latcs_FR2HP;
		la_RL1HP = latcs_RL1HP;
		la_RL2HP = latcs_RL2HP;
		la_FL1HP = latcs_FL1HP;
		la_FL2HP = latcs_FL2HP;
		la_RR1HP = latcs_RR1HP;
		la_RR2HP = latcs_RR2HP;
	}
	else 
    { 
        LA_vResetWheelValvePwmDuty(&latcs_FR1HP);
        LA_vResetWheelValvePwmDuty(&latcs_FR2HP);
        LA_vResetWheelValvePwmDuty(&latcs_RL1HP);
        LA_vResetWheelValvePwmDuty(&latcs_RL2HP);
		LA_vResetWheelValvePwmDuty(&latcs_FL1HP);
        LA_vResetWheelValvePwmDuty(&latcs_FL2HP);
        LA_vResetWheelValvePwmDuty(&latcs_RR1HP);
        LA_vResetWheelValvePwmDuty(&latcs_RR2HP);
    }
}

static void LATCS_vSetWheelValveOutput(struct W_STRUCT *WL_temp, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
    uint8_t u8NoOnTime, u8NcOnTime;
    
    WL = WL_temp;
    
    if(AV_DUMP_wl == 1)
    {
        HV_VL_wl=1;
        AV_VL_wl=1;
        
        if(BTCS_wl==1)
        {
            if(REAR_WHEEL_wl==0)
            {
                	u8NcOnTime = U8_BASE_CTRLTIME;
            	}
            	else
            	{
                	u8NcOnTime = U8_BASE_CTRLTIME;
            }
        }
        else
        {
            u8NcOnTime = U8_BASE_LOOPTIME;/*U8_BASE_CTRLTIME;*/
        }
        
        LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, u8NcOnTime);
    }
    else if(AV_HOLD_wl == 1) 
    {
        HV_VL_wl=1;
        AV_VL_wl=0;
        
        LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
    }
    else 
    {
        HV_VL_wl=0;
        AV_VL_wl=0;

        	if(BTCS_wl==1)
        	{
            	if(REAR_WHEEL_wl==0)
            	{
                	u8NoOnTime = U8_TCS_APPLY_TIME_FRT;
            	}
            	else
            	{
                	u8NoOnTime = U8_TCS_APPLY_TIME_RR;
            	}
        	}
        	else
        	{
            	u8NoOnTime = U8_BASE_LOOPTIME;/*U8_BASE_CTRLTIME;*/
        	}
        	LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, u8NoOnTime);
    	}         
}

#endif  /* tag 2 */   
#endif 	/* tag 1 */

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LATCSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
