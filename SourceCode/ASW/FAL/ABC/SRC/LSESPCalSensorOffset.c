
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCalSensorOffset
	#include "Mdyn_autosar.h"
#endif
#include "LSESPCalSensorOffset.h"
 
#if __GM_FailM == 1
#define __YAW_TEMPERATURE_COMPENSATION	0
#else
#define __YAW_TEMPERATURE_COMPENSATION	1  
#endif
#define YAW_OFFSET_MAX_ERR              250  /* 2.5deg/s */
#define YAW_OFFSET_FS_ERR_DCT           600  /* 6  deg/s */
#define YAW_OFFSET_MAX_CORR             450  /* 4.5deg/s */
#define YAW_THRESHOLD_ADD_MAX           200  /* 2.0deg/s */

#if __CAR==GM_M300
#define  __BLS_CAN_VALID_CHECK          1
#else
#define  __BLS_CAN_VALID_CHECK          0
#endif

#define __DEL_360_OFFSET                1

#if __TEST_MODE_ENABLE==1
#define __TEST_MODE_YAW_TMP_SRCH		0	/*default :0	Test Searching Yaw Map*/
#define __ERASE_YAW_TMP_MAP				0	/*default :0	Erase Yaw Map*/
#endif

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
#define  EEP_RETRY                 1
#define  EEP_DELAY_T               (L_U8_TIME_10MSLOOP_1000MS-2)
#define  EEP_CALC_T                L_U8_TIME_10MSLOOP_500MS
#define  PDF_OFS_LM_THR            S16_TRAVEL_22_MM
#define  PDT_OFS_LM_THR            S16_TRAVEL_22_MM
#define  PDF_OFS_MAX_MIN_THR       S16_TRAVEL_2_MM
#define  PDT_OFS_MAX_MIN_THR       S16_TRAVEL_2_MM
#define  PRES_OFS_LM_THR           MPRESS_15BAR
#define  PRES_OFS_MAX_MIN_THR      MPRESS_15BAR
#define  PEDAL_STB_THR             S16_TRAVEL_2_MM
/* Drving Correction threshold */
#define  PEDAL_DRV_CORRECT_THR     S16_TRAVEL_22_MM

/* Drving EEPROM */
#define  PEDAL_DRV_EEP_OFS_MAX     S16_TRAVEL_22_MM
#define  PEDAL_DRV_EEP_OFS_UPD_THR S16_TRAVEL_1_MM
#define  PRES_DRV_EEP_OFS_MAX      MPRESS_15BAR
#define  PRES_DRV_EEP_OFS_UPD_THR  MPRESS_1BAR

/* 2012_SWD_KJS */
#else
#define  PRES_DRV_EEP_OFS_MAX      MPRESS_15BAR
#define  PRES_DRV_EEP_OFS_UPD_THR  MPRESS_1BAR
/* 2012_SWD_KJS */

#endif

#if __AHB_SYSTEM==1
#define  MP_OFS_VREF_MIN           VREF_3_KPH
#endif

#include "LSESPCalSensorOffset.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"
#include "LSESPCalSlipRatio.h"
#include "LCallMain.h"
#include "LSABSCallSensorSignal.h"
#include "LCABSCallControl.h"
#include "LDABSCallDctForVref.H"
#include "hm_logic_var.h"

#if __SCC
#include "../SRC/SCC/LCWPCCallControl.H"
#endif

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
#define  PRESSURE_EEP_OFFSET_MAX   MPRESS_7BAR
#define  PRESSURE_EEP_UPDATE_TH    MPRESS_1BAR 
#endif

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
#define  BAS_THRES_1           10   // 1 --> 0.1 mm Temp
#define  BAS_OFFSET_LIMIT      100  // Temp
#define  BAS_CHECK_OFFSET      50   // Temp
#define  BAS_EEP_OFFSET_MAX    50   // Think
#define  BAS_EEP_UPDATE_TH     10   // Think
#endif

#define WSTR_MAX2 (WSTR_MAX+900)
#if __VDC


/*========  Model */
static void LSESP_vChkSensorModel(void);
static void LSESP_vCalSensorModel(void);
static void LSESP_vCalSensorResidual(struct OFFSET_STRUCT *PTR);
static void LSESP_vDctStraightDriving(void);
static void LSESP_vDctCurveDriving(void);
static void LSESP_vResetDrivingOffset(struct OFFSET_STRUCT *PTR);
static void LSESP_vCalDrivingSensorOffset(struct OFFSET_STRUCT *PTR);
static void LSESP_vCalSensorReferenceModel(struct OFFSET_STRUCT *PTR);
static void LSESP_vDctSpinningDriving(void);
static void LSESP_vSortSensorModel(struct ARRANGE_STRUCT SORT[], uint8_t n);
static void LSESP_vChkControlHold(void);


/*========  Yaw */
/*-- Temperature offset */
static void LSESP_vCalYawLgTemperature(void);
static void LSESP_vCalTemperatureYawOffset(void);
static void LSESP_vLoadYawTmpMap(void);
static void LSESP_vDetTemperatureYawOffset(void);
static void LSESP_vSaveYawTmpMap(void);
static void LSESP_vUpdateYawTmpMap(void);
static void LSESP_vChkUpdateYawTmpMap(int8_t n);
static void LSESP_vLimitYawTmpMap(void);
static void LSESP_vResetYawTmpMap(void);
#if TC27X_FEE!=ENABLE
static void LSESP_vAddressYawTmpMap(void);
#endif
static void LSESP_vSrchYawTmpMap(void);
static void LSESP_vLoadTestSrchYawTmp(void);
static void LSESP_vEraseYawTmpMap(void);
static void LSESP_vChkYawOffset(void);
/*-- Standstilloffset */
static void LSESP_vCalStandstillReliability(void);
static void LSESP_vChkTurnOnStandstill(void);
static void LSESP_vChkTurnOnStandstate(void);
static void LSESP_vChkCurrentStandstill(void);

static void LSESP_vDctStandState(void);              /*Without Temperature Offset */
static void LSESP_vCalStandYawOffset(void);

/*-- Others */
static void LSESP_vLoadDrivingOffsetParameter(void);


/*========  Ay */
static void LSESP_vCalYawLgDrvOff(void);


/*========  MP */
#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
static void LSESP_vCalPressRawSignal(void);
static void LSESP_vCalMpressOffset(struct  PREM_PRESS_OFFSET_STRUCT  *PRESS);
static void LSESP_vDetPressureOffset(struct  PREM_PRESS_OFFSET_STRUCT  *PRESS);

#else
static void LSESP_vCalMpressOffset(void);
#endif

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
/*=========  BAS  */
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
static void LSESP_vCalBrkAppSenOffset(void);
static void LSESP_vDetBrkAppSenOffset(void);
static void LSESP_vDetEachBrkAppSenOffset(void);

/*--- EOL EEPROM offset--*/
static void LSESP_vCalcEEPSensorOffset(void);
static void LSESP_vWritEEPSensorOffset(void);
static void LSESP_vWritEEPReqSensorOfs(struct ESP_EEP_offsetCalc  *ESP_EEP_CALC);
static void LSESP_vReadEEPSensorOffset(void);
static void LSESP_vCalcEachEEPSensorOffset(int16_t SensorRawSig , int16_t OFS_THR, int16_t OFS_DIFF_THR);
static void LSESP_vCalSensorOffsetFinal(void);
static void LSESP_vDetEachEEPSensorOfsOK(struct ESP_EEP_offsetCalc  *ESP_EEP_CALC, int16_t EEPReadOfs, uint8_t  EEPReadInvalid , int16_t OFS_THR);
static void LSESP_vDetEachEolEEPSensorOffset(int16_t X, int16_t Y, int16_t Z);
static void LSESP_vEepromOfsCalcReset(void);
static void LSESP_vDetEOLeepOfsDelayTime(void);

/*--- Drv offset--*/
static void LSESP_vCalPedalDrvOffset(void);
static void LSESP_vCalEachPedalDrvOffset(void);
static void LSESP_vDctPedalOffCondition(void);
static void LSESP_vCalPedalDrvQuickOffset(void);
static void LSESP_vDetPedalMPDrvEepOfsReset(void);

#endif    
#endif /* AHB_GEN3 */


/*========  SAS */
static void LSESP_vDctPHZAngle(void);
static void LSESP_vCalStableSteerOffset(void);
static void LSESP_vChkSASOk(uint8_t lsespu8TempStrAccInput);
static void LSESP_vChkInitDrivSteerOffset(void);
static void LSESP_vChkFinDrivSteerOffset(void);
static void LSESP_vResetStableSteerOffset(void);
static void LSESP_vResetPHZAngle(void);
static void LSESP_vCalStrDrvOff(void);
static void LSESP_vDctSpinCount( uint8_t, int16_t );
static void LSESP_vDctSpinControl(void);
static void LSESP_vDctDragCount(uint8_t, int16_t);
static void LSESP_vDctSpinDelayTime(void);
static void LSESP_vDctSpinDelayTimeReset(void);


/*========  EEPROM */
static void LSESP_vCalEEPROMSensorOffset(void);
#if  TC27X_FEE!=ENABLE
uint16_t LSESP_u16WriteEEPROM(uint16_t addr,uint16_t data);
#endif
static void LSESP_vReadEEPROMSensorOffset(void);
static void LSESP_vDetEEPROMOffset(void);


/*========= Determine Calc offset */
static void LSESP_vCalRawSensorSignal(void);
static void LSESP_vDetSensorOffset(void);
static int16_t LSESP_s16FilterPecentage(int16_t n, int16_t n_1, uint8_t percent);
static void LSESP_vDetYawTempCompOffset(void);
static void LSESP_vDetYawNormOffset(void);
static void LSESP_vDetSteerOffset(void);
static void LSESP_vDetLatAccOffset(void);

/*========= GM SSTS */
static void LSESP_vCheckOperateCondition(void);


 

uint8_t lsespu8Yawrate1SecCnt;
int16_t lsesps16YawrateMean1Sec;
int32_t lsesps32YawrateSum1Sec;
int16_t lsesps16YawOffsetPile[10];
uint8_t lsespu8YawOffsetPileIndex;
int16_t lsesps16YawTmpMean1Sec;
int32_t lsesps32YawTmpSum1Sec;
int16_t lsesps16YawTmpPile[10];
uint8_t lsespu8YawTmpReadDoneFlg[31];
int16_t lsesps16YawTmpEEPMap[31];
int16_t lsesps16YawTmpMap[31];
uint8_t lsespu8YawTmpReadIndex;
uint16_t lsespu16YawTmpAddress[31];
int16_t lsesps16YawTmpLimit[31];
uint8_t lsespu8YawTmpEEPOKFlg[31];
uint8_t lsespu8YawTmpOKFlg[31];
uint8_t lsespu8YawTmpInitRqFlg[31];
uint8_t lsespu8YawTmpWriteRqFlg[31];
int8_t lsesps8YawTmpWriteIndex;
int16_t lsesps16Tmp1Sec;
uint8_t lsespu8Tmp1SecCnt;
int32_t lsesps32Tmp1secSum;
int16_t lsesps16YawOffsetByTmp;
int8_t lsesps8SrchYawTmpMapIndex;
int8_t lsesps8SrchTestCnt;
int8_t lsesps8SrchYawTmpMapMinusCnt;
int8_t lsesps8SrchYawTmpMapPlusCnt;
int16_t lsesps16Yawrate100msSlope;
int16_t lsesps16LatAccel100msSlope;
int16_t lsesps16Yawrate100msMean;
int16_t lsesps16LatAccel100msMean;
int32_t lsesps32Yawrate100msSum;
int32_t lsesps32LatAccel100msSum;
int16_t lsesps16Yawrate100msStableCnt;
int16_t lsesps16LatAccel100msStableCnt;

int16_t lsesps16Yawrate1secMean;
int16_t lsesps16LatAccel1secMean;
int16_t lsesps16YawTmp1SecMean;
int32_t lsesps32Yawrate1secSum;
int32_t lsesps32LatAccelsecSum;
int32_t lsesps32YawTmp1SecSum;
int8_t lsesps8YawTmp1SecIndex;
int16_t  lsesps16Yawrate1secMeanOld;
int16_t  lsesps16LatAccel1secMeanOld;
int16_t  lsesps16Yawrate1secSlope;
int16_t  lsesps16LatAccelsecSlope;
int16_t  lsesps16Yawrate1secMeanOver;

int16_t steer_360_chk_timer3;
int16_t steer_360_chk_timer8;
uint16_t steer_rate_deg_s2;
uint8_t lsespu8Mean100msCnt;
uint8_t lsespu8Mean1secCnt;
uint8_t lsespu8StableCheckDelayCnt;
int16_t lsesps16StableSteerOffsetTemp;
int16_t lsesps16CntDelaySASCheck;

uint16_t lsespu16CntLatGUpdate;
int16_t lsesps16TempLatAccelOffsetOld;

int16_t lsesps16MaxYawThrAdd;
int16_t lsesps16MaxLatGThrAdd;
int16_t lsesps16MaxSteerThrAdd;

uint16_t lsespu16YawLgSnHigh;
uint16_t lsespu16YawLgSnLow;
uint16_t lsespu16YawLgSnHighOld;
uint16_t lsespu16YawLgSnLowOld;

SEN_OFFSET_FLG lsespu8SenOffFlg1;
SEN_OFFSET_FLG lsespu8SenOffFlg2;
SEN_OFFSET_FLG lsespu8SenOffFlg3;
SEN_OFFSET_FLG lsespu8SenOffFlg4;
SEN_OFFSET_FLG lsespu8SenOffFlg5;
SEN_OFFSET_FLG lsespu8SenOffFlg6;
SEN_OFFSET_FLG lsespu8SenOffFlg7;
SEN_OFFSET_FLG lsespu8SenOffFlg8;
SEN_OFFSET_FLG lsespu8SenOffFlg9;

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
SEN_OFFSET_FLG lsespu8SenOffFlg10;
SEN_OFFSET_FLG lsespu8SenOffFlg11;
#endif
SEN_OFFSET_FLG lsespu8SenOffFlg12;

int16_t lsesps16MoitorOffsetVar01;
int16_t lsesps16MoitorOffsetVar02;
int16_t lsesps16MoitorOffsetVar03;

uint16_t lsespu16MoitorOffsetVar01;
uint16_t lsespu16MoitorOffsetVar02;
uint16_t lsespu16MoitorOffsetVar03;

/*----- Offset Spin -----------*/
uint8_t lsespu8CtrlHold1secCnt;

uint16_t lsesp16OfsSpinCNT[4];
uint8_t OFST_SPIN[4];

uint8_t OFST_DRAG[2];
uint16_t  OFST_DRAG_CNT[2];

uint8_t OFST_CONTR;
uint8_t OFST_CONTR1;
uint8_t OFST_CONTR_SET_CNT;
uint16_t  SPIN_DELAY_T;
uint16_t  SPIN_DELAY_TimeCalc;
uint8_t lsespu8spinCalc1secCNT;
uint8_t lsespu8spinCalcCNT;
uint16_t  lsespu16MuEst1secAverage;
uint16_t  lsespu16MuEstAverage;
int32_t  lsesps32MuEstSum;

uint8_t INHIBIT_OFST_SPIN_1;
uint8_t INHIBIT_OFST_SPIN_2;
uint8_t INHIBIT_OFST_SPIN_3;
uint8_t INHIBIT_OFST_SPIN_4;

/*----- GM SSTS -----------------*/
uint16_t  lsespu16ESPnotReadyCheckTime;


/*----- Yaw Standstill state check ---*/
uint16_t  lsespu16ABSfz1MinAfterCount;
uint8_t lsespu8DeltaYawrateCnt;
int32_t   lsesps32DeltaYawrateSum;
int16_t   lsesps16DeltaYawrate250mMean;
int16_t   lsesps16DeltaYawrate250mMeanMax;
int16_t   lsesps16DeltaYawrate250mMeanMin;
int16_t   lsesps16DeltaYawrate250mDiff;
uint8_t lsespu8DeltaYawrateStableCnt;

/*----- Yaw Standstill offset calc ---*/
uint8_t lsespu8YawStandOffsetCalcCNT;
int32_t   lsesps32YawStandOffset1secSum;
int16_t   lsesps16YawStandOffset1secMean;
int16_t   lsesps16YawStandOffset;
int16_t   lsesps16YawStandOffsetOld;
int16_t   lsesps16YawStandOffsetOld1;
int16_t   lsesps16YawStandOffsetOld2;
uint8_t   lsespu8YawStandCNT;
int16_t   lsesps16YawSpcOvrThres;

/*----  Yaw Normal Offset EEP -----*/
int16_t   lsesps16EEPYawStandOfsRead;

int16_t lsesps16WheelVelocityFL;
int16_t lsesps16WheelVelocityFR;
int16_t lsesps16WheelVelocityRL;
int16_t lsesps16WheelVelocityRR;

#if(__YAW_SS_MODEL_ENABLE==1)
int16_t lsesps16YawSS_temp;
#endif

/* SAS Count     */
uint8_t lsespu8SASsigStbCnt;

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)

struct  PREM_PRESS_OFFSET_STRUCT  *PRESS, PRESS_M  ;

#if  __FL_PRESS == ENABLE
struct  PREM_PRESS_OFFSET_STRUCT  PRESS_FL ;
#endif

#if  __FR_PRESS == ENABLE
struct  PREM_PRESS_OFFSET_STRUCT  PRESS_FR ;
#endif

#if  __RL_PRESS == ENABLE
struct  PREM_PRESS_OFFSET_STRUCT  PRESS_RL ;
#endif

#if  __RR_PRESS == ENABLE
struct  PREM_PRESS_OFFSET_STRUCT  PRESS_RR ;
#endif

int16_t lsesps16Temp1;

#else

#endif
 
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
int16_t  lsesps16BrkAppSenRaw;
int16_t  lsesps16BrkAppSenRaw2;

int16_t  lsesps16BrkAppSenPDTFilt;
int16_t  lsesps16BrkAppSenPDFFilt;
int16_t  lsesps16BrkAppSenFiltF;

int16_t  lsesps16BASOfsPDFFinal;
int16_t  lsesps16BASOfsPDTFinal;

int16_t  lsesps16MpresEolFirstOfs;

#else

int16_t  lsesps16BrkAppSenRawGM;
int16_t  lsesps16BrkAppSenGMFilt;
int16_t  lsesps16BASOfsFinalGM;
int16_t  lsesps16BrkAppSenFiltF;

#endif

/* Mpress offset */
int16_t  lsesps16MpresDrvOfs;
int16_t  lsesps16MpresOfsFinal;
int16_t  lsesps16MpresDrvOfsEEPw;
int16_t  lsesps16MpresDrvEEPOfs;

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))

struct  PEDAL_offset   *PEDAL_CALC,PDF, PDT ;
struct  ESP_EEP_offsetCalc	*ESP_EEP_CALC, *ESP_CALC, ESP_PDF, ESP_PDT,ESP_MP;

#endif

//extern int16_t   read_yaw_eeprom_offset;
//extern uint8_t   com_rx_buf[8];
//extern int16_t   yaw_eeprom_offset_write;


void LSESP_vCalSensorOffset(void)
{
	/*Failsafe Raw Signal -> Logic Raw Signal*/
	LSESP_vCalRawSensorSignal();

	/*Load Driving Offset Parameter*/
	LSESP_vLoadDrivingOffsetParameter();


  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */	
  if(wu8IgnStat == CE_ON_STATE)
  {
	/*Calculate EEPROM Offset*/
	   LSESP_vCalEEPROMSensorOffset();
	   LSESP_vReadEEPROMSensorOffset();
  }
  else
  {
    ;
  }
  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */

    /* Vehicle Stand state Detection and Yaw-rate stand offset Calculation */
    LSESP_vDctStandState();
    LSESP_vCalStandYawOffset();
    

#if __YAW_TEMPERATURE_COMPENSATION

	/*Calculate delta Raster Frequency -> Temperature*/
	if(fu1SenRasterSuspect==0)
	{
	  LSESP_vCalYawLgTemperature();
	}
	else
	{
	  raster_tmp_spec_over_flg=1;
	}

	/*Calculate Yaw-Tmp Map -> Yaw Offset*/
   	if(fu1SenRasterSuspect==0)
   	{
	  LSESP_vCalTemperatureYawOffset();
	  }
	  else
	  {
	    ;
	  }

#endif

    LSESP_vCalStandstillReliability();

	/*Calculate Sensor Model with other sensors*/
	LSESP_vCalSensorModel();
	LSESP_vChkSensorModel();

	/*Calculate Reference Sensor Model*/
	LSESP_vCalSensorReferenceModel(&KSTEER);
	LSESP_vCalSensorReferenceModel(&KYAW);
	LSESP_vCalSensorReferenceModel(&KLAT);

	/*Detect Driving Condition*/
	LSESP_vDctStraightDriving();
	LSESP_vDctCurveDriving();
	LSESP_vDctSpinningDriving();

	/*Calculate Steer Driving Offset*/
	LSESP_vCalStrDrvOff();

	/*Calculate YawLg Driving Offset*/
	LSESP_vCalYawLgDrvOff();

	/*CHECK Initial STN position*/
	LSESP_vDctPHZAngle();

    /*Calculate Master Pressure Sensor Offset*/
#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    LSESP_vCalMpressOffset(&PRESS_M);
    
    #if  __FL_PRESS == ENABLE    
    LSESP_vCalMpressOffset(&PRESS_FL);
    #endif

    #if  __FR_PRESS == ENABLE    
    LSESP_vCalMpressOffset(&PRESS_FR);
    #endif

    #if  __RL_PRESS == ENABLE    
    LSESP_vCalMpressOffset(&PRESS_RL);
    #endif
    
    #if  __RR_PRESS == ENABLE
    LSESP_vCalMpressOffset(&PRESS_RR);
    #endif
    
#else

    #if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
    LSESP_vCalMpressOffset();
    #endif 

#endif    
    
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    #if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
    LSESP_vCalBrkAppSenOffset();
    #endif    
#endif    

    /*Determine Yaw,LatG,Steer Offset*/
	LSESP_vDetSensorOffset();

    LSESP_vCheckOperateCondition();
  

}

static void LSESP_vCalRawSensorSignal(void)
{
  /* FailManagement */

	/*FS_YAW_OFFSET_OK_FLG = yaw_offset_end_flg;*/
	#if __YAW_TEMPERATURE_COMPENSATION
	FS_YAW_OFFSET_OK_FLG = yaw_offset_srch_ok_flg|lsespu1YawStandOffsCalIGN1st;
	#else
	FS_YAW_OFFSET_OK_FLG = lsespu1YawStandOffsCalIGN1st;
	#endif

	#if __STEER_SENSOR_TYPE==CAN_TYPE/*060602 for HM reconstruction by eslim*/
	PHZ_detected_flg2=1;
	#elif __STEER_SENSOR_TYPE==ANALOG_TYPE
	if(PHZ_detected_flg==1)
	{
		PHZ_detected_flg2=1;
	}
	else
	{
		PHZ_detected_flg2=0;
	}
	#else
	PHZ_detected_flg2=1;
	#endif
/*	if((McrAbs(fcs8SenRasterFreqency)>=100)||(McrAbs(yaw_1_100deg)>=YAW_90DEG)) */
	if( McrAbs(yaw_1_100deg)>=YAW_90DEG )
	{
		yaw_over_flg=1;
	}
	else
	{
		yaw_over_flg=0;
	}

	steer_1_10deg2_old = steer_1_10deg2;
	yaw_1_100deg2_old = yaw_1_100deg2;
	a_lat_1_1000g2_old = a_lat_1_1000g2;



  /* FailManagement */
    if(fu1SteerSenPwr1sOk==0)
    {
    	steer_1_10deg2 = WSTR_0_DEG ;
    	
    	lsespu8SASsigStbCnt=0;
    	lsespu1SASsigStbFlg=0;
    }
    else
    {
		if(fu1StrSusDet==1)
		{
			steer_1_10deg2 = steer_1_10deg2_old;
		}
		else
		{
			steer_1_10deg2 = steer_1_10deg;
		}

	    if(lsespu1SASsigStbFlg == 0)
	    {
	    	if(lsespu8SASsigStbCnt >= L_U8_TIME_10MSLOOP_500MS)
	    	{
	    		lsespu1SASsigStbFlg = 1;
	    	}
	    	else
	    	{
	    		lsespu8SASsigStbCnt = lsespu8SASsigStbCnt + 1;
	    	}
	    }
	    else
	    {
	    	;
	    }
	}



/*	if((cbit_process_step!=1)&&(yaw_lg_raster_jump_flg==0))  */ /*Valid Yaw&LatG - Needed Additional Sensor Invalid Signal*/
	if((cbit_process_step!=1)&&(yaw_over_flg==0)) /*Valid Yaw&LatG - Needed Additional Sensor Invalid Signal*/
	{  
		yaw_1_100deg2 = yaw_1_100deg ;	/* Temp	*/
		a_lat_1_1000g2 = a_lat_1_1000g;
	}
	else
	{
		yaw_1_100deg2 = yaw_1_100deg2_old;
		a_lat_1_1000g2 = a_lat_1_1000g2_old;
	}
	

//  steer_rate_deg_s2=LCESP_s16Lpf1Int(abs_wstr_dot/10,steer_rate_deg_s2,L_U8FILTER_GAIN_10MSLOOP_2HZ);// optime: skeon 0305 (old)
  steer_rate_deg_s2=LCESP_s16Lpf1Int(((int16_t)(abs_wstr_dot/10)),steer_rate_deg_s2,L_U8FILTER_GAIN_10MSLOOP_2HZ);  // optime: skeon 0305 (new)  

	yaw_1_10deg_ss_old=yaw_1_10deg_ss;
	
	tempW3 = (int16_t)( (((int32_t)(yaw_1_100deg2 - yaw_1_100deg2_old))*100)/7 ); 
	yaw_1_10deg_ss=LCESP_s16Lpf1Int(tempW3, yaw_1_10deg_ss_old, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);

	#if !__DEL_360_OFFSET
	c_steer_1_10deg2 = steer_1_10deg2 + steer_offset_360;
	#else
	c_steer_1_10deg2 = steer_1_10deg2;
	#endif

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    LSESP_vCalPressRawSignal();
#elif ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	raw_mpress = 0; // tmp...........pos_mc_1_100bar/10;
#else    
	raw_mpress = pos_mc_1_100bar/10;
#endif	

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))

    /* C710 PEDAL */

    /* PDF Signal Interface */
    #if (__USE_PDF_SIGNAL==ENABLE)
    /* F/S Sensor Raw Signal */
    lsesps16BrkAppSenRaw = fs16CalcPDF;
    /* Sensor Raw Signal */
    PDF.lsesps16PedalSenRaw = lsesps16BrkAppSenRaw;
    /* EOL EEP Read Signal */
    PDF.lsesps16OfsEolFirstEep = wbs16PDFOfsFirstEep;
    /* EOL EEP Read OK Flag */
    PDF.lsespu1PedalEolFirstOfsRead = wbu1PDFOfsReadEep;
    /* Drv EEP OFS Signal*/
    PDF.lsesps16DrvOfsEepRead = wbs16PDFDrivingOfsFirstEep; 
    /* Drv EEP OFS Signal Read OK  */
    PDF.lsespu1OfsEepReadOK = wbu1PDFDrivingOfsReadEep; 
    /* PDF Validity Flag */
    ESP_PDF.lsespu1fsFailInvalid = fu1EscPdFInvalid;
    #else
    lsesps16BrkAppSenRaw = 0;    
    PDF.lsesps16PedalSenRaw = 0;
    PDF.lsesps16OfsEolFirstEep = 0;
    PDF.lsespu1PedalEolFirstOfsRead = 0;
    PDF.lsesps16DrvOfsEepRead = 0; 
    PDF.lsespu1OfsEepReadOK = 0;
    ESP_PDF.lsespu1fsFailInvalid = 0;
    #endif
    /* PDF Signal Interface */

    /* PDT Signal Interface */
    #if (__USE_PDT_SIGNAL==ENABLE)
    /* F/S Sensor Raw Signal */
    lsesps16BrkAppSenRaw2 = fs16CalcPDT;
    /* Sensor Raw Signal */
    PDT.lsesps16PedalSenRaw = lsesps16BrkAppSenRaw2;    
    /* EOL EEP Read Signal */
    PDT.lsesps16OfsEolFirstEep = wbs16PDTOfsFirstEep;
    /* EOL EEP Read OK Flag */
    PDT.lsespu1PedalEolFirstOfsRead = wbu1PDTOfsReadEep;
    /* Drv EEP OFS Signal*/  
    PDT.lsesps16DrvOfsEepRead = wbs16PDTDrivingOfsFirstEep; 
    /* Drv EEP OFS Signal Read OK  */   
    PDT.lsespu1OfsEepReadOK = wbu1PDTDrivingOfsReadEep; 
    /* PDF Validity Flag */
    ESP_PDT.lsespu1fsFailInvalid = fu1EscPdTInvalid;
    #else
    lsesps16BrkAppSenRaw2 = 0;
    PDT.lsesps16PedalSenRaw = 0;    
    PDT.lsesps16OfsEolFirstEep = 0;
    PDT.lsespu1PedalEolFirstOfsRead = 0;
    PDT.lsesps16DrvOfsEepRead = 0;    
    PDT.lsespu1OfsEepReadOK = 0; 
    ESP_PDT.lsespu1fsFailInvalid = 0;
    #endif    
    /* PDT Signal Interface */
                                      
    /* EOL EEP Read Signal */
    lsesps16MpresEolFirstOfs   = wbs16MPOfsFirstEep ;    
    /* EOL EEP Read OK Flag */
    lsespu1MpresEolFirstOfsRead     = wbu1MPOfsReadEep;        
    /* Drv EEP OFS Signal*/      
    lsesps16MpresDrvEEPOfs       =  wbs16MPDrivingOfsFirstEep;       
    /* Drv EEP OFS Signal Read OK  */      
    lsespu1MpresDrvEEPOfsRead    =  wbu1MPDrivingOfsReadEep;           
    ESP_MP.lsespu1fsFailInvalid  = fu1MCPErrorDet; 

#else

    lsesps16MpresDrvEEPOfs = wbs16MPDrivingOfsFirstEep;
	lsespu1MpresDrvEEPOfsRead = wbu1MPDrivingOfsReadEep;
    
#endif

#endif

}

static void LSESP_vChkSASOk(uint8_t lsespu8TempStrAccInput)
{

	if(lsespu8TempStrAccInput>=30)
	{
		lsesps16CntDelaySASCheck++;

		if(lsesps16CntDelaySASCheck>L_U8_TIME_10MSLOOP_2S)
		{
			lsesps16CntDelaySASCheck=L_U8_TIME_10MSLOOP_2S;
		}
		else
		{
			;
		}
	}
	else
	{
		lsesps16CntDelaySASCheck=0;
	}

	if((lsesps16CntDelaySASCheck>L_U8_TIME_10MSLOOP_1000MS) && (fu1YawSenPwr1sOk==1) && (lsespu1SASsigStbFlg ==1))
	{
		SAS_CHECK_OK=1;
	}
	else
	{
		SAS_CHECK_OK=0;
	}

}

static void LSESP_vLoadDrivingOffsetParameter(void)
{
/* 2005.02.04 y.w.shin */
	#if !__DEL_360_OFFSET
		#if __STEER_SENSOR_TYPE==CAN_TYPE/*060602 for HM reconstruction by eslim*/
		KSTEER.offset_cal_max =WSTR_720_DEG;
		#elif __STEER_SENSOR_TYPE==ANALOG_TYPE
		KSTEER.offset_cal_max =WSTR_360_DEG;
		#else
		KSTEER.offset_cal_max =WSTR_360_DEG;
		#endif
	#else
		#if __STEER_SENSOR_TYPE==CAN_TYPE
		KSTEER.offset_cal_max =2*WSTR_720_DEG;
		#else
		KSTEER.offset_cal_max =4*WSTR_720_DEG;
		#endif
	#endif

	KYAW.offset_cal_max =YAW_8DEG;
	KLAT.offset_cal_max =LAT_0G45G;

	KSTEER.STRAIGHT = WSTR_4_DEG;
	KYAW.STRAIGHT =YAW_1G5DEG;
	KLAT.STRAIGHT =LAT_0G1G;

	KSTEER.STRAIGHT2 = WSTR_2G5_DEG;
	KYAW.STRAIGHT2 =YAW_0G5DEG;
	KLAT.STRAIGHT2 =LAT_0G02G;

	KYAW.fs_measured_offset = -yaw_correction;

	KSTEER.offset_60_avr_limit=WSTR_360_DEG;
	KYAW.offset_60_avr_limit=YAW_4DEG;
	KLAT.offset_60_avr_limit=LAT_0G005G;

	/***********reliablity of offset*************/
	KSTEER.first_driving_accuracy=30;
	KYAW.first_driving_accuracy=30;
	KLAT.first_driving_accuracy=30;

	KSTEER.deviation_driving_threshold=WSTR_1_DEG;
	KYAW.deviation_driving_threshold=YAW_0G03DEG;
	KLAT.deviation_driving_threshold=LAT_0G005G;
}



static void LSESP_vDetYawNormOffset(void)
{


    if(lsespu1YawStandOffsCalIGN1st==1)
    {
	    yaw_offset_f = lsesps16YawStandOffset;
	    lsesps16MaxYawThrAdd=(McrAbs(yaw_offset_f)>YAW_OFFSET_MAX_ERR)?(McrAbs(yaw_offset_f)-YAW_OFFSET_MAX_ERR):0;
	}
	else
	{
		if(lsespu1EEPYawStandOfsReadOK==1)
		{
			yaw_offset_f = lsesps16EEPYawStandOfsRead;
			lsesps16MaxYawThrAdd=(McrAbs(yaw_offset_f)>YAW_OFFSET_MAX_ERR)?(McrAbs(yaw_offset_f)-YAW_OFFSET_MAX_ERR):0;
		}
		else
		{
		    if(KYAW.FIRST_OFFSET_CAL_FLG==1)
		    {
			    yaw_offset_f = KYAW.offset_60avr;
			    lsesps16MaxYawThrAdd=(McrAbs(yaw_offset_f)>YAW_OFFSET_MAX_ERR)?(McrAbs(yaw_offset_f)-YAW_OFFSET_MAX_ERR):0;
		    }
		    else
		    {
		    	yaw_offset_f=0;
			    lsesps16MaxYawThrAdd = YAW_2G5DEG ;
		    }
		}
	}

	

	if(lsesps16MaxYawThrAdd>YAW_THRESHOLD_ADD_MAX)
	{
		lsesps16MaxYawThrAdd=YAW_THRESHOLD_ADD_MAX;
	}
	else
	{
		;
	}
	
	if(yaw_offset_f>YAW_OFFSET_MAX_CORR)
	{
		yaw_offset_f=YAW_OFFSET_MAX_CORR;
	}
	else if(yaw_offset_f<-YAW_OFFSET_MAX_CORR)
	{
		yaw_offset_f=-YAW_OFFSET_MAX_CORR;
	}
	else
	{
		;
	}

}


static void LSESP_vChkFinDrivSteerOffset(void)
{
	if((KSTEER.FIRST_OFFSET_CAL_FLG==1) && (McrAbs(steer_offset_f -KSTEER.offset_60avr)>WSTR_5_DEG) )
	{
		KSTEER.vdc_sensor_check_cnt=0;
		KSTEER.straight_cnt=0;
		KSTEER.sum_model[0]=0;
		KSTEER.sum_model[1]=0;
		KSTEER.sum_model[2]=0;
		KSTEER.sum_model[3]=0;
		KSTEER.sum_model[4]=0;
		KSTEER.sum_measured_by_sens=0;

		KSTEER.avr_model[1]=0;
		KSTEER.avr_model[2]=0;
		KSTEER.avr_model[3]=0;
		KSTEER.avr_model[4]=0;
		KSTEER.STRAIGHT_FLAG=0;
		KSTEER.DURING_STRAIGHT_FLG=0;
		KSTEER.offset_ok_cnt=0;
		KSTEER.offset_10sum=0;
		KSTEER.offset_10avr=0;

		KSTEER.MODEL_OK_AT_15KPH=0;
		KSTEER.OFFSET_10AVR_CAL_OK_FLG=0;
		KSTEER.FIRST_OFFSET_CAL_FLG=0;
		KSTEER.NTH_OFFSET_CAL_FLG=0;

		KSTEER.offset_at_15kph=0;
		KSTEER.offset_10avr_old =0;
		KSTEER.offset_nth_sum=0;
		KSTEER.offset_at_15kph_nth=0;
		KSTEER.OFFSET_AT_15KPH_NTH_OK=0;
		KSTEER.offset_10_cnt=0;
		KSTEER.offset_10_sec_cnt=0;
		KSTEER.OFFSET_60AVR_CAL_OK_FLG=0;
		KSTEER.OFFST_10AVR_EEPROM=0;
		KSTEER.offset_10avr_first=0;
		KSTEER.offset_60avr=0;
		KSTEER.offset_60avr_old=0;
		KSTEER.driving_accuracy=0;

		steer_initial_check_flg=0;

		steer_final_check_flg=1;
	}
	else
	{
		;
	}

}
static void LSESP_vChkInitDrivSteerOffset(void)
{
	if((KSTEER.MODEL_OK_AT_15KPH==0)&&(FLAG_INHIBIT_OFST_SPIN==1))
	{
		KSTEER.vdc_sensor_check_cnt=0;
		KSTEER.straight_cnt=0;
		KSTEER.sum_model[0]=0;
		KSTEER.sum_model[1]=0;
		KSTEER.sum_model[2]=0;
		KSTEER.sum_model[3]=0;
		KSTEER.sum_model[4]=0;
		KSTEER.sum_measured_by_sens=0;

		KSTEER.avr_model[1]=0;
		KSTEER.avr_model[2]=0;
		KSTEER.avr_model[3]=0;
		KSTEER.avr_model[4]=0;
		KSTEER.STRAIGHT_FLAG=0;
		KSTEER.DURING_STRAIGHT_FLG=0;
		KSTEER.offset_ok_cnt=0;
		KSTEER.offset_10sum=KSTEER.offset_10avr=0;

		KSTEER.MODEL_OK_AT_15KPH=0;
		KSTEER.OFFSET_10AVR_CAL_OK_FLG=0;
		KSTEER.FIRST_OFFSET_CAL_FLG=0;
		KSTEER.NTH_OFFSET_CAL_FLG=0;

		KSTEER.offset_at_15kph=0;
		KSTEER.offset_10avr_old =0;
		KSTEER.offset_nth_sum=0;
		KSTEER.offset_at_15kph_nth=0;
		KSTEER.OFFSET_AT_15KPH_NTH_OK=0;
		KSTEER.offset_10_cnt=0;
		KSTEER.offset_10_sec_cnt=0;
		KSTEER.OFFSET_60AVR_CAL_OK_FLG=0;
		KSTEER.OFFST_10AVR_EEPROM=0;
		KSTEER.offset_10avr_first=0;
		KSTEER.offset_60avr=0;
		KSTEER.offset_60avr_old=0;
		KSTEER.driving_accuracy=0;
	}
	else
	{
		;
	}
	#if __STEER_SENSOR_TYPE==CAN_TYPE
	if(KSTEER.MODEL_OK_AT_15KPH==1)
	{
		if( (McrAbs(KSTEER.offset_of_eeprom -KSTEER.offset_at_15kph) > WSTR_10_DEG) && (steer_initial_check_flg==0) )
	    {
    		KSTEER.vdc_sensor_check_cnt=0;
    		KSTEER.straight_cnt=0;
    		KSTEER.sum_model[0]=0;
    		KSTEER.sum_model[1]=0;
    		KSTEER.sum_model[2]=0;
    		KSTEER.sum_model[3]=0;
    		KSTEER.sum_model[4]=0;
    		KSTEER.sum_measured_by_sens=0;

    		KSTEER.avr_model[1]=0;
    		KSTEER.avr_model[2]=0;
    		KSTEER.avr_model[3]=0;
    		KSTEER.avr_model[4]=0;
    		KSTEER.STRAIGHT_FLAG=0;
    		KSTEER.DURING_STRAIGHT_FLG=0;
    		KSTEER.offset_ok_cnt=0;
    		KSTEER.offset_10sum=0;
    		KSTEER.offset_10avr=0;

    		KSTEER.MODEL_OK_AT_15KPH=0;
    		KSTEER.OFFSET_10AVR_CAL_OK_FLG=0;
    		KSTEER.FIRST_OFFSET_CAL_FLG=0;
    		KSTEER.NTH_OFFSET_CAL_FLG=0;

    		KSTEER.offset_at_15kph=0;
    		KSTEER.offset_10avr_old =0;
    		KSTEER.offset_nth_sum=0;
    		KSTEER.offset_at_15kph_nth=0;
    		KSTEER.OFFSET_AT_15KPH_NTH_OK=0;
    		KSTEER.offset_10_cnt=0;
    		KSTEER.offset_10_sec_cnt=0;
    		KSTEER.OFFSET_60AVR_CAL_OK_FLG=0;
    		KSTEER.OFFST_10AVR_EEPROM=0;
    		KSTEER.offset_10avr_first=0;
    		KSTEER.offset_60avr=0;
    		KSTEER.offset_60avr_old=0;
    		KSTEER.driving_accuracy=0;

    		steer_initial_check_flg=1;

	    }
	    else
	    {
			steer_initial_check_flg=1;
	    }
	}
	else
	{
		;
	}
	#endif
}

static void LSESP_vDetSteerOffset(void)
{
	int16_t lsesps16TemporarySteerOffset;
	int8_t lsesps8TemporarySteerOffsetAcc;
	uint8_t lsespu8TempStrAcc;
	#if __STEER_SENSOR_TYPE==CAN_TYPE
	lsesps16TemporarySteerOffset=KSTEER.offset_of_eeprom;
	lsesps8TemporarySteerOffsetAcc=KSTEER.eeprom_accuracy;
	#else
	lsesps16TemporarySteerOffset=0;
	lsesps8TemporarySteerOffsetAcc=0;
	#endif
	
	#if __CAN_SAS_EEP_OFFSET_APPLY==1
	if(KSTEER.eeprom_accuracy>=30)
	{
		lsesps16TemporarySteerOffset=KSTEER.offset_of_eeprom;
		lsesps8TemporarySteerOffsetAcc=30;
	}
	else
	{
		;
	}	
    #else
        
    #endif
    

	if(KSTEER.driving_accuracy_stable>=lsesps8TemporarySteerOffsetAcc)
	{
		lsesps16TemporarySteerOffset=steer_offset_stable;
		lsesps8TemporarySteerOffsetAcc=KSTEER.driving_accuracy_stable;
	}
	else
	{
		;
	}

	if(KSTEER.driving_accuracy>=lsesps8TemporarySteerOffsetAcc)
	{
		if(KSTEER.FIRST_OFFSET_CAL_FLG==1)
		{
			lsesps16TemporarySteerOffset=KSTEER.offset_60avr;
		}
		else
		{
            lsesps16TemporarySteerOffset=KSTEER.offset_at_15kph;
		}

		lsesps8TemporarySteerOffsetAcc=KSTEER.driving_accuracy;
	}
	else
	{
		;
	}

	if((lsesps8TemporarySteerOffsetAcc<30)&&(McrAbs(lsesps16TemporarySteerOffset)>WSTR_90_DEG))/*옵셋 보장 범위 90도*/
	{
		lsesps16TemporarySteerOffset=0;
		lsesps8TemporarySteerOffsetAcc=0;
	}
	else
	{
		;
	}

    #if __STEER_SENSOR_TYPE==CAN_TYPE
        #if  __CAN_SAS_EEP_OFFSET_APPLY == 1
        if( (lsespu1EEPSASresetFlg==1) && (STEER_STABLE_OK==0) && (KSTEER.FIRST_OFFSET_CAL_FLG==0) )
        {
		    steer_offset_f=0;	    
		}
		else
		{
			steer_offset_f=lsesps16TemporarySteerOffset;
		}
		#else
		    steer_offset_f=lsesps16TemporarySteerOffset;
		#endif
	
	#else
	steer_offset_f=lsesps16TemporarySteerOffset;
	#endif
	
	lsespu8TempStrAcc=(lsesps8TemporarySteerOffsetAcc>0)?(uint8_t)McrAbs(lsesps8TemporarySteerOffsetAcc):0;

	LSESP_vChkSASOk(lsespu8TempStrAcc);

/* 2007-01 Issue list Solution By YooJH	 */
#if __STEER_SENSOR_TYPE==CAN_TYPE
	tempW2=(McrAbs(steer_offset_f)>WSTR_20_DEG)?(McrAbs(steer_offset_f)-WSTR_20_DEG):0;
#else
	if(STR_OFFSET_360_OK==1)
	{
		tempW2=(McrAbs(steer_offset_f+steer_offset_360)>WSTR_20_DEG)?(McrAbs(steer_offset_f+steer_offset_360)-WSTR_20_DEG):0;
	}
	else
	{
		tempW2=0;
	}
#endif

/*	tempW2=(McrAbs(steer_offset_f)>WSTR_20_DEG)?(McrAbs(steer_offset_f)-WSTR_20_DEG):0; */
	/*55deg is too big. how about 20~90 instead of 55~90 , 20deg is yaw 5deg at high speed60~120, 55 is 1.5*fsspec55*/
	tempW1=100;
	tempW0=LCESP_s16IInter3Point(vref5,VREF_20_KPH,90,VREF_40_KPH,50,VREF_80_KPH,40);
	
	lsesps16MaxSteerThrAdd=(int16_t)( (((int32_t)tempW2)*tempW1)/tempW0 );

	if(lsesps16MaxSteerThrAdd>YAW_10DEG)
	{
		lsesps16MaxSteerThrAdd=YAW_10DEG;
	}
	else
	{
		;
	}

#if __STEER_SENSOR_TYPE==CAN_TYPE
	if((McrAbs(steer_offset_f) > WSTR_20_DEG ) &&(SAS_CHECK_OK==1) )
	{
		if(lsespu8TempStrAcc>=30)
		{
			steer_offset_reliablity=(uint8_t)McrAbs(LCESP_s16IInter2Point(McrAbs(steer_offset_f),WSTR_20_DEG,30,WSTR_90_DEG,0));
		}
		else
		{
			steer_offset_reliablity=0;
		}
	}
	else
	{
		steer_offset_reliablity=lsespu8TempStrAcc;
	}
#else
	if((McrAbs(steer_offset_f+steer_offset_360) > WSTR_20_DEG )&&(STR_OFFSET_360_OK==1)&&(SAS_CHECK_OK==1))
	{
		if(lsespu8TempStrAcc>=30)
		{
			steer_offset_reliablity=(uint8_t)LCESP_s16IInter2Point(McrAbs(steer_offset_f+steer_offset_360),WSTR_20_DEG,30,WSTR_90_DEG,0);
		}
		else
		{
			steer_offset_reliablity=0;
		}
	}
	else
	{
		steer_offset_reliablity=lsespu8TempStrAcc;
	}
#endif

}

static void LSESP_vDetLatAccOffset(void)
{
	int16_t lsesps16TemporaryLatAccelOffset;
	int8_t lsesps8TemporaryLatAccelOffsetAcc;

	lsesps8TemporaryLatAccelOffsetAcc=KLAT.eeprom_accuracy;
	lsesps16TemporaryLatAccelOffset=KLAT.offset_of_eeprom;

	if(KLAT.driving_accuracy>=lsesps8TemporaryLatAccelOffsetAcc)
	{
		if(KLAT.FIRST_OFFSET_CAL_FLG==1)
		{
			lsesps16TemporaryLatAccelOffset=KLAT.offset_60avr;
		}
		else
		{
			lsesps16TemporaryLatAccelOffset=KLAT.offset_at_15kph;
		}

		lsesps8TemporaryLatAccelOffsetAcc=KLAT.driving_accuracy;

	}
	else
	{
		;
	}

	/* FailManagement */
	if(fu1AyErrorDet==1)
	{
		FLAG_INITIAL_LAT_OFFSET=0;
	}
	else
	{
		;
	}

	if(FLAG_INITIAL_LAT_OFFSET==1)
	{
		if(lsespu16CntLatGUpdate>L_U16_TIME_10MSLOOP_1MIN)
		{
			lsesps16TempLatAccelOffsetOld=alat_offset_f;

			if(lsesps16TemporaryLatAccelOffset>(lsesps16TempLatAccelOffsetOld+LAT_0G002G))
			{
				alat_offset_f=lsesps16TempLatAccelOffsetOld+LAT_0G002G;
			}
			else if(lsesps16TemporaryLatAccelOffset<(lsesps16TempLatAccelOffsetOld-LAT_0G002G))
			{
				alat_offset_f=lsesps16TempLatAccelOffsetOld-LAT_0G002G;
			}
			else
			{
				alat_offset_f=lsesps16TemporaryLatAccelOffset;
			}
			alat_offset_reliablity=(lsesps8TemporaryLatAccelOffsetAcc>0)?(uint8_t)McrAbs(lsesps8TemporaryLatAccelOffsetAcc):0;
			lsespu16CntLatGUpdate=0;
		}
		else
		{
			lsespu16CntLatGUpdate++;
		}
	}
	else
	{
		if(lsesps8TemporaryLatAccelOffsetAcc>0)
		{
			FLAG_INITIAL_LAT_OFFSET=1;

			if((KLAT.eeprom_accuracy==0)&&(McrAbs(lsesps16TemporaryLatAccelOffset)>LAT_0G1G))
			{
				alat_offset_f=LAT_0G1G;
				alat_offset_reliablity=0;
			}
			else
			{
				alat_offset_f=lsesps16TemporaryLatAccelOffset;
				alat_offset_reliablity=lsesps8TemporaryLatAccelOffsetAcc;
			}
		}
		else
		{
			alat_offset_f=0;
			alat_offset_reliablity=0;
		}
	}

	tempW2=(McrAbs(alat_offset_f)>LAT_0G1G)?(McrAbs(alat_offset_f)-LAT_0G1G):0;
	tempW1 = 1617;
	if(vref5 < VREF_15_KPH)
	{
		tempW0 = VREF_15_KPH;
	}
	else
	{
		tempW0 = vref5;
	}

    lsesps16MaxLatGThrAdd= (int16_t)( (((int32_t)tempW2)*tempW1)/tempW0 );

	if(lsesps16MaxLatGThrAdd>YAW_5DEG)
	{
		lsesps16MaxLatGThrAdd=YAW_5DEG;
	}
	else
	{
		;
	}

	if(alat_offset_f>LAT_0G2G)
	{
		alat_offset_f=LAT_0G2G;
		alat_offset_reliablity=0;
	}
	else if(alat_offset_f<-LAT_0G2G)
	{
		alat_offset_f=-LAT_0G2G;
		alat_offset_reliablity=0;
	}
	else if(McrAbs(alat_offset_f)>LAT_0G1G)
	{
		if(alat_offset_reliablity>=30)
		{
			alat_offset_reliablity=(uint8_t)McrAbs(LCESP_s16IInter2Point(McrAbs(alat_offset_f),LAT_0G1G,30,LAT_0G2G,0));
		}
		else
		{
			alat_offset_reliablity=0;
		}
	}
	else
	{
		;
	}
}
static void LSESP_vDetSensorOffset(void)
{
	#if  __YAW_TEMPERATURE_COMPENSATION==1
	LSESP_vDetYawTempCompOffset();
	#else
	LSESP_vDetYawNormOffset();
	#endif

	LSESP_vDetSteerOffset();
	LSESP_vDetLatAccOffset();
	
	#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
	LSESP_vDetPressureOffset(&PRESS_M);
	
	    #if  __FL_PRESS == ENABLE
	    LSESP_vDetPressureOffset(&PRESS_FL);
	    #endif
	    
	    #if  __FR_PRESS == ENABLE
	    LSESP_vDetPressureOffset(&PRESS_FR);
	    #endif
	    
	    #if  __RL_PRESS == ENABLE
	    LSESP_vDetPressureOffset(&PRESS_RL);
	    #endif
	    
	    #if  __RR_PRESS == ENABLE
	    LSESP_vDetPressureOffset(&PRESS_RR);
	    #endif
	    
	#endif
	
    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    #if ((__AHB_GEN3_SYSTEM == DISABLE)  && (__IDB_LOGIC == DISABLE))
	LSESP_vDetBrkAppSenOffset();
	#endif
	#endif
	
  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */	
  if(wu8IgnStat == CE_ON_STATE)
  {
	    LSESP_vDetEEPROMOffset();
  }
  else
  {
    ;
  }
  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */
  
}


static void LSESP_vCheckOperateCondition(void)
{
	if(lsespu1ESPnotReadyCond==1)		
	{ 
		/*  Reset Condition */
		if(SAS_CHECK_OK==1)
		{
		  lsespu1ESPnotReadyCond=0;  	
		}
		else
		{
		  ;	
		}		
		
		lsespu16ESPnotReadyCheckTime=0;
	}
	else
	{
		/* Set Condition */
		if(SAS_CHECK_OK==1)
		{ 
			lsespu16ESPnotReadyCheckTime=0;
		}
		else
		{
			if( vref > (K_ESC_Center_Speed_Threshold*8) )
			{
				lsespu16ESPnotReadyCheckTime=lsespu16ESPnotReadyCheckTime+1;
				if(lsespu16ESPnotReadyCheckTime > (K_ESC_Center_Time_Threshold*L_U8_TIME_10MSLOOP_1000MS) )
				{
					lsespu1ESPnotReadyCond=1;
					lsespu16ESPnotReadyCheckTime=0;					
				}
				else
				{
				  ;	
				}				
			}
			else
			{
				lsespu16ESPnotReadyCheckTime=0;
				
			}			
		}		
	}
	
}


static void LSESP_vDetEEPROMOffset(void)
{
    int16_t  lsesps16EEPYawStandOfsDiff;
    #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    int16_t  lsesps16EEPPressureOfsDiff;
    #endif    

    #if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))    
    int16_t  s16PedalEEPOfsDiff;    
    int16_t  s16MpressEEPOfsDiff;
    /* 2012_swd_kjs */
    #else
    int16_t  s16MpressEEPOfsDiff;
    /* 2012_swd_kjs */    
    #endif    
    #endif     

	KYAW.model_offset = yaw_offset_f;
	KYAW.offset_diff = McrAbs(yaw_offset_f - KYAW.offset_of_eeprom2);
	if( KYAW.offset_diff >= KYAW.eeprom_update_TH)
	{
		if (KYAW.model_offset > KYAW.eeprom_update_limit)
		{
			KYAW.model_offset = KYAW.eeprom_update_limit;
		}
		else
		{
			;
		}
		if (KYAW.model_offset < -KYAW.eeprom_update_limit)
		{
			KYAW.model_offset = -KYAW.eeprom_update_limit;
		}
		else
		{
			;
		}

		KYAW.NEED_TO_WR_OFFSET_FLG= 1;
	}
	else
	{
		KYAW.NEED_TO_WR_OFFSET_FLG=0;
	}

	if(KYAW.NEED_TO_WR_OFFSET_FLG==1)
	{
	    tempW0 =McrAbs(KYAW.model_offset - KYAW.offset_of_eeprom2);
	    if(KYAW.eeprom_update_TH > tempW0)
	    {
	    	KYAW.NEED_TO_WR_OFFSET_FLG=0;
	    }
	    else
	    {
	    	;
	    }
	}
	else
	{
		;
	}

/* Normal Yaw offset EEPROM Determination */

	lsesps16EEPYawStandOfsDiff = McrAbs(lsesps16YawStandOffset - lsesps16EEPYawStandOfsRead);
	if( (lsesps16EEPYawStandOfsDiff >= KYAW.eeprom_update_TH) && (lsespu8YawStandCNT==3) )
	{
		lsespu1EEPYawStandWrtReq= 1;
	}
	else
	{
		lsespu1EEPYawStandWrtReq= 0;
	}

    #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
    if(PRESS_M.u1prem_PressOffsetOK==1)
    {
        lsesps16EEPPressureOfsDiff= McrAbs(PRESS_M.s16premDrvOffset - PRESS_M.s16premEEPOfsRead);
        if(lsesps16EEPPressureOfsDiff >= PRESSURE_EEP_UPDATE_TH )
        {
    	    PRESS_M.u1premNeedToWriteOffset=1;
        }
        else
        {
    	    PRESS_M.u1premNeedToWriteOffset=0;
        }
    }
    else
    {
        PRESS_M.u1premNeedToWriteOffset=0;
    }
    
        #if  __FL_PRESS == ENABLE
        if(PRESS_FL.u1prem_PressOffsetOK==1)
        {
            lsesps16EEPPressureOfsDiff= McrAbs(PRESS_FL.s16premDrvOffset - PRESS_FL.s16premEEPOfsRead);
            if(lsesps16EEPPressureOfsDiff >= PRESSURE_EEP_UPDATE_TH )
            {
        	    PRESS_FL.u1premNeedToWriteOffset=1;
            }
            else
            {
        	    PRESS_FL.u1premNeedToWriteOffset=0;
            }
        }
        else
        {
            PRESS_FL.u1premNeedToWriteOffset=0;
        }    
        #endif
        
        
        #if  __FR_PRESS == ENABLE
        if(PRESS_FR.u1prem_PressOffsetOK==1)
        {        
            lsesps16EEPPressureOfsDiff= McrAbs(PRESS_FR.s16premDrvOffset - PRESS_FR.s16premEEPOfsRead);
            if(lsesps16EEPPressureOfsDiff >= PRESSURE_EEP_UPDATE_TH )
            {
        	    PRESS_FR.u1premNeedToWriteOffset=1;
            }
            else
            {
        	    PRESS_FR.u1premNeedToWriteOffset=0;
            }
        }
        else
        {
            PRESS_FR.u1premNeedToWriteOffset=0;
        }    
        #endif
        
        
        #if  __RL_PRESS == ENABLE
        if(PRESS_RL.u1prem_PressOffsetOK==1)
        {    
            lsesps16EEPPressureOfsDiff= McrAbs(PRESS_RL.s16premDrvOffset - PRESS_RL.s16premEEPOfsRead);
            if(lsesps16EEPPressureOfsDiff >= PRESSURE_EEP_UPDATE_TH )
            {
        	    PRESS_RL.u1premNeedToWriteOffset=1;
            }
            else
            {
        	    PRESS_RL.u1premNeedToWriteOffset=0;
            }
        }
        else
        {
            PRESS_RL.u1premNeedToWriteOffset=0;
        }        
        #endif
        
        #if  __RR_PRESS == ENABLE
        if(PRESS_RR.u1prem_PressOffsetOK==1)
        {    
            lsesps16EEPPressureOfsDiff= McrAbs(PRESS_RR.s16premDrvOffset - PRESS_RR.s16premEEPOfsRead);
            if(lsesps16EEPPressureOfsDiff >= PRESSURE_EEP_UPDATE_TH )
            {
        	    PRESS_RR.u1premNeedToWriteOffset=1;
            }
            else
            {
        	    PRESS_RR.u1premNeedToWriteOffset=0;
            }
        }
        else
        {
            PRESS_RR.u1premNeedToWriteOffset=0;
        }
        #endif            
    
    #endif

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    
    /*--- PDF Write Request ---*/
    if(ESP_PDF.lsespu1ESPEepOfsCalOkFlg == 1)
    {
    	PDF.lsespu1NeedToWriteOfs = 1;
    	
    	if( PDF.lsespu1PedalDrv60secOfsOK == 1 )
    	{
    		PDF.lsesps16PedalDrvOfsEEPw = PDF.lsesps16PedalDrvOfs_f ;
    	}
    	else
    	{
    		PDF.lsesps16PedalDrvOfsEEPw = ESP_PDF.lsesps16ESPofsFirstCal;
    	}    	
    }
    else
    {
    	if(PDF.lsespu1PedalDrv60secOfsOK == 1 )
    	{
            s16PedalEEPOfsDiff = McrAbs(PDF.lsesps16PedalDrvOfs_f - PDF.lsesps16DrvOfsEepRead);
            
            if(s16PedalEEPOfsDiff >= PEDAL_DRV_EEP_OFS_UPD_THR  )
            {
            	PDF.lsespu1NeedToWriteOfs = 1;
            	PDF.lsesps16PedalDrvOfsEEPw = PDF.lsesps16PedalDrvOfs_f ;
            }
            else
            {
            	PDF.lsespu1NeedToWriteOfs = 0;
            	PDF.lsesps16PedalDrvOfsEEPw =0;
            }    		
    	}
    	else
    	{
    		PDF.lsespu1NeedToWriteOfs = 0;
    		PDF.lsesps16PedalDrvOfsEEPw =0;
    	}
    }
    
    PDF.lsesps16PedalDrvOfsEEPw = LCABS_s16LimitMinMax(PDF.lsesps16PedalDrvOfsEEPw , -PEDAL_DRV_EEP_OFS_MAX, PEDAL_DRV_EEP_OFS_MAX) ; 
    
    
    /*--- PDT Write Request ---*/
    if(ESP_PDT.lsespu1ESPEepOfsCalOkFlg == 1)
    {
    	PDT.lsespu1NeedToWriteOfs = 1;
    	
    	if( PDT.lsespu1PedalDrv60secOfsOK == 1 )
    	{
    		PDT.lsesps16PedalDrvOfsEEPw = PDT.lsesps16PedalDrvOfs_f ;
    	}
    	else
    	{
    		PDT.lsesps16PedalDrvOfsEEPw = ESP_PDT.lsesps16ESPofsFirstCal;
    	}    	
    }
    else
    {
    	if(PDT.lsespu1PedalDrv60secOfsOK == 1 )
    	{
            s16PedalEEPOfsDiff = McrAbs(PDT.lsesps16PedalDrvOfs_f - PDT.lsesps16DrvOfsEepRead);
            
            if(s16PedalEEPOfsDiff >= PEDAL_DRV_EEP_OFS_UPD_THR  )
            {
            	PDT.lsespu1NeedToWriteOfs = 1;
            	PDT.lsesps16PedalDrvOfsEEPw = PDT.lsesps16PedalDrvOfs_f ;
            }
            else
            {
            	PDT.lsespu1NeedToWriteOfs = 0;
            	PDT.lsesps16PedalDrvOfsEEPw=0;
            }    		
    	}
    	else
    	{
    		PDT.lsespu1NeedToWriteOfs = 0;
    		PDT.lsesps16PedalDrvOfsEEPw=0;
    	}
    }
    
    PDT.lsesps16PedalDrvOfsEEPw = LCABS_s16LimitMinMax(PDT.lsesps16PedalDrvOfsEEPw , -PEDAL_DRV_EEP_OFS_MAX, PEDAL_DRV_EEP_OFS_MAX) ; 

    /*--- Mpress Write Request ---*/
    if(ESP_MP.lsespu1ESPEepOfsCalOkFlg == 1)
    {
    	lsespu1MpresDrvOfsNeedWriteEep = 1;
    	
    	if( lsespu1MpressDrvOffsetOK == 1 )
    	{
    		lsesps16MpresDrvOfsEEPw = lsesps16MpresDrvOfs ;
    	}
    	else
    	{
    		lsesps16MpresDrvOfsEEPw = ESP_MP.lsesps16ESPofsFirstCal;
    	}    	
    }
    else
    {
    	if(lsespu1MpressDrvOffsetOK == 1 )
    {
            s16MpressEEPOfsDiff = McrAbs(lsesps16MpresDrvOfs - lsesps16MpresDrvEEPOfs );
            
            if(s16MpressEEPOfsDiff >= PRES_DRV_EEP_OFS_UPD_THR  )
        {
            	lsespu1MpresDrvOfsNeedWriteEep = 1;
            	lsesps16MpresDrvOfsEEPw        = lsesps16MpresDrvOfs ;
        }
        else
        {
            	lsespu1MpresDrvOfsNeedWriteEep = 0;
        }
    }
    else
    {
    		lsespu1MpresDrvOfsNeedWriteEep = 0;
    	}
    }
        
    lsesps16MpresDrvOfsEEPw = LCABS_s16LimitMinMax(lsesps16MpresDrvOfsEEPw , -PRES_DRV_EEP_OFS_MAX, PRES_DRV_EEP_OFS_MAX) ;

/* 2012_swd_kjs */
    #else
    
    if(lsespu1MpressDrvOffsetOK == 1 )
    {
        s16MpressEEPOfsDiff = McrAbs(lsesps16MpresDrvOfs - lsesps16MpresDrvEEPOfs );
        
        if(s16MpressEEPOfsDiff >= PRES_DRV_EEP_OFS_UPD_THR  )
        {
        	lsespu1MpresDrvOfsNeedWriteEep = 1;
        	lsesps16MpresDrvOfsEEPw        = lsesps16MpresDrvOfs ;
        }
        else
        {
        	lsespu1MpresDrvOfsNeedWriteEep = 0;
        }    		
    }
    else
    {
    	lsespu1MpresDrvOfsNeedWriteEep = 0;
    }
        
    lsesps16MpresDrvOfsEEPw = LCABS_s16LimitMinMax(lsesps16MpresDrvOfsEEPw , -PRES_DRV_EEP_OFS_MAX, PRES_DRV_EEP_OFS_MAX) ;

/* 2012_swd_kjs */
    
    #endif
#endif

	KLAT.model_offset = alat_offset_f;
	KLAT.offset_diff = McrAbs(alat_offset_f - KLAT.offset_of_eeprom2);
	if( KLAT.offset_diff >= KLAT.eeprom_update_TH)
	{
		if (KLAT.model_offset > KLAT.eeprom_update_limit)
		{
			KLAT.model_offset = KLAT.eeprom_update_limit;
		}
		else
		{
			;
		}
		if (KLAT.model_offset < -KLAT.eeprom_update_limit)
		{
			KLAT.model_offset = -KLAT.eeprom_update_limit;
		}
		else
		{
			;
		}

		KLAT.NEED_TO_WR_OFFSET_FLG= 1;
	}
	else
	{
		KLAT.NEED_TO_WR_OFFSET_FLG=0;
	}

	if(KLAT.NEED_TO_WR_OFFSET_FLG==1)
	{
	    tempW0 =McrAbs(KLAT.model_offset - KLAT.offset_of_eeprom2);
	    if(KLAT.eeprom_update_TH > tempW0)
	    {
	    	KLAT.NEED_TO_WR_OFFSET_FLG=0;
	    }
	    else
	    {
	    	;
	    }
	}
	else
	{
		;
	}

	#if __STEER_SENSOR_TYPE==CAN_TYPE
	KSTEER.model_offset = steer_offset_f;
	KSTEER.offset_diff = McrAbs(steer_offset_f - KSTEER.offset_of_eeprom2);
	if( KSTEER.offset_diff >= KSTEER.eeprom_update_TH)
	{
		if (KSTEER.model_offset > KSTEER.eeprom_update_limit)
		{
			KSTEER.model_offset = KSTEER.eeprom_update_limit;
		}
		else
		{
			;
		}
		if (KSTEER.model_offset < -KSTEER.eeprom_update_limit)
		{
			KSTEER.model_offset = -KSTEER.eeprom_update_limit;
		}
		else
		{
			;
		}

		KSTEER.NEED_TO_WR_OFFSET_FLG= 1;
	}
	else
	{
		KSTEER.NEED_TO_WR_OFFSET_FLG=0;
	}

	if(KSTEER.NEED_TO_WR_OFFSET_FLG==1)
	{
	    tempW0 =McrAbs(KSTEER.model_offset - KSTEER.offset_of_eeprom2);
	    if(KSTEER.eeprom_update_TH > tempW0)
	    {
	    	KSTEER.NEED_TO_WR_OFFSET_FLG=0;
	    }
	    else
	    {
	    	;
	    }
	}
	else
	{
		;
	}
	
	
		#if __CAN_SAS_EEP_OFFSET_APPLY  ==1
		if(lsespu1EEPSASresetFlg==1)
		{
			KSTEER.NEED_TO_WR_OFFSET_FLG=1;
    
            if( (lsespu1EEPSASresetFlg==1) && (STEER_STABLE_OK==0) && (KSTEER.FIRST_OFFSET_CAL_FLG==0) )
            {
		        KSTEER.model_offset = 0;	    
		    }
		    else
		    {
			    KSTEER.model_offset = steer_offset_f;
		    }		
			
		}
		else
		{
			;
		}
		#else
		
		#endif
		
	#endif


	if((wbu1YawSNUnmatchFlg||(KYAW.eep_max==KYAW.eep_min)||((KYAW.eep_max-KYAW.eep_min)>YAW_4DEG))
	&&(KYAW.eep_max_w_flg==0)&&(KYAW.eep_min_w_flg==0)&&(yaw_offset_reliablity>=40)&&(!KYAW.eep_first_ok_flg))
	{
		KYAW.eep_max=yaw_offset_f;
		KYAW.eep_min=yaw_offset_f;
		KYAW.eep_first_ok_flg=1;
		KLAT.eep_max_w_flg=1;
		KLAT.eep_min_w_flg=1;
	}
	else
	{
		;
	}
	if(yaw_offset_reliablity>=60)
	{
		if(yaw_offset_f>KYAW.eep_max)
		{
			KYAW.eep_max_w=yaw_offset_f;
			KYAW.eep_max_w_flg=1;
		}
		else if(yaw_offset_f<KYAW.eep_min)
		{
			KYAW.eep_min_w=yaw_offset_f;
			KYAW.eep_min_w_flg=1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	if((wbu1YawSNUnmatchFlg||(KLAT.eep_max==KLAT.eep_min)||((KLAT.eep_max-KLAT.eep_min)>LAT_0G15G))
	&&(KLAT.eep_max_w_flg==0)&&(KLAT.eep_min_w_flg==0)&&(alat_offset_reliablity>=40)&&(!KLAT.eep_first_ok_flg))
	{
		KLAT.eep_max=alat_offset_f;
		KLAT.eep_min=alat_offset_f;
		KLAT.eep_first_ok_flg=1;
		KLAT.eep_max_w_flg=1;
		KLAT.eep_min_w_flg=1;
	}
	else
	{
		;
	}
	if(alat_offset_reliablity>=60)
	{
		if(alat_offset_f>KLAT.eep_max)
		{
			KLAT.eep_max_w=alat_offset_f;
			KLAT.eep_max_w_flg=1;
		}
		else if(alat_offset_f<KLAT.eep_min)
		{
			KLAT.eep_min_w=alat_offset_f;
			KLAT.eep_min_w_flg=1;
		}
		else
		{
			;
		}

	}
	else
	{
		;
	}
	#if __STEER_SENSOR_TYPE==CAN_TYPE
	if((KSTEER.eep_max_w_flg==0)&&(KSTEER.eep_min_w_flg==0)&&(steer_offset_reliablity>=30)&&(!KSTEER.eep_first_ok_flg))
	{
		KSTEER.eep_max_w=steer_offset_f;
		KSTEER.eep_min_w=steer_offset_f;
		KSTEER.eep_first_ok_flg=1;
		KSTEER.eep_max_w_flg=1;
		KSTEER.eep_min_w_flg=1;
	}
	else
	{
		;
	}
	if(steer_offset_reliablity>=30)
	{
		if(steer_offset_f>KSTEER.eep_max_w)
		{
			KSTEER.eep_max_w=steer_offset_f;
			KSTEER.eep_max_w_flg=1;
			KSTEER.eep_min_w_flg=1;
		}
		else if(steer_offset_f<KSTEER.eep_min_w)
		{
			KSTEER.eep_min_w=steer_offset_f;
			KSTEER.eep_max_w_flg=1;
			KSTEER.eep_min_w_flg=1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	#endif
	if((wbu1YawSNUnmatchFlg||(yaw_still_eep_max==yaw_still_eep_min)||((yaw_still_eep_max-yaw_still_eep_min)>YAW_2DEG))
	&&(yaw_ofst_max_wr_flg==0)&&(yaw_ofst_min_wr_flg==0)&&FS_YAW_OFFSET_OK_FLG&&(!yaw_first_ok_flg))
	{
		yaw_still_eep_max=yaw_correction;
		yaw_still_eep_min=yaw_correction;
		yaw_first_ok_flg=1;
		yaw_ofst_max_wr_flg=1;
		yaw_ofst_min_wr_flg=1;
	}
	else
	{
		;
	}
	
	if(FS_YAW_OFFSET_OK_FLG)
	{
		if(yaw_correction>yaw_still_eep_max)
		{
			yaw_still_eep_max_w=yaw_correction;
			yaw_ofst_max_wr_flg=1;
		}
		else if(yaw_correction<yaw_still_eep_min)
		{
			yaw_still_eep_min_w=yaw_correction;
			yaw_ofst_min_wr_flg=1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	
}

static void LSESP_vChkSensorModel(void)
{
	/*Initialize*/
	KLAT.M1_FRONT_OK=1;
	KSTEER.M1_FRONT_OK=1;
	KYAW.M1_FRONT_OK=1;
	KLAT.M2_REAR_OK=1;
	KSTEER.M2_REAR_OK=1;
	KYAW.M2_REAR_OK=1;
	KLAT.M3_LAT_YAW_STR=1;
	KSTEER.M3_LAT_YAW_STR=1;
	KYAW.M3_LAT_YAW_STR=1;
	KLAT.M4_LAT_YAW_STR=1;
	KSTEER.M4_LAT_YAW_STR=1;
	KYAW.M4_LAT_YAW_STR=1;

  #if __REAR_D
	/*wheel check*/
	#if __SPLIT_TYPE    /*FR Split  */
	if((ABS_fl==1)||(ABS_fr==1)||(BTCS_fl==1)||(BTCS_fr==1)||(ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1) ||
		(TCL_DEMAND_PRIMARY==1)||
		(HV_VL_fl==1)||(HV_VL_fr==1)||(AV_VL_fl==1)||(AV_VL_fr==1)||(BUILT_fl==1)||(BUILT_fr==1) )
    #else               /* X split */
	if((ABS_fl==1)||(ABS_fr==1)||(BTCS_fl==1)||(BTCS_fr==1)||(ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1) ||
		(TCL_DEMAND_fl==1)||(TCL_DEMAND_fr==1)||
		(HV_VL_fl==1)||(HV_VL_fr==1)||(AV_VL_fl==1)||(AV_VL_fr==1)||(BUILT_fl==1)||(BUILT_fr==1)  )
	#endif    		
	{
		 KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	#if __SPLIT_TYPE   
	if((ABS_rl==1)||(ABS_rr==1)||(BTCS_rl==1)||(BTCS_rr==1)||(ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1) ||
		(TCL_DEMAND_SECONDARY==1)||
		(HV_VL_rl==1)||(HV_VL_rr==1)||(AV_VL_rl==1)||(AV_VL_rr==1)||(BUILT_rl==1)||(BUILT_rr==1)||
		(TCS_ON==1) )
	#else
	if((ABS_rl==1)||(ABS_rr==1)||(BTCS_rl==1)||(BTCS_rr==1)||(ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1) ||
		(TCL_DEMAND_fl==1)||(TCL_DEMAND_fr==1)||
		(HV_VL_rl==1)||(HV_VL_rr==1)||(AV_VL_rl==1)||(AV_VL_rr==1)||(BUILT_rl==1)||(BUILT_rr==1)||
		(TCS_ON==1) )	
	#endif	
	{
		KYAW.M2_REAR_OK=0;
	}
	
  #else  /* Front Wheel Drive */

	/*wheel check*/
	#if __SPLIT_TYPE    /*FR Split  */
	if((ABS_fl==1)||(ABS_fr==1)||(BTCS_fl==1)||(BTCS_fr==1)||(ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1) ||
		(TCL_DEMAND_PRIMARY==1)||
		(HV_VL_fl==1)||(HV_VL_fr==1)||(AV_VL_fl==1)||(AV_VL_fr==1)||(BUILT_fl==1)||(BUILT_fr==1)||
		(TCS_ON==1)  )
    #else               /* X split */
	if((ABS_fl==1)||(ABS_fr==1)||(BTCS_fl==1)||(BTCS_fr==1)||(ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1) ||
		(TCL_DEMAND_fl==1)||(TCL_DEMAND_fr==1)||
		(HV_VL_fl==1)||(HV_VL_fr==1)||(AV_VL_fl==1)||(AV_VL_fr==1)||(BUILT_fl==1)||(BUILT_fr==1) ||
		(TCS_ON==1) )
	#endif    		
	{
		 KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	#if __SPLIT_TYPE   
	if((ABS_rl==1)||(ABS_rr==1)||(BTCS_rl==1)||(BTCS_rr==1)||(ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1) ||
		(TCL_DEMAND_SECONDARY==1)||
		(HV_VL_rl==1)||(HV_VL_rr==1)||(AV_VL_rl==1)||(AV_VL_rr==1)||(BUILT_rl==1)||(BUILT_rr==1) )
	#else
	if((ABS_rl==1)||(ABS_rr==1)||(BTCS_rl==1)||(BTCS_rr==1)||(ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1) ||
		(TCL_DEMAND_fl==1)||(TCL_DEMAND_fr==1)||
		(HV_VL_rl==1)||(HV_VL_rr==1)||(AV_VL_rl==1)||(AV_VL_rr==1)||(BUILT_rl==1)||(BUILT_rr==1) )	
	#endif	
	{
		KYAW.M2_REAR_OK=0;
	}  
  
  
  #endif	
	
	
	KLAT.M1_FRONT_OK=KYAW.M1_FRONT_OK;
	KSTEER.M1_FRONT_OK=KYAW.M1_FRONT_OK;
	KLAT.M2_REAR_OK=KYAW.M2_REAR_OK;
	KSTEER.M2_REAR_OK=KYAW.M2_REAR_OK;

	if (SAS_CHECK_OK==1)
	{
	    esp_tempW5=LCESP_s16IInter2Point(vref5,160,800,320,500);
	}
	else
	{
	    esp_tempW5=LCESP_s16IInter3Point(vref5,160,800,320,500,400,300);
	}

	if(ALG_INHIBIT_fl||ALG_INHIBIT_fr)
	{
		KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	if (fu1WheelFLSusDet || fu1WheelFRSusDet)
	{
		KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

/*
    if(McrAbs(vref-w_speed_fl) >=V_3KPH)
*/
	if(McrAbs(vref-(FL.lsabss16WhlSpdRawSignal64/8)) >=VREF_3_KPH)
	{
		KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

/*
    if(McrAbs(vref-w_speed_fr) >=V_3KPH)
*/
	if(McrAbs(vref-(FR.lsabss16WhlSpdRawSignal64/8)) >=VREF_3_KPH)
	{
		KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	if((vrad_crt_fl<=VREF_2_375_KPH)||(vrad_crt_fr<=VREF_2_375_KPH))
	{
		KYAW.M1_FRONT_OK=0; /* ign on후 급출발시 후진 감지 위해 변경.*/
	}
	else
	{
		;
	}

	if((McrAbs(slip_m_fl)>=esp_tempW5)||(McrAbs(slip_m_fr)>=esp_tempW5))
	{
		KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	if(ALG_INHIBIT_rl||ALG_INHIBIT_rr)
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}

	if (fu1WheelRLSusDet || fu1WheelRRSusDet)
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}

/*
    if(McrAbs(vref-w_speed_rl) >=V_3KPH)
*/
	if(McrAbs(vref-(RL.lsabss16WhlSpdRawSignal64/8)) >=VREF_3_KPH)
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}

/*
    if(McrAbs(vref-w_speed_rr) >=V_3KPH)
*/
	if(McrAbs(vref-(RR.lsabss16WhlSpdRawSignal64/8)) >=VREF_3_KPH)
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}

	if((vrad_crt_rl<=VREF_2_375_KPH)||(vrad_crt_rr<=VREF_2_375_KPH))
	{
		KYAW.M2_REAR_OK=0; /* ign on후 급출발시 후진 감지 위해 변경.*/
	}
	else
	{
		;
	}

	if((McrAbs(slip_m_rl)>=esp_tempW5)||(McrAbs(slip_m_rr)>=esp_tempW5))
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}

	KLAT.M1_FRONT_OK=KYAW.M1_FRONT_OK;
	KSTEER.M1_FRONT_OK=KYAW.M1_FRONT_OK;
	KLAT.M2_REAR_OK=KYAW.M2_REAR_OK;
	KSTEER.M2_REAR_OK=KYAW.M2_REAR_OK;

	if(FLAG_INHIBIT_OFST_SPIN==1)
	{
		KYAW.M4_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	/*yaw alat steer check*/
	/* Fail Management */
	if((fu1AySusDet !=0) || (fu1SenRasterSuspect==1))
	{
		KYAW.M3_LAT_YAW_STR=0;
		KSTEER.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	/* Fail Management */
	if((fu1YawSusDet !=0) || (fu1SenRasterSuspect==1))
	{
		KLAT.M3_LAT_YAW_STR=0;
		KSTEER.M3_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	/* FailManagement */
	if((fu1StrSusDet ==1)||(KSTEER.SUSPECT_FLG))
	{
		KYAW.M4_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	/*
	if(McrAbs(a_yaw_deg_ss2) > 200)
	{
		KLAT.M3_LAT_YAW_STR=KSTEER.M3_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	*/
	if(McrAbs(d_alat_1_100g_s) > 650)
	{
		KYAW.M3_LAT_YAW_STR=0;
		KSTEER.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	if(SAS_CHECK_OK==0)
	{
		KYAW.M4_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	if(steer_rate_deg_s2 > 800)
	{
		KYAW.M4_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	if(McrAbs((vref/8) * (wheel_angle_100deg/25)) > 12500)
	{
		KYAW.M4_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}


	/*low speed check*/
	if(vref5<VREF_2_KPH)
	{
		KSTEER.M3_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	if(vref5<VREF_3_KPH)
	{
	    KYAW.M1_FRONT_OK=0;
	    KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}
	if(vref5<VREF_5_KPH)
	{
	    KLAT.M1_FRONT_OK=0;
	    KSTEER.M1_FRONT_OK=0;
	    KLAT.M2_REAR_OK=0;
	    KSTEER.M2_REAR_OK=0;
	}
	else
	{
		;
	}
	if(vref5<VREF_10_KPH)
	{
		KLAT.M3_LAT_YAW_STR=0;
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	if(vref5<VREF_15_KPH)
	{
		KYAW.M3_LAT_YAW_STR=0;
		KSTEER.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	/*maximum check*/
	if(McrAbs(KYAW.ALGO_MODEL[1]) > YAW_75DEG)
	{
		 KYAW.M1_FRONT_OK=0;
	}
	else
	{
		;
	}

	if(McrAbs(KLAT.ALGO_MODEL[1]) > LAT_1G)
	{
		KLAT.M1_FRONT_OK=0;
	}
	else
	{
		;
	}
	if(McrAbs(KYAW.ALGO_MODEL[2]) > YAW_75DEG)
	{
		KYAW.M2_REAR_OK=0;
	}
	else
	{
		;
	}
	
	if(McrAbs(KLAT.ALGO_MODEL[2]) > LAT_1G)
	{
		KLAT.M2_REAR_OK=0;
	}
	else
	{
		;
	}
	if(McrAbs(KYAW.ALGO_MODEL[3]) > 9000)
	{
		KYAW.M3_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

	if(McrAbs(KLAT.ALGO_MODEL[3]) > LAT_1G)
	{
		KLAT.M3_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	if(McrAbs(KYAW.ALGO_MODEL[4]) > YAW_75DEG)
	{
		KYAW.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

  if(McrAbs(KLAT.ALGO_MODEL[4]) > LAT_1G)
	{
		KLAT.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}
	if(McrAbs(wheel_angle_100deg) > 2800)
	{
		KYAW.M4_LAT_YAW_STR=0;
	}
	else
	{
		;
	}

}


static void LSESP_vCalSensorModel(void)
{
	uint8_t i;
	uint16_t WBSR;
    // uint32_t l,h,s,vch;
	// opttime : ksw// 
	int32_t  vch,l;
	uint32_t h,s;

	int32_t w,y;
	static int16_t lsesps16TemporaryYawrate;
	static int16_t lsesps16TemporaryLatG;
	static int16_t lsesps16TemporarySteer;

	uint8_t lsespu8FilterWheelVelocity;
	uint8_t lsespu8CntWheelRoughFlg;

/*---------------------------------------------------------------------------------------------------
  noise filtering
---------------------------------------------------------------------------------------------------*/
	KSTEER.raw_signal_old = KSTEER.raw_signal;
	KYAW.raw_signal_old = KYAW.raw_signal;
	KLAT.raw_signal_old = KLAT.raw_signal;

	KSTEER.raw_signal = LCESP_s16Lpf1Int(c_steer_1_10deg2, KSTEER.raw_signal_old, L_U8FILTER_GAIN_10MSLOOP_12HZ); /*/12Hz*/
	if(cbit_process_step!=1)
	{
	    KYAW.raw_signal = LCESP_s16Lpf1Int(yaw_1_100deg2, KYAW.raw_signal_old, L_U8FILTER_GAIN_10MSLOOP_7HZ); /*/7Hz*/

		if(a_lat_1_1000g2>(a_lat_1_1000g2_old+70))
		{
			tempW0=a_lat_1_1000g2_old+70; /*/0.07G/7ms=10G/s*/
		}
		else if(a_lat_1_1000g2<(a_lat_1_1000g2_old-70))
		{
			tempW0=a_lat_1_1000g2_old-70;
		}
		else
		{
			tempW0=a_lat_1_1000g2;
		}

		KLAT.raw_signal = LCESP_s16Lpf1Int(tempW0, KLAT.raw_signal_old, L_U8FILTER_GAIN_10MSLOOP_3HZ); /*/3Hz*/
	}
	else
	{
	    KYAW.raw_signal = KYAW.raw_signal_old;
	    KLAT.raw_signal = KLAT.raw_signal_old;
	}
	KSTEER.raw_signal2 = LCESP_s16Lpf1Int(steer_1_10deg, KSTEER.raw_signal2, L_U8FILTER_GAIN_10MSLOOP_12HZ); /*/12Hz*/
	KYAW.raw_signal2 = LCESP_s16Lpf1Int(yaw_1_100deg, KYAW.raw_signal2, L_U8FILTER_GAIN_10MSLOOP_7HZ); /*/7Hz*/
	KLAT.raw_signal2 = LCESP_s16Lpf1Int(a_lat_1_1000g, KLAT.raw_signal2, L_U8FILTER_GAIN_10MSLOOP_3HZ); /*/3Hz*/

	lsespu8CntWheelRoughFlg=(uint8_t)FL.Rough_road_detect_flag0+(uint8_t)FR.Rough_road_detect_flag0+(uint8_t)RL.Rough_road_detect_flag0+(uint8_t)RR.Rough_road_detect_flag0;

	if (vref5<VREF_2_KPH)
	{
		lsespu8FilterWheelVelocity=99;/*0.2Hz*/
	}
	else if (lsespu8CntWheelRoughFlg>=2)
	{
		lsespu8FilterWheelVelocity=90;/*2Hz*/
	}
	else if (lsespu8CntWheelRoughFlg>=1)
	{
		lsespu8FilterWheelVelocity=85;/*3Hz*/
	}
	else
	{
		lsespu8FilterWheelVelocity=70;/*7Hz*/
	}

	#if __WL_SPEED_RESOL_CHANGE 
	lsesps16WheelVelocityFL = LSESP_s16FilterPecentage(FL.vrad_crt_resol_change, lsesps16WheelVelocityFL, lsespu8FilterWheelVelocity);/* r:1/64kph*/
	lsesps16WheelVelocityFR = LSESP_s16FilterPecentage(FR.vrad_crt_resol_change, lsesps16WheelVelocityFR, lsespu8FilterWheelVelocity);
	lsesps16WheelVelocityRL = LSESP_s16FilterPecentage(RL.vrad_crt_resol_change, lsesps16WheelVelocityRL, lsespu8FilterWheelVelocity);
	lsesps16WheelVelocityRR = LSESP_s16FilterPecentage(RR.vrad_crt_resol_change, lsesps16WheelVelocityRR, lsespu8FilterWheelVelocity);

	#else
	lsesps16WheelVelocityFL = LSESP_s16FilterPecentage(vrad_crt_fl<<3, lsesps16WheelVelocityFL, lsespu8FilterWheelVelocity);/* r:1/64kph*/
	lsesps16WheelVelocityFR = LSESP_s16FilterPecentage(vrad_crt_fr<<3, lsesps16WheelVelocityFR, lsespu8FilterWheelVelocity);
	lsesps16WheelVelocityRL = LSESP_s16FilterPecentage(vrad_crt_rl<<3, lsesps16WheelVelocityRL, lsespu8FilterWheelVelocity);
	lsesps16WheelVelocityRR = LSESP_s16FilterPecentage(vrad_crt_rr<<3, lsesps16WheelVelocityRR, lsespu8FilterWheelVelocity);
	#endif

/*---------------------------------------------------------------------------------------------------
  phase compensatioin
--------------------------------------------------------------------------------------------------- */
	KSTEER.measured_by_sens_old = KSTEER.measured_by_sens;
	KYAW.measured_by_sens_old = KYAW.measured_by_sens;
	KLAT.measured_by_sens_old = KLAT.measured_by_sens;

	if(SAS_CHECK_OK==1)
	{
		KSTEER.measured_by_sens = LSESP_s16FilterPecentage(KSTEER.raw_signal, KSTEER.measured_by_sens_old, 95); /*1Hz*/
	}
	else
	{
		KSTEER.measured_by_sens = KSTEER.raw_signal;
	}
	KYAW.measured_by_sens   = LSESP_s16FilterPecentage(KYAW.raw_signal, KYAW.measured_by_sens_old, 90); /*2Hz*/
	KLAT.measured_by_sens   = KLAT.raw_signal;


/* KSTEER.ALGO_MODEL[0]*/

	KSTEER.ALGO_MODEL_old[0]=lsesps16TemporarySteer;
	KSTEER.ALGO_MODEL[0] = KSTEER.raw_signal2- steer_offset_f;
	lsesps16TemporarySteer = KSTEER.measured_by_sens - steer_offset_f;

    /* C626_MGH80_MACRO_CHECK_JJJ */
    /*
	#if __MODEL_CHANGE==1
	tempW2 = lsesps16TemporarySteer;
	tempW1 = 100;
	tempW0 = pEspModel->S16_STEERING_RATIO_10;
	#else
	tempW2 = lsesps16TemporarySteer;
	tempW1 = 10;
	tempW0 = STEERING_RATIO;
	#endif
    */
	tempW2 = lsesps16TemporarySteer;
	tempW1 = 100;
	tempW0 = pEspModel->S16_STEERING_RATIO_10;
	/* C626_MGH80_MACRO_CHECK_JJJ */

	wheel_angle_100deg = (int16_t)( (((int32_t)tempW2)*tempW1)/tempW0 ) ;

/* KYAW.ALGO_MODEL[0]*/

	KYAW.ALGO_MODEL_old[0]=lsesps16TemporaryYawrate;
	KYAW.ALGO_MODEL[0] = KYAW.raw_signal2 - yaw_offset_f;

	if (vref5<VREF_1_KPH)
	{
		lsesps16TemporaryYawrate = LSESP_s16FilterPecentage((KYAW.measured_by_sens - yaw_offset_f), KYAW.ALGO_MODEL_old[0], 90);
	}
	else
	{
		lsesps16TemporaryYawrate=KYAW.measured_by_sens - yaw_offset_f;
	}

	if(BACK_DIR==1)
	{
	    KYAW.ALGO_MODEL[0] = -KYAW.ALGO_MODEL[0];
	    lsesps16TemporaryYawrate = -lsesps16TemporaryYawrate;
	    /*a_yaw_deg_ss2 = -a_yaw_deg_ss2;*/
	}
	else
	{
		;
	}

/* KLAT.ALGO_MODEL[0] */

	KLAT.ALGO_MODEL_old[0]=lsesps16TemporaryLatG;
	KLAT.ALGO_MODEL[0] = KLAT.raw_signal2 - alat_offset_f;

	if (vref5<VREF_1_KPH)
	{
		lsesps16TemporaryLatG = LSESP_s16FilterPecentage((KLAT.measured_by_sens - alat_offset_f), KLAT.ALGO_MODEL_old[0], 90);
	}
	else
	{
		lsesps16TemporaryLatG = KLAT.measured_by_sens - alat_offset_f;
	}


/* model_1*/

	KYAW.ALGO_MODEL_old[1]=KYAW.ALGO_MODEL[1];

	tempW2 = lsesps16WheelVelocityFR-lsesps16WheelVelocityFL;
	if( tempW2>1280 )
	{
		tempW2 = 1280;
	}
	else if( tempW2<-1280 )
	{
		tempW2  =-1280;
	}
	else
	{
		;
	}
	tempW1 = 2487;
	/* C626_MGH80_MACRO_CHECK_JJJ */
	/*
	#if __MODEL_CHANGE==1
	tempW0 = (pEspModel->S16_WHEEL_TREAD_FRONT/10);
	#else
	tempW0 = (TRACK_WIDTH_F/10);
	#endif
    */
    tempW0 = (pEspModel->S16_WHEEL_TREAD_FRONT/10);
	/* C626_MGH80_MACRO_CHECK_JJJ */
	
	tempW3=(int16_t)( (((int32_t)tempW2)*tempW1)/tempW0 )  ;
	
	KYAW.ALGO_MODEL[1] = LSESP_s16FilterPecentage(tempW3, KYAW.ALGO_MODEL_old[1], 90);

	if(KYAW.ALGO_MODEL[1]>(KYAW.ALGO_MODEL_old[1]+150))
	{
		KYAW.ALGO_MODEL[1]=KYAW.ALGO_MODEL_old[1]+150;
	}
	else if(KYAW.ALGO_MODEL[1]<(KYAW.ALGO_MODEL_old[1]-150))
	{
		KYAW.ALGO_MODEL[1]=KYAW.ALGO_MODEL_old[1]-150;
	}
	else
	{
		;
	}

	KLAT.ALGO_MODEL_old[1]=KLAT.ALGO_MODEL[1];

	KLAT.ALGO_MODEL[1] = (int16_t)( (((int32_t)KYAW.ALGO_MODEL[1])*vref5)/1617 ) ;


/* model_2*/

	KYAW.ALGO_MODEL_old[2]=KYAW.ALGO_MODEL[2];

	tempW2 = lsesps16WheelVelocityRR-lsesps16WheelVelocityRL;
	if( tempW2>1280 )
	{
		tempW2 = 1280;
	}
	else if( tempW2<-1280 )
	{
		tempW2  =-1280;
	}
	else
	{
		;
	}
	tempW1 = 2487;
	
    /* C626_MGH80_MACRO_CHECK_JJJ */
    /*	
	#if __MODEL_CHANGE==1
	tempW0 = (pEspModel->S16_WHEEL_TREAD_REAR/10);
	#else
	tempW0 = (TRACK_WIDTH_R/10);
	#endif
	*/
	tempW0 = (pEspModel->S16_WHEEL_TREAD_REAR/10);	
	/* C626_MGH80_MACRO_CHECK_JJJ */
	
	tempW3 = (int16_t)( (((int32_t)tempW2)*tempW1)/tempW0 ) ;

	KYAW.ALGO_MODEL[2] = LSESP_s16FilterPecentage(tempW3, KYAW.ALGO_MODEL_old[2], 80);

	if(KYAW.ALGO_MODEL[2]>(KYAW.ALGO_MODEL_old[2]+150))
	{
		KYAW.ALGO_MODEL[2]=KYAW.ALGO_MODEL_old[2]+150;
	}
	else if(KYAW.ALGO_MODEL[2]<(KYAW.ALGO_MODEL_old[2]-150))
	{
		KYAW.ALGO_MODEL[2]=KYAW.ALGO_MODEL_old[2]-150;
	}
	else
	{
		;
	}

	KLAT.ALGO_MODEL_old[2]=KLAT.ALGO_MODEL[2];

	KLAT.ALGO_MODEL[2] = (int16_t)( (((int32_t)KYAW.ALGO_MODEL[2])*vref5)/1617 ) ;


/* model_3*/

	KYAW.ALGO_MODEL_old[3]=KYAW.ALGO_MODEL[3];

	tempW2 = lsesps16TemporaryLatG;
	if(vref5 < VREF_10_KPH)
	{
		if( tempW2>LAT_0G8G )
		{
			tempW2 = LAT_0G8G;
		}
		else if( tempW2<-LAT_0G8G )
		{
			tempW2  =-LAT_0G8G;
		}
		else
		{
			;
		}

		tempW0 = VREF_10_KPH;
	}
	else
	{
		tempW0 = vref5;
	}
	KYAW.ALGO_MODEL[3]= (int16_t)( (((int32_t)tempW2)*1617)/tempW0 ) ;

	KLAT.ALGO_MODEL_old[3]=KLAT.ALGO_MODEL[3];

    tempW3= (int16_t)( (((int32_t)lsesps16TemporaryYawrate)*vref5)/1617 ) ;

	if(vref5 < VREF_30_KPH)
	{
		KLAT.ALGO_MODEL[3] = LSESP_s16FilterPecentage(tempW3, KLAT.ALGO_MODEL_old[3], 90);
	}
	else
	{
		KLAT.ALGO_MODEL[3] = tempW3;
	}


/* model_4 */


	/*S_R*WB/VREF*(1+(VREF/VCH)^2)*YAW = STEER
	S_R*WB/1000*3.6*8/VREF*10000*(1+VREF/VCH^2)*YAW/100*10/10000 = STEER
	(4.5@80or100kph,5.5@40,9@20)*yaw=steer
	wheel diff. 1kph=10dps
	0.4g,0.8g@100kph=8dps,16dps  0.4g,0.8g@80kph=10dps,20dps  0.4g,0.8g@60kph=13.5,27dps 0.4g,0.8g@40kph=20,40dps
	80kph 0.4g->1kph diff/10dps/45deg */
	
	/* C626_MGH80_MACRO_CHECK_JJJ */
	/*
	#if __MODEL_CHANGE==1
    */
	/* 81368=10(scale)*9.81*3.6^2*8^2 */
    /*
//	vch=(uint32_t)(((uint32_t)pEspModel->S16_LENGTH_CG_TO_FRONT+(uint32_t)pEspModel->S16_LENGTH_CG_TO_REAR)*81368)/pEspModel->S16_UNDERSTEER_GRADIENT;
//	l = (uint32_t)(((uint32_t)1000000*(uint32_t)vref5/vch)*(uint32_t)vref5/100) + (uint32_t)10000;
//	WBSR =(uint16_t)(((uint32_t)18*((uint32_t)pEspModel->S16_LENGTH_CG_TO_FRONT+(uint32_t)pEspModel->S16_LENGTH_CG_TO_REAR)*pEspModel->S16_STEERING_RATIO_10)/(uint32_t)6250);

	// opttime : ksw
	tempW0 = pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR;	
	vch=( ((int32_t)tempW0*81368)/(pEspModel->S16_UNDERSTEER_GRADIENT) ) ;	
	l =(((int32_t)1000000*vref5/vch*vref5)/100) + 10000;
	
	tempW0 = pEspModel->S16_LENGTH_CG_TO_FRONT+ pEspModel->S16_LENGTH_CG_TO_REAR ;
	WBSR  =  (uint16_t)(((uint32_t)tempW0*pEspModel->S16_STEERING_RATIO_10)/(347));

	#else
	l = (uint32_t)((uint32_t)10000*(uint32_t)vref5/(uint32_t)V_CH*(uint32_t)vref5/(uint32_t)V_CH) + (uint32_t)10000;
	WBSR =(uint16_t)(((uint32_t)18*WHEEL_BASE*STEERING_RATIO)/(uint32_t)625);
	#endif
	*/
    tempW0 = pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR;	
	vch=( ((int32_t)tempW0*81368)/(pEspModel->S16_UNDERSTEER_GRADIENT) ) ;	
	l =(((int32_t)1000000*vref5/vch*vref5)/100) + 10000;
	
	tempW0 = pEspModel->S16_LENGTH_CG_TO_FRONT+ pEspModel->S16_LENGTH_CG_TO_REAR ;
	WBSR  =  (uint16_t)(((uint32_t)tempW0*pEspModel->S16_STEERING_RATIO_10)/(347));
    /* C626_MGH80_MACRO_CHECK_JJJ */
	
	h =(uint32_t)((uint32_t)l*WBSR/1000);
	s =(uint32_t)((uint32_t)vref5*100);

	KYAW.ALGO_MODEL_old[4]=KYAW.ALGO_MODEL[4];

	w=(int32_t)((int32_t)lsesps16TemporarySteer*(int32_t)s);
	KYAW.ALGO_MODEL[4] =(int16_t)( (int32_t)w /(int32_t)h );

	KLAT.ALGO_MODEL_old[4]=KLAT.ALGO_MODEL[4];

	/*   yaw steady state   */
    #if (__YAW_SS_MODEL_ENABLE==1) && (!SIM_MATLAB)
	lsesps16YawSS_temp = (int16_t)( (int32_t)((int32_t)wstr*(int32_t)s) /(int32_t)h );
    #endif

    KLAT.ALGO_MODEL[4] = (int16_t)( (((int32_t)KYAW.ALGO_MODEL[4])*vref5)/1617 ) ;

	  if(s<=0)
	 {
	 	s=1;
	 }
	else
	{
		;
	}

	KSTEER.ALGO_MODEL_old[1]=KSTEER.ALGO_MODEL[1];

	y=(int32_t)((int32_t)h*(int32_t)KYAW.ALGO_MODEL[1] );
	KSTEER.ALGO_MODEL[1]=(int16_t)( (int32_t)y /(int32_t)s );

	KSTEER.ALGO_MODEL_old[2]=KSTEER.ALGO_MODEL[2];

	y=(int32_t)((int32_t)h*(int32_t)KYAW.ALGO_MODEL[2] );
	KSTEER.ALGO_MODEL[2]=(int16_t)( (int32_t)y /(int32_t)s );

	KSTEER.ALGO_MODEL_old[3]=KSTEER.ALGO_MODEL[3];

	y=(int32_t)((int32_t)h*(int32_t)lsesps16TemporaryYawrate );
	KSTEER.ALGO_MODEL[3]=(int16_t)( (int32_t)y /(int32_t)s );

	KSTEER.ALGO_MODEL_old[4]=KSTEER.ALGO_MODEL[4];

	y=(int32_t)((int32_t)h*(int32_t)KYAW.ALGO_MODEL[3] );
	KSTEER.ALGO_MODEL[4]=(int16_t)( (int32_t)y /(int32_t)s );

	if(vref5<VREF_5_KPH)
	{
	    for (i=1;i<=4;i++)
	    {
	        if(KSTEER.ALGO_MODEL[i]>KSTEER.ALGO_MODEL_old[i]+50)
	        {
	        	KSTEER.ALGO_MODEL[i]=KSTEER.ALGO_MODEL_old[i]+50;
	        }
	        else if(KSTEER.ALGO_MODEL[i]<KSTEER.ALGO_MODEL_old[i]-50)
	        {
	        	KSTEER.ALGO_MODEL[i]=KSTEER.ALGO_MODEL_old[i]-50;
	        }
	        else
	        {
	        	;
	        }

	        if(KSTEER.ALGO_MODEL[i]>10000)
	        {
	        	KSTEER.ALGO_MODEL[i]=10000;
	        }
	        else if(KSTEER.ALGO_MODEL[i]<-10000)
	        {
	        	KSTEER.ALGO_MODEL[i]=-10000;
	        }
	        else
	        {
	        	;
	        }
	    }
	}
	else
	{
		;
	}
}

static void LSESP_vCalSensorResidual(struct OFFSET_STRUCT *PTR)
{
    PTR->diff_model_to_sens = PTR->measured_by_sens - PTR->model_avr;
}

static void LSESP_vDctStraightDriving(void)
{
	uint16_t lsespu16MinWhSpeed;
	uint16_t lsespu16MaxWhSpeed;
	uint16_t lsespu16FlWhspeed;
	uint16_t lsespu16FrWhspeed;
	uint16_t lsespu16RlWhspeed;
	uint16_t lsespu16RrWhspeed;

	#if __WL_SPEED_RESOL_CHANGE
	lsespu16FlWhspeed=McrAbs(FL.vrad_crt_resol_change);
	lsespu16FrWhspeed=McrAbs(FR.vrad_crt_resol_change);
	lsespu16RlWhspeed=McrAbs(RL.vrad_crt_resol_change);
	lsespu16RrWhspeed=McrAbs(RR.vrad_crt_resol_change);
	#else
	lsespu16FlWhspeed=McrAbs(vrad_crt_fl<<3);
	lsespu16FrWhspeed=McrAbs(vrad_crt_fr<<3);
	lsespu16RlWhspeed=McrAbs(vrad_crt_rl<<3);
	lsespu16RrWhspeed=McrAbs(vrad_crt_rr<<3);
	#endif
	tempW4=(lsespu16FlWhspeed>lsespu16FrWhspeed)?lsespu16FlWhspeed:lsespu16FrWhspeed;
	tempW5=(lsespu16RlWhspeed>lsespu16RrWhspeed)?lsespu16RlWhspeed:lsespu16RrWhspeed;
	tempW6=(lsespu16FlWhspeed<lsespu16FrWhspeed)?lsespu16FlWhspeed:lsespu16FrWhspeed;
	tempW7=(lsespu16RlWhspeed<lsespu16RrWhspeed)?lsespu16RlWhspeed:lsespu16RrWhspeed;
	lsespu16MaxWhSpeed=(tempW4>tempW5)?tempW4:tempW5;
	lsespu16MinWhSpeed=(tempW6<tempW7)?tempW6:tempW7;

	if( (lsespu16MinWhSpeed > VREF_15_KPH_RESOL_CHANGE)&&(lsespu16MaxWhSpeed < VREF_120_KPH_RESOL_CHANGE) )
    {
    	SPEED_OK_FLG=1;
    }
    else
    {
    	SPEED_OK_FLG=0;
    }

    if( McrAbs(KSTEER.ALGO_MODEL[1]) >= WSTR_5_DEG)
    {
        if(pm4deg_cnt_at_steer1 <0xff)
        {
        	pm4deg_cnt_at_steer1++;
        }
        else
        {
        	;
        }
    }
    else
    {
    	pm4deg_cnt_at_steer1=0;
    }

    if( McrAbs(KYAW.ALGO_MODEL[1]) >= YAW_1DEG)
    {
        if(pm1deg_cnt_at_yaw1 <0xff)
        {
        	pm1deg_cnt_at_yaw1++;
        }
    }
    else
    {
    	pm1deg_cnt_at_yaw1=0;
    }

/*
    yaw_m1_1_100deg = (int16_t)(((int32_t)(125*(int32_t)(FR.M_vrad-FL.M_vrad))*14325)/(9*TRACK_WIDTH_F));
    m = ((int32_t)yaw_m1_1_100deg*i)/10;
    STEER.model[1] = ((int32_t)m*CON2_STEER_MDL)/100000;

    yaw_m1_1_100deg=[125*(808-800)]*14325 / (9*1540)=14325000/13860=1033.5, where FR.M_vrad=101kph=808.
    m=(1033.5*3894.46)/10=402492.44
    STEER.model[1]=(402492.44*124.416)/100000=50079499/100000=500.76
    즉, EF,vref=100kph,FR.M_vrad-FL.M_vrad=1kph일때, STEER.model[1]=50 deg
    따라서, EF,vref=100kph,STEER.model[1]=4deg는 FR.M_vrad-FL.M_vrad=0.08kph, 0.125kph(1)차이일때 6.25deg
*/

    if( McrAbs(KSTEER.ALGO_MODEL[2]) >= WSTR_5_DEG)
    {
        if(pm4deg_cnt_at_steer2 <0xff)
        {
        	pm4deg_cnt_at_steer2++;
        }
        else
        {
        	;
        }

    }
    else
    {
    	pm4deg_cnt_at_steer2=0;
    }

    if( McrAbs(KYAW.ALGO_MODEL[2]) >= YAW_1DEG)
    {
        if(pm1deg_cnt_at_yaw2 <0xff)
        {
        	pm1deg_cnt_at_yaw2++;
        }
        else
        {
        	;
        }
    }
    else
    {
    	pm1deg_cnt_at_yaw2=0;
    }

    if( McrAbs(KSTEER.ALGO_MODEL[3]) >= WSTR_2_DEG)
    {
        if(more_than_pm2deg_at_steer3 <0xff)
        {
        	more_than_pm2deg_at_steer3++;
        }
        else
        {
        	;
        }
    }
    else
    {
    	more_than_pm2deg_at_steer3 =0;
    }
/*
    i = ((uint32_t)vref*1000)/V_CH;    l = i*i;    l += 1,000,000; i = l/(uint16_t)vref;
    ex, EF vref=100kph. i=(800*1000)/550=1454.54, l=3115570.2, i= 3115570.2/800=3894.46

    m = ((int32_t)YAW.model[0]*i)/10;
    STEER.model[3] = ((int32_t)m*CON2_STEER_MDL)/100000;
    ex, YAW.model[0]=100(1deg)일때. m=(100*i)/10 =100*3894.46/10=38944.6
    #define CON2_STEER_MDL (uint16_t)(((uint32_t)8*3600*WHEEL_BASE*STEERING_RATIO)/(uint32_t)10000000)
    CON2_STEER_MDL=(8*3600*2700*16)/10000000=124.416

    STEER.model[3]=[m(38944.6)*CON2_STEER_MDL(124.416)]/100000=48.45
    즉, EF,vref=100kph,yaw=1deg일때, STEER.model[3]=4.845 deg
    따라서, EF,vref=100kph,STEER.model[3]=2deg는 yaw=0.413 deg해당함.
*/
    if( McrAbs(KSTEER.ALGO_MODEL[4]) >= WSTR_2_DEG)
    {
        if(more_than_pm2deg_at_steer4 <0xff)
        {
        	more_than_pm2deg_at_steer4++;
        }
        else
        {
        	;
        }
    }
    else
    {
    	more_than_pm2deg_at_steer4 =0;
    }
/*
    yaw_m1_1_100deg=(ALAT.model[0]*vref)/1617=100(0.1g)*800/1617=49.47,
    m = ((int32_t)yaw_m1_1_100deg*i)/10;
    m=49.47*3894.46(i)/10=19267.58
    STEER.model[4] = ((int32_t)m*CON2_STEER_MDL)/100000;
    STEER.model[4]=19267.58*124.416/100000=23.96
    즉, EF,vref=100kph,lateral=0.1g일때, STEER.model[4]=2.396 deg
    따라서, EF,vref=100kph,STEER.model[4]=2deg는 lateral=0.083 g해당함.
*/

    if((pm4deg_cnt_at_steer1 <=L_U8_TIME_10MSLOOP_100MS)&&(KSTEER.M1_FRONT_OK==1))
    {
    	SM1_VALID=1;
    }
    else
    {
    	SM1_VALID=0;
    }

    if((pm4deg_cnt_at_steer2 <=L_U8_TIME_10MSLOOP_100MS)&&(KSTEER.M2_REAR_OK==1))
    {
    	SM2_VALID=1;
    }
    else
    {
    	SM2_VALID=0;
    }

    if((more_than_pm2deg_at_steer3 <=L_U8_TIME_10MSLOOP_60MS)&&(KSTEER.M3_LAT_YAW_STR==1))
    {
    	SM3_VALID=1;
    }
    else
    {
    	SM3_VALID=0;
    }

    if((more_than_pm2deg_at_steer4 <=L_U8_TIME_10MSLOOP_60MS)&&(KSTEER.M4_LAT_YAW_STR==1))
    {
    	SM4_VALID=1;
    }
    else
    {
    	SM4_VALID=0;
    }

    if((SM1_VALID==1)&&(SM2_VALID==1))
    {
    	SM1_N_SM2=1;
    }
    else
    {
    	SM1_N_SM2=0;
    }

    if((SM1_VALID==1)||(SM2_VALID==1))
    {
    	SM1_OR_SM2=1;
    }
    else
    {
    	SM1_OR_SM2=0;
    }

    if((SM3_VALID==1)||(SM4_VALID==1))
    {
    	SM3_OR_SM4=1;
    }
    else
    {
    	SM3_OR_SM4=0;
    }

    if( (SM3_OR_SM4==1)&&(SM1_OR_SM2==1) )
    {
    	SM1_OR_2_N_SM3_OR_4=1;
    }
    else
    {
    	SM1_OR_2_N_SM3_OR_4=0;
    }
}

static void LSESP_vDctCurveDriving(void)
{
    temp_curv_sign_old=temp_curv_sign;

    /*-----------------------------straight condition*/
    if( (PHZ_detected_flg2==1)&&(SPEED_OK_FLG==1) && (steer_rate_deg_s2 <= 40)&&(!FLAG_BANK_DETECTED)&&((SM1_OR_2_N_SM3_OR_4==1) || (SM1_N_SM2==1)) )
    {
    	tempB4=1;
    }
    else
    {
    	tempB4=0;
    }

    esp_tempW0=LCESP_s16IInter2Point(vref5,VREF_20_KPH,50,VREF_100_KPH,50);
    if( (yaw_out<=esp_tempW0)&&(yaw_out>= -esp_tempW0)&&(tempB4==1) )
    {
    	tempB5=1;
    }
    else
    {
    	tempB5=0;
    }
    /*-----------------------------yaw_out*/
    CURV_KYAW_BUF = LCESP_s16IInter2Point(vref5,VREF_20_KPH,25,VREF_100_KPH,25);

    if( yaw_out >= CURV_KYAW_BUF)
    {
    	KYAW_SIGN = 1;
    }
    else if ( (yaw_out > -CURV_KYAW_BUF) && (KYAW_SIGN == 1) )
    {
    	KYAW_SIGN =1;
    }
    else if ( (yaw_out <= -CURV_KYAW_BUF)  )
    {
    	KYAW_SIGN = -1;
    }
    else if ( (yaw_out < CURV_KYAW_BUF) && (KYAW_SIGN == -1) )
    {
    	KYAW_SIGN =-1;
    }
    else
    {
    	KYAW_SIGN = 0;
    }

    /*-----------------------------DETECT STEER DIRECTION AND COUNT UP*/
    if( (KYAW_SIGN>0) &&(tempB5==1) )
    {
        temp_curv_sign=1;
        if( yaw_out >= CURV_KYAW_BUF)
        {
        	curv_pos_count++;
        	curv_neg_count--;
        }
        else
        {
        	;
        }
    }
    else if( (KYAW_SIGN<0)&&(tempB5==1) )
    {
        temp_curv_sign=-1;
        if(yaw_out <= -CURV_KYAW_BUF)
        {
        	curv_neg_count++;
        	curv_pos_count--;
        }
    }
    else
    {
		;
    }
    if(lsespu1YawGstandstillflg==1)
    {
        curv_pos_count=0;
        curv_neg_count=0;
    }
    else
    {
    	;
    }
    /*-----------------------------COUNTER LIMITATION*/
    if(curv_pos_count<0)
    {
    	curv_pos_count=0;
    }
    else if(curv_pos_count>MAX_CURV_COUNT)
    {
    	curv_pos_count=MAX_CURV_COUNT;
    }
    else
    {
    	;
    }
    if(curv_neg_count<0)
    {
    	curv_neg_count=0;
    }
    else if(curv_neg_count>MAX_CURV_COUNT)
    {
    	curv_neg_count=MAX_CURV_COUNT;
    }
    else
    {
    	;
    }

    /*-----------------------------IF OPPOSITE SIDE 3SECONDS, COUNTER RESET*/
    if(temp_curv_sign>0)
    {
        if( (curv_pos_count==CURV_COUNT_CHANGE) && (temp_curv_sign_old>0) )
        {
        	curv_neg_count=0;
        }
        else
        {
        	;
        }
    }
    else if(temp_curv_sign<0)
    {
        if( (curv_neg_count==CURV_COUNT_CHANGE) && (temp_curv_sign_old<0) )
        {
        	curv_pos_count=0;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    /*-----------------------------OVER ONE SIDE COUNTER 15SEC,DETECTION  AT INHIBITION,IF OPPOSITE scattered 3SECONDS, RESET*/
    if( (curv_pos_count>DET_CURV_COUNT) && (curv_neg_count<DET_CURV_COUNT) )
    {
        FLAG_INHIBIT_OFST_TURN=1;
        ofst_curv_sign=1;
        reset_neg_turn_count=0;

        if((yaw_out<0)&&(tempB4==1) )
        {
        	reset_pos_turn_count++;
        }
        else
        {
        	;
        }
        if(reset_pos_turn_count>CURV_COUNT_CHANGE)
        {
            curv_pos_count=0;
            reset_pos_turn_count=0;
        }
        else
        {
        	;
        }

    }
    else if( (curv_neg_count>DET_CURV_COUNT) && (curv_pos_count<DET_CURV_COUNT) )
    {
        FLAG_INHIBIT_OFST_TURN=1;
        ofst_curv_sign=-1;
        reset_pos_turn_count=0;

        if((yaw_out>0)&&(tempB4==1) )
        {
        	reset_neg_turn_count++;
        }
        else
        {
        	;
        }
        if(reset_neg_turn_count>CURV_COUNT_CHANGE)
        {
            curv_neg_count=0;
            reset_neg_turn_count=0;
        }
        else
        {
        	;
        }
    }
    else
    {
        FLAG_INHIBIT_OFST_TURN=0;
        ofst_curv_sign=0;
        reset_pos_turn_count=0;
        reset_neg_turn_count=0;
    }
}

static void LSESP_vDctSpinningDriving(void)
{
	uint8_t  i ;
	int16_t    deltV;

	#if __REAR_D

  /*---------- Spin Comparison 1    McrAbs(vrad_crt_fl-vrad_crt_rl) */
	i=0;
	deltV=(vrad_crt_rl-vrad_crt_fl);
	LSESP_vDctSpinCount(i,deltV );
	LSESP_vDctDragCount(i,deltV );

	INHIBIT_OFST_SPIN_1 = OFST_SPIN[i];

  /*---------- Spin Comparison 2    McrAbs(vrad_crt_fr-vrad_crt_rr) */
	i=1;
	deltV=(vrad_crt_rr-vrad_crt_fr);
	LSESP_vDctSpinCount(i,deltV );
	LSESP_vDctDragCount(i,deltV );

	INHIBIT_OFST_SPIN_2 = OFST_SPIN[i] ;

  /*---------- Spin Comparison 3    McrAbs(vrad_crt_rl-vref5) */
	i=2;
	deltV=(vrad_crt_rl-vref5);
  LSESP_vDctSpinCount(i,deltV );

	INHIBIT_OFST_SPIN_3 = (OFST_SPIN[i]) ;

  /*---------- Spin Comparison 4    McrAbs(vrad_crt_rr-vref5) */
	i=3;
	deltV=(vrad_crt_rr-vref5);
	LSESP_vDctSpinCount(i,deltV );

	INHIBIT_OFST_SPIN_4 = (OFST_SPIN[i]) ;

	#else
  /*---------- Spin Comparison 1    McrAbs(vrad_crt_fl-vrad_crt_rl) */
	i=0;
	deltV=(vrad_crt_fl-vrad_crt_rl);
	LSESP_vDctSpinCount(i,deltV );
	LSESP_vDctDragCount(i,deltV );

	INHIBIT_OFST_SPIN_1 = OFST_SPIN[i] ;

  /*---------- Spin Comparison 2    McrAbs(vrad_crt_fr-vrad_crt_rr) */
	i=1;
	deltV=(vrad_crt_fr-vrad_crt_rr);
	LSESP_vDctSpinCount(i,deltV );
	LSESP_vDctDragCount(i,deltV );

	INHIBIT_OFST_SPIN_2 = OFST_SPIN[i] ;

  /*---------- Spin Comparison 3    McrAbs(vrad_crt_fl-vref5) */
	i=2;
	deltV=(vrad_crt_fl-vref5);
	LSESP_vDctSpinCount(i,deltV );

	INHIBIT_OFST_SPIN_3 = (OFST_SPIN[i]) ;

  /*---------- Spin Comparison 4    McrAbs(vrad_crt_fr-vref5) */
	i=3;
	deltV=(vrad_crt_fr-vref5);
	LSESP_vDctSpinCount(i,deltV );

	INHIBIT_OFST_SPIN_4 = (OFST_SPIN[i]) ;

	#endif

  LSESP_vDctSpinControl();

	if( (INHIBIT_OFST_SPIN_1==1)|| (INHIBIT_OFST_SPIN_2==1) || (INHIBIT_OFST_SPIN_3==1)|| (INHIBIT_OFST_SPIN_4==1) )
	{
		FLAG_INHIBIT_OFST_SPIN=1;
	}
	else
	{
		FLAG_INHIBIT_OFST_SPIN=0;
	}

}

static void LSESP_vResetStableSteerOffset(void)
{
	vdc_stable_cnt=0;
	steer_offset_stable_sum=0;
	steer_offset_stable=0;
	KSTEER.driving_accuracy_stable=0;
}

static void LSESP_vResetPHZAngle(void)
{

	steer_offset_360=0;
	#if __STEER_SENSOR_TYPE==CAN_TYPE/*060602 for HM reconstruction by eslim*/
	STR_OFFSET_360_OK=1;
	WSTR_360_CHECK=1;
	WSTR_360_OK=1;
	#elif __STEER_SENSOR_TYPE==ANALOG_TYPE
	STR_OFFSET_360_OK=0;
	WSTR_360_CHECK=0;  
	WSTR_360_OK=0;
	#else
	STR_OFFSET_360_OK=1;
	WSTR_360_CHECK=1;
	WSTR_360_OK=1;
	#endif

}

static void LSESP_vResetDrivingOffset(struct OFFSET_STRUCT *PTR)
{
	PTR->vdc_sensor_check_cnt=0;
	PTR->straight_cnt=0;
	PTR->sum_model[0]=0;
	PTR->sum_model[1]=0;
	PTR->sum_model[2]=0;
	PTR->sum_model[3]=0;
	PTR->sum_model[4]=0;
	PTR->sum_measured_by_sens=0;

	PTR->avr_model[1]=0;
	PTR->avr_model[2]=0;
	PTR->avr_model[3]=0;
	PTR->avr_model[4]=0;
	PTR->STRAIGHT_FLAG=0;
	PTR->DURING_STRAIGHT_FLG=0;
	PTR->offset_ok_cnt=0;
	PTR->offset_10sum=0;
	PTR->offset_10avr=0;

	PTR->OFFSET_10AVR_CAL_OK_FLG=0;
	PTR->MODEL_OK_AT_15KPH=0;
	PTR->FIRST_OFFSET_CAL_FLG=0;
	PTR->NTH_OFFSET_CAL_FLG=0;
	/*ONE_MORE_CHECK=0;*/

	PTR->offset_at_15kph=0;
	PTR->offset_10avr_old =0;
	PTR->offset_nth_sum=0;
	PTR->offset_at_15kph_nth=0;
	PTR->OFFSET_AT_15KPH_NTH_OK=0;
	PTR->offset_10_cnt=0;
	PTR->offset_10_sec_cnt=0;
	PTR->OFFSET_60AVR_CAL_OK_FLG=0;
	PTR->OFFST_10AVR_EEPROM=0;
	PTR->offset_10avr_first=0;
	PTR->offset_60avr=0;
	PTR->offset_60avr_old=0;
	PTR->driving_accuracy=0;

}

static void LSESP_vCalEEPROMSensorOffset(void)
{
	if(cyctime>L_U16_TIME_10MSLOOP_3MIN)
	{/*상온보장, 엔진룸 idling시 0.15'C/sec이므로 -30'C->0'C는 200sec =>3min, cyctime int32_t->2^31*0.007/60/60/24=173days*/
	    KSTEER.eeprom_update_TH =WSTR_2_DEG;
	    KYAW.eeprom_update_TH =YAW_0G2DEG;
	    KLAT.eeprom_update_TH =LAT_0G02G;
	}
	else
	{
	    KSTEER.eeprom_update_TH =WSTR_5_DEG;
	    KYAW.eeprom_update_TH =YAW_1DEG;
	    KLAT.eeprom_update_TH =LAT_0G1G;
	}

	#if __STEER_SENSOR_TYPE==CAN_TYPE/*060602 for HM reconstruction by eslim*/
	KSTEER.eeprom_update_limit =  2*WSTR_720_DEG;
	#else
	#if !__DEL_360_OFFSET
	KSTEER.eeprom_update_limit =  WSTR_180_DEG;
	#else
	KSTEER.eeprom_update_limit =  2*WSTR_720_DEG;
	#endif
	#endif
	KYAW.eeprom_update_limit   =  YAW_4DEG;
	KLAT.eeprom_update_limit   =  LAT_0G2G;
	#if __STEER_SENSOR_TYPE==CAN_TYPE
	if (KSTEER.fs_eeprom_ok_flg)
	{
		wstr_offset=KSTEER.offset_of_eeprom2;
		
		#if __CAN_SAS_EEP_OFFSET_APPLY==1
		KSTEER.eeprom_accuracy=30;
		#else
		KSTEER.eeprom_accuracy=20;
		#endif
	}
	else
	{
		if((KSTEER.eep_max_read_flg==1)&&(KSTEER.eep_min_read_flg==1))
		{
			wstr_offset=(KSTEER.eep_max+KSTEER.eep_min)/2;
			KSTEER.eeprom_accuracy=20;
		}
		else if(KSTEER.eep_max_read_flg==1)
		{
			wstr_offset=KSTEER.eep_max;
			KSTEER.eeprom_accuracy=10;
		}
		else if(KSTEER.eep_min_read_flg==1)
		{
			wstr_offset=KSTEER.eep_min;
			KSTEER.eeprom_accuracy=10;
		}
		else
		{
			wstr_offset=0;
			KSTEER.eeprom_accuracy=0;
		}
	}
	
		#if __CAN_SAS_EEP_OFFSET_APPLY==1
		if(cdu1DiagSasCaltoCL==1)
		{
		    lsespu1SASresetDiagFlg=1;	
		    KSTEER.eeprom_accuracy=0;
		}
		else
		{
			if(lsespu1SASresetDiagFlg==1)
			{
				lsespu1SASresetDiagFlg=0;
				KSTEER.eeprom_accuracy=30;
				wstr_offset=0;			
				LSESP_vResetDrivingOffset(&KSTEER);
			    LSESP_vResetStableSteerOffset();
			    lsespu1EEPSASresetFlg=1;
			}
			else
			{
				;
			}
		}
		#else
	
	    #endif
	
	
	#else
	wstr_offset=0;
	KSTEER.eeprom_accuracy=0;
	#endif

	if((KYAW.eep_max_read_flg==1)&&(KYAW.eep_min_read_flg==1))
	{
		if(KYAW.eep_max==KYAW.eep_min)
		{
			if (KYAW.fs_eeprom_ok_flg )
			{
				yaw_offset=KYAW.offset_of_eeprom2;
				KYAW.eeprom_accuracy=20;
			}
			else
			{
				yaw_offset=0;
				KYAW.eeprom_accuracy=0;
			}
		}
		else
		{
			yaw_offset=(KYAW.eep_max+KYAW.eep_min)/2;

			tempW0=KYAW.eep_max-KYAW.eep_min;
			if(tempW0<0)
			{
				tempW2=-tempW0;
			}
			else
			{
				tempW2=tempW0;
			}

            tempW4=40- (int16_t)( (((int32_t)tempW2)*10)/YAW_0G5DEG ) ;

			if(tempW4>30)
			{
				tempW4=30;
			}
			else
			{
				;
			}
			if(tempW4<0)
			{
				tempW4=0;
			}
			else
			{
				;
			}
			KYAW.eeprom_accuracy=(uint8_t)tempW4;
		}
	}
	else
	{
			if (KYAW.fs_eeprom_ok_flg )
			{
				yaw_offset=KYAW.offset_of_eeprom2;
				KYAW.eeprom_accuracy=20;
			}
			else
			{
				yaw_offset=0;
				KYAW.eeprom_accuracy=0;
			}
	}
	if((KLAT.eep_max_read_flg==1)&&(KLAT.eep_min_read_flg==1))
	{
		if(KLAT.eep_max==KLAT.eep_min)
		{
			if (KLAT.fs_eeprom_ok_flg )
			{
				alat_offset=KLAT.offset_of_eeprom2;
				KLAT.eeprom_accuracy=30;
			}
			else
			{
				alat_offset=0;
				KLAT.eeprom_accuracy=0;
			}
		}
		else
		{
			alat_offset=(KLAT.eep_max+KLAT.eep_min)/2;

			tempW0=KLAT.eep_max-KLAT.eep_min;
			if(tempW0<0)
			{
				tempW2=-tempW0;
			}
			else
			{
				tempW2=tempW0;
			}

            tempW4=50- (int16_t)( (((int32_t)tempW2)*10)/LAT_0G05G ) ;
			
			if(tempW4>40)
			{
				tempW4=40;
			}
			else
			{
				;
			}
			if(tempW4<0)
			{
				tempW4=0;
			}
			else
			{
				;
			}
			KLAT.eeprom_accuracy=(uint8_t)tempW4;
		}
	}
	else
	{
			if (KLAT.fs_eeprom_ok_flg )
			{
				alat_offset=KLAT.offset_of_eeprom2;
				KLAT.eeprom_accuracy=30;
			}
			else
			{
				alat_offset=0;
				KLAT.eeprom_accuracy=0;
			}
	}
	if((yaw_ofst_max_rd_flg==1)&&(yaw_ofst_min_rd_flg==1))
	{
		if(yaw_still_eep_max==yaw_still_eep_min)
		{
			if (fs_yaw_offset_eeprom_ok_flg )
			{
				yaw_offset=-read_yaw_eeprom_offset;
				KYAW.eeprom_accuracy=30;
			}
			else
			{
				;
			}
		}
		else
		{
			yaw_offset=-(yaw_still_eep_max+yaw_still_eep_min)/2;

			tempW0=yaw_still_eep_max-yaw_still_eep_min;
			if(tempW0<0)
			{
				tempW2=-tempW0;
			}
			else
			{
				tempW2=tempW0;
			}
            
            tempW4=50- (int16_t)( (((int32_t)tempW2)*10)/YAW_0G5DEG )  ;
			
			if(tempW4>40)
			{
				tempW4=40;
			}
			else
			{
				;
			}
			if(tempW4<0)
			{
				tempW4=0;
			}
			else
			{
				;
			}
			KYAW.eeprom_accuracy=(uint8_t)tempW4;
		}
	}
	else
	{
			if (fs_yaw_offset_eeprom_ok_flg )
			{
				yaw_offset=-read_yaw_eeprom_offset;
				KYAW.eeprom_accuracy=30;
			}
			else
			{
				;
			}
	}
	KSTEER.offset_of_eeprom =wstr_offset;
	KYAW.offset_of_eeprom = yaw_offset;
	KLAT.offset_of_eeprom =alat_offset;

	if((!FS_YAW_OFFSET_OK_FLG)&&(!KYAW.FIRST_OFFSET_CAL_FLG))
	{
	if((KYAW.offset_of_eeprom > YAW_4G5DEG)||(KYAW.offset_of_eeprom <-YAW_4G5DEG))
	    {
	    	KYAW.EEPROM_SUSPECT_FLG=1;
	    }
	    else
	    {
	    	KYAW.EEPROM_SUSPECT_FLG=0;
	    }
	}
	else
	{
		KYAW.EEPROM_SUSPECT_FLG=0;
	}

	#if (!__DEL_360_OFFSET) || (__STEER_SENSOR_TYPE==CAN_TYPE )
	KSTEER.threshold_of_eeprom  =   WSTR_2_DEG;
	#else
	KSTEER.threshold_of_eeprom  =   0;
	#endif
	KYAW.threshold_of_eeprom    =   YAW_0G5DEG;
	KLAT.threshold_of_eeprom    =   LAT_0G05G;

	if(wbu1YawSNUnmatchFlg==1)
	{
		KYAW.offset_of_eeprom=0;
		KLAT.offset_of_eeprom=0;
		KYAW.eeprom_accuracy=0;
		KLAT.eeprom_accuracy=0;
		yaw_value_init_rq_flg=1;
		lg_value_init_rq_flg=1;
	}
	else
	{
		;
	}

	if((KYAW.offset_of_eeprom>YAW_3DEG)||(KYAW.offset_of_eeprom<-YAW_3DEG))
	{
		KYAW.eeprom_accuracy-=10;
	}
	else
	{
		;
	}
	if((KLAT.offset_of_eeprom>LAT_0G05G)||(KLAT.offset_of_eeprom<-LAT_0G05G))
	{
		KLAT.eeprom_accuracy-=10;
	}
	else
	{
		;
	}


}

static void LSESP_vCalStandstillReliability(void)
{

/*cumulative*/
/*	if( (standstill_flg==1)&&(yaw_unstable_flg==0)&&(yaw_suspcs_flg==0)&&(yaw_1_100deg<YAW_7DEG)&&(yaw_1_100deg>-YAW_7DEG)
		&&(turn_table_counter==0)&&(cbit_process_step != 1)&&(fou1SenPwr1secOk==1))  */

	/* FailManagement */
	if( (lsespu1YawGstandstillflg==1)&&(fu1YawSusDet==0)&&(yaw_1_100deg<YAW_7DEG)&&(yaw_1_100deg>-YAW_7DEG)
		&&(turn_table_counter==0)&&(cbit_process_step != 1)&&(fou1SenPwr1secOk==1))
	{
		yaw_still_sec_cnt++;
		if(yaw_still_sec_cnt>L_U16_TIME_10MSLOOP_5MIN)
		{
			yaw_still_sec_cnt=L_U16_TIME_10MSLOOP_5MIN;
		}
		else
		{
			;
		}
		yaw_still_sec=yaw_still_sec_cnt;
	}
	else
	{
		yaw_still_sec_cnt=0;
	}

	esp_tempW1=yaw_still_sec/14;/*yaw_offset_sec:standstill sec 0~200sec 8571:1min 142:1s*/
	if(esp_tempW1<0)
	{
		esp_tempW1=0;
	}
	else
	{
		;
	}
	if(esp_tempW1>30)
	{
		esp_tempW1=30;
	}
	else
	{
		;
	}
	yaw_still_cum=(uint8_t)esp_tempW1;

/*repetitive*/
	if((lsespu1YawGstandstillflg==0)&&(KYAW.standstill_old==1)&&(yaw_still_sec>=L_U8_TIME_10MSLOOP_1000MS)&&FS_YAW_OFFSET_OK_FLG)
	{
		yaw_still_arr[(yaw_still_arr_cnt%6)]=lsesps16EEPYawStandOfsRead;

		if(yaw_still_arr_cnt>=11)
		{
			yaw_still_arr_cnt=6;
		}
		else
		{
			yaw_still_arr_cnt++;
		}

		if(yaw_still_arr_cnt>=5)
		{
			tempW0=((yaw_still_arr[0]+yaw_still_arr[1]+yaw_still_arr[2]+yaw_still_arr[3]+yaw_still_arr[4]+yaw_still_arr[5])/6);
			tempW1=McrAbs(yaw_still_arr[0]-tempW0);
			tempW2=McrAbs(yaw_still_arr[1]-tempW0);
			tempW3=McrAbs(yaw_still_arr[2]-tempW0);
			tempW4=McrAbs(yaw_still_arr[3]-tempW0);
			tempW5=McrAbs(yaw_still_arr[4]-tempW0);
			tempW6=McrAbs(yaw_still_arr[5]-tempW0);
			tempW7=(tempW1+tempW2+tempW3+tempW4+tempW5+tempW6)/6;

			tempW3 = (int16_t)( (((int32_t)tempW7)*10)/YAW_0G1DEG ) ;
			
			if(tempW3>0)
			{
				esp_tempW2=10-tempW3;
			}
			else
			{
				esp_tempW2=10+tempW3;
			}
			if(esp_tempW2<0)
			{
				esp_tempW2=0;
			}
			else
			{
				;
			}
			if(esp_tempW2>10)
			{
				esp_tempW2=10;
			}
			else
			{
				;
			}

			yaw_still_rep=(uint8_t)esp_tempW2;
		}
		else
		{
			;
		}
	}


#if __YAW_TEMPERATURE_COMPENSATION

if(fu1SenRasterSuspect==0)
{
   /*temperature*/
	if((lsespu1YawGstandstillflg==0)&&(KYAW.standstill_old==1)&&(yaw_still_sec>=L_U8_TIME_10MSLOOP_1000MS)&&FS_YAW_OFFSET_OK_FLG)
	{
		if(yaw_lg_temp_d_cnt>2)
		{
			yaw_lg_temp_still=yaw_lg_temp_d;
		}
		else
		{
			yaw_lg_temp_still=yaw_lg_temp;
		}
		yaw_lg_temp_still_flg=1;
	}
	else
	{
		;
	}
	KYAW.standstill_old=lsespu1YawGstandstillflg;

	if(yaw_lg_temp_still_flg)
	{
		tempW4=(yaw_lg_temp-yaw_lg_temp_still)*2/5;/*0.5dps/20K -> 25/1000 ->3/100-> *10->3/10*/
	}
	else
	{
		tempW4=0;
	}
	if(yaw_lg_temp_d_cnt>2)
	{
		tempW5=(yaw_lg_temp_dot2*1)/3;/*0.1dps/3K -> 10/300 ->sf 1/15 ->*10 ->*2/3*/
	}
	else
	{
		tempW5=0;
	}
	if(tempW4<0)
	{
		tempW4=-tempW4;
	}
	else
	{
		;
	}
	if(tempW5<0)
	{
		tempW5=-tempW5;
	}
	else
	{
		;
	}
	tempW3=(int16_t)((tempW4+tempW5)/YAW_0G2DEG);
	if(tempW3<0)
	{
		tempW3=0;
	}
	else
	{
		;
	}
	if(tempW3>40)
	{
		tempW3=40;
	}
	else
	{
		;
	}
	esp_tempW3=tempW3;

	yaw_still_tmp=(uint8_t)esp_tempW3;
}
else
{
	 yaw_still_tmp=0;
}

#else

 yaw_still_tmp=0;

#endif

/*total*/
	if(lsespu1YawGstandstillflg==1)
	{
		if(yaw_unstable_flg==1)
		{
			yaw_standstill_ok_flg=0;
		}
		else
		{
			yaw_standstill_ok_flg=1;
		}
	}
	else
	{
		;
	}

	if(FS_YAW_OFFSET_OK_FLG)
	{
		if(yaw_standstill_ok_flg==1)
		{
			if((lsespu1YawGstandstillflg==0)||((lsespu1YawGstandstillflg==1)&&(yaw_still_sec<L_U8_TIME_10MSLOOP_1000MS)))
			{
				esp_tempW4=70+yaw_still_cum+yaw_still_rep;
				if(esp_tempW4>100)
				{
					esp_tempW4=100;
				}
				else
				{
					;
				}
				KYAW.standstill_accuracy=(uint8_t)esp_tempW4-yaw_still_tmp;
			}
			else
			{
				KYAW.standstill_accuracy=70+yaw_still_cum+yaw_still_rep;
			}
		}
		else
		{
			KYAW.standstill_accuracy=40;
		}
	}
	else
	{
		KYAW.standstill_accuracy=0;
	}

	if(KYAW.standstill_accuracy>90)
	{
		KYAW.standstill_accuracy=90;
	}
	else
	{
		;
	}


}

static void LSESP_vCalStrDrvOff(void)
{

		LSESP_vChkControlHold();


	if( (PHZ_detected_flg2==1)&&(KSTEER.SUSPECT_FLG==0)&&(fu1StrErrorDet==0)
#if !__DEL_360_OFFSET
		&&(STR_OFFSET_360_OK==1)
#endif
	)
	{


		/* FailManagement */
		if( (FLAG_INHIBIT_OFST_SPIN==0) && (fu1StrErrorDet==0) )
		{
			LSESP_vCalDrivingSensorOffset(&KSTEER);
			LSESP_vCalStableSteerOffset();
		}
		else
		{
			;
		}

		LSESP_vChkInitDrivSteerOffset();
		LSESP_vChkFinDrivSteerOffset();
	}
	else
	{
		LSESP_vResetDrivingOffset(&KSTEER);
		LSESP_vResetStableSteerOffset();
	}
}

static void LSESP_vCalYawLgDrvOff(void)
{
	if( (fu1YawSenPwr1sOk==1)&& (fu1AySenPwr1sOk==1) && (FLAG_INHIBIT_OFST_TURN==0) )
	{
		/* FailManagement */

		if(fu1YawErrorDet==0)
		{
			LSESP_vCalDrivingSensorOffset(&KYAW);
		}
		else
		{
			;
		}


		/* FailManagement */

		if(fu1AyErrorDet==0)
		{
			LSESP_vCalDrivingSensorOffset(&KLAT);
		}
		else
		{
			;
		}

	}
	else
	{
		;
	}

	/* FailManagement */

	if(fu1YawErrorDet==1)
	{
		LSESP_vResetDrivingOffset(&KYAW);
	}
	else
	{
		;
	}

  /* FailManagement */

	if(fu1AyErrorDet==1)
	{
		LSESP_vResetDrivingOffset(&KLAT);
	}
	else
	{
		;
	}
}

static void LSESP_vCalDrivingSensorOffset(struct OFFSET_STRUCT *PTR)
{

    uint8_t i;

    PTR->DURING_STRAIGHT_FLG=0;
    PTR->OFFSET_10AVR_CAL_OK_FLG=0;

    if( (PHZ_detected_flg2==1)&&(SPEED_OK_FLG==1))
    {
    	tempB5=1;
    }
    else
    {
    	tempB5=0;
    }

    if (FS_YAW_OFFSET_OK_FLG)
    {
        if(McrAbs(yaw_out) > YAW_2DEG)
        {
        	tempB5=0;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    if( (tempB5==1)&&((SM1_OR_2_N_SM3_OR_4==1)||(SM1_N_SM2==1))&&(steer_rate_deg_s2 <= 40)&&(!FLAG_BANK_DETECTED) && (PTR->mdl_ok_num>=2))/* steer_rate_deg_s2=140 =>약 1deg/7ms*/
    {

        PTR->DURING_STRAIGHT_FLG=1;

        if(++(PTR->straight_cnt) > L_U8_TIME_10MSLOOP_100MS)
        {
            PTR-> STRAIGHT_FLAG=1;
            PTR->vdc_sensor_check_cnt++;
        }
        else
        {
        	PTR-> STRAIGHT_FLAG=0;
        }

        if(PTR->vdc_sensor_check_cnt <= L_U8_TIME_10MSLOOP_900MS)
        {

            PTR->sum_model[0] =(int32_t)PTR->sum_model[0]+(int32_t)PTR->model_avr;
            PTR->sum_measured_by_sens =(int32_t)((int32_t)PTR->sum_measured_by_sens+(int16_t)PTR->measured_by_sens);
        }
        else
        {                      /* vdc_sensor_check_cnt=129, straight_cnt=129+14일때,*/

            PTR->mean_offset_at_model = (int16_t)( (int32_t)PTR->sum_model[0] /L_U8_TIME_10MSLOOP_1000MS );

            PTR->avr_measured_by_sens = (int16_t)((int32_t)PTR->sum_measured_by_sens /L_U8_TIME_10MSLOOP_1000MS);
            PTR->model_sens_diff_offset =PTR->avr_measured_by_sens - PTR->mean_offset_at_model;

            if(McrAbs(PTR->mean_offset_at_model) <PTR->STRAIGHT2)
            {
            	PTR->second_straight_flg=1;
                if(PTR->MODEL_OK_AT_15KPH==0)
                {
                    PTR->offset_at_15kph =PTR->model_sens_diff_offset;
                    PTR->MODEL_OK_AT_15KPH=1;
                    PTR->driving_accuracy = PTR->first_driving_accuracy;
                }
                else
                {
                	;
                }
      /****************************** 10회 누적 *************************************************************/
                if(++(PTR->offset_ok_cnt) <= (int8_t)10)
                {
                    PTR->offset_10sum += (int32_t)PTR->model_sens_diff_offset;

                    if(!PTR->FIRST_OFFSET_CAL_FLG)
                    {
                        PTR->offset_nth_sum = PTR->offset_nth_sum + (int32_t)PTR->model_sens_diff_offset;
                        PTR->offset_at_15kph_nth = (int16_t)(PTR->offset_nth_sum/((int32_t)PTR->offset_ok_cnt));
                        if (PTR->offset_ok_cnt>=2)
                        {
                            if ((McrAbs(PTR->offset_at_15kph-PTR->offset_at_15kph_nth))<PTR->eeprom_update_TH)
                            {
                                    PTR->OFFSET_AT_15KPH_NTH_OK=1;
                            }
                            else
                            {
                            	PTR->OFFSET_AT_15KPH_NTH_OK=0;
                            }
                        }
                        else
                        {
                        	;
                        }
                    }
                    else
                    {
                    	;
                    }

                }
                else
                {             /* vdc_sensor_check_cnt=129이고 offset_ok_cnt==11이면*/
                    PTR->offset_10avr_old =PTR->offset_10avr;
                    PTR->offset_10avr = (int16_t)(PTR->offset_10sum/10);

                    if(PTR->FIRST_OFFSET_CAL_FLG==1)
                    {
                    	PTR->NTH_OFFSET_CAL_FLG=1;
                    }
                    else
                    {
                    	PTR->NTH_OFFSET_CAL_FLG=0;
                    }

                    PTR->offset_10sum=0;
                    PTR->offset_ok_cnt=0;
                    PTR->offset_nth_sum=0;
                    PTR->OFFSET_10AVR_CAL_OK_FLG=1;
                    PTR->FIRST_OFFSET_CAL_FLG=1;
/*-------------------------------------------------------------------*/
                    PTR->offset_10_cnt++;
                    if (PTR->offset_10_sec_cnt<100)
                    {
                    	PTR->offset_10_sec_cnt++;
                    }
                    else
                    {
                    	;
                    }
                    PTR->offset_60avr_old=PTR->offset_60avr;

                    if ((PTR->offset_10_cnt==1)&&(!PTR->OFFSET_60AVR_CAL_OK_FLG))
                    {
                        PTR->offset_10avr_first = PTR->offset_10avr;
                    }
                    else
                    {
                    	;
                    }
                    if (McrAbs(PTR->offset_of_eeprom - PTR->offset_10avr_first)< PTR->threshold_of_eeprom)
                    {
                        PTR->OFFST_10AVR_EEPROM=1;
                    }
                    else
                    {
                        PTR->OFFST_10AVR_EEPROM=0;
                    }

                    if (!PTR->OFFSET_60AVR_CAL_OK_FLG)
                    {
                        if (PTR->OFFST_10AVR_EEPROM)
                        {
                            PTR->offset_10avr_arr[0]                  = PTR->offset_of_eeprom;
                            PTR->offset_10avr_arr[PTR->offset_10_cnt] = PTR->offset_10avr;

                            PTR->offset_60sum=0;
                            for (i =0 ; i <= PTR->offset_10_cnt ; i++){
                                PTR->offset_60sum = PTR->offset_60sum + PTR->offset_10avr_arr[i];
                            }
                            PTR->offset_60avr = (int16_t)(PTR->offset_60sum/(PTR->offset_10_cnt+1));
							PTR->driving_accuracy = PTR->first_driving_accuracy + PTR->offset_10_cnt*5;
                            if (PTR->offset_10_cnt>=5)
                            {
                                PTR->OFFSET_60AVR_CAL_OK_FLG=1;
                                PTR->offset_10_cnt=-1;
                                PTR->deviation_driving_sum=0;
                        		for (i = 0 ; i < 6 ; i++)
                        		{
                        			tempW0=PTR->offset_10avr_arr[i]-PTR->offset_60avr;
                        			if(tempW0>0)
                        			{
                        				PTR->deviation_driving_sum = PTR->deviation_driving_sum + tempW0;
                        			}
                        			else
                        			{
                        				PTR->deviation_driving_sum = PTR->deviation_driving_sum - tempW0;
                        			}
                        		}
                        		PTR->deviation_driving = PTR->deviation_driving_sum/6;
                                
                                tempW3= (int16_t)( (((int32_t)PTR->deviation_driving)*10)/PTR->deviation_driving_threshold ) ;
                        		
                        		if(tempW3>60)
                        		{
                        			tempW3=60;
                        		}
                        		else
                        		{
                        			;
                        		}
                        		PTR->deviation_driving_accuracy = 30 - tempW3;
                        		PTR->driving_accuracy = PTR->first_driving_accuracy + 30 + PTR->deviation_driving_accuracy;
                            }
                            else
                            {
                            	;
                            }
                        }
                        else
                        {
                            PTR->offset_10avr_arr[PTR->offset_10_cnt-1] = PTR->offset_10avr;
                            PTR->offset_60sum=0;
                            for (i =0 ; i <= PTR->offset_10_cnt-1 ; i++)
                            {
                                PTR->offset_60sum = PTR->offset_60sum + PTR->offset_10avr_arr[i];
                            }
                            PTR->offset_60avr = (int16_t)(PTR->offset_60sum/PTR->offset_10_cnt);
                            PTR->driving_accuracy = PTR->first_driving_accuracy + PTR->offset_10_cnt*5;
                            if (PTR->offset_10_cnt>=6)
                            {
                                PTR->OFFSET_60AVR_CAL_OK_FLG=1;
                                PTR->offset_10_cnt=-1;

                                PTR->deviation_driving_sum=0;
                        		for (i = 0 ; i < 6 ; i++)
                        		{
                        			tempW0=PTR->offset_10avr_arr[i]-PTR->offset_60avr;
                        			if(tempW0>0)
                        			{
                        				PTR->deviation_driving_sum = PTR->deviation_driving_sum + tempW0;
                        			}
                        			else
                        			{
                        				PTR->deviation_driving_sum = PTR->deviation_driving_sum - tempW0;
                        			}
                        		}
                        		PTR->deviation_driving = PTR->deviation_driving_sum/6;
                                
                                tempW3 = (int16_t)( (((int32_t)PTR->deviation_driving)*10)/PTR->deviation_driving_threshold ) ;
                        		
                        		if(tempW3>60)
                        		{
                        			tempW3=60;
                        		}
                        		else
                        		{
                        			;
                        		}
                        		PTR->deviation_driving_accuracy = 30 - tempW3;
                        		PTR->driving_accuracy = PTR->first_driving_accuracy + 30 + PTR->deviation_driving_accuracy;
                            }
                            else
                            {
                            	;
                            }
                        }
                    }
                    else
                    {      /*if(PTR->OFFSET_60AVR_CAL_OK_FLG)*/
                        if (PTR->offset_10_cnt>=6)
                        {
                        	PTR->offset_10_cnt=0;
                        }
                        else
                        {
                        	;
                        }
                        PTR->offset_10avr_arr[PTR->offset_10_cnt] = PTR->offset_10avr;
                        PTR->offset_60sum=0;
                        for (i = 0 ; i < 6 ; i++)
                        {
                            PTR->offset_60sum = PTR->offset_60sum + PTR->offset_10avr_arr[i];
                        }
                        tempW2 = (int16_t)(PTR->offset_60sum/6);

						if(tempW2>(PTR->offset_60avr_old+PTR->offset_60_avr_limit))
						{
							PTR->offset_60avr=PTR->offset_60avr_old+PTR->offset_60_avr_limit;
						}
						else if(tempW2<(PTR->offset_60avr_old-PTR->offset_60_avr_limit))
						{
							PTR->offset_60avr=PTR->offset_60avr_old-PTR->offset_60_avr_limit;
						}
						else
						{
							PTR->offset_60avr=tempW2;
						}

                        PTR->deviation_driving_sum=0;
                        for (i = 0 ; i < 6 ; i++)
                        {
                        	tempW0=PTR->offset_10avr_arr[i]-PTR->offset_60avr;
                        	if(tempW0>0)
                        	{
                        		PTR->deviation_driving_sum = PTR->deviation_driving_sum + tempW0;
                        	}
                        	else
                        	{
                        		PTR->deviation_driving_sum = PTR->deviation_driving_sum - tempW0;
                        	}
                        }
                        PTR->deviation_driving = PTR->deviation_driving_sum/6;
                        
                        tempW3= (int16_t)( (((int32_t)PTR->deviation_driving)*10)/PTR->deviation_driving_threshold )  ;
                        
                        if(tempW3>60)
                        {
                        	tempW3=60;
                        }
                        else
                        {
                        	;
                        }
                        PTR->deviation_driving_accuracy = 30 - tempW3;
                        PTR->driving_accuracy = PTR->first_driving_accuracy + 30 + PTR->deviation_driving_accuracy;
                    }

                    if(PTR->offset_cal_max < McrAbs(PTR->offset_10avr))
                    {
                        PTR->offset_10avr=0;
                        PTR->OFFSET_10AVR_CAL_OK_FLG=0;
                        PTR->FIRST_OFFSET_CAL_FLG=0;
                        PTR->NTH_OFFSET_CAL_FLG=0;
                        PTR->offset_10_cnt=0;
                        PTR->offset_10_sec_cnt=0;
                        PTR->OFFSET_60AVR_CAL_OK_FLG=0;
                        PTR->OFFST_10AVR_EEPROM=0;
                        PTR->offset_10avr_first=0;
                        PTR->offset_60avr=0;

                    }
                    else
                    {
                    	;
                    }


                }

            }
            else
            {
            	;
            }
			PTR->second_straight_flg=0;
            PTR->vdc_sensor_check_cnt=0;
            PTR->straight_cnt=0;
            PTR->sum_model[0]=PTR->sum_model[1]=PTR->sum_model[2]=PTR->sum_model[3]=PTR->sum_model[4]=0;
            PTR->sum_measured_by_sens=0;
        }
    }
    else
    {
        PTR->sum_model[0]=PTR->sum_model[1]=PTR->sum_model[2]=PTR->sum_model[3]=PTR->sum_model[4]=0;
        PTR->sum_measured_by_sens=0;
        PTR->vdc_sensor_check_cnt=0;
        PTR->straight_cnt=0;
        PTR->DURING_STRAIGHT_FLG=0;
        PTR->STRAIGHT_FLAG=0;
    }

    if ((PTR->OFFSET_AT_15KPH_NTH_OK)&&((PTR->offset_ok_cnt)>=2)&&(!PTR->FIRST_OFFSET_CAL_FLG))
    {
        PTR->offset_at_15kph=LCESP_s16Lpf1Int(PTR->offset_at_15kph_nth,PTR->offset_at_15kph,L_U8FILTER_GAIN_10MSLOOP_5HZ);
    }
    else
    {
    	;
    }

}

static void LSESP_vCalStableSteerOffset(void)
{

	if((vref5 > VREF_10_KPH)&&(steer_rate_deg_s2<=70)&&(control_hold_flg==0)
	    &&(McrAbs(yaw_1_100deg2)<YAW_5DEG)&&(McrAbs(a_lat_1_1000g2)<LAT_0G2G)&&(McrAbs(KYAW.model_avr)<YAW_5DEG)&&(McrAbs(KLAT.model_avr)<LAT_0G2G)
	    &&(McrAbs(KSTEER.model_avr)<WSTR_60_DEG)&&(McrAbs(yaw_1_10deg_ss)<YAW_2DEG)&&(det_beta_dot<YAW_1DEG)
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)	    
	    && FS_YAW_OFFSET_OK_FLG &&(!MPRESS_BRAKE_ON) && (OFST_CONTR==0) &&(OFST_DRAG[0]==0) &&(OFST_DRAG[1]==0)
    #else
	    && FS_YAW_OFFSET_OK_FLG &&(!lsespu1MpresBrkOnByAHBgen3) && (OFST_CONTR==0) &&(OFST_DRAG[0]==0) &&(OFST_DRAG[1]==0)
    #endif
	    #if !__DEL_360_OFFSET
	    &&STR_OFFSET_360_OK
	    #endif
	    )
	{

		if(vdc_stable_cnt>L_U8_TIME_10MSLOOP_1000MS)
		{

			steer_offset_stable_old=steer_offset_stable;

			lsesps16StableSteerOffsetTemp=(int16_t)(steer_offset_stable_sum/L_U8_TIME_10MSLOOP_1000MS);

			if(STEER_STABLE_OK==1)
			{
				steer_offset_stable=LCESP_s16Lpf1Int(lsesps16StableSteerOffsetTemp, steer_offset_stable_old, L_U8FILTER_GAIN_10MSLOOP_3HZ);
			}
			else
			{
				steer_offset_stable=lsesps16StableSteerOffsetTemp;
			}

			KSTEER.driving_accuracy_stable=30;

			vdc_stable_cnt=0;
			steer_offset_stable_sum=0;


			if(steer_stable_check_flg==1)
			{
				lsespu8StableCheckDelayCnt++;
			}
			else
			{
				lsespu8StableCheckDelayCnt=0;
			}

			if(lsespu8StableCheckDelayCnt>2)
			{
				steer_stable_check_flg=0;
			}
			else
			{
				;
			}

			if((STEER_STABLE_OK==1)&&(steer_stable_check_flg==0)&&(McrAbs(steer_offset_stable-steer_offset_stable_old)>WSTR_5_DEG))
			{
				vdc_stable_cnt=0;
				steer_offset_stable_sum=0;
				steer_offset_stable=0;
				KSTEER.driving_accuracy_stable=0;

				steer_stable_check_flg=1;
			}
			else
			{
				;
			}
		}
		else
		{
			vdc_stable_cnt++;
			steer_offset_stable_sum+=(KSTEER.measured_by_sens-KSTEER.ALGO_MODEL[3]);
		}


	}
	else
	{
		vdc_stable_cnt=0;
		steer_offset_stable_sum=0;
	}

	if(FLAG_INHIBIT_OFST_SPIN==1)
	{
		vdc_stable_cnt=0;
		steer_offset_stable_sum=0;

		if(STEER_STABLE_OK==1)
		{
			steer_offset_stable=steer_offset_stable_old;
		}
		else
		{
			steer_offset_stable=0;
			KSTEER.driving_accuracy_stable=0;
		}
	}
	else
	{
		;
	}

	if(KSTEER.driving_accuracy_stable>=30)
	{
		STEER_STABLE_OK=1;
	}
	else
	{
		STEER_STABLE_OK=0;
	}

}

static void LSESP_vCalSensorReferenceModel(struct OFFSET_STRUCT *PTR)/*original name: SENSORs_MODEL_SORTING*/
{
    int8_t i=0;

    PTR->mdl_ok_num=0;

    if(PTR->M1_FRONT_OK==1)
    {
        ARRANGE[i].lavel=1;
        ARRANGE[i++].content = PTR->ALGO_MODEL[1];
    }
    else
    {
    	;
    }
    if(PTR->M2_REAR_OK==1)
    {
        ARRANGE[i].lavel=2;
        ARRANGE[i++].content = PTR->ALGO_MODEL[2];
    }
    else
    {
    	;
    }
    if(PTR->M3_LAT_YAW_STR==1)
    {
        ARRANGE[i].lavel=3;
        ARRANGE[i++].content = PTR->ALGO_MODEL[3];
    }
    else
    {
    	;
    }
    if(PTR->M4_LAT_YAW_STR==1)
    {
        ARRANGE[i].lavel=4;
        ARRANGE[i++].content = PTR->ALGO_MODEL[4];
    }
    else
    {
    	;
    }

    PTR->mdl_ok_num = i;
    i--;


	if(i==3)
	{   /* 4 개 ok*/
	    LSESP_vSortSensorModel(ARRANGE, i);

	    PTR->min = ARRANGE[0].content;
	    PTR->max_3rd = ARRANGE[1].content;
	    PTR->max_2nd = ARRANGE[2].content;
	    PTR->max = ARRANGE[3].content;
	    PTR->model_avr =(PTR->max_2nd + PTR->max_3rd) >>(int8_t)1;
	    PTR->model_diff = (int16_t)(PTR->max_2nd - PTR->max_3rd);
	}
	else if(i==2)
	{     /* 3 개*/
	    LSESP_vSortSensorModel(ARRANGE, i);

	    PTR->max_3rd = ARRANGE[0].content;
	    PTR->max_2nd = ARRANGE[1].content;
	    PTR->max = ARRANGE[2].content;
	    PTR->min=0;
	    if (McrAbs((PTR->max)-(PTR->max_2nd))>McrAbs((PTR->max_2nd)-(PTR->max_3rd)))
	    {
	        PTR->model_avr =(PTR->max_2nd + PTR->max_3rd) >>(int8_t)1;
	        PTR->model_diff = (int16_t)(PTR->max_2nd - PTR->max_3rd);
	    }
	    else
	    {
	        PTR->model_avr =(PTR->max + PTR->max_2nd) >>(int8_t)1;
	        PTR->model_diff = (int16_t)(PTR->max - PTR->max_2nd);
	    }
	}
	else if(i==1)
	{     /* 2 개*/
	    PTR->max_3rd = ARRANGE[0].content;
	    PTR->max_2nd = ARRANGE[1].content;
	    PTR->min =0;
	    PTR->max=0;
	    PTR->model_avr =(PTR->max_2nd + PTR->max_3rd) >>(int8_t)1;
	    PTR->model_diff = (int16_t)(PTR->max_2nd - PTR->max_3rd);
	}
	else if(i==0)
	{     /* 1 개*/
	    PTR->max_2nd = ARRANGE[0].content;
	    PTR->max_3rd = 0;
	    PTR->min =0;
	    PTR->max=0;
	    PTR->model_avr =PTR->max_2nd;
	    PTR->model_diff = (int16_t)PTR->max_2nd;
	}
	else
	{
		PTR->min=0;
		PTR->max_2nd=0;
		PTR->max_3rd=0;
		PTR->max=0;
	    PTR->model_avr =(int16_t)( ((int32_t)PTR->ALGO_MODEL[1]+(int32_t)PTR->ALGO_MODEL[2]+(int32_t)PTR->ALGO_MODEL[3]+(int32_t)PTR->ALGO_MODEL[4])>>(int8_t)2 );
	    PTR->model_diff = 0;
	}

}

static void LSESP_vDctPHZAngle(void)
{
#if __STEER_SENSOR_TYPE==ANALOG_TYPE
    uint8_t F;

	int16_t aver_temp, diff_temp;
    steer_model_aver_lf_old=steer_model_aver_lf;
    steer_offset_360_old=steer_offset_360;

    if(PHZ_detected_flg2==1)
    {

        if(STR_OFFSET_360_OK==0)
        {
		#if WSTR_MAX2 > 7200
            if(steer_1_10deg2 < -(WSTR_MAX2+STEER_360DEG))
            {
                steer_offset_360 = 7200;
                STR_OFFSET_360_OK=1;
            }
            else if(steer_1_10deg2 > (WSTR_MAX2+STEER_360DEG))
            {
                steer_offset_360 = -7200;
                STR_OFFSET_360_OK=1;
            }
            else
            {
            	;
            }
		#else
            if(steer_1_10deg2 < -(WSTR_MAX2))
            {
                steer_offset_360 = STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
            else if(steer_1_10deg2 > (WSTR_MAX2))
            {
                steer_offset_360 = -STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
            else
            {
            	;
            }
		#endif
        }
        else
        {
			;
        }
    }
    else
    {
    	STR_OFFSET_360_OK=0;
    	steer_offset_360=0;
    }

    /* FailManagement */
    if((fu1StrSusDet==0)&&(PHZ_detected_flg2==1)&&(STR_OFFSET_360_OK==0)&&(FLAG_INHIBIT_OFST_SPIN==0))
    {


        steer_model_aver = (int16_t)KSTEER.model_avr;
        steer_model_diff = McrAbs(KSTEER.model_diff);

		if((KSTEER.mdl_ok_num ==4)&&(vref5 >= VREF_15_KPH)&&(BACK_DIR==0))
		{
			STEER_AVR_CAL_OK=1;
		}
		else if((KSTEER.mdl_ok_num ==3)&&(vref5 >= VREF_15_KPH)&&(BACK_DIR==0))
		{
			STEER_AVR_CAL_OK=1;
		}
		else if((KSTEER.mdl_ok_num ==2)&&(vref5 >= VREF_40_KPH)&&(BACK_DIR==0))
		{
			STEER_AVR_CAL_OK=1;
		}
		else if( (KSTEER.mdl_ok_num >=1)&&(vref5 >= VREF_60_KPH) )
        {
        	STEER_AVR_CAL_OK=1;
        }
        else
        {
        	STEER_AVR_CAL_OK=0;
        }


        if((KSTEER.mdl_ok_num >=2)&&(vref5 >= VREF_20_KPH))
        {
            if( ((KSTEER.M1_FRONT_OK)||(KSTEER.M2_REAR_OK))&&((KSTEER.M3_LAT_YAW_STR)||(KSTEER.M4_LAT_YAW_STR)) )
            {
                if((KSTEER.M1_FRONT_OK)||(KSTEER.M2_REAR_OK))
                {
                    if(KSTEER.M1_FRONT_OK)
                    {
                    	tempW1 =KSTEER.ALGO_MODEL[1];
                    }
                    else
                    {
                    	tempW1 =KSTEER.ALGO_MODEL[2];
                    }
                }
                else
                {
                	;
                }

                if((KSTEER.M3_LAT_YAW_STR)||(KSTEER.M4_LAT_YAW_STR))
                {
                    if(KSTEER.M3_LAT_YAW_STR)
                    {
                    	tempW2 =KSTEER.ALGO_MODEL[3];
                    }
                    else
                    {
                    	tempW2 =KSTEER.ALGO_MODEL[4];
                    }
                }
                else
                {
                	;
                }
                aver_temp =(int16_t)(((int32_t)tempW1+tempW2) >>(int8_t)1);
                diff_temp =McrAbs(tempW1-tempW2);
                F=1;
            }
            else
            {
            	F=0;
            }
        }
        else
        {
        	F=0;
        }

    }
    else
    {
        steer_360_chk_timer=0;
        steer_360_chk_timer2=0;
        STEER_AVR_CAL_OK=0;
        F=0;
    }


    if(STEER_AVR_CAL_OK==1)
    {
    #if WSTR_MAX2>7200
        if((steer_1_10deg2+5400 < steer_model_aver_lf)&&(steer_model_diff < STEER_50DEG))
        {
            steer_360_chk_timer2=0;
            steer_360_chk_timer=0;
            if(steer_360_chk_timer3 < (L_U8_TIME_10MSLOOP_750MS))
            {
            	steer_360_chk_timer3++;
            }
            else
            {
                steer_offset_360 = 7200;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2 > steer_model_aver_lf+5400)&&(steer_model_diff < STEER_50DEG))
        {
            steer_360_chk_timer2=0;
            steer_360_chk_timer=0;
            if(steer_360_chk_timer3 >= -(L_U8_TIME_10MSLOOP_750MS))
            {
            	steer_360_chk_timer3--;
            }
            else
            {
                steer_offset_360 = -7200;
                STR_OFFSET_360_OK=1;
            }
        }
    	else if((steer_1_10deg2+STEER_180DEG < steer_model_aver_lf)&&(steer_model_diff < STEER_50DEG))
    #else
        if((steer_1_10deg2+STEER_180DEG < steer_model_aver_lf)&&(steer_model_diff < STEER_50DEG))
    #endif
        {
            steer_360_chk_timer2=0;
            steer_360_chk_timer3=0;
            if(steer_360_chk_timer < (L_U8_TIME_10MSLOOP_750MS))
            {
            	steer_360_chk_timer++;
            }
            else
            {
                steer_offset_360 = STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2 > steer_model_aver_lf+STEER_180DEG)&&(steer_model_diff < STEER_50DEG))
        {
            steer_360_chk_timer2=0;
            steer_360_chk_timer3=0;
            if(steer_360_chk_timer >= -(L_U8_TIME_10MSLOOP_750MS))
            {
            	steer_360_chk_timer--;
            }
            else
            {
                steer_offset_360 = -STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2+STEER_180DEG > steer_model_aver_lf)&&(steer_1_10deg2 -STEER_180DEG < steer_model_aver_lf))
        {
            steer_360_chk_timer=0;
            steer_360_chk_timer3=0;
            if((steer_model_diff < STEER_50DEG)&&((McrAbs(vrad_crt_fl-vrad_crt_fr) <=V_2KPH)||(McrAbs(vrad_crt_rl-vrad_crt_rr) <=V_2KPH)))
            {
                if(steer_360_chk_timer2 < L_U8_TIME_10MSLOOP_1000MS)
                {
                	steer_360_chk_timer2++;
                }
                else
                {
                    steer_offset_360 = 0;
                    STR_OFFSET_360_OK=1;
                    steer_360_chk_timer2=0;
                }
            }
            else
            {
            	steer_360_chk_timer2=0;
            }
        }
        else
        {
            steer_360_chk_timer=0;
            steer_360_chk_timer2=0;
            steer_360_chk_timer3=0;
        }
    }
    else
    {
        steer_360_chk_timer=0;
        steer_360_chk_timer2=0;
        steer_360_chk_timer3=0;
    }



    if((F==1)&&(STEER_AVR_CAL_OK==0))
    {
     #if WSTR_MAX2>7200
        if((steer_1_10deg2+5400 < aver_temp)&&(diff_temp < STEER_50DEG))
        {
            steer_360_chk_timer6=0;
            steer_360_chk_timer7=0;
            if(steer_360_chk_timer8 < L_U16_TIME_10MSLOOP_5S)
            {
            	steer_360_chk_timer8++;
            }
            else
            {
                steer_offset_360 = 7200;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2 > aver_temp+5400)&&(diff_temp < STEER_50DEG))
        {
            steer_360_chk_timer6=0;
            steer_360_chk_timer7=0;
            if(steer_360_chk_timer8 >= -L_U16_TIME_10MSLOOP_5S)
            {
            	steer_360_chk_timer8--;
            }
            else
            {
                steer_offset_360 = -7200;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2+STEER_180DEG < aver_temp)&&(diff_temp < STEER_50DEG))
	#else
        if((steer_1_10deg2+STEER_180DEG < aver_temp)&&(diff_temp < STEER_50DEG))
    #endif
        {
            steer_360_chk_timer6=0;
            steer_360_chk_timer8=0;
            if(steer_360_chk_timer7 < L_U16_TIME_10MSLOOP_5S)
            {
            	steer_360_chk_timer7++;
            }
            else
            {
                steer_offset_360 = STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
        }
        else if((steer_1_10deg2 > aver_temp+STEER_180DEG)&&(diff_temp < STEER_50DEG))
        {
            steer_360_chk_timer6=0;
            steer_360_chk_timer8=0;
            if(steer_360_chk_timer7 >= -L_U16_TIME_10MSLOOP_5S)
            {
            	steer_360_chk_timer7--;
            }
            else
            {
                steer_offset_360 = -STEER_360DEG;
                STR_OFFSET_360_OK=1;
            }
        }
        else if(((steer_1_10deg2+STEER_180DEG) > aver_temp)&&((steer_1_10deg2 -STEER_180DEG) < aver_temp))
        {
            steer_360_chk_timer7=0;
            steer_360_chk_timer8=0;
            if((tempW4 < STEER_50DEG)&&((McrAbs(vrad_crt_fl-vrad_crt_fr) <=V_2KPH)||(McrAbs(vrad_crt_rl-vrad_crt_rr) <=V_2KPH)))

            {
                if(steer_360_chk_timer6 < L_U16_TIME_10MSLOOP_4S)
                {
                	steer_360_chk_timer6++;
                }
                else
                {
                    steer_offset_360 = 0;
                    STR_OFFSET_360_OK=1;
                    steer_360_chk_timer6=0;
                }
            }
            else
            {
            	steer_360_chk_timer6=0;
            }
        }
        else
        {
            steer_360_chk_timer6=0;
            steer_360_chk_timer7=0;
            steer_360_chk_timer8=0;
        }
    }
    else
    {
        steer_360_chk_timer6=0;
        steer_360_chk_timer7=0;
        steer_360_chk_timer8=0;
    }

    steer_model_aver_lf=LCESP_s16Lpf1Int(steer_model_aver,steer_model_aver_lf_old,L_U8FILTER_GAIN_10MSLOOP_3HZ);
#elif __STEER_SENSOR_TYPE==CAN_TYPE
	if(fu1StrErrorDet==1)
	{
		STR_OFFSET_360_OK=0;
		steer_offset_360=0;
	}
	else
	{
		STR_OFFSET_360_OK=1;
		steer_offset_360=0;
	}
#else
	STR_OFFSET_360_OK=1;
	steer_offset_360=0;
#endif


    /* FailManagement */
	if( (PHZ_detected_flg2==0)||(KSTEER.SUSPECT_FLG==1)||(fu1StrErrorDet==1) )
	{
		LSESP_vResetPHZAngle();
	}
	else
	{
		;
	}
}

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)

static void LSESP_vCalPressRawSignal(void)
{

	/* Each Sensor Raw Signal */
	/* Temp */
    PRESS_M.s16premRaw         = pos_mc_1_100bar/10;
    PRESS_M.u1premPower        = fu1McpSenPwr1sOk;    
    PRESS_M.u1prem_PressErrDet = fu1MCPErrorDet;
    PRESS_M.u1prem_PressSusDet = fu1MCPSusDet;    
        
    #if ( __FL_PRESS==ENABLE )
    PRESS_FL.s16premRaw        = pos_fl_1_100bar/10;
    PRESS_FL.u1premPower       = fu1McpSenPwr1sOk;    
    PRESS_FL.u1prem_PressErrDet= fu1FLPErrorDet;
    PRESS_FL.u1prem_PressSusDet= (flp_hw_suspcs_flg | flp_signal_suspcs_flg);    
    #endif    
    
    #if ( __FR_PRESS==ENABLE )
    PRESS_FR.s16premRaw        = pos_fr_1_100bar/10;    
    PRESS_FR.u1premPower       = fu1McpSenPwr1sOk;    
    PRESS_FR.u1prem_PressErrDet= fu1FRPErrorDet;    
    PRESS_FR.u1prem_PressSusDet= (frp_hw_suspcs_flg | frp_signal_suspcs_flg);
    #endif
    
    #if ( __RL_PRESS==ENABLE )        
    PRESS_RL.s16premRaw        = pos_rl_1_100bar/10;   
    PRESS_RL.u1premPower       = fu1McpSenPwr1sOk;    
    PRESS_RL.u1prem_PressErrDet= fu1RLPErrorDet;    
    PRESS_RL.u1prem_PressSusDet= (rlp_hw_suspcs_flg | rlp_signal_suspcs_flg);    
    #endif 
     
    #if ( __RR_PRESS==ENABLE ) 
    PRESS_RR.s16premRaw        = pos_rr_1_100bar/10;    
    PRESS_RR.u1premPower       = fu1McpSenPwr1sOk;
    PRESS_RR.u1prem_PressErrDet= fu1RRPErrorDet;  
    PRESS_RR.u1prem_PressSusDet= (rrp_hw_suspcs_flg | rrp_signal_suspcs_flg);  
    #endif
 
	
}



static void LSESP_vCalMpressOffset(struct PREM_PRESS_OFFSET_STRUCT  *PRESS)
{
	if(PRESS->u1premPower==1)
	{
		if((PRESS->u1premAVR1ok==1)&&(PRESS->u1premAVR2ok==1))
		{
		    tempW6 =McrAbs(PRESS->s16premAVR1 - PRESS->s16premAVR2);
		    PRESS->u1premAVR1ok=0;
		    PRESS->u1premAVR2ok=0;
		    if(tempW6 < MPRESS_1BAR)
		    {
		    	PRESS->s16premDrvOffset =(PRESS->s16premAVR1 + PRESS->s16premAVR2)/2;
		    	PRESS->u1premPressOffsetCheck=1;  
		    }
		    else
		    {
		    	;
		    }

		    if(PRESS->s16premDrvOffset <= MPRESS_15BAR)
		    {
		        if(PRESS->s16premDrvOffset >= MPRESS_3BAR)
		        {
		            if((PRESS->u1prem_NEED_TO_RECHECK_PRESS==0)&&(PRESS->u1prem_MORE_THAN_3BAR_CHECK==0))
		            {
		                PRESS->s16premDrvOffset=0;
		                PRESS->u1prem_NEED_TO_RECHECK_PRESS=1;
		            }
		            else
		            {
		                PRESS->u1prem_MORE_THAN_3BAR_CHECK=1;
		                PRESS->u1prem_NEED_TO_RECHECK_PRESS=0;
		                
		                if(PRESS->u1premPressOffsetCheck==1)
		        	      {
		        		      PRESS->u1prem_PressOffsetOK=1;
		        	      }
		        	      else
		        	      {
		        		      ;
		        	      }    			                
		            }
		        }
		        else
		        {
		        	PRESS->u1prem_MORE_THAN_3BAR_CHECK=0;
		        	if(PRESS->u1premPressOffsetCheck==1)
		        	{
		        		PRESS->u1prem_PressOffsetOK=1;
		        	}
		        	else
		        	{
		        		;
		        	}
		        			        	
		        }
		    }
		    else
		    {
		        PRESS->s16premDrvOffset=0;
		        PRESS->u1prem_NEED_TO_RECHECK_PRESS=0;
		        PRESS->u1prem_MORE_THAN_3BAR_CHECK=0;
		        PRESS->u1premPressOffsetCheck=0;		        
		    }
		}
		else
		{
			PRESS->u1premPressOffsetCheck=0;
		}
		
		/*-- Flag --*/
		lsespu1MpressOffsetOK = PRESS_M.u1prem_PressOffsetOK;
    /* FailManagement */

    /* 2012_SWD_KJS : Adding None Braking Detection Condition */
		if( (PRESS->u1prem_PressErrDet==0)
			&&(fu1BLSSignal==0)&&((BLS==0)
			&&(PRESS->u1prem_PressSusDet==0)&& (fu1BlsSusDet==0 ) && (fu1BLSErrDet==0 ) )
			#if __AHB_SYSTEM==1
			&&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			&&(vref > MP_OFS_VREF_MIN)
			#else 
			&& (vref > VREF_10_KPH)
			#endif
			)
		{
			PRESS->u1prem_BRAKE_PEDAL_OFF=1;
		}
		#if (__BLS_NO_WLAMP_FM==ENABLE)
		else if(   ((fu1BlsSusDet==1) || (fu1BLSErrDet==1))
		         &&( (PRESS->u1prem_PressErrDet==0)&&(PRESS->u1prem_PressSusDet==0) )
			       #if __AHB_SYSTEM==1
			       &&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			       &&(vref > MP_OFS_VREF_MIN)
			       #else 
			       && (vref > VREF_10_KPH)
			       #endif
			       &&( (mtp>=MTP_15_P) && (fu1MainCanSusDet==0) && (fu1MainCanLineErrDet==0) 
		              &&(fu1EMSTimeOutSusDet==0) && (fu1EMSTimeOutErrDet==0) && (lespu1EMS_PV_AV_CAN_ERR==0) )
			)
		{
			PRESS->u1prem_BRAKE_PEDAL_OFF=1;
		}
		#endif
	  else if(   ((fu1BlsSusDet==1)&&(fu1BLSErrDet==0))
		         &&( (PRESS->u1prem_PressErrDet==0)&&(PRESS->u1prem_PressSusDet==0) )
			       #if __AHB_SYSTEM==1
			       &&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			       &&(vref > MP_OFS_VREF_MIN)
			       #else 
			       && (vref > VREF_10_KPH)
			       #endif
			       &&( (mtp>=MTP_15_P) && (fu1MainCanSusDet==0) && (fu1MainCanLineErrDet==0) 
		              &&(fu1EMSTimeOutSusDet==0) && (fu1EMSTimeOutErrDet==0) && (lespu1EMS_PV_AV_CAN_ERR==0) )
		  )
		{
		  PRESS->u1prem_BRAKE_PEDAL_OFF=1;
		}    
    /* 2012_SWD_KJS : Adding None Braking Detection Condition */		
		else
		{
			PRESS->u1prem_BRAKE_PEDAL_OFF=0;
		}

		if(PRESS->u1prem_BRAKE_PEDAL_OFF==1) 
		{
            
            if((ABS_fz==0)&&(YAW_CDC_WORK==0)&&(EBD_RA==0)&&(BTC_fz==0)&& (ACTIVE_BRAKE_ACT==0)
                #if __SCC	
            	&& (lcu8WpcCtrlReq ==0)
            	#endif 
            	)            	             
		    {
		            if(PRESS->s16premPressTimer < (L_U8_TIME_10MSLOOP_500MS*L_U8_TIME_10MSLOOP_20MS))
		            {
		                if(PRESS->u1prem_PRESS_TIMER_DEC_MODE ==0)
		                {
		                    PRESS->s16premPressTimer = PRESS->s16premPressTimer +L_U8_TIME_10MSLOOP_20MS;
		                    PRESS->s16premPressSum =   PRESS->s16premPressSum +  PRESS->s16premRaw;
		                    PRESS->u1prem_PRESS_TIMER_INC_MODE=1;
		                    PRESS->u1prem_PRESS_TIMER_DEC_MODE=0;
		                }
		                else
		                {
		                    PRESS->s16premPressTimer--;
		                    PRESS->s16premPressSum=0;
		                    if(PRESS->s16premPressTimer == 0)
		                    {
		                    	PRESS->u1prem_PRESS_TIMER_DEC_MODE=0;
		                    }
		                    else
		                    {
		                    	;
		                    }
		                }
		            }
		            else if(PRESS->s16premPressTimer == (L_U8_TIME_10MSLOOP_500MS*L_U8_TIME_10MSLOOP_20MS))
		            {
		                PRESS->s16premPressTimer--;
		                PRESS->u1prem_PRESS_TIMER_DEC_MODE=1;
		                PRESS->u1prem_PRESS_TIMER_INC_MODE=0;
		                if(PRESS->u1premAVR1ok==0)
		                {
		                    PRESS->u1premAVR1ok=1;
		                    PRESS->s16premAVR1=(int16_t)(PRESS->s16premPressSum/L_U8_TIME_10MSLOOP_500MS);
		                }
		                else if(PRESS->u1premAVR2ok==0)
		                {
		                    PRESS->u1premAVR2ok=1;
		                    PRESS->s16premAVR2=(int16_t)(PRESS->s16premPressSum/L_U8_TIME_10MSLOOP_500MS);
		                }
		                else
		                {
		                	;
		                }
		            }
		            else
		            {
		            	;
		            }
		    }
		    else
		    {
		        PRESS->s16premPressTimer=0;
		        PRESS->s16premPressSum=0;
		        PRESS->u1prem_PRESS_TIMER_DEC_MODE=0;
		        PRESS->u1prem_PRESS_TIMER_INC_MODE=0;
		        PRESS->u1premAVR1ok=0;
		        PRESS->u1premAVR2ok=0;
		        PRESS->s16premAVR1=0;
		        PRESS->s16premAVR2=0;
		    }
		}
		else
		{
		    PRESS->s16premPressTimer= 0;
		    PRESS->s16premPressSum=0;
		    PRESS->u1prem_PRESS_TIMER_DEC_MODE=0;
		    PRESS->u1prem_PRESS_TIMER_INC_MODE=0;
		    PRESS->u1premAVR1ok=0;
		    PRESS->u1premAVR2ok=0;
		    PRESS->s16premAVR1=0;
		    PRESS->s16premAVR2=0;
		}
	}
	else
	{
		;
	}
}

/* eeprom 옵셋과 현재 check된 주행중 옵셋과 비교 구조 필요함 */

static void LSESP_vDetPressureOffset(struct PREM_PRESS_OFFSET_STRUCT  *PRESS)
{
	if(PRESS->u1prem_PressOffsetOK==1)
	{
		PRESS->s16premPressOfs_f= PRESS->s16premDrvOffset;
	}
	else
	{
		if(PRESS->u1premEEPOfsReadOK==1)
		{
			PRESS->s16premPressOfs_f= PRESS->s16premEEPOfsRead;
		}
		else
		{
			PRESS->s16premPressOfs_f=0;
		}		
	}
}

#else

static void LSESP_vCalMpressOffset(void)
{
	if(fou1SenPwr1secOk==1)
	{

		if((MP_AVR1_OK==1)&&(MP_AVR2_OK==1))
		{
		    tempW6 =McrAbs(mpress_avr1 -mpress_avr2);
		    MP_AVR1_OK=0;
		    MP_AVR2_OK=0;
		    if(tempW6 < MPRESS_1BAR)
		    {
		    	lsesps16MpresDrvOfs =(mpress_avr1 + mpress_avr2)>>1;
		    	lsespu1MpressOffsetCheck=1;  
		    }
		    else
		    {
		    	;
		    }

		    if(lsesps16MpresDrvOfs <= MPRESS_15BAR)
		    {
		        if(lsesps16MpresDrvOfs >= MPRESS_3BAR)
		        {
		            if((NEED_TO_RECHECK_MPRESS==0)&&(MORE_THAN_3BAR_CHECK==0))
		            {
		                lsesps16MpresDrvOfs=0;
		                NEED_TO_RECHECK_MPRESS=1;
		            }
		            else
		            {
		                MORE_THAN_3BAR_CHECK=1;
		                NEED_TO_RECHECK_MPRESS=0;
		                
		                if(lsespu1MpressOffsetCheck==1)
		        	      {
		        		      lsespu1MpressDrvOffsetOK=1;
		        	      }
		        	      else
		        	      {
		        		      ;
		        	      }    			                
		            }
		        }
		        else
		        {
		        	MORE_THAN_3BAR_CHECK=0;
		        	if(lsespu1MpressOffsetCheck==1)
		        	{
		        		lsespu1MpressDrvOffsetOK=1;
		        	}
		        	else
		        	{
		        		;
		        	}
		        			        	
		        }
		    }
		    else
		    {
		        lsesps16MpresDrvOfs=0;
		        NEED_TO_RECHECK_MPRESS=0;
		        MORE_THAN_3BAR_CHECK=0;
		        lsespu1MpressDrvOffsetOK=0;		        
		    }
		}
		else
		{
			lsespu1MpressOffsetCheck=0;
		}

    /* FailManagement */

		if( (fu1BLSSignal==0)
			    #if __BLS_CAN_VALID_CHECK ==1
			    &&(fpu1ECMBLS==0) && (fpu1ECMBLSValid==0)  /*  fpu1ECMBLSValid = 0 valid    */
			    #endif
			&&(BLS==0)
			&&(mp_hw_suspcs_flg==0)&& (fu1MCPSusDet==0) && (fu1MCPErrorDet==0) 
			&&(fu1BlsSusDet==0 ) && (fu1BLSErrDet==0 )
			#if __AHB_SYSTEM==1
			&&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			&&(vref > MP_OFS_VREF_MIN)
			#else 
			&& (vref > VREF_10_KPH)
			#endif
			)
		{
			BRAKE_PEDAL_OFF=1;
		}
	    #if (__BLS_NO_WLAMP_FM==ENABLE)
		else if( ((fu1BlsSusDet==1 ) || (fu1BLSErrDet==1 )) 
		    &&( (mp_hw_suspcs_flg==0)&&(fu1MCPSusDet==0)&&(fu1MCPErrorDet==0))
			#if __AHB_SYSTEM==1
			&&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			&&(vref > MP_OFS_VREF_MIN)
			#else 
			&& (vref > VREF_10_KPH)
			#endif		           
		    && ( (mtp >= MTP_15_P ) && (fu1MainCanSusDet==0) && (fu1MainCanLineErrDet==0) 
		    && (fu1EMSTimeOutSusDet==0 ) && (fu1EMSTimeOutErrDet==0) && (lespu1EMS_PV_AV_CAN_ERR==0) )	  
		 )
		{
		    BRAKE_PEDAL_OFF=1;      
		}  
		#endif
    /* 2012_SWD_KJS : Adding None Braking Detection Condition */
	  else if(   ((fu1BlsSusDet==1)&&(fu1BLSErrDet==0))
		         &&( (mp_hw_suspcs_flg==0)&&(fu1MCPSusDet==0)&&(fu1MCPErrorDet==0) )
			       #if __AHB_SYSTEM==1
			       &&( ((fu1AHBInv_Main == 0) && (fu2AhbAct_Main==0)) || ((fu1AHBInv_Sub == 0) && (fu2AhbAct_Sub == 0))  )
			       &&(vref > MP_OFS_VREF_MIN)
			       #else 
			       && (vref > VREF_10_KPH)
			       #endif
			       &&( (mtp>=MTP_15_P) && (fu1MainCanSusDet==0) && (fu1MainCanLineErrDet==0) 
		              &&(fu1EMSTimeOutSusDet==0) && (fu1EMSTimeOutErrDet==0) && (lespu1EMS_PV_AV_CAN_ERR==0) )
		  )
		{
		  BRAKE_PEDAL_OFF=1;
		}		
    /* 2012_SWD_KJS : Adding None Braking Detection Condition */		
		else
		{
			BRAKE_PEDAL_OFF=0;
		}

		if(BRAKE_PEDAL_OFF==1)
		{
			#if __HBB
		    if((ABS_fz==0)&&(YAW_CDC_WORK==0)&&(EBD_RA==0)&&(BTC_fz==0)&& (ACTIVE_BRAKE_ACT==0) && (HBB_ON == 0) )
		    #else
		    if((ABS_fz==0)&&(YAW_CDC_WORK==0)&&(EBD_RA==0)&&(BTC_fz==0)&& (ACTIVE_BRAKE_ACT==0))
		    #endif
		    {
		            if(mpress_timer < (L_U8_TIME_10MSLOOP_500MS*L_U8_TIME_10MSLOOP_20MS))
		            {
		                if(MP_TIMER_DEC_MODE ==0)
		                {
		                    mpress_timer = mpress_timer+L_U8_TIME_10MSLOOP_20MS;
		                    mpress_sum = mpress_sum + raw_mpress;
		                    MP_TIMER_INC_MODE=1;
		                    MP_TIMER_DEC_MODE=0;
		                }
		                else
		                {
		                    mpress_timer--;
		                    mpress_sum=0;
		                    if(mpress_timer == 0)
		                    {
		                    	MP_TIMER_DEC_MODE=0;
		                    }
		                    else
		                    {
		                    	;
		                    }
		                }
		            }
		            else if(mpress_timer == (L_U8_TIME_10MSLOOP_500MS*L_U8_TIME_10MSLOOP_20MS))
		            {
		                mpress_timer--;
		                MP_TIMER_DEC_MODE=1;
		                MP_TIMER_INC_MODE=0;
		                if(MP_AVR1_OK==0)
		                {
		                    MP_AVR1_OK=1;
		                    mpress_avr1=(int16_t)(mpress_sum/L_U8_TIME_10MSLOOP_500MS);
		                }
		                else if(MP_AVR2_OK==0)
		                {
		                    MP_AVR2_OK=1;
		                    mpress_avr2=(int16_t)(mpress_sum/L_U8_TIME_10MSLOOP_500MS);
		                }
		                else
		                {
		                	;
		                }
		            }
		            else
		            {
		            	;
		            }
		    }
		    else
		    {
		        mpress_timer=0;
		        mpress_sum=0;
		        MP_TIMER_DEC_MODE=0;
		        MP_TIMER_INC_MODE=0;
		        MP_AVR1_OK=0;
		        MP_AVR2_OK=0;
		        mpress_avr1=0;
		        mpress_avr2=0;
		    }
		}
		else
		{
		    mpress_timer= 0;
		    mpress_sum=0;
		    MP_TIMER_DEC_MODE=0;
		    MP_TIMER_INC_MODE=0;
		    MP_AVR1_OK=0;
		    MP_AVR2_OK=0;
		    mpress_avr1=0;
		    mpress_avr2=0;
		}
	}
	else
	{
		;
	}

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
    #if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
	
	if(ESP_PDF.lsespu1ESPEepOfsCalOkFlg ==1 )
	{
        if( ESP_MP.lsespu1ESPEepOfsCalOkFlg ==1 )
        {
        	if(lsespu1MpressDrvOffsetOK ==1 )
        	{
        	    lsesps16MpresOfsFinal = lsesps16MpresDrvOfs ;  
        	    lsespu1MpressOffsetOK = 1 ;
        	}
        	else
        	{
        		lsesps16MpresOfsFinal =ESP_MP.lsesps16ESPofsFirstCal;
        		lsespu1MpressOffsetOK = 1 ;
        	}
        }
        else
        {
        	lsespu1MpressOffsetOK = 0 ;
        }
    }
    else
    {
    	if(lsespu1MpressDrvOffsetOK ==1 )
    	{
    	    lsesps16MpresOfsFinal = lsesps16MpresDrvOfs ;  
    	    lsespu1MpressOffsetOK = 1 ;
    	}
    	else
    	{
    		if(lsespu1MpresDrvEEPOfsRead ==1 )
    		{
    			lsesps16MpresOfsFinal = lsesps16MpresDrvEEPOfs;
    			lsespu1MpressOffsetOK = 1 ;    			
    		}
    		else
    		{
    			if(lsespu1MpresEolFirstOfsRead ==1 )
    			{
    				lsesps16MpresOfsFinal = lsesps16MpresEolFirstOfs ;
    				lsespu1MpressOffsetOK = 1 ;  
    			}
    			else
    			{
    				lsesps16MpresOfsFinal = 0 ;
    				lsespu1MpressOffsetOK = 0 ; 
    			}
    		}
    		
    	}
    	
    }	

	mpress_offset = lsesps16MpresOfsFinal ;

	#else

   	if(lsespu1MpressDrvOffsetOK ==1)
    {
        lsesps16MpresOfsFinal = lsesps16MpresDrvOfs;  
    	lsespu1MpressOffsetOK = 1;
    }
    else
    {
        if(lsespu1MpresDrvEEPOfsRead==1)
    	{
    		lsesps16MpresOfsFinal = lsesps16MpresDrvEEPOfs;
    		lsespu1MpressOffsetOK = 1;    			
    	}
    	else
    	{
    		lsesps16MpresOfsFinal = 0;
    	  lsespu1MpressOffsetOK = 0; 
    	}
    }

	mpress_offset = lsesps16MpresOfsFinal;
			
	#endif
#endif

}

#endif

#if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
static void  LSESP_vCalBrkAppSenOffset(void)
{
	
    LSESP_vCalcEEPSensorOffset();
    LSESP_vWritEEPSensorOffset();
    LSESP_vCalPedalDrvOffset(); 
	
}

void LSESP_vCalcEEPSensorOffset(void)
{
	/* Start Flag Temp */

    /* C710_DH */
    /*	
    ESP_PDF.lsespu1EEPofsCalcFWStartFlg    = ccu1CalPdlReqFlg;	
    ESP_PDT.lsespu1EEPofsCalcFWStartFlg    = ccu1CalPdlReqFlg;
    ESP_MP.lsespu1EEPofsCalcFWStartFlg     = ccu1CalPdlReqFlg;  	
	*/

    ESP_PDF.lsespu1EEPofsCalcFWStartFlg    = 0;	
    ESP_PDT.lsespu1EEPofsCalcFWStartFlg    = 0;
    ESP_MP.lsespu1EEPofsCalcFWStartFlg     = 0;

    /*------------- EEPROM Enter Condition ------------- */
    if( ((BLS ==0) && (fu1BLSErrDet==0 ) ) ||  ( fu1BLSErrDet==1 ) )
    {
    	lsespu1ESPeepEnterOK =1 ;
    }
    else
    {
    	lsespu1ESPeepEnterOK =0 ;
    }
    
    /*------------- EEPROM Calc Loop Check  ------------- */
    if(lsespu1ESPeepCalcLoopOK ==1 )
    {
    	if( (ESP_PDF.lsespu1ESPEepOfsCalEndFlg==1)&&(ESP_PDT.lsespu1ESPEepOfsCalEndFlg==1) && (ESP_MP.lsespu1ESPEepOfsCalEndFlg==1) )
    	{
    		lsespu1ESPeepCalcLoopOK = 0;
    	}
    	else
    	{
    		;
    	}
    }
    else
    {
	    if(     (ESP_PDF.lsespu1EEPofsCalcFWStartFlg==1)
	    	 && (lsespu1ESPeepEnterOK ==1 )  )
	    {
	    	lsespu1ESPeepCalcLoopOK=1;
	    	
	    	ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDF;   
	    	LSESP_vEepromOfsCalcReset();
	    	ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDT;   
	    	LSESP_vEepromOfsCalcReset();	    	
	    	ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_MP;   
	    	LSESP_vEepromOfsCalcReset();	  
	    	
	    	LSESP_vDetPedalMPDrvEepOfsReset();  	
	    }
	    else
	    {
	    	;
	    }
   	    	
    }
    
  
    /*========= EEPROM During Calc Loop Main Logic   ======== */
    if(lsespu1ESPeepCalcLoopOK==1)
    {
        /*---------= EEPROM During Calc Condition  ----------- */
        
        if( (BLS ==1) && (fu1BLSErrDet==0 ) )
        {
        	lsespu1ESPeepWhileOK = 0;
        }
        else
        {
        	lsespu1ESPeepWhileOK =1  ;
        }    
        
            
        ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDF;   
        if(ESP_PDF.lsespu1ESPEepOfsCalEndFlg == 0 )
        {
            LSESP_vDetEachEolEEPSensorOffset(PDF.lsesps16PedalSenRaw , PDF_OFS_LM_THR , PDF_OFS_MAX_MIN_THR  );
        }
        else
        {
        	;
        }
        
        ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDT;     
        if(ESP_PDT.lsespu1ESPEepOfsCalEndFlg == 0 )
        {
            LSESP_vDetEachEolEEPSensorOffset(PDT.lsesps16PedalSenRaw , PDT_OFS_LM_THR , PDT_OFS_MAX_MIN_THR  );        
        }
        else
        {
        	;
        }
             
        ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_MP;     
        if(ESP_MP.lsespu1ESPEepOfsCalEndFlg == 0)
        {
            LSESP_vDetEachEolEEPSensorOffset(raw_mpress , PRES_OFS_LM_THR , PRES_OFS_MAX_MIN_THR  );  
        }
        else
        {
        	;
        }
        
                     
        lsespu1AHBsenEOLEEPofsCalcEND = ESP_PDF.lsespu1ESPEepOfsCalEndFlg;
        
          
        if((ESP_PDF.lsespu1ESPEepOfsCalOkFlg==1) )
        {
        	PDF.lsespu1PedalOfsInvalid = 0;
        }
        else
        {
        	PDF.lsespu1PedalOfsInvalid = 1;
        } 
        
        if((ESP_PDT.lsespu1ESPEepOfsCalOkFlg==1) )
        {
        	PDT.lsespu1PedalOfsInvalid = 0;
        }
        else
        {
        	PDT.lsespu1PedalOfsInvalid = 1;
        }         
                          
        
    }
    else
    {
    	;
    }          
}

void LSESP_vDetEachEolEEPSensorOffset(int16_t X, int16_t Y, int16_t Z)
{
	

	if(ESP_EEP_CALC->lsespu8ESPEEPofsMaxMinLMOverCnt <= 0 )
	{
		LSESP_vCalcEachEEPSensorOffset(X, Y, Z);
	}
	else
	{  
		if(ESP_EEP_CALC->lsespu1EEPOfsEepReCalcOK==1)
		{
			LSESP_vCalcEachEEPSensorOffset(X, Y, Z);
		}
		else
		{
			LSESP_vDetEOLeepOfsDelayTime();
		}
	}      			
    	
}

void LSESP_vWritEEPSensorOffset(void)
{

    ESP_PDF.lsespu1ESPEepOfsWriteWhile =   wbu1PDFOfsEepWriteReq;
    ESP_PDT.lsespu1ESPEepOfsWriteWhile =   wbu1PDTOfsEepWriteReq ;
    ESP_MP.lsespu1ESPEepOfsWriteWhile   =  wbu1MPOfsEepWriteReq; 

     	
    LSESP_vWritEEPReqSensorOfs( &ESP_PDF );
    LSESP_vWritEEPReqSensorOfs( &ESP_PDT );
    LSESP_vWritEEPReqSensorOfs( &ESP_MP );                 		

/*
    if( ESP_PDF.lsespu1ESPEepOfsWriteReqOK==1 )
    {
        lsespu1AHBsenEOLEEPofsCalcEND = 1;	
    }
    else
    {
        lsespu1AHBsenEOLEEPofsCalcEND = 0;	
    }         
*/

}

void    LSESP_vWritEEPReqSensorOfs(struct ESP_EEP_offsetCalc  *ESP_EEP_CALC)
{
    if(ESP_EEP_CALC->lsespu1ESPEepOfsWriteReq ==1 )
    {
    	if(ESP_EEP_CALC->lsespu1ESPEepOfsWriteWhile ==0 )
    	{
    		ESP_EEP_CALC->lsespu1ESPEepOfsWriteReq = 0;    		
            ESP_EEP_CALC->lsespu1ESPEepOfsWriteReqOK =1;           		
    	}
    	else
    	{
    		; 
    	}
    }
    else
    {
        if( (ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg ==1) && (ESP_EEP_CALC->lsespu1ESPEepOfsWriteReqOK ==0 ))  /*start flag*/
        {        
              ESP_EEP_CALC->lsespu1ESPEepOfsWriteReq = 1;         
        }
        else
        {
           ;
        }				    	
    } 			
}

void LSESP_vCalcEachEEPSensorOffset(int16_t SensorRawSig , int16_t OFS_THR, int16_t OFS_DIFF_THR)
{
	int16_t  lsesps16EEPOfsCalcMaxMinTemp;
	
	if((ESP_EEP_CALC->lsespu1EEPofsCalcFWStartFlg==1) && (lsespu1ESPeepWhileOK==1)  && (ESP_EEP_CALC->lsespu1fsFailInvalid==0))  /* BLS Fail 조건 고려할 것! */
	{
	      		
		if(ESP_EEP_CALC->lsespu8ESPofsFirstCnt < EEP_CALC_T)
		{
			if(ESP_EEP_CALC->lsespu8ESPofsFirstCnt==0)
			{
				ESP_EEP_CALC->lsesps32ESPOfsFirstSum = 0 ;
				ESP_EEP_CALC->lsesps16ESPofsFirstCalMax = SensorRawSig;
				ESP_EEP_CALC->lsesps16ESPofsFirstCalMin = SensorRawSig;
				ESP_EEP_CALC->lsesps32ESPOfsFirstSum =ESP_EEP_CALC->lsesps32ESPOfsFirstSum  + SensorRawSig;									
			}
			else
			{
				ESP_EEP_CALC->lsesps32ESPOfsFirstSum=ESP_EEP_CALC->lsesps32ESPOfsFirstSum + SensorRawSig;
				if( ESP_EEP_CALC->lsesps16ESPofsFirstCalMax < SensorRawSig  )
				{
					ESP_EEP_CALC->lsesps16ESPofsFirstCalMax= SensorRawSig;
				}
				else if(ESP_EEP_CALC->lsesps16ESPofsFirstCalMin > SensorRawSig )
		{
					ESP_EEP_CALC->lsesps16ESPofsFirstCalMin= SensorRawSig;
		}
		else
		{
			;
		}
		
			}	 	
        
			ESP_EEP_CALC->lsespu8ESPofsFirstCnt=ESP_EEP_CALC->lsespu8ESPofsFirstCnt+1;	
			
		}
		else    
		{
			ESP_EEP_CALC->lsesps16ESPofsFirstCal = (int16_t)(ESP_EEP_CALC->lsesps32ESPOfsFirstSum/(EEP_CALC_T) );
			lsesps16EEPOfsCalcMaxMinTemp =  ESP_EEP_CALC->lsesps16ESPofsFirstCalMax-ESP_EEP_CALC->lsesps16ESPofsFirstCalMin ;		
			ESP_EEP_CALC->lsespu8ESPEEPofsMaxMinLMOverCnt=ESP_EEP_CALC->lsespu8ESPEEPofsMaxMinLMOverCnt+1;							 
        
			if(  McrAbs(ESP_EEP_CALC->lsesps16ESPofsFirstCal)<= OFS_THR  )
		{
				ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK=1;
			}
			else
			{
				ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK=0;
			}	 
			
			if(  lsesps16EEPOfsCalcMaxMinTemp <= OFS_DIFF_THR  )
			{
				ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK=1;
			}
			else
			{
				ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK=0;
			}	 		
			
            if( (ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK==1) && (ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK==1) )
            {
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=1;
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg =1;                	
            }
            else if( (ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK==1) && (ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK==0) )
            {
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=0;
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg =0;                     	
            }
            else if( (ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK==0) && (ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK==1) )
			{
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=1;
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg =0;                          	
            }
            else
				{
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=1;
            	ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg =0;   
            }					
    								
    		if(ESP_EEP_CALC->lsespu8ESPEEPofsMaxMinLMOverCnt>= (EEP_RETRY+1) )
    		{
    			ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=1;
    		}
    		else
    		{
  			    ;
    		}	
    																	
			ESP_EEP_CALC->lsespu8ESPofsFirstCnt= 0;					
	        ESP_EEP_CALC->lsespu8EEPOfsDelyCnt=0;
	        ESP_EEP_CALC->lsespu1EEPOfsEepReCalcOK=0;  				
		
		}		
						
				}
				else
				{
        ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=1;    	
	}			
	
}

void LSESP_vDetEOLeepOfsDelayTime(void)
{
	ESP_EEP_CALC->lsespu8EEPOfsDelyCnt=ESP_EEP_CALC->lsespu8EEPOfsDelyCnt +1 ;
					
	if(ESP_EEP_CALC->lsespu8EEPOfsDelyCnt > EEP_DELAY_T )
					{
		ESP_EEP_CALC->lsespu1EEPOfsEepReCalcOK =1 ;
					}
					else
					{
						;
					}					
				}				

void LSESP_vEepromOfsCalcReset(void)
{
	ESP_EEP_CALC->lsespu8ESPEEPofsMaxMinLMOverCnt=0;
	ESP_EEP_CALC->lsespu8ESPofsFirstCnt= 0;	
    ESP_EEP_CALC->lsespu1EEPofsEEPLimitOK=0;
	ESP_EEP_CALC->lsespu1EEPofsEEPMaxMinOK=0;
	ESP_EEP_CALC->lsespu1ESPEepOfsCalEndFlg=0;
	ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg=0;		
	
	ESP_EEP_CALC->lsespu8EEPOfsDelyCnt=0;
	ESP_EEP_CALC->lsespu1EEPOfsEepReCalcOK=0;	
	ESP_EEP_CALC->lsespu1ESPEepOfsWriteReqOK =0;	
	
}

void LSESP_vDetPedalMPDrvEepOfsReset(void)
{ 
	/* PDF */
    PDF.lsespu8PedalDrvOfsCnt             =  0;
    PDF.lsesps16PedalDrvOfsMean           =  0;
    PDF.lsesps32PedalDrvOfsSum            =  0;
    PDF.lsespu1PedalDrvOfsFirst           =  0;
    PDF.lsesps16PedalDrvOfsArray[0]       =  0;
    PDF.lsesps16PedalDrvOfsArray[1]       =  0;
    PDF.lsesps16PedalDrvOfsArray[2]       =  0;
    PDF.lsesps16PedalDrvOfsArray[3]       =  0;
    PDF.lsesps16PedalDrvOfsArray[4]       =  0;
    PDF.lsesps16PedalDrvOfsArray[5]       =  0;
    PDF.lsesps16PedalDrvOfsArray[6]       =  0;
    PDF.lsesps16PedalDrvOfsArray[7]       =  0;
    PDF.lsesps16PedalDrvOfsArray[8]       =  0;
    PDF.lsesps16PedalDrvOfsArray[9]       =  0;
    PDF.lsesps16PedalDrvOfsArrayMax       =  0;
    PDF.lsesps16PedalDrvOfsArrayMin       =  0;
    PDF.lsesps32PedalDrvOfsArraySum       =  0;
    PDF.lsespu8PedalDrvOfsArrayCnt        =  0;
    PDF.lsespu1PedalDrv1secOfsOK          =  0;
    PDF.lsesps16PedalDrvOfsArrayMean      =  0;
    PDF.lsesps16PedalDrvOfs_f             =  0;
    PDF.lsesps16PedalDrv10secMean         =  0;
    PDF.lsespu1PedalDrv10secOfsOK         =  0;
    PDF.lsespu8PedalDrvOfs10sArrayCnt     =  0;
    PDF.lsesps32PedalDrvOfs10sArraySum    =  0;
    PDF.lsesps16PedalDrvOfs60sMean        =  0;
    PDF.lsesps16PedalDrvOfs60sMeanOld     =  0;
    PDF.lsespu1PedalDrv60secOfsOK         =  0;
    PDF.lsesps16PedalDrvOfsArrayFilter    =  0;
    PDF.lsesps16PedalDrvOfsArrayFConv     =  0;
    PDF.lsespu1PedalDrv60secOfsReOK       =  0;
	
	
	/* PDT */
    PDT.lsespu8PedalDrvOfsCnt             =  0;
    PDT.lsesps16PedalDrvOfsMean           =  0;
    PDT.lsesps32PedalDrvOfsSum            =  0;
    PDT.lsespu1PedalDrvOfsFirst           =  0;
    PDT.lsesps16PedalDrvOfsArray[0]       =  0;
    PDT.lsesps16PedalDrvOfsArray[1]       =  0;
    PDT.lsesps16PedalDrvOfsArray[2]       =  0;
    PDT.lsesps16PedalDrvOfsArray[3]       =  0;
    PDT.lsesps16PedalDrvOfsArray[4]       =  0;
    PDT.lsesps16PedalDrvOfsArray[5]       =  0;
    PDT.lsesps16PedalDrvOfsArray[6]       =  0;
    PDT.lsesps16PedalDrvOfsArray[7]       =  0;
    PDT.lsesps16PedalDrvOfsArray[8]       =  0;
    PDT.lsesps16PedalDrvOfsArray[9]       =  0;
    PDT.lsesps16PedalDrvOfsArrayMax       =  0;
    PDT.lsesps16PedalDrvOfsArrayMin       =  0;
    PDT.lsesps32PedalDrvOfsArraySum       =  0;
    PDT.lsespu8PedalDrvOfsArrayCnt        =  0;
    PDT.lsespu1PedalDrv1secOfsOK          =  0;
    PDT.lsesps16PedalDrvOfsArrayMean      =  0;
    PDT.lsesps16PedalDrvOfs_f             =  0;
    PDT.lsesps16PedalDrv10secMean         =  0;
    PDT.lsespu1PedalDrv10secOfsOK         =  0;
    PDT.lsespu8PedalDrvOfs10sArrayCnt     =  0;
    PDT.lsesps32PedalDrvOfs10sArraySum    =  0;
    PDT.lsesps16PedalDrvOfs60sMean        =  0;
    PDT.lsesps16PedalDrvOfs60sMeanOld     =  0;
    PDT.lsespu1PedalDrv60secOfsOK         =  0;
    PDT.lsesps16PedalDrvOfsArrayFilter    =  0;
    PDT.lsesps16PedalDrvOfsArrayFConv     =  0;
    PDT.lsespu1PedalDrv60secOfsReOK       =  0;	

    PDT.lsespu1PedalOfsInvalid =1;
    PDF.lsespu1PedalOfsInvalid =1;
	
	lsespu1MpressDrvOffsetOK   =0;
	lsesps16MpresDrvOfs        =0;
	
    	
}

void LSESP_vReadEEPSensorOffset(void)
{
	;
}

/*--- Driving offset ---*/
/* --1) Limitation 추가할 것 Min/Max -OK
     2) Pedal travel stable 조건 추가 -ok
     3) EEPROM 보정 추가할 것         -eol시 주행옵셋 reset하는것
     4) IGN OFF-ON EEPROM Write 추가할 것 - FW 추후반영  
     
     ---*/
     
static void  LSESP_vCalPedalDrvOffset(void)
{
    #if (__USE_PDF_SIGNAL==ENABLE)
    PEDAL_CALC = (struct PEDAL_offset  *)&PDF;   
    LSESP_vCalEachPedalDrvOffset();
    #endif

    #if (__USE_PDT_SIGNAL==ENABLE)    
    PEDAL_CALC = (struct PEDAL_offset *)&PDT;     
    LSESP_vCalEachPedalDrvOffset();	
    #endif

}

static void  LSESP_vCalEachPedalDrvOffset(void)
{

    LSESP_vDctPedalOffCondition();
    LSESP_vCalPedalDrvQuickOffset();
	
}

static void  LSESP_vDctPedalOffCondition(void)
{

	/*-- Signal is within limitation --*/	
	int16_t lsesps16DiffSensorRawTemp;
	
	/*-- 250msec Signal Calculation --*/		        	       
	if(PEDAL_CALC->lsespu8PdlNormCondCnt > L_U8_TIME_10MSLOOP_250MS)
	{		
		PEDAL_CALC->lsespu8PdlNormCondCnt      = 0;
		PEDAL_CALC->lsesps16PdlNormCondMeanOld = PEDAL_CALC->lsesps16PdlNormCondMean;				
		PEDAL_CALC->lsesps16PdlNormCondMean    = (int16_t)(PEDAL_CALC->lsesps32PdlNormCondSum/L_U8_TIME_10MSLOOP_250MS) ;
		PEDAL_CALC->lsesps32PdlNormCondSum     = 0;	

	    PEDAL_CALC->lsespu8PdlDrvModeDeltTCnt = PEDAL_CALC->lsespu8PdlDrvModeDeltTCnt + 1;
	    PEDAL_CALC->lsespu8PdlDrvModeDeltTCnt = (uint8_t)(LCABS_s16LimitMinMax( ((int16_t)PEDAL_CALC->lsespu8PdlDrvModeDeltTCnt) , 0, 200) );		
		
		if(PEDAL_CALC->lsespu1NormCondFirstCheck==0)
		{
		    PEDAL_CALC->lsespu1NormCondFirstCheck   = 1;	
		    PEDAL_CALC->lsesps16PdlNormCondMeanDiff = 0;	 
		    
		    PEDAL_CALC->lsesps16PdlDrvMin  =  PEDAL_CALC->lsesps16PdlNormCondMean ;  			    
			}
			else
			{
			PEDAL_CALC->lsesps16PdlNormCondMeanDiff =  PEDAL_CALC->lsesps16PdlNormCondMean - PEDAL_CALC->lsesps16PdlNormCondMeanOld ;
			
			/* Drv Min value Calculation */
			if(PEDAL_CALC->lsesps16PdlNormCondMean < PEDAL_CALC->lsesps16PdlDrvMin  )
				{
			    PEDAL_CALC->lsesps16PdlDrvMin = PEDAL_CALC->lsesps16PdlNormCondMean ;			
				}
				else
				{
					;
				}
			}
		}
		else
		{
		PEDAL_CALC->lsespu8PdlNormCondCnt =  PEDAL_CALC->lsespu8PdlNormCondCnt+1 ;
		PEDAL_CALC->lsesps32PdlNormCondSum = PEDAL_CALC->lsesps32PdlNormCondSum + PEDAL_CALC->lsesps16PedalSenRaw ;		
		}


   	lsesps16DiffSensorRawTemp = PEDAL_CALC->lsesps16PedalSenRaw - PEDAL_CALC->lsesps16PdlNormCondMean;
    	
    /* -- 250msec Mean Diff , Stable Decision ---*/
  	if( ( (PEDAL_CALC->lsespu8PdlDrvModeDeltTCnt) >=2)  &&
   		(McrAbs(PEDAL_CALC->lsesps16PdlNormCondMeanDiff) < PEDAL_STB_THR )&& (McrAbs(lsesps16DiffSensorRawTemp) < PEDAL_STB_THR )  )
   	{
   		PEDAL_CALC->lsespu1PedalNormCond = 1;
	}
	else
	{
   		PEDAL_CALC->lsespu1PedalNormCond = 0;
	}
	
	
	//PEDAL_CALC->lsespu1PedalNormCond = 1 ;
		
	/*===============================================================================*/
	

	
	/* Driving Offset Calc condition */

    /* PDF condition */
	if( (BLS==0) &&(fu1BlsSusDet==0 ) && (fu1BLSErrDet==0 ) &&                                  /* 1. BLS==0          */
		    #if __BLS_CAN_VALID_CHECK ==1
		    (fpu1ECMBLS==0) && (fpu1ECMBLSValid==0)&&                                           /*  fpu1ECMBLSValid = 0 valid    */
		    #endif
		(PEDAL_CALC->lsespu1PedalNormCond ==1)     &&                                           /* 2. PDT Stable      */
		(vref > VREF_10_KPH)                       &&                                           /* 4. V > 10kph       */
		(mtp >  MTP_20_P )                         && (ldabsu1CanErrFlg==0) &&                  /* 5. Accel Pedal > ***, Error 조건 추가필요 */
                                                                                                /* 6. PDT Err/Sus==0  */ 
		(PEDAL_CALC->lsesps16PedalSenRaw < PEDAL_DRV_CORRECT_THR)     
			)
		{
		PEDAL_CALC->lsespu1PedalDrvOfsCheckCond = 1;
		
		}
		else
		{
		PEDAL_CALC->lsespu1PedalDrvOfsCheckCond = 0;		
		}	
		
}

static void  LSESP_vCalPedalDrvQuickOffset(void)
{	
    /*
	if( PEDAL_CALC->lsespu1PedalDrvOfsCheckCond ==1 )
	{
		if(PEDAL_CALC->lsespu8PedalDrvOfsCnt >= L_U8_TIME_10MSLOOP_1000MS )
		{
			PEDAL_CALC->lsespu8PedalDrvOfsCnt   = 0;
			PEDAL_CALC->lsesps16PedalDrvOfsMean = (PEDAL_CALC->lsesps32PedalDrvOfsSum/ L_U8_TIME_10MSLOOP_1000MS);
			PEDAL_CALC->lsesps32PedalDrvOfsSum  = 0;
			
			if(PEDAL_CALC->lsespu1PedalDrvOfsFirst ==0 )
			{
				PEDAL_CALC->lsesps16PedalDrvOfs_f = PEDAL_CALC->lsesps16PedalDrvOfsMean ;
				PEDAL_CALC->lsespu1PedalDrvOfsFirst =1 ;
			}
			else
			{				
				PEDAL_CALC->lsesps16PedalDrvOfs_f  = LCESP_s16Lpf1Int( PEDAL_CALC->lsesps16PedalDrvOfsMean , PEDAL_CALC->lsesps16PedalDrvOfs_f , L_U8FILTER_GAIN_10MSLOOP_5HZ  );  
			}						
		}
		else
		{
			PEDAL_CALC->lsespu8PedalDrvOfsCnt  = PEDAL_CALC->lsespu8PedalDrvOfsCnt+1;
			PEDAL_CALC->lsesps32PedalDrvOfsSum = PEDAL_CALC->lsesps32PedalDrvOfsSum + PEDAL_CALC->lsesps16PedalSenRaw ;
		}
	}
	else
	{
		PEDAL_CALC->lsespu8PedalDrvOfsCnt   = 0;
		PEDAL_CALC->lsesps32PedalDrvOfsSum  = 0;
	}				
	
	*/
	
	if( PEDAL_CALC->lsespu1PedalDrvOfsCheckCond ==1 )
            {
		if(PEDAL_CALC->lsespu8PedalDrvOfsCnt >= L_U8_TIME_10MSLOOP_1000MS )
            	{
			PEDAL_CALC->lsespu8PedalDrvOfsCnt   = 0;
			PEDAL_CALC->lsesps16PedalDrvOfsMean = (int16_t)(PEDAL_CALC->lsesps32PedalDrvOfsSum/ L_U8_TIME_10MSLOOP_1000MS);
			PEDAL_CALC->lsesps32PedalDrvOfsSum  = 0;
			PEDAL_CALC->lsespu1PedalDrvOfsFirst = 1;
			
			/*-- Array Calculation --*/			
			PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt] = PEDAL_CALC->lsesps16PedalDrvOfsMean;

			if(PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt==0)
            		{
				PEDAL_CALC->lsesps16PedalDrvOfsArrayMax = PEDAL_CALC->lsesps16PedalDrvOfsArray[0];
				PEDAL_CALC->lsesps16PedalDrvOfsArrayMin = PEDAL_CALC->lsesps16PedalDrvOfsArray[0];
            		}
            		else
            		{
				if(PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt] > PEDAL_CALC->lsesps16PedalDrvOfsArrayMax)
				{
					PEDAL_CALC->lsesps16PedalDrvOfsArrayMax = PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt];
				}
				else if(PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt] < PEDAL_CALC->lsesps16PedalDrvOfsArrayMin)
            			{
					PEDAL_CALC->lsesps16PedalDrvOfsArrayMin = PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt];
            			}
            			else
            			{
            				;
            			}
            		}
			
			PEDAL_CALC->lsesps32PedalDrvOfsArraySum= PEDAL_CALC->lsesps32PedalDrvOfsArraySum + PEDAL_CALC->lsesps16PedalDrvOfsArray[PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt];
			
			if(PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt >= 2)
			{
				PEDAL_CALC->lsespu1PedalDrv1secOfsOK = 1 ;
				PEDAL_CALC->lsesps16PedalDrvOfsArrayMean = (int16_t)( (PEDAL_CALC->lsesps32PedalDrvOfsArraySum - PEDAL_CALC->lsesps16PedalDrvOfsArrayMax -PEDAL_CALC->lsesps16PedalDrvOfsArrayMin)/(PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt -1)) ;
				
				PEDAL_CALC->lsesps16PedalDrvOfs_f = PEDAL_CALC->lsesps16PedalDrvOfsArrayMean ;
            	}
			else
			{
                ;
			}
									
			PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt = PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt + 1;
			
			if(PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt > 9)
			{
				PEDAL_CALC->lsespu8PedalDrvOfsArrayCnt  = 0;
				PEDAL_CALC->lsesps32PedalDrvOfsArraySum = 0;
				PEDAL_CALC->lsesps16PedalDrv10secMean   = PEDAL_CALC->lsesps16PedalDrvOfsArrayMean ;
				PEDAL_CALC->lsespu1PedalDrv10secOfsOK   = 1;
				
				PEDAL_CALC->lsespu8PedalDrvOfs10sArrayCnt = PEDAL_CALC->lsespu8PedalDrvOfs10sArrayCnt + 1 ;				
				PEDAL_CALC->lsesps32PedalDrvOfs10sArraySum= PEDAL_CALC->lsesps32PedalDrvOfs10sArraySum + PEDAL_CALC->lsesps16PedalDrv10secMean;				
				
				PEDAL_CALC->lsesps16PedalDrvOfs_f = PEDAL_CALC->lsesps16PedalDrv10secMean ;
				
				if(PEDAL_CALC->lsespu8PedalDrvOfs10sArrayCnt >= 6 )   /*60sec offset */
            	{
					PEDAL_CALC->lsespu8PedalDrvOfs10sArrayCnt  = 0 ;			
					PEDAL_CALC->lsesps16PedalDrvOfs60sMeanOld  = PEDAL_CALC->lsesps16PedalDrvOfs60sMean ;		
					PEDAL_CALC->lsesps16PedalDrvOfs60sMean     = (int16_t)((PEDAL_CALC->lsesps32PedalDrvOfs10sArraySum)/6) ;
					PEDAL_CALC->lsesps32PedalDrvOfs10sArraySum = 0 ;
					
					if(PEDAL_CALC->lsespu1PedalDrv60secOfsOK ==0 )
            		{
						PEDAL_CALC->lsesps16PedalDrvOfsArrayFilter = PEDAL_CALC->lsesps16PedalDrvOfs60sMean ;
						PEDAL_CALC->lsesps16PedalDrvOfsArrayFConv  = (PEDAL_CALC->lsesps16PedalDrvOfs60sMean)*10 ;
            		}
					else
            		{
						PEDAL_CALC->lsespu1PedalDrv60secOfsReOK =1 ;
						PEDAL_CALC->lsesps16PedalDrvOfsArrayFConv  = LCESP_s16Lpf1Int( (PEDAL_CALC->lsesps16PedalDrvOfs60sMean*10) , PEDAL_CALC->lsesps16PedalDrvOfsArrayFConv, L_U8FILTER_GAIN_10MSLOOP_5HZ  );  
						PEDAL_CALC->lsesps16PedalDrvOfsArrayFilter = PEDAL_CALC->lsesps16PedalDrvOfsArrayFConv/10 ; 
					}
					
					PEDAL_CALC->lsespu1PedalDrv60secOfsOK =1;		
					PEDAL_CALC->lsesps16PedalDrvOfs_f     =PEDAL_CALC->lsesps16PedalDrvOfsArrayFilter ; 			
            		}
            		else
            		{
            			;
            		}
            	}
            	else
            	{
            		;
            	}
            	
            }
            else
            {
			PEDAL_CALC->lsespu8PedalDrvOfsCnt  = PEDAL_CALC->lsespu8PedalDrvOfsCnt+1;
			PEDAL_CALC->lsesps32PedalDrvOfsSum = PEDAL_CALC->lsesps32PedalDrvOfsSum + PEDAL_CALC->lsesps16PedalSenRaw ;
            }
		}
		else
		{
		PEDAL_CALC->lsespu8PedalDrvOfsCnt   = 0;
		PEDAL_CALC->lsesps32PedalDrvOfsSum  = 0;
		}
}

/*--- Driving offset ---*/
static void  LSESP_vDetBrkAppSenOffset(void)
{
    #if (__USE_PDF_SIGNAL==ENABLE)
	ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDF; 
    PEDAL_CALC = (struct PEDAL_offset  *)&PDF;   
    LSESP_vDetEachBrkAppSenOffset();
    
    lsesps16BASOfsPDFFinal         = PDF.lsesps16PedalOfsFinal;
    lsespu1BrkAppSenPDFFiltInvalid = PDF.lsespu1PedalOfsInvalid;    
    #endif

    #if (__USE_PDT_SIGNAL==ENABLE)
    ESP_EEP_CALC = (struct ESP_EEP_offsetCalc *)&ESP_PDT;
    PEDAL_CALC = (struct PEDAL_offset  *)&PDT;   
    LSESP_vDetEachBrkAppSenOffset();
        
    lsesps16BASOfsPDTFinal         = PDT.lsesps16PedalOfsFinal;
    lsespu1BrkAppSenPDTFiltInvalid = PDT.lsespu1PedalOfsInvalid;	
    #endif
	
}

static void  LSESP_vDetEachBrkAppSenOffset(void)
{
    if(ESP_PDF.lsespu1ESPEepOfsCalOkFlg == 1)
    { 
        if(ESP_EEP_CALC->lsespu1ESPEepOfsCalOkFlg ==1 )
        {
            if(PEDAL_CALC->lsespu1PedalDrv60secOfsOK ==1)
            {
            	PEDAL_CALC->lsesps16PedalOfsFinal  = PEDAL_CALC->lsesps16PedalDrvOfs_f ;
            	PEDAL_CALC->lsespu1PedalOfsInvalid =  0;
            }
            else
            {    	
        	    PEDAL_CALC->lsesps16PedalOfsFinal  = ESP_EEP_CALC->lsesps16ESPofsFirstCal ;
        	    PEDAL_CALC->lsespu1PedalOfsInvalid = 0;
        	}
        }
        else
        {
        	;
        }
    }
    else
    {    
        if(PEDAL_CALC->lsespu1PedalDrv60secOfsOK ==1)
        {
        	PEDAL_CALC->lsesps16PedalOfsFinal  = PEDAL_CALC->lsesps16PedalDrvOfs_f ;
        	PEDAL_CALC->lsespu1PedalOfsInvalid =  0;
        }
        else
{
        	if(PEDAL_CALC->lsespu1OfsEepReadOK == 1)
	{
        		PEDAL_CALC->lsesps16PedalOfsFinal = PEDAL_CALC->lsesps16DrvOfsEepRead;
        		PEDAL_CALC->lsespu1PedalOfsInvalid =  0;
	}
	else
	{
        		if(PEDAL_CALC->lsespu1PedalEolFirstOfsRead ==1 )
		{
        			PEDAL_CALC->lsesps16PedalOfsFinal  =  PEDAL_CALC->lsesps16OfsEolFirstEep ;
        			PEDAL_CALC->lsespu1PedalOfsInvalid =  0;
		}
		else
		{
        			PEDAL_CALC->lsesps16PedalOfsFinal  =  0;
        			PEDAL_CALC->lsespu1PedalOfsInvalid =  1;
		}		
	}          
}
    }

}
/*--------------------------------------------*/

#endif 
#endif /* AHB_GEN3 */

static void LSESP_vSortSensorModel(struct ARRANGE_STRUCT SORT[], uint8_t n)
{
    uint8_t i, j;

    for(i=0; i<n; i++)
    {
        for(j=0; j<(n-i); j++)
        {
            if(SORT[j].content > SORT[j+1].content)
            {
                imsi = SORT[j];
                SORT[j] = SORT[j+1];
                SORT[j+1] = imsi;
            }
            else
            {
            	;
            }
        }
    }
}

static void LSESP_vChkControlHold(void)
{
	if((BACK_DIR==1)    ||(FLAG_BANK_DETECTED==1)||(ESP_TCS_ON==1)||
		 (YAW_CDC_WORK==1)||(ABS_fz==1)            ||(BTC_fz==1)    ||
		 (ETCS_ON==1)     ||(FTCS_ON==1)           ||(FLAG_ACTIVE_BRAKING==1))
	{
		control_hold_flg=1;
		lsespu8CtrlHold1secCnt=0;
	}
	else
	{
		if(control_hold_flg==1)
		{
			if(lsespu8CtrlHold1secCnt< L_U8_TIME_10MSLOOP_1000MS )
			{
				lsespu8CtrlHold1secCnt=lsespu8CtrlHold1secCnt+1;
			}
			else
			{
				control_hold_flg=0;
				lsespu8CtrlHold1secCnt=0;
			}
		}
		else
		{
			lsespu8CtrlHold1secCnt=0;
		}
	}

}



static void  LSESP_vDctSpinCount( uint8_t j, int16_t DiffVelocity )
{
		uint16_t DiffV;

		DiffV=McrAbs(DiffVelocity);

		if(OFST_SPIN[j]==1)
		{
			if(DiffV >VREF_5_KPH)
			{
		   	lsesp16OfsSpinCNT[j]=0;
			}
			else
			{
				  if(lsesp16OfsSpinCNT[j] > L_U8_TIME_10MSLOOP_2S)
				  {
				    	OFST_SPIN[j]=0;
				    	lsesp16OfsSpinCNT[j]=0;
				  }
				  else
				 	{
	  	        if(DiffV < VREF_1_KPH)
	  	        {
				          lsesp16OfsSpinCNT[j] = lsesp16OfsSpinCNT[j] + 2 ;
	  	        }
	  	        else if(DiffV < VREF_2_KPH)
	  	        {
				          lsesp16OfsSpinCNT[j] = lsesp16OfsSpinCNT[j] + 1 ;
	  	        }
	  	        else
	  	        {
				          ;
	  	        }
				 	}
			}

		}
		else
		{
	      if( DiffV >VREF_5_KPH )
	      {
		      OFST_SPIN[j]=1;
	      }
	      else
	      {
          ;
		    }
		      lsesp16OfsSpinCNT[j]=0;
	  }

}

static void  LSESP_vDctSpinControl( void  )
{

	uint8_t SPIN_flag_temp;
	uint8_t CONT_flag_temp;


	SPIN_flag_temp=( (OFST_SPIN[0]==1) || (OFST_SPIN[1]==1) || (OFST_SPIN[2]==1) || (OFST_SPIN[3]==1) );
	CONT_flag_temp=( (BTC_fz==1) ||(ETCS_ON==1) || (FTCS_ON==1));


	if(OFST_CONTR1==1)
	{
		LSESP_vDctSpinDelayTime();

    if(CONT_flag_temp==0)
    {
    	OFST_CONTR1=0;
    }
    else
    {
    	;
    }
    OFST_CONTR_SET_CNT=0;
    spin_control_delay_flag1=1;

	}
	else
	{
	  if((SPIN_flag_temp==1) && (CONT_flag_temp==1) )
	  {
      OFST_CONTR_SET_CNT=OFST_CONTR_SET_CNT+1;

      if(OFST_CONTR_SET_CNT >= L_U8_TIME_10MSLOOP_1000MS)
      {
    	  OFST_CONTR1=1;
    	  OFST_CONTR_SET_CNT=0;
      }
      else
      {
    	  ;
      }
	  }
	  else
	  {
		  OFST_CONTR1=0;
		  OFST_CONTR_SET_CNT=0;
	  }

	  if(spin_control_delay_flag1==1)
	  {
	  	SPIN_DELAY_T = SPIN_DELAY_TimeCalc;
	  	spin_control_delay_flag1=0;
	  }
	  else
	  {
	  	;
	  }

	  LSESP_vDctSpinDelayTimeReset();
	}

	if(vref < VREF_70_KPH )
	{
	  if(SPIN_DELAY_T>0)
	  {
	    SPIN_DELAY_T=SPIN_DELAY_T-1;
	  }
	  else
	  {
      ;
	  }
	}
	else
	{
		SPIN_DELAY_T=0;
	}

  if((SPIN_DELAY_T>0)||(OFST_CONTR1==1))
  {
  	OFST_CONTR=1;
  }
  else
  {
  	OFST_CONTR=0;
  }


}

static void  LSESP_vDctSpinDelayTime(void)
{
	int16_t   MU_EST ;
	uint16_t  SPIN_DELAY_T_velo_temp;
	uint8_t SPIN_flag_vdiff;
	uint8_t CONT_flag_temp;
	int16_t  deltV_L;
	int16_t  deltV_R;
	int16_t  deltV_vref_L;
	int16_t  deltV_vref_R;
	int16_t  deltV_SET;

	MU_EST=	FZ1.filter_out ;  /* Resolution 1000 */
	CONT_flag_temp=( (BTC_fz==1) ||(ETCS_ON==1) || (FTCS_ON==1));
	deltV_SET = VREF_2_KPH;

	#if __REAR_D
	deltV_L     =(vrad_crt_rl-vrad_crt_fl);
  deltV_R     =(vrad_crt_rr-vrad_crt_fr);
  deltV_vref_L=(vrad_crt_rl-vref);
  deltV_vref_R=(vrad_crt_rr-vref);
  #else
	deltV_L     =(vrad_crt_fl-vrad_crt_rl);
  deltV_R     =(vrad_crt_fr-vrad_crt_rr);
  deltV_vref_L=(vrad_crt_fl-vref);
  deltV_vref_R=(vrad_crt_fr-vref);
  #endif

	if((deltV_L >= deltV_SET ) || (deltV_R>= deltV_SET ))
	{
	  SPIN_flag_vdiff=1;
	}
	else
	{
		SPIN_flag_vdiff=0;
	}


  if( MU_EST>0 )
  {
  	if(SPIN_flag_vdiff==1)
  	{
  	  if(lsespu8spinCalc1secCNT >= L_U8_TIME_10MSLOOP_500MS)
  	  {
  		  lsespu8spinCalcCNT=lsespu8spinCalcCNT+1;
  	    lsespu16MuEst1secAverage = (int16_t)(lsesps32MuEstSum/L_U8_TIME_10MSLOOP_500MS);
  	    lsesps32MuEstSum=0;
  	    lsespu8spinCalc1secCNT=0;

  	    if(lsespu8spinCalcCNT==1)
  	    {
  	  	  lsespu16MuEstAverage=lsespu16MuEst1secAverage;
  	    }
  	    else
  	    {
  	  	  lsespu16MuEstAverage= LCESP_s16Lpf1Int(lsespu16MuEst1secAverage, lsespu16MuEstAverage, L_U8FILTER_GAIN_10MSLOOP_3HZ);
  	    }

  	  }
  	  else
  	  {
  		  lsesps32MuEstSum = lsesps32MuEstSum + MU_EST ;
  		  lsespu8spinCalc1secCNT=lsespu8spinCalc1secCNT+1;
  	  }
  	}
  	else
  	{
  		;
  	}
  }
  else
  {
  	lsesps32MuEstSum=0;
  	lsespu16MuEst1secAverage=0;
  	lsespu8spinCalcCNT=0;
  	lsespu8spinCalc1secCNT=0;

  }

	if( (lsespu16MuEstAverage >0 ) && (lsespu16MuEstAverage <= LAT_0G05G) )
	{
		SPIN_DELAY_TimeCalc = L_U16_TIME_10MSLOOP_20S;
	}
	else if((lsespu16MuEstAverage > LAT_0G05G) && (lsespu16MuEstAverage <= LAT_0G1G) )
	{
    SPIN_DELAY_TimeCalc = LCESP_s16IInter2Point( lsespu16MuEstAverage,LAT_0G05G,L_U16_TIME_10MSLOOP_20S,LAT_0G1G,L_U8_TIME_10MSLOOP_2S ) ;
	}
	else
	{
		SPIN_DELAY_TimeCalc = L_U8_TIME_10MSLOOP_2S;
	}

	/* SPIN_DELAY_T=dV/a=[(x-10)/3.6]/(0.03*9.8)*142 =[x-10]*135 =[x-10]*135/8(resol)=[x-10]*17 */
	if( vref>VREF_10_KPH )
	{
	  SPIN_DELAY_T_velo_temp = (vref-VREF_10_KPH)*17;

	  if(SPIN_DELAY_TimeCalc>SPIN_DELAY_T_velo_temp)
	  {
	  	SPIN_DELAY_TimeCalc= SPIN_DELAY_T_velo_temp;
	  }
	  else
	  {
	    ;
	  }
	}
	else
	{
	  SPIN_DELAY_TimeCalc = L_U8_TIME_10MSLOOP_2S;
	}


	if(SPIN_DELAY_TimeCalc>L_U16_TIME_10MSLOOP_20S)
	{
		SPIN_DELAY_TimeCalc= L_U16_TIME_10MSLOOP_20S;
	}
	else if(SPIN_DELAY_TimeCalc <= L_U8_TIME_10MSLOOP_2S )
	{
		SPIN_DELAY_TimeCalc= L_U8_TIME_10MSLOOP_2S;
	}
	else
	{
		;
	}


}

static void  LSESP_vDctSpinDelayTimeReset(void)
{
 	  lsespu16MuEst1secAverage = 0;
 	  lsespu16MuEstAverage=0;
 	  lsesps32MuEstSum=0;
 	  lsespu8spinCalcCNT=0;
 	  lsespu8spinCalc1secCNT=0;
}

static void  LSESP_vDctDragCount ( uint8_t j, int16_t DiffVelocity )
{

	if((ESP_TCS_ON==0)&&(YAW_CDC_WORK==0)&&(ABS_fz==0)&&(BTC_fz==0)&&
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 0)	    
		 (ETCS_ON==0)&&(FTCS_ON==0)&&(FLAG_ACTIVE_BRAKING==0)&&(MPRESS_BRAKE_ON==0))
#else
		 (ETCS_ON==0)&&(FTCS_ON==0)&&(FLAG_ACTIVE_BRAKING==0)&&(lsespu1MpresBrkOnByAHBgen3==0))
#endif
	{

		if(OFST_DRAG[j]==1)
		{
			if(DiffVelocity >= -VREF_1_KPH )
			{
				OFST_DRAG_CNT[j]=OFST_DRAG_CNT[j]+1;
				if(OFST_DRAG_CNT[j] >= L_U8_TIME_10MSLOOP_2S)
				{
					OFST_DRAG[j]=0;
					OFST_DRAG_CNT[j]=0;
				}
				else
				{
					;
				}
			}
			else
			{
				OFST_DRAG_CNT[j]=0;
			}

		}
		else
		{
		  if(DiffVelocity<= -VREF_2_KPH )
		  {
		  	OFST_DRAG[j]=1;
		  }
		  else
		  {
		  	;
		  }
		  OFST_DRAG_CNT[j]=0;
		}
	}
	else
	{
		OFST_DRAG[j]=0;
		OFST_DRAG_CNT[j]=0;
	}

}
#if  TC27X_FEE==ENABLE


static void LSESP_vReadEEPROMSensorOffset(void)
{
	if(SAS_read_eeprom_end_flg==0 || YAW_read_eeprom_end_flg ==0 ||ALAT_read_eeprom_end_flg == 0|| FS_YAW_read_eeprom_end_flg==0
	     ||(KYAW.eep_max_read_flg==0)||(KYAW.eep_min_read_flg==0)||(KSTEER.eep_max_read_flg==0)||(KSTEER.eep_min_read_flg==0)
	     ||(KLAT.eep_max_read_flg==0)||(KLAT.eep_min_read_flg==0)||(yaw_ofst_max_rd_flg==0)||(yaw_ofst_min_rd_flg==0)
	     ||(lsespu1EEPYawStandOfsReadEnd==0)
	     )
	{
    /*---------------------/ STR ---------------------------*/
    #if __STEER_SENSOR_TYPE==CAN_TYPE
    if(SAS_read_eeprom_end_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KSTEER_offset_of_eeprom+(uint32_t)NVM_KSTEER_offset_of_eeprom_c==0xffffffff)
				{
					KSTEER.offset_of_eeprom2 = (int16_t)NVM_KSTEER_offset_of_eeprom;
			if(McrAbs(KSTEER.offset_of_eeprom2) <= 1300)   /*/ limit ok (130도 이하)*/
			{
				str_value_init_rq_flg=0;
				KSTEER.fs_eeprom_ok_flg=1;
			}
			else
			{
				str_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
				KSTEER.fs_eeprom_ok_flg=0;
				KSTEER.offset_of_eeprom2=0;
			}
		}
		else
		{
			str_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
			KSTEER.fs_eeprom_ok_flg=0;
			KSTEER.offset_of_eeprom2=0;
		}
		SAS_read_eeprom_end_flg=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
    #else
    KSTEER.fs_eeprom_ok_flg=0;
    KSTEER.offset_of_eeprom2=0;
    SAS_read_eeprom_end_flg=1;
    #endif

    /*---------------------/ YAW ---------------------------*/
    if(YAW_read_eeprom_end_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KYAW_offset_of_eeprom+(uint32_t)NVM_KYAW_offset_of_eeprom_c==0xffffffff)
				{
					KYAW.offset_of_eeprom2 = (int16_t)NVM_KYAW_offset_of_eeprom;
			if(McrAbs(KYAW.offset_of_eeprom2) <= 1000)   /*/ limit ok (10도/s 이하)*/
			{
				yaw_value_init_rq_flg=0;
				KYAW.fs_eeprom_ok_flg=1;
			}
			else
			{
				yaw_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
				KYAW.fs_eeprom_ok_flg=0;
				KYAW.offset_of_eeprom2=0;
			}
		}
		else
		{
			yaw_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
			KYAW.fs_eeprom_ok_flg=0;
			KYAW.offset_of_eeprom2=0;
		}
		YAW_read_eeprom_end_flg=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}

    /*---------------------/ YAW Stand Noraml---------------------------*/
    if(lsespu1EEPYawStandOfsReadEnd==0)
    {
		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_lsesps16EEPYawStandOfs+(uint32_t)NVM_lsesps16EEPYawStandOfs_c==0xffffffff)
				{
					lsesps16EEPYawStandOfsRead = (int16_t)NVM_lsesps16EEPYawStandOfs;
			if(McrAbs(lsesps16EEPYawStandOfsRead) <= YAW_OFFSET_MAX_CORR)
			{
				lsespu1EEPYawStandOfsInitReq=0;
				lsespu1EEPYawStandOfsReadOK=1;
			}
			else
			{
				lsespu1EEPYawStandOfsInitReq=1;    /*/ limit 벗어나면 초기화*/
				lsespu1EEPYawStandOfsReadOK=0;
				lsesps16EEPYawStandOfsRead=0;
			}

		}
		else
		{
			lsespu1EEPYawStandOfsInitReq=1;    /*/ 값 초기화 요청*/
			lsespu1EEPYawStandOfsReadOK=0;
			lsesps16EEPYawStandOfsRead=0;
		}
		lsespu1EEPYawStandOfsReadEnd=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}

    /*---------------------/ LG  ---------------------------*/
    if(ALAT_read_eeprom_end_flg==0)
    {
		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KLAT_offset_of_eeprom+(uint32_t)NVM_KLAT_offset_of_eeprom_c==0xffffffff)
				{
					KLAT.offset_of_eeprom2 = (int16_t)NVM_KLAT_offset_of_eeprom;
			if(McrAbs(KLAT.offset_of_eeprom2) <= 600)   /*/ limit ok (0.6G 이하)*/
			{
				lg_value_init_rq_flg=0;
				KLAT.fs_eeprom_ok_flg=1;
			}
			else
			{
				lg_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
				KLAT.fs_eeprom_ok_flg=0;
				KLAT.offset_of_eeprom2=0;
			}
		}
		else
		{
			lg_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
			KLAT.fs_eeprom_ok_flg=0;
			KLAT.offset_of_eeprom2=0;
		}
		ALAT_read_eeprom_end_flg=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	
    /*---------------------/ FS YAW  ---------------------------*/
    //should be moved to FS -NVM read/write
    if(FS_YAW_read_eeprom_end_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_fs_yaw_eeprom_offset+(uint32_t)NVM_fs_yaw_eeprom_offset_c==0xffffffff)
				{
					read_yaw_eeprom_offset = (int16_t)NVM_fs_yaw_eeprom_offset;
			if(McrAbs(read_yaw_eeprom_offset) <= YAW_7DEG)   /*/ limit ok 7'/s 이하)*/
			{
				fs_yaw_init_rq_flg=0;
				fs_yaw_offset_eeprom_ok_flg=1;
			}
			else
			{
				fs_yaw_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
				fs_yaw_offset_eeprom_ok_flg=0;
				read_yaw_eeprom_offset=0;
			}
		}
		else
		{
			fs_yaw_init_rq_flg=1;    /*/ 값 초기화 요청*/
			fs_yaw_offset_eeprom_ok_flg=0;
			read_yaw_eeprom_offset=0;
		}
		FS_YAW_read_eeprom_end_flg=1;
			}
			else
			{
				;
			}
    }
    else
    {
    	;
    }

    /*---------------------/ YAW_MAX ---------------------------*/
    if(KYAW.eep_max_read_flg==0)
    {
		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KYAW_eep_max+(uint32_t)NVM_KYAW_eep_max_c==0xffffffff)
				{
					KYAW.eep_max = (int16_t)NVM_KYAW_eep_max;
			if(McrAbs(KYAW.eep_max) <= 1000)   /*/ limit ok (10도/s 이하)*/
			{
				KYAW.eep_max_init_flg=0;
				KYAW.eep_max_ok_flg=1;
			}
			else
			{
				KYAW.eep_max_init_flg=1;    /*/ limit 벗어나면 초기화*/
				KYAW.eep_max_ok_flg=0;
				KYAW.eep_max=0;
			}
		}
		else
		{
			KYAW.eep_max_init_flg=1;    /*/ 값 초기화 요청*/
			KYAW.eep_max_ok_flg=0;
			KYAW.eep_max=0;
		}
		KYAW.eep_max_read_flg=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	
    /*---------------------/ YAW_MIN ---------------------------*/
    if(KYAW.eep_min_read_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KYAW_eep_min+(uint32_t)NVM_KYAW_eep_min_c==0xffffffff)
				{
					KYAW.eep_min = (int16_t)NVM_KYAW_eep_min;
			if(McrAbs(KYAW.eep_min) <= 1000)   /*/ limit ok (10도/s 이하)*/
			{
				KYAW.eep_min_init_flg=0;
				KYAW.eep_min_ok_flg=1;
			}
			else
			{
				KYAW.eep_min_init_flg=1;    /*/ limit 벗어나면 초기화*/
				KYAW.eep_min_ok_flg=0;
				KYAW.eep_min=0;
			}
		}
		else
		{
			KYAW.eep_min_init_flg=1;    /*/ 값 초기화 요청*/
			KYAW.eep_min_ok_flg=0;
			KYAW.eep_min=0;
		}
		KYAW.eep_min_read_flg=1;

    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	
    #if __STEER_SENSOR_TYPE==CAN_TYPE
    /*---------------------/ STR_MAX ---------------------------*/
    if(KSTEER.eep_max_read_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KSTEER_eep_max+(uint32_t)NVM_KSTEER_eep_max_c==0xffffffff)
				{
					KSTEER.eep_max = (int16_t)NVM_KSTEER_eep_max;
			if(McrAbs(KSTEER.eep_max) <= 1300)   /*/ limit ok (130deg 이하)*/
			{
				KSTEER.eep_max_init_flg=0;
				KSTEER.eep_max_ok_flg=1;
			}
			else
			{
				KSTEER.eep_max_init_flg=1;    /*/ limit 벗어나면 초기화*/
				KSTEER.eep_max_ok_flg=0;
				KSTEER.eep_max=0;
			}
		}
		else
		{
			KSTEER.eep_max_init_flg=1;    /*/ 값 초기화 요청*/
			KSTEER.eep_max_ok_flg=0;
			KSTEER.eep_max=0;
		}
		KSTEER.eep_max_read_flg=1;

    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	
    /*---------------------/ STR_MIN ---------------------------*/
    if(KSTEER.eep_min_read_flg==0)
    {

		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KSTEER_eep_min+(uint32_t)NVM_KSTEER_eep_min_c==0xffffffff)
				{
					KSTEER.eep_min = (int16_t)NVM_KSTEER_eep_min;
			if(McrAbs(KSTEER.eep_min) <= 1300)   /* limit ok (130deg 이하)*/
			{
				KSTEER.eep_min_init_flg=0;
				KSTEER.eep_min_ok_flg=1;
			}
			else
			{
				KSTEER.eep_min_init_flg=1;    /* limit 벗어나면 초기화*/
				KSTEER.eep_min_ok_flg=0;
				KSTEER.eep_min=0;
			}
		}
		else
		{
			KSTEER.eep_min_init_flg=1;    /*/ 값 초기화 요청*/
			KSTEER.eep_min_ok_flg=0;
			KSTEER.eep_min=0;
		}
		KSTEER.eep_min_read_flg=1;
			}
			else
			{
				;
			}
    }
    else
    {
    	;
    }
    #else
    KSTEER.eep_max_read_flg=1;
    KSTEER.eep_min_read_flg=1;
    KSTEER.eep_max_ok_flg=0;
    KSTEER.eep_max=0;
    KSTEER.eep_min_ok_flg=0;
    KSTEER.eep_min=0;
    #endif
	
    /*---------------------/ LAT_MAX ---------------------------*/
    if(KLAT.eep_max_read_flg==0)
    {

    	if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KLAT_eep_max+(uint32_t)NVM_KLAT_eep_max_c==0xffffffff)
				{
					KLAT.eep_max = (int16_t)NVM_KLAT_eep_max;
			if(McrAbs(KLAT.eep_max) <= 600)
			{
				KLAT.eep_max_init_flg=0;
				KLAT.eep_max_ok_flg=1;
			}
			else
			{
				KLAT.eep_max_init_flg=1;
				KLAT.eep_max_ok_flg=0;
				KLAT.eep_max=0;
			}
		}
		else
		{
			KLAT.eep_max_init_flg=1;
			KLAT.eep_max_ok_flg=0;
			KLAT.eep_max=0;
		}
		KLAT.eep_max_read_flg=1;

    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	    
    /*---------------------/ LAT_MIN ---------------------------*/
    if(KLAT.eep_min_read_flg==0)
    {
		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_KLAT_eep_min+(uint32_t)NVM_KLAT_eep_min_c==0xffffffff)
				{
					KLAT.eep_min = (int16_t)NVM_KLAT_eep_min;
			if(McrAbs(KLAT.eep_min) <= 600)
			{
				KLAT.eep_min_init_flg=0;
				KLAT.eep_min_ok_flg=1;
			}
			else
			{
				KLAT.eep_min_init_flg=1;
				KLAT.eep_min_ok_flg=0;
				KLAT.eep_min=0;
			}
		}
		else
		{
			KLAT.eep_min_init_flg=1;
			KLAT.eep_min_ok_flg=0;
			KLAT.eep_min=0;
		}
		KLAT.eep_min_read_flg=1;

    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	    
    /*---------------------/ STL_MAX ---------------------------*/
    if(yaw_ofst_max_rd_flg==0)
    {
    	if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_yaw_still_eep_max+(uint32_t)NVM_yaw_still_eep_max_c==0xffffffff)
				{
					yaw_still_eep_max = (int16_t)NVM_yaw_still_eep_max;
			if(McrAbs(yaw_still_eep_max) <= 700)
			{
				yaw_ofst_max_init_flg=0;
				yaw_ofst_max_ok_flg=1;
			}
			else
			{
				yaw_ofst_max_init_flg=1;
				yaw_ofst_max_ok_flg=0;
				yaw_still_eep_max=0;
			}
		}
		else
		{
			yaw_ofst_max_init_flg=1;
			yaw_ofst_max_ok_flg=0;
			yaw_still_eep_max=0;
		}
		yaw_ofst_max_rd_flg=1;
    }
    else
    {
    	;
    }
		}
		else
		{
			;
		}
	    
    /*---------------------/ STL_MIN ---------------------------*/
    if(yaw_ofst_min_rd_flg==0)
    {
		if(FeeDataReadInvalidFlg==0)
		{
				if((uint32_t)NVM_yaw_still_eep_min+(uint32_t)NVM_yaw_still_eep_min_c==0xffffffff)
				{
					yaw_still_eep_min = (int16_t)NVM_yaw_still_eep_min;
			if(McrAbs(yaw_still_eep_min) <= 700)
			{
				yaw_ofst_min_init_flg=0;
				yaw_ofst_min_ok_flg=1;
			}
			else
			{
				yaw_ofst_min_init_flg=1;
				yaw_ofst_min_ok_flg=0;
				yaw_still_eep_min=0;
			}
		}
		else
		{
			yaw_ofst_min_init_flg=1;
			yaw_ofst_min_ok_flg=0;
			yaw_still_eep_min=0;
		}
		yaw_ofst_min_rd_flg=1;
    }
    else
    {
    	;
    }
        }
        else
        {
        	;
        }
            }
            else
            {
            	;
            }
        }

void LSESP_vWriteEEPROMSensorOffset(void)
{
	if(wu8IgnStat == CE_OFF_STATE)
	{
	if((str_value_init_rq_flg==1)||(yaw_value_init_rq_flg==1)||(lg_value_init_rq_flg==1)||(fs_yaw_init_rq_flg==1)
		||(KSTEER.NEED_TO_WR_OFFSET_FLG==1)||(KYAW.NEED_TO_WR_OFFSET_FLG==1)||(KLAT.NEED_TO_WR_OFFSET_FLG==1)/*||(req_yaw_eeprom_write_flg==1) */
		||(KYAW.eep_max_w_flg==1)||(KYAW.eep_min_w_flg==1)||(KLAT.eep_max_w_flg==1)||(KLAT.eep_min_w_flg==1)
		||(KSTEER.eep_max_w_flg==1)||(KSTEER.eep_min_w_flg==1)||(yaw_ofst_max_wr_flg==1)||(yaw_ofst_min_wr_flg==1)
		||(KYAW.eep_max_init_flg==1)||(KYAW.eep_min_init_flg==1)||(KSTEER.eep_max_init_flg==1)||(KSTEER.eep_min_init_flg==1)
		||(KLAT.eep_max_init_flg==1)||(KLAT.eep_min_init_flg==1)||(yaw_ofst_max_init_flg==1)||(yaw_ofst_min_init_flg==1)
		||(lsespu1EEPYawStandWrtReq==1)||(lsespu1EEPYawStandOfsInitReq==1)
		)
	{
		write_sensor_offset_eeprom_flg=1;
	}
	else
	{
		write_sensor_offset_eeprom_flg=0;
	}

	if(write_sensor_offset_eeprom_flg==1)
	{

		if(KSTEER.NEED_TO_WR_OFFSET_FLG==0 && str_value_init_rq_flg==1)
		{
			str_value_init_rq_flg=0;
			KSTEER.model_offset=0;
			KSTEER.NEED_TO_WR_OFFSET_FLG=1;
		}
		else
		{
			;
		}

		if(KYAW.NEED_TO_WR_OFFSET_FLG==0 && yaw_value_init_rq_flg==1)
		{
			yaw_value_init_rq_flg=0;
			KYAW.model_offset=0;
			KYAW.NEED_TO_WR_OFFSET_FLG=1;
		}
		else
		{
			;
		}

		if(KLAT.NEED_TO_WR_OFFSET_FLG==0 && lg_value_init_rq_flg==1)
		{
			lg_value_init_rq_flg=0;
			KLAT.model_offset=0;
			KLAT.NEED_TO_WR_OFFSET_FLG=1;
		}
		else
		{
			;
		}

//		if(req_yaw_eeprom_write_flg==0 && fs_yaw_init_rq_flg==1)
//		{
//			fs_yaw_init_rq_flg=0;
//			read_yaw_eeprom_offset=0;
//			req_yaw_eeprom_write_flg=1;
//		}
//		else
//		{
//			;
//		}

		if(KSTEER.eep_max_w_flg==0 && KSTEER.eep_max_init_flg==1)
		{
			KSTEER.eep_max_init_flg=0;
			KSTEER.eep_max_w=0;
			KSTEER.eep_max_w_flg=1;
		}
		else
		{
			;
		}
		if(KSTEER.eep_min_w_flg==0 && KSTEER.eep_min_init_flg==1)
		{
			KSTEER.eep_min_init_flg=0;
			KSTEER.eep_min_w=0;
			KSTEER.eep_min_w_flg=1;
		}
		else
		{
			;
		}
		if(KYAW.eep_max_w_flg==0 && KYAW.eep_max_init_flg==1)
		{
			KYAW.eep_max_init_flg=0;
			KYAW.eep_max_w=0;
			KYAW.eep_max_w_flg=1;
		}
		else
		{
			;
		}
		if(KYAW.eep_min_w_flg==0 && KYAW.eep_min_init_flg==1)
		{
			KYAW.eep_min_init_flg=0;
			KYAW.eep_min_w=0;
			KYAW.eep_min_w_flg=1;
		}
		else
		{
			;
		}
		if(KLAT.eep_max_w_flg==0 && KLAT.eep_max_init_flg==1)
		{
			KLAT.eep_max_init_flg=0;
			KLAT.eep_max_w=0;
			KLAT.eep_max_w_flg=1;
		}
		else
		{
			;
		}
		if(KLAT.eep_min_w_flg==0 && KLAT.eep_min_init_flg==1)
		{
			KLAT.eep_min_init_flg=0;
			KLAT.eep_min_w=0;
			KLAT.eep_min_w_flg=1;
		}
		else
		{
			;
		}
		if(yaw_ofst_max_wr_flg==0 && yaw_ofst_max_init_flg==1)
		{
			yaw_ofst_max_init_flg=0;
			yaw_still_eep_max_w=0;
			yaw_ofst_max_wr_flg=1;
		}
		else
		{
			;
		}
		if(yaw_ofst_min_wr_flg==0 && yaw_ofst_min_init_flg==1)
		{
			yaw_ofst_min_init_flg=0;
			yaw_still_eep_min_w=0;
			yaw_ofst_min_wr_flg=1;
		}
		else
		{
			;
		}

		if( (lsespu1EEPYawStandWrtReq==0) && (lsespu1EEPYawStandOfsInitReq==1) )
		{
			lsespu1EEPYawStandOfsInitReq=0;
			lsesps16YawStandOffset=0;
			lsespu1EEPYawStandWrtReq=1;
		}
		else
		{
			;
		}


		if(KSTEER.NEED_TO_WR_OFFSET_FLG==1)
		{
				NVM_KSTEER_offset_of_eeprom   = (int32_t)KSTEER.model_offset;
				NVM_KSTEER_offset_of_eeprom_c = (NVM_KSTEER_offset_of_eeprom^0xffffffff);
			KSTEER.NEED_TO_WR_OFFSET_FLG=0;

		}
		if(KYAW.NEED_TO_WR_OFFSET_FLG==1)
		{
				NVM_KYAW_offset_of_eeprom   = (int32_t)KYAW.model_offset;
				NVM_KYAW_offset_of_eeprom_c = (NVM_KYAW_offset_of_eeprom^0xffffffff);
			KYAW.NEED_TO_WR_OFFSET_FLG=0;
		}
		if(KLAT.NEED_TO_WR_OFFSET_FLG==1)
		{
				NVM_KLAT_offset_of_eeprom   = (int32_t)KLAT.model_offset;
				NVM_KLAT_offset_of_eeprom_c = (NVM_KLAT_offset_of_eeprom^0xffffffff);
			KLAT.NEED_TO_WR_OFFSET_FLG=0;

		}
//		if(req_yaw_eeprom_write_flg==1)
//		{
//			NVM_fs_yaw_eeprom_offset=yaw_eeprom_offset_write;
//			req_yaw_eeprom_write_flg=0;
//		}


		if(KYAW.eep_max_w_flg==1)
		{
				NVM_KYAW_eep_max   = (int32_t)KYAW.eep_max_w;
				NVM_KYAW_eep_max_c = (NVM_KYAW_eep_max^0xffffffff);
			KYAW.eep_max_w_flg=0;
		}
		if(KYAW.eep_min_w_flg==1)
		{
				NVM_KYAW_eep_min   = (int32_t)KYAW.eep_min_w;
				NVM_KYAW_eep_min_c = (NVM_KYAW_eep_min^0xffffffff);
			KYAW.eep_min_w_flg=0;
		}
		if(KSTEER.eep_max_w_flg==1)
		{
				NVM_KSTEER_eep_max   = (int32_t)KSTEER.eep_max_w;
				NVM_KSTEER_eep_max_c = (NVM_KSTEER_eep_max^0xffffffff);
			KSTEER.eep_max_w_flg=0;

		}
		if(KSTEER.eep_min_w_flg==1)
		{
				NVM_KSTEER_eep_min   = (int32_t)KSTEER.eep_min_w;
				NVM_KSTEER_eep_min_c = (NVM_KSTEER_eep_min^0xffffffff);
			KSTEER.eep_min_w_flg=0;

		}
		if(KLAT.eep_max_w_flg==1)
		{
				NVM_KLAT_eep_max   = (int32_t)KLAT.eep_max_w;
				NVM_KLAT_eep_max_c = (NVM_KLAT_eep_max^0xffffffff);
			KLAT.eep_max_w_flg=0;
		}
		if(KLAT.eep_min_w_flg==1)
		{
				NVM_KLAT_eep_min   = (int32_t)KLAT.eep_min_w;
				NVM_KLAT_eep_min_c = (NVM_KLAT_eep_min^0xffffffff);
			KLAT.eep_min_w_flg=0;
		}
		if(yaw_ofst_max_wr_flg==1)
		{
				NVM_yaw_still_eep_max   = (int32_t)yaw_still_eep_max_w;
				NVM_yaw_still_eep_max_c = (NVM_yaw_still_eep_max^0xffffffff);
			yaw_ofst_max_wr_flg=0;
		}
		if(yaw_ofst_min_wr_flg==1)
		{
				NVM_yaw_still_eep_min   = (int32_t)yaw_still_eep_min_w;
				NVM_yaw_still_eep_min_c = (NVM_yaw_still_eep_min^0xffffffff);
			yaw_ofst_min_wr_flg=0;

		}
		if(lsespu1EEPYawStandWrtReq==1)
		{
				NVM_lsesps16EEPYawStandOfs   = (int32_t)lsesps16YawStandOffset;
				NVM_lsesps16EEPYawStandOfs_c = (NVM_lsesps16EEPYawStandOfs^0xffffffff);
			lsespu1EEPYawStandWrtReq=0;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
#if __YAW_TEMPERATURE_COMPENSATION

	LSESP_vSaveYawTmpMap();


/*	if((write_sensor_offset_eeprom_flg==0)&&(write_yaw_tmp_map_eeprom_flg==0)&&(init_yaw_tmp_map_eeprom_flg==0)&&(wbu1YawSNUnmatchFlg==0))  */
	if((write_sensor_offset_eeprom_flg==0)&&(write_yaw_tmp_map_eeprom_flg==0)&&(init_yaw_tmp_map_eeprom_flg==0) )
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
	#if __TEST_MODE_YAW_TMP_SRCH ==1
	if(__TEST_MODE_YAW_TMP_SRCH==1)
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
	#endif
#else
	if(write_sensor_offset_eeprom_flg==0)
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
#endif

#if __ERASE_YAW_TMP_MAP

	fs_vcc_off_flg=0;

	LSESP_vEraseYawTmpMap();

	fs_vcc_off_flg=1;

#endif

}
	else
	{
		;
	}
}

#else
uint16_t LSESP_u16WriteEEPROM(uint16_t addr,uint16_t data)
{
    if(WE_u8ReadEEP_Byte(addr,&com_rx_buf[0],4)==1)
    {
        if((*(uint16_t*)&com_rx_buf[0]!=data)
            ||(*(uint16_t*)&com_rx_buf[2])!=(data^0xffff))
        {
            weu16EepBuff[0]=data;
            weu16EepBuff[1]=(data^0xffff);
            weu16Eep_addr=addr;
            weu1WriteSector=1;
            return 1; /*writing*/
        }
        else
        {
        	return 0; /*written already*/
        }
    }
    else
    {
    	return 2; /*read fail*/
    }
}

static void LSESP_vReadEEPROMSensorOffset(void)
{

	if(SAS_read_eeprom_end_flg==0 || YAW_read_eeprom_end_flg ==0 ||ALAT_read_eeprom_end_flg == 0|| FS_YAW_read_eeprom_end_flg==0
	     ||(KYAW.eep_max_read_flg==0)||(KYAW.eep_min_read_flg==0)||(KSTEER.eep_max_read_flg==0)||(KSTEER.eep_min_read_flg==0)
	     ||(KLAT.eep_max_read_flg==0)||(KLAT.eep_min_read_flg==0)||(yaw_ofst_max_rd_flg==0)||(yaw_ofst_min_rd_flg==0)
	     ||(lsespu1EEPYawStandOfsReadEnd==0) 
	     #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
	     ||(PRESS_M.u1premEEPOfsReadEnd==0) 
	         #if ( __FL_PRESS==ENABLE )
	         || (PRESS_FL.u1premEEPOfsReadEnd==0)
	         #endif
	         
	         #if ( __FR_PRESS==ENABLE )
	         || (PRESS_FR.u1premEEPOfsReadEnd==0)
	         #endif
	         
	         #if ( __RL_PRESS==ENABLE )
	         ||(PRESS_RL.u1premEEPOfsReadEnd==0)
	         #endif
	         
	         #if ( __RR_PRESS==ENABLE )
	         || (PRESS_RR.u1premEEPOfsReadEnd==0)
	         #endif
	     #endif	  
	     
	     )
	{
    /*---------------------/ STR ---------------------------*/
    #if __STEER_SENSOR_TYPE==CAN_TYPE
    if(SAS_read_eeprom_end_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x180,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KSTEER.offset_of_eeprom2 = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KSTEER.offset_of_eeprom2) <= 1300)   /*/ limit ok (130도 이하)*/
                {
                    str_value_init_rq_flg=0;
                    KSTEER.fs_eeprom_ok_flg=1;
                }
                else
                {
                    str_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
                    KSTEER.fs_eeprom_ok_flg=0;
                    KSTEER.offset_of_eeprom2=0;
                }
            }
            else
            {
                str_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
                KSTEER.fs_eeprom_ok_flg=0;
                KSTEER.offset_of_eeprom2=0;
            }
            SAS_read_eeprom_end_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    #else
    KSTEER.fs_eeprom_ok_flg=0;
    KSTEER.offset_of_eeprom2=0;
    SAS_read_eeprom_end_flg=1;
    #endif

    /*---------------------/ YAW ---------------------------*/
    if(YAW_read_eeprom_end_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x184,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KYAW.offset_of_eeprom2 = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KYAW.offset_of_eeprom2) <= 1000)   /*/ limit ok (10도/s 이하)*/
                {
                    yaw_value_init_rq_flg=0;
                    KYAW.fs_eeprom_ok_flg=1;
                }
                else
                {
                    yaw_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
                    KYAW.fs_eeprom_ok_flg=0;
                    KYAW.offset_of_eeprom2=0;
                }
            }
            else
            {
                yaw_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
                KYAW.fs_eeprom_ok_flg=0;
                KYAW.offset_of_eeprom2=0;
            }
            YAW_read_eeprom_end_flg=1; 
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    
    /*---------------------/ YAW Stand Noraml---------------------------*/
    if(lsespu1EEPYawStandOfsReadEnd==0)
    {
        if(WE_u8ReadEEP_Byte(0x1c6,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                lsesps16EEPYawStandOfsRead = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(lsesps16EEPYawStandOfsRead) <= YAW_OFFSET_MAX_CORR)  
                {
                    lsespu1EEPYawStandOfsInitReq=0;
                    lsespu1EEPYawStandOfsReadOK=1;                    
                }
                else
                {
                    lsespu1EEPYawStandOfsInitReq=1;    /*/ limit 벗어나면 초기화*/
                    lsespu1EEPYawStandOfsReadOK=0;
                    lsesps16EEPYawStandOfsRead=0;
                }
                
            }
            else
            {
                lsespu1EEPYawStandOfsInitReq=1;    /*/ 값 초기화 요청*/
                lsespu1EEPYawStandOfsReadOK=0;
                lsesps16EEPYawStandOfsRead=0;
            }
            lsespu1EEPYawStandOfsReadEnd=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    
    
    /*---------------------/ LG  ---------------------------*/
    if(ALAT_read_eeprom_end_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x188,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KLAT.offset_of_eeprom2 = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KLAT.offset_of_eeprom2) <= 600)   /*/ limit ok (0.6G 이하)*/
                {
                    lg_value_init_rq_flg=0;
                    KLAT.fs_eeprom_ok_flg=1;
                }
                else
                {
                    lg_value_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
                    KLAT.fs_eeprom_ok_flg=0;
                    KLAT.offset_of_eeprom2=0;
                }
            }
            else
            {
                lg_value_init_rq_flg=1;    /*/ 값 초기화 요청*/
                KLAT.fs_eeprom_ok_flg=0;
                KLAT.offset_of_eeprom2=0;
            }
            ALAT_read_eeprom_end_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ FS YAW  ---------------------------*/
    if(FS_YAW_read_eeprom_end_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x18c,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                read_yaw_eeprom_offset = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(read_yaw_eeprom_offset) <= YAW_7DEG)   /*/ limit ok 7'/s 이하)*/
                {
                    fs_yaw_init_rq_flg=0;
                    fs_yaw_offset_eeprom_ok_flg=1;
                }
                else
                {
                    fs_yaw_init_rq_flg=1;    /*/ limit 벗어나면 초기화*/
                    fs_yaw_offset_eeprom_ok_flg=0;
                    read_yaw_eeprom_offset=0;
                }
            }
            else
            {
                fs_yaw_init_rq_flg=1;    /*/ 값 초기화 요청*/
                fs_yaw_offset_eeprom_ok_flg=0;
                read_yaw_eeprom_offset=0;
            }
            FS_YAW_read_eeprom_end_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    /*---------------------/ YAW_MAX ---------------------------*/
    if(KYAW.eep_max_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x190,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KYAW.eep_max = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KYAW.eep_max) <= 1000)   /*/ limit ok (10도/s 이하)*/
                {
                    KYAW.eep_max_init_flg=0;
                    KYAW.eep_max_ok_flg=1;
                }
                else
                {
                    KYAW.eep_max_init_flg=1;    /*/ limit 벗어나면 초기화*/
                    KYAW.eep_max_ok_flg=0;
                    KYAW.eep_max=0;
                }
            }
            else
            {
                KYAW.eep_max_init_flg=1;    /*/ 값 초기화 요청*/
                KYAW.eep_max_ok_flg=0;
                KYAW.eep_max=0;
            }
            KYAW.eep_max_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ YAW_MIN ---------------------------*/
    if(KYAW.eep_min_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x194,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KYAW.eep_min = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KYAW.eep_min) <= 1000)   /*/ limit ok (10도/s 이하)*/
                {
                    KYAW.eep_min_init_flg=0;
                    KYAW.eep_min_ok_flg=1;
                }
                else
                {
                    KYAW.eep_min_init_flg=1;    /*/ limit 벗어나면 초기화*/
                    KYAW.eep_min_ok_flg=0;
                    KYAW.eep_min=0;
                }
            }
            else
            {
                KYAW.eep_min_init_flg=1;    /*/ 값 초기화 요청*/
                KYAW.eep_min_ok_flg=0;
                KYAW.eep_min=0;
            }
            KYAW.eep_min_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    #if __STEER_SENSOR_TYPE==CAN_TYPE
    /*---------------------/ STR_MAX ---------------------------*/
    if(KSTEER.eep_max_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x198,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KSTEER.eep_max = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KSTEER.eep_max) <= 1300)   /*/ limit ok (130deg 이하)*/
                {
                    KSTEER.eep_max_init_flg=0;
                    KSTEER.eep_max_ok_flg=1;
                }
                else
                {
                    KSTEER.eep_max_init_flg=1;    /*/ limit 벗어나면 초기화*/
                    KSTEER.eep_max_ok_flg=0;
                    KSTEER.eep_max=0;
                }
            }
            else
            {
                KSTEER.eep_max_init_flg=1;    /*/ 값 초기화 요청*/
                KSTEER.eep_max_ok_flg=0;
                KSTEER.eep_max=0;
            }
            KSTEER.eep_max_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ STR_MIN ---------------------------*/
    if(KSTEER.eep_min_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x19c,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KSTEER.eep_min = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KSTEER.eep_min) <= 1300)   /* limit ok (130deg 이하)*/
                {
                    KSTEER.eep_min_init_flg=0;
                    KSTEER.eep_min_ok_flg=1;
                }
                else
                {
                    KSTEER.eep_min_init_flg=1;    /* limit 벗어나면 초기화*/
                    KSTEER.eep_min_ok_flg=0;
                    KSTEER.eep_min=0;
                }
            }
            else
            {
                KSTEER.eep_min_init_flg=1;    /*/ 값 초기화 요청*/
                KSTEER.eep_min_ok_flg=0;
                KSTEER.eep_min=0;
            }
            KSTEER.eep_min_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    #else
    KSTEER.eep_max_read_flg=1;
    KSTEER.eep_min_read_flg=1;
    KSTEER.eep_max_ok_flg=0;
    KSTEER.eep_max=0;
    KSTEER.eep_min_ok_flg=0;
    KSTEER.eep_min=0;
    #endif
    /*---------------------/ LAT_MAX ---------------------------*/
    if(KLAT.eep_max_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x1a0,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KLAT.eep_max = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KLAT.eep_max) <= 600)
                {
                    KLAT.eep_max_init_flg=0;
                    KLAT.eep_max_ok_flg=1;
                }
                else
                {
                    KLAT.eep_max_init_flg=1;
                    KLAT.eep_max_ok_flg=0;
                    KLAT.eep_max=0;
                }
            }
            else
            {
                KLAT.eep_max_init_flg=1;
                KLAT.eep_max_ok_flg=0;
                KLAT.eep_max=0;
            }
            KLAT.eep_max_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ LAT_MIN ---------------------------*/
    if(KLAT.eep_min_read_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x1a4,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                KLAT.eep_min = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(KLAT.eep_min) <= 600)
                {
                    KLAT.eep_min_init_flg=0;
                    KLAT.eep_min_ok_flg=1;
                }
                else
                {
                    KLAT.eep_min_init_flg=1;
                    KLAT.eep_min_ok_flg=0;
                    KLAT.eep_min=0;
                }
            }
            else
            {
                KLAT.eep_min_init_flg=1;
                KLAT.eep_min_ok_flg=0;
                KLAT.eep_min=0;
            }
            KLAT.eep_min_read_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ STL_MAX ---------------------------*/
    if(yaw_ofst_max_rd_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x1a8,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                yaw_still_eep_max = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(yaw_still_eep_max) <= 700)
                {
                    yaw_ofst_max_init_flg=0;
                    yaw_ofst_max_ok_flg=1;
                }
                else
                {
                    yaw_ofst_max_init_flg=1;
                    yaw_ofst_max_ok_flg=0;
                    yaw_still_eep_max=0;
                }
            }
            else
            {
                yaw_ofst_max_init_flg=1;
                yaw_ofst_max_ok_flg=0;
                yaw_still_eep_max=0;
            }
            yaw_ofst_max_rd_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    /*---------------------/ STL_MIN ---------------------------*/
    if(yaw_ofst_min_rd_flg==0)
    {
        if(WE_u8ReadEEP_Byte(0x1ac,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                yaw_still_eep_min = *(int16_t*)&com_rx_buf[0];
                if(McrAbs(yaw_still_eep_min) <= 700)
                {
                    yaw_ofst_min_init_flg=0;
                    yaw_ofst_min_ok_flg=1;
                }
                else
                {
                    yaw_ofst_min_init_flg=1;
                    yaw_ofst_min_ok_flg=0;
                    yaw_still_eep_min=0;
                }
            }
            else
            {
                yaw_ofst_min_init_flg=1;
                yaw_ofst_min_ok_flg=0;
                yaw_still_eep_min=0;
            }
            yaw_ofst_min_rd_flg=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

	}
    else
    {
    	;
    }
    
    #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)    
    if(PRESS_M.u1premEEPOfsReadEnd==0)
    {
        if(WE_u8ReadEEP_Byte(0x1ca,&com_rx_buf[0],4)==1)
        {
            if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
            {
                PRESS_M.s16premEEPOfsRead = *(int16_t*)&com_rx_buf[0];
                
                if(McrAbs(PRESS_M.s16premEEPOfsRead) <= PRESSURE_EEP_OFFSET_MAX)  
                {
                    PRESS_M.u1premValueInitReq=0;
                    PRESS_M.u1premEEPOfsReadOK=1;
                }
                else
                {
                    PRESS_M.u1premValueInitReq=1;    // limit 벗어나면 초기화
                    PRESS_M.u1premEEPOfsReadOK=0;
                    PRESS_M.s16premEEPOfsRead=0;
                }
                
                PRESS_M.u1premEEPOfsReadOK=1;
            }
            else
            {
                PRESS_M.u1premValueInitReq=1;    /*/ 값 초기화 요청*/
                PRESS_M.u1premEEPOfsReadOK=0;
                PRESS_M.s16premEEPOfsRead=0;
            }
            PRESS_M.u1premEEPOfsReadEnd=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }    
        #if ( __FL_PRESS==ENABLE )
        if(PRESS_FL.u1premEEPOfsReadEnd==0)
        {
            if(WE_u8ReadEEP_Byte(0x1ce,&com_rx_buf[0],4)==1)
            {
                if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
                {
                    PRESS_FL.s16premEEPOfsRead = *(int16_t*)&com_rx_buf[0];
                    if(McrAbs(PRESS_FL.s16premEEPOfsRead) <= PRESSURE_EEP_OFFSET_MAX)  
                    {
                        PRESS_FL.u1premValueInitReq=0;
                        PRESS_FL.u1premEEPOfsReadOK=1;
                    }
                    else
                    {
                        PRESS_FL.u1premValueInitReq=1;    /*/ limit 벗어나면 초기화*/
                        PRESS_FL.u1premEEPOfsReadOK=0;
                        PRESS_FL.s16premEEPOfsRead=0;
                    }
                }
                else
                {
                    PRESS_FL.u1premValueInitReq=1;    /*/ 값 초기화 요청*/
                    PRESS_FL.u1premEEPOfsReadOK=0;
                    PRESS_FL.s16premEEPOfsRead=0;
                }
                PRESS_FL.u1premEEPOfsReadEnd=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }        
        #endif
        
        #if ( __FR_PRESS==ENABLE )
        if(PRESS_FR.u1premEEPOfsReadEnd==0)
        {
            if(WE_u8ReadEEP_Byte(0x1d2,&com_rx_buf[0],4)==1)
            {
                if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
                {
                    PRESS_FR.s16premEEPOfsRead = *(int16_t*)&com_rx_buf[0];
                    if(McrAbs(PRESS_FR.s16premEEPOfsRead) <= PRESSURE_EEP_OFFSET_MAX)  
                    {
                        PRESS_FR.u1premValueInitReq=0;
                        PRESS_FR.u1premEEPOfsReadOK=1;
                    }
                    else
                    {
                        PRESS_FR.u1premValueInitReq=1;    /*/ limit 벗어나면 초기화*/
                        PRESS_FR.u1premEEPOfsReadOK=0;
                        PRESS_FR.s16premEEPOfsRead=0;
                    }
                }
                else
                {
                    PRESS_FR.u1premValueInitReq=1;    /*/ 값 초기화 요청*/
                    PRESS_FR.u1premEEPOfsReadOK=0;
                    PRESS_FR.s16premEEPOfsRead=0;
                }
                PRESS_FR.u1premEEPOfsReadEnd=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }         
        #endif
           
        #if ( __RL_PRESS==ENABLE )
        if(PRESS_RL.u1premEEPOfsReadEnd==0)
        {
            if(WE_u8ReadEEP_Byte(0x1d6,&com_rx_buf[0],4)==1)
            {
                if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
                {
                    PRESS_RL.s16premEEPOfsRead = *(int16_t*)&com_rx_buf[0];
                    if(McrAbs(PRESS_RL.s16premEEPOfsRead) <= PRESSURE_EEP_OFFSET_MAX)  
                    {
                        PRESS_RL.u1premValueInitReq=0;
                        PRESS_RL.u1premEEPOfsReadOK=1;
                    }
                    else
                    {
                        PRESS_RL.u1premValueInitReq=1;    /*/ limit 벗어나면 초기화*/
                        PRESS_RL.u1premEEPOfsReadOK=0;
                        PRESS_RL.s16premEEPOfsRead=0;
                    }
                }
                else
                {
                    PRESS_RL.u1premValueInitReq=1;    /*/ 값 초기화 요청*/
                    PRESS_RL.u1premEEPOfsReadOK=0;
                    PRESS_RL.s16premEEPOfsRead=0;
                }
                PRESS_RL.u1premEEPOfsReadEnd=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }        
        #endif        
        
        #if ( __RR_PRESS==ENABLE )
        if(PRESS_RR.u1premEEPOfsReadEnd==0)
        {
            if(WE_u8ReadEEP_Byte(0x1da,&com_rx_buf[0],4)==1)
            {
                if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
                {
                    PRESS_RR.s16premEEPOfsRead = *(int16_t*)&com_rx_buf[0];
                    if(McrAbs(PRESS_RR.s16premEEPOfsRead) <= PRESSURE_EEP_OFFSET_MAX)  
                    {
                        PRESS_RR.u1premValueInitReq=0;
                        PRESS_RR.u1premEEPOfsReadOK=1;
                    }
                    else
                    {
                        PRESS_RR.u1premValueInitReq=1;    /*/ limit 벗어나면 초기화*/
                        PRESS_RR.u1premEEPOfsReadOK=0;
                        PRESS_RR.s16premEEPOfsRead=0;
                    }
                }
                else
                {
                    PRESS_RR.u1premValueInitReq=1;    /*/ 값 초기화 요청*/
                    PRESS_RR.u1premEEPOfsReadOK=0;
                    PRESS_RR.s16premEEPOfsRead=0;
                }
                PRESS_RR.u1premEEPOfsReadEnd=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }    
        #endif                
    
    #endif
    
}

void LSESP_vWriteEEPROMSensorOffset(void)
{

	if((str_value_init_rq_flg==1)||(yaw_value_init_rq_flg==1)||(lg_value_init_rq_flg==1)||(fs_yaw_init_rq_flg==1)
		||(KSTEER.NEED_TO_WR_OFFSET_FLG==1)||(KYAW.NEED_TO_WR_OFFSET_FLG==1)||(KLAT.NEED_TO_WR_OFFSET_FLG==1)||(req_yaw_eeprom_write_flg==1)
		||(KYAW.eep_max_w_flg==1)||(KYAW.eep_min_w_flg==1)||(KLAT.eep_max_w_flg==1)||(KLAT.eep_min_w_flg==1)
		||(KSTEER.eep_max_w_flg==1)||(KSTEER.eep_min_w_flg==1)||(yaw_ofst_max_wr_flg==1)||(yaw_ofst_min_wr_flg==1)
		||(KYAW.eep_max_init_flg==1)||(KYAW.eep_min_init_flg==1)||(KSTEER.eep_max_init_flg==1)||(KSTEER.eep_min_init_flg==1)
		||(KLAT.eep_max_init_flg==1)||(KLAT.eep_min_init_flg==1)||(yaw_ofst_max_init_flg==1)||(yaw_ofst_min_init_flg==1)
		||(lsespu1EEPYawStandWrtReq==1)||(lsespu1EEPYawStandOfsInitReq==1)
		#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
		    ||(PRESS_M.u1premNeedToWriteOffset==1)
		    #if ( __FL_PRESS==ENABLE )
		    || (PRESS_FL.u1premNeedToWriteOffset==1)
		    #endif
		    #if ( __FR_PRESS==ENABLE )
		    || (PRESS_FR.u1premNeedToWriteOffset==1)
		    #endif
		    #if ( __RL_PRESS==ENABLE )
		    ||(PRESS_RL.u1premNeedToWriteOffset==1)
		    #endif
		    #if ( __RR_PRESS==ENABLE )
		    || (PRESS_RR.u1premNeedToWriteOffset==1)       				
		    #endif
		#endif  
		) 
	{
		write_sensor_offset_eeprom_flg=1;
	}
	else
	{
		write_sensor_offset_eeprom_flg=0;
	}

	if(write_sensor_offset_eeprom_flg==1)
	{
		if((weu1EraseSector==0)&&(weu1WriteSector==0))
		{
			if(KSTEER.NEED_TO_WR_OFFSET_FLG==0 && str_value_init_rq_flg==1)
			{
			    str_value_init_rq_flg=0;
			    KSTEER.model_offset=0;
			    KSTEER.NEED_TO_WR_OFFSET_FLG=1;
			}
			else
			{
				;
			}

			if(KYAW.NEED_TO_WR_OFFSET_FLG==0 && yaw_value_init_rq_flg==1)
			{
			    yaw_value_init_rq_flg=0;
			    KYAW.model_offset=0;
			    KYAW.NEED_TO_WR_OFFSET_FLG=1;
			}
			else
			{
				;
			}

			if(KLAT.NEED_TO_WR_OFFSET_FLG==0 && lg_value_init_rq_flg==1)
			{
			    lg_value_init_rq_flg=0;
			    KLAT.model_offset=0;
			    KLAT.NEED_TO_WR_OFFSET_FLG=1;
			}
			else
			{
				;
			}

			if(req_yaw_eeprom_write_flg==0 && fs_yaw_init_rq_flg==1)
			{
				fs_yaw_init_rq_flg=0;
			    read_yaw_eeprom_offset=0;
			    req_yaw_eeprom_write_flg=1;
			}
			else
			{
				;
			}

			if(KSTEER.eep_max_w_flg==0 && KSTEER.eep_max_init_flg==1)
			{
			    KSTEER.eep_max_init_flg=0;
			    KSTEER.eep_max_w=0;
			    KSTEER.eep_max_w_flg=1;
			}
			else
			{
				;
			}
			if(KSTEER.eep_min_w_flg==0 && KSTEER.eep_min_init_flg==1)
			{
			    KSTEER.eep_min_init_flg=0;
			    KSTEER.eep_min_w=0;
			    KSTEER.eep_min_w_flg=1;
			}
			else
			{
				;
			}
			if(KYAW.eep_max_w_flg==0 && KYAW.eep_max_init_flg==1)
			{
			    KYAW.eep_max_init_flg=0;
			    KYAW.eep_max_w=0;
			    KYAW.eep_max_w_flg=1;
			}
			else
			{
				;
			}
			if(KYAW.eep_min_w_flg==0 && KYAW.eep_min_init_flg==1)
			{
			    KYAW.eep_min_init_flg=0;
			    KYAW.eep_min_w=0;
			    KYAW.eep_min_w_flg=1;
			}
			else
			{
				;
			}
			if(KLAT.eep_max_w_flg==0 && KLAT.eep_max_init_flg==1)
			{
			    KLAT.eep_max_init_flg=0;
			    KLAT.eep_max_w=0;
			    KLAT.eep_max_w_flg=1;
			}
			else
			{
				;
			}
			if(KLAT.eep_min_w_flg==0 && KLAT.eep_min_init_flg==1)
			{
			    KLAT.eep_min_init_flg=0;
			    KLAT.eep_min_w=0;
			    KLAT.eep_min_w_flg=1;
			}
			else
			{
				;
			}
			if(yaw_ofst_max_wr_flg==0 && yaw_ofst_max_init_flg==1)
			{
				yaw_ofst_max_init_flg=0;
			    yaw_still_eep_max_w=0;
			    yaw_ofst_max_wr_flg=1;
			}
			else
			{
				;
			}
			if(yaw_ofst_min_wr_flg==0 && yaw_ofst_min_init_flg==1)
			{
				yaw_ofst_min_init_flg=0;
			    yaw_still_eep_min_w=0;
			    yaw_ofst_min_wr_flg=1;
			}
			else
			{
				;
			}

			if( (lsespu1EEPYawStandWrtReq==0) && (lsespu1EEPYawStandOfsInitReq==1) )
			{
			    lsespu1EEPYawStandOfsInitReq=0;
			    lsesps16YawStandOffset=0;
			    lsespu1EEPYawStandWrtReq=1;
			}
			else
			{
				;
			}            


            #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
            if((PRESS_M.u1premNeedToWriteOffset==0) && (PRESS_M.u1premValueInitReq==1) )
            {
            	PRESS_M.u1premValueInitReq=0;
            	PRESS_M.s16premDrvOffset=0;
            	PRESS_M.u1premNeedToWriteOffset=1;
            }
            else
            {
            	;
            }	
                #if ( __FL_PRESS==ENABLE )
                if((PRESS_FL.u1premNeedToWriteOffset==0) && (PRESS_FL.u1premValueInitReq==1) )
                {
                	PRESS_FL.u1premValueInitReq=0;
                	PRESS_FL.s16premDrvOffset=0;
                	PRESS_FL.u1premNeedToWriteOffset=1;
                }
                else
                {
                	;
                }	            
                #endif
                
                #if ( __FR_PRESS==ENABLE )                
                if((PRESS_FR.u1premNeedToWriteOffset==0) && (PRESS_FR.u1premValueInitReq==1) )
                {
                	PRESS_FR.u1premValueInitReq=0;
                	PRESS_FR.s16premDrvOffset=0;
                	PRESS_FR.u1premNeedToWriteOffset=1;
                }
                else
                {
                	;
                }	               
                #endif
                
                #if ( __RL_PRESS==ENABLE )
                if((PRESS_RL.u1premNeedToWriteOffset==0) && (PRESS_RL.u1premValueInitReq==1) )
                {
                	PRESS_RL.u1premValueInitReq=0;
                	PRESS_RL.s16premDrvOffset=0;
                	PRESS_RL.u1premNeedToWriteOffset=1;
                }
                else
                {
                	;
                }	                  
                #endif
                
                #if ( __RR_PRESS==ENABLE )
                if((PRESS_RR.u1premNeedToWriteOffset==0) && (PRESS_RR.u1premValueInitReq==1) )
                {
                	PRESS_RR.u1premValueInitReq=0;
                	PRESS_RR.s16premDrvOffset=0;
                	PRESS_RR.u1premNeedToWriteOffset=1;
                }
                else
                {
                	;
                }	                        
                #endif
                
            #endif
            
            
			if(KSTEER.NEED_TO_WR_OFFSET_FLG==1)
			{
			    if(LSESP_u16WriteEEPROM(0x180,(uint16_t)KSTEER.model_offset)==0)
			    {
			    	KSTEER.NEED_TO_WR_OFFSET_FLG=0;
			    }
			    else
				{
					;
				}
			}
			else if(KYAW.NEED_TO_WR_OFFSET_FLG==1)
			{
			    if(LSESP_u16WriteEEPROM(0x184,(uint16_t)KYAW.model_offset)==0)
			    {
			    	KYAW.NEED_TO_WR_OFFSET_FLG=0;
			    }
			    else
				{
					;
				}
			}
			else if(KLAT.NEED_TO_WR_OFFSET_FLG==1)
			{
			    if(LSESP_u16WriteEEPROM(0x188,(uint16_t)KLAT.model_offset)==0)
			    {
			    	KLAT.NEED_TO_WR_OFFSET_FLG=0;
			    }
			    else
				{
					;
				}
			}
			else if(req_yaw_eeprom_write_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x18c,(uint16_t)yaw_eeprom_offset_write)==0)
			    {
			    	req_yaw_eeprom_write_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KYAW.eep_max_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x190,(uint16_t)KYAW.eep_max_w)==0)
			    {
			    	KYAW.eep_max_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KYAW.eep_min_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x194,(uint16_t)KYAW.eep_min_w)==0)
			    {
			    	KYAW.eep_min_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KSTEER.eep_max_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x198,(uint16_t)KSTEER.eep_max_w)==0)
			    {
			    	KSTEER.eep_max_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KSTEER.eep_min_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x19c,(uint16_t)KSTEER.eep_min_w)==0)
			    {
			    	KSTEER.eep_min_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KLAT.eep_max_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x1a0,(uint16_t)KLAT.eep_max_w)==0)
			    {
			    	KLAT.eep_max_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(KLAT.eep_min_w_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x1a4,(uint16_t)KLAT.eep_min_w)==0)
			    {
			    	KLAT.eep_min_w_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(yaw_ofst_max_wr_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x1a8,(uint16_t)yaw_still_eep_max_w)==0)
			    {
			    	yaw_ofst_max_wr_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(yaw_ofst_min_wr_flg==1)
			{
			    if(LSESP_u16WriteEEPROM(0x1ac,(uint16_t)yaw_still_eep_min_w)==0)
			    {
			    	yaw_ofst_min_wr_flg=0;
			    }
			    else
				{
					;
				}
			}
			else if(lsespu1EEPYawStandWrtReq==1)
			{
			    if(LSESP_u16WriteEEPROM(0x1c6,(uint16_t)lsesps16YawStandOffset)==0)
			    {
			    	lsespu1EEPYawStandWrtReq=0;
			    }
			    else
				{
					;
				}
			}
            #if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
            else if(PRESS_M.u1premNeedToWriteOffset==1)
            {
			    if(LSESP_u16WriteEEPROM(0x1ca,(uint16_t)PRESS_M.s16premDrvOffset)==0)
			    {
			    	PRESS_M.u1premNeedToWriteOffset=0;
			    }
			    else
				{
					;
				}            	
            }
            #if ( __FL_PRESS==ENABLE )
            else if(PRESS_FL.u1premNeedToWriteOffset==1)
            {
			    if(LSESP_u16WriteEEPROM(0x1ce,(uint16_t)PRESS_FL.s16premDrvOffset)==0)
			    {
			    	PRESS_FL.u1premNeedToWriteOffset=0;
			    }
			    else
				{
					;
				}            	
            }            
            #endif
            
            #if ( __FR_PRESS==ENABLE )
            else if(PRESS_FR.u1premNeedToWriteOffset==1)
            {
			    if(LSESP_u16WriteEEPROM(0x1d2,(uint16_t)PRESS_FR.s16premDrvOffset)==0)
			    {
			    	PRESS_FR.u1premNeedToWriteOffset=0;
			    }
			    else
				{
					;
				}            	
            }       
            #endif
            
            #if ( __RL_PRESS==ENABLE )  
            else if(PRESS_RL.u1premNeedToWriteOffset==1)
            {
			    if(LSESP_u16WriteEEPROM(0x1d6,(uint16_t)PRESS_RL.s16premDrvOffset)==0)
			    {
			    	PRESS_RL.u1premNeedToWriteOffset=0;
			    }
			    else
				{
					;
				}            	
            }       
            #endif
            
            #if ( __RR_PRESS==ENABLE )
            else if(PRESS_RR.u1premNeedToWriteOffset==1)
            {
			    if(LSESP_u16WriteEEPROM(0x1da,(uint16_t)PRESS_RR.s16premDrvOffset)==0)
			    {
			    	PRESS_RR.u1premNeedToWriteOffset=0;
			    }
			    else
				{
					;
				}            	
            }           
            #endif                    
            #endif

			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
#if __YAW_TEMPERATURE_COMPENSATION

	LSESP_vSaveYawTmpMap();


/*	if((write_sensor_offset_eeprom_flg==0)&&(write_yaw_tmp_map_eeprom_flg==0)&&(init_yaw_tmp_map_eeprom_flg==0)&&(wbu1YawSNUnmatchFlg==0))  */
	if((write_sensor_offset_eeprom_flg==0)&&(write_yaw_tmp_map_eeprom_flg==0)&&(init_yaw_tmp_map_eeprom_flg==0) )
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
	#if __TEST_MODE_YAW_TMP_SRCH
	if(__TEST_MODE_YAW_TMP_SRCH==1)
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
	#endif
#else
	if(write_sensor_offset_eeprom_flg==0)
	{
		fs_vcc_off_flg=1;
	}
	else
	{
		;
	}
#endif

#if __ERASE_YAW_TMP_MAP

	fs_vcc_off_flg=0;

	LSESP_vEraseYawTmpMap();

	fs_vcc_off_flg=1;

#endif

}


#endif
/* Vehicle Stand state Detection and Yaw-rate stand offset Calculation */
static void LSESP_vDctStandState(void)
{   
	/*----- Detect Condition 1 */
	
	if( (FL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (FR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) &&
		(RL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (RR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) &&
		(fu1WheelFLErrDet==0) && (fu1WheelFRErrDet==0) && (fu1WheelRLErrDet==0) && (fu1WheelRRErrDet==0)&&
		(fu1YawSusDet==0)&& (fu1YawErrorDet==0)&& (fu1YawSenPwr1sOk==1)  )
	{
	    stop_state_flag=1;
	}
	else
	{
		stop_state_flag=0;
	}
    
    /*----- Detect Condition 2: Delay 1 minute after ABS control  */
    
    if(ABS_fz==1)
    {
        lsespu16ABSfz1MinAfterCount=L_U16_TIME_10MSLOOP_1MIN;	
    }
    else
    {
        if(lsespu16ABSfz1MinAfterCount>0)
        {
            lsespu16ABSfz1MinAfterCount=lsespu16ABSfz1MinAfterCount-1;
        }
        else
        {
      	    lsespu16ABSfz1MinAfterCount=0;;
        }    
    }
    
    /*----- Detect Condition 3: Stable Check , Dyaw rate change  Similar to FS condition */
    
    if( (lsespu1YawTurnTableStop==1) && (lsespu16ABSfz1MinAfterCount==0)  && (lsespu1YawStableDctDelayFlag==0))
    {
    	if(lsespu8DeltaYawrateCnt>L_U8_TIME_10MSLOOP_250MS)
    	{
    	    lsespu8DeltaYawrateCnt=0;
    	    lsesps16DeltaYawrate250mMean=(int16_t)(lsesps32DeltaYawrateSum/L_U8_TIME_10MSLOOP_250MS);	
    	    lsesps32DeltaYawrateSum=0;
    	  
    	    if(lsespu1Yaw250meanCheck==0)
    	    {
    	  	    lsesps16DeltaYawrate250mMeanMax=lsesps16DeltaYawrate250mMean;
    	  	    lsesps16DeltaYawrate250mMeanMin=lsesps16DeltaYawrate250mMean;
    	  	    lsespu1Yaw250meanCheck=1;
    	    }
    	    else
    	    {
    	  	    if(lsesps16DeltaYawrate250mMean>lsesps16DeltaYawrate250mMeanMax)
    	  	    {
    	  	        lsesps16DeltaYawrate250mMeanMax=lsesps16DeltaYawrate250mMean;
    	  	    }
    	  	    else if(lsesps16DeltaYawrate250mMean< lsesps16DeltaYawrate250mMeanMin )
    	  	    {
    	  	        lsesps16DeltaYawrate250mMeanMin=lsesps16DeltaYawrate250mMean;
    	  	    }
    	  	    else
    	  	    {
    	  	        ;
    	  	    }    	  	
    	    }
    	  
    	    lsesps16DeltaYawrate250mDiff=lsesps16DeltaYawrate250mMeanMax-lsesps16DeltaYawrate250mMeanMin;
    	  
    	    if(lsesps16DeltaYawrate250mDiff < YAW_1DEG )
    	    {    	  	
    	  	    if(lsespu8DeltaYawrateStableCnt<7) /* 2sec */
    	  	    {
    	  	        lsespu8DeltaYawrateStableCnt=lsespu8DeltaYawrateStableCnt+1;
    	  	        lsespu1YawSignalStableFlag=0;
    	  	    }
    	  	    else
    	  	    {
    	  	        lsespu1YawSignalStableFlag=1;
    	  	    }
    	    }
    	    else
    	    {
    	    	if(lsespu1YawSignalStableFlag==1)
    	    	{
    	    		lsespu1YawStableDctDelayFlag=1;
    	    	}
    	    	else
    	    	{
    	    		;
    	    	}
    	  	    lsespu8DeltaYawrateStableCnt=0;
    	  	    lsespu1YawSignalStableFlag=0;
    	  	    lsesps16DeltaYawrate250mMeanMax=lsesps16DeltaYawrate250mMean;
    	  	    lsesps16DeltaYawrate250mMeanMin=lsesps16DeltaYawrate250mMean;
    	    }    	    	      	     	
    	}
    	else
    	{
    	    lsespu8DeltaYawrateCnt=lsespu8DeltaYawrateCnt+1;
    	    lsesps32DeltaYawrateSum=lsesps32DeltaYawrateSum+yaw_1_100deg2;
    	}
    }
    else
    {
    	if(vref_resol_change>VREF_5_KPH_RESOL_CHANGE )
    	{
    		lsespu1YawStableDctDelayFlag=0;
    	}
    	else
    	{
    	    ; 	
    	}
    	lsespu8DeltaYawrateCnt=0;
    	lsesps32DeltaYawrateSum=0;
    	lsespu1Yaw250meanCheck=0;
    	lsesps16DeltaYawrate250mMeanMax=0;
    	lsesps16DeltaYawrate250mMeanMin=0;
    	lsesps16DeltaYawrate250mDiff=0;
    	lsespu8DeltaYawrateStableCnt=0;
    	lsespu1YawSignalStableFlag=0;    	
    }
    
    
    /*----- Detect Condition 4: TurnTable Detect  */                
    LSESP_vChkTurnOnStandstate();
    
    /*----- Standstate Detection   */ 
    if( ((yaw_turn_table_sus_flg==0) && (yawlg_stop_1sec_flag==1))  && (lsespu1YawSignalStableFlag==1) && (stop_state_flag==1) )
    {
    	lsespu1YawGstandstillflg=1;
    }
    else
    {
    	lsespu1YawGstandstillflg=0;
    }
    	
}

static void LSESP_vCalStandYawOffset(void)
{		
	int16_t  S16YawStandOffset;
	int16_t  S16YawStandOfsEEPOfsDiff;
	int16_t  S16YawSpecLimit;	
		
	if((lsespu1YawGstandstillflg==1) && ( McrAbs(KYAW.raw_signal) <  (YAW_OFFSET_FS_ERR_DCT + YAW_1DEG ) ) )
	{
		if(lsespu8YawStandOffsetCalcCNT >= L_U8_TIME_10MSLOOP_1000MS )
		{
			lsespu8YawStandOffsetCalcCNT=0;
			lsesps16YawStandOffset1secMean= (int16_t)(lsesps32YawStandOffset1secSum/L_U8_TIME_10MSLOOP_1000MS);
			lsesps32YawStandOffset1secSum=0;
			
			lsespu1YawStandOffsCalIGN1st=1;
			
			if(lsespu1YawStandOffsCalState==1)
			{
				lsesps16YawStandOffset=LCESP_s16Lpf1Int(lsesps16YawStandOffset1secMean,lsesps16YawStandOffset, L_U8FILTER_GAIN_10MSLOOP_3HZ );
			}
			else
			{
				lsesps16YawStandOffset = lsesps16YawStandOffset1secMean;
				lsespu1YawStandOffsCalState=1;
			}
			
		}
		else
		{
			lsespu8YawStandOffsetCalcCNT=lsespu8YawStandOffsetCalcCNT+1 ;	
			lsesps32YawStandOffset1secSum=lsesps32YawStandOffset1secSum+yaw_1_100deg2;		
		}
	}
	else
	{
		if(lsespu1YawStableDctDelayFlag==1)
		{
			lsesps16YawStandOffset=lsesps16YawStandOffsetOld;
		}
		else
		{
			;
		}
			
        if(lsespu1YawStandOffsCalState==1)
		{
		 	if(lsespu8YawStandCNT==0)
		  	{
		   		lsesps16YawStandOffsetOld= 0;
		   		lsespu8YawStandCNT=lsespu8YawStandCNT+1;
		   	}
		   	else if(lsespu8YawStandCNT==1)
		  	{
		   		lsesps16YawStandOffsetOld= lsesps16YawStandOffset;
		   		lsespu8YawStandCNT=lsespu8YawStandCNT+1;
		   	}
		   	else if(lsespu8YawStandCNT==2)
		   	{
		   		lsesps16YawStandOffsetOld1=lsesps16YawStandOffsetOld;
		   		lsesps16YawStandOffsetOld =lsesps16YawStandOffset;
		   		lsespu8YawStandCNT=lsespu8YawStandCNT+1;
		   	}
		   	else
		   	{
		   		lsesps16YawStandOffsetOld2=lsesps16YawStandOffsetOld1;		    		
		   		lsesps16YawStandOffsetOld1=lsesps16YawStandOffsetOld;
		   		lsesps16YawStandOffsetOld =lsesps16YawStandOffset;	
		   		lsespu8YawStandCNT=3;    				    		
		   	}		    	
			    
		}
		else
		{
		    ;
		}				

	    lsespu8YawStandOffsetCalcCNT=0;	
	    lsesps32YawStandOffset1secSum=0;
	    lsesps16YawStandOffset1secMean=0;
	    lsespu1YawStandOffsCalState=0;
	}
	
	
	lsesps16YawStandOffset = LCABS_s16LimitMinMax(lsesps16YawStandOffset , (-YAW_OFFSET_FS_ERR_DCT) , YAW_OFFSET_FS_ERR_DCT  );
	
	
	/*========== Yaw offset suspect & threshold =================*/
	
    #if __YAW_TEMPERATURE_COMPENSATION ==1	
	    S16YawSpecLimit = lsesps16YawTmpLimit[lsesps8SrchYawTmpMapIndex] ;
	#else
	    S16YawSpecLimit = YAW_OFFSET_MAX_ERR ;  
	#endif
	
	S16YawStandOffset = McrAbs(lsesps16YawStandOffset);

	
	if(lsespu1YawSpecOverSus == 1)
	{
        if( S16YawStandOffset <= S16YawSpecLimit )
		{
			lsespu1YawSpecOverSus  = 0;		
			lsesps16YawSpcOvrThres = 0;	
		}
		else
		{
			;
		}				
	}
	else
	{
		if( S16YawStandOffset > S16YawSpecLimit )
		{
			lsespu1YawSpecOverSus     = 1;	
			lsesps16YawSpcOvrThres = ( S16YawStandOffset -  S16YawSpecLimit );				
		}
		else
		{
			;
		}		
	}			
		
}



static void LSESP_vChkTurnOnStandstate(void)
{

    #if __YAW_TEMPERATURE_COMPENSATION==1
	LSESP_vLimitYawTmpMap();
    #endif

    if(lsespu1YawTurnTableStop==1)
    {
    	if(((FL.lsabss16WhlSpdRawSignal64 >= VREF_0_25_KPH_RESOL_CHANGE ) && (FR.lsabss16WhlSpdRawSignal64 >= VREF_0_25_KPH_RESOL_CHANGE ) &&
		   (RL.lsabss16WhlSpdRawSignal64 >= VREF_0_25_KPH_RESOL_CHANGE ) && (RR.lsabss16WhlSpdRawSignal64 >= VREF_0_25_KPH_RESOL_CHANGE ) &&
		   (fu1WheelFLErrDet==0) && (fu1WheelFRErrDet==0) && (fu1WheelRLErrDet==0) && (fu1WheelRRErrDet==0)) 
		   || ( vref > VREF_1_KPH) )
    	{
    		lsespu1YawTurnTableStop= 0;
    	}
    	else
    	{
    		;
    	}    	
    }
    else
    {
    	if((FL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (FR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) &&
		   (RL.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) && (RR.lsabss16WhlSpdRawSignal64 <= VREF_0G125_KPH_RESOL_CHANGE ) &&
		   (fu1WheelFLErrDet==0) && (fu1WheelFRErrDet==0) && (fu1WheelRLErrDet==0) && (fu1WheelRRErrDet==0)&&
		   (fu1YawSusDet==0)&& (fu1YawErrorDet==0)&& (fu1YawSenPwr1sOk==1)  )
    	{
    		lsespu1YawTurnTableStop= 1;
    	}
    	else
    	{
    		;
    	}
    }

	if( (lsespu1YawTurnTableStop==1) && (lsespu16ABSfz1MinAfterCount==0) )
	{
	  if(lsespu8Mean1secCnt>L_U8_TIME_10MSLOOP_1000MS)
	  {
		  lsesps16Yawrate1secMeanOld=lsesps16Yawrate1secMean;
		  lsesps16LatAccel1secMeanOld=lsesps16LatAccel1secMean;
		  lsesps16Yawrate1secMean=(int16_t)(lsesps32Yawrate1secSum/L_U8_TIME_10MSLOOP_1000MS);
		  lsesps16LatAccel1secMean=(int16_t)(lsesps32LatAccelsecSum/L_U8_TIME_10MSLOOP_1000MS);
		  
		  #if __YAW_TEMPERATURE_COMPENSATION==1
		  lsesps16YawTmp1SecMean=(int16_t)(lsesps32YawTmp1SecSum/L_U8_TIME_10MSLOOP_1000MS);
		  lsesps8YawTmp1SecIndex = (int8_t)((lsesps16YawTmp1SecMean/TMP_5_DC)+11);

		  if(lsesps8YawTmp1SecIndex<0)
		  {
			  lsesps8YawTmp1SecIndex=0;
		  }
		  else if(lsesps8YawTmp1SecIndex>30)
		  {
			  lsesps8YawTmp1SecIndex=30;
		  }
		  else
		  {
			  ;
		  }
          lsesps32YawTmp1SecSum=0;
          #endif
          
          
		  lsesps32Yawrate1secSum=0;
		  lsesps32LatAccelsecSum=0;		  
		  lsespu8Mean1secCnt=0;

		  if(yawlg_stop_1sec_flag==1)
		  {
		    lsesps16Yawrate1secSlope=lsesps16Yawrate1secMean  - lsesps16Yawrate1secMeanOld;
		    lsesps16LatAccelsecSlope=lsesps16LatAccel1secMean - lsesps16LatAccel1secMeanOld;
		  }
		  else
		  {
		  	lsesps16Yawrate1secSlope=0;
		  	lsesps16LatAccelsecSlope=0;
		  	yawlg_stop_1sec_flag=1;
		  }

	  }
	  else
	  {
		  lsespu8Mean1secCnt++;
		  lsesps32Yawrate1secSum+=yaw_1_100deg2;
		  lsesps32LatAccelsecSum+=a_lat_1_1000g2;
		  
		  #if __YAW_TEMPERATURE_COMPENSATION==1
 		  lsesps32YawTmp1SecSum+=yaw_lg_temp;
 		  #endif

	  }

	  /*- Yaw Temp Limitation Check --*/
	  if(yawlg_stop_1sec_flag==1)
	  {
	  	#if __YAW_TEMPERATURE_COMPENSATION==1
	    if( McrAbs(lsesps16Yawrate1secMean) >= lsesps16YawTmpLimit[lsesps8YawTmp1SecIndex])
	    {
	    	yaw_offset_tmp_over_flg=1;
	    	yaw_offset_tmp_over_hold_flg=1;

	    }
	    else
	    {
	    	yaw_offset_tmp_over_flg=0;
	    	yaw_offset_tmp_over_hold_flg=0;
	    }
	    #else  
	    if( McrAbs(lsesps16Yawrate1secMean) >=  YAW_OFFSET_MAX_ERR )
	    {
	    	yaw_offset_spec_over_flg=1;
	    }
	    else
	    {
	    	yaw_offset_spec_over_flg=0;
	    }	    
	    
	    #endif


	    if(McrAbs(lsesps16Yawrate1secSlope) >= YAW_0G5DEG)
	    {
	    	yaw_offset_slope_over_flg=1;
	    }
	    else
	    {
	    	yaw_offset_slope_over_flg=0;
	    }

        
		if( yaw_offset_slope_over_flg==1)  
		{
		    yaw_turn_table_sus_flg=1;
		}
		else
		{
		    ;
		}

	  }
	  else
	  {
	  	#if __YAW_TEMPERATURE_COMPENSATION==1
	  	yaw_offset_tmp_over_flg=0;
	  	#else
	  	yaw_offset_spec_over_flg=0;
	  	#endif
	  	
	  	yaw_offset_slope_over_flg=0;
	  	
	  }


	}
	else
	{
		lsesps16Yawrate1secMean=0;
		lsesps16LatAccel1secMean=0;		
		lsesps32Yawrate1secSum=0;
		lsesps32LatAccelsecSum=0;		
		lsesps16Yawrate1secSlope=0;
		lsesps16LatAccelsecSlope=0;
		yawlg_stop_1sec_flag=0;
	    yaw_offset_slope_over_flg=0;

	    lsespu8Mean1secCnt=0;
	    
	    #if __YAW_TEMPERATURE_COMPENSATION==1
	    lsesps16YawTmp1SecMean=0;
	    lsesps32YawTmp1SecSum=0;
   	    yaw_offset_tmp_over_flg=0;
   	    #else
   	    yaw_offset_spec_over_flg=0;
   	    #endif
   	    
		if( vref >= VREF_5_KPH )  
		{
		    yaw_turn_table_sus_flg=0;
		}
		else
		{
		    ;
		}      	    

	}

}



#if __ERASE_YAW_TMP_MAP
static void LSESP_vEraseYawTmpMap(void)
{
	uint8_t lsespuCntEraseTmpMap;

	for(lsespuCntEraseTmpMap=0;lsespuCntEraseTmpMap<=30;lsespuCntEraseTmpMap++)
	{
		weu16Eep_addr=lsespu16YawTmpAddress[lsespuCntEraseTmpMap];
		WE_vStartWriteEep(1);
	}
}
#endif

#if __YAW_TEMPERATURE_COMPENSATION

static void LSESP_vCalYawLgTemperature(void)
{
	yaw_lg_temp_old=yaw_lg_temp;
	/* YAW_G_SENSOR Temperature gradient */
	/* fcs8SenRasterFreqency:8bit, r:0.5Hz => (0.5*fcs8SenRasterFreqency)(Hz) */
	/* -0.42Hz/K => -2.38K/Hz, Temperature = RoomTemperature + (-1.19*fcs8SenRasterFreqency)(K) */
/*
	if(yaw_lg_raster_jump_flg==0)
	{
		yaw_lg_temp= TMP_23_DC-119*((int16_t)fcs8SenRasterFreqency);
	}
	else
	{
		;
	}
*/
#ifdef __SVDO_IMU
		yaw_lg_temp= TMP_23_DC-119*((int16_t)fcs8SenRasterFreqency);
#endif

	if((yaw_lg_temp>TMP_85_DC ) || ( yaw_lg_temp <TMP_M_40_DC))
	{
	   raster_tmp_spec_over_flg = 1;
	}
	else
	{
	   raster_tmp_spec_over_flg = 0;
	}

/*	if((cbit_process_step==1)||(yaw_lg_raster_jump_flg==1))  */
	if(cbit_process_step==1 )
	{
		yaw_lg_temp=yaw_lg_temp_old;
	}
	else if((McrAbs(yaw_lg_temp-yaw_lg_temp_old)>TMP_5_DC)&&(cyctime>=L_U16_TIME_10MSLOOP_5S))/*(cyctime<T_3_S)&&(fou1SenPwr1secOk==1)*/
	{
		yaw_lg_temp=yaw_lg_temp_old;
	}
	else
	{
		;
	}

	if(yaw_lg_temp_cnt<8192)
	{
		yaw_lg_temp_cnt++;
		yaw_lg_temp_sum+=yaw_lg_temp;
	}
	else
	{
		yaw_lg_temp_d_old=yaw_lg_temp_d;
		yaw_lg_temp_d=(int16_t)(yaw_lg_temp_sum>>13);/*r: 0.01'C / K/min*/
		yaw_lg_temp_d_flg=1;
		if((yaw_lg_temp_d_flg==1)&&(yaw_lg_temp_d_cnt<200))
		{
			yaw_lg_temp_d_cnt++;
		}
		else
		{
			;
		}
		yaw_lg_temp_dot_old=yaw_lg_temp_dot;
		yaw_lg_temp_dot=yaw_lg_temp_d-yaw_lg_temp_d_old;
		yaw_lg_temp_cnt=0;
		yaw_lg_temp_sum=0;
	}

	if(yaw_lg_temp_d_cnt>2)
	{
		yaw_lg_temp_dot2=yaw_lg_temp_dot;
	}
	else
	{
		yaw_lg_temp_dot2=0;
	}
}

static void LSESP_vCalTemperatureYawOffset(void)
{
	uint8_t lsespu8LoadYawMapCnt;

	/*Load Yaw-Tmp Map*/
	LSESP_vLoadYawTmpMap();

	/*Reset Yaw-Tmp Map if Sensor repair*/

	/* FailManagement */

  /* 2012.01.13 KJS mgh-80 qpr_yaw map */
//	if( ( (wbu1YawSNUnmatchFlg==1) && (yaw_reset_check_done_flg==0) ) || (fu1YawErrorDet==1) )
//	{
//		LSESP_vResetYawTmpMap();
//	}
//	else
//	{
//		;
//	}

	if( ( (wbu1YawSNUnmatchFlg==1) && (yaw_reset_check_done_flg==0) ) || ( (fu1YawErrorDet==1) && (yaw_reset_check_done_flg==0) ) )
	{
		LSESP_vResetYawTmpMap();
	}
	else
	{
		;
	}
  /* 2012.01.13 KJS mgh-80 qpr_yaw map */

	/*Check Deviation Within 1Sec*/

	LSESP_vChkTurnOnStandstill();

	if(cyctime<L_U16_TIME_10MSLOOP_5S)
	{
		if(yaw_lg_tmp_read_done_flg==1)
		{
			/*Update Current Standstill Offset*/
			/*LSESP_vChkCurrentStandstill();*/

			/*Determin Yaw Offset*/
			LSESP_vDetTemperatureYawOffset();
		}
		else
		{
			;
		}
	}
	else
	{
		if(yaw_lg_tmp_read_done_flg==0)
		{
			for(lsespu8LoadYawMapCnt=1;lsespu8LoadYawMapCnt<=29;lsespu8LoadYawMapCnt++)
			{
				lsesps16YawTmpMap[lsespu8LoadYawMapCnt] =lsesps16YawTmpEEPMap[lsespu8LoadYawMapCnt];
			}
			for(lsespu8LoadYawMapCnt=1;lsespu8LoadYawMapCnt<=29;lsespu8LoadYawMapCnt++)
			{
				lsespu8YawTmpOKFlg[lsespu8LoadYawMapCnt] =lsespu8YawTmpEEPOKFlg[lsespu8LoadYawMapCnt];
			}
			yaw_lg_tmp_read_done_flg=1;
		}
		else
		{
			;
		}

		/*Update Current Standstill Offset*/
		LSESP_vChkCurrentStandstill();

		/*Determin Yaw Offset*/
		LSESP_vDetTemperatureYawOffset();
	}

	LSESP_vChkYawOffset();

}


static void LSESP_vChkYawOffset(void)
{
	if(lsespu1YawSpecOverSus ==0)
	{
		if(lsespu1YawGstandstillflg ==1)
		{
	        /*Advised by Choi SH*/
	        if((lsespu1YawStandOffsCalIGN1st==1)&&(McrAbs(lsesps16YawOffsetByTmp-lsesps16YawStandOffset)>YAW_4DEG))  
	        {
	        	yaw_offset_srch_ok_flg=0;
	        }
	        else
	        {
	        	;
	        }
	        /*Advised by Choi SH*/
	        if((KYAW.driving_accuracy>=60)&&(McrAbs(lsesps16YawOffsetByTmp-KYAW.offset_60avr)>YAW_5DEG))
	        {
	        	yaw_offset_srch_ok_flg=0;
	        }
	        else
	        {
	        	;
	        }
	    }
	    else
	    {
	    	;
	    }
    }
    else
    {
    	;
    }
}

static void LSESP_vChkTurnOnStandstill(void)
{
	int16_t lsesps16Yawrate100msMeanOld;
	int16_t lsesps16LatAccel100msMeanOld;
	int16_t lsesps16Yawrate100msSlopeOld;
	int16_t lsesps16LatAccel100msSlopeOld;

	if(lsespu8Mean100msCnt>L_U8_TIME_10MSLOOP_100MS)
	{
		lsesps16Yawrate100msMeanOld=lsesps16Yawrate100msMean;
		lsesps16LatAccel100msMeanOld=lsesps16LatAccel100msMean;
		lsesps16Yawrate100msSlopeOld=lsesps16Yawrate100msSlope;
		lsesps16LatAccel100msSlopeOld=lsesps16LatAccel100msSlope;
		lsesps16Yawrate100msMean=(int16_t)(lsesps32Yawrate100msSum/L_U8_TIME_10MSLOOP_100MS);
		lsesps16LatAccel100msMean=(int16_t)(lsesps32LatAccel100msSum/L_U8_TIME_10MSLOOP_100MS);
		lsesps32Yawrate100msSum=0;
		lsesps32LatAccel100msSum=0;
		lsespu8Mean100msCnt=0;
		lsesps16Yawrate100msSlope=LCESP_s16Lpf1Int((lsesps16Yawrate100msMean-lsesps16Yawrate100msMeanOld), lsesps16Yawrate100msSlopeOld, L_U8FILTER_GAIN_10MSLOOP_10HZ);
		lsesps16LatAccel100msSlope=LCESP_s16Lpf1Int((lsesps16LatAccel100msMean-lsesps16LatAccel100msMeanOld), lsesps16LatAccel100msSlopeOld, L_U8FILTER_GAIN_10MSLOOP_10HZ);
	}
	else
	{
		lsespu8Mean100msCnt++;
		lsesps32Yawrate100msSum+=yaw_1_100deg2;
		lsesps32LatAccel100msSum+=a_lat_1_1000g2;

	}

	if(yaw_100ms_slope_ok_flg==1)
	{
		if(McrAbs(lsesps16Yawrate100msSlope)>YAW_0G2DEG)
		{
			yaw_100ms_slope_ok_flg=0;
		}
		else
		{
			;
		}
	}
	else
	{
		if(McrAbs(lsesps16Yawrate100msSlope)<=YAW_0G2DEG)
		{
			lsesps16Yawrate100msStableCnt++;
		}
		else
		{
			lsesps16Yawrate100msStableCnt=0;
		}

		if(lsesps16Yawrate100msStableCnt>L_U8_TIME_10MSLOOP_1000MS)
		{
			yaw_100ms_slope_ok_flg=1;
			lsesps16Yawrate100msStableCnt=0;
		}
		else
		{
			;
		}
	}

	if(lat_g_100ms_slope_ok_flg==1)
	{
		if(McrAbs(lsesps16LatAccel100msSlope)>LAT_0G01G)
		{
			lat_g_100ms_slope_ok_flg=0;
		}
		else
		{
			;
		}
	}
	else
	{
		if(McrAbs(lsesps16LatAccel100msSlope)<=LAT_0G01G)
		{
			lsesps16LatAccel100msStableCnt++;
		}
		else
		{
			lsesps16LatAccel100msStableCnt=0;
		}

		if(lsesps16LatAccel100msStableCnt>L_U8_TIME_10MSLOOP_1000MS)
		{
			lat_g_100ms_slope_ok_flg=1;
			lsesps16LatAccel100msStableCnt=0;
		}
		else
		{
			;
		}
	}
}

static void LSESP_vChkCurrentStandstill(void)
{	
	
	/* FailManagement */
	if((lsespu1YawGstandstillflg==1)&&(yaw_100ms_slope_ok_flg==1)&&(lat_g_100ms_slope_ok_flg==1)&&(fou1SenPwr1secOk==1)&&(fu1YawSusDet==0)&&(yaw_1_100deg2<YAW_5DEG)&&(yaw_1_100deg2>-YAW_5DEG) && (fu1YawErrorDet==0) && (wbu1YawSerialNOKFlg==1)&&
		 (yaw_offset_tmp_over_flg==0) && (yaw_offset_slope_over_flg==0) )
	{
		standstill_on_flg=1;
		if(lsespu8Yawrate1SecCnt>=L_U8_TIME_10MSLOOP_1000MS)
		{
			lsespu8Yawrate1SecCnt=0;

			lsesps16YawrateMean1Sec=(int16_t)(lsesps32YawrateSum1Sec/L_U8_TIME_10MSLOOP_1000MS);
			lsesps32YawrateSum1Sec=0;
			lsesps16YawTmpMean1Sec=(int16_t)(lsesps32YawTmpSum1Sec/L_U8_TIME_10MSLOOP_1000MS);
			lsesps32YawTmpSum1Sec=0;
			lsesps16YawOffsetPile[lsespu8YawOffsetPileIndex]=lsesps16YawrateMean1Sec;
			lsesps16YawTmpPile[lsespu8YawOffsetPileIndex]=lsesps16YawTmpMean1Sec;

			lsespu8YawOffsetPileIndex++;

			if(lsespu8YawOffsetPileIndex>9)
			{
				lsespu8YawOffsetPileIndex=lsespu8YawOffsetPileIndex-1;
				LSESP_vUpdateYawTmpMap();
				lsespu8YawOffsetPileIndex=1;
				lsesps16YawOffsetPile[0]=lsesps16YawOffsetPile[9];
				lsesps16YawTmpPile[0]=lsesps16YawTmpPile[9];
			}
			else
			{
				;
			}
		}
		else
		{
			lsespu8Yawrate1SecCnt++;
			lsesps32YawrateSum1Sec+=yaw_1_100deg2;
			lsesps32YawTmpSum1Sec+=yaw_lg_temp;
		}
	}
	else
	{
		lsespu8Yawrate1SecCnt=0;
		lsesps32YawrateSum1Sec=0;
		lsesps32YawTmpSum1Sec=0;

		if(standstill_on_flg)
		{
			if(lsespu8YawOffsetPileIndex>1)
			{
				lsespu8YawOffsetPileIndex=lsespu8YawOffsetPileIndex-1;
				LSESP_vUpdateYawTmpMap();
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
		standstill_on_flg=0;
		lsespu8YawOffsetPileIndex=0;
	}
}
#if TC27X_FEE==ENABLE
#else
static void LSESP_vAddressYawTmpMap(void)
{
	/*2006.02.10 Logic EEPROM 0xe0~0x1F0 by Kim K S*/
	/*2006.06.27 Logic EEPROM 0x200~0x2ff by Kim K S*/
	lsespu16YawTmpAddress[0] =0x200;
	lsespu16YawTmpAddress[1] =0x204;
	lsespu16YawTmpAddress[2] =0x208;
	lsespu16YawTmpAddress[3] =0x20c;
	lsespu16YawTmpAddress[4] =0x210;
	lsespu16YawTmpAddress[5] =0x214;
	lsespu16YawTmpAddress[6] =0x218;
	lsespu16YawTmpAddress[7] =0x21c;
	lsespu16YawTmpAddress[8] =0x220;
	lsespu16YawTmpAddress[9] =0x224;
	lsespu16YawTmpAddress[10]=0x228;
	lsespu16YawTmpAddress[11]=0x22c;
	lsespu16YawTmpAddress[12]=0x230;
	lsespu16YawTmpAddress[13]=0x234;
	lsespu16YawTmpAddress[14]=0x238;
	lsespu16YawTmpAddress[15]=0x23c;
	lsespu16YawTmpAddress[16]=0x240;
	lsespu16YawTmpAddress[17]=0x244;
	lsespu16YawTmpAddress[18]=0x248;
	lsespu16YawTmpAddress[19]=0x24c;
	lsespu16YawTmpAddress[20]=0x250;
	lsespu16YawTmpAddress[21]=0x254;
	lsespu16YawTmpAddress[22]=0x258;
	lsespu16YawTmpAddress[23]=0x25c;
	lsespu16YawTmpAddress[24]=0x260;
	lsespu16YawTmpAddress[25]=0x264;
	lsespu16YawTmpAddress[26]=0x268;
	lsespu16YawTmpAddress[27]=0x26c;
	lsespu16YawTmpAddress[28]=0x270;
	lsespu16YawTmpAddress[29]=0x274;
	lsespu16YawTmpAddress[30]=0x278;

}
#endif
static void LSESP_vResetYawTmpMap(void)
{
	uint8_t lsespu8ResetYawTmpCnt;

	for(lsespu8ResetYawTmpCnt=0;lsespu8ResetYawTmpCnt<=30;lsespu8ResetYawTmpCnt++)
	{
		lsesps16YawTmpMap[lsespu8ResetYawTmpCnt] =0;
	}
	for(lsespu8ResetYawTmpCnt=0;lsespu8ResetYawTmpCnt<=30;lsespu8ResetYawTmpCnt++)
	{
		lsespu8YawTmpOKFlg[lsespu8ResetYawTmpCnt] =0;
	}
	for(lsespu8ResetYawTmpCnt=0;lsespu8ResetYawTmpCnt<=30;lsespu8ResetYawTmpCnt++)
	{
		lsespu8YawTmpInitRqFlg[lsespu8ResetYawTmpCnt] =1;
	}
	for(lsespu8ResetYawTmpCnt=0;lsespu8ResetYawTmpCnt<=30;lsespu8ResetYawTmpCnt++)
	{
		lsespu8YawTmpEEPOKFlg[lsespu8ResetYawTmpCnt] =0;
	}
	for(lsespu8ResetYawTmpCnt=0;lsespu8ResetYawTmpCnt<=30;lsespu8ResetYawTmpCnt++)
	{
		lsesps16YawTmpEEPMap[lsespu8ResetYawTmpCnt] =0;
	}


	yaw_lg_tmp_read_done_flg=1;

	yaw_reset_check_done_flg=1;
}
#if TC27X_FEE==ENABLE
static void LSESP_vLoadYawTmpMap(void)
{
	uint8_t lsespu8LoadYawTmpCnt;

	if(yaw_lg_tmp_read_done_flg==0)
	{
		for(lsespu8YawTmpReadIndex=1;lsespu8YawTmpReadIndex<=29;lsespu8YawTmpReadIndex++)
		{
			if(lsespu8YawTmpReadDoneFlg[lsespu8YawTmpReadIndex]==0)
			{
				if(FeeDataReadInvalidFlg==0)
				{
					if((uint32_t)NVM_YawTmpEEPMap[lsespu8YawTmpReadIndex]+(uint32_t)NVM_YawTmpEEPMap_c[lsespu8YawTmpReadIndex]==0xffffffff)
					{
						lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex] = (int16_t)NVM_YawTmpEEPMap[lsespu8YawTmpReadIndex];
					if(McrAbs(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]) <= YAW_4G5DEG)
					{
						lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;
						lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=1;
					}
					else
					{
						lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;    /*limitation*/
						lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=0;
						if(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]>YAW_4G5DEG)
						{
							lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=YAW_4G5DEG;
						}
						else if(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]<-YAW_4G5DEG)
						{
							lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=-YAW_4G5DEG;
						}
						else
						{
							;
						}

					}
				}
				else
				{
					lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;
					lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=0;
					lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=0;
				}
				lsespu8YawTmpReadDoneFlg[lsespu8YawTmpReadIndex]=1;
			}
			else
			{
				;
			}
		}
			else
			{
				;
			}
		}


		lsespu8YawTmpReadDoneFlg[0]=1;
		lsespu8YawTmpReadDoneFlg[30]=1;

		if(((lsespu8YawTmpReadDoneFlg[0]==1)&&(lsespu8YawTmpReadDoneFlg[1]==1)&&(lsespu8YawTmpReadDoneFlg[2]==1)&&(lsespu8YawTmpReadDoneFlg[3]==1)&&(lsespu8YawTmpReadDoneFlg[4]==1)&&(lsespu8YawTmpReadDoneFlg[5]==1)&&(lsespu8YawTmpReadDoneFlg[6]==1)&&(lsespu8YawTmpReadDoneFlg[7]==1)&&(lsespu8YawTmpReadDoneFlg[8]==1)&&(lsespu8YawTmpReadDoneFlg[9]==1)&&(lsespu8YawTmpReadDoneFlg[10]==1)&&(lsespu8YawTmpReadDoneFlg[11]==1)&&(lsespu8YawTmpReadDoneFlg[12]==1)&&(lsespu8YawTmpReadDoneFlg[13]==1)&&(lsespu8YawTmpReadDoneFlg[14]==1)&&(lsespu8YawTmpReadDoneFlg[15]==1)&&(lsespu8YawTmpReadDoneFlg[16]==1)&&(lsespu8YawTmpReadDoneFlg[17]==1)&&(lsespu8YawTmpReadDoneFlg[18]==1)&&(lsespu8YawTmpReadDoneFlg[19]==1)&&(lsespu8YawTmpReadDoneFlg[20]==1)&&(lsespu8YawTmpReadDoneFlg[21]==1)&&(lsespu8YawTmpReadDoneFlg[22]==1)&&(lsespu8YawTmpReadDoneFlg[23]==1)&&(lsespu8YawTmpReadDoneFlg[24]==1)&&(lsespu8YawTmpReadDoneFlg[25]==1)&&(lsespu8YawTmpReadDoneFlg[26]==1)&&(lsespu8YawTmpReadDoneFlg[27]==1)&&(lsespu8YawTmpReadDoneFlg[28]==1)&&(lsespu8YawTmpReadDoneFlg[29]==1)&&(lsespu8YawTmpReadDoneFlg[30]==1))==1)
		{
			yaw_lg_tmp_read_done_flg=1;

			for(lsespu8LoadYawTmpCnt=1;lsespu8LoadYawTmpCnt<=29;lsespu8LoadYawTmpCnt++)
			{
				lsesps16YawTmpMap[lsespu8LoadYawTmpCnt] =lsesps16YawTmpEEPMap[lsespu8LoadYawTmpCnt];
			}
			for(lsespu8LoadYawTmpCnt=1;lsespu8LoadYawTmpCnt<=29;lsespu8LoadYawTmpCnt++)
			{
				lsespu8YawTmpOKFlg[lsespu8LoadYawTmpCnt] =lsespu8YawTmpEEPOKFlg[lsespu8LoadYawTmpCnt];
			}
		}
		else
		{
			yaw_lg_tmp_read_done_flg=0;
		}
	}
	else
	{
		;
	}
}
#else
static void LSESP_vLoadYawTmpMap(void)
{
	uint8_t lsespu8LoadYawTmpCnt;

	if(yaw_lg_tmp_read_done_flg==0)
	{
		LSESP_vAddressYawTmpMap();
		

		for(lsespu8YawTmpReadIndex=1;lsespu8YawTmpReadIndex<=29;lsespu8YawTmpReadIndex++)
		{
			if(lsespu8YawTmpReadDoneFlg[lsespu8YawTmpReadIndex]==0)
			{
			    if(WE_u8ReadEEP_Byte(lsespu16YawTmpAddress[lsespu8YawTmpReadIndex],&com_rx_buf[0],4)==1)
			    {
			        if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff)
			        {
			            lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex] = *(int16_t*)&com_rx_buf[0];
			            if(McrAbs(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]) <= YAW_4G5DEG)
			            {
			                lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;
			                lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=1;
			            }
			            else
			            {
			                lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;    /*limitation*/
			                lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=0;
			                if(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]>YAW_4G5DEG)
			                {
			                	lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=YAW_4G5DEG;
			                }
			                else if(lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]<-YAW_4G5DEG)
			                {
			                	lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=-YAW_4G5DEG;
			                }
			                else
			                {
			                	;
			                }

			            }
			        }
			        else
			        {
			            lsespu8YawTmpInitRqFlg[lsespu8YawTmpReadIndex]=0;
			            lsespu8YawTmpEEPOKFlg[lsespu8YawTmpReadIndex]=0;
			            lsesps16YawTmpEEPMap[lsespu8YawTmpReadIndex]=0;
			        }
			        lsespu8YawTmpReadDoneFlg[lsespu8YawTmpReadIndex]=1;
			    }
			    else
			    {
			    	;
			    }
			}
			else
			{
				;
			}
		}


		lsespu8YawTmpReadDoneFlg[0]=1;
		lsespu8YawTmpReadDoneFlg[30]=1;

		if(((lsespu8YawTmpReadDoneFlg[0]==1)&&(lsespu8YawTmpReadDoneFlg[1]==1)&&(lsespu8YawTmpReadDoneFlg[2]==1)&&(lsespu8YawTmpReadDoneFlg[3]==1)&&(lsespu8YawTmpReadDoneFlg[4]==1)&&(lsespu8YawTmpReadDoneFlg[5]==1)&&(lsespu8YawTmpReadDoneFlg[6]==1)&&(lsespu8YawTmpReadDoneFlg[7]==1)&&(lsespu8YawTmpReadDoneFlg[8]==1)&&(lsespu8YawTmpReadDoneFlg[9]==1)&&(lsespu8YawTmpReadDoneFlg[10]==1)&&(lsespu8YawTmpReadDoneFlg[11]==1)&&(lsespu8YawTmpReadDoneFlg[12]==1)&&(lsespu8YawTmpReadDoneFlg[13]==1)&&(lsespu8YawTmpReadDoneFlg[14]==1)&&(lsespu8YawTmpReadDoneFlg[15]==1)&&(lsespu8YawTmpReadDoneFlg[16]==1)&&(lsespu8YawTmpReadDoneFlg[17]==1)&&(lsespu8YawTmpReadDoneFlg[18]==1)&&(lsespu8YawTmpReadDoneFlg[19]==1)&&(lsespu8YawTmpReadDoneFlg[20]==1)&&(lsespu8YawTmpReadDoneFlg[21]==1)&&(lsespu8YawTmpReadDoneFlg[22]==1)&&(lsespu8YawTmpReadDoneFlg[23]==1)&&(lsespu8YawTmpReadDoneFlg[24]==1)&&(lsespu8YawTmpReadDoneFlg[25]==1)&&(lsespu8YawTmpReadDoneFlg[26]==1)&&(lsespu8YawTmpReadDoneFlg[27]==1)&&(lsespu8YawTmpReadDoneFlg[28]==1)&&(lsespu8YawTmpReadDoneFlg[29]==1)&&(lsespu8YawTmpReadDoneFlg[30]==1))==1)
		{
			yaw_lg_tmp_read_done_flg=1;

			for(lsespu8LoadYawTmpCnt=1;lsespu8LoadYawTmpCnt<=29;lsespu8LoadYawTmpCnt++)
			{
				lsesps16YawTmpMap[lsespu8LoadYawTmpCnt] =lsesps16YawTmpEEPMap[lsespu8LoadYawTmpCnt];
			}
			for(lsespu8LoadYawTmpCnt=1;lsespu8LoadYawTmpCnt<=29;lsespu8LoadYawTmpCnt++)
			{
				lsespu8YawTmpOKFlg[lsespu8LoadYawTmpCnt] =lsespu8YawTmpEEPOKFlg[lsespu8LoadYawTmpCnt];
			}
		}
		else
		{
			yaw_lg_tmp_read_done_flg=0;
		}
	}
	else
	{
		;
	}
}
#endif
static void LSESP_vLimitYawTmpMap(void)
{
	lsesps16YawTmpLimit[0]=YAW_3G5DEG;   	/*-55 ~ -50*/
	lsesps16YawTmpLimit[1]=YAW_3G5DEG;   	/*-50 ~ -45*/
	lsesps16YawTmpLimit[2]=YAW_3G5DEG;   	/*-45 ~ -40*/
	lsesps16YawTmpLimit[3]=YAW_3G5DEG;   	/*-40 ~ -35*/
	lsesps16YawTmpLimit[4]=YAW_3G5DEG;   	/*-35 ~ -30*/
	lsesps16YawTmpLimit[5]=YAW_3G5DEG;   	/*-30 ~ -25*/
	lsesps16YawTmpLimit[6]=YAW_3G5DEG;   	/*-25 ~ -20*/
	lsesps16YawTmpLimit[7]=YAW_3DEG;   	/*-20 ~ -15    */
	lsesps16YawTmpLimit[8]=YAW_3DEG;   	/*-15 ~ -10    */
	lsesps16YawTmpLimit[9]=YAW_3DEG;   	/*-10 ~ -5     */
	lsesps16YawTmpLimit[10]=YAW_3DEG;  	/*-5  ~ 0      */
	lsesps16YawTmpLimit[11]=YAW_2DEG;  	/*0   ~ 5      */
	lsesps16YawTmpLimit[12]=YAW_2DEG;  	/*5   ~ 10     */
	lsesps16YawTmpLimit[13]=YAW_2DEG;  	/*10  ~ 15     */
	lsesps16YawTmpLimit[14]=YAW_2DEG;  	/*15  ~ 20     */
	lsesps16YawTmpLimit[15]=YAW_2DEG;  	/*20  ~ 25     */
	lsesps16YawTmpLimit[16]=YAW_2DEG;  	/*25  ~ 30     */
	lsesps16YawTmpLimit[17]=YAW_2DEG;  	/*30  ~ 35     */
	lsesps16YawTmpLimit[18]=YAW_2DEG;  	/*35  ~ 40     */
	lsesps16YawTmpLimit[19]=YAW_3DEG;  	/*40  ~ 45     */
	lsesps16YawTmpLimit[20]=YAW_3DEG;  	/*45  ~ 50     */
	lsesps16YawTmpLimit[21]=YAW_3DEG;  	/*50  ~ 55     */
	lsesps16YawTmpLimit[22]=YAW_3DEG;  	/*55  ~ 60     */
	lsesps16YawTmpLimit[23]=YAW_3G5DEG;  	/*60  ~ 65 */
	lsesps16YawTmpLimit[24]=YAW_3G5DEG;  	/*65  ~ 70 */
	lsesps16YawTmpLimit[25]=YAW_3G5DEG;  	/*70  ~ 75 */
	lsesps16YawTmpLimit[26]=YAW_3G5DEG;  	/*75  ~ 80 */
	lsesps16YawTmpLimit[27]=YAW_3G5DEG;  	/*80  ~ 85 */
	lsesps16YawTmpLimit[28]=YAW_3G5DEG;  	/*85  ~ 90 */
	lsesps16YawTmpLimit[29]=YAW_3G5DEG;  	/*90  ~ 95 */
	lsesps16YawTmpLimit[30]=YAW_3G5DEG;  	/*95  ~ 100*/
}

static void LSESP_vUpdateYawTmpMap(void)
{
	int8_t lsesps8UpdateYawTmpMapCnt;

	for(lsesps8UpdateYawTmpMapCnt=0;lsesps8UpdateYawTmpMapCnt<=lsespu8YawOffsetPileIndex;lsesps8UpdateYawTmpMapCnt++)
	{
		lsesps8YawTmpWriteIndex = (int8_t)((lsesps16YawTmpPile[lsesps8UpdateYawTmpMapCnt]/TMP_5_DC)+11);

		if(lsesps8YawTmpWriteIndex<0)
		{
			lsesps8YawTmpWriteIndex=0;
		}
		else if(lsesps8YawTmpWriteIndex>30)
		{
			lsesps8YawTmpWriteIndex=30;
		}
		else
		{
			;
		}

		if((lsesps8YawTmpWriteIndex>0)&&(lsesps8YawTmpWriteIndex<30))
		{
			if(lsespu8YawTmpOKFlg[lsesps8YawTmpWriteIndex]==1)
			{
				if(lsesps16YawOffsetPile[lsesps8UpdateYawTmpMapCnt]>YAW_4G5DEG)
				{
					tempW0=YAW_4G5DEG;
				}
				else if(lsesps16YawOffsetPile[lsesps8UpdateYawTmpMapCnt]<-YAW_4G5DEG)
				{
					tempW0=-YAW_4G5DEG;
				}
				else
				{
					tempW0=lsesps16YawOffsetPile[lsesps8UpdateYawTmpMapCnt];
				}
				tempW1=lsesps16YawTmpMap[lsesps8YawTmpWriteIndex];
				lsesps16YawTmpMap[lsesps8YawTmpWriteIndex]=LCESP_s16Lpf1Int(tempW0, tempW1, L_U8FILTER_GAIN_10MSLOOP_5HZ); /*5Hz*/

				lsespu8YawTmpInitRqFlg[lsesps8YawTmpWriteIndex]=0;
			}
			else
			{
				lsesps16YawTmpMap[lsesps8YawTmpWriteIndex]=lsesps16YawOffsetPile[lsesps8UpdateYawTmpMapCnt];
				lsespu8YawTmpOKFlg[lsesps8YawTmpWriteIndex]=1;

				lsespu8YawTmpInitRqFlg[lsesps8YawTmpWriteIndex]=0;
			}
		}
		else
		{
			;
		}

	}
}
#if __TEST_MODE_YAW_TMP_SRCH
static void LSESP_vLoadTestSrchYawTmp(void)
{
	lsesps16YawTmpMap[0] =310;   	/*-55 ~ -50*/
	lsesps16YawTmpMap[1] =300;   	/*-50 ~ -45*/
	lsesps16YawTmpMap[2] =300;   	/*-45 ~ -40*/
	lsesps16YawTmpMap[3] =200;   	/*-40 ~ -35*/
	lsesps16YawTmpMap[4] =200;   	/*-35 ~ -30*/
	lsesps16YawTmpMap[5] =200;   	/*-30 ~ -25*/
	lsesps16YawTmpMap[6] =100;   	/*-25 ~ -20*/
	lsesps16YawTmpMap[7] =90;   	/*-20 ~ -15    */
	lsesps16YawTmpMap[8] =80;   	/*-15 ~ -10    */
	lsesps16YawTmpMap[9] =70;   	/*-10 ~ -5     */
	lsesps16YawTmpMap[10]=60;  	/*-5  ~ 0      */
	lsesps16YawTmpMap[11]=50;  	/*0   ~ 5      */
	lsesps16YawTmpMap[12]=40;  	/*5   ~ 10     */
	lsesps16YawTmpMap[13]=30;  	/*10  ~ 15     */
	lsesps16YawTmpMap[14]=20;  	/*15  ~ 20     */
	lsesps16YawTmpMap[15]=10;  	/*20  ~ 25     */
	lsesps16YawTmpMap[16]=0;  	/*25  ~ 30     */
	lsesps16YawTmpMap[17]=-10;  	/*30  ~ 35     */
	lsesps16YawTmpMap[18]=-20;  	/*35  ~ 40     */
	lsesps16YawTmpMap[19]=-30;  	/*40  ~ 45     */
	lsesps16YawTmpMap[20]=-40;  	/*45  ~ 50     */
	lsesps16YawTmpMap[21]=-50;  	/*50  ~ 55     */
	lsesps16YawTmpMap[22]=-60;  	/*55  ~ 60     */
	lsesps16YawTmpMap[23]=-70;  	/*60  ~ 65 */
	lsesps16YawTmpMap[24]=-80;  	/*65  ~ 70 */
	lsesps16YawTmpMap[25]=-90;  	/*70  ~ 75 */
	lsesps16YawTmpMap[26]=-100;  	/*75  ~ 80 */
	lsesps16YawTmpMap[27]=-110;  	/*80  ~ 85 */
	lsesps16YawTmpMap[28]=-120;  	/*85  ~ 90 */
	lsesps16YawTmpMap[29]=-130;  	/*90  ~ 95 */
	lsesps16YawTmpMap[30]=-140;  	/*95  ~ 100*/

	lsespu8YawTmpOKFlg[0] =0;   	/*-55 ~ -50*/
	lsespu8YawTmpOKFlg[1] =1;   	/*-50 ~ -45*/
	lsespu8YawTmpOKFlg[2] =0;   	/*-45 ~ -40*/
	lsespu8YawTmpOKFlg[3] =1;   	/*-40 ~ -35*/
	lsespu8YawTmpOKFlg[4] =0;   	/*-35 ~ -30*/
	lsespu8YawTmpOKFlg[5] =0;   	/*-30 ~ -25*/
	lsespu8YawTmpOKFlg[6] =1;   	/*-25 ~ -20*/
	lsespu8YawTmpOKFlg[7] =0;   	/*-20 ~ -15    */
	lsespu8YawTmpOKFlg[8] =0;   	/*-15 ~ -10    */
	lsespu8YawTmpOKFlg[9] =0;   	/*-10 ~ -5     */
	lsespu8YawTmpOKFlg[10]=0;  	/*-5  ~ 0      */
	lsespu8YawTmpOKFlg[11]=0;  	/*0   ~ 5      */
	lsespu8YawTmpOKFlg[12]=0;  	/*5   ~ 10     */
	lsespu8YawTmpOKFlg[13]=0;  	/*10  ~ 15     */
	lsespu8YawTmpOKFlg[14]=0;  	/*15  ~ 20     */
	lsespu8YawTmpOKFlg[15]=0;  	/*20  ~ 25     */
	lsespu8YawTmpOKFlg[16]=1;  	/*25  ~ 30     */
	lsespu8YawTmpOKFlg[17]=1;  	/*30  ~ 35     */
	lsespu8YawTmpOKFlg[18]=1;  	/*35  ~ 40     */
	lsespu8YawTmpOKFlg[19]=1;  	/*40  ~ 45     */
	lsespu8YawTmpOKFlg[20]=1;  	/*45  ~ 50     */
	lsespu8YawTmpOKFlg[21]=1;  	/*50  ~ 55     */
	lsespu8YawTmpOKFlg[22]=1;  	/*55  ~ 60     */
	lsespu8YawTmpOKFlg[23]=1;  	/*60  ~ 65 */
	lsespu8YawTmpOKFlg[24]=1;  	/*65  ~ 70 */
	lsespu8YawTmpOKFlg[25]=1;  	/*70  ~ 75 */
	lsespu8YawTmpOKFlg[26]=1;  	/*75  ~ 80 */
	lsespu8YawTmpOKFlg[27]=0;  	/*80  ~ 85 */
	lsespu8YawTmpOKFlg[28]=0;  	/*85  ~ 90 */
	lsespu8YawTmpOKFlg[29]=0;  	/*90  ~ 95 */
	lsespu8YawTmpOKFlg[30]=0;  	/*95  ~ 100*/

}
#endif
static void LSESP_vSrchYawTmpMap(void)
{

	int8_t lsesps8SrchMapMinusCnt;
	int8_t lsesps8SrchMapPlusCnt;


	lsesps8SrchYawTmpMapIndex=(int8_t)(lsesps16Tmp1Sec/TMP_5_DC)+11;

	#if __TEST_MODE_YAW_TMP_SRCH
	if(__TEST_MODE_YAW_TMP_SRCH==1)
	{
		LSESP_vLoadTestSrchYawTmp();

		if(lsesps8SrchTestCnt>30)
		{
			lsesps8SrchTestCnt=0;
		}
		else
		{
			;
		}
		lsesps8SrchYawTmpMapIndex=lsesps8SrchTestCnt;
		lsesps8SrchTestCnt++;
	}
	else
	{
		;
	}
	#endif

	if(lsesps8SrchYawTmpMapIndex<0)
	{
		lsesps8SrchYawTmpMapIndex=0;
	}
	else if(lsesps8SrchYawTmpMapIndex>30)
	{
		lsesps8SrchYawTmpMapIndex=30;
	}
	else
	{
		;
	}

	if(lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapIndex]==1)
	{
		lsesps16YawOffsetByTmp=lsesps16YawTmpMap[lsesps8SrchYawTmpMapIndex];
		yaw_offset_srch_ok_flg=1;
	}
	else
	{

		for(lsesps8SrchMapMinusCnt=lsesps8SrchYawTmpMapIndex;lsesps8SrchMapMinusCnt>=0;lsesps8SrchMapMinusCnt--)
		{
			if(yaw_map_minus_cnt_ok_flg==0)
			{
				if(lsespu8YawTmpOKFlg[lsesps8SrchMapMinusCnt]==1)
				{
					lsesps8SrchYawTmpMapMinusCnt=lsesps8SrchMapMinusCnt;
					yaw_map_minus_cnt_ok_flg=1;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		for(lsesps8SrchMapPlusCnt=lsesps8SrchYawTmpMapIndex;lsesps8SrchMapPlusCnt<=30;lsesps8SrchMapPlusCnt++)
		{
			if(yaw_map_plus_cnt_ok_flg==0)
			{
				if(lsespu8YawTmpOKFlg[lsesps8SrchMapPlusCnt]==1)
				{
					lsesps8SrchYawTmpMapPlusCnt=lsesps8SrchMapPlusCnt;
					yaw_map_plus_cnt_ok_flg=1;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		if(yaw_map_minus_cnt_ok_flg==0)
		{
			lsesps8SrchYawTmpMapMinusCnt=0;
		}
		else
		{
			;
		}
		if(yaw_map_plus_cnt_ok_flg==0)
		{
			lsesps8SrchYawTmpMapPlusCnt=30;
		}
		else
		{
			;
		}

		if((yaw_map_minus_cnt_ok_flg==1)&&(yaw_map_plus_cnt_ok_flg==1)&&(McrAbs(lsesps8SrchYawTmpMapIndex-lsesps8SrchYawTmpMapMinusCnt)<3)&&(McrAbs(lsesps8SrchYawTmpMapIndex-lsesps8SrchYawTmpMapPlusCnt)<3))
		{
			if((lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapMinusCnt]==1)&&(lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapPlusCnt]==1))
			{
				lsesps16YawOffsetByTmp=LCESP_s16IInter2Point( (int16_t)lsesps8SrchYawTmpMapIndex, (int16_t)lsesps8SrchYawTmpMapMinusCnt, lsesps16YawTmpMap[lsesps8SrchYawTmpMapMinusCnt], (int16_t)lsesps8SrchYawTmpMapPlusCnt, lsesps16YawTmpMap[lsesps8SrchYawTmpMapPlusCnt] );
				yaw_offset_srch_ok_flg=1;
			}
			else
			{
				yaw_offset_srch_ok_flg=0;
			}
		}
		else if((yaw_map_minus_cnt_ok_flg==1)&&(McrAbs(lsesps8SrchYawTmpMapIndex-lsesps8SrchYawTmpMapMinusCnt)<3))
		{
			if(lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapMinusCnt]==1)
			{
				lsesps16YawOffsetByTmp=lsesps16YawTmpMap[lsesps8SrchYawTmpMapMinusCnt];
				yaw_offset_srch_ok_flg=1;
			}
			else
			{
				yaw_offset_srch_ok_flg=0;
			}
		}
		else if((yaw_map_plus_cnt_ok_flg==1)&&(McrAbs(lsesps8SrchYawTmpMapIndex-lsesps8SrchYawTmpMapPlusCnt)<3))
		{
			if(lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapPlusCnt]==1)
			{
				lsesps16YawOffsetByTmp=lsesps16YawTmpMap[lsesps8SrchYawTmpMapPlusCnt];
				yaw_offset_srch_ok_flg=1;
			}
			else
			{
				yaw_offset_srch_ok_flg=0;
			}
		}
		else
		{
			yaw_offset_srch_ok_flg=0;
		}
		yaw_map_minus_cnt_ok_flg=0;
		yaw_map_plus_cnt_ok_flg=0;
	}

	if(yaw_offset_srch_ok_flg==1)
	{
		/*LSESP_vLimitYawTmpMap();*/
		if(lsesps16YawOffsetByTmp>YAW_4G5DEG)
		{
			lsesps16YawOffsetByTmp=YAW_4G5DEG;
		}
		else if(lsesps16YawOffsetByTmp<-YAW_4G5DEG)
		{
			lsesps16YawOffsetByTmp=-YAW_4G5DEG;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

static void LSESP_vDetTemperatureYawOffset(void)
{
	uint8_t lsespu8DetWriteYawTmpCnt;
	uint16_t lsespu16YawTmpEEPMapUpdateTH;

	if(lsespu8Tmp1SecCnt>=L_U8_TIME_10MSLOOP_1000MS)
	{
		lsespu8Tmp1SecCnt=0;

		tempW0=lsesps16Tmp1Sec;
		/*lsesps16Tmp1Sec=LCESP_s16Lpf1Int((int16_t)(lsesps32Tmp1secSum/L_U8_TIME_10MSLOOP_1000MS), tempW0, 28); */
		lsesps16Tmp1Sec=(int16_t)(lsesps32Tmp1secSum/L_U8_TIME_10MSLOOP_1000MS);
		lsesps32Tmp1secSum=0;
		tmp_1sec_delay_flg=1;

		LSESP_vSrchYawTmpMap();

	}
	else
	{
		lsespu8Tmp1SecCnt++;
		lsesps32Tmp1secSum+=yaw_lg_temp;
	}

	if(tmp_1sec_delay_flg==0)
	{
		lsesps16Tmp1Sec=yaw_lg_temp;
		LSESP_vSrchYawTmpMap();
	}
	else

	{
		;
	}

	if(cyctime<L_U16_TIME_10MSLOOP_3MIN)
	{
		lsespu16YawTmpEEPMapUpdateTH=YAW_0G2DEG;
	}
	else
	{
		lsespu16YawTmpEEPMapUpdateTH=YAW_0G05DEG;
	}

  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */
  if(wu8IgnStat == CE_ON_STATE)
  {
	for(lsespu8DetWriteYawTmpCnt=1;lsespu8DetWriteYawTmpCnt<=29;lsespu8DetWriteYawTmpCnt++)
	{
		if((lsespu8YawTmpOKFlg[lsespu8DetWriteYawTmpCnt]==1)&&(lsespu8YawTmpEEPOKFlg[lsespu8DetWriteYawTmpCnt]==0))
		{
			lsespu8YawTmpWriteRqFlg[lsespu8DetWriteYawTmpCnt]=1;
		}
		else if((lsespu8YawTmpOKFlg[lsespu8DetWriteYawTmpCnt]==1)&&(lsespu8YawTmpEEPOKFlg[lsespu8DetWriteYawTmpCnt]==1))
		{
			if(McrAbs(lsesps16YawTmpMap[lsespu8DetWriteYawTmpCnt]-lsesps16YawTmpEEPMap[lsespu8DetWriteYawTmpCnt])>lsespu16YawTmpEEPMapUpdateTH)
			{
				lsespu8YawTmpWriteRqFlg[lsespu8DetWriteYawTmpCnt]=1;
			}
			else
			{
				lsespu8YawTmpWriteRqFlg[lsespu8DetWriteYawTmpCnt]=0;
			}
		}
		else
		{
			lsespu8YawTmpWriteRqFlg[lsespu8DetWriteYawTmpCnt]=0;
		}
	}
  }
  else
  {
    ;
  }	
  /* 2012_SWD_KJS : EEPROM & ECU ShutDown */
	
}

#if TC27X_FEE==ENABLE
static void LSESP_vSaveYawTmpMap(void)
{
	uint8_t lsespu8CntSaveTmpMap;

	if(((lsespu8YawTmpWriteRqFlg[0]==1)||(lsespu8YawTmpWriteRqFlg[1]==1)||(lsespu8YawTmpWriteRqFlg[2]==1)||(lsespu8YawTmpWriteRqFlg[3]==1)
			||(lsespu8YawTmpWriteRqFlg[4]==1)||(lsespu8YawTmpWriteRqFlg[5]==1)||(lsespu8YawTmpWriteRqFlg[6]==1)||(lsespu8YawTmpWriteRqFlg[7]==1)
			||(lsespu8YawTmpWriteRqFlg[8]==1)||(lsespu8YawTmpWriteRqFlg[9]==1)||(lsespu8YawTmpWriteRqFlg[10]==1)||(lsespu8YawTmpWriteRqFlg[11]==1)
			||(lsespu8YawTmpWriteRqFlg[12]==1)||(lsespu8YawTmpWriteRqFlg[13]==1)||(lsespu8YawTmpWriteRqFlg[14]==1)||(lsespu8YawTmpWriteRqFlg[15]==1)
			||(lsespu8YawTmpWriteRqFlg[16]==1)||(lsespu8YawTmpWriteRqFlg[17]==1)||(lsespu8YawTmpWriteRqFlg[18]==1)||(lsespu8YawTmpWriteRqFlg[19]==1)
			||(lsespu8YawTmpWriteRqFlg[20]==1)||(lsespu8YawTmpWriteRqFlg[21]==1)||(lsespu8YawTmpWriteRqFlg[22]==1)||(lsespu8YawTmpWriteRqFlg[23]==1)
			||(lsespu8YawTmpWriteRqFlg[24]==1)||(lsespu8YawTmpWriteRqFlg[25]==1)||(lsespu8YawTmpWriteRqFlg[26]==1)||(lsespu8YawTmpWriteRqFlg[27]==1)
	  ||(lsespu8YawTmpWriteRqFlg[28]==1)||(lsespu8YawTmpWriteRqFlg[29]==1)||(lsespu8YawTmpWriteRqFlg[30]==1))==1)
	{
		write_yaw_tmp_map_eeprom_flg=1;
	}
	else
	{
		write_yaw_tmp_map_eeprom_flg=0;
	}
	if(((lsespu8YawTmpInitRqFlg[0]==1)||(lsespu8YawTmpInitRqFlg[1]==1)||(lsespu8YawTmpInitRqFlg[2]==1)||(lsespu8YawTmpInitRqFlg[3]==1)
			||(lsespu8YawTmpInitRqFlg[4]==1)||(lsespu8YawTmpInitRqFlg[5]==1)||(lsespu8YawTmpInitRqFlg[6]==1)||(lsespu8YawTmpInitRqFlg[7]==1)
			||(lsespu8YawTmpInitRqFlg[8]==1)||(lsespu8YawTmpInitRqFlg[9]==1)||(lsespu8YawTmpInitRqFlg[10]==1)||(lsespu8YawTmpInitRqFlg[11]==1)
			||(lsespu8YawTmpInitRqFlg[12]==1)||(lsespu8YawTmpInitRqFlg[13]==1)||(lsespu8YawTmpInitRqFlg[14]==1)||(lsespu8YawTmpInitRqFlg[15]==1)
			||(lsespu8YawTmpInitRqFlg[16]==1)||(lsespu8YawTmpInitRqFlg[17]==1)||(lsespu8YawTmpInitRqFlg[18]==1)||(lsespu8YawTmpInitRqFlg[19]==1)
			||(lsespu8YawTmpInitRqFlg[20]==1)||(lsespu8YawTmpInitRqFlg[21]==1)||(lsespu8YawTmpInitRqFlg[22]==1)||(lsespu8YawTmpInitRqFlg[23]==1)
			||(lsespu8YawTmpInitRqFlg[24]==1)||(lsespu8YawTmpInitRqFlg[25]==1)||(lsespu8YawTmpInitRqFlg[26]==1)||(lsespu8YawTmpInitRqFlg[27]==1)
			||(lsespu8YawTmpInitRqFlg[28]==1)||(lsespu8YawTmpInitRqFlg[29]==1)||(lsespu8YawTmpInitRqFlg[30]==1))==1)
	{
		init_yaw_tmp_map_eeprom_flg=1;
	}
	else
	{
		init_yaw_tmp_map_eeprom_flg=0;
	}

	if((write_yaw_tmp_map_eeprom_flg==1)||(init_yaw_tmp_map_eeprom_flg==1))
	{
		for(lsespu8CntSaveTmpMap=0;lsespu8CntSaveTmpMap<=30;lsespu8CntSaveTmpMap++)
		{
			if((lsespu8YawTmpWriteRqFlg[lsespu8CntSaveTmpMap]==0) && (lsespu8YawTmpInitRqFlg[lsespu8CntSaveTmpMap]==1))
			{
				NVM_YawTmpEEPMap[lsespu8CntSaveTmpMap]=0;
				NVM_YawTmpEEPMap_c[lsespu8CntSaveTmpMap]     = (NVM_YawTmpEEPMap[lsespu8CntSaveTmpMap]^0xffffffff);
				lsespu8YawTmpInitRqFlg[lsespu8CntSaveTmpMap]=0;
			}
		}

		for (lsespu8CntSaveTmpMap=1;lsespu8CntSaveTmpMap<=29;lsespu8CntSaveTmpMap++)
		{
			if(lsespu8YawTmpWriteRqFlg[lsespu8CntSaveTmpMap]==1)
			{
				NVM_YawTmpEEPMap[lsespu8CntSaveTmpMap]        = (int32_t)lsesps16YawTmpMap[lsespu8CntSaveTmpMap];
				NVM_YawTmpEEPMap_c[lsespu8CntSaveTmpMap]      = (NVM_YawTmpEEPMap[lsespu8CntSaveTmpMap]^0xffffffff);
				lsespu8YawTmpWriteRqFlg[lsespu8CntSaveTmpMap]=0;

			}
		}
	}
	else
	{
		;
	}
}

#else
static void LSESP_vSaveYawTmpMap(void)
{
	uint8_t lsespu8CntSaveTmpMap;

	LSESP_vAddressYawTmpMap();

	if(((lsespu8YawTmpWriteRqFlg[0]==1)||(lsespu8YawTmpWriteRqFlg[1]==1)||(lsespu8YawTmpWriteRqFlg[2]==1)||(lsespu8YawTmpWriteRqFlg[3]==1)
			||(lsespu8YawTmpWriteRqFlg[4]==1)||(lsespu8YawTmpWriteRqFlg[5]==1)||(lsespu8YawTmpWriteRqFlg[6]==1)||(lsespu8YawTmpWriteRqFlg[7]==1)
			||(lsespu8YawTmpWriteRqFlg[8]==1)||(lsespu8YawTmpWriteRqFlg[9]==1)||(lsespu8YawTmpWriteRqFlg[10]==1)||(lsespu8YawTmpWriteRqFlg[11]==1)
			||(lsespu8YawTmpWriteRqFlg[12]==1)||(lsespu8YawTmpWriteRqFlg[13]==1)||(lsespu8YawTmpWriteRqFlg[14]==1)||(lsespu8YawTmpWriteRqFlg[15]==1)
			||(lsespu8YawTmpWriteRqFlg[16]==1)||(lsespu8YawTmpWriteRqFlg[17]==1)||(lsespu8YawTmpWriteRqFlg[18]==1)||(lsespu8YawTmpWriteRqFlg[19]==1)
			||(lsespu8YawTmpWriteRqFlg[20]==1)||(lsespu8YawTmpWriteRqFlg[21]==1)||(lsespu8YawTmpWriteRqFlg[22]==1)||(lsespu8YawTmpWriteRqFlg[23]==1)
			||(lsespu8YawTmpWriteRqFlg[24]==1)||(lsespu8YawTmpWriteRqFlg[25]==1)||(lsespu8YawTmpWriteRqFlg[26]==1)||(lsespu8YawTmpWriteRqFlg[27]==1)
			||(lsespu8YawTmpWriteRqFlg[28]==1)||(lsespu8YawTmpWriteRqFlg[29]==1)||(lsespu8YawTmpWriteRqFlg[30]==1))==1
	)
	{
		write_yaw_tmp_map_eeprom_flg=1;
	}
	else
	{
		write_yaw_tmp_map_eeprom_flg=0;
	}
	if(((lsespu8YawTmpInitRqFlg[0]==1)||(lsespu8YawTmpInitRqFlg[1]==1)||(lsespu8YawTmpInitRqFlg[2]==1)||(lsespu8YawTmpInitRqFlg[3]==1)
			||(lsespu8YawTmpInitRqFlg[4]==1)||(lsespu8YawTmpInitRqFlg[5]==1)||(lsespu8YawTmpInitRqFlg[6]==1)||(lsespu8YawTmpInitRqFlg[7]==1)
			||(lsespu8YawTmpInitRqFlg[8]==1)||(lsespu8YawTmpInitRqFlg[9]==1)||(lsespu8YawTmpInitRqFlg[10]==1)||(lsespu8YawTmpInitRqFlg[11]==1)
			||(lsespu8YawTmpInitRqFlg[12]==1)||(lsespu8YawTmpInitRqFlg[13]==1)||(lsespu8YawTmpInitRqFlg[14]==1)||(lsespu8YawTmpInitRqFlg[15]==1)
			||(lsespu8YawTmpInitRqFlg[16]==1)||(lsespu8YawTmpInitRqFlg[17]==1)||(lsespu8YawTmpInitRqFlg[18]==1)||(lsespu8YawTmpInitRqFlg[19]==1)
			||(lsespu8YawTmpInitRqFlg[20]==1)||(lsespu8YawTmpInitRqFlg[21]==1)||(lsespu8YawTmpInitRqFlg[22]==1)||(lsespu8YawTmpInitRqFlg[23]==1)
			||(lsespu8YawTmpInitRqFlg[24]==1)||(lsespu8YawTmpInitRqFlg[25]==1)||(lsespu8YawTmpInitRqFlg[26]==1)||(lsespu8YawTmpInitRqFlg[27]==1)
			||(lsespu8YawTmpInitRqFlg[28]==1)||(lsespu8YawTmpInitRqFlg[29]==1)||(lsespu8YawTmpInitRqFlg[30]==1))==1)
	{
		init_yaw_tmp_map_eeprom_flg=1;
	}
	else
	{
		init_yaw_tmp_map_eeprom_flg=0;
	}

	if((write_yaw_tmp_map_eeprom_flg==1)||(init_yaw_tmp_map_eeprom_flg==1))
	{
		if((weu1EraseSector==0)&&(weu1WriteSector==0))
		{
			//    			for(lsespu8CntSaveTmpMap=0;lsespu8CntSaveTmpMap<=30;lsespu8CntSaveTmpMap++)
			//    			{
			//				if((lsespu8YawTmpWriteRqFlg[lsespu8CntSaveTmpMap]==0) && (lsespu8YawTmpInitRqFlg[lsespu8CntSaveTmpMap]==1))
			//				{
			//					weu16Eep_addr=lsespu16YawTmpAddress[lsespu8CntSaveTmpMap];
			//					WE_vStartWriteEep(1);
			//					lsespu8YawTmpInitRqFlg[lsespu8CntSaveTmpMap]=0;
			//				}
			//				else
			//				{
			//					;
			//				}
			//			}

			/* 20110624_KJS */
			if((lsespu8YawTmpWriteRqFlg[0]==0) && (lsespu8YawTmpInitRqFlg[0]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[0],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[0]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[1]==0) && (lsespu8YawTmpInitRqFlg[1]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[1],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[1]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[2]==0) && (lsespu8YawTmpInitRqFlg[2]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[2],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[2]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[3]==0) && (lsespu8YawTmpInitRqFlg[3]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[3],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[3]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[4]==0) && (lsespu8YawTmpInitRqFlg[4]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[4],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[4]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[5]==0) && (lsespu8YawTmpInitRqFlg[5]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[5],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[5]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[6]==0) && (lsespu8YawTmpInitRqFlg[6]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[6],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[6]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[7]==0) && (lsespu8YawTmpInitRqFlg[7]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[7],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[7]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[8]==0) && (lsespu8YawTmpInitRqFlg[8]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[8],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[8]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[9]==0) && (lsespu8YawTmpInitRqFlg[9]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[9],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[9]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[10]==0) && (lsespu8YawTmpInitRqFlg[10]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[10],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[10]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[11]==0) && (lsespu8YawTmpInitRqFlg[11]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[11],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[11]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[12]==0) && (lsespu8YawTmpInitRqFlg[12]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[12],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[12]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[13]==0) && (lsespu8YawTmpInitRqFlg[13]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[13],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[13]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[14]==0) && (lsespu8YawTmpInitRqFlg[14]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[14],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[14]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[15]==0) && (lsespu8YawTmpInitRqFlg[15]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[15],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[15]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[16]==0) && (lsespu8YawTmpInitRqFlg[16]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[16],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[16]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[17]==0) && (lsespu8YawTmpInitRqFlg[17]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[17],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[17]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[18]==0) && (lsespu8YawTmpInitRqFlg[18]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[18],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[18]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[19]==0) && (lsespu8YawTmpInitRqFlg[19]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[19],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[19]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[20]==0) && (lsespu8YawTmpInitRqFlg[20]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[20],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[20]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[21]==0) && (lsespu8YawTmpInitRqFlg[21]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[21],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[21]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[22]==0) && (lsespu8YawTmpInitRqFlg[22]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[22],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[22]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[23]==0) && (lsespu8YawTmpInitRqFlg[23]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[23],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[23]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[24]==0) && (lsespu8YawTmpInitRqFlg[24]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[24],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[24]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[25]==0) && (lsespu8YawTmpInitRqFlg[25]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[25],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[25]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[26]==0) && (lsespu8YawTmpInitRqFlg[26]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[26],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[26]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[27]==0) && (lsespu8YawTmpInitRqFlg[27]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[27],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[27]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[28]==0) && (lsespu8YawTmpInitRqFlg[28]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[28],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[28]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[29]==0) && (lsespu8YawTmpInitRqFlg[29]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[29],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[29]=0;
				}
				else
				{
					;
				}
			}
			else if((lsespu8YawTmpWriteRqFlg[30]==0) && (lsespu8YawTmpInitRqFlg[30]==1))
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[30],(uint16_t)(0))==0)
				{
					lsespu8YawTmpInitRqFlg[30]=0;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
			/* 20110624_KJS */

			/*if(lsespu8YawTmpWriteRqFlg[0]==1)
    		{
    		    if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[0],(uint16_t)lsesps16YawTmpMap[0])==0)
    		    {
    		    	lsespu8YawTmpWriteRqFlg[0]=0;
    		    }
    		    else
				{
					;
				}
    		}
    		else*/
			if(lsespu8YawTmpWriteRqFlg[1]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[1],(uint16_t)lsesps16YawTmpMap[1])==0)
				{
					lsespu8YawTmpWriteRqFlg[1]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[2]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[2],(uint16_t)lsesps16YawTmpMap[2])==0)
				{
					lsespu8YawTmpWriteRqFlg[2]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[3]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[3],(uint16_t)lsesps16YawTmpMap[3])==0)
				{
					lsespu8YawTmpWriteRqFlg[3]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[4]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[4],(uint16_t)lsesps16YawTmpMap[4])==0)
				{
					lsespu8YawTmpWriteRqFlg[4]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[5]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[5],(uint16_t)lsesps16YawTmpMap[5])==0)
				{
					lsespu8YawTmpWriteRqFlg[5]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[6]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[6],(uint16_t)lsesps16YawTmpMap[6])==0)
				{
					lsespu8YawTmpWriteRqFlg[6]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[7]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[7],(uint16_t)lsesps16YawTmpMap[7])==0)
				{
					lsespu8YawTmpWriteRqFlg[7]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[8]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[8],(uint16_t)lsesps16YawTmpMap[8])==0)
				{
					lsespu8YawTmpWriteRqFlg[8]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[9]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[9],(uint16_t)lsesps16YawTmpMap[9])==0)
				{
					lsespu8YawTmpWriteRqFlg[9]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[10]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[10],(uint16_t)lsesps16YawTmpMap[10])==0)
				{
					lsespu8YawTmpWriteRqFlg[10]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[11]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[11],(uint16_t)lsesps16YawTmpMap[11])==0)
				{
					lsespu8YawTmpWriteRqFlg[11]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[12]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[12],(uint16_t)lsesps16YawTmpMap[12])==0)
				{
					lsespu8YawTmpWriteRqFlg[12]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[13]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[13],(uint16_t)lsesps16YawTmpMap[13])==0)
				{
					lsespu8YawTmpWriteRqFlg[13]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[14]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[14],(uint16_t)lsesps16YawTmpMap[14])==0)
				{
					lsespu8YawTmpWriteRqFlg[14]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[15]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[15],(uint16_t)lsesps16YawTmpMap[15])==0)
				{
					lsespu8YawTmpWriteRqFlg[15]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[16]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[16],(uint16_t)lsesps16YawTmpMap[16])==0)
				{
					lsespu8YawTmpWriteRqFlg[16]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[17]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[17],(uint16_t)lsesps16YawTmpMap[17])==0)
				{
					lsespu8YawTmpWriteRqFlg[17]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[18]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[18],(uint16_t)lsesps16YawTmpMap[18])==0)
				{
					lsespu8YawTmpWriteRqFlg[18]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[19]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[19],(uint16_t)lsesps16YawTmpMap[19])==0)
				{
					lsespu8YawTmpWriteRqFlg[19]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[20]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[20],(uint16_t)lsesps16YawTmpMap[20])==0)
				{
					lsespu8YawTmpWriteRqFlg[20]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[21]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[21],(uint16_t)lsesps16YawTmpMap[21])==0)
				{
					lsespu8YawTmpWriteRqFlg[21]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[22]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[22],(uint16_t)lsesps16YawTmpMap[22])==0)
				{
					lsespu8YawTmpWriteRqFlg[22]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[23]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[23],(uint16_t)lsesps16YawTmpMap[23])==0)
				{
					lsespu8YawTmpWriteRqFlg[23]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[24]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[24],(uint16_t)lsesps16YawTmpMap[24])==0)
				{
					lsespu8YawTmpWriteRqFlg[24]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[25]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[25],(uint16_t)lsesps16YawTmpMap[25])==0)
				{
					lsespu8YawTmpWriteRqFlg[25]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[26]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[26],(uint16_t)lsesps16YawTmpMap[26])==0)
				{
					lsespu8YawTmpWriteRqFlg[26]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[27]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[27],(uint16_t)lsesps16YawTmpMap[27])==0)
				{
					lsespu8YawTmpWriteRqFlg[27]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[28]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[28],(uint16_t)lsesps16YawTmpMap[28])==0)
				{
					lsespu8YawTmpWriteRqFlg[28]=0;
				}
				else
				{
					;
				}
			}
			else if(lsespu8YawTmpWriteRqFlg[29]==1)
			{
				if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[29],(uint16_t)lsesps16YawTmpMap[29])==0)
				{
					lsespu8YawTmpWriteRqFlg[29]=0;
				}
				else
				{
					;
				}
			}
			/*else if(lsespu8YawTmpWriteRqFlg[30]==1)
    		{
    		    if(LSESP_u16WriteEEPROM(lsespu16YawTmpAddress[30],(uint16_t)lsesps16YawTmpMap[30])==0)
    		    {
    		    	lsespu8YawTmpWriteRqFlg[30]=0;
    		    }
    		    else
				{
					;
				}
    		}*/
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

#endif
static void LSESP_vDetYawTempCompOffset(void)
{
	int16_t lsesps16TemporaryYawOffset;
	int8_t lsesps8TemporaryYawOffsetAcc;

	lsesps8TemporaryYawOffsetAcc=KYAW.eeprom_accuracy;
	lsesps16TemporaryYawOffset=KYAW.offset_of_eeprom;

	if(KYAW.driving_accuracy>=lsesps8TemporaryYawOffsetAcc)
	{
		if(KYAW.FIRST_OFFSET_CAL_FLG==1)
		{
			lsesps16TemporaryYawOffset = KYAW.offset_60avr;
		}
		else
		{
			lsesps16TemporaryYawOffset = KYAW.offset_at_15kph;
		}

		lsesps8TemporaryYawOffsetAcc = KYAW.driving_accuracy;
	}
	else
	{
		;
	}

	if(KYAW.standstill_accuracy >=40)
	{
		lsesps8TemporaryYawOffsetAcc=KYAW.standstill_accuracy;
		lsesps16TemporaryYawOffset=lsesps16YawStandOffset;
	}
	else
	{
		;
	}

	yaw_offset_f=lsesps16TemporaryYawOffset;
	yaw_offset_reliablity=(lsesps8TemporaryYawOffsetAcc>0)?(uint8_t)McrAbs(lsesps8TemporaryYawOffsetAcc):0;

    if(lsespu1YawSpecOverSus ==1 )
    {
    	;
    }
    else
    {
	    if((yaw_offset_srch_ok_flg==1) && (wbu1YawSerialNOKFlg==1) )
	    {
	    	yaw_offset_f=lsesps16YawOffsetByTmp;
	    	if(lsespu8YawTmpOKFlg[lsesps8SrchYawTmpMapIndex]==1)
	    	{
	    		yaw_offset_reliablity=100;
	    	}
	    	else
	    	{
	    		yaw_offset_reliablity=90;
	    	}
	    }
	    else
	    {
	    	;
	    }
	}
	

	/* Offset-Range check */

	LSESP_vLimitYawTmpMap();

	
	if(lsespu1YawSpecOverSus ==1 )
	{
		lsesps16MaxYawThrAdd= lsesps16YawSpcOvrThres ; 
    }
    else
    {
   		lsesps16MaxYawThrAdd=(McrAbs(yaw_offset_f)>lsesps16YawTmpLimit[lsesps8SrchYawTmpMapIndex])?(McrAbs(yaw_offset_f)-lsesps16YawTmpLimit[lsesps8SrchYawTmpMapIndex]):0; 
    }

	if(lsesps16MaxYawThrAdd >YAW_THRESHOLD_ADD_MAX)
	{
		lsesps16MaxYawThrAdd =YAW_THRESHOLD_ADD_MAX;
	}
	else
	{
		;
	}
	
	if(yaw_offset_f>YAW_OFFSET_MAX_CORR)
	{
		yaw_offset_f=YAW_OFFSET_MAX_CORR;
		yaw_offset_reliablity=0;
	}
	else if(yaw_offset_f<-YAW_OFFSET_MAX_CORR)
	{
		yaw_offset_f=-YAW_OFFSET_MAX_CORR;
		yaw_offset_reliablity=0;
	}
	else if(McrAbs(yaw_offset_f)>lsesps16YawTmpLimit[lsesps8SrchYawTmpMapIndex])
	{
		if(yaw_offset_reliablity>=30)
		{
			yaw_offset_reliablity=(uint8_t)McrAbs(LCESP_s16IInter2Point(McrAbs(yaw_offset_f),lsesps16YawTmpLimit[lsesps8SrchYawTmpMapIndex],30,YAW_4G5DEG,0));
		}
		else
		{
			yaw_offset_reliablity=0;
		}
	}
	else
	{
		;
	}

}

#endif

static int16_t LSESP_s16FilterPecentage(int16_t n, int16_t n_1, uint8_t percent)
{
    int16_t i;
/****************** CT 개선 전 : 2005.02.20 **********************************

            tempW2 = n;
            tempW1 = 100-percent;
            tempW0 = 100;
            s16muls16divs16();
            i= tempW3;

            tempW2 = n_1;
            tempW1 = percent;
            tempW0 = 100;
            s16muls16divs16();
*****************************************************************************/

/****************** CT 개선 후 : 2005.02.20 (C 코드로 환원) ******************/

            i=(int16_t)(((int32_t)n*(100-percent))/100);

            tempW3=(int16_t)(((int32_t)n_1*percent)/100);
/*****************************************************************************/

     return (i+tempW3);
}
#endif /*__VDC*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCalSensorOffset
	#include "Mdyn_autosar.h"
#endif
