#ifndef __LADECCALLACTHW_H__
#define __LADECCALLACTHW_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition**************************/
  #if __DEC
#define	U8_DEC_HOLD			1
#define	U8_DEC_APPLY		2
#define	U8_DEC_DUMP			3

#define	S16_DEC_ABSDump_ESV_Close_Th1	L_U8_TIME_10MSLOOP_150MS
#define	S16_DEC_ABSDump_ESV_Close_Th2	L_U8_TIME_10MSLOOP_100MS

#define S16_DEC_T_VOLT_At_HSA           MSC_2_V

  #endif

/*Global MACRO FUNCTION Definition**************************/

/*Global Extern Variable  Declaration***********************/
  #if __DEC

typedef struct
{
	uint16_t lau1DecWL1No :1;
	uint16_t lau1DecWL1Nc :1;
	uint16_t lau1DecWL2No :1;
	uint16_t lau1DecWL2Nc :1;
}DEC_VALVE_t;

   #if (__SPLIT_TYPE==0)
#define	DEC_HV_VL_fr			laDecValveP.lau1DecWL1No
#define	DEC_AV_VL_fr			laDecValveP.lau1DecWL1Nc
#define	DEC_HV_VL_rl			laDecValveP.lau1DecWL2No
#define	DEC_AV_VL_rl			laDecValveP.lau1DecWL2Nc

#define	DEC_HV_VL_fl			laDecValveS.lau1DecWL1No
#define	DEC_AV_VL_fl			laDecValveS.lau1DecWL1Nc
#define	DEC_HV_VL_rr			laDecValveS.lau1DecWL2No
#define	DEC_AV_VL_rr			laDecValveS.lau1DecWL2Nc
   #else
#define	DEC_HV_VL_fl			laDecValveP.lau1DecWL2No
#define	DEC_AV_VL_fl			laDecValveP.lau1DecWL2Nc
#define	DEC_HV_VL_fr			laDecValveP.lau1DecWL1No
#define	DEC_AV_VL_fr			laDecValveP.lau1DecWL1Nc

#define	DEC_HV_VL_rl			laDecValveS.lau1DecWL1No
#define	DEC_AV_VL_rl			laDecValveS.lau1DecWL1Nc
#define	DEC_HV_VL_rr			laDecValveS.lau1DecWL2No
#define	DEC_AV_VL_rr			laDecValveS.lau1DecWL2Nc
   #endif

   #if __ADVANCED_MSC
extern int16_t		dec_msc_target_vol;
   #endif

/********** To */
extern uint8_t	lcu8DecFlNoValve, lcu8DecFlNcValve, lcu8DecFrNoValve, lcu8DecFrNcValve;
extern uint8_t	lcu8DecRlNoValve, lcu8DecRlNcValve, lcu8DecRrNoValve, lcu8DecRrNcValve;

extern uint8_t	lcu8DecPriESVCloseTime;
extern uint8_t	lcu8DecSecESVCloseTime;

  #if __SPLIT_TYPE
extern uint8_t	lcu8DecFlFrEsvValve, lcu8DecRlRrEsvValve;
extern uint8_t	lcu8DecFlFrTcValve, lcu8DecRlRrTcValve;
  #else
extern uint8_t	lcu8DecFlRrEsvValve, lcu8DecFrRlEsvValve;
extern uint8_t	lcu8DecFlRrTcValve, lcu8DecFrRlTcValve;
  #endif

extern uint8_t	lcu8DecValveMode, lcu8DecMotorMode;
extern int16_t		lcs16DecMscTargetVol;


extern DEC_VALVE_t	laDecValveP, laDecValveS;

  #endif

/*Global Extern Functions  Declaration**********************/
  #if __DEC
extern void LADEC_vCallActHW(void);

#if	__Decel_Ctrl_Integ
extern int16_t LAMFC_s16SetMFCCurrentDEC(uint8_t CIRCUIT, DEC_VALVE_t *pDec, int16_t MFC_Current_old);
#endif

  #endif

   #if __ADVANCED_MSC
extern void LCMSC_vSetDECTargetVoltage(void);
   #endif


#endif
