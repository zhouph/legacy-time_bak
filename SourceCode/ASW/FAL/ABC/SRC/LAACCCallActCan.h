#ifndef __LAACCCALLACTCAN_H__
#define __LAACCCALLACTCAN_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition**************************/

/*Global MACRO FUNCTION Definition**************************/

/*Global Type Declaration **********************************/

/*Global Extern Variable  Declaration***********************/
  #if __ACC
   #if defined(__ACC_RealDevice_INTERFACE)
/********** From */
extern uint8_t	TCS1[8];
   #endif
  #endif

/*Global Extern Functions  Declaration**********************/
  #if __ACC
extern void LAACC_vCallActCan(void);
  #endif
#endif
