
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif

#include "LCESPCallControl.h"
#include "LDROPCallDetection.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"
#include "LDESPDetectVehicleStatus.h"
#include "LAESPActuateNcNovalveF.h"
#include "LCESPCalInterpolation.h"

#if (__ESC_TCMF_CONTROL ==1)
#include "LCESPInterfaceTCMF.h"
#endif

int16_t del_need_count_fl;
int16_t msc_count_enable_fl;
int16_t del_need_count_fr;
int16_t msc_count_enable_fr;
int16_t msc_rise_scan;
int16_t msc_rise_scan_fl;
int16_t msc_rise_scan_fr;
int16_t pulse_dump_nc_duty_front;
int16_t pulse_dump_nc_duty_rear;
int16_t esp_slip_nc_duty_front;
int16_t esp_slip_nc_duty_rear;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//uint8_t dump_counter_assurance_flag_front;
//uint8_t dump_counter_assurance_flag_rear;
#else
uint8_t dump_counter_assurance_flag_front;
uint8_t dump_counter_assurance_flag_rear;
#endif

int16_t fade_rate_counter_fl ;
int16_t fade_rate_counter_fr ;
int16_t fade_rate_counter_rl ;
int16_t fade_rate_counter_rr ;

int16_t fade_dump_rate_fl;
int16_t fade_dump_rate_fr;
int16_t fade_dump_rate_rl;
int16_t fade_dump_rate_rr;

#if Drum_Brake
uint8_t dump_cycle;
#endif
 
 

#if __VDC
void LAESP_vCalMototTime(void);
void LAESP_vResetTempValve(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LAESP_vActuateNcNovalveF(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
void LAESP_vActuateNcNovalveR(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL);
#else
void LAESP_vActuateNcNovalveF(void);
void LAESP_vActuateNcNovalveR(void);
#endif

#if Drum_Brake
void LAESP_vCheckDumpCycle(void);
#endif

/*060607 modified and added for FR Split by eslim*/
#if !__SPLIT_TYPE
void LAESP_vActXsplitEsvTcvalve(void);
#else
void LAESP_vActFRsplitEsvTcvalve(void);
#endif
/*060607 modified and added for FR Split by eslim*/
void LAESP_vCalNcDutyInEsp(void);
/*#if __PWM 	061113 deleted by eslim */

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
extern void LCESP_vCheckPressureCycleR(void);
extern void LCESP_vCheckPressureCycleF(void);
#endif


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern void LCESP_vDetectFullRise(void);
#else
extern void LCESP_vDetectFullRise(void);
#endif

/*#endif	061113 deleted by eslim */
#if ENABLE_PULSE_DOWN_DUMP
    void LAESP_vActDumpAtEndEsp(void);
extern  void LCESP_vSetPulseDump(void);
extern  void LCESP_vCountPulseDump(void);
extern  void LAESP_vCalNcDutyAtEndEsp(void);
#endif

void LAESP_vActFadeout(void);
void LAESP_vActFadeoutFL(void);
void LAESP_vActFadeoutFR(void);
void LAESP_vActFadeoutRL(void);
void LAESP_vActFadeoutRR(void);

#endif

#define S16_ESP_PRESSURE_DUTY_COM  100

#define S16_MPRESS_RISE_MODE_ENT_SLIP		WHEEL_SLIP_10

/*060607 modified and added for FR Split by eslim*/
#if __VDC 
#if !__SPLIT_TYPE
void LAESP_vActXsplitEsvTcvalve(void)
{
/**********FL ESV VALVE***************/
    #if(__ESC_TCMF_CONTROL ==1)
    if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FL_old==1))
    #else
    if(SLIP_CONTROL_NEED_FL==1)
    #endif
    {
            TEMP_TCL_DEMAND_fl=1;
            
#if __TCMF_ENABLE 
		T_S_V_LEFT=1;
#else

	#if __TCS_NOISE
        if((esp_mu < lsesps16MuMedHigh)&&(delta_yaw_dot>100)&&(!TEMP_AV_VL_fl)
            &&((slip_control_need_counter_fl<(esp_pressure_count_fl+4))
            ||((slip_m_fl>=(-300)))))
        {
                T_S_V_LEFT=1;
        }
        else
        {
            if((slip_control_need_counter_fl>42)&&(delta_yaw_dot<100)&&(delta_yaw_dot>(-100)))
            {
                T_S_V_LEFT=0;
            }
            else
            {
                ;
            }
            T_S_V_LEFT=!TEMP_HV_VL_fl;
        }
	#else
                T_S_V_LEFT=1;
	#endif
#endif
    }
    else
    {
    	;
    }

/**********FR ESV VALVE***************/
    #if(__ESC_TCMF_CONTROL ==1)
    if((SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FR_old==1))
    #else
    if (SLIP_CONTROL_NEED_FR==1)
    #endif
    {
            TEMP_TCL_DEMAND_fr=1;

#if __TCMF_ENABLE 
	T_S_V_RIGHT = 1;
#else

	#if __TCS_NOISE
        if((esp_mu < lsesps16MuMedHigh)&&(delta_yaw_dot>100)&&(!TEMP_AV_VL_fr)
          &&((slip_control_need_counter_fr<(esp_pressure_count_fr+4))
          ||((slip_m_fr>=(-300)))))
        {
                T_S_V_RIGHT=1;
        }
        else
        {
            if((slip_control_need_counter_fr>42)&&(delta_yaw_dot<100)&&(delta_yaw_dot>(-100)))
            {
                T_S_V_RIGHT = 0;
            }
            else
            {
                ;
            }

            T_S_V_RIGHT=!TEMP_HV_VL_fr;
        }
	#else
                T_S_V_RIGHT=1;
	#endif
#endif
    }
    else
    {
        ;
    }
/**********RL / RR ESV VALVE***************/
    if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
    {
        if(SLIP_CONTROL_NEED_RL==1)
        {
            TEMP_TCL_DEMAND_fr=1;

#if __TCMF_ENABLE 
	T_S_V_RIGHT = 1;
#else

        #if __TCS_NOISE
            if((esp_mu < lsesps16MuMedHigh)&&(((slip_control_need_counter_rl<=7)&&(!TEMP_AV_VL_rl))
                    ||((slip_m_rl>=(-200))&&(!TEMP_AV_VL_rl))))
            {
                T_S_V_RIGHT=1;
            }
            else
            {
                T_S_V_RIGHT=!TEMP_HV_VL_rl;
            }
        #else
                T_S_V_RIGHT=1;
        #endif
#endif 
        }
        else
        {
            ;
        }
        
        if(SLIP_CONTROL_NEED_RR==1)
        {
                TEMP_TCL_DEMAND_fl=1;
#if __TCMF_ENABLE 
	T_S_V_LEFT = 1;
#else
        #if __TCS_NOISE
                if((esp_mu < lsesps16MuMedHigh)&&(((slip_control_need_counter_rr<=7)&&(!TEMP_AV_VL_rr))
                        ||((slip_m_rr>=(-200))&&(!TEMP_AV_VL_rr))))
                {
                        T_S_V_LEFT=1;
                }
                else
                {
                    T_S_V_LEFT=!TEMP_HV_VL_rr;
                }
        #else
                T_S_V_LEFT=1;
        #endif
#endif
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
}
#else
void LAESP_vActFRsplitEsvTcvalve(void)
{
/**********FL ESV VALVE***************/
    if(SLIP_CONTROL_NEED_FL==1)
    {
        TEMP_TCL_DEMAND_PRIMARY=1;
#if __TCMF_ENABLE 
	T_S_V_PRIMARY=1;
#else
	#if __TCS_NOISE
        if((esp_mu < lsesps16MuMedHigh)&&(delta_yaw_dot>100)&&(!TEMP_AV_VL_fl)
            &&((slip_control_need_counter_fl<(esp_pressure_count_fl+4))
            ||((slip_m_fl>=(-300)))))
        {
            T_S_V_PRIMARY=1;
        }
        else
        {
            if((slip_control_need_counter_fl>42)&&(delta_yaw_dot<100)&&(delta_yaw_dot>(-100)))
            {
                T_S_V_PRIMARY=0;
            }
            else
            {
                ;
            }
            T_S_V_PRIMARY=!TEMP_HV_VL_fl;
        }
	#else
                T_S_V_PRIMARY=1;
	#endif
#endif
    }
    else
    {
        ;
    }
/**********FR ESV VALVE***************/
    
    if (SLIP_CONTROL_NEED_FR==1)
    {
        TEMP_TCL_DEMAND_PRIMARY=1;
#if __TCMF_ENABLE 
	T_S_V_PRIMARY=1;
#else
	#if __TCS_NOISE
        if((esp_mu < lsesps16MuMedHigh)&&(delta_yaw_dot>100)&&(!TEMP_AV_VL_fr)
          &&((slip_control_need_counter_fr<(esp_pressure_count_fr+4))
          ||((slip_m_fr>=(-300)))))
        {
                T_S_V_PRIMARY=1;
        }
        else
        {
            if((slip_control_need_counter_fr>42)&&(delta_yaw_dot<100)&&(delta_yaw_dot>(-100)))
            {
                T_S_V_PRIMARY=1;
            }
            else
            {
                ;
            }
            T_S_V_PRIMARY=!TEMP_HV_VL_fr;
        }
	#else
                T_S_V_PRIMARY=1;
	#endif
#endif
    }
    else
    {
        ;
    }
/**********RL / RR ESV VALVE***************/
    if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RR==1))
    {
        if(SLIP_CONTROL_NEED_RL==1)
        {
            TEMP_TCL_DEMAND_SECONDARY=1;
#if __TCMF_ENABLE 
	T_S_V_SECONDARY=1;
#else
        #if __TCS_NOISE

                T_S_V_SECONDARY=!TEMP_HV_VL_rl;
        #else
                T_S_V_SECONDARY=1;
        #endif
#endif
        }
        else
        {
            ;
        }
        if(SLIP_CONTROL_NEED_RR==1)
        {
                TEMP_TCL_DEMAND_SECONDARY=1;
#if __TCMF_ENABLE 
		T_S_V_SECONDARY=1;
#else        
        #if __TCS_NOISE
                    T_S_V_SECONDARY=!TEMP_HV_VL_rr;
        #else
                T_S_V_SECONDARY=1;
        #endif
#endif
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
}
#endif
/*060607 modified and added for FR Split by eslim*/

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LAESP_vActuateNcNovalveF(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
    if(WL_ESC->SLIP_CONTROL_NEED==1)        
    {
        if((lcu1ABSCBSCombCntrl_FL==1)||(lcu1ABSCBSCombCntrl_FR==1))
        	{
        		WL_ESC->rate_counter=0;
            WL_ESC->dump_counter_assurance_flag = 0;
            
            WL_ESC->TEMP_HV_VL=0;
            WL_ESC->TEMP_AV_VL=0;
            WL_ESC->esp_control_mode=1;
          }
        
        else if((WL_ESC->slip_control <= WL_ESC->slip_rise_thr_change)||(WL_ESC->FLAG_COMPULSION_RISE==1))
        {
            WL_ESC->rate_counter=0;
            WL_ESC->dump_counter_assurance_flag = 0;
            
				#if	( __Ext_Under_Ctrl == 1 )	 
            
            if(((EXTREME_UNDER_CONTROL==1)&&(WL->u8_Estimated_Active_Press >= WL_ESC->Extr_Under_Frt_Press_Max)))
            {            	
                WL_ESC->TEMP_HV_VL=1;                       
                WL_ESC->TEMP_AV_VL=0;                       
                WL_ESC->esp_control_mode=3;                 
            }
            else
            {
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0;
	          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	            if((ABS_fz==1)||(lsespu1AHBGEN3MpresBrkOn==1))
	          #else
                if((ABS_fz==1)||(MPRESS_BRAKE_ON==1))
       	      #endif
                {
                		WL_ESC->esp_control_mode=1;
                }
                else
                {
                        WL_ESC->esp_control_mode=1;
                }
            }
         #else
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0;
	          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	            if((ABS_fz==1)||(lsespu1AHBGEN3MpresBrkOn==1))
	          #else
                if((ABS_fz==1)||(MPRESS_BRAKE_ON==1))
              #endif
                {
                		WL_ESC->esp_control_mode=1;
                }
                else
                {
                        WL_ESC->esp_control_mode=1;
                }
          #endif
        }
        else if((WL_ESC->slip_control>WL_ESC->slip_rise_thr_change)&&(WL_ESC->slip_control<WL_ESC->slip_dump_thr_change)
            &&(WL_ESC->esp_pressure_count>=0))
        {
			if(WL_ESC->dump_counter_assurance_flag == 0)
			{
				WL_ESC->rate_counter=0;
				}
			else
			{
			    WL_ESC->rate_counter++;
      		 		if(WL_ESC->rate_counter>WL_ESC->rate_interval)
				{
			    	WL_ESC->rate_counter=0;
			        WL_ESC->dump_counter_assurance_flag = 0;
				}
				else
				{
					;
				}
			}
            WL_ESC->TEMP_HV_VL=1;
            WL_ESC->TEMP_AV_VL=0;
            WL_ESC->esp_control_mode=3;

        }
        else
        {
        	
        #if(__ESC_TCMF_CONTROL ==1)
        	if(WL_ESC->lcespu1TCMFControl ==1)
        	{
        		WL_ESC->TEMP_HV_VL=1;
                WL_ESC->TEMP_AV_VL=1;
                WL_ESC->esp_control_mode=4;
        	}
        	else if(WL_ESC->FLAG_COMPULSION_HOLD==1)
        	{
        	    WL_ESC->TEMP_HV_VL=1;
        	    WL_ESC->TEMP_AV_VL=0;
        	    WL_ESC->esp_control_mode=3;
        	}
        #else
                if(WL_ESC->FLAG_COMPULSION_HOLD==1)
                {
                    WL_ESC->TEMP_HV_VL=1;
                    WL_ESC->TEMP_AV_VL=0;
                    WL_ESC->esp_control_mode=3;

                }
        #endif
                else
                {
            if(WL_ESC->rate_counter==0)
            {
                     WL_ESC->TEMP_HV_VL=1;
                     WL_ESC->TEMP_AV_VL=1;
                     WL_ESC->esp_control_mode=4;

                }
            else
            {
                 if(WL_ESC->esp_pressure_count<0)
                 {
                    WL_ESC->TEMP_HV_VL=1;
                    WL_ESC->TEMP_AV_VL=1;
                    WL_ESC->esp_control_mode=4;

                 }
                 else
                 {
                    WL_ESC->TEMP_HV_VL=1;
                    WL_ESC->TEMP_AV_VL=0;
                    WL_ESC->esp_control_mode=3;

                 }
            }
          }

       		WL_ESC->dump_counter_assurance_flag = 1;
		    WL_ESC->rate_counter++;
		    	
         	if(WL_ESC->rate_counter>WL_ESC->rate_interval)
			{
				WL_ESC->rate_counter=0;
			}
			else if(WL_ESC->FLAG_COMPULSION_HOLD == 1)
			{
				WL_ESC->rate_counter=0;
			}
			else
			{
				;
			}
					
        }
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if((!ABS_fz)&&(lsesps16AHBGEN3mpress>300) && (WL_ESC->slip_m >= -S16_MPRESS_RISE_MODE_ENT_SLIP))        /* mpress > Wheel press */
      #else
        if((!ABS_fz)&&(mpress>300) && (WL_ESC->slip_m >= -S16_MPRESS_RISE_MODE_ENT_SLIP))     /* mpress > Wheel press */
      #endif
        {
            if(WL_ESC->mpress_rise_mode==0)
            {
              #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                if((lsesps16AHBGEN3mpress<600)&&(mpress_slop_filt>80))      /* mpress_slop >> motor_press_slop */
              #else   
                if((mpress<600)&&(mpress_slop_filt>80)) /* mpress_slop >> motor_press_slop */
              #endif
                {                                       /* then use mpress force */
                    WL_ESC->mpress_rise_mode = 1;
                    WL_ESC->TEMP_HV_VL=0;
	                WL_ESC->TEMP_AV_VL=0;
	
	                WL_ESC->esp_control_mode=1;

                }
                else
                {
                  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                    if((lsesps16AHBGEN3mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #else
                    if((mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #endif
                    {                                           /* then use mpress force */
                        WL_ESC->mpress_rise_mode = 1;
                        WL_ESC->TEMP_HV_VL=0;
		                WL_ESC->TEMP_AV_VL=0;
		
		                WL_ESC->esp_control_mode=1;

                    }
                    else
                    {
                       WL_ESC->mpress_rise_mode = 0;
                    }
                }
            }
            else /* mpress_rise_mode==1 */
            {
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0;

                WL_ESC->esp_control_mode=1;

            }
        }
        else
        {
            WL_ESC->mpress_rise_mode = 0;
        }
    }
    else
    {
        ;
    }


    VDC_MOTOR_ON=1;
    
}

void LAESP_vActuateNcNovalveR(struct ESC_STRUCT *WL_ESC,struct W_STRUCT *WL)
{
/*
	#if (__MGH60_ESC_IMPROVE_CONCEPT==1) 
  
    if(WL_ESC->SLIP_CONTROL_NEED==1)
    {
    	esp_tempW1=McrAbs(WL->u8_Estimated_Active_Press);
    	
		  esp_tempW2=LCESP_s16IInter4Point( vrefk, 	S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, S16_ESP_REAR_PRESS_LMT_H_MU_S_V,
		                                            S16_ESP_SLIP_SPEED_GAIN_DEP_L_V, S16_ESP_REAR_PRESS_LMT_H_MU_L_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, S16_ESP_REAR_PRESS_LMT_H_MU_M_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_H_V, S16_ESP_REAR_PRESS_LMT_H_MU_H_V ); 	// High-mu, Hold_Press_Th    	
    }
    else
  	{
    	esp_tempW1=0;
    	esp_tempW2=1;
  	}
  	
 	#if (__Ext_Under_Ctrl == 1)
    if((esp_tempW1 > esp_tempW2)&&(det_alatm >= 600)&&(EXTREME_UNDER_CONTROL==0)&&(SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0))
	#else
		if((esp_tempW1 > esp_tempW2)&&(det_alatm >= 600)&&(SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0))
	#endif
    {
    		WL_ESC->REAR_PRESSURE_HOLD = 1 ;
  	}
  	else
		{
    		WL_ESC->REAR_PRESSURE_HOLD = 0 ;
		}  
 
	#endif
*/
		
    if(WL_ESC->SLIP_CONTROL_NEED==1)
    {
    	if((lcu1ABSCBSCombCntrl_RL==1)||(lcu1ABSCBSCombCntrl_RR==1))
      {
      	WL_ESC->rate_counter=0;
        WL_ESC->dump_counter_assurance_flag = 0;
        
        WL_ESC->TEMP_HV_VL=0;
        WL_ESC->TEMP_AV_VL=0;
        WL_ESC->esp_control_mode=1;
      }

#if (__Pro_Active_Roll_Ctrl==1)&&(__ROP == 1 )     
      else if((WL_ESC->slip_control< WL_ESC->slip_rise_thr_change)||((WL_ESC->lcu1ropRearCtrlReq == 1)&&(WL_ESC->slip_m >= -WHEEL_SLIP_5)))    

#else
      else if(WL_ESC->slip_control< WL_ESC->slip_rise_thr_change)
        	
#endif
        {
	        WL_ESC->rate_counter=0;
			WL_ESC->dump_counter_assurance_flag=0;
			
        #if (__Pro_Active_Roll_Ctrl == 1)&&(__ROP == 1 )  
            if((WL_ESC->REAR_PRESSURE_HOLD==1)||(WL_ESC->rop_rear_ctrl_mode == 2))    
        #else
            if(WL_ESC->REAR_PRESSURE_HOLD==1)
        #endif			 
            {
                WL_ESC->TEMP_HV_VL=1;
                WL_ESC->TEMP_AV_VL=0;
                WL_ESC->esp_control_mode=3;


            }
        
           
            else
            {
                WL_ESC->TEMP_HV_VL=0;
                WL_ESC->TEMP_AV_VL=0;
#if ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 0))
								if(ABS_fz==1)/*((esp_control_cycle_rl==1)||(MPRESS_BRAKE_ON==1))*/
                {
                	WL_ESC->esp_control_mode=2;

                }
                else if(WL_ESC->slip_control_need_counter < variable_rise_scan_rear)
                {
                    WL_ESC->esp_control_mode=1;

                }
                else
                {
                	WL_ESC->esp_control_mode=2;

                }
#elif ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 1))
 									WL_ESC->esp_control_mode=1;

#else
                	WL_ESC->esp_control_mode=1;

#endif
                if((mpress_slop_filt>=80)&&(ABS_fz==0))
                {
                    WL_ESC->esp_control_mode=1;

                }
                else
                {
                    ;
                }

            }
        }
#if Drum_Brake        
        else if((WL_ESC->slip_control>=WL_ESC->slip_rise_thr_change)&&(WL_ESC->slip_control<WL_ESC->slip_dump_thr_change) 
             &&(WL_ESC->esp_pressure_count >=0))
#else
        else if((WL_ESC->slip_control>=WL_ESC->slip_rise_thr_change)&&(WL_ESC->slip_control<WL_ESC->slip_dump_thr_change)
            &&(WL_ESC->esp_pressure_count>=0))
#endif  
        {
			if(WL_ESC->dump_counter_assurance_flag == 0)
			{
				WL_ESC->rate_counter=0;
			}
			else
			{
			    WL_ESC->rate_counter++;
			    		
        		if(WL_ESC->rate_counter>WL_ESC->rate_interval)
				{
			    	WL_ESC->rate_counter=0;
			        WL_ESC->dump_counter_assurance_flag=0;
				}
				else
				{
					;
				}
			}
            WL_ESC->TEMP_HV_VL=1;
            WL_ESC->TEMP_AV_VL=0;
            WL_ESC->esp_control_mode=3;

        }
        else
        {
                if(WL_ESC->FLAG_COMPULSION_HOLD==1)
                {
                    WL_ESC->TEMP_HV_VL=1;
                    WL_ESC->TEMP_AV_VL=0;
                    WL_ESC->esp_control_mode=3;

                }
                else
                {
            if((WL_ESC->rate_counter==0) || (WL_ESC->FLAG_NULL_DUMP == 1))
            {
                    WL_ESC->TEMP_HV_VL=1;
                    WL_ESC->TEMP_AV_VL=1;
                    WL_ESC->esp_control_mode=4;

                }
            else
            {
                WL_ESC->TEMP_HV_VL=1;
                WL_ESC->TEMP_AV_VL=0;
                WL_ESC->esp_control_mode=3;

            }
          }

	        WL_ESC->dump_counter_assurance_flag=1;
		    WL_ESC->rate_counter++;
		    				
    		if(WL_ESC->rate_counter>WL_ESC->rate_interval)
			{
				WL_ESC->rate_counter=0;
			}
					  else if(WL_ESC->FLAG_COMPULSION_HOLD == 1)
			{
				WL_ESC->rate_counter=0;
			}
			else
			{
				;
			}
        }

    }
    else
    {
        ;
    }

    VDC_MOTOR_ON=1;
    
}
#else
void LAESP_vActuateNcNovalveF(void)
{
    LCESP_vDetectFullRise();

    tempW0=(md_delta_mpress/S16_ESP_PRESSURE_DUTY_COM);

/********SLIP CONTROL NEED FL*************************/
    if((SLIP_CONTROL_NEED_FL==1)&&(slip_status_position==1))        
    {
        if((slip_control_front <= slip_f_rise_thr_change)||(FLAG_COMPULSION_RISE==1))
        {
            rate_counter_front=0;
            dump_counter_assurance_flag_front = 0;
            
				#if	( __Ext_Under_Ctrl == 1 )	 
            
            if((FRONT_PRESSURE_HOLD==1)||
            	 ((EXTREME_UNDER_CONTROL==1)&&(FL.u8_Estimated_Active_Press >= Extr_Under_Frt_fl_Press_Max)))
					
				#else					
				
            if(FRONT_PRESSURE_HOLD==1)
            	
				#endif	
            {
                TEMP_HV_VL_fl=1;
                TEMP_AV_VL_fl=0;
                esp_control_mode_fl=3;
                TCS_built_reapply_pattern_fl=0;
            }
            else
            {
                TEMP_HV_VL_fl=0;
                TEMP_AV_VL_fl=0;

#if ESC_PULSE_UP_RISE
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((ABS_fz==1)||(lsespu1AHBGEN3MpresBrkOn==1))
              #else
                if((ABS_fz==1)||(MPRESS_BRAKE_ON==1))
              #endif               
                {
                		esp_control_mode_fl=1;
                    TCS_built_reapply_pattern_fl=0;
                }
#else
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((esp_control_cycle_fl==1)||(lsespu1AHBGEN3MpresBrkOn==1))
              #else
                if((esp_control_cycle_fl==1)||(MPRESS_BRAKE_ON==1))
              #endif    
                {
                	if(FULL_RISE_MODE == 1)
                	{
                		esp_control_mode_fl=1;
                    	TCS_built_reapply_pattern_fl=0;
                	}
                	else
                	{
	                    esp_control_mode_fl=2;
	                    TCS_built_reapply_pattern_fl=3;
	                }
                    if((mpress_slop_filt>=80)&&(ABS_fz==0))
                    {
                        esp_control_mode_fl=1;
                        TCS_built_reapply_pattern_fl=0;
                    }
                    else
                    {
                        ;
                    }
                }
#endif
                else
                {
                        esp_control_mode_fl=1;
                        TCS_built_reapply_pattern_fl=1;
                }
            }
        }
        else if((slip_control_front>slip_f_rise_thr_change)&&(slip_control_front<slip_f_dump_thr_change)
            &&(esp_pressure_count_fl>=0))
        {
			if(dump_counter_assurance_flag_front == 0)
			{
				rate_counter_front=0;
				}
			else
			{
			    rate_counter_front++;
      		 		if(rate_counter_front>rate_interval_front)
				{
			    	rate_counter_front=0;
			        dump_counter_assurance_flag_front = 0;
				}
				else
				{
					;
				}
			}
            TEMP_HV_VL_fl=1;
            TEMP_AV_VL_fl=0;
            esp_control_mode_fl=3;
            TCS_built_reapply_pattern_fl=0;
        }
        else
        {
                if(FLAG_COMPULSION_HOLD==1)
                {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=0;
                    esp_control_mode_fl=3;
                    TCS_built_reapply_pattern_fl=0;
                }
                else
                {
            if(rate_counter_front==0)
            {
                     TEMP_HV_VL_fl=1;
                     TEMP_AV_VL_fl=1;
                     esp_control_mode_fl=4;
                    TCS_built_reapply_pattern_fl=0;
                }
            else
            {
                 if(esp_pressure_count_fl<0)
                 {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=1;
                    esp_control_mode_fl=4;
                    TCS_built_reapply_pattern_fl=0;
                 }
                 else
                 {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=0;
                    esp_control_mode_fl=3;
                    TCS_built_reapply_pattern_fl=0;
                 }
            }
          }

       		dump_counter_assurance_flag_front = 1;
		    rate_counter_front++;
         	if(rate_counter_front>rate_interval_front)
			{
				rate_counter_front=0;
			}
			else if(FLAG_COMPULSION_HOLD == 1)
			{
				rate_counter_front=0;
			}
			else
			{
				;
			}
        }
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        if((!ABS_fz)&&(lsesps16AHBGEN3mpress>300) && (slip_m_fl >= -S16_MPRESS_RISE_MODE_ENT_SLIP))     /* mpress > Wheel press */
      #else
       if((!ABS_fz)&&(mpress>300) && (slip_m_fl >= -S16_MPRESS_RISE_MODE_ENT_SLIP))     /* mpress > Wheel press */
      #endif    
        {
            if(mpress_rise_mode==0)
            {
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((lsesps16AHBGEN3mpress<600)&&(mpress_slop_filt>80)) /* mpress_slop >> motor_press_slop */
              #else        
                if((mpress<600)&&(mpress_slop_filt>80)) /* mpress_slop >> motor_press_slop */
              #endif    
                {                                       /* then use mpress force */
                    mpress_rise_mode = 1;
                    TEMP_HV_VL_fl=0;
	                TEMP_AV_VL_fl=0;
	
	                esp_control_mode_fl=1;
	                TCS_built_reapply_pattern_fl=0;
                }
                else
                {
                  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                    if((lsesps16AHBGEN3mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #else
                    if((mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #endif
                    {                                           /* then use mpress force */
                        mpress_rise_mode = 1;
                        TEMP_HV_VL_fl=0;
		                TEMP_AV_VL_fl=0;
		
		                esp_control_mode_fl=1;
		                TCS_built_reapply_pattern_fl=0;
                    }
                    else
                    {
                        mpress_rise_mode = 0;
                    }
                }
            }
            else /* mpress_rise_mode==1 */
            {
                TEMP_HV_VL_fl=0;
                TEMP_AV_VL_fl=0;

                esp_control_mode_fl=1;
                TCS_built_reapply_pattern_fl=0;
            }
        }
        else
        {
            mpress_rise_mode = 0;
        }
    }
    else
    {
        ;
    }
/*******SLIP CONTROL NEED FR*************************/
    if((SLIP_CONTROL_NEED_FR==1)&&(slip_status_position==2))
    {
        if((slip_control_front<= slip_f_rise_thr_change)||(FLAG_COMPULSION_RISE==1))
        {
            rate_counter_front=0;
            dump_counter_assurance_flag_front = 0;
            
				#if	( __Ext_Under_Ctrl == 1 )	 
            
            if((FRONT_PRESSURE_HOLD==1)||
            	 ((EXTREME_UNDER_CONTROL==1)&&(FR.u8_Estimated_Active_Press >= Extr_Under_Frt_fr_Press_Max)))
					
				#else					
				
            if(FRONT_PRESSURE_HOLD==1)
            	
				#endif	 
            {
                TEMP_HV_VL_fr=1;
                TEMP_AV_VL_fr=0;
                esp_control_mode_fr=3;
                TCS_built_reapply_pattern_fr=0;
            }
            else
            {
                TEMP_HV_VL_fr=0;
                TEMP_AV_VL_fr=0;
#if ESC_PULSE_UP_RISE
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((ABS_fz==1)||(lsespu1AHBGEN3MpresBrkOn==1))
              #else
                if((ABS_fz==1)||(MPRESS_BRAKE_ON==1))
              #endif                
                {
                		esp_control_mode_fr=1;
                    TCS_built_reapply_pattern_fr=0;
                }
#else
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((esp_control_cycle_fr==1)||(lsespu1AHBGEN3MpresBrkOn==1))
              #else
                if((esp_control_cycle_fr==1)||(MPRESS_BRAKE_ON==1))            
              #endif
                {
                	if(FULL_RISE_MODE == 1)
                	{
                		esp_control_mode_fr=1;
                    	TCS_built_reapply_pattern_fr=0;
                	}
                	else
                	{
                    	esp_control_mode_fr=2;
                    	TCS_built_reapply_pattern_fr=3;
                    }
                    if((mpress_slop_filt>=80)&&(!ABS_fz))
                    {
                        esp_control_mode_fr=1;
                        TCS_built_reapply_pattern_fr=0;
                    }
                    else
                    {
                        ;
                    }
                }
#endif
                else
                {
                        esp_control_mode_fr=1;
                        TCS_built_reapply_pattern_fr=1;
                }
            }
        }
        else if((slip_control_front>slip_f_rise_thr_change)&&(slip_control_front<slip_f_dump_thr_change)
                &&(esp_pressure_count_fr>=0))
        {
			if(dump_counter_assurance_flag_front == 0)
			{
				rate_counter_front=0;
			}
			else
			{
			    rate_counter_front++;			    
         		if(rate_counter_front>rate_interval_front)
				{
			    	rate_counter_front=0;
			         dump_counter_assurance_flag_front = 0;
				}
				else
				{
					;
				}
			}
            TEMP_HV_VL_fr=1;
            TEMP_AV_VL_fr=0;
            esp_control_mode_fr=3;
            TCS_built_reapply_pattern_fr=0;

        }
        else
        {
                if(FLAG_COMPULSION_HOLD==1)
                {
                    TEMP_HV_VL_fr=1;
                    TEMP_AV_VL_fr=0;
                    esp_control_mode_fr=3;
                    TCS_built_reapply_pattern_fr=0;
                }
                else
                {
            if(rate_counter_front==0)
            {
                    TEMP_HV_VL_fr=1;
                    TEMP_AV_VL_fr=1;
                    esp_control_mode_fr=4;
                    TCS_built_reapply_pattern_fr=0;
                }
            else
            {
                if(esp_pressure_count_fr < 0)
                {
                    TEMP_HV_VL_fr=1;
                    TEMP_AV_VL_fr=1;
                    esp_control_mode_fr=4;
                    TCS_built_reapply_pattern_fr=0;
                }
                else
                {
                    TEMP_HV_VL_fr=1;
                    TEMP_AV_VL_fr=0;
                    esp_control_mode_fr=3;
                    TCS_built_reapply_pattern_fr=0;
                }
            }
          }

        	dump_counter_assurance_flag_front = 1;
		    rate_counter_front++;
         	 if(rate_counter_front>rate_interval_front)
			{
				rate_counter_front=0;
			}
			else if(FLAG_COMPULSION_HOLD == 1)
			{
				rate_counter_front=0;
			}
			else
			{
				;
			}
            
        }
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        if((!ABS_fz)&&(lsesps16AHBGEN3mpress>300) && (slip_m_fr >= -S16_MPRESS_RISE_MODE_ENT_SLIP))     /* mpress > Wheel press */
      #else
        if((!ABS_fz)&&(mpress>300) && (slip_m_fr >= -S16_MPRESS_RISE_MODE_ENT_SLIP))     /* mpress > Wheel press */
      #endif
        {
            if(mpress_rise_mode==0)
            {
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if((lsesps16AHBGEN3mpress<600)&&(mpress_slop_filt>80)) /* mpress_slop >> motor_press_slop */
              #else
                if((mpress<600)&&(mpress_slop_filt>80)) /* mpress_slop >> motor_press_slop */
              #endif
                {                                       /* then use mpress force */
                    mpress_rise_mode = 1;
                    TEMP_HV_VL_fr=0;
	                TEMP_AV_VL_fr=0;
	
	                esp_control_mode_fr=1;
	                TCS_built_reapply_pattern_fr=0;
                }
                else
                {
                  #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                    if((lsesps16AHBGEN3mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #else
                    if((mpress>=600)&&(mpress_slop_filt>160))   /* mpress_slop >> motor_press_slop */
                  #endif
                    {                                           /* then use mpress force */
                        mpress_rise_mode = 1;
                        TEMP_HV_VL_fr=0;
		                TEMP_AV_VL_fr=0;
		
		                esp_control_mode_fr=1;
		                TCS_built_reapply_pattern_fr=0 ;
                    }
                    else
                    {
                        mpress_rise_mode = 0;
                    }
                }
            }
            else /* mpress_rise_mode==1 */
            {
                TEMP_HV_VL_fr=0;
                TEMP_AV_VL_fr=0;

                esp_control_mode_fr=1;
                TCS_built_reapply_pattern_fr=0;
            }
        }
        else
        {
            mpress_rise_mode = 0;
        }
    }
    else
    {
        ;
    }

    VDC_MOTOR_ON=1;


    if((SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FR==0))
    {
    	mpress_rise_mode = 0;
    }
    else {;}
}

void LAESP_vActuateNcNovalveR(void)
{
    tempW0=(md_delta_mpress/S16_ESP_PRESSURE_DUTY_COM);

	#if (__MGH60_ESC_IMPROVE_CONCEPT==1) 
  
    if(SLIP_CONTROL_NEED_RL==1)
    {
    	esp_tempW1=McrAbs(RL.u8_Estimated_Active_Press);
    	
		  esp_tempW2=LCESP_s16IInter4Point( vrefk, 	S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, S16_ESP_REAR_PRESS_LMT_H_MU_S_V,
		                                            S16_ESP_SLIP_SPEED_GAIN_DEP_L_V, S16_ESP_REAR_PRESS_LMT_H_MU_L_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, S16_ESP_REAR_PRESS_LMT_H_MU_M_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_H_V, S16_ESP_REAR_PRESS_LMT_H_MU_H_V ); 	/* High-mu, Hold_Press_Th */    	
    }
    else if(SLIP_CONTROL_NEED_RR==1)
    {
    	esp_tempW1=McrAbs(RR.u8_Estimated_Active_Press);
		  esp_tempW2=LCESP_s16IInter4Point( vrefk, 	S16_ESP_SLIP_SPEED_GAIN_DEP_S_V, S16_ESP_REAR_PRESS_LMT_H_MU_S_V,
		                                            S16_ESP_SLIP_SPEED_GAIN_DEP_L_V, S16_ESP_REAR_PRESS_LMT_H_MU_L_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_M_V, S16_ESP_REAR_PRESS_LMT_H_MU_M_V,
                                          			S16_ESP_SLIP_SPEED_GAIN_DEP_H_V, S16_ESP_REAR_PRESS_LMT_H_MU_H_V ); 	/* High-mu, Hold_Press_Th */    	
    }
    else
  	{
    	esp_tempW1=0;
    	esp_tempW2=1;
  	}
  	
 	#if (__Ext_Under_Ctrl == 1)
    if((esp_tempW1 > esp_tempW2)&&(det_alatm >= 600)&&(EXTREME_UNDER_CONTROL==0)&&(SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0))
	#else
		if((esp_tempW1 > esp_tempW2)&&(det_alatm >= 600)&&(SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0))
	#endif
    {
    		REAR_PRESSURE_HOLD = 1 ;
  	}
  	else
		{
    		REAR_PRESSURE_HOLD = 0 ;
		}  
 
	#endif
		
    if((SLIP_CONTROL_NEED_RL==1)&&(slip_status_position==3)) 
    {
#if (__Pro_Active_Roll_Ctrl==1)&&(__ROP == 1 )     
        if((slip_control_rear< slip_r_rise_thr_change)||((lcu1ropRearCtrlReqRL == 1)&&(slip_m_rl >= -WHEEL_SLIP_5)))    

#else
        if(slip_control_rear< slip_r_rise_thr_change)
        	
#endif
        {
	        rate_counter_rear=0;
			dump_counter_assurance_flag_rear=0;
			
        #if (__Pro_Active_Roll_Ctrl == 1)&&(__ROP == 1 )  
            if((REAR_PRESSURE_HOLD==1)||(rop_rear_ctrl_mode_rl == 2))    
        #else
            if(REAR_PRESSURE_HOLD==1)
        #endif			 
            {
                TEMP_HV_VL_rl=1;
                TEMP_AV_VL_rl=0;
                esp_control_mode_rl=3;
                TCS_built_reapply_pattern_rl=0;

            }
            else{
                TEMP_HV_VL_rl=0;
                TEMP_AV_VL_rl=0;
#if ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 0))
								if(ABS_fz==1)/*((esp_control_cycle_rl==1)||(MPRESS_BRAKE_ON==1))*/
                {
                	esp_control_mode_rl=2;
                	TCS_built_reapply_pattern_rl=3;
                }
                else if(slip_control_need_counter_rl < variable_rise_scan_rear)
                {
                    esp_control_mode_rl=1;
                	 TCS_built_reapply_pattern_rl=0; 
                }
                else
                {
                	esp_control_mode_rl=2;
                	TCS_built_reapply_pattern_rl=3;
                }
#elif ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 1))
 									esp_control_mode_rl=1;
                	TCS_built_reapply_pattern_rl=0;
#else
		#if  ESC_PULSE_UP_RISE

                	esp_control_mode_rl=1;
                	TCS_built_reapply_pattern_rl=1;

        #else
        				esp_control_mode_rl=2;
                TCS_built_reapply_pattern_rl=3;     
        #endif
#endif
                if((mpress_slop_filt>=80)&&(ABS_fz==0))
                {
                    esp_control_mode_rl=1;
                    TCS_built_reapply_pattern_rl=0;
                }
                else
                {
                    ;
                }

            }
        }
#if Drum_Brake        
        else if((slip_control_rear>=slip_r_rise_thr_change)&&(slip_control_rear<slip_r_dump_thr_change) 
             &&(esp_pressure_count_rl >=0))
#else
        else if((slip_control_rear>=slip_r_rise_thr_change)&&(slip_control_rear<slip_r_dump_thr_change)
            &&(esp_pressure_count_rl>=0))
#endif  
        {
			if(dump_counter_assurance_flag_rear == 0)
			{
				rate_counter_rear=0;
			}
			else
			{
			    rate_counter_rear++;
        		if(rate_counter_rear>rate_interval_rear)
				{
			    	rate_counter_rear=0;
			        dump_counter_assurance_flag_rear=0;
				}
				else
				{
					;
				}
			}
            TEMP_HV_VL_rl=1;
            TEMP_AV_VL_rl=0;
            esp_control_mode_rl=3;
            TCS_built_reapply_pattern_rl=0;
        }
        else
        {
                if(FLAG_COMPULSION_HOLD_REAR==1)
                {
                    TEMP_HV_VL_rl=1;
                    TEMP_AV_VL_rl=0;
                    esp_control_mode_rl=3;
                    TCS_built_reapply_pattern_rl=0;
                }
                else
                {
            if((rate_counter_rear==0) || (FLAG_NULL_DUMP_R == 1))
            {
                    TEMP_HV_VL_rl=1;
                    TEMP_AV_VL_rl=1;
                    esp_control_mode_rl=4;
                    TCS_built_reapply_pattern_rl=0;
                }
            else
            {
                TEMP_HV_VL_rl=1;
                TEMP_AV_VL_rl=0;
                esp_control_mode_rl=3;
                TCS_built_reapply_pattern_rl=0;
            }
          }

	        dump_counter_assurance_flag_rear=1;
		    rate_counter_rear++;
    		if(rate_counter_rear>rate_interval_rear)
			{
				rate_counter_rear=0;
			}
					  else if(FLAG_COMPULSION_HOLD_REAR == 1)
			{
				rate_counter_rear=0;
			}
			else
			{
				;
			}
        }

    }
    else
    {
        ;
    }

    if((SLIP_CONTROL_NEED_RR==1)&&(slip_status_position==4)) 
    {
    #if (__Pro_Active_Roll_Ctrl == 1)&&(__ROP == 1 )    
        if((slip_control_rear< slip_r_rise_thr_change)||((lcu1ropRearCtrlReqRR == 1)&&(slip_m_rr >= -WHEEL_SLIP_5)))    
    #else
        if(slip_control_rear< slip_r_rise_thr_change)
    #endif         
        {
        	rate_counter_rear=0;
	        dump_counter_assurance_flag_rear=0;
	        
        #if (__Pro_Active_Roll_Ctrl == 1)&&(__ROP == 1 )    
            if((REAR_PRESSURE_HOLD==1)||(rop_rear_ctrl_mode_rr == 2))    
        #else
            if(REAR_PRESSURE_HOLD==1)
        #endif			 
            {
                TEMP_HV_VL_rr=1;
                TEMP_AV_VL_rr=0;
                esp_control_mode_rr=3;
                TCS_built_reapply_pattern_rr=0;
            }
            else{
                TEMP_HV_VL_rr=0;
                TEMP_AV_VL_rr=0;
#if ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 0))
				if(ABS_fz==1)/*((esp_control_cycle_rr==1)||(MPRESS_BRAKE_ON==1))*/
                {
                	esp_control_mode_rr=2;
                	TCS_built_reapply_pattern_rr=3;
                }
                else if(slip_control_need_counter_rr < variable_rise_scan_rear)
                {
                    esp_control_mode_rr=1;
                	TCS_built_reapply_pattern_rr=0; 
                }
                else
                {
                	esp_control_mode_rr=2;
                	TCS_built_reapply_pattern_rr=3;
                }
#elif ((__REAR_FULL_RISE==1)&&(ESC_PULSE_UP_RISE == 1))
 									esp_control_mode_rr=1;
                	TCS_built_reapply_pattern_rr=0;
#else
  		#if ESC_PULSE_UP_RISE
                	esp_control_mode_rr=1;
                	TCS_built_reapply_pattern_rr=1;
        #else
        			esp_control_mode_rr=2;
                	TCS_built_reapply_pattern_rr=3;
        #endif

#endif
                if((mpress_slop_filt>=80)&&(ABS_fz==0))
                {
                    esp_control_mode_rr=1;
                    TCS_built_reapply_pattern_rr=0;
                }
                else
                {
                    ;
                }
            }
        }
#if Drum_Brake        
        else if((slip_control_rear>=slip_r_rise_thr_change)&&(slip_control_rear<slip_r_dump_thr_change) 
             &&(esp_pressure_count_rr >=0))
#else
        else if((slip_control_rear>=slip_r_rise_thr_change)&&(slip_control_rear<slip_r_dump_thr_change) 
             &&(esp_pressure_count_rr >=0))
#endif            
        {
			if(dump_counter_assurance_flag_rear == 0)
			{
				rate_counter_rear=0;
			}
			else
			{
			    rate_counter_rear++;
         		if(rate_counter_rear>rate_interval_rear)
				{
			    	rate_counter_rear=0;
			        dump_counter_assurance_flag_rear=0;
				}
				else
				{
					;
				}
			}
            TEMP_HV_VL_rr=1;
            TEMP_AV_VL_rr=0;
            esp_control_mode_rr=3;
            TCS_built_reapply_pattern_rr=0;
        }
        else
        {
                if(FLAG_COMPULSION_HOLD_REAR==1)
                {
                    TEMP_HV_VL_rr=1;
                    TEMP_AV_VL_rr=0;
                    esp_control_mode_rr=3;
                    TCS_built_reapply_pattern_rr=0;
                }
                else
                {
            if((rate_counter_rear==0) || (FLAG_NULL_DUMP_R == 1))
            {
                    TEMP_HV_VL_rr=1;
                    TEMP_AV_VL_rr=1;
                    esp_control_mode_rr=4;
                    TCS_built_reapply_pattern_rr=0;
                }
            else
            {
                TEMP_HV_VL_rr=1;
                TEMP_AV_VL_rr=0;
                esp_control_mode_rr=3;
                TCS_built_reapply_pattern_rr=0;
            }
          }

	        dump_counter_assurance_flag_rear=1;
		    rate_counter_rear++;
    		if(rate_counter_rear>rate_interval_rear)
			{
				rate_counter_rear=0;
			}
					  else if(FLAG_COMPULSION_HOLD_REAR == 1)
			{
				rate_counter_rear=0;
			}
			else
			{
				;
			}
        }

    }
    else
    {
        ;
    }

    VDC_MOTOR_ON=1;

}
#endif

#if Drum_Brake
void LAESP_vCheckDumpCycle(void)
{
    if((ACT_TWO_WHEEL==0)&&(SLIP_CONTROL_NEED_RL==1))
    {
        if((dump_cycle==0)&&(AV_VL_rl == 0))
        {
            dump_cycle = 0;
        }
        else if((dump_cycle==0)&&(AV_VL_rl == 1))
        {
            dump_cycle = 1;
        }
        else if((dump_cycle==1)&&(AV_VL_rl == 1))
        {
            dump_cycle = 2;
        }
        else
        {
            ;
        }
    }
    else if((ACT_TWO_WHEEL==0)&&(SLIP_CONTROL_NEED_RR==1))
    {
        if((dump_cycle==0)&&(AV_VL_rr == 0))
        {
            dump_cycle = 0;
        }
        else if((dump_cycle==0)&&(AV_VL_rr == 1))
        {
            dump_cycle = 1;
        }
        else if((dump_cycle==1)&&(AV_VL_rr == 1))
        {
            dump_cycle = 2;
        }
        else
        {
            ;
        }
    }
    else 
    {
        dump_cycle = 0;
    }
}
#endif

void LAESP_vCalMototTime(void)
{
      #if (__INITIAL_MTR_DRIVE_IMPROVE == 1)
    if ((SLIP_CONTROL_NEED_FL == 1)||(SLIP_CONTROL_NEED_FR == 1))
    {
        esp_tempW0 = LCESP_s16IInter4Point(vref5, S16_ESC_INIT_MTR_SCAN_REF_SPD1, S16_ESC_INIT_MTR_SCAN_HMU_SPD1, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD2, S16_ESC_INIT_MTR_SCAN_HMU_SPD2,
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD3, S16_ESC_INIT_MTR_SCAN_HMU_SPD3,
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD4, S16_ESC_INIT_MTR_SCAN_HMU_SPD4);
                                                    
        esp_tempW1 = LCESP_s16IInter4Point(vref5, S16_ESC_INIT_MTR_SCAN_REF_SPD1, S16_ESC_INIT_MTR_SCAN_MMU_SPD1, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD2, S16_ESC_INIT_MTR_SCAN_MMU_SPD2, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD3, S16_ESC_INIT_MTR_SCAN_MMU_SPD3, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD4, S16_ESC_INIT_MTR_SCAN_MMU_SPD4);
        
        esp_tempW2 = LCESP_s16IInter4Point(vref5, S16_ESC_INIT_MTR_SCAN_REF_SPD1, S16_ESC_INIT_MTR_SCAN_LMU_SPD1, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD2, S16_ESC_INIT_MTR_SCAN_LMU_SPD2, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD3, S16_ESC_INIT_MTR_SCAN_LMU_SPD3, 
                                                  S16_ESC_INIT_MTR_SCAN_REF_SPD4, S16_ESC_INIT_MTR_SCAN_LMU_SPD4);
        
        msc_rise_scan = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW0, esp_tempW1, esp_tempW2); 
    }
    
     #else
    if ( SLIP_CONTROL_NEED_FL==1 ) 
    {
        msc_count_enable_fr = 0 ;
        msc_rise_scan_fr = 28 ;
        del_need_count_fr = 0 ;
        if  (msc_count_enable_fl==0)
        {
            esp_tempW3 = slip_control_need_counter_fl-3 ;
            if ( (esp_mu<=lsesps16MuMedHigh)&&(vrefk <= 500)&&(esp_pressure_count_fl <= esp_tempW3) )
            {
                msc_count_enable_fl = 1 ;
                msc_rise_scan_fl = msc_rise_scan_fl - 3 ;
                if ( msc_rise_scan_fl < 14 )
                {
                    msc_rise_scan_fl = 14 ;
                }
                else
                {
                    ;
                }
                del_need_count_fl = del_need_count_fl + 1 ;
            }
            else
            {
                ;
            }
        } else {
            esp_tempW4 = slip_control_need_counter_fl - del_need_count_fl - 3;
            if ((esp_mu<=lsesps16MuMedHigh)&&(vrefk <= 500)&&(esp_pressure_count_fl <= esp_tempW4 ) )
            {
                msc_rise_scan_fl = msc_rise_scan_fl - 3 ;
                if ( msc_rise_scan_fl < 14 )
                {
                    msc_rise_scan_fl = 14 ;
                }
                else
                {
                    ;
                }
                del_need_count_fl = del_need_count_fl + 1 ;
            } else {
                msc_rise_scan_fl = msc_rise_scan_fl + 2 ;
                if ( msc_rise_scan_fl >= 28 )
                {
                     msc_rise_scan_fl = 28 ;
                }
                else
                {
                    ;
                }
            }
        }
        msc_rise_scan = msc_rise_scan_fl ;
    } else if ( SLIP_CONTROL_NEED_FR ==1) {

        msc_count_enable_fl = 0 ;
        msc_rise_scan_fl = 28 ;
        del_need_count_fl = 0 ;
        if (msc_count_enable_fr==0)
        {
            esp_tempW1 = slip_control_need_counter_fr-3 ;
            if ( (esp_mu<=lsesps16MuMedHigh)&&(vrefk <= 500)&&(esp_pressure_count_fr <= esp_tempW1 ) )
            {
                msc_count_enable_fr = 1 ;
                msc_rise_scan_fr = msc_rise_scan_fr - 3 ;
                if ( msc_rise_scan_fr < 14 )
                {
                    msc_rise_scan_fr = 14 ;
                }
                else
                {
                    ;
                }
                del_need_count_fr = del_need_count_fr + 1 ;
            }
        } else {
            esp_tempW2 = slip_control_need_counter_fr - del_need_count_fr - 3;
            if ( (esp_mu<=lsesps16MuMedHigh)&&(vrefk <= 500)&&(esp_pressure_count_fr <= esp_tempW2 ) )
            {
                msc_rise_scan_fr = msc_rise_scan_fr - 3 ;
                if ( msc_rise_scan_fr < 14 )
                {
                     msc_rise_scan_fr = 14 ;
                }
                else
                {
                    ;
                }
                del_need_count_fr = del_need_count_fr + 1 ;

            } else {
                msc_rise_scan_fr = msc_rise_scan_fr + 2 ;
                if ( msc_rise_scan_fr >= 28 )
                {
                     msc_rise_scan_fr = 28 ;
                }
                else
                {
                    ;
                }
            }
        }
        msc_rise_scan = msc_rise_scan_fr ;
    } 
    #endif
    else if ( (SLIP_CONTROL_NEED_FR==0) && (SLIP_CONTROL_NEED_FL==0 )) 
    {
        msc_count_enable_fl = 0 ;
        msc_count_enable_fr = 0 ;
        msc_rise_scan_fl = 28 ;
        msc_rise_scan_fr = 28 ;
        del_need_count_fl = 0 ;
        del_need_count_fr = 0 ;
        msc_rise_scan = 28 ;
    }
    else
    {
        ;
    }
}

void LAESP_vCalNcDutyInEsp(void)
{
	#if Drum_Brake
	LAESP_vCheckDumpCycle();
	#endif
    
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((SLIP_CONTROL_NEED_FL==1)&&(lsespu1AHBGEN3MpresBrkOn==0)&&(BTCS_ON==0))
  #else
    if((SLIP_CONTROL_NEED_FL==1)&&(MPRESS_BRAKE_ON==0)&&(BTCS_ON==0))
  #endif

    {
    	#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      if((TEMP_AV_VL_fl==1)&&(FL_ESC.slip_control<0)&&(FL_ESC.slip_control>=(FL_ESC.slip_dump_thr_change))
            &&(slip_m_fl >(-1000)))
      #else
        if((TEMP_AV_VL_fl==1)&&(slip_control_front<0)&&(slip_control_front>=(slip_f_dump_thr_change))
            &&(slip_m_fl >(-1000)))
      #endif
        {
            NC_VALVE_DUTY_CONTROL_FRONT = 1;
            if(esp_pressure_count_fl >= 28)
            {
                esp_slip_nc_duty_front = 5;
            }
            else
            {
                esp_slip_nc_duty_front = 10;
            }
        }
        else
        {
            NC_VALVE_DUTY_CONTROL_FRONT = 0;
            esp_slip_nc_duty_front=10;
        }
    }
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    else if ((SLIP_CONTROL_NEED_FR==1)&&(lsespu1AHBGEN3MpresBrkOn==0)&&(BTCS_ON==0))
  #else
    else if ((SLIP_CONTROL_NEED_FR==1)&&(MPRESS_BRAKE_ON==0)&&(BTCS_ON==0))
  #endif    
    {
    	#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      if((TEMP_AV_VL_fr==1)&&(FR_ESC.slip_control<0)&&(FR_ESC.slip_control>=(FR_ESC.slip_dump_thr_change))
            &&(slip_m_fr >(-1000)))
      #else
        if((TEMP_AV_VL_fr==1)&&(slip_control_front<0)&&(slip_control_front>=(slip_f_dump_thr_change))
            &&(slip_m_fr >(-1000)))
      #endif
        {
            NC_VALVE_DUTY_CONTROL_FRONT = 1;
            if(esp_pressure_count_fr >= 28)
            {
                esp_slip_nc_duty_front = 5;
            }
            else
            {
                esp_slip_nc_duty_front = 10;
            }
        }
        else
        {
            NC_VALVE_DUTY_CONTROL_FRONT = 0;
            esp_slip_nc_duty_front=10;
        }
    }
    else
    {
        NC_VALVE_DUTY_CONTROL_FRONT = 0;
        esp_slip_nc_duty_front=10;
    }

#if Drum_Brake
    #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(((RL_ESC.slip_control>=RL_ESC.slip_dump_thr_change)&&((TEMP_AV_VL_rl==1)&&(SLIP_CONTROL_NEED_RL==1)))
     ||((RR_ESC.slip_control>=RR_ESC.slip_dump_thr_change)&&((TEMP_AV_VL_rr==1)&&(SLIP_CONTROL_NEED_RR==1))))
    #else
    if((slip_control_rear>=slip_r_dump_thr_change) && ( ((TEMP_AV_VL_rl==1)&&(SLIP_CONTROL_NEED_RL == 1))
    	   || ((TEMP_AV_VL_rr==1)&&(SLIP_CONTROL_NEED_RR==1)) ) )
    #endif
    {
        if(dump_cycle < 2)
        {
            esp_slip_nc_duty_rear = 5;
            NC_VALVE_DUTY_CONTROL_REAR = 1;
        }
        else
        {
            esp_slip_nc_duty_rear = 10;
            NC_VALVE_DUTY_CONTROL_REAR = 0;
        }
    }
    else
    {
        esp_slip_nc_duty_rear = 10;
        NC_VALVE_DUTY_CONTROL_REAR = 0;
    }
#else
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((SLIP_CONTROL_NEED_RL==1)&&(lsespu1AHBGEN3MpresBrkOn==0)&&(BTCS_ON==0))
  #else
    if((SLIP_CONTROL_NEED_RL==1)&&(MPRESS_BRAKE_ON==0)&&(BTCS_ON==0))
  #endif
    {
    	#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      if((TEMP_AV_VL_rl==1)&&(RL_ESC.slip_control>=RL_ESC.slip_dump_thr_change)&&(RL_ESC.slip_control<=(-40))
            &&(slip_m_rl >(-1000)))
      #else
        if((TEMP_AV_VL_rl==1)&&(slip_control_rear>=slip_r_dump_thr_change)&&(slip_control_rear<=(-40))
            &&(slip_m_rl >(-1000)))
      #endif
        {
            NC_VALVE_DUTY_CONTROL_REAR = 1;
            if(esp_pressure_count_rl >= 21)
            {
                esp_slip_nc_duty_rear = 5;
            }
            else
            {
                esp_slip_nc_duty_rear = 10;
            }
        }
        else
        {
            NC_VALVE_DUTY_CONTROL_REAR = 0;
            esp_slip_nc_duty_rear=10;
        }
    }
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    else if ((SLIP_CONTROL_NEED_RR==1)&&(lsespu1AHBGEN3MpresBrkOn==0)&&(BTCS_ON==0))
  #else
    else if ((SLIP_CONTROL_NEED_RR==1)&&(MPRESS_BRAKE_ON==0)&&(BTCS_ON==0))
  #endif
    {
    	#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      if((TEMP_AV_VL_rr==1)&&(RR_ESC.slip_control>=RR_ESC.slip_dump_thr_change)&&(RR_ESC.slip_control<=(-40))
            &&(slip_m_rr >(-1000)))
      #else
        if((TEMP_AV_VL_rr==1)&&(slip_control_rear>=slip_r_dump_thr_change)&&(slip_control_rear<=(-40))
            &&(slip_m_rr >(-1000)))
      #endif
        {
            NC_VALVE_DUTY_CONTROL_REAR = 1;
            if(esp_pressure_count_rr >= 21)
            {
                esp_slip_nc_duty_rear = 5;
            }
            else
            {
                esp_slip_nc_duty_rear = 10;

            }
        }
        else
        {
            NC_VALVE_DUTY_CONTROL_REAR = 0;
            esp_slip_nc_duty_rear=10;
        }
    }
    else
    {
        NC_VALVE_DUTY_CONTROL_REAR = 0;
        esp_slip_nc_duty_rear=10;
    }
#endif     
}
void LAESP_vResetTempValve(void)
{
    VDC_MOTOR_ON=0;
/*060607 modified and added for FR Split by eslim*/    
#if !__SPLIT_TYPE
    TEMP_TCL_DEMAND_fl=0; TEMP_TCL_DEMAND_fr=0;
    T_S_V_RIGHT=0; T_S_V_LEFT=0;
#else
    TEMP_TCL_DEMAND_PRIMARY=0; TEMP_TCL_DEMAND_SECONDARY=0;
    T_S_V_PRIMARY=0; T_S_V_SECONDARY=0;
#endif
/*060607 modified and added for FR Split by eslim*/
    TEMP_HV_VL_fl=0; TEMP_AV_VL_fl=0;
    TEMP_HV_VL_fr=0; TEMP_AV_VL_fr=0;
    TEMP_HV_VL_rl=0; TEMP_AV_VL_rl=0;
    TEMP_HV_VL_rr=0; TEMP_AV_VL_rr=0;

}
#if ENABLE_PULSE_DOWN_DUMP
void LAESP_vCalNcDutyAtEndEsp(void)
{
    if((SLIP_CONTROL_NEED_FL_old==1)&&(ABS_fz==0))
    {
        if(esp_pressure_count_fl_old >= 21)
        {
            pulse_dump_nc_duty_front = 5;
        }
        else
        {
            pulse_dump_nc_duty_front = 10;
        }
    }
    else if((SLIP_CONTROL_NEED_FR_old==1)&&(ABS_fz==0))
    {
        if(esp_pressure_count_fr_old >= 21)
        {
            pulse_dump_nc_duty_front = 5;
        }
        else
        {
            pulse_dump_nc_duty_front = 10;
        }
    }
    else
    {
        pulse_dump_nc_duty_front = 10;
    }

    if((SLIP_CONTROL_NEED_RL_old==1)&&(ABS_fz==0))
    {
        if(esp_pressure_count_rl_old >= 14)
        {
            pulse_dump_nc_duty_rear = 5;
        }
        else
        {
            pulse_dump_nc_duty_rear = 10;
        }
    }
    else if((SLIP_CONTROL_NEED_RR_old==1)&&(ABS_fz==0))
    {
        if(esp_pressure_count_rr_old >= 14)
        {
            pulse_dump_nc_duty_rear = 5;
        }
        else
        {
            pulse_dump_nc_duty_rear = 10;
        }
    }
    else
    {
        pulse_dump_nc_duty_rear = 10;
    }
}

void LAESP_vActFadeoutFL(void)
    {
        if(slip_m_fl < -WHEEL_SLIP_60)
        {
            esp_pulse_down_dump_rate_confirm_fl = 1;
        }
        else
        {
            ;
        }
        
        if(esp_pulse_down_dump_rate_confirm_fl==0)
        {
        	fade_dump_rate_fl = 0;
		}
		else
		{
        	fade_dump_rate_fl = (esp_on_counter_fl%esp_pulse_down_dump_rate_confirm_fl);			
		}
        if((esp_on_counter_fl<pulse_dump_time_front_fl)||(esp_pressure_count_fl_old > 7))
        {
            if((ABS_fz==1)&&(PARTIAL_BRAKE==0))
            {
                if(fade_dump_rate_fl == 0)
                {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=1;
                    esp_control_mode_fl=4;
                    TCS_built_reapply_pattern_fl=0;
                    if(SLIP_CONTROL_NEED_FL==1)
                    {
                        esp_pressure_count_fl_old = esp_pressure_count_fl;
                    }
                    else
                    {
                        esp_pressure_count_fl_old = esp_pressure_count_fl_old-3;
                    }
                    FL_ESC.lcesps16IdbTarPress = FL_ESC.lcesps16IdbTarPressold- FL_ESC.lcesps16FadedelP;
                }
                else
                {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=0;
                    esp_control_mode_fl=3;
                    TCS_built_reapply_pattern_fl=0;
	                esp_pressure_count_fl_old = esp_pressure_count_fl_old;
	                FL_ESC.lcesps16IdbTarPress = FL_ESC.lcesps16IdbTarPressold;
                }                    
            }
            else if(esp_pulse_down_dump_rate_confirm_fl > 0)
            {
                if(fade_dump_rate_fl==0)
                {
                    TEMP_HV_VL_fl=1;
                    TEMP_AV_VL_fl=1;
                    esp_control_mode_fl=4;
                    TCS_built_reapply_pattern_fl=0;
                    if(SLIP_CONTROL_NEED_FL==1)
                    {
                        esp_pressure_count_fl_old = esp_pressure_count_fl;
                    }
                    else
                    {
                        esp_pressure_count_fl_old = esp_pressure_count_fl_old-1;
                    }
                    FL_ESC.lcesps16IdbTarPress = FL_ESC.lcesps16IdbTarPressold- FL_ESC.lcesps16FadedelP;
                 }
                 else
                 {
                     TEMP_HV_VL_fl=1;
                     TEMP_AV_VL_fl=0;
                     esp_control_mode_fl=3;
                     TCS_built_reapply_pattern_fl=0;
                     FL_ESC.lcesps16IdbTarPress = FL_ESC.lcesps16IdbTarPressold;    
                 }
             }
             else
             {
             	;	
             }
/************FL TC Valve close******************/    
#if !__SPLIT_TYPE
       		 if(SLIP_CONTROL_NEED_RR==0)
#else
			 if(SLIP_CONTROL_NEED_FR==0)
#endif
       		 {      
					#if NEW_PULSE_DOWN_DUMP
					#if !__SPLIT_TYPE
					    if((SLIP_CONTROL_NEED_FR==0)&&(esp_on_counter_fl < tc_close_time_fl))
					    {
					        TEMP_TCL_DEMAND_fl =1;
					#else
					    if((SLIP_CONTROL_NEED_RR==0)&&(esp_on_counter_fl < tc_close_time_fl))
					    {
					        TEMP_TCL_DEMAND_PRIMARY =1;
					#endif
					    }
					    else
					    {
					#if !__SPLIT_TYPE
					        TEMP_TCL_DEMAND_fl =0;
					#else
					        TEMP_TCL_DEMAND_PRIMARY =0;
					#endif
					    }
					#endif
#if __TCMF_ENABLE 
					#if !__SPLIT_TYPE
					        T_S_V_LEFT = TEMP_TCL_DEMAND_fl;
					#else
					        T_S_V_PRIMARY =TEMP_TCL_DEMAND_PRIMARY;
					#endif
#else
				
					#if __TCS_NOISE
					#if !__SPLIT_TYPE
					        T_S_V_LEFT=!TEMP_HV_VL_fl;
					#else
					        T_S_V_PRIMARY=!TEMP_HV_VL_fl;
					#endif
					#else
					#if !__SPLIT_TYPE
					        T_S_V_LEFT=1;
					#else
					        T_S_V_PRIMARY=1;
					#endif
					#endif
#endif
			}
			else
			{
				;
			}

/***********TC Valve******************/
        }
        fade_rate_counter_fl++;
        if(fade_rate_counter_fl==esp_pulse_down_dump_rate_confirm_fl)
	    {
	            fade_rate_counter_fl=0;
	    }
	    else
	    {
	        ;
	    }

    
    #if (__ESC_TCMF_CONTROL ==1)    	
	if(FL_ESC.lcespu1TCMFControl ==1)
    {
    	esp_control_mode_fl = 0;
    	TEMP_TCL_DEMAND_fl =1;
    	T_S_V_LEFT = 1;
    }
    else
    {
    	;
    }
    #endif
    if(FL_ESC.lcesps16IdbTarPress < MPRESS_0BAR)
    {
       FL_ESC.lcesps16IdbTarPress= MPRESS_0BAR; 
    }
    else
    {
        ;   
    }
}
    
void LAESP_vActFadeoutFR(void)
    {
       if(slip_m_fr < -WHEEL_SLIP_60)
       {
            esp_pulse_down_dump_rate_confirm_fr = 1;
       }
       else
       { 
            ;
       }
       if(esp_pulse_down_dump_rate_confirm_fr==0)
       {
        	fade_dump_rate_fr = 0;
		}
		else
		{
        	fade_dump_rate_fr = (esp_on_counter_fr%esp_pulse_down_dump_rate_confirm_fr);		
		}
            
            if((esp_on_counter_fr<pulse_dump_time_front_fr)||(esp_pressure_count_fr_old > 5))
            {
                if((ABS_fz==1)&&(PARTIAL_BRAKE==0))
                {
                    if(fade_dump_rate_fr  == 0)
                    {
                        TEMP_HV_VL_fr=1;TEMP_AV_VL_fr=1;
                        esp_control_mode_fr=4;
                        TCS_built_reapply_pattern_fr=0;
                        if(SLIP_CONTROL_NEED_FR==1)
                        {
                            esp_pressure_count_fr_old = esp_pressure_count_fr;
                        }
                        else
                        {
                            esp_pressure_count_fr_old = esp_pressure_count_fr_old-3;
                        }
                        FR_ESC.lcesps16IdbTarPress = FR_ESC.lcesps16IdbTarPressold- FR_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_fr=1;TEMP_AV_VL_fr=0;
                        esp_control_mode_fr=3;
                        TCS_built_reapply_pattern_fr=0;
                        esp_pressure_count_fr_old = esp_pressure_count_fr_old;
                        FR_ESC.lcesps16IdbTarPress = FR_ESC.lcesps16IdbTarPressold;
                   }
                }
                else if(esp_pulse_down_dump_rate_confirm_fr > 0)
                {
                    if(fade_dump_rate_fr==0)
                    {
                        TEMP_HV_VL_fr=1;TEMP_AV_VL_fr=1;
                        esp_control_mode_fr=4;
                        TCS_built_reapply_pattern_fr=0;
                        if(SLIP_CONTROL_NEED_FR==1)
                        {
                            esp_pressure_count_fr_old = esp_pressure_count_fr;
                        }
                        else
                        {
                            esp_pressure_count_fr_old = esp_pressure_count_fr_old-1;
                        }
                        FR_ESC.lcesps16IdbTarPress = FR_ESC.lcesps16IdbTarPressold- FR_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_fr=1;TEMP_AV_VL_fr=0;
                        esp_control_mode_fr=3;
                        TCS_built_reapply_pattern_fr=0;
                        FR_ESC.lcesps16IdbTarPress = FR_ESC.lcesps16IdbTarPressold;
                    }
                }
                else
                {
                	;
                }

/************TC Valve close******************/             
#if !__SPLIT_TYPE
       		 if(SLIP_CONTROL_NEED_RL==0)
#else
			 if(SLIP_CONTROL_NEED_FL==0)
#endif
      		 {      

				#if NEW_PULSE_DOWN_DUMP
					#if !__SPLIT_TYPE
				    if((SLIP_CONTROL_NEED_FL==0)&&(esp_on_counter_fr < tc_close_time_fr))
				    {
					        TEMP_TCL_DEMAND_fr =1;
					#else
				    if((SLIP_CONTROL_NEED_RL==0)&&(esp_on_counter_fr < tc_close_time_fr))
				    {
					        TEMP_TCL_DEMAND_PRIMARY =1;
					#endif
				    }
				    else
				    {
					#if !__SPLIT_TYPE
					        TEMP_TCL_DEMAND_fr =0;
					#else
					        TEMP_TCL_DEMAND_PRIMARY =0;
					#endif
				    }
				#endif
#if __TCMF_ENABLE 
					#if !__SPLIT_TYPE
					        T_S_V_RIGHT = TEMP_TCL_DEMAND_fr;
					#else
					        T_S_V_PRIMARY =TEMP_TCL_DEMAND_PRIMARY;
					#endif
#else
				#if __TCS_NOISE
					#if !__SPLIT_TYPE
					        T_S_V_RIGHT=!TEMP_HV_VL_fr;
					#else
					        T_S_V_PRIMARY=!TEMP_HV_VL_fr;
					#endif
				#else
					#if !__SPLIT_TYPE
					        T_S_V_RIGHT=1;
					#else
					        T_S_V_PRIMARY=1;
					#endif
				#endif
#endif
			}
			else
			{
				;
			}
/**********************TC Valve**********************************/			
          }
          fade_rate_counter_fr++;          
          if(fade_rate_counter_fr==esp_pulse_down_dump_rate_confirm_fr)
		  {
		          fade_rate_counter_fr=0;
		  }
		  else
		  {
		       ;
		  }

       
    #if (__ESC_TCMF_CONTROL ==1) 
    if(FR_ESC.lcespu1TCMFControl ==1)
    {
    	esp_control_mode_fr = 0;
    	TEMP_TCL_DEMAND_fr =1;
    	T_S_V_RIGHT = 1;
       }
       else
       {
    		;
       }
    #endif
    if(FR_ESC.lcesps16IdbTarPress < MPRESS_0BAR)
    {
       FR_ESC.lcesps16IdbTarPress= MPRESS_0BAR; 
    }
    else
    {
        ;   
    }
       
}
    
void LAESP_vActFadeoutRL(void)
        {
            if(slip_m_rl < -WHEEL_SLIP_50)
            {
                esp_pulse_down_dump_rate_confirm_rl = 1;
            }
            else
            {
                ;
            }
			if(esp_pulse_down_dump_rate_confirm_rl==0)
	        {
	        	fade_dump_rate_rl = 0;
			}
			else
			{
            	fade_dump_rate_rl =(esp_on_counter_rl%esp_pulse_down_dump_rate_confirm_rl);
			}
            if((esp_on_counter_rl<pulse_dump_time_rear_rl)||(esp_pressure_count_rl_old > 5))
            {
                if((ABS_fz==1)&&(PARTIAL_BRAKE==0))
                {
                    if(fade_dump_rate_rl  == 0)
                    {
                        TEMP_HV_VL_rl=1;
                        TEMP_AV_VL_rl=1;
                        esp_control_mode_rl=4;
                        TCS_built_reapply_pattern_rl=0;
                        if(SLIP_CONTROL_NEED_RL==1)
                        {
                            esp_pressure_count_rl_old = esp_pressure_count_rl;
                        }
                        else
                        {
                            esp_pressure_count_rl_old = esp_pressure_count_rl_old-2;
                        }
                        RL_ESC.lcesps16IdbTarPress = RL_ESC.lcesps16IdbTarPressold- RL_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_rl=1;
                        TEMP_AV_VL_rl=0;
                        esp_control_mode_rl=3;
                        TCS_built_reapply_pattern_rl=0;

                        esp_pressure_count_rl_old = esp_pressure_count_rl_old;
                        RL_ESC.lcesps16IdbTarPress = RL_ESC.lcesps16IdbTarPressold;
                    }
                }
                else if(esp_pulse_down_dump_rate_confirm_rl > 0)
                {
                     if(fade_dump_rate_rl==0)
                    {
                        TEMP_HV_VL_rl=1;
                        TEMP_AV_VL_rl=1;
                        esp_control_mode_rl=4;
                        TCS_built_reapply_pattern_rl=0;
                        if(SLIP_CONTROL_NEED_RL==1)
                        {
                            esp_pressure_count_rl_old = esp_pressure_count_rl;
                        }
                        else
                        {
                            esp_pressure_count_rl_old = esp_pressure_count_rl_old-2;
                        }
                        RL_ESC.lcesps16IdbTarPress = RL_ESC.lcesps16IdbTarPressold- RL_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_rl=1;
                        TEMP_AV_VL_rl=0;
                        esp_control_mode_rl=3;
                        TCS_built_reapply_pattern_rl=0;

                        esp_pressure_count_rl_old = esp_pressure_count_rl_old;
                        RL_ESC.lcesps16IdbTarPress = RL_ESC.lcesps16IdbTarPressold;
                    }
                }
                else
                {
                	;	
                }
/*********************TC Valve close**********************************/
#if !__SPLIT_TYPE
       		 if(SLIP_CONTROL_NEED_FR==0)
#else
			 if(SLIP_CONTROL_NEED_RR==0)
#endif
				{					
					#if NEW_PULSE_DOWN_DUMP
					#if !__SPLIT_TYPE
					    if((SLIP_CONTROL_NEED_RR==0)&&(esp_on_counter_rl < tc_close_time_rl))
					    {
					        TEMP_TCL_DEMAND_fr =1;
					#else
					    if((SLIP_CONTROL_NEED_FR==0)&&(esp_on_counter_rl < tc_close_time_rl))
					    {					
					        TEMP_TCL_DEMAND_SECONDARY =1;
					#endif
					    }
					    else
					    {
					#if !__SPLIT_TYPE
					        TEMP_TCL_DEMAND_fr =0;
					#else
					        TEMP_TCL_DEMAND_SECONDARY =0;
					#endif
					    }
					#endif
#if __TCMF_ENABLE 
				#if !__SPLIT_TYPE
				        T_S_V_RIGHT = TEMP_TCL_DEMAND_rl;
				#else
				        T_S_V_SECONDARY =TEMP_TCL_DEMAND_SECONDARY;
				#endif
#else				
				#if __TCS_NOISE
					#if !__SPLIT_TYPE
					        T_S_V_RIGHT=!TEMP_HV_VL_rl;
					#else
					        T_S_V_SECONDARY=!TEMP_HV_VL_rl;
					#endif
				#else
					#if !__SPLIT_TYPE
					        T_S_V_RIGHT=1;
					#else
					        T_S_V_SECONDARY=1;
					#endif
				#endif
#endif 
				}
				else
				{
					;
				}
/**********************TC Valve **********************************/
        	}
            fade_rate_counter_rl++;
			if(fade_rate_counter_rl==esp_pulse_down_dump_rate_confirm_rl)
	    	{
	            fade_rate_counter_rl=0;
	    	}
	    	else
	    	{	
	        	;
	    	}
        
    if(RL_ESC.lcesps16IdbTarPress < MPRESS_0BAR)
    {
       RL_ESC.lcesps16IdbTarPress= MPRESS_0BAR; 
    }
    else
    {
        ;   
    }        
    	}
        
void LAESP_vActFadeoutRR(void)
        {

            if(slip_m_rr < -WHEEL_SLIP_50)
            {
                esp_pulse_down_dump_rate_confirm_rr = 1;
            }
            else
            {
                ;
            }
			if(esp_pulse_down_dump_rate_confirm_rr==0)
	        {
	        	fade_dump_rate_rr = 0;
			}
			else
			{
            	fade_dump_rate_rr = (esp_on_counter_rr%esp_pulse_down_dump_rate_confirm_rr);
			}
            
            if((esp_on_counter_rr<pulse_dump_time_rear_rr)||(esp_pressure_count_rr_old > 5))
            {
                if((ABS_fz==1)&&(PARTIAL_BRAKE==0))
                {
                    if(fade_dump_rate_rr  == 0)
                    {
                        TEMP_HV_VL_rr=1;
                        TEMP_AV_VL_rr=1;
                        esp_control_mode_rr=4;
                        TCS_built_reapply_pattern_rr=0;
                        if(SLIP_CONTROL_NEED_RR==1)
                        {
                            esp_pressure_count_rr_old = esp_pressure_count_rr;
                        }
                        else
                        {
                            esp_pressure_count_rr_old = esp_pressure_count_rr_old-2;
                        }
                        RR_ESC.lcesps16IdbTarPress = RR_ESC.lcesps16IdbTarPressold- RR_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_rr=1;
                        TEMP_AV_VL_rr=0;
                        esp_control_mode_rr=3;
                        TCS_built_reapply_pattern_rr=0;

                        esp_pressure_count_rr_old = esp_pressure_count_rr_old;
                        RR_ESC.lcesps16IdbTarPress = RR_ESC.lcesps16IdbTarPressold;
                   }
                }
                else if(esp_pulse_down_dump_rate_confirm_rr > 0)
                {
                    if(fade_dump_rate_rr==0)
                    {
                        TEMP_HV_VL_rr=1;
                        TEMP_AV_VL_rr=1;
                        esp_control_mode_rr=4;
                        TCS_built_reapply_pattern_rr=0;

                       if(SLIP_CONTROL_NEED_RR==1)
                       {
                            esp_pressure_count_rr_old = esp_pressure_count_rr;
                       }
                       else
                       {
                            esp_pressure_count_rr_old = esp_pressure_count_rr_old-2;
                       }
                       RR_ESC.lcesps16IdbTarPress = RR_ESC.lcesps16IdbTarPressold- RR_ESC.lcesps16FadedelP;
                    }
                    else
                    {
                        TEMP_HV_VL_rr=1;
                        TEMP_AV_VL_rr=0;
                        esp_control_mode_rr=3;
                        TCS_built_reapply_pattern_rr=0;

                        esp_pressure_count_rr_old = esp_pressure_count_rr_old;
                        RR_ESC.lcesps16IdbTarPress = RR_ESC.lcesps16IdbTarPressold;
                    }
                }
                else
                {
                	;
                }
/**********************TC Valve close**********************************/
#if !__SPLIT_TYPE
       		 if(SLIP_CONTROL_NEED_FL==0)
#else
			 if(SLIP_CONTROL_NEED_RL==0)
#endif		
				{
					#if NEW_PULSE_DOWN_DUMP
					#if !__SPLIT_TYPE
					    if((SLIP_CONTROL_NEED_RL==0)&&(esp_on_counter_rr < tc_close_time_rr))
					    {
					        TEMP_TCL_DEMAND_fl =1;
					#else
					    if((SLIP_CONTROL_NEED_FL==0)&&(esp_on_counter_rr < tc_close_time_rr))
					    {
					        TEMP_TCL_DEMAND_SECONDARY =1;
					#endif
					    }
					    else
					    {
					#if !__SPLIT_TYPE
					        TEMP_TCL_DEMAND_fl =0;
					#else
					        TEMP_TCL_DEMAND_SECONDARY =0;
					#endif
					    }
					#endif
#if __TCMF_ENABLE 
				#if !__SPLIT_TYPE
				        T_S_V_LEFT = TEMP_TCL_DEMAND_rr;
				#else
				        T_S_V_SECONDARY =TEMP_TCL_DEMAND_SECONDARY;
				#endif
#else	
				#if __TCS_NOISE
					#if !__SPLIT_TYPE
					        T_S_V_LEFT=!TEMP_HV_VL_rr;
					#else
					        T_S_V_SECONDARY=!TEMP_HV_VL_rr;
					#endif
				#else
					#if !__SPLIT_TYPE
					        T_S_V_LEFT=1;
					#else
					        T_S_V_SECONDARY=1;
					#endif
				#endif
#endif
				}
				else
				{
					;
				}
/**********************TC Valve**********************************/				
	  		}
            fade_rate_counter_rr++;
	  		if(fade_rate_counter_rr==esp_pulse_down_dump_rate_confirm_rr)
	    	{
	            fade_rate_counter_rr=0;
	    	}
	    	else
	    	{	
	        	;
	    	}
    if(RR_ESC.lcesps16IdbTarPress < MPRESS_0BAR)
    {
       RR_ESC.lcesps16IdbTarPress= MPRESS_0BAR; 
    }
    else
    {
        ;   
    }	    	
    	}

void LAESP_vActFadeout(void)
{
	LAESP_vCalNcDutyAtEndEsp();
	
	esp_pulse_down_dump_rate_confirm_fl = new_pulse_down_rate_fl;
   	esp_pulse_down_dump_rate_confirm_fr = new_pulse_down_rate_fr;
   	esp_pulse_down_dump_rate_confirm_rl = new_pulse_down_rate_rl;
   	esp_pulse_down_dump_rate_confirm_rr = new_pulse_down_rate_rr;
#if(__IDB_LOGIC == ENABLE)	
	if((SLIP_CONTROL_NEED_FL_old==1)&&(ESP_PULSE_DUMP_FL==1))
	{
		FL_ESC.lcesps16IdbTarPressold = FL_ESC.lcesps16IdbTarPress;
		LAESP_vActFadeoutFL();
	}
    else
    {
    	;
    }
    	
	if((SLIP_CONTROL_NEED_FR_old==1)&&(ESP_PULSE_DUMP_FR==1))
	{
		FR_ESC.lcesps16IdbTarPressold = FR_ESC.lcesps16IdbTarPress;
		LAESP_vActFadeoutFR();
	}
    else
    {
    	;
    }
    	
	if((SLIP_CONTROL_NEED_RL_old==1)&&(ESP_PULSE_DUMP_RL==1))
	{
		RL_ESC.lcesps16IdbTarPressold = RL_ESC.lcesps16IdbTarPress;
		LAESP_vActFadeoutRL();
	}
    else
    {
    	;
    }
    	
	if((SLIP_CONTROL_NEED_RR_old==1)&&(ESP_PULSE_DUMP_RR==1))
	{
		RR_ESC.lcesps16IdbTarPressold = RR_ESC.lcesps16IdbTarPress;
		LAESP_vActFadeoutRR();
    }
    else
    {
    	;
    }
#else	
	if((SLIP_CONTROL_NEED_FL_old==1)&&(ESP_PULSE_DUMP_FL==1))
	{
		LAESP_vActFadeoutFL();
	}
	else if((SLIP_CONTROL_NEED_FR_old==1)&&(ESP_PULSE_DUMP_FR==1))
	{
		LAESP_vActFadeoutFR();
	}
	else if((SLIP_CONTROL_NEED_RL_old==1)&&(ESP_PULSE_DUMP_RL==1))
	{
		LAESP_vActFadeoutRL();
	}
	else if((SLIP_CONTROL_NEED_RR_old==1)&&(ESP_PULSE_DUMP_RR==1))
	{
		LAESP_vActFadeoutRR();
    }
    else
    {
    	;
    }
 #endif   	
 }
					#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif
