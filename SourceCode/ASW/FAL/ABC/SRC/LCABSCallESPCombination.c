/*******************************************************************/
/*  Program ID: MGH-40 ESP                                         */
/*  Program description: ABS+ESP Assistance Control                */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes:                                                 */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author          Description                     */
/*  -------       ----------      ---------------------------      */
/*  03.02.14      JinKoo Lee      Initial Release                  */
/*  04.01.06      JinKoo Lee      Modified at '04SW winter         */
/*  05.11.23      JinKoo Lee      Modified for MISRA rule          */
/*******************************************************************/

/* Includes ********************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCABSCallESPCombination        
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include"LCABSDecideCtrlState.h"
#include"LCABSCallESPCombination.h"
#include "LDESPDetectVehicleStatus.h"

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/



/* Local Function prototype ****************************************/

#if __VDC
void LCABS_vChangeABStoESP(void);
void LCABS_vChangeESPtoABS(void);
void LCABS_vChangeABStoESPWheel(struct W_STRUCT *WL);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCABS_vChangeESPtoABSWheel(struct W_STRUCT *WL, struct ESC_STRUCT *WL_ESC);
#else
void LCABS_vChangeESPtoABSWheel(struct W_STRUCT *WL);
#endif

void LCABS_vRestoreLFCVariable(void);
void LCABS_vRecallLFCVariable(void);
void LCABS_vRestoreLFCVariableWheel(struct W_STRUCT *WL);
void LCABS_vRecallLFCVariableWheel(struct W_STRUCT *WL);

  #if __ESP_ABS_COOPERATION_CONTROL
    #if	__ABS_COMB
void LCABS_vForcedDumpHoldWheel(void);
void LCABS_vDecideAbsEscCompLevel(struct W_STRUCT *WL);
    #else
void LCABS_vForcedDumpHold(void);
void LCABS_vForcedDumpHoldWheel(struct W_STRUCT *WL);
    #endif
  #endif
#endif

/* GlobalFunction prototype ****************************************/



/* Implementation***************************************************/

#if __VDC

void LCABS_vChangeABStoESP(void)
{
  #if __ESP_ABS_COOPERATION_CONTROL  
    if((ABS_fz==1)&&(PARTIAL_BRAKE==0))
    {
        if(ESP_BRAKE_CONTROL_FL==1) 
        {
            FR.ESP_ABS_Over_Front_Inside = 1;
            RR.ESP_ABS_Over_Rear_Inside = 1;
            RL.ESP_ABS_Over_Rear_Outside = 1;
            
            if(delta_yaw>=YAW_20DEG)
            {
                ESP_ABS_SLIP_CONTROL_fr=1;
                ESP_ABS_SLIP_CONTROL_rr=1;
            }
            else
            {
                ESP_ABS_SLIP_CONTROL_fr=0;
                ESP_ABS_SLIP_CONTROL_rr=0;
            }
        }
        else
        {
        	;
        }
        
        if(ESP_BRAKE_CONTROL_FR==1) 
        {
            FL.ESP_ABS_Over_Front_Inside = 1;
            RL.ESP_ABS_Over_Rear_Inside = 1;
            RR.ESP_ABS_Over_Rear_Outside = 1;
            
            if(delta_yaw>=YAW_20DEG)
            {
                ESP_ABS_SLIP_CONTROL_fl=1;
                ESP_ABS_SLIP_CONTROL_rl=1;
            }
            else
            {
                ESP_ABS_SLIP_CONTROL_fl=0;
                ESP_ABS_SLIP_CONTROL_rl=0;
            }
        }
        else
        {
        	;
        }
        
        if(ESP_BRAKE_CONTROL_RL==1)
        {
            RR.ESP_ABS_Under_Rear_Outside = 1;
            ESP_ABS_SLIP_CONTROL_rr=1;
        }
        else
        {
        	;
        }
        
        if(ESP_BRAKE_CONTROL_RR==1)
        {
            RL.ESP_ABS_Under_Rear_Outside = 1;
            ESP_ABS_SLIP_CONTROL_rl=1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_fl==1)||(ESP_ABS_SLIP_CONTROL_fl==1)||(ESP_BRAKE_CONTROL_FL==1))
    {
        L_SLIP_CONTROL_fl = 1;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_fr==1)||(ESP_ABS_SLIP_CONTROL_fr==1)||(ESP_BRAKE_CONTROL_FR==1))
    {
    	L_SLIP_CONTROL_fr = 1;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_rl==1)||(ESP_ABS_SLIP_CONTROL_rl==1)||(ESP_BRAKE_CONTROL_RL==1)||((ESP_BRAKE_CONTROL_RR==1)&&(lcu1US3WHControlFlag==1)))
    {
    	L_SLIP_CONTROL_rl = 1;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_rr==1)||(ESP_ABS_SLIP_CONTROL_rr==1)||(ESP_BRAKE_CONTROL_RR==1)||((ESP_BRAKE_CONTROL_RL==1)&&(lcu1US3WHControlFlag==1)))
    {
    	L_SLIP_CONTROL_rr = 1;
    }
    else
    {
    	;
    }
  #else
    if((ESP_BRAKE_CONTROL_FL==1)||(CROSS_SLIP_CONTROL_fl==1))
    {
    	L_SLIP_CONTROL_fl = 1;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_FR==1)||(CROSS_SLIP_CONTROL_fr==1))
    {
    	L_SLIP_CONTROL_fr = 1;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_RL==1)||(CROSS_SLIP_CONTROL_rl==1))
    {
    	L_SLIP_CONTROL_rl = 1;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_RR==1)||(CROSS_SLIP_CONTROL_rr==1))
    {
    	L_SLIP_CONTROL_rr = 1;
    }
    else
    {
    	;
    }
  #endif 
    
    if(((L_SLIP_CONTROL_rl==1)||(L_SLIP_CONTROL_rr==1))&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))
    {
        SPLIT_PULSE_rl=SPLIT_PULSE_rr=0;
        gma_dump_rl=gma_dump_rr=0;
    }
    else
    {
    	;
    }
    
    LCABS_vDecideAbsEscCompLevel(&FL);
    LCABS_vDecideAbsEscCompLevel(&FR);
    LCABS_vDecideAbsEscCompLevel(&RL);
    LCABS_vDecideAbsEscCompLevel(&RR);
    
    LCABS_vChangeABStoESPWheel(&FL);
    LCABS_vChangeABStoESPWheel(&FR);
    LCABS_vChangeABStoESPWheel(&RL);
    LCABS_vChangeABStoESPWheel(&RR);
}


void LCABS_vChangeESPtoABS(void)
{
    if(ESP_BRAKE_CONTROL_FL==0)
    {
      #if !__SPLIT_TYPE  /* X-split */
        CROSS_SLIP_CONTROL_rr=0;
      #else              /* FR-split */
        if(ESP_BRAKE_CONTROL_RL==0)
        {
        	CROSS_SLIP_CONTROL_rr=0;
        }
      #endif

      #if __ESP_ABS_COOPERATION_CONTROL
        ESP_ABS_SLIP_CONTROL_fr=0;
        ESP_ABS_SLIP_CONTROL_rr=0;  
        FR.ESP_ABS_Over_Front_Inside = 0;
        RR.ESP_ABS_Over_Rear_Inside = 0;
        RL.ESP_ABS_Over_Rear_Outside = 0;
        RR.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;
        FR.ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;
      #endif
    }
    else
    {
    	;
    }
    if(ESP_BRAKE_CONTROL_FR==0)
    {
      #if !__SPLIT_TYPE  /* X-split */  
        CROSS_SLIP_CONTROL_rl=0;
      #else              /* FR-split */
        if(ESP_BRAKE_CONTROL_RR==0)
        {
        	CROSS_SLIP_CONTROL_rl=0;
        }
      #endif  

      #if __ESP_ABS_COOPERATION_CONTROL
        ESP_ABS_SLIP_CONTROL_fl=0;
        ESP_ABS_SLIP_CONTROL_rl=0;  
        FL.ESP_ABS_Over_Front_Inside = 0;
        RL.ESP_ABS_Over_Rear_Inside = 0;
        RR.ESP_ABS_Over_Rear_Outside = 0;
        RL.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;
        FL.ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;
      #endif
    }
    else
    {
    	;
    }
    if(ESP_BRAKE_CONTROL_RL==0)
    {
      #if !__SPLIT_TYPE  /* X-split */    
        CROSS_SLIP_CONTROL_fr=0;
      #else              /* FR-split */
        if(ESP_BRAKE_CONTROL_FL==0)
        {
        	CROSS_SLIP_CONTROL_fr=0;
        }
      #endif

      #if __ESP_ABS_COOPERATION_CONTROL  
        ESP_ABS_SLIP_CONTROL_rr=0;
        RR.ESP_ABS_Under_Rear_Outside = 0;
/*      RR.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;    */
      #endif
    }
    else
    {
    	;
    }
    if(ESP_BRAKE_CONTROL_RR==0) 
    {
      #if !__SPLIT_TYPE  /* X-split */      
        CROSS_SLIP_CONTROL_fl=0;
      #else              /* FR-split */
        if(ESP_BRAKE_CONTROL_FR==0)
        {
        	CROSS_SLIP_CONTROL_fl=0;
        }
      #endif  

      #if __ESP_ABS_COOPERATION_CONTROL
        ESP_ABS_SLIP_CONTROL_rl=0;
        RL.ESP_ABS_Under_Rear_Outside = 0;
/*      RL.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;    */
      #endif
    }
    else
    {
    	;
    }
   
   #if __ESP_ABS_COOPERATION_CONTROL
    if((CROSS_SLIP_CONTROL_fl==0)&&(ESP_ABS_SLIP_CONTROL_fl==0)&&(ESP_BRAKE_CONTROL_FL==0))
    {
    	L_SLIP_CONTROL_fl = 0;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_fr==0)&&(ESP_ABS_SLIP_CONTROL_fr==0)&&(ESP_BRAKE_CONTROL_FR==0))
    {
    	L_SLIP_CONTROL_fr = 0;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_rl==0)&&(ESP_ABS_SLIP_CONTROL_rl==0)&&(ESP_BRAKE_CONTROL_RL==0))
    {
    	L_SLIP_CONTROL_rl = 0;
    }
    else
    {
    	;
    }
    if((CROSS_SLIP_CONTROL_rr==0)&&(ESP_ABS_SLIP_CONTROL_rr==0)&&(ESP_BRAKE_CONTROL_RR==0))
    {
    	L_SLIP_CONTROL_rr = 0;
    }
    else
    {
    	;
    }
   #else
    if((ESP_BRAKE_CONTROL_FL==0)&&(CROSS_SLIP_CONTROL_fl==0))
    {
    	L_SLIP_CONTROL_fl = 0;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_FR==0)&&(CROSS_SLIP_CONTROL_fr==0))
    {
    	L_SLIP_CONTROL_fr = 0;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_RL==0)&&(CROSS_SLIP_CONTROL_rl==0))
    {
    	L_SLIP_CONTROL_rl = 0;
    }
    else
    {
    	;
    }
    if((ESP_BRAKE_CONTROL_RR==0)&&(CROSS_SLIP_CONTROL_rr==0))
    {
    	L_SLIP_CONTROL_rr = 0;
    }
    else
    {
    	;
    }
   #endif
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    LCABS_vChangeESPtoABSWheel(&FL, &FL_ESC);
    LCABS_vChangeESPtoABSWheel(&FR, &FR_ESC);
    LCABS_vChangeESPtoABSWheel(&RL, &RL_ESC);
    LCABS_vChangeESPtoABSWheel(&RR, &RR_ESC);
#else
    LCABS_vChangeESPtoABSWheel(&FL);
    LCABS_vChangeESPtoABSWheel(&FR);
    LCABS_vChangeESPtoABSWheel(&RL);
    LCABS_vChangeESPtoABSWheel(&RR);
#endif
    
}


void LCABS_vChangeABStoESPWheel(struct W_STRUCT *WL)
{
    l_SLIP_K_wl=0;
    
    if(L_SLIP_CONTROL_OLD_wl==0) 
    {
        if(L_SLIP_CONTROL_wl==1) 
        {   
            l_SLIP_K_wl=1;
            WL->ESP_end_counter = 0;
        }
        else
        {
        	;
        }
        if(WL->ESP_end_counter != 0)
        {
            if(WL->ESP_end_counter<U8_TYPE_MAXNUM)
            {
        	    WL->ESP_end_counter=WL->ESP_end_counter+L_1SCAN;
        	}
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    
    if(l_SLIP_K_wl==1)
    {
        if(WL->LFC_to_ESP_flag==0)
        {
        	WL->LFC_to_ESP_start_flag=1;
        }
        else
        {
        	;
        }
        WL->LFC_to_ESP_flag=1;
        WL->LFC_built_reapply_flag=0;
        WL->LFC_built_reapply_flag_old=0;
/*        WL->pwm_reapply_flag=0; */
        WL->LFC_s0_reapply_counter=0;
        WL->LFC_s0_reapply_counter_old=0;
        WL->LFC_built_reapply_counter=0;
        WL->LFC_built_reapply_counter_old=0;
/*      WL->hold_timer_new_ref_old=0;        */
        WL->timer_keeping_LFC_duty=0;
    }
    else {
        WL->LFC_to_ESP_start_flag=0;
    }
    
/*  if((WL->LFC_to_ESP_flag==1)&&(L_SLIP_CONTROL_wl==0)){
        if((WL->LFC_built_reapply_flag==1)||(ABS_wl==0)){
            WL->LFC_to_ESP_end_flag=1;
            WL->LFC_to_ESP_flag=0;
            WL->ESP_end_counter_final = WL->ESP_end_counter;
        }
    }
    else */
    if(WL->LFC_to_ESP_end_flag==1) 
    {
        WL->LFC_to_ESP_end_flag=0;
        WL->ESP_end_counter = 0;
    }
    else
    {
    	;
    }
    if(L_SLIP_CONTROL_wl==1)
    {
        if((HV_VL_wl==1)&&(AV_VL_wl==1)&&(AV_DUMP_wl==1))
        {
            if(WL->ESP_partial_dump_counter<U8_TYPE_MAXNUM) 
            {
            	WL->ESP_partial_dump_counter = WL->ESP_partial_dump_counter + L_1SCAN;
            }
            else
            {
            	;
            }
        }
        else
        {
        	WL->ESP_partial_dump_counter=0;
        }
    }
    else
    {
    	WL->ESP_partial_dump_counter=0;
    }
    
    L_SLIP_CONTROL_OLD_wl = L_SLIP_CONTROL_wl;
/*  WL->LFC_to_ESP_flag_old = WL->LFC_to_ESP_flag;  */
}

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LCABS_vChangeESPtoABSWheel(struct W_STRUCT *WL, struct ESC_STRUCT *WL_ESC)
{
    l_SLIP_A_wl=0;
    
    if(L_SLIP_CONTROL_OLD_wl==1) 
    {
        if(L_SLIP_CONTROL_wl==0)
        {
            l_SLIP_A_wl=1;
            WL->esp_final_mode = WL_ESC->esp_control_mode_old;
            WL->ESP_end_counter=L_1ST_SCAN;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

  #if	!__TCMF_CONTROL
    WL_ESC->esp_control_mode_old = WL_ESC->esp_control_mode;
  #endif
}
#else
void LCABS_vChangeESPtoABSWheel(struct W_STRUCT *WL)
{
    l_SLIP_A_wl=0;
    
    if(L_SLIP_CONTROL_OLD_wl==1) 
    {
        if(L_SLIP_CONTROL_wl==0)
        {
            l_SLIP_A_wl=1;
            WL->esp_final_mode = WL->esp_control_mode_old;
            WL->ESP_end_counter=L_1ST_SCAN;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
    
  #if	!__TCMF_CONTROL
    WL->esp_control_mode_old = WL->esp_control_mode;
  #endif
}
#endif



#if __ESP_ABS_COOPERATION_CONTROL 
#if	!__ABS_COMB
void LCABS_vForcedDumpHold(void)
{
    if(delta_yaw>=YAW_20DEG)
    {
        if(FL.ESP_ABS_Over_Front_Inside==1)
        {
        	LCABS_vForcedDumpHoldWheel(&FL);
        }
        else
        {
        	;
        }
        if(FR.ESP_ABS_Over_Front_Inside==1)
        {
        	LCABS_vForcedDumpHoldWheel(&FR);
        }
        else
        {
        	;
        }
        if(RL.ESP_ABS_Over_Rear_Inside==1)
        {
        	LCABS_vForcedDumpHoldWheel(&RL);
        }
        else
        {
        	;
        }
        if(RR.ESP_ABS_Over_Rear_Inside==1)
        {
            LCABS_vForcedDumpHoldWheel(&RR);
        }
        else
        {
        	;
        }
    }
    else
    {
        FL.ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;
        FR.ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;
        RL.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;
        RR.ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;
    }
    
    if(RL.ESP_ABS_Under_Rear_Outside==1)
    {
        RL.ESP_ABS_Rear_Forced_Dump_timer=0;
        LCABS_vForcedDumpHoldWheel(&RL);
    }
    else
    {
    	;
    }
    if(RR.ESP_ABS_Under_Rear_Outside==1){
        RR.ESP_ABS_Rear_Forced_Dump_timer=0;
        LCABS_vForcedDumpHoldWheel(&RR);
    }
    else
    {
    	;
    }
}
#endif


#if	__ABS_COMB
void LCABS_vForcedDumpHoldWheel(void)
#else
void LCABS_vForcedDumpHoldWheel(struct W_STRUCT *WL)
#endif
{
/*  WL->ESP_ABS_Rear_Forced_Dump_K = 0; */
    if(((REAR_WHEEL_wl==1)&&(WL->ESP_ABS_Rear_Forced_Dump_timer>0))||
       ((REAR_WHEEL_wl==0)&&(WL->ESP_ABS_Front_Forced_Dump_timer>0)))
    {
/*      if(WL->ESP_ABS_Rear_Forced_Dump_timer>=10) WL->ESP_ABS_Rear_Forced_Dump_K = 1;    */
        if(REAR_WHEEL_wl==1)
        {
            WL->ESP_ABS_Rear_Forced_Dump_timer = (int8_t)LCABS_u8DecountStableDumpNum((uint8_t)WL->ESP_ABS_Rear_Forced_Dump_timer);
        }
        else 
        {
        	WL->ESP_ABS_Front_Forced_Dump_timer = (int8_t)LCABS_u8DecountStableDumpNum((uint8_t)WL->ESP_ABS_Front_Forced_Dump_timer);
        }
        WL->AV_DUMP=1;
        WL->AV_HOLD=0;
        WL->AV_REAPPLY=0;
        WL->BUILT=0;
        WL->flags=118;
    }
/*  else if(AV_DUMP_wl==0){    */
    else 
    {
        WL->AV_DUMP=0;
        WL->AV_HOLD=1;
        WL->AV_REAPPLY=0;
        WL->BUILT=0;
        WL->flags=119;
    }
}
#endif/*__ABS_COMB*/

#if	__ABS_COMB
void LCABS_vDecideAbsEscCompLevel(struct W_STRUCT *WL)
{	
	if(REAR_WHEEL_wl==0)
	{
		if(WL->ESP_ABS_Over_Front_Inside==1)
		{
			if(delta_yaw>=YAW_20DEG)/*or moment_q_front<-20*/
            {
            	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL3;
            }
       		else if(moment_q_front<-15)
       		{
       			WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL2;
       		}
       		else if(moment_q_front<-10)
       		{
       			WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL1;
       		}
            else 
            {
            	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL0;
            }
        }
        else
        {
        	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL0;
        }
    }
    else
    {
        if((WL->ESP_ABS_Over_Rear_Inside==1)||(WL->ESP_ABS_Over_Rear_Outside==1))
        {
	    	if(delta_yaw>=YAW_20DEG)
            {
            	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL3;
            }
       		else if(moment_q_front<-15)
       		{
       			WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL2;
       		}
       		else if(moment_q_front<-10)
       		{
       			WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL1;
       		}
            else 
            {
            	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL0;
            }
        }
        else if((McrAbs(wstr)>WSTR_20_DEG)&&(delta_yaw>=YAW_3DEG))
        {
        	WL->lcabsu8AbsEscCoopLevel = U8_ABS_ESC_COOP_LEVEL1;
        }
        else
        {
        	WL->lcabsu8AbsEscCoopLevel=U8_ABS_ESC_COOP_LEVEL0;
        }
    }
}
#endif/*__ABS_COMB*/

void LCABS_vRestoreLFCVariable(void)
{    
    if(FL.LFC_to_ESP_start_flag==1)
    {
    	LCABS_vRestoreLFCVariableWheel(&FL);
    }
    else
    {
    	;
    }
    if(FR.LFC_to_ESP_start_flag==1)
    {
    	LCABS_vRestoreLFCVariableWheel(&FR);
    }
    else
    {
    	;
    }
    if(RL.LFC_to_ESP_start_flag==1)
    {
    	LCABS_vRestoreLFCVariableWheel(&RL);
    }
    else
    {
    	;
    }
    if(RR.LFC_to_ESP_start_flag==1)
    {
    	LCABS_vRestoreLFCVariableWheel(&RR);
    }
    else
    {
    	;
    }
}


void LCABS_vRecallLFCVariable(void)
{
    if(FL.LFC_to_ESP_end_flag==1)
    {
    	LCABS_vRecallLFCVariableWheel(&FL);
    }
    else
    {
    	;
    }
    if(FR.LFC_to_ESP_end_flag==1)
    {
    	LCABS_vRecallLFCVariableWheel(&FR);
    }
    else
    {
    	;
    }
    if(RL.LFC_to_ESP_end_flag==1)
    {
    	LCABS_vRecallLFCVariableWheel(&RL);
    }
    else
    {
    	;
    }
    if(RR.LFC_to_ESP_end_flag==1)
    {
    	LCABS_vRecallLFCVariableWheel(&RR);
    }
    else
    {
    	;
    }
}


void LCABS_vRestoreLFCVariableWheel(struct W_STRUCT *WL)
{
    WL->zyklus_z_backup=WL->zyklus_z;       
    WL->LFC_zyklus_z_backup=WL->LFC_zyklus_z;       
/*  WL->init_duty_backup=WL->init_duty;     */
    WL->LFC_initial_set_duty_backup=WL->LFC_initial_set_duty;
    WL->LFC_pres_rise_delay_comp_backup=WL->LFC_pres_rise_delay_comp;
    WL->LFC_special_comp_duty_backup=WL->LFC_special_comp_duty;
    #if (__VDC && __MP_COMP && __PRESS_EST)
    if(lcabsu1ValidBrakeSensor==1)
    {
        WL->LFC_MP_Cyclic_comp_duty_backup=WL->LFC_MP_Cyclic_comp_duty;
    }
    #endif
}


void LCABS_vRecallLFCVariableWheel(struct W_STRUCT *WL)
{
    if(WL->LFC_zyklus_z_backup>0) {
        WL->zyklus_z=WL->zyklus_z_backup;       
        WL->LFC_zyklus_z=WL->LFC_zyklus_z_backup;
/*      WL->init_duty=WL->init_duty_backup;    */
        WL->LFC_initial_set_duty=WL->LFC_initial_set_duty_backup;
        WL->LFC_pres_rise_delay_comp=WL->LFC_pres_rise_delay_comp_backup;
        WL->LFC_special_comp_duty=WL->LFC_special_comp_duty_backup;
/*      WL->LFC_pres_rise_comp_disable_flag=1;    */
/*      WL->LFC_special_comp_disable_flag=1;      */
        WL->LFC_s0_reapply_counter=1;
        #if (__VDC && __MP_COMP && __PRESS_EST)
	    if(lcabsu1ValidBrakeSensor==1)
        {
            WL->LFC_MP_Cyclic_comp_duty=WL->LFC_MP_Cyclic_comp_duty_backup;
	    }
	    #endif
    }
    else
    {
    	;
    }
    
    if((LFC_H_to_Split_flag==1)&&(WL->Forced_n_th_deep_slip_comp_flag==0))
    {
        if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1))||((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1)))
        {
            if((WL->LFC_n_th_deep_slip_pre_flag==0)&&(WL->LFC_initial_deep_slip_comp_flag==0))
            {
                WL->LFC_n_th_deep_slip_pre_flag=1;
                WL->LFC_n_th_deep_slip_comp_flag=1;
                WL->Forced_n_th_deep_slip_comp_flag=1;
			  
			  #if (__VDC && __MP_COMP && __PRESS_EST)
			    if(lcabsu1ValidBrakeSensor==1)
			    {
			        ;
			    }
			    else 
			    {
			  #endif
                    if(REAR_WHEEL_wl==0)
                    {
                        WL->LFC_special_comp_duty += (int16_t)U8_LFC_Jump_Down_Comp_F;
                    }
                    else
                    {
                        WL->LFC_special_comp_duty += (int16_t)U8_LFC_Jump_Down_Comp_R;
                    }   
			  #if (__VDC && __MP_COMP && __PRESS_EST)
			    }
			  #endif
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
}

#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCABSCallESPCombination
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
