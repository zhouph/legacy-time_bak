/*******************************************************************
* Project Name:
* File Name: 
* Description: 
********************************************************************/


/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallVrefCompForESP
	#include "Mdyn_autosar.h"
#endif
#include 	"LDABSCallVrefCompForESP.H"

#if __VDC
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"


#include 	"LCallMain.h"

/*******************************************************************/

/* Local Definition  ***********************************************/
#define	VREF5_DEC_LIMIT_AT_DRIVING_STATE	-16
#define	VREF5_INC_LIMIT_AT_DRIVING_STATE	8
#define	VREF5_DEC_LIMIT_AT_BRAKEING_STATE	-24

/*******************************************************************/

/* Variables Definition*********************************************/
	U8_BIT_STRUCT_t VESPF0;

    int16_t   	vref5_resol_change;
    int16_t		yaw_c_vref_resol_change;
    int16_t   	vdc_vref_resol_change;

	
	uint8_t		vdc_ref_work_clear_timer;
	int16_t		yaw_c_vref;

/*******************************************************************/

/* Local Function prototype ****************************************/

void	LDABS_vCorretVrefByYawRate(void);
/*
void	LDABS_vCompVrefOnESPControl(void);
void	LDABS_vCalESPTargetVref(struct W_STRUCT *WL);
*/
void 	LDABS_vConverseVrefForScale(void);
void    LDABS_vCalWheelBasedVref(void);
void	LDABS_vCallVrefCompForESP(void);

/*******************************************************************/
void	LDABS_vCallVrefCompForESP(void)
{
	ESP_SENSOR_RELIABLE_FLG=1;
    if((fu1AySusDet==1)||(fu1AyErrorDet==1))
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
    else if((fu1StrSusDet==1)||(fu1StrErrorDet==1))
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
    else if((fu1YawSusDet==1)||(fu1YawErrorDet==1))
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
/*
    else if(yaw_offset_reliablity<30)
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
*/
/*    else if(steer_offset_reliablity<30)*/
	else if(SAS_CHECK_OK==0)
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
/*
    else if(alat_offset_reliablity<30)
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
*/    
    #if  ! __MY_09_VREF
    else if((BACK_DIR==0)&&(((yaw_out>0)&&(alat<0))||((yaw_out<0)&&(alat>0))))
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
    else if((BACK_DIR==1)&&(((yaw_out>0)&&(alat>0))||((yaw_out<0)&&(alat<0))))
    {
    	ESP_SENSOR_RELIABLE_FLG=0;
    }
    #endif
    else
    {
    	;
    }
 #if  ! __MY_09_VREF    
    if(YAW_CDC_WORK==1)
    {
    	ESP_SENSOR_RELIABLE_FLG=1;
    }
 #endif
 
    if(YAW_CORRECTION!=ESP_SENSOR_RELIABLE_FLG)
    {
    	YAW_CORRECTION_MODE_CHANGE=1;
    }
    else
    {
    	YAW_CORRECTION_MODE_CHANGE=0;
    }
    YAW_CORRECTION=ESP_SENSOR_RELIABLE_FLG;
	LDABS_vCorretVrefByYawRate();	
	LDABS_vCalWheelBasedVref();
	
/*	LDABS_vCompVrefOnESPControl */
	LDABS_vConverseVrefForScale();
}
/*******************************************************************/
void	LDABS_vCorretVrefByYawRate(void)
{
	int16_t vehicle_speed_1;
	int16_t vehicle_speed_2;
	int16_t vehicle_speed_correted_by_yaw;
	int16_t calculated_yaw_correction;
	int16_t vehicle_speed_old;
	
	vehicle_speed_1=vref_resol_change;
	vehicle_speed_2=FZ2.voptfz_resol_change;
	vehicle_speed_old=vref5_resol_change;

	
  	if(ESP_SENSOR_RELIABLE_FLG==1) 	
  	{
/*
  		tempW2=abs(yaw_out);
  		tempW1=1;
  		tempW0=32;
  		s16muls16divs16();
*/  		
  		tempW3=McrAbs(yaw_out)/32;
  		if (tempW3>VREF_3_KPH_RESOL_CHANGE) 
  		{
  		    tempW3=VREF_3_KPH_RESOL_CHANGE;
  		}
  		else 
  		{
  		    ;
  		}
  		yaw_c_vref_resol_change=tempW3;
  		yaw_c_vref=yaw_c_vref_resol_change/8;
  		calculated_yaw_correction=yaw_c_vref_resol_change;

		
		
		tempW0=(voptfz2+VREF_0_25_KPH);
		#if __MY_09_VREF
		if(YAW_CDC_WORK==1) 
		{
			VDC_REF_WORK=1;
			vdc_ref_work_clear_timer=L_U8_TIME_10MSLOOP_1000MS;
		}
		else 
		{
			if (vdc_ref_work_clear_timer>0)
			{
				vdc_ref_work_clear_timer--;
				if(vrad_diff_max<VREF_3_KPH)
				{
					if(vdc_ref_work_clear_timer>2)
					{
						vdc_ref_work_clear_timer-=2;
					}
					else 
					{
						vdc_ref_work_clear_timer=0;
					}
				}
			}
			else 
			{
			    ;
			}
			
			if((ESP_BRAKE_CONTROL_FL==0)&&(ESP_BRAKE_CONTROL_FR==0)&&(ESP_BRAKE_CONTROL_RL==0)&&(ESP_BRAKE_CONTROL_RR==0))
			{
			    if ((McrAbs(yaw_out)<YAW_3DEG)&&(vrad_diff_max<VREF_3_KPH)
			    	&&(McrAbs(vrad_fl-vrad_fr)<VREF_2_KPH)&&(McrAbs(vrad_rl-vrad_rr)<VREF_2_KPH))
			    {
			        VDC_REF_WORK=0;
			    }
			}
			
			
			if (vdc_ref_work_clear_timer<=0)
			{
				VDC_REF_WORK=0;
			}
			else 
			{
			    ;
			}
		}		
		#else
		if(YAW_CDC_WORK==1) 
		{
			VDC_REF_WORK=1;
			vdc_ref_work_clear_timer=L_U8_TIME_10MSLOOP_500MS;
		}
		else 
		{
			if (vdc_ref_work_clear_timer>0)
			{
				vdc_ref_work_clear_timer--;
			}
			else 
			{
			    ;
			}
			
			if(tempW0>vref_alt) 
			{
			    tempB0=ESP_BRAKE_CONTROL_FL+ESP_BRAKE_CONTROL_FR+ESP_BRAKE_CONTROL_RL+ESP_BRAKE_CONTROL_RR;

			    if ((abs(yaw_out)<YAW_5DEG)&&(vrad_diff_max<VREF_1_5_KPH))
			    {
			        VDC_REF_WORK=0;
			    }
			    else if(tempB0==0)
			    {
			        VDC_REF_WORK=0;
			    }
			    else
			    {
			        ;
			    }
			}
			else if (vdc_ref_work_clear_timer<=0)
			{
				VDC_REF_WORK=0;
			}
			else 
			{
			    ;
			}
		}
		
		#endif
		vehicle_speed_correted_by_yaw=vehicle_speed_1-calculated_yaw_correction;
			
	}
	else 
	{
		yaw_c_vref=0;
		VDC_REF_WORK=0;
		vdc_ref_work_clear_timer=0;
		yaw_c_vref_resol_change=0;

  		vehicle_speed_correted_by_yaw=vehicle_speed_1;
  		 		
    }  
	

	vref5_resol_change=vehicle_speed_correted_by_yaw;
	vref5=vref5_resol_change/8;
	    		
    if(vref5_resol_change > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	vref5_resol_change=VREF_MAX_RESOL_CHANGE;
    }
    else if(vref5_resol_change < (int16_t)VREF_MIN_SPEED) 
    {
    	vref5_resol_change=VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }

	
	if(vref5 > (int16_t)VREF_MAX) 
    {
    	vref5=VREF_MAX;
    }
    else if(vref5 < (int16_t)VREF_MIN_SPEED_RESOL_8)
    {
    	 vref5=VREF_MIN_SPEED_RESOL_8;
    }
    else
    {
        ;
    }
}

/*******************************************************************/
/*
void	LDABS_vCompVrefOnESPControl(void)
{
	if(ESP_BRAKE_CONTROL_FL==1) 
	{
	    LDABS_vCalESPTargetVref(&RR);
	}
	else 
	{
	    ;
	}
	if(ESP_BRAKE_CONTROL_FR==1) 
	{
	    LDABS_vCalESPTargetVref(&RL);
	}
	else 
	{
	    ;
	}
	if(ESP_BRAKE_CONTROL_RL==1) 
	{
	    LDABS_vCalESPTargetVref(&FR);
	}
	else 
	{
	    ;
	}
	if(ESP_BRAKE_CONTROL_RR==1) 
	{
	    LDABS_vCalESPTargetVref(&FL);
	}
	else 
	{
	    ;
	}
}

*/

/*******************************************************************/
/*
void    LDABS_vCalESPTargetVref(struct W_STRUCT *WL)
{

    if(ESP_ERROR_FLG==0) 
    {
        if((PARTIAL_BRAKE==0)&&(ABS_wl==1)) 
        {
        	
        	tempW1=21;
        	tempW0=20;
        	#if __WL_SPEED_RESOL_CHANGE
        	tempW2=WL->wvref_resol_change;        	
        	#else
        	tempW2=WL->wvref;
        	#endif
        	s16muls16divs16();
        	#if __WL_SPEED_RESOL_CHANGE
        	WL->wvref_resol_change=tempW3;
        	WL->wvref=WL->wvref_resol_change>>3;
        	#else
        	WL->wvref=tempW3;
        	#endif
        }
        else 
        {
            ;
        }
    }
    else 
    {
        ;
    }
}
*/
/*******************************************************************/
void LDABS_vConverseVrefForScale(void)
{
    vdc_vref=vref5;         
	vdc_vref_resol_change=vref5_resol_change;

/*---------------------------------------------------------------------
 	Calculation of vrefk (km/h)                                         
---------------------------------------------------------------------*/
    vrefk_old = vrefk;
/*    vrefk = (uint16_t)( (((uint32_t)vdc_vref)*10)/8);   r : 0.1    */

	tempUW2 = vdc_vref_resol_change; tempUW1 = 10; tempUW0 = 64;
    u16mulu16divu16();
    vrefk = tempUW3;    
/*---------------------------------------------------------------------
 	Calculation of vrefm (m/sec)                                         
---------------------------------------------------------------------*/                
/*    vrefm = (uint16_t)( (((uint32_t)vdc_vref)*10)/29);      r : 0.1; 29/8=3.6  */
	tempUW2 = vdc_vref_resol_change; tempUW1 = 10; tempUW0 = 230;

    u16mulu16divu16();
    vrefm = tempUW3;    
    
    if(vrefm<=27) 
    {
        vrefm=27;
    }
    else 
    {
        ;
    }
}
/*******************************************************************/

void    LDABS_vCalWheelBasedVref(void)
{	

/*
	tempW2=yaw_c_vref_resol_change;	
	tempW1=1;
	tempW0=3;
	s16muls16divs16();
	tempW6=tempW3;
*/
/*
	tempW6=yaw_c_vref_resol_change/3;
    tempW3=0;
    if((abs_wstr>WSTR_250_DEG)&&(vref<VREF_30_KPH)) 
    {
        tempW2=abs_wstr-WSTR_250_DEG;
        tempW0=52; 
        tempW1=1;
        s16muls16divs16();
        tempW3=(abs_wstr-WSTR_250_DEG)/52;
        
        if(tempW3>VREF_2_KPH_RESOL_CHANGE)
        {
            tempW3=VREF_2_KPH_RESOL_CHANGE;
        }
        else 
        {
            ;
        }
        tempW3+=tempW6;
    }
    else 
    {
        ;
    }	
    
*/
	tempW3=wstr_correction;
    if(ESP_SENSOR_RELIABLE_FLG==0)
    {
	    FL.wvref_resol_change=vref_resol_change;
	    FR.wvref_resol_change=vref_resol_change;
	    RL.wvref_resol_change=vref_resol_change;
	    RR.wvref_resol_change=vref_resol_change;    	
    }
    else
    {
	    FL.wvref_resol_change=vref5_resol_change;
	    FR.wvref_resol_change=vref5_resol_change;
	    RL.wvref_resol_change=vref5_resol_change;
	    RR.wvref_resol_change=vref5_resol_change;
	    if(((yaw_out>0)&&(BACK_DIR==0))||((yaw_out<0)&&(BACK_DIR==1)))
        {
            FL.wvref_resol_change-=yaw_c_vref_resol_change;
            FL.wvref_resol_change+=tempW3;
            FR.wvref_resol_change+=yaw_c_vref_resol_change;
            FR.wvref_resol_change+=tempW3;
            RL.wvref_resol_change-=yaw_c_vref_resol_change;
            RR.wvref_resol_change+=yaw_c_vref_resol_change;
        }
        else 
        {
        
            FL.wvref_resol_change+=yaw_c_vref_resol_change;
            FL.wvref_resol_change+=tempW3;
            FR.wvref_resol_change-=yaw_c_vref_resol_change;
            FR.wvref_resol_change+=tempW3;
            RL.wvref_resol_change+=yaw_c_vref_resol_change;
            RR.wvref_resol_change-=yaw_c_vref_resol_change;
        }
	}
	/*
   	if((vref5>(INT)VREF_4_KPH) && (ESP_SENSOR_RELIABLE_FLG==1))
   	{
        if(((yaw_out>0)&&(BACK_DIR==0))||((yaw_out<0)&&(BACK_DIR==1)))
        {
            FL.wvref_resol_change-=yaw_c_vref_resol_change;
            FL.wvref_resol_change+=tempW3;
            FR.wvref_resol_change+=yaw_c_vref_resol_change;
            FR.wvref_resol_change+=tempW3;
            RL.wvref_resol_change-=yaw_c_vref_resol_change;
            RR.wvref_resol_change+=yaw_c_vref_resol_change;
        }
        else 
        {
        
            FL.wvref_resol_change+=yaw_c_vref_resol_change;
            FL.wvref_resol_change+=tempW3;
            FR.wvref_resol_change-=yaw_c_vref_resol_change;
            FR.wvref_resol_change+=tempW3;
            RL.wvref_resol_change+=yaw_c_vref_resol_change;
            RR.wvref_resol_change-=yaw_c_vref_resol_change;
        }
    }
    else 
    {
        ;
    }
    */
    wvref_fl=FL.wvref_resol_change/8;
    wvref_fr=FR.wvref_resol_change/8;
    wvref_rl=RL.wvref_resol_change/8;
    wvref_rr=RR.wvref_resol_change/8;

}
/*******************************************************************/
#endif /* __VDC */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallVrefCompForESP
	#include "Mdyn_autosar.h"
#endif
