/****************************************************
 *  LAABSIdbGenerateDuty.c                                         
 *  Created on: 25-9-2014 PM 7:27:56
 *  Implementation of the Class LAABSIdbGenerateDuty       
 *  Original author: seongyon.ryu                     
 ****************************************************/
#include "LAABSIdbGenerateDuty.h"
#include "LACallMain.h"
#include "LCABSCallControl.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "LCABSCallESPCombination.h"
#include "LDABSDctWheelStatus.h"

#include "LCEBDCallControl.h"

#include "LCABSStrokeRecovery.h"

#include "Hardware_Control.h"




WHEEL_VV_OUTPUT_t laabs_FL1HP,laabs_FL2HP,laabs_FR1HP,laabs_FR2HP;
WHEEL_VV_OUTPUT_t laabs_RL1HP,laabs_RL2HP,laabs_RR1HP,laabs_RR2HP;

/* LocalFunction prototype ***************************************************/
static void LAABS_vInitializeLFCVarsBFABS(struct W_STRUCT* WL_temp);
static void LAABS_vDecideLFCVars(struct W_STRUCT* WL_temp);
static void LAABS_vSetDumpScantime(void);
static uint8_t LCABS_u8GetDumpTimePerDumpCnt(uint8_t u8DumpCounter, uint8_t DumpTim1stDumpscan, uint8_t DumpTim23Dumpscan, uint8_t DumpTim45Dumpscan, uint8_t DumpTimAb5Dumpscan);
static void LAABS_vSetDumpScantime1stCycle(void);
static void LAABS_vCompensateLFCDuty(void);
static void LAABS_vCompLFCCyclicCompDuty(struct W_STRUCT* WL_temp);
static void LAABS_vCompLFCSpecialCompDuty(struct W_STRUCT* WL_temp);
static void LAABS_vCompLFCMPCyclicCompDuty(struct W_STRUCT* WL_temp);
static void LAABS_vLimitDiffofFrontLFCDuty(void);
static void LAABS_vSyncBothWheelDuty(struct W_STRUCT* pW1, struct W_STRUCT* pW2);
static void LAABS_vCalcLFCInitialDuty(struct W_STRUCT* WL_temp);
static int16_t LAABS_s16SetInitDutyFromDp(void);
static void LAABS_vGenerateLFCDutyWL(struct W_STRUCT* WL_temp);
static void LAABS_vLimitDiffofRearLFCDuty(void);
static void LAABS_vLimitDiffofRearAtSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE);
static void LAABS_vLimitDiffofRearAtnSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE);
static void LAABS_vInterfaceValveOutputs(const struct W_STRUCT* pActWL, WHEEL_VV_OUTPUT_t * laabsHP1, WHEEL_VV_OUTPUT_t * laabsHP2);
/*****************************************************************************/

/* Local Definition*********************************************************/
#define S16_ReferenceMCPress        MPRESS_150BAR
#define MAX_MP_COMP_DUTY            40 /* 5 : orifice 0.9 */

#define __FRONT_REAR_MP_COMP_SEPERATE ENABLE

/**************************************************************************/

/**
 * FUNCTION NAME:   LAABS_vGenerateLFCDuty
 * CALLED BY:	LAABS_vCallActHW in LAABSCallActHW.c
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  ABS duty actuation main call function
 */
void LAABS_vGenerateLFCDuty(void)
{
    LAABS_vInitializeLFCVarsBFABS(&FL);
    LAABS_vInitializeLFCVarsBFABS(&FR);
    LAABS_vInitializeLFCVarsBFABS(&RL);
    LAABS_vInitializeLFCVarsBFABS(&RR);

#if __VDC
    LCABS_vRestoreLFCVariable();
#endif

    LAABS_vDecideLFCVars(&FL);
    LAABS_vDecideLFCVars(&FR);
    LAABS_vDecideLFCVars(&RL);
    LAABS_vDecideLFCVars(&RR);

    LAABS_vCompensateLFCDuty();

    LAABS_vLimitDiffofFrontLFCDuty();

#if __VDC
    LCABS_vRecallLFCVariable();
#endif

    LAABS_vCalcLFCInitialDuty(&FL);
    LAABS_vCalcLFCInitialDuty(&FR);
    LAABS_vCalcLFCInitialDuty(&RL);
    LAABS_vCalcLFCInitialDuty(&RR);

    LAABS_vGenerateLFCDutyWL(&FL);
    LAABS_vGenerateLFCDutyWL(&FR);

    LAABS_vGenerateLFCDutyWL(&RL);
    LAABS_vGenerateLFCDutyWL(&RR);

#if __CHANGE_MU
	if((L_SLIP_CONTROL_rl==0)&&(L_SLIP_CONTROL_rr==0)&&(ABS_fz==1))
	{
		LAABS_vLimitDiffofRearLFCDuty();
	}
#endif

/*#if __HW_TEST_MODE
	LAABS_vTestWheelNoValve();
#endif*/

    FL.pwm_duty_temp_dash = FL.pwm_duty_temp;
    FR.pwm_duty_temp_dash = FR.pwm_duty_temp;
    RL.pwm_duty_temp_dash = RL.pwm_duty_temp;
    RR.pwm_duty_temp_dash = RR.pwm_duty_temp;

  #if (__MGH_80_10MS == ENABLE)
    LAABS_vInterfaceValveOutputs(&FL,&laabs_FL1HP,&laabs_FL2HP);
    LAABS_vInterfaceValveOutputs(&FR,&laabs_FR1HP,&laabs_FR2HP);
    LAABS_vInterfaceValveOutputs(&RL,&laabs_RL1HP,&laabs_RL2HP);
    LAABS_vInterfaceValveOutputs(&RR,&laabs_RR1HP,&laabs_RR2HP);
  #endif

	if(ABS_fz==1)
	{
		la_FL1HP = laabs_FL1HP;
		la_FL2HP = laabs_FL2HP;
		la_FR1HP = laabs_FR1HP;
		la_FR2HP = laabs_FR2HP;

		la_RL1HP = laabs_RL1HP;
		la_RL2HP = laabs_RL2HP;
		la_RR1HP = laabs_RR1HP;
		la_RR2HP = laabs_RR2HP;
	}
	else
	{
		if(EBD_RA==1)
		{
			la_RL1HP = laabs_RL1HP;
			la_RL2HP = laabs_RL2HP;
			la_RR1HP = laabs_RR1HP;
			la_RR2HP = laabs_RR2HP;
		}
	}
}

/**
 * FUNCTION NAME:    LAABS_vInitializeLFCVarsBFABS
 * CALLED BY:	LAABS_vGenerateLFCDuty
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  Valve actuation variable initialization
 */
static void LAABS_vInitializeLFCVarsBFABS(struct W_STRUCT* WL_temp)
{
	WL = WL_temp;

    if((FSF_wl==1)&&(ABS_fz==0)) {
        WL->Holding_LFC_duty=0;
        WL->LFC_reapply_end_duty=0;
        WL->LFC_reapply_current_duty=0;

        WL->pwm_temp=0;
        WL->pwm_duty_temp=0;
        WL->pwm_duty_temp_dash=0;

        WL->MSL_long_hold_flag=0;

	#if __CHANGE_MU
        WL->Duty_Limitation_flag=0;
		WL->lcabsu1TarDeltaPLimitFlg=0;
	#endif

	#if __VDC && __WP_DROP_COMP
        WL->Estimated_Press_init=0;
        WL->Estimated_Press_end=0;
        WL->Drop_Press_Comp_T=0;
        WL->WP_Drop_comp_flag=0;
        WL->LFC_WP_Drop_comp_duty=0;
	#endif

	#if __VDC
        WL->ESP_partial_dump_counter=0;
        WL->Forced_n_th_deep_slip_comp_flag = 0;
      #if __ESP_ABS_COOPERATION_CONTROL
        if(REAR_WHEEL_wl==1) {WL->ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;}
        else {WL->ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;}
      #endif
	#endif

    }
}

/**
 * FUNCTION NAME:  LAABS_vDecideLFCVars
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:   NC on-time decision and related variable setting
 */
static void LAABS_vDecideLFCVars(struct W_STRUCT* WL_temp)
{
	WL = WL_temp;

	WL->Holding_LFC_duty=0;

	if((L_SLIP_CONTROL_wl!=1)&&(BTC_fz!=1))
	{
	    if((BUILT_wl==1) && (ABS_wl==1))
	    {
	      #if __CHANGE_MU
	        if(REAR_WHEEL_wl==1) {WL->Duty_Limitation_flag=0;}
	      #endif
	    }
	    else
	    {
	        if(HV_VL_wl==1)
	        {
	            if(AV_VL_wl==1)/***************** Dump! **************************************/
	            {
	            	LAABS_vSetDumpScantime();

	                if((STAB_A_wl==1)||(WL->SL_Dump_Start==1))
	                {          /* The end of stabil(starting dump point) */
	                    if(WL->LFC_s0_reapply_counter_old>=1)
	                    {
			                if((REAR_WHEEL_wl==1) || (WL->LFC_fictitious_cycle_flag2==0))
			                {
	                            WL->LFC_reapply_end_duty=WL->LFC_reapply_current_duty;
	                        }
	                    }
	                }
	              #if __CHANGE_MU
	                WL->Duty_Limitation_flag=0;
	                WL->lcabsu1TarDeltaPLimitFlg=0;
	              #endif
	            }
	            else /******************* Hold!!! ***********************************/
	            {
	                if(WL->LFC_built_reapply_flag==0)
	                {
	                  #if (L_CONTROL_PERIOD>=2)
	                    if(WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_01)
	                    {
	                        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -U8_BASE_CTRLTIME);
	                    }
	                    else
	                    {
	                        WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -(U8_BASE_CTRLTIME*2));
	                    }
	                  #else
	                  	WL->on_time_outlet_pwm = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->on_time_outlet_pwm, -U8_BASE_CTRLTIME);
	                  #endif
	                    if(WL->s_diff>0) {WL->on_time_outlet_pwm=0;}

	                }
	                else
	                {
	                      #if __CHANGE_MU
	                    if((REAR_WHEEL_wl==1)&&(WL->MSL_BASE==1))
	                    {
	                    	WL->Duty_Limitation_flag=0;
	                    	WL->lcabsu1TarDeltaPLimitFlg=0;
	                    }
	                      #endif
	                    if(WL->Hold_Duty_Comp_flag==0)  {
	                      #if __CHANGE_MU
	                        if(REAR_WHEEL_wl==0) {
	                            WL->Holding_LFC_duty=1;
	                        }
	                        else {
	                            if(WL->Duty_Limitation_flag==0) {
	                                WL->Holding_LFC_duty=1;
	                            }
	                        }
	                      #else
	                        WL->Holding_LFC_duty=1;
	                      #endif
	                    }
	                }
	            }
	        }
	        else /****************** Reapply!! ****************************/
	        {
	            WL->on_time_outlet_pwm= 0;
	        }
	    }

	    if(WL->LFC_s0_reapply_counter >=1) {
	        if(WL->Holding_LFC_duty ==1) {                                          /*1228. start */
	          #if __REORGANIZE_ABS_FOR_MGH60
	        	if(WL->timer_keeping_LFC_duty < 200) {WL->timer_keeping_LFC_duty = WL->timer_keeping_LFC_duty + L_1SCAN;}
	          #else
	            WL->timer_keeping_LFC_duty = timer_keeping_LFC_duty + L_1SCAN;

	            if(WL->timer_keeping_LFC_duty >=(uint8_t)(WL->hold_timer_new_ref2+L_1SCAN)) {
	                if(WL->timer_keeping_LFC_duty > 200) {WL->timer_keeping_LFC_duty  = timer_keeping_LFC_duty - L_1SCAN;} /* added on 2002.winter */
	            }
	          #endif
	        }
	        else {
	            WL->timer_keeping_LFC_duty =0;
	        }
	    }
	}

}

/**
 * FUNCTION NAME:   LAABS_vSetDumpScantime
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:   NC dump scantime decision main call function
 */
static void LAABS_vSetDumpScantime(void)
{
	uint8_t Inhibit_Increase_Dumpscantime;
	int16_t   DumpScanRefMu;

	DumpScanRefMu=afz;

	Inhibit_Increase_Dumpscantime = 0;
	WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;

	if((WL->LFC_UNSTABIL_counter>=L_TIME_170MS) && (WL->LFC_dump_counter>=8))
	{
		if(WL->peak_acc<ARAD_8G0) {WL->Jump_Mu_Cascading_sus_flag = 1;}
		else {WL->Jump_Mu_Cascading_sus_flag = 0;}
	}
	else {WL->Jump_Mu_Cascading_sus_flag = 0;}

	  #if __VDC && __MP_COMP
	if(((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1)) && ((FSF_wl==1) || (WL->Jump_Mu_Cascading_sus_flag==1)))
	{
		LAABS_vSetDumpScantime1stCycle();
	}
	else
      #endif/*#if __VDC && __MP_COMP*/
	{
		if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1) || (Rough_road_detect_vehicle==1))
		{
			Inhibit_Increase_Dumpscantime = 1;
		}

		if(REAR_WHEEL_wl==1)
		{
		  #if __INCREASE_SP_LOW_REAR_DUMPTIM
			if((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
			   &&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU))
			{
				if(U8_SP_LOW_RER_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)
				{
					DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MU;
				}
				else if(U8_SP_LOW_RER_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)
				{
					DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MED_MU;
				}
				else { } /*afz*/
			}
		  #endif /*__INCREASE_SP_LOW_REAR_DUMPTIM*/

			if((AFZ_OK==1) && (afz<-(int16_t)U8_HIGH_MU))
			{
				if(vref >= (int16_t)VREF_20_KPH)
				{
					if(STAB_A_wl==1)
					{
						if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LHDF_R;} /* 7 */
						else if(WL->High_dump_factor<=(2000)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_MHDF_R;} /* 9 */
						else {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_HHDF_R;}
					}
					else { }
				}
				else
				{
					if(STAB_A_wl==1)
					{
						if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_LHDF_B_20_R;}
						else                             {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_B_20_R;}
					}
					else { }
				}
			}
			else if ((AFZ_OK==1) && (DumpScanRefMu>=-(int16_t)U8_PULSE_LOW_MED_MU))
			{
				if(vref >= (int16_t)VREF_10_KPH) {
					if(DumpScanRefMu>=-(int16_t)U8_PULSE_LOW_MU)
					{
						WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMu_R,U8_2nd3rd_Dumpscantime_LMu_R,U8_4th5th_Dumpscantime_LMu_R,U8_nth_Dumpscantime_LMu_R);
					}
					else
					{
						WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_R,U8_2nd3rd_Dumpscantime_LMMu_R,U8_4th5th_Dumpscantime_LMMu_R,U8_nth_Dumpscantime_LMMu_R);
					}

					if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
					else if((DumpScanRefMu<-(int16_t)U8_PULSE_LOW_MU) && (Inhibit_Increase_Dumpscantime==1))
					{
						WL->on_time_outlet_pwm = 7;
					}
					else { }
				}
				else
				{
					if(afz>=-(int16_t)U8_PULSE_LOW_MU) {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMu_R;}
					else {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMMu_R;}
				}
			}
			else if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_MEDIUM_MU) && (Inhibit_Increase_Dumpscantime==0))
			{
				if(vref >= (int16_t)VREF_20_KPH)
				{
					if(WL->LFC_dump_counter==1)
					{
						WL->on_time_outlet_pwm = U8_1st_Dumpscantime_MMu_R;
					}
					else
					{
						WL->on_time_outlet_pwm = U8_nth_Dumpscantime_MMu_R;
					}
				}
				else
				{
					WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_MMu_R;
				}
			}
			else if(EBD_wl==1)
			{
				if(vref >= (int16_t)VREF_20_KPH)
				{
					if(WL->EBD_Dump_counter<=1)
					{
						WL->on_time_outlet_pwm = U8_1st_Dumpscantime_EBD_R;
					}
					else
					{
						WL->on_time_outlet_pwm = U8_nth_Dumpscantime_EBD_R;
					}
				}
				else {}
			}
			else {}
		}
		else/*Front wheel*/
		{
		  #if __INCREASE_SP_LOW_FRONT_DUMPTIM
			if((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
			   &&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)
			)
			{
				if(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)
				{
					DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MU;
				}
				else if(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)
				{
					DumpScanRefMu = -(int16_t)U8_PULSE_LOW_MED_MU;
				}
				else { } /*afz*/
			}
		  #endif

			if((AFZ_OK==1) && (afz<-(int16_t)U8_HIGH_MU))
			{
				if(vref >= (int16_t)VREF_20_KPH)
				{
					if(STAB_A_wl==1)
					{
						if(WL->High_dump_factor<=(1000))      {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LHDF_F;}
						else if(WL->High_dump_factor<=(1500)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_LMHDF_F;}
						else if(WL->High_dump_factor<=(2500)) {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_MHDF_F;}
						else {WL->on_time_outlet_pwm = U8_1st_Dumpscantime_HMu_HHDF_F;}
					}
					else if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = 5;}
					else { }
				}
				else
				{
					if(STAB_A_wl==1)
					{
						if(WL->High_dump_factor<=(1000)) {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_LHDF_B_20_F;}
						else                             {WL->on_time_outlet_pwm = U8_1st_Dumptime_HMu_B_20_F;}
					}
					else { }
				}
			}
			else if(!((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==0)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==1)))&&(WL->PULSE_DOWN_Forbid_flag==0)))
			{
			  #if   __REAR_D && ((__CAR != GM_TAHOE) ||(__CAR !=GM_SILVERADO))
				if((WL->LFC_UNSTABIL_counter_old>=U8_UnstableTim_for_IncDumptime)&&(WL->peak_slip_old >= -S8_MaxSlipForIncFrontDumpT_RWD)&&(WL->LFC_dump_counter_old>=U8_DumpRefForIncFrontDumpT_RWD)&&(Inhibit_Increase_Dumpscantime==0)) {
			  #else
				if((WL->LFC_UNSTABIL_counter_old>=U8_UnstableTim_for_IncDumptime)&&(Inhibit_Increase_Dumpscantime==0)) {
			  #endif
					if(((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MU))
						#if __INCREASE_SP_LOW_FRONT_DUMPTIM
					  || ((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
						  &&(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MU)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU))
						#endif
					)
					{
						if(vref >= (int16_t)VREF_10_KPH) {
							WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMu_F,U8_2nd3rd_Dumpscantime_LMu_F,U8_4th5th_Dumpscantime_LMu_F,U8_nth_Dumpscantime_LMu_F);

							if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
						}
						else {WL->on_time_outlet_pwm = U8_B_10KPH_Dumpscantime_LMu_F;}
					}

					else if(((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MED_MU))
						#if __INCREASE_SP_LOW_FRONT_DUMPTIM
					  || ((LFC_Split_flag==1)&&(((MSL_BASE_rl==1) && (LEFT_WHEEL_wl==1)) || ((MSL_BASE_rr==1) && (LEFT_WHEEL_wl==0)))
						  &&(SP_LOW_FRT_DUMPTIM_AFZ<=U8_PULSE_LOW_MED_MU)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU))
						#endif
					)
					{
						if(vref >= (int16_t)VREF_20_KPH)
						{
							if(afz>=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))
							{
								WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_F,U8_2nd3rd_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F);
							}
							else
							{
								WL->on_time_outlet_pwm = LCABS_u8GetDumpTimePerDumpCnt(WL->LFC_dump_counter,U8_1st_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F,U8_nth_Dumpscantime_LMMu_F);
							}
							if(WL->CAS_SUSPECT_flag==1) {WL->on_time_outlet_pwm = WL->on_time_outlet_pwm + 7;}
						}
						else {WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_LMMu_F;}
					}
					else if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_MEDIUM_MU))
					{
						if(vref >= (int16_t)VREF_20_KPH)
						{
							if(WL->LFC_dump_counter==1)
							{
								WL->on_time_outlet_pwm = U8_1st_Dumpscantime_MMu_F;
							}
							else
							{
								WL->on_time_outlet_pwm = U8_nth_Dumpscantime_MMu_F;
							}
						}
						else
						{
							WL->on_time_outlet_pwm = U8_B_20KPH_Dumpscantime_MMu_F;
						}
					}
					else { }
				}
				else { }
			}
		  #if __STABLE_DUMPHOLD_ENABLE
			else if(WL->lcabsu1StabDump == 1)
			{
				if((AFZ_OK==1) && (afz <= -(int16_t)U8_PULSE_UNDER_LOW_MED_MU))
				{
					WL->on_time_outlet_pwm = U8_STABDUMP_SCANTIME_F;
				}
				else {/*default*/}
			}
		  #endif
		   #if __VDC && __ESP_SPLIT_2
			else if(WL->GMA_ESP_Control==1) {WL->on_time_outlet_pwm = 5;}
		   #endif
			else { }
		}
	}

  #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
	if(WL->lcabsu1CornerRearOutCtrl==1)
	{
		if(WL->LFC_dump_counter==1)
		{
			if(WL->on_time_outlet_pwm<21)
			{
				WL->on_time_outlet_pwm = 21;
			}
			else{}
		}
		else if(WL->LFC_dump_counter==2)
		{
			if(WL->on_time_outlet_pwm<14)
			{
				WL->on_time_outlet_pwm = 14;
			}
			else{}
		}
		else{}
	}
	else{}
  #endif

  #if __PRESS_EST_ABS
	if((WL->LOW_MU_SUSPECT_by_IndexMu==1) && (LOW_to_HIGH_suspect==0))
	{
		WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)(WL->on_time_outlet_pwm*2),21,35);
	}
	else
	{
		WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->on_time_outlet_pwm,3,28);
	}
  #else
	WL->on_time_outlet_pwm = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->on_time_outlet_pwm,3,28);
  #endif
}

/**
 * FUNCTION NAME:  LCABS_u8GetDumpTimePerDumpCnt
 * CALLED BY:	LAABS_vSetDumpScantime
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  A map used for determining dump scantime
 */
static uint8_t LCABS_u8GetDumpTimePerDumpCnt(uint8_t u8DumpCounter, uint8_t DumpTim1stDumpscan, uint8_t DumpTim23Dumpscan, uint8_t DumpTim45Dumpscan, uint8_t DumpTimAb5Dumpscan)
{
    if(u8DumpCounter==1)
    {
        return DumpTim1stDumpscan;
    }
    else if(u8DumpCounter<=3)
    {
        return DumpTim23Dumpscan;
    }
    else if(u8DumpCounter<=5)
    {
        return DumpTim45Dumpscan;
    }
    else
    {
        return DumpTimAb5Dumpscan;
    }
}

/**
 * FUNCTION NAME:    LAABS_vSetDumpScantime1stCycle
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:   Dump scantime decision in 1st cycle in only ESC system
 */
static void LAABS_vSetDumpScantime1stCycle(void)
{
	if(STABIL_wl==0)
    {
        if(WL->s16_Estimated_Active_Press<=S16_WPThres_for_IncDumptim_1st)
        {
            if(WL->s16_Estimated_Active_Press<=MPRESS_5BAR)
            {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_below_5bar;}
                else {tempW1 = Press_Dec_gain_Rear_below_5bar;}
                tempW4 = MPRESS_1BAR;
            }
            else if(WL->s16_Estimated_Active_Press<=MPRESS_10BAR)
            {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_5_10bar;}
                else {tempW1 = Press_Dec_gain_Rear_5_10bar;}
                tempW4 = MPRESS_3BAR;
            }
            else
            {
                if(REAR_WHEEL_wl==0) {tempW1 = Press_Dec_gain_Front_10_70bar;}
                else {tempW1 = Press_Dec_gain_Rear_10_70bar;}
                tempW4 = MPRESS_2BAR + (WL->s16_Estimated_Active_Press/10);
            }

            tempW0 = WL->s16_Estimated_Active_Press - tempW4;
            if(tempW0<=0) {tempW0 = 0;}
            tempW2 = tempW1 * (int16_t)(LDABS_u16FitSQRT((uint16_t)tempW0));
            tempW3 = tempW2/4;

            if(tempW3<1){tempW3=1;} /* Prevent division by zero */
            tempW5 = (70*7)/tempW3; /* MPRESS_7BAR = 70; */

            if(tempW5<=5) {tempW5 = 5;}
            else if(tempW5>=21) {tempW5 = 21;}
            else { }

            WL->on_time_outlet_pwm = (uint8_t)tempW5;
        }
        else  {WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;}
    }
    else {WL->on_time_outlet_pwm = U8_DEFAULT_DUMPSCANTIME;}
}

/**
 * FUNCTION NAME:   LAABS_vCompensateLFCDuty
 * CALLED BY:   NO duty compensation main function
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:
 */
static void LAABS_vCompensateLFCDuty(void)
{
	LAABS_vCompLFCCyclicCompDuty(&FL);
	LAABS_vCompLFCCyclicCompDuty(&FR);
	LAABS_vCompLFCCyclicCompDuty(&RL);
	LAABS_vCompLFCCyclicCompDuty(&RR);

	LAABS_vCompLFCSpecialCompDuty(&FL);
	LAABS_vCompLFCSpecialCompDuty(&FR);
	LAABS_vCompLFCSpecialCompDuty(&RL);
	LAABS_vCompLFCSpecialCompDuty(&RR);

	  #if __VDC && __MP_COMP
	/*if(lcabsu1ValidBrakeSensor==1)
	{
	  LAABS_vCompLFCMPCyclicCompDuty(&FL);
	  LAABS_vCompLFCMPCyclicCompDuty(&FR);
	  LAABS_vCompLFCMPCyclicCompDuty(&RL);
	  LAABS_vCompLFCMPCyclicCompDuty(&RR);
	}*/
	  #endif
}

/**
 * FUNCTION NAME:  LAABS_vCompLFCCyclicCompDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  NO valve cyclic compenation
 */
static void LAABS_vCompLFCCyclicCompDuty(struct W_STRUCT* WL_temp)
{
	int16_t s16CyclicComp;

	WL = WL_temp;

	if((FSF_wl==1)&&(ABS_wl==0))
	{
		WL->LFC_pres_rise_delay_comp = 0;
	}
	else if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1SCAN))
	{
	  #if   __VDC && __ABS_COMB
		if(WL->L_SLIP_CONTROL==0) {
	  #endif
		if((WL->LFC_zyklus_z<=1) || (WL->LFC_fictitious_cycle_flag==1)) {WL->LFC_pres_rise_delay_comp = 0;}
		else if(WL->Check_double_brake_flag==1) {WL->LFC_pres_rise_delay_comp = 0;}
		else if(WL->LFC_Cyclic_Comp_flag==1)
		{
			s16CyclicComp = WL->LFC_built_reapply_counter_delta/((int16_t)WL->hold_timer_new_ref_old+L_1SCAN);

			if(s16CyclicComp >= 0)
			{
				s16CyclicComp = LCABS_s16Interpolation2P(s16CyclicComp, 0, 20, 0, -5);
/*				if(WL->LFC_zyklus_z==2){
					if(s16CyclicComp > 10) {s16CyclicComp = 10;}
				}
			  #if (__ROUGH_COMP_IMPROVE==ENABLE)
				else if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1)){
					if(s16CyclicComp > 10) {s16CyclicComp = 10;}
				}
			  #endif
				else {
					if(s16CyclicComp > 5) {s16CyclicComp = 5;}
				}*/
			}
			else
			{
				s16CyclicComp = LCABS_s16Interpolation2P(s16CyclicComp, -20, 0, 5, 0);
/*				if(vref<=VREF_20_KPH) {
					s16CyclicComp = s16CyclicComp/2;
				}

			  #if __UNSTABLE_REAPPLY_ENABLE
				if(WL->Reapply_Accel_cycle_old==1) {
					if(s16CyclicComp < -1) {s16CyclicComp = -1;}
				}
				else
			  #endif
				{
					if(s16CyclicComp < -4) {s16CyclicComp = -4;}
				}*/
			}

			WL->LFC_pres_rise_delay_comp = WL->LFC_pres_rise_delay_comp + s16CyclicComp;
		}
		else { }
	  #if   __VDC && __ABS_COMB
		}
	  #endif
	}
	else { }
}

/**
 * FUNCTION NAME:   LAABS_vCompLFCSpecialCompDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  NO valve special compensation such as L2H/H2L detection
 */
static void LAABS_vCompLFCSpecialCompDuty(struct W_STRUCT* WL)
{
    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_special_comp_duty = 0;
        WL->lau1SPHighSideCompFlag = 0;
    }
    else if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
        if(WL->Check_double_brake_flag==1) {
            WL->LFC_special_comp_duty = 0;
        }
        if((WL->LFC_zyklus_z==1) || (WL->Check_double_brake_flag==1)) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1))
            {
                ;
            }
            else
            {
          #endif
            if(WL->LFC_initial_deep_slip_comp_flag==1) {
                if(REAR_WHEEL_wl==0) {
                    WL->LFC_special_comp_duty = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                }
                else {
                    WL->LFC_special_comp_duty = -(int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                }
            }
          #if (__VDC && __MP_COMP && __PRESS_EST)
           }
          #endif
        	if(WL->LFC_special_comp_duty==0)
        	{
            	if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
            	{
            		if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
            		{
                		WL->LFC_special_comp_duty = -(int16_t)S8_Comp_Duty_For_SPHside_F;
            		}
            	}
        	}

      #if   __VDC && __ABS_COMB
        }
      #endif
            WL->Check_double_brake_flag=0;
        }
        else if(WL->LFC_zyklus_z>=2) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
          #if (__VDC && __MP_COMP && __PRESS_EST)
            if((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1)) {
                if(WL->LFC_big_rise_and_dump_flag==1) {
					#if (__IDB_LOGIC==ENABLE)
					 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
                    if(S16_PRESS_RESOL_100_TO_10(lcabss16RefFinalTargetPres)>=(WL->MP_Init+MPRESS_20BAR))
					 #else
                    if(lcabss16RefFinalTargetPres>=(WL->MP_Init+MPRESS_20BAR)) 
					 #endif
					#else
					if(lcabss16RefMpress>=(WL->MP_Init+MPRESS_20BAR)) 
					#endif
					{	/* AHBGen3 MP Fluctuation */
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_BIG_RISE_DMP_COMP_DUTY;
                    }
                }
            }
            else {
          #endif
                if(WL->LFC_n_th_deep_slip_comp_flag==1) {
                    if((WL->LFC_zyklus_z==2) && (WL->LFC_initial_deep_slip_comp_flag==1)) {     /* '04 SW Winter test */
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_2ND_CYC_COMP_LOW;
                    }
                    else if(REAR_WHEEL_wl==0) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Jump_Down_Comp_F;
                    }
                    else {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Jump_Down_Comp_R;
                    }
                }
                else {
                    if(WL->LFC_big_rise_and_dump_flag==1) {
                        WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_BIG_RISE_DMP_COMP_DUTY;
                    }
                    else if(WL->LFC_zyklus_z<=(U8_IN_GEAR_detect_cycle+1)) {
                      #if __INGEAR_VIB  /**************************************************Front Wheel Drive */
                       #if __REORGANIZE_ABS_FOR_MGH60
                          #if (__REAR_D==ENABLE)
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==1)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(RL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(RR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                            else { }
                        }
                          #else
                        if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0))
                        {
                        	if(REAR_WHEEL_wl==0)
                        	{
                        		if(WL->In_Gear_flag==1)
                        		{
                                    WL->LFC_initial_deep_slip_comp_flag=1;
                                    WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                                }
                            }
                            else if(((LEFT_WHEEL_wl==1)&&(FL.In_Gear_flag==1)) || ((LEFT_WHEEL_wl==0)&&(FR.In_Gear_flag==1)))
                            {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                            else { }
                        }
                          #endif/*(__REAR_D==ENABLE)*/
                       #else
                        if(REAR_WHEEL_wl==0) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(WL->In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_F;
                            }
                        }
                        else if(LEFT_WHEEL_wl==1) {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FL.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                        else {
                            if((WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_n_th_deep_slip_pre_flag==0)&&(FR.In_Gear_flag==1)) {
                                WL->LFC_initial_deep_slip_comp_flag=1;
                                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty - (int16_t)U8_LFC_Initial_Duty_Comp_Low_R;
                            }
                        }
                       #endif/*#if __REORGANIZE_ABS_FOR_MGH60*/
                      #endif            /********************************************************************/
                    }
                    else { }
                }
          #if (__VDC && __MP_COMP && __PRESS_EST)
            }
          #endif

      #if   __VDC && __ABS_COMB
        }
      #endif
        }
        else { }
    }
    else { }

    if(LOW_to_HIGH_suspect_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Comp_F;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Comp_R;
            if((LOW_to_HIGH_suspect2_old==1)
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            	||(lcabsu1L2hSuspectByWhPres==1)
              #endif
              )
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
        }
    }
    else if(LOW_to_HIGH_suspect2_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
          #endif
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            if(lcabsu1L2hSuspectByWhPres==1)
            {
            	WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
          #endif
        }
    }
  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    else if(lcabsu1L2hSuspectByWhPres_K==1)
    {
        if(REAR_WHEEL_wl==0) {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)U8_LFC_L2H_Suspect_Comp_F;
            }
        }
        else
        {
            WL->LFC_special_comp_duty = WL->LFC_special_comp_duty + (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            if(LOW_to_HIGH_suspect2_old==1)
            {
                WL->LFC_special_comp_duty = WL->LFC_special_comp_duty  - (int16_t)(U8_LFC_L2H_Suspect_Comp_R + 5);
            }
        }
    }
  #endif
    else { }

  /***********************************************************************************************/
  /* Move from LCABS_vDctLFCMuChangeWL()                                                         */
  /***********************************************************************************************/
    if((REAR_WHEEL_wl==0) && (LFC_H_to_Split_flag==1) && (WL->Front_SL_dump_flag_K==1))
    {
        if(( U8_F_High_side_SL_dump>0 )&&(WL->lau1SPHighSideCompFlag==0))
        {
            if(S8_Comp_Duty_For_SPHside_F>=(int8_t)(U8_F_High_side_SL_dump*2))
            {
	            WL->LFC_special_comp_duty=WL->LFC_special_comp_duty - (int16_t)(U8_F_High_side_SL_dump*2);
	        }
            else
            {
                WL->LFC_special_comp_duty=WL->LFC_special_comp_duty - (int16_t)S8_Comp_Duty_For_SPHside_F;
            }
            WL->lau1SPHighSideCompFlag = 1;
        }
        else{}
    }
  /***********************************************************************************************/
}


/**
 * FUNCTION NAME: LAABS_vCompLFCMPCyclicCompDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description: NO valve MP/dP pressure compensation
 */
static void LAABS_vCompLFCMPCyclicCompDuty(struct W_STRUCT *WL_temp)
{
    int16_t  	s16CompDutyForMPVariation;
    int16_t 	s16CompDutyForDiffFromHMuSkid;
    int16_t		s16MasterPress, s16DPDutyCompRef_HighMuSkid;
    int16_t		s16DPDutyCompRef_MPVariation, s16DPDutyCompRef_PDiffFromSkid;

    WL=WL_temp;
	
	if(WL->lcabss16FadeOutCnt==1)
	{
		WL->MP_Cyclic_comp_duty = 0;
		WL->MP_Init_Ref = S16_ReferenceMCPress;
    }
	else{}

	#if (__IDB_LOGIC==ENABLE)
	 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		s16MasterPress = lcabss16RefFinalTargetPres/10;
	 #else
		s16MasterPress = lcabss16RefFinalTargetPres;
     #endif
	#else
	 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		s16MasterPress = lcabss16RefMpress/10;
	 #else
		s16MasterPress = lcabss16RefMpress;
	 #endif
	#endif

  	if(REAR_WHEEL_wl == 0)
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariation;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkid;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkid*MPRESS_1BAR;
  	}
  	else
  	{
  		s16DPDutyCompRef_MPVariation = (int16_t)U8_DPDutyCompRef_MPVariationR;
  		s16DPDutyCompRef_PDiffFromSkid = (int16_t)U8_DPDutyCompRef_PDiffFromSkidR;
  		s16DPDutyCompRef_HighMuSkid = (int16_t)U8_DPDutyCompRef_HighMuSkidR*MPRESS_1BAR;
  	}

	s16CompDutyForMPVariation =  ((s16MasterPress - WL->MP_Init_Ref)/s16DPDutyCompRef_MPVariation);
	s16CompDutyForDiffFromHMuSkid = ((s16DPDutyCompRef_HighMuSkid - WL->s16_Estimated_Active_Press)/s16DPDutyCompRef_PDiffFromSkid);

    if(WL->L_SLIP_CONTROL==0)
    {
		if((WL->LFC_built_reapply_flag==1) /*&& (ABS_DURING_MPRESS_FLG==1)*/)
        {
			if(WL->lcabss16FadeOutCnt==1)
            {
				tempW7 = 0;

				if(REAR_WHEEL_wl==0)
                {
					tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
					if((int16_t)U8_Initial_Duty_Comp_Low_F<(tempW7-3)) {tempW7 = tempW7-3;}
					else if((int16_t)U8_Initial_Duty_Comp_Low_F<tempW7) {tempW7 = (int16_t)U8_Initial_Duty_Comp_Low_F;}
					else {}
                }
                else
                {
					if((FL.s16_Estimated_Active_Press>=MPRESS_60BAR) && (FR.s16_Estimated_Active_Press>=MPRESS_60BAR))
                    {
                        tempW7 = s16CompDutyForMPVariation;
                    }
					else
					{
						tempW7 = s16CompDutyForMPVariation + s16CompDutyForDiffFromHMuSkid;
						if((int16_t)U8_Initial_Duty_Comp_Low_R<(tempW7-3)) {tempW7 = tempW7-3;}
						else if((int16_t)U8_Initial_Duty_Comp_Low_R<tempW7) {tempW7 = (int16_t)U8_Initial_Duty_Comp_Low_R;}
						else { }
					}
	            }

				WL->MP_Cyclic_comp_duty = WL->MP_Cyclic_comp_duty + tempW7;
				WL->MP_Cyclic_comp_duty = LCABS_s16LimitMinMax(WL->MP_Cyclic_comp_duty,-MAX_MP_COMP_DUTY,MAX_MP_COMP_DUTY);
				/*WL->MP_Init = s16MasterPress;*/
	        }
	        else { }
        }
    }
}

/**
 * FUNCTION NAME: LAABS_vLimitDiffofFrontLFCDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  NO valve duty limitation as for either side valve using special
 * compensation
 */
static void LAABS_vLimitDiffofFrontLFCDuty(void)
{
    if((ABS_fz==1)&&(vref>=VREF_6_KPH)) {
      #if   __VDC && __ABS_COMB
        if((FSF_fl==1) && (FL.LFC_ABS_Forced_Enter_flag==1) && (L_SLIP_CONTROL_fl==0))
      #else
        if((FSF_fl==1) && (FL.LFC_ABS_Forced_Enter_flag==1))
      #endif
        {
            if(FR.LFC_reapply_current_duty > 0)
            {
                FL.LFC_special_comp_duty = ((int16_t)(FR.LFC_reapply_current_duty)-(int16_t)(FL.LFC_initial_set_duty));
            }
            else if(FR.LFC_reapply_end_duty > 0)
            {
                FL.LFC_special_comp_duty = ((int16_t)(FR.LFC_reapply_end_duty)-(int16_t)(FL.LFC_initial_set_duty));
            }
            else
            {
                ;
            }
        }
      #if   __VDC && __ABS_COMB
        else if((FSF_fr==1) && (FR.LFC_ABS_Forced_Enter_flag==1) && (L_SLIP_CONTROL_fr==0))
      #else
        else if((FSF_fr==1) && (FR.LFC_ABS_Forced_Enter_flag==1))
      #endif
        {
            if(FL.LFC_reapply_current_duty>0)
            {
                FR.LFC_special_comp_duty = ((int16_t)(FL.LFC_reapply_current_duty)-(int16_t)(FR.LFC_initial_set_duty));
            }
            else if(FL.LFC_reapply_end_duty>0)
            {
                FR.LFC_special_comp_duty = ((int16_t)(FL.LFC_reapply_end_duty)-(int16_t)(FR.LFC_initial_set_duty));
            }
            else
            {
                ;
            }
        }
        else {
          #if   __VDC && __ABS_COMB
            if((L_SLIP_CONTROL_fl==0) && (L_SLIP_CONTROL_fr==0)) {
          #endif
            if(((FL.Check_double_brake_flag2==1)&&(FR.Check_double_brake_flag2==1)) || (LOW_to_HIGH_suspect==1))
            {
                LAABS_vSyncBothWheelDuty(&FL, &FR);
            }
          #if   __VDC && __ABS_COMB
            }
          #endif
        }
    }
    else {
        if((ABS_LIMIT_SPEED_fl==1) && (ABS_LIMIT_SPEED_fr==1)) {
            LAABS_vSyncBothWheelDuty(&FL, &FR);
        }
    }
}

/**
 * FUNCTION NAME:  LAABS_vSyncBothWheelDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:  Front NO valve duty synchronization using special compensation
 *
 * RETURN VALUE:
 * Description:
 */
static void LAABS_vSyncBothWheelDuty(struct W_STRUCT* pW1, struct W_STRUCT* pW2)
{
    if((pW1->LFC_built_reapply_flag==1)&&(pW2->LFC_built_reapply_flag==1)) {
        if(pW1->LFC_reapply_current_duty > (pW2->LFC_reapply_current_duty+1)) {
            pW1->LFC_special_comp_duty_sync-=((int16_t)(pW1->LFC_reapply_current_duty)-(int16_t)(pW2->LFC_reapply_current_duty)-1);
        }
        else if(pW2->LFC_reapply_current_duty > (pW1->LFC_reapply_current_duty+1)) {
            pW2->LFC_special_comp_duty_sync -=((int16_t)(pW2->LFC_reapply_current_duty)-(int16_t)(pW1->LFC_reapply_current_duty)-1);
        }
        else { }
    }
	else{}	
}

/**
 * FUNCTION NAME: LAABS_vCalcLFCInitialDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  NO valve initial duty setting(including compensation)
 */
static void LAABS_vCalcLFCInitialDuty(struct W_STRUCT* WLtmp)
{
	int8_t s8MaxOn = 20;
	int8_t s8MinOn = 3;

#if __VDC && __TCMF_CONTROL
    uint8_t  u8ActiveBrkLfcInitDuty = 0;
    int16_t  s16TcPressMin = (la_PtcMfc.MFC_Target_Press>la_StcMfc.MFC_Target_Press) ? la_StcMfc.MFC_Target_Press : la_PtcMfc.MFC_Target_Press;
    s16TcPressMin = s16TcPressMin*MPRESS_1BAR;
#endif

	WL = WLtmp;

	if(WL->lcabss16TarDeltaWhlP > 0)
	{
		WL->LFC_initial_set_duty = LAABS_s16SetInitDutyFromDp();
	}
	else
	{
		if (WL->lcabss16TarDeltaWhlP == 0)
		{
			WL->LFC_initial_set_duty = 0;
		}
		else {}
		if(WL->LFC_initial_set_duty == 0)
		{
			/*if(WL->REAR_WHEEL==0){WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_F;}
			else 				 {WL->LFC_initial_set_duty = (int16_t)U8_Initial_Duty_Ratio_R;} */

		  #if   __VDC && __TCMF_CONTROL
			if((ACTIVE_BRAKE_FLG==1)/* && (BRAKE_BY_MPRESS_FLG==0)*/)/*ACTIVE_BRAKE_FLG has already BRAKE_BY_MPRESS_FLG condition*/
			{
				u8ActiveBrkLfcInitDuty = (uint8_t)LCABS_s16Interpolation2P(s16TcPressMin,MPRESS_20BAR,MPRESS_100BAR,(int16_t)U8_INIT_DUTY_ACTIVE_BRK_MIN, (int16_t)U8_INIT_DUTY_ACTIVE_BRK_MAX);
				if(WL->LFC_initial_set_duty > (int16_t)u8ActiveBrkLfcInitDuty)
				{
					WL->LFC_initial_set_duty = (int16_t)u8ActiveBrkLfcInitDuty;
				}
			}
		  #endif
		}
		else{/*keep the previous value*/}
	}
	#if (NO_VV_ONOFF_CTRL==ENABLE)
		WL->init_duty = WL->LFC_initial_set_duty /*+ WL->LFC_MP_Cyclic_comp_duty + (int16_t)(WL->LFC_special_comp_duty)*/;
	#else
		WL->init_duty = WL->LFC_initial_set_duty + WL->LFC_MP_Cyclic_comp_duty - (int16_t)(WL->LFC_pres_rise_delay_comp) + (int16_t)(WL->LFC_special_comp_duty);
	#endif
    

	  #if __ROUGH_ROAD_DUMP_IMPROVE
	if((lcabsu1TwoWlABSUneven==1) || (lcabsu1OneWlABSUneven==1))
	{
		if((WL->ABS==1) && (WL->STABIL==1) && (WL->LFC_STABIL_counter>=L_TIME_100MS) && (WL->rel_lam>=-LAM_3P))
		{
			WL->init_duty = WL->init_duty + 3;
		}
	}
	  #endif

	  #if (__SLIGHT_BRAKING_COMPENSATION)
	if(lcabsu1SlightBraking==1)
	{
		s8MaxOn = (int8_t)LCABS_s16Interpolation2P(mpress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS,100, 60);
	}
	else
	{
		s8MaxOn = 50;
	}
	  #else  /*(__SLIGHT_BRAKING_COMPENSATION)*/
	    #if (__ROUGH_COMP_IMPROVE==ENABLE)
	if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1))
	{
		s8MaxOn = 70;
	}
	else
	    #endif/*__ROUGH_COMP_IMPROVE*/
	{
		s8MaxOn = 50;
	}
	  #endif /*(__SLIGHT_BRAKING_COMPENSATION)*/

	/*WL->init_duty = LCABS_s16LimitMinMax(WL->init_duty, s8MinOn, s8MaxOn);*/ /* For MSL : Hold at Target deltaP limitation */
	WL->init_duty = LCABS_s16LimitMinMax(WL->init_duty, 0, s8MaxOn);
}

/**
 * FUNCTION NAME:  LAABS_s16SetInitialDuty
 * CALLED BY: LAABS_vCalcLFCInitialDuty
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  Setting initial duty from target dP
 */

#if(__DESIRED_BOOST_ONTIME_MAP == DISABLE)
static int16_t LAABS_s16SetInitDutyFromDp(void)
{
	int16_t	s16InitSetDuty = WL->LFC_initial_set_duty;
	int16_t s16TimePerTarWhlPGain = MPRESS_10BAR;
	int16_t s16TimePerTarWhlPOffset = 0;
	int16_t s16dPdiff = (lcabss16RefCircuitPress-WL->s16_Estimated_Active_Press);
	int16_t s16dPdiffComp = 0;
	int16_t s16CaliperFldCns = 0;

	/* need limitation */

	if((HV_VL_wl==0)&&(AV_VL_wl==0))
	{
		if(REAR_WHEEL_wl == 0)
		{
			if(lcabss16RefCircuitPress >= MPRESS_120BAR)
			{
				s16TimePerTarWhlPGain   = 80; /*percent*/
				s16TimePerTarWhlPOffset = -1;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_60BAR)
				{
					s16TimePerTarWhlPOffset = -1;
					s16dPdiffComp = (int16_t)((MPRESS_60BAR-s16dPdiff)/MPRESS_15BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}
			else if(lcabss16RefCircuitPress >= MPRESS_60BAR)
			{
				s16TimePerTarWhlPGain   = 120; /*percent*/
				s16TimePerTarWhlPOffset = -1;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_40BAR)
				{
					s16TimePerTarWhlPOffset = -4;
					s16dPdiffComp = (int16_t)((MPRESS_30BAR-s16dPdiff)/MPRESS_5BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}
			else
			{
				s16TimePerTarWhlPGain   = 130; /*percent*/
				s16TimePerTarWhlPOffset = -1;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_30BAR)
				{
					s16TimePerTarWhlPOffset = -4;
					s16dPdiffComp = (int16_t)((MPRESS_30BAR-s16dPdiff)/MPRESS_5BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}
			if(WL->s16_Estimated_Active_Press <= MPRESS_3BAR)
			{
				s16CaliperFldCns = 3 + s16TimePerTarWhlPOffset; /* caliper fluid consumption term. Discard offset influence*/
			}
			else
			{
				s16CaliperFldCns = 0; /* caliper fluid consumption term */
			}
		}
		else
		{
			if(lcabss16RefCircuitPress >= MPRESS_120BAR)
			{
				s16TimePerTarWhlPGain   = 50; /*percent*/
				s16TimePerTarWhlPOffset = -2;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_40BAR)
				{
					s16dPdiffComp = (int16_t)((MPRESS_40BAR-s16dPdiff)/MPRESS_10BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}
			else if(lcabss16RefCircuitPress >= MPRESS_60BAR)
			{
				s16TimePerTarWhlPGain   = 60; /*percent*/
				s16TimePerTarWhlPOffset = -2;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_30BAR)
				{
					s16TimePerTarWhlPOffset = -3;
					s16dPdiffComp = (int16_t)((MPRESS_20BAR-s16dPdiff)/MPRESS_5BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}
			else
			{
				s16TimePerTarWhlPGain   = 80; /*percent*/
				s16TimePerTarWhlPOffset = -2;  /*ms. Set this value from linear relation bet. TarWhlP-Time*/

				if(s16dPdiff <= MPRESS_10BAR)
				{
					s16TimePerTarWhlPOffset = -3;
					s16dPdiffComp = (int16_t)((MPRESS_10BAR-s16dPdiff)/MPRESS_5BAR); /* 1ms per 12bar*/
				}
				else
				{
					s16dPdiffComp = 0;
				}
			}

			if(WL->s16_Estimated_Active_Press <= MPRESS_3BAR)
			{
				s16CaliperFldCns = 3 + s16TimePerTarWhlPOffset; /* caliper fluid consumption term. Discard offset influence*/
			}
			else
			{
				s16CaliperFldCns = 0; /* caliper fluid consumption term */
			}

		}

		s16InitSetDuty = (int16_t)(((int32_t)WL->lcabss16TarDeltaWhlP*s16TimePerTarWhlPGain)/MPRESS_100BAR) + s16CaliperFldCns + s16dPdiffComp - s16TimePerTarWhlPOffset; /* default: 1bar - 1ms matching */

		/*
		if((Rough_road_detect_vehicle==1)&&(YAW_CDC_WORK==0))
		{
	        if(REAR_WHEEL_wl==0)
	        {
	        	s16InitSetDuty = (int16_t)(s16InitSetDuty + U8_Initial_Duty_Rough_Comp_F);
	        }
	        else
	        {
	        	s16InitSetDuty = (int16_t)(s16InitSetDuty + U8_Initial_Duty_Rough_Comp_R);
	        }
		}
	    else if(Rough_road_suspect_vehicle==1)
	    {
	        if(REAR_WHEEL_wl==0) {
	        	s16InitSetDuty = (int16_t)(s16InitSetDuty + U8_Init_Duty_Rough_Sus_Comp_F);
	        }
	        else
	        {
	        	s16InitSetDuty = (int16_t)(s16InitSetDuty + U8_Init_Duty_Rough_Sus_Comp_R);
	        }
	    }
	    else{}
	    */
	}

	s16InitSetDuty = LCABS_s16LimitMinMax(s16InitSetDuty,3,30);


	return s16InitSetDuty;
}

#else

static int16_t LAABS_s16SetInitDutyFromDp(void)
{
	int16_t	s16InitSetDuty = WL->LFC_initial_set_duty;

	int16_t s16dPdiff = 0;
	int16_t s16dPdiffComp = 0;
	int16_t s16CaliperFldCns = 0;

	if((HV_VL_wl==0)&&(AV_VL_wl==0))
	{
		if(REAR_WHEEL_wl == 0)
		{
			s16dPdiff = (lcabss16RefCircuitPress - lcabss16EstFrontWhlSkidPress);

			s16InitSetDuty = (int16_t)L_s16IInter6Point(WL->lcabss16TarDeltaWhlP, MPRESS_1BAR, 3,
																						  MPRESS_3BAR, 5,
																						  MPRESS_5BAR, 6,
																						  MPRESS_7BAR, 9,
																						  MPRESS_9BAR, 13,
																						  MPRESS_15BAR,16);

			if(s16dPdiff > MPRESS_80BAR)
			{
				s16dPdiffComp = -1;
			}
			else if(s16dPdiff > MPRESS_40BAR)
			{
				s16dPdiffComp = 0;
			}
			else
			{
				if(lcabsu1FastPressRecoveryBfAbs == 1)
				{
					s16dPdiffComp = 5; /**/
				}
				else
				{
					s16dPdiffComp = 1;
				}
			}
		}
		else
		{
			s16dPdiff = (lcabss16RefCircuitPress - lcabss16EstRearWhlSkidPress);

			s16InitSetDuty = (int16_t)L_s16IInter6Point(WL->lcabss16TarDeltaWhlP, MPRESS_1BAR, 3,
																						  MPRESS_2BAR, 4,
																						  MPRESS_5BAR, 5,
																						  MPRESS_7BAR, 6,
																						  MPRESS_12BAR,9,
																						  MPRESS_15BAR,12);
			if(WL->lcabss16TarDeltaWhlP == 0)
			{
				s16InitSetDuty = 0;
			}
			else {}

			if(s16dPdiff > MPRESS_80BAR)
			{
				s16dPdiffComp = -1;
			}
			else if(s16dPdiff > MPRESS_50BAR)
			{
				s16dPdiffComp = 0;
			}
			else
			{
				if(lcabsu1FastPressRecoveryBfAbs == 1)
				{
					s16dPdiffComp = 5;
				}
				else
				{
					s16dPdiffComp = 1;
				}
			}

		}
	}

	s16InitSetDuty = s16InitSetDuty + s16dPdiffComp;


	s16InitSetDuty = LCABS_s16LimitMinMax(s16InitSetDuty,0,30); /*For MSL : Hold at target delta P limit*/

/*	s16InitSetDuty = LCABS_s16LimitMinMax(s16InitSetDuty,3,30); */


	return s16InitSetDuty;
}

#endif

/**
 * FUNCTION NAME:  LAABS_vGenerateLFCDutyWL
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  Final actuation duty(pwm_duty_temp) decision
 */
static void LAABS_vGenerateLFCDutyWL(struct W_STRUCT* WL_temp)
{
	static int8_t s8MinConvenReapplyTim  = 0;  /* common : 3 and 0 for MSL */
    static int8_t s8MaxConvenReapplyTim  = 30; /* MGH-80 : 20ms, Gen3 : 30ms */

	WL = WL_temp;

  #if (__UR_DUTY_COMP_IMPROVE==ENABLE)
    int8_t  s8URCompDutyPerLFCCycle=15;
    int8_t  s8URCompDutyPerURCycle=0;
  #endif /*(__UR_DUTY_COMP_IMPROVE==ENABLE)*/

	if((WL->L_SLIP_CONTROL==0)&&(!BTC_fz)&&(!PBA_pulsedown))
	{
		if(WL->LFC_built_reapply_flag==0)
		{
			WL->LFC_s0_reapply_counter=0;
			if(HV_VL_wl==1)
			{
				if(AV_VL_wl==1) /* ************* Dump! ********* */
				{
					WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
					WL->pwm_duty_temp=PWM_duty_DUMP;
				}
				else 			/* ************* Hold!!! ******** */
				{
					WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_Conven_Reapply_Time, -(U8_BASE_CTRLTIME*2));
				  #if (__UNSTABLE_REAPPLY_ENABLE==ENABLE)
					if(WL->Forced_Hold_After_Unst_Reapply==1)
					{
					  #if (NO_VV_ONOFF_CTRL==DISABLE)
						WL->pwm_temp = WL->init_duty + S8_DPRISE_FORCED_HOLD_DUTY;
						WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
						if((WL->UNSTABLE_LowMu_comp_flag==1) || (WL->UNSTABLE_LowMu_comp_flag2==1))
						{
							WL->pwm_duty_temp = WL->pwm_duty_temp + 10;
						}
					      #if __VDC && __MP_COMP
						WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);
					      #endif /*#if __VDC && __MP_COMP*/
					  #else
						WL->pwm_temp = PWM_duty_HOLD;
						WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
					  #endif/*#if (NO_VV_ONOFF_CTRL==DISABLE)*/
					}
					else
				  #endif /*#if (__UNSTABLE_REAPPLY_ENABLE==ENABLE)*/
				  #if __STABLE_DUMPHOLD_ENABLE
					if(WL->lcabsu1HoldAfterStabDumpFlg==1)
					{
						WL->pwm_temp = PWM_duty_HOLD;
						WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
					}
					else
				  #endif/*#if __STABLE_DUMPHOLD_ENABLE*/
				  #if (__HOLD_LFC_DUTY_CONTROL == ENABLE)
					if((WL->GMA_SLIP==1)&&(GMA_PULSE==1)&&(WL->s0_reapply_counter >= 1))
					{
						WL->pwm_temp = U8_MIN_HOLD_DUTY;
						WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
					}
					else
				  #endif/*(__HOLD_LFC_DUTY_CONTROL == ENABLE)*/
					{
					  #if __EBD_LFC_MODE
						if(WL->EBD_LFC_RISE_MODE==1) {
							WL->pwm_temp=U8_EBD_LFC_INITIAL_DUTY - (int16_t)WL->s0_reapply_counter;
							if(WL->pwm_temp<=0) {WL->pwm_temp=(int16_t)pwm_duty_null;}
							WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
						}
						else {WL->pwm_duty_temp=PWM_duty_HOLD;}
					  #else
						WL->pwm_duty_temp=PWM_duty_HOLD;
					  #endif
					}
				}
			}
			else 				/* ************ Reapply !!! **********/
			{
			  #if __ENABLE_202_DUTY
				if(AV_VL_wl==0)
				{
			  #endif
				  if(WL->LFC_Conven_OnOff_Flag==1)
				  {
					if(REAR_WHEEL_wl==1)
					{
						if(ABS_fz==1)
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
						  #endif

						  #if (__SLIGHT_BRAKING_COMPENSATION)
							WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS,
																(WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_MP_R), WL->LFC_Conven_Reapply_Time);
							/* Further consideration for EBD */
						  #elif (__ROUGH_COMP_IMPROVE==ENABLE)
							if(lcebdu1RoughSlightBrk==1)
							{
								WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + 5;
							}
						  #endif/*__SLIGHT_BRAKING_COMPENSATION*/
						}
						else if(EBD_wl==1)
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R_EBD;
						  #endif
						}
						else
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #endif
						  #if __EBD_MI_ENABLE
							if(WL->EBDMICtrlMode>=MI_LEVEL1)
							{
								if(WL->LFC_Conven_Reapply_Time>8)
								{
									WL->LFC_Conven_Reapply_Time=8;
								}
							}
							else
						  #endif
							{WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;}
						}
					}
					else
					{
						#if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						#else
					   if(GMA_SLIP_wl==1)
					   {
							if(vref>=S16_GMA_H_SPEED)
							{
								if(WL->s0_reapply_counter <= 1)
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE1;
								}
								else if(WL->s0_reapply_counter <= 4)
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE2;
								}
								else
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_H_SPEED_PHASE3;
								}
							}
							else if(vref>=S16_GMA_L_SPEED)
							{
								if(WL->s0_reapply_counter <= 1)
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE1;
								}
								else if(WL->s0_reapply_counter <= 4)
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE2;
								}
								else
								{
									WL->LFC_Conven_Reapply_Time=S8_GMA_RiseTime_ML_SPEED_PHASE3;
								}
							}
							else
							{
								WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
							}

						  #if __HIGH_MPRESS_COMP
							if((WL->LFC_Conven_Reapply_Time > 4) && (lcabsu1ValidBrakeSensor==1))
							{
								if(lcabss16RefCircuitPress > MPRESS_150BAR)
								{
									tempB5 = (int8_t)((MPRESS_150BAR - lcabss16RefCircuitPress)/MPRESS_30BAR); /* 1ms/10bar */

								}
								else if(lcabss16RefCircuitPress < MPRESS_100BAR)
								{
									tempB5 = (int8_t)((MPRESS_100BAR - lcabss16RefCircuitPress)/MPRESS_30BAR); /* 1ms/10bar */
								}
								else
								{
									tempB5 = 0;
								}
								WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + tempB5;
							}
							  #if __EPB_INTERFACE
						   if(lcu1EpbActiveFlg==1)
						   {
							   WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P((-ebd_filt_grv),AFZ_0G2,AFZ_0G5,
															 (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_DECEL),WL->LFC_Conven_Reapply_Time);
						   }
							 #endif
						  #else/*#if __HIGH_MPRESS_COMP*/
							if(WL->s0_reapply_counter>12) {WL->LFC_Conven_Reapply_Time = WL->LFC_Conven_Reapply_Time + 1;}
						  #endif/*#if __HIGH_MPRESS_COMP*/
						}
						else
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
						  #endif
						}
						  #endif

					  #if (__SLIGHT_BRAKING_COMPENSATION)
						WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS,
															 (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_MP_F), WL->LFC_Conven_Reapply_Time);
					  #endif

					  #if __EPB_INTERFACE
						if(lcu1EpbActiveFlg==1)
						{
							WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16Interpolation2P((-ebd_filt_grv),AFZ_0G2,AFZ_0G5,
															 (WL->LFC_Conven_Reapply_Time+S8_ADDON_TIME_FOR_LOW_DECEL),WL->LFC_Conven_Reapply_Time);
						}
					  #endif
						WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_Conven_Reapply_Time,(int16_t)s8MinConvenReapplyTim,(int16_t)s8MaxConvenReapplyTim);
					}

					#if M_ON_OFF_BUILT_PATTERN==ENABLE
					if(WL->lcabss16TarDeltaWhlP==0)
					{
						WL->pwm_duty_temp = PWM_duty_HOLD; /*For TardP limitation*/
					}
					else {WL->pwm_duty_temp = pwm_duty_null;}

					#endif
				  }
				#if __EBD_LFC_MODE
				  else if(WL->EBD_LFC_RISE_MODE==1) {
						WL->LFC_Conven_Reapply_Time=0;
				  }
				#endif
				#if __UNSTABLE_REAPPLY_ENABLE
				  else if(WL->Reapply_Accel_flag == 1)
				  {
					#if (NO_VV_ONOFF_CTRL==ENABLE)
					  if((REAR_WHEEL_wl==0) || ((REAR_WHEEL_wl==1)&&(WL->lcabsu1TarDeltaPLimitFlg==0))) /*for tardP limitation*/
					  {
						  WL->LFC_Conven_Reapply_Time=U8_DEFAULT_RISESCANTIME;
					  }
					#else
						WL->LFC_Conven_Reapply_Time=0;
					#endif
				  }
				#endif
				  else
				  {
					  if((REAR_WHEEL_wl==0) || ((REAR_WHEEL_wl==1)&&(WL->lcabsu1TarDeltaPLimitFlg==0))) /*For tardP limitation*/
					  {
						  WL->LFC_Conven_Reapply_Time=U8_DEFAULT_RISESCANTIME;
					  }
				  }

				#if (__UNSTABLE_REAPPLY_ENABLE == ENABLE)&&(NO_VV_ONOFF_CTRL == DISABLE)
				  if(WL->Reapply_Accel_flag == 1)
				  {
					if(WL->init_duty >= S8_DPRISE_COMP_DUTY) {
					  #if __UNSTABLE_REAPPLY_ADAPTATION
						if((WL->UNSTABLE_LowMu_comp_flag==0) && (WL->Unstable_Rise_Pres_Cycle_cnt>=1))
						{
						  #if (__UR_DUTY_COMP_IMPROVE==1)

							if(WL->LFC_zyklus_z<=1)
							{
								s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY-5;
							}
							else if(WL->LFC_zyklus_z<=3)
							{
								s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY-3;
							}
							else
							{
								s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY;

								if(((LFC_Split_flag==1)&&(REAR_WHEEL_wl==0)&&(((MSL_BASE_rl==1)&&(LEFT_WHEEL_wl==0)) || ((MSL_BASE_rr==1)&&(LEFT_WHEEL_wl==1))))
									|| ((lcabsu1CertainHMu==1)&&(REAR_WHEEL_wl==0)))
								{
									if(WL->Unst_Reapply_Scan_Ref>=L_TIME_30MS)
									{
										s8URCompDutyPerLFCCycle = S8_DPRISE_COMP_DUTY + 5;
									}
								}
							}

							s8URCompDutyPerLFCCycle = (s8URCompDutyPerLFCCycle<5)?5:s8URCompDutyPerLFCCycle;

							if(WL->Unstable_Rise_Pres_Cycle_cnt>=3)
							{
								s8URCompDutyPerURCycle = 5;
							}
							else if(WL->Unstable_Rise_Pres_Cycle_cnt>=2)
							{
								s8URCompDutyPerURCycle = 3;
							}
							else
							{
								s8URCompDutyPerURCycle = 0;
							}


							WL->pwm_duty_temp=(uint8_t)(WL->init_duty - s8URCompDutyPerLFCCycle + s8URCompDutyPerURCycle);

						  #else /* (__UR_DUTY_COMP_IMPROVE==1)*/

							if((WL->LFC_zyklus_z<=1) || (WL->Unstable_Rise_Pres_Cycle_cnt>=3))
							{
								WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY + 5);
							}
							else if(WL->Unstable_Rise_Pres_Cycle_cnt>=2)
							{
								WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY + 3);
							}
							else {WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);}
						  #endif /* (__UR_DUTY_COMP_IMPROVE==1)*/
						}
						else {WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);}
					  #else
						WL->pwm_duty_temp=(uint8_t)(WL->init_duty - S8_DPRISE_COMP_DUTY);
					  #endif
					}
					else {
						WL->pwm_duty_temp=(uint8_t)WL->init_duty;
					}

				  #if __DETECT_MU_FOR_UR
					if((WL->lcabsu1LMuForUR==1)||(WL->UNSTABLE_LowMu_comp_flag==1))
				  #else
					if(WL->UNSTABLE_LowMu_comp_flag==1)
				  #endif
					{
						WL->pwm_duty_temp = WL->pwm_duty_temp + 10;
						/*
						  #if __ABS_RBC_COOPERATION
						if(lcabss16RBCPressBfABS>0) {WL->pwm_duty_temp = WL->pwm_duty_temp - 5;}
						  #endif
						*/
						  #if (__SLIGHT_BRAKING_COMPENSATION)
						if(lcabsu1SlightBraking==1) {WL->pwm_duty_temp = WL->pwm_duty_temp - 5;}
						  #endif

					  #if __VDC && __MP_COMP
						if(WL->Unstable_Rise_MP_comp_duty < 0)
						{
							if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
							else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
						}
						else if(WL->Unstable_Rise_MP_comp_duty > 10)
						{
							WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty-10);
						}
						else { }
					  #endif
					}
					else if(WL->UNSTABLE_LowMu_comp_flag2==1)
					{
						WL->pwm_duty_temp = WL->pwm_duty_temp + 5;
					  #if __VDC && __MP_COMP
						if(WL->Unstable_Rise_MP_comp_duty < 0)
						{
							if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
							else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
						}
						else if(WL->Unstable_Rise_MP_comp_duty > 5)
						{
							WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty-5);
						}
						else { }
					  #endif
					}
					else
					{
					  #if __VDC && __MP_COMP
						if(((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty)<15) {WL->pwm_duty_temp=15;}
						else {WL->pwm_duty_temp = (uint8_t)((int16_t)WL->pwm_duty_temp+(int16_t)WL->Unstable_Rise_MP_comp_duty);}
					  #else
						;
					  #endif
					}

				  #if __ABS_RBC_COOPERATION
					if(lcabss16RBCPressBfABS>0) /*  && (REAR_WHEEL_wl==0) */
					{
						if(WL->UNSTABLE_LowMu_comp_flag==1)
						{
							WL->pwm_duty_temp = WL->pwm_duty_temp - 5;
						}
						else
						{
						  #if(__HEV_MP_MOVING_AVG==ENABLE)
							if(laabss16MPAvg<=MPRESS_50BAR)
						  #else
							if(lcabss16RefCircuitPress<=MPRESS_50BAR)
						  #endif
							{
								WL->pwm_duty_temp = pwm_duty_null;
							}
							else
							{
								WL->pwm_duty_temp = (WL->pwm_duty_temp > 40)? (WL->pwm_duty_temp - 40) : 0;
							}
						}
					}
				  #endif
				  }
				  else {
				#endif
					#if __EBD_LFC_MODE
					  if(WL->EBD_LFC_RISE_MODE==1) {
		/*              if(WL->pwm_duty_temp>0) WL->pwm_duty_temp=(uint8_t)70 - WL->s0_reapply_counter; */
		/*              else WL->pwm_duty_temp=pwm_duty_null; */
						WL->pwm_temp=U8_EBD_LFC_INITIAL_DUTY - (int16_t)WL->s0_reapply_counter;
						if(WL->pwm_temp<=0) {WL->pwm_temp=(int16_t)pwm_duty_null;}
						WL->pwm_duty_temp=(uint8_t)WL->pwm_temp;
					  }
					  else
					  {
						  if (WL->lcabss16TarDeltaWhlP==0)
						  {
							  WL->pwm_duty_temp = PWM_duty_HOLD;
						  } /*For tardP limitation*/
						  else
						  {
							  WL->pwm_duty_temp = pwm_duty_null;
						  }
					  }
					#else
					  WL->pwm_duty_temp = pwm_duty_null;
					#endif
				#if (__UNSTABLE_REAPPLY_ENABLE == ENABLE)&&(NO_VV_ONOFF_CTRL == DISABLE)
				  }
				#endif
			  #if __ENABLE_202_DUTY
				}
				else {        /* if (AV_VL_wl==1){ */
					WL->pwm_duty_temp = pwm_duty_null;
					#if __BDW
						if (BDW_ON==1){
						WL->pwm_duty_temp = PWM_duty_CIRC;
						WL->LFC_Conven_Reapply_Time=0;
					}
					#endif
					#if __EBP
						if (EBP_ON==1){
						WL->pwm_duty_temp = PWM_duty_CIRC;
						WL->LFC_Conven_Reapply_Time=0;
					}
					#endif
/*					#if __ENABLE_PREFILL
						if (EPC_ON==1){
						WL->pwm_duty_temp = PWM_duty_CIRC;
						WL->LFC_Conven_Reapply_Time=0;
					}
					#endif*/
				}
			  #endif    /* ~ __ENABLE_202_DUTY */
			}
		}
		else/*if(WL->LFC_built_reapply_flag==1)*//* Built Reapply !!! */
		{
			if(WL->LFC_Conven_OnOff_Flag==1)
			{
			  #if (NO_VV_ONOFF_CTRL == DISABLE)
				WL->LFC_s0_reapply_counter=0;
			  #endif

				if(HV_VL_wl==1)
				{
					if(AV_VL_wl==1)           /* ************* Dump! ********* */
					{
						WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */
						WL->pwm_duty_temp=PWM_duty_DUMP;
					}
					else                      /* ************* Hold!!! ******** */
					{
						if(WL->lcabsu1WhlPForcedHoldCmd==1)
						{
							WL->LFC_Conven_Reapply_Time = 0;
						}
						else
						{
							WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_Conven_Reapply_Time, -(U8_BASE_LOOPTIME));
						}
						WL->pwm_duty_temp=PWM_duty_HOLD;
					}
				}
				else                          /* ************ Reapply !!! ******/
				{
					if(REAR_WHEEL_wl==1)
					{  /* Reapply over 7ms (by CSH 2002 Winter) */
						if(ABS_fz==1)
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
							/*if((WL->init_duty-(int8_t)WL->LFC_s0_reapply_counter) > s8MinConvenReapplyTim)
							{
								WL->LFC_Conven_Reapply_Time = WL->init_duty - (int8_t)WL->LFC_s0_reapply_counter;
							}
							else
							{
								WL->LFC_Conven_Reapply_Time = s8MinConvenReapplyTim;
							}*/
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
						  #endif
						}
						else if(EBD_wl==1)
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R_EBD;
						  #endif

						}
						else
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_R;
						  #endif
						}
					}
					else
					{
						if(ABS_fz==1)
						{
					      #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
					      #else
						WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
					      #endif
						}
						else
						{
						  #if (NO_VV_ONOFF_CTRL==ENABLE)
							WL->LFC_Conven_Reapply_Time=(int8_t)WL->init_duty;
						  #else
							WL->LFC_Conven_Reapply_Time=S8_LFC_Conven_Reapply_Time_F;
						  #endif
						}
					}
					if(WL->lcabss16TarDeltaWhlP==0) /*For tardP limitation*/
					{
						WL->pwm_duty_temp = PWM_duty_HOLD;
					}
					else
					{
						WL->pwm_duty_temp = pwm_duty_null;
					}

					WL->LFC_Conven_Reapply_Time = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->LFC_Conven_Reapply_Time,(int16_t)s8MinConvenReapplyTim,(int16_t)s8MaxConvenReapplyTim);
					WL->LFC_Conven_Reapply_Time_Old = WL->LFC_Conven_Reapply_Time;
				}
			}
			else {                  /* Built Reapply !!! */
				WL->LFC_Conven_Reapply_Time=0;      /* Reapply over 7ms (by CSH 2002 Winter) */

	/* Changed By CSH 030624 */
			  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
				if(WL->LFC_s0_reapply_counter==1)
				{
					WL->pwm_temp = WL->init_duty;
				}
				else
				{
					WL->pwm_temp = WL->init_duty - (((int16_t)WL->LFC_s0_reapply_counter*2) - 2);
				}
			  #else

				if (lcabsu1InAbsAllWheelEntFadeOut == 1)
				{
					LAABS_vCompLFCMPCyclicCompDuty(WL);
					
					if (REAR_WHEEL_wl==0)
					{
						WL->pwm_temp = (U8_Initial_Duty_F - ((int16_t)WL->LFC_s0_reapply_counter)) + WL->MP_Cyclic_comp_duty + WL->LFC_special_comp_duty_sync;
					}
					else
					{
						WL->pwm_temp = (U8_Initial_Duty_R - ((int16_t)WL->LFC_s0_reapply_counter)) + WL->MP_Cyclic_comp_duty + WL->LFC_special_comp_duty_sync;
					}
				}
				else
					{
					WL->pwm_temp = PWM_duty_HOLD;
					}
			  #endif

				if(WL->pwm_temp<=0) {WL->LFC_reapply_current_duty = 0;}
				else {WL->LFC_reapply_current_duty = (uint8_t)WL->pwm_temp;}

				if(((WL->timer_keeping_LFC_duty >=(uint8_t)(WL->hold_timer_new_ref2+L_1SCAN))||(WL->Hold_Duty_Comp_flag==1)) /* '03 USA High Mu */
					&&(ABS_LIMIT_SPEED_wl==0))
				{
					#if (NO_VV_ONOFF_CTRL==ENABLE)
					 	WL->pwm_temp = PWM_duty_HOLD;
					#else					
					 if(REAR_WHEEL_wl==0) {WL->pwm_temp += (int16_t)U8_LFC_Long_Hold_Comp_Duty_F;}
					 else {WL->pwm_temp += (int16_t)U8_LFC_Long_Hold_Comp_Duty_R;}
					#endif

					 WL->MSL_long_hold_flag=1;
				}
				else {
					 WL->MSL_long_hold_flag=0;
				}
				if(WL->pwm_temp <= 0 ) {
					WL->pwm_temp=0;
				}
				else if(WL->pwm_temp >= (int16_t)PWM_duty_HOLD ) {
				   WL->pwm_temp=PWM_duty_HOLD;
				}
				else { }

				WL->pwm_duty_temp = (uint8_t)WL->pwm_temp;
			}
		}

/*--------------------------------------------------------------------------------------------
*                  Added by J.K.Lee at 03 SW_winter
*
* If ESP control ended as mode 1 or 2, Forcible Hold mode is added before ABS built apply
*
* mode 1: reapply, 2: built reapply, 3: Hold, 4: dump
*--------------------------------------------------------------------------------------------*/

#if __VDC   /* ESP to LFC assistance control */
    if(WL->LFC_to_ESP_end_flag==1){
        if(WL->ESP_end_counter_final < L_TIME_20MS){
            if(WL->esp_final_mode <= 2){
                WL->pwm_duty_temp = PWM_duty_HOLD;
                WL->ESP_to_LFC_hold_timer = 1;
            }
        }
    }
    else{
        if(WL->ESP_end_counter_final < L_TIME_20MS){
            if(WL->esp_final_mode == 1){
                if(WL->ESP_to_LFC_hold_timer > 0){
                    WL->pwm_duty_temp = PWM_duty_HOLD;
                }
            }
        }
        if(WL->ESP_to_LFC_hold_timer != 0) {WL->ESP_to_LFC_hold_timer = 0;}
    }
  #if ESC_PULSE_UP_RISE
    WL->pulseup_cycle_count = 0;
    WL->pulseup_hold_cycle_count = 0;
  #endif
#endif/*#if __VDC  ESP to LFC assistance control */

/*-------------------------------------------------------------------------------------------- */
	}

  #if ((__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE))
	if(WL->pwm_duty_temp < PWM_duty_HOLD)
	{
		WL->laabsu1Rise = 1;
	}
	else
	{
		WL->laabsu1Rise = 0;
	}
	if((WL->LFC_Conven_OnOff_Flag==1) && (WL->LFC_Conven_Reapply_Time>0))
	{
	    WL->laabsu1PulseupRise = 1;
	}
	else
	{
	    WL->laabsu1PulseupRise = 0;
	}
  #endif
}

/**
 * FUNCTION NAME: LAABS_vLimitDiffofRearLFCDuty
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description: Rear NO valve duty limitation as for either side wheel valve
 */
static void LAABS_vLimitDiffofRearLFCDuty(void)
{
    if((RL.LFC_reapply_end_duty==0)&&(RR.LFC_reapply_end_duty==0)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0)) {
        if((RL.LFC_Conven_OnOff_Flag==0)&&(RR.LFC_Conven_OnOff_Flag==0)) {

            if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_built_reapply_flag==1)) {

                tempC1 = 1;
              #if __LOWMU_REAR_1stCYCLE_RISE_DELAY
                if(lcabsu1Rear1stCycleRiseDelay==1)
                {
                	tempC1 = 5;
                	if(MSL_BASE_rl==1)
                	{
                		if(RL.LFC_s0_reapply_counter<=tempC1)
                		{
                            RR.LFC_reapply_current_duty = 0;
                            if(RR.LFC_built_reapply_counter>=L_2SCAN) {RR.LFC_built_reapply_counter = L_2SCAN;}
                            RR.LFC_s0_reapply_counter = 1;
                            RR.pwm_duty_temp=PWM_duty_HOLD;
                        }
                        else
                        {
                        	LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
                        }
                	}
                	if(MSL_BASE_rr==1)
                	{
                		if(RR.LFC_s0_reapply_counter<=tempC1)
                		{
                            RL.LFC_reapply_current_duty = 0;
                            if(RL.LFC_built_reapply_counter>=L_2SCAN) {RL.LFC_built_reapply_counter = L_2SCAN;}
                            RL.LFC_s0_reapply_counter = 1;
                            RL.pwm_duty_temp=PWM_duty_HOLD;
                        }
                        else
                        {
                        	LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
                        }
                	}
                }
                else
              #endif
                if(RR.LFC_reapply_current_duty > RL.LFC_reapply_current_duty) {
                    LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
                }
                else if(RL.LFC_reapply_current_duty > RR.LFC_reapply_current_duty) {
                    LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
                }
                else { }
            }
/*          else if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_zyklus_z<=0)) { */
            else if((RL.LFC_built_reapply_flag==1)&&(RR.LFC_zyklus_z<=0)&&(ABS_rr==1)&&(vref>=VREF_10_KPH)) {
                RL.LFC_reapply_current_duty = 0;
                if(RL.LFC_built_reapply_counter>=L_2SCAN) {RL.LFC_built_reapply_counter = L_2SCAN;}
                RL.LFC_s0_reapply_counter = 1;

              #if __UNSTABLE_REAPPLY_ENABLE
                if((RR.Reapply_Accel_flag==1) && (RR.pwm_duty_temp>0) && (RR.pwm_duty_temp<=PWM_duty_HOLD))
                {
                    RL.pwm_duty_temp = RR.pwm_duty_temp;
                }
                else
              #endif
                {
                    RL.pwm_duty_temp=PWM_duty_HOLD;
                }
            }

/*          else if((RR.LFC_built_reapply_flag==1)&&(RL.LFC_zyklus_z<=0)) { */
            else if((RR.LFC_built_reapply_flag==1)&&(RL.LFC_zyklus_z<=0)&&(ABS_rl==1)&&(vref>=VREF_10_KPH)) {
                RR.LFC_reapply_current_duty = 0;
                if(RR.LFC_built_reapply_counter>=L_2SCAN) {RR.LFC_built_reapply_counter = L_2SCAN;}
                RR.LFC_s0_reapply_counter = 1;

              #if __UNSTABLE_REAPPLY_ENABLE
                if((RL.Reapply_Accel_flag==1) && (RL.pwm_duty_temp>0) && (RL.pwm_duty_temp<=PWM_duty_HOLD))
                {
                    RR.pwm_duty_temp = RL.pwm_duty_temp;
                }
                else
              #endif
                {
                    RR.pwm_duty_temp=PWM_duty_HOLD;
                }
            }
            else { }
        }
    }
    else {

        if((RL.LFC_built_reapply_flag==1)&&(RL.LFC_Conven_OnOff_Flag==0)&&(MSL_BASE_rr==1)) {
            if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)
          #if __SELECT_LOW_FOR_LMU
            ||(lcabsu1SelectLowforNonSplit==1)
          #endif
            ){

                LAABS_vLimitDiffofRearAtSplit(&RL, &RR);
            }
            else {
                tempC1=1;
                LAABS_vLimitDiffofRearAtnSplit(&RL, &RR);
            }
        }

        if((RR.LFC_built_reapply_flag==1)&&(RR.LFC_Conven_OnOff_Flag==0)&&(MSL_BASE_rl==1)) {
            if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)
          #if __SELECT_LOW_FOR_LMU
            ||(lcabsu1SelectLowforNonSplit==1)
          #endif
            ){

                LAABS_vLimitDiffofRearAtSplit(&RR, &RL);
            }
            else {
                tempC1=1;
                LAABS_vLimitDiffofRearAtnSplit(&RR, &RL);
            }
        }

      #if __UNSTABLE_REAPPLY_ENABLE
        if((RL.LFC_built_reapply_flag==0) && (RL.SL_Unstable_Rise_flg==1))
        {
            RL.pwm_duty_temp = RR.pwm_duty_temp;
            RL.LFC_Conven_Reapply_Time = 0;
            RL.SL_Unstable_Rise_flg = 0;
        }
        else if((RR.LFC_built_reapply_flag==0) && (RR.SL_Unstable_Rise_flg==1))
        {
            RR.pwm_duty_temp = RL.pwm_duty_temp;
            RR.LFC_Conven_Reapply_Time = 0;
            RR.SL_Unstable_Rise_flg = 0;
        }
        else { }
      /*
        if((RL.LFC_built_reapply_flag==0)&&(RL.SPLIT_PULSE==1)&&(MSL_BASE_rr==1)&&(RL.state==DETECTION))
        {
            if(RR.Reapply_Accel_flag == 1)
            {
                RL.pwm_duty_temp = RR.pwm_duty_temp;
                RL.LFC_Conven_Reapply_Time = 0;
            }
        }
        else if((RR.LFC_built_reapply_flag==0)&&(RR.SPLIT_PULSE==1)&&(MSL_BASE_rl==1)&&(RR.state==DETECTION))
        {
            if(RL.Reapply_Accel_flag == 1)
            {
                RR.pwm_duty_temp = RL.pwm_duty_temp;
                RR.LFC_Conven_Reapply_Time = 0;
            }
        }
        else { }
      */
      #endif

    }
  #if __REORGANIZE_ABS_FOR_MGH60
    if((RL.ABS_LIMIT_SPEED==1) && (RR.ABS_LIMIT_SPEED==1))
    {
        LAABS_vSyncBothWheelDuty(&RL, &RR);
    }
  #endif
}

/**
 * FUNCTION NAME:  LAABS_vLimitDiffofRearAtSplit
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description: Rear NO valve duty limitation at split road surface
 */
static void LAABS_vLimitDiffofRearAtSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE)
{
    if(pBASE->LFC_built_reapply_flag==1){
	  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
		if(pBASE->LFC_s0_reapply_counter==1)
		{
			pNBASE->pwm_temp = pBASE->init_duty;
		}
		else
		{
			pNBASE->pwm_temp = pBASE->init_duty - (((int16_t)pBASE->LFC_s0_reapply_counter*2) - 2);
		}
	  #else
	  	pNBASE->pwm_temp = pBASE->pwm_temp;
	  	/*pNBASE->pwm_temp = (int16_t)pBASE->init_duty - (int16_t)(pBASE->LFC_s0_reapply_counter);*/
	  #endif  /*(__MGH80_LFC_PATTERN_CHANGE == ENABLE)*/
        if(pNBASE->pwm_temp<=0) {pNBASE->LFC_reapply_current_duty =0; pNBASE->pwm_temp = 0;}
        else {pNBASE->LFC_reapply_current_duty = (uint8_t)pNBASE->pwm_temp;}
        pNBASE->LFC_pres_rise_delay_comp = pBASE->LFC_pres_rise_delay_comp;
        pNBASE->LFC_special_comp_duty = pBASE->LFC_special_comp_duty;
      #if __VDC && __MP_COMP
        pNBASE->MP_Cyclic_comp_duty = pBASE->MP_Cyclic_comp_duty; /*2005.07.31 NZ winter */
      #endif
        pNBASE->LFC_s0_reapply_counter = pBASE->LFC_s0_reapply_counter;
        if((pBASE->LFC_built_reapply_counter>=L_2SCAN) || (pNBASE->LFC_built_reapply_counter<=0)) {
            pNBASE->LFC_built_reapply_counter = pBASE->LFC_built_reapply_counter;
        }
        else {pNBASE->LFC_built_reapply_counter = L_2SCAN;}
        if(pBASE->MSL_long_hold_flag==1) {
            pNBASE->pwm_temp = (int16_t)pNBASE->LFC_reapply_current_duty + (int16_t)U8_LFC_Long_Hold_Comp_Duty_R;
        }
        pNBASE->pwm_duty_temp = (uint8_t)pNBASE->pwm_temp;
    }
    else {
      #if !__MIRC_IMPROVE
        pNBASE->pwm_duty_temp = pBASE->pwm_duty_temp;
      #else
        if(pBASE->LFC_zyklus_z<1)
        {
        	pNBASE->pwm_duty_temp = pBASE->pwm_duty_temp;
        }
        else if(pNBASE->MSL_long_hold_flag==0)
        {
            pNBASE->pwm_duty_temp = pNBASE->pwm_duty_temp + U8_LFC_Long_Hold_Comp_Duty_R;
        }
        else{}
      #endif
    }
}

/**
 * FUNCTION NAME:  LAABS_vLimitDiffofRearAtnSplit
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  Rear NO valve duty limitation at non-split surface only for
 * special vehicles(ex. RWD vehicles)
 */
static void LAABS_vLimitDiffofRearAtnSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE)
{
    int16_t BASE_LFC_Rep_duty;

  #if (__MGH80_LFC_PATTERN_CHANGE == ENABLE)
  	tempC1 = tempC1*2;
  #endif

    if((pBASE->LFC_reapply_current_duty > pBASE->LFC_reapply_end_duty)&&(pBASE->LFC_reapply_end_duty>0)) {
        BASE_LFC_Rep_duty = (int16_t)(pBASE->LFC_reapply_end_duty - tempC1);
    }
    else if(pBASE->LFC_reapply_current_duty>tempC1) {
        BASE_LFC_Rep_duty = (int16_t)(pBASE->LFC_reapply_current_duty - tempC1);
    }
    else {BASE_LFC_Rep_duty = 0;}

    if(pNBASE->LFC_reapply_current_duty < (uint8_t)BASE_LFC_Rep_duty) {
        if(pNBASE->AV_HOLD==0){
            if(pNBASE->LFC_s0_reapply_counter==1){
                pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty-(int16_t)pNBASE->LFC_reapply_current_duty);
            }
            else if(pNBASE->LFC_s0_reapply_counter>1) {
                pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty-(int16_t)pNBASE->LFC_reapply_current_duty-1);
                pNBASE->LFC_s0_reapply_counter--;
                pNBASE->LFC_built_reapply_counter = pNBASE->LFC_built_reapply_counter - L_1SCAN;
                pNBASE->Duty_Limitation_flag=1;
            }
            else { }
        }
        else {
            pNBASE->LFC_special_comp_duty += (BASE_LFC_Rep_duty - (int16_t)pNBASE->LFC_reapply_current_duty);
            if(pNBASE->LFC_s0_reapply_counter>1) {pNBASE->Duty_Limitation_flag=1;}
        }

        pNBASE->LFC_reapply_current_duty = (uint8_t)BASE_LFC_Rep_duty;
        pNBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty;
        if(pNBASE->MSL_long_hold_flag==1) {
            pNBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty + (uint8_t)U8_LFC_Long_Hold_Comp_Duty_R;
        }
        if((pBASE->LFC_initial_deep_slip_comp_flag==1) && (pNBASE->LFC_initial_deep_slip_comp_flag==0)) {   /* '04 SW */
            pNBASE->LFC_initial_deep_slip_comp_flag=1;
        }

      #if __VDC && __MP_COMP
        if(pNBASE->LFC_zyklus_z<=1)
        {
        	if((pBASE->MP_Init - pNBASE->MP_Init) >= MPRESS_10BAR)
        	{
        		pNBASE->MP_Init = pBASE->MP_Init - MPRESS_10BAR;
        	}
        	else if((pBASE->MP_Init - pNBASE->MP_Init) <= -MPRESS_10BAR)
        	{
        		pNBASE->MP_Init = pBASE->MP_Init + MPRESS_10BAR;
        	}
        	else { }
        }
      #endif
    }

    if((pBASE->STABIL==0) && (pBASE->s_diff<=0)) {  /*if(pBASE->STABIL==0) */
        if(pNBASE->Duty_Limitation_flag==1) {pNBASE->SPLIT_PULSE = 1;}
    }
    else{
        if(pNBASE->SPLIT_PULSE == 1) {pNBASE->SPLIT_PULSE = 0;}
    }

  #if (__REORGANIZE_ABS_FOR_MGH60==0)
    if((pBASE->ABS_LIMIT_SPEED==1) && (pNBASE->ABS_LIMIT_SPEED==1)) {
        if((pBASE->LFC_built_reapply_flag==1) && (pNBASE->LFC_built_reapply_flag==1)) {
            if((pBASE->LFC_reapply_current_duty+1) < pNBASE->LFC_reapply_current_duty) {
                pNBASE->LFC_special_comp_duty -= (int16_t)(pNBASE->LFC_reapply_current_duty - pBASE->LFC_reapply_current_duty - 1);
                pNBASE->pwm_duty_temp = pBASE->LFC_reapply_current_duty;
            }
            else if((pNBASE->LFC_reapply_current_duty+1) < pBASE->LFC_reapply_current_duty) {
                pBASE->LFC_special_comp_duty -= (int16_t)(pBASE->LFC_reapply_current_duty - pNBASE->LFC_reapply_current_duty - 1);
                pBASE->pwm_duty_temp = pNBASE->LFC_reapply_current_duty;
            }
            else { }
        }
    }
  #endif
}

/**
 * FUNCTION NAME: LAABS_vInterfaceValveOutputs
 * CALLED BY:
 * Preconditions:
 * PARAMETER:
 * RETURN VALUE:
 * Description:  Final duty arrangement dividing 2 period(per 5ms)
 */
static void LAABS_vInterfaceValveOutputs(const struct W_STRUCT* pActWL, WHEEL_VV_OUTPUT_t * laabsHP1, WHEEL_VV_OUTPUT_t * laabsHP2)
{
    /* ------------------- Full Rise -------------------*/
    if(pActWL->LFC_Conven_Reapply_Time>0)
    {
        laabsHP1->u8OutletOnTime = 0;
        laabsHP2->u8OutletOnTime = 0;

        laabsHP1->u8PwmDuty = pwm_duty_null;
        laabsHP2->u8PwmDuty = PWM_duty_HOLD;


        if(pActWL->LFC_Conven_Reapply_Time<=U8_BASE_CTRLTIME)
        {
            laabsHP1->u8InletOnTime = (uint8_t)pActWL->LFC_Conven_Reapply_Time;
            laabsHP2->u8InletOnTime = 0;
        }
        else
        {
            laabsHP1->u8InletOnTime = U8_BASE_CTRLTIME;
            laabsHP2->u8InletOnTime = (uint8_t)pActWL->LFC_Conven_Reapply_Time - U8_BASE_CTRLTIME;
        }
    }
    else
    {
        laabsHP1->u8InletOnTime = 0;
        laabsHP2->u8InletOnTime = 0;

        if((pActWL->LFC_built_reapply_flag==1) || (pActWL->pwm_duty_temp_dash<PWM_duty_HOLD)) /* For redundancy */
        {
            laabsHP1->u8PwmDuty = laabsHP2->u8PwmDuty = pActWL->pwm_duty_temp_dash;

            laabsHP1->u8OutletOnTime = 0;
            laabsHP2->u8OutletOnTime = 0;
        }
        else
        {
	        /* ------------------- Dump -------------------*/

	        if(pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_01)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_HOLD;
	            laabsHP2->u8PwmDuty = PWM_duty_DUMP;

	            laabsHP1->u8OutletOnTime = 0;
	            laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	        }
	        else if (pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_10)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	            laabsHP2->u8PwmDuty = PWM_duty_HOLD;

	            if(pActWL->on_time_outlet_pwm<=U8_BASE_CTRLTIME)
	            {
	                laabsHP1->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	                laabsHP2->u8OutletOnTime = 0;
	            }
	            else
	            {
	                laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	                laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm - U8_BASE_CTRLTIME;
	            }
	        }
	        else if(pActWL->lcabss8DumpCase==U8_L_DUMP_CASE_11)
	        {
	            laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	            laabsHP2->u8PwmDuty = PWM_duty_DUMP;

	            laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	            laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	        }
	        else /* U8_L_DUMP_CASE_00 */
	        {
	            if(pActWL->on_time_outlet_pwm>0) /*Remaining dump scantime*/
	            {
	                if(pActWL->on_time_outlet_pwm<=U8_BASE_CTRLTIME)
	                {
	                    laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	                    laabsHP2->u8PwmDuty = PWM_duty_HOLD;

	                    laabsHP1->u8OutletOnTime = pActWL->on_time_outlet_pwm;
	                    laabsHP2->u8OutletOnTime = 0;
	                }
	                else
	                {
	                    laabsHP1->u8PwmDuty = PWM_duty_DUMP;
	                    laabsHP2->u8PwmDuty = PWM_duty_DUMP;

	                    laabsHP1->u8OutletOnTime = U8_BASE_CTRLTIME;
	                    laabsHP2->u8OutletOnTime = pActWL->on_time_outlet_pwm - U8_BASE_CTRLTIME;
	                }
	            }
	            else
	            {
	                laabsHP1->u8PwmDuty = laabsHP2->u8PwmDuty = pActWL->pwm_duty_temp_dash;

	                laabsHP1->u8OutletOnTime = 0;
	                laabsHP2->u8OutletOnTime = 0;
	            }
	        }
		}
    }

  #if defined (__ACTUATION_DEBUG)
	laabsHP1->u8debugger = 4;
	laabsHP2->u8debugger = 4;
  #endif /* defined (__ACTUATION_DEBUG) */
}
