#ifndef __LCHDCCALLCONTROL_H__
#define __LCHDCCALLCONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"
#include "hm_logic_var.h"

/*Global MACRO CONSTANT Definition******************************************************/
  #if __HDC
  
  #if (__CAR_MAKER==GM_KOREA) || defined(__dDCT_INTERFACE)
#define __TARGET_SPD_ADAPTATION     	        1   
  #else
#define __TARGET_SPD_ADAPTATION     	        0     
  #endif
  
#define     S8_HDC_STANDBY                      1
#define     S8_HDC_ACTIVATION                   2
#define     S8_HDC_CLOSING                      3
#define     S8_HDC_TERMINATION                  4

#define     U8_HDC_NEU_GEAR                     0
#define     U8_HDC_1ST_GEAR                     1
#define     U8_HDC_2ND_GEAR                     2
#define     U8_HDC_3RD_GEAR                     3
#define     U8_HDC_4TH_GEAR                     4
#define     U8_HDC_5TH_GEAR                     5
#define     U8_HDC_6TH_GEAR                     6
#define     U8_HDC_REV_GEAR                     7
#define     U8_HDC_PARK_GEAR                    10

#define     HDC_MANUAL_TM                       0
#define     HDC_AUTO_TM                         1

#define     HDC_ACT_AT_ACCEL_BRAKE              1
#define     HDC_DE_ACT_AT_ACCEL_BRAKE           2

///////////////////  vehicle setting /////////////////////

  #if (__CAR==KMC_HM)
#define __LOW_MODE_ENABLE_VEHICLE      	1
#define __ECS_ENABLE_VEHICLE      	    1
  #else
#define __LOW_MODE_ENABLE_VEHICLE      	0
#define __ECS_ENABLE_VEHICLE         	0
  #endif

  #if ((__CAR_MAKER==HMC_KMC)||(__CAR_MAKER==SSANGYONG))
#define __TURBINE_RPM_ENABLE            1
  #else
#define __TURBINE_RPM_ENABLE            0
  #endif
///////////////////  vehicle setting /////////////////////

#define     S16_HDC_BRAKE_ENTER_TIME            L_U8_TIME_10MSLOOP_50MS
#define     S16_HDC_BRAKE_EXIT_TIME             L_U8_TIME_10MSLOOP_1000MS

#define 	U8_HDC_MT_STALL_SUSPECT_TIME		L_U8_TIME_10MSLOOP_70MS
#define 	U8_HDC_MT_STALL_SUS_MAINTAIN_T		L_U8_TIME_10MSLOOP_200MS
#define 	U8_HDC_AX_STABLE_TIME_ONHILL		L_U8_TIME_10MSLOOP_200MS
#define 	U8_HDC_MT_STALL_SUS_MAX_T       	L_U8_TIME_10MSLOOP_1000MS
#define 	S16_HDC_MAX_SPD_STALL_DETECT_MT 	VREF_10_KPH 

#define 	S16_HDC_ENOUGH_DECEL                10
#define		HDC_MAX_MP_OFFSET_BLS_FAIL          MPRESS_8BAR


/* tunning parameter */
/*
#define     U16_HDC_NO_PRE_CUR                  400
#define     S16_HDC_FADE_OUT_CTRL_DECEL         80
#define     S16_HDC_FADE_OUT_CTRL_DECEL2        150
#define     S16_HDC_ENT_DISC_TEMP               TMP_400
#define     S16_HDC_EXIT_DISC_TEMP              TMP_650
#define     U8_HDC_ACCEL_ENTER                  MTP_5_P
#define     U8_HDC_ACCEL_EXIT                   MTP_2_P

#define     U8_HDC_TARGET_SPD_ADAPT_ENABLE      1
#define     U8_HDC_TARGET_SPD_ADAPT_MODE        1

#define     U8_HDC_TAR_SPD_REF_LOW_ACC			5
#define     U8_HDC_TAR_SPD_REF_MED_ACC			30
#define     U8_HDC_TAR_SPD_REF_HIG_ACC			70
#define     S16_HDC_TAR_SPD_GAP_AT_LOW_ACC		VREF_0_5_KPH
#define     S16_HDC_TAR_SPD_GAP_AT_MED_ACC		VREF_0_75_KPH
#define     S16_HDC_TAR_SPD_GAP_AT_HIG_ACC		VREF_1_KPH
#define     U8_HDC_RAPID_STALL_RPM_RATIO     	8           
#define     S16_HDC_TS_RPMC_FAST_RPM_DW         300         
#define     S16_HDC_TS_RPMC_MID_RPM_DW     		100         
*/
/* tunning parameter */

#define     S16_ECS_LOW_MODE_G_COMP             -LONG_0G014G
#define     S16_ECS_HIGHWAY_MODE_G_COMP         -LONG_0G014G
#define     S16_ECS_OFFROAD_MODE_G_COMP         LONG_0G014G
#define     S16_ECS_HIGH_MODE_G_COMP            LONG_0G014G
#define     S16_ECS_NORMAL_MODE_G_COMP          LONG_0G


#define     LONG_0G                             0
#define     LONG_0G001G                         1
#define     LONG_0G002G                         2
#define     LONG_0G003G                         3
#define     LONG_0G005G                         5
#define     LONG_0G008G                         8
#define     LONG_0G01G                          10
#define     LONG_0G014G                         14
#define     LONG_0G015G                         15
#define     LONG_0G02G                          20
#define     LONG_0G025G                         25
#define     LONG_0G03G                          30
#define     LONG_0G035G                         35
#define     LONG_0G04G                          40
#define     LONG_0G05G                          50
#define     LONG_0G055G                         55
#define     LONG_0G06G                          60
#define     LONG_0G065G                         65
#define     LONG_0G07G                          70
#define     LONG_0G075G                         75
#define     LONG_0G08G                          80
#define     LONG_0G09G                          90
#define     LONG_0G1G                           100
#define     LONG_0G12G                          120
#define     LONG_0G15G                          150
#define     LONG_0G2G                           200
#define     LONG_0G25G                          250
#define     LONG_0G3G                           300
#define     LONG_0G35G                          350
#define     LONG_0G4G                           400
#define     LONG_0G45G                          450
#define     LONG_0G5G                           500
#define     LONG_0G55G                          550
#define     LONG_0G6G                           600
#define     LONG_0G65G                          650
#define     LONG_0G7G                           700
#define     LONG_0G75G                          750
#define     LONG_0G8G                           800
#define     LONG_0G85G                          850
#define     LONG_0G9G                           900
#define     LONG_0G95G                          950
#define     LONG_1G                             1000

  #endif

/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/
  #if __HDC

extern struct  U8_BIT_STRUCT_t HDC00;
extern struct  U8_BIT_STRUCT_t HDC01;
extern struct  U8_BIT_STRUCT_t HDC02;
extern struct  U8_BIT_STRUCT_t HDC03;
extern struct  U8_BIT_STRUCT_t HDC04;
extern struct  U8_BIT_STRUCT_t HDC05;
extern struct  U8_BIT_STRUCT_t HDC06;
extern struct  U8_BIT_STRUCT_t HDC07;
extern struct  U8_BIT_STRUCT_t HDC08;
extern struct  U8_BIT_STRUCT_t HDC09;
extern struct  U8_BIT_STRUCT_t HDC10;

#define lcu1hdcVehicleForward           HDC00.bit0
#define lcu1hdcVehicleBackward          HDC00.bit1
#define lcu1hdcFlatByPress              HDC00.bit2
#define lcu1HdcLongGSuspectFlg          HDC00.bit3
#define lcu1HdcDrivelineLowmodeFlg      HDC00.bit4
#define lcu1hdcFlatPresSuspectFlg       HDC00.bit5
#define lcu1HdcEcsCompFailFlg           HDC00.bit6
#define lcu1HdcEcsCompFailSigFlg        HDC00.bit7

#define lcu1hdcSDownHillFlag            HDC01.bit0
#define lcu1hdcMDownHillFlag            HDC01.bit1
#define lcu1hdcBDownHillFlag            HDC01.bit2
#define lcu1hdcADownHillFlag            HDC01.bit3
#define lcu1hdcGFlatFlag                HDC01.bit4
#define lcu1HdcRightGearFlg             HDC01.bit5
#define lcu1hdcStopOnUpHillFlg          HDC01.bit6
#define lcu1hdcStopOnDownHillFlg        HDC01.bit7

#define lcu1hdcVehicleDirectionSusFlg   HDC02.bit0
#define lcu1HdcNoForcedHoldAtBrk        HDC02.bit1
#define lcu1HdcOverCreepSpeedFlg        HDC02.bit2
#define lcu1HdcDiscTempErrFlg           HDC02.bit3
#define lcu1HdcRightThetaGFlg           HDC02.bit4
#define lcu1HdcVehicleDirbySensorFlg    HDC02.bit5
#define lcu1HdcVehicleDirbySensorSusFlg HDC02.bit6
#define lcu1HdcMtTsCompByInGear         HDC02.bit7

#define lcu1hdcEcsEquipFlg              HDC03.bit0
#define lcu1hdcEcsCanMsgTimeout         HDC03.bit1
#define lcu1hdcVehicleForwardSus1Flg    HDC03.bit2
#define lcu1hdcVehicleBackwardSus1Flg   HDC03.bit3
#define lcu1hdcVehicleForwardSus2Flg    HDC03.bit4
#define lcu1hdcVehicleBackwardSus2Flg   HDC03.bit5
#define lcu1HdcDownhillAfeterStopFlg    HDC03.bit6
#define lcu1hdcUpHillBackwardSusFlg     HDC03.bit7

#define lcu1hdcVehicleForwardSensor     HDC04.bit0
#define lcu1hdcVehicleBackwardSensor    HDC04.bit1
#define lcu1hdcVehicleForwardTM         HDC04.bit2
#define lcu1hdcVehicleBackwardTM        HDC04.bit3
#define lcu1hdcVehicleForwardThetaG     HDC04.bit4
#define lcu1hdcVehicleBackwardThetaG    HDC04.bit5
#define lcu1HdcDownhillFlg              HDC04.bit6
#define lcu1HdcQuickDownhillFlg         HDC04.bit7

/* HDC.c Only   */
#define lcu1HdcReenterFlg               HDC05.bit0
#define lcu1HdcTargetSpeedUpFlg         HDC05.bit1
#define lcu1HdcTargetSpeedDownFlg       HDC05.bit2
#define lcu1HdcmtpSetFlg                HDC05.bit3
#define lcu1HdcmpresSetFlg              HDC05.bit4
#define lcu1hdcBackRollHillFlg       HDC05.bit5
#define lcu1hdcEngStallSuspect       HDC05.bit6
#define lcu1hdcVehicleDirectionOKFlg    HDC05.bit7

#define lcu1HdcSwitchFlg       		    HDC06.bit0
#define lcu1HdcActiveFlg     		    HDC06.bit1
#define lcu1HdcInhibitFlg       		HDC06.bit2
#define lcu1HdcDeadzoneFlg       		HDC06.bit3
#define lcu1HdcSwFlgByCan  		        HDC06.bit4
#define lcu1HdcSwFlgByCanInvalid   		HDC06.bit5
#define HDC_TM_TYPE             		HDC06.bit6
#define lcu1HdcMtTsCompByRPM       		HDC06.bit7

/* MT   */
#define lcu1hdcPreviousGearOkFlg        HDC07.bit0
#define lcu1hdcGearR_Switch             HDC07.bit1
#define lcu1hdcClutchSwitch             HDC07.bit2
#define lcu1hdcClutchSwitch_old         HDC07.bit3
#define lcs16HdcThetaG_N_OK_Flg         HDC07.bit4
#define lcu1HdcEngIdleTorqueOKFlg       HDC07.bit5
#define lcu1HdcEngTorq_N_OKFlg          HDC07.bit6
#define lcu1HdcMTAvailableSignalFlg     HDC07.bit7

#define lcu1hdcGear1stOkFlg             HDC08.bit0
#define lcu1hdcGear2ndOkFlg             HDC08.bit1
#define lcu1hdcGear3rdOkFlg             HDC08.bit2
#define lcu1hdcGear4thOkFlg             HDC08.bit3
#define lcu1hdcGear5thOkFlg             HDC08.bit4
#define lcu1hdcGear6thOkFlg             HDC08.bit5
#define lcu1hdcMTGearSusFlag            HDC08.bit6
#define lcu1HdcClutchIndependEngageFlg  HDC08.bit7

#define lcu1hdcEngEngageOkFlg           HDC09.bit0
#define lcu1hdcMTGearSusFlag2           HDC09.bit1
#define lcu1hdcVehicleForwardYaw        HDC09.bit2
#define lcu1hdcVehicleBackwardYaw       HDC09.bit3
#define lcu1HdcEngTorq_OKFlg            HDC09.bit4
#define lcu1hdcPowerEngageAtClutchSWon	HDC09.bit5
#define lcu1hdcPowerEngage				HDC09.bit6
#define lcu1hdcEngNoverload				HDC09.bit7

#define lcu1hdcEngStallSuspect2			HDC10.bit0
#define lcu1HdcMpressOn			        HDC10.bit1
#define hdc_flg10_2non_used			    HDC10.bit2
#define hdc_flg10_3non_used			    HDC10.bit3
#define hdc_flg10_4non_used			    HDC10.bit4
#define hdc_flg10_5non_used			    HDC10.bit5
#define hdc_flg10_6non_used			    HDC10.bit6
#define hdc_flg10_7non_used			    HDC10.bit7
/********** From */
  #if defined(__CAN_SW)
extern uint8_t  GS_338h[8], TCU1[8];
extern uint16_t   HDC_CAN_SWITCH;
extern int16_t    HDC_Target_Speed_Input;
  #endif
   #if (__HDC_SW_TYPE==ANALOG_TYPE)
extern struct   HDC_SW_FLG_t wpHdcFlg;
   #endif
/********** To */

 

/* 임시 변수 모아 놓기  */
extern int16_t lcs16HdcEngIdleTorque;
extern int16_t lcs16HdcEngIdleTorque_cnt;
extern int16_t lcs16hdcEngEngageOkCnt;
extern uint8_t lcu8HdcThetaGDisableCnt,lcu8HdcSpeedCnt;
extern int16_t lcs16HdcThetaG_F_cnt,lcs16HdcThetaG_F_diff,lcs16HdcThetaG_F_old,lcs16HdcThetaG_F,lcs16HdcAXg_filter_old,lcs16HdcAXg_filter,lcs16HdcThetaG_F_diff_lpf;
extern int16_t lcs16HdcThetaG_B_cnt,lcs16HdcThetaG_B_diff,lcs16HdcThetaG_B_old,lcs16HdcThetaG_B,lcs16HdcWhg_filter_old,lcs16HdcWhg_filter,lcs16HdcThetaG_B_diff_lpf;
extern int16_t lcs16HdcWhg_filter_diff,lcs16HdcWhg_filter_diff_old;
extern int16_t lcs16HdcAXg_filter_diff,lcs16HdcAXg_filter_diff_old;
extern int16_t lcs16HdcThetaG_N_cnt;
extern uint8_t   lcu8HdcWhgSusCnt, lcu8HdcDriverAccelIntend, lcu8HdcDriverBrakeIntend;
extern int16_t     lcs16HdcWhgSusmin,lcs16HdcWhgSusmax;
extern uint8_t	 lcu8HdcEngEngageCnt1,lcu8HdcEngEngageCnt2,lcu8hdcASenSTBCnt,lcu8HdcDecelOKtimer,lcu8HdcEngTorq_N_cnt,lcu8HdcGearPositionOld;
extern int16_t     lcs16HdcEngrpmDotPeakN,lcs16HdcEngrpmDot,lcs16HdcEstEngrpmDot;
extern int16_t     lcs16HdcWhgSusmin_3,lcs16HdcWhgSusmax_3;
extern int16_t     lcs16HdcWhgSusmin_2,lcs16HdcWhgSusmax_2;
extern int16_t     lcs16HdcWhgSusmin_1,lcs16HdcWhgSusmax_1;
extern int16_t     lcs16HdcWhgSusmin_0,lcs16HdcWhgSusmax_0, lcs16HdcDecel_a, lcs16HdcDecel_d;
extern int16_t    lcs16HdcWhgSusTimer;
extern uint8_t  lcu8HdcMFCCtrlStep;
extern int16_t    lcs16hdcLongOffsetAvrlf;
extern int16_t    lcs16hdcLowSpeedCnt,lcs16HdcThetaG_org,lcs16HdcDecel1;
extern int16_t    lcs16HdcThetaG1,lcs16HdcThetaG0;
extern uint8_t  lcu8HdcThetaGMode1Cnt,lcu8HdcThetaGMode2Cnt;
extern uint8_t  lcu8hdcVehicleDirction,lcu8HdcDownHillbyAxAsenCnt;

/* ~~~임시 변수 모아 놓기   */
extern int16_t    ls16HdcDiffTurbineRPMlf,lcs16HdcYawFactorTh,lcs16HdcYawDotFactorTh;
extern uint8_t  lcu8HdcBumpSuspect_cnt,lcu8HdcBumpSuspect;

extern uint8_t  lcu8HdcBumpEstTime,lcu8HdcBumpDelayActTime;

extern int8_t   lcs8HdcState,lcs8HdcSubState;
extern uint8_t  lcu8HdcGearPosition;
extern uint8_t  lcu8HdcSmaThetaGCnt, lcu8HdcSmaThetaGEstTime;
extern uint8_t  lcu8HdcSmaThetaGNCnt;
extern uint8_t  lcu8HdcMidThetaGCnt, lcu8HdcMidThetaGEstTime;
extern uint8_t  lcu8HdcBigThetaGCnt, lcu8HdcBigThetaGEstTime;
extern uint8_t  lcu8HdcBumpGCnt, lcu8HdcFlatPreCnt, lcu8HdcFlatGCnt;
extern uint16_t   lcu16HdcObserver, lcu16HdcTimeToAct;
extern int16_t    lcs16HdcDecel, lcs16HdcASen;
extern int16_t    lcs16HdcThetaG, lcs16HdcThetaGSteer, lcs16HdcThetaGPitch;
extern int16_t    lcs16HdcSmaThetaGTh, lcs16HdcMidThetaGTh, lcs16HdcBigThetaGTh, lcs16HdcExThetaGTh;
extern int16_t    lcs16HdcTargetSpeed, lcs16HdcTargetG;
extern int16_t    lcs16HdcCtrlError, lcs16HdcCtrlErrorDot, lcs16HdcCtrlInput;
extern uint16_t   lcu16HdcClosingCnt;

extern int16_t    lcs16HdcAvr_ThetaG;
extern int16_t    turbine_rpm;

extern int16_t    lcs16HdcMaxDiscTemp;
extern int16_t    lcs16hdcFlatPressTH;
extern uint8_t  lcu8HdcDetmpt;


extern uint8_t  lcu8ecsWLamp;
extern uint8_t  lcu8ecsDef;
extern uint8_t  lcu8ecsDiag;
extern uint8_t  lcu8ecsLevelChangeNA;
extern uint8_t  lcu8ecsLifting;
extern uint8_t  lcu8ecsLowering;
extern uint8_t  lcu8ecsREQHeight;
extern uint8_t  lcu8ecsREQLevel;
extern uint8_t  lcu8ecsACTHeight;
extern int16_t    ls16ecsHeighRL;
extern int16_t    ls16ecsHeighRR;

extern int16_t    lcs16HdcEcsLongGCompHeight;
extern int16_t    ls16ecsHeighAverage,ls16ecsHeighAverage_old;
extern uint8_t  lcu8HdcEcsCompFail,lcu8HdcEcsSuspect_cnt,lcu8HdcEcsCompEnable;
extern int16_t    lcs16HdcEcsLongGComp;



extern uint8_t  lcu8HdcEcsCompEnable;
extern int16_t    lcs16HdcEcsModeGComp;

extern int16_t    lcs16HdcStandbyFlg_on_cnt, lcs16HdcStandbyFlg_off_cnt;
extern uint8_t  lcu8HdcStandbyFlg_K,lcu8HdcStandbyFlg_A;

extern int16_t    lcs16HdcActiveFlg_on_cnt, lcs16HdcActiveFlg_off_cnt;
extern uint8_t  lcu8HdcActiveFlg_K,lcu8HdcActiveFlg_A;

extern int16_t    lcs16HdcClosingFlg_on_cnt, lcs16HdcClosingFlg_off_cnt;
extern uint8_t  lcu8HdcClosingFlg_K,lcu8HdcClosingFlg_A;

extern int16_t    lcs16HdcTerminationFlg_on_cnt, lcs16HdcTerminationFlg_off_cnt;
extern uint8_t  lcu8HdcTerminationFlg_K,lcu8HdcTerminationFlg_A;

extern int16_t     lcs16HdcSwitchFlg_on_cnt, lcs16HdcSwitchFlg_off_cnt;
extern uint8_t   lcu8HdcStandbyFlg_K,lcu8HdcStandbyFlg_A;

extern int16_t    HDC_T_DATA9,HDC_T_DATA8,HDC_T_DATA7;

extern int16_t     lcs16_hdc_tempW0,lcs16_hdc_tempW1,lcs16_hdc_tempW2,lcs16_hdc_tempW3,lcs16_hdc_tempW4;
extern int16_t     lcs16_hdc_tempW5,lcs16_hdc_tempW6,lcs16_hdc_tempW7,lcs16_hdc_tempW8,lcs16_hdc_tempW9;

 

  #endif

/*Global Extern Functions  Declaration******************************************************/
  #if __HDC
extern void LCHDC_vCallControl(void);
  #endif
#endif
