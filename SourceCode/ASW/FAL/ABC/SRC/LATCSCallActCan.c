		/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LATCSCallActCan
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
#include "LVarHead.h"
#include "LCTCSCallControl.h"
#include "LCRDCMCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LDROPCallDetection.h"
#include "LCESPCallEngineControl.h"
#include "LCHDCCallControl.h"
#include "LCHSACallControl.h"
#include "LCEPBCallControl.h"
#include "LCAVHCallControl.h"
#include "LDABSCallEstVehDecel.H"
#if __SCC
#include "../SRC/SCC/LCSCCCallControl.h"
#endif
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSESPCallSensorSignal.h"
#include "LDBDWCallDetection.h"
#include "LCTODCallControl.h"
#include "LCACCCallControl.h"
#include "LCETSCCallControl.h"
#include "LSENGCallSensorSignal.h"
#include "LDESPEstDiscTemp.h"
#include "hm_logic_var.h"
#include "LAABSCallActCan.h"

/* MBD ETCS */
#include "ETCSGenCode/LCETCS_vCallMain.h"

int16_t latcss16CalTorqLowLimit;

#if(__CAR==PSA_C3)
#define U8_ESC_LAMP_ON_STRATEGY      1	/* Comment [ GM ESC LAMP ON Strategy Enable/Disable ] */
#define S16_LAMP_Enter_ESP_DT_THR               100     	/* Comment [ Delta Enter Threshold Torque[cal torque&drive torque] at ESP Control ] */                                             
#define S16_LAMP_Enter_ETC_DT_THR               100     	/* Comment [ Delta Enter Threshold Torque[cal torque&drive torque] at ETCS Control ] */
#define S16_LAMP_Enter_TSP_THR                    0     	/* Comment [ Enter Theshold at TSP Control ] */
#define S16_LAMP_FS_Slip_THR                    900     	/* Comment [ -Delta Enter Slip Threshold[Front] ] */
#define S16_LAMP_RS_Slip_THR                    900     	/* Comment [ -Delta Enter Slip Threshold[Rear] ] */
#define S8_LAMP_Enter_ESP_THR                    70     	/* Comment [ Drive Torque Enter Threshold at ESP Control ] */
#define S8_LAMP_Enter_ETC_THR                    70     	/* Comment [ Drive Torque Enter Threshold at ETC Control ] */
#define U16_LAMP_Enter_ESC_OFF_DT_THR          1000     	/* Comment [ Delta Enter Threshold Torque[cal torque&drive torque] at ESP Control Switch Off ] */
#define U16_LAMP_FS_Slip_ESC_OFF_THR           1300     	/* Comment [ -Delta Enter Slip Threshold[Front] at ESP Control Switch Off ] */
#define	K_VDC_Min_Activation_Time_1              45
#define	K_VDC_Min_Activation_Time_2             107
#define	K_VDC_Min_Activation_Time_3               1
#endif

#if ((GMLAN_ENABLE==ENABLE) || (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_CTS_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_J300) ||(__CAR == GM_MALIBU) ||(__CAR == GM_LAMBDA)||(__CAR==GM_SILVERADO)||(__CAR==GM_T300))
#define TDC_SPEED_THRESHOLD		160		// Torque decay control more than this speed. 20kph
#define TDC_MTP_THRESHOLD		40		// Torque decay control more than this mtp. 40%
#define TDC_MTP_SLOP_THR1		-4		// Torque decay control less than this mtp slop. -4% within 4scan
#define TDC_MTP_SLOP_THR2		2		// Torque decay control exit more than this mtp slop. 2% within 4scan
#define TDC_DEFALUT_GRDNT		11		// Torque decay control gradient. 11 for 0.16Nm/ms (80Nm per 500msec) (255 max)
#define TDC_MAX_CTRL_SCAN		71		// Torque decay control scantime maximum. 500msec
#define TRANSMISSION_NO_ACTION	0
#define	TRANSMISSION_MAX_GEAR_REQUEST	1
#define TRANSMISSION_MIN_GEAR_REQUEST 2
#define	TRANSMISSION_HOLD_GEAR_REQUEST	3
#define S8_Common_Temp5 125  /* 2011.07.12 Test code */
int8_t	ltcss8TorqDecayMtpDrop;
int8_t	ltcss8TorqDecayMtp[4];
uint8_t	ltcsu8TorqDecayCtrl;
uint8_t	ltcsu8TorqDecayCtrlScan;
uint16_t	ltcsu16TorqDecayCtrlGrdnt;
void LATCS_vCallTorqDecayCtrl(void);
#endif

	#if(NEW_FM_ENABLE==ENABLE)  

    uchar8_t lu1DrvModeInv;
    uchar8_t lu1eng_torqInv;
    uchar8_t lu1TMModeIdInv;
    uchar8_t lu1EMS_ENG_CHRInv;
    uchar8_t lu1EMS_ENG_VOLInv;
    uchar8_t lu1max_torqInv;
    uchar8_t lu1TCU_F_TCUInv;
    uchar8_t lu1gs_enaInv;
    uchar8_t lu1drive_torqInv;
    uchar8_t lu1gs_posInv;
    uchar8_t lu1DriveLineEnableInv;
    uchar8_t lu1fric_torqInv;
	
	#endif/*NEW_FM_ENABLE*/
	
#if __TCS||__ETC
void LATCS_vCallActCan(void);
void LATCS_vCoordinateCommand(void);
	#if __HMC_CAN_VER==CAN_1_3 ||__HMC_CAN_VER==CAN_1_4 ||__HMC_CAN_VER==CAN_1_6 ||__HMC_CAN_VER==CAN_2_0 || defined(SSANGYONG_CAN)
void LATCS_SetTCS4Message(void);
void LATCS_ResetTCS4Message(void);
	#endif
	#if !SIM_MATLAB
void LATCS_vConvertTCS2EMS(void);

	#if defined(SSANGYONG_CAN)
static void LATCS_vConvertTCS2EMSForSYC(void);
	#elif defined(HMC_CAN) || defined (YFE_CAN)	
static void LATCS_vConvertTCS2EMSForHMC(void);
	#elif __GM_VEHICLE
static void LATCS_vConvertTCS2EMSForGM(void);
	#elif (__DEMO_VEHICLE)
		#if (__CAR==FORD_C214)
static void LATCS_vConvertTCS2EMSForFORD(void);
		#elif __CAR==CHINA_B12
static void LATCS_vConvertTCS2EMSForB12(void);
		#elif __CAR==BYD_5A
static void LATCS_vConvertTCS2EMSFor5A(void);
		#elif (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU) || (__CAR==PSA_C3)	
static void LATCS_vConvertTCS2EMSForC4(void);		
		#elif (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU)
static void LATCS_vConvertTCS2EMSForCOMPASS(void);
		#elif __CAR==RSM_QM5 || __CAR==RSM_QM5_MECU
static void LATCS_vConvertTCS2EMSForQM5(void);			
		#elif (__CAR==BMW740i)
static void LATCS_vConvertTCS2EMSForBMW740i(void);		
		#elif (__CAR==VV_V40)
static void LATCS_vConvertTCS2EMSForV40(void);		
		#elif (__CAR==FIAT_PUNTO)
static void LATCS_vConvertTCS2EMSForPUNTO(void);		
		#endif
		
	#endif

static void LATCS_vConvertVafs2EMS(void);
	#endif /*!SIM_MATLAB*/
#endif /*__TCS||__ETC*/

#if ((GMLAN_ENABLE==ENABLE) || (__CAR==GM_C100)||(__CAR==GM_J300)||(__CAR == GM_MALIBU)||(__CAR == GM_LAMBDA)||(__CAR==GM_T300)) && __VDC
void LATCS_vCallTorqDecayCtrl(void)
{
	/* high-mu 노면 high ay 상황에서 drop throttle 시 torque를 decay 시켜주기 위한 제어 */
	/* 관련 신호 error 발생 시 제어 종료 */
	if((fu1MainCanLineErrDet==0)&&(fu1EMSTimeOutErrDet==0)
	   &&(fu8EngineModeStep!=0)&&(fu8EngineModeStep!=1)
	   &&(fu1AyErrorDet==0)&&(fu1OnDiag==0))
	{
		lespu1_TTDGCDcyCntrlEnbld = 1;          /*ltcsu8TorqDecayCtrl;		 Traction Torque Decay Gradient Control : Decay Control Enabled*/
		ltcsu16TorqDecayCtrlGrdnt = (uint16_t)TDC_DEFALUT_GRDNT;
		
		ltcss8TorqDecayMtp[3] = ltcss8TorqDecayMtp[2];
		ltcss8TorqDecayMtp[2] = ltcss8TorqDecayMtp[1];
		ltcss8TorqDecayMtp[1] = ltcss8TorqDecayMtp[0];
		ltcss8TorqDecayMtp[0] = (int8_t)mtp;				/* use 4 scan mtp slop */
		 
		tempW0 = (int16_t)(ltcss8TorqDecayMtp[0] - ltcss8TorqDecayMtp[3]); /* negative value, if mtp drop */
		
		ltcss8TorqDecayMtpDrop = 0; /* drop throttle 정보 clear */
		if(vref>(int16_t)TDC_SPEED_THRESHOLD)	/* torque decay 제어 시작 속도 이상에서 */
		{
			if(tempW0 > TDC_MTP_SLOP_THR2)		/* 운전자 가속 시 제어 종료 */
			{
				ltcss8TorqDecayMtpDrop = 0;
			}
			else if((tempW0<(int16_t)TDC_MTP_SLOP_THR1)&&(mtp>(int16_t)TDC_MTP_THRESHOLD))
			{
				ltcss8TorqDecayMtpDrop = 1;			/* drop throttle 판단 */
			}
			else
			{
				ltcss8TorqDecayMtpDrop = 0;	/* drop throttle 정보 clear */
			}
		}
		else
		{
			ltcss8TorqDecayMtpDrop = 0;			
		}
		/* for setting the torque decay control at the time drop throttle detected */
		if(ltcsu8TorqDecayCtrl==0)
		{
			if((ltcss8TorqDecayMtpDrop==1)&&(det_alatm>(int16_t)S16_HM_EMS)) /* high-mu 선회 중 제어 시작 */
			{
				ltcsu8TorqDecayCtrl = 1;
				ltcsu8TorqDecayCtrlScan = TDC_MAX_CTRL_SCAN;	
			}
			else
			{
				ltcsu8TorqDecayCtrl = 0;
				ltcsu8TorqDecayCtrlScan = 0;
			}
		}
		else	/* torque decay 제어 중 */
		{
			if((vref<=(int16_t)TDC_SPEED_THRESHOLD)||(tempW0>TDC_MTP_SLOP_THR2)) // 저속 및 운전자 가속 시 제어 즉시 종료
			{
				ltcsu8TorqDecayCtrl = 0;
				ltcsu8TorqDecayCtrlScan = 0;				
			}
			else if(ltcsu8TorqDecayCtrlScan>0)	/* 0.5초간 torque dacay 제어 수행 */
			{
				ltcsu8TorqDecayCtrl = 1;
				ltcsu8TorqDecayCtrlScan=ltcsu8TorqDecayCtrlScan-1;
			}
			else
			{
				ltcsu8TorqDecayCtrl = 0;
				ltcsu8TorqDecayCtrlScan = 0;
			}
		}
	}
	else	/* error 상황에서 관련 변수 clear */
	{
		ltcss8TorqDecayMtpDrop = 0;
		ltcsu8TorqDecayCtrl = 0;
		lespu1_TTDGCDcyCntrlEnbld = 0;
		ltcsu16TorqDecayCtrlGrdnt = 1023; /* 15Nm/ms */
		ltcsu8TorqDecayCtrlScan = 0;
		ltcss8TorqDecayMtp[0]=ltcss8TorqDecayMtp[1]=ltcss8TorqDecayMtp[2]=ltcss8TorqDecayMtp[3]=0;
	}
}
#endif

#if __TCS_CONV_ABS_TORQ
void LATCS_vConvAbsToCanTorq(void);
//=============================================================================
//	Conversion of CAN signal resolution torque from absolute torque
//=============================================================================
void LATCS_vConvAbsToCanTorq(void)
{
  #if (((__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_TAHOE)) || (GMLAN_ENABLE==ENABLE) || (__HMC_CAN_VER==CAN_HE_KE) ||(__CAR==GM_J300)||(__CAR == GM_MALIBU)||(__CAR == GM_LAMBDA)||(__CAR==GM_SILVERADO)||(__CAR==GM_T300))
	
  #elif __HMC_CAN_VER==CAN_1_2||__HMC_CAN_VER==CAN_1_3||__HMC_CAN_VER==CAN_1_4||__HMC_CAN_VER==CAN_1_6||__HMC_CAN_VER==CAN_2_0||__HMC_CAN_VER==CAN_HD_HEV||__HMC_CAN_VER==CAN_YF_HEV||__HMC_CAN_VER==CAN_EV||__HMC_CAN_VER==CAN_FCEV||__CAR==BYD_5A||__CAR==FIAT_PUNTO
    tempW0 = (int16_t)((int32_t)cal_torq*10/max_torq);
    tempW1 = (int16_t)((int32_t)drive_torq*10/max_torq);
    if(tempW0>1000)
    {
    	tempW0=1000;
    }
    else
    {
    	;
    }
    if(tempW1>1000)
    {
    	tempW1=1000;
    }
    else
    {
    	;
    }    
	lesps16TCS_TQI_TCS = tempW0;
	lesps16EMS_DRIVE_TQI = tempW1;
  #else

  #endif
}
#endif

#if __TCS||__ETC
void LATCS_vCallActCan(void)
{
	/* MBD ETCS */
	/*LATCS_vCoordinateCommand();*/
	#if !SIM_MATLAB
	LATCS_vConvertTCS2EMS();
	LATCS_vConvertVafs2EMS();
	#endif
}

	#if __HMC_CAN_VER==CAN_1_3 ||__HMC_CAN_VER==CAN_1_4 ||__HMC_CAN_VER==CAN_1_6 ||__HMC_CAN_VER==CAN_2_0 || defined(SSANGYONG_CAN)
void LATCS_SetTCS4Message(void)
{
#if !__TOD
  #if !defined(SSANGYONG_CAN)
	/* Error Handling */
	#if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
	if((lcu1TcsCtrlFail==1)||(lcu1TcsDiscTempErrFlg==1))
	#else
	if((abs_error_flg==1)||(ebd_error_flg==1)||(tcs_error_flg==1)
	||(tc_error_flg==1)||(vdc_error_flg==1)
	||(fu1VoltageUnderErrDet==1)||(fu1VoltageLowErrDet==1)||(fu1VoltageOverErrDet==1)) 
	#endif
	{
		lespu8TCS_4WD_OPEN = 3;    /* 4WD OPEN : ABS/EBD/ESP error 시 0x03h */
		lespu1TCS_4WD_LIM_REQ = 0; /* 4WD_LIM_REQ=0 */
		lespu1TCS_4WD_LIM_MODE = 0;/* 4WD_LIM_MODE=0 임시 Signal*/
	}
	else
	{
		lespu8TCS_4WD_OPEN = 0;		/*4WD_OPEN=00H : Clutch not influenced by ABS/TCS/ESP*/
		lespu1TCS_4WD_LIM_REQ = 0;	/*4WD_LIM_REQ=0 : Not request from ESP*/
		lespu1TCS_4WD_LIM_MODE = 0;

		lespu16TCS_4WD_TQC_LIM = 0xFAFF;/*4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP*/
		lespu8TCS_4WD_CLU_LIM = 0;		/*4WD_CLU_LIM=00H : Clutch duty not limited by ESP*/
	}

	if ((lespu8TCS_4WD_OPEN)!=3) 
	{/* Error가 발생하지 않은 경우 */
		if (ABS_fz == 1) 
		{
			LimitCardanShaftTorque = 64255; /* 4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP */
			lespu8TCS_4WD_OPEN = 1;			/* 4WD_OPEN=01H */
			lespu1TCS_4WD_LIM_REQ = 0;	    /*4WD_LIM_REQ=0 : Not request from ESP*/
			lespu1TCS_4WD_LIM_MODE = 0;
			lespu8TCS_4WD_CLU_LIM = 0;		/* 4WD_CLU_LIM=00H */
		}
		else if (ETO_SUSTAIN_1SEC==1)	/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
	  	{
			for(s8TODLoopCounter=0;s8TODLoopCounter<3;s8TODLoopCounter++)
			{
				s16TODLimitTorqTemp[s8TODLoopCounter]=LCESP_s16IInter3Point(det_alatm,LOW_MU_EMS,  s16TODLimitTorq[0][s8TODLoopCounter],
														   ((int16_t)((LOW_MU_EMS+HIGH_MU_EMS)/2)),s16TODLimitTorq[1][s8TODLoopCounter],
			                                                             HIGH_MU_EMS, s16TODLimitTorq[2][s8TODLoopCounter]);
			}

			s16SetTODSpeed[0]=LCESP_s16IInter3Point(det_alatm,LOW_MU_EMS, LOW_MU_ESP_TORQ_CON_L_SPEED,
		    	                      ((int16_t)((LOW_MU_EMS+HIGH_MU_EMS)/2)),MED_MU_ESP_TORQ_CON_L_SPEED,
			    	                                 HIGH_MU_EMS,HIGH_MU_ESP_TORQ_CON_L_SPEED);
			s16SetTODSpeed[1]=LCESP_s16IInter3Point(det_alatm,LOW_MU_EMS, LOW_MU_ESP_TORQ_CON_M_SPEED,
									  ((int16_t)((LOW_MU_EMS+HIGH_MU_EMS)/2)),MED_MU_ESP_TORQ_CON_M_SPEED,
			    	                                 HIGH_MU_EMS,HIGH_MU_ESP_TORQ_CON_M_SPEED);
			s16SetTODSpeed[2]=LCESP_s16IInter3Point(det_alatm,LOW_MU_EMS, LOW_MU_ESP_TORQ_CON_H_SPEED,
									  ((int16_t)((LOW_MU_EMS+HIGH_MU_EMS)/2)),MED_MU_ESP_TORQ_CON_H_SPEED,
			    	                                 HIGH_MU_EMS,HIGH_MU_ESP_TORQ_CON_H_SPEED);

			LimitCardanShaftTorque = (uint16_t)LCESP_s16IInter3Point(vrefk, s16SetTODSpeed[0], s16TODLimitTorqTemp[0],
				                                         s16SetTODSpeed[1], s16TODLimitTorqTemp[1],
		    		                                     s16SetTODSpeed[2], s16TODLimitTorqTemp[2]);
			LimitCardanShaftTorque = LimitCardanShaftTorque * 10;

			lespu8TCS_4WD_OPEN = 2; 	/* 4WD_OPEN=02H */
			lespu1TCS_4WD_LIM_REQ = 1;	/* 4WD_LIM_REQ=0 : Not request from ESP*/
		  #if __REAR_D
			/* Minimum Limiting Mode */
			#if !defined(__AUTOSAR_CORE_ENA)
                #if ( (__4WD==ENABLE) || (__4WD_VARIANT_CODE==ENABLE) )
                #endif
			lespu1TCS_4WD_LIM_MODE = 1;
			#endif
  		  #else
			/* Maximum Limiting Mode */
            #if ( (__4WD==ENABLE) || (__4WD_VARIANT_CODE==ENABLE) )
            #endif
			lespu1TCS_4WD_LIM_MODE = 0;
  		  #endif
			lespu8TCS_4WD_CLU_LIM = 0;	 /* 4WD_CLU_LIM=00H */
		}
		else 
		{/* ABS_fz==0 AND ESP_TCS_ON==0 */
			LimitCardanShaftTorque = 64255; /* 4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP */
			lespu8TCS_4WD_OPEN = 0;		    /* 4WD_OPEN=00H : Clutch not influenced by ABS/TCS/ESP */
			lespu1TCS_4WD_LIM_REQ = 0;	    /* 4WD_LIM_REQ=0 : Not request from ESP*/
			lespu1TCS_4WD_LIM_MODE = 0;
			lespu8TCS_4WD_CLU_LIM = 0;		/* 4WD_CLU_LIM=00H : Clutch duty not limited by ESP */
		}
		/* 4WD_TQC_LIM ↘*/
		lespu16TCS_4WD_TQC_LIM = LimitCardanShaftTorque;
		/* 4WD_TQC_LIM ↗*/
	}
	else  
	{/* Error가 발생한 경우 */
		/* Control function disabled due to error detection by ABS/TCS/ESP*/
		/* In this state, a vehicle should behave like a 2WD vehicle*/
		lespu16TCS_4WD_TQC_LIM = 0xFFFF;				/* 4WD_TQC_LIM = FFFFH(ERROR)*/
		lespu8TCS_4WD_CLU_LIM = 0xFF;					/* 4WD_CLU_LIM = FFH(ERROR)*/
	}
  #endif	
#else
	#if (NEW_FM_ENABLE == ENABLE)  /*NEW_FM_ENABLE*/
	if((lcu1TodCtrlFail==1)||(lcu1TcsDiscTempErrFlg==1))
	#else
	if((abs_error_flg==1)||(ebd_error_flg==1)||(tcs_error_flg==1)
	||(tc_error_flg==1)||(vdc_error_flg==1)
	||(fu1VoltageUnderErrDet==1)||(fu1VoltageLowErrDet==1)||(fu1VoltageOverErrDet==1)) 
	#endif
	{
		lespu8TCS_4WD_OPEN = 3;         /* 4WD OPEN : ABS/EBD/ESP error 시 0x03h */
		lespu1TCS_4WD_LIM_REQ = 0;      /* 4WD_LIM_REQ=0 */
		lespu1TCS_4WD_LIM_MODE = 0;     /* 4WD_LIM_MODE=0 임시 Signal*/
		lespu16TCS_4WD_TQC_LIM = 0xFFFF;/* 4WD_TQC_LIM = FFFFH(ERROR)*/
		lespu8TCS_4WD_CLU_LIM = 0xFF;	/* 4WD_CLU_LIM = FFH(ERROR)*/
	}
	else if(fu1ESCDisabledBySW==1)
	{
		if (laabsu1TrnsBrkSysCltchRelRqd == 1) 
		{
			lespu8TCS_4WD_OPEN = 1;			/* 4WD_OPEN=01H */
			lespu1TCS_4WD_LIM_REQ = 0;		/* 4WD_LIM_REQ=0 : Not request from ESP*/
			lespu1TCS_4WD_LIM_MODE = 0;
			lespu16TCS_4WD_TQC_LIM = 0x0000;/* 4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP*/
			lespu8TCS_4WD_CLU_LIM = 0;		/* 4WD_CLU_LIM=00H */
		}
		else 
		{
			lespu8TCS_4WD_OPEN = 0;			/* 4WD_OPEN=00H : Clutch not influenced by ABS/TCS/ESP*/
			lespu1TCS_4WD_LIM_REQ = 0;		/* 4WD_LIM_REQ=0 : Not request from ESP*/
			lespu1TCS_4WD_LIM_MODE = 0;
			lespu16TCS_4WD_TQC_LIM = 0xFAFF;/* 4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP*/
			lespu8TCS_4WD_CLU_LIM = 0;		/* 4WD_CLU_LIM=00H : Clutch duty not limited by ESP*/
		}
	}
	else
	{
		lespu8TCS_4WD_OPEN 		= TOD4wdOpen;
		lespu1TCS_4WD_LIM_REQ 	= TOD4wdLimReq;
		lespu1TCS_4WD_LIM_MODE 	= TOD4wdLimMode;
		lespu8TCS_4WD_CLU_LIM 	= TOD4wdCluLim;
		lespu16TCS_4WD_TQC_LIM 	= (uint16_t)TOD4wdTqcLimTorq;
	}
#endif
}

void LATCS_ResetTCS4Message(void)
{
	lespu8TCS_4WD_OPEN = 0;		/*4WD_OPEN=00H : Clutch not influenced by ABS/TCS/ESP*/
	lespu1TCS_4WD_LIM_REQ = 0;	/*4WD_LIM_REQ=0 : Not request from ESP*/
	lespu1TCS_4WD_LIM_MODE = 0;

	lespu16TCS_4WD_TQC_LIM = 0xFAFF;/*4WD_TQC_LIM=FAFFH : 4WD cardan shaft torque not limited by ESP*/
	lespu8TCS_4WD_CLU_LIM = 0x00;	/*4WD_CLU_LIM=00H : Clutch duty not limited by ESP*/
}
	#endif
	
#endif

#if __TCS||__ETC	/*TAG4*/
void LATCS_vCoordinateCommand(void)
{
    if((ETCS_ON==0)&&(FTCS_ON==0)&&(AY_ON==0)
	  #if __VDC
    &&(ESP_TCS_ON==0)
      #endif
	  #if __TSP
    &&(TSP_ON==0)
	  #endif    
	  #if __RDCM_PROTECTION
    &&(ltu1RDCMProtENGOn==0)
	  #endif
	  )
	{
        eng_stall_count=0;
        down_count=0;
    #if __COMPETITIVE_EXIT_CONTROL /*0311 modi*/
    	lctcsu1EECActSusFlag =0;
    	lctcsu1EECAfterFastExitFlg = 0;
    #endif	
    }
	else
	{
		#if __VARIABLE_MINIMUM_TORQ_LEVEL
		if( fric_torq > lctcss16NewEngOKTorq )
		{
		   	latcss16CalTorqLowLimit = lctcss16NewEngOKTorq;
		}
		#else 
		if( fric_torq > ENG_OK_TORQ )
		{
		   	latcss16CalTorqLowLimit = ENG_OK_TORQ;
    }
		#endif /*__VARIABLE_MINIMUM_TORQ_LEVEL */
	else
	{
			latcss16CalTorqLowLimit = fric_torq;
		}
		#if (__TCS_ALLOW_NEGATIVE_TQ == DISABLE)
		latcss16CalTorqLowLimit = LCTCS_s16ILimitMinimum(latcss16CalTorqLowLimit, 0);
		#endif
		
#if __VDC
        if(ESP_TCS_ON==1)
        {                                 /* MAIN04 */
        	if ((!ETCS_ON)&&(!FTCS_ON)&&(!AY_ON))
        	{
        		cal_torq=esp_cmd;
        	}
        	else
        	{
            	if(esp_cmd<cal_torq)
            	{
            		cal_torq=esp_cmd;
            	}
            	else if((U8_EEC_FADE_OUT_CONTROL==1)&&(lts16WheelSpinError <= 0))
            	{
            		cal_torq=esp_cmd;
            	}
            	else
            	{
            		;
            	}
            }
			cal_torq = LCTCS_s16ILimitRange( cal_torq, (int16_t)latcss16CalTorqLowLimit , MAX_TORQUE );
        }
        else
        {
        	;
        }
#endif

        #if __TSP
        	if((TSP_ENG_CTRL_ON==1)&&(tsp_torq<cal_torq))
        		{
        			cal_torq=tsp_torq;
        		}
        		else
        		{
        			;
        		}
        #endif

        #if __ROP
        	if((ROP_REQ_ENG)&&(rop_torq < cal_torq))
        	{
        		cal_torq=rop_torq;
        	}
        	else
        	{
        		;
        	}
        #endif
        
        #if __RDCM_PROTECTION
        	if (ltu1RDCMProtENGSustain1Sec==1)
        	{
        		if (ltu1RDCMProtENGOn==1)
        		{
           			if (RDCM_cmd<cal_torq)
           			{
            			cal_torq=RDCM_cmd;
   	        		}
       	    		else
           			{
           				;
   	        		}
    	        }
    	        else	/* Fade out Exit */
				{
					cal_torq = cal_torq + (tcs_cmd-cal_torq)/2;	
        	    }
            }
        	else
        	{
        		;
        	}
		#endif /*__RDCM_PROTECTION*/               
		
		#if __ETSC
			if (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
			{
				LCETSC_vCordEngTorqCmd();
			}
			else
			{
			}
		#endif	/* #if __ETSC */			                      

	#if ((__CAR==FORD_C214) || (__CAR==FORD_C214_MECU) || (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_TAHOE) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE_MECU)||(__CAR==GM_SILVERADO))
        if(cal_torq>=drive_torq)
        {
			cal_torq = LCTCS_s16ILimitMaximum( (int16_t)cal_torq , MAX_TORQUE );
            if((ETCS_ON)||(FTCS_ON))
            {
                tcs_cmd=cal_torq;
                tcs_cmd = LCTCS_s16ILimitMaximum( (int16_t)tcs_cmd , MAX_TORQUE );
				if((drive_torq > 900)&&(cal_torq>drive_torq+50))
				{
					tcs_hold_count=tcs_hold_count+2;
				}
                else if((drive_torq > 900)||(cal_torq>drive_torq+50))
                {
                	tcs_hold_count++;
                }
                else
                {
                	tcs_hold_count=0;
                }

				/* NEW_TCS */
                if ((ESP_TCS_ON)||(BTCS_ON==1))
                {
                	if(tcs_hold_count>U8_TCS_HOLD_COUNT_TIME)
                	{
                	   	tcs_hold_count=U8_TCS_HOLD_COUNT_TIME - 1;
                	}
                	else
                	{
                		;
                	}
                }
                else
                {
                	;
                }
				/* NEW_TCS */
                if(tcs_hold_count>U8_TCS_HOLD_COUNT_TIME)
                {
                    tcs_hold_count=0;
                    ETCS_ON=FTCS_ON=0;
                }
                else
                {
                	;
                }
			}
            else
            {
            	tcs_cmd=drive_torq;
            }
        }
        else
        {
          #if __CAR==GM_DAT_MAGNUS
        	cal_torq = LCTCS_s16ILimitMinimum( (int16_t)cal_torq, 0 );
 		  #else
        	cal_torq = LCTCS_s16ILimitMinimum( (int16_t)cal_torq, (int16_t)latcss16CalTorqLowLimit );
 		  #endif
            tcs_hold_count=0;
        }
	#else
        #if __COMPETITIVE_EXIT_CONTROL
        if (ESP_TCS_ON==1)
        {
        	lctcsu1EECActSusFlag =1;
        }
        else
        {
        	;
        }
        
        if (lctcsu1EECAfterFastExitFlg==0) /*Fast Exit 진입*/
        {
        	if ((lctcsu1EECActSusFlag==1)&&(ESP_TCS_ON==0)
        	&&(lts16WheelSpin<=VREF_1_KPH)&&(det_alatm>LAT_0G5G))
        	{
        		lctcsu1EECAfterFastExitFlg = 1;
        	}
        	else
        	{
        		;
        	}
        }
        else  /*Fast Exit 해제*/
        {
        	if((lts16WheelSpin>=VREF_3_KPH)||(ESP_TCS_ON==1))
        	{
        		lctcsu1EECAfterFastExitFlg = 0;
        		lctcsu1EECActSusFlag = 0;
        	}
        	else
        	{
        		;
        	}
        }
        tcs_tempW0 = (max_torq*U16_TCS_OFF_OFFSET_IN_TURN);
        #endif
        
        if((cal_torq>=drive_torq)
          #if __COMPETITIVE_EXIT_CONTROL
          ||((cal_torq>=tcs_tempW0)&&(lctcsu1EECAfterFastExitFlg==1))
          #endif
        )
        {
          #if 	__COMPETITIVE_EXIT_CONTROL
          	  if (lctcsu1EECAfterFastExitFlg ==1)
          	  {
          	  	if(cal_torq<(tcs_tempW0+20))
              	{
              		cal_torq=tcs_tempW0+20;   /* drv_t error */
              	}
              	else
              	{
              		;
              	}
          	  }
          	  else
          	  {
          	     if(cal_torq<(drive_torq+20))
              	{
              		cal_torq=drive_torq+20;   /* drv_t error */
              	}
              	else
        {
              		;
              	}	
          	  }
          #else
          #if __CAR==GM_DAT_MAGNUS
            cal_torq=drive_torq+20;   /* drv_t error */
		  #else
            if(cal_torq<(drive_torq+20))
            {
            	cal_torq=drive_torq+20;   /* drv_t error */
            }
            else
            {
            	;
            }
          #endif
          #endif
			cal_torq = LCTCS_s16ILimitMaximum( (int16_t)cal_torq , MAX_TORQUE );

            tcs_tempW1 = lctcsu16etcsthreshold1 + lctcss16WhlSpdreferanceByMiniT - VREF_1_KPH;
            tcs_tempW2 = lctcsu16etcsthreshold2 + lctcss16WhlSpdreferanceByMiniT - VREF_1_KPH;
            
            tcs_tempW1 = LCTCS_s16ILimitMinimum(tcs_tempW1,VREF_1_KPH);
            tcs_tempW2 = LCTCS_s16ILimitMinimum(tcs_tempW2,VREF_1_KPH);

            AY_ON=0;
            if( ((ETCS_ON)||(FTCS_ON)) && ((lts16WheelSpin<VREF_5_KPH)||((TCS_MINI_TIRE_SUSPECT==1)&&((lctcss16SmallSpin< tcs_tempW1) && (lctcss16BigSpin < tcs_tempW2))))
			 	&& (ESP_TCS_ON == 0) 
			#if __RDCM_PROTECTION
    		||(ltu1RDCMProtENGOn==1)
    		#endif            	
    		)
            {
                tcs_cmd=cal_torq;
				tcs_cmd = LCTCS_s16ILimitMaximum( (int16_t)tcs_cmd , MAX_TORQUE );
                tcs_hold_count++;

                if ((ESP_TCS_ON)||(BTCS_ON==1))
                {
                	if(tcs_hold_count> (uint16_t)U8_TCS_HOLD_COUNT_TIME)
                	{
                	   	tcs_hold_count=(uint16_t)U8_TCS_HOLD_COUNT_TIME - 1;
                	}
                	else
                	{
                		;
                	}
                }
                else
                {
                	;
                }

                if(tcs_hold_count>(uint16_t)U8_TCS_HOLD_COUNT_TIME)
                {
                    tcs_hold_count=0;
                    ETCS_ON=FTCS_ON=0;
                    #if __RDCM_PROTECTION
                    ltu1RDCMProtENGOn=0;
                    ltu1RDCMActReference=0;
                    ltu8RDCM1stControlCount=0;
                    #endif                    
                }
                else
                {
                	;
                }
            }
            else
            {
            	tcs_cmd=drive_torq;
            }
        }
        else
        {
        #if __CAR==GM_DAT_MAGNUS
			cal_torq = LCTCS_s16ILimitMinimum( (int16_t)cal_torq, 0 );
        #else
			cal_torq = LCTCS_s16ILimitMinimum( (int16_t)cal_torq, (int16_t)latcss16CalTorqLowLimit );
		#endif
            tcs_hold_count=0;
        }
	#endif
	}
}

	#if !SIM_MATLAB  /* TAG1 */
void LATCS_vConvertTCS2EMS(void)
{
	#if defined(SSANGYONG_CAN)
		LATCS_vConvertTCS2EMSForSYC();
	#elif defined(HMC_CAN) || defined (YFE_CAN)	
		LATCS_vConvertTCS2EMSForHMC();
	#elif __GM_VEHICLE
		LATCS_vConvertTCS2EMSForGM();
	#elif (__DEMO_VEHICLE)
		#if (__CAR==FORD_C214)
		LATCS_vConvertTCS2EMSForFORD();
		#elif __CAR==CHINA_B12
		LATCS_vConvertTCS2EMSForB12();
		#elif __CAR==BYD_5A
		LATCS_vConvertTCS2EMSFor5A();
		#elif (__CAR==PSA_C4) || (__CAR==PSA_C4_MECU) || (__CAR==PSA_C3)	
		LATCS_vConvertTCS2EMSForC4();		
		#elif (__CAR==DCX_COMPASS) || (__CAR==DCX_COMPASS_MECU)
		LATCS_vConvertTCS2EMSForCOMPASS();
		#elif __CAR==RSM_QM5 || __CAR==RSM_QM5_MECU
		LATCS_vConvertTCS2EMSForQM5();			
		#elif (__CAR==BMW740i)
		LATCS_vConvertTCS2EMSForBMW740i();		
		#elif (__CAR==VV_V40)
		LATCS_vConvertTCS2EMSForV40();		
		#elif (__CAR==FIAT_PUNTO)
		LATCS_vConvertTCS2EMSForPUNTO();
		#endif
		
	#endif
}

#if defined(SSANGYONG_CAN)
static void LATCS_vConvertTCS2EMSForSYC(void)
{
    if (lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=0;
    }
    else
    {
    	;
    }
  #if __VDC
    if (lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {/* error flag 통합:ESP_ERROR_FLG=>tcs_error_flg*/
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=ESP_TCS_ON=ETO_SUSTAIN_1SEC=0;
    }
    else
    {
    	;
    }
  #endif

	if(BTC_fz||ETCS_ON||FTCS_ON||ESP_TCS_ON||AY_ON||YAW_CDC_WORK
  #if __ROP	
	||ROP_REQ_ENG
  #endif	
  #if __TSP	
	||(TSP_ON==1)
  #endif
  	)
	{
       	lespu1ESP_ESP_INFO_BL=1; /* ESP_INFO_BL ESP lamp, setting 1h */
	}
	else
	{
       	lespu1ESP_ESP_INFO_BL=0; /* ESP_INFO_BL ESP lamp, setting 0h  */
	}

	if( YAW_CDC_WORK || BTC_fz)  /* Active condition TOD duty 30% fixed by B.H.Kwak */
	{
		lespu1TCS_BRE_AKT_ESP = 1; /* BRE_AKT_ESP Brake intervention ESP, setting 1h */
	}
	else
	{
		lespu1TCS_BRE_AKT_ESP = 0; /* BRE_AKT_ESP Brake intervention ESP, setting 0h */
	}

	lespu8ESP_GMAX_ESP = 0;
	lespu8ESP_GMINMAX_ART = 0;
	lespu8ESP_GMIN_ESP = 0;	

	if(ETCS_ON||FTCS_ON||ESP_TCS_ON||AY_ON
  #if __ROP
      ||ROP_REQ_ENG
  #endif
  #if __TSP
	  ||(TSP_ON==1)
  #endif
    )
	{
		lespu1ESP_AMR_AKT_ESP=1; 		/* AMR_AKT_ESP Driver moment regulation active, setting 1 */
		lespu1TCS_TCS_REQ=1;			/* MMIN_ESP Engine moment requirement min, setting 1 */
		lespu1TCS_MSR_C_REQ=0;			/* MMOTSE MMAX_ESP = 0, this signal setting 0*/

		lesps16TCS_TQI_TCS=cal_torq;	/* MMOTSR Required reduction of engine torque ASR */
        								/* MMOTSR inside ESP, setting 0 ~ FEh */
        								/* MMOTSR 0-762NM scail 0-254,resolution 3NM */
		lesps16TCS_TQI_MSR=0;
		lesps16TCS_TQI_ESP=cal_torq;    /* M_ESP */
	}
  #if __EDC
	else if(EDC_ON)
	{
		lespu1ESP_AMR_AKT_ESP=1;
		lespu1TCS_MSR_C_REQ=1;			/* MMOTSE MMAX_ESP = 0, this signal setting 1*/
		lespu1TCS_TCS_REQ=0;
		lesps16TCS_TQI_MSR=MSR_cal_torq;
		lesps16TCS_TQI_TCS=0x2FFF;
		lesps16TCS_TQI_ESP=MSR_cal_torq;
	}
  #endif
	else
	{
		lespu1ESP_AMR_AKT_ESP=0; 		/* AMR_AKT_ESP Driver moment regulation active, setting 0 */
		lespu1TCS_TCS_REQ=0;			/* MMIN_ESP Engine moment requirement min, setting 0 */
		lespu1TCS_MSR_C_REQ=0;
		lesps16TCS_TQI_TCS=0x2FFF;  	/* MMOTSR outside ESP, setting FEh */
		lesps16TCS_TQI_MSR=0;
		lesps16TCS_TQI_ESP=0x1FFF;		/* M_ESP outside ESP, setting 1FFEh */
	}
	
	if (ABS_fz==1) 
	{
		lespu8TCS_EBS_STATUS = 1; 
	}
	#if (__GEAR_SHIFT_INHIBIT)
	else if ((ETCS_ON==1) || (FTCS_ON==1)||(ETO_SUSTAIN_1SEC==1))
	{
		if((eng_rpm<3000)&&(gs_pos>=1))
		{
			lespu8TCS_EBS_STATUS = 2;    /* set TCS_GSC , 03SWD , 04SWD */
		}
		else if((eng_rpm>3200)||(gs_pos<1))
		{
			lespu8TCS_EBS_STATUS = 0;
		}			
		else
		{
			;
		}
	}
  	#else
    /* EEC제어 시 제어 진입 당시의 기어 위치로부터 Gear Shift 금지 request -> EBS_STATUS = 0x02 (SYMC only) */
	else if ( (ETO_SUSTAIN_1SEC==1) || ((ETCS_ON==1)&&(ltcss16ETCSEnterExitFlags==4)) )	/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
  	{
		lespu8TCS_EBS_STATUS = 2;
	}
	#endif /*__GEAR_SHIFT_INHIBIT*/				
	else
	{
		lespu8TCS_EBS_STATUS = 0;
	}		
	
	/* ================== */
	/* TOD CONTROL        */
	/* ================== */
  	#if __4WD_VARIANT_CODE==ENABLE
    if(lsu8DrvMode == DM_2WD)
    {
		LATCS_ResetTCS4Message();
    }
    else
    {
		LATCS_SetTCS4Message();
    }
  	#else
		#if __4WD
		LATCS_SetTCS4Message();
		#else
		LATCS_ResetTCS4Message();
		#endif
  	#endif	
}

#elif defined(HMC_CAN) || defined (YFE_CAN)	
static void LATCS_vConvertTCS2EMSForHMC(void)
{
	#if __TCS_CONV_ABS_TORQ
	LATCS_vConvAbsToCanTorq();
	#endif
	  
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=0;
    }
    else
    {
    	;
    }
  #if __VDC
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)    
    {/* error flag 통합:ESP_ERROR_FLG=>tcs_error_flg*/
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=ESP_TCS_ON=ETO_SUSTAIN_1SEC=0;
    }
    else
    {
    	;
    }
  #endif

 #if __EMS_TEST_T
    ENGINE_TEST_MODE();
 #else
  #if __VDC
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	#endif
	#if __CDM
	  ||(lcu1CdmEngReq==1)
	#endif
	#if __ETSC
     ||(letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
    #endif
	)    
    {
    	tcs_tempW0 = S16TCSGearShiftInhibitLATAcc + LAT_0G05G;
		tcs_tempW0 = LCTCS_s16ILimitMinimum(tcs_tempW0, 0);
		    	
    	#if __TCS_GEAR_SHIFT_CONTROL
		if (lespu1EMS_DCT_TCU==1)
		{
			#if __EEC_GEAR_INHIBIT_MODE	
			if ((ETCS_ON==1) || (FTCS_ON==1)||(AY_ON==1)||(ETO_SUSTAIN_TIME==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
			#else
			if ((ETCS_ON==1) || (FTCS_ON==1)||(AY_ON==1)||(ETO_SUSTAIN_1SEC==1))
			#endif	
			{
				if((gs_pos==1)&&(FTCS_ON==1)) /*Hill 등판 위해 1단 에서는 전영역 Gear_Shift_Inhibit 고려*/
				{
					if (eng_rpm<U16_TCS_GSC_RPM_ENTER)
					{
						lespu1TCS_TCS_GSC = 1;
					}
					else if (eng_rpm>U16_TCS_GSC_RPM_EXIT)
					{
						lespu1TCS_TCS_GSC = 0;
					}
					else
					{
						;
					}
						
				}
				else     /*DCT 미션 1단 아닌 경우에는 EEC & EEC_After 상황에서만 변속금지 고려*/
				{ 
					if ((ETO_SUSTAIN_TIME==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
					{
						if ((eng_rpm<U16_TCS_GSC_RPM_ENTER)&&(det_alatm<S16TCSGearShiftInhibitLATAcc))
						{
					lespu1TCS_TCS_GSC = 1;
				}
						else if ((eng_rpm>U16_TCS_GSC_RPM_EXIT)||(det_alatm >= tcs_tempW0))
				{
					lespu1TCS_TCS_GSC = 0;
				}			
				else
				{
					;
				}
			}
			else
					{
						lespu1TCS_TCS_GSC = 0;
					}
				}
			}
			else
			{/* reset TCS_GSC*/
				lespu1TCS_TCS_GSC = 0;                         
			}
		}
		else
		{	
			#if __EEC_GEAR_INHIBIT_MODE
			if((ETO_SUSTAIN_TIME==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
			#else
			if(ETO_SUSTAIN_1SEC==1)
			#endif
			{
			    if((eng_rpm<U16_TCS_GSC_RPM_ENTER)&&(det_alatm<S16TCSGearShiftInhibitLATAcc))
			    {
			    	lespu1TCS_TCS_GSC = 1;   
			    }
			    else if ((eng_rpm>U16_TCS_GSC_RPM_EXIT)||(det_alatm >= tcs_tempW0))
			    {
			    	lespu1TCS_TCS_GSC = 0;
			    }
			    else
			    {
			    	;
			    }
			}
			else
			{/* reset TCS_GSC*/	
				lespu1TCS_TCS_GSC = 0;                         
			}
    	}
    	#endif /*__TCS_GEAR_SHIFT_CONTROL*/        
  #else
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1))     
    {/* TCS Control*/
        #if __TCS_GEAR_SHIFT_CONTROL
        if(AY_ON)
        {/* set TCS_GSC*/
        	lespu1TCS_TCS_GSC = 1;                    
        }
        else
        {/* reset TCS_GSC*/
        	lespu1TCS_TCS_GSC = 0;                         
        }
        #endif /*__TCS_GEAR_SHIFT_CONTROL*/
  #endif
        lespu1TCS_TCS_REQ = 1;
        lespu1TCS_TCS_CTL = 1;
        
      #if __CDM
	    if(lcu1CdmEngReq==1)
	    {
	      #if __TCS_CONV_ABS_TORQ  
	        lesps16TCS_TQI_TCS = fric_torq_rel;
	      #else
	        cal_torq = fric_torq_rel;
	      #endif
	    }    
	  #endif

        /*	===============================================================   */
		/* 2005-02-01 '05 SWD TQI_SLOW_TCS 적용(TQI_TCS + 7% 유지)  Added by Baek, Jongtak */
        /* 2006-02-25 '06 China TQI_SLOW_TCS 적용 */
        #if (__ETC_TYPE_VEHICLE == DISABLE)
		if (lespu8EMS_ENG_CHR>3) 
		{/* ETC 적용 차량 */
		#endif /*(__ETC_TYPE_VEHICLE == DISABLE)*/	
		  #if __TCS_CONV_ABS_TORQ
			tempW1 = lesps16TCS_TQI_TCS;
		    tempW2 = (int16_t)(U8_SLOW_TORQ_DEVIATION*10);
  			if (tempW1<(lesps16EMS_DRIVE_TQI-tempW2))	
  			{
  				tempW1=tempW1+tempW2;	/* TQI_SLOW_TCS */
  			}	
	  		else if (tempW1>lesps16EMS_DRIVE_TQI) 
	  		{
	  			tempW1=tempW1;
	  		}	
		  	else 
		  	{
		  		tempW1=lesps16EMS_DRIVE_TQI;			
		  	}	
		  #else
		    tempW2 = (int16_t)(U8_SLOW_TORQ_DEVIATION*10);
			if (cal_torq<(drive_torq-tempW2))	
			{
				tempW1=cal_torq+tempW2;	/* TQI_SLOW_TCS */
			}	
			else if (cal_torq>drive_torq) 
			{
				tempW1=cal_torq;
			}	
			else 
			{	
				tempW1=drive_torq;
			}	
		  #endif
			/*if (tempW1<200) tempW1=200;*/
			if (tempW1>U16_TCS_TQI_MAX_TORQUE) tempW1=U16_TCS_TQI_MAX_TORQUE;
        	lesps16TCS_TQI_SLOW_TCS=tempW1;                 /* set TQI_SLOW_TCS	*/
        	lespu1TCS_TCS_MFRN = 0;                         /* TCS_MFRN=0(FAST&SLOW) */
        #if (__ETC_TYPE_VEHICLE == DISABLE)	
		}
		else 
		{/* ETC 미적용 차량 */
    		lesps16TCS_TQI_SLOW_TCS=0x3fc;			        /* clear TQI_SLOW_TCS     */
    		lespu1TCS_TCS_MFRN = 1;                         /* TCS_MFRN=1(Bosch:0, Teves:1) */
    	}
    	#endif /*(__ETC_TYPE_VEHICLE == DISABLE)*/

	  #if !__TCS_CONV_ABS_TORQ
        tempW0=cal_torq;                            
      #else
      	tempW0=lesps16TCS_TQI_TCS;
      #endif
        if(tempW0>U16_TCS_TQI_MAX_TORQUE)
        {
        	tempW0=U16_TCS_TQI_MAX_TORQUE;
        }
        else
        {
        	;
        }
        lesps16TCS_TQI_TCS = tempW0;                      /* set TQI_TCS*/
        lesps16TCS_TQI_MSR = 0;		                      /* clear TQI_MSR*/
        lespu1TCS_MSR_C_REQ = 0;	        		      /* reset MSR_C_REQ */
    }
    else
    {/* No EMS Control*/
	    lespu1TCS_TCS_REQ = 0;                            /* reset TCS_REQ */
        lespu1TCS_TCS_GSC = 0;                 		      /* reset TCS_GSC */
        lespu1TCS_MSR_C_REQ = 0;     				      /* reset MSR_C_REQ */
        lespu1TCS_TCS_CTL = 0;                            /* reset TCS_CTL */
        if(BTC_fz) lespu1TCS_TCS_CTL = 1;                 /* set TCS_CTL */
        lesps16TCS_TQI_TCS = U16_TCS_TQI_DDEFAULT_TORQUE; /* clear TQI_TCS */
        lesps16TCS_TQI_MSR = 0;                                /* clear TQI_MSR */
        lesps16TCS_TQI_SLOW_TCS = U16_TCS_TQI_DDEFAULT_TORQUE; /* clear TQI_SLOW_TCS */
	  #if !__TCS_CONV_ABS_TORQ
        cal_torq=drive_torq;
      #endif
	}

  #if __SCC && __TCS_GEAR_SHIFT_CONTROL
	if ( (lcu1SccEngCtrlReq==1) &&  (lcu1SccGearHoldFlg==1) )
	{/* set TCS_GSC */
		lespu1TCS_TCS_GSC = 1;
	}
	else
	{
		;
	}
  #endif	/* #if __SCC */    
    
    if(ABS_fz == 1)
    {/* set ABS_ACT*/
        lespu1TCS_ABS_ACT = 1;                              
    }
    else
    {/* reset ABS_ACT*/
        lespu1TCS_ABS_ACT = 0;                              
    }
  #if __VDC
    if((ESP_TCS_ON)||(YAW_CDC_WORK)||(ETO_SUSTAIN_1SEC)
   #if __HDC && __CAR==KMC_HM
    	||(lcu1HdcActiveFlg==1)
   #endif
    )
    {/* set ESP_CTL*/
        lespu1TCS_ESP_CTL = 1;                              
    }
    else
    {/* reset ESP_CTL*/
        lespu1TCS_ESP_CTL = 0;                              
    }

	if (U8_ESP_Output_Selection_Mode==10)
	{/* CAN CHECK시 ESP OFF 상태일 경우 GEAR SHIFT INHIBITION*/
		if((!vdc_on_flg)&&(gs_pos>1))
		{/* set TCS_GSC */
			lespu1TCS_TCS_GSC = 1;    
			lespu1TCS_ESP_CTL = 1;
		}
		else
		{
			;
		}
	}
  #endif
 #endif

	#if __ETC_TYPE_VEHICLE
	  #if (__AHB_SYSTEM==ENABLE)
	lespu1ActiveBrake = FLAG_ACTIVE_BRAKING;
	  #endif
	#endif

	/* ================== */
	/* TOD CONTROL        */
	/* ================== */
  #if __HMC_CAN_VER==CAN_1_3 ||__HMC_CAN_VER==CAN_1_4 ||__HMC_CAN_VER==CAN_1_6 ||__HMC_CAN_VER==CAN_2_0 || defined(SSANGYONG_CAN)
  	#if __4WD_VARIANT_CODE==ENABLE
    if(lsu8DrvMode == DM_2WD)
    {
		LATCS_ResetTCS4Message();
    }
    else
    {
		LATCS_SetTCS4Message();
    }
  	#else
		#if __4WD
		LATCS_SetTCS4Message();
		#else
		LATCS_ResetTCS4Message();
		#endif
  	#endif
  #endif

	#if __HMC_CAN_VER==CAN_1_2
 #if __REAR_D
    lespu8TCS_WHEEL=(uint8_t)( (RL.lsabss16WhlSpdRawSignal64 + RR.lsabss16WhlSpdRawSignal64) / 128 );          /* mean rear wheel velocity*/
    if(ALG_INHIBIT_rl==1)
    {
    	lespu8TCS_WHEEL=(uint8_t)(RR.lsabss16WhlSpdRawSignal64/64);
    }
    else
    {
    	;
    }

    if(ALG_INHIBIT_rr==1)
    {
    	lespu8TCS_WHEEL=(uint8_t)(RL.lsabss16WhlSpdRawSignal64/64);
    }
    else
    {
    	;
    }

    if((ALG_INHIBIT_rl==1)&&(ALG_INHIBIT_rr==1))
    {
    	lespu8TCS_WHEEL=0xff;
    }
    else
    {
    	;
    }
 #else	/* #if __REAR_D */
    lespu8TCS_WHEEL=(uint8_t)( (FL.lsabss16WhlSpdRawSignal64 + FR.lsabss16WhlSpdRawSignal64) / 128 );          /* mean front wheel velocity*/
    if(ALG_INHIBIT_fl==1)
    {
    	lespu8TCS_WHEEL=(uint8_t)(FR.lsabss16WhlSpdRawSignal64/64);
    }
    else
    {
    	;
    }

    if(ALG_INHIBIT_fr==1)
    {
    	lespu8TCS_WHEEL=(uint8_t)(FL.lsabss16WhlSpdRawSignal64/64);
    }
    else
    {
    	;
    }

    if((ALG_INHIBIT_fl==1)&&(ALG_INHIBIT_fr==1))
    {
    	lespu8TCS_WHEEL=0xff;
    }
    else
    {
    	;
    }
 #endif	/* #if __REAR_D */

    lespu8TCS_WHEEL_FL_TCS=(uint8_t)(FL.lsabss16WhlSpdRawSignal64/64);
    if(ALG_INHIBIT_fl==1)
    {
    	lespu8TCS_WHEEL_FL_TCS=0xff;
    }
    else
    {
    	;
    }
    lespu8TCS_WHEEL_FR_TCS=(uint8_t)(FR.lsabss16WhlSpdRawSignal64/64);
    if(ALG_INHIBIT_fr==1)
    {
    	lespu8TCS_WHEEL_FR_TCS=0xff;
    }
    else
    {
    	;
    }
    lespu8TCS_WHEEL_RL_TCS=(uint8_t)(RL.lsabss16WhlSpdRawSignal64/64);
    if(ALG_INHIBIT_rl==1)
    {
    	lespu8TCS_WHEEL_RL_TCS=0xff;
    }
    else
    {
    	;
    }
    lespu8TCS_WHEEL_RR_TCS=(uint8_t)(RR.lsabss16WhlSpdRawSignal64/64);
    if(ALG_INHIBIT_rr==1)
    {
    	lespu8TCS_WHEEL_RR_TCS=0xff;
    }
    else
    {
    	;
    }
    #endif /*__HMC_CAN_VER==CAN_1_2*/
    /* MBD ETCS */
 	if (TMC_REQ_GEAR_STRUCT.lctmcu8_BSTGRReqType == 1)
 	{
  		lespu1TCS_TCS_GSC = 1;
 	}
 	else
 	{
  		lespu1TCS_TCS_GSC = 0;
 	}   
}

#elif __GM_VEHICLE
static void LATCS_vConvertTCS2EMSForGM(void)
{
    /* Coded by KJH, 2006.06.02 */
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)   
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	#endif
    #if __RDCM_PROTECTION
      ||(ltu1RDCMProtENGSustain1Sec==1)	
	#endif
	#if __ETSC
    	||(letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
    #endif				
	)
	{
    	lespu1TCS_TCS_REQ    = 1;		/*Chassis System Engine Torque Request : Torque Intervention Type*/
	}
	else
	{
    	lespu1TCS_TCS_REQ    = 0;
	}

	/* Tx Cal Torque */
	lesps16TCS_TQI_TCS = cal_torq;	
	
	#if __TCS_CONV_ABS_TORQ
		LATCS_vConvAbsToCanTorq();
	#endif

  #if __4WD_VARIANT_CODE==ENABLE
	if (lsu8DrvMode == DM_AUTO)
	{
		#if __TOD
		if (ESP_TCS_ON==1)/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
	  	{
	  		if (laabsu1TrnsBrkSysCltchRelRqd==1)
	  		{
	  			lespu16_TCCRCplReqVal = 0x0000;
	  			lespu1_TCCRCplReqAct = 1;
	  		}
	  		else
	  		{
				lespu16_TCCRCplReqVal = TOD4wdTqcLimTorq;/* Transfer Case Coupling Request : Coupling Request Value */
				lespu1_TCCRCplReqAct = 1;		  	     /* Transfer Case Coupling Request : Coupling Request Active */	  			  			
			}
	  	}
	  	else
	  	{
	  		if (laabsu1TrnsBrkSysCltchRelRqd==1)
	  		{
	  			lespu16_TCCRCplReqVal = 0x0000;
	  			lespu1_TCCRCplReqAct = 1;
	  		}
	  		else
	  		{
				lespu16_TCCRCplReqVal = 0x07FF;
				lespu1_TCCRCplReqAct = 0;	  			
			}
		}	  				  			
		#else /*!__TOD*/
		lespu16_TCCRCplReqVal = 0x07FF;              /* Transfer Case Coupling Request : Coupling Request Value */
		lespu1_TCCRCplReqAct = 0;		  	         /* Transfer Case Coupling Request : Coupling Request Active */		
		#endif
	}
	else
	{
		lespu16_TCCRCplReqVal = 0x07FF;              /* Transfer Case Coupling Request : Coupling Request Value */
		lespu1_TCCRCplReqAct = 0;		  	         /* Transfer Case Coupling Request : Coupling Request Active */				
	}
	lespu8_TCCDGCDecayGrad = 0x7F;      		     /* Transfer Case Coupling Decay Gradient Control : Decay Gradient */
	lespu1_TCCDGCDecayCtrlEn = 0; 		             /* Transfer Case Coupling Decay Gradient Control : Decay Control Enabled */

  #elif	(__CAR==GM_TAHOE) || (__CAR==GM_SILVERADO) || ( (__CAR==GM_C100) && (__4WD) ) || ( (__CAR==GM_C100) && (__4WD) )
	#if __TOD
	if (ESP_TCS_ON==1)/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
  	{
  		if (laabsu1TrnsBrkSysCltchRelRqd==1)
  		{
  			lespu16_TCCRCplReqVal = 0x0000;
  			lespu1_TCCRCplReqAct = 1;
  		}
  		else
  		{
			lespu16_TCCRCplReqVal = TOD4wdTqcLimTorq;/* Transfer Case Coupling Request : Coupling Request Value */
			lespu1_TCCRCplReqAct = 1;		  	     /* Transfer Case Coupling Request : Coupling Request Active */	  			  			
		}
  	}
  	else
  	{
  		if (laabsu1TrnsBrkSysCltchRelRqd==1)
  		{
  			lespu16_TCCRCplReqVal = 0x0000;
  			lespu1_TCCRCplReqAct = 1;
  		}
  		else
  		{
			lespu16_TCCRCplReqVal = 0x07FF;
			lespu1_TCCRCplReqAct = 0;	  			
		}
	}	  				  			
	#else /*!__TOD*/
	lespu16_TCCRCplReqVal = 0x07FF;              /* Transfer Case Coupling Request : Coupling Request Value */
	lespu1_TCCRCplReqAct = 0;		  	         /* Transfer Case Coupling Request : Coupling Request Active */		
	#endif
  #elif	(__CAR==GM_LAMBDA)
	if (ESP_TCS_ON==1)/* ETO_SUSTAIN_1SEC : ESP_TCS_ON이 Clear 된 이후 1초 유지 */
  	{
  		if (laabsu1ABSActCan==1)
  		{
  			lespu16_TCCRCplReqVal = 0x0000;
  			lespu1_TCCRCplReqAct = 1;
  		}
  		else
  		{
			lespu16_TCCRCplReqVal = TOD4wdTqcLimTorq * 20;/* Transfer Case Coupling Request : Coupling Request Value */
			lespu1_TCCRCplReqAct = 1;		  	          /* Transfer Case Coupling Request : Coupling Request Active */	  			  			
		}
  	}
  	else
  	{
  		if (laabsu1ABSActCan==1)
  		{
  			lespu16_TCCRCplReqVal = 0x0000;
  			lespu1_TCCRCplReqAct = 1;
  		}
  		else
  		{
			lespu16_TCCRCplReqVal = 0x07FF;
			lespu1_TCCRCplReqAct = 0;	  			
		}
	}	  				  			  
  #else
	lespu16_TCCRCplReqVal = 0;		  	         /* Transfer Case Coupling Request : Coupling Request Value */
	lespu1_TCCRCplReqAct = 0;		  	         /* Transfer Case Coupling Request : Coupling Request Active */
	lespu8_TCCDGCDecayGrad = 0;			         /* Transfer Case Coupling Decay Gradient Control : Decay Gradient */
	lespu1_TCCDGCDecayCtrlEn = 0; 		         /* Transfer Case Coupling Decay Gradient Control : Decay Control Enabled */
  #endif    
  
  #if __GM_VEHICLE
    lespu1TracTrqDecCntAtv = 0;			/*Traction Torque Decay Control Active*/
    lespu16_TTDGCDcyGrad = 1023;		/*Traction Torque Decay Gradient Control : Decay Gradient*/
  #else
	LATCS_vCallTorqDecayCtrl();
	lespu1TracTrqDecCntAtv = ltcsu8TorqDecayCtrl;			/*Traction Torque Decay Control Active*/
	lespu16_TTDGCDcyGrad = ltcsu16TorqDecayCtrlGrdnt;		/*Traction Torque Decay Gradient Control : Decay Gradient*/
  #endif
  
	lespu16_TracCntrlMaxTorqIncRt = 1008;  /*Traction Control Maximum Torque Increase Rate*/
	lespu8_WhlSensRghRdMag = 1;			   /*Wheel Sensor Rough Road Magnitude*/

	#if __TCS_GEAR_SHIFT_CONTROL
	lespu8_BSTGRReqTypeOld = lespu8_BSTGRReqType;
	if((fu1TCUTimeOutErrDet==0)
	   &&((ETO_SUSTAIN_1SEC==1)||((ETCS_ON==1)&&(ltcss16ETCSEnterExitFlags==4))
	   #if __EEC_GEAR_INHIBIT_MODE
	   ||(EEC_GEAR_INHIBIT_REQUEST==1)
	   #endif
	   ))	   
	{
		if((eng_rpm<(uint16_t)U16_GEAR_SHIFT_UP_RPM)&&(gs_pos>1))
        {
			lespu8_BSTGRReqType = TRANSMISSION_HOLD_GEAR_REQUEST;	/*Brake System Transmission Gear Request : Request Type*/
			lespu8_BSTGRReqdGear = lespu8GearPosBeforeEECActivated;	/*Brake System Transmission Gear Request : Requested Gear*/			
        }
        else if((eng_rpm>=(uint16_t)U16_GEAR_SHIFT_UP_RPM))
        {
			lespu8_BSTGRReqType = TRANSMISSION_MIN_GEAR_REQUEST;	/*Brake System Transmission Gear Request : Request Type*/
			if ( (lespu8_BSTGRReqTypeOld!=TRANSMISSION_MIN_GEAR_REQUEST) && 
				 (lespu8_BSTGRReqType==TRANSMISSION_MIN_GEAR_REQUEST) )
			{/* lespu8TCU_TAR_GC는 변하는 값이므로 Gear Control Mode가 Min으로 변경 될때만 Update */
				lespu8_BSTGRReqdGear = lespu8TCU_TAR_GC + 1;	    /*Brake System Transmission Gear Request : Requested Gear*/
				lespu8GearPosBeforeEECActivated = lespu8_BSTGRReqdGear;				
			}
			else
			{
				;
			}

        }
        else
        {
			lespu8_BSTGRReqType = TRANSMISSION_HOLD_GEAR_REQUEST;	/*Brake System Transmission Gear Request : Request Type*/
			lespu8_BSTGRReqdGear = lespu8GearPosBeforeEECActivated;	/*Brake System Transmission Gear Request : Requested Gear*/
        }      
	}
	else if(ETCS_ON)
	{
		if((eng_rpm>=(uint16_t)U16_GEAR_SHIFT_UP_RPM))
        {
			lespu8_BSTGRReqType = TRANSMISSION_MIN_GEAR_REQUEST;	 /*Brake System Transmission Gear Request : Request Type*/
			if ( (lespu8_BSTGRReqTypeOld!=TRANSMISSION_MIN_GEAR_REQUEST) && 
				 (lespu8_BSTGRReqType==TRANSMISSION_MIN_GEAR_REQUEST) )
			{/* lespu8TCU_TAR_GC는 변하는 값이므로 Gear Control Mode가 Min으로 변경 될때만 Update */
				lespu8_BSTGRReqdGear = lespu8TCU_TAR_GC + 1;	     /*Brake System Transmission Gear Request : Requested Gear*/
				lespu8GearPosBeforeEECActivated = lespu8_BSTGRReqdGear;				
			}
			else
			{
				;
			}			
        }
        else
        {		
			lespu8_BSTGRReqType = TRANSMISSION_HOLD_GEAR_REQUEST;
			lespu8_BSTGRReqdGear = lespu8GearPosBeforeEECActivated;
		}
	}
	else
	{
		lespu8_BSTGRReqType = TRANSMISSION_NO_ACTION;			/*Brake System Transmission Gear Request : Request Type*/
		lespu8_BSTGRReqdGear = 0;			                    /*Brake System Transmission Gear Request : Requested Gear*/
		lespu8GearPosBeforeEECActivated = lespu8TCU_TAR_GC;
	}
	#endif /*__TCS_GEAR_SHIFT_CONTROL*/
	lespu1_WhlSensRghRdMagV = 0;		/*Wheel Sensor Rough Road Magnitude Validity*/
	lespu8_VhDynFLWhlSt = 0;			/*Vehicle Dynamics Control Front Left Wheel Status*/
	lespu8_VhDynFRWhlSt = 0;			/*Vehicle Dynamics Control Front Right Wheel Status*/
	lespu8_VhDynRLWhlSt = 0;            /*Vehicle Dynamics Control Rear Left Wheel Status*/
	lespu8_VhDynRRWhlSt = 0;            /*Vehicle Dynamics Control Rear Right Wheel Status*/

	/* Added by Kim Jeonghun in 2007.7.21 */
	if((tc_on_flg==1)&&(fu8EngineModeStep!=0)&&(fu8EngineModeStep!=1))
	{
		lespu8TCSysOpMd = 1;
	}
	else
	{
		lespu8TCSysOpMd = 0;
	}

	if((YAW_CDC_WORK==1)||(ESP_TCS_ON==1))
	{
		lespu8VehStabEnhmntMd = 1;
	}
	else
	{
		lespu8VehStabEnhmntMd = 0;
	}

	lespu1TreInfMonSysRstPrfmd = 0;
	lespu1TirePrsLowIO = 0;

	if(PBA_ON==1)
	{
		lespu1BrkSysBrkLtsReqd = 1;
	}
	else if(DECLAMP_ON ==1)
	{
		lespu1BrkSysBrkLtsReqd = 1;		
	}	
#if __TSP
	else if(TSP_BRAKE_CTRL_ON==1)
	{
		lespu1BrkSysBrkLtsReqd = 1;
	}
#endif 	
#if __HSA
	else if(HSA_flg==1)
	{
		lespu1BrkSysBrkLtsReqd = 1;
	}
#endif
#if __HDC
	else if(lcu1HdcActiveFlg==1)
	{
		lespu1BrkSysBrkLtsReqd = 1;
	}
#endif
	else
	{
		lespu1BrkSysBrkLtsReqd = 0;
	}

	lespu1BrkSysRedBrkTlltlReq = 0;
	lespu8BrkSysVTopSpdLimVal = 255;
	lespu1ActVehAccelV = lsabsu8Decelvalidity;
	lesps16ActVehAccel_0 = lsabss16DecelRefiltByVref5;
	lespu8TracCntrlMaxTorqIncRt = 63;


#if __EDC	
  #if __GM_VEHICLE
    if(fu8EngineModeStep==0 || fu8EngineModeStep==1)
    {
    	EDC_ON = 0;
    }
    else
    {
    	;
    }				
  #endif
#endif  
  	
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)    
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	#endif
    #if __RDCM_PROTECTION
      ||(ltu1RDCMProtENGSustain1Sec==1)	
	#endif			
	#if __ETSC 
	  || (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
	#endif
	)
	{
		if( (lespu1EngTqMinExtRngVal==0) && (lesps16EngTqMinExtRng >= 11995) )
    	{
    		lespu8TqIntvnTyp = 0;
    	}
    	else
    	{
    		lespu8TqIntvnTyp = 1;
    	}	
	}
#if __EDC
	else if(EDC_ON==1)
	{
		lespu8TqIntvnTyp = 2;
	}
#endif
	else
	{
		lespu8TqIntvnTyp = 0;
	}

  #if (__CAR == GM_V275) || (__CAR == GM_TAHOE) || (__CAR == GM_C100)
    if(lespu8TqIntvnTyp==0)
    {
    	lespu16TorqReqVal = 800;
    }
    else
    {
    	lespu16TorqReqVal = (uint16_t)((((int32_t)lesps16TCS_TQI_TCS + 2000) * 4) / 10);
    }	    
  #else
    if(lespu8TqIntvnTyp==0)
    {
    	lespu16TorqReqVal = 1696;
    }
    else
    {
    	lespu16TorqReqVal = (uint16_t)((((int32_t)lesps16TCS_TQI_TCS + 8480) * 2) / 10);
    }	  
  #endif

		#if __GM_FailM		
	if((lctcsu1InhibitByFM==1) && (EDC_ON==0))
	{
		lespu8TqIntvnTyp = 0;
	}	
	else
	{
		;
	}					
		#endif	
		    
	if(laabsu1TrnsBrkSysCltchRelRqd==1) /* laabsu1ABSActCan==1 */
	{
		lespu1ABSAct=1;
	}
	else
	{
		lespu1ABSAct=0;
	}

	#if __TSP
	if(TSP_ON==1)
	{
		TCS_ON=1;
	}
	else
	{
		;
	}
	#endif

	if(TCS_ON==1)
	{
		lespu1TCSAct=1;
	}
		#if __EDC
	else if(EDC_ON==1)
	{
		lespu1TCSAct=1;
	}
		#endif		
    #if __RDCM_PROTECTION
	else if(ltu1RDCMProtENGSustain1Sec==1)
	{
		lespu1TCSAct=1;
	}    
	#endif									
	else
	{
		lespu1TCSAct=0;
	}

	if((YAW_CDC_WORK==1)||(ESP_TCS_ON==1))
	{
		lespu1VSEAct = 1;
	}
	else
	{
		lespu1VSEAct = 0;
	}

    	#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	if((lsespu1AHBGEN3MpresBrkOn==1)||((fu1BLSErrDet!=1)&&(BLS_EEPROM!=1)&&(BLS==1)))        	
    	#else
	if((MPRESS_BRAKE_ON==1)||((fu1BLSErrDet!=1)&&(BLS_EEPROM!=1)&&(BLS==1)))
		#endif
	{
		lespu1DrvIndpntBrkAppAct = 0;
	}
	else if((BTC_fz==1)||(YAW_CDC_WORK==1)||(ROP_REQ_ACT==1)||(PBA_ON==1))
	{
		lespu1DrvIndpntBrkAppAct = 1;
	}
#if __TSP
	else if((TSP_ON==1)||(TSP_BRAKE_CTRL_ON==1))
	{
		lespu1DrvIndpntBrkAppAct = 1;
	}
#elif __HSA
	else if(HSA_flg==1)
	{
		lespu1DrvIndpntBrkAppAct = 1;
	}
#endif
	else
	{
		lespu1DrvIndpntBrkAppAct = 0;
	}

	lespu1AutoBrkngAct = 0;
	lespu1BrkSysTrnsGrRqTnstnTp = 0;
	
	lespu8WhlSlpSt = 0;
#if __HSA
	if(HSA_flg==1)
	{
		lespu1HlStrAssActIO = 1;
	}
	else
	{
		lespu1HlStrAssActIO = 0;
	}
#else
	lespu1HlStrAssActIO = 0;
#endif	

  if ((FL.MINI_SPARE_WHEEL==1) || (FR.MINI_SPARE_WHEEL==1)
  	|| (RL.MINI_SPARE_WHEEL==1) || (RR.MINI_SPARE_WHEEL==1))
  {
  	lespu8BrkSysVTopSpdLimVal = (uint8_t)S8_Common_Temp5*2; /*Conversion이 2일경우 TP에 25반영시 최종 100(25*2*2)송출*/
  } 	
  else
  {
  	lespu8BrkSysVTopSpdLimVal = 255;
  }

  #if (__CAR==GM_GSUV)	
  if (U8_ESC_LAMP_ON_STRATEGY == 1) /*U8_ESC_LAMP_ON_STRATEGY 의미-->ESC_LAMP_ON_STRATEGY_GM*/
  {
  	tcs_tempW1 = (int16_t)S8_LAMP_Enter_ESP_THR;
  	tcs_tempW2 = (int16_t)S8_LAMP_Enter_ETC_THR;
  	
  	tcs_tempW1 = (int16_t)(((int32_t)drive_torq*tcs_tempW1)/100);
  	tcs_tempW2 = (int16_t)(((int32_t)drive_torq*tcs_tempW2)/100);
  	tcs_tempW3 = drive_torq-cal_torq;
  	

  	if(((ESP_TCS_ON == 1)&&(cal_torq < tcs_tempW1)&&(tcs_tempW3>S16_LAMP_Enter_ESP_DT_THR))
  		||(((ETCS_ON == 1)||(FTCS_ON == 1))&&(cal_torq < tcs_tempW2)&&(tcs_tempW3>S16_LAMP_Enter_ETC_DT_THR ))
  		||(((BTCS_fl == 1)&&(FL.s16_Estimated_Active_Press > MPRESS_10BAR)) 
  		|| ((BTCS_fr == 1)&&(FR.s16_Estimated_Active_Press > MPRESS_10BAR))
  		|| ((BTCS_rl == 1)&&(RL.s16_Estimated_Active_Press > MPRESS_10BAR))
  		|| ((BTCS_rr == 1)&&(RR.s16_Estimated_Active_Press > MPRESS_10BAR)))
  		#if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
		|| ((YAW_CDC_WORK == 1)&&(lsespu1AHBGEN3MpresBrkOn == 0)&&((del_target_slip_fl < (-S16_LAMP_FS_Slip_THR))
		#else
  		|| ((YAW_CDC_WORK == 1)&&(MPRESS_BRAKE_ON == 0)&&((del_target_slip_fl < (-S16_LAMP_FS_Slip_THR))
		#endif
											   		   || (del_target_slip_fr < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_rl < (-S16_LAMP_RS_Slip_THR))
  													   || (del_target_slip_rr < (-S16_LAMP_RS_Slip_THR))))
  		)
  	{
  					lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1;  	
  		}  		  
  	else if((YAW_CDC_WORK==1)&&((Flg_ESC_SW_ABS_OK==1)||(ABS_fz==1))&&((del_target_slip_fl < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_fr < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_rl < (-S16_LAMP_RS_Slip_THR))
  													   || (del_target_slip_rr < (-S16_LAMP_RS_Slip_THR))))
    {
    		lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1;

  	}
  	#if __ESC_SW_OFF_ROP_ACT_ENABLE
  	else if ((Flg_ESC_SW_ROP_OK==1)&&((del_target_slip_fl < -(int16_t)(U16_LAMP_FS_Slip_ESC_OFF_THR))
  													       || (del_target_slip_fr < -(int16_t)(U16_LAMP_FS_Slip_ESC_OFF_THR))))
  	{
  		lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_3; /*1*/
  	}
  	#endif
    #if __TSP	
  	else if((TSP_ON == 1)&&(lcs16TspTargetDecelInit < S16_LAMP_Enter_TSP_THR ))
  	{
  				lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1;
  	}
  	#endif
  	else
  	{
  		lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_2; /*107*/
  	}	
  	
  	if(U8_DRIVE_LINE_PROTECTION==1) /* Drive Line Protection */
  	{
  		if ((fu1TCSDisabledBySW == 1)
		#if __TSP
		&&(TSP_ON==0)
		#endif	
  	  )
  	{
  		TCS_ON = 0;
  	}
  		else{}
  	}
  	else
  	{
  		if ((fu1TCSDisabledBySW == 1)
  		#if __TSP	
  		&&(TSP_ON == 0)
  		#endif
  	 	)
  	 	{                                                          
  	      tcs_tempW2 = (int16_t)(((int32_t)drive_torq*U8_LAMP_Enter_ETC_OFF_THR)/100);
  	 		
  			if (
  				(((ETCS_ON == 1)||(FTCS_ON == 1))&&(cal_torq < tcs_tempW2)&&(tcs_tempW3>(int16_t)U16_LAMP_Enter_ESC_OFF_DT_THR))
  				||(((BTCS_fl == 1)&&(FL.s16_Estimated_Active_Press > MPRESS_10BAR)) 
  				|| ((BTCS_fr == 1)&&(FR.s16_Estimated_Active_Press > MPRESS_10BAR))
  				|| ((BTCS_rl == 1)&&(RL.s16_Estimated_Active_Press > MPRESS_10BAR))
  				|| ((BTCS_rr == 1)&&(RR.s16_Estimated_Active_Press > MPRESS_10BAR)))  	  		
  	 	    )
  	 		{
  	 			lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1; /*21*/
  	 		}
  	 		else{}
  	 	}
  	 	else{}		
  	}

  	  /*Inside Wheel Lift Control시 Lamp Off*/
  	  if((ETCS_ON==0)&&(FTCS_ON==0)
		#if __RDCM_PROTECTION
  	  &&(ltu1RDCMProtENGOn==0)
  	  #endif    	
  	#if __TSP	
  	&&(TSP_ON == 0)
  	#endif
  	  )
  	{
  		TCS_ON = 0;
  	}
  	else
  	{
  		;
  	}
  	/*DriveLine Protection시 Lamp Off*/	  
  }
  else
  {
  	lctcsu8FunctionLampOnTime = K_VDC_Min_Activation_Time;
  }		  
  #endif /*__CAR==GM_GSUV*/		  
}

#elif (__DEMO_VEHICLE)
	#if (__CAR==FORD_C214)
static void LATCS_vConvertTCS2EMSForFORD(void)
{
	if(ETCS_ON||FTCS_ON||ESP_TCS_ON||AY_ON
  #if __ROP
  	  ||ROP_REQ_ENG
  #endif
  #if __TSP  
	  ||(TSP_ON==1)
  #endif
    )
	{
        ConvCaltorq=(int16_t) ( ( (int32_t)(cal_torq/10)*cmu16_TransmissionGearRatio)/100);
	}
	else
	{
		ConvCaltorq=(uint16_t)0xFFFE-10000;
	}
}

	#elif __CAR==CHINA_B12
static void LATCS_vConvertTCS2EMSForB12(void)
{
	if(ETCS_ON || FTCS_ON || ESP_TCS_ON)
	{
    	lespu1TCS_TCS_REQ = 1;
    	lespu1TCS_TCS_CTL = 1;
        tempW0=cal_torq;
        lesps16TCS_TQI_TCS = tempW0;
	}
	else
	{
    	lespu1TCS_TCS_REQ = 0;
    	lespu1TCS_TCS_CTL = 0;
        lesps16TCS_TQI_TCS = 0;
    	if(BTC_fz) lespu1TCS_TCS_CTL = 1;
        cal_torq=drive_torq;
	}

    if(ABS_fz)
    {
        lespu1TCS_ABS_ACT = 1;
    }
    else
    {
        lespu1TCS_ABS_ACT = 0;
    }
  #if __VDC
    if((ESP_TCS_ON)||(YAW_CDC_WORK)||(ETO_SUSTAIN_1SEC))
    {
        lespu1TCS_ESP_CTL = 1;
    }
    else
    {
        lespu1TCS_ESP_CTL = 0;
    }
  #endif
}

	#elif __CAR==BYD_5A
static void LATCS_vConvertTCS2EMSFor5A(void)
{
	#if __TCS_CONV_ABS_TORQ
	LATCS_vConvAbsToCanTorq();
	#endif
	  
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=0;
    }
    else
    {
    	;
    }
  #if __VDC
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)    
    {/* error flag 통합:ESP_ERROR_FLG=>tcs_error_flg*/
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=ESP_TCS_ON=ETO_SUSTAIN_1SEC=0;
    }
    else
    {
    	;
    }
  #endif

  #if __VDC
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	#endif
	#if __AEB
	  ||(lcu1CdmEngReq==1)
	#endif
	#if __ETSC
     ||(letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
    #endif
	)    
    {
		#if __EEC_GEAR_INHIBIT_MODE
		if((ETO_SUSTAIN_1SEC==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
		#else
		if(ETO_SUSTAIN_1SEC==1)
		#endif
		{
		    if((eng_rpm<4000)&&(gs_pos>1))
		    {/* set TCS_GSC , 03SWD , 04SWD */
		    	lespu1TCS_TCS_GSC = 1;   
		    }
		    else
		    {
		    	;
		    }
		}
		else
		{/* reset TCS_GSC*/	
			lespu1TCS_TCS_GSC = 0;                         
		}
  #else
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1))     
    {/* TCS Control*/
        #if __TCS_GEAR_SHIFT_CONTROL
        if(AY_ON)
        {/* set TCS_GSC*/
        	lespu1TCS_TCS_GSC = 1;                    
        }
        else
        {/* reset TCS_GSC*/
        	lespu1TCS_TCS_GSC = 0;                         
        }
        #endif /*__TCS_GEAR_SHIFT_CONTROL*/
  #endif
        lespu1TCS_TCS_REQ = 1;
        lespu1TCS_TCS_CTL = 1;
        
        /*	===============================================================   */
		/* 2005-02-01 '05 SWD TQI_SLOW_TCS 적용(TQI_TCS + 7% 유지)  Added by Baek, Jongtak */
        /* 2006-02-25 '06 China TQI_SLOW_TCS 적용 */
		  #if __TCS_CONV_ABS_TORQ
			tempW1 = lesps16TCS_TQI_TCS;
		    tempW2 = (int16_t)(U8_SLOW_TORQ_DEVIATION*10);
  			if (tempW1<(lesps16EMS_DRIVE_TQI-tempW2))	
  			{
  				tempW1=tempW1+tempW2;	/* TQI_SLOW_TCS */
  			}	
	  		else if (tempW1>lesps16EMS_DRIVE_TQI) 
	  		{
	  			tempW1=tempW1;
	  		}	
		  	else 
		  	{
		  		tempW1=lesps16EMS_DRIVE_TQI;			
		  	}	
		  #else
		    tempW2 = (int16_t)(U8_SLOW_TORQ_DEVIATION*10);
			if (cal_torq<(drive_torq-tempW2))	
			{
				tempW1=cal_torq+tempW2;	/* TQI_SLOW_TCS */
			}	
			else if (cal_torq>drive_torq) 
			{
				tempW1=cal_torq;
			}	
			else 
			{	
				tempW1=drive_torq;
			}	
		  #endif
			if (tempW1<200) tempW1=200;
        	lesps16TCS_TQI_SLOW_TCS=tempW1;                 /* set TQI_SLOW_TCS	*/

	  #if !__TCS_CONV_ABS_TORQ
        tempW0=cal_torq;                            
      #else
     	tempW0=lesps16TCS_TQI_TCS;
      #endif
        lesps16TCS_TQI_TCS = tempW0;                      /* set TQI_TCS*/
    }
    else
    {/* No EMS Control*/
	    lespu1TCS_TCS_REQ = 0;                            /* reset TCS_REQ */
        lespu1TCS_TCS_GSC = 0;                 		      /* reset TCS_GSC */
        lespu1TCS_TCS_CTL = 0;                            /* reset TCS_CTL */
        if(BTC_fz) lespu1TCS_TCS_CTL = 1;                 /* set TCS_CTL */
        lesps16TCS_TQI_TCS = U16_TCS_TQI_DDEFAULT_TORQUE; /* clear TQI_TCS */
        lesps16TCS_TQI_SLOW_TCS = U16_TCS_TQI_DDEFAULT_TORQUE; /* clear TQI_SLOW_TCS */
	  #if !__TCS_CONV_ABS_TORQ
        cal_torq=drive_torq;
      #endif
	}

  #if (__SCC || __AEB) && __TCS_GEAR_SHIFT_CONTROL
	if ( (lcu1SccEngCtrlReq==1) &&  (lcu1SccGearHoldFlg==1) )
	{/* set TCS_GSC */
		lespu1TCS_TCS_GSC = 1;
	}
	else
	{
		;
	}
  #endif	/* #if __SCC || __AEB */    
    
    if(ABS_fz == 1)
    {/* set ABS_ACT*/
        lespu1TCS_ABS_ACT = 1;                              
    }
    else
    {/* reset ABS_ACT*/
        lespu1TCS_ABS_ACT = 0;                              
    }
  #if __VDC
    if((ESP_TCS_ON)||(YAW_CDC_WORK)||(ETO_SUSTAIN_1SEC)
   #if __HDC && __CAR==KMC_HM
    	||(lcu1HdcActiveFlg==1)
   #endif
    )
    {/* set ESP_CTL*/
        lespu1TCS_ESP_CTL = 1;                              
    }
    else
    {/* reset ESP_CTL*/
        lespu1TCS_ESP_CTL = 0;                              
    }

  #endif
}

	#elif (__CAR==PSA_C4) || (__CAR==PSA_C3)
static void LATCS_vConvertTCS2EMSForC4(void)
{
	/* C4는 수동 차량이므로 Gear 제어 명령 0으로 Setting  */
	/*lespu8_ShiftingAuthorization = 0;*/
	#if __EEC_GEAR_INHIBIT_MODE
	if((ETO_SUSTAIN_1SEC==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
	#else
	if(ETO_SUSTAIN_1SEC==1)
	#endif
	{
		if((eng_rpm<(uint16_t)U16_GEAR_SHIFT_UP_RPM)&&(gs_pos>1))
        {
	    	lespu8_ShiftingAuthorization = 2;   
	    }
        else if((eng_rpm>=(uint16_t)U16_GEAR_SHIFT_UP_RPM))
        {
        	lespu8_ShiftingAuthorization = 1;   	    
        }
	    else
	    {
	lespu8_ShiftingAuthorization = 0;
	    }
	}
	else
	{/* reset TCS_GSC*/	
		lespu8_ShiftingAuthorization = 3;
	}	
	
	if( (ETCS_ON==1)||(FTCS_ON==1)||(ESP_TCS_ON==1) )
	{
		/* Cns_cple_dyn_ASR : ASR dynamic torque request */
		lesps16TCS_TQI_TCS = cal_torq;

		/* Cns_cple_stat_ASR : ASR static torque request */
		/* Diesel 이므로 Dynamic/Static Torque Request 동일 */
		lesps16TCS_TQI_SLOW_TCS = lesps16TCS_TQI_TCS;
	
		/* Cns_cple_MSR : ASR torque request */
		/* functionality Check */		
		lesps16TCS_TQI_MSR = 3100 - lesps16TCS_TQI_TCS;
		
		/* Type_pil_cple_ESP : ESP Torque request status */
		lespu8_TorqueRequestStatus = 5;
	}
	#if __EDC
	else if (EDC_ON == 1)
	{
		/* Cns_cple_MSR : ASR torque request */
		lesps16TCS_TQI_MSR = MSR_cal_torq;
		
		/* Cns_cple_dyn_ASR : ASR dynamic torque request */
		/* functionality Check */				
		lesps16TCS_TQI_TCS = 3100 - lesps16TCS_TQI_MSR; /*~lesps16TCS_TQI_MSR;*/
		
		/* Cns_cple_stat_ASR : ASR static torque request */
		/* functionality Check */		
		lesps16TCS_TQI_SLOW_TCS = lesps16TCS_TQI_TCS; /*0xFA0;*/

		/* Type_pil_cple_ESP : ESP Torque request status */
		lespu8_TorqueRequestStatus = 6;		
	}
	#endif /*__EDC*/
	else	/* No Engine Intervention */
	{
		/* Cns_cple_dyn_ASR : ASR dynamic torque request */
		lesps16TCS_TQI_TCS = 0xFA0;

		/* Cns_cple_stat_ASR : ASR static torque request */
		lesps16TCS_TQI_SLOW_TCS = 0xFA0;
	
		/* Cns_cple_MSR : ASR torque request */
		/* functionality Check */		
		lesps16TCS_TQI_MSR = -1000;

		/* Type_pil_cple_ESP : ESP Torque request status */
		lespu8_TorqueRequestStatus = 0;				
	}	

	/* Contact_frein3 : Brake pedal switch */
 	if (BLS==1)
 	{
 		lespu1_BrakePedalSwitch = 1;
 	}
 	else
 	{
 		lespu1_BrakePedalSwitch = 0;
 	}			

	/* Freinage_en_cours : Braking in progress */
	if 		(BLS==1)
	{
		lespu8_BrakingInProgress = 1;
	}
	else
	{
		if ( (BTCS_ON==1)||(YAW_CDC_WORK==1) )
		{
			lespu8_BrakingInProgress = 3;
		}
		else
		{
			lespu8_BrakingInProgress = 0;
		}
	}
	
	/* Regul_ABR : ABS in regulation */
	if (ABS_fz==1)
	{
		lespu1_ReuglABS = 1;
	}
	else
	{
		lespu1_ReuglABS = 0;		
	}

	/* Regul_AFU : AFU in regulation */		
	if ( (PBA_ON==1)||(PBA_WORK==1)||(PBA_ACT==1) )
	{
		lespu1_RegulPBA = 1;
	}
	else
	{
		lespu1_RegulPBA = 0;
	}
	/* Regul_ASR : ASR in regulation */
	if ( ( ETCS_ON==1) || (FTCS_ON==1) || (ESP_TCS_ON==1) )
	{
		lespu1_RegulASR = 1;
	}
	else
	{
		lespu1_RegulASR = 0;		
	}
	
	/* Regul_BASR : BASR in regulation */
	if (BTCS_ON==1)
	{
		lespu1_RegulBASR = 1;
	}
	else
	{
		lespu1_RegulBASR = 0;
	}

	/* Regul_ESP : ASR-ESP in regulation */
	if ( (ETCS_ON==1) || (FTCS_ON==1) || (BTCS_ON==1) || (ESP_TCS_ON==1) || (YAW_CDC_WORK==1) )
	{
		lespu1_RegulESP = 1;
	}
	else
	{
		lespu1_RegulESP = 0;
	}
	
	/* Regul_ESP_seul : ESP in regulation */
	if (YAW_CDC_WORK==1)
	{
		lespu1_RegulESPSeul = 1;
	}
	else
	{
		lespu1_RegulESPSeul = 0;		
	}
	
	/* Regul_MSR : MSR in regulation */
	if (EDC_ON==1)
	{
		lespu1_RegulEDC = 1;
	}
	else
	{
		lespu1_RegulEDC = 0;
	}

	/* Regul_REF : REF in regulation */
	if (EBD_RA==1)
	{
		lespu1_RegulEBD = 1;
	}
	else
	{
		lespu1_RegulEBD = 0;
	}

	/* Req_lampe_regul_ASR_ESP : Request to light ASR-ESP regulation lamp */		
	if (FUNCTION_LAMP_ON==1)
	{
		lespu1_FunctionLamp = 1;
	}
	else
	{
		lespu1_FunctionLamp = 0;		
	}
	
  #if (__EPB_INTERFACE) || (__AVH)
	/* Tx Signal For EPBi, AVH */
	lcanu8LDM_STAT = lcu8epbLdmstate;					/* longitudinal dynamic management state */
	lcanu8ECD_ACT = lcu1EpbActiveFlg;					/* ECD activation signal */
  #endif	/* #if (__EPB_INTERFACE) || (__AVH) */  

  #if __AVH
	/* Tx Signal For AVH */
	lcanu8AVH_STAT = lcu8AvhState;						/* AVH state */
	lcanu8AVH_LAMP = lcu8AvhLampRequest;				/* info lamp request to cluster */
	lcanu8AVH_ALARM = lcu8AvhAlarmRequest;				/* Audio active status lamp request to cluster */
	lcanu8REQ_EPB_ACT = lcu8AvhReqEpbAct;				/* ESC request to EPB */
	lcanu8REQ_EPB_STAT = lcu1AvhReqEpbActInvalid;		/* validity of EPB activation request */
	lcanu8AVH_CLU = lcu8AvhCluMsg;
  #endif	/* __AVH */  
	
#if (__CAR==PSA_C3)
  if (U8_ESC_LAMP_ON_STRATEGY == 1) /*U8_ESC_LAMP_ON_STRATEGY 의미-->ESC_LAMP_ON_STRATEGY_GM*/
  {
  	tcs_tempW1 = (int16_t)S8_LAMP_Enter_ESP_THR;
  	tcs_tempW2 = (int16_t)S8_LAMP_Enter_ETC_THR;
  	
  	tcs_tempW1 = (int16_t)(((int32_t)drive_torq*tcs_tempW1)/100);
  	tcs_tempW2 = (int16_t)(((int32_t)drive_torq*tcs_tempW2)/100);
  	tcs_tempW3 = drive_torq-cal_torq;
  	

  	if(((ESP_TCS_ON == 1)&&(cal_torq < tcs_tempW1)&&(tcs_tempW3>S16_LAMP_Enter_ESP_DT_THR))
  		||(((ETCS_ON == 1)||(FTCS_ON == 1))&&(cal_torq < tcs_tempW2)&&(tcs_tempW3>S16_LAMP_Enter_ETC_DT_THR ))
  		||(((BTCS_fl == 1)&&(FL.s16_Estimated_Active_Press > MPRESS_10BAR)) 
  		|| ((BTCS_fr == 1)&&(FR.s16_Estimated_Active_Press > MPRESS_10BAR))
  		|| ((BTCS_rl == 1)&&(RL.s16_Estimated_Active_Press > MPRESS_10BAR))
  		|| ((BTCS_rr == 1)&&(RR.s16_Estimated_Active_Press > MPRESS_10BAR)))
  		|| ((YAW_CDC_WORK == 1)&&(MPRESS_BRAKE_ON == 0)&&((del_target_slip_fl < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_fr < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_rl < (-S16_LAMP_RS_Slip_THR))
  													   || (del_target_slip_rr < (-S16_LAMP_RS_Slip_THR))))
  		)
  	{
  			lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1;  	
  	}  		  
  	else if((YAW_CDC_WORK==1)&&((Flg_ESC_SW_ABS_OK==1)||(ABS_fz==1))&&((del_target_slip_fl < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_fr < (-S16_LAMP_FS_Slip_THR))
  													   || (del_target_slip_rl < (-S16_LAMP_RS_Slip_THR))
  													   || (del_target_slip_rr < (-S16_LAMP_RS_Slip_THR))))
    {
    		lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_1;

  	}
  	else
  	{
  		lctcsu8FunctionLampOnTime = (uint8_t)K_VDC_Min_Activation_Time_2; /*107*/
  	}	
	

  	  /*Inside Wheel Lift Control시 Lamp Off*/
  	  if((ETCS_ON==0)&&(FTCS_ON==0)  	
  	  )
  	{
  		TCS_ON = 0;
  	}
  	else
  	{
  		;
  	}
  	/*DriveLine Protection시 Lamp Off*/	  
  }
  else
  {
  	lctcsu8FunctionLampOnTime = K_VDC_Min_Activation_Time;
  }		  
  #endif 		
}

	#elif __CAR==PSA_C4_MECU
static void LATCS_vConvertTCS2EMSForC4(void)
{
	/* BOSCH Signal Monitoring */
	/* Cns_cple_dyn_ASR : ASR dynamic torque request */
	lesps16TCS_TQI_TCS = (lesps16TCS_TQI_TCS * 2) - 100;	/* Nm */
	lesps16TCS_TQI_TCS = lesps16TCS_TQI_TCS * 10;			/* Resolution : 0.1 Nm */
	
	/* Cns_cple_stat_ASR : ASR static torque request */
	lesps16TCS_TQI_SLOW_TCS = (lesps16TCS_TQI_SLOW_TCS * 2) - 100;	/* Nm */
	lesps16TCS_TQI_SLOW_TCS = lesps16TCS_TQI_SLOW_TCS * 10;			/* Resolution : 0.1 Nm */		
	
	/* Cns_cple_MSR : ASR torque request */
	lesps16TCS_TQI_MSR = (lesps16TCS_TQI_MSR * 2) - 100;	/* Nm */
	lesps16TCS_TQI_MSR = lesps16TCS_TQI_MSR * 10;			/* Resolution : 0.1 Nm */				
}

	#elif __CAR==DCX_COMPASS
static void LATCS_vConvertTCS2EMSForCOMPASS(void)
{
	/* Gear 제어 명령 Setting  */
	//lespu8ESP_GMIN_ESP = 0;  /*B/M후 작업 필요*/
	//lespu8ESP_GMAX_ESP = 0;
	/*
	MTGL_ESP, MPAR_ESP F/W 작업 필요
	*/

	/* ABS in regulation */
	if (ABS_fz==1)
	{
		lespu1BS_ABS_BrkEvt = 1;
		lespu1BS_FullBrk_Actv1 = 1;
		lespu1TCS_MSR_C_REQ = 0;
		lespu1TCS_TCS_REQ = 0;
		lesps16TCS_TQI_ESP = (lesps16EMS_TQFR);
	}
	else
	{
		lespu1BS_ABS_BrkEvt = 0;
		lespu1BS_FullBrk_Actv1 = 0;
		lespu1TCS_MSR_C_REQ = 0;
		lespu1TCS_TCS_REQ = 0;
		lesps16TCS_TQI_ESP = (lesps16EMS_TQFR);
	}

	if( (ETCS_ON==1)||(FTCS_ON==1)||(ESP_TCS_ON==1) )  /* Engine torque request (reducing torque) */
	{
		lespu1TCS_MSR_C_REQ = 0;
		lespu1TCS_TCS_REQ = 1;
		lesps16TCS_TQI_ESP = (cal_torq / 4);   /* 확인필요 : cal_torq[Nm]=cal_torq[%]*max_torq/1000 */
		lesps16TCS_TQI_ESP = LCTCS_s16ILimitRange( lesps16TCS_TQI_ESP, lesps16EMS_TQFR, lesps16EMS_TQI );
		lespu8BS_SLV_ESP = 1;
    }
#if __EDC
	else if (EDC_ON == 1)	/* Engine torque request (increasing torque) */
	{
		lespu1TCS_MSR_C_REQ = 1;
		lespu1TCS_TCS_REQ = 0;
		lesps16TCS_TQI_ESP = (MSR_cal_torq / 4);   /* 확인필요 : MSR_cal_torq[Nm]=MSR_cal_torq[%]*max_torq/1000 */
		lesps16TCS_TQI_ESP = LCTCS_s16ILimitRange( lesps16TCS_TQI_ESP, lesps16EMS_TQFR, lesps16EMS_TQI );
		lespu8BS_SLV_ESP = 0;
	}
#endif
	else	
	{/* No Engine Intervention */
		lespu1TCS_MSR_C_REQ = 0;
		lespu1TCS_TCS_REQ = 0;
		lesps16TCS_TQI_ESP = (lesps16EMS_TQFR);
		lespu8BS_SLV_ESP = 0;
	}

	/* Brake pedal switch */
 	if (BLS==1)
 	{
 		lespu8BS_ESP_BRK_SW = 1;
 	}
 	else
 	{
 		lespu8BS_ESP_BRK_SW = 0;
 	}

	/* PBA in regulation */
	if ( (PBA_ON==1)||(PBA_WORK==1)||(PBA_ACT==1) )
	{
		lespu1BS_BA_ACTIVE = 1;
	}
	else
	{
		lespu1BS_BA_ACTIVE = 0;
	}

	/* Brake intervention only */
	if ((BTCS_ON==1) || (YAW_CDC_WORK==1))
	{
		lespu1BS_BRK_ACTIVE = 1;
		lespu8BS_SLV_ESP = 2;
	}
	else
	{
		lespu1BS_BRK_ACTIVE = 0;
		lespu8BS_SLV_ESP = 0;
	}

	/* Req_lampe_regul_ASR_ESP : Request to light ASR-ESP regulation lamp */
	if (FUNCTION_LAMP_ON==1)
	{
		lespu1ESP_ESP_INFO_BL = 1;
		lespu1BS_TM_AUS = 1;
	}
	else
	{
		lespu1ESP_ESP_INFO_BL = 0;
		lespu1BS_TM_AUS = 0;
	}
}

	#elif __CAR==DCX_COMPASS_MECU
static void LATCS_vConvertTCS2EMSForCOMPASS(void)
{
	/* TEVES Signal Monitoring */
	cal_torq = (lesps16TCS_TQI_ESP); /* Nm->% */
	cal_torq = (cal_torq * 4);
												   /* Resolution : 0.1 % */
}

	#elif __CAR==RSM_QM5
static void LATCS_vConvertTCS2EMSForQM5(void)
{
	lespu8ESP_EARLY_GSUP = 0;	/* 0h = no shift up request ,  1h = shift up request */
	
    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	#endif
	)    
    {
	  	#if __EEC_GEAR_INHIBIT_MODE
	  	if((ETO_SUSTAIN_1SEC==1)||(EEC_GEAR_INHIBIT_REQUEST==1))
	  	#else
	    if(ETO_SUSTAIN_1SEC==1)
	    #endif
        {
            if((eng_rpm<4000)&&(gs_pos>1))
            {
            	lespu8ESP_SHIFT_AUTHOR = 0;    /* set TCS_GSC , 03SWD , 04SWD */
            	/* 
	   				0 = gear shift forbidden
	   				1 = shift down authorized
	   				2 = shift up authorized
	   				3 = shift up or down authorized
				*/
            }
            else
            {
            	;
            }
        }
        else
        {
        	lespu8ESP_SHIFT_AUTHOR = 3;                         /* reset TCS_GSC*/
        }
        
		if((eng_rpm>=U16_GEAR_SHIFT_UP_RPM))
        {
        	lespu8ESP_SHIFT_AUTHOR = 3;
        	lespu8ESP_EARLY_GSUP = 1;
        }        
		else
		{
			lespu8ESP_EARLY_GSUP = 0;
		}
				      
        lespu1TCS_ENG_CTL_MAP = 1;
        lespu1TCS_TCS_REQ = 1;
        lespu1TCS_TCS_CTL = 1;                                     /* set TCS_CTL*/        
        lespu1TCS_MSR_CTL = 0;
        lesps16TCS_TQI_TCS = (uint16_t)((int32_t)cal_torq+4000)/5; /* set TQI_TCS*/
        lesps16TCS_TQI_TCS = LCTCS_s16ILimitMaximum(lesps16TCS_TQI_TCS, 5000);
        lesps16TCS_TQI_MSR = ~lesps16TCS_TQI_TCS;
        lespu1TCS_MSR_C_REQ = 0;	        	    /* reset MSR_C_REQ */
    }
    else if(EDC_ON == 1)
    {
	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
	    lespu8ESP_SHIFT_AUTHOR = 3;
        lespu1TCS_MSR_C_REQ = 1;     				/* reset MSR_C_REQ */
        lespu1TCS_MSR_CTL = 1;        
        lespu1TCS_TCS_CTL = 0;                      /* reset TCS_CTL */
        if(BTC_fz) lespu1TCS_TCS_CTL = 1;           /* set TCS_CTL */
        lesps16TCS_TQI_MSR = (uint16_t)((int32_t)MSR_cal_torq+4000)/5;/* set TQI_TCS */
        lesps16TCS_TQI_MSR = LCTCS_s16ILimitMaximum(lesps16TCS_TQI_MSR, 5000);        	
        lesps16TCS_TQI_TCS = ~lesps16TCS_TQI_MSR;   /* clear TQI_TCS */
        lespu1TCS_ENG_CTL_MAP = 1;
    }    
    else
    {/* No EMS Control*/
	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
	    lespu8ESP_SHIFT_AUTHOR = 3;
        lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */
        lespu1TCS_TCS_CTL = 0;                      /* reset TCS_CTL		  */
		lespu1TCS_MSR_CTL = 0;         
        if(BTC_fz) lespu1TCS_TCS_CTL = 1;           /* set TCS_CTL		      */
        lesps16TCS_TQI_TCS = 0xFFE;                 /* clear TQI_TCS		  */
        lesps16TCS_TQI_MSR = 0;                     /* clear TQI_MSR		  */
        cal_torq=drive_torq;
      	lespu1TCS_ENG_CTL_MAP = 0;
	}        	
	
	if ( (BTCS_ON==1)||(YAW_CDC_WORK==1) )
	{
		lespu1TCS_ESP_CTL = 1;
	}
	else
	{
		lespu1TCS_ESP_CTL = 0;
	}
	
	/* Regul_ABR : ABS in regulation */
	if (ABS_fz==1)
	{
		lespu1TCS_ABS_ACT = 1;
	}
	else
	{
		lespu1TCS_ABS_ACT = 0;		
	}

	if (EBD_RA==1)
	{
		lespu1TCS_EBD_CTL	 = 1;
	}
	else
	{
		lespu1TCS_EBD_CTL = 0;
	}
}

	#elif __CAR==BMW740i
static void LATCS_vConvertTCS2EMSForBMW740i(void)
{
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=0;
    }
    else
    {
    	;
    }
  #if __VDC
    if(lctcss16EngCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
    {/* error flag 통합:ESP_ERROR_FLG=>tcs_error_flg*/
        ETCS_ON=FTCS_ON=AY_ON=TRACE_ON=DRAG_ON=ESP_TCS_ON=ETO_SUSTAIN_1SEC=0;
    }
    else
    {
    	;
    }
  #endif

    if((ETCS_ON==1)||(FTCS_ON==1)||(AY_ON==1)||(ESP_TCS_ON==1)
    #if __ROP
      ||(ROP_REQ_ENG==1)
    #endif
    #if __TSP
      ||(TSP_ON==1)	
	  #endif
	)    
    {
        lespu1TCS_TCS_REQ = 1;                              /* set TCS_REQ*/
        lespu1TCS_TCS_CTL = 1;                              /* set TCS_CTL*/

        tempW0 = cal_torq;
        if(tempW0 > MAX_TORQUE)
        {
             tempW0 = MAX_TORQUE;
        }
        else
        {
             ;
        } 
       
        lesps16TCS_TQI_TCS = tempW0;                  /* set TQI_TCS */
        lesps16TCS_TQI_TCS = lesps16TCS_TQI_TCS;      /* set TQI_TCS */
        lesps16TCS_TQI_MSR = 0;		                  /* clear TQI_MSR */
        lespu1TCS_MSR_C_REQ = 0;	        		  /* reset MSR_C_REQ */
    }
    else
    { 										          /* No EMS Control */
	    lespu1TCS_TCS_REQ = 0;                        /* reset TCS_REQ */
        lespu1TCS_MSR_C_REQ = 0;     				  /* reset MSR_C_REQ */
        lespu1TCS_TCS_CTL = 0;                        /* reset TCS_CTL */
        if(BTC_fz) lespu1TCS_TCS_CTL = 1;             /* set TCS_CTL */
        lesps16TCS_TQI_TCS = 0xFFFF;                  /* clear TQI_TCS */
        lesps16TCS_TQI_MSR = 0;                       /* clear TQI_MSR */
        lesps16TCS_TQI_SLOW_TCS = 0xFFFF;             /* clear TQI_SLOW_TCS */
	    #if !__TCS_CONV_ABS_TORQ
        cal_torq=drive_torq;
      #endif
	}
    
    if(ABS_fz)
    {
        lespu1TCS_ABS_ACT = 1;                              /* set ABS_ACT*/
    }
    else
    {
        lespu1TCS_ABS_ACT = 0;                              /* reset ABS_ACT*/
    }
  #if __VDC
    if((ESP_TCS_ON)||(YAW_CDC_WORK)||(ETO_SUSTAIN_1SEC))
    {
        lespu1TCS_ESP_CTL = 1;                              /* set ESP_CTL*/
    }
    else
    {
        lespu1TCS_ESP_CTL = 0;                              /* reset ESP_CTL*/
    }
  #endif
}
#elif __CAR==VV_V40
static void LATCS_vConvertTCS2EMSForV40(void)
{

	/*BCMText*//* 0으로 고정*/
/*	
	tempW0 = 0;
	
	if(EBD_RA == 0)
	{
		tempW0 = tempW0 + 1;
	}
	else
	{
		tempW0 = tempW0 + 0;
	}

	if(ABS_fz == 0)
	{
		tempW0 = tempW0 + 2;
	}
	else
	{
		tempW0 = tempW0 + 0;
	}

	tempW1 = 0;

	if((TCS_ON == 1) || ((ESP_TCS_ON == 1)||(YAW_CDC_WORK == 1)||(ETO_SUSTAIN_1SEC == 1)))
	{
		tempW1 = 0;
	}
	else
	{
		tempW1 = 12;

		if(ESP_ERROR_FLG == 1)
		{
			tempW1 = 4;
		}
		else
		{;}

		if(((TMP_ERROR_fl==1)||(TMP_ERROR_fr==1)||(TMP_ERROR_rl==1)||(TMP_ERROR_rr==1))&&(TCS_ON==0))
		{
			tempW1 = 8;
		}
		else
		{;}
	}

	tempW0 = tempW0 + tempW1;

	if(fu1TCSDisabledBySW == 1)
	{
		tempW0 = tempW0 + 16;
	}
	else
	{;}
*/
	tempW0 = 0;
	lespu8BCM_TEXT = tempW0;	


	/*AYCMode*/ /* ok */
	if((ESP_TCS_ON)||(YAW_CDC_WORK)/*||(ETO_SUSTAIN_1SEC)*/)
	{
		lespu1TCS_ESP_CTL = 1;                              /* set ESP_CTL*/
	}
	else
	{
		lespu1TCS_ESP_CTL = 0;                              /* reset ESP_CTL*/
	}

	/*ABSMode */  /* ok */
	if(ABS_fz)
	{
		lespu1TCS_ABS_ACT = 1;                              /* set ABS_ACT*/
	}
	else
	{
		lespu1TCS_ABS_ACT = 0;                              /* reset ABS_ACT*/
	}

	if((ETCS_ON==1) || (FTCS_ON==1) || (ESP_TCS_ON==1))
	{
	/* PropulsionTrqReqMax*/ /*자동 변속 중간에는 선형적으로 나옴 자동 변속기 : TrqRatio 수동변속기 TrqRatioEst */
	lesps16TCS_TQI_TCS = (cal_torq / 10);
	lesps16TCU_CUR_G_RATIO = LCTCS_s16ILimitMinimum(lesps16TCU_CUR_G_RATIO, 10);
	lesps16TCS_TQI_TCS = (int16_t)(((int32_t)lesps16TCS_TQI_TCS*lesps16TCU_CUR_G_RATIO)/10);
	}
	else
	{
		lesps16TCS_TQI_TCS = 0x4000;
	}
	/*	SCMode , TCMode */ /* ok */
	if((ETCS_ON==1) || (FTCS_ON==1) || (ESP_TCS_ON==1))
	{
		lespu1TCS_ETCS_CTL = 1;
		lespu1TCS_TCS_REQ = 1;
	}
	else
	{
		lespu1TCS_ETCS_CTL = 0;
		lespu1TCS_TCS_REQ = 0;
	}

	if(BTC_fz == 1)
	{
		lespu1TCS_BTCS_CTL = 1;
	}
	else
	{
		lespu1TCS_BTCS_CTL = 0;
	}	

	/*DSRReqSplitMuFlag*/	 /* ABS 제어에서만 들어옴*/
	if((LFC_Split_flag== 1)
		/*||(FL.lctcsu1SymSpinDctFlg==0)||(FR.lctcsu1SymSpinDctFlg==0)||(RL.lctcsu1SymSpinDctFlg==0)||(RR.lctcsu1SymSpinDctFlg==0))
	    ||(ESP_SPIT == 1 )*/)	
	{
		lespu1DSR_SPLIT_CTL = 1;
	}
	else
	{
		lespu1DSR_SPLIT_CTL = 0;
	}
	
#if __EDC
	if(EDC_ON)
	{
		lespu1TCS_MSR_C_REQ=1;			/* MSRMode*//* ok */
		lesps16TCS_TQI_MSR = (MSR_cal_torq / 10) ;
		lesps16TCU_CUR_G_RATIO = LCTCS_s16ILimitMinimum(lesps16TCU_CUR_G_RATIO, 10);	
		lesps16TCS_TQI_MSR = (int16_t)(((int32_t)lesps16TCS_TQI_MSR*lesps16TCU_CUR_G_RATIO)/10); /*PropulsionTrqReqMin *//* ok */
	}
	else
	{
		lespu1TCS_MSR_C_REQ=0;
		lesps16TCS_TQI_MSR=-(0x3FF8);
	}
#endif

	/*DSRReqOversteerFlag */  /* ESP file 에서 set*/

	/*DSRReqUndersteerFlag */ /*ESP file 에서 set*/
	
	/*LongAccOverGround*/
	//lesps16ActVehAccel_0 = (int16_t)((((int32_t)lsabss16DecelRefiltByVref5/100)+18)*1000/35);
	lesps16ActVehAccel_0 = lsabss16DecelRefiltByVref5;
	/*TotWheelBrakeTrq*/
	//lcans16CSTBATS_TrqVl = (lds16TotalBrktorq/10);
	lcans16CSTBATS_TrqVl = lds16TotalBrktorq;
	
	/*MaxWheelAccelerationFr */
	//lsu8maxwheelaccel = (lsu8maxwheelaccel/36);	
	
	/*UnderSteer!!!!ldespu1UnderSteerStatus로직상 set 됨  완료!!!*/
	/*OverSteer!!!!ldespu1OverSteerStatus 로직상 set 됨 완료!!!*/
	/*ACCBrakeStatus !!!!! 무시 가능!!!!!*/
	/*ACCBrakeStopModeStatu !!!!!!무시 가능!!!!!*/

	/*HLAStatusChime!!!!!!!!!!O으로 송출*/
	/*HLAStatusMsg!!!!!!!!!!O으로 송출*/
	/*EmergencyBrakingEvent !!!!!!!!!fcu2ESSSystemStatus*/
	
	/* STEER ANGLE SIGN 반영해야함 LEFT : -  RIGHT : + */
	

}

#elif __CAR==FIAT_PUNTO
static void LATCS_vConvertTCS2EMSForPUNTO(void)
{
	
	#if __TCS_CONV_ABS_TORQ
	LATCS_vConvAbsToCanTorq();
	#endif
	
	if((ESP_TCS_ON)||(YAW_CDC_WORK)/*||(ETO_SUSTAIN_1SEC)*/)
	{
		lespu1TCS_ESP_CTL = 1;                              /* set ESCActive*/
	}
	else
	{
		lespu1TCS_ESP_CTL = 0;                              /* reset ESCActive*/
	}
	
	if(ABS_fz)
	{
		lespu1TCS_ABS_ACT = 1;                              /* set ABS_ACT*/
	}
	else
	{
		lespu1TCS_ABS_ACT = 0;                              /* reset ABS_ACT*/
	}
	
	if((lctcsu1EngCtrlInhibit==1) || (lctcsu1BrkCtrlInhibit==1))
	{
		lespu1TCS_INHIBIT_ACT = 1;
	}
	else
	{
		lespu1TCS_INHIBIT_ACT = 0;
	}
	
	if((ETCS_ON==1) || (FTCS_ON==1) || (ESP_TCS_ON==1))
	{
		lespu1TCS_TCS_REQ = 1;
	}
	else
	{
		lespu1TCS_TCS_REQ = 0;
	}
	if((ETCS_ON==1) || (FTCS_ON==1) || (ESP_TCS_ON==1))
	{
	
		lesps16TCS_TQI_SLOW_TCS = lesps16TCS_TQI_TCS;
		//lesps16TCS_TQI_TCS = cal_torq;
	}
	else
	{
		lesps16TCS_TQI_TCS = 0xFF*4;
		lesps16TCS_TQI_SLOW_TCS =0xFF*4; 
	}
	
	#if __EDC
	if(EDC_ON)
	{
		lespu1TCS_MSR_C_REQ=1;			
	}
	else
	{
		lespu1TCS_MSR_C_REQ=0;
	}
	#endif
	

}

	#endif /*__DEMO_VEHICLE*/
#endif

static void LATCS_vConvertVafs2EMS(void)
{
  	#if (__EPB_INTERFACE) || (__AVH)
	/* Tx Signal For EPBi, AVH */
	lcanu8LDM_STAT = lcu8epbLdmstate;					/* longitudinal dynamic management state */
	lcanu8ECD_ACT = lcu1EpbActiveFlg;					/* ECD activation signal */
  	#endif	/* #if (__EPB_INTERFACE) || (__AVH) */  

  	#if __AVH
	/* Tx Signal For AVH */
	lcanu8AVH_STAT = lcu8AvhState;						/* AVH state */
	lcanu8AVH_LAMP = lcu8AvhLampRequest;				/* info lamp request to cluster */
	lcanu8AVH_ALARM = lcu8AvhAlarmRequest;				/* Audio active status lamp request to cluster */
	lcanu8REQ_EPB_ACT = lcu8AvhReqEpbAct;				/* ESC request to EPB */
	lcanu8REQ_EPB_STAT = lcu1AvhReqEpbActInvalid;		/* validity of EPB activation request */
	lcanu8AVH_CLU = lcu8AvhCluMsg;
  	#endif	/* __AVH */  
  
  		#if defined(HMC_CAN)
  	#if __ACC
  	lcanu1CF_Esc_BrkCtl = lcu1AccActiveFlg;
  	#endif	/* __ACC */	
  		#endif /*defined(HMC_CAN)*/
}	
    #endif  /* TAG1 */
#endif /*TAG4*/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LATCSCallActCan
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
