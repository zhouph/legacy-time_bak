#ifndef __LAESPCALLACTHW_H__
#define __LAESPCALLACTHW_H__
/*Includes *********************************************************************/
#include "LVarHead.h"
//#include "LCESPCallControl.h"

/*Global MACRO CONSTANT Definition *********************************************/
#define   __ESC_LFC_DUTY_COMP   DISABLE   

#define   ESV_FADE_CTRL_IN_TIME    L_U8_TIME_10MSLOOP_1000MS       /*UCHAR*/
#define   ESV_FADE_CTRL_INI_DUTY   20         /*UCHAR*/

/*AHB GEN3 REAR CONTROL*/
#define lcespu1AHB_Rear_Control                VDCF33.bit0
#define lcespu1AHB_Rear_ControlOld             VDCF33.bit1
#define lcespu1AHB_Rear_ControlInhibitFlag     VDCF33.bit2
#define lcespu1AHB_InitialPhase                VDCF33.bit3
#define lcespu1AHB_ControlPhase                VDCF33.bit4
#define lcespu1AHB_FadeOutPhase                VDCF33.bit5

/***********************/


   #if (__CAR == GM_J300)
#define S16_ESC_LFC_RISE_DUTY_FRONT         80
#define S16_ESC_LFC_RISE_DUTY_REAR          87
#define S16_ESC_LFC_RISE_DUTY_FRONT_ABS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_ABS      80
#define S16_ESC_LFC_RISE_DUTY_FRONT_CBS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_CBS      80
   #elif (__CAR == GM_MALIBU)
#define S16_ESC_LFC_RISE_DUTY_FRONT         80
#define S16_ESC_LFC_RISE_DUTY_REAR          87
#define S16_ESC_LFC_RISE_DUTY_FRONT_ABS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_ABS      80
#define S16_ESC_LFC_RISE_DUTY_FRONT_CBS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_CBS      80
   #elif (__CAR == GM_LAMBDA)
#define S16_ESC_LFC_RISE_DUTY_FRONT         80
#define S16_ESC_LFC_RISE_DUTY_REAR          100
#define S16_ESC_LFC_RISE_DUTY_FRONT_ABS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_ABS      80
#define S16_ESC_LFC_RISE_DUTY_FRONT_CBS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_CBS      80
   #elif (__CAR == HMC_BH)
#define S16_ESC_LFC_RISE_DUTY_FRONT         80
#define S16_ESC_LFC_RISE_DUTY_REAR          87
#define S16_ESC_LFC_RISE_DUTY_FRONT_ABS     90
#define S16_ESC_LFC_RISE_DUTY_REAR_ABS      90
#define S16_ESC_LFC_RISE_DUTY_FRONT_CBS     90
#define S16_ESC_LFC_RISE_DUTY_REAR_CBS      90
   #else
#define S16_ESC_LFC_RISE_DUTY_FRONT         80
#define S16_ESC_LFC_RISE_DUTY_REAR          87
#define S16_ESC_LFC_RISE_DUTY_FRONT_ABS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_ABS      80
#define S16_ESC_LFC_RISE_DUTY_FRONT_CBS     80
#define S16_ESC_LFC_RISE_DUTY_REAR_CBS      80
   #endif

  
/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern uint8_t lau8_S_VALVE_PRIMARY_in_cnt;
extern int16_t vdc_msc_target_vol;
extern int16_t lcesps16OS2TarVol;


  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
extern int16_t laesps16PriTcTargetPress, laesps16SecTcTargetPress;
  #endif /* __AHB_GEN3_SYSTEM == ENABLE */

/*Global Extern Functions Declaration ******************************************/
extern void	LCMSC_vSetVDCTargetVoltage(void);
extern void LAESP_vGenerateLFCDuty(void);

   #if (__TCMF_UNDERSTEER_CONTROL == 1)
extern int16_t LAMFC_s16SetMFCCurrentESP(uint8_t CIRCUIT, int16_t MFC_Current_old);
  #endif
   
#endif
