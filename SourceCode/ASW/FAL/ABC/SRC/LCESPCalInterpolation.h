#ifndef __LCESPCALINTERPOLATION_H__
#define __LCESPCALINTERPOLATION_H__
/*Includes *********************************************************************/
#include "LVarHead.h"
 
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/
extern int16_t LCESP_s16IInter2Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2 );
extern int16_t LCESP_s16IInter3Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3 );
extern int16_t LCESP_s16IInter4Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4 );
extern int16_t LCESP_s16IInter5Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5 );
extern int16_t LCESP_s16IInter6Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6 );
extern int16_t LCESP_s16IInter7Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6, int16_t x_7, int16_t y_7 );
#endif
