/*******************************************************************
* Project Name: 
* File Name: 
* Description: 
********************************************************************/
/* Includes ********************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallVrefEst
	#include "Mdyn_autosar.h"
#endif


#include 	"LDRTACallDetection.h"
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#include 	"LCABSDecideCtrlState.h"
#if __VDC
#include 	"LDABSCallVrefCompForESP.H"


#endif

#include 	"LDABSDctRoadGradient.h"
#include 	"LSABSEstLongAccSensorOffsetMon.h"
#include 	"LSCallLogData.h"
#include 	"LCABSCallControl.h"

#include "LCallMain.h"
/*******************************************************************/


/* Local Definition  ***********************************************/

#define INSIDE_ABS_INC_LIMIT_MAX        32
#define INSIDE_ABS_DEC_LIMIT_MAX        -32
#define OUTSIDE_ABS_INC_LIMIT_MAX       16
#define OUTSIDE_ABS_DEC_LIMIT_MAX       -16

/*******************************************************************/

/* Variables Definition*********************************************/
/* LDABS_vImplementVref */
  	int16_t 	vref_resol_change; 	
 	int16_t 	vref_alt_resol_change;
  


/* LDABS_vCallVrefEst*/
/* LDABS_vLimitationVrefMinMax */
/* LDABS_vCal4WDVref */
	int16_t vref4;
/* LDABS_vLimitationVrefByLongAcc*/
	int16_t		count_ice_up;
	int16_t   	along_quo, along_mod;
/* LDABS_vEstEachWheelSlip */
	uint8_t	inst_zeit;
	
U8_BIT_STRUCT_t VREFMF0;

/*******************************************************************/

/* Local Function prototype ****************************************/
void	LDABS_vImplementVref(void);
void	LDABS_vCallVrefEst(void);
void	LDABS_vLimitationVrefMinMax(void);
  #if ((__4WD_VARIANT_CODE==ENABLE)||(__4WD))&&(__AX_SENSOR)
void 	LDABS_vCal4WDVref(int16_t voptfz_by_whl, int16_t voptfz_by_G, int16_t speed_old);
  #endif

void    LDABS_vEstEachWheelSlip(struct W_STRUCT *WL_temp);
void	LDABS_vEstWheelSlip(void);
#if __VDC
void	LSABS_vCallVrefLogData(void);
#endif
/*******************************************************************/
void	LDABS_vCallVrefEst(void)
{
	
	LDABS_vCallDctForVref();
	LDABS_vCallVrefMainFilterprocess();
	LDABS_vImplementVref();
	
	#if __VDC
	LDABS_vCallVrefCompForESP();
	#endif
	
	
	LDABS_vLimitationVrefMinMax();
	
	LDABS_vCallEstVehDecel();

    if(speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS) 
    {              
        voptfz1=voptfz_mittel1=vref=vrselect1;              
 		voptfz2=voptfz_mittel2=vref2=vrselect2;

        vref4=FZ2.vrselect_resol_change;

        filter_out1=filter_out2=voptfz_rest1=voptfz_rest2=0;
        FZ1.voptfz_resol_change=FZ1.voptfz_mittel_resol_change=vref_resol_change=FZ1.vrselect_resol_change;
        FZ2.voptfz_resol_change=FZ2.voptfz_mittel_resol_change=FZ2.vrselect_resol_change;
        FZ1.filter_out_resol_change=FZ2.filter_out_resol_change=0;

		
		#if ((__4WD_VARIANT_CODE==ENABLE)||__4WD) &&(__AX_SENSOR==1)
		
		FZ_for_4WD.voptfz_resol_change=FZ1.vrselect_resol_change;
		FZ_for_4WD.voptfz_mittel_resol_change=FZ1.vrselect_resol_change;
		FZ_for_4WD.filter_out_resol_change=0;	

		FZ_for_4WD.voptfz=FZ1.vrselect;
		FZ_for_4WD.voptfz_mittel=FZ1.vrselect;
		FZ_for_4WD.filter_out=0;
		FZ_for_4WD.voptfz_rest=0;			
		#endif		
		
		#if __VDC
		vref5_resol_change=vref_resol_change;	
        vref5=vref;
        #endif
    } 
    else 
    {
        ;
    } 
     
   	if((RE_ABS_flag==1) 
    #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
    ||(FREE_ROLLING_DETECT==1)
 	#endif
 	) 
 	{    
 		tempW0=vrselect1;
 		tempW1=vrselect2;
		voptfz1=voptfz_mittel1=vref=tempW0;
		voptfz2=voptfz_mittel2=vref2=tempW1;
		filter_out1=filter_out2=voptfz_rest1=voptfz_rest2=0;

		FZ1.voptfz_resol_change=(tempW0*8);
		FZ1.voptfz_mittel_resol_change=(tempW0*8);
		vref_resol_change=(tempW0*8);
		FZ2.voptfz_resol_change=(tempW1*8);
		FZ2.voptfz_mittel_resol_change=(tempW1*8);
		FZ1.filter_out_resol_change=0;
		FZ2.filter_out_resol_change=0;
		    #if ((__4WD_VARIANT_CODE==ENABLE)||__4WD)&&(__AX_SENSOR==1)
		FZ_for_4WD.voptfz_resol_change=FZ1.voptfz_resol_change;
		FZ_for_4WD.voptfz_mittel_resol_change=FZ1.voptfz_mittel_resol_change;
		FZ_for_4WD.filter_out_resol_change=0;
		    #endif

		
		#if ((__4WD_VARIANT_CODE==ENABLE)||__4WD)&&(__AX_SENSOR==1)
		FZ_for_4WD.voptfz=FZ1.voptfz;
		FZ_for_4WD.voptfz_mittel=FZ1.voptfz_mittel;
		FZ_for_4WD.filter_out=0;		
		#endif
	}   
   
}
/*******************************************************************/

void	LDABS_vImplementVref(void)
{
	vref_alt=vref;
   	vref_alt_resol_change=vref_resol_change;


/*---------------------------------------------------------------------
	vref, vref2, vref4        calc                                      
---------------------------------------------------------------------*/
#if __4WD_VARIANT_CODE==ENABLE

#if __AX_SENSOR
    if( lsu8DrvMode != DM_2WD )
    {

    	LDABS_vCal4WDVref(FZ1.voptfz_resol_change,FZ_for_4WD.voptfz_resol_change,vref_alt_resol_change);
    	vref_resol_change=vref4;

/*    	if((BRAKE_SIGNAL)||(ABS_fz))*/
		if((BRAKING_STATE==1)&&(vref_resol_change>ABS_entry_speed))
    	{
    		#if __VDC
    		  #if __BRK_SIG_MPS_TO_PEDAL_SEN  ==1
    		if((lsesps16EstBrkPressByBrkPedalF>MPRESS_15BAR)&&(mtp<5)&&(ldabsu1CanErrFlg==0)&&(lsespu1BrkAppSenInvalid==0))
    		    		  
    		  #else
    		if((mpress>MPRESS_15BAR)&&(mtp<5)&&(ldabsu1CanErrFlg==0)&&(fu1MCPSusDet==0)&&(fu1MCPErrorDet==0))
    		
    		  #endif
    		{	
    		#endif
    			vref_resol_change=ABS_entry_speed;
    		#if __VDC
    		}
    		#endif
    		
        }
        else 
        {
            ;
        }

        vref=vref2=(vref_resol_change>>3);


    }
    else 
    {
#endif    	
      	vref_resol_change=FZ1.voptfz_resol_change;
        vref=voptfz1;
        vref2=voptfz2;
#if __AX_SENSOR    
    }
#endif
#else	
  #if	__4WD && (__AX_SENSOR==1)
	LDABS_vCal4WDVref(FZ1.voptfz_resol_change,FZ_for_4WD.voptfz_resol_change,vref_alt_resol_change);
	vref_resol_change=vref4;
/*    	if((BRAKE_SIGNAL)||(ABS_fz))*/
		if((BRAKING_STATE==1)&&(vref_resol_change>ABS_entry_speed))
    	{
    		#if __VDC
    		  #if __BRK_SIG_MPS_TO_PEDAL_SEN  ==1
    		if((lsesps16EstBrkPressByBrkPedalF>MPRESS_15BAR)&&(mtp<5)&&(ldabsu1CanErrFlg==0)&&(lsespu1BrkAppSenInvalid==0))
  		  
    		  #else
    		if((mpress>MPRESS_15BAR)&&(mtp<5)&&(ldabsu1CanErrFlg==0)&&(fu1MCPSusDet==0)&&(fu1MCPErrorDet==0))

    		  #endif	
    		  {
    		#endif
    			vref_resol_change=ABS_entry_speed;
    		#if __VDC
    		}
    		#endif
    		
        }
        else 
        {
            ;
        }
    vref=vref2=(vref_resol_change>>3);


  #else /*2WD*/
  	vref_resol_change=FZ1.voptfz_resol_change;
    vref=voptfz1;
    vref2=voptfz2;
  #endif
#endif  	
    if(ABS_fz == 1) 
    {
        if(vref2 > vref) 
        {
            vref2=vref;
        }
        else 
        {
            ;
        }
    }
    else 
    {
        ;
    }
}
/*******************************************************************/
#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)

void LDABS_vCal4WDVref(int16_t voptfz_by_whl, int16_t voptfz_by_G, int16_t speed_old)
{
	int16_t vref_inc_limit;
	int16_t vref_dec_limit;
	
    if((!USE_ALONG)&&(speed_old>voptfz_by_whl)) 
    {                                       
        vref4=voptfz_by_whl;
    }
    else 
    {
    	vref4=voptfz_by_G;

		#if __SPIN_DET_IMPROVE == ENABLE
		if((ldu1vehSpinDetOld == 0)&&(ldu1vehSpinDet == 1))
		{
			vref4 = AX_1_P.lds16VspeedByAxSen;
			speed_old = AX_1_P.lds16VspeedByAxSenOld;
		}
		else if((ldu1vehSpinDetOld == 1)&&(ldu1vehSpinDet == 0))
		{
			vref4 = AX_1_P.lds16VspeedByAxSen;
			speed_old = AX_1_P.lds16VspeedByAxSenOld;
		}
		#else
    	if((ldu1VrefMaxupperlim_old == 1) && (ldu1VrefMaxupperlim == 0) && (ACCEL_SPIN_FZ == 1))
    	{
    		vref4 = lds16vrefbyalong ;
    		speed_old = lds16vrefbyalongold;
    	}
    	else
    	{
    		;
    	}
		#endif


    	LDABS_vDetVrefLimitByLongAcc(vref_output_inc_limit, vref_output_dec_limit,4);

		tempW2=vref_dec_limit_by_g*8+FZ_for_4WD.vref_dec_limit_mod;
/*		
		tempW0=25;
	    tempW1=1;
	    s16muls16divs16();
*/	    
		tempW3=tempW2/35;
	    vref_dec_limit=tempW3;
	    FZ_for_4WD.vref_dec_limit_mod=tempW2-35*tempW3;
	    
	   	tempW2=vref_inc_limit_by_g*8+FZ_for_4WD.vref_inc_limit_mod;
/*	   	
		tempW0=25;
	    tempW1=1;
	    s16muls16divs16();
*/	    
		tempW3=tempW2/35;
	    vref_inc_limit=tempW3;
	    FZ_for_4WD.vref_inc_limit_mod=tempW2-35*tempW3;

		if((vref4-speed_old)>vref_inc_limit) 
		{
			vref4=speed_old+vref_inc_limit;
/*			vref_limit_inc_counter++; */
			vref_limit_dec_flag=0;
		}
		else if((vref4-speed_old)<vref_dec_limit) 
		{
			vref4=speed_old+vref_dec_limit;
            if(vref_limit_dec_counter_frt<L_U8_TIME_10MSLOOP_1750MS)
            {
              if(vref_dec_limit_by_g <= -AFZ_1G0)
              {    
                vref_limit_dec_counter_frt = vref_limit_dec_counter_frt + 5;
              }
              else if((vref_dec_limit_by_g>-AFZ_1G0) && (vref_dec_limit_by_g<= -AFZ_0G5 ))
              {
                vref_limit_dec_counter_frt = vref_limit_dec_counter_frt + 2;
              }
              else
              {
                vref_limit_dec_counter_frt++;
              }
            }
            else
            {
              ;
            }
          
            if(vref_limit_dec_counter_rr<L_U8_TIME_10MSLOOP_1750MS) 
            {
              if(vref_dec_limit_by_g <= -AFZ_1G0)
              {    
                vref_limit_dec_counter_rr = vref_limit_dec_counter_rr + 5;
              }
              else if((vref_dec_limit_by_g>-AFZ_1G0) && (vref_dec_limit_by_g<= -AFZ_0G5 ))
              {
                vref_limit_dec_counter_rr = vref_limit_dec_counter_rr + 2;
              }
              else
              {
                vref_limit_dec_counter_rr++;
              }
            }
            else
            {
              ;
            }
/*			
			tempW0=vref;
			tempW1=100;
			tempW2=vref-vrselect1;
			s16muls16divs16();
*/
			tempW3=(int16_t)(((int32_t)(vref-vrselect1)*100)/vref);
			if((tempW3>3)||((vref-vrselect1)>=VREF_1_25_KPH))
			{
				vref_limit_dec_flag=1;
			}
			else if(vrselect1>vref)
			{
				vref_limit_dec_flag=0;
			} 
			else 
			{
			    ;
			}
		}
	    else {
/*	    	vref_limit_inc_counter=0; */
	    	if(vref_limit_dec_counter_frt<=L_U8_TIME_10MSLOOP_210MS) 
	    	{
	    		vref_limit_dec_counter_frt=0;
	    	}
	    	else 
	    	{
	    		vref_limit_dec_counter_frt -=3;
	    	}
	    	
	    	if(vref_limit_dec_counter_rr<=L_U8_TIME_10MSLOOP_210MS) 
	    	{
	    		vref_limit_dec_counter_rr=0;
	    	}
	    	else 
	    	{
	    		vref_limit_dec_counter_rr -=2;
	    	}		         
	    	vref_limit_dec_flag=0;
	    	/*
	    	if(vref_limit_dec_counter_frt<=0)
	    	{
	    	    vref_limit_dec_counter_frt=0;
	    	}
	    	else
	    	{
	    	    ;
	    	}
	    	if(vref_limit_dec_counter_rr<=0) 
	    	{
	    	    vref_limit_dec_counter_rr=0;
	    	}
	    	else
	    	{
	    	    ;
	    	}
	    	*/
	    }			    
	}
/*
	if(vref4<(int16_t)VREF_MIN_SPEED) 
	{
	    vref4=(int16_t)VREF_MIN_SPEED;
	}
    else if(vref4>(int16_t)VREF_MAX_SPEED) 
    {
        vref4=(int16_t)VREF_MAX_SPEED;    
    }
    else
    {
        ;
    }
*/
    vref4=LCABS_s16LimitMinMax(vref4,VREF_MIN_SPEED,VREF_MAX_SPEED);
}
#endif
/*******************************************************************/
void	LDABS_vLimitationVrefMinMax(void)
{

	vref_resol_change=LCABS_s16LimitMinMax(vref_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	#if __VDC
	vref5_resol_change=LCABS_s16LimitMinMax(vref5_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	FL.wvref_resol_change=LCABS_s16LimitMinMax(FL.wvref_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	FR.wvref_resol_change=LCABS_s16LimitMinMax(FR.wvref_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	RL.wvref_resol_change=LCABS_s16LimitMinMax(RL.wvref_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	RR.wvref_resol_change=LCABS_s16LimitMinMax(RR.wvref_resol_change,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	#endif

	
	
	vref=LCABS_s16LimitMinMax(vref,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	#if __VDC
	vref5=LCABS_s16LimitMinMax(vref5,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	wvref_fl=LCABS_s16LimitMinMax(wvref_fl,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	wvref_fr=LCABS_s16LimitMinMax(wvref_fr,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	wvref_rl=LCABS_s16LimitMinMax(wvref_rl,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	wvref_rr=LCABS_s16LimitMinMax(wvref_rr,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
	#endif
/*	
	#if __WL_SPEED_RESOL_CHANGE
    if(vref_resol_change > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	vref_resol_change=VREF_MAX_RESOL_CHANGE;
    }
    else if(vref_resol_change < (int16_t)VREF_MIN_SPEED) 
    {
    	vref_resol_change=VREF_MIN_SPEED;
    }
    else
    {
        ;
    }
    	#if __VDC	
    if(vref5_resol_change > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	vref5_resol_change=VREF_MAX_RESOL_CHANGE;
    }
    else if(vref5_resol_change < (int16_t)VREF_MIN_SPEED) 
    {
    	vref5_resol_change=VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }
    
    if(FL.wvref_resol_change  > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	FL.wvref_resol_change =VREF_MAX_RESOL_CHANGE;
    }
    else if(FL.wvref_resol_change  < (int16_t)VREF_MIN_SPEED) 
    {
    	FL.wvref_resol_change =VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }
    
    if(FR.wvref_resol_change  > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	FR.wvref_resol_change =VREF_MAX_RESOL_CHANGE;
    }
    else if(FR.wvref_resol_change  < (int16_t)VREF_MIN_SPEED) 
    {
    	FR.wvref_resol_change =VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }
    
    if(RL.wvref_resol_change  > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	RL.wvref_resol_change =VREF_MAX_RESOL_CHANGE;
    }
    else if(RL.wvref_resol_change  < (int16_t)VREF_MIN_SPEED) 
    {
    	RL.wvref_resol_change =VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }
    if(RR.wvref_resol_change  > (int16_t)VREF_MAX_RESOL_CHANGE) 
    {
    	RR.wvref_resol_change =VREF_MAX_RESOL_CHANGE;
    }
    else if(RR.wvref_resol_change  < (int16_t)VREF_MIN_SPEED) 
    {
    	RR.wvref_resol_change =VREF_MIN_SPEED;    
    }
    else
    {
        ;
    }
  		#endif
	#endif
    if(vref > (int16_t)VREF_MAX) 
    {
    	vref=VREF_MAX;
    }
    else if(vref < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	vref=VREF_MIN_SPEED_RESOL_8;
    }
    else
    {
        ;
    }
    if(vref2 > (int16_t)VREF_MAX) 
    {
    	vref2=VREF_MAX;
    }
    else if(vref2 < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	vref2=VREF_MIN_SPEED_RESOL_8;
    }
    else
    {
    	;
    }
  #if __VDC
    if(vref5 > (int16_t)VREF_MAX) 
    {
    	vref5=VREF_MAX;
    }
    else if(vref5 < (int16_t)VREF_MIN_SPEED_RESOL_8)
    {
    	 vref5=VREF_MIN_SPEED_RESOL_8;
    }
    else
    {
        ;
    }
    
    if(FL.wvref  > (int16_t)VREF_MAX) 
    {
    	FL.wvref =VREF_MAX;
    }
    else if(FL.wvref  < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	FL.wvref=VREF_MIN_SPEED_RESOL_8;    
    }
    else
    {
        ;
    }
    
    if(FR.wvref  > (int16_t)VREF_MAX) 
    {
    	FR.wvref =VREF_MAX;
    }
    else if(FR.wvref  < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	FR.wvref=VREF_MIN_SPEED_RESOL_8;    
    }
    else
    {
        ;
    }
    
    if(RL.wvref  > (int16_t)VREF_MAX) 
    {
    	RL.wvref =VREF_MAX;
    }
    else if(RL.wvref  < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	RL.wvref=VREF_MIN_SPEED_RESOL_8;    
    }  
    else
    {
        ;
    }      
    if(RR.wvref  > (int16_t)VREF_MAX) 
    {
    	RR.wvref =VREF_MAX;
    }
    else if(RR.wvref  < (int16_t)VREF_MIN_SPEED_RESOL_8) 
    {
    	RR.wvref=VREF_MIN_SPEED_RESOL_8;    
    }
    else
    {
        ;
    }
  #endif
*/
}
/*******************************************************************/
void	LDABS_vEstWheelSlip(void)
{
	LDABS_vEstEachWheelSlip(&FL);
	LDABS_vEstEachWheelSlip(&FR);
	LDABS_vEstEachWheelSlip(&RL);
	LDABS_vEstEachWheelSlip(&RR);	

}

/*******************************************************************/
void    LDABS_vEstEachWheelSlip(struct W_STRUCT *WL)
{
/* set SCHNELLSTE_VREF flag */
/*                                        
    if(abmittel_fz <= (int16_t)ABMITTEL_FZ_60P) SCHNELLSTE_VREF_wl=0;
    else if(abmittel_fz >= (int16_t)ABMITTEL_FZ_80P) {
        SCHNELLSTE_VREF_wl=1;
        goto Check_emergency;
    }

    if(vref >= (int16_t)VREF_70_KPH) SCHNELLSTE_VREF_wl=1;      

    if(REAR_WHEEL_wl==1) {
        if(SCHNELLSTE_VREF_wl==0) {
            if(AFZ_OK==1) {
                if(afz >= -(int16_t)U8_LOW_MU) SCHNELLSTE_VREF_wl=1;
            }
        }
    }

    Check_emergency:

    if(inst_zeit==(uint8_t)0xff) SCHNELLSTE_VREF_wl=0;
*/

	if(abmittel_fz >= (int16_t)ABMITTEL_FZ_80P) 
	{
		if(inst_zeit==(uint8_t)0xff)
		{
		    SCHNELLSTE_VREF_wl=0;
		}
		else 
		{
		    SCHNELLSTE_VREF_wl=1;
		}
	}
	else 
	{
		if(vref >= (int16_t)VREF_70_KPH)
		{
		    SCHNELLSTE_VREF_wl=1;
		}
		else if(abmittel_fz <= (int16_t)ABMITTEL_FZ_60P) 
		{
		    SCHNELLSTE_VREF_wl=0;
		}
		else
		{
		    ;
		}
		
	    if(REAR_WHEEL_wl==1) 
	    {
	        if(SCHNELLSTE_VREF_wl==0) 
	        {
	            if(AFZ_OK==1) 
	            {
	                if(afz >= -(int16_t)U8_LOW_MU) 
	                {
	                    SCHNELLSTE_VREF_wl=1;
	                }
	                else
	                {
	                    ;
	                }
	            }
	            else
	            {
	                ;
	            }
	        }
	        else	        
	        {
	            ;
	        }
	    }
	    else
	    {
	        ;
	    }
	}	

#if __VDC
    tempW4=WL->wvref;
#else
    #if __4WD_VARIANT_CODE==ENABLE
    if(lsu8DrvMode !=DM_2WD)
    {
        tempW4 = vref;                                    
    }
    else
    {
    						
/*    
        if((SCHNELLSTE_VREF_wl==1)||(REAR_WHEEL_wl == 1)) 
        {
            tempW4 = voptfz1;
        }
        else 
        {
            ;
        }
*/
      #if __REAR_D
        if((fu1WheelFLErrDet==1)||(fu1WheelFLSusDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelFRSusDet==1))
        {
            tempW4 = voptfz2;
        }
      #else
        if((fu1WheelRLErrDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRErrDet==1)||(fu1WheelRRSusDet==1))
        {
            tempW4 = voptfz2;
        }
       #endif
        else
        {
            tempW4 = voptfz1;
        }
    }  
    #else 
#if __4WD 
    tempW4 = vref;                                    
#else
/*
	tempW4 = voptfz1;					

    if((SCHNELLSTE_VREF_wl==1)||(REAR_WHEEL_wl == 1)) 
    {
        tempW4 = voptfz1;
    }
    else 
    {
        ;
    }
*/
  #if __REAR_D
    if((fu1WheelFLErrDet==1)||(fu1WheelFLSusDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelFRSusDet==1))
    {
        tempW4 = voptfz2;
    }
  #else
    if((fu1WheelRLErrDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRErrDet==1)||(fu1WheelRRSusDet==1))
    {
        tempW4 = voptfz2;
    }
   #endif
    else
    {
        tempW4 = voptfz1;
    }
#endif
    #endif
#endif

  #if __SPARE_WHEEL && ! __RTA_ENABLE
    if(MINI_SPARE_WHEEL_wl==1) 
    {
/*
        tempW0=100;
        tempW1=tempW4;
        tempW2=(100-(int16_t)U8_MINI_WHEEL_DOWN_RATE)+100;          
        s16muls16divs16();

        tempW4=tempW3;
*/
		tempW4=(int16_t)(((int32_t)tempW4*	U8_MINI_WHEEL_DOWN_RATE)/100);
    }
    else
    {
        ;
    }
  #endif
    tempW6=WL->vrad-tempW4;
    /*rel_lam calculation*/
/*
    tempW2=tempW6;                                      
    tempW1=100;
    tempW0=tempW4;
    s16muls16divs16();
*/
	#if ! __RTA_ENABLE	
	tempW3=(int16_t)(((int32_t)(WL->vrad-tempW4)*100)/tempW4);
	#else
	tempW3=(int16_t)(((int32_t)(WL->vrad_crt-tempW4)*100)/tempW4);
	#endif
	tempW3=LCABS_s16LimitMinMax(tempW3,-100,100);
/*
    if(tempW3 > (int16_t)100) 
    {
        tempW3=100;
    }
    else if(tempW3 < (int16_t)(-100))
    {
        tempW3=-100;
    }
    else
    {
        ;
    }
*/
/*    WL->rel_lam_alt=WL->rel_lam; */
    WL->rel_lam=(int8_t)tempW3;

     /* set slip flags*/
    SLIP_2P_NEG_wl=0;                                  
    SLIP_5P_NEG_wl=0;
    SLIP_10P_NEG_wl=0;
    SLIP_20P_NEG_wl=0;
    SLIP_30P_NEG_wl=0;

    if(tempW3 < (int16_t)(-LAM_2P)) 
    {
        if(tempW6 < (int16_t)(-VREF_2_KPH)) 
        {
            SLIP_2P_NEG_wl=1;
            if(tempW3 < (int16_t)(-LAM_5P)) 
            {
                if(CAS_wl==0) 
                {
                    tempW0=-VREF_2_KPH;
                }
                else 
                {
                    tempW0=-VREF_1_KPH;
                }
                if(tempW6 < tempW0) 
                {
                    SLIP_5P_NEG_wl=1;
                    if(tempW3 < (int16_t)(-LAM_10P)) 
                    {
                        if(tempW6 < (int16_t)(-VREF_6_KPH)) 
                        {
                            SLIP_10P_NEG_wl=1;
                            if(tempW3 < (int16_t)(-LAM_20P)) 
                            {
                                if(tempW6 < (int16_t)(-VREF_12_KPH)) 
                                {
                                    SLIP_20P_NEG_wl=1;
                                    if(tempW3 < (int16_t)(-LAM_30P)) 
                                    {
                                        if(tempW6 < (int16_t)(-VREF_15_KPH)) 
                                        {
                                            SLIP_30P_NEG_wl=1;
                                        }
                                        else
                                        {
                                            ;
                                        }
                                    }
                                    else
                                    {
                                        ;
                                    }
                                }
                                else
                                {
                                    ;
                                }
                            }
                            else
                            {
                                ;
                            }
                        }
                        else
                        {
                            ;
                        }
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    ;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
}
/*******************************************************************/
/* #if __LOGGER && (LOGGER_DATA_FORMAT==1) */
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1
#if __ECU==ESP_ECU_1
#include "LSCallLogData.h"



	#if (__VREF_LOGDATA_SELECTION == 1)
void	LSABS_vCallVrefLogData(void)
{
	    uint8_t i;
        CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);				/*1*/
        CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);           
        CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_crt_resol_change);             /*2*/
        CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_crt_resol_change>>8);
        CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_crt_resol_change);             /*3*/
        CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_crt_resol_change>>8);
        CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_crt_resol_change);             /*4*/
        CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_crt_resol_change>>8);
                              
        CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);				/*5*/
        CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);                       
        CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);					/*6*/
        CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);                       
        CAN_LOG_DATA[12] = (uint8_t)(FZ1.vrselect_resol_change);			/*7*/
        CAN_LOG_DATA[13] = (uint8_t)(FZ1.vrselect_resol_change>>8);                       
		
		#if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		if(lsu8DrvMode !=DM_2WD)
		{
			#endif
        CAN_LOG_DATA[14] = (uint8_t)(lds16HPFWheelAccel);               /*8*/
        CAN_LOG_DATA[15] = (uint8_t)(lds16HPFWheelAccel>>8);                   
        CAN_LOG_DATA[16] = (uint8_t)(lds16HPFLongG);                    /*9*/
        CAN_LOG_DATA[17] = (uint8_t)(lds16HPFLongG>>8);
        CAN_LOG_DATA[18] = (uint8_t)(lds16VrefDelta);                   /*10*/
        CAN_LOG_DATA[19] = (uint8_t)(lds16VrefDelta>>8);
        CAN_LOG_DATA[20] = (uint8_t)(vref_by_along);                    /*11*/
        CAN_LOG_DATA[21] = (uint8_t)(vref_by_along>>8);
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                            
    		#endif
        #endif
        
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		else
		{
			#endif        
        CAN_LOG_DATA[14] = (uint8_t)(RTA_FL.det_rta_suspect_cnt);               		/*8*/
        CAN_LOG_DATA[15] = (uint8_t)(RTA_FL.det_rta_suspect_cnt>>8);                     
        CAN_LOG_DATA[16] = (uint8_t)(RTA_FR.det_rta_suspect_cnt);                    /*9*/
        CAN_LOG_DATA[17] = (uint8_t)(RTA_FR.det_rta_suspect_cnt>>8);
        CAN_LOG_DATA[18] = (uint8_t)(RTA_RL.det_rta_suspect_cnt);                   	/*10*/
        CAN_LOG_DATA[19] = (uint8_t)(RTA_RL.det_rta_suspect_cnt>>8);
        CAN_LOG_DATA[20] = (uint8_t)(RTA_RR.det_rta_suspect_cnt);                    /*11*/
        CAN_LOG_DATA[21] = (uint8_t)(RTA_RR.det_rta_suspect_cnt>>8);
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                           
    		#endif        
    		#endif        
		
        CAN_LOG_DATA[22] = (uint8_t)(lds16Abs1StVreflimCnt);                         /*12*/
        CAN_LOG_DATA[23] = (uint8_t)(lds16Abs1StVreflimCnt>>8);
		#if __AX_SENSOR == ENABLE
        CAN_LOG_DATA[24] = (uint8_t)(along);                    	   /*13*/
        CAN_LOG_DATA[25] = (uint8_t)(along>>8);
        #else
       	CAN_LOG_DATA[24] = (uint8_t)(speed_calc_timer);//(rta_reset_speed_cnt);                               /*13*/
        CAN_LOG_DATA[25] = (uint8_t)(speed_calc_timer>>8);//(rta_reset_speed_cnt>>8);
        #endif      
        CAN_LOG_DATA[26] = (uint8_t)(eng_torq);                        /*14*/
        CAN_LOG_DATA[27] = (uint8_t)(eng_torq>>8);
        CAN_LOG_DATA[28] = (uint8_t)(yaw_out);                         /*15*/
        CAN_LOG_DATA[29] = (uint8_t)(yaw_out>>8);
        CAN_LOG_DATA[30] = (uint8_t)(alat);                            /*16*/
        CAN_LOG_DATA[31] = (uint8_t)(alat>>8);
		
        CAN_LOG_DATA[32] = (uint8_t)(wstr);                            /*17*/
        CAN_LOG_DATA[33] = (uint8_t)(wstr>>8);

        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if (__4WD_VARIANT_CODE==ENABLE)
        if(lsu8DrvMode !=DM_2WD)
		{
		#endif
        CAN_LOG_DATA[34] = (uint8_t)(WheelAccelG_1);                   /*18*/
        CAN_LOG_DATA[35] = (uint8_t)(WheelAccelG_1>>8);
        #if (__4WD_VARIANT_CODE==ENABLE)
    	}
    	#endif
        #endif
        
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if (__4WD_VARIANT_CODE==ENABLE)
        else
        {
        #endif
        CAN_LOG_DATA[34] = (uint8_t)(rta_ratio);                               /*18*/
        CAN_LOG_DATA[35] = (uint8_t)(rta_ratio>>8);
        #if (__4WD_VARIANT_CODE==ENABLE)     
    	}  
    	#endif
        #endif
        
        CAN_LOG_DATA[36] = (uint8_t)(lsabss16DecelRefiltByVref5);       /*19*/
        CAN_LOG_DATA[37] = (uint8_t)(lsabss16DecelRefiltByVref5>>8);
        CAN_LOG_DATA[38] = (uint8_t)(mpress);                           /*20*/
        CAN_LOG_DATA[39] = (uint8_t)(mpress>>8);     
        
        CAN_LOG_DATA[40] = (uint8_t)(vref5_resol_change);                     /*21*/
        CAN_LOG_DATA[41] = (uint8_t)(vref5_resol_change>>8);
        CAN_LOG_DATA[42] = (uint8_t)(Vehicle_Decel_EST_filter);                                    /*22*/ /*temp1*/
        CAN_LOG_DATA[43] = (uint8_t)(Vehicle_Decel_EST_filter>>8);
        
        CAN_LOG_DATA[44] = (uint8_t)(FL.s16_Estimated_Active_Press);                                    /*23*/ /*temp2*/
        CAN_LOG_DATA[45] = (uint8_t)(FL.s16_Estimated_Active_Press>>8);
        CAN_LOG_DATA[46] = (uint8_t)(FR.s16_Estimated_Active_Press);/*24*/ /*temp3*/
        CAN_LOG_DATA[47] = (uint8_t)(FR.s16_Estimated_Active_Press>>8);   
        CAN_LOG_DATA[48] = (uint8_t)(RL.s16_Estimated_Active_Press);                                            /*31*/
        CAN_LOG_DATA[49] = (uint8_t)(RL.s16_Estimated_Active_Press>>8);                                            /*32*/
        CAN_LOG_DATA[50] = (uint8_t)(RR.s16_Estimated_Active_Press);                                            /*33*/
        CAN_LOG_DATA[51] = (uint8_t)(RR.s16_Estimated_Active_Press>>8);                                            /*34*/
        
        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if    (__4WD_VARIANT_CODE==ENABLE) 
        if(lsu8DrvMode !=DM_2WD)
        {
			#endif       
        CAN_LOG_DATA[52] = (uint8_t)(lds8SpinSetCount);                             /*25*/
        CAN_LOG_DATA[53] = (uint8_t)(lds8SpinSetCount_2);  
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}
    		#endif
        #endif
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
    	else
    	{     
			#endif 
        CAN_LOG_DATA[52] = (uint8_t)(rta_ratio/10);                             /*25*/
        CAN_LOG_DATA[53] = (uint8_t)(0); 
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}
        	#endif   
        #endif
                                 /*26*/
        CAN_LOG_DATA[54] = (uint8_t)(gs_pos);                                   /*27*/
        CAN_LOG_DATA[55] = (uint8_t)(mtp);                                   /*28*/
        CAN_LOG_DATA[56] = (uint8_t)(wheel_selection_mode);                                            /*29*/
        CAN_LOG_DATA[57] = (uint8_t)(ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(int8_t)ebd_refilt_grv);                                            /*30*/
        

        CAN_LOG_DATA[58] = (uint8_t)(afz>127)?127:((afz<-127)?-afz:(int8_t)afz);                                           /*35*/
        CAN_LOG_DATA[59] = (uint8_t)(ldrtau8RtaState);  
                                                  /*36*/
        CAN_LOG_DATA[60] = (uint8_t)(ldu8absmustate);                                            /*37*/
        CAN_LOG_DATA[61] = (uint8_t)(slew_rate_timer_fl);                                            /*38*/
        CAN_LOG_DATA[62] = (uint8_t)(slew_rate_timer_fr);                                            /*39*/
        CAN_LOG_DATA[63] = (uint8_t)(RR.arad);                                            /*40*/
        
        
        CAN_LOG_DATA[64] = (uint8_t)(FL.arad_avg);                /*41*/
        CAN_LOG_DATA[65] = (uint8_t)(FL.rel_lam);
        					/*42*/
        CAN_LOG_DATA[66] = (uint8_t)(FL.pwm_duty_temp);                /*43*/
        CAN_LOG_DATA[67] = (uint8_t)(FR.pwm_duty_temp);                /*44*/
        CAN_LOG_DATA[68] = (uint8_t)(RL.pwm_duty_temp);                /*45*/
        CAN_LOG_DATA[69] = (uint8_t)(RR.pwm_duty_temp);                /*46*/
                                          

        i=0;                                  
        if (ABS_fz                          ==1) i|=0x01; 
        if (BRAKE_SIGNAL                    ==1) i|=0x02; 
        if (BTCS_ON                         ==1) i|=0x04; 
        if (ETCS_ON                         ==1) i|=0x08; 
		if (ESP_TCS_ON                      ==1) i|=0x10;
		if (YAW_CDC_WORK                    ==1) i|=0x20; 
		if (VDC_REF_WORK                    ==1) i|=0x40; 
		if ((ESP_ERROR_FLG                   ==1)||(BACK_DIR==1)) i|=0x80; 

		CAN_LOG_DATA[70] = i;
		
		i=0; 
		
		if (vref_limit_dec_flag           	==1) i|=0x01;         
        if (LOW_MU_SUSPECT_FLG              ==1) i|=0x02;
        if ((FL.SPECIAL_SPIN_OK==1)||(FR.SPECIAL_SPIN_OK==1)) i|=0x04; 
        if ((RL.SPECIAL_SPIN_OK==1)||(RR.SPECIAL_SPIN_OK==1)) i|=0x08; 
        if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) i|=0x10;
        if (ESP_SENSOR_RELIABLE_FLG         ==1) i|=0x20; 
        	
        #if	__AX_SENSOR	
        if (BACKWARD_MOVE						==1) i|=0x40;   
        #else
        if (0							==1) i|=0x40; 
        #endif		                                      
        if (BRAKING_STATE                   ==1) i|=0x80;      
        CAN_LOG_DATA[71] = i;               

        i = 0;                                   
        if (JUMP_DOWN_fl                          ==1) i|=0x01;
        if (JUMP_DOWN_fr                          ==1) i|=0x02;
        if (ABS_rl                          ==1) i|=0x04;
		if (ABS_rr                          ==1) i|=0x08; 
        if (FL.ESC_BRAKE_SLIP_OK            ==1) i|=0x10;
        if (FR.ESC_BRAKE_SLIP_OK            ==1) i|=0x20;
        if (RL.ESC_BRAKE_SLIP_OK            ==1) i|=0x40;
        if (RR.ESC_BRAKE_SLIP_OK            ==1) i|=0x80;
        CAN_LOG_DATA[72] = i;      
        
        i = 0;                                          
        if (HV_VL_fl                        ==1) i|=0x01; 
        if (AV_VL_fl                        ==1) i|=0x02; 
        if (HV_VL_fr                        ==1) i|=0x04; 
		if (AV_VL_fr                        ==1) i|=0x08; 
        if (HV_VL_rl                        ==1) i|=0x10; 
		if (AV_VL_rl                        ==1) i|=0x20;
        if (HV_VL_rr                        ==1) i|=0x40; 
        if (AV_VL_rr                        ==1) i|=0x80; 
        CAN_LOG_DATA[73] = i;  
       
         i = 0;
        if ((FL.UNRELIABLE_AT_ABS_ENTRY ==1)||(FL.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x01;
        if ((FR.UNRELIABLE_AT_ABS_ENTRY ==1)||(FR.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x02; 
        if ((RL.UNRELIABLE_AT_ABS_ENTRY ==1)||(RL.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x04;  
        if ((RR.UNRELIABLE_AT_ABS_ENTRY ==1)||(RR.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x08;  
        if (BTCS_fl    ==1) i|=0x10;           
        if (BTCS_fr    ==1) i|=0x20;  
        if (BTCS_rl    ==1) i|=0x40;       
        if (BTCS_rr    ==1) i|=0x80;                
        CAN_LOG_DATA[74] = i;  
                               
         i = 0;
        if (FL.ESC_CNT_SPIN_OK              ==1) i|=0x01;
        if (FR.ESC_CNT_SPIN_OK              ==1) i|=0x02; 
        if (RL.ESC_CNT_SPIN_OK              ==1) i|=0x04;  
        if (RR.ESC_CNT_SPIN_OK              ==1) i|=0x08;       
        if (FL.DRIVING_SPIN_OK              ==1) i|=0x10;           
        if (FR.DRIVING_SPIN_OK     			==1) i|=0x20;  
        if (RL.DRIVING_SPIN_OK              ==1) i|=0x40;       
        if (RR.DRIVING_SPIN_OK              ==1) i|=0x80;        	
        CAN_LOG_DATA[75] = i;

        i = 0;
        if (FL.RELIABLE_FOR_VREF            ==1) i|=0x01;              
        if (FR.RELIABLE_FOR_VREF            ==1) i|=0x02;              
        if (RL.RELIABLE_FOR_VREF            ==1) i|=0x04;              
        if (RR.RELIABLE_FOR_VREF           	==1) i|=0x08;                  
        if (FL.WHL_SELECT_FLG               ==1) i|=0x10;                        
        if (FR.WHL_SELECT_FLG     			==1) i|=0x20;                        
        if (RL.WHL_SELECT_FLG               ==1) i|=0x40;                    
        if (RR.WHL_SELECT_FLG               ==1) i|=0x80;                    
        CAN_LOG_DATA[76] = i;  

        i = 0;
        if (RELIABILITY_MAX_WL              ==1) i|=0x01;
        if (RELIABILITY_2ND_WL              ==1) i|=0x02; 
        if (RELIABILITY_3RD_WL              ==1) i|=0x04;  
        if (RELIABILITY_MIN_WL              ==1) i|=0x08;      
        if ((FL.RELIABLE_FOR_VREF2==1)||(FR.RELIABLE_FOR_VREF2==1)) i|=0x10;           
        if ((RL.RELIABLE_FOR_VREF2==1)||(RR.RELIABLE_FOR_VREF2==1)) i|=0x20;
        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		if(lsu8DrvMode !=DM_2WD)
		{
			#endif  	         
        if (ACCEL_SPIN_FZ					==1) 					i|=0x40;       
        if (BIG_ACCEL_SPIN_FZ				==1) 					i|=0x80; 
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                            
    		#endif
        #endif	
        
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		else
		{
			#endif 	
        if (LFC_GMA_Split_flag					==1) 					i|=0x40;       
        if (RTA_RESET_STATE						==1) 					i|=0x80; 	         
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}                           
    		#endif        
        #endif   
        CAN_LOG_DATA[77] = i;                 

        i = 0;
#if __RTA_ENABLE
        if (RTA_FL.RTA_SUSPECT_WL           ==1) i|=0x01;
        if (RTA_FR.RTA_SUSPECT_WL           ==1) i|=0x02; 
        if (RTA_RL.RTA_SUSPECT_WL           ==1) i|=0x04;  
        if (RTA_RR.RTA_SUSPECT_WL          	==1) i|=0x08;
#else 
        if (MINI_SPARE_WHEEL_fl             ==1) i|=0x01;
        if (MINI_SPARE_WHEEL_fr             ==1) i|=0x02; 
        if (MINI_SPARE_WHEEL_rl             ==1) i|=0x04;  
        if (MINI_SPARE_WHEEL_rr            	==1) i|=0x08;
#endif
        if ((RL.PARKING_BRAKE_WHEEL         ==1)||(ldu1absl2hdetectionflag==1))  i|=0x10;           
        if ((RR.PARKING_BRAKE_WHEEL         ==1)||(ldu1absl2hFilterRespon ==1)) i|=0x20;  
        if (BLS                             ==1) i|=0x40;       
        if ((VrefPress_Limit_flag==1)||(VrefFilot_Limit_flag==1)) i|=0x80;          
                      
        CAN_LOG_DATA[78] = i;    

        i = 0;
        if (TCL_DEMAND_fl                  	==1) i|=0x01;
        if (TCL_DEMAND_fr                   ==1) i|=0x02; 
        if (S_VALVE_LEFT                    ==1) i|=0x04;  
        if (S_VALVE_RIGHT                   ==1) i|=0x08;       
        if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10;           
        if (ESP_BRAKE_CONTROL_FR     		==1) i|=0x20;         
        if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x40;       
        if (ESP_BRAKE_CONTROL_RR            ==1) i|=0x80;      
        CAN_LOG_DATA[79] = i;    


        
        
        
        
        
    

}
	#elif (__VREF_LOGDATA_SELECTION == 2)
void	LSABS_vCallVrefLogData(void)
{
	    uint8_t i;
        CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);				/*1*/
        CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);           
        CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_crt_resol_change);             /*2*/
        CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_crt_resol_change>>8);
        CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_crt_resol_change);             /*3*/
        CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_crt_resol_change>>8);
        CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_crt_resol_change);             /*4*/
        CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_crt_resol_change>>8);
                              
        CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);				/*5*/
        CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);                       
        CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);					/*6*/
        CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);                       
        CAN_LOG_DATA[12] = (uint8_t)(FZ1.vrselect_resol_change);			/*7*/
        CAN_LOG_DATA[13] = (uint8_t)(FZ1.vrselect_resol_change>>8);                       
		
		#if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		if(lsu8DrvMode !=DM_2WD)
		{
			#endif
        CAN_LOG_DATA[14] = (uint8_t)(lds16HPFWheelAccel);               /*8*/
        CAN_LOG_DATA[15] = (uint8_t)(lds16HPFWheelAccel>>8);                   
        CAN_LOG_DATA[16] = (uint8_t)(lds16HPFLongG);                    /*9*/
        CAN_LOG_DATA[17] = (uint8_t)(lds16HPFLongG>>8);
        CAN_LOG_DATA[18] = (uint8_t)(lds16VrefDelta);                   /*10*/
        CAN_LOG_DATA[19] = (uint8_t)(lds16VrefDelta>>8);
        CAN_LOG_DATA[20] = (uint8_t)(vref_by_along);                    /*11*/
        CAN_LOG_DATA[21] = (uint8_t)(vref_by_along>>8);
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                            
    		#endif
        #endif
        
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		else
		{
			#endif        
        CAN_LOG_DATA[14] = (uint8_t)(RTA_FL.det_rta_suspect_cnt);               		/*8*/
        CAN_LOG_DATA[15] = (uint8_t)(RTA_FL.det_rta_suspect_cnt>>8);                     
        CAN_LOG_DATA[16] = (uint8_t)(RTA_FR.det_rta_suspect_cnt);                    /*9*/
        CAN_LOG_DATA[17] = (uint8_t)(RTA_FR.det_rta_suspect_cnt>>8);
        CAN_LOG_DATA[18] = (uint8_t)(RTA_RL.det_rta_suspect_cnt);                   	/*10*/
        CAN_LOG_DATA[19] = (uint8_t)(RTA_RL.det_rta_suspect_cnt>>8);
        CAN_LOG_DATA[20] = (uint8_t)(RTA_RR.det_rta_suspect_cnt);                    /*11*/
        CAN_LOG_DATA[21] = (uint8_t)(RTA_RR.det_rta_suspect_cnt>>8);
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                           
    		#endif        
        #endif      
		
        CAN_LOG_DATA[22] = (uint8_t)(eng_rpm);//(eng_rpm);                         /*12*/
        CAN_LOG_DATA[23] = (uint8_t)(eng_rpm>>8);//(eng_rpm>>8);
		#if __AX_SENSOR == ENABLE
        CAN_LOG_DATA[24] = (uint8_t)(along);                    	   /*13*/
        CAN_LOG_DATA[25] = (uint8_t)(along>>8);
        #else
       	CAN_LOG_DATA[24] = (uint8_t)(speed_calc_timer);//(rta_reset_speed_cnt);                               /*13*/
        CAN_LOG_DATA[25] = (uint8_t)(speed_calc_timer>>8);//(rta_reset_speed_cnt>>8);
        #endif
        CAN_LOG_DATA[26] = (uint8_t)(eng_torq);                        /*14*/
        CAN_LOG_DATA[27] = (uint8_t)(eng_torq>>8);
        CAN_LOG_DATA[28] = (uint8_t)(yaw_out);                         /*15*/
        CAN_LOG_DATA[29] = (uint8_t)(yaw_out>>8);
        CAN_LOG_DATA[30] = (uint8_t)(alat);                            /*16*/
        CAN_LOG_DATA[31] = (uint8_t)(alat>>8);

        CAN_LOG_DATA[32] = (uint8_t)(wstr);                            /*17*/
        CAN_LOG_DATA[33] = (uint8_t)(wstr>>8);

        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if (__4WD_VARIANT_CODE==ENABLE)
        if(lsu8DrvMode !=DM_2WD)
		{
		#endif
        CAN_LOG_DATA[34] = (uint8_t)(WheelAccelG_1);                   /*18*/
        CAN_LOG_DATA[35] = (uint8_t)(WheelAccelG_1>>8);
        #if (__4WD_VARIANT_CODE==ENABLE)
    	}
    	#endif
        #endif
        
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if (__4WD_VARIANT_CODE==ENABLE)
        else
        {
        #endif
        CAN_LOG_DATA[34] = (uint8_t)(rta_ratio);                               /*18*/
        CAN_LOG_DATA[35] = (uint8_t)(rta_ratio>>8);
        #if (__4WD_VARIANT_CODE==ENABLE)     
    	}  
    	#endif
        #endif
        
        CAN_LOG_DATA[36] = (uint8_t)(lds16VrefPressDecelG);//(lsabss16DecelRefiltByVref5);       /*19*/
        CAN_LOG_DATA[37] = (uint8_t)(lds16VrefPressDecelG>>8);//(lsabss16DecelRefiltByVref5>>8);
        CAN_LOG_DATA[38] = (uint8_t)(mpress);                           /*20*/
        CAN_LOG_DATA[39] = (uint8_t)(mpress>>8);     
        
        CAN_LOG_DATA[40] = (uint8_t)(FZ1.voptfz_resol_change);//(vref5_resol_change);                     /*21*/
        CAN_LOG_DATA[41] = (uint8_t)(FZ1.voptfz_resol_change>>8);//(vref5_resol_change>>8);
        CAN_LOG_DATA[42] = (uint8_t)(Vehicle_Decel_EST_filter);                                    /*22*/ /*temp1*/
        CAN_LOG_DATA[43] = (uint8_t)(Vehicle_Decel_EST_filter>>8);
        
        CAN_LOG_DATA[44] = (uint8_t)(FL.s16_Estimated_Active_Press);                                    /*23*/ /*temp2*/
        CAN_LOG_DATA[45] = (uint8_t)(FL.s16_Estimated_Active_Press>>8);
        CAN_LOG_DATA[46] = (uint8_t)(FR.s16_Estimated_Active_Press);/*24*/ /*temp3*/
        CAN_LOG_DATA[47] = (uint8_t)(FR.s16_Estimated_Active_Press>>8);   
        CAN_LOG_DATA[48] = (uint8_t)(RL.s16_Estimated_Active_Press);                                            /*31*/
        CAN_LOG_DATA[49] = (uint8_t)(RL.s16_Estimated_Active_Press>>8);                                            /*32*/
        CAN_LOG_DATA[50] = (uint8_t)(RR.s16_Estimated_Active_Press);                                            /*33*/
        CAN_LOG_DATA[51] = (uint8_t)(RR.s16_Estimated_Active_Press>>8);                                            /*34*/
        
        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
        #if    (__4WD_VARIANT_CODE==ENABLE) 
        if(lsu8DrvMode !=DM_2WD)
        {
			#endif       
        CAN_LOG_DATA[52] = (uint8_t)(lds8SpinSetCount);                             /*25*/
        CAN_LOG_DATA[53] = (uint8_t)(lds8SpinSetCount_2);  
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}
    		#endif
        #endif
        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
    	else
    	{     
			#endif 
        CAN_LOG_DATA[52] = (uint8_t)(0);                             /*25*/
        CAN_LOG_DATA[53] = (uint8_t)(abs_duration_timer); 
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}
        	#endif   
        #endif
                                 /*26*/
        CAN_LOG_DATA[54] = (uint8_t)(gs_pos);                                   /*27*/
        CAN_LOG_DATA[55] = (uint8_t)(mtp);                                   /*28*/
        CAN_LOG_DATA[56] = (uint8_t)(wheel_selection_mode);                                            /*29*/
        CAN_LOG_DATA[57] = (uint8_t)(ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(int8_t)ebd_refilt_grv);                                            /*30*/
        

        CAN_LOG_DATA[58] = (uint8_t)(afz>127)?127:((afz<-127)?-afz:(int8_t)afz);/*35*/

        //CAN_LOG_DATA[59] = (uint8_t)(lds16vehgradientG>127)?127:((lds16vehgradientG<-127)?-afz:(int8_t)lds16vehgradientG);  //(ldrtau8RtaState);  
                                         /*36*/
        CAN_LOG_DATA[60] = (uint8_t)(ldu8absmustate);                                            /*37*/
        CAN_LOG_DATA[61] = (uint8_t)(slew_rate_timer_fl);//(FR.arad);                                            /*38*/
        CAN_LOG_DATA[62] = (uint8_t)(slew_rate_timer_fr);//(RL.arad);                                            /*39*/
        CAN_LOG_DATA[63] = (uint8_t)(RR.arad);                                            /*40*/
        
        
        CAN_LOG_DATA[64] = (uint8_t)(FL.arad_avg);                /*41*/
        CAN_LOG_DATA[65] = (uint8_t)(FL.rel_lam);
        					/*42*/
        CAN_LOG_DATA[66] = (uint8_t)(FL.pwm_duty_temp);                /*43*/
        CAN_LOG_DATA[67] = (uint8_t)(FR.pwm_duty_temp);                /*44*/
        CAN_LOG_DATA[68] = (uint8_t)(RL.pwm_duty_temp);                /*45*/
        CAN_LOG_DATA[69] = (uint8_t)(RR.pwm_duty_temp);                /*46*/
                                          

        i=0;                                  
        if (ABS_fz                          ==1) i|=0x01; 
        if (BRAKE_SIGNAL                    ==1) i|=0x02; 
        if (BTCS_ON                         ==1) i|=0x04; 
        if (ETCS_ON                         ==1) i|=0x08; 
		if (ESP_TCS_ON                      ==1) i|=0x10;
		if (YAW_CDC_WORK                    ==1) i|=0x20; 
		if (VDC_REF_WORK                    ==1) i|=0x40; 
		if ((ESP_ERROR_FLG                   ==1)||(BACK_DIR==1)) i|=0x80; 

		CAN_LOG_DATA[70] = i;
		
		i=0; 
		
        if (vref_limit_dec_flag           	 ==1) i|=0x01;         
        if (LOW_MU_SUSPECT_FLG              ==1) i|=0x02;
        if ((FL.SPECIAL_SPIN_OK==1)||(FR.SPECIAL_SPIN_OK==1)) i|=0x04; 
        if ((RL.SPECIAL_SPIN_OK==1)||(RR.SPECIAL_SPIN_OK==1)) i|=0x08; 
        if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) i|=0x10;
        if (ESP_SENSOR_RELIABLE_FLG         ==1) i|=0x20; 
                                                 
        #if	__AX_SENSOR	
        if (BACKWARD_MOVE						==1) i|=0x40; 
        #else
        if (0							==1) i|=0x40; 
			#endif		
        if (BRAKING_STATE         ==1) i|=0x80;      
        CAN_LOG_DATA[71] = i;               

        i = 0;                                   
        if (JUMP_DOWN_fl                    ==1) i|=0x01;
        if (JUMP_DOWN_fr                    ==1) i|=0x02;
        if (ABS_rl                       ==1) i|=0x04;
		if (ABS_rr                      ==1) i|=0x08; 
        if (FL.ESC_BRAKE_SLIP_OK            ==1) i|=0x10;
        if (FR.ESC_BRAKE_SLIP_OK            ==1) i|=0x20;
        if (RL.ESC_BRAKE_SLIP_OK            ==1) i|=0x40;
        if (RR.ESC_BRAKE_SLIP_OK            ==1) i|=0x80;
        CAN_LOG_DATA[72] = i;      
        
        i=0;                                          
        if (HV_VL_fl                          ==1) i|=0x01; 
        if (AV_VL_fl                        ==1) i|=0x02; 
        if (HV_VL_fr                       ==1) i|=0x04; 
		if (AV_VL_fr                      ==1) i|=0x08; 
        if (HV_VL_rl                      ==1) i|=0x10; 
		if (AV_VL_rl     ==1) i|=0x20;
        if (HV_VL_rr                        ==1) i|=0x40; 
        if (AV_VL_rr                        ==1) i|=0x80; 
        CAN_LOG_DATA[73] = i;  
       
         i = 0;
        if ((FL.UNRELIABLE_AT_ABS_ENTRY ==1)||(FL.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x01;
        if ((FR.UNRELIABLE_AT_ABS_ENTRY ==1)||(FR.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x02; 
        if ((RL.UNRELIABLE_AT_ABS_ENTRY ==1)||(RL.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x04;  
        if ((RR.UNRELIABLE_AT_ABS_ENTRY ==1)||(RR.UNRELIABLE_AT_BRAKE_ENTRY ==1)) i|=0x08;  
        if (BTCS_fl    ==1) i|=0x10;           
        if (BTCS_fr    ==1) i|=0x20;  
        if (BTCS_rl    ==1) i|=0x40;       
        if (BTCS_rr    ==1) i|=0x80;                
        CAN_LOG_DATA[74] = i;  
                               
         i = 0;
        if (FL.ESC_CNT_SPIN_OK              ==1) i|=0x01;
        if (FR.ESC_CNT_SPIN_OK              ==1) i|=0x02; 
        if (RL.ESC_CNT_SPIN_OK              ==1) i|=0x04;  
        if (RR.ESC_CNT_SPIN_OK              ==1) i|=0x08;       
        if (FL.DRIVING_SPIN_OK              ==1) i|=0x10;           
        if (FR.DRIVING_SPIN_OK     			==1) i|=0x20;  
        if (RL.DRIVING_SPIN_OK              ==1) i|=0x40;       
        if (RR.DRIVING_SPIN_OK              ==1) i|=0x80;        	
        CAN_LOG_DATA[75] = i;

        i = 0;
        if (FL.RELIABLE_FOR_VREF                           	==1) i|=0x01;              
        if (FR.RELIABLE_FOR_VREF                        	==1) i|=0x02;              
        if (RL.RELIABLE_FOR_VREF                        	==1) i|=0x04;              
        if (RR.RELIABLE_FOR_VREF                   		==1) i|=0x08;                  
        if (FL.WHL_SELECT_FLG                  		==1) i|=0x10;                        
        if (FR.WHL_SELECT_FLG     					==1) i|=0x20;                        
        if (RL.WHL_SELECT_FLG                         ==1) i|=0x40;                    
        if (RR.WHL_SELECT_FLG                         ==1) i|=0x80;                    
        CAN_LOG_DATA[76] = i;  

        i = 0;

        if (RELIABILITY_MAX_WL                           	==1) i|=0x01;
        if (RELIABILITY_2ND_WL                        	==1) i|=0x02; 
        if (RELIABILITY_3RD_WL                        	==1) i|=0x04;  
        if (RELIABILITY_MIN_WL                   		==1) i|=0x08;      
        if ((FL.RELIABLE_FOR_VREF2==1)||(FR.RELIABLE_FOR_VREF2==1)) i|=0x10;           
        if ((RL.RELIABLE_FOR_VREF2==1)||(RR.RELIABLE_FOR_VREF2==1)) i|=0x20;
        #if __4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		if(lsu8DrvMode !=DM_2WD)
		{
			#endif  	         
        if (ACCEL_SPIN_FZ					==1) 					i|=0x40;       
        if (BIG_ACCEL_SPIN_FZ				==1) 					i|=0x80; 
        	#if (__4WD_VARIANT_CODE==ENABLE)
    	}                            
    		#endif
        #endif	

        #if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
		else
		{
			#endif 	
        if (LFC_GMA_Split_flag					==1) 					i|=0x40;       
        if (RTA_RESET_STATE						==1) 					i|=0x80; 	         
            #if (__4WD_VARIANT_CODE==ENABLE)
    	}                           
    		#endif        
        #endif   
        CAN_LOG_DATA[77] = i;                 

        i = 0;
#if __RTA_ENABLE
        if (0          ==1) i|=0x01;
        if (0	       ==1) i|=0x02; 
        if (0          ==1) i|=0x04;  
        if (0          ==1) i|=0x08;
#else 
        if (MINI_SPARE_WHEEL_fl                          	==1) i|=0x01;
        if (MINI_SPARE_WHEEL_fr                    	==1) i|=0x02; 
        if (MINI_SPARE_WHEEL_rl                        	==1) i|=0x04;  
        if (MINI_SPARE_WHEEL_rr                   		==1) i|=0x08;
#endif  
        if (ldu1absl2hdetectionflag         ==1) i|=0x10;           
        if (VrefPress_Limit_flag            ==1) i|=0x20;  
        if (BLS                             ==1) i|=0x40;       
        if (VrefFilot_Limit_flag			==1) i|=0x80;          
                      
        CAN_LOG_DATA[78] = i;    

        i = 0;
        if (TCL_DEMAND_fl                           	==1) i|=0x01;
        if (TCL_DEMAND_fr                   ==1) i|=0x02; 
        if (S_VALVE_LEFT                        	==1) i|=0x04;  
        if (S_VALVE_RIGHT                   		==1) i|=0x08;       
        if (ESP_BRAKE_CONTROL_FL                  		==1) i|=0x10;           
        if (ESP_BRAKE_CONTROL_FR     					==1) i|=0x20;         
        if (ESP_BRAKE_CONTROL_RL                         ==1) i|=0x40;       
        if (ESP_BRAKE_CONTROL_RR                         ==1) i|=0x80;      
        CAN_LOG_DATA[79] = i;    


        
        
        
        
        
    

}
	#else
void	LSABS_vCallVrefLogData(void)
{
	    uint8_t i;
/*1*/        CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);
/*1*/        CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);           
/*2*/        CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_resol_change);   
/*2*/        CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_resol_change>>8);   
/*3*/        CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_resol_change);
/*3*/        CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_resol_change>>8); 
/*4*/        CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_resol_change);
/*4*/        CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_resol_change>>8);                          
/*5*/        CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);
/*5*/        CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);                    
/*6*/        CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);
/*6*/        CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);                       
/*7*/        CAN_LOG_DATA[12] = (uint8_t)(FZ1.vrselect_resol_change);
/*7*/        CAN_LOG_DATA[13] = (uint8_t)(FZ1.vrselect_resol_change>>8);                     

/*8*/        CAN_LOG_DATA[14] = (uint8_t)(FL.lsabss16WhlSpdRawSignal64);
/*8*/        CAN_LOG_DATA[15] = (uint8_t)(FL.lsabss16WhlSpdRawSignal64>>8);
/*9*/        CAN_LOG_DATA[16] = (uint8_t)(FR.lsabss16WhlSpdRawSignal64);
/*9*/        CAN_LOG_DATA[17] = (uint8_t)(FR.lsabss16WhlSpdRawSignal64>>8);
/*10*/        CAN_LOG_DATA[18] = (uint8_t)(RL.lsabss16WhlSpdRawSignal64);
/*10*/        CAN_LOG_DATA[19] = (uint8_t)(RL.lsabss16WhlSpdRawSignal64>>8);
/*11*/        CAN_LOG_DATA[20] = (uint8_t)(RR.lsabss16WhlSpdRawSignal64);
/*11*/        CAN_LOG_DATA[21] = (uint8_t)(RR.lsabss16WhlSpdRawSignal64>>8);
        
        #if	__RTA_ENABLE
/**/        if(RTA_FL.RTA_SUSPECT_WL==1){
/*8*/        CAN_LOG_DATA[14] = (uint8_t)(FL.vrad_crt_resol_change);
/*8*/        CAN_LOG_DATA[15] = (uint8_t)(FL.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_FR.RTA_SUSPECT_WL==1){
/*9*/        CAN_LOG_DATA[16] = (uint8_t)(FR.vrad_crt_resol_change);
/*9*/        CAN_LOG_DATA[17] = (uint8_t)(FR.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_RL.RTA_SUSPECT_WL==1){
/*10*/        CAN_LOG_DATA[18] = (uint8_t)(RL.vrad_crt_resol_change);
/*10*/        CAN_LOG_DATA[19] = (uint8_t)(RL.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_RR.RTA_SUSPECT_WL==1){
/*11*/        CAN_LOG_DATA[20] = (uint8_t)(RR.vrad_crt_resol_change);
/*11*/        CAN_LOG_DATA[21] = (uint8_t)(RR.vrad_crt_resol_change>>8);                   
/**/    	}       
        #endif
/**/        
/**/        #if	__RTA_2WHEEL_DCT
/**/        if(RTA_FL.RTA_SUSPECT_2WHL==1){
/*8*/        CAN_LOG_DATA[14] = (uint8_t)(FL.vrad_crt_resol_change);
/*8*/        CAN_LOG_DATA[15] = (uint8_t)(FL.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_FR.RTA_SUSPECT_2WHL==1){
/*9*/        CAN_LOG_DATA[16] = (uint8_t)(FR.vrad_crt_resol_change);
/*9*/        CAN_LOG_DATA[17] = (uint8_t)(FR.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_RL.RTA_SUSPECT_2WHL==1){
/*10*/        CAN_LOG_DATA[18] = (uint8_t)(RL.vrad_crt_resol_change);
/*10*/        CAN_LOG_DATA[19] = (uint8_t)(RL.vrad_crt_resol_change>>8);                   
/**/    	}
/**/        if(RTA_RR.RTA_SUSPECT_2WHL==1){
/*11*/        CAN_LOG_DATA[20] = (uint8_t)(RR.vrad_crt_resol_change);
/*11*/        CAN_LOG_DATA[21] = (uint8_t)(RR.vrad_crt_resol_change>>8);                   
/**/    	}         
/**/        #endif
/*12*/        CAN_LOG_DATA[22] = (uint8_t)FL.arad;                    
/*13*/        CAN_LOG_DATA[23] = (uint8_t)FR.arad;                    
/*14*/        CAN_LOG_DATA[24] = (uint8_t)RL.arad;                    
/*15*/        CAN_LOG_DATA[25] = (uint8_t)RR.arad;                    
/*16*/        CAN_LOG_DATA[26] = (uint8_t)FL.whl_reliability_chk_flag;
/*17*/        CAN_LOG_DATA[27] = (uint8_t)FR.whl_reliability_chk_flag;
/*18*/        CAN_LOG_DATA[28] = (uint8_t)RL.whl_reliability_chk_flag;
/*19*/        CAN_LOG_DATA[29] = (uint8_t)RR.whl_reliability_chk_flag;
/**/
/*20*/        CAN_LOG_DATA[30] = FL.rel_lam;                   
/*21*/        CAN_LOG_DATA[31] = FR.rel_lam;     				
/*22*/        CAN_LOG_DATA[32] = RL.rel_lam;   					
/*23*/        CAN_LOG_DATA[33] = RR.rel_lam;					
/**/        #if __REAR_D        
/*24*/        CAN_LOG_DATA[34] = RL.whl_reliability_chk_cnt;            
/*25*/        CAN_LOG_DATA[35] = RR.whl_reliability_chk_cnt;  	
			#else
/*24*/        CAN_LOG_DATA[34] = FL.whl_reliability_chk_cnt;            
/*25*/        CAN_LOG_DATA[35] = FR.whl_reliability_chk_cnt;  			
			#endif		
/*26*/        CAN_LOG_DATA[36] = slew_rate_timer_fl;   			
/*27*/        CAN_LOG_DATA[37] = slew_rate_timer_fr;				

/*28*/        CAN_LOG_DATA[38] = (uint8_t)(vref5_resol_change);         
/*28*/        CAN_LOG_DATA[39] = (uint8_t)(vref5_resol_change>>8);              
/*29*/        CAN_LOG_DATA[40] = (uint8_t)(wstr);                    
/*29*/        CAN_LOG_DATA[41] = (uint8_t)(wstr>>8) ;                
/*30*/        CAN_LOG_DATA[42] = (uint8_t)(yaw_out);                 
/*30*/        CAN_LOG_DATA[43] = (uint8_t)(yaw_out>>8); 
/*31*/ 		  CAN_LOG_DATA[44] = (uint8_t)((alat/10)>127)?127:(((alat/10)<-127)?-127:(int8_t)(alat/10));
/*32*/	      CAN_LOG_DATA[45] = (uint8_t)((mpress/10)>250)?250:(((mpress/10)<0)?0:(uint8_t)(mpress/10));    			                    
				            
/**/        
/*33*/        CAN_LOG_DATA[46] = (uint8_t)(drive_torq);   
/*33*/        CAN_LOG_DATA[47] = (uint8_t)(drive_torq>>8); 			         		   
/*34*/        CAN_LOG_DATA[48] = (uint8_t)(eng_torq); 
/*34*/        CAN_LOG_DATA[49] = (uint8_t)(eng_torq>>8);              
/*35*/        CAN_LOG_DATA[50] = (uint8_t) mtp;         
/*36*/		  CAN_LOG_DATA[51] = (uint8_t)(inhibition_number =INHIBITION_INDICATOR());
/*37*/        CAN_LOG_DATA[52] = (uint8_t)(aref_avg>127)?127:((aref_avg<-127)?-127:(int8_t)aref_avg); 
/*38*/		  CAN_LOG_DATA[53] = (uint8_t)(ldrtau8RtaState);
/*39*/        CAN_LOG_DATA[54] = (uint8_t)(afz>127)?127:((afz<-127)?-127:(int8_t)afz);
	
				 		
/*40*/        CAN_LOG_DATA[55] = (uint8_t)(vref_output_inc_limit>250)?250:((vref_output_inc_limit<0)?0:(uint8_t)vref_output_inc_limit); 
/*41*/        CAN_LOG_DATA[56] = (uint8_t)(vref_output_dec_limit>127)?127:((vref_output_dec_limit<-127)?-127:(int8_t)vref_output_dec_limit);         
/*42*/		  CAN_LOG_DATA[57] = (uint8_t)(vref2); 
/*42*/        CAN_LOG_DATA[58] = (uint8_t)(vref2>>8);  
/*43*/        CAN_LOG_DATA[59] = (uint8_t)(FL.arad_avg);                               
/*43*/        CAN_LOG_DATA[60] = (uint8_t)(FL.arad_avg>>8);    
   
                
		#if __4WD || (__4WD_VARIANT_CODE==ENABLE)
			#if (__4WD_VARIANT_CODE==ENABLE)
			if (lsu8DrvMode !=DM_2WD){
			#endif	
/*40*/        CAN_LOG_DATA[55] = (uint8_t)(vref_inc_limit_by_g>250)?250:((vref_inc_limit_by_g<0)?0:(uint8_t)vref_inc_limit_by_g); 
/*41*/        CAN_LOG_DATA[56] = (uint8_t)(vref_dec_limit_by_g>127)?127:((vref_dec_limit_by_g<-127)?-127:(int8_t)vref_dec_limit_by_g);      
/*42*/		  CAN_LOG_DATA[57] = (uint8_t)(FZ1.voptfz_resol_change); 
/*42*/        CAN_LOG_DATA[58] = (uint8_t)(FZ1.voptfz_resol_change>>8);   
/*43*/        CAN_LOG_DATA[59] = (uint8_t)(ax_Filter);                               
/*43*/        CAN_LOG_DATA[60] = (uint8_t)(ax_Filter>>8);    
			#if (__4WD_VARIANT_CODE==ENABLE)			
			}                     
            #endif
		#endif   
/*44*/        CAN_LOG_DATA[61] = (uint8_t)(lsabss16DecelRefiltByVref5);      
/*44*/        CAN_LOG_DATA[62] = (uint8_t)(lsabss16DecelRefiltByVref5>>8);   		          			
/*45*/        CAN_LOG_DATA[63] = (uint8_t)(abmittel_fz>250)?250:((abmittel_fz<0)?0:(uint8_t)abmittel_fz);                     
/*46*/        CAN_LOG_DATA[64] = (uint8_t)(FiloutDecelG>127)?127:((FiloutDecelG<-127)?-127:(int8_t)FiloutDecelG);   	
/*47*/        CAN_LOG_DATA[65] = (uint8_t)(VrefPressDecelG>127)?127:((VrefPressDecelG<-127)?-127:(int8_t)VrefPressDecelG);                                    
/*48*/        CAN_LOG_DATA[66] = (uint8_t)(Vehicle_Decel_EST>127)?127:((Vehicle_Decel_EST<-127)?-127:(int8_t)Vehicle_Decel_EST);  
                  
/*49*/        CAN_LOG_DATA[67] = (uint8_t)(rta_ratio/10);  
         
/*50*/		  CAN_LOG_DATA[68] = (uint8_t)(ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(int8_t)ebd_refilt_grv);       
/*51*/        CAN_LOG_DATA[69] = (uint8_t) wheel_selection_mode;    
                                          

        i=0;                                  
        if (ABS_fz                          ==1) i|=0x01; 
        if (BRAKE_SIGNAL                    ==1) i|=0x02; 
        if (BTCS_ON                         ==1) i|=0x04; 
        if (ETCS_ON                         ==1) i|=0x08; 
		if (ESP_TCS_ON                      ==1) i|=0x10;
		if (YAW_CDC_WORK                    ==1) i|=0x20; 
		if (VDC_REF_WORK                    ==1) i|=0x40; 
		if (ESP_ERROR_FLG                   ==1) i|=0x80; 

		CAN_LOG_DATA[70] = i;
		
		i=0; 
		
		#if __4WD||(__4WD_VARIANT_CODE==ENABLE)
                
        if (SPIN_CALC_FLG                   ==1) i|=0x01;
        if (BIG_ACCEL_SPIN_FZ               ==1) i|=0x02; 
        if (ACCEL_SPIN_FZ                   ==1) i|=0x04; 
        if (BACKWARD_MOVE					==1) i|=0x08; 
        if (ldu1SlowestWheelSelectFlag      ==1) i|=0x10;
        if (lsu1VcaReqbySpinSusFlg          ==1) i|=0x20;
        if (lsu1VcaReqbyAllWhlSpinFlg       ==1) i|=0x40;
        if (VCA_ON                          ==1) i|=0x80;        	     
		#endif
        CAN_LOG_DATA[71] = i;               

        i = 0;                                   
        if (ABS_fl                          ==1) i|=0x01;
        if (ABS_fr                          ==1) i|=0x02;
        if (ABS_rl                          ==1) i|=0x04;
		if (ABS_rr                          ==1) i|=0x08; 
        if (FSF_fl                  ==1) i|=0x10;
        if (FSF_fr                  ==1) i|=0x20;
        if (JUMP_DOWN_fl            ==1) i|=0x40;
        if (JUMP_DOWN_fr            ==1) i|=0x80;
        CAN_LOG_DATA[72] = i;      
        
        i = 0;                                          
        if (HV_VL_fl                        ==1) i|=0x01; 
        if (AV_VL_fl                        ==1) i|=0x02; 
        if (HV_VL_fr                        ==1) i|=0x04; 
		if (AV_VL_fr                        ==1) i|=0x08; 
        if (HV_VL_rl                        ==1) i|=0x10; 
		if (AV_VL_rl                        ==1) i|=0x20;
        if (HV_VL_rr                        ==1) i|=0x40; 
        if (AV_VL_rr                        ==1) i|=0x80; 
        CAN_LOG_DATA[73] = i;  
       
         i = 0;
        if (LOW_to_HIGH_suspect            ==1) i|=0x01;
        if (ldu1HighMuSuspectFlag             ==1) i|=0x02; 
        if (FL.LOW_MU_SUSPECT_by_IndexMu   ==1) i|=0x04;  
        if (FR.LOW_MU_SUSPECT_by_IndexMu   ==1) i|=0x08;  
        if (VrefFilot_Limit_flag     ==1) i|=0x10;           
        if (VrefFilot_Toggle_flag    ==1) i|=0x20;  
        if (VrefPress_Limit_flag    ==1) i|=0x40;       
        if (VrefPress_Toggle_flag   ==1) i|=0x80;                
        CAN_LOG_DATA[74] = i;  
                               
         i = 0;
         
        if ((FL.SPECIAL_SPIN_OK==1)||(FL.DRIVING_SPIN_OK==1)) i|=0x01;
        if ((FR.SPECIAL_SPIN_OK==1)||(FR.DRIVING_SPIN_OK==1)) i|=0x02; 
        if ((RL.SPECIAL_SPIN_OK==1)||(RL.DRIVING_SPIN_OK==1)) i|=0x04;  
        if ((RR.SPECIAL_SPIN_OK==1)||(RR.DRIVING_SPIN_OK==1)) i|=0x08;      
        if (FL.RELIABLE_FOR_VREF2             ==1) i|=0x10;           
        if (FR.RELIABLE_FOR_VREF2     			==1) i|=0x20;  
        if (RL.RELIABLE_FOR_VREF2              ==1) i|=0x40;       
        if (RR.RELIABLE_FOR_VREF2              ==1) i|=0x80;        	
        CAN_LOG_DATA[75] = i;

        i = 0;
        if (FL.RELIABLE_FOR_VREF            ==1) i|=0x01;              
        if (FR.RELIABLE_FOR_VREF            ==1) i|=0x02;              
        if (RL.RELIABLE_FOR_VREF            ==1) i|=0x04;              
        if (RR.RELIABLE_FOR_VREF           	==1) i|=0x08;  
        if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) i|=0x10;
        if (ESP_SENSOR_RELIABLE_FLG         ==1) i|=0x20; 
        if (BACK_DIR						==1) i|=0x40;                                         
        if (BRAKING_STATE                   ==1) i|=0x80;      
                  
        CAN_LOG_DATA[76] = i;  

        i = 0;
        if ((FL.ESC_BRAKE_SLIP_OK==1)||(FL.ESC_BRAKE_SLIP_OK==1)) i|=0x01;
        if ((FR.ESC_BRAKE_SLIP_OK==1)||(FR.ESC_BRAKE_SLIP_OK==1)) i|=0x02; 
        if ((RL.ESC_BRAKE_SLIP_OK==1)||(RL.ESC_BRAKE_SLIP_OK==1)) i|=0x04;  
        if ((RR.ESC_BRAKE_SLIP_OK==1)||(RR.ESC_BRAKE_SLIP_OK==1)) i|=0x08;      
        if (RTA_FL.RTA_SUSPECT_WL_FOR_BTCS           ==1) i|=0x10;           
        if (RTA_FR.RTA_SUSPECT_WL_FOR_BTCS     		==1) i|=0x20;         
        if (RTA_RL.RTA_SUSPECT_WL_FOR_BTCS           ==1) i|=0x40;       
        if (RTA_RR.RTA_SUSPECT_WL_FOR_BTCS           ==1) i|=0x80;          
        CAN_LOG_DATA[77] = i;                 

        i = 0;

#if	__RTA_2WHEEL_DCT        	
		if ((RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FL.RTA_SUSPECT_2WHL==1)) i|=0x01;
		if ((RTA_FR.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_2WHL==1)) i|=0x02;
		if ((RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RL.RTA_SUSPECT_2WHL==1)) i|=0x04;
		if ((RTA_RR.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_2WHL==1)) i|=0x08;		        
#elif __RTA_ENABLE
        if (RTA_FL.RTA_SUSPECT_WL           ==1) i|=0x01;
        if (RTA_FR.RTA_SUSPECT_WL           ==1) i|=0x02; 
        if (RTA_RL.RTA_SUSPECT_WL           ==1) i|=0x04;  
        if (RTA_RR.RTA_SUSPECT_WL          	==1) i|=0x08;
#else        	
        if (MINI_SPARE_WHEEL_fl             ==1) i|=0x01;
        if (MINI_SPARE_WHEEL_fr             ==1) i|=0x02; 
        if (MINI_SPARE_WHEEL_rl             ==1) i|=0x04;  
        if (MINI_SPARE_WHEEL_rr            	==1) i|=0x08;
#endif
        if (RL.PARKING_BRAKE_WHEEL         	==1) i|=0x10;           
        if (RR.PARKING_BRAKE_WHEEL     		==1) i|=0x20;  
        if (BLS                             ==1) i|=0x40;       
        if (EBD_RA                          ==1) i|=0x80;          
                      
        CAN_LOG_DATA[78] = i;    

        i = 0;
        if (TCL_DEMAND_fl                  	==1) i|=0x01;
        if (TCL_DEMAND_fr                   ==1) i|=0x02; 
        if (S_VALVE_LEFT                    ==1) i|=0x04;  
        if (S_VALVE_RIGHT                   ==1) i|=0x08;       
        if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10;           
        if (ESP_BRAKE_CONTROL_FR     		==1) i|=0x20;         
        if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x40;       
        if (ESP_BRAKE_CONTROL_RR            ==1) i|=0x80;      
        CAN_LOG_DATA[79] = i;    

}
	#endif

#endif

#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallVrefEst
	#include "Mdyn_autosar.h"
#endif
