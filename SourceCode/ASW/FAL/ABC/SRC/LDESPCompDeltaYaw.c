
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCompDeltaYaw
	#include "Mdyn_autosar.h"
#endif

#include "LVarHead.h"
#include "LDESPEstBeta.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCESPCallcontrol.h"

#if __VDC
int16_t yaw_sign_th;

void LDESP_vCompDeltaYaw(void);


void LDESP_vCompDeltaYaw(void)
{
/***********************************************************************/
/* Calculation of Delta_Yaw Measured                                   */
/***********************************************************************/

    delta_yaw_old=delta_yaw;
    delta_yaw_old2=delta_yaw2;
    delta_yaw_first_alt=delta_yaw_first;
    delta_yaw_first_old2 = delta_yaw_first2;

/*******************************************************************************/
/* Partial steer,  delta_yaw_old                //2004.06.04 */
/*******************************************************************************/
/*    if (PRE_ESP_CONTROL) {									*/
/*        delta_yaw_old=delta_yaw_org;                          */
/*        delta_yaw_old2=delta_yaw2_org;                        */
/*        delta_yaw_first_alt=delta_yaw_first_org;				*/
/*        delta_yaw_first_old2 = delta_yaw_first2_org;          */
/*    }  // '05 china  											*/    
/* 	  tempW0 = (int16_t)( (((int32_t)(yaw_out-rf2))*28)/128);        	*/
/*    tempW1 = (int16_t)( (((int32_t)delta_yaw_old2)*100)/128);		*/
/*    delta_yaw2=tempW0+tempW1;  */
    if ( ESP_ROUGH_ROAD ==1)
    {
        esp_tempW1 = L_U8FILTER_GAIN_10MSLOOP_3_5HZ;
    }
    else
    {
        esp_tempW1 = L_U8FILTER_GAIN_10MSLOOP_6_5HZ;
	}
    delta_yaw2 = LCESP_s16Lpf1Int( yaw_out-rf2, delta_yaw_old2, (uint8_t)esp_tempW1);      

    yaw_sign_old = yaw_sign;
    
    if ( (ABS_fz==1) || (ESP_ROUGH_ROAD==1) || (FLAG_DETECT_DRIFT==1) ) 
    {
    	if ( FLAG_BETA_UNDER_DRIFT==1 ) 
    	{
    		tempW0 = 100;
    	}
    	else 
    	{
    		tempW0 = 40;
    	}
    } 
    else 
    {
    	tempW0 = 25;
    }

	#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)
	
      if((wstr>100)&&(yaw_out>0)) 
      {         
        esp_tempW0 = 100; 
        Flg_Yaw_Sign_Ch_Th_1 = 1 ;       
        Flg_Yaw_Sign_Ch_Th_2 = 0 ;    
      } 
      else if ((wstr>100)&&(yaw_out<=0)&&(Flg_Yaw_Sign_Ch_Th_1==1))
      {
        esp_tempW0 = S16_YAW_SIGN_CH_THRESHOLD; 
        Flg_Yaw_Sign_Ch_Th_1 = 1 ;     
        Flg_Yaw_Sign_Ch_Th_2 = 0 ;  
      }   
      else if ((wstr<-100)&&(yaw_out<0))
      {
        esp_tempW0 = 100; 
        Flg_Yaw_Sign_Ch_Th_1 = 0 ;     
        Flg_Yaw_Sign_Ch_Th_2 = 1 ;  
      }   
      else if ((wstr<-100)&&(yaw_out>=0)&&(Flg_Yaw_Sign_Ch_Th_2==1))
      {
        esp_tempW0 = S16_YAW_SIGN_CH_THRESHOLD; 
        Flg_Yaw_Sign_Ch_Th_1 = 0 ;     
        Flg_Yaw_Sign_Ch_Th_2 = 1 ;  
      }               
      else 
    {
      	esp_tempW0 = 100 ;  /* Original */
        Flg_Yaw_Sign_Ch_Th_1 = 0 ;  
        Flg_Yaw_Sign_Ch_Th_2 = 0 ;  
    }
    
    	yaw_sign_th = LCESP_s16IInter2Point(det_alatm,lsesps16MuMedHigh, esp_tempW0,lsesps16MuHigh,25); 
     
	#else 
/*    if ( wmu1CycleTime21ms_0 == 1 )  
      {*/ 
    	yaw_sign_th = LCESP_s16IInter2Point(det_alatm,lsesps16MuMedHigh, 100,lsesps16MuHigh,25);
/*    }*/    
	#endif       
    
    if(yaw_out >= yaw_sign_th) 
    {
    	yaw_sign =1;
    }
    else if(yaw_out < (-yaw_sign_th))
    {
    	yaw_sign = -1;
    }
    else if((Beta_MD <0)&&(yaw_sign==1))
    {
    	yaw_sign = 1;
	}
	else if((Beta_MD >0)&&(yaw_sign==(-1))) 
	{
		yaw_sign = -1;    
    }
    else 
    {
    	yaw_sign = yaw_sign;
    }  
    if( yaw_sign!=yaw_sign_old ) 
    { 
        FLAG_DELTAYAW_SIGNCHANGE = 1;
        counter_deltayaw_signchange = 0;
    }
    else 
    {
        counter_deltayaw_signchange++;
        if ( counter_deltayaw_signchange > L_U8_TIME_10MSLOOP_40MS ) 
        {
        	counter_deltayaw_signchange = L_U8_TIME_10MSLOOP_40MS;
        }
        else
        {
        	;
        }
        FLAG_DELTAYAW_SIGNCHANGE = 0;
    }
    
    delta_yaw = delta_yaw2*yaw_sign;       
    

    delta_yaw_first2 = LCESP_s16Lpf1Int( yaw_out-rf, delta_yaw_first_old2, (uint8_t)esp_tempW1);        

    delta_yaw_first = delta_yaw_first2*yaw_sign;      

/***********************************************************************/
/* Calculation of delta_yaw_diff                                       */
/***********************************************************************/
    tempW0 = McrAbs(delta_yaw);
    tempW1 = McrAbs(delta_yaw_old);
    
    delta_yaw_diff=tempW0-tempW1;

    if( vrefk < 50) 
    {
        if ( delta_yaw > 6 )      
        {
        	 delta_yaw = delta_yaw - 5;
        
    	}
    	else if ( delta_yaw < -6 ) 
    	{
    		delta_yaw = delta_yaw + 5;
    	}
        else
        {
        	delta_yaw = 0;
        }
        
        if ( delta_yaw_first > 6 )       
        {
        	delta_yaw_first = delta_yaw_first - 5;
        }
        else if ( delta_yaw_first < -6 ) 
        {
        	delta_yaw_first = delta_yaw_first + 5;
    	}
    	else 
    	{
    		delta_yaw_first = 0;
    	}
    }     
    else
    {
    	;
    }  
    
    nosign_delta_yaw_first = delta_yaw_first2;
    nosign_delta_yaw = delta_yaw2;
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCompDeltaYaw
	#include "Mdyn_autosar.h"
#endif
