/******************************************************************************
* Project Name: Low Vacuum Boosting Assist
* File: LALVBACallActHW.C
* Description: Valve/Motor actuaction by LVBA Controller
* Date: June. 7. 2011
******************************************************************************/

/* Includes ******************************************************************/
#include "LALVBACallActHW.h"
#include "LCLVBACallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"


/* Logcal Definiton  *********************************************************/

/* Variables Definition*******************************************************/

/* LocalFunction prototype ***************************************************/
  #if __LVBA
void	LALVBA_vCallActHW(void);
int16_t LAMFC_s16SetMFCCurrentLVBA(uint8_t CIRCUIT, int16_t MFC_Current_old);
#if __ADVANCED_MSC
void LCMSC_vSetLVBATargetVoltage(void);
#endif
  #endif/*__LVBA*/

/* Implementation*************************************************************/
int16_t		lclvbas16MtrOffTimeAtLowSpd, lclvbas16LVBAMtrOffCnt, lvba_msc_target_vol_old;
int16_t     lvba_msc_target_vol; // Variable Define missing : GHS Compile  

#if __LVBA
/******************************************************************************
* FUNCTION NAME:      LALVBA_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Hydraulic Brake Boost Controller
******************************************************************************/
void	LALVBA_vCallActHW(void)
{
	if(lclvbau1ActiveFlg==1) {
		if(lclvbas8LVBAOtherCtrlEnable==0)
		{
		  #if (__SPLIT_TYPE==0)	
		  	if((ESP_BRAKE_CONTROL_FR==0) && (ESP_BRAKE_CONTROL_RL==0))
		  	{
				TCL_DEMAND_fr = LVBA_TC_Primary_ACT;
				S_VALVE_RIGHT = LVBA_ESV_Primary_ACT;
			}
			if((ESP_BRAKE_CONTROL_RR==0) && (ESP_BRAKE_CONTROL_FL==0))
		  	{
				TCL_DEMAND_fl = LVBA_TC_Secondary_ACT;
				S_VALVE_LEFT = LVBA_ESV_Secondary_ACT;
			}
		  #else
		  	if((ESP_BRAKE_CONTROL_FR==0) && (ESP_BRAKE_CONTROL_FL==0))
		  	{
				TCL_DEMAND_PRIMARY = LVBA_TC_Primary_ACT;
				S_VALVE_PRIMARY = LVBA_ESV_Primary_ACT;
			}
		  	if((ESP_BRAKE_CONTROL_RR==0) && (ESP_BRAKE_CONTROL_RL==0))
		  	{
				TCL_DEMAND_SECONDARY = LVBA_TC_Secondary_ACT;
				S_VALVE_SECONDARY = LVBA_ESV_Secondary_ACT;
			}
		  #endif	
		
			VALVE_ACT = 1;
		}
	}
} 

int16_t LAMFC_s16SetMFCCurrentLVBA(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
    int16_t MFC_Current_new = 0;
    int16_t MFC_Current_new_release = 0;

  #if   __LVBA
    if(lclvbau1ActiveFlg==1) 
    {
        if((CIRCUIT==U16_PRIMARY_CIRCUIT) || (CIRCUIT==U16_SECONDARY_CIRCUIT))
        {
            if((lclvbau8LVBAOnCnt <= T_200_MS) && ((lclvbau8LVBAOffCnt==0) || (lclvbau8LVBAOffCnt>T_100_MS)))
            {
                /*tempW7 = MPRESS_150BAR;// + lclvbau8LVBAOnCnt * MPRESS_5BAR;                
                tempW7 = lclvbas16AssistPressure/10*15+MPRESS_50BAR;*/
                tempW7 = lclvbas16AssistPressure+MPRESS_50BAR;
            }
            else if((lclvbau8LVBAOnCnt <= T_700_MS) && ((lclvbau8LVBAOffCnt==0) || (lclvbau8LVBAOffCnt>T_200_MS)))
            {
                if((lclvbau8LVBAStopApply == 1)||(ABS_fz == 1))
                {
                	tempW7 = lclvbas16AssistPressure;
                	tempW9 = MPRESS_180BAR - lclvbas16MPressAvg;
                }
                else
                {
                	tempW7 = (lclvbas16AssistPressure/10)*15;
                	tempW9 = MPRESS_180BAR - lclvbas16MPressAvg;
                }

                if(tempW7 > tempW9) 
                {
                	tempW7 = tempW9;
                }
                else if(tempW7 < MPRESS_10BAR) 
                {
                	tempW7 = MPRESS_10BAR;
                }
                else 
                {
                	;
                }
            }
            else
            {
            	tempW7 = (lclvbas16AssistPressure/10)*10;
                tempW9 = MPRESS_180BAR - lclvbas16MPressAvg;
            }   
            
            if(lclvbas8ControlState == S8_LVBA_FADEOUT)/*TC rise rate decision in Fade out mode*/
            {
            	tempW7 = (int16_t)(((int32_t)tempW7*lclvbas16FadeOutTcCurrent)/100);	
            }
                     
            if(tempW7 < MPRESS_45BAR)
            {
                MFC_Current_new = (int16_t)U8_TC_INIT_CURR_AT_LVBA_END + (((tempW7)/10)*5);
            }
            else
            {
                MFC_Current_new = 525 + (((tempW7-MPRESS_45BAR)/10)*4);
            }
        }
        else
        {
            MFC_Current_new = 0;
        }

    }
    else 
    {
        MFC_Current_new = 0;
    }

  #else
    MFC_Current_new = 0;
  #endif

    return MFC_Current_new;
}

/******************************************************************************
* FUNCTION NAME:      LCMSC_vSetLVBATargetVoltage
* CALLED BY:          LCMSC_vInterfaceTargetvol()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set Motor Target BEMF Voltage for LVBA
******************************************************************************/

    #if __ADVANCED_MSC
void LCMSC_vSetLVBATargetVoltage(void)
{
    int16_t lclvbas16EstWheelPressAvg;
    int16_t lclvbas16LVBAMscTargetVolOld = lvba_msc_target_vol;    
	int16_t lclvbas16LVBATemp01, lclvbas16LVBATemp02, lclvbas16LVBATemp03;

    lclvbas16EstWheelPressAvg = (FL.s16_Estimated_Active_Press+FR.s16_Estimated_Active_Press)/2;
    /*lclvbas16DelBoostPress = lclvbas16AssistPressure + mpress - lclvbas16EstWheelPressAvg; original*/
    lclvbas16DelBoostPress = lclvbas16AssistPressure + mpress - lclvbas16EstWheelPressAvg;    

    if(lclvbas16DelBoostPress < 0)
    {
    	lclvbas16DelBoostPress = 0;
    }

  #if __LVBA_MSC_DEBUG
    LVBA_MSC_DEBUGGER = 0;
  #endif

    if(LVBA_MSC_MOTOR_ON==1) 
    {
  	  #if __LVBA_MSC_DEBUG
    	LVBA_MSC_DEBUGGER = 1;
   	  #endif
        if((lclvbas16AssistPressBigRiseCnt > 0) || ((lclvbas1LVBAStartFlg==1) && ((lclvbau8LVBAOffCnt==0) || (lclvbau8LVBAOffCnt>T_200_MS))))
        {
            if(lclvbas16AssistPressureRate < MPRESS_10BAR)
            {
                lclvbas16AssistPressureRate = MPRESS_10BAR;
            }			
            if(lclvbas16VacuumLevel<=10)
            {
                lvba_msc_target_vol = LCESP_s16IInter4Point(lclvbas16AssistPressure, 	MPRESS_5BAR,   S16_INIT_TARGET_V_AT_VAC_FAIL1,
                    											 				 		MPRESS_10BAR,  S16_INIT_TARGET_V_AT_VAC_FAIL2,
                    											 				 		MPRESS_50BAR,  S16_INIT_TARGET_V_AT_VAC_FAIL3,
                    											 				 		MPRESS_100BAR, S16_INIT_TARGET_V_AT_VAC_FAIL4);
              #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 10;
              #endif
            }
            else if(lclvbas16EstWheelPressAvg <= MPRESS_10BAR)
            {
            	lclvbas16LVBATemp02 = LCESP_s16IInter3Point(lclvbas16VacuumLevel,   10, S16_INIT_TARGET_V_W_EST_P1,
            													 					20, S16_INIT_TARGET_V_W_EST_P2, 
            													 					25, S16_INIT_TARGET_V_W_EST_P3); 
            													 
            	lclvbas16LVBATemp01 = LCESP_s16IInter2Point(lclvbas16EstWheelPressAvg, 
            												   MPRESS_1BAR, lclvbas16LVBATemp02, 
            												   MPRESS_10BAR, S16_INIT_TARGET_VMAX_W_EST_P3);
            												   
            	lclvbas16LVBATemp03 = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 	10, 3,
            																		20, 3,
            													 					25, 1);

                lvba_msc_target_vol = LCESP_s16IInter4Point((lclvbas16AssistPressureRate*2+lclvbas16DelBoostPress)+(lclvbas16LVBAMpressSlop*lclvbas16LVBATemp03),
                    										MPRESS_5BAR,  lclvbas16LVBATemp01,
                    										MPRESS_10BAR, lclvbas16LVBATemp01,
                    										MPRESS_50BAR, lclvbas16LVBATemp01,
                    										MPRESS_100BAR, lclvbas16LVBATemp01);
              #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 11;
              #endif
            }
            else
            {
            	lclvbas16LVBATemp03 = LCESP_s16IInter3Point(lclvbas16AssistPressureRate, MPRESS_50BAR,  S16_INIT_TARGET_V_APR_VAC10_1,
																 						 MPRESS_100BAR, S16_INIT_TARGET_V_APR_VAC10_2,
																 						 MPRESS_150BAR, S16_INIT_TARGET_V_APR_VAC10_3);
				
				lvba_msc_target_vol = LCESP_s16IInter4Point(lclvbas16AssistPressure, 
                																MPRESS_5BAR,  S16_INIT_TARGET_V_AP_VAC10_1+lclvbas16LVBATemp03,
                    															MPRESS_10BAR, S16_INIT_TARGET_V_AP_VAC10_2+lclvbas16LVBATemp03,
                    															MPRESS_50BAR, S16_INIT_TARGET_V_AP_VAC10_3+lclvbas16LVBATemp03,
                    															MPRESS_100BAR,S16_INIT_TARGET_V_AP_VAC10_4+lclvbas16LVBATemp03);
              #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 12;
              #endif
            }
            
	       	if(vref>=VREF_100_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_H;
	       	}
	       	else if(vref>=VREF_60_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_M;
	       	}
	       	else if(vref>=VREF_40_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_L;
	       	}
	       	else
	       	{
	       		;
	       	}            
        }
        else
        {
          #if __LVBA_MSC_DEBUG
        	LVBA_MSC_DEBUGGER = 2;
          #endif
            /*lclvbas16LVBATemp01 = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 95, 3);*/
            
			if(lclvbas16VacuumLevel<=10)
            {
                lvba_msc_target_vol = LCESP_s16IInter4Point(lclvbas16AssistPressure,    MPRESS_5BAR,   S16_TARGET_V_AP_BW_VAC10_1,   
                    											 				 	    MPRESS_10BAR,  S16_TARGET_V_AP_BW_VAC10_2,
                    											 				 	    MPRESS_50BAR,  S16_TARGET_V_AP_BW_VAC10_3,
               		   											 				 	    MPRESS_100BAR, S16_TARGET_V_AP_BW_VAC10_4);                   											
			  #if __LVBA_MSC_DEBUG                		   											 				 	    
                LVBA_MSC_DEBUGGER = 20;
			  #endif
            }
            else
            {
               	lvba_msc_target_vol = LCESP_s16IInter4Point(lclvbas16AssistPressure,
                    																	MPRESS_5BAR,  S16_TARGET_V_AP_AB_VAC10_1,
                    																	MPRESS_10BAR, S16_TARGET_V_AP_AB_VAC10_2,
                    																	MPRESS_50BAR, S16_TARGET_V_AP_AB_VAC10_3,
                    																	MPRESS_100BAR,S16_TARGET_V_AP_AB_VAC10_4);
			  #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 21;
			  #endif
                
            }
            /*else
            {
                if((lclvbas16AssistPressure + mpress - lclvbas16EstWheelPressAvg <= -MPRESS_3BAR) && (lclvbas16AssistPressureRate*2+lclvbas16DelBoostPress<=MPRESS_10BAR))
                {
                	if(lclvbas16LVBAMpressSlop < 0)
                	{
                		lclvbas16LVBATemp01 = LCESP_s16IInter3Point(lclvbas16VacuumLevel,
                														 70,MSC_0_2_V,
                														 80,MSC_0_2_V, 
                														 90, MSC_0_25_V);
                	}
                }

                lvba_msc_target_vol = LCESP_s16IInter3Point(lclvbas16VacuumLevel,
                           											 60, MSC_2_V,
                           											 80, MSC_3_V,
                           											 90, MSC_4_V);
                           											                            									
                LVBAdebugger = 70;	
                if((vref <= VREF_5_KPH) && (lclvbas16VacuumLevel < 85))
                {
                	lvba_msc_target_vol = MSC_0_5_V;
                }
                else
                {
                	;
                }
                #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 8;
                #endif
                lvba_msc_target_vol = LCESP_s16IInter2Point(vref,                                         
     											   			VREF_50_KPH,  lvba_msc_target_vol+MSC_1_V,                  
     											   			VREF_100_KPH, lvba_msc_target_vol+MSC_5_V);  //tunning!!! 
            }*/
	       	if(vref>=VREF_100_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_H;	       		
	       	}
	       	else if(vref>=VREF_60_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_M;
	       	}
	       	else if(vref>=VREF_40_KPH)
	       	{
	       		lvba_msc_target_vol = lvba_msc_target_vol + S16_TARGET_V_COMP_SPEED_L;
	       	}
	       	else
	       	{
	       		;
	       	}            

            if(lvba_msc_target_vol < (lclvbas16LVBAMscTargetVolOld - MSC_0_5_V))
            {
                lclvbas8MscTargetVolDecCnt++;
                if((LVBA_MP_VALID==1)&&(mpress==0)) 
                {
                	lclvbas8MscTargetVolDecCnt=lclvbas8MscTargetVolDecCnt+4;
                }

                if(lclvbas8MscTargetVolDecCnt < ((lclvbas16LVBAMscTargetVolOld/MSC_1_V) + 0))
                {
                    lvba_msc_target_vol = lclvbas16LVBAMscTargetVolOld;
                 
                    /*#if __LVBA_MSC_DEBUG
                    LVBA_MSC_DEBUGGER = 30;
                    #endif*/
                }
                else
                {
                    lclvbas8MscTargetVolDecCnt = 0;
                    if((LVBA_MP_VALID==1)&&(mpress==0))
                    {
                        lvba_msc_target_vol = lclvbas16LVBAMscTargetVolOld-MSC_1_V;
                    }
                    else
                    {
                        lvba_msc_target_vol = lclvbas16LVBAMscTargetVolOld-MSC_0_5_V;
                    }
                 
                    /*#if __LVBA_MSC_DEBUG
                    LVBA_MSC_DEBUGGER = 31;
                    #endif*/
                }
                lclvbas8MscTargetVolIncCnt = 0;
            }
            else if(lvba_msc_target_vol > (lclvbas16LVBAMscTargetVolOld))
            {
                lclvbas8MscTargetVolDecCnt = 0;
                
                if(lclvbau8LVBAOnCnt<20)
                {
                	lclvbas16LVBATemp01 = (LCESP_s16IInter3Point(lclvbas16LVBAMpressSlop,
                															 				1, MSC_0_2_V,
                															 				5, MSC_0_5_V,
                														     				20,MSC_2_V))/2;
                															
                	if((vref<=VREF_0_125_KPH)&&(lclvbas16LvbaEnterSpeed>VREF_0_125_KPH)) 
                	{
                		lclvbas16LVBATemp01=0;
                	}
                	
                	if((lvba_msc_target_vol-lclvbas16LVBAMscTargetVolOld)>lclvbas16LVBATemp01) 
                	{
                		lvba_msc_target_vol = lclvbas16LVBAMscTargetVolOld + lclvbas16LVBATemp01;
                	}
                }
                /*#if __LVBA_MSC_DEBUG    
                LVBA_MSC_DEBUGGER = 41; 
                #endif*/
            }
            else
            {
                if(lclvbas8MscTargetVolIncCnt >= 2)
                {
                    lclvbas8MscTargetVolIncCnt = lclvbas8MscTargetVolIncCnt - 2;
                }
                else
                {
                    lclvbas8MscTargetVolIncCnt = 0;
                }

                if(lclvbas8MscTargetVolDecCnt >= 2)
                {
                    lclvbas8MscTargetVolDecCnt = lclvbas8MscTargetVolDecCnt - 2;
                }
                else
                {
                    lclvbas8MscTargetVolDecCnt = 0;
                }
            }	
        }             
       	
        if(((vref<=VREF_0_125_KPH)&&(lclvbau8LVBAOnCnt>T_300_MS)) || (lclvbas16LVBAReleaseCnt>=T_500_MS))/*when braking to stop after driving*/
        {
        	if(lvba_msc_target_vol > MSC_4_V)
        	{
        		lvba_msc_target_vol = MSC_4_V;
        	}
        }
        
        lclvbas16MtrOffTimeAtLowSpd = LCESP_s16IInter2Point(lclvbas16LvbaEnterSpeed, VREF_0_25_KPH, T_100_MS, 
        															 				 VREF_2_KPH, 	T_50_MS);
        													         													 
        if(lclvbau8LVBAStopApply == 1) 
        {
        	lclvbas16MtrOffTimeAtLowSpd=LCESP_s16IInter2Point(lclvbas16VacuumLevel, 0,				    		  T_2_S,
            															 			U8_LVBA_ACT_VACUUM_LEVEL, T_1000_MS); 
            															 			        	
        	if (lclvbau8LVBAOnCnt <= T_300_MS)
        	{
        		lvba_msc_target_vol = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 	0,  U16_INIT_TARGET_VOLT_AT_STOP_L1,
        																			10, U16_INIT_TARGET_VOLT_AT_STOP_L2,
        																			25, U16_INIT_TARGET_VOLT_AT_STOP_L3);
        	  #if __LVBA_MSC_DEBUG
        		LVBA_MSC_DEBUGGER = 50;
        	  #endif
        	}
        	else
        	{
        		lvba_msc_target_vol = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 	0,  U16_TARGET_VOLT_AT_STOP_L1,
        																			10, U16_TARGET_VOLT_AT_STOP_L2,
        																			25, U16_TARGET_VOLT_AT_STOP_L3);
        	  #if __LVBA_MSC_DEBUG
        		LVBA_MSC_DEBUGGER = 51;
        	  #endif
        	}
        	
        }

        if((vref<=VREF_0_125_KPH)&&(lclvbas16LVBAReleaseCnt>=lclvbas16MtrOffTimeAtLowSpd))
        {
            if((lclvbas16LVBAMscTargetVolOld > 0) && (vref<=VREF_0_125_KPH))
            {
                lvba_msc_target_vol = S16_TARGET_V_ON_CAR_STOP;
                /*LVBA_MSC_MOTOR_ON = 0;*/
                
              #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 60;
              #endif
            }
            else if(lclvbas16LVBAMscTargetVolOld==0)
            {
                lvba_msc_target_vol = 0; 
                LVBA_MSC_MOTOR_ON = 0;
                
              #if __LVBA_MSC_DEBUG
                LVBA_MSC_DEBUGGER = 61;
              #endif
            }
            else 
            { 
            }
        }
		else { }
		
		lvba_msc_target_vol = LCABS_s16LimitMinMax(lvba_msc_target_vol, 0, MSC_10_V);

        if(lclvbas16AssistPressBigRiseCnt==0)
        {
            if(lclvbas16AssistPressureRate>=MPRESS_150BAR) 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt+2;
            }
        }
        else
        {
            if(lclvbas16AssistPressureRate>=MPRESS_150BAR) 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt+2;
            }
            else if(lclvbas16AssistPressureRate>=MPRESS_100BAR) 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt+1;
            }
            else if(lvba_msc_target_vol > MSC_5_V) 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt+2;
            }
            else if(lvba_msc_target_vol > MSC_3_V) 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt+1;
            }
            else 
            {
            	lclvbas16AssistPressBigRiseCnt = lclvbas16AssistPressBigRiseCnt-1;
            }
        }
        /**09SW**/
        if (( vref < VREF_30_KPH ) && (lclvbas16VacuumLevel > 10))
        {
        	max_lvba_msc_target_vol = LCESP_s16IInter3Point(vref,
        											VREF_30_KPH, MSC_4_V,
        											VREF_10_KPH, MSC_2_5_V,
        											VREF_5_KPH,  MSC_1_5_V);
        	
        	if (lvba_msc_target_vol > max_lvba_msc_target_vol)
        	{
        		lvba_msc_target_vol = max_lvba_msc_target_vol;
        	}
        }   			  
        /**09SW**/	
    }
    else 
    {
        lvba_msc_target_vol = 0;
        lclvbas16LVBAMtrOffCnt=0;
        lclvbas16AssistPressBigRiseCnt=0;
    }


    if((LVBA_MSC_MOTOR_ON==1)&&(lvba_msc_target_vol>0)&&(ABS_MSC_MOTOR_ON==1))
    {
        LVBA_ABS_COMB_MSC_MOTOR_ON=1;
        lvba_msc_target_vol = MSC_2_V;
    }
    else
    {
        LVBA_ABS_COMB_MSC_MOTOR_ON=0;
    }

    if(lvba_msc_target_vol==0) 
    {
    	LVBA_MSC_MOTOR_ON = 0;
    }
    if((lvba_msc_target_vol<MSC_0_1_V)&&(lvba_msc_target_vol>0)) 
    {
    	lvba_msc_target_vol = MSC_0_1_V;
    }
}
    #endif/*__ADVANCED_MSC*/
    
#endif/*__LVBA*/
