#ifndef __LCHRCCALLCONTROL_H__
#define __LCHRCCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"


  #if defined(__dDCT_INTERFACE)
 
/*Global Type Declaration ****************************************************/
#define	lcu1HrcInhibitFlg		HRCF0.bit7
#define	HRC_TM_TYPE			    HRCF0.bit6
#define	lcu1HrcOnReqFlg			HRCF0.bit5
#define	hrc_non_use_5_4	        HRCF0.bit4
#define	hrc_non_use_5_3			HRCF0.bit3
#define	hrc_non_use_5_2			HRCF0.bit2
#define	hrc_non_use_5_1			HRCF0.bit1
#define	hrc_non_use_5_0			HRCF0.bit0

/*  HSA Flag1 (HSA CONTROL)  */
#define hrc_non_use_4_7			HRCF1.bit7
#define hrc_non_use_4_6			HRCF1.bit6
#define hrc_non_use_4_5			HRCF1.bit5
#define hrc_non_use_4_4			HRCF1.bit4
#define hrc_non_use_4_3			HRCF1.bit3
#define hrc_non_use_4_2			HRCF1.bit2
#define hrc_non_use_4_1			HRCF1.bit1
#define hrc_non_use_4_0			HRCF1.bit0

/*  HSA Flag2 (INPUT SETTING)  */
#define hrc_non_use_3_7	        HRCF2.bit7
#define hrc_non_use_3_6	        HRCF2.bit6
#define hrc_non_use_3_5	        HRCF2.bit5
#define hrc_non_use_3_4	        HRCF2.bit4
#define hrc_non_use_3_3	        HRCF2.bit3
#define hrc_non_use_3_2	        HRCF2.bit2
#define hrc_non_use_3_1	        HRCF2.bit1
#define hrc_non_use_3_0	        HRCF2.bit0

#define hrc_non_use_2_7			HRCF3.bit7
#define hrc_non_use_2_6			HRCF3.bit6
#define hrc_non_use_2_5			HRCF3.bit5
#define hrc_non_use_2_4			HRCF3.bit4
#define hrc_non_use_2_3			HRCF3.bit3
#define hrc_non_use_2_2			HRCF3.bit2
#define hrc_non_use_2_1			HRCF3.bit1
#define hrc_non_use_2_0			HRCF3.bit0

#define hrc_non_use_1_7			HRCF4.bit7
#define hrc_non_use_1_6			HRCF4.bit6
#define hrc_non_use_1_5    		HRCF4.bit5
#define hrc_non_use_1_4         HRCF4.bit4
#define hrc_non_use_1_3         HRCF4.bit3 
#define hrc_non_use_1_2         HRCF4.bit2
#define hrc_non_use_1_1	    	HRCF4.bit1
#define hrc_non_use_1_0         HRCF4.bit0

#define hrc_non_use_0_7		    HRCF5.bit7
#define hrc_non_use_0_6         HRCF5.bit6
#define hrc_non_use_0_5    		HRCF5.bit5
#define hrc_non_use_0_4         HRCF5.bit4
#define hrc_non_use_0_3         HRCF5.bit3 
#define hrc_non_use_0_2         HRCF5.bit2
#define hrc_non_use_0_1	    	HRCF5.bit1
#define hrc_non_use_0_0      	HRCF5.bit0




/**************** macro setting ***********************/
#define __TOSS_SIGNAL_USE				0
#define __TEST_MODE_ESC_DISABLE_SW  	1

////////////////////////////////////////
#define HRC_AUTO_TM				1
#define HRC_MANUAL_TM			0

#define HRC_DRIVE				1
#define HRC_REVERSE				2
#define HRC_NEUTRAL				3
////////////////////////////////////////

#define U8_dDCT_HSA_ACT_LEVEL1_SLOPE           5
#define U8_dDCT_HSA_ACT_LEVEL2_SLOPE		   4	
#define U8_dDCT_HSA_ACT_LEVEL3_SLOPE           3

#define U8_HRC_HSA_FO_Rate_MinG_LV3 		   3	   
#define U8_HRC_HSA_FO_Rate_LowMedG_LV3         3
#define U8_HRC_HSA_FO_Rate_HighMedG_LV3        3
#define U8_HRC_HSA_FO_Rate_MaxG_LV3            3
   
#define U8_HRC_HSA_FO_Rate_MinG_LV2            2    
#define U8_HRC_HSA_FO_Rate_LowMedG_LV2         2
#define U8_HRC_HSA_FO_Rate_HighMedG_LV2        2
#define U8_HRC_HSA_FO_Rate_MaxG_LV2            2

#define U8_HRC_HSA_FO_Rate_MinG_LV1            1
#define U8_HRC_HSA_FO_Rate_LowMedG_LV1         1
#define U8_HRC_HSA_FO_Rate_HighMedG_LV1        1
#define U8_HRC_HSA_FO_Rate_MaxG_LV1            1

#define S16_HRC_TARGET_SPD_LV0				   VREF_6_KPH
#define S16_HRC_TARGET_SPD_LV1				   VREF_5_KPH
#define S16_HRC_TARGET_SPD_LV2				   VREF_4_KPH
#define S16_HRC_TARGET_SPD_LV3				   VREF_3_KPH
	
#define S16_Hrc_Ctrl_Under_Speed_Target_G_1st	2
#define S16_Hrc_Ctrl_Under_Speed_Target_G_2nd	3
#define S16_Hrc_Ctrl_Under_Speed_Target_G_3rd	6


#define S16_HRC_MIN_INITIAL_CURRENT             300
/*Global Extern Variable  Declaration*****************************************/
extern struct	U8_BIT_STRUCT_t HRCF0, HRCF1, HRCF2, HRCF3, HRCF4, HRCF5;



extern uint8_t			lcu8HrcTmReqHsaActLevel, lcu8HrcTmReqHsaDecayLevel, lcu8HrcTmReqHrcTargetSpdLevel, lcu8HrcGearState, lcu8HrcInitMotReqCnt;

extern int16_t			lcs16HrcReqHsaActMinSlope, lcs16HrcReqHsaDecayRate,lcs16HrcTargetSpd;




/*Global Extern Functions  Declaration****************************************/
extern  void 	LCHRC_vCallControl(void);

  #endif
#endif
