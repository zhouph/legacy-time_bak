/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LDCallMain.c
* Description:      Detection Main Code of Logic
* Logic version:    HV25
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C12         eslim           Initial Release
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDCallMain
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/


#include "LDCallMain.h"
/*#include "LDPBAEstPressure.h"*/
#if __ESS==ENABLE
#include "LDESSCallEmergencyBrkDet.h"
#endif
#if __CBC
#include "LCCBCCallControl.h"
#endif
#if __ROP
#include "LDROPCallDetection.h"
#endif

#include "LDTCSCallCalculation.h"
/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/
void LD_vCallMainDetection(void);
extern void LDESP_vActPreAction(void);


/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LD_vCallMainDetection
* CALLED BY:            L_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Detection Code of each system
********************************************************************************/
void LD_vCallMainDetection(void)
{
    LDABS_vCallDetection();

#if __CBC
    LDCBC_vCallEstimation();
#endif    

#if __EDC || __ETC || __TCS		/* ABS_PLUS_ECU 적용시 macro 삭제 예정 */
    LDENG_vCallDetection();
#endif    

#if __BTC
  #if	defined(BTCS_TCMF_CONTROL)
    LDTCS_vCallCalculation();
  #endif    /* BTCS_TCMF_CONTROL */
    LDTCS_vCallDetection();
#endif

#if __ETSC
	LDETSC_vCallDetection();
#endif

#if __ROP
	LDROP_vCallDetection(); 
#endif 

#if __VDC
   #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    LDESP_vUpdateState();
   #endif

    LDESP_vCallDetection();
#endif

#if defined(__CDC)
    LDCDC_vCallDetection();
#endif

#if __VSM
 	LDEPS_vCallDetection();		/* EPS협조제어 전용 */
#endif	

#if __TVBB
	LDTVBB_vCallDetection();
#endif

#if __TSP
    LDTSP_vCallDetection();
#endif

#if __FBC
    LDFBC_vCallDetection();
#endif

#if __BDW
    LDBDW_vCallDetection();
#endif

#if __EBP
    LDEBP_vCallDetection();
#endif

#if __ENABLE_PREFILL
    LDESP_vActPreAction();
#endif

#if __SCC
    LDSCC_vCallDection();
#endif

#if __ESS
	LDABS_vCallEmergencyBrkMain();
#endif

#if (__DDS == ENABLE)
    LDDDS_vCallDetection();
    #if (__DDS_PLUS == ENABLE)
    LDDDSp_vCallDetection();
    #endif
    
    LDDDS_vDetTireLowPressureState();
#endif
}
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDCallMain
	#include "Mdyn_autosar.h"
#endif