
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_Lbtcs_Neo
	#include "Mdyn_autosar.h"               
#endif



//uint8_t ee_btcs_data; //kks
#define __BTCS_NEO_STRUCTURE 0

#if __BTCS_NEO_STRUCTURE

#define BTCS_HOLD_TIME1                         7       //COUNT20=0,BLS,HIGH_SLIP=1,평균7<-5kph일때
#define BTCS_HOLD_TIME2                         7       //CNT20=0,BLS,H_SL=1,평균7<=0kph,차량SLIP<=12kph일때
#define BTCS_HOLD_TIME3                         6       //CNT20=0,BLS,H_SL=1,평균7<=0kph,차량SLIP>12kph일때
#define BTCS_HOLD_TIME4                         1       //COUNT20=0,BLS,HIGH_SLIP=1,평균7>=-5kph일때
#define BTCS_HOLD_TIME5                         2       //COUNT20=0,BLS,HIGH_SLIP=1,상기 이외의 조건일때
#define BTCS_HOLD_TIME6                         8       //차량SLIP>=절대SLIP,SLIP증가량>14kph일때
#define BTCS_HOLD_TIME7                         10      //차량SLIP>=절대SLIP,SLIP증가량>10kph일때
#define BTCS_HOLD_TIME8                         13      //차량>=절대SLIP,SLIP증가량>7kph,SLIP차>30kph일때
#define BTCS_HOLD_TIME9                         14      //차량>=절대SLIP,SLIP증가량>6kph,SLIP차>25kph일때
#define BTCS_HOLD_TIME10                        15      //차량>=절대SLIP,SLIP증가량>5kph,SLIP차>20kph일때
#define BTCS_HOLD_TIME11                        16      //차량>=절대SLIP,SLIP증가량>4kph,SLIP차>15kph일때
#define BTCS_HOLD_TIME12                        17      //차량SLIP>=절대SLIP,SLIP증가량>0.5kph일때
#define BTCS_HOLD_TIME13                        18      //차량SLIP>=절대SLIP,상기 이외의 조건일때
#define BTCS_HOLD_TIME14                        16      //차량SLIP<절대SLIP,SLIP증가량<=-절대SLIP일때
#define BTCS_HOLD_TIME15                        17      //차량SLIP<절대SLIP,SLIP증가량<=0kph일때
#define BTCS_HOLD_TIME16                        18      //차량SLIP<절대SLIP,SLIP증가량>0kph일때
#define BTCS_HOLD_TIME17                        17      //차량SLIP<절대SLIP,이외 조건,차량SLIP<=절대SLIP일때
#define BTCS_HOLD_TIME18                        18      //차량SLIP<절대SLIP,이외 조건,차량SLIP>절대SLIP일때
#define BTCS_HOLD_TIME19                        14      //BAND내 SLIP,SLIP증가>5kph일때
#define BTCS_HOLD_TIME20                        17      //BAND내 SLIP,SLIP증가>절대SLIP+2kph일때
#define BTCS_HOLD_TIME21                        18      //BAND내 SLIP,상기 이외의 조건일때

#define BTCS_INC_COUNT1                         9       //차량SLIP>=절대SLIP,SLIP증가량>14kph일때
#define BTCS_INC_COUNT2                         7       //차량SLIP>=절대SLIP,SLIP증가량>10kph일때
#define BTCS_INC_COUNT3                         4       //차량>=절대SLIP,SLIP증가량>7kph,SLIP차>30kph일때
#define BTCS_INC_COUNT4                         3       //차량>=절대SLIP,SLIP증가량>6kph,SLIP차>25kph일때
#define BTCS_INC_COUNT5                         2       //차량>=절대SLIP,SLIP증가량>5kph,SLIP차>20kph일때
#define BTCS_INC_COUNT6                         1       //차량>=절대SLIP,SLIP증가량>4kph,SLIP차>15kph일때
#define BTCS_INC_COUNT7                         3       //BAND내 SLIP,SLIP증가>5kph일때

#define BTCS_DEC_COUNT1                         1       //차량SLIP<절대SLIP,SLIP증가량<=-절대SLIP일때

#define BTCS_MOT_RUNTIME1                       4       //BTCS MOTOR RUN-TIME COUNT20이 ON일때
#define BTCS_MOT_RUNTIME2                       6       //BTCS MOTOR RUN-TIME POS_HOLD가 ON일때
#define BTCS_MOT_RUNTIME3                       10      //BTCS MOTOR RUN-TIME EXIT TIMER

#define TCS_Conven_Reapply_Time_Front           10
#define TCS_Conven_Reapply_Time_Rear            7

#if __BTC
#if !SIM_MATLAB
void    BTCSET(void);
#endif
void    EXITBTCS(void);
void    BTCS_SPIN_CHECK(void);
void    ABDMOTOR(void);
void	BTC_VIB_SET_wl(void);

void    TCSTATE1(void);
void    TCSTATE2(void);
void    BTCTEMP(struct W_STRUCT *WL_temp);
void    TEMPCALC(void);
void    TEMPERATUREC(void);
void    TMP_OFF(struct W_STRUCT *WL);

void    SET_fl(void);
void    SET_fr(void);
#if __4WD || __REAR_D
void    SET_rl(void);
void    SET_rr(void);
#endif
void    RET_fl(void);
void    RET_fr(void);
#if __4WD || __REAR_D
void    RET_rl(void);
void    RET_rr(void);
#endif

#if __4WD
void    BTCSTAGET(void);
#endif

#define BTCS_EXIT_NO_CHECKED	0
#define BTCS_EXIT_CHECKED		1
#define BTCS_EXIT_CONFIRMED		2

#define BTCS_TEMP_NO_CHECKED	0
#define BTCS_TEMP_CHECKED		1

#define	BTCS_STATE1_NO_CHECKED	0
#define	BTCS_STATE1_CHECKED		1
	
#define	BTCS_STATE1_DEFAULT		0
#define	BTCS_STATE1_INC			1
#define	BTCS_STATE1_HOLD		2
#define	BTCS_STATE1_DUMP0		3
#define	BTCS_STATE1_DUMP1		4

#define	BTCS_STATE2_NO_CHECKED 	0
#define	BTCS_STATE2_CHECKED 	1
		
#define	BTCS_STATE2_DEFAULT 	0
#define	BTCS_STATE2_INC 		1
#define	BTCS_STATE2_HOLD 		2
#define	BTCS_STATE2_DUMP2 		3

void    EXITBTCS_CHECK_ERROR(void);
void    EXITBTCS_CHECK_ESP(void);
void    EXITBTCS_CHECK_PEDAL(void);
void    EXITBTCS_CHECK_SPEED(void);	
void    EXITBTCS_CHECK_SPIN(void);
void    EXITBTCS_CHECKED(void);
void    EXITBTCS_CONFIRMED(void);
void    EXITBTCS_DEFAULT_SETTING();

void    TCSTATE1_COMP_DV_THRESHOLD(void);
void    TCSTATE1_DEFAULT_SETTING(void);
void    TCSTATE1_DUMP_COUNT_CHECK(void);
void    TCSTATE1_DUMP_HOMO_MU(void);
void    TCSTATE1_DUMP_LOW_RPM(void);
void    TCSTATE1_DUMP_LOW_SIDE(void);
void    TCSTATE1_DUMP_HIGH_SIDE(void);

void    TCSTATE1_INC_COUNT_CHECK(void);
void    TCSTATE1_INC_LOW_SIDE(void);
	
void    TCSTATE1_HOLD_COUNT_CHECK(void);
void    TCSTATE1_HOLD_DEFAULT(void);
	
void    TCSTATE1_HOLD_COUNT(void);	

void    TCSTATE1_HOLD_MODE(void);
void    TCSTATE1_INC_MODE(void);
void    TCSTATE1_DUMP0_MODE(void);
void    TCSTATE1_DUMP1_MODE(void);

void    TCSTATE2_COMP_DV_THRESHOLD(void);
void    TCSTATE2_DEFAULT_SETTING(void);
void    TCSTATE2_DUMP_COUNT_CHECK(void);
void    TCSTATE2_DUMP_LOW_RPM(void);
void    TCSTATE2_DUMP_LOW_SIDE(void);
void    TCSTATE2_DUMP_HIGH_SIDE(void);

void    TCSTATE2_INC_COUNT_CHECK(void);
void    TCSTATE2_INC_LOW_SIDE(void);
    
void    TCSTATE2_HOLD_DEFAULT(void);

void    TCSTATE2_HOLD_MODE(void);
void    TCSTATE2_INC_MODE(void);
void    TCSTATE2_DUMP2_MODE(void);
void    TCSTATE2_HOLD_COUNT(void);

/* ====================================================================== */
#if !SIM_MATLAB
void    BTCSET(void)
{
    av_fspeed_fl=av_fspeed_fr=av_fspeed_rl=av_fspeed_rr=(int16_t)VREF_MIN;
    tc_on_flg=1;
    btc_tmp_fl=btc_tmp_fr=btc_tmp_rl=btc_tmp_rr=(int16_t)TMP_20;
    if((ee_btcs_data & 0x10) != 0) BLS_EEPROM = 1;
    if((ee_btcs_data & 0x20) != 0) {
        btc_tmp_fl = ee_tmp_fl;
        btc_tmp_fr = ee_tmp_fr;
        btc_tmp_fl <<=8;
        btc_tmp_fr <<=8;
        btc_tmp_rl = ee_tmp_rl;
        btc_tmp_rr = ee_tmp_rr;
        btc_tmp_rl <<=8;
        btc_tmp_rr <<=8;
    }
}
#endif

void    EXITBTCS_CHECKED(void)
{
    TCS_VALVE_EXIT_wl=1;
    if(WL->valve_delay_count<8) WL->valve_delay_count++;
    else EXITBTCS_CONFIRMED();
}

void    EXITBTCS_CONFIRMED(void)
{
    BTCS_wl=0;
    TCL_DEMAND_wl=BUILT_wl=0;
    POS_HOLD_wl=COUNT20_wl=OPT_NEWSET_wl=HILL_wl=0;
    TCS_VALVE_EXIT_wl=0;
    WL->btc_slip=WL->fspeed=WL->ffspeed=WL->av_fspeed=(int16_t)VREF_MIN;
    WL->average7=(int16_t)VREF_0_KPH;
    WL->av_ref=0;
    WL->tmp_state=WL->btc_count1=WL->valve_delay_count=WL->vib_count1=0;
    WL->inc_count3 = 0;	
    	
    WL->exit_timer1 = 0;
#if __TCS
    WL->exit_timer2 = 0;
    WL->exit_timer3 = 0;
    WL->exit_timer5 = 0;
#endif

    WL->state=DETECTION;	
		
	Btcs_Exit_Check		= BTCS_EXIT_NO_CHECKED;
	Btcs_Temp_Check		= BTCS_TEMP_NO_CHECKED;
	Btcs_State1_Check	= BTCS_STATE1_NO_CHECKED;
	Btcs_State1_Mode	= BTCS_STATE1_DEFAULT;	
	Btcs_State2_Check	= BTCS_STATE2_NO_CHECKED;
	Btcs_State2_Mode	= BTCS_STATE2_DEFAULT;	
	
	WL->BTCS_flags		= 0;
}

/* ====================================================================== */
void    EXITBTCS_DEFAULT_SETTING(void)
{
    if(TMP_ERROR_wl==1) tc_error_flg=1;
    TCS_VALVE_EXIT_wl=0;
    
    Btcs_Exit_Check = BTCS_EXIT_NO_CHECKED;
    
#if __VDC
    tempW0 = (int16_t)(WL->vrad - WL->wvref);
    tempW1 = (int16_t)(WL->av_fspeed - WL->wvref);
    tempW5 = (int16_t)(WL->vrad_crt - WL->wvref);
#else
    tempW0 = (int16_t)(WL->vrad - vref);
    tempW1 = (int16_t)(WL->av_fspeed - vref);
    tempW5 = (int16_t)(WL->vrad_crt - vref);
#endif

#if __REAR_D
    tempW2 = (int16_t)(vrad_fl - vrad_fr);
#else
    tempW2 = (int16_t)(vrad_rl - vrad_rr);
#endif
    if(tempW2 < 0) tempW2 = (int16_t)-tempW2;	
}

/* ====================================================================== */
void    EXITBTCS_CHECK_ERROR(void)
{
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
    	if(tcs_error_flg == 1) {
    		WL->flags=17;
   			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED; 
    	}
    	if(tc_error_flg == 1) {
    		WL->flags=18;
   			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED; 
    	}
    	if(tc_on_flg == 0) {
    		WL->flags=19;
   			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED; 
    	}
    	if(under_v_flg == 0) {
    		WL->flags=20;
   			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED; 
    	}
    }	
}

/* ====================================================================== */
void    EXITBTCS_CHECK_ESP(void)
{
#if __VDC
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    if((vdc_on_flg)&&(!tcs_error_flg)) {
        	if((ESP_SPLIT==0)&&(YAW_CDC_WORK==1)) {
        		WL->flags=4;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        		
        	}
        	#if __4WD
        	if(ITM_4WD_LOCK) {
        		WL->flags=5;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        		
        	}
        	#endif
		}
    }
#endif	
}

/* ====================================================================== */
void    EXITBTCS_CHECK_PEDAL(void)
{	
#if __VDC
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    if((vdc_on_flg)&&(!tcs_error_flg)) {
    	#if !__FOR_DV
        	if(MPRESS_BRAKE_ON==1) {
        		WL->flags=3;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;
    	    }
	    #endif
	    }
	}
#else
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    if((BLS_ERROR!=1)&&(BLS_EEPROM!=1)) {                   // MAIN05
    	    if(BLS==1) {
        	    if(WL->s_diff <= (int16_t)VREF_0_KPH) {
            		WL->flags=7;
	    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        		
            	}
        	}
        }
    }   
#endif		

#if __TCS && !SIM_MATLAB
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    if(mtp <= BTCS_EXT_MTP) {
    	    if(tempW0<(int16_t)VREF_5_KPH) {
	    		WL->flags=21;
    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;
        	}
        	#if __4WD
	    	    if(vref<(int16_t)VREF_3_KPH) {							// jt_031014
    		    	WL->exit_timer5+=1;
	        		if(WL->exit_timer5 > 70) {
		    			WL->flags=22;
	    				Btcs_Exit_Check = BTCS_EXIT_CHECKED;
        			}
    	    	}
	        	else WL->exit_timer5=0;        	        	
        	#endif
    	}
    	else {
    		WL->exit_timer5=0;
    	}
	}
#endif
}

/* ====================================================================== */
void    EXITBTCS_CHECK_SPEED(void)
{
#if __TCS
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    if(vref >= (int16_t)(BTCS_EXT_VREF_MAX*8)) {
    		WL->flags=1;
    		Btcs_Exit_Check = BTCS_EXIT_CHECKED;
    	}
    }
#else
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
		if(vref >= (int16_t)(BTCS_EXT_VREF_BTCS*8)) {
    		WL->flags=2;
    		Btcs_Exit_Check = BTCS_EXIT_CHECKED;
    	}
    }
#endif	

	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{   
		#if __4WD
    	if(vref2 > (int16_t)VREF_8_KPH) {
	        if(vref2 <= vref_alt) {
        	    if(WL->exit_timer3 > 80) {
        	    	WL->flags=24;
    				Btcs_Exit_Check = BTCS_EXIT_CHECKED;
    			}
    	        else {
	                if(vref2<vref_alt) WL->exit_timer3+=2;
                	else WL->exit_timer3+=1;
            	}
        	}
    	    else WL->exit_timer3 = 0;
	    }		
		
		#else
	    if(WL->state == BTCS_STATE2) {
    	    if(tempW0 < (int16_t)VREF_10_KPH) {
        	    if(vref <= vref_alt) {
            	    if(WL->exit_timer3 > 100) {
                	    WL->flags=25;
		    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;                	    
	                }
    	            else {
        	            if(vref<vref_alt) WL->exit_timer3+=2;
            	        else WL->exit_timer3+=1;
                	}
	            }
    	        else WL->exit_timer3 = 0;
        	}
        	else WL->exit_timer3 = 0;
        }
        #endif
    }
}

/* ====================================================================== */
void    EXITBTCS_CHECK_SPIN(void)
{
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
	    tempW4 = tempW2;
    	tempW4 += (int16_t)VREF_3_KPH;
	    if(tempW0 < (int16_t)(-tempW4)) {
    	    BTC_LOW_SLIP_wl = 1;
        	WL->hold_count = 0;
        	WL->flags=6;
    		Btcs_Exit_Check = BTCS_EXIT_CHECKED;
    	}
    }	

#if !__TCS
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
		if(BTCS_wl && !LEFT_WHEEL_wl) {
			if((arad_fl <= -8)&&((vref - vrad_fl) >= VREF_5_KPH)) {
				WL->flags=8;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						
			}
			if((arad_rr <= -8)&&((vref - vrad_rr) >= VREF_5_KPH)) {
				WL->flags=9;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						
			}
		}
		if(BTCS_wl && LEFT_WHEEL_wl) {
			if((arad_fr <= -8)&&((vref - vrad_fr) >= VREF_5_KPH)) {
				WL->flags=10;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						
			}
			if((arad_rl <= -8)&&((vref - vrad_rl) >= VREF_5_KPH)) {
				WL->flags=11;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						
			}
		}
	}
#endif

	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{
		if(U16_BTCS_TWO_ON_HOMO_MU)
		{
    		if(vref>(int16_t)VREF_10_KPH) {
        		if(BTCS_TWO_OFF) 
        		{
        			WL->flags=13;
    				Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						        			
        		}
    		}    
    
		    if(BTCS_TWO_OFF) {
    			#if	__REAR_D
		    		if(BTCS_rl && inc_count3_rl > 213) {
		    			WL->flags=14;
		    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;
		    		}
   	 				if(BTCS_rr && inc_count3_rr > 213) {
   	 					WL->flags=15;
   	 					Btcs_Exit_Check = BTCS_EXIT_CHECKED;
		    		}        	
    			#else
	    			if(BTCS_fl && inc_count3_fl > 213) {
	    				WL->flags=14;
	    				Btcs_Exit_Check = BTCS_EXIT_CHECKED;
	    			}
		    		if(BTCS_fr && inc_count3_fr > 213) {
		    			WL->flags=15;
		    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;
		    		}    	
    			#endif    
    		}
    	}
		else	
		{
	    	if(BTCS_TWO_OFF) {
    			WL->flags=16;
    			Btcs_Exit_Check = BTCS_EXIT_CONFIRMED;        						        			
    		}
		}
	} 
    
#if __TCS
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{       	
    	if(ETCS_ON) {
        	if(tempW0<(int16_t)VREF_5_KPH) {
	    		WL->flags=23;
    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;
        	}
    	}
    }
#endif

	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{   
	  #if __4WD    	
    	if((tempW0<(int16_t)VREF_2_KPH)||(tempW1<(int16_t)VREF_2_KPH)) {
        	if(WL->exit_timer1 >= 70) {
    	        WL->flags=26;
    			Btcs_Exit_Check = BTCS_EXIT_CHECKED;    	            
        	}
    	    else {
	            WL->exit_timer1++;
            }
    	}
    	else
    	{
	    	WL->exit_timer1 = 0;
	    }
	  #else

	    tempW4 = tempW2;
    	tempW4 += (int16_t)VREF_2_KPH;
	    if(tempW2<(int16_t)VREF_8_KPH) {                            // GK36
    	    if((tempW0<tempW4)||(tempW5<tempW4)) {
        		//=================================================================
        		//	for mini wheel exit time by Kim Jeonghun in 2004.3.2
	        	//-----------------------------------------------------------------
    	    	if(MINI_SPARE_WHEEL_wl && (tempW5<tempW4) && (WL->exit_timer1 > 14)) {
        	        WL->flags=27;
    				Btcs_Exit_Check = BTCS_EXIT_CHECKED;        	        
        		}
        	
	            if(WL->exit_timer1 > 70) {
    	            WL->flags=28;
    				Btcs_Exit_Check = BTCS_EXIT_CHECKED;    	            
            	}
	            else {
    	            WL->exit_timer1++;
            	}
        	}
        }
        else
        {
		    WL->exit_timer1 = 0;        
        }
      #endif
    }
    
	if(Btcs_Exit_Check == BTCS_EXIT_NO_CHECKED) 
	{       
		//===========================================================================
		//	Brake two wheel on ice by Kim Jeonghun in 2004.2.24
		//---------------------------------------------------------------------------
		#if __TCS
			if(U16_BTCS_TWO_ON_HOMO_MU==0)
			{

    			if((FTCS_ON == 0) && (ETCS_ON == 1)) {
					#if __REAR_D
        				if(vrad_rl > vrad_rr) tempW2 = (int16_t)(vrad_rl - vrad_rr);
        				else tempW2 = (int16_t)(vrad_rr - vrad_rl);
					#else
        				if(vrad_fl > vrad_fr) tempW2 = (int16_t)(vrad_fl - vrad_fr);
        				else tempW2 = (int16_t)(vrad_fr - vrad_fl);
					#endif
        			
        			if(tempW2 <= (int16_t)VREF_2_KPH) WL->exit_timer2++;
        			else WL->exit_timer2 = 0;

        			if(WL->exit_timer2 >= 30) {
        				WL->flags=29;
    					Btcs_Exit_Check = BTCS_EXIT_CHECKED;        				
        			}
    			}
			}
		#endif
	}	
}

/* ====================================================================== */
void    EXITBTCS(void)
{
	EXITBTCS_DEFAULT_SETTING();
	EXITBTCS_CHECK_ERROR();
	EXITBTCS_CHECK_ESP();
	EXITBTCS_CHECK_PEDAL();
	EXITBTCS_CHECK_SPEED();	
	EXITBTCS_CHECK_SPIN();	

	switch(Btcs_Exit_Check)
	{
		case BTCS_EXIT_NO_CHECKED:
			break;
		case BTCS_EXIT_CHECKED:	
			EXITBTCS_CHECKED();
			break;
		case BTCS_EXIT_CONFIRMED:
			EXITBTCS_CONFIRMED();
			break;	
	}
}

		#if __4WD
void    BTCSTAGET(void)
{
    tempF1 = 0;
    if(BTC_fz == 1) {
        tempB3 = (int8_t)(vref2 - vref_alt);
        if(vcount == 0) {
            vrefdiff_alt2 = vrefdiff_alt1;
            vrefdiff_alt1 = vrefdiff;
            vrefdiff = 0;
        }
        if(vcount <= 12) {
            vrefdiff += tempB3;
            vcount++;
        }
        else {
            vrefdiff += tempB3;
            avr_vref_alt = avr_vref;
            avr_vref = vrefdiff + vrefdiff_alt1 + vrefdiff_alt2;
            vcount = 0;
            tempF1 = 1;
        }

        tempW3 = avr_fspeed - vref2;
        if(SLIP_SET == 0) {
            avr_btc_slip = (int16_t)VREF_7_KPH;
            if(VREF_4WD == 1) {
                if(A_OPT_NEWSET == 1) {
                    sum_btc_slip += tempW3;
                    avr_count3 ++;
                }
            }
            if(tempF1 == 1) {
                if(avr_vref > avr_vref_alt) {
                    A_FIRST_INC = 1;
                    A_OPT_NEWSET = 1;
                    sum_btc_slip = 0;
                    sum_btc_slip += tempW3;
                    avr_count3 = 0;
                    avr_count3 ++;
                }
                else if(avr_vref < avr_vref_alt) {
                    A_FIRST_INC = 0;
                    A_OPT_NEWSET = 0;
                    sum_btc_slip = 0;
                    avr_count3 = 0;
                }
            }
        }
        else {
            if(avr_slip_min < (int16_t)VREF_5_KPH) avr_slip_min = (int16_t)VREF_5_KPH;
            if(VREF_4WD == 1) {
                if(A_OPT_NEWSET == 1) {
                    if(tempW3 > avr_btc_slip) sum_btc_slip += avr_btc_slip;
                    else sum_btc_slip += tempW3;
                    avr_count3 ++;
                }
                else if(A_OPT_SET_HIGH == 1) {
                    if(tempW3 < sum_btc_slip_alt) {
                        sum_btc_slip += tempW3;
                        avr_count1 ++;
                    }
                }
            }
            if(tempF1 == 1) {
                if(tempW3 < avr_btc_slip) {
                    if(avr_vref > avr_vref_alt) A_FIRST_INC = 1;
                }
                if(A_FIRST_INC == 0) {
                    if(tempW3 > avr_btc_slip) {
                        A_OPT_NEWSET = 0;
                        avr_count3 = 0;
                        if(avr_vref > avr_vref_alt) {
                            if(avr_count2 >= 3) A_OPT_SET_HIGH = 1;
                            else avr_count2 ++;
                        }
                        else avr_count2 = 0;
                        if(A_OPT_SET_HIGH == 1) {
                            if(avr_vref > avr_vref_alt) {
                                sum_btc_slip = 0;
                                sum_btc_slip_alt = tempW3;
                                sum_btc_slip += tempW3;
                                avr_count1 = 0;
                                avr_count1 ++;
                            }
                            else if(avr_vref < avr_vref_alt) {
                                avr_btc_slip = (sum_btc_slip / (int8_t)avr_count1);
                                if(avr_btc_slip > avr_slip_min + (int16_t)VREF_5_KPH) {
                                    avr_btc_slip = avr_slip_min + (int16_t)VREF_5_KPH;
                                }
                                else if(avr_btc_slip < avr_slip_min) {
                                    avr_btc_slip = avr_slip_min;
                                    avr_slip_min -= (int16_t)VREF_2_KPH;
                                }
                                A_OPT_SET_HIGH = 0;
                                avr_count1 = 0;
                                avr_count2 = 0;
                                sum_btc_slip = 0;
                            }
                        }
                    }
                }
                else {
                    A_OPT_SET_HIGH = 0;
                    avr_count1 = 0;
                    avr_count2 = 0;
                    if(avr_vref > avr_vref_alt) A_OPT_NEWSET = 1;
                    if(A_OPT_NEWSET == 1) {
                        if(avr_vref > avr_vref_alt) {
                            sum_btc_slip = 0;
                            sum_btc_slip += tempW3;
                            avr_count3 = 0;
                            avr_count3 ++;
                        }
                        else if(avr_vref < avr_vref_alt) {
                            avr_btc_slip = (sum_btc_slip / (int8_t)avr_count3);
                            if(avr_btc_slip > avr_slip_min + (int16_t)VREF_5_KPH) {
                                avr_btc_slip = avr_slip_min + (int16_t)VREF_5_KPH;
                            }
                            else if(avr_btc_slip < avr_slip_min) {
                                avr_btc_slip = avr_slip_min;
                                avr_slip_min -= (int16_t)VREF_2_KPH;
                            }
                            A_FIRST_INC = 0;
                            A_OPT_NEWSET = 0;
                            avr_count3 = 0;
                            sum_btc_slip = 0;
                        }
                    }
                }
                if(tempW3 <= avr_btc_slip) {
                    A_OPT_SET_HIGH = 0;
                    avr_count1 = 0;
                    avr_count2 = 0;
                    if(avr_vref > avr_vref_alt) A_OPT_NEWSET = 1;
                    if(A_OPT_NEWSET == 1) {
                        if(avr_vref > avr_vref_alt) {
                            sum_btc_slip = 0;
                            sum_btc_slip += tempW3;
                            avr_count3 = 0;
                            avr_count3 ++;
                        }
                        else if(avr_vref < avr_vref_alt) {
                            avr_btc_slip = (sum_btc_slip / (int8_t)avr_count3);
                            if(avr_btc_slip > avr_slip_min + (int16_t)VREF_5_KPH) {
                                avr_btc_slip = avr_slip_min + (int16_t)VREF_5_KPH;
                            }
                            else if(avr_btc_slip < avr_slip_min) {
                                avr_btc_slip = avr_slip_min;
                                avr_slip_min -= (int16_t)VREF_2_KPH;
                            }
                            A_FIRST_INC = 0;
                            A_OPT_NEWSET = 0;
                            avr_count3 = 0;
                            sum_btc_slip = 0;
                        }
                    }
                }
            }
        }
    }
    else {
        BTC_LEFT = 0;
        BTC_RIGHT = 0;
        SLIP_SET = 0;
        A_OPT_NEWSET = 0;
        vcount = 0;
        avr_vref = 0;
        avr_vref_alt = 0;
        avr_fspeed = (int16_t)VREF_MIN;
        avr_btc_slip = (int16_t)VREF_MIN;
        vrefdiff = 0;
        vrefdiff_alt1 = 0;
        vrefdiff_alt2 = 0;
    }
    
    VREF_4WD = 0;
}
		#endif
/* ====================================================================== */
void    BTCS_SPIN_CHECK(void)
{
#if __VDC
    tempW2 = vrad_fl - wvref_fl;                           // vref2
    tempW3 = vrad_fr - wvref_fr;
    tempW4 = vrad_rl - wvref_rl;
    tempW5 = vrad_rr - wvref_rr;
#else
    tempW2 = vrad_fl - vref2;
    tempW3 = vrad_fr - vref2;
    tempW4 = vrad_rl - vref2;
    tempW5 = vrad_rr - vref2;
#endif

#if !__4WD
	#if __REAR_D
		tempW2 = VREF_0_KPH;
		tempW3 = VREF_0_KPH;
	#else
		tempW4 = VREF_0_KPH;
		tempW5 = VREF_0_KPH;
	#endif
#endif

    if((BTCS_fl == 0) && (BTCS_fr == 0)) {
        if((tempW2 > (int16_t)VREF_4_25_KPH) && (tempW3 > (int16_t)VREF_4_25_KPH)) {
            if((s_diff_fl > (int16_t)VREF_0_KPH) && (s_diff_fr >(int16_t)VREF_0_KPH)) {
                BTCS_TWO_OFF = 1;
			  #if __VDC
                TCS_SPLIT=0;    // MAIN02C
			  #endif
            }
        }
    }
    if((BTCS_rl == 0) && (BTCS_rr == 0)) {
        if((tempW4 > (int16_t)VREF_4_25_KPH) && (tempW5 > (int16_t)VREF_4_25_KPH)) {
            if((s_diff_rl > (int16_t)VREF_0_KPH) && (s_diff_rr >(int16_t)VREF_0_KPH)) {
                BTCS_TWO_OFF = 1;
			  #if __VDC
                TCS_SPLIT=0;    // MAIN02C
			  #endif
            }
        }
    }

    if(BTC_fz == 1) {	// Transplantation from ABDMOTOR()    	
        if((POS_HOLD_fl == 0) && (POS_HOLD_fr == 0)) {
            if((tempW2 > (int16_t)VREF_12_KPH) && (tempW3 > (int16_t)VREF_12_KPH)) {
                if((s_diff_fl > (int16_t)VREF_0_KPH) && (s_diff_fr >(int16_t)VREF_0_KPH)) {
                    BTCS_TWO_OFF = 1;
				  #if __VDC
                    TCS_SPLIT=0;    // MAIN02C
				  #endif
                }
            }
        }
        if((POS_HOLD_rl == 0) && (POS_HOLD_rr == 0)) {
            if((tempW4 > (int16_t)VREF_12_KPH) && (tempW5 > (int16_t)VREF_12_KPH)) {
                if((s_diff_rl > (int16_t)VREF_0_KPH) && (s_diff_rr >(int16_t)VREF_0_KPH)) {
                    BTCS_TWO_OFF = 1;
				  #if __VDC
                    TCS_SPLIT=0;    // MAIN02C
				  #endif
                }
            }
        }
        
	  #if !__4WD && __REAR_D && __CAR!=HMC_SM_SIGMA
	  	if(U16_BTCS_TWO_ON_HOMO_MU==0)
		{
	        if(BTCS_TWO_OFF==1 && ((BTCS_rl==1)||(BTCS_rr==1))) {
            	if((btc_count1_rl>20)&&(btc_count1_rr<5)) BTCS_TWO_OFF=0;
        	    else if((btc_count1_rr>20)&&(btc_count1_rl<5)) BTCS_TWO_OFF=0;
    	    }
	    }	  
	  #elif !__4WD && !__REAR_D && __CAR!=HMC_SM_SIGMA
		if(U16_BTCS_TWO_ON_HOMO_MU==0)
		{
    	    if(BTCS_TWO_OFF==1 && ((BTCS_fl==1)||(BTCS_fr==1))) {
	            if((btc_count1_fl>20)&&(btc_count1_fr<5)) BTCS_TWO_OFF=0;
            	else if((btc_count1_fr>20)&&(btc_count1_fl<5)) BTCS_TWO_OFF=0;
        	}
       	}
	  #endif
	  	          
    }
    else {
	  #if __VDC
	   #if __TCS
        if((BTCS_TWO_OFF==0)&&(ETCS_ON==0)) {
	   #else
        if(BTCS_TWO_OFF==0) {
	   #endif
            TCS_SPLIT=1;                // MAIN02C, MAIN03
        }
	  #endif
	}
	
	if((tempW2 < (int16_t)VREF_2_KPH) || (tempW3 < (int16_t)VREF_2_KPH)) {
        if((tempW4 < (int16_t)VREF_2_KPH) || (tempW5 < (int16_t)VREF_2_KPH)) {
       	    BTCS_TWO_OFF = 0;
       	}
    }
}
/* ====================================================================== */
void    ABDMOTOR_DEFAULT_SETTING(void)
{
    if((TMP_ERROR_fl==0)&&(TMP_ERROR_fr==0)&&(TMP_ERROR_rl==0)&&(TMP_ERROR_rr==0)) tc_error_flg=0;

#if __VDC
    tempW2 = vrad_fl - wvref_fl;                           // vref2
    tempW3 = vrad_fr - wvref_fr;
    tempW4 = vrad_rl - wvref_rl;
    tempW5 = vrad_rr - wvref_rr;
#else
    tempW2 = vrad_fl - vref2;
    tempW3 = vrad_fr - vref2;
    tempW4 = vrad_rl - vref2;
    tempW5 = vrad_rr - vref2;
#endif
    tempB3 = (int8_t)(vref2 - vref_alt);
    	
#if !__4WD
	#if __REAR_D
		tempW2 = VREF_0_KPH;
		tempW3 = VREF_0_KPH;
	#else
		tempW4 = VREF_0_KPH;
		tempW5 = VREF_0_KPH;
	#endif
#endif		
}

/* ====================================================================== */
void    ABDMOTOR_MOT_ON(void)
{
  #if __PWM_MOT
        if((POS_HOLD_fl)||(POS_HOLD_fr)||(POS_HOLD_rl)||(POS_HOLD_rr)) {
            TC_MOT_ON=1;
            mot_count=0;
        }
        else {
        	TC_MOT_ON=0;
        }
        
        if(TC_MOT_ON==0)
        {
        	if(mot_count<14) mot_count++;
        	else TC_MOT_ON=1;
        }
  #else		
        if((exit_timer1_fl>BTCS_MOT_RUNTIME3)||(exit_timer1_fr>BTCS_MOT_RUNTIME3)
            ||(exit_timer1_rl>BTCS_MOT_RUNTIME3)||(exit_timer1_rr>BTCS_MOT_RUNTIME3)) {
            mot_count++;
            if(mot_count <= BTCS_MOT_RUNTIME1) TC_MOT_ON = 1;
            else {
                TC_MOT_ON = 0;
                if(mot_count>9) mot_count = 0;
            }
        }
        else if((COUNT20_fl==1)||(COUNT20_fr==1)||(COUNT20_rl==1)||(COUNT20_rr==1)) {
            mot_count++;
            if(mot_count <= BTCS_MOT_RUNTIME2) TC_MOT_ON = 1;
            else {
                TC_MOT_ON = 0;
                if(mot_count>9) mot_count = 0;
            }
        }
        else if((POS_HOLD_fl==1)||(POS_HOLD_fr==1)||(POS_HOLD_rl==1)||(POS_HOLD_rr==1)) {
            mot_count++;
            if(mot_count <= BTCS_MOT_RUNTIME2) TC_MOT_ON = 1;
            else {
                TC_MOT_ON = 0;
                if(mot_count>9) mot_count = 0;
            }
        }
        else TC_MOT_ON = 1;

	#if !__4WD && __REAR_D
    	#if __TCS
	        if((BTCS_rl==1)&&(BTCS_rr==1)) {
        	    if((exit_timer1_rl > BTCS_MOT_RUNTIME3)&&(exit_timer1_rr > BTCS_MOT_RUNTIME3)) TC_MOT_ON = 0;
    	    }
	        else if(BTCS_rl==1) {
        	    if(exit_timer1_rl > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    }
	        else if(BTCS_rr==1) {
        	    if(exit_timer1_rr > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    }
	    #else
        	if(exit_timer1_rl > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    else if(exit_timer1_rr > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
	    #endif
	#elif !__4WD && !__REAR_D
    	#if __TCS
	        if((BTCS_fl==1)&&(BTCS_fr==1)) {
        	    if((exit_timer1_fl > BTCS_MOT_RUNTIME3)&&(exit_timer1_fr > BTCS_MOT_RUNTIME3)) TC_MOT_ON = 0;
    	    }
	        else if(BTCS_fl==1) {
        	    if(exit_timer1_fl > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    }
	        else if(BTCS_fr==1) {
        	    if(exit_timer1_fr > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    }
	    #else
        	if(exit_timer1_fl > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
    	    else if(exit_timer1_fr > BTCS_MOT_RUNTIME3) TC_MOT_ON = 0;
	    #endif
    #endif
  #endif		
}

/* ====================================================================== */
void    ABDMOTOR_VALVE(void)
{       
	if(BTCS_fl==1) 
	{
		TCL_DEMAND_fl = 1;       

	    #if !__ESV && !__4WD	//for the 2WD TCS only type
            if(valve_delay_count_fl<5) valve_delay_count_fl++;
            else if(TCS_VALVE_EXIT_fl==0) valve_delay_count_fl=5;
            if(valve_delay_count_fl>5) TCL_DEMAND_fl=0;
    	#endif        
	}
	if(BTCS_fr==1) 
	{
		TCL_DEMAND_fr = 1;                		

	    #if !__ESV && !__4WD	//for the 2WD TCS only type
            if(valve_delay_count_fl<5) valve_delay_count_fl++;
            else if(TCS_VALVE_EXIT_fl==0) valve_delay_count_fl=5;
            if(valve_delay_count_fl>5) TCL_DEMAND_fl=0;
    	#endif        
	}
	if(BTCS_rl==1) 
	{
		TCL_DEMAND_rl = 1;
	}
	if(BTCS_rr==1) 
	{
		TCL_DEMAND_rr=1;
	}   
       
    #if __ESV
	    #if __4WD 
    	    if((BTCS_fl)&&(BTCS_rr)) {
        		S_VALVE_LEFT=1;
        		#if __TCS_NOISE
            		if((HV_VL_fl)&&(HV_VL_rr)) S_VALVE_LEFT=0;
            	#endif
        	}
	        else if(BTCS_fl) {
    	    	S_VALVE_LEFT=1;
        		#if __TCS_NOISE    	    	
        	    	if(HV_VL_fl) S_VALVE_LEFT=0;
        	    #endif
        	}
	        else if(BTCS_rr) {
    	    	S_VALVE_LEFT=1;
        		#if __TCS_NOISE    	    	
        	    	if(HV_VL_rr) S_VALVE_LEFT=0;
        	    #endif
        	}
	        if((BTCS_fr)&&(BTCS_rl)) {
    	    	S_VALVE_RIGHT=1;
        		#if __TCS_NOISE    	    	
        	    	if((HV_VL_fr)&&(HV_VL_rl)) S_VALVE_RIGHT=0;
        	    #endif
        	}
	        else if(BTCS_fr) {
    	    	S_VALVE_RIGHT=1;
        		#if __TCS_NOISE    	    	
        	    	if(HV_VL_fr) S_VALVE_RIGHT=0;
        	    #endif
        	}
	        else if(BTCS_rl) {
    	    	S_VALVE_RIGHT=1;
        		#if __TCS_NOISE    	    	
        	    	if(HV_VL_rl) S_VALVE_RIGHT=0;
        	    #endif
        	}
        #else // __2WD (FWD or RWD)
			if(BTCS_fl==1) 
			{
   	    	    if(valve_delay_count_fl<5) valve_delay_count_fl++;
       	    	else if(TCS_VALVE_EXIT_fl==0) valve_delay_count_fl=5;

	           	if(valve_delay_count_fl>6) TCL_DEMAND_fl=S_VALVE_LEFT=0;
    	       	else if(valve_delay_count_fl>5) TCL_DEMAND_fl=0;

        	    if(valve_delay_count_fl>1) S_VALVE_LEFT=1;

        		#if __TCS_NOISE
	        	if(HV_VL_fl==1) {
    	        	S_VALVE_LEFT = 0;
        		}
        		#endif		
                
	        	if(U16_BTCS_ESV_CONTROL)
	        	{
	    	        if(HV_VL_fl==1)	//HOLD, DUMP
    	    	    {
        	    		S_VALVE_LEFT=0;
            	    	Btcs_ESV_Timer_fl = BTCS_ESV_ON_DELAY;
	            	}
	    	        else {			//RISE
    	    	    	if(Btcs_ESV_Timer_fl>0) //RISE 2 Times?
        	    		{
            				S_VALVE_LEFT=0;
            				Btcs_ESV_Timer_fl--;
	            		}         		            		            	
    	        	}
    	        }
			}    	
    		if(BTCS_fr)
    		{
       	    	if(valve_delay_count_fl<5) valve_delay_count_fl++;
	           	else if(TCS_VALVE_EXIT_fl==0) valve_delay_count_fl=5;
	
    	       	if(valve_delay_count_fl>6) TCL_DEMAND_fl=S_VALVE_LEFT=0;
        	   	else if(valve_delay_count_fl>5) TCL_DEMAND_fl=0;

   	        	if(valve_delay_count_fl>1) S_VALVE_LEFT=1;
	
	    	    #if __TCS_NOISE
    	    	if(HV_VL_fr==1) {
       			    S_VALVE_RIGHT = 0;
   		    	}
	        	#endif        
	        
       			if(U16_BTCS_ESV_CONTROL)
       			{
	           		if(HV_VL_fl==1)	//HOLD, DUMP
		       	    {
   			        	S_VALVE_LEFT=0;
        		        Btcs_ESV_Timer_fl = BTCS_ESV_ON_DELAY;
           			}
		       	    else {			//RISE
   			        	if(Btcs_ESV_Timer_fl>0) //RISE 2 Times?
        		    	{
           					S_VALVE_LEFT=0;
           					Btcs_ESV_Timer_fl--;
           				}         		            		            	
       	    		}
       	    	}
   			}
    		if(BTCS_rl)
    		{
				S_VALVE_RIGHT  = 1;		
    		    #if __TCS_NOISE
		        if(HV_VL_rl==1) {
        	    	S_VALVE_RIGHT = 0;
    	    		}
	    	    #endif	    			
    		}
    		if(BTCS_rr) 
    		{
				S_VALVE_LEFT = 1;				
	    	    #if __TCS_NOISE
        		if(HV_VL_rr==1) {
        	    	S_VALVE_LEFT = 0;
    	    	}
	        	#endif    	
    		}
    	#endif	// end of #if __4WD
    #endif // end of #if __ESV
}
	
/* ====================================================================== */
void    ABDMOTOR(void)
{
	ABDMOTOR_DEFAULT_SETTING();

	if(BTC_fz==1)
	{	
		ABDMOTOR_MOT_ON();
		ABDMOTOR_VALVE();	
	}
    else {
        TC_MOT_ON = mot_count = 0;
        YAW_SET = BTCS_VIB = HIGH_SLIP = 0;
        TCL_DEMAND_fl=TCL_DEMAND_fr=TCL_DEMAND_rl=TCL_DEMAND_rr=0;
        exit_timer2_fl = exit_timer2_fr = exit_timer2_rl = exit_timer2_rr = 0;
        vrad_alt_fl = vrad_alt_fr = vrad_alt_rl = vrad_alt_rr = 0;
        
	  #if __ESV
        esv_count_l=esv_count_r=0;
        S_VALVE_LEFT=S_VALVE_RIGHT=0;
	  #endif    	    	
	}	
}

/* ====================================================================== */ 
void    TCSTATE1(void)
{
	/* call from the file 'btcs.c' */
	BTC_VIB_SET_wl();	
	/* call from the file 'btcs.c' finished */
		
	TCSTATE1_DEFAULT_SETTING();
	TCSTATE1_COMP_DV_THRESHOLD();	
	
	TCSTATE1_DUMP_COUNT_CHECK();
	TCSTATE1_DUMP_LOW_RPM();
	TCSTATE1_DUMP_LOW_SIDE();
	TCSTATE1_DUMP_HIGH_SIDE();
	TCSTATE1_DUMP_HOMO_MU();
	
	TCSTATE1_INC_COUNT_CHECK();
	TCSTATE1_INC_LOW_SIDE();
	
	TCSTATE1_HOLD_COUNT_CHECK();
	TCSTATE1_HOLD_DEFAULT();
	
	switch(Btcs_State1_Mode)
	{		
		case BTCS_STATE1_DEFAULT:
			TCSTATE1_HOLD_MODE();
			break;
					
		case BTCS_STATE1_HOLD:
			TCSTATE1_HOLD_MODE();
			break;
			
		case BTCS_STATE1_INC:
			TCSTATE1_INC_MODE();
			break;
			
		case BTCS_STATE1_DUMP0:
			TCSTATE1_DUMP0_MODE();
			break;
									
		case BTCS_STATE1_DUMP1:
			TCSTATE1_DUMP1_MODE();
			break;			
	}
	
	TCSTATE1_HOLD_COUNT();	
}

/* ====================================================================== */ 
void    TCSTATE1_DEFAULT_SETTING(void)
{
    WL->tmp_state 		= 2;
    TCL_INCF2_wl 		= 0;
	Btcs_State1_Check	= BTCS_STATE1_NO_CHECKED;
	Btcs_State1_Mode	= BTCS_STATE1_DEFAULT;
	    
    if(WL->btc_count1 < 40) WL->btc_count1 ++;	
    
	#if __VDC
    	tempW0 = (int16_t)(WL->vrad - WL->wvref);
   		tempW3 = (WL->av_fspeed - WL->wvref);
	#else
    	tempW0 = (int16_t)(WL->vrad - vref);
   		tempW3 = (WL->av_fspeed - vref);
	#endif	  
    tempW1 = WL->vdiff;	  
}

/* ====================================================================== */ 
void    TCSTATE1_COMP_DV_THRESHOLD(void)
{
	if(!XMT_VIB_wl) {//수정#3
    	if(HILL_wl == 1) {
        	WL->b_rad = (int16_t)BTCS_CMP_DV_HILL;        	
    	}
    	else {   	
        	WL->b_rad = (int16_t)BTCS_CMP_DV_FLAT;        	
    	}
    }		
}

/* ====================================================================== */ 
void    TCSTATE1_DUMP_COUNT_CHECK(void)
{
    //===========================================================
    //	BTCS on ice by Kim Jeonghun at 2004.2.11
    //-----------------------------------------------------------   
    #if __VDC
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {   	
       	if((ETCS_ON)) 
       	{	
       		#if __REAR_D
       			if(BTCS_wl && WL->inc_count3>=ETCS_ON_BRAKE_TIME) {
       				WL->BTCS_flags = 1;
					Btcs_State1_Check = BTCS_STATE1_CHECKED;
					Btcs_State1_Mode = BTCS_STATE1_DUMP1;
       			}  
       		#else
	       		if(BTCS_wl && WL->inc_count3>=ETCS_ON_BRAKE_TIME) {
    				WL->BTCS_flags = 1;
					Btcs_State1_Check = BTCS_STATE1_CHECKED;
					Btcs_State1_Mode = BTCS_STATE1_DUMP1;
       			}
       		#endif			

    	}
		//=================================================================
		//	for smooth BTCS control by Kim Jeonghun in 2004.3.2
		//-----------------------------------------------------------------    	
    	if(WL->inc_count3 >= FTCS_ON_BRAKE_TIME) {
    		WL->BTCS_flags = 2;	/* checking result : this flag occur 1 times split control */
			Btcs_State1_Check = BTCS_STATE1_CHECKED;
			Btcs_State1_Mode = BTCS_STATE1_DUMP1;
    	}
    }
    #endif
}

void    TCSTATE1_DUMP_LOW_RPM(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
		//=====================================================================
    	//	offset setting for M/T by Kim Jeonghun in 2004.4.22
	    //---------------------------------------------------------------------
   		if(U16_BTCS_BUILD_PATTERN && __VDC)
   		{
    		if(WL->inc_count3 >= T_400_MS) 
    		{
       			if(WL->hold_timer == 0) 
       			{    	
					tempW5 = (int16_t)(BTCS_1ST_DMP_LOW_RPM*10);	/* 1500rpm */
			
	      	  		if((eng_rpm<tempW5) && (acc_f < ACCEL_X_0G0G)) 
	      	  		{ 
	      	  			WL->hold_timer = BTCS_1ST_HLD_TIME1;      		    	
       			    	WL->BTCS_flags = 3;       		    	
						Btcs_State1_Check = BTCS_STATE1_CHECKED;
						Btcs_State1_Mode = BTCS_STATE1_DUMP1;
	        		}
    			}    
   	 		}
		}
	}
}

void    TCSTATE1_DUMP_LOW_SIDE(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
	    if(tempW0 - WL->b_rad <= (int16_t)BTCS_1ST_DMP_DV_LOW_SIDE) {
			#if __4WD
            	avr_btc_slip = (int16_t)VREF_8_KPH;
            	avr_slip_min = (int16_t)VREF_6_KPH;
            #endif
            
  	        WL->btc_slip = tempW4;
       	    if(WL->btc_slip < (int16_t)VREF_10_KPH) {
   	            WL->btc_slip = (int16_t)VREF_10_KPH;
   	        }
   	        WL->slip_min = WL->btc_slip - (int16_t)VREF_2_KPH;
       	        		  
	        WL->btcdec_count = 1;
        	WL->BTCS_flags = 4;
			Btcs_State1_Check = BTCS_STATE1_CHECKED;
			Btcs_State1_Mode = BTCS_STATE1_DUMP1;
    	}
    }
}

void    TCSTATE1_DUMP_HIGH_SIDE(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
		//=============================================================================
		//	for High Side Spin Control by Kim Jeonghun in 2005.07.04
		//-----------------------------------------------------------------------------
		if((!__TCS) || (U16_BTCS_HIGH_SIDE_CHECK && __VDC))
		{
	  		#if __READ_D
	  			if(LEFT_WHEEL_wl == 1) 
	  			{
	  				tempW6 = vrad_rr - vref;
	  			}
	  			else 
	  			{
	  				tempW6 = vrad_rl - vref;
	  			}
	  		#else
	  			if(LEFT_WHEEL_wl == 1) 
	  			{
	  				tempW6 = vrad_fr - vref;
	  			}
	  			else 
	  			{
	  				tempW6 = vrad_fl - vref;
	  			}	  	  
	  		#endif

			if(U16_BTCS_TWO_ON_HOMO_MU)
			{
    			if(vref<(int16_t)VREF_10_KPH) {	/* Skip high side control when BTCS_TWO_ON_HOMO_MU */
        			if(BTCS_TWO_OFF) 
        			{
    					tempW6 = VREF_0_KPH;        						        			
        			}
    			}
			}

			if(YAW_SET == 0)
			{
				if(tempW6 > (int16_t)BTCS_1ST_DMP_DV_HIGH_SIDE) 
				{	
                	WL->hold_timer = BTCS_1ST_HLD_TIME1;
	                YAW_SET = 1;
    	            WL->BTCS_flags = 5;
					Btcs_State1_Check = BTCS_STATE1_CHECKED;
					Btcs_State1_Mode = BTCS_STATE1_DUMP0;			
				}
			}
			else
			{
				if(tempW6 < (int16_t)VREF_1_KPH) 
				{
                	YAW_SET = 0;				
				}
			
				if((WL->hold_timer==0)&&(tempW6 > (int16_t)BTCS_1ST_DMP_DV_HIGH_SIDE))
				{
                	WL->hold_timer = BTCS_1ST_HLD_TIME1;
                	WL->BTCS_flags = 6;
					Btcs_State1_Check = BTCS_STATE1_CHECKED;
					Btcs_State1_Mode = BTCS_STATE1_DUMP0;				
				}	
			}
		}
	}	
}

void    TCSTATE1_DUMP_HOMO_MU(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
	//===========================================================================
	//	Brake two wheel on ice by Kim Jeonghun in 2004.2.24
	//---------------------------------------------------------------------------        
	#if __TCS
		if(U16_BTCS_TWO_ON_HOMO_MU)
		{
    		if((BTCS_TWO_OFF)||(ETCS_ON)) {
        		if(POS_HOLD_wl) {
            		if(tempW0 < (int16_t)BTCS_1ST_DMP_DV_HOMO_MU) {
                		WL->BTCS_flags = 9;
						Btcs_State1_Check = BTCS_STATE1_CHECKED;
						Btcs_State1_Mode = BTCS_STATE1_DUMP1;
    		        }
	    	    }
    		}
    	}
	#endif
	}
}

/* ====================================================================== */ 
void    TCSTATE1_INC_COUNT_CHECK(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
	    if(WL->btcinc_count != 0) {
    	    WL->btcinc_count --;
        	WL->BTCS_flags = 21;
			Btcs_State1_Check = BTCS_STATE1_CHECKED;
			Btcs_State1_Mode = BTCS_STATE1_INC;
    	}
	}
	
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
	    if((WL->hold_count>=(int16_t)U16_HOLD_TIME_MAX)&&(WL->btc_speed>(int16_t)(-VREF_3_KPH)) ) {
			#if __VDC
    	    	if((WL->av_fspeed - WL->wvref) > (int16_t)BTCS_1ST_RIS_DV_HOLD_MAX) {
			#else
    	    	if((WL->av_fspeed - vref) > (int16_t)BTCS_1ST_RIS_DV_HOLD_MAX) {
			#endif
       	        WL->hold_count = 0;
           	    WL->hold_timer = BTCS_1ST_HLD_TIME2;

               	WL->BTCS_flags = 22;
				Btcs_State1_Check = BTCS_STATE1_CHECKED;
				Btcs_State1_Mode = BTCS_STATE1_INC;
        	}
    	}
    }
}

void    TCSTATE1_INC_LOW_SIDE(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
		if(POS_HOLD_wl==0) {
        	if((tempF0 == 1)&&(WL->average7 <= (int16_t)VREF_0_KPH)) {
   	            POS_HOLD_wl = 1;
       		}
       						
			if(WL->inc_count3 < BTCS_1ST_RIS_FULL_ON_SCAN) {		// POS_HOLD_wl==0
				WL->BTCS_flags = 23; /* checking result : this flag occur 27 times split control */
				Btcs_State1_Check = BTCS_STATE1_CHECKED;
				Btcs_State1_Mode = BTCS_STATE1_INC;
   			}
   			else if(WL->hold_timer == 0) { // Build Reapply Pattern
   				WL->btcinc_count = 1;
   				WL->hold_timer = BTCS_1ST_HLD_TIME2;			
	    		WL->BTCS_flags = 24;
				Btcs_State1_Check = BTCS_STATE1_CHECKED;
				Btcs_State1_Mode = BTCS_STATE1_INC;	   					   				
   			}
		}
   	    else {
           	if(WL->hold_timer == 0) {
                WL->btcinc_count = 1;
   	            WL->hold_timer = BTCS_1ST_HLD_TIME1;
       	        WL->BTCS_flags = 25;
				Btcs_State1_Check = BTCS_STATE1_CHECKED;
				Btcs_State1_Mode = BTCS_STATE1_INC;               		        
        	}
    	}   	
    }
}

/* ====================================================================== */ 
void    TCSTATE1_HOLD_COUNT_CHECK(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_CHECKED && Btcs_State1_Mode==BTCS_STATE1_DUMP1) 
    {
		#if __VDC
		    if(WL->btc_count1 < T_214_MS) {
		    	if(ETCS_ON) {
		    		if(WL->inc_count3<ETCS_ON_BRAKE_TIME) {
	    				WL->BTCS_flags = 41;
						Btcs_State1_Mode = BTCS_STATE1_HOLD;		    				    		
					}
		    	}
		    	else {
	    			WL->BTCS_flags = 42; /* checking result : this flag occur 2 times split control */
					Btcs_State1_Mode = BTCS_STATE1_HOLD;		    				    		
		    	}
	    	}		   	
   		#else
		    if(WL->btc_count1 < T_214_MS) {
		    	WL->BTCS_flags = 43;
				Btcs_State1_Mode = BTCS_STATE1_HOLD;
	    	}
		#endif    	   	
    }	
}

void    TCSTATE1_HOLD_DEFAULT(void)
{
    if(Btcs_State1_Check==BTCS_STATE1_NO_CHECKED) 
    {
	    WL->BTCS_flags = 44;
		Btcs_State1_Check = BTCS_STATE1_CHECKED;
		Btcs_State1_Mode = BTCS_STATE1_HOLD;
    }
}    

/* ====================================================================== */ 
void    TCSTATE1_INC_MODE()
{
    AV_REAPPLY_wl = 1;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;

    WL->state = BTCS_STATE1;
}

void    TCSTATE1_HOLD_MODE()
{
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 1;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    if(WL->hold_timer != 0) WL->hold_timer--;

   	WL->state = BTCS_STATE1; 
}	    	

void    TCSTATE1_DUMP0_MODE()
{
    HIGH_SLIP = 1;
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 1;
    BUILT_wl = 0;
    WL->state = BTCS_STATE1;
}

void    TCSTATE1_DUMP1_MODE()
{
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 1;
    BUILT_wl = 0;
    HALF_PULS_wl = 0;
    VRAD_DEC_wl = 0;
    POS_HOLD_wl = 1;
    WL->btcinc_count = 0;
    WL->hold_timer = 0;
    WL->average7 = (int16_t)VREF_0_KPH;
    WL->count7 = 0;
    WL->btc_count2 = 0;
    WL->state = BTCS_STATE2;
    WL->tmp_state = 3;
    WL->slip_count4 = 0;
    
    if(HILL_wl == 1) {
        WL->btc_slip += (int16_t)VREF_2_KPH;
        WL->slip_min += (int16_t)VREF_4_KPH;
    }
    else {
        WL->btc_slip -= (int16_t)VREF_2_KPH;
        WL->slip_min -= (int16_t)VREF_4_KPH;
    }
}

void    TCSTATE1_HOLD_COUNT(void)	
{    
    if(AV_HOLD_wl == 1) {
        WL->hold_count ++;
        WL->hold_count_both_spin ++;
    }
    else {
        WL->hold_count = 0;
        WL->hold_count_both_spin = 0;
    }
    
	#if __VDC
    	if((WL->av_fspeed - WL->wvref) <= (int16_t)VREF_20_KPH) WL->hold_count = 0;
	#else
    	if((WL->av_fspeed - vref) <= (int16_t)VREF_20_KPH) WL->hold_count = 0;
	#endif
}

/* ====================================================================== */
void    TCSTATE2(void)
{
	/* call from the file 'btcs.c' */
  #if !SIM_MATLAB
	#if !__4WD
    TARGET_THR_MIN_Set();	// WL->slip_min=BTCS_TAR_DV_MIN_HILL or BTCS_TAR_DV_MIN_FLAT
    #endif
  #endif    
  
	BTC_VIB_SET_wl();
	/* call from the file 'btcs.c' finished */
	
	TCSTATE2_DEFAULT_SETTING();
	TCSTATE2_COMP_DV_THRESHOLD();
	TCSTATE2_DUMP_COUNT_CHECK();
	TCSTATE2_DUMP_LOW_RPM();
	TCSTATE2_DUMP_LOW_SIDE();
	TCSTATE2_DUMP_HIGH_SIDE();

	TCSTATE2_INC_COUNT_CHECK();
	TCSTATE2_INC_LOW_SIDE();
    
    TCSTATE2_HOLD_DEFAULT();

	switch(Btcs_State2_Mode)
	{		
		case BTCS_STATE2_DEFAULT:
			TCSTATE2_HOLD_MODE();
			break;
					
		case BTCS_STATE2_HOLD:
			TCSTATE2_HOLD_MODE();
			break;
			
		case BTCS_STATE2_INC:
			TCSTATE2_INC_MODE();
			break;
			
		case BTCS_STATE2_DUMP2:
			TCSTATE2_DUMP2_MODE();
			break;								
	}
	
	TCSTATE2_HOLD_COUNT();
}

/* ====================================================================== */
void    TCSTATE2_DEFAULT_SETTING(void)
{
    WL->tmp_state 		= 4;
    TCL_INCF2_wl 		= 0;
	Btcs_State2_Check	= BTCS_STATE2_NO_CHECKED;
	Btcs_State2_Mode	= BTCS_STATE2_DEFAULT;	

	#if __VDC
    	tempW0 = (int16_t)(WL->vrad - WL->wvref);
   		tempW3 = (WL->av_fspeed - WL->wvref);
	#else
    	tempW0 = (int16_t)(WL->vrad - vref);
   		tempW3 = (WL->av_fspeed - vref);
	#endif
	
	#if __4WD
    	if(avr_btc_slip > BTCS_TAR_DV_MAX_FLAT) avr_btc_slip = BTCS_TAR_DV_MAX_FLAT;      // MAIN04
	    if(avr_btc_slip < BTCS_TAR_DV_MIN_FLAT) avr_btc_slip = BTCS_TAR_DV_MIN_FLAT;      // MAIN04	
    #endif
}

/* ====================================================================== */ 
void    TCSTATE2_COMP_DV_THRESHOLD(void)
{
	if(!XMT_VIB_wl) {//수정#3
    	if(HILL_wl == 1) {
        	WL->b_rad = (int16_t)BTCS_CMP_DV_HILL;        	
    	}
    	else {   	
        	WL->b_rad = (int16_t)BTCS_CMP_DV_FLAT;        	
    	}
    }		
    
   	if(WL->hold_count >= BTCS_CTRL_HLD_SCAN_MAX) {
		#if __VDC
   	    	tempW5 = WL->av_fspeed - WL->wvref;
		#else
   	    	tempW5 = WL->av_fspeed - vref;
		#endif
			
       	tempW6 = WL->btc_slip;
   	    tempW6 += (int16_t)VREF_2_KPH;

        if(tempW5 < tempW6) {
       	    if(WL->btc_speed >= (int16_t)VREF_0_KPH) {
   	            if(WL->btc_slip >= (int16_t)VREF_5_KPH) ARAD_LOW_wl = 1;
            }
           	else ARAD_LOW_wl = 0;
       	}
   	    else ARAD_LOW_wl = 0;
    }     
}

/* ====================================================================== */ 
void    TCSTATE2_DUMP_COUNT_CHECK(void)
{
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {	
	    if(WL->btcdec_count != 0) {
    	    WL->btcdec_count --;

        	WL->BTCS_flags = 51; /* checking result : this flag occur 8 times split control */
			Btcs_State2_Check	= BTCS_STATE2_CHECKED;
			Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
    	}
    }
}	

void    TCSTATE2_DUMP_LOW_RPM(void)
{
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {		
    	//=====================================================================
    	//	offset setting for M/T by Kim Jeonghun in 2004.4.22
    	//---------------------------------------------------------------------
		if(U16_BTCS_BUILD_PATTERN && __VDC)
		{
	        if(tempF0==1) {
				tempW5 = (int16_t)(BTCS_2ND_DMP_LOW_RPM*10);

        		if((eng_rpm<tempW5)&&(acc_f < ACCEL_X_0G0G)) 
    	    	{
	       	    	WL->BTCS_flags = 52;
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
        		}
    		}    
		}
	}
}

void    TCSTATE2_DUMP_LOW_SIDE(void)
{
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {		
	    if((tempW0 < (int16_t)BTCS_2ND_DMP_DV_LOW_SIDE1)&&(WL->hold_timer == 0))
	    {
   	    	if(tempW0 > (int16_t)(-VREF_1_KPH)) {
       	    	if(WL->exit_timer1 >= 20) {
           			WL->BTCS_flags = 53; /* checking result : this flag occur 6 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
    	        }
   	    	    else if(WL->btc_count2 > 1) {
       	    	    WL->btc_count2 = 0;
           	    	WL->BTCS_flags = 54; /* checking result : this flag occur 4 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
    	        }
   	    	    else {
       	    	    WL->btc_count2 ++;
        	    }
        	    //Added by KJH in 2005.06.22
    	        if(tempW0 < (int16_t)(VREF_2_KPH)) {
           	    	WL->BTCS_flags = 55; /* checking result : this flag occur 17 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;    	        	    	        	
    	        }        	    
   	    	}
    	    else {
   	    		WL->BTCS_flags = 56;
				Btcs_State2_Check	= BTCS_STATE2_CHECKED;
				Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
   	    	}
    	}
    	else WL->btc_count2 = 0;
    }
    
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {	
		if(tempF2 == 1) {
		#if __VDC
        	tempW3 = (int16_t)(WL->av_fspeed - WL->wvref);
        	tempW4 = (int16_t)(WL->av_fspeed_alt - WL->wvref);
		#else
        	tempW3 = (int16_t)(WL->av_fspeed - vref);
        	tempW4 = (int16_t)(WL->av_fspeed_alt - vref);
		#endif		
        
        	if(VRAD_DEC_wl == 1) {
            	if(tempW3 < WL->btc_slip) {
                	VRAD_DEC_wl = 0;
	                if(WL->av_ref >= WL->av_ref_alt) {
    	                if(WL->btc_speed < (int16_t)VREF_0_KPH) {
        	                WL->hold_timer = BTCS_1ST_HLD_TIME2;
            	            WL->BTCS_flags = 57;
							Btcs_State2_Check	= BTCS_STATE2_CHECKED;
							Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
                    	}
	                    else {
    	                    WL->hold_timer = BTCS_1ST_HLD_TIME2;

	                        WL->BTCS_flags = 58;
							Btcs_State2_Check	= BTCS_STATE2_CHECKED;
							Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
        	            }
            	    }
            	}
	            else {
    	            if(WL->btc_speed < (int16_t)(-VREF_5_KPH)) {
        	            WL->btcdec_count = 1;
            	        WL->hold_timer = BTCS_1ST_HLD_TIME2;
                	    WL->BTCS_flags = 59; /* checking result : this flag occur 4 times split control */
						Btcs_State2_Check	= BTCS_STATE2_CHECKED;
						Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
	                }
    	            else if(WL->btc_speed < (int16_t)(-VREF_3_KPH)) {
        	            WL->hold_timer = BTCS_1ST_HLD_TIME2;
            	        WL->BTCS_flags = 60; /* checking result : this flag occur 2 times split control */
						Btcs_State2_Check	= BTCS_STATE2_CHECKED;
						Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
                	}
            	}
	        }
    	    else {
        	    if(tempW3 >= WL->btc_slip + (int16_t)VREF_1_5_KPH) VRAD_DEC_wl = 1;
        	}

		    tempW5 = WL->btc_slip;
    	    tempW5 += (int16_t)VREF_2_KPH;
        	tempW6 = tempW3 - WL->btc_slip;
        
	        if(tempW3 <= WL->btc_slip - (int16_t)VREF_0_5_KPH) {
    	        tempW5 = (uint16_t)WL->btc_slip>>2;

        	    if(WL->btc_speed <= (int16_t)(-tempW5)) {
            	    WL->btcdec_count = 1;  /*1*/
                	WL->hold_timer = BTCS_2ND_HLD_TIME2;  /*16*/
	                WL->BTCS_flags = 61; /* checking result : this flag occur 4 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
        	    }
	            else if(WL->btc_speed < (int16_t)VREF_0_KPH) {
    	            WL->hold_timer = BTCS_2ND_HLD_TIME2;  /*17*/
        	        WL->BTCS_flags = 62; /* checking result : this flag occur 9 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_DUMP2;
	            }
        	}       
		}            	
    }    
}

void    TCSTATE2_DUMP_HIGH_SIDE(void)
{    
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {	
		//=============================================================================
		//	for High Side Spin Control by Kim Jeonghun in 2005.07.04
		//-----------------------------------------------------------------------------
		if((!__TCS) || (U16_BTCS_HIGH_SIDE_CHECK && __VDC))
	  	{
	  		#if __READ_D
	  			if(LEFT_WHEEL_wl == 1) 
	  			{
	  				tempW6 = vrad_rr - vref;
	  			}
	  			else 
	  			{
	  				tempW6 = vrad_rl - vref;
	  			}
	  		#else
	  			if(LEFT_WHEEL_wl == 1) 
	  			{
	  				tempW6 = vrad_fr - vref;
	  			}
	  			else 
	  			{
	  				tempW6 = vrad_fl - vref;
	  			}	  	  
	  		#endif
		
			if(YAW_SET == 0)
			{
				if(tempW6 > (int16_t)BTCS_2ND_DMP_DV_HIGH_SIDE) 
				{	
                	WL->hold_timer = BTCS_1ST_HLD_TIME1;
	                YAW_SET = 1;
    	            WL->BTCS_flags = 65;
					Btcs_State2_Check = BTCS_STATE2_CHECKED;
					Btcs_State2_Mode = BTCS_STATE2_DUMP2;			
				}
			}
			else
			{
				if(tempW6 < (int16_t)VREF_1_KPH) 
				{
                	YAW_SET = 0;				
				}
			
				if((WL->hold_timer==0)&&(tempW6 > (int16_t)BTCS_2ND_DMP_DV_HIGH_SIDE))
				{
                	WL->hold_timer = BTCS_1ST_HLD_TIME1;
                	WL->BTCS_flags = 66;
					Btcs_State2_Check = BTCS_STATE2_CHECKED;
					Btcs_State2_Mode = BTCS_STATE2_DUMP2;				
				}	
			}
		}
	}
}

/* ====================================================================== */ 
void    TCSTATE2_INC_COUNT_CHECK(void)
{	
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {		
	    if(WL->btcinc_count != 0) {
    	    WL->btcinc_count --;
        	WL->BTCS_flags = 71; /* checking result : this flag occur 43 times split control */
			Btcs_State2_Check	= BTCS_STATE2_CHECKED;
			Btcs_State2_Mode	= BTCS_STATE2_INC;
    	}
	}	
}    


void    TCSTATE2_INC_LOW_SIDE(void)
{	
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    { 
	    if(WL->hold_timer==0) {
			#if __VDC
        		tempW3 = (int16_t)(WL->av_fspeed - WL->wvref);
        		tempW4 = (int16_t)(WL->av_fspeed_alt - WL->wvref);
			#else
		        tempW3 = (int16_t)(WL->av_fspeed - vref);
        		tempW4 = (int16_t)(WL->av_fspeed_alt - vref);
			#endif

        	tempW5 = WL->btc_slip;
	        tempW5 += (int16_t)VREF_5_KPH;
    	    tempW6 = tempW3 - WL->btc_slip;
        
        	if(tempW3 >= tempW5) {        	         
            	if(WL->btc_speed >= (int16_t)VREF_0_5_KPH) {
                	WL->hold_timer = BTCS_2ND_HLD_TIME1;  /*7*/
	                WL->btcinc_count = 1;
	                WL->BTCS_flags = 72; /* checking result : this flag occur 11 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_INC;
        	    }	                    
	        }
	        
	        if(tempW0 >= WL->b_rad + BTCS_2ND_RIS_DV_LOW_SIDE) {
                	WL->hold_timer = BTCS_2ND_HLD_TIME1;  /*7*/
	                WL->btcinc_count = 2;
	                WL->BTCS_flags = 73; /* checking result : this flag occur 16 times split control */
					Btcs_State2_Check	= BTCS_STATE2_CHECKED;
					Btcs_State2_Mode	= BTCS_STATE2_INC;	        		        	
	        }	        
    	}   	    
	}
}

/* ====================================================================== */ 
void    TCSTATE2_HOLD_DEFAULT(void)
{
    if(Btcs_State2_Check==BTCS_STATE2_NO_CHECKED) 
    {	
		WL->BTCS_flags = 91; /* checking result : this flag occur 711 times split control */
		Btcs_State2_Check	= BTCS_STATE2_CHECKED;
		Btcs_State2_Mode	= BTCS_STATE2_HOLD;
	}
}

/* ====================================================================== */ 
void    TCSTATE2_HOLD_MODE(void)
{
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 1;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    if(WL->hold_timer != 0) WL->hold_timer--;	
}

void    TCSTATE2_INC_MODE(void)
{
    AV_REAPPLY_wl = 1;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    HALF_PULS_wl = 0;
   	WL->slip_count4 = 0;    
}

void    TCSTATE2_DUMP2_MODE(void)
{
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 1;
    BUILT_wl = 0;
    HALF_PULS_wl = 0;		
}

void    TCSTATE2_HOLD_COUNT(void)
{
    if(AV_HOLD_wl == 1) WL->hold_count ++;
    else WL->hold_count = 0;		
}

/* ====================================================================== */ 
void    BTCTEMP(struct W_STRUCT *WL_temp)
{
	WL = WL_temp;

    tempF0 = 0;
    tempF1 = 0;

	Btcs_Temp_Check = BTCS_TEMP_NO_CHECKED;
	
	#if __4WD
		vref_temperature = vref2;
	#else
		vref_temperature = vref;
	#endif
	
    if(BTC_fz == 1) {
        if(WL->arad > WL->av_ref_max) WL->av_ref_max = WL->arad;
    }

    if(WL->btc_tmp > (int16_t)TMP_500) {
        TMP_ERROR_wl = 1;
        if(WL->btc_tmp > (int16_t)TMP_600) WL->btc_tmp = (int16_t)TMP_600;
    }
    else if(WL->btc_tmp < (int16_t)TMP_20) WL->btc_tmp = (int16_t)TMP_20;
    else if(WL->btc_tmp < (int16_t)TMP_300) TMP_ERROR_wl = 0;

// CBS/ABS heating
    if(BLS_ERROR == 0) {
        if(BLS == 1) {
            TMP_RISE_wl = 1;
            tempF0 = 1;
        }
        if(BLS_K == 1) {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
    }
    else {
        if((ABS_fz==1)||(EBD_RA==1)) {
            TMP_RISE_wl = 1;
            tempF0 = 1;
        }
        if((TMP_RISE_wl == 0) && (BTC_fz == 0)) {
            WL->btc_tmp_alt = WL->btc_tmp;
            WL->tmprad_alt = vref;
            WL->tmp_time = 0;
        }
    }

    if(vref <= (int16_t)18) {
        if(WL->inc_count1 > 160) TMP_RISE_wl = 0; //tempF0 = 0;
        else WL->inc_count1++;
    }
    else WL->inc_count1 = 0;

    if(TMP_RISE_wl == 1) {
        if(tempF0 == 0) {
            if(WL->inc_count2 > 160) TMP_RISE_wl = 0;
            else WL->inc_count2++;
        }
    }
    else WL->inc_count2 = 0;

    if(TMP_RISE_wl == 1) {
        if(WL->tmp_time == 140) {
            WL->tmp_time = 0;
            WL->btc_tmp_alt = WL->btc_tmp;
            tempF1 = 1;
        }
        else WL->tmp_time++;
    }
    else {
        WL->tmp_time = 0;
    }

   	if(tempF1 == 1) {
       	if(WL->tmprad_alt > vref) tempW0 = WL->tmprad_alt - vref;
       	else Btcs_Temp_Check=BTCS_TEMP_CHECKED;       

		if(Btcs_Temp_Check==BTCS_TEMP_NO_CHECKED)
		{        
        	tempW0 = (uint16_t)tempW0<<4;

	        WL->tmp_acc = tempW0/(int16_t)29;
    	    if(WL->tmp_acc < 1) WL->tmp_acc = 1;

        	if(ABS_fz == 1) {
            	if(WL->tmp_acc >= (int16_t)78) WL->acc_perc = (int16_t)65; 
            	else WL->acc_perc = (int16_t)60; 
        	}
        	else {
            	if(WL->tmp_acc >= (int16_t)78) WL->acc_perc = (int16_t)128; 
            	else WL->acc_perc = (int16_t)117; 
        	}

        	tempW1 = (uint16_t)WL->tmprad_alt<<4;
        	tempW2 = WL->tmp_acc*29;
        	WL->out_time = (uint16_t)(tempW1/tempW2);
        	WL->out_time += 1;

	        tempW4 = (uint16_t)((int16_t)42/WL->out_time);
    	    tempW4 = (int16_t)64 - tempW4;

			tempUW2 = WL->tmp_acc;		
			tempUW1 = WL->acc_perc;		
			tempUW0 = 100;				
			u16mulu16divu16();			
			tempW6 = tempUW3;			
		
        	tempW3 = (uint16_t)WL->tmprad_alt>>2;	
        
        	if(tempW6 < 1) tempW6 = 1;
        	tempW5 = (uint16_t)((uint32_t)tempW3 * (uint16_t)tempW4);
//        	tempW5 = (uint16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>13);
			tempUW2 = tempW5;			// 2005 07
			tempUW1 = tempW6;			// 2005 07
			tempUW0 = 8192;				// 2005 07
			u16mulu16divu16();			// 2005 07
			tempW5 = tempUW3;			// 2005 07
		        	
        	WL->btc_tmp = (uint16_t)(tempW5/WL->out_time);
        	WL->btc_tmp = (uint16_t)WL->btc_tmp<<5;
        	WL->btc_tmp += WL->btc_tmp_alt;

        	WL->tmprad_alt = vref;
      		Btcs_Temp_Check=BTCS_TEMP_CHECKED;
		}

    }
    
    if(Btcs_Temp_Check==BTCS_TEMP_NO_CHECKED)
	{
		// DISC cooling
    	if(TMP_RISE_wl == 1) {
        	WL->tmp_up = 0;
    	}
    	else {
		// BTCS heating
        	if(BTCS_wl == 1) {
            	if(WL->tmp_state == 1) {
                	WL->btc_tmp_alt = WL->btc_tmp;
	                WL->tmprad_alt = vref_temperature;      // WL->vrad;
        	        WL->tmp_count = 0;
            	}
	            if(WL->tmp_state == 2) {
    	            WL->tmp_count++;
        	    }
            	else if(WL->tmp_state == 3) {
                	WL->tmprad_max = WL->tmprad_alt;
	                WL->tmprad_max += WL->btc_slip;
    	            if(WL->tmprad_max > WL->tmprad_alt) {
        	            tempW0 = WL->tmprad_max - WL->tmprad_alt;
            	        WL->tmprad_max = WL->btc_slip;
                	}
                	else Btcs_Temp_Check=BTCS_TEMP_CHECKED;

					if(Btcs_Temp_Check==BTCS_TEMP_NO_CHECKED)
					{
		                TEMPCALC();

		                tempUW2 = WL->btc_tmp;		// 2004 Sweden
    		            tempUW1 = WL->tmp_count;		// 2004 Sweden
        		        tempUW0 = 141;				// 2004 Sweden
            		    u16mulu16divu16();			// 2004	Sweden
                		WL->btc_tmp = tempUW3;		// 2004 Sweden
                      
	        	        if(HILL_wl == 1) WL->btc_tmp = (uint16_t)WL->btc_tmp<<1;
    	        	    WL->btc_tmp += WL->btc_tmp_alt;
       	        		WL->tmprad_alt = vref_temperature;
            	    	WL->max_spin = 0;
            	    	Btcs_Temp_Check=BTCS_TEMP_CHECKED;
            	    }
            	}
            	else if(WL->tmp_state == 4){
           			tempW0 = WL->vrad - vref_temperature;

	                if(tempW0 > WL->max_spin) WL->max_spin = tempW0;
    	            if(WL->tmp_up == 140) {
        	            WL->tmp_up = 0;
            	        WL->btc_tmp_alt = WL->btc_tmp;
           	        	WL->tmprad_max = vref_temperature;
   	                	if(vref_temperature >= (int16_t)VREF_30_KPH) {
                        	if(WL->max_spin > (int16_t)VREF_20_KPH) {
                           		WL->max_spin = (uint16_t)WL->max_spin>>1;
                       		}
                   		}

	                    WL->tmprad_max += (uint16_t)WL->max_spin>>2;
    	                if(WL->tmprad_max > WL->tmprad_alt) {
        	                tempW0 = WL->tmprad_max - WL->tmprad_alt;
            	        }
                	    else Btcs_Temp_Check=BTCS_TEMP_CHECKED;

						if(Btcs_Temp_Check==BTCS_TEMP_NO_CHECKED)
						{	
    		                TEMPCALC();

        		            if(HILL_wl == 1) WL->btc_tmp = (uint16_t)WL->btc_tmp<<1;
            		        WL->btc_tmp += WL->btc_tmp_alt;
            		        WL->tmprad_alt = vref_temperature;
                    		WL->max_spin = 0;

	                    	Btcs_Temp_Check=BTCS_TEMP_CHECKED;
	                    }
    	            }
        	        else WL->tmp_up++;
            	}
        	}
    	}
    }

    if(WL->tmp_down == 143) {
        WL->tmp_down = 0;
        tempW0 = (uint16_t)WL->btc_tmp>>5;   // /32
        if(tempW0 >= (int16_t)450) tempW1 = 1;
        else if(tempW0 >= (int16_t)350) tempW1 = 2;
        else if(tempW0 >= (int16_t)250) tempW1 = 3;
        else if(tempW0 >= (int16_t)170) tempW1 = 4;
        else if(tempW0 >= (int16_t)110) tempW1 = 5;
        else if(tempW0 >= (int16_t)70) tempW1 = 6;
        else if(tempW0 >= (int16_t)40) tempW1 = 7;
        else tempW1 = 8;
        if(vref_temperature <= (int16_t)VREF_10_KPH) {
            tempW1 = (uint16_t)tempW1<<2;
            tempW1 += 4;
        }
        else if(vref_temperature <= (int16_t)VREF_30_KPH) {
            tempW1 = (uint16_t)tempW1<<1;
            tempW1 += 2;
        }
        else if(vref_temperature <= (int16_t)VREF_60_KPH) tempW1 += 1;
        else if(vref_temperature <= (int16_t)VREF_90_KPH) {
            tempW1 = (uint16_t)tempW1>>1;
            tempW1 += 1;
        }
        else tempW1 = (uint16_t)tempW1>>1;
        tempW1 += 5;
        WL->cool_tmp = tempW0/tempW1;
        WL->btc_tmp -= WL->cool_tmp;
    }
    else WL->tmp_down++;	
}

/* ====================================================================== */
void    TEMPCALC(void)
{
    tempW0 = (uint16_t)tempW0<<4;
                                         // temp_acc*16
    WL->tmp_acc = tempW0/(int16_t)29;
    if(WL->tmp_acc < 1) WL->tmp_acc = 1;
                                         // % calculate*100
    if(WL->tmp_acc >= (int16_t)78) WL->acc_perc = (int16_t)85;
    else WL->acc_perc = (int16_t)78;
                                         // ts calculate
    tempW1 = (uint16_t)WL->tmprad_max<<4;
    tempW2 = WL->tmp_acc*29;
    WL->out_time = (uint16_t)(tempW1/tempW2);
    WL->out_time += 1;
                                         // tmprad_alt(kph)*0.259*8
//    tempW3 = (uint16_t)WL->tmprad_max>>2;
                                         // (1-2t/3ts)*64
    tempW4 = (uint16_t)((int16_t)42/WL->out_time);
    tempW4 = (int16_t)64 - tempW4;
                                         // /100 /8192
//        tempW6 = (uint16_t)(((uint32_t)WL->tmp_acc * (uint16_t)WL->acc_perc)/(uint16_t)100);	// 2004 Sweden
		tempUW2 = WL->tmp_acc;		// 2004 Sweden
		tempUW1 = WL->acc_perc;		// 2004 Sweden
		tempUW0 = 100;				// 2004 Sweden
		u16mulu16divu16();			// 2004 Sweden
		tempW6 = tempUW3;			// 2004 Sweden
		
        tempW3 = (uint16_t)WL->tmprad_max>>2;	// 2004 Sweden
    
    if(tempW6 < 1) tempW6 = 1;
    tempW5 = (uint16_t)((uint32_t)tempW3 * (uint16_t)tempW4);
//    tempW5 = (uint16_t)(((uint32_t)tempW5 * (uint16_t)tempW6)>>13);
	tempUW2 = tempW5;			// 2005 07
	tempUW1 = tempW6;			// 2005 07
	tempUW0 = 8192;				// 2005 07
	u16mulu16divu16();			// 2005 07
	tempW5 = tempUW3;			// 2005 07
			
    WL->btc_tmp = (uint16_t)(tempW5/WL->out_time);
    WL->btc_tmp = (uint16_t)WL->btc_tmp<<5;
}

/* ====================================================================== */
void    TEMPERATUREC(void)
{
//  vcc_off_flg = 1;
#if __4WD
	TMP_OFF(&FL);
   	TMP_OFF(&FR);
	TMP_OFF(&RL);
    TMP_OFF(&RR);

    vcc_off_flg = 0;
   	if((btc_tmp_fl < (int16_t)TMP_80) && (btc_tmp_fr < (int16_t)TMP_80) && (BLS_EPR == 1) &&
   		(btc_tmp_rl < (int16_t)TMP_80) && (btc_tmp_rr < (int16_t)TMP_80) && (BLS_EPR == 1)) {
       	vcc_off_flg = 1;
   	}
#else // __2WD (FWD or RWD)
	#if __REAR_D
    	TMP_OFF(&RL);
    	TMP_OFF(&RR);

	    vcc_off_flg = 0;
    	if((btc_tmp_rl < (int16_t)TMP_80) && (btc_tmp_rr < (int16_t)TMP_80) && (BLS_EPR == 1)) {
        	vcc_off_flg = 1;
    	}
	#else
    	TMP_OFF(&FL);
    	TMP_OFF(&FR);

    	vcc_off_flg = 0;
    	if((btc_tmp_fl < (int16_t)TMP_80) && (btc_tmp_fr < (int16_t)TMP_80) && (BLS_EPR == 1)) {
        	vcc_off_flg = 1;
    	}
	#endif
#endif

#if !SIM_MATLAB
#if __S12D
    if(BLS_EPR==0) {
        if(BLS_WRITE == 1) ee_btcs_data |= 0x10;
        else ee_btcs_data &= 0xef;
        ee_btcs_data &= 0xdf;
        if((weu1EraseSector==0)&&(weu1WriteSector==0))
        {
            if((WE_u8ReadEEP_Byte(0xa8, &com_rx_buf[0], 4))==1)
            {
                if(com_rx_buf[0]!=ee_btcs_data)
                {
                    weu16Eep_addr=0xa8;
                    com_rx_buf[0]=ee_btcs_data;
                    weu16EepBuff[0]=*(uint16_t*)&com_rx_buf[0];
                    weu16EepBuff[1]=*(uint16_t*)&com_rx_buf[2];
                    weu1WriteSector=1;
            	}
                else BLS_EPR=1;
            }
        }
    }
    
#else    //D60A 
    if(BLS_EPR==0) {
        if(BLS_WRITE == 1) ee_btcs_data |= 0x10;
        else ee_btcs_data &= 0xef;
        ee_btcs_data &= 0xdf;
        if((row_erase_flg==0)&&(byte_write_flg==0))
        {
            if((WE_u8ReadEEP_Byte(0xa8, &com_rx_buf[0], 1))==1)
            {
                if(com_rx_buf[0]!=ee_btcs_data)
                {
                    weu16Eep_addr=0xa8;
                    data_ee=ee_btcs_data;
                    byte_write_flg=1;


                }
                else BLS_EPR=1;
            }
        }
    }
#endif
#endif
}

/* ====================================================================== */
void    TMP_OFF(struct W_STRUCT *WL)
{
    if(WL->tmp_down == 143) {
        WL->tmp_down = 0;
        tempW0 = (uint16_t)WL->btc_tmp>>5;   // /32
        if(tempW0 >= (int16_t)450) tempW1 = 1;
        else if(tempW0 >= (int16_t)350) tempW1 = 2;
        else if(tempW0 >= (int16_t)250) tempW1 = 3;
        else if(tempW0 >= (int16_t)170) tempW1 = 4;
        else if(tempW0 >= (int16_t)110) tempW1 = 5;
        else if(tempW0 >= (int16_t)70) tempW1 = 6;
        else if(tempW0 >= (int16_t)40) tempW1 = 7;
        else tempW1 = 8;
        tempW1 = (uint16_t)tempW1<<2;
        tempW1 += 9;
        WL->cool_tmp = tempW0/tempW1;
        WL->btc_tmp -= WL->cool_tmp;
    }
    else WL->tmp_down++;
}
#endif

#endif //end of #if __BTCS_NEO_STRUCTURE

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_Lbtcs_Neo
	#include "Mdyn_autosar.h"               
#endif
