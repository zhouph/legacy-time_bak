#ifndef _LCESPINTERFACEIVSS_
#define _LCESPINTERFACEIVSS_
#include "LVarHead.h"
struct IVSS_U8_BIT_STRUCT_t 
{
    unsigned char bit7	:1;
    unsigned char bit6	:1;
    unsigned char bit5	:1;
    unsigned char bit4	:1;
    unsigned char bit3	:1;
    unsigned char bit2	:1;
    unsigned char bit1	:1;
    unsigned char bit0	:1;
};

struct IVSS_U8_BIT_STRUCT_t IVSSF01, IVSSF02, IVSSF03, IVSSF04, IVSSF05;

#define Ivss_TCS1_TCS_REQ            IVSSF01.bit0       
#define Ivss_TCS1_TCS_CTL            IVSSF01.bit1     
#define Ivss_ABS_CTL                 IVSSF01.bit2     
#define Ivss_TCS1_ESP_CTL            IVSSF01.bit3     
#define Ivss_TCS3_BrakeLight         IVSSF01.bit4  
#define Ivss_TCS3_DriverBraking      IVSSF01.bit5
#define Ivss_Clu_ActiveEcoSW         IVSSF01.bit6
#define lccanIVSS_Clu_ActiveEcoSW    IVSSF01.bit7

#define Ivss_EMS1_N_IVD              IVSSF02.bit0
#define Ivss_EMS1_TQI_IVD            IVSSF02.bit1
#define Ivss_EMS2_BRAKE_ACT_IVD      IVSSF02.bit2
#define Ivss_EMS2_TPS_IVD            IVSSF02.bit3
#define Ivss_EMS2_PV_AV_CAN_IVD      IVSSF02.bit4
#define Ivss_ESP2_LAT_ACCEL_IVD      IVSSF02.bit5
#define Ivss_ESP2_LONG_ACCEL_IVD     IVSSF02.bit6
#define Ivss_ESP2_CYL_PRES_IVD       IVSSF02.bit7

#define Ivss_ESP2_YAW_RATE_IVD       IVSSF03.bit0
#define Ivss_SAS1_SAS_Angle_IVD      IVSSF03.bit1
#define Ivss_TCS5_WHEEL_FL_IVD       IVSSF03.bit2
#define Ivss_TCS5_WHEEL_FR_IVD       IVSSF03.bit3
#define Ivss_TCS5_WHEEL_RL_IVD       IVSSF03.bit4
#define Ivss_TCS5_WHEEL_RR_IVD       IVSSF03.bit5
#define Ivss_VSM2_CR_Mdps_StrTq_IVD  IVSSF03.bit6
#define Ivss_VSM2_CR_Mdps_OutTq_IVD  IVSSF03.bit7

#define Ivss_SCC3_VSM_Warn_IVD       IVSSF04.bit0
#define Ivss_EMS11_RxMissing_Flg     IVSSF04.bit1
#define Ivss_EMS12_RxMissing_Flg     IVSSF04.bit2
#define Ivss_TCU12_RxMissing_Flg     IVSSF04.bit3
#define Ivss_MDPS12_RxMissing_Flg    IVSSF04.bit4
#define Ivss_SCC12_RxMissing_Flg     IVSSF04.bit5
#define Ivss_CLU13_RxMissing_Flg     IVSSF04.bit6
#define Ivss_TCU2_CUR_GR_IVD         IVSSF04.bit7

#define Ivss_TCS3_DriverBraking_IVD  IVSSF05.bit0

extern uint8_t Ivss_ModeECS;
extern uint8_t Ivss_ModeMDPS;
extern uint8_t Ivss_ModeTM;
extern uint8_t Ivss_ModeENG;
extern uint8_t Ivss_ModeESC;
extern uint8_t Ivss_Mode4WD;
extern uint8_t Ivss_AliveCounter;
extern uint8_t Ivss_CheckSum;

extern int16_t Ivss_EMS1_TQI_ACOR;
extern int16_t Ivss_EMS1_TQI;
extern int16_t Ivss_EMS1_TQFR;
extern int16_t Ivss_EMS2_TQ_STND;
extern int16_t Ivss_EMS2_TPS;
extern int16_t Ivss_EMS2_PV_AV_CAN;
extern int16_t Ivss_ESP2_LAT_ACCEL;
extern int16_t Ivss_ESP2_LONG_ACCEL;
extern int16_t Ivss_ESP2_CYL_PRES;
extern int16_t Ivss_ESP2_YAW_RATE;
extern int16_t Ivss_SAS1_SAS_Angle;
extern int16_t Ivss_TCS1_TQI_TCS;
extern int16_t Ivss_TCS5_WHEEL_FL;
extern int16_t Ivss_TCS5_WHEEL_FR;
extern int16_t Ivss_TCS5_WHEEL_RL;
extern int16_t Ivss_TCS5_WHEEL_RR;
extern int16_t Ivss_TCU2_CUR_GR;
extern int16_t Ivss_VSM2_CR_Mdps_StrTq;
extern int16_t Ivss_VSM2_CR_Mdps_OutTq;

extern uint16_t Ivss_EMS1_N;

extern uint8_t Ivss_EMS2_MUL_CODE;
extern uint8_t Ivss_EMS2_BRAKE_ACT;
extern uint8_t Ivss_VSM2_CF_Mdps_Def;
extern uint8_t Ivss_SCC3_VSM_Warn;
extern uint8_t Ivss_Clu_DrivingModeSwi;

//extern uint8_t CF_Clu_DrivingModeSwi;

extern uint8_t lccanIVSS_EMS2_MUL_CODE;
extern uint8_t lccanIVSS_VSM_Warn_b;
extern uint8_t lcespIVSS_Clu_DrivingModeSwi;
extern int16_t lccanIVSS_TCU2_CUR_GR;
extern uint8_t Ivss_ECS1_RoadCond;


#endif
