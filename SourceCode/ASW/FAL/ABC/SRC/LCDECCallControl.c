/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCDECCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCDECCallControl.h"
  #if __DEC
#include "LADECCallActHW.h"
#include "LCESPCalInterpolation.h"
#include "Hardware_Control.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "Hardware_Control.h"
#include "LSABSCallSensorSignal.h"
#include "LCACCCallControl.h"
#include "LDABSCallEstVehDecel.H"

  #endif
  #if __HDC
#include "LCHDCCallControl.h"
  #endif
  #if __EPB_INTERFACE
#include "LCEPBCallControl.h"
  #endif

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __DEC

 

int8_t    lcs8DecState;
uint16_t    lcu16DecObserver;
uint16_t    lcu16DecFeedForwardCnt, lcu16DecFeedForwardTh;
uint16_t    lcu16DecTimeToAct, lcu16DecTimeToActCnt;
uint16_t    lcu16DecClosingCnt, lcu16DecClosingTime;
int16_t     lcs16DecTargetG, lcs16DecDecel, lcs16DecDecelOld;
int16_t     lcs16DecRoughCnt,lcs16DecCtrlError,lcs16DecClosingCur;
int16_t		lcs16DecCtrlInput;
uint16_t    lcu16DecClosingNum;

static int16_t     lcs16_dec_tempW9,lcs16_dec_tempW8,lcs16_dec_tempW7;
static uint8_t   lcu8DecIntCnt;
static int16_t     lcs16DecCtrlPGain, lcs16DecCtrlDGain, lcs16DecCtrlIGain;
static int16_t     lcs16DecCtrlRoughPGain, lcs16DecCtrlRoughDGain, lcs16DecCtrlRoughIGain;
static int16_t     lcs16DecCtrlErrorOld, lcs16DecCtrlErrorDot;
static int16_t     lcs16DecCtrlErrorInt, lcs16DecCtrlErrorSum;
static int16_t     lcs16DecPreCurMax;

 


DEC_CTRL_t clDecHdc, clDecAcc, clDecEpb, clDecTsp;

struct  U8_BIT_STRUCT_t DEC00,DEC01;
  #endif

/* Local Function prototype ****************************************/
  #if __DEC
static void LCDEC_vCalDecelError(void);
static void LCDEC_vDetFinalCmd(void);
static void LCDEC_vCalFeedForwardTh(void);
static void LCDEC_vCalFeedBackCtrlGain(void);
static void LCDEC_vCalFeedBackCtrlInput(void);
static void LCDEC_vFeedForwardCtrl(void);
static void LCDEC_vFeedBackCtrl(void);
static void LCDEC_vSmoothClosingCtrl(void);
static void LCDEC_vResetCtrlVariable(void);
  #endif

/* Implementation***************************************************/
  #if __DEC
void LCDEC_vCallControl(void)
{

	/* Initial clear	*/
	lcu1decFFCtrl_flg =0;

    LCDEC_vDetFinalCmd();

    LCDEC_vCalDecelError();

   lcu1DecActiveFlgOld = lcu1DecActiveFlg;
    switch(lcs8DecState)
    {
        /********************************************************/
        case S8_DEC_STANDBY:                /* STANDBY */
            lcu1DecActiveFlg=0;
            if(lcu1DecCtrlReqFlg==0)
            {
                lcs8DecState=S8_DEC_TERMINATION;
                LCDEC_vResetCtrlVariable();
                lcu16DecObserver=4;
            }
            else if(lcs16DecTargetG<30)
            {
                lcu1DecActiveFlg=1;
                lcs8DecState=S8_DEC_ACTIVATION;
                LCDEC_vCalFeedForwardTh();
                if((lcu1DecFeedForwardReqFlg==1)&&(lcu16DecFeedForwardCnt<lcu16DecFeedForwardTh)
                	&&(lcs16DecCtrlError < -S16_DEC_FFCTRL_ERROR_MAX))
                {
                    LCDEC_vFeedForwardCtrl();
                    lcu16DecFeedForwardCnt++;
                    lcu16DecObserver=5;
                }
                else
                {
                    LCDEC_vFeedBackCtrl();
                    lcu16DecObserver=6;
                }
            }
            else
            {
                lcs8DecState=S8_DEC_STANDBY;
                lcu16DecObserver=7;
            }

            break;

        /********************************************************/
        case S8_DEC_ACTIVATION:             /* ACTIVATION */
            lcu1DecActiveFlg = 1;
            if((lcu1DecCtrlReqFlg==0)||(lcs16DecTargetG>=30))
            {
                if(lcu1DecSmoothClosingReqFlg==1)
                {
                    lcs8DecState=S8_DEC_CLOSING;
                    lcs16DecClosingCur=lcs16DecPreCurMax-U16_DEC_NO_PRE_CUR;
                    if(lcs16DecClosingCur>168) {lcs16DecClosingCur=168;}
                    else if(lcs16DecClosingCur<50) {lcs16DecClosingCur=50;}
                    else {lcs16DecClosingCur=lcs16DecClosingCur;}
                    LCDEC_vSmoothClosingCtrl();
                    lcu16DecObserver=8 ;
                }
                else
                {
                    lcu1DecActiveFlg=0;
                    lcs8DecState=S8_DEC_TERMINATION;
                    LCDEC_vResetCtrlVariable();
                    lcu16DecObserver=9;
                }
            }
            else
            {
                LCDEC_vCalFeedForwardTh();
                if((lcu1DecFeedForwardReqFlg==1)&&(lcu16DecFeedForwardCnt<lcu16DecFeedForwardTh)
                	&&(lcs16DecCtrlError < -S16_DEC_FFCTRL_ERROR_MAX))
                {
                    LCDEC_vFeedForwardCtrl();
                    lcu16DecFeedForwardCnt++;
                    lcu16DecObserver=10;
                }
                else
                {
                    LCDEC_vFeedBackCtrl();
                }
                lcu1DecSmoothClosingEndFlg=0;
            }
            break;

        /********************************************************/
        case S8_DEC_CLOSING:                /* CLOSING */
            lcu1DecActiveFlg=1;
            lcu16DecClosingTime=lcu16DecFeedForwardTh/2;
              #if __TCMF_CONTROL
            if(lcs16DecPreCurMax<=U16_DEC_NO_PRE_CUR)
              #else
            if(lcu16DecClosingCnt>=lcu16DecClosingTime)
              #endif
            {
                lcu1DecActiveFlg=0;
                lcu1DecSmoothClosingEndFlg=1;
                lcs8DecState = S8_DEC_TERMINATION;
                LCDEC_vResetCtrlVariable();
                lcu16DecObserver=12;
            }
            else
            {
                lcu1DecSmoothClosingEndFlg=0;
                lcs8DecState=S8_DEC_CLOSING;
                LCDEC_vSmoothClosingCtrl();
                lcu16DecClosingCnt++;
                lcu16DecObserver=13;
            }
            break;

        /********************************************************/
        case S8_DEC_TERMINATION:                /* TERMINATION */
            lcu1DecActiveFlg=0;
            if(lcu1DecCtrlReqFlg==1)
            {
                lcs8DecState=S8_DEC_STANDBY;
                LCDEC_vResetCtrlVariable();
                lcu16DecObserver=3;
            }
            else
            {
                lcs8DecState=S8_DEC_TERMINATION;
                lcu16DecObserver=2;
            }

            break;

        /********************************************************/
        default:                            /* DEFAULT */
            lcu1DecActiveFlg=0;
            lcs8DecState=S8_DEC_TERMINATION;
            lcu16DecObserver=1;
            break;
    }
}


static void LCDEC_vCalDecelError(void)
{
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)
        ||(FL.Rough_road_detect_flag2==1)||(FR.Rough_road_detect_flag2==1)
        ||(RL.Rough_road_detect_flag2==1)||(RR.Rough_road_detect_flag2==1))
    {
        if (lcs16DecRoughCnt < L_U8_TIME_10MSLOOP_100MS)
        {
            lcs16DecRoughCnt++;
        }
        else {}
    }
    else
    {
        if (lcs16DecRoughCnt > 0)
        {
            lcs16DecRoughCnt--;
        }
        else {}
    }

    lcs16_dec_tempW9 = LCESP_s16IInter2Point(lcs16DecRoughCnt,0      , L_U8FILTER_GAIN_10MSLOOP_6_5HZ,  /* 5Hz  */
                                              L_U8_TIME_10MSLOOP_100MS, L_U8FILTER_GAIN_10MSLOOP_2HZ); /* 2Hz  */

    if ((ABS_fl==1)||(ABS_fr==1)||(ABS_rl==1)||(ABS_rr==1))
    {
        lcs16_dec_tempW9 = L_U8FILTER_GAIN_10MSLOOP_2HZ;
    }
    else { }

    lcs16DecDecelOld=lcs16DecDecel;
    if((ABS_fl==1)&&(rel_lam_fl<-5)
        &&(ABS_fr==1)&&(rel_lam_fr<-5)
        &&(ABS_rl==1)&&(rel_lam_rl<-5)
        &&(ABS_rr==1)&&(rel_lam_rr<-5))
    {
      #if  __HDC
        lcs16DecDecel=LCESP_s16Lpf1Int((lsabss16RawAxSignal/10),lcs16DecDecelOld,(uint8_t)lcs16_dec_tempW9);
      #else
        lcs16DecDecel=LCESP_s16Lpf1Int(ebd_filt_grv,lcs16DecDecelOld,(uint8_t)lcs16_dec_tempW9);
      #endif
    }
    else
    {
	  #if __ACC
    	lcs16DecDecel = lsabss16DecelRefiltByVref5/10;
	  #else
        lcs16DecDecel=LCESP_s16Lpf1Int(ebd_filt_grv,lcs16DecDecelOld,(uint8_t)lcs16_dec_tempW9);
	  #endif
    }


    lcs16DecCtrlError=lcs16DecTargetG-lcs16DecDecel;

  #if  __HDC
    lcs16_dec_tempW9 = LCESP_s16IInter2Point (lcs16DecTargetG*10, - LONG_0G8G,LONG_0G8G,
                                                                  - LONG_0G5G,LONG_0G5G);
    lcs16_dec_tempW9 = lcs16_dec_tempW9/10;
  #else
    lcs16_dec_tempW9 = LCESP_s16IInter2Point (lcs16DecTargetG*10, - 800,800,
                                                                  - 500,500);
    lcs16_dec_tempW9 = lcs16_dec_tempW9/10;
  #endif
    if(lcs16DecCtrlError>lcs16_dec_tempW9) {lcs16DecCtrlError=lcs16_dec_tempW9;}
    else if(lcs16DecCtrlError<-lcs16_dec_tempW9) {lcs16DecCtrlError=-lcs16_dec_tempW9;}
    else {lcs16DecCtrlError=lcs16DecCtrlError;}

    lcs16DecCtrlErrorDot=lcs16DecCtrlError-lcs16DecCtrlErrorOld;
    if(lcs16DecCtrlErrorDot>=5) {lcs16DecCtrlErrorDot=5;}
    else if(lcs16DecCtrlErrorDot<-5) {lcs16DecCtrlErrorDot=-5;}
    else {lcs16DecCtrlErrorDot=lcs16DecCtrlErrorDot;}

    lcs16DecCtrlErrorOld=lcs16DecCtrlError;

	if (lcu8DecIntCnt<L_U8_TIME_10MSLOOP_20MS)
	{
		lcs16_dec_tempW9 = (lcs16DecCtrlError>5)?5:lcs16DecCtrlError;
		lcs16_dec_tempW9 = (lcs16_dec_tempW9>-5)?lcs16_dec_tempW9:-5;

        lcs16DecCtrlErrorSum+=lcs16_dec_tempW9;
        lcu8DecIntCnt++;

        if(lcu8DecIntCnt>=L_U8_TIME_10MSLOOP_20MS)
        {
	        lcu8DecIntCnt=0;
	        lcs16DecCtrlErrorInt=lcs16DecCtrlErrorSum;
	        lcs16DecCtrlErrorSum=0;
        }
        else { }
	}
	else
	{
		lcu8DecIntCnt=0;
		lcs16DecCtrlErrorSum=0;
		lcs16DecCtrlErrorInt=0;
	}
}



static void LCDEC_vDetFinalCmd(void)
{
    int16_t i;
    DEC_CTRL_t *lcDecCtrlPt[4];

    lcDecCtrlPt[0]=&clDecHdc;
    lcDecCtrlPt[1]=&clDecAcc;
    lcDecCtrlPt[2]=&clDecEpb;
    lcDecCtrlPt[3]=&clDecTsp;

    lcu1DecCtrlReqFlg=0;
    lcu1DecSmoothClosingReqFlg=0;
    lcs16DecTargetG=lcDecCtrlPt[0]->lcs16DecReqTargetG;
    lcu1DecFeedForwardReqFlg=lcDecCtrlPt[0]->lcu8DecFeedForwardReq;

    for(i=0;i<4;i++)
    {
        if((lcDecCtrlPt[i]->lcu8DecCtrlReq)>=1)
        {
            lcu1DecCtrlReqFlg|=1;
            if((lcDecCtrlPt[i]->lcs16DecReqTargetG)<lcs16DecTargetG)
            {
                lcs16DecTargetG=lcDecCtrlPt[i]->lcs16DecReqTargetG;
                lcu1DecFeedForwardReqFlg=lcDecCtrlPt[i]->lcu8DecFeedForwardReq;
            }
        }
    }
    if(lcu1DecCtrlReqFlg==0)
    {
        for(i=0;i<4;i++)
        {
            if((lcDecCtrlPt[i]->lcu8DecSmoothClosingReq)>=1)
            {
                lcu1DecSmoothClosingReqFlg|=1;
            }
        }
    }

    lcs16DecTargetG = lcs16DecTargetG/10;

    /**************************************************************/
      #if __TCMF_CONTROL
    if((AFZ_OK==1)&&(ABS_fl==1)&&(ABS_fr==1)&&(ABS_rl==1)&&(ABS_rr==1)
        &&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
    {
        if (lcs16DecTargetG < -AFZ_0G08)
        {
            if(aref_avg>afz)
            {
                if(lcs16DecTargetG<(aref_avg-AFZ_0G08))
                {
                    lcs16DecTargetG=aref_avg-AFZ_0G08;
                }
                else { }
            }
            else
            {
                if(lcs16DecTargetG<(afz-AFZ_0G08))
                {
                    lcs16DecTargetG = afz-AFZ_0G08;
                }
                else { }
            }

            if (lcs16DecTargetG > -AFZ_0G08)
            {
                lcs16DecTargetG = -AFZ_0G08;
            }
            else  { }
        }
        else { }
    }
    else if ((AFZ_OK==1)&&(ABS_fl==1)&&(ABS_fr==1)&&(ABS_rl==1)&&(ABS_rr==1))
    {
        if (lcs16DecTargetG < -AFZ_0G1)
        {
            if(aref_avg>afz)
            {
                if(lcs16DecTargetG<(aref_avg-AFZ_0G1))
                {
                    lcs16DecTargetG=aref_avg-AFZ_0G1;
                }
                else { }
            }
            else
            {
                if(lcs16DecTargetG<(afz-AFZ_0G1))
                {
                    lcs16DecTargetG = afz-AFZ_0G1;
                }
                else { }
            }

            if (lcs16DecTargetG > -AFZ_0G1)
            {
                lcs16DecTargetG = -AFZ_0G1;
            }
            else  { }
        }
        else { }
    }
    else { }

    /**************************************************************/
/*08SWD-10*/
#if	__Decel_Ctrl_Integ
    lcs16DecPreCurMax=(MFC_Current_DEC_P>=MFC_Current_DEC_S)?MFC_Current_DEC_P:MFC_Current_DEC_S;
#else
      #if __ACC
    lcs16DecPreCurMax=(MFC_Current_ACC_P>=MFC_Current_ACC_S)?MFC_Current_ACC_P:MFC_Current_ACC_S;
      #elif __EPB_INTERFACE
    lcs16DecPreCurMax=(MFC_Current_EPB_P>=MFC_Current_EPB_S)?MFC_Current_EPB_P:MFC_Current_EPB_S;
      #else
    lcs16DecPreCurMax=(TC_MFC_Voltage_Primary>=TC_MFC_Voltage_Secondary)?TC_MFC_Voltage_Primary:TC_MFC_Voltage_Secondary;
      #endif
#endif


      #endif
}


static void LCDEC_vSmoothClosingCtrl(void)
{

      #if __TCMF_CONTROL
    if(lcs16DecPreCurMax>(U16_DEC_NO_PRE_CUR+100))
    {
        lcu16DecClosingNum=3;
    }
    else if(lcs16DecPreCurMax>(U16_DEC_NO_PRE_CUR+50))
    {
        lcu16DecClosingNum=4;
    }
    else
    {
        lcu16DecClosingNum=5;
    }
       #if ((__ACC)&&(__ADC))
    if(lcu1AccActiveFlg==1)
    {
    	if(mtp>U8_ADC_FADE_OUT_LEVEL_ACCEL_REF)
    	{
    		lcu16DecClosingNum = (uint16_t)U8_ADC_FADE_OUT_LEVEL_ACCEL;
    	}
    	else
    	{
			lcu16DecClosingNum = (uint16_t)U8_ADC_FADE_OUT_LEVEL;
		}
    }
    else { }
       #elif (__ACC)&&(!defined(__ACC_RealDevice_INTERFACE))
    if(mtp>MTP_20_P)
    {
        lcu16DecClosingNum=1;
    }
    else if(mtp>MTP_5_P)
    {
        lcu16DecClosingNum=2;
    }
    else
    {
        ;
    }
       #endif
       
      #if (__EPB_INTERFACE)&&(__DISABLE_DITHER_EPBI)
    if(lcu1EpbActiveFlg==1)
    {
        if(lcu8EpbCtrlReq==1)
        {
            lcu16DecClosingNum=3;	
        }
        else
        {
        	lcu16DecClosingNum=2;
        }
    }
      #endif       
      #else
    lcu16DecClosingNum=4;
      #endif

    if((lcu16DecClosingCnt%lcu16DecClosingNum)<1)
    {
        lcu8DecValveMode=U8_DEC_DUMP;
          #if __TCMF_CONTROL
        lcu8DecMotorMode=0;
          #else
        lcu8DecMotorMode=1;
          #endif
    }
    else
    {
        lcu8DecValveMode=U8_DEC_HOLD;
        lcu8DecMotorMode=0;
    }
}


static void LCDEC_vResetCtrlVariable(void)
{
    lcu16DecTimeToActCnt=0;
    lcu16DecFeedForwardCnt=0;
    lcu16DecClosingCnt=0;

	lcs16DecCtrlInput=0;
	lcs16DecCtrlPGain=0;
	lcs16DecCtrlDGain=0;
	lcs16DecCtrlIGain=0;
}


static void LCDEC_vCalFeedForwardTh(void)
{
    if(lcs16DecCtrlError>=0)
    {
        lcu16DecFeedForwardTh=U16_DEC_FEEDFORWARD_MIN;
    }
    else
    {
        lcu16DecFeedForwardTh=(uint16_t)((-lcs16DecCtrlError)+U16_DEC_FEEDFORWARD_MIN);
    }
}


static void LCDEC_vFeedForwardCtrl(void)
{
	lcu1decFFCtrl_flg =1;

	lcs16_dec_tempW9 = -lcs16DecTargetG*10;							/* + lcs16HdcAvr_ThetaG; 		reg (0.001g)	*/

#if __HDC																					/* MGH40 > MGH60 : 100 정도 차이남	*/
    lcs16_dec_tempW8 = LCESP_s16IInter5Point(lcs16_dec_tempW9,LONG_0G05G   	,350,/*21,153ms */
    														  LONG_0G1G		,350,/*32,230ms */
    														  LONG_0G2G		,365,/*43,304ms */
        	                          			              LONG_0G3G   	,420,/*55,390ms,*/
            	                                			  LONG_0G4G   	,453);/*64);450ms*/
#else
    lcs16_dec_tempW8 = LCESP_s16IInter5Point(lcs16_dec_tempW9,50   		,350,/*21,153ms */
    														  100		,350,/*32,230ms */
    														  200		,365,/*43,304ms */
        	                          			              300   	,420,/*55,390ms,*/
            	                                			  400   	,453);/*64);450ms*/
#endif

	if(((lcu16DecFeedForwardCnt%2)==0)
		&&(MFC_Current_HDC_S<lcs16_dec_tempW8)&&(MFC_Current_HDC_P<lcs16_dec_tempW8)
		&&(MFC_Current_DEC_S<lcs16_dec_tempW8)&&(MFC_Current_DEC_P<lcs16_dec_tempW8))
	{
    	lcu8DecValveMode=U8_DEC_APPLY;
    }
    else
    {
    	lcu8DecValveMode=U8_DEC_HOLD;
    }

    if((lcu16DecFeedForwardCnt%L_U8_TIME_10MSLOOP_70MS)<2)
    {
        lcu8DecMotorMode=1;
    }
    else
    {
        lcu8DecMotorMode=0;
    }
}


static void LCDEC_vFeedBackCtrl(void)
{

    LCDEC_vCalFeedBackCtrlGain();
    LCDEC_vCalFeedBackCtrlInput();

    if(((lcs16DecCtrlError<=S16_DEC_DEADZONE_U)&&(lcs16DecCtrlError>=S16_DEC_DEADZONE_L)
  #if __HDC
        &&
        (
        ((lcu1HdcActiveFlg==1)&&(lcs16HdcCtrlError<-VREF_0_5_KPH))
        ||((lcu1HdcActiveFlg==1)&&(lcs16HdcCtrlError>VREF_0_5_KPH))
        ||(lcu1HdcActiveFlg==0)
        )
  #endif
    )
  #if (__HDC) && (__TARGET_SPD_ADAPTATION)    
        || ((lcu1HdcActiveFlg==1)&&(lcu8HdcDriverBrakeIntend==1))
  #endif
  #if __EPB_INTERFACE
        ||((lcu1EpbActiveFlg==1)&&((vref<S16_EPBI_FORCED_HOLD_SPD)||(lcu8EpbiEpbState==EPBI_EPB_CLAMPING)))
  #endif
    )    
    {
        lcu8DecValveMode=U8_DEC_HOLD;
        lcu8DecMotorMode=0;

        if (lcu16DecTimeToActCnt < U16_DEC_CTRL_PERIOD_MAX)
        {
            lcu16DecTimeToActCnt++;
        }
        else
        {
            lcu16DecTimeToActCnt = U16_DEC_CTRL_PERIOD_MAX;
        }
        lcu16DecObserver=50;
    }
    else
    {
        tempUW0=lcu16DecTimeToActCnt%lcu16DecTimeToAct;
        if((tempUW0>0)&&(lcu16DecTimeToAct>lcu16DecTimeToActCnt))
        {
            lcu8DecValveMode=U8_DEC_HOLD;
            lcu8DecMotorMode=0;
            lcu16DecTimeToActCnt++;
            lcu16DecObserver=51;
        }
        else
        {
            if(lcs16DecCtrlInput>0)
            {
                lcu16DecTimeToActCnt=1;
                lcu8DecValveMode=U8_DEC_DUMP;
                  #if __TCMF_CONTROL
                lcu8DecMotorMode=0;
                  #else
                lcu8DecMotorMode=1;
                  #endif
                lcu16DecObserver=52;
            }
            else if(lcs16DecCtrlInput<0)
            {
                lcu16DecTimeToActCnt=1;
                lcu8DecValveMode=U8_DEC_APPLY;
                lcu8DecMotorMode=1;
                lcu16DecObserver=53;
            }
            else
            {
                lcu16DecTimeToActCnt++;
                lcu8DecValveMode=U8_DEC_HOLD;
                lcu8DecMotorMode=0;
                lcu16DecObserver=54;
            }
        }
    }
}


static void LCDEC_vCalFeedBackCtrlGain(void)
{
    if (lcs16DecTargetG <= 0)
    {

        if(lcs16DecCtrlError>0)
        {
            if(lcs16DecCtrlErrorDot>0)
            {
                lcs16DecCtrlPGain=S16_DEC_PE_PED_PGAIN;
                lcs16DecCtrlDGain=S16_DEC_PE_PED_DGAIN;
                lcs16DecCtrlIGain=S16_DEC_PE_PED_IGAIN;
            }
            else if(lcs16DecCtrlErrorDot<0)
            {
                lcs16DecCtrlPGain=S16_DEC_PE_MED_PGAIN;
                lcs16DecCtrlDGain=S16_DEC_PE_MED_DGAIN;
                lcs16DecCtrlIGain=S16_DEC_PE_MED_IGAIN;
            }
            else
            {
            	if ((lcs16DecCtrlPGain==0)&&(lcs16DecCtrlDGain==0)&&(lcs16DecCtrlIGain==0))
            	{
	                lcs16DecCtrlPGain=(S16_DEC_PE_PED_PGAIN+S16_DEC_PE_MED_PGAIN)/2;
    	            lcs16DecCtrlDGain=(S16_DEC_PE_PED_DGAIN+S16_DEC_PE_MED_DGAIN)/2;
        	        lcs16DecCtrlIGain=(S16_DEC_PE_PED_IGAIN+S16_DEC_PE_MED_IGAIN)/2;

            	}
            	else
            	{
					lcs16DecCtrlPGain=lcs16DecCtrlPGain;
	    	        lcs16DecCtrlDGain=lcs16DecCtrlDGain;
    	    	    lcs16DecCtrlIGain=lcs16DecCtrlIGain;
    	    	}
            }
        }
        else if(lcs16DecCtrlError<0)
        {
            if(lcs16DecCtrlErrorDot>0)
            {
                lcs16DecCtrlPGain=S16_DEC_ME_PED_PGAIN;
                lcs16DecCtrlDGain=S16_DEC_ME_PED_DGAIN;
                lcs16DecCtrlIGain=S16_DEC_ME_PED_IGAIN;
            }
            else if(lcs16DecCtrlErrorDot<0)
            {
                lcs16DecCtrlPGain=S16_DEC_ME_MED_PGAIN;
                lcs16DecCtrlDGain=S16_DEC_ME_MED_DGAIN;
                lcs16DecCtrlIGain=S16_DEC_ME_MED_IGAIN;
            }
            else
            {
            	if ((lcs16DecCtrlPGain==0)&&(lcs16DecCtrlDGain==0)&&(lcs16DecCtrlIGain==0))
            	{
	                lcs16DecCtrlPGain=(S16_DEC_ME_PED_PGAIN+S16_DEC_ME_MED_PGAIN)/2;
    	            lcs16DecCtrlDGain=(S16_DEC_ME_PED_DGAIN+S16_DEC_ME_MED_DGAIN)/2;
        	        lcs16DecCtrlIGain=(S16_DEC_ME_PED_IGAIN+S16_DEC_ME_MED_IGAIN)/2;
            	}
            	else
            	{
					lcs16DecCtrlPGain=lcs16DecCtrlPGain;
	    	        lcs16DecCtrlDGain=lcs16DecCtrlDGain;
    	    	    lcs16DecCtrlIGain=lcs16DecCtrlIGain;
    	    	}
            }
        }
        else
        {
            lcs16DecCtrlPGain=lcs16DecCtrlPGain;
            lcs16DecCtrlDGain=lcs16DecCtrlDGain;
            lcs16DecCtrlIGain=lcs16DecCtrlIGain;
        }
    }
    else
    {
        if(lcs16DecCtrlError>0)
        {
            if(lcs16DecCtrlErrorDot>0)
            {
                lcs16DecCtrlPGain=S16_DEC_PE_PED_PGAIN_ACC;
                lcs16DecCtrlDGain=S16_DEC_PE_PED_DGAIN_ACC;
                lcs16DecCtrlIGain=S16_DEC_PE_PED_IGAIN_ACC;
            }
            else if(lcs16DecCtrlErrorDot<0)
            {
                lcs16DecCtrlPGain=S16_DEC_PE_MED_PGAIN_ACC;
                lcs16DecCtrlDGain=S16_DEC_PE_MED_DGAIN_ACC;
                lcs16DecCtrlIGain=S16_DEC_PE_MED_IGAIN_ACC;
            }
            else
            {
            	if ((lcs16DecCtrlPGain==0)&&(lcs16DecCtrlDGain==0)&&(lcs16DecCtrlIGain==0))
            	{
	                lcs16DecCtrlPGain=(S16_DEC_PE_PED_PGAIN_ACC+S16_DEC_PE_MED_PGAIN_ACC)/2;
    	            lcs16DecCtrlDGain=(S16_DEC_PE_PED_DGAIN_ACC+S16_DEC_PE_MED_DGAIN_ACC)/2;
        	        lcs16DecCtrlIGain=(S16_DEC_PE_PED_IGAIN_ACC+S16_DEC_PE_MED_IGAIN_ACC)/2;
            	}
            	else
            	{
					lcs16DecCtrlPGain=lcs16DecCtrlPGain;
	    	        lcs16DecCtrlDGain=lcs16DecCtrlDGain;
    	    	    lcs16DecCtrlIGain=lcs16DecCtrlIGain;
    	    	}
            }
        }
        else if(lcs16DecCtrlError<0)
        {
            if(lcs16DecCtrlErrorDot>0)
            {
                lcs16DecCtrlPGain=S16_DEC_ME_PED_PGAIN_ACC;
                lcs16DecCtrlDGain=S16_DEC_ME_PED_DGAIN_ACC;
                lcs16DecCtrlIGain=S16_DEC_ME_PED_IGAIN_ACC;
            }
            else if(lcs16DecCtrlErrorDot<0)
            {
                lcs16DecCtrlPGain=S16_DEC_ME_MED_PGAIN_ACC;
                lcs16DecCtrlDGain=S16_DEC_ME_MED_DGAIN_ACC;
                lcs16DecCtrlIGain=S16_DEC_ME_MED_IGAIN_ACC;
            }
            else
            {
            	if ((lcs16DecCtrlPGain==0)&&(lcs16DecCtrlDGain==0)&&(lcs16DecCtrlIGain==0))
            	{
	                lcs16DecCtrlPGain=(S16_DEC_ME_PED_PGAIN_ACC+S16_DEC_ME_MED_PGAIN_ACC)/2;
    	            lcs16DecCtrlDGain=(S16_DEC_ME_PED_DGAIN_ACC+S16_DEC_ME_MED_DGAIN_ACC)/2;
        	        lcs16DecCtrlIGain=(S16_DEC_ME_PED_IGAIN_ACC+S16_DEC_ME_MED_IGAIN_ACC)/2;
            	}
            	else
            	{
					lcs16DecCtrlPGain=lcs16DecCtrlPGain;
	    	        lcs16DecCtrlDGain=lcs16DecCtrlDGain;
    	    	    lcs16DecCtrlIGain=lcs16DecCtrlIGain;
    	    	}
            }
        }
        else
        {
	    	if ((lcs16DecCtrlPGain==0)&&(lcs16DecCtrlDGain==0)&&(lcs16DecCtrlIGain==0))
	    	{
	    	    lcs16DecCtrlPGain=(S16_DEC_ME_PED_PGAIN_ACC+S16_DEC_ME_MED_PGAIN_ACC)/2;
			    lcs16DecCtrlDGain=(S16_DEC_ME_PED_DGAIN_ACC+S16_DEC_ME_MED_DGAIN_ACC)/2;
	    	    lcs16DecCtrlIGain=(S16_DEC_ME_PED_IGAIN_ACC+S16_DEC_ME_MED_IGAIN_ACC)/2;
	    	}
	    	else
	    	{
				lcs16DecCtrlPGain=lcs16DecCtrlPGain;
	    	    lcs16DecCtrlDGain=lcs16DecCtrlDGain;
			    lcs16DecCtrlIGain=lcs16DecCtrlIGain;
			}
        }
    }

    if (lcs16DecCtrlPGain==0)
    {
        lcs16_dec_tempW9 = 100;
        lcs16_dec_tempW9 = (lcs16_dec_tempW9 > S16_DEC_ME_MED_PGAIN)? S16_DEC_ME_MED_PGAIN: lcs16_dec_tempW9;
        lcs16_dec_tempW9 = (lcs16_dec_tempW9 > S16_DEC_ME_PED_PGAIN)? S16_DEC_ME_PED_PGAIN: lcs16_dec_tempW9;
        lcs16_dec_tempW9 = (lcs16_dec_tempW9 > S16_DEC_PE_MED_PGAIN)? S16_DEC_PE_MED_PGAIN: lcs16_dec_tempW9;
        lcs16_dec_tempW9 = (lcs16_dec_tempW9 > S16_DEC_PE_PED_PGAIN)? S16_DEC_PE_PED_PGAIN: lcs16_dec_tempW9;

        if (lcs16_dec_tempW9 >=1)
        {
            lcs16DecCtrlPGain = lcs16_dec_tempW9;
        }
        else
        {
            lcs16DecCtrlPGain =1;
        }
    }
    else { }

    if (lcs16DecRoughCnt >= L_U8_TIME_10MSLOOP_100MS)
    {
        lcs16DecCtrlRoughPGain = 11;
        lcs16DecCtrlRoughDGain =  9;
        lcs16DecCtrlRoughIGain = 10;
    }
    else
    {
        lcs16DecCtrlRoughPGain = 10;
        lcs16DecCtrlRoughDGain = 10;
        lcs16DecCtrlRoughIGain = 10;
    }
}


static void LCDEC_vCalFeedBackCtrlInput(void)
{
    lcs16DecCtrlInput=(int16_t)(((int32_t)lcs16DecCtrlPGain*lcs16DecCtrlError*lcs16DecCtrlRoughPGain/10)+
                            ((int32_t)lcs16DecCtrlDGain*lcs16DecCtrlErrorDot*lcs16DecCtrlRoughDGain/10)+
                            ((int32_t)lcs16DecCtrlIGain*lcs16DecCtrlErrorInt*lcs16DecCtrlRoughIGain/10));


    if (lcs16DecCtrlInput==0)
    {
        lcu16DecTimeToAct=U16_DEC_CTRL_PERIOD_MAX;
    }
    else if ((vref<VREF_5_KPH)
      #if __HDC
        &&(lcu1HdcActiveFlg==0)
      #endif
      #if __DEC
        &&(lcu1DecActiveFlg==0)
      #endif
        )
    {
        lcu16DecTimeToAct=U16_DEC_CTRL_PERIOD_MAX;
    }
    else if ((YAW_CDC_WORK==1)
        ||((ABS_fl==1)&&(ABS_fr==1)&&(ABS_rl==1)&&(ABS_rr==1)))
    {
        tempUW2=1;
        tempUW1=U16_DEC_CTRL_PERIOD_MAX_A_ABS;
        tempUW0=(uint16_t)(abs(lcs16DecCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=U16_DEC_CTRL_PERIOD_MIN_A_ABS)
        {
            lcu16DecTimeToAct=U16_DEC_CTRL_PERIOD_MIN_A_ABS;
        }
        else
        {
            lcu16DecTimeToAct=tempUW3;
        }
    }
    else if ((ABS_fl==1)||(ABS_fr==1)||(ABS_rl==1)||(ABS_rr==1))
    {
        tempUW2=1;
        tempUW1=U16_DEC_CTRL_PERIOD_MAX_P_ABS;
        tempUW0=(uint16_t)(abs(lcs16DecCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=U16_DEC_CTRL_PERIOD_MIN_P_ABS)
        {
            lcu16DecTimeToAct=U16_DEC_CTRL_PERIOD_MIN_P_ABS;
        }
        else
        {
            lcu16DecTimeToAct=tempUW3;
        }
    }
    else
    {
        tempUW2=1;
        tempUW1=U16_DEC_CTRL_PERIOD_MAX;
        tempUW0=(uint16_t)(abs(lcs16DecCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=U16_DEC_CTRL_PERIOD_MIN)
        {
            lcu16DecTimeToAct=U16_DEC_CTRL_PERIOD_MIN;
        }
        else
        {
            lcu16DecTimeToAct=tempUW3;
        }
    }
}
#endif


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCDECCallControl
	#include "Mdyn_autosar.h"
#endif