/* Includes ********************************************************/
#include "LVarHead.h"


/* Local Definition  ***********************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDCDCCallDetection
	#include "Mdyn_autosar.h"
#endif

/* Variables Definition*********************************************/
extern uint8_t CDC_STATUS;
extern uint8_t CDC_LOG_DATA3[8];

/* Local Function prototype ****************************************/

#if defined(__UCC_1)

	void LDCDC_vCallDetection(void);
	void LDCDC_vDetectBumpPotHole(void);
	void CALC_PITCH_MOTION(void);
	void Integrator(void);
	void Calcu_Four_BodyvAcc(void);
	void DECISION_NORMAL_LOAD(void);
	
  #if __UCC_COOPERATION_CONTROL
    void LCCDC_vSetABSCyclePotHole(struct W_STRUCT *WL);
    void LCCDC_vLimitReapplyTimePotHole(void);
  #endif

#endif

/* Implementation***************************************************/

#if defined(__UCC_1)

void LDCDC_vCallDetection(void)
{
    LDCDC_vDetectBumpPotHole(); 
    CALC_PITCH_MOTION();
    DECISION_NORMAL_LOAD();   	

}
/******************************************************************/
void LDCDC_vDetectBumpPotHole(void)
{
    
    if((Rough_road_detect_vehicle==0)&&(vref>VREF_10_KPH)&&(ESP_ERROR_FLG==0)&&(CDC_STATUS==2)){
    
    //-------------------------------------------------------------------------------------
    //         Detect Bump
    //------------------------------------------------------------------------------------- 
        if(cdc_bz_fl>=400) FL.UCC_DETECT_BUMP_flag=1; // +body Fz_fl>=0.3g
        if(cdc_bz_fr>=400) FR.UCC_DETECT_BUMP_flag=1; // +body Fz_fr>=0.3g
    
        if((FL.UCC_DETECT_BUMP_flag==1)&&(FR.UCC_DETECT_BUMP_flag==1)) UCC_DETECT_BUMP_Vehicle_flag=1;
        else UCC_DETECT_BUMP_Vehicle_flag=0;
        
        if(FL.UCC_DETECT_BUMP_flag==1){
            if(cdc_bump_counter_fl<300) cdc_bump_counter_fl++;
            else{
                cdc_bump_counter_fl=0;
                FL.UCC_DETECT_BUMP_flag=0;
            }
        }
        else cdc_bump_counter_fl=0;
        
        if(FR.UCC_DETECT_BUMP_flag==1){
            if(cdc_bump_counter_fr<300) cdc_bump_counter_fr++;
            else{
                cdc_bump_counter_fr=0;
                FR.UCC_DETECT_BUMP_flag=0;
            }
        }
        else cdc_bump_counter_fr=0;
        
    //--------------------------------------------------------------------------------------
    //           Detect Pot Hole
    //--------------------------------------------------------------------------------------     
        
        if((cdc_bz_fl<=-1000)&&(FL.UCC_DETECT_BUMP_flag==0)) FL.UCC_DETECT_PotHole_flag=1;
        if((cdc_bz_fr<=-1000)&&(FR.UCC_DETECT_BUMP_flag==0)) FR.UCC_DETECT_PotHole_flag=1;
        
        if(FL.UCC_DETECT_PotHole_flag==1){
            if(cdc_PotHole_counter_fl<300) cdc_PotHole_counter_fl++;
            else{
                cdc_PotHole_counter_fl=0;
                FL.UCC_DETECT_PotHole_flag=0;
            }
        }
        else cdc_PotHole_counter_fl=0;
        
        if(FR.UCC_DETECT_PotHole_flag==1){
            if(cdc_PotHole_counter_fr<300) cdc_PotHole_counter_fr++;
            else{
                cdc_PotHole_counter_fr=0;
                FR.UCC_DETECT_PotHole_flag=0;
            }
        }
        else cdc_PotHole_counter_fr=0;
    }
    else{
        UCC_DETECT_BUMP_Vehicle_flag=0;
        FL.UCC_DETECT_BUMP_flag=0;
        FR.UCC_DETECT_BUMP_flag=0;
        FL.UCC_DETECT_PotHole_flag=0;
        FR.UCC_DETECT_PotHole_flag=0;
    }
}

#if __UCC_COOPERATION_CONTROL
void LCCDC_vLimitReapplyTimePotHole(void) 
{
    if((ABS_wl==1)&&(WL->UCC_DETECT_PotHole_flag==1)) {
        if(WL->LEFT_WHEEL==1) {
            if((ABS_fr==0)&&(FR.UCC_DETECT_PotHole_flag==0)) {
                if(WL->reapply_tim > (uint8_t)T_300_MS) {
                    WL->reapply_tim=(uint8_t)T_300_MS;
                }
            }
        }
        else {
            if((ABS_fl==0)&&(FL.UCC_DETECT_PotHole_flag==0)) {
                if(WL->reapply_tim > (uint8_t)T_300_MS) {
                    WL->reapply_tim=(uint8_t)T_300_MS;
                }
            }
        }
    }
}
    
void LCCDC_vSetABSCyclePotHole(struct W_STRUCT *WL)
{    
    if((ABS_wl==1) && (WL->UCC_DETECT_PotHole_flag==1)) {
        if(WL->state==DETECTION) {
        	if(WL->ABS_PotHole_Stable_flg2==0) {
        		WL->ABS_PotHole_Stable_flg1=1;
				WL->ABS_PotHole_Stable_flg2=1;
			}
		}
		else if(WL->state==UNSTABLE) {
			WL->ABS_PotHole_Stable_flg1=0;
		}
	}
	else {
        WL->ABS_PotHole_Stable_flg1=0;
   	    WL->ABS_PotHole_Stable_flg2=0;
	}	
}
#endif

/******************************************************************/
void CALC_PITCH_MOTION(void)
{
/*      
    unsigned int8_t i;
    if(CDC_STATUS == 2) {
        
        lrswBodyvAccel[0] = cdc_bz_fl_HF;
        lrswBodyvAccel[1] = cdc_bz_fr_HF;
        lrswBodyvAccel[2] = cdc_bz_rr_HF;
        
        Calcu_Four_BodyvAcc();  // Four body corner
        
        for(i=0; i<4; i++)
        {
            lrswCorrectBodyvAccel_1[i] = lrswCorrectBodyvAccelU[i];
            lrswCorrectBodyvAccelHF_1[i] = lrswCorrectBodyvAccelHF[i];
            lrswCorrectBodyvAccelU[i] = lrswCorrectBodyvAccel[i];
            lrswCorrectBodyvAccelHF[i] = HPF_0_1Hz(lrswCorrectBodyvAccelU[i], lrswCorrectBodyvAccel_1[i], lrswCorrectBodyvAccelHF_1[i]);    //HPF fc=0.1Hz
                        
            //lrswBodyvVel[i] = ((508*lrswBodyvVel[i])+(5*lrswCorrectBodyvAccelHF[i]))/512; //Integrator fc=0.1Hz
            luswUk = lrswCorrectBodyvAccelHF[i];
            luswYk1 = lrswBodyvVel[i];
            Integrator();
            lrswBodyvVel[i]=luswYk;
            
            lrswBodyvVelU_1[i] = lrswBodyvVelU[i];
            lrswBodyvVelHF_1[i] = lrswBodyvVelHF[i];
            lrswBodyvVelU[i] = lrswBodyvVel[i];
            lrswBodyvVelHF[i] = HPF_0_1Hz(lrswBodyvVelU[i], lrswBodyvVelU_1[i], lrswBodyvVelHF_1[i]);   //HPF fc=0.1Hz
        }
        
        lrswModalVelPitch = (lrswBodyvVelHF[0] - lrswBodyvVelHF[2])+(lrswBodyvVelHF[1] - lrswBodyvVelHF[3]);
    }   
*/      
    lrswModalVelPitch = (signed int16_t) (CDC_LOG_DATA3[0] + (CDC_LOG_DATA3[1]<<8));
}
/******************************************************************/

void Integrator(void)
{                                                   //fc=1 (0.1 Hz)
    luslTmp[0]  = (signed int32_t) 490*luswYk1;        //= 1/(1+2*3.141592*fc) * (2^9) = 490.4
    luslTmp[1]  = (signed int32_t) (34*luswUk/10);         //= 0.007/(1+2*3.141592*fc) * (2^9) = 3.43
    luslTmp[2]  = (luslTmp[0]+luslTmp[1]);  
    luswYk  = (signed int16_t)(luslTmp[2]>>9);
}
/******************************************************************/

void Calcu_Four_BodyvAcc(void)
{
    unsigned int8_t i,j;
    signed int32_t tempSL0;
    for(i=0; i<4; i++)                                                                          // Four body corner
    {
        tempSL0 = 0L;
        for(j=0; j<3; j++)
        {
            tempSL0 += ((signed int32_t) lrscCoefCorrectAccel[j][i]) * ((signed int32_t) lrswBodyvAccel[j]);    // 가속도 센서 보정계수 * 가속도 센서 
        }
        lrswCorrectBodyvAccel[i] = (signed int16_t) (tempSL0/10);                                             // 차체 수직 가속도를 총합/64
    }
}
/******************************************************************/

void DECISION_NORMAL_LOAD(void)
{
    if(lducFlagDive != 0)
    {
        if(lrswModalVelPitch < -160) {           //DIVE
            vfiltn_normal_load_front = 4;   //0.5kph//logic.c
            vfiltn_normal_load_rear = -4;
            LFC_gain_normal_load_front = 1; //ventil.c
            LFC_gain_normal_load_rear = -1;
        }
        else if(lrswModalVelPitch > 160) {
            vfiltn_normal_load_front = -4;
            vfiltn_normal_load_rear = 4;
            LFC_gain_normal_load_front = -1;
            LFC_gain_normal_load_rear = 1;
        }
        else {
            vfiltn_normal_load_front = 0;
            vfiltn_normal_load_rear = 0;
            LFC_gain_normal_load_front = 0;
            LFC_gain_normal_load_rear = 0;
        }
    }      
    else {
        vfiltn_normal_load_front = 0;
        vfiltn_normal_load_rear = 0;
        LFC_gain_normal_load_front = 0;
        LFC_gain_normal_load_rear = 0;
    }

}
/******************************************************************/

#endif		
	
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDCDCCallDetection
	#include "Mdyn_autosar.h"
#endif
