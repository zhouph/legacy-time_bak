#ifndef __LDBDWCALLDETECTION_H__
#define __LDBDWCALLDETECTION_H__

/*includes********************************************************************/

#include "LVarHead.h"
#include "esp_var.h"
/*Global Type Declaration ****************************************************/
/* BDWFLAG00*/
extern U8_BIT_STRUCT_t BDWD0, BDWD1;

#define DETECT_RAIN                         BDWD0.bit0
#define BDW_ACT_CONDTION                    BDWD0.bit1
#define DISC_CLEAN_ACT_CONDTION             BDWD0.bit2
#define DISC_CLEAN_NEED                     BDWD0.bit3
#define DISC_CLEAN_NEED_TEMP                BDWD0.bit4
#define BDW_INIHIBIT                        BDWD0.bit5
#define BDW_flag_00_6                       BDWD0.bit6
#define BDW_flag_00_7                       BDWD0.bit7

 
#define lsbdwu1WiperIntSW                   BDWD1.bit0
#define lsbdwu1WiperLow                     BDWD1.bit1
#define lsbdwu1WiperHigh                    BDWD1.bit2
#define lsbdwu1WiperAuto                    BDWD1.bit3
#define lsbdwu1Clu2WiperMsgRxOkFlg          BDWD1.bit4
#define BDWD_flag_01_5                      BDWD1.bit5
#define BDWD_flag_01_6                      BDWD1.bit6
#define BDWD_flag_01_7                      BDWD1.bit7

#if __BDW

extern int16_t bdw_contorl_priod,bdw_active_time;
extern uint8_t lsbdwu8RainSnsStat;
 
/*Global Extern Variable  Declaration*****************************************/





/*Global Extern Functions  Declaration****************************************/

  #endif    /*__BDW~ */
#endif

