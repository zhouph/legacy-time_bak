#ifndef LDEDCCALLDETECTION_H
#define LDEDCCALLDETECTION_H

/*includes**************************************************/
#include "LVarHead.h"


  #if __EDC
/*Global MACRO CONSTANT Definition******************************************************/
/*
#define U8_VEL_GAP_MOVING_WINDOW_LENGTH 14
#define S8_EDC_ENT_THR_REL_LAM_AT_B_DCT  -LAM_7P
#define S8_EDC_ENT_THR_REL_LAM           -LAM_15P
#define U8_EDC_ENT_THR_DEL_YAW			 YAW_1DEG
*/



/*Global MACRO FUNCTION Definition******************************************************/
	#if __4WD
#define __EDC_COMPENSATION_IN_TURN 1
	#else	
#define __EDC_COMPENSATION_IN_TURN 1
	#endif

#define __EDC_GSD_IMPROVE          ENABLE

#define S16_EDC_EXIT_TQ_MIN        50 /* 5% */
#define S16_EDC_EXIT_ACCEL_SUM     (AFZ_1G0*5)
#define S16_EDC_TQ_DOWN_ACCEL_SUM  AFZ_1G0        
/*
#define U8_EDC_CONTROL_TIME_MAX	   30	
*/
/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/


/*Global Extern Functions  Declaration******************************************************/
extern void LDEDC_vCallDetection(void);
extern void LDEDC_vDetectNorEdc(void); 
extern void LDEDC_vCount1SecAfterEDC(void);	
extern void LDEDC_vInitializeEdcDetection(void);
extern void LDEDC_vParkingBrakeOperation(void);
extern void LDEDC_vEnableMiniWheelDetect(void);
extern void LDEDC_vDetectHighMu(void);
extern void LDEDC_vDetectGearShiftDown(void);
extern void LDEDC_vDetectExcessiveTorqUp(void);

  #endif /* __EDC */
#endif