#ifndef __LSFBCCALLSENSORSIGNAL_H__
#define __LSFBCCALLSENSORSIGNAL_H__
/*Includes *********************************************************************/
#include "LVarHead.h"




/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern uint8_t mp_hw_err_cnt;
extern uint8_t mcp_unstable_cnt;

/*Global Extern Functions Declaration ******************************************/
extern void	LSESP_vCallSensorSignal(void);

#endif
