#ifndef	__LDABSVREF5COMP_H__
#define	__LDABSVREF5COMP_H__

/*includes********************************************************************/
#include	"LVarHead.h"

#if __VDC 

/*Global Type Declaration ****************************************************/

extern	U8_BIT_STRUCT_t VESPF0;

#define	VREF5_CHANGE				VESPF0.bit7
#define	ESP_SENSOR_RELIABLE_FLG		VESPF0.bit6
#define	YAW_CORRECTION				VESPF0.bit5
#define	YAW_CORRECTION_MODE_CHANGE	VESPF0.bit4
#define	VESPF0_unused_4				VESPF0.bit3
#define	VESPF0_unused_5				VESPF0.bit2
#define	VESPF0_unused_6				VESPF0.bit1
#define	VESPF0_unused_7				VESPF0.bit0

/*Global Extern	Variable  Declaration*****************************************/


/*	extern int16_t	vref5; */
/*	extern int16_t	vdc_vref; */
	extern int16_t	yaw_c_vref;

	extern int16_t	vref5_resol_change;
	extern int16_t	yaw_c_vref_resol_change;
	extern int16_t	vdc_vref_resol_change;
	
	extern int16_t	vref5_diff;



	
/*Global Extern	Functions  Declaration****************************************/
extern void	LDABS_vCallVrefCompForESP(void);
#endif
#endif
