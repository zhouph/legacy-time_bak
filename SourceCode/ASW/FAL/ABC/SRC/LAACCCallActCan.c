/* Includes ********************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAACCCallActCan
	#include "Mdyn_autosar.h"               
#endif


#include "LAACCCallActCan.h"
  #if __ACC
#include "LCACCCallControl.h"
  #endif
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/

/* Local Function prototype ****************************************/
  #if __ACC
   #if defined(__ACC_RealDevice_INTERFACE)
static void LAACC_vCtrlEngTorque(void);
   #endif
  #endif

/* Implementation***************************************************/
  #if __ACC
void LAACC_vCallActCan(void)
{
	  #if defined(__ACC_RealDevice_INTERFACE)
	LAACC_vCtrlEngTorque();
	  #endif
}

   #if defined(__ACC_RealDevice_INTERFACE)
void LAACC_vCtrlEngTorque(void)
{
	uint16_t	lcu16AccTorqCmd;
	
	  #if __CAR==HMC_NF
	if(((TCS1[0]&0x01)==0)&&(EDC_ON == 0))				/* TCS_REQ and EDC_REQ disable */
	{
		if(lcu8AccEngCtrlReq==1)
		{
	   		TCS1[0]|=0x02;    						/* set MSR_C_REQ */
            	lcu16AccTorqCmd=lcu16AccCalTorq;
    	    		TCS1[4]=(uint8_t)(lcu16AccTorqCmd/4); 		/* set TQI_MSR */
    	    		TCS1[3]=~TCS1[4];						/* 1's complement of TQI_MSR */
            	TCS1[0]&=0xF6;                  			/* reset TCS_REQ, TCS_GSC */
		}
		else
		{																								/* No EMS Control */
		  lcu16AccTorqCmd=lcu16AccCalTorq=drive_torq;	
            TCS1[0]&=0xF6;							/* reset TCS_REQ, TCS_GSC */
            TCS1[0]&=0xFD;							/* reset MSR_C_REQ */
            TCS1[1]&=0xFD;							/* reset TCS_CTL */
            if(BTC_fz==1)
            {
            	TCS1[1]|=0x02;							/* set TCS_CTL */
            }
            else
            {
            	;
            }
            TCS1[3]=0xff;							/* clear TQI_TCS */
            TCS1[4]=0;								/* clear TQI_MSR */
 		}
 	}
 	else
 	{
 		;
 	}
	  #endif
}
   #endif
  #endif
 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAACCCallActCan
	#include "Mdyn_autosar.h"               
#endif  
