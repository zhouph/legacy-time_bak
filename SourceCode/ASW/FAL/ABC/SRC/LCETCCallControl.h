#ifndef LCETCCALLCONTROL_H
#define LCETCCALLCONTROL_H

/*includes**************************************************/

#include "LVarHead.h"
/*Global MACRO CONSTANT Definition******************************************************/


/*Global MACRO FUNCTION Definition******************************************************/


/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/


/*Global Extern Functions  Declaration******************************************************/
	#if !SIM_MATLAB
extern void LATCS_vConvertEMS2TCS(void);                	     /* 엔진ECU 데이타 수신 */
	#endif
extern void LCTCS_vLoadParameter(void);
extern void LCTCS_vCalTractionWheelSpeed(void);
extern void LCTCS_vDetectSplitMu(void);
extern void LCTCS_vCalFilteredWheelSpeed(void);
extern void LCTCS_vCalculateDV(void);
extern void LCTCS_vCalculateACC(void);
extern void LCTCS_vEstmateTMGear(void);          /* 변속기어비 추정 */
extern void LCTCS_vEstimateTargetTorque(void);
extern void LCTCS_vCallDetectTraction(void);
extern void LCTCS_vCallDetectTrace(void);
extern void LCTCS_vControlTraction(void);
extern void LCTCS_vControlTrace(void);
extern void LCTCS_vControlCommand(void);
	#if !SIM_MATLAB
extern void LATCS_vControlFuncLamp(void);
extern void LATCS_vConvertTCS2EMS(void);
	#endif
extern	int16_t INTER_2POINT( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2 );

#endif
