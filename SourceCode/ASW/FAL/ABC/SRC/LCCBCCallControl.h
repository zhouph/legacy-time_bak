#ifndef __LCCBCCALLCONTROL_H__
#define __LCCBCCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __CBC
/*Global Type Declaration ****************************************************/

#if __VDC
  #define     __FRONT_CBC_ENABLE            ENABLE   /* selective */
  #define	__CBC_WHEEL_SLIP_CONDITION		ENABLE
  #define	__CBC_TARGET_P_COMP_BY_DEL_YAW	ENABLE
#else
  #define     __FRONT_CBC_ENABLE            DISABLE   /* default */
  #define	__CBC_WHEEL_SLIP_CONDITION		DISABLE
  #define	__CBC_TARGET_P_COMP_BY_DEL_YAW	DISABLE
#endif  


#if __4WD && (__4WD_VARIANT_CODE==DISABLE)
  #if ((__CAR_MAKER==HMC_KMC)||(__CAR==GM_LAMBDA)) /* for 4WD cbc compile temp */
    #define     __ABS_CBC_DISABLE         DISABLE   /* selective */
  #else
    #define     __ABS_CBC_DISABLE         ENABLE   /* default */
  #endif
#else
  #define     	__ABS_CBC_DISABLE         DISABLE   /* selective */
#endif

  #if ((__CAR_MAKER==HMC_KMC)||(__CAR==GM_LAMBDA))
#define       __4WD_ABS_CBC_ENABLE        ENABLE
  #else
#define       __4WD_ABS_CBC_ENABLE        DISABLE
  #endif

/* for CBC Debug */
/* #define __CBC_DEBUG */

/* CBC Mode */
#define U8_ESC_CBC                      1
#define U8_ABS_CBC                      2
#define U8_CBC_INHIBIT                  0

#define __CBC_FADE_OUT_OFF_MODE		    ENABLE  /* for FadeOut Off Mode */
#define __CBC_FADE_OUT_LFC_RISE			DISABLE

/* Press Ctrl Mode */                                                                         
#define U8_CBC_PRESS_RESET              0                                
#define U8_CBC_PRESS_DUMP               1                                
#define U8_CBC_PRESS_RISE               2                                
#define U8_CBC_PRESS_HOLD               3                                
#define U8_CBC_PRESS_FADEOUT            4
/* Fade Out Mode */
#define CBC_FADEOUT_OFF		         	0
#define CBC_FADEOUT_PULSEUP          	1
#define CBC_FADEOUT_FULLRISE         	2
#define CBC_FADEOUT_LFCRISE          	3

#define U8_CBC_Limit_Cnt_Pulseup		L_U8_CNT_10 /* need to edit */
#define U8_CBC_Limit_Time_Full_rise		L_U8_TIME_10MSLOOP_200MS /* need to edit */
#define	U8_CBC_Limit_LFC_Duty			30 /* need to edit */

#define S16_CBC_MIN_ALAT_ENTER_TH		LAT_0G2G /* limit Ay Enter threshold for CBC*/
#define S16_CBC_MIN_ALAT_ENTER_TH_ABS	LAT_0G5G /* limit Ay Enter threshold for ABS_CBC*/
#define S16_CBC_MIN_ALAT_EXIT_TH		LAT_0G1G /* limit Ay Exit threshold for CBC*/    
#define S16_CBC_MIN_ALAT_EXIT_TH_ABS	LAT_0G1G /* limit Ay Exit threshold for ABS_CBC*/

#define U8_CBC_COMP_TH_MIN_CNT			L_U8_TIME_10MSLOOP_500MS  /* RE-CBC Detection Cnt for MAX Comp Thres*/
#define U8_CBC_COMP_TH_MAX_CNT			L_U8_TIME_10MSLOOP_1000MS /* RE-CBC Detection Cnt for Min Comp Thres*/
#define U8_CBC_RECBC_CNT_MAX_TIME		L_U8_TIME_10MSLOOP_1000MS

#define S16_CBC_VREF_ENTER_TH			VREF_40_KPH	/* limit vref Enter threshold */
#define S16_CBC_VREF_EXIT_TH			VREF_30_KPH /* limit vref Exit  threshold */

#define U8_CBC_MIN_TARGET_P				MPRESS_3BAR /* Target Pressure Lower limit */
#define S16_CBC_DELTA_PRES_MAX_RISE_F   MPRESS_10BAR
#define S16_CBC_DELTA_PRES_MAX_DUMP_F   MPRESS_8BAR
  /*------------------------------------------------------------------------------*/
  /*           vref - vref_alt                                                    */
  /*    Ax =  -------------------- * 1000  = (vref - vref_alt) * 708  (g*1000)    */
  /*           8*3.6*9.81*0.005                                                   */
  /*------------------------------------------------------------------------------*/
#define S16_CBC_DV_AX_CONV_RATIO    354 /* MGH-60(7ms) : 500, MGH-80(5ms) : 708, MGH-80(10ms) : 354 */

/**********************need to delete for BKT *****************************/
#define S16_CBC_THRES_LOW_SPEED			VREF_60_KPH
#define S16_CBC_THRES_MED_SPEED         VREF_90_KPH
#define S16_CBC_THRES_HIGH_SPEED        VREF_120_KPH
#define U8_CBC_MAX_HOLD_TIMER           L_U8_TIME_10MSLOOP_40MS

	#if (__CAR==GM_GSUV)
#define U8_CBC_DUMP_DEAD_PRESS			MPRESS_8BAR
#define U8_CBC_RISE_DEAD_PRESS			MPRESS_2BAR
#define U8_CBC_DEAD_PRESS_FRONT			MPRESS_5BAR
#define U8_CBC_DUMP_DEAD_PRESS_ABS		MPRESS_2BAR
#define U8_CBC_RISE_DEAD_PRESS_ABS		MPRESS_2BAR
	#else
#define U8_CBC_DUMP_DEAD_PRESS			MPRESS_2BAR
#define U8_CBC_RISE_DEAD_PRESS			MPRESS_2BAR
#define U8_CBC_DEAD_PRESS_FRONT			MPRESS_2BAR
#define U8_CBC_DUMP_DEAD_PRESS_ABS		MPRESS_2BAR
#define U8_CBC_RISE_DEAD_PRESS_ABS		MPRESS_2BAR
	#endif

/**********************need to delete for BKT *****************************/

#define DETECT_LIMIT_CORNER                 CBCC0.bit0
#define DETECT_PARTIAL_BRAKING              CBCC0.bit1
#define CBC_ENABLE_old      	            CBCC0.bit2
#define CBC_ENABLE                          CBCC0.bit3
#define CBC_ON                              CBCC0.bit4
#define CBC_not_used_bit_0_5                CBCC0.bit5
#define CBC_not_used_bit_0_6                CBCC0.bit6
#define CBC_not_used_bit_0_7                CBCC0.bit7

#define CBC_not_used_bit_1_0                CBCC1.bit0
#define CBC_not_used_bit_1_1                CBCC1.bit1
#define CBC_not_used_bit_1_2                CBCC1.bit2
#define CBC_not_used_bit_1_3                CBCC1.bit3
#define CBC_not_used_bit_1_4                CBCC1.bit4
#define CBC_not_used_bit_1_5                CBCC1.bit5
#define CBC_not_used_bit_1_6                CBCC1.bit6
#define CBC_not_used_bit_1_7                CBCC1.bit7

#define CBC_not_used_bit_2_0                CBCC2.bit0
#define CBC_not_used_bit_2_1                CBCC2.bit1
#define CBC_not_used_bit_2_2                CBCC2.bit2
#define CBC_not_used_bit_2_3                CBCC2.bit3
#define CBC_not_used_bit_2_4                CBCC2.bit4
#define CBC_not_used_bit_2_5                CBCC2.bit5
#define CBC_not_used_bit_2_6                CBCC2.bit6
#define CBC_not_used_bit_2_7                CBCC2.bit7

#define CBC_not_used_bit_3_0                CBCC3.bit0
#define CBC_not_used_bit_3_1                CBCC3.bit1
#define CBC_not_used_bit_3_2                CBCC3.bit2
#define CBC_not_used_bit_3_3                CBCC3.bit3
#define CBC_not_used_bit_3_4                CBCC3.bit4
#define CBC_not_used_bit_3_5                CBCC3.bit5
#define CBC_not_used_bit_3_6                CBCC3.bit6
#define CBC_not_used_bit_3_7                CBCC3.bit7

#define CBC_not_used_bit_4_0                CBCC4.bit0
#define CBC_not_used_bit_4_1                CBCC4.bit1
#define EBD_Dump_chk_rl                     CBCC4.bit2
#define EBD_Dump_chk_rr                     CBCC4.bit3
#define CBC_FIRST_SCAN                      CBCC4.bit4
#define CBC_INHIBIT_ESC_FLAG                CBCC4.bit5
#define CBC_INHIBIT_ABS_FLAG                CBCC4.bit6
#define lccbcu1VehSteadyState               CBCC4.bit7

#define lccbcu1AyEstErrorSus                CBCC5.bit0
#define lccbcu1AyEstErrorDet                CBCC5.bit1
#define lccbcu1AyCheckOK                    CBCC5.bit2
#define lccbcu1UnstableRearSlip             CBCC5.bit3
#define lccbcu1CbcSlipOK					CBCC5.bit4
#define lccbcu1ReCBCFlag	                CBCC5.bit5
#define lccbcu1CbcSpeedOK                   CBCC5.bit6
#define ldcbcu1AyEstValid                   CBCC5.bit7

#define lccbcu1CbcFrontDvOK                 CBCC6.bit0
#define lccbcu1AbsCtrl                      CBCC6.bit1
#define lccbcu1InsideAbsCtrl                CBCC6.bit2
#define CBC_not_used_bit_6_3                CBCC6.bit3
#define CBC_not_used_bit_6_4                CBCC6.bit4
#define CBC_not_used_bit_6_5                CBCC6.bit5
#define CBC_not_used_bit_6_6                CBCC6.bit6
#define CBC_not_used_bit_6_7                CBCC6.bit7


typedef struct {
	
	int16_t  s16CbcWPress;
	uint8_t  u8PresCtrlMode;
	int8_t   s8PressRiseCnt;
	int8_t   s8FadeOutMode;
	int16_t  s16FadeOutCnt;
	uint8_t  u8CbcPwmDuty;
  #if defined(__CBC_DEBUG)
    uint8_t  u8Debugger;
  #endif  	
	
	uint16_t u1CbcRearWL		:1;
	uint16_t u1CbcLeftWL		:1;
	uint16_t u1CbcActive		:1;
	uint16_t u1CbcEnable		:1;
	uint16_t u1CbcFadeOut		:1;
	uint16_t u1CbcAbsAct		:1;
	uint16_t u1CbcEbdDump		:1;
	uint16_t u1CbcEscAct		:1;	
} CBC_WHL_CTRL_t;

#define CBC_ENABLE_fl                       lccbcFL.u1CbcEnable
#define CBC_ENABLE_fr                       lccbcFR.u1CbcEnable
#define CBC_ENABLE_rl                       lccbcRL.u1CbcEnable
#define CBC_ENABLE_rr                       lccbcRR.u1CbcEnable
#define CBC_ACTIVE_fl                       lccbcFL.u1CbcActive
#define CBC_ACTIVE_fr                       lccbcFR.u1CbcActive
#define CBC_ACTIVE_rl                       lccbcRL.u1CbcActive
#define CBC_ACTIVE_rr                       lccbcRR.u1CbcActive
#define CBC_FADE_OUT_fl                     lccbcFL.u1CbcFadeOut
#define CBC_FADE_OUT_fr                     lccbcFR.u1CbcFadeOut
#define CBC_FADE_OUT_rl                     lccbcRL.u1CbcFadeOut
#define CBC_FADE_OUT_rr                     lccbcRR.u1CbcFadeOut
#define CBC_FADE_OUT_fl_cnt                 lccbcFL.s16FadeOutCnt
#define CBC_FADE_OUT_fr_cnt                 lccbcFR.s16FadeOutCnt
#define CBC_FADE_OUT_rl_cnt                 lccbcRL.s16FadeOutCnt
#define CBC_FADE_OUT_rr_cnt                 lccbcRR.s16FadeOutCnt
#define lccbcs16WPressFL                    lccbcFL.s16CbcWPress
#define lccbcs16WPressFR                    lccbcFR.s16CbcWPress
#define lccbcs16WPressRL                    lccbcRL.s16CbcWPress
#define lccbcs16WPressRR                    lccbcRR.s16CbcWPress
#define lccbcu8PresCtrlModeFL               lccbcFL.u8PresCtrlMode
#define lccbcu8PresCtrlModeFR               lccbcFR.u8PresCtrlMode
#define lccbcu8PresCtrlModeRL               lccbcRL.u8PresCtrlMode
#define lccbcu8PresCtrlModeRR               lccbcRR.u8PresCtrlMode
#define lccbcs8FadeOutModeFL                lccbcFL.s8FadeOutMode
#define lccbcs8FadeOutModeFR                lccbcFR.s8FadeOutMode
#define lccbcs8FadeOutModeRL                lccbcRL.s8FadeOutMode
#define lccbcs8FadeOutModeRR                lccbcRR.s8FadeOutMode

/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t CBCC0,CBCC1,CBCC2,CBCC3,CBCC4,CBCC5,CBCC6;
extern CBC_WHL_CTRL_t lccbcRL, lccbcRR;
#if __FRONT_CBC_ENABLE
extern CBC_WHL_CTRL_t lccbcFL, lccbcFR;
#endif

extern int16_t CBC_target_p, CBC_target_delta_p;
extern int16_t lccbcs16CbcMpress;

extern uint8_t lccbcu8CbcMode;
extern int8_t lccbcs8RiseHoldCnt,lccbcs8DumpHoldCnt;
extern int8_t lccbcs8RiseHoldCntFront,lccbcs8DumpHoldCntFront;
extern int8_t lccbcs8RiseHoldMax,lccbcs8DumpHoldMax;
extern int16_t lccbcs16FadeOutMaxTime;

#if (__ABS_CBC_DISABLE==0)
extern int16_t lccbcs16EstVref,lccbcs16EstAy,lccbcs16EstAx,lccbcs16EstWPressRL,lccbcs16EstWPressRR,lccbcs16EstMpress;
extern int16_t lccbcs16EstAy2,lccbcs16AyCheckCounter,lccbcs16AyCheckCounter2,lccbcs16AyOKCounter;
#endif

extern int16_t lccbcs16ReardeltaP, lccbcs16FrontdeltaP;
extern int16_t cbc_alat_enter_th, cbc_alat_exit_th;
/*Global Extern Functions  Declaration****************************************/

extern void LCCBC_vCallControl(void);
extern void LCCBC_vResetControl(void);
extern void LDCBC_vCallEstimation(void);


  #endif /* #ifndef __LCCBCCALLCONTROL_H__ */
#endif	/* #if __CBC */





