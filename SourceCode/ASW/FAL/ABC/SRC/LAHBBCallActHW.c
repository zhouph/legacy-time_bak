/******************************************************************************
* Project Name: Hill Start Assist control
* File: LAHSACallActHW.C
* Description: Valve/Motor actuaction by Hill Start Assist Controller
* Date: June. 4. 2005
******************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAHBBCallActHW 
	#include "Mdyn_autosar.h"               
#endif

/* Includes ******************************************************************/
#include "LAHBBCallActHW.h"
#include "LCHBBCallControl.h"
#include "LACallMain.h"



/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/

/* LocalFunction prototype ***************************************************/


/* Implementation*************************************************************/


#if __TCMF_LowVacuumBoost
/******************************************************************************
* FUNCTION NAME:      LAHBB_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Hydraulic Brake Boost Controller
******************************************************************************/
void	LAHBB_vCallActHW(void)
{
	if(TCMF_LowVacuumBoost_flag==1) 
	{
		if(lcs8HbbOtherCtrlEnable==0)
		{
		  #if (__SPLIT_TYPE==0)	
		  	if((ESP_BRAKE_CONTROL_FR==0) && (ESP_BRAKE_CONTROL_RL==0))
		  	{
				TCL_DEMAND_fr = HBB_TC_Primary_ACT;
				S_VALVE_RIGHT = HBB_ESV_Primary_ACT;
			}
			if((ESP_BRAKE_CONTROL_RR==0) && (ESP_BRAKE_CONTROL_FL==0))
		  	{
				TCL_DEMAND_fl = HBB_TC_Secondary_ACT;
				S_VALVE_LEFT  = HBB_ESV_Secondary_ACT;
			}
		  #else
		  	if((ESP_BRAKE_CONTROL_FR==0) && (ESP_BRAKE_CONTROL_FL==0))
		  	{
				TCL_DEMAND_PRIMARY = HBB_TC_Primary_ACT;
				S_VALVE_PRIMARY    = HBB_ESV_Primary_ACT;
			}
		  	if((ESP_BRAKE_CONTROL_RR==0) && (ESP_BRAKE_CONTROL_RL==0))
		  	{
				TCL_DEMAND_SECONDARY = HBB_TC_Secondary_ACT;
				S_VALVE_SECONDARY    = HBB_ESV_Secondary_ACT;
			}
		  #endif	
		
			VALVE_ACT = 1;
		}
	}
} 
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAHBBCallActHW 
	#include "Mdyn_autosar.h"               
#endif
