#ifndef __LSABSCALLSENSORSIGNAL_H__
#define __LSABSCALLSENSORSIGNAL_H__
/* Includes ************************************************************/
#include "LVarHead.h"

/**********************************************************************/

/* GROBAL Definition  *************************************************/
#define		__LONG_LSABS_vEstLongGsensorOffset_CALC	0
#define     __WHEEL_VEL_VERIFY          			1
extern U8_BIT_STRUCT_t LSABSF0;
#if	__TCS || __ETC || __EDC
extern	U8_BIT_STRUCT_t LSABSF1;
#endif

#define ldu1VehicleStopStateFlg     LSABSF0.bit7
#define ALONG_OFFSET                LSABSF0.bit6
#define G_SEN_FAIL                  LSABSF0.bit5
#define BRAKE_SIGNAL                LSABSF0.bit4
#define R_GEAR_DET					LSABSF0.bit3
#define BRAKING_STATE				LSABSF0.bit2
#define ACTIVE_BRAKE_ACT			LSABSF0.bit1
#define BRAKING_STATE_OLD			LSABSF0.bit0

#if	__TCS || __ETC || __EDC
#define ldu1Gear_Known     				LSABSF1.bit7
#define ldu1gear_R                		LSABSF1.bit6
#define ldu1gear_D                  	LSABSF1.bit5
#define ldu1gear_N_P                	LSABSF1.bit4
#define LSABSF1unusedflag3			LSABSF1.bit3
#define LSABSF1unusedflag2			LSABSF1.bit2
#define LSABSF1unusedflag1			LSABSF1.bit1
#define LSABSF1unusedflag0			LSABSF1.bit0
#endif

/**********************************************************************/
#define 	SCALE_FACTOR	8
/* Global Variable  Declaration****************************************/
/*  extern uint8_t speed_calc_timer;*/

#if __AX_SENSOR
    extern	int16_t	ax_Filter_old;
/*	extern	int16_t	along;*/
	extern	int16_t	along_old;
#endif	



    extern	uint8_t	bls_k_timer;
    #if __VDC
 	extern	uint8_t	brk_st_det_cnt;
 	#endif
	extern	uint8_t	ldabsu8BrakingStateResetTimer; 	
 	#if __AX_SENSOR
	extern	int16_t	lss16absAx1000g;
	extern	int16_t	lss16absAx1000gDiff;
	extern	int16_t	lss16absAxOffsetComp1000g;
	extern	int16_t	lsabss16RawAxSignal;
	extern  int16_t	lsabss16FilteredAxSignal;
	extern  int16_t lsabss16RawAxSignalTemp;
	#endif
	
	#if _ESCi_ENA==ENABLE
		extern int16_t lsabss16FilteredAxSignalOld_i;
		extern int16_t lsabss16FilteredAxSignal_i;
		extern int16_t lsabss16FilteredAxSignalTemp_i;    

		extern int16_t lsabss16FilteredAxSignalOld_i2;
		extern int16_t lsabss16FilteredAxSignal_i2;
		extern int16_t lsabss16FilteredAxSignalTemp_i2;   

		extern int16_t lsesps16ESCiYawAccelCompAx;
		extern int16_t lsesps16ESCiYawAccelCompAxN;
		extern int16_t lsabss16FilteredAxSignalOld_iN;
		extern int16_t lsabss16FilteredAxSignal_iN;
		extern int16_t lsabss16FilteredAxSignalTemp_iN;   
    #endif

/* Global Function Declaration ****************************************/
extern	void	LSABS_vCallSensorSignal(void);
/**********************************************************************/

#endif
