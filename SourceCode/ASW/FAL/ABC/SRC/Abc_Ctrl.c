/**
 * @defgroup Abc_Ctrl Abc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Abc_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Abc_Ctrl.h"
#include "Abc_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LCallMain.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ABC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Abc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Abc_Ctrl_HdrBusType Abc_CtrlBus;

/*Logger Var*/
uint8       CAN_LOG_DATA[80];
uint8       AHBLOG[80];
/* Version Info */
const SwcVersionInfo_t Abc_CtrlVersionInfo = 
{   
    ABC_CTRL_MODULE_ID,           /* Abc_CtrlVersionInfo.ModuleId */
    ABC_CTRL_MAJOR_VERSION,       /* Abc_CtrlVersionInfo.MajorVer */
    ABC_CTRL_MINOR_VERSION,       /* Abc_CtrlVersionInfo.MinorVer */
    ABC_CTRL_PATCH_VERSION,       /* Abc_CtrlVersionInfo.PatchVer */
    ABC_CTRL_BRANCH_VERSION       /* Abc_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t Abc_CtrlEemFailData;
Eem_MainEemCtrlInhibitData_t Abc_CtrlEemCtrlInhibitData;
Proxy_RxCanRxAccelPedlInfo_t Abc_CtrlCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t Abc_CtrlCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t Abc_CtrlCanRxEngTempInfo;
Proxy_RxCanRxEscInfo_t Abc_CtrlCanRxEscInfo;
Msp_CtrlMotRotgAgSigInfo_t Abc_CtrlMotRotgAgSigInfo;
Swt_SenEscSwtStInfo_t Abc_CtrlEscSwtStInfo;
Wss_SenWhlSpdInfo_t Abc_CtrlWhlSpdInfo;
Diag_HndlrSasCalInfo_t Abc_CtrlSasCalInfo;
Det_5msCtrlBrkPedlStatusInfo_t Abc_CtrlBrkPedlStatusInfo;
Spc_5msCtrlCircPFildInfo_t Abc_CtrlCircPFildInfo;
Spc_5msCtrlCircPOffsCorrdInfo_t Abc_CtrlCircPOffsCorrdInfo;
Det_5msCtrlEstimdWhlPInfo_t Abc_CtrlEstimdWhlPInfo;
Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Abc_CtrlPedlSimrPOffsCorrdInfo;
Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Abc_CtrlPedlTrvlOffsCorrdInfo;
Spc_5msCtrlPistPFildInfo_t Abc_CtrlPistPFildInfo;
Spc_5msCtrlPistPOffsCorrdInfo_t Abc_CtrlPistPOffsCorrdInfo;
Rbc_CtrlRgnBrkCoopWithAbsInfo_t Abc_CtrlRgnBrkCoopWithAbsInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Abc_CtrlStkRecvryActnIfInfo;
Pct_5msCtrlMuxCmdExecStInfo_t Abc_CtrlMuxCmdExecStInfo;
Proxy_TxTxESCSensorInfo_t Abc_CtrlTxESCSensorInfo;
SenPwrM_MainSenPwrMonitor_t Abc_CtrlSenPwrMonitorData;
Eem_MainEemSuspectData_t Abc_CtrlEemSuspectData;
Eem_MainEemEceData_t Abc_CtrlEemEceData;
YawM_MainYAWMSerialInfo_t Abc_CtrlYAWMSerialInfo;
YawM_MainYAWMOutInfo_t Abc_CtrlYAWMOutInfo;
Arb_CtrlFinalTarPInfo_t Abc_CtrlFinalTarPInfo;
BbsVlvM_MainFSBbsVlvInitTest_t Abc_CtrlFSBbsVlvInitEndFlg;
Mom_HndlrEcuModeSts_t Abc_CtrlEcuModeSts;
Prly_HndlrIgnOnOffSts_t Abc_CtrlIgnOnOffSts;
Ioc_InputSR1msVBatt1Mon_t Abc_CtrlVBatt1Mon;
AbsVlvM_MainFSEscVlvInitTest_t Abc_CtrlFSEscVlvInitEndFlg;
Diag_HndlrDiagSci_t Abc_CtrlDiagSci;
Proxy_RxCanRxGearSelDispErrInfo_t Abc_CtrlCanRxGearSelDispErrInfo;
Pct_5msCtrlPCtrlAct_t Abc_CtrlPCtrlAct;
Swt_SenBlsSwt_t Abc_CtrlBlsSwt;
Swt_SenEscSwt_t Abc_CtrlEscSwt;
Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Abc_CtrlBrkPRednForBaseBrkCtrlr;
Det_5msCtrlPedlTrvlFinal_t Abc_CtrlPedlTrvlFinal;
Rbc_CtrlRgnBrkCtrlrActStFlg_t Abc_CtrlRgnBrkCtrlrActStFlg;
Pct_5msCtrlFinalTarPFromPCtrl_t Abc_CtrlFinalTarPFromPCtrl;
Bbc_CtrlTarPFromBaseBrkCtrlr_t Abc_CtrlTarPFromBaseBrkCtrlr;
Bbc_CtrlTarPFromBrkPedl_t Abc_CtrlTarPFromBrkPedl;
MtrM_MainMTRInitTest_t Abc_CtrlMTRInitEndFlg;

/* Output Data Element */
Abc_CtrlWhlVlvReqAbcInfo_t Abc_CtrlWhlVlvReqAbcInfo;
Abc_CtrlCanTxInfo_t Abc_CtrlCanTxInfo;
Abc_CtrlAbsCtrlInfo_t Abc_CtrlAbsCtrlInfo;
Abc_CtrlAvhCtrlInfo_t Abc_CtrlAvhCtrlInfo;
Abc_CtrlBaCtrlInfo_t Abc_CtrlBaCtrlInfo;
Abc_CtrlEbdCtrlInfo_t Abc_CtrlEbdCtrlInfo;
Abc_CtrlEbpCtrlInfo_t Abc_CtrlEbpCtrlInfo;
Abc_CtrlEpbiCtrlInfo_t Abc_CtrlEpbiCtrlInfo;
Abc_CtrlEscCtrlInfo_t Abc_CtrlEscCtrlInfo;
Abc_CtrlHdcCtrlInfo_t Abc_CtrlHdcCtrlInfo;
Abc_CtrlHsaCtrlInfo_t Abc_CtrlHsaCtrlInfo;
Abc_CtrlSccCtrlInfo_t Abc_CtrlSccCtrlInfo;
Abc_CtrlStkRecvryCtrlIfInfo_t Abc_CtrlStkRecvryCtrlIfInfo;
Abc_CtrlTcsCtrlInfo_t Abc_CtrlTcsCtrlInfo;
Abc_CtrlTvbbCtrlInfo_t Abc_CtrlTvbbCtrlInfo;
Abc_CtrlFunctionLamp_t Abc_CtrlFunctionLamp;
Abc_CtrlActvBrkCtrlrActFlg_t Abc_CtrlActvBrkCtrlrActFlg;
Abc_CtrlAy_t Abc_CtrlAy;
Abc_CtrlVehSpd_t Abc_CtrlVehSpd;

uint32 Abc_Ctrl_Timer_Start;
uint32 Abc_Ctrl_Timer_Elapsed;

#define ABC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ABC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ABC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Abc_MemMap.h"
#define ABC_CTRL_START_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/** Variable Section (32BIT)**/


#define ABC_CTRL_STOP_SEC_VAR_32BIT
#include "Abc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ABC_CTRL_START_SEC_CODE
#include "Abc_MemMap.h"

void ActiveBrakeControl_Init(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Abc_Ctrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BBSSol = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ESCSol = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_FrontSol = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_RearSol = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Motor = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_MPS = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_MGD = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BBSValveRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ESCValveRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ECUHw = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ASIC = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_OverVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_UnderVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_LowVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_LowerVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SenPwr_12V = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SenPwr_5V = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Yaw = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ay = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Ax = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Str = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_CirP1 = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_CirP2 = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SimP = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BLS = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ESCSw = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_HDCSw = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_AVHSw = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BrakeLampRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_EssRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_GearR = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_Clutch = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_ParkBrake = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_PedalPDT = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_PedalPDF = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_BrakeFluid = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_TCSTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_HDCTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SCCTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_TVBBTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_MainCanLine = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SubCanLine = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_FWDTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_HCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_MCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_VariantCoding = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssFL = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssFR = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssRL = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_WssRR = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_SameSideWss = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_DiagonalWss = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_FrontWss = 0;
    Abc_CtrlBus.Abc_CtrlEemFailData.Eem_Fail_RearWss = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Cbs = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Edc = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Btcs = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Etcs = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hsa = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hdc = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Pba = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Avh = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Moc = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_BBS_AllControlInhibit = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_BBS_DegradeModeFail = 0;
    Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Abc_CtrlBus.Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal = 0;
    Abc_CtrlBus.Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TarGearPosi = 0;
    Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.GearSelDisp = 0;
    Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TcuSwiGs = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEngTempInfo.EngTemp = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEngTempInfo.EngTempErr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.Ax = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.YawRate = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.SteeringAngle = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.Ay = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.PbSwt = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ClutchSwt = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.GearRSwt = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngActIndTq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngRpm = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngIndTq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngFrictionLossTq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngStdTq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TurbineRpm = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ThrottleAngle = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TpsResol1000 = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.PvAvCanResol1000 = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngChr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngVol = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.GearType = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngClutchState = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MotEstTq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIntvTCU = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIntvSlowTCU = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TqIncReq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.DecelReq = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.RainSnsStat = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.EngSpdErr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.AtType = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.MtType = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.CvtType = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.DctType = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HevAtTcu = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TurbineRpmErr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.ThrottleAngleErr = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperIntSW = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperLow = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperHigh = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperValid = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.WiperAuto = 0;
    Abc_CtrlBus.Abc_CtrlCanRxEscInfo.TcuFaultSts = 0;
    Abc_CtrlBus.Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt = 0;
    Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt = 0;
    Abc_CtrlBus.Abc_CtrlWhlSpdInfo.FlWhlSpd = 0;
    Abc_CtrlBus.Abc_CtrlWhlSpdInfo.FrWhlSpd = 0;
    Abc_CtrlBus.Abc_CtrlWhlSpdInfo.RlWhlSpd = 0;
    Abc_CtrlBus.Abc_CtrlWhlSpdInfo.RrlWhlSpd = 0;
    Abc_CtrlBus.Abc_CtrlSasCalInfo.DiagSasCaltoAppl = 0;
    Abc_CtrlBus.Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = 0;
    Abc_CtrlBus.Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = 0;
    Abc_CtrlBus.Abc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = 0;
    Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP = 0;
    Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP = 0;
    Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP = 0;
    Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP = 0;
    Abc_CtrlBus.Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlPistPFildInfo.PistPFild_1_100Bar = 0;
    Abc_CtrlBus.Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd = 0;
    Abc_CtrlBus.Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip = 0;
    Abc_CtrlBus.Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff = 0;
    Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = 0;
    Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = 0;
    Abc_CtrlBus.Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe = 0;
    Abc_CtrlBus.Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi = 0;
    Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.EstimatedYaw = 0;
    Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.EstimatedAY = 0;
    Abc_CtrlBus.Abc_CtrlTxESCSensorInfo.A_long_1_1000g = 0;
    Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssFL = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssFR = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssRL = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_WssRR = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SameSideWss = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_DiagonalWss = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_FrontWss = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_RearWss = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BBSSol = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ESCSol = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_FrontSol = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_RearSol = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Motor = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_MPS = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_MGD = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BBSValveRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ESCValveRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ECUHw = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ASIC = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_OverVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_UnderVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_LowVolt = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_12V = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_5V = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Yaw = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Ay = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Ax = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Str = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP1 = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_CirP2 = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SimP = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BS = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BLS = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ESCSw = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_HDCSw = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_AVHSw = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BrakeLampRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_EssRelay = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_GearR = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_Clutch = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_BrakeFluid = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_TCSTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_HDCTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SCCTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_TVBBTemp = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_SubCanLine = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_FWDTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_HCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_MCUTimeOut = 0;
    Abc_CtrlBus.Abc_CtrlEemSuspectData.Eem_Suspect_VariantCoding = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Wss = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Yaw = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Ay = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Ax = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Cir1P = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Cir2P = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_SimP = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Bls = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Pedal = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Motor = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Vdc_Sw = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Hdc_Sw = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_GearR_Sw = 0;
    Abc_CtrlBus.Abc_CtrlEemEceData.Eem_Ece_Clutch_Sw = 0;
    Abc_CtrlBus.Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg = 0;
    Abc_CtrlBus.Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg = 0;
    Abc_CtrlBus.Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err = 0;
    Abc_CtrlBus.Abc_CtrlFinalTarPInfo.FinalMaxCircuitTp = 0;
    Abc_CtrlBus.Abc_CtrlFSBbsVlvInitEndFlg = 0;
    Abc_CtrlBus.Abc_CtrlEcuModeSts = 0;
    Abc_CtrlBus.Abc_CtrlIgnOnOffSts = 0;
    Abc_CtrlBus.Abc_CtrlVBatt1Mon = 0;
    Abc_CtrlBus.Abc_CtrlFSEscVlvInitEndFlg = 0;
    Abc_CtrlBus.Abc_CtrlDiagSci = 0;
    Abc_CtrlBus.Abc_CtrlCanRxGearSelDispErrInfo = 0;
    Abc_CtrlBus.Abc_CtrlPCtrlAct = 0;
    Abc_CtrlBus.Abc_CtrlBlsSwt = 0;
    Abc_CtrlBus.Abc_CtrlEscSwt = 0;
    Abc_CtrlBus.Abc_CtrlBrkPRednForBaseBrkCtrlr = 0;
    Abc_CtrlBus.Abc_CtrlPedlTrvlFinal = 0;
    Abc_CtrlBus.Abc_CtrlRgnBrkCtrlrActStFlg = 0;
    Abc_CtrlBus.Abc_CtrlFinalTarPFromPCtrl = 0;
    Abc_CtrlBus.Abc_CtrlTarPFromBaseBrkCtrlr = 0;
    Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl = 0;
    Abc_CtrlBus.Abc_CtrlMTRInitEndFlg = 0;
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[i] = 0;   
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvReq = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrIvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlIvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrIvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlOvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FrOvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RlOvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.RrOvDataLen = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvTCS = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvMsr = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TqIntvSlowTCS = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.MinGear = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.MaxGear = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsReq = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsCtrl = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.AbsAct = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsGearShiftChr = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.EspCtrl = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.MsrReq = 0;
    Abc_CtrlBus.Abc_CtrlCanTxInfo.TcsProductInfo = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDesTarP = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioReLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsPrioReRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhActFlg = 0;
    Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlAvhCtrlInfo.AvhTarP = 0;
    Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaActFlg = 0;
    Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaCDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlBaCtrlInfo.BaTarP = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPRateReLe = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdTarPRateReRi = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdCtrlModeReLe = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdCtrlModeReRi = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDelTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdDelTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpActFlg = 0;
    Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlEbpCtrlInfo.EbpTarP = 0;
    Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiActFlg = 0;
    Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo.EpbiTarP = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscCtrlModeReRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscTarPRateReRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPrioReRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscDelTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcActFlg = 0;
    Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlHdcCtrlInfo.HdcTarP = 0;
    Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaActFlg = 0;
    Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlHsaCtrlInfo.HsaTarP = 0;
    Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccActFlg = 0;
    Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlSccCtrlInfo.SccTarP = 0;
    Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime = 0;
    Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime = 0;
    Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe = 0;
    Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi = 0;
    Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbActFlg = 0;
    Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg = 0;
    Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo.TvbbTarP = 0;
    Abc_CtrlBus.Abc_CtrlFunctionLamp = 0;
    Abc_CtrlBus.Abc_CtrlActvBrkCtrlrActFlg = 0;
    Abc_CtrlBus.Abc_CtrlAy = 0;
    Abc_CtrlBus.Abc_CtrlVehSpd = 0;

	ActiveBrakeControl_Init();
	
}

void Abc_Ctrl(void)
{
    uint16 i;
    
    Abc_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Abc_Ctrl_Read_Abc_CtrlEemFailData(&Abc_CtrlBus.Abc_CtrlEemFailData);
    /*==============================================================================
    * Members of structure Abc_CtrlEemFailData 
     : Abc_CtrlEemFailData.Eem_Fail_BBSSol;
     : Abc_CtrlEemFailData.Eem_Fail_ESCSol;
     : Abc_CtrlEemFailData.Eem_Fail_FrontSol;
     : Abc_CtrlEemFailData.Eem_Fail_RearSol;
     : Abc_CtrlEemFailData.Eem_Fail_Motor;
     : Abc_CtrlEemFailData.Eem_Fail_MPS;
     : Abc_CtrlEemFailData.Eem_Fail_MGD;
     : Abc_CtrlEemFailData.Eem_Fail_BBSValveRelay;
     : Abc_CtrlEemFailData.Eem_Fail_ESCValveRelay;
     : Abc_CtrlEemFailData.Eem_Fail_ECUHw;
     : Abc_CtrlEemFailData.Eem_Fail_ASIC;
     : Abc_CtrlEemFailData.Eem_Fail_OverVolt;
     : Abc_CtrlEemFailData.Eem_Fail_UnderVolt;
     : Abc_CtrlEemFailData.Eem_Fail_LowVolt;
     : Abc_CtrlEemFailData.Eem_Fail_LowerVolt;
     : Abc_CtrlEemFailData.Eem_Fail_SenPwr_12V;
     : Abc_CtrlEemFailData.Eem_Fail_SenPwr_5V;
     : Abc_CtrlEemFailData.Eem_Fail_Yaw;
     : Abc_CtrlEemFailData.Eem_Fail_Ay;
     : Abc_CtrlEemFailData.Eem_Fail_Ax;
     : Abc_CtrlEemFailData.Eem_Fail_Str;
     : Abc_CtrlEemFailData.Eem_Fail_CirP1;
     : Abc_CtrlEemFailData.Eem_Fail_CirP2;
     : Abc_CtrlEemFailData.Eem_Fail_SimP;
     : Abc_CtrlEemFailData.Eem_Fail_BLS;
     : Abc_CtrlEemFailData.Eem_Fail_ESCSw;
     : Abc_CtrlEemFailData.Eem_Fail_HDCSw;
     : Abc_CtrlEemFailData.Eem_Fail_AVHSw;
     : Abc_CtrlEemFailData.Eem_Fail_BrakeLampRelay;
     : Abc_CtrlEemFailData.Eem_Fail_EssRelay;
     : Abc_CtrlEemFailData.Eem_Fail_GearR;
     : Abc_CtrlEemFailData.Eem_Fail_Clutch;
     : Abc_CtrlEemFailData.Eem_Fail_ParkBrake;
     : Abc_CtrlEemFailData.Eem_Fail_PedalPDT;
     : Abc_CtrlEemFailData.Eem_Fail_PedalPDF;
     : Abc_CtrlEemFailData.Eem_Fail_BrakeFluid;
     : Abc_CtrlEemFailData.Eem_Fail_TCSTemp;
     : Abc_CtrlEemFailData.Eem_Fail_HDCTemp;
     : Abc_CtrlEemFailData.Eem_Fail_SCCTemp;
     : Abc_CtrlEemFailData.Eem_Fail_TVBBTemp;
     : Abc_CtrlEemFailData.Eem_Fail_MainCanLine;
     : Abc_CtrlEemFailData.Eem_Fail_SubCanLine;
     : Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_FWDTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_HCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_MCUTimeOut;
     : Abc_CtrlEemFailData.Eem_Fail_VariantCoding;
     : Abc_CtrlEemFailData.Eem_Fail_WssFL;
     : Abc_CtrlEemFailData.Eem_Fail_WssFR;
     : Abc_CtrlEemFailData.Eem_Fail_WssRL;
     : Abc_CtrlEemFailData.Eem_Fail_WssRR;
     : Abc_CtrlEemFailData.Eem_Fail_SameSideWss;
     : Abc_CtrlEemFailData.Eem_Fail_DiagonalWss;
     : Abc_CtrlEemFailData.Eem_Fail_FrontWss;
     : Abc_CtrlEemFailData.Eem_Fail_RearWss;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlEemCtrlInhibitData(&Abc_CtrlBus.Abc_CtrlEemCtrlInhibitData);
    /*==============================================================================
    * Members of structure Abc_CtrlEemCtrlInhibitData 
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Cbs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Edc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Btcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Etcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hsa;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hdc;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Pba;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Avh;
     : Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Moc;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_AllControlInhibit;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DiagControlInhibit;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DegradeModeFail;
     : Abc_CtrlEemCtrlInhibitData.Eem_BBS_DefectiveModeFail;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlCanRxAccelPedlInfo(&Abc_CtrlBus.Abc_CtrlCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxAccelPedlInfo 
     : Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_TarGearPosi(&Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TarGearPosi);
    Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_GearSelDisp(&Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.GearSelDisp);
    Abc_Ctrl_Read_Abc_CtrlCanRxIdbInfo_TcuSwiGs(&Abc_CtrlBus.Abc_CtrlCanRxIdbInfo.TcuSwiGs);

    Abc_Ctrl_Read_Abc_CtrlCanRxEngTempInfo(&Abc_CtrlBus.Abc_CtrlCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxEngTempInfo 
     : Abc_CtrlCanRxEngTempInfo.EngTemp;
     : Abc_CtrlCanRxEngTempInfo.EngTempErr;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlCanRxEscInfo(&Abc_CtrlBus.Abc_CtrlCanRxEscInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlCanRxEscInfo 
     : Abc_CtrlCanRxEscInfo.Ax;
     : Abc_CtrlCanRxEscInfo.YawRate;
     : Abc_CtrlCanRxEscInfo.SteeringAngle;
     : Abc_CtrlCanRxEscInfo.Ay;
     : Abc_CtrlCanRxEscInfo.PbSwt;
     : Abc_CtrlCanRxEscInfo.ClutchSwt;
     : Abc_CtrlCanRxEscInfo.GearRSwt;
     : Abc_CtrlCanRxEscInfo.EngActIndTq;
     : Abc_CtrlCanRxEscInfo.EngRpm;
     : Abc_CtrlCanRxEscInfo.EngIndTq;
     : Abc_CtrlCanRxEscInfo.EngFrictionLossTq;
     : Abc_CtrlCanRxEscInfo.EngStdTq;
     : Abc_CtrlCanRxEscInfo.TurbineRpm;
     : Abc_CtrlCanRxEscInfo.ThrottleAngle;
     : Abc_CtrlCanRxEscInfo.TpsResol1000;
     : Abc_CtrlCanRxEscInfo.PvAvCanResol1000;
     : Abc_CtrlCanRxEscInfo.EngChr;
     : Abc_CtrlCanRxEscInfo.EngVol;
     : Abc_CtrlCanRxEscInfo.GearType;
     : Abc_CtrlCanRxEscInfo.EngClutchState;
     : Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv;
     : Abc_CtrlCanRxEscInfo.MotEstTq;
     : Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv;
     : Abc_CtrlCanRxEscInfo.TqIntvTCU;
     : Abc_CtrlCanRxEscInfo.TqIntvSlowTCU;
     : Abc_CtrlCanRxEscInfo.TqIncReq;
     : Abc_CtrlCanRxEscInfo.DecelReq;
     : Abc_CtrlCanRxEscInfo.RainSnsStat;
     : Abc_CtrlCanRxEscInfo.EngSpdErr;
     : Abc_CtrlCanRxEscInfo.AtType;
     : Abc_CtrlCanRxEscInfo.MtType;
     : Abc_CtrlCanRxEscInfo.CvtType;
     : Abc_CtrlCanRxEscInfo.DctType;
     : Abc_CtrlCanRxEscInfo.HevAtTcu;
     : Abc_CtrlCanRxEscInfo.TurbineRpmErr;
     : Abc_CtrlCanRxEscInfo.ThrottleAngleErr;
     : Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct;
     : Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV;
     : Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct;
     : Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV;
     : Abc_CtrlCanRxEscInfo.WiperIntSW;
     : Abc_CtrlCanRxEscInfo.WiperLow;
     : Abc_CtrlCanRxEscInfo.WiperHigh;
     : Abc_CtrlCanRxEscInfo.WiperValid;
     : Abc_CtrlCanRxEscInfo.WiperAuto;
     : Abc_CtrlCanRxEscInfo.TcuFaultSts;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlMotRotgAgSigInfo_StkPosnMeasd(&Abc_CtrlBus.Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlEscSwtStInfo_EscDisabledBySwt(&Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt);
    Abc_Ctrl_Read_Abc_CtrlEscSwtStInfo_TcsDisabledBySwt(&Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt);

    Abc_Ctrl_Read_Abc_CtrlWhlSpdInfo(&Abc_CtrlBus.Abc_CtrlWhlSpdInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlWhlSpdInfo 
     : Abc_CtrlWhlSpdInfo.FlWhlSpd;
     : Abc_CtrlWhlSpdInfo.FrWhlSpd;
     : Abc_CtrlWhlSpdInfo.RlWhlSpd;
     : Abc_CtrlWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlSasCalInfo(&Abc_CtrlBus.Abc_CtrlSasCalInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlSasCalInfo 
     : Abc_CtrlSasCalInfo.DiagSasCaltoAppl;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlBrkPedlStatusInfo_DrvrIntendToRelsBrk(&Abc_CtrlBus.Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar(&Abc_CtrlBus.Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar);
    Abc_Ctrl_Read_Abc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar(&Abc_CtrlBus.Abc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd(&Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd);
    Abc_Ctrl_Read_Abc_CtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd(&Abc_CtrlBus.Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd);

    Abc_Ctrl_Read_Abc_CtrlEstimdWhlPInfo(&Abc_CtrlBus.Abc_CtrlEstimdWhlPInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEstimdWhlPInfo 
     : Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP;
     : Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd(&Abc_CtrlBus.Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlPedlTrvlOffsCorrdInfo_PdfRawOffsCorrd(&Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd);
    Abc_Ctrl_Read_Abc_CtrlPedlTrvlOffsCorrdInfo_PdtRawOffsCorrd(&Abc_CtrlBus.Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlPistPFildInfo_PistPFild_1_100Bar(&Abc_CtrlBus.Abc_CtrlPistPFildInfo.PistPFild_1_100Bar);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlPistPOffsCorrdInfo_PistPOffsCorrd(&Abc_CtrlBus.Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd);

    Abc_Ctrl_Read_Abc_CtrlRgnBrkCoopWithAbsInfo(&Abc_CtrlBus.Abc_CtrlRgnBrkCoopWithAbsInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlRgnBrkCoopWithAbsInfo 
     : Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip;
     : Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlStkRecvryActnIfInfo_RecommendStkRcvrLvl(&Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl);
    Abc_Ctrl_Read_Abc_CtrlStkRecvryActnIfInfo_StkRecvryCtrlState(&Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState);

    Abc_Ctrl_Read_Abc_CtrlMuxCmdExecStInfo(&Abc_CtrlBus.Abc_CtrlMuxCmdExecStInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlMuxCmdExecStInfo 
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe;
     : Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlTxESCSensorInfo(&Abc_CtrlBus.Abc_CtrlTxESCSensorInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlTxESCSensorInfo 
     : Abc_CtrlTxESCSensorInfo.EstimatedYaw;
     : Abc_CtrlTxESCSensorInfo.EstimatedAY;
     : Abc_CtrlTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlSenPwrMonitorData_SenPwrM_12V_Stable(&Abc_CtrlBus.Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable);

    Abc_Ctrl_Read_Abc_CtrlEemSuspectData(&Abc_CtrlBus.Abc_CtrlEemSuspectData);
    /*==============================================================================
    * Members of structure Abc_CtrlEemSuspectData 
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssFL;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssFR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssRL;
     : Abc_CtrlEemSuspectData.Eem_Suspect_WssRR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SameSideWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_DiagonalWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FrontWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_RearWss;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BBSSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FrontSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_RearSol;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Motor;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MPS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MGD;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BBSValveRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCValveRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ECUHw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ASIC;
     : Abc_CtrlEemSuspectData.Eem_Suspect_OverVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_UnderVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_LowVolt;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_12V;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_5V;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Yaw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Ay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Ax;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Str;
     : Abc_CtrlEemSuspectData.Eem_Suspect_CirP1;
     : Abc_CtrlEemSuspectData.Eem_Suspect_CirP2;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SimP;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BLS;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ESCSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HDCSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_AVHSw;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BrakeLampRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_EssRelay;
     : Abc_CtrlEemSuspectData.Eem_Suspect_GearR;
     : Abc_CtrlEemSuspectData.Eem_Suspect_Clutch;
     : Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake;
     : Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT;
     : Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF;
     : Abc_CtrlEemSuspectData.Eem_Suspect_BrakeFluid;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TCSTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HDCTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SCCTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TVBBTemp;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine;
     : Abc_CtrlEemSuspectData.Eem_Suspect_SubCanLine;
     : Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_FWDTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_HCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_MCUTimeOut;
     : Abc_CtrlEemSuspectData.Eem_Suspect_VariantCoding;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlEemEceData(&Abc_CtrlBus.Abc_CtrlEemEceData);
    /*==============================================================================
    * Members of structure Abc_CtrlEemEceData 
     : Abc_CtrlEemEceData.Eem_Ece_Wss;
     : Abc_CtrlEemEceData.Eem_Ece_Yaw;
     : Abc_CtrlEemEceData.Eem_Ece_Ay;
     : Abc_CtrlEemEceData.Eem_Ece_Ax;
     : Abc_CtrlEemEceData.Eem_Ece_Cir1P;
     : Abc_CtrlEemEceData.Eem_Ece_Cir2P;
     : Abc_CtrlEemEceData.Eem_Ece_SimP;
     : Abc_CtrlEemEceData.Eem_Ece_Bls;
     : Abc_CtrlEemEceData.Eem_Ece_Pedal;
     : Abc_CtrlEemEceData.Eem_Ece_Motor;
     : Abc_CtrlEemEceData.Eem_Ece_Vdc_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_Hdc_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_GearR_Sw;
     : Abc_CtrlEemEceData.Eem_Ece_Clutch_Sw;
     =============================================================================*/
    
    Abc_Ctrl_Read_Abc_CtrlYAWMSerialInfo(&Abc_CtrlBus.Abc_CtrlYAWMSerialInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlYAWMSerialInfo 
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg;
     : Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlYAWMOutInfo_YAWM_Temperature_Err(&Abc_CtrlBus.Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err);

    /* Decomposed structure interface */
    Abc_Ctrl_Read_Abc_CtrlFinalTarPInfo_FinalMaxCircuitTp(&Abc_CtrlBus.Abc_CtrlFinalTarPInfo.FinalMaxCircuitTp);

    Abc_Ctrl_Read_Abc_CtrlFSBbsVlvInitEndFlg(&Abc_CtrlBus.Abc_CtrlFSBbsVlvInitEndFlg);
    Abc_Ctrl_Read_Abc_CtrlEcuModeSts(&Abc_CtrlBus.Abc_CtrlEcuModeSts);
    Abc_Ctrl_Read_Abc_CtrlIgnOnOffSts(&Abc_CtrlBus.Abc_CtrlIgnOnOffSts);
    Abc_Ctrl_Read_Abc_CtrlVBatt1Mon(&Abc_CtrlBus.Abc_CtrlVBatt1Mon);
    Abc_Ctrl_Read_Abc_CtrlFSEscVlvInitEndFlg(&Abc_CtrlBus.Abc_CtrlFSEscVlvInitEndFlg);
    Abc_Ctrl_Read_Abc_CtrlDiagSci(&Abc_CtrlBus.Abc_CtrlDiagSci);
    Abc_Ctrl_Read_Abc_CtrlCanRxGearSelDispErrInfo(&Abc_CtrlBus.Abc_CtrlCanRxGearSelDispErrInfo);
    Abc_Ctrl_Read_Abc_CtrlPCtrlAct(&Abc_CtrlBus.Abc_CtrlPCtrlAct);
    Abc_Ctrl_Read_Abc_CtrlBlsSwt(&Abc_CtrlBus.Abc_CtrlBlsSwt);
    Abc_Ctrl_Read_Abc_CtrlEscSwt(&Abc_CtrlBus.Abc_CtrlEscSwt);
    Abc_Ctrl_Read_Abc_CtrlBrkPRednForBaseBrkCtrlr(&Abc_CtrlBus.Abc_CtrlBrkPRednForBaseBrkCtrlr);
    Abc_Ctrl_Read_Abc_CtrlPedlTrvlFinal(&Abc_CtrlBus.Abc_CtrlPedlTrvlFinal);
    Abc_Ctrl_Read_Abc_CtrlRgnBrkCtrlrActStFlg(&Abc_CtrlBus.Abc_CtrlRgnBrkCtrlrActStFlg);
    Abc_Ctrl_Read_Abc_CtrlFinalTarPFromPCtrl(&Abc_CtrlBus.Abc_CtrlFinalTarPFromPCtrl);
    Abc_Ctrl_Read_Abc_CtrlTarPFromBaseBrkCtrlr(&Abc_CtrlBus.Abc_CtrlTarPFromBaseBrkCtrlr);
    Abc_Ctrl_Read_Abc_CtrlTarPFromBrkPedl(&Abc_CtrlBus.Abc_CtrlTarPFromBrkPedl);
    Abc_Ctrl_Read_Abc_CtrlMTRInitEndFlg(&Abc_CtrlBus.Abc_CtrlMTRInitEndFlg);

    /* Process */
    L_vCallMain();
    /* Output */
    Abc_Ctrl_Write_Abc_CtrlWhlVlvReqAbcInfo(&Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlWhlVlvReqAbcInfo 
     : Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData;
     : Abc_CtrlWhlVlvReqAbcInfo.FlIvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.FrIvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.RlIvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.RrIvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.FlOvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.FrOvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.RlOvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.RrOvReq;
     : Abc_CtrlWhlVlvReqAbcInfo.FlIvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.FrIvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.RlIvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.RrIvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.FlOvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.FrOvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.RlOvDataLen;
     : Abc_CtrlWhlVlvReqAbcInfo.RrOvDataLen;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlCanTxInfo(&Abc_CtrlBus.Abc_CtrlCanTxInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlCanTxInfo 
     : Abc_CtrlCanTxInfo.TqIntvTCS;
     : Abc_CtrlCanTxInfo.TqIntvMsr;
     : Abc_CtrlCanTxInfo.TqIntvSlowTCS;
     : Abc_CtrlCanTxInfo.MinGear;
     : Abc_CtrlCanTxInfo.MaxGear;
     : Abc_CtrlCanTxInfo.TcsReq;
     : Abc_CtrlCanTxInfo.TcsCtrl;
     : Abc_CtrlCanTxInfo.AbsAct;
     : Abc_CtrlCanTxInfo.TcsGearShiftChr;
     : Abc_CtrlCanTxInfo.EspCtrl;
     : Abc_CtrlCanTxInfo.MsrReq;
     : Abc_CtrlCanTxInfo.TcsProductInfo;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlAbsCtrlInfo(&Abc_CtrlBus.Abc_CtrlAbsCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlAbsCtrlInfo 
     : Abc_CtrlAbsCtrlInfo.AbsActFlg;
     : Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
     : Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarP;
     : Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
     : Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe;
     : Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReLe;
     : Abc_CtrlAbsCtrlInfo.AbsPrioReRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe;
     : Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlAvhCtrlInfo(&Abc_CtrlBus.Abc_CtrlAvhCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlAvhCtrlInfo 
     : Abc_CtrlAvhCtrlInfo.AvhActFlg;
     : Abc_CtrlAvhCtrlInfo.AvhDefectFlg;
     : Abc_CtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlBaCtrlInfo(&Abc_CtrlBus.Abc_CtrlBaCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlBaCtrlInfo 
     : Abc_CtrlBaCtrlInfo.BaActFlg;
     : Abc_CtrlBaCtrlInfo.BaCDefectFlg;
     : Abc_CtrlBaCtrlInfo.BaTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlEbdCtrlInfo(&Abc_CtrlBus.Abc_CtrlEbdCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEbdCtrlInfo 
     : Abc_CtrlEbdCtrlInfo.EbdActFlg;
     : Abc_CtrlEbdCtrlInfo.EbdDefectFlg;
     : Abc_CtrlEbdCtrlInfo.EbdTarPFrntLe;
     : Abc_CtrlEbdCtrlInfo.EbdTarPFrntRi;
     : Abc_CtrlEbdCtrlInfo.EbdTarPReLe;
     : Abc_CtrlEbdCtrlInfo.EbdTarPReRi;
     : Abc_CtrlEbdCtrlInfo.EbdTarPRateReLe;
     : Abc_CtrlEbdCtrlInfo.EbdTarPRateReRi;
     : Abc_CtrlEbdCtrlInfo.EbdCtrlModeReLe;
     : Abc_CtrlEbdCtrlInfo.EbdCtrlModeReRi;
     : Abc_CtrlEbdCtrlInfo.EbdDelTarPReLe;
     : Abc_CtrlEbdCtrlInfo.EbdDelTarPReRi;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlEbpCtrlInfo(&Abc_CtrlBus.Abc_CtrlEbpCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEbpCtrlInfo 
     : Abc_CtrlEbpCtrlInfo.EbpActFlg;
     : Abc_CtrlEbpCtrlInfo.EbpDefectFlg;
     : Abc_CtrlEbpCtrlInfo.EbpTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlEpbiCtrlInfo(&Abc_CtrlBus.Abc_CtrlEpbiCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEpbiCtrlInfo 
     : Abc_CtrlEpbiCtrlInfo.EpbiActFlg;
     : Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg;
     : Abc_CtrlEpbiCtrlInfo.EpbiTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlEscCtrlInfo(&Abc_CtrlBus.Abc_CtrlEscCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlEscCtrlInfo 
     : Abc_CtrlEscCtrlInfo.EscActFlg;
     : Abc_CtrlEscCtrlInfo.EscDefectFlg;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPReRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReLe;
     : Abc_CtrlEscCtrlInfo.EscTarPRateReRi;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPrioFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPrioReLe;
     : Abc_CtrlEscCtrlInfo.EscPrioReRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
     : Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReLe;
     : Abc_CtrlEscCtrlInfo.EscDelTarPReRi;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlHdcCtrlInfo(&Abc_CtrlBus.Abc_CtrlHdcCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlHdcCtrlInfo 
     : Abc_CtrlHdcCtrlInfo.HdcActFlg;
     : Abc_CtrlHdcCtrlInfo.HdcDefectFlg;
     : Abc_CtrlHdcCtrlInfo.HdcTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlHsaCtrlInfo(&Abc_CtrlBus.Abc_CtrlHsaCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlHsaCtrlInfo 
     : Abc_CtrlHsaCtrlInfo.HsaActFlg;
     : Abc_CtrlHsaCtrlInfo.HsaDefectFlg;
     : Abc_CtrlHsaCtrlInfo.HsaTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlSccCtrlInfo(&Abc_CtrlBus.Abc_CtrlSccCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlSccCtrlInfo 
     : Abc_CtrlSccCtrlInfo.SccActFlg;
     : Abc_CtrlSccCtrlInfo.SccDefectFlg;
     : Abc_CtrlSccCtrlInfo.SccTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlStkRecvryCtrlIfInfo(&Abc_CtrlBus.Abc_CtrlStkRecvryCtrlIfInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlStkRecvryCtrlIfInfo 
     : Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime;
     : Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime;
     : Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlTcsCtrlInfo(&Abc_CtrlBus.Abc_CtrlTcsCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlTcsCtrlInfo 
     : Abc_CtrlTcsCtrlInfo.TcsActFlg;
     : Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
     : Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
     : Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe;
     : Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlTvbbCtrlInfo(&Abc_CtrlBus.Abc_CtrlTvbbCtrlInfo);
    /*==============================================================================
    * Members of structure Abc_CtrlTvbbCtrlInfo 
     : Abc_CtrlTvbbCtrlInfo.TvbbActFlg;
     : Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg;
     : Abc_CtrlTvbbCtrlInfo.TvbbTarP;
     =============================================================================*/
    
    Abc_Ctrl_Write_Abc_CtrlFunctionLamp(&Abc_CtrlBus.Abc_CtrlFunctionLamp);
    Abc_Ctrl_Write_Abc_CtrlActvBrkCtrlrActFlg(&Abc_CtrlBus.Abc_CtrlActvBrkCtrlrActFlg);
    Abc_Ctrl_Write_Abc_CtrlAy(&Abc_CtrlBus.Abc_CtrlAy);
    Abc_Ctrl_Write_Abc_CtrlVehSpd(&Abc_CtrlBus.Abc_CtrlVehSpd);

    Abc_Ctrl_Timer_Elapsed = STM0_TIM0.U - Abc_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ABC_CTRL_STOP_SEC_CODE
#include "Abc_MemMap.h"

 void ActiveBrakeControl_Init(void)
{
	L_vInitializeVariables();

	pDATA_apCalAbs=(DATA_APCALABS_t*)&apCalAbs2WD;
	pDATA_apCalAbsVafs=(DATA_APCALABSVAFS_t*)&apCalAbsVafs2WD;
	pDATA_apCalEsp = (DATA_APCALESP_t*)&apCalEsp2WD;
	pDATA_apCalEspVafs=(DATA_APCALESPVAFS_t*)&apCalEspVafs2WD;
	pDATA_apCalEspEng=(DATA_APCALESPENG_t*)&apCalEspEng01;
	pEspModel=(ESP_MODEL_t*)&apEspModel2WD;

}

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
