/*******************************************************************/
/*  Program ID: MGH-60 RDCM						                   */
/*  Program description:. Rear Drive Control Module		           */
/*  Input files: 												   */
/*  Output files:                                            	   */
/*  Special notes: none                                            */
/*******************************************************************/

/* Includes ********************************************************/


#include "hm_logic_var.h"
#include "LCallMain.h"
#include "LCCallMain.h"
#include "LCTCSCallControl.h"
#include "LCCBCCallControl.h"
#include "LCSLSCallControl.h"
#include "LCHSACallControl.h"
#include "LCHDCCallControl.h"
#include "LCHRBCallControl.h"
#include "LDROPCallDetection.h"
#include "LCRDCMCallControl.h"


/* Local Definition  ***********************************************/


  #if __RDCM_PROTECTION

struct  bit_position RDCMflag_00;
struct  bit_position RDCMflag_01;
struct  bit_position RDCMflag_02;
struct  bit_position RDCMflag_03;
struct  bit_position RDCMflag_04;
struct  bit_position RDCMflag_05;
struct  bit_position RDCMflag_06;
struct  bit_position RDCMflag_07;
	
int16_t RDCM_cmd;						
int16_t	lts16DeltaRPMForRDCM; 			
int16_t	lts16DeltaRPMErrForRDCM; 		
int16_t	lts16TargetRPMForRDCM;			
int16_t	lts16DeltaRPMErrDiffForRDCM;	
int16_t	lts16PGainForRDCM;				
int16_t	lts16DGainForRDCM;				
int16_t	lts16PDGainEffectForRDCM;
int16_t	lts16TorqRiseforRDCM;
int16_t	lts16RDCMOKTorq;		
uint8_t ltu8RDCMStatus;				
uint8_t ltcsu81SecCntAfterRDCM;
uint8_t ltu8RDCM1stControlCount;
uint8_t RDCM_FTCS_flags;

int16_t lts16DeltaRPMFiltOldForRDCM;    
int16_t lts16DeltaRPMFiltForRDCM;       
uint16_t ltu16DrivenWheelAvgSpeed;      
uint16_t ltu16NonDrivenWheelAvgSpeed;   
uint16_t ltu16TireDynamicLength;        
uint16_t ltu16MainWheelAvgRPM;          
uint16_t ltu16SubWheelAvgRPM;           
int16_t lts16PGainEffectForRDCM;        
int16_t lts16DGainEffectForRDCM;	    

	#endif /*__RDCM_PROTECTION*/
		

/* Variables Definition*********************************************/

/* Local Function prototype ****************************************/
#if __RDCM_PROTECTION
void LCRDCM_vCallControl(void);
static void LCTCS_vCallEngPDCntrlForRDCM(void);
static void LCTCS_vCalDeltaRPM(void);
static void LCTCS_vCalDeltaRPMError(void);
static void LCTCS_vCalPDGainForRDCM(void);
static void LCTCS_vCallDetectRDCMProtCtrl(void);
static void LCTCS_vFailManagementForRDCM(void);
static void LCTCS_vCount1SecAfterRDCM(void);
static void LCTCS_vControlTractionforRDCM(void);
static void LCTCS_vDecideMinTorqLvlforRDCM(void);
#endif  /* __RDCM_PROTECTION */	

#if __RDCM_PROTECTION
void LCRDCM_vCallControl(void)
{
	if(U8_RDCM_CONTROL_FUNCTION == 1)
	{
	LCTCS_vCallEngPDCntrlForRDCM();
	/*LCTCS_vCalDeltaRPM();*/
	/*LCTCS_vCalDeltaRPMError();*/
	/*LCTCS_vCalPDGainForRDCM();*/
	LCTCS_vCallDetectRDCMProtCtrl();
	/*LCTCS_vFailManagementForRDCM();*/
	/*LCTCS_vCount1SecAfterRDCM();*/
	LCTCS_vControlTractionforRDCM();
	/*if(LOOP_TIME_20MS_TASK_1==1)*/
	/*{*/ 	
		LCTCS_vDecideMinTorqLvlforRDCM();
	/*}*/
	/*else{}*/
	}
	else
	{
		;
	}
}
#endif  /* __RDCM_PROTECTION */	

/* Implementation***************************************************/

#if __RDCM_PROTECTION
static void LCTCS_vCallEngPDCntrlForRDCM(void)
{
	/*=============================================================================*/
	/* Purpose : PD Effect for RDCM	Protection 			 			   			   */
	/* Input Variable : 													       */
	/* Output Variable : 														   */
	/*=============================================================================*/	
	LCTCS_vCalDeltaRPM();	
	LCTCS_vCalDeltaRPMError();
	if( LOOP_TIME_20MS_TASK_1 == 1 )
	{ 	
		LCTCS_vCalPDGainForRDCM();
	}
	else{}	
	
	lts16PGainEffectForRDCM = (int16_t)((int32_t)lts16PGainForRDCM * lts16DeltaRPMErrForRDCM / (int16_t)U16_TCS_GAIN_SCALING_FACTOR);
	lts16DGainEffectForRDCM = (int16_t)((int32_t)lts16DGainForRDCM * lts16DeltaRPMErrDiffForRDCM / (int16_t)U16_TCS_GAIN_SCALING_FACTOR);
	lts16PDGainEffectForRDCM = (lts16PGainEffectForRDCM + lts16DGainEffectForRDCM) * -1;
}

static void LCTCS_vCalDeltaRPM(void)
{
	/*=============================================================================*/
	/* Purpose : Calculate Delta RPM					  						   */
	/* Input Variable : 									                       */
	/* Output Variable : 											               */
	/*=============================================================================*/
        #if __REAR_D
    ltu16DrivenWheelAvgSpeed=(uint16_t)((vrad_crt_rl+vrad_crt_rr)/2);
    ltu16NonDrivenWheelAvgSpeed=(uint16_t)((vrad_crt_fl+vrad_crt_fr)/2);
    /*ltu16TireDynamicLength = U16_TIRE_L_R;  */
        #else
    ltu16DrivenWheelAvgSpeed=(uint16_t)((vrad_crt_fl+vrad_crt_fr)/2);
    ltu16NonDrivenWheelAvgSpeed=(uint16_t)((vrad_crt_rl+vrad_crt_rr)/2);  
    /*ltu16TireDynamicLength = U16_TIRE_L;*/   
        #endif	
    /*ltu16TireDynamicLength = 2110; temporary */        
	/* KPH to RPM Conversion Constant : 2083 = 10e6 / 60 / 8(speed resolution)  */    	        
	tcs_tempW1 = (int16_t)( ((uint32_t)ltu16DrivenWheelAvgSpeed * 2083) / (U16_TIRE_L) );	
	tcs_tempW2 = (int16_t)( ((uint32_t)ltu16NonDrivenWheelAvgSpeed * 2083) / (U16_TIRE_L_R) );	
	/* Rear Based 4WD에서 확인 필요 */
    ltu16MainWheelAvgRPM =(uint16_t)( ((uint32_t)tcs_tempW1*U16_PTU_GEAR_RATIO / 1000) );		        
    ltu16SubWheelAvgRPM =(uint16_t)( ((uint32_t)tcs_tempW2*1000 / U16_RDM_GEAR_RATIO) );		            
	lts16DeltaRPMForRDCM = (int16_t)(ltu16MainWheelAvgRPM-ltu16SubWheelAvgRPM);
}
	
static void LCTCS_vCalDeltaRPMError(void)
{
	/*=============================================================================*/
	/* Purpose : Error & Calculate Change Value of Error  						   */
	/* Input Variable : 									                       */
	/* Output Variable : 											               */
	/*=============================================================================*/
	lts16DeltaRPMFiltOldForRDCM = lts16DeltaRPMFiltForRDCM;
    lts16DeltaRPMErrForRDCM = lts16DeltaRPMForRDCM - lts16TargetRPMForRDCM;
	lts16DeltaRPMErrForRDCM = LCTCS_s16ILimitMaximum(lts16DeltaRPMErrForRDCM, (int16_t)U16_TCS_DELTA_RPM_LIMIT);
    tcs_tempW2 = LCTCS_s16ILimitRange(lts16DeltaRPMForRDCM, 0, (int16_t)U16_TCS_DELTA_RPM_LIMIT);
    tcs_tempW2 = tcs_tempW2*100;	
    if(!AUTO_TM)
    {
    	tcs_tempW1=L_U8FILTER_GAIN_10MSLOOP_3HZ;  /*17*/
    }
    else
    {
    	tcs_tempW1=L_U8FILTER_GAIN_10MSLOOP_8HZ; /*35*/  
    }
    lts16DeltaRPMFiltForRDCM = LCESP_s16Lpf1Int( tcs_tempW2, lts16DeltaRPMFiltOldForRDCM, (uint8_t)tcs_tempW1);	
	lts16DeltaRPMErrDiffForRDCM = (lts16DeltaRPMFiltForRDCM - lts16DeltaRPMFiltOldForRDCM)/10;
}

static void LCTCS_vCalPDGainForRDCM(void)
{
	/*=============================================================================*/
	/* Purpose : Torque Rise for RDCM Protection 			   					   */
	/*           																   */
	/* Input Variable : 													       */
	/* Output Variable : 														   */
	/*=============================================================================*/
	if (lts16DeltaRPMErrForRDCM > 0)
	{
		if (lts16DeltaRPMErrDiffForRDCM > 0)
		{
			lts16TorqRiseforRDCM = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_0_0, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_0_1, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_0_2, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_0_3,
		    	                                                 (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_1_0, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_1_1, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_1_2, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_1_3);
	    	lts16TorqRiseforRDCM = -lts16TorqRiseforRDCM;
			RDCM_FTCS_flags = 43;
		}
		else
		{
			lts16TorqRiseforRDCM = 0;
			RDCM_FTCS_flags = 44;
		}	
	}
	else
	{
			lts16TorqRiseforRDCM = LCTCS_s16IInterpDepAccel2by4( (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_0_0, (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_0_1, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_0_2, (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_0_3,
		    	                                                 (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_1_0, (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_1_1, (int16_t)U8_TCS_TORQ_DEC_RATE_RDCM_1_2, (int16_t)U8_TCS_TORQ_INC_RATE_RDCM_1_3);
		RDCM_FTCS_flags = 45;
	}
}

static void LCTCS_vCallDetectRDCMProtCtrl(void)
{
	/*=============================================================================*/
	/* Purpose : RDCM Protection Detection(Only Engine Control)		   			   */
	/*           													               */
	/* Input Variable : 													       */
	/* Output Variable : 														   */
	/*=============================================================================*/		
	if (lts16DeltaRPMErrForRDCM > 0)
	{
		ltu1RDCMActReference = 1;
	}
	else
	{
		;
	}	
    if((ltu8RDCMStatus==0)&&(ltu1RDCMProtectionReq==1)
    	&&(ltu1RDCMActReference==1)&&((mtp>(int16_t)S8_COMPLETE_CLOSE_THROTTLE_POS)||(lespu1SccActFlg==1))
    	&&(ENG_STALL==0)&&(lts16TargetRPMForRDCM<254))  
    {
		if( (ABS_fz==1) || (EBD_RA==1)
				#if __VDC
			|| (YAW_CDC_WORK==1) || (ESP_TCS_ON==1) || (PBA_ON==1)
			 	#endif
			 	#if __CBC
			|| (CBC_ON==1)
				#endif
			 	#if __TSP
			|| (TSP_ON==1)
				#endif
			 	#if __SLS
			|| (SLS_ON==1)
				#endif
			 	#if __ROP
			|| (ROP_REQ_ACT==1) || (ROP_REQ_ENG==1)
				#endif
			 	#if __HRB
			|| (lchrbu1ActiveFlag==1)
				#endif    
			 	#if __HSA
			|| (HSA_flg==1)
				#endif     						    			    			    			
			 	#if __HDC
			|| (lcu1HdcActiveFlg==1)
				#endif     	
		)
		{
			ltu1RDCMProtENGOn = 0;
			ltu1RDCMProtBRKOn = 0;
		}
		else
		{
			ltu1RDCMProtENGOn = 1;
			ltu1RDCMProtBRKOn = 1;
			ltu8RDCM1stControlCount = ltu8RDCM1stControlCount +1;
			ltu8RDCM1stControlCount = LCTCS_s16ILimitRange((int16_t)ltu8RDCM1stControlCount, 0, 250);			
		}
	}  					
    else
    {
		ltu1RDCMProtENGOn = 0;
		ltu1RDCMProtBRKOn = 0;
		if (ENG_STALL==1)
		{
			ltu1RDCMProtENGSustain1Sec = 0;
		}
		else{}	
    }				
	LCTCS_vFailManagementForRDCM();	
	LCTCS_vCount1SecAfterRDCM();
}

static void LCTCS_vFailManagementForRDCM(void)
{
	/*=============================================================================*/
	/* Purpose : RDCM Fail management					 			   			   */
	/* Input Variable : 													       */
	/* Output Variable : 														   */
	/*=============================================================================*/	
	if( (fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)
		||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||(fu1SameSideWSSErrDet==1)
		||(fu1DiagonalWSSErrDet==1)||(fu1ESCSolErrDet==1)
		||(fu1FrontSolErrDet==1)||(fu1RearSolErrDet==1)||(fu1ECUHwErrDet==1)
		||(fu1MotErrFlg==1)
	    ||(fu1VoltageLowErrDet==1)||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)
	    ||(fu8EngineModeStep==0)||(fu8EngineModeStep==1)
		  #if __BRAKE_FLUID_LEVEL==ENABLE
		||(fu1DelayedBrakeFluidLevel==1)
		||(fu1LatchedBrakeFluidLevel==1)				
		  #endif
		/* AWD Fail 반영 필요 */	
	  )
	{
		ltu1RDCMProtENGOn = 0;
		ltu1RDCMProtBRKOn = 0;
		ltu1RDCMProtENGSustain1Sec = 0;
		
	}
	else
	{
		;
	}

	if((fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1))
	{
		ltu1RDCMProtENGOn = 0;
		ltu1RDCMProtENGSustain1Sec = 0;
	}
	else
	{
		;
	}
					
	if(tc_error_flg==1)
	{
		ltu1RDCMProtBRKOn = 0;
	}
	else
	{
		;
	}		

	if((fu1ESCDisabledBySW==1)||(fu1TCSDisabledBySW==1)||(fu1YawErrorDet==1)||(fu1BLSErrDet==1)||(fu1AyErrorDet==1)
	#if (__AX_SENSOR == ENABLE)
	||(fu1AxErrorDet==1)
	#endif
	||(fu1MCPErrorDet==1)||(fu1StrErrorDet==1)||(fu1ESCSwitchFail==1)
	||(fu1FWDTimeOutErrDet==1)||(fu1TCUTimeOutErrDet==1))
	{
		;
	}
	else
	{
		;
	}
	
	if(ltu1RDCMProtENGOn==0)
	{
		ltu1RDCMActReference = 0;
		ltu8RDCM1stControlCount = 0;
	}
	else
	{
		;
	}		
}

static void LCTCS_vCount1SecAfterRDCM(void)
{
	/*=============================================================================*/
	/* Purpose : After Clear RDCM Protection Engine Control, 1sec Control Set	   */
	/* 																			   */
	/* Input Variable : 														   */
	/* Output Variable :              											   */
	/*=============================================================================*/
	if(ltu1RDCMProtENGOn==1)
	{
		ltcsu81SecCntAfterRDCM = L_U8_TIME_10MSLOOP_1000MS;
		ltu1RDCMProtENGSustain1Sec = 1;
	}
	else
	{
		if (ltcsu81SecCntAfterRDCM>0)
		{
			ltcsu81SecCntAfterRDCM--;
		}
		else
		{
			ltu1RDCMProtENGSustain1Sec = 0;
			ltcsu81SecCntAfterRDCM = 0;
		}
	}
}	

static void LCTCS_vControlTractionforRDCM(void)
{
	/*=============================================================================*/
	/* Purpose : Calculate RDCM Engine Control Torque Command Value				   */
	/* 			 																   */
	/* Input Variable : 														   */
	/* Output Variable :              											   */
	/*=============================================================================*/
	if (ltu1RDCMProtENGOn == 0)
	{
		RDCM_cmd = drive_torq; 
		/*Consider in TCS Non Control*/
	}
	else if((ltu1RDCMProtENGOn == 1)&&(ltu8RDCM1stControlCount == 1))
	{
		RDCM_cmd = LCESP_s16IInter4Point(vref, (int16_t)U16_TCS_SPEED1, S16_TCS_1ST_SCAN_TRQ_RDCM_0,
									    	   (int16_t)U16_TCS_SPEED2, S16_TCS_1ST_SCAN_TRQ_RDCM_1,
		                                       (int16_t)U16_TCS_SPEED3, S16_TCS_1ST_SCAN_TRQ_RDCM_2,
		                                       (int16_t)U16_TCS_SPEED4, S16_TCS_1ST_SCAN_TRQ_RDCM_3);
		RDCM_cmd = LCTCS_s16IFindMinimum( RDCM_cmd , eng_torq - 30 );   		                                       
	}
	else
	{
		RDCM_cmd = RDCM_cmd + lts16TorqRiseforRDCM;		
		RDCM_cmd = LCTCS_s16ILimitRange(RDCM_cmd, lts16RDCMOKTorq, S16_TCS_ENG_TORQUE_MAX);
	}
}	

static void LCTCS_vDecideMinTorqLvlforRDCM(void)
{
	lts16RDCMOKTorq = LCTCS_s16IInterpDepRPM3by3( (int16_t)S16_RDCM_OK_TORQ_0_0, (int16_t)S16_RDCM_OK_TORQ_0_1, (int16_t)S16_RDCM_OK_TORQ_0_2, 
												  (int16_t)S16_RDCM_OK_TORQ_1_0, (int16_t)S16_RDCM_OK_TORQ_1_1, (int16_t)S16_RDCM_OK_TORQ_1_2, 
												  (int16_t)S16_RDCM_OK_TORQ_2_0, (int16_t)S16_RDCM_OK_TORQ_2_1, (int16_t)S16_RDCM_OK_TORQ_2_2);                	
}

#endif /* __RDCM_PROTECTION */


