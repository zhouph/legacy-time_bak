#ifndef LSENGCALLSENSORSIGNAL_H
#define LSENGCALLSENSORSIGNAL_H

/*includes**************************************************/
#include "LVarHead.h"


/*Global MACRO CONSTANT Definition******************************************************/

#if defined(HMC_CAN)  

	#define __CAN_MAX_TQ_AVAILABLE    
    #define __CAN_DRV_TQ_FILTERING
    #define __CAN_DRV_TQ_SELECTION
	#define __CAN_ENG_RPM_FILTERING
	#define __CAN_MTP_FILTERING
	#define __CAN_ACCEL_PDL_TRAVL_AVAILABLE
  #if (__HMC_CAN_VER!=CAN_1_2) && (__HMC_CAN_VER!=CAN_YF_HEV) && (__HMC_CAN_VER!=CAN_HD_HEV) && (__HMC_CAN_VER!=CAN_EV) && (__HMC_CAN_VER!=CAN_FCEV)      
	#define __CAN_THROTTLE_ANGLE_AVAILABLE
  #endif	

	#define __CAN_MAX_TQ_SCALING
	#define U8_HIGH_POWER_ENG_VOL      50

  #if (__HMC_CAN_VER==CAN_YF_HEV)
	#define __CAN_HYBRID_VEHICLE_TQ
  #elif (__HMC_CAN_VER==CAN_HD_HEV)	
  	#define __CAN_HYBRID_VEHICLE_TQ_NON_AHB
  	#define __CAN_TM_GEAR_POS_NOT_AVAILABLE
  #elif (__HMC_CAN_VER==CAN_EV) || (__HMC_CAN_VER==CAN_FCEV)
	#define __CAN_ELECTRIC_VEHICLE_TQ
	#define __CAN_ENG_RPM_NOT_AVAILABLE
	#define __CAN_TC_RPM_NOT_AVAILABLE
  #endif
  
  #if (__HMC_CAN_VER==CAN_HD_HEV)
  	#define __ETC_TYPE_VEHICLE			0
  	#define U16_TCS_TQI_MAX_TORQUE		MAX_TORQUE
  	#define U16_TCS_TQI_DDEFAULT_TORQUE	0x3fc
  #elif (__HMC_CAN_VER==CAN_FCEV) || (__HMC_CAN_VER==CAN_EV)
  	#define __ETC_TYPE_VEHICLE			0
  	#define U16_TCS_TQI_MAX_TORQUE		MAX_TORQUE
  	#define U16_TCS_TQI_DDEFAULT_TORQUE	0x7c9
  #elif (__HMC_CAN_VER==CAN_HE_KE)||(__HMC_CAN_VER==CAN_YF_HEV)		
  	#define __ETC_TYPE_VEHICLE			1
  	#define U16_TCS_TQI_MAX_TORQUE		0x7c8
  	#define U16_TCS_TQI_DDEFAULT_TORQUE	0x7c9  	
  #else
  	#define __ETC_TYPE_VEHICLE			0
  	#define U16_TCS_TQI_MAX_TORQUE		1000
  	#define U16_TCS_TQI_DDEFAULT_TORQUE	0x3fc  	
  #endif	  

#elif defined(SSANGYONG_CAN)

	#define __CAN_ACCEL_PDL_TRAVL_AVAILABLE
	
  #if (__CAR==SYC_W190)||(__CAR==SYMC_Q150)||(__CAR==SYMC_Q160)||(__CAR==SYC_Y295)||(__CAR==SYC_A160)
	#define __CAN_TM_GEAR_SW_NOT_AVAILABLE
  #endif
    
    /*#define __CAN_FRC_TQ_SELECTION*/  /*2013.11.15 X100 이후 기존 SYMC 모든 차종 TQFR 사용*/
    #define __DIESEL_VEHICLE			(lespu8MS_VEH_CODE_ENG == 0x33)
  
#elif (GMLAN_ENABLE==ENABLE) 

	#define __CAN_ACCEL_PDL_TRAVL_AVAILABLE

#elif (__CAR==CHINA_B12)

	#define __CAN_THROTTLE_ANGLE_AVAILABLE
	#define __AUTO_TM_VEHICLE

#elif defined(TEST_CAR_CAN)

	#define __CAN_ACCEL_PDL_TRAVL_AVAILABLE

  #if (__CAR==RSM_QM5)
	#define __CAN_TM_GEAR_SW_NOT_AVAILABLE
  #endif

  #if (__CAR==GM_C100) || (__CAR==GM_TAHOE) || (__CAR==RSM_QM5) || (__CAR==DCX_COMPASS) || (__CAR==PSA_C4) || (__CAR==GM_LAMBDA) || (__CAR==GM_J300) || (__CAR==GM_MALIBU) || (__CAR==GM_XTS)||(__CAR==GM_T300)||(__CAR==VV_V40)
	#define __AUTO_TM_VEHICLE
  #endif	

  #if (__CAR==BYD_5A)
  	#define __ETC_TYPE_VEHICLE			0
  	#define U16_TCS_TQI_MAX_TORQUE		MAX_TORQUE
  	#define U16_TCS_TQI_DDEFAULT_TORQUE	0xFF 
  #endif
  
  #if (__CAR == FIAT_PUNTO)
  	#define __CAN_MAX_TQ_AVAILABLE
  #endif

#else

	#define __CAN_ACCEL_PDL_TRAVL_AVAILABLE

#endif


	#if (__CAR==KMC_UN) || (__CAR==KMC_XM) || (__CAR==KMC_AM)  || (__CAR==KMC_TD) || (__CAR == GM_C100) || (__CAR == HMC_JM) || (__CAR == HMC_NF)
#define __USE_ABS_TORQ		0		
	#else
#define __USE_ABS_TORQ		1
	#endif

	#if ((GMLAN_ENABLE==ENABLE)||(__CAR==GM_J300)||(__CAR==GM_T300)||(__CAR == GM_MALIBU) || (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_TAHOE)||(__CAR == GM_LAMBDA)||(__CAR==GM_SILVERADO))
#define __GM_VEHICLE		1
	#else
#define __GM_VEHICLE		0
	#endif
	
	#if ((__CAR==FORD_C214)||(__CAR==CHINA_B12)||(__CAR==BYD_5A)||(__CAR==PSA_C4) || (__CAR==PSA_C4_MECU) || (__CAR==PSA_C3) ||(__CAR==DCX_COMPASS)||(__CAR==DCX_COMPASS_MECU)||(__CAR==RSM_QM5)||(__CAR==RSM_QM5_MECU)||(__CAR==BMW740i)||(__CAR==VV_V40)||(__CAR==FIAT_PUNTO))
#define __DEMO_VEHICLE		1
	#else
#define __DEMO_VEHICLE		0
	#endif		
	
	#if ((__HMC_CAN_VER==CAN_FCEV)||(__HMC_CAN_VER==CAN_EV))
#define __TCS_GEAR_SHIFT_CONTROL	0
	#else
#define __TCS_GEAR_SHIFT_CONTROL	1
	#endif	

    #if ((__CAR == PSA_C3) || (__CAR == VV_V40))
#define __TCS_ALLOW_NEGATIVE_TQ   ENABLE
    #else
#define __TCS_ALLOW_NEGATIVE_TQ   DISABLE
    #endif

/*Global MACRO FUNCTION Definition******************************************************/
	#if (__CAR==KMC_UN) || (__CAR==KMC_XM) || (__CAR==KMC_AM)  || (__CAR==KMC_TD)
#define __EDC_CONV_ABS_TORQ		0		
	#elif __HMC_CAN_VER==CAN_1_2||__HMC_CAN_VER==CAN_1_3||__HMC_CAN_VER==CAN_1_4||__HMC_CAN_VER==CAN_1_6||__HMC_CAN_VER==CAN_2_0||__HMC_CAN_VER==CAN_HD_HEV	|| __CAR == FIAT_PUNTO
#define __EDC_CONV_ABS_TORQ		1	
	#elif __CAR == GM_C100
#define __EDC_CONV_ABS_TORQ		0	
	#else
#define __EDC_CONV_ABS_TORQ		0
	#endif

/*Global Type Declaration *******************************************************************/


/*Global Extern Variable  Declaration*******************************************************************/

extern int16_t lscans16DrvTqSelection;
/*Global Extern Functions  Declaration******************************************************/
extern void LSENG_vCallSensorSignal(void);
extern void LSCAN_vProcessCANSignal(void);

#endif
