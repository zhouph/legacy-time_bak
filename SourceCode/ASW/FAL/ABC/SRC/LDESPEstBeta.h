#ifndef __LDESPESTBETA_H__
#define __LDESPESTBETA_H__

/* Includes ************************************************************/
#include "LVarHead.h"

#define __YAW_FILTER_FOR_BETA DISABLE //ENABLE DISABLE

#if __VDC && __MGH40_Beta_Estimation
/*Global Type Declaration **********************************************/

typedef struct
{
    uint16_t beta_flag_bit7     :1;
    uint16_t beta_flag_bit6     :1;
    uint16_t beta_flag_bit5     :1;
    uint16_t beta_flag_bit4     :1;
    uint16_t beta_flag_bit3     :1;
    uint16_t beta_flag_bit2     :1;
    uint16_t beta_flag_bit1     :1;
    uint16_t beta_flag_bit0     :1;
}BETA_ESTIMATION_FLAG;

#define Beta_Mu_Saturation                      ldu8BetaEstFlg0.beta_flag_bit7
#define Beta_Reset_Condition                    ldu8BetaEstFlg0.beta_flag_bit6
#define Beta_Reset_Need_Flag                    ldu8BetaEstFlg0.beta_flag_bit5
#define Beta_Bank_Comp_Flag                     ldu8BetaEstFlg0.beta_flag_bit4
#define Beta_Bank_Susp_Flag                     ldu8BetaEstFlg0.beta_flag_bit3
#define Beta_Sensor_Offset_Detect_Flag          ldu8BetaEstFlg0.beta_flag_bit2
#define Beta_Sensor_Offset_Suspect_Flag         ldu8BetaEstFlg0.beta_flag_bit1
#define Bank_Susp_Flag                          ldu8BetaEstFlg0.beta_flag_bit0

#define Bank_Comp_Flag                          ldu8BetaEstFlg1.beta_flag_bit7
#define Bank_Susp_MGH25_Flag                    ldu8BetaEstFlg1.beta_flag_bit6
#define Bank_Comp_MGH25_Flag                    ldu8BetaEstFlg1.beta_flag_bit5
#define MedLowMu_Suspect_Flag                   ldu8BetaEstFlg1.beta_flag_bit4
#define MedLowMu_Suspect_Flag1                  ldu8BetaEstFlg1.beta_flag_bit3
#define MedLowMu_Suspect_Flag2                  ldu8BetaEstFlg1.beta_flag_bit2
#define MedLowMu_Suspect_Flag3                  ldu8BetaEstFlg1.beta_flag_bit1
#define Beta_Brake_Condition_Flag               ldu8BetaEstFlg1.beta_flag_bit0

#define MedLowMu_Detect_BySlip_Flag             ldu8BetaEstFlg2.beta_flag_bit7
#define MedLowMu_Detect_BySlip_Flag1            ldu8BetaEstFlg2.beta_flag_bit6
#define MedLowMu_Detect_BySlip_Flag2            ldu8BetaEstFlg2.beta_flag_bit5
#define MedLowMu_Detect_BySlip_Flag3            ldu8BetaEstFlg2.beta_flag_bit4
#define Beta_Small_Sensor_Offset_Flag           ldu8BetaEstFlg2.beta_flag_bit3
#define ldespu1BankLowerSuspectFlag             ldu8BetaEstFlg2.beta_flag_bit2
#define ldescu1BankYCWFlag                      ldu8BetaEstFlg2.beta_flag_bit1
#define beta_flag_not_used2                     ldu8BetaEstFlg2.beta_flag_bit0

#define ldescu1DetDynBank                       ldu8BetaEstFlg3.beta_flag_bit7
#define beta_flag_not_used3                     ldu8BetaEstFlg3.beta_flag_bit6  /* ldescu1SlipMuDetectionFL                ldu8BetaEstFlg3.beta_flag_bit6 */    /*11.10.07.hbjeon*/
#define beta_flag_not_used4                     ldu8BetaEstFlg3.beta_flag_bit5  /* ldescu1SlipMuDetectionFR                ldu8BetaEstFlg3.beta_flag_bit5 */    /*11.10.07.hbjeon*/
#define beta_flag_not_used5                     ldu8BetaEstFlg3.beta_flag_bit4  /* ldescu1SlipMuDetectionVehicle           ldu8BetaEstFlg3.beta_flag_bit4 */    /*11.10.07.hbjeon*/
#define beta_flag_not_used6                     ldu8BetaEstFlg3.beta_flag_bit3  /* ldescu1SlipMuDetectionFLOld             ldu8BetaEstFlg3.beta_flag_bit3 */    /*11.10.07.hbjeon*/
#define beta_flag_not_used7                     ldu8BetaEstFlg3.beta_flag_bit2  /* ldescu1SlipMuDetectionFROld             ldu8BetaEstFlg3.beta_flag_bit2 */    /*11.10.07.hbjeon*/
#define Bank_K_alatm_ok                         ldu8BetaEstFlg3.beta_flag_bit1
#define Bank_K_Comp_Flag                        ldu8BetaEstFlg3.beta_flag_bit0

/* Global Variables Declaration ****************************************/

/* Global For Other's Use */

extern BETA_ESTIMATION_FLAG ldu8BetaEstFlg0, ldu8BetaEstFlg1, ldu8BetaEstFlg2, ldu8BetaEstFlg3;

extern int16_t Beta_Dyn;
extern int16_t Beta_MD;
extern int16_t Beta_MD_Dot;
extern int16_t Beta_K_Dot;
extern int16_t Vy_MD;
extern int16_t Alpha_front;
extern int16_t Alpha_rear;
extern int16_t Bank_Angle_Final;
extern int16_t TF_Bank_Yaw,TF_Bank_Ay;
  
  /* Global For Logging */
  
extern int16_t Beta_MD_Front;
extern int16_t Linear_Gain_F;
extern int16_t Alpha_Gain;
extern int16_t Beta_Mu;
extern int16_t Beta_K_Dot2;
extern int16_t Bank_Angle_Yaw;
extern int16_t Bank_Angle_Ay;
extern int16_t Bank_Angle_K, Bank_Angle_K_raw;
extern int16_t Bank_Angle_K_Dot;
extern int16_t Dynamic_Factor;
extern int16_t Dynamic_Factor_Dot;
/* extern int16_t ldescs16SlipMuVehicle; */   /* 11.10.07.hbjeon */
extern int16_t ldescs16DynBankCounter;

extern int16_t Dynamic_Factor_Total;
extern int16_t alat_LPF3_bank,alat_LPF3_bank_old;
extern int16_t ldesps16BankModelDiff;

#if (__BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE)
	extern int16_t ldesps16BankLowerSuspectCounter;
#endif
 
#if (__BANK_DFC_CHK == ENABLE)
	extern int16_t lsesps16DynamicFactorTotalFilt;
	extern int16_t ldesps16ModelLatAccel;
	extern int16_t ldesps16ModelLatAccelFilt;
	extern int16_t ldesps16FiltedYawm;
	extern int16_t ldesps16VrefMulYawmBank;
	extern int16_t ldesps16alatLPF3Bank;
#endif

#if (__BANK_YCW == ENABLE)
	extern int16_t ldescs16BankDctValidCNT;
#endif

extern int16_t ldesps16BankAlatRollGain;
extern int16_t ldesps16BankYawFilter;
  
/* Global Function Declaration *****************************************/

extern void LDESP_vEstBeta(void);


#endif
#endif
