#ifndef _LAESPACTUATETCMFESC_
#define _LAESPACTUATETCMFESC_

#if __ESC_TCMF_CONTROL
/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/
#define	U8_TCMF_FADE_SLOW_YAWM_HM	S16_ESC_RESERVED_01 //1000
#define U8_TCMF_FADE_SLOW_YAWM_MM	S16_ESC_RESERVED_02 //1000
#define U8_TCMF_FADE_SLOW_YAWM_LM	S16_ESC_RESERVED_03 //1000

#define	U8_TCMF_FADE_OUT_FULL_HM	S16_ESC_RESERVED_04 //500
#define	U8_TCMF_FADE_OUT_FULL_MM	S16_ESC_RESERVED_05 //500
#define	U8_TCMF_FADE_OUT_FULL_LM	S16_ESC_RESERVED_06 //500
                                    
#define U8_TCMF_FADE_DUTY_DEC_SLOW	S16_ESC_RESERVED_07 //10
#define U8_TCMF_FADE_DUTY_DEC_FAST	S16_ESC_RESERVED_08 //50

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern uint8_t lcespu8TCMFControlWheel;
/*Global Extern Functions Declaration ******************************************/
extern int16_t LAMFC_s16SetMFCCurESP_TCMF_Pri(uint8_t CIRCUIT, int16_t MFC_Current_old);
extern int16_t LAMFC_s16SetMFCCurESP_TCMF_Sec(uint8_t CIRCUIT, int16_t MFC_Current_old);
extern int16_t LAMFC_s16CalMFCCurrentESP_TCMF(struct ESC_STRUCT *WL_ESC, int16_t MFC_Current_old);
#endif
#endif
