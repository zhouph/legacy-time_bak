#ifndef __LSESPFILTERESPSENSOR_H__
#define __LSESPFILTERESPSENSOR_H__

#include "LVarHead.h"



#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
extern   U8_BIT_STRUCT_t PEDAL0;

#define lsespu1PedalBrakeOn               PEDAL0.bit0
#define lsespu1unused_pedal_1             PEDAL0.bit1
#define lsespu1unused_pedal_2             PEDAL0.bit2
#define lsespu1unused_pedal_3             PEDAL0.bit3
#define lsespu1unused_pedal_4             PEDAL0.bit4
#define lsespu1unused_pedal_5             PEDAL0.bit5
#define lsespu1unused_pedal_6             PEDAL0.bit6
#define lsespu1unused_pedal_7             PEDAL0.bit7

/* C710 PEDAL */
/* TUNING PARAMETER */

#define S16_PEDAL_TRAVEL_1				S16_TRAVEL_3_MM   
#define S16_PEDAL_TRAVEL_2  			S16_TRAVEL_11_MM  
#define S16_PEDAL_TRAVEL_3  			S16_TRAVEL_13_MM  
#define S16_PEDAL_TRAVEL_4  			S16_TRAVEL_16_MM  
#define S16_PEDAL_TRAVEL_5      		S16_TRAVEL_21_MM  
#define S16_PEDAL_TRAVEL_6      		S16_TRAVEL_25_MM 
#define S16_PEDAL_TRAVEL_7     			S16_TRAVEL_28_MM 		
#define S16_PEDAL_TRAVEL_8      		S16_TRAVEL_34_MM
/*#define S16_PEDAL_TRAVEL_MAX            S16_TRAVEL_70_MM */
#define S16_PEDAL_TRAVEL_MAX_TEMP            S16_TRAVEL_70_MM
                                                          
#define S16_PEDAL_TARGET_P_1   	  		MPRESS_1BAR  
#define S16_PEDAL_TARGET_P_2  	  		MPRESS_2BAR 
#define S16_PEDAL_TARGET_P_3  	  		MPRESS_3BAR 
#define S16_PEDAL_TARGET_P_4  	  		MPRESS_5BAR 
#define S16_PEDAL_TARGET_P_5  	  		MPRESS_10BAR
#define S16_PEDAL_TARGET_P_6      		MPRESS_15BAR
#define S16_PEDAL_TARGET_P_7  			MPRESS_20BAR    		
#define S16_PEDAL_TARGET_P_8  			MPRESS_30BAR    		
/*#define S16_PEDAL_TARGET_P_MAX		    MPRESS_150BAR*/
#define S16_PEDAL_TARGET_P_MAX_TEMP		    MPRESS_55BAR

extern int16_t  lsesps16EstBrkPressByBrkPedal;
extern int16_t  lsesps16EstBrkPressByBrkPedalF;

extern uint8_t lsespu8EstBrkPressEnterCnt;
extern uint8_t lsespu8EstBrkPressExitCnt;

#endif

extern void LSESP_vFilterEspSensor(void);

#endif

