/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCACCCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCACCCallControl.h"
#include "LADECCallActHW.h"
#include "LAABSCallMotorSpeedControl.h"
  #if __ACC
#include "LCDECCallControl.h"
#include "LCESPCalInterpolation.h"
  #endif

/* Local Definition  ***********************************************/
  #if __ACC
struct  U8_BIT_STRUCT_t ACC00;

#define     S16_ACC_ENGAGE_SPD              160
#define     S16_ACC_TERMINATE_SPD           40
  #endif

/* Variables Definition*********************************************/
  #if __ACC
 
uint8_t 	lcu8AccEngCtrlReq;
uint16_t    lcu16AccCalTorq;
int16_t     lcs16AccTargetG, lcs16AccTargetGOld, lcs16AdcTGdelaycycle, lcs16AccTargetGIf, lcs16AccBrkLampOnRefCnt;
int8_t   	lcs8AccState;
uint8_t     lcu8AccCtrlReq;
static uint8_t   lcu8AccMpressOffCnt;
static uint8_t   lcu8Acc1SecMtpCnt;
static uint16_t    lcu16AccObserver;
 


  #endif

/* Local Function prototype ****************************************/
  #if __ACC
static void LCACC_vChkInhibition(void);
static void LCACC_vChkDriverInput(void);
static void LCACC_vAdcControl(void);
   #if defined(__ACC_RealDevice_INTERFACE)
static void LCACC_vWithAccEcu(void);
   #endif
static void LCACC_vWithoutAccEcu(void);
static void LCACC_vResetCtrlVariable(void);
static void LCACC_vSetDecelCtrl(uint8_t, uint8_t, uint8_t, int16_t);
static void LCACC_vAdcBrkLampOnReq(void);
  #endif

/* Implementation***************************************************/
  #if __ACC
void LCACC_vCallControl(void) {
    LCACC_vChkInhibition();
    LCACC_vChkDriverInput();

      #if __ADC
    LCACC_vAdcControl();  
    LCACC_vAdcBrkLampOnReq();    
      #elif defined(__ACC_RealDevice_INTERFACE)
    LCACC_vWithAccEcu();
      #else
    LCACC_vWithoutAccEcu();
      #endif
}

#if __ADC
void LCACC_vAdcControl(void)
{
    switch(lcs8AccState)
    {
        /********************************************************/
        case S8_ACC_STANDBY:                /* STANDBY */
            lcu1AccActiveFlg=0;
            if((lcu8AccCtrlReq==0)||(lcu1AccInhibitFlg==1))
            {
                lcs8AccState=S8_ACC_TERMINATION;
                LCACC_vResetCtrlVariable();
            }
            else
            {
                if(lcs16AccTargetG<=0)
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                      #if __TCMF_CONTROL
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #else
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #endif
                }
                else
                {
                    lcu1AccActiveFlg=0;
                    lcs8AccState=S8_ACC_STANDBY;
                }
            }
        break;

        /********************************************************/
        case S8_ACC_ACTIVATION:             /* ACTIVATION */

            if((lcu8AccCtrlReq==0)||(lcu1AccInhibitFlg==1))
            {
                if(lcu1DecSmoothClosingEndFlg==1)
                {
                    lcu1AccActiveFlg=0;
                    lcs8AccState=S8_ACC_TERMINATION;
                    LCACC_vSetDecelCtrl(0,0,0,0);
                }
                else
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                    LCACC_vSetDecelCtrl(0,0,1,0);
                }
            }
            else
            {
                lcu1AccActiveFlg = 1;
                if(lcs16AccTargetG<=0)
                {
                    lcs8AccState = S8_ACC_ACTIVATION;
                      #if __TCMF_CONTROL
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #else
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #endif
                }
                else
                {
                    if(lcu1DecSmoothClosingEndFlg==1)
                    {
                        lcu1AccActiveFlg = 0;
                        lcs8AccState = S8_ACC_TERMINATION;
                        LCACC_vSetDecelCtrl(0,0,0,0);
                    }
                    else
                    {
                        lcu1AccActiveFlg=1;
                        lcs8AccState=S8_ACC_ACTIVATION;
                        LCACC_vSetDecelCtrl(0,0,1,0);
                    }
                }
            }

            break;

        /********************************************************/
        case S8_ACC_TERMINATION:                /* TERMINATION */
            lcu1AccActiveFlg = 0;

            if((lcu8AccCtrlReq==1)&&(lcu1AccInhibitFlg==0))
            {
                lcs8AccState=S8_ACC_STANDBY;
                LCACC_vResetCtrlVariable();
            }
            else
            {
                lcs8AccState=S8_ACC_TERMINATION;
            }

            break;

        /********************************************************/
        default:                            /* DEFAULT */
            lcu1AccActiveFlg=0;
            lcs8AccState=S8_ACC_TERMINATION;

            break;
    }
}
  #endif

  #if defined(__ACC_RealDevice_INTERFACE)
void LCACC_vWithAccEcu(void)
{
    switch(lcs8AccState)
    {
        /********************************************************/
        case S8_ACC_STANDBY:                /* STANDBY */
            lcu1AccActiveFlg=0;
            if((lcu8AccCtrlReq==0)||(lcu1AccInhibitFlg==1))
            {
                lcs8AccState=S8_ACC_TERMINATION;
                LCACC_vResetCtrlVariable();
            }
            else
            {
                if(lcs16AccTargetG<0)
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                      #if __TCMF_CONTROL
                    LCACC_vSetDecelCtrl(1,0,1,(lcs16AccTargetG*10));
                      #else
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #endif
                    lcu8AccEngCtrlReq=0;
                }
                else if(lcs16AccTargetG==0)
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                    LCACC_vSetDecelCtrl(0,0,0,0);
                    lcu8AccEngCtrlReq=1;
                }
                else
                {
                    lcu1AccActiveFlg=0;
                    lcs8AccState=S8_ACC_STANDBY;
                }
            }
        break;

        /********************************************************/
        case S8_ACC_ACTIVATION:             /* ACTIVATION */

            if((lcu8AccCtrlReq==0)||(lcu1AccInhibitFlg==1))
            {
                if(lcu1DecSmoothClosingEndFlg==1)
                {
                    lcu1AccActiveFlg=0;
                    lcs8AccState=S8_ACC_TERMINATION;
                    LCACC_vSetDecelCtrl(0,0,0,0);
                    lcu8AccEngCtrlReq = 0;
                }
                else
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                    LCACC_vSetDecelCtrl(0,0,1,0);
                    lcu8AccEngCtrlRe =0;
                }
            }
            else
            {
                lcu1AccActiveFlg = 1;
                if(lcs16AccTargetG<0)
                {
                    lcs8AccState = S8_ACC_ACTIVATION;
                      #if __TCMF_CONTROL
                    LCACC_vSetDecelCtrl(1,0,1,(lcs16AccTargetG*10));
                      #else
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #endif
                    lcu8AccEngCtrlReq=0;
                }
                else if(lcs16AccTargetG==0)
                {
                    lcs8AccState = S8_ACC_ACTIVATION;
                    LCACC_vSetDecelCtrl(0,0,0,0);
                    lcu8AccEngCtrlReq = 1;
                }
                else
                {
                    if(lcu1DecSmoothClosingEndFlg==1)
                    {
                        lcu1AccActiveFlg = 0;
                        lcs8AccState = S8_ACC_TERMINATION;
                        LCACC_vSetDecelCtrl(0,0,0,0);
                        lcu8AccEngCtrlReq=0;
                    }
                    else
                    {
                        lcu1AccActiveFlg=1;
                        lcs8AccState=S8_ACC_ACTIVATION;
                        LCACC_vSetDecelCtrl(0,0,1,0);
                        lcu8AccEngCtrlReq=0;
                    }
                }
            }

            break;

        /********************************************************/
        case S8_ACC_TERMINATION:                /* TERMINATION */
            lcu1AccActiveFlg = 0;

            if((lcu8AccCtrlReq==1)&&(lcu1AccInhibitFlg==0))
            {
                lcs8AccState=S8_ACC_STANDBY;
                LCACC_vResetCtrlVariable();
            }
            else
            {
                lcs8AccState=S8_ACC_TERMINATION;
            }

            break;

        /********************************************************/
        default:                            /* DEFAULT */
            lcu1AccActiveFlg=0;
            lcs8AccState=S8_ACC_TERMINATION;

            break;
    }
}
  #endif


  #if !defined(__ACC_RealDevice_INTERFACE)
void LCACC_vWithoutAccEcu(void)
{
    switch(lcs8AccState)
    {
        /********************************************************/
        case S8_ACC_STANDBY:                /* STANDBY */
            lcu1AccActiveFlg=0;

            if((lcu1AccSwitchFlg==0)||(lcu1AccInhibitFlg==1))
            {
                lcs8AccState=S8_ACC_TERMINATION;
                LCACC_vResetCtrlVariable();
                lcu8Acc1SecMtpCnt=0;
                lcu16AccObserver=4;
            }
            else if((MPRESS_BRAKE_ON==0)&&(mtp<MTP_3_P)&&(lcs16AccTargetG<=0)&&(vref>S16_ACC_ENGAGE_SPD))
            {

                if(lcu8Acc1SecMtpCnt<L_U8_TIME_10MSLOOP_1000MS)
                {
                    lcu8Acc1SecMtpCnt++;
                    lcu16AccObserver=5;
                }
                else
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;
                      #if __TCMF_CONTROL
                    LCACC_vSetDecelCtrl(1,0,1,(lcs16AccTargetG*10));
                      #else
                    LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                      #endif
                    lcu16AccObserver=6;
                }
            }
            else
            {
                lcs8AccState=S8_ACC_STANDBY;
                lcu8Acc1SecMtpCnt=0;
                lcu16AccObserver=7;
            }
            break;

        /********************************************************/
        case S8_ACC_ACTIVATION:             /* ACTIVATION */
            lcu1AccActiveFlg = 1;
              #if __TCMF_CONTROL
            tempW7=(int16_t)(abs(lcs16AccTargetG*8)/10)+S16_ACC_TERMINATE_SPD;
              #else
            tempW7=S16_ACC_TERMINATE_SPD;
              #endif

            if((vref<=tempW7)||(lcu1AccSwitchFlg==0)||(lcu1AccInhibitFlg==1)||(mtp>=MTP_5_P))
            {
                if(lcu1DecSmoothClosingEndFlg==1)
                {
                    lcu1AccActiveFlg=0;
                    lcs8AccState=S8_ACC_TERMINATION;
                    LCACC_vSetDecelCtrl(0,0,0,0);
                    lcu16AccObserver=8;
                }
                else
                {
                    lcu1AccActiveFlg=1;
                    lcs8AccState=S8_ACC_ACTIVATION;

                    LCACC_vSetDecelCtrl(0,0,1,0);
                    lcu16AccObserver=9;
                }
            }
            else if(MPRESS_BRAKE_ON==1)
            {
                lcu1AccActiveFlg=0;
                lcs8AccState=S8_ACC_STANDBY;
                LCACC_vResetCtrlVariable();
                lcu16AccObserver=10;
            }
            else
            {
                lcs8AccState=S8_ACC_ACTIVATION;
                  #if __TCMF_CONTROL
                LCACC_vSetDecelCtrl(1,0,1,(lcs16AccTargetG*10));
                  #else
                LCACC_vSetDecelCtrl(1,1,1,(lcs16AccTargetG*10));
                  #endif
                lcu16AccObserver=11;
            }
            break;

        /********************************************************/
        case S8_ACC_TERMINATION:                /* TERMINATION */
            lcu1AccActiveFlg=0;
            if((lcu1AccSwitchFlg==1)&&(lcu1AccInhibitFlg==0))
            {
                lcs8AccState=S8_ACC_STANDBY;
                LCACC_vResetCtrlVariable();
                lcu16AccObserver=2;
            }
            else
            {
                lcs8AccState=S8_ACC_TERMINATION;
                lcu16AccObserver=3;
            }
            break;

        /********************************************************/
        default:                            /* DEFAULT */
            lcu1AccActiveFlg=0;
            lcs8AccState=S8_ACC_TERMINATION;
            lcu16AccObserver=1;
            break;
    }
}
  #endif


void LCACC_vChkInhibition(void)
{
	#if __GM_FailM
	if ((fu1OnDiag==1)
	    ||(init_end_flg==0)
	    ||(ebd_error_flg==1)
		||(fu1ESCEcuHWErrDet==1)																			/* ESC HW	*/
		||(fu1MCPErrorDet==1)																			/* MP		*/
		||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)	/* Wheel	*/
		||(fu1YawErrorDet==1)																			/* YAW		*/
		||(fu1AyErrorDet==1)																			/*	LAT		*/
		||(fu1MainCanLineErrDet==1)																		/* GMLAN (Vehicle)	*/
																										/* GMLAN (ESC Sensor)	*/
		#if __BRAKE_FLUID_LEVEL
		||(fu1DelayedBrakeFluidLevel==1)
        ||(fu1LatchedBrakeFluidLevel==1)	/* 091030 for Bench Test */
        #endif
        #if __ADC
	    ||(ccu1EmsBrkReqInv==1)
	    #endif
	    ||(wu8IgnStat==CE_OFF_STATE)           
	)
  	#else
	if((fu1OnDiag==1)||(init_end_flg==0)||(can_error_flg==1)||(vdc_error_flg==1)||(wu8IgnStat==CE_OFF_STATE)
	    #if __ADC
	    ||(ccu1EmsBrkReqInv==1)
	    #endif
	)
  	#endif
    {
        lcu1AccInhibitFlg=1;
        LCACC_vResetCtrlVariable();
    }
    else
    {
        lcu1AccInhibitFlg=0;
    }
}

void LCACC_vAdcBrkLampOnReq(void)
{
	if(lcu1AccActiveFlg==1)
	{
	    if(dec_msc_target_vol>MSC_5_V)
	    {
	    	lcs16AccBrkLampOnRefCnt = lcs16AccBrkLampOnRefCnt + 7;
	    }
	    else if(dec_msc_target_vol>MSC_3_V)
	    {
	    	lcs16AccBrkLampOnRefCnt = lcs16AccBrkLampOnRefCnt + 5;
	    }
	    else if(dec_msc_target_vol>MSC_1_V)
	    {
	    	lcs16AccBrkLampOnRefCnt = lcs16AccBrkLampOnRefCnt + 3;
	    }
	    else if(dec_msc_target_vol>MSC_0_V)
	    {
	    	lcs16AccBrkLampOnRefCnt = lcs16AccBrkLampOnRefCnt + 1;
	    }
	    else
	    {
	    	;
	    }
	    
	    if(lcs16AccBrkLampOnRefCnt>20000)
	    {
	    	lcs16AccBrkLampOnRefCnt = 20000;
	    }
	    else { }
	}
	else
	{
		lcs16AccBrkLampOnRefCnt = 0;
	}
	
	if((lcu1AccActiveFlg==1)&&(lcs16AccBrkLampOnRefCnt>=S16_ADC_BRK_LAMP_ON_REF_TIMER))
	{
		lcu1AdcBrkLampOnReq = 1;
	}
	else
	{
		lcu1AdcBrkLampOnReq = 0;
	}
}

void LCACC_vChkDriverInput(void)
{
#if __ADC
	  
	  #if __ADC_G_TEST
	lcs16AccTargetGOld = lcs16AccTargetG;
	if(vref<=VREF_40_KPH)
	{
		if(lcs16AccTargetG <-10)
		{
			lcu8AccCtrlReq = 1;
			lcs16AccTargetG = lcs16AccTargetG + 1;
		}
		else
		{
			lcu8AccCtrlReq = 0;
			lcs16AccTargetG = 0;
		}
	}
	else if((vref<VREF_100_KPH)&&(mtp<1))
	{
		lcu8AccCtrlReq = 1;
		lcs16AccTargetG = -30;
	}	 
	else
	{
		lcu8AccCtrlReq = 0;
		lcs16AccTargetG = 0;
	}	 
	  
	  #else
	lcu1AccCtrlReqIf = lcanu1CF_Ems_BrkReq;
    lcs16AccTargetGIf = lcans16CF_Ems_DecelReq;
    
	lcs16AccTargetGOld = lcs16AccTargetG;
	lcu8AccCtrlReq  = lcu1AccCtrlReqIf;
	lcs16AccTargetG = lcs16AccTargetGIf/10;
	
	if(lcs16AccTargetG < S16_ADC_TARGET_G_MAX_LIMIT)
	{
		lcs16AccTargetG = S16_ADC_TARGET_G_MAX_LIMIT;
	}
	else { }
	  #endif
	  
	if(lcu8AccCtrlReq==1)
	{
		if(lcs16AdcTGdelaycycle<S16_ADC_TARGET_G_UPDATE_CYCLE) {lcs16AdcTGdelaycycle++;}
		else {lcs16AdcTGdelaycycle = 0;}
		
		if(lcs16AccTargetGOld==0)
		{
			if(lcs16AccTargetG<S16_ADC_TARGET_G_INIT_MAX)
			{
				lcs16AccTargetG = S16_ADC_TARGET_G_INIT_MAX;
			}
			else
			{
				;
			}
		}
		else
		{
			if((lcs16AccTargetGOld-lcs16AccTargetG)>S16_ADC_TARGET_G_JUMP_LIMIT)
			{
				if(lcs16AdcTGdelaycycle==S16_ADC_TARGET_G_UPDATE_CYCLE)
				{
				    lcs16AccTargetG = lcs16AccTargetGOld - S16_ADC_TARGET_G_JUMP_LIMIT;
				}
				else
				{
					lcs16AccTargetG = lcs16AccTargetGOld;
				}
			}
			else 
			{
				;
			}
		}
	}
	else
	{
		lcs16AdcTGdelaycycle = 0;
	}

      #elif defined(__CAN_SW)	
    lcu1AccSwitchFlg=(uint8_t)DECEL_CAN_SWITCH;
    if(Target_DECEL_G>178)
    {
        lcs16AccTargetG=-50;
    }
    else if(Target_DECEL_G>128)
    {
        lcs16AccTargetG=128-(int16_t)Target_DECEL_G;
    }
    else
    {
        lcs16AccTargetG=0;
    }
      #elif defined(__ACC_RealDevice_INTERFACE)
    lcu8AccCtrlReq=ccu1_ACC420ACC_REQ;
    if(ccu8_ACC420TargetDecel>178)
    {
        lcs16AccTargetG=-50;
    }
    else if(ccu8_ACC420TargetDecel>128)
    {
        lcs16AccTargetG=128-(int16_t)ccu8_ACC420TargetDecel;
    }
    else
    {
        lcs16AccTargetG=0;
    }
    lcu16AccCalTorq=ccu8_ACC420DesiredEngineTorqueRate;
      #endif
}


void LCACC_vResetCtrlVariable(void)
{
    lcu1AccActiveFlg=0;
    lcu8AccEngCtrlReq=0;
    lcu8AccMpressOffCnt=0;
    LCACC_vSetDecelCtrl(0,0,0,0);
}


void LCACC_vSetDecelCtrl(uint8_t lcu8AccDecReq,uint8_t lcu8AccDecFfReq,uint8_t lcu8AccDecClosingReq,int16_t lcs16AccDecTargetG)
{
    clDecAcc.lcu8DecCtrlReq=lcu8AccDecReq;
    clDecAcc.lcu8DecFeedForwardReq=lcu8AccDecFfReq;
    clDecAcc.lcu8DecSmoothClosingReq=lcu8AccDecClosingReq;
    clDecAcc.lcs16DecReqTargetG=lcs16AccDecTargetG;
}
  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCACCCallControl
	#include "Mdyn_autosar.h"
#endif  
