
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCalVehicleModel
	#include "Mdyn_autosar.h"
#endif
#include "LDESPCalVehicleModel.h"
#include "LDESPEstBeta.h"
#include "LCESPCalInterpolation.h"
#include "LCallMain.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"
#include "LDROPCallDetection.h"

#include "LSESPCalSensorOffset.h"
#include "LDABSCallEstVehDecel.H"
#include "LCABSCallControl.h"
#include "LDESPDetectVehicleStatus.h"
#include "LDRTACallDetection.h"
#include "LSABSCallSensorSignal.h"
#include "LDABSCallVrefMainFilterProcess.H"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LSABSCall2ndOrderLpf.h"
#include "LSESPFilterEspSensor.h"


#if	__VDC

void LDESP_vCalVehicleModel(void);

#if !__MODEL_CHANGE
	static void LDESP_vCalYawRateDesired(void);
	static void LDESP_vCalYawRateDesiredGain(void);
	static void LDESP_vCalYawRateBicycleModel(void);
	static void LDESP_vCalYawRateDesiredFilter(void);
	static void LDESP_vFilterYawRateDesired(void);
	static void LDESP_vCalLatAccelDesired(void);
	static void LDESP_vCalLatAccelDesiredGain(void);
	static void LDESP_vFilterLatAccelDesired(void);
#endif

#if __MODEL_CHANGE
	static void LDESP_vCalDynBicycleModel(void);
	static void LDESP_vCalYawRateDynGain(void);
#endif

#if  __ADAPTIVE_MODEL_VELO == ENABLE
	void  LDESP_vCalModelVeloCompMain(void);
	void  LDESP_vCalModelVeloRefSpeed(void);
	void  LDESP_vDetModelVeloCheckCondition(void);
	void  LDESP_vCalModelVeloRatio(void);  
	void  LDESP_vDetModelVeloUpdate(void);
	void  LDESP_vDetADMveloReset(void);
	void  LDESP_vDetModelVeloeepReq(void);  
#endif

#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
	void  LDESP_vCalModelVchGENCompMain(void);
	void  LDESP_vDetModelVchGENCheckCondition(void);
	void  LDESP_vCalModelVchGENRatio(void);  
	void  LDESP_vDetModelVchGENUpdate(void);  
	void  LDESP_vDetADMVchGENReset(void);
	void  LDESP_vDetADMVcheepReq(void);  
	/*
	void  LDESP_vCalModelVchSteerCondition(void);
	void  LDESP_vCalModelVchBank(void);
	*/
#endif

int16_t   LDESP_s16CalcSQRT(int16_t s16Input );

#if ( ( __ADAPTIVE_MODEL_VCH_GEN == ENABLE ) || (__ADAPTIVE_MODEL_VELO == ENABLE) )   
	void LDESP_vCalcADMaverageData( int16_t S16ADM_data, int16_t DATA_1SEC_THR, uint8_t U8_ADM_1ST_UPD_TIME, uint8_t U8_ADM_2ND_UPD_TIME, uint8_t U8_ADM_3RD_UPD_TIME );
	void  LDESP_vDetADMInitialization(void);
	void  LDESP_vADMreadEEP(void);
#endif

/*----- Theta Degree Input / Tangent Degree Output  ----

   
   INPUT  =  Tire angle(degree)*100    -> degree*100
   
   OUTPUT = Tangent( Tire angle )*100  -> degree*100


-------------------------------------------------------*/

#define  TAN_X_INP_0       0 
#define  TAN_X_INP_5       500 
#define  TAN_X_INP_10     1000 
#define  TAN_X_INP_15     1500 
#define  TAN_X_INP_20     2000 
#define  TAN_X_INP_25     2500 
#define  TAN_X_INP_30     3000 
#define  TAN_X_INP_35     3500 
#define  TAN_X_INP_40     4000 
#define  TAN_X_INP_45     4500 
#define  TAN_X_INP_50     5000 
#define  TAN_X_INP_55     5500 
#define  TAN_X_INP_60     6000 

#define  TAN_X_OUT_0      0
#define  TAN_X_OUT_5      501
#define  TAN_X_OUT_10     1010
#define  TAN_X_OUT_15     1535
#define  TAN_X_OUT_20     2085
#define  TAN_X_OUT_25     2671
#define  TAN_X_OUT_30     3308
#define  TAN_X_OUT_35     4011
#define  TAN_X_OUT_40     4807
#define  TAN_X_OUT_45     5730
#define  TAN_X_OUT_50     6828
#define  TAN_X_OUT_55     8183
#define  TAN_X_OUT_60     9924


/*------- SQRT INTERPOLATION ---------------------------------------
  sqrt(0.3)=0.547  ==>   input(300) -> output(547) 
  sqrt(0.5)=0.707  ==>   input(500) -> output(707) 
  sqrt(0.8)=0.894  ==>   input(800) -> output(894) 
  sqrt(1)  =1      ==>   input(1000)-> output(1000) 
  sqrt(1.5)=1.225  ==>   input(1500)-> output(1225)
  sqrt(2)  =1.414  ==>   input(2000)-> output(1414)
  sqrt(3)  =1.732  ==>   input(3000)-> output(1732)
  sqrt(4)  =2      ==>   input(4000)-> output(2000)
  sqrt(5)  =2.236  ==>   input(5000)-> output(2236)
--------------------------------------------------------------------*/

/*-- Resolution 1000 --*/
#define  SQRT_X_INP_0G3     300
#define  SQRT_X_INP_0G5     500
#define  SQRT_X_INP_0G8     800
#define  SQRT_X_INP_1G0     1000
#define  SQRT_X_INP_1G5     1500
#define  SQRT_X_INP_2G0     2000
#define  SQRT_X_INP_3G0     3000
#define  SQRT_X_INP_4G0     4000
#define  SQRT_X_INP_5G0     5000
#define  SQRT_X_INP_6G0     6000
#define  SQRT_X_INP_7G0     7000
#define  SQRT_X_INP_8G0     8000
#define  SQRT_X_INP_9G0     9000
#define  SQRT_X_INP_10G0    10000

#define  SQRT_Y_OUT_0G3     547
#define  SQRT_Y_OUT_0G5     707
#define  SQRT_Y_OUT_0G8     894
#define  SQRT_Y_OUT_1G0     1000    
#define  SQRT_Y_OUT_1G5     1225   
#define  SQRT_Y_OUT_2G0     1414  
#define  SQRT_Y_OUT_3G0     1732  
#define  SQRT_Y_OUT_4G0     2000  
#define  SQRT_Y_OUT_5G0     2236
#define  SQRT_Y_OUT_6G0     2449
#define  SQRT_Y_OUT_7G0     2646
#define  SQRT_Y_OUT_8G0     2828
#define  SQRT_Y_OUT_9G0     3000
#define  SQRT_Y_OUT_10G0    3162

#define S16_BETA_CAL_SCALE	50
#define S16_YAW_CAL_SCALE	100
#define S16_DELTA_CAL_SCALE	10

int16_t ldesps16TireAngle;

#if	 ( (__ADAPTIVE_MODEL_VELO == ENABLE )	|| (__ADAPTIVE_MODEL_VCH_GEN ==	ENABLE) )	
	struct ACC_FILTER WSTR_ACC, YAWM_ACC, DEL_YAW_ACC, YAWC_ACC;
	
	/*--- WSTR ACCEL ---*/
    #define s16WSTRFilterFreq  100	 /*	resol 10, 100-->10Hz	 */
    #define s16WSTRFilterZeta  10	 /* resol 10, 10 -->1	 damping coeff	*/	
    #define s16WSTRAccMulti	   1	 /* Diff	multiply		*/
    #define s16WSTRdiffcoefCov 10	 /*	1scan 0.1DEG  => 10DEG/S	 */	
    
    /*--- YAWM ACCEL ---*/	
    #define s16YAWMFilterFreq  10	 /*	resol 10, 10-->	1Hz	 */
    #define s16YAWMFilterZeta  10	 /*	resol 10, 10-->	1	damping	coeff  */	
    #define s16YAWMAccMulti    1	 /*	Diff multiply		 */
    #define s16YAWMdiffcoefCov 10	 /*	1scan 0.1DEG/S^2  => 10 DEG/S^2	*/			
#endif

#if __MODEL_CHANGE

	#define	WSTR_MAX			pEspModel->S16_STEERING_MAX_DEG
	#define S16_ACKERMANN_SPEED	VREF_10_KPH

	int16_t ldesps16GainBetadBeta;
	int32_t ldesps32GainBetadYaw;
	int16_t ldesps16GainBetadDelta;
	int16_t ldesps16GainYawdBeta;
	int16_t ldesps16GainYawdYaw;
	int16_t ldesps16GainYawdDelta;
	int32_t ldesps32BetadBeta;
	int32_t ldesps32BetadYaw;
	int32_t ldesps32BetadDelta;
	int32_t ldesps32YawdBeta;
	int32_t ldesps32YawdYaw;
	int32_t ldesps32YawdDelta;
	int16_t ldesps16VehicleVelocity;
	int16_t ldesps16BetaDynModel;
	int16_t ldesps16YawDynModel;
	int16_t ldesps16YawDynModelOld;
	int16_t ldesps16DeltaDynModel;
	int32_t ldesps32BetaDynModel;
	int32_t ldesps32YawDynModel;
	int32_t ldesps32BetaDynModelOld;
	int32_t ldesps32YawDynModelOld;
	int16_t ldesps16LatGDynModel;
	int16_t ldesps16BetarateDynModel;
	int16_t ldesps16YawRfDynModel;
	int16_t ldesps16YawDynGain;
	int16_t ldesps16FiltdYawDynModel;
	int16_t ldesps16FiltdLatGDynModel;
	int16_t ldesps16ackermannyaw;
	int8_t  ldesps8ModelSteerStbCnt;
	#if __YAW_SS_MODEL_ENABLE
		int16_t lsesps16YawSS;
	#endif  
#endif

/*  GM Bank   */
#if (__GM_CPG_BANK==1)
	uint8_t Bank_GM_Susp;
	int16_t ldescS16GMBankPrsRiseCount;
	int16_t ldescS16GMBankNextRiseCount;
	int16_t	ldescS16GMBankRiseNum;
	int16_t	ldescS16GMBankPrsRiseCountOld;
	int16_t ldescS16GMBankRiseInterval;
#endif

/*--- Variable Define ---*/
#if  (__ADAPTIVE_MODEL_VELO == ENABLE )
	int16_t	ldesps16AdmVcrtVeloDiff;
	
	int32_t ldesps32AdmVcrtVeloCalcLHSum;   
	int32_t ldesps32AdmVcrtDiffdotVeloLHSum;    
	uint8_t ldespu8AdmVcrtDataLHCNT;
	int16_t ldesps16AdmVcrtRatioLHRaw;   
	int16_t ldesps16AdmVcrtRatioLH;              /* 1--> 0.1 %  */
	
	int32_t ldesps32AdmVcrtVeloCalcRHSum;   
	int32_t ldesps32AdmVcrtDiffdotVeloRHSum;    
	uint8_t ldespu8AdmVcrtDataRHCNT;
	int16_t ldesps16AdmVcrtRatioRHRaw;   
	int16_t ldesps16AdmVcrtRatioRH;              /* 1--> 0.1 %  */
	
	int16_t ldesps16AdmVcrtRatioApply;
	int16_t ldesps16AdmVcrtRatioApplyF;
	int16_t ldesps16AdmVcrtRatioApplyFvelo;
	
	int16_t ldesps16AdmVestUsingYawSteer;
	int16_t ldesps16AdmVeloRatioCalc;
	
	int16_t ldesps16TireAngleVch;
	int16_t ldesps16AdmVchForVcrtRatio;
	
	int16_t ldesps16AdmVcrtEEPread;
	int16_t ldesps16AdmVcrtRadius ;
	
	uint8_t ldespu8AdmVeloWheelSpinCNT;
#endif

#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
	int16_t ldesps16AdmVchGEN;
	int16_t ldesps16AdmVchGENLH;
	int16_t ldesps16AdmVchGENRH;
	int16_t ldesps16AdmVchGEN_F;
	int16_t ldesps16AdmVchGENinSQRT;
	int16_t ldesps16AdmVchApplyF;
	uint8_t ldespu8AdmVchGEN_FChgCNT;
	
	int16_t ldesps16AdmVchGEN1secLHMean;
	int32_t ldesps32AdmVchGENCalcLHSum;
	uint8_t ldespu8AdmVchGENDataLHCNT;
	
	int16_t ldesps16AdmVchGEN1secRHMean;
	int32_t ldesps32AdmVchGENCalcRHSum;
	uint8_t ldespu8AdmVchGENDataRHCNT;
	
	int16_t ldesps16AdmVchGENEEPread;
	uint8_t ldespu8AdmVchUpdPercent;
	
	/* int16_t    ldesps16AdmVchBankYaw; */
	int16_t ldesps16AdmVchAyComp;
	
	int16_t ldesps16DetYawAcc;
	int16_t	ldesps16DetYawAccFilt;
	uint8_t ldespu8DetYawAccCnt;
	int16_t ldesps16DetDelYawAcc;
	int16_t	ldesps16DetDelYawAccFilt;
	uint8_t ldespu8DetDelYawAccCnt;
#endif

#if  (  (__ADAPTIVE_MODEL_VELO == ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)  )
	struct  ADAPTIVE_M	*ADM, ADMvelo_LH,ADMvelo_RH ,  ADMvchGEN_LH, ADMvchGEN_RH ;
	int16_t ldesps16ADMTireAngle;
	int16_t ldesps16ADMDeltaDynModel;
#endif

#if  ( (__ADAPTIVE_MODEL_VELO == ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE) || (__LOAD_ADAPTIVE_CTRL == ENABLE) )
	uint8_t   u8DataCNT;  /* For Data Logging */
#endif

VEHICLE_MODEL_FLG lsu8VehicleModelFlg1;

#if  __ADAPTIVE_MODEL_VELO == ENABLE
	VEHICLE_MODEL_FLG lsu8VehicleModelFlg4 , lsu8VehicleModelFlg5;
#endif

#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
	VEHICLE_MODEL_FLG lsu8VehicleModelFlg6 , lsu8VehicleModelFlg7;
#endif

/*  GM Bank   */
#if (__GM_CPG_BANK==1)
	void LDESP_vDetGMBank(void);
#endif

void	LDESP_vCalVehicleModel(void)
{
	#if  ( (__ADAPTIVE_MODEL_VCH_GEN == ENABLE )|| (__ADAPTIVE_MODEL_VELO == ENABLE) )
		LDESP_vDetADMInitialization();
    	LDESP_vADMreadEEP();
    #endif

    #if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
		LDESP_vCalModelVchGENCompMain();    
    #endif    
        
    #if  __ADAPTIVE_MODEL_VELO == ENABLE
        LDESP_vCalModelVeloCompMain();
    #endif

	#if !__MODEL_CHANGE
		LDESP_vCalYawRateDesired();
		LDESP_vCalLatAccelDesired();
	#else
		LDESP_vCalDynBicycleModel();
	#endif

	/*ESC functions - consider side effects of (moving calling parts) */
	/*MUDETECTION*/
	LDESP_vCalLatAccAbsolute();
	#if (__GM_CPG_BANK == 1)
		LDESP_vDetGMBank();
	#endif
	LDESP_vEstRoadAdhesion();
	/*Limit Rf*/
	LDESP_vLimitYawRateDesired();
	/*ESC functions*/

	#if	( (__ADAPTIVE_MODEL_VELO == ENABLE) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE) )
		if(speed_calc_timer	< (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
		{
			;
		}
		else
		{
			#if (__ADAPTIVE_MODEL_VELO == ENABLE) || (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)
				ACC_CALC =	(struct	ACC_FILTER *)&WSTR_ACC;
				LDABS_vCalcEachFilterSignalAccel((lsesps16ADMwstr),	s16WSTRFilterFreq, s16WSTRFilterZeta, s16WSTRAccMulti, s16WSTRdiffcoefCov);
			#else
				ACC_CALC =	(struct	ACC_FILTER *)&WSTR_ACC;
				LDABS_vCalcEachFilterSignalAccel((0),	s16WSTRFilterFreq, s16WSTRFilterZeta, s16WSTRAccMulti, s16WSTRdiffcoefCov);
			#endif
			
			ACC_CALC =	(struct	ACC_FILTER *)&YAWM_ACC;
			LDABS_vCalcEachFilterSignalAccel((yaw_out), s16YAWMFilterFreq, s16YAWMFilterZeta, s16YAWMAccMulti, s16YAWMdiffcoefCov);
		
        	ACC_CALC = 	(struct ACC_FILTER *)&DEL_YAW_ACC;
        	LDABS_vCalcEachFilterSignalAccel((yaw_out-rf2), s16YAWMFilterFreq, s16YAWMFilterZeta, s16YAWMAccMulti, s16YAWMdiffcoefCov);
		
			ACC_CALC =	(struct	ACC_FILTER *)&YAWC_ACC;
			LDABS_vCalcEachFilterSignalAccel((ldesps16YawRfDynModel), s16YAWMFilterFreq, s16YAWMFilterZeta, s16YAWMAccMulti, s16YAWMdiffcoefCov);
		}
	#endif
}


#if !__MODEL_CHANGE
	static void LDESP_vCalYawRateDesired(void)
	{
		LDESP_vCalYawRateDesiredGain();
		LDESP_vCalYawRateBicycleModel();
		LDESP_vCalYawRateDesiredFilter();
		LDESP_vFilterYawRateDesired();
	}
#endif

#if !__MODEL_CHANGE
	#if __CAR==SYC_RAXTON || __CAR==SYC_KYRON
		static void LDESP_vCalYawRateDesiredGain(void)
		{
		
			k_vref = vrefk;     /* resolution : 0.1*/
			if(k_vref<=50) 
			{
				k_vref=50; /* k_vref limit - 저속에서 이상계산되는 것 막음*/
			}
			else
			{
				;
			}
			
			if(k_vref<pEspModel->Yc_Vx[1]) 
			{
				yc_v_index=0;
			}
			else if(k_vref>=pEspModel->Yc_Vx[18])  
			{
				yc_v_index=18;
			}
			else 
			{
				yc_v_index=(int16_t)(k_vref/50)-1;
			}
			
			if(abs_wstr<pEspModel->Yc_St[1]) 
			{
				yc_st_index=0;
			}
			else if(abs_wstr>=pEspModel->Yc_St[20]) 
			{
				yc_st_index=20;
			}
			else 
			{
				yc_st_index=(int16_t)((abs_wstr-100)/250);
			}
			
			yc_v1=pEspModel->Yc_Vx[yc_v_index];
			yc_v2=pEspModel->Yc_Vx[yc_v_index+1];
			
			yc_st1=pEspModel->Yc_St[yc_st_index];
			yc_st2=pEspModel->Yc_St[yc_st_index+1];
			
			
			yc_g11=pEspModel->Yc_Map[yc_st_index][yc_v_index];
			yc_g12=pEspModel->Yc_Map[yc_st_index][yc_v_index+1];
			yc_g21=pEspModel->Yc_Map[yc_st_index+1][yc_v_index];
			yc_g22=pEspModel->Yc_Map[yc_st_index+1][yc_v_index+1];
			
			
			/*    yc_g01 = yc_g11 + (int16_t)( ((int32_t)(yc_g12 - yc_g11))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
			tempW2 = yc_g12 - yc_g11; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
			s16muls16divs16();
			yc_g01 = yc_g11 + tempW3;      
			
			/*    yc_g02 = yc_g21 + (int16_t)( ((int32_t)(yc_g22 - yc_g21))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
			tempW2 = yc_g22 - yc_g21; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
			s16muls16divs16();
			yc_g02 = yc_g21 + tempW3;   
			    
			/*    yawgain = yc_g01 + (int16_t)( ((int32_t)(yc_g02-yc_g01))*(abs_wstr-yc_st1)/(yc_st2-yc_st1) ); */
			tempW2 = yc_g02-yc_g01; tempW1 = abs_wstr-yc_st1 ; tempW0 = yc_st2-yc_st1 ;
			s16muls16divs16();
			yawgain = yc_g01 + tempW3;   
			/*  TRC 임시 조치임 / 모델 확인후 삭제*/        
			/*
			        #if (__CAR==SYC_RAXTON)&&(__SUSPENSION_TYPE ==0) 
			    
			             esp_tempW1 = LCESP_s16IInter6Point( vrefk, VREF_K_30_KPH, 100, 
			                                               VREF_K_60_KPH, 100, 
			                                               VREF_K_80_KPH, 100, 
			                                               VREF_K_100_KPH,97, 
			                                               VREF_K_120_KPH,91, 
			                                               VREF_K_150_KPH,90);
				        yawgain = (int16_t)( (((int32_t)yawgain)*esp_tempW1)/100);	                                          
				   #endif
			*/
		}
	#else/*#if __CAR==SYC_RAXTON || __CAR==HMC_EN|| __CAR==SYC_KYRON*/
		static void LDESP_vCalYawRateDesiredGain(void)
		{
		/*---------------------------------------------------------------------*/
		/* Calculation of k_vref(km/h)                                         */
		/*---------------------------------------------------------------------*/
			k_vref = vrefk;     /* resolution : 0.1*/
			if(k_vref<=50) 
			{
				k_vref=50; /* k_vref limit - 저속에서 이상계산되는 것 막음*/
			}
			else
			{
				;
			}
		/*---------------------------------------------------------------------*/
		/* Calculation of yawgain                                              */
		/*---------------------------------------------------------------------*/
		   
			if(k_vref<Yc_Vx[1]) 
			{
				yc_v_index=0;
			}
			else if(k_vref>=Yc_Vx[18])  
			{
				yc_v_index=18;
			}
			else 
			{
				yc_v_index=(int16_t)(k_vref/50)-1;
			}
			
			if(abs_wstr<Yc_St[1]) 
			{
				yc_st_index=0;
			}
			else if(abs_wstr>=Yc_St[20]) 
			{
				yc_st_index=20;
			}
			else 
			{
				yc_st_index=(int16_t)((abs_wstr-100)/250);
			}
			
			yc_v1=Yc_Vx[yc_v_index];
			yc_v2=Yc_Vx[yc_v_index+1];
			
			yc_st1=Yc_St[yc_st_index];
			yc_st2=Yc_St[yc_st_index+1];
			
			yc_g11=Yc_Map[yc_st_index][yc_v_index];
			yc_g12=Yc_Map[yc_st_index][yc_v_index+1];
			yc_g21=Yc_Map[yc_st_index+1][yc_v_index];
			yc_g22=Yc_Map[yc_st_index+1][yc_v_index+1];
			
			/*    yc_g01 = yc_g11 + (int16_t)( ((int32_t)(yc_g12 - yc_g11))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
			tempW2 = yc_g12 - yc_g11; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
			s16muls16divs16();
			yc_g01 = yc_g11 + tempW3;      
			
			/*    yc_g02 = yc_g21 + (int16_t)( ((int32_t)(yc_g22 - yc_g21))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
			tempW2 = yc_g22 - yc_g21; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
			s16muls16divs16();
			yc_g02 = yc_g21 + tempW3;   
			    
			/*    yawgain = yc_g01 + (int16_t)( ((int32_t)(yc_g02-yc_g01))*(abs_wstr-yc_st1)/(yc_st2-yc_st1) ); */
			tempW2 = yc_g02-yc_g01; tempW1 = abs_wstr-yc_st1 ; tempW0 = yc_st2-yc_st1 ;
			s16muls16divs16();
			yawgain = yc_g01 + tempW3;   
			/*  TRC 임시 조치임 / 모델 확인후 삭제*/        
			/*
			        #if (__CAR==SYC_RAXTON)&&(__SUSPENSION_TYPE ==0) 
			    
			             esp_tempW1 = LCESP_s16IInter6Point( vrefk, VREF_K_30_KPH, 100, 
			                                               VREF_K_60_KPH, 100, 
			                                               VREF_K_80_KPH, 100, 
			                                               VREF_K_100_KPH,97, 
			                                               VREF_K_120_KPH,91, 
			                                               VREF_K_150_KPH,90);
				        yawgain = (int16_t)( (((int32_t)yawgain)*esp_tempW1)/100);	                                          
				   #endif
			*/
		}
	#endif
#endif

#if __MODEL_CHANGE
	static void LDESP_vCalYawRateDynGain(void)
	{
		uint8_t ldespu8LoopCntSrchStr;
		uint8_t ldespu8LoopCntSrchVel;
	
		k_vref = vrefk;     /* resolution : 0.1*/
		if(k_vref<=50) 
		{
			k_vref=50; /* k_vref limit - 저속에서 이상계산되는 것 막음*/
		}
		else
		{
			;
		}
		/*Yc_Vx[12] [0]~[11] [10]: 200kph [11]:500kph*/
		if(k_vref<pEspModel->Yc_Vx[1]) 
		{
			yc_v_index=0;
		}
		else if(k_vref>=pEspModel->Yc_Vx[10]) 
		{
			yc_v_index=10;
		}
		else 
		{
			for (ldespu8LoopCntSrchVel=1;ldespu8LoopCntSrchVel<=9;ldespu8LoopCntSrchVel++)
			{
				if( (k_vref>=pEspModel->Yc_Vx[ldespu8LoopCntSrchVel]) && (k_vref<pEspModel->Yc_Vx[ldespu8LoopCntSrchVel+1]) )
				{
					yc_v_index=(int16_t)ldespu8LoopCntSrchVel;
				}
				else
				{
					;
				}
			}	
		}
		/*Yc_St[15] [0]~[12]540 [13]MAX [14]4*MAX*/
		if(WSTR_MAX>pEspModel->Yc_St[12])
		{
			if(abs_wstr<pEspModel->Yc_St[1]) 
			{
				yc_st_index=0;
			}
			else if(abs_wstr>=WSTR_MAX) 
			{
				yc_st_index=13;
			}    	
			else if(abs_wstr>=pEspModel->Yc_St[12]) 
			{
				yc_st_index=12;
			}
			else 
			{
				for (ldespu8LoopCntSrchStr=1;ldespu8LoopCntSrchStr<=11;ldespu8LoopCntSrchStr++)
				{
					if( (abs_wstr>=pEspModel->Yc_St[ldespu8LoopCntSrchStr]) && (abs_wstr<pEspModel->Yc_St[ldespu8LoopCntSrchStr+1]) )
					{
						yc_st_index=(int16_t)ldespu8LoopCntSrchStr;
					}
					else
					{
						;
					}
				}
			} 
		}
		else
		{
			if(abs_wstr<pEspModel->Yc_St[1]) 
			{
				yc_st_index=0;
			}
			else if(abs_wstr>=pEspModel->Yc_St[11]) 
			{
				yc_st_index=11;
			}
			else 
			{
				for (ldespu8LoopCntSrchStr=1;ldespu8LoopCntSrchStr<=10;ldespu8LoopCntSrchStr++)
				{
					if( (abs_wstr>=pEspModel->Yc_St[ldespu8LoopCntSrchStr]) && (abs_wstr<pEspModel->Yc_St[ldespu8LoopCntSrchStr+1]) )
					{
						yc_st_index=(int16_t)ldespu8LoopCntSrchStr;
					}
					else
					{
						;
					}
				}	
			}
		}  
			
		yc_v1=pEspModel->Yc_Vx[yc_v_index];
		yc_v2=pEspModel->Yc_Vx[yc_v_index+1];
		if( (WSTR_MAX>pEspModel->Yc_St[12]) && (yc_st_index==13) )/*Over MAX*/
		{
			yc_st1=WSTR_MAX;
			yc_st2=4*WSTR_MAX;/*maximum sensor's mis-calibration + maximum mis-alignment*/			
		}
		else if( (WSTR_MAX>pEspModel->Yc_St[12]) && (yc_st_index==12) )/*Over 540*/
		{
			yc_st1=pEspModel->Yc_St[yc_st_index];
			yc_st2=WSTR_MAX;			
		}
		else
		{
			yc_st1=pEspModel->Yc_St[yc_st_index];
			yc_st2=pEspModel->Yc_St[yc_st_index+1];
		}
		
		yc_g11=pEspModel->Yc_Map[yc_st_index][yc_v_index];
		yc_g12=pEspModel->Yc_Map[yc_st_index][yc_v_index+1];
		yc_g21=pEspModel->Yc_Map[yc_st_index+1][yc_v_index];
		yc_g22=pEspModel->Yc_Map[yc_st_index+1][yc_v_index+1];
		
		/*    yc_g01 = yc_g11 + (int16_t)( ((int32_t)(yc_g12 - yc_g11))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
		tempW2 = yc_g12 - yc_g11; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
		s16muls16divs16();
		yc_g01 = yc_g11 + tempW3;      
		
		/*    yc_g02 = yc_g21 + (int16_t)( ((int32_t)(yc_g22 - yc_g21))*(k_vref-yc_v1)/(yc_v2-yc_v1) );  */
		tempW2 = yc_g22 - yc_g21; tempW1 = k_vref-yc_v1 ; tempW0 = yc_v2-yc_v1 ;
		s16muls16divs16();
		yc_g02 = yc_g21 + tempW3;   
		    
		/*    yawgain = yc_g01 + (int16_t)( ((int32_t)(yc_g02-yc_g01))*(abs_wstr-yc_st1)/(yc_st2-yc_st1) ); */
		tempW2 = yc_g02-yc_g01; tempW1 = abs_wstr-yc_st1 ; tempW0 = yc_st2-yc_st1 ;
		s16muls16divs16();
		ldesps16YawDynGain = yc_g01 + tempW3;   
	
		#if(__YAW_SS_MODEL_ENABLE==1) &&(!SIM_MATLAB)
	    	lsesps16YawSS = (INT)( (((LONG)lsesps16YawSS_temp)*ldesps16YawDynGain)/10000 );
	    #endif
	}
#endif

#if !__MODEL_CHANGE
	#if __CAR==SYC_RAXTON || __CAR==SYC_KYRON
		static void LDESP_vCalYawRateBicycleModel(void)
		{
			tempUW2 = k_vref; 
			tempUW1 = pEspModel->M_Den1_10N; 
			tempUW0 = 1000;
			u16mulu16divu16();
			esp_tempUW1 = tempUW3;   
			tempUW2 = pEspModel->M_Den2_10N; 
			tempUW1 = 100; 
			tempUW0 = k_vref;
			u16mulu16divu16();
			esp_tempUW2 = tempUW3;        
			tempUW2 = 10000; 
			tempUW1 = pEspModel->M_Num_10N; 
			tempUW0 = esp_tempUW2 + esp_tempUW1;
			u16mulu16divu16();
			esp_tempUW3 = tempUW3;            
			
			/*  esp_tempW2 = (uint16_t)( (((uint32_t)esp_tempW3)*yawgain)/10000);   resolution : 0.001 */
			tempUW2 = esp_tempUW3; 
			tempUW1 = yawgain; 
			tempUW0 = 10000;
			u16mulu16divu16();
			esp_tempUW2 = tempUW3;  
			    
			/*    yawno = (int16_t)( (((int32_t)esp_tempUW2)*wstr)/100);           resolution : 0.01  */
			tempW2 = esp_tempUW2; 
			tempW1 = wstr; 
			tempW0 = 100;
			s16muls16divs16();
			yawno = tempW3;    
			                                                   
			esp_tempW0 = vrefm;
			
			if( esp_tempW0 <= 83 )  
			{
				esp_tempW0 = 83;                      
			}
			else 
			{    
				;
			}    
			                                                  
			tempUW2 = LAT_1G3G; 
			tempUW1 = 562; 
			tempUW0 = esp_tempW0;
			u16mulu16divu16();
			esp_tempW1 = tempUW3;       
			
			if ( yawno > esp_tempW1 ) 
			{
				yawno = esp_tempW1;
			}
			else if ( yawno < -esp_tempW1 ) 
			{
				yawno = -esp_tempW1;
			}
			else 
			{    
				;
			}    
		
		}
		
		static void LDESP_vCalYawRateDesiredFilter(void)
		{
		/*---------------------------------------------------------------------*/
		/* Calculation of FILTER GAIN OF rf (filted yaw)                       */
		/*---------------------------------------------------------------------*/
		    if(k_vref<=400) 
		    {
		    	k_vref=400; /* k_vref limit - rf filter 의 발산 방지*/
		    }
		    else 
		    {    
		    	;
		    }    
			/*   c1 = (uint16_t)( (((uint32_t)13769)*10)/k_vref);   resolution : 0.1  */   
		    tempUW2 =pEspModel->C_1; 
		    tempUW1 = 10; 
		    tempUW0 = k_vref;
		    u16mulu16divu16();
		    c1 = tempUW3;   
		    
			/*    tempW1 = (uint16_t)( (((uint32_t)5690)*10)/k_vref);
			    tempW2 = (uint16_t)( (((uint32_t)tempW1)*tempW1)/10);
			    c2 = 758 + tempW2; 
			*/
		    tempUW2 = pEspModel->C_22; 
		    tempUW1 = 10; 
		    tempUW0 = k_vref;
		    u16mulu16divu16();
		    esp_tempUW1 = tempUW3; 
		         
		    tempUW2 = esp_tempUW1; 
		    tempUW1 = esp_tempUW1; 
		    tempUW0 = 10;
		    u16mulu16divu16();
		    esp_tempUW2 = tempUW3; 
		         
		    c2 = pEspModel->C_21 + esp_tempUW2;  
		    
		        /* speed varying filter  , Sp_f1= 600, Sp_f2= 200, Sp_f3= 7, Sp_f4= 400;	*/
		    if(k_vref > pEspModel->Sp_f1 )  
		    {
		    	esp_tempUW1 = S16_MODEL_C2_RATIO;   
		    }
		    else if(k_vref > pEspModel->Sp_f2) 
		    {
		    /*  esp_tempW1 = 10 + (uint16_t)( (((uint32_t)(k_vref-400))*(S16_MODEL_C2_RATIO-10))/200);*/
		        tempUW2 = k_vref-pEspModel->Sp_f2;
		         tempUW1 = S16_MODEL_C2_RATIO-pEspModel->Sp_f3; 
		         tempUW0 = pEspModel->Sp_f4;
		        u16mulu16divu16();
		        esp_tempUW1 = pEspModel->Sp_f3 + tempUW3;     
		    }   
		    else        
		    {
		    	esp_tempUW1 = pEspModel->Sp_f3;
		    }
		
		    tempW2 = c2; 
		    tempW1 = esp_tempUW1; 
		    tempW0 = 10;
		    s16muls16divs16();
		    c2 = tempW3;            
		    
		}
	#else/*#if __CAR==SYC_RAXTON || __CAR==HMC_EN|| __CAR==SYC_KYRON*/
		static void LDESP_vCalYawRateBicycleModel(void)
		{
		/*---------------------------------------------------------------------*/
		/* Calculation of yawno (no-filted yaw)                                */
		/*---------------------------------------------------------------------*/
		
		/*    tempW5 = (uint16_t)( (((uint32_t)k_vref)*4612)/1000);             resolution : 0.0001
		    tempW4 = (uint16_t)( (((uint32_t)19690)*100)/k_vref);               resolution : 0.0001
		    tempW3 = (uint16_t)( (((uint32_t)10000)*122)/(tempW4+tempW5)); */
		    
		    tempUW2 = k_vref; 
		    tempUW1 = M_Den1_10N; 
		    tempUW0 = 1000;
		    u16mulu16divu16();
		    esp_tempUW1 = tempUW3;   
		    tempUW2 = M_Den2_10N; 
		    tempUW1 = 100; 
		    tempUW0 = k_vref;
		    u16mulu16divu16();
		    esp_tempUW2 = tempUW3;        
		    tempUW2 = 10000; 
		    tempUW1 = M_Num_10N; 
		    tempUW0 = esp_tempUW2 + esp_tempUW1;
		    u16mulu16divu16();
		    esp_tempUW3 = tempUW3;            
		
			/*  esp_tempW2 = (uint16_t)( (((uint32_t)esp_tempW3)*yawgain)/10000);   resolution : 0.001 */
		    tempUW2 = esp_tempUW3; 
		    tempUW1 = yawgain; 
		    tempUW0 = 10000;
		    u16mulu16divu16();
		    esp_tempUW2 = tempUW3;  
		        
			/*    yawno = (int16_t)( (((int32_t)esp_tempUW2)*wstr)/100);           resolution : 0.01  */
		    tempW2 = esp_tempUW2; 
		    tempW1 = wstr; 
		    tempW0 = 100;
		    s16muls16divs16();
		    yawno = tempW3;    
		                                                       
		    esp_tempW0 = vrefm;
		    
		    if( esp_tempW0 <= 83 )  
		    {
		    	esp_tempW0 = 83;                      
		    }
		    else 
		    {    
		    	;
		    }    
		                                                      
		    tempUW2 = LAT_1G3G; 
		    tempUW1 = 562; 
		    tempUW0 = esp_tempW0;
		    u16mulu16divu16();
		    esp_tempW1 = tempUW3;       
		    
		    if ( yawno > esp_tempW1 ) 
		    {
		    	yawno = esp_tempW1;
		    }
		    else if ( yawno < -esp_tempW1 ) 
		    {
		    	yawno = -esp_tempW1;
		    }
		    else 
		    {    
		    	;
		    }    
		}
		
		static void LDESP_vCalYawRateDesiredFilter(void)
		{
		/*---------------------------------------------------------------------*/
		/* Calculation of FILTER GAIN OF rf (filted yaw)                       */
		/*---------------------------------------------------------------------*/
		
		    if(k_vref<=400) 
		    {
		    	k_vref=400; /* k_vref limit - rf filter 의 발산 방지*/
		    }
		    else 
		    {    
		    	;
		    }    
			/*   c1 = (uint16_t)( (((uint32_t)13769)*10)/k_vref);   resolution : 0.1  */   
		    tempUW2 =C_1; 
		    tempUW1 = 10; 
		    tempUW0 = k_vref;
		    u16mulu16divu16();
		    c1 = tempUW3;   
		    
			/*    tempW1 = (uint16_t)( (((uint32_t)5690)*10)/k_vref);
			    tempW2 = (uint16_t)( (((uint32_t)tempW1)*tempW1)/10);
			    c2 = 758 + tempW2; 
			*/
		    tempUW2 = C_22; 
		    tempUW1 = 10; 
		    tempUW0 = k_vref;
		    u16mulu16divu16();
		    esp_tempUW1 = tempUW3; 
		         
		    tempUW2 = esp_tempUW1; 
		    tempUW1 = esp_tempUW1; 
		    tempUW0 = 10;
		    u16mulu16divu16();
		    esp_tempUW2 = tempUW3; 
		         
		    c2 = C_21 + esp_tempUW2;  
		    
		        /* speed varying filter  , Sp_f1= 600, Sp_f2= 200, Sp_f3= 7, Sp_f4= 400;	*/
		    if(k_vref > Sp_f1 )  
		    {
		    	esp_tempUW1 = S16_MODEL_C2_RATIO;   
		    }
		    else if(k_vref > Sp_f2) 
		    {
		    /*  esp_tempW1 = 10 + (uint16_t)( (((uint32_t)(k_vref-400))*(S16_MODEL_C2_RATIO-10))/200);*/
		        tempUW2 = k_vref-Sp_f2; 
		        tempUW1 = S16_MODEL_C2_RATIO-Sp_f3; 
		        tempUW0 = Sp_f4;
		        u16mulu16divu16();
		        esp_tempUW1 = Sp_f3 + tempUW3;     
		    }   
		    else        
		    {
		    	esp_tempUW1 = Sp_f3;
		    }
		
		    tempW2 = c2; 
		    tempW1 = esp_tempUW1; 
		    tempW0 = 10;
		    s16muls16divs16();
		    c2 = tempW3;            
		}
	#endif/*#if __CAR==SYC_RAXTON || __CAR==HMC_EN|| __CAR==SYC_KYRON*/
	
	static void LDESP_vFilterYawRateDesired(void)
	{
	
	/*---------------------------------------------------------------------*/
	/* Calculation of rf (filted yaw)                                      */
	/*---------------------------------------------------------------------*/
	
	    /* rf filtering : euler method */
	    rfdo=rfd;
	    rfo=rf;
	
		/*  esp_tempW1 = (int16_t)( (((int32_t)c2)*7)/100);     resolution : 0.01    */
	    tempW2 = c2; 
	    tempW1 = 7; 
	    tempW0 = 100 ;
	    s16muls16divs16();
	    esp_tempW1 = tempW3;   
	        
		/*  esp_tempW2 = (int16_t)( (((int32_t)esp_tempW1)*((int16_t)(yawno-rfo)))/1000);   resolution : 0.1 ; c2(yawno-rfo)*dt */
	    tempW2 = esp_tempW1; 
	    tempW1 = yawno-rfo; 
	    tempW0 = 1000;
	    s16muls16divs16();
	    esp_tempW2 = tempW3;   
	
		/*  esp_tempW3 = (uint16_t)( (((uint32_t)c1)*7)/100);   resolution : 0.01    */
	    tempUW2 = c1; 
	    tempUW1 = 7; 
	    tempUW0 = 100;
	    u16mulu16divu16();
	    esp_tempUW3 = tempUW3;      
	    
		/*  esp_tempW4 = (int16_t)( (((int32_t)rfdo)*esp_tempUW3)/100);  resolution : 0.01   */
	    tempW2 = rfdo; 
	    tempW1 = esp_tempUW3; 
	    tempW0 = 100;
	    s16muls16divs16();
	    esp_tempW4 = tempW3;       
	
	    rfd = rfdo + esp_tempW2 - esp_tempW4 ;                   
	
		/*  esp_tempW5 = (int16_t)( (((int32_t)rfd)*7)/100);    resolution : 0.01    */
	    tempW2 = rfd; 
	    tempW1 = 7; 
	    tempW0 = 100;
	    s16muls16divs16();
	    esp_tempW5 = tempW3;  
	        
	    rf = rfo + esp_tempW5 ; 
	
	    if( vrefk < 50)
	    {
	        if ( rf > 6 )       
	        {
	        	rf = rf - 5;
	        }
	        else if ( rf < -6 ) 
	        {
	        	rf = rf + 5;
	        }
	        else                
	        {
	        	rf = 0;
	        }
	        
	        if ( rfd > 6 )       
	        {
	        	rfd = rfd - 5;
	        }
	        else if ( rfd < -6 ) 
	        {
	        	rfd = rfd + 5;
	        }
	        else                 
	        {
	        	rfd = 0;       
	        }
	    }
	    else 
	    {    
	    	;
	    }    
	    
	    if((WSTR_360_JUMP)&&(!SAS_CHECK_OK)&&(WSTR_360_JUMP_on_cnt<L_U8_TIME_10MSLOOP_70MS)) 
	    {
	        if ( abs(rf) > 6 )       
	        {
	        	rf=rf>>1;        
	        }
	        else 
	        {    
	        	;
	        }    
	        
	        if ( abs(rfd) > 6 )       
	        {
	        	rfd = rfd>>1;
	        }
	        else 
	        {    
	        	;
	        }    
	        
	    }
	    else 
	    {    
	    	;
	    }    
	}
#endif

#if !__MODEL_CHANGE
	static void LDESP_vCalLatAccelDesired(void)
	{
	    LDESP_vCalLatAccelDesiredGain();
	    LDESP_vFilterLatAccelDesired();
	}
	#if __CAR==SYC_RAXTON /*|| __CAR==HMC_EN*/ || __CAR==SYC_KYRON
		static void LDESP_vCalLatAccelDesiredGain(void)
		{
		    nor_alat_old = nor_alat;
		    nor_alat_unlimit_old = nor_alat_unlimit;
		    no_fillter_alat_old = no_fillter_alat;
		
		
		    alat_filt_value1 = LCESP_s16IInter5Point( vrefk, pEspModel->Alat_speed_v1, pEspModel->Alat_filt_value_v1_from_speed, 
		                                            pEspModel->Alat_speed_v2, pEspModel->Alat_filt_value_v2_from_speed,
		                                            pEspModel->Alat_speed_v3, pEspModel->Alat_filt_value_v3_from_speed,
		                                            pEspModel->Alat_speed_v4, pEspModel->Alat_filt_value_v4_from_speed,
		                                            pEspModel->Alat_speed_v5, pEspModel->Alat_filt_value_v5_from_speed);
		
		
		    alat_filt_value2=128-alat_filt_value1;
		}
	#else
		static void LDESP_vCalLatAccelDesiredGain(void)
		{
		    nor_alat_old = nor_alat;
		    nor_alat_unlimit_old = nor_alat_unlimit;
		    no_fillter_alat_old = no_fillter_alat;
		
			if ( LOOP_TIME_20MS_TASK_1 == 1 ) /* Cycle by Cho */
			{
			    alat_filt_value1 = LCESP_s16IInter5Point( vrefk, Alat_speed_v1, Alat_filt_value_v1_from_speed, 
			                                            Alat_speed_v2, Alat_filt_value_v2_from_speed,
			                                            Alat_speed_v3, Alat_filt_value_v3_from_speed,
			                                            Alat_speed_v4, Alat_filt_value_v4_from_speed,
			                                            Alat_speed_v5, Alat_filt_value_v5_from_speed);
			
			
			    alat_filt_value2=128-alat_filt_value1;
			}
		}
	#endif
	
	static void LDESP_vFilterLatAccelDesired(void)
	{
	   /*---------------------------------------------------------------------*/
	   /* Calculation of Normal Alat                                          */
	   /*---------------------------------------------------------------------*/
		/*    ayno= (int16_t)( (((int32_t)rf)*vrefm)/562);    r : 0.001 , 562 = 180/pi*9.81*/
	    tempW2 = rf; 
	    tempW1 = vrefm; 
	    tempW0 = 562;
	    s16muls16divs16();
	    ayno = tempW3;     
	
		/*  esp_tempW4 = (int16_t)( (((int32_t)ayno)*alat_filt_value1)/128);*/
	    tempW2 = ayno; 
	    tempW1 = alat_filt_value1; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW4 = tempW3;      
	    
		/*  esp_tempW3 = (int16_t)( (((int32_t)nor_alat_unlimit_old)*alat_filt_value2)/128);*/
	    tempW2 = nor_alat_unlimit_old; 
	    tempW1 = alat_filt_value2; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW3 = tempW3;      
	
	    nor_alat_unlimit = esp_tempW4 + esp_tempW3;
	
		/*  ay_c 의 저속 조향각 회복시 delay 부분 개선 */
	    tempW2 = yawno; 
	    tempW1 = vrefm; 
	    tempW0 = 562;
	    s16muls16divs16();
	    no_fillter_alat = tempW3;   
	
		/*  esp_tempW4 = (int16_t)( (((int32_t)no_fillter_alat)*alat_filt_value1)/128);*/
	    tempW2 = no_fillter_alat; 
	    tempW1 = 96; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW4 = tempW3;      
	    
		/*  esp_tempW3 = (int16_t)( (((int32_t)nor_alat_unlimit_old)*alat_filt_value2)/128);*/
	    tempW2 = no_fillter_alat_old; 
	    tempW1 = 32; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW3 = tempW3;      
		/*  040116 
		nor_alat_unlimit_ems = esp_tempW4 + esp_tempW3;
		HIGH-MU에서 ayc의 phase delay 위하여 nor_alat_unlimit를 사용
		*/
	    nor_alat_unlimit_ems = nor_alat_unlimit;
	
	    if ( ayno > LAT_1G3G ) 
	    {
	    	ayno = LAT_1G3G;
	    }
	    else if ( ayno < -LAT_1G3G ) 
	    {
	    	ayno = -LAT_1G3G;
	    }
	    else 
	    {    
	    	;
	    }    
	
		/*  esp_tempW4 = (int16_t)( (((int32_t)ayno)*alat_filt_value1)/128);*/
	    tempW2 = ayno; 
	    tempW1 = alat_filt_value1; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW4 = tempW3;     
	        
		/*  esp_tempW3 = (int16_t)( (((int32_t)nor_alat_old)*alat_filt_value2)/128);*/
	    tempW2 = nor_alat_old; 
	    tempW1 = alat_filt_value2; 
	    tempW0 = 128;
	    s16muls16divs16();
	    esp_tempW3 = tempW3;         
	
	    nor_alat = esp_tempW4 + esp_tempW3;
	
	}
#endif

/* 
void MODEL_ADAPTATION(void)
{


    STABLE_MODEL_ADAPT = !(YAW_CDC_WORK | BACK_DIR) & ( AUTO_TM | BACK_DIR_DECT);
    STABLE_MODEL_ADAPT = STABLE_MODEL_ADAPT & !(BLS | vdc_error_flg | ABS_fz | BTC_fz | ESP_TCS_ON);
    STABLE_MODEL_ADAPT = STABLE_MODEL_ADAPT & !(ESP_SPLIT | EBD_RA);
    STABLE_MODEL_ADAPT = STABLE_MODEL_ADAPT & !(MINI_SPARE_WHEEL_fl | MINI_SPARE_WHEEL_fr | MINI_SPARE_WHEEL_rl | MINI_SPARE_WHEEL_rr);
    
    if( (vrefk > 400) && (vrefk < 800) && ( det_abs_wstr < 1200 ) && ( det_wstr_dot < 500 )
        && ( det_alatm > 300 ) && ( det_alatm < 500 ) && ( esp_mu > 750 )
        && STABLE_MODEL_ADAPT ) 
    {   
        mean_abs_yawc_old = mean_abs_yawc;
        mean_abs_yawm_old = mean_abs_yawm;
    
        abs_yawc = McrAbs(rf);
        
        mean_abs_yawc = LCESP_u16Lpf1Int(abs_yawc, mean_abs_yawc_old, 1); 
        mean_abs_yawm = LCESP_u16Lpf1Int(abs_yaw_out, mean_abs_yawm_old, 1); 
            
        count_adap_yawc++;
        
        if ( count_adap_yawc > 1000 ) 
        {
            
            mean_abs_yawc_buffer_old = mean_abs_yawc_buffer;
            mean_abs_yawm_buffer_old = mean_abs_yawm_buffer;
            
            esp_tempW0 = 8 - (count_adap_model>>4); 
            if ( esp_tempW0 < 1 ) 
            {
            	esp_tempW0 = 1;
            }
            else 
            {    
            	;
            }    

            mean_abs_yawc_buffer = LCESP_u16Lpf1Int(mean_abs_yawc, mean_abs_yawc_buffer_old, esp_tempW0); 
            mean_abs_yawm_buffer = LCESP_u16Lpf1Int(mean_abs_yawm, mean_abs_yawm_buffer_old, esp_tempW0); 
        
            tempW2 = mean_abs_yawc; tempW1 = 110; tempW0 = 100;
            s16muls16divs16();
            esp_tempW1 = tempW3;    

            tempW2 = mean_abs_yawc; tempW1 = 90; tempW0 = 100;
            s16muls16divs16();
            esp_tempW2 = tempW3;    


            tempW2 = mean_abs_yawm_buffer; tempW1 = 100; 
            if ( (mean_abs_yawc_buffer > 800) 
                 && ( mean_abs_yawc_buffer<esp_tempW1) && ( mean_abs_yawc_buffer>esp_tempW2)  ) 
            {
            	tempW0 = mean_abs_yawc_buffer;
            }
            else 
            {    
            	;
            }    
            
            else tempW0 = mean_abs_yawm_buffer;
            s16muls16divs16();
            gain_adap_yawc = tempW3;    
        
            if ( count_adap_model < 128 ) 
            {
            	count_adap_model++;
            }
            else 
            {    
            	;
            }    
            
            count_adap_yawc = 0;    
        }
        else 
        {    
        	;
        }               
        
    }
    else 
    {    
    	;
    }     
}   
*/

#if __MODEL_CHANGE  
	static void LDESP_vCalDynBicycleModel(void)
	{
		uint8_t ldespu8FilterYawDynModel;
		uint8_t ldespu8FilterLatGDynModel;
		int16_t ldesps16LatGExtraGain;
		int16_t ldesps16LatGLinearRange;
		int16_t ldesps16LatGLinearGain;
		int16_t ldesps16LatGLinearModel;
	  
		#if  (__ADAPTIVE_MODEL_VCH_GEN == ENABLE) 
	 		int16_t ldesps16Mass;
			int16_t ldesps16Cf;
			int16_t ldesps16Cr;
			int16_t ldesps16Mf;
			int16_t ldesps16Mr;
			int16_t ldesps16Iz;
			int16_t ldesps16Lf;
			int16_t ldesps16Lr;
			int16_t ldesps16VchSquare;
			int16_t ldesps16Temp0;
			int16_t ldesps16Temp1;
			int16_t ldesps16Temp5;
			int16_t ldesps16Temp6;
			int16_t ldesps16Temp7;
			int16_t ldesps16Temp8;	
		#endif 	
	
		#if  (__ADAPTIVE_MODEL_VCH_GEN == ENABLE) 
		    if(  (ldespu1AdmVchGENCrtApply ==1)  || ( speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS  )   )
		    {	
		        ldesps16Lf    = pEspModel->S16_LENGTH_CG_TO_FRONT;
		        ldesps16Lr    = pEspModel->S16_LENGTH_CG_TO_REAR;
		        ldesps16Iz    = pEspModel->S16_MOMENT_OF_INERTIA;
		        ldesps16Mf    = pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR;
		        ldesps16Mr    = pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR;
		        ldesps16Mass  = ldesps16Mf + ldesps16Mr;
		        ldesps16Cr    = pEspModel->S16_CORNER_STIFF_REAR;
		        
		        ldesps16VchSquare = (int16_t)(((int32_t)ldesps16AdmVchApplyF*ldesps16AdmVchApplyF)/829); /* Vch^2 = (m/s)^2 // 8^2*3.6^2 = 829.44 */
		        ldesps16Temp0     = (int16_t)( ( (((int32_t)ldesps16Cr*(ldesps16Lf+ldesps16Lr))/10) -  ((int32_t)ldesps16VchSquare*ldesps16Mr)  )/ldesps16Mf );
		        ldesps16Cf        = -(int16_t)(((int32_t)ldesps16VchSquare*ldesps16Cr)/ldesps16Temp0);                  /* Cf = -Vch^2*Cr/(Cr*(Lf+Lr)/Mf - Vch^2*Mr/Mf) */          
		            
		        ldesps16Temp1     = (int16_t)( ( ((((int32_t)ldesps16Lf*ldesps16Lf)/ldesps16Iz)*ldesps16Cf) + ((((int32_t)ldesps16Lr*ldesps16Lr)/ldesps16Iz)*ldesps16Cr) )/347);
		                    /* 100000/(dt*3.6*8) -> 7msec 496 ,   100000/(dt*3.6*8) -> 347   */
		        ldesps16Temp5 = (int16_t)((((int32_t)ldesps16Cf*ldesps16Lf)*10)/ldesps16Iz);
		        ldesps16Temp6 = (int16_t)((((int32_t)ldesps16Cr*ldesps16Lr)*10)/ldesps16Iz);
			                /* 7msec -> 7, 10msec -> 10 */
		        ldesps16Temp8 = (ldesps16Temp5 - ldesps16Temp6)/100;
		        
		        if( ldesps16Temp8 < 10 ) /* prevent overflow */
		        { 
		            ldesps16Temp8 = 10;
		        }
		        else
		        {
		            ;
		        }
			  
		        ldesps16GainBetadBeta	=  (int16_t)(((int32_t)(ldesps16Cf+ldesps16Cr)*1440)/ldesps16Mass); 
		                                    /*(Cf+Cr)/m*dt*3.6*8*S16_BETA_CAL_SCALE/10*/
		                                    /* 7msec-> 1008,  10msec -> 1440          */ 
		        ldesps32GainBetadYaw	= (((((int32_t)ldesps16Cf*ldesps16Lf)*41) -  ((int32_t)ldesps16Cr*ldesps16Lr*41))/ldesps16Mass);
											/* dt*3.6^2*8^2*S16_BETA_CAL_SCALE/10000 */
		        ldesps16GainBetadDelta	= -(int16_t)(((int32_t)ldesps16Cf*14400)/ldesps16Mass);             
		                                    /*(-Cf)/m*dt*3.6*8*S16_DELTA_CAL_SCALE*S16_BETA_CAL_SCALE/10*/
		                                    /* 7msec->10080,  10msec -> 14400   */	                                
		        ldesps16GainYawdBeta	=  ldesps16Temp8;                                         
		                                   /*(CfLf-CrLr)/Iz*dt*S16_YAW_CAL_SCALE/1000*/
		        ldesps16GainYawdYaw     =  ldesps16Temp1 ;
		                                   /*(CfLf^2+CrLr^2)/Iz*dt*3.6*8*S16_YAW_CAL_SCALE/10000000*/
		        ldesps16GainYawdDelta	= -(int16_t)(((((int32_t)ldesps16Cf*ldesps16Lf)*10)/ldesps16Iz)/10);    
		                                   /*(-CfLf)/Iz*dt*S16_DELTA_CAL_SCALE*S16_YAW_CAL_SCALE/1000*/ 
		                                   /* 7msec -> ldesps16Lf*7/ldesps16Iz/10 ,  10msec   ldesps16Lf*10/ldesps16Iz/10 */                               
		        
		    }
		    else
		    {
		        ;
		    }                       
		#else
	    	if(   speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS  )
	    	{	      
		    	/* Bicycle Model */	
				ldesps16GainBetadBeta	= pEspModel->S16_GAIN_BETA_D_BETA; /*(Cf+Cr)/m*dt*3.6*8*S16_BETA_CAL_SCALE*/
				ldesps32GainBetadYaw	= (int32_t)pEspModel->U16_GAIN_BETA_D_YAW;  /*(CfLf-CrLr)/m*dt*3.6^2*8^2*S16_BETA_CAL_SCALE*/
				ldesps16GainBetadDelta	= pEspModel->S16_GAIN_BETA_D_DELTA;/*(-Cf)/m*dt*3.6*8*S16_DELTA_CAL_SCALE*S16_BETA_CAL_SCALE*/
				ldesps16GainYawdBeta	= pEspModel->S16_GAIN_YAW_D_BETA;  /*(CfLf-CrLr)/Iz*dt*S16_YAW_CAL_SCALE*/
				ldesps16GainYawdYaw     = pEspModel->S16_GAIN_YAW_D_YAW;   /*(CfLf^2+CrLr^2)/Iz*dt*3.6*8*S16_YAW_CAL_SCALE*/
				ldesps16GainYawdDelta	= pEspModel->S16_GAIN_YAW_D_DELTA; /*(-CfLf)/Iz*dt*S16_DELTA_CAL_SCALE*S16_YAW_CAL_SCALE*/
	    	}
	    	else
	    	{
	    	    ;
	    	}
	  
		#endif     
	
	    LDESP_vCalYawRateDynGain();
	    
	    tempW1 = McrAbs(wstr);
    		if(wstr>=0)
    		{
   			tempW0 = LCESP_s16IInter6Point( tempW1, pEspModel->S16_STR_1st_Point, pEspModel->S16_STR_RATIO_10_1st,
    											              pEspModel->S16_STR_2nd_Point, pEspModel->S16_STR_RATIO_10_2nd,
    											              pEspModel->S16_STR_3rd_Point, pEspModel->S16_STR_RATIO_10_3rd,
    											              pEspModel->S16_STR_4th_Point, pEspModel->S16_STR_RATIO_10_4th,
    											              pEspModel->S16_STR_5th_Point, pEspModel->S16_STR_RATIO_10_5th,
    											              pEspModel->S16_STEERING_MAX_DEG, pEspModel->S16_STR_RATIO_10_MAX );
    		}
    		else
    		{
    		tempW0 = LCESP_s16IInter6Point( tempW1, pEspModel->S16_STR_1st_Point, pEspModel->CW_S16_STR_RATIO_10_1st,
    											              pEspModel->S16_STR_2nd_Point, pEspModel->CW_S16_STR_RATIO_10_2nd,
    											              pEspModel->S16_STR_3rd_Point, pEspModel->CW_S16_STR_RATIO_10_3rd,
    											              pEspModel->S16_STR_4th_Point, pEspModel->CW_S16_STR_RATIO_10_4th,
    											              pEspModel->S16_STR_5th_Point, pEspModel->CW_S16_STR_RATIO_10_5th,
    											              pEspModel->S16_STEERING_MAX_DEG, pEspModel->CW_S16_STR_RATIO_10_MAX );
    		}
	
	    /*-------------------     Tangent(theta) calculation    -------------------
	    
	    -. Radian
	        
	              WSTR*100          π                  WSTR
	      tan(  ----------- *100*-------- ) = tan (  -------- * 175   )   = Theta ,,
	               STR             180                  STR
	               
	               
	    -. Degree
	                 180      1                  573
	        Theta* ------- *------  =  Theta * --------          
	                  π       100                1000
	    
	    -------------------------------------------------------------------------*/
	    
		/*prevent overestimating Vehicle model by excessive steer rate when sensor power on*/
		if(ldespu1ModelSteerStbOk==0)
		{
		if(fu1SteerSenPwr1sOk==1)
		{
				ldesps8ModelSteerStbCnt = ldesps8ModelSteerStbCnt + 1;
	                    
				if(ldesps8ModelSteerStbCnt>=L_U8_TIME_10MSLOOP_200MS)
				{
					ldesps8ModelSteerStbCnt=L_U8_TIME_10MSLOOP_200MS;
					ldespu1ModelSteerStbOk = 1;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		else
		{
			if(fu1SteerSenPwr1sOk==0)
			{
				ldespu1ModelSteerStbOk = 0;
				ldesps8ModelSteerStbCnt = 0;
			}
			else
			{
				;
			}
		}
		/*prevent overestimating Vehicle model by excessive steer rate when sensor power on*/
	                    
		if(fu1SteerSenPwr1sOk==1)
		{
			tempW1 = (int16_t)( ((int32_t)(wstr)*100)/tempW0 );    
			tempW3 = McrAbs(tempW1);
	        /*--------- Tangent Apply -----------*/
	        if( tempW3 <= TAN_X_INP_25 )
	        {
	            tempW2 = LCESP_s16IInter6Point( tempW3, TAN_X_INP_0, TAN_X_OUT_0, TAN_X_INP_5, TAN_X_OUT_5, TAN_X_INP_10, TAN_X_OUT_10, TAN_X_INP_15, TAN_X_OUT_15 ,TAN_X_INP_20, TAN_X_OUT_20 ,TAN_X_INP_25, TAN_X_OUT_25)  ;              
	        }
	        else if( tempW3 < TAN_X_INP_50 )
	        {
	            tempW2 = LCESP_s16IInter6Point( tempW3 ,TAN_X_INP_25, TAN_X_OUT_25, TAN_X_INP_30, TAN_X_OUT_30, TAN_X_INP_35, TAN_X_OUT_35, TAN_X_INP_40, TAN_X_OUT_40, TAN_X_INP_45, TAN_X_OUT_45 ,TAN_X_INP_50, TAN_X_OUT_50);
	        }
	        else
	        {
	            tempW2 = LCESP_s16IInter3Point( tempW3, TAN_X_INP_50, TAN_X_OUT_50, TAN_X_INP_55, TAN_X_OUT_55, TAN_X_INP_60, TAN_X_OUT_60 );  
	        }
	        
	        if(wstr > 0)
	        {
	            ldesps16TireAngle = tempW2;
	        }
	        else
	        {
	            ldesps16TireAngle = -tempW2;
	        }
	        
	        /*ADM wstr 2013.3.21*/
	        #if ( (__ADAPTIVE_MODEL_VELO ==ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN ==ENABLE ) )
	        	tempW1 = McrAbs(lsesps16ADMwstr);	        
	            if(lsesps16ADMwstr>=0)
        		{
        			tempW0 = LCESP_s16IInter6Point( tempW1, pEspModel->S16_STR_1st_Point, pEspModel->S16_STR_RATIO_10_1st,
        											        pEspModel->S16_STR_2nd_Point, pEspModel->S16_STR_RATIO_10_2nd,
        											        pEspModel->S16_STR_3rd_Point, pEspModel->S16_STR_RATIO_10_3rd,
        											        pEspModel->S16_STR_4th_Point, pEspModel->S16_STR_RATIO_10_4th,
        											        pEspModel->S16_STR_5th_Point, pEspModel->S16_STR_RATIO_10_5th,
        											        pEspModel->S16_STEERING_MAX_DEG, pEspModel->S16_STR_RATIO_10_MAX );
        		}
        		else
        		{
        			tempW0 = LCESP_s16IInter6Point( tempW1, pEspModel->S16_STR_1st_Point, pEspModel->CW_S16_STR_RATIO_10_1st,
        											        pEspModel->S16_STR_2nd_Point, pEspModel->CW_S16_STR_RATIO_10_2nd,
        											        pEspModel->S16_STR_3rd_Point, pEspModel->CW_S16_STR_RATIO_10_3rd,
        											        pEspModel->S16_STR_4th_Point, pEspModel->CW_S16_STR_RATIO_10_4th,
        											        pEspModel->S16_STR_5th_Point, pEspModel->CW_S16_STR_RATIO_10_5th,
        											        pEspModel->S16_STEERING_MAX_DEG, pEspModel->CW_S16_STR_RATIO_10_MAX );
        		}
        		tempW1 = (int16_t)( ((int32_t)(lsesps16ADMwstr)*100)/tempW0 );    
                tempW3 = McrAbs(tempW1);
                if( tempW3 <= TAN_X_INP_25 )
                {
                    tempW2 = LCESP_s16IInter6Point( tempW3, TAN_X_INP_0, TAN_X_OUT_0, TAN_X_INP_5, TAN_X_OUT_5, TAN_X_INP_10, TAN_X_OUT_10, TAN_X_INP_15, TAN_X_OUT_15 ,TAN_X_INP_20, TAN_X_OUT_20 ,TAN_X_INP_25, TAN_X_OUT_25)  ;              
                }
                else if( tempW3 < TAN_X_INP_50 )
                {
                    tempW2 = LCESP_s16IInter6Point( tempW3 ,TAN_X_INP_25, TAN_X_OUT_25, TAN_X_INP_30, TAN_X_OUT_30, TAN_X_INP_35, TAN_X_OUT_35, TAN_X_INP_40, TAN_X_OUT_40, TAN_X_INP_45, TAN_X_OUT_45 ,TAN_X_INP_50, TAN_X_OUT_50);
                }
                else
                {
                    tempW2 = LCESP_s16IInter3Point( tempW3, TAN_X_INP_50, TAN_X_OUT_50, TAN_X_INP_55, TAN_X_OUT_55, TAN_X_INP_60, TAN_X_OUT_60 );  
                }
                
                if(lsesps16ADMwstr > 0)
                {
                    ldesps16ADMTireAngle = tempW2;
                }
                else
                {
                    ldesps16ADMTireAngle = -tempW2;
                }
            #endif
	    }
		else
		{
	  		ldesps16TireAngle = 0;
	  		#if ( (__ADAPTIVE_MODEL_VELO ==ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN ==ENABLE ) )
	  			ldesps16ADMTireAngle = 0;
	  		#endif
		}	
		
		ldesps16DeltaDynModel=(int16_t)( (((int32_t)ldesps16TireAngle)*ldesps16YawDynGain)/10000 );
		#if ( (__ADAPTIVE_MODEL_VELO ==ENABLE ) || (__ADAPTIVE_MODEL_VCH_GEN ==ENABLE ) )
			ldesps16ADMDeltaDynModel=(int16_t)( (((int32_t)ldesps16ADMTireAngle)*ldesps16YawDynGain)/10000 );
		#endif
	
	
		#if (__ADAPTIVE_MODEL_VELO ==ENABLE )
		    ldesps16AdmVcrtRatioApplyFvelo = LCESP_s16IInter2Point( vref5 , VREF_40_KPH , ldesps16AdmVcrtRatioApplyF , VREF_60_KPH , 1000 );	   	    
		    ldesps16VehicleVelocity        = (int16_t)(((int32_t)vref5*ldesps16AdmVcrtRatioApplyFvelo)/1000);
	    	
		    if(ldesps16VehicleVelocity <= S16_ACKERMANN_SPEED )
		    {
		        ldesps16VehicleVelocity = S16_ACKERMANN_SPEED;
		    }
		    else
		    {
		        ;
		    }	    
	    #else
		    if(vref5>S16_ACKERMANN_SPEED)
		    {
		    	ldesps16VehicleVelocity=vref5;		
		    }
		    else
		    {
		    	ldesps16VehicleVelocity=S16_ACKERMANN_SPEED;
		    }    
	    #endif
	
		ldesps32BetadBeta	=(  (int32_t)ldesps16GainBetadBeta *ldesps16BetaDynModel)  /ldesps16VehicleVelocity;
		ldesps32BetadYaw	=((((int32_t)ldesps32GainBetadYaw  *ldesps16YawDynModel)   /ldesps16VehicleVelocity)/ldesps16VehicleVelocity)-(((int32_t)ldesps16YawDynModel*10)/(1000/S16_BETA_CAL_SCALE)); /*hbjeon cycletime*/
		ldesps32BetadDelta	=( ((int32_t)ldesps16GainBetadDelta*ldesps16DeltaDynModel) /ldesps16VehicleVelocity)/S16_DELTA_CAL_SCALE;
		ldesps32YawdBeta	=(  (int32_t)ldesps16GainYawdBeta  *ldesps16BetaDynModel);
		ldesps32YawdYaw	    =(  (int32_t)ldesps16GainYawdYaw	  *ldesps16YawDynModel)/ldesps16VehicleVelocity;
		ldesps32YawdDelta	=(  (int32_t)ldesps16GainYawdDelta *ldesps16DeltaDynModel) /S16_DELTA_CAL_SCALE; 
	    
		ldesps32BetaDynModelOld  =	ldesps32BetaDynModel;
		ldesps32YawDynModelOld   =	ldesps32YawDynModel;
	
		ldesps32BetaDynModel     =	(ldesps32BetadBeta+ldesps32BetadYaw+ldesps32BetadDelta+ldesps32BetaDynModelOld);
		ldesps32YawDynModel      =	(ldesps32YawdBeta+ldesps32YawdYaw+ldesps32YawdDelta+ldesps32YawDynModelOld);
		
		ldesps16YawDynModelOld   =	ldesps16YawDynModel;
		ldesps16BetaDynModel     =  (int16_t)(ldesps32BetaDynModel/S16_BETA_CAL_SCALE);
		ldesps16YawDynModel      =	(int16_t)(ldesps32YawDynModel/S16_YAW_CAL_SCALE);
	
		/* Filtered yaw rate desired*/
		if(ldesps16VehicleVelocity < VREF_80_KPH )
		{
			tempW2=LCESP_s16IInter4Point(ldesps16VehicleVelocity,VREF_20_KPH,pEspModel->S16_YAWC_FILTER_20,VREF_40_KPH,pEspModel->S16_YAWC_FILTER_40,VREF_60_KPH,pEspModel->S16_YAWC_FILTER_60,VREF_80_KPH,128);
			ldespu8FilterYawDynModel=(uint8_t)(McrAbs(tempW2));	
		}
		else
		{
			ldespu8FilterYawDynModel=128;
		}   
		
		if(vref5 < S16_ACKERMANN_SPEED)
		{
			tempW2 = ((vref5 *347)/10);
			tempW1 = ldesps16DeltaDynModel;
			tempW0 = (pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR);
			s16muls16divs16();
			ldesps16ackermannyaw = tempW3;
		}
		else
		{
			;
		}
		
		if(ldespu8FilterYawDynModel>92)
		{
			if(vref5 < S16_ACKERMANN_SPEED)
			{
				ldesps16FiltdYawDynModel=ldesps16ackermannyaw;
			}
			else
			{
				ldesps16FiltdYawDynModel=ldesps16YawDynModel;
			}
		}
		else
		{
			if(vref5 < S16_ACKERMANN_SPEED)
			{
				ldesps16FiltdYawDynModel=LCESP_s16Lpf1Int(ldesps16ackermannyaw,ldesps16FiltdYawDynModel,ldespu8FilterYawDynModel);
			}
			else
			{
				ldesps16FiltdYawDynModel=LCESP_s16Lpf1Int(ldesps16YawDynModel,ldesps16FiltdYawDynModel,ldespu8FilterYawDynModel);
			}
		}
		
	    /* Filtered Betarate */
		tempW2=(int16_t)(ldesps32BetadBeta+ldesps32BetadYaw+ldesps32BetadDelta);
		tempW2=LCABS_s16LimitMinMax(tempW2,-5000,5000);/*possible limitation of beta rate */
		tempW1=(1000/S16_BETA_CAL_SCALE);
		tempW0=10;  /* Cycle time 7msec -> 7, 10msec -> 10 */
		s16muls16divs16();
		ldesps16BetarateDynModel=tempW3;
		
		/* Filtered Lat G desired*/
		tempW2=LCESP_s16IInter7Point(ldesps16VehicleVelocity,VREF_20_KPH,pEspModel->S16_LAT_G_FILTER_20,
															 VREF_40_KPH,pEspModel->S16_LAT_G_FILTER_40,
															 VREF_60_KPH,pEspModel->S16_LAT_G_FILTER_60,
															 VREF_80_KPH,pEspModel->S16_LAT_G_FILTER_80,
															 VREF_100_KPH,pEspModel->S16_LAT_G_FILTER_100,
															 VREF_120_KPH,pEspModel->S16_LAT_G_FILTER_120,
															 VREF_150_KPH,pEspModel->S16_LAT_G_FILTER_150);
		ldespu8FilterLatGDynModel=(uint8_t)(McrAbs(tempW2));
	
		/*prevent overestimating Vehicle model by excessive steer rate when sensor power on*/
		if(ldespu1ModelSteerStbOk == 1)
		{
		if(vref5 < S16_ACKERMANN_SPEED)
		{
			tempW2=(ldesps16ackermannyaw+ldesps16BetarateDynModel);
		}
		else
		{
			tempW2=(ldesps16YawDynModel+ldesps16BetarateDynModel);
		}
		}
		else
		{
			if(vref5 < S16_ACKERMANN_SPEED)
			{
				tempW2=(ldesps16ackermannyaw);
			}
			else
			{
				tempW2=(ldesps16YawDynModel);
			}
		}
		/*prevent overestimating Vehicle model by excessive steer rate when sensor power on*/

		tempW1=ldesps16VehicleVelocity;
		tempW0=1617;
		s16muls16divs16();
		if(ldespu8FilterLatGDynModel>92)
		{
			ldesps16FiltdLatGDynModel=tempW3;
		}
		else
		{
			ldesps16FiltdLatGDynModel=LCESP_s16Lpf1Int(tempW3, ldesps16FiltdLatGDynModel, ldespu8FilterLatGDynModel);
		}
	
		/* YawC DynModel */
		ldesps16YawRfDynModel = ldesps16FiltdYawDynModel;
	    
		/* AlatC DynModel */		
		/* 0.2g-> deg/s@kph, 20@20, 10@40, 5@80, 4@100, 2@200*/	
		ldesps16LatGLinearRange=(pEspModel->S16_MAX_ROAD_ADHESION*40)/5;
		ldesps16LatGLinearGain=pEspModel->S16_LAT_G_CAL_SCALE;
		ldesps16LatGExtraGain=LCESP_s16IInter6Point(ldesps16VehicleVelocity,VREF_40_KPH,pEspModel->S16_LAT_G_EXTRA_40,
																			VREF_60_KPH,pEspModel->S16_LAT_G_EXTRA_60,
																			VREF_80_KPH,pEspModel->S16_LAT_G_EXTRA_80,
																			VREF_100_KPH,pEspModel->S16_LAT_G_EXTRA_100,
																			VREF_120_KPH,pEspModel->S16_LAT_G_EXTRA_120,
																			VREF_150_KPH,pEspModel->S16_LAT_G_EXTRA_150);
		
		tempW2=ldesps16FiltdLatGDynModel;
		tempW1=ldesps16LatGLinearGain;
		tempW0=100;
		s16muls16divs16();
		ldesps16LatGLinearModel=tempW3;
		
		tempW2=ldesps16FiltdLatGDynModel;
		if( McrAbs(ldesps16LatGLinearModel) < ldesps16LatGLinearRange )
		{
			tempW1=ldesps16LatGLinearGain;
		}
		else if( McrAbs(ldesps16LatGLinearModel) < ((pEspModel->S16_MAX_ROAD_ADHESION)*10))
		{
			tempW1=LCESP_s16IInter2Point( McrAbs(ldesps16LatGLinearModel),ldesps16LatGLinearRange,pEspModel->S16_LAT_G_CAL_SCALE,((pEspModel->S16_MAX_ROAD_ADHESION)*10),ldesps16LatGExtraGain);
		}
		else
		{
			tempW1=ldesps16LatGExtraGain;
		}
	
		tempW0=100;
		s16muls16divs16();
		ldesps16LatGDynModel=tempW3;
		
		yawno = ldesps16YawRfDynModel;
		rf    = ldesps16YawRfDynModel;
		
		if ( ldesps16LatGDynModel > LAT_1G3G ) 
		{
			nor_alat = LAT_1G3G;
		}
		else if ( ldesps16LatGDynModel < -LAT_1G3G ) 
		{
			nor_alat = -LAT_1G3G;
		}
		else 
		{    
			nor_alat=ldesps16LatGDynModel;
		}   	
		nor_alat_unlimit=ldesps16LatGDynModel;
		nor_alat_unlimit_ems=ldesps16LatGDynModel;
	}
#endif

#if  (__ADAPTIVE_MODEL_VCH_GEN == ENABLE )|| (__ADAPTIVE_MODEL_VELO == ENABLE)
	void    LDESP_vDetADMInitialization(void)
	{
	    int8_t  i;
	  
	    if(speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS) 
	    {         		        
		    for (i=0;i<=9;i=i+1)
		    {
	            ADMvchGEN_LH.ldesps16ADM_1SecCalc[i]=0;
	            ADMvchGEN_LH.ldesps16ADM_10SecCalc[i]=0;	 
	            ADMvchGEN_RH.ldesps16ADM_1SecCalc[i]=0;
	            ADMvchGEN_RH.ldesps16ADM_10SecCalc[i]=0;            
	            ADMvelo_LH.ldesps16ADM_1SecCalc[i]=0;
	            ADMvelo_LH.ldesps16ADM_10SecCalc[i]=0;	
	            ADMvelo_RH.ldesps16ADM_1SecCalc[i]=0;
	            ADMvelo_RH.ldesps16ADM_10SecCalc[i]=0;	            
		    }
	    }    
	}
	
	void    LDESP_vADMreadEEP(void)
	{
	    #if  (__ADAPTIVE_MODEL_VCH_GEN == ENABLE )
		    ldespu1AdmVchGENeepReadOK = wbu1AdModelVchReadEep ;    
	    	ldesps16AdmVchGENEEPread  = wbs16AdModelVchFirstEep ; 
	    #endif
	    
	    #if (__ADAPTIVE_MODEL_VELO == ENABLE)	    
	    	ldespu1AdmVeloeepReadOK  = wbu1AdModelVeloReadEep ;    
	    	ldesps16AdmVcrtEEPread   = wbs16AdModelVeloFirstEep;  
		#endif
	}    
#endif

#if  __ADAPTIVE_MODEL_VCH_GEN == ENABLE
	void  LDESP_vCalModelVchGENCompMain(void)
	{
	    LDESP_vDetModelVchGENCheckCondition();
	    LDESP_vCalModelVchGENRatio();       
	    LDESP_vDetModelVchGENUpdate();
	    LDESP_vDetADMVcheepReq();
	}
	
	void  LDESP_vDetModelVchGENCheckCondition(void)
	{
	    uint8_t u8VchApply = 1;
	    
	    /*--------------------------------------------------------------
	    1. Inhibition                       condition 
	    2. Good Data Gathering              condition            
	    -----------------------------------------------------------------*/        
	    
	    /*------- 1. Inhibition Condtion -----*/
	    if( (lsespu1SASsigStbFlg==1)  && (fu1StrErrorDet==0)      && (fu1StrSusDet==0)     &&      /* 1. SAS Stable */
	        (fu1YawSenPwr1sOk   ==1)  && (fu1YawErrorDet==0)      && (fu1YawSusDet==0)     &&      /* 2. Yaw Sensor */
	        (fu1AySenPwr1sOk    ==1)  && (fu1AyErrorDet==0)       && (fu1AySusDet ==0)     &&      /* 3. Ay  Sensor */
	        (FLAG_BANK_SUSPECT  ==0)  && (FLAG_BANK_DETECTED==0)  &&                               /* 4. BANK       */
            #if (NEW_FM_ENABLE == ENABLE)
            	(fu1FMWheelFLErrDet==0) && (fu1FMWheelFRErrDet==0) && (fu1FMWheelRLErrDet==0) && (fu1FMWheelRRErrDet==0) &&  /* 5. WSS Error   */
            #else
            	(fu1WheelFLErrDet==0) && (fu1WheelFRErrDet==0) && (fu1WheelRLErrDet==0) && (fu1WheelRRErrDet==0) && /* 5. WSS Error   */
            #endif
	        (RTA_FL.RTA_SUSPECT_WL==0)&&(RTA_FR.RTA_SUSPECT_WL==0)&&(RTA_RL.RTA_SUSPECT_WL==0)&&(RTA_RR.RTA_SUSPECT_WL==0)
			#if __RTA_2WHEEL_DCT
				&&(RTA_FL.RTA_SUSPECT_2WHL==0)&&(RTA_FR.RTA_SUSPECT_2WHL==0)   
				&&(RTA_RL.RTA_SUSPECT_2WHL==0)&&(RTA_RR.RTA_SUSPECT_2WHL==0) 
			#endif 
			&& (u8VchApply == U8_ADM_VCH_APPLY )        		
	        )
	    {
	        ldespu1AdmVchGENInhibit = 0;    
	    }
	    else
	    {
	        ldespu1AdmVchGENInhibit = 1; 
	    }    
	    
	
	    /*-----  2. Vch  General Gathering Condition   ----*/    
	    
	    if(McrAbs(yaw_out)> S16_ADM_CURVE_YAW )
	    {   
	        tempW0 = (pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR);
	        tempW1 = (int16_t)(((int32_t)ldesps16VehicleVelocity*3472)/tempW0) ;
	        /*
	        tempW2 = (int16_t)( ((int32_t)yaw_out*(pEspModel->S16_STEERING_RATIO_10))/100) ;        
	        tempW3 = (int16_t)( ((int32_t)tempW1*wstr)/tempW2 );        
	        ldesps16AdmVchGENinSQRT =    10*(tempW3 - 100) ; 
	        */
	        
	        tempW2 = (int16_t)(((int32_t)ldesps16ADMDeltaDynModel*tempW1)/yaw_out) ;
	        ldesps16AdmVchGENinSQRT =    10*(tempW2 - 100) ; 
	    }
	    else
	    {
	        ;
	    } 
	
	    
	    /*-----  3. Vch  Noise Rejection Hysterisis Condition   ----*/
	    ldesps16DetYawAcc = McrAbs(YAWM_ACC.lsesps16SignalAccel) ;
	    if(ldesps16DetYawAcc <= S16_ADM_VCH_YAW_ACC_LIMIT)
	    {
	    	if(ldesps16DetYawAcc > ldesps16DetYawAccFilt)
	    	{
	    		ldesps16DetYawAccFilt = ldesps16DetYawAcc ;
	    	}
	    	else
	    	{
	    		if(ldespu8DetYawAccCnt<S16_ADM_VCH_YAW_ACC_TIME)
	    		{
	    			ldespu8DetYawAccCnt=ldespu8DetYawAccCnt + 1;
	    			
	    		}
	    		else
	    		{
	    			ldespu8DetYawAccCnt  = 0;
	    			ldesps16DetYawAccFilt = ldesps16DetYawAccFilt - 1 ;
	    		}
	    		
	    		if(ldesps16DetYawAcc > ldesps16DetYawAccFilt )
	    		{
	    			ldesps16DetYawAccFilt = ldesps16DetYawAcc ;
	    		}
	    		else
	    		{
	    			;
	    		}
	    		
	    	}
	    }
	    else
	    {
	    	;
	    }
	    
	    ldesps16DetDelYawAcc = McrAbs(DEL_YAW_ACC.lsesps16SignalAccel) ;
	    if(ldesps16DetDelYawAcc <= S16_ADM_VCH_DEL_YAW_ACC_LIMIT)
	    {
	    	if(ldesps16DetDelYawAcc > ldesps16DetDelYawAccFilt)
	    	{
	    		ldesps16DetDelYawAccFilt = ldesps16DetDelYawAcc ;
	    	}
	    	else
	    	{
	    		if(ldespu8DetDelYawAccCnt<S16_ADM_VCH_DEL_YAW_ACC_TIME)
	    		{
	    			ldespu8DetDelYawAccCnt=ldespu8DetDelYawAccCnt + 1;
	    		}
	    		else
	    		{
	    			ldespu8DetDelYawAccCnt  = 0;
	    			ldesps16DetDelYawAccFilt = ldesps16DetDelYawAccFilt - 1 ;
	    		}
	    		
	    		if(ldesps16DetDelYawAcc > ldesps16DetDelYawAccFilt )
	    		{
	    			ldesps16DetDelYawAccFilt = ldesps16DetDelYawAcc ;
	    		}
	    		else
	    		{
	    			;
	    		}
	    	}
	    }
	    else
	    {
	    	;
	    }
	    
	    esp_tempW0 = McrAbs(alat);
	    esp_tempW1 = McrAbs(WSTR_ACC.lsesps16SignalAccel);
	    esp_tempW2 = McrAbs(yaw_out);
	    esp_tempW3 = McrAbs(Bank_Angle_K);
	    	    
	    if( (ldespu1AdmVchGENInhibit==0)                 						    && /* 1. Inhibition                  */
	    	#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
		    	( (((fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0))&&(lsespu1PedalBrakeOn==0))||
		    	  ((lsespu1MpresByAHBgen3Invalid==0)&&(lsespu1MpresBrkOnByAHBgen3==0)) )&&
	    	#else
		        (MPRESS_BRAKE_ON==0)                   						        && /* 2. No brake                    */
	        #endif
	        (control_hold_flg==0)                       					        && /* 3. No control                  */
	        (steer_rate_deg_s2<=70)                   					            && /* 4. SAS rate-Think more         */
	        (esp_tempW0>S16_ADM_VCH_ALAT_MIN)                  					&&
	        (esp_tempW0<S16_ADM_VCH_ALAT_MAX)                  					&& /* 5. 0.1g < |alat| < 0.4g        */ 
	        (esp_tempW1 <  S16_ADM_VCH_WSTR_ACC_MAX   )   && /* 6. abs(Steer Accel)<1 deg/s    */ 
	        (ldesps16DetYawAccFilt   			  <  S16_ADM_VCH_YAW_ACC_MAX    )	&& /* 7-1. abs(YAWM  Accel)<1  deg/s   */
	        (ldesps16DetDelYawAccFilt 			  <  S16_ADM_VCH_DEL_YAW_ACC_MAX)   && /* 7-2. abs(DEL YAW  Accel)<1  deg/s   */
	        (SAS_CHECK_OK==1)                              						    &&                 /* 8. SAS OK                      */
	        (vref5 > S16_ADM_VCH_SPEED_MIN)                      					&& /* 9. Min Speed 70kph             */  
	        (vref5 < S16_ADM_VCH_SPEED_MAX)                      					&& /*10. Max Speed 120kph            */         
	        /* (ldespu1AdmVchSteerCondOK==1 )                       					&& */ /*11. 7 < WSTR                    */
	        (esp_tempW2 > S16_ADM_CURVE_YAW )                   				&& /*12. yaw > 5 deg/s               */
	        (det_beta_dot<S16_ADM_VCH_BETADOT_MAX)               					&& /*13. beta_dot < 1deg/s           */
	        (ldesps16AdmVchGENinSQRT > 500 )                     					&& /*14. Min value of sqrt           */
	        (lsabss16DecelRefiltByVref5 < S16_ADM_VCH_ACC_MAX )  					&& /*15. Vehicle accel 0.02g         */
	        (lsabss16DecelRefiltByVref5 > S16_ADM_VCH_DEC_MIN )  					&& /*16. Vehicle decel -0.07g        */  
	        ( esp_tempW3  < S16_ADM_BANK_LIMIT )       					&& /*17. BANK ANGLE < 1deg           */
	        ( (Rough_road_suspect_vehicle==0) && (Rough_road_detect_vehicle==0) )   && /*18. ROUGH ROAD==0               */
	        (vrad_diff_max < S16_ADM_VCH_WHEEL_SPIN_MAX)         					&& /*19. Wheel spin cond --> 바꿀것*/
	        (mtp < U8_ADM_ACCEL_THR)                             					&& /*20. Max Acceleration < 70%      */
	        (  ( (lsesps16ADMwstr>0)&&(yaw_out>0) ) ||  (( lsesps16ADMwstr<0) && (yaw_out<0 ))  ) 
	        )                                                                 
	    {
	        ldespu1AdmVchGENGoodCond = 1;
	    }
	    else
	    {
	        ldespu1AdmVchGENGoodCond = 0;
	    }        
	    
	           
	}
	
	void  LDESP_vCalModelVchGENRatio(void)
	{
	    int16_t  S16CalcSQRT;
	    int16_t  S16CalcVeloByAy, S16SQRTinputVelo2, S16CalcSQRT2 ;
	    
	    /*=============   Vch Calculation =====================
	    
	                                 V
	      Vch  =   -------------------------------------
	                 sqrt(  V*steer/(L*yaw)  -  1    )
	                 
	      =>           
	              V*steer            (V/(8*3.6))*(steer/(STR) )
	       1000*----------  =  1000*-------------------------------
	              L*yaw               (L/1000)*(yawm/100)
	              
	                             347*V        100*steer
	                        =  --------- * ----------------
	                              L          (STR*yaw/100)
	                              
	                              
	             V*steer             (V/(8*3.6))*(tan*100/STR * (f/10000))/100    
	       1000*---------- = 1000*--------------------------------------------------                       
	             L*yaw                (L/1000)*(yawm/100)
	             
	                           3472*V            ((tan*100/STR)*(f/10000)) 
	                       = -----------* 10*-------------------
	                             L                  yawm
	                       
	    --------------------------------------------------------------------------
	    
	                 sqrt(    V*(ay/yaw)   )
	      Vch  =   -------------------------------------
	                 sqrt(  V*steer/(L*yaw)  -  1    )    
	    
	    
	       ay         (ay/1000)*(9.81)*(3.6)*8          ay*202*8
	      ------  =  ----------------------------- = --------------
	       yaw         (yaw/100)*(pi/180)                yaw
	       
	                                             [     (vref/8)*(ay*202/yaw)  ] 
	                                    sqrtfunct[  ------------------------- ]*8*1000
	                                             [           10               ] 
	     sqrt(    V*(ay/yaw)   ) =   ------------------------------------------------------   
	                                                         10                   
	    
	                       
	    ===================================================================================*/
	
	    /*========  Roll Compensation ============*/
	    if( McrAbs(alat) <= S16_ADM_VCH_AY_COMP  )
	    {
	        ldesps16AdmVchAyComp  =   alat ;
	    }
	    else
	    {
	        if(alat > 0 )
	        {
	            ldesps16AdmVchAyComp = S16_ADM_VCH_AY_COMP +    (int16_t)(((int32_t)( alat - S16_ADM_VCH_AY_COMP)*S16_ADM_VCH_AY_COMP_PERCENT )/100 ) ;
	        }
	        else
	        {
	            ldesps16AdmVchAyComp = -S16_ADM_VCH_AY_COMP +    (int16_t)(((int32_t)( alat + S16_ADM_VCH_AY_COMP)*S16_ADM_VCH_AY_COMP_PERCENT )/100 ) ;
	        }
	    }
	
	    if(ldespu1AdmVchGENGoodCond==1  )
	    {
	        if( (S16_ADM_VCH_AY_COMP ==0) && (S16_ADM_VCH_AY_COMP_PERCENT==0 ) )
	        {        
		        S16CalcSQRT       =  LDESP_s16CalcSQRT( ldesps16AdmVchGENinSQRT ) ;
	    	    ldesps16AdmVchGEN =  (int16_t)( ((int32_t)ldesps16VehicleVelocity*1000)/S16CalcSQRT );
	        }
	        else
	        {               
	            S16CalcVeloByAy   =  (int16_t)( ((int32_t)ldesps16AdmVchAyComp*202)/yaw_out ) ;
	            S16SQRTinputVelo2 =  (int16_t)(((int32_t)ldesps16VehicleVelocity*S16CalcVeloByAy)/80 ) ;
	            S16CalcSQRT2      =  LDESP_s16CalcSQRT( S16SQRTinputVelo2 ) ; 
	
	            S16CalcSQRT       =  LDESP_s16CalcSQRT( ldesps16AdmVchGENinSQRT ) ;
	            ldesps16AdmVchGEN =  (int16_t)( ((int32_t)S16CalcSQRT2*800)/S16CalcSQRT );               
	        }
	        
	        if(lsesps16ADMwstr > 0 )
	        {
	            if(ldespu8AdmVchGENDataLHCNT < L_U8_TIME_10MSLOOP_1000MS )
	            {
	                ldespu8AdmVchGENDataLHCNT         =  ldespu8AdmVchGENDataLHCNT + 1;
	                ldesps32AdmVchGENCalcLHSum        =  ldesps32AdmVchGENCalcLHSum   + ldesps16AdmVchGEN ;
	            }
	            else
	            {
	                ldesps16AdmVchGEN1secLHMean  =  (int16_t)(ldesps32AdmVchGENCalcLHSum/ldespu8AdmVchGENDataLHCNT );
	                
	                ADM = (struct  ADAPTIVE_M *) &ADMvchGEN_LH;  
	                LDESP_vCalcADMaverageData(ldesps16AdmVchGEN1secLHMean , S16_ADM_VCH_THR_OK , U8_ADM_VCH_1ST_UPD_TIME , U8_ADM_VCH_2ND_UPD_TIME , U8_ADM_VCH_3RD_UPD_TIME);
	                            
	                ldesps32AdmVchGENCalcLHSum = 0;
	                ldespu8AdmVchGENDataLHCNT  = 0;                                                                            
	            }
	        }
	        else
	        {
	            if(ldespu8AdmVchGENDataRHCNT < L_U8_TIME_10MSLOOP_1000MS )
	            {
	                ldespu8AdmVchGENDataRHCNT         =  ldespu8AdmVchGENDataRHCNT + 1;
	                ldesps32AdmVchGENCalcRHSum        =  ldesps32AdmVchGENCalcRHSum   + ldesps16AdmVchGEN ;
	            }
	            else
	            {
	                ldesps16AdmVchGEN1secRHMean  =  (int16_t)(ldesps32AdmVchGENCalcRHSum/ldespu8AdmVchGENDataRHCNT );
	                
	                ADM = (struct  ADAPTIVE_M *) &ADMvchGEN_RH;  
	                LDESP_vCalcADMaverageData(ldesps16AdmVchGEN1secRHMean , S16_ADM_VCH_THR_OK , U8_ADM_VCH_1ST_UPD_TIME , U8_ADM_VCH_2ND_UPD_TIME , U8_ADM_VCH_3RD_UPD_TIME);
	                            
	                ldesps32AdmVchGENCalcRHSum = 0;
	                ldespu8AdmVchGENDataRHCNT  = 0;                                                                            
	            }            
	 
	        }
	        
	        if( ADMvchGEN_LH.ldespu1ADM_10SecCheckOK == 1)
	        {
	            ldesps16AdmVchGENLH = ADMvchGEN_LH.ldesps16ADM_100SecCalcMean ;    
	        }
	        else if(ADMvchGEN_LH.ldespu1ADM_1SecFirstOK==1 )
	        {
	            ldesps16AdmVchGENLH = ADMvchGEN_LH.ldesps16ADM_1SecCalcMean ;                
	        }
	        else
	        {
	            ldesps16AdmVchGENLH = pEspModel->S16_ADM_VCH_NOMINAL ;
	        }
	        
	        if( ADMvchGEN_RH.ldespu1ADM_10SecCheckOK == 1)
	        {
	            ldesps16AdmVchGENRH = ADMvchGEN_RH.ldesps16ADM_100SecCalcMean ;    
	        }
	        else if(ADMvchGEN_RH.ldespu1ADM_1SecFirstOK==1 )
	        {
	            ldesps16AdmVchGENRH = ADMvchGEN_RH.ldesps16ADM_1SecCalcMean ;                
	        }
	        else
	        {
	            ldesps16AdmVchGENRH = pEspModel->S16_ADM_VCH_NOMINAL ;
	        }                
	        
	        ldesps16AdmVchGEN_F = (ldesps16AdmVchGENLH + ldesps16AdmVchGENRH )/2 ;
	        ldesps16AdmVchGEN_F = LCABS_s16LimitMinMax(ldesps16AdmVchGEN_F , VREF_60_KPH , VREF_120_KPH );        
	    }
	    else
	    {
	        ;
	    }    
	}
	
	void  LDESP_vDetModelVchGENUpdate(void)
	{
	    int16_t s16VchDiff;
	    uint8_t u8VchApply = 1;
	    
	   /*-----  1. Vch Change Determine  condition ----*/ 
	    if(ldespu1AdmVchGENCrtApply == 1)
	    {
	        ldespu1AdmVchGENCrtApply = 0;
	    }
	    else
	    {                 
	        s16VchDiff = McrAbs( ldesps16AdmVchApplyF - ldesps16AdmVchGEN_F ) ;
	            
	        if(ldespu1AdmVchGENCrtDuring ==1 )
	        {
	            if( (ADMvchGEN_LH.ldespu1ADM_Update3rdOK==1) &&  (ADMvchGEN_RH.ldespu1ADM_Update3rdOK==1) )
	            {
	                if(ldespu1AdmVch3rdUpdateOK==0)
	        	    {
	                    ldespu1AdmVchGENCrtOK    = 1; 
	    	            ldespu1AdmVchGENCrtApply = 1;
	                    ldespu8AdmVchUpdPercent  = U8_ADM_VCH_3RD_UPD_PERCENT ;
	                    ldespu1AdmVch3rdUpdateOK = 1;
	                }
	                else
	                {
	                    if( (ADMvchGEN_LH.ldespu1ADM_10SecUpdateFlg==1) && (ADMvchGEN_RH.ldespu1ADM_10SecUpdateFlg==1) )
	                    {
	            		    ldespu1AdmVchGENCrtOK    = 1;  
	                        ldespu1AdmVchGENCrtApply = 1;
	                        ldespu8AdmVchUpdPercent  = U8_ADM_VCH_3RD_UPD_PERCENT ;
	        	        	ADMvchGEN_LH.ldespu1ADM_10SecUpdateFlg = 0;
	            	    	ADMvchGEN_RH.ldespu1ADM_10SecUpdateFlg = 0;
	                    }
	                    else
	                    {
	                        ;
	                    }                                        
	                }                
	            }
	            else if( (ldespu1AdmVch2ndUpdateOK==0) && (ADMvchGEN_LH.ldespu1ADM_Update2ndOK==1) &&  (ADMvchGEN_RH.ldespu1ADM_Update2ndOK==1) )
	            {
	                ldespu1AdmVchGENCrtOK    = 1; 
	                ldespu1AdmVchGENCrtApply = 1;
	                ldespu8AdmVchUpdPercent  = U8_ADM_VCH_2ND_UPD_PERCENT ;
	                ldespu1AdmVch2ndUpdateOK = 1;
	            }
	            else if( (ldespu1AdmVch1stUpdateOK==0) && (ADMvchGEN_LH.ldespu1ADM_Update1stOK==1) &&  (ADMvchGEN_RH.ldespu1ADM_Update1stOK==1) )
	            {
	                ldespu1AdmVchGENCrtOK    = 1;   
	                ldespu1AdmVchGENCrtApply = 1;
	                ldespu8AdmVchUpdPercent  = U8_ADM_VCH_1ST_UPD_PERCENT ;
	                ldespu1AdmVch1stUpdateOK = 1;                
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if( (ADMvchGEN_LH.ldespu1ADM_Update1stOK==1) && (ADMvchGEN_RH.ldespu1ADM_Update1stOK==1) &&
	                ( s16VchDiff > S16_ADM_VCH_CRT_THR )  )
	            {
	                ldespu1AdmVchGENCrtDuring = 1;
	            }
	            else
	            {
	                ;
	            }
	        }        
	        
	        /*-----  2. Final Vch Apply  condition    ----*/ 
	        if(ldespu1AdmVchGENCrtOK == 1)
	        {
	            if(ldespu1AdmVchGENCrtApply==1)
	            {   
	                ldesps16AdmVchApplyF =  ldesps16AdmVchApplyF + (int16_t)(((int32_t)(ldesps16AdmVchGEN_F- ldesps16AdmVchApplyF)*ldespu8AdmVchUpdPercent)/100) ; 
	                ldesps16AdmVchApplyF =  LCABS_s16LimitMinMax(ldesps16AdmVchApplyF , ( pEspModel->S16_ADM_VCH_NOMINAL - S16_ADM_VCH_CRT_THR_F ) , ( pEspModel->S16_ADM_VCH_NOMINAL + S16_ADM_VCH_CRT_THR_F ) );                         
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if(ldespu1AdmVchGENeepReadOK == 1 )
	            {
	                ldesps16AdmVchApplyF = ldesps16AdmVchGENEEPread ;            
	            }
	            else
	            {
	                ldesps16AdmVchApplyF = pEspModel->S16_ADM_VCH_NOMINAL ;
	            }                 
	        }
	        
	        ldesps16AdmVchApplyF =  LCABS_s16LimitMinMax(ldesps16AdmVchApplyF , ( pEspModel->S16_ADM_VCH_NOMINAL - S16_ADM_VCH_CRT_THR_F ) , ( pEspModel->S16_ADM_VCH_NOMINAL + S16_ADM_VCH_CRT_THR_F ) );           
	    }
	    
	    
	    if(u8VchApply == U8_ADM_VCH_APPLY )
	    {
	        ;
	    }                
	    else
	    {
	        ldesps16AdmVchApplyF = pEspModel->S16_ADM_VCH_NOMINAL ;
	    }
	            
	}
	
	  
	void  LDESP_vDetADMVchGENReset(void)
	{
	    int8_t  i;
	    
	    ldesps16AdmVchGEN1secLHMean = 0;
	    ldesps32AdmVchGENCalcLHSum  = 0;
	    ldespu8AdmVchGENDataLHCNT   = 0;
	  
	    ldesps16AdmVchGEN1secRHMean = 0;
	    ldesps32AdmVchGENCalcRHSum  = 0;
	    ldespu8AdmVchGENDataRHCNT   = 0;
	        
		for (i=0;i<=9;i=i+1)
		{ 
	        ADMvchGEN_LH.ldesps16ADM_1SecCalc[i] =0;
	        ADMvchGEN_LH.ldesps16ADM_10SecCalc[i]=0;	
	        
	        ADMvchGEN_RH.ldesps16ADM_1SecCalc[i] =0;
	        ADMvchGEN_RH.ldesps16ADM_10SecCalc[i]=0;	        
		}       
		           
	    ADMvchGEN_LH.ldesps16ADM_1SecCalcMAX       =0;          
	    ADMvchGEN_LH.ldesps16ADM_1SecCalcMIN       =0;          
	    ADMvchGEN_LH.ldesps16ADM_10SecCalcMean     =0;        
	    ADMvchGEN_LH.ldesps16ADM_100SecCalcMean    =0;       
	    ADMvchGEN_LH.ldesps16ADM_100SecCalcMeanOld =0;    
	    ADMvchGEN_LH.ldespu8ADM_1SecCalcCnt        =0;           
	    ADMvchGEN_LH.ldespu8ADM_10SecCalcCnt       =0;                          
	    ADMvchGEN_LH.ldespu1ADM_10SecCalcOK        =0;	      
	    ADMvchGEN_LH.ldespu1ADM_1SecFirstOK        =0;      
	    ADMvchGEN_LH.ldespu1ADM_100SecCalcOK       =0;                    
	    ADMvchGEN_LH.ldespu1ADM_10SecFirstOK       =0;      
	    ADMvchGEN_LH.ldespu1ADM_100SecFirstOK      =0;       
	    
	    ADMvchGEN_RH.ldesps16ADM_1SecCalcMAX       =0;          
	    ADMvchGEN_RH.ldesps16ADM_1SecCalcMIN       =0;          
	    ADMvchGEN_RH.ldesps16ADM_10SecCalcMean     =0;        
	    ADMvchGEN_RH.ldesps16ADM_100SecCalcMean    =0;       
	    ADMvchGEN_RH.ldesps16ADM_100SecCalcMeanOld =0;    
	    ADMvchGEN_RH.ldespu8ADM_1SecCalcCnt        =0;           
	    ADMvchGEN_RH.ldespu8ADM_10SecCalcCnt       =0;                          
	    ADMvchGEN_RH.ldespu1ADM_10SecCalcOK        =0;	  
	    ADMvchGEN_RH.ldespu1ADM_1SecFirstOK        =0;      
	    ADMvchGEN_RH.ldespu1ADM_100SecCalcOK       =0;      
	    ADMvchGEN_RH.ldespu1ADM_10SecFirstOK       =0;      
	    ADMvchGEN_RH.ldespu1ADM_100SecFirstOK      =0;          
	}
	
	void  LDESP_vDetADMVcheepReq(void)
	{
	    int16_t  s16AdmVchDiffforEEPwrite;
	
	    if( ldespu1AdmVchGENCrtOK == 1)
		{
	        if(ldespu1AdmVchGENeepReadOK == 1)
	    	{
	            s16AdmVchDiffforEEPwrite = McrAbs( ldesps16AdmVchApplyF - ldesps16AdmVchGENEEPread  );
	    	}
	    	else
	    	{
	            s16AdmVchDiffforEEPwrite = McrAbs( ldesps16AdmVchApplyF - pEspModel->S16_ADM_VCH_NOMINAL  );
	    	}    
	    
	    	if(s16AdmVchDiffforEEPwrite >= S16_ADM_VCH_EEP_THR )
	    	{
	            ldespu1AdmVchGENeepWriteReq = 1 ;
	    	}
	    	else
	    	{
	            ldespu1AdmVchGENeepWriteReq = 0;
	    	}        
	    }
	    else
	    {
	        ;
	    }
	    
	    
	}   
#endif
    
int16_t  LDESP_s16CalcSQRT(int16_t s16Input )
{
	s16Input = LCABS_s16LimitMinMax(s16Input , 300 , 5000 );

	/*------- SQRT INTERPOLATION ---------------------------------------
	  sqrt(0.3)=0.547  ==>   input(300) -> output(547) 
	  sqrt(0.5)=0.707  ==>   input(500) -> output(707) 
	  sqrt(0.8)=0.894  ==>   input(800) -> output(894) 
	  sqrt(1)  =1      ==>   input(1000)-> output(1000) 
	  sqrt(1.5)=1.225  ==>   input(1500)-> output(1225)
	  sqrt(2)  =1.414  ==>   input(2000)-> output(1414)
	  sqrt(3)  =1.732  ==>   input(3000)-> output(1732)
	  sqrt(4)  =2      ==>   input(4000)-> output(2000)
	  sqrt(5)  =2.236  ==>   input(5000)-> output(2236)
	--------------------------------------------------------------------*/
    if(s16Input <= SQRT_X_INP_2G0)
    {
       tempW0 =  LCESP_s16IInter6Point( s16Input, SQRT_X_INP_0G3 , SQRT_Y_OUT_0G3 , SQRT_X_INP_0G5, SQRT_Y_OUT_0G5, SQRT_X_INP_0G8, SQRT_Y_OUT_0G8, SQRT_X_INP_1G0, SQRT_Y_OUT_1G0, SQRT_X_INP_1G5, SQRT_Y_OUT_1G5, SQRT_X_INP_2G0, SQRT_Y_OUT_2G0 );
    }
    else
    {
       tempW0 =  LCESP_s16IInter4Point( s16Input, SQRT_X_INP_2G0, SQRT_Y_OUT_2G0, SQRT_X_INP_3G0, SQRT_Y_OUT_3G0, SQRT_X_INP_4G0, SQRT_Y_OUT_4G0, SQRT_X_INP_5G0, SQRT_Y_OUT_5G0 );
    }

    return tempW0;
}    

#if  __ADAPTIVE_MODEL_VELO == ENABLE
	void  LDESP_vCalModelVeloCompMain(void)
	{
	    LDESP_vCalModelVeloRefSpeed();
	    LDESP_vDetModelVeloCheckCondition();
	    LDESP_vCalModelVeloRatio();  
	    LDESP_vDetModelVeloUpdate();   
	    LDESP_vDetModelVeloeepReq();  
	}
	
	void    LDESP_vCalModelVeloRefSpeed(void)
	{
	    int16_t  s16ADMWheelBaseTemp;
	    int16_t  s16ADMSteerInputTemp1, s16ADMSteerInputTemp2;    
	    int16_t  s16ADMSteerVch;
	   
	    /*--------- V correction ratio calculation --------------------------------
	    
	                Vwheelsensor           Vwheelsensor
	      a  =  ------------------ =  ----------------------- 
	                Vreal                  Vest
	                
	                
	      Vwheelsensor = vref5 
	      
	                             2*L                                              yaw 
	      Vest = ------------------------------------------------------------* ---------
	             tan(δ)/STR  + sqrt( (tan(δ)/STR )^2  -( 2*L*yaw/(f*Vch) )^2)      f
	             
	             
	                   2*L           yaw 
	           = ---------------- * -----       
	               steer_input        f
	               
	       [Resolution Conversion]
	       
	                      2*(L/1000)          (yawm/100)
	       Vest(m/s) = ------------------ * -------------
	                    (steer_input/100)     (f/10000)
	               
	                     (2*L/10) *  (yawm/10)
	                 = ----------------------------   
	                    (steer_input*f/1000)
	                    
	
	       Vest(kph,resol8) =  (3.6*8)* Vest(m/s) 
	       
	                             (36*8)
	                        =   --------* Vest(m/s)                          
	                              10
	                              
	                            (288*L/5000)*(yawm)  
	                        =  -----------------------      
	                           (steer_input*f/10000  )
	
	
	
	
	          Vwheelsensor
	    a = --------------- = ( 1 + △a )
	           Vest
	      
	      Vwheelsensor - Vest  =  △a*Vest
	      
	      error = Σ (Vwheelsensor - Vest   - △a*Vest )^2
	      
	      d(error)/d(△a) = 0
	      
	                Σ { (Vwheelsensor - Vest )*(Vest )  } 
	      △a  =  ----------------------------------
	                Σ ( Vest^2 )
	                
	    
	     ==> Resolution Fitting
	     
	                       Σ { (10*(Vwheelsensor - Vest ))*(Vest )  } 
	      1000*△a =  -------------------------------------------------             
	                      Σ (  (Vest)^2 )/100
	                      
	               = 1  ( 0.1 % )                                  
	                  
	    -----------------------------------------------------------------------------*/
	    /*------------- SQRT CALCULATION ------------------------------------------
	        
	        [     tanδ            2*L*yaw         ]
	    sqrt[  (-------)^2  -  ( ----------)^2    ]
	        [     STR               Vch           ] 
	    
	    
	    ----------------------------------------------------------------------------*/
	    
	    #if (__ADAPTIVE_MODEL_VCH_GEN == ENABLE)  
	        ldesps16AdmVchForVcrtRatio = ldesps16AdmVchApplyF ;
	    #else
	        ldesps16AdmVchForVcrtRatio = pEspModel->S16_ADM_VCH_NOMINAL ; 
	    #endif
	
	    /*-- Think more --*/
	    tempW0 = McrAbs(lsesps16ADMwstr);
	    if( tempW0 > WSTR_30_DEG )  
	    {
	        tempW0 = (pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR);
	        s16ADMWheelBaseTemp   =  (int16_t)(((int32_t)tempW0*288)/5000) ;
	        
	        /*----- sqrt( ) ----*/
	        tempW1         =  (int16_t)(((int32_t)ldesps16AdmVchForVcrtRatio*173)/10) ;
	        s16ADMSteerVch =  (int16_t)(((int32_t)tempW0*yaw_out)/tempW1);
	        
	        tempW2 = McrAbs( (ldesps16ADMDeltaDynModel - s16ADMSteerVch)/10  );
	        tempW3 = McrAbs( (ldesps16ADMDeltaDynModel + s16ADMSteerVch)/10  ) ;
	        
	        if(tempW2 <= (SQRT_X_INP_2G0/10) )
	        {
	           tempW4 =  LCESP_s16IInter6Point( tempW2, SQRT_X_INP_0G3/10 , SQRT_Y_OUT_0G3 , SQRT_X_INP_0G5/10, SQRT_Y_OUT_0G5, SQRT_X_INP_0G8/10, SQRT_Y_OUT_0G8, SQRT_X_INP_1G0/10, SQRT_Y_OUT_1G0, SQRT_X_INP_1G5/10, SQRT_Y_OUT_1G5, SQRT_X_INP_2G0/10, SQRT_Y_OUT_2G0 );
	        }
	        else if( tempW2 <=(SQRT_X_INP_7G0/10))
	        {
	           tempW4 =  LCESP_s16IInter6Point( tempW2, SQRT_X_INP_2G0/10, SQRT_Y_OUT_2G0, SQRT_X_INP_3G0/10, SQRT_Y_OUT_3G0, SQRT_X_INP_4G0/10, SQRT_Y_OUT_4G0, SQRT_X_INP_5G0/10, SQRT_Y_OUT_5G0, SQRT_X_INP_6G0/10, SQRT_Y_OUT_6G0 , SQRT_X_INP_7G0/10, SQRT_Y_OUT_7G0 );
	        }
	        else
	        {
	           tempW4 =  LCESP_s16IInter4Point( tempW2, SQRT_X_INP_7G0/10, SQRT_Y_OUT_7G0, SQRT_X_INP_8G0/10, SQRT_Y_OUT_8G0, SQRT_X_INP_9G0/10, SQRT_Y_OUT_9G0, SQRT_X_INP_10G0/10, SQRT_Y_OUT_10G0);
	        }
	        
	        if(tempW3 <= (SQRT_X_INP_2G0/10) )
	        {
	           tempW5 =  LCESP_s16IInter6Point( tempW3, SQRT_X_INP_0G3/10 , SQRT_Y_OUT_0G3 , SQRT_X_INP_0G5/10, SQRT_Y_OUT_0G5, SQRT_X_INP_0G8/10, SQRT_Y_OUT_0G8, SQRT_X_INP_1G0/10, SQRT_Y_OUT_1G0, SQRT_X_INP_1G5/10, SQRT_Y_OUT_1G5, SQRT_X_INP_2G0/10, SQRT_Y_OUT_2G0 );
	        }
	        else if( tempW3 <=(SQRT_X_INP_7G0/10))
	        {
	           tempW5 =  LCESP_s16IInter6Point( tempW3, SQRT_X_INP_2G0/10, SQRT_Y_OUT_2G0, SQRT_X_INP_3G0/10, SQRT_Y_OUT_3G0, SQRT_X_INP_4G0/10, SQRT_Y_OUT_4G0, SQRT_X_INP_5G0/10, SQRT_Y_OUT_5G0, SQRT_X_INP_6G0/10, SQRT_Y_OUT_6G0 , SQRT_X_INP_7G0/10, SQRT_Y_OUT_7G0 );
	        }
	        else
	        {
	           tempW5 =  LCESP_s16IInter4Point( tempW3, SQRT_X_INP_7G0/10, SQRT_Y_OUT_7G0, SQRT_X_INP_8G0/10, SQRT_Y_OUT_8G0, SQRT_X_INP_9G0/10, SQRT_Y_OUT_9G0, SQRT_X_INP_10G0/10, SQRT_Y_OUT_10G0);
	        }
	       
	        if( lsesps16ADMwstr > 0 )
	        {
	            ldesps16TireAngleVch =  (int16_t)(((int32_t)tempW4*tempW5)/1000) ;
	            
	            if(ldesps16TireAngleVch  < 0 )
	            {
	                ldesps16TireAngleVch = 0;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            ldesps16TireAngleVch =  -(int16_t)(((int32_t)tempW4*tempW5)/1000) ; 
	            
	            if(ldesps16TireAngleVch > 0 )
	            {
	                ldesps16TireAngleVch = 0;
	            }
	            else
	            {
	                ;
	            }            
	        }
	            
	        s16ADMSteerInputTemp1 =  ldesps16ADMDeltaDynModel + ldesps16TireAngleVch ;      
	        
	        if(ldesps16YawDynGain < 1000)
	        {
	            ldesps16YawDynGain = 1000 ;        
	        }
	        else
	        {
	            ;
	        }        
	
	        ldesps16AdmVestUsingYawSteer =   (int16_t)( ((int32_t)s16ADMWheelBaseTemp*yaw_out)/s16ADMSteerInputTemp1 ) ;
	                
	        ldesps16AdmVcrtVeloDiff       =   (vref5 -  ldesps16AdmVestUsingYawSteer );
	        
	        /* radius calculation */
	        /*
	                2*L                      2*(L/1000)              (L*573/50)
	        R = --------------  =  ---------------------------- = -----------------------  [m]
	             steer_input       (steer_input/100)*(pi/180)           steer_input                
	        */
	        
	        tempW0 = (pEspModel->S16_LENGTH_CG_TO_FRONT + pEspModel->S16_LENGTH_CG_TO_REAR);
	        tempW1 = (int16_t)(((int32_t)tempW0*573)/50);
	        ldesps16AdmVcrtRadius = McrAbs(tempW1/s16ADMSteerInputTemp1) ;
	    }
	    else
	    {
	        ;
	    }
	}
	
	void    LDESP_vDetModelVeloCheckCondition(void)
	{
	    int16_t  s16AdmVeloFrtRrAbsYawDiff ;
	    int16_t  s16AdmVeloCorrSpeedLimit ;
	    uint8_t u8VeloApply = 1;
	    int16_t  s16AdmVeloLHsideVelDiff;
	    int16_t  s16AdmVeloRHsideVelDiff;
	    
	    s16AdmVeloLHsideVelDiff = McrAbs( FL.vrad_crt_resol_change  - RL.vrad_crt_resol_change  ) ;
	    s16AdmVeloRHsideVelDiff = McrAbs( FR.vrad_crt_resol_change  - RR.vrad_crt_resol_change  ) ;
	    
	    /*--------------------------------------------------------------
	    1. Inhibition                                           condition 
	    2. Wheel spin determine condition  FRT/REAR YAW MODEL   condition
	    3. Velocity & Steer angle                               condition 
	    4. Good Data Gathering                                  condition             
	    -----------------------------------------------------------------*/        
	    
	    /*------- 1. Inhibition Condtion -----*/
	    if( (lsespu1SASsigStbFlg==1)  && (fu1StrErrorDet==0)      && (fu1StrSusDet==0)     &&      /* 1. SAS Stable */
	        (fu1YawSenPwr1sOk   ==1)  && (fu1YawErrorDet==0)      && (fu1YawSusDet==0)     &&      /* 2. Yaw Sensor */
	        (fu1AySenPwr1sOk    ==1)  && (fu1AyErrorDet==0)       && (fu1AySusDet ==0)     &&      /* 3. Ay  Sensor */
	        (FLAG_BANK_SUSPECT  ==0)  && (FLAG_BANK_DETECTED==0)  &&                               /* 4. BANK       */ 
            #if (NEW_FM_ENABLE == ENABLE)
            	(fu1FMWheelFLErrDet==0) && (fu1FMWheelFRErrDet==0) && (fu1FMWheelRLErrDet==0) && (fu1FMWheelRRErrDet==0) &&  /* 5. WSS Error   */
            #else
            	(fu1WheelFLErrDet==0) && (fu1WheelFRErrDet==0) && (fu1WheelRLErrDet==0) && (fu1WheelRRErrDet==0) && /* 5. WSS Error   */
            #endif
	        (RTA_FL.RTA_SUSPECT_WL==0)&&(RTA_FR.RTA_SUSPECT_WL==0)&&(RTA_RL.RTA_SUSPECT_WL==0)&&(RTA_RR.RTA_SUSPECT_WL==0)
			#if __RTA_2WHEEL_DCT
			&&(RTA_FL.RTA_SUSPECT_2WHL==0)&&(RTA_FR.RTA_SUSPECT_2WHL==0)   
			&&(RTA_RL.RTA_SUSPECT_2WHL==0)&&(RTA_RR.RTA_SUSPECT_2WHL==0) 
			#endif       
			&& (u8VeloApply == U8_ADM_VELO_APPLY)  
	        )
	    {
	        ldespu1AdmVeloInhibit = 0;    
	    }
	    else
	    {
	        ldespu1AdmVeloInhibit = 1; 
	    }  

	    /*------2.  Wheel spin determine condition  FRT/REAR YAW MODEL     */
	    s16AdmVeloFrtRrAbsYawDiff = McrAbs( KYAW.ALGO_MODEL[1] - KYAW.ALGO_MODEL[2] ) ;
	    
	    if( ldespu1AdmVeloWheelSpin ==1 )
	    {
	        if(  s16AdmVeloFrtRrAbsYawDiff < YAW_2DEG  )
	        {
	            ldespu8AdmVeloWheelSpinCNT = ldespu8AdmVeloWheelSpinCNT + 1 ;
	        }
	        else if ( s16AdmVeloFrtRrAbsYawDiff < YAW_5DEG )
	        {
	            if(ldespu8AdmVeloWheelSpinCNT > 0 )
	            {
	                ldespu8AdmVeloWheelSpinCNT = ldespu8AdmVeloWheelSpinCNT - 1 ;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            ldespu8AdmVeloWheelSpinCNT = 0;    
	        }
	        
	        if(ldespu8AdmVeloWheelSpinCNT > S16_ADM_WHEEL_YAW_STB_TIME )
	        {
	            ldespu1AdmVeloWheelSpin = 0 ;
	        }
	        else
	        {
	            ;
	        }
	    }
	    else
	    {
	        if( s16AdmVeloFrtRrAbsYawDiff > YAW_5DEG )
	        {
	            ldespu1AdmVeloWheelSpin = 1; 
	        }
	        else
	        {
	            ;   
	        }
	        
	        ldespu8AdmVeloWheelSpinCNT = 0;  
	    }
	    
	    /*-----  3. Velocity & Steer angle condition         ----*/
	    tempW0 = McrAbs(lsesps16ADMwstr);
	    if(tempW0 > WSTR_90_DEG)
	    {        
	        s16AdmVeloCorrSpeedLimit = LCESP_s16IInter2Point( McrAbs(lsesps16ADMwstr) , WSTR_90_DEG , VREF_20_KPH, WSTR_120_DEG, VREF_30_KPH); 
	        
	        if(vref5 <= s16AdmVeloCorrSpeedLimit )
	        {
	            ldespu1AdmVeloSteerCondFlg = 1;
	        }
	        else
	        {
	            ldespu1AdmVeloSteerCondFlg = 0;
	        }        
		}
	    else
	    {
	        ldespu1AdmVeloSteerCondFlg = 0;
	    }
	                
	    esp_tempW0 = (wstr_correction + VREF_1_KPH_RESOL_CHANGE);
	    esp_tempW2 = McrAbs(alat);
	    esp_tempW3 = McrAbs(WSTR_ACC.lsesps16SignalAccel);
	    esp_tempW4 = McrAbs(YAWM_ACC.lsesps16SignalAccel);
	    esp_tempW5 = McrAbs(lsesps16ADMwstr);
	    esp_tempW6 = McrAbs(yaw_out);
	    esp_tempW7 = McrAbs(nosign_delta_yaw_first);
	    esp_tempW8 = McrAbs(Beta_MD_Dot);
	    
	    /*-----  4. Velocity ratio change Gathering Condition ----*/    
	    if( (ldespu1AdmVeloInhibit==0)                       						&&  /* 1. Inhibition                  */
	    	#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
		    	( (((fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0))&&(lsespu1PedalBrakeOn==0))||
		    	  ((lsespu1MpresByAHBgen3Invalid==0)&&(lsespu1MpresBrkOnByAHBgen3==0)) )&&
	    	#else
	        	(MPRESS_BRAKE_ON==0)	                       						&&  /* 2. No brake                    */
	        #endif
	        (control_hold_flg==0)                            						&&  /* 3. No control                  */
	        (esp_tempW2<S16_ADM_VELO_ALAT_MAX)             						&&  /* 5.        |alat| < 0.5g        */ 
	        (esp_tempW3 <  30   )  						&&  /* 6. abs(Steer Accel)<3 deg/s    */ 
	        (esp_tempW4 <  30   )  						&&  /* 7. abs(YAWM  Accel)<3 deg/s    */
	        (SAS_CHECK_OK==1)                                						&&  /* 8. SAS OK                      */
	        (vref5 > S16_ADM_VELO_SPEED_MIN)                 						&&  /* 9.   5KPH < VREF < 30KPH       */
	        (ldespu1AdmVeloSteerCondFlg==1 )                 						&&  /*10.  90deg <|wstr|  < 360deg    */
	        (esp_tempW5 > S16_ADM_VELO_WSTR_MIN )          						&&
	        (esp_tempW5 < S16_ADM_VELO_WSTR_MAX )          						&&
	        (esp_tempW6 > S16_ADM_VELO_YAW_MIN )        						&&  /*11.         |yaw| > 5 deg/s     */
	        (ldesps16AdmVcrtRadius< S16_ADM_VELO_RADIUS_MAX )						&&  /* THINK MORE */  /*12.  Radius R < 27m             */        
	        (  ( (lsesps16ADMwstr>0)&&(yaw_out>0) ) ||  (( lsesps16ADMwstr<0) && (yaw_out<0 ))  ) 		&&
	        (ldespu1AdmVeloWheelSpin ==0 )                   						&&  /*13. No wheel spin condition     */
	        (esp_tempW7 < YAW_5DEG )     						&&  /*14. delta yaw < 5deg/s          */
	        (det_beta_dot < S16_ADM_VELO_BETADOT_MAX)        						&&  /*15. det_beta dot <  4deg/s      */
	        ((Rough_road_suspect_vehicle==0) && (Rough_road_detect_vehicle==0) )	&&  /*16. ROUGH ROAD==0               */
	        (s16AdmVeloLHsideVelDiff < esp_tempW0)&&  /*17. Side Wheel spin diff        */
	        (s16AdmVeloRHsideVelDiff < esp_tempW0)&&
	        (esp_tempW8<S16_ADM_VELO_MD_BETADOT_MAX)      
	         )                                                                   
	    {
	        ldespu1AdmVeloRGoodCond = 1;
	    }
	    else
	    {
	        ldespu1AdmVeloRGoodCond = 0;
	    }                
	}

	void    LDESP_vCalModelVeloRatio(void)
	{
	    if(ldespu1AdmVeloRGoodCond==1  )
	    {        
	        if( (lsesps16ADMwstr > 0) )
	        {        
	            if(ldespu8AdmVcrtDataLHCNT < L_U8_TIME_10MSLOOP_1000MS )
	            {
	                ldespu8AdmVcrtDataLHCNT         =  ldespu8AdmVcrtDataLHCNT + 1;
	                ldesps32AdmVcrtVeloCalcLHSum     =  ldesps32AdmVcrtVeloCalcLHSum    + (((int32_t)ldesps16AdmVestUsingYawSteer*ldesps16AdmVestUsingYawSteer)/100) ;
	                ldesps32AdmVcrtDiffdotVeloLHSum =  ldesps32AdmVcrtDiffdotVeloLHSum  + (((int32_t)ldesps16AdmVcrtVeloDiff*ldesps16AdmVestUsingYawSteer)*10);
	            }
	            else
	            {
	                ldesps16AdmVcrtRatioLHRaw  =  (int16_t)(ldesps32AdmVcrtDiffdotVeloLHSum/ldesps32AdmVcrtVeloCalcLHSum );
	              
	                ADM = (struct  ADAPTIVE_M * ) &ADMvelo_LH;  
	                LDESP_vCalcADMaverageData(ldesps16AdmVcrtRatioLHRaw , S16_ADM_VELO_THR_OK ,U8_ADM_VELO_1ST_UPD_TIME , U8_ADM_VELO_2ND_UPD_TIME , U8_ADM_VELO_3RD_UPD_TIME);
	           
	                ldesps32AdmVcrtVeloCalcLHSum = 0;
	                ldesps32AdmVcrtDiffdotVeloLHSum =0;
	                ldespu8AdmVcrtDataLHCNT =0;                                    
	            }
	        }
	        else if( (lsesps16ADMwstr < 0)  )
	        {
	            if(ldespu8AdmVcrtDataRHCNT < L_U8_TIME_10MSLOOP_1000MS )  
	            {
	                ldespu8AdmVcrtDataRHCNT         =  ldespu8AdmVcrtDataRHCNT + 1;
	                ldesps32AdmVcrtVeloCalcRHSum    =  ldesps32AdmVcrtVeloCalcRHSum     + (((int32_t)ldesps16AdmVestUsingYawSteer*ldesps16AdmVestUsingYawSteer)/100) ;
	                ldesps32AdmVcrtDiffdotVeloRHSum =  ldesps32AdmVcrtDiffdotVeloRHSum  + (((int32_t)ldesps16AdmVcrtVeloDiff*ldesps16AdmVestUsingYawSteer)*10);
	            }
	            else
	            {
	                ldesps16AdmVcrtRatioRHRaw  =  (int16_t)(ldesps32AdmVcrtDiffdotVeloRHSum/ldesps32AdmVcrtVeloCalcRHSum );
	                
	                ADM = (struct  ADAPTIVE_M *) &ADMvelo_RH;  
	                LDESP_vCalcADMaverageData(ldesps16AdmVcrtRatioRHRaw , S16_ADM_VELO_THR_OK ,U8_ADM_VELO_1ST_UPD_TIME , U8_ADM_VELO_2ND_UPD_TIME , U8_ADM_VELO_3RD_UPD_TIME  );
	
	                              
	                ldesps32AdmVcrtVeloCalcRHSum = 0;
	                ldesps32AdmVcrtDiffdotVeloRHSum =0;
	                ldespu8AdmVcrtDataRHCNT =0;                                    
	            }
	        }
	        else
	        {
	            ;
	        }
	                
	        if( ADMvelo_LH.ldespu1ADM_10SecCheckOK == 1)
	        {
	            ldesps16AdmVcrtRatioLH = ADMvelo_LH.ldesps16ADM_100SecCalcMean ;    
	        }
	        else if(ADMvelo_LH.ldespu1ADM_1SecFirstOK==1 )
	        {
	            ldesps16AdmVcrtRatioLH = ADMvelo_LH.ldesps16ADM_1SecCalcMean ;                
	        }
	        else
	        {
	            ldesps16AdmVcrtRatioLH = 0 ;
	        }
	        
	        if( ADMvelo_RH.ldespu1ADM_10SecCheckOK == 1)
	        {
	            ldesps16AdmVcrtRatioRH = ADMvelo_RH.ldesps16ADM_100SecCalcMean ;    
	        }
	        else if(ADMvelo_RH.ldespu1ADM_1SecFirstOK==1 )
	        {
	            ldesps16AdmVcrtRatioRH = ADMvelo_RH.ldesps16ADM_1SecCalcMean ;                
	        }
	        else
	        {
	            ldesps16AdmVcrtRatioRH = 0 ;
	        }           
	        
	        ldesps16AdmVeloRatioCalc   = (ldesps16AdmVcrtRatioLH + ldesps16AdmVcrtRatioRH )/2 ;
	        ldesps16AdmVcrtRatioApply  = (1000 - ldesps16AdmVeloRatioCalc );              
	    }
	    else
	    {
	        ;
	    }
	}            
	            
	
	void    LDESP_vDetModelVeloUpdate(void)
	{
	    int16_t s16ADMVelo10secDiffLH;
	    int16_t s16ADMVelo10secDiffRH;
	    int16_t s16ADMVeloTargetCurrentDiff;
	    uint8_t u8VeloApply = 1;
	    
	    if(ldespu1AdmVeloCrtApply == 1)
	    {
	        ldespu1AdmVeloCrtApply = 0;
	    }
	    else
	    {                 
	        if(ldespu1AdmVeloCrtDuring ==1 )
	        {
	            if( (ADMvelo_LH.ldespu1ADM_Update3rdOK==1) &&  (ADMvelo_RH.ldespu1ADM_Update3rdOK==1) )
	            {
	                if(ldespu1AdmVelo3rdUpdateOK==0)
	                {
	                    ldespu1AdmVeloCrtOK       = 1;
	                    ldespu1AdmVeloCrtApply    = 1;
	                    ldespu1AdmVelo3rdUpdateOK = 1;
	                }
	                else
	                {
	                    if( (ADMvelo_LH.ldespu1ADM_10SecUpdateFlg==1) && (ADMvelo_RH.ldespu1ADM_10SecUpdateFlg==1) )
	                    {
	                        ldespu1AdmVeloCrtOK    = 1;
	                        ldespu1AdmVeloCrtApply = 1;
	                        ADMvelo_LH.ldespu1ADM_10SecUpdateFlg = 0;
	                        ADMvelo_RH.ldespu1ADM_10SecUpdateFlg = 0;
	                    }
	                    else
	                    {
	                        ;
	                    }                                        
	                }                
	            }
	            else if( (ldespu1AdmVelo2ndUpdateOK==0) && (ADMvelo_LH.ldespu1ADM_Update2ndOK==1) &&  (ADMvelo_RH.ldespu1ADM_Update2ndOK==1) )
	            {
	                ldespu1AdmVeloCrtOK    = 1;
	                ldespu1AdmVeloCrtApply = 1;
	                ldespu1AdmVelo2ndUpdateOK = 1;
	            }
	            else if( (ldespu1AdmVelo1stUpdateOK==0) && (ADMvelo_LH.ldespu1ADM_Update1stOK==1) &&  (ADMvelo_RH.ldespu1ADM_Update1stOK==1) )
	            {
	                ldespu1AdmVeloCrtOK    = 1;   
	                ldespu1AdmVeloCrtApply = 1;
	                ldespu1AdmVelo1stUpdateOK = 1;                
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	        	s16ADMVeloTargetCurrentDiff = McrAbs(ldesps16AdmVcrtRatioApply-ldesps16AdmVcrtRatioApplyF);
	            if( (ADMvelo_LH.ldespu1ADM_Update1stOK==1) && (ADMvelo_RH.ldespu1ADM_Update1stOK==1) &&
	                ( s16ADMVeloTargetCurrentDiff > S16_ADM_VELO_CRT_THR )  )
	            {
	                ldespu1AdmVeloCrtDuring = 1;
	        	}
	        	else
	        	{
	            	;
	        	}
	        }        
	    }       
	        
	    /*-----  2. Velocity Ratio Apply  condition ----*/
		if(ldespu1AdmVeloCrtOK == 1)
		{
		    if(ldespu1AdmVeloCrtApply==1)
		    {
	            ldesps16AdmVcrtRatioApplyF = LCABS_s16LimitMinMax(ldesps16AdmVcrtRatioApply  , (ldesps16AdmVcrtRatioApplyF- S16_ADM_VELO_CRT_THR)    , ( ldesps16AdmVcrtRatioApplyF + S16_ADM_VELO_CRT_THR) );  /* 20 -> 2% Tuning p */	        	        	                        	            
		    }
		    else
		    {
		        ; 
		    }
		}
		else
		{
		    if(ldespu1AdmVeloeepReadOK == 1 )
		    {
		        ldesps16AdmVcrtRatioApplyF = ldesps16AdmVcrtEEPread ; 
			}
	    	else
	    	{
		        ldesps16AdmVcrtRatioApplyF = 1000 ;     /* 1000 -> 100% */
		    }	        
		}
		
		ldesps16AdmVcrtRatioApplyF = LCABS_s16LimitMinMax(ldesps16AdmVcrtRatioApplyF , (1000 - S16_ADM_VELO_CRT_THR_F) , ( 1000 + S16_ADM_VELO_CRT_THR_F )  );                                          /* Max 6% Tuning p */  	        	        
		
		if(u8VeloApply == U8_ADM_VELO_APPLY )
		{
	        ;
	    }
		else
		{
		    ldesps16AdmVcrtRatioApplyF = 1000 ;
		}
	}
	
	void    LDESP_vDetADMveloReset(void)
	{
	    int8_t  i;
	      		        	    	     
	    ldesps16AdmVcrtVeloDiff        =0; 
	    
	    ldesps32AdmVcrtVeloCalcLHSum     =0; 
	    ldesps32AdmVcrtDiffdotVeloLHSum =0;    
	    ldespu8AdmVcrtDataLHCNT         =0;
	    ldesps16AdmVcrtRatioLHRaw       =0;   
	    ldespu1AdmVeloR1SecCalLHFlg     =0;
	    
	    ldesps32AdmVcrtVeloCalcRHSum     =0;
	    ldesps32AdmVcrtDiffdotVeloRHSum =0;    
	    ldespu8AdmVcrtDataRHCNT         =0;
	    ldesps16AdmVcrtRatioRHRaw       =0;   
	    ldespu1AdmVeloR1SecCalRHFlg     =0;    
	
	        
		for (i=0;i<=9;i=i+1)
		{ 
	        ADMvelo_LH.ldesps16ADM_1SecCalc[i] =0;
	        ADMvelo_LH.ldesps16ADM_10SecCalc[i]=0;	
	        
	        ADMvelo_RH.ldesps16ADM_1SecCalc[i] =0;
	        ADMvelo_RH.ldesps16ADM_10SecCalc[i]=0;	        
		}       
		           
	    ADMvelo_LH.ldesps16ADM_1SecCalcMAX       =0;          
	    ADMvelo_LH.ldesps16ADM_1SecCalcMIN       =0;          
	    ADMvelo_LH.ldesps16ADM_10SecCalcMean     =0;        
	    ADMvelo_LH.ldesps16ADM_100SecCalcMean    =0;       
	    ADMvelo_LH.ldesps16ADM_100SecCalcMeanOld =0;    
	    ADMvelo_LH.ldespu8ADM_1SecCalcCnt        =0;           
	    ADMvelo_LH.ldespu8ADM_10SecCalcCnt       =0;                          
	    ADMvelo_LH.ldespu1ADM_10SecCalcOK        =0;	  
	    
	    ADMvelo_LH.ldespu1ADM_1SecFirstOK        =0;      
	    ADMvelo_LH.ldespu1ADM_100SecCalcOK       =0;      
	              
	    ADMvelo_LH.ldespu1ADM_10SecFirstOK       =0;      
	    ADMvelo_LH.ldespu1ADM_100SecFirstOK      =0;       
	    
	    ADMvelo_RH.ldesps16ADM_1SecCalcMAX       =0;          
	    ADMvelo_RH.ldesps16ADM_1SecCalcMIN       =0;          
	    ADMvelo_RH.ldesps16ADM_10SecCalcMean     =0;        
	    ADMvelo_RH.ldesps16ADM_100SecCalcMean    =0;       
	    ADMvelo_RH.ldesps16ADM_100SecCalcMeanOld =0;    
	    ADMvelo_RH.ldespu8ADM_1SecCalcCnt        =0;           
	    ADMvelo_RH.ldespu8ADM_10SecCalcCnt       =0;                          
	    ADMvelo_RH.ldespu1ADM_10SecCalcOK        =0;	  
	
	    ADMvelo_RH.ldespu1ADM_1SecFirstOK        =0;      
	    ADMvelo_RH.ldespu1ADM_100SecCalcOK       =0;      
	
	    ADMvelo_RH.ldespu1ADM_10SecFirstOK       =0;      
	    ADMvelo_RH.ldespu1ADM_100SecFirstOK      =0;          
	}
	
	void    LDESP_vDetModelVeloeepReq(void)
	{
	    int16_t  s16AdmVeloDiffforEEPwrite;
	            
	    if( ldespu1AdmVeloCrtOK == 1)
	    {
	        if(ldespu1AdmVeloeepReadOK == 1)
	        {
	            s16AdmVeloDiffforEEPwrite = McrAbs( ldesps16AdmVcrtRatioApplyF - ldesps16AdmVcrtEEPread  );
	        }
	        else
	        {
	            s16AdmVeloDiffforEEPwrite = McrAbs( ldesps16AdmVcrtRatioApplyF -  1000 );
	        }
	
	        if(s16AdmVeloDiffforEEPwrite >= S16_ADM_VELO_EEP_THR )
	        {
	            ldespu1AdmVeloeepWriteReq = 1 ;
	        }
	        else
	        {
	            ldespu1AdmVeloeepWriteReq = 0;
	        }
	    }
	    else
	    {
	        ;
	    }
	}
#endif

#if ( ( __ADAPTIVE_MODEL_VCH_GEN == ENABLE ) || (__ADAPTIVE_MODEL_VELO == ENABLE)  )   
	void     LDESP_vCalcADMaverageData( int16_t  S16ADM_data , int16_t  DATA_1SEC_THR ,uint8_t U8_ADM_1ST_UPD_TIME  , uint8_t U8_ADM_2ND_UPD_TIME , uint8_t U8_ADM_3RD_UPD_TIME )
	{
	    int16_t    S16ADM_1secMean;
	
		/*------------   1sec  Mean --------------------*/		
		S16ADM_1secMean= S16ADM_data;
		
		/*------------   10sec Mean --------------------*/		
		if(ADM->ldespu8ADM_1SecCalcCnt < 9 )
		{	
			/*---------- First 1sec calculation --------------*/
			ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] = S16ADM_1secMean;
			
			if(ADM->ldespu1ADM_1SecFirstOK==0)
			{
				ADM->ldespu1ADM_1SecFirstOK = 1;
			}
			else
			{
				;
			}
			
			if(ADM->ldespu8ADM_1SecCalcCnt == 0)
			{
				ADM->ldesps16ADM_1SecCalcMAX= ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];
				ADM->ldesps16ADM_1SecCalcMIN= ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];		    		    
				ADM->ldesps16ADM_1SecCalcMean= ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];	
				ADM->ldesps16ADM_1secSum     = ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];    		    
			}
			else
			{
	            if( ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] > ADM->ldesps16ADM_1SecCalcMAX )
	            {
	            	ADM->ldesps16ADM_1SecCalcMAX=ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];
	            }
	            else if( ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] < ADM->ldesps16ADM_1SecCalcMIN )
	            {
					ADM->ldesps16ADM_1SecCalcMIN=ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];
	            }
	            else
	            {
	            	;
	            }
	            
	            /*
		        for(j=0;j<=(ADM->ldespu8ADM_1SecCalcCnt);j=j+1)
		        {
		          ldesps32ADM_1secSum=ldesps32ADM_1secSum+ADM->ldesps16ADM_1SecCalc[j];
		        }
		        */
		        
		        ADM->ldesps16ADM_1secSum = ADM->ldesps16ADM_1secSum + ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] ;
		        
		        ADM->ldesps16ADM_1SecCalcMean = (int16_t)(ADM->ldesps16ADM_1secSum/(ADM->ldespu8ADM_1SecCalcCnt+1));		
	
			}
			
		    ADM->ldespu8ADM_1SecCalcCnt=ADM->ldespu8ADM_1SecCalcCnt+1;		    	   	    
			
		    if( (ADM->ldespu1ADM_10SecCheckOK==1) || (ADM->ldespu8ADM_1SecCalcCnt >= U8_ADM_3RD_UPD_TIME) )
		    {
		        ADM->ldespu1ADM_Update3rdOK = 1 ;
		    }
		    else if((ADM->ldespu1ADM_10SecCheckOK==1) || (ADM->ldespu8ADM_1SecCalcCnt >= U8_ADM_2ND_UPD_TIME) )
		    {
		        ADM->ldespu1ADM_Update2ndOK = 1 ;
		    }
		    else if((ADM->ldespu1ADM_10SecCheckOK==1) || (ADM->ldespu8ADM_1SecCalcCnt >= U8_ADM_1ST_UPD_TIME) )
		    {
		        ADM->ldespu1ADM_Update1stOK = 1 ;
		    }
		    else
		    {
		        ;
		    }
		}
		else
		{
	        /* ADM->ldespu8ADM_1SecCalcCnt =9 인 경우 */
	        ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] = S16ADM_1secMean;
		    
	        if( ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] > ADM->ldesps16ADM_1SecCalcMAX )
	        {
	        	ADM->ldesps16ADM_1SecCalcMAX=ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];
	        }
	        else if( ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] < ADM->ldesps16ADM_1SecCalcMIN )
	        {
	            ADM->ldesps16ADM_1SecCalcMIN=ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt];
	        }
	        else
	        {
	        	;
	        }
	        
	        ADM->ldesps16ADM_1secSum       = ADM->ldesps16ADM_1secSum + ADM->ldesps16ADM_1SecCalc[ADM->ldespu8ADM_1SecCalcCnt] ;
		    ADM->ldesps16ADM_10SecCalcMean = (int16_t)(ADM->ldesps16ADM_1secSum/10);
	        
	        /*--- 10sec Average -------------*/	    
			if( ( ADM->ldesps16ADM_1SecCalcMAX -  ADM->ldesps16ADM_1SecCalcMIN ) <= DATA_1SEC_THR  )
			{
			    ADM->ldespu1ADM_10SecCalcOK	  = 1 ;
			    ADM->ldespu1ADM_10SecCheckOK  = 1;
			    ADM->ldespu1ADM_10SecUpdateFlg= 1;
			    
			    /*
			    for(j=0;j<=9;j=j+1)
			    {
			      S32ADM_10secSum= S32ADM_10secSum + ADM->ldesps16ADM_1SecCalc[j];		 
			    }
			    */
			}
			else
			{
				ADM->ldespu1ADM_10SecCalcOK	= 0 ;
			}
			
	       /*------------   10sec*10 (100sec) Moving Average -----------------*/			
	        if(ADM->ldespu1ADM_10SecCalcOK ==1)
	        {
	            if(ADM->ldespu1ADM_100SecCalcOK==1)
		        {
	                if(ADM->ldespu8ADM_10SecCalcCnt>9)
	                {
	          	        ADM->ldespu8ADM_10SecCalcCnt=0;
	                }
	                else
	                {
	          	        ;
	                }                
	            
	                ADM->ldesps16ADM_100secSum = (ADM->ldesps16ADM_100secSum - ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt]) + ADM->ldesps16ADM_10SecCalcMean  ; 
	                ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt] = ADM->ldesps16ADM_10SecCalcMean ;
	                ADM->ldespu8ADM_10SecCalcCnt=ADM->ldespu8ADM_10SecCalcCnt+1;
	                
	        	    /*
	        	    for(j=0;j<=9;j=j+1)
	        	    {
	        		    S32ADM_100secSum=S32ADM_100secSum+ADM->ldesps16ADM_10SecCalc[j];
	        	    }
	        	    */        	      	            	    
	        	    
	        	    ADM->ldesps16ADM_100SecCalcMeanOld = ADM->ldesps16ADM_100SecCalcMean;
	        	    ADM->ldesps16ADM_100SecCalcMean    = (int16_t)(ADM->ldesps16ADM_100secSum/10);      
	        	        	    	    	      	        
	            }
	            else
	            {         
		            /*------ First 10sec  calculation ---------*/
		            if(ADM->ldespu1ADM_10SecFirstOK==0)
		            {
		        		ADM->ldespu1ADM_10SecFirstOK=1;
		            }
		            else
		            {
						;
		            }

		        	if(ADM->ldespu8ADM_10SecCalcCnt< 9)	
		        	{
		                ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt] = ADM->ldesps16ADM_10SecCalcMean ;
		                
		                if( ADM->ldespu8ADM_10SecCalcCnt == 0 )
		                {
		                    ADM->ldesps16ADM_10SecCalcMAX   = ADM->ldesps16ADM_10SecCalcMean ;
		                    ADM->ldesps16ADM_10SecCalcMIN   = ADM->ldesps16ADM_10SecCalcMean ;
		                    ADM->ldesps16ADM_100SecCalcMean = ADM->ldesps16ADM_10SecCalcMean ; 
		                    ADM->ldesps16ADM_100secSum      = ADM->ldesps16ADM_10SecCalcMean ;
		                }
		                else
		                {
		                    if(ADM->ldesps16ADM_10SecCalcMean > ADM->ldesps16ADM_10SecCalcMAX )
		                    {
		                        ADM->ldesps16ADM_10SecCalcMAX = ADM->ldesps16ADM_10SecCalcMean ;
		                    }
		                    else if(ADM->ldesps16ADM_10SecCalcMean < ADM->ldesps16ADM_10SecCalcMIN )
		                    {
		                        ADM->ldesps16ADM_10SecCalcMIN = ADM->ldesps16ADM_10SecCalcMean ;
		                    }
		                    else
		                    {
		                        ;
		                    }	 
		        	  	    
		                    /*
		        	  	for(j=0;j<=(ADM->ldespu8ADM_10SecCalcCnt);j=j+1)
		        	  	{
		        	  	  S32ADM_100secSum=S32ADM_100secSum+ADM->ldesps16ADM_10SecCalc[j];
		        	  	}
		        	  	    */
	
		        	  	    ADM->ldesps16ADM_100secSum      = ADM->ldesps16ADM_100secSum  + ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt];	        	  	    
		        	  	    ADM->ldesps16ADM_100SecCalcMean = (int16_t)(ADM->ldesps16ADM_100secSum /(ADM->ldespu8ADM_10SecCalcCnt+1));	                                       
		                }	                	        	  	    			  			  	  	  	
		                ADM->ldespu8ADM_10SecCalcCnt = ADM->ldespu8ADM_10SecCalcCnt+1;		    	    
		            }
		            else
		            {
		                ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt] = ADM->ldesps16ADM_10SecCalcMean ;
		                
		        	    /*------ First 100 sec  calculation ---------*/
		        	    if(ADM->ldespu1ADM_100SecFirstOK==0)
		        	  	{
		        	  		  ADM->ldespu1ADM_100SecFirstOK=1;
		        	  	}
		        	  	else
		        	  	{
		        	  		;
		        	  	}
		        	  	 
		        	  	ADM->ldespu1ADM_100SecCalcOK=1;	    	  	
		        	  	
		        	  	/*
		        	  	for(j=0;j<=9;j=j+1)
		        	  	{
		        	  		  S32ADM_100secSum= S32ADM_100secSum + ADM->ldesps16ADM_10SecCalc[j];
		        	  	}	        	  		        	  	   	  	
		        	  	*/      	  		        	  	   	  	
		        	  		    	  	
		        	  	ADM->ldesps16ADM_100secSum         = ADM->ldesps16ADM_100secSum + ADM->ldesps16ADM_10SecCalc[ADM->ldespu8ADM_10SecCalcCnt] ;	    	  	
		        	  	ADM->ldesps16ADM_100SecCalcMean    = (int16_t)(ADM->ldesps16ADM_100secSum/10);
		                ADM->ldespu8ADM_10SecCalcCnt = 0;				            	        		
		            }	        
		        }  	
	    	}
	        else 
	        {
	        	;
	        }  	  		  		

			ADM->ldespu8ADM_1SecCalcCnt = 0;
	        ADM->ldesps16ADM_1secSum    = 0;
		}					
	}
#endif

#if (__GM_CPG_BANK==1)
	void LDESP_vDetGMBank(void)
	{
	    if(
	    	#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
		    	( (((fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0))&&(lsespu1PedalBrakeOn==0))||
		    	  ((lsespu1MpresByAHBgen3Invalid==0)&&(lsespu1MpresBrkOnByAHBgen3==0)) )&&
	    	#else
	    		(MPRESS_BRAKE_ON == 0)			&&
	    	#endif
	    	(ABS_fz == 0)
	    )
	    {
	    	/********   FL   wheel    ************/
			if(SLIP_CONTROL_NEED_FL == 1)  /* SLIP_NEED and Pressure Rise Mode  */
			{
				if(ldescS16GMBankPrsRiseCount < 0)
				{
					ldescS16GMBankPrsRiseCount = 0;
					ldescS16GMBankPrsRiseCountOld = 0;
					ldescS16GMBankRiseNum = 0;
				}
				else
				{
					;
				}
				ldescS16GMBankNextRiseCount = 0;		/* Time Counter between one SLIP_NEED and Next  */
				
				if((HV_VL_fl == 0) && (AV_VL_fl == 0))
				{
					ldescS16GMBankPrsRiseCount++;				/* Time Counter between Pressure Rise Mode */
				}
				else			/* No Rise time count for Hold mode or Dump */
				{
					;
				}
				if(ldescS16GMBankPrsRiseCount > 250)
				{	
					ldescS16GMBankPrsRiseCount = 250;
				}
				else
				{
					;
				}
			}
			else if((SLIP_CONTROL_NEED_FL == 0) && (ldescS16GMBankPrsRiseCount > 0) )
			{
				if((alat < LAT_0G3G) && (wstr < WSTR_40_DEG))			/* bank condition */
				{
					if(ldescS16GMBankNextRiseCount == 0)		/* count the num of rise mode for 20scan */
					{
						ldescS16GMBankRiseInterval = ldescS16GMBankPrsRiseCount - ldescS16GMBankPrsRiseCountOld;
						if(ldescS16GMBankRiseInterval >  U8_CPG_BANK_RISE_CNT_THR)
						{
							ldescS16GMBankRiseNum = ldescS16GMBankRiseNum + 1;
						}
						else
						{
							;
						}
						ldescS16GMBankPrsRiseCountOld = ldescS16GMBankPrsRiseCount;
					}
					else
					{
						;
					}
	
					ldescS16GMBankNextRiseCount++;
					if(ldescS16GMBankNextRiseCount >= S16_CPG_BANK_NEXT_RISE_INTV)		/* If no rise for XX scan, count reset */
					{
						ldescS16GMBankPrsRiseCount = 0;
						ldescS16GMBankNextRiseCount = 0;
						ldescS16GMBankPrsRiseCountOld = 0;
						ldescS16GMBankRiseNum = 0;
					}
					else
					{
						;
					}
				}
				else
				{
					ldescS16GMBankPrsRiseCount = 0;
					ldescS16GMBankNextRiseCount = 0;
					ldescS16GMBankRiseNum = 0;
					ldescS16GMBankPrsRiseCountOld = 0;
				}
			}
			else
			{
				;
			}
			
			/********   FR   wheel    ************/
			if((SLIP_CONTROL_NEED_FR == 1))
			{
				if(ldescS16GMBankPrsRiseCount > 0)
				{
					ldescS16GMBankPrsRiseCount = 0;
					ldescS16GMBankPrsRiseCountOld = 0;
					ldescS16GMBankRiseNum = 0;
				}
				else
				{
					;
				}
				ldescS16GMBankNextRiseCount = 0;
				
				if((HV_VL_fr == 0) && (AV_VL_fr == 0))
				{
					ldescS16GMBankPrsRiseCount--;			/* FR '-' count */
				}
				else
				{
					;
				}
				if(ldescS16GMBankPrsRiseCount < -250)
				{	
					ldescS16GMBankPrsRiseCount = -250;
				}
				else
				{
					;
				}
			}
			else if((SLIP_CONTROL_NEED_FR == 0) && (ldescS16GMBankPrsRiseCount < 0))
			{
				if((alat > -LAT_0G3G) && (wstr > -WSTR_40_DEG))
				{
					if(ldescS16GMBankNextRiseCount == 0)		/* count the num of rise mode for 20scan */
					{
						ldescS16GMBankRiseInterval = ldescS16GMBankPrsRiseCountOld - ldescS16GMBankPrsRiseCount;
						if(ldescS16GMBankRiseInterval > U8_CPG_BANK_RISE_CNT_THR)
						{
							ldescS16GMBankRiseNum = ldescS16GMBankRiseNum + 1;
						}
						else
						{
							;
						}
						ldescS16GMBankPrsRiseCountOld = ldescS16GMBankPrsRiseCount;
					}
					else
					{
						;
					}
	
					ldescS16GMBankNextRiseCount++;
					if(ldescS16GMBankNextRiseCount >= S16_CPG_BANK_NEXT_RISE_INTV)
					{
						ldescS16GMBankPrsRiseCount = 0;
						ldescS16GMBankNextRiseCount = 0;
						ldescS16GMBankPrsRiseCountOld = 0;
						ldescS16GMBankRiseNum = 0;
					}
					else
					{
						;
					}
				}
				else
				{
					ldescS16GMBankPrsRiseCount = 0;
					ldescS16GMBankNextRiseCount = 0;
					ldescS16GMBankPrsRiseCountOld = 0;
					ldescS16GMBankRiseNum = 0;
				}
			}
			else
			{
				;
			}
		}
		else
		{
			ldescS16GMBankPrsRiseCount = 0;
			ldescS16GMBankNextRiseCount = 0;
			ldescS16GMBankPrsRiseCountOld = 0;
			ldescS16GMBankRiseNum = 0;
		}
		
		if((Bank_GM_Susp == 0) && (ldescu1DetDynBank == 1) && (det_alatm_fltr >= LAT_0G6G) 
				   && (McrAbs(ldescS16GMBankPrsRiseCount) >= S16_CPG_BANK_CNTR_ENT_RISE_CNT) && (ldescS16GMBankRiseNum >= 2))
		{
			Bank_GM_Susp = 1;
		}
		else if((Bank_GM_Susp == 1) && ((ldescu1DetDynBank == 0) || (det_alatm_fltr < LAT_0G5G) || (ldescS16GMBankPrsRiseCount == 0)))
		{
	    	Bank_GM_Susp = 0;
		}
		else
		{
			;
		}
	}
#endif


#endif /*__VDC*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCalVehicleModel
	#include "Mdyn_autosar.h"
#endif
