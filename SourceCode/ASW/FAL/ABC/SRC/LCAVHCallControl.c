/******************************************************************************
* Project Name: Auto Vehie Hold
* File: LCAVHCallControl.
* Description: Auto Vehic Hold Controller
* Date: January. 23. 2009
******************************************************************************/

/* Includes ******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCAVHCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCAVHCallControl.h"
#include "LAAVHCallActHW.h"
#include "LDABSDctWheelStatus.h"
#include "LSABSCallSensorSignal.h"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LCESPCalLpf.h"
#include "LCESPCalInterpolation.h"
#include "Hardware_Control.h"
#include "LCEPBCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCalInterpolation.h"
#include "LSESPCalSensorOffset.h"
#include "LCHSACallControl.h"
#include "LSESPFilterEspSensor.h"
  #if (__SCC==1)
#include "SCC/LCSCCCallControl.h"
  #endif

#include "hm_logic_var.h"


/***************************** Logcal Definiton ******************************/

/* Variables Definition*******************************************************/
  #if __EPB_INTERFACE || __AVH
struct	U8_BIT_STRUCT_t AVHF0;
struct	U8_BIT_STRUCT_t AVHF1;
struct	U8_BIT_STRUCT_t AVHF2;
struct	U8_BIT_STRUCT_t AVHF3;
struct	U8_BIT_STRUCT_t AVHF4;
struct	U8_BIT_STRUCT_t AVHF5;
struct	U8_BIT_STRUCT_t AVHF6;
struct	U8_BIT_STRUCT_t AVHF7;

int8_t    lcs8AvhControlState;

uint8_t	lcu8AvhGearPosition, lcu8AvhGearPositionOld, lcu8AvhState, lcu8AvhState2;
uint8_t	lcu8AvhAceelIntendCnt, lcu8AvhGearChageCnt, lcu8AvhReqEpbAct, lcu8AvhEpbState, lcu8AvhEpbStateOld;
uint8_t	lcu8AvhAxStationaryCnt, lcu8AvhTotalRiseAtempt, lcu8AvhChkUphillCnt, lcu8AvhChkDownhillCnt;
uint8_t	lcu8AvhDrvDoorOpen, lcu8AvhDrvSeatUnBelted, lcu8AvhHoodOpen, lcu8AvhTrunkOpen;
uint8_t	lcu8AvhLampRequest, lcu8AvhAlarmRequest, lcu8AvhRiseEndCnt, lcu8AvhReBrkCnt;
uint8_t   lcu8AvhDriverInsideCnt, lcu8AvhMaxHillCnt, lcu8AvhMpOkCnt, lcu8AvhDownDrvLevel;
uint8_t   lcu8AvhCluMsg, lcu8AvhPrateLevel, lcu8AvhTcCurAvgPeriodCnt, AVH_debuger;
uint8_t   lcu8AvhTcTemperDeCount, lcu8AvhRiseInitOffCnt, lcu8AvhAutoAct1SecCnt, lcu8AvhInitCurCnt;
uint8_t   lcu8AvhLampOffdelay, lcu8AvhEntSpdOk, lcu8AvhParkGearCnt, lcu8AvhReBrkReleaseCnt;
uint8_t   lcu8AvhEngIsgControlAct, lcu8AvhEngIsgControlActOld, lcu8AvhIsgActOldCnt;
uint8_t   lcu8AvhAlarmCase, lcu8AvhInitIgnOnCnt, lcu8AvhAhbMpressOnCnt;

int16_t		lcs16AvhRetainingPress, lcs16AvhEnterPress, lcs16AvhDefficientPress, lcs16AvhMpressSlopThres, lcs16AvhMpress;
int16_t     lcs16AvhSafeHoldPress, lcs16AvhApplyCnt, lcs16AvhApplyTime;
int16_t		lcs16AvhAxG, lcs16AvhAxGAbs, lcs16AvhFilteredAxOld, lcs16AvhFilteredAx;
int16_t		lcs16AvhTargetPress, lcs16AvhFilteredTorq, lcs16AvhActDeCountCycle;
int16_t		lcs16AvhReleaseTime, lcs16AvhMaxHillOnCnt, lcs16AvhValveActMp;
int16_t		lcs16AvhEngIdleCnt, lcs16AvhIdleTorq, lcs16AvhEpbReqCnt;
int16_t     lcs16AvhMoveDistance_fl, lcs16AvhCompleteStopCnt_fl;
int16_t     lcs16AvhMoveDistance_fr, lcs16AvhCompleteStopCnt_fr;
int16_t     lcs16AvhMoveDistance_rl, lcs16AvhCompleteStopCnt_rl;
int16_t     lcs16AvhMoveDistance_rr, lcs16AvhCompleteStopCnt_rr;
int16_t     lcs16AvhAbnormalHoldCnt, lcs16AvhMinEntPres, lcs16AvhAlarmOnTime;
int16_t		lcs16AvhHoldCnt, lcs16AvhHold1SecCnt, lcs16AvhHoldCntFor1IGN, lcs16AvhTpApplyInit;
int16_t		laavhs16TcTargetPress, lcs16AvhTpApply;

uint16_t    lcu16AvhRollStateCnt, lcu16AvhTcCurAvg, lcu16AvhAutoActTotal;
uint16_t    lcu16AvhTcCurSum,lcu16AvhTcCurTemper;

int32_t    lcs32AvhTorqSum;

static uint8_t  lcu8AvhTemp0;
static int16_t    lcs16AvhTemp0;


  #endif
/*Global Variable Declaration*************************************************/

/* LocalFunction prototype ***************************************************/
  #if __AVH
static void	LCAVH_vVariableInitialize(void);
static void	LCAVH_vCheckInhibition(void);
static void	LCAVH_vCheckAvhSwitch(void);
static void	LCAVH_vCheckAvhAutoActRequest(void);
static void	LCAVH_vCheckAvhVehicleTM(void);
static void	LCAVH_vCheckAvhInsideVehicle(void);
static void	LCAVH_vGetAxSensor(void);
static void	LCAVH_vCheckHillGrade(void);
static void	LCAVH_vCheckAtGearPosition(void);
static void	LCAVH_vCheckMtGearPosition(void);
static void	LCAVH_vCheckEpbState(void);
static void	LCAVH_vCheckEnterCondition(void);
static void	LCAVH_vCheckExitCondition(void);
static void	LCAVH_vDecideAvhEnterPress(void);
static void	LCAVH_vCheckAvhEntCommonnCnd(void);
static void	LCAVH_vCheckAvhToEpbTransition(void);
static void	LCAVH_vCheckApplyCondition(void);
static void	LCAVH_vCheckAccelIntend(void);
static void	LCAVH_vDetectVehicleRoll(void);
static void	LCAVH_vReleaseMode(void);
static void	LCAVH_vHoldMode(void);
static void	LCAVH_vApplyMode(void);
static void	LCAVH_vAvhOnCount(void);
static void	LCAVH_vResetCtrlVariable(void);
static void	LCAVH_vCalcTargetPress(void);
static void	LCAVH_vCalcIdleEngTorque(void);
static void	LCAVH_vCheckHill(void);
static void	LCAVH_vDecideAvhCanMsg(void);
static void	LCAVH_vCalcMpSlope(void);
static void	LCAVH_vChcekReBrkRelease(void);
static void	LCAVH_vEstTcCoilTemperature(void);
static void	LCAVH_vChkAvhSWActReady(void);
static void	LCAVH_vChkAvhOverActTime(void);
static void	LCAVH_vChkAvhArrangeEpbActReq(void);
static void	LCAVH_vChkAvhLimitAutoMode(void);
static void	LCAVH_vCheckAvhEntSpeed(void);
static void	LCAVH_vCheckMpressOn(void);
#if __ISG
static void	LCAVH_vCheckIsgActState(void);
#endif
#if __AVH_HOOD_TRUNK_CONCEPT==2
static void	LCAVH_vDecideAvhArlamOnTime(void);
#endif
  #endif
 
/* GlobalFunction prototype **************************************************/
void  LCAVH_vTxLdmState(void);
void LCAVH_vControlAvhAbnormalMode(void);
/* Implementation*************************************************************/

/******************************************************************************
* FUNCTION NAME:      LCAVH_vCallControl
* CALLED BY:          StartupLogic()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        AVH controller main function
******************************************************************************/
#if __AVH
void 	LCAVH_vCallControl(void)
{
	LCAVH_vVariableInitialize();
	LCAVH_vCheckInhibition();
	LCAVH_vCheckAvhAutoActRequest();
	LCAVH_vCheckAvhVehicleTM();
	LCAVH_vCheckMpressOn();
	LCAVH_vCheckAvhInsideVehicle();
	LCAVH_vChkAvhSWActReady();
	LCAVH_vCheckAvhSwitch();
	  #if __AVH_HOOD_TRUNK_CONCEPT==2
	LCAVH_vDecideAvhArlamOnTime();
	  #endif
	LCAVH_vGetAxSensor();
	LCAVH_vCheckHillGrade();
	  #if __ISG
	LCAVH_vCheckIsgActState();
	  #endif

	if(lcu1AvhAutoTM==AVH_AUTO_TM)
	{
 	    LCAVH_vCheckAtGearPosition();
 	}
 	else
 	{
        LCAVH_vCheckMtGearPosition();      
    }
	LCAVH_vCheckEpbState();
	LCAVH_vCalcIdleEngTorque();
	LCAVH_vCheckHill();
    LCAVH_vDecideAvhEnterPress();

    LCAVH_vEstTcCoilTemperature();
    
	switch (lcs8AvhControlState)
	{

		/*-------------------------------------------------------------------------- */
		case AVH_READY:
			if(lcu1AvhInhibitFlg==1)
			{
				lcs8AvhControlState = AVH_INHIBIT;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=1;
			}
			else if(lcu1AvhSwitchOn==1)
			{
				LCAVH_vCheckEnterCondition();
				
				if(lcu1AvhEnterSatisfy==1)
				{
					lcs8AvhControlState = AVH_HOLD;
					lcu1AvhActFlg = 1;
					lcu1AvhActReady = 0;
					lcs16AvhRetainingPress = lcs16AvhMpress;
					lcu1AvhComCndSatisfy = 0;
					lcu1AvhEnterSatisfy = 0;
					LCAVH_vHoldMode();
					AVH_debuger=2;
				}
				else if(lcu1AvhEnterSatisfy2==1) 
				{
					lcs8AvhControlState = AVH_HOLD;                
					lcu1AvhAutonomousActFlg = 1;  
					  #if (__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE)
					lcs16AvhRetainingPress = (PRESS_FL.s16premFiltPress+PRESS_FR.s16premFiltPress)/2;
					  #elif (__FL_PRESS==ENABLE)
                    lcs16AvhRetainingPress = PRESS_FL.s16premFiltPress;
                      #elif (__FR_PRESS==ENABLE)
                    lcs16AvhRetainingPress = PRESS_FR.s16premFiltPress;
                      #else
                        #if (__SCC==1)
                    lcs16AvhRetainingPress = lcs16SccTargetPres;
                        #else
                    lcs16AvhRetainingPress = MPRESS_10BAR;
                        #endif
                      #endif
					lcu1AvhComCndSatisfy = 0;
					lcu1AvhEnterSatisfy2 = 0;                       
					LCAVH_vHoldMode();
					AVH_debuger=3;
				}
				else if(lcu1AvhActReady==1)
				{
				    LCAVH_vCheckAvhToEpbTransition();
				    AVH_debuger=33;
				}	
				else
				{
					AVH_debuger=4; /* AVH_READY */
				}
			}
			else
			{
				if(lcu1AvhActReq==1)
				{
					LCAVH_vCheckEnterCondition();
					if(lcu1AvhEnterSatisfy2==1) 
				    {
					    lcs8AvhControlState = AVH_HOLD;                
					    lcu1AvhAutonomousActFlg = 1;   
					    lcu1AvhComCndSatisfy = 0;                
					      #if (__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE)
					    lcs16AvhRetainingPress = (PRESS_FL.s16premFiltPress+PRESS_FR.s16premFiltPress)/2;
					      #elif (__FL_PRESS==ENABLE)
                        lcs16AvhRetainingPress = PRESS_FL.s16premFiltPress;
                          #elif (__FR_PRESS==ENABLE)
                        lcs16AvhRetainingPress = PRESS_FR.s16premFiltPress;
                          #else
                            #if (__SCC==1)
                        lcs16AvhRetainingPress = lcs16SccTargetPres;
                            #else
                        lcs16AvhRetainingPress = MPRESS_10BAR;
                            #endif
                          #endif
					    lcu1AvhEnterSatisfy2 = 0;                       
					    LCAVH_vHoldMode();
					    AVH_debuger=5;
				    }
				    else
				    {
				    	AVH_debuger=6; /* AVH_READY */
				    }
				}
				else
				{
                    lcs8AvhControlState = AVH_READY;
                    LCAVH_vResetCtrlVariable();
                    AVH_debuger=7;
				}
			}
			
			  #if (__AVH_ACT_1IGN_DECOUNT==DISABLE)
			if(lcu1AvhActReady==1)
			{
				LCAVH_vAvhOnCount();
			}
			else
			{
        	    ;
	        }
	          #endif

			break;			
		/*-------------------------------------------------------------------------- */
		case AVH_HOLD:

			LCAVH_vChcekReBrkRelease();
	        LCAVH_vDetectVehicleRoll();
			LCAVH_vCheckExitCondition();

			if (lcu1AvhFastExitSatisfy==1)
			{
				lcs8AvhControlState = AVH_READY;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=8;
			}
			else if ((lcu1AvhAccelExitOK==1)||(lcu1AvhDownDrive==1)||(lcu1AvhSlowExitSatisfy==1)||(lcu1AvhMedExitSatisfy==1))
			{
				lcs8AvhControlState = AVH_RELEASE;
				LCAVH_vReleaseMode();
				AVH_debuger=9;
			}	
			else
			{
				LCAVH_vCheckApplyCondition();

				if((lcu1AvhApplySatisfy==1)&&(lcu1AvhRiseModeEnd==0))
				{
					lcs8AvhControlState = AVH_APPLY;
					lcu1AvhApplySatisfy = 0;
					LCAVH_vApplyMode();
					AVH_debuger=10;
				}
				else
				{
					LCAVH_vHoldMode();
					AVH_debuger=11;
				}
			}
			break;
		/*-------------------------------------------------------------------------- */
		case AVH_APPLY:				
            
            LCAVH_vDetectVehicleRoll();
			LCAVH_vCheckExitCondition();

			if (lcu1AvhFastExitSatisfy==1)
			{
				lcs8AvhControlState = AVH_READY;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=12;
			}
			else if ((lcu1AvhAccelExitOK==1)||(lcu1AvhDownDrive==1)||(lcu1AvhSlowExitSatisfy==1)||(lcu1AvhMedExitSatisfy==1))
			{
				lcs8AvhControlState = AVH_RELEASE;
				LCAVH_vReleaseMode();
				AVH_debuger=13;
			}
			else
			{
				if(lcu1AvhRiseModeEnd==1)
				{
					lcs8AvhControlState = AVH_HOLD;
					LCAVH_vHoldMode();
					AVH_debuger=14;
				}
				else
				{
					LCAVH_vApplyMode();
					AVH_debuger=15;
				}
			}
			
			break;
		/*-------------------------------------------------------------------------- */
		case AVH_RELEASE:

			LCAVH_vCheckExitCondition();
			lcu1AvhEpbActReq = 0;
			lcu1AvhAlarmReq = 0;
			
			
			if ((lcu1AvhFastExitSatisfy==1)||((lcu1AvhActFlg==1)&&(lcu1AvhSwitchOn==0)&&(lcu8AvhEpbState!=EPB_CLAMPED))||(lcu1AvhMpressOn==1))
			{
				lcs8AvhControlState = AVH_READY;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=16;	
			}
			else if ((lcu1AvhAccelExitOK==1)||(lcu1AvhSlowExitSatisfy==1)||(lcu1AvhDownDrive==1)||(lcu1AvhMedExitSatisfy==1))
			{
				if (lcu1AvhDownDrive==1)
				{
					tempW0 = VREF_10_KPH;
				}
				else
				{
					tempW0 = VREF_1_KPH;
				}
				
				if ((vref>tempW0)||(lcs16AvhReleaseTime>L_U16_TIME_10MSLOOP_4S)
			  #if( __AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
				||(laavhs16TcTargetPress<=MPRESS_0BAR)
			  #else
				||(MFC_Current_AVH_P<AVH_TCMF_CTRL_MIN_CURRENT)||(MFC_Current_AVH_S<AVH_TCMF_CTRL_MIN_CURRENT)
			  #endif
				)
				{
					lcs8AvhControlState = AVH_READY;
					LCAVH_vResetCtrlVariable();
					AVH_debuger=17;
				}
				else
				{
					lcs8AvhControlState = AVH_RELEASE;
					LCAVH_vReleaseMode();
					AVH_debuger=18;
				}
			}
			else
			{
				lcs8AvhControlState = AVH_READY;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=19;
			}

			break;
		/*-------------------------------------------------------------------------- */
		case AVH_INHIBIT:
			if(lcu1AvhInhibitFlg==0)
			{
				lcs8AvhControlState = AVH_READY;
				LCAVH_vResetCtrlVariable();
				AVH_debuger=23;			
			}
			else 
			{
				/*
				if(lcu1AvhEpbFailFlg==0)
				{
					if((lcu8AvhEpbState==EPB_RELEASED)&&(lcu1AvhActReq==1))
					{
						lcu1AvhEpbActReq=1;
						AVH_debuger=24;
					}
					else
					{
						lcu8AvhReqEpbAct = AVH_EPB_NO_REQUEST;
						AVH_debuger=25;
					}
				}
				else
				{
					AVH_debuger=26; 
				}
				*/
			}

        	lcs16AvhHoldCnt = 0;
	        lcs16AvhHold1SecCnt = 0;

			break;
		/*-------------------------------------------------------------------------- */
		default:
			if(lcu1AvhInhibitFlg==1)
			{
				lcs8AvhControlState = AVH_INHIBIT;
				AVH_debuger=27;
			}
			else 
			{
				lcs8AvhControlState = AVH_READY;
				AVH_debuger=29;
			}

		    LCAVH_vResetCtrlVariable();

			break;			
	}

  #if (__AVH_ACT_1IGN_DECOUNT==ENABLE)
	LCAVH_vAvhOnCount();
  #endif

    LCAVH_vChkAvhOverActTime();
    LCAVH_vChkAvhLimitAutoMode();
    LCAVH_vChkAvhArrangeEpbActReq();
    LCAVH_vDecideAvhCanMsg();
    LCAVH_vTxLdmState();	
}

static void	LCAVH_vVariableInitialize(void)
{
    if(wu1IgnRisingEdge==1)
    {
        LCAVH_vResetCtrlVariable();
     
        /* only initialize at IGN on */
        lcu1AvhSwitchOn = 0;
        lcs8AvhControlState = AVH_INHIBIT;
        lcu8AvhInitIgnOnCnt = 0;
        lcs16AvhHoldCntFor1IGN = 0;
        lcu16AvhAutoActTotal = 0;
        lcu16AvhTcCurTemper = 0;
        lcu1AvhOverActTime = 0;
        lcu1AvhTcOverTemper = 0;
        lcu1AvhAutoOverActTime = 0;
    }
    else { }
}

static void	LCAVH_vCheckInhibition(void)
{
	if((fu1OnDiag==1)||(init_end_flg==0)
	  ||((fu1AVHErrorDet==1)&&(avh_sw_error_flg==1)) /* except AVH switch fail */
  	    #if (EPB_VARIANT_ENABLE==ENABLE)
	  ||(wu8EpbVariantCodingEndFlag==0)
	    #endif
        #if (AVH_INLINE_ENABLE==ENABLE)
      || (fu2AvhInlineState!=1)
        #endif
	    #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      || (lsespu1MpresByAHBgen3Invalid==1)
        #endif
        #if (__BRK_SIG_MPS_TO_PEDAL_SEN==ENABLE)   
      || (lsespu1BrkAppSenInvalid==1)
        #endif      
	)	
	{
		lcu1AvhInhibitFlg=1;
	}	
	else
	{
        lcu1AvhInhibitFlg=0;
	}
	
	if(avh_sw_error_flg==0)
	{
		lcu1AvhSwitchFail=1;
	}
	else
	{
		lcu1AvhSwitchFail=0;
	}
}

static void	LCAVH_vChkAvhSWActReady(void)
{
  #if ( __AVH_SWITCH_OFF_CONCEPT == 1)
	if (lcu8AvhState==AVH_IN_ACTIVATION)
	{
	  #if (__BLS_NO_WLAMP_FM==ENABLE)
		if (((fu1BlsSusDet==0)&&(fu1BLSErrDet==0)&&(lcs16AvhMpress>MPRESS_7BAR))||(((fu1BlsSusDet==1)||(fu1BLSErrDet==1))&&(lcs16AvhMpress>(MPRESS_7BAR+S16_AVH_MAX_MP_OFFSET_BLS_FAIL))))
	  #else
		if (lcs16AvhMpress>MPRESS_7BAR)
	  #endif
		{
			lcu1AvhSWActReady = 1;
		}
		else
		{
			if (lcu1AvhSWActReady==1)
			{
			  #if (__BLS_NO_WLAMP_FM==ENABLE)
				if (((fu1BlsSusDet==0)&&(fu1BLSErrDet==0)&&(lcs16AvhMpress<MPRESS_5BAR))||(((fu1BlsSusDet==1)||(fu1BLSErrDet==1))&&(lcs16AvhMpress<(MPRESS_5BAR+S16_AVH_MAX_MP_OFFSET_BLS_FAIL))))
			  #else
				if (lcs16AvhMpress<MPRESS_5BAR)
			  #endif
				{
					lcu1AvhSWActReady = 0;
				}
				else
				{
					;
				}
			}
			else
			{
	            ;
	        }
	    }
	}
	else
	{
		lcu1AvhSWActReady = 1;
	}
  #elif ( __AVH_SWITCH_OFF_CONCEPT == 2)
/***** decide lcu1AvhInhibitSwOn *****/
    if(((lcu8AvhHoodOpen!=0)&&(lcu8AvhGearPosition==AVH_FORWARD))
     ||((lcu8AvhTrunkOpen!=0)&&(lcu8AvhGearPosition==AVH_REVERSE))
     ||(lcu1AvhDriverInVehicle==0)
    )
    {
    	lcu1AvhInhibitSwOn = 1;
    	if(lcu8AvhEpbState==EPB_RELEASED)
    	{
    		lcu1AvhEpbActOkForSwOff = 0;
    	}
    	else { }
    }
    else 
    {
    	lcu1AvhInhibitSwOn = 0;
		lcu1AvhEpbActOkForSwOff = 0;
	}
/*************************************/

/***** decide lcu1AvhInhibitSwOff *****/
	if(lcu8AvhState==AVH_IN_ACTIVATION)
	{
	  #if (__BLS_NO_WLAMP_FM==ENABLE)
		if (((fu1BlsSusDet==0)&&(fu1BLSErrDet==0)&&(lcs16AvhMpress>MPRESS_7BAR))||(((fu1BlsSusDet==1)||(fu1BLSErrDet==1))&&(lcs16AvhMpress>(MPRESS_7BAR+S16_AVH_MAX_MP_OFFSET_BLS_FAIL))))
	  #else
		if (lcs16AvhMpress>MPRESS_7BAR) 
	  #endif
		{
			lcu1AvhInhibitSwOff = 0;
		}
		else
		{
			if (lcu1AvhInhibitSwOff==0)
			{
			  #if (__BLS_NO_WLAMP_FM==ENABLE)
				if (((fu1BlsSusDet==0)&&(fu1BLSErrDet==0)&&(lcs16AvhMpress<MPRESS_5BAR))||(((fu1BlsSusDet==1)||(fu1BLSErrDet==1))&&(lcs16AvhMpress<(MPRESS_5BAR+S16_AVH_MAX_MP_OFFSET_BLS_FAIL))))
			  #else
				if (lcs16AvhMpress<MPRESS_5BAR) 
			  #endif
				{
					lcu1AvhInhibitSwOff = 1;
				}
				else { }
			}
			else { }
	    }
	}
	else
	{
		lcu1AvhInhibitSwOff = 0;
	}
/**************************************/  
  
/***** decide lcu1AvhForcedSwOff ******/
	if(lcu1AvhForcedSwOff==0)
	{
		if((lcu1AvhSwitchOn==1)&&(lcu1AvhInhibitSwOn==1)&&(lcu8AvhEpbStateOld==EPB_RELEASING_BY_SW)&&(lcu8AvhEpbState==EPB_RELEASED))
		{
			lcu1AvhForcedSwOff = 1;
		}
		else { }
	}
	else
	{
		if((lcu1AvhSwitchOn==0)||(lcu1AvhInhibitSwOn==0))
		{
			lcu1AvhForcedSwOff = 0;
		}
		else { }
	}
/**************************************/  

/***** decide lcu1AvhForcedSwOffEpbAct ******/
	if(lcu1AvhForcedSwOffEpbAct==0)
	{
		if((lcu1AvhInhibitSwOn==0)&&(lcu1AvhSwitchOn==1))
		{
			if(((lcu8AvhEpbStateOld==EPB_RELEASING_BY_SW)&&(lcu8AvhEpbState==EPB_RELEASED))
			 ||((lcu8AvhEpbStateOld==EPB_CLAMPING_BY_SW)&&(lcu8AvhEpbState==EPB_CLAMPED))
			)
			{
				lcu1AvhForcedSwOffEpbAct = 1;
			}
			else { }
		}
		else { }
	}
	else
	{
		if(lcu1AvhSwitchOn==0)
		{
			lcu1AvhForcedSwOffEpbAct = 0;
		}
		else { }
	}
/**************************************/   	   
  #else
    lcu1AvhSWActReady = 1;
  #endif
}

static void	LCAVH_vCheckAvhSwitch(void)
{
  /* AVH S/W ON    signal : fu1AVHSwMode     : 0->ON, 1->OFF */
  /* AVH S/W Error signal : avh_sw_error_flg : 0->fail, 1->Normal */  

  #if ((__AVH_SW_TYPE==ANALOG_TYPE)||(__AVH_SW_TYPE==CAN_TYPE))
      #if __AVH_SWITCH_OFF_CONCEPT == 1
    if ((fu1AVHSwMode==0)&&(lcu1AvhDriverInVehicle==1)&&(lcu1AvhSwitchFail==0)) /* 0 : switch on */ 
    {
        lcu1AvhSwitchOn = 1;
    }
    else
    {
    	if(lcu1AvhSwitchFail==1)
    	{
    		if(lcu8AvhState==AVH_IN_ACTIVATION)
        	{
       			lcu1AvhSwitchOn = 1;
        	}
        	else
        	{
        		lcu1AvhSwitchOn = 0;
        	}
    	}
    	else if(fu1AVHSwMode==1)
    	{
            lcu1AvhSwitchOn = 0;
        }
        else if(lcu1AvhDriverInVehicle==0)
        {
        	if(lcu8AvhState==AVH_IN_ACTIVATION)
        	{
       			lcu1AvhSwitchOn = 1;
        	}
        	else
        	{
        		lcu1AvhSwitchOn = 0;
        	}
        }
        else
        {
        	;
        }
    }
      #elif __AVH_SWITCH_OFF_CONCEPT == 2
	lcu1AvhSwitchActOld = lcu1AvhSwitchAct;
    lcu1AvhSwitchAct = fu1AvhSwitchAct;
    lcu1AvhSwitchOnOld = lcu1AvhSwitchOn;
    if((lcu1AvhInhibitFlg==1)||(lcu1AvhSwitchFail==1)||((lcu1AvhActFlg==0)&&(lcu1AvhEpbFailFlg==1))) /* AVH warnning lamp on condition */
    {
    	lcu1AvhSwitchOn = 0;
    }
    else
    {
		if(lcu1AvhSwitchOn==0)  /* lamp off */
		{
			if(lcu1AvhInhibitSwOn==1) 								 { lcu1AvhSwitchOn = 0; }
			else if((lcu1AvhSwitchActOld==0)&&(lcu1AvhSwitchAct==1)) { lcu1AvhSwitchOn = 1; }
			else 													 { lcu1AvhSwitchOn = 0; }
		}
		else  /* lamp on */
		{
			if((lcu1AvhForcedSwOff==1)||(lcu1AvhForcedSwOffEpbAct==1)) 								 
			{
				lcu1AvhSwitchOn = 0; 
				lcu1AvhEpbActOkForSwOff = 0;
			}
			else if(lcu1AvhInhibitSwOff==1)							 { lcu1AvhSwitchOn = 1; }
			else if((lcu1AvhSwitchActOld==0)&&(lcu1AvhSwitchAct==1)) { lcu1AvhSwitchOn = 0; }
			else													 { lcu1AvhSwitchOn = 1; }
		}
	}
      #else
    if ((fu1AVHSwMode==0)&&(avh_sw_error_flg==1)) /* 0 : switch on */ 
    {
        lcu1AvhSwitchOn = 1;
    }
    else
    {      
    	lcu1AvhSwitchOn = 0;
    }
      #endif
  #else
      #if defined(__CAN_SW)&&(__AVH_SWITCH_USING_CTRL_BOX==1) /* Control Box HSA switch */
	if(HSA_CAN_SWITCH==1) 
	{
		lcu1AvhSwitchOn=1;
	}
	else 
	{
		lcu1AvhSwitchOn=0;
	}
	  #elif(__AVH_SWITCH_USING_ESC_SW==1)	
	if (fu1ESCDisabledBySW==1) /* ESC off switch */ 
	{
		lcu1AvhSwitchOn = 1;
	}
	else 
	{
		lcu1AvhSwitchOn = 0;
	}
	  #else
	     lcu1AvhSwitchOn = 0;
	  #endif
  #endif
}

static void	LCAVH_vCheckAvhAutoActRequest(void)
{
  #if (__AUTONOMOUS_HOLD_ENABLE==1)
      #if (__SCC==1)
    if(lcu1SccStopReqAVH==1)
    {
 	    lcu1AvhActReq=1;
    }
    else 
    {
 	    lcu1AvhActReq=0;
    }
      #endif
  #else
    lcu1AvhActReq=0;	  
  #endif     
}

static void	LCAVH_vCheckAvhVehicleTM(void)
{
	if((lespu1EMS_AT_TCU==1)||(lespu1EMS_CVT_TCU==1)||(lespu1EMS_DCT_TCU==1)||(lespu1EMS_HEV_AT_TCU==1))	
	{
		lcu1AvhAutoTM=AVH_AUTO_TM;
	}
	else
	{
		lcu1AvhAutoTM=AVH_MANUAL_TM;
	}
	
	/* For 10SWD Demo */
	#if ((__CAR==GM_V275)||(__CAR==GM_CTS)||(__CAR==RSM_QM5))
	  lcu1AvhAutoTM = AVH_AUTO_TM;
	#elif __CAR==FORD_C214
	  lcu1AvhAutoTM=AVH_MANUAL_TM;
	#endif
}

static void	LCAVH_vCheckMpressOn(void)
{
  #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
//	if((fu1BcpPriSusDet==0)&&(fu1BcpSecSusDet==0))
//	{
//		tempW1 = (lcans16AhbCircuitBoostPressP > lcans16AhbCircuitBoostPressS)? lcans16AhbCircuitBoostPressS:lcans16AhbCircuitBoostPressP; 
//	}
//	else if((fu1BcpPriSusDet==0)&&(fu1BcpSecSusDet==1))
//	{
//		tempW1 = lcans16AhbCircuitBoostPressP;
//	}
//	else if((fu1BcpPriSusDet==1)&&(fu1BcpSecSusDet==0))
//	{
//		tempW1 = lcans16AhbCircuitBoostPressS;
//	}
//	else /* both suspect */
//	{
//		tempW1 = lsesps16MpressByAHBgen3; /* average value */
//	}
	
	if (fu1BcpPriSusDet==0)
	{
		tempW1=lis16MasterPress;
	}
	else
	{
		tempW1 = lsesps16MpressByAHBgen3;
	}
	if(lcu1AvhMpressOn==0)
	{
		if((lsesps16EstBrkPressByBrkPedalF >= S16_AHB_AVH_PEDAL_ON_TH)&&(liu1AHBon==1)&&(tempW1 >= S16_AHB_AVH_PEDAL_ON_TH))
		{
			if(lcu8AvhAhbMpressOnCnt >= U8_AHB_AVH_PEDAL_ON_TH_TIME)
			{
				lcu1AvhMpressOn = 1;
			}
			else
			{
				lcu8AvhAhbMpressOnCnt = lcu8AvhAhbMpressOnCnt + 1;
			}
		}
		else
		{
			if(lcu8AvhAhbMpressOnCnt>0)
			{
				lcu8AvhAhbMpressOnCnt = lcu8AvhAhbMpressOnCnt - 1;
			}
			else
			{
				lcu8AvhAhbMpressOnCnt = 0;
			}
		}
	}
	else
	{
		lcu8AvhAhbMpressOnCnt = 0;
		if(lsesps16EstBrkPressByBrkPedalF < S16_AHB_AVH_PEDAL_OFF_TH) /*pedal suspect �߰� �ʿ� */
		{
			lcu1AvhMpressOn = 0;
		}
		else
		{
			;
		}
	}
	
	if(lcu1AvhMpressOn==1)
	{
		lcs16AvhMpress = tempW1;
	}
	else
	{
		lcs16AvhMpress = MPRESS_0BAR;
	}
  #elif (__BRK_SIG_MPS_TO_PEDAL_SEN==ENABLE) 
    lcs16AvhMpress = lsesps16EstBrkPressByBrkPedalF;
	lcu1AvhMpressOn = lsespu1PedalBrakeOn;	
  #elif (__BLS_NO_WLAMP_FM==ENABLE)
    lcs16AvhMpress = mpress;
	if((fu1BlsSusDet==1)||(fu1BLSErrDet==1))
	{
		if(lcu1AvhMpressOn==0)
		{
			if(lcs16AvhMpress >= (MPRESS_4BAR + S16_AVH_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1AvhMpressOn = 1;
			}
			else { }
		}
		else /* lcu1AvhMpressOn==1 */
		{
			if(lcs16AvhMpress < (MPRESS_1BAR + S16_AVH_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1AvhMpressOn = 0;
			}
			else { }
		}
	}
	else
	{
		lcu1AvhMpressOn = MPRESS_BRAKE_ON;
	}
  #else
    lcs16AvhMpress = mpress;
    lcu1AvhMpressOn = MPRESS_BRAKE_ON;
  #endif
}

static void	LCAVH_vCheckAvhInsideVehicle(void)
{

    if(lcu8AvhInitIgnOnCnt<T_1000_MS) {lcu8AvhInitIgnOnCnt++;}
    else {lcu8AvhInitIgnOnCnt = T_1000_MS;}

    if(lcu8AvhInitIgnOnCnt<=T_100_MS) {lcu1AvhDriverInVehicle = 1;}
    else { }	

	if(lcu1AvhDriverInVehicle==0)
	{
		if((lcu8AvhDrvDoorOpen==0)&&((lcu8AvhDrvSeatUnBelted==0)||(lcu1AvhMpressOn==1)))
	    {
	    	if(lcu8AvhDriverInsideCnt>U8_AVH_DRIVER_INSIDE_DCT_TIME)
	    	{
                lcu8AvhDriverInsideCnt=0;
                lcu1AvhDriverInVehicle=1;                
            }
            else
            {
            	lcu8AvhDriverInsideCnt = lcu8AvhDriverInsideCnt + 1;
            }
	    }
	    else
	    {
            lcu8AvhDriverInsideCnt=0;
	    }
	}
	else /* lcu1AvhDriverInVehicle==1 */
	{
		  #if __AVH_HOOD_TRUNK_CONCEPT==2
		if((lcu8AvhDrvSeatUnBelted!=0)&&(lcu8AvhDrvDoorOpen!=0))
		  #else
		if((lcu8AvhDrvSeatUnBelted==1)&&(lcu8AvhDrvDoorOpen==1))
		  #endif
		{
			if(lcu8AvhDriverInsideCnt>U8_AVH_DRIVER_OUTSIDE_DCT_TIME)
			{
				lcu8AvhDriverInsideCnt=0;
			    lcu1AvhDriverInVehicle=0;	
			}
			else
			{
				lcu8AvhDriverInsideCnt = lcu8AvhDriverInsideCnt + 1;
			}
		}
		else
		{
			lcu8AvhDriverInsideCnt=0;
		}
	}
}

static void	LCAVH_vGetAxSensor(void)
{

  #if __AX_SENSOR
    lcs16AvhAxG = lss16absAxOffsetComp1000g/10;
  #else
    lcs16AvhAxG = 0;       
  #endif

	if(lcs16AvhAxG>=0) {lcs16AvhAxGAbs = lcs16AvhAxG;}
	else               {lcs16AvhAxGAbs = -lcs16AvhAxG;}

    lcs16AvhFilteredAxOld = lcs16AvhFilteredAx;
    lcs16AvhFilteredAx = LCESP_s16Lpf1Int(lcs16AvhAxG, lcs16AvhFilteredAx, L_U8FILTER_GAIN_10MSLOOP_10HZ);  /* 7Hz */

    if ((lcs16AvhFilteredAxOld==lcs16AvhFilteredAx)&&(vref<=VREF_0_5_KPH))	
	{
		if (lcu8AvhAxStationaryCnt<L_U8_TIME_10MSLOOP_1500MS) {lcu8AvhAxStationaryCnt = lcu8AvhAxStationaryCnt + 1;}
		else { }
		
		if (lcu8AvhAxStationaryCnt>L_U8_TIME_10MSLOOP_1000MS) {lcu1AvhAxStationary = 1;}
		else { }
	}
	else
	{
		lcu1AvhAxStationary=0;
		lcu8AvhAxStationaryCnt=0;
	}
}

static void	LCAVH_vCheckHillGrade(void)
{
	if(lcu1AvhMaxHill==0)	
	{
		lcs16AvhMaxHillOnCnt = 0;
	    if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1))
	    {
	    	  #if __AVH_MAXHILL_UP_DW_SEPARATE == ENABLE
	    	if((lcu1AvhAxStationary==1)&&((lcs16AvhAxG>=(int16_t)U8_AVH_ACT_MAX_GRADE)||(lcs16AvhAxG<=(int16_t)S8_AVH_ACT_MAX_GRADE_DW)))
	    	  #else
	        if((lcu1AvhAxStationary==1)&&(lcs16AvhAxGAbs>=(int16_t)U8_AVH_ACT_MAX_GRADE))
	          #endif
	        {
	        	if(lcu8AvhMaxHillCnt>L_U8_TIME_10MSLOOP_200MS)
	        	{
	        	    lcu1AvhMaxHill=1;
	        	    lcu8AvhMaxHillCnt=0;
	        	}
	        	else
	        	{
	        		lcu8AvhMaxHillCnt = lcu8AvhMaxHillCnt + 1;
	        	}
	        }
	        else
	        {
	        	lcu8AvhMaxHillCnt=0;
	        }
	    }
	    else
	    {
	    	lcu8AvhMaxHillCnt=0;
	    }
	}
	else /* lcu1AvhMaxHill==1 */
	{
		if((lcu1AvhActReady==0)&&(lcu1AvhActFlg==0)&&(lcu1AvhAutonomousActFlg==0))
		{
    	    lcu1AvhMaxHill=0;
    	}
		else
		{
			;
		}
		
		if (lcs16AvhMaxHillOnCnt<L_U16_TIME_10MSLOOP_1MIN)
		{
			lcs16AvhMaxHillOnCnt = lcs16AvhMaxHillOnCnt + 1;
		}
		else
		{
			lcs16AvhMaxHillOnCnt = L_U16_TIME_10MSLOOP_1MIN;
		}
	}
}
  #if __ISG
static void	LCAVH_vCheckIsgActState(void)
{
	lcu8AvhEngIsgControlActOld = lcu8AvhEngIsgControlAct;

    if(ccu1IsgEmsH2Timeout==1) /* EMS_H2 msg timeout */
    {
        lcu8AvhEngIsgControlAct = 0;
    }
    else
    {
	    if((lespu8EMS_EmsIsgStat==AVH_ISG_ACTIVE)||(lespu8EMS_EmsIsgStat==AVH_ISG_ENG_AUTO_START))
	    {
	    	lcu8AvhEngIsgControlAct = 1;
	    }
	    else
	    {
	    	lcu8AvhEngIsgControlAct = 0;
	    }
    }
	
    if((lcu8AvhEngIsgControlActOld==1)&&(lcu8AvhEngIsgControlAct==0)) /* detect falling edge ISG On to Off */
    {
    	lcu8AvhIsgActOldCnt = (uint8_t)S16_AVH_ISG_IDLETOQ_INHIBIT_TIME;
    }
    else if(lcu8AvhIsgActOldCnt>0)
    {
    	lcu8AvhIsgActOldCnt = lcu8AvhIsgActOldCnt - 1;
    }
    else
    {
    	lcu8AvhIsgActOldCnt = 0;
    }
}
  #endif
static void	LCAVH_vCheckAtGearPosition(void)
{
    /* gs_sel->0:P 1:L 2:2 3:3 5:D 6:N 7:R 8:sport mode/manual */		
	if (gs_sel<10) 
	{
		if(gs_sel==0) /* parking */
		{
			lcu8AvhGearPositionOld = lcu8AvhGearPosition;
			if(lcu8AvhParkGearCnt>0) 
			{
				lcu8AvhParkGearCnt = lcu8AvhParkGearCnt - 1;
			}
			else
			{
				lcu8AvhParkGearCnt = 0;
				lcu8AvhGearPosition = (uint8_t)gs_sel;
			}
		}
		else
		{
			lcu8AvhParkGearCnt = U8_AVH_AT_PARK_GEAR_DELAY_TIME;
	    lcu8AvhGearPositionOld = lcu8AvhGearPosition;
	    lcu8AvhGearPosition = (uint8_t)gs_sel;
	}
	    /*lcu8AvhGearPositionOld = lcu8AvhGearPosition;
	    lcu8AvhGearPosition = (uint8_t)gs_sel;*/
	}
	else 
	{
		;  /* For Except unvalid gear selection */
	}

    if((lcu8AvhGearPosition==AVH_DRIVE)||(lcu8AvhGearPosition==AVH_SPORTS))
    {
    	lcu8AvhGearPosition = AVH_FORWARD;
    }
    else { }

	if(lcu8AvhGearPositionOld==lcu8AvhGearPosition)
	{
		if(lcu8AvhGearChageCnt>=L_U8_TIME_10MSLOOP_1000MS) {lcu8AvhGearChageCnt = L_U8_TIME_10MSLOOP_1000MS;}
		else {lcu8AvhGearChageCnt = lcu8AvhGearChageCnt + 1;}
	}
	else {lcu8AvhGearChageCnt = 0;}

	if(lcu8AvhGearChageCnt>L_U8_TIME_10MSLOOP_200MS) {lcu1AvhGearChange=1;}
	else                             {lcu1AvhGearChange=0;}
}


static void	LCAVH_vCheckMtGearPosition(void)
{
    /* For 10SWD Demo */
  #if __CAR==FORD_C214
    lcu8AvhGearPositionOld=lcu8AvhGearPosition;
    if(b_ClutchPedalSwitch_b==1)
    {
    	lcu1AvhClutchPressed=1;
    	lcu8AvhTemp0=AVH_DRIVE;
    }
    else
    {
    	lcu1AvhClutchPressed=0;
    	lcu8AvhTemp0=AVH_NEUTRAL;
    }
    
    if(cmu16_TransmissionGearPosition==14)
    {
    	lcu1AvhMtReverseGear=1;
    	lcu8AvhTemp0=AVH_REVERSE;
    }
    else
    {
    	lcu1AvhMtReverseGear=0;
    }
    lcu8AvhGearPosition=lcu8AvhTemp0;
  #else	
  
    #if (__GEAR_POSITION_SENSOR_ENABLE==ENABLE)
	  lcu8AvhGearPositionOld = lcu8AvhGearPosition;
	  lcu8AvhGearPosition = (uint8_t)gs_sel;
    #elif (__GEAR_SWITCH_TYPE!=NONE)&&(__CLUTCH_SWITCH_TYPE!=NONE)
       lcu8AvhGearPositionOld=lcu8AvhGearPosition;
 
       if(fu1GearR_Switch==1)
       {
           lcu8AvhTemp0=AVH_REVERSE;	
       }
       else
       {
           if(lespu1EPB_GEAR_N_SWITCH_NEUTRAL==1)
           {
           	   lcu8AvhTemp0=AVH_NEUTRAL;
           }
           else
           {
           	   lcu8AvhTemp0=AVH_FORWARD;
           }
       }
	   lcu8AvhGearPosition=lcu8AvhTemp0;

       if(fu1ClutchSwitch==1) 
       {
           lcu1AvhClutchPressed=1;
       } 
       else 
       {
           lcu1AvhClutchPressed=0;
       }
        
       if(fu1GearR_Switch==1)
       {
           lcu1AvhMtReverseGear=1;
       }
       else 
       {
           lcu1AvhMtReverseGear=0;
       }
    #else
   	  lcu8AvhGearPositionOld=lcu8AvhGearPosition;
      lcu8AvhGearPosition=AVH_DRIVE;
    #endif

  #endif
}

static void	LCAVH_vCheckEpbState(void)
{
	lcu8AvhEpbStateOld = lcu8AvhEpbState;
	
    if(lcu8epbStatusEPB==EPB_RELEASED_STATE)
    {
    	lcu8AvhEpbState = EPB_RELEASED;
    }
    else if(lcu8epbStatusEPB==EPB_CLAMPED_STATE)
    {
    	lcu8AvhEpbState = EPB_CLAMPED;
    }
    else if(lcu8epbStatusEPB==EPB_RELEASING_STATE)
    {
    	lcu8AvhEpbState = EPB_RELEASING;
    }    
    else if(lcu8epbStatusEPB==EPB_CLAMPING_STATE)
    {
    	lcu8AvhEpbState = EPB_CLAMPING;
    }
    else if(lcu8epbStatusEPB==EPB_DYNAMIC_BRK_STATE)
    {
    	lcu8AvhEpbState = EPB_DYNAMIC_BRK;
    }        
	else if(lcu8epbStatusEPB==EPB_RELEASING_BY_SW_STATE)
    {
    	lcu8AvhEpbState = EPB_RELEASING_BY_SW;
    }
	else if(lcu8epbStatusEPB==EPB_CLAMPING_BY_SW_STATE)
    {
    	lcu8AvhEpbState = EPB_CLAMPING_BY_SW;
    }       
    else
    {
    	lcu8AvhEpbState = EPB_UKNOWN;
    }
    
    if (fu1EPBErrorDet==1)
    {
    	lcu1AvhEpbFailFlg=1;
    }
    else
    {
    	lcu1AvhEpbFailFlg=0;
    }
    
    #if __AVH_SWITCH_USING_ESC_SW==1
      lcu8AvhEpbState = EPB_RELEASED;
      lcu1AvhEpbFailFlg=0;
    #endif
   
}	

static void	LCAVH_vCheckEnterCondition(void)
{
	/*LCAVH_vDecideAvhEnterPress();*/
	LCAVH_vCheckAvhEntCommonnCnd();

/********************************* stand alone mode enter condition **************************************/
	if((lcu1AvhComCndSatisfy==1)
     &&(lcu1AvhSwitchOn==1)	
     &&(lcs16AvhMpress>=lcs16AvhEnterPress)
	 &&(lcu8AvhMpOkCnt>L_U8_TIME_10MSLOOP_100MS)
       #if (__AVH_HOOD_TRUNK_CONCEPT==1)
     &&(lcu1AvhDriverInVehicle==1)
     &&(lcu8AvhHoodOpen==0)
     &&(lcu8AvhTrunkOpen==0)
       #elif (__AVH_HOOD_TRUNK_CONCEPT==2)
       #else
     &&(lcu1AvhDriverInVehicle==1)  
     &&(((lcu8AvhHoodOpen==0)&&(lcu8AvhGearPosition==AVH_FORWARD))
       ||((lcu8AvhTrunkOpen==0)&&(lcu8AvhGearPosition==AVH_REVERSE))
       ||(lcu8AvhGearPosition==AVH_NEUTRAL))
       #endif
	)
	{
		lcu1AvhActReady=1;
		  #if __AVH_NOISE_REDUCTION == ENABLE
        if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+lcs16AvhValveActMp))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		  ||(lcs16AvhMpress<=(lcs16AvhSafeHoldPress+MPRESS_3BAR))
		)          
          #else
		if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+MPRESS_1BAR))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		  ||(lcs16AvhMpress<=lcs16AvhSafeHoldPress)
		)
		  #endif
		{
			lcu1AvhEnterSatisfy=1;
		}
		else 
		{
			;
		}
	}
	else if(lcu1AvhActReady==1)
	{
		lcu1AvhEnterSatisfy=1;
	}
	else
	{
		lcu1AvhActReady=0;
		lcu1AvhEnterSatisfy=0;
	}

    if((lcu1AvhAutoTM==AVH_MANUAL_TM)&&(lespu1EPB_GEAR_N_SWITCH_INVALID==1)) /* DO NOT ENTER at M/T Neutral gear invalid */
    {
    	lcu1AvhActReady=0; 
		lcu1AvhEnterSatisfy=0;
    }
    else
    {
    	;
    }
/******************************************* autonomous enter condition *************************************/	
	if((lcu1AvhActReq==1)&&(lcu1AvhComCndSatisfy==1))
	{
		if(lcs16AvhMpress>=lcs16AvhMinEntPres)
		{
			  #if __AVH_NOISE_REDUCTION == ENABLE
            if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+lcs16AvhValveActMp))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		      ||(lcs16AvhMpress<=(lcs16AvhSafeHoldPress+MPRESS_3BAR))
		    )          
              #else
		    if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+MPRESS_1BAR))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		      ||(lcs16AvhMpress<=lcs16AvhSafeHoldPress)
		    )
		      #endif
		    {
		    	lcu1AvhEnterSatisfy2=1;
		    }
		    else { }
		}
		else
		{
		    lcu1AvhEnterSatisfy2=1;
		}
	}
	else 
	{
		;
	}

	if(((lcu1AvhEnterSatisfy==1)||(lcu1AvhEnterSatisfy2==1))
	&&((ABS_fz==1) || (BTC_fz==1) || (YAW_CDC_WORK==1))) /* need to change ESC activation signal*/
	{
	   lcu1AvhEnterSatisfy = 0;
	   lcu1AvhEnterSatisfy2 = 0;
	}
	else 
	{
		;
	}
}

static void	LCAVH_vCheckAvhEntCommonnCnd(void)
{
	LCAVH_vCheckAvhEntSpeed();
	
	if((lcu1AvhInhibitFlg==0)                      /* AVH not fail */
      &&(lcu1AvhEpbFailFlg==0)                     /* EPB not fail */
      &&(lcu8AvhEpbStateOld==EPB_RELEASED)
      &&(lcu8AvhEpbState==EPB_RELEASED)            /* EPB is released */
      &&(lcu8AvhGearPosition!=AVH_PARKING)         /* Gear selection condition */
      #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      &&(wu8IgnStat==CE_ON_STATE)
      #else
      &&((eng_rpm > 100)                           /* Engine Run (RPM) */ 
        #if __ISG
      ||(lcu8AvhEngIsgControlAct==1)  /* Engine Run (ISG state) */
        #endif
      )
      #endif
      &&(mtp<3)                                    /* Accel Pedal */
      &&(lcu8AvhEntSpdOk==1)                       /* Speed */
    )
    {
        lcu1AvhComCndSatisfy = 1;	
    }
    else
    {
    	lcu1AvhComCndSatisfy = 0;
    }
}
static void	LCAVH_vCheckAvhEntSpeed(void)
{
	if((vrad_fl<=S16_AVH_ENTER_MIN_SPEED)&&(vrad_fr<=S16_AVH_ENTER_MIN_SPEED)&&(vrad_rl<=S16_AVH_ENTER_MIN_SPEED)&&(vrad_rr<=S16_AVH_ENTER_MIN_SPEED))
	{
		lcu8AvhTemp0 = 0;
		if(vrad_fl<=VREF_0_125_KPH) {lcu8AvhTemp0 = lcu8AvhTemp0 + 1;}
		if(vrad_fr<=VREF_0_125_KPH) {lcu8AvhTemp0 = lcu8AvhTemp0 + 1;}
		if(vrad_rl<=VREF_0_125_KPH) {lcu8AvhTemp0 = lcu8AvhTemp0 + 1;}
		if(vrad_rr<=VREF_0_125_KPH) {lcu8AvhTemp0 = lcu8AvhTemp0 + 1;}
		
		if(lcu8AvhTemp0>=2)
		{
			lcu8AvhEntSpdOk = 1;
		}
		else
		{
			lcu8AvhEntSpdOk = 0;
		}
	}
	else
	{
		lcu8AvhEntSpdOk = 0;
	}
}

static void	LCAVH_vDecideAvhEnterPress(void)
{
	if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1))
	{
      #if __SENSITIVE_ROLL_DETECT
		if(lcu1AvhAutonomousActFlg==1)
		{
			if(lcs16AvhSafeHoldPress<(S16_AVH_MIN_SLOP_SAFE_H_P+MPRESS_5BAR))
			{
				lcs16AvhSafeHoldPress = S16_AVH_MIN_SLOP_SAFE_H_P+MPRESS_5BAR;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	  #else
		;
	  #endif
	}
	else
	{
		  if (lcu1AvhAutoTM==AVH_AUTO_TM)
		  {
		      lcs16AvhMinEntPres = S16_AVH_MIN_SLOP_MIN_H_P;  	
		  }
		  else /* M/T */
		  {
		      lcs16AvhMinEntPres = S16_AVH_MIN_SLOP_MIN_H_P_MT;
		  }
		  
        if(lcs16AvhAxG>=0)
		  {
              if(lcu8AvhGearPosition==AVH_FORWARD)
              {
                  tempW1 = S16_AVH_L_M_SLOP_MIN_H_P_UP;
                  tempW2 = S16_AVH_H_M_SLOP_MIN_H_P_UP;
                  tempW3 = S16_AVH_MAX_SLOP_MIN_H_P_UP;

                  tempW4 = S16_AVH_MIN_SLOP_SAFE_H_P;
                  tempW5 = S16_AVH_L_M_SLOP_SAFE_H_P_UP;
                  tempW6 = S16_AVH_H_M_SLOP_SAFE_H_P_UP;
                  tempW7 = S16_AVH_MAX_SLOP_SAFE_H_P_UP;
              }
              else
              {
                  tempW1 = S16_AVH_LOW_MID_SLOP_MIN_H_P;
                  tempW2 = S16_AVH_HIGH_MID_SLOP_MIN_H_P;
                  tempW3 = S16_AVH_MAX_SLOP_MIN_H_P;

                  tempW4 = S16_AVH_MIN_SLOP_SAFE_H_P;
                  tempW5 = S16_AVH_LOW_MID_SLOP_SAFE_H_P;
                  tempW6 = S16_AVH_HIGH_MID_SLOP_SAFE_H_P;
                  tempW7 = S16_AVH_MAX_SLOP_SAFE_H_P;
              }
          }
          else
          {
              if(lcu8AvhGearPosition==AVH_REVERSE)
              {
                  tempW1 = S16_AVH_L_M_SLOP_MIN_H_P_UP;
                  tempW2 = S16_AVH_H_M_SLOP_MIN_H_P_UP;
                  tempW3 = S16_AVH_MAX_SLOP_MIN_H_P_UP;

                  tempW4 = S16_AVH_MIN_SLOP_SAFE_H_P;
                  tempW5 = S16_AVH_L_M_SLOP_SAFE_H_P_UP;
                  tempW6 = S16_AVH_H_M_SLOP_SAFE_H_P_UP;
                  tempW7 = S16_AVH_MAX_SLOP_SAFE_H_P_UP;
              }
              else
              {          
                  tempW1 = S16_AVH_LOW_MID_SLOP_MIN_H_P;
                  tempW2 = S16_AVH_HIGH_MID_SLOP_MIN_H_P;
                  tempW3 = S16_AVH_MAX_SLOP_MIN_H_P;

                  tempW4 = S16_AVH_MIN_SLOP_SAFE_H_P;
                  tempW5 = S16_AVH_LOW_MID_SLOP_SAFE_H_P;
                  tempW6 = S16_AVH_HIGH_MID_SLOP_SAFE_H_P;
                  tempW7 = S16_AVH_MAX_SLOP_SAFE_H_P;
              }
          }
                                
        lcs16AvhSafeHoldPress = LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP, tempW4,
                                                                      (int16_t)U8_AVH_LOW_MID_SLOP, tempW5,
                                                                      (int16_t)U8_AVH_HIGH_MID_SLOP, tempW6,
                                                                      (int16_t)U8_AVH_MAX_SLOP, tempW7);

        lcs16AvhEnterPress = LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP, lcs16AvhMinEntPres,
                                                                   (int16_t)U8_AVH_LOW_MID_SLOP, tempW1,
                                                                   (int16_t)U8_AVH_HIGH_MID_SLOP, tempW2,
                                                                   (int16_t)U8_AVH_MAX_SLOP, tempW3);
	
        if((fu1BlsSusDet==0)&&(fu1BLSErrDet==0)&&(BLS==1))
        {
        	;
        }
        else
        {
          #if (__BLS_NO_WLAMP_FM==ENABLE)
            lcs16AvhEnterPress = lcs16AvhEnterPress + S16_AVH_MAX_MP_OFFSET_BLS_FAIL;
            lcs16AvhSafeHoldPress = lcs16AvhSafeHoldPress + S16_AVH_MAX_MP_OFFSET_BLS_FAIL;
          #else
        	lcs16AvhEnterPress = lcs16AvhEnterPress + MPRESS_5BAR;
          #endif
        }
    }

    if(lcs16AvhSafeHoldPress<lcs16AvhEnterPress)
    {
    	lcs16AvhSafeHoldPress = lcs16AvhEnterPress;
    }
    else
    {
    	;
    }

    if(lcs16AvhMpress>=lcs16AvhEnterPress)
    {
    	if(lcu8AvhMpOkCnt>=L_U8_TIME_10MSLOOP_1000MS)
    	{
    		lcu8AvhMpOkCnt=L_U8_TIME_10MSLOOP_1000MS;
    	}
    	else
    	{
    		lcu8AvhMpOkCnt = lcu8AvhMpOkCnt + 1;
    	}
    }
    else
    {

    	if(lcs16AvhMpress<lcs16AvhMinEntPres)
    	{
    	    lcu8AvhMpOkCnt = 0;	
    	}
    	else if(lcu8AvhMpOkCnt>0)
    	{
    		lcu8AvhMpOkCnt=lcu8AvhMpOkCnt-1;
    	}
    	else
    	{
    		lcu8AvhMpOkCnt = 0;
    	}
    }

    LCAVH_vCalcMpSlope();
}

static void	LCAVH_vCalcMpSlope(void)
{
    lcs16AvhMpressSlopThres = LCESP_s16IInter2Point(lcs16AvhMpress, MPRESS_10BAR, -MPRESS_0G2BAR, MPRESS_20BAR, -MPRESS_0G5BAR);
    
    lcs16AvhTemp0 = -(MCpress[0]-MCpress[2]);
    lcs16AvhValveActMp = LCESP_s16IInter2Point(lcs16AvhTemp0, S16_AVH_MIN_MP_SLOP, S16_AVH_VV_ACT_COMP_MIN_MP_SLP, 
															  S16_AVH_MAX_MP_SLOP, S16_AVH_VV_ACT_COMP_MAX_MP_SLP);    
}	

static void	LCAVH_vCheckExitCondition(void)
{
    LCAVH_vCheckAccelIntend();
	
	/* AVH stand alone mode */
	if(lcu1AvhActFlg==1)
	{
		/* press Slow release condition */
		/*
		if((lcu1AvhSwitchOn==0)&&(lcs16AvhEpbReqCnt>=T_2_S))
		{
			lcu1AvhSlowExitSatisfy = 1;
		}
		else { }
		*/

  		  #if __AVH_EXIT_AT_SBW_GEAR_P == ENABLE
		if((lcu8AvhGearPosition==AVH_PARKING)&&(lcu1AvhDriverInVehicle==1)&&(lcu1AvhMpressOn==0))
		{
			lcu1AvhSlowExitSatisfy = 1;
		}
		else { }
		  #endif

        /* press Medium release condition */
		if((lcu1AvhMpressOn==1)&&((lcu1AvhSwitchOn==0)||(lcu1AvhEpbFailFlg==1)))
		{
		    lcu1AvhMedExitSatisfy = 1;
		}
		else { }

        /* press Quickly release condition */
        lcs16AvhTemp0 = LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP,      S16_AVH_F_END_E_TQ_MIN_SLOP,
                                                              (int16_t)U8_AVH_LOW_MID_SLOP,  S16_AVH_F_END_E_TQ_LOW_MID_SLOP,
                                                              (int16_t)U8_AVH_HIGH_MID_SLOP, S16_AVH_F_END_E_TQ_MID_SLOP,
                                                              (int16_t)U8_AVH_MAX_SLOP,      S16_AVH_F_END_E_TQ_MAX_SLOP );

		  #if __AVH_EXIT_AT_SBW_GEAR_P == ENABLE
		if(((lcu8AvhGearPosition==AVH_PARKING)&&(lcu1AvhDriverInVehicle==1)&&(lcu1AvhMpressOn==1))
		  #else
		if((lcu8AvhGearPosition==AVH_PARKING)
		  #endif
		||((lcs16AvhFilteredTorq>lcs16AvhTemp0)&&(mtp>10)&&(lcu1AvhMpressOn==0)&&(lcu8AvhGearPosition!=AVH_NEUTRAL))	
		||(BTCS_ON==1)
		||((mtp>S16_AVH_FAST_EXIT_MTP_TH)&&((FL.arad>=S16_AVH_FAST_EXIT_ARAD_TH)||(FR.arad>=S16_AVH_FAST_EXIT_ARAD_TH)||(RL.arad>=S16_AVH_FAST_EXIT_ARAD_TH)||(RR.arad>=S16_AVH_FAST_EXIT_ARAD_TH)))
		) 
		{
			lcu1AvhFastExitSatisfy = 1;
		}
		else { }
	}
	else { }
	
	/* AVH autonomous mode */
	if(lcu1AvhAutonomousActFlg==1)
	{
		  #if (__SCC==1)
	    if((lcu1SccCtrlReq==0)&&(lcu1SccEpbReqAVH==0))
	    {
	    	lcu1AvhMedExitSatisfy = 1;
	    }
	    else { }
	      #endif
	}
	else { }
	
	if((lcu1AvhSlowExitSatisfy==0)&&(lcu1AvhMedExitSatisfy==0)&&(lcu1AvhFastExitSatisfy==0)&&(lcu1AvhDownDrive==0)&&(lcu1AvhAccelExitOK==0))
	{
        LCAVH_vCheckAvhToEpbTransition(); 
    }
    else 
    {
    	lcu1AvhEpbActReq = 0;
    	lcu1AvhAlarmReq = 0;
	}
}

static void	LCAVH_vCheckAvhToEpbTransition(void)
{
	if(lcu1AvhEpbFailFlg==0)
	{
	    if(lcu8AvhEpbState==EPB_RELEASED) 
	    {
	    	if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
	    	{
	    		if((lcs16AvhHoldCnt>=S16_MAX_ON_TIME_ONE_CYCLE)
	    		||(lcu8AvhTotalRiseAtempt>=U8_MAX_ROLL_RISE_ATTEMPT)
	    		||(lcu1AvhVehicleRoll==1)
	    		||(lcu1AvhInhibitFlg==1)
	    		||(lcu1AvhSwitchFail==1)
	    		||((lcu1AvhMaxHill==1)&&(lcs16AvhMaxHillOnCnt>S16_EPB_ACT_DELAY_MAXHILL))
	    		  #if (__AVH_HOOD_TRUNK_CONCEPT==1)
	    		||(lcu1AvhSwitchOn==0)
	    		||(lcu8AvhHoodOpen==1)
	    		||(lcu8AvhTrunkOpen==1)
	    		  #elif (__AVH_HOOD_TRUNK_CONCEPT==2)
				||((lcu8AvhHoodOpen!=0)&&(lcu8AvhGearPosition==AVH_FORWARD)) 	
	    		||((lcu8AvhTrunkOpen!=0)&&(lcu8AvhGearPosition==AVH_REVERSE))	    		  
	    		  #else
	    		||(lcu1AvhSwitchOn==0)
	    		||((lcu8AvhHoodOpen==1)&&(lcu8AvhGearPosition==AVH_FORWARD)) 	
	    		||((lcu8AvhTrunkOpen==1)&&(lcu8AvhGearPosition==AVH_REVERSE))
	    		  #endif
	    		||(lcu1AvhDriverInVehicle==0)
	    		||(lcu1AvhTcOverTemper==1)
	    		||(lcu1AvhOverActTime==1)
                #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
                ||(wu8IgnStat==CE_OFF_STATE)  
                #else
	    		||((eng_rpm<50)&&(wu8IgnStat==CE_ON_STATE)
	    		  #if __ISG
                &&(lcu8AvhEngIsgControlAct==0)
                  #endif
                )
	    		#endif
		   		)
	    		{
	    			lcu1AvhEpbActReq=1;
	    		}
	    		else
	    		{
	    			;
	    		}
	    	}
	    	else if(lcu1AvhAutonomousActFlg==1)
	    	{
	    		if((lcs16AvhHoldCnt>=S16_MAX_ON_TIME_ONE_CYCLE_AUTO)
    		    ||(lcu8AvhTotalRiseAtempt>=U8_MAX_ROLL_RISE_ATTEMPT)
	    		||(lcu1AvhVehicleRoll==1)
	    		||(lcu1AvhInhibitFlg==1)
	    		||((lcu1AvhMaxHill==1)&&(lcs16AvhMaxHillOnCnt>S16_EPB_ACT_DELAY_MAXHILL))
	    		||(lcu1AvhDriverInVehicle==0)
	    		||(lcu1AvhTcOverTemper==1)
                ||(lcu1AvhAutoOverActTime==1)
                #if (__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
                ||(wu8IgnStat==CE_OFF_STATE)  
                  #else
	    		||((eng_rpm<50)&&(wu8IgnStat==CE_ON_STATE)
	    		  #if __ISG
                &&(lcu8AvhEngIsgControlAct==0)
                  #endif
                )
                #endif	    		
	    		)
	    		{
                    lcu1AvhEpbActReq=1;
	    		}
	    		else
	    		{
	    			;
	    		}
	    	}
	    	else
	    	{
	    		;
	    	}
	    }
	    else if ((lcu8AvhEpbState==EPB_CLAMPING)||(lcu8AvhEpbState==EPB_CLAMPING_BY_SW))
	    {
            ;
	    }
	    else if ((lcu8AvhEpbState==EPB_RELEASING)||(lcu8AvhEpbState==EPB_RELEASING_BY_SW))
	    {
            lcu1AvhEpbActReq=0;
            lcu1AvhFastExitSatisfy = 1;
	    }
	    else if (lcu8AvhEpbState==EPB_CLAMPED) 
	    {
	    	  #if ( __AVH_SWITCH_OFF_CONCEPT == 2)
	    	if((lcu1AvhInhibitSwOn==1)&&(lcu1AvhAutonomousActFlg==0)) 
	    	{
	    		lcu1AvhEpbActOkForSwOff = 1; 
	    	}
	    	else { lcu1AvhEpbActOkForSwOff = 0; }
	    	  #endif
            lcu1AvhEpbActReq=0;
	    	lcu1AvhSlowExitSatisfy = 1;
	    }
	    else /* EPB Unknown state */
	    {
            ;
	    }
	}
	else /* EPB error during AVH act */
	{
		lcu1AvhAlarmReq = 1;
	}

  #if (__AVH_INFINITE_HOLD_EPB_FAIL == DISABLE)
	if ((lcs16AvhEpbReqCnt>=L_U8_TIME_10MSLOOP_2S)||(lcu1AvhAlarmReq==1))
	{
		if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
	    {
	        if((lcs16AvhHoldCnt>=(S16_MAX_ON_TIME_ONE_CYCLE+S16_ADD_ACT_TIME_NO_ACT_EPB))
	        ||(lcu1AvhOverActTime==1)
	        ||(lcu1AvhTcOverTemper==1)
	        )
	        {
	        	lcu1AvhSlowExitSatisfy = 1;
	        }
	    }
	    else if(lcu1AvhAutonomousActFlg==1)
	    {
	    	if((lcs16AvhHoldCnt>=(S16_MAX_ON_TIME_ONE_CYCLE_AUTO+S16_ADD_ACT_TIME_NO_ACT_EPB))
	    	||(lcu1AvhAutoOverActTime==1)
	    	||(lcu1AvhTcOverTemper==1)
	    	)
	    	{
	    		lcu1AvhSlowExitSatisfy = 1;
	    	}
	    }
	    else
	    {
	    	;
	    }
	}
	else
	{
		;
	}
  #endif		
}

static void	LCAVH_vCalcTargetPress(void)
{
	if (lcs8AvhControlState==AVH_HOLD)
	{
		if(lcu1AvhRiseModeEnd==1)
		{
		    lcs16AvhTargetPress = lcs16AvhRetainingPress;
		    
		    if(lcs16AvhTargetPress>=S16_AVH_MAX_SLOP_SAFE_H_P)
			{
				lcs16AvhTargetPress=S16_AVH_MAX_SLOP_SAFE_H_P;
			}
			else{ }
		}
		else 
		{
			if(lcs16AvhMpress>lcs16AvhSafeHoldPress)
			{
				lcs16AvhRetainingPress = lcs16AvhSafeHoldPress;
				lcs16AvhTargetPress = lcs16AvhSafeHoldPress; 
			}
			else
			{
				if(lcs16AvhMpress>lcs16AvhRetainingPress)
                {
            	    lcs16AvhRetainingPress = lcs16AvhMpress;
                }
                else { }	
			    lcs16AvhTargetPress = lcs16AvhRetainingPress;
			}
		}
	}
	else if (lcs8AvhControlState==AVH_APPLY)
	{
        lcs16AvhTpApplyInit = lcs16AvhRetainingPress;

        if(lcu1AvhRiseInit==1)
        {
            lcs16AvhTpApply = lcs16AvhSafeHoldPress;
        }
        else if(lcs16AvhApplyCnt==0)
        {
        	lcs16AvhTpApply = lcs16AvhTargetPress + S16_ROLL_RISE_PRESSURE;
        }
        else
        {
        	;
        }
            
	    lcs16AvhTargetPress = LCESP_s16IInter2Point(lcs16AvhApplyCnt,	0,	lcs16AvhTpApplyInit,
                   	                                (lcs16AvhApplyTime-1),	lcs16AvhTpApply);

	    if(lcs16AvhTargetPress>=S16_AVH_MAX_SLOP_SAFE_H_P)
		{
			lcs16AvhTargetPress=S16_AVH_MAX_SLOP_SAFE_H_P;
		}
		else{ }
		    		
		lcs16AvhRetainingPress = lcs16AvhTargetPress;
	}
	else if (lcs8AvhControlState==AVH_RELEASE)
	{
		tempW1 = MFC_Current_AVH_old - MFC_Current_AVH_S;
		if(tempW1>0)
		{
			tempW2 = LCESP_s16IInter3Point(tempW1,  (int16_t)U8_AVH_TC_DROP_MIN_REF,    S16_AVH_TP_DROP_MIN,
            	                                    (int16_t)U8_AVH_TC_DROP_MID_REF,    S16_AVH_TP_DROP_MID,
                	                                (int16_t)U8_AVH_TC_DROP_MAX_REF,    S16_AVH_TP_DROP_MAX );
		}
		else
		{
			tempW2 = 0;
		}
		
		lcs16AvhTargetPress = lcs16AvhTargetPress - tempW2;
		
		if(lcs16AvhTargetPress < MPRESS_0BAR)
		{
			lcs16AvhTargetPress = MPRESS_0BAR;
		}
		else{ }
	}
	else
	{
		lcs16AvhTargetPress = MPRESS_0BAR;
	}
	
	laavhs16TcTargetPress = lcs16AvhTargetPress;
}

static void	LCAVH_vCheckApplyCondition(void)
{
	lcs16AvhDefficientPress = lcs16AvhSafeHoldPress-lcs16AvhRetainingPress;
 
    if((lcs16AvhDefficientPress>MPRESS_0BAR)&&(lcu1AvhVehicleRoll2==1))
    {
        lcu1AvhRiseInit=1;
        lcu1AvhRiseRoll=0;
        lcu8AvhRiseInitOffCnt=0;
    }
    else if((lcu1AvhVehicleRoll2==1)&&(lcu16AvhRollStateCnt<(uint16_t)U8_ROLL_RISE_TIME))
    {
    	if((lcu1AvhRiseInit==1)&&(lcu8AvhRiseInitOffCnt<L_U8_TIME_10MSLOOP_1000MS))
    	{
    		lcu8AvhRiseInitOffCnt++;
    	}
    	else
    	{
    		lcu8AvhRiseInitOffCnt=0;
    		lcu1AvhRiseInit=0;
    	    lcu1AvhRiseRoll=1;
    	}
    }
    else
    {
    	lcu8AvhRiseInitOffCnt=0;
        lcu1AvhRiseInit=0;
        lcu1AvhRiseRoll=0;
    }

	if(lcu1AvhRiseModeEnd==0)
	{
	  #if (__BLS_NO_WLAMP_FM==ENABLE)
	    if((lcu1AvhMpressOn==0)&&(mtp<3))
	  #else
 	    if((lcs16AvhMpress<lcs16AvhMinEntPres)&&(mtp<3))  
 	  #endif
	    {
	    	if((lcu1AvhRiseInit==1)||(lcu1AvhRiseRoll==1))
	    	{
	    		lcu1AvhApplySatisfy = 1;
	    	}
	    	else 
	    	{
	    		lcu1AvhApplySatisfy = 0;
	    	}
	    }
	    else 
	    {
	    	lcu1AvhApplySatisfy = 0;
	    }
	}
	else /*lcu1AvhRiseModeEnd==1*/
	{
		if(lcu1AvhRiseRoll==1)
		{
			lcu1AvhRiseModeEnd=0;
			lcu1AvhApplySatisfy=1;
		}
		else
		{
		    lcu1AvhApplySatisfy = 0;
		}
	}
	
	/*
    if((lcu8AvhReqEpbAct==AVH_EPB_ACT_REQUEST)||(lcu8AvhEpbState==EPB_CLAMPED))
    {
    	lcu1AvhApplySatisfy = 0;
    }
    else { }	
    */
}

static void	LCAVH_vDetectVehicleRoll(void)
{
	  #if __SENSITIVE_ROLL_DETECT
    int16_t s16RollThresForRise;
      #endif

	  #if __USE_MTP_RESOL_1000
	if((lcs8AvhControlState==AVH_HOLD)&&(mtp_resol1000<S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcu1AvhMpressOn==0))	
	  #else
	if((lcs8AvhControlState==AVH_HOLD)&&(mtp<1)&&(lcu1AvhMpressOn==0))	
	  #endif
	{
	    lcs16AvhTemp0 = (int16_t)((((int32_t)fsu8NumberOfEdge_fl*(int32_t)U16_TIRE_L*1000)/(U8_TEETH*2))/1000);
	    if(lcs16AvhTemp0>0)
	    {
	    	if(lcs16AvhMoveDistance_fl > 5000)
	    	{
	    		;
	    	}
	    	else
	    	{
	            lcs16AvhMoveDistance_fl = lcs16AvhMoveDistance_fl + lcs16AvhTemp0;
	        }
	        lcs16AvhCompleteStopCnt_fl = 0;
	    }
	    else
	    {
            if(lcs16AvhCompleteStopCnt_fl<S16_AVH_MOVING_DETECT_CYCLE)
	    	{
	    		lcs16AvhCompleteStopCnt_fl = lcs16AvhCompleteStopCnt_fl + 1;
	    	}
	    	else
	    	{
	    		lcs16AvhMoveDistance_fl = 0;
	    	}
	    }
	    
	    lcs16AvhTemp0 = (int16_t)((((int32_t)fsu8NumberOfEdge_fr*(int32_t)U16_TIRE_L*1000)/(U8_TEETH*2))/1000);
	    if(lcs16AvhTemp0>0)
	    {
	    	if(lcs16AvhMoveDistance_fr > 5000)
	    	{
	    		;
	    	}
	    	else
	    	{
	            lcs16AvhMoveDistance_fr = lcs16AvhMoveDistance_fr + lcs16AvhTemp0;
	        }
	        lcs16AvhCompleteStopCnt_fr = 0;
	    }
	    else
	    {
            if(lcs16AvhCompleteStopCnt_fr<S16_AVH_MOVING_DETECT_CYCLE)
	    	{
	    		lcs16AvhCompleteStopCnt_fr = lcs16AvhCompleteStopCnt_fr + 1;
	    	}
	    	else
	    	{
	    		lcs16AvhMoveDistance_fr = 0;
	    	}
	    }	    
			
	    lcs16AvhTemp0 = (int16_t)((((int32_t)fsu8NumberOfEdge_rl*(int32_t)U16_TIRE_L_R*1000)/(U8_TEETH_R*2))/1000);
	    if(lcs16AvhTemp0>0)
	    {
	    	if(lcs16AvhMoveDistance_rl > 5000)
	    	{
	    		;
	    	}
	    	else
	    	{
	            lcs16AvhMoveDistance_rl = lcs16AvhMoveDistance_rl + lcs16AvhTemp0;
	        }
	        lcs16AvhCompleteStopCnt_rl = 0;
	    }
	    else
	    {
            if(lcs16AvhCompleteStopCnt_rl<S16_AVH_MOVING_DETECT_CYCLE)
	    	{
	    		lcs16AvhCompleteStopCnt_rl = lcs16AvhCompleteStopCnt_rl + 1;
	    	}
	    	else
	    	{
	    		lcs16AvhMoveDistance_rl = 0;
	    	}
	    }

	    lcs16AvhTemp0 = (int16_t)((((int32_t)fsu8NumberOfEdge_rr*(int32_t)U16_TIRE_L_R*1000)/(U8_TEETH_R*2))/1000);
	    if(lcs16AvhTemp0>0)
	    {
	    	if(lcs16AvhMoveDistance_rr > 5000)
	    	{
	    		;
	    	}
	    	else
	    	{
	            lcs16AvhMoveDistance_rr = lcs16AvhMoveDistance_rr + lcs16AvhTemp0;
	        }
	        lcs16AvhCompleteStopCnt_rr = 0;
	    }
	    else
	    {
            if(lcs16AvhCompleteStopCnt_rr<S16_AVH_MOVING_DETECT_CYCLE)
	    	{
	    		lcs16AvhCompleteStopCnt_rr = lcs16AvhCompleteStopCnt_rr + 1;
	    	}
	    	else
	    	{
	    		lcs16AvhMoveDistance_rr = 0;
	    	}
	    }
	}
	else
	{
		lcs16AvhMoveDistance_fl = 0;
		lcs16AvhCompleteStopCnt_fl = 0;		
		lcs16AvhMoveDistance_fr = 0;
		lcs16AvhCompleteStopCnt_fr = 0;		
		lcs16AvhMoveDistance_rl = 0;
		lcs16AvhCompleteStopCnt_rl = 0;
		lcs16AvhMoveDistance_rr = 0;
		lcs16AvhCompleteStopCnt_rr = 0;
	}

	if(lcu1AvhVehicleRoll2==0)
	{
	  #if __SENSITIVE_ROLL_DETECT
	   if((lcu1AvhAutonomousActFlg==1)&&(lcs16AvhTargetPress<lcs16AvhSafeHoldPress))
	    {
	    	s16RollThresForRise = SENSITIVE_ROLL_TH;
	    }
	    else
	    {
	    	s16RollThresForRise = (int16_t)U8_ROLL_TH_FOR_RISE;
	    }
	    
	    if((lcu1AvhAutonomousActFlg==1)&&(lcs16AvhTargetPress<lcs16AvhSafeHoldPress))
	    {
	    	  #if (__SMART_WHEEL_SENSOR==ENABLE)
	    	if((lcs16AvhMoveDistance_fl >= s16RollThresForRise)||(lcs16AvhMoveDistance_fr >= s16RollThresForRise))  
              #else
	    	if(((lcs16AvhMoveDistance_fl >= s16RollThresForRise)&&(lcs16AvhMoveDistance_rr >= s16RollThresForRise))
	    	||((lcs16AvhMoveDistance_fr >= s16RollThresForRise)&&(lcs16AvhMoveDistance_rl >= s16RollThresForRise))
	    	)
	    	  #endif
	    	{
	    		lcu1AvhVehicleRoll2=1;
       	        lcu16AvhRollStateCnt++;
       	    }
       	    else
            {
            	lcu16AvhRollStateCnt = 0;
            }
       	}
       	else
       	{
		    if((lcs16AvhMoveDistance_fl >= s16RollThresForRise)
            &&(lcs16AvhMoveDistance_fr >= s16RollThresForRise)
              #if (__SMART_WHEEL_SENSOR==ENABLE)
              #else
            &&(lcs16AvhMoveDistance_rl >= s16RollThresForRise)
            &&(lcs16AvhMoveDistance_rr >= s16RollThresForRise)
              #endif
            )
            {
            	lcu1AvhVehicleRoll2=1;
       	        lcu16AvhRollStateCnt++;
            }
            else
            {
            	lcu16AvhRollStateCnt = 0;
            }
        }
	  #else
        if((lcs16AvhMoveDistance_fl >= (int16_t)U8_ROLL_TH_FOR_RISE)
        &&(lcs16AvhMoveDistance_fr >= (int16_t)U8_ROLL_TH_FOR_RISE)
          #if (__SMART_WHEEL_SENSOR==ENABLE)
          #else
        &&(lcs16AvhMoveDistance_rl >= (int16_t)U8_ROLL_TH_FOR_RISE)
        &&(lcs16AvhMoveDistance_rr >= (int16_t)U8_ROLL_TH_FOR_RISE)
          #endif
        )
        {
        	lcu1AvhVehicleRoll2=1;
       	    lcu16AvhRollStateCnt++;
        }
        else
        {
        	lcu16AvhRollStateCnt = 0;
        }
      #endif
	}
	else 
	{
		if((lcs16AvhMoveDistance_fl+lcs16AvhMoveDistance_fr+lcs16AvhMoveDistance_rl+lcs16AvhMoveDistance_rr) < 10)
		{
			lcu1AvhVehicleRoll2=0;
			lcu16AvhRollStateCnt = 0;
		}
		else
		{
			if(lcu16AvhRollStateCnt<((uint16_t)U8_ROLL_RISE_TIME+L_U16_TIME_10MSLOOP_3S))
			{
        	    lcu16AvhRollStateCnt++;
        	}
      	    else
      	    {
      	    	lcu16AvhRollStateCnt = 0;
      	    }
		}
	}
	
	if(lcu1AvhVehicleRoll==0)
	{
        if((lcs16AvhMoveDistance_fl >= (int16_t)U8_ROLL_TH_FOR_EPB_ACT)
        &&(lcs16AvhMoveDistance_fr >= (int16_t)U8_ROLL_TH_FOR_EPB_ACT)
          #if (__SMART_WHEEL_SENSOR==ENABLE)
          #else
        &&(lcs16AvhMoveDistance_rl >= (int16_t)U8_ROLL_TH_FOR_EPB_ACT)
        &&(lcs16AvhMoveDistance_rr >= (int16_t)U8_ROLL_TH_FOR_EPB_ACT)
          #endif        
        )
        {
        	lcu1AvhVehicleRoll=1;
        }
        else
        {
        	;
        }
	}
	else 
	{
		if((lcs16AvhMoveDistance_fl+lcs16AvhMoveDistance_fr+lcs16AvhMoveDistance_rl+lcs16AvhMoveDistance_rr) < 10)
		{
			lcu1AvhVehicleRoll=0;
		}
		else
		{
			;
		}
	}	
}

static void	LCAVH_vCheckAccelIntend(void)
{

    /******************************* check vehicle down driving *************************************************/
	if (lcu1AvhMpressOn==1)
	{
		lcu8AvhDownDrvLevel = 0;
		lcu1AvhDownDrive = 0;
	}
	else if (lcu1AvhActFlg==1)
	{	
	    if(((lcu1AvhUphill==1)&&(lcu8AvhGearPosition==AVH_REVERSE))||((lcu1AvhDownhill==1)&&(lcu8AvhGearPosition==AVH_FORWARD)))
	    {
	    	if((mtp>=10)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH1_LV3)))
	    	{
	    		lcu8AvhDownDrvLevel = U8_AVH_DW_DRV_FO_RATE_LV3_M1;
    			lcu1AvhDownDrive = 1;
	    	}	    	
	    	else if((mtp>=5)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH1_LV2)))
	    	{
	    		lcu8AvhDownDrvLevel = U8_AVH_DW_DRV_FO_RATE_LV2_M1;
    			lcu1AvhDownDrive = 1;
	    	}
	    	  #if __USE_MTP_RESOL_1000
	    	else if((mtp_resol1000>S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH1)))
	    	  #else
	    	else if((mtp>0)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH1)))
	    	  #endif	    	
	    	{
	    		lcu8AvhDownDrvLevel = 1;
    			lcu1AvhDownDrive = 1;
	    	}
	    	else{ }
	    }
	    else{ }
	}
	else if (lcu1AvhAutonomousActFlg==1)
	{
	    if(((lcu1AvhUphill==1)&&(lcu8AvhGearPosition==AVH_REVERSE))||((lcu1AvhDownhill==1)&&(lcu8AvhGearPosition==AVH_FORWARD)))
	    {
	    	if((mtp>=10)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2_LV3)))
	    	{
	    		lcu8AvhDownDrvLevel = U8_AVH_DW_DRV_FO_RATE_LV3_M2;
    			lcu1AvhDownDrive = 1;
	    	}	    	
	    	else if((mtp>=5)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2_LV2)))
	    	{
	    		lcu8AvhDownDrvLevel = U8_AVH_DW_DRV_FO_RATE_LV2_M2;
    			lcu1AvhDownDrive = 1;
	    	}
              #if __USE_MTP_RESOL_1000            
                #if (__SCC==1)          
            else if (((lcu1AvhActReq==0)&&(lcu1SccEpbReqAVH==0))||((mtp_resol1000>S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2))))	    	
                #else
            else if ((lcu1AvhActReq==0)||((mtp_resol1000>S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2))))	    	
                #endif
              #else
                #if (__SCC==1)
            else if (((lcu1AvhActReq==0)&&(lcu1SccEpbReqAVH==0))||((mtp>0)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2))))	    	
              #else
            else if ((lcu1AvhActReq==0)||((mtp>0)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_DW_DRV_FO_TQ_TH2))))	    	
              #endif	    	
              #endif	    	
	    	{
	    		lcu8AvhDownDrvLevel = 1;
    			lcu1AvhDownDrive = 1;
	    	}
	    	else{ }
	    }
	    else { }
	}
	else { }


    /**************************************** check accel start *************************************************/
	if (lcu1AvhMpressOn==1)
	{
		lcu1AvhAccelExitOK = 0;
		lcu8AvhAceelIntendCnt = 0;
		lcu8AvhPrateLevel = 0;
	}
	else if (lcu1AvhActFlg==1)
	{
		if (lcu1AvhAutoTM==AVH_AUTO_TM)
		{
		    lcs16AvhTemp0 = LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP,      S16_AVH_FO_TQ_TH1_MIN,
                                                                  (int16_t)U8_AVH_LOW_MID_SLOP,  S16_AVH_FO_TQ_TH1_LMID,
                                                                  (int16_t)U8_AVH_HIGH_MID_SLOP, S16_AVH_FO_TQ_TH1_HMID,
                                                                  (int16_t)U8_AVH_MAX_SLOP,      S16_AVH_FO_TQ_TH1_MAX );
        }
        else
        {
	        lcs16AvhTemp0 = LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP,      S16_AVH_FO_TQ_TH1_MIN_MT,
                                                                  (int16_t)U8_AVH_LOW_MID_SLOP,  S16_AVH_FO_TQ_TH1_LMID_MT,
                                                                  (int16_t)U8_AVH_HIGH_MID_SLOP, S16_AVH_FO_TQ_TH1_HMID_MT,
                                                                  (int16_t)U8_AVH_MAX_SLOP,      S16_AVH_FO_TQ_TH1_MAX_MT );
        }                                                              	
		
		  #if __USE_MTP_RESOL_1000  
		if ((mtp_resol1000>S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+lcs16AvhTemp0))&&(lcu8AvhGearPosition!=AVH_NEUTRAL))
			#else
		if ((mtp>0)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+lcs16AvhTemp0))&&(lcu8AvhGearPosition!=AVH_NEUTRAL))
			#endif		
	    {
	    	if (lcu1AvhAutoTM==AVH_AUTO_TM)
	    	{
	            lcu8AvhTemp0 = (uint8_t)LCESP_s16IInter4Point(lcs16AvhAxGAbs,   (int16_t)U8_AVH_MIN_SLOP,      (int16_t)U8_AVH_T_DELAY_TH1_MIN,
                                                                                (int16_t)U8_AVH_LOW_MID_SLOP,  (int16_t)U8_AVH_T_DELAY_TH1_LMID,
                                                                                (int16_t)U8_AVH_HIGH_MID_SLOP, (int16_t)U8_AVH_T_DELAY_TH1_HMID,
                                                                                (int16_t)U8_AVH_MAX_SLOP,      (int16_t)U8_AVH_T_DELAY_TH1_MAX );	
            }
            else
            {
	            lcu8AvhTemp0 = (uint8_t)LCESP_s16IInter4Point(lcs16AvhAxGAbs, (int16_t)U8_AVH_MIN_SLOP,      (int16_t)U8_AVH_T_DELAY_TH1_MIN_MT,
                                                                              (int16_t)U8_AVH_LOW_MID_SLOP,  (int16_t)U8_AVH_T_DELAY_TH1_LMID_MT,
                                                                              (int16_t)U8_AVH_HIGH_MID_SLOP, (int16_t)U8_AVH_T_DELAY_TH1_HMID_MT,
                                                                              (int16_t)U8_AVH_MAX_SLOP,      (int16_t)U8_AVH_T_DELAY_TH1_MAX_MT );	
            }                                                                          

	    	if(lcu8AvhAceelIntendCnt>lcu8AvhTemp0) 
	    	{
	    		lcu1AvhAccelExitOK = 1;
	    	}
	    	else 
	    	{
	    		lcu8AvhAceelIntendCnt = lcu8AvhAceelIntendCnt + 1;
	    	}
	    	
	    	if (lcu1AvhAccelExitOK==1)
	    	{
	    	    if (lcu1AvhAutoTM==AVH_AUTO_TM)	
	    	    {
	    	        if((mtp>=20)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV3_TQ_TH)))
	    	        {
	    	        	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV3;
	    	        }	    	
	    	        else if((mtp>=10)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV2_TQ_TH)))
	    	        {
	    	        	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV2;
	    	        }
	    	        else 
	    	        {
	    	        	lcu8AvhPrateLevel = 1;
	    	        }
	    	    }
	    	    else
	    	    {
	    	    	if((mtp>=20)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV3_TQ_TH_MT)))
	    	        {
	    	        	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV3_MT;
	    	        }	    	
	    	        else if((mtp>=10)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV2_TQ_TH_MT)))
	    	        {
	    	        	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV2_MT;
	    	        }
	    	        else 
	    	        {
	    	        	lcu8AvhPrateLevel = 1;
	    	        }
	    	    }
	    	}
	    	else
	    	{
	    		;
	    	}
	    }
	    else 
	    {
            ;
	    }
	}
	else if (lcu1AvhAutonomousActFlg==1)
	{
		lcs16AvhTemp0 = LCESP_s16IInter4Point(lcs16AvhAxGAbs,          (int16_t)U8_AVH_MIN_SLOP,      S16_AVH_FO_TQ_TH2_MIN,
                                                                       (int16_t)U8_AVH_LOW_MID_SLOP,  S16_AVH_FO_TQ_TH2_LMID,
                                                                       (int16_t)U8_AVH_HIGH_MID_SLOP, S16_AVH_FO_TQ_TH2_HMID,
                                                                       (int16_t)U8_AVH_MAX_SLOP,      S16_AVH_FO_TQ_TH2_MAX );	
        #if __USE_MTP_RESOL_1000
	    if ((lcu1AvhActReq==0)&&(mtp_resol1000>S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+lcs16AvhTemp0))&&(lcu8AvhGearPosition!=AVH_NEUTRAL))	    
	    	#else
	    if ((lcu1AvhActReq==0)&&(mtp>0)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+lcs16AvhTemp0))&&(lcu8AvhGearPosition!=AVH_NEUTRAL))
	    	#endif
	    {
	        lcu8AvhTemp0 = (uint8_t)LCESP_s16IInter4Point(lcs16AvhAxGAbs,   (int16_t)U8_AVH_MIN_SLOP,      (int16_t)U8_AVH_T_DELAY_TH2_MIN,
                                                                            (int16_t)U8_AVH_LOW_MID_SLOP,  (int16_t)U8_AVH_T_DELAY_TH2_LMID,
                                                                            (int16_t)U8_AVH_HIGH_MID_SLOP, (int16_t)U8_AVH_T_DELAY_TH2_HMID,
                                                                            (int16_t)U8_AVH_MAX_SLOP,      (int16_t)U8_AVH_T_DELAY_TH2_MAX );

	    	if (lcu8AvhAceelIntendCnt>lcu8AvhTemp0)
	    	{
	    		lcu1AvhAccelExitOK = 1;
	    	}
	    	else
	    	{
	    		lcu8AvhAceelIntendCnt = lcu8AvhAceelIntendCnt + 1;
	    	}
	    	
	    	if (lcu1AvhAccelExitOK==1)
	    	{
	    	    if((mtp>=20)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV3_TQ_TH)))
	    	    {
	    	    	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV3;
	    	    }	    	
	    	    else if((mtp>=10)&&(lcs16AvhFilteredTorq>(lcs16AvhIdleTorq+S16_AVH_UP_DRV_FO_LV2_TQ_TH)))
	    	    {
	    	    	lcu8AvhPrateLevel = U8_AVH_UP_DRV_FO_LV2;
	    	    }
	    	    else 
	    	    {
	    	    	lcu8AvhPrateLevel = 1;
	    	    }
	    	}
	    	else
	    	{
	    		;
	    	}	    	
	    }
	    else 
	    {
            ;
	    }
	}
	else { }
}

static void	LCAVH_vChcekReBrkRelease(void)
{
	  #if __AVH_NOISE_REDUCTION == ENABLE
	if((lcs16AvhMpress>=(lcs16AvhTargetPress+S16_AVH_REBRAKE_DCT_THRESHOLD))&&(lcu8AvhReBrkReleaseCnt>(uint8_t)(U8_AVH_CUR_INIT_UP_TIME+U8_AVH_CUR_INIT_DOWN_TIME)))
	  #else
	if(lcs16AvhMpress>=(lcs16AvhSafeHoldPress+MPRESS_3BAR))
      #endif

	{
		if(lcu8AvhReBrkCnt<L_U8_TIME_10MSLOOP_1500MS) {lcu8AvhReBrkCnt = lcu8AvhReBrkCnt + 1;}
		else {lcu8AvhReBrkCnt = L_U8_TIME_10MSLOOP_1500MS;}
	}
	else
	{
		if(lcu8AvhReBrkCnt>1)      {lcu8AvhReBrkCnt = lcu8AvhReBrkCnt - 2;}
		else {lcu8AvhReBrkCnt = 0;}
	}
	
	if(lcu1AvhReBrk==0)
	{
	    lcu1AvhReBrkRelease=0;
		if(lcu8AvhReBrkCnt>U8_AVH_REBRAKE_DCT_TIME) 
		{
			lcu1AvhReBrk = 1;
			lcu8AvhReBrkReleaseCnt = 0;
		}
	    else 
	    {
	    	if(lcu8AvhReBrkReleaseCnt<L_U8_TIME_10MSLOOP_1500MS) { lcu8AvhReBrkReleaseCnt++ ; }
	    else { }
	}
	}
	else /*lcu1AvhReBrk==1*/
	{
		  #if __AVH_NOISE_REDUCTION == ENABLE
        if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+lcs16AvhValveActMp))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		  ||(lcs16AvhMpress<=(lcs16AvhTargetPress+MPRESS_1BAR))
		)          
          #else
		if(((lcs16AvhMpress<=(lcs16AvhSafeHoldPress+MPRESS_1BAR))&&((MCpress[0]-MCpress[2])<lcs16AvhMpressSlopThres))
		  ||(lcs16AvhMpress<=lcs16AvhSafeHoldPress)
		)
		  #endif
	    {
	    	lcu1AvhReBrkRelease=1;
	    	lcu1AvhReBrk=0;
	    	lcu8AvhReBrkCnt=0;
	    	lcu8AvhReBrkReleaseCnt++ ;
	    }
	    else { }
	}
}
 
static void	LCAVH_vReleaseMode(void)
{
	lcu8AvhRiseEndCnt=0;
	LCAVH_vCalcTargetPress();

    AVH_TCL_DEMAND_fl = 1;
    AVH_TCL_DEMAND_fr = 1;
    AVH_S_VALVE_LEFT=0;
    AVH_S_VALVE_RIGHT=0;
    AVH_MSC_MOTOR_ON=0;

  #if( __AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
	if(laavhs16TcTargetPress<=MPRESS_0BAR)
  #else
	if((MFC_Current_AVH_P<(AVH_TCMF_CTRL_MIN_CURRENT+50))||(MFC_Current_AVH_S<(AVH_TCMF_CTRL_MIN_CURRENT+50)))
  #endif    
    {
    	AVH_TCL_DEMAND_fl = 0;
        AVH_TCL_DEMAND_fr = 0;
    }
    else { }

  #if (__AVH_ACT_1IGN_DECOUNT==DISABLE)
	LCAVH_vAvhOnCount();
  #endif
	if (lcs16AvhReleaseTime<L_U16_TIME_10MSLOOP_10S)	{lcs16AvhReleaseTime = lcs16AvhReleaseTime+1;}
	else { }
}

static void	LCAVH_vHoldMode(void)
{
	LCAVH_vCalcTargetPress();
		
	AVH_TCL_DEMAND_fl=1;
	AVH_TCL_DEMAND_fr=1;

  #if (__AVH_ACT_1IGN_DECOUNT==DISABLE)
	LCAVH_vAvhOnCount();
  #endif
	
    if(lcu1AvhRiseModeEnd==1)
	{
		if(lcu8AvhRiseEndCnt>0) {lcu8AvhRiseEndCnt = lcu8AvhRiseEndCnt - 1;}
		else                    {lcu8AvhRiseEndCnt = 0;}
	}
	else { }	
	
	if(lcu8AvhInitCurCnt<L_U8_TIME_10MSLOOP_1500MS)
	{
		lcu8AvhInitCurCnt++;
	}
	else
	{
		lcu8AvhInitCurCnt=L_U8_TIME_10MSLOOP_1500MS;
	}
	
	if(lcu8AvhInitCurDownTime>0)
    {
    	lcu8AvhInitCurDownTime = lcu8AvhInitCurDownTime - 1;
    }
    else
    {
    	;
    }
}

static void	LCAVH_vApplyMode(void)
{
	LCAVH_vCalcTargetPress();
	
	AVH_TCL_DEMAND_fl=1;
	AVH_TCL_DEMAND_fr=1;

	if(lcs16AvhDefficientPress<=MPRESS_0BAR)
	{
    	lcs16AvhApplyTime = (int16_t)U8_ROLL_RISE_TIME;		
	}
	else
	{
		lcs16AvhApplyTime = LCESP_s16IInter5Point(lcs16AvhDefficientPress, S16_AVH_MOT_ON_TIME_1ST_REF_P,  (int16_t)U8_AVH_MOT_ON_TIME_1ST, 
						  							    				   S16_AVH_MOT_ON_TIME_2ND_REF_P,  (int16_t)U8_AVH_MOT_ON_TIME_2ND, 
													   					   S16_AVH_MOT_ON_TIME_3RD_REF_P,  (int16_t)U8_AVH_MOT_ON_TIME_3RD,  
													   					   S16_AVH_MOT_ON_TIME_4TH_REF_P,  (int16_t)U8_AVH_MOT_ON_TIME_4TH,  
													   					   S16_AVH_MOT_ON_TIME_5TH_REF_P,  (int16_t)U8_AVH_MOT_ON_TIME_5TH); 	
	}

	if(lcs16AvhApplyCnt<=lcs16AvhApplyTime)
	{
		if(lcs16AvhApplyCnt<=L_U16_TIME_10MSLOOP_5S)
		{
			lcs16AvhApplyCnt = lcs16AvhApplyCnt + 1;
    	}
    	else { }
    	lcu1AvhRiseModeEnd=0;
		AVH_S_VALVE_LEFT=1;
    	AVH_S_VALVE_RIGHT=1;
    	AVH_MSC_MOTOR_ON=1;
    }
    else
    {
    	lcu1AvhRiseModeEnd=1;
    	lcs16AvhApplyCnt=0;
		AVH_S_VALVE_LEFT=0;
    	AVH_S_VALVE_RIGHT=0;
    	AVH_MSC_MOTOR_ON=0;
    	lcu8AvhRiseEndCnt = U8_AVH_CUR_DOWN_TIME_AFTER_RISE;
    	if(lcu1AvhRiseInit==0)
    	{
    		lcu8AvhTotalRiseAtempt++;
    	}
    	else { }
    }

  #if (__AVH_ACT_1IGN_DECOUNT==DISABLE)
	LCAVH_vAvhOnCount();
  #endif
}

static void	LCAVH_vResetCtrlVariable(void)
{
	lcu8AvhTotalRiseAtempt=0;
	lcu1AvhRiseInit=0;
	lcs16AvhHoldCnt = 0;
	lcs16AvhHold1SecCnt = 0;
	lcu1AvhActFlg = 0;
	lcu1AvhAutonomousActFlg = 0;
	lcu1AvhExitSatisfy = 0;
	lcu1AvhEnterSatisfy = 0;
	lcu1AvhEnterSatisfy2 = 0;
	/*lcs16AvhEnterPress = 0;*/
	lcs16AvhRetainingPress = 0;
	lcu8AvhReqEpbAct = AVH_EPB_NO_REQUEST;
	lcs16AvhApplyTime = 0;

	lcs16AvhApplyCnt=0;
	lcu1AvhRiseModeEnd=0;
	lcs16AvhDefficientPress = 0;
	lcu1AvhMedExitSatisfy =0;
	lcu1AvhAccelExitOK=0;
	lcu1AvhSlowExitSatisfy=0;

	lcs16AvhTargetPress=0;
	laavhs16TcTargetPress=0;
	lcs16AvhTpApplyInit=0;
	lcu1AvhFastExitSatisfy=0;
	lcs16AvhReleaseTime=0;
	lcu1AvhUphill=0;
	lcu1AvhDownhill=0;
	lcu1AvhDownDrive=0;
	lcu8AvhDownDrvLevel=0;
	lcu8AvhPrateLevel=0;
	lcu1AvhActReady=0;
	lcs16Avhdropcurrent=0;
	lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_OFF;
	lcu8AvhCluMsg = AVH_ALARM_CLU_MSG_OFF;
	lcu8AvhRiseEndCnt = 0;
	lcu1AvhComCndSatisfy = 0;
	lcu1AvhReBrkRelease =0;
	lcu8AvhReBrkCnt=0;
	lcu1AvhReBrk=0;
	lcu1AvhVehicleRoll=0;
	lcu1AvhVehicleRoll2=0;
	lcu8AvhInitCurCnt=0;
	lcu8AvhEntSpdOk=0;
    lcs16AvhAbnormalHoldCnt=0;
    lcs16AvhMoveDistance_fl=0;
    lcs16AvhMoveDistance_fr=0;
    lcs16AvhMoveDistance_rl=0;
    lcs16AvhMoveDistance_rr=0;
    lcs16AvhCompleteStopCnt_fl=0;
    lcs16AvhCompleteStopCnt_fr=0;
    lcs16AvhCompleteStopCnt_rl=0;
    lcs16AvhCompleteStopCnt_rr=0;
    lcu8AvhAceelIntendCnt=0;
    lcu8AvhReBrkReleaseCnt=0;

	AVH_MSC_MOTOR_ON = 0;
	AVH_TCL_DEMAND_fl = 0;
	AVH_TCL_DEMAND_fr = 0;
	AVH_S_VALVE_LEFT = 0;
	AVH_S_VALVE_RIGHT = 0;

	/*New Reset Variable 20120206 */

    lcu1AvhEpbActReq = 0;
    lcu1AvhAlarmReq = 0;
    lcu1AvhApplySatisfy = 0;
    lcu1AvhRiseRoll = 0;
    lcu8AvhRiseInitOffCnt = 0;
    lcu16AvhRollStateCnt = 0;

}

  #if (__AVH_ACT_1IGN_DECOUNT==ENABLE)
static void	LCAVH_vAvhOnCount(void)
{
	if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1))
	{
		lcs16AvhActDeCountCycle = 0;
		if(lcs16AvhHold1SecCnt<L_U8_TIME_10MSLOOP_1000MS)
		{
			lcs16AvhHold1SecCnt++;
		}
		else
		{
			lcs16AvhHold1SecCnt=0;
    	    if(lcs16AvhHoldCnt<S16_MAX_ON_TIME_ONE_IGN)
    	    {
    			lcs16AvhHoldCnt = lcs16AvhHoldCnt + 1;
    			if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
    			{
    			    lcs16AvhHoldCntFor1IGN = lcs16AvhHoldCntFor1IGN + 1;
    			}
                else if((lcu1AvhAutonomousActFlg==1)&&(lcs16AvhHoldCntFor1IGN>0))
				{
					lcs16AvhHoldCntFor1IGN = lcs16AvhHoldCntFor1IGN - 1;
				}
    			else
    			{
    				;
    			}
    	
    			if (lcs16AvhHoldCntFor1IGN>S16_MAX_ON_TIME_ONE_IGN)
    			{
    				lcs16AvhHoldCntFor1IGN = S16_MAX_ON_TIME_ONE_IGN;
    			}
				else if (lcs16AvhHoldCntFor1IGN < 0)
    			{
    				lcs16AvhHoldCntFor1IGN = 0;
    			}
    			else
    			{
    				;
    			}
    	    }
    	    else { }
    	}
    }
    else /* At AVH off, lcs16AvhHoldCntFor1IGN de-count */
    {
    	lcs16AvhHold1SecCnt = 0;
    	if(lcs16AvhActDeCountCycle<S16_AVH_IGN_DECOUNT_CYCLE)
		{
			lcs16AvhActDeCountCycle++;
		}
		else
		{
			lcs16AvhActDeCountCycle=0;
    	    if(lcs16AvhHoldCntFor1IGN>0)
    	    {
    			lcs16AvhHoldCntFor1IGN = lcs16AvhHoldCntFor1IGN - 1;
    	    }
    	    else 
    	    {
    	    	lcs16AvhHoldCntFor1IGN = 0;
			}
    	}
    }
}
  #else
static void	LCAVH_vAvhOnCount(void)
{
	if(lcs16AvhHold1SecCnt<L_U8_TIME_10MSLOOP_1000MS)
	{
		lcs16AvhHold1SecCnt++;
	}
	else
	{
		lcs16AvhHold1SecCnt=0;
        if(lcs16AvhHoldCnt<=1800)
        {
    		lcs16AvhHoldCnt = lcs16AvhHoldCnt + 1;
    		if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
    		{
    		    lcs16AvhHoldCntFor1IGN = lcs16AvhHoldCntFor1IGN + 1;
    		}
    		else
    		{
    			;
    		}

    		if (lcs16AvhHoldCntFor1IGN>20000)
    		{
    			lcs16AvhHoldCntFor1IGN = 20000;
    		}
    		else
    		{
    			;
    		}
        }
        else { }
    }
}
  #endif

static void	LCAVH_vChkAvhOverActTime(void)
{
    if(lcs16AvhHoldCntFor1IGN >= S16_MAX_ON_TIME_ONE_IGN)
    {
    	if(lcu1AvhOverActTime==1)
    	{
    		;
    	}
    	else
    	{
    	    if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
    	    {
    		    lcu1AvhOverActTime=0;
    	    }
    	    else
    	    {
    		    lcu1AvhOverActTime=1;
    	    }
        }
    }
    else
    {
      #if (__AVH_ACT_1IGN_DECOUNT==ENABLE)
        if(lcu1AvhOverActTime==1)
        {
        	if(lcs16AvhHoldCntFor1IGN > S16_AVH_RECOVERY_IGN_CNT_REF)
        	{
        		lcu1AvhOverActTime = 1;
        	}
        	else
        	{
        		lcu1AvhOverActTime = 0;
        	}
        }
        else
        {
        	lcu1AvhOverActTime = 0;
        }
      #else
    	lcu1AvhOverActTime=0;
      #endif
    }
}

static void	LCAVH_vChkAvhLimitAutoMode(void)
{
	if(lcu8AvhAutoAct1SecCnt<L_U8_TIME_10MSLOOP_1000MS)
	{
		lcu8AvhAutoAct1SecCnt++;
	}
	else
	{
		lcu8AvhAutoAct1SecCnt=0;
	}	
	
	if(lcu1AvhAutonomousActFlg==1)
	{
		if(lcu1AvhAutoOverActTime==1)
		{
			lcu16AvhAutoActTotal = (uint16_t)S16_MAX_ON_TIME_ONE_IGN_AUTO;
		}
        else if((lcu8AvhAutoAct1SecCnt==L_U8_TIME_10MSLOOP_1000MS)&&(lcu16AvhAutoActTotal<(uint16_t)S16_MAX_ON_TIME_ONE_IGN_AUTO))
        {
        	lcu16AvhAutoActTotal = lcu16AvhAutoActTotal + 1;
        }
        else
        {
        	if(lcu16AvhAutoActTotal>=(uint16_t)S16_MAX_ON_TIME_ONE_IGN_AUTO)
            {
    	        lcu1AvhAutoOverActTime = 1;
            }
            else
            {
            	;
            }
        }
    }
    else
    {
    	if((lcu1AvhAutoOverActTime==1)&&(lcu16AvhAutoActTotal<((uint16_t)S16_MAX_ON_TIME_ONE_IGN_AUTO-(uint16_t)S16_AUTO_PAUSE_TIME)))
    	{
    		lcu16AvhAutoActTotal = 0;
    		lcu1AvhAutoOverActTime = 0;
    	}
    	else if((lcu8AvhAutoAct1SecCnt==L_U8_TIME_10MSLOOP_1000MS)&&(lcu16AvhAutoActTotal>0))
    	{
    		lcu16AvhAutoActTotal = lcu16AvhAutoActTotal - 1;
    	}
    	else
    	{
    		;
    	}
    }
}

static void	LCAVH_vCalcIdleEngTorque(void)
{
    lcs16AvhFilteredTorq = LCESP_s16Lpf1Int(eng_torq_rel, lcs16AvhFilteredTorq, L_U8FILTER_GAIN_10MSLOOP_10HZ);  /* 7Hz */

      #if __USE_MTP_RESOL_1000
    if((vref<=VREF_0_25_KPH)&&(mtp_resol1000<S16_AVH_ACC_PEDAL_ON_MIN_TH)&&(eng_rpm < 8000)&&(eng_torq_rel>AVH_IDLETOQ_MIN_TORQ)
    	#else
    if((vref<=VREF_0_25_KPH)&&(mtp<1)&&(eng_rpm < 8000)&&(eng_torq_rel>AVH_IDLETOQ_MIN_TORQ)
    	#endif
        #if __ISG
      && (lcu8AvhEngIsgControlAct==0) /* exclude ISG On */
      && (lcu8AvhIsgActOldCnt==0) /* exclude peak torq at engine restart */
        #endif
    )
    {
    	if (lcs16AvhEngIdleCnt<=L_U8_TIME_10MSLOOP_700MS) {lcs16AvhEngIdleCnt = lcs16AvhEngIdleCnt + 1;}
    	else                              {lcs16AvhEngIdleCnt = 0;}
    }
    else {lcs16AvhEngIdleCnt = 0;}
    
    if (lcs16AvhEngIdleCnt>0)
    {
    	lcs32AvhTorqSum = lcs32AvhTorqSum + (int32_t)lcs16AvhFilteredTorq;

    	
    	if (lcs16AvhEngIdleCnt==L_U8_TIME_10MSLOOP_700MS)
    	{
    		lcs16AvhIdleTorq=(int16_t)(lcs32AvhTorqSum/L_U8_TIME_10MSLOOP_700MS);
    	}
    	else 
    	{
    		;
    	}
    }
    else
    {
    	lcs32AvhTorqSum = 0;
    	if (lcs16AvhIdleTorq==0)
    	{
    		lcs16AvhIdleTorq = S16_AVH_DEFAULT_IDLE_TORQ; /* default 10% */
    	}
    	else 
    	{
    		;
    	}
    }
}

static void	LCAVH_vCheckHill(void)
{
	if ((lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1))
	{
		if ((mtp<3)&&(lcs16AvhFilteredTorq<(lcs16AvhIdleTorq+50)))
		{
			if(lcs16AvhAxG>=S16_AVH_DETECTION_HILL_TH) 
			{
				if (lcu8AvhChkUphillCnt>L_U8_TIME_10MSLOOP_300MS)
				{
					lcu1AvhUphill = 1;
					lcu1AvhDownhill = 0;
				}
				else 
				{
					lcu8AvhChkUphillCnt = lcu8AvhChkUphillCnt+1;
					lcu8AvhChkDownhillCnt = 0;
				}
			}
			else if(lcs16AvhAxG<=-S16_AVH_DETECTION_HILL_TH)
			{
				if(lcu8AvhChkDownhillCnt>L_U8_TIME_10MSLOOP_300MS)
				{
					lcu1AvhUphill = 0;
					lcu1AvhDownhill = 1;
				}
				else
				{
					lcu8AvhChkDownhillCnt = lcu8AvhChkDownhillCnt+1;
					lcu8AvhChkUphillCnt = 0;
				}
			}
			else                          
			{                             
				lcu1AvhUphill = 0;        
				lcu1AvhDownhill = 0;      
        		lcu8AvhChkUphillCnt = 0;  
	    		lcu8AvhChkDownhillCnt = 0;
	    	}
	    }
	    else { }
	}
	else
	{
		lcu1AvhUphill = 0;        
		lcu1AvhDownhill = 0;      
		lcu8AvhChkUphillCnt = 0;  
		lcu8AvhChkDownhillCnt = 0;
	}
}

static void LCAVH_vEstTcCoilTemperature(void)
{
	if((MFC_PWM_DUTY_P>0)||(MFC_PWM_DUTY_S>0)) /*upcount*/
	{
		lcu8AvhTcTemperDeCount=0;
        if(lcu8AvhTcCurAvgPeriodCnt<TC_CUR_AVG_PERIOD) 
        {
        	lcu16AvhTcCurAvg=0;
        	lcu8AvhTcCurAvgPeriodCnt++;
        	
        	if(MFC_PWM_DUTY_P>=MFC_PWM_DUTY_S) {lcu8AvhTemp0=MFC_PWM_DUTY_P;}
        	else                               {lcu8AvhTemp0=MFC_PWM_DUTY_S;}
        	lcu16AvhTcCurSum = lcu16AvhTcCurSum + (uint16_t)lcu8AvhTemp0;
        }
	    else
	    {
            lcu16AvhTcCurAvg = lcu16AvhTcCurSum/TC_CUR_AVG_PERIOD;
            lcu8AvhTcCurAvgPeriodCnt=0;
            lcu16AvhTcCurSum=0;
        }

        if(lcu8AvhTcCurAvgPeriodCnt==0)
        {
        	lcs16AvhTemp0 = (int16_t)lcu16AvhTcCurTemper/1000;
            if(lcu16AvhTcCurAvg==0) { }
            else if(lcu16AvhTcCurAvg>190) {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+15;}
            else if(lcu16AvhTcCurAvg>160) {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+8;}
            else if(lcu16AvhTcCurAvg>140) {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+6;}
            else if(lcu16AvhTcCurAvg>130)  {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+4;}
            else if(lcu16AvhTcCurAvg>120)  {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+3;}
            else if(lcu16AvhTcCurAvg>100)  {lcu16AvhTcCurTemper=lcu16AvhTcCurTemper+1;}            
            else
            {
            	if(lcu16AvhTcCurTemper<15800)  /* 158 degree */
            	{
            		lcu16AvhTcCurTemper = lcu16AvhTcCurTemper + 1;
            	}
            	else
            	{
            		;
            	}
            }
        }        
        else { }
        
        if(lcu16AvhTcCurTemper>30000)   /* max limit 300 degree */
        {
        	lcu16AvhTcCurTemper = 30000;
        }
        else
        {
        	;
        }
    }
	else /*decount */
	{
		lcu8AvhTcCurAvgPeriodCnt=0;
		lcu16AvhTcCurAvg=0;
		lcu16AvhTcCurSum=0;

		if(lcu8AvhTcTemperDeCount<TC_CUR_DECOUNT_PERIOD)
		{
			lcu8AvhTcTemperDeCount++;
		}
		else
		{
			lcu8AvhTcTemperDeCount=0;
			if(lcu16AvhTcCurTemper>=12502)
			{
				lcu16AvhTcCurTemper = lcu16AvhTcCurTemper - 2;
			}
			/*
			else if(lcu16AvhTcCurTemper>=12501)
			{
				lcu16AvhTcCurTemper = lcu16AvhTcCurTemper - 1;
			}
			*/
			else
			{
				lcu16AvhTcCurTemper = 12500;  /* defalut 125 degree */
			}
		}
		
		if(lcu16AvhTcCurTemper==0) {lcu16AvhTcCurTemper = 12500;}
		else { }
	}
	
	if((lcu1AvhTcOverTemper==0)&&(lcu16AvhTcCurTemper>=16000)) /* over temper TH. 160 degree */
	{
		lcu1AvhTcOverTemper=1;
	}
	else if((lcu1AvhTcOverTemper==1)&&(lcu16AvhTcCurTemper>=15000))
	{
		lcu1AvhTcOverTemper=1;
	}
	else
	{
		lcu1AvhTcOverTemper=0;
	}
}


static void	LCAVH_vChkAvhArrangeEpbActReq(void)
{
	if((lcu1AvhEpbActReq==1)
	  #if __SCC
	||(lcu1SccEpbReqAVH==1)
	  #endif
	)
	{
		lcu8AvhReqEpbAct = AVH_EPB_ACT_REQUEST;
	}
	else
	{
		lcu8AvhReqEpbAct = AVH_EPB_NO_REQUEST;
	}

    if(lcu8AvhReqEpbAct==AVH_EPB_ACT_REQUEST)
    {
    	if(lcs16AvhEpbReqCnt<L_U16_TIME_10MSLOOP_10S)
    	{
    		lcs16AvhEpbReqCnt = lcs16AvhEpbReqCnt + 1;
    	}
    	else
    	{
    		;
    	}
    }
    else
    {
    	lcs16AvhEpbReqCnt = 0;
    }
    
    /* validity of EPB Active Request Signal : 0->valid */		
     if(lcu8AvhReqEpbAct==AVH_EPB_ACT_REQUEST)
    {
	    lcu1AvhReqEpbActInvalid=0;    	
	}
	else
	{
		lcu1AvhReqEpbActInvalid=1;    	
	}

    /* AVH I LAMP for SCC */	
	if(lcu1AvhSwitchOn==1)
	{
		lcu1AvhILamp = 0;
	}
	else
	{
		if(lcu8AvhReqEpbAct==AVH_EPB_ACT_REQUEST)
		{
			lcu1AvhILamp = 0;
		}
		else
		{
			lcu1AvhILamp = 1;
		}
	}
}

  #if __AVH_HOOD_TRUNK_CONCEPT==2
static void	LCAVH_vDecideAvhArlamOnTime(void)
{
	if((lcu1AvhInhibitFlg==1)||(lcu1AvhSwitchFail==1))
	{
		lcs16AvhAlarmOnTime = 0;
		lcu8AvhAlarmCase = 0;
	}
	else if ((lcu1AvhInhibitSwOn==0)&&(lcu1AvhInhibitSwOff==0))
	{
		lcs16AvhAlarmOnTime = 0;
		lcu8AvhAlarmCase = 0;
	}
	else if ((lcu1AvhInhibitSwOff==1)&&(lcu1AvhSwitchActOld==0)&&(lcu1AvhSwitchAct==1)) 
	{
		lcs16AvhAlarmOnTime = S16_AVH_ALARM_TIME_INBIT_SW_OFF;
		lcu8AvhAlarmCase = 1;
	}
	else if ((lcu1AvhSwitchOnOld==0)&&(lcu1AvhSwitchOn==0)&&(lcu1AvhInhibitSwOn==1)&&(lcu1AvhSwitchActOld==0)&&(lcu1AvhSwitchAct==1)) 
	{
		lcs16AvhAlarmOnTime = S16_AVH_ALARM_TIME_INBIT_SW_ON;
		lcu8AvhAlarmCase = 2;
	}
	else if (lcu1AvhForcedSwOff==1)
	{
		lcs16AvhAlarmOnTime = S16_AVH_ALARM_TIME_INBIT_SW_ON;
		lcu8AvhAlarmCase = 3;
	}
	else if ((lcu1AvhEpbActOkForSwOffOld==0)&&(lcu1AvhEpbActOkForSwOff==1))
	{
		lcs16AvhAlarmOnTime = S16_AVH_ALARM_TIME_EPB_OK;
		lcu8AvhAlarmCase = 4;
	}
	else if (lcs16AvhAlarmOnTime>0)
	{
		lcs16AvhAlarmOnTime = lcs16AvhAlarmOnTime - 1;
	}
	else
	{	  
		lcs16AvhAlarmOnTime = 0;
		lcu8AvhAlarmCase = 0;
	}
	
	lcu1AvhEpbActOkForSwOffOld = lcu1AvhEpbActOkForSwOff;  
	}		
	  #endif

static void	LCAVH_vDecideAvhCanMsg(void)
{
/************************************************************************************************/
    /* AVH state for TX (only stand alone mode)*/
    /* lcu8AvhState  : For TX CAN message */
    /* lcu8AvhState2 : For Deciding priority between AVH press control and SCC press control */
    if((lcu1AvhActReady==1)||(lcu1AvhActFlg==1))
    {
	    if((lcu1AvhActReady==1)||(lcs8AvhControlState==AVH_HOLD)||(lcs8AvhControlState==AVH_APPLY))
	    {
	    	lcu8AvhState=AVH_IN_ACTIVATION;
	    }
	    else if (lcs8AvhControlState==AVH_RELEASE)
	    {
	    	lcu8AvhState=AVH_IN_RELEASE;
	    }
	    else
	    {
	    	lcu8AvhState=AVH_OUT_ACTIVATION;
	    }
	}
	else if(lcu1AvhAutonomousActFlg==1)
	{
	    if((lcs8AvhControlState==AVH_HOLD)||(lcs8AvhControlState==AVH_APPLY))
	    {
	    	lcu8AvhState2=AVH_IN_ACTIVATION;
	    }
	    else if (lcs8AvhControlState==AVH_RELEASE)
	    {
	    	lcu8AvhState2=AVH_IN_RELEASE;
	    }
	    else
	    {
	    	lcu8AvhState2=AVH_OUT_ACTIVATION;
	    }		
	}
	else
	{
		lcu8AvhState=AVH_OUT_ACTIVATION;
		lcu8AvhState2=AVH_OUT_ACTIVATION;
	}
	
/************************************************************************************************/

    /* AVH Act Lamp On request (only stand alone mode)*/	
	if(lcu8AvhState==AVH_IN_ACTIVATION)
	{
		lcu8AvhLampOffdelay=0;
		lcu8AvhLampRequest=AVH_LAMP_ON_REQUEST;
		lcu1AvhLampOnReq = AVH_LAMP_ON_REQUEST;
	}
	else
	{
		if(lcu8AvhLampOffdelay<1)
		{
			lcu8AvhLampOffdelay++;
		}
		else
		{
		    lcu8AvhLampRequest=AVH_LAMP_OFF_REQUEST;
		    lcu1AvhLampOnReq=AVH_LAMP_OFF_REQUEST;
		}
	}

    /* Alram request */	
    if ((lcs16AvhEpbReqCnt>=L_U8_TIME_10MSLOOP_2S)||(lcu1AvhAlarmReq==1))
    {
    	lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_ON;
    	lcu8AvhCluMsg = AVH_ALARM_CLU_MSG_ON;
    	
    }
    else if(lcs16AvhAlarmOnTime>0)
    {
    	if(lcu8AvhAlarmCase==1)
    {
    	lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_ON;
    	lcu8AvhCluMsg = AVH_CLU_MSG_SW_PUSH_AT_ACT;
    }
    	else if(lcu8AvhAlarmCase==2)
    	{
    		lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_ON;
    		lcu8AvhCluMsg = AVH_CLU_MSG_HTD_OPEN;
    	}
    	else if(lcu8AvhAlarmCase==3)
    	{
    		lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_ON;
    		lcu8AvhCluMsg = AVH_CLU_MSG_HTD_OPEN;
    	}
    	else if(lcu8AvhAlarmCase==4)
    	{
    		lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_OFF;
    		lcu8AvhCluMsg = AVH_CLU_MSG_HTD_OPEN;
    	}
    	else 
    	{
    		lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_OFF;
    		lcu8AvhCluMsg = AVH_ALARM_CLU_MSG_OFF;
    	}
    }    
    else
    {
    	lcu8AvhAlarmRequest=AVH_ALARM_REQUEST_OFF;
    	lcu8AvhCluMsg = AVH_ALARM_CLU_MSG_OFF;
    }
}


void	LCAVH_vControlAvhAbnormalMode(void)
{
	int16_t s16AvhAbnormalHoldRef;
	
	if(lcIgnOffModeFlg==1)
	{
		lcu8epbStatusEPB = EPB_RELEASED_STATE;
		s16AvhAbnormalHoldRef = S16_AVH_ABNORMAL_HOLD_TIME;
	}
	else
	{
		lcu8epbStatusEPB = lcanu8EPB_STATUS;				/* force sataus of EPB */	
		s16AvhAbnormalHoldRef = 0;		
	}

	if((lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1)) 
	{
		if(lcu8epbStatusEPB==EPB_CLAMPED_STATE)
		{
			LCAVH_vResetCtrlVariable();  
		}
		else
		{
			if(lcs16AvhAbnormalHoldCnt<L_U16_TIME_10MSLOOP_10S)
			{
			    lcs16AvhAbnormalHoldCnt = lcs16AvhAbnormalHoldCnt + 1;
			}
			else
			{
			    lcs16AvhAbnormalHoldCnt = L_U16_TIME_10MSLOOP_10S;
			}
            lcu8AvhReqEpbAct = AVH_EPB_ACT_REQUEST; /* for EBD error */
			if(lcs16AvhAbnormalHoldCnt<s16AvhAbnormalHoldRef)
			{
				MFC_PWM_DUTY_P = S16_AVH_ABNORMAL_HOLD_DUTY;
				MFC_PWM_DUTY_S = S16_AVH_ABNORMAL_HOLD_DUTY;
			}
			else
			{
				if(((lcs16AvhAbnormalHoldCnt%S16_AVH_ABNORMAL_FO_REF_SCAN)==0)
				&&(MFC_PWM_DUTY_P>S16_AVH_ABNORMAL_F_O_MIN_DUTY)&&(MFC_PWM_DUTY_S>S16_AVH_ABNORMAL_F_O_MIN_DUTY))
				{
					MFC_PWM_DUTY_P = MFC_PWM_DUTY_P - 1;
					MFC_PWM_DUTY_S = MFC_PWM_DUTY_S - 1;
				}
				else
				{
					;
				}
				
				if((MFC_PWM_DUTY_P<=S16_AVH_ABNORMAL_F_O_MIN_DUTY)||(MFC_PWM_DUTY_S<=S16_AVH_ABNORMAL_F_O_MIN_DUTY))
				{
					MFC_PWM_DUTY_P = 0;
					MFC_PWM_DUTY_S = 0;
					LCAVH_vResetCtrlVariable();
				}
				else
				{
					;
				}
			}
			
			if((MFC_PWM_DUTY_P>0)||(MFC_PWM_DUTY_S>0))
			{
	  		  #if (__SPLIT_TYPE==0)	
				TCL_DEMAND_fl = 1;
				TCL_DEMAND_fr = 1;
	  	      #else
				TCL_DEMAND_SECONDARY = 1;
				TCL_DEMAND_PRIMARY = 1;
	  		  #endif	
	  		}
	  		else
	  		{
	  		  #if (__SPLIT_TYPE==0)	
				TCL_DEMAND_fl = 0;
				TCL_DEMAND_fr = 0;
	  	      #else
				TCL_DEMAND_SECONDARY = 0;
				TCL_DEMAND_PRIMARY = 0;
	  		  #endif
	  		}
		}
	}
	else
	{
		LCAVH_vResetCtrlVariable();  
	}

  #if (__SPLIT_TYPE==0)
    if((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1)||(TCL_DEMAND_fr==1)||
       (TCL_DEMAND_rl==1)) {VALVE_ACT=1;}
  #else
    if((TCL_DEMAND_PRIMARY==1)||(TCL_DEMAND_SECONDARY==1)) {VALVE_ACT=1;}
  #endif
}

#endif /*__AVH */

#if (__AVH)||(__EPB_INTERFACE)
void	LCAVH_vTxLdmState(void) 
{
  #if __EPB_INTERFACE
    if(lcu1EpbInhibitFlg==1)
	{
		lcu8epbLdmstate=1; /* 1:ESP slave, Dynamic Braking By EPB */
	}
	else 
  #endif	
  #if __AVH
    if((lcu1AvhInhibitFlg==1)||(lcu1AvhSwitchFail==1))
	{
		lcu8epbLdmstate=1; /* 1:ESP slave, Dynamic Braking By EPB */
	}
	else
  #endif
	{
		lcu8epbLdmstate=0; /* 0: ESP master, Dynamic Brking By ESC */
	}
}
#endif /* (__AVH)||(__EPB_INTERFACE) */


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCAVHCallControl
	#include "Mdyn_autosar.h"
#endif    
