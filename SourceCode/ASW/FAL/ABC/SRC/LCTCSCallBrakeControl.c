/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCTCSCallBrakeControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "hm_logic_var.h"
#include "LCTCSCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCallEngineControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCTCSCallControl.h"
#include "LDTCSCallDetection.h"
#include "LDBTCSCallDetection.h"
#include "LCRDCMCallControl.h"
#include "LDRTACallDetection.h"
#include "LCESPCallControl.h"
#include "LCEDCCallControl.h"
#include "LDABSCallDctForVref.H"
#include "LDENGEstPowertrain.h"
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LSENGCallSensorSignal.h"
#include "LCTCSCallBrakeControl.h"


#include "ETCSGenCode/LCETCS_vCallMain.h" /* MBD ETCS */

/* Temporary Tuning Parameter */

/* Temporary Tuning Parameter */


#if	defined(BTCS_TCMF_CONTROL) /* TAG1 */
/* GlobalFunction prototype **************************************************/
void LCTCS_vBrakeControl(void);
int16_t LCTCS_vConvPres2CurntOnTC (int16_t Pressure);   
int16_t LCTCS_vConvCurntOnTC2Pres (int16_t Current);

/* LocalFunction prototype ***************************************************/

static void LCTCS_vCalBsStTarWhlSpinDiff(void);  
static void LCTCS_vCalBsHillTarWhlSpinDiff(void); 
static void LCTCS_vCalBsTurnTarWhlSpinDiff(void); 
static void LCTCS_vCalCoodTarWhlSpinDiff(void);

static void LCTCS_vCalBrkCtlErr(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);	
static void LCTCS_vCalIntegralOfBrkCtlErr(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vStrBsTBrkTorq4AsymOld(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalBsTarBrkTorq4AsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalBsTarBrkTrqLimit(struct TCS_AXLE_STRUCT *Axle_TCS);

static void LCTCS_vCalBrkTorqFFAsymSCtlSt(struct TCS_AXLE_STRUCT *Axle_TCS); 
static void LCTCS_vCalBrkTorqFFAsymSCtlHill(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalBrkTorqFFAsymSCtlTurn(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalCoordBrkTqFFAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalMdfyBrkTqFFGearAT(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vCalMdfyBrkTqFFVibration(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
static void LCTCS_vAdpBrkTqFFAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS);

static void LCTCS_vCalBaseStPGainByGear(void);  
static void LCTCS_vCalBaseHillPGainByGear(void);
static void LCTCS_vCalBaseTurnPGainByGear(void);
static void LCTCS_vCoordBasePGainByGear(void);

static void LCTCS_vCalPGainFactInSt(void); 
static void LCTCS_vCalPGainFactInSt(void);
static void LCTCS_vCalPGainFactInTurn(void);
static void LCTCS_vCoordPGain4FedBckCtl(void);


static void LCTCS_vCalBaseStIGainByGear(void); 
static void LCTCS_vCalBaseHillIGainByGear(void);
static void LCTCS_vCalBaseTurnIGainByGear(void);
static void LCTCS_vCoordBaseIGainByGear(void);

static void LCTCS_vCalIGainFactInSt(void); 
static void LCTCS_vCalIGainFactInTurn(void);
static void LCTCS_vCoordIGain4FedBckCtl(void);

static void LCTCS_vCalBrkTorqFBAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vDecBsTarBrkInESCBrk(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
static void LCTCS_vDecBsTarBrkInFadOut(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vDecBsTarBrkInSpCase(struct TCS_AXLE_STRUCT *Axle_TCS);
static void LCTCS_vDecBsTarBrkInVCA(struct TCS_AXLE_STRUCT *Axle_TCS);

/*
static void LCTCS_vDecBsTarBrkInStuck(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT); 
*/

static void LCTCS_vDecBsTarBrkInVIB(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);

static void LCTCS_vCalWhlTarBrkTorq4ASCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
static void LCTCS_vCalBsTarBrkTorq4SymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT,struct W_STRUCT *W_L,struct W_STRUCT *W_R);
static void LCTCS_vCalWhlTarBrkTorq4VCACtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
/*
static void LCTCS_vCalWhlTarBrkTorq4StuckCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
*/
static void LCTCS_vCordBsTarBrkTorq (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
static void LCTCS_vCovBrkTrq2WhlPres (void);
static void LCTCS_vCalTarAxlePres(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
#if (__SPLIT_TYPE==0)     /* X-SPLIT */
static void LCTCS_vCalTarCirPresXSplit(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR);
#else
static void LCTCS_vCalTarCirPresFRSplit(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
#endif
/* Implementation  **********************************************************/
    

void LCTCS_vBrakeControl(void) /*TCMF*/
{
    LCTCS_vCalBsStTarWhlSpinDiff();
    LCTCS_vCalBsHillTarWhlSpinDiff(); 
    LCTCS_vCalBsTurnTarWhlSpinDiff(); 
    LCTCS_vCalCoodTarWhlSpinDiff();
    
    LCTCS_vCalBrkCtlErr(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vCalBrkCtlErr(&RA_TCS,&RL_TCS,&RR_TCS);
    
    LCTCS_vCalIntegralOfBrkCtlErr(&FA_TCS);
    LCTCS_vCalIntegralOfBrkCtlErr(&RA_TCS);
    
    LCTCS_vCalBaseStPGainByGear();  
    LCTCS_vCalBaseHillPGainByGear();
    LCTCS_vCalBaseTurnPGainByGear(); 
    LCTCS_vCoordBasePGainByGear();
    
    
    LCTCS_vCalPGainFactInSt(); 
    LCTCS_vCalPGainFactInTurn();
    LCTCS_vCoordPGain4FedBckCtl();
    
    LCTCS_vCalBaseStIGainByGear();
    LCTCS_vCalBaseHillIGainByGear();
    LCTCS_vCalBaseTurnIGainByGear();
    LCTCS_vCoordBaseIGainByGear();
    
    LCTCS_vCalIGainFactInSt();
    LCTCS_vCalIGainFactInTurn();
    LCTCS_vCoordIGain4FedBckCtl();
    
    LCTCS_vStrBsTBrkTorq4AsymOld(&FA_TCS);  /*호출 위치 주의 lctcss16BsTBrkTorq4Asym 최종 결정 후 저장 */
    LCTCS_vStrBsTBrkTorq4AsymOld(&RA_TCS);
    
	LCTCS_vCalBrkTorqFFAsymSCtlSt(&FA_TCS);
	LCTCS_vCalBrkTorqFFAsymSCtlSt(&RA_TCS);
	LCTCS_vCalBrkTorqFFAsymSCtlHill(&FA_TCS);
	LCTCS_vCalBrkTorqFFAsymSCtlHill(&RA_TCS);
	LCTCS_vCalBrkTorqFFAsymSCtlTurn(&FA_TCS);
	LCTCS_vCalBrkTorqFFAsymSCtlTurn(&RA_TCS);
	LCTCS_vCalCoordBrkTqFFAsymSCtl(&FA_TCS);
	LCTCS_vCalCoordBrkTqFFAsymSCtl(&RA_TCS);
	LCTCS_vCalMdfyBrkTqFFGearAT(&FA_TCS);
	LCTCS_vCalMdfyBrkTqFFGearAT(&RA_TCS);
	LCTCS_vCalMdfyBrkTqFFVibration(&FA_TCS,&FL_TCS,&FR_TCS); 
	LCTCS_vCalMdfyBrkTqFFVibration(&RA_TCS,&RL_TCS,&RR_TCS); 
	LCTCS_vAdpBrkTqFFAsymSCtl(&FA_TCS);
	LCTCS_vAdpBrkTqFFAsymSCtl(&RA_TCS);
	
	LCTCS_vCalBrkTorqFBAsymSCtl(&FA_TCS);
	LCTCS_vCalBrkTorqFBAsymSCtl(&RA_TCS);

    LCTCS_vCalBsTarBrkTorq4AsymSCtl(&FA_TCS);
    LCTCS_vCalBsTarBrkTorq4AsymSCtl(&RA_TCS);    
    
    LCTCS_vDecBsTarBrkInESCBrk(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vDecBsTarBrkInESCBrk(&RA_TCS,&RL_TCS,&RR_TCS);    
    
	LCTCS_vDecBsTarBrkInFadOut(&FA_TCS);
	LCTCS_vDecBsTarBrkInFadOut(&RA_TCS);

    LCTCS_vDecBsTarBrkInSpCase(&FA_TCS);
    LCTCS_vDecBsTarBrkInSpCase(&RA_TCS);
    
	LCTCS_vCalBsTarBrkTrqLimit(&FA_TCS);
	LCTCS_vCalBsTarBrkTrqLimit(&RA_TCS);
    
    LCTCS_vDecBsTarBrkInVCA(&FA_TCS); 
    LCTCS_vDecBsTarBrkInVCA(&RA_TCS);  
    
    /*
    LCTCS_vDecBsTarBrkInStuck(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vDecBsTarBrkInStuck(&RA_TCS,&RL_TCS,&RR_TCS);
    */
    
    LCTCS_vDecBsTarBrkInVIB(&FA_TCS,&FL_TCS,&FR_TCS); 
    LCTCS_vDecBsTarBrkInVIB(&RA_TCS,&RL_TCS,&RR_TCS); 
    
    
    LCTCS_vCalWhlTarBrkTorq4ASCtl(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vCalWhlTarBrkTorq4ASCtl(&RA_TCS,&RL_TCS,&RR_TCS);
    
    /* MBD ETCS */
    /*LCTCS_vCalBsTarBrkTorq4SymSCtl(&FA_TCS,&FL_TCS,&FR_TCS,&FL,&FR);
    LCTCS_vCalBsTarBrkTorq4SymSCtl(&RA_TCS,&RL_TCS,&RR_TCS,&RL,&RR);*/
    FA_TCS.lctcss16BsTBrkTorq4Sym = ETCS_BRK_CTL_CMD.lcetcss16SymBrkTrqCtlCmdFA;
    FL_TCS.lctcss16BsTarBrkTorq4SymSCtl = FA_TCS.lctcss16BsTBrkTorq4Sym;
    FR_TCS.lctcss16BsTarBrkTorq4SymSCtl = FA_TCS.lctcss16BsTBrkTorq4Sym;

    RA_TCS.lctcss16BsTBrkTorq4Sym = ETCS_BRK_CTL_CMD.lcetcss16SymBrkTrqCtlCmdRA;
    RL_TCS.lctcss16BsTarBrkTorq4SymSCtl = RA_TCS.lctcss16BsTBrkTorq4Sym;
    RR_TCS.lctcss16BsTarBrkTorq4SymSCtl = RA_TCS.lctcss16BsTBrkTorq4Sym; 
    
    
    LCTCS_vCalWhlTarBrkTorq4VCACtl(&FA_TCS,&FL_TCS,&FR_TCS); 
    LCTCS_vCalWhlTarBrkTorq4VCACtl(&RA_TCS,&RL_TCS,&RR_TCS);
  
    /*
    LCTCS_vCalWhlTarBrkTorq4StuckCtl(&FA_TCS,&FL_TCS,&FR_TCS,&FL,&FR);
    LCTCS_vCalWhlTarBrkTorq4StuckCtl(&RA_TCS,&RL_TCS,&RR_TCS,&RL,&RR);
    */
    
    
    LCTCS_vCordBsTarBrkTorq(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vCordBsTarBrkTorq(&RA_TCS,&RL_TCS,&RR_TCS);
    
    LCTCS_vCovBrkTrq2WhlPres ();
    LCTCS_vCalTarAxlePres(&FA_TCS,&FL_TCS,&FR_TCS);
    LCTCS_vCalTarAxlePres(&RA_TCS,&RL_TCS,&RR_TCS);
    

  #if (__SPLIT_TYPE==0)     /* X-SPLIT */
    LCTCS_vCalTarCirPresXSplit(&PC_TCS, &FR_TCS, &RL_TCS);
    LCTCS_vCalTarCirPresXSplit(&SC_TCS, &FL_TCS, &RR_TCS);
  #else						/* FR-SPLIT */
	LCTCS_vCalTarCirPresFRSplit(&PC_TCS, &FL_TCS, &FR_TCS);
	LCTCS_vCalTarCirPresFRSplit(&SC_TCS, &RL_TCS, &RR_TCS);	
  #endif	  
}

static void LCTCS_vCalBsStTarWhlSpinDiff(void)
{
	ltcss16BaseStTarWhlSpinDiff = LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)U8TCSCpBaseTarWhlSpinDiff_1,
				   		  						   	    	                (int16_t)U16_TCS_SPEED2, (int16_t)U8TCSCpBaseTarWhlSpinDiff_2,
	            		                                                    (int16_t)U16_TCS_SPEED3, (int16_t)U8TCSCpBaseTarWhlSpinDiff_3,
	            		                                                    (int16_t)U16_TCS_SPEED4, (int16_t)U8TCSCpBaseTarWhlSpinDiff_4,
	                    		                                            (int16_t)U16_TCS_SPEED5, (int16_t)U8TCSCpBaseTarWhlSpinDiff_5); 
	                    		                               
	ltcss16BaseStTarWhlSpinDiff = LCTCS_s16ILimitRange(ltcss16BaseStTarWhlSpinDiff , 0, VREF_200_KPH );
}

static void LCTCS_vCalBsHillTarWhlSpinDiff(void)
{
	ltcss16BaseHillTarWhlSpinDiff = LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)U8TCSCpTarWhlSpinDiffHill_1,
	   		  						   	    	                              (int16_t)U16_TCS_SPEED2, (int16_t)U8TCSCpTarWhlSpinDiffHill_2,
	            		                                                      (int16_t)U16_TCS_SPEED3, (int16_t)U8TCSCpTarWhlSpinDiffHill_3,
	            		                                                      (int16_t)U16_TCS_SPEED4, (int16_t)U8TCSCpTarWhlSpinDiffHill_4,
	                    		                                              (int16_t)U16_TCS_SPEED5, (int16_t)U8TCSCpTarWhlSpinDiffHill_5); 
	                    		                               
	ltcss16BaseHillTarWhlSpinDiff = LCTCS_s16ILimitRange(ltcss16BaseHillTarWhlSpinDiff , 0, VREF_200_KPH );
}

static void LCTCS_vCalBsTurnTarWhlSpinDiff(void)
{
	ltcss16BaseTurnTarWhlSpinDiff = LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)U8TCSCpTarWhlSpinDiffTurn_1,
    	  						   	    	             					  (int16_t)U16_TCS_SPEED2, (int16_t)U8TCSCpTarWhlSpinDiffTurn_2,
               		                                     					  (int16_t)U16_TCS_SPEED3, (int16_t)U8TCSCpTarWhlSpinDiffTurn_3,
               		                                     					  (int16_t)U16_TCS_SPEED4, (int16_t)U8TCSCpTarWhlSpinDiffTurn_4,
                       		                             					  (int16_t)U16_TCS_SPEED5, (int16_t)U8TCSCpTarWhlSpinDiffTurn_5); 
      
	ltcss16BaseTurnTarWhlSpinDiff = LCTCS_s16ILimitRange(ltcss16BaseTurnTarWhlSpinDiff , 0, VREF_200_KPH );
}   

static void LCTCS_vCalCoodTarWhlSpinDiff(void)
{
	if (lctcsu1DctSplitHillFlg==0)
	{
    	ltcss16BaseTarWhlSpinDiff =  LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, ltcss16BaseStTarWhlSpinDiff,
    																		   100, ltcss16BaseTurnTarWhlSpinDiff); 
	}
   else
   {						
		ltcss16BaseTarWhlSpinDiff = ltcss16BaseHillTarWhlSpinDiff;
   }
   
	if (lctcss16BrkCtrlMode == S16_TCS_CTRL_MODE_SP)
	{
		ltcss16BaseTarWhlSpinDiff = ltcss16BaseTarWhlSpinDiff + S16TCSCpADDBTCTarSP4ComPMod ;
	}
	else
	{
		;
	}
   
   
}

static void LCTCS_vCalBrkCtlErr(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==0) )
	{
		tcs_tempW0 = Axle_TCS->ltcss16WhlSpinDiff;
	}
	else if( (WL_LEFT->lctcsu1BTCSWhlAtv==0) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		tcs_tempW0 = - Axle_TCS->ltcss16WhlSpinDiff;
	}
	else
	{
		tcs_tempW0 = 0;
	}
	Axle_TCS->lctcss16BrkCtlErr = tcs_tempW0 - ltcss16BaseTarWhlSpinDiff;
}

static void LCTCS_vCalIntegralOfBrkCtlErr(struct TCS_AXLE_STRUCT *Axle_TCS)
{	
	if (Axle_TCS->ltcss8IIntAct==1)
	{
		Axle_TCS->lctcss32IntgBrkCtlErrOld = Axle_TCS->lctcss32IntgBrkCtlErr;
		Axle_TCS->lctcss32IntgBrkCtlErr = ((Axle_TCS->lctcss32IntgBrkCtlErrOld*999)/1000) + (((int32_t)Axle_TCS->lctcss16BrkCtlErr*10)/8);
	}
	else
	{
		Axle_TCS->lctcss32IntgBrkCtlErrOld = 0;
		Axle_TCS->lctcss32IntgBrkCtlErr = 0;
	}
}

static void LCTCS_vStrBsTBrkTorq4AsymOld(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->lctcss16BsTBrkTorq4AsymOld = Axle_TCS->lctcss16BsTBrkTorq4Asym;
}

static void LCTCS_vCalBsTarBrkTorq4AsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if 		(Axle_TCS->ltcsu1ModOfAsymSpnCtl == S16TCSAsymSpinFwdCtlMode)		
	{
		Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16FFBrkTorq4Asym;
	}
	else if (Axle_TCS->ltcsu1ModOfAsymSpnCtl == S16TCSAsymSpinBwdCtlMode)	
	{
		Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16FBBrkTorq4Asym;
	}
	else
	{
		Axle_TCS->lctcss16BsTBrkTorq4Asym = 0;
	}
}

static void LCTCS_vCalBsTarBrkTrqLimit(struct TCS_AXLE_STRUCT *Axle_TCS)
{	
	if (Axle_TCS->lctcsu1BrkTrqMaxLimited == 1)
	{	/* Split-mu hill 등판시 wheel on high mu unsable 감지시 learning 된 brake torque로 maximum limitation */
		Axle_TCS->lctcss16BsTBrkLimTorq4Asym = Axle_TCS->lctcss16BrkTrqMax4Asym;
	}		
	else
	{
		if(Axle_TCS->ltcsu1RearAxle==0)
		{
			tcs_tempW0 = (int16_t)U8TCSCpSclFctTrq2PresFrt;
		}
		else
		{
			tcs_tempW0 = (int16_t)U8TCSCpSclFctTrq2PresRer;
		}
	
	 	/* 직진 상태 Limit값 통상 Max Target Pressure를 160bar로 잡고 이를 토크로 환산*/
		Axle_TCS->lctcss16BsTarBrkTorqMaxST = (int16_t)((int32_t)(S16TCSCpTarWhlPreStMax * tcs_tempW0)/10) ;
  
	 	/*Lim값 = 속도에 따라 Interpolation	속도 높을수록 Limit 작게*/
		tcs_tempW1=(U8TCSCpTarWhlPreTurnMax_1 * tcs_tempW0)/10 ;
		tcs_tempW2=(U8TCSCpTarWhlPreTurnMax_2 * tcs_tempW0)/10 ;
		tcs_tempW3=(U8TCSCpTarWhlPreTurnMax_3 * tcs_tempW0)/10 ;
		tcs_tempW4=(U8TCSCpTarWhlPreTurnMax_4 * tcs_tempW0)/10 ;
		tcs_tempW5=(U8TCSCpTarWhlPreTurnMax_5 * tcs_tempW0)/10 ; 
	
		Axle_TCS->lctcss16BsTarBrkTorqMaxTurn = LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t) U16_TCS_SPEED1,  tcs_tempW1,
			   							   					                              (int16_t) U16_TCS_SPEED2,  tcs_tempW2,
															                              (int16_t) U16_TCS_SPEED3,  tcs_tempW3,
											  				                              (int16_t) U16_TCS_SPEED4,  tcs_tempW4,
											   				                              (int16_t) U16_TCS_SPEED5,  tcs_tempW5);
  
		Axle_TCS->lctcss16BsTBrkLimTorq4Asym = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, Axle_TCS->lctcss16BsTarBrkTorqMaxST,      
        	          														             100, Axle_TCS->lctcss16BsTarBrkTorqMaxTurn);
	}
	Axle_TCS->lctcss16BsTBrkTorq4Asym =	 LCTCS_s16ILimitRange(Axle_TCS->lctcss16BsTBrkTorq4Asym , 0, Axle_TCS->lctcss16BsTBrkLimTorq4Asym);
}                                      

static void LCTCS_vCalBrkTorqFFAsymSCtlSt(struct TCS_AXLE_STRUCT *Axle_TCS)
{	
	if (gear_pos == GEAR_POS_TM_R)
	{
		FA_TCS.lctcss16AsmdMuDiffSt = (int16_t)U8TCSCpBsAsmdMuDiffSt_F1;
		RA_TCS.lctcss16AsmdMuDiffSt = (int16_t)U8TCSCpBsAsmdMuDiffSt_R1;
	}
	else
	{
		FA_TCS.lctcss16AsmdMuDiffSt = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffSt_F1,  
		                                                              (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffSt_F2,  
		                                                              (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffSt_F3,  
		                                                              (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffSt_F4,  
		                                                              (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffSt_F5);
    	
    	RA_TCS.lctcss16AsmdMuDiffSt = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffSt_R1,  
		                                                              (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffSt_R2,  
		                                                              (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffSt_R3,  
		                                                              (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffSt_R4,  
		                                                              (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffSt_R5);  
	}
	Axle_TCS->lctcss16FFBrkTorqSt4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16AsmdMuDiffSt * Axle_TCS->lctcss16MassOfOneCorn) /100)*(Axle_TCS->lctcss16WhlRolR/10))/10;/*resolution  Mu 1/100 WhlRolR 1/1000 중력가속도 10*/	
}  

static void LCTCS_vCalBrkTorqFFAsymSCtlHill(struct TCS_AXLE_STRUCT *Axle_TCS)
{	
	if (gear_pos == GEAR_POS_TM_R)
	{
		FA_TCS.lctcss16AsmdMuDiffHill = (int16_t)U8TCSCpBsAsmdMuDiffHill_F1;
		RA_TCS.lctcss16AsmdMuDiffHill = (int16_t)U8TCSCpBsAsmdMuDiffHill_R1;
	}
	else
	{
		FA_TCS.lctcss16AsmdMuDiffHill = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffHill_F1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffHill_F2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffHill_F3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffHill_F4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffHill_F5);
      
		RA_TCS.lctcss16AsmdMuDiffHill = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffHill_R1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffHill_R2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffHill_R3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffHill_R4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffHill_R5);  
	}
	Axle_TCS->lctcss16FFBrkTorqHill4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16AsmdMuDiffHill * Axle_TCS->lctcss16MassOfOneCorn) /100)*(Axle_TCS->lctcss16WhlRolR/10))/10;/*resolution  Mu 1/100 WhlRolR 1/1000 중력가속도 10*/	
} 

static void LCTCS_vCalBrkTorqFFAsymSCtlTurn(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (gear_pos == GEAR_POS_TM_R)
	{
		FA_TCS.lctcss16AsmdMuDiffTurn = (int16_t)U8TCSCpBsAsmdMuDiffTurn_F1;
    	RA_TCS.lctcss16AsmdMuDiffTurn = (int16_t)U8TCSCpBsAsmdMuDiffTurn_R1;
	}
	else
	{
		FA_TCS.lctcss16AsmdMuDiffTurn = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffTurn_F1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffTurn_F2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffTurn_F3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffTurn_F4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffTurn_F5);
      
		RA_TCS.lctcss16AsmdMuDiffTurn = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsAsmdMuDiffTurn_R1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsAsmdMuDiffTurn_R2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsAsmdMuDiffTurn_R3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsAsmdMuDiffTurn_R4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsAsmdMuDiffTurn_R5);  
	}
	Axle_TCS->lctcss16FFBrkTorqTurn4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16AsmdMuDiffTurn * Axle_TCS->lctcss16MassOfOneCorn) /100)*(Axle_TCS->lctcss16WhlRolR/10))/10;/*resolution  Mu 1/100 WhlRolR 1/1000 중력가속도 10*/	
}

static void LCTCS_vCalCoordBrkTqFFAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->lctcss16NorFFBrkTorq4AsymOld = Axle_TCS->lctcss16NorFFBrkTorq4Asym;
	if(lctcsu1DctSplitHillFlg==0)
	{ 
		Axle_TCS->lctcss16FFBrkTorq4Asym = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, Axle_TCS->lctcss16FFBrkTorqSt4Asym,
                               														 100, Axle_TCS->lctcss16FFBrkTorqTurn4Asym);    
	}
	else
	{                                                   														 
  		Axle_TCS->lctcss16FFBrkTorq4Asym = Axle_TCS->lctcss16FFBrkTorqHill4Asym;
    }
    Axle_TCS->lctcss16NorFFBrkTorq4Asym = Axle_TCS->lctcss16FFBrkTorq4Asym;
}

static void LCTCS_vCalMdfyBrkTqFFGearAT(struct TCS_AXLE_STRUCT *Axle_TCS)   /*AT gear shift시 급격한 변경 방지*/
{
	if  ( AUTO_TM == 1 ) 
	{
		if (AT_gear_pos_old != gear_pos)
		{
			Axle_TCS->lctcss16NorGearSftFFBrkTrqDif = ( Axle_TCS->lctcss16NorFFBrkTorq4Asym - Axle_TCS->lctcss16NorFFBrkTorq4AsymOld)/L_U8_TIME_10MSLOOP_300MS;
			Axle_TCS->lctcss16NorFFBrkTorq4AsymOldMem = Axle_TCS->lctcss16NorFFBrkTorq4AsymOld;
			Axle_TCS->lctcsu1GearSftFFBrkTorqStart = 1;	
		}
		else
		{
			;
		}
		
		if( Axle_TCS->lctcsu1GearSftFFBrkTorqStart == 1 )
		{
				Axle_TCS->lctcss16GearSftFFBrkTorqCnt = Axle_TCS->lctcss16GearSftFFBrkTorqCnt + 1;
				Axle_TCS->lctcss16FFBrkTorq4Asym = Axle_TCS->lctcss16NorFFBrkTorq4AsymOldMem + (Axle_TCS->lctcss16NorGearSftFFBrkTrqDif * Axle_TCS->lctcss16GearSftFFBrkTorqCnt) ;
			
			if ( Axle_TCS->lctcss16GearSftFFBrkTorqCnt == L_U8_TIME_10MSLOOP_300MS )
				{
					Axle_TCS->lctcss16GearSftFFBrkTorqCnt = 0;
					Axle_TCS->lctcsu1GearSftFFBrkTorqStart = 0;	
			}
			else
			{
				;
			} 
		}
		else
		{
			Axle_TCS->lctcss16GearSftFFBrkTorqCnt = 0;
			Axle_TCS->lctcsu1GearSftFFBrkTorqStart = 0;
		}
	}
	else
	{
		Axle_TCS->lctcss16GearSftFFBrkTorqCnt = 0;
		Axle_TCS->lctcsu1GearSftFFBrkTorqStart = 0;
	}	
}

static void LCTCS_vCalMdfyBrkTqFFVibration(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT) 
{
	#if((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))
	if((WL_LEFT->lctcsu1VIBFlag_tcmf==1)||(WL_RIGHT->lctcsu1VIBFlag_tcmf==1))
	#else
	if(((WL_LEFT->lctcsu1VIBFlag_tcmf==1)||(WL_RIGHT->lctcsu1VIBFlag_tcmf==1))&&
		((lctcsu1DctSplitHillFlg==0)||((lctcsu1DctSplitHillFlg==1)&&(U8TCSHillVibrationCTRL==1)))  		/*2WD Flat only, Hill option */  
		)                                        
	#endif
	{
		Axle_TCS->tcs_axle_tempW0 = Axle_TCS->lctcss16FFBrkTorq4Asym/3;
		Axle_TCS->tcs_axle_tempW0 = LCTCS_s16IFindMaximum(Axle_TCS->tcs_axle_tempW0, S16FFBrkTorqReduce4Vibration);

		Axle_TCS->lctcss16FFBrkTorq4AsymVib = Axle_TCS->lctcss16FFBrkTorq4Asym - Axle_TCS->tcs_axle_tempW0;
		Axle_TCS->lctcss16FFBrkTorq4AsymVib = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16FFBrkTorq4AsymVib, (int16_t)U8TCSFadeOutBrkCtrlMinTorq);	
	}
	else
	{
		;
	}
}

static void LCTCS_vAdpBrkTqFFAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if 		(Axle_TCS->lctcsu1HuntingDcted==1)
	{
		Axle_TCS->lctcss16FFBrkTorq4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16FFBrkTorq4Asym)*U8TCSCpFFReductFactInHunting)/100);
	    if (Axle_TCS->lctcsu1L2SplitSuspectDcted==1)
	    {
			if ((Axle_TCS->lctcsu1L2SplitSuspectDctedOld==0)&&(Axle_TCS->lctcsu1L2SplitSuspectDcted==1))
			{
				Axle_TCS->lctcss16MemFFBrkTorq4L2SpSus = Axle_TCS->lctcss16FFBrkTorq4Asym;
			}
			else
			{
				Axle_TCS->lctcss16FFBrkTorq4L2SpSus = Axle_TCS->lctcss16MemFFBrkTorq4L2SpSus + (int16_t)((int32_t)Axle_TCS->lctcss16CntAfterL2SplitSus*U8TCSCpL2SplitSuspectTorq);
				Axle_TCS->lctcss16FFBrkTorq4Asym = LCTCS_s16ILimitMaximum(Axle_TCS->lctcss16FFBrkTorq4L2SpSus, Axle_TCS->lctcss16NorFFBrkTorq4Asym);
			}
		}
		else
		{
			;
		}
	}
	else if (Axle_TCS->lctcsu1FFAdaptByIIntReset==1)
	{
		Axle_TCS->lctcss16FFBrkTorq4Asym = Axle_TCS->lctcss16FFBrkTorq4Asym + Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset;
	}
	else
	{
		;
	}
	
	if((Axle_TCS->lctcsu1OK2AdaptFF==0) && (lctcsu1DctSplitHillFlg==0) && (Axle_TCS->lctcss16HghMuUstbReptReduceCnt >= 1) )  /*mu 차이가 작은 ice/snow Flat split에서의 잦은 HSS 유발 방지*/
	{
		/*Detection부 HSS counter 구현 할 것   BTCS 제어 중 HSS 발생 시 마다 ++ count 일정 시간 발생하지 않으면 --  BTCS제어 아니면 0    범위 0 ~ 5  */
		Axle_TCS->lctcss16FFBrkTorq4Asym = (Axle_TCS->lctcss16FFBrkTorq4Asym - ((Axle_TCS->lctcss16FFBrkTorq4Asym/5)*Axle_TCS->lctcss16HghMuUstbReptReduceCnt) );
		Axle_TCS->lctcss16FFBrkTorq4Asym = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16FFBrkTorq4Asym, 0);
	}	
	else if(Axle_TCS->lctcsu1FFAdaptByHghMuWhlUnstab==1)  /*FF Brake torque가 high side unstable을 유발한 Torque보다는 작도록 limit*/
	{
		Axle_TCS->lctcss16FFBrkTorq4Asym = LCTCS_s16ILimitMaximum(Axle_TCS->lctcss16FFBrkTorq4Asym, Axle_TCS->lctcss16AdaptedFFBrkTorq4Asym);
	}
	else
	{
		;
	}
}
	
				
static void LCTCS_vCalBrkTorqFBAsymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS)
{   
	Axle_TCS->lctcss16FBBrkTorq4AsymOld = Axle_TCS->lctcss16FBBrkTorq4Asym;
	Axle_TCS->lctcss16PeffOnBsTBrkTorq4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16BrkCtlErr) * Axle_TCS->lctcss16PG4ASFBCtrl)/10);
	Axle_TCS->lctcss16IeffOnBsTBrkTorq4Asym = (int16_t)((Axle_TCS->lctcss32IntgBrkCtlErr * Axle_TCS->lctcss16IG4ASFBCtrl)/1000);
	Axle_TCS->lctcss16FBBrkTorq4Asym = Axle_TCS->lctcss16FFBrkTorq4Asym + Axle_TCS->lctcss16PeffOnBsTBrkTorq4Asym  + Axle_TCS-> lctcss16IeffOnBsTBrkTorq4Asym;
}                                                  


static void LCTCS_vCalBaseStPGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R)
	{
		lctcs16BsStPG4ASFBCtlPositiveF = (int16_t)U8TCSCpBsStPG4ASFBCtlP_F1;    
		lctcs16BsStPG4ASFBCtlNegativeF = (int16_t)U8TCSCpBsStPG4ASFBCtlN_F1;      
		lctcs16BsStPG4ASFBCtlPositiveR = (int16_t)U8TCSCpBsStPG4ASFBCtlP_R1;  
    	lctcs16BsStPG4ASFBCtlNegativeR = (int16_t)U8TCSCpBsStPG4ASFBCtlN_R1;
	}
	else
	{
		lctcs16BsStPG4ASFBCtlPositiveF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStPG4ASFBCtlP_F1,  
	                                                                     (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStPG4ASFBCtlP_F2,  
	                                                                     (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStPG4ASFBCtlP_F3,  
	                                                                     (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStPG4ASFBCtlP_F4,  
	                                                                     (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStPG4ASFBCtlP_F5); 
	    
		lctcs16BsStPG4ASFBCtlNegativeF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStPG4ASFBCtlN_F1, 
                                                                         (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStPG4ASFBCtlN_F2, 
                                                                         (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStPG4ASFBCtlN_F3, 
                                                                         (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStPG4ASFBCtlN_F4, 
                                                                         (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStPG4ASFBCtlN_F5);
                                                                                                                               
		lctcs16BsStPG4ASFBCtlPositiveR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStPG4ASFBCtlP_R1,                                                             
                                                                         (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStPG4ASFBCtlP_R2,                                 
                                                                         (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStPG4ASFBCtlP_R3,                                 
                                                                         (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStPG4ASFBCtlP_R4, 
                                                                         (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStPG4ASFBCtlP_R5);
                                                                                               
		lctcs16BsStPG4ASFBCtlNegativeR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStPG4ASFBCtlN_R1, 
                                                                         (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStPG4ASFBCtlN_R2, 
                                                                         (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStPG4ASFBCtlN_R3, 
                                                                         (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStPG4ASFBCtlN_R4,                                  
                                                                         (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStPG4ASFBCtlN_R5);
	}
}
                                                                                    
static void LCTCS_vCalBaseHillPGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R)
	{
		lctcs16BsHillPG4ASFBCtlPositF = (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F1;    
		lctcs16BsHillPG4ASFBCtlNegatF = (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F1;      
		lctcs16BsHillPG4ASFBCtlPositR = (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R1;  
		lctcs16BsHillPG4ASFBCtlNegatR = (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R1;                     
	}
	else
	{
		lctcs16BsHillPG4ASFBCtlPositF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_F5); 
	    
		lctcs16BsHillPG4ASFBCtlNegatF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F1, 
                                                                        (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F2, 
                                                                        (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F3, 
                                                                        (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F4, 
                                                                        (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_F5);
                                                                                                                             
		lctcs16BsHillPG4ASFBCtlPositR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R1,                                                             
        		                                                        (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R2,                                 
                                                                        (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R3,                                 
                                                                        (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R4, 
                                                                        (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillPG4ASFBCtlP_R5);
                                                                                        
		lctcs16BsHillPG4ASFBCtlNegatR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R1, 
        	                        	                                (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R2, 
            	                	                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R3, 
                	        	                                        (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R4,                                  
                    		                                            (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillPG4ASFBCtlN_R5);
	}
}

static void LCTCS_vCalBaseTurnPGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R)
	{
		lctcs16BsTurnPG4ASFBCtlPositiveF = (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F1;    
		lctcs16BsTurnPG4ASFBCtlNegativeF = (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F1;      
		lctcs16BsTurnPG4ASFBCtlPositiveR = (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R1;  
		lctcs16BsTurnPG4ASFBCtlNegativeR = (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R1;
	}
	else
	{
	    lctcs16BsTurnPG4ASFBCtlPositiveF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F1,  
	     	                                                               (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F2,  
	                                                                       (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F3,  
	                                                                       (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F4,  
	                                                                       (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_F5); 
	                                                                                              
		lctcs16BsTurnPG4ASFBCtlNegativeF = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F1, 
                                                                           (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F2, 
                                                                           (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F3, 
                                                                           (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F4, 
                                                                           (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_F5);
                                                                                                                            
		lctcs16BsTurnPG4ASFBCtlPositiveR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R1,                                                             
                                                                           (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R2,                                 
                                                                           (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R3,                                 
                                                                           (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R4, 
                                                                           (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnPG4ASFBCtlP_R5);
                                                                                                
		lctcs16BsTurnPG4ASFBCtlNegativeR = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R1, 
                                                                           (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R2, 
                                                                           (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R3, 
                                                                           (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R4,                                  
                                                                           (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnPG4ASFBCtlN_R5);
	}
}

static void LCTCS_vCoordBasePGainByGear(void)
{
	if (lctcsu1DctSplitHillFlg==0)
    {
		lctcs16BsPG4ASFBCtlPositiveF = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStPG4ASFBCtlPositiveF,
                                       											 100, lctcs16BsTurnPG4ASFBCtlPositiveF);
		
		lctcs16BsPG4ASFBCtlNegativeF = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStPG4ASFBCtlNegativeF,
                                       											 100, lctcs16BsTurnPG4ASFBCtlNegativeF);
    	                                                 											 
		lctcs16BsPG4ASFBCtlPositiveR = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStPG4ASFBCtlPositiveR,
                                       											 100, lctcs16BsTurnPG4ASFBCtlPositiveR);
    	                                                 											 
		lctcs16BsPG4ASFBCtlNegativeR = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStPG4ASFBCtlNegativeR,
                                       											 100, lctcs16BsTurnPG4ASFBCtlNegativeR);	
    }
    else
    {
		lctcs16BsPG4ASFBCtlPositiveF = lctcs16BsHillPG4ASFBCtlPositF;                             
		lctcs16BsPG4ASFBCtlNegativeF = lctcs16BsHillPG4ASFBCtlNegatF;
		lctcs16BsPG4ASFBCtlPositiveR = lctcs16BsHillPG4ASFBCtlPositR;
		lctcs16BsPG4ASFBCtlNegativeR = lctcs16BsHillPG4ASFBCtlNegatR;
    }       
}                                 
                                      
static void LCTCS_vCalPGainFactInSt(void)            
{
	FA_TCS.lctcss16PGF4ASFBCtrlSt = LCESP_s16IInter5Point((int16_t)eng_rpm, 1000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_F1, 
	                                                               2000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_F2, 
	                                                               3000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_F3, 
	                                                               4000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_F4, 
	                                                               5000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_F5);
	
	RA_TCS.lctcss16PGF4ASFBCtrlSt = LCESP_s16IInter5Point((int16_t)eng_rpm, 1000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_R1, 
	                                                               2000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_R2,     
	                                                               3000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_R3,     
	                                                               4000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_R4,     
	                                                               5000, (int16_t)U8TCSCpPgFac4ASFBCtlSt_R5);    
}

static void LCTCS_vCalPGainFactInTurn(void)      
{
	FA_TCS.lctcss16PGF4ASFBCtrlTurn = LCESP_s16IInter5Point((int16_t)eng_rpm, 1000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_F1, 
	  																 2000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_F2, 
																	 3000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_F3, 
																	 4000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_F4, 
																	 5000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_F5);
	
	RA_TCS.lctcss16PGF4ASFBCtrlTurn = LCESP_s16IInter5Point((int16_t)eng_rpm, 1000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_R1, 
	                                                                 2000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_R2,     
	                                                                 3000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_R3,     
	                                                                 4000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_R4,     
	                                                                 5000, (int16_t)U8TCSCpPgFac4ASFBCtlTurn_R5); 
}

static void LCTCS_vCoordPGain4FedBckCtl(void)  
{
	/*---------Front Axle---------*/
	if(FA_TCS.lctcss16BrkCtlErr > 0)  /*Error positive영역*/
	{ 
		FA_TCS.lctcss16BsPG4ASFBCtrl = lctcs16BsPG4ASFBCtlPositiveF; /*Base P*/
	
		/*gear shift시 P gain Factor 감소*/
		if ((lctcsu1GearShiftFlag == 1)&&( AUTO_TM == 0))            /*P Factor*/
		{
			FA_TCS.lctcss16PGF4ASFBCtrl = (int16_t)U8TCSCpReducePgainF4GearShift;
		}
		else
		{
			FA_TCS.lctcss16PGF4ASFBCtrl = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, FA_TCS.lctcss16PGF4ASFBCtrlSt,    
	    	                                              						100, FA_TCS.lctcss16PGF4ASFBCtrlTurn);
		}

	}
	else     /*Error negative영역*/
	{
		FA_TCS.lctcss16BsPG4ASFBCtrl = lctcs16BsPG4ASFBCtlNegativeF;
		FA_TCS.lctcss16PGF4ASFBCtrl = TCSPgainFactor100pcnt ; /*Error negative영역에서는 RPM에 따른 P Factor 적용 하지 않음*/
	}
		
	/*---------Rear Axle---------*/
	if(RA_TCS.lctcss16BrkCtlErr > 0)  /*Error positive영역*/
	{		
		RA_TCS.lctcss16BsPG4ASFBCtrl = lctcs16BsPG4ASFBCtlPositiveR;
	
		/*gear shift시 P gain Factor 감소*/
		if ((lctcsu1GearShiftFlag == 1)&&( AUTO_TM == 0) )
	{		
			RA_TCS.lctcss16PGF4ASFBCtrl = (int16_t)U8TCSCpReducePgainF4GearShift;
	}
	else
	{
			RA_TCS.lctcss16PGF4ASFBCtrl = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, RA_TCS.lctcss16PGF4ASFBCtrlSt,    
	    	                                              						100, RA_TCS.lctcss16PGF4ASFBCtrlTurn);
	}
	}
	else     /*Error negative영역*/
	{
		RA_TCS.lctcss16BsPG4ASFBCtrl = lctcs16BsPG4ASFBCtlNegativeR;
		RA_TCS.lctcss16PGF4ASFBCtrl = TCSPgainFactor100pcnt ; /*Error negative영역에서는 RPM에 따른 P Factor 적용 하지 않음*/
	}		
	
	
	FA_TCS.lctcss16PG4ASFBCtrl = (int16_t)((((int32_t)FA_TCS.lctcss16BsPG4ASFBCtrl)*FA_TCS.lctcss16PGF4ASFBCtrl)/100);
	RA_TCS.lctcss16PG4ASFBCtrl = (int16_t)((((int32_t)RA_TCS.lctcss16BsPG4ASFBCtrl)*RA_TCS.lctcss16PGF4ASFBCtrl)/100);
}
	

static void LCTCS_vCalBaseStIGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R) 
	{
		lctcs16BsStIG4ASFBCtlFront = (int16_t)U8TCSCpBsStIG4ASFBCtl_F1;
		lctcs16BsStIG4ASFBCtlRear =  (int16_t)U8TCSCpBsStIG4ASFBCtl_R1;  
	}
	else
	{
		lctcs16BsStIG4ASFBCtlFront = LCESP_s16IInter5Point(gear_pos,  (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStIG4ASFBCtl_F1,  
	                                                                  (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStIG4ASFBCtl_F2,  
	                                                                  (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStIG4ASFBCtl_F3,  
	                                                                  (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStIG4ASFBCtl_F4,  
	                                                                  (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStIG4ASFBCtl_F5); 
	   
    	lctcs16BsStIG4ASFBCtlRear = LCESP_s16IInter5Point(gear_pos,   (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsStIG4ASFBCtl_R1, 	
	                                                                  (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsStIG4ASFBCtl_R2, 
	                                                                  (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsStIG4ASFBCtl_R3, 
	                                                                  (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsStIG4ASFBCtl_R4, 
	                                                                  (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsStIG4ASFBCtl_R5);
	}          
}

static void LCTCS_vCalBaseHillIGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R) 
	{
		lctcs16BsHillIG4ASFBCtlFront = (int16_t)U8TCSCpBsHillIG4ASFBCtl_F1;
	    lctcs16BsHillIG4ASFBCtlRear =  (int16_t)U8TCSCpBsHillIG4ASFBCtl_R1;  
	}
	else
	{
		lctcs16BsHillIG4ASFBCtlFront = LCESP_s16IInter5Point(gear_pos,  (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillIG4ASFBCtl_F1,  
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillIG4ASFBCtl_F2,  
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillIG4ASFBCtl_F3,  
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillIG4ASFBCtl_F4,  
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillIG4ASFBCtl_F5); 
	   
		lctcs16BsHillIG4ASFBCtlRear = LCESP_s16IInter5Point(gear_pos,   (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsHillIG4ASFBCtl_R1, 	
	                                                                    (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsHillIG4ASFBCtl_R2, 
	                                                                    (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsHillIG4ASFBCtl_R3, 
	                                                                    (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsHillIG4ASFBCtl_R4, 
	                                                                    (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsHillIG4ASFBCtl_R5);
	}                                                                 
}

static void LCTCS_vCalBaseTurnIGainByGear(void)
{
	if (gear_pos == GEAR_POS_TM_R) 
	{
		lctcs16BsTurnIG4ASFBCtlFront = (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F1;
		lctcs16BsTurnIG4ASFBCtlRear =  (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R1;  
	}
	else
	{
		lctcs16BsTurnIG4ASFBCtlFront = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F1,  
	                                                                   (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F2,  
	                                                                   (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F3,  
	                                                                   (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F4,  
	                                                                   (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_F5); 
	   
		lctcs16BsTurnIG4ASFBCtlRear = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R1, 	
	                                                                  (int16_t)GEAR_POS_TM_2, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R2, 
	                                                                  (int16_t)GEAR_POS_TM_3, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R3, 
	                                                                  (int16_t)GEAR_POS_TM_4, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R4, 
	                                                                  (int16_t)GEAR_POS_TM_5, (int16_t)U8TCSCpBsTurnIG4ASFBCtl_R5);
	}          
}

static void LCTCS_vCoordBaseIGainByGear(void)
{
    if (lctcsu1DctSplitHillFlg==0)
    {
		lctcs16BsIG4ASFBCtlFront = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStIG4ASFBCtlFront,
                                       										 100, lctcs16BsTurnIG4ASFBCtlFront);
	      
        lctcs16BsIG4ASFBCtlRear =  LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcs16BsStIG4ASFBCtlRear,
                                                         					 100, lctcs16BsTurnIG4ASFBCtlRear);                                             										  
    }                                                 										  
    else
    {                                                 										  
		lctcs16BsIG4ASFBCtlFront = lctcs16BsHillIG4ASFBCtlFront;                                                     										  
        lctcs16BsIG4ASFBCtlRear = lctcs16BsHillIG4ASFBCtlRear;
    }
}

static void LCTCS_vCalIGainFactInSt(void)
{
	FA_TCS.lctcss16IGF4ASFBCtrlSt = LCESP_s16IInter3Point(FA_TCS.lctcss16BrkCtlErr, -VREF_5_KPH, (int16_t)U8TCSCpIgainEffPctStN_F, /*작게 적용*/ 
                                                                                 0, 100, 
                                                                       VREF_10_KPH, (int16_t)U8TCSCpIgainEffPctStP_F); /*크게 적용*/
                                                                                   
	RA_TCS.lctcss16IGF4ASFBCtrlSt = LCESP_s16IInter3Point(RA_TCS.lctcss16BrkCtlErr, -VREF_5_KPH, (int16_t)U8TCSCpIgainEffPctStN_R, 
                                                                                 0, 100, 
                                                                       VREF_10_KPH, (int16_t)U8TCSCpIgainEffPctStP_R);                                                                                    
}

static void LCTCS_vCalIGainFactInTurn(void)
{
	FA_TCS.lctcss16IGF4ASFBCtrlTurn = LCESP_s16IInter3Point(FA_TCS.lctcss16BrkCtlErr, -VREF_5_KPH, (int16_t)U8TCSCpIgainEffPctTurnN_F, /*작게 적용*/ 
                                                                                   0, 100, 
                                                                         VREF_10_KPH, (int16_t)U8TCSCpIgainEffPctTurnP_F); /*크게 적용*/
                                                                                   
    RA_TCS.lctcss16IGF4ASFBCtrlTurn = LCESP_s16IInter3Point(RA_TCS.lctcss16BrkCtlErr, -VREF_5_KPH, (int16_t)U8TCSCpIgainEffPctTurnN_R, 
                                                                                   0, 100, 
                                                                         VREF_10_KPH, (int16_t)U8TCSCpIgainEffPctTurnP_R);                                                                                    
}

static void LCTCS_vCoordIGain4FedBckCtl(void)
{
	FA_TCS.lctcss16IGF4ASFBCtrl = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, FA_TCS.lctcss16IGF4ASFBCtrlSt,
                                   										    100, FA_TCS.lctcss16IGF4ASFBCtrlTurn);
	
	RA_TCS.lctcss16IGF4ASFBCtrl = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, RA_TCS.lctcss16IGF4ASFBCtrlSt,
                                    										100, RA_TCS.lctcss16IGF4ASFBCtrlTurn);
                                                     										  
    FA_TCS.lctcss16BsIG4ASFBCtrl = lctcs16BsIG4ASFBCtlFront;     
	RA_TCS.lctcss16BsIG4ASFBCtrl = lctcs16BsIG4ASFBCtlRear;	      

	FA_TCS.lctcss16IG4ASFBCtrl = (int16_t)((((int32_t)FA_TCS.lctcss16BsIG4ASFBCtrl)*FA_TCS.lctcss16IGF4ASFBCtrl)/100);	
	RA_TCS.lctcss16IG4ASFBCtrl = (int16_t)((((int32_t)RA_TCS.lctcss16BsIG4ASFBCtrl)*RA_TCS.lctcss16IGF4ASFBCtrl)/100);	
}

static void LCTCS_vDecBsTarBrkInESCBrk(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	/*Decrease Base Target Brake Torque in ESC Brake Same Axle, Cross Wheel*/
	if ((WL_LEFT->lctcsu1BrkTqFactorModeInESC == 1) || (WL_RIGHT->lctcsu1BrkTqFactorModeInESC == 1))
	{
		Axle_TCS->lctcss16BsTBrkTorq4Asym = (int16_t)(((int32_t)Axle_TCS->lctcss16BsTBrkTorq4Asym*U8TCSCpDecBsTarBrkF4ESCBrkCtl)/100);
	}
	else
	{
		;
	}	
}

static void LCTCS_vDecBsTarBrkInFadOut(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1)
	{
		if (((Axle_TCS->lctcsu1SymSpinDctFlg==0)&&(Axle_TCS->lctcss16BsTarBrkTorqAXLEdiff <= U8TCSFadeOutBrkCtrlMinTorq))
			||(Axle_TCS->lctcsu1CtrlWLSpdCross == 1)   /*Fade-Out중 Wheel 역전시, 제어량 Reset*/
			)
		{
			Axle_TCS->lctcss16BsTBrkTorq4FadOut = 0;
		}
		else
		{
			/* 직진시 1scan 당 Fade out 제어량 */
			Axle_TCS->lctcss16FadOutCtrlDeTqST = LCESP_s16IInter2Point(Axle_TCS->lctcss16TarAxlePres,  S16TCSCpFadeOutCtrlLowPreSTTh,  S16TCSCpFadeOutDecTQ4LowPreST, 
								   													     		       S16TCSCpFadeOutCtrlHghPreSTTh, S16TCSCpFadeOutDecTQ4HghPreST  );
			Axle_TCS->lctcss16FadOutCtrlDeTqST = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16FadOutCtrlDeTqST, 1);	
			
			/* 선회 1scan 당 Fade out 제어량*/
			Axle_TCS->lctcss16FadOutCtrlDeTqTurn = LCESP_s16IInter2Point(Axle_TCS->lctcss16TarAxlePres,  S16TCSCpFadeOutCtrlLowPreTunTh,  S16TCSCpFadeOutDecTQ4LowPreTurn, 
								   													       				 S16TCSCpFadeOutCtrlHghPreTunTh, S16TCSCpFadeOutDecTQ4HghPreTurn  ); 
			Axle_TCS->lctcss16FadOutCtrlDeTqTurn = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16FadOutCtrlDeTqTurn, 1);	
			
			/*선회 경향에 따른 제어량 적용*/ 
			Axle_TCS->lctcss16FadOutCtrlDeTq = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, Axle_TCS->lctcss16FadOutCtrlDeTqST,
                                     				               	              100, Axle_TCS->lctcss16FadOutCtrlDeTqTurn);
			
			Axle_TCS->lctcss16BsTBrkTorq4FadOut = Axle_TCS->lctcss16MemBsTBrkTorq4FadOut - (Axle_TCS->lctcss16FadOutCtrlDeTq * Axle_TCS->lctcss16FadeOutCtrlCnt);
		}
		
		Axle_TCS->lctcss16BsTBrkTorq4FadOut = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16BsTBrkTorq4FadOut, 0);			

	}
	else
	{
		Axle_TCS->lctcss16BsTBrkTorq4FadOut = 0;
	}
}



static void LCTCS_vDecBsTarBrkInSpCase(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if(Axle_TCS->lctcsu1BrkCtlActAxle == 1)
	{
		if(ENG_STALL==1)
		{
			if(ENG_STALL_OLD == 0) /* ENG_STALL 0->1 순간 현재 토크 제어값 저장하고 Torq 감소 */
			{
				Axle_TCS->lctcss16MemBsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4Asym;
				
				Axle_TCS->lctcss16BsTBrkTorq4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16BsTBrkTorq4AsymOld) * U8TCSCpBrkTorqDecPcntOfEngStl)/100);
			}
			else
    		{
    			Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4AsymOld - U8TCSCpBrkTorqDecRateOfEngStl;
    		}
		}
		else	
		{
			if(ENG_STALL_OLD == 1)  /*ENG_STALL 1->0 으로 변화 하는 순간.  저장했던 토크 제어값의 T/P %만큼 복구*/
			{
				Axle_TCS->lctcss16BsTBrkTorq4Asym = (int16_t)((((int32_t)Axle_TCS->lctcss16MemBsTBrkTorq4Asym)*U8TCSCpBrkTorqRecPcntOfEngStl)/100);
			
				Axle_TCS->lctcss16MemBsTBrkTorq4Asym = 0;
			}
			else  /*ENG_STALL Faling edge 아니고 ENG_STALL=0 인 구간*/
			{
				if(Axle_TCS ->lctcsu1OverBrkAxle==1) /*OverBrake인 경우*/
				{
					if(Axle_TCS ->lctcsu1OverBrkAxleOld==0)
					{
						Axle_TCS->lctcss16BsTBrkTorq4Asym = (int16_t)(((int32_t)Axle_TCS->lctcss16BsTBrkTorq4AsymOld*U8TCSCpBrkTorqDecPcntOfOvBrk)/100);
					}
					else
					{
						Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4AsymOld - S16TCSCpBrkTorqDecRateOfOvBrk;
					}
				}
				else 
				{
					if(Axle_TCS->lctcsu8StatWhlOnHghMu==TCSHighMuUnStable) /*High Side Mu Spin 발생*/
					{
						if(Axle_TCS->lctcsu1Any2HghMuWhlUnstb==1)
						{
							Axle_TCS->lctcss16MemBsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4AsymOld;
							if (lctcsu1DctSplitHillFlg==0)
							{
							Axle_TCS->lctcss16BsTBrkTorq4Asym = (Axle_TCS->lctcss16BsTBrkTorq4AsymOld - (int16_t)S16TCSCpBrkTorq1stDecTrqOfHWIS);
						}	
						else
						{
								Axle_TCS->lctcss16BsTBrkTorq4Asym = (Axle_TCS->lctcss16BsTBrkTorq4AsymOld - (int16_t)S16TCSCpBrkTq1stDecTrqOfHWISHill);
							}
						}	
						else
						{
							if(Axle_TCS->lctcsu1FastDecPresInHghUstb == 1)
							{/*HSS pattern 제어 임시 back-up 구조*/
								Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4AsymOld - (int16_t)U8TCSCpBrkTorqFastDecRateOfHWIS;
							}
							else
							{
								Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4AsymOld - (int16_t)U8TCSCpBrkTorqDecRateOfHWIS;
							}
						}
						Axle_TCS->lctcsu1FlgOfHSSRecvry = 0;
					}
					else
                    {
                    	if( (Axle_TCS->lctcsu1HghMuWhlUnstb2Any==1)&&(Axle_TCS->lctcsu1LowSpinofCtrlAxle == 0) )
                    	{
                    	    Axle_TCS->lctcsu1FlgOfHSSRecvry = 1;
                    	   /*HSS 후 2 wheel의 spin 차이가 클경우 recovery level로 압력 빠르게 복귀 */
                    		Axle_TCS->lctcss16BsTBrkTorq4Asym =(int16_t)((((int32_t)Axle_TCS->lctcss16MemBsTBrkTorq4Asym)*U8TCSCpBrkTorqRecPcntOfHWIS)/100);
                    		Axle_TCS->lctcss16MemBsTBrkRecTrq4HSSEnd = Axle_TCS->lctcss16BsTBrkTorq4Asym;
                    		Axle_TCS->lctcss16MemBsTBrkTorq4Asym = 0;
                    	}
                    	else if ( (Axle_TCS->lctcsu1HghMuWhlUnstb2Any==1)&&(Axle_TCS->lctcsu1LowSpinofCtrlAxle == 1) 
                    		    && (Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1) )
                    	{
                    		Axle_TCS->lctcss16MemBsTBrkTorq4FadOut = LCTCS_s16IFindMinimum( Axle_TCS->lctcss16BsTBrkTorq4Asym , Axle_TCS->lctcss16BsTBrkTorq4FadOut ) ;
                    		Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16MemBsTBrkTorq4FadOut;
                    	}
                    	else
                    	{
                    		if (Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1)
                    		{
                    			Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16BsTBrkTorq4FadOut;
                    		}
                    		else
                    		{
                    			/*HSS After Control*/
                    			if ((Axle_TCS->lctcsu1FlgOfAfterHghMuUnstb==1) && (Axle_TCS->lctcsu1FlgOfHSSRecvry==1))  /*HSS Recovery 한 경우는 과도한 압력 상승을 유예하기 위한 HSS After 로 천천히 압력제어*/
                    			{	
                    				tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U8CpAfterHghMuUnstbCntTh, 1);
                    				Axle_TCS->lctcss16BsTBrkTorq4Asym = Axle_TCS->lctcss16MemBsTBrkRecTrq4HSSEnd + (int16_t)(((int32_t)Axle_TCS->lctcsu16CntAfterHghMuUnstb*(Axle_TCS->lctcss16FBBrkTorq4Asym - Axle_TCS->lctcss16MemBsTBrkRecTrq4HSSEnd))/tcs_tempW0    );
                    			}
                    			else
                    			{			
                    				;
                    			}	
                    		}
                    	}
                    }
				}
				
			}
		}
	}
	else
	{
		Axle_TCS->lctcsu1FlgOfHSSRecvry = 0;
		Axle_TCS->lctcss16BsTBrkTorq4Asym = 0;
		Axle_TCS->lctcss16MemBsTBrkTorq4Asym = 0;
		Axle_TCS->lctcss16MemBsTBrkRecTrq4HSSEnd = 0;
	}
	Axle_TCS->lctcss16BsTBrkTorq4Asym = LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16BsTBrkTorq4Asym, 0);
}

static void LCTCS_vDecBsTarBrkInVCA(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (VCA_ON_TCMF == 1)  /* 4WD Vref Correction Aid mode */
	{
		Axle_TCS->lctcss16BsTBrkTorq4VCA = (int16_t)S16TCSCpBrkCtlTrqOfVCA ;
	}
	else
	{
		;
	}
}

/*
static void LCTCS_vDecBsTarBrkInStuck(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if (TCS_STUCK_ON == 1)
	{	  	
		if((WL_LEFT->lctcsu1BTCSWhlAtv==1)||(WL_RIGHT->lctcsu1BTCSWhlAtv==1))
		{	
				Axle_TCS->lctcss16BsTBrkTorq4Stuck = (int16_t)S16TCSCpBrkCtlTrqOfStuck ;
		}
		else
		{
			  Axle_TCS->lctcss16BsTBrkTorq4Stuck = 0	;
		}
	}	
	else
	{
		;
	}
}
*/

static void LCTCS_vDecBsTarBrkInVIB(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	#if((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))
	if(((WL_LEFT->lctcsu1VIBFlag_tcmf==1)&&(WL_LEFT->lctcsu1BTCSWhlAtv==1)&&(WL_RIGHT->lctcsu1BTCSWhlAtv==0))
		||((WL_RIGHT->lctcsu1VIBFlag_tcmf==1)&&(WL_LEFT->lctcsu1BTCSWhlAtv==0)&&(WL_RIGHT->lctcsu1BTCSWhlAtv==1))) /*AWD*/
	#else
	if((((WL_LEFT->lctcsu1VIBFlag_tcmf==1)&&(WL_LEFT->lctcsu1BTCSWhlAtv==1)&&(WL_RIGHT->lctcsu1BTCSWhlAtv==0))
			||((WL_RIGHT->lctcsu1VIBFlag_tcmf==1)&&(WL_LEFT->lctcsu1BTCSWhlAtv==0)&&(WL_RIGHT->lctcsu1BTCSWhlAtv==1)))
		&&((lctcsu1DctSplitHillFlg==0)||((lctcsu1DctSplitHillFlg==1)&&(U8TCSHillVibrationCTRL==1)))  /*2WD Flat Only,  hill option*/
		)
	#endif
	{	 
		Axle_TCS->lctcss16BsTBrkTorq4AsymVib = Axle_TCS->lctcss16FFBrkTorq4AsymVib + Axle_TCS->lctcss16IeffOnBsTBrkTorq4Asym ;  
		Axle_TCS->lctcss16BsTBrkTorq4Asym = LCTCS_s16IFindMinimum( Axle_TCS->lctcss16BsTBrkTorq4Asym , Axle_TCS->lctcss16BsTBrkTorq4AsymVib ) ;	
	}
	else
	{
		;
	}
	
}

static void LCTCS_vCalWhlTarBrkTorq4ASCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if((WL_LEFT->lctcsu1BTCSWhlAtv==1)&& (WL_RIGHT->lctcsu1BTCSWhlAtv==0))
	{
		WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Asym;
		WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
	}
	else if ((WL_LEFT->lctcsu1BTCSWhlAtv==0)&& (WL_RIGHT->lctcsu1BTCSWhlAtv==1))
	{
		WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
		WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Asym;
	}
	else if ((WL_LEFT->lctcsu1BTCSWhlAtv==1)&& (WL_RIGHT->lctcsu1BTCSWhlAtv==1))
	{
		if( Axle_TCS->ltcss16WhlSpinDiff>0)
		{
			WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Asym;
			WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
		}
		else if ( Axle_TCS->ltcss16WhlSpinDiff<0)
		{
			WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
			WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Asym;
		}
		else
		{
			WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
			WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
		}	
	}
	else
	{
		WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
		WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl = 0;
	}
}

static void LCTCS_vCalBsTarBrkTorq4SymSCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *W_L, struct W_STRUCT *W_R)
{
	Axle_TCS->lctcss16BsTBrkTorq4SymOld = Axle_TCS->lctcss16BsTBrkTorq4Sym;
	
    if	( (W_L->BTCS == 1) && (W_R->BTCS == 1)&& (Axle_TCS->lctcsu1SymSpinDctFlg==1) )
    {
		tcs_tempW0 = lctcss16SymSpnCtlTh - U8TCSCpSymSpFadeOutHystSpd;
		if	(ENG_STALL==1)
		{
			Axle_TCS->lctcss16BsTBrkTorq4Sym = Axle_TCS->lctcss16BsTBrkTorq4SymOld - (int16_t)U8TCSCpTBrkTorqDec4SymInStall;
		}
		else if (Axle_TCS->lctcss16WhlSpinAxleAvg < tcs_tempW0)
		{
			Axle_TCS->lctcss16BsTBrkTorq4Sym = Axle_TCS->lctcss16BsTBrkTorq4SymOld - (int16_t)U8TCSCpTBrkTorqDec4Sym;	
	    }
	    else
	    {	
			if (Axle_TCS->ltcsu1RearAxle==0)
			{	/* Front Axle */
				Axle_TCS->lctcss16BsTBrkTorq4Sym = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1, (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_F1,
				 	   					                                           (int16_t)GEAR_POS_TM_2, (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_F2,
														                           (int16_t)GEAR_POS_TM_3, (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_F3,
										  				                           (int16_t)GEAR_POS_TM_4, (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_F4,
										   				                           (int16_t)GEAR_POS_TM_5, (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_F5);
																							   				                                     
			}
			else
			{   /* Rear Axle */         	                                                                   
	        	Axle_TCS->lctcss16BsTBrkTorq4Sym = LCESP_s16IInter5Point(gear_pos, (int16_t)GEAR_POS_TM_1,  (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_R1,
	            		 	                                                       (int16_t)GEAR_POS_TM_2,  (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_R2,
	        				 	                                                   (int16_t)GEAR_POS_TM_3,  (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_R3,
	        				 	                                                   (int16_t)GEAR_POS_TM_4,  (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_R4,
                       	                                                           (int16_t)GEAR_POS_TM_5,  (int16_t)S16TCSCpBsTBrkTorq4SymWhlSp_R5);                                                                                                  
			}
		}
		Axle_TCS->lctcss16BsTBrkTorq4Sym = 	LCTCS_s16ILimitMinimum(Axle_TCS->lctcss16BsTBrkTorq4Sym, 0);	 
    }
    else
    {
    	Axle_TCS->lctcss16BsTBrkTorq4Sym = 0;
    }
        WL_LEFT->lctcss16BsTarBrkTorq4SymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Sym;
        WL_RIGHT->lctcss16BsTarBrkTorq4SymSCtl = Axle_TCS->lctcss16BsTBrkTorq4Sym;
}

static void LCTCS_vCalWhlTarBrkTorq4VCACtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if (VCA_ON_TCMF==1)    /*VCA_ON */
	{
		if((WL_LEFT->lctcsu1BTCSWhlAtv==1)&& (WL_RIGHT->lctcsu1BTCSWhlAtv==0))
		{
			WL_LEFT->lctcss16BsTarBrkTorq4VCACtl = Axle_TCS->lctcss16BsTBrkTorq4VCA;
			WL_RIGHT->lctcss16BsTarBrkTorq4VCACtl = 0;
		}
		else if ((WL_LEFT->lctcsu1BTCSWhlAtv==0)&& (WL_RIGHT->lctcsu1BTCSWhlAtv==1))
		{
			WL_LEFT->lctcss16BsTarBrkTorq4VCACtl = 0;
			WL_RIGHT->lctcss16BsTarBrkTorq4VCACtl = Axle_TCS->lctcss16BsTBrkTorq4VCA;
		}
		else
		{
			WL_LEFT->lctcss16BsTarBrkTorq4VCACtl = 0;
			WL_RIGHT->lctcss16BsTarBrkTorq4VCACtl = 0;
		}
	}
	else
	{
		WL_LEFT->lctcss16BsTarBrkTorq4VCACtl = 0;
		WL_RIGHT->lctcss16BsTarBrkTorq4VCACtl = 0;
	}
}

//static void LCTCS_vCalWhlTarBrkTorq4StuckCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
//{
//	if (TCS_STUCK_ON == 1)
//	{
//		if (Axle_TCS->lctcsu8StuckControlLeftStart==0) /*Stuck 제어 시작함 1st cycle만 판단 이후 진입X */
//		{	
//	  		if((WL_L->ltcss16MovingAveragedWhlSpd - WL_R->ltcss16MovingAveragedWhlSpd)>0)
//	  	    {
//	   	    	Axle_TCS->lctcsu8StuckControlLeftStart = 1; /*왼쪽제어로 시작*/
//	  	    }
//	  	    else
//	  	    {
//	  	  		Axle_TCS->lctcsu8StuckControlLeftStart = 2; /*오른쪽제어로 시작*/
//	  	    }
//	    }
//		else
//	    {
//			;
//		}    
//	  
//		if(Axle_TCS->lctcsu8StuckControlLeftStart == 1)
//		{
//	      /*Stuck 제어 패턴 왼쪽 바퀴 부터 시작*/
//	    	if(Axle_TCS->lctcss16StuckControlCnt < (int16_t)U8TCSCpStuckPressureRiseTime)
//	        {
//				WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl = Axle_TCS->lctcss16BsTBrkTorq4Stuck;
//				WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl = 0;
//			}
//			else 
//			{
//    	    	WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl = 0;
//    	      	WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl = Axle_TCS->lctcss16BsTBrkTorq4Stuck;
//    	    }
//    	}
//    	else 
//    	{  
//    		/*Stuck 제어 패턴 시작 우측 바퀴 부터 */
//            if(Axle_TCS->lctcss16StuckControlCnt < (int16_t)U8TCSCpStuckPressureRiseTime)           
//            {                                                                               
//            	WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl = 0; 
//            	WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl = Axle_TCS->lctcss16BsTBrkTorq4Stuck;                                 
//            }                                                                               
//            else                                                                            
//            {                                                                               
//            	WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl = Axle_TCS->lctcss16BsTBrkTorq4Stuck;                                  
//            	WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl = 0;
//            }                                                                                                                                                              
//    	}
//    	  
//    	if (Axle_TCS->lctcss16StuckControlCnt == (((int16_t)U8TCSCpStuckPressureRiseTime*2)-1)) 
//        {                                                                               
//        	Axle_TCS->lctcss16StuckControlCnt = 0;                                      
//        }                                                                               
//        else                                                                            
//        {                                                                               
//        	Axle_TCS->lctcss16StuckControlCnt = Axle_TCS->lctcss16StuckControlCnt + 1;  
//        }	
//    }
//    else
//    {
//    	WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl = 0;
//        WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl = 0;
//        Axle_TCS->lctcsu8StuckControlLeftStart = 0;
//    }
//}

static void LCTCS_vCordBsTarBrkTorq(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	/*우선순위 4  Vehicle speed lctcss16EstCirPreslctcss16EstCirPreslctcss16EstCirPresimation aid
	  우선순위 3  Front and rear axle speed difference controller
	              추후 구현 예정 */
	
	if (VCA_ON_TCMF==1)    /*VCA_ON */
	{
		WL_LEFT->lctcss16BsTarBrkTorq = WL_LEFT->lctcss16BsTarBrkTorq4VCACtl;
		WL_RIGHT->lctcss16BsTarBrkTorq = WL_RIGHT->lctcss16BsTarBrkTorq4VCACtl;
	}
//	else if (TCS_STUCK_ON==1)  /*검증 후 Enable예정*/
//	{
//		WL_LEFT->lctcss16BsTarBrkTorq = WL_LEFT->lctcss16BsTarBrkTrq4StuckCtl;
//		WL_RIGHT->lctcss16BsTarBrkTorq = WL_RIGHT->lctcss16BsTarBrkTrq4StuckCtl;
//	}
	else if(Axle_TCS->lctcsu1SymSpinDctFlg==0)  /*우선순위 1  Asymmetric spin controller*/
	{
		WL_LEFT->lctcss16BsTarBrkTorq = WL_LEFT->lctcss16BsTarBrkTorq4AsymSCtl;
		WL_RIGHT->lctcss16BsTarBrkTorq = WL_RIGHT->lctcss16BsTarBrkTorq4AsymSCtl;
	}
	else                         /*우선순위 2 Symmetric spin controller*/
	{
		WL_LEFT->lctcss16BsTarBrkTorq = WL_LEFT->lctcss16BsTarBrkTorq4SymSCtl; 
		WL_RIGHT->lctcss16BsTarBrkTorq = WL_RIGHT->lctcss16BsTarBrkTorq4SymSCtl;
	}
}

static void LCTCS_vCovBrkTrq2WhlPres (void)
{
	/* 튜닝 파라미터는      U8TCSCpSclFctTrq2PresFrt=2×μ_(b,front)×A_front×r_(b,front)×g;*/
	/*엔지니어가 계산 후 반영 U8TCSCpSclFctTrq2PresRer=2×μ_(b,rear)×A_rear×r_(b,rear)×g;*/
   
	tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U8TCSCpSclFctTrq2PresFrt, 1);
	tcs_tempW1 = LCTCS_s16ILimitMinimum((int16_t)U8TCSCpSclFctTrq2PresRer, 1);
	
	FL_TCS.lctcss16TarWhlPre =  ((FL_TCS.lctcss16BsTarBrkTorq*10)/(tcs_tempW0));
	FR_TCS.lctcss16TarWhlPre =  ((FR_TCS.lctcss16BsTarBrkTorq*10)/(tcs_tempW0));
	RL_TCS.lctcss16TarWhlPre =  ((RL_TCS.lctcss16BsTarBrkTorq*10)/(tcs_tempW1));
	RR_TCS.lctcss16TarWhlPre =  ((RR_TCS.lctcss16BsTarBrkTorq*10)/(tcs_tempW1));
}

static void LCTCS_vCalTarAxlePres(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if 		( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==0) )
	{
		Axle_TCS->lctcss16TarAxlePres = WL_LEFT->lctcss16TarWhlPre;
	}
	else if ( (WL_LEFT->lctcsu1BTCSWhlAtv==0) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		Axle_TCS->lctcss16TarAxlePres = WL_RIGHT->lctcss16TarWhlPre;
	}
	else if ( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		Axle_TCS->lctcss16TarAxlePres = LCTCS_s16IFindMaximum(WL_LEFT->lctcss16TarWhlPre, WL_RIGHT->lctcss16TarWhlPre);
	}
	else
	{
		Axle_TCS->lctcss16TarAxlePres = 0;
	}
}


#if (__SPLIT_TYPE==0)     /* X-SPLIT */   /*TAG2*/
static void LCTCS_vCalTarCirPresXSplit(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_FRONT, struct TCS_WL_STRUCT *WL_REAR)
{
	if		( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==0) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_FRONT->lctcss16TarWhlPre;
	}
	else if	( (WL_FRONT->lctcsu1BTCSWhlAtv==0) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_REAR->lctcss16TarWhlPre;
	}
	else if ( (WL_FRONT->lctcsu1BTCSWhlAtv==1) && (WL_REAR->lctcsu1BTCSWhlAtv==1) )
	{
		/* Rear wheel의 target wheel pressure를 사용하면 Front에 
		과도한 brake torque가 생성 되므로 front wheel의 target wheel pressure를 사용함 */
		CIRCUIT->lctcss16TarCirPres = WL_FRONT->lctcss16TarWhlPre;	
	}
	else
	{
		CIRCUIT->lctcss16TarCirPres = 0;
	}
}
#endif            /*TAG2*/

#if (__SPLIT_TYPE!=0)     /* FR-SPLIT */              /*TAG3*/
static void LCTCS_vCalTarCirPresFRSplit(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if 		( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==0) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_LEFT->lctcss16TarWhlPre;
	}
	else if ( (WL_LEFT->lctcsu1BTCSWhlAtv==0) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		CIRCUIT->lctcss16TarCirPres = WL_RIGHT->lctcss16TarWhlPre;
	}
	else if ( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
	{
		CIRCUIT->lctcss16TarCirPres = LCTCS_s16IFindMaximum(WL_LEFT->lctcss16TarWhlPre, WL_RIGHT->lctcss16TarWhlPre);
	}
	else
	{
		CIRCUIT->lctcss16TarCirPres = 0;
	}
}
#endif                        /*TAG3*/

int16_t LCTCS_vConvPres2CurntOnTC (int16_t Pressure)
{
	int16_t OutputValue;

	OutputValue=LCESP_s16IInter5Point( Pressure,  (int16_t)S16TCSCpWhlPre_1, (int16_t)S16TCSCpTCcrnt_1, 
		                                          (int16_t)S16TCSCpWhlPre_2, (int16_t)S16TCSCpTCcrnt_2,
		                                          (int16_t)S16TCSCpWhlPre_3, (int16_t)S16TCSCpTCcrnt_3,
		                                          (int16_t)S16TCSCpWhlPre_4, (int16_t)S16TCSCpTCcrnt_4,
												  (int16_t)S16TCSCpWhlPre_5, (int16_t)S16TCSCpTCcrnt_5);
	return OutputValue;
}      

int16_t LCTCS_vConvCurntOnTC2Pres (int16_t Current)
{
	int16_t OutputValue;

	OutputValue=LCESP_s16IInter5Point( Current, (int16_t)S16TCSCpTCcrnt_1, (int16_t)S16TCSCpWhlPre_1,  
		                                        (int16_t)S16TCSCpTCcrnt_2, (int16_t)S16TCSCpWhlPre_2, 
		                                        (int16_t)S16TCSCpTCcrnt_3, (int16_t)S16TCSCpWhlPre_3, 
		                                        (int16_t)S16TCSCpTCcrnt_4, (int16_t)S16TCSCpWhlPre_4, 
												(int16_t)S16TCSCpTCcrnt_5, (int16_t)S16TCSCpWhlPre_5) ;
	return OutputValue;
}  

#endif 		/* TAG1 */

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCTCSCallBrakeControl
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
