/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LDABS_vDctRoadSurface.C
* Description: Road surface Detection for ABS control
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSDctRoadSurface
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LDABSDctRoadSurface.h"
#include "LCABSDecideCtrlState.h"

  #if __PAC
#include "LCPACCallControl.h"
  #endif

/* Local Definiton  *********************************************************/



/* Variables Definition*******************************************************/

uint8_t Rough_road_detector_counter;
uint8_t Rough_road_detector_counter2;
uint8_t scan_counter;
uint8_t num;

/* LocalFunction prototype ***************************************************/

static void LDABS_vDetectVehicleRoughRoad(void);
static void LDABS_vSetRoughAradOK(uint8_t DetectorRefF, uint8_t DetectorRefR);
static void LDABS_vDetectWheelRoughness(struct W_STRUCT *WL);
static void LDABS_vDetectWheelRoughRoad(struct W_STRUCT *WL);


/* GlobalFunction prototype ***************************************************/



/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LDABS_vDctRoadSurface
* CALLED BY:          LDABS_vCallDetection()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Wheel Status Detection for ABS control
******************************************************************************/
void LDABS_vDctRoadSurface(void)
{
	LDABS_vDetectVehicleRoughRoad();
}


/******************************************************************************
* FUNCTION NAME:      LDABS_vDetectVehicleRoughRoad
* CALLED BY:          LDABS_vDctRoadSurface()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        This function is used to detect and cope with rough road
******************************************************************************/

/*****************************************************************************/
/*  Modification   Log                                                       */
/*  Date           Author           Description                              */
/*  -------       ----------      ---------------------------                */
/*  02.10.02      JinKoo Lee      Initial Release                            */
/*  03.06.30      JinKoo Lee      Added arad information                     */
/*  06.05.12      JinKoo Lee      Easier rough detection below 20km/h        */
/*****************************************************************************/

static void LDABS_vDetectVehicleRoughRoad(void)
{
    if( vref>=VREF_3_KPH )
    { 
        if(vref>=VREF_60_KPH)
        {
            LDABS_vSetRoughAradOK(12,8);
        }
        else if(vref>=VREF_20_KPH)
        {
            LDABS_vSetRoughAradOK(8,6);
        }
        else{
            LDABS_vSetRoughAradOK(6,4);
        }
        
        LDABS_vDetectWheelRoughness(&FL);
        LDABS_vDetectWheelRoughness(&FR);
        LDABS_vDetectWheelRoughness(&RL);
        LDABS_vDetectWheelRoughness(&RR);
        
        if( scan_counter == 0 )
        {
        	LDABS_vDetectWheelRoughRoad(&FL);
        }
        else if( scan_counter == 1 )
        {
        	LDABS_vDetectWheelRoughRoad(&FR);
        }
        else if( scan_counter == 2 )
        {
        	LDABS_vDetectWheelRoughRoad(&RL);
        }
        else if( scan_counter == 3 ) 
        {
        	LDABS_vDetectWheelRoughRoad(&RR);
        }
        else if( scan_counter == 4 )
        {
            Rough_road_detector_counter=0;
            Rough_road_detector_counter=Rough_road_detector_counter2;
            Rough_road_detector_counter2=0;
            if( num >= 6 )
            { 
            	num=0;
            }
            else 
            {
            	num++;
            }
        }
        else
        {
        	;
        }
        
        if( scan_counter >= 6 )/*9*/
        {
        	scan_counter =0;
        }
        else
        {
            scan_counter++;
        }
                        
        if( Rough_road_detect_vehicle ==0 )
        { 
            if(((Rough_road_detector_counter >= 12)&&( vref >= VREF_20_KPH ))
             ||((Rough_road_detector_counter >= 8 )&&( vref <  VREF_20_KPH )))
            {
                if( Rough_road_arad_OK == 1 )
                {
                    Rough_road_detect_vehicle = 1;
                    Rough_road_suspect_vehicle= 0;
                }
                else
                {
                	Rough_road_suspect_vehicle = 1;
                }
            }
            else
            {
                if( ABS_fz == 0 )
                {
                    if(((Rough_road_detector_counter < 8)&&( vref >= VREF_20_KPH ))
                     ||((Rough_road_detector_counter < 6)&&( vref <  VREF_20_KPH )))
                    {
                    	Rough_road_suspect_vehicle = 0;
                    }
                    else
                    {
                    	;
                    }
                }
                else
                {   
                    if(((Rough_road_detector_counter < 6)&&( vref >= VREF_20_KPH ))
                     ||((Rough_road_detector_counter < 4)&&( vref <  VREF_20_KPH )))
                    {
                    	Rough_road_suspect_vehicle = 0;
                    }
                    else
                    {
                    	;
                    }
                }
            }   
        }   
        else {
            if( ABS_fz == 0 )
            {
                if(((Rough_road_detector_counter < 8)&&( vref >= VREF_20_KPH ))
                 ||((Rough_road_detector_counter < 6)&&( vref <  VREF_20_KPH )))
                {
                	Rough_road_detect_vehicle = 0;
                }
                else
                {
                	;
                }
            }
            else 
            {
                if(((Rough_road_detector_counter < 6)&&( vref >= VREF_20_KPH ))
                 ||((Rough_road_detector_counter < 4)&&( vref <  VREF_20_KPH )))
                {
                	Rough_road_detect_vehicle = 0;
                }
                else
                {
                	;
                }
            }
        }
        
     #if __ROUGH_EBD  /* rough ebd (CT) */
        
        if(((FL.Rough_road_detect_flag1==1) && (RL.Rough_road_detect_flag1==1))||   
           ((FR.Rough_road_detect_flag1==1) && (RR.Rough_road_detect_flag1==1))) 
        {
            if(Rough_road_detect_vehicle_EBD == 0)
            {
                if(Rough_road_detector_counter >= 6)
                {
                    Rough_road_detect_vehicle_EBD = 1;
                }
                else
                {
                	Rough_road_detect_vehicle_EBD = 0;
                }
            }
            else
            {
            	;
            }
        }
        else 
        {
            if(((FL.Rough_road_detect_flag0==0) && (RL.Rough_road_detect_flag0==0))||
               ((FR.Rough_road_detect_flag0==0) && (RR.Rough_road_detect_flag0==0))) 
            {
                Rough_road_detect_vehicle_EBD = 0;
            }
            else
            {
            	;
            }
        }
            
    #endif
    
    }
    else
    {
        Rough_road_detect_vehicle = 0;
        Rough_road_suspect_vehicle = 0;
      #if __ROUGH_EBD
        Rough_road_detect_vehicle_EBD = 0;
      #endif
    }
}


static void LDABS_vSetRoughAradOK(uint8_t DetectorRefF, uint8_t DetectorRefR)
{
    if(( (FL.Rough_road_detector_arad + FR.Rough_road_detector_arad) >= DetectorRefF )&&
       ( (RL.Rough_road_detector_arad + RR.Rough_road_detector_arad) >= DetectorRefR ))
    {
        Rough_road_arad_OK = 1;
    }      
    else
    {  
        Rough_road_arad_OK = 0;
    }
}

static void LDABS_vDetectWheelRoughness(struct W_STRUCT *WL)
{

    int8_t sign1,multi_sign;
    int16_t diff,temp1,temp2,temp3;

    if((WL->arad >= 0)&&(WL->arad > WL->peak_acc_rough))
    {
    	WL->peak_acc_rough = WL->arad;
    }
    else
    {
    	;
    }

    diff = WL->vrad - WL->vrad_old;
    
    if( diff > 0 )
    {
    	sign1 = 1;
    }
    else if( diff < 0 )
    {
    	sign1 = -1;
    }
    else
    {
    	sign1 = 0;
    }
        
    if(sign1 == 0)
    {
    	sign1 = WL->sign_old;
    }
    else
    {
    	;
    }
        
    multi_sign = sign1*(WL->sign_old);
    
    if(WL->diff_max2_counter < U8_TYPE_MAXNUM)
    {
    	WL->diff_max2_counter += 1;     /* TEMP */
    }
    else
    {
    	;
    }
    
    if( multi_sign < 0 )
    {
        if(REAR_WHEEL_wl==0)
        {
            temp1 = (int16_t)S8_Counter_threshold_Upper_Lim_F;
            temp2 = (int16_t)S8_Counter_threshold_Lower_Lim_F;
        }
        else 
        {
            temp1 = (int16_t)S8_Counter_threshold_Upper_Lim_R;
            temp2 = (int16_t)S8_Counter_threshold_Lower_Lim_R;
        }
        
        if( vref>=VREF_20_KPH )
        { 
          #if (__ROUGH_COMP_IMPROVE == ENABLE)
            if(REAR_WHEEL_wl==0)
        	{
            	temp3 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*VREF_1_KPH),((int16_t)U8_ROUGH_SPEED_LEVEL2*VREF_1_KPH),
                	                                  10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2);
            }
            else
            {
            	temp3 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*VREF_1_KPH),((int16_t)U8_ROUGH_SPEED_LEVEL2*VREF_1_KPH),
                	                                  10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2_R);
            }   
           
          #else
            temp3 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*VREF_1_KPH),((int16_t)U8_ROUGH_SPEED_LEVEL2*VREF_1_KPH),
                                                  10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2);
          #endif 
            temp1 = (temp1*temp3)/10 ;
            temp2 = (temp2*temp3)/10 ;                                   
        }
        else
        {
        	if(ABS_fz==0)
            {	temp1 = (temp1*3)/4; /* 75% */
            	temp2 = (temp2*3)/4; /* 75% */
            }
            else
            {
            	;
            }
        }

          #if (__PAC == ENABLE)
        if(lcpacu1PacActiveFlg==1)
        {
        	temp1 = (temp1*3)/2; /* 150% */
        	temp2 = (temp2*3)/2; /* 150% */        	
        }
          #endif        
        
    /****************** threshold limitation *****************/
        if(temp1 < VREF_0_5_KPH)
        {
            temp1 = VREF_0_5_KPH;
        }
        else { }
        
        if(temp2 < VREF_0_5_KPH)
        {
            temp2 = VREF_0_5_KPH;
        }
        else { }
    /*********************************************************/
        
        WL->diff_max2 = (int16_t)McrAbs(WL->vrad - WL->vrad_max_old);       /* TEMP */
        if(WL->diff_max2>VREF_1_KPH)
        {
        	WL->diff_max2=VREF_1_KPH;
        }
        else
        {
        	;
        }
        if(WL->diff_max2_counter > 15)
        {
        	WL->diff_max2=0;
        }
        else
        {
        	;
        }
        WL->diff_max2_counter=0;
        
        WL->Rough_road_counter += 1;       
        WL->Rough_road_counter2 += 1;
        WL->Rough_road_counter3 += 1;
        WL->Rough_road_counter4 += 1;
        WL->Rough_road_counter5 += 1;
        
        WL->diff_max     = WL->vrad_old - WL->vrad_max_old;
        WL->vrad_max_old = WL->vrad_old;
        
        if(WL->sign_old>0)
        {
            if(WL->diff_max < temp1)
            {
                WL->Rough_road_counter -=1;
                WL->Rough_road_counter2 -=1;
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max < VREF_3_KPH)
            {
                WL->Rough_road_counter2 -=1;
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max < VREF_5_KPH)
            {
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max < VREF_10_KPH)
            {
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max < VREF_15_KPH)
            {
                WL->Rough_road_counter5 -=1;
            }
            else
            {
            	;
            }
            
            if( ABS_fz==1 )
            {
                if(REAR_WHEEL_wl==0)
                {
                    if(WL->peak_acc_rough >= ARAD_10G0 )
                    {
                    	WL->Rough_road_counter_arad +=2;
                    }
                    else if(WL->peak_acc_rough >= ARAD_3G0 )
                    {
                    	WL->Rough_road_counter_arad +=1;
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    if(WL->peak_acc_rough >= ARAD_5G0 )
                    {
                    	WL->Rough_road_counter_arad +=2;
                    }
                    else if(WL->peak_acc_rough >= ARAD_3G0 )
                    {
                    	WL->Rough_road_counter_arad +=1;
                    }
                    else
                    {
                    	;
                    }
                }
            }
            else
            {
                if(WL->peak_acc_rough >= ARAD_4G0 )
                {
                	WL->Rough_road_counter_arad +=2;
                }
                else if(WL->peak_acc_rough >= ARAD_2G0 )
                {
                	WL->Rough_road_counter_arad +=1;
                }
                else
                {
                	;
                }
            }
            WL->peak_acc_rough = 0;
        }
        else if( WL->sign_old < 0 )
        {
            if(WL->diff_max > -temp2)
            {
                WL->Rough_road_counter -=1;
                WL->Rough_road_counter2 -=1;
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max > -VREF_3_KPH)
            {
                WL->Rough_road_counter2 -=1;
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max > -VREF_5_KPH)
            {
                WL->Rough_road_counter3 -=1;
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max > -VREF_10_KPH)
            {
                WL->Rough_road_counter4 -=1;
                WL->Rough_road_counter5 -=1;
            }
            else if(WL->diff_max > -VREF_15_KPH)
            {
                WL->Rough_road_counter5 -=1;
            }
            else
            {
            	;
            }
        }
        else
        {
        	;
        }
    }
        
    WL->vrad_old = WL->vrad;
    WL->sign_old = sign1;
}


static void LDABS_vDetectWheelRoughRoad(struct W_STRUCT *WL)
{
    WL->Rough_road_detector  = WL->Rough_road_detector
                             + WL->Rough_road_counter - WL->Rough_road_counter_sum[num];
    WL->Rough_road_detector2 = WL->Rough_road_detector2
                             + WL->Rough_road_counter2 - WL->Rough_road_counter_sum2[num];
    WL->Rough_road_detector3 = WL->Rough_road_detector3
                             + WL->Rough_road_counter3 - WL->Rough_road_counter_sum3[num];
    WL->Rough_road_detector4 = WL->Rough_road_detector4
                             + WL->Rough_road_counter4 - WL->Rough_road_counter_sum4[num];
    WL->Rough_road_detector5 = WL->Rough_road_detector5
                             + WL->Rough_road_counter5 - WL->Rough_road_counter_sum5[num];
    WL->Rough_road_detector_arad = WL->Rough_road_detector_arad
                             + WL->Rough_road_counter_arad - WL->Rough_road_counter_arad_sum[num];                                                                        
            
    WL->Rough_road_counter_sum[num]  = WL->Rough_road_counter;
    WL->Rough_road_counter_sum2[num] = WL->Rough_road_counter2;
    WL->Rough_road_counter_sum3[num] = WL->Rough_road_counter3;
    WL->Rough_road_counter_sum4[num] = WL->Rough_road_counter4;
    WL->Rough_road_counter_sum5[num] = WL->Rough_road_counter5;
    WL->Rough_road_counter_arad_sum[num] = WL->Rough_road_counter_arad;
    
    WL->Rough_road_counter = 0;
    WL->Rough_road_counter2 = 0;
    WL->Rough_road_counter3 = 0;
    WL->Rough_road_counter4 = 0;
    WL->Rough_road_counter5 = 0;
    WL->Rough_road_counter_arad = 0;
    
    if(REAR_WHEEL_wl==0) 
    {
        tempB0 = S8_Rough_road_detector_F_level0;
        tempB1 = S8_Rough_road_detector_F_level1;
        tempB2 = S8_Rough_road_detector_F_level2;
    }
    else 
    {
        tempB0 = S8_Rough_road_detector_R_level0;
        tempB1 = S8_Rough_road_detector_R_level1;
        tempB2 = S8_Rough_road_detector_R_level2;
    }
    
    if(WL->Rough_road_detector >= tempB0) 
    {
        WL->Rough_road_detect_flag0 = 1;
        if(WL->Rough_road_detector >= tempB1) 
        {
            WL->Rough_road_detect_flag1 = 1;
            if(WL->Rough_road_detector >= tempB2) 
            {
                WL->Rough_road_detect_flag2 = 1;
            }
            else
            {
            	WL->Rough_road_detect_flag2 = 0;
            }
        }
        else 
        {
            WL->Rough_road_detect_flag1 = 0;
            WL->Rough_road_detect_flag2 = 0;
        }
    }
    else 
    {
        WL->Rough_road_detect_flag0 = 0;
        WL->Rough_road_detect_flag1 = 0;
        WL->Rough_road_detect_flag2 = 0;
    }
    
    if( vref<=VREF_3_KPH )
    {
        WL->Rough_road_detect_flag0 = 0;
        WL->Rough_road_detect_flag1 = 0;
        WL->Rough_road_detect_flag2 = 0;
    }
    else
    {
    	;
    }
    
    if(WL->Rough_road_detect_flag2==1)
    {
    	Rough_road_detector_counter2+=4;
    }
    else if(WL->Rough_road_detect_flag1==1)
    {
    	Rough_road_detector_counter2+=2;
    }
    else if(WL->Rough_road_detect_flag0==1)
    {
        Rough_road_detector_counter2+=1;
    }
    else
    {
    	;
    }
}
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSDctRoadSurface
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
