/******************************************************************************
* Project Name: Hill Start Assist control
* File: LAHSACallActHW.C
* Description: Valve/Motor actuaction by Hill Start Assist Controller
* Date: June. 4. 2005
******************************************************************************/

/* Includes ******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAHSACallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LAHSACallActHW.h"
#include "LCHSACallControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
#include "LAABSCallMotorSpeedControl.h"
 #if defined(__dDCT_INTERFACE)
#include "LCHRCCallControl.h"
  #endif


#if __HSA
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/
int16_t     MFC_HSA_DEC_CURRENT;
uint8_t     HSA_phase_out_toggle_cnt;
int16_t     hsa_msc_target_vol;
/* LocalFunction prototype ***************************************************/
int16_t LAMFC_s16SetMFCCurrentHSA(int16_t MFC_Current_old);
void	LCMSC_vSetHSATargetVoltage(void);
/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAHSA_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Hill Start Assist Controller
******************************************************************************/
void	LAHSA_vCallActHW(void)
{
	if(HSA_flg==1) {
	  #if (__SPLIT_TYPE==0)	
		TCL_DEMAND_fl = HSA_TCL_DEMAND_fl;
		TCL_DEMAND_fr = HSA_TCL_DEMAND_fr;
		
		if(HSA_Control==HSA_APPLY) {
			S_VALVE_LEFT = HSA_S_VALVE_LEFT;
			S_VALVE_RIGHT = HSA_S_VALVE_RISHT;
			VDC_MOTOR_ON = HSA_MSC_MOTOR_ON;
		}
	  #else
		TCL_DEMAND_SECONDARY = HSA_TCL_DEMAND_fl;
		TCL_DEMAND_PRIMARY = HSA_TCL_DEMAND_fr;
		
		if(HSA_Control==HSA_APPLY) {
			S_VALVE_SECONDARY = HSA_S_VALVE_LEFT;
			S_VALVE_PRIMARY = HSA_S_VALVE_RISHT;
			VDC_MOTOR_ON = HSA_MSC_MOTOR_ON;
		}
	  #endif	
		
		VALVE_ACT = 1;
	}
} 

int16_t LAMFC_s16SetMFCCurrentHSA(int16_t MFC_Current_old)
{
    int16_t MFC_Current_new = 0;
    int16_t MFC_HSA_HOLD_Current = 0;

  #if __HSA
    if(HSA_flg==1)
    {
        MFC_HSA_HOLD_Current = LCESP_s16IInter3Point(HSA_retaining_press, S16_HSA_TC_HOLD_PRESS_MIN,  S16_HSA_TC_HOLD_CUR_MIN,   
                                                                          S16_HSA_TC_HOLD_PRESS_MID,  S16_HSA_TC_HOLD_CUR_MID,
                                                                          S16_HSA_TC_HOLD_PRESS_MAX,  S16_HSA_TC_HOLD_CUR_MAX);

        if(HSA_Control==HSA_HOLD)
        {
        	tempW3 = S16_HSA_INIT_HOLD_CNTR_T/2;
        	tempW4 = MFC_HSA_HOLD_Current + S16_HSA_INIT_CNTR_CUR_MAX_COMP;

        	if(lcs16HsaInitHoldCntrCnt<=S16_HSA_INIT_HOLD_CNTR_T)
        	{
        		if(lcs16HsaInitHoldCntrCnt==1)
        		{
        			tempW6 = LCESP_s16IInter3Point(MFC_HSA_HOLD_Current, S16_HSA_TC_HOLD_CUR_MIN,  S16_HSA_TC_INITIAL_COMP_MIN,   
                                               				             S16_HSA_TC_HOLD_CUR_MID,  S16_HSA_TC_INITIAL_COMP_MID,
                                                  				         S16_HSA_TC_HOLD_CUR_MAX,  S16_HSA_TC_INITIAL_COMP_MAX);
        			
        			MFC_Current_new = MFC_HSA_HOLD_Current + tempW6;
        		}
        		else if(lcs16HsaInitHoldCntrCnt<=tempW3)
				{
					MFC_Current_new = LCESP_s16IInter2Point(lcs16HsaInitHoldCntrCnt, 1, MFC_Current_old,
                        	                                                    tempW3, tempW4);
				}
				else
				{
					MFC_Current_new = LCESP_s16IInter2Point(lcs16HsaInitHoldCntrCnt, tempW3, MFC_Current_old,
                    	                                           S16_HSA_INIT_HOLD_CNTR_T, MFC_HSA_HOLD_Current);
				}
			}
			else if(lcu1HsaReBrk==1)
			{
				MFC_Current_new = S16_HSA_CNTR_MIN_TC_CURRENT;
			}
        	else if(lcs16HsaReBrkReleaseCnt<=S16_HSA_INIT_HOLD_CNTR_T)
        	{
        		if(lcs16HsaReBrkReleaseCnt==1)
        		{
                	tempW6 = LCESP_s16IInter3Point(MFC_HSA_HOLD_Current, S16_HSA_TC_HOLD_CUR_MIN,  S16_HSA_TC_INITIAL_COMP_MIN,   
                                               				             S16_HSA_TC_HOLD_CUR_MID,  S16_HSA_TC_INITIAL_COMP_MID,
                                                  				         S16_HSA_TC_HOLD_CUR_MAX,  S16_HSA_TC_INITIAL_COMP_MAX);
                	
                	MFC_Current_new = MFC_HSA_HOLD_Current + tempW6;
				}
				else if(lcs16HsaReBrkReleaseCnt<=tempW3)
				{
					MFC_Current_new = LCESP_s16IInter2Point(lcs16HsaReBrkReleaseCnt,  1, MFC_Current_old,
                                                                                 tempW3, tempW4);
				}
				else
				{
					MFC_Current_new = LCESP_s16IInter2Point(lcs16HsaReBrkReleaseCnt, tempW3, MFC_Current_old,
                                                                   S16_HSA_INIT_HOLD_CNTR_T, MFC_HSA_HOLD_Current);
				}
			}
			else 
			{
				MFC_Current_new = MFC_HSA_HOLD_Current;
			}	
        			
			if(MFC_Current_new>TC_CURRENT_MAX)
			{
				MFC_Current_new = TC_CURRENT_MAX;
			}
			else { }
        }
        else if(HSA_Control==HSA_RELEASE)
        {
            if(HSA_pressure_release_counter==1)
            {
            	MFC_HSA_DEC_CURRENT = S16_HSA_ISG_1st_Drop_Cur;
            	if(MFC_Current_old>MFC_HSA_HOLD_Current)
            	{
            		MFC_Current_new = MFC_HSA_HOLD_Current - MFC_HSA_DEC_CURRENT;
            	}
            	else
            	{
            		MFC_Current_new = MFC_Current_old - MFC_HSA_DEC_CURRENT;
            	}
            }
            else
            {
                if(HSA_phase_out_rate >= MPRESS_0G2BAR)  
                {
                    if((HSA_phase_out_rate%2)==1)
                    {
                        if(HSA_phase_out_toggle_cnt<=1)
                        {
                            HSA_phase_out_toggle_cnt = HSA_phase_out_toggle_cnt + 1;
                            MFC_HSA_DEC_CURRENT = ((HSA_phase_out_rate*TC_I_5mA)/MPRESS_1BAR);
                        }
                        else
                        {
                            HSA_phase_out_toggle_cnt = HSA_phase_out_toggle_cnt + 1;
                            if(HSA_phase_out_toggle_cnt==4) {HSA_phase_out_toggle_cnt = 0;}
                            MFC_HSA_DEC_CURRENT = (((HSA_phase_out_rate*TC_I_5mA)/MPRESS_1BAR) + 1);
                        }
                    }
                    else
                    {
                        HSA_phase_out_toggle_cnt = 0;
                        MFC_HSA_DEC_CURRENT = ((HSA_phase_out_rate*TC_I_5mA)/MPRESS_1BAR);
                    }

                    if(MFC_HSA_DEC_CURRENT<1) {MFC_HSA_DEC_CURRENT = 1;}
                }
                else if(HSA_phase_out_rate > MPRESS_0BAR)
                {
                    if(HSA_phase_out_toggle_cnt<=1)
                    {
                        HSA_phase_out_toggle_cnt = HSA_phase_out_toggle_cnt + 1;
                        MFC_HSA_DEC_CURRENT = 1;
                    }
                    else
                    {
                        HSA_phase_out_toggle_cnt = HSA_phase_out_toggle_cnt + 1;
                        if(HSA_phase_out_toggle_cnt==4) {HSA_phase_out_toggle_cnt = 0;}
                        MFC_HSA_DEC_CURRENT = 0;
                    }
                }
                else
                {
                    MFC_HSA_DEC_CURRENT = 10; HSA_phase_out_toggle_cnt = 0;
                }
                
                  #if defined(__dDCT_INTERFACE)
				if(lcu1HrcOnReqFlg==1)
				{
					if(VEHICLE_MOVE_WO_EXIT_OK==1)
					{
						if(VEHICLE_MOVE_WO_EXIT_OK_old==0)
						{
							MFC_Current_new = MFC_Current_old + S16_HSA_TC_comp_HRC_transiton;
						}
						else
						{
							MFC_Current_new = MFC_Current_old;
						}
					}
					else
					{
						if(VEHICLE_MOVE_WO_EXIT_OK_old==1)
						{
							MFC_Current_new = MFC_Current_old - S16_HSA_TC_comp_HRC_transiton;
						}
						else 
						{
							MFC_Current_new = MFC_Current_old - (int16_t)MFC_HSA_DEC_CURRENT;
						}
					}
				}
				else
				{
					MFC_Current_new = MFC_Current_old - (int16_t)MFC_HSA_DEC_CURRENT;
				}				  
				  #else
                MFC_Current_new = MFC_Current_old - (int16_t)MFC_HSA_DEC_CURRENT;
                  #endif
            }
            
			if(MFC_Current_new<S16_HSA_CNTR_MIN_TC_CURRENT)
        	{
            	MFC_Current_new = S16_HSA_CNTR_MIN_TC_CURRENT;
        	}
        	else { }
        }
        else if(HSA_Control==HSA_APPLY)
        {
        	MFC_Current_new = S16_HSA_CUR_AT_RISE;
        }
        else
        {
        	MFC_Current_new = 0;
        	MFC_HSA_DEC_CURRENT = 0;
        	HSA_phase_out_toggle_cnt = 0;
        }
    }
    else 
    {
        MFC_Current_new = 0;
        MFC_HSA_DEC_CURRENT = 0;
        HSA_phase_out_toggle_cnt = 0;
    }
  #else
    MFC_Current_new = 0;
  #endif

    return MFC_Current_new;
}

void	LCMSC_vSetHSATargetVoltage(void)
{
	if(HSA_MSC_MOTOR_ON==1)
	{
		hsa_msc_target_vol = (int16_t)(U8_HSA_MOTOR_TARGET_VOLT*100);
	}
	else 
	{
		hsa_msc_target_vol = MSC_0_V;
	}
}
#endif


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAHSACallActHW
	#include "Mdyn_autosar.h"               
#endif 