
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCompThreshold
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LSESPCalSensorOffset.h"
#include "LDESPEstBeta.h"
#include "LCESPCallControl.h"

#if (__MGH60_ESC_IMPROVE_CONCEPT==1)

#define S16_DEL_YAW_THRES_WGT_IN_EC_MM    10


    #define		_OS_TH_CONCEPT_BRK_HOLD             1   /* Braking --> OS_TH holding */
    #define		_OS_TH_CONCEPT_BRK_OFFSET		        0   /* Braking --> OS_TH hysteresis --> Choice (1)  */	 
    #define		_OS_TH_CONCEPT_BRK_H_INI_VALUE		  1   /* Braking --> OS_TH Initial value --> Choice (2) */	

#else 
    #define		_OS_TH_CONCEPT_BRK_HOLD             0
    #define		_OS_TH_CONCEPT_BRK_OFFSET		        0	 
    #define		_OS_TH_CONCEPT_BRK_H_INI_VALUE		  0   /* Braking --> OS_TH Initial value */	    
    
#endif


#if (__BANK_ESC_IMPROVEMENT_CONCEPT ==1)
#define S16_BANK_UNDER_THRES_SIGN 0
#endif

#if __VDC
void LCESP_vCompThreshold(void);
#if	__SIDE_SLIP_ANGLE_MODULE
void LDESP_vCompSideSlipThreshold(void);
#endif

int16_t u_th_add ;
int16_t os_th_hysteresis ;
int16_t os_th_count_01 ; 
int8_t esc_th_ctrl_01; 
int8_t esc_th_ctrl_02; 
int8_t esc_slip_need_monitor_1;  
int8_t esc_slip_need_monitor_2; 
int16_t os_th_braking_ini_value ;

void LCESP_vCompThreshold(void)
{
    u_delta_yaw_thres_old = u_delta_yaw_thres;
   // o_delta_yaw_thres_old = o_delta_yaw_thres;

/*  Dependency on Steeering wheel angle*/

#if (__MGH60_ESC_IMPROVE_CONCEPT==1)  /* 11 SWD. Request by GME Opel */
 
	    thres_base_value=YAW_4DEG;
	    
	    esp_tempW1 = LCESP_s16IInter4Point( vrefk, 0, 600, S16_SPEED_THR_LOW, S16_DEL_YAW_THR_WSTR_LOW 
	                            ,S16_SPEED_THR_MED, S16_DEL_YAW_THR_WSTR_MED 
	                            ,S16_SPEED_THR_HIG, S16_DEL_YAW_THR_WSTR_HIG );
	                            
	    esp_tempW2 = LCESP_s16IInter4Point( vrefk, 0, 600, S16_SPEED_THR_LOW, S16_DEL_YAW_THR_WSTR_LOW_ABS 
	                            ,S16_SPEED_THR_MED, S16_DEL_YAW_THR_WSTR_MED_ABS 
	                            ,S16_SPEED_THR_HIG, S16_DEL_YAW_THR_WSTR_HIG_ABS );
	    
	    /*
	    if ( esp_tempW1 < thres_base_value ) 
	    {
	    	esp_tempW1 = thres_base_value;     
	    }
	    else
      {    
	    	;
	    }
	    */
	   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	    if((ABS_fz==1)&&(lsespu1AHBGEN3MpresBrkOn==1))
	   #else
	    if((ABS_fz==1)&&(MPRESS_BRAKE_ON==1))
	   #endif
	    {
	    	  thres_wstr = LCESP_s16IInter3Point( abs_wstr, S16_WSTR_THR_SMALL, esp_tempW2
	                            ,S16_WSTR_THR_MEDIU, esp_tempW2 
	                            ,S16_WSTR_THR_LARGE, S16_DEL_YAW_THR_WSTR_LARGE_VAL_ABS);
	    }
	    else
	    {
	    	  thres_wstr = LCESP_s16IInter3Point( abs_wstr, S16_WSTR_THR_SMALL, esp_tempW1
	                            ,S16_WSTR_THR_MEDIU, esp_tempW1 
	                            ,S16_WSTR_THR_LARGE, S16_DEL_YAW_THR_WSTR_LARGE_VAL);
	    }
 
      u_th_add = 0;

#else
 
	    thres_base_value=YAW_5DEG;
	    
	    esp_tempW1 = LCESP_s16IInter3Point( vrefk, S16_SPEED_THR_LOW, S16_DEL_YAW_THR_WSTR_LOW 
	                            ,S16_SPEED_THR_MED, S16_DEL_YAW_THR_WSTR_MED 
	                            ,S16_SPEED_THR_HIG, S16_DEL_YAW_THR_WSTR_HIG );
	    if ( esp_tempW1 < thres_base_value ) 
	    {
	    	esp_tempW1 = thres_base_value;     
	    }
	    else
	    {
	    	;
	    }
	                        
	    thres_wstr = LCESP_s16IInter3Point( abs_wstr, S16_WSTR_THR_SMALL, S16_DEL_YAW_THR_WSTR_SMALL_VAL
	                            ,S16_WSTR_THR_MEDIU, esp_tempW1 
	                            ,S16_WSTR_THR_LARGE, S16_DEL_YAW_THR_WSTR_LARGE_VAL);
	                            
        u_th_add = 0;   
#endif

    #if __CAN_SAS_EEP_OFFSET_APPLY == 1

	    if((STEER_STABLE_OK==0)&&(KSTEER.MODEL_OK_AT_15KPH==0))
    	{
    		thres_wstr = thres_wstr +YAW_2DEG ;
	    }
	    else
        {
            ;
        }
	               
    #endif  
	                            
/*	}
	else
	{
		;
	}*/
/*  Dependency on Vehicle speed*/

//#if (__MGH60_ESC_IMPROVE_CONCEPT==1)  /* '11.10 : Cycle time Optimize */

    delta_yaw_middle_thres = thres_wstr;

/*
#else 
   
	    esp_tempUW3 = (uint16_t)(LCESP_s16IInter3Point( vrefk, S16_SPEED_THR_LOW, S16_DEL_YAW_THR_STR_DOT_SML_L_V 
	                            ,S16_SPEED_THR_MED, S16_DEL_YAW_THR_STR_DOT_SML_M_V 
	                            ,S16_SPEED_THR_HIG, S16_DEL_YAW_THR_STR_DOT_SML_H_V ));
	                            	                            
	    tempUW2 = (uint16_t)det_wstr_dot;
	    tempUW1 = esp_tempUW3; 
	    tempUW0 = (uint16_t)S16_DEL_YAW_THR_STR_DOT_SML; // 50 deg, fast steering ratio 200 deg/s -> 2 deg/s
	    u16mulu16divu16();
	    esp_tempUW1 = tempUW3; 
	    if ( esp_tempUW1 > esp_tempUW3 ) 
	    {
	    	esp_tempUW1 = esp_tempUW3;
	    }
	    else
	    { 
	    	;
	    }
	    
	    tempUW2 = (uint16_t)det_wstr_dot;
	    tempUW1 = (uint16_t)S16_DEL_YAW_THR_STR_DOT_LARGE_V;
	    tempUW0 = (uint16_t)S16_DEL_YAW_THR_STR_DOT_LARGE; // 120 deg, fast steering ratio 200 deg/s -> 0.57 deg/s
	    u16mulu16divu16();
	    esp_tempUW2 = tempUW3; 
	    
	    esp_tempUW4 = (uint16_t)S16_DEL_YAW_THR_STR_DOT_LARGE_V;
	
	    if ( esp_tempUW2 > esp_tempUW4 ) 
	    {
	       esp_tempUW2 = esp_tempUW4;		 
	    }	
	    else 
	    {
	    	;
	    }
    	thres_wstr_dot = LCESP_s16IInter2Point( det_abs_wstr, S16_WSTR_DOT_THR_SMALL, (int16_t)esp_tempUW1
                                  ,S16_WSTR_DOT_THR_LARGE, (int16_t)esp_tempUW2 );
   
    delta_yaw_middle_thres = thres_wstr_dot+thres_wstr;
#endif    
*/    
    if(delta_yaw_middle_thres > S16_DEL_YAW_THR_WSTR_LARGE_VAL)
    {
      delta_yaw_middle_thres = S16_DEL_YAW_THR_WSTR_LARGE_VAL;
    } 
    else
    {
    	;
    }
	 
	 #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	  if((ABS_fz==1)&&(lsespu1AHBGEN3MpresBrkOn==1))      /* '06.05.25 : Requested by LeeJG, Adjust by ChoYJ  */
	 #else
	  if((ABS_fz==1)&&(MPRESS_BRAKE_ON==1))      /* '06.05.25 : Requested by LeeJG, Adjust by ChoYJ  */
	 #endif
    {

//#if (__MGH60_ESC_IMPROVE_CONCEPT==1)   /* 11 SWD. Request by GME Opel */
            delta_yaw_middle_thres = delta_yaw_middle_thres ;     	
//#else 
//            delta_yaw_middle_thres = S16_THRESHOLD_ABS;    
//#endif

           /* if ( delta_yaw_middle_thres < YAW_1DEG ) 
            {
            	delta_yaw_middle_thres = YAW_1DEG;
            }
            else
            {
            	;
            }*/

    }
    else
    {
    	;
    }
	
	if(steer_offset_reliablity<=30)
	{
		delta_yaw_middle_thres +=lsesps16MaxSteerThrAdd;
	}
	else
	{
		;
	} 
	
#if __GM_FailM == 1	
    delta_yaw_middle_thres +=lsesps16MaxYawThrAdd;
#else
	if(yaw_offset_reliablity<=30)
	{
		delta_yaw_middle_thres +=lsesps16MaxYawThrAdd;
	}
	else
	{
		;
	}
#endif	
	

	if(alat_offset_reliablity<=30)
	{
		delta_yaw_middle_thres +=lsesps16MaxLatGThrAdd;
	}
	else
	{
		;
	}	    
	
	if(raster_tmp_spec_over_flg ==1)   /* Sensor Temp. : -55~-40, 85~100 */
	{
		delta_yaw_middle_thres +=YAW_5DEG;
	}
	else
	{
		;
	}	    	
	 
	if(delta_yaw_middle_thres>YAW_15DEG)
	{
		delta_yaw_middle_thres=YAW_15DEG;
	}
	else
	{
		;
	}
	
 /********************************************************************/
     #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
  
     delta_yaw_thres=delta_yaw_middle_thres;
		 
		#else 
     delta_yaw_thres=delta_yaw_middle_thres;
    if((YAW_CHANGE_LIMIT==0)&&(esp_mu>650)&&(det_wstr_dot>=3500)) 
    {
        if((delta_yaw>=o_delta_yaw_thres)||(delta_yaw<=u_delta_yaw_thres)) 
        {
            delta_yaw_thres+=YAW_1G5DEG;
        }
        else
        { 
        	;
        }
    }
    else
    {
    	;
    }
		#endif 
    
#if __CAN_SAS_EEP_OFFSET_APPLY == 1
 
    #if  __BANK_DETECT_ESC_CTRL_ENABLE == 1 
    
      if ((!((PARTIAL_PERFORM==1)||(PRE_ESP_CONTROL==1))) && (FLAG_BANK_DETECTED==0) && (FLAG_BANK_SUSPECT==0) &&                 
    	 ((STEER_STABLE_OK==1)||(KSTEER.MODEL_OK_AT_15KPH==1)))    
            
    #else 
    
      if ((!((PARTIAL_PERFORM==1)||(PRE_ESP_CONTROL==1))) &&     
    	  ((STEER_STABLE_OK==1)||(KSTEER.MODEL_OK_AT_15KPH==1)))     
    	  
    #endif   
    	 
#else             
        
    #if  __BANK_DETECT_ESC_CTRL_ENABLE == 1 
    
      if ((!((PARTIAL_PERFORM==1)||(PRE_ESP_CONTROL==1))) && (FLAG_BANK_DETECTED==0) && (FLAG_BANK_SUSPECT==0))  
            
    #else  
        
      if  (!((PARTIAL_PERFORM==1)||(PRE_ESP_CONTROL==1)))    
            
    #endif 
                      
#endif  

 
    { 
    
	    if((esp_mu<=650)&&(YAW_CHANGE_LIMIT==1)) 
	    {
	      
	         #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
	          
	       o_delta_yaw_thres = delta_yaw_thres ;      	         
	         	
	         #else     
	      
	        tempUW2 = (uint16_t)delta_yaw_thres; tempUW1 = (uint16_t)8; tempUW0 = (uint16_t)10;
	        u16mulu16divu16();
	       o_delta_yaw_thres = (int16_t)tempUW3;                  
	                    
	         #endif	       
	                 
	       u_delta_yaw_thres=-delta_yaw_thres;
	    }
		#if (__MGH60_ESC_IMPROVE_CONCEPT==1)
	    else if (((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))&&(EXTREME_UNDER_CONTROL==0)&&(((ETCS_ON==1)||(ESP_TCS_ON==1)||((YAW_CDC_WORK==0)&&(FLAG_DETECT_DRIFT==1)))))
		#else  
	    else if (((ETCS_ON==1)||(ESP_TCS_ON==1)||((YAW_CDC_WORK==0)&&(FLAG_DETECT_DRIFT==1))))
		#endif
	    {
	        	
	            if( FLAG_DETECT_DRIFT==1 ) 
	            {
					          if ( FLAG_BANK_SUSPECT==1 ) 
					          {	
	                        esp_tempW9 = 10;       /* BCNK DETECT Threshold */
	                  }   
	                  else if(FS_YAW_OFFSET_OK_FLG==1)
	                  {
	                        esp_tempW7 = LCESP_s16IInter2Point(vrefk, 200, 10, 500, 5);  /* med mu*/
	                        esp_tempW8 = LCESP_s16IInter2Point(vrefk, 200, 10, 500, 7);  /* high mu*/
	                        esp_tempW9 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMed ,esp_tempW7, lsesps16MuHigh, esp_tempW8); /* stop offset */
	                  }
	                  else
	                  {
	                        esp_tempW9 = 10;   /* offset*/
	                  }
	            }
	            else 
	            {
	                    #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
	                esp_tempW8 = (INT)S16_DEL_YAW_THRES_WGT_IN_EC_MM;
	                if (esp_tempW8 < 7)
	                {
	                    esp_tempW8 = 7;
	                }
	                else if (esp_tempW8 > 10)
	                {
	                    esp_tempW8 = 10;
	                }
	                else
	                {
	                    ;
	                }
	                esp_tempW9 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMed ,esp_tempW8, lsesps16MuHigh, 10);
	                    #else
	            	  esp_tempW9 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMed ,7, lsesps16MuHigh, 10);
	            	    #endif	            	 
	            	
	            }
	        /*o_delta_yaw_thres= (uint16_t)( (((uint32_t)delta_yaw_thres)*esp_tempW9)/10); */
	        tempUW2 = (uint16_t)delta_yaw_thres; tempUW1 = (uint16_t)esp_tempW9; tempUW0 = (uint16_t)10;
	        u16mulu16divu16();
	        o_delta_yaw_thres = (int16_t)tempUW3;      	        
	        u_delta_yaw_thres=-delta_yaw_thres;
	    }
	    else 
	    {
	       o_delta_yaw_thres= delta_yaw_thres;                  
	       u_delta_yaw_thres=-delta_yaw_thres;
	    }  
	     
		#if (__MGH60_ESC_IMPROVE_CONCEPT==1)
	    if( ((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))&&(EXTREME_UNDER_CONTROL==0)) 
		#else  
	    if( (YAW_CDC_WORK==1) && (OVER_UNDER_JUMP==1) && (FLAG_ON_CENTER==0) ) 
		#endif 	     
	    {
	        o_delta_yaw_thres = (o_delta_yaw_thres*5)/10;        /* '06/04/04, min : 2 -> 5   */
	    }
		#if (__MGH60_ESC_IMPROVE_CONCEPT==1)
	    else if( ((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1))&&(EXTREME_UNDER_CONTROL==0) && (Flag_REAR_CONTROL==0) && (Flag_UNDER_CONTROL==0) && (FLAG_ON_CENTER==0) ) 
		#else  
	    else if( (YAW_CDC_WORK==1) && (Flag_REAR_CONTROL==0) && (Flag_UNDER_CONTROL==0) && (FLAG_ON_CENTER==0) ) 
		#endif 
	    {   
	        esp_tempW1 = 10; /* '06/04/04, min : 2 -> 5   */
	        esp_tempW2 = 5;  /* slow_wstr_dot*/
	        esp_tempW3 = 5;    
	        esp_tempW4 = 5;  /* fast_wstr_dot*/
	        if ( det_wstr_dot <= 500 )
	        {   
	            tempW7 = esp_tempW1;
	            tempW8 = esp_tempW2;
	        } 
	        else if ( det_wstr_dot < 1000 ) 
	        {  
	            tempW2 = det_wstr_dot-500; tempW1 = esp_tempW3-esp_tempW1; tempW0 = 1000;
	            s16muls16divs16();
	            tempW7 = esp_tempW1 + tempW3;                
	            tempW2 = det_wstr_dot-500; tempW1 = esp_tempW4-esp_tempW2; tempW0 = 1000;
	            s16muls16divs16();
	            tempW8 = esp_tempW2 + tempW3;           
	    	  } 
	    	  else 
	    	  {
	            tempW7 = esp_tempW3;
	            tempW8 = esp_tempW4;
	        }

	        if ( abs_wstr <= 250 )          
	        {
	        	esp_tempW0 = tempW7;
	        }
	        else if ( abs_wstr < 500 ) 
	        {
	            tempW2 = abs_wstr-250; tempW1 = tempW8-tempW7; tempW0 = 250;
	            s16muls16divs16();
	            esp_tempW0 = tempW7 + tempW3;               
	        } 
	        else
	        {    
	        	esp_tempW0 = tempW8;
	        }

        	o_delta_yaw_thres = (o_delta_yaw_thres*esp_tempW0)/10;
    	}
    	else
    	{ 
    		;
    	}
    
		#if (__MGH60_ESC_IMPROVE_CONCEPT==1)

    	esp_tempW1 = (int16_t)((((int32_t)delta_yaw_thres) * S16_DEL_YAW_THR_LMT_WEG)/100); 

	    if (o_delta_yaw_thres < esp_tempW1 )
    	{
    		  o_delta_yaw_thres = esp_tempW1 ;
    	}
    	else
    	{
    		  ;
    	}

      /*esp_tempW1 = LCESP_s16IInter4Point( vrefk, 0, YAW_5DEG, 
	    																					 S16_SPEED_THR_LOW, YAW_4DEG, 
	                            									 S16_SPEED_THR_MED, YAW_3DEG, 
	                            									 S16_SPEED_THR_HIG, YAW_3DEG );
	                            
      if ( o_delta_yaw_thres < esp_tempW1 )   
    	{
    		o_delta_yaw_thres = esp_tempW1 ;
    	}
    	else
    	{
    		;
      }*/		

    	
      #if (_OS_TH_CONCEPT_BRK_OFFSET==1)||(_OS_TH_CONCEPT_BRK_H_INI_VALUE==1)
    	  os_th_hysteresis = o_delta_yaw_thres ; 
    	  
      #endif    	
        
		#else 
		    
    	if ( o_delta_yaw_thres < YAW_3DEG )   /* '06/4/4, Min: 1deg -> 3deg  */
    	{
    		o_delta_yaw_thres = YAW_3DEG;
    	}
    	else
    	{
    		;
    	}

		#endif     

	} 
	else 
	{          /*   PARTIAL_PERFORM || PRE_ESP_CONTROL*/
	    o_delta_yaw_thres=    delta_yaw_thres ;                     
	    u_delta_yaw_thres= -( delta_yaw_thres ) ;                     
	}   
	    
  #if __MGH40_Bank_Estimation
  
    #if  __BANK_DETECT_ESC_CTRL_ENABLE == 1    
     #if __BANK_ESC_IMPROVEMENT_CONCEPT == 1  /* '11.08 : Improve a bank-detection & ESC Ctrl to 3deg/s */
                                              /*  '11.9.20 : Improved by  */
        /*  S16_Bank_Lower_Suspect_Thres2: Bank Suspect Lower Threshold, default=3
            S16_Bank_Suspect_Yaw_Thres2: Additional Del_Yaw_Thres at S16_Bank_Suspect_Thres2 under Bank Condition, default=1
            S16_Bank_Detect_L_Yaw_Thres2: Additional Del_Yaw_Thres at S16_Bank_Detect_Thres2 under Bank Condition, default=2
            S16_Bank_Detect_U_Yaw_Thres2: Additional Del_Yaw_Thres at (S16_Bank_Detect_Thres2)*3 under Bank Condition, default=6 */
                                                                                                                                                                
        if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1)|| (ldespu1BankLowerSuspectFlag==1))	
    	    {			 /*	reserve	Banked road	*/
           
           if ( McrAbs(Bank_Angle_Final) < S16_Bank_Lower_Suspect_Thres2)   
           {
           		delta_bank_angle_thres =  S16_Bank_Lower_Suspect_Thres2 ;
           }
           else if ( McrAbs(Bank_Angle_Final) > (S16_Bank_Detect_Thres2*3) )   /*  '09.1.12 : Bank Improve  */
           {
           	    delta_bank_angle_thres =  (S16_Bank_Detect_Thres2*3) ;         
           }
           else  
           {
           		delta_bank_angle_thres =  McrAbs(Bank_Angle_Final) ;   
           }
		   

		       if (delta_bank_angle_thres < S16_Bank_Lower_Suspect_Thres2)
		       {
			        esp_tempW0 = 0;
		       }
		       else if ((delta_bank_angle_thres >= S16_Bank_Lower_Suspect_Thres2) && (delta_bank_angle_thres < S16_Bank_Suspect_Thres2))
		       {
			        esp_tempW0 = LCESP_s16IInter2Point(delta_bank_angle_thres, S16_Bank_Lower_Suspect_Thres2, 0, S16_Bank_Suspect_Thres2, S16_Bank_Suspect_Yaw_Thres2);
		       }
		       else if ((delta_bank_angle_thres >= S16_Bank_Suspect_Thres2) && (delta_bank_angle_thres < S16_Bank_Detect_Thres2))
		       {
			       esp_tempW0 = LCESP_s16IInter2Point(delta_bank_angle_thres, S16_Bank_Suspect_Thres2, S16_Bank_Suspect_Yaw_Thres2, S16_Bank_Detect_Thres2, S16_Bank_Detect_L_Yaw_Thres2);
		       }
		       else if ((delta_bank_angle_thres >= S16_Bank_Detect_Thres2) && (delta_bank_angle_thres < S16_Bank_Detect_Thres2*3))
		       {
			       esp_tempW0 = LCESP_s16IInter2Point(delta_bank_angle_thres, S16_Bank_Detect_Thres2, S16_Bank_Detect_L_Yaw_Thres2, S16_Bank_Detect_Thres2*3, S16_Bank_Detect_U_Yaw_Thres2);
		       }
		       else
		       {
		       	  esp_tempW0 = S16_Bank_Detect_U_Yaw_Thres2;
		       }
		       
		         if((ldespu1BankLowerSuspectFlag==1)&&(FLAG_BANK_SUSPECT==0)&&(FLAG_BANK_DETECTED==0))
		          {
		          	  if((det_alatm >= S16_BANK_THRES_ADD_ALAT_COND)&&(vref5 >= S16_BANK_THRES_ADD_VREF_COND))
		              {
		         o_delta_yaw_thres = o_delta_yaw_thres + esp_tempW0;                         
	                    u_delta_yaw_thres= -(delta_yaw_thres - (esp_tempW0*(S16_BANK_UNDER_THRES_SIGN)))-u_th_add ;
		              }
		              else
		              {
		              	  ;
		              }
		          }
		          else if((FLAG_BANK_DETECTED==1)||(FLAG_BANK_SUSPECT==1))
		          {
		          	  o_delta_yaw_thres = o_delta_yaw_thres + esp_tempW0;                         
	                u_delta_yaw_thres= -(delta_yaw_thres - (esp_tempW0*(S16_BANK_UNDER_THRES_SIGN)))-u_th_add ;
		          }
		          else
		          {
		          	  ;
		          }
		      }
		      else 
          {
        	  ;
          }
     
      #else

      if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
    	{			 /*	reserve	Banked road	*/
           
           if ( abs(Bank_Angle_Final) < S16_Bank_Exit_Thres2 )   
           {
           		delta_bank_angle_thres =  S16_Bank_Exit_Thres2 ;
           }
           else if ( abs(Bank_Angle_Final) > (S16_Bank_Detect_Thres2*3) )   /*  '09.1.12 : Bank Improve  */
           {
           	    delta_bank_angle_thres =  (S16_Bank_Detect_Thres2*3) ;
           }
           else  
           {
           		delta_bank_angle_thres =  abs(Bank_Angle_Final) ;   
           }
                
           if ( delta_bank_angle_thres <= S16_Bank_Detect_Thres2 )   
           { 
                esp_tempW0 = (INT)((((LONG)(delta_bank_angle_thres - S16_Bank_Exit_Thres2))*200)/(S16_Bank_Detect_Thres2 - S16_Bank_Exit_Thres2));        
                 
           }
           else  
           {
                esp_tempW1 = (INT)((((LONG)(delta_bank_angle_thres - S16_Bank_Detect_Thres2))*400)/((S16_Bank_Detect_Thres2*3) - S16_Bank_Detect_Thres2)); 
                esp_tempW0 = esp_tempW1 + 200 ;            
           } 
           
            o_delta_yaw_thres = o_delta_yaw_thres + esp_tempW0; 
        } 
        else 
        {
        	;
        }
      
      #endif
    
    #else   
  
        if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
    	{			 /*	reserve	Banked road	*/
           
           if ( abs(Bank_Angle_Final) < S16_Bank_Exit_Thres2 )   
           {
           		delta_bank_angle_thres =  S16_Bank_Exit_Thres2 ;
           }
           else if ( abs(Bank_Angle_Final) > S16_Bank_Detect_Thres2 )  
           {
           	    delta_bank_angle_thres =  S16_Bank_Detect_Thres2 ;
           }
           else  
           {
           		delta_bank_angle_thres =  abs(Bank_Angle_Final) ;   
           }
                
            esp_tempW0 = (INT)((((LONG)(delta_bank_angle_thres - S16_Bank_Exit_Thres2))*200)/(S16_Bank_Detect_Thres2 - S16_Bank_Exit_Thres2));        
            
            o_delta_yaw_thres = o_delta_yaw_thres + esp_tempW0; 
        } 
        else 
        {
        	;
        }
  
    #endif   
  
  #else
  	    
	if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
	{			 /*	reserve	Banked road	*/
       
       if ( delta_banked_angle < S16_BANK_LOW )   
       {
       		delta_bank_angle_thres =  S16_BANK_LOW ;
       }
       else if ( delta_banked_angle > S16_BANK_HIGH )  
       {
       	delta_bank_angle_thres =  S16_BANK_HIGH ;
       }
       else  
       {
       		delta_bank_angle_thres =  delta_banked_angle ;   
       }
            
		tempW2 = delta_bank_angle_thres - S16_BANK_LOW ; 
        tempW1 = 200 ;   
        tempW0 = S16_BANK_HIGH-S16_BANK_LOW ;
        s16muls16divs16();
        esp_tempW0 = tempW3;        
        
        o_delta_yaw_thres = o_delta_yaw_thres + esp_tempW0; 
    } 
    else 
    {
    	;
    }
              
  #endif
              
#if (_OS_TH_CONCEPT_BRK_HOLD==1) 
    
    if ((SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FL==1)) 
    { 
      esc_slip_need_monitor_1 = 0 ;    
      esc_slip_need_monitor_2 = 1 ; 
      os_th_count_01 = 0; 
    }
    else if (((esc_slip_need_monitor_2==1)||(os_th_count_01<50))&&(SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FL==0))
    {
      esc_slip_need_monitor_1 = 1 ;   /* OS_TH Recovery */
      esc_slip_need_monitor_2 = 0 ; 
      os_th_count_01 = os_th_count_01 + 1 ;
    }
    else
    {
      esc_slip_need_monitor_1 = 0 ;   
      esc_slip_need_monitor_2 = 0 ; 
      os_th_count_01 = 50;
    }
              
    esp_tempW3 = S16_BRK_OS_TH_HOLD_MPRESS_ENTER ; 
    
    if ( esp_tempW3 < MPRESS_7BAR)
    {
      esp_tempW3 = MPRESS_7BAR ; 
    }      
    
   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lsesps16AHBGEN3mpress>esp_tempW3)&&(esc_th_ctrl_01==0))
   #else
    if((mpress>esp_tempW3)&&(esc_th_ctrl_01==0))
   #endif              
    { 
        esc_th_ctrl_01 = 1 ;       
        o_delta_yaw_thres = o_delta_yaw_thres_old/10 ; 
        
//      #if (_OS_TH_CONCEPT_BRK_H_INI_VALUE==1)        
        os_th_braking_ini_value = o_delta_yaw_thres ; 
//      #endif         
         
    }
   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    else if ((lsesps16AHBGEN3mpress>MPRESS_7BAR)&&(esc_th_ctrl_01==1))
   #else
    else if ((mpress>MPRESS_7BAR)&&(esc_th_ctrl_01==1))
   #endif
    { 
        esc_th_ctrl_01 = 1 ;       
        o_delta_yaw_thres = o_delta_yaw_thres_old/10 ; 
                
        #if (_OS_TH_CONCEPT_BRK_H_INI_VALUE==1) 
        
          if ((SLIP_CONTROL_NEED_FR==1) || (SLIP_CONTROL_NEED_FL==1))
          {   
              esp_tempW8 = (os_th_braking_ini_value*3)/10  ;
              
              esp_tempW7 = (int16_t)((((int32_t)os_th_braking_ini_value) * S16_DEL_YAW_THR_LMT_WEG_ABS)/100); 
	            
	            if ( esp_tempW8 < esp_tempW7)
	              {	              	
	                esp_tempW8 = esp_tempW7 ; 
	          }
	          else
        	  {
	            	  ;
            }  
            
            esp_tempW9 = LCESP_s16Lpf1Int( esp_tempW8*10, o_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
            o_delta_yaw_thres = esp_tempW9/10;
            o_delta_yaw_thres_old = o_delta_yaw_thres*10 ;      
          }   
          else if (esc_slip_need_monitor_1==1)
          {   
            esp_tempW9 = LCESP_s16Lpf1Int( os_th_braking_ini_value*10, o_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_7HZ);
            o_delta_yaw_thres = esp_tempW9/10;
            o_delta_yaw_thres_old = o_delta_yaw_thres*10 ;      
          }             
          else
          {
            ;
          }
          
        #endif        
                        
    }
    else
    {  
        esc_th_ctrl_01 = 0 ;       
        esp_tempW1 = LCESP_s16Lpf1Int( o_delta_yaw_thres*10, o_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
        
        o_delta_yaw_thres_old = esp_tempW1;
        o_delta_yaw_thres = esp_tempW1/10 ;
    }              
               
    u_delta_yaw_thres = LCESP_s16Lpf1Int( u_delta_yaw_thres, u_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);

#else

    esp_tempW1 = LCESP_s16Lpf1Int( o_delta_yaw_thres*10, o_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
    
    o_delta_yaw_thres_old = esp_tempW1;
    o_delta_yaw_thres = esp_tempW1/10 ;
    
    u_delta_yaw_thres = LCESP_s16Lpf1Int( u_delta_yaw_thres, u_delta_yaw_thres_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);

#endif              

    delta_yaw_out_thres=YAW_3DEG;
    delta_yaw_out_thres2=YAW_1G5DEG;
            
    if(delta_yaw<0)
    {
        delta_yaw_out_thres +=YAW_2DEG;
        delta_yaw_out_thres2+=YAW_2DEG;
    }
    else
    { 
    	;
    }
    
    sign_o_delta_yaw_thres =yaw_sign*o_delta_yaw_thres;
    sign_u_delta_yaw_thres =yaw_sign*u_delta_yaw_thres;
}


#if	__SIDE_SLIP_ANGLE_MODULE
void LDESP_vCompSideSlipThreshold(void)
{
    o_delta_beta_thres_old = o_delta_beta_thres ;
    u_delta_beta_thres_old = u_delta_beta_thres ;
    
/*******    Calculation of Delta_Beta       *******/
            delta_beta_old = delta_beta   ;
            delta_beta2_old = delta_beta2 ;

        delta_beta2 = LCESP_s16Lpf1Int( (beta_est-beta_ref) , delta_beta2_old, L_U8FILTER_GAIN_10MSLOOP_6_5HZ); /* 5Hz 0.032sec 100 28*/
        
            esp_tempW0 = 100 ; /* r=0.001, 0.1 deg     */         
            if( beta_est >= esp_tempW0 ) 
            {
            	beta_sign = 1;
            }
            else if ((beta_est > -esp_tempW0) && (beta_sign == 1))  
            {
            	beta_sign =1;
            }
            else if ((beta_est <= -esp_tempW0))                     
            {
            	beta_sign = -1;
            }
            else if ((beta_est < esp_tempW0) && (beta_sign == -1))  
            {
            	beta_sign =-1;
            }
            else 
            {
            	beta_sign = 0;
            }
        
/*     delta_beta = delta_beta2*beta_sign ;     */  
        delta_beta = delta_beta2 ;          
            
/*  Steeering wheel angle*/
    /*if(LOOP_TIME_20MS_TASK_1==1) 
    { */   
	    if(abs_wstr<=WSTR_120_DEG)
	    {
	        beta_thres_base = 2000 ;   /* 2deg, r=1/1000*/
	    } 
	    else if( (abs_wstr > WSTR_120_DEG) && (abs_wstr<=WSTR_550_DEG))
	    {
	/*      beta_thres_base = 2000 + (int16_t)( (((int32_t)(abs_wstr-WSTR_120_DEG))*3000)/(WSTR_550_DEG-WSTR_120_DEG));   //   r=1/1000 */
	        tempW2 = abs_wstr-WSTR_120_DEG; tempW1 = 3000; tempW0 = WSTR_550_DEG-WSTR_120_DEG;
	        s16muls16divs16() ;
	        beta_thres_base = 2000 + tempW3;                    
	    }
	    else if( abs_wstr>WSTR_550_DEG ){
	        beta_thres_base = 5000 ;   /* 5deg, r=1/1000 */
	    }
	    else
	    {
	        ;
	    }	    
	    
	/*  Vehicle speed  */
	    if ( vrefk <= 200 ) 
	    {
	    	beta_thres_vel = 1000;          /* r : 0.001*/
	    }
	    else if ( vrefk < 400 )
	    {
	/*  beta_thres_vel = 1000 +(int16_t)( (((int32_t)(vrefk-200))*(-1000))/200);       */
		    tempW2 = vrefk-200; tempW1 = -1000; tempW0 = 200;
		    s16muls16divs16() ;
		    beta_thres_vel = 1000 + tempW3 ;            
	    } 
	    else 
	    {
	    	beta_thres_vel = 0 ;
	    }

	/*  Steering rate */
	    if ( det_wstr_dot <= 3500 ) 
	    {               /* r : 0.1*/
	/*      beta_thres_wstr_dot = 1000 +(int16_t)( (((int32_t)(det_wstr_dot-3500))*(1000))/3500);      // r = 0.001    */
	        tempW2 = det_wstr_dot-3500; tempW1 = 1000; tempW0 = 3500;
	        s16muls16divs16() ;
	        beta_thres_wstr_dot = 1000 + tempW3 ;           
	    } 
	    else if ( det_wstr_dot > 3500 ) 
	    {  
	        beta_thres_wstr_dot = 1000 ;        /* r = 0.001  */          
	    }
	    else
	    {
	    	;
	    }
	/*}
	else
	{
		;
	}*/
    beta_middle_thres = beta_thres_base + beta_thres_vel + beta_thres_wstr_dot ;

    o_delta_beta_thres =  beta_middle_thres;                  
    u_delta_beta_thres = -1*beta_middle_thres;

    o_delta_beta_thres = LCESP_s16Lpf1Int( o_delta_beta_thres , o_delta_beta_thres_old , L_U8FILTER_GAIN_10MSLOOP_3_5HZ); 
    
    u_delta_beta_thres = LCESP_s16Lpf1Int( u_delta_beta_thres , u_delta_beta_thres_old , L_U8FILTER_GAIN_10MSLOOP_3_5HZ); 

}
#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCompThreshold
	#include "Mdyn_autosar.h"
#endif
