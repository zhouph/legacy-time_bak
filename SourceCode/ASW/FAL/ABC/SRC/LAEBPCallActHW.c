/******************************************************************************
* Project Name: ELECTRONIC BRAKE PREFILL
* File: LDEBPCallActHW.C
* Date: June. 4. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAEBPCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* Includes ******************************************************************/

#include "LAEBPCallActHW.h"
#include "LAEPCCallActHW.h"
#include "LCEBPCallControl.h"
  #if __ADVANCED_MSC
    #include "LAABSCallMotorSpeedControl.h"
    void LCMSC_vSetEBPTargetVoltage(void);
  #endif

#if __EBP
#include "LDEBPCallDetection.h"
#include "LCEBPCallControl.h"
#endif

#if __EBP
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/
void LAEBP_vCallActHW(void);
void LAEBP_vCallActValve(void);


/* Implementation*************************************************************/




void    LAEBP_vCallActHW(void)
{
    LAEBP_vCallActValve();
}


void LAEBP_vCallActValve(void){

	if (EBP_ON)
    {
        HV_VL_fl = EBP_HV_VL_fl;
        AV_VL_fl = EBP_AV_VL_fl;
        HV_VL_fr = EBP_HV_VL_fr;
        AV_VL_fr = EBP_AV_VL_fr;
        HV_VL_rl = EBP_HV_VL_rl;
        AV_VL_rl = EBP_AV_VL_rl;
        HV_VL_rr = EBP_HV_VL_rr;
        AV_VL_rr = EBP_AV_VL_rr;
      #if __SPLIT_TYPE      /* FR Split */
        S_VALVE_PRIMARY      = EBP_S_VALVE_PRIMARY;
        S_VALVE_SECONDARY    = EBP_S_VALVE_SECONDARY;
        TCL_DEMAND_PRIMARY   = EBP_TCL_DEMAND_PRIMARY;
        TCL_DEMAND_SECONDARY = EBP_TCL_DEMAND_SECONDARY;
      #else                 /* X Split */
        S_VALVE_RIGHT = EBP_S_VALVE_PRIMARY;
        S_VALVE_LEFT  = EBP_S_VALVE_SECONDARY;
        TCL_DEMAND_fr = EBP_TCL_DEMAND_PRIMARY;
        TCL_DEMAND_fl = EBP_TCL_DEMAND_SECONDARY;
      #endif

    }
    else { }
}

#if __ADVANCED_MSC
void LCMSC_vSetEBPTargetVoltage(void)
{
    EBP_MSC_MOTOR_ON = EBP_ON;
    if (EBP_MSC_MOTOR_ON)
    {
        ebp_msc_target_vol = MSC_3_V;
    }
    else
    {
        ebp_msc_target_vol = MSC_0_V;
    }
}
#endif

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAEBPCallActHW
	#include "Mdyn_autosar.h"               
#endif
