#ifndef __LAAVHCALLACTHW_H__
#define __LAAVHCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __AVH
/*Global Type Declaration ****************************************************/
#define	nonaa0       	AVHAF0.bit7
#define	nonaa1			AVHAF0.bit6
#define	nonaa2			AVHAF0.bit5
#define	nonaa3			AVHAF0.bit4
#define	nonaa4			AVHAF0.bit3
#define	nonaa5			AVHAF0.bit2
#define	nonaa6			AVHAF0.bit1
#define	nonaa7			AVHAF0.bit0

/* Logcal Definiton  *********************************************************/
#define AVH_TCMF_CTRL_MIN_CURRENT             100
/*****************************************************************************/

/****************************TC current compensation**************************/
#define AVH_TC_CUR_COMP_CYCLE             180 /* 60sec */
#define AVH_TC_CUR_COMP_CURENT            0 /* mA */
/*****************************************************************************/

/********************************Initial Cur tunning**************************/
#define AVH_CUR_AT_INITIAL                        1000 /* mA */

/*
#define S16_AVH_CUR_INIT_DOWN_TIME           		L_U8_TIME_10MSLOOP_200MS
#define S16_AVH_CUR_INIT_UP_TIME                    L_U8_TIME_10MSLOOP_150MS
*/
/*****************************************************************************/

/********************************Rise mode tunning****************************/
#define AVH_CUR_AT_RISE                           1000 /* mA */
/******************************************************************************/


/* tuning parameter */
/*
#define S16_AVH_MIN_CUR_INIT_COMP                 -50
#define S16_AVH_LOW_CUR_INIT_COMP                 -30
#define S16_AVH_MED_CUR_INIT_COMP                 100
#define S16_AVH_HIGH_CUR_INIT_COMP                150
#define S16_AVH_CUR_INIT_MAX_CUR_COMP             150
*/
/* tuning parameter */

/*Global Extern Variable  Declaration*****************************************/
extern struct	U8_BIT_STRUCT_t AVHAF0;

extern int16_t    lcs16Avhdropcurrent, lcs16Avhdropcurrentholdtime;
extern int16_t    lcs16AvhSecondholdtime, lcs16AvhFadeoutrefscan, lcs16AvhFadeoutDropCurrent; 
extern int16_t    lcs16Avhdropcurrent, lcs16AvhCurrentTemp, lcs16AvhCurInitComp, lcs16AvhCurInitMaxComp;
extern int16_t    avh_msc_target_vol, lcs16AvhDownDrvInitDrop, lcs16AvhCurDropByCycle;

extern uint8_t    lcu8AvhInitCurDownTime, AVH_debuger2;

                                        

/*Global Extern Functions  Declaration****************************************/
extern void	LAAVH_vCallActHW(void);
extern void	LCMSC_vSetAVHTargetVoltage(void);
extern int16_t	LAMFC_s16SetMFCCurrentAVH(int16_t MFC_Current_old);
  #endif
#endif
