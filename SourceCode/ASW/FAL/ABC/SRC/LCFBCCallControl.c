/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LCFBCCallControl.c
* Description:		Main Control of FBC
* Logic version:	HV26
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C15			eslim			Initial Release
*  6210			eslim			Control Box mode, exit mode modified
*  6216			eslim			Modified based on MISRA rule
********************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCFBCCallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/


#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#if __FBC
#include "LCFBCCallControl.h"
#include "LCPBACallControl.h"
#include "LDFBCCallDetection.h"
		  
/* Logcal Definiton ************************************************************/


	
/* Variables Definition ********************************************************/
U8_BIT_STRUCT_t FBCF00;


/* FBC (Fading Brake Compensation) */
    int16_t	FBC_mode, FBC_hold_timer, FBC_on_timer, fbc_abs_time;
    uint8_t	FBC_flags;
    uint8_t	FBC_ABS_timer;
    uint8_t	FBC_MPS_FAIL_tester;
    int16_t FBC_exit_pressure_cnt;
    uint8_t	FBC_dct_cnt;
    int16_t	FBC_MPS_exit_timer;           


/* Local Function prototype ****************************************************/
void LCFBC_vCallControl(void);
void LCFBC_vInitializeVariables(void);
void LCFBC_vExitControl(void);
void LCFBC_vResetVariable(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LCFBC_vCallControl
* CALLED BY:			LC_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			If brake temperature overheating is detected, 
*						then FBC is enable.
*						When mpress is larger than 80 bar(= generated 0.8g's decel
*						at high coe surface) and Vehicle speed is over 50 km/h
*						but deceleration is under 0.6g then the FBC mode start.
********************************************************************************/
void LCFBC_vCallControl(void)
{   
/*********************************************************
*  FBC control, exit condition :
*
*	1. mpress drop less than 5 bar [EXIT MODE]
*	2. vref become less than 10 kph [EXIT MODE]
*                      Coded by Lim Eunseong in 2005.09.01
**********************************************************/
/*    LDFBC_vCallDetection(); will be modified to be called from LD_vCallMain() */

	if((FBC_ON==0)&&(FBC_ACTIVE==1)&&(PBA_EXIT_CONTROL==1))
     {
       		FBC_ON=1;
       		
  
       		if((BA_mode == PBA_DUMP)||(BA_mode == PBA_STANDARD))
       		{
       			FBC_mode = BA_HOLD;
       		}
       		else 
       		{
       			FBC_mode = BA_mode;
       		}
       }
       else 
       {
       		;
       }
    LCFBC_vExitControl();

		if(FBC_mode == FBC_STANDBY)
		{
			FBC_ON=0;
			if(FBC_ACTIVE)
			{
			  #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
				if(((lsesps16EstBrkPressByBrkPedalF>=S16_FBC_PRESS_ENTER_THR)&&(lsespu1BrkAppSenInvalid == 0))
				  &&(vdc_vref>=S16_FBC_SPEED_ENTER_THR))		
			  #else
				if((mpress>=S16_FBC_PRESS_ENTER_THR)&&(vdc_vref>=S16_FBC_SPEED_ENTER_THR))
			  #endif	
				{
					FBC_dct_cnt++;
					if(FBC_dct_cnt>L_U8_TIME_10MSLOOP_210MS)  /* 10ms Task */
					{
						if(((FBC_decel_sum>=(-S16_FBC_ENTER_DECEL*10))&&(!ABS_fz)))
						{
							FBC_ON=1;
							FBC_hold_timer=0;
							FBC_mode=FBC_APPLY;

						}
						else
						{
							;
						}
					}
					else if(FBC_dct_cnt>=255)
					{
						FBC_dct_cnt=255;
					}
					else
					{
						;
					}
				}
				else
				{
					;
				}
	        }
	        else
	        {
            	;
            }
        }	        
        else if(FBC_mode == FBC_APPLY )
        {
            if(ABS_fz==0)
            {
            	FBC_hold_timer++;
            }
            else{}
            
            if((ABS_fz==0)&&(FBC_hold_timer>U16_FBC_MAX_APPLY_TIME_BEFORE_ABS))	/* 1sec */
            { 
                FBC_mode=FBC_HOLD;
            }
            else
            {
            	;
            }
		}
       else
       {
       		FBC_hold_timer=0;
       }   
       
       if(FBC_mode==FBC_APPLY)
       {
       		FBC_WORK=1;
       }
}

/*******************************************************************************
* FUNCTION NAME:		LCFBC_vCallInitializeVariables
* CALLED BY:			LC_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Initialize Varibles of FBC
********************************************************************************/
void LCFBC_vInitializeVariables(void)
{
    FBC_ON=FBC_WORK=0; 
    FBC_mode=FBC_STANDBY;
    FBC_hold_timer=FBC_ABS_timer=0;
    FBC_MPS_exit_timer=0;
    FBC_LOW_MU=0;
    
}

/*******************************************************************************
* FUNCTION NAME:		LCFBC_vExitControl
* CALLED BY:			LCFBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Exit Condition of FBC
********************************************************************************/
void LCFBC_vExitControl(void)
{
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
	if((lsesps16EstBrkPressByBrkPedalF<(S16_FBC_PRESS_EXIT_THR1+MPRESS_10BAR))&&(lsespu1BrkAppSenInvalid == 0))				  
  #else
	if(mpress<(S16_FBC_PRESS_EXIT_THR1+MPRESS_10BAR))
  #endif		
	{
		FBC_exit_pressure_cnt++;
		if(FBC_exit_pressure_cnt>=T_10_S)
		{
			FBC_exit_pressure_cnt=T_10_S; 
		}
		else
		{
			;
		}
	}
	else
	{
		FBC_exit_pressure_cnt=0;
	}

  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
    if(((lsesps16EstBrkPressByBrkPedalF<S16_FBC_PRESS_EXIT_THR1)&&(lsespu1BrkAppSenInvalid == 0))
    	||(vdc_vref<S16_FBC_SPEED_EXIT_THR))
  #else  
    if((mpress<S16_FBC_PRESS_EXIT_THR1)||(vdc_vref<S16_FBC_SPEED_EXIT_THR))
  #endif  
    {
    	FBC_dct_cnt=0;  
		if(FBC_ON==1)
		{
			FBC_hold_timer=0;
		}
		else
		{
			;
		}
		
		if((fu1BLSErrDet==0)&&(fu1BlsSusDet==0)) /* DM requirement for Autobuild Test at 2011.11 */
		{
		    if((BLS==1)&&(mtp<=(int16_t)U8_FBC_EXIT_MTP1)) /* Braking only */
		    {
		    	tempW1 = S16_LOW_MPRESS_FBC_ON_TIME_BRAKING; /* 3sec */
		    }
		    else if((BLS==0)&&(mtp>=(int16_t)U8_FBC_EXIT_MTP2)) /* Acceleration only */
		    {
		    	tempW1 = S16_LOW_MPRESS_FBC_ON_TIME_ACCEL; /* 0.1sec */
		    }
		    else /* Default */
		    {
		    	tempW1 = S16_LOW_MPRESS_FBC_ON_TIME_DEFAULT; /* 0.5sec */
		    }
		}
		else 
		{
			if(mtp>=(int16_t)U8_FBC_EXIT_MTP2)
			{
			    tempW1 = S16_LOW_MPRESS_FBC_ON_TIME_ACCEL; /* 0.1sec */
			}
			else
			{
				tempW1 = S16_LOW_MPRESS_FBC_ON_TIME_DEFAULT; /* 0.5sec */
			}
		}
		
		if((FBC_ON==1)&&(FBC_exit_pressure_cnt>=tempW1))
		{ /* 286 scan = 2 sec */
	        FBC_ON=0;   
	        FBC_mode=FBC_STANDBY;
	        FBC_ABS_timer=0;
	        FBC_LOW_MU=0;
	    }
	    else
	    {
	    	;
	    }
	    
	    if(vdc_vref<S16_FBC_SPEED_EXIT_THR-60)
	    {
	        FBC_ON=0;   
	        FBC_mode=FBC_STANDBY;
	        FBC_ABS_timer=0;
	        FBC_LOW_MU=0;
	    }
	    else
	    {
	    	;
	    }
	}    
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
    if(((lsesps16EstBrkPressByBrkPedalF>S16_FBC_PRESS_EXIT_THR2)&&(lsespu1BrkAppSenInvalid == 0))
    	&&(FBC_ON==1))
  #else  
    else if((FBC_ON==1)&&(mpress>S16_FBC_PRESS_EXIT_THR2))
  #endif 
    {
		LCFBC_vResetVariable();
    }
    /* Add FBC_LOW_MU detect */
    else if((FBC_ON==1)&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_LOW/10))
    {
        LCFBC_vResetVariable();
        FBC_LOW_MU=1;
    }
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
    else if((FBC_ON==1)&&((lsesps16EstBrkPressByBrkPedalF>MPRESS_40BAR)&&(lsespu1BrkAppSenInvalid == 0))
    		&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_MEDIUM/10))
  #else  
    else if((FBC_ON==1)&&(mpress>MPRESS_40BAR)
    		&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_MEDIUM/10))
  #endif   
    {
        LCFBC_vResetVariable();
        FBC_LOW_MU=1;
    }
    /* GM SSTS requiments */ 
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
    else if((FBC_ON==1)&&(FBC_WORK==1)
    	&&((lsesps16EstBrkPressByBrkPedalF>S16_FBC_PRESS_EXIT_THR3)&&(lsespu1BrkAppSenInvalid == 0)))
  #else  
	else if((FBC_ON==1)&&(FBC_WORK==1)&&(mpress<S16_FBC_PRESS_EXIT_THR3))
  #endif   	
	{
		if(FBC_MPS_exit_timer < U8_FBC_PRESS_EXIT_THR3_TIMER ) 
		{
			FBC_MPS_exit_timer++;	
		}	
		else 
		{
			if(FBC_mode!=FBC_DUMP)
			{
				FBC_hold_timer=0;
			}
			else
			{
				;
			}
			
	        FBC_mode=FBC_DUMP;
        	FBC_ABS_timer=0;				
		}
	}
	/* End of GM SSTS requiments */
	else
	{
		;
	}
	
    if((esp_mu<S16_FBC_ESP_MU_LOW)&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_LOW/10))
    {
        LCFBC_vResetVariable();
        FBC_LOW_MU=1;
    }
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
    else if(((lsesps16EstBrkPressByBrkPedalF>MPRESS_40BAR)&&(lsespu1BrkAppSenInvalid == 0))
    	&&(esp_mu<S16_FBC_ESP_MU_MEDIUM)&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_MEDIUM/10))
  #else  
    else if((mpress>MPRESS_40BAR)&&(esp_mu<S16_FBC_ESP_MU_MEDIUM)
    		&&(AFZ_OK==1)&&(afz>-S16_FBC_ESP_MU_MEDIUM/10))
  #endif
    {
        LCFBC_vResetVariable();
        FBC_LOW_MU=1;
    }
    else
    {
    	;
    }    
}

/*******************************************************************************
* FUNCTION NAME:		LCFBC_vResetVariable
* CALLED BY:			LCFBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Reset Varibles of FBC
********************************************************************************/
void LCFBC_vResetVariable(void)
{
    FBC_ON=0;  	
    FBC_mode=FBC_STANDBY;
    FBC_hold_timer=0;    
    FBC_ABS_timer=0;
    FBC_MPS_exit_timer=0;	
}

#endif /* __FBC */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCFBCCallControl
	#include "Mdyn_autosar.h"
#endif    
