
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPFilterEspSensor
	#include "Mdyn_autosar.h"
#endif

#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"

#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"

#if defined(__UCC_1)
	#if !defined(__AUTOSAR_CORE_ENA)
		#include"../System/WDecSenSig.h"
	#else
		#include"../System/WContlSenSig.h"
	#endif

#if defined(__AFS)
#include"UCC_Logic/LCAFSCallControl.h"
#endif

#endif

/* Local Definition  ***********************************************/
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
#define PRESS_BY_PEDAL_BRAKE_ON_THR			MPRESS_4BAR
#endif 

#define S16_PEDAL_MAX_TRAVEL_GM			15500 /* 1_100 resol 155mm */

/* Variables Definition*******************************************************/

#if (__EPB_INTERFACE==ENABLE)
extern struct	U8_BIT_STRUCT_t EPBF0;
#endif

#if	__VDC
#if !SIM_MATLAB

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
static void LSESP_vCalMpress(void);
static void LSESP_vCalPressure(struct  PREM_PRESS_OFFSET_STRUCT  *PRESS);
#else
static void LSESP_vCalMpress(void);
#endif

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
static void LSESP_vCalBrkAppSenSignal(void);
static void LSESP_vEstBrkPressByBrkPedal(void);
static void LSESP_vChkBrkPedalApp(void);
#else
static void LSESP_vCalBrkAppSenSignal_GM(void);
#endif


static void LSESP_vCalWheelSteeringAngle(void);
static void LSESP_vCalYawRate(void);
static void LSESP_vCalLateralAcceleration(void);
static void LSESP_vChkMpressStable(void);
static void LSESP_vChkInertialSensor(void);
static void LSESP_vChkStrYawLg(void);
#endif
static void LSESP_vCalWheelSteeringVelocity(void);
static void LSESP_vCalYawRateAbsolute(void);
static void LSESP_vCalYawAcceleration(void);
static void LSESP_vChkMpressStable(void);


extern uint8_t mcp_unstable_cnt;

uint8_t mpress_count_en;
uint8_t mpress_count_ex;

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
int16_t  lsesps16EstBrkPressByBrkPedal;
int16_t  lsesps16EstBrkPressByBrkPedalF;

uint8_t lsespu8EstBrkPressEnterCnt;
uint8_t lsespu8EstBrkPressExitCnt;

U8_BIT_STRUCT_t PEDAL0;

#endif

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
uint8_t lsespu8MpressByAHBgen3EnCT;
uint8_t lsespu8MpressByAHBgen3ExCT;
#endif


void LSESP_vFilterEspSensor(void)
{
#if !SIM_MATLAB

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
	LSESP_vCalMpress();
	
	#if  __FL_PRESS == ENABLE
    LSESP_vCalPressure(&PRESS_FL);
    #endif
    
    #if  __FR_PRESS == ENABLE
    LSESP_vCalPressure(&PRESS_FR);
    #endif
    
    #if  __RL_PRESS == ENABLE
    LSESP_vCalPressure(&PRESS_RL);
    #endif
    
    #if  __RR_PRESS == ENABLE
    LSESP_vCalPressure(&PRESS_RR);    
    #endif
    
#else
    LSESP_vCalMpress();
#endif    
    
#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
    LSESP_vCalBrkAppSenSignal();
    LSESP_vEstBrkPressByBrkPedal();
    LSESP_vChkBrkPedalApp();
#else
    LSESP_vCalBrkAppSenSignal_GM();    
#endif
    
	LSESP_vCalWheelSteeringAngle();
    
	LSESP_vCalYawRate();
        
	LSESP_vCalLateralAcceleration();

	LSESP_vChkStrYawLg();
#endif/*!SIM_MATLAB*/

	LSESP_vCalWheelSteeringVelocity();
	
	LSESP_vCalYawRateAbsolute();
	
	LSESP_vCalYawAcceleration();

	LSESP_vChkMpressStable();	    
}

#if !SIM_MATLAB

#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)
static void LSESP_vCalMpress(void)
{
	mpress_old=mpress;
	mpress_est_old=mpress_est;
	
	tempW3 = PRESS_M.s16premRaw -PRESS_M.s16premPressOfs_f;  // premium
//	tempW3 = raw_mpress ;  // premium
		
	if (mcp_signal_suspcs_flg)
	{
	    if (mcp_unstable_cnt>=L_U8_TIME_10MSLOOP_150MS)
	    {
	        if (BRAKE_PEDAL_ON) 
	        {
	            if (mpress_suspect_old>MPRESS_5BAR) 
	            {
	            	mpress = mpress_suspect_old;    
	            }
	            else                                
	            {
	            	mpress = MPRESS_5BAR;
	            }
	        }
	        else                                    
	        {
	        	mpress = 0;
	        }
	    }
	    else 
	    {
	        mpress = LCESP_s16Lpf1Int(tempW3,mpress_old,L_U8FILTER_GAIN_10MSLOOP_11_5HZ);
	    }   
	}  
	else
	{  
		tempW4=(int16_t)( (((int32_t)tempW3)*30)/100 ) ;
		
		mpress=tempW4+ (int16_t)( (((int32_t)mpress_old)*70)/100 ) ;   /* r : 0.1*/
		
		tempW3 = PRESS_M.s16premRaw - PRESS_M.s16premDrvOffset;  
		
		tempW4=(int16_t)( (((int32_t)tempW3)*60)/100 ) ;
		
		mpress_est= tempW4 + (int16_t)( (((int32_t)mpress_est_old)*40)/100 ) ;  
		
		mpress_suspect_old = MCpress[3];
	}
}

static void LSESP_vCalPressure(struct PREM_PRESS_OFFSET_STRUCT  *PRESS)
{
	PRESS->s16premFiltPressOld = PRESS->s16premFiltPress ;
	tempW3 = PRESS->s16premRaw - PRESS->s16premPressOfs_f; 
	PRESS->s16premFiltPress = LCESP_s16Lpf1Int(tempW3,PRESS->s16premFiltPressOld,L_U8FILTER_GAIN_10MSLOOP_7HZ);
	
	
	if(PRESS->s16premFiltPress <= 0)
	{
		PRESS->s16premFiltPress = 0;
	}
	else
	{
		;
	}
	
	 
}

#else

static void LSESP_vCalMpress(void)
{
    
#if __AHB_GEN3_SYSTEM == ENABLE || __IDB_SYSTEM == ENABLE

    lsesps16MpressByAHBgen3Old = lsesps16MpressByAHBgen3;
//
//    /* BCP1_OK / BCP2_OK */
//    if((liu1BCP1SenFaultDet==0)&&(fsu1BCP2SenFaultDet==0))
//    {
//        lsesps16MpressByAHBgen3 = (int16_t)((lsahbPrimarySen.lss16BCpresFilt + lsahbSecondarySen.lss16BCpresFilt)/2);       
//    }
//    /* BCP1_NG / BCP2_OK */    
//    else if((liu1BCP1SenFaultDet==1)&&(fsu1BCP2SenFaultDet==0))
//    {
//        lsesps16MpressByAHBgen3 = lsahbSecondarySen.lss16BCpresFilt;
//    }
//    /* BCP1_OK / BCP2_NG */    
//    else if((liu1BCP1SenFaultDet==0)&&(fsu1BCP2SenFaultDet==1))
//    {
//        lsesps16MpressByAHBgen3 = lsahbPrimarySen.lss16BCpresFilt;
//    }    
//    /* BCP1_NG / BCP2_NG */
//    else
//    {
//        lsesps16MpressByAHBgen3 = (int16_t)((lsahbPrimarySen.lss16BCpresFilt + lsahbSecondarySen.lss16BCpresFilt)/2);  
//    }    
//


    lsesps16MpressByAHBgen3 = lis16MasterPress;     
     
    PC_ESC.s16CircPressRaw = lis16MCPRawPress;
    SC_ESC.s16CircPressRaw = lis16MCP2RawPress;

    PC_ESC.s16CircPressFilt = LCESP_s16Lpf1Int(PC_ESC.s16CircPressRaw, PC_ESC.s16CircPressFilt, L_U8FILTER_GAIN_10MSLOOP_7HZ);
    SC_ESC.s16CircPressFilt = LCESP_s16Lpf1Int(SC_ESC.s16CircPressRaw, SC_ESC.s16CircPressFilt, L_U8FILTER_GAIN_10MSLOOP_7HZ);

    FL_ESC.s16CircPressFilt = SC_ESC.s16CircPressFilt;
    FR_ESC.s16CircPressFilt = PC_ESC.s16CircPressFilt;
    RL_ESC.s16CircPressFilt = PC_ESC.s16CircPressFilt;
    RR_ESC.s16CircPressFilt = SC_ESC.s16CircPressFilt;
     
    
    /* Mpress Validity Check */   
    if((liu1BCP1SenFaultDet==1) /*&&(fsu1BCP2SenFaultDet==1)*/ )
    {
        lsespu1MpresByAHBgen3Invalid = 1; 
    }
    else
    {
        lsespu1MpresByAHBgen3Invalid = 0;
    }
        
#else
    
	mpress_old=mpress;
	mpress_est_old=mpress_est;
			
	if (mcp_signal_suspcs_flg)
	{
	    if (mcp_unstable_cnt>=L_U8_TIME_10MSLOOP_150MS)
	    {
	        if (BRAKE_PEDAL_ON) 
	        {
	            if (mpress_suspect_old>MPRESS_5BAR) 
	            {
	            	mpress = mpress_suspect_old;    
	            }
	            else                                
	            {
	            	mpress = MPRESS_5BAR;
	            }
	        }
	        else                                    
	        {
	        	mpress = 0;
	        }
	    }
	    else 
	    {
	        mpress = LCESP_s16Lpf1Int(tempW3,mpress_old,L_U8FILTER_GAIN_10MSLOOP_11_5HZ);
	    }   
	}  
	else
	{  
		tempW4=(int16_t)( (((int32_t)tempW3)*40)/100 ) ;
		
		mpress=tempW4+ (int16_t)( (((int32_t)mpress_old)*60)/100 ) ;   /* r : 0.1*/
		
		tempW3 = raw_mpress -mpress_offset;  
		
		tempW4=(int16_t)( (((int32_t)tempW3)*60)/100 ) ;
		
		mpress_est= tempW4 + (int16_t)( (((int32_t)mpress_est_old)*40)/100 ) ;    
		
		mpress_suspect_old = MCpress[3];
	}

#endif

}

#endif

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
static void LSESP_vCalBrkAppSenSignal(void)
{
    #if (__USE_PDF_SIGNAL==ENABLE)
    
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	tempW3= lis16PDFRaw-lis16PDFofs;//lsahbs16PDFRaw - lsahbs16PDFofs;
  	#else
	tempW3= lsesps16BrkAppSenRaw - lsesps16BASOfsPDFFinal;  	
    #endif  	
  	
  	lsesps16BrkAppSenPDFFilt= LCESP_s16Lpf1Int(tempW3,lsesps16BrkAppSenPDFFilt,L_U8FILTER_GAIN_10MSLOOP_7HZ);
    #endif

    #if (__USE_PDT_SIGNAL==ENABLE)
    
    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
  	//tempW3=lsahbs16PDTRaw - lsahbs16PDTofs;
  	tempW3=lis16PDTRaw - lis16PDTofs;
  	#else  	
  	tempW3=lsesps16BrkAppSenRaw2 - lsesps16BASOfsPDTFinal;  	
    #endif  
  	
  	lsesps16BrkAppSenPDTFilt=LCESP_s16Lpf1Int(tempW3,lsesps16BrkAppSenPDTFilt,L_U8FILTER_GAIN_10MSLOOP_7HZ);  
    #endif
  	
   	#if (__USE_PDT_SIGNAL==ENABLE)
        #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if(liu1PdtSigSusSet ==0)
  	    #else        
        if(fu1EscPdTInvalid ==0)
        #endif                    
        {
    	    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenPDTFilt;
    	    lsespu1BrkAppSenInvalid= 0;
        }
      #if (__USE_PDF_SIGNAL==ENABLE)
        #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        else if(liu1PdfSigSusSet ==0)
  	    #else
        else if(fu1EscPdFInvalid ==0)  	                
        #endif
        {
    	    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenPDFFilt;
    	    lsespu1BrkAppSenInvalid= 0;
        }
      #endif
        else
        {
    	    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenPDTFilt;
		#if (__IDB_LOGIC==ENABLE)
			lsespu1BrkAppSenInvalid= 0;		/* liu1PdtSigSusSet 되고 있어서 임시로 막음 */
		#else
			lsespu1BrkAppSenInvalid= 1;
		#endif
        }
    #else   	
      #if (__USE_PDF_SIGNAL==ENABLE)
        #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if(liu1PdfSigSusSet ==0)
  	    #else
        if(fu1EscPdFInvalid ==0)
        #endif  	                
        {
    	    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenPDFFilt;
    	    lsespu1BrkAppSenInvalid= 0;
        }
        else
        {
    	    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenPDFFilt;
    	    lsespu1BrkAppSenInvalid= 1;            
        }
      #endif
    #endif

}

static void LSESP_vEstBrkPressByBrkPedal(void)
{
/*	
	if(lsesps16BrkAppSenFiltF <S16_PEDAL_TRAVEL_1)
	{
		lsesps16EstBrkPressByBrkPedal = MPRESS_0BAR ;
	}
	else
	{
		lsesps16EstBrkPressByBrkPedal = LCESP_s16IInter6Point(lsesps16BrkAppSenFiltF,                                 
														      S16_PEDAL_TRAVEL_1    , S16_PEDAL_TARGET_P_1  ,             
														      S16_PEDAL_TRAVEL_2    , S16_PEDAL_TARGET_P_2  ,             
														      S16_PEDAL_TRAVEL_3    , S16_PEDAL_TARGET_P_3  ,             
														      S16_PEDAL_TRAVEL_4    , S16_PEDAL_TARGET_P_4  ,             
														      S16_PEDAL_TRAVEL_5    , S16_PEDAL_TARGET_P_5  ,                        
//														      S16_PEDAL_TRAVEL_MAX  , S16_PEDAL_TARGET_P_MAX       );
														      S16_PEDAL_TRAVEL_MAX_TEMP  , S16_PEDAL_TARGET_P_MAX_TEMP       );

	}
*/

    lsesps16EstBrkPressByBrkPedal = lis16DriverIntendedTP;//ldahbs16DriverIntendedTP;
	
	/*lsesps16EstBrkPressByBrkPedalF =  LCESP_s16Lpf1Int(lsesps16EstBrkPressByBrkPedal,lsesps16EstBrkPressByBrkPedalF, L_U8FILTER_GAIN_10MSLOOP_3HZ);*/
	lsesps16EstBrkPressByBrkPedalF = lsesps16EstBrkPressByBrkPedal;
}
 
static void LSESP_vChkBrkPedalApp(void)
{
	if(lsespu1PedalBrakeOn==0)		
	{	
		if(lsesps16EstBrkPressByBrkPedalF >= (PRESS_BY_PEDAL_BRAKE_ON_THR + MPRESS_4BAR))
		{
			lsespu8EstBrkPressEnterCnt = lsespu8EstBrkPressEnterCnt + 5;
		}
		else if(lsesps16EstBrkPressByBrkPedalF >= (PRESS_BY_PEDAL_BRAKE_ON_THR + MPRESS_2BAR))	
		{
			lsespu8EstBrkPressEnterCnt = lsespu8EstBrkPressEnterCnt + 2;
		}
		else if(lsesps16EstBrkPressByBrkPedalF >= PRESS_BY_PEDAL_BRAKE_ON_THR )
		{
			lsespu8EstBrkPressEnterCnt = lsespu8EstBrkPressEnterCnt +1;
		}
		else
		{
			lsespu8EstBrkPressEnterCnt = 0;
		}
		if(lsespu8EstBrkPressEnterCnt > 30) lsespu8EstBrkPressEnterCnt=30;
			
		lsespu8EstBrkPressExitCnt = 0;
	}
	else
	{		
		if(lsesps16EstBrkPressByBrkPedalF <= PRESS_BY_PEDAL_BRAKE_ON_THR )
		{
			lsespu8EstBrkPressExitCnt = lsespu8EstBrkPressExitCnt + 2; /* 15/2 = 5 scan 유지 */
		}
		else if(lsesps16EstBrkPressByBrkPedalF <= (PRESS_BY_PEDAL_BRAKE_ON_THR + MPRESS_2BAR) )	
		{
			lsespu8EstBrkPressExitCnt = lsespu8EstBrkPressExitCnt + 1; /* 15/1 = 10 scan 유지 */
		}
		else 
		{
			lsespu8EstBrkPressExitCnt = 0;
		}

		if(lsespu8EstBrkPressExitCnt > 30) lsespu8EstBrkPressExitCnt= 30;
			
		lsespu8EstBrkPressEnterCnt = 0;
	}
	/*****************************/			

    if(lsespu1PedalBrakeOn==0)
    {
    	if(    ((lsesps16EstBrkPressByBrkPedalF >  PRESS_BY_PEDAL_BRAKE_ON_THR) && (lsespu8EstBrkPressEnterCnt >= 10))  
    		|| ((lsesps16EstBrkPressByBrkPedalF > (PRESS_BY_PEDAL_BRAKE_ON_THR + MPRESS_4BAR) ))  )
  	{
    		lsespu1PedalBrakeOn=1;
    		lsespu8EstBrkPressEnterCnt=0;
    	}
    	else
    	{
    		;
    	}
    }
    else
    {
    	if( (lsesps16EstBrkPressByBrkPedalF <= PRESS_BY_PEDAL_BRAKE_ON_THR ) && (lsespu8EstBrkPressExitCnt >= 15) )
    	{
        	lsespu1PedalBrakeOn=0;
    		lsespu8EstBrkPressEnterCnt = 0;
  	}
  	else
  	{
  	    ;
  	}
}
    
}

#else

static void LSESP_vCalBrkAppSenSignal_GM(void)
{
    
    int16_t S16PedalCoff1;
    
    S16PedalCoff1 = S16_PEDAL_MAX_TRAVEL_GM/255; 
    
    lsesps16BrkAppSenRawGM = (int16_t)(((int16_t)fcu8BrkPdlPos*S16PedalCoff1)/10);

    /* C529 Temp Code */
    lsesps16BASOfsFinalGM = 0;
    /* C529 Temp Code */
    
	tempW3= lsesps16BrkAppSenRawGM - lsesps16BASOfsFinalGM;
  	lsesps16BrkAppSenGMFilt = LCESP_s16Lpf1Int(tempW3, lsesps16BrkAppSenGMFilt, L_U8FILTER_GAIN_10MSLOOP_7HZ);     

    lsesps16BrkAppSenFiltF = lsesps16BrkAppSenGMFilt;

}

#endif

static void LSESP_vCalWheelSteeringAngle(void)
{
      
	#if __CAR==SYC_RAXTON_MECU
		c_steer_1_10deg2 = -c_steer_1_10deg2;
	#endif
	
	
        #if defined(__UCC_1)
         
    		#if defined(__AFS)
    			 	                                          
    			if ((umu16StrTagAng!=0) && (ABS_fz && ESP_SPLIT))//||(umu8AFSCombination==5)) 
    			{                            
        			tempW2=umu16AFSAngle;				                      
        			/* 상위 조향각 : 운전자 의지 */                       
        		} 
        		else 
        		{                                                
    				tempW2=c_steer_1_10deg2;		                      
    				/* 하위 조향각 */                                     
    			}
    		#else
    		                                                
    			tempW2=c_steer_1_10deg2;		                      
    			/* 하위 조향각 */                                     
    		#endif		                                              
    		 		
        #else    	
            tempW2=c_steer_1_10deg2;
    	#endif    		                                 
		 		
	
	/* steering sensor filtering : Cutoff 12 Hz*/
	wstr_old=wstr;
	tempW7 =tempW2-steer_offset_f;		// premium
	wstr= LCESP_s16Lpf1Int(tempW7, wstr_old, L_U8FILTER_GAIN_10MSLOOP_26HZ);
		
}

#if (__IDB_RIG_TEST==ENABLE)
static void LSESP_vCalYawRate(void)
{
	yaw_out_old=yaw_out;
	tempW1 = LCESP_s16IInter5Point(abs_wstr, STEER_15DEG, YAW_1G5DEG,
											STEER_100DEG, YAW_12DEG,
											STEER_200DEG, YAW_25DEG,
											STEER_300DEG, YAW_40DEG,
											STEER_450DEG, YAW_60DEG);
	if(wstr<0)
	{
		tempW1 = tempW1*(-1);
	}
	
	tempW3 = LCESP_s16IInter3Point(vref, VREF_25_KPH, 10,
										VREF_50_KPH, 15,
										VREF_100_KPH, 20);
	
	tempW2 = (int16_t)((int32_t)tempW3*tempW1/10);
	
	yaw_out = LCESP_s16Lpf1Int(tempW2, yaw_out_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
}
#else /* __IDB_RIG_TEST */
static void LSESP_vCalYawRate(void)
{

	/* yaw rate sensor filtering : Cutoff 7 Hz */
	yaw_out_old=yaw_out;
	/*
	KYAW.offset_filter_old = KYAW.offset_filter;
	esp_tempW9=yaw_offset_f;		
	KYAW.offset_filter = LCESP_s16Lpf1Int(esp_tempW9,KYAW.offset_filter_old,13);   	
	tempW2 = yaw_1_100deg2 - KYAW.offset_filter;              
	*/
	tempW2=yaw_1_100deg2 - yaw_offset_f;  // premium
	yaw_out = LCESP_s16Lpf1Int(tempW2, yaw_out_old, L_U8FILTER_GAIN_10MSLOOP_7HZ);    

}
#endif /* __IDB_RIG_TEST */

static void LSESP_vCalLateralAcceleration(void)
{

	/* Lateral G-sensor filtering : Cutoff 3 Hz */
		
	alatold=alat;
	tempW2=a_lat_1_1000g2 - alat_offset_f;
	alat=LCESP_s16Lpf1Int(tempW2, alatold, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
	
	abs_alat_diff=McrAbs(alat)-McrAbs(alatold);	
	
	alat_ems_old=alat_ems;
	alat_ems=LCESP_s16Lpf1Int(alat, alat_ems_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);
    
}

static void LSESP_vChkStrYawLg(void)
{
	if(!PHZ_detected_flg2)
	{ 
	    wstr_old=0;
	    wstr=0;
	}
	else
	{
		;
	}
	
	if((STR_OFFSET_360_OK==1)&&(WSTR_360_OK==0)) 
	{
		WSTR_360_OK=1; /*Don't Use this Flag! This Flag is dead!*/
	}
	else
	{
		;
	}
	
	/*
	if(fou1SenPwr1secOk==0)
	{
		mpress=0;
		yaw_out=0;
		alat=0;  
	}
	else
	{
		;
	}
	*/
	
	if(fu1YawSenPwr1sOk==0)
	{
		yaw_out=0;
	}
	else
	{
		;
	}
   
	if(fu1AySenPwr1sOk==0)
	{
		alat=0; 
	}
	else
	{
		;
	}

    #if ((__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC == DISABLE))

	if(fu1McpSenPwr1sOk==0)
	{
		mpress=0;
	}
	else
	{
		;
	}

    #endif

	if ((fu1YawSenPwr1sOk==1) && (fu1AySenPwr1sOk==1) && ((YAW_CDC_WORK_OLD==0)||(YAW_CDC_WORK==0))) 
	{
		LSESP_vChkInertialSensor();        
	}
	else    
	{
		KYAW.SUSPECT_FLG=0;
		KLAT.SUSPECT_FLG=0;
	}
	
	#if __STEER_SENSOR_TYPE==CAN_TYPE/*060602 for HM reconstruction by eslim*/
	KSTEER.SUSPECT_FLG=0;
	#elif __STEER_SENSOR_TYPE==ANALOG_TYPE        
	if((steer_int_sus_flg)||(steer_suspect_flg))    
	{
		KSTEER.SUSPECT_FLG=1;
	}
	else                                            
	{
		KSTEER.SUSPECT_FLG=0;
	}
	#else
	KSTEER.SUSPECT_FLG=0;
	#endif
}
#endif /*!SIM_MATLAB*/ 	


static void LSESP_vCalWheelSteeringVelocity(void)
{

	abs_wstr = McrAbs(wstr);
	
	lf_wstr_old = lf_wstr;
	lf_wstr2_old = lf_wstr2;    
	det_wstr_dot_old = det_wstr_dot;
	det_wstr_dot2_old = det_wstr_dot2;    
	det_wstr_dot_lf_old = det_wstr_dot_lf;
	det_wstr_dot_lf2_old = det_wstr_dot_lf2;
	det_abs_wstr_old = det_abs_wstr;
	det_abs_wstr2_old = det_abs_wstr2 ;
	
	
	if ( abs_wstr >= det_abs_wstr )
	{
		det_abs_wstr = LCESP_s16Lpf1Int(abs_wstr, det_abs_wstr_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
	}
	else
	{
		det_abs_wstr = LCESP_s16Lpf1Int(abs_wstr, det_abs_wstr_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
	}
	
	if ( abs_wstr >= det_abs_wstr2 )
	{
		det_abs_wstr2 = LCESP_s16Lpf1Int(abs_wstr, det_abs_wstr2_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
	}
	else
	{
		det_abs_wstr2 = LCESP_s16Lpf1Int(abs_wstr, det_abs_wstr2_old, L_U8FILTER_GAIN_10MSLOOP_0_2HZ);            
	}
	
	
	lf_wstr = LCESP_s16Lpf1Int(wstr, lf_wstr_old, L_U8FILTER_GAIN_10MSLOOP_2HZ);
	
	lf_wstr2 = LCESP_s16Lpf1Int(wstr, lf_wstr2_old, L_U8FILTER_GAIN_10MSLOOP_7HZ);    
	
	wstr_dot2 = (int16_t)( (((int32_t)( lf_wstr2 - lf_wstr2_old ) )*1000)/10 ) ;  /* 1000)/7 -> 1000)/10 20111018 KJS */       
	abs_wstr_dot2 = McrAbs(wstr_dot2);    
	
	
	wstr_dot = (int16_t)( (((int32_t)( lf_wstr-lf_wstr_old ) )*1000)/10 ) ; /* 1000)/7 -> 1000)/10 20111018 KJS */         
	    
	abs_wstr_dot = McrAbs(wstr_dot);
	
	
	
	if ( abs_wstr_dot >= det_wstr_dot ) 
	{
	    det_wstr_dot = LCESP_u16Lpf1Int(abs_wstr_dot, det_wstr_dot_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
	} 
	else 
	{
	    det_wstr_dot = LCESP_u16Lpf1Int(abs_wstr_dot, det_wstr_dot_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);
	}   
	if ( abs_wstr_dot2 >= det_wstr_dot2 ) 
	{		
	    det_wstr_dot2 = LCESP_u16Lpf1Int(abs_wstr_dot2, det_wstr_dot2_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
	} 
	else 
	{
	    det_wstr_dot2 = LCESP_u16Lpf1Int(abs_wstr_dot2, det_wstr_dot2_old, L_U8FILTER_GAIN_10MSLOOP_0_2HZ);
	}       
	
	
	det_wstr_dot_lf = LCESP_u16Lpf1Int(det_wstr_dot, det_wstr_dot_lf_old, L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
	
	
	esp_tempW5 = LCESP_s16IInter2Point( esp_mu, 400, L_U8FILTER_GAIN_10MSLOOP_0_2HZ, 600, L_U8FILTER_GAIN_10MSLOOP_1HZ); 
	
	if(det_wstr_dot <= det_wstr_dot_lf2)
	{
	    det_wstr_dot_lf2 = LCESP_u16Lpf1Int(det_wstr_dot, det_wstr_dot_lf2_old, (uint8_t)esp_tempW5);  
	}
	else
	{
		det_wstr_dot_lf2=det_wstr_dot;
	}
	
	if (WSTR_360_JUMP) 
	{
		det_wstr_dot_lf2=0;
	}
	else
	{
		;
	}
	
	if (!FLAG_COUNTER_STEER && (( (wstr > 300)&&(wstr_dot < -500) )|| ( (wstr < -300)&&(wstr_dot > 500) ) ) )
	{
		FLAG_COUNTER_STEER = 1;
	}
	else if ( (FLAG_COUNTER_STEER && (( (wstr > 10)&&(wstr_dot < 50) ) || ( (wstr < -10)&&(wstr_dot > -50) ) ) ) )
	{
	    FLAG_COUNTER_STEER = 1;
	}
	else
	{
	    FLAG_COUNTER_STEER = 0;  
	}       

}

static void LSESP_vCalYawRateAbsolute(void)
{
	abs_yaw_out = McrAbs(yaw_out);
	
	det_yaw_out_old = det_yaw_out;
	
	if ( abs_yaw_out >= det_yaw_out ) 
	{
	    det_yaw_out = LCESP_u16Lpf1Int(abs_yaw_out, det_yaw_out_old, L_U8FILTER_GAIN_10MSLOOP_7HZ); 
	}
	else 
	{
	    det_yaw_out = LCESP_u16Lpf1Int(abs_yaw_out, det_yaw_out_old, L_U8FILTER_GAIN_10MSLOOP_0_2HZ);   
	}
}

static void LSESP_vCalYawAcceleration(void)
{
 
	yawm_lf_old = yawm_lf;
	
	yawm_lf = LCESP_s16Lpf1Int(yaw_out, yawm_lf_old, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);  
	
	
	yawm_dot = (int16_t)( (((int32_t)( yawm_lf - yawm_lf_old ))*100)/10 ) ; /* 100)/7 -> 100)/10 20111018 KJS */
        
}

static void LSESP_vChkMpressStable(void)
{

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))

 		if((ETCS_ON==1)||(FTCS_ON==1)||(MOT_ON_FLG==1)||(BTCS_ON==1)||(YAW_CDC_WORK==1)
				|| (fu16CalVoltIntMOTOR >= MSC_5_V))
		{       	
			esp_tempW0 = MPRESS_2BAR; 			  	
		}
		else
		{
			esp_tempW0 = MPRESS_0BAR;
		}	
		/***********06' Winter ************/	
		if(lsespu1MpresBrkOnByAHBgen3==0)		
		{	
			if(lsesps16MpressByAHBgen3 >= MPRESS_8BAR)
			{
				lsespu8MpressByAHBgen3EnCT = lsespu8MpressByAHBgen3EnCT + 5;
			}
			else if(lsesps16MpressByAHBgen3 >= MPRESS_6BAR)	
			{
				lsespu8MpressByAHBgen3EnCT = lsespu8MpressByAHBgen3EnCT + 2;
			}
			else if(lsesps16MpressByAHBgen3 >= MPRESS_4BAR)
			{
				lsespu8MpressByAHBgen3EnCT = lsespu8MpressByAHBgen3EnCT +1;
			}
			else
			{
				lsespu8MpressByAHBgen3EnCT = 0;
			}
			if(lsespu8MpressByAHBgen3EnCT > 30) lsespu8MpressByAHBgen3EnCT=30;
			lsespu8MpressByAHBgen3ExCT = 0;
		}
		else
		{		
			if(lsesps16MpressByAHBgen3 <= MPRESS_4BAR)
			{
				lsespu8MpressByAHBgen3ExCT = lsespu8MpressByAHBgen3ExCT + 2; /* 15/2 = 7 scan 유지 */
			}
			else if(lsesps16MpressByAHBgen3 <= MPRESS_6BAR)	
			{
				lsespu8MpressByAHBgen3ExCT = lsespu8MpressByAHBgen3ExCT + 1; /* 15/1 = 15 scan 유지 */
			}
			else 
			{
				lsespu8MpressByAHBgen3ExCT = 0;
			}

			if(lsespu8MpressByAHBgen3ExCT > 30) lsespu8MpressByAHBgen3ExCT= 30;
				
			lsespu8MpressByAHBgen3EnCT = 0;
		}
		/*****************************/			
	
		if( ((lsespu1MpresBrkOnByAHBgen3==0)&&(lsesps16MpressByAHBgen3 > (MPRESS_4BAR + esp_tempW0))&&(lsespu8MpressByAHBgen3EnCT >= 10))
		    || ((lsespu1MpresBrkOnByAHBgen3==1) && (lsesps16MpressByAHBgen3 >= MPRESS_4BAR))
		    || ((lsespu1MpresBrkOnByAHBgen3==1) && (lsesps16MpressByAHBgen3 < MPRESS_4BAR) && (lsespu8MpressByAHBgen3ExCT < 15))
		    || (lsesps16MpressByAHBgen3 > MPRESS_8BAR) ) {  
   
        lsespu1MpresBrkOnByAHBgen3=1;
        lsesps8MpressByAHBgen3OnCT = 0;
        lsesps16MpressByAHBgen3CkCT++;
        if(lsesps16MpressByAHBgen3CkCT==1)
        {
            lsesps16MpressByAHBgen3Init=lsesps16MpressByAHBgen3;
        }
        else if(lsesps16MpressByAHBgen3CkCT>=4)
        {
            tempW0=lsesps16MpressByAHBgen3-lsesps16MpressByAHBgen3Init;
            lsesps16MpressByAHBgen3CkCT=0;

            if(tempW0<MPRESS_1BAR) 
            {
            	MPRESS_STABLE_FLAG=1;
            }
            else 
            {
            	MPRESS_STABLE_FLAG=0;
            }
        }
        else
        {
        	;
        }
    }
    else 
    {
    	if((lsespu1MpresBrkOnByAHBgen3==1)&&(lsespu8MpressByAHBgen3ExCT < 15)  )
    	{
    		;
   		} 
   		else if ( (lsespu1MpresBrkOnByAHBgen3==1)&&(lsespu8MpressByAHBgen3ExCT >= 15))
   		{
   			lsespu1MpresBrkOnByAHBgen3 = 0;
		} 
		else 
		{
        	lsespu1MpresBrkOnByAHBgen3=0;
			lsesps8MpressByAHBgen3OnCT = 0;
		}			
				
        lsesps16MpressByAHBgen3CkCT=0;
        lsesps16MpressByAHBgen3Init=0;
    }
                
#else
    
/* MASTER 압력이 변하지 않는 구간에서 일정시간동안 머물러 있을경우 MPRESS_STABLE_FLAG를 SET시킴*/
    
 		if((ETCS_ON==1)||(FTCS_ON==1)||(MOT_ON_FLG==1)||(BTCS_ON==1)||(YAW_CDC_WORK==1)
				|| (mot_mon_ad >= MSC_5_V))
		{       	
			esp_tempW0 = MPRESS_2BAR; 			  	
		}
		else
		{
			esp_tempW0 = MPRESS_0BAR;
		}	
		/***********06' Winter ************/	
		if(MPRESS_BRAKE_ON==0)		
		{	
			if(mpress >= MPRESS_8BAR)
			{
				mpress_count_en = mpress_count_en + 5;
			}
			else if(mpress >= MPRESS_6BAR)	
			{
				mpress_count_en = mpress_count_en + 2;
			}
			else if(mpress >= MPRESS_4BAR)
			{
				mpress_count_en = mpress_count_en +1;
			}
			else
			{
				mpress_count_en = 0;
			}
			if(mpress_count_en > 30) mpress_count_en=30;
			mpress_count_ex = 0;
		}
		else
		{		
			if(mpress <= MPRESS_4BAR)
			{
				mpress_count_ex = mpress_count_ex + 2; /* 15/2 = 7 scan 유지 */
			}
			else if(mpress <= MPRESS_6BAR)	
			{
				mpress_count_ex = mpress_count_ex + 1; /* 15/1 = 15 scan 유지 */
			}
			else 
			{
				mpress_count_ex = 0;
			}

			if(mpress_count_ex > 30) mpress_count_ex= 30;
				
			mpress_count_en = 0;
		}
		/*****************************/			
	
		if( ((MPRESS_BRAKE_ON==0)&&(mpress > (MPRESS_4BAR + esp_tempW0))&&(mpress_count_en >= 10))
		    || ((MPRESS_BRAKE_ON==1) && (mpress >= MPRESS_4BAR))
		    || ((MPRESS_BRAKE_ON==1) && (mpress < MPRESS_4BAR) && (mpress_count_ex < 15))
		    || (mpress > MPRESS_8BAR) ) {  
   
        MPRESS_BRAKE_ON=1;
        mpress_on_counter = 0;
        mpress_slop_check_count++;
        if(mpress_slop_check_count==1)
        {
            init_mpress=mpress;
        }
        else if(mpress_slop_check_count>=4)
        {
            tempW0=mpress-init_mpress;
            mpress_slop_check_count=0;

            if(tempW0<MPRESS_1BAR) 
            {
            	MPRESS_STABLE_FLAG=1;
            }
            else 
            {
            	MPRESS_STABLE_FLAG=0;
            }
        }
        else
        {
        	;
        }
    }
    else 
    {
    	if((MPRESS_BRAKE_ON==1)&&(mpress_count_ex < 15)  )
    	{
    		;
   		} 
   		else if ( (MPRESS_BRAKE_ON==1)&&(mpress_count_ex >= 15))
   		{
   			MPRESS_BRAKE_ON = 0;
		} 
		else 
		{
        	MPRESS_BRAKE_ON=0;
			mpress_on_counter = 0;
		}			
				
        mpress_slop_check_count=0;
        init_mpress=0;
    }  
 
#if defined(__ADDON_TCMF)   

   	if((!BLS_ERROR)&&(!BLS_EEPROM)) 
   	{
   		if(BLS == 0)
   		{
   			esp_tempW9 = MPRESS_40BAR;
   		}
   		else
   		{
   			esp_tempW9 = MPRESS_35BAR;
   		}   		
   	}
   	else
   	{
   		esp_tempW9 = MPRESS_40BAR;
   	}

	if((FLAG_ACTIVE_BOOSTER_PRE==1)&&(mpress <= esp_tempW9))
	{
		MPRESS_BRAKE_ON = 0;
	}
	else
	{
		;
	}
#endif

#endif

}


#if !SIM_MATLAB
static void LSESP_vChkInertialSensor(void)
{    


    /*if(stable_check_cnt !=0) 
    {
    	stable_check_cnt--;
    }
    else
    {
    	;
    	
    }

    if((McrAbs(wstr) >= WSTR_90_DEG)) 
    {
    	stable_check_cnt=L_U16_TIME_10MSLOOP_2S;  
    }
    else
    {
    	;
    }*/
    /*
    if((FL.Rough_road_detect_flag0==1)||(FR.Rough_road_detect_flag0==1)||
       (RL.Rough_road_detect_flag0==1)||(RR.Rough_road_detect_flag0==1)) 
    {
       	ROUGH_ROAD_FOR_YAW=1;
    }
    else 
    {
    	ROUGH_ROAD_FOR_YAW=0;
    }*/

    /*
    if((KYAW.mdl_ok_num >=2)&&(McrAbs(KYAW.max_2nd - KYAW.max_3rd) < YAW_4DEG)&&(vref5 >= VREF_20_KPH)&&(ROUGH_ROAD_FOR_YAW==0)&&(!YAW_CDC_WORK)&&(ABS_fz==0)&&(BTC_fz==0)&&(FLAG_ACTIVE_BRAKING==0))*/ /*&&(!MPRESS_BRAKE_ON)*/
    /*{
        if((McrAbs(KYAW.model_avr) <=YAW_4DEG)&&(McrAbs(KLAT.model_avr) <=LAT_0G2G)&&(stable_check_cnt==0)&&(steer_rate_deg_s2 <= 40)) 
        {
            if(KYAW.mdl_suspect_cnt <=L_U8_TIME_10MSLOOP_55MS) 
            {
            	tempW1 =McrAbs(yaw - KYAW.model_avr);
            }
            else 
            {
                tempW1 =McrAbs(yaw_out - KYAW.model_avr);
                tempW2 =McrAbs(yaw_out - yaw);
            }

            if(tempW1 >= YAW_5DEG) 
            {
                if(KYAW.mdl_suspect_cnt > 250) 
                {
                	KYAW.MDL_SUSPECT_FLG=1;
                }
                else 
                {
                    KYAW.mdl_suspect_cnt++;
                    if(KYAW.mdl_suspect_cnt >= L_U8_TIME_10MSLOOP_55MS) 
                    {
                    	KYAW.MDL_SUSPECT_FLG=1;
                    }
                    else 
                    {
                    	KYAW.MDL_SUSPECT_FLG=0;
                    }
                }
            }
            else if((KYAW.mdl_suspect_cnt >L_U8_TIME_10MSLOOP_55MS)&&(!KYAW.SUSPECT_DELAY)) 
            {  
                KYAW.MDL_SUSPECT_FLG=1;
                KYAW.SUSPECT_DELAY=1;
                KYAW.suspect_delay_timer =5;
                KYAW.mdl_suspect_cnt=0;
            }
            else if((KYAW.suspect_delay_timer > 0)&&(tempW2 >=YAW_0G5DEG)) 
            {
                KYAW.suspect_delay_timer--;
                KYAW.MDL_SUSPECT_FLG=1;
            }
            else 
            {
                KYAW.MDL_SUSPECT_FLG=0;
                KYAW.SUSPECT_DELAY=0;
                KYAW.mdl_suspect_cnt=0;
                KYAW.suspect_delay_timer=0;
            }
        }
        else 
        {
                KYAW.MDL_SUSPECT_FLG=0;
                KYAW.SUSPECT_DELAY=0;
                KYAW.mdl_suspect_cnt=0;
                KYAW.suspect_delay_timer=0;
        }
    }
    else 
    {
        KYAW.MDL_SUSPECT_FLG=0;
        KYAW.SUSPECT_DELAY=0;
        KYAW.mdl_suspect_cnt=0;
        KYAW.suspect_delay_timer=0;
    }

	if(McrAbs(yaw - yawold) >=YAW_5DEG) 
	{
        KYAW.PULSE_SUSPECT_FLAG=1;
        KYAW.pulse_suspect_cnt=8;
	}
	else if ((KYAW.pulse_suspect_cnt >0)&&(YAW.steady_flg)&&(YAW.residual>YAW_7DEG))
	{ */ /*FailSafe 요청에 의해서 추가.*/
	/*    KYAW.PULSE_SUSPECT_FLAG=1; 
	}   
	else if(KYAW.pulse_suspect_cnt >0) 
	{
	    KYAW.PULSE_SUSPECT_FLAG=1;
	    KYAW.pulse_suspect_cnt--;
	}                            
	else 
	{
	    KYAW.PULSE_SUSPECT_FLAG=0;
	    KYAW.pulse_suspect_cnt=0;      
	}

    KYAW.FAILSAFE_SUSPECT_FLAG=0; */
/*     
    if((KYAW.PULSE_SUSPECT_FLAG)||(KYAW.MDL_SUSPECT_FLG)||(YAW_G_GND_OPEN_FLG)||(KYAW.FAILSAFE_SUSPECT_FLAG)) KYAW.SUSPECT_FLG=1;
    else KYAW.SUSPECT_FLG=0;
    2005.1.27 China....*/
    KYAW.SUSPECT_FLG=0;
/*
  2003.12.08 kim yong kil
  ESP 작동후에 ESP 재진입 금지 개선   
      if(YAW_CDC_WORK) KYAW.SUSPECT_FLG=0;*/
    /*if((!YAW_G_GND_OPEN_FLG)&&(((YAW_CDC_WORK_OLD)&&(YAW_CDC_WORK))||(ESP_end_timer>(L_U16_TIME_10MSLOOP_5S-L_U8_TIME_10MSLOOP_35MS))||(ESP_TCS_ON_off_cnt>(L_U16_TIME_10MSLOOP_5S-L_U8_TIME_10MSLOOP_35MS)))) 
    {
    	KYAW.SUSPECT_FLG=0; 
    }
    else
    {
    	;
    }*/


	if((KLAT.mdl_ok_num >=2)&&(McrAbs(KLAT.max_2nd - KLAT.max_3rd) < LAT_0G2G)) 
	{
        tempW1 =McrAbs(alat - KLAT.model_avr);
        if(tempW1 > LAT_0G4G) 
        {
            if(KLAT.mdl_suspect_cnt++ < 0xff) 
            {
                if(KLAT.mdl_suspect_cnt >= L_U8_TIME_10MSLOOP_20MS) 
                {
                	KLAT.SUSPECT_FLG=1;
                }
                else
                {
                	;
                }
            }
            else
            {
            	;
            }
        }
        else 
        { 
        	KLAT.SUSPECT_FLG=0;  
        	KLAT.mdl_suspect_cnt=0; 
        }
    }
    else 
    {
        KLAT.SUSPECT_FLG=0;
        KLAT.mdl_suspect_cnt=0;
    }
}

#endif/*!SIM_MATLAB*/

#endif /*__VDC*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPFilterEspSensor
	#include "Mdyn_autosar.h"
#endif
