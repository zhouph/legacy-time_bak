/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LSHDCCallLogData.c
* Description:      Logging Data of HDC
* Logic version:    CV3
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  6120         jbchoi          Initial Release
********************************************************************************/


/* Includes ********************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSHDCCallLogData
	#include "Mdyn_autosar.h"
#endif


#include "LSHDCCallLogData.h"
  #if __HDC || __DEC || __HSA
#include "LSCallLogData.h"
#include "LCACCCallControl.h"
#include "LCHDCCallControl.h"
#include "LCHSACallControl.h"
#include "LCDECCallControl.h"
#include "LAACCCallActHW.h"
#include "LAHDCCallActHW.h"
#include "LADECCallActHW.h"
#include "LADECCallActCan.h"
#include "Hardware_Control.h"
#include "LCABSDecideCtrlState.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LSABSEstLongAccSensorOffsetMon.h"


/* �ӽ� */
#include "LDESPEstBeta.h"
#include "LDABSCallEstVehDecel.H"
#include "LDABSCallVrefCompForESP.H"
//#include "../System/WMonPort.h"

   #if __TCMF_ENABLE
    #if !SIM_MATLAB
//#include "../System/WContlADC.h"
    #endif
#include "algo_var.h"
   #endif
  #endif

/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

/* Local Function prototype ****************************************************/

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LSHDC_vCallHDCLogData
* CALLED BY:            LS_vCallLogData()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Logging Data of HDC Control
********************************************************************************/
  #if defined(__LOGGER) && (__LOGGER_DATA_TYPE==1)
void LSHDC_vCallHDCLogData(void)
{
    uint8_t i;

      #if __HDC || __DEC || __HSA
    /***********************************************************************/
    CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);                /* 01 */
    CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);
    CAN_LOG_DATA[2]  = (uint8_t)(vrad_fl);                            /* 02 */
    CAN_LOG_DATA[3]  = (uint8_t)(vrad_fl>>8);
    CAN_LOG_DATA[4]  = (uint8_t)(vrad_fr);                            /* 03 */
    CAN_LOG_DATA[5]  = (uint8_t)(vrad_fr>>8);
    CAN_LOG_DATA[6]  = (uint8_t)(vrad_rl);                            /* 04 */
    CAN_LOG_DATA[7]  = (uint8_t)(vrad_rl>>8);
    CAN_LOG_DATA[8]  = (uint8_t)(vrad_rr);                            /* 05 */
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_rr>>8);

    CAN_LOG_DATA[10] = (uint8_t)(vref5);                              /* 06 */
    CAN_LOG_DATA[11] = (uint8_t)(vref5>>8);

#if __Voltage_Resol_Change
    CAN_LOG_DATA[12] = (uint8_t)((int16_t)target_vol/100);                 	/* 07 */
    CAN_LOG_DATA[13] = (uint8_t)((int16_t)fu16CalVoltMOTOR/100);                 /* 08 */
#else
    CAN_LOG_DATA[12] = (uint8_t)((target_vol*10)/51);                 /* 07 */
    CAN_LOG_DATA[13] = (uint8_t)((mot_mon_ad*10)/51);                 /* 08 */
#endif

    CAN_LOG_DATA[14] = (uint8_t)(vref);                               /* 09 */
    CAN_LOG_DATA[15] = (uint8_t)(vref>>8);
    /***********************************************************************/
      #if __HDC
    CAN_LOG_DATA[16] = (uint8_t)(lcu8HdcSmaThetaGCnt);                /* 10 */
    CAN_LOG_DATA[17] = (uint8_t)(lcu8HdcSmaThetaGNCnt);               /* 11 */
    CAN_LOG_DATA[18] = (uint8_t)(0);                             /* 12 */
    CAN_LOG_DATA[19] = (uint8_t)(lcu8HdcSmaThetaGEstTime);            /* 13 */
        CAN_LOG_DATA[20] = (uint8_t)(lcs16HdcMaxDiscTemp/320);        /* 14 */
        CAN_LOG_DATA[21] = (uint8_t)(lcu8HdcBumpGCnt);                /* 15 */
        CAN_LOG_DATA[22] = (uint8_t)(lcu8HdcFlatPreCnt);              /* 16 */
        CAN_LOG_DATA[23] = (uint8_t)(lcu8HdcFlatGCnt);                /* 17 */
      #endif
    /***********************************************************************/
      #if __HDC
    CAN_LOG_DATA[24] = (uint8_t)(lcs16HdcTargetSpeed);                /* 18 */
    CAN_LOG_DATA[25] = (uint8_t)(lcs16HdcTargetSpeed>>8);
    CAN_LOG_DATA[26] = (uint8_t)(lcs16HdcYawFactorTh);                /* 19 */
    CAN_LOG_DATA[27] = (uint8_t)(lcs16HdcYawFactorTh>>8);
    CAN_LOG_DATA[28] = (uint8_t)(lcs16HdcThetaG);                     /* 20 */
    CAN_LOG_DATA[29] = (uint8_t)(lcs16HdcThetaG>>8);
      #endif
      #if __DEC
    CAN_LOG_DATA[30] = (uint8_t)(lcs16DecCtrlInput);                  /* 21 */
    CAN_LOG_DATA[31] = (uint8_t)(lcs16DecCtrlInput>>8);
      #endif
      #if __HDC
    CAN_LOG_DATA[32] = (uint8_t)(lcs16HdcCtrlInput);                  /* 22 */
    CAN_LOG_DATA[33] = (uint8_t)(lcs16HdcCtrlInput>>8);

    CAN_LOG_DATA[34] = (uint8_t)(lcs16HdcThetaG0);                    /* 23 */
    CAN_LOG_DATA[35] = (uint8_t)(lcs16HdcThetaG0>>8);

    CAN_LOG_DATA[36] = (uint8_t)(lcs16HdcThetaG1);                    /* 24 */
    CAN_LOG_DATA[37] = (uint8_t)(lcs16HdcThetaG1>>8);
      #endif

    /***********************************************************************/
      #if __HDC
    CAN_LOG_DATA[38] = (uint8_t)(lcu8HdcGearPosition);                /* 25 */
      #endif
    CAN_LOG_DATA[39] = (uint8_t)mtp;                                  /* 26 */
    /***********************************************************************/
    CAN_LOG_DATA[40] = (uint8_t)(wstr);                               /* 27 */
    CAN_LOG_DATA[41] = (uint8_t)(wstr>>8);
    CAN_LOG_DATA[42] = (uint8_t)(yaw_out);                            /* 28 */
    CAN_LOG_DATA[43] = (uint8_t)(yaw_out>>8);
      #if __DEC
    CAN_LOG_DATA[44] = (uint8_t)(lcs16DecTargetG);                    /* 29 */
    CAN_LOG_DATA[45] = (uint8_t)(lcs16DecTargetG>>8);
      #endif
	  #if __HDC
    CAN_LOG_DATA[46] = (uint8_t)(lcs16HdcDecel);                      /* 30 */
    CAN_LOG_DATA[47] = (uint8_t)(lcs16HdcDecel>>8);
    CAN_LOG_DATA[48] = (uint8_t)(lcs16HdcASen);                       /* 31 */
    CAN_LOG_DATA[49] = (uint8_t)(lcs16HdcASen>>8);

    CAN_LOG_DATA[50] = (uint8_t)(lcs16HdcExThetaGTh);                 /* 32 */
    CAN_LOG_DATA[51] = (uint8_t)(lcs16HdcExThetaGTh>>8);
      #endif
      #if __DEC
    CAN_LOG_DATA[52] = (uint8_t)(lcs16DecDecel);                      /* 33 */
    CAN_LOG_DATA[53] = (uint8_t)(lcs16DecDecel>>8);
      #endif
      #if __HDC
    CAN_LOG_DATA[54] = (uint8_t)(lcs16HdcSmaThetaGTh);                /* 34 */
    CAN_LOG_DATA[55] = (uint8_t)(lcs16HdcSmaThetaGTh>>8);
      #endif
    CAN_LOG_DATA[56] = (uint8_t)(alat);                               /* 35 */
    CAN_LOG_DATA[57] = (uint8_t)(alat>>8);
    /***********************************************************************/

      #if __DEC
    CAN_LOG_DATA[58] = (uint8_t)(lcs16DecTargetG);                    /* 36 */
    CAN_LOG_DATA[59] = (uint8_t)(lcs16DecTargetG>>8);
       #if __TCMF_ENABLE
    CAN_LOG_DATA[60] = (uint8_t)(TC_MFC_Voltage_Secondary_dash);      /* 37 */
    CAN_LOG_DATA[61] = (uint8_t)(TC_MFC_Voltage_Secondary_dash>>8);
    CAN_LOG_DATA[62] = (uint8_t)(TC_MFC_Voltage_Primary_dash);        /* 38 */
    CAN_LOG_DATA[63] = (uint8_t)(TC_MFC_Voltage_Primary_dash>>8);
       #endif
      #endif
    CAN_LOG_DATA[64] = (uint8_t)(mpress);                             /* 39 */
    CAN_LOG_DATA[65] = (uint8_t)(mpress>>8);
    /***********************************************************************/
    i=0;
    /***********************************************************************/
      #if __HDC
    if(lcu1HdcSwitchFlg==1)                 i|=0x01;                    /* 01 */
    if(lcu1HdcActiveFlg==1)                 i|=0x02;                    /* 02 */
    if(lcu1HdcDownhillFlg==1)               i|=0x04;                    /* 03 */
    if(lcu1HdcDownhillAfeterStopFlg==1)     i|=0x08;                    /* 04 */
    if(lcu1HdcOverCreepSpeedFlg==1)         i|=0x10;                    /* 05 */  
    if(lcu1HdcInhibitFlg==1)                i|=0x20;                    /* 06 */
      #endif
      #if __DEC
    if(lcu1DecCtrlReqFlg==1)                   i|=0x40;                    /* 07 */
      #endif
    if(AUTO_TM==1)                          i|=0x80;                    /* 08 */

    CAN_LOG_DATA[66] = (uint8_t)(i);                                      /* 40 */
    i=0;

    /***********************************************************************/
      #if __HDC
    if(lcu1HdcRightGearFlg==1)             i|=0x01;                    /* 09 */
      #endif
    if(MOT_ON_FLG==1)                       i|=0x02;                    /* 10 */
    if(ABS_fz==1)                           i|=0x04;                    /* 11 */
    if(EBD_RA==1)                           i|=0x08;                    /* 12 */
	if(AUTO_TM==0)
	{
      #if __HDC		
    	if(lcu1hdcPowerEngageAtClutchSWon==1)	i|=0x10;                /* 13 */
    	if(lcu1hdcPowerEngage==1)           i|=0x20;                    /* 14 */
        if(lcs16HdcThetaG_N_OK_Flg==1)      i|=0x40;                    /* 15 */
    	if(lcu1HdcEngTorq_N_OKFlg==1)       i|=0x80;                    /* 16 */
      #endif
    }
    else
    {
    if(YAW_CDC_WORK==1)                     i|=0x10;                    /* 13 */
    if(ESP_TCS_ON==1)                       i|=0x20;                    /* 14 */
    if(FLAG_ESP_CONTROL_ACTION==1)          i|=0x40;                    /* 15 */
    if(ACTIVE_BRAKE_FLG==1)                 i|=0x80;                    /* 16 */
	}
 
    CAN_LOG_DATA[67] = (uint8_t)(i);                                      /* 40 */
    i=0;
    /***********************************************************************/
    if(AV_VL_fl==1)                         i|=0x01;                    /* 17 */
    if(HV_VL_fl==1)                         i|=0x02;                    /* 18 */
    if(ABS_fl==1)                           i|=0x04;                    /* 19 */
      #if __HDC
    if(lcu1hdcVehicleForwardSensor==1)      i|=0x08;                    /* 20 */
      #endif
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_SECONDARY==1)             i|=0x10;                    /* 21 */
      #else
    if(TCL_DEMAND_fl==1)                    i|=0x10;                    /* 21 */
      #endif
      #if __HDC
    if(lcu1hdcEngNoverload==1)    			i|=0x20;                    /* 22 */
    if(lcu1hdcVehicleForwardThetaG==1)      i|=0x40;                    /* 23 */
    if(lcu1hdcVehicleBackwardThetaG==1)     i|=0x80;                    /* 24 */
      #endif

    CAN_LOG_DATA[68] = (uint8_t)(i);                                      /* 40 */
    i=0;
    /***********************************************************************/
    if(AV_VL_fr==1)                         i|=0x01;                    /* 25 */
    if(HV_VL_fr==1)                         i|=0x02;                    /* 26 */
    if(ABS_fr==1)                           i|=0x04;                    /* 27 */
      #if __HDC
    if(lcu1hdcVehicleForwardYaw==1)     	i|=0x08;                    /* 28 */
      #endif
      #if (__SPLIT_TYPE==1)
    if(TCL_DEMAND_PRIMARY==1)               i|=0x10;                    /* 29 */
      #else
    if(TCL_DEMAND_fr==1)                    i|=0x10;                    /* 29 */
      #endif
      #if __HDC
    if(lcu1hdcVehicleBackwardYaw==1)    	i|=0x20;                    /* 30 */
    if(lcu1hdcGearR_Switch==1)              i|=0x40;                	/* 31 */
    if(lcu1hdcClutchSwitch==1)     			i|=0x80;                	/* 32 */
      #endif
    CAN_LOG_DATA[69] = (uint8_t)(i);                                      /* 40 */
    i=0;
    /***********************************************************************/
    if(AV_VL_rl==1)                         i|=0x01;                    /* 33 */
    if(HV_VL_rl==1)                         i|=0x02;                    /* 34 */
    if(ABS_rl==1)                           i|=0x04;                    /* 35 */
      #if __HDC
    if(lcu1HdcVehicleDirbySensorFlg==1)     i|=0x08;                    /* 36 */
    if(lcu1HdcVehicleDirbySensorSusFlg==1)  i|=0x10;                    /* 37 */
      #endif
    if ((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)
        ||(FL.Rough_road_detect_flag2==1)||(FR.Rough_road_detect_flag2==1)
        ||(RL.Rough_road_detect_flag2==1)||(RR.Rough_road_detect_flag2==1))
                                            i|=0x20;                    /* 38 */

      #if (__SPLIT_TYPE==1)
    if(S_VALVE_SECONDARY==1)                i|=0x40;                    /* 39 */
      #else
    if(S_VALVE_LEFT==1)                     i|=0x40;                    /* 39 */
      #endif
      #if __HSA
    if(HSA_flg==1)                          i|=0x80;                    /* 40 */
      #endif
    CAN_LOG_DATA[70] = (uint8_t)(i);                                      /* 40 */
    i=0;
    /***********************************************************************/
    if(AV_VL_rr==1)                         i|=0x01;                    /* 41 */
    if(HV_VL_rr==1)                         i|=0x02;                    /* 42 */
    if(ABS_rr==1)                           i|=0x04;                    /* 43 */
      #if __HDC
    if(lcu1hdcVehicleForward==1)            i|=0x08;                    /* 44 */
    if(lcu1hdcVehicleBackward==1)           i|=0x10;                    /* 45 */
    if(lcu1hdcVehicleDirectionOKFlg==1) 	i|=0x20;                    /* 46 */
      #endif
      #if (__SPLIT_TYPE==1)
    if(S_VALVE_PRIMARY==1)                  i|=0x40;                    /* 47 */
      #else
    if(S_VALVE_RIGHT==1)                    i|=0x40;                    /* 47 */
      #endif
    if(SAS_CHECK_OK==1)                     i|=0x80;                    /* 48 */
    CAN_LOG_DATA[71] = (uint8_t)(i);                                      /* 40 */
    /***********************************************************************/

      #if __HDC
    CAN_LOG_DATA[72] = (uint8_t)(eng_torq_rel);                        /* 46 */
    CAN_LOG_DATA[73] = (uint8_t)(eng_torq_rel>>8);
        CAN_LOG_DATA[74] = (uint8_t)(eng_rpm);                            /* 47 */
        CAN_LOG_DATA[75] = (uint8_t)(eng_rpm>>8);
    /***********************************************************************/
        CAN_LOG_DATA[76] = (uint8_t)(lcu8HdcBumpEstTime);                 /* 48 */
        CAN_LOG_DATA[77] = (uint8_t)(lcu8HdcBumpDelayActTime);            /* 49 */
        CAN_LOG_DATA[78] = (uint8_t)(lcu8HdcBumpSuspect_cnt);             /* 50 */
        CAN_LOG_DATA[79] = (uint8_t)(lcu8HdcBumpSuspect);                 /* 51 */
          #endif
    /***********************************************************************/
      #endif
}
  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSHDCCallLogData
	#include "Mdyn_autosar.h"
#endif  
