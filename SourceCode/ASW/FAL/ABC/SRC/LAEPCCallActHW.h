#ifndef __LAEPCCALLACTHW_H__
#define __LAEPCCALLACTHW_H__


/*includes********************************************************************/
#include "LVarHead.h"

/*Global Type Declaration ****************************************************/
#define EPC_HV_VL_fl                        EPCA0.bit0
#define EPC_AV_VL_fl                        EPCA0.bit1
#define EPC_HV_VL_fr                        EPCA0.bit2
#define EPC_AV_VL_fr                        EPCA0.bit3
#define EPC_HV_VL_rl                        EPCA0.bit4
#define EPC_AV_VL_rl                        EPCA0.bit5
#define EPC_HV_VL_rr                        EPCA0.bit6
#define EPC_AV_VL_rr                        EPCA0.bit7

#define EPC_S_VALVE_PRIMARY                 EPCA1.bit0
#define EPC_S_VALVE_SECONDARY               EPCA1.bit1
#define EPC_TCL_DEMAND_PRIMARY              EPCA1.bit2
#define EPC_TCL_DEMAND_SECONDARY            EPCA1.bit3
#define EPC_ON                              EPCA1.bit4
#define EPC_flag_01_5                       EPCA1.bit5
#define EPC_flag_01_6                       EPCA1.bit6
#define EPC_flag_01_7                       EPCA1.bit7

#define FL_PRE_CIRCULATION                  EPCA2.bit0
#define FR_PRE_CIRCULATION                  EPCA2.bit1
#define RL_PRE_CIRCULATION                  EPCA2.bit2
#define RR_PRE_CIRCULATION                  EPCA2.bit3
#define EPC_flag_02_4                       EPCA2.bit4
#define EPC_flag_02_5                       EPCA2.bit5
#define EPC_flag_02_6                       EPCA2.bit6
#define EPC_flag_02_7                       EPCA2.bit7

/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t EPCA0;
extern U8_BIT_STRUCT_t EPCA1;
extern U8_BIT_STRUCT_t EPCA2;


/*Global Extern Functions  Declaration****************************************/


#endif
