
/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDENGCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ********************************************************/

#include "LDENGCallDetection.h"
/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/

#if __TCS || __ETC || __EDC // ABS_PLUS_ECU 적용시 macro 삭제 예정

/* Local Function prototype ****************************************/

void LDENG_vCallDetection(void);

/* Implementation***************************************************/

void LDENG_vCallDetection(void)
{
	LDENG_vEstimatPowertrain();
}
	
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDENGCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
