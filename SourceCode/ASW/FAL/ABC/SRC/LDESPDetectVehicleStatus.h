#ifndef __LDESPDETECTVEHICLESTATUS_H__
#define __LDESPDETECTVEHICLESTATUS_H__
/*Includes *********************************************************************/
#include "LVarHead.h"


/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/
#define __EUROPE_ADAC				        0 /*EHC - Evasive Hanling Control*/
#if (__CRAM==1)&&(__EUROPE_ADAC == 1)	 
extern int16_t	moment_pre_front_adac_preact;
extern struct	U8_BIT_STRUCT_t ADACSTATE_0  ;
extern struct	U8_BIT_STRUCT_t ADACSTATE_1  ;
extern int16_t	adac_ttp_cnt_fl;
extern int16_t	adac_ttp_cnt_fr;

#define ldu1espAyStartADAC_FR    					ADACSTATE_0.bit7
#define ldu1espAyStartADAC_FL    					ADACSTATE_0.bit6
#define Not_used_0_5								ADACSTATE_0.bit5
#define ldu1espWstrStartADACFL						ADACSTATE_0.bit4
#define lcu1espADACPreactFRT						ADACSTATE_0.bit3
#define lcu1espADACPreact_FL						ADACSTATE_0.bit2
#define lcu1espADACPreact_FR						ADACSTATE_0.bit1
#define Not_used_0_0								ADACSTATE_0.bit0

#define Not_used_1_7					ADACSTATE_1.bit7
#define Not_used_1_6					ADACSTATE_1.bit6
#define ldu1espWstrStartADAC_FL					ADACSTATE_1.bit5
#define ldu1espWstrStartADAC_FR					ADACSTATE_1.bit4
#define ldu1espAyStartADAC_FL_old				ADACSTATE_1.bit3
#define ldu1espAyStartADAC_FR_old				ADACSTATE_1.bit2
#define Not_used_1_1							ADACSTATE_1.bit1		
#define Not_used_1_0							ADACSTATE_1.bit0					
#endif
#define S16_EHC_PEAKED_SPEED_V1       		     400	
#define S16_EHC_PEAKED_SPEED_V2              	     600	                                     	        	
#define S16_EHC_PEAKED_SPEED_V3              	     800                                     	        	
#define S16_EHC_PEAKED_SPEED_V4              	    1000                                     	        	
#define S16_EHC_PEAKED_SPEED_V5              	    1200                                     	        	
#define S16_EHC_WSTR_DOT_ENT_100K            	    1800                                     	        	
#define S16_EHC_WSTR_DOT_ENT_120K            	    1000                                     	        	
#define S16_EHC_WSTR_DOT_ENT_40K             	    8500                                     	        	
#define S16_EHC_WSTR_DOT_ENT_50K             	    3500                                     	        	
#define S16_EHC_WSTR_DOT_ENT_60K             	    3000                                     	        	
#define S16_EHC_WSTR_DOT_ENT_70K             	    2000                                    	        	
#define S16_EHC_WSTR_DOT_ENT_80K             	    2000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_100K            	    1000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_120K            	     800                                     	        	
#define S16_EHC_WSTR_DOT_EXT_40K             	    8000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_50K             	    3000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_60K             	    2000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_70K             	    1000                                     	        	
#define S16_EHC_WSTR_DOT_EXT_80K             	    1000                                     	        	
#define S16_EHC_YAW_DOT_ENT_100K             	     60                                     	        	
#define S16_EHC_YAW_DOT_ENT_120K             	     60                                     	        	
#define S16_EHC_YAW_DOT_ENT_40K              	    180                                     	        	
#define S16_EHC_YAW_DOT_ENT_50K              	    160                                     	        	
#define S16_EHC_YAW_DOT_ENT_60K              	      90                                     	        	
#define S16_EHC_YAW_DOT_ENT_70K              	      70                                     	        	
#define S16_EHC_YAW_DOT_ENT_80K              	      60                                     	        	
#define S16_EHC_YAW_DOT_EXT_100K             	     50                                      	        	
#define S16_EHC_YAW_DOT_EXT_120K             	     50                                    	        	
#define S16_EHC_YAW_DOT_EXT_40K              	     160                                     	        	
#define S16_EHC_YAW_DOT_EXT_50K              	     110                                     	        	
#define S16_EHC_YAW_DOT_EXT_60K              	      90                                     	        	
#define S16_EHC_YAW_DOT_EXT_70K              	      70                                     	        	
#define S16_EHC_YAW_DOT_EXT_80K              	      50                                      	        	
#define S16_EHC_PEAKED_WSTR_V1               	    2500                                     	        	
#define S16_EHC_PEAKED_WSTR_V2               	    1200                                     	        	
#define S16_EHC_PEAKED_WSTR_V3               	    1000                                     	        	
#define S16_EHC_PEAKED_WSTR_V4               	     900                                     	        	
#define S16_EHC_PEAKED_WSTR_V5               	     900                                     	        	
#define S16_EHC_PEAKED_YAW_V1                	    3000                                     	        	
#define S16_EHC_PEAKED_YAW_V2                	    2000                                     	        	
#define S16_EHC_PEAKED_YAW_V3                	    1000                                      	        	
#define S16_EHC_PEAKED_YAW_V4                	    1000                                    	        	
#define S16_EHC_PEAKED_YAW_V5                	     800                                     	        	
#define S16_EHC_2ND_ENGAGE_SPEED_V1          	    1000                                       	        	
#define S16_EHC_2ND_ENGAGE_SPEED_V2          	    1050                                       	        	
#define S16_EHC_2ND_ENGAGE_V1_WSTR           	      50 
#define S16_EHC_2ND_ENGAGE_V1_YAW            	     500	                                  	        	
#define S16_EHC_2ND_ENGAGE_V2_WSTR           	      50	                                     	        	
#define S16_EHC_2ND_ENGAGE_V2_YAW            	     100	
                                     	        	
#define U8_OVER2WHEEL_ENTER_EST_P 0

#define U8_ON_CENTER_DCT_CNT1 42
#define U8_ON_CENTER_DCT_CNT2 18
#define U8_ON_CENTER_DCT_CNT3 45   
#define U8_ON_CENTER_DCT_CYCLE 3

#define U8_SLALOM_DCT_CNT1 245
#define U8_SLALOM_DCT_CNT2  50
#define U8_SLALOM_DCT_CNT3 245

#define S16_YAW_LIMT_HIGHMU                 750
#define S16_YAW_LIMIT_DELTA_LOW_VSLOW_ABS   400
#define S16_YAW_LIMIT_DELTA_LOW_VSLOW       300
#define S16_YAW_LIMIT_DELTA_HIGH_VSLOW      200
#define S16_YAW_LIMIT_DELTA_HIGH_VSLOW_ABS  300

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
extern int16_t del_target_2wheel_slip;
extern int16_t lcesps16Over2wheelEnterMoment;
extern int16_t lcesps16Over2wheelExitMoment;


#if (__ESC_CYCLE_DETECTION)
extern uint8_t  ldespu8ESCCycle;
extern uint8_t  ldespu8ESCCycleOld;
extern uint8_t  ldespu8ESCControlWheel;
extern uint8_t  ldespu8ESCControlWheelOld;
extern uint8_t  ldespu8ESCControlWheel2ndOld;
extern int16_t  lcesps16ESCCycleCount;

//extern uint8_t  ldespu8CycleDetQualifiedCnt;

#endif


/*Global Extern Functions Declaration ******************************************/
extern void LDESP_vDetectCombControl(void);

extern void LDESP_vCalLatAccAbsolute(void);
extern void LDESP_vEstRoadAdhesion(void);
extern void LDESP_vLimitYawRateDesired(void);

#if (__ESC_CYCLE_DETECTION)
extern void LDESP_vDetESCCycle(void);
#endif

extern int16_t lcs16US2WH_FrtDelM;
extern uint8_t lcu8US2WH_count;

 /* CRAM */
#if __CRAM
#define __CRAM_FRONT_2WH_ENABLE    0 
#define __CRAM_PRE_FRONT_W_CRTL		 1

#if (__CRAM_PRE_FRONT_W_CRTL == 1) 
extern void LDESP_vCheckPreCRAM(void);
#endif

extern struct	U8_BIT_STRUCT_t ESCSTATE_0  ;
extern struct	U8_BIT_STRUCT_t ESCSTATE_1  ;
extern struct	U8_BIT_STRUCT_t ESCSTATE_3  ;

#define ldu1espDetectCRAM    				ESCSTATE_0.bit7
#define ldu1espDetectUnderLimit    	ESCSTATE_0.bit6
#define ldu1espDetectCRAM_FL				ESCSTATE_0.bit5
#define ldu1espDetectCRAM_FR				ESCSTATE_0.bit4
#define lcu1espCramPreactFRT				ESCSTATE_0.bit3
#define lcu1espCramPreact_FL				ESCSTATE_0.bit2
#define lcu1espCramPreact_FR				ESCSTATE_0.bit1
#define ldu1espCramDynStabPhase			ESCSTATE_0.bit0

#define ldu1espAyStartCRAM_FL				ESCSTATE_1.bit7
#define ldu1espAyStartCRAM_FR				ESCSTATE_1.bit6
#define ldu1espWstrStartCRAM_FL			    ESCSTATE_1.bit5
#define ldu1espWstrStartCRAM_FR			    ESCSTATE_1.bit4
#define ldu1espAyStartCRAM_FL_old		    ESCSTATE_1.bit3
#define ldu1espAyStartCRAM_FR_old		    ESCSTATE_1.bit2
#define ldu1espDetSteerPeak_FR		        ESCSTATE_1.bit1
#define ldu1espDetSteerPeak_FL		        ESCSTATE_1.bit0

#define ESC_STATUS_not_using_3_7				ESCSTATE_3.bit7
#define ESC_STATUS_not_using_3_6					ESCSTATE_3.bit6
#define ldu1espCRAMpreactWithRear_RL			ESCSTATE_3.bit5
#define ldu1espCRAMpreactWithRear_RR			ESCSTATE_3.bit4
#define ldu1espDetYawPeak_FR					ESCSTATE_3.bit3
#define ldu1espDetYawPeak_FL					ESCSTATE_3.bit2
#define ldu1espDetSteerPeak_FR					ESCSTATE_3.bit1
#define ldu1espDetSteerPeak_FL					ESCSTATE_3.bit0

/************* CRAM Tuning Parameter *************/    

#define 		S16_CRAM_ENTER_ALAT						500     

#define 		S16_CRAM_IN_FRT_SLIP_LMT      1400	
#define 		S16_CRAM_PREFILL_MPRESS_TH		S16_ROP_PREFILL_MPRESS_TH
        		
extern int16_t lds16espDelVcram;
extern int16_t lds16espVelcram;
extern int16_t lds16espVelcram_old;
extern int16_t cram_msc_target_vol;
extern int16_t moment_pre_front_cram_preact;
extern int16_t lcs16espCramCount;

extern int16_t lcs16CramTargetG;
extern int16_t moment_pre_front_cram;
extern int16_t moment_cram_delv;
extern int16_t moment_pre_rear_cram;

#endif

  #if (__YAW_M_OVERSHOOT_DETECTION == 1)          

extern U8_BIT_STRUCT_t ESCSTATE_2;

#define ldespu1AlatSign    		            ESCSTATE_2.bit7
#define ldespu1YawMSign    	                ESCSTATE_2.bit6
#define ldespu1YawRateOverShoot             ESCSTATE_2.bit5
#define ldespu1YawRateOverShootSign         ESCSTATE_2.bit4
#define ldespu1DelYawStable                 ESCSTATE_2.bit3
#define ldespu1AlatYawMSamePhase            ESCSTATE_2.bit2
#define ESC_STATUS_not_using_2_1            ESCSTATE_2.bit1
#define ESC_STATUS_not_using_2_0            ESCSTATE_2.bit0

extern  int16_t    ldesps16MulevelOverVxThreshold;
  #endif

extern int16_t s16_yaw_accel_fb_gain ; 
extern int16_t ay_vx_dot_sig_old, ay_vx_dot_sig_minus, ay_vx_dot_sig_plus;
extern uint8_t esc_debugger;
  
  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
extern int16_t ay_vx_dot_sig_1st_th, ay_vx_dot_sig_2nd_th ;
extern int16_t ay_vx_dot_sig_1st_th_Mmu, ay_vx_dot_sig_2nd_th_Mmu ;
extern int16_t ab_rfm_gain;
  #endif
extern int16_t ldesps16YawMaxAlatm, ldesps16YawMaxAlatmSign ;
extern int16_t ldesps16YawOffset ;
extern int16_t abrfm_new_old , abrfm_new, rf2_new  ;
extern int16_t ldesps16AxEst, ldesps16AxEst_old, Stack_Mu_delyaw; 
extern int16_t det_banked_angle_G_old, det_banked_angle_G ; 
extern int16_t pre_filling_enter_cond;
  
#define S16_YAW_LIMIT_GAIN_H_SL    95    

/**************** Decel Brake Lamp (A430)**************/
#if (GMLAN_ENABLE==ENABLE)
extern int8_t	lsespu8DecelConfirmCounter;
#endif
/******************************************************/

#if __CAR==GM_S4500 || __CAR==GM_T300 || __CAR==GM_XTS
#define		U8_ESC_Active_Lamp_Enable		0
#define		S16_ESC_Lamp_Act_Decel_High		70
#define		S16_ESC_Lamp_Act_Decel_Low		30
#endif

#endif
