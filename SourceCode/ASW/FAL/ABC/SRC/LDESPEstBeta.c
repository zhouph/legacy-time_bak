/*******************************************************************/
/*  Program ID: MGH-40 Beta Estimation                             */
/*  Program description: Vehicle Sideslip Angle Estimation         */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes: Will be applicated to KMC HM(SOP.07.08)         */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author           Description                    */
/*  -------       ----------    -----------------------------      */
/*  04.11.30      JinKoo Lee    Initial Release                    */
/*  05.06.20      JinKoo Lee    Added Bank Angle Estimation        */
/*  05.08.15      JinKoo Lee    Added Sensor Reliability Check     */
/*  05.11.23      JinKoo Lee    Modified for MISRA rule            */
/*  06.07.04      JinKoo Lee    Added Application Vehicle Model    */
/*  07.07.23      JinKoo Lee    Modified for Application Parameter */
/*  08.03.20      JinKoo Lee    Added ESC Test Pulse Mu Detection  */
/*******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPEstBeta
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************/

#include "LDESPEstBeta.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCESPCallControl.h"
#include "LDESPCalVehicleModel.h"
#include "LDABSCallEstVehDecel.h"
#include "LSABSCall2ndOrderLpf.h"


#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	#include "LSESPCalSensorOffset.h"
	#include "LSESPFilterEspSensor.h"
#endif
/* Local Definition  ***********************************************/

#if __VDC && __MGH40_Beta_Estimation

  /*---------------------------------------------------------------*/
  /*       Vehicle Parameters                                      */
  /*---------------------------------------------------------------*/
  
#if __MODEL_CHANGE   
  
    #define Cf0        pEspModel->S16_CORNER_STIFF_FRONT       /* N/rad/100  */
    #define Cr0        pEspModel->S16_CORNER_STIFF_REAR        /* N/rad/100  */
    #define Lfront    ((pEspModel->S16_LENGTH_CG_TO_FRONT)/10)   /* m*100      optime: skeon 0305 (new) */
    #define Lrear     ((pEspModel->S16_LENGTH_CG_TO_REAR)/10)    /* m*100      optime: skeon 0305 (new) */
    #define Mass      (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */
    #define Iz         pEspModel->S16_MOMENT_OF_INERTIA        /* Kg*m */
    #define Ku         pEspModel->S16_UNDERSTEER_GRADIENT      /* rad/g*10000 */

#else  
  
  #if (__CAR==HMC_GK) || (__CAR==ASTRA_MECU)
  
    #define Cf0             -900      /* N/rad/100  : Cf_high = -115000(old) =>  -90000 (N/rad) */
    #define Cr0             -1000     /* N/rad/100  : Cr_high = -249000(old) => -100000 (N/rad) */
    #define Lfront          92        /* m*100 */
    #define Lrear           168       /* m*100 */
    #define Mass            1330      /* Kg */
    #define Iz              2400      /* Kg*m */
    #define Ku              477       /* rad/g*10000 */
    #define Str_ratio       160       /* ratio*10 */
  
  #elif (__CAR==SYC_RAXTON)||(__CAR==SYC_RAXTON_MECU)
    
    #define Cf0             -1450     /* N/rad/100  : Cf_high = -145000 (N/rad) */
    #define Cr0             -2866     /* N/rad/100  : Cr_high = -286566 (N/rad) */
    #define Lfront          125       /* m*100 */
    #define Lrear           157       /* m*100 */
    #define Mass            2096      /* Kg */
    #define Iz              4226      /* Kg*m */
    #define Ku              472       /* rad/g*10000 */
    #define Str_ratio       200       /* ratio*10 */
    
  #elif (__CAR==SYC_KYRON)||(__CAR==SYC_KYRON_MECU)
  
    #define Cf0             -1700     /* N/rad/100  : Cf_high = -170000 (N/rad) */
    #define Cr0             -3066     /* N/rad/100  : Cr_high = -306600 (N/rad) */
    #define Lfront          123       /* m*100 */
    #define Lrear           149       /* m*100 */
    #define Mass            2026      /* Kg */
    #define Iz              4933      /* Kg*m */
    #define Ku              350       /* rad/g*10000 */
    #define Str_ratio       200       /* ratio*10 */
  
  #elif (__CAR==FORD_C214)||(__CAR==FORD_C214_MECU)
  
    #define Cf0             -1050     /* N/rad/100  : Cf_high = -145000/2(old) => -105000/2 (N/rad) */
    #define Cr0             -1299     /* N/rad/100  : Cr_high = -585057/2(old) => -129910/2 (N/rad) */
    #define Lfront          109       /* m*100 */
    #define Lrear           154       /* m*100 */
    #define Mass            1508      /* Kg */
    #define Iz              3040      /* Kg*m */
    #define Ku              495       /* rad/g*10000 */
    #define Str_ratio       150
                         
  #else  /* NF */
  
    #define Cf0             -950      /* N/rad/100  : Cf_high = -145000(old) =>  -95000 (N/rad) */
    #define Cr0             -1200     /* N/rad/100  : Cr_high = -312870(old) => -120000 (N/rad) */
    #define Lfront          112       /* m*100 */
    #define Lrear           161       /* m*100 */
    #define Mass            1562      /* Kg */
    #define Iz              3149      /* Kg*m */
    #define Ku              434       /* rad/g*10000 */
    #define Str_ratio       180       /* ratio*10 */

  #endif

#endif         

#define S16_Bank_Low_Speed        VREF_60_KPH
#define S16_Bank_Creep_Speed      VREF_20_KPH
#define S16_Bank_Creep_Speed_Gain 100
#define S16_DFC_Decrease_Value    3

#define S16_BANK_LOWER_SUS_MODEL_DIFF BANK_ANGLE_5DEG

#if (__BANK_DFC_CHK == ENABLE)
	struct ACC_FILTER BANK_K_ACC;
	
   	#define s16BANKKFilterFreq  10   /* resol 10, 10--> 1Hz  */
   	#define s16BANKKFilterZeta  10   /* resol 10, 10--> 1   damping coeff  */	
   	#define s16BANKKAccMulti    1    /* Diff multiply        */
   	#define s16BANKKdiffcoefCov 10   /* 1scan 0.001 deg  =>  10 deg/s,  100 -> 1deg/s */	    
#endif

/* Variables Definition*********************************************/

/* Global For Other's Use */
  
BETA_ESTIMATION_FLAG ldu8BetaEstFlg0, ldu8BetaEstFlg1, ldu8BetaEstFlg2, ldu8BetaEstFlg3;
  
int16_t Beta_Dyn;
int16_t Beta_MD;
int16_t Beta_MD_Dot;
int16_t Beta_K_Dot;
int16_t Vy_MD;
int16_t Alpha_front;
int16_t Alpha_rear;
int16_t Bank_Angle_Final;
  
  /* Global For Logging */
  
int16_t Beta_MD_Front;
int16_t Linear_Gain_F;
int16_t Alpha_Gain;
int16_t Beta_Mu;
int16_t Beta_K_Dot2;
int16_t Bank_Angle_Yaw;
int16_t Bank_Angle_Ay;
int16_t Bank_Angle_K;
int16_t Bank_Angle_K_Dot;
int16_t Dynamic_Factor;
int16_t Dynamic_Factor_Dot;

#if (__BANK_DFC_CHK == ENABLE)
	int16_t lsesps16DynamicFactorTotalFilt;
	int16_t ldesps16ModelLatAccel;
	int16_t ldesps16ModelLatAccelFilt;
	int16_t ldesps16FiltedYawm;
	int16_t lsesps16DynamicFactorTotalFiltOld;
    int16_t ldesps16BankLatAccFiltCoeff;
    int16_t ldesps16VrefMulYawmBank;
    int16_t ldesps16alatLPF3Bank;
#endif

/* bank angle suspect for ESC 110812.HBJEON */
#if (__BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE)
	int16_t ldesps16BankLowerSuspectCounter; 
#endif
  
#if (__BANK_YCW == ENABLE)
	int16_t ldescs16BankDctValidCNT;
#endif

/* Bank Model Improve - Roll, YawFilter */
int16_t ldesps16BankAlatRollGain;
int16_t ldesps16BankYawFilter;

  /* Local */
  
int16_t Ts=10;
int16_t Fy_front,Fy_rear;           
int16_t Beta_Dyn_old;
int16_t YawE, YawE_old,Yaw_sensor;
int16_t Beta_MD_raw,Beta_MD_old;
int16_t Beta_MD_Dot_raw,Beta_MD_Dot_old;
int16_t Beta_K_Dot_raw,Beta_K_Dot_old;
int16_t Beta_K_Dot2_raw,Beta_K_Dot2_old;
int16_t Beta_MD_Front_raw,Beta_MD_Front_old;
int16_t Vy_MD_old,Vx_MD;
int16_t alat_LPF3,alat_LPF3_old,alat_corrected,alat_LPF;
int16_t Linear_Gain_F_old,Linear_Gain;
int16_t Alpha_front_Dyn,Alpha_rear_Dyn;
int16_t Beta_Mu_old,Beta_Mu_Level;
int16_t C_front,C_rear;
int16_t Beta_Nolinear_Region_Counter,Yaw_Stable_Counter;
int16_t Beta_Mu_Error,Beta_Mu_Reset_Counter;
int16_t Delta_wstr;                   
int16_t Integration_Gain;
int16_t Linear_limit_angle;           
int16_t Nonlinear_limit_angle; /* Always Nonlinear_limit_angle>Linear_limit_angle */
int16_t Del_Yaw_Linear_Upper_Limit,Del_Yaw_Nonlinear_Lower_Limit;
int16_t TF_Bank_Yaw,TF_Bank_Ay;
int16_t Bank_Angle_Yaw_raw,Bank_Angle_Yaw_old;
int16_t Bank_Angle_Ay_raw,Bank_Angle_Ay_old;
int16_t Bank_Angle_K_raw,Bank_Angle_K_old;
int16_t Bank_Angle_K_Dot_raw,Bank_Angle_K_Dot_old;
int16_t alat_LPF3_bank,alat_LPF3_bank_old;
int16_t Dynamic_Factor_raw,Dynamic_Factor_old,Dynamic_Factor_Total;
int16_t Dynamic_Factor_Dot_raw,Dynamic_Factor_Dot_old;
int16_t Bank_Angle_Final_raw,Bank_Angle_Final_old;
int16_t Beta_Rate_Offset,Beta_Rate_Offset_old,Beta_Sensor_Offset_counter,Beta_Small_Sensor_Offset_counter;
int16_t Bank_Detection_Counter;
int16_t Brake_Reset_Counter;
int16_t Bank_K_Detection_Counter;
int16_t Bank_K_Reset_Counter;
int16_t DF_Gain1,DF_Gain2,DF_Gain3;
int16_t Beta_tempW0,Beta_tempW1,Beta_tempW2,Beta_tempW3,Beta_tempW4;
int16_t Bank_Reset_Counter,Bank_Reset_Counter2;


/* CHAR ldescs8WheelSlipFL, ldescs8WheelSlipFR; */  /*11.10.07.hbjeon*/
/* UCHAR ldescu8SlipMuFlagResetCntFL,ldescu8SlipMuFlagResetCntFR; */    /*11.10.07.hbjeon*/
/* INT ldescs16RiseModeTimeFL,ldescs16RiseModeTimeFR; */    /*11.10.07.hbjeon*/
/* INT ldescs16SlipMuFL,ldescs16SlipMuFR; */    /*11.10.07.hbjeon*/
/* INT ldescs16SlipMuVehicle,ldescs16SlipMuVehicleOld; */   /* 11.10.07.hbjeon */
int16_t ldescs16DynBankCounter;
int16_t ldesps16BankModelDiff;

/* Local Function prototype ****************************************/

void LDESP_vEstRoadMu(void);
void LDESP_vEstTireModel(void);
void LDESP_vEstDynamicModel(void);
void LDESP_vEstBankAngle(void);
void LDESP_vDetSensorReliability(void);
void LDESP_vEstRoadMuByWheelSlip(void);
/* void LDESP_vEstRoadMuByESPSlip2(void); */    /* 11.10.07.hbjeon */

/* GlobalFunction prototype ****************************************/

/* Implementation **************************************************/

void LDESP_vEstBeta(void)
{

  /*---------------------------------------------------------------------------------------------*/
  /*       Tuning Parameters                                                                     */
  /*---------------------------------------------------------------------------------------------*/
  
    int16_t Alpha_Front_Ratio              = 6;   /* 10 is 100% */
    int16_t Alpha_Gain_Min                 = 300; /* degree*100 */ 
    int16_t Alpha_Gain_Max                 = 600; /* degree*100 */ 
    int16_t Alpha_Gain_Max_Brake           = 800; /* degree*100 */ 
    int16_t Alpha_Gain_Min_Low_Speed       = 300; /* degree*100 */ 
    int16_t Alpha_Gain_Max_Low_Speed       = 1000;/* degree*100 */ 
    int16_t Alpha_Gain_Max_Brake_Low_Speed = 1200;/* degree*100 */ 
    int16_t Alpha_Gain_Max_Small_Rough     = 800; /* degree*100 */
    int16_t Alpha_Gain_Max_Medium_Rough    = 1000;/* degree*100 */
    int16_t Alpha_Gain_Max_Large_Rough     = 1400;/* degree*100 */
    int16_t Alpha_Gain_Max_Sensor_Offset   = 1200;/* degree*100 */ 
    
  /*---------------------------------------------------------------------------------------------*/
    
  
  /*---------------------------------------------------------------------------------------------*/
  /*       Sensor Signal Processing : Filter, Limit                                              */
  /*---------------------------------------------------------------------------------------------*/
    
	/*BANK ROLL COMPENSATION 2012.8.31. in Alps Corvara*/
	#if (__BANK_ROLL_COMPENSATION == ENABLE)
   		ldesps16BankAlatRollGain = pEspModel->S16_BANK_ALAT_Roll_Gain;
	    alat_LPF   = LCESP_s16Lpf1Int( alat,alat_LPF,L_U8FILTER_GAIN_10MSLOOP_3HZ ); /* 3Hz LPF */ /* Lateral G sign! */
	    alat_LPF3  = (int16_t)((((int32_t)alat_LPF)*(pEspModel->S16_BANK_ALAT_Roll_Gain))/100) ; 
    #else
    	alat_LPF   = LCESP_s16Lpf1Int( alat,alat_LPF,L_U8FILTER_GAIN_10MSLOOP_3HZ ); /* 3Hz LPF */ /* Lateral G sign! */
	    alat_LPF3  = alat_LPF; 
	#endif
    
	#if (__BANK_DFC_CHK == ENABLE)
    	tempW0 = LCESP_s16IInter4Point(vref5, VREF_60_KPH, pEspModel->S16_Yawm_Filter_Gain_60kph,
											  VREF_90_KPH, pEspModel->S16_Yawm_Filter_Gain_90kph,
											  VREF_120_KPH, pEspModel->S16_Yawm_Filter_Gain_120kph,
											  VREF_150_KPH, pEspModel->S16_Yawm_Filter_Gain_150kph);
		ldesps16BankYawFilter = tempW0;
		ldesps16FiltedYawm = LCESP_s16Lpf1Int(yaw_out, ldesps16FiltedYawm, (uint8_t)tempW0);
    #endif

	#if ( (__YAW_FILTER_FOR_BETA == ENABLE) && (__BANK_DFC_CHK == ENABLE) )
        if(ldesps16FiltedYawm>=YAW_70DEG)
        {
            Yaw_sensor =  YAW_70DEG; /* Prevent overflow */
        }
        else if(ldesps16FiltedYawm<=(-YAW_70DEG))
        {
            Yaw_sensor = -YAW_70DEG; /* 70degree/s limit */
        }
        else
        {
           	Yaw_sensor = ldesps16FiltedYawm;
        }
    #else
    if(yaw_out>=YAW_70DEG)
    {
        Yaw_sensor =  YAW_70DEG; /* Prevent overflow */
    }
    else if(yaw_out<=(-YAW_70DEG))
    {
        Yaw_sensor = -YAW_70DEG; /* 70degree/s limit */
    }
    else
    {
        Yaw_sensor = yaw_out;
    }
	#endif
	    
    if(vref5>=VREF_200_KPH) 
    {
        Vx_MD = VREF_200_KPH;
    }
    else if(vref5<=VREF_10_KPH)
    {
        Vx_MD = VREF_10_KPH;
    }
    else
    {
        Vx_MD = vref5;
    }

/*        
    tempW2 = wstr;  tempW1 = 100;  tempW0 = Str_ratio;
    s16muls16divs16();
    Delta_wstr = tempW3;
*/
	Delta_wstr = ldesps16TireAngle; /* 2012SWD.hbjeon */
    
    if(
    	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		    ( (((fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0))&&(lsespu1PedalBrakeOn==0))||
		      ((lsespu1MpresByAHBgen3Invalid==0)&&(lsespu1MpresBrkOnByAHBgen3==0)) )
	    #else
    		(MPRESS_BRAKE_ON==1)
    	#endif
    )
    {
        Beta_Brake_Condition_Flag=1;
        Brake_Reset_Counter=0;
    }
    else
    {
    	if( Brake_Reset_Counter >= S16_Brake_Reset_Time_Ref )
    	{
    		Beta_Brake_Condition_Flag=0;
    	}
    	else
    	{
    		Beta_Brake_Condition_Flag=1;
    		Brake_Reset_Counter++;
    	}
    }
    
  /*---------------------------------------------------------------------------------------------*/
    
    LDESP_vEstRoadMuByWheelSlip();
   
/*    LDESP_vEstRoadMuByESPSlip2(); */ /* 11.10.07.hbjeon */
    
    LDESP_vEstBankAngle();
    
    LDESP_vEstRoadMu();
    
    LDESP_vEstTireModel();
    
    LDESP_vEstDynamicModel();
    
  /*--------------------------------------------------------------------------------------------------------*/
  /*  Reset Beta 
  
      1. Lower than 5kph
      2. Backward Driving
      3. Before Steer Angle Sensor offset compensation
      4. ESP System Error ( = ESC H/W Error || ESC Sensor Error || ESC Sensr Suspect || Wheel Speed Error )
  ----------------------------------------------------------------------------------------------------------*/
  
    if(vref5 <= VREF_5_KPH )
    {
        Beta_Reset_Condition=1;
    }
    else if(BACK_DIR==1)
    {
        Beta_Reset_Condition=1;
    }
    else if(SAS_CHECK_OK==0)
    {
        Beta_Reset_Condition=1;
    }
    #if (NEW_FM_ENABLE == ENABLE)
		else if((lcu1EscCtrlFail==1)
		||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1))
	#else
  #if __GM_FailM  
    else if((fu1ESCEcuHWErrDet==1)||(fu1ESCSensorErrDet==1)
          ||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1)
          ||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1))
  #else  /* HMC  */
    else if((vdc_error_flg==1)
          ||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1))
  #endif        
	#endif        
    {
        Beta_Reset_Condition=1;
    }
    else 
    {
        Beta_Reset_Condition=0;
    }
  
  /*---------------------------------------------------------------------------------------------*/
   
  if( Beta_Reset_Condition==1 )
  {
      Alpha_front_Dyn = 0;
      Alpha_rear_Dyn  = 0;
      Alpha_front = 0;
      Alpha_rear  = 0;
      Alpha_Gain  = 0;
      YawE =  0;
      Vy_MD  = 0;
      Beta_MD  = 0;
      Beta_Dyn  = 0;
      Beta_MD_Dot = 0;
      Beta_K_Dot  = 0;
      Beta_K_Dot2 = 0;
      Beta_MD_Front = 0;     
      C_front = Cf0;
      C_rear  = Cr0;
      Linear_Gain_F = 100;
      
      Beta_Mu = 800; /* High-Mu */
      Beta_Mu_Reset_Counter = 0;
      Beta_Mu_Saturation = 0;

      Bank_Angle_Yaw = 0;
      Bank_Angle_Ay = 0;
      Bank_Angle_K =0;
      Bank_Angle_K_Dot = 0;
      Bank_Angle_Final = 0;
      Beta_Bank_Comp_Flag = 0;
      Beta_Bank_Susp_Flag = 0;
      Bank_Comp_Flag = 0;
      Bank_Susp_Flag = 0;
      Bank_Detection_Counter =0;
  }
  else
  {
      
  /*---------------------------------------------------------------------------------------------*/
  /*       Beta Combination Model                                                                */
  /*---------------------------------------------------------------------------------------------*/
  /*
     Vy(n+1) = (1-a)*Vy(n) - Ts*Yawm(n)*Vx(n) + Ts*alat(n) + Ts*K*(Vym(n) - Vy(n))
  
          
     Vy(n+1) = (1-Pseudo_gain)*Vy(n) - Ts*vref5*yawm*314/(18000000*8) + Ts*latm_F3*981*36/10000000 
                                     + Ts*KK_F*(Beta_Dy*vref5*314/(18000*8) - Vy(n))/1000
  
  -----------------------------------------------------------------------------------------------*/
  
    tempW2 = Lfront;  tempW1 = 288;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW0 = tempW3;
    
    tempW2 = esp_tempW0;  tempW1 = Yaw_sensor;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    esp_tempW1 = tempW3;
    
    tempW2 = Lrear;  tempW1 = 288;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    
    tempW2 = esp_tempW2;  tempW1 = Yaw_sensor;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    esp_tempW3 = tempW3;
    
    Alpha_front = (Beta_MD_old + esp_tempW1) - Delta_wstr; /* degree*100 */
    Alpha_rear  = Beta_MD_old - esp_tempW3;              /* degree*100 */
    
    /* Alpha_front = Beta_MD_old + (int16_t)((int32_t)288*Lfront*Yaw_sensor/((int32_t)Vx_MD*1000)) - Delta_wstr; */ /* degree*100 */
    /* Alpha_rear  = Beta_MD_old - (int16_t)((int32_t)288*Lrear*Yaw_sensor/((int32_t)Vx_MD*1000)); */               /* degree*100 */

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{ */   
    tempW2 = Alpha_front;  tempW1 = Alpha_Front_Ratio;  tempW0 = 10;
    s16muls16divs16();
    esp_tempW4 = tempW3;
    
    tempW2 = Alpha_rear;  tempW1 = (10-Alpha_Front_Ratio);  tempW0 = 10;
    s16muls16divs16();
    esp_tempW5 = tempW3;
    
    Alpha_Gain = esp_tempW4 + esp_tempW5;
      
    /* Alpha_Gain = (int16_t)((int32_t)Alpha_Front_Ratio*Alpha_front/10 + (int32_t)(10-Alpha_Front_Ratio)*Alpha_rear/10); */
    
    Linear_limit_angle    = LCESP_s16IInter2Point(Vx_MD,VREF_20_KPH,Alpha_Gain_Min_Low_Speed,
                                                        VREF_40_KPH,Alpha_Gain_Min);
    
    Nonlinear_limit_angle = LCESP_s16IInter2Point(Vx_MD,VREF_20_KPH,Alpha_Gain_Max_Low_Speed,
                                                        VREF_40_KPH,Alpha_Gain_Max);
    
  /*--------- Braking Compensation --------------------------------------------------------------*/
    
    if(ABS_fz==1)
    {
        Nonlinear_limit_angle = LCESP_s16IInter2Point(Vx_MD,VREF_20_KPH,Alpha_Gain_Max_Brake_Low_Speed,
                                                            VREF_40_KPH,Alpha_Gain_Max_Brake);
    }
    else
    {
        ;
    }
/*}*/      
    
  /*--------- Rough Road Compensation -----------------------------------------------------------*/
    
    if((Rough_road_detect_vehicle==1)&&(Nonlinear_limit_angle<Alpha_Gain_Max_Large_Rough))
    {
        Nonlinear_limit_angle = Alpha_Gain_Max_Large_Rough;
    }
    else if((Rough_road_suspect_vehicle==1)&&(Nonlinear_limit_angle<Alpha_Gain_Max_Medium_Rough))
    {
        Nonlinear_limit_angle = Alpha_Gain_Max_Medium_Rough;
    }
    else if((ESP_ROUGH_ROAD==1)&&(det_alatm<LAT_0G7G)&&(Nonlinear_limit_angle<Alpha_Gain_Max_Small_Rough))
    {
        Nonlinear_limit_angle = Alpha_Gain_Max_Small_Rough;
    }
    else
    {
        ;
    }
    
  /*---------- Sensor Offset Compensation -------------------------------------------------------*/
    
    if((Beta_Sensor_Offset_Suspect_Flag==1)&&(Nonlinear_limit_angle<Alpha_Gain_Max_Sensor_Offset))
    {
        Nonlinear_limit_angle = Alpha_Gain_Max_Sensor_Offset;
    }
    else
    {
        ;
    }
    
  /*---------- U-Turn at Low Speed --------------------------------------------------------------*/ 
	esp_tempW0 = McrAbs(wstr);
	esp_tempW1 = McrAbs(Delta_wstr);
    if((esp_tempW0>=WSTR_180_DEG)&&(vref5<=VREF_40_KPH)&&(Nonlinear_limit_angle<esp_tempW1)&&(YAW_CDC_WORK==0))
    {
        Nonlinear_limit_angle = esp_tempW1;  
    } 
    else
    {
        ;
    } 
    
  /*------ Bank Road ----------------------------------------------------------------------------*/
    esp_tempW0 = McrAbs(Bank_Angle_Final)+300;
    if((Beta_Bank_Susp_Flag==1)&&(Nonlinear_limit_angle<esp_tempW0))
    {
        Nonlinear_limit_angle = esp_tempW0;
    }
    else
    {
        ;
    }
            
  /*---------------------------------------------------------------------------------------------*/
  /*        Using Dynamic Model 100% 
  
          1. abs(alpha_gain) < 3degree
          2. Straight Driving
          3. Sensor Signal Offset or a little lateral slope
          4. Lower than 10km/h
          5. U-turn at Low Speed
          6. Bank Road Detection ( slope > 10degree )                                            
  -----------------------------------------------------------------------------------------------*/
	esp_tempW0 = McrAbs(Alpha_Gain);
	esp_tempW1 = McrAbs(wstr);
	esp_tempW2 = McrAbs(yaw_out);
	esp_tempW3 = McrAbs(alat);
    
    if(esp_tempW0<Linear_limit_angle)
    {
        Linear_Gain = 100;
    }
    else if((esp_tempW1<WSTR_10_DEG)&&(esp_tempW2<YAW_1DEG)&&(esp_tempW3<LAT_0G05G))
    {
        Linear_Gain = 100;
    }
    else if((esp_tempW1<WSTR_15_DEG)&&(esp_tempW2<YAW_2DEG)&&(esp_tempW3<LAT_0G1G)&&(Beta_Mu>LAT_0G2G)&&(det_wstr_dot<WSTR_30_DEG))
    {
        Linear_Gain = 100;
    }
    else if(Beta_Sensor_Offset_Detect_Flag==1)
    {
        Linear_Gain = 100;
    }
    else if((vref5<=VREF_10_KPH)&&(YAW_CDC_WORK==0)&&(ESP_TCS_ON==0))
    {
        Linear_Gain = 100;
    }
    else if(Beta_Bank_Comp_Flag==1)
    {                                
        Linear_Gain = 100;
    }
    else if(esp_tempW0>Nonlinear_limit_angle)
    {
        Linear_Gain = 0;
    }
    else 
    {
        tempW2 = Nonlinear_limit_angle-esp_tempW0;     tempW1 = 100;
        tempW0 = Nonlinear_limit_angle-Linear_limit_angle;
        s16muls16divs16();
        Linear_Gain = tempW3; 
    }
    
    /* Linear_Gain = (int16_t)((int32_t)100*(Nonlinear_limit_angle-abs(Alpha_Gain))/(Nonlinear_limit_angle-Linear_limit_angle)); */
    
  /*---------------------------------------------------------------------------------------------*/
  /*   Prevent Beta from Diverging by Kinematics Model                                           */
  /*---------------------------------------------------------------------------------------------*/
    
    if(Linear_Gain == 0)
    {
        Beta_Nolinear_Region_Counter +=2;
    }
    else if(Linear_Gain <= 10)
    {
        Beta_Nolinear_Region_Counter +=1;
    }
    else if(Linear_Gain <= 90)
    {
        Beta_Nolinear_Region_Counter -=1;
    }
    else
    {
        Beta_Nolinear_Region_Counter = 0;
        Beta_Reset_Need_Flag = 0;
    }    
    
    if(Beta_Nolinear_Region_Counter<=0)
    {
        Beta_Nolinear_Region_Counter = 0;
    }
    else if(Beta_Nolinear_Region_Counter>=32000)
    {
        Beta_Nolinear_Region_Counter = 32000;
    }
    else
    {
        ;
    }
    
	esp_tempW0 = McrAbs(delta_yaw_first);
	esp_tempW1 = (tempW4+YAW_2DEG);
	esp_tempW2 = (tempW4-YAW_4DEG);
	esp_tempW3 = (tempW4-YAW_2DEG);
	esp_tempW4 = McrAbs(wstr);

    if(Beta_Reset_Need_Flag==1)
    {
        if(Beta_Nolinear_Region_Counter == 0)
        {
            Beta_Reset_Need_Flag=0;
        }
        else
        {
            ;
        }
        
        if( o_delta_yaw_thres < YAW_5DEG )
        { 
            tempW4 = YAW_5DEG;
        }
        else
        {
            tempW4 = o_delta_yaw_thres;
        }        
        
        if(esp_tempW0>esp_tempW1)
        {
            Yaw_Stable_Counter=0;
        }
        else if(esp_tempW0>tempW4)
        {
            Yaw_Stable_Counter-=1;
        }
        else if(YAW_CDC_WORK==1)
        {
            Yaw_Stable_Counter-=1;
        }
        else if(esp_tempW0<=esp_tempW2)
        {
            Yaw_Stable_Counter+=2;
        }
        else if(esp_tempW0<=esp_tempW3)
        {
            Yaw_Stable_Counter+=1;
        }
        else
        {
            ;
        }
                
        if(Yaw_Stable_Counter<=0)
        {
            Yaw_Stable_Counter = 0;
        }
        else if(Yaw_Stable_Counter>=32000)
        {
            Yaw_Stable_Counter = 32000;
        }
        else
        {
            ;
        }
        
        if(Yaw_Stable_Counter >= S16_Yaw_Stable_Time_Ref)
        {
            Linear_Gain = Linear_Gain_F_old + 5; /* 3sec */
        }
        else
        {
            ;
        }
    }
    else
    {
        if(Beta_Nolinear_Region_Counter >= S16_Beta_Nonlinear_Time_Ref)
        {
            Beta_Reset_Need_Flag=1; /* After 2sec */
        }
        else if((esp_tempW4>=WSTR_180_DEG)&&(vref5<=VREF_50_KPH)&&(Beta_Nolinear_Region_Counter>0))
        {
            Beta_Reset_Need_Flag=1; /* Severe Turn */
        }
        else
        {
            ;
        }
        Yaw_Stable_Counter=0;
    }
    
    if(Linear_Gain>=100)
    {
        Linear_Gain=100;
    }
    else if(Linear_Gain<=0)
    {
        Linear_Gain=0;
    }
    else
    {
        ;
    }
    
  /*---------------------------------------------------------------------------------------------*/
    
    Linear_Gain_F = LCESP_s16Lpf1Int(Linear_Gain,Linear_Gain_F_old,L_U8FILTER_GAIN_10MSLOOP_10HZ); /* 10Hz LPF */
    
    if(Beta_Sensor_Offset_Suspect_Flag==1)
    { 
        Integration_Gain = S16_Pseudo_Gain_Offset_Suspect;
    }
    else if((esp_tempW4>=WSTR_180_DEG)&&(vref5<=VREF_40_KPH))
    {
        if((YAW_CDC_WORK==0)&&(ESP_TCS_ON==0))
        {
        	Integration_Gain = 3;  
        }
        else
        {
        	Integration_Gain = 2; 
        }
    }
    
/*#if __4WD_VARIANT_CODE==ENABLE  
    else if(((lsu8DrvMode==DM_AUTO)||(lsu8DrvMode==DM_4H))
          &&(YAW_CDC_WORK==1)&&(det_alatm<LAT_0G2G)&&((ESP_TCS_ON==1)||(ETCS_ON==1)))
    {
        Integration_Gain = 2;  
    } 
  #else
   #if __4WD 
    else if((YAW_CDC_WORK==1)&&(det_alatm<LAT_0G2G)&&((ESP_TCS_ON==1)||(ETCS_ON==1)))
    {
        Integration_Gain = 2;  
    } 
   #endif
  #endif */ 
  
    else if((esp_tempW0<YAW_5DEG)&&(Rough_road_detect_vehicle==1))
    {
        Integration_Gain = S16_Pseudo_Gain_Large_Rough_Road;
    }
    else if((esp_tempW0<YAW_5DEG)&&(Rough_road_suspect_vehicle==1))
    {
        Integration_Gain = S16_Pseudo_Gain_Med_Rough_Road;
    }   
    else if(Beta_Bank_Susp_Flag==1)
    {
        Integration_Gain = S16_Pseudo_Gain_Bank_Suspect;
    }
    else if((YAW_CDC_WORK==0)&&(Beta_Brake_Condition_Flag==1)&&(ABS_fz==0))
    {
        Integration_Gain = S16_Pseudo_Gain_Brake; 
    }
    else if(Beta_Small_Sensor_Offset_Flag==1)
    {
        Integration_Gain = S16_Pseudo_Gain_Offset_Small;
    }
    else
    {
        Integration_Gain = S16_Pseudo_Gain_Default;
    }
    
    /*13 swd prevent Beta divergence by cycle time 7ms -> 10ms*/
    Integration_Gain = Integration_Gain + 1;
    /*13 swd prevent Beta divergence by cycle time 7ms -> 10ms*/
    
    tempW2 = Vx_MD*Ts;  tempW1 = Yaw_sensor;  tempW0 = 5730;/* 1rad = 57.296 degree */
    s16muls16divs16(); 
    esp_tempW6 = tempW3;
    
    tempW2 = esp_tempW6;  tempW1 = 1;  tempW0 = 80;
    s16muls16divs16(); 
    esp_tempW7 = tempW3;
    
    tempW2 = alat_LPF3;  tempW1 = Ts*100;  tempW0 = 28316;  /* 9.81*3.6/10000 */
    s16muls16divs16(); 
    esp_tempW8 = tempW3;
    
    tempW2 = Beta_Dyn;  tempW1 = Vx_MD*10;  tempW0 = 4584;  /* 573*8 */
    s16muls16divs16(); 
    esp_tempW9 = tempW3 - Vy_MD_old;
    
    tempW2 = esp_tempW9;  tempW1 = Ts*Linear_Gain_F;  tempW0 = 1000;  
    s16muls16divs16(); 
    esp_tempW10 = tempW3;
    
    tempW2 = 1000 - Integration_Gain;  tempW1 = Vy_MD_old;  tempW0 = 1000;
    s16muls16divs16();
    esp_tempW11 = tempW3;
    
    Vy_MD = ((esp_tempW11 - esp_tempW7) + esp_tempW8) + esp_tempW10;
    
    if( Vy_MD>=30000 )
    {
        Vy_MD= 30000;   /* Max. Vy = 300km/h */
    }
    else if( Vy_MD<=-30000 )
    {
        Vy_MD=-30000;
    }
    else
    {    
        ;
    }
       
    /* Vy_MD = (int16_t)((int32_t)(1000-Integration_Gain)*Vy_MD_old/1000 - (int32_t)Ts*Vx_MD*Yaw_sensor/(57296*8) + (int32_t)Ts*alat_LPF3*100/28316  */
    /*             + (int32_t)Ts*Linear_Gain_F*(Beta_Dyn*Vx_MD*10/(573*8)-Vy_MD_old)/1000); */  /* 1rad = 57.296degree */
    
    tempW2 = Vy_MD;  tempW1 = 4584;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    Beta_MD_raw = tempW3;
    
    /* Beta_MD_raw = (int16_t)((int32_t)18000*8*Vy_MD/((int32_t)Vx_MD*314)); */
    
    Beta_MD = LCESP_s16Lpf1Int(Beta_MD_raw,Beta_MD_old,L_U8FILTER_GAIN_10MSLOOP_2HZ); /* 2Hz LPF */
    
    if(Beta_MD>=5000)
    {
        Beta_MD =  5000; /* Prevent overflow */
    }
    else if(Beta_MD<=-5000)
    {
        Beta_MD = -5000; /* 50degree limit   */
    }
    else
    {
        ;
    }
    
  /*---------------------------------------------------------------------------------------------*/
  /*        Beta Dot                                                                             */
  /*---------------------------------------------------------------------------------------------*/
    
    tempW2 = Beta_MD - Beta_MD_old;  tempW1 = 1000;  tempW0 = Ts;
    s16muls16divs16();
    Beta_MD_Dot_raw = tempW3;
    
    /* Beta_MD_Dot_raw = (int16_t)((int32_t)(Beta_MD - Beta_MD_old)*1000/Ts); */   /* degree/s*100 */
    
    Beta_MD_Dot = LCESP_s16Lpf1Int(Beta_MD_Dot_raw,Beta_MD_Dot_old,L_U8FILTER_GAIN_10MSLOOP_3HZ); /* 3Hz LPF */
    
    if(Beta_MD_Dot>=5000)
    {
        Beta_MD_Dot =  5000; /* Prevent overflow */
    }
    else if(Beta_MD_Dot<=-5000)
    {
        Beta_MD_Dot = -5000; /* 50degree/s limit */
    }
    else
    {
        ;
    }
    
  /*---------------------------------------------------------------------------------------------*/
    
    tempW2 = alat;  tempW1 = 1620;  tempW0 = Vx_MD; /* 180*9.81*3.6*8/3.14=16196 */
    s16muls16divs16();
    Beta_K_Dot_raw = tempW3 - Yaw_sensor;
    
    /* Beta_K_dot_raw = (int16_t)((int32_t)alat*1618/Vx_MD)-Yaw_Sensor; */  /* degree/s*100 */
    
    Beta_K_Dot = LCESP_s16Lpf1Int(Beta_K_Dot_raw,Beta_K_Dot_old,L_U8FILTER_GAIN_10MSLOOP_3HZ); /* 3Hz LPF */
    
    if(Beta_K_Dot>=5000)
    {
        Beta_K_Dot =  5000; /* Prevent overflow */
    }
    else if(Beta_K_Dot<=-5000)
    {
        Beta_K_Dot = -5000; /* 50degree/s limit */
    }
    else
    {
        ;
    }
    
  /*---------------------------------------------------------------------------------------------*/
    
    tempW2 = alat_LPF3;  tempW1 = 1620;  tempW0 = Vx_MD; /* 180*9.81*3.6*8/3.14=16196 */
    s16muls16divs16();
    Beta_K_Dot2_raw = tempW3 - Yaw_sensor;
    
    /* Beta_K_dot2_raw = (int16_t)((int32_t)alat_LPF3*1618/Vx_MD)-Yaw_Sensor; */  /* degree/s*100 */
    /* Use for Sensor Reliability Check */
    
    Beta_K_Dot2 = LCESP_s16Lpf1Int(Beta_K_Dot2_raw,Beta_K_Dot2_old,L_U8FILTER_GAIN_10MSLOOP_3HZ); /* 3Hz LPF */
    
    if(Beta_K_Dot2>=5000)
    {
        Beta_K_Dot2 =  5000; /* Prevent overflow */
    }
    else if(Beta_K_Dot2<=-5000)
    {
        Beta_K_Dot2 = -5000; /* 50degree/s limit */
    }
    else
    {
        ;
    }  
    
  /*---------------------------------------------------------------------------------------------*/
  /*        Beta on Front of Vehicle                                                             */
  /*---------------------------------------------------------------------------------------------*/
    
    tempW2 = S16_Front_length;  tempW1 = 288;  tempW0 = 10;
    s16muls16divs16();
    esp_tempW12 = tempW3;
    
    tempW2 = Yaw_sensor;  tempW1 = esp_tempW12;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    Beta_MD_Front_raw = Beta_MD_raw + tempW3;
    
    /* Beta_MD_Front_raw = Beta_MD_raw + (int16_t)((int32_t)288*Front_length*Yaw_sensor/((int32_t)Vx_MD*100)); */
    
    Beta_MD_Front = LCESP_s16Lpf1Int(Beta_MD_Front_raw,Beta_MD_Front_old,L_U8FILTER_GAIN_10MSLOOP_1HZ); /* 1Hz LPF */  
    
    if(Beta_MD_Front>=5000)
    {
        Beta_MD_Front =  5000; /* Prevent overflow */
    }
    else if(Beta_MD_Front<=-5000)
    {
        Beta_MD_Front = -5000; /* 50degree limit */
    }
    else
    {
        ;
    }
    
  /*---------------------------------------------------------------------------------------------*/
    
  }
    LDESP_vDetSensorReliability();  
    
    YawE_old               = YawE;             /* degree/s*100 */
    Vy_MD_old              = Vy_MD;            /* km/h*800     */
    Beta_Dyn_old           = Beta_Dyn;         /* degree*100   */
    Beta_MD_old            = Beta_MD;          /* degree*100   */
    Beta_MD_Dot_old        = Beta_MD_Dot;      /* degree/s*100 */
    Beta_K_Dot_old         = Beta_K_Dot;       /* degree/s*100 */
    Beta_K_Dot2_old        = Beta_K_Dot2;      /* degree/s*100 */
    Beta_MD_Front_old      = Beta_MD_Front;    /* degree*100   */
    alat_LPF3_old          = alat_LPF3;        /* G*1000       */
    Linear_Gain_F_old      = Linear_Gain_F;
    Beta_Mu_old            = Beta_Mu;          /* G*1000       */
    Bank_Angle_Yaw_old     = Bank_Angle_Yaw;   /* degree*100   */
    Bank_Angle_Ay_old      = Bank_Angle_Ay;    /* degree*100   */
    Bank_Angle_K_old       = Bank_Angle_K;     /* degree*100   */
    Dynamic_Factor_old     = Dynamic_Factor; 
    Dynamic_Factor_Dot_old = Dynamic_Factor_Dot;
    Bank_Angle_Final_old   = Bank_Angle_Final; /* degree*100   */
    alat_LPF3_bank_old     = alat_LPF3_bank;   /* G*1000       */
    Beta_Rate_Offset_old   = Beta_Rate_Offset; /* degree/s*100 */
}



void LDESP_vEstRoadMu(void)
{
  /*---------------------------------------------------------------------------------------------*/
  /*       Tuning Parameter : Road Friction Coeffient Level for Beta Estimation                  */
  /*---------------------------------------------------------------------------------------------*/
    int16_t Linear_Time_Ref = L_U8_TIME_10MSLOOP_1000MS; /* 1sec */
    int16_t Beta_High_Mu = 800;    /* 0.8g */
    
    int16_t Del_Yaw_Stable_Min_Angle = 300;    /* 3.00 degree/s */
    int16_t Del_Yaw_Stable_Max_Angle = 400;    /* 4.00 degree/s */
    int16_t Del_Yaw_Stable_Min_Angle_Low_Speed = 400;    /* 4.00 degree/s */
    int16_t Del_Yaw_Stable_Max_Angle_Low_Speed = 500;    /* 5.00 degree/s */
    
  /*int16_t Del_Yaw_Wstr_dot_Comp_Angle = 500;*/
    int16_t Rough_Mu_comp;    
  /*---------------------------------------------------------------------------------------------*/
/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/
    Del_Yaw_Linear_Upper_Limit    = LCESP_s16IInter2Point(vref5,VREF_15_KPH,Del_Yaw_Stable_Min_Angle_Low_Speed,       
                                                                VREF_40_KPH,Del_Yaw_Stable_Min_Angle);
                                                           
    Del_Yaw_Nonlinear_Lower_Limit = LCESP_s16IInter2Point(vref5,VREF_15_KPH,Del_Yaw_Stable_Max_Angle_Low_Speed,               
                                                                VREF_40_KPH,Del_Yaw_Stable_Max_Angle);
/*}*/        
    if(o_delta_yaw_thres>500) /* Considering bank, Low Speed, Sensor Suspect etc. */
    { 
        tempW1 = o_delta_yaw_thres - ( 500 - Del_Yaw_Stable_Min_Angle );
        tempW2 = o_delta_yaw_thres - ( 500 - Del_Yaw_Stable_Max_Angle );
    }
    else
    {
        tempW1 = 0; 
        tempW2 = 0;
    }
                
    if( tempW1 > Del_Yaw_Linear_Upper_Limit )
    {
        Del_Yaw_Linear_Upper_Limit = tempW1;
    }
    else
    {
        ;
    }
    if( tempW2 > Del_Yaw_Nonlinear_Lower_Limit ) 
    {
        Del_Yaw_Nonlinear_Lower_Limit = tempW2;
    }
    else
    {
        ;
    }
    
/* On-Center Compensation */

/*  if(( det_wstr_dot >= WSTR_300_DEG )&&( det_abs_wstr2 < WSTR_70_DEG )) 
    {
    	tempW7 = Del_Yaw_Wstr_dot_Comp_Angle;
    }        
    else if(( det_wstr_dot >= WSTR_200_DEG )&&( det_abs_wstr2 < WSTR_70_DEG ))
    {
        tempW2 = det_wstr_dot - WSTR_200_DEG;
        tempW1 = Del_Yaw_Wstr_dot_Comp_Angle;
        tempW0 = WSTR_300_DEG - WSTR_200_DEG;
        s16muls16divs16();      
        tempW7 = tempW3;
    }    
    else
    {
    	tempW7 = 0;
    }   
    
    Del_Yaw_Linear_Upper_Limit    += tempW7;
    Del_Yaw_Nonlinear_Lower_Limit += tempW7; */   

    if( S16_Del_Ay_Sat_Below_Med_Mu < LAT_0G1G ) /* Tuning Parameter Limit */
    {
    	Beta_tempW1 = LAT_0G1G;
    }
    else
    {
    	Beta_tempW1 = S16_Del_Ay_Sat_Below_Med_Mu;
    }
    
    if( S16_Del_Ay_Sat_Above_Med_Mu < LAT_0G15G ) /* Tuning Parameter Limit */
    {
    	Beta_tempW2 = LAT_0G15G;
    }
    else
    {
    	Beta_tempW2 = S16_Del_Ay_Sat_Above_Med_Mu;
    }
        
    if((det_alatm<=LAT_0G5G)&&(det_wstr_dot<WSTR_200_DEG)&&(vref5 <= VREF_90_KPH))
    {
        tempW5 = Beta_tempW1;
    }
    else
    {
        tempW5 = Beta_tempW2;
    }
        
    if((Rough_road_detect_vehicle==1)||(Beta_Bank_Comp_Flag==1)||(Beta_Sensor_Offset_Suspect_Flag==1))
    {
        tempW3 = Del_Yaw_Stable_Min_Angle + YAW_4DEG;
        tempW4 = Del_Yaw_Stable_Max_Angle + YAW_4DEG;
        tempW5+= LAT_0G15G;
        Rough_Mu_comp = LAT_0G4G;
    }
    else if(Rough_road_suspect_vehicle==1)
    {
        tempW3 = Del_Yaw_Stable_Min_Angle + YAW_2DEG;
        tempW4 = Del_Yaw_Stable_Max_Angle + YAW_2DEG;
        tempW5+= LAT_0G1G;
        Rough_Mu_comp = LAT_0G2G;
    }
    else if((ESP_ROUGH_ROAD==1)||(Beta_Bank_Susp_Flag==1))
    {
        tempW3 = Del_Yaw_Stable_Min_Angle + YAW_1DEG;
        tempW4 = Del_Yaw_Stable_Max_Angle + YAW_1DEG;
        tempW5+= LAT_0G05G;
        Rough_Mu_comp = LAT_0G1G;
    }
    else
    {
        tempW3 = 0; 
        tempW4 = 0;
        Rough_Mu_comp = 0;
    }
        
    if( tempW3 > Del_Yaw_Linear_Upper_Limit )
    {
        Del_Yaw_Linear_Upper_Limit = tempW3;
    }
    else
    {
        ;
    }
    if( tempW4 > Del_Yaw_Nonlinear_Lower_Limit ) 
    {
        Del_Yaw_Nonlinear_Lower_Limit = tempW4;
    }
    else
    {
        ;
    }
          
    tempW6 = det_alatc_fltr - det_alatm_fltr;
    
    if( det_wstr_dot >= WSTR_100_DEG )
    {
    tempW7 = LCESP_s16IInter2Point( vref5, VREF_60_KPH,  YAW_3DEG,
                                           VREF_100_KPH, YAW_5DEG );
    }
    else if( det_wstr_dot >= WSTR_50_DEG )
    { 
        tempW7 = YAW_2DEG;
    }
    else
    {
    	tempW7 = YAW_1DEG;
    }
                                           
	esp_tempW0 = McrAbs(delta_yaw_first);
	esp_tempW1 = (nor_alat*alat);
	esp_tempW2 = (Del_Yaw_Linear_Upper_Limit-YAW_1DEG);
	esp_tempW3 = (tempW5+LAT_0G1G);
	esp_tempW4 = McrAbs(wstr);
	esp_tempW5 = McrAbs(yaw_out);
	esp_tempW6 = McrAbs(alat);
	
    if(Beta_Mu_Saturation==0) 
    {
        if((esp_tempW0>Del_Yaw_Nonlinear_Lower_Limit)
          ||((tempW6>tempW5)&&(esp_tempW1>0)&&(vref5 >= VREF_20_KPH)&&(vref5 <= VREF_120_KPH)
          &&(esp_tempW0>Del_Yaw_Linear_Upper_Limit))
          ||((tempW6>tempW5)&&(esp_tempW1>0)&&(vref5 >= VREF_20_KPH)&&(vref5 <= VREF_100_KPH)
          &&(esp_tempW0>esp_tempW2)&&(ETCS_ON==1))
          ||((tempW6>esp_tempW3)&&(esp_tempW1>0)&&(vref5 >= VREF_20_KPH)&&(vref5 <= VREF_100_KPH)
          &&(esp_tempW0>esp_tempW2))
          ||((YAW_CDC_WORK==1)&&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0)
          &&(esp_tempW0>tempW7)))
        {
            if((det_alatm<LAT_0G7G)&&(Beta_Bank_Comp_Flag==0)&&(Beta_Sensor_Offset_Detect_Flag==0))
            {
            	Beta_Mu_Saturation=1;
            }
            else
            {
            	;
            }
        }
        else
        {
            ;
        }

        tempW0 = LCESP_s16IInter2Point( esp_tempW0, Del_Yaw_Linear_Upper_Limit,0,
                                                              Del_Yaw_Nonlinear_Lower_Limit,Beta_High_Mu);
        Beta_Mu_Error = Beta_High_Mu - tempW0;
        Beta_Mu_Level = det_alatm + Beta_Mu_Error;
    }
    else
    {
        if(((esp_tempW4<WSTR_10_DEG)&&(esp_tempW5<YAW_1DEG)&&(esp_tempW6<LAT_0G05G))||
           (esp_tempW0<=Del_Yaw_Linear_Upper_Limit))
        {
            if(Beta_Mu_Reset_Counter>=Linear_Time_Ref) /* 1sec */
            { 
                Beta_Mu_Saturation=0;
                Beta_Mu_Reset_Counter=0;
            }
            else if(YAW_CDC_WORK==0)
            {
                if(esp_tempW0<=YAW_1DEG)
                {
                    Beta_Mu_Reset_Counter+=2;
                }
                else
                {
                	Beta_Mu_Reset_Counter+=1;
                }
            }
            else
            {
            	;
            }
        }
        else if((esp_tempW0>Del_Yaw_Nonlinear_Lower_Limit)||(YAW_CDC_WORK==1))
        {
            Beta_Mu_Reset_Counter=0;
        }
        else
        {
            ;
        }
        
        if(ABS_fz==1)
        {
          #if __4WD_VARIANT_CODE==ENABLE	
            if((lsu8DrvMode !=DM_2WD)&&(fu1AxErrorDet==0)&&(fu1AxSusDet==0))
            {
            	Beta_Mu_Level = (int16_t)(LCESP_u16CompSqrt((uint16_t)det_alatm,(uint16_t)(McrAbs(along)))) + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;
            }
            else
            {
            	Beta_Mu_Level = (int16_t)(LCESP_u16CompSqrt((uint16_t)det_alatm,(uint16_t)(McrAbs(afz))))   + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;
            }
          #else   
            #if __4WD
           	if((fu1AxErrorDet==0)&&(fu1AxSusDet==0))
            {
                Beta_Mu_Level = (int16_t)(LCESP_u16CompSqrt((uint16_t)det_alatm,(uint16_t)(McrAbs(along)))) + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;
            }
            else
            {    
            	Beta_Mu_Level = (int16_t)(LCESP_u16CompSqrt((uint16_t)det_alatm,(uint16_t)(McrAbs(afz))))   + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;	
            }
          #else   
                Beta_Mu_Level = (int16_t)(LCESP_u16CompSqrt((uint16_t)det_alatm,(uint16_t)(McrAbs(afz))))   + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;
            #endif  
          #endif
        } 
        else
        {
            Beta_Mu_Level = det_alatm + (2*Beta_Mu_Reset_Counter) + Rough_Mu_comp;
        }
    }
        
    if( Beta_Mu_Level<det_alatm )
    {
        Beta_Mu_Level = det_alatm;
    }
    else
    {
        ;
    }
    if((Beta_Mu_Level>Beta_High_Mu)||(Beta_Bank_Comp_Flag==1)||(Beta_Sensor_Offset_Detect_Flag==1))
    {
        Beta_Mu_Level = Beta_High_Mu;
    }
    else
    {
        ;
    }

    Beta_Mu = LCESP_s16Lpf1Int( Beta_Mu_Level, Beta_Mu_old, L_U8FILTER_GAIN_10MSLOOP_1HZ ); /* 1Hz LPF */
}  
  


void LDESP_vEstTireModel(void) /* Linear Tire Model Considering Road-mu Level & Brake Slip Ratio */
{
  /*---------------------------------------------------------------------------------------------*/
  /*       Tuning Parameter : Road Friction Coeffient Level for Beta Estimation                  */
  /*---------------------------------------------------------------------------------------------*/
    int16_t High_Mu_Lower_Limit_Level   = 700; /* 0.7g */
    int16_t Medium_Mu_Upper_Limit_Level = 500; /* 0.5g */
    int16_t Medium_Mu_Lower_Limit_Level = 300; /* 0.3g */
    int16_t Low_Mu_Upper_Limit_Level    = 100; /* 0.1g */
  /*---------------------------------------------------------------------------------------------*/
    
    int16_t Cf1,       Cr1;
    int16_t Cf_High,   Cr_High;
    int16_t Cf_Medium, Cr_Medium;
    int16_t Cf_Low,    Cr_Low;
    
    int16_t Cf_ABS_Large_Slip,  Cr_ABS_Large_Slip;
    int16_t Cf_ABS_Small_Slip,  Cr_ABS_Small_Slip;
    
/*  int16_t Cf_ESP_Large_Slip,  Cr_ESP_Large_Slip;
    int16_t Cf_ESP_Small_Slip,  Cr_ESP_Small_Slip; */
    
    int16_t Front_Slip_Ratio;
    int16_t Rear_Slip_Ratio;
  
  /*---------------------------------------------------------------------------------------------*/
      
    if(S16_Beta_Cf_Gain < 80)       /* Tuning Parameter Limit */
    {
    	Beta_tempW3 = 80;
    }
    else if(S16_Beta_Cf_Gain > 200) /* Tuning Parameter Limit */
    {
    	Beta_tempW3 = 200;
    }
    else
    {
    	Beta_tempW3 = S16_Beta_Cf_Gain;
    }
    
    if(S16_Beta_Cr_Gain < 80)       /* Tuning Parameter Limit */
    {
    	Beta_tempW4 = 80;
    }
    else if(S16_Beta_Cr_Gain > 200) /* Tuning Parameter Limit */
    {
    	Beta_tempW4 = 200;
    }
    else
    {
    	Beta_tempW4 = S16_Beta_Cr_Gain;
    }    
    
    tempW2 = Cf0;    tempW1 = Beta_tempW3;    tempW0 = 100;  /* Cf model tuning */
    s16muls16divs16();
    Cf1 = tempW3;             
    tempW2 = Cr0;    tempW1 = Beta_tempW4;    tempW0 = 100;  /* Cr model tuning */
    s16muls16divs16();
    Cr1 = tempW3;             
    
    tempW2 = Cf1;    tempW1 = 2;    tempW0 = 3;  /* Cf_High = Cf0/1.5; */
    s16muls16divs16();
    Cf_High = tempW3;
    tempW2 = Cr1;    tempW1 = 2;    tempW0 = 3;  /* Cr_High = Cr0/1.5; */
    s16muls16divs16();
    Cr_High = tempW3;
    
    tempW2 = Cf1;    tempW1 = 1;    tempW0 = 6;  /* Cf_Medium = Cf0/6; */
    s16muls16divs16();
    Cf_Medium = tempW3;
    tempW2 = Cr1;    tempW1 = 1;    tempW0 = 6;  /* Cr_Medium = Cr0/6; */
    s16muls16divs16();
    Cr_Medium = tempW3;
    
    tempW2 = Cf1;    tempW1 = 1;    tempW0 = 15;  /* Cf_Low  = Cf0/15; */
    s16muls16divs16();
    Cf_Low = tempW3;
    tempW2 = Cr1;    tempW1 = 1;    tempW0 = 15;  /* Cr_Low  = Cr0/15; */
    s16muls16divs16();
    Cr_Low = tempW3;

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/   
    C_front = LCESP_s16IInter4Point(Beta_Mu,Low_Mu_Upper_Limit_Level,   Cf_Low,
                                            Medium_Mu_Lower_Limit_Level,Cf_Medium,
                                            Medium_Mu_Upper_Limit_Level,Cf_Medium,
                                            High_Mu_Lower_Limit_Level,  Cf_High);
    
    C_rear  = LCESP_s16IInter4Point(Beta_Mu,Low_Mu_Upper_Limit_Level,   Cr_Low,
                                            Medium_Mu_Lower_Limit_Level,Cr_Medium,
                                            Medium_Mu_Upper_Limit_Level,Cr_Medium,
                                            High_Mu_Lower_Limit_Level,  Cr_High);
    
  /*---------------------------------------------------------------------------------------------*/
  /*        Braking Slip & Driving Slip                                                                    */
  /*---------------------------------------------------------------------------------------------*/
    
    if(ABS_fz==1)
    {
        Front_Slip_Ratio = rel_lam_fl + rel_lam_fr;
        Rear_Slip_Ratio  = rel_lam_rl + rel_lam_rr;
    
        tempW2 = C_front;   tempW1 = 1;    tempW0 = 2;  /* Cf_ABS_Large_Slip = C_front/4; */
        s16muls16divs16();
        Cf_ABS_Large_Slip = tempW3;
        tempW2 = C_rear;    tempW1 = 1;    tempW0 = 2;  /* Cr_ABS_Large_Slip = C_rear/4;  */
        s16muls16divs16();
        Cr_ABS_Large_Slip = tempW3;
    
        tempW2 = C_front;   tempW1 = 3;    tempW0 = 4;  /* Cf_ABS_Small_Slip = C_front/2; */
        s16muls16divs16();
        Cf_ABS_Small_Slip = tempW3;
        tempW2 = C_rear;    tempW1 = 3;    tempW0 = 4;  /* Cr_ABS_Small_Slip = C_rear/2;  */
        s16muls16divs16();
        Cr_ABS_Small_Slip = tempW3;
        
        C_front = LCESP_s16IInter2Point(Front_Slip_Ratio,-LAM_50P,Cf_ABS_Large_Slip,
                                                         -LAM_20P,Cf_ABS_Small_Slip); 
        
        C_rear  = LCESP_s16IInter2Point(Rear_Slip_Ratio ,-LAM_50P,Cr_ABS_Large_Slip,
                                                         -LAM_20P,Cr_ABS_Small_Slip);
    }
/*  else if((YAW_CDC_WORK==1)&&(vref5 >= VREF_20_KPH))
    {       
        tempW2 = C_front;   tempW1 = 1;    tempW0 = 4;  
        s16muls16divs16();
        Cf_ESP_Large_Slip = tempW3;
        tempW2 = C_rear;    tempW1 = 1;    tempW0 = 4;  
        s16muls16divs16();
        Cr_ESP_Large_Slip = tempW3;
    
        tempW2 = C_front;   tempW1 = 2;    tempW0 = 3;  
        s16muls16divs16();
        Cf_ESP_Small_Slip = tempW3;
        tempW2 = C_rear;    tempW1 = 2;    tempW0 = 3;  
        s16muls16divs16();
        Cr_ESP_Small_Slip = tempW3;
        
        if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FL_old==1)
         ||(SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FR_old==1))
        {
            Front_Slip_Ratio = abs(rel_lam_fl) + abs(rel_lam_fr);
            
            C_front = LCESP_s16IInter2Point(Front_Slip_Ratio,LAM_10P,Cf_ESP_Small_Slip,
                                                             LAM_50P,Cf_ESP_Large_Slip); 
        }
        else
        {
            ;
        }
        
        if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RL_old==1)
         ||(SLIP_CONTROL_NEED_RR==1)||(SLIP_CONTROL_NEED_RR_old==1))
        {
            Rear_Slip_Ratio  = abs(rel_lam_rl) + abs(rel_lam_rr);
            
            C_rear  = LCESP_s16IInter2Point(Rear_Slip_Ratio ,LAM_10P,Cr_ESP_Small_Slip,
                                                             LAM_50P,Cr_ESP_Large_Slip);
        }
        else
        {
            ;
        }       
    }
    else if((ETCS_ON==1)||(ESP_TCS_ON==1))
    {
        ;
    }*/           
    else
    {
        ;
    }
    
/*}*/
    
}


void LDESP_vEstDynamicModel(void)
{
  /*---------------------------------------------------------------------------------------------*/
  /*                    Linear Observer Gain                                                     */
  /*---------------------------------------------------------------------------------------------*/
    int16_t Dyn_Gain11 = 1;    
    int16_t Dyn_Gain21 = 100; /* old value */
    int16_t Dyn_Gain21_Low_Speed = 10;
    int16_t Dyn_Gain21_MedHigh_Speed = 100; 
  
  /*---------------------------------------------------------------------------------------------*/
  /*       Beta Dynamic Model                                                                    */
  /*---------------------------------------------------------------------------------------------*/
  /*
       a_f = Beta + Lf*yawE/Vx - Wstr
       a_r = Beta - Lr*yawE/Vx
    
       Fyf = Cf*a_f
       Fyr = Cr*a_r
  
       Beta(n+1) = Beta(n) + Ts*((Fyf+Fyr)/(Mass*Vx) + K1*(yawm-yawE) - yawE)  : degree*100
       yawE(n+1) = yawE(n) + Ts*((Lf*Fyf-Lr*Fyr)/Iz  + K2*(yawm-yawE))         : degree/s*100 
  
  -----------------------------------------------------------------------------------------------*/
    
    tempW2 = Lfront;  tempW1 = 288;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW0 = tempW3;
    
    tempW2 = esp_tempW0;  tempW1 = YawE_old;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    esp_tempW1 = tempW3;
    
    tempW2 = Lrear;  tempW1 = 288;  tempW0 = 100;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    
    tempW2 = esp_tempW2;  tempW1 = YawE_old;  tempW0 = Vx_MD*10;
    s16muls16divs16();
    esp_tempW3 = tempW3;
    
    Alpha_front_Dyn = (Beta_Dyn_old + esp_tempW1) - Delta_wstr; /* degree*100 */
    Alpha_rear_Dyn  = Beta_Dyn_old - esp_tempW3;              /* degree*100 */
    
   /* Alpha_front_Dyn = Beta_Dyn_old + (int16_t)((int32_t)288*Lfront*YawE_old/((int32_t)Vx_MD*1000)) - Delta_wstr; */   /* degree*100 */
   /* Alpha_rear_Dyn  = Beta_Dyn_old - (int16_t)((int32_t)288*Lrear*YawE_old/((int32_t)Vx_MD*1000)); */                 /* degree*100 */
   
    if(Alpha_front_Dyn>=5000)
    {
        Alpha_front_Dyn =  5000; /* Prevent overflow  */
    }
    else if(Alpha_front_Dyn<=(-5000))
    {
        Alpha_front_Dyn = -5000; /* 50degree limit    */
    }
    else
    {
        ;
    }
    if(Alpha_rear_Dyn >=5000) 
    {
        Alpha_rear_Dyn  =  5000; /* Prevent overflow  */
    }
    else if(Alpha_rear_Dyn<=(-5000))
    {
        Alpha_rear_Dyn  = -5000; /* 50degree limit    */
    }
    else
    {
        ;
    }   

    Dyn_Gain21 = LCESP_s16IInter2Point(Vx_MD,VREF_5_KPH, Dyn_Gain21_Low_Speed,
                                             VREF_10_KPH,Dyn_Gain21_MedHigh_Speed); 
    
/*  Fy_front = (int32_t)C_front*Alpha_front_Dyn; */ /* N/rad*degree */
/*  Fy_rear  = (int32_t)C_rear*Alpha_rear_Dyn;   */ /* N/rad*degree */

/*  tempL0 = (int32_t)(Fy_front+Fy_rear)*36;
    tempL1 = (int32_t)(tempL0/((int32_t)Mass*Vx_MD/80)); 
    tempL2 = (int32_t)Dyn_Gain11*(Yaw_sensor-YawE_old);
    tempL3 = (int32_t)(Fy_front*Lfront-Fy_rear*Lrear)/Iz;
    tempL4 = (int32_t)Dyn_Gain21*(Yaw_sensor-YawE_old);   
    
    tempL5 = (int32_t)(tempL1 - YawE_old + tempL2)*Ts/1000;
    tempL6 = (int32_t)(tempL3 + tempL4)*Ts/1000;

    Beta_Dyn = Beta_Dyn_old + (int16_t)tempL5;
    YawE     = YawE_old     + (int16_t)tempL6; */
    
    if(Beta_Bank_Comp_Flag==1)
    {
    	tempW2 = Bank_Angle_Final;  tempW1 = 2825;  tempW0 = Vx_MD*10;
        s16muls16divs16();
    	esp_tempW10 = tempW3;
    }
    else
    {
    	esp_tempW10 = 0;
    } 
    /* Force Effect by Bank Angle : Bank_Angle_Final*9.81*3.6*8*Ts/(vref5*1000) */
    
    tempW2 = C_front;  tempW1 = Alpha_front_Dyn;  tempW0 = 1000;
    s16muls16divs16();
    Fy_front = tempW3; /* N/rad*degree/1000 */
    
    tempW2 = C_rear;  tempW1 = Alpha_rear_Dyn;  tempW0 = 1000;
    s16muls16divs16();
    Fy_rear = tempW3; /* N/rad*degree/1000 */
    
    tempW2 = Fy_front+Fy_rear;  tempW1 = 36*Ts;  tempW0 = Mass;
    s16muls16divs16();
    esp_tempW4 = tempW3; 
    
    tempW2 = esp_tempW4;  tempW1 = 80;  tempW0 = Vx_MD;
    s16muls16divs16();
    esp_tempW5 = tempW3; 
    
    tempW2 = (Dyn_Gain11*(Yaw_sensor-YawE_old))-YawE_old-esp_tempW10;  tempW1 = Ts;  tempW0 = 1000;
    s16muls16divs16();    
    esp_tempW6 = tempW3;
    
    tempW2 = Fy_front;  tempW1 = Lfront*Ts;  tempW0 = Iz;
    s16muls16divs16();
    esp_tempW7 = tempW3;
    
    tempW2 = Fy_rear;  tempW1 = Lrear*Ts;  tempW0 = Iz;
    s16muls16divs16();
    esp_tempW8 = tempW3;
    
    tempW2 = Dyn_Gain21*Ts;  tempW1 = Yaw_sensor-YawE_old;  tempW0 = 1000;
    s16muls16divs16();
    esp_tempW9 = tempW3;    
    
    Beta_Dyn = Beta_Dyn_old + esp_tempW5 + esp_tempW6;
    YawE     = ((YawE_old + esp_tempW7) - esp_tempW8) + esp_tempW9;
  
   /* Beta_Dyn = Beta_Dyn_old + (int16_t)(((int32_t)36*(Fy_front + Fy_rear)/((int32_t)Mass*Vx_MD/80) + (int32_t)Dyn_Gain11*(Yaw_sensor-YawE_old) - YawE_old)*Ts/1000); */
   /* YawE     = YawE_old     + (int16_t)(((int32_t)(Fy_front*Lfront-Fy_rear*Lrear)/Iz + (int32_t)Dyn_Gain21*(Yaw_sensor-YawE_old))*Ts/1000);  */
    
    if(YawE>=7000)
    {
        YawE =  7000;  /*  Prevent overflow  */
    }
    else if(YawE<=(-7000))
    {
        YawE = -7000;  /*  70degree/s limit  */
    }
    else
    {
        ;
    }
    
    if(Beta_Dyn>=5000)
    {
        Beta_Dyn =  5000; /*  Prevent overflow */
    }
    else if(Beta_Dyn<=(-5000))
    {
        Beta_Dyn = -5000; /*  50degree limit   */
    }
    else
    {
        ;
    }
}


void LDESP_vEstBankAngle(void)
{
  /*---------------------------------------------------------------------------------------------*/
  /*       Bank Detection Model                                                                  */
  /*---------------------------------------------------------------------------------------------*/
  /*
                                             Ku/10000 * vrefm/10
         TF_Bank_Yaw    = 1000 * ---------------------------------------------
                                  (Lf+Lr)/100 + Ku/10000 * vrefm^2/(100*9.81)
  
                                             Ku/10000 * vrefm^2/100
         TF_Bank_Ay     =  100 * --------------------------------------------- - 100*9.81
                                  (Lf+Lr)/100 + Ku/10000 * vrefm^2/(100*9.81)
  
         sin(BankA_K)   = - ( vref * yawm - latm )
  
         sin(BankA_yaw) = - 1000*( yawm - yawc )/TF_Bank_Yaw
  
         sin(BankA_Ay)  = - 100*9.81*57.3/10*( latm - latc )/TF_Bank_Ay
  
         Dynamic Factor = vrefm/10 * TF_Bank_Yaw/1000 * (sin(BankA_yaw) - sin(BankA_K))*10/(57.3*9.81)
                        + TF_Bank_Ay/100 * (sin(BankA_Ay) - sin(BankA_K))*10/(57.3*9.81)  
  
         sin(BankA_final) = sin(BankA_K) * max[ 0,1 - K1*abs(DFC) - K2*abs(DFC_dot) - K3*abs(BankA_K_Dot) ]
  
  -----------------------------------------------------------------------------------------------*/

   	alat_LPF3_bank  = LCESP_s16Lpf1Int( alat,alat_LPF3_bank_old,L_U8FILTER_GAIN_10MSLOOP_3HZ ); /* 3Hz LPF */
    
    if( vrefm<=555 )
    { /* 200km/h  */ 
        tempW2 = vrefm;       tempW1 = vrefm;        tempW0 = 10;
        s16muls16divs16();
        tempW2 = Ku;          tempW1 = tempW3;       tempW0 = 981; /* vrefm : m/s*10 */
        s16muls16divs16();
        tempW2 = Ku*10;       tempW1 = vrefm;        tempW0 = ((Lfront+Lrear)*10) + tempW3; 
    }
    else
    { /* Prevent overflow */
        tempW2 = vrefm;       tempW1 = vrefm;        tempW0 = 100;
        s16muls16divs16();
        tempW2 = Ku;          tempW1 = tempW3;       tempW0 = 981; /* vrefm : m/s*10 */
        s16muls16divs16();
        tempW2 = Ku;          tempW1 = vrefm;        tempW0 = (Lfront+Lrear) + tempW3; 
    }
    s16muls16divs16();
    TF_Bank_Yaw = tempW3;  /* Trasfer Function of Bank_Angle -> Yaw */
    
    if(TF_Bank_Yaw==0)
    {
        TF_Bank_Yaw=1;
    }
    else
    {
        ;
    }
    
    /*Prevent Overflow 2012.3.5. hbjeon*/
    tempW0 = (int16_t)(delta_yaw_first2 / TF_Bank_Yaw);
    if(tempW0 >= 32)
    {
    	Bank_Angle_Yaw_raw = -32000;
    }
    else if(tempW0 <= -32)
    {
    	Bank_Angle_Yaw_raw = 32000;
    }
    else
    {
        tempW2 = 1000;    tempW1 = delta_yaw_first2;    tempW0 = TF_Bank_Yaw; 
    	s16muls16divs16();    
    	Bank_Angle_Yaw_raw = -tempW3;
	}
    
    Bank_Angle_Yaw = LCESP_s16Lpf1Int(Bank_Angle_Yaw_raw,Bank_Angle_Yaw_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ); /* 0.5Hz LPF */
 
  /*---------------------------------------------------------------------------------------------*/
    
    if( vrefm<=555 )  /* 200km/h  */ 
    { 
        tempW2 = vrefm;       tempW1 = vrefm;        tempW0 = 10;
        s16muls16divs16();
        tempW2 = Ku;          tempW1 = tempW3;       tempW0 = 981; /* vrefm : m/s*10  */
        s16muls16divs16();
        tempW2 = tempW3;      tempW1 = 981;          tempW0 = ((Lfront+Lrear)*10) + tempW3; 
    }
    else  /* Prevent overflow */
    { 
        tempW2 = vrefm;       tempW1 = vrefm;        tempW0 = 100;
        s16muls16divs16();
        tempW2 = Ku;          tempW1 = tempW3;       tempW0 = 981; /* vrefm : m/s*10  */
        s16muls16divs16();
        tempW2 = tempW3;      tempW1 = 981;           tempW0 = (Lfront+Lrear) + tempW3; 
    }
    s16muls16divs16();
    TF_Bank_Ay = tempW3 - 981;  /* Trasfer Function of Bank_Angle -> Ay  */
    
    if(TF_Bank_Ay==0)
    {
        TF_Bank_Ay=1;
    }
    else
    {
        ;
    }
    
    tempW2 = 5621;    tempW1 = alat_LPF3_bank - nor_alat_unlimit;    tempW0 = TF_Bank_Ay; 
    s16muls16divs16();    
    Bank_Angle_Ay_raw = -tempW3;

    Bank_Angle_Ay = LCESP_s16Lpf1Int(Bank_Angle_Ay_raw,Bank_Angle_Ay_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ); /* 0.5Hz LPF */
    
 /*---------------------------------------------------------------------------------------------*/
    #if (__BANK_DFC_CHK ==ENABLE)
    	esp_tempW1 = (int16_t)((((int32_t)vrefm)*ldesps16FiltedYawm)/196);
    	esp_tempW2 = (int16_t)((((int32_t)alat_LPF3)*573)/100);

    	ldesps16VrefMulYawmBank = esp_tempW1*2;
    	ldesps16alatLPF3Bank = esp_tempW2;

	        Bank_Angle_K_raw = - (esp_tempW1*2) + esp_tempW2;
        
        /*---- Beta Dot Filter Coefficient Consideration ----*/
    	tempW3 = (int16_t)((((int32_t)ldesps16BetarateDynModel)*ldesps16VehicleVelocity)/1617);
    	ldesps16ModelLatAccel = McrAbs(tempW3);
        ldesps16ModelLatAccelFilt = LCESP_s16Lpf1Int((ldesps16ModelLatAccel*10),ldesps16ModelLatAccelFilt,8); 
    	ldesps16BankLatAccFiltCoeff = LCESP_s16IInter3Point(ldesps16ModelLatAccelFilt,100,39,200,30,400,16);
        
        Bank_Angle_K = LCESP_s16Lpf1Int1024(Bank_Angle_K_raw,Bank_Angle_K, (uint8_t)ldesps16BankLatAccFiltCoeff);
        
        esp_tempW1 = Bank_Angle_K - Bank_Angle_K_old;
        
        if (esp_tempW1 >= 10)
        {
        	esp_tempW1 = 10;
        }
        else if(esp_tempW1 <= -10)
        {
        	esp_tempW1 = -10;
        }
        else
        {
        	;
        }
        
        Bank_Angle_K = Bank_Angle_K_old + esp_tempW1;
        
    #else
	    tempW2 = vrefm;    tempW1 = yaw_out;    tempW0 = 196; /* 9.81*10*2 = 196.2 */
	    s16muls16divs16();    
	    esp_tempW1 = tempW3;
	      
	    tempW2 = alat_LPF3_bank;    tempW1 = 573;    tempW0 = 100; 
	    s16muls16divs16();    
	    esp_tempW2 = tempW3;
	    
	    Bank_Angle_K_raw = - (esp_tempW1*2) + esp_tempW2;
	    Bank_Angle_K = LCESP_s16Lpf1Int(Bank_Angle_K_raw,Bank_Angle_K_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ); /* 0.5Hz LPF */
	#endif
    
    if((Bank_Angle_K - Bank_Angle_K_old)>210)
    {
        tempW2 = 210;
    }
    else if((Bank_Angle_K - Bank_Angle_K_old)<-210)
    {
        tempW2 = -210;
    }
    else
    {
        tempW2 = Bank_Angle_K - Bank_Angle_K_old;
    }
               
    tempW1 = 1000;    tempW0 = Ts; /* Sampling Time = 0.01ms */
    s16muls16divs16();    
    Bank_Angle_K_Dot_raw = tempW3; /* degree/s*100 */
    
    #if (__BANK_DFC_CHK ==ENABLE)
		if(speed_calc_timer	< (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
		{
			;
		}
		else
		{
			ACC_CALC = 	(struct ACC_FILTER *)&BANK_K_ACC;
			LDABS_vCalcEachFilterSignalAccel( (Bank_Angle_K*10) , s16BANKKFilterFreq, s16BANKKFilterZeta , s16BANKKAccMulti , s16BANKKdiffcoefCov );
		}
    #endif   
    
    #if (__BANK_DFC_CHK == ENABLE)
        Bank_Angle_K_Dot = (int16_t)(((int32_t)BANK_K_ACC.lsesps16SignalAccel*56)/128) ; 
    #else
    	Bank_Angle_K_Dot = LCESP_s16Lpf1Int(Bank_Angle_K_Dot_raw,Bank_Angle_K_Dot_old,L_U8FILTER_GAIN_10MSLOOP_10HZ); /* 10Hz LPF */
    #endif
  
  /*---------------------------------------------------------------------------------------------*/
  
    tempW2 = vrefm;    tempW1 = TF_Bank_Yaw;    tempW0 = 50;  
    s16muls16divs16();    
        
    tempW2 = tempW3;   tempW1 = Bank_Angle_Yaw - Bank_Angle_K;    tempW0 = 11242;/* 57.3*9.81*20 */
    s16muls16divs16();
    esp_tempW6 = tempW3;
    
    tempW2 = TF_Bank_Ay;   tempW1 = Bank_Angle_Ay - Bank_Angle_K; tempW0 = 5621; /* 57.3*9.81*10 */
    s16muls16divs16();
    esp_tempW7 = tempW3;
    
    tempW2 = esp_tempW6;   tempW1 = S16_DFC_Yaw_Model_Gain;    tempW0 = 100;
    s16muls16divs16();
    esp_tempW8 = tempW3;
    
    tempW2 = esp_tempW7;   tempW1 = S16_DFC_Ay_Model_Gain;    tempW0 = 100;
    s16muls16divs16();
    esp_tempW9 = tempW3;    
        
    Dynamic_Factor_raw = esp_tempW8 + esp_tempW9; /* DFC of Tseng Paper */
    
    Dynamic_Factor = LCESP_s16Lpf1Int(Dynamic_Factor_raw,Dynamic_Factor_old,L_U8FILTER_GAIN_10MSLOOP_1HZ); /* 1Hz LPF */
    
    tempW2 = Dynamic_Factor - Dynamic_Factor_old;    tempW1 = 1000;    tempW0 = Ts; /* Sampling Time = 0.01ms */
    s16muls16divs16();    
    Dynamic_Factor_Dot_raw = tempW3;
    
    Dynamic_Factor_Dot = LCESP_s16Lpf1Int(Dynamic_Factor_Dot_raw,Dynamic_Factor_Dot_old,L_U8FILTER_GAIN_10MSLOOP_1HZ); /* 1Hz LPF */
    
  /*---------------------------------------------------------------------------------------------*/
    
    if((Bank_Comp_Flag==1)||((Bank_Susp_Flag==1)&&(det_wstr_dot>=S16_Bank_Hold_Fast_Str_Thres)))
    {
    	if(det_wstr_dot<WSTR_20_DEG) /* Steady-State */
        {
            DF_Gain1 = S16_Bank_SlowSteer_DFC_Gain;
            DF_Gain2 = S16_Bank_SlowSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_Bank_SlowSteer_Bank_Dot_Gain;
        }
        else if(det_wstr_dot<WSTR_50_DEG)
        {
        	DF_Gain1 = S16_Bank_MedSteer_DFC_Gain;
            DF_Gain2 = S16_Bank_MedSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_Bank_MedSteer_Bank_Dot_Gain;
        }
        else
        {
            DF_Gain1 = S16_Bank_FastSteer_DFC_Gain;
            DF_Gain2 = S16_Bank_FastSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_Bank_FastSteer_Bank_Dot_Gain;
        }
    }
    else
    {
    	/*if((ldescu1SlipMuDetectionVehicle==1)&&(ldescs16SlipMuVehicle>=LAT_0G7G)&&(det_alatm<=LAT_0G6G))
    	{
    		DF_Gain1 = 30;
            DF_Gain2 = 0;
            DF_Gain3 = 0;
    	}*/
    	if(det_wstr_dot<WSTR_20_DEG) /* Steady-State */
        { 
            DF_Gain1 = S16_SlowSteer_DFC_Gain;
            DF_Gain2 = S16_SlowSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_SlowSteer_Bank_Dot_Gain;
        }
        else if(det_wstr_dot<WSTR_50_DEG)
        {
            DF_Gain1 = S16_MedSteer_DFC_Gain;
            DF_Gain2 = S16_MedSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_MedSteer_Bank_Dot_Gain;
        }
        else 
        {
            DF_Gain1 = S16_FastSteer_DFC_Gain;
            DF_Gain2 = S16_FastSteer_DFC_Dot_Gain;
            DF_Gain3 = S16_FastSteer_Bank_Dot_Gain;
        }
    }                                                 
    
    if( vref5>=S16_Bank_MedLow_Speed )
    {
	    esp_tempW10 = LCESP_s16IInter2Point(vref5,S16_Bank_MedLow_Speed, 100,
	                                              S16_Bank_High_Speed,   S16_Bank_High_Speed_Gain);
    }
    else if( vref5<=S16_Bank_Low_Speed )
   	{
	   	esp_tempW10 = LCESP_s16IInter2Point(vref5,S16_Bank_Creep_Speed, S16_Bank_Creep_Speed_Gain,
												  S16_Bank_Low_Speed,   100);
	}
	else
	{
		esp_tempW10 = 100;
	}

	tempW2 = DF_Gain1*esp_tempW10;   tempW1 = Dynamic_Factor;        tempW0 = 1000;
    s16muls16divs16();    
    esp_tempW3 = tempW3; 
    
    tempW2 = DF_Gain2*esp_tempW10;   tempW1 = Dynamic_Factor_Dot;    tempW0 = 1000;
    s16muls16divs16();    
    esp_tempW4 = tempW3; 
    
    tempW2 = DF_Gain3*esp_tempW10;   tempW1 = Bank_Angle_K_Dot;      tempW0 = 10000;
    s16muls16divs16();    
    esp_tempW5 = tempW3; 
	
	Dynamic_Factor_Total = McrAbs(esp_tempW3) + McrAbs(esp_tempW4) + McrAbs(esp_tempW5);
	
	esp_tempW4 = McrAbs(Bank_Angle_K - Bank_Angle_Ay);
   	esp_tempW5 = McrAbs(Bank_Angle_Yaw - Bank_Angle_K);
   	ldesps16BankModelDiff = esp_tempW4+esp_tempW5;
        
    #if (__BANK_DFC_CHK ==ENABLE)
    	lsesps16DynamicFactorTotalFiltOld = lsesps16DynamicFactorTotalFilt;
		lsesps16DynamicFactorTotalFilt = LCESP_s16Lpf1Int( Dynamic_Factor_Total,lsesps16DynamicFactorTotalFilt,L_U8FILTER_GAIN_10MSLOOP_5HZ);  /* 5 Hz LPF */  
        tempW3 = lsesps16DynamicFactorTotalFiltOld - S16_DFC_Decrease_Value; 

	    if(lsesps16DynamicFactorTotalFilt <= 1000)
	    { 
	    	if( lsesps16DynamicFactorTotalFilt >= tempW3)
	    	{
	    		;
	    	}
	    	else
	    	{
    			lsesps16DynamicFactorTotalFilt = lsesps16DynamicFactorTotalFiltOld - S16_DFC_Decrease_Value;
	    	}
	    }
	    else
	    {
	    	;
	    }
	    
        if(lsesps16DynamicFactorTotalFilt > 1000)
        {
        	Bank_Angle_Final = 0;
        }
        else
        {
			tempW2 = Bank_Angle_K;   tempW1 = 1000-lsesps16DynamicFactorTotalFilt;    tempW0 = 1000;
        	s16muls16divs16();    
        	Bank_Angle_Final = tempW3;
        }
	#else
    	if(Dynamic_Factor_Total > 1000)
    	{
	        Bank_Angle_Final_raw = 0;
	    }
	    else
	    {
        	tempW2 = Bank_Angle_K;   tempW1 = 1000-Dynamic_Factor_Total;    tempW0 = 1000;
        	s16muls16divs16();    
        	Bank_Angle_Final_raw = tempW3; 
    	}
    	Bank_Angle_Final = LCESP_s16Lpf1Int(Bank_Angle_Final_raw,Bank_Angle_Final_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ); /* 0.5Hz LPF */
    #endif
    
    if(Bank_Angle_Final>=5000)
    {
        Bank_Angle_Final = 5000; /*  Prevent overflow */
    }
    else if(Bank_Angle_Final<=(-5000))
    {
        Bank_Angle_Final = -5000; /*  50degree limit   */
    }
    else
    {
        ;
    }
    
    tempW4 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Detect_Thres_LowSpeed,
                                         VREF_80_KPH,S16_Bank_Detect_Thres);
    tempW5 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Suspect_Thres_LowSpeed,
                                         VREF_80_KPH,S16_Bank_Suspect_Thres);
    tempW6 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Exit_Thres_LowSpeed,
                                         VREF_80_KPH,S16_Bank_Exit_Thres);
    
    if(MedLowMu_Detect_BySlip_Flag==1)
    {
        Beta_Bank_Comp_Flag = 0;
        Beta_Bank_Susp_Flag = 0;
        Bank_Reset_Counter2 = 0;
    }
    else if(( McrAbs(Bank_Angle_Final) >= tempW4 )&&(MedLowMu_Suspect_Flag==0)&&( delta_yaw_first >= 0 )&&(YAW_CDC_WORK==0))
    {
        Beta_Bank_Comp_Flag = 1;
        Beta_Bank_Susp_Flag = 1;
        Bank_Reset_Counter2 = 0;
    }
    else if( McrAbs(Bank_Angle_Final) >= tempW5 )
    {
        Beta_Bank_Susp_Flag = 1;
        Bank_Reset_Counter2 = 0;
    }
    else
    {
        if((Beta_Bank_Comp_Flag==1)||(Beta_Bank_Susp_Flag==1))
        {
            if(det_wstr_dot>=S16_Bank_Hold_Fast_Str_Thres)
            {
            	Bank_Reset_Counter2 = 0;
            }
            else if(( McrAbs(Bank_Angle_Final)<tempW6 )&&(Bank_Reset_Counter2<S16_Bank_Reset_Time))
            {
                Bank_Reset_Counter2++;
            }            
            else if( McrAbs(Bank_Angle_Final)<tempW6 )
            {
                Beta_Bank_Comp_Flag = 0;
                Beta_Bank_Susp_Flag = 0;
                Bank_Reset_Counter2 = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            Beta_Bank_Comp_Flag = 0;
            Beta_Bank_Susp_Flag = 0;
            Bank_Reset_Counter2 = 0;
        }
    }
  
  /*---------------------------------------------------------------------------------------------*/
  /*       Bank Detection for ESP Prohibition                                                    */
  /*---------------------------------------------------------------------------------------------*/
    
    tempW4 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Detect_Thres_LowSpeed2,
                                         VREF_80_KPH,S16_Bank_Detect_Thres2);
    tempW5 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Suspect_Thres_LowSpeed2,
                                         VREF_80_KPH,S16_Bank_Suspect_Thres2);
    tempW6 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Exit_Thres_LowSpeed2,
                                         VREF_80_KPH,S16_Bank_Exit_Thres2);
	#if (__BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE)
		tempW7 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,S16_Bank_Lower_Suspect_Thres_LowSpeed2,
                                         	 VREF_80_KPH,S16_Bank_Lower_Suspect_Thres2);    
	#endif

    #if (__BANK_YCW == ENABLE)
	    if(ldescu1BankYCWFlag == 1)
    	{
	    	if(YAW_CDC_WORK ==0)
    		{
	    		ldescu1BankYCWFlag = 0;
    		}
    		else
    		{
	    		;
    		}
    	}
    	else
    	{
	    	if(YAW_CDC_WORK==1)
    		{
	    		ldescs16BankDctValidCNT = ldescs16BankDctValidCNT + 1;
    
    			if(ldescs16BankDctValidCNT > S16_BANK_CHK_VALID_TIME )
    			{
	    			ldescu1BankYCWFlag          = 1;
    				ldescs16BankDctValidCNT     = 0;
    			}
    			else if( (FL.s16_Estimated_Active_Press > S16_BANK_VALID_PRESSURE) ||
    			     	 (FR.s16_Estimated_Active_Press > S16_BANK_VALID_PRESSURE) ||
    			     	 (RL.s16_Estimated_Active_Press > S16_BANK_VALID_PRESSURE) || 
    			     	 (RR.s16_Estimated_Active_Press > S16_BANK_VALID_PRESSURE)   )
    			{
	    			ldescu1BankYCWFlag          = 1;
    				ldescs16BankDctValidCNT     = 0;
    			}
	    		else
    			{
	    			;
    			}
    		}
    		else
    		{
	    		ldescs16BankDctValidCNT = 0 ;
    		}
    	}

    	if(MedLowMu_Detect_BySlip_Flag==1)
    	{
    	    Bank_Comp_Flag = 0;
	        Bank_Susp_Flag = 0;
	        Bank_Detection_Counter =0;
        	Bank_Reset_Counter = 0;
    	}    
    	else if(( McrAbs(Bank_Angle_Final) >= tempW4 )&&(MedLowMu_Suspect_Flag==0)&&( delta_yaw_first >= 0 )&&(ldescu1BankYCWFlag==0))
    	{
	        if(Bank_Detection_Counter<S16_Bank_Detection_Time_Ref)
        	{
	            Bank_Detection_Counter++;
        	}
        	else
        	{
	            Bank_Comp_Flag = 1;
        	}
        	Bank_Susp_Flag = 1;
        	Bank_Reset_Counter = 0;
    	}
    	else if(( McrAbs(Bank_Angle_Final) >= tempW5 )&&(ldescu1BankYCWFlag==0))
    	{
	        Bank_Susp_Flag = 1;
        	Bank_Reset_Counter = 0;
    	}
    	else
    	{        
	        if((Bank_Comp_Flag==1)||(Bank_Susp_Flag==1))
        	{
	            if(det_wstr_dot>=S16_Bank_Hold_Fast_Str_Thres)
            	{
	            	Bank_Reset_Counter = 0;
            	}
            	else if(( McrAbs(Bank_Angle_Final)<tempW6 )&&(Bank_Reset_Counter<S16_Bank_Reset_Time))
            	{
	                Bank_Reset_Counter++;
            	}
            	else if( McrAbs(Bank_Angle_Final)<tempW6 )
            	{
	                Bank_Comp_Flag = 0;
                	Bank_Susp_Flag = 0;
                	Bank_Reset_Counter = 0;
            	}
            	else
            	{
	                ;
            	}
        	}
        	else
        	{
	            Bank_Comp_Flag = 0;
            	Bank_Susp_Flag = 0;
            	Bank_Reset_Counter = 0;
        	}
        	Bank_Detection_Counter =0;
    	}
	#else  /*------------------ No Bank Detect/Suspect on ESC Ctrl -------------*/
	    if(MedLowMu_Detect_BySlip_Flag==1)
	    {
        	Bank_Comp_Flag = 0;
        	Bank_Susp_Flag = 0;
        	Bank_Detection_Counter =0;
        	Bank_Reset_Counter = 0;
    	}    
    	else if(( McrAbs(Bank_Angle_Final) >= tempW4 )&&(MedLowMu_Suspect_Flag==0)&&( delta_yaw_first >= 0 )&&(YAW_CDC_WORK==0))
    	{
	        if(Bank_Detection_Counter<S16_Bank_Detection_Time_Ref)
        	{
	            Bank_Detection_Counter++;
        	}
        	else
        	{
	            Bank_Comp_Flag = 1;
        	}
        	Bank_Susp_Flag = 1;
        	Bank_Reset_Counter = 0;
    	}
    	else if(( McrAbs(Bank_Angle_Final) >= tempW5 )&&(YAW_CDC_WORK==0))
    	{
	        Bank_Susp_Flag = 1;
        	Bank_Reset_Counter = 0;
    	}
    	else
    	{        
	        if((Bank_Comp_Flag==1)||(Bank_Susp_Flag==1))
        	{
	            if(det_wstr_dot>=S16_Bank_Hold_Fast_Str_Thres)
            	{
	            	Bank_Reset_Counter = 0;
            	}
            	else if(( McrAbs(Bank_Angle_Final)<tempW6 )&&(Bank_Reset_Counter<S16_Bank_Reset_Time))
            	{
	                Bank_Reset_Counter++;
            	}
            	else if( McrAbs(Bank_Angle_Final)<tempW6 )
            	{
	                Bank_Comp_Flag = 0;
                	Bank_Susp_Flag = 0;
                	Bank_Reset_Counter = 0;
            	}
            	else
            	{
	                ;
            	}
        	}
        	else
        	{
	            Bank_Comp_Flag = 0;
            	Bank_Susp_Flag = 0;
            	Bank_Reset_Counter = 0;
        	}
        	Bank_Detection_Counter =0;
    	}
	#endif
    
        #if (T_300_CPG_BANK_SUSPECT_SET == ENABLE)
    
    esp_tempW0 = McrAbs(alat);
    esp_tempW1 = McrAbs(Bank_Angle_K_Dot);
    esp_tempW2 = McrAbs(Bank_Angle_K);
    esp_tempW3 = McrAbs(Dynamic_Factor);
    esp_tempW4 = McrAbs(det_wstr_dot2);
    if (Bank_K_alatm_ok == 0)
    {
        if ((det_alatm >= S16_BANK_K_DETECT_ALAT)&&(esp_tempW0 >= S16_BANK_K_DETECT_ALAT))
        {
            Bank_K_alatm_ok = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        if ((det_alatm <= S16_BANK_K_EXIT_ALAT)||(esp_tempW0 <= S16_BANK_K_EXIT_ALAT))
        {
            Bank_K_alatm_ok = 0;
        }
        else
        {
            ;
        }
    }
  
    if(MedLowMu_Detect_BySlip_Flag==1)
    {       
        Bank_K_Comp_Flag = 0;
        Bank_K_Detection_Counter =0;
        Bank_K_Reset_Counter = 0;
    }    
    else if(((esp_tempW1 <= S16_BANK_K_DETECT_BANK_K_DOT)&&(det_wstr_dot <= S16_BANK_K_DETECT_WSTR_DOT)&&(Bank_K_alatm_ok == 1)&&(vref5 <= S16_BANK_K_DETECT_VREF))
            &&((esp_tempW2 >= S16_BANK_K_DETECT_BANK_K)&&(MedLowMu_Suspect_Flag==0))
            #if (__BANK_YCW == ENABLE)
            	&&(ldescu1BankYCWFlag == 0)
		    #else
            	&&(ESP_BRAKE_CONTROL == 0)
            #endif
            &&(esp_tempW3 <= S16_BANK_K_DETECT_DYN_F)
            &&(esp_tempW4 <= S16_BANK_K_DETECT_WSTR_DOT)
            )
    {
        if(Bank_K_Detection_Counter<S16_Bank_Detection_Time_Ref)
        {
            Bank_K_Detection_Counter++;
        }
        else
        {
            Bank_K_Comp_Flag = 1;
        }
        Bank_K_Reset_Counter = 0;
    }
    else
    {        
        if(Bank_K_Comp_Flag==1)
        {
            if(det_wstr_dot>=S16_Bank_Hold_Fast_Str_Thres)
            {
            	Bank_K_Reset_Counter = 0;
            }
            else if((((esp_tempW1 > S16_BANK_K_EXIT_BANK_K_DOT)||(det_wstr_dot > S16_BANK_K_EXIT_WSTR_DOT)||(Bank_K_alatm_ok == 0)||(vref5 > S16_BANK_K_EXIT_VREF))
                    ||(esp_tempW2 < S16_BANK_K_EXIT_BANK_K )
                    ||(esp_tempW3 > S16_BANK_K_EXIT_DYN_F)
                    ||(esp_tempW4 > S16_BANK_K_EXIT_WSTR_DOT)) 
                    &&(Bank_Reset_Counter<S16_Bank_Reset_Time))
            {
                if (Bank_K_Reset_Counter<S16_Bank_Reset_Time)
                {
                    Bank_K_Reset_Counter++;    
                }
                else
                {
                    Bank_K_Comp_Flag = 0;
                    Bank_K_Reset_Counter = 0;
                    Bank_K_Detection_Counter = 0;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            Bank_K_Comp_Flag = 0;
            Bank_K_Reset_Counter = 0;
            if (Bank_K_Detection_Counter >= 1)
            {
                Bank_K_Detection_Counter = Bank_K_Detection_Counter-1;
            }
            else
            {
                Bank_K_Detection_Counter = 0;
            }
        }
    }
    if (Bank_K_Comp_Flag == 1)
    {
        Bank_Angle_Final = ((int16_t)((int32_t)Bank_Angle_K*7/10));
        Bank_Susp_Flag = 1;
    }
    else
    {
        ;
    } 
        #endif /*(T_300_CPG_BANK_SUSPECT_SET == ENABLE)*/
    
	
    
  /* -------------- GM CPG Dynamic Bank Detect ------------------------- */
  
  #if  __GM_CPG_BANK
    if(((Bank_Angle_K>=700)||(Bank_Angle_K<=-700))&&(det_alatm>=LAT_0G5G))
    {
    	ldescs16DynBankCounter+=1;
    }
    else if((Bank_Angle_K>=-100)&&(Bank_Angle_K<=100))
    {
    	ldescs16DynBankCounter-=2;
    }
    else if((Bank_Angle_K>=-500)&&(Bank_Angle_K<=500))
    {
    	ldescs16DynBankCounter-=1;
    }
    else{}
    
    if(ldescs16DynBankCounter > L_U8_TIME_10MSLOOP_1000MS)
    {
    	ldescs16DynBankCounter = L_U8_TIME_10MSLOOP_1000MS;
    }
    else if(ldescs16DynBankCounter<0)
    {
    	ldescs16DynBankCounter = 0;
    }
    else{}
        
    if((vref5<VREF_50_KPH)||(det_alatm<LAT_0G3G))
    {
    	ldescs16DynBankCounter=0;
    	ldescu1DetDynBank=0;
    } 
    else if((ldescu1DetDynBank==0)&&(ldescs16DynBankCounter >= L_U8_TIME_10MSLOOP_1000MS))
    {
    	ldescu1DetDynBank = 1;
    }
    else if((ldescu1DetDynBank==1)&&(ldescs16DynBankCounter<1))
    {   
    	ldescu1DetDynBank = 0;
    }
    else{}
  #endif  
    
  /*-------------------------------------------------------------------- */  

	/* START
	bank angle suspect for ESC 110812.HBJEON */
	#if ( __BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE )
	    /* gathering & setting condition */
	    esp_tempW0 = tempW7 + BANK_ANGLE_3DEG;
	    esp_tempW1 = tempW7 + BANK_ANGLE_2DEG;
	    esp_tempW2 = tempW7 + BANK_ANGLE_1DEG;
	    esp_tempW3 = tempW7 - BANK_ANGLE_1DEG;
    	esp_tempW4 = McrAbs(alat);
		
	    if(ldespu1BankLowerSuspectFlag == 0) 
	    {
	        if( (McrAbs(Bank_Angle_Final) >= esp_tempW0) && ( (ldesps16BankModelDiff <= S16_BANK_LOWER_SUS_MODEL_DIFF) || (esp_tempW4 > LAT_0G6G) ) )
    		{
				ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter + S8_BANK_LOWER_SUSPECT_TIME_4TH_ADD;
    		}
    		else if( (McrAbs(Bank_Angle_Final) >= esp_tempW1) && ( (ldesps16BankModelDiff <= S16_BANK_LOWER_SUS_MODEL_DIFF) || (esp_tempW4 > LAT_0G6G) )  )
    		{
				ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter + S8_BANK_LOWER_SUSPECT_TIME_3RD_ADD;
    		}
    		else if( (McrAbs(Bank_Angle_Final) >= esp_tempW2) && ( (ldesps16BankModelDiff <= S16_BANK_LOWER_SUS_MODEL_DIFF) || (esp_tempW4 > LAT_0G6G) )  )
    		{
				ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter + S8_BANK_LOWER_SUSPECT_TIME_2ND_ADD;
    		}
    		else if( (McrAbs(Bank_Angle_Final) >= tempW7) && ( (ldesps16BankModelDiff <= S16_BANK_LOWER_SUS_MODEL_DIFF) || (esp_tempW4 > LAT_0G6G) )  )
    		{
				ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter + S8_BANK_LOWER_SUSPECT_TIME_1ST_ADD;
    		}
    		else if( (McrAbs(Bank_Angle_Final) < tempW7) ) 
    		{
    		    ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter - S8_BANK_LOWER_SUSPECT_TIME_SUB;
    		}
    		else
    		{
    		    ;
    		}
    		
    		/* Prevent Overflow & Flag set */
    		if(ldesps16BankLowerSuspectCounter >= S16_BANK_LOWER_SUSPECT_TIME)
    		{
    		    ldesps16BankLowerSuspectCounter = S16_BANK_LOWER_SUSPECT_TIME;
    		    ldespu1BankLowerSuspectFlag     = 1;
    		}
    		else if( ldesps16BankLowerSuspectCounter <= 0 )
    		{
    		    ldesps16BankLowerSuspectCounter = 0;
    		}
    		else
    		{
    		    ;
    		}
    	}
    	else /* resetting condition */
    	{
    	    if( McrAbs(Bank_Angle_Final) < esp_tempW3 )
    		{
				ldesps16BankLowerSuspectCounter = 0;
    		    ldespu1BankLowerSuspectFlag     = 0;
    		}
    		else if( McrAbs(Bank_Angle_Final) < tempW7 )
    		{
    		    if( ldesps16BankLowerSuspectCounter > 0 )
    		    {
    		        ldesps16BankLowerSuspectCounter = ldesps16BankLowerSuspectCounter - S8_BANK_LOWER_SUSPECT_TIME_SUB;
    		    }
    		    else
    		    {
    		        ldesps16BankLowerSuspectCounter = 0;
    		        ldespu1BankLowerSuspectFlag     = 0;
    		    }
    		}
    		else
    		{
    		    ;
    		}
    	}
	#endif
	/* END
	bank angle suspect for ESC 110812.HBJEON */
}


void LDESP_vDetSensorReliability(void)
{
  /*---------------------------------------------------------------------------------------------*/
  /*       Tuning Parameters                                                                     */
  /*---------------------------------------------------------------------------------------------*/
    
    int16_t Beta_Sensor_Offset_Check_Min_Time = L_U16_TIME_10MSLOOP_10S; /* 10sec. with 1degree offset  */
    int16_t Beta_Sensor_Offset_Check_Max_Time = L_U16_TIME_10MSLOOP_20S; /* 20sec. with 1degree offset  */
    int16_t Beta_Sensor_Offset_Check_Reset_Time = L_U16_TIME_10MSLOOP_5S; /* 5sec. within 1degree offset  */
    
    int16_t Beta_Small_Sensor_Offset_Check_Time = L_U16_TIME_10MSLOOP_10S; /* 10sec. with 0.5degree offset  */
    int16_t Beta_Small_Sensor_Offset_Reset_Time = L_U16_TIME_10MSLOOP_5S;  /* 5sec. within 0.5degree offset */
  
  /*---------------------------------------------------------------------------------------------*/
    
    Beta_Rate_Offset = Beta_K_Dot2 - Beta_MD_Dot;
    esp_tempW0 = McrAbs(wstr);
    esp_tempW1 = McrAbs(Beta_Rate_Offset);
    
    if(vref5 <= VREF_5_KPH)
    {
        Beta_Rate_Offset =0;
        Beta_Sensor_Offset_counter=0;
        Beta_Small_Sensor_Offset_counter=0;
    }       
    else if((esp_tempW0<WSTR_180_DEG)&&(Linear_Gain_F>=90)&&(ABS_fz==0)&&(YAW_CDC_WORK==0))
    { 
        if((esp_tempW1>=S16_Sensor_Offset_Level2)&&(det_wstr_dot<=WSTR_100_DEG)&&(vref5 >= VREF_10_KPH))
        {
            Beta_Sensor_Offset_counter+=2;
        }
        else if(esp_tempW1>=S16_Sensor_Offset_Level1)
        {
            Beta_Sensor_Offset_counter+=1;
        }
        else if(esp_tempW1<=50)
        {
            Beta_Sensor_Offset_counter-=2;
        }
        else if(esp_tempW1<=100)
        {
            Beta_Sensor_Offset_counter-=1;
        }
        else
        {
            ;
        }
    
      /* 20sec => counter = 2857 */
        if(Beta_Sensor_Offset_counter>L_U16_TIME_10MSLOOP_20S) 
        {
            Beta_Sensor_Offset_counter = L_U16_TIME_10MSLOOP_20S;
        }
        else if(Beta_Sensor_Offset_counter<0)
        {
            Beta_Sensor_Offset_counter = 0;
        }
        else
        {
            ;
        }
        
        if((esp_tempW1>=S16_Sensor_Small_Offset_Level2)&&(esp_tempW0<WSTR_90_DEG)&&(det_wstr_dot<=WSTR_50_DEG))
        {
            Beta_Small_Sensor_Offset_counter+=2;
        }
        else if((esp_tempW1>=S16_Sensor_Small_Offset_Level1)&&(esp_tempW0<WSTR_120_DEG)&&(det_wstr_dot<=WSTR_100_DEG))
        {
            Beta_Small_Sensor_Offset_counter+=1;
        }
        else if(esp_tempW1<=30)
        {
            Beta_Small_Sensor_Offset_counter-=2;
        }
        else if(esp_tempW1<=50)
        {
            Beta_Small_Sensor_Offset_counter-=1;
        }
        else
        {
            ;
        }
    	
    	tempW0 = L_U16_TIME_10MSLOOP_10S + 2;
      /* 10sec => counter = 1000+2 */
        if(Beta_Small_Sensor_Offset_counter>tempW0) 
        {
            Beta_Small_Sensor_Offset_counter = tempW0;
        }
        else if(Beta_Small_Sensor_Offset_counter<0)
        {
            Beta_Small_Sensor_Offset_counter = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }     
    
    if(Beta_Sensor_Offset_counter>Beta_Sensor_Offset_Check_Max_Time)
    {
        Beta_Sensor_Offset_Detect_Flag  = 1;
    }
    else if(Beta_Sensor_Offset_counter>Beta_Sensor_Offset_Check_Min_Time)
    {
        Beta_Sensor_Offset_Suspect_Flag = 1;
    }
    else
    {        
        if((Beta_Sensor_Offset_Detect_Flag==1)||(Beta_Sensor_Offset_Suspect_Flag==1))
        {
            if( Beta_Sensor_Offset_counter<Beta_Sensor_Offset_Check_Reset_Time )
            {
                Beta_Sensor_Offset_Suspect_Flag = 0;
                Beta_Sensor_Offset_Detect_Flag  = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    }
    
    if((Beta_Small_Sensor_Offset_Flag==0)&&(Beta_Small_Sensor_Offset_counter>Beta_Small_Sensor_Offset_Check_Time))
    {
        Beta_Small_Sensor_Offset_Flag = 1;
    }
    else if((Beta_Small_Sensor_Offset_Flag==1)&&(Beta_Small_Sensor_Offset_counter<Beta_Small_Sensor_Offset_Reset_Time))
    {
        Beta_Small_Sensor_Offset_Flag = 0;
    }
    else
    {
    	;
    }
}


void LDESP_vEstRoadMuByWheelSlip(void)
{
    if(vref5 >= VREF_100_KPH )
    {    
        tempW2 = vref5;   tempW1 = S16_Det_Thres_Drv_Slip1;       tempW0 = 800;
        s16muls16divs16();    
        Beta_tempW0 = tempW3;
        
        tempW2 = vref5;   tempW1 = S16_Det_Thres_Drv_Slip2;       tempW0 = 800;
        s16muls16divs16();    
        Beta_tempW1 = tempW3;
        
        tempW2 = vref5;   tempW1 = S16_Reset_Thres_Drv_Slip;      tempW0 = 800;
        s16muls16divs16();    
        Beta_tempW2 = tempW3;
        
        tempW2 = vref5;   tempW1 = S16_Det_Thres_Drv_Big_Slip1;   tempW0 = 800;
        s16muls16divs16();    
        Beta_tempW3 = tempW3;
        
        tempW2 = vref5;   tempW1 = S16_Det_Thres_Drv_Big_Slip2;   tempW0 = 800;
        s16muls16divs16();    
        Beta_tempW4 = tempW3;
    }
    else
    {
    	Beta_tempW0 = S16_Det_Thres_Drv_Slip1;
    	Beta_tempW1 = S16_Det_Thres_Drv_Slip2;
    	Beta_tempW2 = S16_Reset_Thres_Drv_Slip;
    	Beta_tempW3 = S16_Det_Thres_Drv_Big_Slip1;
    	Beta_tempW4 = S16_Det_Thres_Drv_Big_Slip2;
    }
    
  #if __REAR_D
    tempW1 = RL.vrad_crt - vref;
    tempW2 = RR.vrad_crt - vref;
    tempW3 = FL.vrad_crt - vref;
    tempW4 = FR.vrad_crt - vref;
    tempW5 = (int16_t)ESP_BRAKE_CONTROL_RL;
    tempW6 = (int16_t)ESP_BRAKE_CONTROL_RR;
    tempW7 = (int16_t)ESP_BRAKE_CONTROL_FL;
    tempW8 = (int16_t)ESP_BRAKE_CONTROL_FR;
  #else  
    tempW1 = FL.vrad_crt - vref;
    tempW2 = FR.vrad_crt - vref;
    tempW3 = RL.vrad_crt - vref;
    tempW4 = RR.vrad_crt - vref;
    tempW5 = (int16_t)ESP_BRAKE_CONTROL_FL;
    tempW6 = (int16_t)ESP_BRAKE_CONTROL_FR;
    tempW7 = (int16_t)ESP_BRAKE_CONTROL_RL;
    tempW8 = (int16_t)ESP_BRAKE_CONTROL_RR;
  #endif
  
    if(vref5 <= VREF_20_KPH )
    {
        MedLowMu_Suspect_Flag =0;
        MedLowMu_Suspect_Flag1=0;
        MedLowMu_Suspect_Flag2=0;
        MedLowMu_Suspect_Flag3=0;
        MedLowMu_Detect_BySlip_Flag =0;
        MedLowMu_Detect_BySlip_Flag1=0;
        MedLowMu_Detect_BySlip_Flag2=0;
        MedLowMu_Detect_BySlip_Flag3=0;
    }
    else if((Rough_road_suspect_vehicle==0)&&(Rough_road_detect_vehicle==0)&&(det_alatm<=LAT_0G6G)&&(mtp>=10))
    {
        if((MedLowMu_Suspect_Flag1==0)&&((tempW1>Beta_tempW0)||(tempW2>Beta_tempW0)))
        {
            MedLowMu_Suspect_Flag1=1;
        }
        else if((MedLowMu_Suspect_Flag1==0)&&(tempW1>Beta_tempW1)&&(tempW2>Beta_tempW1))
        {
            MedLowMu_Suspect_Flag1=1;
        }
        else if((MedLowMu_Suspect_Flag1==1)&&(tempW1<=Beta_tempW2)&&(tempW2<=Beta_tempW2))
        {
        	MedLowMu_Suspect_Flag1=0;
        }
        else
        {
            ;	
        }
        
        if(MedLowMu_Suspect_Flag2==0) /* Driven Wheel */
        {
            if(((tempW5==1)&&(tempW2>Beta_tempW0))
             ||((tempW6==1)&&(tempW1>Beta_tempW0)))
            {
        	    MedLowMu_Suspect_Flag2=1;
            }
            else
            {
                ;	
            }
        }
        else
        {
            if((tempW1<=Beta_tempW2)&&(tempW2<=Beta_tempW2))
            {
            	MedLowMu_Suspect_Flag2=0;
            }
            else
            {
            	;
            }       
        }
                
        if(MedLowMu_Suspect_Flag3==0) /* Non-Driven Wheel of 4WD */
        {
            if(((tempW7==1)&&(tempW4>Beta_tempW0))
             ||((tempW8==1)&&(tempW3>Beta_tempW0)))
            {
      	        MedLowMu_Suspect_Flag3=1;
            }
            else
            {
                ;	
            }
        }
        else
        {
            if((tempW3<=Beta_tempW2)&&(tempW4<=Beta_tempW2))
            {
            	MedLowMu_Suspect_Flag3=0;
            }
            else
            {
            	;
            }       
        }
    
    #if __4WD_VARIANT_CODE==ENABLE     
        if((MedLowMu_Suspect_Flag1==1)||(MedLowMu_Suspect_Flag2==1)
        ||((MedLowMu_Suspect_Flag3==1)&&(lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)))
    #else
      #if __4WD
        if((MedLowMu_Suspect_Flag1==1)||(MedLowMu_Suspect_Flag2==1)||(MedLowMu_Suspect_Flag3==1))
      #else
        if((MedLowMu_Suspect_Flag1==1)||(MedLowMu_Suspect_Flag2==1))
      #endif
    #endif    
        {
            MedLowMu_Suspect_Flag=1;
        }
        else
        {
        	MedLowMu_Suspect_Flag=0;
        }
        
    /*---------------------------------------------------------------------------------------------*/
    /*             MedLow Mu Detection by using only Wheel Slip                                         */
    /*---------------------------------------------------------------------------------------------*/  
        
        if((MedLowMu_Detect_BySlip_Flag1==0)&&(det_alatm<=LAT_0G5G)&&((tempW1>Beta_tempW3)||(tempW2>Beta_tempW3)))
        {
            MedLowMu_Detect_BySlip_Flag1=1;
        }
        else if((MedLowMu_Detect_BySlip_Flag1==0)&&(det_alatm<=LAT_0G5G)&&(tempW1>Beta_tempW4)&&(tempW2>Beta_tempW4))
        {
            MedLowMu_Detect_BySlip_Flag1=1;
        }
        else if((MedLowMu_Detect_BySlip_Flag1==1)&&(tempW1<=Beta_tempW2)&&(tempW2<=Beta_tempW2))
        {
        	MedLowMu_Detect_BySlip_Flag1=0;
        }
        else
        {
            ;	
        }
        
        if((MedLowMu_Detect_BySlip_Flag2==0)&&(det_alatm<=LAT_0G5G)) /* Driven Wheel */
        {
            if(((tempW5==1)&&(tempW2>Beta_tempW3))
             ||((tempW6==1)&&(tempW1>Beta_tempW3)))
            {
        	    MedLowMu_Detect_BySlip_Flag2=1;
            }
            else
            {
                ;	
            }
        }
        else
        {
            if((tempW1<=Beta_tempW2)&&(tempW2<=Beta_tempW2))
            {
            	MedLowMu_Detect_BySlip_Flag2=0;
            }
            else
            {
            	;
            }       
        }
                
        if((MedLowMu_Detect_BySlip_Flag3==0)&&(det_alatm<=LAT_0G5G)) /* Non-Driven Wheel of 4WD */
        {
            if(((tempW7==1)&&(tempW4>Beta_tempW3))
             ||((tempW8==1)&&(tempW3>Beta_tempW3)))
            {
      	        MedLowMu_Detect_BySlip_Flag3=1;
            }
            else
            {
                ;	
            }
        }
        else
        {
            if((tempW3<=Beta_tempW2)&&(tempW4<=Beta_tempW2))
            {
            	MedLowMu_Detect_BySlip_Flag3=0;
            }
            else
            {
            	;
            }       
        }
    
    #if __4WD_VARIANT_CODE==ENABLE     
        if((MedLowMu_Detect_BySlip_Flag1==1)||(MedLowMu_Detect_BySlip_Flag2==1)
        ||((MedLowMu_Detect_BySlip_Flag3==1)&&(lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)))
    #else
      #if __4WD
        if((MedLowMu_Detect_BySlip_Flag1==1)||(MedLowMu_Detect_BySlip_Flag2==1)||(MedLowMu_Detect_BySlip_Flag3==1))
      #else
        if((MedLowMu_Detect_BySlip_Flag1==1)||(MedLowMu_Detect_BySlip_Flag2==1))
      #endif
    #endif    
        {
            MedLowMu_Detect_BySlip_Flag=1;
        }
        else
        {
        	MedLowMu_Detect_BySlip_Flag=0;
        }
    }
    else
    {
    	MedLowMu_Suspect_Flag =0;
        MedLowMu_Suspect_Flag1=0;
        MedLowMu_Suspect_Flag2=0;
        MedLowMu_Suspect_Flag3=0;
        MedLowMu_Detect_BySlip_Flag =0;
        MedLowMu_Detect_BySlip_Flag1=0;
        MedLowMu_Detect_BySlip_Flag2=0;
        MedLowMu_Detect_BySlip_Flag3=0;
    }
}

/*
void LDESP_vEstRoadMuByESPSlip2(void)
{
  //---------------------------------------------------------------------------------------------
  //       Tuning Parameters                                                                     
  //--------------------------------------------------------------------------------------------- 
  
	int16_t ldescs16SlipMuTemp;
		
  //---------------------------------------------------------------------------------------------

    if( rel_lam_fl > -LAM_1P )
    {
        ldescs8WheelSlipFL = -LAM_1P;
    }
    else
    {
    	ldescs8WheelSlipFL = rel_lam_fl;
    }
    
    if( rel_lam_fr > -LAM_1P )
    {
        ldescs8WheelSlipFR = -LAM_1P;
    }
    else
    {
    	ldescs8WheelSlipFR = rel_lam_fr;
    }
    
  //---------------------------------------------------------------------------------------------
	
	if(ESP_BRAKE_CONTROL_FL==1)
    {
    	if((esp_control_mode_fl<=2)&&(ldescs16RiseModeTimeFL<=L_U8_TIME_5MSLOOP_1000MS)) // Rise Mode
    	{
    	   ldescs16RiseModeTimeFL += 1;
       	}
    	else if((esp_control_mode_fl==3)||(ldescs16RiseModeTimeFL>L_U8_TIME_5MSLOOP_1000MS)) // Hold Mode
    	{
    		ldescs16RiseModeTimeFL = ldescs16RiseModeTimeFL;
    	}
    	else if(ldescs16RiseModeTimeFL>4)
    	{
    		ldescs16RiseModeTimeFL -= 4;
    	}
    	else
    	{
    		ldescs16RiseModeTimeFL = 0;
    	}
    }
    else
    {
    	ldescs16RiseModeTimeFL = 0;
    }
    
    if(ESP_BRAKE_CONTROL_FR==1)
    {
    	if((esp_control_mode_fr<=2)&&(ldescs16RiseModeTimeFR<=L_U8_TIME_5MSLOOP_1000MS)) // Rise Mode
    	{
    	   ldescs16RiseModeTimeFR += 1;
       	}
    	else if((esp_control_mode_fr==3)||(ldescs16RiseModeTimeFR>L_U8_TIME_5MSLOOP_1000MS)) // Hold Mode
    	{
    		ldescs16RiseModeTimeFR = ldescs16RiseModeTimeFR;
    	}
    	else if(ldescs16RiseModeTimeFR>4)
    	{
    		ldescs16RiseModeTimeFR -= 4;
    	}
    	else
    	{
    		ldescs16RiseModeTimeFR = 0;
    	}
    }
    else
    {
    	ldescs16RiseModeTimeFR = 0;
    }

  //---------------------------------------------------------------------------------------------
  
    if((vref5>=VREF_30_KPH)&&(ESP_BRAKE_CONTROL_FL==1)&&((esp_control_mode_fl==3)||(esp_control_mode_fl==4))&&
    (((ldescs8WheelSlipFL<=-S8_SLIP_MU_MIN_SLIP_THRES)&&(FL.u8_Estimated_Active_Press>=5))||
    ((FL.u8_Estimated_Active_Press>=U8_SLIP_MU_MIN_PRESS)&&(ldescs16RiseModeTimeFL>=(int16_t)U8_SLIP_MU_MIN_RISE_TIME))))
    {
    	ldescu1SlipMuDetectionFL = 1;
    	ldescu8SlipMuFlagResetCntFL = U8_SLIP_MU_FLAG_RESET_TIME;
    }
    else if((vref5>=VREF_30_KPH)&&(ESP_BRAKE_CONTROL_FL==1)&&(ldescu1SlipMuDetectionFL==1))
    {
    	ldescu1SlipMuDetectionFL = 1;
    }
    else if((vref5>=VREF_30_KPH)&&(ldescu1SlipMuDetectionFL==1)&&(ldescu8SlipMuFlagResetCntFL>0))
    {
    	ldescu1SlipMuDetectionFL = 1;
    	ldescu8SlipMuFlagResetCntFL -= 1;
    }
    else
    {
    	ldescu1SlipMuDetectionFL = 0;
    	ldescu8SlipMuFlagResetCntFL = 0;
    }
  
    if((vref5>=VREF_30_KPH)&&(ESP_BRAKE_CONTROL_FR==1)&&((esp_control_mode_fr==3)||(esp_control_mode_fr==4))&&
    (((ldescs8WheelSlipFR<=-S8_SLIP_MU_MIN_SLIP_THRES)&&(FR.u8_Estimated_Active_Press>=5))||
    ((FR.u8_Estimated_Active_Press>=U8_SLIP_MU_MIN_PRESS)&&(ldescs16RiseModeTimeFR>=(int16_t)U8_SLIP_MU_MIN_RISE_TIME))))
    {
    	ldescu1SlipMuDetectionFR = 1;
    	ldescu8SlipMuFlagResetCntFR = U8_SLIP_MU_FLAG_RESET_TIME;
    }
    else if((vref5>=VREF_30_KPH)&&(ESP_BRAKE_CONTROL_FR==1)&&(ldescu1SlipMuDetectionFR==1))
    {
    	ldescu1SlipMuDetectionFR = 1;
    }
    else if((vref5>=VREF_30_KPH)&&(ldescu1SlipMuDetectionFR==1)&&(ldescu8SlipMuFlagResetCntFR>0))
    {
    	ldescu1SlipMuDetectionFR = 1;
    	ldescu8SlipMuFlagResetCntFR -= 1;
    }
    else
    {
    	ldescu1SlipMuDetectionFR = 0;
    	ldescu8SlipMuFlagResetCntFR = 0;
    }
  
    if(((ldescu1SlipMuDetectionFL==1)||(ldescu1SlipMuDetectionFR==1))&&(MPRESS_BRAKE_ON==0))
    {
    	ldescu1SlipMuDetectionVehicle = 1;
    }
    else
    {
    	ldescu1SlipMuDetectionVehicle = 0;
    }
    
  //---------------------------------------------------------------------------------------------
    
    if((vref5>=VREF_20_KPH)&&(ESP_BRAKE_CONTROL_FL==1)
    &&((FL.u8_Estimated_Active_Press>=10)||((FL.u8_Estimated_Active_Press>=1)&&(ldescs8WheelSlipFL<=-LAM_5P))))
    {
        tempW2 = FL.u8_Estimated_Active_Press;    tempW1 = det_alatm;    tempW0 = -ldescs8WheelSlipFL*((int16_t)U8_SLIP_MU_LEVEL_GAIN);
        s16muls16divs16();    
        
        if(tempW3>=80)
        {
            ldescs16SlipMuFL = 800;
        }
        else
        {
        	ldescs16SlipMuFL = tempW3*10;
        }
    }
    else
    {
    	ldescs16SlipMuFL = 800;
    }
    
    if((vref5>=VREF_20_KPH)&&(ESP_BRAKE_CONTROL_FR==1)
    &&((FR.u8_Estimated_Active_Press>=10)||((FR.u8_Estimated_Active_Press>=1)&&(ldescs8WheelSlipFR<=-LAM_5P))))
    {
        tempW2 = FR.u8_Estimated_Active_Press;    tempW1 = det_alatm;    tempW0 = -ldescs8WheelSlipFR*((int16_t)U8_SLIP_MU_LEVEL_GAIN);
        s16muls16divs16();    
        
        if(tempW3>=80)
        {
            ldescs16SlipMuFR = 800;
        }
        else
        {
        	ldescs16SlipMuFR = tempW3*10;
        }
    }
    else
    {
    	ldescs16SlipMuFR = 800;
    }
  
  //---------------------------------------------------------------------------------------------
    
    if((ldescu1SlipMuDetectionFL==1)&&(ldescu1SlipMuDetectionFR==0))
    {   
        ldescs16SlipMuTemp = ldescs16SlipMuFL;
    }
    else if((ldescu1SlipMuDetectionFL==0)&&(ldescu1SlipMuDetectionFR==1))
    {
        ldescs16SlipMuTemp = ldescs16SlipMuFR;
    }
    else
    {
        if(ldescs16SlipMuFL > ldescs16SlipMuFR)
        {
        	ldescs16SlipMuTemp = ldescs16SlipMuFR;
        }
        else
        {
        	ldescs16SlipMuTemp = ldescs16SlipMuFL;
        }
    }
     
    if(ldescs16SlipMuVehicle >= ldescs16SlipMuTemp)
    {
    	ldescs16SlipMuVehicle = ldescs16SlipMuTemp;
    }
    else
    {
        if((ldescu1SlipMuDetectionFL==1)&&(ldescu1SlipMuDetectionFR==1)
        &&((ldescu1SlipMuDetectionFLOld+ldescu1SlipMuDetectionFROld)==1))
        {
        	ldescs16SlipMuVehicle = ldescs16SlipMuTemp;
        }
        else if((ldescu1SlipMuDetectionFL==1)||(ldescu1SlipMuDetectionFR==1))
    	{
    		ldescs16SlipMuVehicle = ldescs16SlipMuVehicleOld;
    	}
    	else if((ldescs16SlipMuVehicleOld+(int16_t)U8_SLIP_MU_RESET_RATE)>ldescs16SlipMuTemp)
    	{
    		ldescs16SlipMuVehicle = ldescs16SlipMuTemp;
    	}
    	else
    	{
    		ldescs16SlipMuVehicle = ldescs16SlipMuVehicleOld + (int16_t)U8_SLIP_MU_RESET_RATE;
    	}
    }
  
    ldescs16SlipMuVehicleOld = ldescs16SlipMuVehicle;
    ldescu1SlipMuDetectionFLOld = ldescu1SlipMuDetectionFL;
    ldescu1SlipMuDetectionFROld = ldescu1SlipMuDetectionFR;  
}
*/
/*11.10.07.hbjeon*/

#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPEstBeta
	#include "Mdyn_autosar.h"
#endif
