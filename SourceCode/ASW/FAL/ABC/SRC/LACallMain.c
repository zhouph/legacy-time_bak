/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LACallMain.c
* Description:      Actuation Main Code of Logic
* Logic version:    HV25
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C12         eslim           Initial Release
********************************************************************************/

/* Includes ********************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LACallMain
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LACallMain.h"
#include "LAABSCallActCan.h"
#include "LAABSCallActHW.h"
#include "LAABSGenerateLFCDuty.h"
#include "LAHSACallActHW.h"
#include "LCHSACallControl.h"
#include "LCAVHCallControl.h"
#include "LAAVHCallActHW.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCDECCallControl.h"
#include "LCEPBCallControl.h"
#include "Hardware_Control.h"
#include "LAHBBCallActHW.h"
#include "LAESPActuateNcNovalveF.h"
#include "LCBDWCallControl.h"
#include "LAEPCCallActHW.h"
#include "LAARBCallActHW.h"
#include "LAHRBCallActHW.h"
#include "LCHRBCallControl.h"
#include "LCARBCallControl.h"
#include "LCFBCCallControl.h"
#include "LASPASCallActHW.h"
#include "LCSPASCallControl.h"
#include "LCEVPCallControl.h"
#include "LAABSCallHWTest.h"
#include "LAESPCallTestHW.h"

#if __TVBB 
#include "LCTVBBCallControl.h" 
#endif 
#if __PAC 
#include "LCPACCallControl.h" 
#endif 
#if __CBC 
#include "LCCBCCallControl.h" 
#endif
#if __SLS 
#include "LCSLSCallControl.h" 
#endif
  #if __SCC
#include "SCC/LCWPCCallControl.h"
  #endif

#include "LCPBACallControl.h"

#if	defined(BTCS_TCMF_CONTROL) 	/* BTCS_TCMF_CONTROL */	
#include "LABTCSCallActHW.h"
#endif							/* BTCS_TCMF_CONTROL */	

  #if (__IDB_LOGIC == ENABLE)
#include "LCABSStrokeRecovery.h"
  #endif

/* Local Definiton ***********************************************************/

SOL_LOGIC_DUTY_t la_FLNO,la_FRNO,la_RLNO,la_RRNO;
SOL_LOGIC_DUTY_t la_FLNC,la_FRNC,la_RLNC,la_RRNC;
/*uint8_t la_FLNC,la_FRNC,la_RLNC,la_RRNC;*/
SOL_LOGIC_DUTY_t la_PTC,la_STC,la_PSV,la_SSV;
#if __ADDITIONAL_ESV_VALVES
SOL_LOGIC_DUTY_t la_PRSV,la_SRSV;
#endif
LA_FLAG_t laValveFlags0;


/* Global Variables Declaration************************************************/
WHEEL_VV_OUTPUT_t la_FL1HP, la_FL2HP, la_FR1HP, la_FR2HP;
WHEEL_VV_OUTPUT_t la_RL1HP, la_RL2HP, la_RR1HP, la_RR2HP;

#if __VDC
WHEEL_VV_OUTPUT_t laesc_FL1HP,laesc_FL2HP,laesc_FR1HP,laesc_FR2HP;
WHEEL_VV_OUTPUT_t laesc_RL1HP,laesc_RL2HP,laesc_RR1HP,laesc_RR2HP;

WHEEL_VV_OUTPUT_t laepc_FL1HP,laepc_FL2HP,laepc_FR1HP,laepc_FR2HP;
WHEEL_VV_OUTPUT_t laepc_RL1HP,laepc_RL2HP,laepc_RR1HP,laepc_RR2HP;
#endif

#if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F || __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
WHEEL_VV_OUTPUT_t lavalvetest_FL1HP,lavalvetest_FL2HP,lavalvetest_FR1HP,lavalvetest_FR2HP;
WHEEL_VV_OUTPUT_t lavalvetest_RL1HP,lavalvetest_RL2HP,lavalvetest_RR1HP,lavalvetest_RR2HP;
#endif

#if __TVBB
WHEEL_VV_OUTPUT_t latvbb_FL1HP,latvbb_FL2HP,latvbb_FR1HP,latvbb_FR2HP;
WHEEL_VV_OUTPUT_t latvbb_RL1HP,latvbb_RL2HP,latvbb_RR1HP,latvbb_RR2HP;
#endif

#if __TSP
WHEEL_VV_OUTPUT_t latsp_FL1HP,latsp_FL2HP,latsp_FR1HP,latsp_FR2HP;
WHEEL_VV_OUTPUT_t latsp_RL1HP,latsp_RL2HP,latsp_RR1HP,latsp_RR2HP;
#endif

extern	int8_t	idbmon_1, idbmon_2, idbmon_3, idbmon_4, idbmon_5;

/* Local Function prototype ***************************************************/
void LA_vAllocValveDutyArray(void);
void LA_vAllocWheelValveDutyArray(void);
#if (__MGH_80_10MS == DISABLE) 
void LA_vAllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC);
#else /* (__MGH_80_10MS == DISABLE) */
void LA_vAllocWheelValveDutyArrayWL(const WHEEL_VV_OUTPUT_t *pWL1HP, const WHEEL_VV_OUTPUT_t *pWL2HP, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC);
#endif /* (__MGH_80_10MS == DISABLE) */
/*
void LA_vAllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO, uint8_t pNC);
uint8_t LA_u8AllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO);
*/

  #if __ECU==ESP_ECU_1
void LA_vAllocCircuitValveDutyArray(void);
  #endif
/*
#if __NC_CURRENT_CONTROL
void LA_vGenerateNCDutyWL(struct W_STRUCT *WL);
#endif
*/

static void LA_vResetValveActOutput(void);

/* GlobalFunction prototype **************************************************/
#if __TCS || __BTC
extern void LATCS_vCallActHW(void);
#endif

#if __TCS
extern void LATCS_vCallActCan(void);
#endif

#if __VDC
extern void LAESP_vCallActHW(void);
extern void LAESP_vCallActCan(void);
extern void LABA_vCallActHW(void);
#endif

#if __HDC
extern void LAHDC_vCallActHW(void);
#endif

#if __ACC
extern void LAACC_vCallActHW(void);
#endif

#if __ACC
extern void LAACC_vCallActCan(void);
#endif

#if __DEC
extern void LADEC_vCallActHW(void);
extern void LADEC_vCallActCan(void);
#endif

#if __EPB_INTERFACE
extern void LAEPB_vCallActHW(void);
#endif

#if __TSP
extern void LATSP_vCallActHW(void);
#endif

#if __BDW
extern void LABDW_vCallActHW(void);
#endif

#if __CBC
extern void LACBC_vCallActHW(void);
#endif

#if __SLS
extern void LASLS_vCallActHW(void);
#endif

#if __TVBB
extern void LATVBB_vCallActHW(void); 
#endif 
#if __EBP
extern void LAEBP_vCallActHW(void);
#endif

#if __ENABLE_PREFILL
extern void LAEPC_vCallActHW(void);
#endif

#if __EDC
extern void LAEDC_vCallActCan(void);
#endif
#if defined(__UCC_1)
void LA_CalcSportModeOutput(void);
#endif

extern void LAABS_vSetMotorCommand(void);

/* Implementation *************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LA_vCallMainValve
* CALLED BY:            L_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Actuation Valves on demand of each system
********************************************************************************/
void LA_vCallMainValve(void)
{
/**************************************************************************
* Active Flag Command
***************************************************************************/

#if __VDC
    FLAG_ACTIVE_BRAKING = YAW_CDC_WORK||ABS_fz||BTCS_ON||PBA_ON||EPC_ON
#if __FBC
                    ||FBC_ON
#endif
#if __HOB
                    ||lcu1HobOnFlag
#endif
#if __BDW
                    ||BDW_ON
#endif
#if __TSP
                    ||TSP_ON
#endif
#if __TVBB
					||lctvbbu1TVBB_ON
#endif
#if __HDC
                    ||lcu1HdcActiveFlg
#endif
#if __HSA
                    ||HSA_flg
#endif
#if __AVH
                    ||lcu1AvhActFlg
                    ||lcu1AvhAutonomousActFlg
#endif                    
#if __SCC
                    ||(lcu8WpcActiveFlg==1)
#endif
#if __ACC
                    ||lcu1AccActiveFlg
#endif
#if __EPB_INTERFACE
                    ||lcu1EpbActiveFlg
#endif
#if __SBC
                    ||lcu1SpasActiveFlg
#endif
#if __TCMF_LowVacuumBoost
                    ||TCMF_LowVacuumBoost_flag
#endif
#if __HRB
                    ||lchrbu1ActiveFlag
#endif

#if __PAC
					||lcpacu1PacActiveFlg
#endif

#if __CBC
					||CBC_ON
#endif

#if __SLS
					||SLS_ON
#endif

                    ||ESP_PULSE_DUMP_FLAG;
#endif
    

/**************************************************************************
* Reset VALVE_ACT Flag Command
***************************************************************************/
    VALVE_ACT=0;

  #if __ADDITIONAL_ESV_VALVES
    RS_VALVE_PRIMARY   = 0;
    RS_VALVE_SECONDARY = 0;
  #endif
	LA_vResetValveActOutput();
	
    LAABS_vCallActHW();

  #if __TCS || __BTC || __ETC
    LATCS_vCallActHW();
    #if	defined(BTCS_TCMF_CONTROL) /* BTCS_TCMF_CONTROL */	
    LATCS_vCallActHWGen2();
    #endif	/* BTCS_TCMF_CONTROL */
  #endif
/*
  #if __VDC
	LAESP_vCallActHW();
	#endif
*/
   #if __VDC
    LABA_vCallActHW();
  #endif
  
  #if __HOB
    LAHOB_vCallActHW();
  #endif

  #if __HRB
    LAHRB_vCallActHW();
  #endif

  #if __TCMF_LowVacuumBoost
    LAHBB_vCallActHW();
  #endif

  #if __HSA
    LAHSA_vCallActHW();
  #endif

  #if __AVH
    LAAVH_vCallActHW();
  #endif  

  #if __SCC
    LASCC_vCallActHW();
  #endif
  
  #if __DEC
    LADEC_vCallActHW();
  #endif

  #if __ACC
    LAACC_vCallActHW();
  #endif

  #if __HDC
    LAHDC_vCallActHW();
  #endif

  #if __EPB_INTERFACE
    LAEPB_vCallActHW();
  #endif
  
  #if __SBC
    LASPAS_vCallActHW();
  #endif

  #if __TSP
    LATSP_vCallActHW();
  #endif
  
  #if __TVBB
    LATVBB_vCallActHW();
  #endif

  #if __BDW
    LABDW_vCallActHW();
  #endif

  #if __EBP
    LAEBP_vCallActHW();
  #endif

  #if ((__ENABLE_PREFILL) && (__AHB_GEN3_SYSTEM == DISABLE) && (__IDB_LOGIC==DISABLE))
    LAEPC_vCallActHW();
  #endif

  #if __CBC 
    LACBC_vCallActHW();
  #endif
  
  #if __PAC
if (U8_PACC_ENABLE == 1)
{  
    LAPAC_vCallActHW();
}
  #endif  

  #if __SLS            
    LASLS_vCallActHW();
  #endif                 
  
  #if __VDC
    LAESP_vCallActHW(); 
  #endif

#if __HEV
	LAARB_vCallActHW();
#endif

  #if __TCMF_CONTROL
   #if __TCMF_CONTROL2
    TCNO_CURRENT_CONTROL();
   #else
    LAMFC_vSetMFCCurrent();
   #endif
  #endif

/*
  #if __VDC 
    LAABS_vGenerateLFCDuty(); 
  #endif 
*/
  #if __HW_TEST_MODE
	LAABS_vTestWheelNoValve();
  #endif

  #if (__IDB_HW_TEST_MODE == ENABLE)
	LAIDB_vCallTestMode();
  #endif

    LA_vAllocValveDutyArray();

  #if ((__AHB_GEN3_SYSTEM==DISABLE) && (__IDB_LOGIC == DISABLE))
    LAABS_vSetMotorCommand();
  #endif

    
#if __BTC
    #if __SPLIT_TYPE==0
    if((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1)||(TCL_DEMAND_fr==1)||
       (TCL_DEMAND_rl==1)) {VALVE_ACT=1;}
    #else
    if((TCL_DEMAND_PRIMARY==1)||(TCL_DEMAND_SECONDARY==1)) {VALVE_ACT=1;}
    #endif
#endif

  #if __BTC || __VDC || __CBC || __BDW || __EBP
    if((HV_VL_fl==1)||(HV_VL_fr==1)||(HV_VL_rl==1)||(HV_VL_rr==1)||
       (AV_VL_fl==1)||(AV_VL_fr==1)||(AV_VL_rl==1)||(AV_VL_rr==1)||
       (BUILT_fl==1)||(BUILT_fr==1)||(BUILT_rl==1)||(BUILT_rr==1)) {VALVE_ACT=1;}
  #endif     
}

/*******************************************************************************
* FUNCTION NAME:        LA_vResetValveActOutput
* CALLED BY:            LA_vCallMainValve()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Reset Valve outputs
********************************************************************************/
static void LA_vResetValveActOutput(void)
{
    VALVE_ACT=0;

  #if __ADDITIONAL_ESV_VALVES
    RS_VALVE_PRIMARY   = 0;
    RS_VALVE_SECONDARY = 0;
  #endif	

	la_FL1HP.u8PwmDuty = 0;
	la_FL1HP.u8InletOnTime = 0;
	la_FL1HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_FL1HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_FL2HP.u8PwmDuty = 0;
	la_FL2HP.u8InletOnTime = 0;
	la_FL2HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_FL2HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_FR1HP.u8PwmDuty = 0;
	la_FR1HP.u8InletOnTime = 0;
	la_FR1HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_FR1HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_FR2HP.u8PwmDuty = 0;
	la_FR2HP.u8InletOnTime = 0;
	la_FR2HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_FR2HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_RL1HP.u8PwmDuty = 0;
	la_RL1HP.u8InletOnTime = 0;
	la_RL1HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_RL1HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_RL2HP.u8PwmDuty = 0;
	la_RL2HP.u8InletOnTime = 0;
	la_RL2HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_RL2HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_RR1HP.u8PwmDuty = 0;
	la_RR1HP.u8InletOnTime = 0;
	la_RR1HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_RR1HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */

	la_RR2HP.u8PwmDuty = 0;
	la_RR2HP.u8InletOnTime = 0;
	la_RR2HP.u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	la_RR2HP.u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */
}

/*******************************************************************************
* FUNCTION NAME:        LA_vCallMainValveIgnOffMode
* CALLED BY:            L_vCallMainIgnOffMode()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Actuation Valves on demand of each system during IGN Off
********************************************************************************/
#if __VDC
void LA_vCallMainValveIgnOffMode(void)
{
    VALVE_ACT=0;

  #if __AVH
    LCAVH_vControlAvhAbnormalMode();
  #endif

  #if ((__ECU==ESP_ECU_1) && (__AHB_GEN3_SYSTEM==DISABLE) && (__IDB_LOGIC==DISABLE))
    LA_vAllocCircuitValveDutyArray();
  #endif

  #if (__SPLIT_TYPE==0)
    if((TCL_DEMAND_fl==1)||(TCL_DEMAND_rr==1)||(TCL_DEMAND_fr==1)||
       (TCL_DEMAND_rl==1)) {VALVE_ACT=1;}
  #else
    if((TCL_DEMAND_PRIMARY==1)||(TCL_DEMAND_SECONDARY==1)) {VALVE_ACT=1;}
  #endif

}
#endif

/*******************************************************************************
* FUNCTION NAME:        LA_vCallMainCan
* CALLED BY:            L_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Communication CAN on demand of each system
********************************************************************************/
void LA_vCallMainCan(void)
{
    LAABS_vCallActCan();

  #if __TCS || __ETC
    LATCS_vCallActCan();
  #endif

  #if __VDC
    LAESP_vCallActCan();
  #endif

  #if defined(__UCC_1)
    LA_CalcSportModeOutput();
  #endif


  #if __EDC
    LAEDC_vCallActCan();
  #endif

  #if __DEC
    LADEC_vCallActCan();
  #endif

  #if __ACC
    LAACC_vCallActCan();
  #endif
}


/*******************************************************************************
* FUNCTION NAME:        LA_vAllocValveDutyArray
* CALLED BY:            LA_vCallMainValve()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Allocation Duty Array of Valves on demand of each system
********************************************************************************/
void LA_vAllocValveDutyArray(void)
{
    LA_vAllocWheelValveDutyArray();

  #if ((__ECU==ESP_ECU_1) && (__AHB_GEN3_SYSTEM==DISABLE) && (__IDB_LOGIC==DISABLE))
    LA_vAllocCircuitValveDutyArray();
  #endif

}

/*******************************************************************************
* FUNCTION NAME:        LA_vAllocWheelValveDutyArray
* CALLED BY:            LA_vAllocValveDutyArray()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Allocation Duty Array of Wheel NO/NC Valves
********************************************************************************/
void LA_vAllocWheelValveDutyArray(void)
{
/*
#if __NC_CURRENT_CONTROL
    LA_vGenerateNCDutyWL(&FL); // To be moved to LA_vGenerateWheelDuty()
    LA_vGenerateNCDutyWL(&FR); // To be moved to LA_vGenerateWheelDuty()
    LA_vGenerateNCDutyWL(&RL); // To be moved to LA_vGenerateWheelDuty()
    LA_vGenerateNCDutyWL(&RR); // To be moved to LA_vGenerateWheelDuty()
#endif
*/
/*
    LA_vAllocWheelValveDutyArrayWL(&FL, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&FR, &la_FRNO, &la_FRNC);
    LA_vAllocWheelValveDutyArrayWL(&RL, &la_RLNO, &la_RLNC);
    LA_vAllocWheelValveDutyArrayWL(&RR, &la_RRNO, &la_RRNC);
*/
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    LA_vAllocWheelValveDutyArrayWL(&la_FL1HP, &la_FL2HP, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&la_RL1HP, &la_RL2HP, &la_RLNO, &la_RLNC);
    LA_vAllocWheelValveDutyArrayWL(&la_FR1HP, &la_FR2HP, &la_FRNO, &la_FRNC);
    LA_vAllocWheelValveDutyArrayWL(&la_RR1HP, &la_RR2HP, &la_RRNO, &la_RRNC);
    
  #elif (__MGH_80_10MS == DISABLE)
    #if __SPLIT_TYPE==0
    LA_vAllocWheelValveDutyArrayWL(&FL, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&RL, &la_RLNO, &la_RLNC);
    #else
    LA_vAllocWheelValveDutyArrayWL(&RL, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&FL, &la_RLNO, &la_RLNC);
    #endif
    LA_vAllocWheelValveDutyArrayWL(&FR, &la_FRNO, &la_FRNC);
    LA_vAllocWheelValveDutyArrayWL(&RR, &la_RRNO, &la_RRNC);

  #else 

    #if __SPLIT_TYPE==0
    LA_vAllocWheelValveDutyArrayWL(&la_FL1HP, &la_FL2HP, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&la_RL1HP, &la_RL2HP, &la_RLNO, &la_RLNC);
    #else
    LA_vAllocWheelValveDutyArrayWL(&la_RL1HP, &la_RL2HP, &la_FLNO, &la_FLNC);
    LA_vAllocWheelValveDutyArrayWL(&la_FL1HP, &la_FL2HP, &la_RLNO, &la_RLNC);
    #endif
    LA_vAllocWheelValveDutyArrayWL(&la_FR1HP, &la_FR2HP, &la_FRNO, &la_FRNC);
    LA_vAllocWheelValveDutyArrayWL(&la_RR1HP, &la_RR2HP, &la_RRNO, &la_RRNC);

  #endif /* (__MGH_80_10MS == DISABLE) */

/*
    #if __SPLIT_TYPE==0
    la_FLNC = LA_u8AllocWheelValveDutyArrayWL(&FL, &la_FLNO);
    la_RLNC = LA_u8AllocWheelValveDutyArrayWL(&RL, &la_RLNO);
    #else
    la_FLNC = LA_u8AllocWheelValveDutyArrayWL(&RL, &la_FLNO);
    la_RLNC = LA_u8AllocWheelValveDutyArrayWL(&FL, &la_RLNO);
    #endif
    la_FRNC = LA_u8AllocWheelValveDutyArrayWL(&FR, &la_FRNO);
    la_RRNC = LA_u8AllocWheelValveDutyArrayWL(&RR, &la_RRNO);
*/
}


/*******************************************************************************
* FUNCTION NAME:        LA_u8AllocWheelNCValveDuty
* CALLED BY:            LA_vAllocWheelValveDutyArrayWL()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Allocation Duty Array of Wheel NC Valves
********************************************************************************/
uint8_t   LA_u8AllocWheelNCValveDuty(int8_t NC_ON_TIME)
{
    uint8_t NC_duty_buffer;

    switch(NC_ON_TIME)
    {
        case 0:
            NC_duty_buffer = 0;
            break;
        case 1:
            NC_duty_buffer = 0x01;
            break;
        case 2:
            NC_duty_buffer = 0x03;
            break;
        case 3:
            NC_duty_buffer = 0x07;
            break;
        case 4:
            NC_duty_buffer = 0x0F;
            break;
        case 5:
            NC_duty_buffer = 0x1F;
            break;
        case 6:
            NC_duty_buffer = 0x3F;
            break;
        case 7:
            NC_duty_buffer = 0x7F;
            break;
        default :
            NC_duty_buffer = 0;
            break;
    }

    return NC_duty_buffer;
}


/*******************************************************************************
* FUNCTION NAME:        LA_vAllocWheelValveDutyArray
* CALLED BY:            LA_vAllocValveDutyArray()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Allocation Duty Array of Wheel NO/NC Valves
********************************************************************************/
#if (__MGH_80_10MS == DISABLE)
void LA_vAllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC)
/*
void LA_vAllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO, uint8_t pNC)
uint8_t LA_u8AllocWheelValveDutyArrayWL(struct W_STRUCT *pCtrlWL, SOL_LOGIC_DUTY_t *pNO)
*/
{
    uint8_t pwm_i;
    uint16_t u16NoPwmDuty16Bit = (uint16_t)(((uint32_t)pCtrlWL->pwm_duty_temp_dash*NO_FULL_ON_DUTY_16BIT)/PWM_duty_HOLD);

    /*uint8_t pNC = 0;*/

    if(pCtrlWL->LFC_Conven_Reapply_Time >= U8_BASE_LOOPTIME)    {           /* Reapply over 5ms (by CSH 2002 Winter) */
        for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
            pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
    }
    else if(pCtrlWL->LFC_Conven_Reapply_Time > 0)    {       /* Reapply over 5ms (by CSH 2002 Winter) */
        for(pwm_i=0; pwm_i<(uint8_t)(pCtrlWL->LFC_Conven_Reapply_Time); pwm_i++)    {
            pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
        for(pwm_i=(uint8_t)(pCtrlWL->LFC_Conven_Reapply_Time); pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;       /* 2 ms reapply, 5ms hold. */
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
    }
    else {

        #if     __VDC
        if((pCtrlWL->L_SLIP_CONTROL==0) &&
           (ABS_fz==1) && (pCtrlWL->LFC_built_reapply_flag==0) && (pCtrlWL->HV_VL==1))  {   /* '04 SW Winter for ESP ����� Pulse-down dump */
        #else
        if((ABS_fz==1) && (pCtrlWL->LFC_built_reapply_flag==0) && (pCtrlWL->HV_VL==1))  {   /* Dump && Hold!  */
        #endif
            if(pCtrlWL->on_time_outlet_pwm >= U8_BASE_LOOPTIME) {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++) {
                    pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                }
                /*pNC = 0xFF;*/
            }
            else if(pCtrlWL->on_time_outlet_pwm > 0) {
                for(pwm_i=0; pwm_i<(pCtrlWL->on_time_outlet_pwm); pwm_i++) {
                    pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                }
                for(pwm_i=(pCtrlWL->on_time_outlet_pwm); pwm_i<U8_BASE_LOOPTIME; pwm_i++)   {
                    pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
                /*pNC = LA_u8AllocWheelNCValveDuty((int8_t)pCtrlWL->on_time_outlet_pwm);*/
            }
            else {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++) {
                    if(pCtrlWL->pwm_duty_temp_dash >= PWM_duty_HOLD)
                    {
                        pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    }
                    else
                    {
                        /*pNO->lau16PwmDuty[pwm_i] = pCtrlWL->pwm_duty_temp_dash;*/
                      #if (__DITHER_PATTERN == DISABLE)
                        pNO->lau16PwmDuty[pwm_i] = (uint16_t)(((uint32_t)pCtrlWL->pwm_duty_temp_dash*NO_FULL_ON_DUTY_16BIT)/PWM_duty_HOLD);
                      #elif ((__DITHER_PATTERN == __1MS_DITHER) || (__DITHER_PATTERN == __2MS_DITHER))
			        	if(pwm_i==(U8_BASE_LOOPTIME-1)) 
			        	{
			        		pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit;
			        	}
			        	else if((pwm_i%U8_DITHER_PERIOD)<(U8_DITHER_PERIOD/2)) 
			        	{
			        		pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit + U16_PWM_DITHER_DUTY;
			        		if(pNO->lau16PwmDuty[pwm_i] > NO_FULL_ON_DUTY_16BIT)
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY_16BIT;
			        		}
			        	}
			        	else
			        	{
			        		if(u16NoPwmDuty16Bit < U16_PWM_DITHER_DUTY)
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
			        		}
			        		else
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit - U16_PWM_DITHER_DUTY;
			        		}
			        	}
                      #else
                        pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit;
                      #endif  
                    }
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
            }
        }
        else {
            if(pCtrlWL->pwm_duty_temp_dash == pwm_duty_null)
            {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
            }
            else if(pCtrlWL->pwm_duty_temp_dash < PWM_duty_HOLD)
            {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    /*pNO->lau16PwmDuty[pwm_i] = pCtrlWL->pwm_duty_temp_dash;*/
                      #if (__DITHER_PATTERN == DISABLE)
                        pNO->lau16PwmDuty[pwm_i] = (uint16_t)(((uint32_t)pCtrlWL->pwm_duty_temp_dash*NO_FULL_ON_DUTY_16BIT)/PWM_duty_HOLD);
                      #elif ((__DITHER_PATTERN == __1MS_DITHER) || (__DITHER_PATTERN == __2MS_DITHER))
			        	if(pwm_i==(U8_BASE_LOOPTIME-1)) 
			        	{
			        		pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit;
			        	}
			        	else if((pwm_i%U8_DITHER_PERIOD)<(U8_DITHER_PERIOD/2)) 
			        	{
			        		pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit + U16_PWM_DITHER_DUTY;
			        		if(pNO->lau16PwmDuty[pwm_i] > NO_FULL_ON_DUTY_16BIT)
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY_16BIT;
			        		}
			        	}
			        	else
			        	{
			        		if(u16NoPwmDuty16Bit < U16_PWM_DITHER_DUTY)
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
			        		}
			        		else
			        		{
			        			pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit - U16_PWM_DITHER_DUTY;
			        		}
			        	}
                      #else
                        pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit;
                      #endif  
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
            }
            else if(pCtrlWL->pwm_duty_temp_dash == PWM_duty_HOLD)
            {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
            }
            else if(pCtrlWL->pwm_duty_temp_dash == PWM_duty_DUMP)
            {
              #if !__VDC
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                }
                /*pNC = 0xFF;*/
              #else     /* __VDC */
              /**********1msec*******************************************************/
                if( (pCtrlWL->REAR_WHEEL==0) && ( ((SLIP_CONTROL_NEED_FL==1)&&(pCtrlWL->LEFT_WHEEL==1))
                	|| ((SLIP_CONTROL_NEED_FR==1)&&(pCtrlWL->LEFT_WHEEL==0)) )  )
                {
                    if(NC_VALVE_DUTY_CONTROL_FRONT==1)
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            if(pwm_i < (uint8_t)(esp_slip_nc_duty_front)) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                            }
                            else {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        /*pNC = LA_u8AllocWheelNCValveDuty((int8_t)esp_slip_nc_duty_front);*/
                    }
                    else
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                            pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                        }
                        /*pNC = 0xFF;*/
                    }
                }
                else if( (pCtrlWL->REAR_WHEEL==1) && ( ((SLIP_CONTROL_NEED_RL==1)&&(pCtrlWL->LEFT_WHEEL==1))
                	|| ((SLIP_CONTROL_NEED_RR==1)&&(pCtrlWL->LEFT_WHEEL==0)) )  )
                {
                    if(NC_VALVE_DUTY_CONTROL_REAR==1)
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            if(pwm_i < (uint8_t)(esp_slip_nc_duty_rear)) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                            }
                            else {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        /*pNC = LA_u8AllocWheelNCValveDuty((int8_t)esp_slip_nc_duty_rear);*/
                    }
                    else
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                            pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                        }
                        /*pNC = 0xFF;*/
                    }
                }
                else
                {
                    if( (pCtrlWL->REAR_WHEEL==0) && (ESP_PULSE_DUMP_FLAG_F)
                     && ( ((SLIP_CONTROL_NEED_FL_old)&&(pCtrlWL->LEFT_WHEEL==1))
                    	  ||((SLIP_CONTROL_NEED_FR_old)&&(pCtrlWL->LEFT_WHEEL==0)) ) )
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            if(pwm_i < (uint8_t)(pulse_dump_nc_duty_front)) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                            }
                            else {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        /*pNC = LA_u8AllocWheelNCValveDuty((int8_t)pulse_dump_nc_duty_front);*/
                    }
                    else if( (pCtrlWL->REAR_WHEEL==1) && (ESP_PULSE_DUMP_FLAG_R)
                     && ( ((SLIP_CONTROL_NEED_RL_old)&&(pCtrlWL->LEFT_WHEEL==1))
                    	  ||((SLIP_CONTROL_NEED_RR_old)&&(pCtrlWL->LEFT_WHEEL==0)) ) )
                    {
                        for(pwm_i=0;pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                            if(pwm_i < (uint8_t)(pulse_dump_nc_duty_rear)) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                            }
                            else {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        /*pNC = LA_u8AllocWheelNCValveDuty((int8_t)pulse_dump_nc_duty_rear);*/
                    }
                    else
                    {
                        if(pCtrlWL->lau8NCOntime >= U8_BASE_LOOPTIME)
                        {
                            for(pwm_i=0;pwm_i<U8_BASE_LOOPTIME;pwm_i++) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                              #if __NC_CURRENT_CONTROL
                                pNC->lau16PwmDuty[pwm_i] = (uint16_t)(((uint32_t)pCtrlWL->lau8NCPwmDuty*NC_FULL_ON_DUTY_16BIT)/NC_FULL_ON_DUTY_8BIT);
                              #else
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                              #endif
                            }
                        }
                        else if(pCtrlWL->lau8NCOntime > 0)
                        {
                            for(pwm_i=0;pwm_i<(pCtrlWL->lau8NCOntime);pwm_i++) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                              #if __NC_CURRENT_CONTROL
                                pNC->lau16PwmDuty[pwm_i] = (uint16_t)(((uint32_t)pCtrlWL->lau8NCPwmDuty*NC_FULL_ON_DUTY_16BIT)/NC_FULL_ON_DUTY_8BIT);
                              #else
                                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                              #endif
                            }
                            for(pwm_i=(pCtrlWL->lau8NCOntime);pwm_i<(U8_BASE_LOOPTIME);pwm_i++) {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        else
                        {
                            for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                            }
                        }
                        /*pNC = 0xFF;*/
                    }                    
                }
              /**********************************************************************/                
              #endif        /* __VDC */
            }
          #if __VDC
            else if(pCtrlWL->pwm_duty_temp_dash == PWM_duty_CIRC)
            {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
                }
                /*pNC = 0xFF;*/
            }
          #endif
            else
            {
                for(pwm_i=0; pwm_i<U8_BASE_LOOPTIME; pwm_i++)  {
                    pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                    pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                }
            }
        }
    }
    
    /*return pNC;*/
}

#else /* (__MGH_80_10MS == DISABLE) */
void LA_vAllocWheelValveDutyArrayWL(const WHEEL_VV_OUTPUT_t *pWL1HP, const WHEEL_VV_OUTPUT_t *pWL2HP, SOL_LOGIC_DUTY_t *pNO, SOL_LOGIC_DUTY_t *pNC)
{
	uint8_t pwm_i;
	uint8_t u8NoOnTime1HP; /* = (pWL1HP->u8InletOnTime>=U8_BASE_CTRLTIME)  ? U8_BASE_CTRLTIME : pWL1HP->u8InletOnTime; */
	uint8_t u8NoOnTime2HP; /* = (pWL2HP->u8InletOnTime>=U8_BASE_CTRLTIME)  ? U8_BASE_CTRLTIME : pWL2HP->u8InletOnTime; */

	uint8_t u8NcOnTime1HP; /* = (pWL1HP->u8OutletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL1HP->u8OutletOnTime;*/
	uint8_t u8NcOnTime2HP; /* = (pWL2HP->u8OutletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL2HP->u8OutletOnTime;*/
		
    uint16_t u16NoPwmDuty16Bit1HP = (uint16_t)(((uint32_t)pWL1HP->u8PwmDuty*NO_FULL_ON_DUTY_16BIT)/PWM_duty_HOLD);
    uint16_t u16NoPwmDuty16Bit2HP = (uint16_t)(((uint32_t)pWL2HP->u8PwmDuty*NO_FULL_ON_DUTY_16BIT)/PWM_duty_HOLD);
    
    if(pWL1HP->u8InletOnTime>0)
    {
    	u8NoOnTime1HP = (pWL1HP->u8InletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL1HP->u8InletOnTime;
    }
    else if(pWL1HP->u8PwmDuty == pwm_duty_null)
    {
    	u8NoOnTime1HP = U8_BASE_CTRLTIME;
    }
    else
    {
    	u8NoOnTime1HP = 0;
    }
    
    if(pWL2HP->u8InletOnTime>0)
    {
    	u8NoOnTime2HP = (pWL2HP->u8InletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL2HP->u8InletOnTime;
    }
    else if(pWL2HP->u8PwmDuty == pwm_duty_null)
    {
    	u8NoOnTime2HP = U8_BASE_CTRLTIME;
    }
    else
    {
    	u8NoOnTime2HP = 0;
    }
    
    if(pWL1HP->u8OutletOnTime>0)
    {
    	u8NcOnTime1HP = (pWL1HP->u8OutletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL1HP->u8OutletOnTime;
    }
    else if(pWL1HP->u8PwmDuty == PWM_duty_DUMP)
    {
    	u8NcOnTime1HP = U8_BASE_CTRLTIME;
    }
    else
    {
    	u8NcOnTime1HP = 0;
    }
    
    if(pWL2HP->u8OutletOnTime>0)
    {
    	u8NcOnTime2HP = (pWL2HP->u8OutletOnTime>=U8_BASE_CTRLTIME) ? U8_BASE_CTRLTIME : pWL2HP->u8OutletOnTime;
    }
    else if(pWL2HP->u8PwmDuty == PWM_duty_DUMP)
    {
    	u8NcOnTime2HP = U8_BASE_CTRLTIME;
    }
    else
    {
    	u8NcOnTime2HP = 0;
    }

	if(u8NoOnTime1HP>0)
	{
        for(pwm_i=0; pwm_i<u8NoOnTime1HP; pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
        
        for(pwm_i=u8NoOnTime1HP; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
	}
	else if(u8NcOnTime1HP>0)
	{
        for(pwm_i=0; pwm_i<u8NcOnTime1HP; pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
        }

        for(pwm_i=u8NcOnTime1HP; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
	}
	else
	{
		if(pWL1HP->u8PwmDuty < PWM_duty_HOLD)
        {
            for(pwm_i=0; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit1HP;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
		else if(pWL1HP->u8PwmDuty == PWM_duty_HOLD)
        {
            for(pwm_i=0; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
      #if __VDC  
		else if(pWL1HP->u8PwmDuty == PWM_duty_CIRC)
        {
            for(pwm_i=0; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
            }
        }
      #endif  
		else 
        {
            for(pwm_i=0; pwm_i<U8_BASE_CTRLTIME; pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
	}
	
	if(u8NoOnTime2HP>0)
	{
        for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(u8NoOnTime2HP+U8_BASE_CTRLTIME); pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
        
        for(pwm_i=(u8NoOnTime2HP+U8_BASE_CTRLTIME); pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
	}
	else if(u8NcOnTime2HP>0)
	{
        for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(u8NcOnTime2HP+U8_BASE_CTRLTIME); pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
        }

        for(pwm_i=(u8NcOnTime2HP+U8_BASE_CTRLTIME); pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
        {
            pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
            pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
        }
	}
	else
	{
		if(pWL2HP->u8PwmDuty < PWM_duty_HOLD)
        {
            for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = u16NoPwmDuty16Bit2HP;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
		else if(pWL2HP->u8PwmDuty == PWM_duty_HOLD)
        {
            for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = NO_FULL_ON_DUTY;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
      #if __VDC  
		else if(pWL2HP->u8PwmDuty == PWM_duty_CIRC)
        {
            for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                pNC->lau16PwmDuty[pwm_i] = NC_FULL_ON_DUTY;
            }
        }
      #endif  
		else 
        {
            for(pwm_i=U8_BASE_CTRLTIME; pwm_i<(U8_BASE_CTRLTIME*2); pwm_i++)  
            {
                pNO->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
                pNC->lau16PwmDuty[pwm_i] = VALVE_OFF_DUTY;
            }
        }
	}
}
#endif /* (__MGH_80_10MS == DISABLE) */


/*******************************************************************************
* FUNCTION NAME:        LA_vAllocCircuitValveDutyArray
* CALLED BY:            LA_vAllocValveDutyArray()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Allocation Duty Array of Circuit TC/ESV Valves
********************************************************************************/
  #if __ECU==ESP_ECU_1
void LA_vAllocCircuitValveDutyArray(void)
{
    uint8_t pwm_i;
    uint8_t lau8TempDutyPTC, lau8TempDutySTC, lau8TempDutyPSV, lau8TempDutySSV;

	#if __VDC
	
#if (__SPLIT_TYPE==0)
  #if __TCMF_CONTROL

    if(TCL_DEMAND_fr||TCL_DEMAND_rl) {lau8TempDutyPTC=MFC_PWM_DUTY_P;}
    else                             {lau8TempDutyPTC=VALVE_OFF_DUTY;}
    if(TCL_DEMAND_fl||TCL_DEMAND_rr) {lau8TempDutySTC=MFC_PWM_DUTY_S;}
    else                             {lau8TempDutySTC=VALVE_OFF_DUTY;}
    if(S_VALVE_RIGHT==1)
    {
      #if __ESV_MIN_DUTY_CYCLE_CTRL
        lau8TempDutyPSV=MFC_PWM_DUTY_ESV_P;
      #else
        lau8TempDutyPSV=MFC_PWM_DUTY_ESV;
	  #endif
        if(lau8TempDutyPSV >= SV_FULL_ON_DUTY_8BIT)   {lau8TempDutyPSV=SV_FULL_ON_DUTY_8BIT;}
    }
    else                             {lau8TempDutyPSV=VALVE_OFF_DUTY;}
    if(S_VALVE_LEFT==1)
    {
	  #if __ESV_MIN_DUTY_CYCLE_CTRL
	    lau8TempDutySSV=MFC_PWM_DUTY_ESV_S;
	  #else
        lau8TempDutySSV=MFC_PWM_DUTY_ESV;
	  #endif
        if(lau8TempDutySSV >= SV_FULL_ON_DUTY_8BIT)   {lau8TempDutySSV=SV_FULL_ON_DUTY_8BIT;}
    }
    else                             {lau8TempDutySSV=VALVE_OFF_DUTY;}

  #else

    if(TCL_DEMAND_fr||TCL_DEMAND_rl) {lau8TempDutyPTC=TC_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutyPTC=VALVE_OFF_DUTY;}
    if(TCL_DEMAND_fl||TCL_DEMAND_rr) {lau8TempDutySTC=TC_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutySTC=VALVE_OFF_DUTY;}
    if(S_VALVE_RIGHT==1)             {lau8TempDutyPSV=SV_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutyPSV=VALVE_OFF_DUTY;}
    if(S_VALVE_LEFT==1)              {lau8TempDutySSV=SV_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutySSV=VALVE_OFF_DUTY;}

  #endif
#else
  #if __TCMF_CONTROL

    if(TCL_DEMAND_PRIMARY==1)        {lau8TempDutyPTC=MFC_PWM_DUTY_P;}
    else                             {lau8TempDutyPTC=VALVE_OFF_DUTY;}
    if(TCL_DEMAND_SECONDARY==1)      {lau8TempDutySTC=MFC_PWM_DUTY_S;}
    else                             {lau8TempDutySTC=VALVE_OFF_DUTY;}
    if(S_VALVE_PRIMARY==1)
    {
      #if __ESV_MIN_DUTY_CYCLE_CTRL
        lau8TempDutyPSV=MFC_PWM_DUTY_ESV_P;
      #else
        lau8TempDutyPSV=MFC_PWM_DUTY_ESV;
	  #endif
        if(lau8TempDutyPSV >= SV_FULL_ON_DUTY_8BIT)   {lau8TempDutyPSV=SV_FULL_ON_DUTY_8BIT;}
    }
    else                             {lau8TempDutyPSV=VALVE_OFF_DUTY;}
    if(S_VALVE_SECONDARY==1)
    {
	  #if __ESV_MIN_DUTY_CYCLE_CTRL
	    lau8TempDutySSV=MFC_PWM_DUTY_ESV_S;
	  #else
        lau8TempDutySSV=MFC_PWM_DUTY_ESV;
	  #endif
        if(lau8TempDutySSV >= SV_FULL_ON_DUTY_8BIT)   {lau8TempDutySSV=SV_FULL_ON_DUTY_8BIT;}
    }
    else                             {lau8TempDutySSV=VALVE_OFF_DUTY;}

  #else

    if(TCL_DEMAND_PRIMARY==1)        {lau8TempDutyPTC=TC_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutyPTC=VALVE_OFF_DUTY;}
    if(TCL_DEMAND_SECONDARY==1)      {lau8TempDutySTC=TC_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutySTC=VALVE_OFF_DUTY;}
    if(S_VALVE_PRIMARY==1)           {lau8TempDutyPSV=SV_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutyPSV=VALVE_OFF_DUTY;}
    if(S_VALVE_SECONDARY==1)         {lau8TempDutySSV=SV_FULL_ON_DUTY_8BIT;}
    else                             {lau8TempDutySSV=VALVE_OFF_DUTY;}

  #endif
#endif

	#elif __HEV
  #if __TCMF_CONTROL
		#if ARB_MFC_DUTY_CONTROL
		LCARB_vAllocValveDutyArray();
		#endif
    if(TCL_DEMAND_fr||TCL_DEMAND_rl) {lau8TempDutyPTC=MFC_PWM_DUTY_P;}
    else                             {lau8TempDutyPTC=VALVE_OFF_DUTY;}
    if(TCL_DEMAND_fl||TCL_DEMAND_rr) {lau8TempDutySTC=MFC_PWM_DUTY_S;}
    else                             {lau8TempDutySTC=VALVE_OFF_DUTY;}
                                     {lau8TempDutyPSV=VALVE_OFF_DUTY;}
                                     {lau8TempDutySSV=VALVE_OFF_DUTY;}
  #endif	
	#endif

    for (pwm_i=0; pwm_i<U8_BASE_LOOPTIME ; pwm_i++)
    {
        /*
        la_PTC.lau16PwmDuty[pwm_i] = lau8TempDutyPTC;
        la_STC.lau16PwmDuty[pwm_i] = lau8TempDutySTC;
        la_PSV.lau16PwmDuty[pwm_i] = lau8TempDutyPSV;
        la_SSV.lau16PwmDuty[pwm_i] = lau8TempDutySSV;
        */
        la_PTC.lau16PwmDuty[pwm_i] = (uint16_t)((uint32_t)(lau8TempDutyPTC*TC_FULL_ON_DUTY_16BIT)/TC_FULL_ON_DUTY_8BIT);
        la_STC.lau16PwmDuty[pwm_i] = (uint16_t)((uint32_t)(lau8TempDutySTC*TC_FULL_ON_DUTY_16BIT)/TC_FULL_ON_DUTY_8BIT);
        la_PSV.lau16PwmDuty[pwm_i] = (uint16_t)((uint32_t)(lau8TempDutyPSV*SV_FULL_ON_DUTY_16BIT)/SV_FULL_ON_DUTY_8BIT);
        la_SSV.lau16PwmDuty[pwm_i] = (uint16_t)((uint32_t)(lau8TempDutySSV*SV_FULL_ON_DUTY_16BIT)/SV_FULL_ON_DUTY_8BIT);

    }

}
  #endif



/*******************************************************************************
* FUNCTION NAME:        LA_vSetNominalWheelValvePwmDuty
* CALLED BY:            
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Nominal Allocation of Wheel Valve Output (duty & on-time)
********************************************************************************/
void  LA_vSetNominalWheelValvePwmDuty(uint8_t u8InletAct, uint8_t u8OutletAct, uint8_t u8InletActMs, uint8_t u8OutletActMs, WHEEL_VV_OUTPUT_t *laWL)
{
	if(u8InletAct==0)
	{
		if(u8OutletAct==0)
		{
			laWL->u8PwmDuty = pwm_duty_null;
			laWL->u8InletOnTime = u8InletActMs; 
			laWL->u8OutletOnTime = 0;
		}
		else
		{
			laWL->u8PwmDuty = PWM_duty_CIRC;
			laWL->u8InletOnTime = 0;
			laWL->u8OutletOnTime = 0;
		}
	}
	else
	{
		if(u8OutletAct==0)
		{
			laWL->u8PwmDuty = PWM_duty_HOLD;
			laWL->u8InletOnTime = 0; 
			laWL->u8OutletOnTime = 0;
		}
		else
		{
			laWL->u8PwmDuty = PWM_duty_DUMP;
			laWL->u8InletOnTime = 0; 
			laWL->u8OutletOnTime = u8OutletActMs;
		}
	}
}

void LA_vResetWheelValvePwmDuty(WHEEL_VV_OUTPUT_t *laWL)
{
	laWL->u8PwmDuty = 0;
	laWL->u8InletOnTime = 0; 
	laWL->u8OutletOnTime = 0;
  #if defined (__ACTUATION_DEBUG)
	laWL->u8debugger = 0;
  #endif /* defined (__ACTUATION_DEBUG) */	
}    

void LA_vSetWheelVvOnOffRiseOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8NoReapplyTime)
{
    if(u8NoReapplyTime > U8_BASE_CTRLTIME)
    {
        laWL1HP->u8PwmDuty = pwm_duty_null;
        laWL2HP->u8PwmDuty = pwm_duty_null;
        laWL1HP->u8InletOnTime = U8_BASE_CTRLTIME;
        laWL2HP->u8InletOnTime = u8NoReapplyTime - U8_BASE_CTRLTIME;
    }
    else
    {
        laWL1HP->u8PwmDuty = pwm_duty_null;
        laWL2HP->u8PwmDuty = PWM_duty_HOLD;
        laWL1HP->u8InletOnTime = u8NoReapplyTime;
        laWL2HP->u8InletOnTime = 0;
    }
    laWL1HP->u8OutletOnTime = 0;
    laWL2HP->u8OutletOnTime = 0;
}    

void LA_vSetWheelVvOnOffDumpOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8NcDumpTime)
{
    if(u8NcDumpTime > U8_BASE_CTRLTIME)
    {
        laWL1HP->u8PwmDuty = PWM_duty_DUMP;
        laWL2HP->u8PwmDuty = PWM_duty_DUMP;
        laWL1HP->u8OutletOnTime = U8_BASE_CTRLTIME;
        laWL2HP->u8OutletOnTime = u8NcDumpTime - U8_BASE_CTRLTIME;
    }
    else
    {
        laWL1HP->u8PwmDuty = PWM_duty_DUMP;
        laWL2HP->u8PwmDuty = PWM_duty_HOLD;
        laWL1HP->u8OutletOnTime = u8NcDumpTime;
        laWL2HP->u8OutletOnTime = 0;
    }
    laWL1HP->u8InletOnTime = 0;
    laWL2HP->u8InletOnTime = 0;
}  

void LA_vSetWheelVvOnOffHoldOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP)
{
    laWL1HP->u8PwmDuty = PWM_duty_HOLD;
    laWL2HP->u8PwmDuty = PWM_duty_HOLD;
    
    if(laWL2HP->u8InletOnTime <= U8_BASE_CTRLTIME)
    {
    	laWL1HP->u8InletOnTime = 0;
    	laWL2HP->u8InletOnTime = 0;
    }
    else if(laWL2HP->u8InletOnTime <= (U8_BASE_CTRLTIME*2))
    {
        laWL1HP->u8InletOnTime = laWL2HP->u8InletOnTime - U8_BASE_CTRLTIME;
        laWL2HP->u8InletOnTime = 0;
    }
    else
    {
        laWL1HP->u8InletOnTime = U8_BASE_CTRLTIME;
        laWL2HP->u8InletOnTime = laWL2HP->u8InletOnTime - (U8_BASE_CTRLTIME*2);
    }

    if(laWL2HP->u8OutletOnTime <= U8_BASE_CTRLTIME)
    {
    	laWL1HP->u8OutletOnTime = 0;
    	laWL2HP->u8OutletOnTime = 0;
    }
    else if(laWL2HP->u8OutletOnTime <= (U8_BASE_CTRLTIME*2))
    {
        laWL1HP->u8OutletOnTime = laWL2HP->u8OutletOnTime - U8_BASE_CTRLTIME;
        laWL2HP->u8OutletOnTime = 0;
    }
    else
    {
        laWL1HP->u8OutletOnTime = U8_BASE_CTRLTIME;
        laWL2HP->u8OutletOnTime = laWL2HP->u8OutletOnTime - (U8_BASE_CTRLTIME*2);
    }
}

void LA_vSetWheelVvHold_Forcing(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP)
{
    laWL1HP->u8PwmDuty = PWM_duty_HOLD;
    laWL2HP->u8PwmDuty = PWM_duty_HOLD;
    
    laWL1HP->u8InletOnTime = 0;
    laWL2HP->u8InletOnTime = 0;
    
    laWL1HP->u8OutletOnTime = 0;
    laWL2HP->u8OutletOnTime = 0;
}

void LA_vSetWheelVvOnOffCircOutput(WHEEL_VV_OUTPUT_t *laWL1HP, WHEEL_VV_OUTPUT_t *laWL2HP)
{
	laWL1HP->u8PwmDuty = PWM_duty_CIRC;
	laWL2HP->u8PwmDuty = PWM_duty_CIRC;
	
	laWL1HP->u8OutletOnTime = U8_BASE_CTRLTIME;
	laWL2HP->u8OutletOnTime = U8_BASE_CTRLTIME;
	
	laWL1HP->u8InletOnTime = U8_BASE_CTRLTIME;
	laWL2HP->u8InletOnTime = U8_BASE_CTRLTIME;
}  

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LACallMain
	#include "Mdyn_autosar.h"               
#endif
