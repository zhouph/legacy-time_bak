#ifndef __LAHDCCALLACTHW_H__
#define __LAHDCCALLACTHW_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition**************************/
  #if __HDC
#define	U8_HDC_HOLD			1
#define	U8_HDC_APPLY		2
#define	U8_HDC_DUMP			3

/* ESV off/on tunning at ABS */
#define	S16_MP_TH_ESV_OFF_AT_ABS		MPRESS_10BAR
#define	S16_MP_OFF_TH_ESV_RE_ON_TIME    L_U8_TIME_10MSLOOP_1500MS
/* ESV off/on tunning at ABS */

/*Global MACRO FUNCTION Definition**************************/

/* Ʃ���Ķ���� */
/*
#define	S16_HDC_TC_CUR_AT_MP10bar		290
#define	S16_HDC_TC_CUR_AT_MP20bar		330
#define	S16_HDC_TC_CUR_AT_MP30bar		360

#define	S16_HDC_RISE_CURRENT_INIT		340
#define U8_HDC_RISE_CURRENT_COMP         5

#define S16_HDC_CONTROL_MIN_CURRENT     250
*/
#define S16_HDC_LIMIT_CUR_ACC_DUMP      300
/*
#define U8_HDC_STALL_DUMP_RATE_10P		 15
#define U8_HDC_STALL_DUMP_RATE_20P		  7
#define U8_HDC_STALL_DUMP_RATE_30P		  4	
*/
/* Ʃ���Ķ���� */

  #endif
/*Global Type Declaration **********************************/
  #if __HDC
typedef struct
{
	uint16_t lau1HdcWL1No :1;
	uint16_t lau1HdcWL1Nc :1;
	uint16_t lau1HdcWL2No :1;
	uint16_t lau1HdcWL2Nc :1;
}HDC_VALVE_t;

   #if (__SPLIT_TYPE==0)
#define	HDC_HV_VL_fr			laHdcValveP.lau1HdcWL1No
#define	HDC_AV_VL_fr			laHdcValveP.lau1HdcWL1Nc
#define	HDC_HV_VL_rl			laHdcValveP.lau1HdcWL2No
#define	HDC_AV_VL_rl			laHdcValveP.lau1HdcWL2Nc

#define	HDC_HV_VL_fl			laHdcValveS.lau1HdcWL1No
#define	HDC_AV_VL_fl			laHdcValveS.lau1HdcWL1Nc
#define	HDC_HV_VL_rr			laHdcValveS.lau1HdcWL2No
#define	HDC_AV_VL_rr			laHdcValveS.lau1HdcWL2Nc
   #else
#define	HDC_HV_VL_fr			laHdcValveP.lau1HdcWL1No
#define	HDC_AV_VL_fr			laHdcValveP.lau1HdcWL1Nc
#define	HDC_HV_VL_fl			laHdcValveP.lau1HdcWL2No
#define	HDC_AV_VL_fl			laHdcValveP.lau1HdcWL2Nc

#define	HDC_HV_VL_rl			laHdcValveS.lau1HdcWL1No
#define	HDC_AV_VL_rl			laHdcValveS.lau1HdcWL1Nc
#define	HDC_HV_VL_rr			laHdcValveS.lau1HdcWL2No
#define	HDC_AV_VL_rr			laHdcValveS.lau1HdcWL2Nc
   #endif
  #endif
/*Global Extern Variable  Declaration***********************/
  #if __HDC
/********** To */
extern uint8_t	lcu8HdcValveMode, lcu8HdcMotorMode;
extern HDC_VALVE_t	laHdcValveP, laHdcValveS;
extern uint8_t lcu8HdcEsvActOffInAbs;
/********** From */
   #if __ADVANCED_MSC
extern U8_BIT_STRUCT_t MSC00,MSC01;
/*extern int16_t		hdc_msc_target_vol,hdc_initial_motor_on;*/
   #endif
  #endif

/*Global Extern Functions  Declaration**********************/
  #if __HDC
extern void LAHDC_vCallActHW(void);
   #if __ADVANCED_MSC
extern void LCMSC_vSetHDCTargetVoltage(void);
   #endif
extern int16_t LAMFC_s16SetMFCCurrentHDC(uint8_t CIRCUIT, HDC_VALVE_t *pHdc, int16_t MFC_Current_old);      
  #endif
#endif