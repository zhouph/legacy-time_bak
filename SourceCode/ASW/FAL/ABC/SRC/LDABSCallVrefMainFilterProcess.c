/*******************************************************************
* Project Name: 
* File Name: 
* Description: 
********************************************************************/
/* Includes	********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallVrefMainFilterProcess
	#include	"Mdyn_autosar.h"
#endif




#include 	"LCESPCalInterpolation.h"
#include 	"LSESPFilterEspSensor.h"
#include 	"LCallMain.h"
#include 	"LCESPCalLpf.h"
#include 	"LCABSDecideCtrlState.h"
#include 	"LDABSDctRoadGradient.h"
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#include 	"LDABSDctWheelStatus.h"
#include 	"LCABSCallControl.h"
#if	__VDC
#include 	"LDABSCallVrefCompForESP.H"
#include 	"LSESPCalSensorOffset.h"
#endif

/*******************************************************************/

/* Local Definition	 ***********************************************/
#define	INSIDE_ABS_UPPER_LIMIT				200
#define	ABS_TO_ACCEL_UPPER_LIMIT			100
#define	INSIDE_ABS_LOWER_LIMIT_LOW_MU		-50
#define	INSIDE_ABS_LOWER_LIMIT_HIGH_MU		-150
#define	CBS_HIGH_MU_LOWER_LIMIT				-125
#define	INSIDE_ABS_FILOT_LOWER_LIMIT		-125
#define	INSIDE_ABS_FILOT_UPPER_LIMT			100
#define	OUTSIDE_ABS_LOWER_LIMIT_HIGH_MU		-150
#define	PARKING_LOWER_LIMIT					-50


#define	MAX_VREF_DEVIAITION		23 //32*5/7
#define	SPIN_CHECK_THR			VREF_1_KPH_RESOL_CHANGE
	
#define	G_NEG_OFFSET_INSIDE_ABS_DEFAULT		-30
#define	G_NEG_OFFSET_INSIDE_ABS_SWB_2		-50
#define	G_NEG_OFFSET_INSIDE_ABS_SWB_1		-40
#define	G_POS_OFFSET_SPIN					20
#define	G_POS_OFFSET_BIG_SPIN				6
#define	G_NEG_OFFSET_ENGINE_BRAKE			-30
#define	G_NEG_OFFSET_PARKING_BRAKE			-30

#define	G_NEG_OFFSET_ESP_CONTROL1			-10
#define	G_NEG_OFFSET_ESP_CONTROL2			-50
#define	G_NEG_OFFSET_ESP_CONTROL3			-50
#define	G_NEG_OFFSET_ESP_CONTROL4			-20
#define	G_NEG_OFFSET_ESP_CONTROL5			-100


#define G_Limit_Gain_1   1
#define G_Limit_Gain_2   1
#define U8_ABS_1ST_NTH_CTA  L_U8_TIME_10MSLOOP_1000MS
/*******************************************************************/

/* Variables Definition*********************************************/

/* LDABS_vInformSelectedWheel */
	int8_t		arad_select_wheel;
/* LDABS_vDetVrefInputLimit	*/	
	int16_t		vref_input_inc_limit;
	int16_t		vref_input_dec_limit;
	
/* LDABS_vDetVrefOutputLimit */	
	int16_t		vref_output_inc_limit;
	int16_t		vref_output_dec_limit;
	
	int16_t		vref_limit_inc_counter;
	int16_t		vref_limit_dec_counter_frt;		   
	int16_t		vref_limit_dec_counter_rr;		  
	uint8_t		Unstabil_counter_fl;   
	uint8_t		Unstabil_counter_fr;	  
	
	#if	__VREF_LIMIT_1
	int16_t		CNT_LM1;
	int16_t		WFactor;
	uint8_t		CNT_L2H;
	int16_t		lds16FiloutDecelG;
	/*uint8_t	vref_test_flags01;*/
	#endif	 
	
	#if	__VREF_LIMIT_2
	int16_t		CNT_LM2;
	int16_t		Estimated_Press_ABS_FL;
	int16_t		Estimated_Press_ABS_FR;
	int16_t		Estimated_Press_ABS_RL;
	int16_t		Estimated_Press_ABS_RR;			
	int16_t		Estimated_Press_ABS_MP;	
	int16_t		lds16VrefPressDecelG;		
	uint16_t	lds16Abs1StVreflimCnt;	
	#endif	
/* LDABS_vDetVrefLimitByLongAcc*/
	#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))&& (__AX_SENSOR==1)
	int16_t		vref_inc_limit_by_g;
	int16_t		vref_dec_limit_by_g;
	int16_t		vref_dec_limit_by_g_old;
	int8_t		g_sensor_limit_flags;
	uint8_t		g_sensor_error_cnt;
	int16_t		G_Limit_offset;
	int16_t		G_Limit_offset_old;	
	uint8_t		all_unstable_cnt;
    uint16_t 	lds16TcsVreflimCnt;
	#endif
	
/* LDABS_vSelVrefWheelOnMGH40Plus */
	int16_t	wheel_speed_max;
	int16_t	wheel_speed_2nd;
	int16_t	wheel_speed_3rd;
	int16_t	wheel_speed_min;	
	int16_t	vrad_diff_max;
	int16_t	vrad_diff_max_resol_change;


/* LDABS_vExecuteVrefInputLimit	*/
/* LDABS_vExecuteVrefOutputLimit */
/* LDABS_vSaveVrefCalcResult */
/* LDABS_vExecuteVrefFiltering */	
/*	uint8_t		abs_begin_timer; */
	int16_t		voptfz_new;
	int16_t		lds16VrefRawSignal; 
  
#if	__VREF_LIMIT_1
	U8_BIT_STRUCT_t VREF_F0;
#endif

#if	__VREF_LIMIT_2
	U8_BIT_STRUCT_t VREF_F1;
#endif

	U8_BIT_STRUCT_t VREF_F2;
	U8_BIT_STRUCT_t VREF_F3;
	uint8_t	wheel_speed_arrange_flg;
	uint8_t	wheel_selection_mode;
	
/*-- Tuning	Parameter Declaration ---*/
   int16_t	  lds16VrefInABSLowLimitDefault;
   int16_t	  lds16VrefOutABSLowLimitDefault;
   int16_t	  lds16VrefOutABSUpLimitDefault;
   int16_t	  lds16VrefLowMuDctByDumpRiseCnt1;
   int16_t	  lds16VrefLowMuDctByDumpRiseCnt2;
   
   uint8_t vrsel_crt_flg;
   int16_t wstr_correction;

   int8_t lds8Outputdeclimit;
   int8_t lds8Outputinclimit;
   int8_t lds8Inputdeclimit;
   int8_t lds8Inputinclimit;
/*******************************************************************/

/* Local Function prototype	****************************************/
void	LDABS_vSelectWheelspeedforVref(void);

	#if	(__4WD || (__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)
void    LDABS_vDetVrefLimitByLongAcc(int16_t inc_limit, int16_t dec_limit,int16_t fzcase); 
	#endif
void	LDABS_vSelVrefWheelOnMGH40Plus(void);


void	LDABS_vDetVrefInputLimit(void);
void	LDABS_vDetVrefOutputLimit(int16_t fzcase);
void    LDABS_vExecuteVrefInputLimit(int16_t inc_limit, int16_t dec_limit,int16_t fzcase);
void	LDABS_vExecuteVrefOutputLimit(int16_t dec_limit,int16_t	inc_limit,int16_t fzcase);
void	LDABS_vSaveVrefCalcResult(void);
void	LDABS_vExecuteVrefFiltering(int16_t	fz_case);
void	LDABS_vCallVrefMainFilterprocess(void);
	   
void	LDABS_vDetVrefLimitFstNorm(void);		
void	LDABS_vDetVrefLimitNthNorm(void);	 
#if	__VREF_LIMIT_1
void	LDABS_vDetVrefLimitNthFilt(int16_t X, int16_t Y); 
#endif

#if	__VREF_LIMIT_2	   
void	LDABS_vDetVrefLimitFstPress(int16_t	X, int16_t Y);		
#endif										   

#if	__VDC
void	LDABS_vArrangeWheelSpeed(int16_t whl_spd_fl,int16_t	whl_spd_fr,int16_t whl_spd_rl,int16_t whl_spd_rr);
void	LDABS_vAllocateWheelSpeed(struct W_STRUCT *WL_1,struct W_STRUCT	*WL_2,struct W_STRUCT *WL_3,struct W_STRUCT	*WL_4);
void	LDABS_vCompRefWheelForESP(void);
void	LDABS_vInformSelectedWheel(uint8_t wheel_selection_mode	);
#else
void	LDABS_vArrangeWheelSpeed2(void);
void	LDABS_vInformSelectedWheel(void);
#endif

void	LDABS_vTuningParaLimitation(void);

/*******************************************************************/

void LDABS_vCallVrefMainFilterprocess(void)
{		
	int16_t	FZ_CASE;
	FZ_CASE=0;
	
	LDABS_vTuningParaLimitation();
	LDABS_vSelectWheelspeedforVref();

	LDABS_vDetVrefInputLimit();
    lds8Inputdeclimit=(int8_t)vref_input_dec_limit;
    lds8Inputinclimit=(int8_t)vref_input_inc_limit;
	FZ = (struct V_STRUCT *) &FZ1;	   
	FZ_CASE=1;
    LDABS_vExecuteVrefInputLimit(vref_input_inc_limit, vref_input_dec_limit,FZ_CASE);   
	LDABS_vExecuteVrefFiltering(FZ_CASE);	
	LDABS_vDetVrefOutputLimit(FZ_CASE);	
	LDABS_vExecuteVrefOutputLimit(vref_output_dec_limit,vref_output_inc_limit,FZ_CASE);
	lds8Outputdeclimit=(int8_t)vref_output_dec_limit;
	lds8Outputinclimit=(int8_t)vref_output_inc_limit;
	LDABS_vSaveVrefCalcResult();
#if	!__SIDE_VREF 
	FZ = (struct V_STRUCT *) &FZ2;		
	FZ_CASE=2;
    LDABS_vExecuteVrefInputLimit(vref_input_inc_limit, vref_input_dec_limit,FZ_CASE);
	LDABS_vExecuteVrefFiltering(FZ_CASE);
	LDABS_vDetVrefOutputLimit(FZ_CASE);	
	LDABS_vExecuteVrefOutputLimit(vref_output_dec_limit,vref_output_inc_limit,FZ_CASE);	
	LDABS_vSaveVrefCalcResult();
#else
	if(voptfz3_resol_change>voptfz4_resol_change){
		FZ2.voptfz_resol_change=voptfz4_resol_change;
		voptfz2=FZ2.voptfz_resol_change/8;
	}
	else{
		FZ2.voptfz_resol_change=voptfz3_resol_change;
		voptfz2=FZ2.voptfz_resol_change/8;		  
	}
#endif	  
	
  
	#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE)) && (__AX_SENSOR==1)
	FZ = (struct V_STRUCT *) &FZ_for_4WD;	
	FZ_CASE=3;
    LDABS_vDetVrefLimitByLongAcc(vref_input_inc_limit, vref_input_dec_limit,FZ_CASE);   
    LDABS_vExecuteVrefInputLimit(vref_inc_limit_by_g, vref_dec_limit_by_g,FZ_CASE);
	LDABS_vExecuteVrefFiltering(FZ_CASE);
	LDABS_vDetVrefOutputLimit(FZ_CASE);	
	LDABS_vExecuteVrefOutputLimit(vref_output_dec_limit,vref_output_inc_limit,FZ_CASE);			
	LDABS_vSaveVrefCalcResult();
	#endif /*__4WD && __MGH_40_VREF	*/	  
}
/*******************************************************************/

void LDABS_vTuningParaLimitation(void)
{
	lds16VrefInABSLowLimitDefault  = (int16_t)LCABS_s16LimitMinMax(S16_VREF_LOWER_LIMIT,-150,-100);	 
	lds16VrefOutABSLowLimitDefault = (int16_t)LCABS_s16LimitMinMax(S16_VREF_LOWER_LIMIT,-150,-100);	 
	lds16VrefOutABSUpLimitDefault  = (int16_t)LCABS_s16LimitMinMax(S16_VREF_CBS_UPPER_LIMIT,50,100); 
	lds16VrefLowMuDctByDumpRiseCnt1= (int16_t)LCABS_s16LimitMinMax(S16_VREF_LOW_MU_DUMP_1,100,300);	 
	lds16VrefLowMuDctByDumpRiseCnt2= (int16_t)LCABS_s16LimitMinMax(S16_VREF_LOW_MU_DUMP_2,30,300); 
	
/*
	lds16VrefInABSLowLimitDefault  = S16_VREF_LOWER_LIMIT;
	lds16VrefOutABSLowLimitDefault = S16_VREF_LOWER_LIMIT;
	lds16VrefOutABSUpLimitDefault  = S16_VREF_CBS_UPPER_LIMIT;
	lds16VrefLowMuDctByDumpRiseCnt1= S16_VREF_LOW_MU_DUMP_1;
	lds16VrefLowMuDctByDumpRiseCnt2= S16_VREF_LOW_MU_DUMP_2;	
	
	if(lds16VrefInABSLowLimitDefault <=	-150)
	{
		lds16VrefInABSLowLimitDefault =	-150 ;
	}
	else if(lds16VrefInABSLowLimitDefault>=	-100 )
	{
		lds16VrefInABSLowLimitDefault =-100;
	}
	else
	{
		;
	}
	
	if(lds16VrefOutABSLowLimitDefault <= -150)
	{
		lds16VrefOutABSLowLimitDefault = -150 ;
	}
	else if(lds16VrefOutABSLowLimitDefault>= -100 )
	{
		lds16VrefOutABSLowLimitDefault =-100;
	}
	else
	{
		;
	}	
	
	if(lds16VrefOutABSUpLimitDefault <=	50)
	{
		lds16VrefOutABSUpLimitDefault =	50 ;
	}
	else if(lds16VrefOutABSUpLimitDefault>=	100	)
	{
		lds16VrefOutABSUpLimitDefault =100;
	}
	else
	{
		;
	}		
	
	if(lds16VrefLowMuDctByDumpRiseCnt1 <= 100)
	{
		lds16VrefLowMuDctByDumpRiseCnt1	= 100 ;
	}
	else if(lds16VrefLowMuDctByDumpRiseCnt1>= 300 )
	{
		lds16VrefLowMuDctByDumpRiseCnt1	=300;
	}
	else
	{
		;
	}			
	
	if(lds16VrefLowMuDctByDumpRiseCnt2 <= 30)
	{
		lds16VrefLowMuDctByDumpRiseCnt2	= 30 ;
	}
	else if(lds16VrefLowMuDctByDumpRiseCnt2>= 300 )
	{
		lds16VrefLowMuDctByDumpRiseCnt2	=300;
	}
	else
	{
		;
	}	
	*/		
	
}

void LDABS_vSelectWheelspeedforVref(void)
{
   /* Move to LDABSCallDctForVref.c	 
   #if __VDC
	 #if __MY_09_VREF
	 LDABS_vCompRefWheelForESP();	
	LDABS_vArrangeWheelSpeed(FL.vrad_crt2,FR.vrad_crt2,RL.vrad_crt2,RR.vrad_crt2);
	 #else

	LDABS_vArrangeWheelSpeed(FL.vrad_crt_resol_change,FR.vrad_crt_resol_change,RL.vrad_crt_resol_change,RR.vrad_crt_resol_change);

	 #endif
   #else

	LDABS_vArrangeWheelSpeed2();
   #endif
*/	 

	   
	LDABS_vSelVrefWheelOnMGH40Plus();
	
	#if	__VDC
	LDABS_vInformSelectedWheel(wheel_selection_mode	);
	
	#if	(!__MY_09_VREF)	||(!__VRSECELT_PRE_YAW_CORRECTION)
	LDABS_vCompRefWheelForESP();
	#endif
	#else
	LDABS_vInformSelectedWheel();
	#endif
	
	#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)
	FZ_for_4WD.vrselect_resol_change=FZ1.vrselect_resol_change;
	FZ_for_4WD.vrselect=FZ1.vrselect;
	#endif		

}
/*==================================================================*/

#if	__MGH60_NEW_SELECTION 

void	LDABS_vSelVrefWheelOnMGH40Plus(void)
{
	
/*----------------------------------------------
Ref	Wheel Selection	Algorithm
------------------------------------------------
1. BRAKE or	ABS	
	- Max wheel	seletion, Max wheel	is reliable?
2. Driving State
-----------------------------------------------*/
	int16_t	selected_whl_speed_1;
	int16_t	selected_whl_speed_2;
	int16_t	vehicle_speed;
	int16_t	THR=64;
	int16_t	non_driven_max;
	int16_t	non_driven_min;
	uint8_t	NON_DRVEN_WHEEL_MAX_SELECTED;
	uint8_t	NON_DRVEN_WHEEL_MAX_REL;
	uint8_t	NON_DRVEN_WHEEL_MIN_REL;
	uint8_t	sel_case;
	int8_t lds8NonDrivenMaxWheelArad;
	int16_t	driven_max;
	int16_t	driven_min;
	
#if	__MY_09_VREF &&	__VDC && __VRSECELT_PRE_YAW_CORRECTION
	vehicle_speed=vref_resol_change;

	#if	__REAR_D
	if(FL.vrad_crt2>FR.vrad_crt2)
	{
		non_driven_max=FL.vrad_crt2;
		non_driven_min=FR.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=FL.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=FR.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=FR.vrad_crt2;
		non_driven_min=FL.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=FR.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=FL.RELIABLE_FOR_VREF;		
	}
	
	if(RL.vrad_crt2>RR.vrad_crt2)
	{
		driven_max=RL.vrad_crt2;
		driven_min=RR.vrad_crt2;
	}
	else
	{
		driven_max=RR.vrad_crt2;
		driven_min=RL.vrad_crt2;	
	}	
	#else
	if(RL.vrad_crt2>RR.vrad_crt2)
	{
		non_driven_max=RL.vrad_crt2;
		non_driven_min=RR.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=RL.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=RR.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=RR.vrad_crt2;
		non_driven_min=RL.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=RR.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=RL.RELIABLE_FOR_VREF;		
	}	
	
	if(FL.vrad_crt2>FR.vrad_crt2)
	{
		driven_max=FL.vrad_crt2;
		driven_min=FR.vrad_crt2;
	}
	else
	{
		driven_max=FR.vrad_crt2;
		driven_min=FL.vrad_crt2;	
	}	
	#endif

#else

	vehicle_speed=vref_resol_change;
	#if	__REAR_D
	if(FL.vrad_crt_resol_change>FR.vrad_crt_resol_change)
	{
		non_driven_max=FL.vrad_crt_resol_change;
		non_driven_min=FR.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=FL.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=FR.RELIABLE_FOR_VREF;
		lds8NonDrivenMaxWheelArad=arad_fl;
	}
	else
	{
		non_driven_max=FR.vrad_crt_resol_change;
		non_driven_min=FL.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=FR.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=FL.RELIABLE_FOR_VREF;	
		lds8NonDrivenMaxWheelArad=arad_fr;	
	}
	
	if(RL.vrad_crt_resol_change>RR.vrad_crt_resol_change)
	{
		driven_max=RL.vrad_crt_resol_change;
		driven_min=RR.vrad_crt_resol_change;
	}
	else
	{
		driven_max=RR.vrad_crt_resol_change;
		driven_min=RL.vrad_crt_resol_change;	
	}	
	#else
	if(RL.vrad_crt_resol_change>RR.vrad_crt_resol_change)
	{
		non_driven_max=RL.vrad_crt_resol_change;
		non_driven_min=RR.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=RL.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=RR.RELIABLE_FOR_VREF;
		lds8NonDrivenMaxWheelArad=arad_rl;
	}
	else
	{
		non_driven_max=RR.vrad_crt_resol_change;
		non_driven_min=RL.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=RR.RELIABLE_FOR_VREF;
		NON_DRVEN_WHEEL_MIN_REL=RL.RELIABLE_FOR_VREF;		
		lds8NonDrivenMaxWheelArad=arad_rr;
	}	
	
	if(FL.vrad_crt_resol_change>FR.vrad_crt_resol_change)
	{
		driven_max=FL.vrad_crt_resol_change;
		driven_min=FR.vrad_crt_resol_change;
	}
	else
	{
		driven_max=FR.vrad_crt_resol_change;
		driven_min=FL.vrad_crt_resol_change;	
	}	
	#endif
#endif

	wheel_selection_mode=0;
	sel_case=0;
			
/*			
	if((BRAKE_SIGNAL==1)||(ABS_fz==1))
	if((BRAKING_STATE==1)||((ACTIVE_BRAKE_ACT==1)&&(ABS_fz==1)))
*/
	if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
	{
		if(RELIABILITY_MAX_WL==1)
		{
			sel_case=1;
			wheel_selection_mode=11;
		}
		else if(RELIABILITY_2ND_WL==1)
		{
			sel_case=2;
			wheel_selection_mode=21;
		}
		else if(RELIABILITY_3RD_WL==1)
		{
			sel_case=3;
			wheel_selection_mode=31;						
		}
		else if(RELIABILITY_MIN_WL==1)
		{
			sel_case=4;
			wheel_selection_mode=41;
		}	
		else
		{
			if(wheel_speed_max<vehicle_speed)
			{
				sel_case=1;
				wheel_selection_mode=12;
			}
			else if(wheel_speed_2nd<vehicle_speed)
			{
				sel_case=2;
				wheel_selection_mode=22;
			}
			else if(wheel_speed_3rd<vehicle_speed)
			{
				sel_case=3;
				wheel_selection_mode=32;
			}
			else if(wheel_speed_min<vehicle_speed)
			{
				sel_case=4;
				wheel_selection_mode=42;
			}
			else
			{
				sel_case=4;
				wheel_selection_mode=43;				
			}			
		}
		/*
		#if	__ACCEL_ON_BRK_VREF
		if(ldvrefu1DctVehAccelSpinOnBrk==1)
		{
			sel_case=4;
			wheel_selection_mode=140;			
		}
		else{}
		#endif			
		*/					
	}
	else
	{
		#if	(__4WD==ENABLE)	|| (__4WD_VARIANT_CODE==ENABLE)
			#if    __SPIN_DET_IMPROVE  == ENABLE
		if((ACCEL_SPIN_FZ==1)||(ldu1SlowestWheelSelectFlag==1)||(ldu8vehSpinCnt > 1))
			#else
		if((ACCEL_SPIN_FZ==1)||(ldu1SlowestWheelSelectFlag==1))
			#endif
		{
			if(RELIABILITY_MIN_WL==1)
			{
				if(((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
				&&((vehicle_speed-wheel_speed_min)>SPIN_CHECK_THR))
				{
					sel_case=3;	
					wheel_selection_mode=33;	
				}
				else
				{
					sel_case=4;
					wheel_selection_mode=44;					
				}			
			}
			else if(RELIABILITY_3RD_WL==1)
			{
				sel_case=3;
				wheel_selection_mode=34;					
			}
			else if(RELIABILITY_2ND_WL==1)
			{
				sel_case=2;
				wheel_selection_mode=23;
			}
/*			  
			else if(RELIABILITY_MAX_WL==1)
			{
				sel_case=1;
				wheel_selection_mode=13;
			}
*/
			else
			{
				if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
				{
					if(wheel_speed_min>vehicle_speed)
					{
						sel_case=4;
						wheel_selection_mode=45;							
					}
					else
					{
						sel_case=3;	
						wheel_selection_mode=35;	
					}					
				}					
				#if	__VDC
				else if(VDC_REF_WORK==1)
				{
                	tempF0=0;
                	if((ESP_BRAKE_CONTROL_FL==1)&&(FL.vrad_crt_resol_change<=wheel_speed_min)){
                		tempF0=1;
                	}
               		if((ESP_BRAKE_CONTROL_FR==1)&&(FR.vrad_crt_resol_change<=wheel_speed_min)){
                		tempF0=1;
                	}
               		if((ESP_BRAKE_CONTROL_RL==1)&&(RL.vrad_crt_resol_change<=wheel_speed_min)){
                		tempF0=1;
                	}
               		if((ESP_BRAKE_CONTROL_RR==1)&&(RR.vrad_crt_resol_change<=wheel_speed_min)){
                		tempF0=1;	
					}            
					if((tempF0==1)||((mtp<10)&&(eng_torq_rel<20))){    	
						if(wheel_speed_2nd<vehicle_speed)
						{
							sel_case=2;
							wheel_selection_mode=24;	
						}
						else
						{
							sel_case=3;
							wheel_selection_mode=36;							
						}								
					}
                	else{
                     	sel_case=4;
                    	wheel_selection_mode=46;                 		
                	}
                		                    
                }
				#endif
				
/* --------------------------
Accel Spin+Parking State 
PPR	2009-014 
------------------------------*/
				else if((RL.PARKING_BRAKE_WHEEL==1)||(RR.PARKING_BRAKE_WHEEL==1))
				{
					if(wheel_speed_2nd<vehicle_speed)
					{
						sel_case=2;
						wheel_selection_mode=24;	
					}
					else
					{
						sel_case=3;
						wheel_selection_mode=36;							
					}
				}				
				else
				{
					sel_case=4;
					wheel_selection_mode=46;											
				}
			}
		}
		
		else 
		{
		#endif
		
			NON_DRVEN_WHEEL_MAX_SELECTED=0;
			if(NON_DRVEN_WHEEL_MAX_REL==1)
			{
				if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=0;
				}
				#if	__VDC && !__REAR_D
				else if((wheel_speed_max==RL.vrad_crt_resol_change)&&(alat>LAT_0G7G)&&(fu1AySusDet==0)&&(fu1AyErrorDet==0))
				{
					sel_case=2;
					wheel_selection_mode=25;	
					NON_DRVEN_WHEEL_MAX_SELECTED=1;					
				}
				else if((wheel_speed_max==RR.vrad_crt_resol_change)&&(alat<(-LAT_0G7G))&&(fu1AySusDet==0)&&(fu1AyErrorDet==0))
				{	
					sel_case=2;
					wheel_selection_mode=26;	
					NON_DRVEN_WHEEL_MAX_SELECTED=1;						
				}
				#endif



				#if	 (__4WD_VARIANT_CODE==ENABLE)
				else if((non_driven_max>=(wheel_speed_2nd+VREF_2_KPH_RESOL_CHANGE))&&(lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)
					&&(non_driven_max>=(vehicle_speed+VREF_1_KPH_RESOL_CHANGE))&&(lds8NonDrivenMaxWheelArad>0))
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=0;		
				}
					#if	__ENGINE_INFORMATION
				else if((((mtp>5)||(eng_torq_rel>TORQ_PERCENT_20PRO))&&(ldabsu1CanErrFlg==0))&&(lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)&&(non_driven_max>wheel_speed_2nd))
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=0;
				}				
					#endif				
				#elif __4WD
				else if((non_driven_max>=(wheel_speed_2nd+VREF_2_KPH_RESOL_CHANGE))
					&&(non_driven_max>=(vehicle_speed+VREF_1_KPH_RESOL_CHANGE))&&(lds8NonDrivenMaxWheelArad>0))
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=0;		
				}
					#if	__ENGINE_INFORMATION
				else if(((mtp>5)||(eng_torq_rel>TORQ_PERCENT_20PRO))&&(ldabsu1CanErrFlg==0)&&(non_driven_max>wheel_speed_2nd))
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=0;
				}				
					#endif
				#endif
				else if((non_driven_max>=(vehicle_speed+VREF_3_KPH_RESOL_CHANGE))&&(non_driven_max>=(wheel_speed_2nd+VREF_3_KPH_RESOL_CHANGE))&&(lds8NonDrivenMaxWheelArad>0))
				{
					sel_case=2;
					wheel_selection_mode=27;	
					NON_DRVEN_WHEEL_MAX_SELECTED=1;												
				}				 
				else
				{
					NON_DRVEN_WHEEL_MAX_SELECTED=1;
					wheel_selection_mode=54;
					sel_case=6;						
				}				
			}
			else
			{
				;
			}			

			if(NON_DRVEN_WHEEL_MAX_SELECTED==0)
			{
				if(((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))&&(NON_DRVEN_WHEEL_MAX_REL==1))
				{
					if(non_driven_max>wheel_speed_2nd)
					{
						sel_case=2;
						wheel_selection_mode=28;
					}
					else
					{
						NON_DRVEN_WHEEL_MAX_SELECTED=1;
						wheel_selection_mode=55;
						sel_case=6;	
					}
				}
				#if	__PARKING_BRAKE_OPERATION
				else if(PARKING_BRAKE_OPERATION==1)
				{
					#if	__ENGINE_INFORMATION
					if((mtp<5)&&(eng_torq_rel<TORQ_PERCENT_20PRO)&&(ldabsu1CanErrFlg==0))
					{
		
						sel_case=1;
						wheel_selection_mode=14;						
					}
					else
					{	
					#endif	
						if(wheel_speed_max<vehicle_speed)
						{
							sel_case=1;
							wheel_selection_mode=15;									
						}
						else 
						{
							sel_case=2;
							wheel_selection_mode=29;										
						}
					#if	__ENGINE_INFORMATION	
					}							
					#endif						
				}
				#endif
				
				#if	(__4WD_VARIANT_CODE==ENABLE)
				else if(((mtp>20)||(eng_torq_rel>TORQ_PERCENT_30PRO))&&(ldabsu1CanErrFlg==0)&&(wheel_speed_2nd>(vehicle_speed+VREF_1_KPH_RESOL_CHANGE))&&(lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)){
					#if	__VDC
					if(VDC_REF_WORK==0){
					#endif
						if(wheel_speed_3rd>(vehicle_speed+VREF_1_KPH_RESOL_CHANGE)){
							sel_case=4;	
							wheel_selection_mode=49;							
						}
						else{
							sel_case=3;	
							wheel_selection_mode=32;
						}
					#if	__VDC
					}
					else{
						sel_case=3;	
						wheel_selection_mode=32;
					}	  
					#endif
				}
				#elif __4WD
				else if(((mtp>20)||(eng_torq_rel>TORQ_PERCENT_30PRO))&&(ldabsu1CanErrFlg==0)&&(wheel_speed_2nd>(vehicle_speed+VREF_1_KPH_RESOL_CHANGE))){
					
					#if	__VDC
					if(VDC_REF_WORK==0){
					#endif	  
						if(wheel_speed_3rd>(vehicle_speed+VREF_1_KPH_RESOL_CHANGE)){
							sel_case=4;	
							wheel_selection_mode=49;							
						}
						else{
							sel_case=3;	
							wheel_selection_mode=32;
						}
					#if	__VDC
					}
					else{
						sel_case=3;	
						wheel_selection_mode=32;
					}		 
					#endif			
				}
				   
				#endif
				else if((non_driven_min>wheel_speed_min)&&(NON_DRVEN_WHEEL_MIN_REL==1))
				{
					if(non_driven_min==wheel_speed_2nd)
					{
						sel_case=2;
						wheel_selection_mode=20;								
					}
					else 
					{
						sel_case=3;
						wheel_selection_mode=37;						
					}
				}
				else if(RELIABILITY_2ND_WL==1)
				{
					sel_case=2;
					wheel_selection_mode=20;						
				}
				else if(RELIABILITY_3RD_WL==1)
				{
					sel_case=3;
					wheel_selection_mode=38;
				}		
				else if(RELIABILITY_MIN_WL==1)
				{
					sel_case=4;
					wheel_selection_mode=47;
				}	
				else if(RELIABILITY_MAX_WL==1)
				{
					sel_case=1;
					wheel_selection_mode=14;
				}
				else 
				{
					if(wheel_speed_min>vehicle_speed)
					{
						sel_case=4;
						wheel_selection_mode=48;								
					}						
					else if	(wheel_speed_max<vehicle_speed)	
					{
						sel_case=1;
						wheel_selection_mode=16;								
					}
					else
					{
						sel_case=3;
						wheel_selection_mode=39;	
					}													
				}
									   
			}
			else{}

		#if	__4WD || __4WD_VARIANT_CODE==ENABLE
		}
		#endif
	}
	if(sel_case==1)
	{
		selected_whl_speed_1=wheel_speed_max;
		selected_whl_speed_2=wheel_speed_2nd;		
	}
	else if(sel_case==2)
	{
		selected_whl_speed_1=wheel_speed_2nd;
		selected_whl_speed_2=wheel_speed_3rd;			
	}
	else if(sel_case==3)
	{
		selected_whl_speed_1=wheel_speed_3rd;
		selected_whl_speed_2=wheel_speed_min;			
	}
	else if(sel_case==4)
	{
		selected_whl_speed_1=wheel_speed_min;
		selected_whl_speed_2=wheel_speed_min;	
	}
	else if((NON_DRVEN_WHEEL_MAX_SELECTED==1)&&(sel_case==6))
	{
		selected_whl_speed_1=non_driven_max;
		selected_whl_speed_2=non_driven_min;	
	}
	else
	{
		;
	}
	
	FZ1.vrselect_resol_change=selected_whl_speed_1;
	FZ2.vrselect_resol_change=selected_whl_speed_2;
	FZ1.vrselect=(FZ1.vrselect_resol_change>>3);
	FZ2.vrselect=(FZ2.vrselect_resol_change>>3);
}

#else /* !if__MGH60_NEW_SELECTION */
	
void	LDABS_vSelVrefWheelOnMGH40Plus(void)
{
/*2006.11.10 New concept applied*/

/*----------------------------------------------
Ref	Wheel Selection	Algorithm
------------------------------------------------
1. BRAKE or	ABS	
	- Max wheel	seletion, Max wheel	is reliable?
2. Driving State
-----------------------------------------------*/
	int16_t	selected_whl_speed_1;
	int16_t	selected_whl_speed_2;
	int16_t	vehicle_speed;
	int16_t	THR=64;
	int16_t	non_driven_max;
	int16_t	non_driven_min;
	uint8_t	NON_DRVEN_WHEEL_MAX_SELECTED;
	uint8_t	NON_DRVEN_WHEEL_MAX_REL;
	uint8_t	sel_case;
	
	int16_t	driven_max;
	int16_t	driven_min;
	
#if	__MY_09_VREF &&	__VDC && __VRSECELT_PRE_YAW_CORRECTION

	vehicle_speed=vref_resol_change;


	#if	__REAR_D
	if(FL.vrad_crt2>FR.vrad_crt2)
	{
		non_driven_max=FL.vrad_crt2;
		non_driven_min=FR.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=FL.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=FR.vrad_crt2;
		non_driven_min=FL.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=FR.RELIABLE_FOR_VREF;		
	}
	
	if(RL.vrad_crt2>RR.vrad_crt2)
	{
		driven_max=RL.vrad_crt2;
		driven_min=RR.vrad_crt2;
	}
	else
	{
		driven_max=RR.vrad_crt2;
		driven_min=RL.vrad_crt2;	
	}	
	#else
	if(RL.vrad_crt2>RR.vrad_crt2)
	{
		non_driven_max=RL.vrad_crt2;
		non_driven_min=RR.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=RL.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=RR.vrad_crt2;
		non_driven_min=RL.vrad_crt2;
		NON_DRVEN_WHEEL_MAX_REL=RR.RELIABLE_FOR_VREF;		
	}	
	
	if(FL.vrad_crt2>FR.vrad_crt2)
	{
		driven_max=FL.vrad_crt2;
		driven_min=FR.vrad_crt2;
	}
	else
	{
		driven_max=FR.vrad_crt2;
		driven_min=FL.vrad_crt2;	
	}	
	#endif

#else

	vehicle_speed=vref_resol_change;
	#if	__REAR_D
	if(FL.vrad_crt_resol_change>FR.vrad_crt_resol_change)
	{
		non_driven_max=FL.vrad_crt_resol_change;
		non_driven_min=FR.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=FL.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=FR.vrad_crt_resol_change;
		non_driven_min=FL.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=FR.RELIABLE_FOR_VREF;		
	}
	
	if(RL.vrad_crt_resol_change>RR.vrad_crt_resol_change)
	{
		driven_max=RL.vrad_crt_resol_change;
		driven_min=RR.vrad_crt_resol_change;
	}
	else
	{
		driven_max=RR.vrad_crt_resol_change;
		driven_min=RL.vrad_crt_resol_change;	
	}	
	#else
	if(RL.vrad_crt_resol_change>RR.vrad_crt_resol_change)
	{
		non_driven_max=RL.vrad_crt_resol_change;
		non_driven_min=RR.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=RL.RELIABLE_FOR_VREF;
	}
	else
	{
		non_driven_max=RR.vrad_crt_resol_change;
		non_driven_min=RL.vrad_crt_resol_change;
		NON_DRVEN_WHEEL_MAX_REL=RR.RELIABLE_FOR_VREF;		
	}	
	
	if(FL.vrad_crt_resol_change>FR.vrad_crt_resol_change)
	{
		driven_max=FL.vrad_crt_resol_change;
		driven_min=FR.vrad_crt_resol_change;
	}
	else
	{
		driven_max=FR.vrad_crt_resol_change;
		driven_min=FL.vrad_crt_resol_change;	
	}	
	#endif

#endif

	wheel_selection_mode=0;
	sel_case=0;
			
/*			
	if((BRAKE_SIGNAL==1)||(ABS_fz==1))
	if((BRAKING_STATE==1)||((ACTIVE_BRAKE_ACT==1)&&(ABS_fz==1)))
*/
	if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
	{
		if(RELIABILITY_MAX_WL==1)
		{
			sel_case=1;
			wheel_selection_mode=11;
		}
		else if(RELIABILITY_2ND_WL==1)
		{
			sel_case=2;
			wheel_selection_mode=21;
		}
		else if(RELIABILITY_3RD_WL==1)
		{
			sel_case=3;
			wheel_selection_mode=31;						
		}
		else if(RELIABILITY_MIN_WL==1)
		{
			sel_case=4;
			wheel_selection_mode=49;
		}	
		else
		{
			if(wheel_speed_max<vehicle_speed)
			{
				sel_case=1;
				wheel_selection_mode=12;
			}
			else if(wheel_speed_2nd<vehicle_speed)
			{
				sel_case=2;
				wheel_selection_mode=22;
			}
			else if(wheel_speed_3rd<vehicle_speed)
			{
				sel_case=3;
				wheel_selection_mode=32;
			}
			else if(wheel_speed_min<vehicle_speed)
			{
				sel_case=4;
				wheel_selection_mode=42;
			}
			else
			{
				sel_case=4;
				wheel_selection_mode=50;				
			}			
		}
/*		
		#if	__ACCEL_ON_BRK_VREF
		if(ldvrefu1DctVehAccelSpinOnBrk==1)
		{
			sel_case=4;
			wheel_selection_mode=140;			
		}
		#endif	
*/											
	}
	else
	{
		#if	__4WD ||  (__4WD_VARIANT_CODE==ENABLE)
		if(ACCEL_SPIN_FZ==1)
		{
			if(RELIABILITY_MIN_WL==1)
			{
				if(((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
				&&((vehicle_speed-wheel_speed_min)>SPIN_CHECK_THR))
				{
					sel_case=3;	
					wheel_selection_mode=33;	
				}
				else
				{
					sel_case=4;
					wheel_selection_mode=43;					
				}			
			}
			else if(RELIABILITY_3RD_WL==1)
			{
				sel_case=3;
				wheel_selection_mode=34;					
			}
			else if(RELIABILITY_2ND_WL==1)
			{
				sel_case=2;
				wheel_selection_mode=23;
			}
			else if(RELIABILITY_MAX_WL==1)
			{
				sel_case=1;
				wheel_selection_mode=13;
			}
			else
			{
				if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
				{
					if(wheel_speed_min>vehicle_speed)
					{
						sel_case=4;
						wheel_selection_mode=44;							
					}
					else
					{
						sel_case=3;	
						wheel_selection_mode=35;	
					}					
				}					
				#if	__VDC
				else if(VDC_REF_WORK==1)
				{
					if(wheel_speed_2nd<vehicle_speed)
					{
						sel_case=2;
						wheel_selection_mode=24;	
					}
					else
					{
						sel_case=3;
						wheel_selection_mode=36;							
					}								
				}
				#endif
				else
				{
					sel_case=4;
					wheel_selection_mode=45;											
				}
			}
		}
		else 
		{
		#endif
			#if	__VDC
			if(VDC_REF_WORK==1)
			{
				NON_DRVEN_WHEEL_MAX_SELECTED=0;

					#if	(__4WD_VARIANT_CODE==ENABLE)
				if ((lsu8DrvMode ==DM_2WD)||(lsu8DrvMode ==DM_2H))
				{	
					if(NON_DRVEN_WHEEL_MAX_REL==1)
					{
						NON_DRVEN_WHEEL_MAX_SELECTED=1;
						wheel_selection_mode=53;
						sel_case=6;								
					}								
				}
				else
				{
					if(((mtp<5)&&(eng_torq_rel<TORQ_PERCENT_10PRO))||(non_driven_max<wheel_speed_2nd))
					{
						if(NON_DRVEN_WHEEL_MAX_REL==1)
						{
							NON_DRVEN_WHEEL_MAX_SELECTED=1;
							wheel_selection_mode=53;
							sel_case=6;								
						}
					}					
				}
					#else
				#if	__4WD
				if(((mtp<5)&&(eng_torq_rel<TORQ_PERCENT_10PRO))||(non_driven_max<wheel_speed_2nd))
				{
					if(NON_DRVEN_WHEEL_MAX_REL==1)
					{
						NON_DRVEN_WHEEL_MAX_SELECTED=1;
						wheel_selection_mode=53;
						sel_case=6;								
					}
				}				
				#else
					if(NON_DRVEN_WHEEL_MAX_REL==1)
					{
						NON_DRVEN_WHEEL_MAX_SELECTED=1;
						wheel_selection_mode=53;
						sel_case=6;								
					}				
				#endif
					#endif			
				if(NON_DRVEN_WHEEL_MAX_SELECTED==0)
				{
					if(RELIABILITY_2ND_WL==1)
					{
						sel_case=2;
						wheel_selection_mode=25;						
					}
					else if(RELIABILITY_3RD_WL==1)
					{
						sel_case=3;
						wheel_selection_mode=37;
					}		
					else if(RELIABILITY_MIN_WL==1)
					{
						sel_case=4;
						wheel_selection_mode=46;
					}	
					else if(RELIABILITY_MAX_WL==1)
					{
						sel_case=1;
						wheel_selection_mode=15;
					}
					else 
					{
						if(wheel_speed_min>vehicle_speed)
						{
							sel_case=4;
							wheel_selection_mode=40;								
						}						
						else if	(wheel_speed_max<vehicle_speed)	
						{
							sel_case=1;
							wheel_selection_mode=18;								
						}
						else
						{
							#if	__PARKING_BRAKE_OPERATION
							if(PARKING_BRAKE_OPERATION==1)
							{
								#if	__ENGINE_INFORMATION
								if((mtp<5)&&(eng_torq_rel<200))
								{
								#endif
								sel_case=1;
								wheel_selection_mode=19;	
								#if	__ENGINE_INFORMATION
								}
								else if(wheel_speed_max<vehicle_speed)
								{
									sel_case=1;
									wheel_selection_mode=18;									
								}
								else 
								{
									sel_case=2;
									wheel_selection_mode=20;										
								}							
								#endif						
							}
							else
							{
							#endif	
							sel_case=3;
							wheel_selection_mode=38;	
							#if	__PARKING_BRAKE_OPERATION
							}					
							#endif
						}													
					}						
				}
				/*
				if((((wheel_selection_mode>=20)&&(wheel_selection_mode<=29))||(wheel_selection_mode==52))
				&&(RELIABILITY_2ND_WL==1))
				{
					sel_case=2;
					wheel_selection_mode=25;
				}
				else if(RELIABILITY_3RD_WL==1)
				{
					sel_case=3;
					wheel_selection_mode=37;
				}
				else if(RELIABILITY_2ND_WL==1)
				{
					sel_case=2;
					wheel_selection_mode=26;
				}
				else if((vref>wheel_speed_max)&&(RELIABILITY_MAX_WL==1))
				{
					sel_case=1;
					wheel_selection_mode=14;
				}
				else if(RELIABILITY_MIN_WL==1)
				{
					sel_case=4;
					wheel_selection_mode=46;
				}
				else if(RELIABILITY_MAX_WL==1)
				{
					sel_case=1;
					wheel_selection_mode=15;
				}
				else 
				{
					sel_case=3;
					wheel_selection_mode=38;
				}
				*/
			}
			else
			{
			#endif
				NON_DRVEN_WHEEL_MAX_SELECTED=0;
				if(NON_DRVEN_WHEEL_MAX_REL==1)
				{
					if(non_driven_max<wheel_speed_max)
					{
						wheel_selection_mode=52;
						NON_DRVEN_WHEEL_MAX_SELECTED=1;
						sel_case=6;						
					}
					else if(non_driven_max==wheel_speed_max)						
					{
						#if	__MY_09_VREF					
						
						if((wheel_speed_max-wheel_speed_2nd)<64)
						{
							sel_case=6;	
							wheel_selection_mode=52;
							NON_DRVEN_WHEEL_MAX_SELECTED=1;
						}
						else
						{
							tempF0=0;
							if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
							{
								tempF0=1;
							}
							#if	__RTA_ENABLE
							else if((RTA_FL.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_70MS)||(RTA_FR.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_70MS)
								||(RTA_RL.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_70MS)||(RTA_RR.det_rta_suspect_cnt>L_U8_TIME_10MSLOOP_70MS))
							{
								tempF0=1;
							}
							#endif
							
							#if	__4WD &&__ENGINE_INFORMATION
							else if((mtp>10)&&(eng_torq_rel>150)&&((non_driven_max-vehicle_speed)>=32))
							{
								tempF0=1;
							}
							#endif
							else
							{
								sel_case=6;	
								wheel_selection_mode=52;
								NON_DRVEN_WHEEL_MAX_SELECTED=1;								
							}
							
							if(tempF0==1)
							{
								if(RELIABILITY_2ND_WL==1)
								{
									sel_case=2;	
									wheel_selection_mode=27;
									NON_DRVEN_WHEEL_MAX_SELECTED=1;							
								}
								else 
								{
									sel_case=3;	
									wheel_selection_mode=30;
									NON_DRVEN_WHEEL_MAX_SELECTED=1;								
								}								
							}							
						}			
						#else
						if((wheel_speed_max-wheel_speed_2nd)<32)
						{
							sel_case=6;	
							wheel_selection_mode=52;
							NON_DRVEN_WHEEL_MAX_SELECTED=1;
						}
						
						else if(RELIABILITY_2ND_WL==1)
						{
							sel_case=2;	
							wheel_selection_mode=27;
							NON_DRVEN_WHEEL_MAX_SELECTED=1;							
						}
						else 
						{
							sel_case=3;	
							wheel_selection_mode=30;
							NON_DRVEN_WHEEL_MAX_SELECTED=1;								
						}
						#endif
					}
					else
					{
						;
					}					
				}				
				if(NON_DRVEN_WHEEL_MAX_SELECTED==0)
				{
					#if	__MY_09_VREF
					if(non_driven_min>driven_max)
					{
						if(RELIABILITY_2ND_WL==1)
						{
							sel_case=2;
							wheel_selection_mode=120;								
						}
						else if(RELIABILITY_3RD_WL==1)
						{				
							sel_case=3;
							wheel_selection_mode=130;				
						}
						else if(RELIABILITY_MIN_WL==1)
						{				
							sel_case=4;
							wheel_selection_mode=140;				
						}
						else
						{
							#if	__4WD
							sel_case=3;
							wheel_selection_mode=131;								
							#else
							sel_case=2;
							wheel_selection_mode=121;								
							#endif							
						}
					}
					else if(RELIABILITY_3RD_WL==1)
					{				
						sel_case=3;
						wheel_selection_mode=39;				
					}
					#else
					if(RELIABILITY_3RD_WL==1)
					{				
						sel_case=3;
						wheel_selection_mode=39;				
					}
					#endif
					else if((wheel_speed_3rd<vehicle_speed)&&(RELIABILITY_2ND_WL==1))
					{
						sel_case=2;
						wheel_selection_mode=28;	
					}						
					else if((wheel_speed_3rd>=vehicle_speed)&&(RELIABILITY_MIN_WL==1))
					{
						sel_case=4;
						wheel_selection_mode=48;								
					}
					else if(RELIABILITY_2ND_WL==1)
					{
						sel_case=2;
						wheel_selection_mode=29;					
					}
					else if(RELIABILITY_MIN_WL==1)
					{
						sel_case=4;
						wheel_selection_mode=49;						
					}				
					else if(RELIABILITY_MAX_WL==1)
					{
						sel_case=1;	
						wheel_selection_mode=16;					
					}
					else
					{						
						if(wheel_speed_min>vehicle_speed)
						{
							sel_case=4;
							wheel_selection_mode=40;								
						}						
						else if	(wheel_speed_max<vehicle_speed)	
						{
							sel_case=1;
							wheel_selection_mode=17;								
						}
						else
						{
							#if	__PARKING_BRAKE_OPERATION
							if(PARKING_BRAKE_OPERATION==1)
							{
								#if	__ENGINE_INFORMATION
								if((mtp<5)&&(eng_torq_rel<200))
								{
								#endif
								sel_case=1;
								wheel_selection_mode=19;	
								#if	__ENGINE_INFORMATION
								}
								else if(wheel_speed_max<vehicle_speed)
								{
									sel_case=1;
									wheel_selection_mode=18;									
								}
								else 
								{
									sel_case=2;
									wheel_selection_mode=20;										
								}								
								#endif						
							
							}
							else
							{
							#endif	
							sel_case=3;
							wheel_selection_mode=30;	
							#if	__PARKING_BRAKE_OPERATION
							}					
							#endif		
						}				
					}
				}
			#if	__VDC
			}
			#endif
		#if	__4WD || __4WD_VARIANT_CODE==ENABLE
		}
		#endif
	}
	if(sel_case==1)
	{
		selected_whl_speed_1=wheel_speed_max;
		selected_whl_speed_2=wheel_speed_2nd;		
	}
	else if(sel_case==2)
	{
		selected_whl_speed_1=wheel_speed_2nd;
		selected_whl_speed_2=wheel_speed_3rd;			
	}
	else if(sel_case==3)
	{
		selected_whl_speed_1=wheel_speed_3rd;
		selected_whl_speed_2=wheel_speed_min;			
	}
	else if(sel_case==4)
	{
		selected_whl_speed_1=wheel_speed_min;
		selected_whl_speed_2=wheel_speed_min;	
	}
	else if(sel_case==5)
	{
		selected_whl_speed_1=vehicle_speed;
		selected_whl_speed_2=vehicle_speed;	
	}
	else if((NON_DRVEN_WHEEL_MAX_SELECTED==1)&&(sel_case==6))
	{
		selected_whl_speed_1=non_driven_max;
		selected_whl_speed_2=non_driven_min;	
	}
	else
	{
		;
	}

	FZ1.vrselect_resol_change=selected_whl_speed_1;
	FZ2.vrselect_resol_change=selected_whl_speed_2;
	FZ1.vrselect=(FZ1.vrselect_resol_change>>3);
	FZ2.vrselect=(FZ2.vrselect_resol_change>>3);

}
#endif /* if__MGH60_NEW_SELECTION */

/*******************************************************************/

void	LDABS_vSaveVrefCalcResult(void)
{
	tempW0=voptfz_new;
/*
	if(tempW0 <	(int16_t)VREF_MIN_SPEED) 
	{
		tempW0=VREF_MIN_SPEED;
	}
	else if(tempW0 > (int16_t)VREF_MAX_RESOL_CHANGE) 
	{
		tempW0=VREF_MAX_RESOL_CHANGE;
	}
	else 
	{
		;
	}
	FZ->voptfz_resol_change=tempW0;
*/
	FZ->voptfz_resol_change=LCABS_s16LimitMinMax(tempW0,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	tempW0=(tempW0>>3);

/*
	if(tempW0 <	(int16_t)VREF_MIN_SPEED_RESOL_8) 
	{
		tempW0=VREF_MIN_SPEED_RESOL_8;
	}
	else if(tempW0 > (int16_t)VREF_MAX)	
	{
		tempW0=VREF_MAX;
	}
	else 
	{
		;
	} 
*/
	tempW0=LCABS_s16LimitMinMax(tempW0,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
  #if __4WD_VARIANT_CODE==ENABLE
	if(lsu8DrvMode !=DM_2WD)
	{
		if(!BIG_ACCEL_SPIN_FZ)
		{
			FZ->voptfz_diff=tempW0-FZ->voptfz;
		}
		else		
		{
			FZ->voptfz_diff=(vref2-vref_alt);	
		}	
	}
	else /*2WD */
	{
		FZ->voptfz_diff=tempW0-FZ->voptfz;
	}  
  #else	/*__4WD_VARIANT_CODE==ENABLE*/
#if	__4WD
	
	if(!BIG_ACCEL_SPIN_FZ)
	{
		FZ->voptfz_diff=tempW0-FZ->voptfz;
	}
	else 
	{
		FZ->voptfz_diff=(vref2-vref_alt);	
	}	

#else /*!_4WD */
	FZ->voptfz_diff=tempW0-FZ->voptfz;
#endif /*_4WD*/
  #endif/*__4WD_VARIANT_CODE==ENABLE*/
	FZ->voptfz=tempW0;	
}
/*******************************************************************/
void LDABS_vDetVrefInputLimit(void)
{	
/*	if(ABS_fz==1) */
	if((BRAKING_STATE==1)&&(ABS_fz==1))
	{
		vref_input_inc_limit=INSIDE_ABS_UPPER_LIMIT;
		vref_input_dec_limit=lds16VrefInABSLowLimitDefault;
		#if	(__4WD && (__ETC ||	__TCS) )  || (__4WD_VARIANT_CODE==ENABLE)
		#if	__4WD_VARIANT_CODE==ENABLE 
		if(lsu8DrvMode !=DM_2WD)
		{
		#endif				
		if(vref<vrselect1)
		{
			#if	__ENGINE_INFORMATION
			if(ldabsu1CanErrFlg==0)
			{
				if((mtp>10)&&(eng_torq_rel>TORQ_PERCENT_40PRO)&&(!BRAKE_SIGNAL)&&(!FSF_fl)&&(!FSF_fr)) 
				{
					vref_input_inc_limit=ABS_TO_ACCEL_UPPER_LIMIT;
				}
				else 
				{
					;
				}
			}
			else
			{
			#endif
			
				vref_input_inc_limit=ABS_TO_ACCEL_UPPER_LIMIT;
				
			#if	__ENGINE_INFORMATION
			}
			#endif			 
		}
		else 
		{
			;
		}
		#if	__4WD_VARIANT_CODE==ENABLE 
		}
		#endif		
		#endif

		tempF0=0;
		if(slew_rate_timer_fl >= (uint8_t)L_U8_TIME_10MSLOOP_220MS) 
		{ /* when int32_t dump time, afz >=	-0.5g */
			if(slew_rate_timer_fr >= (uint8_t)L_U8_TIME_10MSLOOP_220MS) 
			{
				if((FSF_fl==0) && (FSF_fr==0)) 
				{
					if((JUMP_DOWN_fl==1) ||	(JUMP_DOWN_fr==1)) 
					{
						if(vref_resol_change > (FZ1.vrselect_resol_change + VREF_1_5_KPH_RESOL_CHANGE))
						{
						tempF0=1;				
						LOW_MU_SUSPECT_FLG=1;
					}	
					else 
					{
						;
					}
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}
		}
		else 
		{
			;
		}
		}
		else 
		{
			;
		}
		if(tempF0==0) 
		{
			if((STABIL_fl==1)||(STABIL_fr==1))
			{
				LOW_MU_SUSPECT_FLG=0;
			}
			else if((STABIL_rl==1)||(STABIL_rr==1))
			{
				LOW_MU_SUSPECT_FLG=0;
			}
			else if((rel_lam_fl>-5)||(rel_lam_fr>-5)||(rel_lam_rl>-5)||(rel_lam_rr>-5))
			{
				LOW_MU_SUSPECT_FLG=0;
			}
			else 
			{
				;
			}
		}
		else 
		{
			;
		}
		if (LONG_ACCEL_STATE==1)
		{
			vref_input_dec_limit=INSIDE_ABS_LOWER_LIMIT_HIGH_MU;
		}
		else if(LOW_MU_SUSPECT_FLG==1) 
		{
			vref_input_dec_limit=INSIDE_ABS_LOWER_LIMIT_LOW_MU;
		}
		else 
		{
			if((AFZ_OK==1)&&(afz <=	-(int16_t)U8_HIGH_MU))
			{
				vref_input_dec_limit=INSIDE_ABS_LOWER_LIMIT_HIGH_MU;
			}
			else 
			{
				if((STABIL_fl+STABIL_fr+STABIL_rl+STABIL_rr)==0)
				{
					tempF1=0;
					#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)
				#if	__4WD_VARIANT_CODE==ENABLE
					if(lsu8DrvMode !=DM_2WD)
					{
				#endif
					if(USE_ALONG){
						if(along>=-(AFZ_0G4))
						{
							tempF1=1;
						}
						else 
						{
							;
						}
					}
					else
					{
						if((AFZ_OK==1)&&(afz>=-(AFZ_0G4)))
						{
							tempF1=1;
						}
						else 
						{
							;
						}
					}
				#if	__4WD_VARIANT_CODE==ENABLE
					}
				#endif					
					#endif
					
					#if	!__4WD ||(__4WD_VARIANT_CODE==ENABLE)||(__AX_SENSOR==0)
				#if	__4WD_VARIANT_CODE==ENABLE
					if(lsu8DrvMode ==DM_2WD)
					{
				#endif					
					if((AFZ_OK==1)&&(afz>=-(AFZ_0G4)))
					{
						tempF1=1;
					}
					else 
					{
						;
					}
				#if	__4WD_VARIANT_CODE==ENABLE
					}
				#endif						
					#endif
					tempF0=0;
					if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
					{
						tempF0=1;
					}
					else 
					{
						;
					}
					if((tempF0==1)&&(tempF1==1)&&(vrad_diff_max>VREF_1_KPH))
					{
						if((arad_fl<ARAD_10G0)&&(arad_fr<ARAD_10G0))
						{
							
							vref_input_dec_limit=INSIDE_ABS_LOWER_LIMIT_LOW_MU;
						}
						else 
						{
							;
						}
					}
					else 
					{
						;
					}
				}
				else 
				{
					;
				}
			}		
		}	
	}
	else { /* outside ABS */
		vref_input_inc_limit=lds16VrefOutABSUpLimitDefault;
		vref_input_dec_limit=lds16VrefOutABSLowLimitDefault;		
		#if	__PARKING_BRAKE_OPERATION
		if((PARKING_BRAKE_OPERATION==1)&&(BRAKE_SIGNAL==0))
		{
			vref_input_dec_limit=PARKING_LOWER_LIMIT;
		}
		else 
		{
		#endif
			if (LONG_ACCEL_STATE==1)
			{
				vref_input_dec_limit=INSIDE_ABS_LOWER_LIMIT_HIGH_MU;
			}
			else if(BRAKE_SIGNAL==1)
			{
				if((afz	<= -(int16_t)U8_HIGH_MU)||(EBD_GRV_HIGH_MU_OK==1))
				{
					vref_input_dec_limit=CBS_HIGH_MU_LOWER_LIMIT;
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}
		#if	__PARKING_BRAKE_OPERATION
		}
		#endif
	}
}
/*******************************************************************/
void LDABS_vDetVrefOutputLimit(	int16_t	fzcase )
{

	int16_t	 VrefDecelG;
/*	
	tempW0=16;
	tempW1=voptfz_new -	FZ1.voptfz_resol_change;
	tempW2=102;
	s16muls16divs16();
	VrefDecelG=tempW3;	  
*/	
	 VrefDecelG= (int16_t)((((int32_t)(voptfz_new - FZ1.voptfz_resol_change))*35)/8);  
	if((ABS_fz==1)&&(BRAKING_STATE==1))	
	{	 
		#if	 __VREF_LIMIT_1
		if(abs_duration_timer <= U8_ABS_1ST_NTH_CTA)/*���߿� Ʃ���Ķ���ͷ� ����*/
		  {	
			vref_output_inc_limit=INSIDE_ABS_UPPER_LIMIT;
			vref_output_dec_limit=lds16VrefInABSLowLimitDefault;		
		  }
		  else
		  {
		  vref_output_inc_limit=INSIDE_ABS_FILOT_UPPER_LIMT;
		  vref_output_dec_limit=INSIDE_ABS_FILOT_LOWER_LIMIT;		
		  }
		#else
		  vref_output_inc_limit=INSIDE_ABS_UPPER_LIMIT;
		  vref_output_dec_limit=lds16VrefInABSLowLimitDefault;	
		#endif
		
		/*FZ->vref_inc_limit_flg=1;	*/
		/*FZ->vref_dec_limit_flg=1;*/
		#if	(__4WD && (__ETC ||	__TCS))	||(__4WD_VARIANT_CODE==ENABLE)				
			#if	__4WD_VARIANT_CODE==ENABLE
		if(lsu8DrvMode !=DM_2WD)
		{
			#endif						
		if(FZ->voptfz<FZ->vrselect)
		{
			#if	__ENGINE_INFORMATION
			if(ldabsu1CanErrFlg==0)
			{
				if((mtp>10)&&(eng_torq_rel>TORQ_PERCENT_40PRO)&&(!BRAKE_SIGNAL)&&(!FSF_fl)&&(!FSF_fr)) 
				{
					vref_output_inc_limit=ABS_TO_ACCEL_UPPER_LIMIT;
				}
				else 
				{
					;
				}
			}
			else
			{
			#endif
			
				vref_output_inc_limit=ABS_TO_ACCEL_UPPER_LIMIT;
				
			#if	__ENGINE_INFORMATION
			}
			#endif
		}
		else 
		{
			;
		}
			#if	__4WD_VARIANT_CODE==ENABLE
		}
			#endif		
		#endif

/*------- Vref Limitation---------------*/
/*------- 1st Cycle	Limitation----------*/
#if	__VREF_LIMIT_2
		if((abs_duration_timer <= U8_ABS_1ST_NTH_CTA) && (VrefPress_Toggle_flag==0))
		{
		  LDABS_vDetVrefLimitFstPress(VrefDecelG, fzcase);
		} 
#else		
		if((FSF_fl==1) || (FSF_fr==1))
		{
		  LDABS_vDetVrefLimitFstNorm();		
		}
#endif					
/*------- Nth Cycle	Limitation----------*/		  
		else
		{
			ldabsu1Arad10gUpperflagFL=0;
			ldabsu1Arad10gUpperflagFR=0;
			ldabsu1Arad5gUpperflagFL=0;
			ldabsu1Arad5gUpperflagFR=0;
			ldu1HighMuSuspectFlag=0;
	 
#if	__VREF_LIMIT_2
			CNT_LM2=0;
			ABS_START_flag=0;		
			VrefPress_Limit_flag=0;
			VrefPress_Toggle_flag=0;
			lds16Abs1StVreflimCnt=0;
			Estimated_Press_ABS_FL=FL.s16_Estimated_Active_Press;
			Estimated_Press_ABS_FR=FR.s16_Estimated_Active_Press;
			Estimated_Press_ABS_RL=RL.s16_Estimated_Active_Press;	
			Estimated_Press_ABS_RR=RR.s16_Estimated_Active_Press;
			#if __BRK_SIG_MPS_TO_PEDAL_SEN == 1
			Estimated_Press_ABS_MP=lsesps16EstBrkPressByBrkPedalF;
			#else
			Estimated_Press_ABS_MP=mpress;
			#endif			 
#endif			  
							 
#if	__VREF_LIMIT_1	 
		   /* vref_test_flags01 = 1;  */
		  LDABS_vDetVrefLimitNthFilt(VrefDecelG,fzcase);
#else
		  LDABS_vDetVrefLimitNthNorm();
#endif	
	   }		
		
	}
	else 
	{
		 vref_output_inc_limit=lds16VrefOutABSUpLimitDefault;
		 vref_output_dec_limit=lds16VrefOutABSLowLimitDefault;
		 /*	FZ->vref_inc_limit_flg=5;*/	
		 /*	FZ1.vref_dec_limit_flg=5;*/
		 
		 #if __VREF_LIMIT_1	
			CNT_LM1=0;
			CNT_L2H=0;
			VrefFilot_Limit_flag=0;
			VrefFilot_Toggle_flag=0;
		 #endif
			ldabsu1Arad10gUpperflagFL=0;
			ldabsu1Arad10gUpperflagFR=0;
			ldabsu1Arad5gUpperflagFL=0;
			ldabsu1Arad5gUpperflagFR=0;
			ldu1HighMuSuspectFlag=0;
		 #if __VREF_LIMIT_2
		 	lds16VrefPressDecelG=0;
		 	VrefPress_Limit_flag=0;
		 	VrefPress_Toggle_flag=0;
		 	ABS_START_flag=0;
		 	lds16Abs1StVreflimCnt=0;
		 #endif
			#if	__PARKING_BRAKE_OPERATION
		if(PARKING_BRAKE_OPERATION==1)
		{
			vref_output_dec_limit=PARKING_LOWER_LIMIT;
			/* FZ1.vref_dec_limit_flg=6;*/
		}
		else 
		{
			if(FZ->filter_out <= (int16_t)(-820)) 
			{
				vref_output_dec_limit=OUTSIDE_ABS_LOWER_LIMIT_HIGH_MU;
				/* FZ1.vref_dec_limit_flg=7;*/
			}
			else 
			{
				;
			}
		}
			#else
		if(FZ->filter_out <= (int16_t)(-820)) 
		{
			vref_output_dec_limit=OUTSIDE_ABS_LOWER_LIMIT_HIGH_MU;
			/* FZ1.vref_dec_limit_flg=8;*/
		}
		else 
		{
			;
		}
			#endif
	}

	if(LONG_ACCEL_STATE==1)
	{
		vref_output_dec_limit=OUTSIDE_ABS_LOWER_LIMIT_HIGH_MU;
	}
	
}

void LDABS_vDetVrefLimitFstNorm(void)
{
	LowMuLimitFlag=0;
	
	if(arad_fl>=ARAD_10G0)
	{
		ldabsu1Arad10gUpperflagFL=1;
	}
	if(arad_fr>=ARAD_10G0)
	{
		ldabsu1Arad10gUpperflagFR=1;
	}
	if(arad_fl>=ARAD_5G0)
	{
		ldabsu1Arad5gUpperflagFL=1;
	}
	if(arad_fr>=ARAD_5G0)
	{
		ldabsu1Arad5gUpperflagFR=1;
	}
						
	if(ldu1HighMuSuspectFlag==0)
	{
		if((ldabsu1Arad10gUpperflagFL==1)&&(ldabsu1Arad10gUpperflagFR==1))
		{
			ldu1HighMuSuspectFlag=1;	
		}
		else if((ldabsu1Arad5gUpperflagFL==0)&&(ldabsu1Arad5gUpperflagFR==0))					
		{
			;  
		}	
		else
		{
/*			tempW3=(int16_t)((((int32_t)voptfz1-vrselect1)*100)/voptfz1);*/	   
			/*opttime :	ksw*/		
			tempW2 =( voptfz1-vrselect1);			 
			tempW3=(int16_t)(( (int32_t)tempW2*100 )/voptfz1 );
			
			if((ldabsu1Arad10gUpperflagFL==1)||(ldabsu1Arad10gUpperflagFR==1))
			{
				if(tempW3<=3){ldu1HighMuSuspectFlag=1;}
			}
			else if((ldabsu1Arad5gUpperflagFL==1)&&(ldabsu1Arad5gUpperflagFR==1))
			{
				if(tempW3<=3){ldu1HighMuSuspectFlag=1;}
			}
			else
			{
				;
			}
		}
	}			
											
	if(	( abmittel_fz >= (int16_t)lds16VrefLowMuDctByDumpRiseCnt1) || 
	((abmittel_fz >= (int16_t)lds16VrefLowMuDctByDumpRiseCnt2)&&((Unstabil_counter_fl>=L_U8_TIME_10MSLOOP_150MS)&&(Unstabil_counter_fr>=L_U8_TIME_10MSLOOP_150MS))))
	{
		
		if((abs_duration_timer > 0)&&(abs_duration_timer<=L_U8_TIME_10MSLOOP_500MS))
		{
			LowMuLimitFlag=1;
		}
		else if	((abs_duration_timer>L_U8_TIME_10MSLOOP_500MS)&&(abs_duration_timer<=L_U8_TIME_10MSLOOP_1000MS))
		{	  

			if((STABIL_fl+STABIL_fr+STABIL_rl+STABIL_rr)==0)
			{

				LowMuLimitFlag=1;


			}
		}				
		else if	((abs_duration_timer>=L_U8_TIME_10MSLOOP_1000MS)&&(abs_duration_timer <= L_U8_TIME_10MSLOOP_1500MS) )
		{
			if((rel_lam_fl<-30)&&(rel_lam_fr<-30)&&(rel_lam_rl<-20)&&(rel_lam_rr<-20))
			{
				LowMuLimitFlag=1;
			}
		}
		else
		{
			;
		}  
		
		/*tempW3=(int16_t)((((int32_t)voptfz1-vrselect1)*100)/voptfz1);*/		
		/* opttime : ksw*/
		
		tempW2 = (voptfz1-vrselect1);
		tempW3 =(int16_t)( ((int32_t)tempW2*100)/voptfz1);

		if((ldu1HighMuSuspectFlag==1)||((tempW3<=3)||(rel_lam_rl>-3)||(rel_lam_rr>-3)))
		{
			LowMuLimitFlag=0;
		}
		else
		{
			;
		}

		if((LowMuLimitFlag==1)&&(ABS_fz==1))
		{
			vref_output_dec_limit=INSIDE_ABS_LOWER_LIMIT_LOW_MU;
		}
		else
		{
			;
		}		   
	}
	else
	{
		if((FZ->filter_out <= (int16_t)(-820)) && (rel_lam_fl>-20)&&(rel_lam_fr>-20) )	
		{
			vref_output_dec_limit=INSIDE_ABS_LOWER_LIMIT_HIGH_MU;
		   /* FZ1.vref_dec_limit_flg=14; */
		}
		else
		{
		   /* FZ1.vref_dec_limit_flg=15;*/
		}
    }					
			
}

void LDABS_vDetVrefLimitNthNorm(void) 
{					
	if(FZ->filter_out <= (int16_t)(-820))
	{
		vref_output_dec_limit=INSIDE_ABS_LOWER_LIMIT_HIGH_MU;
		/* FZ1.vref_dec_limit_flg=3;*/
	}
	else 
	{
		if((STABIL_fl+STABIL_fr+STABIL_rl+STABIL_rr)==0)
		{
			tempF1=0;
			
			#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE))&&(__AX_SENSOR==1)
				#if	__4WD_VARIANT_CODE==ENABLE
			if(lsu8DrvMode !=DM_2WD)	
			{
				#endif
			if(USE_ALONG){
				if(along>=-(AFZ_0G4))
				{
					tempF1=1;
				}
				else 
				{
					;
				}
			}
			else
			{
				if((AFZ_OK==1)&&(afz>=-(AFZ_0G4)))
				{
					tempF1=1;
				}
				else 
				{
					;
				}
			}
				#if	__4WD_VARIANT_CODE==ENABLE
			}
				#endif						
			#endif
			
			#if	!__4WD ||(__4WD_VARIANT_CODE==ENABLE)||(__AX_SENSOR==0)
				#if	__4WD_VARIANT_CODE==ENABLE
			if(lsu8DrvMode ==DM_2WD)	
			{
				#endif
			if((AFZ_OK==1)&&(afz>=-(AFZ_0G4)))
			{
				tempF1=1;
			}
			else 
			{
				;
			}
				#if	__4WD_VARIANT_CODE==ENABLE
			}
				#endif						
			#endif
			
			tempF0=0;
			if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
			{
				tempF0=1;
			}
			else 
			{
				;
			}
			if((tempF0==1)&&(tempF1==1)&&(vrad_diff_max>VREF_1_KPH))
			{
				if((arad_fl<ARAD_10G0)&&(arad_fr<ARAD_10G0))
				{							
					vref_output_dec_limit=INSIDE_ABS_LOWER_LIMIT_LOW_MU;
					/* FZ1.vref_dec_limit_flg=4;*/
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}
		}
	}
}

#if	__VREF_LIMIT_1								   
void LDABS_vDetVrefLimitNthFilt(int16_t	 VrefDecelG, int16_t fzcase)
{	
	int16_t	FILTOT;
	int16_t	Filout01G;
	/*int16_t	lds16FiloutDecelG;*/
	
	Filout01G=-102;	
									  
	if( fzcase==1	)
	{
		VrefFilot_Toggle_flag=0;
		FILTOT=FZ1.filter_out;
	
		if(FILTOT < Filout01G)
		{	
			if(FZ1.voptfz_resol_change <= VREF_5_KPH_RESOL_CHANGE)
			{
		   		WFactor=25;
			}
			else
			{
				if( (FL.LOW_MU_SUSPECT_by_IndexMu==1)&& (FR.LOW_MU_SUSPECT_by_IndexMu==1) )
				{
					WFactor=10;
				}
				else if( LOW_to_HIGH_suspect==1 )
				{
					WFactor=25;										
				}
				#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
				else if(ldu1absl2hdetectionflag == 1)
				{
					WFactor=25;	
				}
				#endif
				else
				{
					WFactor=15;					
				}
			}
		}
		else
		{
			FILTOT=Filout01G;
			WFactor=15;					
		}
		/*										
		tempW0=100;
		tempW1=FILTOT;
		tempW2=WFactor;
		s16muls16divs16();
		lds16FiloutDecelG=tempW3;  
		*/				
		lds16FiloutDecelG=(int16_t)(((int32_t)FILTOT*WFactor)/100);
		/*
		tempW0=16;
		tempW1=voptfz_new -	FZ1.voptfz_resol_change;
		tempW2=102;
		s16muls16divs16();
		VrefDecelG=tempW3;
		*/
		VrefDecelG=(int16_t)(((int32_t)(voptfz_new - FZ1.voptfz_resol_change)*35)/8);
		if(VrefFilot_Limit_flag==1)
		{
	        if((vref_resol_change >= lds16VrefRawSignal) 
	        && (CNT_LM1 < L_U8_TIME_10MSLOOP_500MS) 
	        && (CNT_L2H < L_U8_TIME_10MSLOOP_150MS) 
	        && (LOW_to_HIGH_suspect == 0)
	        #if (__PRESS_DECEL_VREF_APPLY == ENABLE) 
	        && (ldu1absl2hdetectionflag == 0)
	        #endif	
			)
			{	
				CNT_LM1=CNT_LM1+1;
				vref_output_dec_limit=lds16FiloutDecelG;
				VrefFilot_Limit_flag=1;
				/*FZ1.vref_dec_limit_flg=20;  */
	            /*  vref_test_flags01 = 7;        */
				#if	__PREMIUM_VREF
				if((lsesps16PremPressureFL > MPRESS_20BAR)
				&&(lsesps16PremPressureFR > MPRESS_20BAR)
				&&(Vehicle_Decel_EST_filter < (-U8_LOW_MU)))
				{
				#endif
		            if((vrad_diff_max_resol_change <= VREF_5_KPH_RESOL_CHANGE) 
		            &&(vref_resol_change > (wheel_speed_max + VREF_0_5_KPH_RESOL_CHANGE)))
		            {
						#if (__IMPROVED_VREF_IN_ABS_2 == ENABLE) /*nth l2h */
		            	if(lsabs16wheelaccel <= 0 )
		            	{
		                	CNT_L2H = CNT_L2H + 1;
		                }
		                else
				{
		                	if(CNT_L2H > 0)
		                	{
		                		CNT_L2H = CNT_L2H - 1;
		                	}
		                	else
				{
		                		;
		                	}
		                }
						#else
					CNT_L2H=CNT_L2H+1;							
						#endif	                 
				}
				else
				{
					CNT_L2H=0;
				}
				#if	__PREMIUM_VREF
				}
				#endif														
			}
			else
			{	
					if(CNT_L2H == L_U8_TIME_10MSLOOP_150MS)
					{
						VrefFilot_Toggle_flag=1;	
	              /*vref_test_flags01 = 8;*/
					  	/* FZ1.vref_dec_limit_flg=21;	*/
					}
					else if(CNT_LM1	== L_U8_TIME_10MSLOOP_500MS)
					{ 
						VrefFilot_Toggle_flag=1;	
					  	/* FZ1.vref_dec_limit_flg=22;	*/	
	              /*vref_test_flags01 = 9; */
					}
	            else if((LOW_to_HIGH_suspect == 1)
	            #if (__PRESS_DECEL_VREF_APPLY == ENABLE) 
	            ||(ldu1absl2hdetectionflag==1)
	            #endif
	            )
					{
						VrefFilot_Toggle_flag=1;	
	              /*vref_test_flags01 = 10;*/
					  	/* FZ1.vref_dec_limit_flg=23;		*/							
					}
					else
					{												  
					  	/* FZ1.vref_dec_limit_flg=24;		*/					
	              /*vref_test_flags01 = 11;  */         
					}
					CNT_LM1=0;
					CNT_L2H=0;
					VrefFilot_Limit_flag=0;
			}
		}
		else
		{
			CNT_LM1=0;
			CNT_L2H=0;					
			if((AFZ_OK==1) 
			&&(FZ1.voptfz_mittel_resol_change > vref_resol_change) 
			&&(FZ1.voptfz_mittel_resol_change > wheel_speed_max) 
			&&(FZ1.filter_out <= Filout01G) 
			&&(FZ1.voptfz_resol_change >= wheel_speed_max)	
			&&(LOW_to_HIGH_suspect == 0)
			#if (__PRESS_DECEL_VREF_APPLY == ENABLE) 
	        &&(ldu1absl2hdetectionflag == 0)
	        #endif 
	        &&(abs_duration_timer > U8_ABS_1ST_NTH_CTA) /* */
			)
			{
				/*vref_test_flags01 = 3;*/
				#if	__PREMIUM_VREF
				if((VrefDecelG	< lds16FiloutDecelG)
				&&(lsesps16PremPressureFL <= MPRESS_70BAR)
				&&(lsesps16PremPressureFR <= MPRESS_70BAR)
				&&(Vehicle_Decel_EST_filter >=(-U8_HIGH_MU)))
				#else
				if(	VrefDecelG < lds16FiloutDecelG )
				#endif
				{
					/*vref_test_flags01 = 4;*/
					vref_output_dec_limit= lds16FiloutDecelG;
					VrefFilot_Limit_flag=1;
				  /* FZ1.vref_dec_limit_flg=25;		*/					  
				}
				else
				{
					/*vref_test_flags01 = 5;*/
					VrefFilot_Limit_flag=0;	
				  /* FZ1.vref_dec_limit_flg=26;		*/					  
				}							
			}
			else
			{	/*vref_test_flags01 = 6;*/
				VrefFilot_Limit_flag=0;		
				/* FZ1.vref_dec_limit_flg=27;		*/							
			}
		}		
				
		vref_output_dec_limit=LCABS_s16LimitMinMax(vref_output_dec_limit,INSIDE_ABS_FILOT_LOWER_LIMIT,INSIDE_ABS_FILOT_UPPER_LIMT);																
	}
	else
	{
		;
	}
}							  
#endif											   
												   
#if	__VREF_LIMIT_2								   
void LDABS_vDetVrefLimitFstPress(int16_t VrefDecelG, int16_t fzcase)
{			

	int8_t PressLIMIT;
	int8_t PFactor;
	PFactor=11;
			   
	if(ABS_START_flag==0)
	{  
		Estimated_Press_ABS_FL=FL.s16_Estimated_Active_Press;
		Estimated_Press_ABS_FR=FR.s16_Estimated_Active_Press;
		Estimated_Press_ABS_RL=RL.s16_Estimated_Active_Press;	 
		Estimated_Press_ABS_RR=RR.s16_Estimated_Active_Press;
	   #if __BRK_SIG_MPS_TO_PEDAL_SEN == 1
	   Estimated_Press_ABS_MP=lsesps16EstBrkPressByBrkPedalF;
	   #else
	   Estimated_Press_ABS_MP=mpress;
	   #endif	
	   ABS_START_flag=1;	
	}	
	else
	{
	   ;	
	}
	
	if((Estimated_Press_ABS_FL<=MPRESS_70BAR)&&(Estimated_Press_ABS_FR<= MPRESS_70BAR) 
	&&(Estimated_Press_ABS_RL<=MPRESS_70BAR)&&(Estimated_Press_ABS_RR<= MPRESS_70BAR))
	{
		PressLIMIT=1;
	}
	else
	{
		PressLIMIT=0;
	}
	
	
	if(	(ESP_ERROR_FLG==0) && (EST_WHEEL_PRESS_OK_FLG==1) && (PressLIMIT==1) )
	{
		if( fzcase==1	)
		{	  
        	if(lds16Abs1StVreflimCnt < L_U16_TIME_10MSLOOP_1MIN)
        	{
        		lds16Abs1StVreflimCnt = lds16Abs1StVreflimCnt + 1;
		/*				
        		if(lds16VrefRawSignal >= vref_resol_change)
        		{
        			lds16Abs1StVreflimCnt = 1;
        		}
        		else
        		{
        			;
        		}
		*/
        	}
        	else
        	{
        		;
        	}
        	
        	tempW0 = lds16Abs1StVreflimCnt + AFZ_0G1;
        	
        	if(tempW0 > AFZ_0G3)
        	{
        		tempW0 = AFZ_0G3;
        	}
        	else
        	{
        		;
        	}
        	
        	lds16VrefPressDecelG = (((Vehicle_Decel_EST_filter)-tempW0)*PFactor)/10;

			if(VrefPress_Limit_flag==1)	  
			{							
				if((vref_resol_change >= lds16VrefRawSignal) && (CNT_LM2 < L_U8_TIME_10MSLOOP_800MS) && (LFC_GMA_Split_flag==0))			
				{
					vref_output_dec_limit= lds16VrefPressDecelG;
					VrefPress_Limit_flag=1;
					/* FZ1.vref_dec_limit_flg=40;	*/
					CNT_LM2=CNT_LM2+1;					  
				}
				else
				{
					if(	CNT_LM2== L_U8_TIME_10MSLOOP_800MS )
					{
						VrefPress_Toggle_flag=1;
					   	/* FZ1.vref_dec_limit_flg=41; */
					}
					else
					{
					   	/* FZ1.vref_dec_limit_flg=43;*/
					}
				
					VrefPress_Limit_flag=0;								
					CNT_LM2=0;				  
				}
			}
			else
			{
				CNT_LM2=0;	
				if(LFC_GMA_Split_flag==0)
				{				  
					if(	VrefDecelG < lds16VrefPressDecelG )
					{
					  	vref_output_dec_limit= lds16VrefPressDecelG;
					  	VrefPress_Limit_flag=1;
					  /* FZ1.vref_dec_limit_flg=44;	*/						  
					}
					else
					{
						VrefPress_Limit_flag=0;	
					  /* FZ1.vref_dec_limit_flg=45;	*/						  
					}
				}
				else
				{
					VrefPress_Limit_flag=0;
				/* FZ1.vref_dec_limit_flg=46;	*/				
				}										
			}																																							
		}
		else
		{
			;
		}
	}
	else
	{
		
		if((FSF_fl==1) || (FSF_fr==1))
		{
		  LDABS_vDetVrefLimitFstNorm();		
		}
		else
		{
			;
		}
	} 
	
}								   
#endif											   


/*******************************************************************/
void LDABS_vExecuteVrefInputLimit(int16_t inc_limit, int16_t dec_limit,int16_t fzcase)
{
/*	
	tempW1=512;
	

	tempW2=inc_limit;
	tempW0=50;
	s16muls16divs16();
*/	

	tempW3=(int16_t)(((int32_t)inc_limit*358)/35);
	tempW4=FZ->vrselect_resol_change-FZ->voptfz_mittel_resol_change;
	if(tempW4>tempW3)
	{
		tempW4=tempW3;
	}
	else 
	{
		;
	}
/*
	tempW2=dec_limit;
	s16muls16divs16();
*/
	tempW3=(int16_t)(((int32_t)dec_limit*358)/35);
	if(tempW4<tempW3)
	{
		tempW4=tempW3;
	}
	else 
	{
		;
	}
	
	FZ->filt_inp=tempW4;
}
/*******************************************************************/
void LDABS_vExecuteVrefOutputLimit(int16_t dec_limit,int16_t inc_limit,int16_t fzcase)
{
	int16_t	voptfz_new_temp;

	tempW2=(dec_limit*8)+FZ->vref_dec_limit_mod;
/*
	tempW0=25;
	tempW1=1;
	s16muls16divs16();
*/
	tempW4=tempW2/35;
	FZ->vref_dec_limit_mod=(int8_t)(tempW2%35);
	
	tempW2=(inc_limit*8)+FZ->vref_inc_limit_mod;
/*
	tempW0=25;
	tempW1=1;
	s16muls16divs16();
*/
	tempW3=tempW2/35;
	FZ->vref_inc_limit_mod=(int8_t)(tempW2%35);
	
	#if __4WD ||( __4WD_VARIANT_CODE==ENABLE)

		#if (__AX_SENSOR==1)
    if(fzcase==1)
	{
		ldu1VrefMaxupperlim_old = ldu1VrefMaxupperlim;
		ldu1VrefMaxupperlim = 0;
		if (ACCEL_SPIN_FZ==0)
		{
		    if(((voptfz_new - FZ->voptfz_resol_change) >= tempW3)&&(wheel_speed_min > vref_resol_change)
		    &&(ax_Filter > 0)&&(ax_Filter < 30))
		    {
		        ldu1VrefMaxupperlim = 1;
		    }
		    else 
		    {
		        ;
		    }
		}	
	}
		#endif
	#endif

	tempF0=0;
	tempF1=0;
	
	voptfz_new_temp=voptfz_new;
	
	if((FZ->voptfz_resol_change+tempW4)>voptfz_new)	
	{
	   voptfz_new=FZ->voptfz_resol_change+tempW4;
	   tempF1=1;
	}
	else if	((ABS_fz==1)&&((FZ->voptfz_resol_change+tempW3)<voptfz_new)) 
	{
	   voptfz_new=FZ->voptfz_resol_change+tempW3;
	   tempF0=1;
	}		 
	else 
	{
	  ;
	}				   
	
	  #if __VREF_LIMIT_1
	if(	fzcase==1 )
	{
		if(VrefFilot_Toggle_flag==1)
		{
			voptfz_new=voptfz_new_temp;
		}
		else
		{
			;
	  	}
	}
	else
	{
		;
	}
	  #endif
	
	  #if __VREF_LIMIT_2
	if(	fzcase==1 )
	{
		if(VrefPress_Toggle_flag==1)
		{
			voptfz_new=voptfz_new_temp;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	  #endif	

	
	if(fzcase==1)
	{
		#if	((__4WD_VARIANT_CODE==ENABLE)||(!__4WD) )||(__AX_SENSOR==0)
			#if __4WD_VARIANT_CODE==ENABLE
		if(lsu8DrvMode ==DM_2WD)
		{		
			#endif
			if(tempF0==1)	
			{
			 /*	vref_limit_inc_counter++; */
				vref_limit_dec_flag=0;
			}
			else if(tempF1==1)
			{
				if(vref_limit_dec_counter_frt<L_U8_CNT_250)
				{
					if(dec_limit <=	-AFZ_1G0)
					{	 
						vref_limit_dec_counter_frt = vref_limit_dec_counter_frt +	7;
					}
					else if((dec_limit>-AFZ_1G0) &&	(dec_limit<= -AFZ_0G5 ))
					{
						vref_limit_dec_counter_frt = vref_limit_dec_counter_frt +	3;
					}
					else
					{
						vref_limit_dec_counter_frt++;
					}
				}
				else
				{
					;
				}
			  
				if(vref_limit_dec_counter_rr<L_U8_CNT_250) 
				{
					if(dec_limit <=	-AFZ_1G0)
					{	 
						vref_limit_dec_counter_rr	= vref_limit_dec_counter_rr	+ 7;
					}
					else if((dec_limit>-AFZ_1G0) &&	(dec_limit<= -AFZ_0G5 ))
					{
						vref_limit_dec_counter_rr	= vref_limit_dec_counter_rr	+ 3;
					}
					else
					{
						vref_limit_dec_counter_rr++;
					}
				}
				else
				{
					;
				}
				/*	
				tempW0=vref;
				tempW1=100;
				tempW2=vref-vrselect1;
				s16muls16divs16();
				*/
				tempW3=(int16_t)(((int32_t)(vref-vrselect1)*100)/vref);
				if((tempW3>3)||((vref-vrselect1)>=VREF_1_25_KPH))
				{
					vref_limit_dec_flag=1;
				}
				else if(vrselect1>vref)
				{
					vref_limit_dec_flag=0;
				}
				else 
				{
					;
				}	
			}
			else 
			{
				/* vref_limit_inc_counter=0; */
				if(vref_limit_dec_counter_frt<=L_U8_TIME_10MSLOOP_210MS)
				{
					vref_limit_dec_counter_frt=0;
				}
				else 
				{
					vref_limit_dec_counter_frt -=4;
				}
				
				if(vref_limit_dec_counter_rr<=L_U8_TIME_10MSLOOP_210MS)
				{
					vref_limit_dec_counter_rr=0;
				}
				else 
				{
					vref_limit_dec_counter_rr	-=3;
				}				 
				vref_limit_dec_flag=0;
				/*			
				if(vref_limit_dec_counter_frt<=0)	
				{
					vref_limit_dec_counter_frt=0;
				}
				else 
				{
				  ;
				}
				if(vref_limit_dec_counter_rr<=0) 
				{
					vref_limit_dec_counter_rr=0;
				}
				*/
			}
			#if (__4WD_VARIANT_CODE==ENABLE)
		}		
			#endif
		#endif		
		
	}
	else
	{
		;
	}	 
	
}

/*******************************************************************/
#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE)) &&(__AX_SENSOR==1)
void LDABS_vDetVrefLimitByLongAcc(int16_t inc_limit, int16_t dec_limit,int16_t fzcase)
{	
	
	vref_inc_limit_by_g=inc_limit;
	vref_dec_limit_by_g=dec_limit;
	/*Vref upper limit gain*/
	if(USE_ALONG==1)
	{
/*		if((ABS_fz)||(BRAKE_SIGNAL))*/
		if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
		{ 
			if((McrAbs(along)<=U8_HIGH_MU) &&(LONG_ACCEL_STATE==0))
			{
				G_Limit_offset_old=G_Limit_offset;
				G_Limit_offset=G_NEG_OFFSET_INSIDE_ABS_DEFAULT;
				#if	__VDC
				G_Limit_offset+=along_by_drift;
				#endif
				g_sensor_limit_flags=1;
				
				#if	__VDC
				if((((STABIL_fl==0)&&(ESP_BRAKE_CONTROL_FL==0))||(ESP_BRAKE_CONTROL_FL==1))&&
				(((STABIL_fr==0)&&(ESP_BRAKE_CONTROL_FR==0))||(ESP_BRAKE_CONTROL_FR==1))&&
				(((STABIL_rl==0)&&(ESP_BRAKE_CONTROL_RL==0))||(ESP_BRAKE_CONTROL_RL==1))&&
				(((STABIL_rr==0)&&(ESP_BRAKE_CONTROL_RR==0))||(ESP_BRAKE_CONTROL_RR==1)))
				{
					if((((FSF_fl==0)&&(ESP_BRAKE_CONTROL_FL==0))||(ESP_BRAKE_CONTROL_FL==1))&&
					(((FSF_fr==0)&&(ESP_BRAKE_CONTROL_FR==0))||(ESP_BRAKE_CONTROL_FR==1)))
					{
						if(all_unstable_cnt<L_U8_CNT_250)
						{
							all_unstable_cnt++;
						}						
					}
				}
				else if((rel_lam_fl<-10)&&(rel_lam_fr<-10)&&(rel_lam_rl<-10)&&(rel_lam_rr<-10))
				{
					if((((FSF_fl==0)&&(ESP_BRAKE_CONTROL_FL==0))||(ESP_BRAKE_CONTROL_FL==1))&&
					(((FSF_fr==0)&&(ESP_BRAKE_CONTROL_FR==0))||(ESP_BRAKE_CONTROL_FR==1)))
					{
						if(all_unstable_cnt<L_U8_CNT_250)
						{
							all_unstable_cnt++;
						}						
					}
				}
				#else
				if((STABIL_fl==0)&&(STABIL_fr==0)&&(STABIL_rl==0)&&(STABIL_rr==0))
				{
					if((FSF_fl==0)&&(FSF_fr==0))
					{
						if(all_unstable_cnt<L_U8_CNT_250)
						{
							all_unstable_cnt++;
						}						
					}
				}
				else if((rel_lam_fl<-10)&&(rel_lam_fr<-10)&&(rel_lam_rl<-10)&&(rel_lam_rr<-10))
				{
					if((FSF_fl==0)&&(FSF_fr==0))
					{
						if(all_unstable_cnt<L_U8_CNT_250)
						{
							all_unstable_cnt++;
						}						
					}
				}				
				#endif
				else 
				{
					all_unstable_cnt=0;
					G_Limit_offset_old=G_NEG_OFFSET_INSIDE_ABS_DEFAULT;
				}
				
				if(all_unstable_cnt>L_U8_TIME_10MSLOOP_700MS)
				{
					#if	__VDC
					if(ESP_SENSOR_RELIABLE_FLG==1)
					{
						if((McrAbs(yaw_out)>YAW_5DEG)&&(McrAbs(alat)>LAT_0G2G))
						{
							G_Limit_offset=G_Limit_offset_old-5;
						g_sensor_limit_flags=2;
					}
						else if(McrAbs(along_by_drift)>AFZ_0G1)
						{
							G_Limit_offset=G_Limit_offset_old-5;
							g_sensor_limit_flags=3;
						}
						else 
						{
							;
						}
					}
					else
					{
						G_Limit_offset=G_Limit_offset_old-5;
						g_sensor_limit_flags=4;
					}
					#else
					G_Limit_offset=G_Limit_offset_old-5;
					g_sensor_limit_flags=5;
					#endif
				}
				#if	__VDC
				else if((VDC_REF_WORK==1)&&(all_unstable_cnt>L_U8_TIME_10MSLOOP_490MS))
				{
					G_Limit_offset=G_Limit_offset_old-5;
					g_sensor_limit_flags=6;
				}
				#endif
				else
				{
					;
				}
				tempW3=along_vref+G_Limit_offset;
				
	
				if(vref_dec_limit_by_g<	tempW3)
				{
					vref_dec_limit_by_g=tempW3;
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}
			if(ABS_fz==0)
			{
				if(ACCEL_SPIN_FZ)
				{
					vref_inc_limit_by_g=lds16VrefOutABSUpLimitDefault;
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}			
		}
		else 
		{
			g_sensor_limit_flags=12;
			/* INCRREMENT Limitation when Vehicle Spin Detection */
            
            if(fzcase == 4)
			{
				#if __SPIN_DET_IMPROVE == ENABLE
				if(ldu1vehSpinDet == 1) /*spin det*/
				{
					tempW1 = along_vref - lds16vehgradientG + along_by_drift + 0;
				
					if(tempW1 < 5)
				{
					   tempW1 = 5;
					}

					if(vref_inc_limit_by_g > tempW1)
					{
						 vref_inc_limit_by_g = tempW1;
				}
				else 
				{
					;
				}
				}
				else
				{
					if(vref_resol_change > VREF_1_KPH_RESOL_CHANGE)
					{
						tempW1 = along_vref - lds16vehgradientG + along_by_drift + 10;
						if(tempW1 < 5)
						{
						   tempW1 = 5;
						}
				if(vref_inc_limit_by_g>tempW1)
				{
					 vref_inc_limit_by_g=tempW1;
				}
				else 
				{
					;
				}
			}
				}
				#else
                if(BIG_ACCEL_SPIN_FZ == 1)
	            {
	            	if(lds16TcsVreflimCnt < L_U16_TIME_10MSLOOP_1MIN)
	            	{
	            		lds16TcsVreflimCnt = lds16TcsVreflimCnt + 1;
	            	}
			else 
			{
				;
			}
	            	if(vref_resol_change >= wheel_speed_min)
	            	{
	            		lds16TcsVreflimCnt = 1;
	            	}
	            	else
	            	{
	            		;
	            	}
	            }
	            else
	            {
	            	lds16TcsVreflimCnt = 0;
	            }

	            if(ACCEL_SPIN_FZ == 1)
	            {
					tempW1=along_vref + AFZ_0G2;
					
					if(BIG_ACCEL_SPIN_FZ==1)
					{
						if((lds16TcsVreflimCnt > 0) && (lds16TcsVreflimCnt <= L_U8_TIME_10MSLOOP_1500MS))
		                {
			                tempW1 = along_vref + AFZ_0G05;
		                }
		                else
		                {
			                tempW0 = ((lds16TcsVreflimCnt - L_U8_TIME_10MSLOOP_1500MS)*G_Limit_Gain_1)/L_U8_TIME_10MSLOOP_1000MS;/* 0.01g/1sec */
		                	
		                	if(tempW0 > AFZ_0G3)
		                	{
		                		tempW0 = AFZ_0G3;
		                	}
		                	
		                	tempW1 = along_vref + tempW0+AFZ_0G05;
		                }
				}
				else 
				{
					;
				}
				if(vref_inc_limit_by_g>tempW1)
				{
					 vref_inc_limit_by_g=tempW1;
				}
				else 
				{
					;
				}
			}
			else 
			{
				;
			}
				#endif
            }
/*--------------------------------------------------------------------------
	  Decrement	Limitation when	 engine	brake state
----------------------------------------------------------------------------
			#if	__EDC
			if(Engine_braking_flag==1) {
				tempW1=along_vref+G_NEG_OFFSET_ENGINE_BRAKE;
				if(vref_dec_limit_by_g<tempW1)vref_dec_limit_by_g=tempW1;
				g_sensor_limit_flags=4;
			}
			#endif
---------------------------------------------------------------------------*/			
			/* parking brake detection state */
			#if	__PARKING_BRAKE_OPERATION
	
			if(PARKING_BRAKE_OPERATION==1)
			{
				tempW1=along_vref+G_NEG_OFFSET_PARKING_BRAKE;
				if(vref_dec_limit_by_g<tempW1)
				{
					vref_dec_limit_by_g=tempW1;
				}
				else 
				{
					;
				}
				g_sensor_limit_flags=5;
			}
			else {
				;
			}
			#endif
			
			/* Decrement Limitation	when  ESP brake	control	 state*/

			#if	__VDC				
			if((VDC_REF_WORK==1)&&(ACCEL_SPIN_FZ==0))
			{
				G_Limit_offset_old=G_Limit_offset;
				if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0)) 
				{
					if((rel_lam_fl<=-10)&&(rel_lam_fr<=-10)&&(rel_lam_rl<=-10)&&(rel_lam_rr<=-10))
					{						
						if(g_sensor_error_cnt<L_U8_TIME_10MSLOOP_100MS)
						{
							g_sensor_error_cnt++;
						}
						else 
						{
							;
						}
						
						if(g_sensor_error_cnt>=L_U8_TIME_10MSLOOP_100MS)
						{
							G_Limit_offset=G_NEG_OFFSET_ESP_CONTROL5;
							g_sensor_limit_flags=6;	
						}
						else 
						{
							;
						}
					}
					else if((rel_lam_fl<-5)&&(rel_lam_fr<-5)&&(rel_lam_rl<-5)&&(rel_lam_rr<-5))
					{
						G_Limit_offset=G_Limit_offset_old-20;
						g_sensor_limit_flags=7;
					} 
					else 
					{
						G_Limit_offset=G_NEG_OFFSET_ESP_CONTROL3;
						g_sensor_limit_flags=8;
					}
				}
				else if(McrAbs(alat)<LAT_0G2G)
				{
					G_Limit_offset=G_NEG_OFFSET_ESP_CONTROL1;		
					g_sensor_limit_flags=9;
				}
				else 
				{
					G_Limit_offset=G_NEG_OFFSET_ESP_CONTROL4;		
					g_sensor_limit_flags=20;					
				}
				if(ESP_SENSOR_RELIABLE_FLG==1)
				{
					if(McrAbs(yaw_out)>YAW_5DEG)	 
					{
						if((vref_resol_change-wheel_speed_max)>128)
						{
							G_Limit_offset=G_Limit_offset_old-5;
							g_sensor_limit_flags=g_sensor_limit_flags+30;
						}
					}
					else if((wheel_speed_2nd-vref_resol_change)>64)
					{
						G_Limit_offset_old=G_NEG_OFFSET_ESP_CONTROL1;
					}	 
					else
					{
						;
					}			
				}
				else
				{
					
				}
				tempW3=along_vref+along_by_drift+G_Limit_offset;
				
/*				
				if(ESP_SENSOR_RELIABLE_FLG==1)
				{
					tempW5=LCESP_s16IInter3Point( abs_wstr,	0, 0, 300, 10,1200,20 );
					tempW7=McrAbs(yaw_out);
					tempW6=LCESP_s16IInter3Point( tempW7, 0, 0,	1000, 20,2000,40 );
					tempW1=(tempW5+tempW6);
					
					if(McrAbs(alat)>LAT_0G4G)
					{
						tempW1=tempW1+G_NEG_OFFSET_ESP_CONTROL1;
					}
					else if(McrAbs(alat)>LAT_0G15G)
					{
						tempW1=tempW1+G_NEG_OFFSET_ESP_CONTROL4;
					}
					else 
					{
						tempW1=0;
					}
					tempW3=tempW3-tempW1;
				}
				
				tempW1=along_vref+along_by_drift+G_NEG_OFFSET_ESP_CONTROL1;
				if(tempW1<tempW3)
				{
					tempW3=tempW1;
					g_sensor_limit_flags=14;
				}
				else 
				{
					;
				}
*/
				if(vref_dec_limit_by_g<tempW3) 
				{
					vref_dec_limit_by_g=tempW3;
				}
				else 
				{
					;
				}
			}
			#endif
		}
	}
	else 
	{
		g_sensor_limit_flags=11;
	}
	
	/* When	Vehicle	Stop state,	Inhibit	using long G sensor	for	Vref est */
	if((vrad_rr<VREF_MIN)&&(vrad_rl<VREF_MIN)&&(vrad_fr<VREF_MIN)&&(vrad_fl<VREF_MIN)&&(vref<VREF_MIN))
	{ 
		vref_inc_limit_by_g=inc_limit;
		vref_dec_limit_by_g=dec_limit;
		g_sensor_limit_flags=15;
	}
	else
	{
		;
	}
}
#endif
/*******************************************************************/
void LDABS_vExecuteVrefFiltering( int16_t fzcase )
{
//	int16_t	filter_out_new_v;
//	int16_t	non_driven_WL1;
//	int16_t non_driven_WL2;
//	int16_t non_driven_WheelV;

	if(speed_calc_timer <= (uint8_t)L_U8_TIME_10MSLOOP_200MS)	
	{
		FZ->voptfz_mittel=FZ->voptfz=FZ->vrselect;
		FZ->voptfz_mittel_resol_change=FZ->voptfz_resol_change=tempW0=FZ->vrselect_resol_change;
		FZ->voptfz_rest=0;
		
	}
	else
	{
	
	/*-----------------------------------------------------------------------------------
	filter_out=7/8 * filter_out	+ filt_ein 
	------------------------------------------------------------------------------------*/
		tempF0=1;
		tempF1=0;
		/* Filter Gain change when wheel recovery state	*/
		
		if((ABS_fz==1)&&(BRAKING_STATE==1)&&(FZ->vrselect_resol_change>FZ->voptfz_resol_change))
		{
			if((STABIL_fl+STABIL_fr+STABIL_rl+STABIL_rr)==1)
			{
				tempF1=1;
			}
			#if	__REAR_D
			else if((STABIL_fl || STABIL_fr)&&((!STABIL_rl)	&& (!STABIL_rr)))
			{
				tempF1=1;
			}
			#else
			else if((STABIL_rl || STABIL_rr)&&((!STABIL_fl)	&& (!STABIL_fr)))
			{
				tempF1=1;
			}				
			#endif
			else 
			{
				;
			}
		  }
		 
		#if __ENGINE_INFORMATION
		if(ldabsu1CanErrFlg==0)
		{
			if((eng_torq_rel>=TORQ_PERCENT_30PRO)||(mtp>=10)) 
			{
				 tempF0=0;
			} 
		}
		else
		{
			tempF0=0;
		}
		#else
		tempF0=0;
		#endif
	
		#if __ENGINE_INFORMATION
        tempF2=0;
        
        	#if ((__4WD_VARIANT_CODE==ENABLE)&&(__AX_SENSOR==ENABLE))
        if((lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H))
        {
        
	        if((WheelAccelG_1 > 0)
	        &&(FZ1.vrselect_resol_change < VREF_5_KPH_RESOL_CHANGE)
	        &&((vrad_diff_max_resol_change < VREF_5_KPH_RESOL_CHANGE )))
			{
				tempF2=1;
			}
	        else
	        {
	        	;
	        }	
		}
		else
		{
		   ;
		}
        	#elif((__4WD == ENABLE) && (__AX_SENSOR == ENABLE))
        if((WheelAccelG_1 > 0)
        &&(FZ1.vrselect_resol_change < VREF_5_KPH_RESOL_CHANGE)
        &&((vrad_diff_max_resol_change < VREF_5_KPH_RESOL_CHANGE )))
		{
			tempF2=1;
		}
        else
        {
        	;
        }		
        	#endif
        
		#endif
		
		tempW2=FZ->filter_out_resol_change;
		tempW0=12;	
		if((tempF0==1)&&(tempF1==1))
		{
			tempW1=9;
		}
		else if(tempF2==1)
		{
			tempW1=8;
		}
		else 
		{
			tempW1=10;		  
		}
		
		FZ->filter_out_resol_change=(int16_t)((((int32_t)tempW1*tempW2)+(((int32_t)(tempW0-tempW1)*8)*FZ->filt_inp))/tempW0);
				
		FZ->filter_out=FZ->filter_out_resol_change/8;
	/*-----------------------------------------------------------------------------------
	 voptfz_rest=((filter_out /	2) + voptfz_rest) mod 358
	------------------------------------------------------------------------------------*/
	
		tempW3=((FZ->filter_out_resol_change) + FZ->voptfz_rest)/358;
		FZ->voptfz_mittel_resol_change+=tempW3;
		FZ->voptfz_rest= (FZ->filter_out_resol_change +	FZ->voptfz_rest) - (tempW3*358);		
		FZ->lss16VrefVehAccel =	(( FZ->filter_out_resol_change)/8) ;		
		FZ->voptfz_mittel=FZ->voptfz_mittel_resol_change/8;
	/*-----------------------------------------------------------------------------------
	  tempW0=voptfz_mittel+filter_out /	8
	------------------------------------------------------------------------------------*/		
		tempW0=FZ->voptfz_mittel_resol_change+(FZ->filter_out);	 /*10ms*/	
	}

	voptfz_new=LCABS_s16LimitMinMax(tempW0,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);
	
	if(fzcase == 1)
	{
		lds16VrefRawSignal = voptfz_new;
	}
/*-----------------------------------------------------------------------------------
	limit the filtered	vehicle	reference speed	increment
------------------------------------------------------------------------------------*/

	if((ABS_fz==1) &&(BRAKING_STATE==1))
	{					  
	 	tempW1=FZ->voptfz_resol_change;
		if(tempW1 < FZ->vrselect_resol_change)	
		{
			tempW1 += MAX_VREF_DEVIAITION;
			if(tempW1 >	voptfz_new)	
			{
				tempW1=voptfz_new;
			}
			else 
			{
				;
			}
			voptfz_new=tempW1;
			if(tempW1 >	FZ->vrselect_resol_change) 
			{
				voptfz_new=FZ->vrselect_resol_change;
			}
			else 
			{
				;
			}
		}
		else 
		{
		   	tempW1=v_afz<<3;
		  #if __REAR_D					   
			if(tempW1 <	FL.vrad_crt_resol_change) 
			{
				tempW1=FL.vrad_crt_resol_change;
			}
			else 
			{
				;
			}		 
			if(tempW1 <	FR.vrad_crt_resol_change) 
			{
				tempW1=FR.vrad_crt_resol_change;
			}
			else 
			{
				;
			}
		  #else
			if(tempW1 <	RL.vrad_crt_resol_change) 
			{
				tempW1=RL.vrad_crt_resol_change;
			}
			else 
			{
				;
			}
			if(tempW1 <	RR.vrad_crt_resol_change)
			{
				tempW1=RR.vrad_crt_resol_change;
			}
			else 
			{
				;
			}
		  #endif		
			tempW1 += VREF_5_KPH_RESOL_CHANGE;
		
			if(voptfz_new >	tempW1)	
			{
				voptfz_new=tempW1; 
			}
			else 
			{
				;
			}
		}
	}
	else 
	{
		;
	}
/*===================================================================================*/
	if(fzcase==1)
	{
		if(	ABS_fz==1 )	
		{
			/*	Unstable Counter */
			if(STABIL_fl==0) 
			{ 
				Unstabil_counter_fl++;	
			}
			else 
			{
				Unstabil_counter_fl=0; 
			}
		
			if(STABIL_fr==0)
			{ 
				Unstabil_counter_fr++;	
			}
			else
			{
				Unstabil_counter_fr=0;
			}
		
			if(Unstabil_counter_fl >= L_U8_CNT_250)	
			{
				Unstabil_counter_fl=L_U8_CNT_250;
			}
			else 
			{
				;
			}
							
			if(Unstabil_counter_fr >= L_U8_CNT_250)	
			{
				Unstabil_counter_fr=L_U8_CNT_250;		
			}
			else 
			{
				;
			}  
	  	}
		else
		{
			Unstabil_counter_fl=0; 
			Unstabil_counter_fr=0;		  
		}			
	}
	else
	{
		;	
	}
}

/*******************************************************************/

#if	__VDC
/*wheel	Speed Allocation Fn*/
void LDABS_vAllocateWheelSpeed(struct W_STRUCT *WL_1,struct	W_STRUCT *WL_2,struct W_STRUCT *WL_3,struct	W_STRUCT *WL_4)
/* Wheel speed Arrange*/
{
#if	!__MY_09_VREF || !__VRSECELT_PRE_YAW_CORRECTION

	wheel_speed_max=WL_1->vrad_crt_resol_change;					
	wheel_speed_2nd=WL_2->vrad_crt_resol_change;					
	wheel_speed_3rd=WL_3->vrad_crt_resol_change;					
	wheel_speed_min=WL_4->vrad_crt_resol_change;					

#else
	wheel_speed_max=WL_1->vrad_crt2; 
	wheel_speed_2nd=WL_2->vrad_crt2; 
	wheel_speed_3rd=WL_3->vrad_crt2; 
	wheel_speed_min=WL_4->vrad_crt2;	
 
#endif	
	RELIABILITY_MAX_WL=WL_1->RELIABLE_FOR_VREF;
	RELIABILITY_2ND_WL=WL_2->RELIABLE_FOR_VREF;
	RELIABILITY_3RD_WL=WL_3->RELIABLE_FOR_VREF;
	RELIABILITY_MIN_WL=WL_4->RELIABLE_FOR_VREF;	
		
}
/*============================================================================*/
void LDABS_vArrangeWheelSpeed(int16_t whl_spd_fl,int16_t whl_spd_fr,int16_t	whl_spd_rl,int16_t whl_spd_rr)
{
	
	wheel_speed_max=0;
	wheel_speed_2nd=0;
	wheel_speed_3rd=0;
	wheel_speed_min=0;	

	RELIABILITY_MAX_WL=0;
	RELIABILITY_2ND_WL=0;
	RELIABILITY_3RD_WL=0;
	RELIABILITY_MIN_WL=0;	
	
	wheel_speed_arrange_flg=1;
	if(whl_spd_fl>=whl_spd_fr)
	{
		if(whl_spd_rl>=whl_spd_rr)
		{
			if(whl_spd_fr>=whl_spd_rl)
			{
				wheel_speed_arrange_flg=1;
				LDABS_vAllocateWheelSpeed(&FL,&FR,&RL,&RR);
				
			}
			else if(whl_spd_rr>=whl_spd_fl)
			{
				wheel_speed_arrange_flg=2;
				LDABS_vAllocateWheelSpeed(&RL,&RR,&FL,&FR);

			}
			else if((whl_spd_rl>=whl_spd_fl)&&(whl_spd_rr>=whl_spd_fr))
			{
				wheel_speed_arrange_flg=3;
				LDABS_vAllocateWheelSpeed(&RL,&FL,&RR,&FR);							
			}
			else if((whl_spd_rl>=whl_spd_fl)&&(whl_spd_fr>=whl_spd_rr))
			{
				wheel_speed_arrange_flg=4;
				LDABS_vAllocateWheelSpeed(&RL,&FL,&FR,&RR);							
			}
			else if((whl_spd_fl>=whl_spd_rl)&&(whl_spd_rr>=whl_spd_fr))
			{
				wheel_speed_arrange_flg=5;
				LDABS_vAllocateWheelSpeed(&FL,&RL,&RR,&FR);			
			}
			else if((whl_spd_fl>=whl_spd_rl)&&(whl_spd_fr>=whl_spd_rr))
			{
				wheel_speed_arrange_flg=6;
				LDABS_vAllocateWheelSpeed(&FL,&RL,&FR,&RR);

			}
			else
			{
				wheel_speed_arrange_flg=7;
			}
		}
		else
		{
			if(whl_spd_fr>=whl_spd_rr)
			{
				wheel_speed_arrange_flg=8;
				LDABS_vAllocateWheelSpeed(&FL,&FR,&RR,&RL);
		
			}
			else if(whl_spd_rl>=whl_spd_fl)
			{
				wheel_speed_arrange_flg=9;
				LDABS_vAllocateWheelSpeed(&RR,&RL,&FL,&FR);

			}
			else if((whl_spd_rr>=whl_spd_fl)&&(whl_spd_rl>=whl_spd_fr))
			{
				wheel_speed_arrange_flg=10;
				LDABS_vAllocateWheelSpeed(&RR,&FL,&RL,&FR);
							
			}
			else if((whl_spd_rr>=whl_spd_fl)&&(whl_spd_fr>=whl_spd_rl))
			{
				wheel_speed_arrange_flg=11;
				LDABS_vAllocateWheelSpeed(&RR,&FL,&FR,&RL);
							
			}
			else if((whl_spd_fl>=whl_spd_rr)&&(whl_spd_rl>=whl_spd_fr))
			{
				wheel_speed_arrange_flg=12;
				LDABS_vAllocateWheelSpeed(&FL,&RR,&RL,&FR);
		
			}
			else if((whl_spd_fl>=whl_spd_rr)&&(whl_spd_fr>=whl_spd_rl))
			{
				wheel_speed_arrange_flg=13;
				LDABS_vAllocateWheelSpeed(&FL,&RR,&FR,&RL);

			}
			else
			{
				wheel_speed_arrange_flg=14;
			}
		}
	}
	else
	{
		if(whl_spd_rl>=whl_spd_rr)
		{
			if(whl_spd_fl>=whl_spd_rl)
			{
				wheel_speed_arrange_flg=15;
				LDABS_vAllocateWheelSpeed(&FR,&FL,&RL,&RR);
			
			}
			else if(whl_spd_rr>=whl_spd_fr)
			{
				wheel_speed_arrange_flg=16;
				LDABS_vAllocateWheelSpeed(&RL,&RR,&FR,&FL);

			}
			else if((whl_spd_rl>=whl_spd_fr)&&(whl_spd_rr>=whl_spd_fl))
			{
				wheel_speed_arrange_flg=17;
				LDABS_vAllocateWheelSpeed(&RL,&FR,&RR,&FL);
								
			}
			else if((whl_spd_rl>=whl_spd_fr)&&(whl_spd_fl>=whl_spd_rr))

			{
				wheel_speed_arrange_flg=18;
				LDABS_vAllocateWheelSpeed(&RL,&FR,&FL,&RR);								
			}
			else if((whl_spd_fr>=whl_spd_rl)&&(whl_spd_rr>=whl_spd_fl))
			{
				wheel_speed_arrange_flg=19;
				LDABS_vAllocateWheelSpeed(&FR,&RL,&RR,&FL);			
			}
			else if((whl_spd_fr>=whl_spd_rl)&&(whl_spd_fl>=whl_spd_rr))
			{
				wheel_speed_arrange_flg=20;
				LDABS_vAllocateWheelSpeed(&FR,&RL,&FL,&RR);
			}
			else
			{
				wheel_speed_arrange_flg=21;
			}
		}
		else
		{
			if(whl_spd_fl>=whl_spd_rr)
			{
				wheel_speed_arrange_flg=22;
				LDABS_vAllocateWheelSpeed(&FR,&FL,&RR,&RL);				
			}
			else if(whl_spd_rl>=whl_spd_fr)
			{
				wheel_speed_arrange_flg=23;
				LDABS_vAllocateWheelSpeed(&RR,&RL,&FR,&FL);
			}
			else if((whl_spd_rr>=whl_spd_fr)&&(whl_spd_rl>=whl_spd_fl))
			{
				wheel_speed_arrange_flg=24;
				LDABS_vAllocateWheelSpeed(&RR,&FR,&RL,&FL);								
			}
			else if((whl_spd_rr>=whl_spd_fr)&&(whl_spd_fl>=whl_spd_rl))
			{
				wheel_speed_arrange_flg=25;
				LDABS_vAllocateWheelSpeed(&RR,&FR,&FL,&RL);								
			}
			else if((whl_spd_fr>=whl_spd_rr)&&(whl_spd_rl>=whl_spd_fl))
			{
				wheel_speed_arrange_flg=26;
				LDABS_vAllocateWheelSpeed(&FR,&RR,&RL,&FL);			
			}
			else if((whl_spd_fr>=whl_spd_rr)&&(whl_spd_fl>=whl_spd_rl))
			{
				wheel_speed_arrange_flg=27;
				LDABS_vAllocateWheelSpeed(&FR,&RR,&FL,&RL);
			}
			else
			{
				wheel_speed_arrange_flg=28;
			}
		}		
	}
	vrad_diff_max_resol_change=wheel_speed_max-wheel_speed_min;
	vrad_diff_max=(vrad_diff_max_resol_change>>3);

}
/*==================================================================*/
void	LDABS_vInformSelectedWheel(uint8_t wheel_selection_mode	)
{
	FL.WHL_SELECT_FLG=0;
	FR.WHL_SELECT_FLG=0;
	RL.WHL_SELECT_FLG=0;
	RR.WHL_SELECT_FLG=0;
	
	if(wheel_selection_mode<20)
	{
		if((wheel_speed_arrange_flg==1)||(wheel_speed_arrange_flg==5)
		||(wheel_speed_arrange_flg==6)||(wheel_speed_arrange_flg==8)
		||(wheel_speed_arrange_flg==12)||(wheel_speed_arrange_flg==13))
		{
			FL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==15)||(wheel_speed_arrange_flg==19)
		||(wheel_speed_arrange_flg==20)||(wheel_speed_arrange_flg==22)
		||(wheel_speed_arrange_flg==26)||(wheel_speed_arrange_flg==27))
		{
			FR.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==2)||(wheel_speed_arrange_flg==3)
		||(wheel_speed_arrange_flg==4)||(wheel_speed_arrange_flg==16)
		||(wheel_speed_arrange_flg==17)||(wheel_speed_arrange_flg==18))
		{
			RL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==9)||(wheel_speed_arrange_flg==10)
		||(wheel_speed_arrange_flg==11)||(wheel_speed_arrange_flg==23)
		||(wheel_speed_arrange_flg==24)||(wheel_speed_arrange_flg==25))
		{
			RR.WHL_SELECT_FLG=1;
		}						
	}
	else if(wheel_selection_mode<30)
	{
		if((wheel_speed_arrange_flg==3)||(wheel_speed_arrange_flg==4)
		||(wheel_speed_arrange_flg==10)||(wheel_speed_arrange_flg==11)
		||(wheel_speed_arrange_flg==15)||(wheel_speed_arrange_flg==22))
		{
			FL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==1)||(wheel_speed_arrange_flg==8)
		||(wheel_speed_arrange_flg==17)||(wheel_speed_arrange_flg==24)
		||(wheel_speed_arrange_flg==25)||(wheel_speed_arrange_flg==18))
		{
			FR.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==5)||(wheel_speed_arrange_flg==6)
		||(wheel_speed_arrange_flg==9)||(wheel_speed_arrange_flg==19)
		||(wheel_speed_arrange_flg==20)||(wheel_speed_arrange_flg==23))
		{
			RL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==2)||(wheel_speed_arrange_flg==12)
		||(wheel_speed_arrange_flg==13)||(wheel_speed_arrange_flg==16)
		||(wheel_speed_arrange_flg==26)||(wheel_speed_arrange_flg==27))
		{
			RR.WHL_SELECT_FLG=1;
		}			
	}
	else if(wheel_selection_mode<40)
	{
		if((wheel_speed_arrange_flg==2)||(wheel_speed_arrange_flg==9)
		||(wheel_speed_arrange_flg==18)||(wheel_speed_arrange_flg==20)
		||(wheel_speed_arrange_flg==25)||(wheel_speed_arrange_flg==27))
		{
			FL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==4)||(wheel_speed_arrange_flg==6)
		||(wheel_speed_arrange_flg==11)||(wheel_speed_arrange_flg==13)
		||(wheel_speed_arrange_flg==16)||(wheel_speed_arrange_flg==23))
		{
			FR.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==1)||(wheel_speed_arrange_flg==10)
		||(wheel_speed_arrange_flg==12)||(wheel_speed_arrange_flg==15)
		||(wheel_speed_arrange_flg==24)||(wheel_speed_arrange_flg==26))
		{
			RL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==3)||(wheel_speed_arrange_flg==5)
		||(wheel_speed_arrange_flg==8)||(wheel_speed_arrange_flg==17)
		||(wheel_speed_arrange_flg==19)||(wheel_speed_arrange_flg==22))
		{
			RR.WHL_SELECT_FLG=1;
		}			
	}
	else if(wheel_selection_mode<50)
	{
		if((wheel_speed_arrange_flg==16)||(wheel_speed_arrange_flg==17)
		||(wheel_speed_arrange_flg==19)||(wheel_speed_arrange_flg==23)
		||(wheel_speed_arrange_flg==24)||(wheel_speed_arrange_flg==26))
		{
			FL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==2)||(wheel_speed_arrange_flg==3)
		||(wheel_speed_arrange_flg==5)||(wheel_speed_arrange_flg==9)
		||(wheel_speed_arrange_flg==10)||(wheel_speed_arrange_flg==12))
		{
			FR.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==8)||(wheel_speed_arrange_flg==11)
		||(wheel_speed_arrange_flg==13)||(wheel_speed_arrange_flg==22)
		||(wheel_speed_arrange_flg==25)||(wheel_speed_arrange_flg==27))
		{
			RL.WHL_SELECT_FLG=1;
		}
		else if((wheel_speed_arrange_flg==1)||(wheel_speed_arrange_flg==4)
		||(wheel_speed_arrange_flg==6)||(wheel_speed_arrange_flg==15)
		||(wheel_speed_arrange_flg==20)||(wheel_speed_arrange_flg==18))
		{
			RR.WHL_SELECT_FLG=1;
		}			
	}
	else if(wheel_selection_mode<60)
	{
	  #if __REAR_D

		if(FL.vrad_crt_resol_change>FR.vrad_crt_resol_change)
		{
			FL.WHL_SELECT_FLG=1;
		}
		else
		{
			FR.WHL_SELECT_FLG=1;
		}

	  #else
		if(RL.vrad_crt_resol_change>RR.vrad_crt_resol_change)
		{
			RL.WHL_SELECT_FLG=1;
		}
		else
		{
			RR.WHL_SELECT_FLG=1;
		}
	  
	  #endif
	}
	else
	{
		;
	}
	if(	FL.WHL_SELECT_FLG==1 )
	{
		arad_select_wheel=arad_fl ;
	}
	else if( FR.WHL_SELECT_FLG==1 )
	{
		arad_select_wheel=arad_fr ;
	}
	else if( RL.WHL_SELECT_FLG==1 )
	{
		arad_select_wheel=arad_rl ;		
	}
	else
	{
		arad_select_wheel=arad_rr ;
	}			
}
/*=============================================================================*/
void LDABS_vCompRefWheelForESP(void)
{	
#if	__MY_09_VREF &&	__VRSECELT_PRE_YAW_CORRECTION
	int16_t		calculated_yaw_correction;
	int16_t		wstr_correction;
	int16_t	whl_spd_fl;
	int16_t	whl_spd_fr;
	int16_t	whl_spd_rl;
	int16_t	whl_spd_rr;
	int16_t	whl_spd_max;
	int16_t	whl_spd_min;

	calculated_yaw_correction=yaw_c_vref_resol_change;
/*
	tempW2=abs_wstr-WSTR_250_DEG;
	tempW0=52; 
	tempW1=1;
	s16muls16divs16();
*/	
	tempW3=(abs_wstr-WSTR_250_DEG)/52;
	if(tempW3>VREF_2_KPH_RESOL_CHANGE)
	{
		tempW3=VREF_2_KPH_RESOL_CHANGE;
	}
	wstr_correction=tempW3+(calculated_yaw_correction/3);
	whl_spd_fl=FL.vrad_crt_resol_change;
	whl_spd_fr=FR.vrad_crt_resol_change;
	whl_spd_rl=RL.vrad_crt_resol_change;
	whl_spd_rr=RR.vrad_crt_resol_change;	
	

	
	whl_spd_max=whl_spd_fl;
	if(whl_spd_max<whl_spd_fr)
	{
		whl_spd_max=whl_spd_fr;
	}
	if(whl_spd_max<whl_spd_rl)
	{
		whl_spd_max=whl_spd_rl;
	}
	if(whl_spd_max<whl_spd_rr)
	{
		whl_spd_max=whl_spd_rr;
	}
	
	whl_spd_min=whl_spd_fl;
	if(whl_spd_min>whl_spd_fr)
	{
		whl_spd_min=whl_spd_fr;
	}
	if(whl_spd_min>whl_spd_rl)
	{
		whl_spd_min=whl_spd_rl;
	}
	if(whl_spd_min>whl_spd_rr)
	{
		whl_spd_min=whl_spd_rr;
	}
	
	if(ESP_SENSOR_RELIABLE_FLG==1)
	{		
		if(((yaw_out>0)&&(BACK_DIR==0))||((yaw_out<0)&&(BACK_DIR==1)))
		{
			FL.vrad_crt2=whl_spd_fl+2*calculated_yaw_correction;
			FR.vrad_crt2=whl_spd_fr;
			RL.vrad_crt2=whl_spd_rl+2*calculated_yaw_correction;
			RR.vrad_crt2=whl_spd_rr;		
	
		}
		else
		{
			FL.vrad_crt2=whl_spd_fl;
			FR.vrad_crt2=whl_spd_fr+2*calculated_yaw_correction;
			RL.vrad_crt2=whl_spd_rl;
			RR.vrad_crt2=whl_spd_rr+2*calculated_yaw_correction;	
		}
		
		if((SAS_CHECK_OK==1)&&(abs_wstr>WSTR_250_DEG)&&(vref<VREF_30_KPH)&&(vref>VREF_4_KPH))
		{
			if(FL.vrad_crt2>RL.vrad_crt2)
			{
				FL.vrad_crt2-=wstr_correction;
				if(FL.vrad_crt2<RL.vrad_crt2)
				{
					FL.vrad_crt2=RL.vrad_crt2;
				}
			}
			
	
			if(FR.vrad_crt2>RR.vrad_crt2)
			{
				FR.vrad_crt2-=wstr_correction;
				if(FR.vrad_crt2<RR.vrad_crt2)
				{
					FR.vrad_crt2=RR.vrad_crt2;
				}						
			}	
			
		}	
	}
	else
	{
		FL.vrad_crt2=whl_spd_fl;
		FR.vrad_crt2=whl_spd_fr;
		RL.vrad_crt2=whl_spd_rl;
		RR.vrad_crt2=whl_spd_rr;		
	}
	if(FL.vrad_crt2>whl_spd_max)
	{
		FL.vrad_crt2=whl_spd_max;
	}
	else if(FL.vrad_crt2<whl_spd_min)
	{
		FL.vrad_crt2=whl_spd_min;
	}
	else
	{
		;
	}
	
	if(FR.vrad_crt2>whl_spd_max)
	{
		FR.vrad_crt2=whl_spd_max;
	}
	else if(FR.vrad_crt2<whl_spd_min)
	{
		FR.vrad_crt2=whl_spd_min;
	}
	else
	{
		;
	}
	
	if(RL.vrad_crt2>whl_spd_max)
	{
		RL.vrad_crt2=whl_spd_max;
	}
	else if(RL.vrad_crt2<whl_spd_min)
	{
		RL.vrad_crt2=whl_spd_min;
	}
	else
	{
		;
	}
	
	if(RR.vrad_crt2>whl_spd_max)
	{
		RR.vrad_crt2=whl_spd_max;
	}
	else if(RR.vrad_crt2<whl_spd_min)
	{
		RR.vrad_crt2=whl_spd_min;
	}
	else
	{
		;
	}			
#else

	#define	WHEEL_DIFF_THR	VREF_0_5_KPH_RESOL_CHANGE

	

	int16_t		yaw_correction_tmp;
	int16_t		selected_whl_speed_1;
	int16_t		selected_whl_speed_2;
	int16_t		THETA;

	yaw_correction_tmp=yaw_c_vref_resol_change;
	selected_whl_speed_1=FZ1.vrselect_resol_change;
	selected_whl_speed_2=FZ2.vrselect_resol_change;

/*-----------------------------------------------
	V_x	= V_str	* cos(x)
				 1			1
	cos(x) = 1 - - * x^2 + -- *	x^4	- ...
				 2		   24 
	if x=30deg.	 Max. Error	= 3.7%			  
------------------------------------------------*/

	/*	 THETA = (int16_t)(((int32_t)abs_wstr*1000)/((int32_t)(pEspModel->S16_STEERING_RATIO_10)*573));*/
	/* opttime : ksw*/
	tempW0 = (int16_t)(((int32_t)(pEspModel->S16_STEERING_RATIO_10)*573)/10);

	THETA =	(int16_t)( ((int32_t)abs_wstr*100)/tempW0 );
	
	/*tempW2=	(int16_t)(((((int32_t)vref_resol_change*THETA)*THETA)/2)/10000);*/	  
	/* opttime : ksw*/
	
	tempW2=	  (int16_t)((((int32_t)vref_resol_change*THETA)*THETA)/20000);
	
	if(	tempW2 >= VREF_4_KPH_RESOL_CHANGE )
	{
		wstr_correction	= VREF_4_KPH_RESOL_CHANGE;		
	}
	else
	{
		wstr_correction	= tempW2;			
	}
	
	
/*	vrsel_crt_flg=0; */
	YAW_CORRECTIOIN_PERFORMED=0;
	if(ESP_SENSOR_RELIABLE_FLG==1)
	{	
/*		vrsel_crt_flg=1; */
		if(((yaw_out>0)&&(BACK_DIR==0))||((yaw_out<0)&&(BACK_DIR==1)))		
		{	
/*			vrsel_crt_flg=2; */
			/*Yaw Correction : When	Inside Wheel Selected*/
			if((FL.WHL_SELECT_FLG==1)||(RL.WHL_SELECT_FLG==1))
			{
/*				vrsel_crt_flg=3; */
				if((ESP_BRAKE_CONTROL_FR==1)&&(ESP_BRAKE_CONTROL_RR==1))
				{
					tempW1=wheel_speed_max;
/*					vrsel_crt_flg=4; */
				}
				else if((ESP_BRAKE_CONTROL_FR==1)&&(FL.WHL_SELECT_FLG==1))
				{
					tempW1=RR.vrad_crt_resol_change;
/*					vrsel_crt_flg=5; */
				}
				else if((ESP_BRAKE_CONTROL_RR==1)&&(RL.WHL_SELECT_FLG==1))
				{
					tempW1=FR.vrad_crt_resol_change;
/*					vrsel_crt_flg=6; */

				}				
				else if(FL.WHL_SELECT_FLG==1)
				{
					tempW1=FR.vrad_crt_resol_change;
/*					vrsel_crt_flg=7; */
				}		
				else if	(RL.WHL_SELECT_FLG==1)
				{
					tempW1=RR.vrad_crt_resol_change;
/*					vrsel_crt_flg=8;			*/		
				}
				else
				{
					;
				}
				if(selected_whl_speed_1>=tempW1)
				{
					YAW_CORRECTIOIN_PERFORMED=0;
				}
				else
				{
					YAW_CORRECTIOIN_PERFORMED=1;
				}					
			}
			else
			{
/*				vrsel_crt_flg=8; */
			}
		}
		else
		{
/*			vrsel_crt_flg=9; */
			/*Yaw Correction : When	Inside Wheel Selected*/
			if((FR.WHL_SELECT_FLG==1)||(RR.WHL_SELECT_FLG==1))
			{
/*				vrsel_crt_flg=10; */
				if((ESP_BRAKE_CONTROL_FL==1)&&(ESP_BRAKE_CONTROL_RL==1))
				{
					tempW1=wheel_speed_max;
/*					vrsel_crt_flg=11; */
				}
				else if((ESP_BRAKE_CONTROL_FL==1)&&(FR.WHL_SELECT_FLG==1))
				{
					tempW1=RL.vrad_crt_resol_change;
/*					vrsel_crt_flg=13; */
				}
				else if((ESP_BRAKE_CONTROL_RL==1)&&(RR.WHL_SELECT_FLG==1))
				{
					tempW1=FL.vrad_crt_resol_change;
/*					vrsel_crt_flg=12; */

				}
				else if(FR.WHL_SELECT_FLG==1)
				{
					tempW1=FL.vrad_crt_resol_change;
/*					vrsel_crt_flg=14;  */
				}		
				else if	(RR.WHL_SELECT_FLG==1)
				{
					tempW1=RL.vrad_crt_resol_change;
/*					vrsel_crt_flg=15;		*/			
				}
				else
				{
					;
				}
				if(selected_whl_speed_1>=tempW1)
				{
					YAW_CORRECTIOIN_PERFORMED=0;
				}
				else
				{
					YAW_CORRECTIOIN_PERFORMED=1;
				}				
			}	
			else
			{
/*				vrsel_crt_flg=16;  */
			}		
		}
		if(YAW_CORRECTIOIN_PERFORMED==1)
		{
			selected_whl_speed_1+=2*yaw_correction_tmp;
/*			vrsel_crt_flg=50;  */
			if(selected_whl_speed_1>tempW1)
			{
				selected_whl_speed_1=tempW1;
/*				vrsel_crt_flg=51; */
			}		
			
		}	
		/*Wstr Correction :	When Front Wheel Selected*/
		if((FL.WHL_SELECT_FLG==1)||(FR.WHL_SELECT_FLG==1))
		{
			if(((yaw_out>0)&&(BACK_DIR==0))||((yaw_out<0)&&(BACK_DIR==1)))
			{	
				if((ESP_BRAKE_CONTROL_RL==1)&&(ESP_BRAKE_CONTROL_RR==1))
				{
					tempW2=wheel_speed_max;
				}
				else if(ESP_BRAKE_CONTROL_RL==1)
				{
					tempW2=RR.vrad_crt_resol_change;
				}
				else if(ESP_BRAKE_CONTROL_RR==1)
				{
					tempW2=RL.vrad_crt_resol_change+(2*yaw_correction_tmp);
				}
				else
				{
					tempW2=RR.vrad_crt_resol_change;
					if(RR.vrad_crt_resol_change>(RL.vrad_crt_resol_change+(2*yaw_correction_tmp)))
					{
						tempW2=(RL.vrad_crt_resol_change+(2*yaw_correction_tmp));
					}
					else
					{
						;
					}	
				}
				
			}
			else
			{
				if((ESP_BRAKE_CONTROL_RL==1)&&(ESP_BRAKE_CONTROL_RR==1))
				{
					tempW2=wheel_speed_max;
				}
				else if(ESP_BRAKE_CONTROL_RL==1)
				{
					tempW2=RR.vrad_crt_resol_change+(2*yaw_correction_tmp);
				}
				else if(ESP_BRAKE_CONTROL_RR==1)
				{
					tempW2=RL.vrad_crt_resol_change;
				}
				else
				{
					tempW2=RL.vrad_crt_resol_change;
					if(RL.vrad_crt_resol_change>(RR.vrad_crt_resol_change+(2*yaw_correction_tmp)))
					{
						tempW2=(RR.vrad_crt_resol_change+(2*yaw_correction_tmp));
					}
					else
					{
						;
					}	
				}				
			}
			if(selected_whl_speed_1>=tempW2)
			{
				selected_whl_speed_1-=wstr_correction;
				if(selected_whl_speed_1<tempW2)
				{
					selected_whl_speed_1=tempW2;
				}
			}
			else
			{
				;
			}	
		}		
	}
	else
	{
		;
	}
	
	FZ1.vrselect_resol_change=selected_whl_speed_1;
	FZ2.vrselect_resol_change=selected_whl_speed_2;
	FZ1.vrselect=(FZ1.vrselect_resol_change>>3);
	FZ2.vrselect=(FZ2.vrselect_resol_change>>3);
#endif	
}
#else /*__VDC*/
/*=======================================================*/
void LDABS_vArrangeWheelSpeed2(void)
{
	int16_t	whl_spd_fl;
	int16_t	whl_spd_fr;
	int16_t	whl_spd_rl;
	int16_t	whl_spd_rr;

	U8_BIT_STRUCT_t	TEMP;

	#define	FRONT_MAX	TEMP.bit7
	#define	FRONT_MIN	TEMP.bit6
	#define	REAR_MAX	TEMP.bit5
	#define	REAR_MIN	TEMP.bit4
	#define	not_used_temp1	TEMP.bit3
	#define	not_used_temp2	TEMP.bit2
	#define	not_used_temp3	TEMP.bit1
	#define	not_used_temp4	TEMP.bit0

	whl_spd_fl=FL.vrad_crt_resol_change;
	whl_spd_fr=FR.vrad_crt_resol_change;
	whl_spd_rl=RL.vrad_crt_resol_change;
	whl_spd_rr=RR.vrad_crt_resol_change;

	
	if(whl_spd_fl>whl_spd_fr)
	{
		tempW0=whl_spd_fl;
		tempW1=whl_spd_fr;
		FRONT_MAX=FL.RELIABLE_FOR_VREF;
		FRONT_MIN=FR.RELIABLE_FOR_VREF;		
	}
	else
	{
		tempW0=whl_spd_fr;
		tempW1=whl_spd_fl;
		FRONT_MAX=FR.RELIABLE_FOR_VREF;
		FRONT_MIN=FL.RELIABLE_FOR_VREF;			
	}
	
	if(whl_spd_rl>whl_spd_rr)
	{
		tempW2=whl_spd_rl;
		tempW3=whl_spd_rr;
		REAR_MAX=RL.RELIABLE_FOR_VREF;
		REAR_MIN=RR.RELIABLE_FOR_VREF;			
	}
	else 
	{
		tempW2=whl_spd_rr;
		tempW3=whl_spd_rl;
		REAR_MAX=RR.RELIABLE_FOR_VREF;
		REAR_MIN=RL.RELIABLE_FOR_VREF;		
	}
	
	if(tempW0>tempW2) 
	{
		wheel_speed_max=tempW0;
		RELIABILITY_MAX_WL=FRONT_MAX;
		if(tempW1>tempW2) 
		{
			wheel_speed_2nd=tempW1;
			wheel_speed_3rd=tempW2;
			wheel_speed_min=tempW3;
			
			RELIABILITY_2ND_WL=FRONT_MIN;
			RELIABILITY_3RD_WL=REAR_MAX;
			RELIABILITY_MIN_WL=REAR_MIN;
		}
		else 
		{
			wheel_speed_2nd=tempW2;
			RELIABILITY_2ND_WL=REAR_MAX;
			if(tempW1>tempW3)
			{
				wheel_speed_3rd=tempW1;
				wheel_speed_min=tempW3;
				RELIABILITY_3RD_WL=FRONT_MIN;
				RELIABILITY_MIN_WL=REAR_MIN;				
			}
			else 
			{
				wheel_speed_3rd=tempW3;
				wheel_speed_min=tempW1;
				RELIABILITY_3RD_WL=REAR_MIN;
				RELIABILITY_MIN_WL=FRONT_MIN;					
			}
		}
	}
	else 
	{
		wheel_speed_max=tempW2;
		RELIABILITY_MAX_WL=REAR_MAX;
		if(tempW0>tempW3) 
		{
			wheel_speed_2nd=tempW0;
			RELIABILITY_2ND_WL=FRONT_MAX;
			if(tempW1>tempW3) 
			{
				wheel_speed_3rd=tempW1;
				wheel_speed_min=tempW3;
				RELIABILITY_3RD_WL=FRONT_MIN;
				RELIABILITY_MIN_WL=REAR_MIN;				
			}
			else 
			{
				wheel_speed_3rd=tempW3;
				wheel_speed_min=tempW1;
				RELIABILITY_3RD_WL=REAR_MIN;
				RELIABILITY_MIN_WL=FRONT_MIN;					
			}
		}
		else 
		{
			wheel_speed_2nd=tempW3;
			wheel_speed_3rd=tempW0;
			wheel_speed_min=tempW1;
			RELIABILITY_2ND_WL=REAR_MIN;
			RELIABILITY_3RD_WL=FRONT_MAX;
			RELIABILITY_MIN_WL=FRONT_MIN;			
		}
	}	
}
/*=======================================================*/
void LDABS_vInformSelectedWheel(void)
{
	int16_t	wheel_speed_fl;
	int16_t	wheel_speed_fr;
	int16_t	wheel_speed_rl;
	int16_t	wheel_speed_rr;
	int16_t	selected_wheel_speed;
	
	FL.WHL_SELECT_FLG=0;
	FR.WHL_SELECT_FLG=0;
	RL.WHL_SELECT_FLG=0;
	RR.WHL_SELECT_FLG=0;

	wheel_speed_fl=FL.vrad_crt_resol_change;
	wheel_speed_fr=FR.vrad_crt_resol_change;
	wheel_speed_rl=RL.vrad_crt_resol_change;
	wheel_speed_rr=RR.vrad_crt_resol_change;
	selected_wheel_speed=FZ1.vrselect_resol_change;

	#if	__REAR_D
	if(selected_wheel_speed==wheel_speed_fl)
	{
		FL.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_fl ;
	}
	else if(selected_wheel_speed==wheel_speed_fr)
	{
		FR.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_fr ;
	}
	else if(selected_wheel_speed==wheel_speed_rl)
	{
		RL.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_rl ;
	}
	else if(selected_wheel_speed==wheel_speed_rr)
	{
		RR.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_rr ;
	}
	else 
	{
		;
	}
	#else
	if(selected_wheel_speed==wheel_speed_rl)
	{
		RL.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_rl ;
	}
	else if(selected_wheel_speed==wheel_speed_rr)
	{
		RR.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_rr ;
	}
	else if(selected_wheel_speed==wheel_speed_fl)
	{
		FL.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_fl ;
	}
	else if(selected_wheel_speed==wheel_speed_fr)
	{
		FR.WHL_SELECT_FLG=1;
		arad_select_wheel=arad_fr ;
	}
	else 
	{
		;
	}	
	#endif

}
/*=======================================================*/
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallVrefMainFilterProcess
	#include "Mdyn_autosar.h"
#endif
