/*******************************************************************
* Project Name:
* File Name:
* Description:
********************************************************************/
/* Includes	********************************************************/
#if !SIM_MATLAB

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallDctForVref
	#include "Mdyn_autosar.h"
#endif
#endif

#include	"LDESPEstBeta.h"
#include	"LCABSDecideCtrlState.h"
#include	"LDRTACallDetection.h"
/*#include	"../system/WMonPort.h"*/
#include 	"LCallMain.H"
#include 	"LSABSCallSensorSignal.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#include 	"LCABSCallControl.h"
#include    "LSCANSignalInterface.h"
#include    "LSABSCall2ndOrderLpf.h"
#if	__VDC
#include 	"LDABSCallVrefCompForESP.H"
#include 	"LCESPInterfaceSlipController.h"
#include 	"LCESPCalLpf.h"
#include    "LCESPCalInterpolation.h"
#endif



#if	(__EPB_INTERFACE == ENABLE)
#include 	"LCEPBCallControl.H"
#endif



/*backward driving detection for esc control inhibition*/


/* Local Definition	 ***********************************************/


#define		WHEEL_RELIABLE_THR_BRAKE_ENTRY	VREF_1_KPH_RESOL_CHANGE
#define		WHEEL_RELIABLE_THR_ABS2ACCEL	VREF_2_KPH_RESOL_CHANGE
#define		WHEEL_RELIABLE_THR_ABS2ACCEL_2	VREF_3_KPH_RESOL_CHANGE
#define		ABS2ACCEL_DET_ACCEL_THR			AFZ_0G5
#define		ABS2ACCEL_DET_ACCEL_THR2		AFZ_1G0
#define		ABS2ACCEL_DET_ARAD_THR			ARAD_0G5
	#if	__VDC
#define		WHEEL_RELIABLE_THR_ESP			VREF_2_KPH_RESOL_CHANGE
#define		ESP_RELIABILITY_SET_DECEL		-ARAD_0G75
#define		WHEEL_RELIABLE_THR_ESP_CNT		VREF_2_KPH_RESOL_CHANGE
#define		WHEEL_RELIABLE_THR_ESP_CNT_2	VREF_2_KPH_RESOL_CHANGE
#define		WHEEL_RELIABLE_THR_ESP_CNT_3	VREF_3_KPH_RESOL_CHANGE
#define		ESP_CNT_RELIABILITY_SET_ACCEL_2	ARAD_1G0
#define		ESP_CNT_RELIABILITY_SET_ACCEL_1	ARAD_0G5
	#endif
#define		WHEEL_SPIN_DET_THR				VREF_0_5_KPH_RESOL_CHANGE
#define		WHEEL_SPIN_DET_THR_2			VREF_1_KPH_RESOL_CHANGE
#define		WHEEL_SPIN_DET_THR_3			VREF_2_KPH_RESOL_CHANGE
#define		WHEEL_DRAG_DET_THR				-VREF_2_KPH_RESOL_CHANGE
#define		WHEEL_DRAG_DET_THR2				-VREF_3_KPH_RESOL_CHANGE
#define		WHEEL_DRAG_DET_THR3				-VREF_5_KPH_RESOL_CHANGE
#define		THRES_FOR_NON_DRV_WL_CHECK		VREF_2_KPH_RESOL_CHANGE
#define		THRES_FOR_NON_DRV_WL_CHECK2		VREF_1_KPH_RESOL_CHANGE
#define		THRES_FOR_NON_DRV_WL_CHECK3		VREF_1_5_KPH_RESOL_CHANGE
#define		WHL_DIF_RELIABLE_THR			VREF_1_KPH_RESOL_CHANGE
#define		WHL_RELIABLE_THR				VREF_0_5_KPH_RESOL_CHANGE
#define		WHL_RELIABLE_THR2				VREF_2_KPH_RESOL_CHANGE


#define		VRSELECT_DV_THRES2		64
#define		VRSELECT_DV_THRES4		128
#define		SPIN_CLEAR_DV_THRES3	16

#define		SPIN_DCT_DV_THR1	VREF_0_75_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR2	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR3	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR4	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR5	0
#define		SPIN_DCT_DV_THR6	VREF_0_75_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR7	VREF_0_75_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR8	VREF_0_25_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR9	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR10	VREF_2_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR11	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR12	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR13	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR14	VREF_4_KPH_RESOL_CHANGE
#define		HPF_SET_G				10	/* 0.1g	 */
#define		HPF_SET_G_2				10
#define		HPF_SET_G_3				30
#define		HPF_SET_G_4				15
#define		HPF_RESET_G				10
#define		HPF_RESET_G_2			10
#define		SPIN_DCT_RESET_G		5	/* 0.05g */
#define		SPIN_RESET_G			30
#define		SPIN_DEC_FACTOR			2
#define		SPIN_SET_DSPEED		VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_SET_DSPEED2	VREF_1_5_KPH_RESOL_CHANGE
#define		SPIN_SET_TIME		L_U8_TIME_10MSLOOP_70MS
#define		SPIN_SET_TIME_2		L_U8_TIME_10MSLOOP_100MS
#define		SPIN_SET_TIME_3		L_U8_TIME_10MSLOOP_350MS
#define		SPIN_CLR_DV_THR1	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR2	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR3	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR4	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR5	VREF_0_25_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR6	VREF_0_25_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR7	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR8	VREF_2_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR9	VREF_1_KPH_RESOL_CHANGE
#define		SPIN_CLR_DV_THR10	VREF_1_KPH_RESOL_CHANGE
#define		BIG_SPIN_SET_DV_THR1	0
#define		BIG_SPIN_SET_DV_THR2	VREF_0_5_KPH_RESOL_CHANGE
#define		BIG_SPIN_SET_DV_THR3	VREF_2_KPH_RESOL_CHANGE
#define		BIG_SPIN_SET_DV_THR4	VREF_1_KPH_RESOL_CHANGE
#define		BIG_SPIN_CLR_DV_THR1	VREF_0_25_KPH_RESOL_CHANGE
#define		BIG_SPIN_CLR_DV_THR2	VREF_0_5_KPH_RESOL_CHANGE
#define		SPIN_RESET_TIME			L_U8_TIME_10MSLOOP_100MS
#define		SPIN_RESET_TIME2		L_U8_TIME_10MSLOOP_1500MS
#define		BIG_SPIN_SET_TIME		L_U8_TIME_10MSLOOP_100MS
#define		BIG_SPIN_RESET_TIME		L_U8_TIME_10MSLOOP_100MS
#define		BIG_SPIN_SET_G			20
#define		BIG_SPIN_SET_G_2		20
#define		SPIN_DCT_DV_THR_ON_BRK		VREF_1_KPH_RESOL_CHANGE
#define		SPIN_DCT_DV_THR_ON_BRK2		VREF_2_KPH_RESOL_CHANGE
#define		SPIN_SET_TIME_ON_BRK		L_U8_TIME_10MSLOOP_500MS
#define		SPIN_SET_VREF_ACCEL_G_1		15
#define		SPIN_SET_VRAD_DIFF_1		VREF_5_KPH_RESOL_CHANGE
#define		WHL_ACCEL_THR_MIN_WHL_SEL	AFZ_0G1
#define     MULTI                       10

#define	VEH_ACCEL_THR_MIN_WHL_SEL			AFZ_0G2*10

#define	WHL_ACCEL_THR_MIN_WHL_SEL_CLEAR		-AFZ_0G1

#define	VEH_ACCEL_THR_SPIN_SUS_DCT			AFZ_0G15*10

/*Backward driving detection for esc control inhibition*/
#if	__VDC
	
#define  REVERSE_DRV_CHK_YAWDIFF     YAW_10DEG   
#define  REVERSE_DRV_RECHK_YAWDIFF   YAW_3DEG   
#define  REVERSE_DRV_RECHK_TIME      L_U16_TIME_10MSLOOP_3S
#define  REVERSE_DRV_RECHK_TIME2     L_U8_TIME_10MSLOOP_100MS
#define  REVERSE_DRV_RECHK_YAW       YAW_5DEG                             
#define  REVERSE_DRV_DEL_YAW_DIFF    (-YAW_0G05DEG)  /* 1 deg/s 1scan -> 142 deg/s^2  */
	#if __R_GEAR_INFORM_FOR_MT ==1
	#define U8_RGEAR_ERROR_CHK_TIME		L_U8_TIME_10MSLOOP_150MS
	#endif

#endif



/* Variables Definition*********************************************/
U8_BIT_STRUCT_t	   VDCTF0,VDCTF4;
#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))
U8_BIT_STRUCT_t	   VDCTF1;
U8_BIT_STRUCT_t    VDCTF5;
#endif

#if	__AX_SENSOR
U8_BIT_STRUCT_t	   VDCTF2;
#endif

#if	__VDC
U8_BIT_STRUCT_t	   VDCTF3;
U8_BIT_STRUCT_t    VDCTF6;
#endif



#if	 (__4WD	 ||(__4WD_VARIANT_CODE==ENABLE))	&& __AX_SENSOR 
	#if	__VDC
int16_t		along_by_drift;
	#endif
int16_t		along_vref;
#endif
int16_t		ABS_entry_speed;
uint8_t 	abs_duration_timer;	
	

#if	 (__4WD	 ||(__4WD_VARIANT_CODE==ENABLE))	
	#if	__AX_SENSOR	
/*LDABS_vDetVehicleAccelSpin*/
int16_t		vdiff_by_along_1;
int16_t		vdiff_by_along_2;
int16_t		vdiff_by_along_3;
int16_t		vdiff_by_along_4;
int16_t		vdiff_by_along_5;
int16_t		vdiff_by_along_6;
int16_t		vdiff_by_along_7;
int16_t		vdiff_by_along_8;	
int16_t		vdiff_by_along_mod;
int16_t		vref_along_1;
int16_t		vref_along_2;
int16_t		vref_along_3;
int16_t		vref_along_4;
int16_t		vref_along_5;
int16_t		vref_along_6;
int16_t		vref_along_7;
int16_t		vref_along_8;	
int16_t		vref_by_along;
int16_t		vdiff_by_along;
uint8_t		accel_det_flags;
uint8_t		accel_spin_det_cnt1;
uint8_t		accel_spin_det_cnt2;
int16_t		along_vref_sum;

int16_t	filter_out_new_1;
int16_t	filter_out_new_rest_1;
int16_t	voptfz_mittel_new_1;
int16_t	WheelAccelG_1;
int16_t	WheelAccelG_1_old;

int16_t	lds16HPFWheelAccel;
int16_t	lds16HPFLongG;
int16_t	lds16VrefDelta;

int8_t	lds8SpinSetCount;
int8_t	lds8SpinSetCount_2;
int8_t	lds8SpinSetCount_3;
uint8_t	lds8SpinClearCnt;
int8_t	lds8BigSpinSetCnt;
int8_t	lds8BigSpinClearCnt;

int16_t lds16SlowestWhlSelDetCnt;
int16_t	lds16WhlSpinSusClearCnt;
int16_t	lds16WhlSpinSusClearCnt2;
int16_t	lds16SpinSusCountforVcaReq;
int16_t	lds16AllWhlSpinCntforVcaReq;
int16_t lds16AccelSpinSustainCnt;

  int16_t   lds16vdiffbyalongmod;
  int16_t   lds16vdiffbyalong;
  int16_t   lds16vrefbyalong;
  int16_t   lds16vrefbyalongold;

  int16_t   lds16SpinDctDvThr0;
  int16_t   lds16SpinDctDvThr1;
  int16_t   lds16SpinDctDvThr2;
  int16_t   lds16SpinDctDvThr3;
  int16_t   lds16SpinDctDvThr4;
  int16_t   lds16SpinDctDvThr5;

  uint8_t ldu8nospinsetcnt;
  uint8_t ldu8nospinresetcnt;
	#endif
int16_t	lds8BigSpinHoldCount;
#endif

#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE))&&	(__AX_SENSOR==1)
/* LDABS_vChk4WDVrefEstError*/
uint8_t	vehicle_free_rolling_suspect_cnt1;
uint8_t	vehicle_free_rolling_suspect_cnt2;

uint8_t		vref_fail_mode_cnt;
uint16_t	control_fail_count;
uint8_t		count_ice;
uint8_t		vref_fail_count;
uint8_t		vref_fail_count_afz;
uint8_t		vref_fail_dump_count_fl;
uint8_t		vref_fail_dump_count_fr;
uint8_t		vref_fail_dump_count_rl;
uint8_t		vref_fail_dump_count_rr;
#if	__VDC
int8_t		vref_fail_cnt_on_esp;
#endif
/* LDABS_vLimitLongAccInput	*/
#endif

#if	 __AX_SENSOR
/*LDABS_vDctBackDrivingByLongAcc*/
int16_t		grv_diff_cnt;
int16_t		standstill_count;
int16_t		along_standstill, di_along;
#endif

int16_t		ABS_entry_speed2;
#if	__VDC
int16_t		ESP_entry_speed;
#endif

#if	__ACCEL_ON_BRK_VREF
#if	__4WD || (__4WD_VARIANT_CODE==ENABLE)
uint8_t		ldvrefu8CntVehAccelSpinOnBrk;
#endif
#endif

/*backward driving detection for esc control inhibition*/
#if	__VDC

	uint8_t F_gr_det_cnt;
	uint8_t	R_gr_det_cnt;
	uint8_t	Reverse_Gear_cnt;
	int16_t	lds16espBackwardDirCNT;
	int16_t	lds16espBackFlags;
	int16_t	lds16espDetYawmYawcDiff;
	int16_t	lds16espDetYawmYawcDiffOld;
	int16_t	lds16espDetYawmYawcDiffFilt;
	#if (__R_GEAR_INFORM_FOR_MT==1)
	uint8_t  ldu8RgearErrorCnt;
	U8_BIT_STRUCT_t RGEARSTATE;
	#endif

#endif

#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
    int8_t  lds8lowmudetectioncnt;
    int8_t  lds8midmudetectioncnt;
    int8_t  lds8highmudetectioncnt;
    uint8_t ldu8muchangeholdtimer;
    uint8_t ldu8absmustate;
    uint8_t ldu8absmustateold;
    uint8_t ldu8absl2hFilterResDctcnt;
    uint8_t ldu8absl2hFilterResclrcnt;
    uint8_t	WFactor1;
    int16_t	lds16FiloutDecelG_1;
    /*int16_t vrefflags01;*/
#endif

#if __SPIN_DET_IMPROVE == ENABLE

int16_t tempcnt1;
int16_t tempcnt_tmp;
int16_t ls16vehgradient_tmp;
int16_t ldu8vehiclestopstablecnt;
int16_t ldu8vehiclestablecnt;
int16_t lds16vehgradientG;
int16_t lds16vehgradientGold;
int16_t lds16vehgradientGtmp;
uint8_t ldu8vehSpinCnt;
uint8_t ldu8vehSpinCntOld;
int16_t ls16Spindetfactor;
uint8_t ldu8grdindex;
uint8_t ldu8grdindexold;
int16_t lds16tempinput;
uint8_t ldu8gearpositionforgrd;
uint8_t ldu8allspinclrcnt;
uint8_t ldu8grdstatecnt;
uint8_t ldu8offset;
uint8_t ldu8offsetcnt;
uint8_t testflag00;

struct ACC_FILTER WHEEL_ACC, GRD_ACC;
struct WL_SPIN_STRUCT *SPIN_WL, SPIN_FL, SPIN_FR, SPIN_RL, SPIN_RR;
struct AX_ITG_STRUCT *AX_P ,AX_1_P;

#endif
/* Local Function prototype	****************************************/

void	LDABS_vDctWheelCreepNoise(struct W_STRUCT *WL,const struct RTA_STRUCT *RTA_WL );

#if	__PARKING_BRAKE_OPERATION && __VDC
void	LDABS_vDctPakingBrakeOperation(void);
void	LDABS_vDctPakingBrakeWheel(struct W_STRUCT *WL);
#endif

void LDABS_vDetWheelReliability(struct W_STRUCT	*WL,struct W_STRUCT	*WL_CNT	);
void	LDABS_vCallDetWheelState(void);

	#if	(__4WD ||(__4WD_VARIANT_CODE==ENABLE))
void	LDABS_vDetVehicleAccelSpin(void);
#if	(__AX_SENSOR==1)
void    LDABS_DetPseudoVehVelInAccelSpin(void);
void    LDABS_vDetNoSpinState(void);
void	LDABS_vCalAccelOfWheel(void);
void	LDABS_vLimitLongAccInput(void);
		#if	__VDC
void	LDABS_vEstLongAccSensorDrift(void);
		#endif
#endif
	#endif


#if	 __4WD||(__4WD_VARIANT_CODE==ENABLE)
	#if	__AX_SENSOR
void	LDABS_vChkLongAccReliabilty(void);
void	LDABS_vChk4WDVrefEstError(void);
void	LDABS_vCallChkLongAcc(void);
	#endif
#endif

	#if	 __AX_SENSOR
void	LDABS_vDctBackDrivingByLongAcc(void);
	#endif
void	LDABS_vCallDctVehicleState(void);
void	LDABS_vCallDctForVref(void);

void	LDABS_vDetABSEntrySpeed(void);
void 	LCABS_vSetSLEWTIM(struct W_STRUCT	*WL);
void 	LCABS_vCalcAbmittelfz(void);
void    LDABS_vCalcAbsDurationTime(void);
/*
#if	__ACCEL_ON_BRK_VREF
 #if __4WD || (__4WD_VARIANT_CODE==ENABLE)
void LDABS_vDetVicleAccelSpinOnBrake(void);
#endif
#endif
*/
/*backward driving detection for esc control inhibition*/
#if	__VDC
void LDESP_DetectBackd(void);
	#if __R_GEAR_INFORM_FOR_MT ==1
	void LDESP_ChkRgearSwitch(void);
	#endif
#endif

#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
void    LDABS_vMuChangeDetection(void);
#endif

#if __SPIN_DET_IMPROVE == ENABLE
void LDABS_vDetLoadgradient(void);
void LDABS_vDetWLSpin(struct  WL_SPIN_STRUCT  *SPIN_WL, const struct W_STRUCT *WL, int16_t factor1, int16_t factor2, int16_t factor3);
void LDABS_vCalAxITG(struct AX_ITG_STRUCT *AX_P, int16_t Ax_sen, int16_t road_grad , int16_t offset);
void LDABS_vClrLoadgrdvariablesInstop(void);
void LDABS_vDetVehSpin(void);
void LDABS_vCalVehicleSpeed(void);
#endif
/*******************************************************************/
void	LDABS_vCallDctForVref(void)
{
		#if	__VDC
	#if	__MY_09_VREF &&__VRSECELT_PRE_YAW_CORRECTION
	LDABS_vCompRefWheelForESP(); 
	LDABS_vArrangeWheelSpeed(FL.vrad_crt2,FR.vrad_crt2,RL.vrad_crt2,RR.vrad_crt2);
	#else
	LDABS_vArrangeWheelSpeed(FL.vrad_crt_resol_change,FR.vrad_crt_resol_change,RL.vrad_crt_resol_change,RR.vrad_crt_resol_change);
	#endif
		#else
	LDABS_vArrangeWheelSpeed2();
		#endif
   
	LDABS_vCallDetWheelState();
	LDABS_vCallDctVehicleState();

    #if (__PRESS_DECEL_VREF_APPLY == ENABLE)
    LDABS_vMuChangeDetection();
    #endif

	#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))&& __AX_SENSOR
	LDABS_vCallChkLongAcc();
	
		#if (HSA_VARIANT_ENABLE==ENABLE)
	if(wu8HsaVariantCodingEndFlag==0)
	{
		USE_ALONG=0;
	}
		#endif	
	#elif (HSA_VARIANT_ENABLE==ENABLE)	&& __AX_SENSOR
	if(wu8HsaVariantCodingEndFlag==0)
	{
		USE_ALONG=0;
	}
	else 
	{
		USE_ALONG=1;
	}	
	#else
	USE_ALONG=0;
	#endif

}
/*******************************************************************/
void	LDABS_vCallDctVehicleState(void)
{
	#if	__VDC && __PARKING_BRAKE_OPERATION && !__REAR_D
	LDABS_vDctPakingBrakeOperation();
	#elif __VDC	&& __PARKING_BRAKE_OPERATION &&	__MY_09_VREF
	LDABS_vDctPakingBrakeOperation();
	#endif

#if	__4WD_VARIANT_CODE==ENABLE

	#if __SPIN_DET_IMPROVE == ENABLE
	LDABS_vDetLoadgradient();
	LDABS_vDetVehSpin();
	LDABS_vCalVehicleSpeed();
	#endif
	LDABS_vDetVehicleAccelSpin();

#else
	#if	__4WD
		#if __SPIN_DET_IMPROVE == ENABLE
		LDABS_vDetLoadgradient();
		LDABS_vDetVehSpin();
		LDABS_vCalVehicleSpeed();
		#endif
	LDABS_vDetVehicleAccelSpin();
	#endif
#endif

	#if	__AX_SENSOR
	#if	__CAR!=	GM_T300
	LDABS_vDctBackDrivingByLongAcc();
	#endif
	#endif
#if __VDC
	LDESP_DetectBackd();
#endif

	LDABS_vDetABSEntrySpeed();
	LCABS_vCalcAbmittelfz();
	LDABS_vCalcAbsDurationTime();
/*
 #if __4WD || (__4WD_VARIANT_CODE==ENABLE)
#if	__ACCEL_ON_BRK_VREF
	LDABS_vDetVicleAccelSpinOnBrake();
#endif
#endif
*/
	#if	__ENGINE_INFORMATION
	ldabsu1CanErrFlg=0;
	if((fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1))
	{
		ldabsu1CanErrFlg=1;
	}
	else if((fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
	{
		ldabsu1CanErrFlg=1;
	}
	else
	{
		;
	}
	#endif

}
/*******************************************************************/
#if	(__4WD||(__4WD_VARIANT_CODE==ENABLE))&& __AX_SENSOR
void	LDABS_vCallChkLongAcc(void)
{
	#if	__4WD_VARIANT_CODE==ENABLE
	if(lsu8DrvMode !=DM_2WD)
	{
	#endif
	LDABS_vChk4WDVrefEstError();
	LDABS_vChkLongAccReliabilty();


	LDABS_vLimitLongAccInput();
		#if	__VDC
	LDABS_vEstLongAccSensorDrift();
		#endif

	#if	__4WD_VARIANT_CODE==ENABLE
	}
	#endif
}
#endif

/*******************************************************************/
void	LDABS_vDctWheelCreepNoise(struct W_STRUCT *WL,const struct RTA_STRUCT *RTA_WL)
{
	if(speed_calc_timer	>= (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
	{
		#if __RTA_ENABLE
		if(RTA_WL->RTA_SUSPECT_WL==1)
	    {
			
			WL->lsabss16WhlSpdRawSigcmpRta_old = WL->lsabss16WhlSpdRawSigcmpRta;
	        WL->lsabss16WhlSpdRawSigcmpRta=(INT)((((LONG)WL->lsabss16WhlSpdRawSignal64)*rta_ratio)/1000); /*lsabss16WhlSpdRawSigcmpRta wl struct 추가*/
	        
	        if(WL->lsabss16WhlSpdRawSigcmpRta < VREF_MIN_SPEED)
	        {
	            WL->lsabss16WhlSpdRawSigcmpRta = VREF_MIN_SPEED;
	        }
	        else
	        {
	            ;
	        }
	        
	    }
	    	#if __RTA_2WHEEL_DCT
	    else if(RTA_WL->RTA_SUSPECT_2WHL==1)
	    {
	    	WL->lsabss16WhlSpdRawSigcmpRta_old = WL->lsabss16WhlSpdRawSigcmpRta;
	        WL->lsabss16WhlSpdRawSigcmpRta=(INT)((((LONG)WL->lsabss16WhlSpdRawSignal64)*RTA_WL->ldrtas16Rta2WhlCompRatio)/1000); /*lsabss16WhlSpdRawSigcmpRta wl struct 추가*/
	        
	        if(WL->lsabss16WhlSpdRawSigcmpRta < VREF_MIN_SPEED)
	        {
	            WL->lsabss16WhlSpdRawSigcmpRta = VREF_MIN_SPEED;
	        }
	        else
	        {
	            ;
	        }
	    }
	    	#endif
	    else
		{
			WL->lsabss16WhlSpdRawSigcmpRta_old = WL->lsabss16WhlSpdRawSigcmpRta;
			WL->lsabss16WhlSpdRawSigcmpRta = WL->lsabss16WhlSpdRawSignal64 ;
		}
	    #endif
		if(((ABS_fz==1)	|| (BRAKE_SIGNAL==1))&&(vref<VREF_50_KPH)
			#if __VDC
			&&(mtp<MTP_50_P)
			#endif
			)
		{
			if(((WL->lsabss16WhlSpdRawSigcmpRta -WL->lsabss16WhlSpdRawSigcmpRta_old)>(VREF_7_KPH*SCALE_FACTOR))&&(WL->lsabss16WhlSpdRawSigcmpRta	>(vref_resol_change+ (VREF_5_KPH*SCALE_FACTOR))))
			{
				WL->SPIN_WHEEL=1;
				SUSPECT_VREF=1;
			}
			else
			{
				;
			}
		}

		if((WL->SPIN_WHEEL)&&(WL->lsabss16WhlSpdRawSigcmpRta	<(vref_resol_change+ (VREF_1_KPH*SCALE_FACTOR))))
		{
			WL->SPIN_WHEEL=0;
		}
		else
		{
			;
		}
		if((vref<VREF_3_KPH) ||	(ABS_fz==0))
		{
			SUSPECT_VREF=0;
		}
		else
		{
			;
		}
	}
	else
	{
		WL->SPIN_WHEEL=0;
	}

}
/*2006.11.10 Deleted*/
/*******************************************************************/
/*******************************************************************/
#if	__VDC && __PARKING_BRAKE_OPERATION
/* Detection whel all rear 2 wheel are locked  04.09.08	J.K.Lee	*/
/* Update using	Parking	Brake Signal		   06.10.18	J.K.Lee	*/

void LDABS_vDctPakingBrakeOperation(void)
{
	FL.PARKING_BRAKE_WHEEL=0;
	FR.PARKING_BRAKE_WHEEL=0;
	FL.PARKING_DECEL_CHECK=0;
	FR.PARKING_DECEL_CHECK=0;

  #if __MY_09_VREF
	ldabsu1ParkingBrkActSignal=0;

	#if	(__EPB_INTERFACE == ENABLE)
	  #if (EPB_VARIANT_ENABLE==ENABLE)
		if(wu8EpbVariantCodingEndFlag==1)
		{
			if((lcu8epbLdmstate==1)&&((lcu8EpbiEpbState==EPBI_EPB_DYNAMIC_BRK)||(lcu8EpbiEpbState==EPBI_EPB_RELEASING))) 
			{
				ldabsu1ParkingBrkActSignal=1;
			}
			else
			{
				;
			}
		}
		else
		{
			if((fu1DelayedParkingBrakeSignal==1)&&(fu1ParkingBrakeSusDet==0))
			{
				ldabsu1ParkingBrkActSignal=1;
			}	
		}
	  #else
		if((lcu8epbLdmstate==1)&&((lcu8EpbiEpbState==EPBI_EPB_DYNAMIC_BRK) || (lcu8EpbiEpbState==EPBI_EPB_RELEASING))) 
		{
			ldabsu1ParkingBrkActSignal=1;
		}
		else
		{
			;
		}
	  #endif
	#elif ((__PARK_BRAKE_TYPE == CAN_TYPE) ||(__PARK_BRAKE_TYPE == ANALOG_TYPE))
		if((fu1DelayedParkingBrakeSignal==1)&&(fu1ParkingBrakeSusDet==0))
		{
			ldabsu1ParkingBrkActSignal=1;
		}
		
	#endif
  #endif	

	if(ESP_BRAKE_CONTROL_RL==0)
	{
		LDABS_vDctPakingBrakeWheel(&RL);
	}
	else
	{
		;
	}
	if(ESP_BRAKE_CONTROL_RR==0)
	{
		LDABS_vDctPakingBrakeWheel(&RR);
	}
	else
	{
		;
	}

	#if	__MY_09_VREF
	if(ldabsu1ParkingBrkActSignal==1)
	{
		tempW9 = -LAM_20P;
	}
	else
	{
		tempW9 = -LAM_30P;
	}	
			
	if((ABS_fz==0)&&(BRAKE_SIGNAL==0)&&(ACTIVE_BRAKE_ACT==0))
	{
		if((RL.PARKING_BRAKE_WHEEL==1)&&(RR.PARKING_BRAKE_WHEEL==1))
		{
			PARKING_BRAKE_OPERATION=1;
		}
		else if(((RL.PARKING_BRAKE_WHEEL==1)&&(RR.rel_lam<=tempW9))
			  ||((RR.PARKING_BRAKE_WHEEL==1)&&(RL.rel_lam<=tempW9)))
		{
			PARKING_BRAKE_OPERATION=1;
			RL.PARKING_BRAKE_WHEEL=1;  
			RR.PARKING_BRAKE_WHEEL=1;  
		}
		else
		{
			PARKING_BRAKE_OPERATION=0;
		}
	}
	else
	{
		if((RL.PARKING_BRAKE_WHEEL==0)&&(RR.PARKING_BRAKE_WHEEL==0))
		{
			PARKING_BRAKE_OPERATION=0;
		}
	}

  	#else
	if((ABS_fz==0)&&(MPRESS_BRAKE_ON==0))
	{
		if((FL.rel_lam>=RL.rel_lam)&&(FR.rel_lam>=RR.rel_lam))
		{
			if(((RL.PARKING_BRAKE_WHEEL==1)&&(RR.rel_lam<=-LAM_30P))
			 ||((RR.PARKING_BRAKE_WHEEL==1)&&(RL.rel_lam<=-LAM_30P)))
			{
				PARKING_BRAKE_OPERATION=1;
			}
		  #if((__PARK_BRAKE_TYPE == CAN_TYPE)||(__PARK_BRAKE_TYPE == ANALOG_TYPE))	/* Parking Brake Signal	On */
			else if((fu1DelayedParkingBrakeSignal==1)&&(fu1ParkingBrakeSusDet==0)&&(RL.rel_lam<=-LAM_10P)&&(RR.rel_lam<=-LAM_10P))
			{
				PARKING_BRAKE_OPERATION=1;
			}
		  #endif
			else
			{
				PARKING_BRAKE_OPERATION=0;
			}
		}
		else
		{
			PARKING_BRAKE_OPERATION=0;
		}
	}
	else
	{
		PARKING_BRAKE_OPERATION=0;
	}

	if(((FL.vrad<=VREF_5_KPH)&&(FR.vrad<=VREF_5_KPH))
	  ||((((mtp>=3)&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))&&(RL.rel_lam>=-LAM_5P)&&(RR.rel_lam>=-LAM_5P)))
	{
		PARKING_BRAKE_OPERATION=0;
		RL.PARKING_DECEL_CHECK=0;
		RR.PARKING_DECEL_CHECK=0;
		RL.Wheel_Big_Slip_Counter=0;
		RR.Wheel_Big_Slip_Counter=0;
		RL.PARKING_BRAKE_WHEEL=0;
		RR.PARKING_BRAKE_WHEEL=0;
	}
	else
	{
		;
	}
	#endif
}
/*******************************************************************/
void LDABS_vDctPakingBrakeWheel(struct W_STRUCT	*WL)
{

#if	__MY_09_VREF
	int16_t	ldabsu8DVParkingBrkDctThr;
	int8_t ldabsu8SlipParkingBrkDctThr;
	int16_t	ldabsu8DecelParkingBrkDctThr;
	int16_t	ldabsu8DVParkingBrkResetThr;
	int8_t ldabsu8SlipParkingBrkResetThr;
	int16_t	ldabsu8DecelParkingBrkResetThr;
	
		
	int8_t ldabsu8DecelResetSlipThr;
	if(ldabsu1ParkingBrkActSignal==1)
	{
		ldabsu8DVParkingBrkDctThr=-VREF_3_KPH;
		ldabsu8SlipParkingBrkDctThr=-LAM_10P;
		ldabsu8DecelParkingBrkDctThr=-ARAD_1G0;
		ldabsu8DVParkingBrkResetThr=-VREF_2_KPH;
		ldabsu8SlipParkingBrkResetThr=-LAM_5P;
		ldabsu8DecelParkingBrkResetThr=ARAD_0G5;
		ldabsu8DecelResetSlipThr = -LAM_10P;
	}
	else
	{
		ldabsu8DVParkingBrkDctThr=-VREF_5_KPH;
		ldabsu8SlipParkingBrkDctThr=-LAM_40P;
		ldabsu8DecelParkingBrkDctThr=-ARAD_3G0;
		ldabsu8DVParkingBrkResetThr=-VREF_3_KPH;
		ldabsu8SlipParkingBrkResetThr=-LAM_5P;
		ldabsu8DecelParkingBrkResetThr=ARAD_0G5;
		ldabsu8DecelResetSlipThr = -LAM_30P;
	}
	
	/*Set Condition*/	
	if(WL->PARKING_BRAKE_WHEEL==0)
	{

		
		if((ABS_fz==0)&&(BRAKE_SIGNAL==0)&&(ACTIVE_BRAKE_ACT==0))
		{
			if(WL->arad<=ldabsu8DecelParkingBrkDctThr)
			{
				WL->PARKING_DECEL_CHECK=1;
			}
			else if((WL->arad>=ldabsu8DecelParkingBrkResetThr)&&(WL->rel_lam>=ldabsu8DecelResetSlipThr))
			{
				WL->PARKING_DECEL_CHECK=0;
			}
			else
			{
				;
			}
			
			if((WL->rel_lam	>= (int8_t)(-LAM_5P))&&(WL->vrad_crt >=	VREF_3_KPH))
			{
				WL->PARKING_DECEL_CHECK=0;
			}
			else
			{
				;
			}

			if(WL->PARKING_DECEL_CHECK==1)
			{
				#if	__VDC
				if(ESP_BRAKE_CONTROL_FL==1)
				{
					if(ESP_BRAKE_CONTROL_FR==1)
					{
						tempW1=FL.vrad_crt;
						if(FL.vrad_crt<FR.vrad_crt)
						{
							tempW1=FR.vrad_crt;
						}
						if(((WL->vrad_crt -tempW1)<ldabsu8DVParkingBrkDctThr)&&(WL->rel_lam	<= ldabsu8SlipParkingBrkDctThr))
						{
							 WL->Wheel_Big_Slip_Counter++;
						}
						else if(((WL->vrad_crt -tempW1)>ldabsu8DVParkingBrkResetThr)&&(WL->rel_lam > ldabsu8SlipParkingBrkResetThr))
						{
							if(WL->Wheel_Big_Slip_Counter>0)
							{
								WL->Wheel_Big_Slip_Counter--;
							}
						}
						else
						{
							;
						}
					}
					else
					{
						if(((WL->vrad_crt -FR.vrad_crt)<ldabsu8DVParkingBrkDctThr)&&(WL->rel_lam <=	ldabsu8SlipParkingBrkDctThr))
						{
							 WL->Wheel_Big_Slip_Counter++;
						}
						else if(((WL->vrad_crt -FR.vrad_crt)>ldabsu8DVParkingBrkResetThr)&&(WL->rel_lam	> ldabsu8SlipParkingBrkResetThr))
						{
							if(WL->Wheel_Big_Slip_Counter>0)
							{
								WL->Wheel_Big_Slip_Counter--;
							}
						}
						else
						{
							;
						}
					}
				}
				else if(ESP_BRAKE_CONTROL_FR==1)
				{
					if(((WL->vrad_crt -FL.vrad_crt)<ldabsu8DVParkingBrkDctThr)&&(WL->rel_lam <=	ldabsu8SlipParkingBrkDctThr))
					{
						 WL->Wheel_Big_Slip_Counter++;
					}
					else if(((WL->vrad_crt -FL.vrad_crt)>ldabsu8DVParkingBrkResetThr)&&(WL->rel_lam	> ldabsu8SlipParkingBrkResetThr))
					{
						if(WL->Wheel_Big_Slip_Counter>0)
						{
							WL->Wheel_Big_Slip_Counter--;
						}
					}
					else
					{
						;
					}
				}
				else
				{
				#endif
					if(((WL->vrad_crt -FL.vrad_crt)<ldabsu8DVParkingBrkDctThr)&&((WL->vrad_crt -FR.vrad_crt)<ldabsu8DVParkingBrkDctThr)&&(WL->rel_lam <= ldabsu8SlipParkingBrkDctThr))
					{
						 WL->Wheel_Big_Slip_Counter++;
					}
					else if(((WL->vrad_crt -FL.vrad_crt)>ldabsu8DVParkingBrkResetThr)&&((WL->vrad_crt -FR.vrad_crt)>ldabsu8DVParkingBrkResetThr)&&(WL->rel_lam > ldabsu8SlipParkingBrkResetThr))
					{
						if(WL->Wheel_Big_Slip_Counter>0)
						{
							WL->Wheel_Big_Slip_Counter--;
						}
					}
					else
					{
						;
					}
				#if	__VDC
				}
				#endif
			}
/*---------------------------------------- 
Correct	Code Error :Unreachable	Condtion  
PPR	2009-131
----------------------------------------*/
/*	
			else if((WL->vrad_crt<=VREF_2_KPH)&&((WL->vrad_crt-vref)>=VREF_2_KPH)&&((vref>=VREF_5_KPH)||((vrad_crt_fl>=VREF_10_KPH)&&(vrad_crt_fr>=VREF_10_KPH))))
*/
			else if((WL->vrad_crt<=VREF_2_KPH)&&(WL->arad<0)&&((vref>=VREF_5_KPH)&&((vrad_crt_fl>=VREF_10_KPH)&&(vrad_crt_fr>=VREF_10_KPH))))
			{
				WL->Wheel_Big_Slip_Counter++;
			}
			else if((WL->vrad_crt>VREF_3_KPH)&&(WL->Wheel_Big_Slip_Counter>0))
			{
				WL->Wheel_Big_Slip_Counter--;
			}
			else
			{
				;
			}
			if(WL->Wheel_Big_Slip_Counter>=L_U8_TIME_10MSLOOP_70MS)
			{
				WL->PARKING_BRAKE_WHEEL=1;
				WL->Wheel_Big_Slip_Counter=0;
			}
			else
			{
				;
			}
				
		}						
	}
		/*Reset	Condition*/
	else
	{
		if(WL->vrad_crt>VREF_3_KPH)
		{
			if(((WL->vrad_crt -FL.vrad_crt)>ldabsu8DVParkingBrkResetThr)
				&&((WL->vrad_crt -FR.vrad_crt)>ldabsu8DVParkingBrkResetThr)
				&&(WL->rel_lam > ldabsu8SlipParkingBrkResetThr))
			{
				WL->PARKING_BRAKE_WHEEL=0;
			}
			else if((FL.vrad_crt<VREF_5_KPH)&&(FR.vrad_crt<VREF_5_KPH))	
			{
				WL->PARKING_BRAKE_WHEEL=0;
			}
			else
			{
				;
			}			
		}
		else
		{
			if((FL.vrad_crt<VREF_3_KPH)&&(FR.vrad_crt<VREF_3_KPH)&&(WL->vrad_crt<VREF_3_KPH)&&(vref<VREF_3_KPH))
			{
				WL->PARKING_BRAKE_WHEEL=0;
  
			}
			else
			{
				;
			}			
		}
		/*
		if(((WL->vrad_crt -FL.vrad_crt)>ldabsu8DVParkingBrkResetThr)&&((WL->vrad_crt -FR.vrad_crt)>ldabsu8DVParkingBrkResetThr)
			&&(WL->rel_lam > ldabsu8SlipParkingBrkResetThr)&&(WL->vrad_crt>VREF_3_KPH)&&(ldabsu1ParkingBrkActSignal==0))
		{
			WL->PARKING_BRAKE_WHEEL=0;
		}
		else if((FL.vrad_crt<VREF_5_KPH)&&(FR.vrad_crt<VREF_5_KPH)&&(WL->vrad_crt>VREF_3_KPH))
		{
			WL->PARKING_BRAKE_WHEEL=0;
		}
		else
		{
			;
		}
		*/
	}		
#else
	if((ABS_fz==0)&&(MPRESS_BRAKE_ON==0))
	{
		if(WL->arad<=-ARAD_3G0)
		{
			WL->PARKING_DECEL_CHECK=1;
		}
		else if(WL->arad>=ARAD_0G5)
		{
			WL->PARKING_DECEL_CHECK=0;
		}
		else
		{
			;
		}

		if(WL->PARKING_DECEL_CHECK==1)
		{
			if(((WL->rel_lam <=	(int8_t)(-LAM_50P))||(WL->vrad <= VREF_3_KPH))
			  &&(WL->Wheel_Big_Slip_Counter<=L_U8_CNT_250))
			  {
				WL->Wheel_Big_Slip_Counter++;
			}
			else
			{
				;
			}
			if((WL->rel_lam	>= (int8_t)(-LAM_5P))&&(WL->vrad >=	VREF_3_KPH))
			{
				WL->PARKING_DECEL_CHECK=0;
			}
			else
			{
				;
			}

			if(WL->Wheel_Big_Slip_Counter>=L_U8_TIME_10MSLOOP_70MS)
			{
				WL->PARKING_BRAKE_WHEEL=1;
			}
			else
			{
				;
			}
		}
		else if(WL->rel_lam	>= (int8_t)(-LAM_5P))
		{
			WL->Wheel_Big_Slip_Counter=0;
			WL->PARKING_BRAKE_WHEEL=0;
		}
		else
		{
			;
		}
	}
	else
	{
		WL->PARKING_DECEL_CHECK=0;
		WL->Wheel_Big_Slip_Counter=0;
		WL->PARKING_BRAKE_WHEEL=0;
	}
#endif
}

#endif
/*******************************************************************/

#if	__MGH60_NEW_SELECTION

void LDABS_vDetWheelReliability(struct W_STRUCT	*WL,struct W_STRUCT	*WL_CNT	)
{
	int8_t drive_whl_flg;
	int16_t	arad_diff;
	int16_t	whl_ref_spd;
	int16_t	vehicle_speed;
	int16_t	wheel_speed;
	int16_t	cnter_whl_speed;
	int16_t	wheel_speed_diff_1;
	int16_t	wheel_speed_diff_2;
	int16_t	wheel_speed_diff_3;
	int16_t	non_driven_wheel_left;
	int16_t	non_driven_wheel_right;
	int16_t	driven_wheel_left;
	int16_t	driven_wheel_right;
	int16_t	vehicle_speed_variation;
	int16_t	wheel_diff;

	int8_t ldvrefu1DefDrivenWhl;
	
	#if	__ACCEL_ON_BRK_VREF
	int8_t ldvrefu1SatisfyEngCondition=0;
	int8_t ldvrefu1SatisfyDvCondition=0;
	int16_t	 ldvrefs16AccelonBrkDvThr1;
	int16_t	 ldvrefs16AccelonBrkDvThr2;
	int16_t	 ldvrefs16AccelonBrkDvThr3;
	int16_t	 ldvrefs16AccelonBrkEngTorqThr;
	int16_t	 ldvrefs16AccelonBrkMtpThr;
	int16_t	 ldvrefs16AccelonBrkDetTime;
	#endif
	
	int16_t	other_3whl_diff;
	int16_t	arad_diff2;

	vehicle_speed=vref_resol_change;
	wheel_speed=WL->vrad_crt_resol_change;
	cnter_whl_speed=WL_CNT->vrad_crt_resol_change;
	vehicle_speed_variation=vref_resol_change-vref_alt_resol_change;
	wheel_diff=vrad_diff_max_resol_change;
		#if	__REAR_D
	non_driven_wheel_left=FL.vrad_crt_resol_change;
	non_driven_wheel_right=FR.vrad_crt_resol_change;
	driven_wheel_left=RL.vrad_crt_resol_change;
	driven_wheel_right=RR.vrad_crt_resol_change;
		#else
	non_driven_wheel_left=RL.vrad_crt_resol_change;
	non_driven_wheel_right=RR.vrad_crt_resol_change;
	driven_wheel_left=FL.vrad_crt_resol_change;
	driven_wheel_right=FR.vrad_crt_resol_change;
		#endif

		#if	__VDC
	whl_ref_spd=WL->wvref_resol_change;
		#else
	whl_ref_spd=vref_resol_change;
		#endif

	if(WL->REAR_WHEEL==1)
	{
		wheel_speed_diff_1=wheel_speed-FL.vrad_crt_resol_change;
		wheel_speed_diff_2=wheel_speed-FR.vrad_crt_resol_change;
		if(WL->LEFT_WHEEL==1)
		{
			wheel_speed_diff_3=wheel_speed-RR.vrad_crt_resol_change;
		}
		else
		{
			wheel_speed_diff_3=wheel_speed-RL.vrad_crt_resol_change;
		}
	}
	else
	{
		wheel_speed_diff_1=wheel_speed-RL.vrad_crt_resol_change;
		wheel_speed_diff_2=wheel_speed-RR.vrad_crt_resol_change;
		if(WL->LEFT_WHEEL==1)
		{
			wheel_speed_diff_3=wheel_speed-FR.vrad_crt_resol_change;
		}
		else
		{
			wheel_speed_diff_3=wheel_speed-FL.vrad_crt_resol_change;
		}
	}

	tempW0=McrAbs(wheel_speed_diff_1-wheel_speed_diff_2);
	tempW1=McrAbs(wheel_speed_diff_2-wheel_speed_diff_3);
	tempW2=McrAbs(wheel_speed_diff_3-wheel_speed_diff_1);

	if(tempW0>tempW1)
	{
		tempW3=tempW0;
	}
	else
	{
		tempW3=tempW1;
	}

	if(tempW2>tempW3)
	{
		tempW3=tempW2;
	}
	else{}

	other_3whl_diff=tempW3;

	ldvrefu1DefDrivenWhl=0;
	#if	(__4WD_VARIANT_CODE==ENABLE)
	if ((lsu8DrvMode ==DM_2WD)||(lsu8DrvMode ==DM_2H))
	{
		#if	__REAR_D
		if(REAR_WHEEL_wl==1)
		#else
		if(REAR_WHEEL_wl==0)
		#endif
		{
			drive_whl_flg=1;
		}
		else
		{
			drive_whl_flg=0;
		}
		tempW4=ebd_refilt_grv;
		ldvrefu1DefDrivenWhl=1;
	}
	else if(lsu8DrvMode	==DM_AUTO)
	{
		if(ABS_fz==1)
		{
			#if	__REAR_D
			if(REAR_WHEEL_wl==1)
			#else
			if(REAR_WHEEL_wl==0)
			#endif
			{
				drive_whl_flg=1;
			}
			else
			{
				drive_whl_flg=0;
			}
			ldvrefu1DefDrivenWhl=1;
		}
		else
		{
			drive_whl_flg=1;
			ldvrefu1DefDrivenWhl=0;
		}
		
		#if	__AX_SENSOR
		tempW4=ax_Filter;
		#else
		tempW4=ebd_refilt_grv;
		#endif
	}
	else if(lsu8DrvMode	==DM_4H)
	{
		drive_whl_flg=1;
		#if	__AX_SENSOR
		tempW4=ax_Filter;
		#else
		tempW4=ebd_refilt_grv;
		#endif
		ldvrefu1DefDrivenWhl=0;
	}
	else
	{
		drive_whl_flg=1;
		#if	__AX_SENSOR
		tempW4=ax_Filter;
		#else
		tempW4=ebd_refilt_grv;
		#endif
		ldvrefu1DefDrivenWhl=0;
	}
	
	#else
	  #if __4WD
		drive_whl_flg=1;
		#if	__AX_SENSOR
		tempW4=ax_Filter;
		#else
		tempW4=ebd_refilt_grv;
		#endif
		ldvrefu1DefDrivenWhl=0;
	  #else
		#if	__REAR_D
		if(REAR_WHEEL_wl==1)
		#else
		if(REAR_WHEEL_wl==0)
		#endif
		{
			drive_whl_flg=1;
		}
		else
		{
			drive_whl_flg=0;
		}
		tempW4=ebd_refilt_grv;
		ldvrefu1DefDrivenWhl=1;
	  #endif
	#endif
	
	if(	tempW4 < -AFZ_0G5 )
	{
		tempW4=-AFZ_0G5;
	}
	else{}
		
	arad_diff =	WL->arad_avg - tempW4;
	arad_diff2 = WL->arad_avg-ebd_refilt_grv;
	
/*
	RELIABLE_FOR_VREF==1 : Reliabile
	RELIABLE_FOR_VREF==0 : UnReliabile
*/

	WL->whl_reliability_chk_flag=0;

	if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
	{
		WL->whl_reliability_chk_flag=1;
		WL->ESC_BRAKE_SLIP_OK=0;
		WL->ESC_CNT_SPIN_OK=0;
		WL->SPECIAL_SPIN_OK=0;
		WL->DRIVING_SPIN_OK=0;
		
		if(WL->RELIABLE_FOR_VREF2==0) /* Some kind of RTA suspect */
		{
			WL->RELIABLE_FOR_VREF=0;
			WL->whl_reliability_chk_cnt=0;
		}
		else{}
				
		if(WL->RELIABLE_FOR_VREF==0)
		{
			/* REAL	ABS	State*/

			if(ABS_fz_K==1)
			{
				WL->UNRELIABLE_AT_ABS_ENTRY=1;
			}
			else{}
			if((BRAKE_SIGNAL_K==1)||(BRAKING_STATE_OLD==0))
			{
				WL->UNRELIABLE_AT_BRAKE_ENTRY=1;
				WL->whl_reliability_chk_cnt=0;
			}
			else{}
			/*---------------------------------------
			Reliabile Condition	Accel to ABS State
			-----------------------------------------
			1. (vrad-vref)<THR
			2. (vrad-ABS_sntry_speed)<THR && (vrad-CBS_sntry_speed)<THR
			3. when	conditon 1 or 2	is satisfied, increase CNT.
			4. CNT>THR --> WHEEL RELIAIBILITY O.K
			------------------------------------------*/
			WL->whl_reliability_chk_flag=2;
			if(WL->RELIABLE_FOR_VREF2==1)
			{
				WL->whl_reliability_chk_flag=3;
				if((WL->UNRELIABLE_AT_ABS_ENTRY==1)||(WL->UNRELIABLE_AT_BRAKE_ENTRY==1))
				{
					if((wheel_speed-vehicle_speed)<WHEEL_RELIABLE_THR_BRAKE_ENTRY)
					{
						WL->whl_reliability_chk_cnt+=2;
						WL->whl_reliability_chk_flag=4;
					}
					else if((wheel_speed-ABS_entry_speed2)<0)
					{
						WL->whl_reliability_chk_cnt++;
						WL->whl_reliability_chk_flag=5;
					}
					else
					{
						WL->whl_reliability_chk_flag=6;
					}

					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
					{
						WL->whl_reliability_chk_cnt=0;
						WL->UNRELIABLE_AT_ABS_ENTRY=0;
						WL->UNRELIABLE_AT_BRAKE_ENTRY=0;
						WL->RELIABLE_FOR_VREF=1;
					}
					else{}
				}
				else  /*Reliable Condition after ABS to	Accel State*/
				{
					WL->whl_reliability_chk_flag=7;
				  #if __ACCEL_ON_BRK_VREF
					
					ldvrefu1SatisfyDvCondition=0;
					ldvrefu1SatisfyEngCondition=0;

					ldvrefs16AccelonBrkDvThr1=VREF_1_5_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkEngTorqThr=TORQ_PERCENT_20PRO;
					ldvrefs16AccelonBrkMtpThr=MTP_5_P;
					ldvrefs16AccelonBrkDetTime=L_U8_TIME_10MSLOOP_70MS;

					if(ldvrefu1DefDrivenWhl==1)
					{
						WL->whl_reliability_chk_flag=8;
						if(((wheel_speed-non_driven_wheel_right)<ldvrefs16AccelonBrkDvThr1)||
							((wheel_speed-non_driven_wheel_left)<ldvrefs16AccelonBrkDvThr1)||
							((wheel_speed-vehicle_speed)<0))
						{
							ldvrefu1SatisfyDvCondition=1;
							WL->whl_reliability_chk_flag=9;
						}
						else{}
					}
					else if(((wheel_speed-wheel_speed_3rd)<ldvrefs16AccelonBrkDvThr1)
						||((wheel_speed-vehicle_speed)<0))
					{
						ldvrefu1SatisfyDvCondition=1;
						WL->whl_reliability_chk_flag=10;
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
						WL->whl_reliability_chk_flag=11;
					}

					#if	__ENGINE_INFORMATION
					if(((mtp<=ldvrefs16AccelonBrkMtpThr)&&(eng_torq_rel<ldvrefs16AccelonBrkEngTorqThr)
					&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))
					{
						ldvrefu1SatisfyEngCondition=1;
					}
					else
					{
						ldvrefu1SatisfyEngCondition=0;
					}
					#endif

					#if	__ENGINE_INFORMATION
					if((ldvrefu1SatisfyDvCondition==1)&&(ldvrefu1SatisfyEngCondition==1))
					#else
					if(ldvrefu1SatisfyDvCondition==1)
					#endif
					{
						WL->whl_reliability_chk_cnt++;
						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=1;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
					}

					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)
					
					#else
					if((wheel_speed-vehicle_speed)<0)
					{
						#if	__ENGINE_INFORMATION
						if((mtp<5)&&(eng_torq_rel<TORQ_PERCENT_20PRO)&&(ldabsu1CanErrFlg==0))
						{
						#endif
							WL->RELIABLE_FOR_VREF=1;
							WL->whl_reliability_chk_cnt=0;
							WL->whl_reliability_chk_flag=4;
						#if	__ENGINE_INFORMATION
						}
						else if	(((wheel_speed-non_driven_wheel_left)<0)|| ((wheel_speed-non_driven_wheel_right)<0))
						{
							WL->RELIABLE_FOR_VREF=1;
							WL->whl_reliability_chk_cnt=0;
							WL->whl_reliability_chk_flag=5;
						}
						else
						{
							WL->whl_reliability_chk_flag=6;
						}
						#endif
					}
					else{}
					#endif
				}
			}
			else
			{
				WL->whl_reliability_chk_flag=12;
			}
		}
		else
		{
			WL->whl_reliability_chk_flag=13;
			if(drive_whl_flg==1)
			{
				WL->whl_reliability_chk_flag=14;
				/*UnReliable Condition Due to  ABS to Accel	State*/
				#if	__ENGINE_INFORMATION
				if(ldabsu1CanErrFlg==0)
				{
					WL->whl_reliability_chk_flag=15;
					#if	__ACCEL_ON_BRK_VREF
					ldvrefu1SatisfyDvCondition=0;
					ldvrefu1SatisfyEngCondition=0;

					ldvrefs16AccelonBrkDvThr1=VREF_2_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr2=VREF_0_25_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkEngTorqThr=TORQ_PERCENT_15PRO;
					ldvrefs16AccelonBrkMtpThr=MTP_10_P;
					ldvrefs16AccelonBrkDetTime=10;

					if(ldvrefu1DefDrivenWhl==1)
					{
						WL->whl_reliability_chk_flag=16;
						if(((wheel_speed-non_driven_wheel_right)>ldvrefs16AccelonBrkDvThr1)&&
							((wheel_speed-non_driven_wheel_left)>ldvrefs16AccelonBrkDvThr1)&&
							((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
						{
							ldvrefu1SatisfyDvCondition=1;
							WL->whl_reliability_chk_flag=17;
						}
						else{}

						if(((wheel_speed-non_driven_wheel_right)>(ldvrefs16AccelonBrkDvThr1*2))&&
							((wheel_speed-non_driven_wheel_left)>(ldvrefs16AccelonBrkDvThr1*2))&&
							((wheel_speed-vehicle_speed)>(ldvrefs16AccelonBrkDvThr2*2)))
						{
							ldvrefu1SatisfyDvCondition=2;
							WL->whl_reliability_chk_flag=18;
						}
						else{}

/* --------------------------------
Additional Accel on	Brake Reliablity Check Conditiion 
PPR	2009-089  
----------------------------------*/
						if(((wheel_speed-non_driven_wheel_right)>(ldvrefs16AccelonBrkDvThr1*3))&&
							((wheel_speed-non_driven_wheel_left)>(ldvrefs16AccelonBrkDvThr1*3))&&
							((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
						{
							ldvrefu1SatisfyDvCondition=3;
							WL->whl_reliability_chk_flag=41;
						}
						else{}

					}
					else if(((wheel_speed-wheel_speed_3rd)>ldvrefs16AccelonBrkDvThr1)
						&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
					{
						ldvrefu1SatisfyDvCondition=1;
						WL->whl_reliability_chk_flag=19;
						if(((wheel_speed-wheel_speed_3rd)>(ldvrefs16AccelonBrkDvThr1*2))
						&&((wheel_speed-vehicle_speed)>(ldvrefs16AccelonBrkDvThr2*2)))
						{
							ldvrefu1SatisfyDvCondition=2;
							WL->whl_reliability_chk_flag=20;
						}
						else{}
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
						WL->whl_reliability_chk_flag=21;
					}


					if((mtp>ldvrefs16AccelonBrkMtpThr)&&(eng_torq_rel>ldvrefs16AccelonBrkEngTorqThr))
					{
						ldvrefu1SatisfyEngCondition=1;

						if((mtp>(ldvrefs16AccelonBrkMtpThr*2))&&(eng_torq_rel>(ldvrefs16AccelonBrkEngTorqThr*2)))
						{
							ldvrefu1SatisfyEngCondition=2;
						}
						else{}
					}
					else
					{
						ldvrefu1SatisfyEngCondition=0;
					}

					if((ldvrefu1SatisfyEngCondition>=1)&&(ldvrefu1SatisfyDvCondition>=1))
					{
						WL->whl_reliability_chk_flag=22;
						if((vehicle_speed_variation>0)&&(WL->arad>ARAD_0G5))
						{
							WL->whl_reliability_chk_flag=23;
							WL->whl_reliability_chk_cnt++;
							if((ldvrefu1SatisfyEngCondition>=2)||(ldvrefu1SatisfyDvCondition>=2))
							{
								WL->whl_reliability_chk_cnt++;
							}
							else{}
						}
/* --------------------------------
Additional Accel on	Brake Reliablity Check Conditiion 
Not	evasive	wheel acceleration
PPR	2009-089  
----------------------------------*/					 
						else if((vehicle_speed_variation>0)&&(WL->arad_avg>AFZ_0G2))
						{
							WL->whl_reliability_chk_flag=24;
							WL->whl_reliability_chk_cnt++;
						}							
						else{}

						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=0;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
						WL->whl_reliability_chk_flag=25;
					}
					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)

					#else
					if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
					{
						WL->whl_reliability_chk_cnt++;
						WL->whl_reliability_chk_flag=9;
					}
					else if((mtp>10)&&(eng_torq_rel>TORQ_PERCENT_20PRO))
					{
						if(((wheel_speed-vehicle_speed)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&(vehicle_speed_variation>=0))
						{
							WL->whl_reliability_chk_flag=10;	
							if((wheel_speed-vehicle_speed)>WHEEL_RELIABLE_THR_ABS2ACCEL_2 )
							{
								WL->whl_reliability_chk_cnt++;
								WL->whl_reliability_chk_flag=11;
							}
							WL->whl_reliability_chk_cnt++;
						}
						if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
						{
							WL->whl_reliability_chk_cnt++;
							WL->whl_reliability_chk_flag=12;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
						WL->whl_reliability_chk_flag=13;
					}
					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
					}
					else
					{
						;
					}
					#endif
				}
				else
				{
				#endif
					#if	__ACCEL_ON_BRK_VREF
					ldvrefu1SatisfyDvCondition=0;

					ldvrefs16AccelonBrkDvThr1=VREF_3_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr2=VREF_0_75_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr3=VREF_0G125_KPH_RESOL_CHANGE;

					ldvrefs16AccelonBrkDetTime=L_U8_TIME_10MSLOOP_140MS;
					WL->whl_reliability_chk_flag=26;
					if(ldvrefu1DefDrivenWhl==1)
					{
						WL->whl_reliability_chk_flag=27;

						if(((wheel_speed-non_driven_wheel_right)>ldvrefs16AccelonBrkDvThr1)
							&&((wheel_speed-non_driven_wheel_left)>ldvrefs16AccelonBrkDvThr1)
							&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
						{
							ldvrefu1SatisfyDvCondition=1;
							WL->whl_reliability_chk_flag=28;
						}
						else{}
					}
					else if(((wheel_speed-wheel_speed_3rd)>ldvrefs16AccelonBrkDvThr1)
						&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
					{
						ldvrefu1SatisfyDvCondition=1;
						WL->whl_reliability_chk_flag=29;
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
						WL->whl_reliability_chk_flag=30;
					}

					if(ldvrefu1SatisfyDvCondition==1)
					{
						WL->whl_reliability_chk_flag=31;
						if((vehicle_speed_variation>=ldvrefs16AccelonBrkDvThr3)&&(WL->arad>ARAD_1G0))
						{
							WL->whl_reliability_chk_cnt++;
							WL->whl_reliability_chk_flag=32;
						}
						else{}

						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=0;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
						WL->whl_reliability_chk_flag=33;
					}
					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)
					#else
					if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
					{
						WL->whl_reliability_chk_cnt++;
						WL->whl_reliability_chk_flag=14;
					}

					if((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL)
					{
						if((ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR)&&(WL->arad>=ABS2ACCEL_DET_ARAD_THR))
						{
							#if	__4WD_VARIANT_CODE==ENABLE
							if(lsu8DrvMode !=DM_2WD)
							{
								WL->whl_reliability_chk_flag=17;
								if(((wheel_speed-non_driven_wheel_left)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&
								((wheel_speed-non_driven_wheel_right)>WHEEL_RELIABLE_THR_ABS2ACCEL))
								{
									WL->whl_reliability_chk_cnt++;
									WL->whl_reliability_chk_flag=15;	
									if(((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL_2)
									&&(ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR2))
									{
										WL->whl_reliability_chk_cnt++;
										WL->whl_reliability_chk_flag=16;	
									}
								}
							}
							else
							{
								WL->whl_reliability_chk_cnt++;
								WL->whl_reliability_chk_flag=18;
							}
							#else
								#if	__4WD
								if(((wheel_speed-non_driven_wheel_left)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&
								((wheel_speed-non_driven_wheel_right)>WHEEL_RELIABLE_THR_ABS2ACCEL))
								{
									WL->whl_reliability_chk_cnt++;
									WL->whl_reliability_chk_flag=19;
									if(((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL_2)
									&&(ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR2))
									{
										WL->whl_reliability_chk_cnt++;
										WL->whl_reliability_chk_flag=20;
									}
								}
								#else
								WL->whl_reliability_chk_cnt++;
								WL->whl_reliability_chk_flag=21;
								#endif
							#endif
						}
						else
						{
							if(WL->whl_reliability_chk_cnt>0)
							{
								WL->whl_reliability_chk_cnt--;
								WL->whl_reliability_chk_flag=22;
							}
							else
							{
								WL->whl_reliability_chk_cnt=0;
								WL->whl_reliability_chk_flag=23;
							}
						}
					}
					else
					{
						if((wheel_speed-vehicle_speed)<0)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->whl_reliability_chk_flag=24;
						}
						else
						{
							if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_15MS)
							{
								WL->whl_reliability_chk_cnt-=2;
								WL->whl_reliability_chk_flag=25;
							}
							else
							{
								WL->whl_reliability_chk_cnt=0;
								WL->whl_reliability_chk_flag=26;
							}
						}
					}

					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_105MS)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
					}
					else
					{
						;
					}
					#endif

				#if	__ENGINE_INFORMATION
				}
				#endif
			}
			else
			{
				WL->whl_reliability_chk_flag=34;
			}
		}
	}
	else /*Not ABS State & Not Brake Signal*/
	{
		WL->UNRELIABLE_AT_ABS_ENTRY=0;
		WL->UNRELIABLE_AT_BRAKE_ENTRY=0;
		WL->whl_reliability_chk_flag=35;
	
		#if	__VDC
		if(ESP_BRAKE_CONTROL_WL==1)
		{
			if((WL->ESC_BRAKE_SLIP_OK==0)
			&&(wheel_speed<(cnter_whl_speed-VREF_2_KPH_RESOL_CHANGE))&&(wheel_speed<(wheel_speed_2nd-VREF_2_KPH_RESOL_CHANGE)))
			{
				WL->ESC_BRAKE_SLIP_OK=1;
				WL->whl_reliability_chk_flag=37;
			}
			else if((WL->ESC_BRAKE_SLIP_OK==1)&&(wheel_speed<cnter_whl_speed)&&(wheel_speed<wheel_speed_2nd))
			{
				WL->ESC_BRAKE_SLIP_OK=1;
				WL->whl_reliability_chk_flag=38;
			}
			else if((WL->ESC_BRAKE_SLIP_OK==1)&&(WL->rel_lam<-5))
			{
				WL->ESC_BRAKE_SLIP_OK=1;
				WL->whl_reliability_chk_flag=36;
			}
			else
			{
				WL->ESC_BRAKE_SLIP_OK=0;
				WL->whl_reliability_chk_flag=39;
			}
		}
		else
		{
			WL->ESC_BRAKE_SLIP_OK=0;
			WL->whl_reliability_chk_flag=40;	
		}
		
		if(ESP_BRAKE_CONTROL_CNT_WL==1)
		{
			if(drive_whl_flg==1)
			{
				if((WL->ESC_CNT_SPIN_OK==0)
				&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>(wheel_speed_2nd+VREF_2_KPH_RESOL_CHANGE))
				&&((mtp>0)||(eng_torq_rel>TORQ_PERCENT_10PRO))&&(ldabsu1CanErrFlg==0))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=46;
				}
			  
			   #if (__4WD_VARIANT_CODE==ENABLE) /* 4WD */
				
				else if((lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)&&(WL->ESC_CNT_SPIN_OK==0)
				&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>(wheel_speed_3rd+VREF_2_KPH_RESOL_CHANGE))
				&&(mtp>=10)&&(eng_torq_rel>=TORQ_PERCENT_30PRO)&&(ldabsu1CanErrFlg==0))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=47;
				}
				else if((lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)&&
					(WL->ESC_CNT_SPIN_OK==1)&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>wheel_speed_3rd))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=48;
				}
				else if(((lsu8DrvMode==DM_2WD)||(lsu8DrvMode==DM_2H))&&
					(WL->ESC_CNT_SPIN_OK==1)&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>wheel_speed_2nd))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=48;
				}		
			  
			  #elif	(__4WD==ENABLE)				   
				else if((WL->ESC_CNT_SPIN_OK==0)
				&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>(wheel_speed_3rd+VREF_2_KPH_RESOL_CHANGE))
				&&(mtp>=10)&&(eng_torq_rel>=TORQ_PERCENT_30PRO)&&(ldabsu1CanErrFlg==0))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=47;
				}
				else if((WL->ESC_CNT_SPIN_OK==1)&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>wheel_speed_3rd))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=48;
				}
				
			  #else	/* 2WD */
				
				else if((WL->ESC_CNT_SPIN_OK==1)&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>wheel_speed_2nd))
				{
					WL->ESC_CNT_SPIN_OK=1;
					WL->whl_reliability_chk_flag=48;
				}
				
			  #endif
				 
				else
				{
					WL->ESC_CNT_SPIN_OK=0;
					WL->whl_reliability_chk_flag=49;
				}				 
			}
			else
			{
				WL->ESC_CNT_SPIN_OK=0;
				WL->whl_reliability_chk_flag=52;
			}
		}
		else
		{
			WL->ESC_CNT_SPIN_OK=0;
			WL->whl_reliability_chk_flag=53;
		}
		#endif
		
		if(drive_whl_flg==1) /*Driven Wheel	Reliability*/
		{
			WL->whl_reliability_chk_flag=62;
			if(((WL->rel_lam>5)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR))
			||((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_2))
			{
				/*
				if((arad_diff>=10)||((arad_diff>0)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=63;
				}
			  #if __ENGINE_INFORMATION
				else if((mtp>=10)&&((drive_torq_rel>=TORQ_PERCENT_30PRO)||(eng_torq_rel>=TORQ_PERCENT_30PRO))
				&&(ldabsu1CanErrFlg==0)&&(WL->arad_avg>5))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=65;
				}
			  #endif
				else if((WL->rel_lam>10)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)&&(WL->arad_avg>0))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=68;
				}
				else
				{
					WL->whl_reliability_chk_flag=70;
				}
				*/
			  #if __ENGINE_INFORMATION
				if((mtp>=10)&&((drive_torq_rel>=TORQ_PERCENT_30PRO)||(eng_torq_rel>=TORQ_PERCENT_30PRO))
				&&(ldabsu1CanErrFlg==0)&&(WL->arad_avg>5))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=65;
				}
				else if((((arad_diff>=10)&&(WL->arad>=ARAD_0G5)&&(arad_diff2>=5))||((arad_diff>0)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)))&&(ldabsu1CanErrFlg==1))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=63;
				}
				else if((WL->rel_lam>10)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)&&(WL->arad_avg>0)&&(ldabsu1CanErrFlg==1))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=68;
				}
				else
				{
					WL->whl_reliability_chk_flag=70;
				}
			  #else
				if(((arad_diff>=10)&&(WL->arad>=ARAD_0G5)&&(arad_diff2>=5))||((arad_diff>0)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=63;
				}
				else if((WL->rel_lam>10)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3)&&(WL->arad_avg>0))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=68;
				}
				else
				{
					WL->whl_reliability_chk_flag=70;
				}				
			  #endif				
			}
			else if	(((WL->rel_lam<-10)&&((wheel_speed-whl_ref_spd)<WHEEL_DRAG_DET_THR)&&(WL->arad_avg<=-30))
			||((WL->rel_lam<-15)&&((wheel_speed-whl_ref_spd)<WHEEL_DRAG_DET_THR3))
			||(((wheel_speed-non_driven_wheel_left)<WHEEL_DRAG_DET_THR2)&&((wheel_speed-non_driven_wheel_right)<WHEEL_DRAG_DET_THR2)
			&&((WL->vrad_old-WL->vrad)>=2)))
			{
				#if	__ENGINE_INFORMATION
				if(((mtp<10)&&((drive_torq_rel<=TORQ_PERCENT_20PRO)||(eng_torq_rel<=TORQ_PERCENT_20PRO))
				&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))
				{
				#endif
				
					WL->whl_reliability_chk_flag=72;
				
					#if	__VDC
					if(ESP_BRAKE_CONTROL_WL==0)
					{
					#endif
						WL->whl_reliability_chk_cnt++;
						WL->whl_reliability_chk_flag=73;
					#if	__VDC
					}
					else{}
					#endif	 
					
				#if	__ENGINE_INFORMATION
				}
				else
				{
					WL->whl_reliability_chk_flag=74;
				}
				#endif
			}
			else if(WL->whl_reliability_chk_cnt>0)
			{
				WL->whl_reliability_chk_cnt--;
				WL->whl_reliability_chk_flag=76;
			}
			else
			{
				WL->whl_reliability_chk_flag=77;
			}
		
			if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_40MS)
			{
				WL->DRIVING_SPIN_OK=1;
				WL->whl_reliability_chk_cnt=5;
			}
			else if(WL->whl_reliability_chk_cnt==0)
			{
				WL->DRIVING_SPIN_OK=0; 
			}
			else{}
		}
		else  /*Non-Driven Wheel Reliability ->	Tip	up*/
		{
			WL->whl_reliability_chk_flag=78;
			if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK)
			 &&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK)
			 &&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK))
			{
			  #if __VDC
				if((WL->LEFT_WHEEL==1)&&(alat>LAT_0G7G)&&(fu1AySusDet==0)&&(fu1AyErrorDet==0))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=82;
				}
				else if((WL->LEFT_WHEEL==0)&&(alat<(-LAT_0G7G))&&(fu1AySusDet==0)&&(fu1AyErrorDet==0))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=84;
				}
				else if((fu1AySusDet==1)||(fu1AyErrorDet==1))
				{
					WL->whl_reliability_chk_cnt++;
					WL->whl_reliability_chk_flag=83;
				}
				else	
				{
					WL->whl_reliability_chk_flag=85;
				}
			  #else
				WL->whl_reliability_chk_cnt++;
				WL->whl_reliability_chk_flag=80;
			  #endif
			}
			else
			{
				WL->whl_reliability_chk_flag=89;
				if(WL->whl_reliability_chk_cnt>0)
				{
					WL->whl_reliability_chk_cnt--;
				}
				else{}
			}
			
			if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_500MS) /*	500ms */
			{
				WL->SPECIAL_SPIN_OK=1;
				WL->whl_reliability_chk_cnt=10;
			}
			else if(WL->whl_reliability_chk_cnt==0)
			{
				WL->SPECIAL_SPIN_OK=0;	  
			}
			else{}
		}
 
	   /**********************************************************************************/
 
		if((WL->ESC_BRAKE_SLIP_OK==1)||(WL->ESC_CNT_SPIN_OK==1)
		   ||(WL->SPECIAL_SPIN_OK==1)||(WL->DRIVING_SPIN_OK==1)||(WL->RELIABLE_FOR_VREF2==0))
		{
			WL->RELIABLE_FOR_VREF=0;
		}
		else
		{
			WL->RELIABLE_FOR_VREF=1;	
		}
		
	   /**********************************************************************************/		 

		/*RELIABLE_FOR_VREF2 */
		tempB0=0;
		tempW1=(int16_t)(((int32_t)wheel_speed_diff_1*100)/vehicle_speed);

		
		if((tempW1>=(S16_RTA_DET_THRES/10))&&(tempW1<(S16_RTA_DET_THRES_MAX/10)))
		{
			tempB0++;
		}
		else{}
			
		tempW2=(int16_t)(((int32_t)wheel_speed_diff_2*100)/vehicle_speed);

		if((tempW2>=(S16_RTA_DET_THRES/10))&&(tempW2<(S16_RTA_DET_THRES_MAX/10)))
		{
			tempB0++;
		}
		else{}
			
		tempW3=(int16_t)(((int32_t)wheel_speed_diff_3*100)/vehicle_speed);

		if((tempW3>=(S16_RTA_DET_THRES/10))&&(tempW3<(S16_RTA_DET_THRES_MAX/10)))
		{
			tempB0++;
		}
		else{}

		if(WL->RELIABLE_FOR_VREF2==1)
		{
			#if	__VDC
			if(VDC_REF_WORK==0)
			{
			#endif
				if(other_3whl_diff<THRES_FOR_NON_DRV_WL_CHECK3)
				{
					if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(McrAbs(WL->arad)<ARAD_1G0)&&(tempB0==3))
					{
						WL->whl_reliability_chk_cnt2++;
					}
					else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK3)
					&&(WL->whl_reliability_chk_cnt2>0))
					{
						WL->whl_reliability_chk_cnt2--;
					}
					else{}
				}
				else{}
					
				if((WL->whl_reliability_chk_cnt2>L_U8_TIME_10MSLOOP_1450MS)||
				((WL->whl_reliability_chk_cnt2>L_U8_TIME_10MSLOOP_1050MS)&&(drive_whl_flg==0)))
				{
					WL->RELIABLE_FOR_VREF2=0;
					WL->whl_reliability_chk_cnt2=0;
				}
				else{}
					
			#if	__VDC
			}
			else{}
			#endif
		}
		else
		{
			#if	__VDC
			if(VDC_REF_WORK==0)
			{
			#endif
			
				if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
				&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2)
				&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2))
				{
					WL->whl_reliability_chk_cnt2+=2;
				}
				else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
				&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2))
				{
					WL->whl_reliability_chk_cnt2++;
				}
				else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
				&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2))
				{
					WL->whl_reliability_chk_cnt2++;
				}
				else if((wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2)
				&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2))
				{
					WL->whl_reliability_chk_cnt2++;
				}
				else if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK)
				&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK)
				&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK)
				&&(WL->whl_reliability_chk_cnt2>0))
				{
					WL->whl_reliability_chk_cnt2--;
				}
				else{}
				
				if(WL->whl_reliability_chk_cnt2>L_U8_TIME_10MSLOOP_700MS)
				{
					WL->RELIABLE_FOR_VREF2=1;
					WL->whl_reliability_chk_cnt2=0;
				}
				else{}
				
			#if	__VDC
			}
			else{}
			#endif
		}
	}

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
	{
		if(drive_whl_flg==0)
		{
			WL->RELIABLE_FOR_VREF=1;
		}
		else
		{
			WL->RELIABLE_FOR_VREF=0;
		}
		WL->RELIABLE_FOR_VREF2=1;
		WL->whl_reliability_chk_cnt=0;
		WL->whl_reliability_chk_cnt2=0;
	}
	else{}	
		
	if(WL->SPIN_WHEEL==1) /* creep noise */
	{
		WL->whl_reliability_chk_cnt=0;
		WL->RELIABLE_FOR_VREF=0;
	}
  #if __PARKING_BRAKE_OPERATION
	else if(WL->PARKING_BRAKE_WHEEL==1)
	{
		WL->whl_reliability_chk_cnt=0;
		WL->RELIABLE_FOR_VREF=0;
	}
  #endif
	else{}		
}

#else /* !if __MGH60_NEW_SELECTION */

void LDABS_vDetWheelReliability(struct W_STRUCT	*WL,struct W_STRUCT	*WL_CNT	)
{
	int8_t drive_whl_flg;
	int16_t	arad_diff;
	int16_t	whl_ref_spd;
	int16_t	vehicle_speed;
	int16_t	wheel_speed;
	int16_t	cnter_whl_speed;
	int16_t	wheel_speed_diff_1;
	int16_t	wheel_speed_diff_2;
	int16_t	wheel_speed_diff_3;
	int16_t	non_driven_wheel_left;
	int16_t	non_driven_wheel_right;
	int16_t	driven_wheel_left;
	int16_t	driven_wheel_right;
	int16_t	vehicle_speed_variation;
	int16_t	wheel_diff;

	int8_t ldvrefu1DefDrivenWhl;
	#if	__ACCEL_ON_BRK_VREF
	int8_t ldvrefu1SatisfyEngCondition=0;
	int8_t ldvrefu1SatisfyDvCondition=0;
	int16_t	 ldvrefs16AccelonBrkDvThr1;
	int16_t	 ldvrefs16AccelonBrkDvThr2;
	int16_t	 ldvrefs16AccelonBrkDvThr3;
	int16_t	 ldvrefs16AccelonBrkEngTorqThr;
	int16_t	 ldvrefs16AccelonBrkMtpThr;
	int16_t	 ldvrefs16AccelonBrkDetTime;
	#endif

	int16_t	other_3whl_diff;

	vehicle_speed=vref_resol_change;
	wheel_speed=WL->vrad_crt_resol_change;
	cnter_whl_speed=WL_CNT->vrad_crt_resol_change;
	vehicle_speed_variation=vref_resol_change-vref_alt_resol_change;
	wheel_diff=vrad_diff_max_resol_change;
		#if	__REAR_D
	non_driven_wheel_left=FL.vrad_crt_resol_change;
	non_driven_wheel_right=FR.vrad_crt_resol_change;
	driven_wheel_left=RL.vrad_crt_resol_change;
	driven_wheel_right=RR.vrad_crt_resol_change;
		#else
	non_driven_wheel_left=RL.vrad_crt_resol_change;
	non_driven_wheel_right=RR.vrad_crt_resol_change;
	driven_wheel_left=FL.vrad_crt_resol_change;
	driven_wheel_right=FR.vrad_crt_resol_change;
		#endif

		#if	__VDC
	whl_ref_spd=WL->wvref_resol_change;
		#else
	whl_ref_spd=vref_resol_change;
		#endif

	if(WL->REAR_WHEEL==1)
	{
		wheel_speed_diff_1=wheel_speed-FL.vrad_crt_resol_change;
		wheel_speed_diff_2=wheel_speed-FR.vrad_crt_resol_change;
		if(WL->LEFT_WHEEL==1)
		{
			wheel_speed_diff_3=wheel_speed-RR.vrad_crt_resol_change;
		}
		else
		{
			wheel_speed_diff_3=wheel_speed-RL.vrad_crt_resol_change;
		}
	}
	else
	{
		wheel_speed_diff_1=wheel_speed-RL.vrad_crt_resol_change;
		wheel_speed_diff_2=wheel_speed-RR.vrad_crt_resol_change;
		if(WL->LEFT_WHEEL==1)
		{
			wheel_speed_diff_3=wheel_speed-FR.vrad_crt_resol_change;
		}
		else
		{
			wheel_speed_diff_3=wheel_speed-FL.vrad_crt_resol_change;
		}
	}



	tempW0=McrAbs(wheel_speed_diff_1-wheel_speed_diff_2);
	tempW1=McrAbs(wheel_speed_diff_2-wheel_speed_diff_3);
	tempW2=McrAbs(wheel_speed_diff_3-wheel_speed_diff_1);

	if(tempW0>tempW1)
	{
		tempW3=tempW0;
	}
	else
	{
		tempW3=tempW1;
	}

	if(tempW2>tempW3)
	{
		tempW3=tempW2;
	}

	other_3whl_diff=tempW3;

	ldvrefu1DefDrivenWhl=0;
	#if	(__4WD_VARIANT_CODE==ENABLE)
	if ((lsu8DrvMode ==DM_2WD)||(lsu8DrvMode ==DM_2H))
	{
		#if	__REAR_D
		if(REAR_WHEEL_wl==1)
		#else
		if(REAR_WHEEL_wl==0)
		#endif
		{
			drive_whl_flg=1;
		}
		else
		{
			drive_whl_flg=0;
		}
		arad_diff=WL->arad_avg-ebd_refilt_grv;
		ldvrefu1DefDrivenWhl=1;
	}
	  #if __ACCEL_ON_BRK_VREF
	else if(lsu8DrvMode	==DM_2H)
	{
		#if	__REAR_D
		if(REAR_WHEEL_wl==1)
		#else
		if(REAR_WHEEL_wl==0)
		#endif
		{
			drive_whl_flg=1;
		}
		else
		{
			drive_whl_flg=0;
		}
		arad_diff=WL->arad_avg-ebd_refilt_grv;
		ldvrefu1DefDrivenWhl=1;
	}
	else if(lsu8DrvMode	==DM_AUTO)
	{
		if(ABS_fz==1)
		{
			#if	__REAR_D
			if(REAR_WHEEL_wl==1)
			#else
			if(REAR_WHEEL_wl==0)
			#endif
			{
				drive_whl_flg=1;
			}
			else
			{
				drive_whl_flg=0;
			}
			ldvrefu1DefDrivenWhl=1;
		}
		else
		{
			drive_whl_flg=1;
			ldvrefu1DefDrivenWhl=0;
		}
		arad_diff=WL->arad_avg-ax_Filter;
	}
	else if(lsu8DrvMode	==DM_4H)
	{
		drive_whl_flg=1;
		arad_diff=WL->arad_avg-ax_Filter;
		ldvrefu1DefDrivenWhl=0;
	}
	else
	{
		;
	}
	  #else
	else
	{
		drive_whl_flg=1;
		arad_diff=WL->arad_avg-ax_Filter;
		ldvrefu1DefDrivenWhl=0;
	}
	  #endif
	#else
	  #if __4WD
		drive_whl_flg=1;
		arad_diff=WL->arad_avg-ax_Filter;
		ldvrefu1DefDrivenWhl=0;
	  #else
		#if	__REAR_D
		if(REAR_WHEEL_wl==1)
		#else
		if(REAR_WHEEL_wl==0)
		#endif
		{
			drive_whl_flg=1;
		}
		else
		{
			drive_whl_flg=0;
		}
		arad_diff=WL->arad_avg-ebd_refilt_grv;
		ldvrefu1DefDrivenWhl=1;
	  #endif
	#endif
/*
	RELIABLE_FOR_VREF==1 : Reliabile
	RELIABLE_FOR_VREF==0 : UnReliabile
*/

/*	 WL->whl_reliability_chk_flag=0;*/

	if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
	{
/*		 WL->whl_reliability_chk_flag=1;*/
		if(WL->RELIABLE_FOR_VREF==0)
		{
			/* REAL	ABS	State*/

			if(ABS_fz_K==1)
			{
				WL->UNRELIABLE_AT_ABS_ENTRY=1;
			}
			if((BRAKE_SIGNAL_K==1)||(BRAKING_STATE_OLD==0))
			{
				WL->UNRELIABLE_AT_BRAKE_ENTRY=1;
			}
			/*---------------------------------------
			Reliabile Condition	Accel to ABS State
			-----------------------------------------
			1. (vrad-vref)<THR
			2. (vrad-ABS_sntry_speed)<THR && (vrad-CBS_sntry_speed)<THR
			3. when	conditon 1 or 2	is satisfied, increase CNT.
			4. CNT>THR --> WHEEL RELIAIBILITY O.K
			------------------------------------------*/
/*			 WL->whl_reliability_chk_flag=2;*/
			if(WL->RELIABLE_FOR_VREF2==1)
			{
/*				WL->whl_reliability_chk_flag=3;*/
				if((WL->UNRELIABLE_AT_ABS_ENTRY==1)||(WL->UNRELIABLE_AT_BRAKE_ENTRY==1))
				{
					if((wheel_speed-vehicle_speed)<WHEEL_RELIABLE_THR_BRAKE_ENTRY)
					{
						WL->whl_reliability_chk_cnt+=2;
/*						WL->whl_reliability_chk_flag=4;*/
					}
					else if((wheel_speed-ABS_entry_speed2)<0)
					{
						WL->whl_reliability_chk_cnt++;
/*						 WL->whl_reliability_chk_flag=5;*/
					}
					else
					{
/*						WL->whl_reliability_chk_flag=6;*/
					}

					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
					{
						WL->whl_reliability_chk_cnt=0;
						WL->UNRELIABLE_AT_ABS_ENTRY=0;
						WL->UNRELIABLE_AT_BRAKE_ENTRY=0;
						WL->RELIABLE_FOR_VREF=1;
					}
				}
				/*Reliable Condition after ABS to Accel	State*/
				else
				{
/*					WL->whl_reliability_chk_flag=7;*/
					#if	__ACCEL_ON_BRK_VREF

					ldvrefu1SatisfyDvCondition=0;
					ldvrefu1SatisfyEngCondition=0;


					ldvrefs16AccelonBrkDvThr1=VREF_1_5_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkEngTorqThr=TORQ_PERCENT_20PRO;
					ldvrefs16AccelonBrkMtpThr=MTP_5_P;
					ldvrefs16AccelonBrkDetTime=L_U8_TIME_10MSLOOP_70MS;


					if(ldvrefu1DefDrivenWhl==1)
					{
/*						WL->whl_reliability_chk_flag=8;*/
						if(((wheel_speed-non_driven_wheel_right)<ldvrefs16AccelonBrkDvThr1)||
							((wheel_speed-non_driven_wheel_left)<ldvrefs16AccelonBrkDvThr1)||
							((wheel_speed-vehicle_speed)<0))
						{
							ldvrefu1SatisfyDvCondition=1;
/*							WL->whl_reliability_chk_flag=9;*/
						}
					}
					else if(((wheel_speed-wheel_speed_3rd)<ldvrefs16AccelonBrkDvThr1)
						||((wheel_speed-vehicle_speed)<0))
					{
						ldvrefu1SatisfyDvCondition=1;
/*						WL->whl_reliability_chk_flag=10;*/
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
/*						WL->whl_reliability_chk_flag=11;*/
					}

					#if	__ENGINE_INFORMATION
					if((mtp<=ldvrefs16AccelonBrkMtpThr)&&(eng_torq_rel<ldvrefs16AccelonBrkEngTorqThr))
					{
						ldvrefu1SatisfyEngCondition=1;
					}
					else
					{
						ldvrefu1SatisfyEngCondition=0;
					}
					#endif

					#if	__ENGINE_INFORMATION
					if((ldvrefu1SatisfyDvCondition==1)&&(ldvrefu1SatisfyEngCondition==1))
					#else
					if(ldvrefu1SatisfyDvCondition==1)
					#endif
					{
						WL->whl_reliability_chk_cnt++;
						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=1;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
					}

					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)
					#else
					if((wheel_speed-vehicle_speed)<0)
					{
						#if	__ENGINE_INFORMATION
						if((mtp<5)&&(eng_torq_rel<TORQ_PERCENT_20PRO)&&(ldabsu1CanErrFlg==0))
						{
						#endif
							WL->RELIABLE_FOR_VREF=1;
							WL->whl_reliability_chk_cnt=0;
/*							WL->whl_reliability_chk_flag=4;*/
						#if	__ENGINE_INFORMATION
						}
						else if	(((wheel_speed-non_driven_wheel_left)<0)|| ((wheel_speed-non_driven_wheel_right)<0))
						{
							WL->RELIABLE_FOR_VREF=1;
							WL->whl_reliability_chk_cnt=0;
/*							WL->whl_reliability_chk_flag=5;*/
						}
						else
						{
/*							WL->whl_reliability_chk_flag=6;*/
						}
						#endif
					}
					#endif
				}
			}
			else
			{
/*				WL->whl_reliability_chk_flag=7;*/
/*				WL->whl_reliability_chk_flag=12;*/
			}
		}
		else
		{
/*			WL->whl_reliability_chk_flag=13;*/
			if(drive_whl_flg==1)
			{
/*				WL->whl_reliability_chk_flag=14;*/
				/*UnReliable Condition Due to  ABS to Accel	State*/
				#if	__ENGINE_INFORMATION
				if(ldabsu1CanErrFlg==0)
				{
/*					WL->whl_reliability_chk_flag=15;*/
					#if	__ACCEL_ON_BRK_VREF
					ldvrefu1SatisfyDvCondition=0;
					ldvrefu1SatisfyEngCondition=0;


					ldvrefs16AccelonBrkDvThr1=VREF_2_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr2=VREF_0_25_KPH_RESOL_CHANGE;

					ldvrefs16AccelonBrkEngTorqThr=TORQ_PERCENT_15PRO;
					ldvrefs16AccelonBrkMtpThr=MTP_10_P;
					ldvrefs16AccelonBrkDetTime=L_U8_TIME_10MSLOOP_70MS;


					if(ldvrefu1DefDrivenWhl==1)
					{
/*						WL->whl_reliability_chk_flag=16;*/
						if(((wheel_speed-non_driven_wheel_right)>ldvrefs16AccelonBrkDvThr1)&&
							((wheel_speed-non_driven_wheel_left)>ldvrefs16AccelonBrkDvThr1)&&
							((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
						{
							ldvrefu1SatisfyDvCondition=1;
/*							WL->whl_reliability_chk_flag=17;*/
						}

						if(((wheel_speed-non_driven_wheel_right)>(ldvrefs16AccelonBrkDvThr1*2))&&
							((wheel_speed-non_driven_wheel_left)>(ldvrefs16AccelonBrkDvThr1*2))&&
							((wheel_speed-vehicle_speed)>(ldvrefs16AccelonBrkDvThr2*2)))
						{
							ldvrefu1SatisfyDvCondition=2;
/*							WL->whl_reliability_chk_flag=18;*/
						}
					}
					else if(((wheel_speed-wheel_speed_3rd)>ldvrefs16AccelonBrkDvThr1)
						&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
					{
						ldvrefu1SatisfyDvCondition=1;
/*						WL->whl_reliability_chk_flag=19;*/
						if(((wheel_speed-wheel_speed_3rd)>(ldvrefs16AccelonBrkDvThr1*2))
						&&((wheel_speed-vehicle_speed)>(ldvrefs16AccelonBrkDvThr2*2)))
						{
							ldvrefu1SatisfyDvCondition=2;
/*							WL->whl_reliability_chk_flag=20;*/
						}
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
/*						WL->whl_reliability_chk_flag=21;*/
					}


					if((mtp>ldvrefs16AccelonBrkMtpThr)&&(eng_torq_rel>ldvrefs16AccelonBrkEngTorqThr))
					{
						ldvrefu1SatisfyEngCondition=1;

						if((mtp>(ldvrefs16AccelonBrkMtpThr*2))&&(eng_torq_rel>(ldvrefs16AccelonBrkEngTorqThr*2)))
						{
							ldvrefu1SatisfyEngCondition=2;
						}
					}
					else
					{
						ldvrefu1SatisfyEngCondition=0;
					}

					if((ldvrefu1SatisfyEngCondition>=1)&&(ldvrefu1SatisfyDvCondition>=1))
					{
/*						WL->whl_reliability_chk_flag=22;*/
						if((vehicle_speed_variation>0)&&(WL->arad>ARAD_0G5))
						{
/*							WL->whl_reliability_chk_flag=23;*/
							WL->whl_reliability_chk_cnt++;
							if((ldvrefu1SatisfyEngCondition>=2)||(ldvrefu1SatisfyDvCondition>=2))
							{
								WL->whl_reliability_chk_cnt++;
							}
						}


						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=0;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=25;*/
					}
					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)

					#else
					if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
					{
						WL->whl_reliability_chk_cnt++;
/*						WL->whl_reliability_chk_flag=9;*/
					}
					else if((mtp>10)&&(eng_torq_rel>TORQ_PERCENT_20PRO))
					{
						if(((wheel_speed-vehicle_speed)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&(vehicle_speed_variation>=0))
						{
/*							WL->whl_reliability_chk_flag=10;	*/
							if((wheel_speed-vehicle_speed)>WHEEL_RELIABLE_THR_ABS2ACCEL_2 )
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=11;*/
							}
							WL->whl_reliability_chk_cnt++;
						}
						if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
						{
							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=12;*/
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=13;*/
					}
					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
					}
					else
					{
						;
					}
					#endif
				}
				else
				{
				#endif
					#if	__ACCEL_ON_BRK_VREF
					ldvrefu1SatisfyDvCondition=0;



					ldvrefs16AccelonBrkDvThr1=VREF_3_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr2=VREF_0_75_KPH_RESOL_CHANGE;
					ldvrefs16AccelonBrkDvThr3=VREF_0G125_KPH_RESOL_CHANGE;


					ldvrefs16AccelonBrkDetTime=L_U8_TIME_10MSLOOP_140MS;
/*					WL->whl_reliability_chk_flag=26;*/
					if(ldvrefu1DefDrivenWhl==1)
					{
/*						WL->whl_reliability_chk_flag=27;*/

						if(((wheel_speed-non_driven_wheel_right)>ldvrefs16AccelonBrkDvThr1)
							&&((wheel_speed-non_driven_wheel_left)>ldvrefs16AccelonBrkDvThr1)
							&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
						{
							ldvrefu1SatisfyDvCondition=1;
/*							WL->whl_reliability_chk_flag=28;*/
						}
					}
					else if(((wheel_speed-wheel_speed_3rd)>ldvrefs16AccelonBrkDvThr1)
						&&((wheel_speed-vehicle_speed)>ldvrefs16AccelonBrkDvThr2))
					{
						ldvrefu1SatisfyDvCondition=1;
/*						WL->whl_reliability_chk_flag=29;*/
					}
					else
					{
						ldvrefu1SatisfyDvCondition=0;
/*						WL->whl_reliability_chk_flag=30;*/
					}

					if(ldvrefu1SatisfyDvCondition==1)
					{
/*						WL->whl_reliability_chk_flag=31;*/
						if((vehicle_speed_variation>=ldvrefs16AccelonBrkDvThr3)&&(WL->arad>ARAD_1G0))
						{
							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=32;*/

						}

						if(WL->whl_reliability_chk_cnt>ldvrefs16AccelonBrkDetTime)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=0;
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=33;*/
					}
					#elif(__CAR==GM_M300)&&(__ECU==ABS_ECU_1)
					#else
					if(((wheel_speed-ABS_entry_speed)>0)&&(BRAKING_STATE==1))
					{
						WL->whl_reliability_chk_cnt++;
/*						WL->whl_reliability_chk_flag=14;*/
					}

					if((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL)
					{
						if((ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR)&&(WL->arad>=ABS2ACCEL_DET_ARAD_THR))
						{
							#if	__4WD_VARIANT_CODE==ENABLE
							if(lsu8DrvMode !=DM_2WD)
							{
/*								WL->whl_reliability_chk_flag=17;*/
								if(((wheel_speed-non_driven_wheel_left)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&
								((wheel_speed-non_driven_wheel_right)>WHEEL_RELIABLE_THR_ABS2ACCEL))
								{
									WL->whl_reliability_chk_cnt++;
/*									WL->whl_reliability_chk_flag=15;	*/
									if(((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL_2)
									&&(ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR2))
									{
										WL->whl_reliability_chk_cnt++;
/*										WL->whl_reliability_chk_flag=16;	*/
									}
								}
							}
							else
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=18;*/
							}
							#else
								#if	__4WD
								if(((wheel_speed-non_driven_wheel_left)>WHEEL_RELIABLE_THR_ABS2ACCEL)&&
								((wheel_speed-non_driven_wheel_right)>WHEEL_RELIABLE_THR_ABS2ACCEL))
								{
									WL->whl_reliability_chk_cnt++;
/*									WL->whl_reliability_chk_flag=19;*/
									if(((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ABS2ACCEL_2)
									&&(ebd_refilt_grv>ABS2ACCEL_DET_ACCEL_THR2))
									{
										WL->whl_reliability_chk_cnt++;
/*										WL->whl_reliability_chk_flag=20;*/
									}
								}
								#else
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=21;*/
								#endif
							#endif
						}
						else
						{
							if(WL->whl_reliability_chk_cnt>0)
							{
								WL->whl_reliability_chk_cnt--;
/*								WL->whl_reliability_chk_flag=22;*/
							}
							else
							{
								WL->whl_reliability_chk_cnt=0;
/*								WL->whl_reliability_chk_flag=23;*/
							}
						}
					}
					else
					{
						if((wheel_speed-vehicle_speed)<0)
						{
							WL->whl_reliability_chk_cnt=0;
/*							WL->whl_reliability_chk_flag=24;*/
						}
						else
						{
							if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_15MS)
							{
								WL->whl_reliability_chk_cnt-=2;
/*								WL->whl_reliability_chk_flag=25;*/
							}
							else
							{
								WL->whl_reliability_chk_cnt=0;
/*								WL->whl_reliability_chk_flag=26;*/
							}
						}
					}

					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_105MS)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
					}
					else
					{
						;
					}
					#endif

				#if	__ENGINE_INFORMATION
				}
				#endif
			}
			else
			{
/*				WL->whl_reliability_chk_flag=34;*/
			}


		}

	}
	else
	{
		WL->UNRELIABLE_AT_ABS_ENTRY=0;
		WL->UNRELIABLE_AT_BRAKE_ENTRY=0;
/*		WL->whl_reliability_chk_flag=35;*/
		if(WL->RELIABLE_FOR_VREF==1)
		{
/*			WL->whl_reliability_chk_flag=36;*/
			/*Not ABS State	& Not Brake	Signal*/
			#if	__VDC
			if((VDC_REF_WORK==1)&&(ESP_BRAKE_CONTROL_WL==1))
			{
/*				WL->whl_reliability_chk_flag=37;*/

				if((wheel_speed<cnter_whl_speed)&&(wheel_speed<=wheel_speed_3rd))
				{

/*					WL->whl_reliability_chk_flag=38;*/
					if(((WL->rel_lam<=(-LAM_3P))
					||((whl_ref_spd-wheel_speed)>=WHEEL_RELIABLE_THR_ESP))
					&&(WL->arad<=ESP_RELIABILITY_SET_DECEL)&&(WL->esp_rise_cnt>L_U8_TIME_10MSLOOP_20MS))
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=39;*/
					}
					else if(((WL->rel_lam<=(-LAM_5P))
					&&((whl_ref_spd-wheel_speed)>=WHEEL_RELIABLE_THR_ESP))&&(WL->esp_rise_cnt>L_U8_TIME_10MSLOOP_70MS))
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=40;*/
					}
					else if(WL->esp_rise_cnt>L_U8_TIME_10MSLOOP_350MS)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=41;*/
					}
					else if((wheel_speed_diff_1<(-WHL_DIF_RELIABLE_THR))
					&&(wheel_speed_diff_2<(-WHL_DIF_RELIABLE_THR))
					&&(wheel_speed_diff_3<(-WHL_DIF_RELIABLE_THR)))
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=41;*/
					}
					else
					{
/*						WL->whl_reliability_chk_flag=42;*/
					}

				}
				else
				{
/*					WL->whl_reliability_chk_flag=43;*/
				}

			}
			else if	((VDC_REF_WORK==1)&&(ESP_BRAKE_CONTROL_CNT_WL==1))
			{
/*				WL->whl_reliability_chk_flag=44;*/

				if((WL_CNT->esp_rise_cnt>L_U8_TIME_10MSLOOP_20MS)&&(drive_whl_flg==1)
				&&(wheel_speed>cnter_whl_speed)&&(wheel_speed>=wheel_speed_2nd))

				{
/*					WL->whl_reliability_chk_flag=45;*/
					if(((WL->rel_lam>=(LAM_3P))||((wheel_speed-whl_ref_spd)>=WHEEL_RELIABLE_THR_ESP_CNT))
					&&(WL->arad>=ESP_CNT_RELIABILITY_SET_ACCEL_1))
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=46;*/
					}
					else if	(((wheel_speed-non_driven_wheel_left)>WHEEL_RELIABLE_THR_ESP_CNT_2)
					&&((wheel_speed-non_driven_wheel_right)>WHEEL_RELIABLE_THR_ESP_CNT_2))
					{
						if((WL->arad>=ESP_CNT_RELIABILITY_SET_ACCEL_2)
						&&((wheel_speed-whl_ref_spd)>0))
						{
							 WL->RELIABLE_FOR_VREF=0;
							 WL->whl_reliability_chk_cnt=0;
/*							 WL->whl_reliability_chk_flag=47;*/
						}
						else
						{
/*							WL->whl_reliability_chk_flag=48;*/
						}
					}
					else if	(((wheel_speed-driven_wheel_left)>WHEEL_RELIABLE_THR_ESP_CNT_2)
					&&((wheel_speed-driven_wheel_right)>WHEEL_RELIABLE_THR_ESP_CNT_2))
					{
						if((WL->arad>=ESP_CNT_RELIABILITY_SET_ACCEL_2)
						&&((wheel_speed-whl_ref_spd)>0))
						{
							WL->RELIABLE_FOR_VREF=0;
							WL->whl_reliability_chk_cnt=0;
/*							WL->whl_reliability_chk_flag=49;*/
						}
						else
						{
/*							WL->whl_reliability_chk_flag=50;*/
						}
					}
					else
					{
/*						WL->whl_reliability_chk_flag=51;*/
					}
				}
				else if(drive_whl_flg==0)
				{
/*					WL->whl_reliability_chk_flag=52;*/
					if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK)
					&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK)
					&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK))
					{
/*						WL->whl_reliability_chk_flag=53;*/
						if(WL->LEFT_WHEEL==1)
						{
/*							WL->whl_reliability_chk_flag=54;*/
							if((yaw_out>YAW_5DEG)&&(alat>LAT_0G5G))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=55;*/
							}
						}
						else
						{
/*							WL->whl_reliability_chk_flag=56;*/
							if((yaw_out<(-YAW_5DEG))&&(alat<(-LAT_0G5G)))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=57;*/
							}
						}
					}

					else if(WL->whl_reliability_chk_cnt>0)
					{
						WL->whl_reliability_chk_cnt--;
/*						WL->whl_reliability_chk_flag=58;*/
					}
					else
					{
						WL->whl_reliability_chk_cnt=0;
/*						WL->whl_reliability_chk_flag=59;*/
					}

					if(WL->whl_reliability_chk_cnt>10)
					{
						WL->whl_reliability_chk_cnt=0;
						WL->RELIABLE_FOR_VREF=0;
					}

				}
/*
				else if	((wheel_speed-vehicle_speed)>=WHEEL_RELIABLE_THR_ESP_CNT_3)
				{
					WL->RELIABLE_FOR_VREF=0;
				}
*/
				else
				{
/*					WL->whl_reliability_chk_flag=60;*/
				}
			}
			else
			{
			#endif
/*				WL->whl_reliability_chk_flag=61;*/
				/*Driven Wheel Reliability*/
				if(drive_whl_flg==1)
				{
/*					WL->whl_reliability_chk_flag=62;*/
					if((WL->rel_lam>5)||((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR))
					{
/*						WL->whl_reliability_chk_flag=63;*/
						#if	__ENGINE_INFORMATION
						if(((mtp>=10)&&((drive_torq_rel>=300)||(eng_torq_rel>=300))&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))
						{
						#endif
/*							WL->whl_reliability_chk_flag=64;*/
							if((arad_diff>=10)||
							((arad_diff>0)&&((WL->rel_lam>10)||((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_2))))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=65;*/
								if((WL->rel_lam>10)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3))
								{
									WL->whl_reliability_chk_cnt++;
/*									WL->whl_reliability_chk_flag=66;*/
								}
							}
							else
							{
/*								WL->whl_reliability_chk_flag=67;*/
							}

						#if	__ENGINE_INFORMATION
						}
						else if((WL->rel_lam>10)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_2)
						&&((wheel_speed-vehicle_speed)>WHEEL_SPIN_DET_THR))
						{
							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=68;*/
							if((WL->rel_lam>15)&&((wheel_speed-whl_ref_spd)>WHEEL_SPIN_DET_THR_3))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=69;*/
							}
						}
						else
						{
/*							WL->whl_reliability_chk_flag=70;*/
						}
						#endif
					}
					else if	((WL->rel_lam<-10)&&((wheel_speed-whl_ref_spd)<WHEEL_DRAG_DET_THR))
					{
/*						WL->whl_reliability_chk_flag=71;*/
						if(arad_diff<=-30)
						{
/*							WL->whl_reliability_chk_flag=72;*/
							#if	__ENGINE_INFORMATION
							if(((mtp<5)&&((drive_torq_rel<=200)||(eng_torq_rel<=200))&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))
							{
							#endif
							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=73;*/
							#if	__ENGINE_INFORMATION
							}
							#endif
						}
					}
					else if(((wheel_speed-non_driven_wheel_left)<WHEEL_DRAG_DET_THR2)&&((wheel_speed-non_driven_wheel_right)<WHEEL_DRAG_DET_THR2)
					&&((WL->vrad_old-WL->vrad)>=2)&&(ACTIVE_BRAKE_ACT==0))
					{
/*						WL->whl_reliability_chk_flag=74;*/
							#if	__ENGINE_INFORMATION
							if(((mtp<5)&&((drive_torq_rel<=100)||(eng_torq_rel<=100))&&(ldabsu1CanErrFlg==0))||(ldabsu1CanErrFlg==1))
							{
							#endif

							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=75;*/
							#if	__ENGINE_INFORMATION
							}
							#endif
					}
					else if(WL->whl_reliability_chk_cnt>0)
					{
						WL->whl_reliability_chk_cnt--;
/*						WL->whl_reliability_chk_flag=76;*/
					}
					else
					{
/*						WL->whl_reliability_chk_flag=77;*/
					}

					if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_35MS)
					{
						WL->whl_reliability_chk_cnt=0;
						WL->RELIABLE_FOR_VREF=0;
					}
				}
				/*Non-Driven Reliability check*/
				else
				{
/*					WL->whl_reliability_chk_flag=78;*/
					if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK2)
					&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK2)
					&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK2))
					{
						WL->whl_reliability_chk_cnt++;
/*						WL->whl_reliability_chk_flag=79;*/

						if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK)
						&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK)
						&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK))
						{
							WL->whl_reliability_chk_cnt+=2;
/*							WL->whl_reliability_chk_flag=80;*/
						}
						#if	__VDC
						if(WL->LEFT_WHEEL==1)
						{
/*							WL->whl_reliability_chk_flag=81;*/
							if((yaw_out>YAW_5DEG)&&(alat>LAT_0G5G))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=82;*/
							}
						}
						else
						{
/*							WL->whl_reliability_chk_flag=83;*/
							if((yaw_out<(-YAW_5DEG))&&(alat<(-LAT_0G5G)))
							{
								WL->whl_reliability_chk_cnt++;
/*								WL->whl_reliability_chk_flag=84;*/
							}
						}
						#endif
						if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_350MS)
						{
							WL->whl_reliability_chk_cnt=0;
							WL->RELIABLE_FOR_VREF=0;
						}
					}
					#if	!__4WD
					else if((wheel_speed_diff_1<(-THRES_FOR_NON_DRV_WL_CHECK))
					&&(wheel_speed_diff_2<(-THRES_FOR_NON_DRV_WL_CHECK))
					&&(wheel_speed_diff_3<(-THRES_FOR_NON_DRV_WL_CHECK)))
					{
/*						WL->whl_reliability_chk_flag=85;*/
						if((other_3whl_diff<THRES_FOR_NON_DRV_WL_CHECK2)&&(arad_diff<(-AFZ_0G2)))
						{
/*							WL->whl_reliability_chk_flag=86;*/
							WL->whl_reliability_chk_cnt++;
							if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
							{
								WL->whl_reliability_chk_cnt=0;
								WL->RELIABLE_FOR_VREF=0;
							}
						}
					}
					#elif (__4WD_VARIANT_CODE==ENABLE)
					else if((wheel_speed_diff_1<(-THRES_FOR_NON_DRV_WL_CHECK))
					&&(wheel_speed_diff_2<(-THRES_FOR_NON_DRV_WL_CHECK))
					&&(wheel_speed_diff_3<(-THRES_FOR_NON_DRV_WL_CHECK))
					&&((lsu8DrvMode	==DM_2WD)||(lsu8DrvMode	==DM_2H)))
					{
/*						WL->whl_reliability_chk_flag=87;*/
						if((other_3whl_diff<THRES_FOR_NON_DRV_WL_CHECK2)&&(arad_diff<(-AFZ_0G2)))
						{
/*							WL->whl_reliability_chk_flag=88;*/
							WL->whl_reliability_chk_cnt++;
							if(WL->whl_reliability_chk_cnt>L_U8_TIME_10MSLOOP_70MS)
							{
								WL->whl_reliability_chk_cnt=0;
								WL->RELIABLE_FOR_VREF=0;
							}
						}
					}
					#endif
					else
					{
/*						WL->whl_reliability_chk_flag=89;*/
						if(WL->whl_reliability_chk_cnt>0)
						{
							WL->whl_reliability_chk_cnt--;
						}
					}
				}

			#if	__VDC
			}
			#endif
		}
		else
		{
/*			WL->whl_reliability_chk_flag=90;*/
			#if	__VDC
			if(ESP_BRAKE_CONTROL_WL==0)
			{
			#endif
/*				WL->whl_reliability_chk_flag=91;*/
				if(McrAbs(wheel_speed-whl_ref_spd)<=WHL_RELIABLE_THR)
				{
					WL->whl_reliability_chk_cnt++;
/*					WL->whl_reliability_chk_flag=92;*/
					if(wheel_diff<WHL_RELIABLE_THR2)
					{
						WL->whl_reliability_chk_cnt++;
/*						WL->whl_reliability_chk_flag=93;*/
					}
					if((WL->whl_reliability_chk_cnt>=L_U8_TIME_10MSLOOP_20MS)&&(WL->RELIABLE_FOR_VREF2==1))
					{
						WL->RELIABLE_FOR_VREF=1;
						WL->whl_reliability_chk_cnt=0;
					}
				}
				else if(((McrAbs(wheel_speed_diff_1)<THRES_FOR_NON_DRV_WL_CHECK2)&&(McrAbs(wheel_speed_diff_2)<THRES_FOR_NON_DRV_WL_CHECK2))
					||((McrAbs(wheel_speed_diff_2)<THRES_FOR_NON_DRV_WL_CHECK2)&&(McrAbs(wheel_speed_diff_3)<THRES_FOR_NON_DRV_WL_CHECK2))
					||((McrAbs(wheel_speed_diff_3)<THRES_FOR_NON_DRV_WL_CHECK2)&&(McrAbs(wheel_speed_diff_1)<THRES_FOR_NON_DRV_WL_CHECK2)))
				{
/*					WL->whl_reliability_chk_flag=94;*/
					if(drive_whl_flg==1)
					{
						if(McrAbs(wheel_speed-whl_ref_spd)<=THRES_FOR_NON_DRV_WL_CHECK2)
						{
							WL->whl_reliability_chk_cnt++;
/*							WL->whl_reliability_chk_flag=95;*/
						}
						else
						{
/*							WL->whl_reliability_chk_flag=96;*/
						}
					}
					else
					{
						WL->whl_reliability_chk_cnt++;
/*						WL->whl_reliability_chk_flag=97;*/
					}

					if((WL->whl_reliability_chk_cnt>=L_U8_TIME_10MSLOOP_70MS)&&(WL->RELIABLE_FOR_VREF2==1))
					{
						WL->RELIABLE_FOR_VREF=1;
						WL->whl_reliability_chk_cnt=0;
					}
				}
				#if	__4WD_VARIANT_CODE==ENABLE
				else if(((lsu8DrvMode ==DM_4H)||(lsu8DrvMode ==DM_AUTO))&&(ACCEL_SPIN_FZ==1)
					&&(VDC_REF_WORK==0)&&(wheel_speed<vehicle_speed))
				{
					WL->whl_reliability_chk_cnt++;
/*					WL->whl_reliability_chk_flag=101;*/
					if((WL->whl_reliability_chk_cnt>=L_U8_TIME_10MSLOOP_70MS)&&(WL->RELIABLE_FOR_VREF2==1))
					{
						WL->RELIABLE_FOR_VREF=1;
						WL->whl_reliability_chk_cnt=0;
					}
				}
				#elif __4WD
				else if((ACCEL_SPIN_FZ==1)
					#if	__VDC
				&&(VDC_REF_WORK==0)
					#endif
				&&(wheel_speed<vehicle_speed))

				{
					WL->whl_reliability_chk_cnt++;
/*					WL->whl_reliability_chk_flag=102;*/
					if((WL->whl_reliability_chk_cnt>=L_U8_TIME_10MSLOOP_70MS)&&(WL->RELIABLE_FOR_VREF2==1))
					{
						WL->RELIABLE_FOR_VREF=1;
						WL->whl_reliability_chk_cnt=0;
					}
				}
				#endif
				else if(WL->whl_reliability_chk_cnt>0)
				{
					WL->whl_reliability_chk_cnt--;
/*					WL->whl_reliability_chk_flag=98;*/
				}
				else
				{
/*					WL->whl_reliability_chk_flag=99;*/
				}
			#if	__VDC
			}
			else
			{
/*				WL->whl_reliability_chk_flag=100;*/
			}
			#endif
		}

		/*RELIABLE_FOR_VREF2 */
		tempB0=0;
		tempW1=(int16_t)(((int32_t)wheel_speed_diff_1*100)/vehicle_speed);
		if((tempW1>=3)&&(tempW1<20))
		{
			tempB0++;
		}
		tempW2=(int16_t)(((int32_t)wheel_speed_diff_2*100)/vehicle_speed);
		if((tempW2>=3)&&(tempW2<20))
		{
			tempB0++;
		}
		tempW3=(int16_t)(((int32_t)wheel_speed_diff_3*100)/vehicle_speed);
		if((tempW3>=3)&&(tempW3<20))
		{
			tempB0++;
		}

		if(WL->RELIABLE_FOR_VREF2==1)
		{
			#if	__VDC
			if(VDC_REF_WORK==0)
			{
			#endif
				if((BRAKING_STATE==0)&&(other_3whl_diff<THRES_FOR_NON_DRV_WL_CHECK3))
				{
					if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK3)
					&&(McrAbs(WL->arad)<ARAD_1G0)&&(tempB0==3))
					{
						WL->whl_reliability_chk_cnt2++;

					}
					else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK3)
					&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK3))
					{
						if(WL->whl_reliability_chk_cnt2>0)
						{
							WL->whl_reliability_chk_cnt2--;
						}
					}
					else
					{
						;
					}
				}
				else
				{
					;
				}
				if((WL->whl_reliability_chk_cnt2>L_U16_TIME_10MSLOOP_1400MS)||
				((WL->whl_reliability_chk_cnt2>L_U8_TIME_10MSLOOP_1050MS)&&(drive_whl_flg==0)))
				{
					WL->RELIABLE_FOR_VREF2=0;
					WL->whl_reliability_chk_cnt2=0;
					if(WL->RELIABLE_FOR_VREF==1)
					{
						WL->RELIABLE_FOR_VREF=0;
						WL->whl_reliability_chk_cnt=0;
					}
				}
				else
				{
					;
				}
			#if	__VDC
			}
			#endif
		}
		else
		{
			if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
			&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2)
			&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2)&&(BRAKING_STATE==0))
			{
				WL->whl_reliability_chk_cnt2+=2;
			}
			else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
			&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2)&&(BRAKING_STATE==0))
			{
				WL->whl_reliability_chk_cnt2++;
			}
			else if((wheel_speed_diff_1<THRES_FOR_NON_DRV_WL_CHECK2)
			&&(wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2)&&(BRAKING_STATE==0))
			{
				WL->whl_reliability_chk_cnt2++;
			}
			else if((wheel_speed_diff_3<THRES_FOR_NON_DRV_WL_CHECK2)
			&&(wheel_speed_diff_2<THRES_FOR_NON_DRV_WL_CHECK2)&&(BRAKING_STATE==0))
			{
				WL->whl_reliability_chk_cnt2++;
			}
			else if((wheel_speed_diff_1>THRES_FOR_NON_DRV_WL_CHECK)
			&&(wheel_speed_diff_2>THRES_FOR_NON_DRV_WL_CHECK)
			&&(wheel_speed_diff_3>THRES_FOR_NON_DRV_WL_CHECK)&&(BRAKING_STATE==0))
			{
				if(WL->whl_reliability_chk_cnt2>0)
				{
					WL->whl_reliability_chk_cnt2--;
				}
			}
			else
			{
				;
			}

			if(WL->whl_reliability_chk_cnt2>L_U8_TIME_10MSLOOP_350MS)
			{
				WL->RELIABLE_FOR_VREF2=1;
				WL->whl_reliability_chk_cnt2=0;
			}
		}
	}

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
	{
		if(drive_whl_flg==0)
		{
			WL->RELIABLE_FOR_VREF=1;
		}
		else
		{
			WL->RELIABLE_FOR_VREF=0;
		}
		WL->RELIABLE_FOR_VREF2=1;
		WL->whl_reliability_chk_cnt=0;
		WL->whl_reliability_chk_cnt2=0;
	}

	if(WL->SPIN_WHEEL==1)
	{
		WL->whl_reliability_chk_cnt=0;
		WL->RELIABLE_FOR_VREF=0;
	}
	#if	__PARKING_BRAKE_OPERATION
	else if(WL->PARKING_BRAKE_WHEEL==1)
	{
		WL->whl_reliability_chk_cnt=0;
		WL->RELIABLE_FOR_VREF=0;
	}
	#endif
	else
	{
		;
	}
}
#endif /* if __MGH60_NEW_SELECTION */
/*=====================================================================================*/

/*******************************************************************/

/*******************************************************************/
void	LDABS_vCallDetWheelState(void)
{
/*
	LDABS_vDctWheelCreepNoise();
*/
	LDABS_vDctWheelCreepNoise(&FL,&RTA_FL);
	LDABS_vDctWheelCreepNoise(&FR,&RTA_FR);
	LDABS_vDctWheelCreepNoise(&RL,&RTA_RL);
	LDABS_vDctWheelCreepNoise(&RR,&RTA_RR);

	LCABS_vSetSLEWTIM(&FL); 
	LCABS_vSetSLEWTIM(&FR);
	LCABS_vSetSLEWTIM(&RL);
	LCABS_vSetSLEWTIM(&RR);
	
	#if	__VDC
	FL.esp_rise_cnt=esp_pressure_count_fl;
	FR.esp_rise_cnt=esp_pressure_count_fr;
	RL.esp_rise_cnt=esp_pressure_count_rl;
	RR.esp_rise_cnt=esp_pressure_count_rr;

	ESP_BRAKE_CONTROL_WL=ESP_BRAKE_CONTROL_FL;
	ESP_BRAKE_CONTROL_CNT_WL=ESP_BRAKE_CONTROL_FR;
	#endif

	LDABS_vDetWheelReliability(&FL,&FR);

	#if	__VDC
	ESP_BRAKE_CONTROL_WL=ESP_BRAKE_CONTROL_FR;
	ESP_BRAKE_CONTROL_CNT_WL=ESP_BRAKE_CONTROL_FL;
	#endif
	LDABS_vDetWheelReliability(&FR,&FL);

	#if	__VDC
	ESP_BRAKE_CONTROL_WL=ESP_BRAKE_CONTROL_RL;
	ESP_BRAKE_CONTROL_CNT_WL=ESP_BRAKE_CONTROL_RR;
	#endif
	LDABS_vDetWheelReliability(&RL,&RR);

	#if	__VDC
	ESP_BRAKE_CONTROL_WL=ESP_BRAKE_CONTROL_RR;
	ESP_BRAKE_CONTROL_CNT_WL=ESP_BRAKE_CONTROL_RL;
	#endif
	LDABS_vDetWheelReliability(&RR,&RL);


}
/*******************************************************************/
void LCABS_vCalcAbmittelfz(void)
{
    abmittel_fz=(int16_t)(((uint16_t)slew_rate_timer_fl<<2)+((uint16_t)slew_rate_timer_fr<<2)
                +((uint16_t)slew_rate_timer_rl)+((uint16_t)slew_rate_timer_rr));
}
/*******************************************************************/
void LCABS_vSetSLEWTIM(struct W_STRUCT	*WL)
{
    if((AV_DUMP_wl==1) && (UNDERVOLT_DETECT_ABS==0)) {
        WL->slew_rate_timer += 2;
        if(WL->slew_rate_timer > L_U8_TIME_10MSLOOP_230MS) {WL->slew_rate_timer=L_U8_TIME_10MSLOOP_230MS;}
    }
    else {
        if(ABS_wl==0) {WL->slew_rate_timer=0;}
        else {
            if(WL->slew_rate_timer != 0) {
                if(AV_HOLD_wl != 1) {
                    if(BUILT_wl == 1) {WL->slew_rate_timer--;}
                    else {
                        if(WL->slew_rate_timer >= 2) {WL->slew_rate_timer-=2;}
                        else {WL->slew_rate_timer=0;}
                    }
                }
            }
        }

    }
}
void LDABS_vCalcAbsDurationTime(void)
{  	
    if(ABS_fz==1)
    {
    	abs_duration_timer=abs_duration_timer+1;
    	if(abs_duration_timer>=L_U8_TIME_10MSLOOP_2S)
    	{
    		abs_duration_timer=L_U8_TIME_10MSLOOP_2S;
    	}
    	else{}
    }
    else
    {
    	abs_duration_timer=0;
    }
}
/*******************************************************************/
	#if	__4WD ||( __4WD_VARIANT_CODE==ENABLE)
/*--------------------------------------------------------------------------------*/
#if	(__AX_SENSOR==1)
void LDABS_vCalAccelOfWheel(void)
{
	int16_t		non_driven_WL1;
	int16_t		non_driven_WL2;
	int16_t		non_driven_WheelV;
	int16_t		filt_ein_new;
	int16_t		filter_out_new_v_1;

/*
	#if	__REAR_D
	 non_driven_WL1=FL.vrad_crt_resol_change;
	 non_driven_WL2=FR.vrad_crt_resol_change;
	#else
	 non_driven_WL1=RL.vrad_crt_resol_change;
	 non_driven_WL2=RR.vrad_crt_resol_change;
	#endif

	if(non_driven_WL1 >	non_driven_WL2)
	{
	  non_driven_WheelV	= non_driven_WL2;
	}
	else
	{
	  non_driven_WheelV	= non_driven_WL1;
	}
*/
	non_driven_WheelV=FZ1.vrselect_resol_change;

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		filt_ein_new=0;
		filter_out_new_1=0;
		filter_out_new_v_1=0;
		filter_out_new_rest_1=0;
		voptfz_mittel_new_1=vref_resol_change;
		WheelAccelG_1=0;
		WheelAccelG_1_old=0;
	}
	else
	{
		filt_ein_new = non_driven_WheelV - voptfz_mittel_new_1;
/*		
		if(filt_ein_new	> VREF_2_KPH_RESOL_CHANGE )
		{
			filt_ein_new = VREF_2_KPH_RESOL_CHANGE;	 //	 +1g limit 
		}
		else if(filt_ein_new < -VREF_2_KPH_RESOL_CHANGE	)
		{
			filt_ein_new = -VREF_2_KPH_RESOL_CHANGE; //	-1g	limit
		}
		else
		{
			;
		}
*/
        filt_ein_new=LCABS_s16LimitMinMax(filt_ein_new,-VREF_20_KPH_RESOL_CHANGE,VREF_20_KPH_RESOL_CHANGE);
		filter_out_new_1= LCESP_s16Lpf1Int(2*filt_ein_new ,	filter_out_new_1 ,L_U8FILTER_GAIN_10MSLOOP_2HZ ); /* 2Hz */
/*
		tempW0=	16;
		tempW1=	1 ;
		tempW2=	(filter_out_new_1 +	filter_out_new_rest_1 );
		s16muls16divs16();
		filter_out_new_v_1=tempW3;
*/
		filter_out_new_v_1=(filter_out_new_1 + filter_out_new_rest_1 )	/ 11 ;
		voptfz_mittel_new_1	= voptfz_mittel_new_1 +	filter_out_new_v_1;
		filter_out_new_rest_1= (filter_out_new_1 + filter_out_new_rest_1 ) - (filter_out_new_v_1*11);

		WheelAccelG_1_old=WheelAccelG_1;
/*
		tempW0=51;
		tempW1=20;
		tempW2=(filter_out_new_1 );
		s16muls16divs16();
		WheelAccelG_1=tempW3; */ /*	Resolution 100	*/
		
		WheelAccelG_1=(int16_t)	( ((int32_t) filter_out_new_1 *	20)	/ 52 );
	}
}
void LDABS_DetPseudoVehVelInAccelSpin(void)
{   
    lds16vrefbyalongold = lds16vrefbyalong;
    if(ldu1VrefMaxupperlim == 1) 
    {
        tempW2=(int16_t)((along_vref*8)+lds16vdiffbyalongmod);
        lds16vdiffbyalong=((along_vref*8)+lds16vdiffbyalongmod)/35;
        lds16vdiffbyalongmod=(tempW2-(lds16vdiffbyalong*35));
        lds16vrefbyalong+=lds16vdiffbyalong;
    }
    else
    {
        lds16vdiffbyalongmod = 0;
        lds16vdiffbyalong = 0;
        lds16vrefbyalong = vref_resol_change;/*vref로 수정*/    
    }
}

void LDABS_vDetNoSpinState(void)
{
    if(ldu1nospinstate == 0)
    {
        /*vrefflags = 5;*/
        if((vrad_diff_max_resol_change <= VREF_3_KPH_RESOL_CHANGE)
        &&(FZ1.vrselect_resol_change <= VREF_5_KPH_RESOL_CHANGE ))
        {
            if(WheelAccelG_1 > AFZ_0G1)
            {
            	ldu8nospinsetcnt = ldu8nospinsetcnt + 1;
            }
            else
            {
				if(ldu8nospinsetcnt>0)
                {
                    ldu8nospinsetcnt = ldu8nospinsetcnt - 1;
                }
            }
            if(ldu8nospinsetcnt > L_U8_TIME_10MSLOOP_50MS)
            {
                ldu1nospinstate = 1;
                ldu8nospinsetcnt = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            ldu8nospinsetcnt = 0;
        }
    }
    else
    {
        
        ldu8nospinresetcnt = ldu8nospinresetcnt + 1;
        if((ldu8nospinresetcnt > L_U8_TIME_10MSLOOP_1500MS)
        ||(vrad_diff_max_resol_change > VREF_5_KPH_RESOL_CHANGE)
        ||(FZ1.vrselect_resol_change > VREF_5_KPH_RESOL_CHANGE)
        ||(mtp < MTP_20_P))
        {
            /*vrefflags = 1;*/
            ldu1nospinstate = 0;
            ldu8nospinresetcnt = 0;
        }
		else
		{
			;
		}
    }     
}

#endif
/*--------------------------------------------------------------------------------*/
void	LDABS_vDetVehicleAccelSpin(void)
{
    /*int8_t    MULTI=10;*/
	int8_t	SPIN_DCT_INIT=0;
	int8_t	SPIN_RESET_FLG=0;
	int16_t	delta_vref_for_30scan;
	int16_t	current_speed;
	int16_t	selected_wheel_speed;
	int16_t	non_driving_whl1;
	int16_t	non_driving_whl2;
	int16_t	arad_avg_min;
	
	current_speed=vref_resol_change;
	selected_wheel_speed=FZ1.vrselect_resol_change;
		#if	__REAR_D
	non_driving_whl1=FL.vrad_crt_resol_change;
	non_driving_whl2=FR.vrad_crt_resol_change;
		#else
	non_driving_whl1=RL.vrad_crt_resol_change;
	non_driving_whl2=RR.vrad_crt_resol_change;
		#endif

	/* IGN off/on Initialization*/
	#if	(__AX_SENSOR==1)
	LDABS_vCalAccelOfWheel();
    LDABS_DetPseudoVehVelInAccelSpin();
    LDABS_vDetNoSpinState();
	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		SPIN_DCT_INIT=1;
		ACCEL_SPIN_FZ=0;
		BIG_ACCEL_SPIN_FZ=0;
		lsu1VcaReqbyAllWhlSpinFlg=0;
	}
	else
	{
		#if	( __4WD_VARIANT_CODE==ENABLE)
		if((lsu8DrvMode	==DM_AUTO)||(lsu8DrvMode ==DM_4H))
		{
		#endif
		/*Spin Detection & Reset on	Ax sensor Normal*/
			if(USE_ALONG==1)
			{
				/* Calc	Difference between Ax-Sensor & Ax  Using HPF */
                lds16HPFWheelAccel=(int16_t) (((int32_t) (lds16HPFWheelAccel + (MULTI*(WheelAccelG_1 -WheelAccelG_1_old))) * 42) / 43 );
                lds16HPFLongG=(int16_t) (((int32_t) (lds16HPFLongG + (MULTI*(ax_Filter -ax_Filter_old))) * 42) / 43 );               
				
				arad_avg_min=FL.arad_avg;
                if(FR.arad_avg<arad_avg_min){arad_avg_min=FR.arad_avg;}
                if(RL.arad_avg<arad_avg_min){arad_avg_min=RL.arad_avg;}
                if(RR.arad_avg<arad_avg_min){arad_avg_min=RR.arad_avg;}    
						
				if(BRAKING_STATE==0)
				{
					/*Calculate	Vref Using Ax-Sensor*/
					accel_spin_det_cnt1++;
					along_vref_sum+=along_vref;
					if(accel_spin_det_cnt1==3)
					{

                        tempW2=(int16_t)((along_vref_sum*8)+vdiff_by_along_mod);
/*						
						tempW1=(int16_t)1;
						tempW0=(int16_t)25;
						s16muls16divs16();
						vdiff_by_along=tempW3;
*/
						vdiff_by_along=((along_vref_sum*8)+vdiff_by_along_mod)/35;
                        vdiff_by_along_mod=(tempW2-(vdiff_by_along*35));

						accel_spin_det_cnt1=0;
						along_vref_sum=0;
						accel_spin_det_cnt2++;

						if(accel_spin_det_cnt2==1)
						{
							vdiff_by_along_1=vdiff_by_along;
							vref_by_along=vref_along_1;
							vref_along_1=current_speed;
							accel_det_flags=2;
						}
						else if(accel_spin_det_cnt2==2)
						{
							vdiff_by_along_2=vdiff_by_along;
							vref_by_along=vref_along_2;
							vref_along_2=current_speed;
							accel_det_flags=3;
						}
						else if(accel_spin_det_cnt2==3)
						{
							vdiff_by_along_3=vdiff_by_along;
							vref_by_along=vref_along_3;
							vref_along_3=current_speed;
							accel_det_flags=4;
						}
						else if(accel_spin_det_cnt2==4)
						{
							vdiff_by_along_4=vdiff_by_along;
							vref_by_along=vref_along_4;
							vref_along_4=current_speed;

							accel_det_flags=5;
						}
						else if(accel_spin_det_cnt2==5)
						{
							vdiff_by_along_5=vdiff_by_along;
							vref_by_along=vref_along_5;
							vref_along_5=current_speed;
							accel_det_flags=26;
						}
						else if(accel_spin_det_cnt2==6)
						{
							vdiff_by_along_6=vdiff_by_along;
							vref_by_along=vref_along_6;
							vref_along_6=current_speed;
							accel_det_flags=25;
						}
						else if(accel_spin_det_cnt2==7)
						{
							vdiff_by_along_7=vdiff_by_along;
							vref_by_along=vref_along_7;
							vref_along_7=current_speed;
							accel_det_flags=26;
							accel_spin_det_cnt2=0;
						}
						/*
						else if(accel_spin_det_cnt2==8)
						{
							vdiff_by_along_8=vdiff_by_along;
							vref_by_along=vref_along_8;
							vref_along_8=current_speed;
							accel_spin_det_cnt2=0;
							accel_det_flags=27;
						}
						*/												
						else
						{
							;
						}
						delta_vref_for_30scan=vdiff_by_along_1+vdiff_by_along_2+vdiff_by_along_3+vdiff_by_along_4+vdiff_by_along_5+vdiff_by_along_6+vdiff_by_along_7;
						vref_by_along+=delta_vref_for_30scan;

						if(vref_by_along<VREF_MIN_SPEED)
						{
							vref_by_along=VREF_MIN_SPEED;
						}
						else
						{
							;
						}
					}
					else
					{ /* cnt1!=5*/
						accel_det_flags=12;
					}


					/*Accel	Spin Set*/
					if(ACCEL_SPIN_FZ==0)
					{
						if(lds16AccelSpinSustainCnt>0){
							lds16AccelSpinSustainCnt-=1;
							ACCEL_SPIN_FADE_OUT_FLAG=1;
							
							if((rel_lam_fl>=-3)&&(lds16AccelSpinSustainCnt>0)){
								lds16AccelSpinSustainCnt-=1;
							}
							if((rel_lam_fr>=-3)&&(lds16AccelSpinSustainCnt>0)){
								lds16AccelSpinSustainCnt-=1;
							}
							if((rel_lam_rl>=-3)&&(lds16AccelSpinSustainCnt>0)){
								lds16AccelSpinSustainCnt-=1;
							}													
							if((rel_lam_rr>=-3)&&(lds16AccelSpinSustainCnt>0)){
								lds16AccelSpinSustainCnt-=1;
							}
							if(lds16AccelSpinSustainCnt<=0){
								ACCEL_SPIN_FADE_OUT_FLAG=0;
							}
						}
						else{
							lds16AccelSpinSustainCnt=0;
							ACCEL_SPIN_FADE_OUT_FLAG=0;
						}						
						#if	__VDC
						if(VDC_REF_WORK==0)
						{
						#endif
                            if(ldu1nospinstate == 1)
                            {
                                lds16SpinDctDvThr0 = 0 + VREF_1_5_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr1 = VREF_0_75_KPH_RESOL_CHANGE + VREF_1_5_KPH_RESOL_CHANGE; 
                                lds16SpinDctDvThr2 = VREF_0_5_KPH_RESOL_CHANGE + VREF_1_5_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr3 = VREF_1_KPH_RESOL_CHANGE + VREF_1_5_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr4 = VREF_0_25_KPH_RESOL_CHANGE + VREF_1_5_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr5 = VREF_2_KPH_RESOL_CHANGE + VREF_1_5_KPH_RESOL_CHANGE;
                            }
                            else
                            {
                                lds16SpinDctDvThr1 = VREF_0_75_KPH_RESOL_CHANGE; 
                                lds16SpinDctDvThr2 = VREF_0_5_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr3 = VREF_1_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr4 = VREF_0_25_KPH_RESOL_CHANGE;
                                lds16SpinDctDvThr5 = VREF_2_KPH_RESOL_CHANGE;
                            }
                            
                            if(((current_speed-vref_by_along)>lds16SpinDctDvThr1)&& 
                            ((selected_wheel_speed-current_speed)>lds16SpinDctDvThr2)&& 
                            ((selected_wheel_speed-vref_by_along)>lds16SpinDctDvThr3))
						{
							lds8SpinSetCount=lds8SpinSetCount+1;
                                if((wheel_speed_min-current_speed)>lds16SpinDctDvThr2)
							{
								lds8SpinSetCount=lds8SpinSetCount+1;
							}
						}
                            else if(((wheel_speed_min-current_speed)>lds16SpinDctDvThr0)&&
                            ((selected_wheel_speed-current_speed)>lds16SpinDctDvThr1)&&
                            ((current_speed-vref_by_along)>lds16SpinDctDvThr1))
						{
							lds8SpinSetCount=lds8SpinSetCount+1;
						}
                            else if(((selected_wheel_speed-current_speed)<lds16SpinDctDvThr4)&&
						((vref-vref_alt)<=0)&&(lds8SpinSetCount>0))
						{
							lds8SpinSetCount=lds8SpinSetCount-1;
						}
                            else if((wheel_speed_max-current_speed)<lds16SpinDctDvThr2)
						{
							lds8SpinSetCount=0;
						}
                            else if((wheel_speed_min-current_speed)>lds16SpinDctDvThr5)
						{
							lds8SpinSetCount=SPIN_SET_TIME;
						}
						else
						{
							;
						}
						#if	__VDC
						}
						else
						{
                            if(((selected_wheel_speed-current_speed)>SPIN_DCT_DV_THR1) && ((current_speed-ESP_entry_speed)>SPIN_DCT_DV_THR12))
								{
									lds8SpinSetCount++;
								}
                            else if(((current_speed-vref_by_along)>SPIN_DCT_DV_THR1)&&
                            ((selected_wheel_speed-current_speed)>SPIN_DCT_DV_THR2)&&
                            ((selected_wheel_speed-vref_by_along)>SPIN_DCT_DV_THR3))
                            {
                                lds8SpinSetCount=lds8SpinSetCount+1;
                                if((wheel_speed_min-current_speed)>SPIN_DCT_DV_THR4)
                                {
                                    lds8SpinSetCount=lds8SpinSetCount+1;
                                }
                            }
                            else if(((selected_wheel_speed-current_speed)<SPIN_DCT_DV_THR8)&&
                            ((vref-vref_alt)<=0)&&(lds8SpinSetCount>0))
                            {
                                lds8SpinSetCount=lds8SpinSetCount-1;
                            }
                            else if((wheel_speed_max-current_speed)<SPIN_DCT_DV_THR9)
                            {
                                lds8SpinSetCount=0;
                            }
                            else
                            {
                                ;
							}

						}
						#endif
                        lds8SpinSetCount = (int8_t)LCABS_s16LimitMinMax((int16_t)(lds8SpinSetCount),-120 ,120);
						 /*Spin	Detect counter2	increase*/
						if(SPIN_CALC_FLG==1)
						{
							if(lds16HPFWheelAccel >	(lds16HPFLongG + (HPF_SET_G*MULTI))	)
							{
                                lds16VrefDelta=lds16VrefDelta + (int16_t)((((int32_t)lds16HPFWheelAccel-lds16HPFLongG)*8)/(35*MULTI));
								lds8SpinSetCount_2=lds8SpinSetCount_2+1;
							}
							else if(lds16HPFWheelAccel >= lds16HPFLongG	)
							{
								;
							}
							else
							{
								if(	(WheelAccelG_1 <(ax_Filter+	SPIN_DCT_RESET_G) )	 )
								{
									SPIN_CALC_FLG=0;
								}
								else
								{
                                    lds16VrefDelta=lds16VrefDelta + (int16_t)((((int32_t)lds16HPFWheelAccel-lds16HPFLongG)*8*SPIN_DEC_FACTOR)/(35*MULTI));
									lds8SpinSetCount_2=lds8SpinSetCount_2-1;
								}
							}
                            lds8SpinSetCount_2 = (int8_t)LCABS_s16LimitMinMax((int16_t)(lds8SpinSetCount_2),-120,120);
							if((lds16VrefDelta < 0)||(lds8SpinSetCount_2 <0))
							{
								SPIN_CALC_FLG=0;
								lds16VrefDelta=0;
								lds8SpinSetCount_2=0;
							}
							else
							{
								;
							}
						}
						else
						{
	  
							if(	(lds16HPFWheelAccel	> (lds16HPFLongG + (HPF_SET_G*MULTI)) )	&&
							(lds16HPFWheelAccel	>0 ) &&	(WheelAccelG_1 >0 )	&& (vref_limit_dec_flag==0)	)
							{
                                lds16VrefDelta=lds16VrefDelta + (int16_t)((((int32_t)lds16HPFWheelAccel-lds16HPFLongG)*8)/(35*MULTI));
								lds8SpinSetCount_2=lds8SpinSetCount_2+1;
								SPIN_CALC_FLG=1;
							}
							else
							{
								lds16VrefDelta=0;
								lds8SpinSetCount_2=0;
								SPIN_CALC_FLG=0;
							}							
							
						}
						/*lds ldu1SlowestWheelSelectFlag set*/
						if(ldu1SlowestWheelSelectFlag==0){							
							#if	__ENGINE_INFORMATION
							if((mtp>MTP_10_P)&&(eng_torq_rel>TORQ_PERCENT_30PRO))	{
							#endif	
								if(((current_speed-vref_by_along)>SPIN_DCT_DV_THR1)&&
								((selected_wheel_speed-vref_by_along)>SPIN_DCT_DV_THR3)&&
								(arad_avg_min>WHL_ACCEL_THR_MIN_WHL_SEL)&&
								(lsabss16DecelRefiltByVref5>VEH_ACCEL_THR_MIN_WHL_SEL)&&(vrad_diff_max_resol_change>SPIN_DCT_DV_THR10)){
									lds16SlowestWhlSelDetCnt++;
								}
								else{	
									lds16SlowestWhlSelDetCnt--;
										
								}
								
								lds16SlowestWhlSelDetCnt=LCABS_s16LimitMinMax(lds16SlowestWhlSelDetCnt,0,100);
								if((lds16SlowestWhlSelDetCnt>5)&&(lds8SpinSetCount_2>5)){
									ldu1SlowestWheelSelectFlag=1;
									lds16SlowestWhlSelDetCnt=0;
								}
								else{
									;
								}
							#if	__ENGINE_INFORMATION
							}				
							else{
								lds16SlowestWhlSelDetCnt=0;
							}
							#endif
						}
						else{
							lds16SlowestWhlSelDetCnt=0;
							if(((current_speed-selected_wheel_speed)>VREF_1_KPH_RESOL_CHANGE)&&(arad_avg_min<WHL_ACCEL_THR_MIN_WHL_SEL_CLEAR)){
								ldu1SlowestWheelSelectFlag=0;
							}
							else if(vrad_diff_max_resol_change<VREF_1_KPH_RESOL_CHANGE){
								ldu1SlowestWheelSelectFlag=0;
							}
							/*	move to	ACCEL_SPIN_FZ==1 state
							else if(ACCEL_SPIN_FZ==1){
								ldu1SlowestWheelSelectFlag=0;
							}
							*/
							else
							{
								;
							}								
						}
						
						
						#if	__VDC
						if(lsu1VcaReqbySpinSusFlg==0){
							lds16WhlSpinSusClearCnt=0;
							lds16WhlSpinSusClearCnt2=0;
							if((mtp>MTP_30_P)&&(eng_torq_rel>TORQ_PERCENT_30PRO)){
								if(((current_speed-vref_by_along)>SPIN_DCT_DV_THR1)&&
								((selected_wheel_speed-vref_by_along)>SPIN_DCT_DV_THR3)&&
								(lsabss16DecelRefiltByVref5>VEH_ACCEL_THR_SPIN_SUS_DCT)&&
								(vrad_diff_max_resol_change<VREF_2_KPH_RESOL_CHANGE)&&
								/*((wheel_speed_max-wheel_speed_min)>64*2)&&*/
								(ax_Filter>AFZ_0G0)&&(ax_Filter<AFZ_0G1)){
									lds16SpinSusCountforVcaReq++;
								}
								else{
									lds16SpinSusCountforVcaReq--;
								}
								lds16SpinSusCountforVcaReq=LCABS_s16LimitMinMax(lds16SpinSusCountforVcaReq,0,100);
								
								/*판단부*/
								if((lds16SpinSusCountforVcaReq>10)&&(lds8SpinSetCount_2>5)){
									lsu1VcaReqbySpinSusFlg=1;
									lds16SpinSusCountforVcaReq=0;
								}
							}				
							else{
								lds16SpinSusCountforVcaReq=0;
							}							
						}
						else{
							lds16SpinSusCountforVcaReq=0;
							lds16WhlSpinSusClearCnt++;
							if((VCA_ON==1)&&(McrAbs(arad_fl)<ARAD_1G0)&&(McrAbs(arad_fr)<ARAD_1G0)&&(McrAbs(arad_rl)<ARAD_1G0)&&(McrAbs(arad_rr)<ARAD_1G0)){
								lds16WhlSpinSusClearCnt2++;
							}
							else{
                                if(lds16WhlSpinSusClearCnt2>0) {lds16WhlSpinSusClearCnt2++;}
							}
							lds16WhlSpinSusClearCnt	= LCABS_s16LimitMinMax(lds16WhlSpinSusClearCnt,0, L_U16_TIME_10MSLOOP_10S );
							if(mtp<MTP_10_P)
							{
								lsu1VcaReqbySpinSusFlg=0;
								lds16WhlSpinSusClearCnt2=0;
								lds16WhlSpinSusClearCnt=0;
							}
							else if(vrad_diff_max_resol_change>VREF_3_KPH_RESOL_CHANGE)
							{
								lsu1VcaReqbySpinSusFlg=0;
								lds16WhlSpinSusClearCnt2=0;
								lds16WhlSpinSusClearCnt=0;
							}
							else if(lds16WhlSpinSusClearCnt>L_U8_TIME_10MSLOOP_1000MS)
							{
								lsu1VcaReqbySpinSusFlg=0;
								lds16WhlSpinSusClearCnt2=0;
								lds16WhlSpinSusClearCnt=0;
							}
							else if(lds16WhlSpinSusClearCnt2>L_U8_TIME_10MSLOOP_500MS){
								lds16WhlSpinSusClearCnt=0;
								lds16WhlSpinSusClearCnt2=0;
								lsu1VcaReqbySpinSusFlg=0;
							}
							/* move	to ACCEL_SPIN_FZ==1	state								 
							else if(ACCEL_SPIN_FZ==1)
							{
								lsu1VcaReqbySpinSusFlg=0;
								lds16WhlSpinSusClearCnt2=0;
								lds16WhlSpinSusClearCnt=0;
							}
							*/
							else
							{
								;
							}
						}
						#endif
						
						
						/*Engine Condition For Spin	Detection*/
						#if	__ENGINE_INFORMATION
						tempF0=0;
						if ((mtp>=10) && (ldabsu1CanErrFlg==0))
						{
/*							
							tempW0=10;
							tempW1=mtp;
							tempW2=eng_torq_rel;
							s16muls16divs16();
*/							
							tempW3=(int16_t) ( ((int32_t) eng_torq_rel * mtp) /	10 );
							if(tempW3>=300)
							{
								tempF0=1;
							}
							else
							{
								;
							}
						}
						else if(ldabsu1CanErrFlg==1)
						{
							tempF0=1;
						}
						else
						{
							;
						}
						#else
						tempF0=1;
						#endif
						tempF1=0;
						/*Spin Set*/
						accel_det_flags=0;
						if(SPIN_CALC_FLG==1)
						{
							accel_det_flags=1;
							if(tempF0==1)
							{								
								accel_det_flags=2;
								if (lds8SpinSetCount>=SPIN_SET_TIME)
								{
									accel_det_flags=3;
									if(lds16VrefDelta>=SPIN_SET_DSPEED)
									{
										accel_det_flags=4;
										if(lds8SpinSetCount_2>=SPIN_SET_TIME_2)
										{
											tempF1=1;
											accel_det_flags=5;
										}
										else
										{
											accel_det_flags=6;
										}
									}
									else
									{
										accel_det_flags=7;
									}
								}
								else if( ((selected_wheel_speed-current_speed)>SPIN_DCT_DV_THR13) && (ebd_refilt_grv > SPIN_SET_VREF_ACCEL_G_1 ) &&
										 (vrad_diff_max_resol_change <=	SPIN_SET_VRAD_DIFF_1)	)
								{
									if(lds16VrefDelta>=SPIN_SET_DSPEED2)
									{
										if(lds8SpinSetCount_2>=SPIN_SET_TIME_3)
										{
											accel_det_flags=9;
											tempF1=1;
										}
										else
										{
											accel_det_flags=10;
										}
									}
									else
									{
										accel_det_flags=11;
									}
								}
								else
								{
									accel_det_flags=12;
								}
							}
							else
							{
								accel_det_flags=13;
							}
						}
						else
						{
							accel_det_flags=14;
						}

						if(tempF1==1)
						{
							ACCEL_SPIN_FZ=1;
							vref_by_along=current_speed;
							vref_along_1=current_speed;
							vref_along_2=current_speed;
							vref_along_3=current_speed;
							vref_along_4=current_speed;
							vref_along_5=current_speed;
							vref_along_6=current_speed;
							vref_along_7=current_speed;
							vref_along_8=current_speed;
							
							accel_spin_det_cnt1=0;
							accel_spin_det_cnt2=0;
							along_vref_sum=0;
							vdiff_by_along_1=0;
							vdiff_by_along_2=0;
							vdiff_by_along_3=0;
							vdiff_by_along_4=0;
							vdiff_by_along_5=0;
							vdiff_by_along_6=0;
							vdiff_by_along_7=0;
							vdiff_by_along_8=0;							
							vdiff_by_along=0;

							lds8SpinSetCount=0;
							lds8SpinSetCount_2=0;
							lds8SpinSetCount_3=0;
							lds8SpinClearCnt=0;
							lds8BigSpinSetCnt=0;
							lds8BigSpinClearCnt=0;

							lds16VrefDelta=0;
							SPIN_CALC_FLG=0;
						}
						else
						{
							;
						}
					}
					/*Accel	Spin Clear*/
					else
					{	
						if(lds16AccelSpinSustainCnt<T_100_MS){
							lds16AccelSpinSustainCnt=T_100_MS;
						}
						else if(lds16AccelSpinSustainCnt>T_500_MS){
							lds16AccelSpinSustainCnt=T_500_MS;
						}
						else{
							lds16AccelSpinSustainCnt+=1;
						}
						ACCEL_SPIN_FADE_OUT_FLAG=1;	        							
						
						
						if(ldu1SlowestWheelSelectFlag==1)
						{
							ldu1SlowestWheelSelectFlag=0;
							lds16SlowestWhlSelDetCnt=0;
						}

						#if	__VDC
						if(lsu1VcaReqbySpinSusFlg==1)
						{
							lsu1VcaReqbySpinSusFlg=0;
							lds16WhlSpinSusClearCnt2=0;
							lds16WhlSpinSusClearCnt=0;
						}
						#endif
                        #if (__IMPROVED_VREF_IN_TCS_1 == ENABLE)
                        if((ax_Filter>=0)&&(WheelAccelG_1>=0))
                        {
                            ldu1wheelaxgSameSign = 1;
                        }
                        else if((ax_Filter<=0)&&(WheelAccelG_1<=0))
                        {
                            ldu1wheelaxgSameSign = 1;
                        }
                        else
                        {
                            ldu1wheelaxgSameSign = 0;
                        }
                        
                        #endif
                        
                        tempW1 = McrAbs(lds16HPFWheelAccel-lds16HPFLongG);

						#if	__VDC
						/*initialize*/
						if (VDC_REF_WORK==1)
						{
							if((( wheel_speed_2nd-current_speed)<SPIN_CLR_DV_THR8)&&
							((selected_wheel_speed-current_speed)<SPIN_CLR_DV_THR9))
							{
								SPIN_RESET_FLG=1;
							}
						}
						else
						{
						#endif
							if(((wheel_speed_3rd-current_speed)<SPIN_CLR_DV_THR5)
							&&((selected_wheel_speed-current_speed)<SPIN_CLR_DV_THR6)
							&&(WheelAccelG_1<SPIN_RESET_G))
							{
								lds8SpinClearCnt++;
								if(lds8SpinClearCnt>SPIN_RESET_TIME)
								{
									SPIN_RESET_FLG=1;
								}
							}
							else if(((wheel_speed_max-current_speed)<SPIN_CLR_DV_THR1)&&
							((wheel_speed_2nd-current_speed)<SPIN_CLR_DV_THR2)&&
							((wheel_speed_3rd-current_speed)<SPIN_CLR_DV_THR3)&&
							((wheel_speed_min-current_speed)<SPIN_CLR_DV_THR4))
							{
								lds8SpinClearCnt++;
								if(lds8SpinClearCnt>SPIN_RESET_TIME)
								{
									SPIN_RESET_FLG=1;
								}
							}
                            
                            else if((tempW1 < (HPF_RESET_G*MULTI))
                                    &&(vrad_diff_max_resol_change < SPIN_CLR_DV_THR8)
                                    #if (__IMPROVED_VREF_IN_TCS_1 == ENABLE)
                                    &&(ldu1wheelaxgSameSign == 1)
                                    #endif
                            )
							{
								#if	__ENGINE_INFORMATION
                                if ((eng_torq_rel <= TORQ_PERCENT_15PRO) && (ldabsu1CanErrFlg==0))
								{
								#endif
									lds8SpinClearCnt++;
								#if	__ENGINE_INFORMATION
								}
								#endif
                                
								if(lds8SpinClearCnt>SPIN_RESET_TIME2)
								{
									SPIN_RESET_FLG=1;
								}
							}
							else if(((wheel_speed_min-current_speed)>SPIN_CLR_DV_THR10)&&(lds8SpinClearCnt>0))
							{
								lds8SpinClearCnt--;
							}
							else
							{
								;
							}
						#if	__VDC
						}
						#endif
						if(SPIN_RESET_FLG==1)
						{
							ACCEL_SPIN_FZ=0;
							lds8SpinClearCnt=0;
						}

					}
					/*Big Accel	Spin Set Condition*/
					if(BIG_ACCEL_SPIN_FZ==0)
					{
						if(ACCEL_SPIN_FZ==1)
						{
							#if __VDC
							if(VDC_REF_WORK == 1)
							{
	                            if(((FZ1.vrselect_resol_change - current_speed) > BIG_SPIN_SET_DV_THR1)
							&&((WheelAccelG_1-ax_Filter) > BIG_SPIN_SET_G)
							&&((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_2*MULTI))
							&&(ebd_refilt_grv>BIG_SPIN_SET_G_2))
							{
	                                #if __ENGINE_INFORMATION
	                                if((mtp>MTP_10_P)&&(eng_torq_rel>TORQ_PERCENT_10PRO))
								{
								#endif

	                                    if((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_3*MULTI))
	                                    {
	                                        lds8BigSpinSetCnt++;
	                                    }
	                                    else if(((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_4*MULTI))
	                                    &&((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR2))
	                                    {
	                                        lds8BigSpinSetCnt++;
	                                    }
	                                    else
	                                    {
	                                        ;
	                                    }
	
	                                    if((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR3)
	                                    {
	                                        lds8BigSpinSetCnt=BIG_SPIN_SET_TIME;
	                                    }
	                                    else if((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR4)
	                                    {
	                                        lds8BigSpinSetCnt++;
	                                    }
	                                    else
	                                    {
	                                        ;
	                                    }
	                                    if(lds8BigSpinSetCnt>=BIG_SPIN_SET_TIME)
	                                    {
	                                        BIG_ACCEL_SPIN_FZ=1;
	                                        lds8BigSpinSetCnt=0;
	                                    }
								#if	__ENGINE_INFORMATION
	                                }
	                                #endif
	                            }
	                            else if(wheel_speed_min<current_speed)
	                            {
	                                lds8BigSpinSetCnt=0;
	                            }
	                            else
	                            {
	                                ;
	                            }
	                        }
	                        else
	                        {
	                       		if(((wheel_speed_min - current_speed) > BIG_SPIN_SET_DV_THR1)
	                            &&((WheelAccelG_1-ax_Filter) > BIG_SPIN_SET_G)
	                            &&((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_2*MULTI))
	                            &&(ebd_refilt_grv>BIG_SPIN_SET_G_2))
	                            {
								#if	__ENGINE_INFORMATION
	                                if((mtp>MTP_10_P)&&(eng_torq_rel>TORQ_PERCENT_10PRO))
								{
								#endif

									if((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_3*MULTI))
									{
										lds8BigSpinSetCnt++;
									}
									else if(((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_4*MULTI))
									&&((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR2))
									{
										lds8BigSpinSetCnt++;
									}
									else
									{
										;
									}

									if((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR3)
									{
										lds8BigSpinSetCnt=BIG_SPIN_SET_TIME;
									}
									else if((wheel_speed_min-current_speed)	> BIG_SPIN_SET_DV_THR4)
									{
										lds8BigSpinSetCnt++;
									}
									else
									{
										;
									}
									if(lds8BigSpinSetCnt>=BIG_SPIN_SET_TIME)
									{
										BIG_ACCEL_SPIN_FZ=1;
										lds8BigSpinSetCnt=0;
									}
								#if	__ENGINE_INFORMATION
								}
								#endif
	                            }
	                            else if(wheel_speed_min<current_speed)
	                            {
	                                lds8BigSpinSetCnt=0;
	                            }
	                            else
	                            {
	                                ;
	                            }
	                        }
                            #else
                            if(((wheel_speed_min - current_speed) > BIG_SPIN_SET_DV_THR1)
                            &&((WheelAccelG_1-ax_Filter) > BIG_SPIN_SET_G)
                            &&((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_2*MULTI))
                            &&(ebd_refilt_grv>BIG_SPIN_SET_G_2))
                            {
                                #if __ENGINE_INFORMATION
                                if((mtp>MTP_10_P)&&(eng_torq_rel>TORQ_PERCENT_10PRO))
                                {
                                #endif

                                    if((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_3*MULTI))
                                    {
                                        lds8BigSpinSetCnt++;
                                    }
                                    else if(((lds16HPFWheelAccel-lds16HPFLongG)>(HPF_SET_G_4*MULTI))
                                    &&((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR2))
                                    {
                                        lds8BigSpinSetCnt++;
                                    }
                                    else
                                    {
                                        ;
                                    }

                                    if((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR3)
                                    {
                                        lds8BigSpinSetCnt=BIG_SPIN_SET_TIME;
                                    }
                                    else if((wheel_speed_min-current_speed) > BIG_SPIN_SET_DV_THR4)
                                    {
                                        lds8BigSpinSetCnt++;
                                    }
                                    else
                                    {
                                        ;
                                    }
                                    if(lds8BigSpinSetCnt>=BIG_SPIN_SET_TIME)
                                    {
                                        BIG_ACCEL_SPIN_FZ=1;
                                        lds8BigSpinSetCnt=0;
                                    }
                                #if __ENGINE_INFORMATION
								}
								#endif
							}
							else if(wheel_speed_min<current_speed)
							{
								lds8BigSpinSetCnt=0;
							}
							else
							{
								;
							}
                            
                            
                            #endif
						}
						else
						{
							BIG_ACCEL_SPIN_FZ=0;
						}
					}
					/*Big Accel	Spin Reset Condition*/
					else
					{


						if(ACCEL_SPIN_FZ==0)
						{
							BIG_ACCEL_SPIN_FZ=0;
						}
						else
						{
							if(((selected_wheel_speed-current_speed) < BIG_SPIN_CLR_DV_THR1)
							&&((wheel_speed_3rd-current_speed) < BIG_SPIN_CLR_DV_THR2)
							&&((lds16HPFWheelAccel-lds16HPFLongG)<(HPF_RESET_G_2*MULTI)))
							{
								lds8BigSpinClearCnt++;
								if(lds8BigSpinClearCnt>BIG_SPIN_RESET_TIME)
								{
									lds8BigSpinClearCnt=0;
									BIG_ACCEL_SPIN_FZ=0;
								}
							}
						}
					}
				}
				/* At braking State, Initialize	Variables */
				else
				{
					SPIN_DCT_INIT=1;
					lsu1VcaReqbyAllWhlSpinFlg=0;
					accel_det_flags=13;
					/*Spin Reset On	Braking	State*/
					if(ACCEL_SPIN_FZ==1)
					{
						if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
						{
							ACCEL_SPIN_FZ=0;
							BIG_ACCEL_SPIN_FZ=0;
						}
					}
					/*Spin Set Condition On	Braking	State*/
					else
					{
						#if	__ENGINE_INFORMATION
						if((wheel_speed_min-current_speed)>SPIN_DCT_DV_THR_ON_BRK)
						{
							if((eng_torq_rel>150)&&(mtp>10))
							{
								lds8SpinSetCount_3++;
								if((current_speed-ABS_entry_speed)>SPIN_DCT_DV_THR_ON_BRK2)
								{
									lds8SpinSetCount_3++;
								}

								if(lds8SpinSetCount_3>SPIN_SET_TIME_ON_BRK)
								{
									ACCEL_SPIN_FZ=1;
									lds8SpinSetCount_3=0;
								}
							}
						}
						#endif
					}
				}
			}
			/*Spin Detection & Reset on	Ax sensor Error	Case*/
			else
			{
				SPIN_DCT_INIT=1;
				/*SPIN Set Condition*/
				if(ACCEL_SPIN_FZ==0)
				{
					if((!BRAKE_SIGNAL)&&(!ABS_fz))
					{
						if(((selected_wheel_speed-current_speed)>VRSELECT_DV_THRES2)
						&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
						{
							ACCEL_SPIN_FZ=1;
						}
						else if	(((non_driving_whl1-current_speed)>VRSELECT_DV_THRES2)
						&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
						{
							ACCEL_SPIN_FZ=1;
						}
						else if	(((non_driving_whl2-current_speed)>VRSELECT_DV_THRES2)
						&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
						{
							ACCEL_SPIN_FZ=1;
						}
						else
						{
							;
						}
					}
					else
					{
						;
					}
				}
				/*Spin Reset Condition*/
				else
				{
					if(((non_driving_whl1-current_speed)>VRSELECT_DV_THRES4)&&((non_driving_whl2-current_speed)>VRSELECT_DV_THRES4))
					{
						BIG_ACCEL_SPIN_FZ=1;
					}
					else
					{
						;
					}
					if(((non_driving_whl1-current_speed)<SPIN_CLEAR_DV_THRES3)&&((non_driving_whl2-current_speed)<SPIN_CLEAR_DV_THRES3))
					{
						ACCEL_SPIN_FZ=0;
						BIG_ACCEL_SPIN_FZ=0;
					}
					else
					{
						;
					}
				}

			}
			
			#if	__VDC
			
			if(lsu1VcaReqbyAllWhlSpinFlg==0){
				if(ACCEL_SPIN_FZ==1){
					if(((eng_torq_rel>TORQ_PERCENT_30PRO)||((ETCS_ON==1)&&((wheel_speed_min-current_speed)>SPIN_DCT_DV_THR14)))&&(mtp>MTP_30_P)&& (YAW_CDC_WORK	== 0)
						&&((wheel_speed_min-current_speed)>SPIN_DCT_DV_THR10)&&(lsabss16DecelRefiltByVref5>(AFZ_0G05*10)))
					{

						lds16AllWhlSpinCntforVcaReq++;
						if (lds16AllWhlSpinCntforVcaReq	>=L_U8_TIME_10MSLOOP_1000MS)
						{
							lds16AllWhlSpinCntforVcaReq	= 0;
							lsu1VcaReqbyAllWhlSpinFlg =	1;			
						}
					}
					else
					{
						lds16AllWhlSpinCntforVcaReq--;
					}	
					lds16AllWhlSpinCntforVcaReq=LCABS_s16LimitMinMax(lds16AllWhlSpinCntforVcaReq,0,L_U16_TIME_10MSLOOP_10S);
					
				}
				else{
					lsu1VcaReqbyAllWhlSpinFlg=0;
					lds16AllWhlSpinCntforVcaReq=0;						
				}
			}
			else{
				lds16AllWhlSpinCntforVcaReq=0;
				if(mtp<MTP_10_P)
				{
					lsu1VcaReqbyAllWhlSpinFlg=0;	
				}
				else if((wheel_speed_min-current_speed)<SPIN_DCT_DV_THR3)
				{
					lsu1VcaReqbyAllWhlSpinFlg=0;
				}
				else if(ACCEL_SPIN_FZ==0)
				{
					lsu1VcaReqbyAllWhlSpinFlg=0;
				}	
				else{
					;
				}
			}

			#endif


			if(LONG_ACCEL_STATE==0)
			{
				if(BIG_ACCEL_SPIN_FZ==1)
				{
                    if((wheel_speed_min-current_speed)>VREF_2_KPH_RESOL_CHANGE)
					{
						lds8BigSpinHoldCount++;
					}

					if(lds8BigSpinHoldCount>L_U8_TIME_10MSLOOP_1750MS)
					{
						LONG_ACCEL_STATE=1;
						lds8BigSpinHoldCount=0;
					}
				}
				else
				{
					lds8BigSpinHoldCount=0;
				}
			}
			else
			{
				if((ABS_fz==0)&&(BRAKE_SIGNAL==0))
				{
					if(BIG_ACCEL_SPIN_FZ==0)
					{
                        if(vrad_diff_max_resol_change<VREF_2_KPH_RESOL_CHANGE)
						{
							if((McrAbs(rel_lam_fl)<3)&&(McrAbs(rel_lam_fr)<3)&&(McrAbs(rel_lam_rl)<3)&&(McrAbs(rel_lam_rr)<3))
							{
								lds8BigSpinHoldCount++;
							}
						}
						else if(lds8BigSpinHoldCount>0)
						{
							lds8BigSpinHoldCount--;
						}
						else
						{
							;
						}

						if(lds8BigSpinHoldCount>L_U8_TIME_10MSLOOP_140MS)
						{
							LONG_ACCEL_STATE=0;
							lds8BigSpinHoldCount=0;
						}
					}
				}
				else
				{

					if((FSF_fl==0)&&(FSF_fr==0))
					{
						LONG_ACCEL_STATE=0;
					}
					else if((McrAbs(rel_lam_fl)<5)||(McrAbs(rel_lam_fr)<5)||(McrAbs(rel_lam_rl)<5)||(McrAbs(rel_lam_rr)<5))
					{
                        if((vrad_diff_max_resol_change)<VREF_2_KPH_RESOL_CHANGE)
						{
							lds8BigSpinHoldCount++;
							if(lds8BigSpinHoldCount>L_U8_TIME_10MSLOOP_100MS)
							{
								LONG_ACCEL_STATE=0;
							}
						}
					}
					else
					{
						;
					}
				}
			}
		#if	( __4WD_VARIANT_CODE==ENABLE)
		}

/*------------------------------
	2 Wheel	Drive Mode ; Skip the Routine
-------------------------------*/
		else
		{
			SPIN_DCT_INIT=1;
			ACCEL_SPIN_FZ=0;
			BIG_ACCEL_SPIN_FZ=0;			
			lds16AllWhlSpinCntforVcaReq=0;
		}
		#endif
	}

	if(SPIN_DCT_INIT==1)
	{
		vref_by_along=current_speed;
		vref_along_1=current_speed;
		vref_along_2=current_speed;
		vref_along_3=current_speed;
		vref_along_4=current_speed;
		vref_along_5=current_speed;
		vref_along_6=current_speed;
		vref_along_7=current_speed;
		vref_along_8=current_speed;
		
		accel_spin_det_cnt1=0;
		accel_spin_det_cnt2=0;
		along_vref_sum=0;
		vdiff_by_along_1=0;
		vdiff_by_along_2=0;
		vdiff_by_along_3=0;
		vdiff_by_along_4=0;
		vdiff_by_along_5=0;
		vdiff_by_along_6=0;
		vdiff_by_along_7=0;
		vdiff_by_along_8=0;		
		vdiff_by_along=0;

        /*lds16HPFWheelAccel=0;
        lds16HPFLongG=0;*/
		lds8SpinSetCount=0;
		lds8SpinSetCount_2=0;
		lds8SpinSetCount_3=0;
		lds8SpinClearCnt=0;
		lds8BigSpinSetCnt=0;
		lds8BigSpinClearCnt=0;

		lds16VrefDelta=0;
		SPIN_CALC_FLG=0;
		
		lds16SlowestWhlSelDetCnt=0;
		lds16WhlSpinSusClearCnt=0;
		lds16WhlSpinSusClearCnt2=0;
		lds16SpinSusCountforVcaReq=0;
		lsu1VcaReqbySpinSusFlg=0;
		ldu1SlowestWheelSelectFlag=0;
		lds16AccelSpinSustainCnt=0;
		ACCEL_SPIN_FADE_OUT_FLAG=0;  
	}
	#else
#if	__4WD_VARIANT_CODE==ENABLE
if(	lsu8DrvMode	== DM_AUTO ){  
#endif	 
	if(speed_calc_timer	<= (UCHAR)L_U8_TIME_10MSLOOP_200MS)
	{
		ACCEL_SPIN_FZ=0;
		BIG_ACCEL_SPIN_FZ=0;
	}
	else
	{
		if(ACCEL_SPIN_FZ==0)
		{
			if((!BRAKE_SIGNAL)&&(!ABS_fz))
			{
				if(((selected_wheel_speed-current_speed)>VRSELECT_DV_THRES2)
				&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
				{
					ACCEL_SPIN_FZ=1;
				}
				else if	(((non_driving_whl1-current_speed)>VRSELECT_DV_THRES2)
				&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
				{
					ACCEL_SPIN_FZ=1;
				}
				else if	(((non_driving_whl2-current_speed)>VRSELECT_DV_THRES2)
				&&((wheel_speed_3rd-current_speed)>VRSELECT_DV_THRES2))
				{
					ACCEL_SPIN_FZ=1;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		/*Spin Reset Condition*/
		else
		{
			if(((non_driving_whl1-current_speed)>VRSELECT_DV_THRES4)&&((non_driving_whl2-current_speed)>VRSELECT_DV_THRES4))
			{
				BIG_ACCEL_SPIN_FZ=1;
			}
			else
			{
				;
			}
			if(((non_driving_whl1-current_speed)<SPIN_CLEAR_DV_THRES3)&&((non_driving_whl2-current_speed)<SPIN_CLEAR_DV_THRES3))
			{
				ACCEL_SPIN_FZ=0;
				BIG_ACCEL_SPIN_FZ=0;
			}
			else
			{
				;
			}
		}	
		if(LONG_ACCEL_STATE==0)
		{
			if(BIG_ACCEL_SPIN_FZ==1)
			{
                if((wheel_speed_min-current_speed)>VREF_2_KPH_RESOL_CHANGE)
				{
					lds8BigSpinHoldCount++;
				}

				if(lds8BigSpinHoldCount>250)
				{
					LONG_ACCEL_STATE=1;
					lds8BigSpinHoldCount=0;
				}
			}
			else
			{
				lds8BigSpinHoldCount=0;
			}
		}
		else
		{
			if((ABS_fz==0)&&(BRAKE_SIGNAL==0))
			{
				if(BIG_ACCEL_SPIN_FZ==0)
				{
                    if(vrad_diff_max_resol_change<VREF_2_KPH_RESOL_CHANGE)
					{
						if((McrAbs(rel_lam_fl)<3)&&(McrAbs(rel_lam_fr)<3)&&(McrAbs(rel_lam_rl)<3)&&(McrAbs(rel_lam_rr)<3))
						{
							lds8BigSpinHoldCount++;
						}
					}
					else if(lds8BigSpinHoldCount>0)
					{
						lds8BigSpinHoldCount--;
					}
					else
					{
						;
					}

					if(lds8BigSpinHoldCount>20)
					{
						LONG_ACCEL_STATE=0;
						lds8BigSpinHoldCount=0;
					}
				}
			}
			else
			{

				if((FSF_fl==0)&&(FSF_fr==0))
				{
					LONG_ACCEL_STATE=0;
				}
				else if((McrAbs(rel_lam_fl)<5)||(McrAbs(rel_lam_fr)<5)||(McrAbs(rel_lam_rl)<5)||(McrAbs(rel_lam_rr)<5))
				{
                    if((vrad_diff_max_resol_change)<VREF_2_KPH_RESOL_CHANGE)
					{
						lds8BigSpinHoldCount++;
						if(lds8BigSpinHoldCount>14)
						{
							LONG_ACCEL_STATE=0;
						}
					}
				}
				else
				{
					;
				}
			}
		}		
	}
#if	__4WD_VARIANT_CODE==ENABLE
}  
#endif		
	#endif	
}
/*-----------------------------------------------------------------------*/

	#if __SPIN_DET_IMPROVE == ENABLE

void  LDABS_vDetLoadgradient(void)
{
	lds16vehgradientGold =  lds16vehgradientG ;
	ls16vehgradient_tmp  =  (ls16accel) - (ls16wheelaccel);
	lds16tempinput       =  FZ1.vrselect_resol_change;
	ldu8grdindexold      =  ldu8grdindex;
	ACC_CALC = (struct ACC_FILTER *)&WHEEL_ACC;
	LDABS_vCalcEachFilterSignalAccel( lds16tempinput, 40, 10, 10, 44 );
	ls16wheelaccel = WHEEL_ACC.lsesps16SignalAccel;
	ACC_CALC = (struct ACC_FILTER *)&GRD_ACC;
	tempcnt1 = McrAbs(ls16wheelgaccel - ls16accelacc);

	/*N 단에서 뒤로 밀리는 케이스 감지 필요*/

	/*R or D gear shift detection grade change*/
	ldu1gradechangeflag = 0;
	if(ldu8gearpositionforgrd != 7)
	{
		testflag00 = 1;
		if(gs_pos == 7)
		{
			testflag00 = 2;
			ldu1gradechangeflag = 1;
		}
	}
	else if(ldu8gearpositionforgrd == 7) /*R -> D*/
	{
		testflag00 = 3;
		if(gs_pos != 7)
		{
			testflag00 = 4;
			ldu1gradechangeflag = 1;
		}
	}
	else
	{
		testflag00 = 5;
	}
	if(ldu1gradechangeflag == 0)
	{
		if(vref_resol_change < VREF_1_KPH_RESOL_CHANGE)/*저속 구배 판단 : 휠 스피드와 종가속도센서의 응답성 차이 보상을 위함*/
		{
			tempcnt_tmp = LCESP_s16Lpf1Int(tempcnt1,tempcnt_tmp,L_U8FILTER_GAIN_10MSLOOP_1HZ);
			if(tempcnt_tmp == 0)
			{
				if(ldu8vehiclestopstablecnt < 255)ldu8vehiclestopstablecnt = ldu8vehiclestopstablecnt + 1;
			}
			else
			{
				ldu8vehiclestopstablecnt = 0;
			}
			if(ldu8vehiclestopstablecnt > 10)
			{
				testflag00 = 6;
				ldu8grdindex = 2 ;
				ldu8gearpositionforgrd = gs_pos;
				LDABS_vCalcEachFilterSignalAccel(ls16vehgradient_tmp, 10, 10 , 10 , 10) ;
				lds16vehgradientGtmp = GRD_ACC.lsesps16SignalFilter;
				lds16vehgradientG    = lds16vehgradientGtmp/10;
			}
			else
			{
				testflag00 = 7;
				ldu8grdindex = 12 ;
				lds16vehgradientG = lds16vehgradientGold;
			}
		}
		else/*주행 중 노이즈 구간 필터링*/
		{
			tempcnt_tmp = tempcnt1;
			ldu8vehiclestopstablecnt = 0;
			if(ldu8vehSpinCnt < 3)
			{
				if(tempcnt_tmp < 20)
				{
					if(ldu8vehiclestablecnt < 255)ldu8vehiclestablecnt = ldu8vehiclestablecnt + 1;
				}
				else
				{
					ldu8vehiclestablecnt = 0;
				}

				if(ldu8vehiclestablecnt > 5)
				{
					testflag00 = 8;
					ldu8grdindex = 2 ;
					ldu8gearpositionforgrd = gs_pos;
					LDABS_vCalcEachFilterSignalAccel( ls16vehgradient_tmp, 10, 10 , 10 , 10 ) ;
					lds16vehgradientGtmp = GRD_ACC.lsesps16SignalFilter;
					lds16vehgradientG    = lds16vehgradientGtmp/10;
				}
				else
				{
					testflag00 = 9;
					ldu8grdindex = 12;
					lds16vehgradientG = lds16vehgradientGold;
				}
			}
			else
			{
				testflag00 = 10;
				ldu8grdindex = 12 ;
				lds16vehgradientG = lds16vehgradientGold;
				ldu8vehiclestablecnt = 0;
			}
		}
	}
	else
	{
		ldu8grdindex = 2 ;
		ldu8gearpositionforgrd = gs_pos;
		lds16vehgradientG      = -lds16vehgradientGtmp/10;
		GRD_ACC.lsesps16SignalFilter = -lds16vehgradientGtmp;
	}

}

void  LDABS_vDetVehSpin(void)
{
	if(USE_ALONG == 1)
	{
		ldu8vehSpinCntOld = ldu8vehSpinCnt;

		LDABS_vDetWLSpin(&SPIN_FL, &FL, ls16wheelgaccelFL, ls16accelacc, ls16wheelaccelFL);
		LDABS_vDetWLSpin(&SPIN_FR, &FR, ls16wheelgaccelFR, ls16accelacc, ls16wheelaccelFR);
		LDABS_vDetWLSpin(&SPIN_RL, &RL, ls16wheelgaccelRL, ls16accelacc, ls16wheelaccelRL);
		LDABS_vDetWLSpin(&SPIN_RR, &RR, ls16wheelgaccelRR, ls16accelacc, ls16wheelaccelRR);

		ldu8vehSpinCnt    =   SPIN_FL.ldu1SelWLSpinDet + SPIN_FR.ldu1SelWLSpinDet
							+ SPIN_RL.ldu1SelWLSpinDet + SPIN_RR.ldu1SelWLSpinDet;
		ldu1vehSpinDetOld = ldu1vehSpinDet;
		if(ldu1vehSpinDet == 0)
		{
			if(ldu8vehSpinCnt > 3)ldu1vehSpinDet = 1;
		}
		else
		{
			if(ldu8vehSpinCnt < 3)ldu1vehSpinDet = 0;
		}
	}
	else
	{
		ldu8vehSpinCnt = 0;
		ldu8vehSpinCntOld = 0;
		SPIN_FL.ldu1SelWLSpinDet = 0;
		SPIN_FR.ldu1SelWLSpinDet = 0;
		SPIN_RL.ldu1SelWLSpinDet = 0;
		SPIN_RR.ldu1SelWLSpinDet = 0;
	}
}
void LDABS_vCalVehicleSpeed(void)
{
	if(BRAKING_STATE == 0)
	{
		if(vref_resol_change > VREF_1_KPH_RESOL_CHANGE)
		{
			if(ldu1grdstateduration == 0)
			{
				if(ldu8grdindex == 2)
				{
					ldu1grdstateduration = 1;
					ldu8grdstatecnt = L_U8_TIME_10MSLOOP_500MS;
				}
			}
			else
			{
				ldu8grdstatecnt = ldu8grdstatecnt - 1;
				if(ldu8grdstatecnt == 0)
				{
					ldu1grdstateduration = 0;
				}
			}
			if(ldu1grdstateduration == 1)
			{
				ldu8offsetcnt = 0;
			}
			else
			{
				if(ldu8offsetcnt < 100)
				{
					ldu8offsetcnt = ldu8offsetcnt + 1;
					ldu8offset    = ldu8offsetcnt / L_U8_TIME_10MSLOOP_100MS;
				}
			}

			LDABS_vCalAxITG(&AX_1_P, along_vref, lds16vehgradientG , ldu8offset);

			if((ldu1vehSpinDetOld != 0)&&(ldu1vehSpinDet == 0))
			{
				AX_1_P.lds16AxSenForVrefCac = 0;
				AX_1_P.lds16VdiffByAxSenmod = 0;
				AX_1_P.lds16VdiffByAxSen    = 0;
				AX_1_P.lds16VspeedByAxSen   = FZ1.vrselect_resol_change;
				ldu8offsetcnt = 0;
				ldu8offset = 0;
			}
			else if(AX_1_P.lds16VspeedByAxSen > FZ1.voptfz_resol_change)
			{
				AX_1_P.lds16AxSenForVrefCac = 0;
				AX_1_P.lds16VdiffByAxSenmod = 0;
				AX_1_P.lds16VdiffByAxSen    = 0;
				AX_1_P.lds16VspeedByAxSen   = FZ1.voptfz_resol_change;
				ldu8offsetcnt = 0;
				ldu8offset = 0;
			}
		}
		else
		{
			AX_1_P.lds16AxSenForVrefCac = 0;
			AX_1_P.lds16VdiffByAxSenmod = 0;
			AX_1_P.lds16VdiffByAxSen    = 0;
			AX_1_P.lds16VspeedByAxSen   = vref_resol_change;
			ldu8offsetcnt = 0;
			ldu8offset = 0;
		}
	}
	else
	{
		AX_1_P.lds16AxSenForVrefCac = 0;
		AX_1_P.lds16VdiffByAxSenmod = 0;
		AX_1_P.lds16VdiffByAxSen    = 0;
		AX_1_P.lds16VspeedByAxSen   = vref_resol_change;
		ldu8offsetcnt = 0;
		ldu8offset = 0;
	}
}

void  LDABS_vDetWLSpin(struct  WL_SPIN_STRUCT  *SPIN_WL, const struct W_STRUCT *WL, int16_t factor1, int16_t factor2, int16_t factor3)
{
	ls16Spindetfactor = factor1 - factor2;

	if((along >= 0)&&(factor3 >= 0))
	{
		SPIN_WL->ldu1SameSign = 1;
	}
	else if((along <= 0)&&(factor3 <= 0))
	{
		SPIN_WL->ldu1SameSign = 1;
	}
	else
	{
		SPIN_WL->ldu1SameSign = 0;
	}
	if(BRAKING_STATE == 0)
	{
		if(vref_resol_change > VREF_1_KPH_RESOL_CHANGE)
		{
			if(SPIN_WL->ldu1SelWLSpinDet == 0)
			{
				if((McrAbs(ls16Spindetfactor) > 50)&&(WL->vrad_resol_change > AX_1_P.lds16VspeedByAxSen)&&(mtp > 5))
				{
					SPIN_WL->lu8spindetcnt = SPIN_WL->lu8spindetcnt + 1;
				}
				else
				{
					SPIN_WL->lu8spindetcnt = 0;
				}
				if(SPIN_WL->lu8spindetcnt > 6)
				{
					SPIN_WL->ldu1SelWLSpinDet = 1;
					SPIN_WL->lu8spindetcnt = 0;
				}
			}
			else
			{
				if(McrAbs(WL->vrad_resol_change - AX_1_P.lds16VspeedByAxSen) < VREF_2_KPH_RESOL_CHANGE )
				{
					if(ldrtau8RtaState == 0)
					{
						if(eng_torq > 0)
						{
							if((SPIN_WL->ldu1SameSign == 1))
							{
								if(McrAbs(ls16Spindetfactor) < 10)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 5;
								}
								else if(McrAbs(ls16Spindetfactor) < 30)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 3;
								}
								else if(McrAbs(ls16Spindetfactor) < 50)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 1;
								}
								else
								{
									SPIN_WL->lu8spinclrcnt = 0;
								}
							}
							else
							{
								SPIN_WL->lu8spinclrcnt = 0;
							}
						}
						else
						{
							if(McrAbs(ls16Spindetfactor) < 10)
							{
								SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 5;
							}
							else if(McrAbs(ls16Spindetfactor) < 30)
							{
								SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 3;
							}
							else if(McrAbs(ls16Spindetfactor) < 50)
							{
								SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 1;
							}
							else
							{
								SPIN_WL->lu8spinclrcnt = 0;
							}
						}
					}
					else
					{
						if(eng_torq > 0)
						{
							if((SPIN_WL->ldu1SameSign == 1)
							&&(vrad_diff_max_resol_change < VREF_3_KPH_RESOL_CHANGE))
							{
								if(McrAbs(ls16Spindetfactor) < 10)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 5;
								}
								else if(McrAbs(ls16Spindetfactor) < 30)/*no rough*/
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 3;
								}
								else if(McrAbs(ls16Spindetfactor) < 50)/*rough*/
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 1;
								}
								else
								{
									SPIN_WL->lu8spinclrcnt = 0;
								}
							}
							else
							{
								SPIN_WL->lu8spinclrcnt = 0;
							}
						}
						else
						{
							if((vrad_diff_max_resol_change < VREF_3_KPH_RESOL_CHANGE))
							{
								if(McrAbs(ls16Spindetfactor) < 10)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 5;
								}
								else if(McrAbs(ls16Spindetfactor) < 30)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 3;
								}
								else if(McrAbs(ls16Spindetfactor) < 50)
								{
									SPIN_WL->lu8spinclrcnt = SPIN_WL->lu8spinclrcnt + 1;
								}
								else
								{
									SPIN_WL->lu8spinclrcnt = 0;
								}
							}
							else
							{
								SPIN_WL->lu8spinclrcnt = 0;
							}
						}
					}

					if(McrAbs(WL->vrad_resol_change - vref_resol_change) <= VREF_1_KPH_RESOL_CHANGE)
					{
						SPIN_WL->lu8spinclrcnt2 = SPIN_WL->lu8spinclrcnt2 + 1;
					}
					else
					{
						SPIN_WL->lu8spinclrcnt2 = 0;
					}
					if((SPIN_WL->lu8spinclrcnt > 75)||(SPIN_WL->lu8spinclrcnt2 > 20))
					{
						SPIN_WL->ldu1SelWLSpinDet = 0;
						SPIN_WL->lu8spinclrcnt = 0;
						SPIN_WL->lu8spinclrcnt2 = 0;
					}
					else
					{
						;
					}
				}
			}
		}
		else
		{
			SPIN_WL->ldu1SelWLSpinDet = 0;
			SPIN_WL->lu8spinclrcnt = 0;
			SPIN_WL->lu8spindetcnt = 0;
		}
	}
	else
	{
		SPIN_WL->ldu1SelWLSpinDet = 0;
		SPIN_WL->lu8spinclrcnt = 0;
		SPIN_WL->lu8spindetcnt = 0;
	}
}
void  LDABS_vCalAxITG(struct AX_ITG_STRUCT *AX_P, int16_t Ax_sen, int16_t road_grad , int16_t offset)
{
	AX_P->lds16VspeedByAxSenOld = AX_P->lds16VspeedByAxSen;
	AX_P->lds16AxSenForVrefCac = Ax_sen + along_by_drift - road_grad + offset ;
	if(AX_P->lds16AxSenForVrefCac < 5)
	{
		AX_P->lds16AxSenForVrefCac = 5;
	}
	tempW2 =(int16_t)(((AX_P->lds16AxSenForVrefCac )*8) + AX_P->lds16VdiffByAxSenmod);
	AX_P->lds16VdiffByAxSen = ((AX_P->lds16AxSenForVrefCac*8) +  AX_P->lds16VdiffByAxSenmod)/35;
	AX_P->lds16VdiffByAxSenmod = (tempW2 - (AX_P->lds16VdiffByAxSen*35));
	AX_P->lds16VspeedByAxSen = AX_P->lds16VspeedByAxSen + AX_P->lds16VdiffByAxSen;

}
	#endif

#if	(__AX_SENSOR==1)
void LDABS_vLimitLongAccInput(void)
{
	/*--------------------
	along-->ax_filter :2006.07.29

	along_vref=along;
	----------------------*/
    #if __SPIN_DET_IMPROVE == 1
	along_vref = lsabss16RawAxSignalTemp/10;
	#else
    along_vref = along;
    #endif
    if(USE_ALONG==1){
		/* Braking State */
		if((BRAKING_STATE==1)||(ACTIVE_BRAKE_ACT==1))
		{
			if(along_vref>0)
			{
				along_vref=-along_vref;
			}
			else
			{
				;
			}
		}
		else
		{
			#if	__VDC
            if((VDC_REF_WORK == 1)&&(ACCEL_SPIN_FZ == 0))
			{
				if(along_vref>5)
				{
					along_vref=5;
				}
				else
				{
					;
				}
			}
			else
			{
			#endif
/*			Engine Braking State Case, Along Limitation
				#if	__EDC
				if(Engine_braking_flag==1) {
					if(along_vref>5)along_vref=5;
				}
				else {
					if(along_vref<0)along_vref=0;
				}
				#else
*/
				if(along_vref<0)
				{
					along_vref=0;
				}
				else
				{
					;
				}
/*
				#endif
*/
			#if	__VDC
			}
			#endif
		}
	}
}
/*******************************************************************/
		#if	__VDC
void   LDABS_vEstLongAccSensorDrift(void)
{
 /*----------------------------------------------------------------
	Vx_dot = Ax	+ Vy*YawRate/(3.6*9.81*57.3*100)
 ----------------------------------------------------------------*/
/*
	tempW2 = yaw_out;
	tempW1 = 1;
	tempW0 = 10;
	s16muls16divs16();
	tempW4 = tempW3;
*/
/*
	tempW2 = Vy_MD;
	tempW1 = tempW4;
	tempW0 = 20236;	 //	20236 =	3.6*9.81*57.3*10 
	s16muls16divs16();
	along_by_drift = tempW3;
*/
	along_by_drift=(int16_t) ( ((int32_t) Vy_MD	* (	yaw_out	 / 10 )) / 20236 );	
/*
	if(	along_by_drift>50 )
	{
		along_by_drift=50;
	}
	else if( along_by_drift<-100 )
	{
		along_by_drift=-100;
	}
	else
	{
		;
	}
*/
	along_by_drift=LCABS_s16LimitMinMax(along_by_drift,-100,50);
	if (ABS_fz==1)
	{
		if(McrAbs(along_by_drift)>50)
		{
			if ((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
			{
			ALONG_FAIL_BY_DRIFT_ON_ABS=1;
			}
		}
		else if(McrAbs(along_by_drift)>30)
		{
			if ((rel_lam_fl<-10)&&(rel_lam_fr<-10)&&(rel_lam_rl<-10)&&(rel_lam_rr<-10))
			{
				ALONG_FAIL_BY_DRIFT_ON_ABS=1;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		ALONG_FAIL_BY_DRIFT_ON_ABS=0;
	}

	if(VDC_REF_WORK==1)
	{
		if(McrAbs(along_by_drift)>50)
		{
			if ((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
			{
			ALONG_FAIL_BY_DRIFT_ON_ESP=1;
			}
		}
		else if(McrAbs(along_by_drift)>30)
		{
			if ((rel_lam_fl<-10)&&(rel_lam_fr<-10)&&(rel_lam_rl<-10)&&(rel_lam_rr<-10))
			{
				ALONG_FAIL_BY_DRIFT_ON_ESP=1;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		ALONG_FAIL_BY_DRIFT_ON_ESP=0;
	}

}
		#endif
/*******************************************************************/
#endif
	#endif

/*******************************************************************/
#if	 (__4WD	||(__4WD_VARIANT_CODE==ENABLE))&& __AX_SENSOR
void	LDABS_vChkLongAccReliabilty(void)
{


	USE_ALONG=1;

	/*	G-sensor Reliability Check Flag	except VREF_FAIL, Added	at 05NZ	 */
	USE_ALONG_1=1;
  #if __FOR_DV
	USE_ALONG=0;
  #endif
	if((fu1AxSusDet==1)||(fu1AxErrorDet==1)||(CONTROL_FAIL)||(G_SEN_FAIL))
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}

  #if	__CREEP_VIBRATION
    if(SUSPECT_VREF==1)
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}
  #endif


    if(VREF_FAIL_MODE==1)
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}


  #if __TCS
	if((!AUTO_TM)||(tcs_error_flg))
	{
        if(BACKWARD_MOVE==1)
		{
			USE_ALONG=0;
		}
		else
		{
			;
		}
	}
  #else
	if(BACKWARD_MOVE)
	{
		USE_ALONG=0;
	}
  #endif
	else
	{
		;
	}

   #if __4WD_VARIANT_CODE==ENABLE
	if((lsu8DrvMode	== DM_2H )||( lsu8DrvMode == DM_2WD))
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}
   #endif

	if(fu1AxSenPwr1sOk ==0)
	{
		USE_ALONG=0;
	}

#if	__VREF_USING_AX_SENSOR
	USE_ALONG=0;
#endif

	if(	!USE_ALONG )
	{
		USE_ALONG_1=0 ;
	}
	else
	{
		;
	}

	if(	(VREF_FAIL)	|| (!USE_ALONG)	 )
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}

	#if	__VDC 
	if (ALONG_FAIL_BY_DRIFT_ON_ESP==1)
	{
		USE_ALONG=0;
	}
	else if	(ALONG_FAIL_BY_DRIFT_ON_ABS==1)
	{
		USE_ALONG=0;
	}
	else if(VREF_FAIL_ON_ESP==1)
	{
		USE_ALONG=0;
	}
	else
	{
		;
	}
	#endif

	if(FREE_ROLLING_DETECT==1){
		USE_ALONG=0;
	}
}
/*******************************************************************/
void	LDABS_vChk4WDVrefEstError(void)
{
	int8_t dump_cnt_flag_for_vref_fail;

	if(ABS_fz==1)
	{
/*---------------------------------------------------------------------
	CONTROL	flag SET
-----------------------------------------------------------------------
	In case	of Continuous Doum or Hold on both of FRONT	WHEEL for more than	2sec,
	And	negative slip on both of front wheels, suspect control fail
----------------------------------------------------------------------*/
		if((HV_VL_fl==1)&&(HV_VL_fr==1))
		{
			control_fail_count++;
			if(control_fail_count>L_U8_TIME_10MSLOOP_2S)
			{
				if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
				{
					CONTROL_FAIL=1;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		else
		{
			control_fail_count=0;
		}

/*-----------------------------------------------------------------------
	VREF_FAIL flag set
-------------------------------------------------------------------------
	Set	Condition:
	After 50scan of	ABS	Control	enterance
	+Front wheel is	not	First Cycle
	+All wheel Dump	or Hold	30SCAN is for more 30 scantime
	+All wheel Negative	Slip /

	Reset Condition:
	if Any of wheels is	recovered
 ----------------------------------------------------------------------*/
		tempF0=0;
		if((zyklus_z_fl<1) && (zyklus_z_fr<1))
		{
			tempF0=1;
		}
		else
		{
			;
		}
		/*50 scantime after	Braking	state enterance	*/
		if(ACC_CHANGE==0)
		{
			count_ice++;
			if(count_ice > L_U8_TIME_10MSLOOP_350MS)
			{
				ACC_CHANGE=1;
				count_ice=0;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
		/*
	#if	!__TCS
		if(VREF_FAIL==1)
		{
			if((rel_lam_fl>=0)||(rel_lam_fr>=0)||(rel_lam_rl>=0)||(rel_lam_rr>=0))
			{
				VREF_FAIL=0;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	#endif
	*/
	/*6727 MGH40-Dom Vref_fail set condition carry over*/
		if((HV_VL_fl==1)&&(HV_VL_fr==1)&&(HV_VL_rl==1)&&(HV_VL_rr==1)&&(ACC_CHANGE==1)&&(tempF0==0))
		{

			if(AV_VL_fl==1)
			{
				if(vref_fail_dump_count_fl<200)
				{
					vref_fail_dump_count_fl++;
				}
			}
			if(AV_VL_fr==1)
			{
				if(vref_fail_dump_count_fr<200)
				{
					vref_fail_dump_count_fr++;
				}
			}
			if(AV_VL_rl==1)
			{
				if(vref_fail_dump_count_rl<200)
				{
					vref_fail_dump_count_rl++;
				}
			}
			if(AV_VL_rr==1)
			{
				if(vref_fail_dump_count_rr<200)
				{
					vref_fail_dump_count_rr++;
				}
			}
			if((vref_fail_dump_count_fl>L_U8_TIME_10MSLOOP_70MS)&&(vref_fail_dump_count_fr>L_U8_TIME_10MSLOOP_70MS)
			&&(vref_fail_dump_count_rl>L_U8_TIME_10MSLOOP_70MS)&&(vref_fail_dump_count_rr>L_U8_TIME_10MSLOOP_70MS))
			{
				dump_cnt_flag_for_vref_fail=3;
			}
			else if((vref_fail_dump_count_fl>L_U8_TIME_10MSLOOP_40MS)&&(vref_fail_dump_count_fr>L_U8_TIME_10MSLOOP_40MS)
			&&(vref_fail_dump_count_rl>L_U8_TIME_10MSLOOP_40MS)&&(vref_fail_dump_count_rr>L_U8_TIME_10MSLOOP_40MS))
			{
				dump_cnt_flag_for_vref_fail=2;
			}
			else if((vref_fail_dump_count_fl>L_U8_TIME_10MSLOOP_20MS)&&(vref_fail_dump_count_fr>L_U8_TIME_10MSLOOP_20MS)
			&&(vref_fail_dump_count_rl>L_U8_TIME_10MSLOOP_20MS)&&(vref_fail_dump_count_rr>L_U8_TIME_10MSLOOP_20MS))
			{
				dump_cnt_flag_for_vref_fail=1;
			}
			else
			{
				dump_cnt_flag_for_vref_fail=0;
			}
			tempB0=0;
            if(arad_fl>ARAD_1G0){tempB0++;}
            if(arad_fr>ARAD_1G0){tempB0++;}
            if(arad_rl>ARAD_1G0){tempB0++;}
            if(arad_rr>ARAD_1G0){tempB0++;}

			if(dump_cnt_flag_for_vref_fail==0)
			{
				vref_fail_count=0;
			}
			else
			{
				if((arad_fl>ARAD_2G0)&&(arad_fr>ARAD_2G0)
				&&(arad_rl>ARAD_2G0)&&(arad_rr>ARAD_2G0))
				{
					if(vref_fail_count>L_U8_CNT_5)
					{
						vref_fail_count-=L_U8_CNT_5;
					}
					else
					{
						vref_fail_count=0;
					}
				}
				else if(tempB0<=2)/* arad 1g 이상인	wheel이	2개	이하일 때만	count */
				{
					if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
					{
						if(vrad_diff_max>=VREF_10_KPH)
						{
							;                                                          /*	vref_fail_count	hold*/
						}
						else if(vrad_diff_max>=VREF_5_KPH)
						{
							vref_fail_count++;
							if(dump_cnt_flag_for_vref_fail==2)
							{
								vref_fail_count++;
							}
							else if(dump_cnt_flag_for_vref_fail==3)
							{
								vref_fail_count+=2;
							}
						}
						else if(vrad_diff_max>=VREF_3_KPH)
						{
							vref_fail_count+=2;
							if(dump_cnt_flag_for_vref_fail==2)
							{
								vref_fail_count++;
							}
							else if(dump_cnt_flag_for_vref_fail==3)
							{
								vref_fail_count+=2;
							}
						}
						else
						{
							vref_fail_count+=3;
							if(dump_cnt_flag_for_vref_fail==2)
							{
								vref_fail_count++;
							}
							else if(dump_cnt_flag_for_vref_fail==3)
							{
								vref_fail_count+=2;
							}
						}
					}
				}
				else
				{
					;
				}
			}
			if(vref_fail_count>L_U8_TIME_10MSLOOP_1000MS)
			{
				if((rel_lam_fl<0)&&(rel_lam_fr<0)&&(rel_lam_rl<0)&&(rel_lam_rr<0))
				{
					VREF_FAIL=1;
					vref_fail_count=0;
				}
				else
				{
					vref_fail_count=L_U8_TIME_10MSLOOP_1000MS+1;
				}
			}
		}
		else
		{
			vref_fail_count=0;
			vref_fail_dump_count_fl=0;
			vref_fail_dump_count_fr=0;
			vref_fail_dump_count_rl=0;
			vref_fail_dump_count_rr=0;
			dump_cnt_flag_for_vref_fail=0;
		}
		/*2006.11.10 deleted*/

	}
	else
	{
		control_fail_count=0;
		CONTROL_FAIL=0;
		vref_fail_count=0;
		vref_fail_dump_count_fl=0;
		vref_fail_dump_count_fr=0;
		vref_fail_dump_count_rl=0;
		vref_fail_dump_count_rl=0;
		VREF_FAIL=0;
		ACC_CHANGE=0;
	}

/* --------------------------------------------------------------------------
	VREF_FAIL_MODE flag	set
-----------------------------------------------------------------------------
	Set	Condition:
	At Brakeing	State ,	Along is positive value,
	and	slip among the all wheels is greater than threshold
	and	wheel speed	difference among the wheels	isg	reater than	threshold

---------------------------------------------------------------------------*/
/*	  if((BRAKE_SIGNAL)||(ABS_fz)) */
	if(BRAKING_STATE==1)
	{
		if((USE_ALONG==1)&&(along>5))
		{
			if((rel_lam_fl<-15)&&(rel_lam_fr<-15)&&(rel_lam_rl<-15)&&(rel_lam_rr<-15))
			{
				if(vrad_diff_max>=(VREF_5_KPH))
				{
					vref_fail_mode_cnt++;
				}
				else
				{
					;
				}
				if(vref_fail_mode_cnt>=L_U8_TIME_10MSLOOP_200MS)
				{
					VREF_FAIL_MODE=1;
					vref_fail_mode_cnt=0;
				}
				else
				{
					;
				}
			}
			else
			{
				vref_fail_mode_cnt=0;
			}
		}
		else
		{
			vref_fail_mode_cnt=0;
		}

/*-----------------------------------------------------------------------------
	VEHECLE_FREE_ROLLING flag set
 ------------------------------------------------------------------------------
	Set	Condition:
	Full Dump of All wheels	are	performed for more than	0.2sec,	and	then
	No wheel acceleration, suspect NO Braking State

	Reset Condition:
	Reapply	Start
------------------------------------------------------------------------------*/
#if	__VDC
		if(FREE_ROLLING_DETECT==0){                 /* Set Condition of Free Rolling*/
			if((HV_VL_fl==1)&&(HV_VL_fr==1)&&(HV_VL_rl==1)&&(HV_VL_rr==1))
			{
				if((AV_VL_fl==1)&&(AV_VL_fr==1)&&(AV_VL_rl==1)&&(AV_VL_rr==1))
				{
					if(vehicle_free_rolling_suspect_cnt1<=(L_U8_TIME_10MSLOOP_200MS))
					{
						vehicle_free_rolling_suspect_cnt1++;
					}
					else
					{
						;
					}
				}
				else
				{
					;
				}
	
				tempB0=0;
				if(McrAbs(arad_fl)<ARAD_0G5)
				{
					tempB0++;
				}
				else
				{
					;
				}
				if(McrAbs(arad_fr)<ARAD_0G5)
				{
					tempB0++;
				}
				else
				{
					;
				}
				if(McrAbs(arad_rl)<ARAD_0G5)
				{
					tempB0++;
				}
				else
				{
					;
				}
				if(McrAbs(arad_rr)<ARAD_0G5)
				{
					tempB0++;
				}
				else
				{
					;
				}
					tempW0=McrAbs(yaw_c_vref)*2;
					if((tempB0<=1)||(vrad_diff_max>=(VREF_1_5_KPH+tempW0)))
				{
					vehicle_free_rolling_suspect_cnt2=0;
				}
				else
				{
					if(vehicle_free_rolling_suspect_cnt2<=(L_U8_TIME_10MSLOOP_200MS))
					{
							if((tempB0==4)&&(vrad_diff_max<=(VREF_1_KPH+tempW0)))
						{
							vehicle_free_rolling_suspect_cnt2+=2;
						}
							else if((tempB0==3)&&(vrad_diff_max<=(VREF_1_KPH+tempW0)))
						{
							vehicle_free_rolling_suspect_cnt2++;
						}
						else
						{
							;
						}
					}
					else
					{
						;
					}
				}
				if((vehicle_free_rolling_suspect_cnt1>=L_U8_TIME_10MSLOOP_200MS)&&(vehicle_free_rolling_suspect_cnt2>=L_U8_TIME_10MSLOOP_200MS))
				{
					FREE_ROLLING_DETECT=1;
				}
				else
				{
					;
	
				}
			}
			else
			{
				vehicle_free_rolling_suspect_cnt1=0;
				vehicle_free_rolling_suspect_cnt2=0;
				FREE_ROLLING_DETECT=0;
			}
		}
		else{/* Reset Condition of Free Rolling*/
			
			vehicle_free_rolling_suspect_cnt1=0;
			vehicle_free_rolling_suspect_cnt2=0;
			FREE_ROLLING_DETECT=0;
		}
#endif
	}
	else
	{
		VREF_FAIL_MODE=0;
		vref_fail_mode_cnt=0;
		#if	__VDC
		vehicle_free_rolling_suspect_cnt1=0;
		vehicle_free_rolling_suspect_cnt2=0;
		FREE_ROLLING_DETECT=0;
		#endif
	}

/*-----------------------------------------------------------------------------
	G_Sensor Error On ESP BRAKE	CONTROL	 flag set
 ------------------------------------------------------------------------------
	Set	Condition:
	When ESP BRAKE Control,	yaw/wstr/lateral G are greater than	threshold
	+ difference between along and ebd_filt_grv	is greater than	threshold
	+difference	between	voptfz1	and	vref is	greater	than threshold
	+All Wheel slips are  negative

	Reset Condition:
	difference between voptfz and FZ_for_4WD.voptfz	is smaller than	threshold
 ------------------------------------------------------------------------------*/
	#if	__VDC
    if(VDC_REF_WORK==1)
	{
		if(VREF_FAIL_ON_ESP==0)
		{
			if((ABS_fz==0)&& (ESP_SENSOR_RELIABLE_FLG==1))
			{
				if((McrAbs(yaw_out)>YAW_15DEG)&&(abs_wstr>WSTR_30_DEG)&&(McrAbs(alat)>LAT_0G3G))
				{
					if(along>-10)
					{
						if(ebd_refilt_grv<-40)
						{
							if((vref-voptfz1)>VREF_3_KPH)
							{
/*								
								tempW0=vref;
								tempW1=100;
								tempW2=vref-voptfz1;
								s16muls16divs16();
*/
								tempW3=(int16_t) ( ((int32_t) (vref-voptfz1) * 100 )/ vref );
								if(tempW3>5)
								{
									if(vref_fail_cnt_on_esp<L_U8_TIME_10MSLOOP_700MS)
									{
										vref_fail_cnt_on_esp++;
									}
									else
									{
										;
									}
								}
								else if(tempW3<3)
								{
									vref_fail_cnt_on_esp=0;
								}
								else
								{
									if(vref_fail_cnt_on_esp>0)
									{
										vref_fail_cnt_on_esp--;
									}
									else
									{
										;
									}
								}
								if(vref_fail_cnt_on_esp>L_U8_TIME_10MSLOOP_350MS)
								{
									if((vref-vrad_crt_fl>VREF_2_KPH)&&(vref-vrad_crt_fr>VREF_2_KPH)&&(vref-vrad_crt_rl>VREF_2_KPH)&&(vref-vrad_crt_rr>VREF_2_KPH))
									{
										VREF_FAIL_ON_ESP=1;
									}
									else
									{
										;
									}
								}
								else
								{
									;
								}
							}
							else if((vref-voptfz1)<=VREF_1_KPH)
							{
								vref_fail_cnt_on_esp=0;
							}
							else
							{
								;
							}
						}
						else if(vref_fail_cnt_on_esp>0)
						{
							vref_fail_cnt_on_esp--;
						}
						else
						{
							;
						}
					}
					else if(vref_fail_cnt_on_esp>0)
					{
							vref_fail_cnt_on_esp--;
					}
					else
					{
						;
					}
				}
				else
				{
					if(McrAbs(alat)<LAT_0G2G)
					{
						vref_fail_cnt_on_esp=0;
					}
					else if(McrAbs(yaw_out)<YAW_15DEG)
					{
						vref_fail_cnt_on_esp=0;
					}
					else
					{
						;
					}
				}
			}
			else
			{
				;
			}
		}
		else
		{
			if((FZ_for_4WD.voptfz-voptfz1)<VREF_0_KPH5)
			{
				VREF_FAIL_ON_ESP=0;
				vref_fail_cnt_on_esp=0;
			}
			else
			{
				;
			}
		}
	}
	else
	{
		VREF_FAIL_ON_ESP=0;
		vref_fail_cnt_on_esp=0;
	}
	#endif
}
#endif
/*******************************************************************/
#if	__AX_SENSOR
#if	__CAR!=	GM_T300
void	LDABS_vDctBackDrivingByLongAcc(void)
{
#if (HSA_VARIANT_ENABLE==ENABLE)
	if(wu8HsaVariantCodingEndFlag==1)
	{
#endif		
	if(speed_calc_timer	< (UCHAR)L_U8_TIME_10MSLOOP_1000MS)
	{
		standstill_count=0;
	}
	else
	{
		;
	}

	if(voptfz2<=18)
	{
		BACKWARD_MOVE=0;
		standstill_count++;
		if(STANDSTILL==0)
		{
		#if	__TCS || __ETC
			if(standstill_count>L_U8_TIME_10MSLOOP_1000MS)
		#else
			if(standstill_count>L_U8_TIME_10MSLOOP_1400MS)
		#endif
			{
				standstill_count=0;
				along_standstill=along;
				di_along=0;
				STANDSTILL=1;
				EFFECTIVE_ALONG=0;
			}
			else
			{
				;
			}
		}
		else
		{
			if(standstill_count>L_U8_TIME_10MSLOOP_500MS)
			{
				EFFECTIVE_ALONG=1;
			}
			else
			{
				;
			}
		}
	}

	if(STANDSTILL==1)
	{
	#if	__TCS || __ETC
		if(voptfz2<=40)
	#else
		if(voptfz2<=80)
	#endif
		{
			di_along+=(along-along_old);
		}
		else
		{
		#if	__TCS || __ETC
			if((di_along<0)&&(EFFECTIVE_ALONG==1))
		#else
			if((di_along<-2)&&(EFFECTIVE_ALONG==1))
		#endif
			{
				standstill_count=0;
				di_along=0;
				BACKWARD_MOVE=1;
				STANDSTILL=0;
			}
			else
			{
				;
			}
		}
	}
#if	__TCS || __ETC
	if(voptfz2>48)
#else
	if(voptfz2>88)
#endif
	{
		along_standstill=0;
		standstill_count=0;
		di_along=0;
		STANDSTILL=0;
	}
	else
	{
		;
	}

	if(vref>(int16_t)VREF_70_KPH)
	{
		BACKWARD_MOVE=0;
		grv_diff_cnt=0;
	}
	else if(voptfz2>18)
	{
		tempW1=(int16_t)McrAbs(ebd_refilt_grv);
		tempW2=(int16_t)McrAbs(along);
		tempW3=tempW1;
		if(tempW1>tempW2)
		{
			tempW3=tempW2;
		}
		else
		{
			;
		}
		#if	__VDC
		if(VDC_REF_WORK==0)
		{
		#endif
			if(((ebd_refilt_grv<0)&&(along<0))||((ebd_refilt_grv>0)&&(along>0)))
			{
				grv_diff_cnt=0;
			}
			else if((ebd_refilt_grv<0)&&(along>0))
			{
				if((ABS_fz==1) || (BRAKE_SIGNAL==1)	|| ((EBD_rl==1)&&(EBD_rr==1)))
				{
					if(tempW3>5)
					{
/*						
						tempW2=tempW3;
						tempW1=1;
						tempW0=10;
						s16muls16divs16();
*/						
						grv_diff_cnt=grv_diff_cnt+(uint8_t)(tempW3/10)+1;
					}
					else
					{
						;
					}
				}
			}
			else if((ebd_refilt_grv>0)&&(along<0))
			{
				if((ABS_fz==1) || (BRAKE_SIGNAL==1)	|| ((EBD_rl==1)&&(EBD_rr==1)))
				{
					if(tempW3>20)
					{
	/*					grv_diff_cnt=grv_diff_cnt++; */
					}
					else
					{
						;
					}
				}
				else
				{
					if(tempW3>5)
					{
/*						
						tempW2=tempW3;
						tempW1=1;
						tempW0=10;
						S16Muls16Divs16();
*/
						grv_diff_cnt=grv_diff_cnt+(uint8_t)(tempW3/10)+1;
					}
					else
					{
						;
					}
				}
			}
			else if((ebd_refilt_grv==0)||(along==0))
			{
				if(grv_diff_cnt>2)
				{
					grv_diff_cnt=grv_diff_cnt-2;
				}
			}
			else
			{
				;
			}
		#if	__VDC
		}
		#endif

		tempF0=0;

		if(grv_diff_cnt>=L_U8_TIME_10MSLOOP_1400MS)
		{
			tempF0=1;
		}
		else if	(grv_diff_cnt>L_U8_TIME_10MSLOOP_700MS)
		{
			if((ABS_fz==0) &&(BRAKE_SIGNAL==0)&&((EBD_rl==0)||(EBD_rr==0)))
			{
				tempF0=1;
			}
		}
		else
		{
			;
		}

		if(tempF0==1)
		{
			if(BACKWARD_MOVE==1)
			{
				BACKWARD_MOVE=0;
			}
			else
			{
				BACKWARD_MOVE=1;
			}
			grv_diff_cnt=0;
		}
	}
	else
	{
		BACKWARD_MOVE=0;
		grv_diff_cnt=0;
	}
	#if (HSA_VARIANT_ENABLE==ENABLE)
	}
	else
	{
		BACKWARD_MOVE=0;
		grv_diff_cnt=0;	
		along_standstill=0;
		standstill_count=0;
		di_along=0;
		STANDSTILL=0;			
	}
	#endif		

}
#endif
#endif
/*******************************************************************/
void LDABS_vDetABSEntrySpeed(void)
{
	int16_t	vehicle_speed;

	vehicle_speed=vref_resol_change;

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{

		ABS_entry_speed=VREF_MIN_SPEED;
		ABS_entry_speed2=VREF_MIN_SPEED;
		ABS_fz_K=0;
		ABS_fz_OLD=0;
		BRAKE_SIGNAL_K=0;
		BRAKE_SIGNAL_OLD=0;
		#if	__VDC
		#if	__CAR!=	GM_T300
		ESP_entry_speed=vehicle_speed;
		VDC_REF_WOKR_OLD=0;
		#endif
		#endif
	}
	else
	{
		if(ABS_fz==1)
		{
			if(ABS_fz_OLD==0)
			{
				ABS_fz_K=1;
			}
			else
			{
				ABS_fz_K=0;
			}
		}
		else
		{
			 ABS_fz_K=0;
		}

		if(BRAKE_SIGNAL==1)
		{
			if(BRAKE_SIGNAL_OLD==0)
			{
				BRAKE_SIGNAL_K=1;
			}
			else
			{
				BRAKE_SIGNAL_K=0;
			}
		}
		else
		{
			BRAKE_SIGNAL_K=0;
		}

	
		if(BRAKING_STATE==1){
			if(BRAKING_STATE_OLD==0){
				if(ABS_entry_speed2<vref_resol_change){
					ABS_entry_speed=vref_resol_change;
					ABS_entry_speed2=vref_resol_change;
					ldu1AbsEntrySpeedUpdated=1;
				}
							
			}
			else{
				;
			}
			if(ldu1AbsEntrySpeedUpdated==1){
				if((ABS_entry_speed-vehicle_speed)<VREF_5_KPH_RESOL_CHANGE){
					ABS_entry_speed=ABS_entry_speed+3;
				}
				if(ABS_entry_speed>VREF_MAX_SPEED){
					ABS_entry_speed=VREF_MAX_SPEED;
				}					 
			}			  
			
		}
		else {
			if (BRAKING_STATE_OLD==1){
				ABS_entry_speed=VREF_MIN_SPEED;
				ABS_entry_speed2=VREF_MIN_SPEED;   
				ldu1AbsEntrySpeedUpdated=0;				
			}		
			else{
				if(BRAKE_SIGNAL_K==1){
					if(ABS_entry_speed2<vref_resol_change){
						ABS_entry_speed=vref_resol_change;
						ABS_entry_speed2=vref_resol_change;
						ldu1AbsEntrySpeedUpdated=1;
					}
				}
				
				if(ABS_fz_K==1){
					if(ABS_entry_speed2<vref_resol_change){
						ABS_entry_speed=vref_resol_change;
						ABS_entry_speed2=vref_resol_change;
						ldu1AbsEntrySpeedUpdated=1;
					}
				}
				
				if((BRAKE_SIGNAL==0)&&(ABS_fz==0)){		 
					ABS_entry_speed=VREF_MIN_SPEED;
					ABS_entry_speed2=VREF_MIN_SPEED;   
					ldu1AbsEntrySpeedUpdated=0;	   
				}
				else{
					;
				}
				
				if(ldu1AbsEntrySpeedUpdated==1){
					if((ABS_entry_speed-vehicle_speed)<VREF_5_KPH_RESOL_CHANGE){
						ABS_entry_speed=ABS_entry_speed+3;
					}
					if(ABS_entry_speed>VREF_MAX_SPEED){
						ABS_entry_speed=VREF_MAX_SPEED;
					}					 
				}			
			}	  
		}
			
/*
		if(BRAKE_SIGNAL==1)
		{
			if(BRAKE_SIGNAL_K==1)
			{
				ABS_entry_speed=vehicle_speed;
				ABS_entry_speed2=vehicle_speed;
			}
			else
			{
				if((ABS_entry_speed-vehicle_speed)<VREF_5_KPH_RESOL_CHANGE)
				{
					ABS_entry_speed=ABS_entry_speed+3;
				}
				if(ABS_entry_speed>VREF_MAX_SPEED)
				{
					ABS_entry_speed=VREF_MAX_SPEED;
				}
			}
		}
		else
		{
			if(ABS_fz==0)
			{
				ABS_entry_speed=vehicle_speed;
				ABS_entry_speed2=vehicle_speed;
			}
			else
			{
				if(ABS_fz_K==1)
				{
					ABS_entry_speed=vehicle_speed;
					ABS_entry_speed2=vehicle_speed;
				}
				else
				{
					if((ABS_entry_speed-vehicle_speed)<VREF_5_KPH_RESOL_CHANGE)
					{
						ABS_entry_speed=ABS_entry_speed+3;
					}
					if(ABS_entry_speed>VREF_MAX_SPEED)
					{
						ABS_entry_speed=VREF_MAX_SPEED;
					}
				}
			}
		}
		*/
		#if	__VDC
		#if	__CAR!=	GM_T300
		if(VDC_REF_WORK==1)
		{
			if(VDC_REF_WOKR_OLD==0)
			{
				if(ABS_fz==0)
				{
					ESP_entry_speed=vehicle_speed;
				}
			}
			else
			{

			}
		}
		else
		{
			ESP_entry_speed=vehicle_speed;
		}
		#endif
		#endif
		BRAKE_SIGNAL_OLD=BRAKE_SIGNAL;
		ABS_fz_OLD=ABS_fz;

		#if	__VDC
		#if	__CAR!=	GM_T300
		VDC_REF_WOKR_OLD=VDC_REF_WORK;
		#endif
		#endif
	}
}
/*---------------------------------------------
Vehicle	Accel Spin Detect on Brake
----------------------------------------------
 #if __4WD || (__4WD_VARIANT_CODE==ENABLE)
#if	__ACCEL_ON_BRK_VREF
void LDABS_vDetVicleAccelSpinOnBrake(void)
{
	int8_t ldvrefu1SatisfyEngCondition=0;
	int8_t ldvrefu1SatisfyDvCondition=0;
	int16_t	 ldvrefs16CalVrefVariation;
	int16_t	 ldvrefs16VehicleSpeed;
	int16_t	 ldvrefs16SpinSetEngTorqThr;
	int16_t	 ldvrefs16SpinSetMtpThr;
	int16_t	 ldvrefs16SpinSetDvThr;
	int16_t	 ldvrefs16SpinSetVrefDiffThr1;
	int16_t	 ldvrefs16SpinSetVrefDiffThr2;
	int16_t	 ldvrefs16SpinResetEngTorqThr;
	int16_t	 ldvrefs16SpinResetMtpThr;
	int16_t	 ldvrefs16SpinResetDvThr;
	int16_t	 ldvrefs16SpinSetTime;
	int16_t	 ldvrefs16SpinResetTime;
	ldvrefs16VehicleSpeed=vref_resol_change;
	ldvrefs16CalVrefVariation=vref_resol_change-vref_alt_resol_change;



	#if	(__4WD_VARIANT_CODE==ENABLE)
	if((lsu8DrvMode	== DM_4H)||(lsu8DrvMode	== DM_AUTO))
	{
	#endif
		if(BRAKING_STATE==1)
		{
			//Set Contdition
			if(ldvrefu1DctVehAccelSpinOnBrk==0)
			{
				#if	__ENGINE_INFORMATION
				if(ldabsu1CanErrFlg==0)
				{
					ldvrefs16SpinSetEngTorqThr=TORQ_PERCENT_40PRO;
					ldvrefs16SpinSetMtpThr=MTP_20_P;

					ldvrefs16SpinSetDvThr=VREF_2_KPH_RESOL_CHANGE;
					ldvrefs16SpinSetVrefDiffThr1=VREF_0G125_KPH_RESOL_CHANGE;
					ldvrefs16SpinSetVrefDiffThr2=0;

					ldvrefs16SpinSetTime=L_U8_TIME_10MSLOOP_200MS;
					if((mtp>ldvrefs16SpinSetMtpThr)&&(eng_torq_rel>ldvrefs16SpinSetEngTorqThr))
					{
						ldvrefu1SatisfyDvCondition=1;
					}

					if((wheel_speed_min-ldvrefs16VehicleSpeed)>ldvrefs16SpinSetDvThr)
					{
						ldvrefu1SatisfyDvCondition=1;
					}
					if((ldvrefu1SatisfyEngCondition==1)&&(ldvrefu1SatisfyDvCondition==1))
					{
						if(ldvrefs16CalVrefVariation>=ldvrefs16SpinSetVrefDiffThr1)
						{
							ldvrefu8CntVehAccelSpinOnBrk++;
						}
						else if((ldvrefs16CalVrefVariation<ldvrefs16SpinSetVrefDiffThr2)&&(ldvrefu8CntVehAccelSpinOnBrk>0))
						{
							ldvrefu8CntVehAccelSpinOnBrk--;
						}
						else
						{
							;
						}

						if(ldvrefu8CntVehAccelSpinOnBrk>ldvrefs16SpinSetTime)
						{
							ldvrefu1DctVehAccelSpinOnBrk=1;
							ldvrefu8CntVehAccelSpinOnBrk=0;
						}
					}
					else
					{
						ldvrefu8CntVehAccelSpinOnBrk=0;
					}
				}
				else
				{
				#endif

					ldvrefs16SpinSetDvThr=VREF_3_KPH_RESOL_CHANGE;
					ldvrefs16SpinSetVrefDiffThr1=VREF_0G125_KPH_RESOL_CHANGE+2;
					ldvrefs16SpinSetVrefDiffThr2=0;

					ldvrefs16SpinSetTime=L_U8_TIME_10MSLOOP_300MS;

					if((wheel_speed_min-ldvrefs16VehicleSpeed)>ldvrefs16SpinSetDvThr)
					{
						ldvrefu1SatisfyDvCondition=1;
					}
					if(ldvrefu1SatisfyDvCondition==1)
					{
						if(ldvrefs16CalVrefVariation>=ldvrefs16SpinSetVrefDiffThr1)
						{
							ldvrefu8CntVehAccelSpinOnBrk++;
						}
						else if((ldvrefs16CalVrefVariation<ldvrefs16SpinSetVrefDiffThr2)&&(ldvrefu8CntVehAccelSpinOnBrk>0))
						{
							ldvrefu8CntVehAccelSpinOnBrk--;
						}
						else
						{
							;
						}

						if(ldvrefu8CntVehAccelSpinOnBrk>ldvrefs16SpinSetTime)
						{
							ldvrefu1DctVehAccelSpinOnBrk=1;
							ldvrefu8CntVehAccelSpinOnBrk=0;
						}
					}
					else
					{
						ldvrefu8CntVehAccelSpinOnBrk=0;
					}
				#if	__ENGINE_INFORMATION
				}
				#endif
			}
			//Reset	Condition
			else
			{
				#if	__ENGINE_INFORMATION
				if(ldabsu1CanErrFlg==0)
				{
					ldvrefs16SpinResetDvThr=VREF_2_KPH_RESOL_CHANGE;
					ldvrefs16SpinResetEngTorqThr=TORQ_PERCENT_20PRO;
					ldvrefs16SpinResetMtpThr=MTP_10_P;
					ldvrefs16SpinResetTime=L_U8_TIME_10MSLOOP_150MS;
					if((mtp<ldvrefs16SpinResetMtpThr)&&(eng_torq_rel<ldvrefs16SpinResetEngTorqThr))
					{
						ldvrefu1SatisfyDvCondition=1;
					}

					if((wheel_speed_max-ldvrefs16VehicleSpeed)<ldvrefs16SpinResetDvThr)
					{
						ldvrefu1SatisfyDvCondition=1;
					}
					if((ldvrefu1SatisfyEngCondition==1)&&(ldvrefu1SatisfyDvCondition==1))
					{
						ldvrefu8CntVehAccelSpinOnBrk++;

						if(ldvrefu8CntVehAccelSpinOnBrk>ldvrefs16SpinResetTime)
						{
							ldvrefu1DctVehAccelSpinOnBrk=0;
							ldvrefu8CntVehAccelSpinOnBrk=0;
						}
					}
					else
					{
						ldvrefu8CntVehAccelSpinOnBrk=0;
					}
				}
				else
				{
				#endif

					ldvrefs16SpinResetDvThr=VREF_3_KPH_RESOL_CHANGE;
					ldvrefs16SpinResetTime=L_U8_TIME_10MSLOOP_150MS;
					if((wheel_speed_max-ldvrefs16VehicleSpeed)<ldvrefs16SpinResetDvThr)
					{
						ldvrefu1SatisfyDvCondition=1;
					}
					if(ldvrefu1SatisfyDvCondition==1)
					{
						ldvrefu8CntVehAccelSpinOnBrk++;

						if(ldvrefu8CntVehAccelSpinOnBrk>ldvrefs16SpinResetTime)
						{
							ldvrefu1DctVehAccelSpinOnBrk=0;
							ldvrefu8CntVehAccelSpinOnBrk=0;
						}
					}
					else
					{
						ldvrefu8CntVehAccelSpinOnBrk=0;
					}
				#if	__ENGINE_INFORMATION
				}
				#endif
			}
		}
		else
		{
			ldvrefu1DctVehAccelSpinOnBrk=0;
			ldvrefu8CntVehAccelSpinOnBrk=0;
		}

	#if	(__4WD_VARIANT_CODE==ENABLE)
	}
	else
	{
		ldvrefu1DctVehAccelSpinOnBrk=0;
		ldvrefu8CntVehAccelSpinOnBrk=0;
	}
	#endif
}
#endif
#endif
*/

#if __VDC
void LDESP_DetectBackd(void)
{
	#if (__R_GEAR_INFORM_FOR_MT ==1)
   	LDESP_ChkRgearSwitch();
	#endif
	if((AUTO_TM)&&(gs_pos!=0))
    {      /* 0 : P, 7 : R, 6 : N  */
    	#if __REAR_D
    	if((vrad_fl<VREF_7_KPH)&&(vrad_fr<VREF_7_KPH))
    	#else
        if((vrad_rl<VREF_7_KPH)&&(vrad_rr<VREF_7_KPH))
        #endif
        {    /* Creep Vibration*/
            if(BACK_DIR==0)
            {
                if(ldu1gear_R==1)
                {
                    BACK_DIR=1;
                    BACK_DIR_DECT = 1;
                }
            }
            else
            {
                if(ldu1gear_D==1)
                {
                    BACK_DIR=0;
                    BACK_DIR_DECT = 1;
                }
            }
			F_gr_det_cnt=0;
			R_gr_det_cnt=0;
        }
        else
        {
    		if(ldu1gear_R==1)
    		{
    			if(R_gr_det_cnt<L_U8_TIME_10MSLOOP_1500MS)
    			{
    				R_gr_det_cnt++;
    			}
    			if(F_gr_det_cnt>0)
    			{
    				F_gr_det_cnt--;
    			}
    		}
    		else if(ldu1gear_D==1)
    		{
    			if(R_gr_det_cnt>0)
    			{
    				R_gr_det_cnt--;
    			}
    			if(F_gr_det_cnt<L_U8_TIME_10MSLOOP_1500MS)
    			{
    				F_gr_det_cnt++;
    			}
    		}
    		else
    		{
    			;
    		}

    		if(BACK_DIR_DECT==0)
    		{
    			if(BACK_DIR==0)
    			{
	        		if((R_gr_det_cnt>L_U8_TIME_10MSLOOP_500MS)
		        	&&(F_gr_det_cnt<L_U8_TIME_10MSLOOP_100MS)
		        	&&((R_gr_det_cnt-F_gr_det_cnt)>L_U8_TIME_10MSLOOP_300MS)
		        	&&(ldu1gear_R==1))
	        		{
	                    BACK_DIR=1;
	                    BACK_DIR_DECT = 1;
	                    F_gr_det_cnt=0;
	        			R_gr_det_cnt=0;
	        		}
                    else if((F_gr_det_cnt>T_1000_MS)
	        		&&(R_gr_det_cnt<L_U8_TIME_10MSLOOP_100MS)
	        		&&((F_gr_det_cnt-R_gr_det_cnt)>L_U8_TIME_10MSLOOP_300MS)
	        		&&(ldu1gear_D==1))
        			{
	                    BACK_DIR=0;
	                    BACK_DIR_DECT = 1;
	                    F_gr_det_cnt=0;
	        			R_gr_det_cnt=0;
        			}
        			else
        			{
        				;
        			}
    			}
    			else
    			{
	        		if((F_gr_det_cnt>L_U8_TIME_10MSLOOP_500MS)
		        	&&(R_gr_det_cnt<L_U8_TIME_10MSLOOP_100MS)
		        	&&((F_gr_det_cnt-R_gr_det_cnt)>L_U8_TIME_10MSLOOP_300MS)
		        	&&(ldu1gear_D==1))
	        		{
	        		    BACK_DIR=0;
	                    BACK_DIR_DECT = 1;
	                    F_gr_det_cnt=0;
	        			R_gr_det_cnt=0;
	                }
    			}
            }
        }
    }

    else
    {
        model_backdetect_yawrate_old = model_backdetect_yawrate;

/*      tempW0 = (int16_t)( (((int32_t)KYAW.model_avr)*8)/128);      // Fc = 1 Hz
        tempW1 = (int16_t)( (((int32_t)model_backdetect_yawrate_old)*120)/128);
    	model_backdetect_yawrate = tempW0+tempW1;
            0.5HZ   0.299   125 3
            1.1Hz   0.149   122 6
            1.4Hz   0.112   120 8
*/
        /*
            KYAW.ALGO_MODEL[0] : Vehicle signal
            KYAW.ALGO_MODEL[1] : Feont wheel
            KYAW.ALGO_MODEL[2] : rear wheel
            KYAW.ALGO_MODEL[3] : ay
            KYAW.ALGO_MODEL[4] : wstr

            KYAW.M1_FRONT_OK
            KYAW.M2_REAR_OK
            KYAW.M3_LAT_YAW_STR
            KYAW.M4_LAT_YAW_STR
        */

/*Model Validation Condition : 2 deg/s < Model < yawrate,0.5 g*/
        if ( vrefk < 150 )
        {
            esp_tempW3 = (int16_t)((((int32_t)500)*2021)/150);
        }
        else
        {
            esp_tempW3 = (int16_t)((((int32_t)500)*2021)/vrefk);
        }

        if( (SAS_CHECK_OK==1) && (vrefk > 50) )
        {
            model_back_valid_count = 0;
            esp_tempW0 = rf;

        }
        else if ( ((MINI_SPARE_WHEEL_fl==1) || (MINI_SPARE_WHEEL_fr==1)) && (KYAW.mdl_ok_num >=3) )
        {
            model_back_valid_count = 0;
            esp_tempW0 = 0;
            if((KYAW.M2_REAR_OK==1) && (ESP_MINI_TIRE==0)
            	&&(KYAW.ALGO_MODEL[2] <= esp_tempW3)&& (KYAW.ALGO_MODEL[2] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[2];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }
            if ( (KYAW.M3_LAT_YAW_STR==1) && (KYAW.ALGO_MODEL[3] <= esp_tempW3)
            	&&(KYAW.ALGO_MODEL[3] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[3];
                model_back_valid_count++;
            }
            else
            {
            	;
            }
            if ((KYAW.M4_LAT_YAW_STR==1) && (KYAW.ALGO_MODEL[4] <= esp_tempW3)
            	&&(KYAW.ALGO_MODEL[4] >= (-esp_tempW3)) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[4];
                model_back_valid_count++;
            }
            else
            {
            	;
            }
            if ( model_back_valid_count == 0 )
            {
                esp_tempW0 = 0;
            }
            else
            {
                esp_tempW0 = (int16_t)((((int32_t)esp_tempW0)*1)/(int16_t)model_back_valid_count);
            }
        }
        else if ( ((MINI_SPARE_WHEEL_rl==1) || (MINI_SPARE_WHEEL_rr==1) || (ESP_MINI_TIRE==1))&&(KYAW.mdl_ok_num >=3) )
        {
            model_back_valid_count = 0;
            esp_tempW0 = 0;
            if ((KYAW.M1_FRONT_OK==1) && (!((MINI_SPARE_WHEEL_fl==1) || (MINI_SPARE_WHEEL_fr==1)))
            	&& (KYAW.ALGO_MODEL[1] <= esp_tempW3)&&(KYAW.ALGO_MODEL[1] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[1];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }
            if( (KYAW.M3_LAT_YAW_STR==1) && (KYAW.ALGO_MODEL[3] <= esp_tempW3)&&(KYAW.ALGO_MODEL[3] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[3];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }
            if((KYAW.M4_LAT_YAW_STR==1) && (KYAW.ALGO_MODEL[4] <= esp_tempW3)&&(KYAW.ALGO_MODEL[4] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[4];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }
            if ( model_back_valid_count == 0 )
            {
                esp_tempW0 = 0;
            }
            else
            {
                esp_tempW0 = (int16_t)((((int32_t)esp_tempW0)*1)/(int16_t)model_back_valid_count);
            }
          }
        else
        {
            model_back_valid_count = 0;
            esp_tempW0 = 0;
            if ( (KYAW.M1_FRONT_OK==1) && (!((MINI_SPARE_WHEEL_fl==1) ||( MINI_SPARE_WHEEL_fr==1)))
            	&& (KYAW.ALGO_MODEL[1] <= esp_tempW3)&&(KYAW.ALGO_MODEL[1] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[1];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }

            if ( (KYAW.M2_REAR_OK==1) && (ESP_MINI_TIRE==0)&& (KYAW.ALGO_MODEL[2] <= esp_tempW3)
            	&&(KYAW.ALGO_MODEL[2] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[2];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }

            if ((KYAW.M3_LAT_YAW_STR==1)&& (KYAW.ALGO_MODEL[3] <= esp_tempW3)&&(KYAW.ALGO_MODEL[3] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[3];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }

            if ((KYAW.M4_LAT_YAW_STR==1)&& (KYAW.ALGO_MODEL[4] <= esp_tempW3)&&(KYAW.ALGO_MODEL[4] >= -esp_tempW3) )
            {
                esp_tempW0 = esp_tempW0 + KYAW.ALGO_MODEL[4];
                model_back_valid_count=model_back_valid_count+1;
            }
            else
            {
            	;
            }
            if ( model_back_valid_count > 2 )
            {
                esp_tempW0 = KYAW.model_avr;
            }
            else if ( model_back_valid_count == 0 )
            {
            	esp_tempW0 = 0;
            }
            else
            {
                esp_tempW0 = (int16_t)((((int32_t)esp_tempW0)*1)/(int16_t)model_back_valid_count);
            }
          }

        model_backdetect_yawrate = LCESP_s16Lpf1Int( esp_tempW0, model_backdetect_yawrate_old, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);


        esp_tempW0 = (int16_t)((((int32_t)yaw_out)*model_backdetect_yawrate)/10000);

        esp_tempW1 = (int16_t)McrAbs(esp_tempW0);
        esp_tempW2 = (int16_t)McrAbs(yaw_out);
        esp_tempW4 = (int16_t)McrAbs(model_backdetect_yawrate);
        esp_tempW5 = (int16_t)McrAbs( esp_tempW2 - esp_tempW4 );      
        
        lds16espDetYawmYawcDiffOld =  lds16espDetYawmYawcDiff;
        lds16espDetYawmYawcDiff    =  esp_tempW5 ;
        esp_tempW6 = (lds16espDetYawmYawcDiff - lds16espDetYawmYawcDiffOld) ;
        
        /* det delyaw filter ------*/
        if(lds16espDetYawmYawcDiff > lds16espDetYawmYawcDiffFilt)
        {
        	lds16espDetYawmYawcDiffFilt = lds16espDetYawmYawcDiff;
        }
        else
        {
        	lds16espDetYawmYawcDiffFilt = lds16espDetYawmYawcDiffFilt + REVERSE_DRV_DEL_YAW_DIFF;
        	
        	if( lds16espDetYawmYawcDiff > lds16espDetYawmYawcDiffFilt)
        	{
        	   	lds16espDetYawmYawcDiffFilt = lds16espDetYawmYawcDiff ;
        	}
        	else
        	{
        		;
        	}
        	
        }        
        
        

#if __AX_SENSOR
		if(USE_ALONG==1)
		{
        if( (BACK_DIR_DECT==0) && (BACKWARD_MOVE==1) && (esp_tempW2 < YAW_7DEG))
        {
            BACK_DIR = 1;
        }
        else if ((BACK_DIR_DECT==0) && (BACKWARD_MOVE==0) && (esp_tempW2 < YAW_7DEG)
        	&& (STANDSTILL) && (di_along>=0)&&(EFFECTIVE_ALONG==1) )
        {
            BACK_DIR = 0;
        }
        else
        {
        	;
        }
		}
        if(/*(BACK_DIR_DECT==0)*/
/*            &&(WSTR_360_SUSPECT_FLG ==0) &&   */
            (KYAW.SUSPECT_FLG==0)
            &&(KLAT.SUSPECT_FLG==0)
            &&(((KYAW.M1_FRONT_OK==1)&&(KYAW.M2_REAR_OK==1))
	            ||(KYAW.mdl_ok_num >=3)
	            ||((SAS_CHECK_OK ==1)&&(ESP_SPLIT==0) &&(ABS_fz==0))))
	    
#else
        if(
            /*(BACK_DIR_DECT==0)*/
/*            &&(WSTR_360_SUSPECT_FLG==0)   */
            (KYAW.SUSPECT_FLG==0)
            &&(KLAT.SUSPECT_FLG==0)
            &&(((KYAW.M1_FRONT_OK==1)&&(KYAW.M2_REAR_OK==1))||(KYAW.M2_REAR_OK==1)||(KYAW.mdl_ok_num >=3)
               || ((SAS_CHECK_OK==1) &&(ESP_SPLIT==0) && (ABS_fz==0))))
        
#endif
        {
            if( (esp_tempW2 >= 200)
                && (esp_tempW2 <= esp_tempW3)
                && (esp_tempW4 >= 200)
                && (esp_tempW4 <= esp_tempW3)                
                && (McrAbs(Beta_MD)<=2500)
                && (lds16espDetYawmYawcDiffFilt <= REVERSE_DRV_CHK_YAWDIFF )  )
                /* && (vdc_vref <= VREF_70_KPH ) ) */
            {

                if(esp_tempW0>0)
                {
                    if( forward_dir_count < L_U8_TIME_10MSLOOP_1000MS )
                    {
                    	forward_dir_count=forward_dir_count+1;
                    }
                    else
                    {
                    	;
                    }
                    if( backward_dir_count > 0 )
                    {
                    	backward_dir_count=backward_dir_count-1;
                    }
                    else
                    {
                    	;
                    }
                    
                    if(   (esp_tempW2 >=REVERSE_DRV_RECHK_YAW) && (esp_tempW4 >=REVERSE_DRV_RECHK_YAW )  
                    	&&(lds16espDetYawmYawcDiffFilt <  REVERSE_DRV_RECHK_YAWDIFF  )  )
                    {
                        if( lds16espBackwardDirCNT > (-L_U16_TIME_10MSLOOP_5S) )
                        {
                        	lds16espBackwardDirCNT = lds16espBackwardDirCNT -1;
                        }
                        else
                        {
                        	;
                        } 
                    }
                    else
                    {
                    	;
                    }               
                    
                }
                else if (vdc_vref <= VREF_70_KPH )
                {
                    if( forward_dir_count > 0)
                    {
                    	forward_dir_count=forward_dir_count-1;
                    }
                    else
                    {
                    	;
                    }
                    if( backward_dir_count < L_U8_TIME_10MSLOOP_1000MS)
                    {
                    	backward_dir_count=backward_dir_count+1;
                    }
                    else
                    {
                    	;
                    }
                    
                    if(   (esp_tempW2 >=REVERSE_DRV_RECHK_YAW) && (esp_tempW4 >=REVERSE_DRV_RECHK_YAW )  
                    	&&(lds16espDetYawmYawcDiffFilt <  REVERSE_DRV_RECHK_YAWDIFF  ) )
                    {                    
                        if( lds16espBackwardDirCNT < L_U16_TIME_10MSLOOP_5S )
                        {
                        	lds16espBackwardDirCNT = lds16espBackwardDirCNT + 1;
                        }
                        else
                        {
                        	;
                        }
                    }
                    else
                    {
                    	;
                    }
                     
                }
                else{
                    ;
                }
                
            }
            else
            {
            		;
/*                 if( backward_dir_count > 0 ) backward_dir_count--;*/
/*                  if( forward_dir_count > 0) forward_dir_count--;*/
            }

        }
        else
        {
        	;
        }

        if( vdc_vref <= VREF_3_KPH )
        {
            BACK_DIR_RECHECK = 0;
            BACK_DIR = 0;
            BACK_DIR_DECT = 0;
            forward_dir_count = 0;
            backward_dir_count = 0;
			F_gr_det_cnt=0;
			R_gr_det_cnt=0;
			Reverse_Gear_cnt=0;			
			
			lds16espBackwardDirCNT =0;
        }
        else
        {
            if ( (BACK_DIR_RECHECK==0) && (SAS_CHECK_OK==1) )
            {
                BACK_DIR_RECHECK = 1;
                BACK_DIR_DECT = 0;
                forward_dir_count = 0;
                backward_dir_count = 0;
            }
            else if ((BACK_DIR_RECHECK==0) && ((MINI_SPARE_WHEEL_fl==1) ||( MINI_SPARE_WHEEL_fr==1)))
            {
                BACK_DIR_RECHECK = 1;
                BACK_DIR_DECT = 0;
                forward_dir_count = 0;
                backward_dir_count = 0;
            }
            else if ( (BACK_DIR_RECHECK==0) && (ESP_MINI_TIRE==1) )
            {
                BACK_DIR_RECHECK = 1;
                BACK_DIR_DECT = 0;
                forward_dir_count = 0;
                backward_dir_count = 0;
            }
			else
			{
				;
			}
			#if (__R_GEAR_INFORM_FOR_MT==1)
            if(AUTO_TM==0){
                if(ldu1RgearSigFail==0){
                    if(fu1GearR_Switch==1){
                        if(Reverse_Gear_cnt<250){
                            Reverse_Gear_cnt++;
                        }
                    }
                    else if(Reverse_Gear_cnt>0){
                        Reverse_Gear_cnt--;
                    }
                    else{
                        ;
                    }
                }
                else
                {
                	Reverse_Gear_cnt=0;/*added logic*/
                }
            }
            else{
            #endif	
                Reverse_Gear_cnt=0;
            #if (__R_GEAR_INFORM_FOR_MT==1)    
            }
            #endif     
            
            /*    
            if( (BACK_DIR==1) && (BACK_DIR_RECHECK==1) && (vdc_vref >= VREF_50_KPH )
                && ( (forward_dir_count >= (backward_dir_count+L_U8_TIME_10MSLOOP_100MS))
                      || ((forward_dir_count<5) && (backward_dir_count<5)) ) )
            {
                BACK_DIR = 0;
			}
			else
			{
				;
			}
			*/
			
            esp_tempW5 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, L_U8_TIME_10MSLOOP_250MS,
                                          lsesps16MuHigh, L_U8_TIME_10MSLOOP_150MS);

			if (BACK_DIR_DECT==0){
	            if( (forward_dir_count >= L_U8_TIME_10MSLOOP_500MS)&&(SAS_CHECK_OK==1)
	                &&(forward_dir_count>backward_dir_count))
	            {
	                BACK_DIR = 0;
	/*                BACKWARD_MOVE = 0;      */
	                BACK_DIR_DECT = 1;
	                backward_dir_count=0;
	                forward_dir_count=0;                
	            }
	            else if((forward_dir_count >= esp_tempW5)
	                &&(forward_dir_count>backward_dir_count))
	            {
	            	#if (__R_GEAR_INFORM_FOR_MT==1) 
	                if(AUTO_TM==0){
	                    if(((ldu1RgearSigFail==0)&&(Reverse_Gear_cnt==0))||(ldu1RgearSigFail==1)){
	                        BACK_DIR = 0;
	        /*                BACKWARD_MOVE = 0;      */
	                        BACK_DIR_DECT = 0;
	                    }
	                }
	                else{
	                #endif	
	                    BACK_DIR = 0;
	    /*                BACKWARD_MOVE = 0;      */
	                    BACK_DIR_DECT = 0;     
	                #if (__R_GEAR_INFORM_FOR_MT==1)                    
	                }
	                #endif
	            }
	            else if( (backward_dir_count >= L_U8_TIME_10MSLOOP_500MS)&&(SAS_CHECK_OK==1)
	               &&(backward_dir_count > forward_dir_count))
	            {
	                BACK_DIR = 1;
	/*                BACKWARD_MOVE = 1;      */
	                BACK_DIR_DECT = 1;
	                backward_dir_count=0;
	                forward_dir_count=0;
	            }
	            else if( (backward_dir_count >= esp_tempW5)
	               &&(backward_dir_count > forward_dir_count))
	            {
	            	#if (__R_GEAR_INFORM_FOR_MT==1)  
	                if(AUTO_TM==0){
	                    if(((ldu1RgearSigFail==0)&&(Reverse_Gear_cnt>=L_U8_TIME_10MSLOOP_150MS))||(ldu1RgearSigFail==1)){
	                        BACK_DIR = 1;
	        /*                BACKWARD_MOVE = 1;      */
	                        BACK_DIR_DECT = 0;
	                    }
	                    else
	                    {
							;
	                    }
	                }
	                else{
	                #endif	
	                    BACK_DIR = 1;
	    /*                BACKWARD_MOVE = 1;      */
	                    BACK_DIR_DECT = 0;
	                #if (__R_GEAR_INFORM_FOR_MT==1)     
	                }
	                #endif 
	            }
	           	#if (__R_GEAR_INFORM_FOR_MT==1)  
	            else if((forward_dir_count<5)&&(BACK_DIR_DECT==0)&&(AUTO_TM==0)){
	                if((ldu1RgearSigFail==0)&&(Reverse_Gear_cnt>=L_U8_TIME_10MSLOOP_500MS))
					{
	                    BACK_DIR = 1;
	                }
	                else
	                {
	                	;
	                }
	            }   
	            #endif
	            else{
	            	;
	            }
        	}
        	else{
            /*Add Reset*/
            /*
	            if((BACK_DIR==0)&&(backward_dir_count >= T_750MS)&&(SAS_CHECK_OK==1)&&(forward_dir_count<5)){
	                BACK_DIR_DECT = 0;
	                BACK_DIR = 1;
	                backward_dir_count=0;
	                forward_dir_count=0;                   
	            }
	            else if( (BACK_DIR==1)&&(forward_dir_count >= T_750MS)&&(SAS_CHECK_OK==1)&&(backward_dir_count<5)){
	                BACK_DIR_DECT = 0;
	                BACK_DIR = 0;
	                backward_dir_count=0;
	                forward_dir_count=0;                    
	            }  
	            else
	            {
	            	;
	            }
              */

           	    if(BACK_DIR==1)
                {/*-- Backward to Forward driving change condition --*/
                	
                	#if (__R_GEAR_INFORM_FOR_MT==1)
                    if( (lds16espBackwardDirCNT  < (-REVERSE_DRV_RECHK_TIME) ) &&  
                    	 ( (ldu1RgearSigFail==1) ||  ( (ldu1RgearSigFail==0) && (fu1GearR_Switch == 0 ) ) ) )           		
                	#else
                	if( (lds16espBackwardDirCNT  < (-REVERSE_DRV_RECHK_TIME) )  )
                	#endif	
                	{
                		BACK_DIR = 0;
                		
                		lds16espBackFlags =90;
                	}            			
                	else if( (vdc_vref >= VREF_70_KPH ) &&  (lds16espBackwardDirCNT  < (-REVERSE_DRV_RECHK_TIME2) )  )
                	{
                		BACK_DIR = 0;
                		
                		lds16espBackFlags =95;
                	}
                	else
                	{
                		lds16espBackFlags =97;
                	}            		
                }
                else
                {/*-- Forward to Backward driving change condition --*/
         	    
                	#if (__R_GEAR_INFORM_FOR_MT==1)
                    if(Reverse_Gear_cnt >= L_U8_TIME_10MSLOOP_500MS )
                    {
                    	BACK_DIR      = 1 ;          
                    	
                    	lds16espBackFlags =100;           	
                    }            		
                	#else
                	if(0)
                	{ 
                		; 
                	}            			
                	#endif
                	/*
                	else if(ldu1espForwardDrvSpeedChk == 1)
                	{
                		lds16espBackFlags =110;                                 
                	}
                	*/
                	else if( lds16espBackwardDirCNT  > REVERSE_DRV_RECHK_TIME )
                	{
                		BACK_DIR      = 1 ; 
                		lds16espBackFlags =120;
                	}
                	else
                	{
                		;
                	}               		            		            		
                }
            
            }
        	
        


   #if __CAR==FORD_C214

        if(gear_pos==14) {
            BACK_DIR = 1;
/*                BACKWARD_MOVE = 1;      */
            BACK_DIR_DECT = 1;
        } else {
            BACK_DIR = 0;
/*                BACKWARD_MOVE = 0;      */
            BACK_DIR_DECT = 1;
        }

	#endif

   #if __CAR==GM_C100

	if(lespu8_TrnsShftLvrPos==2)
	{
   		BACK_DIR = 1;
/*                BACKWARD_MOVE = 1;      */
          BACK_DIR_DECT = 1;
	}
	else
	{
		BACK_DIR = 0;
/*                BACKWARD_MOVE = 0;      */
          BACK_DIR_DECT = 1;
	}

   #endif


    }
    }
}
#if (__R_GEAR_INFORM_FOR_MT==1)
void LDESP_ChkRgearSwitch(void)
{
	#if (__GEAR_SWITCH_TYPE==ANALOG_TYPE)
	if (fsu1GearRSwitch_error_flg==1)
	{
	    ldu1RgearSigFail=1;
	}
	else
	{
		ldu1RgearSigFail=0;
	}
	#elif (__GEAR_SWITCH_TYPE==CAN_TYPE)
	if((fu1MainCanLineErrDet==0)&&(fu1MainCanSusDet==0))
	{
		if (ccu1GearRSwitchInv==1) 
		{
			if (ldu8RgearErrorCnt < U8_RGEAR_ERROR_CHK_TIME) 
			{
				ldu8RgearErrorCnt = ldu8RgearErrorCnt + 1;
			}
			else
			{
				ldu8RgearErrorCnt = U8_RGEAR_ERROR_CHK_TIME;
				ldu1RgearSigFail=1;
			}
		}
		else /* Reverse gear signal valid */
		{
			if (ldu8RgearErrorCnt>0) 
			{
				ldu8RgearErrorCnt = ldu8RgearErrorCnt - 1;
			}
			else
			{
				ldu8RgearErrorCnt = 0;
				ldu1RgearSigFail=0;
			}
		}
	}
	else
	{
		ldu1RgearSigFail=1;
	}	  
	#else
	ldu1RgearSigFail=1;
	#endif
}
#endif /*__R_GEAR_INFORM_FOR_MT==1*/
#endif /*__VDC*/

#if (__PRESS_DECEL_VREF_APPLY == ENABLE)
void LDABS_vMuChangeDetection(void)
{
    ldu8absmustateold = ldu8absmustate;
    
	if(FZ1.voptfz_resol_change <= VREF_5_KPH_RESOL_CHANGE)
	{
   		WFactor1=25;
	}
	else
	{
		if((FL.LOW_MU_SUSPECT_by_IndexMu == 1)&&(FR.LOW_MU_SUSPECT_by_IndexMu == 1))
		{
			WFactor1=10;
		}
		else if(LOW_to_HIGH_suspect == 1)
		{
			WFactor1=25;										
		}
		else if(ldu1absl2hdetectionflag == 1)
		{
			WFactor1=25;	
		}
		else
		{
			WFactor1=15;					
		}
	}
	
	lds16FiloutDecelG_1 = (int16_t)(((int32_t)FZ1.filter_out * WFactor1)/100);
	
    tempW0 = (lds16FiloutDecelG_1  -  Vehicle_Decel_EST_filter);
    
    if((ABS_fz == 1)&&(vref_resol_change > VREF_5_KPH_RESOL_CHANGE)){
        if((Vehicle_Decel_EST_filter < 0 )&&(Vehicle_Decel_EST_filter >= -AFZ_0G3))
        {
            lds8lowmudetectioncnt = lds8lowmudetectioncnt + 1;
            lds8midmudetectioncnt = lds8midmudetectioncnt - 1;
            lds8highmudetectioncnt = lds8highmudetectioncnt - 1;
        }
        else if ((Vehicle_Decel_EST_filter < -AFZ_0G3)&&(Vehicle_Decel_EST_filter > -AFZ_0G55)){
            lds8lowmudetectioncnt = lds8lowmudetectioncnt - 1;
            lds8midmudetectioncnt = lds8midmudetectioncnt + 1;
            lds8highmudetectioncnt = lds8highmudetectioncnt - 1;    
        }
        else if(Vehicle_Decel_EST_filter < -AFZ_0G55)
        {
            lds8lowmudetectioncnt = lds8lowmudetectioncnt - 1;
            lds8midmudetectioncnt = lds8midmudetectioncnt - 1;
            lds8highmudetectioncnt = lds8highmudetectioncnt + 1;    
        }
        else
        {
        	;
        }
        /*count mix max limitation*/
        lds8lowmudetectioncnt  = (int8_t)LCABS_s16LimitMinMax((int16_t)lds8lowmudetectioncnt,0,127); 
        lds8midmudetectioncnt  = (int8_t)LCABS_s16LimitMinMax((int16_t)lds8midmudetectioncnt,0,127); 
        lds8highmudetectioncnt = (int8_t)LCABS_s16LimitMinMax((int16_t)lds8highmudetectioncnt,0,127);
        /*detect mu state ( low , mid , high )*/
        if(lds8lowmudetectioncnt > L_U8_TIME_10MSLOOP_50MS){
            /*vrefflags01 = 1;*/
            ldu8absmustate = 1;
            lds8lowmudetectioncnt = 0;
            lds8midmudetectioncnt = 0;
            lds8highmudetectioncnt = 0;
        }
        else if(lds8midmudetectioncnt > L_U8_TIME_10MSLOOP_50MS){
            /*vrefflags01 = 2;*/
            ldu8absmustate = 2;
            lds8lowmudetectioncnt = 0;
            lds8midmudetectioncnt = 0;
            lds8highmudetectioncnt = 0;
        }
        else if(lds8highmudetectioncnt > L_U8_TIME_10MSLOOP_50MS){
            /*vrefflags01 = 3;*/
            ldu8absmustate = 3;
            lds8lowmudetectioncnt = 0;
            lds8midmudetectioncnt = 0;
            lds8highmudetectioncnt = 0;
        }
        else
        {
            ; /*vrefflags01 = 4;*/
        }
        /*filter respo.*/
        if(ldu1absl2hFilterRespon == 0)
        {
            if(tempW0 >= AFZ_0G1)
            {
                 ldu8absl2hFilterResDctcnt = ldu8absl2hFilterResDctcnt + 1;
            }
            else
            {
                ldu8absl2hFilterResDctcnt = 0;
            }
            if(ldu8absl2hFilterResDctcnt > L_U8_TIME_10MSLOOP_50MS)
            {
                ldu1absl2hFilterRespon = 1;
                ldu8absl2hFilterResDctcnt = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            if(tempW0 < AFZ_0G05)
            {
                ldu8absl2hFilterResclrcnt = ldu8absl2hFilterResclrcnt + 1;
            }
            else
            {
                ldu8absl2hFilterResclrcnt = 0;
            }
            if(ldu8absl2hFilterResclrcnt > L_U8_TIME_10MSLOOP_50MS)
            {
                ldu1absl2hFilterRespon = 0;
                ldu8absl2hFilterResclrcnt = 0;
            }
            else
            {
                ;
            }
        }
        if(ldu1absl2hdetectionflag == 0){
            /*
            if((ldu8absmustateold == 0)&&(ldu8absmustate == 1)&&(tempW0 >= AFZ_0G2)){
                ldu1absl2hdetectionflag = 1;
            }
            */
            if((ldu8absmustateold == 2)&&(ldu8absmustate == 3)&&(ldu1absl2hFilterRespon == 1)){
                ldu1absl2hdetectionflag = 1;
            }
            else if((ldu8absmustateold == 1)&&(ldu8absmustate == 3)&&(ldu1absl2hFilterRespon == 1)){
                ldu1absl2hdetectionflag = 1;
            }
            else
            {
                ;
            }       
        }
        else{
            ldu8muchangeholdtimer = ldu8muchangeholdtimer + 1 ;
			if(ldu8muchangeholdtimer > L_U8_TIME_10MSLOOP_500MS){
                ldu1absl2hdetectionflag = 0 ;
            }
            else{}
        }
    }
	else
	{
        lds8lowmudetectioncnt = 0; /*초기화*/
        lds8midmudetectioncnt = 0; 
        lds8highmudetectioncnt = 0; 
        ldu8muchangeholdtimer = 0;
        ldu1absl2hdetectionflag = 0;
        ldu8absmustateold = 0;
        ldu8absmustate = 0;
        ldu1absl2hFilterRespon = 0;
        ldu8absl2hFilterResDctcnt = 0;
        ldu8absl2hFilterResclrcnt = 0;
    }
}
#endif




#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallDctForVref
	#include "Mdyn_autosar.h"
#endif
