#ifndef __ALGO_VAR_H_
#define __ALGO_VAR_H_

#include "Abc_Ctrl.h"

#if __VDC
#include "v_struct.h"
#endif

#define     IN_SIM      0      /* set to 1 if simulation will be done */

extern struct W_STRUCT *WL,*WL_Axle,*WL_Side,*WL_Diag, FL, FR, RL, RR;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
extern struct ESC_STRUCT *WL_ESC, FL_ESC, FR_ESC, RL_ESC, RR_ESC;
extern struct ESC_CIRCUIT_STRUCT *CIR_ESC, PC_ESC, SC_ESC;
extern struct ESC_STRUCT_COMB *WL_ESC_COMB, FL_ESC_COMB, FR_ESC_COMB, RL_ESC_COMB, RR_ESC_COMB;
#endif

#if	defined (BTCS_TCMF_CONTROL) /* BTCS_TCMF_CONTROL */
extern struct TCS_WL_STRUCT *WL_TCS, FL_TCS, FR_TCS, RL_TCS, RR_TCS;
extern struct TCS_AXLE_STRUCT *AXLE_TCS, FA_TCS, RA_TCS;
extern struct TCS_CIRCUIT_STRUCT *CIR_TCS, PC_TCS, SC_TCS;
#endif

extern struct V_STRUCT *FZ, FZ1, FZ2;
#if __SIDE_VREF
extern struct V_STRUCT  FZ3, FZ4;
#endif

#if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) 
extern struct V_STRUCT		FZ_for_4WD;
#endif

#if (__VDC && __MGH40_ESP_BEND_DETECT)
extern struct SIDE_STRUCT  *DIRECTION, LEFT, RIGHT;
#endif

extern U8_BIT_STRUCT_t VF0;
extern U8_BIT_STRUCT_t VF1;
extern U8_BIT_STRUCT_t VF2;
extern U8_BIT_STRUCT_t VF3;
extern U8_BIT_STRUCT_t VF4;
extern U8_BIT_STRUCT_t VF5;
extern U8_BIT_STRUCT_t VF6;
extern U8_BIT_STRUCT_t VF7;
extern U8_BIT_STRUCT_t VF8;
extern U8_BIT_STRUCT_t VF9;
extern U8_BIT_STRUCT_t VF10;
extern U8_BIT_STRUCT_t VF11;     //2002.04.09 for LFC
extern U8_BIT_STRUCT_t VF12;     //2002.11.22 for LFC
extern U8_BIT_STRUCT_t VF13;     //2003.07.02 for LFC
extern U8_BIT_STRUCT_t VF14;     //2003.09.23 for LFC
extern U8_BIT_STRUCT_t VF15;     // 2004.02.25. for EDC
extern U8_BIT_STRUCT_t VF16;	//2007.04.30 for EDC
extern U8_BIT_STRUCT_t VF17;	//2007.04.30 for EDC


//#if __MGH_40_VREF
//extern U8_BIT_STRUCT_t VF20;
//extern U8_BIT_STRUCT_t VF25;
//extern U8_BIT_STRUCT_t VF26;
//#endif

extern U8_BIT_STRUCT_t VF21;

#if	__TCMF_CONTROL
extern U8_BIT_STRUCT_t VF19;
extern U8_BIT_STRUCT_t VF22;
#endif

//#if __SIDE_VREF
//extern U8_BIT_STRUCT_t VF23;
//#endif

extern U8_BIT_STRUCT_t VF24;

//#if __VREF_LIMIT_1
//extern U8_BIT_STRUCT_t VF27;
//#endif

//#if __Ax_OFFSET_MON
//extern U8_BIT_STRUCT_t VF28;
//extern U8_BIT_STRUCT_t VF29;
//#endif

//#if __VREF_LIMIT_2
//extern U8_BIT_STRUCT_t VF30;
//#endif

/*Vref New Bit struct*/
extern U8_BIT_STRUCT_t LSABSF0;
#if __Ax_OFFSET_MON
extern U8_BIT_STRUCT_t AxOffsetF0;
#endif

extern U8_BIT_STRUCT_t    VDCTF0;
#if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
extern U8_BIT_STRUCT_t    VDCTF1;
#endif

#if	__AX_SENSOR
extern U8_BIT_STRUCT_t    VDCTF2;
#endif
#if __VDC
extern U8_BIT_STRUCT_t    VDCTF3;
#endif
extern U8_BIT_STRUCT_t AFZF0;
#if __SIDE_VREF && __VREF_AUX
extern struct	U8_BIT_STRUCT_t VAUXF0;
#endif

extern 	U8_BIT_STRUCT_t VESPF0;
extern 	U8_BIT_STRUCT_t VREFMF0;


extern U8_BIT_STRUCT_t VREF_F0;



extern 	U8_BIT_STRUCT_t VREF_F1;



extern 	U8_BIT_STRUCT_t VREF_F2;


//  extern uint16_t cur_cycle_time, max_cycle_time; // For checking ABS cycle time at 03NZ winter

    extern uint8_t lsu8DrvMode;  /* for compile */

    extern int16_t   afz_alt;
    extern uint16_t speed_calc_timer;
//  extern uint8_t abs_begin_timer;
    extern uint8_t inst_zeit;
    extern uint8_t bls_k_timer;
    extern uint8_t bls_filter_timer;
//    extern uint8_t motor_run_on_timer;

    extern int16_t   vref;
    extern int16_t   vref2;
    #if __VREF_AUX
    extern int16_t   vref_aux_resol_change;    /* 051206 KSW */
    extern int16_t   vref_aux_resol_change_old;/* 051206 KSW */
    extern int16_t   vref_aux;    /* 0601 KSW */
    #endif

    #if __VREF_LIMIT_1
    extern int16_t   CNT_LM1;
    extern int16_t   WFactor;
    extern uint8_t CNT_L2H;
    #endif

    #if __VREF_LIMIT_2
    extern int16_t   CNT_LM2;
	extern int16_t  Estimated_Press_ABS_FL;
	extern int16_t  Estimated_Press_ABS_FR;
	extern int16_t  Estimated_Press_ABS_RL;
	extern int16_t  Estimated_Press_ABS_RR;
	extern int16_t  Estimated_Press_ABS_MP;
	extern int16_t  VrefPressDecelG;
    #endif


    extern int16_t  afz;

    extern int16_t   voptfz_new;
    extern int16_t   v_afz;
//    extern int16_t  v_afz_old, v_afz_old2;
    extern int16_t   afz_temp;
//    extern uint16_t  t_afz, t_afz_old, t_afz_old2;
    extern int16_t   abmittel_fz;
//    extern int16_t   Vehicle_Decel_EST;  //0509 KSW
//    extern int16_t   Vehicle_Decel_EST_filter; //051020 KSW
//    extern int16_t   Vehicle_Decel_Max;//051020 KSW
//    extern int8_t  v_afz_time;
//    extern uint8_t afz_flags;
//    extern uint8_t AFZ_SUS_CNT;

    extern int16_t   ax_Filter_temp;        //0601 China Check
    extern int16_t   ax_Filter;


	extern int8_t  arad_select_wheel;     //0606

//    extern uint8_t reserve_log;
    extern uint8_t ebd_filt_count;
  #if __MGH40_EBD
//    extern int8_t  slip_thres;
//    extern int8_t  arad_thres;
//    extern int8_t front_rear_speed_percent;
//    extern int16_t decel_thres;

  #else
//    extern uint8_t s_diff_pos_timer;
  #endif


    extern uint8_t avr_grv_counter;

//    extern uint8_t grv_diff_cnt;
    extern int16_t vrad_diff_max;
//    extern uint8_t vref_fail_mode_cnt;
    extern int16_t   ebd_refilt_grv;


    extern int16_t vref_input_inc_limit;
    extern int16_t vref_input_dec_limit;
    extern int16_t vref_output_inc_limit;
    extern int16_t vref_output_dec_limit;
    extern int16_t vref_limit_inc_counter;
    extern int16_t vref_limit_dec_counter_frt;
    extern int16_t vref_limit_dec_counter_rr;
    extern uint8_t Unstabil_counter_fl;   // 2006.01.10. for VREF_LIMITTER
    extern uint8_t Unstabil_counter_fr;

   	#if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) 
//	extern int16_t vdiff_by_along_1;
//	extern int16_t	vdiff_by_along_2;
//	extern int16_t	vdiff_by_along_3;
//	extern int16_t	vdiff_by_along_4;
//	extern int16_t	vdiff_by_along_5;
//	extern int16_t	vdiff_by_along_6;
//	extern int16_t	vdiff_by_along_mod;
//	extern int16_t	vref_along_1;
//	extern int16_t	vref_along_2;
//	extern int16_t	vref_along_3;
//	extern int16_t	vref_along_4;
//	extern int16_t	vref_along_5;
//	extern int16_t	vref_along_6;
//	extern int16_t vref_by_along;
//	extern int16_t vdiff_by_along;
//	extern int8_t accel_det_flags; // 임시
//	extern uint8_t	accel_spin_det_cnt1;
//	extern uint8_t	accel_spin_det_cnt2;
//	extern uint8_t	accel_spin_off_timer;
//	extern uint8_t	accel_spin_on_timer;
//	extern int16_t		along_vref_sum;
//	extern uint8_t	long_spin_cnt;
//	extern uint8_t 	vrad_diff_max_cnt;
	#endif



    #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
    extern int16_t vref_inc_limit_by_g;
    extern int16_t vref_dec_limit_by_g;
    extern int8_t g_sensor_limit_flags;
    #endif
//  extern int8_t arad_timer;
//  extern int8_t arad_avg_counter;


//  extern int16_t vrad_crt_max;
//  extern int16_t vrad_crt_min;
//  extern int16_t vrad_crt_max_for_vref_est;
//  #if _VRSELECT_ADVANCE
//  extern int16_t vrselect_new1;
//  extern int16_t vrselect_new2;
//  #endif
  	#if __EDC
//  extern uint16_t	eng_rpm_old;
//  extern uint16_t tc_rpm;
//  extern uint16_t tc_rpm_old;
//  extern int16_t diff_eng_tc_rpm;
//  extern int8_t engine_state;
  	#endif

  extern int16_t	vref_limit_by_along_mod;



  #if __WL_SPEED_RESOL_CHANGE
  extern int16_t vrad_diff_max_resol_change;
  extern int16_t vref_resol_change;
  extern int16_t vref_alt_resol_change;
     #if	__SIDE_VREF
//    extern int16_t   afz_ok_sum;                 // Side Wheel Reference 04.11.10
//    extern int8_t   inc_limit_f;                // Side Wheel Reference 04.11.10
//    extern int8_t   inc_limit_n;                // Side Wheel Reference 04.11.10
//    extern int8_t   dec_limit_f;                // Side Wheel Reference 04.11.10
//    extern int8_t   dec_limit_n;                // Side Wheel Reference 04.11.10
    extern int16_t    vref_L;                     // Side Wheel Reference 04.11.11
    extern int16_t    vref_L_old;
    extern int16_t    vref_L_diff;
    extern int16_t    vref_R;                     // Side Wheel Reference 04.11.11
    extern int16_t    vref_R_old;
    extern int16_t    vref_R_diff;
    extern int16_t	  voptfz_new_side;				 // Side Wheel Reference 04.12.01
    extern int16_t   voptfz_new3;                // Side Wheel Reference 04.11.11
    extern int16_t   voptfz_new4;                // Side Wheel Reference 04.11.11
//    extern int16_t   voptfz_out;                 // Side Wheel Reference 04.11.11
//    extern int16_t   voptfz_in;                  // Side Wheel Reference 04.11.11
//    extern int16_t   voptfz_out_1;               // Side Wheel Reference 04.11.11 (n-1) term
//    extern int16_t   voptfz_in_1;                // Side Wheel Reference 04.11.11 (n-1) term
//    extern int8_t   inside_alimit_unl;          // Side Wheel Reference 04.11.11
//    extern int8_t   inside_alimit_lim;          // Side Wheel Reference 04.11.11
//    extern int8_t   inside_alimit_inc;          // Side Wheel Reference 04.11.11
//    extern int8_t   inside_alimit_dec;          // Side Wheel Reference 04.11.11
    extern int16_t   voptfz_new_resol_change;    // Side Wheel Reference 04.11.19
    extern int16_t   dv_yaw_est;                 // Side Wheel Reference 05.05.31
    extern int16_t   dv_yaw_est_filt;            // Side Wheel Reference 05.05.31
    extern int16_t   dv_ay_est;				  // Side Wheel Reference 05.06.02
     #endif
  #endif

//       #if  __Ax_OFFSET_MON
//    extern uint16_t	AxOffCNT;
//	extern int16_t	AxOffset;
//	extern int16_t    AxOffsetCalc;
//	extern int16_t    AxOffsetCalc1;
//	extern int16_t    AxOffsetCalc2;
//	extern int16_t    AxOffsetCalc3;
//	extern int16_t	AxOffsetCalc1temp;
//	extern int16_t	AxOffsetCalc3temp;
//	ELONG   AxOffsetSUM;
//	ELONG   AxOffsetSUM2;
//     #endif

  #if __BEND_DETECT_using_SIDEVREF
//        extern int16_t Side_vref_diff;
  #endif

    extern int16_t   ebd_filt_grv;
    extern int16_t   suma;
    extern int16_t   sumb;
    extern int16_t   sumc;
    extern int16_t   sumd;
    extern int16_t   sume;
    extern int16_t   ebd_filt_avr_grv;
//    extern int16_t   sum_grv;
//    extern int16_t   ebd_avr_grv;
//    extern int16_t   ebd_grv_hold;
//    extern uint8_t grv_avr_counter;
//    extern uint8_t ebd_inc_count;
//    extern uint8_t spin_counter;
//    extern uint8_t ebd_motor_drv_time;
//    extern uint8_t brake_off_time;
//    extern uint8_t afz_cal_delay;
//    extern int8_t  bend_slip_comp;

//    extern int8_t  msl_rough_slip;		//2001. 1025 cjy
//    extern int8_t  msl_band_slip1;
//    extern int8_t  msl_band_slip2;

    extern int16_t   vref_alt;

    extern int16_t   vref3, vref4;
        #if __VDC
    extern int16_t   vref5,vref6;
    #if __WL_SPEED_RESOL_CHANGE
    extern int16_t   vref5_resol_change;
    #endif
    	#endif

	extern int16_t   along;

    #if ((__4WD||(__4WD_VARIANT_CODE==ENABLE)) || __AX_SENSOR)
//    extern uint8_t   count_ice;
//    extern int16_t   along_vref;
//    extern int16_t   along_ice_average;
//    extern int16_t   count_ice_up;
//    extern int16_t   along_ice_up;
//    extern int16_t   along_ice_average_up;

    extern int16_t   along_old;

//    extern int16_t   actual_g;

//    extern int16_t   along_standstill, di_along;
//    extern int16_t   bi_along_quo, bi_along_mod;
//    extern int16_t   g_vref;
//    extern int16_t   bi_vref, a_sen_weight;

//    extern int16_t   along_min;
//    extern int16_t   along_max;
//    extern int16_t   vref2_grv_min;
//    extern int16_t   vref2_grv_max;
//    extern int16_t   along_setted;
//
//    extern int16_t   along_filt;
//    extern int16_t   along_filt_diff[5];
//    extern int16_t   vref2_grv;
//    extern int16_t   vref2_grv_diff[5];
//    extern int16_t   g_offset;
//    extern int16_t   g_offset_diff[5];
//    extern int16_t   vref2_old;
//    extern int16_t   vref2_grv_diff1;
//    extern int16_t   vref2_grv_diff_old;
//    extern int16_t   acc_vref2;
//    extern int16_t   acc_var;
//    extern uint8_t  vref_fail_count;
//    extern uint8_t  vref_fail_count_afz;      //  050817 NZ 추가
//    extern uint8_t  vref_fail_dump_count_fl;  //  050806 NZ 추가
//    extern uint8_t  vref_fail_dump_count_fr;  //  050806 NZ 추가
//    extern uint8_t  vref_fail_dump_count_rl;  //  050806 NZ 추가
//    extern uint8_t  vref_fail_dump_count_rr;  //  050806 NZ 추가
//    extern uint16_t  control_fail_count;
//    extern uint8_t  standstill_count;
    extern uint8_t vref2_grv_count;

    extern int16_t   along_quo, along_mod;
 //   extern uint8_t vref3_count;
 //   extern uint8_t vref5_count;
 //   extern uint8_t scan_20_count;

    extern uint8_t along_fail,g_sensor_check_count,along_offset_count;
    #endif

#if __VDC && (__4WD||(__4WD_VARIANT_CODE==ENABLE)) 
//    extern int16_t   along_by_drift;
#endif

  #if __HSA
  	extern int16_t HSA_ax_sen, HSA_ax_sen_filt;
  #endif

  
#if (__VDC || __BTC || __ETC)  //BY HJH
    extern uint16_t  loop_time;
    extern uint16_t  motor_pwm_count;
    extern int16_t   press_test_count;
#endif

#if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F || __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
    		extern int16_t   esc_press_test_count;
#endif
    
/* #if __PWM */ /* 2006.06.15  K.G.Y */
//    extern uint8_t LFC_active_test_count;
//    extern uint8_t LFC_sol_repeat_period_timer;
//    extern uint8_t LFC_init_check_pass;
//    extern uint8_t LFC_init_ready_ok;
//    extern uint8_t check_lamp_for_LFC_fire;
//    extern uint16_t fl_no_current;
//	  extern uint16_t fr_no_current;
//    extern uint16_t rl_no_current;
//    extern uint16_t rr_no_current;

//    extern uint16_t fl_no_current_old;      //2001.12.03
//	extern uint16_t fr_no_current_old;
//   extern uint16_t rl_no_current_old;
//    extern uint16_t rr_no_current_old;
//    extern uint8_t LFC_sol_curr_cnt;
//    extern uint16_t init_sol_curr_fl;
//    extern uint16_t init_sol_curr_fr;
//    extern uint16_t init_sol_curr_rl;
//    extern uint16_t init_sol_curr_rr;
//    extern uint8_t fli_21ms_on_flg;
//    extern uint8_t fri_21ms_on_flg;
//    extern uint8_t rli_21ms_on_flg;
//    extern uint8_t rri_21ms_on_flg;
//    extern uint8_t sol_volt_comp_end_flg;
//    extern uint8_t sol_14ms_redrive_end;
    extern uint8_t LFC_mot_drv_cnt;
//    extern uint8_t LFC_mot_pwm_ok;
//    extern uint8_t fli_sol_curr_ok_flg;    //2001.12.03
//    extern uint8_t fri_sol_curr_ok_flg;
//    extern uint8_t rli_sol_curr_ok_flg;
//    extern uint8_t rri_sol_curr_ok_flg;

//	extern uint8_t LFC_fli_mon_ok_flg;       // 2001.12.03
//    extern uint8_t LFC_fli_mon_ok_old_flg;
//    extern uint8_t LFC_fri_mon_ok_flg;
//    extern uint8_t LFC_fri_mon_ok_old_flg;
//    extern uint8_t LFC_rli_mon_ok_flg;
//    extern uint8_t LFC_rli_mon_ok_old_flg;
//    extern uint8_t LFC_rri_mon_ok_flg;
//    extern uint8_t LFC_rri_mon_ok_old_flg;
//    extern uint16_t volt_diff;
//    extern uint8_t milli_sec_cnt;
//    extern uint8_t msc_7ms_control;

//	extern int8_t ign_comp_factor;            // 2002.01.28
//    extern uint16_t time_after_motor_off;
//    extern uint8_t ign_14V_above_timer;
//    extern uint8_t ign_13V4_above_timer;
//    extern uint8_t ign_13V4_below_timer;
//    extern uint8_t batt_level_indicater;
//    extern uint8_t thre_sec_pass_aft_mot_off;
//    extern uint8_t LFC_ign_chk_cnt;
//    extern uint8_t LFC_ign_ok;
//   extern uint16_t LFC_ign_mon_old1;
//    extern uint16_t LFC_ign_mon_old2;
//    extern uint16_t LFC_ign_mon_old3;
//    extern uint16_t LFC_ign_mon_old4;
//    extern uint16_t LFC_ign_avr;

    extern int16_t   LFC_mot_corr_factor;

//	extern int8_t	Ref_dump_temp;


// ********* added by y.w.shin at 2002.winter
//    extern uint8_t MSL_opt_mon_rl;
//    extern uint8_t MSL_opt_mon_rr;
//    extern uint8_t gma_dump_cnt_rl_for_LFC;
//    extern uint8_t gma_dump_cnt_rr_for_LFC;
// ********* added by y.w.shin at 2002.winter

//------------------------------------------------------------------
//  added by CSH 020709

//------------------------------------------------------------------
/*  extern uint8_t Rough_road_detector_counter;
    extern uint8_t Rough_road_detector_counter2;
    extern uint8_t scan_counter;     // Moved from a_struct.h by J.K.Lee at 03NZ winter
    extern uint8_t num; */

    extern uint8_t pwm_flg;				// From F/W V 4.24, F/S V ESP 2.8

    extern int16_t	aref0;
    extern int16_t	aref_avg;
    
//    extern int16_t	aref10;
//    extern int16_t	aref20;
//    extern int16_t	aref30;
//    extern int16_t	aref40;
//    extern int16_t	aref50;
//	extern uint8_t	aref_timer;
//	extern uint8_t	aref_avg_counter;
//	extern int16_t	vref0;

//	extern uint8_t	vref_increase_cnt;
//	extern int16_t	vref_trough;




  #if	__TCMF_CONTROL
//	extern uint16_t	TC_MFC_Voltage_Primary, TC_MFC_Voltage_Primary_dash;
//	extern uint16_t	TC_MFC_Voltage_Secondary, TC_MFC_Voltage_Secondary_dash;
	extern uint8_t	TCMF_MOTOR_OFF_cnt;
  #endif

/* #endif */ /* 2006.06.15  K.G.Y */
//******************** 여기까지 MGH-40, MGH-25 ESP 공통

/* #if __ESP_ECU && __PWM */ /* 2006.06.15  K.G.Y */
#if __ECU==ESP_ECU_1
//	extern int8_t delta_p_comp_left;
//  extern int8_t delta_p_comp_right;

//  extern uint8_t ms_int_ok;
//  extern uint16_t ms_otp_cnt;
//  extern uint16_t ms_otp_cnt1;

//  extern uint8_t L_timer_for_5_to_55bar;
//  extern int8_t master_press_comp_const;

//	extern uint8_t pwm_flg;
//  extern uint8_t pwm_cnt;
	extern uint8_t init_duty;
	extern int16_t ESP_MSC_ref_voltage;
//  extern uint8_t duty_ratio_inc;
//  extern uint8_t duty_ratio_dec;

#endif

//const	extern uint8_t pwm_duty_null;
   
#if __BTC
   /* btcs */
	extern uint8_t btcref_count;
    extern uint8_t mot_count;
/* NOT USED AFTER BTCS RESTRUCTURING 120207
    extern int16_t   sum_btc_slip;
    extern int16_t   sum_btc_slip_alt;
    extern int16_t   avr_btc_slip;
    extern int16_t   avr_slip_min;
    extern int16_t   avr_fspeed;
    extern int16_t   avr_fspeed_alt;
    extern uint8_t avr_count1;
    extern uint8_t avr_count2;
    extern uint8_t avr_count3;
    extern uint8_t vcount;
    extern int8_t  vrefdiff;
    extern int8_t  vrefdiff_alt1;
    extern int8_t  vrefdiff_alt2;
    extern int8_t  avr_vref;
    extern int8_t  avr_vref_alt;
 NOT USED AFTER BTCS RESTRUCTURING 120207 */
    extern uint8_t bls_count;
    extern uint8_t write_count;
    extern uint8_t coop_count;
    extern uint8_t esv_count_l;
    extern uint8_t esv_count_r;
    extern uint8_t esv_count_lpa;

    /*051005 New BTCS Structure*/
	extern uint8_t btcs_control_period;
#endif
   
 
#if __WL_SPEED_RESOL_CHANGE
	extern int32_t	tempL0;						extern int32_t	tempL1;
#endif

    extern int16_t   tempW0;                      extern int16_t   tempW1;
    extern int16_t   tempW2;                      extern int16_t   tempW3;
    extern int16_t   tempW4;                      extern int16_t   tempW5;
    extern int16_t   tempW6;                      extern int16_t   tempW7;
    extern int16_t   tempW8;                      extern int16_t   tempW9;

    extern int8_t  tempB0;                      extern int8_t  tempB1;
    extern int8_t  tempB2;                      extern int8_t  tempB3;
    extern int8_t  tempB4;                      extern int8_t  tempB5;
    extern int8_t  tempB6;                      extern int8_t  tempB7;
    extern uint8_t  tempC1;						extern uint8_t  tempC2;

    extern uint8_t  tempT1;						extern uint8_t  tempT2;

/////////////////////////////////////////////////////////MISRA//////////////
    extern uint8_t fsr_on_time;
    extern uint16_t tempUW0, tempUW1, tempUW2, tempUW3, tempUW4;
    extern uint8_t ee_btcs_data;
//    extern uint16_t weu16Eep_addr;
    extern uint8_t com_rx_buf[8];
	///////////////////////////////////////////////////////////////////////////




#endif
