/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LCABSDecideControlVariables.C
* Description: ABS control algorithm
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCABSDecideCtrlVariables
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCABSDecideCtrlVariables.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSStrokeRecovery.h"
#include "LDABSDctWheelStatus.h"
#include "LCABSCallControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"

#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCEPBCallControl.h"
#include "LDABSCallVrefEst.h"
#include "LCEBDCallControl.h"

#include "LDABSCallEstVehDecel.h"


#if __VDC
#include "LSESPFilterEspSensor.h"
#endif

/* Local Definiton  **********************************************************/


BRAKE_PEDAL_DETECTION_t BrkPedalModulationInfo;
/* Variables Definition*******************************************************/
U8_BIT_STRUCT_t ABSCTR01,ABSCTR02,ABSCTR03,ABSCTR04;

uint8_t   eng_brk_timer;
uint8_t   inside_abs_fz_count;
int8_t    LOW_to_HIGH_reset_cnt;
int8_t    LOW_to_HIGH_reset_cnt2;
uint8_t   L2H_sus_cnt_by_aref;
uint8_t   double_counter;
uint8_t   lcabsu8DoubleDctTimForEBD;

uint8_t   under_limit_speed_cnt;
/* #if __BTC_ABS_CLEAR */
uint8_t   btc_end_delay_cnt;
/* #endif */

#if __TEMP_for_CAS
uint8_t   NUM_OF_STABIL_WHEELS;
uint8_t   NUM_OF_STABIL_WHEELS_old;
#endif
uint8_t GMA_dumptime_at_H_to_Split;
uint8_t	lsabsu8L2HMode;

int8_t MP_release_cnt_bw_4bar;
int8_t MP_release_cnt_bw_6bar;
int8_t MP_release_cnt_bw_10bar;

int8_t lcabss8NoABSinRestWLCnt;
int8_t lcabss8AccelInputCnt,lcabss8VrefIncreaseCnt;
#if __LOWMU_REAR_1stCYCLE_RISE_DELAY
uint8_t lcabsu8LowMuTendencyGauge;
#endif

BRAKE_PEDAL_DETECTION_t BrkPedalModulationInfo;

/* LocalFunction prototype ***************************************************/

void LCABS_vDecideWheelCtrlVariable(void);
void LCABS_vDecideWheelCtrlVarWL(struct W_STRUCT *WL_temp);
void LCABS_vDecideWheelABSState(void);
void LCABS_vAssignABSReapplyTim(void);
void LCABS_vLimitABSReapplyTim(void);
void LCABS_vSetSTABLEVariables(void);
void LCABS_vSetStdDumpCounter(void);
void LCABS_vCountLFCCycle(void);
void LCABS_vSetWheelDecelGauge(void);
void LCABS_vDecideRoadWhVehStatus(void);
  #if __TEMP_for_CAS
void LCABS_vDecideCascadePropensity(void);
  #endif
  #if __LOWMU_REAR_1stCYCLE_RISE_DELAY
uint8_t LCABS_vCountLowMuWheelTendency(struct W_STRUCT *WLtemp, uint8_t lcabsu8LowMuTendencyGaugeOld);
  #endif
  #if __AQUA_ENABLE
void LCABS_vDctAQUALOCK(void);
  #endif
void LCABS_vDctLFCFictitiousCycle(void);
void LCABS_vDctLFCFictitiousCycleWL(struct W_STRUCT *WL);
void LCABS_vDctDoubleBraking(void);
  #if __VDC
void LCABS_vDctMpressRelease(void);
  #endif
  #if __RESONANCE
void LCABS_vDctResonanceWheel(void);
void LCABS_vDctResonanceWheelWL(struct W_STRUCT *WL);
  #endif
void LCABS_vDctH2LatHighSpeed(void);
void LCABS_vDctH2LatHighSpeedWL(struct  W_STRUCT *WL);
void LCABS_vDctLFCBigRise(void);
void LCABS_vDctLFCBigRiseWL(struct W_STRUCT *pF1, struct W_STRUCT *pF2);
void LCABS_vDctL2H(void);
void LCABS_vResetAllL2HSuspect2Flg(void);
void LCABS_vSetAllL2HSuspect2Flg(void);
void LCABS_vDctIngearVib(void);
void LCABS_vDctIngearVibWL(struct W_STRUCT *WL);
void LCABS_vDecideLFCCyclicComp(void);
void LCABS_vDecideLFCCyclicCompWL(struct W_STRUCT *WL_temp);
int16_t LCABS_s16SetLFCRiseRefscan(void);
void LCABS_vDisableCompForSmallCycle(void);
void LCABS_vDecideLFCSpecialComp(void);
void LCABS_vDecideLFCSpecialCompWL(struct W_STRUCT *WL_temp);
void LCABS_vDctInitialLowMuForFront(struct W_STRUCT *WL);
/* void LCABS_vDctInitialLowMuForRear(struct W_STRUCT *WL); */
void LCABS_vDctH2LForFront(void);
void LCABS_vDctH2LForRear(void);
void LCABS_vDctL2HFromFrontWheel(void);
void LCABS_vPreventBigRiseAndDump(void);
  #if __CHANGE_MU
void LCABS_vDctLFCMuChange(void);
void LCABS_vDctLFCMuChangeWL(struct W_STRUCT *WLf1,struct W_STRUCT *WLf2,struct W_STRUCT *WLr1,struct W_STRUCT *WLr2);
void LCABS_vCheckLFCBaseWheel(void);
void LCABS_vCheckLFCBaseWheelWL(struct W_STRUCT *WLr1,struct W_STRUCT *WLr2);
void LCABS_vCheckLFCBaseWheelInitial(void);
void LCABS_vCalcLFCRiseDump(struct W_STRUCT *WL);
  #endif
  #if __SPLIT_VERIFY
void LCABS_vVerifySplitByFrontHSide(struct W_STRUCT *WL);
#if __VDC && (__MGH60_CYCLE_TIME_OPT_ABS==DISABLE)
void LCABS_vVerifyWheelMoment(void);
void LCABS_vVerifyWheelMomentWL(struct W_STRUCT *WL);
#endif
  #endif

void LCABS_vDecideVehicleCtrlState(void);
void LCABS_vDctABSatUnevenRoad(void);
void LCABS_vDctNoABSinRestWLs(struct W_STRUCT*FI, struct W_STRUCT*FO, struct W_STRUCT*RI, struct W_STRUCT*RO);

#if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
void LCABS_vDctDriverInputToAccel(void);
#endif

#if __DETECT_LOCK_IN_CYCLE
void LCABS_vDctPossibleLockInCycle(void);
void LCABS_vDctPossibleLockInCycleWL(struct W_STRUCT *WL);
#endif

#if __UNSTABLE_REAPPLY_ADAPTATION
void LCABS_vAdaptdPControl(void);
void LCABS_vAdaptdPControlWL(struct W_STRUCT *UR_WL);
  #if __REDUCE_1ST_UR_PRESS_DIFF
static void LCABS_vURPerOppWheel(struct W_STRUCT *FI, const struct W_STRUCT *FO);
  #endif
#endif

#if __REDUCE_ABS_TIME_AT_BUMP
static void LCABS_vDctPossibleABSForBump(void);
static void LCABS_vDctPossibleABSForBumpWL(struct W_STRUCT *WL_COR, const struct W_STRUCT *WL_OPP);
#endif

  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
static void LCABS_vDctCertainHMu(void);
#endif

static void LCABS_vDctBrkPedlModulation(void);
static int16_t LCABS_s16GetLowestValue(uint8_t Condition, int16_t s16Value1, int16_t s16Value2);

/*Global Extern Variable  Declaration*****************************************/

/* GlobalFunction prototype **************************************************/


/* Implementation*************************************************************/

void LCABS_vDecideWheelCtrlVariable(void)
{
    LCABS_vDecideWheelCtrlVarWL(&FL);
    LCABS_vDecideWheelCtrlVarWL(&FR);
    LCABS_vDecideWheelCtrlVarWL(&RL);
    LCABS_vDecideWheelCtrlVarWL(&RR);
}

void LCABS_vDecideWheelCtrlVarWL(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;                         /* for Compile */

    LCABS_vDecideWheelABSState();       /* set ABS flag */

  #if   __VDC && __ABS_COMB
    if(WL->L_SLIP_CONTROL==0) {
  #endif
    LCABS_vSetSTABLEVariables();        /* set/reset STABIL, STAB_K, STAB_A */
    LCABS_vSetStdDumpCounter();         /* count up ab_z_pr, ab_zaehl and calculate abmittel */
    LCABS_vCountLFCCycle();
    LCABS_vSetWheelDecelGauge();
  #if   __VDC && __ABS_COMB
    }
    else {
/*
        if((!ESP_ERROR_FLG) && (!MPRESS_BRAKE_ON)
      #if __HDC
        && (!lcu1HdcActiveFlg)
      #endif
      #if __ACC
        && (!lcu1AccActiveFlg)
      #endif
      #if __EPB_INTERFACE
        && (!lcu1EpbActiveFlg)
      #endif
        ) {
            ABS_wl=0;
        }
*/
    }
  #endif
}

void LCABS_vDecideWheelABSState(void)
{
    uint16_t u16restWLsReapplyTim;
    uint16_t u16NumOfOutESCWLs   ;

    if(lcabsu1EbdInhibitCondition==1)
    {
        ABS_wl=0;
    }
    else if(ABS_INHIBITION_flag==1)
    {
    	ABS_wl=0;
    }
    else if(((BLS_K==1)&&(lcabsu1MaintainABSatBlsStart==0)) || (RE_ABS_flag==1)) /* PPR 2009-209 */
    {
        ABS_wl=0;
    }
    else if((WL->state == DETECTION) && (REAR_WHEEL_wl==0) && 
         (((LEFT_WHEEL_wl==1)&&(fu1WheelFLErrDet==1))||((LEFT_WHEEL_wl==0)&&(fu1WheelFRErrDet==1))))
    {
        ABS_wl=0;
    }
    /*
  #if __VDC && __HDC
    else if((!ESP_ERROR_FLG) && (!MPRESS_BRAKE_ON) && (!lcu1HdcActiveFlg)) {
        ABS_wl=0;
    }
    */
  #if __VDC
/*
    else if((!ESP_ERROR_FLG) && (!MPRESS_BRAKE_ON)
      #if __HDC
        && (!lcu1HdcActiveFlg)
      #endif
      #if __ACC
        && (!lcu1AccActiveFlg)
      #endif
      #if __EPB_INTERFACE
        && (!lcu1EpbActiveFlg)
      #endif
      ) {
        ABS_wl=0;
    }
*/
  #endif
    else
    {
     #if    __VDC && __ABS_COMB
      if(WL->L_SLIP_CONTROL==0) {
        LCABS_vAssignABSReapplyTim();
      }
     #else
        LCABS_vAssignABSReapplyTim();
     #endif
        LCABS_vLimitABSReapplyTim();

        if(WL->state == DETECTION) {
            if(WL->reapply_tim >= L_1SCAN) {
                if(WL->LFC_ABS_Forced_Enter_flag==1) {
                    ABS_wl=1;
                }
                
              #if    !(__VDC && __ABS_COMB)

                WL->reapply_tim-=L_1SCAN;

              #else /* !(__VDC && __ABS_COMB) */

                if(L_SLIP_CONTROL_wl==1)
                {
                    ; /* Hold Reapply time update */
                }
                else if(l_SLIP_A_wl==1)
                {
                    /* Right after ESC Ctrl, Calc. reapply time based on non-ESC Ctrl wheels */
                    
                    u16restWLsReapplyTim = 0;
                    u16NumOfOutESCWLs   = 0;

                    if((L_SLIP_CONTROL_fl==0) && (l_SLIP_A_fl==0))
                    {
                        u16restWLsReapplyTim = u16restWLsReapplyTim + (uint16_t)FL.reapply_tim;
                        u16NumOfOutESCWLs = u16NumOfOutESCWLs + 1;
                    }

                    if((L_SLIP_CONTROL_fr==0) && (l_SLIP_A_fr==0))
                    {
                        u16restWLsReapplyTim = u16restWLsReapplyTim + (uint16_t)FR.reapply_tim;
                        u16NumOfOutESCWLs = u16NumOfOutESCWLs + 1;
                    }

                    if((L_SLIP_CONTROL_rl==0) && (l_SLIP_A_rl==0))
                    {
                        u16restWLsReapplyTim = u16restWLsReapplyTim + (uint16_t)RL.reapply_tim;
                        u16NumOfOutESCWLs = u16NumOfOutESCWLs + 1;
                    }

                    if((L_SLIP_CONTROL_rr==0) && (l_SLIP_A_rr==0))
                    {
                        u16restWLsReapplyTim = u16restWLsReapplyTim + (uint16_t)RR.reapply_tim;
                        u16NumOfOutESCWLs = u16NumOfOutESCWLs + 1;
                    }
                    
                    if(u16NumOfOutESCWLs > 0)
                    {
                        /*Average reapply time of Outside-ESC Ctrl wheels */
                        WL->reapply_tim = (uint8_t) (u16restWLsReapplyTim / u16NumOfOutESCWLs);
                    }
                    else
                    {
                        WL->reapply_tim = L_TIME_500MS; /* Minimum Fade-out time  */
                    }
                }
			#if (__IDB_LOGIC==ENABLE)
				/* prevent ABS reset by stroke recovery */
				if((lis8StrkRcvrCtrlState_Rx!=S8_STRK_RCVR_READY_STATE)&&
						(lis16DriverIntendedTP>0)&&
						(vref>VREF_10_KPH) )
				{
					; /* Hold Reapply time update */
				}
			#endif
                else
                {
                    WL->reapply_tim-=L_1SCAN;
                }

              #endif /* !(__VDC && __ABS_COMB) */

            }
            else {ABS_wl=0;}
        }
        else if(WL->state == UNSTABLE) {
            ABS_wl=1;
        }
        else { }
    }
}


void LCABS_vAssignABSReapplyTim(void)
{
  #if __SLIGHT_BRAKING_COMPENSATION == ENABLE
    uint8_t u8ReapplyTimRefLowSpd;
    uint8_t u8ReapplyTimRefHighSpd;
  #endif  
    
    if(REAR_WHEEL_wl==0) {
        if(WL->LFC_ABS_Forced_Enter_flag==1)
        {
            if((ABS_wl==0) && (FSF_wl==1))
            {
                if(LEFT_WHEEL_wl==1) {WL->reapply_tim = FR.reapply_tim;}
                else {WL->reapply_tim = FL.reapply_tim;}
            }
        }
    }
    else {
        if((WL->LFC_ABS_Forced_Enter_flag==1) ||
          ((WL->reapply_tim<L_1SCAN) && (((ABS_fl==1) && (ABS_fr==1)) || (LFC_Split_flag==1))))
        {
            if((LEFT_WHEEL_wl==1) && (RR.reapply_tim>=1)) {WL->reapply_tim = RR.reapply_tim;}
            else if((LEFT_WHEEL_wl==0) && (RL.reapply_tim>=1)) {WL->reapply_tim = RL.reapply_tim;}
            else { }
        }
    }

    if(((ABS_wl==1) && (AV_DUMP_wl==1) && (lcabsu1AbsErrorFlag==0)) || (WL->state == UNSTABLE))
    {
      #if __BTC && !__ESV    /* Hydraulic Suttle Valve */
        if(BRAKE_RELEASE) WL->reapply_tim=(uint8_t)L_TIME_100MS;
        else WL->reapply_tim=(uint8_t)L_TIME_1200MS;
      #elif __VDC
        if((ABS_DURING_MPRESS_FLG==1) && (BRAKE_BY_MPRESS_FLG==0)) {WL->reapply_tim=(uint8_t)L_TIME_600MS;}
        else if(ABS_DURING_ACTIVE_BRAKE_FLG==1)
        {
            WL->reapply_tim=(uint8_t)L_TIME_1000MS;
          #if __HDC                                         /******** 7811 NZ *********/
            if(lcu1HdcActiveFlg==1)
            {
                WL->reapply_tim=(uint8_t)L_TIME_800MS;
            }
          #endif                                            /******** 7811 NZ *********/
        }
        else if(BRAKE_BY_MPRESS_FLG==0)
        {
            WL->reapply_tim = (uint8_t)LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_100_KPH, (int16_t)L_TIME_500MS, (int16_t)L_TIME_1000MS);
        }
        else 
        {
          #if __SLIGHT_BRAKING_COMPENSATION == DISABLE
            WL->reapply_tim=(uint8_t)L_TIME_1200MS;
          #else /*__SLIGHT_BRAKING_COMPENSATION */
            u8ReapplyTimRefLowSpd = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefFinalTargetPres, MPRESS_10BAR, MPRESS_100BAR, L_TIME_100MS, L_TIME_1200MS);
            u8ReapplyTimRefHighSpd = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefFinalTargetPres, MPRESS_30BAR, MPRESS_100BAR, L_TIME_1000MS, L_TIME_1200MS);
            WL->reapply_tim = (uint8_t)LCABS_s16Interpolation2P(vref, VREF_10_KPH, VREF_80_KPH, (int16_t)u8ReapplyTimRefLowSpd, (int16_t)u8ReapplyTimRefHighSpd);
          #endif /*__SLIGHT_BRAKING_COMPENSATION */
        }
      #else  /* ABS solo */
        if(lcabsu1BrakeByBLS==0)
        {
            WL->reapply_tim = (uint8_t)LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_100_KPH, (int16_t)L_TIME_500MS, (int16_t)L_TIME_1000MS);
        }
        else
        {
        	WL->reapply_tim=(uint8_t)L_TIME_1200MS;
        }
      #endif
    }
}

void LCABS_vLimitABSReapplyTim(void)
{
    uint8_t last_reapply_time; /* = (uint8_t)T_600_MS; */

    uint8_t Throttle_input_during_ABS = 0;

 #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
    if((INSIDE_ABS_fz==1) && (lcabsu1BrakeByBLS==0)
      #if __VDC
    && (BRAKE_BY_MPRESS_FLG==0)
      #endif
    )
    {
        if((lcabsu1ValidCan==1) && (lcabsu1SuspctAccelIntention==1))
        {
          #if __REAR_D
            if((STABIL_rl==1) && (STABIL_rr==1))
          #else
            if((STABIL_fl==1) && (STABIL_fr==1)/* && (STABIL_rl==1) && (STABIL_rr==1)*/)
          #endif
            {
                Throttle_input_during_ABS = 1;
            }
            else { }
        }
        else { }
    }
 #else /*(__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)*/
 
 #if (__4WD_VARIANT_CODE==ENABLE)
  #if   (__TCS || __VDC)
    if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
    {
        if((INSIDE_ABS_fz==1) && (lcabsu1BrakeByBLS==0)
          #if __VDC
        && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
          #endif
        )
        {
            if((lcabsu1ValidCan==1) && (mtp>=50)) { /* ITCC Clutch off */
                if((STABIL_fl==1) && (STABIL_fr==1) && (STABIL_rl==1) && (STABIL_rr==1))
                {
                    Throttle_input_during_ABS = 1;
                }
                else { }
            }
            else { }
        }
    }
  #endif
 #else /*(__4WD_VARIANT_CODE==ENABLE)*/
  #if   __4WD && (__TCS || __VDC)
    if((INSIDE_ABS_fz==1) && (lcabsu1BrakeByBLS==0)
      #if __VDC
    && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
      #endif
    )
    {
        if((lcabsu1ValidCan==1) && (mtp>=50)) { /* ITCC Clutch off */
            if((STABIL_fl==1) && (STABIL_fr==1) && (STABIL_rl==1) && (STABIL_rr==1))
            {
                Throttle_input_during_ABS = 1;
            }
            else { }
        }
        else { }
    }
    }
  #endif
 #endif /*(__4WD_VARIANT_CODE==ENABLE)*/
 #endif /*(__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)*/

    if((INSIDE_ABS_fz==1) && (((lcabsu1BrakeByBLS==0) && (BLS_A==1))
      #if __VDC
    || (MPRESS_RELEASE_DURING_ABS==1)               /* MPRESS_BRAKE_ON */
    || (ACTIVE_BRAKE_RELEASE_DURING_ABS==1)         /* ACTIVE_BRAKE_FLG */
      #endif
    ))
    {
      #if __VDC
        if((lcabsu1BrakeByBLS==0) && (BLS_A==1)) {BRAKE_RELEASE=1;}
        if(MPRESS_RELEASE_DURING_ABS==1) {BRAKE_RELEASE2=1;}
      #else
        BRAKE_RELEASE=1;
      #endif

        if(WL->state!=DETECTION) {FSF_wl=0;}        /* need to verify */

        WL->state=DETECTION;                /* 00 winter, xd */
      #if __BTC && !__ESV                             /*  Hydraulic Suttle Valve */
        if(WL->reapply_tim>(uint8_t)L_TIME_100MS) {WL->reapply_tim=(uint8_t)L_TIME_100MS;}
      #else
          #if __VDC
        if(MPRESS_RELEASE_DURING_ABS==1)
        {
            if(WL->reapply_tim>(uint8_t)L_TIME_100MS)
            {
                WL->reapply_tim=(uint8_t)L_TIME_100MS;
            }
        }
        else if(ACTIVE_BRAKE_RELEASE_DURING_ABS==1)
        {
            if(WL->reapply_tim>(uint8_t)L_TIME_100MS)
            {
                WL->reapply_tim=(uint8_t)L_TIME_100MS;
            }
        }
        else
        {
          #endif
/*          
            if(WL->reapply_tim>(uint8_t)T_300_MS)
            {
                WL->reapply_tim=(uint8_t)T_300_MS;
            }*/   /*  '04 SW Winter  */
            WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,L_TIME_300MS);
          #if __VDC
        }
          #endif
      #endif
    }
  #if __VDC
    else if((INSIDE_ABS_fz==1) && (ABS_DURING_MPRESS_FLG==1) &&
    ((MP_RELEASE_BW_4BAR_SCAN==1) || (MP_RELEASE_BW_6BAR_SCAN==1)
    ||(MP_RELEASE_BW_10BAR_SCAN==1)) || (MP_RELEASE_BW_SKID_SCAN==1))
    {
        if(MP_RELEASE_BW_4BAR_SCAN==1)
        {
            if(WL->reapply_tim > L_TIME_300MS)
            {
                WL->reapply_tim=(uint8_t)L_TIME_300MS;
            }
            else {}
        }
        else if(MP_RELEASE_BW_6BAR_SCAN==1)
        {
            if(WL->reapply_tim > L_TIME_350MS)
            {
                WL->reapply_tim=(uint8_t)L_TIME_350MS;
            }
            else {}
        }
        else if(MP_RELEASE_BW_10BAR_SCAN==1)
        {
            if(WL->reapply_tim > L_TIME_400MS)
            {
                WL->reapply_tim=(uint8_t)L_TIME_400MS;
            }
            else {}
        }
        else if(MP_RELEASE_BW_SKID_SCAN==1)
        {
        	WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,L_TIME_300MS);
        }
        else {}
    }
  #endif
    else if(Throttle_input_during_ABS==1)
    {
        WL->reapply_tim=0;
    }
    else if(WL->state == DETECTION)
    {
      #if __BTC && !__ESV
        if(vref < (int16_t)VREF_3_KPH)
        {
            if(WL->reapply_tim > (uint8_t)L_TIME_100MS) {
                WL->reapply_tim=(uint8_t)L_TIME_100MS;
            }
      #else
        if(vref < (int16_t)VREF_3_KPH)
        {

            if(WL->hold_timer_new_ref>=L_4SCAN) {last_reapply_time = (uint8_t)L_TIME_600MS;}
            else if(WL->hold_timer_new_ref<=L_2SCAN) {last_reapply_time = (uint8_t)L_TIME_300MS;}
            else {last_reapply_time = L_TIME_300MS + ((WL->hold_timer_new_ref - L_2SCAN)*L_TIME_150MS);}
#if 0 //LCK
            if(ref_mon_ad >= MOTOR_12V) {
/*                
                if(WL->reapply_tim > last_reapply_time) {
                    WL->reapply_tim=last_reapply_time;      // Modified for LFC 
                }
*/
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,(int16_t)last_reapply_time);
            }
            else if(ref_mon_ad >= MOTOR_11V) {
/*                
                if(WL->reapply_tim > (last_reapply_time+(uint8_t)T_100_MS)) {
                    WL->reapply_tim=last_reapply_time+(uint8_t)T_100_MS;      // Modified for LFC
                }
*/
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,(int16_t)last_reapply_time+L_TIME_100MS);
            }
            else {
/*
                if(WL->reapply_tim > (last_reapply_time+(uint8_t)T_200_MS)) {
                    WL->reapply_tim=last_reapply_time+(uint8_t)T_200_MS;      // Modified for LFC
                }
*/
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,(int16_t)last_reapply_time+L_TIME_200MS);
            }
#endif
      #endif

            if(WL->reapply_tim == L_1SCAN){               /* Added by KGY 2002.12.10 */
                WL->motor_force_off_flag=1;/*not used*/

                if((WL->pwm_duty_temp>S16_MINIMUM_EFFECTIVE_LFC_DUTY)&&(under_limit_speed_cnt<=L_TIME_1200MS))
                {
                    WL->reapply_tim += L_1SCAN;
                }
            }
        }
/*      #if __BTC_ABS_CLEAR */
        else if(vref<=(int16_t)VREF_5_KPH) {
            if(WL->reapply_tim == L_1SCAN) {
                WL->motor_force_off_flag=1;
                if((WL->pwm_duty_temp>S16_MINIMUM_EFFECTIVE_LFC_DUTY)&&(btc_end_delay_cnt<=L_TIME_500MS)&&(btc_end_delay_cnt>0)) {WL->reapply_tim += L_1SCAN;}
            }
        }
/*      #endif */
        else
        {
            if(lcabsu1TwoWlABSUneven==1)
            {
/*
                if(WL->reapply_tim > T_400_MS)
                {
                    WL->reapply_tim = T_400_MS;
                }
                else { }
*/
                tempW3 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_80_KPH,L_TIME_400MS,L_TIME_1000MS);
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,tempW3);
            }
            else if(lcabsu1OneWlABSUneven==1)
            {
/*
                if(WL->reapply_tim > T_300_MS)
                {
                    WL->reapply_tim = T_300_MS;
                }
                else { }
*/
                tempW3 = LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_80_KPH,L_TIME_300MS,L_TIME_1000MS);
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,tempW3);
            }
            else
          #if __REDUCE_ABS_TIME_AT_BUMP
            if(PossibleABSForBumpVehicle==1)
            {
			  #if(__BUMP_COMP_IMPROVE==ENABLE)
                tempW3 = LCABS_s16Interpolation2P(vref,S16_BUMP_LOW_SPD,S16_BUMP_HI_SPD,T_300_MS,T_1000_MS);
              #else
                tempW3 = LCABS_s16Interpolation2P(vref,S16_BUMP_LOW_SPD,S16_BUMP_MED_SPD,T_500_MS,T_700_MS);
              #endif
                WL->reapply_tim = (uint8_t)LCABS_s16LimitMinMax((int16_t)WL->reapply_tim,0,tempW3);
            }
            else
          #endif
            { }


          #if __DELAY_ABS_EXIT_FOR_HIGH_DUTY
            if((lcabsu1NoABSinRest3WLs==1)||(lcabsu1NoABSinRest2WLs==1))
            {
                if((WL->reapply_tim <= L_1SCAN)&&(WL->pwm_duty_temp>S16_MINIMUM_EFFECTIVE_LFC_DUTY))
                {
                    WL->reapply_tim += L_1SCAN;
                }
            }
          #endif

        }
    }
    else
    {
        ;
    }

  #if __UCC_COOPERATION_CONTROL
    LCCDC_vLimitReapplyTimePotHole();
  #endif

}

void LCABS_vDctABSatUnevenRoad(void)
{
    if((ABS_fl==1) && (ABS_fr==0))
    {
        LCABS_vDctNoABSinRestWLs(&FL, &FR, &RL, &RR);
    }
    else if((ABS_fl==0) && (ABS_fr==1))
    {
        LCABS_vDctNoABSinRestWLs(&FR, &FL, &RR, &RL);
    }
    else
    {
        lcabss8NoABSinRestWLCnt = 0;
    }

  #if __REDUCE_ABS_TIME_AT_BUMP 
    LCABS_vDctPossibleABSForBump();
  #endif
}

void LCABS_vDctNoABSinRestWLs(struct W_STRUCT *FI, struct W_STRUCT *FO, struct W_STRUCT *RI, struct W_STRUCT *RO)
{
    lcabsu1OneWlABSUneven = 0;
    lcabsu1TwoWlABSUneven = 0;

    if((RI->ABS==0) && (RO->ABS==0)) /* One wheel ABS engaged */
    {
        if(FI->state==DETECTION)
        {
            if(lcabsu1NoABSinRest3WLs==0)
            {
                if((FO->rel_lam > -LAM_3P) || ((vref - FO->vrad_crt) < VREF_1_5_KPH))
                {
                    lcabss8NoABSinRestWLCnt = lcabss8NoABSinRestWLCnt + L_1SCAN;
                }
                else if((FO->rel_lam > -LAM_15P) || ((vref - FO->vrad_crt) < VREF_8_KPH))
                {
                    ;
                }
                else
                {
                    lcabss8NoABSinRestWLCnt=0;
                }

                if(lcabss8NoABSinRestWLCnt > L_TIME_50MS)
                {
                    lcabsu1OneWlABSUneven=1;
                    lcabsu1NoABSinRest3WLs=1;
                }
            }
        }
        else
        {
            lcabss8NoABSinRestWLCnt = 0;
            lcabsu1NoABSinRest3WLs = 0;
        }
    }
    else if((RI->ABS==1) && (RO->ABS==0)) /* One Front and One Rear wheel on same side ABS engaged */
    {
        if(lcabsu1NoABSinRest3WLs==1)
        {
            lcabss8NoABSinRestWLCnt = 0;
            lcabsu1NoABSinRest3WLs = 0;
        }
        if((FI->state == DETECTION) && (RI->state == DETECTION))
        {
            if(lcabsu1NoABSinRest2WLs==0)
            {
                if(((FO->rel_lam > -LAM_3P) || ((vref - FO->vrad_crt) < VREF_1_5_KPH)) &&
                   ((RO->rel_lam > -LAM_3P) || ((vref - RO->vrad_crt) < VREF_1_5_KPH)))
                {
                    lcabss8NoABSinRestWLCnt = lcabss8NoABSinRestWLCnt + L_1SCAN;
                }
                else if(((FO->rel_lam > -LAM_15P) || ((vref - FO->vrad_crt) < VREF_8_KPH)) &&
                        ((RO->rel_lam > -LAM_15P) || ((vref - RO->vrad_crt) < VREF_8_KPH)))
                {
                    ;
                }
                else
                {
                    lcabss8NoABSinRestWLCnt=0;
                }

                if(lcabss8NoABSinRestWLCnt > L_TIME_70MS)
                {
                    lcabsu1TwoWlABSUneven=1;
                    lcabsu1NoABSinRest2WLs=1;
                }
            }
        }
        else
        {
            lcabss8NoABSinRestWLCnt = 0;
            lcabsu1NoABSinRest2WLs = 0;
        }
    }
    else /* More Than two wheels ABS engaged*/
    {
        lcabss8NoABSinRestWLCnt = 0;
        lcabsu1NoABSinRest2WLs = 0;
        lcabsu1NoABSinRest3WLs = 0;
    }
}

#if __REDUCE_ABS_TIME_AT_BUMP
static void LCABS_vDctPossibleABSForBump(void)
{
    LCABS_vDctPossibleABSForBumpWL(&FL,&FR);
    LCABS_vDctPossibleABSForBumpWL(&FR,&FL);

  #if (__ONE_WHEEL_BUMP_DCT==ENABLE)
    if(ABS_fz==1)
    {
        if((FL.PossibleABSForBump==1)&&(FR.PossibleABSForBump==1)
         &&(FL.LFC_STABIL_counter > L_TIME_150MS)&&(FR.LFC_STABIL_counter > L_TIME_150MS))
        {
            PossibleABSForBumpVehicle=1;
            lcabsu1ABSForBumpByOneWhl=0;
        }    
        else if(vref<=S16_BUMP_MED_SPD)
        {
	        if((FL.PossibleABSForBump==1)
	         &&(FL.LFC_STABIL_counter > L_TIME_200MS)               &&((ABS_fr==0)||(FR.LFC_STABIL_counter > L_TIME_200MS))
	         &&((ABS_rl==0)||(RL.LFC_STABIL_counter > L_TIME_150MS))&&((ABS_rr==0)||(RR.LFC_STABIL_counter > L_TIME_150MS)))
	        {
	            PossibleABSForBumpVehicle=1;
	            lcabsu1ABSForBumpByOneWhl=1;
	        }
	        else if((FR.PossibleABSForBump==1)
	         &&(FR.LFC_STABIL_counter > L_TIME_200MS)               &&((ABS_fl==0)||(FL.LFC_STABIL_counter > L_TIME_200MS))
	         &&((ABS_rr==0)||(RR.LFC_STABIL_counter > L_TIME_150MS))&&((ABS_rl==0)||(RL.LFC_STABIL_counter > L_TIME_150MS)))
	        {
	            PossibleABSForBumpVehicle=1;
	            lcabsu1ABSForBumpByOneWhl=1;
	        }
	        else
	        {
	        	PossibleABSForBumpVehicle=0;
	        	lcabsu1ABSForBumpByOneWhl=0;
	        }
    	}
		else
		{
			PossibleABSForBumpVehicle=0;
			lcabsu1ABSForBumpByOneWhl=0;
		}
    }
    else
    {
        PossibleABSForBumpVehicle=0;
        lcabsu1ABSForBumpByOneWhl=0;
    }
  #else
    if(ABS_fz==1)
    {
        if((FL.PossibleABSForBump==1)&&(FR.PossibleABSForBump==1)
         &&(FL.LFC_STABIL_counter > L_TIME_150MS)&&(FR.LFC_STABIL_counter > L_TIME_150MS))
        {
            PossibleABSForBumpVehicle=1;
        }
        else
        {
            PossibleABSForBumpVehicle=0;
        }
    }
    else
    {
        PossibleABSForBumpVehicle=0;
    }
  #endif  /*(__ONE_WHEEL_BUMP_DCT==ENABLE)*/    
}

static void LCABS_vDctPossibleABSForBumpWL(struct W_STRUCT *WL_COR, const struct W_STRUCT *WL_OPP)
{
    /* COR : corresponding, OPP : opposite */

    int8_t PeakAccRefPerDump;
    int8_t PeakAccRefPerDumpLowSpd;
    int8_t PeakAccRefPerDumpMedSpd;
  #if (__BUMP_COMP_IMPROVE==ENABLE)
    int8_t PeakAccRefPerDumpHiSpd;
  #endif
    int8_t s8MaxWheelAccel;

    if(WL_COR->peak_acc_alt > WL_COR->max_arad_instbl)
    {
        s8MaxWheelAccel = WL_COR->peak_acc_alt;
    }
    else
    {
        s8MaxWheelAccel = WL_COR->max_arad_instbl;
    }

	PeakAccRefPerDumpLowSpd=(int8_t)LCABS_s16Interpolation2P((int16_t)WL_COR->LFC_dump_counter,1,(int16_t)U8_BUMP_MAX_DUMP_SCANS,(int16_t)S8_BUMP_PEAK_ACC_REF_1_DUMP,(int16_t)S8_BUMP_PEAK_ACC_REF_MAX_DUMP);    
    PeakAccRefPerDumpMedSpd=(int8_t)(((int16_t)PeakAccRefPerDumpLowSpd*3)/2);
    PeakAccRefPerDump=(int8_t)LCABS_s16Interpolation2P((int16_t)vref,S16_BUMP_LOW_SPD,S16_BUMP_MED_SPD,PeakAccRefPerDumpLowSpd,PeakAccRefPerDumpMedSpd);  
    
  #if (__BUMP_COMP_IMPROVE==ENABLE)
	if(vref > S16_BUMP_MED_SPD)
	{
		PeakAccRefPerDumpHiSpd=(int8_t)(((int16_t)PeakAccRefPerDumpLowSpd*7)/3);
  		PeakAccRefPerDump=(int8_t)LCABS_s16Interpolation2P((int16_t)vref,S16_BUMP_MED_SPD,S16_BUMP_HI_SPD,PeakAccRefPerDumpMedSpd,PeakAccRefPerDumpHiSpd);  	
	}
  #endif

    if((ABS_fz==1)&&(YAW_CDC_WORK==0))
    {
        if(WL_OPP->PossibleABSForBump==0)
        {
            if(WL_COR->STABIL==1)
            {
                if(WL_COR->STAB_K==1)
                {
                    /* Threshold Calculation */
	                /*PeakAccRefPerDump=(int8_t)LCABS_s16Interpolation2P((int16_t)WL_COR->LFC_dump_counter,1,U8_BUMP_MAX_DUMP_SCANS,S8_BUMP_PEAK_ACC_REF_1_DUMP,S8_BUMP_PEAK_ACC_REF_MAX_DUMP);*/                
	                if((WL_COR->LFC_dump_counter <= U8_BUMP_MAX_DUMP_SCANS) && (s8MaxWheelAccel > PeakAccRefPerDump)
	              #if (__BUMP_COMP_IMPROVE==ENABLE)
	                &&(vref<S16_BUMP_HI_SPD)
	              #else
	                &&(vref<S16_BUMP_MED_SPD)  
	              #endif
	                )
                    {
                        WL_COR->PossibleABSForBump=1;
					  #if(__BUMP_COMP_IMPROVE==ENABLE)
	                	if((LFC_Split_flag==1)&&(YMR_check_counter>T_100_MS))
	                	{
	                		if((WL_COR->LFC_dump_counter>=U8_LIMIT_DMP_CNT_FOR_BMP_DCT)||(WL_COR->LFC_UNSTABIL_counter>=U8_LIMIT_CNT_SPLT_FOR_BMP_DCT))
	                	    {
	                			WL_COR->PossibleABSForBump=0;
	                			/*WL->flags=75;*/
	                		}               			
	                	}
	                	else if(AFZ_OK == 0)
	                	{
	                		if((vref>=S16_BUMP_MED_SPD)&&((WL_COR->LFC_dump_counter>U8_LIMIT_DMP_CNT_FOR_BMP_DCT)||(WL_COR->LFC_UNSTABIL_counter>U8_LIMIT_CNT_FOR_BMP_DCT))
	                		  #if (__ONE_WHEEL_BUMP_DCT==ENABLE)
	                		&&(lcabsu1ABSForBumpByOneWhl==0)
	                		  #endif
	                		)
	                	    {
	                			WL_COR->PossibleABSForBump=0;
	                			/*WL->flags=76;*/
	                		}               			
	                	}
	                	else if((afz <= -(int16_t)U8_HIGH_MU)||(WL_COR->Reapply_Accel_cycle==1))
	                	{
	                		WL_COR->PossibleABSForBump=0;
	                		/*WL->flags=77;*/
	                	}
						else {}					
					  #endif/*__BUMP_DETECT_AT_HIGH_SPD*/
	                }
	                else
	                {
	                    WL_COR->PossibleABSForBump=0;
	                }
                }
            }
            else
            {
                WL_COR->PossibleABSForBump=0;
            }
        }
        else
        {
            /* Threshold Calculation */
            PeakAccRefPerDump = (int8_t)(((int16_t)PeakAccRefPerDump*3)/4);
            PeakAccRefPerDump = (int8_t)LCABS_s16LimitMinMax((int16_t)PeakAccRefPerDump,(int16_t)ARAD_1G0,(int16_t)S8_BUMP_PEAK_ACC_REF_MAX_DUMP);

            if(WL_COR->STABIL==1)
            {
                if((WL_COR->LFC_STABIL_counter < L_TIME_50MS) && (WL_OPP->LFC_STABIL_counter < L_TIME_100MS))
                {
                    /* Detection */
                    if((WL_COR->LFC_dump_counter <= U8_BUMP_MAX_DUMP_SCANS) && (s8MaxWheelAccel > PeakAccRefPerDump)
              	  #if (__BUMP_COMP_IMPROVE==ENABLE)
              		  &&(vref<S16_BUMP_HI_SPD)
                  #else
              		  &&(vref<S16_BUMP_MED_SPD)  
              	  #endif
              		)
                    {
                        WL_COR->PossibleABSForBump=1;
                  	  #if(__BUMP_COMP_IMPROVE==ENABLE)
                		if(LFC_Split_flag==1)
                		{
                			if((WL_COR->LFC_dump_counter>=U8_LIMIT_DMP_CNT_FOR_BMP_DCT)||(WL_COR->LFC_UNSTABIL_counter>=U8_LIMIT_CNT_SPLT_FOR_BMP_DCT))
                		    {
                				WL_COR->PossibleABSForBump=0;
                				/*WL->flags=75;*/
                			}               			
                		}
                		else if(AFZ_OK == 0)
                		{
                			if((vref>=S16_BUMP_MED_SPD)&&((WL_COR->LFC_dump_counter>U8_LIMIT_DMP_CNT_FOR_BMP_DCT)||(WL_COR->LFC_UNSTABIL_counter>U8_LIMIT_CNT_FOR_BMP_DCT)))
                		    {
                				WL_COR->PossibleABSForBump=0;
                				/*WL->flags=76;*/
                			}               			
                		}
                		else if((afz <= -(int16_t)U8_HIGH_MU)||(WL_COR->Reapply_Accel_cycle==1))
                		{
                			WL_COR->PossibleABSForBump=0;
                			/*WL->flags=77;*/
                		}
						else {}					
				  	  #endif/*__BUMP_DETECT_AT_HIGH_SPD*/
                    }
                    else
                    {
                        ;
                    }
                }
            }
            else
            {
                WL_COR->PossibleABSForBump=0;
            }
        }
    }
    else
    {
        WL_COR->PossibleABSForBump=0;
    }
}
#endif /*#if __REDUCE_ABS_TIME_AT_BUMP*/

void LCABS_vSetSTABLEVariables(void)
{
    STAB_K_wl=0;
    STAB_A_wl=0;

    if(REAR_WHEEL_wl==0) {    /* update STABIL, STAB_K, STAB_A, zyklus_z for the front wheel */
        if(ABS_wl==0) 
        {
        	WL->zyklus_z=0;
		    WL->frcnt=0;
		    WL->rough_hold_time=0;
        }

        if(STABIL_wl==0) {
            if(WL->state == DETECTION) {
                STABIL_wl=1;
                STAB_K_wl=1;
                HIGH_VFILT_wl=0;

                if((FSF_wl==0) && (GMA_SLIP_wl==0))     /* Changed by KGY 030211 */
                {
                    WL->zyklus_z+=1;
/*                    if(WL->force_dump_counter !=0) {WL->force_dump_counter--;} */
                }
            }
        }
        else {
            if(WL->state != DETECTION) {
                STABIL_wl=0;
                STAB_A_wl=1;

                if((GMA_fl==0) && (GMA_fr==0) && (vref < (int16_t)VREF_70_KPH)) {
                    if(RES_FRQ_wl==1) {
                        RES_FRQ_wl=0;

                        if((ARAD_3G_POS_rl !=0) || (ARAD_3G_POS_rr !=0)) {
                            if(WL->max_arad_instbl > (int8_t)ARAD_10G0) {
                                HOLD_RES_wl=1;
                            /*    RES_FRQ_wl=0;       */
                                ARAD_3G_POS_rl=0;
                                ARAD_3G_POS_rr=0;
                                WL->frcnt++;
                                WL->rough_hold_time=1;
                                if(WL->zyklus_z >= 5) {
                                    if(WL->frcnt < 2) {HOLD_RES_wl=0;}
                                }
                            }
                        }
                    }
                    else {
                        if(WL->rough_hold_time==0) {HOLD_RES_wl=0;}
                        else {WL->rough_hold_time=0;}

                        if(WL->frcnt !=0) {WL->frcnt--;}
                    }
                }
                else {RES_FRQ_wl=0;}

                WL->max_arad_instbl=0;
                WL->peak_dec_max=0; /*  03 USA winter */
            }
        }

    }
    else {                    /* update STABIL, STAB_K, STAB_A, zyklus_z for the rear wheel */
      #if __REAR_MI
        if(ABS_wl==0) 
        {
        	WL->zyklus_z=0;
		    WL->frcnt=0;
		    WL->rough_hold_time=0;
        }
      #else
        if(ABS_rl==0) {
            if(ABS_rr==0) 
            {
            	zyklus_z_rl=zyklus_z_rr=0;
			    RL.frcnt=RR.frcnt=0;
			    RL.rough_hold_time=RR.rough_hold_time=0;
            }
        }
      #endif

/*     Need to check!!!
#if __REAR_MI                               // 030107 zyklus_z++ For LFC
        if(MSL_BASE_wl==0) {            // 030107 zyklus_z++ For LFC
            if((SET_DUMP2_wl==1)&&(WL->msl_reapply_time >= (uint8_t)1)) {
                SET_DUMP2_wl=0;
                WL->zyklus_z+=1;
            }
        }
#endif
*/

        if(STABIL_wl==0) {
          #if __REAR_MI
            if(WL->state==DETECTION)         /* mi03_slip */  /*  OLD SL LOGIC */
          #else
            if((state_rl==DETECTION) && (state_rr==DETECTION))
          #endif
            {
                STABIL_wl=1;
                STAB_K_wl=1;
              #if __EBD_MI_ENABLE == DISABLE
                WL->pulse_up_row_timer = 0;
              #endif  
                if(EBD_wl==0) {WL->hold_timer_new=0;}         /* 030107 zyklus_z++ For LFC */
                if(FSF_wl==0) {WL->zyklus_z+=1;}
            }
        }
        else {
          #if __REAR_MI
            if(WL->state!=DETECTION)         /* mi03_slip */  /*  OLD SL LOGIC */
          #else
            if((state_rl!=DETECTION) && (state_rr!=DETECTION))
          #endif
            {
                STABIL_wl=0;
                STAB_A_wl=1;
                WL->peak_dec_max=0;
            }
        }
    }

#if !__REAR_MI
    if(REAR_WHEEL_wl == 1) {
        if(STABIL_wl == 0) {
            if(state_rl == 0) {
                if(state_rr != 0) {SL_BASE_rr=1;}
            }

            if(state_rr == 0) {
                if(state_rl != 0) {SL_BASE_rl=1;}
            }
        }
    }
#endif

    if(ABS_wl==0) {WL->castim=0;}         /* increment and decrement cascading timer */
    else if(STAB_A_wl==1) {             /* set/clear CAS flag */
        CAS_wl=0;

        if((WL->castim != 0)||((REAR_WHEEL_wl==1)&&(FSF_wl==1))) {CAS_wl=1;}  /*  Changed due to BASALT Cascading 030722 */
        if((vref>=VREF_100_KPH) ||
        ((AFZ_OK==1)&&(afz<=-(int16_t)(AFZ_0G55))&&(vref>=VREF_80_KPH)) ||
        ((AFZ_OK==1)&&(afz>-(int16_t)(AFZ_0G55))&&(vref>=VREF_50_KPH))) {CAS_wl=1;}
        WL->castim=1;
    }
    else {
        if(STABIL_wl==1) {
            if(WL->arad < (int8_t)ARAD_0G5) {
                if(WL->castim != 0) {WL->castim-=1;}
            }
            else if(WL->castim != (uint8_t)0xff) {WL->castim+=1;}
            else { }
        }
        else if(WL->castim != (uint8_t)0xff){ WL->castim+=1;}
        else { }
    }

  #if __AQUA_ENABLE  
    if(REAR_WHEEL_wl==0) {              /* set V_INST_80 */
        if(STAB_A_wl==1) {
            if(vref > (int16_t)VREF_80_KPH) {V_INST_80=1;}
        }
    }
  #endif  

    if((STABIL_wl==1)&&((ABS_wl==1)||(FSF_wl==0))) {
        if(WL->LFC_STABIL_counter<=200) {WL->LFC_STABIL_counter = WL->LFC_STABIL_counter + L_1SCAN;}
        if(WL->LFC_STABIL_counter2<=200) {
            if(REAR_WHEEL_wl==0) {
                if(LEFT_WHEEL_wl==1) {
                    if((WL->rel_lam>=-S8_L2H_STABLE_THR_SLIP_F) && (FR.rel_lam>=-S8_L2H_STABLE_THR_SLIP_F_OPP)) {WL->LFC_STABIL_counter2 = WL->LFC_STABIL_counter2 + L_1SCAN;}
                    else if((WL->rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_F) || (FR.rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_F_OPP)) {
                        WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_1SCAN);
/*
                        if(WL->LFC_STABIL_counter2>0) {WL->LFC_STABIL_counter2--;}
*/
                    }
                    else { }
                }
                else {
                    if((WL->rel_lam>=-S8_L2H_STABLE_THR_SLIP_F) && (FL.rel_lam>=-S8_L2H_STABLE_THR_SLIP_F_OPP)) {WL->LFC_STABIL_counter2 = WL->LFC_STABIL_counter2 + L_1SCAN;}
                    else if((WL->rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_F) || (FL.rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_F_OPP)) {
                        WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_1SCAN);
/*
                        if(WL->LFC_STABIL_counter2>0) {WL->LFC_STABIL_counter2--;}
*/
                    }
                    else { }
                }
            }
            else {
                if(LEFT_WHEEL_wl==1) {
                    if((WL->rel_lam>=-S8_L2H_STABLE_THR_SLIP_R) && (RR.rel_lam>=-S8_L2H_STABLE_THR_SLIP_R_OPP)) {WL->LFC_STABIL_counter2 = WL->LFC_STABIL_counter2 + L_1SCAN;}
                    else if((WL->rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_R) || (RR.rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_R_OPP)) {
                        WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_1SCAN);
/*
                        if(WL->LFC_STABIL_counter2>0) {WL->LFC_STABIL_counter2--;}
*/
                    }
                    else { }
                }
                else {
                    if((WL->rel_lam>=-S8_L2H_STABLE_THR_SLIP_R) && (RL.rel_lam>=-S8_L2H_STABLE_THR_SLIP_R_OPP)) {WL->LFC_STABIL_counter2 = WL->LFC_STABIL_counter2 + L_1SCAN;}
                    else if((WL->rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_R) || (RL.rel_lam<-S8_L2H_UNSTABLE_THR_SLIP_R_OPP)) {
                        WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_1SCAN);
/*
                        if(WL->LFC_STABIL_counter2>0) {WL->LFC_STABIL_counter2--;}
*/
                    }
                    else { }
                }
            }
        }
    }
    else {
      #if __UNSTABLE_REAPPLY_ENABLE && (__UNSTABLE_REAPPLY_ADAPTATION==DISABLE)
        if(STAB_A_wl==1)
        {
            WL->LFC_STABIL_counter_old=WL->LFC_STABIL_counter;
        }
        else
        {
            ;
        }
      #endif /* __UNSTABLE_REAPPLY_ADAPTATION==DISABLE */

        WL->LFC_STABIL_counter=0;
        if((ABS_wl==1) && (REAR_WHEEL_wl==0))
        {
            if(WL->rel_lam <= -S8_L2H_UNSTABLE_THR_SLIP_F_3)
            {
                WL->LFC_STABIL_counter2=0;
            }
            else if(WL->rel_lam <= -S8_L2H_UNSTABLE_THR_SLIP_F_2)
            {
                WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_10SCAN);
/*
                if(WL->LFC_STABIL_counter2>10) {WL->LFC_STABIL_counter2=WL->LFC_STABIL_counter2-10;}
                else {WL->LFC_STABIL_counter2=0;}
*/
            }
            else if(WL->rel_lam <= -S8_L2H_UNSTABLE_THR_SLIP_F_1)
            {
                WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_5SCAN);
/*
                if(WL->LFC_STABIL_counter2>5) {WL->LFC_STABIL_counter2=WL->LFC_STABIL_counter2-5;}
                else {WL->LFC_STABIL_counter2=0;}
*/
            }
            else
            {
                WL->LFC_STABIL_counter2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->LFC_STABIL_counter2,-L_2SCAN);
/*
                if(WL->LFC_STABIL_counter2>2) {WL->LFC_STABIL_counter2=WL->LFC_STABIL_counter2-2;}
                else {WL->LFC_STABIL_counter2=0;}
*/
            }
        }
        else
        {
            WL->LFC_STABIL_counter2=0;
        }
    }

    if((STABIL_wl==0) && (WL->LFC_UNSTABIL_counter<=200)) {WL->LFC_UNSTABIL_counter = WL->LFC_UNSTABIL_counter + L_1SCAN;}
    else if((ABS_fz==0) || (WL->LFC_built_reapply_counter>=L_1ST_SCAN)) {
        if(WL->LFC_built_reapply_counter==L_1ST_SCAN) {WL->LFC_UNSTABIL_counter_old = WL->LFC_UNSTABIL_counter;}
        WL->LFC_UNSTABIL_counter=0;
    }
    else { }

}

void LCABS_vSetStdDumpCounter(void)  /* Need to improve the module structure!!!*/
{
    if((ABS_fz==0)&&(EBD_RA==0)) {
        WL->ab_z_pr=WL->ab_zaehl=0;
        WL->s0_reapply_counter=0;
        WL->s0_reapply_counter_old=0;
    }
    else if((STAB_A_wl==1) || (WL->SL_Dump_Start==1)) {
        WL->ab_z_pr=WL->ab_zaehl=WL->s0_reapply_counter=0;
    }
    else if((GMA_SLIP_wl==1) && (GMA_PULSE==1) && (WL->gma_dump>0)) {
        WL->s0_reapply_counter=0;
    }
    else if(STABIL_wl==1) {
        WL->s0_reapply_counter_old=WL->s0_reapply_counter;
    }
    else { }

    if(UNDERVOLT_DETECT_ABS==0) {           /* update ab_z_pr, ab_zaehl */
        if(STAB_K_wl==0) {
            if(AV_DUMP_wl==1) {
                if(GMA_SLIP_wl==0) {
                    if(WL->ab_z_pr != 0xff) {WL->ab_z_pr+=1;}
                    if(WL->ab_zaehl != 0xff) {WL->ab_zaehl+=1;}
                }
            }
/*
            else if(SL_DUMP_wl==1) {
                if(WL->ab_z_pr != 0xff) {WL->ab_z_pr+=1;}
            }
*/
            else { }
        }
    }
}

void LCABS_vCountLFCCycle(void)
{
    if((FSF_wl==1) && (ABS_fz==0)) {WL->LFC_zyklus_z=0;}
    else {
        if(ABS_wl==1){
            if(REAR_WHEEL_wl==0) {
                if(GMA_SLIP_wl==0)
                {
                    if((WL->LFC_built_reapply_counter==L_1ST_SCAN)&&(WL->LFC_s0_reapply_counter==1)) {
                        WL->LFC_zyklus_z++;     /*  added at 2002.winter            */
                    }

                }

            }
            else {
                if((WL->LFC_built_reapply_counter==L_1ST_SCAN)&&(WL->LFC_s0_reapply_counter==1)) {
                    WL->LFC_zyklus_z++;     /*  added at 2002.winter            */
                }
                if((STAB_A_wl==1) || (WL->SL_Dump_Start==1)) {
                    /*if((WL->LFC_zyklus_z==1) && (WL->LFC_reapply_current_duty==0)) {WL->LFC_zyklus_z=0;}*//*WL->LFC_reapply_current_duty==0 need to change*/
                }
            }
        }
    }
}

void LCABS_vSetWheelDecelGauge(void)
{
  #if __BUMP_SUSPECT	
    WL->LFC_Bump_Suspect_flag=0;
  #endif  
    if(WL->state == UNSTABLE) {
        if(STAB_A_wl==1) {              /* Wheel state : Stable -> Unstable */
            WL->arad_gauge=0;
            WL->pos_arad_sum=0;
            WL->pos_arad_num=0;
            WL->accel_arad_avg=0;
          #if   __TEMP_for_CAS2
            WL->arad_gauge_cas2=0;
            WL->arad_gauge_cas2_cnt=0;
            WL->arad_gauge_cas3=0;
            WL->arad_gauge_cas3_cnt=0;
            WL->CAS_SUSPECT_reset_timer=0;
              #if __IMP_RISEDELAY_AND_DUMP_FOR_CAS
            WL->CAS_SUSPECT_flag_old=WL->CAS_SUSPECT_flag;
              #endif
            WL->CAS_SUSPECT_flag=0;
          #endif
        }
        else {
            if((WL->s_diff > 0) && (WL->arad>ARAD_0G0)) {
                if(WL->arad>=S8_ARAD_GAUGE_LV5) {
                    WL->arad_gauge = WL->arad_gauge + 15;
                }
                else if(WL->arad>=S8_ARAD_GAUGE_LV4) {
                    WL->arad_gauge = WL->arad_gauge + 10;
                }
                else if(WL->arad>=S8_ARAD_GAUGE_LV3) {
                    WL->arad_gauge = WL->arad_gauge + 4;
                }
                else if(WL->arad>=S8_ARAD_GAUGE_LV2) {
                    WL->arad_gauge = WL->arad_gauge + 2;
                }
                else if(WL->arad>=S8_ARAD_GAUGE_LV1) {
                    ;/*WL->arad_gauge = WL->arad_gauge;*/
                }
                else {
                    WL->arad_gauge = WL->arad_gauge - 3;
                }
    
                WL->pos_arad_sum = WL->pos_arad_sum + (uint16_t)WL->arad;
                WL->pos_arad_num = WL->pos_arad_num + 1;
            }
          #if   __TEMP_for_CAS2
            if(WL->CAS_SUSPECT_flag==0) {
              #if ((__IMP_1CYCLE_NON_DRIVEN_W_RISE_DELAY==1)||(__IMP_4WD_1CYCLE_DRIVEN_1W_RISE_DELAY==1))
                if (FSF_wl==0) {tempB0 = 2;}
                else if (REAR_WHEEL_wl==1) {tempB0 = 6;}
                else {tempB0 = 7;}
               
                if(WL->arad>-ARAD_1G0) {
                    if(WL->LFC_dump_counter>=tempB0) {
                        WL->arad_gauge_cas2 = WL->arad_gauge_cas2 + (int16_t)WL->arad;
                        if(WL->arad_gauge_cas2_cnt<=S8_TYPE_MAXNUM) {WL->arad_gauge_cas2_cnt = WL->arad_gauge_cas2_cnt + 1;}
                    }
                    if(WL->LFC_dump_counter>=(tempB0+1)) {
                        WL->arad_gauge_cas3 = WL->arad_gauge_cas3 + (int16_t)WL->arad;
                        if(WL->arad_gauge_cas3_cnt<=S8_TYPE_MAXNUM) {WL->arad_gauge_cas3_cnt = WL->arad_gauge_cas3_cnt + 1;}
                    }
                    
                    tempB1 = 0; 
                    #if ((__4WD==1)||(__4WD_VARIANT_CODE==ENABLE))
                  #if __REAR_D
                    if((FSF_wl==0)||(REAR_WHEEL_wl==0)) {tempB1 = 1;}
                    else {tempB1 = 2;}                            
                  #else
                    if((FSF_wl==0)||(REAR_WHEEL_wl==1)) {tempB1 = 1;}
                    else {tempB1 = 2;}
                  #endif
                  #else
                    tempB1 = 1;
                    #endif

                  #if __REAR_D
                    if(vref>=VREF_15_KPH)
                  #else
                    if((REAR_WHEEL_wl==0) || ((REAR_WHEEL_wl==1) && (vref>=VREF_30_KPH)))
                  #endif
                    {
	                    if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_110MS) {
	                        tempW7 = WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt;
	                        if((tempB1==1)&&(tempW7<=ARAD_0G25)) {WL->CAS_SUSPECT_flag = 1;}
	                        if((tempB1==2)&&(tempW7<=ARAD_0G5)) {WL->CAS_SUSPECT_flag = 1;}
	                    }
	                    if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_70MS) {
	                        tempW7 = WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt;
	                        if((tempB1==1)&&(tempW7<=ARAD_0G25)) {WL->CAS_SUSPECT_flag = 1;}
	                        if((tempB1==2)&&(tempW7<=ARAD_0G5)) {WL->CAS_SUSPECT_flag = 1;}                       
	                    }
                    }
                    else
                    {
                      #if __PRESS_DECEL_VREF_APPLY  
                        if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_140MS) {
                            tempW7 = (WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt)*4;
                            if((tempB1==1)&&(tempW7<=ARAD_0G5)) {WL->CAS_SUSPECT_flag = 1;}
                            if((tempB1==2)&&(tempW7<=ARAD_0G75)) {WL->CAS_SUSPECT_flag = 1;}                             
                        }
                        if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_110MS) {
                            tempW7 = (WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt)*4;
                            if((tempB1==1)&&(tempW7<=ARAD_0G5)) {WL->CAS_SUSPECT_flag = 1;}
                            if((tempB1==2)&&(tempW7<=ARAD_0G75)) {WL->CAS_SUSPECT_flag = 1;}                           
                        }
                      #else 
                        if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_140MS) {
                            tempW7 = (WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt)*4;
                            if((tempB1==1)&&(tempW7<=ARAD_0G75)) {WL->CAS_SUSPECT_flag = 1;}
                            if((tempB1==2)&&(tempW7<=ARAD_1G0)) {WL->CAS_SUSPECT_flag = 1;}  
                        }
                        if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_110MS) {
                            tempW7 = (WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt)*4;
                            if((tempB1==1)&&(tempW7<=ARAD_0G75)) {WL->CAS_SUSPECT_flag = 1;}
                            if((tempB1==2)&&(tempW7<=ARAD_1G0)) {WL->CAS_SUSPECT_flag = 1;}
                        }
                      #endif    
                    }
                }            
                if((FSF_wl==0)&&(WL->LFC_dump_counter>=10)&&(WL->max_arad_instbl<=ARAD_2G0)&&(WL->arad>0)) {WL->CAS_SUSPECT_flag = 1;}
                if(WL->CAS_SUSPECT_flag==1) 
                {
                    if ((FSF_wl==1)&&(REAR_WHEEL_wl==0))
                    {
                        WL->CAS_SUSPECT_reset_timer = 6;
                    }
                    else
                    {
                        WL->CAS_SUSPECT_reset_timer = 3;
                    }
                }
              #else  
                if((FSF_wl==0)&&(WL->arad>-ARAD_1G0)) {
                    if(WL->LFC_dump_counter>=2) {
                        WL->arad_gauge_cas2 = WL->arad_gauge_cas2 + WL->arad;
                        if(WL->arad_gauge_cas2_cnt<=S8_TYPE_MAXNUM) {WL->arad_gauge_cas2_cnt = WL->arad_gauge_cas2_cnt + 1;}
                    }
                    if(WL->LFC_dump_counter>=3) {
                        WL->arad_gauge_cas3 = WL->arad_gauge_cas3 + WL->arad;
                        if(WL->arad_gauge_cas3_cnt<=S8_TYPE_MAXNUM) {WL->arad_gauge_cas3_cnt  = WL->arad_gauge_cas3_cnt + 1;}
                    }

                  #if __REAR_D  
                    if(vref>=VREF_15_KPH)
                  #else
                    if((REAR_WHEEL_wl==0) || ((REAR_WHEEL_wl==1) && (vref>=VREF_30_KPH)))
                  #endif
                    {   
                    if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_110MS) {
                        tempW7 = WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt;
                        if(tempW7<=ARAD_0G25) {WL->CAS_SUSPECT_flag = 1;}
                    }
                    if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_70MS) {
                        tempW7 = WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt;
                        if(tempW7<=ARAD_0G25) {WL->CAS_SUSPECT_flag = 1;}
                    }
                    }
                    else
                    {
                      #if __PRESS_DECEL_VREF_APPLY  
                        if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_140MS) {
                            tempW7 = WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt*4;
                            if(tempW7<=ARAD_0G5) {WL->CAS_SUSPECT_flag = 1;}
                        }
                        if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_110MS) {
                            tempW7 = WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt*4;
                            if(tempW7<=ARAD_0G5) {WL->CAS_SUSPECT_flag = 1;}
                        }
                      #else 
                        if(WL->arad_gauge_cas2_cnt>=L_U8_TIME_10MSLOOP_140MS) {
                            tempW7 = WL->arad_gauge_cas2/(int16_t)WL->arad_gauge_cas2_cnt*4;
                            if(tempW7<=ARAD_0G75) {WL->CAS_SUSPECT_flag = 1;}
                        }
                        if(WL->arad_gauge_cas3_cnt>=L_U8_TIME_10MSLOOP_110MS) {
                            tempW7 = WL->arad_gauge_cas3/(int16_t)WL->arad_gauge_cas3_cnt*4;
                            if(tempW7<=ARAD_0G75) {WL->CAS_SUSPECT_flag = 1;}
                        }
                      #endif    
                    }
                }
                if((FSF_wl==0)&&(WL->LFC_dump_counter>=L_TIME_70MS)&&(WL->max_arad_instbl<=ARAD_2G0)&&(WL->arad>0)) {WL->CAS_SUSPECT_flag = 1;}
                if(WL->CAS_SUSPECT_flag==1) {WL->CAS_SUSPECT_reset_timer = 3;}
              #endif

            }
            else if(WL->arad>=ARAD_1G0) {
                WL->CAS_SUSPECT_reset_timer--;
                if((WL->CAS_SUSPECT_reset_timer==0) || (WL->max_arad_instbl>=ARAD_3G0)) {
                    WL->CAS_SUSPECT_flag = 0;
                    WL->arad_gauge_cas2 = 0;
                    WL->arad_gauge_cas3 = 0;
                    WL->arad_gauge_cas2_cnt = 0;
                    WL->arad_gauge_cas3_cnt = 0;
                }
            }
            else {}
          #endif
        }
    }
    else {
        if((WL->LFC_built_reapply_counter==L_1ST_SCAN)&&(WL->LFC_s0_reapply_counter==1)
          #if __VDC
            && (WL->LFC_to_ESP_end_flag==0)
          #endif
        )
        {
            if(WL->pos_arad_num>0)
            {
                WL->accel_arad_avg = WL->pos_arad_sum / (uint16_t)WL->pos_arad_num ;
            }
            else
            {
                WL->accel_arad_avg = 0; /* to prevent division by zero */
            }

          #if __BUMP_SUSPECT
            /* WL->LFC_Bump_Suspect_flag=0; move to below LCABS_vSetWheelDecelGauge()*/
            if(REAR_WHEEL_wl==0) {
                if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F1)||(WL->arad_gauge > S8_BMP_SUS_ARAD_GAUGE_THRES_F1)) {
                    /*if(WL->peak_slip<=-(int8_t)(LAM_40P)) {WL->LFC_Bump_Suspect_flag=1;} 2006.04.14 D.K.*/
                    WL->LFC_Bump_Suspect_flag=1;
                }
                else if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F2)||(WL->arad_gauge>=S8_BMP_SUS_ARAD_GAUGE_THRES_F2)) {
                    /*if(WL->peak_slip<=-(int8_t)(LAM_30P)) {*/
                        if((WL->LEFT_WHEEL==1) && (FR.LFC_Bump_Suspect_flag==1)) {WL->LFC_Bump_Suspect_flag=1;}
                        else if((WL->LEFT_WHEEL==0) && (FL.LFC_Bump_Suspect_flag==1)) {WL->LFC_Bump_Suspect_flag=1;}
                        else { }
                    /*}*/
                }
                else { }
            }
            else {
                if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_R1)||(WL->arad_gauge>=S8_BMP_SUS_ARAD_GAUGE_THRES_R1)) {
                    /*if(WL->peak_slip<=-(int8_t)(LAM_25P)) {WL->LFC_Bump_Suspect_flag=1;}*/
                    WL->LFC_Bump_Suspect_flag=1;
                }
                else if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_R2)||(WL->arad_gauge>=S8_BMP_SUS_ARAD_GAUGE_THRES_R2)) {
                    /*if(WL->peak_slip<=-(int8_t)(LAM_25P)) {*/
                        if((WL->LEFT_WHEEL==1) && (FL.LFC_Bump_Suspect_flag==1)) {WL->LFC_Bump_Suspect_flag=1;}
                        else if((WL->LEFT_WHEEL==0) && (FR.LFC_Bump_Suspect_flag==1)) {WL->LFC_Bump_Suspect_flag=1;}
                        else { }
                    /*}*/
                }
                else { }
            }
          #endif
        }
        
          #if __IMP_RISEDELAY_AND_DUMP_FOR_CAS
        if(vref<VREF_5_KPH)
        {
        	WL->CAS_SUSPECT_flag=0;
        	WL->CAS_SUSPECT_flag_old=0;
        }
          #endif        	
    }
}

  #if __UNSTABLE_REAPPLY_ADAPTATION
void LCABS_vAdaptdPControl(void)
{
    LCABS_vAdaptdPControlWL(&FL);
    LCABS_vAdaptdPControlWL(&FR);
    LCABS_vAdaptdPControlWL(&RL);
    LCABS_vAdaptdPControlWL(&RR);
  #if __REDUCE_1ST_UR_PRESS_DIFF
    LCABS_vURPerOppWheel(&FL,&FR);
    LCABS_vURPerOppWheel(&FR,&FL);
  #endif
}

void LCABS_vAdaptdPControlWL(struct W_STRUCT *UR_WL)
{
    WL=UR_WL;

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->Unstable_Rise_Pres_Cycle_flg=0;
        WL->Unstable_Rise_Pres_Cycle_time=0;
        WL->Unstable_Rise_Pres_Cycle_cnt=0;
/*      WL->Unstable_Rise_Adapt_flg=0; */
    }
    else
    {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif

            if(WL->Unstable_Rise_Pres_Cycle_flg==1)
            {
                if(AV_DUMP_wl==1)
                {
                    if(WL->Unstable_Rise_Pres_Cycle_time<L_TIME_150MS)
                    {
                        WL->Unstable_Rise_Pres_Cycle_cnt=WL->Unstable_Rise_Pres_Cycle_cnt+1;
                    }
                    else
                    {
                        WL->Unstable_Rise_Pres_Cycle_cnt=0;
                    }
                    WL->Unstable_Rise_Pres_Cycle_flg = 0;
                    WL->Unstable_Rise_Pres_Cycle_time = 0;
                }
                else
                {
                    if(WL->Unstable_Rise_Pres_Cycle_time < U8_TYPE_MAXNUM)
                    {
                        WL->Unstable_Rise_Pres_Cycle_time=WL->Unstable_Rise_Pres_Cycle_time+L_1SCAN;
                    }
                }
            }
            else if(WL->LFC_built_reapply_flag==1)
            {
                WL->Unstable_Rise_Pres_Cycle_cnt = 0;
            }
            else { }

      #if   __VDC && __ABS_COMB
        }
      #endif
    }
}

    #if __REDUCE_1ST_UR_PRESS_DIFF
static void LCABS_vURPerOppWheel(struct W_STRUCT *FI, const struct W_STRUCT *FO)
{
    FI->AdaptURPerOppositeWheel=0;

    if((FI->Reapply_Accel_cycle==0) && (FO->Reapply_Accel_cycle==1))
    {
        if((FI->LFC_zyklus_z<=0) && (FO->LFC_zyklus_z<=1))
        {
            if((FI->LFC_dump_counter>=5) && (FO->LFC_dump_counter>=5))
            {
                FI->AdaptURPerOppositeWheel=1;
            }
        }
    }
}
    #endif /*__REDUCE_1ST_UR_PRESS_DIFF*/
  #endif /*__UNSTABLE_REAPPLY_ADAPTATION*/

void LCABS_vDecideRoadWhVehStatus(void)
{

  #if __TEMP_for_CAS
    LCABS_vDecideCascadePropensity();
  #endif

  #if __AQUA_ENABLE
    LCABS_vDctAQUALOCK();                         /* determine aquaplanning */
  #endif

    LCABS_vDctLFCFictitiousCycle();


    LCABS_vDctDoubleBraking();


  #if __RESONANCE
    LCABS_vDctResonanceWheel();
  #endif

    LCABS_vDctH2LatHighSpeed();

    LCABS_vDctLFCBigRise();

  #if __INGEAR_VIB
    LCABS_vDctIngearVib();
  #endif

    LCABS_vDecideLFCCyclicComp();

    LCABS_vDecideLFCSpecialComp();

    LCABS_vDctL2H();

  #if __CHANGE_MU
    #if __VDC
        if((L_SLIP_CONTROL_fl==0)&&(L_SLIP_CONTROL_fr==0)) {LCABS_vDctLFCMuChange();}
        if((L_SLIP_CONTROL_rl==0)&&(L_SLIP_CONTROL_rr==0)) {LCABS_vCheckLFCBaseWheel();}
    #else
        LCABS_vDctLFCMuChange();
        LCABS_vCheckLFCBaseWheel();
    #endif
  #endif

  #if __SPLIT_VERIFY
    LCABS_vVerifySplitByFrontHSide(&FL);
    LCABS_vVerifySplitByFrontHSide(&FR);

  #if __VDC && (__MGH60_CYCLE_TIME_OPT_ABS==DISABLE)
    LCABS_vVerifyWheelMoment();
  #endif
  #endif

/*  LCABS_vCalcAbmittelfz();                            // calculate abmittel_fz, moved to signal processing */

  #if __UNSTABLE_REAPPLY_ADAPTATION
    LCABS_vAdaptdPControl();
  #endif

    LCABS_vDctABSatUnevenRoad();
    
  #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
    LCABS_vDctDriverInputToAccel();
  #endif  

  #if __DETECT_LOCK_IN_CYCLE
    LCABS_vDctPossibleLockInCycle();
  #endif

  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
    LCABS_vDctCertainHMu();
  #endif

}

#if __TEMP_for_CAS
void LCABS_vDecideCascadePropensity(void)
{
    NUM_OF_STABIL_WHEELS_old = NUM_OF_STABIL_WHEELS;
  #if   __VDC
    if((YAW_CDC_WORK==1) || (ESP_PULSE_DUMP_FLAG==1)) {
        NUM_OF_STABIL_WHEELS = 0;
/*
        if((SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FL_old==0) && (STABIL_fl==1)) {NUM_OF_STABIL_WHEELS += 1;}
        if((SLIP_CONTROL_NEED_FR==0) && (SLIP_CONTROL_NEED_FR_old==0) && (STABIL_fr==1)) {NUM_OF_STABIL_WHEELS += 1;}
        if((SLIP_CONTROL_NEED_RL==0) && (SLIP_CONTROL_NEED_RL_old==0) && (STABIL_rl==1)) {NUM_OF_STABIL_WHEELS += 1;}
        if((SLIP_CONTROL_NEED_RR==0) && (SLIP_CONTROL_NEED_RR_old==0) && (STABIL_rr==1)) {NUM_OF_STABIL_WHEELS += 1;}
*/
        tempB5 = 0;
        if((ESP_BRAKE_CONTROL_FL==0) && (state_fl==DETECTION)) {tempB5 += 1;}
        if((ESP_BRAKE_CONTROL_FR==0) && (state_fr==DETECTION)) {tempB5 += 1;}
        if((ESP_BRAKE_CONTROL_RL==0) && (state_rl==DETECTION)) {tempB5 += 1;}
        if((ESP_BRAKE_CONTROL_RR==0) && (state_rr==DETECTION)) {tempB5 += 1;}
        NUM_OF_STABIL_WHEELS = (uint8_t)tempB5; /* To reduce code size : reduce access frequency to PAGED RAM */

    }
    else {
        tempB5 = 0;
        if(state_fl==DETECTION) {tempB5 += 1;}
        if(state_fr==DETECTION) {tempB5 += 1;}
        if(state_rl==DETECTION) {tempB5 += 1;}
        if(state_rr==DETECTION) {tempB5 += 1;}
        NUM_OF_STABIL_WHEELS = (uint8_t)tempB5; /* To reduce code size : reduce access frequency to PAGED RAM */
    }

  #else
    NUM_OF_STABIL_WHEELS = 0;
    if(state_fl==DETECTION) {NUM_OF_STABIL_WHEELS += 1;}
    if(state_fr==DETECTION) {NUM_OF_STABIL_WHEELS += 1;}
    if(state_rl==DETECTION) {NUM_OF_STABIL_WHEELS += 1;}
    if(state_rr==DETECTION) {NUM_OF_STABIL_WHEELS += 1;}
  #endif

  #if __IMP_RISEDELAY_AND_DUMP_FOR_CAS
    if((WL->CAS_SUSPECT_flag_old==1)&&(WL->max_arad_instbl<ARAD_3G0)) {
    	tempW7 = (int16_t)U8_PULSE_MED_HIGH_MU; /*0.55g*/
    }
    else {
    	tempW7 = (int16_t)U8_PULSE_LOW_MED_MU;  /*0.3g*/
    }
    
    if((NUM_OF_STABIL_WHEELS_old == 1) && (NUM_OF_STABIL_WHEELS < 1)) {
      #if   __REAR_D
        if((AFZ_OK==1) && (afz >= -tempW7)) {
            if(rel_lam_fl < rel_lam_fr) {
                FR.dump_gain_mult_flag = 1;
                FL.dump_gain_mult_flag = 0;
            }
            else {
                FL.dump_gain_mult_flag = 1;
                FR.dump_gain_mult_flag = 0;
            }
        }
      #else
        if((AFZ_OK==1) && (afz >= -tempW7)) {
            if(rel_lam_rl < rel_lam_rr) {
                RR.dump_gain_mult_flag = 1;
                RL.dump_gain_mult_flag = 0;
            }
            else {
                RL.dump_gain_mult_flag = 1;
                RR.dump_gain_mult_flag = 0;
            }
        }
      #endif
    }    
  #else  /* __IMP_RISEDELAY_AND_DUMP_FOR_CAS */
    if((NUM_OF_STABIL_WHEELS_old == 1) && (NUM_OF_STABIL_WHEELS < 1)) {
      #if   __REAR_D
        if((AFZ_OK==1) && (afz >= -(int16_t)U8_PULSE_LOW_MU)) {
            if(rel_lam_fl < rel_lam_fr) {
                FR.dump_gain_mult_flag = 1;
                FL.dump_gain_mult_flag = 0;
            }
            else {
                FL.dump_gain_mult_flag = 1;
                FR.dump_gain_mult_flag = 0;
            }
        }
      #else
        if((AFZ_OK==1) && (afz >= -(int16_t)U8_PULSE_LOW_MED_MU)) {
            if(rel_lam_rl < rel_lam_rr) {
                RR.dump_gain_mult_flag = 1;
                RL.dump_gain_mult_flag = 0;
            }
            else {
                RL.dump_gain_mult_flag = 1;
                RR.dump_gain_mult_flag = 0;
            }
        }
      #endif
    }
  #endif
  
  #if   __TEMP_for_CAS2
    else if((vref_limit_dec_flag==1) && (vref>=VREF_20_KPH)) {
        if((AFZ_OK==1) && (afz > -(int16_t)U8_HIGH_MU)) {  /*U8_PULSE_HIGH_MU->U8_HIGH_MU  below -0.7g->-0.55g */
          #if   __REAR_D
/*
            if((STABIL_fl==0)&&(STABIL_fr==0)) {
                if(rel_lam_fl < rel_lam_fr) {
                    if(FSF_fr==0) {FR.dump_gain_mult_flag2 = 1;}
                    FL.dump_gain_mult_flag2 = 0;
                }
                else {
                    if(FSF_fl==0) {FL.dump_gain_mult_flag2 = 1;}
                    FR.dump_gain_mult_flag2 = 0;
                }
            }
            else if((STABIL_fl==1)&&(STABIL_fr==0)) {
                if(rel_lam_fl < rel_lam_fr) {
                    if(FSF_fr==0) {FR.dump_gain_mult_flag2 = 1;}
                    FL.dump_gain_mult_flag2 = 0;
                }
            }
            else if((STABIL_fl==0)&&(STABIL_fr==1)) {
                if(rel_lam_fl > rel_lam_fr) {
                    if(FSF_fl==0) {FL.dump_gain_mult_flag2 = 1;}
                    FR.dump_gain_mult_flag2 = 0;
                }
            }
            else { }
*/
            if(rel_lam_fl < rel_lam_fr) {
                if(STABIL_fr==0) {
                    if(FSF_fr==0) {FR.dump_gain_mult_flag2 = 1;}
                    FL.dump_gain_mult_flag2 = 0;
                }
                else { }
            }
            else {
                if(STABIL_fl==0) {
                    if(FSF_fl==0) {FL.dump_gain_mult_flag2 = 1;}
                    FR.dump_gain_mult_flag2 = 0;
                }
                else { }
            }

          #else
/*
            if((STABIL_rl==0)&&(STABIL_rr==0)) {
                if(rel_lam_rl < rel_lam_rr) {
                    if(FSF_rr==0) {RR.dump_gain_mult_flag2 = 1;}
                    RL.dump_gain_mult_flag2 = 0;
                }
                else {
                    if(FSF_rl==0) {RL.dump_gain_mult_flag2 = 1;}
                    RR.dump_gain_mult_flag2 = 0;
                }
            }
            else if((STABIL_rl==1)&&(STABIL_rr==0)) {
                if(rel_lam_rl < rel_lam_rr) {
                    if(FSF_rr==0) {RR.dump_gain_mult_flag2 = 1;}
                    RL.dump_gain_mult_flag2 = 0;
                }
            }
            else if((STABIL_rl==0)&&(STABIL_rr==1)) {
                if(rel_lam_rl > rel_lam_rr) {
                    if(FSF_rl==0) {RL.dump_gain_mult_flag2 = 1;}
                    RR.dump_gain_mult_flag2 = 0;
                }
            }
            else { }
*/
            if(rel_lam_rl < rel_lam_rr) {
                if(STABIL_rr==0) {
                    if(FSF_rr==0) {RR.dump_gain_mult_flag2 = 1;}
                    RL.dump_gain_mult_flag2 = 0;
                }
                else { }
            }
            else {
                if(STABIL_rl==0) {
                    if(FSF_rl==0) {RL.dump_gain_mult_flag2 = 1;}
                    RR.dump_gain_mult_flag2 = 0;
                }
                else { }
            }

          #endif
        }
    }
    else if(vref_limit_dec_flag==0) {
      #if   __REAR_D
        FL.dump_gain_mult_flag2 = 0;
        FR.dump_gain_mult_flag2 = 0;
      #else
        RL.dump_gain_mult_flag2 = 0;
        RR.dump_gain_mult_flag2 = 0;
      #endif
    }
    else { }
  #endif

  #if   __REAR_D
    if(STABIL_fl==1) {FL.dump_gain_mult_flag = 0;}
    if(STABIL_fr==1) {FR.dump_gain_mult_flag = 0;}
   #if  __TEMP_for_CAS2
    if(STABIL_rl==1) {RL.dump_gain_mult_flag2 = 0;}
    if(STABIL_rr==1) {RR.dump_gain_mult_flag2 = 0;}
   #endif
  #else
    if(STABIL_rl==1) {
        RL.dump_gain_mult_flag = 0;
      #if   __TEMP_for_CAS2
        RL.dump_gain_mult_flag2 = 0;
      #endif
    }
    if(STABIL_rr==1) {
        RR.dump_gain_mult_flag = 0;
      #if   __TEMP_for_CAS2
        RR.dump_gain_mult_flag2 = 0;
      #endif
    }
  #endif
/*    NUM_OF_STABIL_WHEELS_old = ((uint8_t)STABIL_fl + (uint8_t)STABIL_fr + (uint8_t)STABIL_rl + (uint8_t)STABIL_rr); */

  #if __LOWMU_REAR_1stCYCLE_RISE_DELAY
    if((ABS_fz==0) || (LFC_Split_flag==1) || (LFC_Split_suspect_flag==1) || (BEND_DETECT2==1) || ((AFZ_OK==1) && (afz < -(int16_t)U8_LOW_MU)))
    {
    	lcabsu1Rear1stCycleRiseDelay = 0;
    	lcabsu8LowMuTendencyGauge = 0;
	}
    else
    {
        if((RL.LFC_zyklus_z<1) || (RR.LFC_zyklus_z<1))
        {
        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&FL, lcabsu8LowMuTendencyGauge);
        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&FR, lcabsu8LowMuTendencyGauge);
        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&RL, lcabsu8LowMuTendencyGauge);
        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&RR, lcabsu8LowMuTendencyGauge);
        }
        else if((RL.LFC_zyklus_z==1) && (RR.LFC_zyklus_z==1))
        {   
        	if(lcabsu8LowMuTendencyGauge > 0)
        	{
	        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&RL, lcabsu8LowMuTendencyGauge);
	        	lcabsu8LowMuTendencyGauge = LCABS_vCountLowMuWheelTendency(&RR, lcabsu8LowMuTendencyGauge);
	        	
	        	if((FL.FSF==1) && (FL.max_arad_instbl<=ARAD_5G0)) {lcabsu8LowMuTendencyGauge = lcabsu8LowMuTendencyGauge + 10;}
	        	if((FR.FSF==1) && (FR.max_arad_instbl<=ARAD_5G0)) {lcabsu8LowMuTendencyGauge = lcabsu8LowMuTendencyGauge + 10;}

	        	if(lcabsu8LowMuTendencyGauge >= 20)
	        	{
	        		lcabsu1Rear1stCycleRiseDelay = 1;
	        	}
        		lcabsu8LowMuTendencyGauge = 0;
	        }        	
        }
        else
        {
        	lcabsu1Rear1stCycleRiseDelay = 0;
        	lcabsu8LowMuTendencyGauge = 0;
        }
    }
  #endif /*__LOWMU_REAR_1stCYCLE_RISE_DELAY*/


  #if __SELECT_LOW_FOR_LMU
    if((AFZ_OK==1)&&(afz>-(int16_t)U8_HOMO_MU_SL_ALLOW_AFZ)&&(vref>S16_HOMO_MU_SL_ALLOW_SPEED))
    {
        lcabsu1SelectLowforNonSplit=1; /* Select-low for non-split road */
    }
    else
    {
        lcabsu1SelectLowforNonSplit=0;
    }
  #endif

}
  #if __LOWMU_REAR_1stCYCLE_RISE_DELAY
uint8_t LCABS_vCountLowMuWheelTendency(struct W_STRUCT *WLtemp, uint8_t lcabsu8LowMuTendencyGaugeOld)
{
	WL = WLtemp;
		
	if((WL->STAB_K==1) && (WL->zyklus_z<=1) && (FSF_wl==0))
	{
		if(WL->LOW_MU_SUSPECT_by_IndexMu==1) {tempB7 = 10;}
		else if(WL->IndexMu<=IndexBasalt) {tempB7 = 7;}
		else {tempB7 = 0;}
		
		if((WL->LFC_UNSTABIL_counter < L_SCAN(30)) || (WL->max_arad_instbl>ARAD_5G0)) 
		{
			tempB7 = (int8_t)((int16_t)tempB7/2);
		}
	}
	else
	{
		tempB7 = 0;
	}
	
	return (lcabsu8LowMuTendencyGaugeOld + (uint8_t)tempB7);	
}
  #endif
#endif

  #if __AQUA_ENABLE
void LCABS_vDctAQUALOCK(void)
{
    if(state_fl == DETECTION) {av_t_aqua_fl=0;}
    else {
        if(AFZ_OK==1) {
            if(afz <= -(int16_t)U8_LOW_MU) {
                if(AV_DUMP_fl==1) {
                    if(av_t_aqua_fl < (uint8_t)T_AQUA_LIMIT) {av_t_aqua_fl++;}
                }
            }
        }
    }

    if(state_fr == DETECTION) {av_t_aqua_fr=0;}
    else {
        if(AFZ_OK==1) {
            if(afz <= -(int16_t)U8_LOW_MU) {
                if(AV_DUMP_fr==1) {
                    if(av_t_aqua_fr < (uint8_t)T_AQUA_LIMIT) {av_t_aqua_fr++;}
                }
            }
        }
    }

    if((SLIP_20P_NEG_fl==1)||(SLIP_20P_NEG_fr==1)) {
        if(V_INST_80==1) {
            if(av_t_aqua_fl >= (uint8_t)T_AQUA) {
                if(av_t_aqua_fr >= (uint8_t)T_AQUA) {AQUA_END=1;}
            }
        }
    }
    else {AQUA_END=0;}
}
  #endif


/*-------------------------------------------------------------------------------------*/
/*  Programed by J.K.Lee at 03 SW winter test                                          */
/*-------------------------------------------------------------------------------------*/
/*  Function to prevent from incorrect compensation due to fictitious cycle            */
/*-------------------------------------------------------------------------------------*/
void LCABS_vDctLFCFictitiousCycle(void)
{
    LCABS_vDctLFCFictitiousCycleWL(&FL);
    LCABS_vDctLFCFictitiousCycleWL(&FR);
    LCABS_vDctLFCFictitiousCycleWL(&RL);
    LCABS_vDctLFCFictitiousCycleWL(&RR);
}

void LCABS_vDctLFCFictitiousCycleWL(struct W_STRUCT *WL)
{
    if((FSF_wl==1)&&(ABS_fz==0)) {
        WL->LFC_fictitious_cycle_flag = 0;
        WL->LFC_fictitious_cycle_flag2 = 0;
    }
    else {
        if((STAB_A_wl==1) || (WL->SL_Dump_Start==1)) {
            if((AFZ_OK == 1)&&(afz <= -(int16_t)U8_LOW_MU)) {
                if(afz<=-(int16_t)U8_HIGH_MU) {      /* High mu, afz < -0.55g */
                    tempC1 = 2;
                    tempC2 = 2;
                }
                else {
                    tempC1 = 3;
                    tempC2 = 2;
                }
            }
            else {
                tempC1 = 5;
                tempC2 = 3;
            }

          #if __UNSTABLE_REAPPLY_ENABLE
            if((WL->Reapply_Accel_cycle_old==1) &&
             (((REAR_WHEEL_wl==1)&&(WL->peak_slip<-LAM_20P)) ||
              ((REAR_WHEEL_wl==0)&&(WL->peak_slip<-LAM_15P))))
            {
                tempC1 = 2;
                tempC2 = 2;
            }
          #endif
          
          #if   !__REAR_D
            if(REAR_WHEEL_wl==0)
          #else
            if(REAR_WHEEL_wl==1)
          #endif
            {
                if(WL->LFC_s0_reapply_counter_old < tempC1) {
                    if(WL->LFC_zyklus_z==1) {WL->LFC_fictitious_cycle_flag = 1;}
                    else {
                        if((WL->LFC_zyklus_z<=2) && (WL->LFC_fictitious_cycle_flag==1)) {
                            WL->LFC_fictitious_cycle_flag = 1;
                        }
                        else {WL->LFC_fictitious_cycle_flag = 0;}
                    }
                }
                else {
                    WL->LFC_fictitious_cycle_flag = 0;
                }
            }
            else {WL->LFC_fictitious_cycle_flag = 0;}

            if(REAR_WHEEL_wl==0) {
                if(WL->LFC_s0_reapply_counter_old < tempC2) {
                    if(WL->LFC_fictitious_cycle_flag == 0) {WL->LFC_fictitious_cycle_flag2 = 1;}
                    else {WL->LFC_fictitious_cycle_flag2 = 0;}
                }
                else {WL->LFC_fictitious_cycle_flag2 = 0;}
            }
            else {WL->LFC_fictitious_cycle_flag2 = 0;}
        }
    }
}

void LCABS_vDctDoubleBraking(void)
{
    if((STABIL_fl==1)&&(STABIL_fr==1)&&(STABIL_rl==1)&&(STABIL_rr==1)) {
        if((FL.rel_lam>-LAM_2P)&&(FR.rel_lam>-LAM_2P)&&(RL.rel_lam>-LAM_2P)&&(RR.rel_lam>-LAM_2P)) {
            if(double_counter<U8_TYPE_MAXNUM) {double_counter = double_counter + L_1SCAN;}
        }
        else {
            if((FL.rel_lam<-LAM_2P)||(FR.rel_lam<-LAM_2P)||(RL.rel_lam<-LAM_2P)||(RR.rel_lam<-LAM_2P)) {
                double_counter=0;
            }
        }
    }
    else {double_counter=0;}

/*  if((AFZ_OK==0)||((AFZ_OK==1)&&(afz<=-(int16_t)U8_HIGH_MU))) {*/
    if(((AFZ_OK==0)||((AFZ_OK==1)&&(afz<=-(int16_t)(AFZ_0G4))))&&(BEND_DETECT2==0)) {
        if(aref_avg<=-(int16_t)(AFZ_0G7)) {
            if(((aref_avg-aref0)<=-(int16_t)(AFZ_0G4))&&(aref0<=0)){
                Double_suspect=1;
            }
        }
        else if(aref_avg<=-(int16_t)(AFZ_0G3)) {
            if(((aref_avg-aref0)<=-(int16_t)(AFZ_0G3))&&(aref0<=0)) {
                Double_suspect=1;
            }
        }
        else { }

        if(aref_avg > aref0) {
            Double_suspect=0;
        }

        if((ABS_fz==1)&&(double_counter>=L_TIME_150MS)&&(Double_suspect==1)&&(vref>=VREF_3_KPH)) {
            FL.Check_double_brake_flag2=1;
            FR.Check_double_brake_flag2=1;
            RL.Check_double_brake_flag2=1;
            RR.Check_double_brake_flag2=1;
/*            Double_brake_flag=1; */
        }

        if((EBD_RA==1)&&(double_counter>=L_TIME_150MS)
          &&(Double_suspect==1)&&(vref>=VREF_10_KPH))
        {
            lcebdu1DoubleBrake=1;
        }
        else
        {
            lcebdu1DoubleBrake=0;
        }
    }

  /* braking state depending on mpress release  */
  #if __VDC
    LCABS_vDctMpressRelease();
  #endif
}

#if __VDC
void LCABS_vDctMpressRelease(void)
{
	static int16_t lcabss16MPBwLowestSkidCnt = 0;
    int16_t s16LowestABSSkidPress = 0;
#if (__IDB_LOGIC==ENABLE)
 #if __REF_P_RESOL_CHANGED_10_TO_100 == ENABLE
	int16_t lcabss16RefTargetPres = S16_PRESS_RESOL_100_TO_10(lcabss16RefFinalTargetPres);
 #else
	int16_t lcabss16RefTargetPres = lcabss16RefFinalTargetPres;
 #endif
#else
 #if __REF_P_RESOL_CHANGED_10_TO_100 == ENABLE
	int16_t lcabss16RefTargetPres = S16_PRESS_RESOL_100_TO_10(lcabss16RefMpress);
 #else
	int16_t lcabss16RefTargetPres = lcabss16RefMpress;
 #endif
#endif
    /*if(((mp_hw_suspcs_flg==0)&&(mp_sw_err_flg==1)) && (MPRESS_BRAKE_ON==1))*/
    MP_RELEASE_BW_4BAR_SCAN = 0;
    MP_RELEASE_BW_6BAR_SCAN = 0;
    MP_RELEASE_BW_10BAR_SCAN = 0;
    MP_RELEASE_BW_SKID_SCAN = 0;

    if(ABS_fz==1)
    {
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(ABS_fl,FL.Estimated_Press_SKID,s16LowestABSSkidPress);
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(ABS_fr,FR.Estimated_Press_SKID,s16LowestABSSkidPress);
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(ABS_rl,RL.Estimated_Press_SKID,s16LowestABSSkidPress);
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(ABS_rr,RR.Estimated_Press_SKID,s16LowestABSSkidPress);
    }
	else if(EBD_RA==1)
	{
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(EBD_rl,RL.s16_Estimated_Active_Press,s16LowestABSSkidPress);
		s16LowestABSSkidPress = LCABS_s16GetLowestValue(EBD_rr,RR.s16_Estimated_Active_Press,s16LowestABSSkidPress);
	}
    else
    {
    	s16LowestABSSkidPress = lcabss16RefTargetPres;
    }

    if(lcabsu1ValidBrakeSensor==1)
    {
    	if(lcabss16RefTargetPres < s16LowestABSSkidPress)
    	{
    		if(lcabss16MPBwLowestSkidCnt < L_TIME_100MS)
    		{
    			lcabss16MPBwLowestSkidCnt = lcabss16MPBwLowestSkidCnt + L_1SCAN; /* 7scan */
    		}
    		else
    		{
    			lcabss16MPBwLowestSkidCnt = L_TIME_100MS;
    		}
    	}
    	else
    	{
    		if(lcabss16MPBwLowestSkidCnt > L_1SCAN)
    		{
    			lcabss16MPBwLowestSkidCnt = lcabss16MPBwLowestSkidCnt - L_1SCAN;
    		}
    		else
    		{
    			lcabss16MPBwLowestSkidCnt = 0;
    		}
    	}

        if(lcabss16RefTargetPres < MPRESS_10BAR)
        {
            MP_release_cnt_bw_10bar=MP_release_cnt_bw_10bar + L_1SCAN; /* 7scan */

            if(lcabss16RefTargetPres < MPRESS_6BAR)
            {
                MP_release_cnt_bw_6bar=MP_release_cnt_bw_6bar + L_1SCAN; /* 7scan */

                if(lcabss16RefTargetPres < MPRESS_4BAR)
                {
                    MP_release_cnt_bw_4bar=MP_release_cnt_bw_4bar + L_1SCAN; /* 7scan */
                }
                else
                {
                    MP_release_cnt_bw_4bar=MP_release_cnt_bw_4bar - L_1SCAN;
                }
            }
            else
            {
                MP_release_cnt_bw_6bar=MP_release_cnt_bw_6bar - L_1SCAN;
                MP_release_cnt_bw_4bar=MP_release_cnt_bw_4bar - L_1SCAN;
            }
        }
        else
        {
            MP_release_cnt_bw_10bar=MP_release_cnt_bw_10bar - L_1SCAN;
            MP_release_cnt_bw_6bar=MP_release_cnt_bw_6bar - L_1SCAN;
            MP_release_cnt_bw_4bar=MP_release_cnt_bw_4bar -L_1SCAN;
        }

      /*  limitation  */
        if(MP_release_cnt_bw_4bar < 0)
        {
            MP_release_cnt_bw_4bar = 0;
        }
        else if(MP_release_cnt_bw_4bar > L_TIME_70MS)
        {
            MP_release_cnt_bw_4bar = L_TIME_70MS;
        }
        else {}

        if(MP_release_cnt_bw_6bar < 0)
        {
            MP_release_cnt_bw_6bar = 0;
        }
        else if(MP_release_cnt_bw_6bar > L_TIME_70MS)
        {
            MP_release_cnt_bw_6bar = L_TIME_70MS;
        }
        else {}

        if(MP_release_cnt_bw_10bar < 0)
        {
            MP_release_cnt_bw_10bar = 0;
        }
        else if(MP_release_cnt_bw_10bar > L_TIME_70MS)
        {
            MP_release_cnt_bw_10bar = L_TIME_70MS;
        }
        else {}

      /***************/
      /*   outputs   */
        if(MP_RELEASE_BW_10BAR==0)
        {
            if(MP_release_cnt_bw_10bar >= L_TIME_50MS)
            {
                MP_RELEASE_BW_10BAR=1;
                MP_RELEASE_BW_10BAR_SCAN=1;
            }
            else {}
        }
        else
        {
            if(MP_release_cnt_bw_10bar <= L_TIME_20MS)
            {
                MP_RELEASE_BW_10BAR=0;
            }
            else {}
        }

        if(MP_RELEASE_BW_6BAR==0)
        {
            if(MP_release_cnt_bw_6bar >= L_TIME_50MS)
            {
                MP_RELEASE_BW_6BAR=1;
                MP_RELEASE_BW_6BAR_SCAN = 1;
            }
            else {}
        }
        else
        {
            if(MP_release_cnt_bw_6bar <=L_TIME_20MS)
            {
                MP_RELEASE_BW_6BAR=0;
            }
            else {}
        }

        if(MP_RELEASE_BW_4BAR==0)
        {
            if(MP_release_cnt_bw_4bar >= L_TIME_50MS)
            {
                MP_RELEASE_BW_4BAR=1;
                MP_RELEASE_BW_4BAR_SCAN = 1;
            }
            else {}
        }
        else
        {
            if(MP_release_cnt_bw_4bar <= L_TIME_20MS)
            {
                MP_RELEASE_BW_4BAR=0;
            }
            else {}
        }

        if(MP_RELEASE_BW_SKID==0)
        {
            if(lcabss16MPBwLowestSkidCnt > L_TIME_50MS)
            {
            	MP_RELEASE_BW_SKID = 1;
            	MP_RELEASE_BW_SKID_SCAN = 1;
            }
            else
            {
            	;
            }
        }
        else
        {
            if(lcabss16MPBwLowestSkidCnt <= L_TIME_20MS)
            {
            	MP_RELEASE_BW_SKID=0;
            }
            else {}
        }
       /***************/
    }
    else
    {
       /* reset at sensor error */
        MP_release_cnt_bw_4bar=0;
        MP_release_cnt_bw_6bar=0;
        MP_release_cnt_bw_10bar=0;
        lcabss16MPBwLowestSkidCnt = 0;

        MP_RELEASE_BW_4BAR=0;
        MP_RELEASE_BW_6BAR=0;
        MP_RELEASE_BW_10BAR=0;
        MP_RELEASE_BW_SKID = 0;
       /*************************/
    }

    LCABS_vDctBrkPedlModulation();
}
#endif

static void LCABS_vDctBrkPedlModulation(void)
{
	static int16_t lcabss16PedalReleaseCnt = 0;
	static int16_t lcabss16PedalApplyCnt   = 0;
	int16_t s16RefMpressDiff = 0;
	uint8_t DriverPedalRelSetScan = 0;
	uint8_t DriverPedalApplySetScan = 0;
	DriverPedalRelSetScan = 0;
	DriverPedalApplySetScan = 0;

	if(lcabsu1ValidBrakeSensor==1)
	{
		 #if __REF_P_RESOL_CHANGED_10_TO_100 == ENABLE
		s16RefMpressDiff = S16_PRESS_RESOL_100_TO_10(lcabss16RefMpress - lcabss16RefMpressOld);
		 #else
		s16RefMpressDiff = lcabss16RefMpress - lcabss16RefMpressOld;
		 #endif

		if(s16RefMpressDiff < MPRESS_0BAR)
		{
			if(s16RefMpressDiff < -MPRESS_3BAR)
			{
				lcabss16PedalReleaseCnt += L_4SCAN;
				lcabss16PedalApplyCnt   -= L_6SCAN;
			}
			else if(s16RefMpressDiff < -MPRESS_1BAR)
			{
				lcabss16PedalReleaseCnt += L_2SCAN;
				lcabss16PedalApplyCnt   -= L_4SCAN;
			}
			else
			{
				/*lcabss16PedalReleaseCnt += L_1SCAN;*/
			    lcabss16PedalApplyCnt   -= L_2SCAN;
			}
		}
		else
		{
			if(s16RefMpressDiff > MPRESS_3BAR)
			{
				lcabss16PedalReleaseCnt -= L_4SCAN;
				lcabss16PedalApplyCnt   += L_5SCAN;
			}
			else if(s16RefMpressDiff > MPRESS_1BAR)
			{
				lcabss16PedalReleaseCnt -= L_2SCAN;
				lcabss16PedalApplyCnt   += L_4SCAN;
			}
			else
			{
				lcabss16PedalReleaseCnt -= L_1SCAN;
			    /*hysteresis*/
				/*
				lcabss16PedalApplyCnt   += L_1SCAN;
				*/
			}
		}

		if(lcabsu1DriverPedalRel==0)
		{
			if(lcabss16PedalReleaseCnt >= L_TIME_100MS)
			{
			    DriverPedalRelSetScan = 1;
				lcabsu1DriverPedalRel = 1;
				lcabss16PedalReleaseCnt = L_TIME_100MS;
			}
			else if(lcabss16PedalReleaseCnt <= L_TIME_0MS)
			{
				lcabss16PedalReleaseCnt = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			if(lcabss16PedalReleaseCnt <= L_TIME_0MS)
			{
				lcabsu1DriverPedalRel = 0;
				lcabss16PedalReleaseCnt = 0;
			}
			else if(lcabss16PedalReleaseCnt >= L_TIME_100MS)
			{
				lcabss16PedalReleaseCnt = L_TIME_100MS;
			}
			else
			{
				;
			}
		}

		if(lcabsu1DriverPedalApp==0)
		{
			if(lcabss16PedalApplyCnt >= L_TIME_100MS)
			{
				DriverPedalApplySetScan = 1;
				lcabsu1DriverPedalApp = 1;
				lcabss16PedalApplyCnt = L_TIME_100MS;
			}
			else if(lcabss16PedalApplyCnt <= L_TIME_0MS)
			{
				lcabss16PedalApplyCnt = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			if(lcabss16PedalApplyCnt <= L_TIME_0MS)
			{
				lcabsu1DriverPedalApp = 0;
				lcabss16PedalApplyCnt = 0;
			}
			else if(lcabss16PedalApplyCnt >= L_TIME_100MS)
			{
				lcabss16PedalApplyCnt = L_TIME_100MS;
			}
			else
			{
				;
			}
		}
		
		if(ABS_fz==1)
		{
			if(DriverPedalRelSetScan==1)
			{
				lcabsu1BrkPedalRelDuringABS = 1;
			}

			if(lcabsu1BrkPedalRelDuringABS==1)
			{
				if((FL.state==DETECTION)&&(FR.state==DETECTION))
				{
					if(lcabsu1DriverPedalReapply == 1)
					{
						lcabsu1DriverPedalReapply = 0;
					}
					else
					{
						if(DriverPedalApplySetScan==1)
						{
							lcabsu1DriverPedalReapply = 1;
							lcabsu1BrkPedalRelDuringABS = 0;
						}
						else {}
					}
				}
				else
				{
					lcabsu1DriverPedalReapply=0;
				}
			}
			else if(lcabsu1DriverPedalReapply == 1)
			{
				if((FL.state==DETECTION)&&(FR.state==DETECTION))
				{
					;
				}
				else
				{
					lcabsu1DriverPedalReapply = 0;
				}
			}
			else {}
		}
		else
		{
			lcabsu1BrkPedalRelDuringABS = 0;
			lcabsu1DriverPedalReapply = 0;
		}
		
		
	}
	else
	{
		lcabsu1DriverPedalRel = 0;
		lcabss16PedalReleaseCnt = 0;
		lcabsu1BrkPedalRelDuringABS = 0;
		lcabss16PedalApplyCnt = 0;
		lcabsu1DriverPedalApp = 0;
		lcabsu1DriverPedalReapply = 0;
	}
}


static int16_t LCABS_s16GetLowestValue(uint8_t Condition, int16_t s16Value1, int16_t s16Value2)
{
	if(Condition==TRUE)
	{
		if(s16Value2 > 0)
		{
			if(s16Value1 > 0)
			{
				if(s16Value1 < s16Value2)
				{
					s16Value2 = s16Value1;
				}
				else {}
			}
			else {}
		}
		else
		{
			if(s16Value1 > 0)
			{
				s16Value2 = s16Value1;
			}
			else {}
		}
	}
	else {}

	return s16Value2;
}

#if __RESONANCE

/*******************************************************************Rezzo Resonance*/
void LCABS_vDctResonanceWheel(void)
{
    LCABS_vDctResonanceWheelWL(&FL);
    LCABS_vDctResonanceWheelWL(&FR);
    LCABS_vDctResonanceWheelWL(&RL);
    LCABS_vDctResonanceWheelWL(&RR);
}

void LCABS_vDctResonanceWheelWL(struct W_STRUCT *WL)
{
    if(((FSF_wl==1)&&(ABS_fz==0)) || (REAR_WHEEL_wl==1)) {
        WL->Cycle_frq_cnt=0;                /* Added by KGY 030623 */
        WL->Resonance_counter=0;
        WL->Shift_thresdec_flag=0;
    }
    else {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
        if((WL->LFC_built_reapply_counter==L_1ST_SCAN)&&(WL->LFC_s0_reapply_counter==1)) {
            if((AFZ_OK==1)&&(afz>=-(int16_t)U8_LOW_MU)) {
                if(vref <= VREF_30_KPH) {
                    if(vref <= VREF_10_KPH) {tempB7=-(int8_t)(LAM_35P);}
                    else if(vref <= VREF_20_KPH) {tempB7=-(int8_t)(LAM_30P);}
                    else {tempB7=-(int8_t)(LAM_25P);}
                    if((WL->Cycle_frq_cnt<=20)&&(WL->peak_slip>tempB7)) {
                        WL->Resonance_counter++;
                    }
                    else {
                        WL->Resonance_counter=0;
                    }
                }
            }
            WL->Cycle_frq_cnt=1;

            if(WL->Resonance_counter>=2) {
                WL->Shift_thresdec_flag=1;
            }
            else {WL->Shift_thresdec_flag=0;}
        }
        else {
            if(WL->Cycle_frq_cnt<=200) {WL->Cycle_frq_cnt++;}
        }
      #if   __VDC && __ABS_COMB
        }
      #endif
    }
}
/**************************************************************************************/
#endif /*__RESONANCE*/


void LCABS_vDctH2LatHighSpeed(void)
{
    LCABS_vDctH2LatHighSpeedWL(&FL);
    LCABS_vDctH2LatHighSpeedWL(&FR);
}

void LCABS_vDctH2LatHighSpeedWL(struct  W_STRUCT *WL)
{
    if(((FSF_wl==1)&&(ABS_fz==0)) || (REAR_WHEEL_wl==1)) {
        WL->High_TO_Low_Suspect_flag=0;
    }
    else {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif

        if((vref>=VREF_80_KPH)&&(STABIL_wl==0)) {
            if((WL->LFC_UNSTABIL_counter>=(uint8_t)U16_LFC_H2L_UNSTABIL_TIME_H_V) && (WL->LFC_dump_counter>=U8_LFC_H2L_DUMP_H_V)) {
                if((AFZ_OK==1) && (afz<=-(int16_t)U8_HIGH_MU)) {WL->High_TO_Low_Suspect_flag=1;}
            }
        }

        if(WL->High_TO_Low_Suspect_flag==1) {
            if(STABIL_wl==1) {
                if(afz>=-(int16_t)U8_LOW_MU) {WL->High_TO_Low_Suspect_flag=0;}
                else if(afz>=-(int16_t)U8_HIGH_MU) {
                    if((WL->LFC_UNSTABIL_counter<=U8_LFC_MED_UNSTABIL_TIME) &&  (WL->LFC_dump_counter<=U8_LFC_MED_DUMP)) {
                        if(WL->max_arad_instbl>=S8_LFC_MED_PEAK_ACCEL) {WL->High_TO_Low_Suspect_flag=0;}
                    }
                }
                else {
                    if((WL->LFC_UNSTABIL_counter<=U8_LFC_HIGH_UNSTABIL_TIME) && (WL->LFC_dump_counter<=U8_LFC_HIGH_DUMP))   {
                        if(WL->max_arad_instbl>=S8_LFC_HIGH_PEAK_ACCEL) {WL->High_TO_Low_Suspect_flag=0;}
                    }
                }
            }
            if(WL->arad>=ARAD_10G0) {WL->High_TO_Low_Suspect_flag=0;}
        }
      #if   __VDC && __ABS_COMB
        }
      #endif
    }
}

void LCABS_vDctLFCBigRise(void)
{
    LCABS_vDctLFCBigRiseWL(&FL, &FR);
    LCABS_vDctLFCBigRiseWL(&FR, &FL);
}

void LCABS_vDctLFCBigRiseWL(struct W_STRUCT *pF1, struct W_STRUCT *pF2)
{
    tempF0=0;
    if((pF1->LFC_built_reapply_flag==1)&&(vref>=VREF_15_KPH)) {
        if((AFZ_OK==1)&&(afz<=-(int16_t)U8_HIGH_MU)) {
            if(pF1->LFC_zyklus_z<=1) {
                tempC1=7;
                tempC2=12;
                tempW0=5;
            }
            else {
                tempC1=5;
                tempC2=10;
                tempW0=3;
            }
            if(pF1->LFC_dump_counter>=(uint8_t)tempW0) {
                if(pF1->LFC_s0_reapply_counter>=tempC2) {pF1->LFC_Big_Rise_detect = 0;}
                else if(pF1->LFC_s0_reapply_counter>=tempC1) {
                    pF1->LFC_Big_Rise_detect = 1;
                    tempF0=1;
                }
                else {pF1->LFC_Big_Rise_detect = 0;}
            }
        }
        if(tempF0==0) {
            if((pF2->LFC_reapply_current_duty > pF2->LFC_reapply_end_duty)&&(pF2->LFC_reapply_end_duty>0)) {
                tempW8 = (int16_t)pF2->LFC_reapply_end_duty;
            }
            else if(pF2->LFC_reapply_current_duty>0) {
                tempW8 = (int16_t)pF2->LFC_reapply_current_duty;
            }
            else if(pF1->LFC_reapply_end_duty>0) {tempW8 = (int16_t)pF1->LFC_reapply_end_duty;}
            else {tempW8 = (int16_t)pF1->LFC_reapply_current_duty;}    /* trick!!!!!! Ban Front Hold */

/*          if((tempW8 > (int16_t)pF1->LFC_reapply_end_duty)&&(pF1->LFC_reapply_end_duty>0)) {
                pF1->LFC_Rep_duty = pF1->LFC_reapply_end_duty;
            }
            else pF1->LFC_Rep_duty = tempW8;  */

/*            pF1->LFC_Rep_duty = (uint8_t)tempW8;

            tempW9 = (int16_t)pF1->LFC_Rep_duty-3;
*/
            tempW9 = tempW8 - S16_DUTY_DIFF_FOR_FORCED_HOLD;
            if(tempW9 <= 0) {tempW9 = 0;}
            if(pF1->LFC_reapply_current_duty<=(uint8_t)tempW9) {
                pF1->LFC_Big_Rise_detect = 1;
            }
            else {pF1->LFC_Big_Rise_detect = 0;}
        }
    }
    else {pF1->LFC_Big_Rise_detect = 0;}
}

void LCABS_vDctL2H(void)
{
    uint16_t u16StableCntSum;
    uint16_t u16StableCntThresL;
    uint16_t u16StableCntThresS1;
    uint16_t u16StableCntThresS2;
    int16_t  s16FlWheelPressDiff;
    int16_t  s16FrWheelPressDiff;
    int16_t	 s16FrontWheelPressDiff;
    
    LOW_to_HIGH_suspect_K=0;
    LOW_to_HIGH_suspect2_K=0;
  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    lcabsu1L2hSuspectByWhPres_K=0;
  #endif

    if(ABS_fz==0)
    {
        LOW_to_HIGH_suspect = 0; L2H_sus_cnt_by_aref = 0;
        LOW_to_HIGH_reset_cnt = 0; LOW_to_HIGH_reset_cnt2 = 0;
        LOW_to_HIGH_suspect2_flg = 0; LOW_to_HIGH_suspect2_old = 0;
        LCABS_vResetAllL2HSuspect2Flg();
    }

    if((LOW_to_HIGH_suspect==0)&&(vref>=VREF_10_KPH)) {
        if((AFZ_OK==1)&&(afz>-(int16_t)U8_PULSE_HIGH_MU)&&(CERTAIN_SPLIT_flag==0)&&(GMA_PULSE==0)
          #if __VDC
        &&(det_alatm<LAT_0G5G)&&(YAW_CDC_WORK==0)/*high-mu turn-braking exclusion*/
          #endif/*__VDC*/
        )
        {
            if(((aref_avg-aref0)>=S16_L2H_DCT_DIFF_DECEL)&&(aref0<0))/*(aref_avg<0)aref0*/
            {
                if(L2H_sus_cnt_by_aref <= U8_TYPE_MAXNUM) {L2H_sus_cnt_by_aref = L2H_sus_cnt_by_aref + L_1SCAN;}
            }
            else if(L2H_sus_cnt_by_aref > L_TIME_70MS)
            {
                if(((aref_avg-aref0)<S16_L2H_RESET_DIFF_DECEL) || (aref0>=0))            /* aref_avgaref0 */
                {
                    L2H_sus_cnt_by_aref = 0;
                }
                else
                {
                    L2H_sus_cnt_by_aref = L2H_sus_cnt_by_aref - L_1SCAN;
                }
            }
            else
            {
                L2H_sus_cnt_by_aref = 0;
            }

			  #if (__VDC==ENABLE)&&(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
			s16FlWheelPressDiff = FL.s16_Estimated_Active_Press - FL.Estimated_Press_SKID;
			s16FrWheelPressDiff = FR.s16_Estimated_Active_Press - FR.Estimated_Press_SKID;
			s16FrontWheelPressDiff = McrAbs(FL.Estimated_Press_SKID - FR.Estimated_Press_SKID);

            if((afz<-S16_L2H_DCT_ALLOWED_AFZ)&&(LOW_to_HIGH_suspect2_old==0)&&(FL.s16_Estimated_Active_Press>=MPRESS_35BAR)&&(FR.s16_Estimated_Active_Press>=MPRESS_35BAR))
            {
	            if( ((((s16FlWheelPressDiff>=(int16_t)U8_L2H_DCT_WPRESS_THRES2)||(s16FrWheelPressDiff>=(int16_t)U8_L2H_DCT_WPRESS_THRES2))&&(s16FrontWheelPressDiff<=(int16_t)U8_L2H_DCT_WPRESS_DIFF))
	           	   ||((s16FlWheelPressDiff> (int16_t)U8_L2H_DCT_WPRESS_THRES1)&&(s16FrWheelPressDiff>(int16_t)U8_L2H_DCT_WPRESS_THRES1)))
	              #if __SLIGHT_BRAKING_COMPENSATION
	               &&(lcabsu1SlightBraking==0)
	              #endif
	            )
	            {
	            	if(lcabsu1L2hSuspectByWhPres==0)
	            	{
	            		lcabsu1L2hSuspectByWhPres_K = 1;
	            	}
	            	lcabsu1L2hSuspectByWhPres = 1;
	            }
	            else{}
        	}
              #endif/* (__VDC==ENABLE)&&(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE) */
            
            #if (__L2H_IMPROVE_SPLIT_DETECTION == ENABLE)
            if((LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))
            {
                u16StableCntSum = (uint16_t)FL.LFC_STABIL_counter2 + (uint16_t)FR.LFC_STABIL_counter2 + (uint16_t)RL.LFC_STABIL_counter2 + (uint16_t)RR.LFC_STABIL_counter2;
                u16StableCntThresL = (uint16_t)U8_L2Hsus_STB_TIM_L_DECEL_SUM;
                u16StableCntThresS1 = (uint16_t)U8_L2Hsus_STB_TIM_S_DECEL_SUM1;
                u16StableCntThresS2 = (uint16_t)U8_L2Hsus_STB_TIM_S_DECEL_SUM2;
            }
            else
            {
                if(MSL_BASE_rl==1)
                {
                    u16StableCntSum = (uint16_t)FL.LFC_STABIL_counter2 + (uint16_t)RL.LFC_STABIL_counter2;
                }
                else if(MSL_BASE_rr==1)
                {
                    u16StableCntSum = (uint16_t)FR.LFC_STABIL_counter2 + (uint16_t)RR.LFC_STABIL_counter2;
                }
                else
                {
                    u16StableCntSum = 0;
                }
                u16StableCntThresL = (uint16_t)U8_L2Hsus_SP_STB_TIM_L_DEC_SUM;
                u16StableCntThresS1 = (uint16_t)U8_L2Hsus_SP_STB_TIM_S_DEC_SUM1;
                u16StableCntThresS2 = (uint16_t)U8_L2Hsus_SP_STB_TIM_S_DEC_SUM2;

				/*if(CERTAIN_SPLIT_flag == 1)
		        {
		            u16StableCntThresL = u16StableCntThresL * 2;
		            u16StableCntThresS1 = u16StableCntThresS1 * 2;
		            u16StableCntThresS2 = u16StableCntThresS2 * 2;
		        }*/
	
            }
            #else
                u16StableCntSum = (uint16_t)FL.LFC_STABIL_counter2 + (uint16_t)FR.LFC_STABIL_counter2 + (uint16_t)RL.LFC_STABIL_counter2 + (uint16_t)RR.LFC_STABIL_counter2;
                u16StableCntThresL = (uint16_t)U8_L2Hsus_STB_TIM_L_DECEL_SUM;
                u16StableCntThresS1 = (uint16_t)U8_L2Hsus_STB_TIM_S_DECEL_SUM1;
                u16StableCntThresS2 = (uint16_t)U8_L2Hsus_STB_TIM_S_DECEL_SUM2;
            #endif

            if(afz<-S16_L2H_DCT_ALLOWED_AFZ)/*&& (vref>=VREF_10_KPH)*/ {
                if(L2H_sus_cnt_by_aref >= U8_L2H_DCT_DECEL_CNT_L)
                {
                    if((aref_avg-aref0)>=S16_L2H_DCT_DIFF_DECEL)
                    {
                        if((FL.LFC_STABIL_counter2>=U8_L2H_STB_TIM_L_DECEL_F)&&(FR.LFC_STABIL_counter2>=U8_L2H_STB_TIM_L_DECEL_F)&&((RL.LFC_STABIL_counter2>=U8_L2H_STB_TIM_L_DECEL_R)||(RR.LFC_STABIL_counter2>=U8_L2H_STB_TIM_L_DECEL_R)))
                        {
                            LOW_to_HIGH_suspect_K=1;  LOW_to_HIGH_suspect=1;
                            LOW_to_HIGH_reset_cnt=10; LOW_to_HIGH_reset_cnt2=10;
                            LCABS_vResetAllL2HSuspect2Flg();
                            lsabsu8L2HMode = 11;
                        }
                        else if((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_F1)&&(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_F1)&&((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_R1)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_R1)))
                        {
                            LCABS_vSetAllL2HSuspect2Flg(); 
                            lsabsu8L2HMode = 21;                                 
                        }
                       #if (__L2H_IMPROVE_SPLIT_DETECTION==ENABLE)
                        else if(u16StableCntSum >= u16StableCntThresL)
                       #else 
                        else if((u16StableCntSum >= u16StableCntThresL) && (LFC_Split_flag==0) && (LFC_Split_suspect_flag==0))
                       #endif
                        {
                            if(((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)||(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)) 
                            && ((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)))
                            {
                                LCABS_vSetAllL2HSuspect2Flg();
                                lsabsu8L2HMode = 22;
                            }
                        }
                        else { }
                    }
                    else { }
                }
                else if(L2H_sus_cnt_by_aref >= U8_L2H_DCT_DECEL_CNT_S)
                {
                    if((aref_avg-aref0)>=S16_L2H_DCT_DIFF_DECEL_H)
                    {
                        if((FL.LFC_STABIL_counter2>=U8_L2H_STB_TIM_S_DECEL_F)&&(FR.LFC_STABIL_counter2>=U8_L2H_STB_TIM_S_DECEL_F)&&((RL.LFC_STABIL_counter2>=U8_L2H_STB_TIM_S_DECEL_R)||(RR.LFC_STABIL_counter2>=U8_L2H_STB_TIM_S_DECEL_R)))
                        {
                            LOW_to_HIGH_suspect_K=1;  LOW_to_HIGH_suspect=1;
                            LOW_to_HIGH_reset_cnt=10; LOW_to_HIGH_reset_cnt2=10;
                            LCABS_vResetAllL2HSuspect2Flg();
                            lsabsu8L2HMode = 111;
                        }
                        else if((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_F1)&&(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_F1)&&((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_R1)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_R1)))
                        {
                            LCABS_vSetAllL2HSuspect2Flg();
                            lsabsu8L2HMode = 121;
                        }
                        #if (__L2H_IMPROVE_SPLIT_DETECTION==ENABLE)
                        else if(u16StableCntSum >= u16StableCntThresS1)
                        #else 
                        else if((u16StableCntSum >= u16StableCntThresS1)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))
                        #endif
                        {
                            if(((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)||(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)) 
                            && ((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_L_DECEL_MIN)))
                            {
                                LCABS_vSetAllL2HSuspect2Flg();
                                lsabsu8L2HMode = 122;
                            }
                        }
                        else { }
                    }
                    else if((aref_avg-aref0)>=S16_L2H_DCT_DIFF_DECEL)
                    {
                        if((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_F2)&&(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_F2)&&((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_R2)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_R2)))
                        {
                            LCABS_vSetAllL2HSuspect2Flg();
                            lsabsu8L2HMode = 221;
                        }
                        #if (__L2H_IMPROVE_SPLIT_DETECTION==ENABLE)
                        else if(u16StableCntSum >= u16StableCntThresS2)
                        #else 
                        else if((u16StableCntSum >= u16StableCntThresS2)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))
                        #endif
                        {
                            if(((FL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_MIN)||(FR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_MIN)) 
                            && ((RL.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_MIN)||(RR.LFC_STABIL_counter2>=U8_L2Hsus_STB_TIM_S_DECEL_MIN)))
                            {
                                LCABS_vSetAllL2HSuspect2Flg();
                                lsabsu8L2HMode = 222;
                            }
                        }
                        else { }
                    }
                    else { }
                }
                else { }
            }
            else { }
        }
        else 
        {
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
        	lcabsu1L2hSuspectByWhPres = 0;
          #endif
        }

        if((FL.LFC_built_reapply_flag==1) && (FL.LFC_built_reapply_counter==L_1ST_SCAN)) {FL.LOW_to_HIGH_suspect2=0;}
        if((FR.LFC_built_reapply_flag==1) && (FR.LFC_built_reapply_counter==L_1ST_SCAN)) {FR.LOW_to_HIGH_suspect2=0;}
        if((RL.LFC_built_reapply_flag==1) && (RL.LFC_built_reapply_counter==L_1ST_SCAN)) {RL.LOW_to_HIGH_suspect2=0;}
        if((RR.LFC_built_reapply_flag==1) && (RR.LFC_built_reapply_counter==L_1ST_SCAN)) {RR.LOW_to_HIGH_suspect2=0;}

        if(LOW_to_HIGH_suspect2_flg==0)
        {
            if((FL.LOW_to_HIGH_suspect2==1) || (FR.LOW_to_HIGH_suspect2==1) || (RL.LOW_to_HIGH_suspect2==1) || (RR.LOW_to_HIGH_suspect2==1))
            {
                LOW_to_HIGH_suspect2_flg = 1;
            }
        }
        else
        {
            LOW_to_HIGH_suspect2_K = 0;
            if((FL.LOW_to_HIGH_suspect2==0) && (FR.LOW_to_HIGH_suspect2==0) && (RL.LOW_to_HIGH_suspect2==0) && (RR.LOW_to_HIGH_suspect2==0))
            {
                LOW_to_HIGH_suspect2_flg = 0;
            }
        }

        if((FL.LOW_to_HIGH_suspect2==0) && (FR.LOW_to_HIGH_suspect2==0) && (RL.LOW_to_HIGH_suspect2==0) && (RR.LOW_to_HIGH_suspect2==0)
          && (LOW_to_HIGH_suspect==0)
        )
        {
            LOW_to_HIGH_suspect2_old = 0;
        }
    }
    else {
        if((AFZ_OK==0)||(GMA_PULSE==1)||((AFZ_OK==1)&&(afz<-(int16_t)U8_PULSE_HIGH_MU))) {
            LOW_to_HIGH_suspect=0;
            L2H_sus_cnt_by_aref=0;
        }

        if(STAB_A_fl==1) {
            if((LOW_to_HIGH_reset_cnt==10) || (LOW_to_HIGH_reset_cnt==6)) {
                LOW_to_HIGH_reset_cnt = LOW_to_HIGH_reset_cnt - 6;
            }
        }
        if(STAB_A_fr==1) {
            if((LOW_to_HIGH_reset_cnt==10) || (LOW_to_HIGH_reset_cnt==4)) {
                LOW_to_HIGH_reset_cnt = LOW_to_HIGH_reset_cnt - 4;
            }
        }
        if(STAB_A_rl==1) {
            if((LOW_to_HIGH_reset_cnt2==10) || (LOW_to_HIGH_reset_cnt2==6)) {
                LOW_to_HIGH_reset_cnt2 = LOW_to_HIGH_reset_cnt2 - 6;
            }
        }
        if(STAB_A_rr==1) {
            if((LOW_to_HIGH_reset_cnt2==10) || (LOW_to_HIGH_reset_cnt2==4)) {
                LOW_to_HIGH_reset_cnt2 = LOW_to_HIGH_reset_cnt2 - 4;
            }
        }

        if((LOW_to_HIGH_reset_cnt==0) && (LOW_to_HIGH_reset_cnt2==0))
        {
            LOW_to_HIGH_suspect=0;L2H_sus_cnt_by_aref=0;LOW_to_HIGH_suspect2_old=0;
         /* #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
            lcabsu1L2hSuspectByWhPres = 0;
          #endif*/
        }
    }
}

void LCABS_vResetAllL2HSuspect2Flg(void)
{
    FL.LOW_to_HIGH_suspect2=0;
    FR.LOW_to_HIGH_suspect2=0;
    RL.LOW_to_HIGH_suspect2=0;
    RR.LOW_to_HIGH_suspect2=0;
  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    lcabsu1L2hSuspectByWhPres = 0;
  #endif
}

void LCABS_vSetAllL2HSuspect2Flg(void)
{
    FL.LOW_to_HIGH_suspect2=1;
    FR.LOW_to_HIGH_suspect2=1;
    RL.LOW_to_HIGH_suspect2=1;
    RR.LOW_to_HIGH_suspect2=1;  
    LOW_to_HIGH_suspect2_K=1; LOW_to_HIGH_suspect2_old=1;
}

#if __INGEAR_VIB
void LCABS_vDctIngearVib(void)
{
    #if __REAR_D    /* rear wheel drive vehicle */
        LCABS_vDctIngearVibWL(&RL);
        LCABS_vDctIngearVibWL(&RR);
        if((RL.LFC_zyklus_z<=U8_IN_GEAR_detect_cycle/*3*/)&&(RR.LFC_zyklus_z<=U8_IN_GEAR_detect_cycle/*3*/)) {
            if(((RL.In_gear_vibration_counter>=(U8_IN_GEAR_detect_Vib_cnt-1))&&(RR.In_gear_vibration_counter>=(U8_IN_GEAR_detect_Vib_cnt-1)))
            ||((RL.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))&&((RR.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))||(RR.In_Gear_flag==1)))
            ||((RR.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))&&((RL.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))||(RL.In_Gear_flag==1)))) {
                RL.In_Gear_flag=1;
                RR.In_Gear_flag=1;
            }
        }
    #else           /* front wheel drive vehicle */
        LCABS_vDctIngearVibWL(&FL);
        LCABS_vDctIngearVibWL(&FR);
        if((FL.LFC_zyklus_z<=U8_IN_GEAR_detect_cycle/*3*/)&&(FR.LFC_zyklus_z<=U8_IN_GEAR_detect_cycle/*3*/)) {
            if(((FL.In_gear_vibration_counter>=(U8_IN_GEAR_detect_Vib_cnt-1))&&(FR.In_gear_vibration_counter>=(U8_IN_GEAR_detect_Vib_cnt-1)))
            ||((FL.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))&&((FR.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))||(FR.In_Gear_flag==1)))
            ||((FR.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))&&((FL.Dec_hump_counter>=(U8_IN_GEAR_detect_Dec_Hump_cnt-1))||(FL.In_Gear_flag==1)))) {
                FL.In_Gear_flag=1;
                FR.In_Gear_flag=1;
            }
        }
    #endif
}

void LCABS_vDctIngearVibWL(struct W_STRUCT *WL)
{
    if((FSF_wl==1) && (ABS_fz==0)) {
        WL->Acc_to_Dec_cnt=0;
        WL->Dec_hump_counter=0;
        WL->In_Gear_Vib_flag=0;
        WL->In_Gear_flag=0;
        WL->In_gear_vibration_counter=0;
        WL->In_Gear_arad_credible=0;
    }
    else {
        if(WL->In_gear_vibration_counter>0) {tempB7=S8_IN_GEAR_Vib_Detect_arad2;}
        else {tempB7=S8_IN_GEAR_Vib_Detect_arad1;}

        if(WL->arad>=0){
            if((WL->arad<WL->peak_acc) && (WL->peak_acc>=tempB7)) {WL->Acc_to_Dec_cnt++;}
            else if(WL->peak_acc>ARAD_2G0) {WL->Acc_to_Dec_cnt=0;}
            else if((STABIL_wl==1) && (WL->Acc_to_Dec_cnt>0)) {WL->Acc_to_Dec_cnt++;}
            else {WL->Acc_to_Dec_cnt=0;}
        }
        else if(WL->Acc_to_Dec_cnt>0) {
            if(WL->arad<=WL->peak_dec) {WL->Acc_to_Dec_cnt++;}
            else if(STABIL_wl==1) {WL->Acc_to_Dec_cnt++;}
            else { }
        }
        else {WL->Acc_to_Dec_cnt=0;}

        if(WL->Acc_to_Dec_cnt>=100) {WL->Acc_to_Dec_cnt=100;}

        if(WL->In_Gear_Vib_flag==0) {
            if((WL->Acc_to_Dec_cnt>0) && (WL->Acc_to_Dec_cnt<U8_IN_GEAR_Vib_Acc_to_Dec_time/*20*/)) {
                if((WL->arad>WL->peak_dec) && (WL->peak_dec<-tempB7)) {
                    WL->In_Gear_Vib_flag=1;
                    WL->In_gear_vibration_counter++;
                }
            }
        }
        else {
            if(WL->Acc_to_Dec_cnt==0) {WL->In_Gear_Vib_flag=0;}
            else if(WL->Acc_to_Dec_cnt>=U8_IN_GEAR_Vib_Acc_to_Dec_time/*20*/) {
                WL->In_Gear_Vib_flag=0;
                WL->In_gear_vibration_counter--;
            }
            else { }
        }

        if((ABS_fz==1)&&(WL->LFC_built_reapply_flag==0)) {
            if(WL->In_Gear_arad_credible==0)
            {
                if(WL->arad<-ARAD_1G0) {WL->In_Gear_arad_credible=1;}
            }
            else
            {
                if((WL->arad_alt<0)&&(WL->arad>=0)) {
                    if(WL->peak_dec_alt<-S8_Dec_Hump_Counting_arad) {WL->Dec_hump_counter++; WL->In_Gear_arad_credible=0;}
                }
            }
        }
        else {WL->Dec_hump_counter=0; WL->In_Gear_arad_credible=0;}

    /*****************************************************/
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)
        ||((AFZ_OK==1)&&(afz<=-AFZ_0G3))||(UN_GMA_wl==1)) {
            WL->In_gear_vibration_counter=0;
            WL->Dec_hump_counter=0;
        }
    /*****************************************************/
      #if   __VDC
        if(WL->L_SLIP_CONTROL==1) {             /* '04 SW */
            WL->In_gear_vibration_counter=0;
            WL->Dec_hump_counter=0;
        }
      #endif

        if(WL->LFC_zyklus_z<=U8_IN_GEAR_detect_cycle) {
            if((WL->In_gear_vibration_counter>=U8_IN_GEAR_detect_Vib_cnt)||(WL->Dec_hump_counter>=U8_IN_GEAR_detect_Dec_Hump_cnt)) {
                WL->In_Gear_flag=1;
            }
        }
    }
/*  if((AFZ_OK==1)&&(afz<=-AFZ_0G3)) WL->In_Gear_flag=0; */
/*  if(WL->Check_double_brake_flag==1) WL->In_Gear_flag=0; */
}
#endif

void LCABS_vDecideLFCCyclicComp(void)
{
    LCABS_vDecideLFCCyclicCompWL(&FL);
    LCABS_vDecideLFCCyclicCompWL(&FR);
    LCABS_vDecideLFCCyclicCompWL(&RL);
    LCABS_vDecideLFCCyclicCompWL(&RR);
}

void LCABS_vDecideLFCCyclicCompWL(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;

    if((FSF_wl==1)&&(ABS_wl==0)) {
        WL->LFC_built_reapply_counter_delta=0;
        WL->LFC_Cyclic_Comp_flag=0;
        WL->LFC_rough_road_disable_comp_flag=0;
    }
    else if((WL->LFC_zyklus_z>=2) && (WL->LFC_fictitious_cycle_flag==0)) {
      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif
      
        if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN)) {
            tempW0 = LCABS_s16SetLFCRiseRefscan();

            WL->LFC_built_reapply_counter_delta = (int16_t)WL->LFC_built_reapply_counter_old - tempW0;

            WL->LFC_rough_road_disable_comp_flag=0;
            if(WL->LFC_built_reapply_counter_delta >= ((int16_t)WL->hold_timer_new_ref_old+L_1SCAN)) {             /* Too Large Pressure Rise Delay!!! */
                if(WL->Check_double_brake_flag2==1) {WL->LFC_Cyclic_Comp_flag=0;}
                else {WL->LFC_Cyclic_Comp_flag=1;}
            }
            else if(WL->LFC_built_reapply_counter_delta <= (-(int16_t)((int16_t)WL->hold_timer_new_ref_old+L_1SCAN))){    /* Too Fast Pressure Rise!!! */
                if(WL->LFC_n_th_deep_slip_comp_flag==1) {WL->LFC_Cyclic_Comp_flag=0;}
                else if(WL->In_Gear_flag==1) {WL->LFC_Cyclic_Comp_flag=0;}
                else if(WL->LOW_to_HIGH_suspect2==1) {WL->LFC_Cyclic_Comp_flag=0;}
              #if __STABLE_DUMPHOLD_ENABLE
                else if(WL->lcabsu1StabDumpHoldCycle==1)
                {
                    WL->LFC_Cyclic_Comp_flag=0;
                }
              #endif
                else {
                    LCABS_vDisableCompForSmallCycle();
                    if(WL->LFC_rough_road_disable_comp_flag==1) {WL->LFC_Cyclic_Comp_flag=0;}
                    else {WL->LFC_Cyclic_Comp_flag=1;}
                }
            }
            else {WL->LFC_Cyclic_Comp_flag=0;}
            WL->Check_double_brake_flag2=0;
        }
      #if   __VDC && __ABS_COMB
        }
      #endif
    }
    else {
        WL->LFC_built_reapply_counter_delta=0;
        WL->LFC_Cyclic_Comp_flag=0;
        WL->LFC_rough_road_disable_comp_flag=0;
    }
}

int16_t LCABS_s16SetLFCRiseRefscan(void)
{
    if(REAR_WHEEL_wl == 0) {            /* Front Wheel */
      #if __UCC_COOPERATION_CONTROL
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(UCC_DETECT_BUMP_Vehicle_flag==1))  {
      #else
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) {
      #endif
            tempW1 = (int16_t)U8_LFC_built_cnt_F_Rough_Ref;
        }
        else if((AFZ_OK==1)&&(afz<=-(int16_t)U8_LOW_MU))    {
            if(afz<=-(int16_t)U8_HIGH_MU) {             /* High mu, afz < -0.55g */
                tempW1 = (int16_t)(U8_LFC_built_cnt_F_High_Ref);
            }
            else {                              /* Medium mu, -0.55g<afz<-0.25g */
                tempW1 = (int16_t)U8_LFC_built_cnt_F_Med_Ref;
              #if __MEDMU_LOWSPEED_DECEL_IMPROVE  
                if(vref <= (int16_t)VREF_30_KPH) 
                {
                    tempW1 = (int16_t)(U8_LFC_built_cnt_F_Med_Ref-U8_Hold_Timer_New_Ref_Med-L_1SCAN);
                }
              #endif  

                if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
                {
                    if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
                    {
                        tempW1 = (int16_t)(U8_LFC_built_cnt_F_SpHigh_Ref);
                    }
                }
            }
        }
        else {                                  /* Low mu, -0.25g<afz */
            if((AFZ_OK==1) || (WL->LFC_initial_deep_slip_comp_flag==1))
            {              
                tempW1 = (int16_t)U8_LFC_built_cnt_F_Low_Ref;
                if(vref < (int16_t)VREF_15_KPH) {
                    tempW1 = (int16_t)U8_LFC_built_cnt_F_Low_B_15_Ref;
                }
            }
            else{
                tempW1 = (int16_t)U8_LFC_built_cnt_F_High_Ref;
            }
        }
    }
    else {                              /* Rear Wheel */
      #if __UCC_COOPERATION_CONTROL
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(UCC_DETECT_BUMP_Vehicle_flag==1))  {
      #else
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) {
      #endif
            tempW1 = (int16_t)U8_LFC_built_cnt_R_Rough_Ref;
        }
        else if((AFZ_OK==1)&&(afz<=-(int16_t)U8_LOW_MU))    {
            if(afz<=-(int16_t)U8_HIGH_MU) {             /* High mu, afz < -0.55g */
                tempW1 = (int16_t)U8_LFC_built_cnt_R_High_Ref;
            }
            else {                              /* Medium mu, -0.55g<afz<-0.25g */
                tempW1 = (int16_t)U8_LFC_built_cnt_R_Med_Ref;
              #if __MEDMU_LOWSPEED_DECEL_IMPROVE    
                if(vref <= (int16_t)VREF_30_KPH) 
                {
                    tempW1 = (int16_t)(U8_LFC_built_cnt_R_Med_Ref-U8_Hold_Timer_New_Ref_Med_R-L_1SCAN);
                }
              #endif  
            }
        }
        else {                                  /* Low mu, -0.25g<afz */
            if((AFZ_OK==1) || (WL->LFC_initial_deep_slip_comp_flag==1))
            {
                tempW1 = (int16_t)U8_LFC_built_cnt_R_Low_Ref;
            }
            else{
                tempW1 = (int16_t)U8_LFC_built_cnt_R_High_Ref;
            }
        }
    }
    if(WL->LFC_zyklus_z<=2)
    {
        tempW1 = (((int16_t)WL->hold_timer_new_ref_old+L_1SCAN)*5);
    }

    return tempW1;
}

void LCABS_vDisableCompForSmallCycle(void)
{

    int8_t Ref_peak_slip=0;
    int8_t Ref_dump_temp = 0;

    if((FSF_wl==1) && (ABS_wl==0)) {WL->LFC_rough_road_disable_comp_flag=0;}
    else {
        WL->LFC_rough_road_disable_comp_flag=0;
        if(Rough_road_detect_vehicle==1) {
            if(REAR_WHEEL_wl==0){
                Ref_dump_temp = S8_Rough_Disab_Dump_Ref_F;
                Ref_peak_slip = -LAM_25P;
            }
            else {
                Ref_dump_temp = S8_Rough_Disab_Dump_Ref_R;
                Ref_peak_slip = -LAM_20P;
            }
        }
        else {
            if(REAR_WHEEL_wl==0){
                if(AFZ_OK==1) {
                    if(afz<-(int16_t)U8_LOW_MU) {
                        if(afz<=-(int16_t)U8_HIGH_MU) {      /* High mu, afz <= -0.55g */
                            tempW0=(int16_t)(U8_HIGH_MU)+10;
                            if(afz<(int16_t)(-tempW0)) {Ref_peak_slip = -LAM_5P;}
                            else {Ref_peak_slip = -LAM_10P;}
                            Ref_dump_temp = S8_Rough_Disab_Dump_High_F;
                        }
                        else {                          /* Medium mu, -0.25g < afz < -0.55g */
                            Ref_dump_temp = S8_Rough_Disab_Dump_Med_F;
                            if(vref<=VREF_10_KPH) {Ref_peak_slip = -LAM_20P;}
                            else if(vref<=VREF_30_KPH) {Ref_peak_slip = -LAM_15P;}
                            else {Ref_peak_slip = -LAM_10P;}
                            if((LFC_Split_flag==1)&&(UN_GMA_wl==0)){
                                Ref_dump_temp = S8_Rough_Disab_Dump_High_F;
                                Ref_peak_slip = -LAM_5P;
                            }
                        }
                    }
                }
                else {
                    Ref_dump_temp = S8_Rough_Disab_Dump_BF_AFZOK_F;
                    Ref_peak_slip = -LAM_10P;
                }
            }
            else {
                if(AFZ_OK==1) {
                    if(afz<-(int16_t)U8_LOW_MU) {
                        if(afz<=-(int16_t)U8_HIGH_MU) {          /* High mu, afz =< -0.55g */
                            tempW0=(int16_t)(U8_HIGH_MU)+10;
                            if(afz<=(int16_t)(-tempW0)) {Ref_peak_slip = -LAM_5P;}
                            else {Ref_peak_slip = -LAM_10P;}
                            Ref_dump_temp = S8_Rough_Disab_Dump_High_R;
                        }
                        else {
                            Ref_dump_temp = S8_Rough_Disab_Dump_Med_R;
                            Ref_peak_slip = -LAM_10P;
                        }
                    }
                }
                else {
                    Ref_dump_temp = S8_Rough_Disab_Dump_BF_AFZOK_R;
                    Ref_peak_slip = -LAM_10P;
                }
            }
        }
        if((Rough_road_detect_vehicle==0)&&((AFZ_OK==1)&&(afz>=-(int16_t)U8_LOW_MU))) {
    /*      if((WL->peak_slip>-LAM_30P)&&(WL->peak_slip>WL->slip_at_real_dump*2)) {
                WL->LFC_rough_road_disable_comp_flag=1;
            } */
            if(vref>=VREF_100_KPH) {                    /*Changed 030724 */
                if(WL->peak_slip>-LAM_5P) {WL->LFC_rough_road_disable_comp_flag=1;}
            }
            else if(vref>=VREF_80_KPH) {
                if(WL->peak_slip>-LAM_7P) {WL->LFC_rough_road_disable_comp_flag=1;}
            }
            else if(vref>=VREF_50_KPH) {
                if(WL->peak_slip>-LAM_10P) {WL->LFC_rough_road_disable_comp_flag=1;}
            }
            else if(vref>=VREF_30_KPH) {
                if((WL->peak_slip>-LAM_20P)&&((int16_t)WL->peak_slip>(((int16_t)WL->slip_at_real_dump*3)/2))) {
                    WL->LFC_rough_road_disable_comp_flag=1;
                }
            }
            else {
                if((WL->peak_slip>-LAM_30P)&&((int16_t)WL->peak_slip>((int16_t)WL->slip_at_real_dump*2))) {
                    WL->LFC_rough_road_disable_comp_flag=1;
                }
                else if(vref<=VREF_10_KPH) {
                    WL->LFC_rough_road_disable_comp_flag=1;
                }
                else { }
            }
        }
        else if((WL->LFC_dump_counter<= (uint8_t)Ref_dump_temp)&&(WL->peak_slip>=Ref_peak_slip)){
            WL->LFC_rough_road_disable_comp_flag=1;
        }
        else {WL->LFC_rough_road_disable_comp_flag=0;}

        if(vref<=VREF_10_KPH)
        {
            WL->LFC_rough_road_disable_comp_flag=1;
        }
    }
}

void LCABS_vDecideLFCSpecialComp(void)
{
    LCABS_vDecideLFCSpecialCompWL(&FL);
    LCABS_vDecideLFCSpecialCompWL(&FR);
    LCABS_vDecideLFCSpecialCompWL(&RL);
    LCABS_vDecideLFCSpecialCompWL(&RR);
}

void LCABS_vDecideLFCSpecialCompWL(struct W_STRUCT *WL_temp)
{
    uint8_t   Initial_LOW_MU_DETECT_ENABLE = 0;
    uint8_t   Nth_LOW_MU_DETECT_ENABLE = 0;

    WL=WL_temp;

    if((FSF_wl==1) && (ABS_wl==0)) {
        WL->LFC_initial_deep_slip_comp_flag=0;      /* Initial Duty Compensation 020619 */
        WL->LFC_n_th_deep_slip_comp_flag=0;
        WL->LFC_n_th_deep_slip_pre_flag=0;

        WL->LFC_big_rise_and_dump_flag=0;
      #if __PREV_RISE_DUMP_IMPROVE    
        WL->LFC_big_rise_and_dump_cnt=0;
      #endif
      #if __UNSTABLE_REAPPLY_ENABLE
        WL->UNSTABLE_LowMu_comp_flag=0;
        WL->UNSTABLE_LowMu_comp_flag2=0;
      #endif
    }
    else
    {
        if((WL->LFC_built_reapply_flag==1) && (WL->LFC_built_reapply_counter==L_1ST_SCAN))
        {
            if(WL->Check_double_brake_flag==1)
            {
                WL->LFC_initial_deep_slip_comp_flag=0;      /* Initial Duty Compensation 020619 */
                WL->LFC_n_th_deep_slip_comp_flag=0;
                WL->LFC_n_th_deep_slip_pre_flag=0;
                WL->LFC_big_rise_and_dump_flag=0;
                Initial_LOW_MU_DETECT_ENABLE = 1;
            }
            else if(WL->LFC_zyklus_z==1)
            {
                Initial_LOW_MU_DETECT_ENABLE = 1;
            }
            else if(WL->LFC_zyklus_z>=2)
            {
                Nth_LOW_MU_DETECT_ENABLE = 1;
                }
            else { }
        }
      #if __UNSTABLE_REAPPLY_ENABLE
        else if((WL->Reapply_Accel_flag==1)&&(WL->Reapply_Accel_1st_scan==1))
        {
           #if __DETECT_MU_FOR_UR
            if(WL->LFC_zyklus_z>=1)
            {
                Nth_LOW_MU_DETECT_ENABLE = 2;
            }
            else { }
           #else
            if(WL->Check_double_brake_flag==1)
            {
                WL->LFC_initial_deep_slip_comp_flag=0;
                Initial_LOW_MU_DETECT_ENABLE = 2;
            }
            if((WL->LFC_zyklus_z==0)&&(WL->LFC_initial_deep_slip_comp_flag==0))
            {
                Initial_LOW_MU_DETECT_ENABLE = 2;
            }
            else if(WL->LFC_zyklus_z>=1)
            {
                Nth_LOW_MU_DETECT_ENABLE = 2;
            }
            else { }
           #endif
        }
      #endif
        else { }


      #if   __VDC && __ABS_COMB
        if(WL->L_SLIP_CONTROL==0) {
      #endif

        if(Initial_LOW_MU_DETECT_ENABLE>0) {
            if(REAR_WHEEL_wl==0) {              /* Front Wheels */
                if(LEFT_WHEEL_wl==1) {          /* Left Wheel */
                    LCABS_vDctInitialLowMuForFront(&FL);
                }
                else {                          /* Right Wheel */
                    LCABS_vDctInitialLowMuForFront(&FR);
                }
            }
            else {                              /* Rear Wheel */
                if(LEFT_WHEEL_wl==1) {          /* Left Wheel */
                    if(FL.LFC_initial_deep_slip_comp_flag==1) {
                        WL->LFC_initial_deep_slip_comp_flag=1;
                    }
                    else {
                        LCABS_vDctInitialLowMuForFront(&FL);
                        if(FL.LFC_initial_deep_slip_comp_flag==1) {
                            WL->LFC_initial_deep_slip_comp_flag=1;
                            FL.LFC_initial_deep_slip_comp_flag=0;
                        }
                    }
                }
                else {                          /* Right Wheel */
                    if(FR.LFC_initial_deep_slip_comp_flag==1) {
                        WL->LFC_initial_deep_slip_comp_flag=1;
                    }
                    else {
                        LCABS_vDctInitialLowMuForFront(&FR);
                        if(FR.LFC_initial_deep_slip_comp_flag==1) {
                            WL->LFC_initial_deep_slip_comp_flag=1;
                            FR.LFC_initial_deep_slip_comp_flag=0;
                        }
                    }
                }
            }
          #if __UNSTABLE_REAPPLY_ENABLE
		      #if (__DETECT_MU_FOR_UR==DISABLE)
            if(Initial_LOW_MU_DETECT_ENABLE==2)
            {
                WL->UNSTABLE_LowMu_comp_flag=WL->LFC_initial_deep_slip_comp_flag;
                if(WL->In_Gear_flag==1) {WL->UNSTABLE_LowMu_comp_flag=1;}
                WL->LFC_initial_deep_slip_comp_flag=0;
            }
            else
			  #endif
            {
                WL->UNSTABLE_LowMu_comp_flag=0;
            }
            WL->UNSTABLE_LowMu_comp_flag2=0;
          #endif
        }
        else if(Nth_LOW_MU_DETECT_ENABLE>0) {
            /* Mu_Jump Check !!! */

            WL->LFC_big_rise_and_dump_flag=0;

            if(WL->LFC_n_th_deep_slip_pre_flag==0) { /*n_th_slip_comp를 1회만.. */
                if(REAR_WHEEL_wl==0)
                {
                    LCABS_vDctH2LForFront();
                }
                else
                {
                    LCABS_vDctH2LForRear();
                }

                if(WL->LFC_n_th_deep_slip_comp_flag==0){
                    LCABS_vPreventBigRiseAndDump();
                }
                else {WL->LFC_n_th_deep_slip_pre_flag=1;}

              #if __UNSTABLE_REAPPLY_ENABLE
                if(Nth_LOW_MU_DETECT_ENABLE==2)
                {
                    WL->UNSTABLE_LowMu_comp_flag=WL->LFC_n_th_deep_slip_comp_flag;
                    WL->UNSTABLE_LowMu_comp_flag2=WL->LFC_big_rise_and_dump_flag;
                    if((WL->In_Gear_flag==1)&&(WL->LFC_initial_deep_slip_comp_flag==0)&&(WL->LFC_zyklus_z<=(U8_IN_GEAR_detect_cycle+1)))
                    {
                        WL->UNSTABLE_LowMu_comp_flag=1;
                    }
                    WL->LFC_n_th_deep_slip_comp_flag=0;
                    WL->LFC_n_th_deep_slip_pre_flag=0;
                    WL->LFC_big_rise_and_dump_flag=0;
                }
                else
                {
                    WL->UNSTABLE_LowMu_comp_flag=0;
                    WL->UNSTABLE_LowMu_comp_flag2=0;
                }
              #endif
            }
            else {
              #if __UNSTABLE_REAPPLY_ENABLE
                WL->UNSTABLE_LowMu_comp_flag=0;
                if(Nth_LOW_MU_DETECT_ENABLE==2)
                {
                    LCABS_vPreventBigRiseAndDump();
                    WL->UNSTABLE_LowMu_comp_flag2=WL->LFC_big_rise_and_dump_flag;
                    WL->LFC_big_rise_and_dump_flag=0;
                }
                else
                {
                    WL->UNSTABLE_LowMu_comp_flag2=0;
              #endif

                    WL->LFC_n_th_deep_slip_comp_flag=0;
                    if(REAR_WHEEL_wl==0)
                    {
                        LCABS_vDctL2HFromFrontWheel();      /* L2H Detect */
                        if(WL->LFC_n_th_deep_slip_pre_flag==0)
                        {
                            LCABS_vDctH2LForFront();
                            if(WL->LFC_n_th_deep_slip_comp_flag==0)
                            {
                                LCABS_vPreventBigRiseAndDump();
                            }
                            else {WL->LFC_n_th_deep_slip_pre_flag=1;}
                        }
                        else
                        {
                            LCABS_vPreventBigRiseAndDump();
                        }
                    }
                    else
                    {
                        LCABS_vPreventBigRiseAndDump();
                    }
              #if __UNSTABLE_REAPPLY_ENABLE
                }
              #endif
            }
        }
        else { }
      #if   __VDC && __ABS_COMB
        }
      #endif

    }
}

/*020909 by KGY */
/*021105 changed by KGY */
void LCABS_vDctInitialLowMuForFront(struct W_STRUCT *WL)
{
    int16_t Dump_ratio_temp;

    int8_t Peak_Slip_Thres_Per_Speed;

    if(vref < (int16_t)VREF_40_KPH)
    {
        Peak_Slip_Thres_Per_Speed = S8_LFC_Initial_Peak_Slip_Low + (int8_t)(((vref - VREF_40_KPH)*LAM_2P)/VREF_5_KPH);
    }
    else
    {
        Peak_Slip_Thres_Per_Speed = S8_LFC_Initial_Peak_Slip_Low;
    }

    if((WL->peak_slip>=(int8_t)(Peak_Slip_Thres_Per_Speed))
        && (WL->slip_at_real_dump<=(int8_t)(S8_LFC_Initial_Dump_Slip_Low))){
        WL->LFC_initial_deep_slip_comp_flag=0;
    }
    else if((WL->peak_slip<(int8_t)(Peak_Slip_Thres_Per_Speed))
    && ((int16_t)WL->peak_slip>(((int16_t)S8_LFC_Initial_Slip_Ratio_Low*(WL->slip_at_real_dump-S8_LFC_Initial_Dump_Slip_Low))+(int16_t)Peak_Slip_Thres_Per_Speed))){
        if(Rough_road_detect_vehicle==0){
            if(WL->Mu_change_Dump_counter>=(U8_LFC_Initial_Dump_Scan_Low)){
                WL->LFC_initial_deep_slip_comp_flag=1;
            }
            else {WL->LFC_initial_deep_slip_comp_flag=0;}
        }
    }
    else {
        Dump_ratio_temp = (((int16_t)U8_LFC_Initial_Dump_Scan_Med-(int16_t)U8_LFC_Initial_Dump_Scan_Low)*100)/20;
        if(WL->Mu_change_Dump_counter>=(U8_LFC_Initial_Dump_Scan_Low)){
            WL->LFC_initial_deep_slip_comp_flag=1;
        }
        else if(WL->Mu_change_Dump_counter>=(U8_LFC_Initial_Dump_Scan_Med)){
            if(((int16_t)(WL->Mu_change_Dump_counter-(U8_LFC_Initial_Dump_Scan_Med)))>
                ((((int16_t)WL->slip_at_real_dump+20)*Dump_ratio_temp)/100)){
                /*dump_slip이 -20%일때의 WL->LFC_dump_counter값이 U8_LFC_Initial_Dump_Scan_Med */
                /*dump_slip이 -40%일때의 WL->LFC_dump_counter값이 U8_LFC_Initial_Dump_Scan_Low */
                /*두 포인트를 연결한 직선 위쪽에 존재하는 경우만이 Low_mu */
                WL->LFC_initial_deep_slip_comp_flag=1;
            }
            else {
                WL->LFC_initial_deep_slip_comp_flag=0;
            }
        }
        else {
            WL->LFC_initial_deep_slip_comp_flag=0;
        }

        if(Rough_road_detect_vehicle==1){
            if(WL->Mu_change_Dump_counter<40){
                WL->LFC_initial_deep_slip_comp_flag=0;
            }
        }

        if(WL->peak_slip>(int8_t)(((int16_t)Peak_Slip_Thres_Per_Speed*2)/3))
        {
            WL->LFC_initial_deep_slip_comp_flag=0;
        }
    }
/*
#if __4WD   // Added by J.K.Lee at 03 SW winter
            // if 4WD car, afz is fastly,correctly calculated by G-sensor.
    if((AFZ_OK == 1)&&(afz>-(int16_t)U8_LOW_MU)){
        WL->LFC_initial_deep_slip_comp_flag=1;
    }
#endif
*/

/*    if((WL->accel_arad_avg >= ARAD_5G0)||(WL->arad_gauge>=0)) {      // Added by KGY due to BUMP 030410 */
  #if __UCC_COOPERATION_CONTROL
    if((WL->UCC_DETECT_BUMP_flag==1)||(WL->UCC_DETECT_PotHole_flag==1)
      #if __BUMP_SUSPECT
        ||(WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F1)||((WL->arad_gauge>S8_BMP_SUS_ARAD_GAUGE_THRES_F1)&&(WL->max_arad_instbl>0))
      #endif
      )
    {
        WL->LFC_initial_deep_slip_comp_flag=0;
    }
  #else
      #if __BUMP_SUSPECT
    if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F1)||((WL->arad_gauge>S8_BMP_SUS_ARAD_GAUGE_THRES_F1)&&(WL->max_arad_instbl>0))) {      /*Added by KGY due to BUMP 030410 */
        WL->LFC_initial_deep_slip_comp_flag=0;
    }
  #endif
  #endif

}

/*
void LCABS_vDctInitialLowMuForRear(struct W_STRUCT *WL)
{
    tempW0 = (int16_t)((uint32_t)WL->slip_recovery_counter*(int16_t)1000/vref);
    if(tempW0 >= 50){                                       // 020905 by KGY
        WL->LFC_initial_deep_slip_comp_flag=1;
        WL->LFC_initial_small_slip_comp_flag=0;
    }
    else if (tempW0 >= 25) {
        WL->LFC_initial_deep_slip_comp_flag=0;
        WL->LFC_initial_small_slip_comp_flag=1;
    }
    else {
        WL->LFC_initial_deep_slip_comp_flag=0;
        WL->LFC_initial_small_slip_comp_flag=0;
    }
}
*/

/* 020909 by KGY */
/* 021113 changed by KGY */
void LCABS_vDctH2LForFront(void)
{
    int16_t recovery_indicator_temp;
    int8_t Peak_Slip_Thres_Per_Speed;
    
    recovery_indicator_temp = (int16_t)(((uint32_t)WL->slip_recovery_counter*1428)/(uint32_t)vref);/* 1428=1000*10ms/7ms */

    if(vref < (int16_t)VREF_40_KPH)
    {
        Peak_Slip_Thres_Per_Speed = S8_LFC_Peak_Slip_Jump_Mu + (int8_t)(((vref - VREF_40_KPH)*LAM_2P)/VREF_5_KPH);
    }
    else
    {
        Peak_Slip_Thres_Per_Speed = S8_LFC_Peak_Slip_Jump_Mu;
    }
    
    if(WL->peak_slip>(int8_t)(Peak_Slip_Thres_Per_Speed)) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }
    else if(((int16_t)WL->peak_slip-(int16_t)(Peak_Slip_Thres_Per_Speed))
        >((int16_t)S8_LFC_Slip_Ratio_Jump_Mu*(WL->slip_at_real_dump-(int8_t)S8_LFC_Dump_Slip_Jump_Mu))) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }
    else {
        if(recovery_indicator_temp>=125){       /*125 : 회복지수 1           */
            if(WL->LFC_dump_counter>=10){
                WL->LFC_n_th_deep_slip_comp_flag=1;
            }
        }
        else if(recovery_indicator_temp>=50){
            if(WL->LFC_dump_counter>=(U8_LFC_Dump_Scan_Jump_Mu)){
                WL->LFC_n_th_deep_slip_comp_flag=1;
            }
        }
        else { }

        if(Rough_road_detect_vehicle==1){
            if((recovery_indicator_temp<250)||(WL->LFC_dump_counter<(U8_LFC_Dump_Scan_Rough_Road))) {
                WL->LFC_n_th_deep_slip_comp_flag=0;
            }
        }
/*        if((WL->accel_arad_avg >= ARAD_5G0)||(WL->arad_gauge>=0)) {      //Added by KGY due to BUMP 030410 */
      #if __UCC_COOPERATION_CONTROL
        if((WL->UCC_DETECT_BUMP_flag==1)||(WL->UCC_DETECT_PotHole_flag==1)
        #if __BUMP_SUSPECT
           ||(WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F1)||(WL->arad_gauge>S8_BMP_SUS_ARAD_GAUGE_THRES_F1)
        #endif
          )
        {
            WL->LFC_n_th_deep_slip_comp_flag=0;
        }
      #else
        #if __BUMP_SUSPECT
        if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_F1)||(WL->arad_gauge>S8_BMP_SUS_ARAD_GAUGE_THRES_F1))
        {
            WL->LFC_n_th_deep_slip_comp_flag=0;
        }
      #endif
      #endif

    }

    if((AFZ_OK==1) && (afz>=(int16_t)(-U8_LOW_MU)) && (WL->LFC_initial_deep_slip_comp_flag==1)) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }
    if(vref <= (int16_t)VREF_15_KPH) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }
}

void LCABS_vDctH2LForRear(void)
{

    if(Rough_road_detect_vehicle==0){
        if(LEFT_WHEEL_wl==1){
            if(FL.LFC_n_th_deep_slip_pre_flag==1){
/*                if(WL->LFC_dump_counter>=(uint16_t)(10)){ */
                if(WL->LFC_dump_counter>=7){
                    WL->LFC_n_th_deep_slip_comp_flag=1;
                }
            }
            else {
                if(WL->LFC_dump_counter>=15){
                    WL->LFC_n_th_deep_slip_comp_flag=1;
                }
            }
        }
        else {
            if(FR.LFC_n_th_deep_slip_pre_flag==1){
/*                if(WL->LFC_dump_counter>=(uint16_t)(10)){ */
                if(WL->LFC_dump_counter>=7){
                    WL->LFC_n_th_deep_slip_comp_flag=1;
                }
            }
            else {
                if(WL->LFC_dump_counter>=15){
                    WL->LFC_n_th_deep_slip_comp_flag=1;
                }
            }
        }

/*        if((WL->accel_arad_avg >= ARAD_5G0)||(WL->arad_gauge>=0)) {      //Added by KGY due to BUMP 030410 */
      #if __BUMP_SUSPECT
        if((WL->accel_arad_avg >= S8_BMP_SUS_ARAD_THRES_R1)||(WL->arad_gauge>S8_BMP_SUS_ARAD_GAUGE_THRES_R1)) {      /*Added by KGY due to BUMP 030410 */
            WL->LFC_n_th_deep_slip_comp_flag=0;
        }
      #endif

    }

    if((AFZ_OK==1) && (afz>=(int16_t)(-U8_LOW_MU)) && (WL->LFC_initial_deep_slip_comp_flag==1)) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }
    if(vref <= (int16_t)VREF_15_KPH) {
        WL->LFC_n_th_deep_slip_comp_flag=0;
    }

}


void LCABS_vDctL2HFromFrontWheel(void)
{
    int16_t LFC_Rise_Dump_counter;

    LFC_Rise_Dump_counter = ((int16_t)WL->LFC_s0_reapply_counter_old2-(int16_t)WL->LFC_dump_counter_old)+(int16_t)WL->LFC_s0_reapply_counter_old;

    if(LEFT_WHEEL_wl == 1) {
        if(WL->LFC_s0_reapply_counter_old>=10){
            WL->LFC_n_th_deep_slip_pre_flag=0;
            RL.LFC_n_th_deep_slip_pre_flag=0;
        }
        else if(((int16_t)WL->LFC_s0_reapply_counter_old2-(int16_t)WL->LFC_dump_counter_old)>=0){
            if(LFC_Rise_Dump_counter>=(int16_t)10){
                WL->LFC_n_th_deep_slip_pre_flag=0;
                RL.LFC_n_th_deep_slip_pre_flag=0;
            }
        }
        else { }
    }
    else {
        if(WL->LFC_s0_reapply_counter_old>=10){
            WL->LFC_n_th_deep_slip_pre_flag=0;
            RR.LFC_n_th_deep_slip_pre_flag=0;
        }
        else if(((int16_t)WL->LFC_s0_reapply_counter_old2-(int16_t)WL->LFC_dump_counter_old)>=0){
            if(LFC_Rise_Dump_counter>=(int16_t)10){
                WL->LFC_n_th_deep_slip_pre_flag=0;
                RR.LFC_n_th_deep_slip_pre_flag=0;
            }
        }
        else { }
    }
}


void LCABS_vPreventBigRiseAndDump(void)
{
    if(Rough_road_detect_vehicle==0){
      if(WL->LFC_built_reapply_counter_delta < -((int16_t)WL->hold_timer_new_ref_old+L_1SCAN)){/*조건추가 030208 */
        if(vref >= (int16_t)VREF_10_KPH) {
          #if __PREV_RISE_DUMP_IMPROVE  
            if(WL->peak_slip<(int8_t)((((int16_t)VREF_5_KPH*100)/vref)-LAM_20P))
            {
                if((int16_t)(WL->peak_slip)<(int16_t)((int16_t)(WL->slip_at_real_dump)*3)) 
                {
                    if(WL->peak_dec_max<(int8_t)(-ARAD_5G0)) 
                    {
                      #if __REORGANIZE_ABS_FOR_MGH60	
                        if(WL->LFC_dump_counter>=(int16_t)U8_BIG_DUMP_BY_LOWMU_FASE_RISE) 
                        {
                            WL->LFC_big_rise_and_dump_flag=1; WL->LFC_big_rise_and_dump_cnt=0;
                        }
                        else if(WL->LFC_built_reapply_flag==1)
                        {
                            /* need to modify cal par. name */
                            if(WL->LFC_dump_counter>=U8_BRD_DUMP_CNT_LEVEL3) {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP9;} 
                            else if(WL->LFC_dump_counter>=U8_BRD_DUMP_CNT_LEVEL2) {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP8;}
                            else if(WL->LFC_dump_counter>=U8_BRD_DUMP_CNT_LEVEL1) {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP7;}
                            else {WL->LFC_big_rise_and_dump_cnt=0;} 
                            
                            if(WL->LFC_big_rise_and_dump_cnt>=U8_BRD_DETECTION_REF) {WL->LFC_big_rise_and_dump_flag=1; WL->LFC_big_rise_and_dump_cnt=0;}
                        }
                        else 
                        {
                            ;
                        }
                      #else 
                        if(WL->LFC_built_reapply_flag==1)
                        {
                            if(WL->LFC_dump_counter>=(int16_t)U8_BIG_DUMP_BY_LOWMU_FASE_RISE) 
                            {
                                WL->LFC_big_rise_and_dump_flag=1; WL->LFC_big_rise_and_dump_cnt=0;
                            }
                            else if(WL->LFC_dump_counter>=(int16_t)U8_BRD_DUMP_CNT_LEVEL1)
                            {
                                if(WL->LFC_dump_counter>=(int16_t)U8_BRD_DUMP_CNT_LEVEL3) {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP9;}
                                else if(WL->LFC_dump_counter>=(int16_t)U8_BRD_DUMP_CNT_LEVEL2) {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP8;}
                                else {WL->LFC_big_rise_and_dump_cnt = WL->LFC_big_rise_and_dump_cnt + U8_BIG_DUMP_GAUGE_AT_DUMP7;}
                                
                                if(WL->LFC_big_rise_and_dump_cnt>=U8_BRD_DETECTION_REF) {WL->LFC_big_rise_and_dump_flag=1; WL->LFC_big_rise_and_dump_cnt=0;}
                            }
                            else 
                            {
                                WL->LFC_big_rise_and_dump_cnt=0;
                            }
                        }
                        else
                        {
                            if(WL->LFC_dump_counter>=(int16_t)U8_BIG_DUMP_BY_LOWMU_FASE_RISE) {WL->LFC_big_rise_and_dump_flag=1;}
                        }  
                      #endif                       
                    }
                }
            }
          #else  
            if(REAR_WHEEL_wl==0) {
                if(WL->LFC_dump_counter>=10) {
                    if(WL->peak_slip<(int8_t)(-LAM_30P)) {
                        if((int16_t)(WL->peak_slip)<(int16_t)((int16_t)(WL->slip_at_real_dump)*3)) {
                            if(WL->peak_dec_max<(int8_t)(-ARAD_5G0)) {WL->LFC_big_rise_and_dump_flag=1;}
                        }
                    }
                }
            }
            else {
                if(WL->LFC_dump_counter>=10) {
                    if(WL->peak_slip<(int8_t)(-LAM_30P)) {
                        if((int16_t)(WL->peak_slip)<(int16_t)((int16_t)(WL->slip_at_real_dump)*3)) {
                            if(WL->peak_dec_max<(int8_t)(-ARAD_5G0)) {WL->LFC_big_rise_and_dump_flag=1;}
                        }
                    }
                }
            }
          #endif  
            /***********************************************For Khron******
            if(WL->Rough_road_detector<=(uint8_t)5) {
                if(WL->peak_dec_max<(int8_t)(-ARAD_10G0)) {
                    WL->LFC_big_rise_and_dump_flag=1;
                }
            }
            ***********************************************For Khron******/
        }
      }
    }
}

#if __CHANGE_MU
void LCABS_vDctLFCMuChange(void)
{
    LCABS_vCalcLFCRiseDump(&FL);
    LCABS_vCalcLFCRiseDump(&FR);

    if(ABS_fz==1){

        if(( FL.LFC_reapply_end_duty>0 )&&( FR.LFC_reapply_end_duty>0 )
            &&(FL.LFC_fictitious_cycle_flag==0)&&(FR.LFC_fictitious_cycle_flag==0)){
            LFC_end_duty_OK_flag = 1;
        }
        else {LFC_end_duty_OK_flag = 0;}

        if((FL.LFC_Dump_Rise_factor>=(int16_t)U8_Low_Mu_Dump_Rise_Factor)&&(FR.LFC_Dump_Rise_factor>=(int16_t)U8_Low_Mu_Dump_Rise_Factor)){
            MSL_SPLIT_CLR=1;  /*  Two wheels are on the low-mu; */
        }
        else if((FL.LFC_initial_deep_slip_comp_flag==1)&&(FR.LFC_initial_deep_slip_comp_flag==1)){
            MSL_SPLIT_CLR=1;
        }
        else if((FL.LFC_n_th_deep_slip_comp_flag==1)&&(FR.LFC_n_th_deep_slip_comp_flag==1)){
            MSL_SPLIT_CLR=1;
        }
        else if((AFZ_OK==1)&&(afz>=-(int16_t)U8_H2SP_DET_LOW_MU)) {MSL_SPLIT_CLR=1;}
        else { }

        if(((AFZ_OK==1)&&(afz<-(int16_t)U8_H2SP_DET_LOW_MU))||(vref<=VREF_3_KPH)) {MSL_SPLIT_CLR=0;}

        LCABS_vDctLFCMuChangeWL(&FL,&FR,&RL,&RR);
        LCABS_vDctLFCMuChangeWL(&FR,&FL,&RR,&RL);

    }
    else {
        MSL_SPLIT_CLR=0;
        LFC_Split_flag=0;
        LFC_Split_suspect_flag=0;
        LFC_H_to_Split_flag=0;
        LFC_L_to_Split_flag=0;
        LFC_GMA_Split_flag=0;
      #if (__EBD_MI_ENABLE==0)
        MSL_BASE_rl=0;
        MSL_BASE_rr=0;
      #endif
      
        SPLIT_PULSE_rl=0;
        SPLIT_PULSE_rr=0;
        SPLIT_JUMP_fl=0;
        SPLIT_JUMP_fr=0;
        SPLIT_JUMP_rl=0;
        SPLIT_JUMP_rr=0;
        FL.LFC_maintain_split_flag=0;
        FR.LFC_maintain_split_flag=0;
        GMA_dumptime_at_H_to_Split=0;
        gma_flags=124;
    }
}

void LCABS_vDctLFCMuChangeWL(struct W_STRUCT *WLf1,struct W_STRUCT *WLf2,struct W_STRUCT *WLr1,struct W_STRUCT *WLr2)
{
    if(GMA_PULSE==1){  /*  GMA Condition */
        if(WLf1->GMA_SLIP==1){
            WLr2->MSL_BASE=1;
            WLr1->MSL_BASE=0;
            WLr2->SPLIT_PULSE=0;
            WLr1->SPLIT_PULSE=1;
            LFC_Split_flag=1;
            LFC_GMA_Split_flag=1;
            LFC_Split_suspect_flag=0;
            LFC_H_to_Split_flag=0;
        }
    }
    else {
        if(LFC_Split_flag==1) { /*  Split */

            if(LFC_GMA_Split_flag==0){ /*  Split Clear Check Condition */
                if((WLf1->LFC_maintain_split_flag==0)&&(WLf2->LFC_maintain_split_flag==0)
                  &&( LFC_end_duty_OK_flag == 1 )){

                    if(WLf1->LFC_reapply_end_duty >= WLf2->LFC_reapply_end_duty) {
                        tempW0 = (int16_t)WLf1->LFC_reapply_end_duty - (int16_t)WLf2->LFC_reapply_end_duty;
                    }
                    else {tempW0 = (int16_t)WLf2->LFC_reapply_end_duty - (int16_t)WLf1->LFC_reapply_end_duty;}

                #if __SPLIT_STABILITY_IMPROVE_GMA  
                  if(lcabsu1ValidYawStrLatG==0)
                #endif  
                  {
                    if(tempW0 <= 3) {
                        if((WLf1->det_count>=WLf2->det_count)&&
                          ((WLf1->det_count-WLf2->det_count)<=L_TIME_140MS)){

                            LFC_Split_flag=0;
                            LFC_H_to_Split_flag=0;
                            LFC_L_to_Split_flag=0;
                            WLr1->SPLIT_JUMP=0;
                            WLr2->SPLIT_JUMP=0;
                            WLr1->SPLIT_PULSE=0;
                            WLr2->SPLIT_PULSE=0;
                            WLr1->gma_dump=0;
                            WLr2->gma_dump=0;
                            gma_flags=101;
                        }
                    }
                }
                #if __SPLIT_STABILITY_IMPROVE_GMA  
                  else
                  {
                    if((WLf1->LEFT_WHEEL==0) && (lcabss16YDFSplitLHcnt >= L_TIME_700MS))
                    {
                        tempW7 = 1; /* RH */
                        tempC1 = L_TIME_110MS;
                    }
                    else if((WLf1->LEFT_WHEEL==1) && (lcabss16YDFSplitRHcnt >= L_TIME_700MS))
                    {
                        tempW7 = 1; /* LH */ 
                        tempC1 = L_TIME_110MS;
                    }
                    else
                    {
                        tempW7 = 3; 
                        tempC1 = L_TIME_140MS;
                    }          
                                        
                    if(tempW0 <= tempW7) {
                        if((WLf1->det_count>=WLf2->det_count)&&
                          ((WLf1->det_count-WLf2->det_count)<=tempC1)){

                            LFC_Split_flag=0;
                            LFC_H_to_Split_flag=0;
                            LFC_L_to_Split_flag=0;
                            WLr1->SPLIT_JUMP=0;
                            WLr2->SPLIT_JUMP=0;
                            WLr1->SPLIT_PULSE=0;
                            WLr2->SPLIT_PULSE=0;
                            WLr1->gma_dump=0;
                            WLr2->gma_dump=0;
                            gma_flags=101;
                        }
                    }
                  }
                #endif  

                }
            }

            if((LFC_L_to_Split_flag==1)||((LFC_GMA_Split_flag==1)&&(LFC_H_to_Split_flag==0))){ /* Not split but High-mu */
                if((AFZ_OK==1)&&(afz <= (int16_t)(-AFZ_0G6))){
                    LFC_Split_flag=0;
                    LFC_L_to_Split_flag=0;
                    LFC_GMA_Split_flag=0;
                    WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=0;
                    WLr1->gma_dump=0;
                    WLr2->gma_dump=0;
                    gma_flags=108;
                }
            }

            if((LFC_H_to_Split_flag==1)&&(MSL_SPLIT_CLR==1)){ /*  Not split but Low-mu */
                if((WLf1->LFC_maintain_split_flag==0)&&(WLf2->LFC_maintain_split_flag==0)){
                    LFC_Split_flag=0;
                    LFC_H_to_Split_flag=0;
                    WLr1->SPLIT_JUMP=0;
                    WLr2->SPLIT_JUMP=0;
                    WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=0;
                    WLr1->gma_dump=0;
                    WLr2->gma_dump=0;
                    gma_flags=109;
                }
            }

            if((LFC_H_to_Split_flag==1)&&(WLr1->MSL_BASE==1) /*  Not split but High-mu */
              &&((WLf1->max_arad_instbl>=ARAD_20G0) 
              ||((WLf1->max_arad_instbl>=ARAD_15G0)&&(WLf1->LFC_dump_counter<=15)&&(WLf1->STABIL==1))))
            { 
                    LFC_Split_flag=0;
                    LFC_H_to_Split_flag=0;
                    WLr1->SPLIT_JUMP=0;
                    WLr2->SPLIT_JUMP=0;
                    WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=0;
                    WLr1->gma_dump=0;
                    WLr2->gma_dump=0;
                    gma_flags=100;
            }

            if(CIRCUIT_FAIL==0){  /* Chess condition */
                if((WLr1->MSL_BASE==1)&&((WLf2->LFC_Dump_Rise_factor - WLf1->LFC_Dump_Rise_factor) >= (int16_t)U8_Chess_Dump_Rise_Ref )){
                    LFC_Split_flag=1;
                    WLr1->MSL_BASE=0;
                    WLr2->MSL_BASE=1;
                    WLr1->SPLIT_JUMP=0;
                    WLr2->SPLIT_JUMP=0;
                    WLr1->SPLIT_PULSE=1;
                    WLr2->SPLIT_PULSE=0;
                    WLf1->LFC_maintain_split_flag=1;
                    WLf2->LFC_maintain_split_flag=1;
                    gma_flags=102;
                }
            }
        }

        else if (LFC_Split_suspect_flag==1) {   /*  Split Suspect */

            if( LFC_end_duty_OK_flag == 1 ){  /*  Non Split or Split Check Condition */
                if((WLf1->LFC_maintain_split_flag==0)&&(WLf2->LFC_maintain_split_flag==0)){
                    if((WLf1->LFC_reapply_end_duty>=(WLf2->LFC_reapply_end_duty+3))||
                       (WLf2->LFC_reapply_end_duty>=(WLf1->LFC_reapply_end_duty+3))){
                        LFC_Split_suspect_flag=1;
                        gma_flags=104;
                        if(WLf1->LFC_reapply_end_duty>=(WLf2->LFC_reapply_end_duty+6)){
                            LFC_Split_flag=1;
                            LFC_L_to_Split_flag=1;
                            LFC_Split_suspect_flag=0;
                            WLr1->MSL_BASE=1;
                            WLr2->MSL_BASE=0;
                            WLr1->SPLIT_PULSE=0;
                            WLr2->SPLIT_PULSE=1;
                            gma_flags=105;
                        }
                    }
                    else {
                        LFC_Split_suspect_flag=0;
                        WLr1->SPLIT_PULSE=0;
                        WLr2->SPLIT_PULSE=0;
                        WLr1->gma_dump=0;
                        WLr2->gma_dump=0;
                        gma_flags=106;
                    }
                    if((AFZ_OK==1)&&(afz <= (int16_t)(-AFZ_0G6))){ /* Not split but High-mu */
                        LFC_Split_flag=0;
                        LFC_Split_suspect_flag=0;
                        LFC_L_to_Split_flag=0;
                        WLr1->SPLIT_PULSE=0;
                        WLr2->SPLIT_PULSE=0;
                        WLr1->gma_dump=0;
                        WLr2->gma_dump=0;
                        gma_flags=107;
                    }
                }
            }
        }

        else {                  /*  Non Split */

            if((MSL_SPLIT_CLR==0)||((AFZ_OK == 1)&&(afz <= (int16_t)(-S8_H_to_Split_Detection_AFZ)))){
                                                /* High to Split Check Condition */
                tempW1=WLf2->vrad - WLf1->vrad;
                tempW2=WLr2->vrad - WLr1->vrad;
                tempW4=(int16_t)WLf2->rel_lam - (int16_t)WLf1->rel_lam;
                tempW5=(int16_t)U8_H2SPLIT_VDIFF_F1*8;
                tempW6=(int16_t)U8_H2SPLIT_VDIFF_R1*8;
                tempW7=(int16_t)U8_H2SPLIT_VDIFF_F2*8;
                tempW8=(int16_t)U8_H2SPLIT_VDIFF_R2*8;

                if((((WLf1->LFC_Dump_Rise_factor-WLf2->LFC_Dump_Rise_factor)>=(int16_t)S8_H_to_Split_Dump_Diff)&&(WLf1->max_arad_instbl<ARAD_15G0)
                  &&((tempW1>=(int16_t)S16_H2SP_DETECT_FRONT_VRAD_DIFF1)||((WLf1->peak_dec_max<=S8_H2SP_DETECT_FRONT_DECEL1)&&(tempW1>=VREF_10_KPH))))
                  ||((tempW1>=(int16_t)S16_H2SP_DETECT_FRONT_VRAD_DIFF2)&&(WLf1->peak_dec_max<=S8_H2SP_DETECT_FRONT_DECEL2)))
                {
                    WLr1->MSL_BASE=1;
                    WLr2->MSL_BASE=0;
                    WLr1->MSL_DUMP=0;
                    WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=0;
                    WLf1->SPLIT_JUMP=0;
                    WLf2->SPLIT_JUMP=1;
                    WLr1->SPLIT_JUMP=1;
                    WLr2->SPLIT_JUMP=0;
                    LFC_Split_flag=1;
                    LFC_H_to_Split_flag=1;
/*                   H_to_Split_Front_Dump=1; */
                    LFC_Split_suspect_flag=0;
                    WLf1->LFC_maintain_split_flag=1;
                  #if __FRONT_SL_DUMP

                    WLf2->gma_dump1=(uint8_t)LCESP_s16IInter4Point(vref,VREF_20_KPH, 0,
                                                               			VREF_35_KPH, (int16_t)U8_F_High_side_SL_dump_Bw35KPH,  
                                                               			VREF_70_KPH, (int16_t)U8_F_High_side_SL_dump,  
                                                               			VREF_100_KPH,(int16_t)U8_F_High_side_SL_dump_Ab70KPH);  
                    #if (__FRONT_OUT_ABS_SL_DUMP==1)
                    if(ABS_wl==0)
                    {
                        if(vref>=VREF_70_KPH)
                        {                    
                            WLf2->gma_dump1 = (uint8_t)(((uint16_t)WLf2->gma_dump1*3)/4);
                            WLf2->gma_dump1 = (WLf2->gma_dump1>5)?5:WLf2->gma_dump1;
                        }
                        else
                        {
                            WLf2->gma_dump1 = 0;
                        }
                    }
                    #endif
                           

                    GMA_dumptime_at_H_to_Split = WLf2->gma_dump1;
                  #endif
                    gma_flags=111;
                }
                
                else if((vref>=VREF_40_KPH)&&(BEND_DETECT2==0)&&(Rough_road_detect_vehicle==0)
                	 &&(Rough_road_suspect_vehicle==0)&&(tempW4>=(int16_t)U8_H2SPLIT_SLIP_DIFF)               	
                	 &&(((tempW1>=tempW5)&&(tempW2>=tempW6))
                	  ||((tempW1>=tempW7)&&(tempW2>=tempW8))))
                {
                    WLr1->MSL_BASE=1;
                    WLr2->MSL_BASE=0;
                    WLr1->MSL_DUMP=0;
                    WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=0;
                    WLf1->SPLIT_JUMP=0;
                    WLf2->SPLIT_JUMP=1;
                    WLr1->SPLIT_JUMP=1;
                    WLr2->SPLIT_JUMP=0;
                    LFC_Split_flag=1;
                    LFC_H_to_Split_flag=1;
/*                  H_to_Split_Front_Dump=1; */
                    LFC_Split_suspect_flag=0;
                    WLf1->LFC_maintain_split_flag=1;
                  #if __FRONT_SL_DUMP

                    tempW3=LCESP_s16IInter4Point(vref,VREF_20_KPH, 0,
                                                      VREF_35_KPH, (int16_t)U8_F_High_side_SL_dump_Bw35KPH,  
                                                      VREF_70_KPH, (int16_t)U8_F_High_side_SL_dump,  
                                                      VREF_100_KPH,(int16_t)U8_F_High_side_SL_dump_Ab70KPH);  

                    WLf2->gma_dump1 = (uint8_t)(tempW3/2);  

                    #if (__FRONT_OUT_ABS_SL_DUMP==1)
                    if(ABS_wl==0)
                    {
                        if(vref>=VREF_70_KPH)
                        {                    
                            WLf2->gma_dump1 = (uint8_t)(((uint16_t)WLf2->gma_dump1*3)/4);
                            WLf2->gma_dump1 = (WLf2->gma_dump1>3)?3:WLf2->gma_dump1;
                        }
                        else
                        {
                            WLf2->gma_dump1 = 0;
                        }
                    }
                    #endif
                    
                    GMA_dumptime_at_H_to_Split = WLf2->gma_dump1;
                    
                  #endif
                    gma_flags=110;
                }
                else { }
            }

            else if(vref>=VREF_6_KPH){
                if(LFC_end_duty_OK_flag == 0){      /*  1st cycle */
                                            /*  Low to split Check Condition at 1st cycle */
                    if((WLf1->LFC_zyklus_z==1)&&(WLf1->LFC_reapply_current_duty>0)&&(WLf2->LFC_fictitious_cycle_flag==0)&&(WLf2->LFC_reapply_end_duty>0)){
                        if(WLf2->LFC_reapply_end_duty>=(WLf1->LFC_reapply_current_duty+(uint8_t)S8_L_to_Split_Duty_Diff2)){
                            WLr1->MSL_BASE=0;
                            WLr2->MSL_BASE=1;
                            WLr1->SPLIT_PULSE=1;
                            WLr2->SPLIT_PULSE=0;
                            LFC_Split_flag=0;
                            LFC_Split_suspect_flag=1;
                            WLf1->LFC_maintain_split_flag=1;
                            gma_flags=112;
                        }
                    }

                    else if((WLf1->LFC_zyklus_z==1)&&(WLf2->LFC_zyklus_z==1)&&(WLf1->LFC_reapply_current_duty>0)&&(WLf2->LFC_reapply_current_duty>0)){
                        if(WLf2->LFC_reapply_current_duty>=(WLf1->LFC_reapply_current_duty+(uint8_t)S8_L_to_Split_Duty_Diff2)){
                            WLr1->MSL_BASE=0;
                            WLr2->MSL_BASE=1;
                            WLr1->SPLIT_PULSE=1;
                            WLr2->SPLIT_PULSE=0;
                            LFC_Split_flag=0;
                            LFC_Split_suspect_flag=1;
                            WLf1->LFC_maintain_split_flag=1;
                            gma_flags=113;
                        }
                    }
                    else { }

                    if((WLf1->LFC_reapply_end_duty > 0) &&
                        (WLf1->LFC_reapply_end_duty>=(WLf1->LFC_reapply_current_duty+(uint8_t)S8_L_to_Split_Duty_Diff1))){
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        WLr1->SPLIT_PULSE=1;
                        WLr2->SPLIT_PULSE=0;
                        LFC_Split_flag=0;
                        LFC_Split_suspect_flag=1;
                        WLf1->LFC_maintain_split_flag=1;
                        gma_flags=114;
                    }
                }
                else{
                                             /*  Low to High Check Condition */
                    if(WLf1->LFC_reapply_end_duty>=(WLf1->LFC_reapply_current_duty+(uint8_t)S8_L_to_Split_Duty_Diff1)){
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        WLr1->SPLIT_PULSE=1;
                        WLr2->SPLIT_PULSE=0;
                        LFC_Split_flag=0;
                        LFC_Split_suspect_flag=1;
                        WLf1->LFC_maintain_split_flag=1;
                        gma_flags=115;
                    }
                }
            }
            else { }
        }
    }
}

void LCABS_vCheckLFCBaseWheel(void)
{
  #if __EBD_MI_ENABLE
    if((EBD_RA==1) || (ABS_fz==1))
  #else
    if(ABS_fz==1)
  #endif
    {
        if((MSL_BASE_rl==0)&&(MSL_BASE_rr==0)) {LCABS_vCheckLFCBaseWheelInitial();}
        LCABS_vCheckLFCBaseWheelWL(&RL,&RR);
        LCABS_vCheckLFCBaseWheelWL(&RR,&RL);
        if(MSL_BASE_rl==1) {
            RL.gma_dump=0;
            SPLIT_PULSE_rl=0;
        }
        else if(MSL_BASE_rr==1) {
            RR.gma_dump=0;
            SPLIT_PULSE_rr=0;
        }
        else { }
    }
  #if __EBD_MI_ENABLE
    else
    {
        MSL_BASE_rl=0;
        MSL_BASE_rr=0;
    }
  #endif
}

void LCABS_vCheckLFCBaseWheelWL(struct W_STRUCT *WLr1,struct W_STRUCT *WLr2)
{
	int16_t s16AbsHigherPOfNbase; /* For base wheel selection by target P*/

  #if __EBD_MI_ENABLE
    if(ABS_fz==1)
    {
  #endif
        if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)) {
            if(WLr1->MSL_BASE==1){
                if(WLr2->STABIL==0) {
                    if((WLr1->rel_lam-WLr2->rel_lam)>=(int16_t)(LAM_5P)){
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        WLr1->SPLIT_JUMP=0;
                        WLr2->SPLIT_JUMP=0;
                        WLr1->SPLIT_PULSE=1;
                        WLr2->SPLIT_PULSE=0;
                        FL.LFC_maintain_split_flag=1;
                        FR.LFC_maintain_split_flag=1;
                        gma_flags=103;
                    }
                }
            }
        }
        else{
            if(WLr1->MSL_BASE==1)
			{
				if (((WLr1->LFC_zyklus_z==0) || ((WLr1->LFC_zyklus_z==1) && (WLr1->s0_reapply_counter>=1))) && ((WLr2->LFC_zyklus_z==0) ||((WLr2->LFC_zyklus_z==1) && (WLr2->s0_reapply_counter>=1))))
				{
                    if((WLr1->STABIL==1)&&(WLr2->STABIL==0)) {
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=116;
                    }
                }
				else if( ((WLr1->LFC_zyklus_z == 0) || ((WLr1->LFC_zyklus_z==1) && (WLr1->s0_reapply_counter>=1)))  /*s0_reapply counter(Rise/Dump) or STABIL_wl(Rise/dump)*/
					&& (((WLr2->LFC_zyklus_z==1) && (WLr2->s0_reapply_counter==0)) || (WLr2->LFC_zyklus_z>=2)) )
				{
                    WLr1->MSL_BASE=0;
                    WLr2->MSL_BASE=1;
                    gma_flags=117;
                }
				else if((WLr1->LFC_zyklus_z>=1)&&(WLr2->LFC_zyklus_z>=1)&&(WLr1->ABS_LIMIT_SPEED==0))
				{
					s16AbsHigherPOfNbase = (WLr2->Estimated_Press_SKID > WLr2->s16_Estimated_Active_Press) ? WLr2->Estimated_Press_SKID : WLr2->s16_Estimated_Active_Press;


					if(s16AbsHigherPOfNbase < WLr1->s16_Estimated_Active_Press)
					{
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=118;
                    }
                }
                else { }
            }
            
          #if   __VDC
            if(((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1))&&(det_alatm<=LAT_0G5G)
              &&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
            {
            	if(WLr1->MSL_BASE==1)
                {
                	WLr1->SPLIT_PULSE=0;
                    WLr2->SPLIT_PULSE=1;
                    gma_flags=119;
                }
                else{}
            }
            else{}
          #endif
        }
  #if __EBD_MI_ENABLE
    }
    else
    {
        if(WLr1->MSL_BASE==1)
        {
            if((WLr2->EBD==1) && (WLr1->EBD==0)) /* 1 wheel EBD engaged */
            {
                WLr1->MSL_BASE=0;
                WLr2->MSL_BASE=1;
                gma_flags=130;
            }
            else if((WLr2->EBD==1) && (WLr1->EBD==1)) /* both EBD engaged */
            {
                if((WLr2->s0_reapply_counter) <= (WLr1->s0_reapply_counter)) /* Base wheel pressure must be lower */
                {
                    tempB0 = (WLr1->rel_lam-WLr2->rel_lam);
                    
                    if((WLr2->EBD_MODE==HOLD_MODE) && (WLr1->EBD_MODE==REAPPLY_MODE)) /* EBD 1st scan */
                    {
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=131;
                    }
                    else if((WLr2->EBD_MODE==DUMP_START) && (tempB0>=(int16_t)(LAM_1P))) /* Dump */
                    {
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=132;
                    }
                    else if((WLr1->rel_lam-WLr2->rel_lam)>=(int16_t)(LAM_5P))
                    {
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=134;
                    }
                    else { }
                }
            }
            else { }
          /*
            else if((WLr2->EBD==0) && (WLr1->EBD==0)) / no EBD Ctrl, maybe needed for further rear wheel functions
            {
                if((WLr2->AV_DUMP==1) && (WLr1->AV_DUMP==0)) / Some rear wheel control - don't know, but possibly due to slip
                {
                    WLr1->MSL_BASE=0;
                    WLr2->MSL_BASE=1;
                    gma_flags=135;
                }
                else if((WLr2->AV_DUMP==1) && (WLr1->AV_DUMP==1))
                {
                    if((WLr1->rel_lam-WLr2->rel_lam)>=(int16_t)(LAM_1P))
                    {
                        WLr1->MSL_BASE=0;
                        WLr2->MSL_BASE=1;
                        gma_flags=136;
                    }
                }
                else if((WLr1->rel_lam-WLr2->rel_lam)>=(int16_t)(LAM_5P))
                {
                    WLr1->MSL_BASE=0;
                    WLr2->MSL_BASE=1;
                    gma_flags=137;
                }
                else { }
            }
          */
        }
    }
  #endif /*   #if __EBD_MI_ENABLE*/
}

void LCABS_vCheckLFCBaseWheelInitial(void)
{
/*     if((MSL_BASE_rl==0)&&(MSL_BASE_rr==0)) { */
        if((ABS_rl==1)&&(ABS_rr==0)) {
            MSL_BASE_rl=1;
        }
        else if((ABS_rr==1)&&(ABS_rl==0)) {
            MSL_BASE_rr=1;
        }
        else if((ABS_rr==1)&&(ABS_rl==1)) {
            if((state_rl==UNSTABLE)||(state_rr==UNSTABLE)) {
                if(vrad_rl>=vrad_rr) {
                    MSL_BASE_rr=1;
                }
                else {
                    MSL_BASE_rl=1;
                }
            }
        }
        else {
            if(AV_DUMP_rl==1) {
                MSL_BASE_rl=1;
            }
            else if(AV_DUMP_rr==1) {
                MSL_BASE_rr=1;
            }
            else 
            { 
              #if __EBD_MI_ENABLE
                if(rel_lam_rl > rel_lam_rr)
                {
                    MSL_BASE_rr=1;
                }
                else if(rel_lam_rl < rel_lam_rr)
                {
                    MSL_BASE_rl=1;
                }
                else
                {
                    ;
                }
              #endif /* #if __EBD_MI_ENABLE */
                ;
            }
        }
/*     } */
}

void LCABS_vCalcLFCRiseDump(struct W_STRUCT *WL)
{
    if((FSF_wl==1) && (ABS_wl==0)) {
        WL->LFC_Dump_Rise_factor=0;
        WL->det_count=0;
    }
    else {
        if(WL->LFC_built_reapply_flag == 1){
            WL->LFC_Dump_Rise_factor = (int16_t)WL->Mu_change_Dump_counter-(int16_t)WL->LFC_s0_reapply_counter;
        }
        else{
            WL->LFC_Dump_Rise_factor = ((int16_t)WL->Mu_change_Dump_counter_old
                                     - (int16_t)WL->LFC_s0_reapply_counter_old)+(int16_t)WL->Mu_change_Dump_counter;
        }
        if( WL->LFC_Dump_Rise_factor >= 60 ) {WL->LFC_Dump_Rise_factor = 60;}
        if( WL->LFC_Dump_Rise_factor <= 0  ) {WL->LFC_Dump_Rise_factor = 0;}

        if((WL->AV_DUMP==1)&&(WL->det_count < L_TIME_700MS)&&(WL->GMA_SLIP==0)&&(WL->SPLIT_JUMP==0)) 
        {
        	if(WL->lcabss8DumpCase==U8_L_DUMP_CASE_11)
        	{
        		WL->det_count +=L_4SCAN;
        	}
        	else
        	{
        		WL->det_count +=L_2SCAN;
        	}
        }
        if((WL->AV_REAPPLY==1)&&(WL->det_count >= L_2SCAN)&&(WL->Reapply_Accel_flag==0)) {
            if((WL->LFC_zyklus_z==1)||(WL->LFC_fictitious_cycle_flag==1)) {WL->det_count -=L_1SCAN;}
            else {WL->det_count -=L_2SCAN;}
            if(WL->det_count <= L_TIME_30MS) {WL->det_count=0;}
        }
    }
}

#endif

#if __SPLIT_VERIFY
void LCABS_vVerifySplitByFrontHSide(struct W_STRUCT *WL)
{
    uint8_t Unstable_time;
  #if   __VDC && __ABS_COMB
    if(WL->L_SLIP_CONTROL==0) {
  #endif

    if(ABS_fz==0) {WL->PULSE_DOWN_Forbid_flag=0;}
    else if(LFC_Split_flag==1) {
          #if __UNSTABLE_REAPPLY_ENABLE
        if((REAR_WHEEL_wl==0) && ((WL->LFC_built_reapply_counter==L_1ST_SCAN) || ((WL->Reapply_Accel_flag==1)&&(WL->Reapply_Accel_1st_scan==1))))
          #else
        if((REAR_WHEEL_wl==0) && (WL->LFC_built_reapply_counter==L_1ST_SCAN))
          #endif
        {
            if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1))) {
                if(WL->LFC_built_reapply_flag == 1) {Unstable_time = WL->LFC_UNSTABIL_counter_old;}
                else {Unstable_time = WL->LFC_UNSTABIL_counter;}

                if((Unstable_time>=L_TIME_210MS)&&(WL->arad_gauge<-20)&&(WL->LFC_dump_counter>=4))
                {
/*                  if(WL->Pulse_down_flag==1) {WL->PULSE_DOWN_Forbid_flag=1;} */
                    WL->PULSE_DOWN_Forbid_flag=1;
                }
              #if __PRESS_EST_ABS
                else if((WL->LOW_MU_SUSPECT_by_IndexMu==1) || ((WL->IndexMu<=IndexBasalt) && (FSF_wl==0)))
                {
                    WL->PULSE_DOWN_Forbid_flag=1;
                }
              #endif
                else { }
            }
            else if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))) {
                WL->PULSE_DOWN_Forbid_flag=0;
            }
            else { }
        }
        else if(REAR_WHEEL_wl==1) {WL->PULSE_DOWN_Forbid_flag=0;}
        else { }
    }
    else {WL->PULSE_DOWN_Forbid_flag=0;}
  #if   __VDC && __ABS_COMB
    }
  #endif
}



  #if __VDC  && (__MGH60_CYCLE_TIME_OPT_ABS==DISABLE)
void LCABS_vVerifyWheelMoment(void)
{
    LCABS_vVerifyWheelMomentWL(&FL);
    LCABS_vVerifyWheelMomentWL(&FR);
    LCABS_vVerifyWheelMomentWL(&RL);
    LCABS_vVerifyWheelMomentWL(&RR);
}

void LCABS_vVerifyWheelMomentWL(struct W_STRUCT *WL)
{

    if((ABS_fz==0) && (FSF_wl==1)) {
        WL->Wheel_Inertia_coeff = 0;
        WL->dP_EST = 0;
        WL->arad_ref_at_dump = 0;
        WL->Sum_of_dP_EST = 0;
        WL->Sum_of_delta_arad = 0;
    }

    else if(WL->state != DETECTION) {
        if(HV_VL_wl==1) {
            if(AV_VL_wl==1) {
                if(WL->dP_EST < 150) {WL->dP_EST += 10; }
            }
        }
        else if(AV_VL_wl==0) {
            if(WL->dP_EST >= 7) {WL->dP_EST -= 7;}
        }
        else { }

        WL->Sum_of_dP_EST = WL->Sum_of_dP_EST + (int16_t)WL->dP_EST;

        if(STAB_A_wl==1) {
            WL->arad_ref_at_dump = WL->arad_alt + WL->arad;
        }
        else if(WL->LFC_UNSTABIL_counter==L_2SCAN) {
            WL->arad_ref_at_dump = (int8_t)(((int16_t)WL->arad_ref_at_dump + WL->arad)/3);
            WL->Sum_of_delta_arad = (((int16_t)WL->arad_ref_at_dump*2) - (int16_t)WL->arad_alt - (int16_t)WL->arad);
        }
        else {
            WL->Sum_of_delta_arad = WL->Sum_of_delta_arad + ((int16_t)WL->arad_ref_at_dump - WL->arad);
        }
    }
    else if(STAB_K_wl==1) {
        if(WL->Sum_of_dP_EST>10)
        {
            if(WL->LFC_UNSTABIL_counter>0)
            {
                WL->Wheel_Inertia_coeff = (((WL->Sum_of_delta_arad*10) / (WL->Sum_of_dP_EST/10))*10) / (int16_t)WL->LFC_UNSTABIL_counter;
            }
            else if(WL->LFC_UNSTABIL_counter_old>0)
            {
                WL->Wheel_Inertia_coeff = (((WL->Sum_of_delta_arad*10) / (WL->Sum_of_dP_EST/10))*10) / (int16_t)WL->LFC_UNSTABIL_counter_old;
            }
            else
            {
                WL->Wheel_Inertia_coeff = (((WL->Sum_of_delta_arad*10) / (WL->Sum_of_dP_EST/10))*10);
            }
        }
        else
        {
            WL->Wheel_Inertia_coeff = WL->Sum_of_delta_arad*100; /*to prevent division by zero */
        }

        WL->dP_EST = 0;
        WL->arad_ref_at_dump = 0;
        WL->Sum_of_dP_EST = 0;
        WL->Sum_of_delta_arad = 0;
    }
    else { }
}
  #endif /*__VDC  && (__MGH60_CYCLE_TIME_OPT_ABS==DISABLE)*/
#endif /*__SPLIT_VERIFY*/

/******************************************************************************/

#if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
void LCABS_vDctDriverInputToAccel(void)
{
    
    if(ABS_fz==1)
    {
      #if __VDC
        if((BRAKE_BY_MPRESS_FLG==0) && 
           ((ABS_BEFORE_MPRESS_BRAKE==1) 
           || ((ABS_DURING_MPRESS_FLG==1)&&(BRAKE_BY_MPRESS_FLG==0)) 
           || ((ABS_DURING_ACTIVE_BRAKE_FLG==1)&&(ACTIVE_BRAKE_FLG==0)))
          )
      #else
        if(lcabsu1BrakeByBLS==0)
      #endif
        {
            lcabsu1BrakeReleasePossible = 1;
        }
        else
        {
            lcabsu1BrakeReleasePossible = 0;
        }
    }
    else
    {
        lcabsu1BrakeReleasePossible = 0;
    }

    if(lcabsu1ValidCan==1)
    {
        if(AUTO_TM==1)
        {
            if(gs_pos>0)
            {
                lcabsu1EngageDriveline = 1;
            }
            else
            {
                lcabsu1EngageDriveline = 0;
            }
        }
        else
        {
          #if (__4WD_VARIANT_CODE==ENABLE)
            if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H))
            {
                tempB7 = 4;
            }
            else
            {
                tempB7 = 2;
            }
          #elif __4WD
            tempB7 = 4;
          #else
            tempB7 = 2;
          #endif
            
            if((ABS_fz==1) && (mtp > (int16_t)S8_DetectAccel_Reset_Throttle) && (vref >= VREF_0_25_KPH))
            {
                if(lcabss8VrefIncreaseCnt==0)
                {
                    if(vref >= (vref_alt+VREF_0_125_KPH))
                    {
                        if(tempB7 == 2)
                        {
                          #if __REAR_D
                            if((vrad_rl>vrad_fl) && (vrad_rl>vrad_fr) && (vrad_rr>vrad_fl) && (vrad_rr>vrad_fr))
                          #else
                            if((vrad_fl>vrad_rl) && (vrad_fl>vrad_rr) && (vrad_fr>vrad_rl) && (vrad_fr>vrad_rr))
                          #endif
                            {
                                lcabss8VrefIncreaseCnt = lcabss8VrefIncreaseCnt+L_1SCAN;
                            }
                        }
                        else
                        {
                            lcabss8VrefIncreaseCnt = lcabss8VrefIncreaseCnt+L_1SCAN;
                        }
                    }
                }
                else
                {
                    if(vref >= vref_alt)
                    {
                        if(tempB7 == 2)
                        {
                          #if __REAR_D
                            if((vrad_rl>vrad_fl) && (vrad_rl>vrad_fr) && (vrad_rr>vrad_fl) && (vrad_rr>vrad_fr))
                          #else
                            if((vrad_fl>vrad_rl) && (vrad_fl>vrad_rr) && (vrad_fr>vrad_rl) && (vrad_fr>vrad_rr))
                          #endif
                            {
                                lcabss8VrefIncreaseCnt = lcabss8VrefIncreaseCnt+L_1SCAN;
                            }
                            else
                            {
                                lcabss8VrefIncreaseCnt = lcabss8VrefIncreaseCnt-L_1SCAN;
                            }
                        }
                        else
                        {
                            lcabss8VrefIncreaseCnt = lcabss8VrefIncreaseCnt+L_1SCAN;
                        }
                    }
                    else
                    {
                        lcabss8VrefIncreaseCnt = 0;
                    }
                }
            }
            else
            {
                lcabss8VrefIncreaseCnt = 0;
            }
            
            if(lcabss8VrefIncreaseCnt >= L_TIME_70MS) 
            {
                lcabsu1EngageDriveline = 1;
                lcabss8VrefIncreaseCnt = L_TIME_70MS;
            }
            else if(lcabss8VrefIncreaseCnt <= 0)
            {
                lcabsu1EngageDriveline = 0;
            }
            else { }
        }
    }
    else
    {
        lcabsu1EngageDriveline = 0;
    }
    
    if((lcabsu1BrakeReleasePossible==1) && (lcabsu1EngageDriveline==1))
    {
        if(lcabss8AccelInputCnt==0)
        {
            if((mtp >= (int16_t)S8_DetectAccel_Throttle_P3) && (eng_torq_rel >= ((int16_t)S8_DetectAccel_EngTorq_L3*10)))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt + L_5SCAN;
            }
            else if((mtp >= (int16_t)S8_DetectAccel_Throttle_P2) && (eng_torq_rel >= ((int16_t)S8_DetectAccel_EngTorq_L2*10)))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt + L_3SCAN;
            }
            else { }
        }
        else
        {
            if((mtp >= (int16_t)S8_DetectAccel_Throttle_P3) && (eng_torq_rel >= ((int16_t)S8_DetectAccel_EngTorq_L3*10)))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt + L_5SCAN;
            }
            else if((mtp >= (int16_t)S8_DetectAccel_Throttle_P2) && (eng_torq_rel >= ((int16_t)S8_DetectAccel_EngTorq_L2*10)))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt + L_3SCAN;
            }
            else if((mtp >= (int16_t)S8_DetectAccel_Throttle_P1) && (eng_torq_rel >= ((int16_t)S8_DetectAccel_EngTorq_L1*10)))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt + L_1SCAN;
            }
            else if(mtp <= (int16_t)S8_DetectAccel_Reset_Throttle)
            {
                lcabss8AccelInputCnt = 0;
            }
            else if(eng_torq_rel <= ((int16_t)S8_DetectAccel_Reset_EngTorq*10))
            {
                lcabss8AccelInputCnt = lcabss8AccelInputCnt - L_1SCAN;
            }
            else { }        
        }

        if(lcabss8AccelInputCnt >= L_TIME_200MS)
        {
            lcabsu1SuspctAccelIntention = 1;
            lcabss8AccelInputCnt = L_TIME_200MS;
        }
        else if(lcabss8AccelInputCnt <= L_TIME_50MS)
        {
            lcabsu1SuspctAccelIntention = 0;
        }
        else { }    
    }
    else
    {
        lcabsu1SuspctAccelIntention = 0;
        lcabss8AccelInputCnt = 0;
    }
}
#endif

  #if __DETECT_LOCK_IN_CYCLE
void LCABS_vDctPossibleLockInCycle(void)
{
    LCABS_vDctPossibleLockInCycleWL(&FL);
    LCABS_vDctPossibleLockInCycleWL(&FR);
    LCABS_vDctPossibleLockInCycleWL(&RL);
    LCABS_vDctPossibleLockInCycleWL(&RR);
}

void LCABS_vDctPossibleLockInCycleWL(struct W_STRUCT *WL)
{
    if(STABIL_wl==0)
    {
        if(STAB_A_wl==0)
        {
            if(WL->lcabsu1PossibleWLLockInCycle==0)
            {
                if((McrAbs(WL->arad - WL->arad_alt)) <= ARAD_0G0)
                {
                    if(WL->lcabsu8NoChangeInArad < U8_TYPE_MAXNUM)
                    {
                        WL->lcabsu8NoChangeInArad = WL->lcabsu8NoChangeInArad + L_1SCAN;
                    }
                }
                else
                {
                    WL->lcabsu8NoChangeInArad=0;
                }
            }
    
            if(WL->lcabsu8NoChangeInArad > L_TIME_150MS)
            {
                WL->lcabsu1PossibleWLLockInCycle=1;
            }
            else { }
        }
        else
        {
            WL->lcabsu1PossibleWLLockInCycle=0;
            WL->lcabsu8NoChangeInArad=0;
        }
    }
}
  #endif /*__DETECT_LOCK_IN_CYCLE*/
    
#if __IMPROVE_DECEL_AT_CERTAIN_HMU
static void LCABS_vDctCertainHMu(void)
{
    if(ABS_fz==1)
    {
        if((LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0)
	  #if __VDC
	     &&(det_alatm<=LAT_0G1G)
	  #endif
		)
        {
            if((AFZ_OK==1)&&(afz<-(int16_t)U8_PULSE_HIGH_MU))
            {
				lcabsu1CertainHMuPre = 1;
				if( ((FL.LFC_zyklus_z>=3)&&(FR.LFC_zyklus_z>=3))||(FL.LFC_zyklus_z>=4)||(FR.LFC_zyklus_z>=4) )
                {
                    lcabsu1CertainHMu=1;
					lcabsu1CertainHMuPre = 0;
                }
                else
                {
                    lcabsu1CertainHMu=0;
                }
            }
            else
            {
                lcabsu1CertainHMu = 0;

        		#if __VDC && __EST_MU_FROM_SKID
				if( (FL.Estimated_Press_SKID>=MPRESS_70BAR)&&(FR.Estimated_Press_SKID>=MPRESS_70BAR) )
                {
                    lcabsu1CertainHMuPre = 1;
                }
        		else if( (FL.lcabsu8PossibleMuFromSkid>=U8_PULSE_MED_HIGH_MU)&&(FR.lcabsu8PossibleMuFromSkid>=U8_PULSE_MED_HIGH_MU) )
             	{
         	        lcabsu1CertainHMuPre = 1;
         	    }
         	    else
         	    {
         	        lcabsu1CertainHMuPre = 0;
         	    }
		        #endif /* __VDC && __EST_MU_FROM_SKID */
            }
        }
        else
        {
            lcabsu1CertainHMu=0;
            lcabsu1CertainHMuPre=0;
        }
    }
    else
    {
        lcabsu1CertainHMu=0;
        lcabsu1CertainHMuPre=0;
    }
}
#endif
  
void LCABS_vDecideVehicleCtrlState(void)
{

/*  #if !__VDC */
    if((lcabsu1BrakeByBLS==0)                         /* ENGINE BRAKING */
      #if __VDC
    && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
    && (ACTIVE_BRAKE_FLG==0)                /* HDC||ACC||EPB braking */
      #endif
    )
    {
      #if __REAR_D
        if(ABS_fz==1) {
            if((ABS_fl==0) && (ABS_fr==0)) {
                if((ABS_rl==1) || (ABS_rr==1)) {
                    if(eng_brk_timer >= L_1SCAN) {eng_brk_timer = eng_brk_timer - L_1SCAN;}
                }
            }
        }
        else {eng_brk_timer=L_TIME_500MS;}

        if(eng_brk_timer==0) {
            if((ABS_fl==0) && (ABS_fr==0)) {
                if((rel_lam_fl >=(int8_t)(-LAM_2P)) && (rel_lam_fr >=(int8_t)(-LAM_2P))) {
                    ABS_fl=ABS_fr=ABS_rl=ABS_rr=0;
                    state_fl=state_fr=state_rl=state_rr=0;
                }
            }
        }
      #else
        if(ABS_fz==1) {
            if((ABS_rl==0) && (ABS_rr==0)) {
                if((ABS_fl==1) || (ABS_fr==1)) {
                    if(eng_brk_timer >= L_1SCAN) {eng_brk_timer = eng_brk_timer - L_1SCAN;}
                }
            }
        }
        else {eng_brk_timer=L_TIME_500MS;}

        if(eng_brk_timer==0) {
            if((ABS_rl==0) && (ABS_rr==0)) {
                if((rel_lam_rl >=(int8_t)(-LAM_2P)) && (rel_lam_rr >=(int8_t)(-LAM_2P))) {
                    ABS_fl=ABS_fr=ABS_rl=ABS_rr=0;
                    state_fl=state_fr=state_rl=state_rr=0;
                }
            }
        }
      #endif
    }
/*  #endif */

    INSIDE_ABS_fz=0;
    if((ABS_fl==1)||(ABS_fr==1)||(ABS_rl==1)||(ABS_rr==1)) {INSIDE_ABS_fz=1;}

    if(INSIDE_ABS_fz==1){
        ABS_fz=1;
        inside_abs_fz_count=0;  /* inside_abs_fz_count=15;
                                   changed to match LFC duty and valve commands
                                   when ABS control of all wheels ended
                                   2007.03.08 PPR 2007-184 */
    }
    else {
/*        BRAKE_RELEASE=0; */

        if((lcabsu1BrakeByBLS==1)
      #if __VDC
        || (BRAKE_BY_MPRESS_FLG==1)     /* MPRESS_BRAKE_ON */
      #endif
        )
        {
            if((ABS_fz==1)&&(vref>(int16_t)VREF_5_KPH)) {   /* MAIN05C */
                if(inside_abs_fz_count==0) {ABS_fz=0;}
                else {inside_abs_fz_count = inside_abs_fz_count - L_1SCAN;}

                if(((lcabsu1BrakeByBLS==1) && (BRAKE_RELEASE==1))                     /*Added by KGY 030410 */
              #if __VDC
                || ((BRAKE_BY_MPRESS_FLG==1) && (BRAKE_RELEASE2==1))        /* MPRESS_BRAKE_ON */
              #endif
                )
                {
                    FL.Check_double_brake_flag=1;
                    FR.Check_double_brake_flag=1;
                    RL.Check_double_brake_flag=1;
                    RR.Check_double_brake_flag=1;
                }

            }
            else {
                ABS_fz=0;
                inside_abs_fz_count=0;
            }
        }
        else {
            ABS_fz=0;
            inside_abs_fz_count=0;
        }

        BRAKE_RELEASE=0;
      #if __VDC
        BRAKE_RELEASE2=0;
      #endif
    }
                   /*mi03a(011212) */
    if(ABS_fz==1) {
        if((SET_DUMP_fl==1)&&(SET_DUMP_fr==1)&&(SET_DUMP_rl==1)&&(SET_DUMP_rr==1)) {
            ALL_DUMP=1;
        }
    }
    else {
        SET_DUMP_fl=SET_DUMP_fr=SET_DUMP_rl=SET_DUMP_rr=0;
        ALL_DUMP=0;
    }

    if(ABS_fz==1) {
        if(vref<VREF_3_KPH) {
            if(under_limit_speed_cnt<U8_TYPE_MAXNUM) {under_limit_speed_cnt = under_limit_speed_cnt + L_1SCAN;}
        }
        else {under_limit_speed_cnt=0;}
    }
    else {under_limit_speed_cnt=0;}

/* #if __BTC_ABS_CLEAR */
    if(ABS_fz==1) {
        if((vref>=(int16_t)VREF_3_KPH)&&(vref<=(int16_t)VREF_5_KPH)) {
            if((vref_trough>0)&&(vref_trough<(int16_t)VREF_3_KPH)) {
                if(btc_end_delay_cnt<100) {btc_end_delay_cnt = btc_end_delay_cnt + L_1SCAN;}
            }
            else {btc_end_delay_cnt=0;}
        }
        else {btc_end_delay_cnt=0;}
    }
    else {btc_end_delay_cnt=0;}
/* #endif */
}

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCABSDecideCtrlVariables
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
