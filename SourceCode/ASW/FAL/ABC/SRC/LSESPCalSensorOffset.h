#ifndef __LSESPCALSENSOROFFSET_H__
#define __LSESPCALSENSOROFFSET_H__

#include "LVarHead.h"


#if ( __FL_PRESS==ENABLE ) || ( __FR_PRESS==ENABLE )||( __RL_PRESS==ENABLE )|| ( __RR_PRESS==ENABLE )
    #define __PREMIUM_RBC_PRESSURE_SEN_OFFSET  
#endif

extern void LSESP_vCalSensorOffset(void);
extern void LSESP_vWriteEEPROMSensorOffset(void);


#if defined   (__PREMIUM_RBC_PRESSURE_SEN_OFFSET)

struct PREM_PRESS_OFFSET_STRUCT
{
	int16_t  s16premAVR1;
	int16_t  s16premAVR2;
	int16_t  s16premPressTimer;
	int16_t  s16premPressSum;
	int16_t  s16premRaw;
	int16_t  s16premFiltPress;
	int16_t  s16premFiltPressOld;
	
	int16_t  s16premDrvOffset;
	int16_t  s16premEEPOfsRead;
	int16_t  s16premPressOfs_f;
	
	unsigned  u1premPower:                   1;
	unsigned  u1premAVR1ok:                  1;
	unsigned  u1premAVR2ok:                  1;
	unsigned  u1premPressOffsetCheck:	     1;
	unsigned  u1prem_NEED_TO_RECHECK_PRESS:  1;
	unsigned  u1prem_MORE_THAN_3BAR_CHECK:   1;
	unsigned  u1prem_BRAKE_PEDAL_OFF:        1;
	unsigned  u1prem_PRESS_TIMER_INC_MODE:   1;
		
	unsigned  u1prem_PRESS_TIMER_DEC_MODE:   1;
	unsigned  u1prem_PressOffsetOK:          1;
	unsigned  u1prem_PressErrDet:            1;
	unsigned  u1prem_PressSusDet:            1;
	unsigned  u1premNeedToWriteOffset:       1;
	unsigned  u1premValueInitReq:            1;
	unsigned  u1premEEPOfsReadEnd:           1;
	unsigned  u1premEEPOfsReadOK:            1;						
		       		
};

extern struct  PREM_PRESS_OFFSET_STRUCT  *PRESS, PRESS_M ;

#if  __FL_PRESS == ENABLE
extern struct  PREM_PRESS_OFFSET_STRUCT  PRESS_FL;
#endif

#if  __FR_PRESS == ENABLE
extern struct  PREM_PRESS_OFFSET_STRUCT  PRESS_FR;
#endif

#if  __RL_PRESS == ENABLE
extern struct  PREM_PRESS_OFFSET_STRUCT  PRESS_RL;
#endif

#if  __RR_PRESS == ENABLE
extern struct  PREM_PRESS_OFFSET_STRUCT  PRESS_RR; 
#endif

#if  __FL_PRESS == ENABLE
#define  lsesps16PremPressureFL            PRESS_FL.s16premFiltPress
#endif

#if  __FR_PRESS == ENABLE
#define  lsesps16PremPressureFR            PRESS_FR.s16premFiltPress
#endif

#if  __RL_PRESS == ENABLE
#define  lsesps16PremPressureRL            PRESS_RL.s16premFiltPress
#endif

#if  __RR_PRESS == ENABLE
#define  lsesps16PremPressureRR            PRESS_RR.s16premFiltPress
#endif


extern  int16_t lsesps16Temp1;

#endif


 
 
typedef struct
{
	uint16_t offset_flg_0	:1;
	uint16_t offset_flg_1	:1;
	uint16_t offset_flg_2	:1;
	uint16_t offset_flg_3	:1;
	uint16_t offset_flg_4	:1;
	uint16_t offset_flg_5	:1;
	uint16_t offset_flg_6	:1;
	uint16_t offset_flg_7	:1;
}SEN_OFFSET_FLG;

extern SEN_OFFSET_FLG lsespu8SenOffFlg1;
extern SEN_OFFSET_FLG lsespu8SenOffFlg2;
extern SEN_OFFSET_FLG lsespu8SenOffFlg3;
extern SEN_OFFSET_FLG lsespu8SenOffFlg4;
extern SEN_OFFSET_FLG lsespu8SenOffFlg5;
extern SEN_OFFSET_FLG lsespu8SenOffFlg6;
extern SEN_OFFSET_FLG lsespu8SenOffFlg7;
extern SEN_OFFSET_FLG lsespu8SenOffFlg8;
extern SEN_OFFSET_FLG lsespu8SenOffFlg9;

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
extern SEN_OFFSET_FLG lsespu8SenOffFlg10;
extern SEN_OFFSET_FLG lsespu8SenOffFlg11;
#endif

extern SEN_OFFSET_FLG lsespu8SenOffFlg12;

#define SAS_read_eeprom_end_flg			lsespu8SenOffFlg1.offset_flg_0
#define YAW_read_eeprom_end_flg        		lsespu8SenOffFlg1.offset_flg_1  
#define ALAT_read_eeprom_end_flg        		lsespu8SenOffFlg1.offset_flg_2 
#define FS_YAW_read_eeprom_end_flg      	lsespu8SenOffFlg1.offset_flg_3 
#define str_value_init_rq_flg					lsespu8SenOffFlg1.offset_flg_4
#define yaw_value_init_rq_flg         			lsespu8SenOffFlg1.offset_flg_5  
#define lg_value_init_rq_flg        				lsespu8SenOffFlg1.offset_flg_6 
#define fs_yaw_init_rq_flg      				lsespu8SenOffFlg1.offset_flg_7 


#define	turn_start_flg					lsespu8SenOffFlg2.offset_flg_0
#define	standstill_on_flg				lsespu8SenOffFlg2.offset_flg_1
#define	yaw_lg_tmp_read_done_flg		lsespu8SenOffFlg2.offset_flg_2
#define	tmp_1sec_delay_flg			lsespu8SenOffFlg2.offset_flg_3  
#define	yaw_map_minus_cnt_ok_flg		lsespu8SenOffFlg2.offset_flg_4 
#define	yaw_map_plus_cnt_ok_flg		lsespu8SenOffFlg2.offset_flg_5
#define	yaw_offset_srch_ok_flg			lsespu8SenOffFlg2.offset_flg_6
#define	yaw_reset_check_done_flg		lsespu8SenOffFlg2.offset_flg_7

#define	write_sensor_offset_eeprom_flg	lsespu8SenOffFlg3.offset_flg_0
#define	write_yaw_tmp_map_eeprom_flg	lsespu8SenOffFlg3.offset_flg_1
#define	init_yaw_tmp_map_eeprom_flg	lsespu8SenOffFlg3.offset_flg_2
#define	yaw_100ms_slope_ok_flg		lsespu8SenOffFlg3.offset_flg_3
#define	lat_g_100ms_slope_ok_flg		lsespu8SenOffFlg3.offset_flg_4
#define	steer_initial_check_flg			lsespu8SenOffFlg3.offset_flg_5
#define	steer_final_check_flg			lsespu8SenOffFlg3.offset_flg_6
#define	steer_stable_check_flg			lsespu8SenOffFlg3.offset_flg_7

#define	yaw_over_flg			      lsespu8SenOffFlg4.offset_flg_0
#define	FLAG_INHIBIT_OFST_SPIN		lsespu8SenOffFlg4.offset_flg_1
#define	FLAG_INITIAL_LAT_OFFSET		lsespu8SenOffFlg4.offset_flg_2
#define	yaw_lg_sn_high_read_ok_flg		lsespu8SenOffFlg4.offset_flg_3
#define	yaw_lg_sn_low_read_ok_flg		lsespu8SenOffFlg4.offset_flg_4
#define	yaw_lg_sn_read_ok_flg			lsespu8SenOffFlg4.offset_flg_5
#define	yaw_lg_sn_high_write_ok_flg	lsespu8SenOffFlg4.offset_flg_6
#define	yaw_lg_sn_low_write_ok_flg		lsespu8SenOffFlg4.offset_flg_7

#define	yaw_err_instant_flg				lsespu8SenOffFlg5.offset_flg_0
#define	yaw_err_zerodrift_flg				lsespu8SenOffFlg5.offset_flg_1
#define	latg_err_instant_flg				lsespu8SenOffFlg5.offset_flg_2
#define	latg_err_zerodrift_flg				lsespu8SenOffFlg5.offset_flg_3
#define	str_err_instant_flg					lsespu8SenOffFlg5.offset_flg_4
#define	str_err_zerodrift_flg				lsespu8SenOffFlg5.offset_flg_5
#define	fs_vcc_off_flg						lsespu8SenOffFlg5.offset_flg_6
#define	raster_tmp_spec_over_flg		    lsespu8SenOffFlg5.offset_flg_7

#define	control_hold_flg    	            lsespu8SenOffFlg6.offset_flg_0
#define	spin_control_delay_flag    		    lsespu8SenOffFlg6.offset_flg_1
#define	spin_control_delay_flag1			lsespu8SenOffFlg6.offset_flg_2
#define	spin_control_delay      			lsespu8SenOffFlg6.offset_flg_3
#define	spin_delay_calc_set_flag 			lsespu8SenOffFlg6.offset_flg_4
#define	yaw_offset_tmp_over_flg	            lsespu8SenOffFlg6.offset_flg_5
#define	yaw_offset_slope_over_flg			lsespu8SenOffFlg6.offset_flg_6
#define	yaw_turn_table_sus_flg              lsespu8SenOffFlg6.offset_flg_7

#define	stop_state_flag        	            lsespu8SenOffFlg7.offset_flg_0
#define	yawlg_stop_1sec_flag  		        lsespu8SenOffFlg7.offset_flg_1
#define	yaw_offset_tmp_over_hold_flg        lsespu8SenOffFlg7.offset_flg_2
#define	lsespu1MpressOffsetOK    		    lsespu8SenOffFlg7.offset_flg_3
#define	lsespu1MpressOffsetCheck	        lsespu8SenOffFlg7.offset_flg_4
#define	lsespu1ESPnotReadyCond	            lsespu8SenOffFlg7.offset_flg_5
#define	lsespu1Yaw250meanCheck	            lsespu8SenOffFlg7.offset_flg_6
#define	lsespu1YawSignalStableFlag          lsespu8SenOffFlg7.offset_flg_7



#define	yaw_offset_spec_over_flg      	    lsespu8SenOffFlg8.offset_flg_0
#define	lsespu1YawGstandstillflg	        lsespu8SenOffFlg8.offset_flg_1
#define	lsespu1YawStandOffsCalIGN1st        lsespu8SenOffFlg8.offset_flg_2
#define	lsespu1YawStandOffsCalState    	    lsespu8SenOffFlg8.offset_flg_3
#define	lsespu1YawStableDctDelayFlag        lsespu8SenOffFlg8.offset_flg_4
#define	lsespu1EEPYawStandWrtReq            lsespu8SenOffFlg8.offset_flg_5
#define	lsespu1EEPYawStandOfsReadEnd        lsespu8SenOffFlg8.offset_flg_6
#define	lsespu1EEPYawStandOfsReadOK         lsespu8SenOffFlg8.offset_flg_7

#define	lsespu1EEPYawStandOfsInitReq  	    lsespu8SenOffFlg9.offset_flg_0
#define	lsespu1SASresetDiagFlg              lsespu8SenOffFlg9.offset_flg_1
#define	lsespu1EEPSASresetFlg               lsespu8SenOffFlg9.offset_flg_2
#define	lsespu1MpressDrvOffsetOK        lsespu8SenOffFlg9.offset_flg_3
#define	lsespu1SASsigStbFlg             lsespu8SenOffFlg9.offset_flg_4
#define	lsespu1YawSpecOverSus           lsespu8SenOffFlg9.offset_flg_5
#define	lsespu1YawTurnTableStop         lsespu8SenOffFlg9.offset_flg_6
#define	lsespu1unused_ofs_7                 lsespu8SenOffFlg9.offset_flg_7

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
#define	lsespu1BrkAppSenFiltInvalid  	lsespu8SenOffFlg10.offset_flg_0
#define	lsespu1BrkAppSenPDTFiltInvalid  lsespu8SenOffFlg10.offset_flg_1
#define	lsespu1ESPeepEnterOK            lsespu8SenOffFlg10.offset_flg_2
#define	lsespu1ESPeepCalcLoopOK    	    lsespu8SenOffFlg10.offset_flg_3
#define	lsespu1unused_10ofs_4           lsespu8SenOffFlg10.offset_flg_4
#define	TestPedalTravelSnrCalReq        lsespu8SenOffFlg10.offset_flg_5
#define	lsespu1ESPeepWhileOK            lsespu8SenOffFlg10.offset_flg_6
#define	lsespu1unused_10ofs_7           lsespu8SenOffFlg10.offset_flg_7

#define	lsespu1PedalEEPofsCalcOK        lsespu8SenOffFlg11.offset_flg_0
#define	lsespu1PressEEPofsCalcOK        lsespu8SenOffFlg11.offset_flg_1
#define	CanTestFlag                     lsespu8SenOffFlg11.offset_flg_2
#define	lsespu1AHBsenEOLEEPofsCalcEND   lsespu8SenOffFlg11.offset_flg_3
#define	lsespu1unused_11ofs_4           lsespu8SenOffFlg11.offset_flg_4
#define	lsespu1MpresEolFirstOfsRead     lsespu8SenOffFlg11.offset_flg_5
#define	lsespu1unused_11ofs_6           lsespu8SenOffFlg11.offset_flg_6
#define	lsespu1BrkAppSenInvalid         lsespu8SenOffFlg11.offset_flg_7

#endif

#define	lsespu1MpresDrvOfsNeedWriteEep  	lsespu8SenOffFlg12.offset_flg_0
#define	lsespu1MpresDrvEEPOfsRead           lsespu8SenOffFlg12.offset_flg_1
#define	lsespu1MpresBrkOnByAHBgen3          lsespu8SenOffFlg12.offset_flg_2
#define	lsespu1MpresByAHBgen3Invalid        lsespu8SenOffFlg12.offset_flg_3
#define	lsespu1unused_ofs_12_4              lsespu8SenOffFlg12.offset_flg_4
#define	lsespu1unused_ofs_12_5              lsespu8SenOffFlg12.offset_flg_5
#define	lsespu1unused_ofs_12_6              lsespu8SenOffFlg12.offset_flg_6
#define	lsespu1unused_ofs_12_7              lsespu8SenOffFlg12.offset_flg_7

extern uint8_t lsespu8Yawrate1SecCnt;
extern int16_t lsesps16YawrateMean1Sec;
extern int32_t lsesps32YawrateSum1Sec;
extern int16_t lsesps16YawOffsetPile[10];
extern uint8_t lsespu8YawOffsetPileIndex;
extern int16_t lsesps16YawTmpMean1Sec;
extern int32_t lsesps32YawTmpSum1Sec;
extern int16_t lsesps16YawTmpPile[10];
extern uint8_t lsespu8YawTmpReadDoneFlg[31];
extern int16_t lsesps16YawTmpEEPMap[31];
extern int16_t lsesps16YawTmpMap[31];
extern uint8_t lsespu8YawTmpReadIndex;
extern uint16_t lsespu16YawTmpAddress[31];
extern int16_t lsesps16YawTmpLimit[31];
extern uint8_t lsespu8YawTmpEEPOKFlg[31];
extern uint8_t lsespu8YawTmpOKFlg[31];
extern uint8_t lsespu8YawTmpInitRqFlg[31];
extern uint8_t lsespu8YawTmpWriteRqFlg[31];
extern uint8_t lsespu8YawTmpWriteIndex;
extern int16_t lsesps16Tmp1Sec;
extern uint8_t lsespu8Tmp1SecCnt;
extern int32_t lsesps32Tmp1secSum;
extern uint8_t lsespu8Mean1secCnt;
extern int16_t lsesps16Yawrate1secMean;
extern int16_t lsesps16LatAccel1secMean;
extern int16_t lsesps16YawTmp1SecMean;
extern int32_t lsesps32Yawrate1secSum;
extern int32_t lsesps32LatAccelsecSum;
extern int32_t lsesps32YawTmp1SecSum;

extern int16_t lsesps16YawOffsetByTmp;

extern int8_t lsesps8SrchYawTmpMapIndex;
extern int8_t lsesps8SrchTestCnt;
extern int8_t lsesps8SrchYawTmpMapMinusCnt;
extern int8_t lsesps8SrchYawTmpMapPlusCnt;
extern int16_t lsesps16Yawrate100msSlope;
extern int16_t lsesps16LatAccel100msSlope;
extern int16_t lsesps16Yawrate100msMean;
extern int16_t lsesps16LatAccel100msMean;
extern int32_t lsesps32Yawrate100msSum;
extern uint8_t lsespu8Mean100msCnt;

extern uint16_t steer_rate_deg_s2;
extern int16_t lsesps16MaxYawThrAdd;
extern int16_t lsesps16MaxLatGThrAdd;
extern int16_t lsesps16MaxSteerThrAdd;
extern uint16_t lsespu16YawLgSnHigh;
extern uint16_t lsespu16YawLgSnLow;
extern uint16_t lsespu16YawLgSnHighOld;
extern uint16_t lsespu16YawLgSnLowOld;

extern int16_t lsesps16MoitorOffsetVar01;
extern int16_t lsesps16MoitorOffsetVar02;
extern int16_t lsesps16MoitorOffsetVar03;

extern uint16_t lsespu16MoitorOffsetVar01;
extern uint16_t lsespu16MoitorOffsetVar02;
extern uint16_t lsespu16MoitorOffsetVar03;

/*----- Offset Spin -----------*/
extern uint8_t lsespu8CtrlHold1secCnt;

extern uint16_t lsesp16OfsSpinCNT[4];
extern uint8_t OFST_SPIN[4];

extern uint8_t OFST_DRAG[2];
extern uint16_t  OFST_DRAG_CNT[2];

extern uint8_t OFST_CONTR;
extern uint8_t OFST_CONTR1;
extern uint8_t OFST_CONTR_SET_CNT;
extern uint16_t  SPIN_DELAY_T;
extern uint16_t  SPIN_DELAY_TimeCalc;
extern uint8_t lsespu8spinCalc1secCNT;
extern uint8_t lsespu8spinCalcCNT;
extern uint16_t  lsespu16MuEst1secAverage;
extern uint16_t  lsespu16MuEstAverage;
extern int32_t  lsesps32MuEstSum;

extern uint8_t INHIBIT_OFST_SPIN_1;
extern uint8_t INHIBIT_OFST_SPIN_2;
extern uint8_t INHIBIT_OFST_SPIN_3;
extern uint8_t INHIBIT_OFST_SPIN_4;

extern uint16_t  lsespu16ABSfz1MinAfterCount;
extern uint8_t lsespu8DeltaYawrateCnt;
extern int32_t   lsesps32DeltaYawrateSum;
extern int16_t   lsesps16DeltaYawrate250mMean;
extern int16_t   lsesps16DeltaYawrate250mMeanMax;
extern int16_t   lsesps16DeltaYawrate250mMeanMin;
extern int16_t   lsesps16DeltaYawrate250mDiff;
extern uint8_t   lsespu8DeltaYawrateStableCnt;
extern int16_t   lsesps16YawSpcOvrThres;

/*----- Yaw Standstill offset calc ---*/
extern uint8_t lsespu8YawStandOffsetCalcCNT;
extern int32_t   lsesps32YawStandOffset1secSum;
extern int16_t   lsesps16YawStandOffset1secMean;
extern int16_t   lsesps16YawStandOffset;
extern int16_t   lsesps16YawStandOffsetOld;
extern int16_t   lsesps16YawStandOffsetOld1;
extern int16_t   lsesps16YawStandOffsetOld2;
extern uint8_t lsespu8YawStandCNT;

/*----  Yaw Normal Offset EEP -----*/
extern int16_t   lsesps16EEPYawStandOfsRead;

extern int16_t lsesps16WheelVelocityFL;
extern int16_t lsesps16WheelVelocityFR;
extern int16_t lsesps16WheelVelocityRL;
extern int16_t lsesps16WheelVelocityRR;

#if(__YAW_SS_MODEL_ENABLE==1)
extern int16_t lsesps16YawSS_temp;
#endif

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
extern int16_t   lsesps16BrkAppSenRaw;
extern int16_t   lsesps16BrkAppSenRaw2;

extern int16_t   lsesps16BrkAppSenPDFFilt;
extern int16_t   lsesps16BrkAppSenPDTFilt;
extern int16_t   lsesps16BrkAppSenFiltF;

extern int16_t   lsesps16BASOfsPDFFinal;
extern int16_t   lsesps16BASOfsPDTFinal;

extern int16_t   lsesps16MpresEolFirstOfs;

#else

extern int16_t   lsesps16BrkAppSenFiltF;
extern int16_t   lsesps16BASOfsFinalGM;
extern int16_t   lsesps16BrkAppSenGMFilt;
extern int16_t   lsesps16BrkAppSenRawGM;

#endif

/* Mpress offset  */
extern int16_t   lsesps16MpresDrvOfs;
extern int16_t   lsesps16MpresOfsFinal;
extern int16_t   lsesps16MpresDrvOfsEEPw;
extern int16_t   lsesps16MpresDrvEEPOfs;

#if ((__PEDAL_SENSOR_TYPE==ANALOG_TYPE)||(__PEDAL_SENSOR_TYPE==CAN_TYPE))
/* 2012_SWD_KJS */

struct ESP_EEP_offsetCalc{
       
       uint16_t    lsespu8ESPEEPofsReadInvalid;  //로직 추가
       int16_t     lsesps16ESPEEPofsRead;        //로직 추가
       int16_t     lsesps16ESPofsFirstCal;
       int16_t     lsesps16ESPofsFirstCalMax;
       int16_t     lsesps16ESPofsFirstCalMin;        
       uint16_t    lsespu8ESPofsFirstCnt;    
       int32_t     lsesps32ESPOfsFirstSum;             
	   uint16_t    lsespu8ESPEEPofsMaxMinLMOverCnt;
	   uint16_t    lsespu8EEPOfsDelyCnt;
       

       int16_t     lsesps16ESPOfs_f;         //로직 추가
       int16_t     lsesps16ESPeepWriteOfs;   //로직 추가
       	
       uint16_t    lsespu1ESPEepOfsCalOkFlg         :1;
       uint16_t    lsespu1ESPEepOfsCalEndFlg        :1;
       uint16_t    lsespu1ESPEepOfsWriteWhile       :1;
       uint16_t    lsespu1ESPEEPofsUseOK            :1;
       uint16_t    lsespu1EEPofsCalcFWStartFlg      :1;
       uint16_t    lsespu1EEPofsCalcCLStartFlg      :1;	
       uint16_t    lsespu1EEPOfsEepReCalcOK         :1;
       uint16_t    lsespu1EEPofsEEPLimitOK          :1;       	    
       	
       uint16_t    lsespu1EEPofsEEPMaxMinOK         :1;
       uint16_t    lsespu1ESPEepOfsWriteReq         :1;    	
       uint16_t    lsespu1ESPEepOfsWriteReqOK       :1;     
       
       uint16_t    lsespu1fsFailInvalid             :1;         
   		

	}; 
extern struct  ESP_EEP_offsetCalc	*ESP_EEP_CALC, *ESP_CALC, ESP_PDF, ESP_PDT,ESP_MP;

struct PEDAL_offset{       
       
       //DRV
       uint8_t    lsespu8PedalDrvOfsCnt;       
       int16_t    lsesps16PedalSenRaw ;
       int16_t    lsesps16PedalDrvOfs_f;
       int16_t    lsesps16PedalDrvOfsMean;
       int32_t    lsesps32PedalDrvOfsSum;
       
       int16_t    lsespu8PedalDrvOfsArrayCnt;
       int16_t    lsesps16PedalDrvOfsArray[10];
       int16_t    lsesps16PedalDrvOfsArrayMax;
       int16_t    lsesps16PedalDrvOfsArrayMin;
       int32_t    lsesps32PedalDrvOfsArraySum;
       int16_t    lsesps16PedalDrv10secMean;
       int16_t    lsesps16PedalDrvOfs60sMeanOld;
       uint8_t    lsespu8PedalDrvOfs10sArrayCnt;
       int32_t    lsesps32PedalDrvOfs10sArraySum;
       int16_t    lsesps16PedalDrvOfsArrayFilter;
       int16_t    lsesps16PedalDrvOfs60sMean;
       int16_t    lsesps16PedalDrvOfsArrayFConv;
       int16_t    lsesps16PedalDrvOfsArrayMean;
       
       uint8_t    lsespu8PdlNormCondCnt;
       int16_t    lsesps16PdlNormCondMean;
       int16_t    lsesps16PdlNormCondMeanOld;
       int32_t    lsesps32PdlNormCondSum;
       uint8_t    lsespu8PdlDrvModeDeltTCnt;
       int16_t    lsesps16PdlNormCondMeanDiff;
       int16_t    lsesps16PdlDrvMin;
       int16_t    lsesps16PedalOfsFinal;

       /* EOL Read */
       int16_t    lsesps16OfsEolFirstEep;
       /* Drv EEP Read */
       int16_t    lsesps16DrvOfsEepRead;
       /* Drv EEP Write */
       int16_t    lsesps16PedalDrvOfsEEPw;
       
       /* Drv Flags */
       uint16_t   lsespu1PedalDrvOfsCheckCond     :1;	       
       uint16_t   lsespu1PedalNormCond            :1;
       uint16_t   lsespu1PedalDrvOfsFirst         :1;  	       
       uint16_t   lsespu1NormCondFirstCheck	      :1;
       uint16_t   lsespu1PedalDrv1secOfsOK        :1;
       uint16_t   lsespu1PedalDrv10secOfsOK       :1;
       uint16_t   lsespu1PedalDrv60secOfsOK       :1;
       uint16_t   lsespu1PedalDrv60secOfsReOK     :1;
       uint16_t   lsespu1PedalEolFirstOfsRead     :1;      
       
       /* Drv EEP ofs */
       uint16_t   lsespu1OfsEepReadOK             :1;
       uint16_t   lsespu1NeedToWriteOfs           :1;     
       uint16_t   lsespu1PedalOfsInvalid          :1;            	   	   	
       	           	       		       	        	     	       	   		
	}; 
	
extern struct  PEDAL_offset   *PEDAL_CALC,PDF, PDT ;

#endif  


#endif
