
/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
    #define idx_START
    #define idx_FILE    idx_CL_LDBTCSCallDetection
    #include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCTCSCallControl.h"
#include "LDTCSCallDetection.h"
#include "LCRDCMCallControl.h"
#include "LCHSACallControl.h"
#include "hm_logic_var.h"
#include "LCTCSCallBrakeControl.h"
#include "LDABSCallDctForVref.h"
#include "tcs_var.h"
#include "t_struct.h"
#include "v_struct.h"
#include "LDABSCallDctForVref.h"

#include "ETCSGenCode/LCETCS_vCallMain.h" /* MBD ETCS */


#if __BTC  /* TAG1*/


#if defined(BTCS_TCMF_CONTROL) /* TAG2 */

   /*BTCS Detection  */
    void LDTCS_vCallTCMFDetection(void);

    void LDTCS_vDetGearShift(void);
    
    void LDTCS_vDetBrkEnInhibitByDriver(void);
	void LDTCS_vDetBrkEnInhibitByVehSpd(void);
	void LDTCS_vDetBrkEnInhibitByRPMTorq(void);
	void LDTCS_vDetCoordBrkInhibit(void);
    
	void LDTCS_vDetBrkEnInhibitBySWESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS);    
	void LDTCS_vDetBrkEnInhibitBySCESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS);    
	void LDTCS_vDetBrkEnInhibitByESC(struct TCS_WL_STRUCT *Wl_TCS);
    
    void LDTCS_vDctSymSpin(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
	void LDTCS_vDctOkCondi4FFAdapt(struct TCS_AXLE_STRUCT *Axle_TCS);
	void LDTCS_vDctOKCondi4BrkTrqMaxLim (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctHuntingInAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);    
	void LDTCS_vDctLowToSplit (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);            
    void LDTCS_vDctWhlOnHghMu (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    void LDTCS_vDctInstbOfHghMuWhl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
    void LDTCS_vDctInstbOfHghMuWhl4EngCl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    void LDTCS_vDctTransOfHghMuWhlStat(struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctLowSpin4HghMuReCovery(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
    void LDTCS_vDctFlgOfAfterHghMuUnstb(struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctFastDecPresInHghMuUstb(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
    
    void LDTCS_vDctHghMuUstbRepeatCycle(struct TCS_AXLE_STRUCT *Axle_TCS);
		void LDTCS_vDctHghMuUstbCntStartFlg(struct TCS_AXLE_STRUCT *Axle_TCS);
		void LDTCS_vDctHSSCnt4AdptBaseFF(struct TCS_AXLE_STRUCT *Axle_TCS);  
    
    void LDTCS_vDctAdpFFCtlByHghMuWhlUnst(struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctBrkTrqMaxLim4AsymSpCtl(struct TCS_AXLE_STRUCT *Axle_TCS);
	void LDTCS_vCntBrkTrqMaxLimTime (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctBrkTrqMaxLimInc (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctBrkTrqMaxLimReset (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctOverBrk (struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
    void LDTCS_vDctOverBrkInAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    
    #if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))  /* Temp VCA */
	static void LDTCS_vDctBigSpinAWD(void);
	void LDTCS_vDecideVrefCrtAidStart(void);  
	void LDTCS_vDecideVrefCrtAidCtrlWhl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *W_L,struct W_STRUCT *W_R);
	void LDTCS_vDctBTCPrevCtrlWhl4VCA(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *W_L,struct W_STRUCT *W_R);
		#if (__SPLIT_TYPE==0)
	void LDTCS_vDecideVCAMaxCtrlCirXsp(void);
		#else
	void LDTCS_DecideVCAMaxCtrlCirFRsp(void);
		#endif
	void LDTCS_vSustainFlagAfterVCA(void);
	void LDTCS_vDecideVCAOffByYawRate(struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT); 
	void LDTCS_vDecideVCAOffBySpdDiff4Whl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT); 
	void LDTCS_vDecideVCAOffBySpdDiff(void);
	void LDTCS_vDecideVrefCrtAidEnd(void); 
	#endif                                        /* Temp VCA */
    
    /*Vibration control*/
    void LDTCS_vVIBDetectFlag(struct TCS_WL_STRUCT *Wl_TCS); 
    void LDTCS_vVIBDecideBrakeModeVib(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);  
    void LDTCS_vVIBDetectFlag4Engine(void);
    /*Vibration control*/
    
    void LDTCS_vDctIniMotorCtlMode (void);
    void LDTCS_vDctLowPres4IniTCVCtlMode(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct W_STRUCT *WL_F, struct W_STRUCT *WL_S);
    void LDTCS_vDctIniTCVCtlMode(struct TCS_CIRCUIT_STRUCT *CIRCUIT);
    void LDTCS_vDctIIntCtlAct (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctIintReset4FFAdapt (struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctAdpFFCtlByIIntReset(struct TCS_AXLE_STRUCT *Axle_TCS);
    void LDTCS_vDctTCSSplit(void);
    void LDTCS_vDctAxleCondition4ETCS(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
    void LDTCS_vDctSpinCondition4ETCS(void);

    void LDTCS_vDctBrkCtlFadeOutMode(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    void LDTCS_vDctBrkCtlFadeOutCtrlCnt(struct TCS_AXLE_STRUCT *Axle_TCS);
	void LDTCS_vDctCtlWLSpdCros4FadOutCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);
	

    /*void LDTCS_vEstCirPresByTcCurnt(struct TCS_CIRCUIT_STRUCT *CIRCUIT); IDB System*/
    void LDTCS_vEstWhlBrkTrqAndPresXsp(void);
    void LDTCS_vEstWhlBrkTrqAndPresFRsp(void);

    void LDTCS_vDctSplitMuAxle(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    void LDTCS_vDctSplitMuFlag(void);
    void LDTCS_vDctSplitHill(void);
    void LDTCS_vDctHillByAxSen(void);
    void LDTCS_vDctSplitHillByAxSen(void);
    void LDTCS_vDctSplitHillByVehAccel(void);

    void LCTCS_vTransAsymSpnCtlMod(struct TCS_AXLE_STRUCT *Axle_TCS);
    void LCTCS_vTransCirAsymSpnXspCtlMod(void);
    void LCTCS_vTransCirAsymSpnFRspCtlMod(void);
    void LDTCS_vDctEndOfBrkCtlOfVeh(void);
	void LDTCS_vDctEndOfBrkCtlOfWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);    
	void LDTCS_vDctEndOfBrkCtlOfDrvMd(void);
	void LDTCS_vDctEndOfBrkCtlBySpdDiff(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT,struct W_STRUCT *WL_L,struct W_STRUCT *WL_R);
	void LDTCS_vDctEndofBrkCtlByHunting(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R);
	void LDTCS_vDctEndofBrkCtlOfVCA(void); 
	void LDTCS_vDctEndofBrkCtlofESC(struct TCS_WL_STRUCT *Wl_ESC, struct W_STRUCT *W_L);	  
	void LDTCS_vDctEndofBrkCtlBySlip(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);	  

    static void LDTCS_vStartOfBrkCtlByAsymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT);
    static void LDTCS_vStartOfBrkCtlBySymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT); 	
    static void LDTCS_vBrkCtlTrnsAsym2SymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT); 
	static void LDTCS_vBrkCtlTrnsSym2AsymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT); 
    #if __MOTOR_OFF_FOR_NOISE
    static void LDTCS_vDctFlgOfAfterStall(void);
    static void LDTCS_vDctMtrOFFCondition4Noise(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
    #endif	
    /*static void LDTCS_vStartOfBrkCtlByStuck(void);*/
    static void LDTCS_vStartOfBrkCtlByVCA(void);
    static void LDTCS_vStartOfCordBrkCtl(void);    
	
	static void LDTCS_vCpyBTCSActWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
	static void LDTCS_v2ndCpyBTCSActWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
	static void LDTCS_vCntBTCSActWhlOnTime(struct TCS_WL_STRUCT *Wl_TCS);
	static void LDTCS_vDctBTCSActAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);
	static void LDTCS_v2ndDctBTCSActAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);
	static void LDTCS_vDctBTCSActVehicle (void); 
	static void LDTCS_v2ndDctBTCSActVehicle (void); 
	static void LDTCS_vDctBTCSActCir(void);
	static void LDTCS_v2ndDctBTCSActCir(void);

void LDTCS_vCallTCMFDetection(void)
{
    LDTCS_vDetGearShift();

	LDTCS_vDetBrkEnInhibitByDriver();
	LDTCS_vDetBrkEnInhibitByVehSpd();
	LDTCS_vDetBrkEnInhibitByRPMTorq();
	LDTCS_vDetCoordBrkInhibit();
	
	/*Same Wheel Brake Control in ESC*/
    LDTCS_vDetBrkEnInhibitBySWESC(&FL_TCS,&FL_TCS);
    LDTCS_vDetBrkEnInhibitBySWESC(&FR_TCS,&FR_TCS);
    LDTCS_vDetBrkEnInhibitBySWESC(&RL_TCS,&RL_TCS);
    LDTCS_vDetBrkEnInhibitBySWESC(&RR_TCS,&RR_TCS);    
    /*Same Circuit Brake Control in ESC*/
    #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/ 
    LDTCS_vDetBrkEnInhibitBySCESC(&FL_TCS,&RR_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&FR_TCS,&RL_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&RL_TCS,&FR_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&RR_TCS,&FL_TCS);
    #else					  /*FR-Split 일 경우 */	
    LDTCS_vDetBrkEnInhibitBySCESC(&FL_TCS,&FR_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&FR_TCS,&FL_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&RL_TCS,&RR_TCS);
    LDTCS_vDetBrkEnInhibitBySCESC(&RR_TCS,&RL_TCS);
    #endif
	LDTCS_vDetBrkEnInhibitByESC(&FL_TCS);
	LDTCS_vDetBrkEnInhibitByESC(&FR_TCS);
	LDTCS_vDetBrkEnInhibitByESC(&RL_TCS);
	LDTCS_vDetBrkEnInhibitByESC(&RR_TCS);    
		
    LDTCS_vStartOfBrkCtlByAsymSlip(&FA_TCS,&FL,&FR,&FL_TCS,&FR_TCS);
	LDTCS_vStartOfBrkCtlByAsymSlip(&RA_TCS,&RL,&RR,&RL_TCS,&RR_TCS);
	/* MBD ETCS */
	/*LDTCS_vStartOfBrkCtlBySymSlip(&FA_TCS,&FL,&FR,&FL_TCS,&FR_TCS);
	LDTCS_vStartOfBrkCtlBySymSlip(&RA_TCS,&RL,&RR,&RL_TCS,&RR_TCS);*/
	if (ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqFA==1)
	{
	FL.BTCS = 1;
	FR.BTCS = 1;
	}
	else if (ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqRA==1)
	{
	RL.BTCS = 1;
	RR.BTCS = 1;
	}
	else{;}
	
	LDTCS_vBrkCtlTrnsAsym2SymSlip(&FA_TCS,&FL,&FR,&FL_TCS,&FR_TCS);
	LDTCS_vBrkCtlTrnsAsym2SymSlip(&RA_TCS,&RL,&RR,&RL_TCS,&RR_TCS);
	
	LDTCS_vBrkCtlTrnsSym2AsymSlip(&FA_TCS,&FL,&FR);
	LDTCS_vBrkCtlTrnsSym2AsymSlip(&RA_TCS,&RL,&RR);
	
	
	/*LDTCS_vStartOfBrkCtlByStuck();*/
	LDTCS_vStartOfBrkCtlByVCA();
	LDTCS_vStartOfCordBrkCtl();

    LDTCS_vCpyBTCSActWhl(&FL_TCS,&FL);
    LDTCS_vCpyBTCSActWhl(&FR_TCS,&FR);
    LDTCS_vCpyBTCSActWhl(&RL_TCS,&RL);
    LDTCS_vCpyBTCSActWhl(&RR_TCS,&RR);
    
    LDTCS_vCntBTCSActWhlOnTime(&FL_TCS);
    LDTCS_vCntBTCSActWhlOnTime(&FR_TCS);
    LDTCS_vCntBTCSActWhlOnTime(&RL_TCS);
    LDTCS_vCntBTCSActWhlOnTime(&RR_TCS);
    
    LDTCS_vDctBTCSActVehicle();
    LDTCS_vDctBTCSActAxle(&FA_TCS,&FL,&FR);
    LDTCS_vDctBTCSActAxle(&RA_TCS,&RL,&RR);
   	LDTCS_vDctBTCSActCir();
		
    LDTCS_vDctSymSpin(&FA_TCS,&FL_TCS,&FR_TCS,&FL,&FR);
    LDTCS_vDctSymSpin(&RA_TCS,&RL_TCS,&RR_TCS,&RL,&RR);

	LDTCS_vDctOkCondi4FFAdapt(&FA_TCS);
	LDTCS_vDctOkCondi4FFAdapt(&RA_TCS);
	
	LDTCS_vDctOKCondi4BrkTrqMaxLim(&FA_TCS);
	LDTCS_vDctOKCondi4BrkTrqMaxLim(&RA_TCS);	

	LDTCS_vDctHuntingInAxle (&FA_TCS, &FL_TCS, &FR_TCS);
	LDTCS_vDctHuntingInAxle (&RA_TCS, &RL_TCS, &RR_TCS);    

	LDTCS_vDctLowToSplit(&FA_TCS, &FL_TCS, &FR_TCS);
	LDTCS_vDctLowToSplit(&RA_TCS, &RL_TCS, &RR_TCS);    

    LDTCS_vDctWhlOnHghMu(&FA_TCS,&FL_TCS,&FR_TCS);
    LDTCS_vDctWhlOnHghMu(&RA_TCS,&RL_TCS,&RR_TCS);
    LDTCS_vDctInstbOfHghMuWhl(&FA_TCS,&FL_TCS,&FR_TCS, &FL, &FR);
    LDTCS_vDctInstbOfHghMuWhl(&RA_TCS,&RL_TCS,&RR_TCS, &RL, &RR);
    LDTCS_vDctInstbOfHghMuWhl4EngCl(&FA_TCS,&FL_TCS,&FR_TCS);
    LDTCS_vDctInstbOfHghMuWhl4EngCl(&RA_TCS,&RL_TCS,&RR_TCS);
    LDTCS_vDctTransOfHghMuWhlStat(&FA_TCS);
    LDTCS_vDctTransOfHghMuWhlStat(&RA_TCS);
    LDTCS_vDctLowSpin4HghMuReCovery(&FA_TCS, &FL_TCS,&FR_TCS,&FL, &FR);
    LDTCS_vDctLowSpin4HghMuReCovery(&RA_TCS, &RL_TCS,&RR_TCS,&RL, &RR);
    LDTCS_vDctFlgOfAfterHghMuUnstb(&FA_TCS);
    LDTCS_vDctFlgOfAfterHghMuUnstb(&RA_TCS);
    LDTCS_vDctFastDecPresInHghMuUstb(&FA_TCS,&FL,&FR);
    LDTCS_vDctFastDecPresInHghMuUstb(&RA_TCS,&RL,&RR);

		LDTCS_vDctHghMuUstbRepeatCycle(&FA_TCS);
		LDTCS_vDctHghMuUstbRepeatCycle(&RA_TCS);		
		LDTCS_vDctHghMuUstbCntStartFlg(&FA_TCS);
		LDTCS_vDctHghMuUstbCntStartFlg(&RA_TCS);
		LDTCS_vDctHSSCnt4AdptBaseFF(&FA_TCS);  
		LDTCS_vDctHSSCnt4AdptBaseFF(&RA_TCS);  

    LDTCS_vDctAdpFFCtlByHghMuWhlUnst(&FA_TCS);
    LDTCS_vDctAdpFFCtlByHghMuWhlUnst(&RA_TCS);    

	LDTCS_vDctBrkTrqMaxLim4AsymSpCtl(&FA_TCS);
	LDTCS_vDctBrkTrqMaxLim4AsymSpCtl(&RA_TCS);
	LDTCS_vCntBrkTrqMaxLimTime(&FA_TCS);
	LDTCS_vCntBrkTrqMaxLimTime(&RA_TCS);
	LDTCS_vDctBrkTrqMaxLimInc(&FA_TCS);
	LDTCS_vDctBrkTrqMaxLimInc(&RA_TCS);
	LDTCS_vDctBrkTrqMaxLimReset(&FA_TCS);
	LDTCS_vDctBrkTrqMaxLimReset(&RA_TCS);	
	

    LDTCS_vDctOverBrk (&FL_TCS,&FL);
    LDTCS_vDctOverBrk (&FR_TCS,&FR);
    LDTCS_vDctOverBrk (&RL_TCS,&RL);
    LDTCS_vDctOverBrk (&RR_TCS,&RR);

    LDTCS_vDctOverBrkInAxle (&FA_TCS, &FL_TCS, &FR_TCS);
    LDTCS_vDctOverBrkInAxle (&RA_TCS, &RL_TCS, &RR_TCS);
    
    #if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1)) /* Temp VCA */
		LDTCS_vDctBigSpinAWD();
		LDTCS_vDecideVrefCrtAidStart();      
	  	LDTCS_vDecideVrefCrtAidCtrlWhl(&FA_TCS, &FL_TCS, &FR_TCS, &FL, &FR);
		LDTCS_vDecideVrefCrtAidCtrlWhl(&RA_TCS, &RL_TCS, &RR_TCS, &RL, &RR);
		LDTCS_vDecideVCAOffByYawRate(&FL_TCS, &FR_TCS); 
		LDTCS_vDecideVCAOffByYawRate(&RL_TCS, &RR_TCS); 
		LDTCS_vDecideVCAOffBySpdDiff4Whl(&FA_TCS, &FL_TCS, &FR_TCS); 	  	
		LDTCS_vDecideVCAOffBySpdDiff4Whl(&RA_TCS, &RL_TCS, &RR_TCS); 
		LDTCS_vDecideVCAOffBySpdDiff();
		LDTCS_vDctBTCPrevCtrlWhl4VCA(&FA_TCS, &FL_TCS, &FR_TCS, &FL, &FR);
		LDTCS_vDctBTCPrevCtrlWhl4VCA(&RA_TCS, &RL_TCS, &RR_TCS, &RL, &RR);
	  #if (__SPLIT_TYPE==0)     			/*X-Split 일 경우*/  
		LDTCS_vDecideVCAMaxCtrlCirXsp();
	  #else									/*FR-Split 일 경우*/
		LDTCS_DecideVCAMaxCtrlCirFRsp(); 
	  #endif
	  	LDTCS_vSustainFlagAfterVCA();
	  	LDTCS_vDecideVrefCrtAidEnd();
    #endif                                       /* Temp VCA */
    
    LDTCS_vVIBDetectFlag(&FL_TCS);  /*Vibration control*/    
    LDTCS_vVIBDetectFlag(&FR_TCS); 
    LDTCS_vVIBDetectFlag(&RL_TCS); 
    LDTCS_vVIBDetectFlag(&RR_TCS);  
    
    LDTCS_vVIBDecideBrakeModeVib(&FL_TCS,&FL);
    LDTCS_vVIBDecideBrakeModeVib(&FR_TCS,&FR);
    LDTCS_vVIBDecideBrakeModeVib(&RL_TCS,&RL);
    LDTCS_vVIBDecideBrakeModeVib(&RR_TCS,&RR);
    
    LDTCS_vVIBDetectFlag4Engine();

    LDTCS_vDctIniMotorCtlMode ();
    
    #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/  		
	LDTCS_vDctLowPres4IniTCVCtlMode(&PC_TCS, &FR, &RL);
    LDTCS_vDctLowPres4IniTCVCtlMode(&SC_TCS, &FL, &RR);
	#else                     /*FR-Split 일 경우 */
	LDTCS_vDctLowPres4IniTCVCtlMode(&PC_TCS, &FL, &FR);
    LDTCS_vDctLowPres4IniTCVCtlMode(&SC_TCS, &RL, &RR);
	#endif
    
    LDTCS_vDctIniTCVCtlMode(&PC_TCS);
    LDTCS_vDctIniTCVCtlMode(&SC_TCS);

    LDTCS_vDctIIntCtlAct (&FA_TCS);
    LDTCS_vDctIIntCtlAct (&RA_TCS);

    LDTCS_vDctIintReset4FFAdapt (&FA_TCS);
    LDTCS_vDctIintReset4FFAdapt (&RA_TCS);

    LDTCS_vDctAdpFFCtlByIIntReset(&FA_TCS);
    LDTCS_vDctAdpFFCtlByIIntReset(&RA_TCS);

    LDTCS_vDctBrkCtlFadeOutMode(&FA_TCS,&FL_TCS,&FR_TCS);
    LDTCS_vDctBrkCtlFadeOutMode(&RA_TCS,&RL_TCS,&RR_TCS);
    
    LDTCS_vDctBrkCtlFadeOutCtrlCnt(&FA_TCS);
    LDTCS_vDctBrkCtlFadeOutCtrlCnt(&RA_TCS);

	LDTCS_vDctCtlWLSpdCros4FadOutCtl(&FA_TCS,&FL,&FR);
	LDTCS_vDctCtlWLSpdCros4FadOutCtl(&RA_TCS,&RL,&RR);

    /*LDTCS_vEstCirPresByTcCurnt(&PC_TCS);
    LDTCS_vEstCirPresByTcCurnt(&SC_TCS); IDB System*/

    #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/ 
    LDTCS_vEstWhlBrkTrqAndPresXsp();
    #else                     /*FR-Split 일 경우 */
    LDTCS_vEstWhlBrkTrqAndPresFRsp();
    #endif

    LDTCS_vDctSplitMuAxle(&FA_TCS,&FL_TCS,&FR_TCS);
    LDTCS_vDctSplitMuAxle(&RA_TCS,&RL_TCS,&RR_TCS);    
    LDTCS_vDctSplitMuFlag();
    LDTCS_vDctSplitHill(); 
    
    
    LCTCS_vTransAsymSpnCtlMod(&FA_TCS);
    LCTCS_vTransAsymSpnCtlMod(&RA_TCS);
  
    #if __MOTOR_OFF_FOR_NOISE
    LDTCS_vDctFlgOfAfterStall();
    LDTCS_vDctMtrOFFCondition4Noise(&FA_TCS,&FL_TCS,&FR_TCS);
	  LDTCS_vDctMtrOFFCondition4Noise(&RA_TCS,&RL_TCS,&RR_TCS);
	  #endif
  
    #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/ 
    LCTCS_vTransCirAsymSpnXspCtlMod();
    #else        /*FR-Split 일 경우 */
    LCTCS_vTransCirAsymSpnFRspCtlMod();
    #endif
     
    LDTCS_vDctTCSSplit();
    LDTCS_vDctAxleCondition4ETCS(&FA_TCS,&FL,&FR);
    LDTCS_vDctAxleCondition4ETCS(&RA_TCS,&RL,&RR);
    LDTCS_vDctSpinCondition4ETCS();
	

    LDTCS_vDctEndOfBrkCtlOfVeh();
    LDTCS_vDctEndOfBrkCtlOfWhl(&FL_TCS,&FL);
    LDTCS_vDctEndOfBrkCtlOfWhl(&FR_TCS,&FR);
    LDTCS_vDctEndOfBrkCtlOfWhl(&RL_TCS,&RL);
    LDTCS_vDctEndOfBrkCtlOfWhl(&RR_TCS,&RR);
    LDTCS_vDctEndOfBrkCtlOfDrvMd();
    LDTCS_vDctEndOfBrkCtlBySpdDiff(&FA_TCS,&FL_TCS,&FR_TCS,&FL,&FR);
    LDTCS_vDctEndOfBrkCtlBySpdDiff(&RA_TCS,&RL_TCS,&RR_TCS,&RL,&RR);
    LDTCS_vDctEndofBrkCtlByHunting(&FA_TCS,&FL,&FR);
    LDTCS_vDctEndofBrkCtlByHunting(&RA_TCS,&RL,&RR);    
    
    LDTCS_vDctEndofBrkCtlOfVCA();
    /*Same Wheel Brake Control in ESC*/
    LDTCS_vDctEndofBrkCtlofESC(&FL_TCS,&FL);
    LDTCS_vDctEndofBrkCtlofESC(&FR_TCS,&FR);
    LDTCS_vDctEndofBrkCtlofESC(&RL_TCS,&RL);
    LDTCS_vDctEndofBrkCtlofESC(&RR_TCS,&RR);
    /*Same Circuit Brake Control in ESC*/
    #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/ 
    LDTCS_vDctEndofBrkCtlofESC(&FL_TCS,&RR);
    LDTCS_vDctEndofBrkCtlofESC(&FR_TCS,&RL);
    LDTCS_vDctEndofBrkCtlofESC(&RL_TCS,&FR);
    LDTCS_vDctEndofBrkCtlofESC(&RR_TCS,&FL);
    #else					  /*FR-Split 일 경우 */	
    LDTCS_vDctEndofBrkCtlofESC(&FL_TCS,&FR);
    LDTCS_vDctEndofBrkCtlofESC(&FR_TCS,&FL);
    LDTCS_vDctEndofBrkCtlofESC(&RL_TCS,&RR);
    LDTCS_vDctEndofBrkCtlofESC(&RR_TCS,&RL);
    #endif    
    LDTCS_vDctEndofBrkCtlBySlip(&FL_TCS,&FL);  
    LDTCS_vDctEndofBrkCtlBySlip(&FR_TCS,&FR);  
    LDTCS_vDctEndofBrkCtlBySlip(&RL_TCS,&RL);  
    LDTCS_vDctEndofBrkCtlBySlip(&RR_TCS,&RR);      
	
	
	LDTCS_v2ndCpyBTCSActWhl(&FL_TCS,&FL); /*1scan delay가 HW에 영향 미치지 않도록 1회 더 호출 기존 copy함수와 동일하나 2번 호출시 old값 무효되므로 old값 처리부는 삭제*/
    LDTCS_v2ndCpyBTCSActWhl(&FR_TCS,&FR);
    LDTCS_v2ndCpyBTCSActWhl(&RL_TCS,&RL);
    LDTCS_v2ndCpyBTCSActWhl(&RR_TCS,&RR);
	LDTCS_v2ndDctBTCSActVehicle();    
    LDTCS_v2ndDctBTCSActAxle(&FA_TCS,&FL,&FR);
    LDTCS_v2ndDctBTCSActAxle(&RA_TCS,&RL,&RR);
   	LDTCS_v2ndDctBTCSActCir();

}

void LDTCS_vDetGearShift(void)
{
	if (AUTO_TM==1)
	{
		if (gs_ena==1)
		{
			lctcsu1GearShiftFlag = 1;
		}
		else
		{
			lctcsu1GearShiftFlag = 0;
		}
	}
	else
	{
		#if (NEW_FM_ENABLE == ENABLE)
		if (lctcsu1GearShiftFlag==0)
		{
			if( (vref>=VREF_5_KPH) &&	(fu1ClutchSwitch==1) && (fu1FMClutchErrDet==0) ) 
			{
				lctcsu1GearShiftFlag = 1;
				lctcsu1GearShiftFlagResetCheck = 0;
				lctcsu8GearShiftFlagResetCnt = 0;
			}
			else
			{
				lctcsu1GearShiftFlag = 0;
				lctcsu1GearShiftFlagResetCheck = 0;
				lctcsu8GearShiftFlagResetCnt = 0;
			}
		}
		else
		{
			if (fu1ClutchSwitch==0) 
			{
				lctcsu1GearShiftFlagResetCheck = 1;
			}
			else{}
			
			if (lctcsu1GearShiftFlagResetCheck==1)
			{	
				lctcsu8GearShiftFlagResetCnt = lctcsu8GearShiftFlagResetCnt + 1;
				lctcsu8GearShiftFlagResetCnt = (uint8_t)LCTCS_s16ILimitMaximum((int16_t)lctcsu8GearShiftFlagResetCnt, T_2_S);
			}
			else
			{
				lctcsu8GearShiftFlagResetCnt = 0;
			}
			
			if ( (vref < VREF_5_KPH) || (lctcsu8GearShiftFlagResetCnt > T_800_MS) || (fu1FMClutchErrDet==0) )
			{
				lctcsu1GearShiftFlag = 0;
				lctcsu1GearShiftFlagResetCheck = 0;
				lctcsu8GearShiftFlagResetCnt = 0;
			}
		}				
		#endif
	}
}

void LDTCS_vDetBrkEnInhibitByDriver(void)
{
	if( (lctcsu1BrkCtrlInhibit==1) || (lctcss16BrkCtrlMode==S16_TCS_CTRL_MODE_DISABLE) || (__TCS_DISABLE==1) )
	{
		BTCS_Entry_Inhibit_ByDriver = 1;
	}
	else if((ABS_fz==1)||(lctcsu1ABSCtrlOld4BTCSInhibit==1)) /*1scan delay for HW reset*/
	{
		BTCS_Entry_Inhibit_ByDriver = 1;
	}
  #if __TCS	
	else if( (lctcsu1EngInformInvalid==0) && (lctcsu1DriverAccelIntend==0) )
    {
        BTCS_Entry_Inhibit_ByDriver = 1;
	}
  #endif
  	else
  	{
  		BTCS_Entry_Inhibit_ByDriver = 0;
  	}
  	lctcsu1ABSCtrlOld4BTCSInhibit = ABS_fz;
}	

void LDTCS_vDetBrkEnInhibitByVehSpd(void)
{
	int16_t s16BtcsAllowMaxSpeed;
  #if __TCS
    s16BtcsAllowMaxSpeed = (int16_t)U16_TCS_BRK_CLT_MAX_SPD;
  #else
    s16BtcsAllowMaxSpeed = VREF_45_KPH;
  #endif
    if(vref2>s16BtcsAllowMaxSpeed)
	{
        BTCS_Entry_Inhibit_ByVehSpd = 1;
	}
	else
	{
        BTCS_Entry_Inhibit_ByVehSpd = 0;
	}
}

void LDTCS_vDetBrkEnInhibitByRPMTorq(void)
{
	if ((eng_torq<=(uint16_t)U16_TCS_START_ENGINE_TORQUE)
	#if !__ENG_RPM_DISABLE
   	|| (eng_rpm<=U16_TCS_START_RPM)
	#endif    	    			
	)
	{
		BTCS_Entry_Inhibit_ByRPMTorq = 1;
	}
	else
	{
		BTCS_Entry_Inhibit_ByRPMTorq = 0;
	}
}

void LDTCS_vDetCoordBrkInhibit(void)
{
	if ((BTCS_Entry_Inhibit_ByDriver == 1) || (BTCS_Entry_Inhibit_ByVehSpd == 1) || (BTCS_Entry_Inhibit_ByRPMTorq == 1))
	{
		BTCS_Entry_Inhibition = 1;
	}
	else
	{
		BTCS_Entry_Inhibition = 0;
	}
}

void LDTCS_vDetBrkEnInhibitBySWESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS)
{
	if (Wl_ESC->lctcsu1ESCWhlAtv == 1)
	{
		Wl_TCS->BTCS_Entry_Inhibition_BySWESC = 1;
	}
	else
	{
		Wl_TCS->BTCS_Entry_Inhibition_BySWESC = 0;
	}
}

void LDTCS_vDetBrkEnInhibitBySCESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS)
{
	if (Wl_ESC->lctcsu1ESCWhlAtv == 1)
	{
		Wl_TCS->BTCS_Entry_Inhibition_BySCESC = 1;
	}
	else
	{
		Wl_TCS->BTCS_Entry_Inhibition_BySCESC = 0;
	}
}

void LDTCS_vDetBrkEnInhibitByESC(struct TCS_WL_STRUCT *Wl_TCS)
{
	if ((Wl_TCS->BTCS_Entry_Inhibition_BySWESC == 1) || (Wl_TCS->BTCS_Entry_Inhibition_BySCESC == 1))
	{
		Wl_TCS->BTCS_Entry_Inhibition_ByESC = 1;
	}
	else
	{
		Wl_TCS->BTCS_Entry_Inhibition_ByESC = 0;
	}
}

static void LDTCS_vStartOfBrkCtlByAsymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT)
{
	if( (Axle_TCS->lctcsu1SymSpinDctFlg==0)
	  #if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))
		&&(( (WL_LEFT->BTCS==0)&&(WL_RIGHT->BTCS==0))|| (VCA_ON_TCMF==1))
	  #else
		&& ((WL_LEFT->BTCS==0)&&(WL_RIGHT->BTCS==0)) 
	  #endif  
		&&(BTCS_Entry_Inhibition==0)
		&&((Axle_TCS->ltcss16ChngRateWhlSpinDiffNF>0)||(lctcsu1Low2SplitSuspect==1))
		&&(ENG_STALL==0)
		&&(lctcsu1DriverAccelIntend==1)
		&&(lctcss16BrkCtrlMode!=S16_TCS_CTRL_MODE_DISABLE))
	{	
		Axle_TCS->ltcss16MonTime4AsymSpnCtlStrt = LCESP_s16IInter2Point(Axle_TCS->ltcss16ChngRateWhlSpinDiffNF, (int16_t)U8TCSCpChngRateWhlSpin_1, (int16_t)U8TCSCpMonTime4AsymSpnCtlS_1,
		                                                                                                        (int16_t)U8TCSCpChngRateWhlSpin_2, (int16_t)U8TCSCpMonTime4AsymSpnCtlS_2);		
		if(((Axle_TCS->ltcss16WhlSpinDiffNF > WL_TCS_LEFT->lctcss16AsymSpnCtlTh)
			||((lctcsu1Low2SplitSuspect==1)&&(Axle_TCS->ltcss16WhlSpinDiffNF > VREF_1_KPH)))
		 &&(WL_TCS_LEFT->BTCS_Entry_Inhibition_ByESC==0))
		{
			Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt = Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt+1;
			
      		if((Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt > Axle_TCS->ltcss16MonTime4AsymSpnCtlStrt)
      			||(lctcsu1Low2SplitSuspect==1))
      		{
      			WL_LEFT->BTCS = 1;
      		}
      		else
      		{
      			;
      		}
      	}
      	else if ((((Axle_TCS->ltcss16WhlSpinDiffNF) < (-WL_TCS_RIGHT->lctcss16AsymSpnCtlTh))
      					||((lctcsu1Low2SplitSuspect==1)&&(Axle_TCS->ltcss16WhlSpinDiffNF < -VREF_1_KPH)))
      	        &&(WL_TCS_RIGHT->BTCS_Entry_Inhibition_ByESC==0))
      	{
			Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt = Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt+1;
      		
      		if((Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt > Axle_TCS->ltcss16MonTime4AsymSpnCtlStrt)
      			||(lctcsu1Low2SplitSuspect==1))
      		{
      			WL_RIGHT->BTCS = 1;
      		}
      		else
      		{
      			;
      		}
      	}
      	else
      	{
      		Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt=0; 
      	}
	}
	else
	{
		Axle_TCS->ltcss16MonTime4AsymSpnCtlStrt = 0;
		Axle_TCS->ltcss16Cnt4AsymSpnCtlStrt = 0;
	}
}

static void LDTCS_vStartOfBrkCtlBySymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT)
{
	if (((ETCS_ON==1)||(FTCS_ON==1))
		&&(Axle_TCS->lctcsu1SymSpinDctFlg==1)
		&&(WL_LEFT->BTCS==0)
		&&(WL_RIGHT->BTCS==0)
		&&(Axle_TCS->ltcss16WhlAccAvg >= U8TCSCpAveWhlAcc_1)
		&&(BTCS_Entry_Inhibition==0)
		&&(ENG_STALL==0)
		&&(lctcsu1DriverAccelIntend==1)
		&&(lctcss16BrkCtrlMode!=S16_TCS_CTRL_MODE_DISABLE))
	{	
		Axle_TCS->ltcss16MonTime4SymSpnCtlStrt = LCESP_s16IInter2Point(Axle_TCS->ltcss16WhlAccAvg, (int16_t)U8TCSCpAveWhlAcc_1, (int16_t)U8TCSCpMonTime4SymSpnCtlS_1,
		                                                                                           (int16_t)U8TCSCpAveWhlAcc_2, (int16_t)U8TCSCpMonTime4SymSpnCtlS_2);		
		if((Axle_TCS->lctcss16WhlSpinAxleAvg > lctcss16SymSpnCtlTh)
		 &&(WL_TCS_LEFT->BTCS_Entry_Inhibition_ByESC==0)&&(WL_TCS_RIGHT->BTCS_Entry_Inhibition_ByESC==0))
		{
			Axle_TCS->ltcss16Cnt4SymSpnCtlStrt = Axle_TCS->ltcss16Cnt4SymSpnCtlStrt + 1;
			
      		if(Axle_TCS->ltcss16Cnt4SymSpnCtlStrt > Axle_TCS->ltcss16MonTime4SymSpnCtlStrt)
      		{
				WL_LEFT->BTCS = 1; 
				WL_RIGHT->BTCS = 1;
				Axle_TCS->ltcss16Cnt4SymSpnCtlStrt = 0;
      		}
      		else
      		{
      			;
      		}
      	}
      	else
      	{
      		Axle_TCS->ltcss16Cnt4SymSpnCtlStrt = 0;
      	}
	}
	else
	{
		Axle_TCS->ltcss16MonTime4SymSpnCtlStrt = 0;
		Axle_TCS->ltcss16Cnt4SymSpnCtlStrt = 0;
	}	
}

static void LDTCS_vBrkCtlTrnsAsym2SymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT, struct TCS_WL_STRUCT *WL_TCS_LEFT, struct TCS_WL_STRUCT *WL_TCS_RIGHT)
{
	
	if( 
		((WL_LEFT->BTCS==1)&&(WL_RIGHT->BTCS==0)) ||
		((WL_LEFT->BTCS==0)&&(WL_RIGHT->BTCS==1))
	  )
	{
		if((Axle_TCS->lctcsu1SymSpinDctFlgOld==0)&&(Axle_TCS->lctcsu1SymSpinDctFlg==1))
		{
			if((WL_TCS_LEFT->lctcss16WhlSpinF > lctcss16SymSpnCtlTh) && ( WL_TCS_RIGHT->lctcss16WhlSpinF > lctcss16SymSpnCtlTh))
			{
				WL_LEFT->BTCS=1; 
				WL_RIGHT->BTCS=1;
			}
			else
			{
				WL_LEFT->BTCS=0;
				WL_RIGHT->BTCS=0;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

static void LDTCS_vBrkCtlTrnsSym2AsymSlip(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)
{
	if((WL_LEFT->BTCS==1)&&(WL_RIGHT->BTCS==1))
	{
		if((Axle_TCS->lctcsu1SymSpinDctFlgOld==1)&&(Axle_TCS->lctcsu1SymSpinDctFlg==0))
		{
			if(Axle_TCS->ltcss16WhlSpinDiffNF>=0)
			{
				WL_LEFT->BTCS=1; 
				WL_RIGHT->BTCS=0;
			}
			else
			{
				WL_LEFT->BTCS=0; 
				WL_RIGHT->BTCS=1;
			}
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

#if __MOTOR_OFF_FOR_NOISE
static void LDTCS_vDctMtrOFFCondition4Noise(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if (((Axle_TCS->ltcsu1ModOfAsymSpnCtl == S16TCSAsymSpinBwdCtlMode)&&(VCA_ON_TCMF==0)&&(ENG_STALL==0)&&(lctcsu1FlgOfAfterStall==0)&&(lctcsu1DetectVibration==0))
		&&(((WL_LEFT->lctcsu1BTCSWhlAtv == 1)&&( WL_LEFT->lctcss16BTCSWhlOnTime >= InitialMotorOffInhibitScan))
		||((WL_RIGHT->lctcsu1BTCSWhlAtv == 1)&&( WL_RIGHT->lctcss16BTCSWhlOnTime >= InitialMotorOffInhibitScan)))
		)
		{

			if((Axle_TCS->lctcsu1MotorOFF4Noise==0)                            /*Motor Off set condition*/
				&&((Axle_TCS->ltcsu1SpinDiffInc == 0)||(Axle_TCS->lctcss16BsTarBrkTorqAXLEdiff <= U8TCSFadeOutBrkCtrlMinTorq))
				||((Axle_TCS->lctcsu1MotorOFF4Noise == 1) && (Axle_TCS->lctcsu1MotorOFF4NoiseCnt <= MinimumMotorOffScan))
				)
				{
					Axle_TCS->lctcsu1MotorOFF4Noise = 1;
				}
				else
				{ ; }

					/*Motor off reset condition*/
				if((Axle_TCS->ltcss16ChngRateWhlSpinDiffNF >= MotorOnChngRateWhlSpinDifTh)
						||(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
						||(Axle_TCS->lctcsu1FlgOfAfterHghMuUnstb == 1)
						||((Axle_TCS->lctcsu1MotorOFF4Noise == 1) && (Axle_TCS->lctcsu1MotorOFF4NoiseCnt >= MinimumMotorOffScan) && (Axle_TCS->ltcsu1SpinDiffInc == 1) ))
						{
							Axle_TCS->lctcsu1MotorOFF4Noise = 0;
							Axle_TCS->lctcsu1MotorOFF4NoiseCnt = 0;
						}
						else
						{
							;
						}

					if (Axle_TCS->lctcsu1MotorOFF4Noise == 1)
					{
							Axle_TCS->lctcsu1MotorOFF4NoiseCnt = Axle_TCS->lctcsu1MotorOFF4NoiseCnt + 1;
							Axle_TCS->lctcsu1MotorOFF4NoiseCnt = LCTCS_s16ILimitMaximum(Axle_TCS->lctcsu1MotorOFF4NoiseCnt, MinimumMotorOffScan);
					}
					else{;}
		}
		else
		{
				Axle_TCS->lctcsu1MotorOFF4Noise = 0;
				Axle_TCS->lctcsu1MotorOFF4NoiseCnt = 0;
		}
}

#endif  

/*
static void LDTCS_vStartOfBrkCtlByStuck(void)
{
	if(TCS_STUCK_ON == 1)
	{
		if(FA_TCS.lctcss16WhlSpdAxleAvg > RA_TCS.lctcss16WhlSpdAxleAvg)
		{	
	        if(FL_TCS.lctcss16BsTarBrkTrq4StuckCtl>0)
	        {
				FL.BTCS=1;
	        }
	        else
	        {
	        	FR.BTCS=1;
	        }
	    }
	    else
	    {
			if(RL_TCS.lctcss16BsTarBrkTrq4StuckCtl>0)
	  		{
				RL.BTCS=1; 	
	  		}
			else
			{
				RR.BTCS=1;                           
			}              
	    }
	}
	else
	{
		;
	}
}
*/

static void LDTCS_vStartOfBrkCtlByVCA(void)
{
	tcs_tempW0 = (int16_t)U8TCSCpRearCtlOfVCA; 
	
	if (VCA_ON_TCMF == 1) 
	{
		if (tcs_tempW0 == 1)
		{
	  		if ( RL_TCS.VCA_TCMF_CTL == 1)
			{
				RL.BTCS=1;
				/*temp*/
				if ( FR_TCS.VCA_TCMF_CTL == 1)
			{
				FR.BTCS=1; 	
			}
			else
			{
				;
				} /*temp*/
			}
		    else if ( RR_TCS.VCA_TCMF_CTL == 1)
		{
		    	RR.BTCS=1; 	
		    	/*temp*/
				if ( FL_TCS.VCA_TCMF_CTL == 1)
			{
					FL.BTCS=1;
		    }
				else
		    {
					;
				} /*temp*/
		}
		else
		{
			;
		}
	}
		else
		{
			if ( FL_TCS.VCA_TCMF_CTL == 1)
		{
				FL.BTCS=1; 	
			}
			else if ( FR_TCS.VCA_TCMF_CTL == 1)
			{
				FR.BTCS=1;
		}
		else
		{
			;
		}
	}
}
	else
	{
		;
	}	
}

static void LDTCS_vStartOfCordBrkCtl(void)
{
	if((FL.BTCS==1)||(FR.BTCS==1)||(RL.BTCS==1)||(RR.BTCS==1))
	{
		BTC_fz = 1;	
	}
	else
	{
		BTC_fz = 0;	
	}
}

static void LDTCS_vCpyBTCSActWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	Wl_TCS->lctcsu1BTCSWhlAtvOld = Wl_TCS->lctcsu1BTCSWhlAtv;
	Wl_TCS->lctcsu1BTCSWhlAtv = W_L->BTCS;
	
	if((Wl_TCS->lctcsu1BTCSWhlAtvOld==0)&&(Wl_TCS->lctcsu1BTCSWhlAtv==1))
	{
		Wl_TCS->lctcsu1BTCSWhlRsgEdg = 1;
	}
	else
	{
		Wl_TCS->lctcsu1BTCSWhlRsgEdg = 0;
	}	
}

static void LDTCS_v2ndCpyBTCSActWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	Wl_TCS->lctcsu1BTCSWhlAtv = W_L->BTCS;
	if((Wl_TCS->lctcsu1BTCSWhlAtvOld==0)&&(Wl_TCS->lctcsu1BTCSWhlAtv==1))
	{
		Wl_TCS->lctcsu1BTCSWhlRsgEdg = 1;
	}
	else
	{
		Wl_TCS->lctcsu1BTCSWhlRsgEdg = 0;
	}	
}

static void LDTCS_vCntBTCSActWhlOnTime(struct TCS_WL_STRUCT *Wl_TCS)
{
	if (Wl_TCS->lctcsu1BTCSWhlAtv==1)
	{
		Wl_TCS->lctcss16BTCSWhlOnTime = Wl_TCS->lctcss16BTCSWhlOnTime + 1;
		Wl_TCS->lctcss16BTCSWhlOnTime = LCTCS_s16ILimitMaximum(Wl_TCS->lctcss16BTCSWhlOnTime, L_U16_TIME_10MSLOOP_5MIN);
	}
	else
	{
		Wl_TCS->lctcss16BTCSWhlOnTime = 0;
	}
}

#if __MOTOR_OFF_FOR_NOISE
static void LDTCS_vDctFlgOfAfterStall(void)
{
	/*Detection Condition*/
	if((ENG_STALL_OLD ==1)&&(ENG_STALL==0))
	{
		 lctcsu1FlgOfAfterStall = 1;
     lctcsu16CntAfterStall = 0;
		
	}
	else
	{
		 /*Clear Condition*/
        if (lctcsu1FlgOfAfterStall == 1)
        {
            if (  (lctcsu16CntAfterStall >= U8CpAfterStallCntTh)
                ||(ENG_STALL == 1)
                ||(BTCS_ON == 0)
                )
            {
                lctcsu1FlgOfAfterStall = 0;
                lctcsu16CntAfterStall = 0;
            }
            else
            {
                lctcsu16CntAfterStall = lctcsu16CntAfterStall + 1;
            }
        }
        else
        {
            ;
        }
	}
}
#endif


static void LDTCS_vDctBTCSActVehicle (void)
{
	lctcsu1BTCSVehAtvOld = lctcsu1BTCSVehAtv;
	
	if((FL_TCS.lctcsu1BTCSWhlAtv ==1)||(FR_TCS.lctcsu1BTCSWhlAtv==1)||(RL_TCS.lctcsu1BTCSWhlAtv==1)||(RR_TCS.lctcsu1BTCSWhlAtv==1))
	{
		lctcsu1BTCSVehAtv=1;
	}
	else
	{
		lctcsu1BTCSVehAtv=0;
	}
	
	if((lctcsu1BTCSVehAtvOld==0)&&(lctcsu1BTCSVehAtv==1))
	{
		lctcsu1BTCSVehAtvRsgEdg =1;
	}
	else
	{
		lctcsu1BTCSVehAtvRsgEdg =0;
	}
}

static void LDTCS_v2ndDctBTCSActVehicle (void)
{
	if((FL_TCS.lctcsu1BTCSWhlAtv ==1)||(FR_TCS.lctcsu1BTCSWhlAtv==1)||(RL_TCS.lctcsu1BTCSWhlAtv==1)||(RR_TCS.lctcsu1BTCSWhlAtv==1))
	{
		lctcsu1BTCSVehAtv=1;
	}
	else
	{
		lctcsu1BTCSVehAtv=0;
	}
	
	if((lctcsu1BTCSVehAtvOld==0)&&(lctcsu1BTCSVehAtv==1))
	{
		lctcsu1BTCSVehAtvRsgEdg =1;
	}
	else
	{
		lctcsu1BTCSVehAtvRsgEdg =0;
	}
}

static void LDTCS_vDctBTCSActAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)
{
	if((WL_LEFT->BTCS==1)||(WL_RIGHT->BTCS==1))
	{
		Axle_TCS->lctcsu1BrkCtlActAxle =1;
	}
	else
	{
		Axle_TCS->lctcsu1BrkCtlActAxle =0;
	}
}

static void LDTCS_v2ndDctBTCSActAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)
{
	if((WL_LEFT->BTCS==1)||(WL_RIGHT->BTCS==1))
	{
		Axle_TCS->lctcsu1BrkCtlActAxle =1;
	}
	else
	{
		Axle_TCS->lctcsu1BrkCtlActAxle =0;
	}
}

static void LDTCS_vDctBTCSActCir(void)
{
  	PC_TCS.lctcsu1BrkCtlActCirOld = PC_TCS.lctcsu1BrkCtlActCir;
  	SC_TCS.lctcsu1BrkCtlActCirOld = SC_TCS.lctcsu1BrkCtlActCir;

  #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/
	/* Primary Circuit */
	if ( (FR_TCS.lctcsu1BTCSWhlAtv==1) || (RL_TCS.lctcsu1BTCSWhlAtv==1) )
  	{
  		PC_TCS.lctcsu1BrkCtlActCir = 1;
  	}
  	else
  	{
  		PC_TCS.lctcsu1BrkCtlActCir = 0;
  	}  	
  	
  	/* Secondary Circuit */
  	if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) || (RR_TCS.lctcsu1BTCSWhlAtv==1) )
  	{
  		SC_TCS.lctcsu1BrkCtlActCir = 1;
  	}
  	else
  	{
  		SC_TCS.lctcsu1BrkCtlActCir = 0;
  	}
  #else	/* FR Split */
	PC_TCS.lctcsu1BrkCtlActCir = FA_TCS.lctcsu1BrkCtlActAxle;
	SC_TCS.lctcsu1BrkCtlActCir = RA_TCS.lctcsu1BrkCtlActAxle;
  #endif

	if ( (PC_TCS.lctcsu1BrkCtlActCirOld==0) && (PC_TCS.lctcsu1BrkCtlActCir==1) )
	{
		PC_TCS.lctcsu1BrkCtlActCirRsgEdg = 1;
	}
	else
	{
		PC_TCS.lctcsu1BrkCtlActCirRsgEdg = 0;
	}
	
	if ( (SC_TCS.lctcsu1BrkCtlActCirOld==0) && (SC_TCS.lctcsu1BrkCtlActCir==1) )
	{
		SC_TCS.lctcsu1BrkCtlActCirRsgEdg = 1;
	}
	else
	{
		SC_TCS.lctcsu1BrkCtlActCirRsgEdg = 0;
	}	
}

static void LDTCS_v2ndDctBTCSActCir(void)
{
  #if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/
	/* Primary Circuit */
	if ( (FR_TCS.lctcsu1BTCSWhlAtv==1) || (RL_TCS.lctcsu1BTCSWhlAtv==1) )
  	{
  		PC_TCS.lctcsu1BrkCtlActCir = 1;
  	}
  	else
  	{
  		PC_TCS.lctcsu1BrkCtlActCir = 0;
  	}  	
  	
  	/* Secondary Circuit */
  	if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) || (RR_TCS.lctcsu1BTCSWhlAtv==1) )
  	{
  		SC_TCS.lctcsu1BrkCtlActCir = 1;
  	}
  	else
  	{
  		SC_TCS.lctcsu1BrkCtlActCir = 0;
  	}
  #else	/* FR Split */
	PC_TCS.lctcsu1BrkCtlActCir = FA_TCS.lctcsu1BrkCtlActAxle;
	SC_TCS.lctcsu1BrkCtlActCir = RA_TCS.lctcsu1BrkCtlActAxle;
  #endif

	if ( (PC_TCS.lctcsu1BrkCtlActCirOld==0) && (PC_TCS.lctcsu1BrkCtlActCir==1) )
	{
		PC_TCS.lctcsu1BrkCtlActCirRsgEdg = 1;
	}
	else
	{
		PC_TCS.lctcsu1BrkCtlActCirRsgEdg = 0;
	}
	
	if ( (SC_TCS.lctcsu1BrkCtlActCirOld==0) && (SC_TCS.lctcsu1BrkCtlActCir==1) )
	{
		SC_TCS.lctcsu1BrkCtlActCirRsgEdg = 1;
	}
	else
	{
		SC_TCS.lctcsu1BrkCtlActCirRsgEdg = 0;
	}	
}

void LDTCS_vDctSymSpin(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
    Axle_TCS->lctcsu1SymSpinDctFlgOld = Axle_TCS->lctcsu1SymSpinDctFlg;
    
    tcs_tempW0=LCTCS_s16ILimitMinimum((int16_t)U8TCSCpSymBoth2WlSpinTh,(int16_t)VREF_3_KPH ); /*Symmetric 해지 속도(2KPH) 고려한 히스테리시스 필요 최소값 limit*/

    Axle_TCS->tcs_axle_tempW1 = McrAbs(WL_LEFT->lctcss16EstWhlPre - WL_RIGHT->lctcss16EstWhlPre);
    Axle_TCS->tcs_axle_tempW2 = McrAbs(WL_LEFT->lctcss16TarWhlPre - WL_RIGHT->lctcss16TarWhlPre);

    if(Axle_TCS->lctcsu1SymSpinDctFlg == 0)     /* Symmetric Spin Detection Conditon*/
    {
        if( (Axle_TCS->ltcss16MovAvgDiffLAndR < U8TCSCpSymBoth2WlSpdDifTh)
            &&((WL_LEFT->lctcss16WhlSpinF>(tcs_tempW0))&&(WL_RIGHT->lctcss16WhlSpinF>(tcs_tempW0)))
            &&(Axle_TCS->tcs_axle_tempW1 <= U8TCSCpPresDifLR4SymSp)
            &&(Axle_TCS->tcs_axle_tempW2 <= U8TCSCpPresDifLR4SymSp) 
            &&(ETCS_ON==1) /* MBD ETCS */
		  )
        {
            Axle_TCS->lctcss16SymSpinDctCnt = Axle_TCS->lctcss16SymSpinDctCnt + 1;
            if  ( Axle_TCS->lctcss16SymSpinDctCnt >= U8TCSCpSymDctScanTime )
            {	
            	Axle_TCS->lctcsu1SymSpinDctFlg = 1;
            }
            else
            {
            	;
            }
        }
        else
        {
            Axle_TCS->lctcss16SymSpinDctCnt = 0;
        }
    }
    else        /* Symmetric Spin Clear Conditon*/
    {
        if((((WL_LEFT->lctcss16WhlSpinF <  VREF_2_KPH) || (WL_RIGHT->lctcss16WhlSpinF <  VREF_2_KPH)
            || (Axle_TCS->ltcss16MovAvgDiffLAndR >= U8TCSCpSymBoth2WlSpdDifTh)
            || (Axle_TCS->tcs_axle_tempW1 > U8TCSCpPresDifLR4SymSp))
            &&(Axle_TCS->ltcss16MovAvgDiffLAndR >= (U8TCSCpSymBoth2WlSpdDifTh + U8TCSCpSymExtHysOfMovDiffLnR))) /* 좌우 속도차 조건  hysteresis 적용 */
            &&(ETCS_ON==1) /* MBD ETCS */
            )
        {
            Axle_TCS->lctcsu1SymSpinDctFlg = 0;
        }
        else
        {
            ;
        }
    }
  #if (__4WD_VARIANT_CODE) || (__4WD==1)  
	if (lctcsu1DrvMode4Wheel==1)
    {
    	Axle_TCS->lctcsu1SymSpinDctFlg = 0;
    }
    else
    {
    	;
    }
  #endif
}

void LDTCS_vDctOkCondi4FFAdapt(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst >= S16TCSCpWorkTh4FFAdapt)
	{
		Axle_TCS->lctcsu1OK2AdaptFF = 1;
	}
	else
	{
		Axle_TCS->lctcsu1OK2AdaptFF = 0;
	}
}

void LDTCS_vDctOKCondi4BrkTrqMaxLim (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst >= S16TCSCpWorkTh4BrkTrqMaxLim)
	{
		Axle_TCS->lctcsu1OK2BrkTrqMaxLim = 1;
	}
	else
	{
		Axle_TCS->lctcsu1OK2BrkTrqMaxLim = 0;
	}
}

void LDTCS_vDctHuntingInAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if ( (ETCS_ON==1) || (FTCS_ON==1) )
	{
		if ( (Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl==0) && (Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl==0) )
		{
			if		( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable) )
			{
				Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl = 1;
			}
			else if	( (WL_RIGHT->lctcsu1BTCSWhlAtv==1) && (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable) )
			{
				Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl = 1;
			}
			else
			{
				;
			}
		}
		else
		{
			if (Axle_TCS->lctcsu1SymSpinDctFlg==1)
			{   /* Symmetric spin 감지 되면 Hunting detection procedure를 reset 함 */
				Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl = 0;
				Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl = 0;
			}
			else
			{				
			if 		( (Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
			{
				Axle_TCS->lctcsu1HuntingDcted = 1;
			}
			else if ( (Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl==1) && (WL_LEFT->lctcsu1BTCSWhlAtv==1) )
			{
				Axle_TCS->lctcsu1HuntingDcted = 1;				
			}
			else
			{
				;
			}
		}
	}
		
		/* Hunting 감지 해제 조건 */
		if (Axle_TCS->lctcsu1HuntingDcted==1)
		{	
			Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl = 0;
			Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl = 0;
			if ((Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst >= S16TCSCpWorkTh4HuntDctReset)
				||(lctcsu1DctSplitHillFlg==1))
			{
				Axle_TCS->lctcsu1HuntingDcted = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}			
	}
	else
	{
		Axle_TCS->lctcsu1HghMuWhlUnstbInLftWhlCtl = 0;
		Axle_TCS->lctcsu1HghMuWhlUnstbInRgtWhlCtl = 0;
		Axle_TCS->lctcsu1HuntingDcted = 0;		
	}
}

void LDTCS_vDctLowToSplit(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	Axle_TCS->lctcsu1L2SplitSuspectDctedOld = Axle_TCS->lctcsu1L2SplitSuspectDcted;

	if ((Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuStable)
  	  &&(Axle_TCS->lctcsu1HuntingDcted == 1)
  	  &&(Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst >= S16TCSCpWorkTh4L2SplitSuspect))
	{
		Axle_TCS->lctcsu1L2SplitSuspectDcted = 1;
		Axle_TCS->lctcss16CntAfterL2SplitSus = Axle_TCS->lctcss16CntAfterL2SplitSus + 1;
		Axle_TCS->lctcss16CntAfterL2SplitSus = LCTCS_s16ILimitMaximum((int16_t)Axle_TCS->lctcss16CntAfterL2SplitSus, 100/*S16TCSCpL2SplitSuspectMaxScan*/);
	}
	else
	{
		Axle_TCS->lctcsu1L2SplitSuspectDcted = 0;
		Axle_TCS->lctcss16CntAfterL2SplitSus = 0;
	}
}

void LDTCS_vDctWhlOnHghMu(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
		/* lctcss16BsTarBrkTorq4AsymSCtl의 크기 비교로 high-mu 판단시 brake torque가 0이 되면 판단이 reset되는 문제 발생 */
	if		( (WL_LEFT->lctcsu1BTCSWhlAtv==1) && (WL_RIGHT->lctcsu1BTCSWhlAtv==0) )
        {
            Axle_TCS->lctcsu8WhlOnHghMu=TCSRightHighMu;
        }
	else if	( (WL_LEFT->lctcsu1BTCSWhlAtv==0) && (WL_RIGHT->lctcsu1BTCSWhlAtv==1) )
        {
            Axle_TCS->lctcsu8WhlOnHghMu=TCSLeftHighMu;
        }
        else
        {
            Axle_TCS->lctcsu8WhlOnHghMu = TCSHighMuNotDcted;
        }
    }

void LDTCS_vDctInstbOfHghMuWhl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
    Axle_TCS->lctcsu8StatWhlOnHghMuOld = Axle_TCS->lctcsu8StatWhlOnHghMu;

    if      (Axle_TCS->lctcsu8WhlOnHghMu == TCSLeftHighMu)  /*Left High Mu*/
    {
		if 		(
				  (WL_RIGHT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh) &&
				  (Axle_TCS->ltcss16WhlSpinDiffNF > VREF_1_KPH) &&  /*Low mu speed 일정값 이상이며 high mu wheel과 역전*/
				  (WL_L->arad > 0) 
				)
		{	/* When speed of non-controlling wheel is faster than controlling wheel */
			WL_LEFT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
		}
		else if ( (Axle_TCS->lctcsu1HuntingDcted==1) && (WL_LEFT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlHuntSpinTh) )
		{	/* When hunting is detected and spin of wheel on high-mu is greater then spin threshold. */
			WL_LEFT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
		}
        else if	( (WL_LEFT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh)
            && (Axle_TCS->ltcss16WhlSpinDiffNF >= -(int16_t)U8TCSCpHghMuWhlDelSpinTh)
            && (WL_L->arad > 0)
            )
            {
                WL_LEFT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
            }
        else if ( (WL_LEFT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh)
                &&(Axle_TCS->ltcss16ChngRateWhlSpinDiffNF < 0)
        	)
            {
            	tcs_tempW0 = LCESP_s16IInter3Point(WL_L->arad, ARAD_4G0, 1, ARAD_5G0, 2, ARAD_9G0, 4);
                WL_LEFT->lctcsu8HghMuWhlSpnCnt = WL_LEFT->lctcsu8HghMuWhlSpnCnt + (uint8_t)tcs_tempW0;
            WL_LEFT->lctcsu8HghMuWhlSpnCnt=(uint8_t)LCTCS_s16ILimitMaximum((int16_t)(WL_LEFT->lctcsu8HghMuWhlSpnCnt), 100);
        }
        else
        {
            WL_LEFT->lctcsu8HghMuWhlSpnCnt=0;
        }

        WL_RIGHT->lctcsu8HghMuWhlSpnCnt=0;

        if      (WL_LEFT->lctcsu8HghMuWhlSpnCnt > U8TCSCpHghMuWhlSpinTm)
        {
            Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuUnStable;
        }
        else if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
        {
            if ( (WL_LEFT->lctcss16WhlSpinNF < VREF_1_KPH) ||
                 ((Axle_TCS->ltcss16ChngRateWhlSpinDiffNF > 0)&&(Axle_TCS->ltcss16WhlSpinDiffNF < -VREF_2_5_KPH)))
            {     
            	Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuStable;
        	}
        	else
        	{
            	;
            }
        }
        else
        {
            Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuStable;
        }
    }
    else if     (Axle_TCS->lctcsu8WhlOnHghMu == TCSRightHighMu)  /*Right High Mu*/
    {
		if 		(
				  (WL_LEFT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh) &&
				  (Axle_TCS->ltcss16WhlSpinDiffNF < -VREF_1_KPH) &&
				  (WL_R->arad > 0) 
				)
		{	/* When speed of non-controlling wheel is faster than controlling wheel */
			WL_RIGHT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
		}
		else if ( (Axle_TCS->lctcsu1HuntingDcted==1) && (WL_RIGHT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlHuntSpinTh) )
		{	/* When hunting is detected and spin of wheel on high-mu is greater then spin threshold */
			WL_RIGHT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
		}		
        else if	( (WL_RIGHT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh)
                && (Axle_TCS->ltcss16WhlSpinDiffNF <= (int16_t)U8TCSCpHghMuWhlDelSpinTh)
                && (WL_R->arad >0)
            )
            {
                WL_RIGHT->lctcsu8HghMuWhlSpnCnt = U8TCSCpHghMuWhlSpinTm+1;
            }
        else if ( (WL_RIGHT->lctcss16WhlSpinNF > U8TCSCpHghMuWhlSpinTh)
               &&(Axle_TCS->ltcss16ChngRateWhlSpinDiffNF < 0)
        	)
            {
            	tcs_tempW0 = LCESP_s16IInter3Point(WL_R->arad, ARAD_4G0, 1, ARAD_5G0, 2, ARAD_9G0, 4);
                WL_RIGHT->lctcsu8HghMuWhlSpnCnt = WL_RIGHT->lctcsu8HghMuWhlSpnCnt + (uint8_t)tcs_tempW0;
            WL_RIGHT->lctcsu8HghMuWhlSpnCnt=(uint8_t)LCTCS_s16ILimitMaximum((int16_t)(WL_RIGHT->lctcsu8HghMuWhlSpnCnt), 100);
        }
        else
        {
            WL_RIGHT->lctcsu8HghMuWhlSpnCnt=0;
        }

        WL_LEFT->lctcsu8HghMuWhlSpnCnt=0;

        if      (WL_RIGHT->lctcsu8HghMuWhlSpnCnt > U8TCSCpHghMuWhlSpinTm)
        {
            Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuUnStable;
        }
        else if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
        {
            if ( (WL_RIGHT->lctcss16WhlSpinNF < VREF_1_KPH) ||
                 ((Axle_TCS->ltcss16ChngRateWhlSpinDiffNF > 0)&&(Axle_TCS->ltcss16WhlSpinDiffNF > VREF_2_5_KPH)))
            {
            	Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuStable;
        	}
        	else
        	{
            	;
            }
        }
        else
        {
            Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuStable;
        }
    }
    else
    {
         WL_LEFT->lctcsu8HghMuWhlSpnCnt=0;
         WL_RIGHT->lctcsu8HghMuWhlSpnCnt=0;
         Axle_TCS->lctcsu8StatWhlOnHghMu = TCSHighMuNotDcted;
    }
}

void LDTCS_vDctInstbOfHghMuWhl4EngCl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if (Axle_TCS->lctcsu8WhlOnHghMu == TCSLeftHighMu)  /*Left High Mu*/
	{
		if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
		{
			WL_LEFT->lctcsu8StatOneWhlOnHghMu = TCSHighMuUnStable;
		}
		else
		{
			WL_LEFT->lctcsu8StatOneWhlOnHghMu = TCSHighMuStable;
		}
		WL_RIGHT->lctcsu8StatOneWhlOnHghMu = TCSHighMuNotDcted;
	}
	else if (Axle_TCS->lctcsu8WhlOnHghMu == TCSRightHighMu)  /*Right High Mu*/
	{
		if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
		{
			WL_RIGHT->lctcsu8StatOneWhlOnHghMu = TCSHighMuUnStable;
		}
		else
		{
            WL_RIGHT->lctcsu8StatOneWhlOnHghMu = TCSHighMuStable;
        }
        WL_LEFT->lctcsu8StatOneWhlOnHghMu = TCSHighMuNotDcted;
    }
    else
    {
         WL_LEFT->lctcsu8StatOneWhlOnHghMu = TCSHighMuNotDcted;
         WL_RIGHT->lctcsu8StatOneWhlOnHghMu = TCSHighMuNotDcted;
    }
}

void LDTCS_vDctTransOfHghMuWhlStat (struct TCS_AXLE_STRUCT *Axle_TCS)
{
    if((Axle_TCS->lctcsu8StatWhlOnHghMuOld != TCSHighMuUnStable)&&(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable))
    {
        Axle_TCS->lctcsu1Any2HghMuWhlUnstb = 1;
        Axle_TCS->lctcsu1HghMuWhlUnstb2Any = 0;
    }
    else if ((Axle_TCS->lctcsu8StatWhlOnHghMuOld == TCSHighMuUnStable)&&(Axle_TCS->lctcsu8StatWhlOnHghMu != TCSHighMuUnStable))
    {
        Axle_TCS->lctcsu1Any2HghMuWhlUnstb = 0;
        Axle_TCS->lctcsu1HghMuWhlUnstb2Any = 1;
    }
    else
    {
        Axle_TCS->lctcsu1Any2HghMuWhlUnstb = 0;
        Axle_TCS->lctcsu1HghMuWhlUnstb2Any = 0;
    }
}

void LDTCS_vDctLowSpin4HghMuReCovery(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
	if (  ((WL_L->BTCS == 1) && (WL_R->BTCS == 0)&&(WL_LEFT->lctcss16WhlSpinNF < U8TCSHghMuUnstbRecoveryTh)) 
		||  ((WL_L->BTCS == 0)&&(WL_R->BTCS == 1)&&(WL_RIGHT->lctcss16WhlSpinNF < U8TCSHghMuUnstbRecoveryTh))
		|| (Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1)  )
		
	{	
		Axle_TCS->lctcsu1LowSpinofCtrlAxle = 1;
	}
	else
	{
		Axle_TCS->lctcsu1LowSpinofCtrlAxle = 0;
	}
}

void LDTCS_vDctFlgOfAfterHghMuUnstb(struct TCS_AXLE_STRUCT *Axle_TCS)
{
    /*Detection Condition*/
    if (Axle_TCS->lctcsu1HghMuWhlUnstb2Any == 1)
    {
        Axle_TCS->lctcsu1FlgOfAfterHghMuUnstb = 1;
        Axle_TCS->lctcsu16CntAfterHghMuUnstb = 0;
    }
    else
    {
        /*Clear Condition*/
        if (Axle_TCS->lctcsu1FlgOfAfterHghMuUnstb == 1)
        {
            if (  (Axle_TCS->lctcsu16CntAfterHghMuUnstb >= U8CpAfterHghMuUnstbCntTh)
                ||(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
                ||(Axle_TCS->lctcsu1BrkCtlActAxle == 0)
                )
            {
                Axle_TCS->lctcsu1FlgOfAfterHghMuUnstb = 0;
                Axle_TCS->lctcsu16CntAfterHghMuUnstb = 0;
                Axle_TCS->lctcsu1FlgOfHSSRecvry = 0;
            }
            else
            {
                Axle_TCS->lctcsu16CntAfterHghMuUnstb = Axle_TCS->lctcsu16CntAfterHghMuUnstb + 1;
            }
        }
        else
        {
            ;
        }
    }
}

void LDTCS_vDctFastDecPresInHghMuUstb(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
	if(Axle_TCS->lctcsu8StatWhlOnHghMu==TCSHighMuUnStable)
	{
		if (Axle_TCS->lctcsu8WhlOnHghMu == TCSLeftHighMu) /*Right control*/
		{
			if ((Axle_TCS->ltcss16WhlSpinDiffNF > VREF_3_KPH)&&(WL_L->arad > 0))
			{
				Axle_TCS->lctcsu1FastDecPresInHghUstb = 1;
			}
			else
			{
				Axle_TCS->lctcsu1FastDecPresInHghUstb = 0;
			}
		}	
		else if (Axle_TCS->lctcsu8WhlOnHghMu == TCSRightHighMu) /*Left control*/
		{
			if((Axle_TCS->ltcss16WhlSpinDiffNF < -VREF_3_KPH)&&(WL_R->arad > 0))
			{
				Axle_TCS->lctcsu1FastDecPresInHghUstb = 1;
			}
			else
			{
				Axle_TCS->lctcsu1FastDecPresInHghUstb = 0;
			}	
		}
		else
		{	
			Axle_TCS->lctcsu1FastDecPresInHghUstb = 0;
		}
	}
	else
	{
		Axle_TCS->lctcsu1FastDecPresInHghUstb = 0;	
	}
	
}

void LDTCS_vDctHghMuUstbRepeatCycle(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->lctcss16HghMuUstbRepeatCycleOld = Axle_TCS->lctcss16HghMuUstbRepeatCycle;
	
	if((Axle_TCS->lctcsu1BrkCtlActAxle==1)&&(lctcsu1DctSplitHillFlg==0))
	{	
		if(Axle_TCS->lctcsu1HghMuWhlUnstb2Any==1)
		{
			Axle_TCS->lctcss16HghMuUstbRepeatCycle = Axle_TCS->lctcss16HghMuUstbRepeatCycle + 1;
			Axle_TCS->lctcss16HghMuUstbRepeatCycle = LCTCS_s16ILimitMaximum( Axle_TCS->lctcss16HghMuUstbRepeatCycle, L_U16_CNT_500 );
		}
		else
		{
			;
		}
	}
	else
	{
		Axle_TCS->lctcss16HghMuUstbRepeatCycle = 0;
	}
}

void LDTCS_vDctHghMuUstbCntStartFlg(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if(Axle_TCS->lctcsu1BrkCtlActAxle==1)
	{	
		if(Axle_TCS->lctcsu1HghMuWhlUnstb2Any==1)
		{
			Axle_TCS->lctcsu1HghMuWhlUnstbAftFlg = 1;
		}
		else if (Axle_TCS->lctcsu1Any2HghMuWhlUnstb==1)
		{
			Axle_TCS->lctcsu1HghMuWhlUnstbAftFlg = 0;
		}
		else
		{
			;
		}
	}
	else
	{
		Axle_TCS->lctcsu1HghMuWhlUnstbAftFlg = 0;
	}
	
	if (Axle_TCS->lctcsu1HghMuWhlUnstbAftFlg == 1)
	{
		Axle_TCS->lctcss16HghMuWhlUnstbAftCnt = Axle_TCS->lctcss16HghMuWhlUnstbAftCnt + 1;
		Axle_TCS->lctcss16HghMuWhlUnstbAftCnt = LCTCS_s16ILimitMaximum( Axle_TCS->lctcss16HghMuWhlUnstbAftCnt, L_U16_TIME_10MSLOOP_30S );
	}
	else
	{
		Axle_TCS->lctcss16HghMuWhlUnstbAftCnt = 0;
	}
	
}
	
void LDTCS_vDctHSSCnt4AdptBaseFF(struct TCS_AXLE_STRUCT *Axle_TCS)  /*Reduce HSS counter*/
{
	if ( Axle_TCS->lctcss16HghMuUstbRepeatCycle>=2) /*연속된 HSS 에서만 FF 제어량을 줄이기 위해 1 번 째 HSS 에는 감소시키지 않고 2번째 부터 적용*/
	{
		if( (Axle_TCS->lctcss16HghMuWhlUnstbAftCnt<=L_U8_TIME_10MSLOOP_1000MS)
			&&(Axle_TCS->lctcss16HghMuUstbRepeatCycleOld<Axle_TCS->lctcss16HghMuUstbRepeatCycle))
		{
			Axle_TCS->lctcss16HghMuUstbReptReduceCnt = Axle_TCS->lctcss16HghMuUstbReptReduceCnt + 1;
		}
		else
		{
			;
		}	
	}
	else
	{
		Axle_TCS->lctcss16HghMuUstbReptReduceCnt = 0;		
	}
	
	tcs_tempW0 = Axle_TCS->lctcss16HghMuWhlUnstbAftCnt % L_U8_TIME_10MSLOOP_1500MS ;
	if( (Axle_TCS->lctcss16HghMuWhlUnstbAftCnt>L_U8_TIME_10MSLOOP_1000MS) 
		&&( tcs_tempW0==1)&&(Axle_TCS->lctcss16HghMuUstbReptReduceCnt>=1) )
	{
		Axle_TCS->lctcss16HghMuUstbReptReduceCnt = Axle_TCS->lctcss16HghMuUstbReptReduceCnt - 1;
	}
	else
	{
		;
	}	
	Axle_TCS->lctcss16HghMuUstbReptReduceCnt = LCTCS_s16ILimitRange(Axle_TCS->lctcss16HghMuUstbReptReduceCnt, 0, L_U8_CNT_5);
}


void LDTCS_vDctAdpFFCtlByHghMuWhlUnst(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if	( 
		  (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuNotDcted) || 
		  ((Axle_TCS->lctcsu8StatWhlOnHghMu != TCSHighMuUnStable) && (Axle_TCS->lctcsu1FFAdaptByHghMuWhlUnstab==1) && (Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst >= S16TCSCpWorkTh4FFAdaptReset)) /* 너무 오래 안정된 상황을 유지 하면 reset */
		)
	{	 
		Axle_TCS->lctcss16AdaptedFFBrkTorq4Asym = 0;
		Axle_TCS->lctcsu1FFAdaptUpdateByHSS = 0;
		Axle_TCS->lctcsu1FFAdaptByHghMuWhlUnstab = 0;			
	}
	else if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
		{
			if ( (Axle_TCS->lctcsu1OK2AdaptFF==1) && (Axle_TCS->lctcss16MemBsTBrkTorq4Asym!=0) )
			{	/* lctcss16MemBsTBrkTorq4Asym 가 0이면 아직 lctcss16MemBsTBrkTorq4Asym가 update 되지 않았음을 의미하므로 이 경우에는 lctcss16AdaptedFFBrkTorq4Asym를 update 하지 않음 */
					Axle_TCS->lctcss16AdaptedFFBrkTorq4Asym = Axle_TCS->lctcss16MemBsTBrkTorq4Asym;
					
				Axle_TCS->lctcsu1FFAdaptUpdateByHSS = 1;				
				Axle_TCS->lctcsu1FFAdaptByHghMuWhlUnstab = 1;
			}
			else
			{
				Axle_TCS->lctcsu1FFAdaptUpdateByHSS = 0;
			}
		}
		else
		{
			Axle_TCS->lctcsu1FFAdaptUpdateByHSS = 0;
		}
	}

void LDTCS_vDctBrkTrqMaxLim4AsymSpCtl(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if 		(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuNotDcted)
	{
		Axle_TCS->lctcss16BrkTrqMax4Asym = 0;
		Axle_TCS->lctcsu1BrkTrqMax4AsymUpdated = 0;
		Axle_TCS->lctcsu1BrkTrqMaxLimited = 0;
	}
	else if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
	{
		if ( (Axle_TCS->lctcsu1OK2BrkTrqMaxLim==1) && (Axle_TCS->lctcss16MemBsTBrkTorq4Asym!=0) )
		{	/* lctcss16MemBsTBrkTorq4Asym 가 0이면 아직 lctcss16MemBsTBrkTorq4Asym가 update 되지 않았음을 의미하므로 이 경우에는 lctcss16AdaptedFFBrkTorq4Asym를 update 하지 않음 */
			Axle_TCS->lctcss16BrkTrqMax4Asym = Axle_TCS->lctcss16MemBsTBrkTorq4Asym;
			Axle_TCS->lctcsu1BrkTrqMax4AsymUpdated = 1;				
			Axle_TCS->lctcsu1BrkTrqMaxLimited = 1;
		}
		else
		{
			Axle_TCS->lctcsu1BrkTrqMax4AsymUpdated = 0;
		}
	}
	else
	{	/* Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuStable */
		Axle_TCS->lctcsu1BrkTrqMax4AsymUpdated = 0;
	}
}

void LDTCS_vCntBrkTrqMaxLimTime (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
	{
		Axle_TCS->lctcss16BrkTrqMaxLimTime = 0;
	}
	else if (Axle_TCS->lctcsu1BrkTrqMaxLimited == 1)
	{
		Axle_TCS->lctcss16BrkTrqMaxLimTime = Axle_TCS->lctcss16BrkTrqMaxLimTime + 1;
		Axle_TCS->lctcss16BrkTrqMaxLimTime = LCTCS_s16ILimitRange(Axle_TCS->lctcss16BrkTrqMaxLimTime, 0, L_U16_TIME_10MSLOOP_1MIN);
	}
	else
	{
		Axle_TCS->lctcss16BrkTrqMaxLimTime = 0;
	}
}

void LDTCS_vDctBrkTrqMaxLimInc (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcss16BrkTrqMaxLimTime >= S16TCSCpBrkTrqMaxIncStartTime)
	{
		if ((system_loop_counter%10) == 1)
		{	/* 100ms 마다 lctcss16BrkTrqMax4Asym 를 U8TCSCpBrkTrqMaxIncRate 씩 증가 : learning 이 잘못 되었을때 를 대비한 back up */
			Axle_TCS->lctcss16BrkTrqMax4Asym = Axle_TCS->lctcss16BrkTrqMax4Asym + U8TCSCpBrkTrqMaxIncRate;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

void LDTCS_vDctBrkTrqMaxLimReset (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if		(Axle_TCS->lctcss16BrkTrqMaxLimTime==0)
	{
		Axle_TCS->lctcss16VhSpdAtStrtBrkTrqMaxLim = 0;
		Axle_TCS->lctcss16VhSpd3sAftBrkTrqMaxLim = 0;
	}
	else if (Axle_TCS->lctcss16BrkTrqMaxLimTime==1)
	{	/* Brake torque maximum limitiation 시작 시의 차속 측정 */
		Axle_TCS->lctcss16VhSpdAtStrtBrkTrqMaxLim = lctcss16VehSpd4TCS;
	}
	else if (Axle_TCS->lctcss16BrkTrqMaxLimTime==L_U16_TIME_10MSLOOP_3S)
	{	/* Brake torque maximum limitiation 시작 후 3초 경과시의 차속 측정 */
		Axle_TCS->lctcss16VhSpd3sAftBrkTrqMaxLim = lctcss16VehSpd4TCS;
		if 		(Axle_TCS->lctcss16VhSpd3sAftBrkTrqMaxLim < Axle_TCS->lctcss16VhSpdAtStrtBrkTrqMaxLim)
		{
			Axle_TCS->lctcsu1BrkTrqMaxLimited = 0;
			Axle_TCS->lctcss16BrkTrqMax4Asym = 0;
		}
		else if (Axle_TCS->lctcss16VhSpd3sAftBrkTrqMaxLim < VREF_1_KPH)
		{
			Axle_TCS->lctcsu1BrkTrqMaxLimited = 0;
			Axle_TCS->lctcss16BrkTrqMax4Asym = 0;
		}
		else
		{
			;
		}		
	}
	else
	{
		;
	}
	/* lctcsu1BrkTrqMaxLimited reset 조건 추가 필요 사항 	*/
	/* 1. 차속이 감소하고 있을때							*/
	/* 2. 차가 뒤로 밀리고 있을때							*/
}

void LDTCS_vDctOverBrk (struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	tcs_tempW0 = W_L->wvref - W_L->vrad_crt;
    if(Wl_TCS->lctcsu1BTCSWhlAtv==1)
    {
        if(tcs_tempW0 >= VREF_1_KPH)
        {
            Wl_TCS->lctcsu8OverBrkCnt = Wl_TCS->lctcsu8OverBrkCnt + 1;
        }
        else
        {
            if(Wl_TCS->lctcsu8OverBrkCnt != 0)
            {
                Wl_TCS->lctcsu8OverBrkCnt = Wl_TCS->lctcsu8OverBrkCnt - 1;

            }
            else
            {
                Wl_TCS->lctcsu8OverBrkCnt=0;
            }
        }

        if(Wl_TCS->lctcsu8OverBrkCnt >= S8TCSCpOverBrkTh)
        {
            Wl_TCS->lctcsu1OverBrk=1;
        }
        else
        {
            ;
        }

        if(Wl_TCS->lctcsu8OverBrkCnt==0)
        {
            Wl_TCS->lctcsu1OverBrk=0;
        }
        else
        {
            ;
        }

    }
    else
    {
        Wl_TCS->lctcsu8OverBrkCnt = 0;
        Wl_TCS->lctcsu1OverBrk = 0;
    }
}

void LDTCS_vDctOverBrkInAxle (struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
    Axle_TCS->lctcsu1OverBrkAxleOld = Axle_TCS->lctcsu1OverBrkAxle;

    if((WL_LEFT ->lctcsu1OverBrk == 1)||(WL_RIGHT ->lctcsu1OverBrk == 1))
    {
        Axle_TCS->lctcsu1OverBrkAxle = 1;
    }
    else
    {
        Axle_TCS->lctcsu1OverBrkAxle = 0;
    }
}

#if ((__4WD_VARIANT_CODE==ENABLE) || (__4WD==1))  /*Temp VCA*/      /*TAG3*/
static void LDTCS_vDctBigSpinAWD(void)
{
	/* lctcsu1BigSpnAwdDctd 는 한번 감지 되면 엔진 제어 종료 전에는 계속 감지 유지 */
	if ( (ETCS_ON == 1) || (FTCS_ON == 1) )
	{
		#if __SPIN_DET_IMPROVE
			if (ldu1vehSpinDet==1)
		#else
			if (ACCEL_SPIN_FZ==1)
		#endif
		{
			lctcsu1BigSpnAwdDctd = 1;
		}
		else
		{
			;
		}
	}
	else
	{
		lctcsu1BigSpnAwdDctd = 0;
	}
}                                

void LDTCS_vDecideVrefCrtAidStart(void)
{
	/*=============================================================================*/
	/* Purpose : 4WD 차량에서 통상적인 BTCS 제어가 없을 경우, Spin이 감소하는 경향 */
	/*           을 보일때 Spin이 가장 작은 Wheel의 Brake Force를 증대하여 	       */
	/*           Big Spin 상황에서 Vref Correction 을 돕기 위함					   */
	/* Input Variable : */
	/* Output Variable :*/
	/* 2007. 2. 5 By Jongtak												       */
	/*=============================================================================*/

	VCA_ON_TCMF_OLD = VCA_ON_TCMF;
	
  #if (__4WD_VARIANT_CODE==ENABLE)
  	/* Variant Code 적용된 차량은 2H/2WD 모드에서 VCA 작동 금지 */
	if(lctcsu1DrvMode2Wheel==1)
	{
		VCA_ON_TCMF = 0;
		RL_TCS.VCA_TCMF_CTL = 0; RR_TCS.VCA_TCMF_CTL = 0;
		FL_TCS.VCA_TCMF_CTL = 0; FR_TCS.VCA_TCMF_CTL = 0;
		ltcss8WhlSpinDecCntTCMF = 0;
		VCA_BRAKE_CTRL_END_TCMF = 0;
		ltcss16MaxVrefInVCATCMF = 0;
		ltcsu8VCARunningCntTCMF = 0;
	}
	else
	{
		;
	}
  #endif
	if (VCA_ON_TCMF == 0) 
	{	
		if ( (VCA_SUSTAIN_FLAG_TCMF == 0) && (ETCS_ON == 1) && (lctcsu1BTCSVehAtvOld == 0) &&(lctcsu1BTCSVehAtv == 0) && (YAW_CDC_WORK == 0) && (ESP_TCS_ON == 0) 
			&& (lctcss16VehSpd4TCS > (int16_t)U8_SPEED_TH_VCA) && (McrAbs(nor_alat) < LAT_0G1G) &&
			 ( ((acc_r >= U8_ACC_TH_VCA)&&(acc_r < U16_AX_RANGE_MAX)) || ((FA_TCS.ltcss16WhlSpinAvg >= U8_SPIN_TH_VCA)||(RA_TCS.ltcss16WhlSpinAvg >= U8_SPIN_TH_VCA))) 
			 &&(BTCS_Entry_Inhibition == 0) 
			 &&(lctcsu1BigSpnAwdDctd==1)
			 &&(ENG_STALL==0)
		     #if __RDCM_PROTECTION
		     && (ltu8RDCMStatus == 0) 
		     #endif
		   )
		{
			ltcss8WhlSpinDecCntTCMF = ltcss8WhlSpinDecCntTCMF + 1;
			ltcss8WhlSpinDecCntTCMF = (int8_t)LCTCS_s16ILimitMaximum( (int16_t)ltcss8WhlSpinDecCntTCMF, L_U8_TIME_10MSLOOP_700MS );
		}
		else
		{
			ltcss8WhlSpinDecCntTCMF = ltcss8WhlSpinDecCntTCMF - 1;
			ltcss8WhlSpinDecCntTCMF = (int8_t)LCTCS_s16ILimitMinimum( (int16_t)ltcss8WhlSpinDecCntTCMF, 0 );
		}
	
		if (ltcss8WhlSpinDecCntTCMF > L_U8_TIME_10MSLOOP_40MS)
		{
			ltcss8WhlSpinDecCntTCMF = 0;
			VCA_ON_TCMF = 1;
		}
		else
		{
			;
		}
	}
	else
	{
		ltcss8WhlSpinDecCntTCMF = 0;
	}
}


void LDTCS_vDecideVrefCrtAidCtrlWhl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *W_L,struct W_STRUCT *W_R)
{
	tcs_tempW0 = (int16_t)U8TCSCpRearCtlOfVCA;
	
	if ( ((tcs_tempW0==1)&&(Axle_TCS->ltcsu1RearAxle==1)) ||
		 ((tcs_tempW0==0)&&(Axle_TCS->ltcsu1RearAxle==0)) )
	{	 
		if	( (VCA_ON_TCMF_OLD == 0) &&(VCA_ON_TCMF == 1) ) 
		{	
			if 		(yaw_out > YAW_1DEG)
			{	
				WL_RIGHT->VCA_TCMF_CTL = 1;
			}
			else if (yaw_out < -YAW_1DEG)
			{
				WL_LEFT->VCA_TCMF_CTL = 1;
			}
			else
			{
				if ( (WL_LEFT->lctcsu1VCAPrevCtrlFlag==0) && (WL_RIGHT->lctcsu1VCAPrevCtrlFlag==0) ) /*제어 첫진입시 속도 빠른Wheel 제어*/
				{
					if (W_L->ltcss16MovingAveragedWhlSpd > W_R->ltcss16MovingAveragedWhlSpd)
					{
						WL_LEFT->VCA_TCMF_CTL = 1;	   
					}
					else
					{
						WL_RIGHT->VCA_TCMF_CTL = 1;
					}
				}
				else    /*첫 VCA 제어 이후 좌우 교번VCA제어*/
				{   
					if(WL_LEFT->lctcsu1VCAPrevCtrlFlag ==1)  /*VCA 제어 Wheel 최종 결정 */
					{
						WL_RIGHT->VCA_TCMF_CTL = 1;   
					}
					else if(WL_RIGHT->lctcsu1VCAPrevCtrlFlag ==1)
					{
						WL_LEFT->VCA_TCMF_CTL = 1;   
					}
					else
					{
						;
					}
				}
			}
		}
		else
		{
			;
		}
	}
	else
	{
		WL_RIGHT->VCA_TCMF_CTL = 0;
		WL_LEFT->VCA_TCMF_CTL = 0;
	}
}

void LDTCS_vDecideVCAOffByYawRate(struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT) 
{	
		if((WL_LEFT->VCA_TCMF_CTL == 1)&&(yaw_out > YAW_1DEG))
		{
			WL_LEFT->ltcsu1VCAOffByYawRate=1;
		}
		else if ((WL_RIGHT->VCA_TCMF_CTL == 1)&&(yaw_out < -YAW_1DEG))
		{	 
			WL_RIGHT->ltcsu1VCAOffByYawRate = 1; 
		}
		else
		{
			WL_LEFT->ltcsu1VCAOffByYawRate = 0;
			WL_RIGHT->ltcsu1VCAOffByYawRate = 0;
		}
}

void LDTCS_vDecideVCAOffBySpdDiff4Whl(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT) 
{
	if(VCA_ON_TCMF==1)
	{
		Axle_TCS->ltcss16MonTime4VCASpnCtlStrt = LCESP_s16IInter2Point(Axle_TCS->ltcss16ChngRateWhlSpinDiffNF, (int16_t)U8TCSCpChngRateWhlSpin_1 , (int16_t)U8TCSCpMonTime4AsymSpnCtlS_1,
		                                                                                                       (int16_t) U8TCSCpChngRateWhlSpin_2, (int16_t)U8TCSCpMonTime4AsymSpnCtlS_2);		
		if(Axle_TCS->ltcss16WhlSpinDiffNF > WL_LEFT->lctcss16AsymSpnCtlTh)
		{
			Axle_TCS->ltcss16Cnt4VCASpnCtlStrt = Axle_TCS->ltcss16Cnt4VCASpnCtlStrt+1;
			
      		if(Axle_TCS->ltcss16Cnt4VCASpnCtlStrt > Axle_TCS->ltcss16MonTime4VCASpnCtlStrt)
      		{
      			WL_LEFT->ltcsu1VCAOffBySpdDiffWhl=1; 
			}
			else
			{
      			;
      		}
		}
      	else if ((Axle_TCS->ltcss16WhlSpinDiffNF) < (-WL_RIGHT->lctcss16AsymSpnCtlTh))
      	{
			Axle_TCS->ltcss16Cnt4VCASpnCtlStrt = Axle_TCS->ltcss16Cnt4VCASpnCtlStrt+1;
      		
      		if(Axle_TCS->ltcss16Cnt4VCASpnCtlStrt > Axle_TCS->ltcss16MonTime4VCASpnCtlStrt)
      		{
      			WL_RIGHT->ltcsu1VCAOffBySpdDiffWhl=1; 
			}
			else
			{
      			;
      		}
      	}
      	else
		{
      		Axle_TCS->ltcss16Cnt4VCASpnCtlStrt=0; 
      	}
	}
	else
	{
		WL_LEFT->ltcsu1VCAOffBySpdDiffWhl=0;
		WL_RIGHT->ltcsu1VCAOffBySpdDiffWhl=0;
		Axle_TCS->ltcss16MonTime4VCASpnCtlStrt = 0;
		Axle_TCS->ltcss16Cnt4VCASpnCtlStrt = 0;
	}
}

void LDTCS_vDecideVCAOffBySpdDiff(void)
{ 
	if ( ( FL_TCS.ltcsu1VCAOffBySpdDiffWhl == 1) ||
		 ( FR_TCS.ltcsu1VCAOffBySpdDiffWhl == 1) ||
		 ( RL_TCS.ltcsu1VCAOffBySpdDiffWhl == 1) ||
		 ( RR_TCS.ltcsu1VCAOffBySpdDiffWhl == 1) ||
		 ( FL_TCS.ltcsu1VCAOffByYawRate==1) ||
		 ( FR_TCS.ltcsu1VCAOffByYawRate==1) ||
		 ( RL_TCS.ltcsu1VCAOffByYawRate==1) ||
		 ( RR_TCS.ltcsu1VCAOffByYawRate==1) )
	{
		ltcsu1VCAOffBySpdDiffFlg = 1;
	}
	else
	{
		ltcsu1VCAOffBySpdDiffFlg = 0;
	}
} 


void LDTCS_vDctBTCPrevCtrlWhl4VCA(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT, struct W_STRUCT *W_L,struct W_STRUCT *W_R)
{
	tcs_tempW0 = (int16_t)U8TCSCpRearCtlOfVCA;
	
	if ( ((tcs_tempW0==1)&&(Axle_TCS->ltcsu1RearAxle==1)) ||
		 ((tcs_tempW0==0)&&(Axle_TCS->ltcsu1RearAxle==0)) )
	{	 
		if (ETCS_ON == 0)
		{
			WL_LEFT->lctcsu1VCAPrevCtrlFlag = 0;
			WL_RIGHT->lctcsu1VCAPrevCtrlFlag = 0;
		}
		else
		{
			if ( ( lctcsu1BTCSVehAtvRsgEdg == 1 ) ||(ltcsu1VCAOffBySpdDiffFlg == 1 ) )
			{
				if(((RA_TCS.lctcsu1BrkCtlActAxle==1)&&(Axle_TCS->ltcsu1RearAxle==1)) ||
					((FA_TCS.lctcsu1BrkCtlActAxle==1)&&(Axle_TCS->ltcsu1RearAxle==0)))
				{
					WL_LEFT->lctcsu1VCAPrevCtrlFlag = W_L->BTCS ;  
			    	WL_RIGHT->lctcsu1VCAPrevCtrlFlag = W_R->BTCS ; 
			    }
			    else
			    {
			    	;  
			    }
			}
			else  
			{
				;
			}
		
		}
	}
	else
	{
		;
	}
}

#if (__SPLIT_TYPE==0)		/*TAG4*/
void LDTCS_vDecideVCAMaxCtrlCirXsp(void)
{	
	if (((FL_TCS.VCA_TCMF_CTL==1) && (RR_TCS.VCA_TCMF_CTL==1))||((FR_TCS.VCA_TCMF_CTL==1) && (RL_TCS.VCA_TCMF_CTL==1))) /*temp*/
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
		SC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;			
	}
	else if ((FL_TCS.VCA_TCMF_CTL==0) && (FR_TCS.VCA_TCMF_CTL==1))
	{	
		PC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
		SC_TCS.lctcss16InitMaxVCATCCurnt = 0;
	}
	else if ((FL_TCS.VCA_TCMF_CTL==1)&&(FR_TCS.VCA_TCMF_CTL==0))
	{	
		PC_TCS.lctcss16InitMaxVCATCCurnt = 0;
		SC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
	}
	else if ((RL_TCS.VCA_TCMF_CTL==0)&&(RR_TCS.VCA_TCMF_CTL==1))
	{	
		PC_TCS.lctcss16InitMaxVCATCCurnt = 0;
		SC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
	}
	else if ((RL_TCS.VCA_TCMF_CTL==1)&&(RR_TCS.VCA_TCMF_CTL==0))
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
		SC_TCS.lctcss16InitMaxVCATCCurnt = 0;
	}
	else
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = 0;
		SC_TCS.lctcss16InitMaxVCATCCurnt = 0;
	}	
}
#endif              /*TAG4*/

#if (__SPLIT_TYPE!=0)		 /*TAG5*/	
void LDTCS_DecideVCAMaxCtrlCirFRsp(void)
{
	if	(((FL_TCS.VCA_TCMF_CTL==0) && (FR_TCS.VCA_TCMF_CTL==1))||((FL_TCS.VCA_TCMF_CTL==1) && (FR_TCS.VCA_TCMF_CTL==0)))
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
		SC_TCS.lctcss16InitMaxVCATCCurnt = 0;
	}
	else if (((RL_TCS.VCA_TCMF_CTL==0) && (RR_TCS.VCA_TCMF_CTL==1))||((RL_TCS.VCA_TCMF_CTL==1) && (RR_TCS.VCA_TCMF_CTL==0)))
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = 0;
		SC_TCS.lctcss16InitMaxVCATCCurnt = S16TCSCpInitMaxVCATCCurnt;
	}
	else
	{
		PC_TCS.lctcss16InitMaxVCATCCurnt = 0;
		SC_TCS.lctcss16InitMaxVCATCCurnt = 0;
	}
}
#endif                       /*TAG5*/

void LDTCS_vSustainFlagAfterVCA(void)
{
	/*=============================================================================*/
	/* Purpose : VCA_ON_TCMF이 Clear 된 후 일정시간동안 Set 상태가 지속되는 Flag 생성   */
	/* Input Variable : VCA_ON_TCMF												   	   */
	/* Output Variable : ltcsu8CntAfterVCATCMF, VCA_SUSTAIN_FLAG_TCMF	                   */
	/* 2008. 2. 20 By Jongtak												       */
	/*=============================================================================*/		
	if(VCA_ON_TCMF==1)
	{
		ltcsu8CntAfterVCATCMF = U8_TCS_VCA_REENGAGE_ALLOW_TIME;
		VCA_SUSTAIN_FLAG_TCMF = 1;
	}
	else
	{
		if (ltcsu8CntAfterVCATCMF>0)
		{
			ltcsu8CntAfterVCATCMF--;
		}
		else
		{
			VCA_SUSTAIN_FLAG_TCMF = 0;
			ltcsu8CntAfterVCATCMF = 0;
		}
	}	
}

void LDTCS_vDecideVrefCrtAidEnd(void)
{
	if( VCA_ON_TCMF == 1 )
	{
		ltcsu8VCARunningCntTCMF++;
		ltcsu8VCARunningCntTCMF = (uint8_t)LCTCS_s16ILimitMaximum( (int16_t)ltcsu8VCARunningCntTCMF, (int16_t)L_U8_TIME_10MSLOOP_1750MS);
		ltcss16MaxVrefInVCATCMF = LCTCS_s16IFindMaximum(vref5, ltcss16MaxVrefInVCATCMF);
		
		tcs_tempW1 = ltcss16MaxVrefInVCATCMF - lctcss16VehSpd4TCS;
		if ( (ltcsu8VCARunningCntTCMF > (uint8_t)U8_MAX_VCA_SUSTAIN_SCAN) || 
		 	 (YAW_CDC_WORK == 1) || (tcs_tempW1 > (int16_t)U8_VREF_DECREASE_LIMIT_OF_VCA) ||
		     (det_alatc >= LAT_0G2G) ||
		     (ltcsu1VCAOffBySpdDiffFlg == 1) ||   /*Counter 조건 없이 Spin만 보기때문에 Spin에 의한 조기해제 후 BTCS제어 진입 가능*/
			 (ETCS_ON == 0) 
		   )
		{
			VCA_BRAKE_CTRL_END_TCMF = 1;
		}
		else
		{
			VCA_BRAKE_CTRL_END_TCMF = 0;	/* Do Nothing */
		}
		
	}
	else
	{
		ltcsu8VCARunningCntTCMF = 0;
		ltcss16MaxVrefInVCATCMF = 0;
		VCA_BRAKE_CTRL_END_TCMF = 0;
	}
}

#endif     /*Temp VCA*/               /*TAG3*/

void LDTCS_vVIBDetectFlag(struct TCS_WL_STRUCT *Wl_TCS)       /*Vibration control*/                                              
{	
	/*=============================================================================*/
	/* Purpose : Amplitude및Frequency 기반 Vibration Detection                     */
	/* Input Variable : lctcsu8VIBFlagCycleCnt, lctcsu1VIBAmplitudeUpdCnt          */
	/*                  lctcsu16VIBAmplitude, lctcsu8VIBFreqency                   */
	/*                  U8_TCS_VIB_AMPLITUDE_ENTER, U8_TCS_VIB_FREQUENCY_ENTER     */  
	/*                  U8_TCS_VIB_AMPLITUDE_OUT, U8_TCS_VIB_FREQUENCY_OUT         */  
	/*                  U8_TCS_VIB_FLAG_ENTER_CYCLE, U8_TCS_VIB_FLAG_OUT_CYCLE     */
	/* Output Variable : lctcsu1VIBFlag                                            */
	/* 2009. 10. 23 By ByoungJin Park            							       */
	/*=============================================================================*/
    /*TCS 작동 후*/ 
    /*vibration 감지판단 설정*/
    /*vibration 감지판단 설정 Amplitude 이상값 & Frequency 이상값*/
    tcs_tempW0 = LCTCS_s16ILimitRange((int16_t)U8_TCS_VIB_FREQUENCY_THR, 1, L_U8_TIME_10MSLOOP_250MS);
	
   	
   	if ( (Wl_TCS->lctcsu16VIBAmplitude_tcmf >= (uint16_t)U8_TCS_VIB_AMPLITUDE_ENTER)
   	&& (Wl_TCS->lctcsu8VIBFreqency_tcmf >= U8_TCS_VIB_FREQUENCY_ENTER) )
   	{
   		if ( (Wl_TCS->lctcsu16VIBvradErrvref_tcmf) <= (uint16_t)U8_TCS_VIB_REFERENCE_VREF_ENTER )
   		{
   			if ((Wl_TCS->lctcsu1VIBFallingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 1))
   			{
   				Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf = Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf +1;
   				Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf = (uint8_t)LCTCS_u16ILimitMaximum((uint16_t)Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf, L_U8_TIME_10MSLOOP_1750MS);
   				if ( (Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf >= U8_TCS_VIB_FLAG_ENTER_CYCLE) )
   	        	{
   	        		Wl_TCS->lctcsu1VIBFlag_tcmf = 1;
   	        		
   	        		Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf = 0;
   	        	}
   	        	else
   	        	{
   	        		;
	            }
   			}
	    	else
   			{
   				;
   			}
   		}
   		else
   		{
   			;
   		}
   	}
   	else
   	{
   		;
   	}
   	if ( (Wl_TCS->lctcsu16VIBAmplitude_tcmf <= (uint16_t)U8_TCS_VIB_AMPLITUDE_OUT) 
   	|| (Wl_TCS->lctcsu8VIBFreqency_tcmf <= U8_TCS_VIB_FREQUENCY_OUT)
   	|| (Wl_TCS->lctcsu8VIBFreqency_tcmf > tcs_tempW0)
   	|| (Wl_TCS->lctcsu16VIBvradErrvref_tcmf >= (uint16_t)U8_TCS_VIB_REFERENCE_VREF_OUT) 
   	|| (ESP_TCS_ON == 1)
   	|| (lctcsu1RoughRoad == 1) )
   	{
   		if ((Wl_TCS->lctcsu1VIBRisingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBRising_tcmf == 1)) 
   		{
   			Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf = Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf +1;
   			if (Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf >= U8_TCS_VIB_FLAG_OUT_CYCLE)
   			{
   				Wl_TCS->lctcsu1VIBFlag_tcmf = 0;
   				Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf = 0;
   		      	Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf = 0;
   		    }
   		    else
   		    {
   		    	;
   		    }
   		}
   		else
   		{
   			;
   		}	
   	}
   	else
   	{
   		;
   	}
   	        
}                                     /*Vibration control*/

void LDTCS_vVIBDecideBrakeModeVib(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	tcs_tempW0 = LCTCS_s16ILimitRange((int16_t)U8_TCS_VIB_TIMESHIFT_SCAN_TCMF, 1, L_U8_TIME_10MSLOOP_550MS);
	
   	if ( Wl_TCS->lctcsu1VIBFlag_tcmf == 1 )
   	{
   		/* Frequency 변화에 따른 Hold 구간 적용 시점 계산 */
   	    if ( (Wl_TCS->lctcsu1VIBRisingold_tcmf == 0) && ( Wl_TCS->lctcsu1VIBRising_tcmf == 1) )
   	    {
   	    	if ( (Wl_TCS->lctcsu8VIBFreqencyold_tcmf == Wl_TCS->lctcsu8VIBFreqency_tcmf) )
   	    	{
   	    		Wl_TCS->lctcss8VIBTimeShift_tcmf = (int8_t)tcs_tempW0;
   	    	}
   	        else if ( (Wl_TCS->lctcsu8VIBFreqencyold_tcmf < Wl_TCS->lctcsu8VIBFreqency_tcmf) )
   	        {
   	        	/*Park Byoung Jin Modify 2010.02.10 in the SWD*/
   	        	Wl_TCS->lctcss8VIBTimeShift_tcmf = (int8_t)(tcs_tempW0 - (Wl_TCS->lctcsu8VIBFreqency_tcmf - Wl_TCS->lctcsu8VIBFreqencyold_tcmf));
   	        }
   	        else if ( (Wl_TCS->lctcsu8VIBFreqencyold_tcmf > Wl_TCS->lctcsu8VIBFreqency_tcmf) )
   	        {
   	        	/*Park Byoung Jin Modify 2010.02.10 in the SWD*/
   	        	Wl_TCS->lctcss8VIBTimeShift_tcmf = (int8_t)(tcs_tempW0 + (Wl_TCS->lctcsu8VIBFreqencyold_tcmf - Wl_TCS->lctcsu8VIBFreqency_tcmf));
   	        }
   	        else
   	        {
   	        	;
   	        }
   	    }
   	    else
   	    {
   	    	;
   	    }
   	    Wl_TCS->lctcss8VIBTimeShift_tcmf = (int8_t)LCTCS_s16ILimitRange( (int16_t)Wl_TCS->lctcss8VIBTimeShift_tcmf,0,((int16_t)Wl_TCS->lctcsu8VIBCycleTime_tcmf/2) );        
   	            
   	    if ( (Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf >= ((Wl_TCS->lctcsu8VIBCycleTime_tcmf/2) - Wl_TCS->lctcss8VIBTimeShift_tcmf)) && 
   	    	 (Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf <  (Wl_TCS->lctcsu8VIBCycleTime_tcmf - Wl_TCS->lctcss8VIBTimeShift_tcmf) )  )
   	    {
			Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf = Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf+1;
   	    	if ( Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf == 1)
   	    	{
   	    		Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf = Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf+1;
   	    		if (W_L->BTCS == 1)
   	    		{
   	    			Wl_TCS->lctcsu1VIBBrkScanReset_tcmf = 1;
   	    		}
   	    		else
   	    		{
   	    			;
   	    		}
   	    	}
   	    	else
   	    	{
   	    		Wl_TCS->lctcsu1VIBBrkScanReset_tcmf = 0;
   	    	}
			    Wl_TCS->lctcsu8VIBBrakeMode_tcmf = 1;
			    Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf = (uint8_t)LCTCS_u16ILimitMaximum((uint16_t)Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf, L_U8_TIME_10MSLOOP_1750MS);			
			    Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf = (uint8_t)LCTCS_u16ILimitMaximum((uint16_t)Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf, L_U8_TIME_10MSLOOP_1750MS);			
   	    }
   	    else
   	    {
   	    	Wl_TCS->lctcsu8VIBBrakeMode_tcmf = 0;
   	    	Wl_TCS->lctcsu8VIBBrakeCtlCheck_tcmf = 0;
   	    }
   	    
   	    if (Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf >= 1)
   	    {
   	    	Wl_TCS->lctcsu8VIBBrkCtlPeriod_tcmf = Wl_TCS->lctcsu8VIBBrkCtlPeriodCnt_tcmf -1;      
					
					tcs_tempW0 = LCTCS_s16ILimitMinimum((int16_t)U8_TCS_VIB_BRK_CTL_PERIOD, 1); 
					if((Wl_TCS->lctcsu8VIBBrkCtlPeriod_tcmf % tcs_tempW0)==0)
   	    	{
   	    		;
   	    	}
   	    	else
   	    	{
   	    		Wl_TCS->lctcsu8VIBBrakeMode_tcmf=0;
   	    	}		        
   		}
   		else
   		{
   			;
   		}		        
   	}   	
   	else
   	{
   		;
   	}
}

void LDTCS_vVIBDetectFlag4Engine(void)
{
	if((FL_TCS.lctcsu1VIBFlag_tcmf==1)||(FR_TCS.lctcsu1VIBFlag_tcmf==1)||(RL_TCS.lctcsu1VIBFlag_tcmf==1)||(RR_TCS.lctcsu1VIBFlag_tcmf==1))

	{
		lctcsu1DetectVibration = 1;
	}
	else
	{
		lctcsu1DetectVibration = 0;
	}
}

void LDTCS_vDctLowPres4IniTCVCtlMode(struct TCS_CIRCUIT_STRUCT *CIRCUIT, struct W_STRUCT *WL_F, struct W_STRUCT *WL_S)
{		
    if((WL_F->s16_Estimated_Active_Press<=U8TCSCpLowPresTh4IniTCVCtl)
  	&&(WL_S->s16_Estimated_Active_Press<=U8TCSCpLowPresTh4IniTCVCtl))
   	{	
   		CIRCUIT->lctcsu1EstPresLowFlag =1;
   	}
   	else
   	{
   		CIRCUIT->lctcsu1EstPresLowFlag =0;
   	}
}

void LDTCS_vDctIniTCVCtlMode(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
{
    CIRCUIT->lctcsu1IniTCVCtlModeOld = CIRCUIT->lctcsu1IniTCVCtlMode;
    if ((CIRCUIT->lctcsu1BrkCtlActCirRsgEdg==1)&&(CIRCUIT->lctcsu1EstPresLowFlag==1))
    {
		CIRCUIT->lctcsu1IniTCVCtlMode = 1;
    }
    else
    {
        if (CIRCUIT->lctcsu1IniTCVCtlMode==1)
        {
            CIRCUIT->lctcss8IniTCVCtlModeCnt = CIRCUIT->lctcss8IniTCVCtlModeCnt + 1;
            
            lctcss16IniTCVCtlModeTime = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)U8TCSCpIniTCVCtlModeTime,
                                                                                  100, (int16_t)U8TCSCpIniTCVCtlModeTimeInTurn);
			if ((FA_TCS.lctcsu1HuntingDcted==1)||(RA_TCS.lctcsu1HuntingDcted==1))
            {
            	lctcss16IniTCVCtlModeTime = U8TCSCpIniTCVCtlModeTimeHunt;
            }
            else if (lctcsu1DctSplitHillFlg==1)
        	{	
        		lctcss16IniTCVCtlModeTime = U8TCSCpIniTCVCtlModeTimeHill;
        	}
            else
            {
				;
			}
            if (CIRCUIT->lctcss8IniTCVCtlModeCnt >= lctcss16IniTCVCtlModeTime)
            {
                CIRCUIT->lctcsu1IniTCVCtlMode = 0;
                CIRCUIT->lctcss8IniTCVCtlModeCnt = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            CIRCUIT->lctcsu1IniTCVCtlMode = 0;
            CIRCUIT->lctcss8IniTCVCtlModeCnt = 0;
        }
    }

    if ( (CIRCUIT->lctcsu1IniTCVCtlModeOld==1) && (CIRCUIT->lctcsu1IniTCVCtlMode==0) )
    {
        CIRCUIT->lctcsu1IniTCVCtlModeFalEdg = 1;
    }
    else
    {
        CIRCUIT->lctcsu1IniTCVCtlModeFalEdg = 0;
    }
}

void LDTCS_vDctIniMotorCtlMode (void)
{
    lctcsu1IniMtrCtlModeOld = lctcsu1IniMtrCtlMode;

    if((lctcsu1BTCSVehAtvRsgEdg==1)&&(fu16CalVoltIntMOTOR<=Motor_0_25_Vol ))
    {
        lctcsu1IniMtrCtlMode=1;
    }
    else
    {
        ;
    }

    if(lctcsu1IniMtrCtlMode==1)
    {
		lctcss8IniMtrCtlModeCnt=lctcss8IniMtrCtlModeCnt+1;

        lctcss16IniMtrCtlModeTime = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)U8TCSCpIniMtrCtlModeTime,
                                                                              100, (int16_t)U8TCSCpIniMtrCtlModeTimeInTurn);
        if ((FA_TCS.lctcsu1HuntingDcted==1)||(RA_TCS.lctcsu1HuntingDcted==1))
        {
            lctcss16IniMtrCtlModeTime = U8TCSCpIniTCVCtlModeTimeHunt;
        }
        else if (lctcsu1DctSplitHillFlg==1)
        {
        	lctcss16IniMtrCtlModeTime = U8TCSCpIniTCVCtlModeTimeHill;
        }
        else
        {
			;
		}

        if (lctcss8IniMtrCtlModeCnt >= lctcss16IniMtrCtlModeTime)
        {
            lctcsu1IniMtrCtlMode=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        lctcss8IniMtrCtlModeCnt=0;
    }

    if((lctcsu1IniMtrCtlModeOld==1)&&(lctcsu1IniMtrCtlMode==0))
    {
        lctcsu1IniMtrCtlModeFalEdg=1;
    }
    else
    {
        lctcsu1IniMtrCtlModeFalEdg=0;
    }
}

void LDTCS_vDctIIntCtlAct(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->ltcss8IIntActOld = Axle_TCS->ltcss8IIntAct;
	
    if((Axle_TCS->lctcsu1BrkCtlActAxle == 1)&&(Axle_TCS->ltcsu1ModOfAsymSpnCtl == S16TCSAsymSpinBwdCtlMode))
    {
    	if (Axle_TCS->ltcss8IIntAct == 0)
    	{
        	tcs_tempW7 = McrAbs(Axle_TCS->ltcss16ChngRateWhlSpinDiff);
        	if ((tcs_tempW7) < ((int16_t)U8TCSCpICtlActWhlChgRateTh))
        	{
            	Axle_TCS->ltcss16SStateCnt = Axle_TCS->ltcss16SStateCnt + 1;
            	Axle_TCS->ltcss16SStateCnt = LCTCS_s16ILimitMaximum((int16_t)Axle_TCS->ltcss16SStateCnt, (int16_t)U8TCSCpICtlActCntTh);
        	}
        	else
        	{
            	Axle_TCS->ltcss16SStateCnt = Axle_TCS->ltcss16SStateCnt - 3;
            	Axle_TCS->ltcss16SStateCnt = LCTCS_s16ILimitMinimum((int16_t)Axle_TCS->ltcss16SStateCnt, 0);
        	}
        }
        else 
        {
        	;
        }

        if (Axle_TCS->ltcss16SStateCnt >= U8TCSCpICtlActCntTh)
        {
            Axle_TCS->ltcss8IIntAct = 1;
            Axle_TCS->ltcss16SStateCnt = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
        Axle_TCS->ltcss16SStateCnt = 0;
        Axle_TCS->ltcss8IIntAct = 0;
    }
}

void LDTCS_vDctIintReset4FFAdapt (struct TCS_AXLE_STRUCT *Axle_TCS)
{
    
    if( (Axle_TCS->ltcss8IIntAct == 1 ) && 
    	((Axle_TCS->lctcsu8StatWhlOnHghMuOld != TCSHighMuUnStable)&&(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)) 
      )
    {
 		Axle_TCS->ltcss8IIntAct = 0;
 		Axle_TCS->ltcss16SStateCnt = 0;
 	}
 	else
 	{
 		;
 	}
}

void LDTCS_vDctAdpFFCtlByIIntReset(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if( (Axle_TCS->lctcsu1BrkCtlActAxle == 1 )
		  &&(( AUTO_TM == 0) || (( AUTO_TM == 1)&&(AT_gear_pos_old == gear_pos)) )
		)
	{
		if( ( Axle_TCS->ltcss8IIntActOld == 1) && ( Axle_TCS->ltcss8IIntAct == 0 ) )
		{
				Axle_TCS->lctcsu1FFAdaptByIIntReset = 1;
				Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset = Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset + Axle_TCS->lctcss16IeffOnBsTBrkTorq4Asym;
			}
		else
			{
			if( (Axle_TCS->lctcsu1FFAdaptByIIntReset == 1)
				&&((Axle_TCS->lctcsu8StatWhlOnHghMuOld != TCSHighMuUnStable)&&(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable))) 
			{ /*I adaptation량이 과도해 high side 추가 유발할 경우 감소량 설정*/
				Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset = (Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset/10)*6;
		}
		else
		{
			;
		}
	}
	}
	else
	{
		Axle_TCS->lctcsu1FFAdaptByIIntReset = 0;
		Axle_TCS->lctcss16AdptFFBrkTorq4IIntReset = 0;
	}
}


void LDTCS_vDctFlgOfAfterIIntAct(struct TCS_AXLE_STRUCT *Axle_TCS)  /*사용여부 고려 필*/
{
	if (Axle_TCS->lctcsu1BrkCtlActAxle == 1 )
	{
		if( ( Axle_TCS->ltcss8IIntActOld == 1) && ( Axle_TCS->ltcss8IIntAct == 0 ))
		{
			Axle_TCS->lctcsu1FlgOfAfterIIntAct = 1;
		}
		else
		{
			if( (Axle_TCS->lctcsu1FlgOfAfterIIntAct == 1) && ( Axle_TCS->ltcss8IIntAct == 0 ) ) /*after IIntAct clear condition*/
			{
				Axle_TCS->lctcsu16CntofAfterIIntAct = Axle_TCS->lctcsu16CntofAfterIIntAct + 1;
				Axle_TCS->lctcsu16CntofAfterIIntAct = LCTCS_s16ILimitMaximum(Axle_TCS->lctcsu16CntofAfterIIntAct, (int16_t)U8TCSCpAfterIIntActScan);
				
				if (Axle_TCS->lctcsu16CntofAfterIIntAct >= U8TCSCpAfterIIntActScan)
				{   
					Axle_TCS->lctcsu1FlgOfAfterIIntAct = 0;
				}
				else
				{
					;
				}
			}
			else
			{
				Axle_TCS->lctcsu1FlgOfAfterIIntAct = 0;
				Axle_TCS->lctcsu16CntofAfterIIntAct = 0;
			}			
		}
	}
	else
	{
		Axle_TCS->lctcsu1FlgOfAfterIIntAct = 0;
		Axle_TCS->lctcsu16CntofAfterIIntAct = 0;
	}
}

void LDTCS_vDctTCSSplit(void)
{
    if (lctcsu1BTCSVehAtv==1)
    {
        if((FA_TCS.lctcsu1SymSpinDctFlg==1)||(RA_TCS.lctcsu1SymSpinDctFlg==1))
        {
            TCS_SPLIT=0;
        }
        else
        {
            TCS_SPLIT=1;
        }
    }
    else
    {
        TCS_SPLIT=0;
    }
}

void LDTCS_vDctAxleCondition4ETCS(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
	if((WL_L->lctcss16SpinRaw > (int16_t)VREF_4_25_KPH) && (WL_R->lctcss16SpinRaw> (int16_t)VREF_4_25_KPH))
	{
		if( (WL_L->s_diff> (int16_t)VREF_0_KPH) && (WL_R->s_diff >(int16_t)VREF_0_KPH) )
		{
			Axle_TCS->lctcsu1BTCTwoOff = 1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	
	if((WL_L->lctcss16SpinRaw<(int16_t)VREF_2_KPH)||(WL_R->lctcss16SpinRaw<(int16_t)VREF_2_KPH))
    {
        Axle_TCS->lctcsu1BTCTwoOff = 0;
    }
    else
    {
        ;
    }
	
}

void LDTCS_vDctSpinCondition4ETCS(void)
{
	if ( (FA_TCS.lctcsu1BTCTwoOff == 1) || (RA_TCS.lctcsu1BTCTwoOff == 1))
	{
		BTCS_TWO_OFF = 1;
	}
	else
	{
		BTCS_TWO_OFF = 0;
	}
}

void LDTCS_vDctBrkCtlFadeOutMode(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if ((Axle_TCS->lctcsu1SymSpinDctFlg==0)&&(Axle_TCS->lctcsu1BrkCtlActAxle==1)&&(McrAbs(Axle_TCS->ltcss16WhlSpinDiff) < (int16_t)U8TCSCpFadeOutSpdDiffThr))
	{
		Axle_TCS->S16TCSFadeOutDctCnt = Axle_TCS->S16TCSFadeOutDctCnt + 1;
	}
	else
	{
		Axle_TCS->S16TCSFadeOutDctCnt = Axle_TCS->S16TCSFadeOutDctCnt - 1;
	}
		
	Axle_TCS->S16TCSFadeOutDctCnt = LCTCS_s16ILimitRange(Axle_TCS->S16TCSFadeOutDctCnt, 0, (int16_t)U8TCSCpFadeOutDctScan);
	
	
	if ( Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 0 ) /*Asymmetric Fade Out Mode 진입 Condition*/
	{
		if (Axle_TCS->S16TCSFadeOutDctCnt >= U8TCSCpFadeOutDctScan)
        {
            Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag = 1;
            Axle_TCS->lctcss16MemBsTBrkTorq4FadOut = Axle_TCS->lctcss16BsTBrkTorq4Asym ;
        }
        else
        {
            ;
        }  
	}
	else  /*Asymmetric Fade Out Mode 해제 Condition*/    
	{
		tcs_tempW0 = (int16_t)U8TCSCpFadeOutDctScan - (int16_t)U8TCSCpFadeOutExitScan ;
		tcs_tempW0 = LCTCS_s16ILimitMinimum(tcs_tempW0, 0);
		
		if ( (Axle_TCS->S16TCSFadeOutDctCnt <= tcs_tempW0) 
			|| (Axle_TCS->lctcsu1BrkCtlActAxle == 0) )
        {
            Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag = 0;
            Axle_TCS->lctcss16MemBsTBrkTorq4FadOut = 0;
        }
		else
		{
			;
		}  
		
	}
}

void LDTCS_vDctBrkCtlFadeOutCtrlCnt(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if ( Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1 )
	{
		Axle_TCS->lctcss16FadeOutCtrlCnt = Axle_TCS->lctcss16FadeOutCtrlCnt + 1;
		Axle_TCS->lctcss16FadeOutCtrlCnt = LCTCS_s16ILimitMaximum(Axle_TCS->lctcss16FadeOutCtrlCnt, (int16_t)L_U16_TIME_10MSLOOP_10S);
	}
	else
	{
		Axle_TCS->lctcss16FadeOutCtrlCnt = 0;
	}
}

void LDTCS_vDctCtlWLSpdCros4FadOutCtl(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)
{
	if ( Axle_TCS->lctcsu1BrkCtlFadeOutModeFlag == 1 )
	{
		if ( (WL_LEFT->BTCS == 1) && (WL_RIGHT->BTCS == 0) )  /*Left Wheel 제어시 Wheel 역전*/
		{
			if ( (Axle_TCS->ltcss16WhlSpinDiffNF) <= (- S16TCSCpWLSpdCrosTh4QuickFadOut) )
			{
				Axle_TCS->lctcsu1CtrlWLSpdCross = 1;			
			}
			else  
			{
				;
			}
		}
		else if ( (WL_LEFT->BTCS == 0) && (WL_RIGHT->BTCS == 1) ) /*Righe Wheel 제어시 Wheel  역전*/
		{
			if ( (Axle_TCS->ltcss16WhlSpinDiffNF) >= (S16TCSCpWLSpdCrosTh4QuickFadOut) )
			{
				Axle_TCS->lctcsu1CtrlWLSpdCross = 1;
			}
			else
			{
				;
			}
		}
		else
		{
			Axle_TCS->lctcsu1CtrlWLSpdCross = 0;
		}
	}
	else
	{
		Axle_TCS->lctcsu1CtrlWLSpdCross = 0;
	}
}


//void LDTCS_vEstCirPresByTcCurnt(struct TCS_CIRCUIT_STRUCT *CIRCUIT)
//{
//    if (CIRCUIT->lctcsu1BrkCtlActCir == 1)
//    {
//        if ( ((CIRCUIT->lctcsu1IniTCVCtlMode==0) && (CIRCUIT->lctcsu1IniTCVCtlModeFalEdg==0)&&(VCA_ON_TCMF==0))
//        	  ||((ltcsu8VCARunningCntTCMF>=U8TCSCpIniVCACtlModeTime) && (VCA_ON_TCMF==1 )) )
//        {
//            CIRCUIT->lctcss16TCVCurrentFlt = LCESP_s16Lpf1Int(CIRCUIT->lctcss16TCVCurrent, CIRCUIT->lctcss16TCVCurrentFlt, U8TCSCpCurntFiltGain);
//            
//            if(CIRCUIT->lctcss16TCVCurrentFlt <= TCSMinimumCurrentLevel) /*TC Current가 가질 수 있는 최소값인 경우 */
//            {
//            	CIRCUIT->lctcss16EstCirPres = 0;
//            }
//            else
//            {
//            	CIRCUIT->lctcss16EstCirPres = LCTCS_vConvCurntOnTC2Pres(CIRCUIT->lctcss16TCVCurrentFlt);
//            }
//        }
//        else
//        {
//            CIRCUIT->lctcss16TCVCurrentFlt = 0;
//            CIRCUIT->lctcss16EstCirPres = 0;
//        }
//    }
//    else
//    {
//        CIRCUIT->lctcss16TCVCurrentFlt = 0;
//        CIRCUIT->lctcss16EstCirPres = 0;
//    }
//}

void  LDTCS_vEstWhlBrkTrqAndPresFRsp(void)
{
    tcs_tempW0 = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
    tcs_tempW1 = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
    if      ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;

    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = tcs_tempW1;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
}

void  LDTCS_vEstWhlBrkTrqAndPresXsp(void)
{
    if      ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        FL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
        }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
        {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
        {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (SC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = SC_TCS.lctcss16EstCirPres;
        }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresFrt;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = (PC_TCS.lctcss16EstCirPres/10) * U8TCSCpSclFctTrq2PresRer;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RL_TCS.lctcss16EstWhlPre = PC_TCS.lctcss16EstCirPres;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
    else
    {
        FA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        RA_TCS.lctcss16EstBrkTorqOnLowMuWhl = 0;
        FL_TCS.lctcss16EstWhlPre = 0;
        FR_TCS.lctcss16EstWhlPre = 0;
        RL_TCS.lctcss16EstWhlPre = 0;
        RR_TCS.lctcss16EstWhlPre = 0;
    }
}

void LDTCS_vDctSplitMuAxle(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
	if((WL_LEFT->lctcsu1BTCSWhlAtv==1) || (WL_RIGHT->lctcsu1BTCSWhlAtv==1))
	{    
		Axle_TCS->lctcss16BsBrkTorqTh4SplitDct = (((U8TCSCpAsmdMuDiff4SplitMuDct * Axle_TCS->lctcss16MassOfOneCorn) /100)*(Axle_TCS->lctcss16WhlRolR/10))/10;/*resolution  Mu 1/100 WhlRolR 1/1000 중력가속도 10*/	  	
		Axle_TCS->lctcss16BsBrkTorqTh4SplitExit = ((((U8TCSCpAsmdMuDiff4SplitMuDct - U8TCSCpSplitMuDiffExitHyst) * Axle_TCS->lctcss16MassOfOneCorn) /100)*(Axle_TCS->lctcss16WhlRolR/10))/10;/*resolution  Mu 1/100 WhlRolR 1/1000 중력가속도 10*/	  	
	      
	  	Axle_TCS->lctcss16BsTarBrkTorqAXLEdiff = McrAbs(WL_LEFT->lctcss16BsTarBrkTorq - WL_RIGHT->lctcss16BsTarBrkTorq);  
        Axle_TCS->lctcss16BsTarBrkTorqAXLEdiffFlt = LCESP_s16Lpf1Int(Axle_TCS->lctcss16BsTarBrkTorqAXLEdiff, Axle_TCS->lctcss16BsTarBrkTorqAXLEdiffFlt, U8TCSCpCurntFiltGain);
	  	    
	  	  
		if((Axle_TCS->lctcsu1DctBTCSplitMuAxle == 0)&&(Axle_TCS->lctcsu1SymSpinDctFlg == 0)) /* Split 판단 진입*/
	    {	  
			if(Axle_TCS->lctcss16BsTarBrkTorqAXLEdiffFlt >= Axle_TCS->lctcss16BsBrkTorqTh4SplitDct)
	        {
	        	Axle_TCS->lctcss16DctBTCSplitMuEnterCnt = Axle_TCS->lctcss16DctBTCSplitMuEnterCnt + 1;
	        }
	        else 
	        {
	        	Axle_TCS->lctcss16DctBTCSplitMuEnterCnt = Axle_TCS->lctcss16DctBTCSplitMuEnterCnt - 1;
	        }
	        Axle_TCS->lctcss16DctBTCSplitMuEnterCnt = LCTCS_s16ILimitRange(Axle_TCS->lctcss16DctBTCSplitMuEnterCnt, 0, L_U8_TIME_10MSLOOP_2S);
	      	
	      	if (Axle_TCS->lctcss16DctBTCSplitMuEnterCnt >= U8TCSCpSplitMuDctEnterCnt)
	      	{
	      		Axle_TCS->lctcsu1DctBTCSplitMuAxle = 1;
	      	    Axle_TCS->lctcss16DctBTCSplitMuExitCnt = 0;
	      	}
	      	else
	      	{
	      		;
	      	}	  	  
	    }
	    else  /* Split 판단 해제*/
	    {
	    	Axle_TCS->tcs_axle_tempW0 = Axle_TCS->lctcss16BsBrkTorqTh4SplitExit;
	      	Axle_TCS->tcs_axle_tempW0 = LCTCS_s16ILimitMinimum(Axle_TCS->tcs_axle_tempW0, 0 );
	      	  
	      	if(Axle_TCS->lctcss16BsTarBrkTorqAXLEdiffFlt <= Axle_TCS->tcs_axle_tempW0)
	      	{
	      		Axle_TCS->lctcss16DctBTCSplitMuExitCnt = Axle_TCS->lctcss16DctBTCSplitMuExitCnt + 1;
	      	}
	      	else
	      	{
	      		Axle_TCS->lctcss16DctBTCSplitMuExitCnt = Axle_TCS->lctcss16DctBTCSplitMuExitCnt - 1;
	      	}
	      	Axle_TCS->lctcss16DctBTCSplitMuExitCnt = LCTCS_s16ILimitRange(Axle_TCS->lctcss16DctBTCSplitMuExitCnt, 0, L_U8_TIME_10MSLOOP_2S);
	      	  
	      	if((Axle_TCS->lctcss16DctBTCSplitMuExitCnt >= U8TCSCpSplitMuDctExitCnt)||(Axle_TCS->lctcsu1SymSpinDctFlg == 1))
	      	{
	      		Axle_TCS->lctcsu1DctBTCSplitMuAxle = 0;
	      	  	Axle_TCS->lctcss16DctBTCSplitMuEnterCnt = 0;
	      	}
	      	else
	      	{
	      		;
	      	}
	    }
	}
	else
	{
		Axle_TCS->lctcss16DctBTCSplitMuEnterCnt = 0;
	  	Axle_TCS->lctcss16DctBTCSplitMuExitCnt = 0;
	  	Axle_TCS->lctcsu1DctBTCSplitMuAxle = 0;
	  	Axle_TCS->lctcss16BsTarBrkTorqAXLEdiffFlt = 0;
	}
}
 
void LDTCS_vDctSplitMuFlag(void)
{
	if((FA_TCS.lctcsu1DctBTCSplitMuAxle==1)||(RA_TCS.lctcsu1DctBTCSplitMuAxle==1))
	{
		lctcsu1DctBTCSplitMuFlag = 1;
	}	
	else
	{
		lctcsu1DctBTCSplitMuFlag = 0;
	}
} 

void LDTCS_vDctSplitHill(void)  
{
	LDTCS_vDctHillByAxSen();
 	LDTCS_vDctSplitHillByAxSen(); 		
	LDTCS_vDctSplitHillByVehAccel(); 
}

void LDTCS_vDctHillByAxSen(void)
{
	tcs_tempW0 = ((int16_t)U8TCSCpSplitHilDctAccTh + ACCEL_X_0G04G);/* +0.04g */
	
  #if __HSA 
	if ( ( HSA_flg == 1) && (McrAbs(HSA_ax_sen_filt)>=U8TCSHillDctAlongTh))/* 경사도 조건 추가 */
	{	/* Set Condition */
		lctcsu1DctBTCHillByAx = 1;   /*STOP_ON_HILL_flag민감작동에 따른 HSA flg변경*/
	}
	else
	{	/* Reset Condition */
		if	( ((acc_r) >= (tcs_tempW0)) || ((lctcss16VehSpd4TCS) >= ((int16_t)U8TCSCpSplitHilDctSpdTh)) )
		{
			lctcsu1DctBTCHillByAx = 0;
		}
		else if	(lctcsu1DctBTCSplitMuHill == 1)  /* 일단 split-mu hill 판단 되면 hill 판단은 더 이상 쓸데가 없으니 해제함 */
		{
			lctcsu1DctBTCHillByAx = 0;
		}
		else if (lctcsu16BTCStpNMovDistance >= S16TCSCpSplitHilExtDistanceTh)
		{
			lctcsu1DctBTCHillByAx = 0;	
		}
		else
		{
			;
		}	
	}
  #else
  	lctcsu1DctBTCHillByAx = 0;
  #endif	
}
		

void LDTCS_vDctSplitHillByAxSen(void)
{
	if ( lctcsu1DctBTCSplitMuHill == 0 )   	/*HILL 판단 진입*/
	{
		if		( (lctcsu1DctBTCHillByAx == 1) && (lctcsu1DctBTCSplitMuFlag==1) && ((lctcss16VehSpd4TCS) < ((int16_t)U8TCSCpSplitHilDctSpdTh)) )
		{
			lctcsu1DctBTCSplitMuHill = 1; 
			lctcsu1DctBTCHillByAx = 0;
		}
		else if ( ((lctcsu1DctBTCSplitMuFlag)==1) || ((lctcss16VehSpd4TCS) >= ((int16_t)U8TCSCpSplitHilDctSpdTh)) )
		{
			lctcsu1DctBTCSplitMuHill = 0;
		}
		else  
		{
			;
		}
	}
	else
	{
	   	;
	}
}

void LDTCS_vDctSplitHillByVehAccel(void) 
{
	/* HSA enable된 사양이더라도 주행중 가속시는 ax를 사용한 hill 판단 못하므로 lctcsu1DctBTCHillByAx가 0인 상태에서는 vehicle acceleration 에 의한 split-mu hill 판단 수행한다. */
	if ( (lctcsu1DctBTCHillByAx == 0) && (lctcsu1DctBTCSplitMuFlag == 1) && ((lctcss16VehSpd4TCS) < ((int16_t)U8TCSCpSplitHilDctSpdTh)) )
	{
		if (lctcsu1DctBTCSplitMuHill == 0)
	    {   /*Split Hill 진입 조건*/
	    	if(acc_r<= U8TCSCpSplitHilDctAccTh)
	        {                                                        
	        	lctcsu8SplitMuDecHilEnterCnt = lctcsu8SplitMuDecHilEnterCnt + 1;
	        }  
	        else
	        {
	        	if(lctcsu8SplitMuDecHilEnterCnt >=1)
	        	{
	        		lctcsu8SplitMuDecHilEnterCnt = lctcsu8SplitMuDecHilEnterCnt - 1;
	        }  
	        else
	        {
	        		;
	        }
	        }
	        lctcsu8SplitMuDecHilEnterCnt = (uint8_t)LCTCS_s16ILimitRange((int16_t)lctcsu8SplitMuDecHilEnterCnt, 0, (int16_t)U8TCSCpSplitHilDctEnterCnt);
	          
			if (lctcsu8SplitMuDecHilEnterCnt >= (uint8_t)U8TCSCpSplitHilDctEnterCnt)
		    {
		    	lctcsu1DctBTCSplitMuHill = 1;
				lctcsu8SplitMuDecHilEnterCnt = 0;
		    }
		    else
		    {
		    	;
		    }
		}
		else
		{   /*Split Hill 해제 조건*/
			tcs_tempW0 = ((int16_t)U8TCSCpSplitHilDctAccTh + ACCEL_X_0G04G);/* +0.04g */
			if(acc_r >= tcs_tempW0)
			{
				lctcsu8SplitMuDecHilExitCnt = lctcsu8SplitMuDecHilExitCnt + 1;
			}
			else
			{
				if(lctcsu8SplitMuDecHilExitCnt >= 1)
				{
					lctcsu8SplitMuDecHilExitCnt = lctcsu8SplitMuDecHilExitCnt - 1;
			}	
				else
				{
					;
				}
			}	
			lctcsu8SplitMuDecHilExitCnt = (uint8_t)LCTCS_s16ILimitRange((int16_t)lctcsu8SplitMuDecHilExitCnt, 0, (int16_t)U8TCSCpSplitHilDctExitCnt); 
	  
			if((lctcsu8SplitMuDecHilExitCnt >= (uint8_t)U8TCSCpSplitHilDctExitCnt))
	  		{
				lctcsu1DctBTCSplitMuHill = 0;
			  	lctcsu8SplitMuDecHilExitCnt = 0;
			}
			else
			{
				;
			}
		}
	}
	else
	{
		lctcsu1DctBTCSplitMuHill = 0;
	    lctcsu8SplitMuDecHilEnterCnt = 0;
	    lctcsu8SplitMuDecHilExitCnt = 0;
	}
}
 
void LCTCS_vTransAsymSpnCtlMod (struct TCS_AXLE_STRUCT *Axle_TCS)
{
    Axle_TCS->ltcsu1ModOfAsymSpnCtlOld = Axle_TCS->ltcsu1ModOfAsymSpnCtl;

    if(Axle_TCS->lctcsu1BrkCtlActAxle == 0)
    {
        Axle_TCS->ltcsu1ModOfAsymSpnCtl=S16TCSAsymSpinFwdCtlMode;
        Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt = 0;
    }
    else
    {
			Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt = Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt + 1;
			Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt = LCTCS_s16ILimitMaximum(Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt, (int16_t)U8TCSCpAsymSpnCtlFFmodeTimeUp); 
			
    	if((Axle_TCS->lctcsu1SymSpinDctFlg == 1)||(lctcsu1IniMtrCtlMode == 1)||(lctcsu1IniMtrCtlModeFalEdg == 1))
    	{
    		Axle_TCS->ltcsu1ModOfAsymSpnCtl=S16TCSAsymSpinFwdCtlMode;
    	}
    	else
    	{
        if( ((Axle_TCS->lctcss16EstBrkTorqOnLowMuWhl)>=(Axle_TCS->lctcss16FFBrkTorq4Asym))
            || (Axle_TCS->ltcsu1SpinDiffDec==1)
            || (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable)
            || (Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt >= U8TCSCpAsymSpnCtlFFmodeTimeUp)
            || (ENG_STALL==1)
            || (Axle_TCS->ltcss16WhlSpinDiff==0) 
            )
        {
            Axle_TCS->ltcsu1ModOfAsymSpnCtl=S16TCSAsymSpinBwdCtlMode;
            Axle_TCS->lctcsu1AsymSpnCtlFFmodeCnt = 0;
        }
        else
        {
            ;
        }
        }
    }

    if ( (Axle_TCS->ltcsu1ModOfAsymSpnCtlOld==S16TCSAsymSpinFwdCtlMode) &&
         (Axle_TCS->ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinBwdCtlMode) )
    {
        Axle_TCS->ltcsu1ModOfAsymSpnCtlRsgEdg = 1;
    }
    else
    {
        Axle_TCS->ltcsu1ModOfAsymSpnCtlRsgEdg = 0;
    }
}

void LCTCS_vTransCirAsymSpnXspCtlMod(void)
{
    if	( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
          (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;     	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl; 
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;    	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl; 
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;    	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;     	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
	 	SC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;    	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==1) && (FR_TCS.lctcsu1BTCSWhlAtv==0) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==0) && (RR_TCS.lctcsu1BTCSWhlAtv==1) )
    {
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
	 	if	((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode))
	 	{
	 		SC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
	 	}
	 	else if ((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinBwdCtlMode))
	 	{
	 		SC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;    
	 	}
	 	else if ((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinBwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode))
	 	{
	 		SC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
	 	}
	 	else
	 	{ 
	 		SC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;    
	 	}    	
    }
    else if ( (FL_TCS.lctcsu1BTCSWhlAtv==0) && (FR_TCS.lctcsu1BTCSWhlAtv==1) &&
              (RL_TCS.lctcsu1BTCSWhlAtv==1) && (RR_TCS.lctcsu1BTCSWhlAtv==0) )
    {
		if	((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)) 
		{
			PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
		}
		else if ((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinBwdCtlMode)) 
		{
			PC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;
		}
		else if ((FA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinBwdCtlMode)&&(RA_TCS.ltcsu1ModOfAsymSpnCtl==S16TCSAsymSpinFwdCtlMode))
		{
			PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
		}
		else
		{  
			PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
		}    	
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
    }
    else
    {
		/* More than 3 wheels control is not considered */
		PC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
		SC_TCS.ltcsu1CirModOfAsymSpnCtl = 0;
    }
} 

void LCTCS_vTransCirAsymSpnFRspCtlMod(void)
{	 
	PC_TCS.ltcsu1CirModOfAsymSpnCtl = FA_TCS.ltcsu1ModOfAsymSpnCtl;
	SC_TCS.ltcsu1CirModOfAsymSpnCtl = RA_TCS.ltcsu1ModOfAsymSpnCtl;	
}	    
	

void LDTCS_vDctEndOfBrkCtlOfVeh(void)
{
    if (BTC_fz==1)
    {
        if( (lctcsu1BrkCtrlInhibit==1)
          ||(lctcss16BrkCtrlMode==S16_TCS_CTRL_MODE_DISABLE)
          ||( (lctcsu1EngInformInvalid==0) && (lctcsu1DriverAccelIntend==0) )
          )
        {
            FL.BTCS = 0;
            FR.BTCS = 0;
            RL.BTCS = 0;
            RR.BTCS = 0;
            BTC_fz = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
        FL.BTCS = 0;
        FR.BTCS = 0;
        RL.BTCS = 0;
        RR.BTCS = 0;
        BTC_fz = 0;
    }
}

void LDTCS_vDctEndOfBrkCtlOfWhl(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	if (W_L->BTCS == 1)
	{
		if ( (Wl_TCS->lctcss16TarWhlPre==0) && (Wl_TCS->lctcss16EstWhlPre==0) && (Wl_TCS->lctcss16BTCSWhlOnTime > L_U8_TIME_10MSLOOP_100MS) )
		{
			if (Wl_TCS->lctcss8BTCSEndTmr >= L_U8_TIME_10MSLOOP_100MS)
			{
				W_L->BTCS = 0;
				Wl_TCS->lctcss8BTCSEndTmr = 0;
			}
			else
			{
				Wl_TCS->lctcss8BTCSEndTmr = Wl_TCS->lctcss8BTCSEndTmr + 1;
			}
		}
		else
		{
		    Wl_TCS->lctcss8BTCSEndTmr = 0;
		}
	}
	else
	{
		Wl_TCS->lctcss8BTCSEndTmr = 0;
	}
}


void LDTCS_vDctEndOfBrkCtlOfDrvMd(void)
{
  #if __4WD_VARIANT_CODE==ENABLE  
    if(lctcsu1DrvMode2Wheel==1)
    {
    	/* 2WD MODE */
    	#if __REAR_D
		  FL.BTCS = 0 ;
		  FR.BTCS = 0 ;
    	#else
		  RL.BTCS = 0 ;
		  RR.BTCS = 0 ;
    	#endif
    }
    else
    {
		; 	/* 4WD MODE */
	}
  #else
	#if (__4WD==0) /* 2WD */
      #if __REAR_D
		  FL.BTCS = 0 ;
		  FR.BTCS = 0 ;
      #else
		  RL.BTCS = 0 ;
		  RR.BTCS = 0 ;
      #endif
    #endif
  #endif
}


void LDTCS_vDctEndOfBrkCtlBySpdDiff(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT,struct W_STRUCT *WL_L,struct W_STRUCT *WL_R)
{
	if ( (Axle_TCS->lctcsu1BrkCtlActAxle==1) && (Axle_TCS->lctcsu1SymSpinDctFlg==0) &&(VCA_ON_TCMF==0))
	{
		if (WL_L->BTCS==1)
		{			
			tcs_tempW0 = -WL_LEFT->lctcss16AsymSpnCtlTh;
			if	( (WL_LEFT->lctcss16TarWhlPre == 0) && 
				  (Axle_TCS->ltcss16WhlSpinDiffNF<tcs_tempW0) && (WL_R->arad>0) /* Should control Right wheel */
				)
			{
				WL_L->BTCS = 0;
			}
			else
			{    
				;
			}
		}
		else		/*if (BTCS is ativated on right wheel of axle) */
		{
			tcs_tempW0 = WL_RIGHT->lctcss16AsymSpnCtlTh;
			if	( (WL_RIGHT->lctcss16TarWhlPre == 0) &&
				  (Axle_TCS->ltcss16WhlSpinDiffNF>tcs_tempW0) && (WL_L->arad>0) /* Should control left wheel */
				) 
			{
				WL_R->BTCS = 0;
			}
			else
			{
				;
			}			
		}
	}
	else
	{
		;
	}
}

void LDTCS_vDctEndofBrkCtlByHunting(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_L, struct W_STRUCT *WL_R)
{
	if ( (Axle_TCS->lctcsu1HuntingDcted==1) && (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuUnStable) &&(VCA_ON_TCMF==0))
	{	/* TC valve에 의한 dump는 너무 느려서 homo-mu에서 hunting을 안정화 할만큼 빠른 응답성을 얻을수 없으므로 Hunting 감지시 에는 lctcsu8StatWhlOnHghMu 가 unstable이면 즉시 제어 종료 */
		WL_L->BTCS = 0;
		WL_R->BTCS = 0;
	}
	else
	{
		;
	}
}

void LDTCS_vDctEndofBrkCtlOfVCA(void)
{	
	tcs_tempW0 = (int16_t)U8TCSCpRearCtlOfVCA;
	
	if  (VCA_BRAKE_CTRL_END_TCMF==1)
	{
		if (tcs_tempW0 == 1)
			{
	  		if((RL_TCS.VCA_TCMF_CTL==1) && (RL.BTCS==1))
			{
				RL.BTCS = 0;
            	
            	if((FR_TCS.VCA_TCMF_CTL==1) && (FR.BTCS==1))  /*temp*/
			{
				FR.BTCS = 0;
			}
			else
            	{ ; }	/*temp*/
            }
            else if ((RR_TCS.VCA_TCMF_CTL==1) && (RR.BTCS==1))
			{
            	RR.BTCS = 0;
            	if((FL_TCS.VCA_TCMF_CTL==1) && (FL.BTCS==1))  /*temp*/
            	{
            		FL.BTCS = 0;
            	}
            	else
            	{ ; }	/*temp*/
            }
            else
            {
            	;
            }
        }
        else
        {   
        	if ((FL_TCS.VCA_TCMF_CTL==1) && (FL.BTCS==1) )
            {
            	FL.BTCS = 0;
            }	
            else if ((FR_TCS.VCA_TCMF_CTL==1) && (FR.BTCS==1))
            {
            	FR.BTCS = 0;
            }
	  	    else
            {
            	;
            }
		}
		ltcsu8VCARunningCntTCMF = 0;
		VCA_ON_TCMF_OLD = 0;
		VCA_ON_TCMF = 0;
		RR_TCS.VCA_TCMF_CTL = 0;
		RL_TCS.VCA_TCMF_CTL = 0;
		FR_TCS.VCA_TCMF_CTL = 0;
		FL_TCS.VCA_TCMF_CTL = 0;
		ltcss16MaxVrefInVCATCMF = 0;	
		VCA_BRAKE_CTRL_END_TCMF = 0;		
	}
	else
	{
		;
	}
}

void LDTCS_vDctEndofBrkCtlofESC(struct TCS_WL_STRUCT *Wl_ESC, struct W_STRUCT *W_L)
{
	if ( ( Wl_ESC->lctcsu1ESCWhlAtv == 1) && (W_L->BTCS == 1) )
	{
		W_L->BTCS = 0;
	}
	else
	{
		;
	}
}  

void LDTCS_vDctEndofBrkCtlBySlip(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	Wl_TCS->tcs_wl_tempW0 = W_L->wvref - W_L->vrad_crt;
	
	if ( ( Wl_TCS->lctcsu1OverBrk==1) && (Wl_TCS->tcs_wl_tempW0 >= VREF_3_KPH) )
	{
		W_L->BTCS = 0;
	}
	else
	{
		;
	}
}

#endif  /* TAG2 */
#endif  /* TAG1 */

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
    #define idx_FILE    idx_CL_LDBTCSCallDetection
    #include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

