#ifndef __LDABSSIDEVREF_H__
#define	__LDABSSIDEVREF_H__
#include	"LVarHead.h"

#if __SIDE_VREF
/*       VFLAG16(Side Wheel Ref) */
#define	LEFT_SIDE                   SVREF00.bit7      //Out side-Left ==1/ Inside-Left==0
#define OUT_LEFT                    SVREF00.bit6
#define DRIVE_L                     SVREF00.bit5
#define DRIVE_R                     SVREF00.bit4        
#define WDRIVE                      SVREF00.bit3
#define LMINC                       SVREF00.bit2
#define LMDEC                       SVREF00.bit1        
#define UP_SIDE                     SVREF00.bit0
/*       VFLAG17(Side Wheel Ref) */
#define DOWN_SIDE                   SVREF01.bit7      //Out side-Left ==1/ Inside-Left==0
#define HOLD_SIDE                   SVREF01.bit6
#define LMINC_L                     SVREF01.bit5
#define LMINC_R                     SVREF01.bit4        
#define LMDEC_L                     SVREF01.bit3
#define LMDEC_R                     SVREF01.bit2
#define UP_SIDE_L                   SVREF01.bit1        
#define UP_SIDE_R                   SVREF01.bit0
/*       VFLAG18(Side Wheel Ref) */
#define HOLD_SIDE_L                 SVREF02.bit7      //Out side-Left ==1/ Inside-Left==0
#define HOLD_SIDE_R                 SVREF02.bit6
#define LMInc_OUT                   SVREF02.bit5
#define LMDec_OUT                   SVREF02.bit4        
#define LMInc_IN                    SVREF02.bit3
#define LMDec_IN                    SVREF02.bit2
#define OUTSIDE_UP                  SVREF02.bit1        
#define OUTSIDE_HOLD                SVREF02.bit0
/*       VFLAG19(Side Wheel Ref) */
#define INSIDE_UP                   SVREF03.bit7      //Out side-Left ==1/ Inside-Left==0
#define tempF4            	        SVREF03.bit6       // AFZ������ 051111 By KSW
#define UNUSED6			                SVREF03.bit5
#define UNUSED5                     SVREF03.bit4        
#define UNUSED4                     SVREF03.bit3
#define UNUSED3                     SVREF03.bit2
#define UNUSED2                     SVREF03.bit1        
#define UNUSED1                     SVREF03.bit0
#endif  

#if __SIDE_VREF && __VREF_AUX
#define VrefAux_Limit_flag                        VAUXF0.bit7
#define VrefAux_Limit_flag_K                      VAUXF0.bit6
#define reserved_for_VrefAux5                     VAUXF0.bit5
#define reserved_for_VrefAux4                     VAUXF0.bit4
#define reserved_for_VrefAux3                     VAUXF0.bit3
#define reserved_for_VrefAux2                     VAUXF0.bit2
#define reserved_for_VrefAux1                     VAUXF0.bit1
#define reserved_for_VrefAux0                     VAUXF0.bit0
#endif

/*Global Extern Variable  Declaration***********************/
#if __SIDE_VREF && __VREF_AUX
extern	struct	U8_BIT_STRUCT_t VAUXF0;
#endif

	#if __VREF_AUX  
    extern	int16_t	vref_aux;    /* 0601 KSW */    
    #endif
#if __SIDE_VREF

   	#if __WL_SPEED_RESOL_CHANGE
    extern	int16_t	vref_L;                     // Side Wheel Reference 04.11.11
    extern 	int16_t	vref_L_diff; 
    extern 	int16_t	vref_R;                     // Side Wheel Reference 04.11.11
    extern 	int16_t	vref_R_diff;         
 	#endif
#endif 


/*Global Extern Functions  Declaration**********************/

extern	void	LDABS_vCallMainSideVref(void);
#endif
