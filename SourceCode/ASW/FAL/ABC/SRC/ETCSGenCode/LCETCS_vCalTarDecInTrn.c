/*
 * File: LCETCS_vCalTarDecInTrn.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecInTrn'.
 *
 * Model version                  : 1.171
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarDecInTrn.h"
#include "LCETCS_vCalTarDecInTrn_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarDecInTrn' */
void LCETCS_vCalTarDecInTrn_Init(B_LCETCS_vCalTarDecInTrn_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcss16TarDecInTrn);
}

/* Start for referenced model: 'LCETCS_vCalTarDecInTrn' */
void LCETCS_vCalTarDecInTrn_Start(B_LCETCS_vCalTarDecInTrn_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpCnt4TarMaxDecInTrn);
}

/* Output and update for referenced model: 'LCETCS_vCalTarDecInTrn' */
void LCETCS_vCalTarDecInTrn(int16_T rtu_lcetcss16TarDecDpndDelYaw, int16_T
  rtu_lcetcss16TarSpnDecCnt, int16_T *rty_lcetcss16TarDecInTrn,
  B_LCETCS_vCalTarDecInTrn_c_T *localB)
{
  /* Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpCnt4TarMaxDecInTrn);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_lcetcss16TarSpnDecCnt, rtCP_Constant1_Value,
                        localB->x2, rtCP_Constant3_Value,
                        rtu_lcetcss16TarDecDpndDelYaw,
                        &localB->lcetcss16TarDecInTrn);

  /* Saturate: '<Root>/Saturation' */
  if (localB->lcetcss16TarDecInTrn > 0) {
    *rty_lcetcss16TarDecInTrn = 0;
  } else if (localB->lcetcss16TarDecInTrn < -120) {
    *rty_lcetcss16TarDecInTrn = -120;
  } else {
    *rty_lcetcss16TarDecInTrn = localB->lcetcss16TarDecInTrn;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTarDecInTrn_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
