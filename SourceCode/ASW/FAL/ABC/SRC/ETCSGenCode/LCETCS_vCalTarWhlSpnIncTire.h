/*
 * File: LCETCS_vCalTarWhlSpnIncTire.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncTire'.
 *
 * Model version                  : 1.86
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:36 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpnIncTire_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpnIncTire_h_
#ifndef LCETCS_vCalTarWhlSpnIncTire_COMMON_INCLUDES_
# define LCETCS_vCalTarWhlSpnIncTire_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarWhlSpnIncTire_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarWhlSpnIncTire_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalDetTarWhlIncTire.h"
#include "LCETCS_vCalAbnrmSizeTire.h"

/* Block signals for model 'LCETCS_vCalTarWhlSpnIncTire' */
typedef struct {
  boolean_T lcetcsu1AbnrmSizeTireDet;  /* '<Root>/LCETCS_vCalAbnrmSizeTire' */
  boolean_T lcetcsu1AbnrmSizeTireSus;  /* '<Root>/LCETCS_vCalAbnrmSizeTire' */
} B_LCETCS_vCalTarWhlSpnIncTire_c_T;

typedef struct {
  B_LCETCS_vCalTarWhlSpnIncTire_c_T rtb;
} MdlrefDW_LCETCS_vCalTarWhlSpnIncTire_T;

/* Model reference registration function */
extern void LCETCS_vCalTarWhlSpnIncTire_initialize(void);
extern void LCETCS_vCalTarWhlSpnIncTire_Init(boolean_T
  *rty_lcetcsu1AbnrmSizeTire, B_LCETCS_vCalTarWhlSpnIncTire_c_T *localB);
extern void LCETCS_vCalTarWhlSpnIncTire(const TypeETCSWhlStruct *rtu_ETCS_FL,
  const TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL,
  const TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, boolean_T *rty_lcetcsu1AbnrmSizeTire,
  int16_T *rty_lcetcss16TarWhlSpnIncAbnrmTire, B_LCETCS_vCalTarWhlSpnIncTire_c_T
  *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarWhlSpnIncTire'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpnIncTire_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
