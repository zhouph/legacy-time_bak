/*
 * File: LCTMC_vCalRefUpVehSpdMapbyGear_private.h
 *
 * Code generated for Simulink model 'LCTMC_vCalRefUpVehSpdMapbyGear'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalRefUpVehSpdMapbyGear_private_h_
#define RTW_HEADER_LCTMC_vCalRefUpVehSpdMapbyGear_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef GEAR_POS_TM_1
#error The variable for the parameter "GEAR_POS_TM_1" is not defined
#endif

#ifndef GEAR_POS_TM_2
#error The variable for the parameter "GEAR_POS_TM_2" is not defined
#endif

#ifndef GEAR_POS_TM_3
#error The variable for the parameter "GEAR_POS_TM_3" is not defined
#endif

#ifndef GEAR_POS_TM_4
#error The variable for the parameter "GEAR_POS_TM_4" is not defined
#endif

#ifndef GEAR_POS_TM_5
#error The variable for the parameter "GEAR_POS_TM_5" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_1
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_1" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_2
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_2" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_3
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_3" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_4
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_4" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_5
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCTMC_vCalRefUpVehSpdMapbyGear_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
