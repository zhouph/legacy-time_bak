/*
 * File: LCETCS_vCalEngStallThr_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr'.
 *
 * Model version                  : 1.251
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:52 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallThr_private_h_
#define RTW_HEADER_LCETCS_vCalEngStallThr_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpHighRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmHill" is not defined
#endif

extern const int16_T rtCP_pooled_T1tcrUJCQflL;
extern const int16_T rtCP_pooled_5VZoubGQDuYp;
extern const int16_T rtCP_pooled_Krm7oDVfoe0C;

#define rtCP_Constant1_Value           rtCP_pooled_T1tcrUJCQflL  /* Computed Parameter: Constant1_Value
                                                                  * Referenced by: '<S2>/Constant1'
                                                                  */
#define rtCP_Constant2_Value           rtCP_pooled_5VZoubGQDuYp  /* Computed Parameter: Constant2_Value
                                                                  * Referenced by: '<S2>/Constant2'
                                                                  */
#define rtCP_Constant6_Value           rtCP_pooled_Krm7oDVfoe0C  /* Computed Parameter: Constant6_Value
                                                                  * Referenced by: '<S2>/Constant6'
                                                                  */
#define rtCP_Constant1_Value_h         rtCP_pooled_T1tcrUJCQflL  /* Computed Parameter: Constant1_Value_h
                                                                  * Referenced by: '<S1>/Constant1'
                                                                  */
#define rtCP_Constant2_Value_o         rtCP_pooled_5VZoubGQDuYp  /* Computed Parameter: Constant2_Value_o
                                                                  * Referenced by: '<S1>/Constant2'
                                                                  */
#define rtCP_Constant6_Value_c         rtCP_pooled_Krm7oDVfoe0C  /* Computed Parameter: Constant6_Value_c
                                                                  * Referenced by: '<S1>/Constant6'
                                                                  */
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallThr_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
