/*
 * File: LCETCS_vCalWhlSpinNonFilt.h
 *
 * Code generated for Simulink model 'LCETCS_vCalWhlSpinNonFilt'.
 *
 * Model version                  : 1.82
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:42 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalWhlSpinNonFilt_h_
#define RTW_HEADER_LCETCS_vCalWhlSpinNonFilt_h_
#ifndef LCETCS_vCalWhlSpinNonFilt_COMMON_INCLUDES_
# define LCETCS_vCalWhlSpinNonFilt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalWhlSpinNonFilt_COMMON_INCLUDES_ */

#include "LCETCS_vCalWhlSpinNonFilt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCalWhlSpinNonFilt_initialize(void);
extern void LCETCS_vCalWhlSpinNonFilt(const TypeETCSWhlStruct *rtu_ETCS_FL,
  const TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL,
  const TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, int16_T
  *rty_lcetcss16WhlSpinNonFilt, int16_T *rty_lcetcss16WhlSpinFrtAxlNonFilt,
  int16_T *rty_lcetcss16WhlSpinRrAxlNonFilt);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalWhlSpinNonFilt'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalWhlSpinNonFilt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
