/*
 * File: LCETCS_vCalTarWhlSpnIncHiRd.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncHiRd'.
 *
 * Model version                  : 1.135
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_h_
#ifndef LCETCS_vCalTarWhlSpnIncHiRd_COMMON_INCLUDES_
# define LCETCS_vCalTarWhlSpnIncHiRd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarWhlSpnIncHiRd_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarWhlSpnIncHiRd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalTarWhlSpnIncHiRd' */
typedef struct {
  int16_T lcetcsu8AddTar4RdAdapt;      /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T lcetcsu8AddTar4RdAdapt_h;    /* '<Root>/LCETCS_s16Inter2Point1' */
} B_LCETCS_vCalTarWhlSpnIncHiRd_c_T;

typedef struct {
  B_LCETCS_vCalTarWhlSpnIncHiRd_c_T rtb;
} MdlrefDW_LCETCS_vCalTarWhlSpnIncHiRd_T;

/* Model reference registration function */
extern void LCETCS_vCalTarWhlSpnIncHiRd_initialize(void);
extern void LCETCS_vCalTarWhlSpnIncHiRd_Init(B_LCETCS_vCalTarWhlSpnIncHiRd_c_T
  *localB);
extern void LCETCS_vCalTarWhlSpnIncHiRd(int16_T rtu_lcetcss16LmtCrngDltYaw,
  const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT_OLD, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, uint8_T *rty_lcetcsu8AddTar4HighRd,
  B_LCETCS_vCalTarWhlSpnIncHiRd_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarWhlSpnIncHiRd'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
