/*
 * File: LCETCS_vCalAbnrmSizeTire.h
 *
 * Code generated for Simulink model 'LCETCS_vCalAbnrmSizeTire'.
 *
 * Model version                  : 1.92
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalAbnrmSizeTire_h_
#define RTW_HEADER_LCETCS_vCalAbnrmSizeTire_h_
#ifndef LCETCS_vCalAbnrmSizeTire_COMMON_INCLUDES_
# define LCETCS_vCalAbnrmSizeTire_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalAbnrmSizeTire_COMMON_INCLUDES_ */

#include "LCETCS_vCalAbnrmSizeTire_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalSusAbnrmTire.h"
#include "LCETCS_vCalFinAbnrmTire.h"
#include "LCETCS_vCalDetAbnrmTire.h"

/* Model reference registration function */
extern void LCETCS_vCalAbnrmSizeTire_initialize(void);
extern void LCETCS_vCalAbnrmSizeTire_Init(boolean_T
  *rty_lcetcsu1AbnrmSizeTireDet, boolean_T *rty_lcetcsu1AbnrmSizeTireSus,
  boolean_T *rty_lcetcsu1AbnrmSizeTire);
extern void LCETCS_vCalAbnrmSizeTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1AbnrmSizeTireDet,
  boolean_T *rty_lcetcsu1AbnrmSizeTireSus, boolean_T *rty_lcetcsu1AbnrmSizeTire);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalAbnrmSizeTire'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalAbnrmSizeTire_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
