/*
 * File: LCETCS_vDctSetVca.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSetVca'.
 *
 * Model version                  : 1.570
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSetVca.h"
#include "LCETCS_vDctSetVca_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctSetVca' */
void LCETCS_vDctSetVca_Init(boolean_T *rty_lcetcsu1ReqVcaTrqAct,
  DW_LCETCS_vDctSetVca_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1ReqVcaTrqAct = false;
}

/* Output and update for referenced model: 'LCETCS_vDctSetVca' */
void LCETCS_vDctSetVca(const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int16_T *rty_lcetcss16VehAccErrCount,
  boolean_T *rty_lcetcsu1ReqVcaTrqAct, DW_LCETCS_vDctSetVca_f_T *localDW)
{
  int32_T VehAccErr;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if (((((uint8_T)U8ETCSCpAWDVehicle) == 1) && (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct
        == 1)) && (rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc <
                   rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtSnow)) {
    /* Transition: '<S1>:54' */
    /* Transition: '<S1>:53' */
    VehAccErr = rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd -
      rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl;
    if (VehAccErr > ((uint8_T)U8ETCSCpStDeltAcc)) {
      /* Transition: '<S1>:10' */
      /* Transition: '<S1>:9' */
      VehAccErr = localDW->UnitDelay2_DSTATE + 1;
      if (VehAccErr > 32767) {
        VehAccErr = 32767;
      }

      localDW->UnitDelay2_DSTATE = (int16_T)VehAccErr;

      /* Transition: '<S1>:79' */
      /* Transition: '<S1>:78' */
      /* Transition: '<S1>:82' */
    } else {
      /* Transition: '<S1>:7' */
      if (localDW->UnitDelay1_DSTATE == 1) {
        /* Transition: '<S1>:69' */
        /* Transition: '<S1>:71' */
        if (VehAccErr <= ((uint8_T)U8ETCSCpHysDeltAcc)) {
          /* Transition: '<S1>:73' */
          /* Transition: '<S1>:75' */
          VehAccErr = localDW->UnitDelay2_DSTATE - 1;
          if (VehAccErr < -32768) {
            VehAccErr = -32768;
          }

          localDW->UnitDelay2_DSTATE = (int16_T)VehAccErr;

          /* Transition: '<S1>:78' */
          /* Transition: '<S1>:82' */
        } else {
          /* Transition: '<S1>:77' */
          /* Transition: '<S1>:82' */
        }
      } else {
        /* Transition: '<S1>:81' */
        VehAccErr = localDW->UnitDelay2_DSTATE - 1;
        if (VehAccErr < -32768) {
          VehAccErr = -32768;
        }

        localDW->UnitDelay2_DSTATE = (int16_T)VehAccErr;
      }
    }

    /* Transition: '<S1>:83' */
  } else {
    /* Transition: '<S1>:57' */
    localDW->UnitDelay2_DSTATE = 0;
  }

  /* Transition: '<S1>:60' */
  if (localDW->UnitDelay2_DSTATE >= 25) {
    /* Transition: '<S1>:109' */
    /* Transition: '<S1>:111' */
    *rty_lcetcsu1ReqVcaTrqAct = true;

    /* Transition: '<S1>:131' */
    /* Transition: '<S1>:129' */
    /* Transition: '<S1>:130' */
  } else {
    /* Transition: '<S1>:115' */
    if (localDW->UnitDelay1_DSTATE == 1) {
      /* Transition: '<S1>:118' */
      /* Transition: '<S1>:120' */
      if (localDW->UnitDelay2_DSTATE <= 0) {
        /* Transition: '<S1>:122' */
        /* Transition: '<S1>:126' */
        *rty_lcetcsu1ReqVcaTrqAct = false;

        /* Transition: '<S1>:129' */
        /* Transition: '<S1>:130' */
      } else {
        /* Transition: '<S1>:128' */
        *rty_lcetcsu1ReqVcaTrqAct = localDW->UnitDelay1_DSTATE;

        /* Transition: '<S1>:130' */
      }
    } else {
      /* Transition: '<S1>:124' */
      *rty_lcetcsu1ReqVcaTrqAct = false;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:133' */
  if (localDW->UnitDelay2_DSTATE > 25) {
    *rty_lcetcss16VehAccErrCount = 25;
  } else if (localDW->UnitDelay2_DSTATE < 0) {
    *rty_lcetcss16VehAccErrCount = 0;
  } else {
    *rty_lcetcss16VehAccErrCount = localDW->UnitDelay2_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1ReqVcaTrqAct;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16VehAccErrCount;
}

/* Model initialize function */
void LCETCS_vDctSetVca_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
