/*
 * File: LCTMC_vCalRefEngSpdMapInTrn.h
 *
 * Code generated for Simulink model 'LCTMC_vCalRefEngSpdMapInTrn'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:31 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalRefEngSpdMapInTrn_h_
#define RTW_HEADER_LCTMC_vCalRefEngSpdMapInTrn_h_
#ifndef LCTMC_vCalRefEngSpdMapInTrn_COMMON_INCLUDES_
# define LCTMC_vCalRefEngSpdMapInTrn_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCalRefEngSpdMapInTrn_COMMON_INCLUDES_ */

#include "LCTMC_vCalRefEngSpdMapInTrn_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCTMC_vCalRefEngSpdMapInTrn' */
typedef struct {
  int16_T TurnIndexLowLevel;           /* '<Root>/Turn Index Low Level' */
  int16_T TurnIndexMidLevel;           /* '<Root>/Turn Index Mid Level' */
  int16_T TurnIndexHighLevel;          /* '<Root>/Turn Index High Level' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter3Point' */
} B_LCTMC_vCalRefEngSpdMapInTrn_c_T;

typedef struct {
  B_LCTMC_vCalRefEngSpdMapInTrn_c_T rtb;
} MdlrefDW_LCTMC_vCalRefEngSpdMapInTrn_T;

/* Model reference registration function */
extern void LCTMC_vCalRefEngSpdMapInTrn_initialize(void);
extern void LCTMC_vCalRefEngSpdMapInTrn_Init(B_LCTMC_vCalRefEngSpdMapInTrn_c_T
  *localB);
extern void LCTMC_vCalRefEngSpdMapInTrn_Start(B_LCTMC_vCalRefEngSpdMapInTrn_c_T *
  localB);
extern void LCTMC_vCalRefEngSpdMapInTrn(const TypeETCSGainStruct
  *rtu_ETCS_CTL_GAINS, int16_T *rty_lctmcs16TarShtChnRPMInTrn,
  B_LCTMC_vCalRefEngSpdMapInTrn_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCalRefEngSpdMapInTrn'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCalRefEngSpdMapInTrn_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
