/*
 * File: LCETCS_vCalHmIGainFct4BigSpn.h
 *
 * Code generated for Simulink model 'LCETCS_vCalHmIGainFct4BigSpn'.
 *
 * Model version                  : 1.161
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalHmIGainFct4BigSpn_h_
#define RTW_HEADER_LCETCS_vCalHmIGainFct4BigSpn_h_
#ifndef LCETCS_vCalHmIGainFct4BigSpn_COMMON_INCLUDES_
# define LCETCS_vCalHmIGainFct4BigSpn_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalHmIGainFct4BigSpn_COMMON_INCLUDES_ */

#include "LCETCS_vCalHmIGainFct4BigSpn_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalHmIGainFct4BigSpn' */
typedef struct {
  int16_T y;                           /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T x2;                          /* '<Root>/Constant3' */
  int16_T y2;                          /* '<Root>/Constant5' */
} B_LCETCS_vCalHmIGainFct4BigSpn_c_T;

typedef struct {
  B_LCETCS_vCalHmIGainFct4BigSpn_c_T rtb;
} MdlrefDW_LCETCS_vCalHmIGainFct4BigSpn_T;

/* Model reference registration function */
extern void LCETCS_vCalHmIGainFct4BigSpn_initialize(void);
extern void LCETCS_vCalHmIGainFct4BigSpn_Init(B_LCETCS_vCalHmIGainFct4BigSpn_c_T
  *localB);
extern void LCETCS_vCalHmIGainFct4BigSpn_Start
  (B_LCETCS_vCalHmIGainFct4BigSpn_c_T *localB);
extern void LCETCS_vCalHmIGainFct4BigSpn(const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16HmIGainFct4BigSpn,
  B_LCETCS_vCalHmIGainFct4BigSpn_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalHmIGainFct4BigSpn'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalHmIGainFct4BigSpn_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
