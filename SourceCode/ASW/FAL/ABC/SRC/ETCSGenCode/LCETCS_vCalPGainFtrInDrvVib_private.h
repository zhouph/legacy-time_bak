/*
 * File: LCETCS_vCalPGainFtrInDrvVib_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFtrInDrvVib'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_private_h_
#define RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S8ETCSCpPgTransTmDrvVib
#error The variable for the parameter "S8ETCSCpPgTransTmDrvVib" is not defined
#endif

extern const int16_T rtCP_pooled_ZjYULP70HcKf;
extern const int16_T rtCP_pooled_sqsAoF3Vfmvz;

#define rtCP_percent_Value             rtCP_pooled_ZjYULP70HcKf  /* Computed Parameter: percent_Value
                                                                  * Referenced by: '<Root>/0 percent'
                                                                  */
#define rtCP_u0percent_Value           rtCP_pooled_sqsAoF3Vfmvz  /* Computed Parameter: u0percent_Value
                                                                  * Referenced by: '<Root>/100 percent'
                                                                  */
#define rtCP_Countervaluewhendrivelinevibrationisnotdetect rtCP_pooled_ZjYULP70HcKf/* Computed Parameter: Countervaluewhendrivelinevibrationisnotdetect
                                                                      * Referenced by: '<Root>/Counter value when driveline vibration is not detected'
                                                                      */
#endif                                 /* RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
