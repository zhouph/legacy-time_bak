/*
 * File: LCTMC_vCalRefTarGearMapbyGear.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefTarGearMapbyGear'.
 *
 * Model version                  : 1.243
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefTarGearMapbyGear.h"
#include "LCTMC_vCalRefTarGearMapbyGear_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalRefTarGearMapbyGear' */
void LCTMC_vCalRefTarGearMapbyGear_Init(DW_LCTMC_vCalRefTarGearMapbyGear_f_T
  *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefUpVehSpdMapbyGear' */
  LCTMC_vCalRefUpVehSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefUpVehSpdMapbyGear_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefUpEngSpdMapbyGear' */
  LCTMC_vCalRefUpEngSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefUpEngSpdMapbyGear_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefDnVehSpdMapbyGear' */
  LCTMC_vCalRefDnVehSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefDnVehSpdMapbyGear_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefDnEngSpdMapbyGear' */
  LCTMC_vCalRefDnEngSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefDnEngSpdMapbyGear_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCTMC_vCalRefTarGearMapbyGear' */
void LCTMC_vCalRefTarGearMapbyGear(const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, TypeTMCRefTarGearMap *rty_TMC_REF_TAR_GEAR_MAP,
  DW_LCTMC_vCalRefTarGearMapbyGear_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lctmcs16TarUpShtVSbyGear;
  int16_T rtb_lctmcs16TarUpShtRPMbyGear;
  int16_T rtb_lctmcs16TarDnShtVSbyGear;
  int16_T rtb_lctmcs16TarDnShtRPMbyGear;

  /* ModelReference: '<Root>/LCTMC_vCalRefUpVehSpdMapbyGear' */
  LCTMC_vCalRefUpVehSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    &rtb_lctmcs16TarUpShtVSbyGear,
    &(localDW->LCTMC_vCalRefUpVehSpdMapbyGear_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vCalRefUpEngSpdMapbyGear' */
  LCTMC_vCalRefUpEngSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    &rtb_lctmcs16TarUpShtRPMbyGear,
    &(localDW->LCTMC_vCalRefUpEngSpdMapbyGear_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vCalRefDnVehSpdMapbyGear' */
  LCTMC_vCalRefDnVehSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    &rtb_lctmcs16TarDnShtVSbyGear,
    &(localDW->LCTMC_vCalRefDnVehSpdMapbyGear_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vCalRefDnEngSpdMapbyGear' */
  LCTMC_vCalRefDnEngSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    &rtb_lctmcs16TarDnShtRPMbyGear,
    &(localDW->LCTMC_vCalRefDnEngSpdMapbyGear_DWORK1.rtb));

  /* BusCreator: '<Root>/Bus Creator2' */
  rty_TMC_REF_TAR_GEAR_MAP->lctmcs16TarUpShtVSbyGear =
    rtb_lctmcs16TarUpShtVSbyGear;
  rty_TMC_REF_TAR_GEAR_MAP->lctmcs16TarUpShtRPMbyGear =
    rtb_lctmcs16TarUpShtRPMbyGear;
  rty_TMC_REF_TAR_GEAR_MAP->lctmcs16TarDnShtVSbyGear =
    rtb_lctmcs16TarDnShtVSbyGear;
  rty_TMC_REF_TAR_GEAR_MAP->lctmcs16TarDnShtRPMbyGear =
    rtb_lctmcs16TarDnShtRPMbyGear;
}

/* Model initialize function */
void LCTMC_vCalRefTarGearMapbyGear_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefDnEngSpdMapbyGear' */
  LCTMC_vCalRefDnEngSpdMapbyGear_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefDnVehSpdMapbyGear' */
  LCTMC_vCalRefDnVehSpdMapbyGear_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefUpEngSpdMapbyGear' */
  LCTMC_vCalRefUpEngSpdMapbyGear_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefUpVehSpdMapbyGear' */
  LCTMC_vCalRefUpVehSpdMapbyGear_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
