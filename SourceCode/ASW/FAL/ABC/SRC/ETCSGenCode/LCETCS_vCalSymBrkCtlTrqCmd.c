/*
 * File: LCETCS_vCalSymBrkCtlTrqCmd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSymBrkCtlTrqCmd'.
 *
 * Model version                  : 1.146
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:37 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSymBrkCtlTrqCmd.h"
#include "LCETCS_vCalSymBrkCtlTrqCmd_private.h"

const TypeETCSBrkCtlCmdStruct
  LCETCS_vCalSymBrkCtlTrqCmd_rtZTypeETCSBrkCtlCmdStruct = {
  0,                                   /* lcetcss16SymBrkTrqCtlCmdFA */
  0,                                   /* lcetcss16SymBrkTrqCtlCmdRA */
  0                                    /* lcetcss16MaxLmtSymBrkTrq */
} ;                                    /* TypeETCSBrkCtlCmdStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vCalSymBrkCtlTrqCmd' */
void LCETCS_vCalSymBrkCtlTrqCmd_Init(DW_LCETCS_vCalSymBrkCtlTrqCmd_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE =
    LCETCS_vCalSymBrkCtlTrqCmd_rtZTypeETCSBrkCtlCmdStruct;
}

/* Output and update for referenced model: 'LCETCS_vCalSymBrkCtlTrqCmd' */
void LCETCS_vCalSymBrkCtlTrqCmd(const TypeETCSBrkCtlReqStruct
  *rtu_ETCS_BRK_CTL_REQ, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, int16_T rtu_lcetcss16MaxLmtSymBrkTrq,
  TypeETCSBrkCtlCmdStruct *rty_ETCS_BRK_CTL_CMD,
  DW_LCETCS_vCalSymBrkCtlTrqCmd_f_T *localDW)
{
  int16_T rtb_lcetcss16SymBrkTrqCtlCmd;
  int32_T tmp;
  boolean_T tmp_0;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Switch: '<Root>/Switch' incorporates:
     *  Constant: '<Root>/Constant1'
     */
    /* Transition: '<S1>:11' */
    /* Transition: '<S1>:10' */
    if (((boolean_T)VarFwd)) {
      tmp_0 = rtu_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqFA;
    } else {
      tmp_0 = rtu_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqRA;
    }

    /* End of Switch: '<Root>/Switch' */
    if (tmp_0 == 1) {
      /* Transition: '<S1>:24' */
      /* Transition: '<S1>:88' */
      if (rtu_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc >= 0) {
        /* Transition: '<S1>:79' */
        /* Transition: '<S1>:78' */
        rtb_lcetcss16SymBrkTrqCtlCmd = rtu_lcetcss16MaxLmtSymBrkTrq;

        /* Transition: '<S1>:87' */
      } else {
        /* Switch: '<Root>/Switch1' incorporates:
         *  Constant: '<Root>/Constant2'
         *  UnitDelay: '<Root>/Unit Delay2'
         */
        /* Transition: '<S1>:76' */
        /*   */
        if (((boolean_T)VarFwd)) {
          rtb_lcetcss16SymBrkTrqCtlCmd =
            localDW->UnitDelay2_DSTATE.lcetcss16SymBrkTrqCtlCmdFA;
        } else {
          rtb_lcetcss16SymBrkTrqCtlCmd =
            localDW->UnitDelay2_DSTATE.lcetcss16SymBrkTrqCtlCmdRA;
        }

        /* End of Switch: '<Root>/Switch1' */
        tmp = rtb_lcetcss16SymBrkTrqCtlCmd - ((uint8_T)U8ETCSCpSymBrkTrqDecRate);
        if (tmp < -32768) {
          tmp = -32768;
        }

        rtb_lcetcss16SymBrkTrqCtlCmd = (int16_T)tmp;
      }

      /* Transition: '<S1>:82' */
    } else {
      /* Transition: '<S1>:40' */
      rtb_lcetcss16SymBrkTrqCtlCmd = 0;
    }

    /* Transition: '<S1>:47' */
  } else {
    /* Transition: '<S1>:8' */
    rtb_lcetcss16SymBrkTrqCtlCmd = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:49' */
  if (rtb_lcetcss16SymBrkTrqCtlCmd > 2000) {
    rtb_lcetcss16SymBrkTrqCtlCmd = 2000;
  } else {
    if (rtb_lcetcss16SymBrkTrqCtlCmd < 0) {
      rtb_lcetcss16SymBrkTrqCtlCmd = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Switch: '<Root>/Switch5' incorporates:
   *  Constant: '<Root>/Constant6'
   *  Constant: '<Root>/Constant7'
   */
  if (((boolean_T)VarFwd)) {
    rty_ETCS_BRK_CTL_CMD->lcetcss16SymBrkTrqCtlCmdFA =
      rtb_lcetcss16SymBrkTrqCtlCmd;
  } else {
    rty_ETCS_BRK_CTL_CMD->lcetcss16SymBrkTrqCtlCmdFA = 0;
  }

  /* End of Switch: '<Root>/Switch5' */

  /* Switch: '<Root>/Switch6' incorporates:
   *  Constant: '<Root>/Constant8'
   *  Constant: '<Root>/Constant9'
   */
  if (((boolean_T)VarFwd)) {
    rtb_lcetcss16SymBrkTrqCtlCmd = 0;
  }

  /* End of Switch: '<Root>/Switch6' */

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_BRK_CTL_CMD->lcetcss16SymBrkTrqCtlCmdRA =
    rtb_lcetcss16SymBrkTrqCtlCmd;
  rty_ETCS_BRK_CTL_CMD->lcetcss16MaxLmtSymBrkTrq = rtu_lcetcss16MaxLmtSymBrkTrq;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_ETCS_BRK_CTL_CMD;
}

/* Model initialize function */
void LCETCS_vCalSymBrkCtlTrqCmd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
