/*
 * File: LCETCS_vDctFallingEdge.c
 *
 * Code generated for Simulink model 'LCETCS_vDctFallingEdge'.
 *
 * Model version                  : 1.72
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctFallingEdge.h"
#include "LCETCS_vDctFallingEdge_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctFallingEdge' */
void LCETCS_vDctFallingEdge_Init(boolean_T *rty_FallingEdge,
  DW_LCETCS_vDctFallingEdge_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_FallingEdge = false;
}

/* Output and update for referenced model: 'LCETCS_vDctFallingEdge' */
void LCETCS_vDctFallingEdge(boolean_T rtu_Flag, boolean_T *rty_FallingEdge,
  DW_LCETCS_vDctFallingEdge_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:11' */
  /* comment */
  if ((rtu_Flag == 0) && (localDW->UnitDelay1_DSTATE == 1)) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    *rty_FallingEdge = true;

    /* Transition: '<S1>:8' */
  } else {
    /* Transition: '<S1>:7' */
    *rty_FallingEdge = false;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:6' */
  localDW->UnitDelay1_DSTATE = rtu_Flag;
}

/* Model initialize function */
void LCETCS_vDctFallingEdge_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
