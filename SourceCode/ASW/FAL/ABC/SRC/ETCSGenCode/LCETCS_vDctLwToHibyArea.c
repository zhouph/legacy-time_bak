/*
 * File: LCETCS_vDctLwToHibyArea.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToHibyArea'.
 *
 * Model version                  : 1.188
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwToHibyArea.h"
#include "LCETCS_vDctLwToHibyArea_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwToHibyArea' */
void LCETCS_vDctLwToHibyArea_Init(B_LCETCS_vDctLwToHibyArea_c_T *localB,
  DW_LCETCS_vDctLwToHibyArea_f_T *localDW)
{
  /* InitializeConditions for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE[0] = 0;
  localDW->Delay3_DSTATE[1] = 0;
  localDW->CircBufIdx = 0U;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcss16LwToHiCntbyArea = 0;
  localB->lcetcss16SumOfWhlSpin = 0;
}

/* Output and update for referenced model: 'LCETCS_vDctLwToHibyArea' */
void LCETCS_vDctLwToHibyArea(const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, int16_T *rty_lcetcss16LwToHiCntbyArea,
  B_LCETCS_vDctLwToHibyArea_c_T *localB, DW_LCETCS_vDctLwToHibyArea_f_T *localDW)
{
  int16_T rtb_Add1;
  int32_T tmp;
  int32_T tmp_0;

  /* Sum: '<Root>/Add1' incorporates:
   *  Delay: '<Root>/Delay3'
   */
  rtb_Add1 = (int16_T)(rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt -
                       localDW->Delay3_DSTATE[localDW->CircBufIdx]);

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   *  UnitDelay: '<Root>/Unit Delay3'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:225' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:120' */
    /* Transition: '<S1>:122' */
    /* 250=5sec, 16000=30kph(240) for 2sec */
    if ((localDW->UnitDelay1_DSTATE < 250) && (localB->lcetcss16SumOfWhlSpin <
         24000)) {
      /* Transition: '<S1>:121' */
      /* Transition: '<S1>:201' */
      tmp_0 = localDW->UnitDelay1_DSTATE + 1;
      if (tmp_0 > 32767) {
        tmp_0 = 32767;
      }

      localDW->UnitDelay1_DSTATE = (int16_T)tmp_0;
      tmp = localB->lcetcss16SumOfWhlSpin +
        rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt;
      if (tmp > 32767) {
        tmp = 32767;
      } else {
        if (tmp < -32768) {
          tmp = -32768;
        }
      }

      localB->lcetcss16SumOfWhlSpin = (int16_T)tmp;
      tmp_0 = localB->lcetcss16SumOfWhlSpin / ((int16_T)tmp_0);
      if (tmp_0 > 32767) {
        tmp_0 = 32767;
      } else {
        if (tmp_0 < -32768) {
          tmp_0 = -32768;
        }
      }

      if (((tmp_0 < ((uint8_T)U8ETCSCpAreaSetLwToHiSpin)) &&
           (rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec == 0)) &&
          ((rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt < ((uint8_T)
             U8ETCSCpAreaSetLwToHiSpin)) || (rtb_Add1 < 0))) {
        /* Transition: '<S1>:181' */
        /* Transition: '<S1>:183' */
        tmp_0 = localDW->UnitDelay3_DSTATE + 1;
        if (tmp_0 > 32767) {
          tmp_0 = 32767;
        }

        localB->lcetcss16LwToHiCntbyArea = (int16_T)tmp_0;

        /* Transition: '<S1>:185' */
      } else {
        /* Transition: '<S1>:186' */
        tmp_0 = localDW->UnitDelay3_DSTATE - 1;
        if (tmp_0 < -32768) {
          tmp_0 = -32768;
        }

        localB->lcetcss16LwToHiCntbyArea = (int16_T)tmp_0;
      }

      /* Transition: '<S1>:216' */
      /* Transition: '<S1>:219' */
      /* Transition: '<S1>:220' */
    } else {
      /* Transition: '<S1>:207' */
      if (localB->lcetcss16LwToHiCntbyArea > 0) {
        /* Transition: '<S1>:209' */
        /* Transition: '<S1>:211' */
        if ((rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec == 0) &&
            ((rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt < ((uint8_T)
               U8ETCSCpAreaSetLwToHiSpin)) || (rtb_Add1 < 0))) {
          /* Transition: '<S1>:213' */
          /* Transition: '<S1>:215' */
          localB->lcetcss16LwToHiCntbyArea = localDW->UnitDelay3_DSTATE;

          /* Transition: '<S1>:219' */
          /* Transition: '<S1>:220' */
        } else {
          /* Transition: '<S1>:218' */
          tmp_0 = localDW->UnitDelay3_DSTATE - 1;
          if (tmp_0 < -32768) {
            tmp_0 = -32768;
          }

          localB->lcetcss16LwToHiCntbyArea = (int16_T)tmp_0;

          /* Transition: '<S1>:220' */
        }
      } else {
        /* Transition: '<S1>:221' */
        localB->lcetcss16LwToHiCntbyArea = 0;
        localDW->UnitDelay1_DSTATE = 0;
        localB->lcetcss16SumOfWhlSpin = 0;
      }
    }

    /* Transition: '<S1>:188' */
  } else {
    /* Transition: '<S1>:118' */
    localB->lcetcss16LwToHiCntbyArea = 0;
    localDW->UnitDelay1_DSTATE = 0;
    localB->lcetcss16SumOfWhlSpin = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:126' */
  if (localB->lcetcss16LwToHiCntbyArea > ((uint8_T)U8ETCSCpLwToHiDetAreaCnt)) {
    *rty_lcetcss16LwToHiCntbyArea = ((uint8_T)U8ETCSCpLwToHiDetAreaCnt);
  } else if (localB->lcetcss16LwToHiCntbyArea < 0) {
    *rty_lcetcss16LwToHiCntbyArea = 0;
  } else {
    *rty_lcetcss16LwToHiCntbyArea = localB->lcetcss16LwToHiCntbyArea;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE[localDW->CircBufIdx] =
    rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt;
  if (localDW->CircBufIdx < 1U) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay3' */

  /* Update for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = *rty_lcetcss16LwToHiCntbyArea;
}

/* Model initialize function */
void LCETCS_vDctLwToHibyArea_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
