/*
 * File: LCETCS_vDctInhCndGradient.c
 *
 * Code generated for Simulink model 'LCETCS_vDctInhCndGradient'.
 *
 * Model version                  : 1.305
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctInhCndGradient.h"
#include "LCETCS_vDctInhCndGradient_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctInhCndGradient' */
void LCETCS_vDctInhCndGradient_Init(boolean_T *rty_lcetcsu1InhibitGradient,
  DW_LCETCS_vDctInhCndGradient_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1InhibitGradient = false;
}

/* Output and update for referenced model: 'LCETCS_vDctInhCndGradient' */
void LCETCS_vDctInhCndGradient(int16_T rtu_lcetcss16VehSpd, boolean_T
  *rty_lcetcsu1InhibitGradient, DW_LCETCS_vDctInhCndGradient_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_lcetcss16VehSpd >= ((uint8_T)VREF_30_KPH)) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    *rty_lcetcsu1InhibitGradient = true;

    /* Transition: '<S1>:187' */
    /* Transition: '<S1>:189' */
  } else {
    /* Transition: '<S1>:173' */
    if (rtu_lcetcss16VehSpd < ((int16_T)VREF_25_KPH)) {
      /* Transition: '<S1>:184' */
      /* Transition: '<S1>:186' */
      *rty_lcetcsu1InhibitGradient = false;

      /* Transition: '<S1>:189' */
    } else {
      /* Transition: '<S1>:188' */
      *rty_lcetcsu1InhibitGradient = localDW->UnitDelay1_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:191' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1InhibitGradient;
}

/* Model initialize function */
void LCETCS_vDctInhCndGradient_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
