/*
 * File: LCETCS_vCalPGainIncFacH2L.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainIncFacH2L'.
 *
 * Model version                  : 1.177
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:36 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPGainIncFacH2L.h"
#include "LCETCS_vCalPGainIncFacH2L_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalPGainIncFacH2L' */
void LCETCS_vCalPGainIncFacH2L_Init(B_LCETCS_vCalPGainIncFacH2L_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vCalPGainIncFacH2L' */
void LCETCS_vCalPGainIncFacH2L_Start(B_LCETCS_vCalPGainIncFacH2L_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpH2LDctHldTm);

  /* Start for Constant: '<Root>/Constant3' */
  localB->y1 = ((uint8_T)U8ETCSCpPGainIncFacH2L);
}

/* Output and update for referenced model: 'LCETCS_vCalPGainIncFacH2L' */
void LCETCS_vCalPGainIncFacH2L(const TypeETCSH2LStruct *rtu_ETCS_H2L, int16_T
  *rty_lcetcss16PGainIncFacH2L, B_LCETCS_vCalPGainIncFacH2L_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;

  /* Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpH2LDctHldTm);

  /* Constant: '<Root>/Constant3' */
  localB->y1 = ((uint8_T)U8ETCSCpPGainIncFacH2L);

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  rtb_x = rtu_ETCS_H2L->lcetcss8HiToLwHoldCnt;

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtCP_Constant1_Value, localB->x2, localB->y1,
                        rtCP_Constant4_Value, &localB->y);

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant5'
   */
  if (rtu_ETCS_H2L->lcetcsu1HiToLw) {
    *rty_lcetcss16PGainIncFacH2L = localB->y;
  } else {
    *rty_lcetcss16PGainIncFacH2L = 100;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16PGainIncFacH2L) > 255) {
    *rty_lcetcss16PGainIncFacH2L = 255;
  } else {
    if ((*rty_lcetcss16PGainIncFacH2L) < 100) {
      *rty_lcetcss16PGainIncFacH2L = 100;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalPGainIncFacH2L_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
