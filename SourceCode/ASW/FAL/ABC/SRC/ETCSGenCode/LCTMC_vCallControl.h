/*
 * File: LCTMC_vCallControl.h
 *
 * Code generated for Simulink model 'LCTMC_vCallControl'.
 *
 * Model version                  : 1.389
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCallControl_h_
#define RTW_HEADER_LCTMC_vCallControl_h_
#ifndef LCTMC_vCallControl_COMMON_INCLUDES_
# define LCTMC_vCallControl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCallControl_COMMON_INCLUDES_ */

#include "LCTMC_vCallControl_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vCtrlTargetGear.h"
#include "LCTMC_vCheckGearStatus.h"
#include "LCTMC_vCalReferTarGearMap.h"

/* Block signals for model 'LCTMC_vCallControl' */
typedef struct {
  boolean_T lctmcu1GearShiftCountSet;  /* '<Root>/LCTMC_vCheckGearStatus' */
  boolean_T lctmcu1NonCndTarReqGear;   /* '<Root>/LCTMC_vCheckGearStatus' */
} B_LCTMC_vCallControl_c_T;

/* Block states (auto storage) for model 'LCTMC_vCallControl' */
typedef struct {
  MdlrefDW_LCTMC_vCheckGearStatus_T LCTMC_vCheckGearStatus_DWORK1;/* '<Root>/LCTMC_vCheckGearStatus' */
  MdlrefDW_LCTMC_vCalReferTarGearMap_T LCTMC_vCalReferTarGearMap_DWORK1;/* '<Root>/LCTMC_vCalReferTarGearMap' */
  MdlrefDW_LCTMC_vCtrlTargetGear_T LCTMC_vCtrlTargetGear_DWORK1;/* '<Root>/LCTMC_vCtrlTargetGear' */
} DW_LCTMC_vCallControl_f_T;

typedef struct {
  B_LCTMC_vCallControl_c_T rtb;
  DW_LCTMC_vCallControl_f_T rtdw;
} MdlrefDW_LCTMC_vCallControl_T;

/* Model reference registration function */
extern void LCTMC_vCallControl_initialize(const rtTimingBridge *timingBridge);
extern void LCTMC_vCallControl_Init(TypeTMCReqGearStruct
  *rty_TMC_REQ_GEAR_STRUCT, B_LCTMC_vCallControl_c_T *localB,
  DW_LCTMC_vCallControl_f_T *localDW);
extern void LCTMC_vCallControl_Start(DW_LCTMC_vCallControl_f_T *localDW);
extern void LCTMC_vCallControl(const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS,
  const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL, boolean_T rtu_lcu1TmcInhibitMode,
  const TypeTMCReqGearStruct *rtu_TMC_REQ_GEAR_STRUCT_OLD, TypeTMCReqGearStruct *
  rty_TMC_REQ_GEAR_STRUCT, B_LCTMC_vCallControl_c_T *localB,
  DW_LCTMC_vCallControl_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCallControl'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCallControl_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
