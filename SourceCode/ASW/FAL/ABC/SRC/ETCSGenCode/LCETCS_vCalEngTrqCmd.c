/*
 * File: LCETCS_vCalEngTrqCmd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngTrqCmd'.
 *
 * Model version                  : 1.76
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:46:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngTrqCmd.h"
#include "LCETCS_vCalEngTrqCmd_private.h"

/* Output and update for referenced model: 'LCETCS_vCalEngTrqCmd' */
void LCETCS_vCalEngTrqCmd(const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, TypeETCSEngCmdStruct *rty_ETCS_ENG_CMD)
{
  int32_T u0;

  /* Product: '<Root>/Divide' incorporates:
   *  Gain: '<Root>/Gain'
   */
  u0 = (100 * rtu_ETCS_CTL_CMD->lcetcss32CardanTrqCmd) /
    rtu_ETCS_DRV_MDL->lcetcss16TrqRatio;

  /* Saturate: '<Root>/Saturation' */
  if (u0 > 15000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmd = 15000;
  } else if (u0 < -10000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmd = -10000;
  } else {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmd = (int16_T)u0;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Product: '<Root>/Divide1' incorporates:
   *  Gain: '<Root>/Gain1'
   */
  u0 = (100 * rtu_ETCS_CTL_CMD->lcetcss32PCtlPor) /
    rtu_ETCS_DRV_MDL->lcetcss16TrqRatio;

  /* Saturate: '<Root>/Saturation1' */
  if (u0 > 15000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdPPor = 15000;
  } else if (u0 < -10000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdPPor = -10000;
  } else {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdPPor = (int16_T)u0;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Product: '<Root>/Divide2' incorporates:
   *  Gain: '<Root>/Gain2'
   */
  u0 = (100 * rtu_ETCS_CTL_CMD->lcetcss32ICtlPor) /
    rtu_ETCS_DRV_MDL->lcetcss16TrqRatio;

  /* Saturate: '<Root>/Saturation2' */
  if (u0 > 15000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdIPor = 15000;
  } else if (u0 < -10000) {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdIPor = -10000;
  } else {
    rty_ETCS_ENG_CMD->lcetcss16EngTrqCmdIPor = (int16_T)u0;
  }

  /* End of Saturate: '<Root>/Saturation2' */
}

/* Model initialize function */
void LCETCS_vCalEngTrqCmd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
