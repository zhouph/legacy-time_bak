/*
 * File: LCETCS_vCalRefTrq4IValRst.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrq4IValRst'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalRefTrq4IValRst_h_
#define RTW_HEADER_LCETCS_vCalRefTrq4IValRst_h_
#ifndef LCETCS_vCalRefTrq4IValRst_COMMON_INCLUDES_
# define LCETCS_vCalRefTrq4IValRst_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalRefTrq4IValRst_COMMON_INCLUDES_ */

#include "LCETCS_vCalRefTrq4IValRst_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalVehAccAvgIn1stCyl.h"
#include "LCETCS_vCalRefTrqAtStrtOfCtl.h"
#include "LCETCS_vCalRefTrq2Jmp.h"
#include "LCETCS_vCalJmpUpDnFtr.h"

/* Block signals for model 'LCETCS_vCalRefTrq4IValRst' */
typedef struct {
  int32_T ETCS_REF_TRQ2JMP;            /* '<Root>/LCETCS_vCalRefTrq2Jmp' */
  int16_T lcetcss16RefTrqSetTm;        /* '<Root>/LCETCS_vCalRefTrq2Jmp' */
  boolean_T lcetcsu1RefTrq4JmpSetOk;   /* '<Root>/LCETCS_vCalRefTrq2Jmp' */
} B_LCETCS_vCalRefTrq4IValRst_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalRefTrq4IValRst' */
typedef struct {
  TypeETCSRefTrq2JmpStruct UnitDelay_DSTATE;/* '<Root>/Unit Delay' */
  MdlrefDW_LCETCS_vCalJmpUpDnFtr_T LCETCS_vCalJmpUpDnFtr_DWORK1;/* '<Root>/LCETCS_vCalJmpUpDnFtr' */
  MdlrefDW_LCETCS_vCalVehAccAvgIn1stCyl_T LCETCS_vCalVehAccAvgIn1stCyl_DWORK1;/* '<Root>/LCETCS_vCalVehAccAvgIn1stCyl' */
} DW_LCETCS_vCalRefTrq4IValRst_f_T;

typedef struct {
  B_LCETCS_vCalRefTrq4IValRst_c_T rtb;
  DW_LCETCS_vCalRefTrq4IValRst_f_T rtdw;
} MdlrefDW_LCETCS_vCalRefTrq4IValRst_T;

/* Model reference registration function */
extern void LCETCS_vCalRefTrq4IValRst_initialize(void);
extern const TypeETCSRefTrq2JmpStruct
  LCETCS_vCalRefTrq4IValRst_rtZTypeETCSRefTrq2JmpStruct;/* TypeETCSRefTrq2JmpStruct ground */
extern void LCETCS_vCalRefTrq4IValRst_Init(B_LCETCS_vCalRefTrq4IValRst_c_T
  *localB, DW_LCETCS_vCalRefTrq4IValRst_f_T *localDW);
extern void LCETCS_vCalRefTrq4IValRst_Start(DW_LCETCS_vCalRefTrq4IValRst_f_T
  *localDW);
extern void LCETCS_vCalRefTrq4IValRst(const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, TypeETCSRefTrqStrCtlStruct
  *rty_ETCS_REF_TRQ_STR_CTL, TypeETCSRefTrqStr2CylStruct
  *rty_ETCS_REF_TRQ_STR2CYL, TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP,
  B_LCETCS_vCalRefTrq4IValRst_c_T *localB, DW_LCETCS_vCalRefTrq4IValRst_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalRefTrq4IValRst'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalRefTrq4IValRst_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
