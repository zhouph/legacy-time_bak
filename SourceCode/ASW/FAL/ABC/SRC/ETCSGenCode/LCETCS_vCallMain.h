/*
 * File: LCETCS_vCallMain.h
 *
 * Code generated for Simulink model 'LCETCS_vCallMain'.
 *
 * Model version                  : 1.472
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:22:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallMain_h_
#define RTW_HEADER_LCETCS_vCallMain_h_
#include <stddef.h>
#ifndef LCETCS_vCallMain_COMMON_INCLUDES_
# define LCETCS_vCallMain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCallMain_COMMON_INCLUDES_ */

#include "LCETCS_vCallMain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vInterfaceInpSig.h"
#include "FctEtcsCore.h"

/* Macros for accessing real-time model data structure */

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  MdlrefDW_FctEtcsCore_T Calculation1_DWORK1;/* '<Root>/Calculation1' */
} DW_LCETCS_vCallMain_T;

/* Real-time Model Data Structure */
struct tag_RTM_LCETCS_vCallMain_T {
  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    boolean_T firstInitCondFlag;
  } Timing;
};

/* Block states (auto storage) */
extern DW_LCETCS_vCallMain_T LCETCS_vCallMain_DW;

/*
 * Exported Global Signals
 *
 * Note: Exported global signals are block signals with an exported global
 * storage class designation.  Code generation will declare the memory for
 * these signals and export their symbols.
 *
 */
extern TypeETCSGainStruct ETCS_CTL_GAINS;/* '<Root>/Calculation1' */
extern TypeETCSTarSpinStruct ETCS_TAR_SPIN;/* '<Root>/Calculation1' */
extern TypeETCSIValRstStruct ETCS_IVAL_RST;/* '<Root>/Calculation1' */
extern TypeETCSEscSigStruct ETCS_ESC_SIG;/* '<Root>/Interface' */
extern TypeETCSCtlCmdStruct ETCS_CTL_CMD;/* '<Root>/Calculation1' */
extern TypeETCSSplitHillStruct ETCS_SPLIT_HILL;/* '<Root>/Calculation1' */
extern TypeETCSVehAccStruct ETCS_VEH_ACC;/* '<Root>/Calculation1' */
extern TypeETCSLdTrqStruct ETCS_LD_TRQ;/* '<Root>/Calculation1' */
extern TypeETCSWhlStruct ETCS_FL;      /* '<Root>/Interface' */
extern TypeETCSWhlStruct ETCS_FR;      /* '<Root>/Interface' */
extern TypeETCSWhlStruct ETCS_RL;      /* '<Root>/Interface' */
extern TypeETCSWhlStruct ETCS_RR;      /* '<Root>/Interface' */
extern TypeETCSExtDctStruct ETCS_EXT_DCT;/* '<Root>/Interface' */
extern TypeETCSDrvMdlStruct ETCS_DRV_MDL;/* '<Root>/Calculation1' */
extern TypeETCSCrdnTrqStruct ETCS_CRDN_TRQ;/* '<Root>/Calculation1' */
extern TypeETCSWhlSpinStruct ETCS_WHL_SPIN;/* '<Root>/Calculation1' */
extern TypeETCSCtlActStruct ETCS_CTL_ACT;/* '<Root>/Calculation1' */
extern TypeETCSRefTrqStr2CylStruct ETCS_REF_TRQ_STR2CYL;/* '<Root>/Calculation1' */
extern TypeETCSRefTrq2JmpStruct ETCS_REF_TRQ2JMP;/* '<Root>/Calculation1' */
extern TypeETCSTrq4RdFricStruct ETCS_TRQ_REP_RD_FRIC;/* '<Root>/Calculation1' */
extern TypeETCSLwToSpltStruct ETCS_L2SPLT;/* '<Root>/Calculation1' */
extern TypeETCSGearStruct ETCS_GEAR_STRUCT;/* '<Root>/Interface' */
extern TypeETCSEngStruct ETCS_ENG_STRUCT;/* '<Root>/Interface' */
extern TypeETCSAxlStruct ETCS_FA;      /* '<Root>/Interface' */
extern TypeETCSAxlStruct ETCS_RA;      /* '<Root>/Interface' */
extern TypeETCS4WDSigStruct ETCS_4WD_SIG;/* '<Root>/Interface' */
extern TypeETCSCirStruct ETCS_PC;      /* '<Root>/Interface' */
extern TypeETCSCirStruct ETCS_SC;      /* '<Root>/Interface' */
extern TypeETCSBrkSigStruct ETCS_BRK_SIG;/* '<Root>/Interface' */
extern TypeETCSCtlErrStruct ETCS_CTL_ERR;/* '<Root>/Calculation1' */
extern TypeETCSEngMdlStruct ETCS_ENG_MDL;/* '<Root>/Calculation1' */
extern TypeETCSAxlAccStruct ETCS_AXL_ACC;/* '<Root>/Calculation1' */
extern TypeETCSStrStatStruct ETCS_STR_STAT;/* '<Root>/Calculation1' */
extern TypeETCSCtrlModeStruct ETCS_CTRL_MODE;/* '<Root>/Calculation1' */
extern TypeETCSCyl1stStruct ETCS_CYL_1ST;/* '<Root>/Calculation1' */
extern TypeETCSErrZroCrsStruct ETCS_ERR_ZRO_CRS;/* '<Root>/Calculation1' */
extern TypeETCSRefTrqStrCtlStruct ETCS_REF_TRQ_STR_CTL;/* '<Root>/Calculation1' */
extern TypeETCSLowWhlSpnStruct ETCS_LOW_WHL_SPN;/* '<Root>/Calculation1' */
extern TypeETCSDepSnwStruct ETCS_DEP_SNW;/* '<Root>/Calculation1' */
extern TypeETCSH2LStruct ETCS_H2L;     /* '<Root>/Calculation1' */
extern TypeETCSGainGearShftStruct ETCS_GAIN_GEAR_SHFT;/* '<Root>/Calculation1' */
extern TypeETCSBrkCtlReqStruct ETCS_BRK_CTL_REQ;/* '<Root>/Calculation1' */
extern TypeETCSEngStallStruct ETCS_ENG_STALL;/* '<Root>/Calculation1' */
extern TypeTMCReqGearStruct TMC_REQ_GEAR_STRUCT;/* '<Root>/Calculation1' */
extern TypeETCSBrkCtlCmdStruct ETCS_BRK_CTL_CMD;/* '<Root>/Calculation1' */
extern TypeETCSWhlAccStruct ETCS_WHL_ACC;/* '<Root>/Calculation1' */
extern TypeETCSHighDctStruct ETCS_HI_DCT;/* '<Root>/Calculation1' */
extern TypeETCSReqVCAStruct ETCS_REQ_VCA;/* '<Root>/Calculation1' */
extern TypeETCSEngCmdStruct ETCS_ENG_CMD;/* '<Root>/Calculation1' */
extern TypeETCSAftLmtCrng ETCS_AFT_TURN;/* '<Root>/Calculation1' */
extern int16_T lcetcss16VehSpd;        /* '<Root>/Interface' */
extern int16_T lcetcss16SymBrkCtlStrtTh;/* '<Root>/Calculation1' */
extern boolean_T lcetcsu1VehVibDct;    /* '<Root>/Calculation1' */
extern boolean_T lcetcsu1FuncLampOn;   /* '<Root>/Calculation1' */

/* Model entry point functions */
extern void LCETCS_vCallMain_initialize(void);
extern void LCETCS_vCallMain_step(void);

/* Real-time Model object */
extern RT_MODEL_LCETCS_vCallMain_T *const LCETCS_vCallMain_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCallMain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCallMain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
