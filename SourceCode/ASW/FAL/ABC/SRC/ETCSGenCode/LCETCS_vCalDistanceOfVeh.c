/*
 * File: LCETCS_vCalDistanceOfVeh.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDistanceOfVeh'.
 *
 * Model version                  : 1.372
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDistanceOfVeh.h"
#include "LCETCS_vCalDistanceOfVeh_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDistanceOfVeh' */
void LCETCS_vCalDistanceOfVeh_Init(DW_LCETCS_vCalDistanceOfVeh_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalDistanceOfVeh' */
void LCETCS_vCalDistanceOfVeh(int16_T rtu_lcetcss16VehSpd, int16_T
  *rty_lcetcss16DistVehMovRd, DW_LCETCS_vCalDistanceOfVeh_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if ((rtu_lcetcss16VehSpd > ((int16_T)VREF_0_5_KPH)) && (rtu_lcetcss16VehSpd <=
       ((uint8_T)VREF_30_KPH))) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    /* 4=80ms */
    if (localDW->UnitDelay1_DSTATE < 4) {
      /* Transition: '<S1>:100' */
      /* Transition: '<S1>:102' */
      tmp = localDW->UnitDelay1_DSTATE + 1;
      if (tmp > 127) {
        tmp = 127;
      }

      localDW->UnitDelay1_DSTATE = (int8_T)tmp;

      /* Transition: '<S1>:143' */
    } else {
      /* Transition: '<S1>:110' */
      localDW->UnitDelay1_DSTATE = 0;

      /* kph to mm/10scan = 1000000/(3600*8*10) = 7/2 */
      tmp = ((rtu_lcetcss16VehSpd * 7) / 2) + localDW->UnitDelay2_DSTATE;
      if (tmp > 32767) {
        tmp = 32767;
      }

      localDW->UnitDelay2_DSTATE = (int16_T)tmp;
    }

    /* Transition: '<S1>:174' */
  } else {
    /* Transition: '<S1>:173' */
    localDW->UnitDelay1_DSTATE = 0;
    localDW->UnitDelay2_DSTATE = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:176' */
  if (localDW->UnitDelay2_DSTATE > 3000) {
    *rty_lcetcss16DistVehMovRd = 3000;
  } else if (localDW->UnitDelay2_DSTATE < 0) {
    *rty_lcetcss16DistVehMovRd = 0;
  } else {
    *rty_lcetcss16DistVehMovRd = localDW->UnitDelay2_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16DistVehMovRd;
}

/* Model initialize function */
void LCETCS_vCalDistanceOfVeh_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
