/*
 * const_params.c
 *
 * Code generation for model "LCETCS_vCallControl".
 *
 * Model version              : 1.260
 * Simulink Coder version : 8.7 (R2014b) 08-Sep-2014
 * C source code generated on : Wed Jul 29 18:16:07 2015
 */
#include "rtwtypes.h"

extern const int16_T rtCP_pooled_5VZoubGQDuYp;
const int16_T rtCP_pooled_5VZoubGQDuYp = 3;
extern const int16_T rtCP_pooled_9AzLBbdxaNKp;
const int16_T rtCP_pooled_9AzLBbdxaNKp = -20;
extern const int16_T rtCP_pooled_DVQxxQvdoG06;
const int16_T rtCP_pooled_DVQxxQvdoG06 = -40;
extern const boolean_T rtCP_pooled_DW8BfQUitOnV;
const boolean_T rtCP_pooled_DW8BfQUitOnV = 0;
extern const int16_T rtCP_pooled_Krm7oDVfoe0C;
const int16_T rtCP_pooled_Krm7oDVfoe0C = 1;
extern const int16_T rtCP_pooled_T1tcrUJCQflL;
const int16_T rtCP_pooled_T1tcrUJCQflL = 2;
extern const int16_T rtCP_pooled_ZjYULP70HcKf;
const int16_T rtCP_pooled_ZjYULP70HcKf = 0;
extern const int16_T rtCP_pooled_cQQw9dgYpywR;
const int16_T rtCP_pooled_cQQw9dgYpywR = -10;
extern const int16_T rtCP_pooled_gP6uynpoCzWv;
const int16_T rtCP_pooled_gP6uynpoCzWv = -2;
extern const int16_T rtCP_pooled_sqsAoF3Vfmvz;
const int16_T rtCP_pooled_sqsAoF3Vfmvz = 100;
extern const int16_T rtCP_pooled_xANZMqcfV9JD;
const int16_T rtCP_pooled_xANZMqcfV9JD = -1;
