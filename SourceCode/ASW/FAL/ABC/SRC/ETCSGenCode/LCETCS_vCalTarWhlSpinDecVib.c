/*
 * File: LCETCS_vCalTarWhlSpinDecVib.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpinDecVib'.
 *
 * Model version                  : 1.59
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:22 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpinDecVib.h"
#include "LCETCS_vCalTarWhlSpinDecVib_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpinDecVib' */
void LCETCS_vCalTarWhlSpinDecVib(boolean_T rtu_lcetcsu1VehVibDctOld, int16_T
  *rty_lcetcss16TarWhlSpnDecVib)
{
  /* Switch: '<Root>/Switch2' incorporates:
   *  Constant: '<Root>/Constant7'
   *  Constant: '<Root>/Constant9'
   */
  if (rtu_lcetcsu1VehVibDctOld) {
    *rty_lcetcss16TarWhlSpnDecVib = ((int8_T)S8ETCSCpTarSpnDec4Vib);
  } else {
    *rty_lcetcss16TarWhlSpnDecVib = 0;
  }

  /* End of Switch: '<Root>/Switch2' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpinDecVib_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
