/*
 * File: LCETCS_vCalPGainFacInGearChng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFacInGearChng'.
 *
 * Model version                  : 1.177
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPGainFacInGearChng.h"
#include "LCETCS_vCalPGainFacInGearChng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalPGainFacInGearChng' */
void LCETCS_vCalPGainFacInGearChng_Init(int16_T *rty_lcetcss16PgainFacInGearChng)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16PgainFacInGearChng);
}

/* Start for referenced model: 'LCETCS_vCalPGainFacInGearChng' */
void LCETCS_vCalPGainFacInGearChng_Start(B_LCETCS_vCalPGainFacInGearChng_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant1' */
  localB->x2 = ((uint8_T)U8ETCSCpGainTrnsTmAftrGearShft);

  /* Start for IfAction SubSystem: '<Root>/SetPgainFacInGearChang' */
  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
  /* Start for Constant: '<S3>/Pgain Variation Factor In Gear Change With Negative Control Error at 5th gear' */
  localB->y1 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_5);

  /* Start for Constant: '<S3>/Pgain Variation Factor In Gear Change With Negative Control Error at 4th gear' */
  localB->y2 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_4);

  /* Start for Constant: '<S3>/Pgain Variation Factor In Gear Change With Negative Control Error at 3rd gear' */
  localB->y3 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_3);

  /* Start for Constant: '<S3>/Pgain Variation Factor In Gear Change With Negative Control Error at 2nd gear' */
  localB->y4 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_2);

  /* Start for Constant: '<S3>/Pgain Variation Factor In Gear Change With Negative Control Error at 1st gear' */
  localB->y5 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_1);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for ModelReference: '<S3>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->lcetcss16PgainFacInGearChng);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* End of Start for SubSystem: '<Root>/SetPgainFacInGearChang' */
}

/* Output and update for referenced model: 'LCETCS_vCalPGainFacInGearChng' */
void LCETCS_vCalPGainFacInGearChng(const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSGainGearShftStruct
  *rtu_ETCS_GAIN_GEAR_SHFT, int16_T *rty_lcetcss16PgainFacInGearChng,
  B_LCETCS_vCalPGainFacInGearChng_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_lcetcss16PgainFacInGearChng;

  /* Constant: '<Root>/Constant1' */
  localB->x2 = ((uint8_T)U8ETCSCpGainTrnsTmAftrGearShft);

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  rtb_x = rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu8GainTrnsTm;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Constant'
   *  If: '<S2>/If'
   *  Logic: '<Root>/Logical Operator'
   */
  if (!((rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft) ||
        (rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft))) {
    /* Outputs for IfAction SubSystem: '<Root>/ResetPgainFacInGearChang' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    rtb_lcetcss16PgainFacInGearChng = 100;

    /* End of Outputs for SubSystem: '<Root>/ResetPgainFacInGearChang' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/SetPgainFacInGearChang' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    if (!rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' incorporates:
       *  ActionPort: '<S3>/Action Port'
       */
      localB->y1 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_5);
      localB->y2 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_4);
      localB->y3 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_3);
      localB->y4 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_2);
      localB->y5 = ((uint8_T)U8ETCSCpPgFacGrChgErrNeg_1);

      /* ModelReference: '<S3>/LCETCS_s16Inter5Point' */
      LCETCS_s16Inter5Point(rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                            ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                            localB->y1, localB->y2, localB->y3, localB->y4,
                            localB->y5, &localB->lcetcss16PgainFacInGearChng);
      rtb_lcetcss16PgainFacInGearChng = localB->lcetcss16PgainFacInGearChng;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
    } else {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' incorporates:
       *  ActionPort: '<S4>/Action Port'
       */
      /* If: '<S2>/If' incorporates:
       *  Constant: '<S4>/Constant'
       */
      rtb_lcetcss16PgainFacInGearChng = 100;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
    }

    /* End of Outputs for SubSystem: '<Root>/SetPgainFacInGearChang' */
  }

  /* End of If: '<Root>/If' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtCP_Constant2_Value, localB->x2,
                        rtb_lcetcss16PgainFacInGearChng, rtCP_Constant3_Value,
                        rty_lcetcss16PgainFacInGearChng);
}

/* Model initialize function */
void LCETCS_vCalPGainFacInGearChng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S3>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
