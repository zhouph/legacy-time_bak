/*
 * File: LCETCS_vDctSymBrkCtlEtrExt.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSymBrkCtlEtrExt'.
 *
 * Model version                  : 1.179
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSymBrkCtlEtrExt.h"
#include "LCETCS_vDctSymBrkCtlEtrExt_private.h"

const TypeETCSBrkCtlReqStruct
  LCETCS_vDctSymBrkCtlEtrExt_rtZTypeETCSBrkCtlReqStruct = {
  false,                               /* lcetcsu1SymBrkTrqCtlReqFA */
  false                                /* lcetcsu1SymBrkTrqCtlReqRA */
} ;                                    /* TypeETCSBrkCtlReqStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vDctSymBrkCtlEtrExt' */
void LCETCS_vDctSymBrkCtlEtrExt_Init(DW_LCETCS_vDctSymBrkCtlEtrExt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE =
    LCETCS_vDctSymBrkCtlEtrExt_rtZTypeETCSBrkCtlReqStruct;
}

/* Output and update for referenced model: 'LCETCS_vDctSymBrkCtlEtrExt' */
void LCETCS_vDctSymBrkCtlEtrExt(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR,
  const TypeETCSWhlStruct *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR,
  const TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL, const TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, int16_T
  rtu_lcetcss16SymBrkCtlStrtTh, const TypeETCSBrkCtlCmdStruct
  *rtu_ETCS_BRK_CTL_CMD_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA,
  TypeETCSBrkCtlReqStruct *rty_ETCS_BRK_CTL_REQ,
  DW_LCETCS_vDctSymBrkCtlEtrExt_f_T *localDW)
{
  boolean_T rtb_Switch;
  boolean_T tmp;
  int16_T tmp_0;
  boolean_T tmp_1;

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  if (((boolean_T)VarFwd)) {
    rtb_Switch = rtu_ETCS_FA->lcetcsu1SymSpinDctFlg;
  } else {
    rtb_Switch = rtu_ETCS_RA->lcetcsu1SymSpinDctFlg;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Switch: '<Root>/Switch4' incorporates:
     *  Constant: '<Root>/Constant5'
     *  UnitDelay: '<Root>/Unit Delay'
     */
    /* Transition: '<S1>:11' */
    /* Transition: '<S1>:10' */
    if (((boolean_T)VarFwd)) {
      tmp = localDW->UnitDelay_DSTATE.lcetcsu1SymBrkTrqCtlReqFA;
    } else {
      tmp = localDW->UnitDelay_DSTATE.lcetcsu1SymBrkTrqCtlReqRA;
    }

    /* End of Switch: '<Root>/Switch4' */
    if (tmp == 1) {
      /* Switch: '<Root>/Switch3' incorporates:
       *  Constant: '<Root>/Constant4'
       */
      /* Transition: '<S1>:24' */
      /* Transition: '<S1>:21' */
      if (((boolean_T)VarFwd)) {
        tmp_0 = rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdFA;
      } else {
        tmp_0 = rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdRA;
      }

      /* End of Switch: '<Root>/Switch3' */
      if (((tmp_0 <= 0) || (rtb_Switch == 0)) ||
          (rtu_ETCS_ENG_STALL->lcetcsu1EngStall == 1)) {
        /* Transition: '<S1>:32' */
        /* Transition: '<S1>:27' */
        rtb_Switch = false;

        /* Transition: '<S1>:44' */
      } else {
        /* Transition: '<S1>:43' */
        rtb_Switch = true;
      }

      /* Transition: '<S1>:45' */
      /* Transition: '<S1>:46' */
    } else {
      /* Switch: '<Root>/Switch1' incorporates:
       *  Constant: '<Root>/Constant2'
       */
      /* Transition: '<S1>:34' */
      if (((boolean_T)VarFwd)) {
        tmp = rtu_ETCS_FL->lcetcsu1BTCSWhlAtv;
      } else {
        tmp = rtu_ETCS_RL->lcetcsu1BTCSWhlAtv;
      }

      /* End of Switch: '<Root>/Switch1' */

      /* Switch: '<Root>/Switch2' incorporates:
       *  Constant: '<Root>/Constant3'
       */
      if (((boolean_T)VarFwd)) {
        tmp_1 = rtu_ETCS_FR->lcetcsu1BTCSWhlAtv;
      } else {
        tmp_1 = rtu_ETCS_RR->lcetcsu1BTCSWhlAtv;
      }

      /* End of Switch: '<Root>/Switch2' */
      if (((((((rtb_Switch == 1) && (tmp == 0)) && (tmp_1 == 0)) &&
             (rtu_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc > 0)) &&
            (rtu_ETCS_ENG_STALL->lcetcsu1EngStall == 0)) &&
           (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >= rtu_lcetcss16SymBrkCtlStrtTh))
          && (rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc <= ((uint8_T)
            U8ETCSCpSymBrkCtlIhbAxTh))) {
        /* Transition: '<S1>:38' */
        /* Transition: '<S1>:39' */
        rtb_Switch = true;

        /* Transition: '<S1>:46' */
      } else {
        /* Transition: '<S1>:40' */
        rtb_Switch = false;
      }
    }

    /* Transition: '<S1>:47' */
  } else {
    /* Transition: '<S1>:8' */
    rtb_Switch = false;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Switch: '<Root>/Switch5' incorporates:
   *  Constant: '<Root>/Constant6'
   *  Constant: '<Root>/Constant7'
   */
  /* Transition: '<S1>:49' */
  if (((boolean_T)VarFwd)) {
    rty_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqFA = rtb_Switch;
  } else {
    rty_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqFA = false;
  }

  /* End of Switch: '<Root>/Switch5' */

  /* Switch: '<Root>/Switch6' incorporates:
   *  Constant: '<Root>/Constant8'
   *  Constant: '<Root>/Constant9'
   */
  if (((boolean_T)VarFwd)) {
    rty_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqRA = false;
  } else {
    rty_ETCS_BRK_CTL_REQ->lcetcsu1SymBrkTrqCtlReqRA = rtb_Switch;
  }

  /* End of Switch: '<Root>/Switch6' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_BRK_CTL_REQ;
}

/* Model initialize function */
void LCETCS_vDctSymBrkCtlEtrExt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
