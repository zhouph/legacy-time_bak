/*
 * File: LCETCS_vCalBasDelYawCnvrgGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalBasDelYawCnvrgGain'.
 *
 * Model version                  : 1.261
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_h_
#define RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_h_
#ifndef LCETCS_vCalBasDelYawCnvrgGain_COMMON_INCLUDES_
# define LCETCS_vCalBasDelYawCnvrgGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalBasDelYawCnvrgGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalBasDelYawCnvrgGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalBasDelYawCnvrgGain' */
typedef struct {
  int16_T x3;                          /* '<S2>/Ay level on asphalt' */
  int16_T x1;                          /* '<S2>/Ay level on ice' */
  int16_T x2;                          /* '<S2>/Ay level on snow' */
  int16_T z13;                         /* '<S2>/Delta yaw gain on asphalt at speed 1' */
  int16_T z23;                         /* '<S2>/Delta yaw gain on asphalt at speed 2' */
  int16_T z33;                         /* '<S2>/Delta yaw gain on asphalt at speed 3' */
  int16_T z43;                         /* '<S2>/Delta yaw gain on asphalt at speed 4' */
  int16_T z53;                         /* '<S2>/Delta yaw gain on asphalt at speed 5' */
  int16_T z11;                         /* '<S2>/Delta yaw gain on ice at speed 1' */
  int16_T z21;                         /* '<S2>/Delta yaw gain on ice at speed 2' */
  int16_T z31;                         /* '<S2>/Delta yaw gain on ice at speed 3' */
  int16_T z41;                         /* '<S2>/Delta yaw gain on ice at speed 4' */
  int16_T z51;                         /* '<S2>/Delta yaw gain on ice at speed 5' */
  int16_T z12;                         /* '<S2>/Delta yaw gain on snow at speed 1' */
  int16_T z22;                         /* '<S2>/Delta yaw gain on snow at speed 2' */
  int16_T z32;                         /* '<S2>/Delta yaw gain on snow at speed 3' */
  int16_T z42;                         /* '<S2>/Delta yaw gain on snow at speed 4' */
  int16_T z52;                         /* '<S2>/Delta yaw gain on snow at speed 5' */
  int16_T lcetcss16BasDelYawCnvrgGain; /* '<S2>/LCETCS_s16Inter3by5' */
  int16_T x3_h;                        /* '<S1>/Ay level on asphalt' */
  int16_T x1_g;                        /* '<S1>/Ay level on ice' */
  int16_T x2_h;                        /* '<S1>/Ay level on snow' */
  int16_T z13_a;                       /* '<S1>/Delta yaw gain on asphalt at speed 1' */
  int16_T z23_f;                       /* '<S1>/Delta yaw gain on asphalt at speed 2' */
  int16_T z33_n;                       /* '<S1>/Delta yaw gain on asphalt at speed 3' */
  int16_T z43_o;                       /* '<S1>/Delta yaw gain on asphalt at speed 4' */
  int16_T z53_c;                       /* '<S1>/Delta yaw gain on asphalt at speed 5' */
  int16_T z11_i;                       /* '<S1>/Delta yaw gain on ice at speed 1' */
  int16_T z21_b;                       /* '<S1>/Delta yaw gain on ice at speed 2' */
  int16_T z31_e;                       /* '<S1>/Delta yaw gain on ice at speed 3' */
  int16_T z41_d;                       /* '<S1>/Delta yaw gain on ice at speed 4' */
  int16_T z51_k;                       /* '<S1>/Delta yaw gain on ice at speed 5' */
  int16_T z12_b;                       /* '<S1>/Delta yaw gain on snow at speed 1' */
  int16_T z22_l;                       /* '<S1>/Delta yaw gain on snow at speed 2' */
  int16_T z32_d;                       /* '<S1>/Delta yaw gain on snow at speed 3' */
  int16_T z42_m;                       /* '<S1>/Delta yaw gain on snow at speed 4' */
  int16_T z52_a;                       /* '<S1>/Delta yaw gain on snow at speed 5' */
  int16_T lcetcss16BasDelYawCnvrgGain_o;/* '<S1>/LCETCS_s16Inter3by5' */
} B_LCETCS_vCalBasDelYawCnvrgGain_c_T;

typedef struct {
  B_LCETCS_vCalBasDelYawCnvrgGain_c_T rtb;
} MdlrefDW_LCETCS_vCalBasDelYawCnvrgGain_T;

/* Model reference registration function */
extern void LCETCS_vCalBasDelYawCnvrgGain_initialize(void);
extern void LCETCS_vCalBasDelYawCnvrgGain_Start
  (B_LCETCS_vCalBasDelYawCnvrgGain_c_T *localB);
extern void LCETCS_vCalBasDelYawCnvrgGain(const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16VehSpd, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, int16_T *rty_lcetcss16BasDelYawCnvrgGain,
  B_LCETCS_vCalBasDelYawCnvrgGain_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalBasDelYawCnvrgGain'
 * '<S1>'   : 'LCETCS_vCalBasDelYawCnvrgGain/I gain to increase torque fast'
 * '<S2>'   : 'LCETCS_vCalBasDelYawCnvrgGain/I gain to increase torque slow'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
